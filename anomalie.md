# Commenti dal nome anomalo

### Commento che contiene -c3a8-

"seed": {
"key": "posts-2022-05-26-c3a8-cam",
"title": "È cambiato il metronomo",

"seed": {
"key": "posts-1-01-01-c3a8-cambia",
"title": "È cambiato il metronomo",

### Commento che contiene -a-piena-pagin-

"seed": {
"key": "posts-1-01-01-a-piena-pagin",
"title": "A piena pagina",

### Commento che contiene -codice-di-com

"seed": {
"key": "posts-1-01-01-codice-di-com",
"title": "Codice di comportamento",

### Commento che contiene -come-non-r

"seed": {
"key": "posts-1-01-01-come-non-r",
"title": "Come non resistere alla tentazione (di nuovo)",

"seed": {
"key": "posts-2022-11-11-come-non-r",
"title": "Come non resistere alla tentazione (di nuovo)",

"seed": {
"key": "posts-2022-11-10-come-non-r",
"title": "Come non resistere alla tentazione",

### Commento che contiene -alleggerire-

"seed": {
"key": "blog-2022-02-18-alleggerire",
"title": "Alleggerire la pressione",

"/blog/blog/2016/09/22": [
{
"seed": {
"key": "alleggerire-la-pressione",
"title": "Alleggerire la pressione",

"seed": {
"key": "blog-2022-02-18-alleggerire",
"title": "Alleggerire la pressione",

### Commento che contiene -lo-voglio-g

"seed": {
"key": "blog-2022-02-19-lo-voglio-g",
"title": "Lo voglio gold",

### Commento che contiene -nuovo-backup

"/quickloox/blog/blog/2013/08/12": [
{
"seed": {
"key": "un-nuovo-backup",
"title": "Un nuovo backup",

"/blog/blog/2013/08/12": [
{
"seed": {
"key": "un-nuovo-backup",
"title": "Un nuovo backup",

### Commento che contiene -due-modi

"/quickloox/blog/blog/2013/08/20": [
{
"seed": {
"key": "due-modi-per-toccare-il-fon",
"title": "Due modi per toccare il fondo",
"body": "",
"date": 1376982872,
"path": "/quickloox/blog/blog/2013/08/20",
"author": null,
"likes": []
},
"replies": [
{
"key": "sil-lo-avevo-gia-incontrato",
"body": "Sil lo avevo già incontrato e provato. Brogue invece non lo conoscevo proprio e posso solo dire che è fantastico! Ancora una volta, grazie Lux!",
"date": 1376983009,
"path": "/quickloox/blog/blog/2013/08/20",
"author": {
"path": "@marcoli",
"displayname": "Marco Olivares",
"img": "//graph.facebook.com/100002354253588/picture?type=large"
},
"likes": []
},
{
"key": "sil-lo-avevo-gia-conosciuto",
"body": "Sil lo avevo già conosciuto e provato. Brogue invece non lo avevo proprio mai sentito ed è fantastico! Ancora una volta, grazie Lux!",
"date": 1376982872,
"path": "/quickloox/blog/blog/2013/08/20",
"author": {
"path": "@marcoli",
"displayname": "Marco Olivares",
"img": "//graph.facebook.com/100002354253588/picture?type=large"
},
"likes": []
}
]
}
],

### Commento che contiene prepararsi-per-la-scuola

"/blog/blog/2013/08/21": [
{
"seed": {
"key": "prepararsi-per-la-scuola",
"title": "Prepararsi per la scuola",

"/quickloox/blog/blog/2013/08/21": [
{
"seed": {
"key": "prepararsi-per-la-scuola",
"title": "Prepararsi per la scuola",
