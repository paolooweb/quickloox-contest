---
title: Pro (e contro)
date: 2024-02-13
tags: ["ping"]
---

Mi infastidiscono le nozioni di <em>basic</em> e <em>pro</em> che girano intorno al Mac e all&#8217;informatica in generale.

Il marketing aggiunge la parola <em>pro</em> per vendere configurazioni pi&#249; ricche e potenti, ma finisce l&#236;. La maggior parte del lavoro di sviluppo di <a href="http://php.net/" target="_blank">Php 5</a> &#232; stata svolta su un MacBook. Il vero <em>pro</em> vuole un Mac potente per stare comodo e spremerlo al massimo, ma spreme al massimo qualunque cosa abbia sotto le mani. Prima di arrivare a giocare negli stadi della Champions League, su quell&#8217;erba perfetta, qualunque <em>pro</em> si &#232; prima smazzato il campetto dell&#8217;oratorio.

Sembra anche a volte che un <em>pro</em> sia uno che sa usare molte applicazioni, e un <em>basic</em> solo poche. Col cavolo: <a href="http://www.martinrainone.com" target="_blank">Martin e Rainone</a> lavorano per il 90 percento del tempo in Photoshop e sono eccellenza. Ma chiedergli di usare <a href="http://www.guit.sssup.it/" target="_blank">TeX</a> oppure <a href="http://cran.r-project.org/" target="_blank">R</a> porterebbe a poco.

Un altro mito &#232; che un <em>pro</em> usi per forza applicazioni molto specifiche, le cosiddette <em>verticali</em> di cui si riempiono la bocca i venditori.

Non sono d&#8217;accordo. Un asso del Mac lo metti l&#236; davanti alla dotazione standard del sistema e far&#224; meraviglie. Anzi, conosco <a href="http://www.sourcesense.com/it/home" target="_blank">pro dell&#8217;open source</a> che il Mac lo fanno partire direttamente dal Terminale e usano solo quello, altro che le applicazioni verticali. Certe volte quello che ha bisogno del superprogramma per ottenere un risultato &#232; uno che deve affidarsi al software perch&#233; da solo non ce la fa.

Alla fine, il <em>pro</em> &#232; uno che ha grande conoscenza e fa grandi cose. Il computer &#232; uno strumento, il software pure. Un righello &#232; sempre un righello, in mano a me e in mano a Renzo Piano. Un righello costa niente. Lui sa usarlo, per&#242;.