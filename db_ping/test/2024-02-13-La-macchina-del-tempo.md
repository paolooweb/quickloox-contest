---
title: La macchina del tempo
date: 2024-02-13
tags: ["ping"]
---

Per sapere cosa farà Microsoft domani basta guardare che cosa ha fatto Apple ieri

Apple ha annunciato un negozio di musica online accessibile attraverso iTunes, in collaborazione con varie etichette discografiche che rendono disponibile un totale di ducentomila titoli a 99 centesimi di dollaro per canzone o a 9,99 dollari per un Cd.

Vuol dire due cose.

Primo, uscirà iTunes per Windows.

Secondo, Microsoft annuncerà tra pochi mesi un servizio di qualità inferiore perfettamente uguale nella dinamica, con qualche differenza di forma. E sui giornali si parlerà della rivoluzione di Bill Gates che entra nel mondo della musica digitale.

Usare un Mac è come usare la macchina del tempo: sai già che cosa verrà imitato in futuro.

<link>Lucio Bragagnolo</link>lux@mac.com