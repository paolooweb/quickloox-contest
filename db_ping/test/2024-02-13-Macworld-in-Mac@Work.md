---
title: Nel tuo store preferito
date: 2024-02-13
tags: ["ping"]
---

Informazione viziata da conflitto di interessi

Visto che sono già stato accusato di sfruttare questa rubrica per scopi personali, lo faccio appieno: limitatamente al numero di giugno, ma su base regolare a partire da questo autunno, il tuo mensile Mac preferito sarà in vendita anche nello store Mac di cui sono socio. Sarà anche possibile abbonarsi.

Lo store Mac di cui sono socio, che è Mac@Work di via Carducci a Milano, si avvia inoltre a diventare Apple Center ufficiale: la cosa più vicina possibile agli Apple Store americani. Le differenze si vedranno anche nell’arredamento e nell’esposizione. A Milano diventeranno Apple Center anche CE Communications & Engineering di piazza Firenze e il Media Store di viale Piave. A Roma ce ne saranno tre, uno dei quali sarà Futura Grafica. Ce ne sarà uno anche a Verona e a Padova; in tutto, se non erro, gli Apple Center ufficiali italiani saranno in totale dodici.

Se sia conflitto di interessi o informazione, decidilo tu e insultami pure via mail. L’indirizzo è qui sotto.

<link>Lucio Bragagnolo</link>lux@mac.com