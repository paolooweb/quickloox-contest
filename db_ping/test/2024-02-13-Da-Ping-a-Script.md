---
title: Da Ping! a Script
date: 2024-02-13
tags: ["ping"]
---

La mia generazione ha fatto in tempo a conoscere l&#8217;analogico e ad abbracciare il digitale. Qualcuno la considera una sfortuna, qualcuno una straordinaria e fortunata coincidenza.

Lascio aperto il giudizio e intanto formulo una considerazione inattaccabile: la mia generazione ha dovuto affrontare il passaggio dalla cura delle cose analogiche alla disinvoltura di quelle digitali.

I libri che ho a casa infatti vanno conservati con una certa cura; stampati come sono in innumerevoli copie, ne possiedo un solo esemplare. I bit che maneggio scrivendo o disegnando non hanno bisogno di cure: un tocco e posso riprodurli in quante copie voglio, in giro per il mondo.

La mia generazione ha dovuto imparare che sbagliare un disegno su un foglio di carta comporta la spesa di un foglio di carta e sbagliare un disegno sullo schermo invece non comporta niente, si rif&#224; e basta. Nel mondo digitale ci&#242; che conta veramente sono le idee e il loro contenitore ha importanza assai relativa.

&#200; cos&#236; che le mie idee spero continuino a contare qualcosa, anche se il contenitore cambia. Da ora Ping! diventa Script e le mie intemperanze quotidiane risiederanno l&#236;.

La nuova sede è ancora provvisoria e rimane qualche tubatura da allacciare, ma è gi&#224; confortevole.

Prima di salutare definitivamente voglio inviare un saluto particolarmente grato al reparto tecnico di Idg prima, Nuov@ Periodici successivamente e Just Be poi: persone di pazienza unica e talento tecnico di prim&#8217;ordine. E poi a tutti quanti, in Just Be ora, Nuov@ Periodici prima e Idg prima ancora, hanno sostenuto nei modi e tempi pi&#249; disparati Ping: in ordine qualsiasi Enrico Lotti, Francesco Pignatelli, Marco Tennyson, Paolo Morati e molti altri.

Grazie infine a tutti quanti hanno seguito a qualsiasi titolo questa avventura. Se vorranno continuare a seguirla in un nuovo contenitore, saranno i benvenuti. Sarebbe poco elegante linkarlo qui. Chi vorr&#224; lo potr&#224; trovare facilmente. O <a href="mailto:lux@mac.com?subject=Da%20Ping%20a%20Script" target="_blank">scrivermi</a>.
