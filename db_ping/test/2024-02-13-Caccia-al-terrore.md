---
title: Caccia al terrore
date: 2024-02-13
tags: ["ping"]
---

La Stampa ha pubblicato sul proprio sito <a href="http://www.lastampa.it/_web/cmstp/tmplrubriche/tecnologia/grubrica.asp?ID_blog=30&ID_articolo=8713&ID_sezione=38&sezione=" target="_blank">la notizia dei nuovi MacBook Pro con tecnologia Thunderbolt</a>.

La quantit&#224; di errori ortografici, tecnici, concettuali e deontologici in un testo cos&#236; breve &#232; terrorizzante.

Ho registrato un Pdf della pagina per conservarla a futura memoria.

Un gioco: quanti errori dei tipi sopraelencati si riescono a trovare? Come minimo sono almeno una decina, in venti righe.

Grazie a <b>Iruben</b> per la dritta!