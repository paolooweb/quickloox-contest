---
title: Tutto e il suo contrario
date: 2024-02-13
tags: ["ping"]
---

John Gruber di <a href="http://daringfireball.net" target="_blank">Daring Fireball</a> resta inimitabile. In tre righe ha messo il cappello da asino in testa a <a href="http://blogs.adobe.com/flashplatform/2011/02/update-for-fp-10-2-on-tabs.html" target="_blank">Matt Rozen di Adobe</a>. Ecco come.

Rozen: Adobe offrir&#224; Flash Player 10.2 preinstallato su alcuni <i>tablet</i> e da scaricare entro poche settimane per apparecchi Android 3 (Honeycomb) di prossimo arrivo, Xoom di Motorola per cominciare.

Gruber: <cite>Cos&#236; siamo a febbraio 2011 e Adobe ancora non ha una versione</cite> tablet <cite>di Flash Player.</cite>

Rozen: I consumatori stanno chiedendo con chiarezza il supporto di Flash da parte dei <cite>tablet</cite>.

Gruber: <cite>Comprando iPad?</cite>

Rozen: E la buona notizia &#232; che non dovranno aspettare a lungo.

Gruber: <cite>Ma dovranno aspettare.</cite>