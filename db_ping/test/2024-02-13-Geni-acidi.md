---
title: Geni acidi
date: 2024-02-13
tags: ["ping"]
---

I geni mi raccontano acidamente che a causa di Mac App Store la qualit&#224; del software per Mac diminuir&#224;.

Pu&#242; essere. Fino a ieri tuttavia ignoravo totalmente l&#8217;esistenza di <a href="http://www.typedna.com/" target="_blank">TypeDna</a>.

Il modo pi&#249; sintetico per descriverlo: il dizionario dei sinonimi e dei contrari applicato ai font.

Non so se e quanto funzioni; Macworld.com <a href="http://www.macworld.com/article/157703/2011/02/fontdna2.html" target="_blank">ne parla bene</a>.

Semmai, Mac App Store potrebbe spingere gli autori a produrne una versione nativa per Mac, che non ha bisogno di Flash. Ma software cos&#236; &#232; intelligente e di buon supporto geniale alla creativit&#224;.