---
title: Di tastiera in tastiera
date: 2024-02-13
tags: ["ping"]
---

Quando ho acquistato iPad non avrei mai pensato che ci avrei scritto cos&#236; tanto. Su Mac uso Pages pochissimo, il mio ambiente di scrittura &#232; BBEdit. Ho gi&#224; prodotto molto pi&#249; testo su <a href="http://itunes.apple.com/it/app/pages/id361309726?mt=8" target="_blank">Pages iPad</a> di quanto non ne abbia mai fatto su <a href="http://itunes.apple.com/it/app/pages/id409201541?mt=12" target="_blank">Pages Mac</a>.

Per l&#8217;Html la mia scelta resta <a href="http://www.textasticapp.com/" target="_blank">Textastic</a>, che ora &#232; arrivato all&#8217;edizione 2.1. Si integra con Dropbox, ha l&#8217;autocorrezione e l&#8217;autocompletamento di sistema, una tastiera ausiliaria per aiutare programmatori e accatiemmellisti, un sito dove &#232; straordinariamente efficiente chiedere <i>feedback</i> e un autore che ascolta (una funzione della versione 2.0, ho controllato, l&#8217;ho chiesta io e solo io ed &#232; arrivata. Non ci potevo credere). Non tutto va ancora come vorrei e devo ammettere che un vero emulo di BBEdit per iPad non esiste ancora. Per&#242; si migliora e quando leggo critiche sulla tastiera virtuale di iPad mi viene da ridere. Non pu&#242; emulare al cento percento quella fisica; ma all&#8217;ottanta percento s&#236; e in mobilit&#224; estrema oramai scrivo a una velocit&#224; che fa impressione anche a me.

Avevo gi&#224; scritto pi&#249; o meno queste cose, ma oggi le riscrivo con pi&#249; convinzione e con ulteriore raccomandazione. Tra l&#8217;altro si approssima iPad 2 e per gli scrittori mobili potrebbe essere una occasione ancora migliore.