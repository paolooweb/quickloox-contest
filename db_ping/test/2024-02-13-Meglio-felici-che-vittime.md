---
title: Meglio felici che vittime
date: 2024-02-13
tags: ["ping"]
---

Usare bene un computer significa non dover mai dire “Mi dispiace che ne esca un altro”

Il mese scorso ho comprato un Titanium, per giunta usato. Ma stupendo e con tutto quello che mi serve. Ora sono arrivati i Titanium con il combo drive di serie, quello che legge i Dvd e masterizza pure i Cd.
Ci sono rimasto un po’ così, perché se lo avessi saputo forse avrei rinviato l’acquisto di un mesetto. Reazione emotiva, assai comprensibile. Ma sono ugualmente felice del mio T-Lux, perché ho tutto quello che mi serve, e non lo cambierò per un pezzo.
Poi ho sentito uno, arrabbiato perché ha comprato un Titanium settimana scorsa: Apple avrebbe dovuto avvisarmi. Sono stato quasi truffato. Lo hanno fatto apposta per svuotare i magazzini. Posso denunciare il rivenditore? Eccetera.
Senza pensare che, se Apple avesse avvisato lui, avrebbe scontentato quello che aveva comprato la settimana prima ancora. E così all’infinito.
Ma allora, perché non avvisare tutti un mese prima? Perché si scontenterebbero quelli della settimana prima dell’avviso. E quelli che in quel mese magari avrebbero veramente bisogno di un Mac, ma non lo troverebbero. E Apple resterebbe a vendite zero per un mese ogni volta che fa uscire un nuovo modello. Insomma, una stupidaggine.
Morale: la reazione emotiva è più che comprensibile. L’atteggiamento da vittima invece fa ridere. Se hai comprato un computer che ti serve, ti serve ancora. Se non è il computer che ti serve, hai fatto male a comprarlo e avresti dovuto comprarne uno che ti serviva.
Il mio Titanium, intanto, non ha il combo drive ma lo tratto come un cucciolo. Anche se quello nuovo, di PowerBook G4, è da urlo.