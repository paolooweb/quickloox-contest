---
title: &#200; finita un&#8217;era
date: 2024-02-13
tags: ["ping"]
---

Dedicato ai fanatici del processore, della nuova Usb a tutti i costi, del <cite>pi&#249; &#232; grande meglio &#232;</cite>.

<cite>Nel Dna di Apple c&#8217;&#232; la nozione che la tecnologia da sola non &#232; abbastanza. &#200; la tecnologia sposata con le arti liberali, con l&#8217;umanit&#224;, che produce risultati capaci di fare cantare il cuore. Da nessuna parte ci&#242; &#232; vero come in questi apparecchi post-Pc. E un sacco di persone in questo mercato di tavolette guarda a esse come al prossimo Pc.</cite>

<a href="http://news.cnet.com/8301-17852_3-20038425-71.html" target="_blank">Steve Jobs</a>, indovinare alla presentazione di che cosa.

Fino a che Apple riesce a conservare questa visione e gli altri riescono a muoversi con l&#8217;attuale goffaggine, non c&#8217;&#232; veramente partita.