---
title: Per pochi, ma non per tutti
date: 2024-02-13
tags: ["ping"]
---

Il Sans Institute ha pubblicato la <a href="http://www.sans.org/top20/" target="_blank">top 20 dei rischi sicurezza per il 2007</a>. Documento imperdibile. Al capitolo delle vulnerabilit&#224; client-side, vengono elencate 21 vulnerabilit&#224; per Explorer, 14 per Firefox e quattro per Acrobat Reader.

Ho sempre incontrato gente che mi diceva <cite>non ci sono virus per Mac perch&#233; i Mac sono pochi</cite>, a intendere che un Mac ha gli stessi problemi di Windows e si salva unicamente perch&#233; &#232; meno diffuso.

Eppure Firefox (circa il 13 percento del traffico) &#232; <a href="http://www.thecounter.com/stats/2007/October/browser.php" target="_blank">molto meno diffuso</a> di Internet Explorer (circa il 77 percento del traffico). Se vale l&#8217;argomento della diffusione, come mai ha due terzi delle vulnerabilit&#224; di Explorer, con un sesto del traffico?