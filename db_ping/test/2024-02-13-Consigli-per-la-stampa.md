---
title: Consigli per la stampa
date: 2024-02-13
tags: ["ping"]
---

Tale Paul J. Miller se ne va da Engadget, acquistato da Aol, e scrive la <a href="http://pauljmiller.com/?p=5" target="_blank">lettera d&#8217;addio</a>, comprendente il seguente paragrafo:

<cite>Mi piacerebbe proseguire questo lavoro per sempre, ma sfortunatamente Engadget &#232; propriet&#224; di Aol e Aol si &#232; dimostrata</cite> partner <cite>poco disponibile nell&#8217;evoluzione di questo sito. Non ci vuole un veterano dell&#8217;editoria per capire che Aol ha il cuore nel posto sbagliato rispetto ai contenuti. [&#8230;] Aol vede il contenuto come pretesto per vedere pubblicit&#224;. Potrebbe rendere (sebbene ne dubiti), ma non promuove buon giornalismo n&#233; buon intrattenimento.</cite>

Il buon giornalismo e la buona informazione sono risultati di un connubio equilibrato e di qualit&#224; tra contenuti e informazione.

Leggi quello che ti pare. Ma se &#232; solo un pretesto per vendere pubblicit&#224;, realizzato alla svelta e male, sappi che vale poco. E per loro vali poco anche tu.