---
title: Il coraggio della lamentazione
date: 2024-02-13
tags: ["ping"]
---

Criticare Apple spesso è uno sport. Salviamo almeno la coerenza

Ogni tanto Apple sbaglia. O almeno scontenta una buona fetta dei propri clienti.
Per esempio, sento regolarmente gente lamentarsi del fatto che gli update a Mac OS X sono molto pesanti e scaricarli da Internet è un problema.

Ultimamente Apple ha lanciato Applemusic.com, il servizio per acquistare musica online. Aac è uno standard di compressione più efficiente di Mp3, ma anche così una singola canzone si misura in megabyte e un album intero non è precisamente un fuscello telematico.

Almeno per ora il servizio è attivo solo in Usa e sento molta gente lamentarsi del fatto che il servizio non sia utilizzabile dall’Italia.

Ecco: vorrei sapere se ci sono persone che contemporaneamente si lamentano degli update disponibili solo via Internet, troppo pesanti da scaricare, e dell’impossibilità di usare Applemusic.com, pieno di canzoni pesanti da scaricare.

Ci vuole un coraggio da leoni. O una faccia di bronzo senza pari.

<link>Lucio Bragagnolo</link>lux@mac.com