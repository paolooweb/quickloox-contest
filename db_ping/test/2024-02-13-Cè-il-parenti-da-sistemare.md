---
title: Parenti da sistemare
date: 2024-02-13
tags: ["ping"]
---

Mercoled&#236; 2 marzo prossimo parteciper&#242; al <a href="http://www.insidesrl.it/creativityday/2011/index.cfm" target="_blank">Creativity Day</a> a Milano, organizzato da InSide Professional Training presso il teatro Franco Parenti.

Sar&#242; l&#236; per tutta la giornata e pienamente disponibile a saluti e chiacchiere, ovviamente fatta eccezione per il tempo dedicato al mio intervento.

Fino a tutto domani (gioved&#236;) l&#8217;iscrizione alla manifestazione &#232; gratuita, poi coster&#224; diciotto euro. Il che non dovrebbe essere un problema in quanto, sottoscritto escluso, &#232; presente e mostrer&#224; meraviglie la crema della crema della creativit&#224; italiana e continentale.

Così ci distraiamo come si deve aspettando <a href="http://www.loopinsight.com/2011/02/23/apple-announces-ipad-2-event-for-march-2/" target="_blank">la presentazione del nuovo iPad</a>.