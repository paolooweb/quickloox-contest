---
title: Era personale
date: 2024-02-13
tags: ["ping"]
---

Chiedo venia per l&#8217;autocelebrazione, ma classifico privatamente i Ping con un codice alfabetico di tre lettere, con l&#8217;alfabeto da ventisei lettere.

Questo Ping &#232; il &#8220;numero&#8221; bzz. Significa che il prossimo sar&#224; caa.

Lascio ai volontari del calcolo determinare quanti sono. Io mi limito a farmi modeste, ma sentite e sudate, congratulazioni per essere entrato nella mia tutta personale era della C. Che comunque non sarebbe mai arrivata senza tutta questa graditissima compagnia. Grazie. :-)