---
title: Andarono per pescare...
date: 2024-02-13
tags: ["ping"]
---

Belli i nuovi portatili Compaq, ma basta un iBook per la superiorità tecnologica

Cedo la parola al bravissimo Fab, cuore e cervello di macatwork.net. Attenzione: è una storia vera.

“A proposito di pescatori a mosca... il mio babbo ha un portatile Compaq, bello (beh...) nuovo.
Senza chiedermi nulla si è comprato una fotocamera digitale. Un’Olympus Camedia da quattro megapixel. Bella.
Passo da casa mentre la toglie dall’imballo; lui si installa il mondo di software che viene dato su Cd e mi dice che è tutto per Windows, sorridendo maligno. Io sorrido e fotografo l’iBook azzurro di mia madre, il cane che dormicchia e il mio iBook bianco. Attacco il cavo Usb, sorridendo sempre, e mi siedo vicino a lui che installa e installa e riavvia e riavvia.
Sull’iBook appaiono le anteprime delle foto. Gli dico: guarda che belle... :)
Mia madre chiede di vederle. Entro nel suo disco via AirPort e le dico che le ha già sul desktop, miracolo via radio di questo quasi 2002 davanti a un babbo che riuscirà a scaricarsi le stesse foto solo un’oretta dopo.
Io reinfilo l’iBook nello zaino e filo a casa”.