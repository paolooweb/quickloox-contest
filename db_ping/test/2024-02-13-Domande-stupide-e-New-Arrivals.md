---
title: Nuovi arrivati
date: 2024-02-13
tags: ["ping"]
---

Certe volte il posto migliore per cercare è quello ovvio

Questo è un mondaccio cane, dove non puoi più porre una domanda stupida senza che gli arroganti come me ti ricordino di fare prima una ricerca su Google.

In realtà è vero che le domande stupide, tante volte, non c’è neanche più bisogno di farne. Per esempio, esiste la <link>guida</link>http://guide.apple.com/ Apple all’hardware e al software compatibile Mac. Una ricerca lì e si risolvono in un attimo tanti dubbi.

Non solo: c’è una sezione <link>New Arrivals</link>http://guide.apple.com/newarrivals/ dove si vedono le ultime novità. Un buon posto dove mandare il marziano che crede ancora alle leggende per cui i Mac non hanno stampanti compatibili oppure hanno pochi programmi o altre fregnacce simili.

O, per metterla in altro modo, non solo si possono fare domande stupide, ma in certi casi è stupido già solo fare la domanda.

<link>Lucio Bragagnolo>/link>lux@mac.com