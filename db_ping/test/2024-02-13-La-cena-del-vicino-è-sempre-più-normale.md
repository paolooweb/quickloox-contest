---
title: La cena del vicino &#232; sempre pi&#249; normale
date: 2024-02-13
tags: ["ping"]
---

<em>Mi si &#232; rotto l&#8217;hard disk della marca Tale. Ma non sono il solo, eh? Anche mio cugggino*&#8230; certamente la qualit&#224; non pi&#249; quella di un tempo. Non pu&#242; essere un caso. State lontani dalla marca Tale!</em>

Quella di trovarsi sempre al centro di eventi significativi &#232; una presunzione molto umana e comprensibile. Il pi&#249; delle volte, fallace.

Qualche sera fa eravamo in nove alla cena di compleanno indetta dal vicino di casa. A un certo punto &#232; saltato fuori che c&#8217;erano tre persone dello stesso segno zodiacale. E qualcuno ha espresso meraviglia.

In realt&#224;, se non sto facendo errori grossolani, si tratta di una situazione assolutamente comune, che con questi numeri ha luogo pi&#249; di tre volte su quattro. Eppure a qualcuno sembrava una incredibile combinazione.

Di pi&#249;. Il mio vicino ha amici suoi, fa tutt&#8217;altro lavoro, frequenta altri ambienti. Eppure, tolta dal gruppo la mia signora per non alterare il campione, eravamo in due su otto a usare Mac. Il 25 percento.

Dovrei trarne indicazioni sulla quota di mercato del Mac? Evidentemente no. Anche questo, dunque, era un evento incredibile e singolare? Evidentemente, no.