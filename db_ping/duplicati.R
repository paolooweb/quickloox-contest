library(data.table)

duplicati <- fread("trovati.csv")
duplicati[, pubblicato := as.Date(pubblicato)]
str(duplicati)
uniqueN(duplicati, by = c("titolo"))
duplicati2 <- duplicati[!duplicated(duplicati), ]
duplicati2
setorder(duplicati2, pubblicato)
fwrite(duplicati2, "trovati2.csv", sep = ";")
