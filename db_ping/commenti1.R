library(rvest)
library(data.table)
ping <- read_html("https://web.archive.org/web/20110310114757/http://www.macworld.it:80/category/ping")

titolo <- ping |>
  html_elements("h3") |>
  html_text2()
titolo

pubblicato <- ping |>
  html_elements(".entry") |>
  html_text2()
pubblicato1 <- as.data.table(pubblicato)
class(pubblicato1)

fwrite(pubblicato1, "pubblicato1.csv")

insieme <- as.data.table(cbind(titolo, pubblicato))
class(insieme)
insieme


post <- read_html("https://web.archive.org/web/20100617235936/http://www.macworld.it/ping/nomi/2010/06/12/un-libro-solo-un-libro/")
commenti <- post |>
  html_elements(".singlePing a, .comment-body") |>
  html_text2()
commenti <- as.data.table(commenti)
