#! /opt/homebrew/bin/zsh
#
setopt extendedglob
for t in **/^ledate*; do
	pubb=$(gawk -F ";" '$1=="'"$t"'" {print $2}' ../trovati.csv)
	gsed -i.bu "1a date: $pubb" $t
done
unsetopt extendedglob
