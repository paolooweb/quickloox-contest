# quickloox-contest

## Cambiamenti recenti

Comma è tornato quello originale e le modifiche al sorgente sono state eliminate per possibili problemi di sicurezza. Sono stati aggiunti due processi in javascript, al di fuori di comma, che si occupano di rendere quanto scritto in markdown nei commenti dopo averlo sanificato da eventuale codice malevolo.

Quanto segue è relativo al contest non competitivo che riguarda [Quickloox](https:macintelligence.org).

Questo repo contiene dei file di esempio da poter utilizzare per costruire un flusso di lavoro che consenta di importare i vecchi commenti di Muut nel nuovo sistema di commenti autogestito.

Per conoscere meglio la struttura dei dati dei commenti attuali consiglio di leggere [la pagina di comma](https://github.com/Dieterbe/comma). In questa repo trovate dei file con suffisso .cmt di esempio. Sono file xml, uno per ogni commento che viene lasciato. È sufficientemente intuitivo riconoscere la struttura di ogni singolo file e come si colleghi con il post relativo.

I commenti salvati da Muut sono in formato json e contenuti in un unico grande file. Per semplificare un primo approccio con il problema ho estratto dei commenti in formato json e creato dei commenti, ai medesimi post, con comma. Comma non prevede un sistema di repliche dirette a un commento o la sezione "likes".

Ho aggiunto due cartelle

-   blog-v4
-   comma

### blog-v4

Questa cartella replica la cartella del blog Quickloox per gli esperimenti. È la cartella da cui partire per far girare in locale il blog con il comando

`hugo server --config-dev.toml --disableFastRender --gc`

Parte un server locale raggiungibile a localhost:1313.

Nella cartella content sono contenuti i post e le altre pagine del sito.
Nella cartella layouts/partials trovate comments.html sul quale stiamo discutendo.
La parte finale del file static/css/hugo-octopress.css contiene il css dei commenti.

N.B.: hugo non necessita dell'istallazione di Go.

### comma

Qui trovate i sorgenti di comma e un eseguibile compilato per Apple Silicon M1. Per altri processori dovete scaricare Go e compilare i sorgenti.

Create una cartella per i commenti se ne volete una nuova vuota, altrimenti avete già nel repo la cartella commenti con il risultato del lavoro di paoloo.

Per far partire comma aprite una nuova finestra del terminale e digitate

`./~/path/to/comma nome-cartella-commenti :5888`

Partirà anche qui un server dedicato ai commenti.

Se tutto è andato per il verso giusto, dopo un refresh, la pagina di qualsiasi post mostrerà lo spazio per i commenti.

Richieste di chiarimenti attraverso i soliti canali.

Vanno pubblicamente ringraziati -uso i nickname di Slack- briand06, macmomo e paoloo, che stanno dando contributi fondamentali alla struttura del blog.
