/*
 *
 *
 * 17 Aprile 2024
 *
 * Paoloo Webintosh
 *
 */

const fs = require("fs");
const readline = require("readline");
const { resolve } = require("path");

// Leggi il file JSON, e lo converte in oggetto js
const data = fs.readFileSync("./quickloox.json", "utf8");
const object = JSON.parse(data);

const mdFileNames = [];
const mdFileNamesDate = [];
const mdFileNamesTitles = [];

var hash = 2000000000;

var validPosts_count = 0;
var invalidPosts_count = 0;
var validCmtFiles_count = 0;
var correctedPosts_count = 0;
var correctedPostsPercent_count = 0;
var totalCmtFiles_count = 13088;

var invalidPostNames = [];
var correctedPostNamesForTitlePercentageEquality = [];

var substitution_postnames = [
    "2021-02-23-trilione",
    "2021-08-13-no-4k-ahiahiahiahi",
    "2021-08-26-ipertesti-fisici",
    "2021-09-05-il-canto-delle-sirene",
    "2015-05-12-la-scoperta-del-fuoco",
    "2020-08-11-le28099innesto-della-coe",
    "2023-12-27-la-fonderia-di-una-volta",
    "2024-02-12-rivoluzione-o-conservazione",
];
var substitution_mdFilenames = [
    "2017-09-10-il-trilione",
    "2021-08-13-uno-schermo-per-tutti-tutti-per-una-app",
    "2021-08-26-lavoretti-di-ipertesto",
    "2021-09-03-le-parole-sono-importanti",
    "2015-05-10-la-carica-dei-trentacinquemila",
    "2020-08-11-l’innesto-della-coerenza",
    "2023-12-27-addio-sourceforge",
    "2024-02-12-conservazione-o-rivoluzione",
];
var subsitutedPostnames = [];
var subsitutedPost_count = 0;

// Legge il path della cartella contenente i post, se l'argomento è specificato
if (process.argv[2]) {
    const dirPath = process.argv[2];
    const absDirPath = resolve(process.cwd(), dirPath);

    if (!fs.existsSync(absDirPath)) {
        console.log(`La directory specificata non esiste.`);
        process.exit(1);
    }

    fs.readdirSync(absDirPath).forEach((filename) => {
        if (filename.endsWith(".md")) filename = filename.slice(0, -3);
        if (filename.endsWith(".md~")) return;

        mdFileNames.push(filename);
    });

    // console.log(`\n - La directory contiene ${mdFileNames.length} files .md`);
    console.log(`\n`);

    mdFileNames.forEach((filename) => {
        var fileDate = filename.substring(0, 10);
        var fileName = filename.substring(11);
        mdFileNamesDate.push(fileDate);
        mdFileNamesTitles.push(fileName);
    });

    json2xml(object);
}
// altrimenti legge la lista "mdfilenames.txt" dei files md
else {
    readline
        .createInterface({
            input: fs.createReadStream("./mdfilenames.txt"),
            terminal: false,
        })
        .on("line", function (line) {
            if (line.endsWith(".md")) line = line.slice(0, -3);

            mdFileNames.push(line.toLowerCase());
        })
        .on("close", function () {
            console.log(`\n - Letto "mdFileNames.txt" contenente ${mdFileNames.length} filenames`);
            console.log(`\n`);

            mdFileNames.forEach((filename) => {
                var fileDate = filename.substring(0, 10);
                var fileName = filename.substring(11);
                mdFileNamesDate.push(fileDate);
                mdFileNamesTitles.push(fileName);
            });

            json2xml(object);
        });
}

function editDistancePercent(s, t) {
    if (!s.length) return t.length;
    if (!t.length) return s.length;
    const arr = [];
    for (let i = 0; i <= t.length; i++) {
        arr[i] = [i];
        for (let j = 1; j <= s.length; j++) {
            arr[i][j] = i === 0 ? j : Math.min(arr[i - 1][j] + 1, arr[i][j - 1] + 1, arr[i - 1][j - 1] + (s[j - 1] === t[i - 1] ? 0 : 1));
        }
    }

    var editDistance = arr[t.length][s.length];

    // ritorna la percentuale di uguaglianza fra due stringhe
    return (1 - editDistance / Math.max(t.length, s.length)) * 100;
}

function resultTitleFromPostTitle(title) {
    if (title == "La Prima Colonia - The Montecristo Project: recensione in tre parti - parte zero") return "la-prima-colonia-the-montecristo-project-recensione-in-tre-parti-parte-zero";
    if (title == "La Prima Colonia - The Montecristo Project: recensione in tre parti - parte uno") return "la-prima-colonia-the-montecristo-project-recensione-in-tre-parti-parte-uno";
    if (title == "La Prima Colonia - The Montecristo Project: recensione in tre parti - parte due") return "la-prima-colonia-the-montecristo-project-recensione-in-tre-parti-parte-due";
    if (title == "La Prima Colonia - The Montecristo Project: recensione in tre parti - parte tre") return "la-prima-colonia-the-montecristo-project-recensione-in-tre-parti-parte-tre";
    if (title == "Non si finisce mai di disimparare / 2") return "non-si-finisce-mai-di-disimparare-2";

    var result = title.normalize("NFD").replace(/[\u0300-\u036f]/g, "");
    result = result.replace(" | QuickLoox", "");
    result = result.replace(/:/g, "");
    result = result.replace(/\./g, "");
    result = result.replace(/\//g, "");
    result = result.replace(/\(/g, "");
    result = result.replace(/\)/g, "");
    result = result.replace(/, /g, "-");
    result = result.replace(/\'/g, "-");
    result = result.replace(/\’/g, "-");
    result = result.replace(/ /g, "-");
    result = result.replace(/-+/g, "-");

    return result.toLowerCase();
}

function resultDataFromPostEpoch(epoch) {
    var data = new Date(0); // The 0 there is the key, which sets the date to the epoch
    data.setUTCSeconds(epoch);
    return data.toISOString().substring(0, 10);
}

function resultTitleFromPostKey(key, title) {
    if (key.length == 27) return resultTitleFromPostTitle(title);
    if (key.length == 26) return resultTitleFromPostTitle(title);
    else return key;
}

function resultDataFromPostPath(path) {
    var result = path.split("/").slice(3).join("-");

    if (result.indexOf("#") != -1) result = result.split("#")[0];
    result = result.replace("blog-", "");

    return result;
}

function resultDataFromPostKey(key) {
    if (key.indexOf("blog-") == 0) return key.substring(5, 15);
    if (key.indexOf("posts-") == 0) return key.substring(6, 16);
}

function createCmtFiles(postName, replies) {
    validPosts_count++;

    for (reply in replies) {
        // BEGIN
        var cmt = "<item>\n";

        // POST
        cmt += "\t<parent>" + postName + "</parent>\n";

        // DATA
        var d = new Date(0); // The 0 there is the key, which sets the date to the epoch
        d.setUTCSeconds(replies[reply].date);
        cmt += "\t<w3cdate>" + d.toISOString() + "</w3cdate>\n";

        // COMMENTO
        var body = replies[reply].body;
        body = body.replace(/&quest;/g, "?");

        // Sostituisci tutte le '&' che non fanno parte di una character entity con '&amp;'
        var regex = /&(?!#[0-9]+;|[a-zA-Z]+;)/g;
        body = body.replace(regex, "&amp;");

        cmt += "\t<description>" + body + "</description>\n";

        // IP?
        cmt += "\t<ipaddress></ipaddress>\n";

        // AUTORE
        cmt += "\t<author>" + replies[reply].author.displayname + "</author>\n";

        // EMAIL
        cmt += "\t<email></email>\n";

        // SITO
        cmt += "\t<link></link>\n";

        // END
        cmt += "</item>";

        // FILENAME
        var fileName = postName + "-" + replies[reply].date + "." + hash++ + ".cmt";

        // SCRIVI IL FILE
        fs.writeFileSync("./comments/" + fileName, cmt);

        validCmtFiles_count++;

        process.stdout.write("\r - " + validCmtFiles_count + " files di commento creati");
    }

    // Controlla se ci sono caratteri non alphanumerici (salvo il trattino) in postaName
    // var r = /[^0-9a-zA-Z\-]/g;
    // if (r.test(postName)) console.log(postName);
}

function json2xml(jsonObj) {
    for (postsList in jsonObj.threads) {
        for (post in jsonObj.threads[postsList]) {
            var postKey = jsonObj.threads[postsList][post].seed.key;
            var postEpoch = jsonObj.threads[postsList][post].seed.date;
            var postTitle = jsonObj.threads[postsList][post].seed.title;
            var postPath = jsonObj.threads[postsList][post].seed.path;

            var resultData = "";
            var resultTitle = "";
            var resultPostName = "";

            switch (postsList) {
                case "/comments":
                    resultData = resultDataFromPostKey(postKey);
                    resultTitle = resultTitleFromPostTitle(postTitle);
                    break;

                case "/apple":
                    resultData = resultDataFromPostEpoch(postEpoch);
                    resultTitle = resultTitleFromPostKey(postKey, postTitle);
                    break;
                default:
                    resultData = resultDataFromPostPath(postPath);
                    resultTitle = resultTitleFromPostKey(postKey, postTitle);
                    break;
            }

            var postReplies = jsonObj.threads[postsList][post].replies;

            // Se non ci sono commenti...
            if (postReplies.length == 0) {
                continue;
            }

            resultPostName += resultData + "-" + resultTitle;

            // Se il postName risultante dal json è presente fra gli mdFileNames
            if (mdFileNames.indexOf(resultPostName) != -1) {
                createCmtFiles(resultPostName, postReplies);
            } else {
                // Recupera il postName corretto confrontando risultTitle e crea il file .cmt
                if (mdFileNamesTitles.indexOf(resultTitle) != -1) {
                    var i = mdFileNamesTitles.indexOf(resultTitle);
                    var correctedPostName = mdFileNamesDate[i] + "-" + mdFileNamesTitles[i];

                    // Esistono due file md con stesso titolo "come-da programma"
                    // il primo elaborato ( 2013-10-18-come-da-programma ) nell' if sopra, il secondo ( 2014-04-28-come-da-programma ) elaborato qui.
                    // Adesso correctedPostName risulta erroneamente 2013-10-18, quindi viene sostituito col postname corretto
                    if (resultTitle == "come-da-programma") correctedPostName = "2014-04-28-come-da-programma";

                    createCmtFiles(correctedPostName, postReplies);
                    correctedPosts_count++;
                } else {
                    var highestPercentages = [];
                    var nearestMdFilenames = [];

                    for (let i = 0; i < mdFileNamesTitles.length; i++) {
                        const mdTitle = mdFileNamesTitles[i];
                        const mdDate = mdFileNamesDate[i];

                        const equalityPercentage = editDistancePercent(mdDate + "-" + mdTitle, resultPostName);

                        if (equalityPercentage > 70) {
                            highestPercentages.push(equalityPercentage);
                            nearestMdFilenames.push(mdDate + "-" + mdTitle);
                        }
                    }

                    if (highestPercentages.length > 0) {
                        var i = highestPercentages.indexOf(Math.max.apply(Math, highestPercentages));
                        var nearestMdFilename = nearestMdFilenames[i];

                        if (nearestMdFilename.endsWith("le-sirene-dei-media")) nearestMdFilename = "2023-07-12-le-sirene-dei-media";

                        createCmtFiles(nearestMdFilename, postReplies);

                        correctedPostNamesForTitlePercentageEquality.push(resultPostName + "§" + nearestMdFilename);
                        correctedPostsPercent_count++;
                    } else {
                        if (substitution_postnames.indexOf(resultPostName) != -1) {
                            var i = substitution_postnames.indexOf(resultPostName);
                            var mdFilename = substitution_mdFilenames[i];

                            createCmtFiles(mdFilename, postReplies);

                            subsitutedPostnames.push(resultPostName + "§" + mdFilename);
                            subsitutedPost_count++;
                        } else {
                            invalidPostNames.push(resultPostName);
                            invalidPosts_count++;
                        }
                    }
                }
            }
        }
    }

    console.log("\n\n\nPOST NAMES CORRETTI PER UGUAGLIANZA FILENAMES md > 70%");
    console.log("(viene calcolata la percentuale di uguaglianza fra postnames ricavati dal json e filenames md)\n\n");

    correctedPostNamesForTitlePercentageEquality.forEach((names, i) => {
        var splittedNames = names.split("§");
        console.log("\n " + String(++i).padStart(2, "0") + ". postname invalido : \t\t \x1b[31m" + splittedNames[0] + "\x1b[0m");
        console.log("     preso filename md più prossimo : \t \x1b[36m" + splittedNames[1] + "\x1b[0m ");
    });

    console.log("\n\nIl postname \x1b[31m2023-07-12-le-sirene-dei-media\x1b[0m contiene un carattere speciale invisibile,\nperciò risulta elencato fra i post corretti di cui sopra.");

    console.log("\n\n\nPOST NAMES CORRETTI VIA MAPPA DI SOSTITUZIONE");
    console.log("(la mappa di sostituzione contiene i postnames invalidi del json e i sostituti filenames md)\n\n");

    subsitutedPostnames.forEach((names, i) => {
        var splittedNames = names.split("§");
        console.log("\n " + String(++i).padStart(2, "0") + ". postname invalido : \t\t \x1b[31m" + splittedNames[0] + "\x1b[0m");
        console.log("     sostituito dal filename md : \t \x1b[36m" + splittedNames[1] + "\x1b[0m ");
    });

    console.log("\n\n\nPOST NON VALIDI");
    console.log("(non vengono creati files di commento per i seguenti postnames)\n\n");

    invalidPostNames.forEach((name, i) => {
        console.log(String(++i).padStart(2, "0") + ". \x1b[31m" + name + "\x1b[0m");
    });

    console.log("\n");
    console.log(" - POST CON COMMENTO TOTALI: " + (validPosts_count + invalidPosts_count));
    console.log(" - POST VALIDI : " + validPosts_count);
    console.log(" - POST NON VALIDI : " + invalidPosts_count);

    console.log("\n");
    console.log(" - FILES DI COMMENTO TOTALI: " + totalCmtFiles_count);
    console.log(" - FILES DI COMMENTO CREATI: " + validCmtFiles_count);
    console.log(" - FILES DI COMMENTO SCARTATI: " + (totalCmtFiles_count - validCmtFiles_count));

    console.log("\n");
    console.log(" - POST NAMES CORRETTI PER UGUAGLIANZA TITOLI md: " + correctedPosts_count);
    console.log(" - POST NAMES CORRETTI PER UGUAGLIANZA FILENAMES md > 70%: " + correctedPostsPercent_count);
    console.log(" - POST NAMES CORRETTI VIA MAPPA DI SOSTITUZIONE: " + subsitutedPost_count);

    console.log("\n");
    console.log("   by Paoloo");

    console.log("\n");
}
