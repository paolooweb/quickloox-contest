# Nodejs Comma Converter

Script nodejs per la conversione di commenti dal file di esportazione MUUT `quickloox.json` al sistema Comma.

Converte i commenti dei post registrati nei tre path del file quickloox.json : `YYYY/MM/DD-titolo-del-post`, `/comments`, `/apple` in files xml per essere elaborati da Comma.

## NOTE

Lo script ora effettua un confronto fra i files md della cartella "post" con i postnames ottenuti dal file json `quickloox.json`, per risolvere vari casi di incongruenza di date e titoli di post nei filenames.


## HOW TO

1. Creare la cartella "comments" all'interno della cartella del progetto
2. Assicurarsi che la cartella "comments", se già esistente sia vuota.
3. Assicurarsi che sia presente il file "quickloox.json" all'interno della cartella del progetto
4. Eseguire `node index.js path/to/folder/post`

Esempio:

```
node index.js /users/paoloo/desktop/blog-v4/content/post
```

N.B. Racchiudere il path fra virgolette se ci sono spazi al suo interno.

By Paoloo.

Enjoy.