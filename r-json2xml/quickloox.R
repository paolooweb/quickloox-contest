#---------------------------------------
# Clear the RStudio environment
#---------------------------------------
# clear workspace, console and plots if script is run in Rstudio
if (Sys.getenv("RSTUDIO") == "1") {
    rm(list = ls())     # clear workspace
    cat("\014")         # clear console
    graphics.off()      # clear plots
}
#---------------------------------------


#---------------------------------------
# Required libraries
#---------------------------------------
library(tidyverse)      # mandatory
library(jsonlite)       # manage json files
library(xml2)           # manage XML files
library(textreuse)      # for generatining hashes
library(stringdist)     # for similar hashes
# library(digest)
#---------------------------------------


#---------------------------------------
# Functions
#---------------------------------------
dir_exists <- function(dir) {
    if (dir.exists(dir)) {
        dir <- normalizePath(dir)
        msg <- paste("OK. Directory", dir, "exists", sep = " ")
        cat(msg, fill = TRUE)
    } else {
        msg <- paste("ERROR! Directory", dir, "does not exist", sep = " ")
        cat(msg, fill = TRUE)
        stop()
    }
}
#---------------------------------------

create.dir <- function(dir) {
    if (!dir.exists(dir)) {
        dir.create(dir)
    } else {
        msg <- paste("Warning. Directory", dir, "already exists", sep = " ")
        cat(msg, fill = TRUE)
    }
}
#---------------------------------------


#---------------------------------------
# Constants
#---------------------------------------
# maximum acceptable Jaro-Winkler distance (found by trial and error)
THRESHOLD <- 0.16

# switch(es) for debugging code
REMOTE  <- FALSE        # TRUE:  download json from remote rempository, 
                        # FALSE: use local copy
VERBOSE <- TRUE         # TRUE:  print path and comment file names, 
                        # FALSE: print minimal information about process
DEBUG   <- TRUE
#---------------------------------------


#---------------------------------------
# Setup
#---------------------------------------
# OS-specific constants
SEPARATOR <- .Platform$file.sep

# define the current directory, i.e., the directory that contains the source files
SRC  <- getwd()

# define main directory contining all data files
DATA <-  file.path(SRC, "data") %>% normalizePath()

# define directories for output files (e.g., reports and plots) 
# (they can be created later if they do not already exist)
OUTPUT <-  file.path(SRC, "output") %>% normalizePath()
COMMENTS <- file.path(OUTPUT, "comments") %>% normalizePath()

# check that output file directory exists 
# and eventually create them
create.dir(OUTPUT)
create.dir(COMMENTS)

if (DEBUG) {
    DBG <- file.path(SRC, "debug") %>% normalizePath()
    create.dir(DBG)
}

# set the timezone
Sys.setenv(TZ = "CET")
TZ <- Sys.getenv("TZ")
#---------------------------------------


#---------------------------------------
# read the CSV file containing the whole post list
#---------------------------------------

# NOTE: the code below is commented out because this approach does not work due
# to an (admittedly stunning) difference between the records extracted from the 
# html index file and the corresponding records found in the JSON file exported
# from Muut.

# # read the csv file containing the list of published posts
# filename  <- "unique_posts.csv"
# input_file <- file.path(DATA, filename) %>% normalizePath()
# posts_df <- read_delim(input_file, delim = "\t ",
#                        col_names = FALSE,
#                        col_types = list(
#                            X1 = col_character(), #col_date(format = "%Y/%m/%d"),
#                            X2 = col_character(),
#                            X3 = col_character(),
#                            X4 = col_character()
#                        ),
#                      trim_ws = TRUE, skip = 0)
#
# names(posts_df) <- c("DATE", "SLUG", "TITLE", "PATH")
#
# if (DEBUG) { head(posts_df) }


# NOTE: alternative approach using another CSV file
# read the csv file containing the list of published posts
filename  <- "official_posts.csv"
input_file <- file.path(DATA, filename) %>% normalizePath()
posts_df <- read_csv(input_file, trim_ws = TRUE, skip = 0, 
                     col_names = c("TITLE"),
                     col_types = c(col_character())
                     )

# remove spurious chars (however this should be fixed server-side: DONE)
# posts_df$TITLE <- posts_df$TITLE %>% str_replace("^(\u0004)", "")

# remove file extension
posts_df$TITLE <- posts_df$TITLE %>% str_replace(".md", "")


if (DEBUG) { head(posts_df) }

# is this necessary?
posts_df <- posts_df %>% separate_wider_regex(TITLE, c(DATE = "[0-9]{4}-[0-9]{2}-[0-9]{2}", "[-–]", PATH = ".*"), too_few = "error", cols_remove = FALSE)
#---------------------------------------



#---------------------------------------
# JSON file with all post comments
#---------------------------------------

filename  <- "quickloox.json"
input_file <- file.path(DATA, filename) %>% normalizePath()

# if TRUE download file from URL and save it as `input_file`
# (otherwise use the json file already present in the DATA directory)
if (REMOTE) {
    # most up-to-date version of the json file is always available from the GitLab project repo
    url  <- "https://gitlab.com/emeralit/quickloox-contest/-/raw/main/quickloox.json?ref_type=heads&inline=false"
    header <- setNames("token glpat-s-WFoGC1n2xyNAKp9237", "Authorization")

    download.file(url, destfile = input_file, mode = "wb", headers = header)
} else {
    # check if local input_file exists, otherwise stop script
    if (!file.exists(input_file)) {
        stop("No quickloox.json file available.") 
    }
}

# read the json file present in the DATA directory
df_full <- jsonlite::fromJSON(txt = input_file) %>% as_tibble()


if (DEBUG) {
    #--- while debugging, throw away what is not needed and keep only the useful stuff ---
    # df <- df_full$threads %>% .[3:3]
    # df <- df_full$threads %>% .[6:6]
    # df <- df_full$threads %>% .[1:6]
    # df <- df_full$threads %>% .[1:100]
    # df <- df_full$threads %>% .[53:53]
    # df <- df_full$threads %>% .[44:44]
    # df <- df_full$threads %>% .[27:27]
    # df <- df_full$threads %>% .[1980:1980]
    df <- df_full$threads
} else {
    df <- df_full$threads
}


# this tibble contains the data related to the processing of each post/comment
process_df <- tibble()

# and this tibble is used only for debugging
if (DEBUG) {
    debug_df <- tibble()
}


# cycle over all the main paths
print("Running...")
start_time <- Sys.time()

for (name in names(df)) {
    if (VERBOSE) {
        # write the name of the path
        paste("path:", name) %>% print()
    }

    # dataframe containing all the posts within a given path
    seed <- df %>% .[[name]] %>% .[["seed"]] %>% as_tibble()
    
    seed$likes <- NULL
    seed <- rename(seed, "POST_KEY" = key, "POST_TITLE" = title, "POST_BODY" = body, "POST_EPOCH" = date, "POST_PATH" = path, "POST_AUTHOR" = author)

    nseed <- nrow(seed)
    if (DEBUG) { paste(  "nseed:", nseed) %>% print() }

    df_merged <- tibble()
    for (i in 1:nseed) {
        
        # dataframe containing the replies (i.e., comments) to each post
        replies <- df %>% .[[name]] %>% .[["replies"]] %>% .[[i]] %>% 
            # as.data.frame() %>% 
            as_tibble()
        
        replies$likes <- NULL
        replies <- replies %>%
            unnest(starts_with("author"), names_sep = "_", names_repair = "universal")

        # for some unknown reason, dplyr::rename() does not work here,
        # therefore all column names are simply converted to lowercase
        replies <- rename_with(replies, tolower) 
        # replies <- rename(replies, "COMM_KEY" = key, "COMM_BODY" = body, "COMM_DATE" = date, "COMM_PATH" = path, "COMM_AUTHOR_PATH" = author_path, "COMM_AUTHOR" = author_displayname)

        nreplies <- nrow(replies)
        if (DEBUG) { paste(  "nreplies:", nreplies) %>% print() }
        
        if (nreplies == 0) {
            next
        }

        # for each post associate the related dataframe with the replies;
        # at the end of the for cycle, the df_merged dataframe contains a tidy list of 
        # all the posts and the associated replies (i.e., comments) for the path under processing
        # 
        # NOTE: these complicated for cycles are necessary because the 
        #       two structures coming out from the json file for each path do not contain an
        #       univocal key associating each comment to the post that originated it
        merged <- left_join(seed[i,], replies, join_by(POST_PATH == path), keep = TRUE)
        merged$nreplies <- nreplies
        df_merged <- bind_rows(df_merged, merged)
    }


    # this cycle scans the df_merged dataframe, and uses the gathered information
    # to create an XML file for each comment
    for (j in 1:nrow(df_merged)) {
        if (DEBUG) {
            print("");
            paste("j:", j) %>% print()
        }
        
        df_line <- df_merged[j,]

        if (nrow(df_merged) == 0) {
            next
        }

        # find a date pattern in either POST_PATH or POST_KEY and get the associated POST_KEY...
        if (DEBUG) { 
            paste("> POST_PATH: ", df_line$POST_PATH) %>% print()
            paste("> POST_KEY:  ", df_line$POST_KEY)  %>% print()
            paste("> length POST_KEY:  ", nchar(df_line$POST_KEY))  %>% print()
        }
        
        # ... first check: POST_DATE is extracted from POST_PATH
        df_line$POST_DATE <- df_line$POST_PATH %>%
            str_match("[0-9]{4}[/-][0-9]{2}[/-][0-9]{2}$") %>%
            str_replace_all("/", "-")
            
            if (nchar(df_line$POST_KEY) < 26) {
                df_line$POST_KEY_FIX <- df_line$POST_KEY
            } else {
                df_line$POST_KEY_FIX <- df_line$POST_TITLE
            }

        if (DEBUG) { paste("> matchPATH POST_DATE:  ", df_line$POST_DATE) %>% print() }

        # ...second check: POST_DATE is extracted from POST_KEY
        if (df_line$POST_DATE %>% is.na()) {
            df_line$POST_DATE <- df_line$POST_KEY %>%
                str_match("[0-9]{1,4}[/-][0-9]{2}[/-][0-9]{2}") %>% 
                str_replace_all("/", "-")
                
            df_line$POST_KEY_FIX  <- df_line$POST_TITLE
        }

        if (DEBUG) { 
            paste("> matchKEY POST_DATE:   ", df_line$POST_DATE) %>% print()
            if (df_line$POST_DATE %>% is.na()) {
                cat(paste0("\033[0;", 31, "m", "ERROR" ,"\033[0m","\n"))
            }
        }

        # ... third check: if everything else fails, get POST_DATE from EPOCH 
        epoch_used <- FALSE
        if (df_line$POST_DATE %>% is.na()) {
            epoch_used <- TRUE
            df_line$POST_DATE <- df_line$POST_EPOCH %>% strftime("%Y-%m-%d", tz = "UTC")
            df_line$POST_KEY_FIX <- df_line$POST_KEY
        }

        # cleanup of POST_KEY_FIX
        df_line$POST_KEY_FIX  <- df_line$POST_KEY_FIX %>%
            tolower() %>%
            stringi::stri_trans_general("Latin-ASCII") %>%
            str_replace_all("quickloox", "") %>%
            str_replace_all("[ ]", "-") %>%
            str_replace_all("[']", "-") %>%
            str_replace_all("[^a-zA-Z0-9,/&fixed([-_()])]", "") %>%
            str_replace_all("[-]{2,}", "-") %>%
            str_replace_all("-$", "")
            

        if (DEBUG) { 
            paste("> matchEPOCH POST_DATE: ", df_line$POST_DATE) %>% print()
            paste("> final POST_DATE:      ", df_line$POST_DATE) %>% print()
            paste("> final POST_KEY_FIX:", df_line$POST_KEY_FIX) %>% print()
        }


        # compute the Jaro-Winkler distance between the current post title and the 
        # official list of published post
        posts_df$DIST <- stringdistmatrix(
            posts_df$TITLE, 
            paste(df_line$POST_DATE, df_line$POST_KEY_FIX, sep = "-"), 
            method = "jw")
            
        df_line$POST_FULLTITLE <- posts_df %>% filter(DIST == min(DIST)) %>% select(TITLE)
            paste("> POST_FULLTITLE:", df_line$POST_FULLTITLE) %>% print()
        
        # don't process lines whose string distance is larger than a given threshold
        distance <- posts_df %>% filter(DIST == min(DIST)) %>% select(DIST)
        if (distance >= THRESHOLD) {next}
        
        if (DEBUG) {
            many_matches <- FALSE
            df_line_debug <- posts_df %>% filter(DIST == min(DIST))

            if (nrow(df_line_debug) > 1 ) {
                many_matches <- TRUE
                cat(paste0("\033[0;", 31, "m", "SLICE-ERR" ,"\033[0m","\n"))
            }

            df_line_debug$DIST <- df_line_debug$DIST %>% as.numeric() %>% format(nsmall = 3)

            debug_df <- bind_rows(debug_df, 
                                  tibble(df_line$POST_DATE, df_line$POST_KEY_FIX, epoch_used,
                                         df_line_debug$DATE, df_line_debug$PATH, 
                                         df_line_debug$TITLE, df_line_debug$DIST, many_matches))
        }


        parent <- paste0(df_line$POST_FULLTITLE)
        
        # W3C/ISO 8601 date and time of the comment using UTC timezone, 
        # and adds the special UTC designator "Z" at the end of the string
        w3cdate <- df_line$date %>% strftime("%Y-%m-%dT%H:%M:%OS3", tz = "UTC") %>%
            str_replace("(.*)$", "\\1Z")

        description <- df_line$body
        author <- df_line$author_displayname
        email <- ""
        ipaddress <- ""
        link <- ""

        cmt <- xml_new_document(version = "1.0", encoding = "UTF-8") %>%
            xml_add_child("item", NA) %>%
            xml_add_child("parent", parent, .where = "after") %>%
            xml_add_sibling("w3cdate", w3cdate, .where = "after") %>%
            xml_add_sibling("description", description, .where = "after") %>%
            xml_add_sibling("ipaddress", NA, .where = "after") %>%
            xml_add_sibling("author", author, .where = "after") %>%
            xml_add_sibling("email", NA, .where = "after") %>%
            xml_add_sibling("link", NA, .where = "after") %>%
            xml_root()

        # cleanup of the cmt XML document
        cmt <- cmt %>%
            as.character() %>%
            str_remove_all("NA") %>%
            str_remove("<.*>\\n") %>%
            str_remove("\\n$")
            
        cmt <- cmt %>% 
            str_replace("<item>", "<item>\n") %>%
            # match as few characters as possible
            str_replace_all("(</.*?>)", "\\1\n") %>% 
            # for each line, match everything within "<" and ">", 
            # except tags containing the term "item" 
            # (uses negative look-behind: (?<!subexp) )
            str_replace_all("(<.*(?<!item)>)", "\t\\1") %>%
            # fix spurious tabs in closing tags (actually only one)
            str_replace("\t</", "</>") %>%
            # remove last newline
            str_replace("\n$", "")
        

        # variables used to create a unique file name
        fnpullpath <- df_line$POST_FULLTITLE
        epoch <- df_line$date
        # hash <- digest2int(description) %>% abs()
        hash <- hash_string(description) %>% abs()

        outfile <- paste0(fnpullpath, "-", epoch, ".", hash, ".cmt")
        output_file <- file.path(COMMENTS, outfile)

        if (VERBOSE) { paste("outfile:", outfile) %>% print() }

        write(cmt, file = output_file)

        process_df <- bind_rows(process_df,
                                tibble(nseed, nreplies = df_line$nreplies, count = j, parent, post_key = df_line$POST_KEY, comment_key = df_line$key, w3cdate, comment_author = df_line$author_displayname)
                                )
        }

        if (DEBUG) { 
            paste("=========================================") %>% print() 
        }
}

sprintf("End run")
end_time <- Sys.time()

sprintf("Time elapsed: %.3f s", end_time - start_time)
sprintf("\n")


if (DEBUG) {
    write_csv(debug_df,   file.path(DBG, "debug_data.csv"))
    write_csv(process_df, file.path(DBG, "process_data.csv"))
}


# summary data about the processing (needs to be improved...)
summary_df <- process_df %>% group_by(parent, nreplies) %>% tally() %>% select(!n)
summary_df <- summary_df %>% rename(Posts = parent, Comments = nreplies)

write_csv(summary_df, file.path(OUTPUT, "summary_data.csv"))


paste("total number of posts (with comments):", summary_df %>% nrow())
paste("total number of comments:", summary_df$Comments %>% sum())


system("say Just finished!")

