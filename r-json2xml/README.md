# README



### Conversione da JSON a XML

Lo script `quickloox.R` converte un file JSON esportato da Muut in singoli file di commento nel formato XML accettato da Comma, che vengono salvati nella directory `comments` creata appositamente all'interno della directory di `output` dello script.

Lo script permette sia di scaricare la copia più recente del file JSON dal repository del progetto oppure di usare una copia presente in locale e contenuta nella directory `data`.

Lo script `quickloox.R` può essere eseguito dalla linea di comando tramite:

```{bash}
Rscript quickloox.R
``` 

oppure aprendo il progetto omonimo in RStudio ed eseguendolo tramite l'IDE.

A causa dell'elevatissimo numero di file prodotti dall'elaborazione (~13.000), è consigliabile comprimere sempre la directory `output` con

```
tar -czf output.tar.gz output/
```

prima di trasferirla su un altro computer o sulla repository del progetto.



### Versione originale dello script

La versione originale dello script estraeva i dati relativi alla data e al titolo del post (necessari per ricreare il nome del file del post originale e associare ciascun commento al file corretto) utilizzando un metodo bastato sulla ricerca dei dati richiesti all'interno dei campi POST_PATH, POST_KEY e POST_TITLE tramite espressioni regolari. Nei casi in cui non ci fosse modo di determinare la data, veniva usato il campo POST_EPOCH, purtroppo molto impreciso.

Questo metodo era molto veloce, su un Apple Silicon M1 lo script impiegava 36 secondi dalla linea di comando e di 52 secondi da RStudio. Attivando l'opzione VERBOSE, che stampava su schermo il percorso (path) in corso di analisi e i nomi dei file di commento prodotti dall'elaborazione, il tempo di esecuzione aumentava al più di pochi secondi (2-3 secondo) eseguendo lo script dalla linea di comando, mentre rimaneva praticamente invariato con RStudio.

Alla data  del 10-04-2024 si ottenevano:

```
[1] "total number of posts (with comments): 2436"
[1] "total number of comments: 13088"
```



### Versione rivista dello script

Lo script originale funzionava bene per i casi normali, ma per forza di cose non riusciva a gestire i casi particolari in cui il titolo del post riportato nel file `json` differiva dal nome del file del post.

Per gestire questi casi è stato utilizzato un algoritmo basato sulla distanza di Jaro-Winkler che determina il titolo del post come descritto nella sezione precedente e poi ricerca, nella lista "ufficiale" dei post, il nome del file che più si avvicina al titolo in questione.

Con questo algoritmo è stato possibile associare correttamente tutti i ~13.000 commenti al post relativo, tranne pochi casi _patologici_ relativi a:

- 17 post presenti nel file `json` ma assenti dalla lista "ufficiale";
- 8 post con nomi di file totalmente diversi dai relativi titoli, che dovrebbero essere gestiti a mano tramite ad esempio una lista di sostituzione.

Implementare la lista di sostituzione sarebbe stato facile, ma dato il numero estremamente ridotto di casi, si è preferito introdurre un semplice _threshold_ sulla distanza di Jaro-Winkler, scartando il post ogni volta che questo valore veniva superato (quindi per valori troppo elevati della distanza).

Con questo metodo vengono ritrovati: 

```
[1] "total number of posts (with comments): 2410"
[1] "total number of comments: 13001"
```

perdendo quindi solo 26 post sui 2436 totali (l'1.1%) e 83 commenti su 13.088 commenti totali (lo 0.7 %).



### Dettagli sull'algoritmo

La **prima parte** è facile: ad ogni post tratto dal file `json` vengono associati i relativi commenti, costruendo una matrice ordinata che in R si crea con pochissime righe di codice e con una operazione di `join` analoga a quella che si fa con i database. Questa matrice viene analizzata riga per riga, costruendo il titolo del post in genere dal campo originale `seed:key`, ma usando il `seed:title` se il `seed:key` è troppo lungo (27 caratteri). Purtroppo non basta perché bisogna gestire dei casi particolari, dovuti al succedersi delle diverse ere del blog, cosa che ha rallentato parecchio il lavoro. Una cosa simile si fa con la data, che si trova in posti diversi e scritta in due formati diversi, ma che in tanti casi non si trova proprio e bisogna ricostruirla dall'EPOCH del post  (il numero di secondi trascorsi dal 1-1-1970), che però è un numero inaffidabile a causa delle inevitabili modifiche sui post avvenute dopo la data di pubblicazione,  che se ho capito bene modificano l'EPOCH di Muut ma non la data scritta una volta per tutte nel titolo e nel percorso del post.

Credo che un metodo analogo sia stato usato anche da @Paoolo, non ho guardato volutamente il codice JavaScript per cercare di essere indipendente, ma immagino che la logica sia più o meno quella.

Ma tutto ciò non basta, perché ci sono moltissimi **casi particolari** che rimangono fuori da questo algoritmo, nel senso che i titoli dei post ricavati dai commenti non si _incastravano_ con quelli dei post originali, creando problemi a Muut. Questo a meno di non correggere a mano i titoli incriminati, oppure di non gestire uno ad uno nel codice i casi particolari. 

Cosa che si può fare, non c'è dubbio, ma ci può essere **qualcosa di meglio**?

Chiaramente sì, e l'idea di fondo era quella di confrontare il nome del post determinato con il metodo appena descritto con tutti quelli delle lista ufficiale, trovando quello che gli si avvicinava di più. In pratica determinare quanto la stringa di testo del nome del post tratto da Muut somigliasse a ciascuno dei post post della "lista ufficiale". 

Il problema di **verificare quanto due stringhe di testo si somigliano** è molto comune, un caso tipico è quello di confrontare due sequenze di DNA (che una volta decodificate per il computer sono solo delle stringhe lunghissime). Altro caso tipico è verificare se un testo è stato scopiazzato da un altro. Io stesso anni fa avevo provato ad usare questi metodi per confrontare il livello di somiglianza di file dati diversi. 
Fra gli algoritmi che conosco, quello che preferisco è [`simhash`](https://matpalm.com/resemblance/simhash/), che in pratica crea una _impronta digitale_ di ciascuna stringa da confrontare, ovvero un `hash`. Ma a differenza degli `hash` classici, dove si vuole che stringhe simili abbiano hash profondamente diversi, qui è il contrario, quanto più gli hash sono simili (matematicamente parlando) tanto più le stringhe sono simili.

Purtroppo `simhash` è stato implementato in Python ma non in R, e qui non aveva senso implementarlo da zero. Ho trovato però il package [`stringdist`](https://cran.r-project.org/web/packages/stringdist/index.html) che ha una [buona documentazione](https://cran.r-project.org/web/packages/stringdist/vignettes/RJournal_6_111-122-2014.pdf), e che soprattutto è fatto bene nel senso che accetta tranquillamente le strutture dati tipiche di R (dovrebbe essere ovvio ma spesso non è così).

Stringdist permette di calcolare la distanza fra due stringhe (quindi il grado di somiglianza) usando vari algoritmi, fra cui la distanza di Levenshtein o quella di Jaro-Winkler. Sapevo a priori che la distanza di Levenshtein non avrebbe funzionato bene, perché da come risultato un numero intero, e quindi è facile che il confronto generasse più possibilità (cioè più post con nome simile alla stringa data), un evento che avrebbe introdotto ulteriori complicazioni (dalle prove fatte è stato così, anche se tutto sommato meno di quanto mi aspettassi).

La distanza di Jaro-Winkler è un metodo più elaborato, ed ero abbastanza sicuro che avrebbe funzionato bene. Ed è quello che è successo, infatti con questo algoritmo è possibile trovare facilmente il titolo giusto del post per tutti i 13088 commenti, ad eccezione di 84 casi disperati (sarebbero 81 ma per motivi che saranno chiari fra poco faccio buon peso), che sono gli stessi individuati da @Paoloo.



### Analisi dei risultati

Come faccio a saperlo, mi sono spupazzato uno ad uno i 13000 e passa commenti? Nemmeno per idea, ed ed questo che mi piace di usare la matematica. Quello che ho fatto è stato, mentre l'algoritmo era in funzione, di popolare una nuova matrice dati di `debug` con titolo e data del post tratto dal file `json` di Mutt assieme al relativo titolo e data del post più "vicino", determinato tramite la distanza di Jaro-Winkler e relativo valore della distanza (_score_). Due flag aggiuntivi mi servivano per sapere se la data determinata dal file `json` era tratta dall'EPOCH o no (all fine non è servito a molto), e soprattutto se Jaro-Wrinkler trovava più di un post "vicino" con lo stesso score o no (ed è venuto fuori che, a differenza di Levenshtein, l'algoritmo basato su Jaro-Wrinker trovava sempre un unico _best match_). A quel punto ho salvato la matrice di debug come file `csv` e l'ho importata in Excel (ho provato anche Number in verità, ma aveva problemi con la colonna dello score e ho lasciato perdere). La cosa più difficile è stata convincere Excel a non toccare le date scritte nel file e a considerare lo score un numero e non una stringa, Fatto questo mi sono trovato con  foglio bello grosso composto da 8 colonne e 130898 righe, che ho ordinato in base al valore dello score, dal più alto al più basso, che è stato relativamente facile analizzare.

La stragrandissima parte del foglio ha score pari a zero, che significa che l'accordo fra il titolo tratto dal `json` era identico a quello del post originale, una cosa che **non** si verifica solo in 2172 commenti (su 13088, cioè lo 0.17%). 
Fra questi 2172 commenti, quasi tutti hanno score estremamente bassi, da 0.01 a 0.1, perché le differenze fra il titolo "calcolato" dal `json` e quello "vero" sono minime, magari il trattino lungo al posto di quello corto o l'apostrofo si o no di cui parlavo ieri. 

Rimangono circa 130 commenti da controllare, quelli sì, a mano. E qui è stato prezioso il lavoro di analisi minuziosa di @Paoloo! 
Perché **tutti** (con una eccezione, vedi dopo) i commenti con lo score più alto, da 0.27 a 0.15 sono quelli compresi nella sua lista di ieri di 17 POST NON VALIDI (che nella lista ufficiale non esistono proprio) e di 8 POST CORRETTI VIA MAPPA DI SOSTITUZIONE (cioè a mano), come si può vedere dall'aimmagine qui sotto, dove il viola chiaro/scuro indica i commenti problematici e il bianco quelli corretti.

![](./images/debug_data-all_jw-top_scores.png)

In realtà c'è una eccezione, quella indicata nel gruppo di tre righe verdi dello screenshot, che corrisponde al post "2020-08-11-l‚Äôinnesto-della-coerenza", che Jaro-Winkler trova correttamente anche con i suoi caratteri spuri, perché nel post original @lux ha usato un apostrofo "tipografico" (o come si chiama) al posto di  quello normale che troviamo sulla tastiera. Francamente non so dire cosa potrà succedere in questo caso specifico quando si proverà ad associare i commenti ai post, ma visto che @Paoloo ha messo questo post nella sua lista di sostituzione, immagino che la cosa più semplice da fare sia sostituire nella sua lista l'apostrofo tipografico (`’`) a quello normale in quel post.

Segnalo infine con piacere che Jaro-Winkler trova il post originale "2024-02-12-conservazione-o-rivoluzione", anche se nel `json` il titolo ha le parole invertite "2024-02-12-rivoluzione-o-conservazione", e con uno score molto basso (0.1) a dimostrazioen della bontà dell'algoritmo usato.

E che succede con i ~50 commenti intermedi, quelli con score minore di 0.15 ma maggiore di 0.1? Quelli sono tutti casi molto particolari, nei quali il titolo del `json` era corrotto rispetto a quello vero, come ad esempio "2021-07-26-dallaltra-parte-7w9h" rispetto al titolo vero "2021-07-26-dall-altra-parte", oppure "2021-08-10-non-ce-niente-da-vedere-5q2" rispetto a "2021-08-10-non-c-e-niente-da-vedere". Oppure casi in cui nel titolo dello `json` manca di qualche trattino e/o lettera che nel post originale invece c'è ("2021-07-04-labitudine-non-fa-il-monac" confrontato a "2021-07-04-l-abitudine-non-fa-il-monaco").

Oppure 3 titoli nei quali il post originale ha dei caratteri Unicode che nei titolo dal `json` non ci sono, come ad esempio il titolo `json` "2023-09-09-la-visione-delle-app-piu-da-vicino" che in origine è "2023-09-09‚Äìla-visione-delle-app-piu-da-vicino".[^1]

E i post che nel file `json` hanno data "1-01-01" e che corrispondono ai post (veri)?
- 2022-04-24-codice-di-comportamento.md
- 2022-05-26-e-cambiato-il-metronomo.md
- 2022-11-11-come-non-resistere-alla-tentazione-di-nuovo.md
- 2022-11-27-a-piena-pagina.md

Anche quelli ora vengono ritrovati senza problemi da quelli originali con la data giusta.


[^1]: Questi problemi con Unicode forse potrebbero/dovrebbero essere forse risolti al momento dell'importazione dei file. Bisogna vedere come. Forse i problemi sono relativi a quanto riportato a pag. 114 di [van der Loo, R Journal 2014](https://cran.r-project.org/web/packages/stringdist/vignettes/RJournal_6_111-122-2014.pdf).

Infine, l'unica correzione che vorrei fare alla lista di POST NON VALIDI di @Paoloo è relativa al post #16, "2019-10-05-efa3bf", che in realtà esiste (https://macintelligence.org/post/2019-10-05-/), perché "efa3bf " corrisponde alla mela in Unicode. 
Lo so perché Jaro-Winkler lo trova, con titolo "2019-10-05-Ô£ø", vai a capire come funzionano queste transcodifiche Unicode.



### Appendice: estrazione di stringhe da un file HTML

Nel corso del lavoro è stato sviluppato un semplice script in AWK, `get_post_names.awk`, con il quale è possibile determinare il numero di post unici presenti sul sito https://macintelligence.org. Per vari motivi, alla fine lo script non è stato usato, però potrebbe sempre essere utile per altri progetti.

Bisogna prima di tutto scaricare una copia aggiornata dell'elenco dei post

```
wget -O index.html https://macintelligence.org/all/
```

Il file `index.html` ottento viene _dato in pasto_ allo script AWK 

```
./get_post_names.awk index.html > all_posts.csv
```

ottenendo un file CSV con l'elenco completo dei post, nel quale il titolo di ogni post può essere ripetuto più volte. Per ottenere l'eleco dei post unici:

```
cat all_posts.csv | sort -k 1 | uniq >> unique_posts.csv
```

che restituisce un elenco ordinato dei singoli post pubblicati con percorso completo, data e titolo di ciascuno. 
Purtroppo i titoli dei post ottenuti in questo modo differiscono moltissimo dai titoli esportati da Muut, per cui è stato impossibile usare questi dati per semplificare l'analisi eseguita dallo script `quickloox.R` descritto sopra.

In ogni caso, chi fosse curioso può contare il numero totale di post (ripetuti) elencati in `index.html` con il comando

```
cat all_posts.csv | wc -l
```

mentre, eseguendo lo stesso comando sul file `unique_posts.csv`

```
cat unique_posts.csv | wc -l
```

si ottiene il numero di post unici.
