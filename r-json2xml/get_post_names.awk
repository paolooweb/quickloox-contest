#!/opt/homebrew/bin/gawk -f

# USAGE: 
#   wget -O index.html https://macintelligence.org/all/
#   ./get_post_names.awk index.html > all_posts.csv
#   cat all_posts.csv | sort -k 1 | uniq >> unique_posts.csv
# 
#   cat all_posts.csv    | wc -l        # counts the number of posts listed in the html page
#   cat unique_posts.csv | wc -l        # counts the number of unique posts


BEGIN {
    # FS = ""
    # OFS = ","

    start = "<article role=\"article\">"
    stop = "</article>"
}

# Locate sections/subsections to be counted
$0 ~ start {
    count = 0;
    
    do {
        getline;

        # find lines containing section headers
        if ($0 ~ /<li><a href=/) {
            count = count + 1

            # remove the trailing spaces
            $0 = gensub(/^[ ]+/, "", "g", $0)

            # extract the path
            path = gensub(/<li><a href="(.*)\/">.*$/, "\\1", "g", $0)

            slug = gensub(/.*[0-9]{4}\-[0-9]{2}\-[0-9]{2}-(.*)/, "\\1", "g", path)
            
            # extract the date from the path
            s = match(path, /[0-9]{4}\-[0-9]{2}\-[0-9]{2}/)
            date = substr(path, s, RLENGTH)
            
            # extract the title
            title = gensub(/<li><a href=".*">(.*)<\/a><\/li>/, "\"\\1\"", "g", $0)
            
            # final touches: escape commas in title
            # title = gensub(",", "\,", "g", title)

            # final touches: replace the html symbols with their ascii equivalent
            # title = gensub(/&#39;/, "'", "g", title)
            # title = gensub(/%C3%A0/, "à", "g", title)
            # title = gensub(/%C3%A8/, "è", "g", title)
            
            # print the whole line
            printf("%s\t %s\t %s\t %s\n", date, slug, title, path)
        }
    } while ($0 )
}

END {
    # print count
}
