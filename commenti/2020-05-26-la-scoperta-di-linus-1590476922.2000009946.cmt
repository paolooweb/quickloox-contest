<item>
	<parent>2020-05-26-la-scoperta-di-linus</parent>
	<w3cdate>2020-05-26T07:08:42.000Z</w3cdate>
	<description>Perfettamente d'accordo sul fatto che Threadripper sia una ottima CPU, che sta dando filo da torcere alle CPU Core iX di Intel. Sembra di essere tornati all'inizio del secolo, quando i processori Athlon erano riusciti a scalfire pesantemente il predominio Intel, generando una fortissima concorrenza fra le due aziende e benefici innegabili per l'utente finale.

Quello che non capisco però è quale sia la relazione con i processori ARM. Le CPU Intel e AMD condividono la stessa microarchitettura x86 per cui, anche se non sono interscambiabili fisicamente sulla stessa scheda madre, tutti i sistemi operativi e le applicazioni che girano su una CPU possono farlo senza modifiche anche sull'altra. 

Le CPU ARM invece sono tutta un'altra bestia, ad esempio seguono la _filosofia_ RISC (Reduced Instruction Set)  e non quella CISC (Complex Instruction Set) su cui si basano i processori Intel che usiamo tutti i giorni nei nostri computer. Il problema sta tutto qui: se passare alle CPU ARM può essere tecnicamente fattibile e perfino conveniente sia dal punto di vista della potenza di c alcolo che del consumo energetico, crea grossi problemi a livello di compatibilità con il software esistente. 

Problemi che abbiamo già vissuto circa 15 anni fa, al momento della transizione dalle CPU PowerPC G4/G5 montate sui Mac di allora allo standard Intel. A suo tempo però Apple è stata letteralmente costretta ad effettuare quella transizione, visto che la tecnologia PPC era stagnante e non c'erano margini di miglioramento, e comunque questa andava verso lo standard di mercato. Senza dimenticare che i Mac erano molto meno diffusi di oggi.

Nonostante ciò, però, abbiamo avuto per anni Rosetta, un emulatore in grado di far girare le applicazioni PPC sui Mac con processore Intel, utile per permettere di continuare ad usare il software non ancora _tradotto_ in codice Intel.
In più, per tutti gli anni della transizione, gli sviluppatori sono stati obbligati a compilare le loro applicazioni in modo che fossero compatibili sia per i processori PPC che per quelli Intel, arrivando ad avere ben 4 programmi diversi nella stessa applicazione (due specifici per le CPU PPC a 32 e a 64 bit e due per le CPU Intel a 32/64 bit). L'ho fatto anch'io e anche se gli strumenti per fare tutto ciò messi a disposizione da Apple erano eccellenti, nondimeno era un grosso aggravio di lavoro.

Francamente, anche se sono ormai convinto che la transizione avverrà abbastanza presto e che Apple metterà in campo gli strumenti adatti per gestirla in modo adeguato, mi spaventa un po' il dover ricominciare a combattere con queste incompatibilità, con gli emulatori, e tutto il resto.</description>
	<ipaddress></ipaddress>
	<author>briand06</author>
	<email></email>
	<link></link>
</item>