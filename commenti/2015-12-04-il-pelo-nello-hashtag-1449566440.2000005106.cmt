<item>
	<parent>2015-12-04-il-pelo-nello-hashtag</parent>
	<w3cdate>2015-12-08T09:20:40.000Z</w3cdate>
	<description>Frix io penso che la teoria, l'accademia, è sempre bene tenerla a mente poi ci scontriamo con la realtà di tutti i giorni in cui dobbiamo deviare dalla purezza teorica per ottenere risultati più pratici.

Questo è un discorso generale che può essere fatto in diversi campi dello scibile umano.

Riportando questa verità nel campo linguistico, penso che nello scrivere una parola straniera che inizi per h in maniera imperitura dentro a un testo, possiamo prenderci la briga di andare su Google, spendere dai 5 ai 10 minuti per la ricerca e scoprire che dobbiamo scrivere *lo* hashtag, proprio come abbiamo fatto in ordine cronologico lucio, tu ed io.

Poi ci capita che dobbiamo usare la parola nel parlato e, sapessimo o meno l'articolo che bisognerebbe preporre, ci scontriamo col fatto pratico che l'accoppiata *lo hastag* - seppur corretta e senza controindicazioni nella fase di scrittura - nel parlato risulta invece in un suono cacofonico per quanto innaturale, è certamente innaturale il suono di un articolo terminante in vocale seguito da parola che inizi per vocale (si, la h,  muta o aspirata che sia, nel parlato nemmeno i cani o i pipistrelli la scorgerebbero).

E allora ecco che se sto a discorso verbale con un amico riguardo *l'* hashtag, magari non penso veramente che la Crusca si è fumato un cannone, che i tuoi amici Carducci e Croce erano nel giro dei pusher e Serianni fruitore accanito, ma sicuramente non ne seguo i 'consigli' perché se devo dire 10 volte _lo hashtag_, _uno hashtag_, _lo hobby_, _uno hobby_ faccio prima a cambiare discorso o , appunto, l'articolo.

Il tuo elettricista o l'idraulico fanno a meno di molti aspetti della teoria dei fluidi e dell'elettronica, quegli aspetti  teorici ma non pratici. Puoi chiedere al tuo elettricista se pensa che Tesla si sia fumato un cannone, io almeno Carducci lo conosco per nome.

In ultima analisi, *la regola della Crusca non è una regola*.
Piena di contraddizioni, di eccezioni, di variabili imponderabili dettate dall'orecchio, dal tempo e da un loro giudizio 'quando vogliono loro' (datemi un termine equivalente).

Una non regola, veramente.

Chi decide quando una parola straniera inglese è italianizzata?

Prima o poi lo sarà, ma devo aspettare anni perché la Crusca  decida che 'hashtag' è italianizzata e sentirmi in regola nel dire _l'hashtag_?

Senza contare che oggigiorno qualsiasi parola inglese viene adottata in maniera istantanea e duratura nella nostra lingua.

Per questo dico, la regola non è una regola, fare le pulci al*l*'hashtag con questi presupposti ha veramente, non poco, ma nessun senso.

Mi ripeto: all'autorevolezza c'è un limite. Il buon senso.</description>
	<ipaddress></ipaddress>
	<author>paoloo</author>
	<email></email>
	<link></link>
</item>