Ci sono modifiche importanti alla versione in produzione alla data 31-12-2023. In questo branch:

- da config.toml a hugo.toml per seguire la nuova nomenclatura
- ora ci sono più file di configurazione con diverse finalità
  - hugo-dev.toml deve essere utilizzato in locale per sperimentare il sistema di commenti e viene invocato con 
  ```sh
  hugo server --config hugo-dev.toml
  ```
  - hugo-prod.toml analogamente deve essere utilizzato per la produzione
- rivista la paginazione
- alleggerita la barra di navigazione, ma inserito un link ai post dell'epoca Script (in seguito stesso trattamento per Ping! possibile)
- cambiata la ricerca sul sito
  - nuovo motore fuse.js
  - inserita pagina di ricerca
  - possibilità di utilizzare operatori booleani

Per la nuova ricerca ho seguito principalmente le indicazioni di [Yihui Xie](https://yihui.org/en/2023/09/fuse-search/)
