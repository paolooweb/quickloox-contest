// avatary
// by Paoloo, da un'idea di MacMomo

var avatary = (function (scope) {
    var config = {
        fontFamily: "Geneva",
        capitalsVerticalOffset: 0,
        contrast: true,
        bgShade: true,
        bgReflex: false,
        borderSize: 0,
        radius: 20,
        size: 60,
    };

    var cssToHexadecimalColors = {
        aliceblue: "#f0f8ff",
        antiquewhite: "#faebd7",
        aqua: "#00ffff",
        aquamarine: "#7fffd4",
        azure: "#f0ffff",
        beige: "#f5f5dc",
        bisque: "#ffe4c4",
        black: "#000000",
        blanchedalmond: "#ffebcd",
        blue: "#0000ff",
        blueviolet: "#8a2be2",
        brown: "#a52a2a",
        burlywood: "#deb887",
        cadetblue: "#5f9ea0",
        chartreuse: "#7fff00",
        chocolate: "#d2691e",
        coral: "#ff7f50",
        cornflowerblue: "#6495ed",
        cornsilk: "#fff8dc",
        crimson: "#dc143c",
        cyan: "#00ffff",
        darkblue: "#00008b",
        darkcyan: "#008b8b",
        darkgoldenrod: "#b8860b",
        darkgray: "#a9a9a9",
        darkgreen: "#006400",
        darkgrey: "#a9a9a9",
        darkkhaki: "#bdb76b",
        darkmagenta: "#8b008b",
        darkolivegreen: "#556b2f",
        darkorange: "#ff8c00",
        darkorchid: "#9932cc",
        darkred: "#8b0000",
        darksalmon: "#e9967a",
        darkseagreen: "#8fbc8f",
        darkslateblue: "#483d8b",
        darkslategray: "#2f4f4f",
        darkslategrey: "#2f4f4f",
        darkturquoise: "#00ced1",
        darkviolet: "#9400d3",
        deeppink: "#ff1493",
        deepskyblue: "#00bfff",
        dimgray: "#696969",
        dimgrey: "#696969",
        dodgerblue: "#1e90ff",
        firebrick: "#b22222",
        floralwhite: "#fffaf0",
        forestgreen: "#228b22",
        fuchsia: "#ff00ff",
        gainsboro: "#dcdcdc",
        ghostwhite: "#f8f8ff",
        gold: "#ffd700",
        goldenrod: "#daa520",
        gray: "#808080",
        green: "#008000",
        greenyellow: "#adff2f",
        grey: "#808080",
        honeydew: "#f0fff0",
        hotpink: "#ff69b4",
        indianred: "#cd5c5c",
        indigo: "#4b0082",
        ivory: "#fffff0",
        khaki: "#f0e68c",
        lavender: "#e6e6fa",
        lavenderblush: "#fff0f5",
        lawngreen: "#7cfc00",
        lemonchiffon: "#fffacd",
        lightblue: "#add8e6",
        lightcoral: "#f08080",
        lightcyan: "#e0ffff",
        lightgoldenrodyellow: "#fafad2",
        lightgray: "#d3d3d3",
        lightgreen: "#90ee90",
        lightgrey: "#d3d3d3",
        lightpink: "#ffb6c1",
        lightsalmon: "#ffa07a",
        lightseagreen: "#20b2aa",
        lightskyblue: "#87cefa",
        lightslategray: "#778899",
        lightslategrey: "#778899",
        lightsteelblue: "#b0c4de",
        lightyellow: "#ffffe0",
        lime: "#00ff00",
        limegreen: "#32cd32",
        linen: "#faf0e6",
        magenta: "#ff00ff",
        maroon: "#800000",
        mediumaquamarine: "#66cdaa",
        mediumblue: "#0000cd",
        mediumorchid: "#ba55d3",
        mediumpurple: "#9370db",
        mediumseagreen: "#3cb371",
        mediumslateblue: "#7b68ee",
        mediumspringgreen: "#00fa9a",
        mediumturquoise: "#48d1cc",
        mediumvioletred: "#c71585",
        midnightblue: "#191970",
        mintcream: "#f5fffa",
        mistyrose: "#ffe4e1",
        moccasin: "#ffe4b5",
        navajowhite: "#ffdead",
        navy: "#000080",
        oldlace: "#fdf5e6",
        olive: "#808000",
        olivedrab: "#6b8e23",
        orange: "#ffa500",
        orangered: "#ff4500",
        orchid: "#da70d6",
        palegoldenrod: "#eee8aa",
        palegreen: "#98fb98",
        paleturquoise: "#afeeee",
        palevioletred: "#db7093",
        papayawhip: "#ffefd5",
        peachpuff: "#ffdab9",
        peru: "#cd853f",
        pink: "#ffc0cb",
        plum: "#dda0dd",
        powderblue: "#b0e0e6",
        purple: "#800080",
        rebeccapurple: "#663399",
        red: "#ff0000",
        rosybrown: "#bc8f8f",
        royalblue: "#4169e1",
        saddlebrown: "#8b4513",
        salmon: "#fa8072",
        sandybrown: "#f4a460",
        seagreen: "#2e8b57",
        seashell: "#fff5ee",
        sienna: "#a0522d",
        silver: "#c0c0c0",
        skyblue: "#87ceeb",
        slateblue: "#6a5acd",
        slategray: "#708090",
        slategrey: "#708090",
        snow: "#fffafa",
        springgreen: "#00ff7f",
        steelblue: "#4682b4",
        tan: "#d2b48c",
        teal: "#008080",
        thistle: "#d8bfd8",
        tomato: "#ff6347",
        turquoise: "#40e0d0",
        violet: "#ee82ee",
        wheat: "#f5deb3",
        white: "#ffffff",
        whitesmoke: "#f5f5f5",
        yellow: "#ffff00",
        yellowgreen: "#9acd32",
    };

    // COLORE DI FONDO
    var colorList = [
        "AliceBlue",
        //'AntiqueWhite',
        // 'Aqua',
        // 'Aquamarine',
        // 'Azure',
        "Beige",
        //'Bisque',
        //'Black',
        //'BlanchedAlmond',
        "Blue",
        "BlueViolet",
        //"Brown",
        //"BurlyWood",
        "CadetBlue",
        "Chartreuse", // *
        "Chocolate",
        "Coral",
        "CornflowerBlue",
        "Cornsilk",
        "Crimson",
        "Cyan",
        //"DarkBlue",
        //"DarkCyan",
        "DarkGoldenRod",
        "DarkGray",
        //"DarkGreen",
        "DarkKhaki",
        //"DarkMagenta",
        //"DarkOliveGreen",
        "DarkOrange",
        //"DarkOrchid",
        "DarkRed",
        "DarkSalmon",
        "DarkSeaGreen",
        //"DarkSlateBlue",
        "DarkSlateGrey",
        //"DarkTurquoise",
        //"DarkViolet",
        "DeepPink",
        "DeepSkyBlue",
        "DimGrey",
        "DodgerBlue",
        "FireBrick",
        //'FloralWhite',
        "ForestGreen",
        //"Fuchsia",
        "Gainsboro",
        //"GhostWhite",
        "Gold",
        "GoldenRod",
        //"Grey",
        //"Green",
        "GreenYellow",
        //'HoneyDew',
        "HotPink",
        "IndianRed",
        //"Indigo",
        "Ivory",
        "Khaki",
        //"Lavender",
        "LavenderBlush",
        //"LawnGreen",
        //'LemonChiffon',
        "LightBlue",
        "LightCoral",
        //'LightCyan',
        //'LightGoldenRodYellow',
        "LightGray",
        //'LightGrey',
        "LightGreen",
        //"LightPink",
        "LightSalmon",
        "LightSeaGreen",
        "LightSkyBlue",
        "LightSlateGray",
        //'LightSlateGrey',
        "LightSteelBlue",
        //'LightYellow',
        //'Lime',
        "LimeGreen",
        //"Linen",
        "Magenta",
        "Maroon",
        //"MediumAquaMarine",
        //"MediumBlue",
        "MediumOrchid",
        "MediumPurple",
        "MediumSeaGreen",
        "MediumSlateBlue",
        //"MediumSpringGreen",
        "MediumTurquoise",
        "MediumVioletRed",
        //"MidnightBlue",
        //'MintCream',
        "MistyRose",
        //'Moccasin'
        //'NavajoWhite'
        //'Navy'
        //'OldLace',
        "Olive",
        //"OliveDrab",
        "Orange",
        "OrangeRed",
        "Orchid",
        "PaleGoldenRod",
        "PaleGreen",
        "PaleTurquoise",
        //"PaleVioletRed",
        //'PapayaWhip'
        //'PeachPuff',
        "Peru",
        //'Pink',
        "Plum",
        "PowderBlue",
        //"Purple",
        //"RebeccaPurple",
        "Red",
        "RosyBrown",
        "RoyalBlue",
        "SaddleBrown",
        "Salmon",
        "SandyBrown",
        "SeaGreen",
        //'SeaShell',
        "Sienna",
        "Silver",
        "SkyBlue",
        "SlateBlue",
        "SlateGray",
        "Snow",
        //"SpringGreen",
        "SteelBlue",
        "Tan",
        "Teal",
        "Thistle",
        "Tomato",
        "Turquoise",
        "Violet",
        "Wheat",
        "White",
        //'WhiteSmoke',
        "Yellow",
        //"YellowGreen",
    ];

    function getInitialsFromName(name) {
        var a = "A",
            b = "A",
            parts = name.trim().replace(/\s+/g, " ").split(" ", 2);

        // RECUPERA LE 2 LETTERE
        if (parts.length > 1) {
            a = parts[0].charAt(0);
            b = parts[1].charAt(0);
        } else {
            if (name.length > 1) {
                a = name.charAt(0);
                b = name.charAt(1);
            } else {
                if (name.length > 0) {
                    a = name.charAt(0);
                    b = name.charAt(0);
                }
            }
        }

        return [a, b].join("");
    }

    function getAvatar(name) {
        var initials = getInitialsFromName(name);

        var size = config.size;
        var radius = config.radius;
        var borderSize = config.borderSize;

        var canvas = document.createElement("canvas");
        canvas.setAttribute("width", size);
        canvas.setAttribute("height", size);

        var ctx = canvas.getContext("2d");

        // COLORINDEX FROM NAME
        var colorIndex = 0;
        for (var i = 0; i < name.length; i++) colorIndex = (colorIndex << 5) - colorIndex + name.charCodeAt(i);
        colorIndex = Math.abs(colorIndex % colorList.length);
        // console.log(color);

        // COLORE DI SFONDO
        var bgColor = name == "paoloo" ? "black" : colorList[colorIndex];
        var isBgColorLight = isColorLight(nameToHex(bgColor));

        if (config.borderSize > 0) ctx.roundRect(0, 0, size, size, radius, nameToHSL(bgColor, { l: 30 })); // BORDO ESTERNO CHIARO

        ctx.roundRect(borderSize, borderSize, size - borderSize * 2, size - borderSize * 2, radius - 2, bgColor); // COLORE DI SFONDO

        if (config.bgShade) {
            if (isBgColorLight)
                ctx.roundRect(borderSize, borderSize, size - borderSize * 2, size - borderSize * 2, 9, createSoftShadeForLightColors(ctx, size - borderSize * 2)); // SFUMATURA LEGGERA DELLO SFONDO
            else ctx.roundRect(borderSize, borderSize, size - borderSize * 2, size - borderSize * 2, radius - 2, createSoftShadeForDarkColors(ctx, size - borderSize * 2)); // SFUMATURA LEGGERA DELLO SFONDO
        }

        // INIZIALI
        var contrastedColor = isBgColorLight ? "#00000088" : "#ffffffee";
        ctx.fillStyle = config.contrast ? contrastedColor : "#fff";
        if (name == "paoloo") ctx.fillStyle = "gold";
        if (config.fontFamily) ctx.font = size / 2 + "px '" + config.fontFamily + "', sans-serif";
        else ctx.font = size / 2 + "px sans-serif";
        ctx.textBaseline = "middle";
        ctx.textAlign = "center";

        var verticalOffset = 0;
        if (hasCapitalSizedLetters(initials) && config.capitalsVerticalOffset) verticalOffset = config.capitalsVerticalOffset;

        ctx.fillText(initials, size / 2, size / 2 + verticalOffset);

        // RIFLESSO LUCE VERTICALE
        if (config.bgReflex) ctx.roundRect(borderSize, borderSize, size - borderSize * 2, size - borderSize * 2, radius, createHalfLightReflex(ctx, size - 6));

        // console.log(name + " : " + bgColor);

        // IMMAGINE
        return canvas.toDataURL("image/png");
    }

    function createHalfLightReflex(ctx, height) {
        var reflex = ctx.createLinearGradient(0, 0, height / 2, height / 2);
        reflex.addColorStop("1", "#ffffff00");
        reflex.addColorStop("0.95", "#ffffff30");
        reflex.addColorStop("0", "#ffffff00");

        return reflex;
    }

    function createSoftShadeForDarkColors(ctx, height) {
        var bgShade = ctx.createLinearGradient(0, 0, 0, height);
        bgShade.addColorStop("0", "rgba(255,255,255,0.43)");
        bgShade.addColorStop("1", "rgba(255,255,255,0.03)");

        return bgShade;
    }

    function createSoftShadeForLightColors(ctx, height) {
        var bgShade = ctx.createLinearGradient(0, 0, 0, height);
        bgShade.addColorStop("1", "rgba(0,0,0,0.08)");
        bgShade.addColorStop("0", "rgba(0,0,0,0.0)");

        return bgShade;
    }

    function nameToHSL(name, mods, sets) {
        let rgb = hexToRGBArray(nameToHex(name)),
            r = rgb[0] / 255,
            g = rgb[1] / 255,
            b = rgb[2] / 255,
            cmin = Math.min(r, g, b),
            cmax = Math.max(r, g, b),
            delta = cmax - cmin,
            h = 0,
            s = 0,
            l = 0;

        if (delta == 0) h = 0;
        else if (cmax == r) h = ((g - b) / delta) % 6;
        else if (cmax == g) h = (b - r) / delta + 2;
        else h = (r - g) / delta + 4;

        h = Math.round(h * 60);

        if (h < 0) h += 360;

        l = (cmax + cmin) / 2;
        s = delta == 0 ? 0 : delta / (1 - Math.abs(2 * l - 1));
        s = +(s * 100).toFixed(1);
        l = +(l * 100).toFixed(1);

        if (mods) {
            if (mods.h) h = h + mods.h;
            if (mods.s) s = s + mods.s;
            if (mods.l) l = l + mods.l;

            if (l > 90) l = 90;
            if (s > 60) s = 60;
            if (l < 60) l = 60;
        }

        if (sets) {
            if (sets.h) h = sets.h;
            if (sets.s) s = sets.s;
            if (sets.l) l = sets.l;
        }

        return "hsl(" + h + "," + s + "%," + l + "%)";
    }

    function isColorLight(color) {
        var result = luma(color) >= 200; // 200
        return result;
    }

    function luma(color) {
        // color can be a hx string or an array of RGB values 0-255
        var rgb = typeof color === "string" ? hexToRGBArray(color) : color;
        return 0.2126 * rgb[0] + 0.7152 * rgb[1] + 0.0722 * rgb[2]; // SMPTE C, Rec. 709 weightings
    }

    function hexToRGBArray(color) {
        if (color.length === 3) color = color.charAt(0) + color.charAt(0) + color.charAt(1) + color.charAt(1) + color.charAt(2) + color.charAt(2);
        else if (color.length !== 6) throw "Invalid hex color: " + color;
        var rgb = [];
        for (var i = 0; i <= 2; i++) rgb[i] = parseInt(color.substr(i * 2, 2), 16);
        return rgb;
    }

    function nameToHex(name) {
        return cssToHexadecimalColors[name.toLowerCase()].substring(1);
    }

    function use(settings) {
        var properties = ["fontFamily", "capitalsVerticalOffset", "contrast", "bgShade", "bgReflex", "borderSize", "radius", "size"];

        for (prop in settings) {
            if (properties.indexOf(prop) != -1) {
                config[prop] = settings[prop];
            }
        }
    }

    function hasCapitalSizedLetters(str) {
        return !/[a-z]/.test(str);
    }

    return {
        get: getAvatar,
        use: use,
    };
})();

CanvasRenderingContext2D.prototype.roundRect =
    /**
     * Draws a rounded rectangle using the current state of the canvas.
     * If you omit the last three params, it will draw a rectangle
     * outline with a 5 pixel border radius
     * @param {CanvasRenderingContext2D} ctx
     * @param {Number} x The top left x coordinate
     * @param {Number} y The top left y coordinate
     * @param {Number} width The width of the rectangle
     * @param {Number} height The height of the rectangle
     * @param {Number} theRadius The corner radius. Defaults to 5;
     * @param {Boolean} fill Whether to fill the rectangle. Defaults to false.
     * @param {Boolean} stroke Whether to stroke the rectangle. Defaults to false.
     */
    function (x, y, width, height, theRadius, fill, stroke) {
        if (typeof stroke == "undefined") {
            stroke = false;
        }
        if (typeof theRadius === "undefined") {
            theRadius = 5;
        }
        this.beginPath();
        this.moveTo(x + theRadius, y);
        this.lineTo(x + width - theRadius, y);
        this.quadraticCurveTo(x + width, y, x + width, y + theRadius);
        this.lineTo(x + width, y + height - theRadius);
        this.quadraticCurveTo(x + width, y + height, x + width - theRadius, y + height);
        this.lineTo(x + theRadius, y + height);
        this.quadraticCurveTo(x, y + height, x, y + height - theRadius);
        this.lineTo(x, y + theRadius);
        this.quadraticCurveTo(x, y, x + theRadius, y);
        this.closePath();
        if (fill) {
            this.fillStyle = fill;
            this.fill();
        }
        if (stroke) {
            this.strokeStyle = stroke;
            this.stroke();
        }
    };
