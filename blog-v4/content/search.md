---
title: Pagina di ricerca
date: 2023-12-31
disable_comments: true
---

Il motore di ricerca è [fuse.js](https://www.fusejs.io)[^1]. Gli spazi tra i termini cercati equivalgono all'operatore booleano **AND**, mentre **|** è usato per indicare l'operatore **OR**. Per cercare frasi esatte utilizzare i doppi apici.

<style type="text/css">
.main {
  width: 100%;
}
.title, .toc-line > a {
  font-style: initial;
}
.single .main a, .single .main h2 {
  border-bottom: none;
}
.main h2 {
  text-align: initial;
}
#search-input {
  width: 100%;
  font-size: 1.2em;
  padding: .5em;
}
.search-results {
  font-size: .9em;
}
.search-results b {
  background-color: yellow;
}
.search-preview {
  margin-left: 2em;
}
.footnotes {
  margin-top: 4em;
}
</style>

<input type="search" id="search-input">

<div class="search-results">
<section>
<h2 class="toc-line"><a target="_blank"></a><span class="dots"></span><span class="page-num small"></span></h2>
<div class="search-preview"></div>
</section>
</div>

<script src="https://cdn.jsdelivr.net/npm/fuse.js@7.0.0" defer></script>
<script src="https://cdn.jsdelivr.net/npm/@xiee/utils/js/fuse-search.min.js" defer></script>

[^1]: La ricerca sul sito può anche essere effettuata con altri motori - DuckDuckGo, Google - postponendo al termine cercato  `site:macintelligence.org`, e.g., `Jobs site:macintelligence.org`.
