---
title: "Cos&#236; per Spore"
date: 2008-07-14
draft: false
tags: ["ping"]
---

Dopo avere citato <a href="http://www.macworld.it/blogs/ping/?p=1868" target="_blank">Nerino</a>, è doveroso farlo anche per <strong>Ernesto</strong>:

<cite>Quella è la descrizione di come usare il gioco senza pagare gli sviluppatori facendo credere al programma che lo si è comprato, quando invece lo si è solo craccato.</cite>

<cite>Quando uno parla di</cite> generare il seriale <cite>significa che usa un software, chiamato solitamente</cite> KeyGen<cite>, cioè</cite> key generator <cite>(di solito un</cite> <code>.exe</code> <cite>che gira su PC) per creare un numero seriale corretto simile o identico a quello che ti manderebbero a casa gli sviluppatori dopo che hai pagato.</cite>

<cite>E quelli di Vitality più che</cite> smanettoni <cite>sono veri e propri pirati informatici che distribuiscono in rete copie sprotette o appunto</cite> KeyGen <cite>per attivare programmi non comprati.</cite>

Spore è interessante, qui, perché sta suscitando un sacco di interesse e rappresenta un gioco un po' diverso dal solito Fifa 2008.

Come per tutta la conoscenza, può essere usata bene o male. Sempre qui, si tifa per il bene. Sventra Spore per scoprire quello che c'è dentro, non per rubare.

Vista l'occasione, forse non avevo ancora citato la pagina con le istruzioni per fare funzionare Creature Creator <a href="http://www.tuaw.com/2008/06/17/run-spore-creature-creator-on-unsupported-gma-950-macs/" target="_blank">anche su un Mac mini</a> dotato di pseudoscheda Gma 950 e nominalmente non supportato.