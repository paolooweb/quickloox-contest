---
title: "300+ novità: Dvd Player"
date: 2007-10-24
draft: false
tags: ["ping"]
---

Come racconta Apple nella pagina che racchiude <a href="http://www.apple.com/it/macosx/features/300.html" target="_blank">tutte le modifiche fondamentali apportate a Leopard</a>, Dvd Player ha cambiato completamente l'interfaccia, fatta di pannelli semitrasparenti accessibili nella parte bassa o alta dello schermo. Una delle conseguenze è che adesso si può navigare l'interfaccia del Dvd anche mentre lo si guarda a pieno schermo.

Il nuovo Time Slider permette di scegliere un punto preciso del film da cui avviare la visione, oppure navigare il film stesso in avanti e all'indietro con il mouse.

Esiste un Auto Zoom, che - volendo - riempie automaticamente l'area dello schermo con il contenuto, eliminando le barre orizzontali o verticali intorno al film (ovviamente parte dell'immagine viene tagliata, dipende da come si preferisce la visione).

Ogni capitolo del Dvd adesso ha una propria miniatura. Possiamo personalizzare ogni miniatura con un fotogramma a nostra scelta.

È stato introdotto uno skip di cinque secondi, avanti o indietro, per saltare una scena poco gradita o rivederne una molto gradita.

Leopard fa del suo meglio per rendere riproducibili i Dvd danneggiati.

Si poteva già lavorare prima con il Dvd in una finestra e un'applicazione diversa in uso, ma ora possiamo scegliere di avere la finestra del film in primo piano, qualsiasi altra cosa si stia facendo.

Il controllo censura fa s&#236; che possa essere necessaria un'autorizzazione per riprodurre i Dvd, per esempio se sappiamo che i piccoli hanno accesso al computer e che ci sono titoli che è meglio non vengano visti.

Ultima cosa, una tecnologia che Apple chiama Adaptive Video Analyzation, che applica a richiesta il deinterlacciamento e un <em>inverse 3:2 pulldown</em> per migliorare la qualità del video.