---
title: "Pubblico e privato"
date: 2006-04-18
draft: false
tags: ["ping"]
---

Negli ultimi giorni la mia agenda ha risolto due problemi praticamente da sola. Chi lavora con me ha il mio <a href="http://ical.mac.com/lux/Work" target="_blank">calendario pubblico</a> di iCal, dove pu&ograve; vedere quando ho impegni e quando invece, pur lavorando, sono potenzialmente disponibile. Cos&igrave;, se c&rsquo;&egrave; un consiglio di amministrazione di Mac@Work oppure una riunione con un editore, mi vengono proposti gi&agrave; in partenza una data e un orario che hanno buone probabilit&agrave; di funzionare.

Si risparmiano cos&igrave; penosi giri di telefonate e di posta elettronica. E l&rsquo;abbonamento a .mac si &egrave; gi&agrave; quasi ripagato solo per questo.