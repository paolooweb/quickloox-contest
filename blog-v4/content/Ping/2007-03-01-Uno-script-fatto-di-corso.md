---
title: "Uno script fatto di corso"
date: 2007-03-01
draft: false
tags: ["ping"]
---

Ho tenuto la prima parte di un corsetto molto base di AppleScript presso <a href="http://www.macatwork.net" target="_blank">Mac@Work</a> e nel corso della sessione ho scritto quasi di getto uno scriptino che estrae un numero a caso da 1 a 10 e decide se è un numero pari oppure un numero dispari:

<code>set numero_estratto to (random number from 1 to 10)
set prima_divisione to numero_estratto / 2
set seconda_divisione to numero_estratto / 2 as integer
if prima_divisione is equal to seconda_divisione then
	display dialog (numero_estratto as string) &#38; " è pari!"
else
	display dialog (numero_estratto as string) &#38; " è dispari!"
end if</code>

Ma eravamo verso fine sessione. Cos&#236;, quando <strong>Jacopo</strong> ha chiesto se non si sarebbe potuto modificare lo script in modo da inserire manualmente il numero desiderato, l'ho dribblato per non lasciare niente a metà.

Ogni domanda è debito però, ed ecco qui:

<code>set numero_scelto to text returned of (display dialog "Che numero controlliamo?" default answer ") as integer
set prima_divisione to (numero_scelto / 2)
set seconda_divisione to numero_scelto / 2 as integer
if prima_divisione is equal to seconda_divisione then
	display dialog (numero_scelto as string) &#38; " è pari!"
else
	display dialog (numero_scelto as string) &#38; " è dispari!"
end if</code>

