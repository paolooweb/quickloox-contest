---
title: "TeleFone o Voda2?"
date: 2009-05-11
draft: false
tags: ["ping"]
---

Un tempo lontano passai da Tele2 a FastWeb.

Il 3 novembre invio la disdetta a Tele2 (per stipulare il contratto basta la voce, ma per disdirlo bisogna scrivere).

La raccomandata arriva a Tele2 il 22 novembre (viva le Poste, diciannove giorni ma almeno non l'hanno buttata).

Tele2 elabora la disdetta il 21 dicembre.

Per la effettiva cessazione del contratto, mi dicono, devono decorrere trenta giorni. Quindi 21 gennaio (due mesi e mezzo dopo la disdetta).

Purtroppo, mi dicono, c'è stato un ritardo. La disattivazione effettiva è prevista per il 21 maggio.

Nel frattempo è arrivata un'altra fattura, per un servizio e un numero di telefono che non uso da sei mesi.

(Sono i soldi più preziosi, quelli che si incassano senza dare niente in cambio.)

Del tutto casualmente non riesco più ad accedere alla mia area personale sul sito Tele2. La password che ha sempre funzionato ora non funziona più. Ho provato a dire di avere smarrito la password; la puoi recuperare fornendo l'indirizzo email oppure il codice cliente.

Nessuno dei miei indirizzi email viene riconosciuto. Il codice cliente, che è stampato bello chiaro sulle fatture ora davanti a me, è <cite>errato</cite>.

Chiamo il servizio clienti e mi dicono di spedire un fax all'ufficio amministrazione. Mando il fax. Dopo tre minuti suona il cellulare: è un venditore di Infostrada con le nuove mirabolanti offerte del momento.

Infostrada è proprietà di Vodafone, proprietaria di Tele2. Toh.