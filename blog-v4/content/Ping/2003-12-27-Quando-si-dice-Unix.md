---
title: "Quando si dice Unix"
date: 2003-12-27
draft: false
tags: ["ping"]
---

Cominciano a succedere cose insolite e piacevoli nel mondo dello sviluppo software

Per curiosità ho scaricato un programmino che si chiama Html2Wml, creato, lo dice la parola stessa, per convertire codice Html in Wml, il linguaggio ipertestuale per i cellulari Wap.

Il programmino è per Unix e così ho scaricato dal <link>sito</link>http://htmlwml.sourceforge.net una tarball contenente il codice sorgente, da compilare.

Leggo il file Install alla ricerca di istruzioni e scopro che l’autore del programmino usa come piattaforma di sviluppo Mac OS X.

Uno sviluppatore che usa Mac OS X per creare un programma Unix da usare per il mondo dei cellulari: sembra una cosa normale, ma fino a non molto tempo fa era fantascienza pura.

<link>Lucio Bragagnolo</link>lux@mac.com