---
title: "Posizioni da rivedere"
date: 2008-08-07
draft: false
tags: ["ping"]
---

Medialets ha diffuso, su dati Goldman Sachs, una statistica molto interessante sul destino degli iPhone 3G <a href="http://www.medialets.com/blog/2008/08/04/the-first-1-million-iphones-where-did-they-go/" target="_blank">venduti nel primo weekend</a> di disponibilità.

Risulta che l'Olanda (un francobollo) ha assorbito il 37 percento in più di iPhone dell'Italia. La Svezia, con un numero di abitanti molto inferiore, ne ha venduti il 25 percento in più. La Spagna ci batte sette a uno, la Francia otto a uno, il Giappone nove a uno.

L'Italia supera Belgio, Norvegia, Svizzera, Danimarca, Irlanda. E vorrei vedere.

Se si va a guardare il tasso di penetrazione sul mercato, tuttavia, l'Italia è omogenea agli altri Paesi. CIoè l'Italia avrebbe venduto un numero di iPhone congruo con le dimensioni del proprio mercato.

Tutto questo contrasta fortemente con il sentire comune che siamo uno dei primi mercati al mondo per la vendita di cellulari. Pare una gran balla.

Se non è così, si rafforza l'idea che iPhone non sia un cellulare e che quindi i suoi dati di vendita vadano letti in altro modo.