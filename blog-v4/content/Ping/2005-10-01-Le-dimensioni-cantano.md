---
title: "Le dimensioni cantano<p>"
date: 2005-10-01
draft: false
tags: ["ping"]
---

Per non parlare del peso<p>

Se Apple facesse un portatile che pesa quattro chili e mezzo, il mondo si sbellicherebbe dal ridere.<p>

Oggi si parlava di portatili e mi è stato segnalato un Acer da 17 pollici &ldquo;con un ottimo prezzo&rdquo;. Sono andato a vedere e l&rsquo;oggetto pesava 4,5 chili.<p>

Del prezzo non sto neanche a dire. Il mio Mac da 17 pollici pesa 3,1 chili ed è un portatile. Quell&rsquo;Acer, al massimo, è un sollevabile.<p>

Il guaio è che nessuno si sbellica. Qualunque schifezza passa, basta che costi poco. Ha cessato di avere un valore perfino la schiena di chi il computer se lo deve portare appresso seriamente.<p>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>