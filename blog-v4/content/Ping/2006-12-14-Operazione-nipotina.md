---
title: "Operazione nipotina"
date: 2006-12-14
draft: false
tags: ["ping"]
---

La nipotina ha compiuto i quattro anni e ha pericolosamente cominciato a interessarsi al computer di papà. Il quale ha chiesto soccorso allo zio per fare distrarre la nipotina in questione, possibilmente (quando ti dicono <em>possibilmente</em> significa sempre <em>arrangiati tu che io non ne voglio sapere</em>) a costo zero.

Ho cominciato a frugare nell'assistenza di Mac@Work alla ricerca di una macchina abbandonata e desiderosa di adozione.

Apparentemente la classe di orfani che va per la maggiore in questo momento è quella degli iMac colorati. Ho individuato un Blueberry, che però si è arreso quasi subito: dopo dieci minuti in cui sembrava a posto, la sua scheda video ci ha lasciati per un mondo migliore (il paradiso delle schede video sarà bianco e nero?).

Il candidato di riserva è un <a href="http://lowendmac.com/imacs/imac-d.shtml" target="_blank">Tangerine</a>. Purtroppo non ha porte FireWire e dunque, a meno di hack che non so se avrò il tempo di applicare, credo che non andrà oltre Jaguar. Il che non dovrebbe impedirmi di riempirlo con tutto il software freeware per bambini piccoli che riesco a trovare.

Che impressione, comunque, ritrovarsi con un iMac colorato. Mi vengono in mente i tempi del Blue Bondi Revision A.

Se nei prossimi giorni non arriverà qualche iBook conchiglione (che sarebbe a dire il vero l'ideale) la nipotina si vedrà recapitato un Tangerine. Ma a quattro anni potrebbe essere comunque un buon Natale.