---
title: "La vera riforma scolastica"
date: 2001-12-18
draft: false
tags: ["ping"]
---

“Apple sale in cattedra” era un titolo troppo scontato

Apple inizia a fare sul serio con la scuola anche in Italia. La transizione a Mac OS X è come se fosse fatta apposta per gli studenti, con l’approccio Unix a base open source; ora arrivano soluzioni semplici ma ingegnose come iCart Mobile Classroom, un mobiletto su rotelle che, tra stampante, base Airport, software di rete e iBook vari, permette di informatizzare seriamente una classe - e un’ora dopo la classe dopo - nel giro di cinque minuti.
Apple non può che incontrare difficoltà nella scuola italiana, che mediamente è - nell’informatica - un mondo di ignoranti, faccendieri e piccoli profittatori. Ma mi piace pensare che persone oneste, competenti e volonterose come Riccardo Salafia, Education Sales Manager di Apple, o Alessandro Pisani, Education Sales Engineer, riusciranno a fare le cose giuste anche quando la concorrenza di fatto è il cantinaro raccomandato parente dell’amica del preside.