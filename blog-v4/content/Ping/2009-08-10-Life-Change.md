---
title: "Life Change"
date: 2009-08-10
draft: false
tags: ["ping"]
---

Ricevo da un caro amico in vacanza:

<cite>Un saluto dalla Regent House di Londra. Ci ho passato un intero pomeriggio e poi ci sono tornato alla sera&#8230;</cite>

<cite>Fantastico, emozionante, senza parole. Poi, con la connessione <i>wifi free</i> (certe cose succedono solo fuori dall'Italia) e la <i>app</i> di Lufthansa per iPhone, ho fatto il <i>check-in</i> per il rientro. Posto in  seconda fila&#8230; senza parole! No, no mi sono perso: ho girato in lungo e in largo, tanto avevo la <i>app</i> Metro che mi elencava tutti i cambi di linea e  poi, con Around me, non mi sono perso uno Starbucks! S&#236;, lo so, il mio inglese non è a livello del tuo, ma avevo con me il Collins installato sull'iPhone&#8230;</cite>

<cite>Come la Apple può cambiarti la vita. In meglio.</cite>

È anche molto più bello cos&#236; che ricevere la solita cartolina-saluti-da-Londra.