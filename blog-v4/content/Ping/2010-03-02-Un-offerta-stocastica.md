---
title: "Un'offerta stocastica"
date: 2010-03-02
draft: false
tags: ["ping"]
---

Se ho capito bene, <a href="http://www.isisimaging.com/ICEFIELDS.html" target="_blank">Icefields</a> crea immagini a mezzitoni ottimizzate attraverso l'impiego di tecniche matematiche piuttosto sofisticate di elaborazione delle immagini.

Ho invece capito benissimo che fino al 31 marzo è possibile acquistare il programma per un importo <i>qualsiasi</i>. Il prezzo di listino è 150 dollari e si tratta, comunque, di un bel risparmio.