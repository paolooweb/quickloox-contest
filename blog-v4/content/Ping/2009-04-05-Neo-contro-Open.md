---
title: "Neo contro Open"
date: 2009-04-05
draft: false
tags: ["ping"]
---

Sono uno dei sostenitori dell'integrazione tra <a href="http://www.neooffice.org/neojava/en/index.php" target="_blank">NeoOffice</a> e <a href="http://openoffice.org" target="_blank">OpenOffice.org</a>.

In attesa che accada, come tutti scelgo. Istintivamente scelgo Openoffice.org.

Ora si è insinuato il tarlo del dubbio, perché NeoOffice 3.0 comprende un <em>plugin</em> per la navigazione del Media Browser di Mac OS X, quello che facilita l'inserimento di un brano iTunes dentro una presentazione di iPhoto o uno scatto di iPhoto dentro un video di iMovie.

OpenOffice.org ha una montagna di pregi e un difetto: siccome è scritto per più piattaforme, trascura i vantaggi specifici di ciascuna.

Se NeoOffice oggi ha il Media Browser, da un punto di vista di <em>design</em> dell'applicazione, domani potrebbe decidere di diventare compatibile AppleScript, per dire, o altro. Sarebbe un vantaggio fondamentale.

Cerco di saperne un po' di più. Nel frattempo, la mia volontà di donatore dei miei pochi euro verso il buon software libero non è più univoca come prima.