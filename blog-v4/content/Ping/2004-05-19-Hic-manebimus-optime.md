---
title: "Hic manebimus optime"
date: 2004-05-19
draft: false
tags: ["ping"]
---

La trasparenza sulla sicurezza mostra quale sia la piattaforma più affidabile

Il buco di sicurezza che sfrutta un browser qualunque e il Visore Aiuto per eseguire codice arbitrario da parte di terzi apparentemente è già stato tappato, a detta del programmatore che ha presentato il <link>rimedio</link>http://kermit.solvare.com/fixbug01.dmg.gz. Ovviamente è tutto non ufficiale e chi lo scarica e lo installa lo fa a suo assoluto rischio e pericolo; ma appare chiaro che la soluzione non è impossibile e che molto probabilmente vedremo presto qualcosa di ufficiale da parte di Apple.

Mentre in casa Microsoft possono anche passare sei mesi prima che vengano chiusi bug critici, qui in una manciata di giorni si capisce tutto e probabilmente si risolve anche. Meglio stare qui.

<link>Lucio Bragagnolo</link>lux@mac.com