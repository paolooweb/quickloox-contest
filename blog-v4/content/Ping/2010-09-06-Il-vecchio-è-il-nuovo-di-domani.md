---
title: "Il vecchio è il nuovo di domani"
date: 2010-09-06
draft: false
tags: ["ping"]
---

In tutt'altre faccende affaccendato passo sulla stima di NetMarketShare della <a href="http://www.netmarketshare.com/operating-system-market-share.aspx?qprid=10" target="_blank">diffusione delle varie versioni di sistema operativo</a> in base al traffico web che generano.

Due conti e, in campo Windows, due macchine su tre sono Windows Xp. Roba uscita, credo, nel 2002. La terza macchina è distribuita in modo quasi equanime tra Vista e 7. Insomma, trionfa il vecchiume.

In campo Mac, Snow Leopard (il nuovo) fa cinque volte il traffico di Tiger (quello ancora più vecchio). Sempre Snow Leopard fa una volta e mezzo Leopard, il suo predecessore.

Sarà che un aggiornamento di sistema su Mac è una novità gradita e su Windows fa tremare le vene dei polsi?