---
title: "Note da esportazione"
date: 2006-02-18
draft: false
tags: ["ping"]
---

Grazie a <strong>Odino</strong>, che mentre si chiacchierava mi ha fatto scoprire come una delle Preferenze di Rubrica Indirizzi permetta di esportare in formato <em>vcard</em> anche le note.
