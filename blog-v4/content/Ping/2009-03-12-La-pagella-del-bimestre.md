---
title: "La pagella del bimestre"
date: 2009-03-12
draft: false
tags: ["ping"]
---

Vado a memoria, senza documentazione.

Negli ultimi due mesi dovrebbero essere stati rinnovati o modificati tra hardware e software MacBook Pro 15&#8221; (<em>speed bump</em> al processore), Mac mini (rifatto da capo a piedi), iMac, Mac Pro, iPod shuffle (stranuovo), Front Row, Remote, Apple Tv, iTunes, Time Capsule, AirPort Extreme.

Intanto procede lo sviluppo di Snow Leopard (due nuove <em>beta</em> nel 2009) e domani viene presentato ufficialmente il software 3.0 per iPhone con relativo Sdk.

Tra gli aggiornamenti ricordo almeno un Security Update, iLife '09, iWork '09, Java per Mac OS X e qualcosa d'altro ancora. Nel frattempo procede la messa a punto di Mac OS X 10.5.7 (due beta agli sviluppatori negli ultimi quindici giorni).

Ci sono aziende che su un elenco cos&#236; campano tre anni. Ricordo anche quelli delusi dagli annunci di Macworld Expo. Mancavano le novità.