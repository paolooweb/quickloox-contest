---
title: "Tra l'icona e il file il passo è (più) breve"
date: 2009-06-14
draft: false
tags: ["ping"]
---

Fino a che Apple non tradurrà in italiano la <a href="http://www.apple.com/it/macosx/refinements/enhancements-refinements.html" target="_blank">pagina contenente le novità di Snow Leopard</a> presenterò appunto in italiano una nuova funzione di Snow Leopard al giorno.

<b>Vista icone potenziata</b>

Proprio come in Cover Flow, si possono sfogliare le miniature di un documento a più pagine oppure vedere l'anteprima di un filmato QuickTime direttamente in una finestra del Finder impostata in Vista come icone.

C'è sempre meno bisogno di un doppio clic. Sembra poca roba e invece farà risparmiare grandi quantità di tempo, anche in vista di quello che succede alle icone di Snow Leopard. Cosa che vedremo domani.