---
title: "Non c&rsquo;è più da essere fiere<p>"
date: 2005-09-17
draft: false
tags: ["ping"]
---

Riflessioni al margine della costa atlantica<p>

Apple sta vivendo uno dei periodi migliori della sua storia, buoni prodotti, grandi risultati finanziari ma in particolare ottime idee (iPod nano è il migliore mai fatto finora). Eppure ha tolto da tempo il proprio appoggio al Macworld di Boston, il quale è stato infine cancellato.<p>

In tutta evidenza, le fiere non hanno niente a che vedere con lo stato di salute o il successo di Apple. E la cosa è vera da almeno dieci anni.<p>

Dedicato a chi si lamenta da anni che Apple non presenzia allo Smau, in compagnia dei produttori di macchine per timbrare la carta bollata, inchiostro per macchine per scrivere e controller taiwanesi per gli interruttori della luce in cantina.<p>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>