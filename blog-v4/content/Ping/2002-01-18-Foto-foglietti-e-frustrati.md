---
title: "iPhoto, foglietti e frustrati"
date: 2002-01-18
draft: false
tags: ["ping"]
---

Ovvero una storia di foglietti volanti e di come il Digital Lifestyle di Apple cambia la vita (informatica) rispetto alla triste alternativa

Cedo la parola a iRaf e a quanto ha recentemente pubblicato su MacCool. Ho apportato qualche taglio solo per dovere di sintesi.

“Dopo aver installato iPhoto sul mio PowerBook non ero rimasto molto soddisfatto, né impressionato.
Ieri arriva a casa un amico con un Acer 212T e mi mostra felice il regalo ricevuto a Natale: una Minolta Dimage303. Accende il notebook, scarica le foto sul disco (con foglietti che volano da una cartellina all’altra) e me le mostra.
‘Mi lasci provare con il mio?’ chiedo.
Detto fatto, attacco la macchina al PowerBook: iPhoto riconosce la macchina al volo, con l’amico alle spalle che guarda incuriosito: ‘Ah, ti dice pure di che modello si tratta!’
Io continuo, clicco Importa (per me è lentissimo) ma sento dietro di me (con un espressione un po’ più colorita): ‘Caspita, si vede l’anteprima’.
E con evidente malumore: ‘A me solo i foglietti volanti’.
Io clicco su Organizza e metto le foto fatte a Firenze nell’album vacanze: ‘Fooorte!’. Faccio partire lo slide show. Lo stupore cambia radicalmente in ammirazione: ‘CHE FIGATA!’.
In effetti le foto viste a tutto schermo con musica di sottofondo e in dissolvenza lasciano il mio amico quasi collassato sulla sedia.
Mi accontento di poco :-).
Ma intanto iPhoto si è guadagnato sul campo il suo spazio sul mio Mac”.

Tutta vita vera, non comunicati stampa. C’è di che meditare, su certe frustrazioni. Altrui.

lux@mac.com

<http://www.pegacity.it/maccool/>