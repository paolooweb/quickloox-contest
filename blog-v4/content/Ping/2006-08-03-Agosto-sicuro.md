---
title: "Agosto sicuro"
date: 2006-08-03
draft: false
tags: ["ping"]
---

Riavvio per <a href="http://docs.info.apple.com/article.html?artnum=304063" target="_blank">Security Update 2006-004</a>. Nessun problema e, essendo il riavvio voluto, il conteggio di quelli indesiderati resta a quattro, su 214 giorni, per un riavvio non voluto ogni cinquantatré giorni e mezzo.

Tra l'altro, un update bello nutrito.