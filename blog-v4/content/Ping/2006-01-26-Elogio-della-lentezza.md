---
title: "Elogio della lentezza"
date: 2006-01-26
draft: false
tags: ["ping"]
---

Ho riempito una cartella di masterizzazione e ho dato il comando Masterizza. Il Mac mi ha chiesto che nome dare al mio Dvd di backup e mi ha chiesto a che velocit&agrave; masterizzarlo. Proposte: 8x (pi&ugrave; veloce), 4x, 2x (pi&ugrave; affidabile).

Stavo per avviare la masterizzazione senza guardare. Poi ho pensato che questo &egrave; un computer Unix. Fa multitasking vero. Mentre masterizza posso fare qualunque altra cosa. Scrivo questa nota in pausa pranzo e anche se ci mettesse tutto il pomeriggio, non avrei alcun problema. &Egrave; un Dvd di backup e quindi mi aspetto di ritrovare intatti i miei dati quando dovessi andarli a cercare.

Indovina che velocit&agrave; ho scelto.