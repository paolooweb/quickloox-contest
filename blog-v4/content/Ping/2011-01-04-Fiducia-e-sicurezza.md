---
title: "Fiducia e sicurezza"
date: 2011-01-04
draft: false
tags: ["ping"]
---

Ieri sera ho acceso iPhone e sono andato su App Store, ma mi è stato comunicato che l'<i>account</i> <cite>non era disponibile</cite>.

Dopo qualche tentativo ho provato su Mac con MobileMe e l&#236; ho letto che l'accesso all'<i>account</i> era stato sospeso a causa di un gran numero di tentativi di ingresso.

Evidentemente qualcuno ha tentato ripetutamente di indovinare la <i>password</i> del mio identificativo Apple, senza riuscirci, e il sistema ha inibito l'accesso rendendosi conto che l'insistenza non era naturale.

Per prudenza ho approntato una nuova <i>password</i>, più lunga e robusta della precedente. Volendo non sarebbe neanche stato necessario, visto che la <i>password</i> precedente in realtà ha tenuto.

Non c'è da fidarsi delle cronache quando raccontano di <i>hacker</i> che <i>violano iTunes Store</i>; in realtà hanno solo azzeccato <i>password</i> deboli o sfortunate, quando non se le sono fatte dare direttamente da un proprietario sprovveduto.

Ci si può fidare di iTunes Store e bisogna potersi fidare della propria <i>password</i>. Ci sono diversi sistemi e uno dei migliori è memorizzare una frase, prenderne le iniziali e possibilmente, se è permesso, complicare la cosa. Per esempio partire da <i>Quattro mosche di velluto grigio, film di Dario Argento</i>, e arrivare con la password <i>4mdvg,fdDA</i>.

Un altro trucco interessante me lo ha rivelato l'amico Piero, ipovedente: localizza un certo tasto sulla tastiera e da l&#236; preme altri tasti in base a una sequenza facilmente memorizzabile (tipo due tasti a destra, due tasti in alto, due tasti a destra, due tasti in basso). Come <i>sf57j</i>, per dire. Si può digitare a occhi chiusi, affidando la memoria alle dita.

Se il proprio nome, o il nome di un amico, o di un attore, o il proprio indirizzo, o qualsiasi altra cosa sono anagrammabili, ne viene un'ottima <i>password</i>: <i>luciobragagnolo</i> è una pessima <i>password</i>, ma <i>barcolloagiugno</i> è già differente. Se poi aggiungo un numero, una maiuscola, l'anno in cui la mia squadra del cuore ha vinto il campionato più bello, qualche segno di interpunzione, viene <i>Barcollo-a-Giugno-2011</i> e voglio vedere chi la indovina. Eppure è facilmente memorizzabile. Per generare anagrammi in quantità esiste, per esempio, il <a href="http://anag.nightgaunt.org" target="_blank">Motore Anagrammatico del Gaunt</a>.

Facciamo un patto: nessuno di noi avrà mia più <i>password</i> banali e golose per un ladro telematico. Ok?