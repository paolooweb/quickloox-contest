---
title: "AppleScript: una scelta di calcolo"
date: 2008-10-31
draft: false
tags: ["ping"]
---

Lo so, lo so, sono in ritardo. La festa per i dieci anni di <a href="http://atworkgroup.net" target="_blank">Mac@Work</a> e per i dieci anni del <a href="http://www.poc.it" target="_blank">Poc</a> è costata molto e il resto della vita ne ha risentito. Però oggi potrò passare la giornata in Mac@Work a chiacchierare con chiunque e a tirare sera assieme a un sacco di amici e ciò, per scimmiottare la carta di credito, non ha prezzo.

Fatte le scuse non richieste, l'<a href="http://www.builderau.com.au/program/development/soa/Using-AppleScript-to-build-Mac-OS-X-applications/0,339024626,320283486,00.htm" target="_blank">AppleScript di oggi</a> è speciale nella sua inutilità apparente: permette di eseguire un calcolo dentro il <em>browser</em>, utilizzando le capacità di Google, via Script Editor. Naturalmente lo scopo non è fare più fatica del giusto, bens&#236; scoprire tecniche e metodi che poi servono in altre occasioni.

Buono per le scommesse (<em>scommettiamo che riesco a fare un calcolo dentro Google senza toccare il browser?</em> Con un amico rintronato da Windows, che queste cose se le sogna, si vince una cena a mani basse).

Lo script definisce una routine molto semplice e molto arzigogolata, cercaTrova. Il suo scopo è trasformare caratteri digitati dentro un campo di Script Editor in qualcosa di usabile dentro un Url personalizzato. cercaTrova è la seconda metà dello script.

La prima metà è il vero e proprio motore dello script. Per prima cosa si raccoglie l'<em>input</em> dell'utente e poi si costruisce un indirizzo web da somministrare a Google.

La metà-motore è interessante per le azioni che compie, la metà-elaborazione fa venire il mal di testa ma spiega molte cose su come AppleScript manipola il testo.

Senza porre altro tempo in mezzo, ecco lo script. Per chi vuole, ci si vede in Mac@Work. Non è questione di vendere o di comprare, solo di fare festa assieme. Una bella cosa. Ieri sono stato al <em>party</em> di <a href="http://www.sourcesense.it/it/home" target="_blank">Sourcesense</a> e rivedere Gianugo più un sacco di altre persone è stato molto piacevole. Anche oggi lo sarà.

<code>set calcolo to "
tell application "Finder"
	display dialog "Digita un'operazione: " default answer calcolo buttons {"OK"} default button 1
	set calcolo to text returned of the result
	set calcolo to (my cercaTrova(calcolo, "+", "%2B"))
	tell application "Safari"
		activate
		open location "http://www.google.com/search?&#38;q=" &#38; calcolo
	end tell
end tell

on cercaTrova(stringaOriginale, stringaCerca, stringaCambia)
	set vecchioDelimitatore to AppleScript's text item delimiters
	set AppleScript's text item delimiters to stringaCerca
	set stringaOriginale to text items of stringaOriginale
	set AppleScript's text item delimiters to stringaCambia
	set stringaOriginale to stringaOriginale as string
	set AppleScript's text item delimiters to vecchioDelimitatore
	return stringaOriginale
end cercaTrova</code>