---
title: "Nessuno regala niente"
date: 2009-11-12
draft: false
tags: ["ping"]
---

In questi giorni sto scrivendo varie mail, tutte dirette a software house e tutte con la stessa domanda: <i>come faccio a regalare a un amico un vostro software?</i>

La buona notizia è che tutti rispondono. La notizia discreta è che tutti hanno una soluzione abbastanza ragionevole.

La cattiva notizia è che nessuno prevede una opzione già pronta nella sezione di commercio elettronico del loro sito. Non credo che sia una cosa difficilissima da ottenere, stante che database e procedure di acquisto ci sono già.

Amazon ci ha pensato da secoli e dunque non manca neanche un esempio concreto.

Pensando alla fatica di riuscire a vendere regolarmente il software nonostante la pirateria totale che infuria, viene da pensare a occasioni mancate. E pure vendite.