---
title: "300+ novità: Finder"
date: 2007-10-27
draft: false
tags: ["ping"]
---

La barra laterale del Finder di Leopard, come dice la <a href="http://www.apple.com/it/macosx/features/300.html" target="_blank">pagina relativa del sito Apple</a>, ora raggruppa gli elementi in categorie (posizioni, apparecchi, computer condivisi e ricerche), un po' come iTunes (chi si ricorda i post sui programmi/sistemi operativi? Beh, qualcuno ha già chiamato Leopard l'<em>iTunes Os</em>).

Cover Flow è stranota e mostra i documenti proprio come iTunes (rieccolo) mostra le <em>cover</em> degli album.

Back to My Mac permette di raggiungere un Mac remoto via Internet da un altro Mac. Tutti e due devono avere Leopard. È possibile, praticamente con un clic, prendere il controllo dello schermo del Mac remoto.

Le icone sono miniature del contenuto effettivo dei documenti, non immagini generiche. Questa è una novità fondamentale.

Sembrerà poca cosa, ma - volendolo - si può mostrare nella parte inferiore della finestra del Finder il percorso (path) dei file selezionati nella finestra. Ci si possono anche trasportare file.

Le Opzioni vista si sono arricchite, per esempio con la regolazione della spaziatura della griglia su cui sono disposti i file.

Infine, finalmente, si può condividere al volo qualsiasi singola finestra del Finder, senza passare da una preferenza di sistema globale. Piccolo miglioramento, grande risultato.