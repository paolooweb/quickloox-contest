---
title: "Apriti, finestra!"
date: 2002-05-21
draft: false
tags: ["ping"]
---

A volte Mac OS X sembra testardo. Invece è solo giovane

Talvolta Mac OS X è un pizzico ostinato nelle sue opinioni di come dovrebbero essere impostate le finestre e, per quanto ci rispetti, fa un po’ di testa sua.
Bob LeVitus, uno dei Mac-guru americani, ha pubblicato su Osxfaq.com un bel <link>trucco</link> http://www.osxfaq.com/dailytips/04-2002/04-02.ws su come ridurre il Finder alla ragione e avere le finestre sempre dove e come le vogliamo.
Sono malfunzionamenti? Più che altro, peccati di gioventù. Man mano che Mac OS X matura, gli sforzi di Apple si concentrano sull’interfaccia utente e, per esempio, in Mac OS X 10.2 (previsto a settembre) tornerannno le cartelle a molla da molti rimpiante.
C’è quasi da scommettere che anche le finestre del Finder saranno più ubbidienti.

<link>Lucio Bragagnolo</link>lux@mac.com