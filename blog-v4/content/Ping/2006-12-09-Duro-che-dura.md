---
title: "Duro che dura"
date: 2006-12-09
draft: false
tags: ["ping"]
---

Titolo (e testo) di <strong>Stefano</strong>:

<cite>Dopo lungo e onorato servizio, l'Epson Stylus Color 760 ha &#8220;lasciato&#8221; il coetaneo iMac 400 DV SE di Gennaio 2000.</cite>

<cite>Potrò trovare un'altra compagna per un &#8220;duro&#8221; cos&#236; attempato e rimasto a Mac OS 9.2.2 e con la Ram standard di 128 MByte?</cite>

<cite>Sito Epson per vedere se ci sono modelli compatibili, poi ricerca su Internet, poi ordine on line ad una nota catena di negozi (<em>Expert</em>, che bello fare réclame quando sei contento del fornitore!) nella categoria <em>fine serie</em>. La vendevano anche altri, ma ho potuto scegliere fornitore e prezzo.</cite>

<cite>E ora l'iMac stampa felice sulla sua &#8220;nuova&#8221; Epson Stylus Photo R320, che potrò usare anche col MacIntel che mi fa l'occhiolino da dietro il vetro del rivenditore Apple.</cite>

<cite>Soldi spesi bene ieri (iMac 400 DV SE), oggi (R320) e domani (MacBook&#8230;).</cite>
