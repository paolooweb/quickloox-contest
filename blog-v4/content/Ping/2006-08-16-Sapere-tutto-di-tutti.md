---
title: "Sapere tutto di tutti"
date: 2006-08-16
draft: false
tags: ["ping"]
---

Per avere l'elenco di tutti i comandi Unix presenti nel sistema (poca utilità, grande curiosità), ero rimasto a <code>Ctr-x</code> seguito da <code>Ctrl-d</code>, che però non funziona in bash (bisogna digitare <code>tcsh</code> prima di tutto e, alla fine, digitare <code>exit</code> per ritornare in bash, shell preimpostata su Tiger).

Finalmente ho fatto progressi.

Tieni premuto il tasto Maiuscole e, nel mentre, digita <code>Esc-?</code>. Rispondi <code>y</code> ed ecco l'elenco. È diviso in schermate; la barra spaziatrice procede di schermata in schermata, mentre <code>q</code> termina l'agonia (sul mio Mac le <em>possibilities</em> sono 1.448).

In alternativa: digita <code>Ctrl-x</code> e poi <code>Maiuscolo-1</code> (modo analitico per descrivere un punto esclamativo, ma cos&#236; dovrebbe essere difficile confondersi anche se la tastiera non è Italiana Pro).