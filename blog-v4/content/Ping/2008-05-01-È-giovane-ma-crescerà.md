---
title: "È giovane, ma crescerà"
date: 2008-05-01
draft: false
tags: ["ping"]
---

Nella mia ricerca del successore di Tex-Edit Plus ho verificato <a href="http://www.artman21.com/en/jedit_x/" target="_blank">Jedit X</a>. Ancora insufficiente, ma nel giro di un paio di versioni potrebbe diventare interessante.

Fa quasi tutto quello che cerco. Il problema è che, per ora, lo fa non benissimo.

