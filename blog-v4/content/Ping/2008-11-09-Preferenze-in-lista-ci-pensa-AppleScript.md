---
title: "Preferenze in lista: ci pensa AppleScript"
date: 2008-11-09
draft: false
tags: ["ping"]
---

No, AppleScript non risolve questioni legate alle leggi elettorali. Fa altre cose, talvolta - mi si perdonerà la scorrettezza politica - maggiormente importanti, perché riguardano più la vita quotidiana e meno massimi e distantissimi sistemi.

AppleScript gestisce liste. Una lista è una sequenza di oggetti. In AppleScript standard, una lista è compresa tra parentesi graffe e gli elementi che la compongono sono separati da virgole. Gli elementi che non sono numeri sono compresi tra doppi apici.

Per esempio, questa è una lista AppleScript:

<code>{1, {3}, 4, "Cinque", 6, {"7", 8, 9}}</code>

Per lavorare con una lista la si assegna tipicamente a una variabile:

<code>set questaLista to {1, {3}, 4, "Cinque", 6, {"7", 8, 9}}</code>

Eccezionalmente, mi interrompo qui e pongo due domande, contemporaneamente facili e difficili:

1) da quanti elementi è composta la lista?
2) come inserire al secondo posto della lista l'elemento <code>"2"</code> mediante un comando AppleScript?

Non sembrino domande oziose: imparare a pensare in termini da programmatore aiuta molto la programmazione.

In ogni caso, arriverà risposta.