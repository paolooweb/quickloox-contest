---
title: "Pro musica in rete"
date: 2003-06-10
draft: false
tags: ["ping"]
---

I dolori della giovane Apple nel mantenere l’accordo con la major della musica

Grazie all’integrazione tra iTunes, iPod e AppleMusic, Steve Jobs e compagni hanno messo in piedi una splendida soluzione per il godimento individuale della musica.

Però le paure e i diktat delle case discografiche (se c’è qualcosa di arretrato al mondo, beh, è un mercato ormai tutto digitale dove chi vende non capisce come funziona il digitale) costringono Apple a usare i piedi di piombo e limare di continuo l’offerta, per non impensierire eccessivamente i suoi (arretrati) partner.

Così iTunes 4.0 permetteva di condividere in rete locale la musica presente sul proprio computer; iTunes 4.0.1 no.

Poco male. Nella comunità del software libero c’è chi ci sta <link>lavorando</link>http://sourceforge.net/projects/icommune.<link>

Lucio Bragagnolo</link>lux@mac.com