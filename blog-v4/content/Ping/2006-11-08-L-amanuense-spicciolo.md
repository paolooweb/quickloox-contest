---
title: "L'amanuense spicciolo"
date: 2006-11-08
draft: false
tags: ["ping"]
---

Quando si fa sentire <strong>Jacopo</strong> è sempre per qualcosa di speciale. Stavolta è per <a href="http://www.popcopy.net/" target="_blank">PopCopy</a>, un piccolo monaco amanuense che sta l&#236; e, come i benedettini durante i Secoli Bui, copia, copia e copia il passato salvandolo per i posteri.

Questo monacello tuttavia si accontenta di molto meno; copia e salva quello che passa dalla Clipboard, un elemento dietro l'altro, e permette di utilizzarlo.

Non salva la civiltà dei secoli passati per i secoli futuri, ma aiuta un bel po' il lavoro dei prossimi quindici minuti. Lo compro.