---
title: "Diamoci pure del tool<p>"
date: 2005-06-18
draft: false
tags: ["ping"]
---

Finalmente del software di personalizzazione, anche se nessuno lo userà<p>

Uno dei miti di Mac è che è “fatto per la grafica” (come se chi scrive, per dire, lo dovesse schifare). Un altro mito è che “non ci sono giochi” (non ce ne sono per cambiarne uno ogni tre giorni per vent’anni, vero).<p>

A smitizzare il secondo arriva la notizia della pubblicazione dello <a href="http://www.blizzard.com/support/wow/?id=aww01671p">User Interface Customization Tool per World of Warcraft</a>. WoW è uno dei giochi sulla cresta dell’onda e, guarda caso, non solo viene venduto a parità di funzioni e a parità di uscita di release tra Mac e Windows; non solo gli ingegneri di Blizzard, che lo produce, erano presenti in forze alla Wwdc Apple dei giorni scorsi; adesso esce per Mac, insieme a quello per Windows, il kit per personalizzare l’interfaccia utente del gioco.<p>

È una cosa che anni fa sarebbe stata totalmente impensabile e un segnale forte che molte cose stanno cambiando, in bene, per Mac.<p>

Anche se il kit non lo userà nessuno. Richiede buona conoscenza di Xml e del linguaggio di scripting Lua e non viene supportato in alcun modo da Blizzard. Chi ha World of Warcraft preferirà giocarlo che pasticciare rischiando di rovinare tutto. Ma la presenza del kit per Mac è, ripeto, grande buona notizia.<p>

<a href="mailto:ithilgalad.the.rogue@gmail.com">Ithilgalad</a>