---
title: "Il mio nuovo Time Machine"
date: 2009-03-07
draft: false
tags: ["ping"]
---

Il vecchio disco di backup da 160 gigabyte non è più adeguato per un <em>hard disk</em> interno da 320 gigabyte. Cos&#236; ho acquistato contestualmente al MacBook Pro 17&#8221; un <em>hard disk</em> esterno portatile <a href="http://www.wdc.com/en/products/Products.asp?DriveID=569" target="_blank">My Passport Studio di Western Digital</a>, da 500 gigabyte.

L'ho pagato circa 150 euro, un po' più del minimo assoluto reperibile sul mercato per un prodotto di questo tipo. I motivi sono essenzialmente un buon <em>design</em> e la presenza di una interfaccia FireWire 800.

Fisicamente il disco ha un po' l'aspetto del libriccino, spesso come il portatile, con una &#8220;copertina&#8221; in alluminio satinato e le &#8220;pagine&#8221; in plastica nera lucida. Spessore ovviamente a parte, un centimetro in meno in larghezza e un paio in altezza e potrebbe rivaleggiare con un iPhone in ingombro. È portabilissimo e nella confezione si trova anche una sacchetta apposta.

Nella confezione si trovano anche un cavo FireWire 800/800, un cavo FireWire 400/800 e un cavo Usb/miniUsb. Il disco ha una presa FireWire 800 e la presa miniUsb e dunque, con il cavo giusto, può essere usato da qualsiasi Mac. Però può formare sempre e solo l'ultimo anello di una catena FireWire, avendo una presa sola.

Le due prese sono nascoste da una copertura scorrevole, che le protegge durante l'inutilizzo. Sulla copertura appaiono quattro Led che mostrano lo stato di funzionamento del disco ed eventualmente lo spazio occupato dai dati.

Il disco è preformattato Mac, quindi pronto all'uso appena tirato fuori dalla scatola, e Western Digital lo garantisce per cinque anni. L'ho registrato sul sito dell'azienda; il numero seriale è chiaramente riportato.

All'interno della confezione si trovano un paio di fogli di documentazione e garanzia. Tutto è replicato come file Pdf all'interno del disco, che contiene anche un Wd Drive Manager e un Turbo Driver per Mac.

Nessuno dei due programmi è necessario per usare il disco. Volendo, Wd Drive Manager installa un menu grafico nella barra di Mac OS X, dal quale si evince al volo il livello di riempimento del disco. Lo stesso Wd Drive Manager abilita la segnalazione del livello di riempimento mediante i Led del disco. Tutto sommato è un tocco gradevole e sostanzialmente inutile, che sparisce a disco scollegato e riappare a disco collegato, senza molte altre possibilità. Nel caso desse fastidio, per toglierlo si lancia nuovamente l'applicazione, per dire Remove anziché Install.

Il Turbo Driver dovrebbe velocizzare i trasferimenti di file rispetto al normale driver standard di Mac OS X. Sinceramente, dal momento che il disco mi servirà per Time Machine, ho pochissimo interesse per l'eventuale aumento di velocità, che suppongo sarà anche ininfluente. Ho provato a installare comunque il driver, per vedere se almeno funziona.

Sta funzionando o cos&#236; pare. Visto che per ora lo spazio a disposizione è ingente, ho cambiato strategia per impostare un backup Time Machine standard, cioè integrale (sul disco precedente facevo solo l'essenziale) e vediamo come andrà. Il <em>backup</em> iniziale interessa 1,2 milioni di file, per circa 111 gigabyte, e sta procedendo or ora con l'interfaccia FireWire 800. Molto a naso sono stati trasferiti sessanta gigabyte in un paio d'ore e dunque in un altro paio d'ore il backup di base dovrebbe essere ultimato.

L'insieme è essenziale e si usa appena tirato fuori il disco dalla scatola (un po' plasticosa e difficile da aprire). Il disco è autoalimentato, silenziosissimo (se lo si prende in mano durante il funzionamento si avverte una leggera vibrazione) e discreto. Il <em>feeling</em> tattile è buono. Sulla superficie di appoggio ci sono quattro piedini credo in silicone e direi che ho detto veramente tutto.

Complessivamente un buon prodotto, che tra l'altro risolve il problema del vecchio disco FireWire 400 a contatto con un nuovo Mac FireWire 800; il cavetto ibrido 400/800 è subito finito nello zaino, tornasse utile nel mondo esterno.

Un buon prodotto e, visto l'uso, che resti inutile il più a lungo possibile.