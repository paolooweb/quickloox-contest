---
title: "Previsioni del giorno dopo / 4"
date: 2009-03-20
draft: false
tags: ["ping"]
---

SpiralFrog ha chiuso. Non c'è neanche più un link da dare.

Aveva aperto facendo sensazione nel 2006, con l'idea di offrire musica gratis grazie alla pubblicità sul sito. Il New York Times, Reuters, Usa Today e altri avevano ipotizzato che fosse una minaccia per iTunes Store.

Samsung Movies ha aperto. È partita, solo in Inghilterra e Germania per ora, la beta di <a href="http://movies.uk.samsungmobile.com/" target="_blank">un negozio di film online</a>, per ora compatibile solo con i cellulari Samsung abilitati al video, che venderà appunto film per cellulari, con l'idea di estendere gradualmente il servizio a tutta Europa e di arrivare a servire portatili, lettori digitali tascabili, televisori Samsung.

Cnet <a href="http://news.cnet.com/8301-1023_3-10200819-93.html?part=rss&amp;subj=news&amp;tag=2547-1_3-0-20" target="_blank">ha già iniziato a chiamarlo</a> <em>rivale di iTunes</em>.

La previsione è già fatta.