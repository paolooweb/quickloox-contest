---
title: "Facciamolo riconoscere"
date: 2007-08-07
draft: false
tags: ["ping"]
---

Per chi chiedeva lumi su gocr, il software Unix di riconoscimento ottico dei caratteri, e in completa irreperibilità di Pierfausto, inizio a rimandare al link di <a href="http://gocr.darwinports.com/" target="_blank">gocr su Darwinports</a>. Nel mio post originale linkavo <a href="http://jocr.sourceforge.net/" target="_blank">Sourceforge</a>, dove sta il sito ufficiale che è però di meno aiuto.

Non mi sono ancora messo a tentare installazioni. Chi provasse e avesse voglia di condividere l'esperienza, scriva che c'è spazio. :-)