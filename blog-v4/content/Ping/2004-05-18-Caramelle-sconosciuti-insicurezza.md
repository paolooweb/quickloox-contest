---
title: "Finalmente un vero buco"
date: 2004-05-18
draft: false
tags: ["ping"]
---

Niente è a rischio, se non la salute degli imprudenti e degli sprovveduti

Nessun sistema operativo è inviolabile.
Non bisogna accettare caramelle (o peggio) dagli sconosciuti.

Bastano queste due nozioni a rendere comprensibile che si possa scoprire una falla in Mac OS X che permette a un aggressore di prendere parziale possesso di un sistema altrui, come dimostra questo <link>scriptino</link>http://www.free-go.net/insecure/safari/0x04_test.html, di suo assolutamente innocuo.

Ora la natura seguirà il suo corso. Apple pubblicherà un Security Update, la falla verrà chiusa e nessuno avrà subito realmente danni.

Solo gli imprudenti e gli ingenui continueranno, come hanno sempre fatto, a scaricare qualunque cosa arrivi, senza alcuna cautela. Considerati gli sconosciuti e le caramelle, ci si chiede come siano riusciti a passare i sei anni di età.

<link>Lucio Bragagnolo</link>lux@mac.com