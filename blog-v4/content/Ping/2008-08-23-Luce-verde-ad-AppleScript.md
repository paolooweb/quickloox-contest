---
title: "Luce verde ad AppleScript"
date: 2008-08-23
draft: false
tags: ["ping"]
---

Quei tre pulsanti in alto a sinistra in tutte le finestre del Finder. Il pulsante rosso ha la scorciatoia Comando-W. Il pulsante giallo, Comando-M. E il pulsante verde?

Certe volte una scorciatoia di tastiera per il pulsante verde farebbe proprio comodo. Fortunatamente abbiamo AppleScript.

<code>tell application "System Events"
	if UI elements enabled then
		set FrontApplication to (get name of every process whose frontmost is true) as string
		-- funziona con la finestra di qualsiasi applicazione, purché in primo piano. Con questa istruzione imposta la variabile inserendovi il nome del processo rilevante
		tell process FrontApplication
			click button 2 of window 1
			-- button 2 è il pulsante verde
			-- window 1 è sempre la finestra in primo piano
		end tell
	else
	-- se qualcosa è andato storto&#8230;
		tell application "System Preferences"
			activate
			set current pane to pane "com.apple.preference.universalaccess"
			display dialog "Lo scripting dell'interfaccia utente non è abilitato. Per favore, seleziona 'Abilita l'accesso a dispositivi d'assistenza'"
		end tell
	end if
end tell</code>

L'ultima cosa che manca è applicare una scorciatoia di tastiera allo script. Se ne era già parlato in passato; una delle soluzioni resta <a href="http://blacktree.com/?quicksilver" target="_blank">QuickSilver</a>.

Lo script qui fornito è cortesia di <a href="http://snippets.dzone.com/tag/applescript" target="_blank">DZone</a>. Nel lavorarci sopra, ho scoperto un po' di <a href="http://www.vtc.com/products/AppleScript-Studio-tutorials.htm" target="_blank">filmati introduttivi</a> gratuiti dedicati ad AppleScript Studio. Per provare a fare un passetto in più sono un buon pretesto.