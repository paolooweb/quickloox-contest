---
title: "Un alfabeto dimenticato"
date: 2008-06-18
draft: false
tags: ["ping"]
---

C'è del genio - anche un po' sadico - in una <a href="http://support.apple.com/kb/HT1221" target="_blank">nota tecnica</a> che avvisa come un MacBook Air problematico potrebbe svegliarsi senza fare partire Mac OS X e invece emettere nove cicalini. Tre corti, tre lunghi e tre corti.