---
title: "Rudimenti e aperture<p>"
date: 2005-01-29
draft: false
tags: ["ping"]
---

Consiglio gratuito scomodo e financo non richiesto<p>

Mi chiede <em>ma perché dovrei imparare a programmare</em>?<p>

Non c&rsquo;è nessun bisogno di imparare a programmare. Ma aprire il cervello sì. Il Terminale. AppleScript. Qualche linguaggio facile come Python, o potente e con un sacco di mattoncini già pronti come Perl. Nessuno ti chiede di imparare. Ma provarci, e capire almeno i rudimenti, sì. Sarà meglio di mille corsi improvvisati e anche qualche master ben più costoso.<p>

Lui è un pilota e mi spiegava come è importante saper fare le cose giuste sul ghiaccio, o sulla sabbia. <em>Non ti serve mai</em>, diceva, <em>ma quella volta che ti capita te la cavi alla grande, e in condizioni normali guidi con un sacco di sicurezza in più</em>.<p>

Appunto.<p>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>