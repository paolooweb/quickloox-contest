---
title: "Magia bianca"
date: 2009-12-27
draft: false
tags: ["ping"]
---

Primissimo impatto con il Magic Mouse pervenuto a Natale.

Configurazione zero: attivato l'assistente di configurazione Bluetooth a mouse acceso, lo ha riconosciuto e accoppiato al sistema. Va di lusso perché ho Snow Leopard 10.6.2, che incorpora il software specifico per Magic Mouse; altrimenti tocca scaricare un aggiornamento software, in ogni caso niente di complesso, dal momento che arriva in automatico.

Da un punto di vista costruttivo l'oggetto è molto più robusto e ispira più fiducia rispetto a un Mighty Mouse. La parte scorrevole sul piano di appoggio non è più un'ellisse bens&#236; un binario e probabilmente intercetta meno sporco, anche se potrò verificarlo solo nel tempo. Il telaio, metallico, è un gran progresso rispetto alla plastica passata.

Sul consumo non posso dire ancora niente, ovviamente.

Le <i>gesture</i> (le azioni <i>touch</i>, a sfioramento, sulla superficie del mouse) standard nella configurazione Apple sono poche e funzionanti. Abituato a scorrere verticalmente passando due dita sul <i>trackpad</i> sono rimasto un po' disorientato nel vedere la funzione descritta con un dito solo per Magic Mouse. Per fortuna funziona anche con due dita. Lo scorrimento orizzontale con due dita è una meraviglia con iPhoto. Mi fa un po' paura che faccia avanti e indietro sulla finestra di Safari. Per ora nessun incidente, staremo a vedere.

Il <i>feeling</i> del mouse mi piace. Ho letto molte critiche sulla eccessiva scivolosità, sulla difficoltà di tenerlo in mano e altro. Non sono testimone attendibile, perché preferisco i mouse piccoli di Apple a quelli grandi. Credo di essere l'unico sul pianeta Terra ad avere apprezzato il mouse allegato al primo iMac, che a mio parere se tenuto con la punta delle dita era ottimo.

Da questo punto di vista, il profilo più basso di Magic Mouse e la forma filante mi piacciono molto.

Primo giudizio: chi si aspettava una rivoluzione è rimasto giustamente deluso, ma perché aspettarsi una rivoluzione? Un mouse è un mouse e il <i>touch</i> innova in un solo punto: fa piazza pulita di rotelline, palline, pulsantini e altra roba. Se devo muovere una critica, anzi, è che mi piacerebbe vedere la possibilità di fare clic senza fare clic, semplicemente toccando il mouse, proprio come con il <i>trackpad</i>. Come prima impressione, consiglio certamente il cambio da Mighty Mouse a Magic Mouse.

Unica controindicazione reale: rispetto a un Mighty Mouse, Magic Mouse perde pulsanti. Chi si era configurato un utilizzo complesso di Mighty Mouse piuttosto guarderà a mouse più ricchi di funzioni. Magic Mouse, in pieno stile Apple attuale, semplifica.