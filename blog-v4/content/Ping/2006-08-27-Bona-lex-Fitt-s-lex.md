---
title: "Bona lex, Fitts lex"
date: 2006-08-27
draft: false
tags: ["ping"]
---

Un motivo per cui l'interfaccia di Mac è superiore a quella di Windows: la barra dei menu sta in cima allo schermo. Su Windows viaggia insieme alle finestre ed è molto più difficile raggiungerla.

Si enuncia in una riga, ma per spiegarla ci vuole un dottorato. Ecco perché un mare di ignoranti windowsiani snobba la <a href="http://en.wikipedia.org/wiki/Fitt%27s_law" target="_blank">legge di Fitts</a>.