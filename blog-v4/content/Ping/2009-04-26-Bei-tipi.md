---
title: "Bei tipi"
date: 2009-04-26
draft: false
tags: ["ping"]
---

Mi sono accorto or ora che Typografica si è rifatto il <em>look</em> e, forse forse, ora iniziano anche a pubblicare più spesso.

Intanto hanno pubblicato la loro scelta dei <a href="http://new.typographica.org/2009/features/our-favorite-typefaces-of-2008/" target="_blank">migliori font del 2008</a> e c'è bellezza sparsa a piene mani.

Speriamo che continuino, in un mondo dove i tipi che girano finiscono per essere sempre i soliti.