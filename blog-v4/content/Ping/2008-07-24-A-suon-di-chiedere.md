---
title: "A suon di chiedere"
date: 2008-07-24
draft: false
tags: ["ping"]
---

Sono quasi <a href="http://www.tuaw.com/2008/07/23/a-mac-tablet-not-just-yes-but-heck-yes/" target="_blank">d'accordo con Erica</a>. Al punto che ne traduco un frammento:

<cite>L'annuncio dei risultati finanziari Apple del terzo trimestre potrebbe avere alluso a un prossimo arrivo di un Mac</cite> tablet<cite>, o forse no, ma non è importante. Il Mac</cite> tablet <cite>non è più un</cite> rumor<cite> da tempo. Ce l'ho qui in tasca. Se iPod touch non è un Mac</cite> tablet<cite>, non so che cosa sia. Ha una interfaccia totalmente tattile. Mac OS X più interfaccia tattile uguale Mac </cite>tablet<cite>, da dovunque la si guardi.</cite>

<cite>Il mondo del</cite> computing<cite> sta cambiando. Non siamo più legati ai desktop. Ci spostiamo e portiamo i nostri calcolatori con noi. Avere un computer sempre a portata di mano non è solo una bella idea, ma è anche pratico.</cite>

Mi sono permesso di usare la dizione <em>iPod touch</em> al posto del prodotto di cui Erica scrive nel brano originale. Il significato è esattamente lo stesso e questo dovrebbe fare riflettere sulla reale natura di quel prodotto.