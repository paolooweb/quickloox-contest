---
title: "Ora di supplenza"
date: 2009-06-14
draft: false
tags: ["ping"]
---

Come era già successo per Leopard, Apple ha pubblicato una pagina che riassume <a href="http://www.apple.com/it/macosx/refinements/enhancements-refinements.html" target="_blank">le modifiche significative di Snow Leopard</a>, ma non si è ancora degnata di tradurla in italiano.

Lo faccio io, una modifica al giorno. Sono un centinaio (ricordarsi, c'è sempre qualcuno che dice <cite>è solo un aggiornamento</cite>); se Apple non si sveglia prima, arriveremo all'ultima per fine settembre, più o meno quando potrebbe esserci il software disponibile.

La prima è questa.

<b>Finder riscritto per Snow Leopard</b>

Il Finder è stato completamente riscritto nell'ambiente di programmazione Cocoa perché si avvantaggi di tutte le nuove tecnologie presenti in Snow Leopard, come il supporto per i 64 bit e Grand Central Dispatch, che coordina il lavoro sulle macchine multiprocessore e <i>multicore</i>. Il nuovo Finder è ovunque più reattivo e veloce.

Apple ha dichiarato che Snow Leopard è <cite>un Leopard rifinito</cite> e il nuovo Finder si attaglia perfettamente alla definizione. La riscrittura in Cocoa era attesa da lungo tempo e la maggiore velocità è una piacevole conseguenza. Adesso c'è la base per ulteriori future modifiche all'interfaccia.