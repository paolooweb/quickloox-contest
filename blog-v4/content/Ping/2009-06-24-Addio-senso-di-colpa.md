---
title: "Addio senso di colpa"
date: 2009-06-24
draft: false
tags: ["ping"]
---

Sul sito Microsoft si trovano le <a href="http://windowshelp.microsoft.com/Windows/en-US/help/2e680b8d-211e-41c5-a0bf-9ccc6d7e62a21033.mspx" target="_blank">istruzioni per aprire la scatola</a> di Windows Vista.

Su Windows Blog si parla della nuova scatola che avrà Windows 7, <a href="http://windowsteamblog.com/blogs/windows7/archive/2009/06/23/check-out-the-new-windows-7-packaging.aspx" target="_blank">semplice, pulita e facile da aprire</a>.

E ogni scatola avrà un colore diverso, per distinguere le varie versioni di Windows 7 (credo siano cinque o sei).

Ho sempre praticato il vizio nascosto e solitario di aprire un pezzo per volta le scatole Apple e restare ammirato.

Se mai avessi provato un vago senso di colpa, ora è proprio passato.