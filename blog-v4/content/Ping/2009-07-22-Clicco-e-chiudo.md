---
title: "Clicco e chiudo"
date: 2009-07-22
draft: false
tags: ["ping"]
---

Con il <a href="http://www.apple.com/pr/library/2009/07/21results.html" target="_blank">fatturato record dell'ultimo trimestre</a>, Apple potrebbe benissimo degnarsi di tradurre in italiano la pagina delle <a href="http://www.apple.com/it/macosx/refinements/enhancements-refinements.html" target="_blank">novità di Snow Leopard</a>. Non importa, lo faccio io una novità al giorno e gratis. D'altronde non c'è ancora il comunicato stampa italiano dei risultati e devo linkare quello inglese.

<b>Chiudere le altre chat con un clic</b>

Se ci sono aperte più chat, si possono chiudere tutte le finestre eccetto quella attiva, con un Ctrl-clic sulla finestra attiva e scegliendo Chiudi le altre chat dal menu.

Possibilmente iniziando a digitare <i>ti ricevo male&#8230; non c'è campo&#8230; sto entrando in galleria&#8230;</i>