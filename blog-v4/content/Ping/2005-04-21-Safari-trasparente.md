---
title: "Spesso o trasparente?<p>"
date: 2005-04-21
draft: false
tags: ["ping"]
---

Continuano a inserire funzioni da testare nel menu di debug di Safari<p>

Il menu di debug di Safari contiene spesso funzioni che servono per il collaudo del programma, oppure provvisorie in attesa di diventare definitive. Per esempio, la navigazione a pannelli è stata per un po&rsquo; nel menu di debug prima di essere ufficializzata.<p>

Per abilitare il menu di debug bisogna scrivere nel Terminale, a Safari chiuso,<p>

<code>defaults write com.apple.safari IncludeDebugMenu 1</code><p>

(per toglierlo, stessa cosa con lo zero al posto dell&rsquo;uno)<p>

Nel nuovo Safari 1.3, introdotto dall&rsquo;aggiornamento a 10.3.9, il menu di debug contiene anche la possibilità di avere una pagina trasparente. Una pagina vuota; se si carica qualcosa, Safari ridiventa opaco.<p>

Però vederlo è interessante e chissà che un domani non diventi una funzione quasi utile. Guardo una pagina e intanto, in trasparenza, vedo se ho già scaricato quel file eccetera.<p>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>