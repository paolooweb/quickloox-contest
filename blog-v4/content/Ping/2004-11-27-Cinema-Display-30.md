---
title: "Che cosa posso vendere<p>"
date: 2004-11-27
draft: false
tags: ["ping"]
---

Di solito tendo a esagerare quando descrivo la nuova tecnologia. Stavolta no<p>

Il primo Macintosh fu uno schock culturale. Sparigliava tutti gli schemi mentali e, pur costando un sacco di soldi, spiegava chiaramente che il mondo era cambiato, per sempre.<p>

Ho visto e provato il Cinema Display di Apple da trenta pollici. Esattamente la  stessa sensazione. Non esiste descrizione che tenga, bisogna vederlo dal vivo, toccarlo, usarlo, e arriva lo shock culturale.<p>

Di fianco a me un signore osservava attonito, commentando <cite>purtroppo non lo posso comprare</cite>.<p>

<cite>Neanch&rsquo;io lo posso comprare</cite>, ho risposto. <cite>Ma sto già pensando a che cosa posso vendere</cite>.<p>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>