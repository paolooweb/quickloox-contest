---
title: "Stupido è chi lo stupido non fa"
date: 2007-12-01
draft: false
tags: ["ping"]
---

Si avvicina Natale e dedicare almeno qualche istante a giochi di nessun impegno e grande divertimento è di rigore.

Cos&#236; mi è venuto in mente <a href="http://www.gamehouse.com/gamedetails/?game=monoployc&amp;navpage=downloadgames" target="_blank">Monopoly</a>&#8230; e che il grande Enrico Colombini aveva anche ideato lo splendido <a href="http://www.erix.it/retro.html#mac" target="_blank">Melopoli</a>, al tempo bellissimo software per Mac, capace di restituire lo spirito del tabellone originale su uno schermino monocromatico da nove pollici.

Viva i giochi per Mac, viva il Natale e viva anche Enrico. :-)