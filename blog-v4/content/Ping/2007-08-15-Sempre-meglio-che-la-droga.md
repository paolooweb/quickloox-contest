---
title: "Sempre meglio che la droga"
date: 2007-08-15
draft: false
tags: ["ping"]
---

Che cosa fai in una notte d'estate, quando ti sei tolto veramente tutti gli sfizi e sei sazio, reduce da una nuotata e con tutti gli amici a dormire? Curiosi su <a href="http://osx.freshmeat.net" target="_blank">Freshmeat</a>.

Ti accorgi che sta nascendo <a href="http://emhsoft.com/singularity/index.html" target="_blank">questo gioco</a> in cui interpreti una intelligenza artificiale che per sopravvivere gioca sporco e si trasferisce da un computer all'altro prima di essere scoperta e cancellata. Intrigante.

Il gioco è in Python. Beh, allora? Lo scarichi e non parte. Vuole <a href="http://www.pygame.org/news.html" target="_blank">pygame</a>. Ok. Installi pygame. Si blocca. Vuole Sdl (c'è perché l'hai installato chissà quando per chissà che cos'altro), Sdl_ttf (c'è, vedi sopra), Sdl_image (c'è, come già detto), <a href="http://www.libsdl.org/projects/SDL_mixer" target="_blank">Sdl_mixer</a> (non c'è), smpeg (non c'è), Numeric (non c'è) e PyObjC (non c'è).

Trovi <a href="http://pythonmac.org/packages/py25-fat/index.html" target="_blank">PyObjC</a>, <a href="http://pythonmac.org/packages/py25-fat/index.html" target="_blank">Numeric</a> e Sdl_mixer, in forma di immagine disco o di package pronto da montare. Ma smpeg, non c'è verso. O meglio, trovi solo <a href="http://svn.icculus.org/smpeg/" target="_blank">l'interfaccia a Subversion</a> e non hai questa gran voglia di scoprire come compilare tutto. <a href="http://smpeg.darwinports.com/" target="_blank">DarwinPorts</a> per qualche motivo non ti ispira. <a href="http://inquirylabs.com/blog2005/?p=21" target="_blank">Cvs</a> men che meno. Allora chiedi aiuto a Fink e Fink ce l'ha.

Fink macina e installa, ma non installa nel sistema il framework che pygame vuole. Però la compilazione di pygame parte lo stesso, chissà come va a finire.

Alla fine Endgame:Singularity funziona. È un gestionale carino, stile Defcon, seppure decisamente open source e 0.26. D'altronde è l'ambientazione che ti ha colpito.

Un attimo prima di andare a dormire, sbagli un clic e capiti su un blog dove spiegano, in modo <em>semplicissimo</em>, come <a href="http://girasoli.org/?p=73" target="_blank">installare Daimonin</a> su Mac OS X 10.3.9, con link a 10.4. Inizi a leggere, provare, la notte passa veloce.

E non si dica che i programmi basati su Python e/o Unix non danno&#8230; dipendenze.