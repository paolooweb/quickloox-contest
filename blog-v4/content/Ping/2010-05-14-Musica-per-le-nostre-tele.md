---
title: "Musica per le nostre tele"
date: 2010-05-14
draft: false
tags: ["ping"]
---

Su Slashdot <a href="http://tech.slashdot.org/story/10/05/14/066220/Beautifully-Rendered-Music-Notation-With-HTML5?from=rss&amp;utm_source=feedburner&amp;utm_medium=feed&amp;utm_campaign=Feed%3A+Slashdot%2Fslashdot+%28Slashdot%29" target="_blank">lo hanno definito</a> <cite>incredibile</cite> e l&#236; gli aggettivi si usano di rado.

Mohit Muthanna <a href="http://0xfe.blogspot.com/2010/05/music-notation-with-html5-canvas.html" target="_blank">ha creato la notazione musicale standard</a> usando esclusivamente JavaScript, da usare nell'elemento Canvas di Html5.

Sono visibili anche due <a href="http://0xfe.muthanna.com/jsnotation/demo.html" target="_blank">dimostrazioni di spartito</a>.

Per i curiosi e i programmatori, i commenti di Slashdot riportano anche l'indirizzo del <a href="http://0xfe.muthanna.com/jsnotation/vexnotation.js" target="_blank">codice JavaScript</a>. A disposizione di tutti, qui, ora.

La discussione non è più <i>Flash s&#236;, Flash no</i>. È moltiplicare le opportunità messe a disposizione dal web. E la strada di offrire direttamente al <i>browser</i> e ai programmatori nuove possibilità è intrinsecamente superiore.