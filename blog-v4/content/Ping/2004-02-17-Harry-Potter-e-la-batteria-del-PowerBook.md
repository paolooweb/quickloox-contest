---
title: "Harry Potter e la batteria del PowerBook"
date: 2004-02-17
draft: false
tags: ["ping"]
---

I computer diventeranno oggetto di consumo sempre troppo tardi

Non conosco nessuno che, avendo acquistato un’auto con tre anni di garanzia, si stupisca dell’usura dei penumatici durante quei tre anni e ne chieda la sostituzione in quanto usurati.

Conosco un sacco di gente che compra un PowerBook, si stupisce dell’usura della batteria e arriva a chiederne la sostituzione non perché la batteria si consuma più in fretta del dovuto (prodotto difettoso e sostituzione sacrosanta), ma semplicemente perché la batteria non offre in eterno le stesse prestazioni.

Come se le batterie le fabbricassero alla scuola di Harry Potter.

<link>Lucio Bragagnolo</link>lux@mac.com