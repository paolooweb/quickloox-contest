---
title: "Quando parla il gregge"
date: 2008-05-23
draft: false
tags: ["ping"]
---

I programmi che mancano. È uscito anche <a href="http://voicechatter.org/mambo/" target="_blank">VoiceChatter</a>, audioconferenza per Windows, Mac e Linux con taglio particolare per comunicare a voce mentre si gioca. Ma naturalmente una audioconferenza è una audioconferenza.

Non è che quelli su Msn siano una maggioranza. Sono un gregge. Quando parlano e pretendono, oramai a metterli al loro posto basta un cane.