---
title: "Lo studio di montaggio non fa il montaggista"
date: 2002-02-01
draft: false
tags: ["ping"]
---

Apple vende tecnologia così deliziosa da far dimenticare che sarebbe meglio disporre anche di capacità professionali proprie...

Sono stato a un’eccezionale pomeriggio made in Apple dedicato a “Real World Fx”, per noi poveri mortali il mondo del montaggio video e degli effetti speciali.
Si sono visti programmi eccezionali come After Effects di Adobe e Combustion di Discreet (non sono esperto ma, assai a naso, senza Mac OS X non lo si sarebbe mai visto su un Mac).
Soprattutto si è visto come basti un cavetto FireWire a trasformare un iBook o un Titanium in un perfetto studio di editing digitale per artisti e professionisti della tv e del cinema del nuovo millennio. Ho guardato il mio già vecchio ma fantastico Titanium d’occasione e ho capito che l’hardware c’è; adesso qualcuno dovrebbe “solo” insegnarmi a fare il montaggista.
Giusto per ricordare che Apple fa bellissimo hardware, ma le competenze sono tuttora affidate a processi di sviluppo più artigianali.

lux@mac.com