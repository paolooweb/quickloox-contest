---
title: "Fatti, con parole"
date: 2008-04-04
draft: false
tags: ["ping"]
---

Giusto per ricordare che poco più di tre anni fa l'allora l'amministratore delegato di Dell Kevin Rollins <a href="http://www.news.com/2100-1042_3-5540063.html" target="_blank">definiva iPod una moda</a> (<cite>fad</cite>) e commentava che l'appena annunciato Mac mini alla fine era un prodotto come gli altri, che certo non avrebbe messo sottosopra il mercato.

L'adesso amministratore delegato di Dell, Michael Dell, ha annunciato che per ottenere il previsto risparmio di tre miliardi di dollari annuali, l'azienda dovrà <a href="http://www.cnbc.com/id/23936040" target="_blank">tagliare più posti di lavoro</a> degli 8.800 già annunciati.

Chissà se un Dell mini avrebbe aiutato.