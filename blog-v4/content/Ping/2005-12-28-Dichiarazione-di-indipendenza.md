---
title: "Dichiarazione di indipendenza<p>"
date: 2005-12-28
draft: false
tags: ["ping"]
---

Mai più legati da alcun filo&hellip; se non per volontà<p>

Qualche giorno di mare, metà in vacanza metà al lavoro, ma da remoto. Questo Ping va in onda da una casa priva di qualunque presa telefonica e computer desktop. Ma il mio PowerBook e il cellulare Bluetooth superano qualunque ostacolo.<p>

Si arriva in rete da dovunque e spero vorrai scusare l&rsquo;entusiasmo del neofita, per una cosa che dovrebbe essere ovvia. Lo è, ma al solito toccare con mano è tutt&rsquo;altra cosa che leggerlo sui libri.<p>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>