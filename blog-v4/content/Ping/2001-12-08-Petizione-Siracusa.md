---
title: "Tutti a Canossa da Siracusa"
date: 2001-12-08
draft: false
tags: ["ping"]
---

Se è ancora possibile firmare la petizione, fallo.
Il tema è la difesa dei metadati. Con Mac OS X Apple ha fatto mille scelte giuste e una sbagliata: sta rinunciando all’architettura di Mac OS che prevede per ogni file una serie di informazioni extra, identificative per esempio del tipo di un file (testo? Suoni?) e dell’applicazione che lo ha creato (AppleWorks? BBEdit?).
I metadati sono il segreto della velocità di Sherlock, della visualizzazione della giusta icona per ogni file, dell’apertura dell’applicazione giusta quando si fa doppio clic su un documento e altro ancora. Apple vuole rinunciare a tutto ciò e lasciare che a identificare un file sia solo il suffisso finale, dopo l’ultimo punto nel nome.
È sbagliato. In altre occasioni spiegherò anche perché. John Siracusa di Ars Technica ha avviato una petizione per chiedere ad Apple di riconsiderare la questione, Se puoi e vuoi, firmala.

lux@mac.com

http://www.PetitionOnline.com/osxmd/petition.html