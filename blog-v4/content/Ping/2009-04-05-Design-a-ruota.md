---
title: "Design a ruota"
date: 2009-04-05
draft: false
tags: ["ping"]
---

L'avvento di iPhone e di iPod touch ha generato una conseguenza preziosissima: i programmatori parlano delle loro decisioni di <em>design</em>.

Per esempio, in Tapbots hanno mostrato con tanto di schizzi cartacei e fasi di progettazione <a href="http://tapbots.com/blog/design/designing-convertbot" target="_blank">come sono arrivati a Convertbot</a>, il loro convertitore di unità.

Interessante anche perché di convertitori di unità per iPhone ne esiste una pletora e, volendo, neanche si sente il bisogno di averne uno in più. Oltretutto l'interfaccia più veloce per realizzare un convertitore tende a essere pedestre.

Doppia sfida dunque: creare un convertitore che abbia senso nonostante la concorrenza e venderlo pure.

Da Tapbots spiegano come hanno affrontato il problema con una interfaccia del tutto diversa da quella prevedibile, basata su una ruota girevole come quella degli iPod.

Ed è una lezione di <em>design</em>, proprio alla Steve Jobs: <cite>non è come appare, bens&#236; come funziona</cite>.