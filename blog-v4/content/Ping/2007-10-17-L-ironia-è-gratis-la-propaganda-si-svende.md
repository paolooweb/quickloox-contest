---
title: "L'ironia è gratis, la propaganda si svende"
date: 2007-10-17
draft: false
tags: ["ping"]
---

Chiedo scusa se insisto sul tasto Microsoft, ma da qui a poco ci consoleremo con Leopard. È che accadono cose non ignorabili.

Pochi giorni fa il <a href="http://www.plio.it" target="_blank">Progetto Linguistico Italiano OpenOffice</a> (Plio) ha emesso un comunicato enigmatico, che iniziava cos&#236;:

<cite>Trieste, 15 ottobre 2007 - L'Associazione PLIO ha deciso di festeggiare il settimo anniversario della comunità, che cade in un periodo di crescita particolarmente positivo per la suite, con una versione gratuita per gli studenti universitari italiani, che può essere scaricata liberamente per le piattaforme Windows, MacOS e Linux e ha una licenza illimitata e perpetua, che include tutti i futuri aggiornamenti e può essere trasferita anche agli eredi.</cite>

Enigmatico, perché OpenOffice è gratis da sempre. In realtà era forte ironia verso una recente iniziativa commerciale di Microsoft per l'ambito accademico, ma poteva saperlo solo chi era addentro. Cos&#236; il bravissimo Italo Vignoli, che cura le relazioni pubbliche per il Plio, ha pubblicato una spiegazione:

<cite>Prima di tutto, il tono ironico è stato una scelta precisa, per evitare di inasprire una polemica che è già nata all'interno di quelle università - un ambiente dove OpenOffice.org conta su un ampio numero di utenti e sostenitori - dove gli studenti hanno ricevuto un messaggio di posta elettronica che promuove l'offerta di un'altra suite per ufficio (con uno sconto dell'85% rispetto al prezzo di listino), direttamente al proprio indirizzo personale di ateneo, alla faccia - scusate l'espressione - delle più elementari norme sulla privacy.</cite>

<cite>E già che c'eravamo, abbiamo cercato di sottolineare lo scarso significato che attribuiamo alle ripetute accuse di violazione di 235 brevetti da parte del software open source, perlomeno fino a quando il numero e l'ambito dei brevetti stessi non verrà dichiarato, in modo da eliminare qualsiasi sospetto sul fatto che si tratti di una minaccia generica e tesa esclusivamente ad alimentare la paura, l'incertezza e il dubbio tra gli utenti.</cite>

Uno sconto dell'85 percento non è una operazione commerciale; è una svendita. E la tattica di denunciare la violazione di brevetti&#8230; mi pare di avere scritto qualcosa, su Microsoft e brevetti, giusto ieri.