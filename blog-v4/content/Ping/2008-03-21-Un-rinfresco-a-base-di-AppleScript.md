---
title: "Un rinfresco a base di AppleScript"
date: 2008-03-21
draft: false
tags: ["ping"]
---

Ci sono situazioni particolari in cui Mac OS X ritarda leggermente l'aggiornamento (i programmatori lo chiamano <em>refresh</em>, rinfresco) di una o più finestre dei Finder. Questioni di rete, di processi contemporanei, di vecchie versioni di qualcosa o altro.

Quattro o cinque anni fa poteva pure accadere che una finestra ritardasse molto l'aggiornamento. Oggi non succede più, ma è ugualmente interessante scrivere un AppleScript che aggiorni tutte le finestre aperte del Finder, perché è possibile scriverlo in maniera oltraggiosamente elegante, con un <em>nesting</em> perfetto.

Il <em>nesting</em> è la nidificazione, o la scatola cinese. Dentro un ciclo c'è un altro ciclo, che contiene un terzo ciclo, o un'operazione che viene ripetuta più volte, e cos&#236; via. Il ciclo più interno viene eseguito interamente, per incrementare di un passo l'esecuzione del ciclo che lo circonda, dopo di che il ciclo interno riparte e cos&#236; via. L'effetto è paragonabile a quello dell'orologio con le lancette. Il ciclo interno è la lancetta dei secondi, che gira-gira-gira e a ogni giro fa avanzare di un passo la lancetta dei minuti. Alla lunga, con un giro completo di quest'ultima, avanzerà di un passo anche la lancetta delle ore. Se si indenta ogni fase, ne esce una sagoma &#8220;a freccia&#8221; molto leggibile e che dà soddisfazione al programmatore 8s&#236;, il vero programmatore è attento all'estetica del proprio codice). Sotto con Script Editor, che vediamo come funziona.

<code>tell application "Finder"</code>
(di' al Finder che deve&#8230;)

<code>	repeat with i from 1 to count of Finder windows</code>
	(&#8230;ripetere finestra per finestra del Finder, dalla numero uno fino all'ultima, tutto quello che è compreso in questo ciclo)
	
<code>		tell window i</code>
		(di' alla finestra numero <em>i</em> di&#8230;)
		
<code>			try</code>
			(&#8230;provare a&#8230;)
			
<code>				update every item with necessity</code>
				(&#8230;aggiornare ogni elemento secondo necessità)
				
<code>			end try</code>
			(e smettere di provare quando ha finito)
			
<code>		end tell</code>
		(e smettere di andare finestra per finestra una volta che&#8230;)
		
<code>	end repeat</code>
	(&#8230;le finestre sono state passate tutte e il ciclo si esaurisce)
	
<code>end tell</code>
(e questo chiude tutto)

Questo script è stato realizzato da un programmatore bravo, che ne ha fatto anche una miniapplicazione. Volendo, <a href="http://www.soderhavet.com/refresh/">la si può scaricare</a> e installare nella barra strumenti delle finestre del Finder, con la sua bella icona, in versione Tiger e Leopard.

AppleScript, a volerlo percorrere fino in fondo, è una cosa seria.