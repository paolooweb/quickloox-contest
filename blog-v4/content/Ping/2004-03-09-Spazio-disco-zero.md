---
title: "Spazio zero"
date: 2004-03-09
draft: false
tags: ["ping"]
---

Perché è salutare avere sempre un po’ di spazio libero

Lascia un po’ di spazio libero su disco a disposizione di Mac OS X, specie se apri molti programmi, o molte finestre in un programma.

Sotto i due-trecento mega di spazio libero il sistema darà un avviso. Vuota il Cestino, butta via file inutili, chiudi qualche finestra di programma, chiudi qualche programma.

Se lo spazio arriva a zero, emergenza. Il sistema proporrà di forzare la chiusura di qualche programma aperto. È meglio forzare la chiusura che chiudere con Comando-q!  Infatti, se non c’è spazio su disco, certi programmi cercheranno di aggiornare le loro preferenze nel chiudersi e, non avendo spazio per scrivere ma avendo aperto il file di preferenze, lo danneggeranno.

Uno mi ha detto che gli sembrava ingiusto non poter sfruttare fino all’ultimo byte il disco che ha pagato così salato. Gli ho chiesto quanto spesso il serbatoio di benzina della sua auto è a zero, o se ne tiene dentro sempre almeno un po’, pagandola senza mai consumarla.

<link>Lucio Bragagnolo</link>lux@mac.com