---
title: "L&rsquo;esecuzione &egrave; tutto"
date: 2006-04-24
draft: false
tags: ["ping"]
---

Una volta serviva avere grandi idee. Ora, con Internet, l&rsquo;importante &egrave; fare. Mi stavo chiedendo quanto ci avrebbero messo a creare un foglio di calcolo sfruttabile attraverso il Web, unica cosa che ancora manca nel portfolio di Google, ed <a href="http://www.softwaregarden.com/wkcalpha/" target="_blank">eccolo qua</a> in pieno sviluppo.