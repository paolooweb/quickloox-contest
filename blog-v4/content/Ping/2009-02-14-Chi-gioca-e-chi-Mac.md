---
title: "Chi gioca e chi Mac"
date: 2009-02-14
draft: false
tags: ["ping"]
---

Non gioco stabilmente a Furcadia ma rimango basito ogni volta che leggo <a href="http://www.ghosttiger.com/mfurc/" target="_blank">questa descrizione</a>.

<cite>Ho iniziato mFurc perché avevo un iBook G4 e un account su Furcadia. È davvero un concetto semplice. Ricostruire un gioco online solo Windows, da zero, con conoscenza minima del funzionamento interno del gioco. Renderlo compatibile con tutto il contenuto del gioco creato dagli altri giocatori. Decostruire e ricreare gli strumenti di creazione del contenuto compresi nel gioco. E rendere il tutto una applicazione degna di Mac OS X.</cite>

<cite>Ma non mi bastava copiare. Ho aggiunto funzioni nuove di zecca. Alcune di esse sono ufficialmente in programma, ma non ancora disponibili; altre non arriveranno mai su Windows. E ho aggiunto tutto mantenendo la compatibilità con il client Windows originale.</cite>

Qualcuno si lamenta che i giochi su Mac siano pochi. Io trovo che ci siano su Mac giocatori straordinari. E preferisco cos&#236;.