---
title: "Se saremo stati buoni"
date: 2009-11-11
draft: false
tags: ["ping"]
---

Cito liberamente dal <a href="http://www.google.com/intl/en/press/pressrel/20091110_free_airport_wifi_holiday.html" target="_blank">comunicato stampa di Google</a>:

<cite>Google offrirà connessione</cite> wi-fi <cite>gratuita come regalo di Natale, fino al 15 gennaio 2010. Il regalo copre attualmente <a href="http://www.freeholidaywifi.com/" target="_blank">47 aeroporti americani</a>.</cite>

<cite>La stagione delle feste è una di quelle dove si viaggia più intensamente e passeranno dagli aeroporti partecipanti oltre cento milioni di persone. Per via del cattivo tempo e altre circostanze, i passeggeri hanno a disposizione tempo libero extra una volta oltrepassata la sicurezza, mediamente settanta minuti. Non sorprende che avere una connessione Internet sotto mano possa fare la differenza.</cite>

<cite>Chi si collegherà alle reti</cite> wi-fi <cite>gratuite avrà l'opzione di effettuare una donazione a favore di <a href="http://www.ewb-international.org/" target="_blank">Ingegneria Senza Frontiere</a>, <a href="http://www.one-economy.com/" target="_blank">One Economy Corporation</a> o <a href="http://www.climatesaverscomputing.org/" target="_blank">Climate Savers Computing Initiative</a>. Google metterà di tasca propria altrettanto denaro, fino a un tetto di 250 mila dollari, e la rete di aeroporti che avrà generato la media più alta di donazioni per passeggero riceverà 15 mila dollari da devolvere in beneficenza locale.</cite>

Chissà che iniziative ha in ballo per Natale Telecom Italia. Io scommetto su uno <i>spot</i> di Christian De Sica e Belen Rodriguez, con il cappello rosso, l'albero e la centomillesima tariffa parlaconchivuoi a pagamento anticipato.