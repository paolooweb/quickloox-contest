---
title: "Uomini di Poc parole<p>"
date: 2005-11-05
draft: false
tags: ["ping"]
---

Omaggio al mio club preferito<p>

Scrivo queste righe di ritorno dall&rsquo;ennesima pizzata del Poc, il PowerBook Owners Club. Non ne ho quasi mai parlato qui perché mi capita di scrivere diverse notiziole sul sito relativo e i soliti malpensanti tirano fuori chissà quale conflitto di interesse.<p>

In realtà il Poc è il più grande gruppo di utenza Macintosh italiano, con ben oltre duemila soci, e uno dei più grandi in Europa. L&rsquo;esperienza delle pizzate si ripete ogni ultimo venerdì del mese a Milano da anni e da qualche tempo si è propagata verso Firenze, dove invece il venerdì giusto è il penultimo.<p>

Frequentano e animano il Poc persone speciali, i nomadi digitali. Usano di preferenza un Mac portatile, da cui si separano sempre a malincuore, amano la tecnologia nelle sue forme più creative e positive per la vita di tutti i giorni, si entusiasmano quando un computer non è solo utile ma anche bello e stimolante da usare, si incontrano per chiacchierare di Apple e dintorni e anche (ri)conoscersi come non si può fare via email, e così via.<p>

Sono di ritorno dalla pizzata e sono contento. Ho chiacchierato con persone stimolanti e divertenti, di intelligenza superiore alla media, con una mente vivace. Ho rivisto cari amici, ho scattato qualche foto, attendo che arrivi il prossimo ultimo venerdì del mese per rinnovare l&rsquo;appuntamento. Il Poc ha un <a href="http://www.poc.it">sito</a> che invito a vedere (niente banner, niente conflitti di interessi) e cerca sempre gente con la testa da nomade digitale, vogliosa di scambiare esperienze e condividere entusiasmo.<p>

A qualcuno dovevo dirlo. Spero non ti dispiaccia se ho scelto te.<p>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>