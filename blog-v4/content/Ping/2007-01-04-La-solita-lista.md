---
title: "La solita lista"
date: 2007-01-04
draft: false
tags: ["ping"]
---

Non può mancare, all'inizio del nuovo anno. <a href="http://blog.wired.com/monkeybites/2007/01/new_years_resol.html" target="_blank">Questa</a> però è un po' particolare e il primo a farmela notare è stato Stefano, che ringrazio molto.

Devo dire che non sono neanche messo malissimo, a parte il cyberstalking e la programmazione.

Chiederei a ognuno di aggiungere, anche mentalmente, il suo proposito. Se serve aiuto per l'inglese, in qualsiasi direzione, farò volentieri del mio peggio.