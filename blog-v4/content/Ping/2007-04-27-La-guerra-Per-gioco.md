---
title: "La guerra. Per gioco"
date: 2007-04-27
draft: false
tags: ["ping"]
---

Ho beccato in mailing list l'ennesimo che dice ho installato anche Windows perché su Mac non ci sono i giochi.

Non ho voglia di stare a compilare elenchi. Anche perché, di mio, faccio già fatica a giocarne uno, di gioco, e invidio quelli che lo cambiano una volta ogni giorno dispari.

Invece voglio dimostrare che su Mac i giochi non mancano. Giocando di più.

A breve dichiarerò l'inizio dei <strong>Venerd&#236; Del Gioco</strong>. Ogni luned&#236; annuncerò un gioco giocabile online in multiplayer, almeno uno contro uno se non in tanti, possibilmente open source o comunque dotato di demo giocabile online. Il venerd&#236; successivo mi metto a disposizione dalle 13 alle 14, ora italiana, con chi ha tempo e voglia, e si gioca. Se non c'è nessuno, gioco per conto mio. :-)

Mi impegno solennemente a non prepararmi in anticipo sul gioco, per avere sfide sempre interessanti. Soprattutto, voglio cambiare gioco ogni settimana.

Il genere dei giochi sarà il più vario possibile. Strategia, carte, ruolo, simulazione, guida, qualsiasi cosa.

Non ho fatto né stime né calcoli particolari. Voglio vedere quante volte riesco materialmente a cambiare gioco, prima di poter dire che non ce ne sono più e che mancano. <em>À la guerre comme à la guerre</em>.

Ovviamente, se ci sono suggerimenti, li accetto ben volentieri.