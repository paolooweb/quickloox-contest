---
title: "Vero o falso?"
date: 2010-02-27
draft: false
tags: ["ping"]
---

<i>iPhone non fa</i> multitasking.

<a href="http://itunes.apple.com/it/app/c-span-radio/id322447301?mt=8" target="_blank">C-Span Radio 1.1</a> <i>introduce il</i> background play<i>. Chiede all’utente</i> Vuoi la radio in sottofondo? <i>Background play</i> usa Mobile Safari per riprodurre lo <i>stream</i> di dati e così permette di usare altre applicazioni nello stesso momento.

Quale delle due affermazioni è falsa?

(C-Span Radio è una emittente americana che si occupa di politica).

(<i>multitasking</i> è la capacità di eseguire più di un <i>task</i>, inteso come programma, passando dall’uno all’altro così rapidamente che l’umano ha la sensazione della contemporaneità).