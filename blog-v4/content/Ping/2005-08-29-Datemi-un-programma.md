---
title: "Datemi un programma<p>"
date: 2005-08-29
draft: false
tags: ["ping"]
---

Ma che sia scriptabile però<p>

Solo qui che fremo per avere Pages compatibile AppleScript. Nel frattempo scopro che Anteprima non è compatibile AppleScript. O, se è compatibile, che non l&rsquo;ho ancora capito.<p>

No, non basta avere GraphicConverter di serie con il Mac. Voglio Mac OS X compatibile AppleScript. Almeno scriptabile (per essere una persona seria lo dovrei volere recordable e attachable).<p>

Mac OS X vuol dire tutto quello che sta sul Dvd Apple. È semplice. Apple, mi senti? Per domani è impossibile, lo so. Per quando?<p>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>