---
title: "Falso profeta"
date: 2007-01-08
draft: false
tags: ["ping"]
---

Mi sono sempre fatto tutta una serie di domande, da perfetto ignorante, sui filesystem e sui sistemi operativi, senza mai capire perché certi problemi non venivano affrontati.

La realtà è che non sapevo dell'esistenza di <a href="http://www.opensolaris.org/os/community/zfs/" target="_blank">Zfs</a>. Qualcuno si faceva le mie domande, ma non era ignorante.

Poscritto: inserisco questo post nel sistema alle 23:51 dell'8 gennaio.