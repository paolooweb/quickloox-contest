---
title: "Signorina, grandi firme"
date: 2006-06-14
draft: false
tags: ["ping"]
---

La mia signora ha deciso di convertirsi al banking online e l&rsquo;ho debitamente accompagnata per vedere l&rsquo;effetto che fa.

La procedura ha richiesto almeno un quarto d&rsquo;ora. Quattro firme su quattro documenti diversi (lei &egrave; gi&agrave; correntista da anni, per la cronaca) e tre buste contenenti ognuna una chiave diversa, in funzione dell&rsquo;operazione da effettuare.

L&rsquo;ho gi&agrave; avvisata che nel momento in cui smarrisse una o pi&ugrave; chiavi non dovr&agrave; contare su me. Per il resto mi chiedo che cosa abbia lei fatto di male e chioso, con permesso, che non c&rsquo;&egrave; da stupirsi se come Italia siamo un fanalino di coda.