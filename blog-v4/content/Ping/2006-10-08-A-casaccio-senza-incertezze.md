---
title: "A casaccio senza incertezze"
date: 2006-10-08
draft: false
tags: ["ping"]
---

Ho iniziato il lungo cammino verso l'allestimento di signature random differenziate per account. Userò uno script in Perl che ho trovato sulla mailing list degli utilizzatori di <a href="http://www.barebones.com/products/mailsmith" target="_blank">Mailsmith</a>, <a href="http://www.barebones.com/support/lists/mailsmith_talk.shtml" target="_blank">Mailsmith-Talk</a>:

<code>#!/usr/bin/perl -w
use File::Random qw/random_line/;
($file) = @ARGV;
my $line = random_line($file);
print $line;</code>

Visto che mi sembrava abbastanza innocuo, ho provato a lanciarlo direttamente da Terminale, per vedere se succedeva qualcosa. Non è successo niente e allora ho letto con più attenzione in codice.

Quando in Perl si trova una coppia di due punti significa che viene usato un modulo extra, in questo caso 
<a href="http://search.cpan.org/dist/File-Random/Random.pm" target="_blank">File::Random</a>. Il link porta a Cpan, che è il deposito universale o quasi degli script Perl.

Nella descrizione del modulo viene riportata una dipendenza, cioè un file senza il quale il modulo non funzionerà. Si chiama Want e si trova esattamente nello stesso modo.

Ho scaricato tutto e ho guardato i file readme a caccia di istruzioni. Fortunatamente, nel readme di File::Random sono segnate le istruzioni fondamentali da eseguire nel Terminale, dopo essersi posizionati con <code>cd</code> nella cartella, ops, directory del codice:

<code>perl Makefile.PL</code> (file presente nel pacchetto)

<code>make</code>

<code>sudo make install</code> (sudo serve per andare a installare dentro il Perl di Mac OS X)

Tutto è andato automaticamente al suo posto.

Want non aveva istruzioni ma aveva un file Makefile.PL e le stesse istruzioni hanno funzionato.

Prossimo giro: verificare se funziona davvero.