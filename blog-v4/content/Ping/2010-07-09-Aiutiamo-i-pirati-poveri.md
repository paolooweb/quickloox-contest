---
title: "Aiutiamo i pirati poveri"
date: 2010-07-09
draft: false
tags: ["ping"]
---

Decine di pirati informatici là fuori non hanno di che mettere insieme il pranzo con la cena. Avrebbero le chiavi dei server Apple secondo qualche buontempone, ma misteriosamente si accontentano di qualche <i>account</i> rosicchiato qua e là. Aiutiamoli.

Mi metto in prima fila. Ho due <i>account</i> su iTunes Store, con regolare carta di credito. Una password è ispirata a un album di Frank Zappa, l'altra a un linguaggio di programmazione.

Non ho ancora acquistato Numbers per iPad. Se vedo arrivare la mail di App Store che annuncia l'acquisto, significa che la mia sfida è stata raccolta e almeno uno degli <i>account</i> violato.

Si accomodino. Poveri di inventiva, per i pirati informatici, è assai peggio di poveri nelle tasche.