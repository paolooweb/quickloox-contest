---
title: "Lineetta di confine"
date: 2009-05-03
draft: false
tags: ["ping"]
---

Quando un programma Mac è scritto con Cocoa e prevede uso di testo (solito esempio, TextEdit) mostra il cursore, una lineetta verticale che indica dove verrà inserita la prossima lettera digitata.

Se selezioni una parola o un pezzo di parola, il cursore scompare. Meglio, è in posizione indefinita. Per questo motivo puoi estendere la selezione verso destra, con Maiuscolo più freccia destra, o verso sinistra, con Maiuscolo più freccia sinistra. Capita spesso di avere una selezione non perfetta da perfezionare. Questo vale anche per il doppio clic e vale sia dopo una selezione effettuata - con il mouse o la tastiera - spostandosi da sinistra a destra, sia muovendosi da destra a sinistra.

Su Windows e su Gnome (una delle interfacce grafiche più usate con Linux) il cursore invece non è indefinito e appare nel punto dove termina la selezione. Cos&#236; se vuoi estendere la selezione in quel punto l&#236; è un attimo; se vuoi estenderla all'altro capo della selezione, con il cursore dalla parte opposta, devi lavorare con il mouse o con la tastiera.

Si noti che su Mac la cosa vale con Cocoa e non ancora per tutti i programmi. Per esempio, il Finder non è ancora Cocoa e non si comporta cos&#236;. Tuttavia lo sviluppo Mac va verso Cocoa e nel futuro sempre più programmi Mac si comporteranno nel modo più versatile ed efficiente per quanto riguarda la soluzione del testo.

Windows invece&#8230; qualche volta per fare la differenza basta una lineetta.

L'argomento viene sviscerato su <a href="http://www.codingrobots.com/blog/2009/04/28/selections/" target="_blank">Blogging Robots</a>.