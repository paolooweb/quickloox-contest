---
title: "Records Apple<p>"
date: 2005-03-18
draft: false
tags: ["ping"]
---

L&rsquo;oggetto che mette d&rsquo;accordo Steve, Paul e Ringo<p>

L&rsquo;amico Flavio mi segnala un simpatico <a href="http://reg.email-emigroup.emirecords.co.uk/regp?idc=AQBIsDAQoN0RBCOV6GijSmaDVxO4nz&aid=268733187&n=6">concorso</a> lanciato tra gli iscritti alla mailing list dei Beatles. Tra i premi in palio ci sono anche iPod da venti gigabyte!<p>

(spero che il link funzioni ancora)<p>

Apple Computer e Apple Records saranno anche a litigare tramite avvocati, ma a quanto pare <em>all you need is iPod</em>.<p>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>