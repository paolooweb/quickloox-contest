---
title: "Sulla strada per Girgenti"
date: 2006-10-23
draft: false
tags: ["ping"]
---

Grande successo della pizzata Ping. Affluenza vastamente superiore alle aspettative (in tre e, per parafrasare Venditti, eravamo abbastanza per partire) e mi sono divertito durante una serata del tutto piacevole.

Abbiamo parlato di tutto: dal Ponte sullo stretto alla gastronomia regionale, alle magie del Cobol e mille altre cose che ricordo bene ma tengo per me, perché non sembri tutto buttato l&#236; e dimenticato in una battuta.

Credo che i libri-reliquia abbiano trovato collocazione più che degna e, arrivato a casa, ho dormito meravigliosamente, il che significa.

Gli altri due colpevoli, se hanno voglia e tempo, potranno palesarsi a volontà. Di fatto almeno un terzo dei partecipanti si è espresso con forza a favore di una ripetizione dell'esperienza. Sono favorevole anch'io e, fermo restando che ci vorrà un po' di tempo perché ho i venerd&#236; sempre un po' <a href="http://ical.mac.com/lux/Work" target="_blank">problematici</a>, a una pizzata non si dice mai di no.

Grazie mille. :)