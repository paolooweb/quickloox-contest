---
title: "Fink e c'è vita"
date: 2008-07-20
draft: false
tags: ["ping"]
---

Tanto per tornare a parlare di Mac, tutto il polverone sul computer-che-avendo-un-telefono-nel-nome-deve-essere-un-telefono (e tutte le ragazze che si chiamano Bianca non potranno mai prendere il sole, per logica conseguenza) ha messo un po' in ombra l'uscita di <a href="http://www.finkproject.org/" target="_blank">Fink 0.9.0</a>.

Tempo di ricordarlo e aggiornare. Anche perché Fink ha bisogno di (e merita) uno sponsor. Più gli si fa pubblicità, meglio è.