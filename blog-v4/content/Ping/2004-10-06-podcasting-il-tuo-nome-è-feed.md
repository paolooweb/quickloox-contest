---
title: "Podcasting, il tuo nome è feed<p>"
date: 2004-10-06
draft: false
tags: ["ping"]
---

L&rsquo;ultimissima tendenza che mette al centro del mondo il miglior player di musica che c&rsquo;è<p>

Il trucco sta in una versione di Rss, lo standard dei feed (quelli con cui ci facciamo arrivare notizie dai siti tramite programmi come NetNewsWire), che consente di allegare anche file.<p>

I file possono essere musicali. A questo punto, inviare un feed che contiene la scaletta di una trasmissione radio completa dei brani, piuttosto che l&rsquo;ultima compilation fatta dal famoso deejay o dell&rsquo;amico a una festa, è questione di niente.<p>

Niente, in questo caso, è avere un programma come <a href="http://www.ipodder.org">iPodderX</a>, in grado di leggere i feed e poi riversare tutto su iPod.<p>

Un domani Macworld potrebbe avere il suo podcast. Il direttore suona&hellip; :-)<p>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>