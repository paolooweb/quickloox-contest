---
title: "I Risc di Microsoft<p>"
date: 2004-12-28
draft: false
tags: ["ping"]
---

La domanda da porre al tuo personale appassionato di Pc<p>

Ce l&rsquo;abbiamo tutti, l&rsquo;appassionato di Pc personale. Quello che arriva e chiede a te, l&rsquo;esperto, che ne pensa della ennesima ciofeca vista a Computer Discount, con dentro un Kissacoseon a chissà quanti Furbohertz, che va velocissimo, vero?<p>

Bene, togliti pure la soddisfazione. Digli con noncuranza, <em>ah, a proposito, sai che Microsoft sta puntando forte sulla nuova edizione di Xbox e che per questo ha scelto processori Risc <a href="http://www.theinquirer.net/?article=20146">G5</a>, e gli sviluppatori dei giochi lavoreranno su sistemi ricavati da Power Macintosh G5?</em><p>

Se nei suoi occhi appare una luce strana, non preoccuparti, è un tipico effetto da dissonanza cognitiva.<p>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>