---
title: "Trucchetto da sorvegliante<p>"
date: 2005-03-26
draft: false
tags: ["ping"]
---

A volte la soluzione è già lì, basta coglierla<p>

Capita mai di collegarsi a tante pagine Web diverse più o meno contemporaneamente? A me sì.<p>

Intuitivamente disponevo le finestre sullo schermo in modo da avere un&rsquo;idea dell&rsquo;andamento complessivo dell&rsquo;operazione, ma finivo per riempire lo schermo e per creare più confusione di quella che volevo dirimere.<p>

La soluzione non era intuitiva ma razionale. Exposé.<p>

Exposé visualizza le finestre dinamicamente: F10 (o come lo si è impostato) e si possono sorvegliare tutte le finestre di Safari aperte, fossero anche tutte sovrapposte sul desktop.<p>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>