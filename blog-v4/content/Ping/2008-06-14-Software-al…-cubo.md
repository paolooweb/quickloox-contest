---
title: "Software al&#8230; cubo"
date: 2008-06-14
draft: false
tags: ["ping"]
---

Bella segnalazione di <a href="http://www.bottegascientifica.it/" target="_blank">Carlo</a>:

<cite>il <a href="http://tomas.rokicki.com/" target="_blank">sito di Tomas Rokicki</a>, il quale ha elaborato un algoritmo per la soluzione del cubo di Rubik in 23 mosse.</cite>

<cite>Lo stesso Tomas è uno degli artefici di <a href="http://golly.sourceforge.net" target="_blank">Golly</a>, una versione del &#8220;gioco&#8221; Life per Windows, Mac e Linux.</cite>

<cite>Che bella differenza tra chi i giochi li cerca e magari neanche li utilizza e chi come Tomas li scrive!</cite>

Sottoscrivo e segnalo umilmente, nel mare di software disponibile, un widget Dashboard per dilettarsi con <a href="http://www.c3images.com/dashboard/aboutCube.html" target="_blank">il Cubo su Mac</a>.