---
title: "Cortesie per gli ospiti (indesiderati)"
date: 2010-10-06
draft: false
tags: ["ping"]
---

La <a href="http://www.macworld.it/ping/life/2010/10/03/molto-peggio-del-pesce/">visita al mio <i>account</i> Gmail da parte di un ospite sconosciuto e inatteso</a> ha generato un certo interesse, soprattutto per capire come controllare la situazione.

È semplicissimo. Nella schermata di Gmail di Google, alla fine della tabella dei messaggi in arrivo, compaiono varie informazioni di servizio. Una di queste è l'indicazione dello spazio occupato rispetto a quello disponibile (per me il sei percento; la mia posta cresce quasi al ritmo con cui cresce lo spazio disponibile, piccolo miracolo di Google).

Nella riga immediatamente sotto si fa cenno a quando ho effettuato l'ultimo collegamento e in fondo alla riga c'è un link Dettagli.

Quest'ultimo mostra un elenco degli ultimi accessi, per cronologia e con il numero Ip di chi ha effettuato l'accesso.

Va tenuto presente che Gmail stesso avvisa in caso di numero Ip palesemente fuori squadra; tenere d'occhio questi dati in mancanza di un problema effettivo di sicurezza è un po' perditempo.