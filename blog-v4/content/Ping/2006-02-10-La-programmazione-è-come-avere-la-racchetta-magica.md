---
title: "Poter programmare &egrave; come avere la racchetta magica"
date: 2006-02-10
draft: false
tags: ["ping"]
---

Mi sono appena reso conto che vent&rsquo;anni fa per scrivere <a href="http://www.pong-story.com/" target="_blank">Pong</a> ci voleva un&rsquo;azienda. Vent&rsquo;anni dopo, chiunque di noi pu&ograve; provarci <a href="http://www.linuxdevcenter.com/pub/a/linux/2005/12/15/clone-pong-using-sdl.html" target="_blank">da solo</a> nel giro di un pomeriggio.