---
title: "Tsunami e festicciole<p>"
date: 2004-12-29
draft: false
tags: ["ping"]
---

Piccole differenze tra grandi siti<p>

Sarà ipocrita e cattivo, troppo facile, una stupida questione di stile, ma è un fatto e i fatti restano, anche nel mondo virtuale. Scrivo questo Ping! alle 18 del 29 dicembre 2004.<p>

Apple.com titola all&rsquo;incirca <em>I nostri cuori sono con le persone colpite dagli tsunami dell&rsquo;Oceano Indiano</em>. Ci sono link a Croce Rossa, Unicef e altro.<p>

Microsoft.com titola all&rsquo;incirca <em>Schiaccia lo spam</em> e <em>Dà inizio alla tua festa con Windows XP Media Center Edition</em>. Ci sono link a prodotti Microsoft e nient&rsquo;altro.<p>

Tutto molto facile e molto ipocrita, ma io preferisco comunque avere un Mac.<p>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>