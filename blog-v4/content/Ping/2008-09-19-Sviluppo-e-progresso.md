---
title: "Sviluppo e progresso"
date: 2008-09-19
draft: false
tags: ["ping"]
---

Lo sviluppatore che lascia un segno nel mondo, almeno in quello del software, è colui che si fa carico delle difficoltà in modo che l'utente finale possa lavorare in modo semplice. 

Giustappunto, mi scrive <strong>Lorenzo</strong>:

<a href="http://www.lorenzopantieri.net/LaTeX_files/LaTeXimpaziente.pdf" target="_blank">LaTeX per l'impaziente</a> <cite>è una nuova introduzione a LaTeX, basata sulla mia guida</cite> L'arte di scrivere con LaTeX <cite>(aggiornata di fresco), che si propone di fornire agli utenti di LaTeX di lingua italiana gli elementi essenziali per comporre un documento utilizzando il nostro strumento di scrittura preferito.</cite>

LaTeX per l'impaziente <cite>è, sostanzialmente, una versione ridotta all'osso dell'</cite>Arte di scrivere con LaTeX<cite>, di cui conserva la struttura di base e rispetto alla quale ha il vantaggio di essere breve e di consentire una consultazione molto rapida.</cite>

<cite>Il lettore ideale della guida è un po' diverso da quello dell'</cite>Arte<cite>:</cite>

<cite><ul compact="compact" type="disc">
	<li>ha pochissimo tempo per imparare latex;</li>
	<li>è poco o per nulla interessato a personalizzare gli aspetti tipografici dei propri documenti, ma si accontenta (!) dei risultati predefiniti;</li>
	<li>desidera scrivere in un latex semplice, decoroso, aggiornato.</li>
</ul></cite>

Ecco il link alla <a href="http://www.lorenzopantieri.net/LaTeX_files/ArteLaTeX.pdf" target="_blank">versione maggiore del lavoro</a>:

<cite>E anche i link a due discussioni sul situo del GuIT (<a href="http://www.guit.sssup.it/phpbb/viewtopic.php?t=4775&amp;start=0" target="_blank">una</a> e <a href="http://www.guit.sssup.it/phpbb/viewtopic.php?t=4188&amp;start=0" target="_blank">un'altra</a>), riguardanti i lavori.</cite>

Lorenzo è uno di quelli che lasciano un segno, e anche di più.

Per parte mia, non posso che raccomandare a chiunque di avvicinarsi a LaTeX. È utile, potente, interessante, educativo. E non è programmazione, &#8220;solo&#8221; scrittura a marcatori, un po' come l'Html. LaTeX però è molto più pulito e ordinato.

Davvero, non contano l'età né l'intelligenza: qualche serata di attenzione e chiunque può fare i primi passi in LaTeX, scoprire un mondo nuovo e progredire, sé e quanti ha intorno.