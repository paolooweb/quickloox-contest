---
title: "Cinerama"
date: 2009-07-25
draft: false
tags: ["ping"]
---

Snow Leopard si avvicina sempre più alla data di uscita, ma Apple non traduce la <a href="http://www.apple.com/it/macosx/refinements/enhancements-refinements.html" target="_blank">pagina che ne riassume la novità</a>. una novità al giorno, lo faccio io.

<b>Top Sites</b>

Ci si può godere un'anteprima spettacolare e immediata dei siti preferiti, con Top Sites. Safari 4 tiene conto della nostra navigazione e mostra quelli più frequentati sotto forma di miniature, fino a 24 in una singola pagina.

Devo dire che l'interfaccia per configurarli poteva essere migliore. La presentazione, tuttavia, è di impatto (ricorda gli schermi ricurvi dei Cinerama che impressionò i nonni) e anche utile. Un pizzico di pazienza per impostare i siti giusti ed è tutto molto più efficace dei soliti Preferiti.