---
title: "I libri come una volta"
date: 2009-09-13
draft: false
tags: ["ping"]
---

Mai più senza questa questo eccellente <a href="http://books.google.com/books?id=VYQ4AAAAIAAJ&amp;dq=sinclair+QL+Basic&amp;printsec=frontcover&amp;source=bl&amp;ots=Ha0gto0Rzy&amp;sig=rse1BUCII_CsyqtK2GON_ieyJgk&amp;hl=it&amp;ei=JoqlSqLXH4LQmgOAubj6Dw&amp;sa=X&amp;oi=book_result&amp;ct=result&amp;resnum=3#v=onepage&amp;q=&amp;f=false" target="_blank">libro sul SuperBasic del Ql Sinclair</a> (passare oltre la copertina, mancante).

Mica perché parla del Ql; per la sua impaginazione tipografica assolutamente unica, in tutti i sensi.

Se solo, in attesa di trovare il tempo di riportare in vita il Ql nello scaffale, trovassi almeno un emulatore&#8230;

(grazie a <a href="http://www.gamestar.it/blogs/roots/" target="_blank">OldGen</a> per la paleosegnalazione!)