---
title: "Dare lustro a un dipendente"
date: 2010-09-26
draft: false
tags: ["ping"]
---

Che cosa ti hanno regalato in occasione del tuo quinto anniversario di lavoro in ditta?

Per il primo dipendente di Rogue Amoeba, Mike Ash, hanno pensato a <a href="http://www.rogueamoeba.com/utm/2010/09/23/celebrating-5-years/" target="_blank">qualcosa di particolare</a>. Non c'è bisogno di capire l'inglese, basta guardare le figure.

La qualità del software Rogue Amoeba si spiega più cos&#236; che con mille <i>demo</i>.