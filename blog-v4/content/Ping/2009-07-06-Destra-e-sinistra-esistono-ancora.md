---
title: "Destra e sinistra esistono ancora"
date: 2009-07-06
draft: false
tags: ["ping"]
---

Non in politica, morte e mummificate da lungo tempo a uso dei nostalgici, bens&#236; nei sistemi di scrittura. Snow Leopard ci ha pensato, è solo Apple che non lo vuole dire agli italiani con il ritardo nella traduzione della <a href="http://www.apple.com/it/macosx/refinements/enhancements-refinements.html" target="_blank">pagina apposita</a>. Pagina da cui pubblico una voce al giorno, in italiano.

<b>Testo bidirezionale</b>

Per i linguaggi scritti da destra a sinistra, come arabo ed ebraico, Snow Leopard si destreggia elegantemente nella loro mescolanza con il testo scritto da sinistra a destra. Ha anche una opzione di suddivisione del cursore, che mostra la direzione appropriata del testo nelle zone di confine tra destra-sinistra e sinistra-destra.

E non è neanche un compromesso storico, bens&#236; il modo giusto di fare funzionare le cose.