---
title: "Dio e il bigottismo razionale"
date: 2007-05-20
draft: false
tags: ["ping"]
---

<a href="http://web.mac.com/bluebottazzi/iWeb/BlueBottazzi/MacLovers/79D1B1F9-FEBC-4875-B075-459FCF9E7151.html" target="_blank">Imperdibile Blue Bottazzi</a> sul suo ottimo blog a proposito di programmatori, giocatori, paradossi, avventure, senso religioso. Da antologia il commento del grande Enrico Colombini.

C'entra, il Mac? S&#236;, c'è da convincere Enrico a fare di nuovo qualcosa che ci giri sopra. :)