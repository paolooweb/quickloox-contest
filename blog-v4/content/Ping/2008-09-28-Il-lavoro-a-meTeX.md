---
title: "Il lavoro a meTeX"
date: 2008-09-28
draft: false
tags: ["ping"]
---

Mimmo ha meritoriamente <a href="http://www.macworld.it/blogs/ping/?p=1973&amp;cp=1#comment-34578" target="_blank">ricordato l'esistenza</a> di <a href="http://www.tug.org/mactex/" target="_blank">MacTeX 2008</a>, la soluzione migliore per scaricare e installare LaTeX in un colpo solo.

Però si è dimenticato di pubblicare il link. Ecco rimediata la dimenticanza.

Ne approfitto per essere ripetitivo alla nausea e invitare chiunque non lo abbia fatto a provare LaTeX. Persino <strong>Colum</strong>.