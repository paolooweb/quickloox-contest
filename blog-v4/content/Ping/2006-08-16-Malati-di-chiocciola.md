---
title: "Malati di chiocciola"
date: 2006-08-16
draft: false
tags: ["ping"]
---

So di ripetermi e chiedo scusa, ma non mi è sembrato vero di trovare su Inside Mac Games un <a href="http://www.insidemacgames.com/features/view.php?ID=451" target="_blank">articolo dedicato ad Angband</a>, in pieno 2006.

Per quanti progressi facciano lo hardware e il software 3D, c'è ancora qualcuno, e non pochi, capace di identificarsi in un simbolo di chiocciola e avventurarsi, magari per mesi, in un sotterraneo mortale pieno di mostri maiuscoli e minuscoli. C'è ancora speranza. :-P