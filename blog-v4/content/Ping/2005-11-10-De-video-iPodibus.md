---
title: "De video iPodibus<p>"
date: 2005-11-10
draft: false
tags: ["ping"]
---

Precisazioni, dati e una sorpresa, anzi due, da chi ne sa (più di me)<p>

Mi scrive l&rsquo;amico <strong>Alex</strong> a proposito delle mie intemerate su iPod e video:<p>

<cite>320x240, come definizione è paragonabile a VIDEO CD (NTSC, però, il PAL è un po' meglio e dovrebbe essere 320x288, ma vabbè&hellip;).</cite><p>

<cite>Per stabilità di immagine però H.264 uccide a mani basse VIDEO CD, ponendosi a metà strada tra questo e un DVD di buona qualità.</cite><p>

Uno si chiederà chi sia Alex. È l&rsquo;autore di Skarr, un lungometraggio prodotto con l&rsquo;uso esclusivo di Macintosh. Ed è al lavoro sul progetto successivo. Ho l&rsquo;onore di dare in anteprima assoluta i link al trailer, in <a href="http://www.deltaframe.com/mediacenter/media.asp?id=49&pwd=zerozero">H.264 ad alta definizione</a> e, guarda un po&rsquo;, a <a href="http://www.deltaframe.com/mediacenter/media/zerosigma/teaser_trailer_43.m4v.zip">risoluzione iPod</a>.<p>

Complimenti ad Alex e buona visione a tutti.<p>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>