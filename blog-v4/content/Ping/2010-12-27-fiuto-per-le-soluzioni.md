---
title: "Fiuto per le soluzioni"
date: 2010-12-27
draft: false
tags: ["ping"]
---

S&#236;, fa un bel freddo. La maggior parte dell'Italia gode di un clima complessivamente temperato, ma non tutta e le persone che si ritrovano all'esterno con dieci gradi sottozero, o peggio, non sono poche.

Se sfilarsi i guanti per pilotare iPhone è poco indicato e non c'è a disposizione <a href="http://translate.google.com/translate?hl=en&amp;sl=ko&amp;tl=it&amp;u=http%3A%2F%2Fitnews.inews24.com%2Fphp%2Fnews_view.php%3Fg_serial%3D474508%26g_menu%3D022600" target="_blank">una salsiccia</a>, la soluzione è&#8230; il naso.

La tedesca <a href="http://www.nosedial.com/" target="_blank">NoseDial</a> ha veramente realizzato la <a href="http://itunes.apple.com/de/app/nosedial/id408719052?mt=8" target="_blank"><i>app</i> omonima per iPhone</a>, dal prezzo di 0,79 euro. Direi inutile da marzo a ottobre sotto i tremila metri; da ottobre a marzo però&#8230;