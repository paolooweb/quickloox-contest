---
title: "Sono pazzi questi norvegesi"
date: 2007-01-29
draft: false
tags: ["ping"]
---

Come mi fa notare <strong>Alan Dragster</strong>, in Norvegia dimostrano <a href="http://atvs.vg.no/player/index.php?id=7334" target="_blank">Windows Vista su un iMac</a>.

Non si capisce allora perché vogliano veramente mettere fuorilegge iTunes Store. A meno che lo scopo non sia riuscire a dimostrare una canzone con Drm Microsoft sopra iPod.

Sarà il riscaldamento globale che gli dà alla testa?