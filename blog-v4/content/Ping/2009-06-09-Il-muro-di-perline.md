---
title: "Il muro di perline"
date: 2009-06-09
draft: false
tags: ["ping"]
---

Grazie a <strong>Federico</strong> per avermi segnalato questo <a href="http://www.techcrunch.com/2009/06/08/apples-cool-matrix-style-app-wall/" target="_blank">effetto speciale in onda alla Wwdc</a>.

Il megaschermo è composto da grossi pixel che in realtà corrispondono ad applicazioni presenti in App Store.

Quando qualcuno compra un'applicazione, la sua icona brilla.

L'effetto è davvero suggestivo, anche se &#8211; si dice nella pagina &#8211; non sono rappresentate tutte le oltre cinquantamila apps a disposizione.

Chissà come le hanno scelte. Saranno le &#8220;perle&#8221; più vendute?