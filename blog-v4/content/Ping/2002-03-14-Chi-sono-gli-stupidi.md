---
title: "Chi sono gli stupidi"
date: 2002-03-14
draft: false
tags: ["ping"]
---

Segreti di editori e tattiche di venditori

Per una volta, invece che cliente, oggi ero fornitore di un grosso editore librario. Nella sua sede milanese, uno dei capi stava arringando la forza vendita durante la presentazione delle novità 2002/2003, perché come gli stilisti anche gli editori vanno un po’ per collezioni.
Arriva ai dizionari su Cd-Rom e dice “Sapete che i Cd-Rom sono un problema perché molte volte dobbiamo rifare il software di protezione quando Bill Gates fa uscire una nuova versione di Windows, altrimenti non funziona. E se togliamo la protezione si scatena la pirateria”.
Da una parte pensavo: che stupido un editore incapace di realizzare software durevole. Dall’altra pensavo: che stupida Microsoft a vendere cose che dopo un po’ non vanno più bene.
Poi ci ho ripensato e, con lentezza ma certezza, ho finalmente capito: Microsoft non è stupida, è furba. Gli stupidi sono quelli che.

<link>Lucio Bragagnolo</link>lux@mac.com