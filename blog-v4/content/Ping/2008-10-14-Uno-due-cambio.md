---
title: "Uno, due, cambio!"
date: 2008-10-14
draft: false
tags: ["ping"]
---

Per quanto soddisfattissimo del mio PowerBook G4, sento avvicinarsi il momento del cambio macchina.

Il primo segnale è stato <a href="http://www.apple.com/it/macosx/snowleopard/" target="_blank">Snow Leopard</a>. Se veramente funzionerà solo su Intel, guadagnandosi il sottoscritto da vivere (anche) scrivendo e studiando Mac OS X, non ci saranno alternative.

Ora però è arrivato un secondo segnale: Blizzard ha rivisto verso l'alto i requisiti di <a href="http://www.worldofwarcraft.com/wrath/" target="_blank">Wrath of the Lich King</a>, la prossima espansione di World of Warcraft in uscita per il 13 di novembre, e mette come limite minimo un G5. Esiste la possibilità concretissima che lo abbiano fatto per semplificare e che un G4 di punta, come il mio, permetta comunque la giocabilità. Oppure che proprio non ce la si faccia e allora non starei ad aspettare il terzo segnale.

Per curiosità: quali sono i tuoi segnali?