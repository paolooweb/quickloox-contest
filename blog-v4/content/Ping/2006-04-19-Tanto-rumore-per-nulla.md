---
title: "Tanto rumore per nulla"
date: 2006-04-19
draft: false
tags: ["ping"]
---

Quando chiudo volontariamente la connessione Bluetooth appare un messaggio semiterroristico a dirmi che il collegamento si &egrave; interrotto inaspettatamente. Zelante, ma fuori luogo.