---
title: "Meravigliosamente obsoleto"
date: 2006-04-05
draft: false
tags: ["ping"]
---

<strong>Palmy</strong> mi segnala (grazie!) <a href="http://it.geocities.com/nurcopy/infomisc.html" target="_blank">Joozcom</a>, un sito - a detta dell&rsquo;autore - <cite>utile e graficamente dignitoso utilizzando risorse hardware e software che il mercato giudica obsolete, se non antidiluviane (Apple&copy; Macintosh&copy; LC475, Performa 630 e 5130, iMac DV 350 - Claris HomePage, GoLive 1.1, Netscape Composer, Adobe PageMill).</cite>

Si pu&ograve; fare tutto anche senza ridursi alla dipendenza psicofisica da Dreamweaver. E l&rsquo;Html migliore &egrave; quello cesellato con <a href="http://www.barebones.com/products/textwrangler" target="_blank">TextWrangler</a>.