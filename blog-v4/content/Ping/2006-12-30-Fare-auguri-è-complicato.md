---
title: "Fare auguri è complicato"
date: 2006-12-30
draft: false
tags: ["ping"]
---

Ora che comincia il 2007, faccio il mio proposito: per la fine dell'anno, voglio essere in grado di arrivare in modo consapevole a un quarto di <a href="http://www.macdevcenter.com/pub/a/mac/2006/12/19/building-a-game-engine-with-cocoa.html" target="_blank">un progetto cos&#236;</a>.

Lo stesso, nel tuo campo di preferenza, fosse anche la piccola pasticceria, spero di te. :-D