---
title: "L’incubo di Bill"
date: 2004-05-03
draft: false
tags: ["ping"]
---

Si spiegano tanti perché, ora che c’è QuickTime 6.5

Uno dei motivi della condanna in tribunale di Microsoft per concorrenza sleale e abuso di posizione dominante (stai mica usando programmi fatta da un’azienda così, vero?) è stata tentare a suo tempo di ricattare Apple con minacce tipo se non smetti di produrre QuickTime per Windows io smetto di fare Office per Mac e cose così, di quelle che fanno bene al mercato e alla nostra libertà di scelta (spero si colga una certa vena di ironia nell’ultima frase).

Ora si capisce meglio perché. QuickTime è una architettura multimediale innovativa, evoluta, matura e solida, su cui si appoggia iTunes, che è il nucleo dell’iTunes Music Store, dove si possono acquistare milioni di canzoni da sentira magari su un iPod.

Apple domina il mercato della musica digitale grazie a QuickTime. Microsoft che ha fatto? Un clone malfunzionante, brutto, limitativo della libertà dei singoli utenti in modi che Apple ha cercato di evitare del tutto, salvo dove è stato necessario un accordo con le miopi major discografiche.

Lo faccio io il ricatto a Microsoft ora: se non smettete di produrre quella ciofeca di Windows Media Architecture io continuerò a parlarne male. Chissenefrega? Certo. Chissenefrega, di Windows Media Architecture, quando c’è una alternativa totalmente superiore, come QuickTime?

<link>Lucio Bragagnolo</link>lux@mac.com