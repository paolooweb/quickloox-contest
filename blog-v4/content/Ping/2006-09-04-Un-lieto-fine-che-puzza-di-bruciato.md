---
title: "Un lieto fine che puzza di bruciato"
date: 2006-09-04
draft: false
tags: ["ping"]
---

La storia più carina delle vacanze me l'ha raccontata <strong>Julius</strong>:

<cite>Qua in Sardegna la vita scorre monotona e mi perdo un sacco di MacBook e Pro.</cite>

<cite>Ma oggi è accaduto questo: in passato la casa dove viviamo è stata presa di mira da ladruncoli, quindi mi ero inventato un bel nascondiglio nel forno della nostra cucina&#8230; per il mio PowerBook 12&#8221; e la macchina digitale Fuji&#8230; pur sapendo TUTTI cosa ci fosse dentro, una sassofonista smemorata si è messa in testa di fare una bella torta di mele&#8230;</cite>

<cite>&#8230;ebbene s&#236;, rientro in casa e sento una puzza strana&#8230; non i soliti manici di pentole da due lire sbruciacchiati&#8230; mi fiondo nel forno ed estraggo Fuji e PowerBook in un colpo solo ustionandomi, i bambini erano dentro da 5 minuti&#8230; a 180&#176;&#8230; sigh&#8230; Fuji mezza gonfia, tastini bloccati&#8230; insomma quasi da buttare&#8230; il povero 12&#8221; ansimava con le ventole al massimissimo visto che era in standby come al solito&#8230; &#8220;Cazz di caldo qua dentro!&#8221;&#8230; la puzza del neoprene Tucano che non vi dico&#8230; riesco a staccare la batteria rovente al volo mentre lo appoggio su una lastra di marmo&#8230; aspetto buoni 10 minuti di frenetiche sigarette e mancamenti vari&#8230; Blooom&#8230; si accende! Funziona perfettamente! Miracoloooo!&#8230; un leggero strato untuoso dove si è squagliata la Tucano che viene via con poco alcool&#8230; come nuovo!&#8230; a parte la solita data del 1 gennaio 1970 è tutto a posto&#8230;</cite>

<cite>Alla sassofonista ci penso io&#8230; ringrazio Apple per l'alluminio, Fuji per la pessima plastica, la torta di mele che qualcuno ha osato mangiare lo stesso e se volete testare un Macbook mandatemelo pure :-)</cite>

Il bello è che non è una storia.