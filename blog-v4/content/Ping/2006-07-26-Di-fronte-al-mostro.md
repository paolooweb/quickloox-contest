---
title: "Di fronte al mostro"
date: 2006-07-26
draft: false
tags: ["ping"]
---

Nella mia corsa al fare a meno del mouse ovunque possibile, mi infastidiva molto non avere una scorciatoia di tastiera per il Save As&#8230; di Open Office. Finalmente, preso il coraggio a due mani, mi sono avventurato nei menu.

Tools -> Customize e poi è solo questione di afferrare il meccanismo dell'interfaccia di configurazione, leggermente bizantina. Però non è impossibile. Anche se devo rimettermi. Quando uno ha studiato che una buona interfaccia non ti mette mai a disposizione più di otto elementi tra cui compiere una scelta, queste esperienze fanno molto trekking in Antartide.