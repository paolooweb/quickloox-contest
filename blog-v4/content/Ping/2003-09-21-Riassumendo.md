---
title: "Riassumendo"
date: 2003-09-21
draft: false
tags: ["ping"]
---

In Apple non si butta via niente, soprattutto le tecnologie

Recentemente, sulla mailing list Misterakko, Marco mi ha invitato a scrivere un Ping su Summarize.

Summarize è uno dei servizi di Mac OS X e riassume, a livelli di sintesi definibili dall’utente, un brano di testo che gli venga sottoposto. Funziona anche con l’italiano, funziona piuttosto bene, io personalmente non lo uso ma riconosco che in certe situazioni può tornare veramente utile.

Il bello è che discende direttamente dai laboratori di ricerca avanzata di Apple di una volta, di quando c’era ancora System 7.

Come il riconoscimento della scritttura di Newton, che ritorna in Inkwell, così anche questo è un esempio di tecnologia che non ha conosciuto grandi successi sui media, ma non è stata dimenticata e ritorna. Nessuno, a proposito, ha notato qualche analogia tra la Rubrica Indirizzi e HyperCard?

<link>Lucio Bragagnolo</link>lux@mac.com