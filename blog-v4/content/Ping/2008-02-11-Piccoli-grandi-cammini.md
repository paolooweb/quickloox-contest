---
title: "Piccoli grandi cammini"
date: 2008-02-11
draft: false
tags: ["ping"]
---

Scrive <strong>Paolo</strong>:

<cite>In merito alle tue lezioni di AppleScript su Ping, vorrei mostrarti questa mia <a href="http://italianapple.wordpress.com/itool/" target="_blank">piccola applicazione</a> scritta appunto in questo linguaggio (Xcode+Interface Builder).</cite>

<cite>La mia prima applicazione Apple&#8230;</cite>

Piccola? A me sembra una cosa grande. La dimostrazione che, un passo alla volta, chi vuole veramente camminare arriva dovunque.
