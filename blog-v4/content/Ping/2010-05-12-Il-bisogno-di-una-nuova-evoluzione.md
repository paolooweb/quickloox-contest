---
title: "Il bisogno di una nuova evoluzione"
date: 2010-05-12
draft: false
tags: ["ping"]
---

Rincaso e trovo un cartoncino giallo nella casella delle lettere. C'è il mio nome scarabocchiato a mano e, in stampa, l'avviso di recarmi all'ufficio postale per ritirare una raccomandata.

Mi reco all'ufficio postale e sto in coda dietro ad altre cinque persone. Un'impiegata annoiata scruta di malavoglia il mio cartoncino e inizia a frugare in una scatola di legno, fino a estrarre una busta tra cento altre e passarmela. Ma prima vuole una firma su un foglio scarabocchiato, poi mette un timbro, poi stampa un altro foglio da carta di riciclo, neanche scarabocchia, chiede una seconda firma e timbra altre due volte.

Dentro la busta c'è l'avviso di convocazione a una assemblea cui sono tenuto a partecipare assieme a miei carissimi amici, che mi hanno già avvisato di persona ma sono tenuti per legge a inviare una raccomandata.

Se l'attivazione della posta elettronica certificata servisse a eliminare questo giro primitivo e insensato ci sarebbe da gridare al miracolo. Disgraziatamente la posta suddetta è un ennesimo meccanismo in più, di cui si poteva fare benissimo a meno (come se non esistesse dal secolo scorso la posta elettronica autenticata digitalmente e pure cifrata volendolo), il cui modulo di richiesta sul web fa cadere le braccia e un altro paio di organi.

La beffa finale: per averla, dovrò tornare all'ufficio postale.

Lascio agli illusi l'illusione che le cose vadano male perché governa Questo al posto di Quell'altro e che tutto andrebbe meglio se solo venisse eletto Indaco invece che Ocra, o viceversa (oramai non ci sono neanche più colori decenti salvati dalla politica).

Qui si tratta di sradicare centocinquant'anni di ignoranza, parassitismo, pigrizia mentale, disonestà e &#8211; il peggio del peggio &#8211; burocrazia. Non vale neanche la pena di pensare a da dove cominciare e forse non bastano neanche le fantasiose catastrofi da 2012.

C'è bisogno di una nuova evoluzione. E pensare che a qualcuno un iPad qualsiasi sembra una discontinuità radicale con il passato.