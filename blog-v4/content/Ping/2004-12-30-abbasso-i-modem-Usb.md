---
title: "L&rsquo;augurio e la certezza<p>"
date: 2004-12-30
draft: false
tags: ["ping"]
---

Un felice 2005 non è difficile da conseguire. Basta una piccola attenzione fondamentale<p>

Tanti auguri e felice 2005 a tutti!
Le meraviglie della tecnica permettono di pubblicare un pezzettino anche se non c&rsquo;è nessuno in redazione a pubblicarlo e questa di per sé è una piccola ma significativa conquista.<p>

Una rondine non fa primavera, un caso isolato non fa statistica, un albero non fa la foresta, chi fa da sé fa al massimo per tre ma non di più: lo so.<p>

Il mio buon proposito per l&rsquo;anno nuovo è comunque il seguente: stare il più possibile lontano dai modem Adsl Usb. Sono come quelle automobiline che si vedono in giro ogni tanto, in realtà ciclomotori con abitacolo, guidabili senza patente. Alla prima occhiata sembrano auto come le altre, poi vanno piano, sono rischiose, si guastano e insomma con una vera macchina non hannno niente a che fare.<p>

Comprati un bel modem Ethernet. Costa di più? Per fortuna (tua).<p>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>