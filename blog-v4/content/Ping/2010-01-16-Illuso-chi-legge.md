---
title: "Illuso chi legge"
date: 2010-01-16
draft: false
tags: ["ping"]
---

Come amatore dell’opera di <a href="http://www.mcescher.com/" target="_blank">Maurits Cornelis Escher</a>, posso solo gioire alla scoperta dell’esistenza del font <a href="http://www.emigre.com/EFfeature.php?di=214" target="_blank">Priori Acute</a>. :-)