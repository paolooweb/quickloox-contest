---
title: "Fifty-fifty"
date: 2008-03-05
draft: false
tags: ["ping"]
---

Faceva giustamente notare <a href="http://ipodpalace.com" target="_blank">Stefano</a> che il punto interessante del paragone tra <a href="http://www.nytimes.com/2008/02/04/technology/04link.html?_r=3&amp;ref=technology&amp;oref=slogin&amp;oref=slogin&amp;oref=slogin" target="_blank">Obama e Clinton</a> con Mac e Pc non sta tanto nel fatto che uno sia meglio o peggio; sta nel fatto che è una contesa tra forze sostanzialmente pari.

In Italia diremmo destra-sinistra, conservatore-progressista, liberale-statalista o un sacco di altre cose, ma sempre a significare un rapporto di forze pressoché equivalente. Se pensassimo a Pc-Mac, forse avremmo in mente Golia-Davide, non certo due valori equilibrati in campo.

In tutto ciò, ancora una volta è questione di stile. Evidentemente c'è modo e modo anche di guadagnarsi il voto.

Sarebbe interessante discutere dei siti dei nostri politici, ma solo a condizione che si riesca a discutere dei siti in quanto tali, indipendentemente da chi rappresentano. Altrimenti andate a litigare su <a href="http://se-divento-il-capo.it" target="_blank">Se divento il capo</a>.