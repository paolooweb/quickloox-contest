---
title: "Com’è grande, com’è piccola"
date: 2001-12-14
draft: false
tags: ["ping"]
---

Ricerca e sviluppo, nella Mela, contano. Anche nei numeri

Com’è piccola Apple! Secondo TechReview è la tredicesima azienda nella classifica dei produttori di hardware per spesa assoluta in ricerca e sviluppo (no, Research and Development, che fa più laboratory).
Ma com’è grande Apple. In termini relativi spende il 10% del proprio fatturato in R&D e, se contiamo la spesa media per singolo dipendente, in classifica risale fino al quarto posto.
Se ai suoi tempi Macintosh non avesse trasformato il mondo portando sul mercato l’interfaccia grafica (concepita nei laboratori Xerox di Palo Alto; ma chi ha mai visto veramente in vendita un prodotto Xerox innovativo?), saremmo ancora ai dischi rigidi che si chiamano C-due-punti.
Oops, forse qualcuno incapace di dare un nome ai suoi dischi ancora c’è. Masochista o arretrato?

  <http://www.techreview.com/magazine/dec01/scorecard5.asp>