---
title: "Cancellami o Panther"
date: 2003-11-22
draft: false
tags: ["ping"]
---

Sempre nuove opzioni per disfarsi dei dati inutili

L’informatica potrebbe essere definita la scienza della memorizzazione continua totale perenne e perfetta dei dati, ma per fortuna iniziano ad apparire gli strumenti per disfarsi di quelli inutili (o confidenziali).

In Panther, per esempio, al classico comando Vuota il Cestino si è affiancato Vuota il Cestino Sicuro, che passa e ripassa fisicamente la testina sui dati per azzerarli e minimizzare la possibilità di recupero.

Ancora più intrigante è la nuova opzione di distruzione di dati in serie su Cd riscrivibili (Rw). Da Terminale il comando

drutil bulkerase quick

continua a chiedere Cd-Rw, cancellandone ognuno in un minuto o due, senza soluzione di continuità, fino a quando non arriva un Ctrl-c a fermarlo.

L’opzione gemella, drutil bulkerase full, fa la stessa cosa ma spiana fisicamente la superficie dei dischi Rw per azzerarne tutti i dati, e può metterci più di mezz’ora a disco.

La cosa più carina l’ha detta un partecipante a un mio seminario su Panther: “non mi preoccupo della sicurezza. Prima di cestinare i miei file Word, cancello tutto il testo che c’è dentro”. E ha perfettamente ragione.

<link>Lucio Bragagnolo</link>lux@mac.com