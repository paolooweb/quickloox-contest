---
title: "Gratisserie"
date: 2006-06-18
draft: false
tags: ["ping"]
---

L&rsquo;operazione marketing di <a href="http://www.houdah.com/houdahspot/" target="_blank">HoudahSpot</a> &egrave; riuscita e MacZot <a href="http://maczot.com/blogzot/index.php" target="_blank">offre gratis il programma</a> ai primi cinquemila che si registrano.

Se il link non porta pi&ugrave; al form giusto, significa che l&rsquo;offerta &egrave; terminata ed &egrave; inutile cercare in giro. Di mio sto andando alla massima velocit&agrave; possibile.