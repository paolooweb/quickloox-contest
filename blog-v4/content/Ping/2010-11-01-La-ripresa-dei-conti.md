---
title: "La ripresa dei conti"
date: 2010-11-01
draft: false
tags: ["ping"]
---

Nell'estate del 2009 si erano venduti sette milioni di iPhone.

Nell'estate del 2010 si sono venduti <a href="http://images.apple.com/pr/pdf/q410data_sum.pdf" target="_blank">quattordici milioni di iPhone</a>.

iPhone 4. Quelli che non funzionano, con l'antenna che non prende, la chiamata che cade, la presa della morte. Ah, naturalmente hanno anche <a href="http://venturebeat.com/2010/10/08/glassgate-could-the-iphone-4s-glass-back-be-another-hardware-flaw-for-apple/" target="_blank">il vetro che si rompe</a>.

La gente si è accorta, giustamente, che iPhone 4 non funziona. E ha preso i giusti provvedimenti. Compriamoli tutti, noi che sappiamo del problema, e cos&#236; evitiamo che qualche sprovveduto compri un apparecchio che non funziona. Da qualche parte ci sarà una discarica con dentro una montagna di iPhone 4 eliminati per il bene della comunità.

Si accettano spiegazioni alternative. Il 20 luglio avevo scritto <a href="http://www.macworld.it/ping/hard/2010/07/20/la-presa-di-sale/" target="_blank">bufala mediatica</a>.

C'è tutto lo spazio per chi vorrà fornire ricostruzioni ancora diverse, purché equipaggiate con fatti, numeri, nozioni concrete. Garantisco lo spazio principale sulla pagina. <a href="mailto:lux@mac.com?subject=Ecco%20perche%CC%81%20iPhone%204%20vende%20anche%20se%20non%20funziona">Attendo</a>.