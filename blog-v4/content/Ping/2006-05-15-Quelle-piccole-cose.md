---
title: "Quelle piccole cose"
date: 2006-05-15
draft: false
tags: ["ping"]
---

Che fanno migliore Mac. Se sei con il cursore a met&agrave; di un campo testo e schiacci freccia-in-gi&ugrave;, vai a fine riga. Se c&rsquo;&egrave; un menu gerarchico, il menu secondario resta aperto anche se non passi esattamente dalla freccia.

Sono le cose piccolissime che non entrano mai nei dibattiti epocali e invece dovrebbero, perch&eacute; sanno di gente che ci ha pensato e non dipendono dal processore n&eacute; da chiss&agrave; quali tecnologie. &Egrave; solo questione di pensare a chi dovr&agrave; usare il tuo prodotto.

Mi vengono in mente i semafori di Chicago, che di notte lampeggiano in giallo per le strade con diritto di precedenza e in rosso per quelle con dovere di precedenza. Farlo nelle nostre citt&agrave; sarebbe solo questione di girare qualche interruttore. Ma nessuno pensa a chi dovr&agrave; usare la strada.