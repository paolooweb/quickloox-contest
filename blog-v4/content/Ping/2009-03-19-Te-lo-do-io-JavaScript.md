---
title: "Te lo do io JavaScript"
date: 2009-03-19
draft: false
tags: ["ping"]
---

Come Microsoft ha deciso di infliggere al mondo una nuova edizione di Explorer, Google ha risposto da vera grande.

Per fare un vero test al browser sull'elaborazione JavaScript, prova gli &#8220;esperimenti&#8221; di <a href="http://www.chromeexperiments.com" target="_blank">Chrome Experiments</a>.

Sono cose all'altezza della prossima generazione di browser, per intenderci Safari 4, Chrome stesso e cos&#236; via. Decisamente sconsigliata l'esecuzione se sono aperte altre finestre con contenuto importante.