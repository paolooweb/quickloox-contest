---
title: "Il pozzo e il pendolo"
date: 2007-04-12
draft: false
tags: ["ping"]
---

Oggi mi è riemerso dagli archivi un <a href="http://www.oreillynet.com/mac/blog/2006/03/bbedit_vs_textmate.html" target="_blank">vecchio articolo</a> sul dualismo tra BBEdit e TextMate.

Riguardarlo un anno dopo mi ha rischiarato un po' le idee. Se si potesse pagare per fare i testimonial di BBEdit io pagherei e per questo ho provato a lungo ed estesamente TextMate. A un certo punto ne stavo facendo una questione di funzioni, menu, sintassi supportate, personalizzabilità, scripting e bla bla bla. Mi stavo perdendo in un pozzo profondissimo.

Invece è tutta questione di dove va il pendolo. Ci sono umanisti che si occupano anche di tecnologia e tecnologi (qualcuno direbbe <em>geek</em>) che si occupano anche di text processing.

Sono uno della prima fascia e quindi il mio pendolo va verso <a href="http://www.barebones.com/products/bbedit/" target="_blank">BBEdit</a>. Probabilmente, se fossi un <em>geek</em>, penderebbe su <a href="http://macromates.com/" target="_blank">TextMate</a>. Anche se a quel punto sarei seriamente tentato da <a href="http://members.shaw.ca/akochoi-emacs/" target="_blank">emacs</a>, forse <a href="http://aquamacs.org/" target="_blank">zuccherato</a> con un <a href="http://www.xemacs.org/" target="_blank">cucchiaino</a> di <a href="http://www.webweavertech.com/ovidiu/emacs.html" target="_blank">interfaccia utente</a>, e questione chiusa.