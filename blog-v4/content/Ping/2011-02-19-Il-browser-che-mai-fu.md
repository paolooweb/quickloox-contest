---
title: "Il browser che mai fu"
date: 2011-02-19
draft: false
tags: ["ping"]
---

Come sarebbe stato navigare il web con un Apple II o uno Spectrum Sinclair, o un Commodore 64, nati e finiti prima che arrivasse il web?

Benvenuti a <a href="http://mrgan.tumblr.com/post/3330188156/pixelfari" target="_blank">Pixelfari</a>. Totalmente inefficiente, degno di uno scaricamento e di farsi una sana risata.