---
title: "Tra Chiara, Mario e Carmelo"
date: 2009-10-31
draft: false
tags: ["ping"]
---

Sono già in tre ad avermi evidenziato la pietra miliare dei tremila Ping, passata da qualche giorno (basta guardare l'indirizzo di ogni post per accorgersene).

Sono pertanto tenuto a indire la terza PingPizza, in programma per mercoled&#236; 18 novembre dalle 20 in avanti presso la pizzeria BioSolaire di via Terraggio 20 a Milano. Mi troverò fino alle 19:30 in @Work, in Galleria Borella 3 angolo via Carducci, e fuori dal negozio tra le 19:30 e le 20. Dopo di che, con chi c'è, ci sposteremo verso la pizzeria e chi arrivasse dopo è esortato a raggiungerci direttamente l&#236;.

Come sempre, la qualità della serata non sta necessariamente nel numero. In pochi non viene la foto della tavolata, ma si chiacchiera bene e sentono tutti. Quindi non c'è alcun <i>quorum</i> da raggiungere.

Farò il possibile per portare, come da tradizione, qualche libro inutile da regalare a gente che è costretta dalle circostanze a non poter dire no. :-P