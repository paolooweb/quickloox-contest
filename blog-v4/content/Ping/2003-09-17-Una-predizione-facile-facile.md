---
title: "Una predizione facile facile"
date: 2003-09-17
draft: false
tags: ["ping"]
---

Entro poco vedere i file PostScript sarà più immediato

Una volta che sarà uscita la versione ufficiale di Mac OS X 10.3, alias Panther, arriveranno numerosi programmi in grado di mostrare bene e presto a video file PostScript (oggi bisogna avere superapplicazioni come quelle Adobe, o manovrare con cloni come GhostScript.

La ragione di ciò si chiama Adobe PostScript Normalizer, contenuto in Mac OS X 10.3, e nalla completa compatibilità di Mac OS X con PostScript 3. I dettagli tecnici sono per altre rubriche più spaziose e meno quotidiane.

<link>Lucio Bragagnolo</link>lux@mac.com
