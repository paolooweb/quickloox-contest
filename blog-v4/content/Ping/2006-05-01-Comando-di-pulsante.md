---
title: "Comando di pulsante"
date: 2006-05-01
draft: false
tags: ["ping"]
---

Si vede che &egrave; il periodo delle scoperte infantili. Comando-1, Comando-2, Comando-3 eccetera azionano i corrispondenti bookmark nell&rsquo;omonima barra di Safari.

A qualcosa le vacanze servono.