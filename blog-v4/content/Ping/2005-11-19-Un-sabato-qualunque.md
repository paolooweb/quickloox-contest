---
title: "Un sabato qualunque<p>"
date: 2005-11-19
draft: false
tags: ["ping"]
---

Novecentocinquantanove caratteri basati su una storia vera<p>

Ho lasciato casa con la solita borsa in cui porto il PowerBook, più pesante del consueto causa presenza di adattatori video, cellulare Bluetooth, fotocamera, cavi e accessori vari.<p>

Mi sono collegato senza problemi a un videoproiettore. Ho scattato una serie di foto e le ho importate nel computer, creando più o meno in tempo reale una paginetta web che ho segnalato a un po&rsquo; di gente, supplendo con il cellulare alla carenza di collegamento nel posto dove mi trovavo.<p>

Niente di che. Eppure ho ripensato al mio PowerBook 100 di una volta, quando mi portavo in giro un modemino 14,4K perché il modem interno, installato come optional, funzionava una volta sì e tre no, e per collegarmi dalla casetta al mare dovevo ringraziare AppleLink, che pagavo a carissimo prezzo dopo avere brigato come un matto per riuscire ad averla.<p>

Ne è veramente passato di tempo. Tutto in meglio.<p>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>