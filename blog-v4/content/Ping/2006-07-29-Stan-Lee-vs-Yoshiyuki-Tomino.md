---
title: "Stan Lee vs. Yoshiyuki Tomino"
date: 2006-07-29
draft: false
tags: ["ping"]
---

Un sacco di gente è cresciuta con supereroi dei cartoni animati giapponesi. Ci si è formata una generazione intera e guai a toccargli l'<em>imprinting</em>.

Cartoni che hanno un sacco di cose interessanti, contengono modernità, interpretano lo spirito del tempo. Come tutte le cose, hanno qualche punto debole. Le trame fatte con lo stampino, l'animazione limitata anzi limitatissima, le ossessioni per gli orfani e per le tecniche e cos&#236; via.

Quello che ha veramente cambiato il mondo dei supereroi è arrivato prima e si chiamava Stan Lee. Lee ha preso lo stereotipo di quel noioso Superman e lo ha rivoltato come un calzino, creando una dinastia di supereroi a tre dimensioni, con problematiche di coscienza, amori contrastati, trame complesse, ambivalenza del rapporto tra il supereroe e la gente comune e via dicendo. Stan Lee ha rifondato un genere, mentre i cartoni animati giapponesi sono una operazione dignitossima ma culturalmente limitata.

Si può fare lo stesso ragionamento per esempio nella musica. Togli David Bowie e i Kraftwerk, e cancelli metà della produzione pop di cassetta di oggi. Eccetera.

Dove voglio arrivare? Hanno <a href="http://digitalliving.cnet.co.uk/specials/0,39030785,49282099,00.htm" target="_blank">messo a confronto</a> un Newton MessagePad vecchio di dieci anni e un Origami di Microsoft, per vedere chi vinceva. Indipendentemente dall'esito, c'è chi ha creato un genere, e poi ci sono le operazioni culturalmente limitate, per quanto dignitose.