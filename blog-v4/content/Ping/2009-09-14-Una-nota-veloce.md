---
title: "Una nota veloce"
date: 2009-09-14
draft: false
tags: ["ping"]
---

È oramai molto difficile che un nuovo programma catturi l'attenzione in modo decisivo. Le idee geniali sono sempre più rare.

Tuttavia consiglio un'occhiata a <a href="http://notational.net/" target="_blank">Notational Velocity</a>, open source.

Consente di prendere note alla velocità del lampo e cercarle molto rapidamente. Soprattutto, l'interfaccia è semplificata al massimo e praticamente lo si comanda con un tasto solo.