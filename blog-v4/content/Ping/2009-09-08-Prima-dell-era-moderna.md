---
title: "Prima dell'era moderna"
date: 2009-09-08
draft: false
tags: ["ping"]
---

Grandioso articolo su <a href="http://counternotions.com/2009/08/26/pre-iphone/" target="_blank">come era il mondo cellulare prima di iPhone</a>.

C'è un elenco di venticinque <i>come si stava prima</i>, che è lungo e riprenderò. Intanto fornisco la fine:

<cite>È assennato ricordare che </cite>un singolo apparecchio<cite> prodotto da una azienda con </cite>zero esperienza nel settore<cite> e contro tutte le probabilità ha causato un cambiamento epocale. Cambiamento che non è arrivato perché ci hanno scommesso sopra Nokia, Microsoft, Sony Ericsson, Samsung, Rim o altri che sono sul mercato da quindici anni. Android e webOS, prima di iPhone, non c'erano.</cite>