---
title: "Per grandi disegni. O piccoli"
date: 2008-02-24
draft: false
tags: ["ping"]
---

Ecco <a href="http://www.les-stooges.org/pascal/pencil/" target="_blank">Pencil</a>, programma per disegno e animazione bidimensionale. Open source, gratis, semplice per divertirsi e potente per fare sul serio, o almeno provare.