---
title: "La sorpresa del mattino"
date: 2010-08-27
draft: false
tags: ["ping"]
---

La cosa interessante di iPhone 4 è, definitivamente, il <i>multitasking</i>.

Sono stato abituato a pensare che tornando a Home le <i>app</i> si chiudono; in iPhone 4 invece si torna a Home ma le <i>app</i> restano pronte a intervenire.

Me ne sono reso conto svegliandomi la mattina e vedendo un messaggio arrivato su Skype, che avevo acceso la sera prima e che era rimasto in <i>background</i>.

E poi lo schermo Retina è favoloso per la vista. La nitidezza del testo, anche piccolissimo, entusiasma.

Proveniendo da un iPhone 2G il salto è evidentissimo anche per la velocità. Un bel progresso, niente da dire.