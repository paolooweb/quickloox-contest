---
title: "Una cosa linka l'altra"
date: 2011-01-31
draft: false
tags: ["ping"]
---

Ogni tanto si parla della crisi dell'informazione, del futuro dell'editoria, o forse era il contrario.

Sarà un caso che il quotidiano britannico Guardian abbia sfoderato <a href="http://itunes.apple.com/it/app/the-guardian/id409128287?mt=8" target="_blank">una <i>app</i> coi fiocchi per consultare il proprio sito</a> e sia stato il primo a prevedere una formula di abbonamento (a 5,49 euro annuali, per la cronaca) su App Store, cosa che gli altri ancora non sono arrivati a realizzare?

Può darsi. Metti che abbiano avuto una pensata. La <i>app</i>, essenziale per chi la scarica gratis e fantastica per chi paga, è ottima e tra l'altro permette di consultare il sito anche <i>offline</i>, con video esclusivi e abbondanza di materiale di prim'ordine.

Poi si scopre che tra le teste che albergano nella redazione c'è gente che ha raccolto in fogli di calcolo tutti i dati pubblicati dal Guardian negli ultimi due anni. E che per accedere a tutti i dati pubblicati dal Guardian negli ultimi due anni basta <a href="http://www.guardian.co.uk/news/datablog/2011/jan/27/data-store-office-for-national-statistics" target="_blank">collegarsi a una pagina</a>. E alla fine tutti i fogli sono linkati a un foglio univoco.

Questo è giornalismo, questa è informazione, questa è qualità, questo è servizio.

Scommetto che il Guardian non soffrirà le pene che stanno soffrendo tante altre testate. O ne soffrirà molto meno.

Gli altri, che tagliano i costi all'impossibile pensando solo a quelli, sono spacciati. Competere con il riciclo usa e getta dei siti-spazzatura su Internet è perdente in partenza, senza la qualità.