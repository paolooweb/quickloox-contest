---
title: "Ringraziamenti a Babbo Natale / 4"
date: 2011-01-04
draft: false
tags: ["ping"]
---

Sotto l'albero ho trovato un ultimo oggetto: un secondo disco rigido da un terabyte e più precisamente un <a href="http://www.trekstor.de/en/products/detail_hdd.php?pid=21&amp;cat=0" target="_blank">DataStation max.m.ub di Trekstor</a> (bianco, se interessa).

Per quanto meno elegante e curato del <a href="http://www.macworld.it/ping/hard/2011/01/04/ringraziamenti-a-babbo-natale-3/" target="_blank">Minimus LaCie</a> si tratta di un'unità robusta, ben costruita e dotata di tutto, compreso l'interruttore di accensione e un tasto frontale che dovrebbe avviare un <i>backup</i> automatico, cosa cui non ho minimamente badato, come ho trascurato il Cd-Rom allegato nella confezione, finito direttamente in smaltimento.

Le dimensioni sono più vicine a quelle di un disco 3,5&#8221; standard, quindi maggiori del Minimus. Ho affidato a questo disco un ulteriore <i>backup</i>, giusto per metterlo alla prova, e nella notte ha portato a termine egregiamente il compito. Ho dovuto però formattarlo Mac, prima: l'unità era impostata in Ntfs, il <i>filesystem</i> di Windows che Mac, in assenza di <a href="http://www.tuxera.com/" target="_blank">estensioni specifiche</a>, legge ma non scrive. All'inizio non capivo perché non riuscissi a cancellare il <i>software</i> di servizio Windows presente sul disco e poi finalmente ho guardato le informazioni. Pochi minuti in Utility Disco sono bastati a risolvere il problema.

Anche questo DataStation è rimarchevolmente silenzioso nell'utilizzo. Dispone di un foro di sicurezza che sembra particolarmente robusto e ispira fiducia, buono per ambienti poco fidati. L'unità può funzionare appoggiata orizzontalmente e dispone di una basetta nel caso lo si voglia appoggiare di costa. La basetta però va assicurata mediante due&#8230; <i>viti</i>! Mi sono sentito tornare agli anni novanta e lo sto usando s&#236; di costa, ma senza basetta. Spero verrò perdonato.

DataStation è un disco Usb 2.0 puro, con la velocità di Usb 2.0. Usandolo a scopi di <i>backup</i> e archiviazione, è per me un non problema. Vorrei una Internet più veloce, i dischi vanno benissimo, grazie.

Non ho ancora destinato con precisione questo disco, ma potrebbe diventare il deposito della musica di casa a disposizione di iTunes. Ci devo pensare.