---
title: "Auguri e regali"
date: 2010-07-03
draft: false
tags: ["ping"]
---

Sarà utile, WinX Dvd Ripper per Mac?

Non lo so; ho pochissimi Dvd e ancora meno intenzioni di riversarli su Mac, iPhone o iPad.

Di fatto potrebbe essere utile a qualcuno e forse sarà utile segnalare che fino al 14 luglio il programma <a href="http://www.winxdvd.com/dvd-ripper-for-mac/download.htm" target="_blank">si può avere gratuitamente</a>.

Solo per oggi 4 luglio 2010, inoltre, mTrip Travel Guides lascia scaricare gratuitamente da App Store le proprie guide turistiche su New York, Chicago e San Francisco.

Il fuso orario favorisce i dormiglioni; per noi sarà 4 luglio fino a qualche ora dopo la mezzanotte.

Molti auguri a quanti festeggiano l'indipendenza americana e a qualcun altro di mia privata conoscenza. :)