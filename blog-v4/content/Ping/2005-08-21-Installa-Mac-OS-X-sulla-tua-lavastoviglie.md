---
title: "Il test dell&rsquo;estate<p>"
date: 2005-08-21
draft: false
tags: ["ping"]
---

Sai installare Mac OS X sulla tua lavastoviglie?<p>

Ha fatto scalpore la notizia dell&rsquo;installazione di Mac OS X per processori Intel su normali Pc (è una cosa che Apple assolutamente non vuole).<p>

A parte il fatto che si sta facendo la corsa su sistemi che non saranno quelli venduti da Apple, è interessante vedere quello che riusciranno a fare gli hacker.<p>

A parte il fatto che finora almeno la metà di quello che si è visto è finto, dato che ci si mette un attimo a fare mostrare a Mac OS X il nome di chissà quale processore, sicuramente Apple dovrà impegnarsi a fondo per riuscire a limitare al massimo i pirataggi.<p>

Il lato buffo della cosa è che ora Mac OS X passa per essere installabile su tutto. L&rsquo;ultima moda è attendersi che sia installabile sulla Xbox 360 in arrivo a Natale. Il motivo? Ha dentro un PowerPc (non proprio: ne ha tre). Il fatto che la stessa Microsoft lo dichiari come processore customizzato, e che la scheda logica sia completamente diversa da quella di un Mac, non conta per nessuno. Sognare è bello, ma per rispetto di chi legge forse sarebbe il caso di limitare i sogni alla fantascienza e alla letteratura rosa.<p>

Intendiamoci: lato software, nulla è impossibile. Nessuna protezione è inviolabile, tutto può essere fatto funzionare se si fa fatica adeguata.<p>

Un team motivato e competente potrà riuscire a fare abbastanza fatica e fare girare Darwin, forse persino Mac OS X ma ci credo meno, su Xbox. Un altro team similare potrà certamente riuscire a installare Mac OS X per Intel, quello finale, su un Pc.<p>

Anzi, per molti versi la difficoltà delle due imprese è vagamente paragonabile.<p>

Dico queste cose per chi legge resoconti un po&rsquo; improvvisati e inizia a credere di poter infilare un giorno il suo Dvd di sistema nella Xbox, o sul Pc, e installare Mac OS X con un clic o due.<p>

Non è così semplice.<p>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>