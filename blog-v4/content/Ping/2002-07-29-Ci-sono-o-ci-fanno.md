---
title: "Ci sono o ci fanno?"
date: 2002-07-29
draft: false
tags: ["ping"]
---

Gli Office-dipendenti non sono schiavi del software, ma semplici abitudinari. O in malafede

Ogni tanto sento qualcuno dire che Office è un male necessario perché non si può farne a meno. Poi leggo mail come quella di Claudio Clerici (claudioc@tin.it), che riporto qui con il suo permesso:

&#147;[...] nell&#146;azienda dove lavoro abbiamo deciso di abbandonare Office e sull&#146;80% delle postazioni (un centinaio) ho installato AppleWorks. La compatibilità con Office è molto elevata e, almeno nel nostro caso, riusciamo a salvare e leggere file di Word ed Excel senza problemi di impaginazioni o formattazioni varie.

Attenzione: i convertitori di AppleWorks funzionano benissimo a patto che il documento originale di Word/Excel non contenga parti/funzioni che AppleWorks non è in grado di gestire. Un esempio sono i grafici presenti in un foglio di Excel che non vengono convertiti ma che bisogna rifare in AppleWorks partendo dai dati che vengono importati correttamente.

L&#146;aspetto più problematico è quello di leggere file di Word o Excel mentre se da AppleWorks si devono salvare file per Word o Excel la cosa funziona bene.

Ho notato, con l&#146;utilizzo pratico (visto che non esiste in pratica documentazione), che lavorare con AppleWorks sfruttando bene gli strumenti che mette a disposizione rende molto più produttivi che non lavorare con Office di Microsoft, in particolare quando si devono integrare parti di foglio elettronico, grafici ed altro in un documento di testo o viceversa.

Con AppleWorks si fa il 99% di quello che serve in una azienda ad una frazione dei costi che si dovrebbero affrontare per acquistare Office&#148;.

A questo punto mi chiedo se Claudio prenderà il Nobel per l&#146;informatica oppure se semplicemente basta volerlo fare e invece gli Office-dipendenti, semplicemente, ci fanno. O ci sono.

<link>Lucio Bragagnolo</link>lux@mac.com