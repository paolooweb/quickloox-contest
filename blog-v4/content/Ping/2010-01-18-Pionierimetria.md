---
title: "Pionierimetria"
date: 2010-01-18
draft: false
tags: ["ping"]
---

Come si fa a capire quanto qualcosa sia in anticipo sui tempi?

Con i collegamenti giusti.

Per esempio: torno dalla visione di <a href="http://www.avatarmovie.com/" target="_blank">Avatar</a>, il <i>kolossal</i> computergrafico 3D di James Cameron, già regista di Titanic. Storia semplice, ottimamente confezionata, con messaggio di moda e lieto fine, perfetta per sbancare il botteghino globale, di grande suggestione visiva.

Spero lo vedano tutti, cos&#236; non farò più fatica a spiegare a parole che cosa sia <a href="http://www.wow-europe.com/en/index.xml" target="_blank">World of Warcraft</a>, giocato nel globo da quattordici milioni di persone &#8211; in luogo del miliardo di spettatori che farà Cameron &#8211; ed esistente da un lustro.

Tutte le volte che penso al paesaggio tridimensionale visibile in prima persona e alla biosfera fantastica di World of Warcraft mi torna alla mente <a href="http://www.mysterium.ch/myst/myst_info_e.html" target="_blank">Myst</a> di Cyan Worlds, l'avventura grafica che ha inaugurato una nuova era dei videogiochi prima di ritrovarsi a disposizione di tutti a prezzo irrisorio persino <a href="http://itunes.apple.com/it/app/myst/id311941991?mt=8" target="_blank">su iPhone e iPod touch</a>.

Myst è stato il primo gioco venduto su Cd-Rom. Non casualmente è stato creato su un Mac, primo computer ad avere il lettore Cd-Rom di serie e al tempo unico a disporre di una tecnologia di organizzazione-navigazione-presentazione dati quale <a href="http://c2.com/cgi/wiki?HyperCard" target="_blank">HyperCard</a>.

Myst su Mac è uscito il 24 settembre 1993 (e fino al 2002 è stato il videogioco più venduto della storia). Avatar arriva oltre sedici anni dopo. Ecco.