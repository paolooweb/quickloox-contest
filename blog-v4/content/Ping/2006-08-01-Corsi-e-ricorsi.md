---
title: "Corsi e ricorsi"
date: 2006-08-01
draft: false
tags: ["ping"]
---

Una <a href="http://software.seekingalpha.com/article/14310" target="_blank">analisi stimolante</a> suggerisce che Microsoft potrebbe diventare oggi la Ibm degli anni Ottanta: un'azienda sul punto di perdere il proprio business (hardware per Ibm, software per Microsoft).

La trattazione è un po' semplicistica e non sono altrettanto ottimista. Però è incoraggiante che queste analisi iniziano a diventare possibili. Nessuno si augura la morte di Microsoft, ma una situazione normale di concorrenza, che aumenta la libertà di scelta, senza monopoli che mettono costantemente a rischio il progresso della tecnologia e la crescita del mercato.

Se poi la sorte di Microsoft fosse peggio della morte, allora me la auguro s&#236;.