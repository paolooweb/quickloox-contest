---
title: "Finita (quasi) prima di iniziare"
date: 2006-08-07
draft: false
tags: ["ping"]
---

La transizione a Intel. Alla scorsa Wwdc, Steve Jobs aveva detto che sarebbe partita il 30 giugno scorso per finire a fine 2007&#8230; ed è finita ora, con MacPro e Xserve. Sedici mesi di anticipo non sono poca roba.