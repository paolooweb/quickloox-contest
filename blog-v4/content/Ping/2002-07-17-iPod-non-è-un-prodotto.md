---
title: "iPod non era un prodotto"
date: 2002-07-17
draft: false
tags: ["ping"]
---

Un conto è l’elettronica di consumo, un conto la strategia del digital hub

È arrivato iPod per Windows, che ogni mese veniva dato per imminente dallo scorso Natale, e i produttori di anticipazioni possono rilassarsi visto che, a suon di sparare, qualcosa hanno colpito.

La spiegazione sta nel prezzo, che si è abbassato in modo consistente. Ora iPod non è più un’avanguardia che fa vendere Mac in quanto centro del digital hub, ma una commodity, persino più economica di un telefonino di rango mediobasso (tra l’altro iPod da venti gigabyte è più sottile del 10% rispetto a prima; non è incredibile?).

Quando Apple vuole proporre Mac come digital hub, e come il migliore sul mercato, Mac deve avere il meglio. Quando un dispositivo digitale Apple arriva su Windows ha esaurito la sua funzione strategica e continua a servire, solo con più attenzione al fatturato. È la chiave per capire molto del prossimo sviluppo di Apple.

<link>Lucio Bragagnolo</link>lux@mac.com