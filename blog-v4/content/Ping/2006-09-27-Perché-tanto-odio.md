---
title: "Perché tanto odio?"
date: 2006-09-27
draft: false
tags: ["ping"]
---

In un giorno, dalle mailing list, quello che sostiene che il Finder non possa ordinare le cartelle per dimensioni. Quello che smonta le immagini ma lascia attaccato il disco fisico e non sa come rimontarle (ma perché non le lasci attaccate?), e dice di non riuscire a scaricare un file quando è sufficiente registrarsi a un forum per poterlo fare. Quello che chiede il programma perfetto per fare backup, quando il problema del backup è la strategia giusta, e per il resto si può anche fare manualmente su un hard disk esterno, se è la cosa giusta per te&#8230; ma perché la gente, con tutto quello che può fare su un computer, sceglie di farsi del male?