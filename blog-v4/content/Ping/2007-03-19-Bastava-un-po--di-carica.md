---
title: "Bastava un po' di carica"
date: 2007-03-19
draft: false
tags: ["ping"]
---

Dopo venti minuti di casini con l'Html delle Google Pages per cercare di raggiungere la personalizzazione che volevo, ho finalmente individuato il minuscolo link che permette di caricare direttamente un file Html fatto come mi pare, che <a href="http://pages.google.com/" target="_blank">Google Pages</a> visualizza senza battere ciglio.

Da una parte sei contento, perché neanche Google è esente da miglioramenti alle sue interfacce. Dall'altra non puoi fare a meno di ammirarli, perché l'interfaccia sarà pure migliorabile, ma se si poteva pensare a una funzione loro l'hanno già pensata.