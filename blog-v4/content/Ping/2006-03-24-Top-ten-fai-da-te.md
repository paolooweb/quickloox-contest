---
title: "Top ten fai-da-te"
date: 2006-03-24
draft: false
tags: ["ping"]
---

Una carrettata di immagini e devo selezionare quelle adatte per il prossimo passo del lavoro. Importo tutto in iPhoto e assegno il voto a ciascuna immagine. &Egrave; banalissimo: mela-1 per dare una stellina, mela-5 per darne cinque e si intuisce che cosa stia nel mezzo.

Poi creo una cartella intelligente nel Finder. Criterio di ricerca, il voto o l&rsquo;intervallo di voti che mi interessa.

<em>Voil&agrave;</em>, selezione fatta in automatico e in tempo reale, automaticamente aggiornata in tempo reale se cambia qualcosa. Non vedo Windows da molto tempo, essendo una persona fortunata, ma mi chiedo come si potrebbe ottenere lo stesso risultato con altrettanta <em>nonchalance</em>.