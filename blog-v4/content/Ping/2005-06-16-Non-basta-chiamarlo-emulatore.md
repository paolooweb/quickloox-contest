---
title: "Non basta chiamarlo emulatore<p>"
date: 2005-06-16
draft: false
tags: ["ping"]
---

Una precisazione su Rosetta, perché non sembri uguale a ciò che non è<p>

Ho già visto liquidare in una frase la questione sulle prestazioni delle applicazioni PowerPC che potranno girare sui futuri Mac con processori Intel e la frase è &ldquo;andranno piano, perché è un emulatore&rdquo;.<p>

Che andranno più lente dell&rsquo;originale è sicuramente vero e che Rosetta sia un emulatore è fuori discussione. Ma non tutti gli emulatori sono uguali. Per esempio, VirtualPC lavora sul programma da emulare un&rsquo;istruzione alla volta, e per ogni istruzione Intel che legge esegue le istruzioni equivalenti su PowerPC. Come un acquirente di mobili Ikea che legge una istruzione e monta quel pezzo, poi legge un&rsquo;istruzione e monta un altro pezzo e così via.<p>

Rosetta invece lavora in modo del tutto diverso. Il nostro cliente Ikea prima imparerebbe a memoria tutte le istruzioni e poi monterebbe l&rsquo;intero mobile in una sola passata. Se è un mobile solo potrebbe non esserci questa gran differenza; ma se fossero scaffali di libreria modulari il vantaggio sarebbe grande, perché Rosetta legge le istruzioni una volta per tutte e, fino a che queste restano in memoria, va molto più veloce di chi deve sempre riprendere in mano il manuale.<p>

Inoltre c&rsquo;è un anno di tempo e più per fare progredire la tecnologia. Rosetta è un emulatore, ma il suo andare piano potrebbe essere, per esempio, molto più veloce di VirtualPC.<p>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>