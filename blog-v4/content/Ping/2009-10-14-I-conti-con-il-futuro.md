---
title: "I conti con il futuro"
date: 2009-10-14
draft: false
tags: ["ping"]
---

L'attenzione alle notizie del giorno spesso fa scappare l'attenzione da trend interessanti, però di termine più lungo.

Ho scoperto che ora GnuCash, l'applicazione per finanze personali d'obbligo nel software libero, ora si scarica per Mac sotto forma di immagine disco e di icona da trascinare nelle applicazioni. Prima bisognava darsi alla prestidigitazione con Fink, MacPorts, il Terminale, i file di configurazione e uno i conti preferiva farseli su un foglietto.

Contemporaneamente apprendo della versione 1.6 di Macnification, applicazione di gestione delle immagini da microscopio; una specie di Aperture per scienziati, ricercatori e studenti.

Non solo <a href="http://www.macnification.com/" target="_blank">Macnification</a> supporta pienamente i 64 bit ma è anche compatibile con Grand Central Dispatch, la tecnologia che non esisteva prima di Snow Leopard e che distribuisce le elaborazioni impegnative su tutti i processori o core a disposizione.

Ciliegina sulla torta, arriva <a href="http://cruzapp.com/" target="_blank">Cruz</a>. È l'ennesimo browser per Mac, che si distingue dagli altri per la capacità di mostrare più pagine contemporaneamente nella stessa finestra. Non è certo la rivoluzione, ma testimonia per l'ennesima volta il successo dell'approccio libero di WebKit. Su Mac c'è un parco <i>browser</i> che gli altri si sognano.

Da una parte arriva su Mac software storico; dall'altra si valorizzano le novità software introdotte da Apple meno di due mesi fa (e nella storia mica è sempre successo, anzi); e intanto le parti libere del software Mac sono terreno di evoluzione continua. Sono soddisfazioni.