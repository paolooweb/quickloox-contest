---
title: "Disponibile a metà"
date: 2011-02-10
draft: false
tags: ["ping"]
---

Chiaro che Mac App Store ha bisogno di tempo per rodare e che incidenti di percorso fanno parte di tutti gli inizi.

I più spiritosi nella gestione degli incidenti suddetti sono stati finora quelli di Panic, al momento di fare debuttare <a href="http://itunes.apple.com/it/app/transmit/id403388562?mt=12" target="_blank">Transmit</a> 4.1.5 sullo Store.

La nuove versione è rimasta bloccata per un giorno e mezzo in attesa dell'approvazione di Apple, che ha richiesto alcune modifiche.

Nel frattempo però Panic aveva comunque pubblicato per conto suo un aggiornamento indipendente per quanti avevano già acquistato il programma prima di Mac App Store.

Cos&#236; l'azienda ha annunciato il programma in un <a href="http://www.panic.com/blog/2011/01/transmit-4-1-5/" target="_blank">ilare comunicato</a> come <i>Half-Available</i>, disponibile a metà.

Nel giro di trentasei ore si è risolto tutto e cos&#236; si chiude il <i>post</i> di Panic: <cite>Ringraziamo tutti quelli di Apple per il loro lavoro duro e costante; il loro compito, per quanto autoimposto, non è facile. Per quanto silenziosa in apparenza, so che Apple non è sorda alle critiche e confido che questa procedura non potrà che migliorare con il tempo.</cite>

Saper affrontare anche gli intoppi senza perdere spirito e senso dell'ironia, in una logica positiva, è raro e conferisce ancora più valore a Transmit e agli altri prodotti della premiata ditta.