---
title: "Qualunque cosa sia il bundle<p>"
date: 2005-01-25
draft: false
tags: ["ping"]
---

Hackeraggio avanzato e insospettabile su Mac OS X<p>

L&rsquo;amico Birpi mi ha scritto per avere un parere. Voleva inserire in Mail una signature che riportasse l&rsquo;uptime aggiornato al momento. Nessun problema, solo che lui non voleva che nel Dock si avviasse l&rsquo;AppleScript relativo.<p>

Non sono stato di aiuto. So che dal Terminale si può dire <em>osascript</em> per lanciare comandi AppleScript e che da quest&rsquo;ultimo si ordina un <em>do shell script</em> per lanciare comandi Unix. So inoltre che su <a href="http://osaxen.com">MacScripter.net</a> si trova KinderShell, una Osax che inserisce la sintassi Unix dentro AppleScript. Ma di più non so fare.<p>

Birpi ha risolto alla grande. Come dice lui,<p>

<cite>ho aggirato l&rsquo;ostacolo cronizzando un AppleScript che lancia il comando Unix uptime e infila il risultato dentro una signature di Mail già predisposta&hellip;</cite><p>

<cite>Compilato poi come <em>eseguibile bundle</em> e inserito queste due righe nella plist dell&rsquo;applicazione pacchettizzata ho risolto il problema del Dock...</cite><p>

<code><key>NSUIElement</key></code>
<code><string>1</string></code><p>

A me pare il codice Da Vinci, ma appena vedo Birpi gli chiedo di darmi una dimostrazione. Di fatto lui, che programmatore non è, ha creato una piccola applicazione con AppleScript Studio. Che è alla mia portata. E quindi di tutti.<p>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>