---
title: "Nel regno del fattibile / ultimo"
date: 2010-12-28
draft: false
tags: ["ping"]
---

Un'ultima cosa possibile con iPad e impossibile con un Sony Vaio W: pilotare un <a href="http://ardrone.parrot.com/parrot-ar-drone/it/" target="_blank">Ar.Drone</a>, cosa piuttosto facile e intrigante tanto che negli Stati Uniti <a href="http://www.facebook.com/Parrot?v=app_4949752878" target="_blank">c'è un concorso</a> che vede come <i>testimonial</i> l'asso del basket Nba Steve Nash.

Un Ar.Drone in configurazione standard <a href="http://ardrone.parrotshopping.com/it/p_ardrone_main.aspx" target="_blank">è piuttosto costoso</a>. Comprare e assemblare i singoli componenti, magari rinunciando agli accessori come le videocamere di bordo, consente di limare o almeno distribuire la spesa. È certo un giocattolo, ma divertente ed educativo.

I <i>post</i> della serie <i>regno del fattibile</i> terminano qui. Non perché manchino gli argomenti che anzi si moltiplicano; perché dovrebbe essere chiaro oramai a tutti che un <i>netbook</i> è un computer sacrificato e un iPad è un tipo diverso di computer.

E poi anno nuovo, vita nuova, argomenti nuovi. Buona vigilia a tutti e felice ingresso nel 2011, &#8220;a bordo&#8221; di un Ar.Drone o anche a piedi.