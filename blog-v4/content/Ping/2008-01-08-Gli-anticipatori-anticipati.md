---
title: "Gli anticipatori anticipati"
date: 2008-01-08
draft: false
tags: ["ping"]
---

Una settimana prima di Macworld Expo Apple ha presentato i nuovi <a href="http://www.apple.com/it/macpro/" target="_blank">Mac Pro</a>, ha presentato i nuovi <a href="http://www.apple.com/it/xserve/" target="_blank">Xserve</a> e come ciliegina sulla torta FileMaker (proprietà di Apple) ha presentato <a href="http://www.filemaker.it/products/bento/preview/learn-more.html" target="_blank">Bent&#333;</a>.

Se si vanno a spulciare i siti delle sedicenti anticipazioni, se ne troveranno a decinaia e decinaia che prevedevano i nuovi Mac Pro per il Macworld Expo. Ce ne fosse uno, di questi aruspici, che ha predetto l'anticipo di una settimana.

A dire <em>presentano i Mac Pro</em> sono buoni tutti e non vale niente. Il fatto di non sapere la data conferma, una volta di più, che non sono né gente informata né attenti analisti. Semplicemente, scrivono una cosa plausibile e sperano che sia veramente vera. Peggio degli oroscopi, che almeno richiedono di saper gestire un database. Peggio del <em>gossip</em>, dove almeno il paparazzo deve accordarsi con la <em>starlette</em>.