---
title: "Caratteri facili<p>"
date: 2005-05-22
draft: false
tags: ["ping"]
---

È facile non accorgersi di tutte le migliorie di Tiger<p>

Mi serviva una schermata della palette Unicode di scelta dei caratteri. Ho pasticciato con le Preferenze di Sistema per inserirla nei menu grafici in alto a destra e, alla fine, mi sono accorto che adesso, nei programmi Cocoa, l&rsquo;ultimo elemento del menu Composizione è sempre esattamente quello lì.<p>

Le novità di Tiger sono talmente tante e talmente pervasive che alla fine uno ce la ha anche sotto il naso e non se accorge.<p>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>