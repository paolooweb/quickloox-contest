---
title: "Ragioni difficili"
date: 2009-04-15
draft: false
tags: ["ping"]
---

Le autorità della University of Utah hanno dichiarato che il <em>worm</em> Conficker <a href="http://www.washingtonpost.com/wp-dyn/content/article/2009/04/12/AR2009041200313.html" target="_blank">ha infettato</a> (venerd&#236; 12) oltre 700 computer nel campus, comprese alcune macchine dentro i tre ospedali che fanno parte del campus.

Sono state distribuite istruzioni a staff e studenti per eliminare il <em>worm</em> da computer, dischi esterni, fotocamere e <em>smartphone</em>.

In varie zone del campus è stato chiuso l'accesso a Internet anche per sei ore, con lo scopo di arrivare al <em>weekend</em> e permettere ai tecnici di lavorare nel fine settimana allo sradicamento del <em>worm</em> in assenza di altre attività.

Conficker non infetta Mac OS X.

Ma evidentemente ci sono ragioni per usare Windows. Autolesionismo o miopia?