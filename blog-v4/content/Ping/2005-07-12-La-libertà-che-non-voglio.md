---
title: "La libertà che non voglio<p>"
date: 2005-07-12
draft: false
tags: ["ping"]
---

Perché il mio vicino è diverso (e sono affari suoi)<p>

Suona la porta. È il figlio del vicino che chiede se posso passare un attimo a casa sua. Lo so perché: nel circondario, sono <em>quello che lavora con i computer</em>.<p>

Il vicino mi accoglie disperato e mi racconta la storia. La racconto qui tale e quale.<p>

Gli arriva a casa il figlio lamentandosi perché lui non ha il gioco X, che va per la maggiore tra i compagni di classe. Il vicino va a comprare il gioco X. Solo che è su Dvd e il suo vecchio Pc (Windows) ha solo un lettore Cd.<p>

Il vicino va allora a comprare un lettore Dvd. Avendo a disposizione uno slot libero nel computer lo installa. Da quel momento il lettore Cd preesistente non viene più riconosciuto, ma ci si può accontentare.<p>

Lancia il gioco. Il quale spiega che la scheda video non è sufficiente.<p>

Il vicino va a comprare una scheda video di ultimissima generazione. La monta al posto della scheda precedente, accende il computer e vede Windows a sedici colori. Ancora più brutto del normale.<p>

È il momento in cui mi manda a chiamare.<p>

Insieme scopriamo che la nuova scheda genera, dice Windows, un conflitto di risorse. Codice 12. Fruga e rifruga, si scopre anche che il driver in dotazione alla scheda non è compatibile con il chipset video del computer del vicino.<p>

Accendiamo il modem e ci mettiamo in rotta verso il sito del produttore della scheda. L&rsquo;indirizzo riportato nella documentazione non esiste più. Allora mi affido a Google. Riesco a trovare finalmente una versione aggiornata dei driver. Ventisette megabyte di download. Ovviamente il vicino ha una connessione 56K. È il pretesto buono per tornarmene a casa, che è ora di cena e il software ci metterà altre due ore abbondanti per arrivare.<p>

Rivedo il vicino il giorno dopo. Scuro in volto. Il driver ha funzionato, mi spiega. Adesso Windows si vede normalmente e non ci sono più problemi. Ma il gioco, su quel computer vecchio, gira da schifo, e il figlio ha già chiesto un computer nuovo.<p>

Quando ti raccontano che il Pc ha il vantaggio di poter cambiare qualunque pezzo in piena libertà, ricordati del mio vicino. Per parte mia, quella libertà io non la voglio, grazie.<p>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>