---
title: "Processo al processore"
date: 2006-08-07
draft: false
tags: ["ping"]
---

I nuovi Mac professionali, con nuovi processori Intel, non hanno quel differenziale di prestazione che si è visto con gli iMac e i Mac mini (i portatili non li contiamo neanche). E questo testimonia quanto il G5 fosse (ed è) un processore estremamente valido.

Chiacchiere con un po' di amici nella stanza di iChat <code>wwdc2006lux</code>. Per entrare, Comando-Maiuscole-G e digitare il nome. :)