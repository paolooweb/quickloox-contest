---
title: "Giochi per e giochi diviso"
date: 2007-09-13
draft: false
tags: ["ping"]
---

Mi ha scritto tempo fa <a href="http://homepage.mac.com/matteo.discardi" target="_blank">Matteo</a>:

<cite>[&#8230;] in relazione ai tuoi post sui giochi per Mac, mi aspettavo un commento su Starcraft II (Blizzard), e sul fatto che il rilascio (come consuetudine) è contemporaneo per Mac e per Pc, nel senso che le cose fatte bene, a ben vedere, si possono fare. Non tutti le fanno, ma è un parere accademico. :)</cite>

Per il commento me la sono presa comoda, perché <a href="http://www.starcraft2.com/" target="_blank">Starcraft II</a> non ha ancora neanche una data di uscita. Blizzard scrive nelle Faq del sito che vogliono prendersi tutto il tempo che ci vuole per fare uscire il gioco al massimo delle loro capacità e li invidio tanto.

In generale, con le tecnologie software che ci sono oggi e il monopolio dei processori x86, chi scrive software solo per Windows non ha veramente più scuse. O è indietro nella tecnologia, o ha la mente ristretta. In entrambi i casi questa non è una buona premessa, per creare giochi validi.

È anche la conferma di un processo in corso: quindici anni fa, un sacco di ottimi giochi uscivano non per Mac. Oggi, quando un gioco esce solo per Windows, è sempre più probabile che sia mediocre. Quanto meno è fatto da sbadati che si tagliano fuori da una quota di mercato che non è irrisoria.