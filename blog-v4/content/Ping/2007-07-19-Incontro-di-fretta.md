---
title: "Incontro di fretta"
date: 2007-07-19
draft: false
tags: ["ping"]
---

Il venerd&#236; del gioco di domani (è destino che a luglio si arrivi tardi su tutto) è dedicato a <a href="http://www.cosmicencounter.com/screens/home.html" target="_blank">Cosmic Encounter</a>. Sempre tema fantascientifico, ma basta il browser per giocare.

Come sempre, è aperta la stanza <em>gamefriday</em> su iChat per chiacchierare e coordinarsi. Il tutto dalle 13 alle 14. :-)