---
title: "Pages di feedback"
date: 2007-11-08
draft: false
tags: ["ping"]
---

Ho inviato ad Apple un paio di feedback rispetto a Pages.

Gli stili si possono associare ai tasti funzione, da F1 a F8. Mentre sarebbe desiderabile una versatilità maggiore (anche perché F8 per esempio è lo standard di Spaces), il baco vero è che se hai il documento in primo piano e, per dire, premi Ctrl-F2, applichi lo stile associato a F2. Non va bene, perché le due scorciatoie sono diverse e Ctrl-F2 è pure la scorciatoia base di Apple per arrivare alla barra dei menu.

Il secondo, su cui ho qualche dubbio in più perché fatico a riprodurlo, è che, con un documento aperto e il pannello Font aperto pure lui, se vado su un altro Space e torno, quando torno il focus di Pages non è sul cursore dentro il documento, che mi sembra logico, ma dentro i valori di dimensione dei font. Inizi a digitare testo senza farci caso e ne succedono di tutti i colori, meglio, dimensioni.

A favore, invece, un lavoro di un certo peso che partiva da template di Word. Ho aperto il template con Pages e l'ho registrato come template di Pages. E ho inserito il testo, nonché applicato gli stili del template, da dentro Pages.

La compatibilità con gli stili e con il template non è stata il 100 percento (uno stile tabella centrava gli elementi invece che allinearli a sinistra), ma il 99 percento s&#236;. Ho sistemato lo stile dentro il template di Pages e tutto il resto è filato via liscio come l'olio.

Alla fine ho consegnato i file in formato Word che si aspettava il mio (ottuso) committente e lui non ha avuto niente da ridire, perché l'impaginazione del documento e la corrispondenza degli stili erano ineccepibili.

Se va sempre cos&#236;, posso iniziare a ignorare OpenOffice.