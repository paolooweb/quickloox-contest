---
title: "Tanto va la Gates al lardo"
date: 2010-02-11
draft: false
tags: ["ping"]
---

<cite>Niente di iPad mi fa dire &#8216;vorrei che lo avesse fatto Microsoft'.</cite>
&#8212; <a href="http://blogs.bnet.com/corporate-strategy/?p=101" target="_blank">Bill Gates, 10 febbraio 2010</a>

<cite>Niente di quello che fa iPod mi fa dire &#8216;wow, non penso che non possiamo farlo anche noi'.</cite>
&#8212; Bill Gates, <a href="http://www.businessweek.com/technology/content/sep2004/tc2004092_2455.htm" target="_blank">2 settembre 2004</a>

L'abitudine di copiare l'ha sempre avuta.