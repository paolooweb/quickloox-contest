---
title: "Roma val bene un Apple Store"
date: 2006-02-26
draft: false
tags: ["ping"]
---

Da un thread sulla lista MacLovers riporto qui una domanda a cui mi sono dato diverse risposte ipotetiche ma che resta argomento intrigante. <strong>Perch&eacute; Apple apre un Apple Store a Roma?</strong>

Ci sono citt&agrave; europee con un bacino di utenza assai pi&ugrave; grande, come Parigi. Citt&agrave; ben pi&ugrave; ricche, come Berlino. Citt&agrave; pi&ugrave; attive culturalmente e socialmente, come Madrid.

Contemporaneamente esiste un Apple Store in una citt&agrave; come Edimburgo, che con tutto il rispetto, confrontata con Vienna o Praga, &egrave; un buco fuori mano.

Quindi il criterio di apertura di nuovi Apple Store in Europa &egrave; diverso da quelli immediati, o pi&ugrave; complesso. Qual &egrave;?