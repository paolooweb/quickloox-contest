---
title: "Date a Google quel che è di iCal"
date: 2008-08-07
draft: false
tags: ["ping"]
---

<a href="http://multifinder.wordpress.com" target="_blank">Mario</a> riferisce che Google ha superato le incompatibilità tra iCal a Google Calendar con semplici <a href="http://www.google.com/support/calendar/bin/answer.py?answer=99358" target="_blank">istruzioni per l'uso</a> (che sottintendono una serie di aggiustamenti tecnici, ma è altra questione).

Come ho già risposto a lui, bravi tutti, perfino iCal che comunque ha almeno la possibilità di inserire i dati appropriati.