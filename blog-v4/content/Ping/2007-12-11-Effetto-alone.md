---
title: "Effetto alone"
date: 2007-12-11
draft: false
tags: ["ping"]
---

Entra in negozio dirigente d'azienda. Chiede di poter acquistare un MacBook Pro.

Mentre si perfeziona l'acquisto, si chiacchiera e lui spiega: <cite>ho sempre usato un Pc, ma mi hanno mostrato l'iPhone. Mi sono detto: se un telefono lo fanno cos&#236;, voglio provare il computer</cite>.

Nel frattempo, sulla stampa di settore appaiono articoli intitolati <a href="http://www.news.com/Why-Dell-needs-a-handheld/2100-1041_3-6222010.html" target="_blank">perché a Dell serve un palmare</a>. Toh.