---
title: "Rotella voodoo"
date: 2003-05-06
draft: false
tags: ["ping"]
---

Più che un consiglio è uno scongiuro

Mac OS X 10.2 ha limitato clamorosamente la spinning wheel, la rotella infinita che inizia a girare quando un programma non risponde al sistema da due secondi e quindi è meglio mettersi a fare dell’altro.

Ogni tanto però capita ancora. A volte, forse, può darsi, mi sembra di constatare che il lancio di un altro programma, o il salvataggio di un file da un altro programma, insomma, qualcosa compiuto utilizzando il filesystem possa a volte, forse, può darsi, chissà, dare una sveglia al programma in preda a rotellamento.

Non è una nota tecnica, né una raffinata analisi software, né una intuizione geniale; è pura e semplice scaramanzia, tecnologicamente valida quanto e come i cornetti di Totò.

Se funziona, sempre come si dice dalle (spendide) parti di Totò, ditelo ai vostri amici; se non funziona, fatevi i fatti vostri. :)

<link>Lucio Bragagnolo</link>lux@mac.com