---
title: "Uno, due, Xml"
date: 2007-03-03
draft: false
tags: ["ping"]
---

<strong>Marcello</strong> aveva un problema con Pages: un documento vuoto che, aprendosi, lamentava la mancanza di alcuni font non presenti nel sistema.

Siamo arrivati alla soluzione indipendentemente, selezionando il documento e scegliendo Mostra Contenuti Pacchetto.

Dentro il bundle del file, compresso in formato gzip, c'è un file index.xml. Cambiate le occorrenze dei nomi dei font incriminati con quello di un font sicuramente presente nel sistema, tipo Helvetica, si è risolto il problema.

Marcello non ha ricompresso il file, io s&#236; (con gzip da Terminale), ma pare che non faccia differenza.

Probabilmente la questione è dovuta al fatto che il template che usava era stato creato sul computer di qualcuno con quei font l&#236;.