---
title: "Riprendiamo le trasmissioni scusandoci dell'interruzione"
date: 2009-02-25
draft: false
tags: ["ping"]
---

Esso vive.

È arrivato in uno scatolone di cartone, contenente la scatola vera tenuta in posizione da quattro fermi sempre in cartone. La scatola, protetta da una busta di politene, è poco più grande del computer, un paio di centimetri in alto e altrettanti sui lati più quattro dita sulla destra.

Aperta la scatola, all'interno del coperchio un foglio di gommapiuma protegge il computer, che sta sotto, anch'esso nel politene. Il MacBook Pro alloggia in una vasca di plastica. Da sotto spunta una linguetta di cartone nero, con la scritta <em>Designed by Apple Computer in California</em>.

Tiri la linguetta e il computer si solleva docilmente. Sotto, in alto, l'alimentatore, anche lui dentro il politene, con il connettore MagSafe protetto da un coperchietto di plastica. La sorpresa: in dimensioni è uguale a quello del PowerBook, non un mattone come nei primi Mac Intel. In basso una busta nera di cartoncino, quella che mi fa impazzire. A sinistra contiene il manualetto base, <em>Tutto Mac</em>. A destra la bustina <em>Tutto il resto</em> (è cos&#236; che si fa marketing!), con garanzia e scartoffie di licenza, due Dvd di sistema e applicazioni e una pezzuola per pulire lo schermo. Apro <em>Tutto Mac</em> e leggo <em>Complimenti per essere entrato in sintonia con MacBook Pro</em>. È cos&#236; che si fa marketing. A qualcuno sembra roba da bambini. Invece semplifica, mette a proprio agio, rassicura. Vorrei vedere che cosa c'è dentro un Dell.

<em>Tutto Mac</em> spiega come cambiare la Ram e come cambiare il disco rigido e poco altro. C'è anche un refuso (<em>si apriva</em> al posto di <em>sia priva</em>) ma poteva andare peggio.

A destra del computer una scatola bianca contiene la prolunga Schuko e la spina diretta per l'alimentatore. Attacco tutto e preparo Assistente Migrazione, con un cavo FireWire 800.

64 gigabyte di dati trasferiti in un'ottantina di minuti e il nuovo Mac si riavvia esattamente dove avevo lasciato quello vecchio. La risoluzione, però, è pazzesca. Abituato a 1.440 x 900, i 1.920 x 1.200 fanno capire il significato della parola produttività. Lancio BBEdit, che apre la consueta ventina di finestre, e c'è ancora praticamente metà dello schermo libero.

Ultimo atto sul vecchio PowerBook G4: accendo, lancio iTunes, deautorizzo.

Il corpo <em>unibody</em> è affascinante, difficile da credere. L'idea di robustezza e di leggerezza che trasmette è bellissima. Apprezzo consciamente tutte le sparizioni del superfluo: il pulsante della trackpad, il gancio di chiusura. Le dimensioni sono praticamente identiche ma le rastremature e gli arrotondamenti del corpo lo fanno sembrare più sottile e in effetti è anche lievemente più sottile: il connettore Ethernet è veramente ai limiti dell'altezza disponibile.

Lo schermo lucido è lucido e riflette, esattamente come quello di un iPhone. Almeno su BBedit, però, non è un problema. Mi rendo conto che mi specchio perfettamente nello schermo, ma solo se mi metto volontariamente a fissare la mia immagine. Altrimenti il lavoro procede rapido. La tastiera risponde benissimo - avevo fatto un po' di esercizio, confesso - e le dita si muovono esattamente come prima, nessun bisogno di riaddestramento.

Un giro su System Profiler per vedere che le specifiche corrispondano. Corrispondono.

Cambio nome al disco e al computer ed eccomi qui.

Tutto quello che ho provato finora ha funzionato. La risposta, rispetto al vecchio PowerBook, lo confesso, fa dire <em>era ora</em>. Avrò tempo per sovraccaricarlo come finisco sempre per fare; per ora mi fa capire che dovrò impegnarmi molto più di prima. Sposto una finestra e mi stupisco della velocità. Safari recupera una ventina di finestre in un secondo, dove il PowerBook mi suggeriva la pausa caffè.

Saprò dire nei prossimi giorni. Al primo impatto, cento percento.