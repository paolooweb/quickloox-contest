---
title: "Un giorno in premura"
date: 2008-02-03
draft: false
tags: ["ping"]
---

È un periodo di grosso avanti e indietro, in cui non ho neanche pensato al backup.

È appena apparso un avviso di Mac OS X: sono dieci giorni che non fai il backup, va tutto bene o vuoi modificare le preferenze di sistema nel caso sia cambiato qualcosa?

L'ho adorato.

Time Machine, tecnicamente, è una cosa di una pochezza assoluta. Si fa con un comando Unix e poco più. L'interfaccia, l'interfaccia, l'interfaccia.