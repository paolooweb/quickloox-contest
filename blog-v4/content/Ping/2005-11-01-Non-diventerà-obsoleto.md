---
title: "Non c&rsquo;è rischio<p>"
date: 2005-11-01
draft: false
tags: ["ping"]
---

Ecco perché non è prematuro spiegare che esistono gli Universal Binary<p>

Mi è già stato chiesto un paio di volte.<p>

<cite>Ma non corro il rischio di comprare un Mac oggi e ritrovarmelo presto obsoleto a causa della transizione ai processori Intel?</cite><p>

No.<p>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>