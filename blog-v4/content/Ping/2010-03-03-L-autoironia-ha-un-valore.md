---
title: "L'autoironia ha un valore"
date: 2010-03-03
draft: false
tags: ["ping"]
---

La più pregiata delle <a href="http://www.wired.com/gadgetlab/2007/11/analysts-dont-k/" target="_blank">quindici predizioni più ottuse scritte a proposito di Apple</a> e raccolte da Wired ha valore proprio perché in fondo c'è, sportivamente e spiritosamente, proprio quella di Wired (<cite>ammettetelo. Siete fuori dal giro dello hardware</cite>).

È bello inoltre vedere previsioni palesemente ottuse anche al rialzo, come quella per cui Apple Tv avrebbe cambiato la televisione come iPod la musica.