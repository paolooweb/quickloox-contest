---
title: "Spes viva est"
date: 2006-11-03
draft: false
tags: ["ping"]
---

Ieri notte, tra le consuete amicizie particolari in cirillico e i soliti allungamenti del pene in giapponese, tra i tanti che arrivano sulle reti di chat a cercare conversazione, è passato uno che parlava&#8230; latino.

Purtroppo la maledetta ruggine da desuetudine mi ha impedito di spiccicare più concetti che quello, minimo sindacale, di non riuscire a stargli dietro.

Però mi si è allargato il cuore. C'è ancora speranza. :-)