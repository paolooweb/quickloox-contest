---
title: "Cosa fatta Capodanno ha"
date: 2007-12-29
draft: false
tags: ["ping"]
---

Ci sono quelli che sospirano e concludono <em>un altro anno è passato</em>.

Io sono tra quelli che <em>arriva un anno tutto nuovo</em>.

A metà gennaio ci si ritrova per la solita chattata in contemporanea con il keynote di Steve Jobs. Uscirà nuovo hardware, nuovo software, succederanno tantissime cose. Ci sarà molto da imparare, come in questi ultimi anni.

A chi piace la compagnia, l'invito è rinnovato. Anche per il 2008 qui si continua a scrivere, discutere, approfondire, spiegare, sovvertire quando è giusto. Sono tutti benvenuti. Non ci sono requisiti. Le regole sono semplici: le idee sono separate dalle persone; tutti hanno diritto a un'opinione e il dovere di farsene una nel rispetto dei fatti; a parità di argomento, qualsiasi fatto conta più di qualsiasi opinione.

Comincio dando un'occhiata a <a href="http://plasq.com/skitch" target="_blank">Skitch</a>. Non so perché, ma questo sarà un anno buono per il software libero.