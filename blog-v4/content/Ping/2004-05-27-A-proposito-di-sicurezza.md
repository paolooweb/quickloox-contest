---
title: "A proposito di sicurezza"
date: 2004-05-27
draft: false
tags: ["ping"]
---

Apple si sta comportando non proprio bene

Passo per difensore a oltranza di Apple, ma affermo tranquillamente che c’è da essere abbondantemente insoddisfatti dell’atteggiamento della Mela sul problema sicurezza.

Non è questione tecnica perché, anzi, tra aggiornamento sicurezza e Mac OS X 10.3.4 sembra che tutti i buchi più problematici siano stati risolti. È questione di atteggiamento.

Apple ha un sistema operativo open source eppure sembra fare il possibile per fare passare sotto silenzio le falle di sicurezza, se non addirittura fare finta di niente fino a che le cose non diventano comunque di dominio pubblico.

I nostri computer, essenzialmente, non sono a rischio, a meno che non ci comportiamo da stupidi. Grazie ad Apple per questo. Ma ci faccia anche il favore di parlare chiaramente, per tempo e con sincerità, perché sicurezza è anche fiducia.

<link>Lucio Bragagnolo</link>lux@mac.com