---
title: "Auguriamocelo"
date: 2010-12-23
draft: false
tags: ["ping"]
---

Natale è imminente e c'è sempre più bisogno di cogliere l'occasione. Per chi crede è un'ovvietà (posto che trovi spazio fra i panettoni e i pacchi infiocchettati) e per chi non crede c'è sempre bisogno di ricordarsi, almeno una volta l'anno, dei nostri doveri di fratellanza verso il prossimo, sia il vicino di casa o l'Inuit a nord di Terranova.

Paul Butler di Facebook ha creato alcuni <a href="http://www.facebook.com/note.php?note_id=469716398919&amp;id=9445547199" target="_blank">affascinanti planisferi</a>, disegnati da null'altro che i legami di amicizia stabiliti in Facebook stesso da persone di città differenti. Per quanto Facebook non sia il massimo dell'interazione e del dialogo, mi si è aperto il cuore a vedere le immagini, che parlano comunque di quanto la strada che ci congiunge tutti, magari grazie alle nuove tecnologie, sia davvero breve e degna di essere percorsa. Per arrivare ad accendere fili e luci di amicizia (in senso generale, non quella di Facebook) anche dove in questo momento fa più buio.

Auguri retorici, forse. Ne accetto volentieri di migliori. Nel frattempo, anche prelevare qualche gigabyte di informazioni da <a href="http://hive.apache.org/" target="_blank">Apache Hive</a> per lavorarlo con <a href="http://www.r-project.org/" target="_blank">il software statistico <i>open source</i> R</a>, come ha fatto Butler, non è male. Roba interessante da imparare per fare ingresso nel 2011 a testa ancora più alta.