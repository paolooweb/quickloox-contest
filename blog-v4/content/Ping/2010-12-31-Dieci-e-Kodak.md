---
title: "Dieci e Kodak"
date: 2010-12-31
draft: false
tags: ["ping"]
---

Il gioco combinato di linguaggio e aritmetica fa s&#236; che gli anni Dieci siano già iniziati da trecentosessantasette giorni. È però appena cominciato il secondo decennio di un nuovo secolo e a elencare le trasformazioni e gli sconvolgimenti tecnologici che ha portato il primo decennio (iPod per primo, iPad per ultimo) c'è da trasalire per l'emozione.

Eppure sento ancora tanta gente che ragiona con i vecchi schemi, quelli del secolo scorso, per nessun'altra ragione che in quel momento aveva vent'anni. Età bellissima, ma punto di partenza, non pietra tombale.

Servisse un ulteriore spunto a scrollarsi un po' di polvere da dosso, due giorni fa <a href="http://www.nytimes.com/2010/12/30/us/30film.html" target="_blank">ha chiuso definitivamente i battenti l'ultimo laboratorio di sviluppo Kodachrome al mondo</a>.

È una ennesima epoca che si chiude. Fantastica. È però tempo di viverne altre, altrimenti &#8211; lo avessero fatto tutti prima di noi &#8211; saremmo al rimpianto delle selci scheggiate.