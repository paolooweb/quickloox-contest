---
title: "Giustizia sarà fatta"
date: 2009-09-28
draft: false
tags: ["ping"]
---

Oggi ricorre <a href="http://www.wired.com/thisdayintech/2009/09/0928ie-beats-netscape" target="_blank">l'undicesimo anniversario</a> del sorpasso di Internet Explorer su Netscape.

Netscape mor&#236;, ma dalle sue ceneri è nato Firefox, che sta compiendo una lunga marcia di riavvicinamento.

Probabilmente ci vorranno altri undici anni, ma sarà fatta giustizia.