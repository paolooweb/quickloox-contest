---
title: "Un pesce e qualche volpe"
date: 2009-04-01
draft: false
tags: ["ping"]
---

Cercare pesci d'aprile su Google lo fanno tutti. Ho cercato un <a href="http://maps.google.com/maps?f=q&source=s_q&hl=en&geocode=&q=pesce+d'aprile&sll=45.643808,9.193153&sspn=0.237382,0.365639&ie=UTF8&ll=46.070372,9.141998&spn=0.942266,1.462555&z=10&iwloc=A" target="_blank">pesce d'aprile su Google Maps</a> (senza approfondire, confesso).

In più ho scoperto che sempre su Google Maps la via Gian Battista Vico di Milano viene chiamata <a href="http://maps.google.com/maps?f=q&source=s_q&hl=en&geocode=&q=via+gian+battista+vicolo,+Milan,+Italy&sll=45.462433,9.170204&sspn=0.007442,0.011426&ie=UTF8&ll=45.461884,9.16822&spn=0.007442,0.011426&z=17&iwloc=addr" target="_blank">Gian Battista Vicolo</a>. Ed è cos&#236; almeno dal 31 marzo! (Grazie a Street View gli increduli possono verificare la targa effettivamente affissa all'inizio della via).