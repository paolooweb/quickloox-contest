---
title: "Vota iPhone"
date: 2008-10-02
draft: false
tags: ["ping"]
---

Barack Obama ha fatto realizzare una <a href="http://news.cnet.com/8301-13578_3-10056519-38.html?part=rss&amp;subj=news&amp;tag=2547-1_3-0-20" target="_blank">applicazione per iPhone</a> che permette di gestire il reclutamento di nuovi sostenitori per la sua elezione a Presidente degli Stati Uniti.

La notizia dice un sacco di cose. Non tanto su Obama, ma sulla penetrazione di iPhone e sull'effettiva utilità dello strumento quando la chiamata non è l'applicazione, ma semplicemente un ingrediente della ricetta.