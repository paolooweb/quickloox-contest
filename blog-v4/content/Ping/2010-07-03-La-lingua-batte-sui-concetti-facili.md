---
title: "La lingua batte sui concetti facili"
date: 2010-07-03
draft: false
tags: ["ping"]
---

Da come si riconosce un facilone in queste settimane? Semplice: racconta che a causa di iPad (oppure iPhone, o entrambi) Apple trascura Mac.

La ragione, spiega il facilone, è evidente: Apple fa un sacco di soldi con iPhone e iPad, più soldi che con Mac. Più o meno, per ogni cinque dollari incassati, due sono dovuti agli iApparecchi: il quaranta percento e più.

Vai a spiegare al facilone che nel primo trimestre del 2006 iPod fruttava ad Apple quasi il 56 percento del proprio fatturato, ben più della metà. Ma Apple non ha ucciso né dimenticato Mac, che da allora ha continuato a vendersi in numeri progressivamente crescenti ben sopra la media del mercato.

Che un prodotto trascurato aumenti il proprio successo è roba che può saltare in mente solo a un facilone. iPod oggi fa quasi il 22 percento del fatturato, un dollaro su cinque, invece che undici su venti.

A qualcuno viene in mente che Apple stia trascurando o voglia uccidere iPod?