---
title: "Due d da divulgare"
date: 2008-08-07
draft: false
tags: ["ping"]
---

Ho sempre pensato che un Cd-Rom che funziona, ma improvvisamente si mette a scricchiolare su un certo file, fosse da buttare.

Forse potrebbe essere salvato con <code>dd</code>, un comando di Terminale semisconosciuto e complicato.

Nella sua forma essenziale, <code>dd</code> effettua copie da una fonte di input a una destinazione di output e può essere usato per creare cloni perfetti di un disco o di una cartella, o di un grosso file. Con comandi aggiuntivi può servire a <a href="http://lowendmac.com/brierley/08pb/classic-mac-boot-floppy.html" target="_blank">creare floppy di boot per un vecchissimo Mac</a>, oppure costituisce una <a href="http://forum.maccast.com/lofiversion/index.php/t15167.html" target="_blank">soluzione di backup integrale</a> micidiale per semplicità e crudezza.

<code>dd</code> può essere l'ultima ciambella di salvataggio per <a href="http://www.macosxhints.com/article.php?story=20050302225659382" target="_blank">estrarre i dati da un disco rigido che se ne va</a> e un sacco di altre cose.

Insomma è una di quella cose che vale la pena di passare un'ora sul risultato di <code>man dd</code> e capire il più possibile.