---
title: "Esplorazioni inaspettate"
date: 2009-11-13
draft: false
tags: ["ping"]
---

Esiste un <a href="http://www.frassanito.com/RoverSIM/" target="_blank">simulatore di Lunar Rover</a>. Niente di che rispetto a certe altre cose che girano, ma carino e probabilmente realistico.

Se avessi avuto una cosa come questa in mano ai tempi delle missioni Apollo, ci avrei passato le giornate.