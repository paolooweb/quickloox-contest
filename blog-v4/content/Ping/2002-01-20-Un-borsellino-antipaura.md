---
title: "Un borsellino antipaura"
date: 2002-01-20
draft: false
tags: ["ping"]
---

Apple ha un trimestre profittevole e un borsellino rigonfio di spiccioli

È dal 1985 che sento qualche anima bella chiedermi: “Ma Apple ce la farà?”
La migliore risposta è il fatto che è il 2002 e siamo qui a commentare un profitto trimestrale di 38 milioni di dollari, con 746 mila Macintosh venduti e 125 mila iPod (che equivalgono più o meno ad altri quarantamila Mac per quanto concerne i fatturati).
Alla gente che si è inquietata in passato per un trimestre in perdita, o per il Cube che non si vendeva, un piccolo ma concreto fatto: Apple ha in cassa, come dire nel borsellino di Fred Anderson, il capocontabile della società, la bellezza di quasi 4,4 miliardi di dollari.
Al lettore viene lasciato l’esercizio della conversione in euro. Aggiungo una considerazione: se anche da domani Apple diventasse un colabrodo che perde un miliardo di dollari l’anno, senza un’idea né una soluzione, la società potrebbe ripianare le perdite senza battere ciglio per quattro anni e mezzo. Ovvero, a tutti gli effetti, nel 2007 ci sarebbe ancora.
Pensiamo a che cosa teniamo noi nel borsellino e paragoniamolo al nostro conto in banca. Ce la faremo, vero? E allora, perché non Apple?

lux@mac.com