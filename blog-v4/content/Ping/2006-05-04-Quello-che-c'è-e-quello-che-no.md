---
title: "Quello che c&rsquo;&egrave; e quello che no"
date: 2006-05-04
draft: false
tags: ["ping"]
---

Non &egrave; interessante che il novanta per cento dei confronti tra Mac e Pc si faccia tra un Mac che c&rsquo;&egrave;, pronto da usare appena esce dalla scatola, e un Pc che non c&rsquo;&egrave; e va configurato esattamente in quel modo, ammesso che non dia problemi di alcun tipo e il tempo richiesto per la configurazione sia zero?