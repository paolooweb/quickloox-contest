---
title: "Qualcosa di grosso"
date: 2007-07-15
draft: false
tags: ["ping"]
---

Mi sono collegato alla home del sito Apple. Cinque titoli e tutti riguardano iPhone. A memoria, mai visto prima una cosa del genere.

Non ci sono dati ufficiali. Eppure ho la sensazione che l'affare si faccia grosso. E iPod sia stato solo l'inizio.