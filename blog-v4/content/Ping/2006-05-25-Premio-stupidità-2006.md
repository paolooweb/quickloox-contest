---
title: "Premio stupidit&agrave; 2006"
date: 2006-05-25
draft: false
tags: ["ping"]
---

Si lamenta che la sua stampante a otto colori, pagata ben seicento euro, ha iniziato a stampare malissimo.

Viene aperta la stampante e si verifica che le cartucce colore sono state montate in posizioni scambiate, con ovvi effetti sull&rsquo;uso dei colori stessi.

Quanto si pu&ograve; addebitare a un cliente come diritti di stupidit&agrave;?