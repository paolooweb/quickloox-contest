---
title: "Già che c'eravamo"
date: 2010-03-04
draft: false
tags: ["ping"]
---

Era tempo che non tornavo dentro Google Earth. Hanno aggiunto perfino un simulatore di volo e <a href="http://earth.google.com/intl/en/userguide/v5/ug_flightsim.html" target="_blank">i comandi</a> non sono banali. Ho la sensazione che ci vorrà un bel po' a esplorare le novità.