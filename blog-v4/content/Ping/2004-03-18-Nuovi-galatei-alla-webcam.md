---
title: "Nuovi galatei"
date: 2004-03-18
draft: false
tags: ["ping"]
---

Come si comporterebbe Cristina Parodi davanti a una webcam?

Le nuove tecnologie hanno portato nuovi comportamenti. Tutti, dopo dieci minuti di chat, imparano ad ascoltare l’altro prima di dire la propria. Chi sa scrivere posta elettronica ha imparato a riportare i brani significativi del messaggio a cui risponde, a mettere una riga vuota tra i paragrafi per favore la leggibilità e via dicendo.

Adesso, con le Adsl a prezzi popolari (dammi retta, meglio la flat di quella a tempo, che non ti cambia la vita e aumenta solo le angosce) e aggeggi simpatici come iChat e iSight, emergono nuove possibilità e nuovi vuoti di galateo.

È facile notare infatti che la finestra di videoconferenza, in cui vediamo il nostro interlocutore, si trova in posizione diversa rispetto alla nostra webcam. Se guardiamo la finestra, come viene naturale, dall’altra parte ci vedono sempre con lo sguardo diretto altrove, come se fossimo più timidi del vero o avessimo qualcosa da nascondere.

Ecco allora la prima nuova regola del galateo informatico: si parla guardando la Webcam (cioè chi ascolta); si ascolta guardando la finestra (così vediamo le espressioni di chi parla). Chi ci parla noterà che lo guardiamo negli occhi quando parliamo, e guardiamo lui/lei quando ci parla. E tutto andrà meglio.

Cristina, quando vuoi, sono pronto!

<link>Lucio Bragagnolo</link>lux@mac.com