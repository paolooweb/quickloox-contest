---
title: "La parte per il tutto"
date: 2010-12-11
draft: false
tags: ["ping"]
---

Condivido che l'informazione sulla necessità di portare la propria password MobileMe <a href="http://www.setteb.it/apple-decide-che-la-vostra-password-deve-essere-di-8-caratteri-10201" target="_blank">sia stata inadeguata</a>.

Condivido meno che chi aveva una password corta o debole si sia lamentato di doverne adottare una più lunga. Una eventuale notizia non sarebbe <i>è stata violato l'account di uno che aveva la password troppo semplice o troppo corta</i>, bens&#236; <i>è stato violato MobileMe, sicurezza Apple a rischio</i>.

Se titolassero sui <i>media</i> di tutto il mondo <i>la sicurezza di Carlo Rossi è debole</i>, sono sicuro che Carlo Rossi si affretterebbe a mettere in sicurezza i propri <i>account</i> e quelli della famiglia, giusto per essere sicuro che i ladri di carte di credito non si concentrino sulle sue coordinate. A maggior ragione se Carlo Rossi vendesse servizi su Internet; si preoccuperebbe anche delle password dei propri clienti, giusto per non fare brutta figura per conto terzi.