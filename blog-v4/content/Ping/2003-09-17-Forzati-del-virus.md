---
title: "Forzati del virus"
date: 2003-09-17
draft: false
tags: ["ping"]
---

Quelli che usano Windows sono un danno per la comunità

Nelle università americane, che ricominciano l’attività dopo l’estate, sta succedendo quello che è già successo in migliaia di aziende, americane ma anche italiane e di tutto il mondo: legioni di studenti arrivano con il loro bel portatile Windows, rimasto spento durante le vacanze, ma non prima di essersi preso l’ennesimo virus.

Legioni di studenti si collegano alle reti con il loro portatile contagiato ed ecco una bella alluvione di messaggi generati dai virus. Inoffensivi per i Mac, ma comunque una bella scocciatura se arrivano a decine al giorno.

Meraviglioso sistema, Windows: possiede il 90 per cento del mercato ma riesce a infastidire il 100 per cento dell’utenza, grazie a un uno per cento di sprovveduti.

<link>Lucio Bragagnolo</link>lux@mac.com
