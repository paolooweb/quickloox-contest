---
title: "Mannaggia, funziona!"
date: 2006-05-29
draft: false
tags: ["ping"]
---

Ricevo da <strong>Alex</strong> e pubblico integralmente, ch&eacute; merita.

<cite>Lo so, sono anche &ldquo;capace&rdquo; di &ldquo;fare&rdquo; siti web, ma non ho pi&ugrave; n&eacute; il tempo n&eacute; la voglia. Cos&igrave; per presentare il mio progetto ho convertito in 25 minuti da Pages la cartella stampa del mio prossimo lavoro <a href="http://www.videofaker.com/kds/index.html" target="_blank">copiandincollando in iWeb</a>.</cite>

<cite>Non &egrave; un capolavoro, ha ancora delle cose da aggiustare, ci sono alcune funzionalit&agrave; che mi stanno letteralmente sulle p&hellip; ma, mannaggia, funziona!</cite>

Credo che per un ingegnere software di Apple potrebbe essere un bel complimento.