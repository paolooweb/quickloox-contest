---
title: "È (non) tutta pubblicità"
date: 2008-07-04
draft: false
tags: ["ping"]
---

Giuro che io non lo volevo fare, ma <a href="http://www.atworkgroup.net/iPhone_invito2.php" target="_blank">me l'hanno organizzata</a> e a un certo punto ho detto s&#236;.

Il libro è un pretesto, ci si creda o meno. Sarà bello vedersi per fare due chiacchiere e un sacco di scoperte. Sto già passando le notti a vedere se e come riesco a installare la beta del software 2.0 sul vecchio iPhone&#8230;

