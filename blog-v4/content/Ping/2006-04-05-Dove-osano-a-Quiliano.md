---
title: "Dove osano a Quiliano"
date: 2006-04-05
draft: false
tags: ["ping"]
---

Non posso proprio non fare notare che Macworld Italia e All About Apple hanno unito le forze per organizzare una <a href="http://www.allaboutapple.com/ospiti/naviglio_viaggi.htm" target="_blank">gita al museo Apple di Quiliano</a>?

Il museo &egrave; il pi&ugrave; grande al mondo e ha ricevuto le lodi della stessa Apple, ch&eacute; neanche in California esiste qualcosa del genere. Della gastronomia ligure non star&ograve; a dire. Insomma, una gran bella occasione.