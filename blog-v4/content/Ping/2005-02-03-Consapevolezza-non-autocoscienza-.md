---
title: "Autocoscienza? No, consapevolezza<p>"
date: 2005-02-03
draft: false
tags: ["ping"]
---

Il Mac OS X dei miei sogni è quello che può fare a meno di me<p>

Recentemente ho esaurito lo spazio a disposizione del disco rigido. Anche se Mac OS X invita con vigore a chiudere applicazioni per liberare memoria virtuale, è sbagliatissimo; rischiano di andare in vacca le preferenze. Invece prima bisogna liberare spazio su disco.<p>

Invece di invitarmi a chiudere applicazioni, Mac OS X avrebbe potuto chiedermi &ldquo;non ho più spazio libero su disco; posso vuotare il Cestino, che è pieno?&rdquo; Mi sarei sentito motlo coccolato.<p>

Un&rsquo;altra cosa che succede nella stessa situazione è che, a disco stracolmo, per qualche motivo va in vacca qualcosa nelle preferenze del suono, e il microfono incorporato non mi sente più. Mi ha chiamato un amico in Skype ma non potevo parlargli. Un logout e si aggiusta tutto, ma ho sempre trenta programmi aperti e un logout mi costa anche un quarto d&rsquo;ora.<p>

Mi piacerebbe un Mac OS X che individuasse attività nel sottosistema audio, si chiedesse se per caso serva il microfono, conduca un veloce test e dica &ldquo;la regolazione delle Preferenze Suono non corrispondeva alla situazione effettiva, ma ho sistemato tutto&rdquo;.<p>

Vorrei un Mac OS X consapevole del sistema intorno a sé. Ma non autocosciente. Come fantascienza insegna, i computer timorosi di essere spenti diventano imprevedibili.<p>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>