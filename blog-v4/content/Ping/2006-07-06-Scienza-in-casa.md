---
title: "Scienza in casa"
date: 2006-07-06
draft: false
tags: ["ping"]
---

<a href="http://www.rabellino.it/blog/?p=155" target="_blank">Gianugo</a> ha coniato una definizione grandiosa. Fortunato lui, si è comprato in un colpo solo MacBook Pro e Bmw. Nel giro di due settimana ambedue gli acquisti si sono guastati, con vari accidenti.

Si rifletteva insieme sulle solite questioni di incidenza statistica degli&#8230; incidenti, che non è deterministica ma stocastica (in italiano: una cosa che ha un tempo medio fra guasti di trentamila ore non si guasta al termine di trentamila ore, ma in un'ora a caso delle trentamila possibili).

Lui ha pensato un attimo e ha detto <em>più che stocastica, direi sfigastica</em>.

Quando nasce una parola che descrive con precisa sintesi un fenomeno complesso, giù il cappello.