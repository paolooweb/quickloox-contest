---
title: "Terminale… di calcolo"
date: 2003-12-31
draft: false
tags: ["ping"]
---

Bella la Calcolatrice di Panther. Ma, per i fissati della riga di comando…

Vista la Calcolatrice di Panther? È una figata, come dice il mio amizo Enzo. Cambia le valute prendendo i dati di cambio più aggiornati da Internet, ha un nastro virtuale, un mare di funzioni scientifiche eccetera eccetera.

Ma si trova facile: basta guardare nella cartella Applicazioni (Applications).
Invece, come dice la Settimana Enigmistica, forse non tutti sanno che esiste una calcolatrice anche nel Terminale. Si chiama dc, funziona in notazione polacca inversa ed è meglio chiedere man dc prima di iniziare a utilizzarla, altrimenti non si può capire che funziona con uno stack, che ottenere un risultato equivale a fare un pop sullo stack e, soprattutto, che si esce con q.

Esistono anche un linguaggio di programmazione di calcolo (bc) e quella che dovrebbe essere una calcolatrice grafica in X-window (xcalc). Questa però non mi funziona e non so assolutamente perché. Se qualche anima buona ha voglia di aiutarmi sappia che non mi serve assolutamente a niente (c’è già la Calcolatrice, per noi mortali), ma la mia dotazione di curiosità è infinita.

<link>Lucio Bragagnolo</link>lux@mac.com