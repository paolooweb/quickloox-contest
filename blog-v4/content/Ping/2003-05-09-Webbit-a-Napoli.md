---
title: "Webbit: la carica dei sedicimila"
date: 2003-05-09
draft: false
tags: ["ping"]
---

E si replica a Napoli

Ottocento seminari, sedicimila partecipanti, duecento aziende su ventimila metri quadrati di esposizione, serate animate dalle comunità presenti nell’Arena Dove Non Si Dorme Mai e persino il Ministero per l’innovazione e le tecnologie, che ha presentato il Piano nazionale sulla sicurezza per lo sviluppo delle piccole e medie aziende.

Se non sei venuto a Webbit ti sei veramente perso qualcosa. Ma puoi rimediare: il 29 e il 30 maggio, infatti, Webbit sarà per la prima volta a sud, presso la Mostra d’Oltremare a Napoli.

Da non perdere, anche e soprattutto per un utente Mac.

<link>Lucio Bragagnolo</link>lux@mac.com

