---
title: "Pronta cassa"
date: 2009-11-12
draft: false
tags: ["ping"]
---

Bel <a href="http://www.9to5mac.com/chart-apples-growth-343545" target="_blank">grafico</a> dell'andamento delle riserve di denaro liquido di Apple dal 1996 a oggi.

Da notare l'inizio: quando il becero di turno dava Apple sull'orlo del fallimento, o sostiene (si trovano ancora) che Microsoft abbia salvato Apple con l'acquisto di azioni senza diritto di voto per 150 milioni di dollari, l'azienda superava non di poco il miliardo di dollari disponibile pronta cassa.