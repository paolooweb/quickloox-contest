---
title: "Un tasto e&hellip; funzione<p>"
date: 2005-12-31
draft: false
tags: ["ping"]
---

Un caso particolare, una soluzione generale<p>

Un nuovo Mac e, per mille motivi, una vecchia tastiera Usb, priva del tasto per aprire e chiudere l&rsquo;unità Dvd. Ci si arriva lo stesso in mille altri modi, ma se si volesse usare idiosincraticamente solo e soltanto la tastiera, che fare?<p>

Risposta semplice. F12. :-)<p>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>