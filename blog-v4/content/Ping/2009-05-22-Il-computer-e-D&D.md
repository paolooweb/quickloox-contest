---
title: "Il computer per D&#38;D"
date: 2009-05-22
draft: false
tags: ["ping"]
---

All'ultima sessione di Dungeons &#38; Dragons era assente giustificato Piero e lo abbiamo salutato e coinvolto ugualmente usando Skype su iPhone.

Niente di che, se non un punto essenziale: in otto attorno a un tavolo, tra la mappa del santuario sotterraneo, le miniature, decine di cattivi, i manuali, le schede stampate, dadi ovunque, patatine, dolcetti, bere, mangiare, tazze di caffè, bicchieri di carta e concitazione da scontro decisivo l'unico apparecchio certamente in grado di non soffrire tra unto briciole e liquidi e abbastanza piccolo da non affollare ulteriormente la situazione è proprio quello con la tastiera virtuale.