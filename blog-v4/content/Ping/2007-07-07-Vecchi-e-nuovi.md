---
title: "Vecchi e nuovi"
date: 2007-07-07
draft: false
tags: ["ping"]
---

Provare nuovo software ogni tanto è essenziale.

Oramai il mio lettore di Pdf di fiducia è diventato <a href="http://skim-app.sourceforge.net/index.html" target="_blank">Skim</a>. Addio, Anteprima.

Invece, dopo avere provato <a href="http://www.chocoflop.com/" target="_blank">Chocoflop</a> e <a href="http://www.rhapsoft.com/" target="_blank">LiveQuartz Image</a>, ho deciso che per loro ci vuole ancora tempo. Il programma che fa presto e bene tutte le quattro cose che mi servono in campo grafico, e che non ha controindicazioni, rimane l'inarrivabile <a href="http://www.lemkesoft.de" target="_blank">GraphicConverter</a>.

Match pari, invece, tra <a href="http://www.bean-osx.com/Bean.html" target="_blank">Bean</a> e TextEdit. Il secondo performa a perfezione per quanto mi occorre, ma Bean lo supererà sicuramente a breve. Credo.