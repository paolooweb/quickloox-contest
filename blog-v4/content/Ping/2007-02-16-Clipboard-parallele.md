---
title: "Clipboard parallele"
date: 2007-02-16
draft: false
tags: ["ping"]
---

Diciamo che arriva un documento in formato OpenDocument, con dentro immagini. Lo apro con OpenOffice in X11. C'è una immagine che voglio prendere e, per dire, aprire in Anteprima. Come faccio a estrarla dall'ambiente X11 e portarla in ambiente Aqua?

Copia e incolla non funziona. In OpenOffice non trovo il comando giusto per isolare l'immagine. Il drag and drop non funziona&#8230;