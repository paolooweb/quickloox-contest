---
title: "Chi vuol essere miliardario"
date: 2002-03-13
draft: false
tags: ["ping"]
---

Mille milioni di dollari possono non bastare a ritirarsi su un’isola del Pacifico

Forbes.com, sito che se intende, ha recentemente pubblicato la classifica dei ricconi americani e c’è anche il nostro Steve Jobs, alla posizione numero 413, con un patrimonio stimato di 1,1 miliardi di dollari, più o meno 1,2 miliardi di euro. Però non è un buon pretesto per reclamare Mac che costino meno: la maggior parte della ricchezza di Jobs viene dalle azioni di Pixar, quella di Toy Story e Monsters & Co. I Mac, per inciso, hanno il prezzo giusto. A meno di non volerli per forza paragonare ai cassoni metallici in vendita dal cantinaro dietro l’angolo.

<link>Lucio Bragagnolo</link>lux@mac.com