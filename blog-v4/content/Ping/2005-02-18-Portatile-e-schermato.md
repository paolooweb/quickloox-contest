---
title: "Portatile e&hellip; schermato<p>"
date: 2005-02-18
draft: false
tags: ["ping"]
---

Non me ne ero accorto, ma i nuovi annunci Apple contengono più di quanto si dice<p>

Paolo mi ha fatto giustamente notare che i nuovi PowerBook 15&rdquo; (opzionalmente) e 17&rdquo; (di serie), oltre a tutte le altre migliorie, sono anche in grado di pilotare il Cinema Display 30&rdquo;.<p>

Per la maggior parte di noi la cosa è senza importanza o al massimo fa rosicare un po&rsquo;, ma per alcuni professionisti che conosco questa è una vera e propria notizia.<p>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>