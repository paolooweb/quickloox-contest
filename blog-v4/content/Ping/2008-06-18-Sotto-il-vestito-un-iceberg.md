---
title: "Sotto il vestito un iceberg"
date: 2008-06-18
draft: false
tags: ["ping"]
---

Sapevo che su Mac OS X esiste il comando di Terminale <code>cal</code>. Mi è tornato utile più volte (<code>cal 06 2008</code> e <em>oplà</em>, ecco il calendario di giugno; <code>cal -y 2009</code> e <em>voilà</em>, ecco il calendario di tutto l'anno).

Non immaginavo che tra le opzioni del comando ce ne fosse una che va lanciata da un altro comando, <code>ncal</code>. Né che <code>ncal</code> non fosse presente nominalmente su Mac OS X e, meno di tutto, che bastasse un <a href="http://www.macworld.com/article/133587/2008/05/calterminal.html" target="_blank">trucco nemmeno difficile</a> per avere <code>ncal</code> a disposizione.

Certe volte mentre maneggio l'interfaccia grafica, il vestito di Mac OS X, mi sembra di stare sulla punta di un iceberg. Vedo un sasso e sotto c'è una montagna.