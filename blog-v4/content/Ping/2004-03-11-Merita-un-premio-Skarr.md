---
title: "Merita un premio Skarr"
date: 2004-03-11
draft: false
tags: ["ping"]
---

Un film totalmente indipendente e interamente Mac-made

Tanto tempo fa ho parlato in questa rubrica di Skarr, film di Alex G. Raccuglia interamente realizzato su Mac con Final Cut Pro.

Il film era in corso di realizzazione. Ora è pronto: per saperne di più invito a visitare il <link>sito dell’opera</link>http://www.progettoskarr.net.

Per parte mia mi limito a dire che per Alex, e il suo Mac, ci vorrebbe un premio. Come dire, un Oskarr...

<link>Lucio Bragagnolo</link>lux@mac.com