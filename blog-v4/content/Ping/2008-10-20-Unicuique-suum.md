---
title: "Unicuique suum"
date: 2008-10-20
draft: false
tags: ["ping"]
---

A ciascuno il proprio.

I meandri del computer mi interessano molto se li sto studiando, oppure se ho un problema. Altrimenti niente.

<a href="http://www.teleart.org" target="_blank">Andrea Ack</a>, al contrario, vuole avere costantemente tutto sotto controllo e in modo non intrusivo, perché ha il suo bel lavoro da svolgere durante la giornata.

A me Monitoraggio Attività (cartella Utility). A lui <a href="http://ragingmenace.com/software/menumeters/" target="_blank">MenuMeters</a>.