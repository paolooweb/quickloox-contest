---
title: "Da PostScript a Pdf"
date: 2003-12-09
draft: false
tags: ["ping"]
---

In Panther ci hanno pensato e…

Pochi ne parlano perché i discorsi si fermano sempre all’interfaccia o poco più. Ma Panther contiene in tutto e per tutto l’Adobe PostScript Normalizer, cioè il software Adobe incaricato di prendere un file PostScript e farne un Pdf del tutto simile all’originale.

Siccome Quartz, il sistema grafico 2D di Mac OS X, è basato su Pdf, le implicazioni di quanto sopra dovrebbero essere chiare.

Se non lo fossero, vai nel Terminale e digita man pstopdf.

Non ti stupire se nei prossimi mesi apparirà più di un programma che visualizza file PostScript; il lavoro sporco lo ha già fatto Apple.

<link>Lucio Bragagnolo</link>lux@mac.com