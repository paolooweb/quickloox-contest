---
title: "Beati gli ossessionati perché saranno distratti"
date: 2007-01-19
draft: false
tags: ["ping"]
---

Un mio amico ha comprato la macchina nuova e gli hanno detto che fa i centottanta all'ora. Non è mai in pace; si pone continuamente il dubbio che siano veramente centottanta. Si mette in autostrada apposta con il cronometro tra un cartello chilmetrico e l'altro, verifica il raggio delle ruote, confronta tempi e chilometraggi, consulta i forum su Internet. Ma saranno davvero centottanta? Ma sarà proprio un'ora? E le altre macchine stesso modello andranno veloci uguali? E a suon di rodersi il fegato non si gode la macchina. Non capisco neanche tanto perché l'abbia comprata.

Poveraccio il mio amico. Fortuna che non tutti fanno come lui. Pensa se ci fosse gente che si rovina le serate a misurare la velocità della propria connessione Internet invece che godersi le infinite risorse della Rete!

Non ne esistono, grazie al cielo. Esistessero, proviamo a distrarli: <a href="http://www.afhome.org" target="_blank">Francesco</a> per esempio ha scovato <a href="http://packetgarden.com/" target="_blank">PacketGarden</a> e <a href="http://www.geni.com/" target="_blank">Geni</a>. Li trovo affascinanti a un grado notevole (PacketGarden è affascinante anche da installare). Non bastasse, c'è l'arma finale. Mentre aspettava che mamma gli portasse a casa la sua copia di The Burning Crusade, <strong>Dani1990</strong> ha trovato i minigiochi di <a href="http://www.ferryhalim.com/orisinal/" target="_blank">Orisinal</a>. Solo con il primo in alto a sinistra non si può non passare almeno un quarto d'ora. E ci sono tutti gli altri.

Distraiamo gli ossessionati. È un'opera di misericordia assente dal Vangelo solamente perché a quel tempo l'unica rete plausibile era quella da pesca.