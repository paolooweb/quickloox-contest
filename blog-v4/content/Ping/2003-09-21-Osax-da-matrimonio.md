---
title: "Un Osax un po’ ruffiano"
date: 2003-09-21
draft: false
tags: ["ping"]
---

Dove c’era concubinaggio occasionale sta per nascere un legame solido

Consiglio a tutti la frequentazione di Osaxen <link>http://osaxen.com</link>, eccellente hub di riferimento per trovare le Osax più utili. Un’Osax, per chi non lo sapesse, è una estensione che regala comandi nuovi ad AppleScript.

Alla tastiera di Ed Lai si deve KinderShell, Osax a dire poco esplosiva. Finora, infatti, si poteva solo eseguire comandi di shell Unix via AppleScript (do shell script) oppure eseguire AppleScript da Terminale (osascript).

KinderShell invece permette di usare comandi Unix nella sintassi di AppleScript, per esempio con un comando

grep "Apple Computer" in the file list "test.c"

Che sarebbe altrimenti impossibile tanto in AppleScript quanto nel Terminale.

Attualmente KinderShell è poco più di un prototipo ma è solo questione di (poco) tempo prima che tra Unix e AppleScript si realizzi un matrimonio là dove fino a oggi ci sono state solo brevi infatuazioni.

<link>Lucio Bragagnolo</link>lux@mac.com