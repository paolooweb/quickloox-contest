---
title: "Save It Yourself<p>"
date: 2005-09-01
draft: false
tags: ["ping"]
---

Un cieco può guidare un altro cieco, se parla AppleScript<p>

Siamo comunicatori e non tecnici; i nostri sforzi di plasmare la tecnologia invece che descriverla, ancorché genuini, fanno abbastanza ridere. Eppure ci fanno sentire contenti come bimbi.<p>

È successo che un caro mio amico usa TextWrangler. Il miglior editor di testo gratis al mondo (il migliore a pagamento è BBEdit, papà di TextWrangler). L&rsquo;amico, in un momento di disattenzione, ha chiuso il programma e ha detto di non salvare. Così si è fumato un bel pezzettino scritto con tanta fatica.<p>

Era deluso e si è messo a cercare una funzione di registrazione automatica in TextWrangler. Non c&rsquo;è.<p>

Ho compiuto uno sforzo titanico e nel giro di un patetico quarto d&rsquo;ora sono riuscito a mandargli questo:<p>

<code>repeat</code><br>
<code>delay 300</code><br>
<code>tell application "TextWrangler"</code><br>
<code>activate</code><br>
<code>save text document 1</code><br>
<code>end tell</code><br>
<code>end repeat</code><p>

300 è il numero dei secondi passati i quali lo script salverà automaticamente il documento <a href="http://www.barebones.com/products/textwrangler/">TextWrangler</a> in primo piano.<p>

I programmatori veri in ascolto si stanno rotolando dalle risate, ma io sono felice e orgoglioso. Più o meno come loro quando hanno scritto un articolo, intanto che mi rotolo io.<p>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>