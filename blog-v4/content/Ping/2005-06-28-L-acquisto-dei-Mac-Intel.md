---
title: "Ciò che serve quando serve<p>"
date: 2005-06-28
draft: false
tags: ["ping"]
---

La differenza sui piani di acquisto la fa una cosa sola: il buonsenso<p>

Si leggono notizie e sondaggi di ogni tipo sull&rsquo;intenzione degli utenti Mac di comprare o meno una nuova macchina PowerPC nell&rsquo;interregno tra l&rsquo;annuncio dei Mac con processore Intel e la loro presentazione effettiva.<p>

Molti esprimono perplessità su come potranno andare le vendite, visto che tra gli acquirenti potenziali c&rsquo;è il timore di essere lasciati &ldquo;indietro&rdquo; e comprare una macchina &ldquo;vecchia&rdquo;.<p>

Metto tutto tra virgolette perché non capisco il significato che viene dato a queste parole. La ricetta giusta per comprare il Mac (e qualunque altra cosa) è sempre la stessa: compra quello che ti serve quando ti serve e, se non c&rsquo;è in giro quello che ti serve, aspetta che arrivi.<p>

Tutto il resto è chiacchiera.<p>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>