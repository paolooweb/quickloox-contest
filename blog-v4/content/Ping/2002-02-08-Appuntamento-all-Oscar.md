---
title: "Appuntamento all’Oscar"
date: 2002-02-08
draft: false
tags: ["ping"]
---

A sentire gli ignoranti e i superficiali sembra che Apple faccia computer a uso e consumo dei grafici e degli impaginatori. Invece...

Come è stato ampiamente sottolineato nel corso della manifestazione Real Video Fx tenutasi all’hotel Marriott di Milano e anche a Roma, Apple si è recentemente aggiudicata un Emmy Award e un Grammy Award per il contributo fornito a progetti vincenti in campo musicale e cinematografico. La nuova Cinecittà, nel rinnovamento che sta seguendo la privatizzazione della Mecca del cinema italiana, ha rivolto la sua attenzione alla piattaforma Mac. E sì che a sentire alcuni disinformati i computer della Mela servano solo a disegnatori e impaginatori.
Macintosh, di spettacolare, non ha solo la user experience, ma anche - in tutti i sensi possibili - i risultati. A quando l’Oscar?

<link>Lucio Bragagnolo</link>lux@mac.com