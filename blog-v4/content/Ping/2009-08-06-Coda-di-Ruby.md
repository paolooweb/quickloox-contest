---
title: "Coda di Ruby"
date: 2009-08-06
draft: false
tags: ["ping"]
---

Nel <a href="http://www.macworld.it/blogs/ping/?p=2708" target="_self"><i>post</i></a> dedicato a Ruby mancava la menzione del <i>tutorial</i> di programmazione più interattivo e immediato forse mai realizzato su Internet.

È proprio su Ruby e non l'ho elencato perché non funzionava. Oggi però è a posto e non posso esimermi, ne vale troppo la pena.

<a href="http://tryruby.hobix.com/" target="_blank">Try Ruby!</a> è in inglese e mette a disposizione un ambiente Ruby dentro la finestra del browser. Scrivendo <code>help</code> si comincia il tutorial. Un quarto d'ora interessante e si impara anche.