---
title: "Perché Mac OS 9.2.2?"
date: 2001-12-08
draft: false
tags: ["ping"]
---

Nel momento in cui scrivo è arrivato Mac OS 9.2.2. A uno che decide di applicare o meno l’update potrebbe anche importare di sapere che cosa fa o non fa. Solo che Apple non ha pubblicato alcuna documentazione in merito e, per quanto riguarda Mac OS 9, non lo fa da dopo l’uscita di Mac OS 9.1.
9.2.2 non è neanche un aggiornamento da poco: da 21,2 a 22,9 megabyte secondo la lingua scelta, contenente più di sessanta file tra i quali nuove versioni di Authoring Support, OpenGl, Usb, AppleScript, Mac OS Rom, Apple Audio Extension, DrawSprocket, IrDa, Ethernet, Open Transport, FireWire e AppleShare.
Si capisce che prima dell’uscita di un prodotto si cerchi il riserbo, per evitare che i siti di pettegolezzo alimentino confusione inutile; ma dopo che è uscito, un minmo di documentazione è un servizio apprezzato.
Mac OS 9.2.2 è sicuramente una buona cosa: ma ditemi perché.

lux@mac.com

http://docs.info.apple.com/article.html?artnum=75186