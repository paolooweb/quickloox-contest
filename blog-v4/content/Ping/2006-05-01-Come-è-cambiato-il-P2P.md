---
title: "Come è cambiato il P2P"
date: 2006-05-01
draft: false
tags: ["ping"]
---

Erano mesi e mesi e ancora mesi che non aprivo un programma di P2P. Poi ieri sera ho visto al cinema un film eccezionale, <a href="http://www.diegrossestille.de/english/" target="_blank">Il Grande Silenzio</a>.

Sono andato sul sito a vedere se mi vendevano il Dvd, che però non c’è ancora.

Allora ho aperto <a href="http://www.acquisitionx.com/" target="_blank">Acquisition</a> per vedere se trovavo, intanto un DivX. Ho scaricato subito un aggiornamento e poi mi si è aperto un programma molto migliore di quello che avevo lasciato, con capacità di filtrare i risultati, isolare lo spam del P2P e varie altre cose.

Beh. Per accorgersi di quanto progredisce il software non c’è che ignorarlo per un tempo abbastanza lungo.