---
title: "Vedi o'Newton quant'è bello"
date: 2006-08-16
draft: false
tags: ["ping"]
---

Non è sensazionale che ci siano in giro videocast su come <a href="http://www.tuaw.com/2006/08/15/newton-video-guides-available-as-downloads/" target="_blank">installare Newton Os</a> sopra hardware indipendente? In fin dei conti, per la gente superficiale è roba morta da dieci anni (non dirlo al mio MessagePad 2100 però).