---
title: "Gamma arabica"
date: 2009-07-13
draft: false
tags: ["ping"]
---

A proposito della pagina Apple contenente le <a href="http://www.apple.com/macosx/refinements/enhancements-refinements.html" target="_blank">novità di Snow Leopard</a> e non ancora tradotta, anche oggi c'è una novità.

<b>Gamma 2.2</b>

La gamma di monitor predefinita è stata cambiata da 1,8 a 2,2 per servire meglio le esigenze di colore di produttori e consumatori di contenuti digitali.

Per il gusto del colore che ho, è arabo e mi fido sulla parola.