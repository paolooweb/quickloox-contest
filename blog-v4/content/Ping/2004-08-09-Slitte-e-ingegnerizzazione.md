---
title: "Ingegerizzazione della slitta"
date: 2004-08-09
draft: false
tags: ["ping"]
---

Il bello di Apple sta nei particolari

Non mi piace trattare l’hardware e lo tocco il meno possibile, ma mi sono trovato a dover installare due dischi rigidi dentro un G5. Ci sono due slitte, una sopra l’altra, vicino all’angolo superiore destro del lato della macchina.

A guardarle non ci sono problemi a infilare il disco inferiore, mentre quello superiore sembra non entrare; l’arrotondamento dei bordi della lamiera (così non ci si può fare male) di fatto ostruisce l’angolo e rende impossibile inserire il disco.

Ma in pochi secondi ci si accorge che le guide delle due slitte si incrociano. Per inserire il disco superiore lo si infila nella slitta in basso e basta seguire la guida interna perché scivoli con grazie ed eleganza in posizione. Dopo di che il disco inferiore è questione di un attimo.

Gli ingegneri hanno pensato anche a questi sciocchi particolari. Che, sommati, rendono un Mac inimitabile.

<link>Lucio Bragagnolo</link>lux@mac.com