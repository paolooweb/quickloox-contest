---
title: "Guarda, senza mouse!<p>"
date: 2004-09-18
draft: false
tags: ["ping"]
---

Come toccare (e sporcare) un trackpad il meno possibile<p>

Quest&rsquo;estate ho frequentato posti ancora più caldi del solito e con umidità altissima. Lavorando (ahimé) con il portatile ho constatato a malincuore che tra il sudore e l&rsquo;umidità diventava meno agevole del consueto usare il trackpad. Non avevo un mouse con me ma una trackball, disgraziatamente anch&rsquo;essa sensibile al fenomeno.<p>

A un certo punto ho abilitato dalle Preferenze di Sistema l&rsquo;accesso totale via tastiera; Control-F2 e scendono i menu, Control-F3 e si lavora sul Dock, Comando-Tab per muoversi tra programmi, Exposé per spostarsi tra le finestre. Qualche altro comando personalizzato, un paio di stupidi AppleScript e improvvisamente la produttività è tornata elevata, senza annaspare sul trackpad o lavare la sfera della trackball ogni due ore.<p>

Situazione limite, certo; ma anche in questo caso Mac OS X si dimostrato all&rsquo;altezza.<p>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>