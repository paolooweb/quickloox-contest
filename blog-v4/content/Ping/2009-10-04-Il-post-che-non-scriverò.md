---
title: "Il post che non scriverò"
date: 2009-10-04
draft: false
tags: ["ping"]
---

Avevo cominciato a scrivere raccontando la mia frequentazione occasionale di un blog frequentato da innamorati di Windows e di come una maggioranza silenziosa, educata e intelligente sia ostaggio di una manciata di urlatori disinformati e massimalisti.

Ho cancellato tutto e mi sono messo a scrivere della follia di Palm, che per sincronizzare il Pre con iTunes lo maschera tecnicamente come se fosse un apparecchio Apple, violando le regole dell'Usb Forum e condannandosi all'irrilevanza o peggio, là dove Rim ha creato una ottima utility di sincronizzazione che si aggancia a iTunes senza problemi e in modo lecito.

Non mi piaceva e ho provato a sintetizzare certi <i>gossip</i> che vogliono arrivare un iMac con lettore Blu-ray, ma forse s&#236;, ma forse no, ma forse s&#236;, ma forse no, ma forse, nel vuoto pneumatico di informazione e nell'eccesso evidente di tempo libero.

Al che ho provato a prefigurare i titoli degli stessi siti, quando il 19 ottobre Apple pubblicherà i risultati finanziari e questi appariranno stellari rispetto a quelli precedenti, sia perché sono buoni ma anche perché sono cambiate le regole contabili, ma nessuno se ne ricorderà.

Poco incisivo, quest'epoca non ha memoria. Ho buttato già due righe sul fatto che le barre di progressione di Snow Leopard hanno guadagnato tridimensionalità, ennesima rifinitura di cui non mi ero accorto prima. Ininfluente.

Sto pasticciando con Automator per creare un servizio capace di accorciare un Url lungo (leggi: uno lo ha già fatto, lo devo modificare, mi ci vorrà un mese). Non ho ancora finito.

Insomma, un <i>post</i> oggi non c'è.