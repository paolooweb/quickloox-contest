---
title: "Google Sente"
date: 2009-11-11
draft: false
tags: ["ping"]
---

Solo Google, oggi, può avere ambizione, incoscienza e peso sufficienti per lanciare nientepopodimeno che <a href="http://golang.org/" target="_blank">un nuovo linguaggio di programmazione</a>.

Dettaglio ancora più ambizioso e incosciente, Go nasce per Linux e per Mac OS X. Una volta sarebbe stato inconcepibile. I tempi cambiano. Naturalmente è <i>open source</i>.

Oltre a derivare evidentemente dall'azienda fondatrice ed essere verbo inglese a tutti noto, <i>Go</i> è anche il <a href="http://www.figg.org/" target="_blank">gioco di scacchiera</a> più difficile e affascinante mai concepito. Chi ne cadesse positivamente vittima ha a disposizione opzioni software di ogni tipo, dalla <a href="http://www.sente.ch/software/goban/" target="_blank">scrivania</a> all'immediatezza del <a href="http://361points.com/computergo/#gnugo" target="_blank">gioco nel browser</a>.

<i>Sente</i> è un termine del Go, il cui significato lascio scoprire a chi vorrà. Situazione ricorsiva, perché chi ci proverà davvero avrà dimostrato di padroneggiare la parola prima ancora di averne colto il senso. Complimenti in anticipo.