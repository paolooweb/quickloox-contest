---
title: "I Giochi per abili"
date: 2009-08-10
draft: false
tags: ["ping"]
---

Ho scoperto che sono iniziati i XXI Giochi Olimpici dell'Informatica e che tutte le prove proposte ai partecipanti &#8211; studenti liceali &#8211; <a href="https://online.ioi2009.org:8443/" target="_blank">si trovano anche online</a>.

Se piove in vacanza, ma veramente molto, può essere un diversivo. :-)