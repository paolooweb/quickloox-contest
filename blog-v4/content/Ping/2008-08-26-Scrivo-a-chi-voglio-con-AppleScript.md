---
title: "Scrivo a chi voglio con AppleScript"
date: 2008-08-26
draft: false
tags: ["ping"]
---

Mille grazie a <a href="http://boldlyopen.com/" target="_blank">Gianugo</a> per questo AppleScript istruttivo e pure significativo, nel senso che fa davvero qualcosa di buono.

Gli ingredienti sono Mail, Script Editor e un paio di file di testo. Uno sarà il messaggio che dobbiamo inviare, l'altro l'elenco degli indirizzi di posta elettronica. Rigorosamente un indirizzo per riga.

Inizialmente lo script chiede che account di Mail usare. Poi chiede quale sarà l'oggetto della mail e poi l'indicazione del file con dentro il messaggio e di quello con gli indirizzi. Poi lavora e crea uno per uno tutti i messaggi. Funziona sia con Tiger che con Leopard (due versioni diverse di Mail). Eccolo:

<code>tell application "Mail"
	activate
	set allAccounts to name of every account
	choose from list allAccounts with title "Scegli l'account da usare&#8230;"
	set theAccount to result as string
end tell
set subjectDialog to display dialog "Digita l'oggetto della mail" default answer "nessun oggetto"
set theSubject to text returned of subjectDialog
set theText to (choose file with prompt "Scegli un file di testo contenente il corpo del messaggio")
set theContent to read theText
tell application "Finder"
	set addresses to paragraphs of (read (choose file with prompt "Scegli un file di testo che contiene gli indirizzi email, uno per riga"))
end tell
tell application "Mail"
	activate
	set activeAccount to account theAccount
	repeat with i from 1 to (the length of addresses)
		set newMessage to make new outgoing message with properties {account:activeAccount, subject:theSubject, content:theContent}
		tell newMessage
			set sender to ((full name of activeAccount &#38; " <" &#38; email addresses of activeAccount as string) &#38; ">")
			make new to recipient at end of to recipients with properties {address:(a reference to item i of addresses)}
			set visible to true
		end tell
		--la prossima riga, se non commentata, spedisce subito i messaggi, altrimenti li prepara e basta
		--send newMessage
	end repeat
end tell</code>

Gli spunti di apprendimento sono cos&#236; numerosi che ci si perde. Intanto lo script vale una lezione sulle finestre di dialogo: come si aprono, che cosa ci si mette, come si memorizza l'input umano, come si fa a chiedere la selezione di un file eccetera.

La seconda metà dello script inoltre passa un file riga per riga compiendo una serie di azioni predeterminate per ciascuna riga. Un ciclo, insomma. Tutto da studiare.

E anche di più. Gianugo ha pubblicato sul proprio blog la <a href="http://boldlyopen.com/2008/08/26/poor-mans-mail-merge-in-apple-mail/" target="_blank">versione aggiornata dello script</a>. Esercizio aggiuntivo: che cosa c'è di più, rispetto a quanto sopra?