---
title: "È un attimo"
date: 2007-10-03
draft: false
tags: ["ping"]
---

Per quanto riguarda iPhone: per sapere se lo ha Telecom, oppure no, bisogna attendere una certa loro convention dirigenziale prevista entro fine mese. Se lo dicono l&#236;, s&#236;, altrimenti no. Forse riuscirò a saperne di più.

Per quanto riguarda iPhone: più lo uso (rubato temporaneamente a Fab in Mac@Work) e più mi convinco. Gli ovvietari sanno solo dire <em>non-ha-Umts</em>. Se qualcuno se lo ricorda, il primo Mac non aveva un sacco di cose. Ma l'integrazione, l'interfaccia, la facilità d'uso erano mai viste prima. Non so il mese prossimo, o l'anno prossimo. Qui, adesso, non c'è partita.

Per quanto riguarda iPhone, per <strong>Paolo / deart</strong>: il mio commento è che, non ricordo più su che documento ufficiale Apple, si dice che iPhone accetta solo programmi realizzati in JavaScript.

<em>Currently</em>.