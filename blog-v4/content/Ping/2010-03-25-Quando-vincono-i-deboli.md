---
title: "Quando vincono i deboli"
date: 2010-03-25
draft: false
tags: ["ping"]
---

Ho apprezzo molto il <a href="http://www.tuaw.com/2010/03/25/screenshot-plus-and-iwork-the-poor-mans-screenshot-editing-sui/" target="_blank">post su The Unofficial Apple Weblog</a> dedicato all'uso di iWork più una qualsiasi applicazione per acquisire schermate come <i>poor man's screenshot editing suite</i>, ossia sistema alla buona per modificare le schermate.

L'articolo menziona <a href="http://www.tarcolesfilmarts.com/software/" target="_blank">Screenshot Plus</a> per le schermate e nei commenti si fa riferimento anche a <a href="http://projects.digitalwaters.net/index.php?q=instantshot" target="_blank">InstantShot</a>. In Anteprima e a sé stante dentro la Cartella Utilities c'è ovviamente anche Grab.

Viene fuori che le possibilità di iWork di aggiustare colori, creare maschere, generare il canale alfa sono certamente elementari ma più che sufficienti in un sacco di situazione. E alla fine si butta tutto in Anteprima, per registrarlo in una quantità di formati certamente non universale come sa fare <a href="http://www.lemkesoft.de" target="_blank">GraphicConverter</a> e però adeguatissima per la quotidianità.

Photoshop e compagnia sono strumenti indispensabili al professionista. Il resto di noi se la può cavare quasi sempre in modo più semplice. Quel docente universitario del Politecnico che ripeteva di imparare a usare gli strumenti deboli, prima di ricorrere a quelli forti, disse un sacco di altre cose. Questa è l'unica che continua a essermi utile ogni giorno.