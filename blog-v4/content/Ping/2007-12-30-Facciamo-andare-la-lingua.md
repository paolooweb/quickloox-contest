---
title: "Facciamo andare la lingua"
date: 2007-12-30
draft: false
tags: ["ping"]
---

The Unofficial Apple Weblog ha fatto partire una serie di piccoli e accessibili <a href="http://www.tuaw.com/2007/12/27/applescript-the-script-editor/" target="_blank">tutorial su AppleScript</a>, che sono davvero semplici, compatti e ben fatti.

A qualcuno può interessare una loro resa in italiano? Se ci fossero abbastanza s&#236; da giustificare la fatica, sarebbe un bel modo di iniziare il nuovo anno.