---
title: "Pizza e giornalismo"
date: 2010-09-27
draft: false
tags: ["ping"]
---

Mi segnala <a href="http://www.bluteam.net/GalleriaFoto%20Mondo%20S-Bragagnolo.htm" target="_blank">Lorescuba</a> questo <a href="http://www.corriere.it/notizie-ultima-ora/Cronache_e_politica/Apple-Italia-Pizza-mafia/27-09-2010/1-A_000118278.shtml" target="_blank">incredibile trafiletto del Corriere della Sera</a>.

Siccome in una delle <a href="http://www.corriere.it/notizie-ultima-ora/Cronache_e_politica/Apple-Italia-Pizza-mafia/27-09-2010/1-A_000118278.shtml" target="_blank">267.353 app per iOS </a>, dal titolo di <a href="http://itunes.apple.com/it/app/what-country/id380972782?mt=8" target="_blank">What Country</a>, si parla di mafia in associazione con l'Italia (come se non avessimo il problema), allora <i>Apple</i> identifica l'Italia con la mafia.

Per parafrasare, se entriamo in una biblioteca e prendiamo a prestito un libro erotico, vuol dire che il bibliotecario è un pervertito.

Sempre se fosse vero, bisognerebbe concludere che il Corriere della Sera è scritto da incompetenti. Invece non è affatto vero, anche se ora abbiamo la certezza che almeno uno se ne trova.