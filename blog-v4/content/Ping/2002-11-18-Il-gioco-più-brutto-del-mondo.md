---
title: "Il gioco più brutto del mondo"
date: 2002-11-18
draft: false
tags: ["ping"]
---

Dinosauro sopravvissuto agli anni Sessanta, strega come nessun Diablo potrà fare

Un mio conoscente si lamentava del fatto che, qualsiasi gioco esca, è sempre più bello di tutti quelli che lo hanno preceduto ma invariabilmente spreme come un limone anche il Mac più recente e potenziato. Sono i giochi di adesso, fantastici per immergervisi dentro con i sensi, costruiti per richiedere la scheda video della prossima generazione e forse un po’ tutti uguali a se stessi.

Per consolarlo gli ho dato una dritta e gli fatto scaricare il “gioco più brutto del mondo”, come lo ha detto appena lo ha visto. Angband è un derivato di Rogue, il primo gioco di ruolo della storia. Si giocava sui terminali delle università degli anni Sessanta, talmente poveri che tutto – scenari, tesori, mostri – veniva rappresentato con banali caratteri ASCII.

Solo che in quasi quarant’anni di sviluppo Rogue – e con lui Angband – sono diventati di una complessità e di una ricchezza inimmaginabili per qualsiasi gioco moderno, nato per morire entro una stagione. Il mio conoscente ha snobbato il “gioco più brutto del mondo” e poi, morto la prima volta, ha voluto riprovare. Ora da lì non lo stacca più nessuno e non vede l’ora di penetrare fino al centesimo livello del sotterraneo per sconfiggere Morgoth. Anche perché <link>Angband</link>http://www.thangorodrim.net lo aspetta, e risponde istantaneamente. Che volere di più?

<link>Lucio Bragagnolo</link>lux@mac.com