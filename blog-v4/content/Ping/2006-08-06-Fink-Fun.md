---
title: "Fink Fun"
date: 2006-08-06
draft: false
tags: ["ping"]
---

In una notte d'estate, quando tutti dormono già e non hai voglia di fare assolutamente niente, solo che non hai sonno, poche cose sono meglio che digitare <code>fink selfupdate</code>, lasciargli la password e passare qualche minuto a vedere scorrere istruzioni nel Terminale, chiedendosi senza impegno che cavolo succede. Per chi non aggiorna almeno da luglio dovrebbero succedere cose interessanti. Nel dubbio: può anche darsi sia necessario scaricare un aggiornamento da Web, aggiornare Xcode, insomma, passare una notte in bianco.

Se proprio il sonno non arriva, ci si concede anche un succo di frutta e <code>fink update-all</code>. Posto che si sia già installato <a href="http://fink.sourceforge.net" target="_blank">Fink</a>, chiaro.