---
title: "Non più negozi"
date: 2004-08-02
draft: false
tags: ["ping"]
---

Settembre andiamo, è tempo di Apple Center

Periodicamente Apple riorganizza la rete di distribuzione dei suoi prodotti, sfrondando l’elenco da quanti svolgono un cattivo servizio ad Apple ma, talvolta, anche a loro stessi. Stavolta è stata anche più radicale del solito: i punti vendita Apple più meritevoli in Europa, in assenza di Apple Store ufficiali (con l’eccezione di Regent Street a Londra), diventano Apple Center.

L’aspetto sarà simile a quello degli Apple Store, però. L’arredamento sarà coerente e rinnovato. L’unica differenza è che rimarranno punti vendita indipendenti, non posseduti da Apple ma da gente che crede nella sfida di vendere computer diversi da quelli che danno soddisfazione solo nel prezzo stracciato.

In Italia i primi Apple Center sono tre. Di uno di questi, <link>Mac@Work</link>http://www.macatwork.net, sono socio (di estrema minoranza) e ne sono molto orgoglioso. Garantisco che i soldi non c’entrano proprio.

<link>Lucio Bragagnolo</link>lux@mac.com