---
title: "Ricadute di stile"
date: 2010-09-01
draft: false
tags: ["ping"]
---

Ho sentito sostenere che Mac non è una piattaforma professionale.

In autunno esce AutoCad 11 per Mac.

Ho sentito sostenere che iPad e compagnia sono apparecchi per il consumo e non per la produzione.

In autunno esce AutoCad Ws per visionare, modificare e condividere file Dwg &#8211; quelli di AutoCad &#8211; su iPad, iPhone e iPod touch.

Ho sentito sostenere che Apple, allargando la visione oltre Mac, ha tradito la propria clientela classica e si è allontanata dal proprio core business, l'attività fondamentale.

AutoDesk, azienda di informatica classica come poche altre e produttrice del programma per antonomasia di progettazione assistita dal computer, non solo <a href="http://usa.autodesk.com/adsk/servlet/pc/index?id=15421056&amp;siteID=123112" target="_blank">torna a Mac dopo sedici anni di assenza, ma sbarca su tutta la gamma di apparecchi iOS</a>, a dimostrazione che alla clientela classica sono aggeggi che fanno comodo oltremodo.

Se ne leggessi solo uno, di questi soloni, che riconosce onestamente e apertamente di avere detto sciocchezze, sarebbe una bellissima e invidiabile dimostrazione di stile.