---
title: "Del tubo e del Cubo"
date: 2007-07-28
draft: false
tags: ["ping"]
---

Durante uno dei venerd&#236; del gioco si era provato <a href="http://assault.cubers.net/" target="_blank">AssaultCube</a> e, a dirla tutta, era sembrato un gioco del tubo, per incompletezza e imperfezioni.

<strong>Manuel</strong> è stato gentilissimo a segnalarmi <a href="http://sauerbraten.org/" target="_blank">Sauerbraten</a>, riprogettazione dell'originale Cube che funziona molto, molto meglio.

A ringraziamento anche fattuale, sarà uno dei prossimi venerd&#236; del gioco. A settembre, perché durante agosto non ho certezza di connessione sufficientemente robusta (a prescindere dai problemi di venerd&#236; scorso). Comunque qui, per i giochi, la memoria è da elefanti.

Manuel mi ha segnalato anche la pagina dedicata al <a href="http://www.cubeengine.com/" target="_blank">motore grafico Cube</a>, da cui origina tutto. Di nuovo grazie e arrivederci, per i venerd&#236; del gioco, a settembre.