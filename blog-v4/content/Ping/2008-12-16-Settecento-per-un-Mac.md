---
title: "Settecento per un Mac"
date: 2008-12-16
draft: false
tags: ["ping"]
---

Per Macworld cartaceo racconterò in settecento caratteri quale sia il miglior Mac che ho mai avuto, non necessariamente per ragioni tecnologiche quanto per considerazioni personali a qualsiasi livello.

Vuoi provarci anche tu? La redazione promette che le storie migliori verranno pubblicate sul numero di febbraio, in edicola fine gennaio.

Se ti va di condividere la tua storia, puoi tranquillamente aggiungerla come commento a questo <em>post</em>. Se vuoi che rimanga confidenziale, scrivimela <a href="mailto:lux@mac.com?subject=Settecento%20per%20un%20Mac" target="_blank">in posta elettronica</a> e io la inoltrerò alla redazione.

Se hai bisogno di una ragione per sentirti motivato, pensa che il 24 gennaio 2009 Macintosh compie venticinque anni.

Requisiti: non più di settecento caratteri, lingua italiana. :)