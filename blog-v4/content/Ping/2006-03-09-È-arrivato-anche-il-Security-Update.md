---
title: "È arrivato anche il Security Update"
date: 2006-03-09
draft: false
tags: ["ping"]
---

Aggiornamento Sistema, impostato sul controllo settimanale, ha segnalato il Security Update 2006-001 di Mac OS X. Aggiornamento installato, tutto a posto, tutto funzionante.

Avvio regolare, richiesto dal sistema e non indesiderato. In 68 giorni (<a href="http://www.iuni.com/en/products/software/mac/iutime/index.html" target="_blank">iuTime</a> docet) i riavvii indesiderati restano due.

Gi&agrave; che c&rsquo;ero ho aggiornato anche <a href="http://www.barebones.com/support/bbedit/updates.shtml" target="_blank">BBEdit</a>.

