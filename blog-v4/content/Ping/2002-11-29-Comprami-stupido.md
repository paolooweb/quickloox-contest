---
title: "Comprami, stupido"
date: 2002-11-29
draft: false
tags: ["ping"]
---

L’unico criterio buono per acquistare un Mac

L’unico criterio buono per acquistare un Mac è: quello che serve, quando c’è. Tutti gli altri criteri – attenzione, attenzione – sono minchiate.

Sento quello che si arrabbia perché ha comprato un Titanium senza Superdrive ed è uscito quello con il Superdrive. Ma perché ha comprato un computer che non aveva quello che gli serviva?

Sento quello che si arrabbia perché ha comprato un Mac e otto mesi dopo hanno rinnovato la gamma, così il suo Mac si è deprezzato. Ma dove vive? Da cinque anni Apple rinnova ogni prodotto prima che siano passati dodici mesi. E i Mac, un po’ di valore, lo conservano. Con Windows va molto peggio.

Sento quello che si arrabbia perché ha comprato un Mac e una settimana dopo è uscito il Mac nuovo. Dovrebbero avvisare che escono i Mac nuovi, dice. Bravo: e quelli che comprassero un Mac una settimana prima dell’avviso? O Apple deve smettere di vendere un anno prima che escano le macchine nuove? Perché non ti sei comprato una macchina che ti fa felice ugualmente?

L’unico criterio per comprare un Mac è: quello che serve, quando c’è. E chi deve per forza avere sempre il Mac più potente possibile, si metta il cuore in pace: dura qualche mese. Sono computer, non rustici ristrutturati.

<link>Lucio Bragagnolo</link>lux@mac.com