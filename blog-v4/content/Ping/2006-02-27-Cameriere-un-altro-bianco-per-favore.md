---
title: "Cameriere, un altro bianchino per favore"
date: 2006-02-27
draft: false
tags: ["ping"]
---

Il premio <em>argomento su cui non c&rsquo;&egrave; assolutamente niente da dire e cui si sprecano fiumi di parole inutili peggio che al bar, senza aggiungere alcunch&eacute; di utile al dibattito, ma in compenso alzando una cortina di fumo che impedisce di capire l&rsquo;essenziale</em> per il 2006 &egrave; assegnato d&rsquo;autorit&agrave; al Trusted Computing. Che Mac OS X non usa in alcun modo significativo per l&rsquo;utente finale, bombardato tuttavia da articoli terroristici in quantit&agrave;.