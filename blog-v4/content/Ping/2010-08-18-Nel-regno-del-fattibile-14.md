---
title: "Nel regno del fattibile / 14"
date: 2010-08-18
draft: false
tags: ["ping"]
---

<cite>pregasi elencare azioni fattibili su un iPad e non fattibili su un Sony Vaio W da 10&#8221;</cite>.

<a href="http://dailymobile.se/2010/08/15/video-ipad-powered-beer-monitor/" target="_blank">Distribuire birra gelata</a> con monitoraggio della temperatura, registrazione dei bevitori e raccolta dell'indice di gradimento.

Realizzato dagli ingegneri di Yelp nella loro annuale <a href="http://officialblog.yelp.com/2010/02/first-annual-yelp-hackathon.html" target="_blank">Hackathon</a>, in cui per 48 ore è possibile realizzare assolutamente qualsiasi progetto, con il consenso della ditta.