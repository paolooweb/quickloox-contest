---
title: "Un Mac per chi non lo ha<p>"
date: 2004-12-07
draft: false
tags: ["ping"]
---

Una iniziativa di beneficienza che mi piacerebbe importare dagli Stati Uniti<p>

I tizi di <a href="http://www.mactreasures.com">Mac Treasures</a> hanno messo a disposizione un indirizzo <a href="mailto:unusual@mactreasures.com">email</a> per chi volesse donare un Mac a chi ne avrebbe bisogno, ma non può permetterselo.<p>

Se qualcuno fosse a conoscenza di un&rsquo;iniziativa analoga italiana, questa modesta rubrichetta è più che disposta a segnalarla e fare la sua piccola parte.<p>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>