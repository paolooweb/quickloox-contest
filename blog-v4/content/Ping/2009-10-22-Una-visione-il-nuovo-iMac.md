---
title: "Una visione: il nuovo iMac"
date: 2009-10-22
draft: false
tags: ["ping"]
---

Ho preso in mano il 21,5&#8221;. La fascia inferiore di non-schermo si è ridotta tipo di un terzo e un osservatore distratto fatica a distinguere un <a href="http://www.apple.com/it/imac/" target="_blank">iMac</a> attuale dal un Cinema Display, anche perché la linea si è nuovamente assottigliata. C'è una leggera bombatura sul retro, al centro, ma la vista di profilo è suggestiva.

Non ho avuto modo di accenderlo e metterlo alla prova. Lo schermo da 21,5&#8221; sembra bello e oltretutto è 16:9, cosa che pare non abbia nessun altro (tutti 16:10, si dice, ma non ho verificato).

Ora la tastiera di serie è quella senza fili. Piccola novità, funziona con due batterie al posto di tre.