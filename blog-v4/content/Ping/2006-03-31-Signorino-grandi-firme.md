---
title: "Signorino grandi firme"
date: 2006-03-31
draft: false
tags: ["ping"]
---

Ho deciso che la gestione delle signature postali di <a href="http://www.barebones.com/products/mailsmith" target="_blank">Mailsmith</a> non &egrave; abbastanza flessibile per come la voglio io, che gestisco una marea di account e voglio diversi insiemi di signature random secondo l&rsquo;account e secondo la situazione.

Ho trovato un thread di una mailing list americana dove si racconta come riuscire a fare una cosa del genere pasticciando con Perl. Mailsmith &egrave; limitato nella gestione delle signature ma molto potente nel glossario, quindi mi ci metter&ograve;.

Date le mie competenze di Perl, necessito di tifo e sostegno morale. :-)