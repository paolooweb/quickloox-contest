---
title: "Non si butta via niente"
date: 2009-09-27
draft: false
tags: ["ping"]
---

<a href="http://web.mac.com/manuelmagic/" target="_blank">Manuel</a> mi ha inviato una segnalazione straordinaria, relativa al <a href="http://cssfad.unile.it/" target="_blank">Campus satellitare del Salento</a> e ai suoi <a href="http://cssfad.unile.it/course/index.php" target="_blank">corsi online</a>, che comprendono anche Mac OS X e altri temi di interesse su Mac.

Solo che ho combinato uno dei miei pasticci con la posta e la sua segnalazione è freschissima, di trenta mesi fa.

Visto che occorre una registrazione gratuita e nient'altro, e che i corsi sono ancora disponibili, invito a provare. Anche dove i contenuti sono invecchiati c'è molto da apprezzare.