---
title: "In fila per mille"
date: 2009-06-21
draft: false
tags: ["ping"]
---

Diciamo che ho un lungo elenco di indirizzi web in un documento di testo e voglio vedere se tutti funzionano.

Ci sono programmi già fatti allo scopo. Per esempio <a href="http://www.coriolis.ch/en/sbc/" target="_blank">SafariBookmarkChecker</a> lavora in questo modo con i Preferiti di Safari.

AppleScript è però più divertente:

<code>tell application "BBEdit"</code>
<code>	repeat with ciclo from 1 to count of lines of text window 1</code>
<code>		select line ciclo of text window 1</code>
<code>		set riga to selection</code>
<code>		set indirizzo to characters 1 thru ((length of riga) - 1) of riga as string</code>
<code>		open location indirizzo</code>
<code>	end repeat</code>
<code>end tell</code>

Lo script è essenziale. Dà per scontato che il documento sia aperto e in primo piano. Funziona su BBEdit e sul gratuito <a href="http://www.barebones.com/products/TextWrangler" target="_blank">TextWrangler</a> (sostituendo la parola BBEdit con la parola TextWrangler). Dà anche per scontato che l'elenco sia a posto e che ci sia un indirizzo per riga.

L'unico flebile segno di intelligenza nello script sta nella riga <code>set indirizzo to&#8230;</code>, che serve a tagliare l'ultimo carattere di ciascuna riga. Infatti BBEdit seleziona tutta la riga, compreso l'ultimo carattere, che è un a capo. Questo va tolto, altrimenti l'indirizzo selezionato sarà scorretto.

Il resto è un ciclo banalissimo. Le sfide aperte sono infinite, dal farlo funzionare su più di un <i>browser</i>, al trattare gli eventuali errori in un elenco, all'aprire dieci tab per finestra di Safari e poi aprire una nuova finestra e via dicendo.