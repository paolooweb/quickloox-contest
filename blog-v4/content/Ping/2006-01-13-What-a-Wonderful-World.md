---
title: "What a Wonderful World"
date: 2006-01-13
draft: false
tags: ["ping"]
---

Cullato da una sdraio ascoltavo la risacca sulla spiaggia in piena notte, guardando le stelle e pensando a quanto siamo piccoli e quanto siamo grandi. Tanti anni fa.

Pensieri banali.

Invece <a href="http://earth.google.com/" target="_blank"></a>Google Earth &egrave; eccezionale. Mi sono quasi commosso alle lacrime.