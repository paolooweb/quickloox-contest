---
title: "Un browser al giorno"
date: 2009-05-26
draft: false
tags: ["ping"]
---

Non di solo Safari vive l'uomo Mac. <strong>Fearandil</strong> mi segnala la <a href="http://build.chromium.org/buildbot/snapshots/sub-rel-mac/" target="_blank">pagina contenente i <em>build</em> di Chromium</a>, la resa indipendente per Mac del <em>browser</em> Chrome di Google, e commenta:

<cite>A me ha fatto paura la velocità di apertura dele pagine e del programma. Non è tanto i Javascript, ma il resto!</cite>

Certo è relativamente facile che un <em>browser</em> nuovo, senza il peso della retrocompatibilità  e di tutte le funzioni necessarie, offra buone prestazioni.

Resta il fatto che la diversità è sommamente positiva. Più <em>browser</em> ci sono, meglio si sta sul web.