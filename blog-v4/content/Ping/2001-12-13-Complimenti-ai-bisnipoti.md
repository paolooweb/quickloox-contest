---
title: "Complimenti ai bisnipoti"
date: 2001-12-13
draft: false
tags: ["ping"]
---

Chi è riuscito a catturare l’update 1.0.1 di QuicKeys? È rimasto online per pochissimo tempo, dopo di che è stato sostituito da un file successivo. Succede in molti casi, ma questo è un po’ speciale: stando a un lettore di MacInTouch, un piccolo errore allungava la durata del funzionamento dimostrativo da 30 giorni a più di 31 mila!
L’unico caso al mondo di programma da lasciare in eredità e che useranno anche i bisnipoti. Come gli orologi di supermarca.

<http://www.cesoft.com/downloads/updates.html>