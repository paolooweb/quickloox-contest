---
title: "Andare sul sicuro"
date: 2011-02-18
draft: false
tags: ["ping"]
---

Sguardo di sgomento all'apprendere che il mio <i>account</i> di <a href="http://www.instapaper.com" target="_blank">Instapaper</a> è senza <i>password</i>.

<cite>E se qualcuno ci entra?</cite> Beh, vedrà che cosa leggo. Lo potrà leggere anche lui. Magari gli piace. Perché dovrei gestire una <i>password</i> in più per un elenco di letture?

<cite>Ma non c'è mai troppo da fidarsi.</cite> Vero. Anche preoccuparsi inutilmente è sbagliato. Se ne vanno energie che andrebbero dedicate invece alla sicurezza quando serve davvero.

Tipicamente la gente che si ossessiona per la sicurezza è quella che ha dati di poca o nessuna importanza. Se tutto deve essere massimamente sicuro, niente conta veramente.