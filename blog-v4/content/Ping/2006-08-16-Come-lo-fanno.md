---
title: "Come lo fanno"
date: 2006-08-16
draft: false
tags: ["ping"]
---

L'opinione che uno può farsi dei programmi che hanno vinto i <a href="http://developer.apple.com/ada/" target="_blank">Design Award Apple per il 2006</a> dipende anche fortemente dalla propria attività. Difficile valutare compiutamente un programma per fare 3D oppure un Automator che semplifica la realizzazione di un catalogo di case per la vendita.

Detto ciò, trovo <a href="http://macromates.com/" target="_blank">textmate</a>, editor rivolto ai programmatori, davvero notevole e forse è quello che mi ha impressionato di più. L'essenza della <em>Mac experience</em> è dare grande potenza in modo semplice da usare ed elegante da osservare. Data una funzione base è facile trovare programmi che la svolgono bene tanto su Windows quanto su Macintosh (o Linux, se è per quello). Ma azzardo che di programmi compiuti come textmate, in giro, ce ne sono pochi. Soprattutto su Windows, dove di programmi che fanno cose si scoppia. Appena arrivi a come lo fanno, non sai se piangere o arrabbiarti.