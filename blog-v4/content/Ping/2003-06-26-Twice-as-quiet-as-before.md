---
title: "Twice as quiet as before"
date: 2003-06-26
draft: false
tags: ["ping"]
---

La specifica più importante del nuovo G5?

Sì, è il primo computer desktop con dentro un processore a 64 bit. Sì, è il personal computer più veloce del mondo. Sì, ha le porte FireWire e Usb dietro ma anche davanti, in modo che siano immediatamente raggiungibili. Sì, sì, sì.

Ma, come ha detto Tom Boger (Director, Power Mac Product Marketing) alla presentazione italiana dei nuovi annunci Apple, il nuovo G5 è due volte più silenzioso del più recente G4: 35 dB in condizioni operative normali. Con nove ventole, ognuna delle quali però - grazie a una tecnologia messa a punto da Apple - gira normalmente a un decimo della velocità di una ventola convenzionale. E questo vale tutti i cicli di clock che vuoi.

<link>Lucio Bragagnolo</link>lux@mac.com