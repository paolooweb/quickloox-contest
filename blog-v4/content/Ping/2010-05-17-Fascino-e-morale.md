---
title: "Fascino e morale"
date: 2010-05-17
draft: false
tags: ["ping"]
---

Incredibile la vicenda dell'appassionato che, memore dei tempi dell'Apple ][, compra un Apple //e al mercato delle pulci e <a href="http://www.atomsandelectrons.com/blog/post/Apple-t.aspx" target="_blank">ci realizza sopra un client per Twitter</a>. Le situazioni dove il vecchio secolo incontra quello nuovo sono sempre affascinanti.

Le parti che mi hanno colpito di più sono quelle del collegamento a un computer moderno tramite un cavo fatto in casa (i dati di Twitter arrivano all'Apple //e dal Pc), la riscrittura di un <i>bootloader</i> di 50 byte in linguaggio macchina, il ricordo delle risoluzioni di Apple II: 40 x 40 pixel a sedici colori oppure 280 x 192 pixel a sei colori, con conseguente scrittura di un programma che trasforma gli avatar di Twitter nelle due modalità.

Al termine di tutto l'articolo si trova anche un <a href="http://www.youtube.com/watch?v=j622EyPX6lM" target="_blank">video</a> che mostra l'accrocchio in funzione. Complimenti veri: date sufficienti cognizioni tecniche, nulla è impossibile.

La morale sta in un paragrafo che traduco qui sotto.

<cite>Quanto venne presentato l'Apple //e, ricordo di avere pensato che fosse un abominio. Il mio Apple ][ originale comprendeva lo schema elettrico completo e la documentazione su come programmarlo. Apple //e era il primo computer di Apple che non conteneva istruzioni su come programmarlo. C'era abbastanza software commerciale disponibile e, come i computer attuali, faceva cose utili senza che il proprietario lo dovesse necessariamente programmare. A quindici anni lo consideravo un cambiamento in peggio e nella mia mente Apple //e era un computer di minor valore.</cite>

Per quanto programmare o scriptare sia appassionante &#8211; e ne sono sostenitore accanito &#8211; più il progresso avanza, più l'uso normale del computer si allontana dalla programmazione, per semplificarsi. Quando sento rimpiangere i bei tempi andati di HyperCard o del Basic penso che basterebbe rimpiangere abbastanza in là per ritrovarci tutti, nel 2010, con computer dove bisogna scriversi un <i>bootloader</i> in linguaggio macchina, con risoluzione 40 x 40 a sedici colori. Affascinante e fantastico, ma no, grazie: preferisco il mio schermo 1.920 x 1.200 con colore a 32 bit e la possibilità, se voglio, di scrivere <code>irb</code> nel Terminale e accendere l'interprete Ruby.