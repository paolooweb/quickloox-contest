---
title: "Testa o croce?"
date: 2008-05-01
draft: false
tags: ["ping"]
---

C'è chi vive di <em>rumor</em>. Pettegolezzi sull'elettronica di consumo come li si fanno con le veline e i calciatori.

Adesso c'è chi vive del <em>rumor</em> per cui a giugno, possibilmente il 9 in occasione della Worldwide Developer Conference, arriva il nuovo iPhone.

Su Fortune scrivono che il nuovo iPhone sarà <a href="http://techland.blogs.fortune.cnn.com/2008/04/29/att-to-cut-the-price-of-apples-new-iphone/" target="_blank">più sottile dell'originale</a>.

Su Engadget scrivono che il nuovo iPhone sarà più <a href="http://www.engadget.com/2008/04/25/the-second-gen-iphone-3g-gps-only-slightly-thicker/" target="_blank">spesso dell'originale</a>.

Capisco chi vive di <em>rumor</em>. Più ne scrive, più fa soldi.

Ma chi sta l&#236; a leggerli, chi li capisce?