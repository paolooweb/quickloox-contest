---
title: "Essere alieno oggi"
date: 2006-05-30
draft: false
tags: ["ping"]
---

<strong>Mario</strong> chiedeva di precisare meglio in che modo Gimp ha un feeling alieno rispetto a Photoshop e al comportamento tipico dei programmi di fotoritocco su Mac.

Non c&rsquo;&egrave; spiegazione migliore di una <a href="http://www.gimpshop.net/" target="_blank">edizione di Gimp</a> fatta apposta per somigliare il pi&ugrave; possibile a Photoshop. Basta confrontarle. :-)