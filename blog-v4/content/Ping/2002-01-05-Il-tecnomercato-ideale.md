---
title: "Il tecnomercato ideale"
date: 2002-01-05
draft: false
tags: ["ping"]
---

Nielsen/NetRatings, la società più affidabile per quanto riguarda le statistiche dei siti Interent più visitati, ha diramato la classifica dei siti di shopping online più frequentati a novembre 2001. Amazon è prima assoluta e incontrastata, con più share da sola dei quattro che la seguono. Ma poi seguono Columbia House (musica), Toys ’R’ Us (giocattoli), Barnes and Noble (libri) e... Apple Computer.
A dimostrazione che le caramelle le porta la Befana, ma la migliore tecnologia online viene venduta con il marchio della Mela.

lux@mac.com

< http://www.businesswire.com/cgi-bin/f_headline.cgi?bw.122001/213540085&ticker=NTRT>