---
title: "Il paradiso del text processing"
date: 2009-11-20
draft: false
tags: ["ping"]
---

Non ho idea di quando sia stata inserita la funzione, ma mi accorgo che <a href="http://www.barebones.com/support/bbedit/updates.html#up9" target="_blank">BBEdit 9.3</a> aggiorna e mostra in tempo reale il conteggio di caratteri e parole. Era l'unica cosa di cui sentivo la mancanza e adesso uso ancora meno le alternative.

Non ho trovato l'aggiunta nell'<a href="http://www.barebones.com/support/bbedit/arch_bbedit93.html" target="_blank">elenco dei cambiamenti</a>, che tuttavia resta fulgido esempio di quanto valgano il programma e il supporto che c'è dietro.