---
title: "Much ado about nothing*"
date: 2006-02-16
draft: false
tags: ["ping"]
---

Uno smanettatore di quelli veramente bravi ha sudato per settimane ed &egrave; finalmente riuscito a <a href="http://maxxuss.hotbox.ru/" target="_blank">far funzionare Mac OS X 10.4.4 su un computer non Apple</a>.

Contemporaneamente <a href="http://www.theinquirer.net/?article=29710" target="_blank">The Inquirer</a> ha iniziato a sostenere che Windows non girer&agrave; mai ufficialmente sui Mac.

Adesso ci vuole un altro update per fare girare su computer non Apple Mac OS X 10.4.5, e se l&rsquo;Inquirer scrive qualcosa significa quasi certamente che non &egrave; vero, ma il punto &egrave; un altro: sono due modi straordinariamente simmetrici di buttare il tempo.

*Molto rumore per nulla, <em>according to</em> William Shakespeare.