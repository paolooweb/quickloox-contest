---
title: "Quiz est Veritas"
date: 2007-03-14
draft: false
tags: ["ping"]
---

Sono preoccupato dal proliferare delle leggende urbane (la più creativa del momento: Darwin per Intel non è più open source da un anno).

Non tanto dalle leggende urbane in sé e per sé, quanto dal contesto. Esiste Internet. Per verificare se Darwin per Intel è open source o meno, bastano quattordici secondi.

Eppure nessuno sente il bisogno di farlo, né prima di formulare il teorema, né dopo averlo ascoltato. Si può dire qualsiasi cosa senza alcun rispetto per la verità dei fatti e la realtà della cose.

Il che, parlando di <a href="http://www.opensource.apple.com/darwinsource/" target="_blank">Darwin per Intel</a>, chi se ne frega (ampiamente). Ma se portiamo gli stessi criteri sulla politica, sull'etica, sulla morale, sul lavoro, sullo studio, su tutte le cose importanti, a me pare un disastro annunciato.

Sarò antico.