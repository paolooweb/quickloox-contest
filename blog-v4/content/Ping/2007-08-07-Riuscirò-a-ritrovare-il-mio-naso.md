---
title: "Riuscirò a ritrovare il mio naso?"
date: 2007-08-07
draft: false
tags: ["ping"]
---

Nel tentativo di usare il mouse il meno possibile, uno dei miei crucci era riuscire a selezionare il testo dentro il Terminale solo via tastiera.

La soluzione è <a href="http://www.macworld.com/weblogs/macosxhints/2007/08/termtext/index.php?lsrc=mwrss" target="_blank">nei menu del programma</a> (in Tiger) e non me ne ero mai reso conto.

Nella prossima puntata, andrò alla ricerca del mio naso. Speriamo di trovarlo. Averlo in mezzo alla faccia non è una garanzia.