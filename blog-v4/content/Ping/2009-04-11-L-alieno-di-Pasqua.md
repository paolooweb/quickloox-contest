---
title: "L'alieno di Pasqua"
date: 2009-04-11
draft: false
tags: ["ping"]
---

L'epoca degli <em>easter egg</em>, le sorprese, dentro i programmi è un po' finita, però qualcosa si trova ancora.

Scarica <a href="http://openoffice.org" target="_blank">OpenOffice.org 3.0</a> (non so se funzioni anche sulla 2 e se funzioni, per chi ha PowerPc, sulla <a href="http://ooopackages.good-day.net/pub/OpenOffice.org/MacOSX/3.1.0rc1_20090405/" target="_blank">versione provvisoria più recente</a>). Apri un foglio di calcolo e inserisci in una casella la formula:

<code>=game()</code>

OpenOffice risponderà <em>Say what?</em> (che cosa?)

Nella stessa casella modifica la formula e cambiala in:

<code>=game("StarWars")</code>

Tanti auguri di buona Pasqua a tutti, tranne che agli alieni!

(forse la cosa più divertente di tutto l'<em>easter egg</em> è <em>dopo</em>, quando hai chiuso la finestra e vuoi mostrare nuovamente la scoperta a qualcuno)