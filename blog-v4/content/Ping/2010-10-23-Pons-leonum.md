---
title: "Pons leonum"
date: 2010-10-23
draft: false
tags: ["ping"]
---

Nel parlare matematico si dice <i>pons asinorum</i>, <i>ponte degli asini</i>, un problema che separa quelli bravi che vanno avanti da quelli limitati che si fermano.

Ora che di Lion, Mac OS X 10.7, sappiamo ancora poco più di niente, oso affermare che la pietra di paragone per valutare i suoi progressi sarà il trattamento di AppleScript. Se AppleScript sarà sminuito o dimenticato, pollice verso; se prosegue e soprattuto se migliora, pollice alzato.

Lo dico all'indomani della notizia del <a href="http://hints.macworld.com/article.php?story=20101022032041191" target="_blank">supporto AppleScript migliorato dentro Chrome 7</a>. Se ci pensa Google, meglio che ci pensi anche Apple.