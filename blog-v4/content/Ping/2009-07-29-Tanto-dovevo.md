---
title: "Tanto dovevo"
date: 2009-07-29
draft: false
tags: ["ping"]
---

Su Preferenze di Macworld agosto uno dei giochi proposti è rimasto senza soluzione, con l'accordo che questa sarebbe arrivata su Ping.

<b>Mascia</b> ha inviato la prima soluzione corretta e altre ne stanno seguendo. È tempo di mantenere la promessa.

Il gioco era il seguente:

<b>Ultimo test: il messaggio impossibile</b>

<cite>È stato usato nientepopodimeno che un cifratore Enigma, usato dai tedeschi durante la Seconda guerra mondiale e <a href="http://russells.freeshell.org/enigma" target="_self">variamente disponibile su web</a>. Rotor Type 341, Initial Position 376.</cite>

<cite>XZEV ROFK WGCV CTKT NCRE LMDI AGKF CSCN UCCH CZPV UMQ</cite>

<cite>30 mele a chi decifra esattamente il messaggio; bonus di 70 mele extra a chi ne crea un altro e me lo spedisce.</cite>

La soluzione è:

SE CE L'HAI FATTA HAI DAVVERO GRANDE DETERMINAZIONE

Complimenti a tutti i solutori e grazie per avere partecipato.

EZYV CYZV UBXD EJWT UQHY UXIJ DGHW YFYI PMI!