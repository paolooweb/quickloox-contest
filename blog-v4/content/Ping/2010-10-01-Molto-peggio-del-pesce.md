---
title: "Molto peggio del pesce"
date: 2010-10-01
draft: false
tags: ["ping"]
---

Ho avuto la pessima sorpresa di ritrovarmi la casella Gmail visitata da uno sconosciuto utente americano con il nome a forma di collocatore di <i>spam</i> a buon mercato.

La cosa buona di Gmail è che mi sono ritrovato la segnalazione in cima alla schermata. Ho provveduto a cambiare immediatamente la password, che ora è assai più lunga e intricata.

La cosa cattiva è che la password era già di dodici caratteri. Non si tratta di minacce di massa ma di episodi sfortunati; per esempio, la mia password su Yahoo è inviolata da quindici anni ed è di una semplicità disarmante.

Comunque un pensiero alla sicurezza delle proprie password non fa mai male. Perché come vedi un ospite indesiderato nella tua posta puzza subito, senza che passino i tre giorni di prammatica.