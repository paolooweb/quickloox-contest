---
title: "Ottocento verso due punto zero"
date: 2003-11-18
draft: false
tags: ["ping"]
---

Usb e FireWire sono cose bellissime, da non confrontare. Non c’è partita

Qualche bello spirito ha iniziato a scrivere che, grazie a Usb 2.0, si può fare a meno di FireWire perché tanto la velocità è la stessa.

Anzi: a leggere le specifiche Usb 2.0 potrebbe anche essere leggermente più veloce di FireWire 400. Ma, come sa chiunque abbia fatto il pieno alla sua auto, un conto è quello che si legge sui depliant e un conto è la vita reale.

Nella vita reale un disco FireWire 400 funziona comunque meglio che un disco Usb 2.0, anche se la differenza è minima.

Ma il fatto è che esiste già FireWire 800. Con questa, non c’è proprio partita.

E sono pronte le spcifiche per FireWire 1.600 e FireWire 3.200, è solo questione di tempo. Se Intel sta studiando Usb 3.0 nessuno ancora lo sa, ma è certo che – per offrire vantaggi certi sulla velocità – dovrà sacrificare la compatibilità all’indietro con gli standard Usb esistenti. Un conto è collegare i mouse e le stampanti, ma per andare veramente al massimo la strada giusta è FireWire.

<link>Lucio Bragagnolo</link>lux@mac.com