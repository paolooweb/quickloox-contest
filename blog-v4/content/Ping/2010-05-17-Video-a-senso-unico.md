---
title: "Video a senso unico"
date: 2010-05-17
draft: false
tags: ["ping"]
---

Ho provato <a href="http://codykrieger.com/gfxCardStatus/" target="_blank">gfxCardStatus</a>, il software che permette la commutazione manuale tra scheda video potente e scheda video economica sui nuovi portatili Mac con Core i5 e i7.

Per i MacBook Pro 2009 come il mio il supporto del programma è definito <cite>sperimentale</cite>.

In movimento e in necessità di preservare la batteria, ho commutato la scheda video senza problemi, passando alla modalità più risparmiosa. Ho pensato che la lacuna forse più rilevante di Snow Leopard fosse stata colmata.

Tornato in studio, ho commutato nuovamente la scheda per passare a quella potente e si è manifestato un bel <i>kernel panic</i>. C'è ancora un po' di lavoro da fare, almeno in una direzione.