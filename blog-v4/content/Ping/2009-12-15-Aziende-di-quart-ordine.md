---
title: "Aziende di quart'ordine"
date: 2009-12-15
draft: false
tags: ["ping"]
---

Si era già parlato di Microsoft intenta a <a href="http://www.macworld.it/blogs/ping/?p=3078" target="_blank">rubare codice libero pur di pubblicarlo come proprietario</a>. Non per cattiveria e neanche per malafede; semplicemente per disorganizzazione, disattenzione, trascuratezza, approssimazione.

È passato poco tempo.

Oggi emerge che Microsoft ha <a href="http://blog.plurk.com/2009/12/14/microsoft-rips-plurk/" target="_blank">copiato parti rilevanti dell'interfaccia utente e del codice</a> di un servizio interessante di <i>microblogging</i> in Cina, a tutto vantaggio di un proprio servizio analogo inglobato in Msn.

Non credo sia malafede, né intenzione; molto probabilmente, nell'estrema provincia dell'impero, si sta meno attenti. Probabilmente ci ha lavorato qualche azienda esterna. Probabilmente nessuno ha controllato, o ha controllato come si deve.

Incuria, leggerezza, distrazione, menefreghismo.

Mi chiedo come saranno fatti, dentro, i prodotti di una azienda cos&#236;.