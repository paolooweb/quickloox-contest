---
title: "Giorni di Java"
date: 2006-07-09
draft: false
tags: ["ping"]
---

Torino è già passata, a settembre è la volta di Cagliari. Poi Milano, Pisa, Novara, Palermo, Roma. È <a href="http://www.javaday.it/" target="_blank">JavaDay</a>. Interessa direttamente i programmatori e sviluppatori ma indirettamente tutti. Più Java è vivo e vitale, più programmi sono disponibili. E poi sono programmi che possono funzionare dappertutto, una cosa che dà fastidio ai monopolisti. Quindi è una cosa buona.

Grazie a <strong>Marcello</strong> per la segnalazione!