---
title: "Scripting con il sorriso"
date: 2008-04-09
draft: false
tags: ["ping"]
---

Serve solo a gente veramente impegnata su AppleScript, la nuova versione di <a href="http://www.satimage.fr/software/en/index.html" target="_blank">Smile di Satimage Software</a>.

Però è anche molto più versatile di Script Editor. E l'edizione Regular è gratis.