---
title: "Paura double-face"
date: 2007-03-19
draft: false
tags: ["ping"]
---

Con il passaggio a Intel e soprattutto con il varo di Boot Camp, senza contare Parallels, c'era chi ipotizzava la mor&#236;a dei giochi per Mac (tutti avrebbero preso i giochi edizione Windows e stop).

Lo speciale di Inside Mac Games sui <a href="http://www.insidemacgames.com/features/view.php?ID=505" target="_blank">giochi Mac per il 2007</a> descrive una situazione del tutto diversa. Quanto al diffondersi generalizzato di giochi Intel-only, l'articolo specifica che al massimo sarà una tendenza del 2008.

Curioso come lo stesso fatto possa generare due paure opposte. Sintomatico che né l'una né l'altra si stiano concretizzando.