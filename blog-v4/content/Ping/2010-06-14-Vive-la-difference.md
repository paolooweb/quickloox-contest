---
title: "Vive la difference"
date: 2010-06-14
draft: false
tags: ["ping"]
---

A qualcuno serve forse <a href="http://www.kaleidoscopeapp.com/" target="_blank">Kaleidoscope</a>?

Confesso che resterò legato per la vita o quasi a BBEdit e difficilmente userò con intensità altri strumenti in grado di evidenziare le differenze tra i due testi.

Kaleidoscope però lavora anche con due immagini e, per interfaccia e modalità di lavoro, è davvero moderno e piacevole da usare. Oltretutto costa 29 dollari; può servire quasi solo per lavoro, quindi costa vicino a zero.

È un programma che, per come tratta le differenze, merita l'addizione al disco rigido e promette di moltiplicare la produttività.