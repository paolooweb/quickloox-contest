---
title: "Controlli di parità"
date: 2010-05-27
draft: false
tags: ["ping"]
---

Ora che Apple ha sorpassato di un'incollatura Microsoft in capitalizzazione di mercato, cosa che ha valore pratico nullo ma simbolico notevole, un paio di note a margine.

Le aziende devono sapere innovare o rinnovarsi, o stagnano. Microsoft è cresciuta molto negli ultimi vent'anni in termini di fatturati e dimensioni, ma il valore dell'azienda è rimasto essenzialmente sempre quello. Apple, che ha infilato innovazione a tutto spiano nell'ultimo decennio con iPod, iTunes Store, iPhone, Mac OS X, <i>unibody</i>, iPad, guarda caso ha decuplicato il proprio valore. Ci pensino, quelli che lamentano la presunta trascuratezza del <i>business</i> Macintosh nei confronti di iPhone. Forse possiamo crescere un pochino di valore anche noi, se riconosciamo cose nuove invece di abbarbicarci al passato.

Per brevissimi istanti il valore di mercato di Apple è arrivato a essere <i>un centesimo</i> di quello di Microsoft. La capitalizzazione di mercato moltiplica la quotazione azionaria per le azioni circolanti e quindi è un valore piuttosto volatile. I <i>trend</i> di lungo periodo però sono evidenti e piuttosto validi. Oggi Apple e Microsoft sono in parità sostanziale, per questa metrica. Microsoft tuttavia ricava il proprio valore da rendite di monopolio, mentre Apple deve continuare a centrare i prodotti, come risulterà evidente tra un paragrafo.

Per chi vuole seguire questo tipo di andamenti, è una delle situazioni in cui si scopre tutta l'utilità di <a href="http://www.wolframalpha.com/input/?i=(AAPL+market+cap)+%2F+(MSFT+market+cap)" target="_blank">WolframAlpha</a>, il motore di ricerca computazionale, che non mi stanco di raccomandare. Quello scalino improvviso nel passaggio tra 2000 e il 2001 è il <i>flop</i> del Cube.

Ci sono voluti quattro anni per recuperare un singolo prodotto andato male. Ci pensino quelli che sputano sentenze su prodotti che, generando valore in quantità, evidentemente valgono qualcosa (ed era eccezionale perfino il Cube, purtroppo a prezzo sbagliato e con un'aura negativa che i media sono riusciti a mettergli addosso).