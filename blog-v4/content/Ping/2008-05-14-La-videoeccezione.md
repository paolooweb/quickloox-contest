---
title: "La videoeccezione"
date: 2008-05-14
draft: false
tags: ["ping"]
---

È rarissimo che mandi in giro link a filmati YouTube. Però <strong>Alan</strong> mi ha rinviato alla <a href="http://www.youtube.com/watch?v=6kxDxLAjkO8" target="_blank">migliore demo di Mac OS X mai esistita</a>.

Non è per quello che si vede. È per il cuore che c'è dietro. L'entusiasmo sta su Mac.