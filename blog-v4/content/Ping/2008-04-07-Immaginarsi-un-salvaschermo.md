---
title: "Immaginarsi un salvaschermo"
date: 2008-04-07
draft: false
tags: ["ping"]
---

Sarà la primavera, ma sono saltati fuori due progetti di salvaschermo fai-da-te, che caricano immagini a ripetizione.

Uno <a href="http://hackermojo.com/mt-static/archives/2008/04/metropulse-launched.html#http://franxman.com" target="_blank">è in Python</a> e prende le immagini dal sito Metropulse (ma potrebbe essere qualsiasi altro); l'altro <a href="http://www.tuaw.com/2008/02/15/terminal-tips-creating-a-spotlight-based-gallery/" target="_blank">usa il Terminale</a> (e pure Perl) e carica le ultime venti immagini aggiunte nel Mac grazie a Spotlight.

Mi metto a cercare qualcosa per AppleScript, va'. :)