---
title: "Mele marce"
date: 2010-10-24
draft: false
tags: ["ping"]
---

Macity ha deciso di <a href="http://www.macitynet.it/macity/articolo/Educazione-a-casa-Gates-prodotti-Apple-proibiti-ai-figli/aA46892" target="_blank">informare</a> che Melinda Gates, moglie di Bill, in una <a href="http://www.nytimes.com/2010/10/24/magazine/24fob-q4-t.html?_r=2&amp;ref=magazine" target="_blank">intervista al New York Times</a> del 22 ottobre ha dichiarato di tenere fuori dalla porta di casa i prodotti Apple.

Un notizione. Stretta attualità.

Cos&#236; <a href="http://latimesblogs.latimes.com/technology/2009/03/too-rich-for-an.html" target="_blank">scriveva il Los Angeles Times</a> il 2 marzo 2009, <b>un anno e mezzo fa</b>:

<cite>Ci sono poche cose proibite a casa nostra</cite>, ha dichiarato Melinda Gates, <cite>ma iPod e iPhone sono due cose che non prendiamo per i nostri figli</cite>.

Cos&#236; Macity descrive la situazione:

<cite>&#171;Signora Gates - insiste la  Solomon - lei ha un iPod&#187;. &#171;No - è la risposta sintetica di Melinda Gates - ho uno Zune&#187;. &#171;E quindi non ha neppure un iPad&#187;, incalza la giornalista del New York Times. &#171;Ovviamente no&#187;, risponde altrettanto ovviamente la risoluta Melinda.</cite>

Questo è il testo vero:

<b>Lei ha un iPod, che è prodotto da Apple?</b>
<i>No, ho uno Zune.</i>

<b>Ha un iPad?</b>
<i>Naturalmente no.</i>

La giornalista insiste, poi incalza, intanto Melinda fa la risoluta. È tutto inventato.

Il formato è quello di una pratica giornalistica usuale: l'intervista via email, o al telefono, o via fax. Tanto che l'articolo non aggiunge alcuna descrizione di colore o di ambiente, proprio perché l'intervista non si è materialmente svolta in alcun luogo.

Roba riciclata e insaporita per coprire l'odore di marcio. Chi mangerebbe in un ristorante dove servono roba del genere?