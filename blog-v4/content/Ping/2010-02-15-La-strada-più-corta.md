---
title: "La strada più corta"
date: 2010-02-15
draft: false
tags: ["ping"]
---

L'<a href="http://www.apple.com/it/support/pages/shortcuts/" target="_blank">elenco delle scorciatoie di tastiera di Pages</a>, oltre a rivelare qualche sorpresa gradita, usa un indirizzo che incoraggia a tentare anche alternative. iPhoto, GarageBand, ovviamente Numbers e Keynote&#8230; non ci sono tutti i programmi Apple, o almeno non in forma ovvia. Tuttavia basta un Preferito per arrivare a molte destinazioni.