---
title: "Chi si impegna su OpenOffice"
date: 2002-05-14
draft: false
tags: ["ping"]
---

L’alternativa gratuita a Office è quasi pronta; manca solo un pizzico di impegno

Mesi fa ci si chiedeva sulle liste se ci fosse un’alternativa a Microsoft Office, fabbrica di macrovirus venduta a carissimo prezzo da un’azienda dai comportamenti illegali, sanciti in tribunale; e si parlava di OpenOffice. Ma il lavoro procedeva a rilento e la versione per Mac OS X latitava.
Ora OpenOffice è quasi pronto; più del 90% del codice si compila correttamente e funziona su Mac OS X (per quanto ci voglia il server grafico X11). Il lavoro procede lentamente ma si vede la luce in fondo al tunnel; manca solo un po’ di beta testing da parte di un manipolo di persone con buona volontà che credono nel software libero.
Di tutti quelli che si lamentavano della lentezza di <link>OpenOffice</link>http://www.openoffice.org, chissà quanti spenderanno un minuto del loro tempo per fare progredire gratis un software gratuito.

<link>Lucio Bragagnolo</link>lux@mac.com