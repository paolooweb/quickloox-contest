---
title: "La profezia Dell'arrogante"
date: 2009-10-07
draft: false
tags: ["ping"]
---

Mi sono distratto e per fortuna ha pensato <a href="http://www.macswitching.com./" target="_blank">Stefano</a> a ricordarmi che lo scorso sei ottobre sono passati dodici anni esatti da quando Michael Dell, fondatore dell'omonima azienda, dichiarò che, se fosse stato lui amministratore delegato di Apple, <a href="http://news.cnet.com/Dell-Apple-should-close-shop/2100-1001_3-203937.html" target="_blank">avrebbe chiuso l'azienda e restituito i soldi agli azionisti</a>.

Il confronto dell'<a href="http://it.finance.yahoo.com/echarts?s=AAPL#chart3:symbol=aapl;range=19971006,20091006;compare=dell;indicator=volume;charttype=line;crosshair=on;ohlcvalues=0;logscale=on;source=undefined" target="_blank">andamento delle azioni negli ultimi dodici anni</a> parla chiaro.

Ho una certa opinione su che cosa farei se fossi amministratore delegato di Dell. Chissà se me la chiederanno mai.