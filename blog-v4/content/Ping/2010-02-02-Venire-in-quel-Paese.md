---
title: "Venire in quel paese"
date: 2010-02-02
draft: false
tags: ["ping"]
---

<b>Giovanni</b> mi ha segnalato <a href="http://itunes.apple.com/it/app/italian-gestures/id342571192?mt=8" target="_blank">Italian Gestures</a>, applicazione per iPhone, iPod touch e ora anche iPad da lui sviluppata.

Nell'acquistarla a settantanove centesimi non si guadagna nulla in termini di effetti speciali o mirabolanti utilizzi dell'accelerometro. Tuttavia l'applicazione rappresenta, per idea di base e svolgimento, l'aver capito come sfruttare le potenzialità di iPhone anche in modo semplice e mondano come può essere far comprendere a uno straniero il vocabolario gestuale italiano, a suo modo peculiare ed effettivamente fonte potenziale di imbarazzo.

Difficile capirlo per chi non sia mai stato all'estero, poniamo in America, a vedere usare il nostro gesto delle corna da tifosi appassionati che incitano la propria squadra. Solo un esempio dei tantissimi possibili.

La consultazione dell'applicazione è estremamente semplice e la grafica  è quasi sempre azzeccata. C'è un errore di ortografia (whatch) che si perdona a patto che sia prevista la sua correzione prima o poi, ma è peccato veniale. L'utilità effettiva di Italian Gestures è quella detta. Eppure, se qualcuno pensa di voler intraprendere un qualche lavoro di programmazione iPhone, gliela consiglio, per capire come una idea sufficientemente acuta possa abbattere molte barriere di ingresso.

Complessivamente le prestazioni valgono il prezzo. Mi aspetto una nuova edizione arricchita e, metti che abbia successo, versioni internazionali.