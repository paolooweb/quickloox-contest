---
title: "Prima e dopo"
date: 2007-06-24
draft: false
tags: ["ping"]
---

iPhone sta per uscire e sento decine di persona fantasticare dell'interfaccia di iPhone applicata a tutto, per provare il brivido di sbracciarsi sopra schermi di trenta pollici sensibili al tocco (certo, come minimo sparirebbe il bisogno di palestra).

Scommetto qualsiasi cifra che le stesse persone, il giorno dopo che è uscito iPhone, lamenteranno l'assenza di feedback sulla tastiera virtuale, che oggi vorrebbero estesa ai televisori al plasma, manco fossimo tutti calamari tentacolati..