---
title: "Nessun bug è un’isola"
date: 2004-01-28
draft: false
tags: ["ping"]
---

Ma l’unione dei bug non fa la forza

Ricevo tanta posta di persone con un problema al Mac. Molti di questi mi raccontano che non sono i soli ad avere quel problema, anzi: il forum Apple (o una mailing list, o un certo forum) trabocca di gente con lo stesso problema. Come se il mal comune mezzo gaudio legittimasse in qualche modo il loro problema e lo rendesse più rispettabile o più degno di attenzione.

È un discorso crudele ma aritmetico: le cose vanno messe in prospettiva. Apple vende più ottocentomila Mac a trimestre, quindi quasi tre milioni e mezzo di Mac l’anno. A parte clamorose eccezioni, che sono poche altrimenti non sarebbero eccezioni, se un problema interessasse anche solo il dieci percento dei Mac venduti in un anno (trecentocinquantamila Mac) finirebbe in prima pagina sui giornali.

Quando dieci, venti, cento, cinquecento persone compaiono su un forum, rappresentano cinquecento Mac su tre milioni e mezzo: un settemillesimo del totale.

Tutti i problemi sono importanti, ma novantanove problemi su cento sono comunque isolati. Anche se interessano cinquecento persone.

<link>Lucio Bragagnolo</link>lux@mac.com