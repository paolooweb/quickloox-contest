---
title: "Il mio regno per uno scanner"
date: 2006-02-13
draft: false
tags: ["ping"]
---

<a href="http://www.sourcesense.com" target="_blank">SourceSense</a> annuncia la sua nascita con un concorso: <strong>disegna il nostro logo, vinci un MacBook Pro</strong>.

Un&rsquo;occhiata rapida alle pagine dell&rsquo;annuncio non rivela moltissimo. Impegnati nell&rsquo;open source, focus internazionale e un po&rsquo; di altro <a href="http://www.sourcesense.com/contest/brief.html" target="_blank">aziendalese</a>. Quanto meno sanno che esiste il Mac.

Ma le <a href="http://www.sourcesense.com/contest/rules.html" target="_blank">regole</a> sono chiare: fa comodo avere il formato professionale, ma l&rsquo;importante &egrave; l&rsquo;idea.

Ora metto al lavoro la fidanzata, fresca di Accademia di Brera. Se solo ritrovo il vecchio scanner&hellip;