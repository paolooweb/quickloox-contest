---
title: "Supportsmith"
date: 2007-07-28
draft: false
tags: ["ping"]
---

Locksmith, in inglese, è uno bravo a lavorare con i lucchetti. Wordsmith è uno bravo a creare con le parole. Goldsmith è un bravo orafo.

Mailsmith è il mio programma di posta dal secolo scorso. E giorni fa mi si è corrotta una casella, che dava errore 16435 e non si poteva né cancellare, né ricostruire, né vuotare, né rinominare, né altro. I dati non erano un problema (faccio i backup), ma la struttura delle mailbox s&#236;.

Ho scritto al supporto di Bare Bones. Mi hanno chiesto come si chiamava la casella danneggiata e, saputolo, me l'hanno spiegata cos&#236;:

<cite>1. Quit Mailsmith.</cite>

<cite>2. In a Terminal window, cd to the Mail package directory:</cite>

<code>cd ~/Documents/Mailsmith\ User\ Data/Mail</code>

<cite>3. Use the &#8220;ditto&#8221; command in the Terminal to replace the missing mailbox with a copy of the &#8220;(spam)&#8221; mailbox. Please be sure to enter the command exactly as given:</cite>

<code>ditto -rsrcFork "(spam)" "Mailing varie"</code>

<cite>4. Relaunch Mailsmith, and check that you can access all your mailboxes without error, including the &#8220;Mailing varie&#8221; mailbox.</cite>

Problema risolto in due mail, nello spazio di sei ore, con gentilezza (ho espunto le parti non rilevanti tecnicamente delle mail) e professionalità. Quando uscirà Mailsmith 3.0, metto per iscritto che aggiorno indipendentemente dalla cifra che costa. Piuttosto mi indebito.