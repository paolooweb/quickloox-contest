---
title: "Missioni compiute"
date: 2007-09-16
draft: false
tags: ["ping"]
---

Non parlerò più di aggiornamenti di Mac OS X andati a buon fine. Da inizio anno a oggi ne ho fatti una quantità sconsiderata e non c'è mai stato un problema (ogni aggiornamento che faccio viene installato come minimo su due Mac).

Ho smesso di contare i riavvii indesiderati. Da gennaio non ne ho avuto neanche uno fino all'estate. Poi ne ho fatti numerosi per scrivere la mia parte del libro su Leopard. Ma erano tutti voluti e necessari.

Da oggi e per sempre: gli aggiornamenti funzionano. Un riavvio indesiderato è un evento eccezionale. Quando capiterà l'eccezione, sarà evidente a tutti e non ci sarà bisogno di andare a sfruculiare MacFixit in cerca di chissà che segreti nascosti.