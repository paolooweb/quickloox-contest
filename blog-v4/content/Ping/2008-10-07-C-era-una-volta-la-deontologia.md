---
title: "C'era una volta la deontologia"
date: 2008-10-07
draft: false
tags: ["ping"]
---

Il livello di bassezza delle manovre per speculare sul titolo Apple si è ulteriormente abbassato.

La cosa peggiore non è che i siti abbiano rilanciato una indiscrezione anonima su Steve Jobs.

È che l'abbiano pubblicata sapendo perfettamente che era falsa.

Mi vergogno per loro.