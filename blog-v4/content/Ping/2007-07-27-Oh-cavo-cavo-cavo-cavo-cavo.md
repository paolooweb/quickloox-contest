---
title: "Oh cavo, cavo cavo, cavo cavo&#8230;"
date: 2007-07-27
draft: false
tags: ["ping"]
---

Prendo a prestito Elio e le Storie Tese per annullare il venerd&#236; del gioco di oggi.

Doveva riguardare <a href="http://www.splashdamage.com/?page_id=14" target="_blank">Wolfenstein: Enemy Territory</a> e volevo anche annunciarlo ieri. Invece, proprio da ieri, la connessione continua ad andare e venire e non sono neanche riuscito a scaricarlo.

Pubblico questa mia con la connessione cellulare, che però non ha la stabilità né l'ampiezza per giocare con comodità e il giusto <em>relax</em>.

Mi scuso con tutti. Si recupererà.