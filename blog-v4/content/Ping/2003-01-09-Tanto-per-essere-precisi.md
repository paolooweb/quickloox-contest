---
title: "Tanto per essere precisi"
date: 2003-01-09
draft: false
tags: ["ping"]
---

Inizia il profluvio delle sciocchezze sui nuovi prodotti Apple

Il Macworld Expo è la dimostrazione di come si possa leggere o ascoltare senza capire. Sulle mailing list è infatti già iniziato il concerto delle stupidaggini, specialmente sui nuovi PowerBook G4, privi di riscontro rispetto alle informazioni.

Fai stimare a un amico le dimensioni del PowerBook da 17”: sembra il pescatore con il pesce, che diventa più grande ogni volta. Il 17” è 5,1 centimetri più largo e 1,8 centimetri più lungo. Non è affatto più alto; è alto 2,5 centimetri (e no, caro Marco, non si piegherà sotto il suo peso). È invece più pesante, a 3,1 chilogrammi, il 50% più del 12” (che è una macchina bellissima).

I nuovi PowerBook hanno anche un tasto più dei vecchi e il 17” legge la luce ambientale per consentire di impostare al meglio lo schermo.

Qualcuno si è lamentato che la batteria del 17” duri meno degli altri. Faccia due conti sulla risoluzione aggiuntiva e si ricordi che la batteria se ne va in gran parte per lo schermo retroilluminato.

Insomma, come al solito. Meno ne sanno, più sputano sentenze.

<link>Lucio Bragagnolo</link>lux@mac.com