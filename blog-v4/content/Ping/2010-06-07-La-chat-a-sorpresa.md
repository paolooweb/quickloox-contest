---
title: "La chat a sorpresa"
date: 2010-06-07
draft: false
tags: ["ping"]
---

È un mondo difficile: vita intensa, serenità a momenti e futuro incerto.

Di conseguenza, le buone occasioni vanno sfruttate. Appuntamento a tutti per oggi, luned&#236; 7 giugno sera, dalle 18 italiane in poi, per chiacchierare in Irc sul consueto canale <code>#freesmug</code> della partenza della Wwdc con relativo <i>keynote</i> di Steve Jobs (il <i>keynote</i> inizia alle 19 italiane).

Solo un dettaglio: non so dove sarò. È possibile ogni ipotesi, dalla battigia al treno che mi riporta a Milano a Milano stessa.

Prometto che appena sono in condizione di farlo mi collego. Nel frattempo ripubblico qui sotto quanto scrissi in passato, velocissima guida per instaurare rapidamente una connessione Irc.

Io sarò attivo anche nella stanza tradizionale del Poc, <code>pocroom</code>, sempre per fare da buttadentro.

L'appuntamento vero è su Irc, la chat libera di Internet. Per fare chat Irc esistono decine di <a href="http://www.macworld.it/blogs/ping/?p=2074" target="_blank">programmi, anche gratuiti</a>. Lo scopo è collegarsi al server <code>irc.freenode.net</code> e, una volta collegati, dare il comando <code>/join #freesmug</code>. Ci troviamo l&#236;, sponsorizzati (niente soldi, solo simpatia) da <a href="http://freesmug.org" target="_blank">Gand</a>. Viva il software libero per Mac!

Non c'è davvero altro da sapere per arrivare. Poi, per stare a proprio agio nella stanza, ci regoliamo al volo. I comandi Irc utili sono pochi e semplici e i volonterosi trovano su Internet <a href="http://www.ircbeginner.com/ircinfo/ircc-commands.html" target="_blank">elenchi utili</a>.