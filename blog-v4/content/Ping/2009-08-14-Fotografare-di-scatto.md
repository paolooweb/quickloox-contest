---
title: "Fotografare di scatto"
date: 2009-08-14
draft: false
tags: ["ping"]
---

Le fotocamere digitali hanno sempre una certa latenza. Il momento in cui si preme il pulsante di scatto non è quello in cui viene ripresa l’immagine. E a volte sfuma una bella inquadratura.

Sembra che con iPhone 3GS <a href="http://lonelysandwich.com/post/162364559/always-on" target="_blank">non sia così</a>. Scatti e ovviamente il soggetto si muove proprio mentre si aziona l’otturatore digitale… ma l’immagine è quella che volevi riprendere. Con gli iPhone precedenti non succede.

Non si trova documentazione di questo. L’ipotesi più accreditata è che, quando si lancia l’applicazione per fotografare, iPhone 3GS tenga sempre in una <i>cache</i> quello che viene inquadrato, pronto a registrarlo istantaneamente (visto che è già in memoria) quando si scatta.

L’immagine che vogliamo scattare è pronta già prima che la scattiamo.

Tecnologia già vista in alcune videocamere. Non so se sia mai comparsa prima in un computer da tasca.

Tecnologia capace, assieme al nuovo controllo di messa a fuoco, di <cite>elevare finalmente i nostri scatti al rango di fotografie</cite>. Era da tempo che non trovavo una citazione così bella.