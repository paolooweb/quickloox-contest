---
title: "La cartolina"
date: 2008-09-28
draft: false
tags: ["ping"]
---

Finite le vacanze, è d'obbligo la cartolina.

Con un saluto speciale a Giandonato e uno specialissimo a Carmelo, i cui giochi matematici russi hanno allietato numerosi momenti di relax. :-)