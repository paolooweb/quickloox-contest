---
title: "Il concetto della radio<p>"
date: 2005-04-16
draft: false
tags: ["ping"]
---

Piccola e spontanea lezione su come sono pensati gli iPod<p>

Una mia conoscente ha ricevuto in regalo un iPod shuffle e lei, buona ascoltatrice di musica, è invece utente di computer appena occasionale.<p>

Parlandomi dell&rsquo;esperienza con il regalo ha detto testualmente <em>è la radio migliore che c&rsquo;è perché trasmette proprio la tua musica</em>.<p>

Così dicendo ha messo un aggraziato macigno sulle polemiche di quelli che giudicano iPod non completo senza la radio o altre funzioni che l&rsquo;oggetto non è pensato per essere, anche se potrebbe benissimo fare.<p>

iPod shuffle: la radio migliore che c&rsquo;è. Se serve ad Apple, le cedo lo slogan gratis. D&rsquo;altronde non è mio.<p>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>