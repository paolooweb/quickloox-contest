---
title: "Un preconcetto non virus<p>"
date: 2004-10-18
draft: false
tags: ["ping"]
---

Quando le idee si radicano nel cervello, i fatti non bastano più. Però ci si prova<p>

Perfino tra la gente Mac di esperienza si trova qualche solone che scuote il capo e afferma saccente che no, non è che Mac OS X sia più sicuro; è che il numero di computer Apple in giro è relativamente piccolo (sotto i trenta milioni, insomma) e che quindi gli scrittori di virus non se lo filano perché non c&rsquo;è gusto.<p>

Allora gli chiedi di spiegare Witty e il saccente si defila.<p>

Witty è stato rilasciato il 20 marzo 2004 (per Windows) ed è un worm distruttivo, lungo meno di 700 byte, ma capace di creare copie di se stesso generando 20 mila pacchetti e poi andare a scrivere 65K di dati in un punto a caso del disco. Se il computer è ancora in piedi il ciclo si ripete e via così.<p>

Sì, e allora? Allora Witty è paurosamente efficiente, per velocità, astuzia e precisione del codice. Non è stato scritto da un ragazzino brufoloso e sfigato ma da un programmmatore professionista.<p>

Eppure <a href="http://www.bynkii.com/generic_mac_stuff/archives/2004_09.html">Witty</a> è stato progettato per colpire solo i computer che eseguivano una versione poco aggiornata del programma BlackIce. 12 mila macchine su tutta Internet.<p>

I Mac in rete sono almeno duemila volte più numerosi, caro saccente.<p>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>