---
title: "Il complesso delle cose"
date: 2008-05-01
draft: false
tags: ["ping"]
---

Quanto sforzo occorre per produrre un sistema operativo della raffinatezza di Mac OS X?

Un punto di partenza semplice è guardare le (appena aggiornate) <a href="http://developer.apple.com/documentation/Darwin/Reference/ManPages/index.html" target="_blank">pagine man di Apple su web</a>.

E poi riprendere fiato prima di approfondire.