---
title: "La perfida Albione"
date: 2009-11-22
draft: false
tags: ["ping"]
---

Sento che è ora di prendere definitivamente le distanze dal giornalismo inglese e non solo informatico.

Cnet Uk si distingue dal pollaio, però solo grazie all'originalità demenziale: l'ultimissima è uno <i>scoop</i>, per cos&#236; dire, su <a href="http://crave.cnet.co.uk/gadgets/0,39029552,49304012,00.htm" target="_blank">quanto pesa Internet</a>. Niente a che vedere con i colleghi americani, che scrivono molto e dunque sbagliano molto, ma ci danno dentro con energia e sanno restare autorevoli.

Adesso vedo la Bbc con un articolo inutile su <a href="http://news.bbc.co.uk/2/hi/uk_news/magazine/8367957.stm" target="_blank">che cosa sia accaduto a Second Life</a>.

Una idea senza dubbio carina per raccontare di quanti calcolatori e di quanti <i>server</i> sia composta la Rete è niente più di questo, idea carina; e cos&#236; come erano scomposte le esultanze per i numeri di Second Life del 2006, sono ora fuori luogo le geremiadi su un suo <i>flop</i>. Non ho mai avuto simpatia per l'iniziativa, ma darla per finita o quasi dato che vi si collega un milione di persone al mese, insomma, è parlare a vanvera.