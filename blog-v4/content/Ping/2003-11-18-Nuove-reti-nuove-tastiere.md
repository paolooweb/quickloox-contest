---
title: "Nuove reti, nuove risposte"
date: 2003-11-18
draft: false
tags: ["ping"]
---

Che cosa succede se in una stanza ci sono più tastiere wireless?

Ci sono situazioni limite in cui si possono collegare più tastiere contemporaneamente al computer, per esempio per usare in due un gioco che richieda molti comandi oppure riservi a ciascun giocatore mezza tastiera. È più comodo farlo ognuno con la sua tastiera.

Ora Apple ha presentato tastiera e mouse Bluetooth e mi è stato chiesto che cosa succede se in una stanza vengono messe più tastiere Bluetooth. Forse che ognuno, dalla sua tastiera, può dare ordini al Mac?

Ovviamente no. Tutti i dispositivi Bluetooth che si parlano devono prima accoppiarsi e il Mac quindi parlerà solo con la tastiera e con il mouse con i quali si è verificato l’accoppiamento.

Doptotutto è la stessa ragione per cui la Rubrica Indirizzi non cerca di sincronizzarsi con i cellulari di tutti quelli che stanno nella stanza, ma solo con quello del proprietario del Mac.

<link>Lucio Bragagnolo</link>lux@mac.com