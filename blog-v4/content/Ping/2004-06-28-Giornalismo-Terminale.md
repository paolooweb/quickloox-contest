---
title: "Giornalismo Terminale"
date: 2004-06-28
draft: false
tags: ["ping"]
---

L’afa estiva suggerisce le pratiche informatiche più bizzarre

I pazzi che pensando di poter vivere scrivendo sanno che una delle regole elementari del giornalismo di stampo anglosassone consiste nel redigere agli articoli rispettando la serie who, where, when, what e why, banalmente spiegare chi, dove, quando, che cosa e perché.

Ho provato la regola nel Terminale scoprendo che in ben due casi si tratta di regolari comandi Unix. Per soprammercato, esistono anche i comandi whoami e which.

Che caldo, eh?

<link>Lucio Bragagnolo</link>lux@mac.com