---
title: "Tecnica e paura/1"
date: 2010-05-01
draft: false
tags: ["ping"]
---

Non è difficile incontrare di questo periodo qualcuno che racconta come Apple sia cos&#236; <i>distratta</i> da iPhone e iPad che si disinteressa di Macintosh e anzi lo lascerà morire.

Spiegargli che Apple vende il triplo dei Macintosh di dieci anni fa e rappresenta oltre un terzo dei guadagni di Apple è inutile (alzi la mano chi è disposto a rinunciare a un terzo del proprio stipendio perché è <i>distratto</i> da un altro lavoro, per quanto interessante e proficuo).

Si sa che la Apple di quest'epoca cerca il controllo assoluto sul proprio hardware e sul proprio software. Steve Jobs ha appena scritto una lettera aperta sul perché non vuole che Flash entri in iPhone/iPod/iPad e perché non vuole che i programmi per le stesse piattaforme possano essere scritti in modo <i>write once, run anywhere</i>, li scrivi una volta sola e poi funzionano dovunque. Apple, per quanto può, desidera che siano scritti con i linguaggi da essa preferiti, possibilmente con gli strumenti messi a punto da Apple.

I programmi per iPhone, iPad e iPod touch possono essere scritti &#8211; usando strumenti Apple &#8211; solo su Mac. Su Mac funziona iTunes Store, che serve per vendere musica e applicazioni a centoventicinque milioni di persone e a sincronizzare gli apparecchi di cui sopra.

Chiedi <i>la Apple che vuole il controllo di hardware e software, come può fare a meno di Mac?</i> e la risposta è <i>iTunes e Safari funzionano già su Windows.</i>

Rileggere: la Apple che non vuole Flash su iPhone si metterebbe in condizione di dipendere dalle decisioni di Microsoft in merito al sistema operativo.

Questo scenario è pesantemente incongruo, ma può darsi che continuino a ripeterti la stessa cosa.

Nei prossimi giorni, visto che questo è un blog e non un'enciclopedia, apparirà qualche traduzione parziale che spiega ancora meglio come stanno le cose.

Se mai Macintosh sparirà, sarà perché è diventato inutile. Ma questo nella Apple di oggi è impensabile e dunque non sparirà. Se mai sarà diventato inutile &#8211; e da qui a dieci anni può essere tutto &#8211; sarà altrettanto inutile preoccuparsene, per definizione.