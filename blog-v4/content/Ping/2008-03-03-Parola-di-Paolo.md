---
title: "Parola di Paolo"
date: 2008-03-03
draft: false
tags: ["ping"]
---

Tanto <a href="http://pztake.blogspot.com/" target="_blank">scrive</a>, tanto pubblico, sul suo nuovo MacBook Air:

<cite>Ho il MacBook Air da una settimana.</cite>

<cite>Sostituisce il fido compagno di viaggi, il PowerBook g4 12' (niente code, me lo tengo, va in bacheca!).</cite>

<cite>Comprato perché ogni grammo in meno nello zaino è sollievo.</cite>

<cite>Infatti pesa un grasso kg meno del PB.</cite>

<cite>Travasato i dati dall'iMac in un'oretta con il disco esterno.</cite>

<cite>Potrei finire qui perché per il resto è come un Mac.</cite>

<cite>Ma è anche bello liscio e curvoso  e sottile.</cite>

<cite>Non scalda.</cite>

<cite>Non ha grosse prestazioni ma le applicazioni girano tranquille e per un Mac da viaggio basta e avanza.</cite>

<cite>A livello porte siamo a zero, cioè una USB.</cite>

<cite>Con l'eccezione di FireWire non mi serve altro.</cite>

<cite>Sul portatile non masterizzo.</cite>

<cite>La mia</cite> working partition <cite>sull'iMac è di 80 gigabyte, gli stessi del disco fisso di Air, manco a farlo apposta.</cite>

<cite>Lo schermo è anche troppo luminoso e la retroilluminazione della tastiera utile.</cite>

<cite>La batteria non so quanto duri ma non sono le cinque ore promesse.</cite>

<cite>Se volete un portatile leggero ma con monitor abbastanza grosso e tastiera usabile e pure portarvi appresso un</cite> sex symbol<cite>, Air</cite>.

<cite>Altrimenti, MacBook (un chilogrammo e tante porte in più, DVD e 500 euro in meno).</cite>