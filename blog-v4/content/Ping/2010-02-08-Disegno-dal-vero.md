---
title: "Disegno dal vero"
date: 2010-02-08
draft: false
tags: ["ping"]
---

La galleria degli esempi che preannunciano la fine dell'epoca di Flash si arricchisce con <a href="http://mugtug.com/sketchpad/" target="_blank">Sketchpad</a>.

Provare per credere. Siamo quasi al livello in cui ci si può mettere anche l'infante di casa ed è tutto JavaScript.