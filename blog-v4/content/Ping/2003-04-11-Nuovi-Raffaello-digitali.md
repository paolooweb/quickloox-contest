---
title: "Nuovi Raffaello digitali"
date: 2003-04-11
draft: false
tags: ["ping"]
---

Queste sì che sono tavolette grafiche

Sono stato invitato da Corel a vedere le nuove tavolette Cintia di Wacom e sono rimasto conquistato.
Sì, una volta la tavoletta grafica era un coso grigio grande come un fazzolettino su cui si pasticciava con un bastoncino di plastica sperando che sullo schermo apparisse quello che volevamo.

Sono passati secoli. Ora le Cintia sono essi stessi schermi, di ogni dimensione e anche belli grandi, a cristalli liquidi, riposanti, luminosi e fedeli. Si disegna direttamente sullo schermo con uno stilo degno di questo nome. Il feeling è ottimo, la precisione pure. Usando Photoshop si possono fare miracoli ma un programma pensato per il disegno pittorico, come Corel Painter 8, fa ancora di più e in mani esperte (nel disegno) si vedono autentiche meraviglie.

Tutto Mac OS X e tutto collegato e funzionante al primo colpo.

Importantissimo: le Cintia si appoggiano al tavolo, se proprio uno vuole; altrimenti il loro treppiede permette di inclinarle davanti al disegnatore, che si ritrova come se avesse di fronte una tela su un cavalletto. Per un ingegnere la comodità dell’inclinazione è quella del tecnigrafo.

Non si fatica a immaginare nuovi Raffaello, Correggio, Picasso, Sotti digitali. Se Leonardo avesse avuto in mano una cosa del genere noi avremmo gli stessi libri di scuola da cinquecento anni. I suoi.

<link>Lucio Bragagnolo</link>lux@mac.com