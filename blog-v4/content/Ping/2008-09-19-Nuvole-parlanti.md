---
title: "Nuvole parlanti"
date: 2008-09-19
draft: false
tags: ["ping"]
---

Una volta era solo sinonimo di <em>fumetto</em>. Adesso esistono le <em>tag cloud</em>, nuvole di parole che esprimono graficamente la prevalenza dei vocaboli usati su siti e blog.

<a href="http://wordle.net/" target="_blank">Wordle</a> consente di creare con grande facilità una <em>tag cloud</em>, con le parole che vogliamo noi oppure lavorando su un sito a piacere. Basta digitare le parole, oppure fornire l'indirizzo, o un nome utente di del.icio.us. La cloud si può modificare, stampare, si possono vederne altre a piacere o a caso, sono insomma minuti di divertimento autentico.

Un grosso grazie ad <a href="http://www.teleart.org" target="_blank">Andrea Ack</a> per la segnalazione!