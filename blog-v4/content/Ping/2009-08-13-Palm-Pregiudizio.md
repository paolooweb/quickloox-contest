---
title: "Palm Pre(giudizio)"
date: 2009-08-13
draft: false
tags: ["ping"]
---

Capisco perfettamente che Palm sia interessata a sapere che applicazioni vengono usate e per quanto tempo, ma che se ne fa della <a href="http://kitenet.net/~joey/blog/entry/Palm_Pre_privacy/" target="_blank">posizione Gps del proprietario del Pre</a>?

iPhone ha i Location Services e un sacco di applicazioni sono ansiose di sapere dove mi trovo. Ma l'informazione va alle applicazioni, non ad Apple.