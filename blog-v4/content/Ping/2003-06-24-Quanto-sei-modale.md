---
title: "Quanto sei modale"
date: 2003-06-24
draft: false
tags: ["ping"]
---

Una differenza tra Windows e Mac OS X che molti non sanno neanche nominare

La programmazione modale è quella in cui all’utente compaiono finestre di dialogo che rendono impossibile fare altre cose finoa quando non si esce dalla finestra di dialogo stessa.

Un sistema operativo male progettato e antiproduttivo è capace non solo di abusare della programmazione modale, ma anche di sovrapporre finestra di dialogo a finestra di dialogo e averne persino quattro o cinque una sopra l’altra.

Vuoi vedere la differenza tra Mac OS X e Windows? Prova a usarli per qualche giorno e confronta quante volte e con che profondità vanno in modale. C’è una differenza assai consistente e non c’è bisogno di dire a favore di chi.

<link>Lucio Bragagnolo</link>lux@mac.com