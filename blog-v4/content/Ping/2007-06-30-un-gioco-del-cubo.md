---
title: "Un gioco del cubo"
date: 2007-06-30
draft: false
tags: ["ping"]
---

Sono riuscito a fare un <em>deathmatch</em> decente con <a href="http://assault.cubers.net/" target="_blank">AssaultCube</a> ma devo riconoscere che il gioco è ancora assai immaturo. Ogni tanto si finisce dentro un muro, o sotto il pavimento, e in un ambiente 3D è deleterio. Anche l'interfaccia non è pronta per un uso da parte della gente comune. È <em>open source</em>, crescerà.