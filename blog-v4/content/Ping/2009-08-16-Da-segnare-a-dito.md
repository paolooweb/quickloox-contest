---
title: "Da segnare a dito"
date: 2009-08-16
draft: false
tags: ["ping"]
---

Ma stai seguendo i dipinti che settimanalmente Jorge Colombo realizza per il <i>New Yorker</i> <a href="http://www.newyorker.com/online/blogs/tny/finger-painting/" target="_blank">con iPhone</a> e l’applicazione <a href="http://itunes.apple.com/WebObjects/MZStore.woa/wa/viewSoftware?id=288230264&amp;mt=8" target="_blank">Brushes</a>?

Per ognuno c'è il video che mostra tutte le fasi della sua realizzazione e resto ogni volta ammirato.