---
title: "Un&rsquo;avventura appena diversa dalle altre"
date: 2006-06-11
draft: false
tags: ["ping"]
---

Digita <code>emacs -batch -l dunnet</code> nel Terminale e prova l&rsquo;unica <a href="http://www.rickadams.org/adventure/a_history.html" target="_blank">avventura testuale</a> che conosca a prevedere l&rsquo;uso di una scheda logica di <a href="http://www.webmythology.com/VAXhistory.htm" target="_blank">Vax</a> per arrivare fino in fondo.

Non &egrave; difficile ma discretamente istruttiva e comunque, se sei disperato, c&rsquo;&egrave; anche la <a href="http://www.gamesover.com/walkthroughs/Dunnet.txt" target="_blank">soluzione</a>.