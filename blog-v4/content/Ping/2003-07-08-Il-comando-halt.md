---
title: "Un’altra uscita dal Terminale"
date: 2003-07-08
draft: false
tags: ["ping"]
---

Come fermare il sistema nel modo più rapido possibile

L’amico Orio mi aveva chiesto di pubblicare un elenco di “uscite di emergenza dal Terminale” e ne ho illustrate un po’. Siccome vedo spesso spiegazioni su come usare il comando shutdown del Terminale, ma nessuno parla delle possibili alternative, eccone una. il comando halt.

Equivale a spegnere il sistema. Esattamente come reboot equivale a riavviarlo.

Come direbbe Shakespeare se avesse un Mac, ci sono più cose nel Terminale che in tutta la nostra filosofia. :)

<link>Lucio Bragagnolo</link>lux@mac.com