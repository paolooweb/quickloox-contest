---
title: "Facile, su Mac"
date: 2009-04-17
draft: false
tags: ["ping"]
---

La guida <a href="http://www.guit.sssup.it/phpbb/viewtopic.php?t=4188&amp;postdays=0&amp;postorder=asc&amp;start=0" target="_blank">L'arte di scrivere con TeX</a> di Lorenzo Pantieri ha compiuto un anno e anche da un po' (il ritardo nella segnalazione è tutta colpa mia).

TeX è una gran cosa e su Mac è una cosa ancora migliore (la Guida, ricorda Lorenzo, è stata scritta su un iBook G4 equipaggiato con Mac OS X 10.4 Tiger). Prendo a prestito qualche paragrafo (le caratterizzazioni di stile ed evidenziazioni sono mie):

<cite>Per avere la sillabazione italiana</cite> [su Windows]<cite>, il caricamento del pacchetto</cite> babel <cite>è necessario ma in generale non sufficiente, in quanto serve anche che il file che contiene le relative regole di sillabazione sia attivato.</cite>

<cite>Se si usa</cite> MiKTeX<cite>, per attivare la sillabazione italiana bisogna selezionare, dal pannello</cite> Languages <cite>del menu </cite>Settings <cite>di</cite> MiKTeX<cite>, la voce</cite> Italian <code>e aggiornare il sistema</code> (Update Formats).

<cite>Se si usa TeX Live su Linux, a seconda della distribuzione può essere necessario lanciare</cite> <code>sudo texconfig-sys</code> <cite>dalla linea di comando, scegliere</cite> <code>hyphenation</code> <cite>e togliere il</cite> <code>%</code> <cite>davanti a</cite> <code>italian</code><cite> nel file che comparirà.</cite>

<cite>Se si usa la distribuzione <a href="http://tug.org/mactex/" target="_blank">TeX Live su Mac</a>, non c'è bisogno di fare nulla (tutto l'occorrente viene abilitato automaticamente durante l'installazione).</cite>

Quando una cosa è facile è su Mac.