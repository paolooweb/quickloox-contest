---
title: "Non solo gDisk<p>"
date: 2005-11-05
draft: false
tags: ["ping"]
---

Non era esclusiva, ma unicità<p>

<strong>Filippo</strong> mi ha ricordato che esiste un equivalente Windows (<a href="http://www.viksoe.dk/code/gmail.htm">Gmail Drive shell extension</a>) di <a href="http://gdisk.sourceforge.net">gDisk</a>, il programmino che trasforma i due giga e mezzo di una casella Gmail in un disco rigido virtuale.<p>

In effetti il mio pezzullo recente in proposito era scritto in modo ambiguo e poteva essere interpretato come se gDisk fosse stato un&rsquo;esclusiva Mac sia come funzione che come programma.<p>

È vera la seconda versione. Non è che per Windows non ci sia un programma che lo fa. C&rsquo;è un programma diverso che fa la stessa cosa.<p>

E qui volevo arrivare. Ogni tanto salta fuori qualcuno a chiedere &ldquo;ma esiste per Mac il programma Tale?&rdquo;. Invece dovrebbe chiedersi &ldquo;Esiste un programma Mac che svolge la funzione tale?&rdquo;. Rendersi conto di questo basta a smontare metà del mito per cui su Mac mancherebbero i programmi.<p>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>