---
title: "Il programma che non è"
date: 2002-07-02
draft: false
tags: ["ping"]
---

Celestia è uno dei pezzi di software più affascinanti che abbia visto finora

Non è un simulatore di volo; non è un atlante spaziale; non è un programma didattico; non è un videogioco; non è un quiz; non si fanno punti, non si vince niente, né si paga alcunché.
Eppure scaricati <link>Celestia</link>http://redivi.com/~bob/celestia.html per Mac OS X e, giusto per capire, scegli il comando Demo.
Sono sicuro che concorderai con me. È uno di quei programmi che non cancellerò dal disco per lungo, lungo tempo, intanto che percorro anni luce e macino Unità Astronomiche.

<link>Lucio Bragagnolo</link>lux@mac.com