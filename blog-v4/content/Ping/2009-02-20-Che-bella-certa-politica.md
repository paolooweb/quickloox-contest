---
title: "Che bella certa politica"
date: 2009-02-20
draft: false
tags: ["ping"]
---

L'altroieri sera sono stato a un aperitivo a Milano. Parlava Michele Vianello, vicesindaco di Venezia.

Ha mostrato come Venezia diventerà città <em>wi-fi</em> nel giro di pochi mesi, dopo che è stato chiesto alla cittadinanza dove fosse preferibile collocare gli <em>hotspot</em>.

Ha mostrato le <a href="http://www.comune.venezia.it/flex/cm/pages/ServeBLOB.php/L/IT/IDPagina/21871" target="_blank">applicazioni web 2.0</a> che consentono alla popolazione di essere informata in tempo reale e con piena trasparenza di varie attività e situazioni nevralgiche riguardanti la città, nonché il sistema <a href="http://www.veniceconnected.com/it/" target="_blank">Venice Connected</a> tramite i quali i turisti potranno prenotare le visita in anticipo, pagare prezzi diversi in funzione del periodo che scelgono, cercare alloggio e molto altro.

Molto altro significa che si sta puntando alla dematerializzazione dei servizi. Niente più code allo sportello, niente più scartoffie, niente più timbri: tutto via Rete. Venezia diventerà presto (mesi, non anni, e tutto è già oltre il collaudo) una città dove è possibile avere collegamento <em>wi-fi</em> dappertutto e dove persino i dipendenti pubblici potranno avere una scrivania virtuale che consenta loro di lavorare se necessario senza vincoli fisici. Software <em>open source</em> ovunque possibile, superamento delle lungaggini burocratiche, insomma un progresso di quindici anni rispetto all'oggi, pronto per domani.

Sembrava di stare in un altro mondo e non solo perché si parlava di pubblica amministrazione al reale servizio dei cittadini; perché Vianello ha tenuto botta per un'ora e non ha mai pronunciato le parole <em>partito</em>, <em>potere</em>, <em>maggioranza</em>, <em>opposizione</em>. Si è parlato invece di diritti dei cittadini e di servizi per i cittadini.

In Italia si è abituati a votare con i paraocchi e andare per colore. Lui è buono perché è fucsia, lui è cattivo perché è lilla. Invece andrebbe votata gente diversa, che mette quattro <em>slide</em> su un MacBook Air e ti spiega con semplicità e a suon di fatti come sta cambiando in meglio una città. Che bella, <em>questa</em> politica.