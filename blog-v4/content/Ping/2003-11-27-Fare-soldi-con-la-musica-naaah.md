---
title: "Fare soldi con la musica? Naaah"
date: 2003-11-27
draft: false
tags: ["ping"]
---

Non è la missione di Apple. Invece…

È opinione comune che l’iTunes Music Store, siccome è di gran lunga il sito numero uno al mondo per la vendita di musica online, faccia un mare di soldi.

È opinione sciocca. Il profitto di Apple su ogni singola canzone potrebbe essere di circa venti centesimi di dollaro. Basta fare due conti per scoprire che i profitti dello Store, per quanto benvenuti, sono una picccolissima parte del business.

Il business, quello vero, è fare dell’iTunes Music Store un meccanismo per vendere sempre più iPod. Ogni tre o quattro iPod venduti è come se Apple avesse venduto un Mac in più. Senza contare che diversi compratori di iPod vedono la luce e, dopo avere comprato iPod, passano a Macintosh.

Laddove si conferma per la fantastilionesima volta che il business di Apple è (principalmente) vendere hardware, non software.

<link>Lucio Bragagnolo</link>lux@mac.com