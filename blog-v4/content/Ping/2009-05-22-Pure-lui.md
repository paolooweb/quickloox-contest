---
title: "Pure lui"
date: 2009-05-22
draft: false
tags: ["ping"]
---

Uno dei motivi della ristretta diffusione di Darwin, la base open source di Mac OS X, è che non arriva pronto da installare, come succede a molte versioni di Linux.

Nel prossimo futuro potrebbe aiutare <a href="http://www.puredarwin.org/" target="_blank">PureDarwin</a>, di cui non mi era nota l'esistenza. Il progetto ha dato segno tangibile di vita a Natale, quando è uscita una Developer Preview, e il lavoro sta proseguendo.

La Developer Preview è disponibile come macchina virtuale pronta da installare in Fusion. Se si riuscisse ad arrivare almeno a un LiveCd non sarebbe affatto male.