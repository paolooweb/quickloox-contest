---
title: "La soluzione venti per cento<p>"
date: 2005-07-12
draft: false
tags: ["ping"]
---

Come esaudire l&rsquo;esigenza di software eccellente a buon prezzo<p>

In occasione dell&rsquo;imminente Macworld Expo Bare Bones, quelli di BBEdit e di Mailsmith, offrono fino al 22 luglio il <a href="http://www.barebones.com/special_offer.shtml">20 percento di sconto</a> praticamente su tutta la gamma di prodotti.<p>

Tranne che su <a href="http://www.barebones.com/products/textwrangler/index.shtml">TextWrangler</a>. Per forza; pur essendo un editor di testo eccellente, è gratuito.<p>

Da non perdere.<p>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>