---
title: "Voglio un notbook"
date: 2009-04-23
draft: false
tags: ["ping"]
---

Tim Cook, responsabile operativo e amministratore delegato supplente, alla presentazione dei risultati finanziari di Apple:

<cite>Per noi il punto è creare grandi prodotti. E quando guardo a ciò che viene venduto nell'area dei</cite> netbook <cite>oggi, vedo tastiere sacrificate, software terribile, hardware d'accatto, schermi molto piccoli e di fatto una esperienza [&#8230;] su cui francamente non metteremmo il marchio Mac. Cos&#236; come è oggi non è un'area cui siamo interessati, né crediamo vi siano interessati a lungo termine i consumatori.</cite>

<cite>Ciò detto, teniamo d'occhio l'area e siamo interessati a vedere come rispondono i consumatori. Quanti desiderano un computer piccolo, per cos&#236; dire, per navigare e per la posta potrebbero considerare un iPod touch o un iPhone. Abbiamo prodotti per soddisfare alcune delle esigenze per le quali la gente compra i</cite> netbook<cite>. In un certo senso operiamo già, indirettamente, su questo mercato.</cite>

<cite>E se troviamo un modo per realizzare un prodotto innovativo che realmente porta qualcosa di nuovo, lo faremo. In quest'area abbiamo idee interessanti. Le nostre prossime uscite per la linea Mac sono fantastiche. Se si guarda al passato, in 17 degli ultimi 18 trimestri siamo cresciuti più del mercato e fare meglio del mercato in questa orrenda situazione economica è davvero un traguardo, specialmente se si guarda a questi</cite> netbook <cite>dal costo veramente bassissimo, che credo sia una forzatura chiamare</cite> personal computer <cite>e che però gonfiano le statistiche delle unità vendute.</cite>

Se Apple vuole fare qualcosa, che non sia un inusabile <em>netbook</em>. Piuttosto un <em>notbook</em>, che cambi le regole del gioco, provochi e sconvolga come ha saputo fare iPhone, evidenzi la povertà di pensiero e di progetto della concorrenza.