---
title: "Calcolo, per passione<p>"
date: 2004-12-29
draft: false
tags: ["ping"]
---

Come è nato uno dei programmi più straordinari mai creati per Macintosh<p>

Su Macworld di carta la mia rubrica Preferenze del numero di marzo sarà dedicata alla Calcolatrice Grafica. Uno dei programmi più belli creati per Macintosh e, contemporaneamente, un progetto che non avrebbe mai dovuto esistere, al quale il programmatore capo ha lavorato di nascosto, senza un contratto, intrufolandosi nel campus di Apple senza averne teoricamente il diritto.<p>

Da una parte fa preoccupazione scoprire che le gemme nascono solo sconfiggendo l&rsquo;ottusità delle burocrazie. Dall&rsquo;altra, mi chiedo perché in Italia, nel Paese nelle burocrazie, dove tuttavia nessuna regola sembra degna di rispetto, non nasca neanche un millesimo di software come la Calcolatrice Grafica.<p>

Ne riparleremo. Nel frattempo, puoi scaricare la <a href="http://www.pacifict.com/gc14.dmg">beta</a> di Graphing Calculator per Mac OS X (o la versione <a href="http://www.pacifict.com/gc13.sit.hqx">gratuita</a>, o quella <a href="http://www.pacifict.com/Order.html">commerciale</a>, per Mac OS 9).<p>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>