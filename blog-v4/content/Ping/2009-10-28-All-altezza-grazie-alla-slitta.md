---
title: "All'altezza grazie alla slitta"
date: 2009-10-28
draft: false
tags: ["ping"]
---

<a href="http://www.bean-osx.com/Bean.html">Bean</a>, che ho scelto tempo fa come sostituto di TextEdit, per ora sta dimostrandosi all'altezza. Impagabile la sua funzione di zoom della pagina con il cursore a slitta, veramente di prossima generazione.

Solo una volta ho riscontrato un curioso problema di visualizzazione, ma non sono riuscito a trovare una causa precisa e la cosa non è più ripetuta.

Rimane l'incompatibilità con AppleScript. Però aspetto fiducioso.