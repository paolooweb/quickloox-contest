---
title: "Il compressore più potente"
date: 2003-05-29
draft: false
tags: ["ping"]
---

Piccolo test occasionale su un compito impossibile

Mi sono ritrovato a dover ridurre un file al minimo possibile partendo da un filmato di 23,1 megabyte. Comprimere, non ricampionare o ridimensionare.

Il problema è che un file Mpeg è già compresso di suo ed è impossibile raggiungere risultati significativi. Però ogni byte contava e allora ho provato ugualmente, con DropStuff e DropZip.

Ho impostato entrambi al massimo livello di compressione. Per DropStuff questo significa attivare l’ottimizzazione per Mac OS X, che produce file di estensione .sitx anziché .sit e migliora ulteriormente il risultato.

Dato finale: DropZip 22,3 megabyte, DropStuff 21,8 megabyte. Vince largamente DropStuff. Lo terrò presente per una prossima volta.

<link>Lucio Bragagnolo</link>lux@mac.com