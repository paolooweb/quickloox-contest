---
title: "Ringraziamenti a Babbo Natale / 1"
date: 2010-12-27
draft: false
tags: ["ping"]
---

Dalla slitta è caduto sotto l'albero del sottoscritto un set completo di accessori per iPad: custodia ufficiale Apple, Camera Connection Kit e convertitore Vga, utilissimo per videoproiettori e schermi esterni.

Il collaudo del Camera Connection Kit è avvenuto con pieno successo, proprio per l'importazione in iPad delle foto scattate con iPhone durante il classico pranzo di Natale. La facilità è imbarazzante e l'operazione è stata rapida e indolore.

Due parole sulla custodia, il cui tocco lievemente gommoso a me piace molto (altrimenti non avrei scritto a Babbo Natale) e che però non tutti ameranno, per quanto sia necessario salire di prezzo per fare meglio.

L'oggetto si spacchetta con gusto e fa piccola meraviglia vedere penzolare una terza anta una volta aperta la confezione di cartone. Ci si rende conto subito che si tratta del panno per la pulizia dello schermo, piacevole da vedere disteso e non piegato.

La custodia aderisce come un guanto e risulta leggera e sottile, esattamente quello che mi piace. Per me è vitale avere sempre a disposizione il rialzo per quando scrivo sulla tastiera virtuale di iPad e la custodia mi serve esattamente a questo. Non dovessi scrivere, userei l'apparecchio nudo, al massimo della leggerezza e al minimo dello spessore.

I fori di accesso a ingressi e pulsanti corrispondono con ottima, non perfetta, precisione. Sarebbe perfetta una leggera rastrematura alla base dell'aletta interna da ripiegare all'interno della custodia per fermare in posizione iPad. In mancanza della suddetta rastrematura, la piegatura presenta una lieve abbondanza sugli angoli, di rilievo solo per l'estetica.

Esistono custodie infinitamente più rifinite, eleganti e costose, in materiali pregiati, perfino accessoriate. Al momento scelgo una soluzione essenziale e di ingombro minimo, preferenze per le quali la custodia Apple è assolutamente ideale.

iPad ringrazia e io pure.