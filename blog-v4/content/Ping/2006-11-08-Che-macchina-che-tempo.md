---
title: "Che macchina, che tempo"
date: 2006-11-08
draft: false
tags: ["ping"]
---

<a href="http://www.opencommunity.co.uk/vienna2.php" target="_blank">Vienna</a> ha avuto un aggiornamento e adesso ha perso un filo di velocità in favore di maggiore usabilità (per esempio ora si può modificare manualmente l’ordine dei feed).

Per motivi vari sta diventando per me sempre più programma, direbbero gli americani, <em>mission critical</em>.

Il pretesto però era un altro. Mi sono registrato a un feed di notizie australiano e, per effetto del fuso orario, ho potuto leggere una notizia la cui pubblicazione era avvenuta… domani.

<a href="http://web.tiscali.it/terzamusa/wells.htm" target="_blank">H.G. Wells</a>, non sei nessuno.