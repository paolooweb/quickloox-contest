---
title: "Il mondo non è tutto bianco o nero"
date: 2010-10-15
draft: false
tags: ["ping"]
---

Aggiorno alla versione 7 l'indispensabile <a href="http://www.lemkesoft.biz/gc7/gc7_en.html" target="_blank">GraphicConverter</a>, che si è convertito all'ambiente di programmazione Cocoa ed è più Mac-like che mai, oppure mi tengo la mia versione 6.7.4 che fa cinquanta volte quello che mi serve?

A volte ci sono valutazioni di acquisto che vanno oltre la mera utilità.