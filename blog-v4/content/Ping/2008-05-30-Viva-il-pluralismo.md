---
title: "Viva il pluralismo"
date: 2008-05-30
draft: false
tags: ["ping"]
---

<a href="http://www.afhome.org" target="_blank">Francesco</a>, incapace di muovere un dito in modo non interessante, mi segnala <a href="http://www.pseudotecnico.org/blog/" target="_blank">pseudotecnico:blog</a>. Vi si parla di informatica in generale ma spesso di Mac.

Con opinioni che condivido in misura varia e spesso marginalmente (troppo facile cadere nella trappoletta a buon prezzo MacBook-Air-ha-poca-dotazione&#8230;), ma sempre motivate, meditate e da leggere.

È talmente raro poter raccomandare qualcuno con cui non sono d'accordo che non posso farmi scappare l'occasione. E ringraziare Francesco.