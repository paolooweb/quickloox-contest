---
title: "Le dimensioni contano"
date: 2006-05-04
draft: false
tags: ["ping"]
---

Quelle dei link. L’amico Stefano ha messo a punto tutto da solo <a href="http://www.hardclicker.com/" target="_blank">Hardclicker</a>, un sistema di accorciamento dei link troppo lunghi, che si misura alla pari con concorrenti più vecchi come SnipUrl o TinyUrl. Ho il vago sospetto, ma non la certezza tecnica, che il sistema di Stefano sia già tarato sulle dimensioni della Internet attuale, là dove gli altri invece tra un po’ mostreranno la corda. Staremo a vedere. E cliccheremo, ovvio.