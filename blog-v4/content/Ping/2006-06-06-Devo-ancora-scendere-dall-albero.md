---
title: "Devo ancora scendere dall&rsquo;albero"
date: 2006-06-06
draft: false
tags: ["ping"]
---

Per dare una mano al pap&agrave;, che dopo avere digitalizzato tutti gli album fotografici della sua vita ora sta ricostruendo l&rsquo;albero genealogico del parentado, sto iniziando a esaminare i programmi dedicati. Per ora ho lanciato <a href="http://genj.sourceforge.net/" target="_blank">GenealogyJ</a>, che &egrave; free e open source ma mi sembra poverello. Ma &egrave; ancora presto. Magari avr&agrave; un grande avvenire, per ci&ograve; che &egrave; alle mie spalle. :-)