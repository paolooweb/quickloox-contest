---
title: "Attualmente verso l'Australia"
date: 2008-12-24
draft: false
tags: ["ping"]
---

Si avvicina Natale, questione di ore, e i bimbi intorno a me stanno entrando in agitazione. Per calmarli c'è <a href="http://www.noradsanta.org" target="_blank">il sistema di sorveglianza della slitta</a> allestito da Google e dal Norad, il comando di difesa aerospaziale nordamericano.