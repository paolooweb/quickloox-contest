---
title: "Errore reiterato, errore fortunato<p>"
date: 2005-01-11
draft: false
tags: ["ping"]
---

Bello poter essere felici dei propri errori<p>

Ricordo una sola volta come questa: il giorno in cui mi sono avviato alla conferenza stampa Apple pensando <em>presenteranno tutto, ma certamente non iPod per Windows</em>.<p>

Avevo tortissimo. Ma la cosa bella è che Apple aveva ultraragione e con iPod per Windows sta incassando cifre strepitose.<p>

Spero che perseverare nell&rsquo;errore (mio) abbia lo stesso effetto. Sarebbe proprio un bel viatico per il 2005 della Mela.<p>

Adesso vado a studiare.<p>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>