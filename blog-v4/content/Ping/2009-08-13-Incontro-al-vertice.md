---
title: "Incontro al vertice"
date: 2009-08-13
draft: false
tags: ["ping"]
---

Il modo più comodo di aggiornare Mac OS X è lasciare fare a lui. Fa tutto da solo.

Poi c'è una categoria di persone che crede di essere più furba di un battaglione di programmatori. Sono quelli che <i>io l'aggiornamento lo scarico da me</i> (forse lo verificano byte per byte, passando le mani sul cavo di rete per sentire se i dati scorrono bene) <i>e scarico il combo</i>, quello che reinstalla da capo tutti gli aggiornamenti dal primo all'ultimo. Cos&#236; riescono anche a metterci più tempo.

Esiste un'altra categoria di persone, che per sentirsi in sintonia con il mondo deve riparare i permessi. Anche se non ci sono problemi di permessi. Cos&#236;, perché riparare i permessi suona bene, sa di prendersi cura della macchina.

Le due categorie si sono <a href="http://discussions.apple.com/thread.jspa?threadID=2106528&amp;start=0&amp;tstart=0" target="_blank">incontrate in un vertice sui forum Apple</a> e stanno facendo scintille. S&#236;, perché hanno ibridato ed è nata una nuova specie: quello che installa il combo e ripara pure i permessi. E alla fine, come stupirsene?, magari ha qualche problema.

Al momento il parere più autorevole spiega che la cosa migliore da fare sia installare il combo <i>due volte</i>. Di questo passo riusciranno a impegnare una giornata intera per stare dietro a una funzione totalmente automatica.