---
title: "(Fumetti) imperdibili"
date: 2007-01-12
draft: false
tags: ["ping"]
---

Sono tornati i <a href="http://desktoptales.blogspot.com/" target="_blank">Desktop Tales</a>, di cui ebbi il privilegio di parlare in anteprima tanto tanto tempo fa.

La fatica di un bookmark in più a volte è <em>molto</em> piacevole. :-)