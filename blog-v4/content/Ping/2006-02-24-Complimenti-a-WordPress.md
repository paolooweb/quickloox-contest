---
title: "Complimenti a WordPress"
date: 2006-02-24
draft: false
tags: ["ping"]
---

Mi sono chiesto che cosa sarebbe successo programmando un Ping per farlo uscire in data 29 febbraio 2006.

<a href="http://wordpress.org/" target="_blank">WordPress</a>, che qui fa da motore, lo ha accettato senza fare una piega, ma almeno lo ha inserito datandolo primo marzo.

Non credo che tutti i motori di pubblicazione siano altrettanto sagaci.