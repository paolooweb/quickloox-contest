---
title: "Voglio pagare il doppio"
date: 2003-07-01
draft: false
tags: ["ping"]
---

Non si capisce perché Apple debba regalare software geniale a chi non ha alcun genio

Vedo transitare su alcune mailing list un penoso dibattito sulla liceità o meno di Apple di fare pagare Mac OS X 10.3.

A parte il fatto che nessuno obbliga a comprarlo, vorrei dire solo una cosa. Sto provando la prerelease mostrata al convegno mondiale degli sviluppatori Apple e tacerò di tutto, ma una parola la devo dire: Exposé.

Ho già pagato per avere Mac OS X 10.3. Ma per avere Exposé sarei stato disposto a pagare anche il doppio. Chi si lamenta del prezzo e vuole l’innovazione gratis, vada su Windows. Tra un anno potrà piratare felice una cosa che assomiglia ma fa schifo.

Io preferisco pagare e avere gente che progetta cose come Exposé. Non mi va di essere sfruttato.

<link>Lucio Bragagnolo</link>lux@mac.com