---
title: "Voterai Microsoft"
date: 2009-05-17
draft: false
tags: ["ping"]
---

Microsoft avrebbe un ritmo interno di produzione che le ha impedito di essere aggiornata sui progressi dello standard Odf e le mancanze di Office nel leggere correttamente i formati di OpenOffice.org, nonostante le promesse, sarebbero dovute a questo e non, a quanto sembra, a malafede.

Spero che sia vero. Di sicuro, più il processo è lento più fa comodo a Microsoft.

Per capire con chi si ha a che fare, facciamo un salto in Europa.

La Commissione Europea fa sapere a tutti con clamore quando <a href="http://www.macworld.it/showPage.php?template=notizie&amp;id=13442" target="_self">multa Microsoft per abuso di posizione dominante</a> (promemoria: Microsoft abusa della propria posizione). Invece non ha fatto sapere a nessuno di avere le mani in pasta con Microsoft.

Il parlamentare europeo Marco Cappato riferisce che agli eletti vengono <a href="http://lnx.marcocappato.it/node/38908" target="_blank">forniti computer Microsoft-centrici</a> e che, parlando di <em>browser</em>, su quattromila utenti ci sono una decina di installazioni tollerate di Firefox e tutto il resto è Internet Explorer (nel mondo reale <a href="http://marketshare.hitslink.com/browser-market-share.aspx?qprid=0" target="_blank">Firefox è un browser su cinque</a>).

Sempre Cappato ha chiesto di poter conoscere gli esiti di uno studio europeo del 2005 di valutazione sulle differenze di costo tra adozione di software <em>open source</em> e adozione di software proprietario.

Gli hanno risposto che no, non si poteva sapere, perché avrebbe potuto danneggiare gli interessi dell'azienda, in quanto <cite>l'Unione Europea ha contratti specifici e privilegiati con Microsoft</cite>.

Alla fine della legislatura, passata la festa gabbato lo santo, Cappato ha finalmente potuto ricevere lo studio, che ora <a href="http://lnx.marcocappato.it/node/38918" target="_blank">è pubblico</a>.

Lo studio è un polverone capolavoro e per cavarne fuori qualcosa ci vogliono giorni di fatica. La certezza è che, qualunque crocetta metterai sulla scheda elettorale europea, voterai Microsoft.

L'Unione Europea ha tutto il diritto di scegliere il software che vuole, ma ha il dovere pieno di informare i cittadini e rendere conto delle sue decisioni (i soldi li mettiamo noi). Che l'Europa abbia un contratto privilegiato con Microsoft ci può anche stare. Che i termini del contratto e i motivi della decisione debbano restare segreti, per un'amministrazione pubblica, manda cattivo odore.