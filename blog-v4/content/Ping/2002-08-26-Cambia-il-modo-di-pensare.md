---
title: "Cambia il modo di pensare"
date: 2002-08-26
draft: false
tags: ["ping"]
---

Il perché della coraggiosa campagna pubblicitaria Apple

Ritorno sul tema di <link>Switch</link>http://www.apple.com/switch, la campagna pubblicitaria Apple che ritrae persone comuni che parlano della loro esperienza con Windows e della successiva decisione di passare a Mac.

Prima di tutto non sono attori, ma gente comune, che ha risposto a un invito pubblicato da Apple sul proprio sito. Uno di loro riceve mail di insulti da quando ha accettato di partecipare.

Secondo, perché loro e perché così? Perché attaccare il mercato Windows è meno facile di quello che pensa la gente comune. Magnificare le virtù di Mac rispetto a Windows non funziona: a nessuno fa piacere sentirsi dire “hai sbagliato e devi cambiare”. Inoltre, più Apple si muove per dire che Windows è peggio, più il resto del mondo si muove per negare l’evidenza. Intel paga una percentuale consistente delle campagne pubblicitarie promosse dai costruttori di computer con Cpu Intel; Motorola no.

Allora giusto dare spazio a esperienze autentiche, innegabili, non contestabili se non con falsità e attacchi personali, che danno la misura di chi sta dall’altra parte. Esperienze in cui una persona possa riconoscersi e, forse per la prima volta, iniziare a “pensare differente”.

Anche perché un’altra cosa che ignora la gente comune è questa: la gente non compra Windows perché compie una scelta: compra Windows dato che non ha neanche l’idea che possa esserci un’alternativa. Aiutiamoli. Switch.

<link>Lucio Bragagnolo</link>lux@mac.com