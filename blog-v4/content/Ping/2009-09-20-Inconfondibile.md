---
title: "Inconfondibile"
date: 2009-09-20
draft: false
tags: ["ping"]
---

Sempre cortesia di <b>Roberto</b>, un altro capolavoro di Windows nel pieno esercizio delle sue funzioni pubbliche. Anche nella terra degli ideogrammi, quando salta in aria il software si capisce subito.

Quando <i>crasha</i> il sistema operativo in Cina, lo prendono a&#8230; bacchettate?