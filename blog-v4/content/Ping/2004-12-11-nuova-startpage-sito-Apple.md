---
title: "Un nuovo inizio<p>"
date: 2004-12-11
draft: false
tags: ["ping"]
---

Ora perfino Safari conosce il posto giusto dove andare<p>

Non so se hai installato nuovi Mac ultimamente. Una delle cose più sconcertanti era come, con tutto lo sforzo di marketing e di attenzione ai dettagli che Apple mette nella sua produzione, Safari fosse preimpostato per aprire la pagina livepage.apple.com.<p>

Una pagina che risaleva ai tempi di Noè informatico e magari utilissima, per carità, ma era sempre la seconda cosa che cancellavo (la prima è Internet Explorer).<p>

Qualcosa si è finalmente mosso e ora Apple ha una <a href="http://www.apple.com/startpage">nuova pagina</a> di inizio impostata in Safari. Meglio tardi che mai.<p>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>