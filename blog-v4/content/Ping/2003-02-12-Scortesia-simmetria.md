---
title: "Scortesia, simmetria"
date: 2003-02-12
draft: false
tags: ["ping"]
---

Quando “ho un computer” significa non dover mai chiedere “per favore”

Pochi giorni fa ho letto un messaggio in mailing list: vorrei, diceva più o meno, un programma facile, non di testo, per fare pagine Web, possibilmente gratis.

Io l’ho letto come: quello che interessa a me deve arrivarmi tutto, subito, senza doverci mettere impegno e senza spendere una lira.

Non avevo tempo per rispondere, la risposta sarebbe stata lunga, mi sarei dovuto impegnare e non ero pagato per farlo.

Non ho risposto. Scortesia? No, simmetria.

<link>Lucio Bragagnolo</link>lux@mac.com