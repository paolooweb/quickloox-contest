---
title: "Che fine ha fatto Tastiera?"
date: 2004-02-03
draft: false
tags: ["ping"]
---

In Panther ha cambiato posto. Ed è migliorato

Una volta era un accessorio di scrivania, e Tastiera, o Key Caps in inglese, era stato promosso a utility sotto Mac OS X.
In Panther però manca, sia nella cartella Applicazioni che in quella Utility. Dove è finito?

Nelle Preferenze di sistema, in Internazionale, sotto Menu Input. Ha anche cambiato nome, ma l’icona è sempre quella.

<link>Lucio Bragagnolo</link>luxmac.com