---
title: "Un altro chiodo nella bara (di chi ci crede)"
date: 2010-11-24
draft: false
tags: ["ping"]
---

Altro segnale chiarissimo che Apple oramai punta solo agli apparecchi iOS e trascura Mac.

Nel trimestre estivo, fonte Idc, la vendita di Mac è cresciuta del 28,5 percento e quella del resto dei Pc del 9,7 percento. Praticamente Mac <a href="http://www.tuaw.com/2010/11/23/mac-sales-growth-continues-to-surge-ahead-of-pcs-3-to-1/" target="_blank">cresce il triplo</a>, facendosi largo soprattutto nell'ambito <i>business</i>.

Qualunque margine ragionevole di errore, doveroso da supporre perché questi istituti di ricerca si prendono davvero molte libertà nel fare i conti, non può eliminare il fatto che Mac stia correndo più in fretta dei Pc e anche decisamente più in fretta.

Mica male, Apple: in un mercato di spietata concorrenza, trascura un prodotto e come risultato questo accelera. Fosse matematica, ne potremmo desumere che quando Apple schiferà Mac <i>in toto</i> le sue vendite schizzeranno all'infinito.

Per approfondimento, questo è il <a href="http://www.idc.com/about/viewpressrelease.jsp?containerId=prUS22531110&amp;sectionId=null&amp;elementId=null&amp;pageType=SYNOPSIS" target="_blank">comunicato originale di Idc</a>.