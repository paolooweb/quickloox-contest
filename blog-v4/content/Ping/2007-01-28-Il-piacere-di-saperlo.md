---
title: "Il piacere di saperlo"
date: 2007-01-28
draft: false
tags: ["ping"]
---

Viene fuori che iPhone è stato in lavorazione per due anni e mezzo (e nessuno dei siti preveggenti ha saputo mostrare niente di sensato fino a un'ora dopo il keynote di Steve Jobs, ma è un'altra storia).

A gennaio 2005 usciva il mio PowerBook e Apple intanto stava già lavorando su iPhone. Il pensiero eccitante è che, ora, tutti guardiamo iPhone e Apple sta già lavorando a qualcosa che vedremo solo nel 2009.

Una sensazione inebriante che ho provato di nuovo con <a href="http://www.wow-europe.com" target="_blank">The Burning Crusade</a>, l'espansione di World of Warcraft. Un sacco di posti che, nel gioco, erano chiusi o inutilizzati, improvvisamente hanno acquistato senso e funzione. Un molo abbandonato porta a un'isola che sulle mappe appariva irraggiungibile, e ora non è più. Un portone prima sigillato adesso si apre su novità appassionanti. Le capacità che sembravano avere raggiunto il loro limite massimo ricominciano a crescere.

La morale è che, quando passando per Azeroth o per Outland vedo una costruzione senza entrate, o un territorio apparentemente vuoto e inutile, non penso che non abbiano avuto abbastanza idee per riempire tutto; non hanno abbastanza tempo per inserire tutte le idee che hanno e questa è una bella garanzia. Anche per Apple, gli <a href="http://www.apple.com/it/iphone" target="_blank">iPhone</a> e tutto quello che apparirà in Leopard, di cui in Mac OS X ci sono tutte le premesse, anche se sembrano inutili o superflue.