---
title: "Anche dipinto"
date: 2009-04-02
draft: false
tags: ["ping"]
---

L'unico motivo che mi trattiene dal fare una pazzia e acquistare una costosissima <a href="http://www.20x200.com/art/2009/04/apple-1.html" target="_blank">stampa a grandi dimensioni dell'Apple I</a> ad altissima qualità e in tiratura limitata è che non saprei dove appenderla.