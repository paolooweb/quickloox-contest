---
title: "Safari non fa testo"
date: 2008-09-09
draft: false
tags: ["ping"]
---

A volte capita di digitare testo in una pagina web e rendersi conto che vogliamo passare a un'altra pagina, e che non ci frega niente di quanto abbiamo digitato (a me capita spesso quando commento su Ping, poi mi mordo la lingua e lascio perdere).

Safari avverte del rischio di perdere testo. Nel 99 percento dei casi è inutile e fa perdere tempo.

Allora si può <a href="http://www.tuaw.com/2008/09/09/terminal-tips-stop-safari-saved-text-dialogs/" target="_blank">scrivere nel Terminale</a>:

<code>defaults write com.apple.Safari DebugConfirmTossingUnsubmittedFormText NO</code>

E l'avviso non comparirà mai più. Almeno non fino a quando non si ripeterà il comando con <code>YES</code> al posto di <code>NO</code>.

Il problema di scrivere cose di cui ci si pente prima di dare l'Ok resta, ma non c'è Terminale che tenga. :-)