---
title: "Sony non compra Apple"
date: 2004-03-11
draft: false
tags: ["ping"]
---

Ogni tanto risorgono dalle tombe telematiche i gossip mai morti

Non si capisce perché, ma alcuni siti hanno ripreso una “notizia” secondo cui Sony sarebbe interessata a comprare Apple.

La “notizia” è rancida come il latte troppo scaduto, ma evidentemente ogni tanto mancano altri argomenti.

Diciamo che la possibilità che Apple sia comprata da Sony è equivalente alla possibilità che Steve Jobs venga eletto presidente degli Stati Uniti nel 2004.

<link>Lucio Bragagnolo</link>lux@mac.com