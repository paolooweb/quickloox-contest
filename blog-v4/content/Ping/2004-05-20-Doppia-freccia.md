---
title: "Doppia freccia"
date: 2004-05-20
draft: false
tags: ["ping"]
---

Una impostazione del Finder che non tutti possono impostare

Una volta su Mac OS era possibile impostare le barre di scorrimento delle finestre in modo che avessero due frecce su ambedue gli estremi della barra. Il Finder di Mac OS X invece non lo consente.

I tranquilli possono usare <link>TinkerTool</link>http://www.bresink.de/osx/TinkerTool.html. I temerari, invece, possono aprire il Terminale e digitare

defaults write .GlobalPreferences AppleScrollBarVariant -string DoubleBoth;killall Finder

Se si pentono, possono sempre tornare indietro usando le normali Preferenze di Sistema.

<link>Lucio Bragagnolo</link>lux@mac.com