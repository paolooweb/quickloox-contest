---
title: "C'è sposta per te"
date: 2009-03-25
draft: false
tags: ["ping"]
---

<strong>Alex</strong> mi ha chiesto come aderire a MacLovers, la lista Mac del grande <em>Blue</em> Bottazzi.

E ho scoperto, credo per penultimo sul pianeta Terra dopo Alex, che la lista si è trasferita da Yahoogroups a Google Groups.

In ambito Mac rimane uno dei posti migliori da frequentare e dunque, metti che fossi solo il terz'ultimo, vale la pena di passare l'informazione.