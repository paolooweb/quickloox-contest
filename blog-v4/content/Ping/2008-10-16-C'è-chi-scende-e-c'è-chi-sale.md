---
title: "C'è chi scende e c'è chi sale"
date: 2008-10-16
draft: false
tags: ["ping"]
---

La nuova gamma portatile di Apple ha provocato piccoli aggiustamenti nella mia classifica personale di gradimento, assai più concreta del solito dal momento che devo cambiare Mac entro la fine dell'inverno e forse prima.

In cima alla lista resta il 17&#8221;&#8230; che non c'è. Se ci fosse e fosse cos&#236;, però, lo vorrei fortemente. E credo arriverà più tardi come era già successo l'anno scorso, probabilmente come attrazione di Macworld Expo.

Il monoblocco di alluminio, sottovalutato dai più, è un progresso eccezionale in termini di pulizia del prodotto. Niente più viti, cerchiature di plastica, bordini, incastri, tolleranze, fessure. Sul mio PowerBook attuale ci sono otto vitine, quattro per fiancata, che se spariscono è solo un bene.

Avrei una scheda grafica ottima e una Gpu ausiliaria. Tutto preziosissimo per quando arriva <a href="http://www.apple.com/it/macosx/snowleopard/" target="_blank">Snow Leopard</a>. Usando solo tastiera e di tanto in tanto la <em>trackpad</em> quando proprio non se ne può fare a meno, la <em>trackpad</em> cliccabile è una evoluzione benvenuta.

Se anche nei 17&#8221; si potrà avere qualsiasi schermo purché sia <em>glossy</em>, mi toccherà il <em>glossy</em>. Di istinto preferisco gli schermi opachi; ho sentito gente raccontare che i nuovi <em>glossy</em> retroilluminati a Led sono un'altra vita rispetto a prima. La verità è che non ho una esperienza pratica sufficiente per dare un giudizio.

Secondo in classifica, MacBook Air. Invece che andare per lo schermo più grande possibile, andrei per la massima leggerezza possibile. Con tutta probabilità potrei giocare a <a href="http://www.worldofwarcraft.com/wrath/" target="_blank">Wrath of the Lich King</a>, cosa prima impossibile, e rispetto al mio disco rigido attuale guadagnerei venti gigabyte. Migrare Time Machine in modalità <em>wireless</em> sarebbe indolore.

Terzi, a pari merito, tutti gli altri, che è un altro modo per dire ultimi. Sono tutte proposte ottime che semplicemente troveranno accoglienza presso utenze diverse dalla mia. Il mondo è vario cos&#236; da essere bello.