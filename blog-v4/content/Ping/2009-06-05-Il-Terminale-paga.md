---
title: "Il Terminale paga"
date: 2009-06-05
draft: false
tags: ["ping"]
---

Hanno creato uno <em>screencast</em> di 70 minuti che spiega <a href="http://peepcode.com/products/meet-the-command-line" target="_blank">come usare il Terminale</a>, in modo apparentemente ben studiato.

Costa nove dollari ed è in inglese. Senza inglese, comunque, è abbastanza inutile pensare di volersi interessare al Terminale (e di inglese ne basta pochino pochino).

La parte interessante non è la questione del costo, ma che qualcuno abbia pensato a una possibile nicchia di mercato fatta di non tecnici che vogliono saperne di più.

La materia è effettivamente infinita: ho scoperto giusto ora che <code>Ctrl-X</code> e <code>Ctrl-V</code> mostrano il numero di versione esteso di bash, il motore del Terminale in Mac OS X.

Per avere l'elenco di tutti i comandi installati (io sono a 1.792), <code>Ctrl-X</code> e poi il punto esclamativo.