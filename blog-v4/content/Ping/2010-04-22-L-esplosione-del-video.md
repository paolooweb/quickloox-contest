---
title: "L'esplosione del video"
date: 2010-04-22
draft: false
tags: ["ping"]
---

S&#236;, ci sono cose che Flash può fare e Html5 ancora no.

Ma ci sono anche cose che Html5 può fare fin da subito e Flash non potrà mai. Prova a fare <a href="http://www.craftymind.com/2010/04/20/blowing-up-html5-video-and-mapping-it-into-3d-space/" target="_blank">esplodere questo video</a>, con mappatura dei cocci nello spazio tridimensionale.