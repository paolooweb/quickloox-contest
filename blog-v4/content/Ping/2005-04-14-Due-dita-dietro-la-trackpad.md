---
title: "Due dita dietro<p>"
date: 2005-04-14
draft: false
tags: ["ping"]
---

Caratteristica particolare delle nuove trackpad nei PowerBook<p>

Me ne sono accorto per caso. Le nuove trackpad hanno la famosa gesture delle due dita che, fatte scorrere in senso ortogonale oppure rotatorio, provocano lo scorrimento della finestra.<p>

La cosa singolare è che, se a essere attivo è il Finder, la finestra che scorre non ha bisogno di essere quella in primo piano. Detto in altre parole, se il puntatore si trova sopra una finestra del Finder almeno parzialmente visibile ma comunque non attiva, con le due dita sulla trackpad è possibile farla scorrere.<p>

Ah, se la trackpad facesse cose strane, resettala. Appoggiaci tutto il palmo per tre o quattro secondi e poi levalo con una mossa felina. Lo dice Apple, sarà vero.<p>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>