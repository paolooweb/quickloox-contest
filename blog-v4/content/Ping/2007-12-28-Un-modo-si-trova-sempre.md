---
title: "Un modo si trova sempre"
date: 2007-12-28
draft: false
tags: ["ping"]
---

Uno dei meriti di questo blog è l'essere pieno di gente bravissima, assai più di me. Leggi, per esempio, che dice <strong>Filippo</strong>:

<cite>Ho letto nel tuo blog dell'impossibilità di ascoltare l'Hallelujah Chorus di Haendel eseguito dalla London Symphony Orchestra.</cite>

<cite>Allora non mi sono perso d'animo e ho cercato su Google &#8220;christmas song web radio&#8221;. La ricerca m'ha dato, tra i tanti link, <a href="http://www.web-radio.fm/christmas/" target="_blank">questo</a>.</cite>

<cite>Ho cliccato in particolare su un link di un <a href="http://www.sky.fm" target="_blank">sito che già conoscevo</a>, da cui trasmettono in streaming fantastiche canzoni di smooth jazz.</cite>

<cite>C'è per questo periodo di feste una nuova sezione, &#8220;<a href="http://www.sky.fm/christmas/" target="_blank">The Christmas Channel</a>&#8221;</cite>.

<cite>Se si clicca sul rettangolino azzurro riportante la scritta &#8220;96k&#8221;, posto vicino ad un piccolo logo romboidale contenente all'interno una saetta gialla, si procede allo scaricamento di un piccolissimo file che ha estensione .pls (cioè Playlist audio) apribile da iTunes.</cite>

<cite>L'ascolto di bellissime canzoni e/o jingle, classici e moderni, allieterà in sottofondo queste giornate di festa!</cite>

<cite>Ti auguro buon ascolto!</cite>

E io a tutti. Grazie. :-)