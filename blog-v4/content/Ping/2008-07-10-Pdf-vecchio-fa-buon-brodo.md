---
title: "Pdf vecchio fa buon brodo"
date: 2008-07-10
draft: false
tags: ["ping"]
---

Mdj recupera molto più lentamente del previsto. Ciò non significa che faccia mancare spunti interessanti introvabili altrove.

Tutti i 937 numeri di Mdj in formato Pdf sono stati compressi in tre modi diversi: con Stuffit in formato .sitx, in formato .zip ottenuto tramite un comando ditto e con bzip2.

I risultati sono chiari. Se i Pdf sono vecchi (in particolare anteriori a luglio 2007), Stuffit è il sistema che raggiunge la maggiore compressione, con un vantaggio superiore al 20 percento rispetto al secondo in classifica. Il secondo miglior sistema è lo zip, ovviamente al massimo della compressione impostato nei parametri. bzip2 arriva dopo.

Su 937 Pdf compressi, Stuffit ha ottenuto la migliore compressione 918 volte e per 19 volte l'ha ottenuta Zip. Bzip2 è sempre arrivato secondo o terzo.

Dopo luglio 2007 però le cose sono cambiate e di molto. I Pdf compressi dopo quella data mostrano differenze minuscole e trascurabili tra i tre formati.

La conclusione è che da un anno a questa parte Adobe ha migliorato grandemente la compressione interna dei Pdf. E alle utility esterne non è rimasto più molto su cui lavorare.

Tutto ciò vale entro i confini qui descritti. È ampiamente possibile che in un'altra circostanza, con altri file, i risultati cambino e pure molto. In altre parole&#8230; mai espandere il giudizio sugli strumenti di compressione.