---
title: "Ne vorrei anche per il web"
date: 2010-08-15
draft: false
tags: ["ping"]
---

Sono rimasto estasiato da <a href="http://www.tomscott.com/warnings/" target="_blank">questi adesivi</a>, che il tizio si è autoprodotto per incollarli sulle copie dei quotidiani gratuiti che trova in giro per il metrò di Londra.

Ora, se qualcuno ne realizza una versione per il web di Macity e compagnia, ne attacco qualcuno volentieri.

Cos&#236; mi resta l'appicicaticcio sulle mani, meglio che nella testa.

In particolare punto agli adesivi <i>Questo articolo è sostanzialmente il copiaincolla di un comunicato stampa</i>, <i>Questo articolo si basa su una soffiata anonima non verificata</i>, <i>Questo articolo è stato scopiazzato da un'altra fonte per fare più in fretta</i>, <i>Il giornalista non ha padronanza dell'oggetto dell'articolo</i> e <i>Il giornalista sta nascondendo la propria opinione dietro frasi come &#8220;Secondo alcune voci&#8221;</i>.