---
title: "Navigando si impara"
date: 2007-03-29
draft: false
tags: ["ping"]
---

<a href="http://multifinder.wordpress.com" target="_blank">Mario</a> mi ha mostrato <a href="http://www.macinstruct.com/" target="_blank">MacInstruct</a>. La home page ovviamente cambierà, ma quella che ho visto io al primo impatto titolava <em>How to Backup Your Mac Using Rsync</em> e diciamo che è nata una simpatia. :-)