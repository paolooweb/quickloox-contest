---
title: "Giovani tromboni"
date: 2009-03-20
draft: false
tags: ["ping"]
---

Il momento più alto della <a href="http://events.apple.com.edgesuite.net/0903lajkszg/event/index.html" target="_blank">presentazione di iPhone OS 3.0 a Cupertino</a>: il duetto di trombone eseguito su iPhone da due programmatori di Smule. Comincia a 1:00:57.

Scherzi a parte, presentazione da guardare assolutamente. Miti spazzati via (non è vero che le applicazioni vengono approvate lentamente), funzioni da levare il fiato (il copia e incolla è veramente l'ultima), prospettive di programmazione più che mai allettanti per gli sviluppatori.

Un anno dietro App Store, tutti stanno cercando di fare il proprio App Store. Ho l'impressione che con iPhone OS 3.0 il vantaggio sia di due anni o forse più. Chi ha L'acquisto da dentro l'applicazione, la ricerca dentro i contenuti del telefono, la retrocompatibilità con i modelli precedenti?

Ho anche capito perché <a href="http://itunes.apple.com/WebObjects/MZStore.woa/wa/viewSoftware?id=299813413&amp;mt=8" target="_blank">Lux Dlx</a> non permette di giocare a Risk tra più iPhone, perché potrebbe permetterlo dalla prossima estate e perché mi sa che mi scapperà un acquisto in più su App Store.