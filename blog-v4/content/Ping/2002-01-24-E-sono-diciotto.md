---
title: "E sono diciotto"
date: 2002-01-24
draft: false
tags: ["ping"]
---

Il computer più bello e facile da usare è diventato maggiorenne

Il 24 gennaio di diciotto anni fa è nato Macintosh, il computer che avrebbe mostrato il perché il 1984 (l’anno) non sarebbe stato come 1984 (il libro).
Ancora oggi Mac è l’alternativa di gran lunga più praticabile (e superiore) agli ordigni obsoleti, tozzi e raffazzonati che il Grande Fratello informatico propina a quelli troppo ignoranti per riuscire a pensare anche un solo secondo a qualcosa che non sia il prezzo.
Ma la cosa che dà più soddisfazione è pensare che, a cercare bene, secondo me qualcuno di quegli incredibili Mac 128K (beige!) dotati del rivoluzionario floppy da 3.5” si trova ancora in uso.
Gli ordigni di quell’epoca sono nella spazzatura da tempo, invece.

lux@mac.com