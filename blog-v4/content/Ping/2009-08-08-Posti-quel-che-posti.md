---
title: "Posti quel che posti"
date: 2009-08-08
draft: false
tags: ["ping"]
---

Andando per giochi di ruolo via <i>browser</i> e, <a href="http://en.wikipedia.org/wiki/List_of_multiplayer_browser_games" target="_blank">pagina di Wikipedia</a> a parte, mi sono imbattuto in <a href="http://www.nodiatis.com" target="_blank">Nodiatis</a>.

Il gioco è basato su Java (difetti: niente iPhone! Pregi: grafica e niente attese di caricamento pagine).

Parte un po' lentamente e l'erogazione via Java non è velocissima, ma alle prime battute sembra impegnativo e interessante.

Più di tutto, però, non richiede un indirizzo email. La procedura classica dammi-la-mail-che-ti-spedisco-il-codice appare sempre più stucchevole e finalmente qualcuno ci ha pensato.

Per giocare anche su iPhone, sto dando un'occhiata a <a href="http://www.scrollwars.com" target="_blank">Scroll Wars</a>. Ovviamente, essendo Html puro, la grafica non dà grandi soddisfazioni. L'interfaccia tuttavia si presta meglio di altre allo schermo <i>touch</i>.