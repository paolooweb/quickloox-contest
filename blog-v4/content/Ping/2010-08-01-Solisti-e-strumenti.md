---
title: "Solisti e strumenti"
date: 2010-08-01
draft: false
tags: ["ping"]
---

A completamento delle discussioni nate rispetto al caso del fotografo che apprezza iPhone, suggerisco di dare un'occhiata a questo <a href="http://www.peterbelanger.com/posts/38-macworld-cover-time-lapse-on-youtube" target="_blank">video accelerato</a>.

Mostra la nascita di una copertina di Macworld Usa, dallo studio fotografico fino alla copia stampata. La copertina è stata realizzata da Peter Belanger, fotografo abituale di Macworld, e riguarda tra l'altro iPhone 3GS.

Anche il non addentro alla fotografia professionale converrà che realizzare uno scatto di copertina non è cosa da poco e richiede persone, risorse, strumenti, competenza, capacità.

Adesso è ora di guardare le foto di <i>backstage</i> del lavoro compiuto da Belanger per realizzare un'altra copertina, <a href="http://www.peterbelanger.com/posts/73-macworld-iphone-4-cover" target="_blank">dedicata a iPhone 4</a>.

La particolarità del lavoro, scrive Belanger, è che normalmente per una foto di copertina lui usa una Phase One P65+, fotocamera digitale da sessanta megapixel. Ma questa volta il risultato doveva essere raggiunto scattando con (un altro) iPhone 4, che a parte qualità dell'obiettivo e altre considerazioni, con i suoi cinque megapixel, crea immagini grandi la dodicesima parte.

Non solo: la postproduzione è avvenuta esclusivamente su iPhone 4, con <i>app</i> disponibili per iPhone (PhotoForge e Resize-Photo). Belanger racconta che avrebbe desiderato avere a disposizione una terza <i>app</i>, Camera+ 1.2, non ancora uscita al momento della foto.

Per giudicare il risultato bisognerebbe avere in mano la copertina stampata. Possiamo però giudicare il lavoro, sicuramente professionale e generabile da un bravissimo solista che solo abbia in mano uno strumento adeguato.

<a href="http://www.macworld.com/article/153025/2010/07/iphone_cover.html" target="_blank">Commenta</a> la redazione di Macworld Usa: <cite>iPhone 4 è pronto per un uso intensivo come strumento per la realizzazione di periodici? Probabilmente no. Ma la sua fotocamera è di qualità rimarchevolmente alta. Non avremmo mai detto la stessa cosa della fotocamera dell'iPhone originale, ma nei tre anni intercorsi Apple e iPhone hanno fatto molto strada.</cite>

Si intende che è possibile dubitare delle foto e del filmato qui linkato. Come bisognerebbe fare, d'altronde, nei confronti di tutta la fuffa certamente non professionale di quanti hanno scritto, filmato e favoleggiato sul <i>death grip</i>. Tutto preso subito come autentico, decisivo, definitivo e dimostrato senza appello.