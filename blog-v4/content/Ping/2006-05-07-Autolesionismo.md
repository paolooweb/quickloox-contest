---
title: "Autolesionismo"
date: 2006-05-07
draft: false
tags: ["ping"]
---

Colta in un Apple Center:

<cite>Va bene anche se non &egrave; Word, basta che sia compatibile al 100 percento con Word.</cite>

Potrebbe essere un programma infinitamente migliore ma, se fosse compatibile al 99,999 percento, non andrebbe bene.

Poi uno si chiede perch&eacute; Apple non fa Numbers o perch&eacute; Pages non fa concorrenza diretta a Word.

Chi spende soldi per produrre un buon prodotto quando la gente pretende quello cattivo?