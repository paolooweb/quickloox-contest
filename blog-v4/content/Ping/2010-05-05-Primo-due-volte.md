---
title: "Due volte primo"
date: 2010-05-05
draft: false
tags: ["ping"]
---

Complimentissimi a Enrico Colombini per l'uscita di <a href="http://www.quintadicopertina.com/index.php?option=com_content&amp;view=article&amp;catid=38%3Alocusta-temporis&amp;id=61%3Alocusta-temporis&amp;Itemid=63" target="_blank">Locusta Temporis</a>, <i>ebook</i> interattivo che riprende la tradizione di Avventura nel Castello ripartendo dalle nuove tecnologie di lettura.

Locusta Temporis è anche in formato ePub e quindi funziona su iPhone e su iPad (anche su Mac). Costa niente e sarà assolutamente divertente. Non l'ho ancora preso; prima finisco di scrivere il <i>post</i>. :-)

Non per niente l'editore, Quintadicopertina, ha a che vedere in modo importante con <a href="http://www.venerandi.com/index.html" target="_blank">Fabrizio Venerandi</a>, che ha lungamente scritto su Macworld e che è persona di intelligenza e sensibilità rara (oltre che coautore del nuovo <a href="http://www.neonecronomicon.it/" target="_blank">Neonecronomicon</a>).

<a href="http://www.erix.it/retro/storia_cast.html" target="_blank">Avventura nel Castello</a> fu il primo grande gioco di avventura italiano, mentre nascevano i personal computer. Oggi inizia a nascere la lettura elettronica ed <a href="http://www.erix.it/" target="_blank">Enrico</a> è ancora una volta pioniere. Doppio merito e in bocca al lupo!