---
title: "Espressioni di settembre"
date: 2007-09-21
draft: false
tags: ["ping"]
---

Un elenco di numeri tutte tipo <em>$5.9</em>, dove il <em>5</em> potrebbe anche essere un <em>18</em>, o un <em>345</em>, ossia avere un numero di cifre arbitrario.

Il problema è togliere il segno del dollaro e mettere la virgola decimale al posto del punto. In un solo passaggio.

Con BBEdit ho felicemente coniato l'espressione regolare <code>\$([1-9])+\.</code> che ho sostituito con <code>\1,</code> a soluzione del problema.

Sono stupidaggini, ma per un breve istante orgogliosissimo sento che quasi potrei pure studiare da programmatore. :-)