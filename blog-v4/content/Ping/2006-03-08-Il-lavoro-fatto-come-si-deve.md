---
title: "Il lavoro fatto come si deve"
date: 2006-03-08
draft: false
tags: ["ping"]
---

Sembra incredibile, ma i confezionatori di spazzatura Mac quotidiana non hanno ancora prodotto un titolo a sensazione sulla <a href="http://www.zdnet.com.au/news/security/soa/Mac_OS_X_hacked_in_less_than_30_minutes/0,2000061744,39241748,00.htm" target="_blank">storia di un cretino</a> che ha aperto il suo Mac su Internet invitando la gente ad hackerarlo, cosa che &egrave; stata fatta prontamente (una cosa tipo <em>vediamo chi riesce a rubarmi i soldi dal portafogli che ho appena appena lasciato su una panchina del parco</em>).

Presso l&rsquo;Universit&agrave; del Wisconsin hanno attrezzato una <a href="http://test.doit.wisc.edu/" target="_blank">sfida pi&ugrave; seria</a>, e per questo motivo credo che se ne parler&agrave; molto meno.