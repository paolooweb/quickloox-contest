---
title: "Prima della partita"
date: 2006-07-09
draft: false
tags: ["ping"]
---

Avevo aperto una quantità di roba imbarazzante, compreso Classic, un centinaio di pagine web come minimo, una decina di programmi X11 tra la quarantina di quelli aperti e ho lanciato un installer, con World of Warcraft in sottofondo, la chat in funzione, la posta in recupero, un ftp monumentale in progresso e varie altre cose.

L'interfaccia utente si è piantata completamente e ho dovuto riavviare. Quinto riavvio in centoottantanove giorni calcolati da <a href="http://www.iuni.com/en/products/software/mac/iutime/index.html" target="_blank">iuTime</a>, o un riavvio ogni trentasette giorni e tre quarti.

Ma il riavvio dopo la partita. :-))