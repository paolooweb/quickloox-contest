---
title: "Pensare differente: i font"
date: 2006-05-29
draft: false
tags: ["ping"]
---

Devi pubblicare una Bibbia ma l&rsquo;editore si preoccupa del numero delle pagine.

Il designer pigro comprime il solito font.

Think Different &egrave; <a href="http://www.briansooyco.com/Featured.asp?ID=61" target="_blank">creare un font apposta</a>, che occupi poco spazio e abbia se possibile <a href="http://www.esv.org/blog/2006/01/outreach.edition.font" target="_blank">migliore leggibilit&agrave;</a>.

