---
title: "Non basta il software libero"
date: 2010-02-19
draft: false
tags: ["ping"]
---

Gianugo Rabellino, amministratore delegato di Sourcesense, ha concesso un'intervista a Linux Magazine di marzo. Leggo <a href="http://edmaster.it/index.php?p=prodotti&amp;sez=magazine&amp;cat=3&amp;prod=5" target="_blank">dal loro sito</a>:

<i>INTERVISTA ESCLUSIVA AL CEO DI SOURCESENSE</i>
<i>È possibile fare business investendo nell'</i>open source<i>? La testimonianza e i consigli di chi c'è già riuscito</i>

Interessante, no? Il software libero è un'arma formidabile e <a href="http://www.sourcesense.com/it/home" target="_blank">Sourcesense</a> ha raggiunto risultati ragguardevoli a livello internazionale.

Gianugo è un amico e leggo regolarmente <a href="http://boldlyopen.com/2010/02/18/good-news-bad-news/" target="_blank">il suo blog</a>. Essendo scritto in inglese, ne traduco la parte fondamentale relativa all'intervista. Non ho sottomano la rivista e quindi il risultato sarà leggermente differente, ma la sostanza è quella.

Chiedono a Gianugo per email <cite>da quanto tempo usi Linux</cite> e lui risponde <cite>Dopo la mia prima Slackware sono passato a Debian (devo ammettere di avere avuto rapporti occasionali con Ubuntu) e non sono più tornato indietro. Sebbene mi spezzi il cuore ammettere che il mio computer è di fatto un Mac: la vita è troppo breve per stare a configurare un connettore Bluetooth o armeggiare con un videoproiettore.</cite>

Poi gli chiedono sempre per posta se <cite>Linux sia pronto come sistema operativo desktop per le masse</cite> e lui risponde <cite>Uso un Mac e guai a chi me lo toglie. Sono stato sempre scettico riguardo a Linux come sistema operativo generico per il desktop.</cite>

E adesso, riferisce Gianugo, ecco ciò che appare sulla rivista:

<cite>Dopo la mia prima Slackware sono passato a Debian (devo ammettere di avere avuto rapporti occasionali con Ubuntu) e non sono più tornato indietro</cite>.

<cite>Ho sempre creduto fortemente in Gnu/Linux come sistema operativo server, ma penso che funzioni bene anche come sistema operativo desktop generico.</cite>

Ci fossero di mezzo foto, si potrebbe dire che si è un po' esagerato con Photoshop.

Il software libero è importante (e Mac OS X, amici di Linux Magazine, ha un cuore <i>open source</i>). Non è l'unica libertà a contare, però.