---
title: "Iperabuso di monopolio"
date: 2001-12-03
draft: false
tags: ["ping"]
---

Il processo antitrust contro Microsoft prosegue stancamente nei suoi esiti postgiudiziari, con i giudici che continuano a non capire come stanno effettivamente le cose e Microsoft che se ne approfitta.
L’ultima è recente: come compensazione a una serie di iniziative anticompetitive e di abuso del monopolio di cui l’azienda di Gates, Ballmer e compagni è stata giudicata colpevole, la proposta di Redmond è stata di regalare una quantità spaventevole di software alle scuole americane.
Peccato che quello della didattica, in Usa, sia uno dei pochissimi settori in cui esiste ancora una sana concorrenza, tant’è vero che Apple ne detiene una quota rilevante e per esempio è impegnata in una fiera lotta con Dell per conquistare questa o quella high school.
Così, per espiare le sue colpe, Microsoft finirebbe per dominare completamente, gratis, un mercato in più. Un iperabuso in conseguenza dell’abuso.
Certo, non cambierebbe la situazione nell’hardware. Ma una copia di Office su Mac è colpevole di abuso del monopolio come una copia di Office su Dell.
