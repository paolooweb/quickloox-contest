---
title: "Sempre pi&ugrave; aperti"
date: 2006-06-20
draft: false
tags: ["ping"]
---

Il museo Apple di Quiliano adesso &egrave; aperto anche <a href="http://www.allaboutapple.com/present/sede.htm" target="_blank">durante la settimana</a>. E&rsquo; solo un inizio, ma vuol dire che funziona. E che una visita &egrave; ancora pi&ugrave; possibile.