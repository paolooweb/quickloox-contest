---
title: "Due graffette e tutto va a posto"
date: 2008-12-14
draft: false
tags: ["ping"]
---

Stabilito che il Finder è più comodo e pratico, non è detto che in casi limite un pizzico di Terminale aiuti.

Mi ritrovo con una cartella di circa duemila documenti, un numero imprecisato dei quali inizia con NG oppure con CF. E devo lavorare solo su quelli, ignorando gli altri, a patto che siano documenti Pdf e non altro. E non so neanche se siano pochi o molti.

Con il Finder devo iniziare ricerche abbbastanza avventurose. Con il Terminale, posto di trovarmi nella directory appropriata, digito

<code>ls {CF,NG}*.pdf</code>

e li elenco tutti e solo quelli. Con <code>cp</code> li copierei, con <code>mv</code> li sposterei, con <code>rm</code> li cancellerei eccetera. Nel Finder avrei solo iniziato, nel Terminale ho già finito. Facendo

<code>ls {CF,NG}*.pdf | less</code>

li vedrei anche una schermata per volta.

Tutto ciò è banalotto. Tuttavia non avevo mai visto circolare esempi di Terminale che mettessero a frutto le parentesi graffe.