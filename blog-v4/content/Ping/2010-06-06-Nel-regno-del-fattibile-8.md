---
title: "Nel regno del fattibile / 8"
date: 2010-06-06
draft: false
tags: ["ping"]
---

<cite>pregasi elencare azioni fattibili su un iPad e non fattibili su un Sony Vaio W da 10&#8221;</cite>.

Il turismo interattivo.

Mi spiego meglio. Do per scontato l'orientamento stradale fornito dalle mappe di Google, ma poi viene tutto il resto. Mentre si viaggia, un navigatore può consultare la tavoletta per ottenere siti, informazioni volanti, effettuare magari una chiamata per sapere se quel certo museo è aperto e scoprire in tempo reale tutta una serie di cose interessanti e utili a una gita. Fino a qui usare alternativamente un netbook certo è fattibile, per quanto un po' più scomodo.

Poi però si entra magari nel museo, o si ascolta una guida. Viene voglia di prendere appunti, segnare un dettaglio, verificare una data su Internet, il più delle volte in piedi tra un documento e l'altro, tra una scultura e quella dopo. iPad è perfettamente praticabile, mentre un <i>netbook</i> risulterebbe goffo e imbarazzante, se non altro per la necessità di reggerlo in orizzontale.

L'altra faccia della medaglia è che, terminata la visita guidata, poi bisogna concederne una di iPad a tutti i curiosi. Solo questione di tempo, in breve usarne uno al museo diventerà una faccenda normale.