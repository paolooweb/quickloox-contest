---
title: "Gli uomini di buona volontà"
date: 2007-12-19
draft: false
tags: ["ping"]
---

Ho provato ad aggiornare <a href="http://www.finkproject.org/" target="_blank">Fink</a> per Leopard come spiegato nella pagina del progetto. Non uso molto Unix puro, ma qualcosa s&#236; (compreso Common Lisp, naturalmente). La versione che viene installata con l'aggiornamento è più vecchia di quella descritta, ma le cose sembrano funzionare.

Durante l'aggiornamento appaiono anche messaggi in italiano, come <code>Spacchetto il rimpiazzo di fink</code>. Evidentemente c'è anche da noi qualcuno che mette un po' di buona volontà, non solo natalizia, sopra l'<em>open source</em>.