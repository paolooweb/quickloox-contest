---
title: "Appunti di viaggio"
date: 2010-06-10
draft: false
tags: ["ping"]
---

Un MacBook Pro 17&#8221; è compatibile con il tavolino in dotazione a un posto Eurostar di seconda classe ed è possibile lavorare senza sconfinare negli spazi degli altri passeggeri.

Ora che le batterie del MacBook Pro suddetto consentono di lavorare in autonomia da Ancona a Milano e quasi ritorno, i treni dispongono della presa elettrica su tutti i posti. Quando le batterie duravano niente, per avere una presa bisognava sperare nella prima classe e mica sempre.

Il MacBook Pro piazzato sul tavolino a proiettare <i>full screen</i> il <i>keynote</i> di Steve Jobs in occasione della Wwdc 2010 fa davvero effetto Nuovo Cinema Trenitalia, relax pazzesco e bastano gli auricolari per rispettare chiunque sieda intorno.

Sono pronto a scommettere che avremo la connessione wi-fi nelle carrozze il giorno dopo che tutti i computer del mondo potranno automaticamente farne a meno.

Il sito Trenitalia è stato concepito da persone progettualmente svantaggiate. Tuttavia in qualche modo funziona e avere come biglietto un Sms è dimostrazione del potere liberante della tecnologia usata con saggezza.

L'accensione di un iPad dentro una carrozza ferroviaria applica una distorsione gravitazionale che esercita una forza di attrazione variante con il quadrato della distanza dai passeggeri circostanti.