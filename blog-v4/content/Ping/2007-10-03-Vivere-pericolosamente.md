---
title: "Vivere pericolosamente"
date: 2007-10-03
draft: false
tags: ["ping"]
---

Ho installato <a href="http://porting.openoffice.org/mac/download/aqua-PPC.html" target="_blank">OpenOffice Aqua</a>, la versione ATTENTO-NON-USARE-QUESTO-SOFTWARE-PER-LAVORO-VERO-IN-UN-AMBIENTE-DI-PRODUZIONE QUESTA-È-UNA-VERSIONE-ALPHA.

Non apre i file con il drag and drop, ci vuole per forza il comando Open. A parte questo, sembra tutto a posto.

È una tentazione. Non riesco più a resistere.

No, non ho cancellato la versione X11.