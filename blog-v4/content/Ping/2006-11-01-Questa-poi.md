---
title: "Questa poi"
date: 2006-11-01
draft: false
tags: ["ping"]
---

Che fosse possibile <a href="http://www.macworld.com/weblogs/macosxhints/2006/10/previewscript/" target="_blank">aggiungere il supporto elementare di AppleScript</a> ai programmi che non ce l'hanno è una cosa che non avrei mai immaginato.

In qualche occasione, avere a disposizione anche solo la Standard Suite dei comandi salva la ghirba.