---
title: "Buon compleanno Sophia<p>"
date: 2004-11-12
draft: false
tags: ["ping"]
---

Se un anno di un cane ne vale sette umani, come sono messi i computer?<p>

<a href="http://profmorittu.altervista.org">Piergiovanni</a> ha informato gli amici del felice quarto compleanno di Sophia e mi ha dato il permesso di informarne anche la comunità di Ping.<p>

Sophia è un computer, per la precisione un iMac, che lo scorso undici novembre ha per l&rsquo;appunto spento la quarta candelina.<p>

iMac doveva essere, nella testa degli scettici a oltranza, un computer per poveri di spirito, inutilmente colorato, con una forma non consona a un oggetto triste per scrivanie tristi di gente triste, addirittura senza floppy, in spregio alle abitudini di gente troppo vecchia dentro per capire che le abitudini sono fatte per essere cambiate quando sono stantie.<p>

Invece non solo è un computer vero, non solo i floppy sono morti, ma dura persino. Al punto che ci si affeziona.<p>

Complimenti a Piergiovanni (chi vuole unirsi a me gli può anche <a href="mailto:profmorittu@yahoo.it">scrivere</a>) e cento di questi giorni per Sophia.<p>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>