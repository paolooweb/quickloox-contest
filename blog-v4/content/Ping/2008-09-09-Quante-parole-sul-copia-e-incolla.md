---
title: "Quante parole sul copia e incolla"
date: 2008-09-09
draft: false
tags: ["ping"]
---

Qualcuno si è reso conto che quindici anni fa si faceva tranquillamente <a href="http://www.youtube.com/watch?v=Sue2BR1AHUE" target="_blank">copia e incolla su Newton</a> (usando l'eccezionale <a href="http://code.google.com/p/einstein/" target="_blank">progetto Einstein</a>).

Non si è reso conto, come è stato fatto notare, che per attivare su iPod touch il copia e incolla come funzionava su Newton bisogna <a href="http://www.boingboing.net/2008/09/07/apple-solved-touchsc.html#comment-277305" target="_blank">ridisegnare una parte significativa dell'interfaccia</a>.

Qualcun altro vorrebbe creare il copia e incolla premendo nientemeno che <a href="http://www.ironicsans.com/2008/08/idea_how_i_would_do_iphone_cut.html" target="_blank">due pulsanti</a> posti in parti diverse dello schermo.

Sembra che per implementare e raffinare una interfaccia utente basti farsi venire una buona idea, come per allenare la Nazionale di calcio e governare l'Italia.

Tutti a produrre opinioni in libertà sul <em>copia e incolla</em>. Nessuno, a quanto pare, che sappia fare un onesto <em>taglia</em> sulle proprie elucubrazioni.