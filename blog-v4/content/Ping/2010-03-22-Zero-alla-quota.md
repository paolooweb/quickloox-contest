---
title: "Zero alla quota"
date: 2010-03-22
draft: false
tags: ["ping"]
---

Il concetto di quota di mercato è stato sempre antipatico e, ancora una volta, di misteriosa importanza nel mondo dei computer mentre nel mondo normale non gliene frega niente a nessuno. Anzi, è evidente che l'umano sia tirato per la giacchetta alla ricerca da una parte della sicurezza che viene dal fare come gli altri e contemporaneamente dal desiderio di essere in qualche misura unico.

L'andamento di quota di mercato Apple è lusinghiero: rispetto a cinque anni fa, in Europa è circa triplicata, negli Stati Uniti è più che raddoppiata e nei Paesi emergenti inizia a fare capolino. Siccome più una nazione è sottosviluppata e meno Mac si vendono, si capisce che è un buon segnale anche complessivo.

Eppure è ora di dire basta e cambiare metodo.

Il motivo è semplice: che ci fosse differenza di prezzo o meno, ogni Mac si confrontava più o meno con una concorrenza equivalente. Portatili contro portatili, integrati contro integrati, <i>tower</i> contro <i>tower</i>.

Ora però sono arrivati i <i>netbook</i>. Un <i>netbook</i> non è paragonabile nelle prestazioni neanche al più misero dei Macbook, cos&#236; come il prezzo non è paragonabile. Sono proprio oggetti diversi.

Ma nei calcoli di quota di mercato un MacBook da novecento euro vale come un <i>netbook</i> da trecento. Si conteranno gli iPad come unità Mac vendute? Ne dubito molto.

Questo tipo di critica ai dati di venduto è già emerso in tutt'altro ambito: App Store. A un certo punto si è visto che le applicazioni a prezzo irrisorio, ovviamente vendutissime, sovrastavano nelle classifiche le applicazioni più costose, che magari lo sono perché hanno richiesto più lavorazione, sono più complesse e pure più utili. Ma il frequentatore casuale di App Store non le vedeva mai. Per questo App Store oggi mostra non solo le applicazioni più richieste, ma anche le ultime uscite e soprattutto le più redditizie, in modo da dare visibilità anche a programmi che per loro natura vengono venduti in numeri minori.

Per questa ragione calcolerei gli andamenti dell'industria in un altro modo, rapportando le unità vendute al prezzo con cui vengono vendute. Come prima banale modifica dividerei le unità vendute per il prezzo medio di vendita, ricavandone una sorta di &#8220;atomo di vendita&#8221;. Per Apple è evidente che vendere un MacBook Pro da duemila euro vale il doppio che vendere un MacBook da mille e quindi due Macbook da mille dovrebbero valore come un MacBook Pro da duemila anche nei conteggi.

Cos&#236; è ancora troppo semplice, perché la propensione all'acquisto sta su una curva, non su una retta; più il prezzo sale più è difficile vendere, per cui un oggetto da tremila dovrebbe contare più tre oggetti da mille e molto più di dieci oggetti da trecento.

Chissà che qualche esperto non ci si voglia mettere.