---
title: "Niente di ignorabile"
date: 2002-02-24
draft: false
tags: ["ping"]
---

Apple compra una piccola azienda sconosciuta ai più, ma la mossa non potrebbe essere sottovalutata di meno

Nothing Real: un rapido test al bar, ai giardinetti e in ufficio o a scuola dmostrerà che questo nome non dice niente a quasi nessuno.
I pochi che sanno diranno sveltamente che si tratta di una piccola azienda californiana produttrice del programma leader nel video compositing 2D: Shake (il video compositing è una cosa che si fa solo per lavoro e fanno davvero in pochi). L’azienda è piccola, il programma sconosciuto; ma Shake è stato usato, tanto per dire, nel compositing del Signore degli Anelli.
Apple ha comprato Shake pochi giorni fa e può significare una cosa sola: vuole fare qualcosa di interessante nel mercato dell’editing video ad altissimo livello (e costo).
Mai come in questa occasione si può dire che, almeno stando alle premesse, ne vedremo delle belle.

<link>Lucio Bragagnolo</link>lux@mac.com