---
title: "Incapacità infinita"
date: 2002-03-17
draft: false
tags: ["ping"]
---

Internet permette proprio a tutti di esprimersi. Ma tutti-tutti-tutti

Anche con Mac si possono usare i servizi di spazio Web, come iDisk.
Ho recentemente ricevuto da uno di essi una mail di cui riporto cenni:

[...] siamo nuovamente online!
Abbiamo già reso disponibile il servizio di spazio web e il servizio di ftp, per permetterti di caricare nuovamente tutto il materiale.
Purtroppo il contenuto del tuo sito web è andato perduto, ma potrai utilizzare il tuo indirizzo web di sempre, la tua username e la tua password.
[...]
Ci rendiamo conto che l'incidente ti ha causato fastidiosi disagi e vogliamo rassicurarti:
abbiamo trasferito tutte le nostre macchine presso una primaria server house e potenziato la tecnologia per offrirti un servizio ancora più veloce e affidabile,sempre con SPAZIO INFINITO a disposizione per te.”

Mi piacerebbe proprio sapere che macchine (e che sistema operativo) usavano prima. E vedere le loro facce.
Di infinito c’è solo l’incapacità tecnica. Forse non del tutto loro; ma è evidente che Internet offre spazio proprio a tutti. Infinito.

<link>Lucio Bragagnolo</link>lux@mac.com