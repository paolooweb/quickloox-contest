---
title: "Godimento comune"
date: 2010-01-28
draft: false
tags: ["ping"]
---

Parlo solo per me e dico che ho gradito estremamente la nostra rimpatriata in chat Irc di ieri sera.

Siamo stati sempre tra venti e trenta, le notizie da Internet arrivavano a frammenti perché la banda di chi trasmetteva non bastava veramente mai, ma fra tutti c'era sempre qualcuno con una ricezione funzionante e cos&#236; abbiamo potuto commentare tutti gli accadimenti in tempo reale.

Divertente, istruttivo, allegro: mi piace. Grazie di cuore.

Oggi inizia il valzer di quelli che io lo avevo detto. Io penso all'amico Fab che ha azzeccato il tre al Superenalotto e ha vinto cinquantaquattro euro. Senza leggere siti!

Ha fatto lui meglio di tutti gli altri.