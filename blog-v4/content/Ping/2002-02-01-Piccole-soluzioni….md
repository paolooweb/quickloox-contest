---
title: "Piccole soluzioni sistematiche"
date: 2002-02-01
draft: false
tags: ["ping"]
---

Una delle novità più simpatiche di Mac OS X è talmente sottile che nessuno ne parla

Nei miei deliri occasionali riguardanti Apple mi è capitato di chiedere ai rappresentanti dell’azienda di creare una versione Golden di Mac OS, che rimediasse a mille piccole sciocchezze perennemente trascurate, aggiornamento dopo aggiornamento, a favore delle solite Nuove Funzioni.
Un esempio, ormai vecchio: mi sarebbe piaciuto vedere Mac OS 8 trattare la copia fra due floppy disk sfruttando tutta la memoria presente nel sistema e quindi minimizzando il metti-e-togli dei dischi nel lettore. Insomma, piccole cose che però dessero un’idea di “sì, abbiamo pensato veramente a tutto”.
Mac OS X è ancora in piena lavorazione, ma una cosa piccola eppure importante c’è: la finestra di dailogo di apertura file, oltre che non bloccare il lavoro con gli altri programmi, permette di selezionare più documenti. Non ne parla nessuno ma a volte è una bella comodità. Uhm. Conoscendo la stampa specializzata, ci sarà forse un legame?

lux@mac.com