---
title: "C'è qualcuno alle tue spalle"
date: 2008-04-24
draft: false
tags: ["ping"]
---

Qualcuno ha salutato con favore l'annuncio per Mac della raccolta dei <a href="http://www.ascendercorp.com/pr/pr2008_04_17.html" target="_blank">font ClearType contenuti in Office 2008 per Mac</a>, per 299 dollari e con licenza per cinque utenti.

Io non frequento&#8230; ma mi pare di ricordare che una edizione Home o Student di Office 2008 per Mac costi meno e abbia la licenza per tre utenti. O al massimo si pone la questione del cambio, ma costerebbe meno.

In pratica, se hai bisogno dei font per avere la compatibilità grafica con Office, ti costano più che avere Office.

Sarà un caso, ma se arriva qualcosa da dietro, e non preciso ulteriormente, c'è sempre di mezzo Microsoft.