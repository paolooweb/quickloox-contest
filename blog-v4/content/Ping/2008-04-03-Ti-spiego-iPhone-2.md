---
title: "Ti spiego iPhone/2"
date: 2008-04-03
draft: false
tags: ["ping"]
---

Tornato dall'appuntamento.

Per la prima volta da molti anni ero senza portatile.

Ho preso gli appunti sulle note di iPhone (poteva tranquillamente essere un iPod touch).

Con la tastiera virtuale, velocità più che sufficiente con i due indici. Qualche refuso inevitabile, corretto più tardi con comodo a riunione finita.

La riunione ha avuto un inevitabile strascico. Tutti i partecipanti hanno voluto vedere meglio iPhone e constatare che avevo veramente preso gli appunti.

Mi sa che se ne vende qualcuno in più.