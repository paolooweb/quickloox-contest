---
title: "Che noia la paranoia"
date: 2008-08-21
draft: false
tags: ["ping"]
---

Esclusi i presenti, constato che più una persona si ossessiona per la sicurezza e meno ne ha bisogno (il sottoscritto meno di tutti; quando mi è stato obiettato che Google poteva sapere tutto quello che passava dal mio indirizzo Gmail, ho risposto che quelli di Google non sono certamente messi cos&#236; male).

Adesso ho trovato l'ammazzadialogo. Quando parte con la paranoia chiedo <cite>tu hai attivato la Secure Entry Keyboard sul Terminale, vero?</cite>

Mi guarda come se gli avessi servito il cane di casa fatto allo spiedo con la mela in bocca.

<cite>S&#236;</cite>, spiego, <cite>il Terminale ha il comando per fare s&#236; che niente di quello che viene digitato nelle sue finestre sia intercettabile da altri processi all'interno del computer. Cos&#236; eviti i</cite> keylogger<cite>, i programmi che registrano l'input di tastiera a tua insaputa. E siccome dal Terminale puoi fare veramente un sacco di cose, compreso il fare transitare la posta elettronica delicata, cifrare file e quant'altro, attivare la tastiera sicura aumenta clamorosamente la sicurezza intrinseca del sistema.</cite>

Qui solitamente casca l'asino. C'è gente che ha talmente poco da fare che sceglie di diventare paranoica con la sicurezza per sentirsi importante, ma nessuno di questi ha voglia di tradurre in realtà le proprie fantasie.

E dire che il comando è l&#236; pronto nel menu Terminale, subito sotto le Preferenze.