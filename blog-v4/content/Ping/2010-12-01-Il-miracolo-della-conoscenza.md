---
title: "Il miracolo della conoscenza"
date: 2010-12-01
draft: false
tags: ["ping"]
---

Mi è successo davvero. Dovevo capire che font fosse stato usato per una certa scritta. L'ho inviato a <a href="http://new.myfonts.com/" target="_blank">MyFonts</a> che lo ha riconosciuto con precisione, in una manciata di varianti assai vicine tra loro. Comprensibile, visto l'esempio che avevo a disposizione.

Nel frattempo è arrivata la risposta dell'autore della scritta, che avevo interpellato, a confermare il verdetto.

Si dirà che è un traguardo normale del riconoscimento ottico dei caratteri e che lo si usa da anni, con tanto di <a href="http://itunes.apple.com/it/app/whatthefont/id304304134?mt=8" target="_blank"><i>app</i> su iOS</a>. Sgrano gli occhi ugualmente.