---
title: "(non abbastanza) caro hub ti scrivo&hellip;<p>"
date: 2005-02-12
draft: false
tags: ["ping"]
---

Usb è uno standard ma alcuni costruttori sono più uguali degli altri<p>

È abitudine comune nel mondo Wintel cercare di offrire sempre qualcosa oltre lo standard, a costo di causare problemi. Così Explorer ha introdotto propri tag Html per distruggere i browser avversari, per esempio, oppure si è inventato l&rsquo;overburning per masterizzare dati sui dischi in quantità oltre la capacità teorica. Sistema perfetto per convincere i soliti furbi di stare risparmiando, ridurre l&rsquo;affidabilità dei dischi masterizzati e abbreviare la vita dei masterizzatori.<p>

Un altro esempio tipico è Usb. La specifica Usb viene rispettata da Apple alla lettera. Molti costruttori, invece, per ridurre i costi applicano tolleranze più alte del dovuto. Questo porta a un sacco di problemi stupidi, come attese abnormi all&rsquo;avvio del sistema per chi ha collegato al Mac un hub da due soldi (ma proprio due).<p>

Se un hub Usb costa pochissimo e non è di marca, sono discrete le probabilità che dia problemi. Augh, ho detto.<p>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>