---
title: "Tre giorni e sei crocette"
date: 2006-09-04
draft: false
tags: ["ping"]
---

Ho potuto accontentare solo parzialmente le richieste di inserimento software nel Cd-ROm di Macworld ottobre, ma sono soddisfatto del risultato.

Accontentare tutti era da subito un sogno impossibile, dato che avevo a disposizione tre giorni per preparare l'intero Cd. In compenso è stata approvata l'idea di dare spazio alle richieste dei lettori, che avrà quasi certamente ancora più spazio da qui in poi. Il <em>quasi</em> dipende solo dall'approvazione definitiva da parte della nuova direzione di Macworld.

Nello specifico, il più accontentato di tutti è stato <strong>Tommaso Fantini</strong>, i cui quattro suggerimenti sono entrati tranquillamente e ti credo: sapeva di preciso che cosa voleva e ha messo i link pronti da usare. Con settantadue ore a disposizione, questa strategia è vincente. Sono inoltre riuscito ad accontentare (spero) con qualcosina <strong>Piergiovanni</strong>, <strong>Carmelo</strong>, <strong>frix</strong>, <strong>iRaffa</strong> e <strong>mAx</strong>.

Per gli altri non sono riuscito a fare molto e me ne scuso, ma ho qualche giustificazione e sto anche, come si dice, <em>lavorando per voi</em>.

La struttura del Cd lascia relativamente poco spazio per software ingombrante e questo, se potrò, cambierà, ma deve essere fatto gradualmente e questo ha abbastanza influito. Per esempio, come già detto, per la beta di Neoffice Aqua. Da sola vuole più di 250 mega e non ci stava proprio. Quasi certamente la metterò appena non sarà più beta.

In parte vale anche per <a href="http://www.esm.psu.edu/mac-tex/" target="_blank">LaTeX</a>. Non vedo l'ora di inserire una raccolta come si deve, ma il materiale su Internet è tantissimo e occorreva una selezione più meditata di quella che avevo il tempo di fare. Ho iniziato a farla, però, ed è solo questione di tempo.

Per <a href="http://www.gnome.org/projects/gnumeric/" target="_blank">Gnumerics</a> mi sono scontrato con la difficoltà di installazione. Fink non risolve tutto in un unico passo e l'unica installazione per Mac OS X esistente è a pagamento. Ho trovato qualche indizio che forse permetterà di risolvere la questione, ma ci vuole ancora un po' di tempo.

Grazie a tutti, però! Lavorare cos&#236; è interessante. Se tutto è confermato, si rifà. Attendo le conferme necessarie, o farò comunque una chiamata al buio intorno a metà mese.

Adesso che c'è tempo di lavorarci con calma, vorrei cercare magari di mettere meno software, ma sempre in profondità (cioè presentandolo in tutte le versioni possibili anche per chi non ha per forza Mac OS X e Tiger) e privilegiando i programmi più difficili da scaricare, oltre a cercare di creare raccolte ragionate di plugin e risparmiare la fatica di dover cercare cose in giro per Internet. Il dibattito, naturalmente, è aperto. :-)