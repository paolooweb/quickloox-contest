---
title: "Sempre a una cifra"
date: 2007-03-16
draft: false
tags: ["ping"]
---

Aggiornato con pieno successo anche a Mac OS X 10.4.9 (più iPhoto, ma non avrebbe richiesto il riavvio).

Mi chiedo se Leopard non si stia avvicinando più rapidamente del previsto, considerando che non è mai uscito un Mac OS X 10.x.10. Interessante conflitto tra il dato di fatto e lo studio dei precedenti, che i vari lettori di fondi di caffè e lanci di moneta interpretano in vario modo.

Finora i più bravi sono quelli di <a href="http://switchtoamac.com/site/mac-os-x-leopard-105-a-may-2007-release.html" target="_blank">Switch To A Mac</a>.