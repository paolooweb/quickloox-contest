---
title: "Le azioni fantasma"
date: 2008-09-28
draft: false
tags: ["ping"]
---

<a href="http://multifinder.wordpress.com" target="_blank">Mario</a> ha deciso di affibbiarmi i compiti delle vacanze e scrive:

<cite>Nei commenti di un blog c’è un tizio che dice che non è mai riuscito a trovare documenti che dimostrino in modo inequivocabile il fatto che Microsoft abbia venduto le azioni Apple comprate nel 1997.</cite>

Mario si è imbattuto nel solito furbetto da poco. Quelli che ci mettono niente a fare dietrologia d’accatto, gratis; e pretendono di essere smentiti con grande spesa di tempo.

Il fatto è che ora sono in vacanza. Il tempo è bello e le notti sono corte, altrimenti sarei arrivato in fondo. Sono comunque a buon punto. Ed è questo.

Le aziende americane depositano ogni anno presso la Security Exchange Commission, circa la nostra Consob, un modulo che si chiama 10-K e che riassume l’anno finanziario dell’azienda. I moduli 10-K sono consultabili gratuitamente e con grande fatica, oppure più facilmente e a pagamento, sul servizio <em>online</em> Edgar. Una pagina di Boston.com porta a tutti i <a href="http://finance.boston.com/boston?Page=SEC&amp;Ticker=AAPL" target="_blank">moduli 10-K di Apple degli ultimi anni</a>.

La <a href="http://www.secinfo.com/dVut2.218kb.htm?Find=microsoft&amp;Line=11182#Line11182" target="_blank">riga 11.182 del modulo 10-K 2003</a> spiega che nel 1997 Microsoft investì in Apple i famosi 150 milioni di dollari, in cambio di 150 mila azioni <em>preferred</em>, che non si possono vendere. Le azioni <em>preferred</em> sarebbero diventate convertibili in <em>common stock</em>, azioni normalissime e vendibili, dopo il 5 agosto 2000, con un valore di 8,25 dollari per ciascuna azione <em>common</em>.

Tra il 6 agosto e il 31 dicembre 2000, documenta il modulo 10-K, Microsoft ha convertito in azioni comuni 74.250 azioni <em>preferred</em>, ottenendone in cambio nove milioni di azioni comuni.

Durante il 2001, sempre da 10-K, Microsoft ha convertito in azioni comuni 75.750 azioni preferred, ottenendone altri 9,2 milioni di azioni comuni.

Microsoft ha convertito dunque l’intera scorta di 150 mila azioni <em>preferred</em> e, se non le avesse vendute, ora sarebbe titolare di 18,2 milioni di azioni comuni Apple.

In realtà il doppio, perché nel 2005 <a href="http://phx.corporate-ir.net/phoenix.zhtml?c=107357&amp;p=irol-faq#split1" target="_blank">Apple ha fatto uno <em>split</em></a>: ogni azione è stata sdoppiata. Le azioni oggi in possesso di Microsoft dovrebbero essere quindi 36,4 milioni.

Da adesso diventa più semplice.

Yahoo Finance mantiene costantemente aggiornata la situazione dei <a href="http://finance.yahoo.com/q/mh?s=AAPL" target="_blank">Major Holders</a>, i detentori di più azioni Apple.

La classifica riporta solo i primi dieci investitori istituzionali; al primo posto mentre scrivo c’è Fmr Llc, con 39,4 milioni di azioni, pari al 4,45 percento dell’azienda. Al decimo posto c’è Northern Trust Corporation, con 11,5 milioni di azioni e l’1,29 percento dell’azienda.

Microsoft non appare tra i primi dieci. Se avesse in mano trentasei milioni di azioni Apple, invece, dovrebbe. Le azioni non spariscono né si nascondono; se non compaiono in mano a Microsoft, le ha in mano qualcun altro.

Non ho (ancora) prova evidente che Microsoft non si trovi più bassa in classifica. Tuttavia non figura tra i primi dieci investitori istituzionali e chiunque lo può verificare, o supporre che Yahoo Finance fornisca false informazioni ogni giorno a un pubblico di milioni di persone. Ammesso che Microsoft abbia conservato azioni Apple e si trovi oltre le prime dieci aziende azioniste, sono meno di 11,5 milioni, pari a meno dell'1,29 percento. Meno di un terzo dell’investimento iniziale.

Il furbetto può dire quello che vuole. Ma prima deve spiegare dove stanno venticinque milioni di azioni che mancano all’appello. Come minimo.

<strong>Aggiornamento:</strong> grazie a <strong>Marco</strong> ora abbiamo la lista dei <a href="http://investing.businessweek.com/research/stocks/ownership/institutional.asp?symbol=AAPL.O" target="_blank">primi venti investitori istituzionali in possesso di azioni Apple</a>. Black Rock Adivsors, ventesima in classifica, detiene 4,5 milioni di titoli, pari allo 0,51 percento.

Se Microsott ha ancora in tasca azioni Apple, sono meno di 4,5 milioni. L'azienda si sarebbe disfata come minimo di 31,9 milioni di azioni Apple sui 36,4 iniziali. Per avere il potere di condizionamento su Apple che indiscutibilmente conferisce un duecentesimo della proprietà.

La vita del furbetto si complica sempre più.