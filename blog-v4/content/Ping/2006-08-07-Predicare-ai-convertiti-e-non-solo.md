---
title: "Predicare ai convertiti (e non solo)"
date: 2006-08-07
draft: false
tags: ["ping"]
---

La difficoltà nel presentare una cosa come una versione nuova di Mac OS X agli sviluppatori è che bisogna mostrare contemporaneamente novità interessanti per gli sviluppatori e nuove attrattive per l'utente finale.