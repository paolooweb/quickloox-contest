---
title: "Grazie dei fior (di codice)"
date: 2007-10-17
draft: false
tags: ["ping"]
---

I programmatori di Karelia Software sono probabilmente tra i più geniali che operano su Mac. <a href="http://www.karelia.com/imedia/" target="_blank">iMedia Browser</a>, per esempio, persino gratuito e veramente meritevole.

E succedono varie altre cose. Per esempio, ApiMac ha inserito in <a href="http://www.apimac.com/compress_files/" target="_blank">Compress Files</a> 3.0 il supporto di Xar. Xar viene usato all'interno di Leopard per la compressione dei pacchetti di installazione. Ed è nato all'interno del progetto OpenDarwin, che deriva dalla disponibilità come open source del cuore di Mac OS X. iMedia Browser nasce dal <em>framework</em> di codice usato, per esempio, in iLife.

La disponibilità universale dei pezzi di software fondamentali di Mac OS X paga e ne beneficiamo tutti, perché programmatori di talento li mettono a disposizione della comunità senza dover reinventare la ruota.

È bellissimo quando non sai chi devi veramente ringraziare, in quanto ci hanno messo una mano importante tutti.