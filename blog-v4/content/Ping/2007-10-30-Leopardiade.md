---
title: "Leopardiade"
date: 2007-10-30
draft: false
tags: ["ping"]
---

Aggiornamento quasi notturno. Lavorare a pieno ritmo con il sistema è ben diverso dal testare una beta per scriverci un libro. In queste prime ore sono soddisfattissimo.

Durante la sincronizzazione dei contatti effettuata da .Mac è uscito Conflict Resolver, a segnalare alcune discrepanze tra nomi nella Rubrica Indirizzi e gli stessi nomi nel backup su .Mac. Tutto sistemato in modo semplicissimo e un bel servizio reso da Leopard.

Vienna ha un <em>bug</em> di visualizzazione sulle barre di scorrimento verticali, per il resto funziona.

Ho aggiunto alla lista delle cose che funzionano anche Adium, Skim, Mailsmith, SpamSieve e OpenOffice.

Ho scoperto (grazie Dany!) come usare Time Machine via AirPort e domani mi cimenterò.

La sensazione prevalente è quella delle maggiori prestazioni. Sto lavorando, nella mia maniera e con le mie applicazioni, e tutto funziona in modo percepibilmente più efficiente. I miracoli non attengono al silicio, ma c'è comunque un miglioramento evidente e concreto. In particolare, la gestione della memoria virtuale sembra essere fortemente migliorata.