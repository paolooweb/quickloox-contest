---
title: "Eran trecento(mila)"
date: 2010-08-27
draft: false
tags: ["ping"]
---

Ma quanto sarà abile il software Nmap nell'analisi delle reti?

Lo hanno usato per una analisi un po' particolare, quella delle <i>favicon</i> su Internet (la <i>favicon</i> è l'icona che sta vicino all'indirizzo del sito nel campo testo del <i>browser</i>).

Ne ha trovate 328.427 e gli autori della ricerca hanno pubblicato una <a href="http://nmap.org/favicon/" target="_blank">mappa di Internet</a> piuttosto originale, nella quale l'icona è tanto più grande quanto più traffico riceve quel sito.

La parte del leone la fa ovviamente Google. A guardare bene si vede anche Apple.

L'intera mappa è un quadrato con 37.440 pixel di lato e fortunatamente gli autori consentono di navigarla senza doverla aprire nel <i>browser</i>, che morirebbe soffocato o già di l&#236;.

Hanno anche messo in vendita un <i>poster</i>. Nel salotto giusto ci starebbe benissimo.

Nmap, per la cronaca, funziona benissimo su Mac.