---
title: "Xps vs. Pdf"
date: 2006-06-19
draft: false
tags: ["ping"]
---

<strong>Matt</strong> ha commentato in modo interessante la questione della lite Adobe-Microsoft sull&rsquo;inserimento di Pdf in Office assieme a Xps, un formato proprietario di Microsoft creato apposta per Office 2007. Faccio alcune osservazioni in merito qui.

La tradizione Microsoft &egrave; l&rsquo;<em>embrace and extend</em>. Adottano uno standard, lo &ldquo;estendono&rdquo; in modo che funzioni bene solo su Windows e poi lasciano che l&rsquo;enorme diffusione di Windows faccia il resto. Uno degli esempi pi&ugrave; eclatanti &egrave; Java. Microsoft ha apportato estensioni a Java tali che molti programmatori sono convinti di scrivere multipiattaforma, visto che scrivono in Java, e invece il loro codice funziona bene solo su Windows, a tradimento dell&rsquo;intero spirito della cosa. Sun ha dovuto affrontare Microsoft in tribunale per riuscire a fare in modo che Microsoft fosse diffidata dal mettere in Windows la &ldquo;sua&rdquo; versione di Java. Un altro esempio di questa condotta sono le estensioni FrontPage. In quanto monopolista, Micrsoft non pu&ograve; permettersi di sfruttare il suo monopolio per inquinare gli standard sui quali non ha controllo.

Durante i colloqui tra Microsoft e Adobe per l&rsquo;inclusione di Pdf in Office, secondo le <a href="http://www.bloomberg.com/apps/news?pid=10000103&amp;sid=as0kPe2z6_1o&amp;refer=us" target="_blank">versioni ufficiali</a> della stampa, &egrave; successo questo: Adobe ha chiesto soldi a Microsoft per Pdf. Microsoft ha rifiutato e ha tolto da Office sia Pdf che Xps. Adobe ha chiesto a Microsoft di rendere opzioni a pagamento sia l&rsquo;una che l&rsquo;altra e Microsoft ha rifiutato. Al che Adobe sta riflettendo se ricorrere all&rsquo;antitrust.

Questa &egrave; la mia versione ufficiosa, non confortata da nessuna fonte, basata sulla mia intepretazione dei fatti e sull’esperienza del passato.

Adobe ha detto a Microsoft <cite>siccome conosco le tue abitudini, non voglio che tu scriva una tua versione di Pdf inquinata che poi funziona solo su Windows. Se metti il Pdf deve essere quello vero, descritto nelle specifiche</cite>.

Microsoft ha risposto <cite>col cavolo. Niente Pdf in Office se non &egrave; come pare a me</cite>.

Adobe ha ribattuto <cite>se metti solo Xps mi crei un grosso problema e questo non ci rende pi&ugrave; amici</cite>.

Microsoft ha replicato <cite>benissimo, tolgo anche Xps. Poi dar&ograve; Xps come opzione gratuita e Pdf come opzione a pagamento, facendo pagare ai consumatori la mia licenza di Pdf da usare in Office</cite>.

Adobe ha risposto <cite>e io ti denuncio all&rsquo;antitrust</cite>.

Microsoft ha chiuso con <cite>benissimo, voglio proprio vedere</cite>.

Agli atti va messo che Pdf &egrave; uno standard di cui Adobe controlla la definizione, ma le cui specifiche sono pubbliche. Ognuno pu&ograve; scrivere il suo codice Pdf come vuole, basta che produca Pdf vero. Vedi Apple con Quartz e il supporto di Pdf a livello di sistema. I formati di Office sono interamente controllati da Microsoft. Per programmare il riconoscimento del formato Word, per esempio, bisogna fare ingegneria inversa e andare per tentativi.

Va messo agli atti anche che esiste l&rsquo;<a href="http://www.oasis-open.org/committees/tc_home.php?wg_abbrev=office" target="_blank">Open Document Format</a>, open source, a disposizione di tutti.