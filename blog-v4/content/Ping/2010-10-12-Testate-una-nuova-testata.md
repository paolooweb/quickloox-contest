---
title: "Testate una nuova testata"
date: 2010-10-12
draft: false
tags: ["ping"]
---

Da oggi, pare, dispongo di un nuovo blog su Computerworld Italia online, dal titolo <a href="http://www.cwi.it/blogs/lux-files" target="_blank">luX-files</a>.

La frequentazione di Computerworld è presumibilmente più aziendale in senso stretto di quella di Macworld; certamente è più ecumenica e ci sarà da parlare anche di quello che accade fuori da Apple.

È una sfida interessante con interlocutori difficili ed esigenti&#8230; esattamente come questa, che mi piace da anni e spero continui a piacere.

Chi ha il doppio di tempo da perdere rispetto alla lettura di Ping, ora ha una opportunità supplementare per spenderlo. Previo collaudo, beninteso.