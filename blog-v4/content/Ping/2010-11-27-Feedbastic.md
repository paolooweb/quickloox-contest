---
title: "Feedbastic"
date: 2010-11-27
draft: false
tags: ["ping"]
---

Nella mia ricerca di un editor di testo per iPad che non faccia rimpiangere troppo <a href="http://www.bbedit.com" target="_blank">BBEdit</a> ho optato al momento per <a href="http://www.textasticapp.com/" target="_blank">Textastic</a>.

Il programma ha punti di forza e punti deboli, come tutti gli altri editor di cui seguo l'evoluzione. A fare la differenza però potrebbe essere il programmatore, che pone grande attenzione al <i>feedback</i> dell'utenza.

Sul sito esiste un meccanismo di <i>feedback</i> commentabile e colloquiale, diverso dal solito indirizzo email. Ho già visto diverse funzioni che sono state implementate perché sono state chieste. Ho già avanzato un paio di richieste e una, dopo un breve scambio di opinioni sulla sua utilità, dal nulla ha acquisito lo status di <i>in valutazione</i> per poi passare a <i>pianificata</i>. In altre parole è possibile che un aggiornamento di Textastic mi porterà alcune funzioni che ho richiesto.

Da qui ci sarebbe da scrivere un articolo torrenziale sulle tematiche del <i>feedback</i>. Su come App Store sia una piattaforma dove fornire <i>feedback</i> è importantissimo anche per chi compra, perché un autore cerca di pubblicare software a prezzo conveniente per poi inserire nuove funzioni quando ha avuto un successo che lo rende conveniente. Sul fatto che in realtà nessuno sa ancora bene come scrivere una <i>app</i> perfetta per iPad e quindi tutti i contributi rischiano di avere utilità vera. Sulla valutazione di programmi e programmatori, oltre che dal numero di funzioni che ha un programma, in base alla loro capacità di offrire supporto e ascolto agli utilizzatori. Eccetera eccetera.

Il <i>feedback</i> è un'arma potentissima di miglioramento di massa. E non va giudicato in base al fatto di venire ascoltati direttamente o di ottenere realmente quello che si vuole. Sento beoti lamentarsi che hanno inviato un <i>feedback</i> ad Apple, o altra multinazionale del software dai numeri immensi, delusi perché non hanno avuto risposta.

Prima di tutto il <i>feedback</i> va inviato ogniqualvolta sia opportuno indipendentemente dalla risposta. Butto una cartaccia nel cestino, invece che per terra, indipendentemente da quanto siano pulite le strade.

Secondariamente, il mio <i>feedback</i> potrebbe anche essere stupido, inutile, sbagliato. Pretenderne l'accoglimento è come andare a votare e pensare che solo per via di questo il partito che si è scelto vincerà automaticamente le elezioni.