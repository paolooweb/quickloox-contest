---
title: "Traduzione cercasi<p>"
date: 2005-05-04
draft: false
tags: ["ping"]
---

Constatato che Dashboard serve, come li chiamiamo quei cosi?<p>

Quando Tiger è stato annunciato, ero molto scettico su Dashboard. Ho già abbastanza stupidaggini sullo schermo senza che debba aggiungerne Apple.<p>

Ma la carta vincente di Dashboard è proprio lo sparire a comando. Serve la calcolatrice? F12, calcolo, F12 e ho risolto il problema, senza fastidi sullo schermo (per la cronaca, la Calcolatrice di Tiger non è semplice come quella di Dashboard e merita davvero un sopralluogo). Mi serve un indirizzo che so essere presente nella rubrica? F12, prendo l&rsquo;indirizzo, un altro F12 ed è fatta.<p>

Fantastico. Ma i widget, come li chiamiamo? Accrocchi? Cazzilli? Aggeggi? Manzanilli (grazie, Eta Beta)?<p>

Non ho astio verso l&rsquo;inglese e anzi il sistema operativo lo uso in inglese&hellip; ma non mi dispiace avere una traduzione in italiano per quando serve.<p>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>