---
title: "Salviamo (in) AppleScript"
date: 2008-01-25
draft: false
tags: ["ping"]
---

Un AppleScript può essere salvato (registrato, ma qui dirò salvataggio per evitare ambiguità) su disco in vari modi e tutti sono utili, ma non sempre e non per ciascuno. Ecco le differenze.

Un AppleScript si può salvare come <em>script</em>. L'icona mostra un foglio con un angolo ripiegato e, su esso, una pergamena semiarrotolata alle due estremità; il file ha suffisso <code>.scpt</code>; uno script può essere eseguito da dentro Script Editor, con il comando Esegui (Comando-R), oppure modificato.

Altrimenti si può salvare come <em>applicazione</em>. L'icona mostra la pergamena di prima, che stavolta poggia su una base quadrata. Il file ha suffisso <code>.app</code>; può essere eseguito da Finder con un doppio clic e può essere modificato, o eseguito, da dentro Script Editor.

Altre due possibilità di salvataggio sono <em>bundle dell'applicazione</em> e <em>bundle dello script</em>. Servono per quando creiamo strutture più complicate di uno script singolo, per esempio applicazioni che contengono più script. Un bundle è una struttura equivalente a quella che vediamo quando facciamo Control-clic sopra l'icona di un programma e selezioniamo Mostra Contenuto Pacchetto. Se siamo già a uno stadio di programmazione AppleScript evoluta li conosciamo benissimo, altrimenti non c'è (per ora) ragione di considerarli. Facciamo finta che non esistano.

L'ultima possibilità di salvataggio è come <em>testo</em>. L'icona è come quella dello script, ma sotto la pergamena c'è scritto TEXT; Il suffisso del file è <code>.applescript</code>; un testo AppleScript si modifica dentro qualunque editor di testo (più Script Editor) e si può eseguire dentro Script Editor stesso. Sostanzialmente l'unica ragione per salvare come testo è la necessità eventuale di modificare il file in un editor di testo (come <a href="http://www.barebones.com/products/textwrangler" target="_blank">TextWrangler</a>) invece che in Script Editor.

Dopo tutta questa teoria, un po' di pratica. Accendiamo Script Editor e scriviamoci dentro

<code>tell application "Finder" to display dialog "Vuoi fare un logout?"</code>

<code>tell application "System Events" to log out</code>

(tradotto: <em>di' all'applicazione Finder di mostrare la finestra di dialogo</em>)

(tradotto: <em>di' all'applicazione Sistem Events di fare il logout</em>)

(occhio al comando AppleScript: deve essere log out, con lo spazio)

Salviamo il file come applicazione e facciamolo partire con un doppio clic. Appare la finestra di dialogo con la richiesta di logout. Se clicchiamo Cancel (annulla), non succede niente; se clicchiamo Ok, il Mac fa davvero il logout! Dunque, attenzione e cervello acceso.

L'applicazione System Events è interessante, perché non ha un'icona nella cartella Applicazioni. È un'applicazione virtuale, che serve ad AppleScript per trasmettere comandi non a uno specifico programma, come per esempio il Finder, ma all'intero sistema. Al posto del logout si sarebbe potuto mettere, per dire, Stop (sleep) o  Riavvia (restart).

Questo minitutorial è ispirato a <a href="http://www.tuaw.com/2008/01/13/applescript-saving-scripts/" target="_blank">quest'altro</a>, pubblicato su The Unofficial Apple Weblog. È il terzo della serie: per trovare i primi due, basta tornare indietro di sette giorni in sette giorni dentro Ping.