---
title: "Questo per i cavi"
date: 2008-03-05
draft: false
tags: ["ping"]
---

<cite>Oh cavo, cavo cavo&#8230; non distaccarti&#8230;</cite> (Elio e le Storie Tese)

Nell'ennesimo esperimento di audiofilia, i partecipanti non sono riusciti a distinguere il suono emesso dagli altoparlanti, quando questi erano collegati con cavi di alta qualità e rinomanza, dallo stesso suono emesso dagli stessi altoparlanti quando questi erano collegati con il filo di metallo <a href="http://forums.audioholics.com/forums/showpost.php?s=97d4a3c39d247bf955a57b3953326a34&amp;p=15412&amp;postcount=28" target="_blank">di un appendiabiti</a>.

Saranno gli stessi che giudicano la qualità dei dischi rigidi da come si comporta il disco rigido che tengono in casa.