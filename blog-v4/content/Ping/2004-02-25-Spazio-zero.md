---
title: "Spazio zero"
date: 2004-02-25
draft: false
tags: ["ping"]
---

Se non c’è neanche più un byte da scrivere Mac OS X è in difficoltà

Mac OS X è un sistema Unix e dà per scontata una disponibilità almeno minima di spazio su disco. Mentre su Linux viene configurata una partizione fissa destinata allo scopo, Mac OS X come esce da Apple usa il normale spazio libero.

Se lo spazio si esaurisce sono possibili vari comportamenti irregolari: programmi che perdono le preferenze (il sistema apre il file e cerca di aggiornarlo, ma non può scrivere niente e neanche chiudere il file, che si danneggia), programmi che terminano, scaricamenti che muoiono e altro ancora.

Prima di arrivare questo punto, però, Mac OS X emette almeno un paio di avvisi: uno benevolo e uno più drastico, che invita addirittura alla chiusura forzata di qualche programma.

Se vedi uno di questi avvisi è il momento di chiudere qualche programma, chiudere qualche finestra di programma (se Safari ha un sacco di finestre aperte occupa un sacco di spazio disco temporaneo), vuotare il Cestino e buttare qualche file inutile.

Non giriamoci intorno: in un disco come minimo da quaranta giga, c’è sicuramente qualche file inutile.

<link>Lucio Bragagnolo</link>lux@mac.com