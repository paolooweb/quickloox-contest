---
title: "Leoparderie"
date: 2007-11-02
draft: false
tags: ["ping"]
---

In ordine sparso, ho trovato un paio di bug minori in Tex-Edit Plus. Il menu Font appare e scompare correttamente, al secondo tentativo. Poi non si riesce a ottenere la <em>È</em> con un'unica combinazione di tastiera.

BBEdit ha aggiornato alla 8.7.1, ma Leopard non c'entra niente. In questi giorni è davvero una mosca bianca.

<a href="http://morrick.wordpress.com" target="_blank">Riccardo</a> sta raccontando della sua esperienza-Leopard ed è lettura da non perdere.