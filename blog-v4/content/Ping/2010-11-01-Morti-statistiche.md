---
title: "Morti statistiche"
date: 2010-11-01
draft: false
tags: ["ping"]
---

Ricordo come fosse ieri (bugia: ho spiato l'archivio) di avere <a href="http://www.macworld.it/ping/life/2006/07/28/il-gusto-del-pacco/">annunciato l'acquisto di una AirPort Express</a> il 28 luglio 2006.

Ultimamente il suo comportamento era divenuto erratico. A volte i computer perdevano la connessione con la base, a volte il traffico dati rallentava, a volte spegnere e riaccendere la base durava per mezz'ora e poi bisognava ripetere la mossa, a volte funzionava tutto ma senza certezze che sarebbe durata e avanti cos&#236;.

Non posso dire che si sia rotta la base: immediatamente prima di sostituirla ha funzionato ininterrottamente per quasi una giornata, prima di dare segni di squilibrio. Però ho scelto di cambiarla.

Sono quattro anni e tre mesi di durata.

Dall'archivio è saltato fuori anche l'allarme del sito MacBidouille, per il quale la vita media di una AirPort Express <a href="http://www.macbidouille.com/news/2006/06/16" target="_blank">sarebbe di 15,35 mesi</a>. Lanciato il 16 giugno 2006.

I miei cinquantuno mesi saranno magari una anomalia statistica. Ma per portare la media a quanto sparacchiò MacBidouille, ci vuole un bel numero di basi che si guastino rapidamente e non ho percezione del fenomeno.

L'allarmistica infondata suscita sempre molto clamore quando nasce, assai meno quando sparisce.