---
title: "Tracce da seguire"
date: 2009-07-06
draft: false
tags: ["ping"]
---

S&#236;, le <a href="http://www.apple.com/it/macosx/refinements/enhancements-refinements.html" target="_blank">novità di Snow Leopard</a> degne di menzione sul sito Apple sono un centinaio. Eccone, anche oggi, un'altra, tradotta in italiano.

<b>Migliore scrittura a mano del cinese</b>

Ora è possibile usare una <i>trackpad</i> <i>multitouch</i> per tracciare ideogrammi cinesi nei propri documenti. Appariranno sullo schermo in una nuova finestra di inserimento, che raccomanda i caratteri in base a ciò che è stato disegnato e permette di scegliere quello giusto. La finestra offre inoltre suggerimenti per gli ideogrammi successivi in funzione di quello che è stato scelto.

Una facile profezia: venticinque anni fa, pareva che chi non avesse imparato il russo sarebbe rimasto tagliato fuori. Oggi pare che chi non impari il cinese rimarrà tagliato fuori. Non succederà. A parte ciò, forse non c'è mai stato un modo altrettanto immediato di tracciare ideogrammi sullo schermo di un computer.