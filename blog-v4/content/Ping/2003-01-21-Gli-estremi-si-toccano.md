---
title: "Gli estremi si toccano"
date: 2003-01-21
draft: false
tags: ["ping"]
---

Tra 12” e 17” c’è una cosa in comune: li voglio tutti e due

Apple mi ha gentilmente invitato alla presentazione ufficiale in Italia dei nuovi PowerBook G4, con Enzo Biagini (Apple Mediterraneo) ed Erik Stannow (Apple Europa).

Sono innamorato del modello con schermo da 12”, perché è ultracompatto, sta dappertutto; se il computer mi servisse per coordinare e organizzare dappertutto, ne vorrei assolutamente uno.

Sono innamorato del modello con schermo da 17”, perché ha una risoluzione oltraggiosa per un portatile. Ci sta tutto e persino un disordinato come me potrebbe avere una scrivania come si deve. Una cosa che non dice nessuno: la maggiore risoluzione fa risparmiare almeno il 20% di tempo di uso dell’interfaccia, perché le cose sono già lì sullo schermo pronte da cliccare.

Quello piccolo e quello grande: sono diversissimi, eppure così pericolosamente desiderabili.

<link>Lucio Bragagnolo</link>lux@mac.com