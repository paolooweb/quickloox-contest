---
title: "Alla faccia dell’interfaccia"
date: 2004-05-13
draft: false
tags: ["ping"]
---

Linux fa progressi, sì, ma Mac OS X è veramente un’altra cosa

Seppure cresciuto mostruosamente in facilità di utilizzo negli ultimi anni, Linux deve ancora arrivare a vedere da lontano la facilità di uso di Mac OS X.

Ho provato per necessità di lavoro a installare una Debian su una partizione di un disco esterno che mi torna comoda. Trovare le immagini disco e farne comodi Cd è stato un attimo (anni fa sarebbe stato un incubo. Bravi). Ma da lì in poi inizia il dramma, a meno di non essere molto fortunati.

Sto ancora nuotando nei readme. Per il momento ho capito che il Cd di installazione fa il boot solo da certi sistemi e non da altri (non dal mio), che una partizione Ufs (Unix File System, e sottolineo Unix) non va bene, che si può installare senza fare il boot da Cd ma ci vuole una partizione intermedia Hfs (e solo Hfs) da posizionare esattamente prima di quelsiasi altra partizione sul disco e che alla fine si può fare ma bisogna passare da Open Firmware.

Linux mi sta bene, ma datemi un Cd con dentro un eseguibile che faccia il necessario per avere una installazione funzionante, e punto.

<link>Lucio Bragagnolo</link>lux@mac.com