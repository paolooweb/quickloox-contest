---
title: "Si fax quel che si può"
date: 2006-12-13
draft: false
tags: ["ping"]
---

<strong>Flaminio</strong> scrive (grazie!) per segnalare, dice, <cite>un qualcosa che mi permettesse di inviare fax in modo semplice e soprattutto gratuito.</cite>

<cite>Ho trovato</cite> - continua Flaminio - <a href="http://www.faxator.com/home.htm" target="_blank">Faxator</a>, <cite>che con una semplice registrazione sul loro sito ti permette di inviare, solo in Italia, fax gratis. Basta fornire un indirizzo email valido, un numero di cellulare ed una password e il gioco è fatto. Per noi maniaci della mela c’è anche un simpatico widget per tenere sotto controllo l’invio e per scaricare il rapporto di avvenuta ricezione.</cite>

<cite>Spero possa essere utile.</cite>

A qualcuno, certamente. Il fax è tuttora durissimo a morire.