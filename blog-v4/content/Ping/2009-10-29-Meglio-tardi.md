---
title: "Meglio tardi"
date: 2009-10-29
draft: false
tags: ["ping"]
---

Apple 2.0 <a href="http://brainstormtech.blogs.fortune.cnn.com/2009/10/29/droid-vs-iphone-lets-count-the-apps/">confronta</a> le 93 mila applicazioni di App Store per iPhone e iPod touch con le 11.300 applicazioni per gli smartphone Android di Google.

Quello che è incredibile è il sommario del pezzo, che recita <i>ma quante applicazioni realmente ti servono?</i>

Da venticinque anni sento gente criticare Mac perché <i>ha pochi programmi</i> e questa domanda non l'avevo proprio mai letta. Per fare cambiare l'opinione comune ci voleva proprio iPhone.