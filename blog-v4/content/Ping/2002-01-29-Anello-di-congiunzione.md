---
title: "L’anello di congiunzione"
date: 2002-01-29
draft: false
tags: ["ping"]
---

Una idea: coniugare le possibilità di AppleScript con quelle del Terminale. C’è chi ci sta già pensando

Nell’edizione di dicembre dei Developer Tools di Mac OS X ci sono due regalucci che riguardano AppleScript e contribuiscono a gettare un ponte prezioso tra il linguaggio di scripting di Apple e le infinite possibilità del Terminale.
Il primo è

do shell script [comando del Terminale]

e permette di impartire comandi Unix senza lasciare AppleScript e senza aprire una finestra Terminale.
L’altro regaluccio permette di ottenere un percorso di directory in stile Unix partendo da quello in stile Mac e si scrive POSIX path of [percorso Mac]. Così com’è non funziona sempre come dovrebbe, ma a Macosxhints.com hanno realizzato uno scriptino che rimedia, a patto di non usare caratteri non Ascii nei nomi file (tipo Opzione-f, per dirne uno). Eccolo qua:

on unix_path(mac_path)
  set unixpath to (POSIX path of mac_path)
  set chars to every character of unixpath
  repeat with i from 1 to length of chars
    if "!$&\"'*(){[|;<>?~` \\" contains (item i of chars as text) then
      set item i of chars to "\\" & (item i of chars as text)
    end if
  end repeat
  return every item of chars as string
end unix_path

A chi trova queste cose difficili rispondo che ha ragione: per me sono esoteriche a dire poco. Ma è solo l’inizio: tipicamente, una volta introdotta una cosa furba, arriverà un programmatore con un po’ di sensibilità Mac e aggiungerà un’interfaccia grafica a beneficio del resto di noi. Per questo sono cose da accogliere magari a sopracciglio alzato, ma con fiducia, e magari come pretesto per provare a saperne un pizzico di più.

lux@mac.com