---
title: "Fuochi a volontà"
date: 2009-07-05
draft: false
tags: ["ping"]
---

L'arte ha bisogno di vincoli e di limitazioni, per eccellere. Per questo, viste le limitate capacità della fotocamera dell'iPhone originale, ho sempre pensato che si dovessero organizzare concorsi fotografici per iPhone.

The Unofficial Apple Weblog non ha organizzato il concorso, ma ha raccolto fotografie e filmati realizzati con iPhone in occasione del 4 luglio, festa dell'indipendenza americana.

Il 4 luglio negli Stati Uniti sono d'obbligo i fuochi d'artificio, notoriamente soggetti fotografici non certo semplici. Ai visitatori della <a href="http://drop.io/tuaw_fireworks/chronological" target="_blank">galleria</a> giudicare i risultati.