---
title: "Topi di campagna<p>"
date: 2004-12-29
draft: false
tags: ["ping"]
---

Talmente facile smentirli che fanno tenerezza<p>

PcMagazine sembra avere ufficialmente lanciato una campagna contro Apple e contro Macintosh. Una di quelle cose che periodicamente appaiono sulle riviste per roba Windows, mirata a convincere il mondo che tutti devono usare computer brutti, inefficienti e inaffidabili, anche chi non vuole.<p>

Se però ai tempi della grande crisi di Apple erano pantegane, ora sono topolini. Hanno già dichiarato eMac come peggiore computer desktop dell&rsquo;anno. eMac doveva essere venduto solo nelle scuole e Apple ne ha fatto una versione per il pubblico esattamente in base alla domanda. Un computer venduto solo perché è stato espressamente richiesto non sarà tanto brutto.<p>

Ora si è messo John Dvorak, <em>columnist</em> storico della testata, a spiegare nel suo solito stile di bastian contrario che Apple è destinata a <a href="http://www.pcmagazine.com/article2/0,1759,1745930,00.asp">morire</a>.<p>

L&rsquo;argomento è troppo stupido per meritare una risposta breve (al limite una lunga). Mi limito a suggerire di attendere la comunicazione dei risultati finanziari del primo trimestre 2005 di Apple, previsti per metà gennaio, e spedirli a John Dvorak.<p>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>