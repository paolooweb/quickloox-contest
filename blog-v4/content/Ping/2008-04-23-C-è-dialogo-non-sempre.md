---
title: "C'è dialogo, non sempre"
date: 2008-04-23
draft: false
tags: ["ping"]
---

Sono reduce da una piacevolissima chiacchierata con il Terminale. Mi servivano alcune schermate in formati diversi da quello standard (Png) e il comando <code>screencapture</code> è assai comodo.

<code>screencapture -t tiff <nomefile></code> registra una schermata in formato Tiff, anche se lo standard è Png.

<code>screencapture -i -t tiff <nomefile></code> aggiunge l'interattività (rettangolo invece che tutto schermo, oppure finestra eccetera).

<code>screencapture -c -i -t tiff</code> copia in memoria invece che creare un file.

E cos&#236; via. Una bellezza. <code>man screencapture</code> spiega tutto.

Invece dovevo colloquiare con gente in California e ci siamo attrezzati con Skype, poi con iChat, ma non c'è stato verso. Prima sentivo benissimo io ma non loro, poi loro s&#236; ma io male, poi il server Aim andava e veniva, un disastro. Alla fine c'è voluta, orrore, una telefonata tradizionale.

Di chat e videochat a buon fine ne ho fatte numerose e questo è sicuramente un incidente unico. Però il telefono tradizionale funziona senza fallo da almeno un quarto di secolo. Anche i sistemi informatici dovrebbero arrivare allo stesso tasso di efficacia.