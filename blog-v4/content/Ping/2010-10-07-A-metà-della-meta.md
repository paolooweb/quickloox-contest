---
title: "A metà della meta"
date: 2010-10-07
draft: false
tags: ["ping"]
---

Secondo StatCounter, in settembre Internet Explorer <a href="http://gs.statcounter.com/#browser-ww-monthly-200909-201009" target="_blank">è sceso al 49,87 percento del traffico web globale</a>, perdendo per la prima volta in questo millennio la maggioranza assoluta dei consensi.

Certamente governa tuttora, perché il secondo browser in classifica è Firefox con il 31,5 percento, ancora lontano.

Tuttavia adesso si respira meglio, molto meglio. Solo due anni fa la percentuale di traffico di Internet Explorer era a due terzi del totale, oggi a (meno della) metà.

Nessuno vuole male a Internet Explorer in quanto tale e si augura ogni bene alla versione 9, <i>se</i> è vero che supporta gli standard web e Html5 come i suoi predecessori si sono ben guardati dall'osare, a favorire una stagione di buio e arretratezza durata più di dieci anni.

Allo stesso tempo, la mia visione ideale del traffico web è quella di sette browser diversi ognuno con il 14,29 percento ciascuno.

Oltre è eccesso di scelta, sotto è difetto di libertà. Oggi e ancora per un bel po', chi spegne Internet Explorer e accende qualunque altra alternativa fa del bene al web e a chi lo popola.