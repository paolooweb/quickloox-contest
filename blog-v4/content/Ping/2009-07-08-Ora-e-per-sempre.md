---
title: "Ora e per sempre"
date: 2009-07-08
draft: false
tags: ["ping"]
---

Il paranoico della sicurezza che si nasconde in tutti noi forse apprezzerà l'esistenza di un comando di Terminale per cancellare i file in modo sicuro senza passare dal Cestino e senza applicazioni aggiuntive: <code>srm</code>.

Leggersi bene il manuale prima, perché senza specificare parametri per la cancellazione <i>un po' meno</i> sicura lui passa sopra il file 35 volte e si fa presto a passare dalla paranoia per la sicurezza a quella del tempo che passa.