---
title: "Viva la virtualizzazione"
date: 2006-05-18
draft: false
tags: ["ping"]
---

Dall&rsquo;esperienza provata, se vedo un MacBook Pro che ha fatto il boot da Windows, con lo schermo invaso da Windows, mi viene un po&rsquo; il conato di vomito.

Se invece funziona <a href="http://www.parallels.com/" target="_blank">Parallels Workstation</a>, e Windows &egrave; solo una finestra, padroneggiabile, dentro Mac OS X, si respira molto meglio.

Viva la virtualizzazione.