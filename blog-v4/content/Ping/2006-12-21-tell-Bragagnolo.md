---
title: "tell Bragagnolo"
date: 2006-12-21
draft: false
tags: ["ping"]
---

È ancora presto per date precise, ma intorno a fine gennaio dovrei tenere in <a href="http://www.macatwork.net" target="_blank">Mac@Work</a> un piccolo corso, molto base, su <a href="http://www.apple.com/it/macosx/features/applescript/" target="_blank">AppleScript</a>. Secondo te quali sono i mattoni fondamentali, che se non si capiscono quelli proprio non si riesce a proseguire?

end tell