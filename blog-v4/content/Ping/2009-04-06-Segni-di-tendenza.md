---
title: "Segni di tendenza"
date: 2009-04-06
draft: false
tags: ["ping"]
---

Perché non si dimentichi che il cammino è ancora molto lungo, ma si sta lavorando bene, ecco il dettaglio del <a href="http://marketshare.hitslink.com/os-market-share.aspx?qprid=9" target="_blank">traffico web secondo Net Applications nel corso degli ultimi undici mesi</a>.

Linux continua a restare un caso misterioso. Il resto è lampante.