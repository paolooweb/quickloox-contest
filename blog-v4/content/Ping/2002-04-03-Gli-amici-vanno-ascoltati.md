---
title: "Gli amici vanno ascoltati"
date: 2002-04-03
draft: false
tags: ["ping"]
---

Monopolista e per giunta anche con qualche problema

Il mio amico Stefano, al telefono: “Ma possibile che l’unica cosa che non funziona sotto Mac OS X sia Explorer?”
E io: “Installa Netscape, o Mozilla, o OmniWeb, o iCab, o Opera... non sono i browser che mancano, su Mac OS X, e nessuno di questi è prodotto da aziende condannate in tribunale per abuso di posizione dominante”.
Lui ha deciso che non era una cattiva idea e che avrebbe provato.
Gli amici sono sempre da ascoltare. :-)

<link>Lucio Bragagnolo</link>lux@mac.com