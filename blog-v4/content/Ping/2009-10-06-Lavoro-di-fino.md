---
title: "Lavoro di fino"
date: 2009-10-06
draft: false
tags: ["ping"]
---

Circolano decine di articoli sul <i>design</i> di Apple, ma nessuno prima d'ora <a href="http://glyph.twistedmatrix.com/2009/10/unboxing-you-won-see-on-gizmodo-or.html" target="_blank">aveva descritto</a> il <i>design</i> delle <i>offerte di lavoro Apple</i>.

L'autore chiosa <cite>qualunque azienda che sappia mettere questo livello di attenzione ai dettagli perfino nelle</cite> offerte di lavoro <cite>deve essere un posto interessante in cui lavorare</cite>.

Nella seconda foto, in alto a sinistra, si vede la scritta <i>Ah, paperwork</i>. <i>Ah, scartoffie</i>. Ironia, cioè classe.

E poi mi guardano male quando racconto il gusto di aprire una confezione di iPod.