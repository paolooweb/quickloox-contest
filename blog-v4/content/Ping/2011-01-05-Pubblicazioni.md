---
title: "Pubblicazioni"
date: 2011-01-05
draft: false
tags: ["ping"]
---

Con imperdonabile ritardo do finalmente notizia dell'uscita di <a href="http://www.2spaghi.it/spagoguida/" target="_blank">Spagoguida</a>, la prima guida ai ristoranti italiani compilata dai frequentatori dei ristoranti stessi e assemblata dal sito <a href="http://www.2spaghi.it" target="_blank">2Spaghi</a>. Un libro vero (e ci vorrebbe anche l'ebook), edito da Morellini a 15,90 euro.

Conosco di persona metà dello <i>staff</i> di 2Spaghi e non posso che raccomandare il sito. Diversa è la raccomandazione delle recensioni e dei ristoranti; tuttavia 2Spaghi è un sito dove piuttosto tende a mancare un ristorante, ma le recensioni tendono molto ad avvicinarsi alla realtà e questo continua a piacermi anche dopo anni dalla fondazione del sito. Aggiorno sempre 2Spaghi quando mi capita di uscire a cena e cos&#236; spero di tutti.

Colgo l'occasione per consolare inoltre i ventidue lettori di Preferenze sul defunto Macworld Italia cartaceo (uno meno di Guareschi, che ne aveva due meno di Manzoni, sempre più in basso insomma): le mie intemerate mensili ora appaiono in doppia pagina su Applicando, a partire dal numero di gennaio che dovrebbe già trovarsi in edicola.

La rubrica si chiama <i>Mission Control</i>, pensando al Lion prossimo venturo  ma anche a come Mac e iOS ci permettono di portare a termine le nostre missioni sempre meglio e con più divertimento di quanto permettono gli &#8220;altri&#8221;.