---
title: "A che cosa serve il Terminale?"
date: 2001-12-17
draft: false
tags: ["ping"]
---

Sembra incredibile, ma c’è “a new hope”

Chi scrive ha un debole per le interfacce grafiche; sono quelle, direbbe un Darwin dell’informatica, che distinguono l’homo utens dalla bestia digitans.
Ma l’arte vera consiste nel dare il massimo con gli strumenti minimi.
Prova ad aprire il Terminale di Mac OS X (va bene anche qualunque programma per Mac OS capace di lavorare in telnet, da ZTerm in poi) e digitare

telnet 62.250.7.101

e poi a capo.
Improvvisamente le firmette grafiche in fondo a certa posta elettronica sembreranno, veramente, cose di un’altra epoca. Magari in una galassia lontana lontana...