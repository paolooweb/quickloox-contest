---
title: "La ventola di La Palisse"
date: 2004-06-10
draft: false
tags: ["ping"]
---

Se il marchese avesse usato un PowerBook sarebbe stato in buona compagnia

Mi è testimone l’amico $imy.
Eravamo a inizio maggio a fare la notte in bianco a Padova, collegati alla Lan nonstop di <link>Webbit</link>http://www.poc.it/stories/continua.php?cod=373, e si chiacchierava delle domande lapalissiane che si fanno nelle mailing list (il marchese de La Palisse fu un noto eroe di guerra talmente valoroso che, secondo una poesiola, persino un attimo prima di morire era ancora in vita).

E io ebbi a dire che, appena scoppiata l’estate, qualcuno avrebbe posto una domanda del tipo:

come mai ci sono più di trentacinque gradi all’ombra, bisogna mettere la cioccolata in frigorifero, ho fatto il mutuo per il condizionatore, la notte dormo nudo come un verme, le fontane sono prese d’assalto da turisti in mutande e la ventola del mio PowerBook si accende più spesso che a Natale con tre gradi sotto zero?

La realtà supera sempre la fantasia e adesso queste domande stanno arrivando davvero. Già, chissà come mai.

<link>Lucio Bragagnolo</link>lux@mac.com