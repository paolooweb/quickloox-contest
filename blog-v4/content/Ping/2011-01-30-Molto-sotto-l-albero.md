---
title: "Molto sotto l'albero"
date: 2011-01-30
draft: false
tags: ["ping"]
---

Babbo Natale ha chiuso l'edizione 2010 con un colpo di coda gradito quanto inaspettato: <a href="http://www.avernum.com/avernum6/index.html" target="_blank">Avernum 6 di Spiderweb Software</a>.

Gioco di ruolo di vecchia scuola, con la grafica isometrica e lontana dall'iperrealismo odierno. Però la trama, accidenti se c'è una trama, dopo un'ora che sei dentro ti immedesimi come fosse World of Warcraft. La trama, il <i>lore</i> direbbero i giocatori abituati al <i>fantasy</i>, vale più di tutto.

Garantito: pochi sotterranei tengono testa a quello di Avernum per ricchezza di particolari e profondità di gioco. Non so se arriverò in fondo prima della fine dell'inverno. Ma devo farcela per onorare Babbo Natale e poi perché non so che cosa potrei trovare nell'uovo di Pasqua.