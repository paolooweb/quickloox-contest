---
title: "Dove andiamo"
date: 2009-05-28
draft: false
tags: ["ping"]
---

Giusto per ricordarci chi siamo e da dove veniamo, c'è un signore che ha costruito un processore più o meno equivalente al 6502 che usava l'Apple II. A mano, con i cavi.

Qui c'è <a href="http://www.wired.com/gadgetlab/2009/05/homebrewed-cpu/" target="_blank">una foto significativa</a>. L'autore ha anche <a href="http://www.stevechamberlin.com/cpu/index.html" target="_blank">un blog</a> e ha preparato <a href="http://www.stevechamberlin.com/cpu/Maker%20Faire.ppt" target="_blank">una presentazione</a> del proprio lavoro, che riguardando equipaggiamento oramai primitivo è appropriatamente realizzata nel primitivo PowerPoint (ma si vede tranquillamente con Keynote).