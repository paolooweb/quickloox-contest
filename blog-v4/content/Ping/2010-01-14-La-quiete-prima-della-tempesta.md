---
title: "La quiete prima della tempesta"
date: 2010-01-14
draft: false
tags: ["ping"]
---

Sto finendo di tenere un corso su Mac OS X per una cinquantina di persone.

Visto che la mia prima lezione inaugurava l'aula didattica mi sono preso la libertà di fotografarla appena aperta e vuota, un attimo prima che iniziassero a entrare gli studenti.

Gli iMac si intonano anche con il silenzio.