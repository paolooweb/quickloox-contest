---
title: "Carta da musica"
date: 2009-12-06
draft: false
tags: ["ping"]
---

L'amico <a href="http://www.facebook.com/giulimich" target="_blank">Giuliano</a> mi ha fatto pervenire copia del suo libro <a href="http://www.hoepli.it/libro/fare-musica-con-il-mac.asp?ib=9788820337940" target="_blank">Fare musica con il Mac</a>, edito da Hoepli al prezzo di 16,90 euro.

Appena l'avrò letto lo recensirò.

Nel frattempo è ugualmente raccomandato. Non perché Giuliano sia un amico, ma perché è musicista e macchista di valore.

E poi &#8211; non si dovrebbe dire, perché si recensisce il definitivo &#8211; le bozze le ho già lette. :-)