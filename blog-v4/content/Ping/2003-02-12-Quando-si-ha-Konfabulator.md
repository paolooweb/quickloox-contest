---
title: "Quando si ha tutto da perdere"
date: 2003-02-12
draft: false
tags: ["ping"]
---

Che almeno ci sia qualcosa di bello da guardare

Qual è la traduzione di “widget” in italiano? Ci si avvicina con cazzillo, aggeggio, accrocchio, coso; qualcosa che potrebbe servire oppure no ma è capace di un’attrazione irresistibile.

Ecco, Konfabulator è la collezione di widget più inutile, ostentata, luccicante, giocosa che sia apparsa finora. Devo dire anche che una cosa del genere, su Mac OS 9, era assolutamente impensabile.

Senza costrutto, più forma che sostanza, tutto fumo e niente arrosto, solo chiacchiere e distintivo; ma <link>Konfabulator</link>http://www.konfabulator.com è assolutamente irresistibile. Fai come me: scaricala, carica tutti i widget e poi dimenticatene. Ogni tanto ne salta fuori uno e si ha quella esatta sensazione di quando non hai studiato ma riesci a farla franca all’interrogazione.

<link>Lucio Bragagnolo</link>lux@mac.com