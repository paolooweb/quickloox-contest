---
title: "Giocare col (cavo di) fuoco"
date: 2006-04-24
draft: false
tags: ["ping"]
---

Dopo poche ore dall’annuncio del <a href="http://www.apple.com/it/macbookpro/" target="_blank">MacBook Pro 17”</a> (non hanno ancora neanche italianizzato la pagina) mi piace ricordare un <a href="http://blogs.zdnet.com/Apple/?p=57" target="_blank">pezzo di Jason O’Grady</a> dell’8 dicembre 2005, intitolato <cite>FireWire non è morta, ma è tenuta in vita artificialmente</cite>. Ecco un estratto:

<cite>Ora sento che FireWire è stata completamente abbandonata nei nuovi iBook Intel in arrivo il mese prossimo, ma questa perdita non dovrebbe sorprendere nessuno. Un uccellino mi ha detto che i nuovi PowerBook Intel perderanno completamente FireWire 400 e conserveranno solo una porta FireWire 800 come concessione ai professionisti del video.</cite>

La situazione reale è questa:

<ul type="square">
	<li>iBook Intel: di là da venire.</li>
	<li>MacBook Pro 15”: una porta FireWire 400</li>
	<li>MacBook Pro 17”: una porta FireWire 400 e una porta FireWire 800.</li>
</ul>

Avrei anche una collezione di siti che contestano l’abbandono di FireWire 800 all’indomani dell’annuncio di MacBook Pro 15”, ma facciamo che la butto via, che non ho tempo.