---
title: "Not in my nickname"
date: 2008-04-28
draft: false
tags: ["ping"]
---

Pare che <a href="http://www.koingosw.com/products/macpilot.php" target="_blank">MacPilot 3.0.1</a> abbia aggiunto oltre 200 parametri di Mac OS X all'elenco di quelli che permette di governare attraverso l'interfaccia grafica.

Sembra che MacPilot 3.0.1 consenta di modificare mediante una semplice interfaccia grafica oltre 600 parametri nascosti di Mac OS X.

Io vorrei che l'interfaccia grafica riducesse la difficoltà, invece che aumentarla. Nella mia utenza MacPilot non lo voglio.