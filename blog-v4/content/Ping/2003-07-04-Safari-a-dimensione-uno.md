---
title: "Safari a dimensione uno"
date: 2003-07-04
draft: false
tags: ["ping"]
---

Un gesto del Terminale e il problema è risolto

Sembra che Safari 1.0, per default, non sempre visualizzi il testo di dimensione 1 (parlando di Html) come la gente si aspetta.

Nelll’attesa di un update da parte di Apple è sufficiente aprire il Terminale e digitare:

defaults write com.apple.Safari WebKitMinimumFontSize 9

E tutto si sistema.

<link>Lucio Bragagnolo</link>lux@mac.com