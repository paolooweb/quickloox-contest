---
title: "Cattivi maestri"
date: 2010-11-15
draft: false
tags: ["ping"]
---

L'idea non è mia ma di John Gruber di Daring Fireball e anche lui dice che non è stata sua.

Non ho tempo ora, ma appena ho tempo <a href="http://daringfireball.net/2010/11/flash_free_and_cheating_with_google_chrome" target="_blank">tolgo Flash dal mio Mac</a>.

Essendo un Mac portatile guadagno all'istante una porzione consistente di autonomia e mi risparmio l'enorme molte di buchi di sicurezza e funzionamento che regolarmente emergono.

L'autonomia aumenta anche perché il processore è meno impegnato, ossia consuma meno, il che significa meno sollecitazione della macchina e una virgola in meno di elettricità sprecata. Briciolissime che non salveranno il mondo e neanche la mia bolletta, ma perché non migliorare in efficienza se è facile?

Ci saranno più puntate della <i>fiction</i>. Chi non è ne sicuro non mi imiti. Chi ne è sicuro, che aspetta?