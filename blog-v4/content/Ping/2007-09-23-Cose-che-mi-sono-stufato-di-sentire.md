---
title: "Cose che mi sono stufato di sentire"
date: 2007-09-23
draft: false
tags: ["ping"]
---

<cite>Ognuno ha il diritto di esprimere la sua opinione.</cite>

Certo, e anche il dovere di essere minimamente informato, o di accendere il cervello prima di parlare. È questione di rispetto per chi hai davanti.

<cite>Amo Apple, ma <critica a scelta>.</cite>

Sembra che una critica sia necessariamente più giusta se uno ama Apple e più sbagliata se uno odia Apple. A me sembra che una critica sia giusta se è giusta e sbagliata se è sbagliata.

<cite>Ho comprato novantanove Mac e Apple dovrebbe fare qualcosa per utenti come me.</cite>

Se non erro, ti ha venduto novantanove Mac, presumibilmente con tua soddisfazione. Con lo stesso metro, Apple potrebbe dire <em>ho venduto a X novantanove Mac e X potrebbe anche fare qualcosa per aziende come questa.</em>