---
title: "Il sei è sufficiente"
date: 2007-12-29
draft: false
tags: ["ping"]
---

Come è già accaduto a suo tempo con il <em>clock</em> dei processori, qualcuno inizia finalmente a spiegare che il numero dei megapixel <a href="http://6mpixel.org/en/" target="_blank">non è proporzionale</a> alla qualità della fotocamera.

Se è una compatta, oltre i sei megapixel inizia a essere roba da evitare.