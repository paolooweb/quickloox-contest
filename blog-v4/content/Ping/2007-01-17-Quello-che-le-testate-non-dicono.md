---
title: "Quello che le testate non dicono"
date: 2007-01-17
draft: false
tags: ["ping"]
---

Il seguente articolo mi è stato censurato da una testata e cos&#236; lo ripropongo qui nella sua versione originale. Mi perdonerai se non lo internetizzo&#8230; ma raggiungere il link non è difficile.

-------

Quando la concorrenza tra sistemi operativi lambisce i confini del lecito

Interfacce di bronzo

L'innovazione del vicino di desktop spesso è più verde

La polemica più recente è stata lanciata da David Pogue, opinionista tecnologico del New York Times, che in un filmato (1) pubblicato online lo scorso 14 dicembre ha satireggiato sull'aspetto di Vista, l'atteso aggiornamento di Windows il cui sviluppo, secondo le parole dello stesso Pogue, ha richiesto cinque anni di sviluppo, cinquanta milioni di righe di codice e una serie infinita di ritardi.
Il filmato di Pogue fa riflettere per il suoi sarcasmo. Le opzioni più evidenti di Vista vengono messe a confronto con quelle di Mac OS X di Apple mentre il commentatore, ironicamente, fa notare come le due interfacce siano &#8220;diverse&#8221; dal momento che la ricerca di Mac OS X si trova nell'angolo in alto a destra mentre quella di Windows si apre da quello in basso a sinistra (ma l'aspetto è praticamente identico), oppure per il fatto che i miniprogrammi di Mac OS X si chiamano widget laddove quelli di Vista, a dire poco gemelli, hanno il nome di gadget.
Considerato che Mac OS X ha presentato le proprie funzioni tra il 2000 e il 2005 e Vista le ripresenta di fatto analoghe con molti mesi di ritardo, in qualche caso anni, viene da chiedersi se nel campo delle interfacce grafiche non esista quella protezione del copyright e quella brevettabilità da tempo al centro del dibattito in campi come la musica digitale.
La situazione tra Apple e Microsoft è tuttavia del tutto anomala e dipende da un accordo stipulato tra due società nel tecnologicamente parlando lontanissimo 1985 tra Bill Gates e John Sculley, allora amministratore delegato di Apple.
Il Macintosh era da un anno l'unico computer con interfaccia grafica, mouse e finestre, mentre i PC erano ancora fermi al DOS, con la riga di comando, e Windows era poco più che un prototipo, che però rischiava di scatenare le reazioni legali di Apple.
Gates minacciò di cessare lo sviluppo di Excel per Macintosh, allora come oggi software irrinunciabile per quasi tutti, e riusc&#236; a strappare un accordo che lasciava a Microsoft la possibilità di riprodurre l'interfaccia di Apple in cambio di un ritardo nella commercializzazione di Excel per PC, che peraltro non sarebbe stato pronto per ancora molti mesi.
Apple perse poi una causa cardine nel tentativo di fare dichiarare nullo l'accordo e da allora Microsoft insegue regolarmente, e legalmente, con Windows le innovazioni introdotte man mano da Apple nei suoi sistemi operativi.
Il filmato di David Pogue non è certo l'unico frutto paradossale di questo stato di cose. L'ultima versione del sistema operativo Mac è stata pubblicizzata da Apple con lo slogan Redmond, accendere le fotocopiatrici (a Redmond, stato di Washington, risiede il campus centrale di Microsoft) e l'estate scorsa Steve Jobs, nell'annunciare i primi dettagli del prossimo aggiornamento a Mac OS X, denominato Leopard, ha dichiarato apertamente che avrebbe tenuto nascoste le novità dell'interfaccia, per non permettere a Microsoft di inserirle in Vista all'ultimo momento. E l'inseguimento continua, all'ultimo clic.

Lucio Bragagnolo

LINK
(1) http://snurl.com/17lkj
