---
title: "Ritorna in squadra il pivot?<p>"
date: 2005-04-20
draft: false
tags: ["ping"]
---

Da un pettegolezzo senza fondamento una domanda: non è che per caso Tiger&hellip;?<p>

Sembra, pare, si dice, che Tiger porti su Mac OS X il <a href="http://www.tuaw.com/2005/04/19/the-earth-rotates-why-shouldnt-your-monitor/">supporto</a> per i monitor pivotabili, che se li ruoti l&rsquo;immagine si adegua alla nuova posizione dello schermo.<p>

Il che spiegherebbe una volta di più perché Steve Jobs non ha rilasciato il codice di Newton pur avendo ucciso i MessagePad. Un po&rsquo; alla volta, tutte quelle tecnologie stanno tornando, come ha già fatto Ink per le tavolette grafiche.<p>

Alla fine Newton era dieci anni avanti. Come fai a sopportare quegli squallidi palmari di oggi?<p>

Grazie a <strong>Dani</strong> per la segnalazione!<p>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>