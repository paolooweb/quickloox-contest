---
title: "Apple stracciona"
date: 2008-01-20
draft: false
tags: ["ping"]
---

È il commento più gentile che ho sentito all'annuncio che i possessori dei vecchi iPod touch possono montare Mail, Maps, Stocks, Notes e Weather e proprio come gli utenti di iPhone, ma devono pagare 17,99 euro per l'aggiornamento. Mentre gli iPod touch che escono oggi di fabbrica hanno le stesse applicazioni di serie e in Europa il touch è stato perfino ribassato.

Il fatto è che Apple può essere forse stracciona perché decide che sono 17,99 euro e non, che so, 7,99 euro, o 0,99 euro (ma il software non ha proprio mai alcun valore?). Non per il fatto di fare pagare l'update.

La spiegazione sta nella legge chiamata Sarbanes-Oxley Act, del 2002. Moltissimo in breve, iPhone e Apple Tv sono formalmente prodotti il cui prezzo è un abbonamento. I soldi spesi per una Apple Tv comprendono già i servizi possibili sull'hardware ma non ancora forniti dal software. Quando arriva il software che abilita quei servizi, quel software è gratuito. Ecco perché l'aggiornamento ad Apple Tv è gratuito. Lo stesso vale per iPhone.

Il prezzo di iPod touch non è un abbonamento. Dunque Apple è obbligata, per legge, a fare pagare un aggiornamento di questo tipo.

Si può discutere sul tanto-o-poco. Non sul pagare.