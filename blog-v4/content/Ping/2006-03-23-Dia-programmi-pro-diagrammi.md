---
title: "Dia programmi pro diagrammi"
date: 2006-03-23
draft: false
tags: ["ping"]
---

Sar&agrave; il momento primaverile, ma ogni tre secondi scopro un programma nuovo. <a href="http://www.aisee.com/" target="_blank">Questo</a> &egrave; una delle soluzioni pi&ugrave; complete al problema della rappresentazione diagrammatica che abbia mai incontrato.

Per ora si usa su X11 ma la versione nativa Aqua &egrave; in lavorazione (e annunciata). aiSee &egrave; libero per uso non commerciale, anche se c&rsquo;&egrave; da litigare un po&rsquo; con le procedure per registrarlo. Da tenere presente.