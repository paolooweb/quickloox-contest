---
title: "Più a caso è meno a caso<p>"
date: 2005-09-12
draft: false
tags: ["ping"]
---

Bell&rsquo;esempio della distanza tra l&rsquo;informatica e la gente comune<p>

Sono all&rsquo;hotel Diana Majestic di Milano, gentilmente invitato da Apple alla prima incarnazione italiana di iPod Nano. Vedo il dimostratore che illustra la funzione di ascolto casuale dei brani in iTunes 5, riveduta e ampliata dalle versioni precedenti e ora ribattezzata casualità intelligente o qualsiasi altra traduzione sia stata scelta per Smart Random. La nuova funzione ora consente maggiore controllo sulla possibilità che una canzone venga suonata due volte di seguito, e permette di impostare l&rsquo;ascolto casuale anche sulla base degli album o delle playlist.<p>

Viene da sorridere pensando a Steve Jobs che commentava la nuova funzione raccontando che <cite>people think is more random this way, but is less random indeed</cite>. Non proprio testuale, ma il concetto è questo. La gente pensa che così sia più casuale, ma in realtà lo è meno.<p>

L&rsquo;informatica come scienza, senza scomodare la matematica che sul concetto di random avrebbe un bel po&rsquo; da dire, è distantissima dal pensiero comune. Per fortuna c&rsquo;è una Apple che la rende piacevole per tutti, al punto che possiamo ascoltare la nostra musica da iTunes o iPod (magari nano) esattamente come la vogliamo. E senza preoccuparci di quello che è casuale o meno.<p>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>