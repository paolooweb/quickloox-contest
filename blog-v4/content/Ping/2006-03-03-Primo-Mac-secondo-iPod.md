---
title: "Primo Mac, secondo iPod"
date: 2006-03-03
draft: false
tags: ["ping"]
---

Chi si ricorda del concorso di <a href="http://www.sourcesense.com" target="_blank">Sourcesense</a>? &Egrave; una societ&agrave; di open source che ha indetto una gara pubblica a chi gli disegna il logo migliore, mettendo in palio per il vincitore un MacBook Pro.

Beh, hanno aggiunto un iPod nano per chi arriva secondo. La cosa non motiva maggiormente un grafico professionista, immagino, ma un dilettante come me user&agrave; sicuramente un momento libero del weekend, in tempo per la scadenza finale del 7 marzo.