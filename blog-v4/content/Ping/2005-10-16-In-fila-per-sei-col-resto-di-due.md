---
title: "In fila per sei col resto di due<p>"
date: 2005-10-16
draft: false
tags: ["ping"]
---

Il gusto di usare un sistema in modi non ortodossi<p>

Non fare domande. Mi è toccato di creare, al volo, un gioco enigmistico. Scrivere una frase su più righe, prendere ogni colonna di caratteri, mescolare la successione delle colonne e chiedere al lettore di rimettere tutto a posto per ricostruire la frase originale.<p>

Normalmente i giochi enigmistici si realizzano con programmi appositi, appoggiati a dizionari. Questa era una eccezione; sporco, maledetto e subito.<p>

Pages. La gestione delle tabelle di Pages è molto pratica ed efficiente. Una frase scritta su più righe, di cui vogliamo mischiare le colonne, è nient&rsquo;altro che una tabella con una lettera per casella.<p>

In più Pages pesa niente rispetto a elefanti come Word o Excel.<p>

Una volta di più mi ha sorpreso. Positivamente.<p>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>