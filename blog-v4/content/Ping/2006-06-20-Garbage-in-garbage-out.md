---
title: "Garbage in, garbage out"
date: 2006-06-20
draft: false
tags: ["ping"]
---

Per quanto in inglese e lunghetto, <a href="http://www.roughlydrafted.com/RD/Home/D95D14AF-DA69-4F2B-850D-A3EB6C8C90C0.html" target="_blank">questo pezzo</a> spiega bene la genesi di molte delle scempiaggini che si leggono nell&rsquo;ambito Mac. Una sintesi molto elastica, ma fedele:

<cite>Un articolista si imbatte in una non-notizia. La monta in modo assurdo speculandoci sopra, dal niente. Scrive un articolo che presenta le sue speculazione come una battaglia epica fra il bene e il male. Arrivano stormi di lettori increduli. I quali&hellip;</cite>

<cite>&hellip;finiscono su un sito ritenuto attendibile, che d&agrave; l&rsquo;impressione di avere un lavoro di documentazione dietro i propri articoli. Il sito presenta un&rsquo;idea vaga come se fossero fatti basati su lavoro giornalistico. I lettori confondono le speculazioni con vero lavoro giornalistico. I blogger iniziano a linkare come pazzi, facendo pubblicit&agrave; al tutto e perpetuando il ciclo.</cite>

Mi piacerebbe che fosse scritto per legge sull&rsquo;intestazione di certi siti.