---
title: "Anzi, da prestazione"
date: 2009-02-12
draft: false
tags: ["ping"]
---

Le lezioni di Impara a suonare, la nuova funzione di GarageBand '09, si vedono tranquillamente anche con un Mac PowerPc. Si trovano dentro <code>/Libreria/Supporto Applicazioni/GarageBand/Learn to Play/Basic Lessons</code> e con un doppio clic da l&#236; partono anche se la funzione sarebbe solo per processori Intel.

È vero invece che il programma installa solo la prima lezione per piano e la prima per chitarra, e che per scaricare le altre serve, s&#236;, un Mac Intel. Però poi i file possono essere trasferiti nella cartella suddetta del Mac PowerPc.

Avvertenza: si capisce bene che il vincolo a Intel è prestazionale. Con un G5 tutto sommato le lezioni funzionano bene. Con un G4, beh, insomma.