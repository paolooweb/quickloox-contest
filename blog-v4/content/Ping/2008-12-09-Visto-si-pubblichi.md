---
title: "Visto si pubblichi"
date: 2008-12-09
draft: false
tags: ["ping"]
---

Ho avuto il privilegio di vedere in anteprima OpenOffice 3.0 Aqua per PowerPc. È lento come me lo aspettavo, quindi lento ma usabilissimo; molto pulito rispetto alle versioni precedenti di OpenOffice e molto stabile.

Sinceramente, non capisco come si possa pensare di spendere del denaro per Office avendo a disposizione gratuitamente una applicazione come questa. Ora, semmai, è tempo di superare certi screzi personali e avviare un bel progetto di fusione tra OpenOffice e NeoOffice, al fine di avere finalmente su Mac la <em>suite open source</em> più potente del mondo e anche ottimizzata.