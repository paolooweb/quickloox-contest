---
title: "It's Only Chat &#38; Roll (But I Like It)"
date: 2009-09-09
draft: false
tags: ["ping"]
---

Dai, che stasera <a href="http://www.pocket-lint.com/news/26977/the-beatles-heading-to-itunes" target="_blank">annunciano i Beatles su iTunes Store</a>.

Dai, che stasera <a href="http://blogs.ft.com/techblog/2009/09/apple-might-offer-a-cocktail-of-new-ipods-and-music/" target="_blank">NON annunciano i Beatles su iTunes Store</a>.

(sono bravo, eh? Due notizie importanti. E ci ho preso di sicuro!).

È solo l'ennesima occasione di farsi una chiacchierata&#8230; e la colgo.

Chi c'è e cerca un'alternativa all'aperitivo: appuntamento dalle 18:29 in avanti su <code>irc.freenode.net</code> per dare il comando <code>/join #freesmug</code> ed entrare in chat Irc. Sarò l&#236; e commenteremo gli esiti dell'evento musicale Apple di stasera. Per gli arrugginiti di Irc, <a href="http://www.macworld.it/blogs/ping/?p=2447" target="_blank">a partire da qui</a> ci sono tutte le indicazioni che servono.

Chi fosse arrugginitissimo con Irc si colleghi con iChat alla stanza <code>itsonlyrockandroll</code>, che risolviamo al volo eventuali problemi di collegamento.

Il titolo dell'evento, comunque, <a href="http://www.rollingstones.com/discog/index.php?v=so&amp;a=1&amp;id=175" target="_blank">arriva dai Rolling Stones</a>. Tutt'altro ritmo. :-)

Ancora una volta, grazie a Gand di <a href="http://freesmug.org" target="_blank">Freesmug</a>!