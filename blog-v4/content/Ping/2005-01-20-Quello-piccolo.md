---
title: "Quello piccolo<p>"
date: 2005-01-20
draft: false
tags: ["ping"]
---

Forse sta per accadere qualcosa di nuovo sul mercato<p>

In <a href="http://www.macatwork.net">Mac@Work</a> (l&rsquo;Apple Center di cui sono socio e se ti sembra pubblicità indebita pensalo tranquillamente, non c&rsquo;è problema) è entrato un signore. Si è avvicinato e ha chiesto <em>senta, io vorrei un computer, quello piccolo</em>.<p>

Questo signore non sapeva niente di computer e ha visto Mac mini da qualche parte, probabilmente al Tg5.<p>

Ne ho visto personalmente uno solo di signore così, ma se fossero tanti, e niente vieta che lo siano, Apple potrebbe avere estratto un coniglio dal cilindro.<p>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>