---
title: "Learning to Mac<p>"
date: 2005-05-11
draft: false
tags: ["ping"]
---

Nascono nuovi siti per imparare e saperne sempre di più. In bocca al mouse!<p>

Mi arriva la notizia della messa in onda di <a href="http://www.imparamac.com/">ImparaMac</a>, sito con lo scopo di diffondere conoscenza sulla Mela e di essere punto di riferimento per i seminari gratuiti realizzati, a Treviso e dintorni, dagli organizzatori.<p>

A leggere il comunicato appare che ci sarà da studiare ma anche da divertirsi, perché il programma prevede anche una buona mangiata quando ci vuole!<p>

A partire dal primo appuntamento di martedì prossimo 17 maggio, alle 20:30, di cui puoi vedere tutti i dettagli sul sito.<p>

Complimenti per l&rsquo;iniziativa e che ne seguano cento altre!<p>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>