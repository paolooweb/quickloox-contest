---
title: "Stormrage is Full"
date: 2008-11-16
draft: false
tags: ["ping"]
---

<cite>Stormrage è pieno</cite>. Coda di ingresso di quattro minuti, dietro a oltre cento altri pretendenti.

Era da un bel po' di tempo che non vedevo questo messaggio al momento di entrare in <a href="http://www.wow-europe.com" target="_blank">World of Warcraft</a>.

Mi sa che con il varo della nuova espansione <em>Wrath of the Lich King</em>, Blizzard ha fatto ancora una volta il botto (quella precedente, <em>The Burning Crusade</em>, è stata il gioco più venduto del 2007 e il gioco venduto più in fretta di sempre). Al momento creare un Death Knight non rientra nei miei progetti, ma c'è tempo per godersi tutto.

Per la cronaca, Wrath appare perfettamente giocabile sul mio PowerBook G4. Non ho ancora visitato il nuovo continente di Northrend, però.