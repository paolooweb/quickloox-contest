---
title: "In tutta sicurezza"
date: 2006-09-24
draft: false
tags: ["ping"]
---

Anche l'ultimo Security Update è andato a buon fine e il Mac si è riavviato regolarmente senza alcun problema.

Non sto a riaggiornare il conto dei riavvi non voluti (questo era voluto e va tutto bene). Piuttosto, da inizio anno sono arrivati numerosi aggiornamenti tra sistema, security e applicazioni. SUl mio singolo Mac ha funzionato tutto senza problemi. Eppure, a leggere <a href="http://www.macfixit.com/" target="_blank">MacFixit</a>, si trovano mezzi bollettini di guerra.

Se veramente c'è qualcosa di sbagliato negli update, il mio Mac non potrebbe funzionare bene dopo averli applicati. Deve evidentemente esserci qualcosa di sbagliato nei Mac che danno problemi.