---
title: "Il mare di primavera"
date: 2007-04-27
draft: false
tags: ["ping"]
---

Al ritorno da qualche giorno di mare, posso dire che mi sono collegato. Poco. Per poco tempo. Ma diverse volte, con il maledetto cellulare Bluetooth, e diversamente da altre volte nel passato ha sempre funzionato, senza bisogno di riavvii.

Mi sono anche concesso un'oretta di World of Warcraft. Se la cella non è piena di gente, se la cava bene anche Edge.