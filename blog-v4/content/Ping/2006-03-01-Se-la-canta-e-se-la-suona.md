---
title: "Se la suona e se la canta"
date: 2006-03-01
draft: false
tags: ["ping"]
---

<a href="http://www.accomazzi.net" target="_blank">Akko</a> mi segnala le <a href="http://news.yahoo.com/s/nm/20060301/wr_nm/summit_napster_dc" target="_blank">dichiarazioni</a> del grande capo di Napster, Chris Gorog, sul panorama attuale delle vendite di musica digitale.

Gente veramente messa male. Se finora non è andata come si sperava, è colpa di Microsoft, si raccontano, e poi comunque la supremazia di iTunes Music Store non durerà per sempre. Le stesse rosicate di Bill Gates, qualche mese di supremazia di iTunes dopo.

La verità è una sola: ci sono quelli per cui i computer sono tutti uguali e dunque ti conviene comprare un cassone orrendo che costi poco. Ma quando si tratta di portarsi la propria musica con sé, improvvisamente il concetto non funziona più. Chissà perché.