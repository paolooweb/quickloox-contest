---
title: "Senza parole (ma con l&rsquo;orologio)"
date: 2006-03-23
draft: false
tags: ["ping"]
---

<a href="http://www.macitynet.it/macity/aA24197/index.shtml" target="_blank">Macity</a> si &egrave; accorta che Steve Jobs sta vendendo parte delle sue azioni Apple e scrive che la notizia &egrave; apparsa on line <em>qualche minuto fa</em>.

Peccato che l&rsquo;indirizzo presente sul mio Ping sia consultabile da marted&igrave; 21. Non bastasse, questo <a href="http://finance.messages.yahoo.com/bbs?action=m&amp;board=4686874&amp;tid=aapl&amp;sid=4686874&amp;mid=748622" target="_blank">commento</a> &egrave; datato alla mattina americana, quindi al pomeriggio italiano, di mercoled&igrave; 22.

Arrivare in ritardo capita a tutti, ma dire le bugie&hellip; e neanche spiegano il perch&eacute; della vendita.