---
title: "Un volto inquietante, un computer speciale<p>"
date: 2005-09-04
draft: false
tags: ["ping"]
---

A caccia di screensaver, eccone uno suggestivo e assai indicativo<p>

Vicki Screen Saver è anche un po&rsquo; inquietante, ma alla fine affascina e come salvaschermo non è affatto male. Più di questo, una piccola riflessione: effetti come quello che vedrai, oggi, su Windows sono impossibili. Bisogna aspettare Vista, previsto per fine 2006.<p>

Giusto perché ogni tanto si sente qualcuno venire fuori con la storia che i computer sono t tutti uguali.<p>

Ah, l&rsquo;effetto di <a href="http://www.skyy.0nyx.com/Quartz3.html">Vicki</a> si può avere anche come sfondo scrivania, a patto di installare <a href="http://www.fourminutemilesoftware.com/quartzdesktop/">Quartz Desktop</a>.<p>

Grazie a Filippo per la segnalazione!<p>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>