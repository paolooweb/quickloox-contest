---
title: "Ingenui e candidi"
date: 2008-09-09
draft: false
tags: ["ping"]
---

Sento qualche ingenuo borbottare che <a href="http://www.google.com/chrome" target="_blank">Chrome</a>, il nuovo <em>browser</em> di Google, al momento esiste solo per Windows.

Basta ricordare che l'<em>open source</em> non è ingenuo, ma è candido. Candido vuol dire che, siccome Chrome è basato sul motore <em>open source</em> <a href="http://webkit.org/" target="_blank">WebKit</a> che aziona Safari, tutto quello che Google applica al motore in funzione di Chrome è a disposizione degli sviluppatori di Safari, che possono decidere liberamente - cos&#236; come qualsiasi programmatore nel mondo - di adottare, non adottare, migliorare, affiancare, qualunque cosa.

Per esempio, il Safari in lavorazione incorpora il velocissimo motore JavaScript <a href="http://webkit.org/blog/189/announcing-squirrelfish/" target="_blank">SquirrelFish</a>. Chrome, invece, incorpora l'altrettanto velocissimo motore <a href="http://code.google.com/p/v8/" target="_blank">V8</a>.

Sia V8 che, per fare un altro esempio, la <a href="http://www.atoker.com/blog/2008/09/06/skia-graphics-library-in-chrome-first-impressions/" target="_blank">libreria grafica Skia</a> di Chrome stanno già per entrare nel repository principale di WebKit. È solo questione di tempo e di decisione su quali siano le scelte migliori per tutti.

In altri termini, che i vantaggi di Chrome arrivino su Safari è solo questione di tempo. Mentre non arriveranno mai su Internet Explorer. I cui ignari utenti, destinati anche stavolta a restare arretrati, sono i veri ingenui di tutta la partita.