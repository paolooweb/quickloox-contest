---
title: "L&rsquo;atto di Babele<p>"
date: 2005-07-24
draft: false
tags: ["ping"]
---

Dall&rsquo;alto della torre la visibilità è maggiore e le decisioni sono meditate<p>

Testuale. <cite>Babele Dunnit ammette la superiorità di Apple</cite>.<p>

Per chi conosce la persona è un momento significativo. Per chi non la conosce, è il momento di una esperienza di <a href="http://www.babeledunnit.org">allargamento delle vedute</a>.<p>

Grazie a <a href="http://www.maestrinipercaso.it">Mafe e Vanz</a> per la segnalazione!<p>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>