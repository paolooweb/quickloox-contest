---
title: "Dare a Google quel che è di Google<p>"
date: 2005-10-23
draft: false
tags: ["ping"]
---

La casella di posta gratuita migliore che c&rsquo;è in giro<p>

Senza nulla togliere ai vari e numerosi vantaggi di .mac, non farei (e non faccio) a meno di una casella postale gratuita su <a href="http://mail.google.com/mail/">Gmail</a>. Lo spazio a disposizione è in continua crescita, è semplice da usare, funziona con Safari, adesso c&rsquo;è un servizio di notifica anche per Mac, GoogleTalk è compatibile con iChat&hellip; senza contare che il Mac rimane sempre libero e non si occupa disco rigido.<p>

Se hai bisogno, scrivimi che ti invito.<p>

<a href="mailto:lvcivs@gmail.com">Lucio Bragagnolo</a>