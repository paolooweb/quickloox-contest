---
title: "Cronache del Bar Sport"
date: 2010-04-07
draft: false
tags: ["ping"]
---

<b>Paolo Attivissimo</b> su iPad nel 2010:

<a href="http://attivissimo.blogspot.com/2010/01/aiped-aipad-aipod.html" target="_blank">Niente Flash. Niente multitasking. Niente webcam. Batteria sigillata. Niente Usb. Dipende da un Pc per i backup. Solo software autorizzato da Apple.</a> <a href="http://attivissimo.blogspot.com/2010/04/ipad-keynote-incompatibile-con-keynote.html" target="_blank">Sotto il sole diretto si spegne</a>. <a href="http://attivissimo.blogspot.com/2010/04/ipad-jailbreak.html" target="_blank">Il <i>case</i> di alluminio si graffia</a>.

Il Bar Sport Internet su iPhone nel 2007:

Niente Flash. Niente multitasking. Niente webcam. Batteria sigillata. Niente Usb. Dipende da un Pc per i backup. Solo software autorizzato da Apple. Sotto il sole diretto si spegne. Il <i>case</i> di alluminio si graffia.

Come tutti sanno, la truffa è stata scoperta e oggi nessuno più usa iPhone.

Le idee vecchie, se non vengono ripulite con un po’ di senso critico, mostrano la polvere.