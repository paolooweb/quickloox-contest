---
title: "L&rsquo;ultima apertura<p>"
date: 2005-08-09
draft: false
tags: ["ping"]
---

Quel file è già nel Cestino, ma gli si voleva dare l&rsquo;ultima occhiata&hellip;<p>

Un file nel Cestino non si può aprire con un doppio clic. Bisogna tirarlo fuori per aprirlo. Se poi era proprio l&rsquo;ultima occhiata, per essere sicuri, c&rsquo;è da rimetterlo nel Cestino. Noioso.<p>

Ma c&rsquo;è qualche trucco. Lancia un programma in grado di aprire il file desiderato, che so, Anteprima se si tratta di un&rsquo;immagine. Dai il comando Apri e, quando appare la finestra di dialogo, premi Comando-Maiuscolo-G. Scrivi <code>~/.Trash</code> e clicca Vai. Appare l&rsquo;elenco dei file nel Cestino. Scegli quelli che vuoi ed ecco che si aprono.<p>

Variante: trascina l&rsquo;icona del file sul Dock, sull&rsquo;icona di un programma che lo possa aprire. Stesso risultato.<p>

Niente di che, ma perché sprecare tempo, anche se poco?<p>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>