---
title: "Un bel tacer"
date: 2010-12-02
draft: false
tags: ["ping"]
---

Ho iniziato il mio annuale digiuno di scrittura sulle <i>mailing list</i>, dove tradizionalmente l'abitudine porta a scrivere più del necessario da una parte, e quando non sarebbe necessario dall'altra.

Riprenderò a scrivere, se necessario, dopo l'Epifania.

È importante ricordare che il messaggio più efficace resta quello che non viene scritto, anche se la frase sa di Zen da salotto.