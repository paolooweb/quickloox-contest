---
title: "AppleScript e una data imperdibile"
date: 2008-05-05
draft: false
tags: ["ping"]
---

Va bene il digitale, ma a volte un bel foglio di carta non lo batte nessuno.

Per questo a qualcuno può venire buono un semplice AppleScript che stampa l'agenda del giorno.

Fuori Script Editor e cominciamo

<code>tell application "iCal"</code>
Ordina a iCal di&#8230;

<code>	view calendar at (my (current date))</code>
	&#8230;impostare la vista sulla data odierna

<code>	switch view to day view</code>
Questo è un comando ApplesScript eccezionale. Significa <em>commuta la vista alla vista per giorno</em> ed è linguaggio naturale al cento percento. Inglese a parte, per dare un comando del genere non c'è alcun bisogno di essere programmatori.

<code>	activate</code>
	Il solito comando di attivazione, per dire a iCal <em>forza, al lavoro</em>.

<code>	tell application "System Events"</code>
	E adesso un altro ordine per un'altra applicazione. Non troveremo nel Mac un'icona con questo nome; si tratta di un'applicazione virtuale per permettere ad AppleScript di svolgere attività come&#8230;

<code>		keystroke "p" using command down</code>
		&#8230;premere Comando-P sulla tastiera. Letteralmente c'è scritto <em>tasto</em> P <em>con Comando abbassato</em>.

<code>		delay 1 -- (secondi)</code>
		Ferma lo script per un secondo, dato che una vera pressione del tasto non sarebbe certo istantanea.
		
<code>		keystroke return</code>
		E questa è la resa in AppleScript di <em>ora il tasto</em> P <em>non è più premuto</em>.

<code>		delay 2 -- (secondi)</code>
		Un altro ritardo, per il tasto Comando.

<code>		keystroke return</code>
		Anche lui ora non più premuto.
		
<code>	end tell</code>
	Fine del primo blocco di comandi tell
	
<code>	quit</code>
	Chiudo iCal, ora che è servito.
	
<code>end tell</code>
Fine del secondo blocco tell e dello script.

Qui sotto c'è il listato completo. È uno script molto elegante per semplicità e per concretezza del risultato. E insegna come fare premere ad AppleScript la tastiera.

<code>tell application "iCal"
	view calendar at (my (current date))
	switch view to day view
	activate
	tell application "System Events"
		keystroke "p" using command down
		delay 1 -- (secondi)
		keystroke return
		delay 2 -- (secondi)
		keystroke return
	end tell
	quit
end tell</code>

L’autore dello script è il bravo Cory Bohon di <a href="http://www.tuaw.com/2008/04/28/applescript-print-daily-ical-agenda/" target="_blank">The Unofficial Apple Weblog</a>.