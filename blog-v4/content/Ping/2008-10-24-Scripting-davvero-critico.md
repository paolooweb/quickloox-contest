---
title: "Scripting davvero critico"
date: 2008-10-24
draft: false
tags: ["ping"]
---

Quando pochi giorni fa cianciavo di scripting, non mi ero minimamente accorto che MacRuby è un progetto a sé stante e non una qualche raccolta di roba utile come, che so, AppleScript Studio. Con <a href="http://www.macruby.org/trac/wiki/MacRuby" target="_blank">il suo sito</a>, i suoi tutorial, la sua comunità.

È una gran cosa, perché Ruby è un linguaggio eccellente per diventare produttivi con la massima rapidità e oltretutto leva di torno molti ostacoli che tipicamente affliggono chi inizia a programmare.