---
title: "Quanti buoni auguri"
date: 2006-04-16
draft: false
tags: ["ping"]
---

&Egrave; Pasqua e sulla prima pagina del Sole 24 Ore figura Steve Jobs, a illustrazione della notizia che Apple &egrave; stata proclamata societ&agrave; pi&ugrave; innovativa del 2006.

Auguri che sia cos&igrave; a lungo. Auguri a tutta la stampa non specializzata di riuscire sempre pi&ugrave; spesso a dare le notizie invece di inventarle. Auguri a tutti di sentire dibattiti sulla portata dell&rsquo;innovazione di Apple al posto di lamenti sul prezzo dei Mac.

Auguri a tutti. :-)