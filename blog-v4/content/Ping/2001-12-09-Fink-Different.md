---
title: "Fink Different"
date: 2001-12-09
draft: false
tags: ["ping"]
---

Come si vede che Mac OS X ha radici Unix! Sta iniziando infatti a diffondersi un fenomeno che prima era quasi inesistente: l1arrivo dell1open source su Mac.
Open source è ben diverso da freeware e impone anche un cambio di filosofia. Bellissimo ricevere gratis software di ogni tipo, tuttavia sono moralmente impegnato a dare qualcosa in cambio.
Su tutti si distinguono i ragazzi di Fink, impegnati a modificare quel tanto che basta il software per Linux in modo che possa funzionare sotto Mac OS X oppure, dove non si può arrivare oltre, sotto Darwin con installazione di X Window, che è già una gran cosa.
Sono tutti concetti troppo grandi per una rubrica così piccola. Ma basta ricordarsi una cosa: visitare Fink e cercare di capire. Chi non ci riesce non si preoccupi: c1è tutto il tempo. Chi capisce, sfrutti tutto quello che trova e intanto si chieda che cosa può dare in cambio, rispetto alle sue capacità.
L1open source è un fenomeno da non trascurare. Trust me.

lux@mac.com

<http://fink.sourceforge.net>