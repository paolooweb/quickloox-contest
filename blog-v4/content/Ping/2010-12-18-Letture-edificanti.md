---
title: "Letture edificanti"
date: 2010-12-18
draft: false
tags: ["ping"]
---

Arrivo probabilmente ultimo in questo universo, ma voglio ugualmente segnalare che, se di prende un iPhone/iPad/iPod touch e si apre iBooks, ci si collega a Store e si digita nel campo di ricerca <i>apple developer publications</i>, compaiono sei libri gratuiti di Apple su come programmare per iOS: <i>iOS Human Interface Guidelines</i>, <i>Object Oriented Programming With Objective-C</i>, <i>Cocoa Fundamentals Guide</i>, <i>The Objective-C Programming Language</i>, <i>iOS Technology Overview</i> e <i>iOS Application Programming Guide</i>.

Roba pesantissima e in inglese, da iniziare a leggersi proprio nelle vacanze?

Per il 99,999 percento di noi è da dimenticare. Uno si metterà a leggere. Poi magari aprirà una <a href="http://www.vivexsoftware.com/iphone/apps.html" target="_blank">software house</a> e nella pagina <a href="http://www.vivexsoftware.com/_/about.html" target="_blank">About</a> (informazioni) del suo sito si potrà leggere <cite>Vivex Software è una piccola azienda software, fondata nel maggio 2009 quando avevo solo quattordici anni</cite>.

Vivex Software è scozzese. Se un ragazzo italiano edificasse una attività analoga grazie a questo <i>post</i> la mia vita, lato informatico, avrebbe avuto un po' più significato.