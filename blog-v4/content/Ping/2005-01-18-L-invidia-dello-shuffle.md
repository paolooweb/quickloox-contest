---
title: "L&rsquo;invidia dello shuffle<p>"
date: 2005-01-18
draft: false
tags: ["ping"]
---

Il resto dell&rsquo;industria continua a non capire. Buon per Apple<p>

Prima di iPod tutti i lettori portatili di musica basati su hard disk avevano display microscopici, per contenere i costi, e interfacce orribili per cercare di svolgere le funzioni più svariate.<p>

Apple ha fatto <em>tabula rasa</em> con iPod, player con uno schermo grande (tanto costoso lo sarebbe stato comunque) e demandando tutte le funzioni di manipolazioni della musica a iTunes, sul computer.<p>

Gli stessi costruttori che non avevano capito l&rsquo;importanza dello schermo grande ora sostengono che iPod shuffle sarà irrilevante, perché non ha uno schermo.<p>

Insistono a non capire, questa volta che nessuna interfaccia è meglio di una interfaccia inadeguata come quelle che hanno tentato loro. <em>Tortured User Interface</em>, le ha chiamate Steve Jobs.<p>

Ai concorrenti fa invidia il fatto che Apple capisca. Ma Apple ci lavora sopra, loro no. Per costare meno.<p>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>