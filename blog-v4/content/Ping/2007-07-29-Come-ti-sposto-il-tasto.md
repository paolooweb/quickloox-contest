---
title: "Come ti sposto il tasto"
date: 2007-07-29
draft: false
tags: ["ping"]
---

Non mi era mai passato per l'anticamera del cervello prima, ma dopo avere visto come funziona <a href="http://scripts.sil.org/cms/scripts/page.php?site_id=nrsi&amp;item_id=ukelele" target="_blank">Ukelele</a> sto seriamente pensando di rimettere la chiocciola nella sua antica posizione in alto a sinistra, molto più immediata dell'orrendo Opzione-ò di adesso.