---
title: "Finestra con vista (su iPhoto)"
date: 2003-11-22
draft: false
tags: ["ping"]
---

Un altro modo di collaudare una presentazione

Tutti sanno che il pulsantino a forma di freccia “play” di iPhoto avvia una presentazione a tutto schermo dell’album selezionato.

Pochissimi sanno che che, facendo Ctrl-clic sul pulsantino da Panther in poi, la presentazione avviene in una finestra.

Utile per collaudare lo slideshow e intanto avere mano (e schermo) liberi per altre cose.

<link>Lucio Bragagnolo</link>lux@mac.com