---
title: "Alla ricerca di notizie sullo sviluppo"
date: 2006-11-01
draft: false
tags: ["ping"]
---

Nello scorso trimestre Apple ha speso in ricerca e sviluppo 179 milioni di dollari. Nello stesso trimestre dell'anno prima erano 147 milioni di dollari.

La buona notizia è che questa non è neanche la spesa totale in R&#38;D. Per fare un esempio, nel trimestre del giugno 2004 Apple ha documentato la spesa di cinque milioni di dollari per la ricerca mirata al lavoro per le versioni a venire di Mac OS X, il tutto extra rispetto al budget R&#38;D prefissato.

Quando Apple spende tanto e bene in ricerca e sviluppo è cosa buona per tutti. Da noi che usiamo prodotti più innovativi fino all'industria tutta, che deve ingegnarsi ancora più per copiare decentemente.