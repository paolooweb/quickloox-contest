---
title: "Le ragioni della compatibilità"
date: 2009-08-30
draft: false
tags: ["ping"]
---

Occhio alle descrizioni dei programmi, perché <cite>Snow Leopard compatible</cite> è slogan diventato di moda e viene usato anche a sproposito.

Una applicazione cui è stato aggiunto il supporto dei 64 bit non è diventata compatibile Snow Leopard. Era compatibile anche prima e semplicemente avrebbe funzionato a 32 bit, come una settimana fa..

Altri software invece, come i salvaschermo o i <i>driver</i> delle stampanti, hanno veramente bisogno di un aggiornamento, o non girano.

Come scrivevo altrove, si è diffusa subito l'idea che se non fai il <i>boot</i> a 64 bit non vai bene. Non è affatto vero. La cosa migliore è fare il <i>boot</i> come ce lo propone il Mac, senza stare a rompersi la testa sui dettagli.

A meno che tu non sappia perfettamente il motivo che ti rende indispensabile il <i>boot</i> a 64 bit. In questo caso sarebbe sbagliato non farlo.

Se l'unica ragione per cui ti intriga il boot a 64 bit è che 64 è un numero più grosso di 32, non ne hai alcun bisogno.