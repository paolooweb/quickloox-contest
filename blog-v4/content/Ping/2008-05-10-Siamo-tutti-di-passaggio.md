---
title: "Siamo tutti di passaggio"
date: 2008-05-10
draft: false
tags: ["ping"]
---

C'è una ragione fondamentale per prepararsi a buttare definitivamente nel riciclatore i dischi di Microsoft Office e passare a OpenOffice.org 3.0, ora <a href="http://www.macworld.it/showPage.php?template=notizie&amp;id=14813" target="_blank">in beta pubblica</a> e a settembre in versione definitiva (spero anche per PowerPc).

OpenOffice 3.0 infatti inizia a supportare parzialmente Visual Basic per Applications, che Microsoft ha gentilmente sfilato da sotto i piedi di Office 2008 per Mac in cambio del supporto finto di AppleScript.

Se qualcuno si è fatto turlupinare da Microsoft con Office 2008 ha la possibilità di passare a software migliore, più libero e anche più compatibile con quello che gli arriva da colleghi e amici turlupinati del tutto (oltre che Office pure Windows, poveracci, e quando ne escono?).

In attesa che un giorno una futura versione OpenOffice.org supporti AppleScript e ancora più gente sia libera di passare al software libero.