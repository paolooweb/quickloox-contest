---
title: "La sete di Rosetta"
date: 2006-03-19
draft: false
tags: ["ping"]
---

Un MacBook Pro imbottito di Ram fino al limite massimo dei due gigabyte esegue Photoshop meglio di un PowerBook 17&rdquo; ugualmente imbottito. Su certe operazioni c&rsquo;&egrave; sostanziale parit&agrave;, su altre il MacBook Pro vince nettamente.

La discriminante &egrave; la Ram. Pi&ugrave; ce n&rsquo;&egrave;, meno influisce il collo di bottiglia di Rosetta.