---
title: "Più Trentanove"
date: 2008-03-17
draft: false
tags: ["ping"]
---

Sono i mesi di vita della vecchia batteria del PowerBook, che oggi è bruscamente caduta sotto i quindici minuti di autonomia (settimana scorsa faceva un'ora abbondante) nel viaggio di andata e ha fatto la stessa cosa nel viaggio di ritorno.

Arrivati ai trentasei mesi, a batteria ancora performante, avevo comunque acquistato il ricambio e ora sono già con la batteria nuova. Pronto per altri trentanove mesi. :-)

Tra parentesi, il Mac ha conservato lo stato di stop anche nei pochi secondi di totale assenza di corrente (né rete, né batteria) e, per l'elevato numero di lavori in corso non interrotti, si è comportato proprio bene.