---
title: "Tempi bellamente andati"
date: 2010-11-28
draft: false
tags: ["ping"]
---

Se The Sims 3 venisse distribuito su floppy disk, la sua confezione ne conterrebbe 1.760.

Servirebbero 36 floppy per installare Firefox e 358 per Photoshop Cs4.

iTunes 8 richiederebbe 46 floppy disk.

In alcune situazioni ho visto ancora usare floppy disk. E qualcuno confessa che Apple fece bene a fare uscire iMac privo di floppy disk a sancire ufficialmente che era finita un'epoca, ma lo fece troppo presto.

Invece non sarà mai troppo tardi per lasciarsi dietro le tecnologie obsolete. Il fatto di essere affezionati a un modo di lavorare e pretendere che non cambi mai più è una scusa per spegnere il pensiero attivo e accontentarsi. Invece l'informatica è ancora giovanissima e meno incrostazioni si porta dietro meglio è.

Per i nostalgici dei floppy ci sono <a href="http://www.behance.net/gallery/3_5-inch-poster-set/164212" target="_blank">questi meravigliosi poster</a>.