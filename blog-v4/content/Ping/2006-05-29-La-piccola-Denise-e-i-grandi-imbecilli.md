---
title: "La piccola Denise e i grandi imbecilli"
date: 2006-05-29
draft: false
tags: ["ping"]
---

Non c&rsquo;entra niente con il Mac, ma mi &egrave; gi&agrave; arrivata due volte la mail di richiesta di aiuto per una ragazza di nome Denise <cite>scomparsa ieri a Milano</cite>.

Non era una bufala, ma lo sta diventando. La vicenda si &egrave; <a href="http://attivissimo.blogspot.com/2006/05/denise-amadio-allarme-rientrato.html" target="_blank">conclusa (felicemente) dieci giorni fa</a> e fare girare adesso l&rsquo;appello significa solo dare in giro numeri di cellulare e nomi di gente che ora vorrebbe solo essere lasciata in pace.

Se conosci un imbecille che sta facendo girare la catena (oramai, ripeto, &egrave; una catena), fermalo. Le catene sono cose imbecilli e <a href="http://www.attivissimo.net/antibufala/che_male_fa.htm" target="_blank">se ne occupano imbecilli</a>. Senza eccezioni.