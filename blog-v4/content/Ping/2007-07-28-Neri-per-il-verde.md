---
title: "Neri per il verde"
date: 2007-07-28
draft: false
tags: ["ping"]
---

Leggo i commenti e mi sembra di vedere una gran confusione. L'inquinamento messo insieme al riscaldamento globale messo insieme al senso civico messo insieme a qualcos'altro. Buttare la cartaccia nel cestino (e io, piuttosto che fuori dal cestino, me la porto a casa) non salva il pianeta, ma solo il decoro urbano.

Ma divago. Siccome qui si parla di Mac e di computer, eccomi con una favoletta. La dedico a Rocco, che secondo lui certi ecologisti esistono <cite>solo negli articolini di varietà delle riviste di computer</cite>. (L'assassino di Pym Fortuyn in Olanda aveva vita difficile nei campeggi ecologici che frequentava, perché si opponeva strenuamente a che venissero uccise le zanzare.)

Ma divago. Ecco la favoletta, in tema con l'argomento di questo blog.

Qualcuno se ne esce a dire che, <a href="http://ecoiron.blogspot.com/2007/01/black-google-would-save-3000-megawatts.html" target="_blank">se Google usasse uno sfondo nero per la sua pagina</a>, si risparmierebbe una quantità consistente di energia elettrica ogni anno.

Sull'onda dell'entusiasmo, qualcun altro realizza una pagina di <a href="http://www.blackle.com" target="_blank">Google con lo sfondo nero</a>.

Qualcun altro, scienza e strumenti alla mano, spiega che l'idea è buona, ma vecchia: <a href="http://blogs.wsj.com/numbersguy/does-a-darkened-google-really-save-electricity-104/" target="_blank">valeva per chi usa i tubi catodici</a>. Con gli schermi a cristalli liquidi non è più cos&#236; e l'idea era meglio che venisse cinque anni fa.

Morale della favola: lo sviluppo sostenibile, per quanto può esserlo (nel lunghissimo termine lo stesso Sole terminerà il combustibile e nel farlo si espanderà fino a inglobare sfortunatamente il terzo pianeta), si raggiunge solo facendo avanzare la tecnologia.

Per chiudere il cerchio: qual è stata l'azienda che prima di tutti ha iniziato la transizione verso gli schermi Lcd? Toh. Apple.

Corollario: se qui gira gente che si professa militante ecologista e usa ancora un tubo catodico, magari in nome della difesa dell'ambiente, da oggi è tenuta agli sfondi neri.