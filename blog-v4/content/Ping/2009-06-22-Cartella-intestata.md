---
title: "Cartella intestata"
date: 2009-06-22
draft: false
tags: ["ping"]
---

Non si perde un colpo, qui, nel rendere in italiano la <a href="http://www.apple.com/it/macosx/refinements/enhancements-refinements.html" target="_blank">pagina sulle novità di Snow Leopard</a>, aspettando che lo faccia Apple.

<b>Intestazioni rimovibili nella barra laterale</b>

Se si eliminano gli elementi presenti sotto le intestazioni Dispositivi, Posizioni, Cerca, le intestazioni spariranno. Per reinserirle, è sufficiente trascinare un elemento nella barra laterale.

Il diavolo si nasconde nei dettagli, certo. Ma anche la tensione alla perfezione.