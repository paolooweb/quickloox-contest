---
title: "L&rsquo;art pour l&rsquo;art<p>"
date: 2005-11-01
draft: false
tags: ["ping"]
---

Dove si va per vedere il bello, meglio che sia bello tutto<p>

Sono reduce da una visita alla mostra sulla scultura italiana del Novecento, ospitata presso il neonato spazio della <a href="http://www.fondazionepomodoro.it">Fondazione Arnaldo Pomodoro</a>.<p>

È uno spazio immenso in via Solari a Milano, che prima ospitava una fonderia. Sono rimasti i camminamenti e le piattaforme in quota, tanto che uno dei piaceri più interessanti è poter finalmente osservare le sculture da angoli prima impossibili: dall&rsquo;alto, da sopra, da dietro&hellip; alla fine ritornare a terra è quasi una delusione, anche se temperata dalla soddisfazione di avere potuto provare l&rsquo;esperienza.<p>

Nello spazio adibito a biblioteca e consultazione ci sono due computer. In uno spazio espositivo di arte moderna. Di che marca saranno?<p>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>