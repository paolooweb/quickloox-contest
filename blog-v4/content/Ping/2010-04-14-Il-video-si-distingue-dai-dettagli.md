---
title: "La qualità si video dai dettagli"
date: 2010-04-14
draft: false
tags: ["ping"]
---

La storia del cambio di scheda video è l'esempio perfetto dell'informatica di oggi e della posizione di Apple nell'informatica.

L'antefatto è che <a href="http://www.apple.com/it/macbookpro/" target="_blank">i nuovi MacBook Pro 15&#8221; e 17&#8221;</a> contengono due schede grafiche, una potente e una economica, come i modelli precedenti. A differenza di questi, però, possono cambiare in corso d'opera la scheda grafica utilizzata, per puntare sulle maggiori prestazioni oppure sul minor consumo.

I MacBook Pro precedenti (come il mio, invidia, invidia) possono cambiare scheda grafica solo al termine di un <i>logout</i>.

Buona novità, quindi. Novità assoluta? Assolutamente no. Circolano da tempo numerosi altri sistemi simili su numerosi altri Pc.

E il mestierante conclude che <i>Apple è arrivata in ritardo, non è più innovativa come un tempo</i> (quando lui era giovane e vedeva il mondo con più entusiasmo), <i>Apple pensa solo a iPod e iPad</i>, <i>bla bla bla</i>. Internet è il paradiso di chi parla a basso costo.

Chi se ne intende, per esempio Ars Technica, <a href="http://arstechnica.com/apple/news/2010/04/inside-apples-automatic-gpu-switching.ars" target="_blank">ha messo a confront</a>o la soluzione dei MacBook Pro con le altre. Riassumo:

<cite>Amd/Ati usa un metodo che riconosce la presenza del cavo di alimentazione per attivare la scheda potente, mentre senza cavo attiva la scheda economica. Il problema è che, andando a batteria, si è costretti a usare la scheda economica anche se si vorrebbe diversamente.</cite>

<cite>Optimus di Nvidia usa una combinazione di hardware e software basata su un elenco che spiega al sistema quali applicazioni vogliono la scheda potente e quali quella economica. Esiste un elenco scaricabile da Internet e modificabile dall'utente. Il problema è che la scheda economica rimane alimentata e in funzione in qualsiasi momento. Cos&#236;, quando si vogliono le prestazioni, si usa una scheda sola &#8211; quella potente &#8211; ma il sistema ne utilizza e consuma energia per due, oltre ad aumentare senza vantaggi il traffico dati interno.</cite>

<cite>L'approccio dei MacBook Pro differisce da quello di Optimus in due modi fondamentali. Il primo è che al cambio di scheda provvede automaticamente il sistema senza intervento dell'utente (che può disattivare tutto nelle Preferenze di Sistema, se vuole). Le applicazioni che usano architetture grafiche esigenti come OpenGl, Core Graphics e Quartz Composer attivano automaticamente la scheda potente, le altre quella economica. Se si controlla la posta o si apre un foglio di calcolo, Mac OS X accende la scheda economica. Se si aprono Aperture o Photoshop, accende la scheda potente.</cite>

<cite>La seconda differenza fondamentale è che, quando una scheda è accesa, l'altra è spenta. Più efficiente di Optimus.</cite>

E qui Ars Technica condensa un quarto di secolo di informatica in quattro righe:

<cite>Apple attribuisce la propria soluzione all'attenzione ai dettagli in fatto di interazione (per quanto la sua prima soluzione non fosse cos&#236; precisa) e perché Apple, unica tra i costruttori di Pc, controlla sia hardware che software. Questo livello di integrazione è molto più difficile quando ci sono più costruttori a controllare parti differenti del sistema.</cite>

Lezione importante: in qualsiasi valutazione superficiale, Apple esce sempre perdente. Appena si approfondisce di un centimetro, ecco che emergono i dettagli.