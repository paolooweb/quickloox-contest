---
title: "Attulazzamento"
date: 2008-06-02
draft: false
tags: ["ping"]
---

La solita finta <strong><font color="#FF0033">Comunicazione Urgente</font></strong> da parte di CartaSì, inventata da qualche piratone russo per cercare di avere i dati della carta, oggi mi invitava ad <cite>attulazzare</cite> i dati dell’account.

Ecco il male della posta Html: la gente non vede risaltare il testo e viene confusa dalla veste grafica. A leggere solamente il testo, non farebbe clic neanche il gatto di casa.