---
title: "L&rsquo;elettricit&agrave; spiegata al profano"
date: 2006-04-13
draft: false
tags: ["ping"]
---

La superficie dell&rsquo;alimentatore di MacBook Pro sta a quella dell&rsquo;alimentatore del PowerBook Aluminum come il wattaggio richiesto dal primo sta a quello del secondo.