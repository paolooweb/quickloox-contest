---
title: "Autosmentita"
date: 2007-06-12
draft: false
tags: ["ping"]
---

Mi sono smentito nel giro di dodici ore. Ingolosito oltre ogni limite, ho installato e sto lavorando con <a href="http://www.apple.com/safari/" target="_blank">Safari 3</a>.

Per ora funziona tutto, ma ho fatto assai poco. Mi aspetto di dover seguire con attenzione <a href="http://webkit.org/blog/" target="_blank">Surfin' Safari</a>.