---
title: "…e la finestra non galleggia"
date: 2006-07-17
draft: false
tags: ["ping"]
---

Non più, grazie ai valorosi sforzi di <strong>Evthethinker</strong>, che ha creato <a href="http://millenomi.altervista.org/Afloat/" target="_blank">Afloat</a>. È un plugin di Mac OS X che permette a una finestra Cocoa di galleggiare in primo piano, qualunque cosa succeda nel sistema.

Dopotutto, se galleggiano i widget&#8230;

