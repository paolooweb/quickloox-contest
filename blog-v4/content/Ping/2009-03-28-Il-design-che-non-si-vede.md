---
title: "Il design che non si vede"
date: 2009-03-28
draft: false
tags: ["ping"]
---

Apple ha creato App Store e i programmatori che vogliono vendere le loro applicazioni via App Store possono aderire tramite una quota annua di 99 dollari. Dopo di che il programmatore decide il prezzo e si tiene il 70 percento delle vendite (eccettuate le applicazioni gratuite).

Tutto regolare e tutto scontato, ma forse non lo è.

Se un'applicazione è a pagamento, non può costare meno di 0,99 dollari. Evidentemente sotto questa soglia Apple non è in grado di fare funzionare in modo efficiente il meccanismo di pagamento.

Un programmatore ha libertà assoluta di preparare tutti gli aggiornamenti che desidera e sottoporre ad App Store tutte le applicazioni che vuole. Altrettanto evidentemente il meccanismo è in grado di gestire a perfezione queste dinamiche.

Adesso tutti cercano di imitare App Store.

App World, quello di BlackBerry, <a href="http://crackberry.com/2-99-minimum-paid-app-price-blackberry-app-world" target="_blank">avrà un prezzo minimo di 2,99 dollari</a>. Evidentemente la gestione è molto meno efficiente di quella di App Store.

Windows Mobile Marketplace ha una quota annua di 99 dollari e fino a cinque applicazioni gratis. Dopo di che, per ogni altra applicazione che il programmatore vuole vendere, deve sborsare altri 99 dollari.

Gli aggiornamenti alle applicazioni effettuati entro una settimana dalla pubblicazione dell'applicazione stessa sono gratis. Passata una settimana, <a href="http://news.cnet.com/8301-10805_3-10205583-75.html?part=rss&amp;subj=news&amp;tag=2547-1_3-0-20" target="_blank">contano come nuove applicazioni</a> e, superato il limite di cinque, aggiornare un'applicazione su Windows Mobile Marketplace costerà allo sviluppatore 99 dollari.

Evidentemente la possibilità di pubblicare applicazioni e aggiornamenti non è un meccanismo cos&#236; efficiente, in casa Microsoft.

La morale. Creare App Store e farlo funzionare bene non è affatto scontato. E poi si dice che il <em>design</em> è solo questione di estetica.