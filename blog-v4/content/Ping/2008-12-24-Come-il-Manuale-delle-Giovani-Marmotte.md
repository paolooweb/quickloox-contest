---
title: "Come il Manuale delle Giovani Marmotte"
date: 2008-12-24
draft: false
tags: ["ping"]
---

Sono cresciuto senza Internet e il Manuale delle Giovani Marmotte era indispensabile. Conteneva informazioni essenziali alla sopravvivenza di qualsiasi dodicenne, come la temperatura di fusione del rame oppure l'alfabeto militare internazionale.

Su Err the Blog hanno creato <a href="http://cheat.errtheblog.com/" target="_blank">un deposito di cheat sheet</a>, documenti di consultazione, accessibile in modo semplice da Terminale. C'è di tutto, dall'elenco dei sette nani allo schema dei permessi in Unix.

E anche <a href="http://cheat.errtheblog.com/s/military_alphabet/" target="_blank">l'alfabeto militare</a>.