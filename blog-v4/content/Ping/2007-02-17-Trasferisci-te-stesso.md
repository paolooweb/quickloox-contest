---
title: "Trasferisci te stesso"
date: 2007-02-17
draft: false
tags: ["ping"]
---

A casa di un amico, lui ha manifestato l'intenzione di conoscere <code>sftp</code>, ma non sapeva con chi fare una connessione per provare. Improvvisamente ho avuto l'idea (leggi: sono incompetente, per me vale come idea) e gli ho detto di scrivere nel Terminale <code>sftp localhost</code>.

Si è collegato con&#8230; se stesso e l'ho fatto sbizzarrire. Ovviamente aveva attivo il login remoto.

<code>localhost</code> è una cosa da ricordare. :-)