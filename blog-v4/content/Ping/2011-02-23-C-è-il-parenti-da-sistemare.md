---
title: "Parenti da sistemare"
date: 2011-02-23
draft: false
tags: ["ping"]
---

Mercoled&#236; 2 marzo prossimo parteciperò al <a href="http://www.insidesrl.it/creativityday/2011/index.cfm" target="_blank">Creativity Day</a> a Milano, organizzato da InSide Professional Training presso il teatro Franco Parenti.

Sarò l&#236; per tutta la giornata e pienamente disponibile a saluti e chiacchiere, ovviamente fatta eccezione per il tempo dedicato al mio intervento.

Fino a tutto domani (gioved&#236;) l'iscrizione alla manifestazione è gratuita, poi costerà diciotto euro. Il che non dovrebbe essere un problema in quanto, sottoscritto escluso, è presente e mostrerà meraviglie la crema della crema della creatività italiana e continentale.

Così ci distraiamo come si deve aspettando <a href="http://www.loopinsight.com/2011/02/23/apple-announces-ipad-2-event-for-march-2/" target="_blank">la presentazione del nuovo iPad</a>.