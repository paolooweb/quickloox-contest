---
title: "Protocollo a sorpresa"
date: 2007-07-06
draft: false
tags: ["ping"]
---

Diciamo che vuoi leggere le pagine <code>man</code> di un comando del Terminale, che so, <code>curl</code>.

Puoi toccare solo Safari. Non puoi usare Internet. Come fai?

La risposta è

<code>k-zna-cntr://phey</code>

Per usarla davvero, bisogna prima decifrarla con <a href="http://www.rot13.com" target="_blank">Rot-13</a>.