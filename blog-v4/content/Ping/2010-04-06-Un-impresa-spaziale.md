---
title: "Un'impresa spaziale"
date: 2010-04-06
draft: false
tags: ["ping"]
---

Tutti gli onori a John McAllister, che ha passato Pasqua a battere il record mondiale di Asteroids e luned&#236; 5 aprile, alle 10:18 ora del Pacifico, c'è riuscito.

Con 41.338.740 punti ha battuto il <a href="http://www.wired.com/science/discoveries/news/2008/11/dayintech_1113" target="_blank">record precedente</a>, stabilito dal quindicenne Scott Safran il 14 novembre 1982. Era il record che resisteva da più tempo.

L'impresa non è stata ancora <a href="v" target="_blank">ufficializzata</a>, ma non ci sono dubbi, visto che <a href="http://www.justin.tv/johnmcallister#r=null~" target="_blank">Justin.tv</a> ha trasmesso in diretta tutti i tre giorni di gioco.

A questo proposito, avere giocato almeno una volta a <a href="http://www.devolution.com/~slouken/Maelstrom/binary.html" target="_blank">Maelstrom</a> dovrebbe fare parte del corredo base di qualunque utilizzatore Mac.