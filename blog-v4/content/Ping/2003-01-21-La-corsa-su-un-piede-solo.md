---
title: "La corsa su un piede solo"
date: 2003-01-21
draft: false
tags: ["ping"]
---

I test non significano niente in generale e quelli tra piattaforme ancora meno

Si è sparso l’allarme perché tale Rob Galbraith riporta sul suo <link>sito</link> http://www.robgalbraith.com/diginews/2003-01/2003_01_07_macpc.html alcuni test cronometrici che dimostrerebbero come Windows vada più veloce di Mac.

Il buon Galbraith ha messo insieme configurazioni scelte da lui, su compiti scelti da lui, su programmi scelti da lui, e ha ottenuto i suoi risultati. Non c’è nessuna garanzia che io ottenga gli stessi risultati, neanche con le stesse macchine e neanche nelle stesse operazioni. Scoprire che una foto in formato Raw si scarica più rapidamente sotto Windows è di gran sollievo, ma non mi dice niente di che cosa succede, per esempio, lavorando in formato Tiff, o che cosa accade se i file si scaricano usando Graphic Converter intanto che Photoshop lavora ad altro, e così via.

Senza contare che non si parla di fermi macchina, o affidabilità della piattaforma, o peso delle diverse componenti del test. Che, fatto così, lascia il tempo che trova.

Ho un’idea: ripetere gli stessi testi, pari, pari, facendo funzionare i portatili solo con la batteria. Ho il sospetto che il più miserando degli iBook dopo tre ore correrà infinitamente più veloce di qualunque portatile Windows, per la semplice ragione che nessun portatile Windows ha tre ore di autonomia.

Tanto per dire quanto valgono i test fatti in questo modo. Scommetto che su un piede solo batto nella corsa chiunque. Che poi la gente nella vita abbia – nella maggioranza dei casi – due piedi, dovrebbe interessare o no ai fini della mia scommessa?

<link>Lucio Bragagnolo</link>lux@mac.com