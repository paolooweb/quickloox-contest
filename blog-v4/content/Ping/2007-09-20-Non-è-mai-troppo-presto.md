---
title: "Non è mai troppo presto"
date: 2007-09-20
draft: false
tags: ["ping"]
---

Ricevo e pubblico da <strong>Paolo</strong>:

<cite>Stamattina metto le mani sul nuovo portatile di mio padre, un Dell dell'ultima generazione, leggerissimo, due giga di Ram, webcam integrata (mi ricorda qualcosa) e ovviamente Windows Vista, non ricordo quale delle innumerevoli versioni. Due minuti buoni per caricare l'utente. Inserisco un comunissimo Dvd per testare le casse e lo schermo glossy&#8230; non lo legge, manca il codec. Provo con un altro film. Niente da fare.</cite>

<cite>Nel pomeriggio vado a fare un giro a Milano. Prima tappa: centro commerciale Fiordaliso. Apple Shop appena aperto. Poi passeggiata in via Torino: la Fnac pubblicizza il suo nuovissimo Apple Shop. Piazza Duomo: Apple Shop alla Mondadori.</cite>

<cite>Stavolta qualcosa sta cambiando sul serio.</cite>

Finalmente inizia a esserci un po' di libertà di scelta, mi permetto di aggiungere. Mai troppo presto.