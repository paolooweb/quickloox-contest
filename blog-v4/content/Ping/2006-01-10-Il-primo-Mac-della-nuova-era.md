---
title: "Il primo Mac della nuova era"
date: 2006-01-10
draft: false
tags: ["ping"]
---

E così il primo Mac con processore Intel presentato nella storia è un iMac.

A pensarci <em>dopo</em>, era ovvio. Quei nuovi chip di Intel costano ancora troppo per andare su una macchina molto economica. Al tempo stesso un professionista vuole sicurezza sulla transizione dal punto di vista software, che non si è ancora completata (per quanto riguarda Apple avverrà a marzo). Quindi il primo Mac con processore Intel è una macchina non professionale e relativamente costosa. Ovvio. <em>Dopo</em>.