---
title: "Complesso di aggiornamento<p>"
date: 2005-04-11
draft: false
tags: ["ping"]
---

Dopo avere toccato con mano abbastanza a lungo, non vedo veramente l&rsquo;ora<p>

Man mano che esploro Tiger in attesa di prossima uscita scopro sempre nuove modifiche, aggiunte, novità, curiosità, sorprese. La sera scorsa ho passato qualche momento su Automator, per esempio, e mi sono piuttosto divertito.<p>

Il sistema è ingegnoso e facile, e si capisce piuttosto in fretta. È agevolmente integrabile e ogni tanto è anche furbo. Trasformerà la vita (informatica) di molti, posso garantirlo.<p>

Se l&rsquo;impatto con Panther è stato prevalentemente visivo, quello con Tiger è quasi una spedizione di ricerca. Scavi, curiosi, guardi e continua a saltare fuori qualcosa che non immaginavi.<p>

Qualche bello spirito, come sempre, salterà fuori a dire che è solo un aggiornamento e che non ha senso spendere quella cifra per avere le nuove funzioni. Qualunque sia la cifra e qualsiasi siano le funzioni.<p>

Io, personalmente, non vedo l&rsquo;ora di avere in mano l&rsquo;edizione definitiva.<p>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>