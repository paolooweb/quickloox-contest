---
title: "Pigro e non pigro<p>"
date: 2005-11-16
draft: false
tags: ["ping"]
---

Come mi comporto di fronte a un aggiornamento<p>

Ho appena saputo di un aggiornamento a <a href="http://cyberduck.ch">Cyberduck</a>, il mio programma favorito di Ftp e Sftp. L&rsquo;ho scaricato immediatamente e l&rsquo;ho già installato al posto della versione vecchia.<p>

Con i programmi indipendenti faccio sempre così. Appena vedo che c&rsquo;è un aggiornamento, lo installo subito.<p>

Con Mac OS X sono molto più rilassato. Ho impostato l&rsquo;aggiornamento automatico, su base settimanale, e lascio fare a lui. Altrimenti che automatico è?<p>

A volte è passata anche una settimana da un update di Mac OS X a quando effettivamente è avvenuto l&rsquo;aggiornamento sul mio Mac. Per quanto siano sempre migliorie benvenute, il nucleo del sistema funziona già molto bene e quindi vado avanti a lavorare senza preoccuparmi e perderci tempo sopra. Il caso in cui viene risolto un bug di cui soffrivo personalmente gli effetti è raro o inesistente e, per questo, non ho nessuna urgenza di aggiornare.<p>

È certamente un caso, ma nessun aggiornamento mi ha mai dato problemi.<p>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>