---
title: "Come si cambia per non morire, come si cambia per amore"
date: 2010-03-19
draft: false
tags: ["ping"]
---

Il giornalista <a href="http://www.winsupersite.com/reviews/iphone_02.asp" target="_blank">Paul Thurrott nel 2007</a>:

<cite>[Il copia e incolla] è inesplicabilmente assente da iPhone. Irreale.</cite>

Il giornalista <a href="http://www.winsupersite.com/mobile/wp7_love.asp" target="_blank">Paul Thurrott nel 2010</a>:

<cite>[Windows Phone 7] non ha il copia e incolla? Non importa.</cite>

Garantisco che è sempre lo stesso Paul Thurrott.