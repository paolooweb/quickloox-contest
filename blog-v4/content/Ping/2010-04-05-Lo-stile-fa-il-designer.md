---
title: "Lo stile fa il designer"
date: 2010-04-05
draft: false
tags: ["ping"]
---

Del bombardamento di immagini relativo al lancio di iPad, mi ha colpito più di tutto vedere Jonathan Ive visitare un Apple Store e <a href="http://www.9to5mac.com/node/15630" target="_blank">acquistare di persona</a> un paio di apparecchi.

Ive è il responsabile del <i>design</i> dei prodotti in Apple. Non ha certo bisogno di comprarsi gli iPad in negozio e nel proprio laboratorio supersegretissimo, oltre a tutti gli iPad che vuole, avrà anche fior di prototipi.

Il personaggio notoriamente è schivo, ancora più di Steve Jobs. Sono totalmente da escludersi l'esibizionismo o il gusto di apparire.

Che cosa resta? Stile, rispetto verso il proprio lavoro e la propria azienda, passione per quello che si costruisce con il proprio genio.