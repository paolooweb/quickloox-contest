---
title: "Il padre dei confronti"
date: 2010-02-26
draft: false
tags: ["ping"]
---

Consiglio vivamente la lettura di questa <a href="http://iphonedevelopment.blogspot.com/2010/02/nexus-one-from-iphone-developer.html" target="_blank">visione di Android (su un Nexus One) dal punto di vista di uno sviluppatore di iPhone</a>. Qualche passo saliente:

<cite>Lo schermo sensibile al tocco è avvertibilmente meno preciso e sensibile di quello di iPhone.</cite>

<cite>Dei quattro pulsanti, solo il pulsante</cite> home <cite>ha bisogno di essere hardware e non è per coincidenza che Apple ha fatto cos&#236;. C'è uno schermo</cite> touch<cite>, vivaddio. Tutto quello che è sensibile al contesto e non è universale dovrebbe andare su uno schermo, non in un pulsante. A peggiorare le cose, i sensori dei pulsanti non sono esattamente allineati con le icone e bisogna toccare visibilmente più su del pulsante per attivarlo.</cite>

<cite>Peggio ancora, il pulsante</cite> home <cite>è subito sotto la barra spaziatrice. Non saprei dire quante volte stavo digitando e sono uscito per errore dall'applicazione.</cite>

<cite>Il giudizio complessivo è che Nexus One è ottimo per il 90 percento delle cose ma il dieci percento che resta è proprio quello che lo rende meno appetibile alle persone normali.</cite>

Solo un ultimo paragrafo, che riassume tomi di saggezza per chi vuole acquistare&#8230;

<cite>Lo schermo sembra un classico caso di tentativo di competere sulle specifiche senza chiedersi se davvero servivano specifiche migliori.</cite>

&#8230;e una frase che sto intagliando sulla scrivania per immortalarla (la frase):

<cite>&#8220;Perché si può&#8221; è raramente una buona ragione per inserire una funzione.</cite>