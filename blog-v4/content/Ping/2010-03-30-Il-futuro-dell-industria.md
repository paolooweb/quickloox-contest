---
title: "Il futuro dell'industria"
date: 2010-03-30
draft: false
tags: ["ping"]
---

Marc Benioff, amministratore delegato di <a href="http://www.salesforce.com/it/?ir=1" target="_blank">salesforce.com</a>, a proposito di come <a href="http://techcrunch.com/2010/03/29/ipad-cloud-2/" target="_blank">il mondo stia cambiando</a>:

<cite>Il futuro della nostra industria oggi ha un aspetto totalmente differente dal suo passato. Ha l'aspetto di un foglio di carta e si chiama iPad. Non è questione di fare clic o digitare; è questione di toccare. Non riguarda il testo e nemmeno l'animazione, ma il video. Non concerne un disco locale, ma neanche un desktop, bens&#236; la </cite>cloud <cite>[la</cite> nuvola <cite>con cui oggi si identificano i servizi di rete come <a href="http://www.me.com" target="_blank">MobileMe</a> o <a href="http://docs.google.com" target="_blank">Google Docs</a>].</cite> <cite>Non si parla di andarsi a cercare le informazioni, ma di riceverle automaticamente. Non c'entra più ricondizionare il vecchio software e invece c'entra riscriverlo a zero (perché volete sfruttare il fantastico potenziale dei nuovi computer e di Internet &#8212; e perché volete scalare una vetta ancora più alta). Finalmente l'industria è tornata a essere interessante.</cite>

Benioff traccia anche una mappa dei cambiamenti tra quella che lui sostiene essere una vecchia e una nuova versione della <i>cloud</i>.

Non sono completamente d'accordo e però raccomando caldamente la lettura.