---
title: "Avventure nel passato"
date: 2007-06-26
draft: false
tags: ["ping"]
---

Si vede che è il periodo degli <em>adventure game</em>. Grazie a <strong>Eugenio</strong> per avermi ricordato l'esistenza di <a href="http://www.scummvm.org/" target="_blank">ScummVm</a>, motore che permette di giocare un sacco di bellissime avventure di una volta.

Manca, inevitabilmente, <a href="http://www.erix.it/avventure.html" target="_blank">Avventura nel Castello</a> del grande <strong>Enrico Colombini</strong>, ma in attesa del suo inevitabile ravvedimento verso il Mac ci accontenteremo&#8230; di usare un qualunque <a href="http://apple2.intergalactic.de/" target="_blank">emulatore per Apple II</a>, se non addirittura un Parallels Desktop.