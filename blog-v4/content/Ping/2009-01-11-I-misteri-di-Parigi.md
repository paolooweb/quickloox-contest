---
title: "I misteri di Parigi"
date: 2009-01-11
draft: false
tags: ["ping"]
---

Il 17 dicembre scorso, un giorno dopo che Apple ha annunciato <a href="http://www.macworld.com/article/137587/2008/12/macworldexpo.html" target="_blank">la sua ultima partecipazione a Macworld Expo</a>, Macworld.com ha scritto che <a href="http://www.macworld.com/article/137605/2008/12/appleexpo.html" target="_blank">veniva cancellato Apple Expo di Parigi 2009</a>.

Le scimmie urlatrici ovviamente si sono scatenate e se uno fa una ricerchina su Google trova decine di&#8221;notizie&#8221;, tutte quante scopiazzature cieche del pezzo di Macworld.com.

E fin qui è la triste normalità. Ma siamo andati oltre. Un altro giorno dopo, il 18 dicembre, Notebook Italia scrive che Parigi 2009 è stato cancellato (scopiazzo) e che Parigi 2008 è stato <a href="http://notebookitalia.it/apple-expo-2009-di-parigi-cancellato-4182.html" target="_blank">cancellato a tempo indeterminato</a>.

Ohibò. Apple Expo Parigi 2008 si è tenuto dal 17 al 20 settembre 2008, meno di quattro mesi fa, a Paris-Expo, Porte de Versailles, <em>hall</em> 5.

Lo dice il <a href="http://www.apple-expo.com/" target="_blank">sito ufficiale della manifestazione</a> e fin qui. Su Flickr ho trovato <a href="http://www.flickr.com/search/?q=apple%20expo%20paris%202008&w=all" target="_blank">181 raccolte di foto scattate all'Expo di Parigi 2008</a>. Sono convinto che Parigi 2008 ci sia stato.

A questo punto mi sono ulteriormente insospettito. Macworld.com afferma che di Parigi 2009 glielo hanno dichiarato gli organizzatori, Reed Exhibitions. Sul <a href="http://www.reedexpo.com" target="_blank">sito americano</a> di Reed però non c'è traccia di annuncio ufficiale in merito. Sul <a href="http://www.reedexpo.fr" target="_blank">sito francese</a> di Reed non c'è traccia di annunci di cancellazione. Si legge invece che Apple Expo Paris 2009 si terrà il primo settembre, stesso luogo del 2008.

È evidentemente un <em>placeholder</em>, un segnaposto. Però non è stato tolto, non ci sono comunicati che lo contraddicano e al momento il sito ufficiale dell'organizzatore afferma che la manifestazione 2009 esiste, almeno nelle intenzioni e almeno all'oggi.

Macworld.com afferma di avere parlato con quelli di Reed Exhibitions e gli va concessa la buona fede. Poco prima di scrivere questo Ping! ho inviato una richiesta di chiarimenti a Reed Exhibitions e se mi arriva una risposta la pubblicherò.

Chi ha dato la notizia originale va creduto fino a prova contraria. Chi la sta commentando ha appena chiesto lumi ai diretti interessati.

E le scimmie urlatrici?