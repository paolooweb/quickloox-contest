---
title: "Quarantanove software che parla"
date: 2008-09-13
draft: false
tags: ["ping"]
---

Sono i <em>bug</em> sistemati in <a href="http://www.barebones.com/support/bbedit/arch_bbedit901.html" target="_blank">BBEdit 9.0.1</a>.

Lo dice John Gruber, non li ho contati. Mi fido. Di lui e di Bare Bones.