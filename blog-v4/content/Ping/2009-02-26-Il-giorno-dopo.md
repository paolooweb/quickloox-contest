---
title: "Il giorno dopo"
date: 2009-02-26
draft: false
tags: ["ping"]
---

Vorrei avere cose interessanti da dire sul nuovo MacBook Pro. Invece non ne ho e paradossalmente questo è interessante.

Significa che la migrazione è stata davvero senza problemi. Scrivo alla stessa velocità di prima, non ho ancora visto problemi software, i connettori Usb funzionano. Devo ancora provare Ethernet, lo farò domani. E il lettore Dvd, quando capiterà (probabilmente per masterizzare la beta di Snow Leopard).

Lo schermo è lucido ma, se lo sfondo scrivania non è nero o scurissimo (e permette di vedere il riflesso, se ci si concentra su quello, altrimenti di riflesso non ce n'è), ho bisogno che qualcuno me lo ricordi, altrimenti non capirei la differenza. Sarà che non lavoro con una luce alle spalle o non l'ho ancora fatto. Proverò anche questo.

Sto trovando i compromessi giusti sulla tipografia. Il testo deve essere un po' più grande altrimenti si fatica a vederlo, e piccolo abbastanza per sfruttare l'enorme spazio in più a disposizione. A naso tra prestazioni e pixel si guadagna un bel 50 percento di produttività.

La macchina è silenziosa e scalda decisamente meno del PowerBook. Devo ancora tirarle il collo, però, o meglio riuscire a tirarglielo; una dozzina di applicazioni aperte, che già facevano sbuffare il PowerBook, non gli fanno né caldo é freddo. Per sentire la ventola ho aperto World of Warcraft e ho messo tutti gli effetti video e 3D al massimo, a risoluzione piena. La ventola è partita e fa lo stesso rumore di prima, cioè normale e accettabile. Tra l'altro, giocando con la scheda video vera e tutti gli effetti al massimo, dentro la piazza principale di Stormwind, di giorno (nelle capitali affollate il gioco si appesantisce a causa della presenza di molti giocatori nello stesso luogo), ho ottenuto un numero di fotogrammi per secondo che mi ha fatto vergognare, senza neanche chiudere uno degli altri programmi. Sul PowerBook l'ultima espansione era perfettamente giocabile tra i quindici e i trenta fotogrammi per secondo, purché si fosse chiusa qualunque altra applicazione impegnativa. Qui, con Safari e Firefox aperti, iTunes in funzione, Vienna con una quarantina di tab, Pando a scaricare roba, Mailsmith a ritirare la posta, Skype attivo, ho toccato come picco i 170 fotogrammi per secondo. Più o meno questa macchina possiede una decina di volte le risorse che servono per giocare a World of Warcraft e ancora il sistema non sfrutta pienamente né il doppio core né il coprocessore grafico come farà con Snow Leopard. Fa impressione. Non ci sono abituato e sento quasi di dovermi dimostrare all'altezza, come il pilota seduto in una Formula 1.

Uno dei punti di interesse del nuovo 17&#8221; è la batteria, che dovrebbe durare di più e vivere più a lungo in cambio dell'essere integrata. Ne saprò di più domani, quando passerò la giornata fuori casa. Ho fatto il possibile per ammazzare la durata della batteria preventivamente in fase di ordine, con il processore a 2,93 gigahertz e il disco rigido da 7.200 giri per minuto, e in configurazione, con l'impostazione video sulla scheda grafica vera, che consuma molto di più. In pratica il mio 17&#8221; dovrebbe avere la peggiore autonomia a batteria possibile. Sono curioso di vedere quello che accade.

Una cosa che mi sono dimenticato ieri è il tasto di accensione. Non è più vicino alla tastiera ma proprio sull'angolo del telaio, in alto a destra, è molto più piccolo di prima e molto più discreto di prima. Come si era chiacchierato con Giuliano, nella nuova tastiera manca il tasto fisico Enter che prima stava tra Comando e Opzione a destra della barra spaziatrice. Per ora non ne ho sentito particolarmente la mancanza, ma per chi bazzica molto i fogli di calcolo forse può dare fastidio. Le funzioni secondarie dei tasti funzione (luminosità, volume eccetera) sono state rimescolate e, essenzialmente, chi se ne frega. La nuova disposizione è diversa dalla prima, non peggiore né migliore. L'impatto sull'uso della macchina è quasi zero per le prime due ore, zero da l&#236; in avanti.

Che altro? Ho ripreso presto proprio tutte le vecchie abitudini. Compreso alzarmi distrattamente con un piede sul cavo di alimentazione. Stavolta però, invece che fare una mezza acrobazia, smoccolare e verificare che tutto funzionasse, il connettore MagSafe si è staccato senza drammi.

L'ho riattaccato senza drammi e, a prescindere da tutto il resto, almeno in questo dettaglio la qualità della mia vita informatica è migliorata senza discussione.