---
title: "Ancora pi&ugrave; Profiler"
date: 2006-04-02
draft: false
tags: ["ping"]
---

Ringrazio assai <strong>ilduca69</strong> per avermi fatto notare che, in tema di batterie e software per il conteggio, &egrave; sufficiente aprire <em>System Profiler</em> alla voce Energia (sotto Hardware) per avere i dati sul numero di cicli di carica e la capacit&agrave; corrente della batteria.

Il software apposito fornisce i dati in forma pi&ugrave; leggibile e immediata, ma non &egrave; dunque strettamente necessario.

Giurerei che le cose non stessero cos&igrave;, una volta, e suppongo che le nuove informazioni dipendano da qualche aggiornamento software che ha conferito nuove possibilit&agrave; a System Profiler.