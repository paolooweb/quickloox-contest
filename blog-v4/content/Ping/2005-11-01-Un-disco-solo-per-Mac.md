---
title: "Un disco solo per Mac<p>"
date: 2005-11-01
draft: false
tags: ["ping"]
---

A volte il mantra del software che c&rsquo;è solo per Pc è un po&rsquo; ingiustificato<p>

L&rsquo;amico <a href="http://homepage.mac.com/riccardo.mori/blogwavestudio/">Riccardo</a> mi ha segnalato <a href="http://gdisk.sourceforge.net/">gDisk</a>, un softwarino interessante che trasforma un account Gmail in un disco Internet da due giga e mezzo (e in crescita).<p>

Consulta la pagina e dimmi dove vedi la versione per Windows.<p>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>