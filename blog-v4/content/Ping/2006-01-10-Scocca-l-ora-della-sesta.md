---
title: "Scocca l&rsquo;ora (della) sesta"
date: 2006-01-10
draft: false
tags: ["ping"]
---

iPhoto, iMovie, iDvd, GarageBand, iTunes e arriva anche iWeb.

Se lo fanno facile come HomePage, quello di .mac. sarà un successone. Ma tremo al pensiero di quanta gente non vuole imparare una briciola di Html e vuole comunque pubblicare pagine. Un po&rsquo; come quelli che vogliono scrivere a tutti i costi ma pensano che l&rsquo;italiano sia superfluo.