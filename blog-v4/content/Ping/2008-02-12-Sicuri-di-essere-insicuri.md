---
title: "Sicuri di essere insicuri?"
date: 2008-02-12
draft: false
tags: ["ping"]
---

Non è che, a suon di insistere sulla sicurezza e sulle falle e sulle <em>patch</em>, stiamo finendo per perdere un sacco di tempo su pericoli che non lo sono, con spreco di tempo e di risorse?

Mi ha fatto molto pensare la notizia dell'ennesimo bug, questa volta nel <a href="http://www.trusteer.com/docs/dnsopenbsd.html" target="_blank">generatore di numeri pseudorandom di OpenBsd</a>.

Mac OS X discende dalla famiglia Bsd e il <em>bug</em> lo riguarda, cos&#236; come interessa FreeBsd, NetBsd, DragonFlyBsd e chissà se ce n'è ancora.

Il punto è che il coordinatore di OpenBsd ha scritto che il <em>bug</em> non verrà chiuso, in quanto completamente irrilevante nel mondo reale.

OpenBsd si qualifica come il sistema operativo più sicuro al mondo tra quelli minimamente diffusi (basta guardare la <a href="http://www.openbsd.org/" target="_blank">pagina home</a>). Se non chiude il <em>bug</em> perché irrilevante, mentre Apple magari lo fa (perché se Apple non mostra pubblicamente di essere impegnata sulla sicurezza casca il mondo), vuol dire che Apple è costretta a sprecare tempo e risorse, che potrebbero essere meglio usate.

Vale per tutti i sistemi operativi. Solo che Apple ogni tanto si inventa anche qualcosa e il tempo dei suoi programmatori, quando è sprecato, leva qualcosa a ognuno.