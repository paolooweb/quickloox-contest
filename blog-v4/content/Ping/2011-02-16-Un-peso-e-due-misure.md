---
title: "Un peso e due misure"
date: 2011-02-16
draft: false
tags: ["ping"]
---

Si alimenta la controversia sull'annuncio di Apple del <a href="http://www.apple.com/pr/library/2011/02/15appstore.html" target="_blank">meccanismo per abbonarsi alle riviste</a> su App Store, che prevede per Apple il 30 percento dell'importo.

Silenzio assordante invece sulla <a href="https://kindlepublishing.amazon.com/gp/vendor/kindlepubs/common/get-content?id=200492750" target="_blank">politica di Amazon</a>, che lascia agli editori il 70 percento, <i>escluse le spese di spedizione</i>.

Spese di spedizione? Eh s&#236;; se il libro o la rivista viene erogata tramite un canale a pagamento, per intenderci non attraverso una rete Wi-Fi ma per esempio tramite il canale 3G Whispernet proprio di Kindle, il documento digitale viene tassato a peso, in ragione di 0,99 centesimi di dollaro per megabyte. Detratto questo costo di spedizione dal prezzo di vendita, si passa a calcolare il 70 percento cui ha diritto l'editore.

È scritto a chiarissime lettere in fondo al lungo documento qui sopra linkato. Sembra meno scritto che tutti considerano Internet un paese dei balocchi dove le cose arrivano gratis. Invece farle arrivare costa.