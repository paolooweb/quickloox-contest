---
title: "Psicotico design"
date: 2008-06-29
draft: false
tags: ["ping"]
---

Sul balcone c'è il vicino, che armeggia con il suo portatile, mi vede passare e mi chiede <cite>se posso dare una mano</cite>.

Il pulsante hardware che accende la scheda wireless non funziona più. Il pulsante hardware. Per accendere la scheda wireless.

Il portatile indica la presenza della scheda, ma nessun comando riesce a metterla in funzione.

A un certo punto la disperazione ci spinge fino a una sorta di programma diagnostico.

Quando dopo alcuni minuti appare il messaggio <cite>Premi Fine per iniziare a risolvere il problema</cite> (quasi testuale) abbozzo una scusa, tipo la pasta che scuoce, e mi defilo.

Già l'idea di premere Start per spegnere il computer mi pare poco brillante. Il pulsante Fine che inizia una procedura è decisamente frutto di una mente malata.