---
title: "Apple, mio zio e Punto Informatico"
date: 2006-02-28
draft: false
tags: ["ping"]
---

Scrivere di altri sedicenti media informativi fa imbarazzo. Puoi subito essere accusato di tirare l’acqua al tuo mulino. Poi all’editore non piace gran che quando si parla male di altri. A dire il vero non gli è piaciuto neanche quando si è parlato male di lui. Vorrebbe che si parlasse bene di tutti.

Però non ce la faccio. E ti dico papale papale: la copertura del mondo Mac da parte di Punto Informatico fa ribrezzo. Peggio ancora di quella di Repubblica.

Ultimo dei ribrezzi, l’articolo sul <a href="http://punto-informatico.it/p.asp?i=58031&amp;r=PI" target="_blank">(non) uso del Trusted Computing nei Mac Intel</a>. In mancanza di appigli nel presente, si fa disinformazione totale sul futuro.

Se scrivessi di mio zio dicendo che oggi vive tranquillo con un coltello da cucina in cucina, ma domani potrebbe prendere il coltello e cominciare a squartare la gente per strada e nessuno ci può garantire che non succederà, mio zio mi chiederebbe se sono ammattito io, prima che lui. Invece si può scrivere impunemente che Apple non usa il Trusted Computing, ma <cite>questo però non vuol assolutamente dire che questo non succederà mai</cite>.

Certo. Finora mio zio ha usato il coltello solo per tagliare le bistecche. Ma questo non vuol assolutamente dire eccetera eccetera.

Se poi mio zio comprasse i <a href="http://www.miracleblade.com/" target="_blank">Miracle Blade</a>, sarebbe un chiaro indizio che gli abitanti del suo paese sono tutti a rischio.