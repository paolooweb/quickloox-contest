---
title: "Servizi sottoutilizzati"
date: 2007-02-14
draft: false
tags: ["ping"]
---

Devo registrare per forza in Rtf e ho scritto in BBEdit. Comando-a, Comando-c, Comando-Tab per passare a TextEdit, Comando-v.

No, aspetta.

Comando-a, Ctrl-F2 (per aprire i menu da tastiera), BBEdit -> Services -> TextEdit -> New Window Containing Selection. Fatto.

Un caso in cui i servizi di Mac OS X fanno risparmiare un quarto del lavoro. Vanno riscoperti; sono molti utili e poco utilizzati.