---
title: "Approssimazione in Anteprima"
date: 2009-01-08
draft: false
tags: ["ping"]
---

Sono basito al punto da credere di avere sbagliato qualcosa di grosso o di essere tonto.

Mi sembra che, nella gestione dei Preferiti di Anteprima, i Preferiti possano essere cancellati solo uno per volta e non sia possibile selezionarne più di uno con Maiuscolo-clic, e che Comando-A per selezionarli tutti non sia previsto.

Preferisco scoprire che sono tonto invece di dover cancellare i Preferiti uno per volta. Se sono tonto, dunque, dimmelo e spiegami perché.