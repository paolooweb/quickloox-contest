---
title: "Aspetta e spera"
date: 2006-09-17
draft: false
tags: ["ping"]
---

Sentita da un giornalista durante la presentazione dei nuovi iPod a Milano:

<cite>Speravo proprio che Apple presentasse finalmente il cellulare&#8230; sono tre anni che deve arrivare&#8230; sono un po' deluso.</cite>

Quando poi Apple farà veramente un cellulare scommetto che non si atteggerà a credulone che ha abboccato per tre anni o forse più, ma come preveggente in anticipo sui tempi.