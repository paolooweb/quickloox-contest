---
title: "Browser del futuro"
date: 2006-07-21
draft: false
tags: ["ping"]
---

Grazie a <a href="http://www.kero.it" target="_blank">Neko</a> per avere segnalato questa interessante <a href="http://mark.alittlenoise.com/blog/?p=22" target="_blank">recensione</a> di <a href="http://hmdt-web.net/shiira/en" target="_blank">Shira</a> 2, in attesa che arrivi anche sul sito, dove sono alla 1.2.1 mentre scrivo.

Ogni tanto, oltre a provare di prima mano, leggere un buon parere fa solo bene.