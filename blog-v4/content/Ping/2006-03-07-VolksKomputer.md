---
title: "VolksKomputer"
date: 2006-03-07
draft: false
tags: ["ping"]
---

Mi segnala <strong>Carmelo</strong>:

Nel nuovo spot della Volkswagen, che enumera i sistemi di sicurezza dell&rsquo;auto prodotti nel corso degli anni, prova a contare i prodotti Apple presenti nei vari episodi&hellip;

Non ho visto lo spot, ma mi sa che ne vale la pena. :-)