---
title: "Selezionato, si stampi"
date: 2006-11-21
draft: false
tags: ["ping"]
---

Grazie a <strong>Pierfausto</strong> che è in gran vena creativa e ha trovato su <a href="http://www.macosxhints.com/article.php?story=20061002171102999" target="_blank">Mac OS X Hints</a> un simpatico AppleScript che consente di inviare direttamente alla stampante il testo selezionato nel browser.

Il bello è che basta cambiare il nome del programma per avere lo stesso effetto in qualunque altra applicazione. Che sia compatibile AppleScript.

Come hack è notevole perché non si tratta della solita opzione nascosta, ma c'è anche una parte realizzativa. Complimenti.