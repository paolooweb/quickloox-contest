---
title: "La banda passante degli onesti"
date: 2010-08-09
draft: false
tags: ["ping"]
---

Electronista <a href="http://www.electronista.com/articles/10/08/09/dell.tries.to.scare.users.away.from.macbooks/" target="_blank">racconta agevolmente la faccenda</a> senza troppi orpelli.

<cite>Dell ha causato controversie pubblicando una <a href="http://macnn.com/rd/169970==http://www.dell.com/content/topics/segtopic.aspx/apple-comparison?c=us&amp;cs=19&amp;l=en&amp;s=dhs" target="_blank">comparazione inaccurata</a> tra i cuoi computer e quelli Apple, vantando prezzi anche della metà per specifiche &#8220;simili&#8221;. [&#8230;]</cite>

<cite>Una ispezione accurata rivela che Dell ha scelto ad arte i sistemi e ha glissato sopra le mancanze delle proprie macchine. Nei portatili, gli Studio usano processori i7 a 1,73 gigahertz contro gli i7 a 2,66 gigahertz di MacBook Pro. Quest'ultimo è potenzialmente più veloce nella maggior parte dei casi e raggiunge le nove ore di autonomia, contro le 5,5 ore e le otto ore dei modelli Dell mostrati. Ma non è scritto.</cite>

<cite>Dell omette selettivamente anche i dettagli su qualità degli schermi, retroilluminazione della tastiera, Bluetooth, dimensioni, peso e qualità del supporto. Inoltre i suoi prezzi sono promozionali e non di listino. [&#8230;]</cite>

<cite>Nei</cite> desktop<cite>, Zino Hd è più conveniente di un Mac mini, ma usa un processore Athlon Neo X2 ben più lento; inoltre c'è una differenza di prestazioni grafiche e Mac mini ha Wi-Fi 802.11n e Bluetooth incorporati. [&#8230;]</cite>

<cite>Dell ha inoltre scelto di confrontare il</cite> tower <cite>Inspiron con gli iMac della generazione precedente e non di quella attuale. In più l'Inspiron usa uno schermo Lcd a basso costo mentre l'iMac monta uno schermo con tecnologia Ips.</cite>

<cite>Nessuno dei prezzi Apple tiene conto degli sconti per studenti, che fino all'estate includono anche un iPod gratis.</cite>

A fare costare meno i Pc, cos&#236;, sono buono anch'io.

Devo aggiungere che il 23 luglio la U.S. Security and Exchange Commission, la Consob americana, <a href="http://www.sec.gov/news/press/2010/2010-131.htm" target="_blank">ha multato Dell (azienda) di cento milioni di dollari per frodi contabili</a> e Michael Dell in persona per quattro milioni di dollari?

L'amministrazione di Dell gonfiava gli utili per mostrare che venivano raggiunti gli obiettivi trimestrali e non era vero. Ora fanno lo stesso lavoro, più o meno, con le comparazioni su Internet.

Chi ha comprato un computer Dell ne è vittima. Ma se lo sa e nonostante questo ne compra comunque, diventa complice.