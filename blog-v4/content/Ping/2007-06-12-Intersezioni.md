---
title: "Intersezioni"
date: 2007-06-12
draft: false
tags: ["ping"]
---

Tra Mac, arte, musica, siti, blog.

l'intersezione, qui, avviene con <strong>Alessio</strong>, animatore di <a href="http://www.mac-community.it/" target="_blank">Mac-Community</a>, uno dei migliori siti Mac italiani (infatti non ha annunciato Zfs in Leopard).

Alessio è pianista di talento, <em>web author</em> di talento, organizzatore di talento. Presenterà una delle sue opere <strong>gioved&#236; 14 giugno</strong>, dalle ore <strong>21</strong>, nella <strong>Sala del Buonumore</strong> in <strong>piazza delle Belle Arti 2</strong>, a <strong>Firenze</strong>, durante la manifestazione <a href="http://www.mac-community.it/Blog/files/bec04b4fdca0bfb69b1f1a9f74d4bc76-183.html" target="_blank">Intersezioni</a>. Di cui appunto si può leggere tutto sul suo sito.

Intersezioni schiera una quantità di Mac notevole e ha l'aria di essere una cosa interessante.

Non so se riesco a farcela per gioved&#236; sera a Firenze. Lui, però, ce la farà e sarà grande.