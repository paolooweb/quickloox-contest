---
title: "Castronerie da Macworld Expo<p>"
date: 2005-01-11
draft: false
tags: ["ping"]
---

Quando Apple sta per annunciare novità nessuno capisce più niente, neanch&rsquo;io<p>

Entro poche ore Steve Jobs avrà deliziato la platea del Moscone Center di San Francisco con il suo <em>keynote</em> in occasione del Macworld Expo di gennaio 2005.<p>

Sempre entro poche ore un sacco di persone dovranno rimangiarsi decine di scempiaggini raccontate credendo a siti di dubbia reputazione ma più che altro poco (e male) informati.<p>

Io faccio la mia parte in anticipo: l&rsquo;articolo <em>Orgoglio Apple</em> sulla prima pagina de Il Foglio di sabato non era di Luca Sofri. La fonte della correzione è ineccepibile. Sempre che Luca Sofri non mi stesse prendendo in giro&hellip; :-)<p>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>