---
title: "Alimentazione e profilo<p>"
date: 2005-03-19
draft: false
tags: ["ping"]
---

Ecco perché gli alimentatori dei portatili Apple sono così snelli<p>

Sono eleganti e discreti, gli attuali alimentatori di PowerBook e iBook. Ma quelli a yo-yo a me piacevano molto e, a parte il naturale ricambio delle linee, mi sono chiesto se ci fossero ragioni particolari per fare i parallelepipedi attuali.<p>

Oggi ho avuto l&rsquo;illuminazione. C&rsquo;era una classica ciabatta per inserire più spine, due ragazzi con altrettanti Powerbook e chiaramente due alimentatori che, ricordo, possono essere inseriti direttamente nelle prese, se non serve una lunghezza notevole di cavo.<p>

Beh, le prese sulla ciabatta erano orientate obliquamente, come spesso accade. I due alimentatori erano tranquillamente inseriti in prese adiacenti e lo spessore era perfetto per farli convivere senza problemi.<p>

Dirai che ci arrivo per ultimo e che lo si capisce subito. Vero, ma mi piace lo stesso.<p>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>