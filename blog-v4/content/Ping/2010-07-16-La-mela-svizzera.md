---
title: "La mela svizzera"
date: 2010-07-16
draft: false
tags: ["ping"]
---

AppleJack è arrivato alla versione 1.6 e ora funziona bene anche con Snow Leopard.

Una volta nella vita toccherà a chiunque riavviare il Mac in Single User Mode tenendo premuto Comando-S all'avvio. E quasi nessuno di noi avrà in testa, meno che mai su un foglietto, i criptici ordini da digitare (su una tastiera di <i>layout</i> americano, auguri di trovare il trattino e lo <i>slash</i> se non ricordi dove stanno) per cercare di rimettere in ordine il disco, svuotare le cache e fare altro.

Con AppleJack installato basterà invece digitare <code>applejack</code> e avremo a disposizione un menu semplice, per quanto in inglese. Digitando <code>x</code> apparirà anche un menu per esperti, più versatile e naturalmente pericoloso per un non addetto. Digitando invece <code>applejack auto</code>, il programma fa tutto da solo. Digitando <code>applejack auto restart</code>, il Mac riparte da solo al termine delle operazioni. Digitando <code>man applejack</code> si può scoprire tutto il resto.

<a href="http://applejack.sourceforge.net/" target="_blank">AppleJack</a> è invisibile, non consuma spazio, non consuma memoria, è gratuito, serve solo in caso di problemi.

Naturalmente va installato <i>prima</i> dei problemi e chi non lo fa rinuncia a una possibilità in più di togliersi dai pasticci con facilità. Per giunta gratis ed efficace. Un normale coltellino svizzero risponde perfettamente al secondo requisito, mentre il primo è decisamente meno garantito.