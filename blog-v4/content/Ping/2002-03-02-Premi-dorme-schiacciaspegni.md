---
title: "Premi e dorme, schiaccia e spegni"
date: 2002-03-02
draft: false
tags: ["ping"]
---

La soluzione a un problema che forse qualcuno si porrà

Una delle numerose meraviglie di iMac (Flat Panel), come da nome ufficiale della macchina, è il suo... pulsante di accensione.
Premendolo e basta la macchina (accesa) va in modalità Stop; tenendolo premuto per cinque secondi invece il computer si spegne. All’accensione, invece, tenerlo premuto per più di qualche secondo consente di applicare eventuali aggiornamenti al firmware.
Penso già al momento in cui sulle mailing list e alle redazioni giungeranno lamenti preoccupati: “neanche si spegne... con quello che è costato...”.
Invece no; oltre ad avere un bel G4 come cuore pulsante, iMac (Flat Panel) ha anche un pulsante di buon cuore.

<link>Lucio Bragagnolo</link>lux@mac.com