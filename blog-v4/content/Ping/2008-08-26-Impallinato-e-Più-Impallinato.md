---
title: "Impallinato e Più Impallinato"
date: 2008-08-26
draft: false
tags: ["ping"]
---

Per fare il verso a un certo film, solo che qui emerge una certa dose di intelligenza. Non necessariamente applicata al meglio, ne convengo.

Peggio quello che <a href="http://algebraicthunk.net/~dburrows/blog/entry/package-management-sudoku/" target="_blank">risolve gli schemi di Sudoku usando il sistema di gestione dei pacchetti software Debian</a> oppure quell'altro che ha realizzato <a href="http://mysqlgame4.appspot.com/" target="_blank">un gioco gestionale da affrontare dentro un database a colpi di istruzioni mySql</a>?