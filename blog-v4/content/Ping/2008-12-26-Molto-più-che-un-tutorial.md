---
title: "Molto più che un tutorial"
date: 2008-12-26
draft: false
tags: ["ping"]
---

Lui giocava a World of Warcraft e ha condiviso il suo schermo tramite iChat.

Io, venti chilometri più lontano, vedevo il suo schermo tramite iChat e via audio gli ho dato un paio di dritte forse utili.

Una delle funzioni più sottovalutate e più straordinarie di Mac OS X.