---
title: "Le cose a meta(Mark)"
date: 2007-12-06
draft: false
tags: ["ping"]
---

È noto il mio amore per il comando integrale del Mac via tastiera, che (mi) evita la sindrome del tunnel carpale ed elimina il più possibile la <em>trackpad</em>.

Due corollari di questo amore sono la ricerca della velocità (perché l'interfaccia via tastiera è irrimediabilmente più lenta di quella grafica, checché ne dicano i suoi estimatori) e l'analoga ricerca per la completezza, perché se vuoi fare tutto via tastiera avere un pezzo di procedura inefficiente, o che richiede per forza il mouse, è frustrante e vanifica il resto del lavoro.

Una delle operazioni più utili per me in assoluto è la contrazione degli Url, il lavoro svolto da <a href="http://hardclicker.com" target="_blank">HardClicker</a>, <a href="http://snipurl.com" target="_blank">SnipUrl</a>, <a href="http://tinyurl.com" target="_blank">TinyUrl</a> e altri, utilissimo per mandare in stampa indirizzi web lunghi molte decine di caratteri senza che il lettore debba impazzire a copiarli.

Per arrivare via tastiera a contrarre un Url uso un <em>bookmarklet</em> nella barra di Safari, ma è alquanto inefficiente.

Finalmente ho scoperto qualcosa di meglio: <a href="http://www.leancrew.com/all-this/2007/11/long_and_shortened_url_scripts.html" target="_blank">uno script</a> che copia automaticamente in memoria l'indirizzo della finestra del browser in primo piano e lo contrae, sempre automaticamente, mediante l'interfaccia di <a href="http://metamark.net" target="_blank">Metamark</a>, per copiare in memoria l'indirizzo contratto. A questo punto vado nella finestra dove sto scrivendo e mi basta incollare l'indirizzo contratto. Una meraviglia.

È qualcosa che mostra molto bene anche le potenzialità di AppleScript. Vedere, per credere, la <a href="http://www.leancrew.com/all-this/2007/11/shortened_urls_with_quicksilve.html" target="_blank">prima versione dello script</a>, realizzata in Python.

<a href="http://web.mac.com/brethil/Brethils_Page/Thoughts/Thoughts.html" target="_blank">Dany</a> ha fatto dello script una piccola applicazione, che a me basta chiamare da Spotlight per raggiungere il risultato. Spotlight è relativamente poco efficiente per un'operazione del genere, ma è perfetto per l'uso da tastiera.

Ora devo capire se riesco ad assegnare allo script una combinazione da tastiera e cos&#236; richiamarlo direttamente, senza passare da Spotlight (l'autore usa <a href="http://blacktree.com/" target="_blank">QuickSilver</a>, dove si fa in un attimo, ma a me piace poco perché sa troppo di <em>hack</em> del sistema). Sarebbe una bella dimostrazione di come lo scripting sia in grado di migliorare la (mia) vita sul Mac. Gli Url contratti che crea Metamark sono anche più corti di quelli di SnipUrl, che usavo prima.