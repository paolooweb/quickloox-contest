---
title: "Disabili del XXI secolo"
date: 2007-06-25
draft: false
tags: ["ping"]
---

Rispondono a una mail, vedono il cursore piazzato dal programma stupidamente a inizio messaggio e non sanno come metterlo sotto il messaggio originale.

Nell'era del politicamente corretto bisognerà trovare un neologismo non offensivo. <em>Epistolarmente svantaggiati</em> può andare?

Certo, a me piacerebbe offenderli. Chiamarli <em>e-pistola</em>. Ma non si può fare. Pazienza.