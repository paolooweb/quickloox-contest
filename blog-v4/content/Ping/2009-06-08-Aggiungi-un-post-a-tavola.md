---
title: "Aggiungi un post a tavola"
date: 2009-06-08
draft: false
tags: ["ping"]
---

Sono pronto per la chiacchierata di stasera sull'andamento della Wwdc.

Sono già sul canale <code>#freesmug</code>, luogo del convegno, e già in <code>pocroom</code>, a disposizione per fornire supporto in tempo reale (ma entrare in Irc è facilissimo).

Ho corretto i numerosi errori commessi nello stendere <a href="http://www.macworld.it/blogs/ping/?p=2447" target="_self">il <i>post</i> che riepiloga le istruzioni</a> e ora è tutto sistemato e linkato come si deve. Per qualunque chiarimento, leggerlo, oppure sono a disposizione in <code>pocroom</code> per dare istruzioni <i>à la carte</i>.

Il <i>keynote</i> comincia ufficialmente alle 19 italiane. Chi vuole accomodarsi prima, beh, nessun problema. Il posto in <i>chat</i> è comunque di prima fila. :-)