---
title: "Tempi di pulizie"
date: 2008-07-14
draft: false
tags: ["ping"]
---

Usciti dalla pizza, <strong>Vittorio</strong> commentava come su Leopard sia inutile usare utility che forzano manualmente gli script di manutenzione.

Il giorno prima avevo letto in bagno del comando che consente di vedere la data in cui gli script sono stati eseguiti l'ultima volta: <code>ls -l /var/log/*.out</code>.

Viste le circostanze, viene quasi da chiamarlo il comando della naturale regolarità.