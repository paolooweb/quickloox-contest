---
title: "In viaggio con Podmailing"
date: 2008-10-31
draft: false
tags: ["ping"]
---

Le operazioni di trasferimento remoto di un file da 597 mega dal centro Europa a casa mia mediante Podmailing si sono risolte in un nulla di fatto.

<a href="http://www.podmailing.com" target="_blank">Podmailing</a> è molto interessante in potenza, ma ancora in beta e si vede. Meriterà una prova d'appello una volta che si sarà dimostrato più maturo.

Il vecchio <a href="http://www.pando.com" target="_blank">Pando</a> ha portato a termine la missione senza battere ciglio.