---
title: "L'affollamento del numero primo"
date: 2008-11-20
draft: false
tags: ["ping"]
---

Piovono le adesioni alla pizzata Ping numero tre, in programma mercoled&#236; 26 prossimo con ritrovo dopo le 19:30 davanti a <a href="http://atworkgroup.net" target="_blank">Mac@Work</a> e poi consumazione da Bio Solaire, via Terraggio 20, tre minuti a piedi.

Con tutti questi tre in ballo, mi sento di garantire che almeno un numero primo lo raggiungiamo di sicuro. :-))