---
title: "La campagna anti-App Store"
date: 2009-03-26
draft: false
tags: ["ping"]
---

TechCrunch ha scoperto che una certa clausola del contratto che lega sviluppatori e App Store <cite><a href="http://www.techcrunch.com/2009/03/25/apples-iphone-app-refund-policies-could-bankrupt-developers/" target="_blank">potrebbe mandare in bancarotta gli sviluppatori</a></cite>.

La clausola è l&#236; dall'11 luglio, le applicazioni su App Store sono passate da trecento a trentamila (100x) ed effettivamente qualcuno ha fatto soldi a palate con App Store, mentre di gente messa sulla strada colpevole di avere pubblicato un'applicazione per iPhone non si ha notizia.

Sempre a proposito di App Store, a Fortune non pare vero di poter finalmente annunciare che <a href="http://apple20.blogs.fortune.cnn.com/2009/03/26/iphone-app-store-30000-apps-but-slowing/" target="_blank">la crescita di applicazioni su App Store sta rallentando</a> e che, nonostante si siano raggiunte le trentamila applicazioni, il <em>trend</em> sia appunto di rallentamento.

A parte che potrebbe anche essere fisiologico, dopo avere totalizzato trentamila applicazioni in otto mesi, e che magari gli sviluppatori si stanno studiando la nuova beta dell'Sdk anziché sviluppare, non è vero, o almeno non lo era quando è stato scritto. Si ottiene il rallentamento solo considerando i dati temporanei di marzo nel momento in cui mancavano ancora venti giorni a finire il mese. Sarà interessante vedere che cosa è accaduto veramente a fine marzo e comunque che lo sviluppo di applicazioni rallenti a quota trentamila non sembra esattamente questa catastrofe.

Considerato che Microsoft, Research In Motion, Nokia e Google stanno tutte mettendo in piedi il proprio negozio di applicazioni e che vogliono fare concorrenza ad App Store pur partendo con otto mesi di ritardo, viene il dubbio che cercare ogni pretesto possibile di polemica, anche quando non sta in piedi, ecco, forse sia fatto dietro richiesta (finanziata) di qualcuno.