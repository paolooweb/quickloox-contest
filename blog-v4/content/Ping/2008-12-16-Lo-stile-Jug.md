---
title: "Lo stile Jug"
date: 2008-12-16
draft: false
tags: ["ping"]
---

Luned&#236; sera ho avuto il piacere di partecipare al trentesimo <em>meeting</em> del <a href="http://www.jugmilano.it/vqwiki/jsp/Wiki?StartingPoints" target="_blank">Java User Group (Jug) di Milano</a>, che <a href="http://blog.macatwork.net/?p=850" target="_blank">si è tenuto in Mac@Work</a> con regolare pizzata finale.

A parte il piacere di incontrare persone interessanti e assistere a un paio di relazioni di livello troppo alto per me e tuttavia preziose per le briciole di mia possibile competenza, grande è stata la sorpresa di ritrovarmi in posta elettronica un certificato di partecipazione, appena arrivato a casa.

Ho curiosato allora nel sito del Jug e ho scoperto che in realtà è una Wiki con dentro un sacco di cose utili per persone che condividono un interesse comune e mantengono rapporti, anche personali oppure semplicemente mediati attraverso Internet.

La Rete in questo è davvero di una potenza incredibile e non è mai esistito niente di paragonabile prima per allacciare rapporti, conservare amicizie, coordinare incontri, sostenere progetti comuni. Probabilmente il vero salto generazionale è tra chi vede il computer come strumento solipsistico, semplice amplificatore dell'ego, e quanti invece ci montano sopra e saltano in Rete, alla ricerca non tanto (non solo) di roba da scaricare ma di persone con cui condividere interessi ed esperienze.

Devo davvero ringraziare gli amici del Jug e cercherò di approfondire certe dritte, per quanto la mia conoscenza del linguaggio Java si fermi poco oltre il caffè della varietà omonima.

Detto tra noi, tra i partecipanti all'incontro la percentuale di persone con un portatile era molto sopra la media nazionale. Tra queste, la percentuale di Mac era schiacciante. E il campione era di lavoratori dell'informatica, non di tifosi Apple.