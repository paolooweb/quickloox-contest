---
title: "Potenza della wiki"
date: 2010-04-02
draft: false
tags: ["ping"]
---

Mi chiedono se Xpress 6.5 può girare su Snow Leopard.

Frugo su Google e trovo una <a href="http://snowleopard.wikidot.com/" target="_blank">tabella pubblica di compatibilità software con Snow Leopard</a>.

La tabella è una <i>wiki</i>, realizzata dalla comunità. Chiunque può aggiungere informazioni. Se nessuno collaborasse, sarebbe vuota e inutile.

Niente di meglio per mostrare quanto può essere potente la rete se ognuno di noi ci entra per prendere, ma senza dimenticarsi di dare.