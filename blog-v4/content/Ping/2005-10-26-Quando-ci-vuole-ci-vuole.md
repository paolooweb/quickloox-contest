---
title: "Quando ci vuole ci vuole<p>"
date: 2005-10-26
draft: false
tags: ["ping"]
---

Qui sotto compare una parolaccia, ma c&rsquo;è un perché<p>

Sono a Cologno Monzese, sede Apple, presentazione di Aperture. Dopo tante chiacchiere sulla teoria, parte la dimostrazione pratica.<p>

Il dimostratore parte mostrando come Aperture riconosce le sequenze contenute nella scheda di memoria proveniente dalla fotocamera, e le organizza, premettendo un&rsquo;importazione selettiva automatica di gruppi di foto. Mostra come si possa importare da più schede di memoria contemporaneamente, e intanto continuare a lavorare sul materiale già presente.<p>

Mostra la rapidità di generazione di varianti dell&rsquo;immagine master e l&rsquo;intangibilità del master stesso, che non può essere sovrascritto, neanche volendo. Sottolinea il peso ridottissimo delle varianti, che non sono copie modificate dell&rsquo;originale ma frutto di un&rsquo;elaborazione in tempo reale eseguita sull&rsquo;originale, e quindi consistono in piccolissimi file contenenti le istruzioni che generano la differenza di aspetto tra un&rsquo;immagine e un&rsquo;altra.<p>

Mostra le funzioni di lavoro possibili senza interfaccia utente visibile, che sorprendono, e un sacco di altre cose.<p>

A un fotogiornalista discretamente noto presente alla dimostrazione scappa, a mezza voce ma ampiamente discernibile nella saletta, un nitido &ldquo;cazzo&hellip;&rdquo;.<p>

Che Aperture sia un programma interessante per professionisti della fotografia?<p>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>