---
title: "Calma, ragazzi"
date: 2009-02-26
draft: false
tags: ["ping"]
---

Chi cerca per iPhone e iPod touch un gioco rilassante, meditativo, dove l'importante non è raggiungere una meta ma sentirsi sereni alla fine del gioco, ha trovato Zen Bound.

Costa un po' e lo scopo sembra folle se guardato superficialmente (a chi interessa fare girare una corda intorno a sculture di legno?). Eppure, dopo avere guardato <a href="http://zenbound.com/" target="_blank">il filmato del gioco in funzione sul sito del programma</a>, l'ho comprato.