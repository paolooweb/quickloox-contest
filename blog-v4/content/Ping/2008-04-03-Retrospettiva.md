---
title: "Retrospettiva"
date: 2008-04-03
draft: false
tags: ["ping"]
---

Mentre il pesce d'aprile (che poi erano pinguini volanti) della Bbc è stato inarrivabile come al solito, voglio segnalare i quattro pesciotti architettati da Blizzard: il <a href="http://www.worldofwarcraft.com/wrath/features/bard/bardclass.xml" target="_blank">Bardo</a> come nuova classe eroica di World of Warcraft, il <a href="http://www.starcraft2.com/features/terran/taurenmarine.xml" target="_blank">Tauren Marine</a> come nuova unità di StarCraft II, <a href="http://www.worldofwarcraft.com/moltencore/" target="_blank">World of Warcraft su console</a> (e che console!) e la inarrivabile <a href="http://www.blizzard.com/us/diablo2exp/pinata.html" target="_blank">Diablo Loot Pi&#241;ata</a>.

Più che i temi, guardare la fattura, la costruzione. Cos&#236; si lavora.