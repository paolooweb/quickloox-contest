---
title: "Chax, si va in giro"
date: 2008-03-21
draft: false
tags: ["ping"]
---

Mi racconta <a href="http://tolkieniana.net/" target="_blank">Evk</a> che con <a href="http://www.ksuther.com/chax/" target="_blank">Chax</a> riesce a fare condivisione di schermo via iChat bucando <em>firewall</em> e reti (per non fare nomi, FastWeb) che altrimenti gli risultano insuperabili.

Meglio cos&#236; e più sistemi di controllo remoto ci sono, meglio si sta (io uso <a href="http://hamachix.spaceants.net/" target="_blank">HamachiX</a> e, in caso di emergenza totale, <a href="https://secure.logmein.com/home.asp?lang=en" target="_blank">LogMeIn</a>).

Finisce magari che un giorno funziona pure Back to My Mac di Leopard.