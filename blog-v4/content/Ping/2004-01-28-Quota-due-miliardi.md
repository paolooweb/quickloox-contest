---
title: "Quota due miliardi"
date: 2004-01-28
draft: false
tags: ["ping"]
---

Non succedeva dall’inizio del 2000

Qualcuno ha notato che nell’ultimo trimestre fiscale Apple ha totalizzato 2.006.000.000 di dollari in fatturato? È la prima volta che Apple supera i due miliardi dal tempo del fatturato del primo trimestre 2000, che ammontò a 2,34 miliardi di dollari.

Sempre in questo trimestre sono stati venduti 833 mila Macintosh. Non erano così tanti dal trimestre chiuso in settembre 2001, a quota 850 mila.

Non mi piace essere massa, ma non mi dispiacciono le famiglie numerose.

<link>Lucio Bragagnolo</link>lux@mac.com