---
title: "Espulsione ineccepibile"
date: 2009-06-19
draft: false
tags: ["ping"]
---

Apple ha una pagina che riassume tutte <a href="http://www.apple.com/it/macosx/refinements/enhancements-refinements.html" target="_blank">le novità principali di Snow Leopard</a> ma non la traduce ancora in italiano e cos&#236; lo faccio io, una novità per volta.

Espulsione dischi più affidabile

Snow Leopard aumenta l'affidabilità delle espulsioni dei dischi. I servizi base del sistema, come l'indicizzazione Spotlight e gli eventi di filesystem, interromperanno intelligentemente il proprio lavoro e permetteranno l'espulsione del disco. Inoltre finestre di dialogo migliori spiegano quali programmi stanno usando il disco, in modo che si possano chiuderli e niente ostacoli più lo scollegamento sicuro del disco.

Era una delle critiche più rivolte a Leopard. Ha ricevuto il cartellino rosso.