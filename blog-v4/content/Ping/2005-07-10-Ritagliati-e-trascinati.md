---
title: "Ritagliati e trascinati<p>"
date: 2005-07-10
draft: false
tags: ["ping"]
---

Finalmente in Tiger è possibile<p>

Hai presente i clipping, i ritagli che si ottengono per esempio trascinando del testo con un drag and drop sopra la scrivania?<p>

Con Tiger (e prima invece no) è possibile trascinarli sulle icone appropriate del Dock, con qualche utilità.<p>

Trascini un Url e Safari lo carica, trascini un ritaglio di testo e lui lo cerca su Google, lo stesso ritaglio trascinato in BBEdit apre una finestra del programma con dentro il testo in questione e via dicendo.<p>

Ci avrà pensato un programmatore di buona volontà in un&hellip; ritaglio di tempo.<p>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>