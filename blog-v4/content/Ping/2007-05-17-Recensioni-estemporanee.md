---
title: "Recensioni estemporanee"
date: 2007-05-17
draft: false
tags: ["ping"]
---

Mi scrive, graditissimo, <strong>Carmelo</strong>:

<cite>Ciao Lux,</cite>
<cite>ti segnalo le ulteriori migliorie e soddisfazioni che mi sta dando</cite> <a href="http://www.mozilla.com/thunderbird" target="_blank">Thunderbird 2</a><cite>: dopo aver inserito la gestione della mia mail su Gmail e quella del feed Rss di Ping! ho aggiunto anche il feed RSS del</cite> <a href="http://www.poc.it" target="_blank">Poc</a><cite>, è fantastico!</cite>

<cite>E con la nuova versione è adesso possibile configurare liberamente sia per numero che per colore le etichette, una grande comodità.</cite>

<cite>Rispetto alla versione attuale di Mail, Thunderbird è un gran prodotto più facilmente personalizzabile e per di più gratis&#8230; mi sa che sul nuovo Mac, il nuovo Mail 2 resterà nel dock e basta&#8230;</cite>

<cite>Non conosco le potenzialità del tuo</cite> <a href="http://www.barebones.com/products/mailsmith" target="_blank">Mailsmith</a><cite>&#8230; ma direi che adesso la mia gestione della posta è più agile e chissà quante altre cose devo scoprire&#8230; (su quest'indirizzo finora non ho mai ricevuto spam&#8230;).</cite>

Ho tagliato varie cose più private, ché Carmelo è un amico e mi tratta sempre troppo bene, ma il senso e le cose utili sono tutte qui.