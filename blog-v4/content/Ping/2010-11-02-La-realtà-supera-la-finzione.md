---
title: "La realtà supera la finzione"
date: 2010-11-02
draft: false
tags: ["ping"]
---

<cite>Eh, ma tu con la tavoletta non puoi accedere al sistema di pubblicazione</cite>, mi dice uno dei colleghi di riunione.

<cite>Se c'è un indirizzo web non dovrei avere problemi</cite>, rispondo e infatti mi collego.

<cite>Dovresti vedere la pagina intera del sito però&#8230;</cite>

Giro iPad di novanta gradi ed ecco che si vede la pagina intera del sito.

Prosegue la riunione.

<cite>Bisognerebbe avvisare Tizio&#8230;</cite>

<cite>Ci penso io</cite>, rispondo, <cite>per combinazione mi ha appena inviato un messaggio su Skype.</cite>

<cite>Ah, hai già il 4.2 con il</cite> multitasking<cite>?</cite>

<cite>No</cite>, rispondo, <cite>basta avere accese le notifiche. Non vedo l'ora del</cite> multitasking<cite>, ma non è indispensabile, solo comodo.</cite>

<cite>Lucio, non hai qui quell'impaginato&#8230;?</cite>

<cite>Se me lo dicevate venivo con il Mac&#8230;</cite> (momento a bassa temperatura)

<cite>&#8230;però mi collego con il Mac a casa e te lo spedisco.</cite>

(lancio <a href="http://www.teamviewer.com" target="_blank">TeamViewer</a>, effettivamente mi collego e spedisco il file al tizio che lo riceve pochi istanti dopo ed è visibilmente imbarazzato; mi conferma la ricezione a mezza bocca cercando di non farsi notare)

Finisce la riunione. Facciamo girare una mail collettiva. <cite>Riesci a riceverla subito o devi arrivare a casa?</cite>

<cite>Guarda che mi basterebbe un iPod touch per ricevere una mail</cite>, penso (senza dirlo).

Lo so che sembra un racconto inventato.