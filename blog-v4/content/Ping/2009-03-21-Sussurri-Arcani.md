---
title: "Sussurri Arcani"
date: 2009-03-21
draft: false
tags: ["ping"]
---

Grazie pubblico a <strong>Giorgio</strong> di <a href="http://worldoftwilights.altervista.org" target="_blank">World of Twilights</a>, rumorista per giochi di ruolo, che a seguito di un'intervista su Macworld mi ha inviato i Cd con la sua produzione.

I rumori sono davvero indovinati e saranno presto collaudati sul campo. Di gioco, naturalmente.