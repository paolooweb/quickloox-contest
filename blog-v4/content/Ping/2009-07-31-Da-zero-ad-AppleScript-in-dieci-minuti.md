---
title: "Da zero ad AppleScript in dieci minuti"
date: 2009-07-31
draft: false
tags: ["ping"]
---

Come da richiesta di <b>Massimiliano</b>, alcune indicazioni veramente essenziali per cominciare lo studio di AppleScript.

Per cominciare proprio da zero, &#8220;scoprire&#8221; i pezzi di AppleScript già pronti dentro il Mac, in /Libreria/Scripts. Lanciare /Sistema/Libreria/CoreServices/Menu Extras/Script Menu.menu attiva un menu grafico sulla barra di Mac OS X che li rende accessibili in qualunque istante. Ci sono dentro mattoni tipici da costruzione che servono in moltissimi casi. Non sono sempre di facilissima interpretazione, ma spesso risolvono problemi concettuali di base, tipo &#8220;come si apre un ciclo&#8221; o &#8220;come si fa apparire una finestra di dialogo&#8221;.

Secondo, la <a href="http://www.macosxautomation.com/applescript" target="_blank">pagina ufficiale di Apple su AppleScript</a>. c'è un sacco di materiale utile, compresa la AppleScript Language Guide. Tutto è in inglese ed è meglio cos&#236;: pensare di programmare senza sapere almeno un pizzico di inglese è una strada senza uscita. Meglio saperlo subito. Ottima scusa per imparare un pizzico di inglese!

Terzo, <a href="http://macscripter.net" target="_blank">MacScripter</a>, una risorsa indipendente con un mare di script già pronti; si impara tantissimo copiando e riciclando il lavoro già fatto dagli altri e anche chiedendo aiuto e informazioni.

Infine, sempre cercare in rete. Via Google si arriva a pezzi di AppleScript un po' dovunque. Amo dire provocatoriamente che tutti gli script possibili sono già stati realizzati e si tratta solo di trovarli. :-) Non è vero, ovviamente. Vero è che spesso qualcuno ha già iniziato il lavoro e possiamo approfittarne.

Per chi inizia a sviluppare un interesse più serio per AppleScript, c'è da scaricare <a href="http://www.satimage.fr/software/en" target="_blank">Smile</a>, ambiente di lavoro AppleScript assai più potente di Script Editor (c'è anche in versione gratuita).

Un passo ancora successivo è installare Xcode (di serie nel disco di installazione di Mac OS X) e scoprire AppleScript Studio. Xcode abbonda di documentazione ed esempi pronti.

Chi se la sente può iscriversi a <a href="http://listserv.dartmouth.edu/scripts/wa.exe?A0=MACSCRPT" target="_blank">Macscrpt</a>, lista di discussione Macscrpt ospitata dall'università di Dartmouth list. Ci sono liste in italiano su <a href="http://lists.presso.net/mailman/listinfo/tevac-applescript" target="_blank">Tevac</a> e su <a href="http://www.freelists.org/archive/xcodeitalia/" target="_blank">Xcodeitalia</a>.

Il libro migliore su AppleScript al momento è <a href="http://www.ibs.it/ame/book/9780321149312/soghoian--sal/applescript-3.html" target="_blank">AppleScript 1-2-3 di Sal Soghoian e Bill Cheeseman</a>, sempre in inglese ma ordinabile su Ibs.

Altro su ordinazione. :)