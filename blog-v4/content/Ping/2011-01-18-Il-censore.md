---
title: "Il censore"
date: 2011-01-18
draft: false
tags: ["ping"]
---

Mi dolgo e mi confesso pubblicamente: ho cestinato un commento su Ping.

La vittima è stata avvisata, avvisata, avvisata, ammonita, allertata. Ma non è servito.

È la prima o la seconda volta, in anni di storia, che succede una cosa del genere su Ping. Vorrei con tutto il cuore che non accadesse mai più e che, nel contempo, si potesse chiacchierare serenamente senza approfittare dell'attenzione degli altri per lanciare i propri proclami, di qualsiasi parte. Il proclama ci sta bene anzi benissimo, che però rispetti il contesto e le intelligenze degli altri.

Avrei preferito parlare di Mac, iOS e Apple, oggi. Spero che questa sia una parentesi eccezionale, da chiudere oggi e per sempre. Grazie. :-)