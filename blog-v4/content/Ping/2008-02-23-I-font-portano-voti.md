---
title: "I font portano voti"
date: 2008-02-23
draft: false
tags: ["ping"]
---

La campagna elettorale americana si decide anche sull'uso dei font, con approvazioni varie per il <a href="http://www.helveticafilm.com/blog/2008/02/19/a-font-we-can-believe-in/" target="_blank">Gotham scelto da Barack Obama</a> e le critiche, anche feroci, alle <a href="http://www.typography.com/ask/showBlog.php?blogID=79" target="_blank">scelte tipografiche di Hillary Clinton e John McCain</a>.

Ripenso con un pizzico di orgoglio a quella volta che, tanti anni fa, di fronte all'ennesima stampata, dissi <em>mai più Times e Helvetica</em>. E fu Palatino, come ancora oggi.

Adesso più che altro la piaga sociale è l'Arial, ma è un'altra storia. Meno elettorale.