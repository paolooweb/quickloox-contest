---
title: "Mica è un gioco"
date: 2003-04-18
draft: false
tags: ["ping"]
---

Per nome e sito EscapePod parrebbe questione ludica. In realtà è l’utility dell’anno

Mac OS X ha la memoria protetta ed è un gran progresso, perché quasi sempre, se un programma crolla, il resto del sistema resta in piedi.

Ma che succede se a crollare è l’interfaccia utente e, per esempio, non si muove più il mouse? Oppure un salvaschermo maleducato si rifiuta di farci rientrare, come Hal 9000 con Dave Bowman?

C’è una soluzione splendida che si chiama EscapePod e permette, con una combinazione di tasti, di uccidere il processo in primo piano oppure il Dock senza bisogno di arrivare al Terminale o a un qualunque altro programma attivo.

EscapePod è una piccolissima utility ma a mio parere è anche una delle cose migliori prodotta per Mac OS X. Sarà che ho gusti semplici.

La trovi sul sito di <link>Ambrosia Software</link>http://www.ambrosiasw.com. Non dico esattamente dove, così fai un giro per il sito e vedi che ci sono anche un sacco di giochi splendidi a prezzi per ogni tasca.

<link>Lucio Bragagnolo</link>lux@mac.com