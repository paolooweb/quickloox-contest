---
title: "Un'utopia leggibile"
date: 2008-01-25
draft: false
tags: ["ping"]
---

Ho passato l'età in cui si crede alle utopie. i Grandi Progetti che non hanno una data precisa di compimento e un calendario operativo altrettanto preciso sono solo Fumo negli Occhi.

Il progetto reCaptcha fa eccezione, perché è un'utopia rilassante, leggera e digeribile, elegante nella sua semplicità.

Perché non utilizzare per alti scopi, infatti, tutte quelle strane parole di conferma richieste dai siti per autenticarsi, altrimenti sprecate?

<strong>Beppe</strong>, che me lo ha segnalato, è stato cos&#236; gentile da linkare il <a href="http://recaptcha.net/whyrecaptcha.html" target="_blank">sito originale del progetto</a> e un <a href="http://www.internazionale.it/firme/articolo.php?id=18019" target="_blank">articolo chiarificatore</a> di <a href="http://www.wittgenstein.it" target="_blank">Luca Sofri</a>.

Se hai un blog, o un sito personale, è un'idea che risolve pure un problema. Partecipare è difficile come aggiungere quattro righe di codice già pronte alle proprie pagine, cioè è facile.

P.S.: non sperare di vedere cambiamenti a questo proposito su questo blog. Più facile che la cruna di un ago passi attraverso un cammello.