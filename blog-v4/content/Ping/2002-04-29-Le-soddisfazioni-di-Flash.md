---
title: "Le soddisfazioni di Flash"
date: 2002-04-29
draft: false
tags: ["ping"]
---

Una vera perla, girata su un’ottima mailing list

Luca Accomazzi alias Misterakko non ha bisogno di presentazioni. Mi onoro di copiare un suo intervento dalla lista Misterakko, riguardante le soddisfazioni che dà Flash su Internet dal punto di vista del costruttore di siti:

“Moltissime. Le principali: i motori di ricerca non ti segnaleranno mai; i tuoi visitatori non possono usare i bookmark; i portatori di handicap trovano il sito inutilizzabile; tutti i visitatori che usano un browser vecchio, un palmare, un telefonino, un browser poco diffuso trovano il sito irraggiungibile (e vanno altrove); hai una possibilità aumentata dell'85% secondo le più recenti statistiche che i tuoi visitatori chiudano la pagina mentre ancora sta cominciando a caricare [...].
Flash tecnologicamente è una grandiosa realizzazione. Ma è immatura, soffre di moltissime limitazioni e soprattutto viene usata nel 99,9% dei casi assolutamente a sproposito e tipicamente da incompetenti”.

Per chi voglia approfondire, o iscriversi a Misterakko, tutto quello che c’è da sapere è <link>qui</link>http://www.accomazzi.net.

<link>Lucio Bragagnolo</link>lux@mac.com