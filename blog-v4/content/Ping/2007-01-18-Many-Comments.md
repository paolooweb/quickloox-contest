---
title: "Many Comments"
date: 2007-01-18
draft: false
tags: ["ping"]
---

<strong>Carlo</strong> di <a href="http://freesmug.org/" target="_blank">FreeSmug</a> ha avuto una bellissima idea: <a href="http://magsafecounter.blogspot.com/" target="_blank">raccogliere attraverso un blog</a> i commenti di quanti hanno avuto la vita (del portatile!) salvata dal connettore magnetico <a href="http://www.apple.com/macbookpro/design.html" target="_blank">MagSafe</a> che si sgancia da solo dal portatile, dove invece nel passato si rischiava l'incidente fatale.

Non sono ancora passato a Intel e non posso quindi contribuire. Ma il mio Titanium si ricorda ancora le due cadute causate dall'imbecille che si è tirato dietro il cavo, e che non vedo in faccia solo perché il mio schermo non è <em>glossy</em>.