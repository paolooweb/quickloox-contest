---
title: "Terremoto annunciato"
date: 2006-04-16
draft: false
tags: ["ping"]
---

Adesso che i Mac possono ridursi a fare il boot da Windows, si rischia che non escano pi&ugrave; giochi, chiacchierano.

Intanto Aspyr ha mantenuto le promesse e fatto uscire <a href="http://www.aspyr.com/games.php/mac/quake4/" target="_blank">Quake 4 per Mac</a>, pure Universal.

Lora, pi&ugrave; che chiacchierare, sembra che lavorino.