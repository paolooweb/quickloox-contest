---
title: "Un servitore fedele non va in pensione"
date: 2006-09-27
draft: false
tags: ["ping"]
---

Certamente è solo un'azione dimostrativa, ma <a href="http://www.lisa2.com/" target="_blank">questo sito</a> viene erogato da un Lisa 2, 23 anni di età.

Le pagine sono da leggere; qualcuno, giorni fa, chiedeva che cosa fa Lisa che non faccia oggi un Mac. In certi casi viene da chiedersi come mai c'è voluto tanto tempo per fare cose che già erano alla portata di Lisa. E si parla di Mac, che appena nato era più avanti del suo tempo. Figuriamoci la concorrenza.