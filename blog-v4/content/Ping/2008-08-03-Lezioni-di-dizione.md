---
title: "Lezioni di dizione"
date: 2008-08-03
draft: false
tags: ["ping"]
---

Rimane in molti la convinzione di venire truffati quando leggono sulla confezione <em>320GB</em> e poi, dentro Utility Disco, vedono invece tipo <em>298GB</em>.

Standard alla mano, la scritta sulla confezione parla di <em>gigabyte</em> e i gigabyte sono miliardi di byte. 320 gigabyte sono all'incirca 320 miliardi di byte.

C'è un errore formale invece in Utility Disco e, se è per questo, nell'intera interfaccia grafica di Mac OS X. Infatti le dimensioni sono espresse non in gigabyte (10^9 byte), bens&#236; in <em>gibibyte</em> (2^30 byte). Al posto di GB si dovrebbe vedere <em>Gi</em>.

Per vedere ristabilita la correttezza delle cose bisogna calarsi nel Terminale e digitare <code>diskutil list</code>. Almeno l&#236; la dizione è quella giusta.