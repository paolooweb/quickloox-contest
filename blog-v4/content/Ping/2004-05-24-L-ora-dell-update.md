---
title: "L’ora dell’update"
date: 2004-05-24
draft: false
tags: ["ping"]
---

Un po’ furbetta un po’ comoda, secondo le esigenze

Non so se ci hai fatto caso, ma Apple ama pubblicare i suoi update, soprattutto quelli di sicurezza, nel tardo pomeriggio del venerdì (californiano).

Da una parte la stampa non può occuparsene troppo, che è da furbetti; dall’altra le aziende hanno il respiro giusto per fare qualche prova e assicurarsi che comunque il lunedì possa riprendere tutto senza problemi.

A ognuno il suo giudizio!

<link>Lucio Bragagnolo</link>lux@mac.com