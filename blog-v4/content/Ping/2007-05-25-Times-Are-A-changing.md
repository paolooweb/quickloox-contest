---
title: "Times Are A-changin'"
date: 2007-05-25
draft: false
tags: ["ping"]
---

Ho la sensazione che ne farò un articolo, perché sento che l'era delle mailing list si sta avviando alla fine.

Quelle che frequento sono oramai alla frutta, infestate da rumore di fondo alla massima potenza. I <em>free rider</em> impazzano, tutti conservano archivi sterminati e nessuno li consulta prima di ripetere una domanda cui si è già risposto decine di volte, si è persa qualsiasi traccia della distinzione tra la critica a un'idea e la critica a una persona (posso considerarti intelligente ma trovare la tua idea stupida, e questo non vuol dire che io ti stia dando dello stupido), la media della cultura informatica e della capacità di usare hardware e software è in caduta drammaticamente libera.

Entriamo nell'era dei forum, meravigliosi depositi di vuoto pneumatico. Si possono trovare su Internet forum interamente popolati da avatar animati, firme glitterate, signature chilometriche che servono più a rafforzare l'autostima che non a dire qualcosa. Frequente il messaggio che occupa 5 kilobyte tra testo e grafica, per dire complessivamente <em>:-)</em>.

Non è questione generazionale. Quando avevo quindici anni le agende delle mie amiche erano piene di firme enormi, rotonde, decorate all'impossibile. Niente di più normale che la cosa si ripeta dove si scrive adesso da parte delle quindicenni di adesso. La tragedia è quella dei trenta-cinquantenni, che ti chiedono qual è il software più affidabile, <em>possibilmente gratis</em> (leggi: comunque gratis), per fare in tempo zero a livello professionale qualcosa di cui non hanno la minima idea di dove e come cominciare. Gente che smadonna per mezza giornata su problemi impossibili dando retta a qualunque parere strampalato venga scritto, e intanto vedo la risposta spiegata sulla prima rivista Mac che apro. Ma mezza giornata pare valere meno di cinque euro.

Non è neanche lo spot per vendere Macworld o qualunque altra rivista. Nessuna rivista può arrivare alle contorsioni mentali di chi si improvvisa muratore e si butta, entusiasta, a costruire il tetto chiedendo quale sia la cazzuola più affidabile.

Ma il vero peggio è che devo aggiungere: specialmente in Italia.