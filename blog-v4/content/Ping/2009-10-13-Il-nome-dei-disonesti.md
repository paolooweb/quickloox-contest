---
title: "Il nome dei disonesti"
date: 2009-10-13
draft: false
tags: ["ping"]
---

Quando si leggerà delle prestazioni grafiche di Windows 7, occhio ai <i>driver</i>: se sono quelli Intel e se il test è 3D Mark Vantage di <a href="http://www.futuremark.com/" target="_blank">Futuremark</a>, Intel bara e i risultati sono bugiardi.

Il <i>software</i> Intel controlla se il file in esecuzione è quello del test e, se s&#236;, chiama in causa il processore centrale per gonfiare le prestazioni.

<a href="http://techreport.com/articles.x/17732" target="_blank">Lo mostra The Tech Report</a> in modo veramente banale: eseguono il test, poi cambiano nome al file e rieseguono il test. I risultati sono clamorosamente diversi.

Semplicemente, il <i>driver</i> Intel si basa sul nome del test. Se gli fai eseguire lo stesso test con un nome diverso, non si accorge che è un test e quindi non bara. The Tech Report mostra che questo tipo di &#8220;intelligenza&#8221; viene applicato anche a diversi videogiochi, spesso impiegati per valutare le prestazioni grafiche. Quando il <i>driver</i> Intel riconosce il videogioco dal nome, bara. Quando il videogioco è lo stesso, ma cambia nome, le prestazioni sono nettamente inferiori.

Nel dubbio, barare in questo modo è espressamente vietato dalle <a href="http://www.futuremark.com/pressroom/companypdfs/3DMark_Vantage_Driver_Approval_Policy_v101R2.pdf" target="_blank">linee guida del test</a>.

Adesso non resta che chiedersi in quali altre occasioni Intel e Windows barano, solo che nessuno ci ha mai badato.