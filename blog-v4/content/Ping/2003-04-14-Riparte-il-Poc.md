---
title: "Riparte il Poc"
date: 2003-04-14
draft: false
tags: ["ping"]
---

Dopo un momento di stasi, ritorna l’Amug più grande d’Italia

Modestamente, ho potuto assistere alla messa online del sito del PowerBook Owners Club, riveduto e corretto, con un motore di gestione tutto nuovo fatto in casa e nuova attenzione per i contenuti.

Modestamente, ero presente quando venivano prese le decisioni che rivitalizzano il servizio di posta @poc.it, il PocHelp e tutti i servizi che il Poc ha sempre offerto e per qualche tempo erano stati un po’ persi di vista dai radar.

Il <link>Poc</link>http://www.poc.it è una parte significativa e importante della storia della comunità Apple in Italia e vederlo ripartire con nuovo slancio e nuova linfa vitale fa solo piacere.

E, se devo essere modesto su tutto il resto, di vederlo ripartire sono piuttosto orgoglioso.

<link>Lucio Bragagnolo</link>lux@mac.com