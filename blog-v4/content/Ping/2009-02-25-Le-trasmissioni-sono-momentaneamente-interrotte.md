---
title: "Le trasmissioni sono momentaneamente interrotte"
date: 2009-02-25
draft: false
tags: ["ping"]
---

Il MacBook Pro 17&#8221; è arrivato adesso. Il collo proviene direttamente dalla Cina, tipico delle primissime spedizioni. Esattamente da Tech-Com Computer, Rong Xing Rd., Songjiang Ex Cn Shanghai, qualsiasi cosa significhi.

Tornerò in linea, se tutto va come deve, appena me lo consente Assistente Migrazione.