---
title: "Mio papà e Oliviero<p>"
date: 2005-10-26
draft: false
tags: ["ping"]
---

Messo in altri termini: iPhoto e Aperture<p>

Sono stato a casa dei miei. Mio papà ha praticamente completato la conversione digitale del suo archivio di diapositive, che datano dal 1960 a oggi. Si è armato di pazienza, di uno scanner per diapositive, e di iPhoto.<p>

Dopo diverse migliaia di immagini si è accorto che il suo iMac Dalmatian faceva un po&rsquo; fatica e, in realtà, finiva per fare più fatica lui. Allora ha preso un iMac G5 e ci ha messo dentro un giga di Ram. Adesso quelle diecimila fotografie si gestiscono a ritmo godibile anche da lui, che pure non è tipo da esigere il computer veloce.<p>

Ma sempre con iPhoto. Mio papà è felicemente e meritatamente pensionato. Ha tutto il tempo che si vuole riservare. Può prendersi il lusso di passare una serata a togliere un viraggio da una vecchia foto cui tiene particolarmente. I suoi album, organizzati per temi, persone, cronologia, luoghi, eventi, elementi di paesaggio, sono fatti a suo gusto e non deve renderne conto a nessuno. iPhoto è perfetto, per mio papà e le sue diecimila foto.<p>

Per Oliviero Toscani è un po&rsquo; diverso. Lui di foto ne ha centomila, ha clienti che pretendono la luna per domani mattina, assistenti da coordinare, decisioni da prendere al volo, centinaia di scatti per scegliere una singola immagine in pochissimo tempo, quella che può valere un milione di euro di commessa.<p>

Ecco perché lui, se è astuto, acquisterà <a href="http://www.apple.com/it/aperture">Aperture</a>. E come lui tanti fotografi che non finiscono sui giornali ma vivono ugualmente di fotografia e di talento, e hanno pari pari le stesse necessità, anche se gli euro della commessa sono centomila, o cento.<p>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>