---
title: "Domande da non porsi<p>"
date: 2005-12-17
draft: false
tags: ["ping"]
---

Tipo: dove trovo tutti i comandi Unix per Mac OS X?<p>

Non chiedere un manuale di istruzioni per usare il Terminale. Ti ritrovi con un elenco di ottocento pagine, necessariamente imcompleto. Molto meglio un sito che riporta i <a href="http://www.westwind.com/reference/OS-X/commandline/">comandi veramente utili</a>.<p>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>