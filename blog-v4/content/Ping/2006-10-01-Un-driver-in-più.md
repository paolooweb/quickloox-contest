---
title: "Un driver in più"
date: 2006-10-01
draft: false
tags: ["ping"]
---

Mi chiedono se conosco il nome di uno sviluppatore in grado di creare un driver per un lettore di smart card in ambiente Mac OS X. Prestazione retribuita, mi dicono. Azienda seria, mi dicono.

La richiesta arriva da persona fidatissima e non ho problemi a dargli fiducia.

Se qualcuno è interessato mi <a href="mailto:lux@poc.it?subject=Sviluppatore%20per%20driver" target="_blank">scriva</a>. Grazie in anticipo!