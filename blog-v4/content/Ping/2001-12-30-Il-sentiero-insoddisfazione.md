---
title: "Il sentiero dell’insoddisfazione"
date: 2001-12-30
draft: false
tags: ["ping"]
---

Apple ha cresciuto, tra tanti utenti soddisfatti, una piccola schiera di scontenti a oltranza. Macworld è la loro vita

Scenario: San Francisco, Macworld gennaio 2002. Apple annuncia che so, un iMac con lo schermo pieghevole.
L’Utente Scontento: “Ma come! Ho comprato un iMac due settimane fa! Dovevano avvisarmi. L’hanno fatto apposta... Apple si comporta scorrettamente. E poi costa troppo”.
Scenario: sempre quello, ma Apple annuncia prodotti normali.
L’Utente Scontento: “Ecco, una volta Apple innovava, adesso fa computer come tutti gli altri. Perché non si dà una mossa? Sono già tre mesi e mezzo che non esce un nuovo (prodotto a scelta). Apple non è più quella di una volta”.
Scenario: Apple rinnova tutte le linee di prodotto tranne una.
L’Utente Scontento: “Sono delusissimo. Neanche un progresso sulla gamma (a scelta)... Apple sta perdendo vivacità...”
Morale: l’Utente Scontento lo fa per professione. Tenerne conto, e ignorarlo.

lux@mac.com