---
title: "Un programma outsider"
date: 2006-04-16
draft: false
tags: ["ping"]
---

<strong>Jacopo</strong> mi ha ricordato che avevo raccomandato <a href="http://www.barebones.com/products/yojimbo" target="_blank">Yojimbo</a> qualche tempo fa, al momento dell’annuncio… e ora il database for the rest of them, intesi i them come tutti i frammenti di informazione che non sapremmo dove archiviare, è al primo posto nelle segnalazioni di <a href="http://blog.guykawasaki.com/" target="_blank">Guy Kawasaki</a>.

Se lo dico io non conta niente. Ma se lo dice Kawasaki e non lo sai, ti stai perdendo qualcosa.