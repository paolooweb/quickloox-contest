---
title: "Gli Eroici"
date: 2006-04-05
draft: false
tags: ["ping"]
---

Torno su <a href="http://www.allaboutapple.com/" target="_blank">All About Apple</a>, perch&eacute; il club &egrave; uno dei pi&ugrave; vivi e attivi sulla scena Mac italiana, anzi, direi il pi&ugrave; vivo e vitale di tutti, senza offesa per nessuno.

Stasera organizzano per le 21, all&rsquo;Hotel Garden in viale Faraggiana ad Albissola Marina (SV), una <a href="http://www.allaboutapple.com/ospiti/hernan/concerto.jpg" target="_blank">lezione-concerto</a> del pianista Hernan Laurentis dal titolo <em>I procedimenti dell&rsquo;unit&agrave; in Beethoven</em>.

Che c&rsquo;entra Beethoven con il Mac? In tempi di Amici di Maria de Filippi, con tutto il rispetto, questo &egrave; <em>think different</em> ed &egrave; anche cambiare il mondo&hellip; una sonata alla volta. :-D