---
title: "Niubbi e snobbi"
date: 2010-11-04
draft: false
tags: ["ping"]
---

Sto insegnando Mac più del solito in questo periodo e mi rendo conto del divario immenso che c'è tra noi cosiddetti alfabetizzati informatici e chi invece non possiede questo bagaglio di nozioni.

Siamo cresciuti educati ai file, alle cartelle, ai percorsi, ai <i>driver</i>, ai menu contestuali, alle interfacce di configurazione. Poi incontri qualcuno (un <a href="http://nonciclopedia.wikia.com/wiki/Niubbo" target="_blank">niubbo</a>, lo si chiama nei forum) che non ne sa niente e ti chiede dove stanno i documenti. Cominci a rispondere e ti rendi conto che la risposta cercata non è una dissertazione sugli alberi di <i>directory</i>, ma la rivelazione che nei computer esiste un coso che immagazzina i dati e non li perde al momento di spegnere. La differenza tra memoria di massa, disco, e memoria di lavoro, Ram, può costituire una traversata concettuale pari a quella di Colombo con le caravelle.

Sento gli snob che schifano le funzioni prossime venture di <a href="http://www.apple.com/it/macosx/lion/" target="_blank">Lion</a>: Launchpad, Mission Control, Mac App Store. <i>Sono robe troppo semplici</i>, <i>si sacrificano i professionisti</i>, <i>giocattoli per bambini</i> e grazie al cielo che non si aggiunge il suffisso <i>scemi</i>.

La verità è un'altra. Lion, che sarà più semplice di Snow Leopard, sarà ancora troppo difficile. Siamo solo noi, nati meccanici per necessità, a non renderci conto che smanettare dentro il motore non ci fa andare più lontano, solo passare più tempo in officina. E se sei professionista, va là che Photoshop o FileMaker ci saranno sempre (sai che AutoCad è tornato dopo tredici anni di assenza, proprio ora che il Mac <i>sacrifica i professionisti</i>?). Invece che lamentarti, impara un po' di Terminale. Se proprio snob, almeno con ragione.