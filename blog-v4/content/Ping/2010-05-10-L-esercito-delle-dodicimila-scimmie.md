---
title: "L'esercito delle dodicimila scimmie"
date: 2010-05-10
draft: false
tags: ["ping"]
---

Scrive un amico che chiede di restare anonimo.

<cite>Che vuol dire che non mi possono mettere il Mac a dominio?</cite>

<cite>Lo dice il tecnico dell'ufficio, rete Windows. Devo accedere al server ma una cartella è nascosta e per vederla deve &#8220;mettermi a dominio&#8221;.</cite>

<i>Mettere il Mac a dominio</i>, in Snow Leopard, significa lanciare Utility Directory e inserire i giusti parametri. Il programma sta in un posto particolare, non in <code>/Applicazioni/Utility</code> come verrebbe naturale pensare, bens&#236; in <code>/Sistema/Libreria/CoreServices</code>. Il <i>tecnico dell'ufficio</i>, tuttavia, lo dovrebbe sapere. Altrimenti non è un buon tecnico.

Ogni tanto capita di leggere qualche articolo sul Mac che fa fatica ad affermarsi nelle aziende. Vi si legge invariabilmente che ciò è dovuto alla funzione <i>x</i> oppure alla sua mancanza. Altrimenti si argomenta per lenzuolate sui costi totali di proprietà e fumosità assortite.

Di questo esercito di ominidi, sempre pronti a chiudersi occhi orecchie bocca e pure cervello quando si tratta di lavorare, non leggo mai.