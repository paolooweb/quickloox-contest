---
title: "Programmazione grafica"
date: 2010-01-28
draft: false
tags: ["ping"]
---

A causa di un piccolo cambiamento nel codice della pagina, i flussi di lavoro Automator che si trovano su Internet e che promettono di impostare l'immagine di sfondo della scrivania con la Astronomy Picture of the Day fornita quotidianamente dalla Nasa molte volte funzionano male.

Questo invece funziona.

Suggerisco un piccolo esercizio: lanciare Automator, creare un nuovo flusso di lavoro e ricreare quello che si vede nell'immagine. I mattoni da costruzione si prendono nell'elenco a sinistra (c'è anche un campo per digitare un termine di ricerca e arrivarci subito). I mattoni vanno posizionati dall'alto verso il basso e riempiti dei dati giusti ove opportuno.

Alla fine si salva il tutto come si vuole, oppure si fa clic sul pulsante Esegui per collaudarlo.

I pigri mi scrivano e glielo mando in posta. :-)

P.S. Non è ancora la stessa programmazione grafica del progetto <a href="http://groups.csail.mit.edu/uid/sikuli/" target="_blank">Sikuli</a>. Però Automator è l'unico sistema di programmazione dove ha senso mostrare un programma con una schermata.