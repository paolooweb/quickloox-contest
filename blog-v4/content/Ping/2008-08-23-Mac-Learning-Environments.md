---
title: "Imparare a insegnare"
date: 2008-08-23
draft: false
tags: ["ping"]
---

Su preziosa segnalazione di <a href="http://hardclicker.com" target="_blank">Stefano</a>, riferisco la produzione pubblicata da iTunes U sui <a href="http://deimos3.apple.com/WebObjects/Core.woa/Feed/new.duke.edu.1302914143.01302914153" target="_blank">Mac Learning Environments</a>: resoconti di come si fa istruzione usando hardware e software Apple.

L'audio è in inglese, ma c'è anche il video&#8230; è interessante anche solo guardare le figure. :-)