---
title: "Se me lo dicevi prima"
date: 2010-10-25
draft: false
tags: ["ping"]
---

La canzone omonima di Enzo Jannacci ha questo titolo, tanto utile per spiegare succintamente a un interlocutore che con un minimo di preavviso si sarebbero potuti avere esiti certamente migliori.

Per esempio: dopo tutto il can can sulla diatriba tra Flash e Html5, più aperto uno o più chiuso quell'altro, la libertà di vedere le cose in Flash contro la cattiveria di Apple, uno legge che Adobe ha messo a punto <a href="http://blogs.adobe.com/dreamweaver/2010/10/adobe-announces-the-html5-video-player-widget.html" target="_blank">un player di video Html5</a>.

Bisogna avere installato Air ed è meglio avere in casa un cane malato di rabbia, ma è un <i>player</i> video Html5, che punta su Flash solo se il <i>browser</i> utilizzato non è moderno e non supporta i tag giusti.

Se ce lo dicevano prima&#8230;