---
title: "Amo il rischio e non lo sa"
date: 2009-05-26
draft: false
tags: ["ping"]
---

Da tempo lavoro sulle versioni preliminari di Snow Leopard, che ho sistematicamente installato su un disco esterno per non mettere a repentaglio i dati sul sistema quello vero. Non si espongono mai i dati di lavoro a una versione provvisoria di un sistema operativo, per quanti <em>backup</em> uno possa avere.

Fino a che per un'emergenza non mi è servito quel disco esterno l&#236;.

Al che ho deciso di rimediare installando Snow Leopard su una partizione apposita del disco interno. Soluzione lievemente meno sicura che lavorare su un disco fisicamente diverso, ma ragionevolmente affidabile. La partizione sarebbe stata ricavata ridimensionando quella di lavoro esistente, con Utilità Disco. Anche questa operazione non da provare tutti i giorni, ma sufficientemente affidabile, specie su un disco relativamente recente come quello del mio MacBook Pro, che ha ancora tanto spazio libero.

Procedo un po' distrattamente. Troppo distrattamente. A metà del processo mi rendo conto che ho usato s&#236; Utilità Disco, ma quello di Snow Leopard, con cui prima avevo effettuato il <em>boot</em>.

Tutto ragionevolmente affidabile, ma quell'Utilità Disco arrivava da una versione di sistema preliminare, non pronta per l'uso quotidiano, piena di problemi da sistemare. E le stavo facendo ridimensionare la partizione del mio disco di lavoro principale.

Niente di gravissimo, perché da quando esiste Time Machine ho sempre un <em>backup</em>. Tuttavia passare mezza giornata a ripristinare il sistema non era esattamente la mia massima aspirazione.

Una volta effettuato con successo un <em>boot</em> nella partizione Leopard e constatato che tutto era andato bene, ho smesso di sudare freddo e devo anche avere ripreso a respirare normalmente.

Solitamente preferisco prendermi rischi sapendolo, ecco.