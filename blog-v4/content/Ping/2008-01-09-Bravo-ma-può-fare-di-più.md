---
title: "Bravo, ma può fare di più"
date: 2008-01-09
draft: false
tags: ["ping"]
---

Ho scoperto che dentro la finestra galleggiante di Impostazioni di Pages il conteggio caratteri si aggiorna in tempo reale e basta tenere aperta la finestra suddetta per sapere in ogni momento quanto ho scritto.

Ma non basta ancora. Lo voglio nella finestra del documento, come fanno <a href="http://www.tex-edit.com" target="_blank">Tex-Edit Plus</a> e <a href="http://www.bean-osx.com/Bean.html" target="_blank">Bean</a>.