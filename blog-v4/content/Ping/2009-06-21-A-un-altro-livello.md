---
title: "A un altro livello"
date: 2009-06-21
draft: false
tags: ["ping"]
---

Uno dei luoghi che frequento sovente come nomade digitale è ora dotato di una frugale postazione stabile, composta da un <a href="http://www.griffintechnology.com/products/elevator" target="_blank">Elevator Griffin</a> e da una <a href="http://www.apple.com/it/keyboard/" target="_blank">tastiera wireless Apple</a>.

L'impressione è molto buona per ambedue gli oggetti.

Elevator è semplice ed elegante, ma più di questo è stabile e sicuro anche con il MacBook Pro 17&#8221;, che a parte le dimensioni supera i tre chili. Non scivola e contemporaneamente si sposta, se desiderato, con la facilità giusta. L'ingegnerizzazione consente di riporlo, smontato, in una scatola ultrapiatta se necessario. Montarlo è questione di un momento. È una soluzione non regolabile, che però a occhio sembra soddisfacente nella gran parte delle situazioni; il portatile viene elevato perché la posizione dello schermo sia più ergonomica.

Avevo già provato la tastiera wireless. Adesso ci sono stato sopra per un paio di giorni e la dichiaro al di là del bene e del male. Il tocco è eccellente, superiore a quello delle tastiere dei portatili; contemporaneamente le misure sono le stesse e ciò rende assolutamente indolore il passaggio dall'una all'altra tastiera. La connessione Bluetooth non ha dato alcun intoppo e l'insieme di portatile e tastiera è perfettamente integrato.

Tutto largamente raccomandato.