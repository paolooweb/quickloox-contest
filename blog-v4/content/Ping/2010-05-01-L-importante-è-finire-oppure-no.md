---
title: "L'importante è finire. Oppure no"
date: 2010-05-01
draft: false
tags: ["ping"]
---

Sono un pessimo giocatore di <i>arcade</i> e so fare bene solo una cosa: controllare la racchetta per fare rimbalzare la pallina e mandarla a distruggere il muro di mattoni in cima allo schermo. Mediocre a governare i salti e le finte di un Super Mario, sono però bravo ad anticipare e prevedere i movimenti della pallina e se mi cimento su un gioco del genere riesco quasi invariabilmente a battere chi mi sta accanto.

L'ho scoperto da ragazzino, quando nella sala giochi al mare introdussero un <a href="http://en.wikipedia.org/wiki/Breakout_(video_game)" target="_blank">Breakout</a> e arrivai in fondo. Ci voleva poco, i giochi erano primitivi (i mattoncini erano colorati perché sullo schermo era appiccicata una pellicola multicolore); bastò probabilmente una mezz'oretta. Il problema era solo controllare la pallina con una racchetta che, arrivati all'ultimo muro, si restringeva ai minimi termini.

Fui abbastanza deluso di vedere che, crollato l'ultimo muro, non era prevista una fine, ma neanche una continuazione. Non c'erano più muri da abbattere, ma nemmeno un messaggio, un avviso sonoro, nulla: la pallina rimbalzava nello schermo vuoto, se la tenevo in gioco. Lo feci ancora per qualche minuto, sperando di vedere comparire una novità e poi lasciai perdere.

Però mi sentivo orgogliosissimo. Era la prima volta che battevo un gioco.

Ecco perché non capisco gli sforzi di chi passa una parte consistente di vita a battere un gioco, ma comprendo pienamente che cosa stanno cercando di raggiungere.

Mike Leyde<a href="http://www.bejeweled.com/fan03.php" target="_blank"> ha battuto Bejeweled 2</a> e i giochi non sono più quelli di una volta: ci ha messo tre anni e 2.200 ore ma ce l'ha fatta, totalizzando 2.147.483.647 punti. Il gioco non è capace di computare una cifra superiore e neanche di mostrare un punteggio negativo (cosa che tipicamente succede a un programma in queste condizioni). Quindi smette di mostrare il punteggio. Gioco battuto.

A novembre scorso il taiwanese Little Gray ha <a href="http://videogames.yahoo.com/events/plugged-in/meet-the-world-s-top-world-of-warcraft-player/1377953" target="_blank">&#8220;battuto&#8221; World of Warcraft</a>, totalizzando 986 <i>achievement</i> su 986 (un <i>achievement</i> è una tappa significativa nel gioco, come avere pescato cento pesci oppure avere visitato interamente un continente, o essere sopravvissuto a una caduta di altezza superiore a un certo valore critico). Per farlo ha ucciso 390.895 mostri, assestato 7.255.538.878 punti danno, completato 5.906 <i>quest</i>, saccheggiato 405 <i>dungeon</i> e abbracciato undici personaggi di altri.

Se mai volessi imbarcarmi nell'impresa di battere un gioco stile Breakout su Mac, sceglierei molto probabilmente <a href="http://www.ricochetinfinity.com/" target="_blank">Ricochet Infinity Lost Worlds</a>. A dispetto del nome, ha &#8220;solo&#8221; 216 livelli.

Non lo farei mai, ma li capisco bene. L'importante è che un gioco abbia una fine oppure che non ce l'abbia, e che l'uno o l'altro esito sia stato previsto ampiamente dagli autori. Non datemi però punteggi che svaniscono o palline che rimbalzano a vuoto.

E ora torno su <a href="http://rephial.org" target="_blank">Angband</a>, che finisce. Ma è ancora bello lontano.