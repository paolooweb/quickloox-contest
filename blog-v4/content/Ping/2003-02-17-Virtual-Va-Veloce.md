---
title: "Virtual Va Veloce"
date: 2003-02-17
draft: false
tags: ["ping"]
---

Il passo decisivo per accelerare Virtual PC 6 è tenersi aggiornati

Siamo già in tempi di Mac OS X 10.2.4, ma tanti non hanno ancora aggiornato a 10.2.3, vuoi per diffidenza, vuoi per via del download.

Beh, se hai Virtual Pc 6, l’aggiornamento a Mac OS X 10.2.3 è <emphasis>essenziale</emphasis>. Finalmente, infatti, si risolve un problema fondamentale, connesso alla natura di Windows e a quella di Unix, che limitava pesantemente le prestazioni.

Vuoi che Virtual Pc 6 vada al massimo della velocità? Aggiorna Mac OS X a 10.2.3.

Per spiegare esattamente il perché non basta un Ping, ci vorrebbe un vero articolo. Ma se c’è qualche curioso che vuole proprio saperlo proverò a essere sintetico.

<link>Lucio Bragagnolo</link>lux@mac.com