---
title: "Mozzarella, basilico e pomodorini freschi"
date: 2006-10-18
draft: false
tags: ["ping"]
---

Stasera pizzata-Ping per chi ci sta. Io ci sarò. Entro le 20 davanti a <a href="http://www.macatwork.net" target="_blank">Mac@Work</a>. Se riesco porto qualche vecchio libro tecnico in inglese da regalare, ma non garantisco. Sarà una giornata molto lunga. Non certo per la pizzata.