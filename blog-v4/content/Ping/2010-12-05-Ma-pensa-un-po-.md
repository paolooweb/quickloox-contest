---
title: "Ma pensa un po'"
date: 2010-12-05
draft: false
tags: ["ping"]
---

Htc <a href="http://www.computerweekly.com/Articles/2010/12/02/244306/HTC-defends-HD7-handsets-after-antenna-problems-reported.htm" target="_blank">parla</a> del proprio <i>smartphone</i> Hd7 per bocca di un portavoce:

<cite>&#8230;è inevitabile che la forza del segnale di un telefono si indebolisca un poco quando [l'antenna] viene coperta nella propria interessa dal palmo o dalle dita.</cite>

Su YouTube sono apparsi gli <a href="http://www.youtube.com/watch?v=su6R2ei099A" target="_blank">esempi regolamentari di Hd7 che mostrano il problema</a> (YouTube è la cosa più noiosa del mondo, il nuovo <i>l'ho visto alla televisione quindi è vero</i>)

Ricorda niente? Tipo questa estate, quando i <a href="http://www.youtube.com/watch?v=BGt3N3ZSQCc&amp;feature=related" target="_blank">video di YouTube sull'antenna dell'iPhone</a> che <i>non funzionava</i> dovevano fare cascare il mondo e <i>è chiaro che iPhone ha un problema perché lo mostrano i filmati</i>?