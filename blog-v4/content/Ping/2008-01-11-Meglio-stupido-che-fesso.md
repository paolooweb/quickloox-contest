---
title: "Meglio stupido che fesso"
date: 2008-01-11
draft: false
tags: ["ping"]
---

Bruce Schneier, l'esperto mondiale di sicurezza, ha scritto un <a href="http://www.wired.com/politics/security/commentary/securitymatters/2008/01/securitymatters_0110" target="_blank">pezzo molto lungo e brillantemente provocatorio</a> sulla sicurezza delle reti <em>wireless</em> di casa.

In estrema sintesi, la sua tesi è che corri meno problemi se ti violano la rete quando è sprotetta che non quando è protetta. E avanza considerazioni del tipo <cite>con una rete non sprotetta sarà molto più facile provare che c'è stata un'intrusione, mentre se la rete è protetta sarà molto più difficile da dimostrare e intanto magari ti sequestrano l'equipaggiamento</cite>.

In Italia secondo me ti sequestrano tutto a prescindere, ma fa pensare lo stesso.