---
title: "Stanco di una notte di mezza estate<p>"
date: 2005-07-07
draft: false
tags: ["ping"]
---

E il prossimo che parla di sciocchezze gli telefono due ore prima dell&rsquo;alba<p>

Troppi sono i motivi e le circostanze da spiegare; di fatto ho cominciato a scrivere in serata e ho proseguito per tutta la notte, fino ad arrivare a mezzogiorno prima di prendermi una pausa e schiacciare un pisolino.<p>

Mi rendo conto solo ora che ho dato per scontati due optional del mio PowerBook: la retroilluminazione della tastiera e l&rsquo;autoregolazione della luminosità dello schermo.<p>

Prima di avere questo PowerBook, quando mi capitava di scrivere di notte usavo la <a href="http://www.kensington.com/html/1176.html">Flylight Kensington</a> per illuminare la tastiera. Ottimo, ma non quanto la retroilluminazione, oltretutto regolabile. E lo schermo si adegua automaticamente ai cambiamenti di luminosità dell&rsquo;ambiente, per cui a sole alto non risulta troppo buio, né risalta troppo nell&rsquo;oscurità.<p>

Piccole cose, ma comfort ineguagliabile. Se lavori in condizioni un po&rsquo; estreme anche le piccolezze pesano e aiutano, è proprio il caso di dirlo, a passare la nottata.<p>

C&rsquo;è chi ritiene siano sciocchezze o aggiunte di nessuna utilità messe solo per abbindolare il compratore e gonfiare un po&rsquo; il prezzo. Dipende. Davvero.<p>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>