---
title: "Telecom a Norimberga<p>"
date: 2005-01-10
draft: false
tags: ["ping"]
---

Responsabilità così gravi che ci vorrebbe un processo internazionale<p>

Ogni tanto mi prendo una licenza dal tema Mac e questo è uno di quei giorni.<p>

Flavio mi chiede perché io ce l&rsquo;abbia così con Telecom.<p>

È perché sono vecchio. Ho fatto in tempo a vedere il suo monopolio. La tassa di utenza di concessione telegrafica imposta sui primi modem allo scopo di ritardarne la diffusione. L&rsquo;assurdo costo delle linee di trasmissione dati, che non ha riscontri in altra parte del mondo. Il modo in cui fecero fuori la Video On Line di Nicola Grauso, che voleva portare Internet in tutta Italia; fecero con lui un accordo per il quale la fornitura del servizio Vol sarebbe passata dai nodi Telecom, che però non funzionavano, o venivano installati dopo mesi e mesi di ritardo, in cui Vol non riusciva a erogare il servizio promesso eccetera. Il ritardo cinquantennale con cui sono state introdotte le chiamate a toni e le centraline elettroniche. Il monopolio di fatto odierno, in cui i cosiddetti concorrenti sono tenuti per il collo da Telecom stessa, che possiede le linee su cui transita il traffico di tutti.<p>

Nei decenni l&rsquo;opera di Telecom è stata eccellente nel tenerci in uno stato di arretratezza e inefficienza rispetto al resto del mondo libero, e ha causato danni che non ho modo di stimare ma suppongo astronomici in termini di mancata produttività, burocrazia inutile e inefficienze di sistema.<p>

Per chi l&rsquo;ha guidata ci vorrebbe un nuovo processo di Norimberga, con l&rsquo;imputazione di crimini (telematici) contro l&rsquo;umanità.<p>

Se solo la gente non si facesse abbindolare dall&rsquo;Alice, che non ha nessun merito, se non costare due soldi meno.<p>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>