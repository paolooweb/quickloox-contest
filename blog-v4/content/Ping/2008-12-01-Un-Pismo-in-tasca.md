---
title: "Un Pismo in tasca"
date: 2008-12-01
draft: false
tags: ["ping"]
---

John Gruber paragona con la dovuta elasticità processore, Ram, disco rigido di iPod touch con i Mac del passato e <a href="http://daringfireball.net/2008/05/blackberry_vs_iphone" target="_blank">constata</a> che ci stiamo portando nel taschino un <a href="http://lowendmac.com/pb2/pismo-powerbook-g3-2000.html" target="_blank">Pismo del 1999</a>.

Se tanto dà tanto, nel 2017 potremmo avere nel taschino (o sul bordo di un'unghia, che ne sai?) l'equivalente di un MacBook Pro Core 2 Duo di adesso. Da rabbrividire.

Da rabbrividire anche per Rim, quelli di BlackBerry. O inventano seriamente qualcosa, o sul gioco lungo se la vedranno assai brutta. iPhone c'entra solo fino a un certo punto; contano le tendenze.