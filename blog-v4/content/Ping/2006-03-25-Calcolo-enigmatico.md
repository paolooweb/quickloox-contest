---
title: "Calcolo enigmatico"
date: 2006-03-25
draft: false
tags: ["ping"]
---

Ora la Calcolatrice di Mac stampa su carta il rotolo virtuale. Appena lanciato il programma non lo faceva; il comando di mmenu di impostazione pagina era attivo, ma quello di stampa era grigio.

Ho provato a inserire qualche conto nel rotolo virtuale, selezionare quest&rsquo;ultimo, impostare la pagina e cos&igrave; via, e alla fine la Calcolatrice ha stampato regolarmente. Adesso sto provando a chiuderla e riaprirla per riprodurre il bug, ma non sto compiendo troppi progressi. Apparentemente bisogna che il rotolo sia aperto e in primo piano, ma non &egrave; sempre vero e in una occasione sono riuscito a stampare un rotolo vuoto senza averlo visualizzato.

C&rsquo;&egrave; un bug da segnalare, ma capissi quale.