---
title: "Alla fine rimane solo il cuore"
date: 2009-10-26
draft: false
tags: ["ping"]
---

Dopo quindici anni Yahoo ha staccato la spina a Geocities. In pochissimi possono dire di esserne restati fuori e una delle caratteristiche tipiche delle pagine spesso ruspanti che venivano create erano i mille modi di dire <i>Under Construction</i>, lavori in corso. La comunicazione peggiore per una azienda, ma per una paginetta privata e amatoriale andava più che bene.

Uno dei tributi migliori all'era di Geocities è per l'appunto la pagina che raccoglie un <a href="http://www.textfiles.com/underconstruction/" target="_blank">campionario dei segnali Under Construction</a> più originali.