---
title: "La prima Ping! Wave"
date: 2009-11-29
draft: false
tags: ["ping"]
---

Sedici inviti Google Wave disponibili, oltre quaranta richieste.

Ho proceduto al ballottaggio come da regole e qui sotto i vincenti possono vedersi. Chiedo scusa per non avere risposto alle singole mail e ringrazio ognuno per la gentilezza e la cortesia.

Ricordo che gli inviti si attivano dopo diversi giorni. Chi ha vinto, pazienti; chi non ha vinto&#8230; pure; appena ne avrò altri li metterò in gioco.

Varie persone gentili avevano scritto mettendo a disposizione inviti loro. Se sono ancora dello stesso parere, possono pubblicare il loro indirizzo nei commenti e chi è senza invito può provare a scrivergli. Naturalmente una persona che mette a disposizione i propri inviti può decidere in qualsiasi momento di cambiare idea e nessuno ha diritto di lamentarsi.

Anche non fosse utile, Google Wave serve comunque a fare incontrare persone nuove. :-)