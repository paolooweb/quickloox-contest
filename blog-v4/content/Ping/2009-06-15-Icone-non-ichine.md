---
title: "Icone, non ichine"
date: 2009-06-15
draft: false
tags: ["ping"]
---

Sempre in attesa della traduzione in italiano della <a href="http://www.apple.com/it/macosx/refinements/enhancements-refinements.html" target="_blank">pagina delle novità di Snow Leopard</a>, mi permetto di supplire una novità alla volta.

<b>Icone più grandi</b>

Per vedere chiaramente i documenti nel Finder, si può usare un cursore per regolare la dimensione delle icone fino a 512 x 512 pixel, quattro volte la risoluzione che raggiungevano in Leopard.

Aggiungo che, se l'icona è ingrandita al massimo e si ha buona vista, si riesce a leggere direttamente il testo dei documenti. Pazzesco.