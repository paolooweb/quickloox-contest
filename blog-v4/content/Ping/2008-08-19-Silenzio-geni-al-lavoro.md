---
title: "Silenzio, geni al lavoro"
date: 2008-08-19
draft: false
tags: ["ping"]
---

I periodi come questo mi riempiono di fiducia.

Niente nuovi annunci, niente novità eclatanti, niente rivoluzioni. Invece, beta che si susseguono, di Mac OS X 10.5.5 e iPhone 2.1, con frequenza più che settimanale.

Mac OS X 10.5.5 sistema oltre 125 <em>bug</em>, iPhone 2.1, beh, è una piattaforma appena nata.

È quando Apple non annuncia niente che si stanno preparando le cose migliori.