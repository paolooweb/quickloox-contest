---
title: "Parola d'ordine: virtualizzare"
date: 2006-04-06
draft: false
tags: ["ping"]
---

Visto come tutti i siti specializzati in anticipazioni hanno dato la notizia di <a href="http://www.apple.com/it/pr/comunicati/2006/04/04-bootcamp.html" target="_blank">Boot Camp</a> <em>dopo</em> il comunicato stampa Apple? Questa s&igrave; che &egrave; informazione alternativa.

Ce ne fosse uno che avesse notato una piccola news apparsa su Techworld, che d&agrave; come questione di giorni l&rsquo;arrivo della <a href="http://www.techworld.com/opsys/news/index.cfm?newsID=5712" target="_blank">virtualizzazione su Mac</a>.

(<strong>Aggiornamento:</strong> uno cՏ. <a href="http://www.macitynet.it/macity/aA24291/index.shtml" target="_blank">Macity</a> ci era arrivata. Bravi, quando se lo meritano, e scuse mie per non essermene accorto)

Riassunto della situazione.

Boot Camp. Tra otto mesi (in versione ufficiale) puoi fare il boot del Mac con Mac OS X, oppure con Windows. Finch&eacute; usi uno, non usi l&rsquo;altro. Se becchi un virus robusto su Windows o Windows ti sputtana la partizione, rifai tutto da capo. Formatta, inizializza, reinstalla eccetera.

Virtualizzazione. Praticamente da subito (con qualche distinguo), continui a usare Mac OS X e apri dentro Mac OS X tutte le finestre virtuali che vuoi, Windows e magari anche Linux. Li usi tutti insieme, oppure dai la preminenza a quello che vuoi, praticamente a parit&agrave; di prestazioni. Becchi un virus su Windows, o si sputtana la macchina virtuale? La spegni e ne accendi un&rsquo;altra. O chiudi quella e continui a usare le altre.

Volendo, la virtualizzazione si fa da subito. C&rsquo;&egrave; <a href="http://www.kberg.ch/q/" target="_blank">Q</a>, di cui ho gi&agrave; detto, e una <a href="http://www.thefreecountry.com/emulators/pc.shtml" target="_blank">pagina dedicata al tema</a> con numerosi link interessanti.

Ma tant&rsquo;&egrave;, la gente aspetta fiduciosa il momento in cui potr&agrave; farsi veramente male con gusto.

