---
title: "Il divorzio del secolo"
date: 2008-03-29
draft: false
tags: ["ping"]
---

Ha destato sconcerto nel mondo dell'Information Technology la separazione tra il fondatore di Microsoft, Bill Gates, e la moglie Melinda French. Erano sposati dal 1996.

Melinda Ann French, 44 anni il prossimo 15 agosto, che condivideva col marito anche la Bill &#38; Melinda Foundation, molto attiva nel cosiddetto <em>charity</em>, era da tempo ai ferri corti col famoso consorte.

Si annuncia una dura battaglia legale per l'affidamento dei figli e per il cospicuo assegno di mantenimento chiesto dalla ormai ex signora Gates al facoltoso marito. La situazione è complicata proprio dalla complessa situazione patrimoniale della coppia, dopo che Bill Gates ha recentemente deciso di lasciare alla moglie il timone dell'azienda a partire dal 2008, per dedicarsi alle attività filantropiche.

Sviluppatrice alla Microsoft dal 1987, Melinda si era laureata in informatica ed economia nel 1986 alla Duke University e l'anno successivo aveva conseguito un master in Business Administration presso la Duke's Fuqua School of Business.

I rapporti tra Melinda e Bill erano tesi da diverso tempo, soprattutto dopo lo scorso 29 giugno, giorno della presentazione di iPhone, il telefono di nuova generazione presentato da Apple Inc., notoriamente avversaria storica del colosso di Redmond. Steve Ballmer, amico intimo di Gates, dice che Bill non aveva mai perdonato alla moglie di essere tornata a casa dopo una settimana a New York, con un iPhone nella sua borsetta. Pare che Bill non avesse sopportato che la madre dei suoi figli si fosse disfatta dello Zune Special Edition fatto costruire apposta per lei per San Valentino dagli ingegneri di Redmond, nel personalissimo color vomito di bue muschiato.

Steve Jobs, CEO di Apple e amico di Bill Gates da un trentennio, si è mostrato dispiaciuto per la sorte della coppia. In una sua recente dichiarazione agli inviati di una emittente di Seattle, Jobs ha sottolineato che Bill non aveva mai perdonato alla moglie di essersi rifiutata di cambiare nome. Melinda era infatti un nome scomodo, decisamente una cattiva pubblicità per i prodotti Microsoft e lasciava trasparire una mai del tutto nascosta invidia sociale per l'azienda di Cupertino. Ma la richiesta del magnate di Redmond era caduta nel nulla. Ora si attendono gli sviluppi di un clamoroso divorzio.

Nel frattempo Gates ha messo in vendita la lussuosa e ipertecnologica villa di Medina, nei pressi di Seattle, dove la coppia aveva la propria residenza.

<em>&#8212; Piergiovanni</em>