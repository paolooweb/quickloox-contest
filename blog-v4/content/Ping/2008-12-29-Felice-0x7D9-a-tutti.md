---
title: "Felice 0x7D9 a tutti"
date: 2008-12-29
draft: false
tags: ["ping"]
---

Tra i propositi per il nuovo anno metto simbolicamente questa trascrizione esadecimale del numero 2009. Solo un modo per dire <em>imparare ancora e di più</em>.

Un grande augurio a tutti di mettere a frutto i propri propositi. In subordine, di averne almeno uno.

Per chi volesse distinguersi a tutti i costi al momento del brindisi di Capodanno, c'è un'occasione speciale. Quando tutti urlano e stappano, aspetta un momento prima di seguirli.

Il 2008 infatti <a href="http://hpiers.obspm.fr/iers/bul/bulc/bulletinc.dat" target="_blank">durerà un secondo in più</a> per compensare il rallentamento della rotazione terrestre.

Probabilmente non ti noterà nessuno. Ma vuoi mettere la soddisfazione di fare le cose quando è ora?