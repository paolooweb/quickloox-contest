---
title: "Mettere e togliere / 2"
date: 2008-10-19
draft: false
tags: ["ping"]
---

Si può ironizzare sul fatto che buon <em>design</em> significhi togliere (a proposito: i nuovi portatili hanno tolto anche l'infame gancetto, a favore di una più lineare chiusura magnetica), ma ciò non toglie che sia una verità.

Sillysoft vende <a href="http://sillysoft.net/lux/" target="_blank">Lux Delux</a>, forse il Risiko online più versatile che ci sia (in realtà Risk, perché il Risiko italiano ha cambiato l'idea base, però è un'altra storia). Quattrocento mappe diverse, undici intelligenze artificiali a disposizione, editor di mappe, gioco offline e via Internet e chi più ne ha più ne metta. Formidabile perché puoi personalizzarlo a qualsiasi livello e in qualsiasi modo.

Beh, io sono diventato matto per Lux Touch, versione gratuita creata da Sillysoft per iPod touch. I territori li distribuisce lui, le armate anche, giochi per forza con i blu e per forza offline.

Il gioco dalla personalizzazione infinita su Mac è totalmente obbligato su iPod touch (per ora; uscirà una versione completa a suo tempo, credo). Ma è perfetto per risolvere una partita in pochi minuti e avere tutto il divertimento senza sovraccarico. Su iPod touch sarebbe sciocco un gioco pieno di opzioni al punto che ci vogliono mezze ore per finire e per questo una versione ridotta all'osso, che permette di giocare partite lampo, è perfetta per l'apparecchio. Buon <em>design</em>, raggiunto levando, togliendo, sfrondando. Quando arriverà Lux Delux su iPod touch uno potrà certo scegliere di avere il gioco complicato, oppure quello semplicissimo. Meglio di così è impossibile.

Non vedo l'ora che arrivi anche la versione con il gioco online, perché le intelligenze artificiali di Lux touch non sono alla mia altezza. Modestamente. Però mi va benissimo giocare sempre con i blu.