---
title: "Era nel piano"
date: 2010-10-23
draft: false
tags: ["ping"]
---

L'amico <b>Alessio</b> mi comunica che il <a href="http://www.alessionanni.com/synaesthesia/" target="_blank">video di presentazione del progetto Synaesthesia</a> &#8211; un linguaggio di programmazione che associa luci e forme al suo suonare (mirabilmente) il piano &#8211; ora si vede correttamente e direttamente da iPad, cosa inizialmente impossibile senza accedere al filmato originale su Vimeo.

Dopotutto, per un progetto del genere il tatto è un ennesimo senso da coinvolgere. Ancora complimentissimi.