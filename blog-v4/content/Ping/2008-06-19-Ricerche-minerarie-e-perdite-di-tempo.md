---
title: "Ricerche minerarie e perdite di tempo"
date: 2008-06-19
draft: false
tags: ["ping"]
---

Non ho ancora capito un accidente di quello che sarà del mio iPhone Edge una volta iniziata la fornitura ufficiale da parte di Tim e Vodafone.

Nel frattempo, per curiosità, sto scaricando le perle più curiose presenti nel parco software irregolare nato in questi mesi.

Tralascio per brevità i giochi perché sarebbe imbarazzante, sono troppi. Invece ieri ho tirato a bordo Kaleidoscope, un caleidoscopio (nessuna interfaccia, l'immagine muta con il movimento del computer) e CatchPhrase, un programma fatto per tenere traccia di quanto spesso vengono pronunciate le frasi fatte o gli intercalari e produce a volontà grafici della situazione.

Il mio iPhone sta diventando un aggeggio che non sfigurerebbe in mano a Grunf nel negozio di fiori del Gruppo Tnt (e mi fa guadagnare i favori della nipotina, ma è tutt'altro discorso). A parte le mie disgrazie personali, tuttavia, la riflessione è che tutta questa fioritura di software creato con tutte le difficoltà della mancanza di documentazione dà le vertigini quando si pensa che tra venti giorni si potranno scrivere applicazioni native con la benedizione di Apple e un meccanismo di distribuzione universale a disposizione ben più oliato ed elegante dell'Installer.

Se penso che il meglio della concorrenza, finora, è <em>mettiamo anche noi un accelerometro</em>, fa paura. Ribadisco che i cellulari sono stati la più grande truffa mai architettata sul pianeta. L'alternativa è che a mandare avanti le Nokia e le Samsung siano persone grigie e tristi, oltre che con pessimi ingegneri a libro paga.