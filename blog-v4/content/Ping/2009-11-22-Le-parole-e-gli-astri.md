---
title: "Le parole e gli astri"
date: 2009-11-22
draft: false
tags: ["ping"]
---

I <i>netbook</i> hanno esaurito la loro spinta propulsiva, direbbe qualche politico.

La prossima parola magica è <a href="http://news.cnet.com/8301-13924_3-10400275-64.html?part=rss&amp;subj=news&amp;tag=2547-1_3-0-20" target="_blank">smartbook</a>.

Più o meno la stessa minestra. Ma compariranno decine di articoli a parlare del boom degli <i>smartbook</i>, recensioni degli <i>smartbook</i>, <i>forum</i> dedicati agli <i>smartbook</i> e quant'altro.

Arriverà qualcuno a chiedere angosciosamente <i>perché Apple non fa uno smartbook?</i> e naturalmente i siti acchiappagonzi inizieranno ad annunciare che sta per uscire uno <i>smartbook</i> Apple, forse il prossimo trimestre, no, quello dopo, no, marted&#236; alle diciassette, no, rinviato al 2011.

È tutto talmente evidente e ovvio che stupisce come qualcuno ci possa ancora credere. D'altronde vale lo stesso per l'astrologia.