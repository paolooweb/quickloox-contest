---
title: "Sta per arrivare l'anno nuovo"
date: 2007-12-22
draft: false
tags: ["ping"]
---

Stufo di occuparmi di riavvii indesiderati (uno o forse due in tutto il 2007, abbastanza pochi da risultare irrilevanti e rendere noiosa la questione), sto pensando alla nuova sfida per il 2008.

Ho pensato che da un po' uso iWork e di fatto non apro praticamente più OpenOffice.

E mi sono detto: per quest'anno voglio aprire con iWork tutti i file in arrivo che sono in formato Office. E verificare se e quali problemi si verificano.

Obiettivo della sfida: verificare se iWork è sufficiente. E, se no, dove bisogna migliorare.