---
title: "C'è spazio per tutti"
date: 2007-06-20
draft: false
tags: ["ping"]
---

<a href="http://spaceflight.nasa.gov/gallery/images/station/crew-14/html/iss014e08795.html" target="_blank">Thomas Reiter</a>, astronauta sulla Stazione Spaziale Internazionale. <a href="http://images.spaceref.com/news/2006/IMG_4930.med.jpg" target="_blank">Tizio</a> di cui non ho compreso il nome, a Devon Island, presso la Arthur Clarke Mars Greenhouse. <a href="http://spaceflight.nasa.gov/gallery/images/shuttle/sts-116/html/s116e05405.html" target="_blank">Nicholas J. M. Patrick</a>, navetta spaziale Discovery. <a href="http://spaceflight1.nasa.gov/gallery/images/shuttle/sts-116/html/s116e05401.html" target="_blank">William A. <em>Bill</em> Ofelein</a>, navetta spaziale Discovery.

Tutta gente che, per chi sa cogliere i dettagli, ha preferito portarsi dietro un iPod.