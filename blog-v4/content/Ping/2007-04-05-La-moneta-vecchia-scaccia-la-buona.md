---
title: "La moneta vecchia scaccia la buona"
date: 2007-04-05
draft: false
tags: ["ping"]
---

Dal gorgo delle notizie Mac di ieri:

<cite>Apple Usa aggiorna e ribassa i prezzi dei Cinema Display.</cite>

Vero.

<cite>Tagli al listino anche di 200 euro. Per ora i prezzi italiani sono invariati.</cite>

Qualcuno è rimasto alle lire. Dopotutto c'è ancora chi usa Mac OS 9.