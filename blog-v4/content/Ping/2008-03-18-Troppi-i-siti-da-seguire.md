---
title: "Troppi i siti da seguire"
date: 2008-03-18
draft: false
tags: ["ping"]
---

Eppure le iniziative interessanti vanno almeno segnalate.

Oggi tocca al bravo Nicola D'Agostino, che ha messo in piedi <a href="http://www.machack.it" target="_blank">MacHack.it</a>.

Un tentativo veramente temerario: un sito di vere notizie e di veri fatti sul mondo Mac, in concorrenza con un livello di spazzatura che a confronto la Napoli odierna è un parco. Se ce la fa, è un grande.

Un <em>in bocca al lupo</em> sincero.