---
title: "C&rsquo;è chi gli piace Java<p>"
date: 2005-03-25
draft: false
tags: ["ping"]
---

Il che semplicemente moltiplica le alternative gratuite al software dal prezzo iniquo<p>

Dopo avere letto della notizia di Open Office 2.0 beta per Mac OS X, Mario mi ha obiettato che si dovrebbe anche parlare di NeoOffice/J.<p>

Non ha torto. NeoOffice/J nasce per fornire una versione di OpenOffice che sia veramente Mac e non residente in un ambiente grafico diverso, come avviene per OpenOffice che funziona via X11, con la sua grafica e i suoi font.<p>

<a href="http://www.neooffice.org/">NeoOffice/J</a> è scritto utilizzando Carbon e Java. Questo porta qualche vantaggio (per esempio i font e i menu sono quelli di Aqua) e qualche svantaggio (per esempio lo sviluppo è leggermente in ritardo). Ma è indubbio che sia una ennesima alternativa open source e gratuita, nonché in italiano, a certo software che costa un perù, è pieno di funzioni inutili e viene venduto da mani aziendali non pulitissime.<p>

Non resta che ringraziare Mario per la segnalazione.<p>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>