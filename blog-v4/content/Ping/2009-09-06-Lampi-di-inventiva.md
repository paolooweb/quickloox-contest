---
title: "Lampi di inventiva"
date: 2009-09-06
draft: false
tags: ["ping"]
---

Mi scuso per chi si accorgerà di avere già letto altrove questo intervento, ma la marea sta montando ed è opportuno chiarire.

Snow Leopard installa una versione di Flash (10.23.1) che è più vecchia di quella disponibile all'oggi (10.0.32.18). Il che è comprensibile, perché ovviamente il sistema contiene la versione di Flash che era collaudabile al momento di bloccare le specifiche.

Ora però tutti hanno sulla bocca la notizia <i>Snow Leopard installa un Flash più vecchio</i>, che è verissima. Solo che aggiungono <i>e riapre vecchie vulnerabilità</i>.

Gli ultimi <a href="http://www.adobe.com/support/security/" target="_blank">avvisi di sicurezza di Adobe</a> riguardanti Flash arrivano fino alla versione 10.0.22.87, più vecchia di quella di Snow Leopard.

Una delle due: o Adobe sa qualcosa che noi non sappiamo oppure la versione di Flash di Snow Leopard è sicura. Che poi sia meglio aggiornare è ovvvio: è sempre meglio aggiornare. La paranoia della sicurezza è tuttavia ingiustificata.

Quelli che scrivono delle vulnerabilità, o sanno anche loro quello che sa Adobe e che Adobe non dice, o inventano di sana pianta. Veri e propri <i>flash</i> di creatività.