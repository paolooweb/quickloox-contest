---
title: "Altri cinque anni"
date: 2006-01-10
draft: false
tags: ["ping"]
---

Microsoft ha dichiarato di essere al lavoro sulla versione Universal Binary di Office e che sono impegnati a fornire Office per Mac per almeno altri cinque anni.

Non importa quanto uno possa lavorare duramente. Ci sono problemi che nessun antivirus riesce a estirpare.