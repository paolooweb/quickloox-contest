---
title: "Regali a caduta"
date: 2005-02-06
draft: false
tags: ["ping"]
---

Un altro motivo per dire .mac

Gli abbonati a .mac ricevono in regalo un salvaschermo proprio bello a tema spaziale basato sul programma Freefall e hanno lo sconto del 30 percento sulla versione integrale del programma stesso. Ciò in aggiunta a tutti gli altri vantaggi, tra cui la recente estensione dello spazio su disco a 250 megabyte pro capite.

Per non parlare dei due capitoli gratuiti di un bel libro sui migliori programmi Mac, i 300 loop gratis per GarageBand e tutte le altre offerte. All’inizio mi dicevano <em>ma costa 99 euro l’anno</em> e io rispondevo <em>sì, beh, però, comunque</em>. Adesso rispondo <em>per quello conviene</em>.

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>