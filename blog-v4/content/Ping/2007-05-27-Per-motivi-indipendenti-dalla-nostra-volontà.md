---
title: "Per motivi indipendenti dalla nostra volontà"
date: 2007-05-27
draft: false
tags: ["ping"]
---

Chiedo scusa a tutti per il pacco di venerd&#236;. Purtroppo l'incontro che doveva finire alle 11 è terminato alle 12:30 e il venerd&#236; del gioco ne ha risentito. Sono arrivato in rete tardissimo e non ho trovato nessuno.

Scusarsi serve, però, se si ripara al danno; in via eccezionale, lo scorso venerd&#236; del gioco si tiene in seconda convocazione dopodomani, mercoled&#236; 30 maggio, sempre dalle 13 alle 14, sempre con la stanza iChat <code>gamefriday</code> a coordinare. Il gioco è sempre <a href="http://jrisk.sf.net/" target="_blank">Jrisk</a>.