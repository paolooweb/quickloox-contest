---
title: "C’era una volta"
date: 2008-10-07
draft: false
tags: ["ping"]
---

Il <em>designer</em> grafico Bob Staake <a href="http://www.bobstaake.com/staakenyer.html" target="_blank">realizza copertine</a> per il prestigioso periodico <em>New Yorker</em>. Con Photoshop 3.0 e un Mac con System 7.

L'aggiornamento all'ultimissima versione è veramente indispensabile quando non si è in grado di farne a meno. Nel bene e nel male.