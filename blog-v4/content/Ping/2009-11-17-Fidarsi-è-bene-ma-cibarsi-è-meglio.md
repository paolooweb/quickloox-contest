---
title: "Fidarsi è bene ma cibarsi è meglio"
date: 2009-11-17
draft: false
tags: ["ping"]
---

L'amico <a href="http://web.me.com/radioqueequeg/Il_Peluod/Benvenuto.html" target="_blank">Pelo</a> mi ha fatto gentilmente inviare il suo libro <i>Aiuto! Ho un cyberfiglio!</i>, sottotitolato <i>Manuale per genitori persi nella Rete</i>.

E di questo si tratta, manuale snello che si legge in due o tre serate di buona attenzione, utile per chi si ritrova in casa pargoli oramai adolescenti e che ne sanno, spesso rischiosamente, più di mamma e papà.

L'intento è più pedagogico che tecnico, diretto più a spiegare come affrontare il problema che a dare le solite dritte sui siti (che peraltro, una manciata, ci sono e buone) o a configurare l'ennesimo e inutile controllo genitoriale.

L'autore insegna e si occupa di teatro, e si vede; conosce di prima mano tutte le mattine le nuove generazioni e inoltre ha disseminato nel testo numerose citazioni cinematografiche che diventa gusto aggiuntivo cogliere, nei dialoghi immaginati-ma-non-troppo di una famiglia e anche nella trattazione che li inframmezza (a contare le pagine sembrerebbe il contrario, invece sono i dialoghi il cuore del libro).

Al termine del centinaio di pagine si trova, fatto abbastanza inedito e gradito, una piccola rassegna di <i>podcast</i> italiani di interesse generale e specifico, che non sono il solito Beppe Grillo né la solita banda di Radio 105, ma roba di più spessore e per questo meno pubblicizzata (c'è anche <a href="http://web.me.com/radioqueequeg/Il_Peluod/Radio_Queequeg!/Radio_Queequeg!.html" target="_blank">Radio Queequeg</a> di Pelo, citato anche due volte, ma lo si perdona, visto che si ascolta volentieri).

<a href="http://www.ancoralibri.it/Catalogo/tabid/55/ProductID/6496/Default.aspx" target="_blank"><i>Aiuto! Ho un cyberfiglio</i></a> è lettura molto di base, assai indicata per genitori inquieti al pensiero di che cosa sta combinando la prole chiusa in cameretta da ore dinanzi al Mac, gustosa e leggera per chi ha già metabolizzato il problema e riesce a cibereducare i propri fanciulli. Per qualcuno sarà anche un possibile regalo di Natale, a soli dieci euro, per i tipi di &#192;ncora.