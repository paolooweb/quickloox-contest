---
title: "Tre livelli di lettura"
date: 2010-11-13
draft: false
tags: ["ping"]
---

Eccellente <a href="http://www.appleinsider.com/articles/10/11/09/why_apple_axed_xserve_and_how_it_can_reenter_the_sever_market.html&amp;page=1" target="_blank">articolo di Daniel Eran Dilger su AppleInsider</a>, dedicato alla dismissione dei server Xserve di Apple.

Certo, è un inglese, ma ci sono due grafici pantagruelici da divorare per ricchezza e stile di informazione. Il link vale la pena anche solo per guardare le figure.

I più pazienti leggeranno una analisi molto ragionata e condivisibile sui perché della fine di Xserve (facile: vendeva poco o niente) ma soprattutto sul perché della nascita. Per Apple all'inizio degli anni Duemila era essenziale recuperare credibilità sulla linea Mac e questo passava anche da una buona offerta di server a supporto dello sviluppo di Mac OS X. Con il tempo queste ragioni sono venute a mancare, visto lo straordinario sviluppo di Mac (che ogni mese vende più di quattro volte oggi quello che vendeva nel 2002), l'esplosione di iOS e naturalmente il primo avvento dei servizi <i>cloud</i> come MobileMe, che tra l'altro non è pilotato da software Apple bens&#236; Solaris.

Il terzo livello di lettura è più che altro quello del ragionamento in risposta a chi vede ogni notizia riguardante l'ecosistema Mac come un segno della perdita di interesse di Apple verso i computer, visto che i telefoni vendono a palate (certo: se il primo è molto più saporito del secondo, perché non mangiare solo quello invece di perseguire una alimentazione completa?).

Per i profeti di sventura, Xserve dismesso è la prova che Apple non ha più interesse nel Mac. Sbagliato: è già successo nel 1998, quando Apple ha dismesso i suoi WorkGroup Server. Da allora la vendita di Mac non ha fatto che crescere a livelli superiori rispetto alla media del mercato. Se questo è perdere interesse, speriamo che ne perdano ancora.