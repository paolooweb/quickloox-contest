---
title: "Toshiba getta la maschera"
date: 2003-07-15
draft: false
tags: ["ping"]
---

Loro, semplicemente, non ci arrivano

Ho visto di sfuggita - di più non meritava proprio - il nuovo incredibile portatile Satellite P25 di Toshiba. Ohibò! Uno schermo da 17 pollici! Apple non è più all’avanguardia! E costa meno! Ecco, la solita politica dei prezzi a danno dell’utente!

Poi vai a guardare le specifiche e capisci perché è incredibile. L’autonomia ufficiale - immagina quella effettiva - è di due ore e il peso è superiore ai quattro chili.

Non è un portatile. È un attrezzo per culturisti che lavorano poco e di fretta oppure viaggiano con trenta metri di prolunga a tracolla.

Meglio, è un preciso statement di Toshiba: tecnologicamente parlando, non è all’altezza di Apple.

<link>Lucio Bragagnolo</link>lux@mac.com