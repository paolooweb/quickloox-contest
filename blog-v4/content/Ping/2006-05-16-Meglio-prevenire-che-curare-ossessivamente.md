---
title: "Meglio prevenire che curare ossessivamente"
date: 2006-05-16
draft: false
tags: ["ping"]
---

A <strong>Odino</strong> dico solo che, invece di perdere tempo a fare verifiche con Utility Disco per cercare con insistenza una qualunque malattia fino a che non riesce a trovarla, gli conviene investire quello stesso tempo in un backup e, dovesse malauguratamente andarsene il disco, i dati restano. :-)