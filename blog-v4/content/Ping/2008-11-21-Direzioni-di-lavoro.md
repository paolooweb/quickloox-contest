---
title: "Direzioni di lavoro"
date: 2008-11-21
draft: false
tags: ["ping"]
---

Mac OS X dovrebbe evolversi verso un enorme sistema che ingloba quanto più può in termini di funzioni, per accontentare il maggior numero di utenti da subito dopo l'installazione, oppure un software snello ed essenziale che ognuno è libero di arricchire e personalizzare come può utilizzando ciò che offre la piazza software?