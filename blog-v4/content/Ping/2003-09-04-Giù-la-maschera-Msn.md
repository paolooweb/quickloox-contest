---
title: "Giù la maschera, Msn"
date: 2003-09-04
draft: false
tags: ["ping"]
---

La balla spaziale per cui Microsoft ha smesso di produrre browser per Mac

Il solito informatissimo mi ha raccontato che Microsoft “ha interrotto lo sviluppo di Internet Explorer per Mac”.

Bene, gli ho risposto; allora come fa Msn per Mac OS X (che contiene un browser, caro) a identificarsi su Internet come “Internet Explorer 6.0”?

Lui ha borbottato qualcosa sulla guerra dei browser, come se parlare del matrimonio di Fiorello avesse a che fare con la guerra degli ascolti.

La guerra dei browser non è finita; semplicemente ora si combatte su altri campi e il Web è assai più complesso di dieci anni fa.

<link>Lucio Bragagnolo</link>lux@mac.com
