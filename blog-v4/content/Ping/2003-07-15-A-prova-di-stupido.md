---
title: "Avviso agli inesperti"
date: 2003-07-15
draft: false
tags: ["ping"]
---

Il computer, spesso, spiega che cosa fare. Ascoltalo

Bisognerebbe varare una legge che introduce il reato di distrazione dall’interfaccia utente, destinato ai neofiti che hanno tanta buona volontà ma ancora più pigrizia mentale.

Il dialogo che segue è realmente accaduto.

“Puoi venire a dare un’occhiata? Ho un problema con Aggiornamento Software”.

“Uhm. Prova a usare il menu Aggiorna e a scaricare una copia dell’aggiornamento sulla Scrivania”.

“Ah, si può fare anche così? Ma come facevi a saperlo?”

“È scritto nella finestra di dialogo che appare a video in questo momento”.

“Allora che cosa devo fare?”

“Usare il menu Aggiorna e scaricare una copia dell’aggiornamento sulla Scrivania”.

“Dove sta il menu?”

“Nella barra dei menu, dove stanno tutti gli altri menu”.

“Certo che a volte anche il Mac è difficile”.

<link>Lucio Bragagnolo</link>lux@mac.com
