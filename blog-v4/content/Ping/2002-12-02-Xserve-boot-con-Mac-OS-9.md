---
title: "Il computer che non doveva partire"
date: 2002-12-02
draft: false
tags: ["ping"]
---

Apple produce un eccellente server che può avviarsi solo da Mac OS X. A meno che...

Xserve è uno dei prodotti migliori realizzati da Apple negli ultimi due anni e, come server basato su Unix facilissimo da amministrare ancorché stabile e potente, si è conquistato da subito molte simpatie. Non sempre disinteressate.

Qualcuno infatti si è chiesto se non fosse possibile fare partire Xserve da Mac OS 9. E ha scoperto di sì: bisogna cancellare il file /System/Library/PreferencePanes/StartupDisk.prefPane/Contents/Resources/
OS9Exclusion.nsarray di Mac OS X Server.

Va da sé che Apple non raccomanda né autorizza questa modifica. Chi ci prova lo fa a suo rischio e pericolo e nella perfetta consapevolezza del fatto che sta mettendo le mani dove non dovrebbe. In poche parole, provaci se vuoi, ma sono tutti server tuoi.
(redazione, è sufficiente come disclaimer?)

<link>Lucio Bragagnolo</link>lux@mac.com