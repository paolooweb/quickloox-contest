---
title: "Se i prezzi Apple li fa il Wall Street Journal<p>"
date: 2004-09-23
draft: false
tags: ["ping"]
---

È ora di spazzare via il mito dei prezzi Apple più alti della media<p>

Walt Mossberg è un autorevole columnist del Wall Street Journal e spesso con Apple ci va giù duro. Guarda che cosa ha scritto a proposito dell&rsquo;iMac G5:<p>

<cite>I consumatori percepiscono i desktop Mac come costosi [&hellip;] ma in realtà il nuovo iMac costa meno di una macchina Windows paragonabile. Per fare un esempio, il Profile 5 di Gateway, all-in-one, con schermo piatto da 17” incorporato, costa 1.499 dollari, contro 1.299 per l&rsquo;iMac da 17”. Il Gateway è molto più spesso e non possiede una scheda grafica dedicata come quella di iMac. Anche ad aggiungere memoria sul Mac per pareggiare i 512 mega del Gateway, l&rsquo;iMac costa sempre 125 dollari in meno.</cite><p>

<em>Ipse dixit</em>.<p>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>