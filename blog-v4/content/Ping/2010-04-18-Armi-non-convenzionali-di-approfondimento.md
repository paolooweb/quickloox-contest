---
title: "Armi non convenzionali di approfondimento"
date: 2010-04-18
draft: false
tags: ["ping"]
---

Qualcuno potrebbe usarlo per progettare la compravendita di azioni.

Qualcun altro per capire se la sua percezione del funzionamento di questa o quella azienda è realistica.

Qualcun altro ancora per divertirsi a giocare a come sarebbe se&#8230;

Qualcun altro, infine, per divertirsi e basta, magari in modo un po' istruttivo.

Il modello interattivo di <a href="http://www.trefis.com" target="_blank">Trefis</a> è straordinario per immediatezza.

Diventerà sicuramente uno dei miei strumenti non convenzionali di approfondimento oltre a <a href="http://www.wolframalpha.com/" target="_blank">Wolfram Alpha</a>. S&#236;, si può aggiungere qualcosa a Google.