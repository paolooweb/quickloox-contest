---
title: "A memoria della posterità"
date: 2010-09-01
draft: false
tags: ["ping"]
---

Prima della diretta video della <a href="http://events.apple.com.edgesuite.net/1009qpeijrfn/event" target="_blank">presentazione dei nuovi iPod e di iOS 4.1 e 4.2</a>, l'ultima trasmissione da parte di Apple datava al 17 luglio 2002.

L'avevano guardata <a href="http://www.apple.com/pr/library/2002/jul/22quicktime.html" target="_blank">in cinquantamila</a>.