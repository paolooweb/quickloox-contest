---
title: "Censura, censura"
date: 2007-07-05
draft: false
tags: ["ping"]
---

Avvisi pubblici per lòlòlàl e per l'ufficio stampa di Loredana Bontempi: se un post non dice niente a nessuno ma è solo stupido, viene cancellato (per il primo). I comunicati stampa di roba che non c'entra niente con l'argomento non passano (per il secondo).

La cosa bella è che gli imbecilli sono talmente pochi che, quando si fanno notare, fanno notizia. Meglio cos&#236;.