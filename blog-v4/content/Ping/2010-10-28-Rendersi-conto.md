---
title: "Rendersi conto"
date: 2010-10-28
draft: false
tags: ["ping"]
---

Nessuno legge i moduli 10-K pubblicati da Apple per compiacere la Security Exchange Commission, la Consob americana.

Peccato, perché a volte si trovano numeri che passano poco nel flusso dell'informazione.

Per esempio, ecco una rarissima <a href="http://www.macdailynews.com/index.php/weblog/comments/apple_inc._files_sec_form_10-k_annual_report_with_net_sales_tables/" target="_blank">tabella delle vendite</a> di Apple anno fiscale per anno fiscale, negli ultimi tre anni.

Le vendite europee di Mac nel 2008, per esempio, ammontarono a 2,5 milioni di pezzi. Nel 2010, 3,9 milioni. iPod ha vendite stabili o leggermente declinanti, ma si parla di cinquanta milioni di pezzi l'anno. iPad è passato da zero a 7,5 milioni di unità e cos&#236; via.

L'<a href="http://phx.corporate-ir.net/External.File?item=UGFyZW50SUQ9Njc1MzN8Q2hpbGRJRD0tMXxUeXBlPTM=&t=1" target="_blank">intero documento</a> è liberamente reperibile su Internet e leggerselo tutto è una punizione. Contemporaneamente se ne ricavano spesso cose molto interessanti.