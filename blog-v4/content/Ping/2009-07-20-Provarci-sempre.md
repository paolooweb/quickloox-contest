---
title: "Provarci, sempre"
date: 2009-07-20
draft: false
tags: ["ping"]
---

Niente più dell'atteggiamento verso il mondo dei <i>browser</i> mostra come funziona Microsoft.

Guarda la <a href="http://blogs.msdn.com/ie/archive/2009/05/09/ie-setup-experience-just-the-facts-and-the-screenshots.aspx" target="_blank">meccanica attuale di installazione di Internet Explorer 8</a>. Nove schermate. Confrontalo con l'installazione di Safari, <i>en passant</i>. Ma il punto non è questo.

Alla terza schermata viene chiesto se usare gli <i>Express Settings</i> oppure i <i>Custom Settings</i>. Se usi Express, hai già finito. Sotto, abbastanza in piccolo, abbastanza in fondo, stile clausola dell'assicurazione, c'è scritto che Explorer diventa il <i>browser</i> predefinito.

E cos&#236; un sacco di gente che andava di fretta ma usava Firefox ha cambiato <i>browser</i> senza volerlo veramente. Qualche pigro non vorrà neanche tornare indietro e oplà!, ecco riconquistato alla causa di Explorer qualche milione di clic, per lo più involontari.

Le cose stanno per cambiare. L'installazione di Explorer <a href="http://blogs.msdn.com/ie/archive/2009/07/16/changes-to-ie8-s-first-run.aspx" target="_blank">presenterà come prima schermata</a> la scelta esplicita, a grossi caratteri, di renderlo <i>browser</i> predefinito o meno.

Ci hanno provato. Si è arrabbiata Mozilla, si è arrabbiata Opera, sono arrivati <i>feedback</i> negativi e in Microsoft fanno con calma e con comodo (da metà agosto) quello che avrebbero dovuto fare da subito se avessero un minimo di onestà e trasparenza.

Nel frattempo chi ha avuto ha avuto, chi ha dato ha dato. Chi non capisce bene perché Microsoft viene multata per abuso di posizione dominante ora dovrebbe capirlo meglio.

<cite>Questo cambiamento fa parte del nostro impegno costante a favore della scelta e del controllo da parte dell'utente</cite>, scrivono.

Più che costante è sempre un po' tardivo, sempre dopo averci provato. A parte il fatto che cambiare le cose a favore dell'utente è ammettere che prima di cambiare lo erano meno.