---
title: "Il gusto perverso dell'arbitro"
date: 2007-12-27
draft: false
tags: ["ping"]
---

Abituato a stare attento ad applicazioni in funzione e copie <em>in progress</em> prima di espellere un disco esterno, ho scoperto il piacere sottile di espellere l'hard disk di backup come e quando mi pare. Tanto Time Machine è abbastanza intelligente da fermarsi e riprendere quando lo riattacco.

S&#236;, il Natale ha cambiato la mia strategia di backup. :-)