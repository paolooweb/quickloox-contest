---
title: "Mac OS X si aggiorna e non lo sa"
date: 2008-07-13
draft: false
tags: ["ping"]
---

Il mio Aggiornamento Software è impostato sul controllo settimanale. Più frequente non serve e, se capitasse quell'aggiornamento su mille davvero problematico, farei in tempo a dribblarlo.

Però ci sono eccezioni e, quando ho letto di Mac OS X MobileMe Update 1.1, mi sono detto che forse era il caso di farlo subito, dal momento che il dialogo con iDisk e con le varie sincronizzazioni è continuo e alla luce della migrazione non proprio indolore di questi giorni da .Mac a MobileMe.

Ma ho avviato Aggiornamento Software due volte, nel giro di due giorni, senza che emergesse alcunché. Il terzo giorno, letta l'ennesima notizia, ho caricato non Aggiornamento Software ma la sezione .Mac delle Preferenze di Sistema. È apparso un messaggio a spiegarmi che .Mac è diventato MobileMe e a quel punto ho potuto cliccare un pulsante che ha lanciato Aggiornamento Software. Stavolta l'aggiornamento è arrivato.

Quelle situazioni in cui tutto alla fine funziona, ma non hai capito bene perché e hai un vago giramento di testa. Questa era una.