---
title: "Da Classic a Redux"
date: 2009-04-12
draft: false
tags: ["ping"]
---

Come già detto, la vera ricchezza di iPhone e App Store è che i programmatori raccontano come hanno progettato o riprogettato le loro applicazioni.

John Carmack ha raccontato di come il leggendario Wolfenstein 3D <a href="http://www.idsoftware.com/wolfenstein3dclassic/wolfdevelopment.htm" target="_blank">sia stato portato su iPhone e iPod touch</a>.

Lettura senza prezzo. Intanto mostra molti dei compromessi e delle decisioni che si devono adottare programmando per una piattaforma come iPhone (per esempio si può recuperare un oggetto passandoci meno vicino che nel gioco originale, visto che il direzionamento del personaggio non può essere altrettanto preciso). Secondo, illustra alcune caratteristiche del funzionamento interno di iPhone.

Terzo, offre uno spaccato di assoluto candore su come funziona in certi casi la programmazione. Scrive una mail al compositore originale della musica per sapere se ha ancora in giro i file, ma questo non gli risponde. E altre cos&#236;.

Il migliore di tutti però resta questo passo:

<cite>Wolfenstein fu scritto originalmente in Borland C e Tasm per Dos, ma molto tempo fa avevo reso </cite>open source<cite> il codice e c'erano numerosi progetti che avevano aggiornato il codice originale per farlo funzionare sui sistemi operativi moderni e su OpenGl. Dopo qualche ricerca ho trovato <a href="http://wolf3dredux.sourceforge.net/" target="_blank">Wolf3D Redux</a>.</cite>

Voglio fare una edizione nuova di un mio vecchio gioco e vado su Internet a vedere che ne hanno fatto gli altri programmatori, cos&#236; me lo ritrovo aggiornato e riparto da l&#236;. Come dimostrazione del valore dell'<em>open source</em>, la trovo fantastica.