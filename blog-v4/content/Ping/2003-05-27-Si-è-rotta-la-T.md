---
title: "Si è ro**o il *as*o *"
date: 2003-05-27
draft: false
tags: ["ping"]
---

Analisi di un piccolo inconveniente che può costare caro

Mi si era rotto il tasto T della tastiera del portatile. Sui Titanium i tasti sono agganciati a un micropantografo per mezzo di due gancetti di plastica. Per caso o per eccesso di forza sulla tastiera si è rotto un gancetto e il tasto andava fuori sede nel giro di tre o quattro pressioni.

È incredibile come sia difficile sostituire una T, o un tasto in generale. Di mandarlo in riparazione da Apple non è nemmeno da pensare; rispetto al problema, i costi sono astronomici.

La T è rimasta malferma al suo posto per qualche giorno grazie a un quadratino di biadesivo, mentre riuscivo a trovare una T di riserva da Claudio Zamagni da Torino, mitico riparatore della serie “se non può farlo lui, non c’è speranza”. Poi me la sono cavata ancora prima, riuscendo a cannibalizzare una tastiera dai portatili usati che girano in Mac@Work (sì, sono socio di Mac@Work e questa parentesi è sospettabile di pubblicità occulta). Ma trovare una T si è rivelata una vera impresa. E dire che è una delle lettere più usate in italiano.

Domanda ad Apple: quanto costerebbe fornire nella confezione di ogni portatile un tastino sfuso, non serigrafato, che possa servire da ricambio universale volante nel momento in cui viene a mancare un tasto?

<link>Lucio Bragagnolo</link>lux@mac.com