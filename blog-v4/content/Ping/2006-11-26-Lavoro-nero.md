---
title: "Lavoro nero"
date: 2006-11-26
draft: false
tags: ["ping"]
---

Sto articolando la recensione del <a href="http://tven.terratec.net/modules.php?op=modload&amp;name=News&amp;file=article&amp;sid=236" target="_blank">ricevitore Tv Terratec</a> e vorrei dare voce ai commenti più interessanti che sono stati inviati.

C'è qualcuno che obietta e non desidera essere pubblicato? O qualcuno che non ha problemi, a patto che non si faccia il suo nickname? Oppure qualcuno che ha ulteriori commenti da fare?

Questo è il momento. :)