---
title: "Si fa ma non si dice"
date: 2009-07-10
draft: false
tags: ["ping"]
---

S&#236;, d'accordo, iPhone 3GS è bello perché fa il copia e incolla e manda gli Mms.

Ci sono tuttavia aspetti che neanche Apple pubblicizza più di tanto e significano più di quello che si legge. Per esempio la quantità di Ram di bordo, 256 megabyte in luogo dei precedenti 128 megabyte.

Come si sottolinea in <a href="http://www.taptaptap.com/blog/the-impact-of-the-iphone-3gs-ram-increase/" target="_blank">questo blog</a>, non è solo un raddoppio della Ram: per il sistema e per i programmatori equivale a disporre di cinque-dieci volte la memoria libera disponibile in precedenza, con notevole impatto su stabilità e prestazioni.

Il blog è in inglese, però con grafici a torta immediati e universalmente comprensibili.