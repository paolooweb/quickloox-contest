---
title: "Mille chiacchiere, una tabella"
date: 2009-05-04
draft: false
tags: ["ping"]
---

Non ero stato cos&#236; preciso nel confidare i miei dubbi a <a href="http://www.italovignoli.com" target="_blank">Italo</a>, membro autorevole del <a href="http://www.plio.it/" target="_blank">Progetto linguistico Italiano OpenOffice</a>. Ora ci hanno messo anche la precisione: Office ha <a href="http://www.robweir.com/blog/2009/05/update-on-odf-spreadsheet.html" target="_blank">compatibilità Odf nominale e interoperabilità effettiva tendente a zero</a>.

Chi va molto di fretta legga solo la seconda tabella; dice tutto con un'occhiata.

In altre parole, Microsoft è riuscita a fare finta di aprire il proprio formato Ooxml, con documentazione da gettare come fumo negli occhi degli sprovveduti (o leccapiedi) di Iso, e ora che è riuscita a farselo riconoscere come standard va avanti come prima.

Certo, il <em>plugin</em>, le promesse, gli accordi, le dichiarazioni. Chiacchiere. Scommetto con chiunque che neanche tra cinque anni ci sarà interoperabilità totale tra Odf (lo standard aperto patrocinato da OpenOffice.org) e OoXml.