---
title: "Codici e sorgenti"
date: 2010-07-20
draft: false
tags: ["ping"]
---

Un giornalista chiese a Steve Jobs quanti anni ci fossero voluti per scrivere QuickDraw, l'insieme di istruzioni per fare grafica sullo schermo che facevano del primo Macintosh un progresso tecnologico assoluto.

Jobs lo chiese a Bill Atkinson, principale autore di QuickDraw, autore più tardi di HyperCard e programmatore di straordinario talento.

Atkinson rispose <cite>beh, ci ho lavorato a intermittenza quattro anni</cite>.

Jobs si rivolse al giornalista e tradusse <cite>ventiquattro anni uomo</cite>. Atkinson era un genio tale al punto che il suo lavoro poteva essere conteggiato come quello di sei programmatori comuni.

Il 31 gennaio 1984 cos&#236; Erik Sandberg-Diment scriveva sul New York Times di MacPaint, il primo programma di disegno bitmap, antesignano dei moderni Photoshop e quasi impossibile da credere vero per quanti erano abituati al computer con lo schermo solo testuale e i comandi solo da tastiera:

<cite>MacPaint disvela numerose possibilità grafiche a disposizione del computer, compreso il disegno a mano libera. Nessuno supererà un Brueghel grazie a questo software, che tuttavia migliora di dieci volte qualsiasi altra offerta su un personal computer.</cite>

QuickDraw e MacPaint hanno fatto <a href="http://www.computerhistory.org/highlights/macpaint/" target="_blank">la storia dell'informatica</a>.

Per la prima volta si può liberamente scaricare il codice sorgente dell'<a href="http://s3data.computerhistory.org/102658076_quickdraw_acc.ziphttp://s3data.computerhistory.org/102658076_quickdraw_acc.zip" target="_blank">uno</a> e dell'<a href="http://s3data.computerhistory.org/102658076_macpaint_acc.zip" target="_blank">altro</a>, per usi non commerciali.