---
title: "Undici anni e mezzo"
date: 2004-04-27
draft: false
tags: ["ping"]
---

Aggiornamento per chi ha ancora timore che Apple possa fallire domani

Si sa che la stampa informatica è notoriamente ignorante nei confronti di Apple e che basta una perdita di un dollaro ad annunciare l’imminente morte dell’azienda. Questo ha frenato più di un nuovo acquirente, tentato dai Mac ma riluttante a comprare il prodotto di un’azienda “in crisi”.

Apple ha annunciato per l’ultimo trimestre fiscale un attivo di 46 milioni di dollari, ma non è questo il punto. Piuttosto ha annunciato di disporre di 4,6 miliardi di dollari in cassa e di essere libera da debiti.

Il che vuole dire che Apple potrebbe permettersi di perdere, da domani, 100 milioni di dollari a trimestre e sopravvivere allegramente per undici anni e mezzo.

Il tuo computer durerà meno.

<link>Lucio Bragagnolo</link>lux@mac.com