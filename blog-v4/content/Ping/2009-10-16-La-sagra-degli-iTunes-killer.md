---
title: "La sagra degli iTunes killer"
date: 2009-10-16
draft: false
tags: ["ping"]
---

Nokia un anno fa ha iniziato il lancio del servizio <i>Comes With Music</i>: ti abboni per una cifra annuale e scarichi tutta la musica che vuoi, che puoi eventualmente tenere a fine abbonamento. Il vero <i>iTunes killer</i>, supportato da tutte le grandi case discografiche e promosso con una certa fanfara da Nokia.

Dopo il lancio nel Regno Unito, lo scorso ottobre, il servizio è stato gradualmente esteso a Singapore, Australia, Brasile, Svezia, Italia, Messico, Germania e Svizzera.

Si avvicina l'anniversario dell'inaugurazione nel Regno Unito. <a href="http://musically.com/blog/2009/10/15/comes-with-music-107k-users-worldwide/" target="_blank">Gli abbonati globali sono 107 mila</a>.

iTunes Store ha cento milioni di <i>account</i> attivi, mille per ciascun abbonato Comes With Music.

In Italia, dove il servizio è attivo da aprile, gli abbonati sono <i>seicentonovantuno</i>. (Nel frattempo Tre fa sapere che vende in Italia ventimila iPhone al mese e, se ci fosse disponibilità, sarebbero il doppio).

Si possono raccogliere 691 abbonamenti a un servizio in un fine settimana, con un banchetto presso la Sagra del Fagiolo Borlotto a Castel Manzanillo Inferiore.

Se questo è un <i>iTunes killer</i>, dobbiamo aspettarci tempi lunghi. E molte sagre.