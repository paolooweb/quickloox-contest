---
title: "Automedicazione"
date: 2006-05-17
draft: false
tags: ["ping"]
---

Console e Monitoraggio Attivit&agrave; sono utility pienamente Mac, con icona, interfaccia utente, doppio clic, menu e quant&rsquo;altro. Sono impagabili per capire le ragioni di un programma che si chiude senza motivo oppure per verificare se c&rsquo;&egrave; un programma impazzito che sta predando memoria agli altri. Permettono di risolvere situazioni difficili da soli, ed &egrave; anche una soddisfazione.

Trovassi qualcuno che li usa.

Si capisce che non dobbiamo essere tecnici per forza, eppure - quando compriamo un Mac - passare un&rsquo;ora a vedere che cosa c&rsquo;&egrave; dentro, cartella Utility compresa, non mi sembra un&rsquo;enormit&agrave;.