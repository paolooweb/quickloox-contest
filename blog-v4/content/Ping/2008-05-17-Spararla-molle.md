---
title: "Spararla molle"
date: 2008-05-17
draft: false
tags: ["ping"]
---

Molle nel senso di floppy. Finalmente hanno messo a punto un <a href="http://revver.com/video/874984/datastorm-v10/" target="_blank">sistema per utilizzarli</a>. E pensare che nel 1998, uscito l'iMac che ne faceva a meno, sembrò uno scandalo.

(un'altra videoeccezione, ma poi basta)