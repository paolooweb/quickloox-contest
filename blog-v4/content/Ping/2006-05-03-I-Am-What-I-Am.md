---
title: "I Am What I Am"
date: 2006-05-03
draft: false
tags: ["ping"]
---

Sembra incredibile, ma qualcuno ha iniziato un dibattito sul riconoscersi (o meno) nei personaggi dei nuovi spot Apple.

Ne parla gente che evidentemente non ha guardato gli spot, ma neanche ha aperto la <a href="http://www.apple.com/getamac/" target="_blank">pagina</a> che li contiene, dove sotto i piedi dei due protagonisti c&rsquo;&egrave; scritto <cite>I&rsquo;m a PC</cite> e <cite>I&rsquo;m a Mac</cite>. Non sono utenti, ma sono i due (tipi di) computer.

E s&igrave;, si pu&ograve; fare anche rete con gente che non sia necessariamente giapponese.