---
title: "Sulle ali del drag and drop"
date: 2006-10-23
draft: false
tags: ["ping"]
---

Grande <strong>Mario</strong> per avermi segnalato <a href="http://www.hawkwings.net/" target="_blank">Hawkwings.net</a>, blog clamorosamente di valore per quanti usano Mail.

Sono la prova vivente del fatto che il valore è alto anche per chi Mail non lo usa, perché mano l'ho mai aperto eppure sono rimasto conquistato da <a href="http://www.hawkwings.net/2006/10/22/faster-text-dragging-in-cocoa-apps-like-mail/" target="_blank">questo post</a>.

Finalmente mi hanno spiegato in termini scientifici perché ci ho messo una vita a imparare a trascinare testo selezionato in Safari. Mentre nei programmi Carbon il drag and drop del testo selezionato è istantaneo, nei programmi Cocoa (come Safari) il drag and drop si attiva dopo una frazione di secondo passata ad attendere con il mouse cliccato sopra il testo selezionato in questione.

Volendo, esiste persino un sistema per ridurre il ritardo del drag and drop in Cocoa: basta dare da Terminale il comando

<code>defaults write -g NSDragAndDropTextDelay -int 100</code>

Il perché dettagliato lo spiegano sul blog ed è giusto cliccare per rendere loro il giusto merito. E anche imparare un sacco di roba su Mail.