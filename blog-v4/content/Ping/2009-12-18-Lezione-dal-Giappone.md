---
title: "Lezione dal Giappone"
date: 2009-12-18
draft: false
tags: ["ping"]
---

Giornata di iPhone, oggi. Le ultime suggeriscono che iPhone <a href="http://macnn.com/rd/148472==http://translate.google.com/translate?js=y&amp;prev=_t&amp;hl=en&amp;ie=UTF-8&amp;layout=1&amp;eotf=1&amp;u=http%3A%2F%2Fwww.impressrd.jp%2Fnews%2F091210%2Fsmartphone2010&amp;sl=ja&amp;tl=en" target="_blank">ha fatto il botto</a> sul mercato giapponese in poco più di un anno, accaparrandosi nel 2009 il 46 percento della fascia smartphone. Il terzo in classifica fa il 14,6 percento e, un anno fa, aveva il doppio.

Uno dei fenomeni che si accompagnano i dati è la sconfitta dei cellulari fatti su misura per i giapponesi. <a href="http://www.electronista.com/articles/09/12/17/apple.crushing.sharp.willcom.in.japan/" target="_blank">Leggo</a>:

<cite>I cellulari giapponesi montano interfacce complesse e pesantemente basate su menu, e si focalizzano su funzioni molto specifiche per il Giappone come la mobile Tv di 1Seg, i pagamenti</cite> contactless <cite>di FeliCa e i servizi i-Mode. Apple si è rifiutata di implementare queste [funzioni], che avrebbero costretto a diversificare il processo produttivo, ma la rosa di funzioni più ristretta e la sua interfaccia hanno dato luogo a una piattaforma più accessibile.</cite>

C'è ancora qualcuno che pensa di giudicare la bontà di un prodotto dal numero delle funzioni. Almeno in Giappone hanno capito che non è cosa.