---
title: "Come si sta in iChat<p>"
date: 2005-06-14
draft: false
tags: ["ping"]
---

Non è difficile farlo sapere, con un po&rsquo; di buona volontà<p>

Dario ha creato una suite di script che ripristinano varie delle funzioni disponibili via iChatStatus e che apparentemente la versione di iChat in Tiger non permette più attraverso il programmino.<p>

Non mi interessa tanto che esista una versione aggiornata di iChatStatus oppure che il nuovo iChat, per esempio, possa mostrare già da solo il brano musicale riprodotto in quell&rsquo;istante da iTunes.<p>

Il punto è che una persona disposta ad approfondire un pochino può arrivare in fretta a fare cose buone con AppleScript. Rendersi la vita al computer un pizzico più piacevole e prendersi una bella soddisfazione personale.<p>

Dario lo trovi in iChat, a dddario@mac.com, o per posta, dddario@tin.it.<p>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>