---
title: "Chissà quando arriverò in port"
date: 2008-09-07
draft: false
tags: ["ping"]
---

Sempre sulla falsariga delle grandi pulizie estive, ho spazzato via la vecchia installazione di <a href="http://www.finkproject.org" target="_blank">Fink</a> con l'intenzione di rifarla da zero, senza incrostazioni e roba inutile caricata per motivi che non sono più.

Nel contempo ho deciso di provare anche <a href="http://www.macports.org" target="_blank">MacPorts</a>, che da un po' di tempo mi sembra più vivace come iniziativa per portare facilmente programmi Unix su Mac OS X.

Dopo il primo giorno di esperimento, è in vantaggio Fink. Cancellare la vecchia installazione è stato un attimo (c'è una cartella <code>sw</code> al primo livello del disco e basta eliminare quella). Nessun problema con l'installazione da immagine disco e l'autoaggiornamento ha trovato e applicato un upgrade minimo. Un bel segnale che funziona tutto.

Ho poi installato <a href="http://lynx.isc.org/" target="_blank">lynx</a>, il browser solo testo, con massima comodità. Installare Common Lisp (clisp) è stato un filo più lungo; nell'elenco dei pacchetti visibili all'installazione c'è solo una vecchia versione. Quella vera sta nell'albero rsync della distribuzione di Fink e cos&#236; ho dovuto dare un comando in più. Dopo di che, però, tutto a posto.

MacPorts non lo avevo mai installato. Da immagine disco, tutto regolare. Ho però provato a installare <a href="http://www.fishshell.org/" target="_blank">fish</a>, una <em>shell</em> Unix di nuova concezione che dovrebbe aiutare i neofiti a misurarsi meglio con il Terminale.

Il comando di installazione canonico non ha funzionato per carenza di permessi. L'ho rieseguito con <code>sudo</code> ed è partito.

Le dipendenze di <code>fish</code> sono numerose e mi aspettavo di vedere anche una mezz'ora di <em>download</em> dei più vari. Non di aspettare ore. Si sta scaricando letteralmente il mondo, compresa roba che secondo me non c'entra veramente niente, e non vedo l'ora di sapere come va a finire.