---
title: "Lo scopiazzo non dà la felicità"
date: 2007-09-29
draft: false
tags: ["ping"]
---

Visto i nuovi iMac e la nuova tastiera, e vista la <a href="http://www.macnn.com/print/47658" target="_blank">mediocre imitazione</a> che ora fa uscire Gateway, e viste la dotazione e il prezzo dei Gateway, e visto che il Gateway non può fare funzionare Mac OS X mentre iMac può fare funzionare Windows, e visti i due modelli <a href="http://www.macdailynews.com/index.php/weblog/comments/beleaguered_gateway_to_release_jay_leno_edition_apple_imac_knock_off/" target="_blank">confrontati sulla stessa pagina</a>, l'unica conclusione possibile è che il Gateway è è da tristi.

E non c'è neanche la scusa del risparmio.

Aggiornamento: non è l'unica conclusione. Fantastici quelli di The Apple Unofficial Weblog. Gateway è finalmente <a href="http://www.tuaw.com/2007/09/29/gateway-finally-catches-mac-20th-anniversary-edition/" target="_blank">arrivata al Mac del Ventesimo Anniversario</a>. Apple è al trentesimo&#8230;