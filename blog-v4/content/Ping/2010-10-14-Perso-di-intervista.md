---
title: "Perso di intervista"
date: 2010-10-14
draft: false
tags: ["ping"]
---

Mi ha fatto impressione leggere due sunti di una lunga <a href="http://www.cultofmac.com/john-sculley-the-secrets-of-steve-jobs-success-exclusive-interview/21572" target="_blank">intervista</a> concessa a Leander Kahney da John Sculley, l'uomo chiamato da Steve Jobs a guidare Apple che fin&#236; per cacciare Steve Jobs per poi essere a sua volta defenestrato dopo otto anni di amministrazione in calando.

Sembra quasi che Sculley abbia ancora un senso di colpa per quanto successo, assolutamente non comune per uomini di affari americani separati da venticinque anni di vite parallele. Consiglio la lettura: addirittura <a href="http://www.cultofmac.com/apple%E2%80%99s-big-mistake-was-hiring-me-as-ceo-sculley-interview/63323" target="_blank">un pezzo</a> comincia con in retrospettiva è stato un grande errore assumermi come amministratore delegato [di Apple]. Credo di non avere mai visto una affermazione pubblica di questo tenore da parte di un amministratore delegato.

Ho scaricato la presentazione che Sculley tiene adesso in qualità di conferenziere e che è disponibile sulla sua <a href="http://www.johnsculley.com/index.html" target="_blank">pagina personale</a>. Magari è interessante. L'uomo, senza dubbio, è molto cambiato.