---
title: "La casa di Steve (e la foto di Jim)"
date: 2006-02-06
draft: false
tags: ["ping"]
---

Prosegue la pubblicazione dei <a href="http://www.allaboutapple.com/speciali/s_francisco_2006_b.htm" target="_blank">ricordi di viaggio</a> in California della delegazione dell&rsquo;All About Computer Club. C&rsquo;&egrave; davvero un sacco da leggere e, per chi ha perso l&rsquo;alfabetizzazione elementare, un sacco di figure da guardare!