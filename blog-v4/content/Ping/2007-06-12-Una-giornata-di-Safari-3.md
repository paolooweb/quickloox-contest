---
title: "Una giornata di Safari 3"
date: 2007-06-12
draft: false
tags: ["ping"]
---

Finora, niente da dichiarare. Per ora ho visto solo vantaggi, tipo i form ridimensionabili che ho già citato.

Per la limitata esperienza di oggi, Wordpress funziona, Blogger funziona, Google Groups funziona, Sar-El vecchia versione funziona.

Gmail funziona leggermente meglio (adesso riconosce da subito le scorciatoie di tastiera, prima voleva un intervento manuale nella pagina).

Google Docs ha un comportamento interessante. Mi pare che prima arrivasse un avviso di non supporto. Adesso, il word processor parte senza problemi e sembra funzionare. Lo spreadsheet pubblica un avviso di mancanza di supporto di Safari, ma lascia proseguire e l'ambiente sembra a posto. Ho solo eseguito editing di minima, ma la gestione dei file funziona.

L'impressione (impressione) dopo una giornata è che si sia guadagnato qualcosina in velocità, appena appena sul mio G4. Ho aperto un numero rispettabile di pagine e la risposta con molte pagine aperte appare nettamente migliore. Nessun crash, per ora, come dato di nessuna seria indicatività e mera testimonianza oculare. Ho provato a spostare dei tab come se avessi spostato rocce lunari in laboratorio. Ha funzionato.

Bookmark, cronologia, pulsanti sono stati ereditati da Safari 2 senza problemi.

Sono soddisfatto. Ero timorosissimo (con il browser ci stralavoro). Se va sempre come oggi, viva Safari 3.