---
title: "La meditazione di Ferragosto"
date: 2010-08-07
draft: false
tags: ["ping"]
---

Gli estratti provengono da <a href="http://www.macobserver.com/tmo/article/the_game_of_writing_about_apple_connections/" target="_blank">questo articolo</a> di John Martellaro su the Mac Observer. Buon Ferragosto a tutti!

<cite>Scrivere di una grande azienda è come guardare una partita di scacchi su un computer. Vediamo le tattiche e le strategie. Se siamo buoni giocatori, pensiamo di capire gli schemi e le pianificazioni dietro ogni mossa. Ma quando scriviamo di quello che vediamo, non pensiamo a persone vere. Reagiamo a immagini vaghe comparse sul campo di battaglia simulato di Internet.</cite>

<cite>Talvolta ho l’impressione che alcuni articolisti, nel coprire Apple, stiano intrattenendo le proprie illusioni. L’azienda è un colosso sventrato che proietta la propria ombra sul campo di gioco e non esiste alcuna connessione umana ad alcuna entità vivente.</cite>

<cite>In altri tempi, certi giornalisti sedevano al bar assieme alle loro fonti. Soppesavano i vantaggi tangibili di un segreto divulgato assime al futuro della fonte e della sua famiglia. La vita è equilibrio.</cite>

<cite>Oggi viviamo in una rete interconnessa e siamo più separati che mai gli uni dagli altri. Si scrivono cose sui dipendenti dell’azienda che, dette di persona, frutterebbero all’autore un pugno in faccia. Come in un videogioco, gli epiteti volano come palle di fuoco, per esplodere sugli schermi dei destinatari.</cite>

<cite>Quando scrivo di Apple, è nel contesto dei dipendenti Apple con cui ho lavorato e che ho conosciuto. […] Altri trattano Apple come se fosse un grande macchinario, che pompa prodotti da fabbriche lontane, progettati nelle viscere di <a href="http://maps.google.com/maps?f=q&amp;source=s_q&amp;hl=en&amp;geocode=&amp;q=Infinite+Loop,+Cupertino,+CA,+United+States&amp;sll=37.0625,-95.677068&amp;sspn=67.295907,60.820313&amp;ie=UTF8&amp;hq=&amp;hnear=Infinite+Loop,+Cupertino,+Santa+Clara,+California+95014&amp;ll=37.331787,-122.030764&amp;spn=0.008446,0.007424&amp;t=f&amp;z=17&amp;ecpose=37.32807436,-122.030764,554.16,0,44.996,0" target="_blank">Infinite Loop</a>. L’umanità, le speranze, sangue sudore e lacrime di 20 mila dipendenti possono essere convenientemente ignorate. […]</cite>

<cite>Non è per dire che Apple non commetta errori. Continueranno a commetterne e più l’azienda cresce, più saranno grossi. Quando Apple non ha successo, clienti e osservatori devono avere un equilibrato e maturo senso della prospettiva. Si possono criticare decisioni specifiche di Apple senza perdere di vista ciò che rende grande l’azienda. Come in ogni impresa, equilibrio e giudizio battono sarcasmo e cattiveria in malafede.</cite>

<cite>Il lavoro dei giornalisti è mostrare l’errore quando si vede ma, come negli scacchi, vedere una immagine che brilla sullo schermo è diverso da una scacchiera vera. […]</cite>

<cite>Quando tocco un pezzo e la sua superficie liscia e cesellata, ne sento la trama. Il suo destino è letteralmente nelle mie mani. Sono responsabile del suo futuro. […]</cite>

<cite>Mi auguro che possiamo sempre sentirci in questo modo, molti ordini di grandezza più avanti, rispetto alle persone che lavorano nelle aziende di cui scriviamo. Possano sempre sentire che, perfino quando meritiamo un ceffone, lo abbiamo fatto con rispetto, lealtà e attenzione alla loro esistenza come esseri umani.</cite>