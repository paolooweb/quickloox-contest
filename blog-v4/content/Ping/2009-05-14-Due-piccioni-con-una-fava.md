---
title: "Due piccioni con una fava"
date: 2009-05-14
draft: false
tags: ["ping"]
---

Marted&#236; scorso Microsoft ha aggiornato Office, chiudendo <a href="http://www.microsoft.com/technet/security/Bulletin/MS09-017.mspx" target="_blank">quattordici vulnerabilità</a>, tredici importanti e una critica.

Le vulnerabilità erano note <a href="http://www.microsoft.com/technet/security/advisory/969136.mspx" target="_blank">da un mese e mezzo</a> e già circolava in Rete codice buono per attaccare.

Ma sono state chiuse <a href="http://blogs.technet.com/srd/archive/2009/05/12/ms09-017-an-out-of-the-ordinary-powerpoint-security-update.aspx" target="_blank">solo su Windows</a>. Per Office Mac non c'è stato il tempo e l'aggiornamento arriverà alla prossima data nel calendario Microsoft, il 9 giugno.

Microsoft ha dichiarato che su Mac non consentono di prendere il controllo del computer, come invece accade su Windows, e per questo hanno pensato prima a Windows.

Cos&#236; un eventuale criminale informatico ha un mese in più di tempo per tentare un attacco su Office Mac. Se per caso ci riuscisse si griderebbe alla sicurezza di Mac che non è più quella di un tempo e per Microsoft sarebbero due piccioni con una fava.

I prodotti di un'azienda che lavora cos&#236; io li butterei nella spazzatura senza passare dal via. Se non altro per evitare di cadere vittima del tiro al piccione.