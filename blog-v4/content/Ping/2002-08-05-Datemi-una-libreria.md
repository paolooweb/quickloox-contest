---
title: "Datemi una libreria"
date: 2002-08-05
draft: false
tags: ["ping"]
---

La differenza tra una piattaforma libera e una monopolista è che si può chiedere

Provato Mozilla (.org)? Provalo. È un&#146;ottima alternativa ai browser tradizionali ed è basato sull&#146;ottimo motore di rendering Gecko.

La comunità Linux ha preso Gecko – che è open source – e lo sta usando per sviluppare Galeon, un browser per Linux.

La comunità Windows magari vorrebbe farlo, ma purtroppo per loro l&#146;unico motore di rendering ammesso su Windows è quello di Internet Explorer. In tribunale Microsoft è stata riconosciuta colpevole di abuso di posizione dominante anche a causa della mancanza di libertà a questo riguardo.

Su Mac esiste una libreria di rendering di sistema, usata per esempio dalla guida Apple. Si potrebbe fare di più e Mac non è una piattaforma criminalmente proprietaria come Windows: forse domani un gruppo di programmatori capaci prenderà Gecko e ne farà una libreria Html universale per Macintosh, usabile da tutti i programmi che lo vogliono.

Incrocio le dita. :-)

<link>Lucio Bragagnolo</link>lux@mac.com