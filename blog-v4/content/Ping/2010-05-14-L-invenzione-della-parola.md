---
title: "L'invenzione della parola"
date: 2010-05-14
draft: false
tags: ["ping"]
---

Ecco un esempio concreto del perché Wikipedia dovrebbe l'inizio di una ricerca e non la fine.

La striscia satirica Xkcd ha rappresentato <a href="http://xkcd.com/739/" target="_blank">l'ipotetica pagina Wikipedia di una parola inesistente</a>.

Siccome Wikipedia è tanto bella e utile ma anche acefala, priva di giudizio qualificato e soggetta ai capricci di chi ha più tempo da passarci sopra, in pochi minuti è nata <a href="http://en.wikipedia.org/wiki/Malamanteau" target="_blank">la pagina Wikipedia dedicata alla parola</a>, che prima non esisteva e che l'autore ha coniato in modo abbastanza assurdo da rendere difficile attribuirgli un significato preciso.

Pagina assai scarna, a confronto della animatissima e chilometrica <a href="http://en.wikipedia.org/wiki/Talk:Malamanteau#Oh_my_God_.28_ZOMG.29_everyone_what_are_you_doing." target="_blank">discussione nata attorno all'opportunità di creare la pagina stessa</a>. Qualcuno ha colto l'ironia, qualcun altro no e alla fine a decidere sarà una maggioranza che potrebbe essere di persone intelligenti, oppure di imbecilli, e sarebbe comunque una decisione presa.

Intanto l'autore, che se fossi io avrebbe i crampi allo stomaco dal ridere, ha pubblicato sulla pagina wiki del suo sito una <a href="http://wiki.xkcd.com/irc/Malamanteau" target="_blank">definizione in pieno stile Wikipedia della parola</a>, satirica quanto la striscia.

Wikipedia è tanto bella e utile ma bisogna tenere presente che a decidere non è gente titolata, bens&#236; una maggioranza. E la maggioranza non ha necessariamente ragione, anche se ha più voti.