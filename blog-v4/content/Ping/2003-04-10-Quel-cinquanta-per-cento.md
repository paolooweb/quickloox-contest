---
title: "Quel cinquanta per cento"
date: 2003-04-10
draft: false
tags: ["ping"]
---

Perché il rapporto segnale/rumore delle mailing list è così basso

Nelle mailing list italiane – meno delle altre, ma anche quelle Mac – il 50% di chi scrive fa domande intelligenti, riponde a domande intelligenti in modo intelligente e preparato, propone temi interessanti e li discute; il 50% di chi scrive si comporta da free rider cercando di arraffare quello che può e usando la fatica di chi risponde per risparmiarne di propria; il restante 50%, infine, scrive per realizzarsi e non importa neanche se qualcuno poi i messaggi li legga veramente.

Da qualsiasi parte la si guardi, c’è sempre un cinquanta per cento di troppo.

<link>Lucio Bragagnolo</link>lux@mac.com