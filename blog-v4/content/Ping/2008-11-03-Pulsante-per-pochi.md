---
title: "Pulsante per pochi"
date: 2008-11-03
draft: false
tags: ["ping"]
---

Anche tu stai acquistando una <a href="https://connect.apple.com" target="_blank">Adc Select Membership Apple</a> per mettere le mani in anteprima sulle versioni preliminari di Snow Leopard?

Nella schermata di accettazione di termini e condizioni, manca il pulsante per confermare.

In realtà è solo mal disegnata la pagina: se usi Firefox anziché Safari, riesci a vedere quei due pixel sufficienti per fare clic e procedere con l'acquisto.

Che si dovesse pagare per il privilegio era noto e giusto. Il test da settimana enigmistica (dove sarà il pulsante?) non me lo aspettavo.