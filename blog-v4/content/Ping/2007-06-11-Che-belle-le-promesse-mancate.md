---
title: "Che belle le promesse mancate"
date: 2007-06-11
draft: false
tags: ["ping"]
---

Tempo fa qualcuno fece trapelare che Steve Jobs non avrebbe tenuto il <em>keynote</em> alla Worldwide Developer Conference Apple. Mi sembrava una grossa stupidaggine e me la sono tenuta per me. Era una stupidaggine.

Per giorni qualcuno ha ipotizzato che Leopard avrebbe adottato lo <a href="http://www.opensolaris.org/os/community/zfs/" target="_blank">Zettabyte File System</a>. Evidentemente ignaro di qualsiasi questione tecnica a riguardo. Era una grossa stupidaggine. Ci hanno fatto titoli su titoli.

Zfs arriverà, ma ovviamente non adesso. Non bastasse il senso comune, c'è <a href="http://macjournals.com/" target="_blank">Mdj</a>, che ha pubblicato una spiegazione completa e dettagliata del perché diverse ore prima del keynote. Abbastanza perché i siti di spazzatura lo abbiano schifato.