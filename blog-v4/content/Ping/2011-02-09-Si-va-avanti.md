---
title: "L'inarrestabile marcia del progresso"
date: 2011-02-09
draft: false
tags: ["ping"]
---

Microsoft ha aggiornato le vecchie versioni di Windows <a href="http://www.infoworld.com/t/malware/microsoft-shuts-down-malware-friendly-autorun-898" target="_blank">in modo da disabilitare l'<i>autorun</i></a>, funzione che esegue automaticamente il codice presente su aggeggi Usb ed è responsabile di un numero imbarazzante di infezioni da virus e malware assortito.

Si noti che invece l'<i>autoplay</i>, funzione che fa partire automaticamente il contenuto di un Cd o Dvd, è ancora attivo.

Su Mac questi argomenti appartengono a discussioni tenute nel XX secolo.

Si va avanti.