---
title: "Alla ricerca di un client Twitter"
date: 2009-04-20
draft: false
tags: ["ping"]
---

Per il momento ho adottato <a href="http://pocket.drikin.com/" target="_blank">pocket*</a>, quello che occupa meno spazio di tutti sullo schermo.

Devo provarne ancora, però. Per quanto piccolo, rimane sempre in vista.