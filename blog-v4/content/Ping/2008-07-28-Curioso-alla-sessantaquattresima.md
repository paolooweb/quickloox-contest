---
title: "Curioso alla sessantaquattresima"
date: 2008-07-28
draft: false
tags: ["ping"]
---

Curioso di sapere quali programmi sul tuo Mac possono funzionare a 64 bit?

Da Terminale,

<code>locate -0 app/Contents/MacOS/ | xargs -0 file | grep x86_64</code>

per i Mac Intel e invece

<code>locate -0 app/Contents/MacOS/ | xargs -0 file | grep ppc64</code>

per i Mac PowerPc.

Se qualcosa non funziona,

<code>sudo /usr/libexec/locate.updatedb</code>

ricrea il <em>database</em> del comando <code>locate</code>.

Per chi ama le coincidenze numerologiche, c'è un gioco di serie in Mac OS X che è scritto a 64 bit e si gioca su 64 caselle.