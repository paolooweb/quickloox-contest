---
title: "Du' fa più impression che uan"
date: 2009-10-22
draft: false
tags: ["ping"]
---

L'aspetto più impressionante della continua crescita di App Store è che sono nati già due siti dedicati specificamente a fare incontrare la domanda e offerta di programmazione iPhone e iPod touch: <a href="http://www.getappsdone.com/" target="_blank">Get Apps Done</a> e <a href="http://www.iphoneappfreelancer.com/" target="_blank">iPhone App Freelancer</a>.

È nata una nuova professione, o quasi.