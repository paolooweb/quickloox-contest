---
title: "Serie implicazioni"
date: 2009-10-28
draft: false
tags: ["ping"]
---

Dalla comunicazione inviata da Apple a tutti i rivenditori:

<cite>A partire dal primo gennaio 2010, il numero di serie dei prodotti hardware Apple passerà da undici a dodici caratteri alfanumerici.</cite>

<cite>Domanda: perché i numeri di serie Apple cambiano?</cite>
<cite>Risposta: l'attuale formato a undici cifre è in uso dagli anni ottanta e l'attività di Apple è cresciuta oltre le sue capacità.</cite>

Ci sono tanti modi di dire <i>crescita</i>.