---
title: "HighSight"
date: 2004-03-12
draft: false
tags: ["ping"]
---

Un piccolo accessorio che fa sentire grandi

Avendo messo in casa una Adsl e usando il Mac per lavoro, ho provveduto a dotarmi di uno strumento per fare videoconferenza e ho comprato una webcam iSight di Apple.

Costa al pubblico 169 euro. La differenza fondamentale nel prezzo la fa FireWire rispetto alla Usb delle concorrenti più economiche. Rispetto alla iSweet, un’altra webcam FireWire proveniente dall’Estremo Oriente, ho trovato un prezzo più alto di una trentina di euro.

Non ho fatto prove tecniche prima dell’acquisto; ho aperto la scatola di <link>iSweet</link>http://www.macitynet.it/macprof/archivio/test/isweet.shtml e ho aperto la scatola di iSight.

Il piacere di aprire la scatola di <link>iSight</link>http://www.apple.com/it/isight/, da solo, vale tutti i trenta euro supplementari.

Una di quelle cose piccole ma che ti fanno sentire scelleratamente ricco. Anzi, anche un po’ sopra i tuoi mezzi.

<link>Lucio Bragagnolo</link>lux@mac.com