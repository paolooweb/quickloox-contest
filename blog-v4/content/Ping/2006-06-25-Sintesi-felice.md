---
title: "Sintesi felice"
date: 2006-06-25
draft: false
tags: ["ping"]
---

Sentita a una delle presentazioni del <a href="http://www.macbardolino.com/" target="_blank">MacBar 2006</a>:

<cite>iPod ha cambiato il mondo di sentire la musica</cite>.

Meglio di cos&igrave; era impossibile.