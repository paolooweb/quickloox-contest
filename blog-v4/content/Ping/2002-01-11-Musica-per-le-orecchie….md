---
title: "Musica per le orecchie di chi ha fiuto"
date: 2002-01-11
draft: false
tags: ["ping"]
---

Il nuovo, bellissimo iMac ha fatto un po’ ombra a un dato che, pure, Steve Jobs ha sottolineato nel corso dell’intervento di apertura del Macworld di San Francisco.
iPod, il lettore Mp3 con connessione ultrarapida FireWire che però è anche un hard disk superportatile da cinque gigabyte, è stato venduto in oltre 125 mila esemplari in metà trimestre, dal 10 novembre al 31 dicembre.
Per Apple è una bella notizia, perché in termini di fatturato - questo equivale ad avere venduto quarantamila e passa Mac in più, il tutto in una stagione natalizia e in una congiuntura economica non certe favorevole.
Eppure tanti sedicenti esperti lo avevano snobbato: costa troppo, non serve, è solo un altro lettore Mp3
Non è gente con le orecchie foderate di prosciutto; è gente che non ha abbastanza fiuto per toccare con mano che iPod è tutta un’altra musica.

lux@mac.com