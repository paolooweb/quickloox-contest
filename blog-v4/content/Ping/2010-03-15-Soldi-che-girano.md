---
title: "Soldi che girano"
date: 2010-03-15
draft: false
tags: ["ping"]
---

Html5 inizia a diventare possibile alternativa a Flash persino nei servizi finanziari. Guardare, per credere, una <a href="http://www.humblesoftware.com/finance/index" target="_blank">imitazione Html5</a> di quello che è possibile fare su Yahoo Finance grazie a Flash.

La cosa che mi sorprende sempre quando vedo queste soluzioni è l'estrema compattezza del codice, che rende quasi difficile crederci. Eppure è vero e appesantisce molto meno il lavoro del browser, per non parlare della stabilità.

Una tecnologia vincente è definitivamente tale quando si afferma nei videogiochi e nella pornografia. Però, quando si affaccia sulla finanza, vuol già dire che ha buone possibilità.