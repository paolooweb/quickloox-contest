---
title: "Nel regno del fattibile / 5"
date: 2010-05-20
draft: false
tags: ["ping"]
---

<cite>pregasi elencare azioni fattibili su un iPad e non fattibili su un Sony Vaio W da 10&#8221;</cite>.

<a href="http://www.instructables.com/id/iStick-How-to-make-a-drumstick-for-an-iPad/" target="_blank">Fare pratica di percussioni</a> (con i martelletti).