---
title: "C'era una volta il netbook"
date: 2010-05-07
draft: false
tags: ["ping"]
---

La curva di crescita dei <i>netbook</i> negli Stati Uniti punta decisamente verso sudest, che non è la Florida ma l'angolo a destra in basso: era del 641 percento a gennaio 2009, sarebbe del 5 percento in aprile 2010, <a href="http://fortunebrainstormtech.files.wordpress.com/2010/05/screen-shot-2010-05-06-at-5-50-35-am.png" target="_blank">dicono Npd e Morgan Stanley</a>.

Gli analisti sono cialtroni riguardo al futuro, ma nel predire il passato sono imbattibili e quindi sono numeri di cui ci si può fidare.

Intanto leggo il blog personale di Chuck Hollis, Global Marketing Chief Technical Officer di <a href="http://italy.emc.com/?fromGlobalSiteSelect" target="_blank">Emc Corporation</a>, mica paglia. Un suo post si intitola <a href="http://chucksblog.emc.com/chucks_blog/2010/05/what-ipads-did-to-my-family.html" target="_blank">Che cosa ha fatto iPad alla mia famiglia</a>.

Come al solito riassumo. Ma riferisco abbastanza perché sia chiaro che non si parla di un ragazzino, né di un anziano analfabeta informatico, né di un fanatico Apple, né di un artista di strada, piuttosto di un dirigente affermato in una multinazionale di prestigio.

<cite>Sono sposato da 25 anni e ho tre figli, il più giovane dei quali ha 14 anni. In casa c'è sempre stata un sacco di tecnologia.</cite>

<cite>Abbiamo sei Pc desktop di varie età e tre portatili Windows, di cui due funzionano davvero bene. Lato Mac, abbiamo un iMac e due MacBook Air. Abbiamo da poco aggiornato il server Nas di famiglia con la più recente offerta Omega.</cite>

<cite>Aggiungiamo tre stampanti, tre domini</cite> wireless <cite>separati a coprire la casa, accessori vari per collegare televisori, stereo e tecnologia varia, infiniti lettori Mp3 e chiavette Usb, gomitoli di cavi, telecomandi.</cite>

<cite>E poi ho portato a casa iPad.</cite>

<cite>[Dopo una settimana]</cite>

<cite>I Pc e i portatili di casa, sostanzialmente, sono inutilizzati. I Mac sono inutilizzati. Tutto spento.</cite>

<cite>In famiglia ognuno attende il proprio turno su iPad.</cite>

Ho risparmiato la descrizione delle prime reazioni di noia e disapprovazione, che di giorno in giorno diventavano curiosità e poi interesse e infine passione, con tono perfetto per una <i>situation comedy</i>.