---
title: "Sviluppo e Macintosh"
date: 2004-07-01
draft: false
tags: ["ping"]
---

Riporto uno scambio di battute da una delle mailing list che frequento:

-------
>If it's true what O'Reilly says -- that most of the Perl
>language developers are on Mac OS X now

Most of them are, yes.  Most of Slashdot's staff uses Mac OS X, too ...
it's a brave new world.  And perl 5.8 was primarily developed on an iBook.
-------

È abbastanza chiaro? La maggior parte del lavoro si svilppo su Perl è stata svolta con un iBook. Slashdot è slashdot.org, il punto di riferimento dei programmatori di tutto il mondo. O’Reilly è uno dei più famosi editori di testi di informatica al mondo.

E tutti parlano di come si sta diffondendo Mac OS X tra gli sviluppatori. Senza quella X non sarebbe mai successo.

<link>Lucio Bragagnolo</link>lux@mac.com