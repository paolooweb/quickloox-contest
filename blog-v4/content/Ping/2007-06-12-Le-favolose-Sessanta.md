---
title: "Le favolose Sessanta"
date: 2007-06-12
draft: false
tags: ["ping"]
---

<strong>Guy Kawasaki</strong> ha isolato dal contesto le <a href="http://blog.guykawasaki.com/2007/06/slides_from_tod.html" target="_blank">sessanta slide usate da Steve Jobs</a> al keynote della Wwdc. Da vedere, ché si impara l'arte della presentazione in pubblico, gratis.