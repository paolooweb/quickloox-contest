---
title: "Era personale"
date: 2006-07-05
draft: false
tags: ["ping"]
---

Chiedo venia per l'autocelebrazione, ma classifico privatamente i Ping con un codice alfabetico di tre lettere, con l'alfabeto da ventisei lettere.

Questo Ping è il &#8220;numero&#8221; bzz. Significa che il prossimo sarà caa.

Lascio ai volontari del calcolo determinare quanti sono. Io mi limito a farmi modeste, ma sentite e sudate, congratulazioni per essere entrato nella mia tutta personale era della C. Che comunque non sarebbe mai arrivata senza tutta questa graditissima compagnia. Grazie. :-)