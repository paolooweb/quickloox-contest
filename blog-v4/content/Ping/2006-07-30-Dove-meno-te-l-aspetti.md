---
title: "Dove meno te l'aspetti"
date: 2006-07-30
draft: false
tags: ["ping"]
---

Giravo pigramente sulla pagina home di Freshmeat e, tra i progetti elencati, ce n'era uno esplicitamente compatibile con Mac OS X.

Qualche anno fa sarebbe stato impossibile. Oggi è insolito, nel senso che numerosissimi progetti girano anche su Mac ma richiedono una compilazione o vogliono X11. Un supporto diretto a Mac OS X ancora non è cos&#236; frequente, su <a href="http://www.freshmeat.net" target="_blank">Freshmeat</a>.

Sono rimasto contento. :-)