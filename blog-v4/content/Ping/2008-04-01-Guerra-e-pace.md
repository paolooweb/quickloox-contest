---
title: "Guerra e pace"
date: 2008-04-01
draft: false
tags: ["ping"]
---

Uno dei miei vicini si è dotato di un <em>router</em> wireless SpeedtouchQualcosa che è un'arma mortale. Il segnale della mia AirPort inizia ad andare e venire, la base sparisce e non viene più vista dal computer neanche da un centimetro di distanza, poi torna, ma non c'è Internet, o meglio va e viene a intervalli di quattro secondi. Non riesco a vedere la mia base, su cui sono praticamente seduto, ma la sua appare forte e chiara. Poi il vicino spegne la base e tutto torna a funzionare.

<cite>Si vis pacem para bellum</cite>: ho spiegato alla base di trasmettere sia su 802.11b che su 802.11g (prima faceva solo b) e ho attivato la funzione di resistenza alle interferenze. Sono in attesa del prossimo attacco nemico, ma noto che ho comunque guadagnato una tacca di segnale e sono dunque fiducioso.

Fortunatamente lo sforzo bellico mi lascia tempo per coltivare la civiltà e la filosofia. In un impeto di partecipazione e dopo lungo e profondo debito di riconoscenza, ho donato trenta euro a <a href="http://pcgen.sourceforge.net" target="_blank">PcGen</a>, che se li meritava da un bel po'.