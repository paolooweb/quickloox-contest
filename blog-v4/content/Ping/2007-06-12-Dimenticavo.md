---
title: "Dimenticavo"
date: 2007-06-12
draft: false
tags: ["ping"]
---

Home banking, meglio di prima, con Safari 3. Ogni tanto si incasinava la gestione del secondo codice di sicurezza. Adesso è perfetta.