---
title: "Falsi positivi"
date: 2004-03-03
draft: false
tags: ["ping"]
---

Storie di pene e di seno, ma anche di ego

Una volta ogni tanto questa rubrichetta esce dal tema Mac.
Oggi vorrei  mettere al bando tutti quelli che hanno scoperto i filtri bayesiani e li spremono fino all’estremo aggiustamento in modo da catturare ogni e qualsiasi forma di spam.

Di fatto però aumentano i loro falsi positivi: i messaggi legittimi ma giudicati spam e quindi filtrati senza pietà.

Oggi scopri che a Tizio non puoi scrivere da un indirizzo Tiscali; che non puoi mettere Caio in Bcc: che segnali una notizia a Sempronio utilizzando il form di un sito, ma lui non la vede. E tocca a te chiedergli se per caso hanno ricevuto, se per favore possono controllare, a che altro indirizzo scrivergli e così via.

Questa gente vuole la casella di email immacolata come il bidet in pubblicità, a qualunque prezzo; anche a costo di creare problemi a chi gli sta intorno. Falsi positivi sono loro, con la lista dei messaggi bella pulita e i problemi spazzati sotto il tappeto.

Inorridiscono al pensiero di una mail che promette il sistema per allungare il pene o aumentare il seno. Ma un sano spam di prodotti per sgonfiare l’ego, chi lo manda?

<link>Lucio Bragagnolo</link>lux@mac.com