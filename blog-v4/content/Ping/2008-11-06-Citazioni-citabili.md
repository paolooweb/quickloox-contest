---
title: "Citazioni citabili"
date: 2008-11-06
draft: false
tags: ["ping"]
---

<cite>iPhone è il tipico prodotto Apple. Sino al sessanta percento delle sue funzioni è molto facile da usare. Sul restante quaranta percento ti devi applicare. Ma se non aspiri al cento percento ti perdi il bello della vita informatica attuale.</cite>

Grazie ad <a href="http://www.teleart.it" target="_blank">Andrea Ack</a> per l'illuminazione. :-)