---
title: "Azione dopo azione"
date: 2003-03-14
draft: false
tags: ["ping"]
---

Chi ha in mano le azioni di Apple? Quello con la maggiore quota è...

Una caratteristica delle società americane sono i massicci investimenti in pacchetti azionari da parte di società di investimento che non hanno nessuno scopo se non quello di massimizzare i profitti gestendo il denaro a loro disposizione nella forma più redditizia possibile.

Per questo, al 28 gennaio scorso, il maggiore detentore di azioni Apple non era Steve Jobs né altri dirigenti (Bill Gates, che secondo certi disinformati si era comprato Apple, non ci è mai andato neanche vicino) ma <link>Lord Abbett</link>http://www.lordabbett.com/.

Non un distinto aristocratico inglese, ma una società di gestione del denaro. Che evidentemente crede nell’andamento favorevole di Apple abbastanza da avere portato la propria quota azionaria dal 5,29% all’8,6%. Se non sbaglio, nessun altro azionista possiede più del 5% di Apple.

Se il commercialista ti dice che avanzano soldi dalle tasse, un suggerimento io ce l’avrei.

<link>Lucio Bragagnolo</link>lux@mac.com
