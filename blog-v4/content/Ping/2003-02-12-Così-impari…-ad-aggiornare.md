---
title: "Così impari… ad aggiornare"
date: 2003-02-12
draft: false
tags: ["ping"]
---

Se vuoi il re del 3D, vacci piano con Jaguar

Il mio carissimo amico Stefano mi informa che i Mac dotati di scheda video nVidia GeForce 4Ti (GeForce 4 Titanium) o nVidia GeForce 3, se usano Mac OS X 10.2.3, non riescono a usare Maya 4.5 Personal Learning Edition. Se il computer ha una scheda video con meno di 32 megabyte di memoria video (per esempio la ATI Mobility Radeon M3 o M6) e usa 10.2.3 Maya è estremamente instabile.

Insomma, ci sono situazioni in cui Maya 4.5 Personal Learning Edition e Mac OS X 10.2.3 proprio non vanno d’accordo.

L’unica soluzione è aspettare ad aggiornare; Alias fa sapere che “sta lavorando a stretto contatto con Apple per risolvere il problema appena possibile”.
La Personal Learning Edition è gratuita ma, se serve a catturare nuovi utenti, sarà proprio meglio, cara Alias.

.<link>Lucio Bragagnolo</link>lux@mac.com