---
title: "Per i posteri"
date: 2009-01-02
draft: false
tags: ["ping"]
---

Se scrivi <a href="http://www.apple.com/hypercard" target="_blank">apple.com/hypercard</a> nel browser, vieni mandato su <a href="http://en.wikipedia.org/wiki/HyperCard" target="_blank">una pagina di Wikipedia</a>, l'enciclopedia <em>open source</em>.

Non è tanto che HyperCard, glorioso e semplicissimo database grafico programmabile, diventi definitivamente materiale enciclopedico per la posterità. È che si tratta di un comportamento davvero insolito per un'azienda.

Normalmente uno si aspetterebbe, per esempio, di finire su una pagina di supporto che raccoglie <a href="http://support.apple.com/kb/index?page=search&amp;q=hypercard" target="_blank">i ritrovamenti della parola HyperCard nel sito Apple</a>.

È bizzarro perfino più del famoso indirizzo <a href="http://mammals.org" target="_blank">mammals.org</a> (i <em>mammals</em> sono i mammiferi, in inglese).