---
title: "Petto o coscia?"
date: 2009-10-31
draft: false
tags: ["ping"]
---

È chiaro che il problema dominante del 2010 sarà insegnare agli ignoranti che il design è superiore alla somma delle sue parti. Problema non mio o di Apple, ma evidentemente globale.

Nel piccolo di Apple, segnalo questo articolo da O'Reilly Radar. Intitolato <a href="http://radar.oreilly.com/2009/10/the-iphone-conundrum-on-blackb.html">iPhone killer, BlackBerry e parti di pollo</a>, spiega (in inglese) che <cite>esiste una sfortunata tendenza a confondere la disponibilità di una manciata di parti di pollo con la produzione di un pollo vivo</cite>.

Non è l'elenco delle funzioni che fa l'apparecchio migliore.