---
title: "Da zero a nove"
date: 2008-07-08
draft: false
tags: ["ping"]
---

Scrive <strong>Paolo Frigerio</strong>:

<cite>Vorrei segnalarle <a href="http://www.hqmonza.it" target="_blank">il sito</a> che in due (dilettanti) abbiamo realizzato per un'associazione di cittadini.</cite>

<strong>Ci abbiamo messo mesi e mesi di tempo libero, imparando tutto da zero e usando Dreamweaver, naturalmente su Mac.</strong>

<cite>Il risultato ci pare buono, ci piacerebbe il suo parere.</cite>

Il mio parere conta virgola (è la seconda parte del titolo), meglio averne uno collettivo. Nel mio piccolo: guarda che cosa fanno l'entusiasmo e un computer che ti lascia lavorare invece di farti lavorare continuamente a lui.