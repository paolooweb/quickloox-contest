---
title: "La Raw War in cui vincono tutti"
date: 2006-02-14
draft: false
tags: ["ping"]
---

Guerra per modo di dire. Competizione salutare e benefica per tutti, piuttosto. L&rsquo;amico <strong>Flavio</strong> de <a href="http://www.loscrittoio.it/" target="_blank">Lo Scrittoio</a> segnala RAW Developer, programmma serio per la conversione del formato Raw, prodotto da una software house indipendente e disponibile solo su Mac OS X. Niente versione Windows. Perfetto per sfottere l&rsquo;amico. Ma &egrave; un&rsquo;altra questione.

Non sono programmi totalmente sovrapponibili, ma avere Aperture, il prossimo Lightroom di Adobe e anche <a href="http://www.iridientdigital.com/" target="_blank">RAW Developer</a> &egrave; indizio di qualcosa che ha detto bene Flavio: <cite>credo che il vicino futuro degli appassionati di fotografia digitale sia roseo davvero</cite>.