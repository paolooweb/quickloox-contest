---
title: "È solo un logout"
date: 2007-06-15
draft: false
tags: ["ping"]
---

<strong>Frank</strong> mi ha comunicato la notizia della morte di <a href="http://ggiove.blogspot.com/" target="_blank">Guido</a>.

Ho conosciuto Guido al momento di lanciare l'edizione italiana di Internet .Net Magazine. Senza la sua flessibilità e la sua competenza, unite alla capacità e alla velocità di scrittura, la rivista non sarebbe mai uscita in tempo. Guido è stato sempre capace di ricevere i nostri <em>brief</em> confusi e frettolosi e trasformarli presto e bene in articoli di qualità.

Qualche tempo dopo, ho ritrovato con piacere Guido sulla mailing list MacLovers. Abbiamo discusso molto e anche animatamente; lui sosteneva che Windows 95 avesse pareggiato Mac OS in funzioni e facilità di utilizzo. Per quanto accese, le nostre discussioni erano però sempre caratterizzate dal rispetto totale dell'altro come persona e da un livello molto più alto della media nelle argomentazioni, soprattutto per merito suo, sempre preparato e mai fuori luogo.

Dopo quel periodo ci siamo persi di vista e di lui ho ricevuto solo notizie occasionali. Per come l'ho conosciuto, me lo immaginavo in una posizione lavorativa di responsabilità, con pieno merito e con l'approvazione di tutti i suoi colleghi e sodali.

È una notizia triste. Ma non uno shutdown. Sono sicuro che Guido ha fatto solo logout.