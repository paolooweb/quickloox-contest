---
title: "Mille di questi Logitech<p>"
date: 2005-09-25
draft: false
tags: ["ping"]
---

Prosegue la recensione collettiva del mouse ottico<p>

Avevo proposto ai possessori di un Logitech Mx1000, innovativo stando alle dichiarazioni del produttore. Ecco l&rsquo;esperienza del <strong>Dott. Pettene</strong>:<p>

<cite>Io l&rsquo;ho comprato, il Logitech Mx1000. È davvero fantastico: è super ergonomico, una precisione mostruosa, una fluidità fantastica e una batteria che dura tantissimo (pur mettendoci poco a caricarsi). Io lo uso anche per giocare e ti posso dire che in confronto ad altri mouse è in assoluto uno dei migliori, anche se non ho provato il Raptor M2. L&rsquo;unico neo risiede nel fatto che la targhetta in alluminio sita posteriormente sull&rsquo;impugnatura tende a staccarsi, ma con un po&rsquo; di Attak tutto torna normale.</cite><p>

Altre impressioni, ma basate sull&rsquo;uso concreto, sono sempre benvenute. Mica bisogna essere giornalisti per scrivere di un mouse. :-)<p>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>