---
title: "Come prima di iPhone"
date: 2010-12-09
draft: false
tags: ["ping"]
---

Titolare di guerra tra Apple e Adobe su Flash certamente fa effetto e fa ascolti. Se qualcuno si azzardasse a sollevare una questione generale su Flash, verrebbe bollato qua e là come <i>talebano</i>, <i>fanboy</i>, <i>servo</i> di Apple più varie ed eventuali.

Bene, facciamo finta che Apple non esista e che ci sia solo Flash.

Ecco una delle recenti <a href="http://www.ubergizmo.com/15/archives/2010/12/divx_hiq_boosts_video_playback_performance_by_replacing_flash.html" target="_blank">notizie di &#220;bergizmo</a>:

<cite>DivX ha annunciato <a href="http://labs.divx.com/node/14711" target="_blank">DivX HiQ</a>, una tecnologia che permette a DivX di sostituire il tipico</cite> player <cite>Flash su web con quello di DivX. La cosa avviene al volo, sito per sito. DixX ha da un po' un vantaggio significativo di prestazioni su Flash. Semplicemente usa molte meno risorse del processore rispetto al prodotto Adobe. [&#8230;] Al momento DivX HiQ funziona solo su Motion, Facebook, Espn, MetaCafe, Revision3, The Onion, Vimeo e YouTube, ma il numero di siti potrà crescere nel tempo senza ricorrere a un aggiornamento. La cosa avverrà dietro le quinte.</cite>

<cite>Che cosa c'è di interessante? Per esempio, i</cite> netbook <cite>che non erano abbastanza veloci per riprodurre video Flash ad alta risoluzione ora ce la potrebbero are. Invece di aggiornare lo hardware, perché non migliorare il software? [&#8230;] Secondo, si consuma meno elettricità.</cite>

Più passa il tempo, più la faccenda di Flash somiglia ai cellulari prima che arrivasse iPhone. Una truffa generalizzata propinava ai consumatori prodotti che avrebbero potuto essere molto migliori, se solo le aziende coinvolte ci avessero messo un pizzico di impegno. Apple non ha inventato niente e ha solo messo genialmente insieme tecnologie tutte esistenti, certo con sopra un'interfaccia nuova.

Flash è stato il minimo comune denominatore su cui ci siamo tutti seduti accontentandoci e pensando che fosse il meglio che si poteva avere. Come per i cellulari prima di iPhone, invece era il meno peggio. Una truffa mascherata.