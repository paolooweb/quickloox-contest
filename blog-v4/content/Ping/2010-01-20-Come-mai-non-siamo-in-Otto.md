---
title: "Come mai non siamo in Otto?"
date: 2010-01-20
draft: false
tags: ["ping"]
---

Ho scoperto solo ora che Apple alimenta (poco) un canale Twitter dedicato a Automator, Servizi e AppleScript, intestato a un fantomatico <a href="http://twitter.com/macautomation" target="_blank">Otto the Automator</a>.

Per me questo tipo di informazione non è mai abbastanza e sono immediatamente diventato un <i>follower</i>. Quando riuscirò a essere un <i>leader</i> dell'automazione di Mac, invece, sarà sempre troppo tardi.