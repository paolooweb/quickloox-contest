---
title: "Macchie di Leopard"
date: 2006-11-06
draft: false
tags: ["ping"]
---

Ho installato Leopard.

Se non fosse per lavoro, ne avrei fatto volentieri a meno. Per dare un'idea, la registrazione iniziale (quella tipica che parte dopo una installazione di Mac OS X) è fuori schermo per tre quarti e uno ha da compilarla alla cieca, giocando di tabulatore e barra spaziatrice&#8230; oppure facendosi venire una buona idea (io l'ho avuta, non so se era buona, ma ha risolto).

Per chi volesse provare brividi simili ai miei, ci sono da tirare fuori 500 dollari oppure, per esempio, scaricarsi i <a href="http://developer.apple.com/opensource/internet/nightlywebkit.html" target="_blank">Nightly Build di WebKit</a> e godersi un pezzettino di Safari come sarà.

Che devo averlo già scritto tempo fa. Va bene lo stesso. :-)a