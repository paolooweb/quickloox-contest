---
title: "Vecchi fin da giovani"
date: 2010-08-14
draft: false
tags: ["ping"]
---

In missione ipermercato, noto un cartello inquietante: <cite>supervalutiamo il vostro vecchio notebook o netbook</cite>.

Ma esistono <i>netbook</i> vecchi? Voglio dire, ho sotto gli occhi un PowerBook 12&#8221; che funziona imperterrito a otto-nove anni di distanza. Anche solo cinque anni fa, dei <i>netbook</i> non c'era neanche l'idea.

Ho il sospetto che esistano <i>netbook</i> che hanno già stufato il proprietario, passata la novità e il fascino dell'oggettino piccino, per la manifesta inferiorità nell'uso corrente. Come volevasi dimostrare.