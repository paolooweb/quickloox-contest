---
title: "Soldi ben spesi"
date: 2006-06-27
draft: false
tags: ["ping"]
---

Molti anni fa ho acquistato BBEdit a un prezzo che, oggi, sarebbe superiore.

Tutto il lavoro di <em>versioning</em> del sito di Sourcesense, cui ho dato una mano per i testi, è stato gestito via <a href="http://subversion.tigris.org/" target="_blank">Subversion</a>.

All’inizio mi sono lasciato trascinare dai <em>geek</em> di <a href="http://www.sourcesense.com/sourcesense/index.html" target="_blank">Sourcesense</a> a usare il Terminale. Poi, man mano, ho capito che BBEdit mi permetteva di gestire interamente la cosa tramite interfaccia utente. Lo fanno molti altri programmi. Ma BBEdit integra perfettamente la gestione del testo, più quella dell’Html/Xml necessario, più quella del sistema di versioning. Immediato e preciso, senza bisogno di nient’altro.

Se anni fa avessi saputo quanto mi sarebbe stato utile <a href="http://www.barebones.com/products/bbedit/" target="_blank">BBEdit</a> oggi, sarei stato disposto a pagarlo anche di più.