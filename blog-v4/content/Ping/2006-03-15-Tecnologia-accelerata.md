---
title: "Tecnologia accelerata"
date: 2006-03-15
draft: false
tags: ["ping"]
---

Sar&agrave; che le vedo di rado, ma in <a href="http://www.macatwork.net" target="_blank">Mac@Work</a> sta per cominciare un evento Epson-Wacom-Microsoft e, se la prima &egrave; abbastanza prevedibile e l&rsquo;ultima &egrave; meglio perderla che trovarla, le tavolette grafiche sono invece sempre pi&ugrave; sorprendenti ogni volta che ho l&rsquo;occasione di vedere dal vivo le novit&agrave;.