---
title: "Siamo tutti l&#236;"
date: 2007-06-11
draft: false
tags: ["ping"]
---

Nella stanza di iChat <code>wwcd07</code> (Comando-Maiuscole-G e digita il nome) a chiacchierare del keynote di Steve Jobs in apertura della Worldwide Developers Conference Apple.

Ci sono anche <a href="http://forum.macworld.it/showthread.php?p=78473&amp;posted=1#post78473" target="_blank">i forum di Macworld</a>. :)