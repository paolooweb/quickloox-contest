---
title: "Dock virtuale e programmatori veri"
date: 2004-05-05
draft: false
tags: ["ping"]
---

Ogni tanto è bello incontrare italiani che fanno cose interessanti per Mac

Uno dei pezzettini di software che contribuiscono a fare di Mac OS X un sistema ancora migliore è DockFun, un simpatico aggeggio che permette di ruotare una quantità infinita di configurazioni del Dock. Si può avere un Dock per la grafica, uno per giocare, uno per un lavoro particolare che richiede più documenti eccetera.

E uno degli autori è italianissimo. Trovi DockFun e anche qualcos’altro a <link>Donelleschi Software</link>http://www.donelleschi.com.

Bello quando la forma si accompagna alla funzione.

<link>Lucio Bragagnolo</link>lux@mac.com