---
title: "Ho cambiato idea"
date: 2010-06-22
draft: false
tags: ["ping"]
---

<a href="http://www.avc.com/a_vc/2010/05/ive-changed-my-mind-about-the-ipad.html" target="_blank">Scrive</a> Fred Wilson, capitalista di ventura presso Union Square Ventures:

<cite>Ho preso un iPad per la casa quando è uscita la versione</cite> wifi<cite>. L'ho usato per un giorno e poi ho scritto <a href="http://www.avc.com/a_vc/2010/04/thoughts-on-the-ipad.html" target="_blank">un post su iPad con iPad</a>. Non ero molto entusiasta riguardo all'apparecchio. Alla fine della recensione scrivevo &#8220;Nel tempo può diventare una piattaforma di</cite> computing <cite>di rilievo ma penso che ora non lo sia e non penso che Apple abbia nelle mani l'asso che ha avuto con iPhone&#8221;.</cite>

<cite>Nella scorsa settimana me ne sono innamorato e adesso vi spiego perché.</cite>

<cite>Può essere il miglior apparecchio per la posta che abbia mai avuto. [&#8230;] Se mia moglie o le figlie mi interrompono, è facile posare l'apparecchio e avviare una conversazione. iPad rende meno impegnativo usare il computer. [&#8230;]</cite>

<cite>Mi piace anche il modo come sta sul tavolo di cucina e viene usato per ogni sorta di piccole cose. Sono tornato a casa ieri sera e la mia figlia più grande <a href="http://www.jessicasarawilson.com/" target="_blank">Jessica</a> stava preparando</cite> guacamole<cite>, usando iPad per leggere la ricetta. Ci stava versando sopra succo di limone la cosa mi è piaciuta. Una specie di battesimo.</cite>

<cite>Lo usiamo per i figli lontani, per fare cruciverba, giocare, consultare menu per ordinare a domicilio, leggere e occasionalmente guardare un video. Ha sostituito il computer della cucina. È diventato uno di famiglia. E quando arrivano ospiti, si divertono a usarlo. È ottimo per una serata in compagnia.</cite>

<cite>I nostri iPhone, Android, Blackberry sono personali. Sono con noi ovunque. Il nostro iPad è il computer di famiglia come il MacBook della cucina non è mai stato. [&#8230;]</cite>

<cite>Nella mia recensione iniziale, mi ero focalizzato sulle capacità. E le tavolette sono in mezzo tra la potenza e l'utilità dei portatili e le dimensioni e gli extra di uno</cite> smartphone<cite>. Ma creano anche uno spazio intermedio di usabilità. Quello che mi è sfuggito nel mio primo giorno con iPad. Dà l'idea del computer meno di qualsiasi altro apparecchio-computer che ho avuto. Mi facilita in un modo che gli altri apparecchi non riescono a fare. Cos&#236; ora sono convinto che le tavolette avranno un ruolo importante nelle nostre vite e nella nostra casa.</cite>

Questo è un signore che si guadagna da vivere finanziando progetti tecnologici promettenti. Non un <i>blogger</i> che passa le serate a scrivere opinioni gratis.