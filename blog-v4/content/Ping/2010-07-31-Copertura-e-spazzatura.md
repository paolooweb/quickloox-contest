---
title: "Spazzatura e copertura"
date: 2010-07-31
draft: false
tags: ["ping"]
---

Sempre a seguito dell'esordio italiano di iPhone 4, confrontare prego la copertura dell'evento da parte di <a href="http://www.zeusnews.com/index.php3?ar=stampa&amp;cod=12799" target="_blank">ZeusNews</a> e quella di <a href="http://www.setteb.it/liphone-4-pochi-pezzi-ed-i-furbi-non-vendono-senza-abbonamento-9415" target="_blank">SetteB.it</a>.

La seconda è copertura giornalistica, con galleria fotografica &#8211; loro c'erano veramente &#8211; e informazione di prima mano.

La prima, tutta sensazionalismo gratis, accompagnata da un forum di livello infimo, vale giusto il trasporto alla discarica digitale più vicina.