---
title: "Una pila di schifezze"
date: 2006-09-13
draft: false
tags: ["ping"]
---

<cite>Microsoft, nella mia opinione, non innova. Vista sembra una pila di schifezze, confrontato con Mac OS X e Ubuntu con Glx. Il loro software [di Microsoft] è bacato, esoso e induce stress, I loro strumenti di sviluppo [di Microsoft] sono ordinari, progettati e realizzati da comitati che dovrebbero risolvere ogni tuo concepibile problema, mentre in realtà sono ideali per non risolverne nemmeno uno.</cite>

<a href="http://peterwright.blogspot.com/2006/09/good-bye-microsoft-pete-has-now-left.html" target="_blank">Pete Wright</a> non era proprio l'ultima ruota del carro, in fatto di software per Windows. E adesso scrive dal suo Mac <cite>usando NeoOffice Writer mentre il Pc sotto la scrivania sta eliminando Windows per l'ultima volta, a installare Ubuntu Linux.</cite>

Un altro che ha capito.

<em>Here's to the crazy ones&#8230;</em>.