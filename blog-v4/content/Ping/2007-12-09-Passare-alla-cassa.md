---
title: "Passare alla cassa"
date: 2007-12-09
draft: false
tags: ["ping"]
---

La polemica personale mi scoccia, ma non sopporto le bugie. Qualche giorno fa ho pubblicato un post che paragonava, in fatto di disorganizzazione interna e mancanza di strategia a lungo termine, la Dell dell 2007 alla Apple del 1997.

Rispetto alla Apple del 1997, nei commenti di questo blog si è scritto <cite>Ci sono diversi libri che spiegano come ad un certo punto a Cupertino non c'era liquidità neppure più per l'operatività di un trimestre, insomma, si era vicini alla bancarotta</cite>.

Lo si è fatto senza citare i libri, che è scorretto. Senza citare neanche un passaggio di questi libri, che è doppiamente scorretto. Fin qui, comunque, la correttezza non è una barriera di ingresso per commentare in questo blog. Invece le bugie lo sono e non sono tollerate.

Durante il 1997 Fred Anderson, allora Chief Operating Officer di Apple, dichiarò che Apple aveva bisogno di almeno 0,5 miliardi di dollari in liquidità per funzionare a regime (come da Mwj del 21 gennaio 1998). Per liquidità si intendono tradizionalmente la cassa vera e propria e gli investimenti a breve termine. Questa la liquidità in miliardi di dollari di Apple trimestre per trimestre solare, da fine 1996 a inizio 1998 (a comprendere tutta l'era Amelio, per finire quando inizia la seconda era Jobs). Il tutto è arrotondato ai centomila dollari più vicini.

<table border="1" cellspacing="1">
	<tr>
		<th>Trimestre</th> <th align="right">Cassa</th align="right"> <th>Breve termine</th> <th align="right"><strong>Totale</strong></th>
	</tr>
	<tr>
		<td>I trimestre 1996</td> <td align="right">0,5</td> <td align="right">0,1</td> <td align="right"><strong>0,6</strong></td>
	</tr>
	<tr>
		<td>II trimestre 1996</td> <td align="right">1,4</td> <td align="right">0,0</td> <td align="right"><strong>1,4</strong></td>
	</tr>
	<tr>
		<td>III trimestre 1996</td> <td align="right">1,6</td> <td align="right">0,2</td> <td align="right"><strong>1,7</strong></td>
	</tr>
	<tr>
		<td>IV trimestre 1996</td> <td align="right">1,2</td> <td align="right">0,6</td> <td align="right"><strong>1,8</strong></td>
	</tr>
	<tr>
		<td>I trimestre 1997</td> <td align="right">1,3</td> <td align="right">0,2</td> <td align="right"><strong>1,5</strong></td>
	</tr>
	<tr>
		<td>II trimestre 1997</td> <td align="right">1,0</td> <td align="right">0,2</td> <td align="right"><strong>1,2</strong></td>
	</tr>
	<tr>
		<td>III trimestre 1997</td> <td align="right">1,2</td> <td align="right">0,2</td> <td align="right"><strong>1,5</strong></td>
	</tr>
	<tr>
		<td>IV trimestre 1997</td> <td align="right">1,2</td> <td align="right">0,4</td> <td align="right"><strong>1,6</strong></td>
	</tr>
	<tr>
		<td>I trimestre 1998</td> <td align="right">1,3</td> <td align="right">0,5</td> <td align="right"><strong>1,8</strong></td>
	</tr>
</table>

Tutti i dati sono a disposizione di chiunque, tramite il sito della <a href="http://www.sec.gov" target="_blank">Security Exchange Commission</a>. Qualcuno ora deve dirmi in quale momento del 1997 Apple non ebbe la liquidità per l'operatività di un trimestre.