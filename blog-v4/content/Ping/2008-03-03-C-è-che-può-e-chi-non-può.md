---
title: "C'è chi può e chi non può"
date: 2008-03-03
draft: false
tags: ["ping"]
---

Un amico mi invia un link dal Lussemburgo per mostrarmi <a href="http://store.apple.com/Apple/WebObjects/luxstore?aosid=p204&sitename=apple.lu+%3A+AUG+Luxembourg&siteid=403538&program_id=2701&tduid=a84c290b2a2bfd20a450e66f16e93dc4&type=bizbelfrLU&qprm=82404" target="_blank">i prezzi sull'Apple Store locale</a> e suscitare l'invidia che nasce spontanea in un italiano costretto a subire l'Iva al 20 percento.

Me lo manda, perché è costretto, sotto forma di un link in Outlook. Outlook prima si rifiuta di inviarlo, perché (traduco) Outlook <cite>ha bloccato l'accesso a un allegato potenzialmente pericoloso</cite> (cioè il pericolosissimo link cliccabile qui sopra). L'amico insiste per inviare il messaggio, anche se contiene un link a un sito Internet, e riceve questo avvertimento:

<cite>Questo messaggio contiene allegati potenzialmente insicuri. I destinatari che usano Microsoft Outlook potrebbero non essere in grado di aprirli. Vuoi inviarlo comunque?</cite>

Diceva quello, c'è chi può e chi non può. Io, puah.