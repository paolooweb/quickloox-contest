---
title: "Primi passi all'aperto"
date: 2009-07-09
draft: false
tags: ["ping"]
---

Come promesso torno sull'argomento dello <a href="http://www.builtny.com/showPage.php?pageID=1628" target="_blank">zaino Built</a> che ho comprato per quando posso viaggiare leggero, questa volta con impressioni di utilizzo.

Non è semplicissimo infilare il portatile nella tasca perché non c'è struttura rigida e, nel caso del 17&#8221;, la cerniera non chiude fino in fondo, lasciando una luce di decina di centimetri. La cosa era prevista, guardando le dimensioni dell'oggetto, e inoffensiva; l'unico modo in cui qualcuno potrebbe tentare di estrarre il portatile dalla tasca è avermi ridotto preventivamente in stato di incoscienza. Le cerniere sono comode ma solo a zaino sfilato; a zaino indossato danno verso l'interno e proprio non ci sono santi.

La tasca principale contiene più che agevolemnte il portatile e altro, sia in spessore che in larghezza. La classica rivista trova spazio senza difficoltà, ma &#8211; per rendere l'idea - in base allo spessore potrebbero starci due portatili, senza problemi. Oltretutto l'insieme in neoprene è assai elastico. In larghezza, anche al netto del portatile, c'è spazio in abbondanza sui due lati per altra oggettistica.

Una piccola tasca situata nella parte inferiore e interna della spallina sinistra alloggia portafogli, chiavi varie, forse anche un alimentatore, ma dovrei provare. Non dà alcun fastidio e, sempre a zaino indossato, è totalmente inaccessibile dall'esterno, mentre il proprietario ci arriva facilmente.

L'assetto dello zaino è alto sulle spalle; il peso non finisce per affaticare la zona lombare e rimane sulla parte superiore della schiena. La cinghietta anteriore è non necessaria, ma chi preferisce può allacciarla per sentire meglio addosso lo zaino, o addirittura per il <i>look</i>. Non si può levare, ma sparisce sotto una spallina e non penzola antiesteticamente.

La maniglia è di ottima fattura, robusta molto oltre il necessario e comoda il giusto. Per la forma dello zaino comunque viene naturale porselo in spalla e ricorrere alla maniglia solo per appoggiarlo da qualche parte.

L'alimentatore, nella tasca principale o forse in quella secondaria, avrebbe trovato posto dieci volte, ma a che serve essersi comprato un MacBook Pro con superbatteria per poi cedere sempre alla paranoia? L'ho lasciato a casa e ora tutto sembra indicare che porterò vittoriosamente a termine la mezza giornata di uso del portatile.

Non userò sempre il Built; certe volte la giornata è più impegnativa. Però dà una bella sensazione quando le esigenze di mobilità sono più controllate.