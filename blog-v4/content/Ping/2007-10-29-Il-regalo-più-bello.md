---
title: "Il regalo più bello"
date: 2007-10-29
draft: false
tags: ["ping"]
---

Mi ha emozionato ricevere da <strong>Emanuele</strong> una edizione limitata, limitatissima di PlugSuit e TwiceTab.

Il primo estende il meccanismo dei plugin ad applicazioni che non sono necessariamente Safari, il secondo è un plugin che consente di aprire un tab su Safari (in Leopard) con un doppio clic.

Tutto materiale gratuito che chiunque può avere andando sul sito di <a href="http://infinite-labs.net/" target="_blank">Infinite Labs</a>. Tuttavia è materiale originale creato da Emanuele e sono orgoglioso di possedere uno dei primi cinque Cd sui quali è stato eternato.