---
title: "(re)Furbi(shed)"
date: 2006-09-29
draft: false
tags: ["ping"]
---

Ricevo e volentieri pubblico da <strong>Giuseppe</strong>:

<cite>Una cosa particolarmente positiva scoperta su eBay è stata la disponibilità di alcuni PowerBook 15&#8221; da 1,67 GHz REFURBISHED presso il negozio <a href="http://stores.ebay.it/Eritech-International-Incorporated" target="_blank">Eritech International Inc.</a> a 1.395 USD più spedizione e spese varie, in modalità <strong>compralo subito</strong>.</cite>

<cite>Il mio iBook è in assistenza da due mesi e per me è prematuro acquistare un MacBook Pro, per cui sto cercando di mandare il bonifico bancario al gestore del negozio ed avere uno dei PowerBook disponibili. Te lo segnalo perché a qualche Mac user potrebbe interessare: si evita il disagio della partecipazione all'asta&#8230;</cite>

<em>Refurbished</em> sta per <em>rigenerato</em>. Un PowerBook rimesso a nuovo e tirato a lucido può davvero essere un buon affare e un acquisto furbo.