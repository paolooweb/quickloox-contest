---
title: "Cornette e sordità"
date: 2010-05-19
draft: false
tags: ["ping"]
---

Trovo significativa la notizia della cessazione delle vendite di Response Point mentre esplodono le vendite degli <i>smartphone</i> (dei quali iPhone è solo uno dei rappresentanti).

Response Point è, fino al 31 agosto e poi mai più, un <cite>sistema telefonico per piccole imprese</cite>, come lo ha battezzato Microsoft.

Basta <a href="http://www.microsoft.com/responsepoint/product-information/hardware-phone-system-device-manufacturers.aspx" target="_blank">guardare i modelli</a> per capire che c'era un mondo diverso, vent'anni fa. E gente che ci vive ancora, sorda ai tempi che cambiano.