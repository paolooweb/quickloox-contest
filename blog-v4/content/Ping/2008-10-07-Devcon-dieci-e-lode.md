---
title: "Devcon dieci e lode"
date: 2008-10-07
draft: false
tags: ["ping"]
---

Per gentilezza di <a href="http://www.sinapsistudio.it" target="_blank">Giulio</a> sono in grado di linkare la <a href="http://www.fmdevcon.com/photogallery/" target="_blank">rassegna fotografica</a> del raduno italiano degli sviluppatori FileMaker, svoltosi a Rimini dal 17 al 19 settembre.

Sulle facce i giudizi possono variare, ma sui cervelli non c'è proprio discussione: complimenti e auguri per la prossima edizione.