---
title: "Piccoli particolari periferici"
date: 2008-12-07
draft: false
tags: ["ping"]
---

Altra cosa che Assistente Migrazione non ha gestito correttamente nel passare i dati dal disco del PowerBook caffeinizzato al disco del PowerBook sostitutivo sulla cui tastiera non si è ancora versato niente: la preferenza che fa scaricare in <em>background</em> gli aggiornamenti importanti. Io l'avevo selezionata, sul &#8220;nuovo Mac&#8221; invece no.