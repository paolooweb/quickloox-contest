---
title: "Quanto dura l'avventura"
date: 2008-12-14
draft: false
tags: ["ping"]
---

Ci volevano iPhone e iPod touch per fare tornare gli adventure game alla ribalta.

Quando ho visto <a href="http://itunes.apple.com/WebObjects/MZStore.woa/wa/viewSoftware?id=298071041&amp;mt=8" target="_blank">1112 episode 01 di Agharta Studio</a> sono rimasto davvero sorpreso e mi auguro che si venda bene.

Non è la (lodevole) operazione di recupero di <a href="http://itunes.apple.com/WebObjects/MZStore.woa/wa/viewSoftware?id=287653015&amp;mt=8" target="_blank">Frotz</a>, che poi esisteva già sui personal da anni, ma proprio un nuovo debutto. Complimenti.