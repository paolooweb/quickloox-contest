---
title: "Stiamo a vedere"
date: 2007-09-10
draft: false
tags: ["ping"]
---

Metto da parte momentaneamente la mia idiosincrasia all'uso del video per consigliare vivamente <a href="http://www.youtube.com/watch?v=l6nHBHVNHRU" target="_blank">questo filmato</a> realizzato da <strong>Stefano</strong> di <a href="http://www.ipodpalace.com" target="_blank">iPodPalace</a>.

Al momento, non ho trovato niente di più ravvicinato riguardante l'esperienza d'uso di iPod touch. E non è la solita demo prefabbricata.