---
title: "Tutto è relativo"
date: 2008-08-07
draft: false
tags: ["ping"]
---

MacBook Air, a rigor di processore, è il Mac più lento che ci sia a listino.

Però <strong>Marioz</strong> mi ha mostrato sul suo Air le foto fatte in Bretagna, da Aperture. Nessuna pausa, nessuna attesa. Basse prestazioni s&#236;, in relativo.