---
title: "Chi non conosce il software&#8230;"
date: 2006-12-06
draft: false
tags: ["ping"]
---

Lascio la parola a <strong>Giuseppe</strong>.

<cite>Oggi un collega mi ha mostrato tutto orgoglioso una specie di pre-release di Windows Vista.</cite>

<cite>Muoveva il mouse su una specie di dock, le cui icone si ingrandivano durante lo scorrimento per poi rimpicciolirsi&#8230;</cite>

<cite>Credo di aver già visto qualcosa del genere&#8230; erano i tempi di Mac OS X 10.0. ;-)</cite>

<cite>Domani gli mostro il mio PowerBook nel pieno delle sue funzioni e gli dimostro che il suo futuro è semplicemente il mio passato&#8230;</cite>

Ecco. Se c'era bisogno di una pietra tombale sul dibattito, è arrivata. Vista è una brutta copia del passato. Solo che Mac OS X 10.0 non prendeva virus.

Parafrasando Vico, chi non conosce il software è condannato a ripeterlo.