---
title: "Un po&rsquo; troppo virtuale"
date: 2006-04-17
draft: false
tags: ["ping"]
---

Incoraggiato da <strong>Paolo</strong> ho iniziato i miei primi esperimenti di virtualizzazione, allo scopo di avere una istanza di Linux funzionante sotto <a href="http://www.kberg.ch/q/" target="_blank">Q</a>.

Ma non vado da nessuna parte. Il meglio che ho ottenuto &egrave; fare eseguire alla macchina virtuale il boot da un&rsquo;immagine disco di <a href="http://www.gentoo.org/" target="_blank">Gentoo</a>. Alla fine ottengo per&ograve; solo un pinguino in alto a sinistra e, per il resto, schermo nero e silente.

Suppongo, da un primo e superficiale approfondimento, di dover inserire qualche comando modificatore per l&rsquo;emulatore Qemu che sta sotto Q, per risolvere un (suppongo sempre) problema video. Non &egrave; colpa di Q, che &egrave; dichiaratamente in stadio di sviluppo alpha. Tuttavia, ecco, ancora non &egrave; una cosa immediata. Per me, intendo.

La virtualizzazione, per ora e per me, &egrave; un obiettivo virtuale.