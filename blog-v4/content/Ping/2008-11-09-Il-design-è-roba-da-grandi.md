---
title: "Il design è roba da grandi"
date: 2008-11-09
draft: false
tags: ["ping"]
---

Impagabile <a href="http://www.sinapsistudio.it/" target="_blank">Giulio</a>:

<cite>Mia figlia Anna, 9 mesi.</cite>

<cite>La faccio giocare spesso con le mie cose, anche con il mio iPhone.</cite>

<cite>Viene un amico, vede la scena e mi da del tecnofighetto. tira fuori il suo Zune (marrone! Bleah) e lo mette davanti ad Anna a fianco dell'iPhone.</cite>

<cite>Anna guarda zune, poi guarda l'iPhone, lo prende e lo morde. Lo maneggia e ci gioca. Zune rimarrà li - ignorato - per tutta la serata.</cite>

<cite>Siamo d'accordo. Non ha valenze di mercato. Non vale come statistica e nemmeno come raffronto tecnico.</cite>

<cite>Ma secondo me è anche più importante. Non di solo pane vive l'uomo&#8230; con quello che segue.</cite>

Non vale come mercato, né come statistica né come raffronto tecnico. Spiega però benissimo che cosa sia il <em>design</em>. Roba cos&#236; grande che ci vuole una bambina piccola. Intanto i ciechi compilano gli elenchi di funzioni.