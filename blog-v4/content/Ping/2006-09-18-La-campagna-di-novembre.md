---
title: "La campagna di novembre"
date: 2006-09-18
draft: false
tags: ["ping"]
---

Ci riproviamo? Fuori i suggerimenti per il Cd-Rom di Macworld novembre.

Non c'è bisogno di ripetere cose già dette, da LaTeX in poi.

Gli acutissimi per cui sarebbe più facile accontentare altre persone piuttosto che fare come preferisco io possono pure astenersi. :-)