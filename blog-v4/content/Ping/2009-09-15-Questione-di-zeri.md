---
title: "Questione di zeri"
date: 2009-09-15
draft: false
tags: ["ping"]
---

Apple Italia sostiene che l'installazione di Snow Leopard è <a href="http://www.apple.com/it/macosx/refinements/enhancements-refinements.html" target="_blank">fino al 45 percento più veloce</a> di quella di Leopard.Poi dichiara anche il <a href="http://www.apple.com/it/macosx/refinements/" target="_blank">50 percento</a>, ma insomma ci siamo capiti.

Un ingegnere Microsoft ha bloggato sulle <a href="http://blogs.technet.com/chris_hernandez/archive/2009/09/02/windows-7-upgrade-performance.aspx" target="_blank">prestazioni di aggiornamento di Windows 7</a>. E rivela che l'obiettivo di Microsoft era migliorare la velocità dell'aggiornamento a Windows 7 almeno del cinque percento.

Come Snow Leopard. A parte uno zero.

Sono elencati i cronometraggi delle installazioni in varie situazioni e c'è la possibilità che un aggiornamento a WIndows 7 duri 1.220 minuti. Esatto, venti ore e venti minuti. Un bel miglioramento, dato che aggiornare a Vista la stessa configurazione avrebbe richiesto ventuno ore.

Non comincerei neanche a parlare dello spazio su disco, area dove Snow Leopard libera fino a sette gigabyte di disco (in realtà di più, perché l'indicizzazione di Spotlight ora è più compatta).

E questa è solo l'installazione&#8230;