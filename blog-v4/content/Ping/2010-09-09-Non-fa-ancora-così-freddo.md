---
title: "Non fa ancora cos&#236; freddo"
date: 2010-09-09
draft: false
tags: ["ping"]
---

Apple ha talmente trascurato i Mac, per pensare solo a iPad e iPhone, che <a href="http://www.pcmag.com/article2/0,2817,2368146,00.asp" target="_blank">i premi Readers' Choice di Pc Magazine per il 2010</a> sono andati ad Apple per la categoria <i>Computer Notebooks</i> (portatili), ad Apple per la categoria <i>Computer Desktops</i> (da scrivania) e ad Apple per la categoria <i>Network Routers</i> (AirPort).

Merita notare che intanto Apple si è aggiudicata il primo posto nelle categorie <i>Cellphones</i> (iPhone) e <i>Portable Media Players</i> (iPod).

<i>Readers' Choice</i> significa <i>scelta dei lettori</i>. Per un inquadramento storico e politico, cinque o sei anni fa Pc Magazine neanche parlava di prodotti Apple, se non per ospitare occasionalmente qualche articolo sul prossimo fallimento dell'azienda.

Non fa ancora cos&#236; freddo, ma si è già visto un cappotto.