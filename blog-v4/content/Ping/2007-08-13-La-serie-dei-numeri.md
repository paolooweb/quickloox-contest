---
title: "La serie dei numeri"
date: 2007-08-13
draft: false
tags: ["ping"]
---

Altre due cose che si perdonano a Numbers di <a href="http://www.apple.com/iwork/trial" target="_blank">iLife '08</a>, a patto che vengano rapidamente rimediate in un prossimo update: la mancanza di serie (le ho fatte artigianalmente a lungo su AppleWorks, prima di scoprire che si facevano automaticamente) e la mancanza di AppleScript.

Sulle serie potrei sbagliarmi, ma se il comando c'è, non l'ho trovato. E sembra (sembra) che non si possa incollare su più celle il contenuto di una cella sola.

<em>Aggiornamento</em>: grazie a <strong>Nero</strong>, ho scoperto che, trascinando il cerchietto che compare in basso a destra nel bordo della cella selezionata, la serie si propaga. Mica male. Una cosa in meno da perdonare.