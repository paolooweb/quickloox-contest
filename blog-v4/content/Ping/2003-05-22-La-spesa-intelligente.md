---
title: "La spesa intelligente"
date: 2003-05-22
draft: false
tags: ["ping"]
---

Qual è la suite giusta per l’ufficio e la produttività individuale

C’era una volta il mercato dei browser, su cui avevano costruito la loro fortuna aziende come Netscape. Un’altra azienda, in seguito condannata in tribunale a seguito di azioni simili a quella che sto per descrivere, decise di fare le scarpe a Netscape iniziando, lei che poteva permetterselo, a regalare il browser.

Netscape finì nella polvere e venne poi acquistata da America On Line.

Quell’altra azienda ha costruito la propria fortuna su sistemi operativi, programmi per la produttività individuale e abuso di monopolio, sancito in tribunale.

La comunità del software libero sviluppa oggi sistemi operativi che funzionano meglio, vanno più veloci, sono più sicuri e sono costantemente migliorati e aggiornati. Tra i suoi componenti c’è Apple, che lavora insieme e a fianco della comunità su progetti come Darwin, WebCore e altri.

La morale? Semplice. Fai come quando il browser è diventato gratis e, tu che puoi permettertelo, scegli il software gratuito, specie quando è equivalente in prestazioni. Se quell’azienda finisse eventualmente nella polvere, beh, succede. È successo anche a Netscape (solo uno dei tanti esempi possibili) e quell’azienda sa benissimo perché.

<link>Lucio Bragagnolo</link>lux@mac.com