---
title: "Imbecilli al quattro per cento"
date: 2002-06-13
draft: false
tags: ["ping"]
---

Macintosh conta nel mondo dei computer più dell’Italia nel mondo

Recentemente ho risentito un discorso imbecille (e che torna sempre) per bocca di un imbecille a cui era stato chiesto un parere sui Mac.
L’imbecille ha risposto “Bello, bello, peccato però che non conti niente, avendo il 4% del mercato”.
Gli ho chiesto come si sentiva, in quanto italiano, ad appartenere a una nazione che, dati alla mano, controlla il 3,6% degli scambi commerciali nel mondo. Ha saputo solo balbettare la frase degli imbecilli quando capiscono di essere imbecilli: “Sì, ma è diverso”.
Certo che è diverso. L’imbecille, in qualunque percentuale, non conterà mai niente. :-)

<link>Lucio Bragagnolo</link>lux@mac.com