---
title: "Soddisfazioni superficiali"
date: 2010-02-08
draft: false
tags: ["ping"]
---

Quando <a href="http://attivissimo.blogspot.com/2010/01/aiped-aipad-aipod.html#c1121688604847449795" target="_blank">ho fatto notare</a> a Paolo Attivissimo <cite>Il tuo commento</cite> [su iPad] <cite>sembrava un copia e incolla di una stroncatura di iPhone nel 2007. Si è visto come è andata.</cite> lui <a href="http://attivissimo.blogspot.com/2010/01/aiped-aipad-aipod.html#c4430648526246144924" target="_blank">mi ha risposto</a> <cite>Continuo a non capire perché il successo commerciale di un prodotto sia citato come metro assoluto di soddisfazione del cliente.</cite>

Non ho scritto da nessuna parte <i>metro assoluto</i> e non mi riferivo tanto alla soddisfazione del cliente, quanto al fatto che evidentemente certe critiche un po' facili non hanno retto alla prova del tempo.

Già che ci siamo, però, certamente il successo iniziale di un prodotto non è indice di soddisfazione del cliente. Il cliente potrebbe essersi sbagliato; il prodotto potrebbe rivelarsi pessimo con l'andare del tempo; un sacco di gente azzecca qualcosa che poi non sa fare crescere.

Ma il tempo passa. Quando vedi che la gente, comprato il prodotto, non lo cambia tre mesi dopo oppure sei mesi dopo vengono dei sospetti; forse si sono trovati bene. Se di quel prodotto se ne vende di meno con l'andare del tempo vuol dire che era un <i>bluff</i>. Se invece le vendite continuano a crescere, significa che c'è anche un passaparola positivo. Significa che più gente si lascia convincere. Nel caso di iPhone tutto si può dire tranne che sia mancata la concorrenza. Si cita spesso il marketing di Apple, certo formidabile. A gioco lungo però non c'è marketing che tenga.

Tutti insoddisfatti quelli che hanno comprato iPhone e contemporaneamente sempre più iPhone venduti? Come è possibile che un apparecchio preso da gente poi insoddisfatta faccia il due percento del venduto e il quaranta percento del traffico web? Come è possibile che un apparecchio deludente generi picchi di traffico web nel fine settimana, proprio quando chi è costretto a usarlo controvoglia potrebbe buttarlo in un cassetto fino a luned&#236; mattina? Tutti illusi dalla magia di Steve Jobs, gli acquirenti di iPhone? Se è cos&#236;, però, sono insoddisfatti senza saperlo.

Numerose inchieste sulla <i>customer satisfaction</i> hanno dato ragione a iPhone. Non le cito perché non mi piacciono, le trovo semplicistiche e tendenziose (in generale). Per avere certe risposte basta fare la domanda nel modo giusto e accumulare un campione di risposte realmente indicativo è estremamente difficile. A maggior ragione eviterei di fare discorsi sulla soddisfazione di quaranta milioni di persone (per Paolo sono nove tra l'altro, ma voglio pensare sia stato un <i>lapsus</i>).

Tutto questo mi pare esagerato. E superficiale.