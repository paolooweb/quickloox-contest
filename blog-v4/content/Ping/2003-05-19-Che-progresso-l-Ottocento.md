---
title: "Che progresso, l’Ottocento"
date: 2003-05-19
draft: false
tags: ["ping"]
---

Per capire il concetto di velocità ci vuole FireWire 2

Ho visto un hard disk FireWire 800 (quella nuova, con velocità doppia della precedente) in funzione.

Fa spavento da quanto è veloce. Filmati, backup pesanti, archivi di posta di decenni... è sempre questione di una manciata di minuti.

Non c’è veramente confronto con il prima.

Siamo nel Duemila ma, se questo è l’Ottocento, lo raccomando caldamente.

<link>Lucio Bragagnolo</link>lux@mac.com