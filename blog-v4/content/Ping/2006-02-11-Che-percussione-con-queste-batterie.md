---
title: "Che percussione, &rsquo;ste batterie"
date: 2006-02-11
draft: false
tags: ["ping"]
---

Non sopporto le discussioni su quanto ti dura la batteria, e quanto spazio hai sul disco rigido, e quanto tira la tua Adsl, e tutte queste cose che per met&agrave; fanno competizione inutile e per l&rsquo;altra met&agrave; fanno ansia altrettanto inutile. Le dimensioni contano, ma in altri ambiti (per esempio, il sollevamento pesi). Ci sono guru dell&rsquo;open source che realizzano miracoli su iBook modello base usati e abusati al punto che ci vergogneremmo di passarne uno alla suocera.

Tuttavia c&rsquo;&egrave; chi ha bisogno della sua dose quotidiana di rassicurazione, o frustrazione, dipende, e quindi non mi sottraggo. <a href="http://www.coconut-flavour.com/coconutbattery/" target="_blank">Coconut Battery</a> mi informa che, dopo tredici mesi di vita, ho eseguito 123 cicli di carica della batteria del portatile e che la batteria suddetta conserva l&rsquo;84 percento della capacit&agrave; iniziale.

Come fare per allungare la vita della batteria? Onestamente, se sapessi come allungare la vita di qualcosa, mi darei da fare sui miei affetti e sui miei amici. La vita di una batteria si pu&ograve; solo accorciare.