---
title: "Di quanto bolle in pentola&#8230;"
date: 2006-08-30
draft: false
tags: ["ping"]
---

&#8230;per Leopard, il sito Apple dà ovviamente solo le informazioni di copertina. Mentre ero in vacanza <a href="http://www.accomazzi.net" target="_blank">Akko</a> mi ha segnalato (e ringrazio di cuore scusandomi per il ritardo) <a href="http://trac.macosforge.org/projects/calendarserver/wiki/CalendarClients" target="_blank">questa pagina</a>, dove possiamo sapere che dentro Leopard Server ci sarà anche un server wiki.

Niente male. Come si vede, <a href="http://www.macosforge.org/" target="_blank">Mac OS Forge</a> inizia a divenire realmente interessante.