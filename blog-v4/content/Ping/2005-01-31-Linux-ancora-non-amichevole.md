---
title: "Linux test fallito<p>"
date: 2005-01-31
draft: false
tags: ["ping"]
---

Il mio sistema personale conferma i risultati già ottenuti<p>

Quando mi dicono che ormai Linux è amichevole come Mac, ho un mio test infallibile.<p>

Tempo fa ho partizionato un disco FireWire esterno e ho tenuto una partizione apposta per esperimenti vari. Non ho ancora trovato una distribuzione Linux che si installi su quella partizione senza dare problemi e che parta, alternativamente al mio Mac OS X, in modo semplice.<p>

Stavolta ci ho provato con una Debian <em>for human beings</em>, <a href="http://www.ubuntulinux.org">Ubuntu</a>.<p>

Anche lui preferisce le partizioni nell&rsquo;ordine che vuole lui e, nonostante ci fosse esattamente quello che voleva per installare il dual boot, non riusciva a farlo. Non c&rsquo;era nessun meccanismo perché il sistema Linux venisse riconosciuto da Open Firmware senza andare a pasticciare manualmente in Open Firmware e poi varie altre cose.<p>

Prendere un computer da zero e metterci su Linux sembra effettivamente piuttosto facile, ma il mio disco di Mac OS X lo installo sempre e comunque. Linux ancora no.<p>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>