---
title: "Open Source, e Fast"
date: 2007-08-20
draft: false
tags: ["ping"]
---

Si era incastrato <a href="http://www.finkproject.org" target="_blank">Fink</a> e secondo me era un bug. Ho lasciato una segnalazione sul sito. Quattro giorni e hanno sistemato tutto.

Era un <em>bug</em>. Se avessi lasciato un feedback ad Apple, avrei potuto aspettare l'inverno.