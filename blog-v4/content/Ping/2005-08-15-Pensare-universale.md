---
title: "Aumenta chi pensa universale<p>"
date: 2005-08-15
draft: false
tags: ["ping"]
---

Altra buona notizia in vista, un giorno lontano, dei Mac con processore Intel<p>

Anche <a href="http://www.codingmonkeys.de/subethaedit/">SubEthaEdit</a>, con la versione 2.2, si aggiunge all&rsquo;elenco dei programmi che gireranno su qualunque Mac di oggi o di domani, con un file Universal Binary indipendente dal processore.<p>

Visto che c&rsquo;è ancora un anno di tempo prima che si vedano i primi Mac della nuova era, c&rsquo;è da chiedersi se sia vero quanto sostengono alcuni, che avere il software per i nuovi sistemi sia un problema.<p>

Forse il problema è solo pensare ristretto, invece che universale.<p>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>