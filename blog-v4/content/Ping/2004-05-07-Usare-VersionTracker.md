---
title: "Mi sento unico"
date: 2004-05-07
draft: false
tags: ["ping"]
---

E preferirei essere uno dei tanti

Sarà che sono a Webbit e l’umore è buono, sarà che chatto sempre con tutti, ma trovo incredibile come, quando si parli di programmi, invariabilmente mi venga chiesto se conosco un programma che fa la cosa di cui si sta parlando.

Quando me lo chiedono, io vado su <link>VersionTracker</link>http://www.versiontracker.com, guardo e rispondo. Ci vogliono trenta secondi ed è facilissimo.

VersionTracker esiste da anni, viene nominato almeno una volta alla settimana in tutte le mailing list, è convenzionato con il servizio .mac di Apple e viene citato spesso e volentieri anche sulle riviste Mac, Macworld in prima fila. Difficile che uno non sappia che esista.

Per questo mi chiedo: possibile che io sia l’unico a usare VersionTracker?

Non è questione di fare il prezioso o lo scocciato. È che io ho molti interessi, ma non infiniti. Non so tutto, sbaglio spesso e a volte posso dare un consiglio sensato, a volte invece proprio no.

Quindi che tu impari a usare VersionTracker non è fare un favore a me, ma a te.

<link>Lucio Bragagnolo</link>lux@mac.com