---
title: "Binario vivo<p>"
date: 2005-07-18
draft: false
tags: ["ping"]
---

Sensazioni positive per gli Universal Binary<p>

Apple ha una strategia di transizione del software da PowerPc a Intel tutto sommato ragionevole. A patto che gli sviluppatori la seguano.<p>

C&rsquo;è un anno di tempo prima che il problema diventi concreto e la sensazione è positiva. In giro per la rete ho trovato molto più materiale di quanto ho modo di trattarne da qualunque punto di vista e la gamma delle espressioni va dal ragionevolmente fattibile al già fatto.<p>

I casi peggiori sono quelli di progetti dove effettivamente c&rsquo;è lavoro fastidioso da fare, ma il tempo a disposizione è più che sufficiente per uscirne.<p>

Naturalmente chi non intende creare file Universal Binary non si mette ad annunciarlo in rete e quindi non ha senso fare statistiche. Ma l&rsquo;impressione è che almeno chi si è posto il problema lo abbia trovato risolvibile. Ed è bene.<p>


<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>