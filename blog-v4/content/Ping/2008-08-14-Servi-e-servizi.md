---
title: "Servi e servizi"
date: 2008-08-14
draft: false
tags: ["ping"]
---

La Rai, per le Olimpiadi di Pechino, offre la <a href="http://www.pechino2008.rai.it/dl/RaiSport/PublishingBlock-b4e0c005-45c3-4562-870c-9cff574c184d.html" target="_blank">visione su Internet</a> in ben due modi: con tecnologia proprietaria Microsoft (Windows Media Player) o con tecnologia proprietaria Microsoft (Silverlight).

La Bbc <a href="http://www.bbc.co.uk/blogs/bbcinternet/2008/08/open_industry_standards_for_au.html" target="_blank">scrive sul proprio <em>blog</em></a> che <cite>è arrivato il momento di iniziare ad adottare standard aperti come H.264 e Aac per i nostri servizi audio e video su web</cite>.

Non sono solo chiacchiere; Bbc ha già lanciato gli <em>streaming</em> per iPhone e iPod touch in H.264 e Aac e già <a href="http://www.bbc.co.uk/blogs/bbcinternet/anthony_rose/" target="_blank">da questa settimana</a> gli inglesi possono scegliere se usare il vecchio <em>codec</em> o quello nuovo con H.264. Successivamente verrà riconosciuto automaticamente il <em>bitrate</em> più adatto e tutti i computer il cui hardware è all'altezza riceveranno <em>stream</em> H.264 e Aac (di qualità superiore a quelli erogati finora).

Dove è chiarissima la differenza tra chi fornisce un servizio e chi serve il padrone.