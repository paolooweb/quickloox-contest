---
title: "Tempus fugit"
date: 2008-12-23
draft: false
tags: ["ping"]
---

Ultime ore per commentare qui sotto settecento caratteri sul Mac che hai amato di più, o <a href="mailto:lux@mac.com?subject=Settecento%20caratteri%20sul%20mio%20Mac%20preferito">per mandarmi le settecento battute via posta</a>.

Io passo alla redazione, che deciderà insindacabilmente chi e che cosa pubblicare. :-)