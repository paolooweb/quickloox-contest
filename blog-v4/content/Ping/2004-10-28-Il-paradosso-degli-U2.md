---
title: "Il paradosso degli U2<p>"
date: 2004-10-28
draft: false
tags: ["ping"]
---

Gli scherzi che causa il destino sono niente, in confronto a quelli della tecnologia<p>

Finalmente Apple ha aperto l&rsquo;iTunes Music Store in tutta Europa, con un evento che ha coinvolto nientepopodimeno gli U2.<p>

Chissà che cosa avrà detto Bono nello scoprire che iTunes Music Store è attivo in tutta l&rsquo;area dell&rsquo;euro e naturalmente nel Regno Unito, ma <em>non</em> in Irlanda.<p>

La bizzarria pare chiarita. Lo Store di san Patrizio è rimasto indietro all&rsquo;ultimissimo momento a causa di un problema tecnico, ora risolto, e dovrebbe regolarmente aprire entro qualche giorno.<p>

Se non altro, per una volta non è l&rsquo;Italia quella che arriva sempre per ultima.<p>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>