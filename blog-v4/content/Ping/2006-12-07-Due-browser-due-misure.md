---
title: "Due browser, due misure"
date: 2006-12-07
draft: false
tags: ["ping"]
---

Mano a mano che aumenta il mio ricorso a Google (ora ho svariati account su Gmail e alcuni lavori in corso procedono in collaborazione con altre persone via Google Docs), la limitazione a un solo account aperto di Gmail, ogni tanto, si fa sentire.

Però Google Docs su Safari funziona maluccio, dove invece va a meraviglia con Firefox. E Gmail applica il controllo a livello di singolo browser.

Cos&#236; lavoro con Safari e Firefox aperti insieme. Firefox tiene l'account su cui al momento si lavora con Google Docs e Safari un altro. E vado a meraviglia.

Peccato che <code>lynx</code> non abbia il supporto dell'https. :-)