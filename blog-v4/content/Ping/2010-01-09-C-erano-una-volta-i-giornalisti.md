---
title: "C'erano una volta i giornalisti"
date: 2010-01-09
draft: false
tags: ["ping"]
---

Apple ha iniziato a fornire le <a href="http://www.apple.com/itunes/charts/songs/" target="_blank">anteprime audio da trenta secondi di iTunes</a> anche via <i>browser</i>.

Provo a scaricare una delle anteprime. È solo un po' macchinoso: scelgo una pagina di anteprime, guardo nel codice il link del file di anteprima, lo infilo in una pagina web creata da me al momento e, caricata in Safari la mia pagina con il link, dico a Safari di scaricare sul disco il file linkato.

Apro il file di anteprima con QuickTime e guardo nell'inspector (Comando-I). Si vede che l'anteprima è un file Aac da 256 kilobit.

<a href="http://www.dailytech.com/article.aspx?newsid=17343" target="_blank">DailyTech</a>, <a href="http://www.9to5mac.com/itunes_gets_the_browser_lala_20282?utm_source=feedburner&amp;utm_medium=feed&amp;utm_campaign=Feed%3A+9To5Mac-MacAllDay+%289+to+5+Mac+-+Apple+Intelligence%29" target="_blank">9to5Mac</a>, <a href="http://distorted-loop.com/2010/01/07/apple-enables-itunes-web-browser-previews/" target="_blank">Distorted-Loop</a>, <a href="http://www.macrumors.com/2010/01/07/apple-tweaks-itunes-star-ratings-adds-30-second-song-samples-to-browser-based-itunes-preview/" target="_blank">MacRumors</a>, <a href="http://gigaom.com/2010/01/06/apple-itunes-web-preview/" target="_blank">Gigaom</a> e numerosi altri scrivono invece che le anteprime sono codificate a <cite>300+ kilobit</cite>. Se fosse vero, sarebbe una notizia; significherebbe che l'anteprima ha qualità migliore del brano acquistato e che Apple imbroglia il cliente potenziale.

Invece è falso. Ma viene scritto ugualmente e non si capisce perché.

Gli orecchianti italiani, per non essere da meno, hanno scopiazzato che l'anteprima audio nel <i>browser</i> ha a che vedere con l'acquisto lo scorso dicembre da parte Apple di Lala, un servizio di streaming musicale. Sordi alla conoscenza, ignorano che l'anteprima audio di iTunes la può fare chiunque, in qualsiasi momento, con conoscenze tecniche minime. Per esempio c'è una estensione del sistema di pubblicazione Joomla, <a href="http://www.n-tunes.com/" target="_blank">n-Tunes</a>, che fa esattamente questo.

Figuriamoci se Apple doveva comprare un'azienda per ottenere un risultato alla portata di software libero che chiunque di noi può scaricare in mezz'ora.

Una volta c'erano i giornalisti, persone che si documentavano sui fatti e li spiegavano. Poi sono arrivati i mitomani.