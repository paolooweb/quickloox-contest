---
title: "Le (buone) azioni di Steve<p>"
date: 2005-12-23
draft: false
tags: ["ping"]
---

Secondo qualcuno è pagato troppo. La realtà è diversa<p>

Molti sedicenti esperti hanno criticato i compensi presi da Steve Jobs come amministratore delegato di Apple, in quanto esageratamente alti. Ovviamente si parla comunque di cifre che chiunque di noi si sogna, ma qui l&rsquo;esagerazione si riferisce alla media dei compensi di gente impegnata in ruoli equivalenti al suo.<p>

Il 19 marzo 2006 Steve entrerà in possesso di dieci milioni di azioni restricted, che ha accettato in cambio di 55 milioni di azioni sotto forma di più classiche stock options. Il punto di pareggio, che segnava un vantaggio per Jobs oppure per Apple, era a 22,43 dollari per azione. Sotto questo valore, ci guadagnava Jobs. Sopra, ci guadagnava Apple.<p>

Mentre scrivo, le azioni Apple sono a 72,11 dollari. Ci ha guadagnato Apple, e di un bel po&rsquo;. Se Jobs avesse conservato i suoi 55 milioni di stock options, oggi potrebbe incassare quasi tre miliardi di dollari. I dieci milioni di azioni che ha accettato in cambio valgono invece 721 milioni di dollari. Sempre una bella cifra, ma neanche un quarto di quello che poteva essere.<p>

Jobs è amministratore delegato di Apple da otto anni e cinque mesi e, da allora, non ha venduto neanche un&rsquo;azione Apple, né ha esercitato alcuna stock option. Ha ricevuto da Apple un aereo personale da 90 milioni di dollari, ma è un aereo. Può venderlo, ma fino a che non lo fa non sono soldi concreti. Fino a questo momento, negli ultimi otto anni Apple ha versato denaro liquido nelle tasche di Steve Jobs per un totale di otto dollari.<p>

Si può dire tutto di Steve Jobs. Ma chi lo ha supposto avido di denaro ha letto male, o ha scritto male.<p>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>