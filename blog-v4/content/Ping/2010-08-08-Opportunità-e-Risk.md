---
title: "Opportunità e Risk"
date: 2010-08-08
draft: false
tags: ["ping"]
---

Ennesimo segnale del successo di App Store: Electronic Arts ha portato su iPod Touch, iPhone e iPad Risk, il gioco ufficiale, da cui prende le mosse - con una importante variante di gioco tesa a eliminare i problemi di copyright &#8211; l'italico Risiko.

Non che mancasse la scelta: a mia conoscenza sono da tempo disponibili via iTunes come minimo Lux Dlx 2 (anche in versione gratuita), Strategery e Dominion. Il gioco &#8220;vero&#8221; però è una tappa importante.

Da tenere presente che la variante di cui sopra incoraggia fortemente il gioco d'attacco. Gli attendisti, in Risk, devono essere anche attentisti, e guardarsi bene da ogni lato.

Si gioca splendidamente a Risk anche su Mac: ci sono l'eccellente <a href="http://sillysoft.net/games/macosx/" target="_blank">Lux Delux</a> di Sillysoft e l'ottimo &#8211; pure gratuito &#8211; <a href="http://yura.net/" target="_blank">Domination</a>.

In Editrice Giochi, depositaria del <a href="http://www.risiko.it" target="_blank">Risiko</a> italiano, sono ancora al decennio scorso. Quanto meno non c'è più bisogno di Windows per giocare e basta un browser, ma a pagamento e con una procedura più lunga di quella per avere il passaporto. Consiglio di darsi al regolamento americano, che oltretutto contiene le partite in tempi più che ragionevoli.