---
title: "Piccolo schermo grande feticcio<p>"
date: 2005-12-04
draft: false
tags: ["ping"]
---

Sarà che ormai ho un&rsquo;età, ma le risoluzioni contano<p>

Lancio una modesta provocazione, che però mi sorge prepotente dall&rsquo;animo e dal polpastrello.<p>

Gli iPod. Che non si chiamano iPod video, ma solo iPod e basta.<p>

Posso arrivare a capire il riversamento su iPod dei filmati personali. Quelli che amano atterrire gli amici e i parenti con ore di noiosissimo girato dell&rsquo;ultima vacanza a Malindi o la macroripresa del parto della cognata hanno a disposizione, con i nuovi iPod, un&rsquo;arma devastante.<p>

Posso vagamente intuire il perché uno voglia mettersi su iPod il Dvd del film che gli è piaciuto tanto, un po&rsquo; per backup perché è meno costoso che masterizzare un Dvd (sicuro? Fatto due conti? Io ho qualche dubbio) un po&rsquo; per impressionare i colleghi in ufficio, un po&rsquo; qui, un po&rsquo; là.<p>

Però ho visto, per la prima volta, temo non l&rsquo;ultima, una persona stare a guardare un film sullo schermo dell&rsquo;iPod. Sarà che ho un&rsquo;età, sarà che sono un bastian contrario, sarà che ho una vista pressoché perfetta e posso godermi uno schermo grande, ma a me sembra un&rsquo;aberrazione.<p>

Ultimamente mi è capitato di guardare una quindicina di minuti de <em>Il Ritorno del Re</em> sul PowerBook 17&rdquo;. Per me è il minimo sindacale di dimensione per la visione di un film. Almeno di un film che va anche visto.<p>

Sullo schermo dell&rsquo;iPod potrei guardarne a malapena quindici secondi. In qualsiasi altro caso non sto guardando il film, ma prendendo a pretesto quel pollice di diagonale per rivivere l&rsquo;emozione. Bello, intendiamoci; ma fa tanto Proust e <em>madeleinette</em>, alla ricerca del tempo perduto.<p>

Sono vecchio e arretrato io, oppure il film visto sullo schermo di iPod è un feticcio? Il dibattito è aperto.<p>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>