---
title: "Finezze di Pages<p>"
date: 2005-08-14
draft: false
tags: ["ping"]
---

Il programma Apple più sottovalutato degli ultimi anni<p>

Lascio la parola a <strong>Luca</strong> che dice esattamente le cose giuste:<p>

<cite>In Apri recenti, se hai aperto due file diversi ma con lo stesso nome, Pages ti fa vedere, solo accanto a questi, anche la cartella d'origine, per diversificarli. Sono consapevole che è una finezza, ma se sommi insieme tutte le finezze di Pages c'è da farsi venir voglia di accarezzarlo.</cite><p>

<cite>Lo uso da quando è uscito, e l'unico (vero) neo che finora gli ho trovato è nella non fedeltà di formattazione nelle esportazioni .doc, cronica ad esempio nei punti elenco e nel respiro delle celle. Quanto al resto sono contentissimo, e non è poco, considerando che di programmi simili ne ho provati mille.</cite><p>

Datemi la compatibilità AppleScript e Pages sarà una vera gemma. Forse il programma più sottovalutato di Apple dai tempi di HyperCard.<p>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>