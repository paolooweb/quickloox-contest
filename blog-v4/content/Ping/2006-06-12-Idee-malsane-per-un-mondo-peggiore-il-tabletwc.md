---
title: "Idee malsane per un mondo peggiore: il TabletWc"
date: 2006-06-12
draft: false
tags: ["ping"]
---

Saluto con piacere <strong>Peppe</strong> per riconoscenza, dopo che mi ha posto la domanda retorica <cite>Che fine ha fatto quell'<a href="http://www.microsoft.com/windowsxp/umpc/default.mspx" target="_blank">UMPC Microsoftcoso</a> cos&igrave; tanto esaltato dal marketing e divenuto un silenzioso flop in meno di sei mesi?</cite> e ha anche suggerito il titolo del post.

Il marketing Microsoft &egrave; potentissimo e d&agrave; l&rsquo;idea di un colosso infallibile e invicinbile. Non &egrave; cos&igrave;. Le idee che hanno sono sempre di seconda mano e quando devono fare concorrenza leale sono sempre a disagio.

