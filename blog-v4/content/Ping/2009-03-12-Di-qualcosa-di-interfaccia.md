---
title: "Di qualcosa di interfaccia"
date: 2009-03-12
draft: false
tags: ["ping"]
---

La prima: i tasti funzione dei MacBook Pro <em>unibody</em> che controllano il volume di iTunes anche se iTunes non è in primo piano hanno veramente il loro perché.

La seconda: consiglio vivamente questa <a href="http://blog.cocoia.com/2009/01/07/iwork-ilife-09-ui-roundup/" target="_blank">disamina delle icone adottate da Apple in iLife e iWork '09</a>. È in inglese, ma anche guardando le figure e basta è interessante. Viene fuori che Apple sta seguendo due stili diversi su sistema e applicazioni e che certe cose sono coerenti, altre migliorabili.

La terza: devo ricredermi sul mio approccio a Time Machine. Ho sempre considerato i tempi del backup una variabile inutile, dato che tanto se ne occupa il Mac e non devo pensarci io.

Time Machine su FireWire 800, invece, diventa davvero trasparente, mentre prima me ne accorgevo. Credo sia anche questione della maggiore velocità di tutto il parco hardware. Di fatto, prima facevo per staccare il disco e Time Machine spesso era in funzione. Lo si può interrompere, però, povero, finivo sempre per aspettarlo. Ora non succede assolutamente più; comunque vada, i miei spazi di non attenzione gli sono sempre sufficienti per fare il backup.

O sarà che prima il backup avveniva solo su cartelle selezionate e adesso invece su tutto il disco, per cui deve ragionare meno? Cercherò di farci caso.