---
title: "Il mio piccolo e sottile matrimonio Mac"
date: 2003-07-22
draft: false
tags: ["ping"]
---

Ovvero, come la tecnologia portatile facilita il lavoro dei deejay on the road

Il dovere lo ha già fatto Macdevcenter di O’Reilly Network, questo sì un sito che andrebbe consultato periodicamente. Il piacere è quello di godersi una festa in musica dovunque, con il solo aiuto di un buon Mac portatile e di un iPod.

L’ambientazione dell’<link>articolo</link>http://www.macdevcenter.com/pub/a/mac/2003/04/18/tibook_dj.html è quella del matrimonio ma mi viene da pensare a feste in spiaggia o prossime alla spiaggia. Sia di buon auspicio per le (buone) vacanze di ciascuno.

<link>Lucio Bragagnolo</link>lux@mac.com
