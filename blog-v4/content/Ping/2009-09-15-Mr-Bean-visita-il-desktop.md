---
title: "Mr. Bean visita il desktop"
date: 2009-09-15
draft: false
tags: ["ping"]
---

Da oggi, TextEdit viene sostituito ufficialmente da Bean come mio produttore semplice di testo formattato.

Sto anche pensando di usare Bean anche per pensionare <a href="http://www.tex-edit.com/" target="_blank">Tex-Edit Plus</a>, che uso sempre meno, il cui sviluppo ristagna ed è uno dei pochissimi programmi che mi richiede Rosetta per funzionare sul MacBook Pro. Sono determinato a eliminare l’uso di Rosetta entro fine anno.

<a href="http://www.bean-osx.com/" target="_blank">Bean</a> è cresciuto molto da quando era partito, restando però semplice e compatto, con tocchi interessanti che attendevo, come il riconoscimento delle espressioni regolari. Più volte l’ho però accantonato per risultati inferiori alle aspettative.

Vediamo se questa volta passa l’esame. Gli grava sopra un pregiudizio per me importante: non parla con AppleScript.