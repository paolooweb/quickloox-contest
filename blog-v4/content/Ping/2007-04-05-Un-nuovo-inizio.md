---
title: "Un nuovo inizio"
date: 2007-04-05
draft: false
tags: ["ping"]
---

Nella mia ultima visita alla redazione di Macworld ho perorato con forza la ricerca di una soluzione al problema dello spam che invadeva i commenti.

Ero abituato a vedermi pressoché ignorato dal <em>team</em> tecnico. Stavolta, da un giorno a quello dopo, è cambiato il mondo.

Non solo i commenti-spam sono scomparsi, ma mi sono ritrovato sotto i polpastrelli una versione di WordPress aggiornatissima e luccicante, che devo ancora finire di scoprire in tutte le sue novità e nuove comodità. intanto mi sembra di essere passato da un tronco rotolante a uno <em>skateboard</em>.

Persino la compatibilità con Safari adesso è tutto un altro paio di maniche.

Complimenti agli autori di WordPress (mi era scappato il livello reale raggiunto dal software) e complimenti a redazione e tecnici di Macworld. Bloggare mi è ridiventato un piacere.