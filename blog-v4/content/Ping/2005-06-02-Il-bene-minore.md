---
title: "Il bene minore<p>"
date: 2005-06-02
draft: false
tags: ["ping"]
---

Scoprire le scorciatoie di Panther grazie all&rsquo;adozione di Tiger<p>

Dopo avere installato Tiger noti subito che qualcosa è cambiato, per esempio dall&rsquo;icona di Spotlight, che cambia la vita.<p>

Andando avanti a usarlo scopri che è cambiato tutto e a un certo punto ti metti persino a cercare cose che non avresti mai pensato. Se è cambiato tutto è molto probabile scoprire qualcosa.<p>

Uso molto spesso Comando-Tab per passare rapidamente, in avanti, tra un programma e l&rsquo;altro. Il comando per muoversi indietro è Comando-Maiuscolo-Tab, che è piuttosto scomodo.<p>

Ho scoperto che per tornare indietro è molto più comodo Comando-minore (il simbolo <). Scorciatoia che però esisteva già in Panther, e chissà, magari anche in Jaguar.<p>

Se Tiger non mi avesse risvegliato la curiosità, però, non so quando me ne sarei accorto.<p>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>