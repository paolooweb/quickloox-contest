---
title: "Se questo è un upgrade"
date: 2003-11-27
draft: false
tags: ["ping"]
---

C’è ancora qualcuno che pensa a una pantera come a un giaguaro cresciutello

Panther è stato per Apple un grosso successo, come nuova versione di Mac OS X. Ed è una nuova versione, non un “semplice upgrade” come pretendono tuttora alcuni poveri di spirito, o se vogliamo furbetti che speravano di non doverlo pagare. Da oltre dieci anni non è mai successo che una nuova versione di Mac OS non fosse a pagamento pieno, con la sola eccezione del passaggio da Mac OS X Public Beta a Mac OS X.

Tanto per capire le differenze tra Panther e Jaguar, comunque, invito a compulsare i seguenti link:

http://www.apple.com/macosx/newfeatures/
http://www.apple.com/macosx/newfeatures/creativeprofessionals.html
http://www.apple.com/macosx/newfeatures/systemadministrators.html
http://www.apple.com/macosx/newfeatures/opensourceandunix.html

Attendo opinioni.

<link>Lucio Bragagnolo</link>lux@mac.com