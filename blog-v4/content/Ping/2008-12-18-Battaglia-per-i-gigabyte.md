---
title: "Battaglia per i gigabyte"
date: 2008-12-18
draft: false
tags: ["ping"]
---

Il primo tentativo di <a href="http://www.macworld.it/blogs/ping/?p=2090" target="_blank">copiare un file di oltre quattro gigabyte su iDisk</a> è stato piuttosto deludente. Anche il secondo.

Il Finder di Leopard si è rifiutato di copiare, adducendo un errore zero. Cos&#236; ho fatto ricorso al comando <code>cp</code> del Terminale. La copia è partita regolarmente e me ne sono dimenticato. Al mattino. La sera, sempre dimenticato, ho messo meccanicamente in stop il portatile prima di andare a dormire. La copia ovviamente si è chiusa e l'inchiesta ha determinato rapidamente che la colpa era da attribuirsi a errore umano.

La mattina del giorno seguente ho riavviato la copia dal Terminale e ho lasciato il portatile aperto per tre giorni e tre notti. Il quarto giorno era luned&#236; mattina e la copia era ancora in corso. A malincuore, dovendo recarmi a Milano con il portatile, ho interrotto il comando.

Una indagine successiva ha rivelato che ben presto, in realtà, qualcosa aveva iniziato a non funzionare. La Console era inondata da errori 400 relativi alla connessione WebDav che fa funzionare iDisk. Per ora l'errore ha ancora radici sconosciute.

Unico fatto che forse può interessare qualcuno: al termine della vicenda, dentro iDisk c'era un file corrispondente a quello che intendevo copiare, sebbene di dimensione zero. In compenso iDisk segnalava una occupazione dello spazio disco come se le due copie fossero andate a buon fine, otto gigabyte e passa in meno.

La situazione si è normalizzata qualche ora dopo.

Non mi sono arreso però, troppi errori non ancora investigati e poi sospetto che in realtà, a parte la pesantezza del file e la lunghezza della copia, non sussista un problema a priori. Cos&#236; ora il file è sull'iMac di backup, quello con Tiger. Ho avviato la copia da Finder ed è partita regolarmente. Dopo una giornata sta ancora procedendo. Saprò dire.