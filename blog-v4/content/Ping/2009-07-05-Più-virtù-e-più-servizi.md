---
title: "Più virtù e più (ser)vizi"
date: 2009-07-05
draft: false
tags: ["ping"]
---

L'avevo già detto che Apple ha una pagina che racconta tutte <a href="http://www.apple.com/it/macosx/refinements/enhancements-refinements.html" target="_blank">le modifiche apportate a Snow Leopard</a> e che non la traduce? S&#236;, ma oggi è sempre un altro giorno. E ogni giorno ecco una voce tradotta, senza costi aggiuntivi per Apple.

<b>Menu Servizi riprogettato</b>

Il menu Servizi è stato razionalizzato e ora mostra solo i servizi rilavanti all'applicazione o ai contenuti che si stanno usando. Il menu si può personalizzare e anche arricchire, con nuovi servizi creati via Automator.

Ora c'è un'interfaccia e quindi, paradossalmente, c'è qualcosa da capire prima di sistemarsi su misura i servizi. Prima non c'era da capire niente, a patto di sapere in che cartella cacciare i nuovi servizi. E il menu diventava una lista infinita di cose che solitamente non servivano, mentre con Snow Leopard migliora in efficacia ed efficienza. Punto a favore.