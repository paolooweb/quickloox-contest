---
title: "Tè ai fiori di sambuco"
date: 2009-08-21
draft: false
tags: ["ping"]
---

Nervi a fior di pelle presso un sito italiano che Macworld mi chiede cortesemente di non nominare.

Oggi un esagitato titola <b>Su Apple Store Italia conferma: Snow Leopard il 28 agosto</b>.

Sempre oggi, uno stressato titola <b>Snow Leopard: nuovi indizi puntano al lancio del 28 agosto</b>. E straparla di <cite>fonti consultate e giudicate dal nostro sito attendibili</cite> (leggi: copia e incolla dai siti americani senza neanche capire bene).

L'esagitato pubblica un'immagine, come se nel mondo digitale fosse una garanzia (a fabbricare una immagine cos&#236; bastano tre minuti) e poi mette un link.

<a href="http://store.apple.com/it/product/MC204T/A?afid=p204%7C403275" target="_blank">Lo visito</a> e leggo: settembre. Guardo sulla <a href="http://www.apple.com/it/macosx/" target="_blank">pagina ufficiale Apple</a> e leggo: settembre.

Esagitati e stressati sono velocissimi a inventare; per la verità hanno assai meno tempo.

Certamente si deve a caldo e mancanza di sonno.

Tè ai fiori di sambuco, funziona benissimo. Oltretutto rilassa i nervi. I pellirosse mangiavano cipolle crude e preparavano infusi di teste di papavero. Le donne inglesi usano da sempre l'acqua dei fiori di arancio, spremendo il succo di due arance in acqua calda con un pizzico di miele.

Se riusciamo a farli dormire un po' di più, scrivono meno cretinate. È nel loro interesse.