---
title: "Suonala ancora, shell"
date: 2006-05-25
draft: false
tags: ["ping"]
---

Per qualche motivo non esiste un comando del Terminale che riproduca un suono, cos&igrave; qualcuno ha pensato di farne <a href="http://steike.com/PlaySound" target="_blank">uno</a>.

In realt&agrave; ho detto una mezza bugia. Su Mac OS X si pu&ograve; passare dal comando <code>osascript</code> per farlo via AppleScript, e il Terminale dispone di un suo comando <code>say</code> ma il pretesto &egrave; valido per raccomandare un&rsquo;occhiatina al sito, pieno di piccole cose interessanti e curiose.