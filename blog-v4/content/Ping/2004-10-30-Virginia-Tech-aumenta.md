---
title: "Tante piccole potenti mele<p>"
date: 2004-10-30
draft: false
tags: ["ping"]
---

Per Virginia Tech non è mai abbastanza<p>

<em>System X</em> non è software di sistema operativo, bensì il supercomputer dell&rsquo;americana Virginia Tech University, formato da 1.100 Power Macintosh G5, capaci di erogare fino a 10,28 teraflop (un teraflop vale mille miliardi di operazioni su numeri in virgola mobile. Sì, la potenza di un computer non è solo il clock del processore).<p>

Non contenta, Virginia Tech è stata ben lieta di sostituire tutti i Power Mac G5 che poteva con più potenti ed efficienti Xserve, e ha anche aggiunto altri 50 nodi (server interni al sistema) alla configurazione.<p>

Risultato: ora i teraflop sono 12,25.<p>

Non ho spazio per dettagliare tutti gli usi che si fanno di supercomputer come questi. Si sappia solo che, se le previsioni del tempo sono più precise che cinquant&rsquo;anni fa, o se vengono risolti teoremi di alta matematica che una volta venivano considerati impossibili, può essere che un contributo decisivo lo abbiano dato tante piccole, potenti mele che lavorano insieme.<p>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>