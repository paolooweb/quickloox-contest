---
title: "Landa desolata<p>"
date: 2005-08-31
draft: false
tags: ["ping"]
---

Chi crede nel potere della chiacchiera non ne legge abbastanza<p>

In questo periodo mi sto prendendo il lusso di leggere quello che si dice in giro, per abusare l’ennesima volta della formula. Normalmente partecipo a numerose discussioni, un po’ perché mi appassiono, un po’ perché sono cocciuto, un po’ perché mi piace. Ma il miglior messaggio, in pieno stile Zen, è quello che non viene scritto, e per un po’ voglio digiunare. Per purificarmi. Scherzo.<p>

Dicevo, leggo e basta. Nel giro di quarantott’ore ho letto, dalle fonti più disparate, che a) Microsoft ha presentato un certo brevetto mesi prima che Apple mettesse sul mercato iPod. Falso; b) che Firefox è un browser open source e Safari invece no. <a href="http://webkit.opendarwin.org/">Falso</a>; c) che Microsoft possiede ancora azioni Apple. Falso; d) che Apple dovrebbe <a href="http://www.pbs.org/cringely/pulpit/pulpit20050825.html">regalare Tiger</a> nel momento in cui farà uscire Leopard. Discutibile, ma tendenzialmente stupido; e) che Tiscali Fax da mesi non funziona. Non è per fare spot a Tiscali, ma in agosto l’ho usato per fare un favore a un amico, che aveva bisogno di un fax, e ha funzionato perfettamente.<p>

Al termine (quello canonico almeno) delle vacanze il panorama informatico è quello di una landa desolata, dove il sole picchia durissimo ed evidentemente si è riallargato il buco nell’ozono. Per quanto mi riguarda, vado avanti a leggere, e basta. All’ombra.<p>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>