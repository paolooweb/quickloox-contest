---
title: "Stimolo ad apparire"
date: 2006-05-26
draft: false
tags: ["ping"]
---

Mentre constato sulla mia pelle che faccio videochat solo quando ho effettivamente voglia di montare la iSight sul PowerBook, per molti titolari di MacBook e MacBook Pro, che ce l&rsquo;hanno incorporata, effettivamente diventa uno strumento abituale.