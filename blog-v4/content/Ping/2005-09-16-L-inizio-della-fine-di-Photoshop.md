---
title: "L&rsquo;inizio della fine (di Photoshop)<p>"
date: 2005-09-16
draft: false
tags: ["ping"]
---

Tecnologia libera più libera iniziativa danno grandi risultati<p>

Saranno ancora tanti. Ma i giorni di Photoshop sono contati.<p>

Non ci credi? Prova per un quarto d&rsquo;ora <a href="http://www.belightsoft.com/products/imagetricks/overview.php">Image Tricks</a>. Nessun problema, il programma è gratuito.<p>

Image Tricks esiste grazie alla disponibilità di Core Image: le routine di manipolazione grafica dentro Mac OS X, che Apple ha reso aperte a qualunque programmatore le voglia usare.<p>

Ogni tanto salta su qualcuno a spiegare che Apple dovrebbe fare un programma concorrente di Photoshop. Sbagliato. Apple deve creare – e crea – le tecnologie di base. E poi che nascano mille programmi, e che vinca il migliore.<p>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>