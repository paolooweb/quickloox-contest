---
title: "Sergio e chi ci crede<p>"
date: 2005-06-19
draft: false
tags: ["ping"]
---

Storia di una iniziativa e di chi crede in quello che dice<p>

Sergio ha avuto - e mi ha comunicato - una grande idea: fare una colletta e ordinare un Power Macintosh sperimentale con processore Intel, quelli che Apple noleggia agli svilupppatori fino a fine 2006 per collaudare il loro codice e favorire la Transizione.<p>

Avere anche solo una macchina disponibile significa molto, pure oltre i confini geografici, perché un Mac può essere utilizzato anche remotamente. 999 dollari, per un piccolo sviluppatore, possono essere una cifra consistente.<p>

Lui era disposto a metterci cento euro. A me l&rsquo;idea piaceva e ho impegnato cento euro. Purtroppo, sui canali contattati, non c&rsquo;è stato seguito.<p>

Allora ho parlato dell&rsquo;idea di Sergio ai miei soci di <a href="http://www.macatwork.net">Mac@Work</a>. L&rsquo;hanno trovata buona e hanno già ordinato la macchina, a disposizione di tutti gli sviluppatori impegnati nella transizione che troveranno comodo approfittarne.<p>

Come minimo, in Italia ci sarà una postazione in più per gli sviluppatori (non credo ce ne siano tantissime altre). L&rsquo;idea di Sergio ha portato a fatti concreti e a una situazione migliore di quella che c&rsquo;era prima.<p>

Altre idee, invece, lasciano il deserto che trovano. A me piace più quella di Sergio.<p>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>