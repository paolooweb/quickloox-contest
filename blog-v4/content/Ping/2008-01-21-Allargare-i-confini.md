---
title: "Allargare i confini"
date: 2008-01-21
draft: false
tags: ["ping"]
---

Mi scuso con tutti quelli che non sono riusciti a entrare nella stanza di chat keynote2008 in occasione del keynote suddetto.

Sembra che una stanza sui server Aim non possa contenere più di 35 partecipanti e, colpevolmente, non lo sapevo.

Il prossimo anno si fa comunque su iChat, ma mi sdoppierò anche su Irc. A mia conoscenza, in un canale Irc si sta più larghi.

Di solito uso <a href="http://xchataqua.sourceforge.net/" target="_blank">X-Chat Aqua</a>, ma di programmi abili allo scopo ce ne sono numerosi.

Se serve, sono disponibile a sessioni di prova per familiarizzare con il nuovo ambiente. :-)