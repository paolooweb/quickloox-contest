---
title: "Aiutiamoli"
date: 2007-06-16
draft: false
tags: ["ping"]
---

Qualcuno non riusciva a segnalare non so che bug su Safari a causa del crash continuo della beta. Non deve essere il solo, dato che Surfin' Safari ha dedicato un post ai <a href="http://webkit.org/blog/107/safari-beta-301-for-windows/" target="_blank">sistemi per segnalare bug</a> senza dover usare per forza Safari.

Se qualcuno pensa che segnalare bug sia nell'interesse di Apple, ha perfettamente ragione. Deve solo aggiungere qualunque programmatore nel mondo che potrebbe avere voglia di prendere WebKit e costruire un browser in più a nostra disposizione.