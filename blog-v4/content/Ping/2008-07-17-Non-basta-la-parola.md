---
title: "Non basta la parola"
date: 2008-07-17
draft: false
tags: ["ping"]
---

Vengo spernacchiato perché spiego che iPhone è un computer, non un cellulare. Oltretutto, se un prodotto contiene Phone nel nome, è un telefono, giusto?

Poi <strong>Iruben</strong> mi segnala questo <a href="http://www.corriere.it/scienze_e_tecnologie/08_luglio_16/telefonino_freerunner_e7674206-533b-11dd-a364-00144f02aabc.shtml" target="_blank">pezzo del Corriere</a>. A parte il tono vagamente surreale e la consueta mancanza di connessione stabile con la realtà, leggo <cite>Come nel caso del dispositivo di casa Apple, non si tratta di un semplice cellulare, ma di un vero e proprio computer portatile con cui telefonare e navigare in piena libertà</cite>. Ma guarda.

Se i-Phone è per forza un telefono, che cosa sarà per forza <a href="http://www.openmoko.com/product.html#" target="_blank">Free-Runner</a>? Una scarpa da <em>jogging</em>?

Mentre scrivo questo Ping è arrivato in posta <strong>Nicola</strong> a segnalare l'articolo sul Corriere. E non solo: di tutto quello che si può dire di interessante su iPhone, ecco che cosa si riduce a fare <a href="http://www.repubblica.it/2008/07/sezioni/scienza_e_tecnologia/iphone-3g/iphone-numeri/iphone-numeri.html" target="_blank">Repubblica</a>.