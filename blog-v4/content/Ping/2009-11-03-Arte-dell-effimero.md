---
title: "Arte dell'effimero"
date: 2009-11-03
draft: false
tags: ["ping"]
---

Diversamente dal classico esercizio di stile grafico del programmatore che mette alla prova qualche algoritmo, gli <a href="http://7art-screensavers.com/Mac_OS_X.shtml" target="_blank">orologi-salvaschermo di 7art</a> bordeggiano verso l'ambizione artistica.

Sono tanti, spesso gratuiti e come da tradizione qui non si ignora mai un orologio un po' speciale.