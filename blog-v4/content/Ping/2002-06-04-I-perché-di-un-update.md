---
title: "I perché di un update"
date: 2002-06-04
draft: false
tags: ["ping"]
---

Una manciata di professionisti è particolarmente contenta di Mac OS 10.1.4

Un update è sempre una cosa buona. Anche Mac OS X 10.1.4, che nella breve storia del nuovo sistema operativo Apple è l’aggiornamento più piccolo che sia uscito finora.
Tuttavia, insisme ai consueti bug fix, comprende anche un cambiamento che riguarda pochi ma significa molto, come accade in genere quando ci sono di mezzo i soldi.
Mac OS X 10.1.4 introduce infatti la compatibilità con il Dvr-S201 Pioneer: un masterizzatore di Dvd capace di lavorare sui Dvd cosiddetti authoring, usati per la produzione di Dvd su vasta scala. L’<link>unità Pioneer</link>http://www.pioneerelectronics.com/Pioneer/CDA/Industrial/IndustrialProductDetails/0,1444,797,00.html costa quei 4.500 dollarucci e riguarda pochi; ma quei pochi - obbligatoriamente in possesso di Dvd Studio Pro - possono considerare Mac OS X in un ambito professionale importante. A noi comuni mortali Apple ha già pensato da un pezzo, con il SuperDrive. È giusto che ce ne sia per tutti, no?

<link>Lucio Bragagnolo</link>lux@mac.com