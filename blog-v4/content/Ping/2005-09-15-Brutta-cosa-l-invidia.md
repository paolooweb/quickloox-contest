---
title: "Imitatori che non fanno ridere<p>"
date: 2005-09-15
draft: false
tags: ["ping"]
---

E c&rsquo;è persino qualcuno che dà loro soldi<p>

Sono anni che esiste Konfabulator e mesi che esiste Dashboard. È prossimo l&rsquo;avvento di una grande novità: i <a href="http://microsoftgadgets.com/">Microsoft Gadgets</a>.<p>

Brutta cosa l&rsquo;invidia. Specie quando sei cronicamente in ritardo.<p>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>