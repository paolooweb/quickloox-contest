---
title: "Un mouse da fine del mondo<p>"
date: 2005-07-27
draft: false
tags: ["ping"]
---

Il primo con tracciamento laser<p>

Andrea mi segnala quello che a suo parere è il miglior mouse disponibile in assoluto: il <a href="http://www.logitech.com/index.cfm/products/details/IT/IT,CRID=2135,CONTENTID=9043">Laser Cordless Mouse Mx1000</a> di Logitech.<p>

Non l&rsquo;ho provato personalmente, ma di Andrea mi fido ciecamente, soprattutto quando si parla di ergonomia e apparecchi di input.<p>

Stando al sito Logitech, è il primo mouse al mondo con tecnologia laser. Dovrebbe essere molto meglio dei mouse ottici e renderli obsoleti. O almeno non più trendy.<p>

Se qualcuno lo acquista sono molto interessato a sentirne le impressioni d&rsquo;uso.<p>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>