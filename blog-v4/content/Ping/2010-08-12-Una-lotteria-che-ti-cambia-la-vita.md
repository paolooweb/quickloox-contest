---
title: "Una lotteria che ti cambia la vita"
date: 2010-08-12
draft: false
tags: ["ping"]
---

Shannon Rosa, mamma qualunque di Redwood City, ha vinto un iPad alla lotteria organizzata per raccogliere fondi dalla scuola dei suoi figli.

Ha messo iPad in mano al piccolo Leo, con gravi problemi di autismo, senza troppe aspettative, dal momento che il bimbo non aveva combinato gran che con un iPod touch.

Lo schermo più grande invece ha facilitato le cose e ora Leo <a href="http://www.sfweekly.com/2010-08-11/news/ihelp-for-autism/1/" target="_blank">sta ricevendo un grande aiuto da iPad</a>. Anche sua mamma, che ha uno strumento in più per mantenere gestibili le intemperanze di un bambino autistico.

Gli iPad venduti potrebbero essere quattro o cinque milioni. E sebbene siano tutti uguali, qualcuno vale più degli altri. Non per via del 3G.