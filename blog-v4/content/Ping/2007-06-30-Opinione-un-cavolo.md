---
title: "Opinione un cavolo"
date: 2007-06-30
draft: false
tags: ["ping"]
---

A me è accaduto milioni di volte. So, nell'intimo, che cosa è meglio e dove sta la ragione. Ma sono piccolo e ignorante, dunque non so con precisione <em>perché</em>.

Un esempio per tutti, le infinite discussioni su Mac OS (X) e Windows. Dopo quasi un quarto di secolo, sono sicurissimo, ma non sempre so esattamente perché. Lo sento in modo soverchiante e basta.

Tanti anni sentivo che lo Spectrum 48k era superiore al Commodore 64. Grazie (mille) a Paolo di <a href="http://www.gamestar.it/blogs/roots/" target="_blank">Back to the Roots</a> e al per me sconosciuto (ma voglio erigergli un monumento) <a href="http://www.alfonsomartone.itb.it/rqftcg.html" target="_blank">Alfredo Martone</a>, adesso so esattamente perché. E sogno di mettere in piedi una competizione equivalente tra Mac OS X e Windows.

So già come andrebbe a finire, anche se non sempre perché.

E se qualcuno scrolla le spalle dicendo che sono tutte opinioni, primo si legga tutta la pagina di Martone, e magari anche la pagina dei commenti. Poi gli spezzo le braccine.