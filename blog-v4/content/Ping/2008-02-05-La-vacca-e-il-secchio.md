---
title: "La vacca e il secchio"
date: 2008-02-05
draft: false
tags: ["ping"]
---

Il grande allenatore di basket Aza Nikolic parlava di certi giocatori in questi termini:

<cite>Come vacca che fa buon latte e poi dà calcio a secchio.</cite>

Abbiamo un grande giocatore: Danny Goodman, l'autore dei migliori libri su AppleScript mai visti.

Abbiamo tanto buon latte: HyperPort, applicazione che permette di esportare dati presenti in <em>stack</em> di HyperCard, cos&#236; da poter usare i dati stessi in altre applicazioni e soprattutto in Leopard, che ha eliminato il supporto delle applicazioni Classic (come HyperCard). HyperPort, all'inizio prodotto commerciale, adesso <a href="http://dannyg.com/archive/hyperport.html" target="_blank">diventa gratis</a>.

HyperPort però è uno stack di HyperCard. Va usato <em>prima</em> di aggiornare. Cos&#236; sono buoni tutti.

Da parte di un mago di AppleScript, è un calcio al secchio.