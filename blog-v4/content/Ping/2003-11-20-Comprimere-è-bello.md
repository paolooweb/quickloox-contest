---
title: "Comprimere è bello"
date: 2003-11-20
draft: false
tags: ["ping"]
---

Come avere i file più compressi possibile

Un file compresso occupa meno spazio su disco, si trasferisce più velocemente attraverso la rete e magari può essere cifrato, quindi ridotto e pure confidenziale.

Stabilito che comprimere è bello, che programma usare?

Beh, la compressione migliore in assoluto la ottiene il formato StuffIt X (quello con estensione .sitx). La migliore alternativa possibile è bzip2, da Terminale.

La tabella di verifica più completa e attendibile che conosco sta su <link>Compression.ca</link>http://compression.ca/mac/act-canterbury.html.

Già che siamo qui: chi ha comprato StuffIt Deluxe 8 faccia attenzione, che ci sono problemi seri di scompattamento dei file compressi con StuffIt 5.

<link>Lucio Bragagnolo</link>lux@mac.com