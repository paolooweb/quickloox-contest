---
title: "Soldi da buttare?"
date: 2009-12-21
draft: false
tags: ["ping"]
---

Sta nascendo <a href="http://download.ooo4kids.org/it" target="_blank">OOo4Kids.org</a>, una versione di OpenOffice pensata per bambini dai sette ai dodici anni, gratuita e multilingue.

C'è già una versione scaricabile e il progetto cerca finanziamenti. Invece che pronunciare scemenze tipo <cite>faccio usare Word a mio figlio perché tra dieci anni ci dovrà lavorare</cite>, se proprio bisogna buttare soldi dalla finestra ecco un bersaglio molto più nobile e intelligente.

E non sono nemmeno più soldi buttati.