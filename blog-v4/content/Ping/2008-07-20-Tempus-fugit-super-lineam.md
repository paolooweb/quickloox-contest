---
title: "Tempus fugit super lineam"
date: 2008-07-20
draft: false
tags: ["ping"]
---

Visto che oramai su Ping <a href="http://www.macworld.it/blogs/ping/?p=1832" target="_blank">si alimenta</a> anche la collezione di orologi, è&#8230; tempo di aggiungere <a href="http://www.johnhaney.com/timeslider/" target="_blank">TimeSlider</a>.