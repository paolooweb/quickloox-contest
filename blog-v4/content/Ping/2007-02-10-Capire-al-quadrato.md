---
title: "Capire al quadrato"
date: 2007-02-10
draft: false
tags: ["ping"]
---

Grazie a <strong>Pierfausto</strong> per avermi fatto conoscere <a href="http://mac4windiots.blogspot.com/" target="_blank">Mac4Windiots</a>. Il sottotitolo del blog dice tutto: come si possa capire e, dopo avere capito, si possa anche vedere ancora più in là.

Per tutti quelli che non hanno ancora capito, neanche il primo giro. :-)