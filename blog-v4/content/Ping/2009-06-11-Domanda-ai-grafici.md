---
title: "Domanda ai grafici"
date: 2009-06-11
draft: false
tags: ["ping"]
---

Non intendo riaprire la diatriba tra schermi lucidi e schermi opachi. Invece ho un dubbio.

I nuovi schermi, retroilluminati a Led invece che a lampada fluorescente, sono sicuramente illuminati in modo più uniforme. Inoltre Apple sostiene che il <em>gamut</em>, la gamma di colori rappresentabile, è cresciuta del 60 percento.

Questi elementi compensano in parte o in qualche modo la scelta dello schermo lucido? O chi è per lo schermo opaco ritiene che ci sia solo perdita?

Non esperto, pendo dalle labbra di chi ne sa. :-)