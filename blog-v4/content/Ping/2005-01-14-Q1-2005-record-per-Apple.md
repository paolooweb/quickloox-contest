---
title: "Il trimestre dei record<p>"
date: 2005-01-14
draft: false
tags: ["ping"]
---

Buone notizie per quanti temono che Apple possa sparire<p>

John Dvorak poco tempo fa ha nuovamente sostenuto che Apple &ldquo;è condannata&rdquo;. Poveraccio.<p>

Comunque ci sono sempre quelli che temono Apple possa scomparire dalla sera alla mattina, come se fosse un&rsquo;aziendina di due persone dentro la cantina di casa Jobs.<p>

Siccome è l&rsquo;ignoranza che genera la paura, si sappia che il trimestre natalizio 2004 ha segnato il record assoluto nella storia di Apple per fatturato e profitti. Le vendite di Macintosh hanno totalizzato meno del 50 percento del fatturato (non succedeva da quindici anni) e questo vuol dire che l&rsquo;azienda non sta dipendendo unicamente dai suoi computer. Contemporaneamente le vendite e il fatturato di Mac sono però in forte aumento e a momenti facevano il record anche loro.<p>

Apple è qui per restare. E fare belle cose per tutti noi che amiamo la tecnologia dal volto umano.<p>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>