---
title: "Non è l'ora di Norah"
date: 2009-01-20
draft: false
tags: ["ping"]
---

Bizzarro e non ho ancora modo di verificare, ma parrebbe che Learn To Play in GarageBand '09 <a href="http://www.9to5mac.com/Garage-band-intel-only" target="_blank">funzioni solo su Mac con processore Intel</a>, nonostante la specifica generale di iLife '09 copra anche PowerPc.

Se è cos&#236;, ancora per un po' non potrò palleggiarmi Sting o Norah Jones. Nel frattempo mi arrangio crudamente con <a href="http://www.teoria.com" target="_blank">Teoria</a>.