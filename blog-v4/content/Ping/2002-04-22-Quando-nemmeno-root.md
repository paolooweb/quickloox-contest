---
title: "Quando nemmeno root"
date: 2002-04-22
draft: false
tags: ["ping"]
---

Come liberarsi di un problema insolubile (e di un file incancellabile)

La struttura di permessi e privilegi di cui è permeato il cuore Unix di Mac OS X fa sì che a volte un’azione sia possibile solo all’utente root, una vera e propria divinità locale a cui tutto è concesso.
Ma capitano a volte file che nessuno, neanche root, riesce a cancellare. Prova a duplicare la cartella di AppleWorks 6 e a buttarla nel Cestino: è probabile che un certo file resista ostinatamente e con lui la cartella che lo contiene.
Il segreto è noto a pochi: molto probabilmente quel file è marcato - per un errore o per una svista - con un flag che si chiama uchg: in gergo Unix, immutable (non c’è bisogno di tradurre).
La soluzione: usare il comando <b>chflags</b> da Terminale, dopo avere impersonato root, e impostare il parametro nouchg per quel file.
Il comando <b>man chflags</b> nel Terminale, per una volta, chiarirà tutto.

<link>Lucio Bragagnolo</link>lux@mac.com