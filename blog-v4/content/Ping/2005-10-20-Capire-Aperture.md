---
title: "Capire Aperture<p>"
date: 2005-10-20
draft: false
tags: ["ping"]
---

In attesa che arrivi Apple, spiego il posizionamento del suo nuovo programma per professionisti<p>

Apple ha annunciato Aperture e già la gente si inventa paragoni assurdi con Photoshop. Come se fossero programmi concorrenti.<p>

Provo a spiegarlo in modo semplice (anche se non lo sarà mai abbastanza):<p>

iDvd&hellip; Dvd Studio Pro<br>
GarageBand&hellip; Soundtrack Pro<br>
iMovie&hellip; Final Cut Pro<br>
iPhoto&hellip; ???<p>

Oggi come oggi, chiunque di noi può fare doppio clic su una miniatura in iPhoto e lanciare Photoshop.<p>

Apple pensa che un fotografo professionista vorrebbe poter fare doppio clic da un programma di catalogazione professionale e lanciare Photoshop. iPhoto è un programma per tutti. <a href="http://www.apple.com/aperture">Aperture</a> è per chi ci deve lavorare seriamente. Any questions?<p>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>