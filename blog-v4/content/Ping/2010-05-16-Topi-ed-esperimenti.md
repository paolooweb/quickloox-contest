---
title: "Topi ed esperimenti"
date: 2010-05-16
draft: false
tags: ["ping"]
---

Ho appena cambiato le batterie al Magic Mouse.

Avevo segnato la data di prima accensione e posso dire che la durata delle batterie precedenti, un paio di Energizer, è stata di 141 giorni, quasi cinque mesi.

Ho voluto calcolare i giorni con Numbers e ho scoperto che la funzione DATEDIF usa come separatore il punto e virgola, mentre l'aiuto suggerisce erroneamente l'uso della virgola.

Pistolare su Numbers alla ricerca delle funzioni giuste è peraltro assai più divertente che contare i giorni di durata delle pile.