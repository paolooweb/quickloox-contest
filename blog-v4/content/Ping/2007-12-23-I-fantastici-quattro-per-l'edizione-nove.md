---
title: "I fantastici quattro per l'edizione nove"
date: 2007-12-23
draft: false
tags: ["ping"]
---

Ho finito di leggere <a href="http://www.fmws.it/?p=44" target="_blank">FileMaker Pro 9, La grande guida</a>, di Riccardo Albieri, Marco Consigliere, Vincenzo Gentile e Giulio Villani.

Non sono esperto né interessato a FileMaker: le cose più complicate che devo gestire stanno in un una schermata di BBEdit. Invece mi interessava vedere se il libro è fatto bene e se vale la pena di consigliarlo.

S&#236;, s&#236;. Il libro è scritto benissimo e altrettanto organizzato. Si vede che gli autori (animatori del notevole <a href="http://www.fmws.it/" target="_blank">FileMaker Workshop</a>) sono gente ordinata. E come non esserlo, da professionisti del database?

A mio giudizio il lettore ideale del libro è il 75 percento meno preparato dei potenziali utenti di FileMaker Pro. Si spiega molto bene come usare il programma per definire database relazionali e soprattutto come organizzare database relazionali nel mondo migliore.

Grazie al cielo le pagine di elencazione delle voci dei menu o dei comandi di scripting sono state validamente sostituite da altrettante pagine che spiegano come disporre i campi nelle tabelle, come integrare FileMaker con altre applicazioni e come realmente arrivare a diventare sviluppatori professionisti.

Probabilmente il quarto superiore degli utenti non sarà interessato al libro. Credo che sbagli, perché è facile fare il superiore e poi accorgersi che qualcuno ti sta spiegando chiaramente e comprensibilmente una cosa che avevi solamente intuito, e che una ripassata delle basi di uso di FileMaker aiuta a eliminare certi peccati di gioventù che uno poi si porta testardamente dietro fino a fine carriera.

Se si usa FileMaker, o lo si vuole usare, è un libro da acquistare. A meno che non si sia bravi abbastanza da insegnare agli autori e l'impressione è che questo sarà difficile.