---
title: "Quando aprile cadeva in novembre"
date: 2006-04-03
draft: false
tags: ["ping"]
---

Apple sta pianificando un nuovo sistema di distribuzione di contenuti da presentare al prossimo Macworld Expo insieme a un Mac mini con funzioni di media center. La nuova tecnologia di Apple distribuir&agrave; i contenuti in modo che di fatto non risiederanno sul disco rigido dell&rsquo;utente.

Questo era <a href="http://www.thinksecret.com/news/0511contentdist.html" target="_blank">ThinkSecret il 2 novembre 2005</a>. Bello leggerlo passata la sbornia del primo di aprile. :-)