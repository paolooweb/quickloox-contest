---
title: "Il palmare che non si dice"
date: 2002-04-02
draft: false
tags: ["ping"]
---

Ciò che non passa dalla porta a volte rientra dalla finestra

Un sacco di gente continua a inseguire il sogno di un palmare targato Apple.
Amico, Apple non produrrà un palmare. Per lo meno non ora, quando il mercato relativo non sembra esattamente una miniera d’oro.
Però, però, dato un’occhiata ultimamente a iPod? È piccolo, veloce, con una gran batteria, adesso anche dieci gigabyte di spazio. Sì, dice, ma serve per gli Mp3. Già, ma è anche un disco di backup; e ora memorizza indirizzi, se qualcuno è stato attento. Un giretto su <link>VersionTracker</link>http://www.versiontracker.com rivela programmi e programmini che iniziano a sfruttare iPod per attività molto più ampie dell’originale ascolto di musica.
E se alla fine <link>iPod</link>http://www.apple.com/it/ipod restasse ufficialmente un bel gadget ma, di funzione in funzione, di fatto diventasse un aggeggio altrettanto utile, senza usare mai quella parola?

<link>Lucio Bragagnolo</link>lux@mac.com