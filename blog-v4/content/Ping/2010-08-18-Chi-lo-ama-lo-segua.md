---
title: "Chi lo ama lo segua"
date: 2010-08-18
draft: false
tags: ["ping"]
---

Guy Kawasaki <a href="http://blog.guykawasaki.com/2010/08/how-to-get-a-free-copy-of-my-first-book-the-macintosh-way.html" target="_blank">elargisce gratis</a> una copia Pdf del leggendario libro <i>The Macintosh Way</i> a chi lo segua su Twitter.

Il libro, da solo, è una scusa sufficiente per mettersi di impegno e imparare quel po' di inglese ove scarseggi. La storia è fantastica e le cose da imparare sono innumerevoli.