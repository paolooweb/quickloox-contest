---
title: "Questione di ore"
date: 2009-12-21
draft: false
tags: ["ping"]
---

Come già in passato, se la mia nipotina sta leggendo, nell'imminenza dell'ora X può seguire il <a href="http://www.noradsanta.org/it/index.html" target="_blank">sito di tracciamento di Babbo Natale</a>, allestito dal Comando nordamericano di difesa aerospaziale.

Al papà (e a tutti i papà, e le mamme) consiglio l'articolo di Cnet in cui si narra <a href="http://news.cnet.com/8301-13772_3-10418101-52.html?part=rss&amp;subj=news&amp;tag=2547-1_3-0-20" target="_blank">l'inizio della tradizione</a>. Nel 1955, quando un'innocente campagna pubblicitaria pubblicò il numero per chiamare Babbo Natale. Solo che il numero venne stampato mancante di una cifra e portò decine di bimbi a impegnare la linea telefonica segreta che avrebbe dovuto segnalare un eventuale attacco sovietico.