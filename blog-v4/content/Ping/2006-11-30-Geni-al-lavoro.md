---
title: "Geni al lavoro"
date: 2006-11-30
draft: false
tags: ["ping"]
---

Sabato scorso la posta non partiva più. Non era la prima volta che succedeva e nel giro di qualche ora (colpa loro), o con un logout-login (colpa mia), la situazione si sistemava da sola.

Luned&#236; mattina la posta continuava a non partire. Prova e riprova, scopro che i geni di Tele2 hanno deciso di adottare l'Smtp autenticato, su una porta che devono avere estratto a sorte.

Stando al sito di Tele2, il cambiamento è avvenuto il 30 settembre. In realtà, fino a sabato scorso, ho spedito tranquillamente tutta la posta immaginabile.

Se avessi tenuto d'occhio il loro sito, avrei letto una sciocchezza. Non avendolo tenuto d'occhio, cambiano l'Smtp in modo complicato e nessuno mi avvisa. Io me la cavo, ma se fosse accaduto a mio fratello, che di Smtp non ne sa niente, si sarebbe attaccato al supporto tecnico, cosa assai molesta per il provider stesso. Morale: da qualunque parte la si guardi, non si fa cos&#236;.

A margine, ho dovuto rifare gli script che configurano Mailsmith per le varie postazioni di lavoro e, per paradosso, il bailamme causato da Tele2 mi ha semplificato molto le cose: adesso il mio script tipico è tipo

<code>tell application "Mailsmith"
	set use smtp authentication of every mail account to true
	set the smtp server port of every mail account to 587
	set smtp user of every mail account to "la mia id"
	set smtp password of every mail account to "la mia password"
	set the smtp server of every mail account to "smtp.tele2.it"
end tell</code>

Lo si fa una volta a mano mentre Script Editor registra e, per creare gli script relativi alle altre postazioni, basta cambiare i parametri.

Tele2 geni per dire, Mailsmith programma geniale, AppleScript sistema altrettanto geniale. Ci sono dentro anch'io. L'eccezione. :-)