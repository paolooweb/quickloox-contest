---
title: "L'anno dell'esperienza"
date: 2010-12-31
draft: false
tags: ["ping"]
---

Una <a href="http://news.cnet.com/8301-13924_3-20026877-64.html" target="_blank">favoletta</a> istruttiva per iniziare bene il nuovo anno.

<cite>Eviterò di nominare quello che penso sia il miglior prodotto dell'anno. Ma posso dire con certezza che iPad sia stato quello di maggior rottura. [&#8230;]</cite>

<cite>Ho sempre pensato che il mio MacBook Air fosse il mio portatile definitivo e che non avrei mai avuto bisogno di nient'altro. Sbagliato. Salta fuori che un</cite> laptop <cite>non è sempre la macchina migliore in viaggio o quando c'è una veloce esigenza di computer in casa. iPad mi ha mostrato che esiste qualcosa di meglio per un numero sorprendente di situazioni.</cite>

<cite>E quella apparentemente banale funzione di accensione all'istante, combinata con i sette etti di peso, fa un sacco di differenza.</cite>

<cite>In breve, quando passo dall'usare il portatile la maggior parte del tempo a usarlo la metà del tempo, è una discontinuità, un punto di rottura. E credo di non essere il solo. [&#8230;]</cite>

<cite>Possa il 2011 essere l'anno del</cite> tablet<cite>.</cite>

Aggiungo: che sia Air o iPad importa poco. Possa il 2011 essere l'anno dell'esperienza, in cui daremo giudizi di cose che abbiamo veramente vissuto. L'esperienza concreta potrebbe portarci sorprese.

Non è una favola. Neanche quella che ho citato.

Abbiamo un anno meraviglioso davanti. Usiamolo, stando alle mie competenze, per computare alla grande. Fuori dalle mie competenze, per vivere allo stesso modo. :-)