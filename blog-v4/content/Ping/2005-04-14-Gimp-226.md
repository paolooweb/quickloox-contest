---
title: "Chi ha bisogno di Photoshop?<p>"
date: 2005-04-14
draft: false
tags: ["ping"]
---

Di più: chi ha bisogno di sapere l&rsquo;inglese?<p>

Mi segnala l&rsquo;amico Mario che la nuova versione di <a href="http://www.gimp-app.sourceforge.net">Gimp</a>, la 2.2.6, ha anche il supporto della lingua italiana.<p>

Gimp, per ora, supporta solo X11, ma si installa in modo semplice, è una normalissima icona nella cartella Applicazioni e si lancia con un normalissimo doppio clic. Dopo di che fa quasi qualunque cosa faccia Photoshop.<p>

Per una persona normale Gimp è infinitamente più del necessario. Ed è gratis, se segui il link di questo pezzo. Ha ancora senso cercarsi un Photoshop rubato in giro per il p2p, se non per vantarsi da stupidi davanti a una platea di altri più stupidi?<p>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>