---
title: "Il meccanico da tavolino"
date: 2010-06-03
draft: false
tags: ["ping"]
---

L'ultimo giro di interventi su Unix e <i>scripting</i> ha riscosso una certa attenzione e può darsi che il mio amico Luca Misterakko Accomazzi abbia fatto la cosa giusta, con il fare uscire proprio in questo periodo <a href="http://www.accomazzi.net/TlibriI113.html" target="_blank">Mac OS X sotto il cofano</a>, per i tipi di Apogeo.

Akko ha evitato di scrivere quello che con serviva, cioè un monumentale terrificante manuale della shell, per produrre invece un libretto agile, semplice da leggere, che al posto di spiegare la creazione del mondo si concentra su cose semplici, raccontate in modo chiaro, facili da leggere, con abbondanza di esempi.

È il modo più ragionevole possibile per accostarsi al Terminale e a Unix per una persona normale e per imparare davvero qualcosa. Il qualcosa sono i mattoni fondamentali, dai quali è poi possibile sviluppare a piacimento ulteriore conoscenza, se interessa. Ma anche no e restare felicemente, se cos&#236; si crede, alle nozioni fondamentali del Terminale, che tornano utili in numerose circostanze.

Prima delle danze più tecniche c'è un bel primo capitolo, che riassume in poche pagine la storia di Unix in modo sorprendente e informato, partendo dagli sforzi di una pattuglia di raccomandati che volevano preservare i propri privilegi e, anziché anelare alla pensione d'oro, si sono ingegnati a gettare le basi di un sistema operativo.

Le danze tecniche coprono esattamente le prime cose che servono: le nozioni di base per muoversi nel Terminale, il collegamento a distanza con altri Mac (e non), i rapporti tra Terminale e Internet, la documentazione sui comandi eccetera eccetera.

Si parla delle dotazioni più importanti della parte Unix di Mac OS X, come Php, Python, Apache, Perl e altro; di come gestire i permessi su file e cartelle (una delle fonti maggiori di mal di testa, finalmente spiegata come si deve); dell'aiutarsi nel diagnosticare i problemi leggendo i log di sistema; di X11; di come progettare l'infrastruttura di un sito dinamico con il solo Terminale (la parte vale da sola l'intero libro) e via digitando.

Ho infine apprezzato l'attenzione dedicata alle espressioni regolari e ai meccanismi di <i>piping</i> con cui si esprime la vera potenza di Unix, che i mestieranti gabellano come conoscenza di comandi esoterici, mentre non è affatto cos&#236;: la mossa vincente è saper concatenare insieme i comandi, cosa che nessun elenco insegna e che qui, al contrario, è possibile scoprire.

Sul sito dell'autore, oltre alla scheda linkata più sopra, è possibile leggere <a href="http://www.accomazzi.net/copertina-libro/cofano-sommario-introduzione.pdf" target="_blank">sommario e introduzione</a>. Del prezzo non parlo neanche; a Milano copre a fatica una pausa pranzo al baretto e il libro contiene conoscenza e nozioni con i quali un neofita può imparare per mesi e mesi. Rapporto prestazioni/prezzo stellare e formato perfetto per stare a fianco di Mac, da aprire appena si apre il cofano e appare il motore Unix.

Akko è mio ottimo amico e qualcuno penserà che parli bene del suo libro per questo. Ne parlo bene perché è un buon libro, invece. Del resto <a href="http://www.apogeonline.com/libri/9788850329502/scheda" target="_blank">sul sito Apogeo</a> si possono scaricare due capitoli aggiuntivi, non presenti nell'edizione in libreria, e dovrebbe essere veramente più che abbastanza per giungere a una valutazione personale. Se il tema interessa minimamente e se si possiede una percezione vagamente adulta della nozione di valore (se ricordo bene Sky offre i Mondiali di calcio a sei volte il prezzo di questo libro), è un acquisto già fatto.