---
title: "L'elefante e il topolino"
date: 2009-02-07
draft: false
tags: ["ping"]
---

Apple, lato informatica, vende computer. Apple non ha mai avuto problemi con Linux. Preferisce vendere Mac su cui gira Mac OS X e, a questo scopo, investe risorse ingenti in Mac OS X. Se però andasse male e l'utente volesse Linux, quanto meno Apple avrebbe portato a casa la vendita del computer. Tant'è che uno potrebbe scaricarsi <a href="http://www.opensource.apple.com/" target="_blank">Darwin</a>, installarci sopra un'interfaccia grafica come <a href="http://kde.org/" target="_blank">Kde</a> e di fatto usare un sistema operativo <em>open source</em> fornito quasi interamente da Apple su qualunque macchina, anche non Apple.

Da quando è avvenuta la transizione ai processori Intel, anche Windows ha smesso di essere un problema. Si lavora sempre perché la gente capisca e usi Mac OS X, ma se proprio insiste nel farsi male l'importante è avere venduto almeno il computer. A oggi, lo stato di salute di Apple nel vendere computer è invidiabile.

Microsoft, lato informatica, vende software. Apple non è mai stata un problema, per Microsoft. A quella gente che si ostina a volere un sistema operativo migliore si può comunque vendere Office. Con la transizione ai processori Intel, per Microsoft si è semplicemente allargata la base potenziale di computer su cui si può installare Windows.

Per Microsoft, Linux costituisce invece un grosso problema. A uno che usa Linux Microsoft non può vendere niente, pena abbattere vari pilastri strategici su cui l'azienda si regge da vent'anni.

Microsoft è terrorizzata da Linux. Nell'analisi dei rischi aziendali depositata presso la Consob americana non si menziona la minaccia di un eventuale successo dei Mac, ma quella dei sistemi operativi <em>open source</em>, cioè Linux.

L'azienda ha varato una Windows Competitive Strategy e sta assemblando un gruppo di lavoro incaricato di combattere con tutti i mezzi i Linux da scrivania, come Ubuntu.

Il bello della situazione è che <a href="http://marketshare.hitslink.com/operating-system-market-share.aspx?qprid=8" target="_blank">Windows fa l'88 percento delle scrivanie, Linux lo 0,8</a>. L'elefante scalcia alla vista del topolino.