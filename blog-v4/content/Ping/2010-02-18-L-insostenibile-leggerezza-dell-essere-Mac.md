---
title: "L'insostenibile leggerezza dell'essere Mac"
date: 2010-02-18
draft: false
tags: ["ping"]
---

Seduto in treno, chiudo il MacBook Pro 17&#8221; per prepararmi a scendere e un signore mi chiede <cite>ma quanto pesa il suo computer, che è cos&#236; sottile?</cite>

<cite>Tre chili e qualcosa</cite>. Glielo faccio soppesare.

<cite>Più o meno come il mio</cite>, commenta, <cite>ma il mio è spesso il doppio&#8230; e mi devo portare dietro l'alimentatore&#8230;</cite>

In effetti sono in giro per appuntamenti, con lo <a href="http://www.builtny.com/laptops-backpacks/laptop-backpack-medium.html" target="_blank">zaino minimale</a> e il solo computer.

Il suo è un Toshiba, 16&#8221; o 17&#8221;, non capisco bene. Effettivamente lo spessore è doppio del mio. Il materiale è plastica a buon mercato. Deve essere spesso per forza.

La scena di vita vissuta termina qui, perché devo scendere. Considero che a tavolino chiunque mi critica il portatile perché gli manca qualcosa. Il senso comune vuole Blu-ray, due batterie, questa porta, quel collegamento, il processore del momento, il <i>gadget</i> che devono avere tutti eccetera.

Ma i portatili, lo dice la parola stessa, si portano. E la gente, in verità, vuole viaggiare leggera.