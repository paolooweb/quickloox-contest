---
title: "Non sono mica gli americani"
date: 2010-03-19
draft: false
tags: ["ping"]
---

Qualcuno ha ironizzato nei giorni scorsi sul fatto che io possa giudicare <i>avanti</i> gli americani.

Non è quanto sono avanti loro, ma quanto siamo indietro noi. Se gli americani danno fastidio, ecco il <a href="http://www.nordicgameprogram.org/" target="_blank">Nordic Game Program</a> danese:

<cite>La missione del Nordic Game Program è assicurare ai ragazzi e ai giovani accesso a giochi nordici di qualità.</cite>

Parafrasando, quando un ragazzo italiano pensa che potrebbe guadagnarsi da vivere scrivendo videogiochi deve imparare alla svelta l'arte di arrangiarsi.

Quando un ragazzo danese pensa la stessa cosa, ha il supporto (e anche i finanziamenti) delle istituzioni.

C'entra niente con il <a href="http://www.macworld.it/ping/nomi/2010/03/17/povera-patria/" target="_self">Colosseo a calligramma</a>? C'entra moltissimo. Modi di pensare.