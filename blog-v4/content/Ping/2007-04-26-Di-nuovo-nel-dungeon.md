---
title: "Di nuovo nel dungeon"
date: 2007-04-26
draft: false
tags: ["ping"]
---

Lontano da World of Warcraft causa minivacanza al mare, sono ripiombato nel tunnel di <a href="http://angband.rogueforge.net/" target="_blank">Angband</a>. Quasi per coincidenza, il gioco ha ritrovato un maintainer e si annunciano aggiornamenti.

Vediamo se al ritorno riesco a smettere, o se invaderà le pause-bagno. :-D