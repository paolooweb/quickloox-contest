---
title: "Se fare un sito diventa naturale"
date: 2006-07-18
draft: false
tags: ["ping"]
---

Andrea <em>von Kaja</em> ha la passione di <a href="http://www.travian.it/" target="_blank">Travian</a> e ha creato con iWeb <a href="http://web.mac.com/genesi/iWeb/FUNEDO" target="_blank">due</a> <a href="http://web.mac.com/genesi/iWeb/Genesi/HomePage.html" target="_blank">siti</a> che narrano delle sue avventure.

Fino a qui niente di che. Ma lui non è un web designer e non ha il tempo di mettersi seriamente a fare siti. È <a href="http://www.apple.com/it/ilife/iweb/" target="_blank">iWeb</a> che semplifica, facilita, velocizza.

Ecco perché chiedersi come il programma ottimizzi le immagini è certamente doveroso, ma manca il punto fondamentale. Che è il suo ottimizzare il tempo di chi vuole avere in fretta altre cose importanti a cui pensare.