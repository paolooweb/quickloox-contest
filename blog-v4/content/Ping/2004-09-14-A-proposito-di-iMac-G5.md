---
title: "Il G7 del 2010<p>"
date: 2004-09-14
draft: false
tags: ["ping"]
---

Tanto per rendersi conto di quanto corra la tecnologia<p>

Nel 1998 c&rsquo;era solo un iMac: il modello color blu Bondi, dal nome di una spiaggia australiana, con un G3 a 233 megahertz, 32 megabyte di Ram a 100 megahertz, un lettore Cd e un hard disk da quattro gigabyte, al prezzo originale di 1.299 dollari.<p>

Nel 2004 l&rsquo;iMac G5 modello base costa 1.299 dollari, ma dispone di un G5 a 1,6 gigahertz, hard disk da 80 gigabyte e un lettore Dvd/masterizzatore Cd, dalle velocità molto superiori.<p>

Viene voglia di chiedersi come sarà l&rsquo;iMac G7 del 2010.<p>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>