---
title: "Pensare in piccolo"
date: 2008-05-12
draft: false
tags: ["ping"]
---

In merito alla <a href="http://www.macworld.it/blogs/ping/?p=1773">velocità di Office vecchio</a>, Office nuovo, OpenOffice, NeoOffice e addentellati, c'è anche una questione di dotazione dei programmi.

Ho creato in Numbers la pagina dei turni delle pulizie del mio condominio. Una cosa bellissima (in realtà vistosa, cos&#236; i condòmini si sentono importanti). Numbers è lento e ha tutti gli strumenti per creare una pagina vistosissima. Non mi importava la lentezza, ma realizzare un lavoro eccezionale che nessun altro programma. Meglio: potrei fare lo stesso con OpenOffice e probabilmente anche con Office, ma con lentezza equivalente.

Dovevo anche fare quattro conti al volo su un <em>budget</em> e certi tempi di consegna. Niente di più complicato che addizioni moltiplicazioni e divisioni elementari, con nessuna esigenza grafica.

Ho lanciato <a href="http://www.x-tables.eu/more/overview.html" target="_blank">Tables</a>. Tre secondi ed ero operativo.

Se Office parte in tre minuti, ma devo gestire un impero, tre minuti vanno benissimo. Forse l'errore è dare tre minuti a Office per fare quattro conti da tovagliolo. (sto cercando di montare un foglio elettronico in&#8230; emacs, ma non sono ancora all'altezza)

Lo stesso vale in tutti gli altri campi. Per creare una normale paginetta di bell'aspetto con tutte le cosine giuste al loro posto, TextEdit basta, avanza ed è pure troppo. E parte in tre secondi.

Perché usare programmi grandi per compiti piccoli?