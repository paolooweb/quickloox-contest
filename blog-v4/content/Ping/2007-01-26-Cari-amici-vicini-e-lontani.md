---
title: "Cari amici vicini e lontani"
date: 2007-01-26
draft: false
tags: ["ping"]
---

Con la magia (in senso lato, anzi software) di <a href="http://homepage.mac.com/lxr/homepage/spaceants/hamachix/" target="_blank">HamachiX</a>, accendo la rete Airport e arrivano sia la notifica di Bonjour da parte dell'iMac di backup, sul mobile di fianco alla mia poltrona, sia quella del Mac di un amico a cinquanta chilometri di distanza. Mica paglia. :-)