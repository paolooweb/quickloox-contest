---
title: "OpenFirmware per neurochirurghi"
date: 2001-12-14
draft: false
tags: ["ping"]
---

Se proprio bisogna passare il tempo a pensare al computer

Amo Mac perché è un computer che ti permette di ignorarlo e concentrarti sulla mia attività, gioco o lavoro che sia, mentre gli altri computer mi obbligano a concentrarmi sul computer, altrimenti non funziona niente.
Neanche un Mac però è perfetto; a volte non funziona. A volte non funziona di brutto.
Quando il caso è ancora peggiore, bisogna scendere fino nei meandri di OpenFirmware; è più o meno l’equivalente di quando si fa un elettroencefalogramma.
Chi non sa che cos’è OpenFirmware, consiglio, faccia del suo meglio per continuare a non saperlo; chi lo sa, consulti questa guida. È il meglio in assoluto che abbia visto finora. Se proprio si va a curiosare nel cervello del Mac, almeno si è ben informati.

<http://www.imaclinux.net/gh.php?single=76+index=0>