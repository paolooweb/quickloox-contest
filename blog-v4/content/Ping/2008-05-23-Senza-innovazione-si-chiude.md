---
title: "Senza innovazione si chiude"
date: 2008-05-23
draft: false
tags: ["ping"]
---

La tecnologia <em>multitouch</em> non è che sia questa rivoluzione.

Dopotutto l'inquadramento teorico del tema data agli anni Settanta.

Il programmatore Christian Moore ha creato Lux, un <em>framework</em> (infrastruttura) di programmazione che permette di avere un <em>multitouch</em> economico, anche se non efficace come quello di Mac OS X, su qualsiasi computer, <a href="http://gizmodo.com/391103/full+screen-multitouch-mac-os-x-is-here-but-not-from-apple" target="_blank">anche un MacBook</a>. <a href="http://www.iphonealley.com/" target="_blank">iPhone Alley</a> ha pubblicizzato un filmato dove si crea una periferica di input multitouch con uno scatolone, un foglio di carta e una <em>webcam</em> di quelle che oramai anche nelle uova di Pasqua. Faccio l'esperimento e lo infilo qui:

<object width="425" height="355"><param name="movie" value="http://www.youtube.com/v/pQpr3W-YmcQ&#38;hl=en"></param><param name="wmode" value="transparent"></param><embed src="http://www.youtube.com/v/pQpr3W-YmcQ&#38;hl=en" type="application/x-shockwave-flash" wmode="transparent" width="425" height="355"></embed></object>

La comunità del software aperto, insomma, ha scoperto l'idea e il software si prepara a fioccare.

Se però non ci fosse stato iPhone di mezzo, a scompaginare le idee con sopra troppo calcare, nessuno ci avrebbe pensato. Il software aperto funziona quando gli innovatori indicano la strada.

In mezzo c'è Microsoft, che ha creato il suo Surface per pochi esclusivi clienti danarosi, invece che portare il <em>multitouch</em> nelle dita di milioni di persone. Non ha (mai) innovato e il suo software è il più chiuso sulla piazza.

Non innova e non è aperta: sta inutilmente in mezzo a succhiare soldi ai dipendenti da Office. <em>Aurea mediocritas</em>.

Grazie a <a href="http://tiphones.com" target="_blank">Stefano</a> per la dritta!