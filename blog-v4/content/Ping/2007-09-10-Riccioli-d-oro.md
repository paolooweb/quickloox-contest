---
title: "Riccioli d'oro"
date: 2007-09-10
draft: false
tags: ["ping"]
---

Ci sono arrivato tardi, ma grazie a <a href="http://multifinder.wordpress.com/" target="_blank">Mario</a> ho scoperto <a href="http://daringfireball.net/projects/smartypants/" target="_blank">SmartyPants</a> di John Gruber, quello di DaringFireball.

Serve a effettuare alcune sostituzioni di caratteri non Ascii nelle giuste entità Html senza creare problemi con quello che è contenuto nei tag (un esempio per tutti, le virgolette <em>curly</em>, quelle ricciolute, che possono interferire con la sintassi Html).

Perché? Perché <cite>proper typographic punctuation looks sharp</cite>. Una punteggiatura tipografica come si deve trasmette precisione.