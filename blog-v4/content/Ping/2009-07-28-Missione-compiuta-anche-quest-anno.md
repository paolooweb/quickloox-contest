---
title: "Missione compiuta anche quest'anno"
date: 2009-07-28
draft: false
tags: ["ping"]
---

Grazie a <b>Thom</b> per avere segnalato che Apple ha finalmente reso in italiano la <a href="http://www.apple.com/it/macosx/refinements/enhancements-refinements.html" target="_blank">pagina italiana delle novità di Snow Leopard</a>.

Non servo più (era successo lo stesso <a href="http://www.macworld.it/blogs/ping/?p=1537" target="_self">il 14 ottobre 2007</a>). E resisto eroicamente per pura prudenza alla forte tentazione di installare Snow Leopard direttamente sul disco di lavoro. Ognuno si farà la propria idea delle novità; io mi sono fatto quella dell'utilizzo e soffro a non averlo da subito. Non è assennato basare il sistema su una Developer Preview, per quanto finora solida come la roccia.