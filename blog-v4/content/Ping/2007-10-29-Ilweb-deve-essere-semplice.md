---
title: "i(l)Web deve essere semplice"
date: 2007-10-29
draft: false
tags: ["ping"]
---

Sento spesso critiche verso iWeb per le cose che non fa, oppure per le cose che fa in un modo diverso da quello più strettamente professionale.

Penso a quello che costa iWeb (un quinto di 79 euro) e mi chiedo che cosa vogliano i detrattori. A Milano, con un quinto di 79 euro, vai a mangiare la pizza e se prendi pure il caffè non ti lasciano uscire fino a che non hai tirato fuori quello che manca.

È un programma che permette di partire in modo semplice e senza porsi troppe domande, superando la difficoltà intrinseca dello strumento. E detta cos&#236; potrebbe essere un'apologia del Mac.

Poi rifletto sul lavoro di Dany, il cui blog è partito letteralmente da zero. Ora i suoi <a href="http://web.mac.com/brethil/Brethils_Page/Thoughts/Thoughts.html" target="_blank">Thoughts</a> sono bene impaginati ed eleganti. Ha qualcosa da dire e lo dice, per di più in (buon) inglese.

Sento di Andrea Ack, dove iWeb è servito per una applicazione (super)professionale.

Gusto i <a href="http://www.fabfood.net/FabFood/FabFood_Blog/FabFood_Blog.html" target="_blank">post alimentari</a> di Fab.

iWeb è pieno di difetti. Ma ti fa andare sul web, subito, senza pensarci.

Se vuoi essere professionale, beh, <a href="http://plone.org/" target="_blank">Plone</a> è l&#236;. Per non parlare di <a href="http://wordpress.org/" target="_blank">WordPress</a>. Perfino gratis. Ma ci devi pensare.