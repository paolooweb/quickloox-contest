---
title: "La luna e il dito"
date: 2004-07-15
draft: false
tags: ["ping"]
---

Il ragionamento di uno stolto fa capire tante cose

Un brillante utente non registrato di Punto Informatico ha constatato che emulare Mac OS X su un computer Wintel dà risultati penosi per la lentezza del tutto.

Una persona normale penserebbe che l’hardware del Pc non è all’altezza dell’emulazione che si vuole ottenere; lui, poveraccio, conclude che Mac OS X non è un buon software!

Quando il dito indica la luna, lo stolto guarda il dito. Qualcuno riesce anche a metterselo in un occhio.

<link>Lucio Bragagnolo</link>lux@mac.com
