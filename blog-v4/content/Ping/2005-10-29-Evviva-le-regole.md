---
title: "Evviva le regole<p>"
date: 2005-10-29
draft: false
tags: ["ping"]
---

Quelle che aiutano invece di ostacolare, chiaro<p>

Mi ha scritto un&rsquo;amica che ha chiesto l&rsquo;anonimato:<p>

<cite>ti stavo scrivendo per chiederti come fare ad aggiungere qualche font alle quattro di base di OpenOffice, poi ho ripensato alla regola del minuto.</cite><p>

<cite><a href="http://www.openoffice.org">Sito di OpenOffice</a>, da lì ai forum, ricerca con la parola fonts e poi, con fondu, è uscito <a href="http://trinity.neooffice.org/modules.php?name=Forums&file=viewtopic&t=1552&highlight=fondu">questo</a>. Aperto FinkCommander, scaricato fondu, seguite le istruzioni, vivo felice e contento con le mie font Mac OS. :-)</cite><p>

<cite>Però se lo racconti su Ping cambiami almeno il nome. :-P</cite><p>

Ho fatto ben più di questo, mia cara.<p>

Grazie comunque di cuore per avere ricordato la Regola: <em>prima di chiedere aiuto / studia lo schermo per un minuto</em>.<p>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>