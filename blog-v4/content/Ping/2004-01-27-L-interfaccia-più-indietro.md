---
title: "L’interfaccia più indietro"
date: 2004-01-27
draft: false
tags: ["ping"]
---

Per confrontare meglio Windows e Macintosh, a volte, tertium datur

Chattavo con un mio amico, Unixaro accanito, di interfacce grafiche. Così mi sono reso conto, per esempio, che Kde ha le trasparenze e le icone in formato Svg, quindi ridimensionabili a piacere senza perdere qualità come su Mac OS X.

Chatta tu che chatto io ci siamo resi conto di una cosa: c’è una sola interfaccia grafica che, rispetto allo stato dell’arete, è tremendamente indietro. Windows.

Le cose che sono su Mac OS X oggi (e su Kde, gratuito) Windows non le vedrà prima del 2005/2006.

Windows è indietro, gente. Scelta di retroguardia, di miseria, non quella del portafogli.

<link>Lucio Bragagnolo</link>lux@mac.com