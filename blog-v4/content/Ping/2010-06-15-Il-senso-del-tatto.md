---
title: "Il senso del tatto"
date: 2010-06-15
draft: false
tags: ["ping"]
---

Scettico: <cite>ma perché tutta questa mania di fare apparecchi che funzionano con le dita, quando abbiamo</cite> mouse<cite>,</cite> joystick<cite>,</cite> trackpad<cite>,</cite> trackball<cite>, interfaccia vocale, stilo&#8230;?</cite>

Risposta: <cite>perché sono apparecchi digitali, che si chiamano</cite> digitali <cite>perché hanno cominciato gli americani e</cite> digit <cite>significa</cite> cifra<cite>,</cite> numero <cite>in inglese. Ma</cite> digit <cite>viene da</cite> digitus<cite>, che in latino significa</cite> dito<cite>.</cite>

È cos&#236; che la storia della civiltà trova (un piccolo e specifico, ma importante) compimento e che il cerchio si chiude. Tracciato con un dito.