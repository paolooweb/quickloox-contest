---
title: "Nel regno del fattibile / 12"
date: 2010-07-24
draft: false
tags: ["ping"]
---

<cite>pregasi elencare azioni fattibili su un iPad e non fattibili su un Sony Vaio W da 10&#8221;</cite>.

Usarlo come <a href="http://airturn.com/press-releases/ipad-bluetooth-page-turner-footswitch-prototype-from-airturn-inc" target="_blank">spartito musicale</a>.

Uno dei problemi dei musicisti sul palco resta girare pagina con la mano, problema che con iPad rimane. AirTurn sta sviluppando un sistema per girare pagina con un movimento del piede, che sarà il benvenuto da molti musicisti con l'eccezione probabile di batteristi, percussionisti, pianisti e organisti, che i piedi li devono usare.

Resta il fatto che su un leggio iPad ci sta alla grande e il Vaio proprio no.