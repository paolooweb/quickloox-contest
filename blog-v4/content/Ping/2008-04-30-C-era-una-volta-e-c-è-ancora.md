---
title: "C'era una volta (e c'è ancora)"
date: 2008-04-30
draft: false
tags: ["ping"]
---

Il progetto Copland, che avrebbe dovuto fare evolvere Mac OS negli anni Novanta e mai decollò, non è stato un fallimento completo come alcuni sostengono. Molte delle sue tecnologie sono arrivate su Mac una alla volta, alla spicciolata.

E anche su Mac OS X. Alzi la mano chi, davanti a <a href="http://www.macworld.com/article/133222/2008/04/time_machine_copland.html" target="_blank">queste schermate</a>, non riconosce l'embrione di Time Machine e di Spotlight.