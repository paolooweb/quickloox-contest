---
title: "Il troppo (poco) stroppia"
date: 2006-07-04
draft: false
tags: ["ping"]
---

Sono estremamente favorevole alle interfacce discrete e non invasive. Proprio per questo <a href="http://www.hogbaysoftware.com/product/writeroom" target="_blank">WriteRoom</a>, nonostante le apparenze, mi sembra esagerato.

A quel punto, tanto vale scrivere con emacs.