---
title: "Garanzie sui tempi"
date: 2002-01-01
draft: false
tags: ["ping"]
---

Il tempo giusto per ogni cosa e ogni cosa a suo tempo: il vero significato di real time

Sento un amico musicista che dice: sono perplesso, perché con Mac OS X e questo Unix temo che le mie applicazioni possano perdere colpi a causa del multitasking prelazionale.
Nessun problema, anzi, meno problemi di prima, perché Mac OS X garantisce il real time; ossia, quando un’applicazione ha bisogno di un rispetto preciso di tempi e sincronie, il sistema operativo si impegna al massimo per garantirlo.
Prima, in Mac OS 9, il sistema operativo si disinteressava completamente della situazione e lasciava che fosse l’applicazione a doverci pensare lei, con il rischio che magari uno spooling di stampa dimenticato mettesse a rischio il rispetto dei tempi in questione. Ora non può più succedere.
Ciò non significa, naturalmente, che il computer ora possa eseguire compiti fuori portata per la sua potenza di elaborazione: un filmato male compresso su un disco troppo frammentato sarà sempre a rischio, real time o meno.

lux@mac.com