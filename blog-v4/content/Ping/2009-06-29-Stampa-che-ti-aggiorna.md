---
title: "Stampa che ti aggiorna"
date: 2009-06-29
draft: false
tags: ["ping"]
---

Tenere duro e tradurre tutti i giorni una voce dalla pagina delle <a href="http://www.apple.com/it/macosx/refinements/enhancements-refinements.html" target="_blank">novità di Snow Leopard</a>, che Apple dovrebbe tradurre e non fa (lo farà, ma in ritardo).

<b>Aggiornamenti automatici per i driver di stampa</b>

Quando si collega una nuova stampante, un Mac con Snow Leopard può scaricare da Internet il driver più aggiornato. Il sistema controlla periodicamente che il driver sia sempre aggiornato e scarica automaticamente nuove versioni via Aggiornamento Software.

L'eliminazione di oltre un gigabyte di driver inutili, per avere a bordo solo quelli che servono; questa novità andrebbe&#8230; stampata ovunque.