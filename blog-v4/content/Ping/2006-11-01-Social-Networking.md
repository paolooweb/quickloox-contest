---
title: "Social Networking"
date: 2006-11-01
draft: false
tags: ["ping"]
---

Sul treno per Milano, esperimento. Attivi AirPort, crei una rete, apri iChat e Proteus.

S&#236;. Trovi un qualche portatile acceso con AirPort acceso con un qualche programma di chat lasciato acceso (il treno per Milano ci mette mezz'ora e non serve economizzare batteria).

Ti metti a chattare con qualcuno che è da qualche altra parte nella carrozza e che altrimenti non avresti mai &#8220;incontrato&#8221;.

Interessante.