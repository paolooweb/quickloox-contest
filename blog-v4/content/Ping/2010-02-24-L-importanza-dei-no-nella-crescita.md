---
title: "L'importanza dei no nella crescita"
date: 2010-02-24
draft: false
tags: ["ping"]
---

Tim Cook, <i>Chief Operating Officer</i> di Apple, è intervenuto alla <a href="http://www.apple.com/quicktime/qtv/goldmansachs10/" target="_blank">Goldman Sachs Technology & Internet Conference</a>. Negli ultimi tre minuti ha detto una cosa fondamentale per capire chi è e che cosa fa Apple. In parte parafrasata, eccola:

<cite>Siamo l'azienda più focalizzata che io conosca o di cui abbia letto o della quale abbia notizia.</cite>

<cite>Ogni giorno diciamo no a buone idee. Diciamo no a grandi idee allo scopo di concentrarci su un numero molto ridotto di progetti su cui possiamo concentrare un'energia enorme.</cite>

<cite>Sul tavolino che avete davanti potreste probabilmente appoggiare tutti i prodotti che fabbrica Apple. Eppure, per cos&#236; pochi, Apple ha fatturato quaranta miliardi di dollari l'anno scorso.</cite>

<cite>Credo che qualunque altra azienda in grado di dire questo sia un'azienda petrolifera.</cite>

Conosco un sacco di gente con buone idee. Ne conosco pochissimi che hanno avuto veramente successo. Sono quelli che hanno saputo dire no alle cose non essenziali.