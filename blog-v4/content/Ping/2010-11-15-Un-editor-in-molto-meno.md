---
title: "Un editor in (molto) meno"
date: 2010-11-15
draft: false
tags: ["ping"]
---

Si riapre la ricerca di un editor di testo e codice per iPad che, a prendere <a href="http://bbedit.com/" target="_blank">BBEdit</a> come modello ideale, riesca a richiamarne decentemente alcuni tratti per me essenziali come la colorazione della sintassi Html e buone funzioni essenziali di trattamento del testo puro (che non è <i>word processing</i>, dove su iPad <a href="http://itunes.apple.com/it/app/pages/id361309726?mt=8" target="_blank">Pages</a> basta e avanza).

<a href="http://itunes.apple.com/it/app/editor-for-ipad/id372231108?mt=8" target="_blank">Editor for iPad</a> si è rivelato pesantemente inadeguato. Una mezza schifezza, anzi. Lo schermo è costantemente in ritardo nel mostrare il testo digitato, difetto inaccettabile. Il programma non registra automaticamente le modifiche e, nella violazione delle linee guida dell'interfaccia umana per iPad, effettua sempre l'equivalente di un Registra con nome. Oltre cioè a costringere al salvataggio manuale, che non si dovrebbe, richiede due comandi quando ne basterebbe uno.

Il peggio del peggio è la deriva del cursore di testo, che a un certo punto se ne va in una posizione (e in una riga) diversa da quella dove effettivamente si inserisce il testo. In una interfaccia a tocco è un problema critico.

Il prossimo candidato verrà scelto in una rosa che comprende <a href="http://itunes.apple.com/it/app/ewebeditor/id364904271?mt=8" target="_blank">eWebEditor</a>, <a href="http://itunes.apple.com/it/app/for-i-code-editor-for-the-ipad/id363493710?mt=8" target="_blank">for i: Code Editor per iPad</a>, <a href="http://itunes.apple.com/it/app/kytekhtmleditor/id349354394?mt=8" target="_blank">KyTekHtmlEditor</a>, <a href="http://itunes.apple.com/it/app/nebulous-notes-for-dropbox/id375006422?mt=8" target="_blank">Nebulous Notes (for Dropbox)</a>, <a href="http://itunes.apple.com/it/app/id383577124?mt=8" target="_blank">Textastic</a> e <a href="http://itunes.apple.com/it/app/web-page-developer/id365157541?mt=8" target="_blank">Web Page Developer</a>.

Si accettano pareri. Attenzione, in caso di esigenze minime, a <a href="http://itunes.apple.com/it/app/edhita-open-source-text-editor/id398896655?mt=8" target="_blank">Edhita</a>, un editorino modesto modesto che fa quasi nulla e che però è gratis e <i>open source</i>. In proiezione futura potrebbe sorprendere. Oggi vale poco più di quello che costa.