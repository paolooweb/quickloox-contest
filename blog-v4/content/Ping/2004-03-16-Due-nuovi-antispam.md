---
title: "Altre armi di distruzione di spam"
date: 2004-03-16
draft: false
tags: ["ping"]
---

La corsa al 100 percento di efficacia contro la posta indesiderata

Più o meno siamo al 99,996 percento di capacità di riconoscere la posta-spazzatura da quella normale, ma i programmatori non sono ancora contenti e nascono in continuazione strumenti sempre più sofisticati.

Ne segnalo due, <link>PopFile</link>http://popfile.sourceforge.net/ e <link>JunkMatcher</link>http://www-2.cs.cmu.edu/~benhdj/Mac/junkMatcher.html. Uno esiste in formato platform-independent, l’altro funziona con Mail di Apple (e il suo sito contiene numerose informazioni che vale la pena di esaminare).

Che siano meglio o peggio degli altri è questione delicata, perché dipendente anche dall’intelligenza di chi usa i programmi e non solo dai programmi.

Purtroppo sulla criminale stupidità degli spammer possiamo sempre contare.

<link>Lucio Bragagnolo</link>lux@mac.com