---
title: "Lettura per un sabato di pioggia"
date: 2007-03-09
draft: false
tags: ["ping"]
---

Non credo che sarà <em>questo</em> sabato. Pochi pezzi sul linguaggio scritto e sulla sua decadenza (nonché sulla possibilità di recuperare) sono a mio parere densi di significato come <a href="http://www.orwell.ru/library/essays/politics/english/e_polit" target="_blank">questo saggio</a>.

Di George Orwell, 1946. Noi mi sa che un Orwell lo dobbiamo ancora trovare, anche se con Montanelli ci siamo andati in qualche modo vicino. In tutti i casi, provare a leggere il testo pensando <em>Italiano</em> invece che <em>English</em> e <em>2007</em> al posto di <em>1946</em> è scioccante.

<cite>La battaglia contro il cattivo italiano non è frivola e non è preoccupazione esclusiva degli scrittori professionisti.</cite>

Che c'entra con il Mac? Niente o forse tutto. I Mac finiscono perlopiù in mano a persone più ricche, culturalmente parlando. I peggiori analfabeti che ho conosciuto usano Windows e, se generalizzare è sempre pericoloso, stavolta si va sul sicuro.

Oppure: impariamo, anche sul Mac, a <a href="http://flesh.sourceforge.net/" target="_blank">scrivere in modo più semplice</a>.