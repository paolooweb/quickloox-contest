---
title: "tuncfg, chi era costui?"
date: 2008-09-28
draft: false
tags: ["ping"]
---

Durante una sessione di Console in abbinamento a Monitoraggio Attività ho notato che il mio PowerBook dedica molto tempo di Cpu a <code>syslogd</code>. Non so che cosa sia, ma dal nome ha l'aria del <em>daemon</em> che provvede ad aggiornare i <em>log</em> di sistema o forse <code>system.log</code>.

Dovrebbe esserci quindi qualcosa che riempie i <code>log</code> di sistema. In effetti, ogni dieci secondi muore e viene rilanciato il processo <code>tuncfg</code>, cui provvede <code>tuncfgd</code>, suppongo il suo <em>daemon</em> deputato.

Il nome di <code>tuncfg</code> però non mi suggerisce niente (<em>cfg</em> in genere sta per <em>configuration</em>, ma boh). In attesa di un lampo di ispirazione o di una disamina della documentazione di Mac OS X, c'è un'anima buona preparata in argomento che mi racconta un po'? Ringraziamenti in anticipo.