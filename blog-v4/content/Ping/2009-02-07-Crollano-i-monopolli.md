---
title: "Crollano i monopolli"
date: 2009-02-07
draft: false
tags: ["ping"]
---

<cite>In aggiunta ho scoperto un concorrente di iTunes che sta crescendo a dismisura nelle scuole: Ruckus. Costa cinque ragionevoli dollari al mese per scaricamento illimitato. La crescita, mi dicono, è praticamente verticale e iTunes neanche offre qualcosa di analogo.</cite>

<cite>Quando questi studenti lasceranno la scuola, proprio come è accaduto per il Mac, molto probabilmente non supporteranno Apple e se Ruckus continua a crescere questo significherà nel lungo termine una discesa per iTunes e iPod, a meno che Apple non fornisca un servizio simile che soddisfi le esigenze degli attuali utenti di Ruckus.</cite>

<cite>Certo ci vorranno diversi anni perché questa tendenza si sviluppi e probabilmente non sarà visibile fino al 2009 o più tardi, ma questo è il tipico modo in cui crollano i monopoli, erosi ai margini e, in questo caso, anticipati su mercati futuri grazie al successo ottenuto con i consumatori più giovani.</cite>

Questo era <a href="http://news.digitaltrends.com/talk-back/166/iphone-vs-lg-prada-ruckus-vs-itunes" target="_blank">Rob Enderle su Digital Trends, 24 gennaio 2007</a>. L’articolo è molto più lungo e contiene anche una ottima spiegazione del perché l’Lg Prada avrà venduto più di iPhone.

Questo è <a href="http://www.ruckus.net" target="_blank">Ruckus</a>.