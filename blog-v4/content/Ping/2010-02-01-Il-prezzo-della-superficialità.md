---
title: "Il prezzo della superficialità"
date: 2010-02-01
draft: false
tags: ["ping"]
---

Paolo Attivissimo ha pubblicato una <a href="http://attivissimo.blogspot.com/2010/01/aiped-aipad-aipod.html" target="_blank">presa d'atto di iPad</a> (chiamarla recensione mi è impossibile, nell'evidenza che non c'è stata una prova, ma nemmeno un qualsiasi minimo approfondimento) che sembra una stroncatura di iPhone riciclata dal 2007, unita a considerazioni insolite, data la loro superficialità, per un personaggio della sua levatura. Da lui sono abituato ad attendermi molto e bene, ed è un complimento sincero. Sarebbe suo pieno diritto stroncare o approvare iPad, una volta in possesso delle informazioni necessarie e sviluppate le argomentazioni appropriate.

Se si torna sulla sua pagina seguendo il link sopra si nota che è stato registrato il mio intervento. Non ho capito se non godo di abbastanza stima da essere chiamato per nome o se Paolo crede che io rappresenti l'intera testata di Macworld e questo sarebbe ovviamente secondario (non conto), se non fosse aggiungere superficialità: la seconda interpretazione è palesemente errata. La prima è nel suo diritto, certo poco simpatica. Sarebbe per me uno stimolo a lavorare di più e meglio. Altri se la prenderebbero.

Prendiamo la sua analisi del prezzo: <cite>629 dollari più accessori e forse Iva per la versione minima con 3G.</cite>

Estratta dal contesto sembra neutra. Nel contesto, invece, ha accezione integralmente negativa. E sarebbe un giudizio incontestabile, se argomentato.

Invece sottolinea un dato che in sé non conta niente: 629 dollari. È tanto? È poco? In base a che cosa? Il modello base costa 499 dollari, quello massimo 829 dollari: sono sempre troppi? O troppo pochi? Il dato, per Paolo, è negativo e si capisce chiarissimamente. Ma non si capisce perché. Per fare un solo esempio, non ricordo di avere visto previsioni inferiori agli ottocento dollari. Quale sarebbe il prezzo giusto? E perché? Misteri. Ho già sentito gente che intende acquistare la versione senza 3G. Io non lo farei, perché avrei già in mente una situazione precisa in cui mi servirebbe a perfezione un iPad con 3G. Ma questo non vuol dire che il punto di riferimento debba essere per forza la configurazione che serve a me.

La dose viene rincarata. Sempre con accezione negativa, il prezzo è 629 dollari <cite>più accessori</cite>. Che cosa brutta, eh? Eppure a me pare che il costo di <i>qualsiasi cosa</i> sia <cite>più accessori</cite>. O magari non ho capito io e disporre di accessori è una caratteristica negativa.

Il colpo finale: <cite>e forse Iva</cite>. Forse che noi italianuzzi potremo evaderla? Forse che la presenza dell'Iva sia un errore progettuale di Apple? Forse che altri apparecchi costino senza Iva? Non capisco.

Non capisco soprattutto perché sia negativo il prezzo in dollari, considerato che il prezzo in Italia e in Svizzera <i>non è ancora stato definito</i>. Apple ce ne ha fatte vedere di tutti i colori, dall'equiparazione euro-dollaro a equivalenze pratiche del rapporto di cambio. iPad, qui, potrebbe costare &#8211; nel modello indicato &#8211; seicento euro, o cinquecento, magari settecento. Per Paolo non importa: è sbagliato comunque. Qualunque prezzo avrà questo oggetto, è un prezzo sbagliato.

A questo punto viene da pensare che il giudizio sul prezzo sia espresso per partito preso, anziché previa riflessione, informazione e valutazione. E mi dispiace un po', perché vorrei sentire voci critiche di ogni orientamento su iPad e questa non è una voce, ma un timbro messo con sprezzo e superficialità.

Si guardi <a href="http://www.fantascienza.com/blog/esseasterisco/2010/01/29/vi-spiego-lipad/" target="_blank">che cosa scrive Silvio Sosio di iPad</a>. Lui è abbastanza favorevole. Avrebbe potuto essere abbastanza contrario, non è certo sospettabile di simpatie automatiche per Apple. Però c'è una riflessione, ci sono fatti, ci sono considerazioni argomentate, c'è una linea di pensiero. <cite>629 dollari e forse Iva</cite>, beh, sembra di essere al bar sport.

Per Paolo, se mi legge: con stima immutata.