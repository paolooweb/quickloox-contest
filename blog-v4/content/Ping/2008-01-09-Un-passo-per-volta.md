---
title: "Un passo per volta"
date: 2008-01-09
draft: false
tags: ["ping"]
---

Ci si sta lavorando. L'autore di <a href="http://www.asymptopia.org/index.php?topic=TWS" target="_blank">TuxWordSmith</a> ha fatto uscire una nuova versione che rimedia ad alcuni dei problemi che non fanno installare il gioco su Mac.

Alcuni, non tutti. Ma il lavoro è in corso. Leggere, per credere, il registro dei <a href="http://www.asymptopia.org/software/CHANGES-TWS-0.6.0" target="_blank">cambiamenti</a> apportati. :-)