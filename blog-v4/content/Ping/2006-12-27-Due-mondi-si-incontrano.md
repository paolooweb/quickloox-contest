---
title: "Due mondi si incontrano"
date: 2006-12-27
draft: false
tags: ["ping"]
---

Oberato dagli impegni e dalla disorganizzazione (mia) non sono più riuscito a varare la seconda Ping-pizza e devo varie e numerose scuse, in particolare modo a Carmelo.

Detto ciò, venerd&#236; 29 dicembre (domani!) c'è la pizzata del <a href="http://www.poc.it" target="_blank">Poc</a>. Sicuramente saremo abbastanza poc-hi&#8230; se qualcuno da Ping se la sente di aggiungersi all'allegra brigata, è assolutamente invitatissimo.

Ci troviamo a Milano presso <a href="http://www.macatwork.net" target="_blank">Mac@Work</a> e in un momento imprecisato dopo le ore 20 ci spostiamo alla pizzeria Bio Solaire di via Terraggio 20, letteralmente a due passi.

Possiamo farci gli auguri di fine anno e pure quelli di Natale. 2007, se necessario. :-)