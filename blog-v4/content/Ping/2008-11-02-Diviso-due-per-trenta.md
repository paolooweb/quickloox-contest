---
title: "Diviso due per trenta"
date: 2008-11-02
draft: false
tags: ["ping"]
---

Le statistiche di Net Applications sul <a href="http://marketshare.hitslink.com/report.aspx?qprid=9" target="_blank">traffico web per il mese di ottobre</a> indicano che iPhone fa poco meno della metà del traffico di Linux, 0,33 contro 0,71.

Contemporaneamente indicano che Symbian e Windows Mobile non sono neanche nel radar. In altre parole, iPhone e iPod touch fanno oltre trenta volte il traffico web di decine di milioni di Nokia e altrettanti Windows Mobile messi insieme. Trenta volte. E poi si questiona sugli Mms.