---
title: "Vecchi compagni d'arme"
date: 2006-11-25
draft: false
tags: ["ping"]
---

Insieme nella compagnia storica nella quale giochiamo di ruolo pur essendo ormai chi padre, chi imprenditore, chi rintronato (il sottoscritto) e chi più ne ha più ne metta, stiamo preparando il programma per la tradizionale <em>plenaria</em> natalizia (ci troviamo qualche giorno prima di Natale e giochiamo come scemi per una giornata intera).

Quest'anno il programma è inedito: nel pomeriggio metteremo su un <em>lan party</em> giocando a <a href="http://nwn.bioware.com/" target="_blank">Neverwinter Nights</a>. Gioco vecchio, ma lo abbiamo tutti e sette. Siamo in fase di reperimento dei due portatili che mancano all'appello e, tra wireless e cavetteria assortita, sappiamo già come cablare le macchine per avere la massima stabilità dal server del gioco.

I primi esperimenti hanno confermato che giocare a <a href="http://www.wizards.com/dnd" target="_blank">Dungeon &#38; Dragons</a> attraverso il computer sarà un gran casino&#8230; ma potrebbe anche essere divertente.

Dimenticavo: cinque dei sette portatili sono Mac.