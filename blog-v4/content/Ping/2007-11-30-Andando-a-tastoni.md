---
title: "Andando a tastoni"
date: 2007-11-30
draft: false
tags: ["ping"]
---

Scrive <strong>Nicola</strong>:

<cite>Sono sicuro che un estimatore della tastiera come te apprezzerà <a href="http://www.usingmac.com/2007/11/21/mac-os-x-leopard-200-productivity-booster-hotkeys" target="_blank">questa pagina</a>:</cite>

<cite>È aggiornata al 21 novembre. Da neofita Mac l'ho trovata estremamente utile e, a mio parere, ben fatta.</cite>

È ben fattissima. Ho già scoperto un paio di cose che mi sfuggivano. Complimenti agli autori e a Nicola per averla scoperta!