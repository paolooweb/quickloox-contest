---
title: "Colpo d'incontro"
date: 2009-01-11
draft: false
tags: ["ping"]
---

Sono uscito da Facebook nel momento in cui Microsoft ne ha assunto la gestione pubblicitaria. Ma sono generalmente favorevole a un uso intelligente del <em>social networking</em>.

Cos&#236;, per curiosità, ho aderito al danese <a href="http://myapplespace.com" target="_blank">MyAppleSpace</a>. Si capisce dal nome che la comunità ha un punto di riferimento ben preciso.

Vediamo che succede, chi si incontra, se ne vale la pena.