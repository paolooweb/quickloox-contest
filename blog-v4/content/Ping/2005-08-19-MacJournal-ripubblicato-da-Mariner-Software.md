---
title: "Grandi applicazioni crescono comunque<p>"
date: 2005-08-19
draft: false
tags: ["ping"]
---

Un caso esemplare di signor software che mette su famiglia<p>

L&rsquo;opinione (condivisa) di <strong>Luca</strong>:<p>

<cite>Non so se conosci già MacJournal <em>[se ne era parlato tempo fa, ma merita comunque. N.d.l.]</em>, ma personalmente non mi stancherò mai di consigliarlo a tutti.</cite><p>

<cite><a href="http://homepage.mac.com/dschimpf">MacJournal</a>, fantastica applicazione scritta in Cocoa, è una di quei software tra virgolette piccoli, ma che cambiano il modo di approcciarsi alle idee, agli appunti, ai progetti; se passi molte ore davanti a un monitor, come nel mio caso, senza volerlo MacJournal ti cambia un pochetto la vita. In meglio.</cite><p>

<cite>L&rsquo;applicazione è stata recentemente acquistata da <a href="http://www.marinersoftware.com/sitepage.php?page=85">Mariner Software</a>, che vende la versione 3.x a circa 30 dollari. Ma se vuoi provare, tutte le versioni precedenti sono gratuite, compresa la 2.6 che è quella di cui ti sto raccontando.</cite><p>

Ho da aggiungere solo che l&rsquo;autore originale di MacJournal continua a essere proprietario e unico sviluppatore del programma. Che può solo diventare ancora migliore.<p>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>