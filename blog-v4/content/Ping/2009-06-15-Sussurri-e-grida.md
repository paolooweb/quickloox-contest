---
title: "Sussurri e grida"
date: 2009-06-15
draft: false
tags: ["ping"]
---

Sempre alla ricerca di audiochat ecumeniche, sia per Windows che per Mac OS X, ecco Mumble, <a href="http://mumble.sourceforge.net/" target="_blank">open source</a>.

È nato come programma per la chat dentro i giochi <i>online</i> e su alcuni di essi ha persino il supporto posizionale: la voce del giocatore arriva in cuffia dalla direzione in cui si trova il suo personaggio.