---
title: "Di sarcasmo non ce n'è mai abbastanza"
date: 2010-01-16
draft: false
tags: ["ping"]
---

Una azienda americana, tale Sarcasm, ha inventato un nuovo <a href="http://02d9656.netsoljsp.com/SarcMark/modules/user/commonfiles/supported.jsp" target="_blank">segno di interpunzione</a> da usare su Internet per indicare una frase sarcastica e lo&#8230; vende per 1,99 dollari.

La pagina annuncia che il supporto per Mac e iPhone ancora non è pronto.

Per me significa che la gente intelligente il sarcasmo lo capisce da sola.