---
title: "I vantaggi dei netbook"
date: 2009-04-12
draft: false
tags: ["ping"]
---

La spedizione che scala l'Everest e produce materiale video in alta definizione per il proprio <em>sponsor</em> <a href="http://blog.firstascent.com/2009/04/10/behind-the-scenes-high-altitude-production/" target="_blank">si porta dietro una flotta di MacBook Pro</a>.

Il peso, l'ingombro, la batteria&#8230; se devi lavorare veramente, anche a seimila metri, non c'è <em>netbook</em> che tenga.