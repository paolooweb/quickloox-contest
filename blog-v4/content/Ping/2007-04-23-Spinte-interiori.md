---
title: "Spinte interiori"
date: 2007-04-23
draft: false
tags: ["ping"]
---

Non c'entra con il Mac, ma quando c'è di mezzo un'interfaccia sento sempre una spinta interiore.

Dovevo cercare la zona di Milano a cui corrisponde il codice di avviamento postale 20141. Sono andato sul sito delle Poste.

Le quali scrivono che è possibile trovare l'elenco delle località o degli indirizzi appartenenti ad un determinato Cap.

Nel caso delle 27 città divise in zone postali, però, è necessario inserire anche l'indirizzo.

Riassunto: per trovare quello che sto cercando, devo averlo già trovato e dire che lo sto cercando.

Il sito delle Poste mi genera una spinta interiore. Diversa da quella di prima.