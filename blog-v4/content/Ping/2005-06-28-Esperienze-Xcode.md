---
title: "Nuove esperienze<p>"
date: 2005-06-28
draft: false
tags: ["ping"]
---

Tra gli sviluppatori c&rsquo;è chi non sta perdendo tempo<p>

Al convegno mondiale degli sviluppatori Apple Steve Jobs ha reiterato l&rsquo;invito, un po&rsquo; pressante, ad adottare Xcode come sistema di sviluppo dei programmi per Mac.<p>

La cosa bella è che alcuni sviluppatori, come <a href="http://xcode-experiences.blogspot.com">Marshall Clow</a>, hanno aperto blog in cui raccontano della loro esperienza da switcher, dell&rsquo;ambiente di sviluppo però.<p>

Per tutti quelli che fanno programmazione oltre la dose modica quotidiana di AppleScript sono letture utili, che aiuteranno sicuramente nella transizione.<p>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>