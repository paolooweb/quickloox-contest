---
title: "Un clic è fin troppo"
date: 2010-03-16
draft: false
tags: ["ping"]
---

Molti anni fa, oramai si può dire, collaborai al varo della prima <i>community</i> di utenti sul sito Mediaset.

Ci fu una grossa discussione sul fatto che la procedura di registrazione era troppo lenta, chiedeva troppi dati e alla fine richiedeva anche un clic di troppo.

Feci notare che quel clic andava assolutamente eliminato e un manager rispose con fare annoiato <cite>non ci vuole niente a fare un clic</cite>.

Fecero come diceva il manager e sei mesi dopo rifecero tutto da capo a piedi. I clic sono importanti.

All'oggi, mi sono registrato a Macworld.com &#8211; quello americano, non questo &#8211; e la registrazione impone di iscriversi ad almeno una <i>newsletter</i> del sito, altrimenti è impossibile.

Mi serviva registrarmi e quindi l'ho fatto.

Contestualmente, la <i>software house</i> che produce uno dei programmi che ho acquistato in sconto con MacHeist mi ha scritto, tramite MacHeist, per sapere se ero interessato a ricevere la loro <i>newsletter</i>.

Su MacHeist, per aderire o sganciarsi da una <i>newsletter</i>, metti o togli un segno di spunta con un clic.

Su Macworld.com sei obbligato a iscriverti a una <i>newsletter</i> anche se non te ne frega niente.

Naturalmente ambedue i sistemi permettono di disiscriversi.

Indovinello. Alla fine di questa esperienza mi sono ritrovato iscritto a una sola <i>newsletter</i>: secondo te è quella del programma di <a href="http://www.macheist.com" target="_blank">MacHeist</a> o quella di <a href="http://www.macworld.com" target="_blank">Macworld.com</a>?