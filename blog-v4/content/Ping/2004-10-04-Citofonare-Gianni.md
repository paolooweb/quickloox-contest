---
title: "Citofonare Gianni<p>"
date: 2004-10-04
draft: false
tags: ["ping"]
---

Altra testimonianza sul prezzo di iMac G5<p>

Daniele Savi vuole rubarmi il posto (scherzo, lo ringrazio molto, invece!) e mi ha inviato un altro messaggio che veramente non ha bisogno di commenti:<p>

<cite>Ieri leggevo un depliant dell&rsquo;Unieuro e ho beccato una cosa molto divertente: in offerta promozionale un PC Sony &ldquo;All in one&rdquo;, solo schermo e mouse/tastiera, praticamente identico come &ldquo;idea&rdquo; al nuovo iMac, solo molto più triste come design. Beh, il prezzo in offerta era di quasi 1.400 Euro&hellip; e ovviamente non c&rsquo;è Mac OS X, ma Windows. :-D</cite><p>

<cite>Ah, e ovviamente c&rsquo;è su un Pentium 4, non un processore a 64bit&hellip;</cite><p>

<cite>Tira tu le conclusioni. :-D</cite><p>

Si tirano da sole. L&rsquo;ottimismo è il profumo della Mela.<p>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>