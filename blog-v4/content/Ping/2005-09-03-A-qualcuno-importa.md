---
title: "A qualcuno importa<p>"
date: 2005-09-03
draft: false
tags: ["ping"]
---

Non finisco mai di trovare novità in Tiger<p>

Come sempre sono dettagli, eppure quando te le accorgi ti senti orgoglioso, anche se ci sono sono arrivati in venticinque milioni prima di te.<p>

Acquisizione Immagine è sparita e le sue funzioni sono state comprese in Anteprima. Una scelta buona, che riduce la complessità del sistema senza appesantire oltre il lecito un programma che peraltro sta diventando sempre più importante (oramai è il mio default per la visione dei Pdf e della grafica in generale).<p>

Come notizia non è certo una&hellip; anteprima, ma perché negarsela?<p>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>