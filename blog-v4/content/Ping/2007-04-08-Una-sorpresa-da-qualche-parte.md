---
title: "Una sorpresa da qualche parte"
date: 2007-04-08
draft: false
tags: ["ping"]
---

Gli americani chiamano uovo di Pasqua (<em>easter egg</em>) qualcosa di imprevedibile nascosto dentro un programma, che può andare da un simulatore di volo (nascosto dentro una edizione di Excel di anni fa) fino al banale elenco degli autori del software, che però si può vedere solamente compiendo una manovra atipica con i tasti.

È Pasqua e ognuno di noi vorrebbe essere in un posto particolare e ricevere una sorpresa. Auguro a tutti di esserci davvero e riceverne una bellssima. E intanto, apri Dashboard. Apri il widget Weather. Tieni premuti i tasti Comando e Opzione e clicca sull'icona del tempo che fa, quella in alto al centro.

Anche Dashboard vorrebbe essere da qualche parte. :-)