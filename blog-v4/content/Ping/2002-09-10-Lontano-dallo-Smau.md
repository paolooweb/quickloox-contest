---
title: "Lontano da Smau"
date: 2002-09-10
draft: false
tags: ["ping"]
---

Varie buonissime ragioni per cui Apple evita di sprecare soldi alla fiera del gadget

C’era una volta la fiera, prima di Internet. Lì ti mostravano i nuovi prodotti, ti vendevano i prodotti vecchi con lo sconto, conoscevi nuovi operatori, intervistavi la gente famosa.

Adesso c’è Internet. I nuovi prodotti li presentano in tempi globalizzati, buoni per l’Europa e per l’Asia insieme. Tutti i fatti e le opinioni sui prodotti sono in Rete. Sull’Applestore per comprare basta un clic. I nuovi operatori ti ammollano un biglietto da visita e ti dicono di richiamarli dopo Smau, in un inferno di bambini che spingono, signore con passeggino, pensionati in gita-gadget per conto dei figli che sono anche genitori, venditori che vogliono solo vendere (la parola “informazioni” ora si traduce in “prendi una brochure e vai in pace”). Adolescenti-con-zainetto in cerca di una ragione per non andare a scuola. Voyeur della domenica che cercano ragazze abbastanza nude. Dammi la matitina, posso avere la pallina, firma qui e vinci un portachiavi galleggiante. La gente famosa sta negli alberghi intorno a Smau, con l’aria condizionata invece dei trentacinque gradi all’ombra e dell’assalto prandiale a ristoranti costruiti per essere troppo piccoli.

Andare a Smau, per un’azienda, costa un patrimonio e i suoi visitatori annui si possono esprimere in una frazione delle persone che visitano quotidianamente l’Apple Store. Apple fa benissimo a non andare a Smau, perché prima di Internet serviva a qualcosa e ora serve solo a giustificare lo stipendio del personale fieristico. Per quanto mi concerne, vado a Smau mezza giornata, per salutare i vecchi amici costretti a lavorare lì da un marketing manager nato durante una guerra mondiale qualsiasi. Andarci per lavoro? Sto sul Web un paio d’ore e ho più materiale di quanto potrei raccoglierne in tre settimane di Smau. Andarci per svago? Buono per famiglie in vacanza, turisti giapponesi e liceali senza voglia di studiare. Le standiste nude? Capisco l’erotismo aziendale, ma c’è di meglio a tutti i livelli. Morale: vanno allo Smau tutti, ma nessuno per conoscere o comprare un Mac, cose che si fanno assai meglio altrove.

Se accetti un consiglio, guarda lo Smau via Internet. Apple c’è già arrivata da un pezzo.

<link>Lucio Bragagnolo</link>lux@mac.com