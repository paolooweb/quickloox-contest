---
title: "Ogni regola ha la sua eccezione"
date: 2010-06-09
draft: false
tags: ["ping"]
---

Ricevo e pubblico:

<cite>Sto cercando uno/a stagista per una</cite> internship <cite>estiva a Londra.</cite>

<cite>Ovviamente lo</cite> stage <cite>sarebbe pagato.</cite>

<cite>Mi servirebbe un neolaureato sveglio con tanta voglia di:</cite>

<cite>imparare</cite><br />
<cite>lavorare</cite><br />
<cite>divertirsi (nell'ordine ;-)</cite>

<cite>Conoscenze necessarie? un livello di inglese dignitoso.</cite>

<cite>Conoscenze simpatiche da avere? esperienza in PLSQL e modellazione dati.</cite>

<cite>Dove? In centro a Londra per una società che gestisce scommesse sportive e giochi</cite> online<cite>, durante i mondiali è il posto giusto dove essere!</cite>

<a href="mailto:aieie.brazor@gmail.com">Beppe</a>

Ping! non è una agenzia di collocamento, ma una eccezione si può fare, soprattutto per una bella occasione di fare esperienza all'estero.

Chi fosse interessato scriva direttamente a Beppe, linkato qui sopra.