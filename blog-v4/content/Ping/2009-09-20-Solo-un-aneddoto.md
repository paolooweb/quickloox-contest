---
title: "Solo un aneddoto"
date: 2009-09-20
draft: false
tags: ["ping"]
---

Segue la traduzione di un messaggio comparso sulla <i>mailing list</i> <a href="http://www.listmoms.net/lists/apple-perspectives/" target="_blank">Apple Perspectives</a>:

<i>Vuole essere solo un resoconto divertente, che non intende dimostrare alcunché.</i>

<i>Ieri sono entrato in un negozio di elettronica in cerca di un piccolo accessorio. Il gestore ha risposto che riteneva fosse fuori produzione e comunque non lo aveva in assortimento. Gli ho chiesto se potesse controllare sul sito della catena e mi ha risposto che sfortunatamente non poteva. Cos&#236; ho estratto iPhone e mi sono collegato, per cercare da me.</i>

<i>Questa è quasi testualmente la conversazione che ne è scaturita:</i>

<i>Gestore:</i> È iPhone?

<i>Io:</i> S&#236;, è il primo modello.

<i>[gli spiego e gli faccio vedere che il primo iPhone è un po' lento e che ci mette un po' a scaricare la pagina del sito della catena]</i>

<i>Gestore:</i> Oh, sta pensando di prenderne uno nuovo?

<i>Io:</i> No, perlomeno non adesso. Perché?

<i>Gestore:</i> Se ne compra uno nuovo, io sarei interessato a comprare il suo.

<i>Io:</i> Uhm. Questo negozio è partner Sprint, vero? Cos&#236; lei potrebbe avere quello con WebOS, Palm Pre, giusto?

<i>Gestore [indicando una piccola fondina di cuoio alla cintura]:</i> S&#236;, ne ho preso uno...

<i>[pausa]</i>

<i>Gestore:</i> Farebbe cambio?