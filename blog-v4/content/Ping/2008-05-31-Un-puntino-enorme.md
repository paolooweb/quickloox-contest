---
title: "Un puntino in crescita"
date: 2008-05-31
draft: false
tags: ["ping"]
---

Il museo più bello del mondo <a href="http://www.allaboutapple.com/movies/raitre_pixel_2008.htm" target="_blank">ha conquistato anche RaiTre</a>.

Non c’è impresa mediatica abbastanza grande per All About Apple. :-)