---
title: "Solo chiacchiere e istintivo"
date: 2007-11-28
draft: false
tags: ["ping"]
---

Apple non ha mai rivelato dati sull'affidabilità delle sue macchine, sulla percentuale degli interventi in garanzia eccetera. Questo ingenera ogni genere di sciocchezze. Tipo, <em>mia suocera ha il Mac guasto, non è che adesso li fanno con componenti di scarsa qualità?</em> e non manca mai quello che <em>una volta era meglio</em> (dimenticandosi che una volta un Mac Plus costava sei milioni, oggi un iMac 24 pollici costa la metà di quella cifra, senza aggiustamento dell'inflazione).

Trattasi di comune reazione inconscia. <em>Avere un guasto alla macchina diminuisce la mia autostima e mi potrebbe fare dubitare di avere effettuato l'Acquisto Perfetto che fa di me una Persona Intelligente, quindi deve esserci una colpa e deve stare all'esterno.</em> Fermarsi qui, però, è infantile.

Con Internet, le reti, il social networking, creare un sistema indipendente di valutazione dell'affidabilità delle macchine Apple è un attimo. Si mette su un server centrale (peraltro poca cosa, visti i dati necessari). Si scrive un programmino che si mette in distribuzione libera sulla Rete. Il programmino non fa altro che collegarsi una volta al giorno e dire <em>Il Mac su cui risiedo sta bene</em>.

Quando il Mac si guasta, il proprietario incazzato del Mac si collega manualmente al sito e clicca una casellina che dice <em>il mio Mac si è guastato</em>. Quando il Mac ritorna in vita, il programmino riprende a trasmettere.

C'è qualche virgola da sistemare e per il resto il progetto è tutto qui. Ci sono in rete venticinque milioni di Mac; ne tiri su diecimila che scaricano il programmino e improvvisamente hai raccolto un campione molto più attendibile di tua zia e del salumiere all'angolo, con dati largamente attendibili sulla percentuale di fermo macchina dei Mac.

Non è neanche un problema di saper programmare. Per aprire un progetto su <a href="http://sf.net" target="_blank">SourceForge</a> basta saper scrivere in inglese maccheronico, o avere sottomano un ragazzino che faccia le medie e abbia un computer in casa. Cos&#236;, se non c'è un programmatore, lo si trova.

Ma nessuno di questi espertoni spreca dieci minuti del proprio tempo su un progetto concreto. Sproloquiare sulla qualità dei prodotti il giorno dopo che gli è caduto in terra l'iPod evidentemente è più semplice. Sono solo chiacchiere e istinto. Anzi, istintivo, avrebbbe detto De Niro.