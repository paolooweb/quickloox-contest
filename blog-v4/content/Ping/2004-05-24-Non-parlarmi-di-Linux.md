---
title: "Non parlarmi di Linux"
date: 2004-05-24
draft: false
tags: ["ping"]
---

L’open source è un concetto potente, ma anche la semplicità

L’open source è una delle rivoluzioni di questi anni a cavallo tra un millennio e l’altro. Sistemi operativi aperti come Linux (e lo stesso Darwin che sta sotto Mac OS X) sono destinati a fare epoca e a contare sempre più nel futuro.

Detto questo, la facilità di utilizzo è ancora una chimera e appena un mestierante non tecnico, come me, ci mette le mani è un incubo.

Prendiamo il seguente problema. Ho un disco FireWire esterno diviso in due partizioni, una delle quali è vuota e libera.

Vorrei montare Linux sulla partizione libera del disco FireWire esterno. In tre settimane di sforzi occasionali non ci sono ancora riuscito. Con Mac OS X avrei fatto un boot da Cd e in tre quarti d’ora avrei finito.

Sicuramente è possibile farlo, e Linux è un buonissimo sistema operativo. Ma prima di avere le cose veramente facili c’è ancora un po’ di strada da fare.

<link>Lucio Bragagnolo</link>lux@mac.com