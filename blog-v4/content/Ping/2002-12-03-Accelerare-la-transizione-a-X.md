---
title: "Croce e delizia nel segno di X"
date: 2002-12-03
draft: false
tags: ["ping"]
---

Una delle migliori risorse di Apple lo è se Apple darà il meglio

La transizione a Mac OS X è velocissima in senso relativo (il passaggio da un sistema operativo a un altro chiede tempo) ma lenta in senso assoluto: alla fine del 2002 la maggioranza assoluta ce l’avrà ancora Mac OS 9.

Che una quota fisiologica di persone abbia una resistenza al cambiamento è normale: è gente che ha resistenza a tutti i cambiamenti e quindi anche a questo. Però Apple deve togliere di mezzo tutte le altre scuse possibili e lo sta facendo solo in parte.

Finora Apple ha pensato, di volta in volta, che certi non passassero a Mac OS X aspettando Jaguar, oppure aspettando Photoshop, e adesso pensa che aspettino XPress (gennaio). Non è così: la potenza di Unix è una grandissima cosa ma non a costo di perdere la facilità di utilizzo di Mac.

È un buon segno il fatto che da 10.2.2 gli installer siano in grado di trovare un programma anche se è stato spostato dalla sua cartella originale. Sono tornate le cartelle a molla, ma si stanno perdendo i metadati associati ai file, e questo è un male, oltre a essere sempre stata una carta vincente dell’interfaccia.

Ognuno faccia la sua parte, noi a inviare tonnellate di <link>feedback</link>http://www.apple.com/it/macosx/feedback ad Apple, per avere quello che vogliamo; e Apple ad ascoltarci, per darcelo. Scommetto che così la transizione sarà sempre più un piacere.

<link>Lucio Bragagnolo</link>lux@mac.com