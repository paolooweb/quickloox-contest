---
title: "Navel Gazers"
date: 2007-06-07
draft: false
tags: ["ping"]
---

<a href="http://www.scripting.com/" target="_blank">Dave Winer</a>, che un po' se ne intende, sostiene che Twitter è una grande innovazione. A me sembra roba per gente che ha paura di annoiarsi e sente il bisogno costante di rimirarsi l'ombelico.

Siccome è ampiamente possibile che mi sbagli, ho provato a <a href="http://twitter.com/loox" target="_blank">iscrivermi</a>. Vediamo che succede.