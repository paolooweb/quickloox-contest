---
title: "Il mio proposito<p>"
date: 2005-12-30
draft: false
tags: ["ping"]
---

Guardo il Mac e penso che&hellip;<p>

A parte gli aggiornamenti di sistema che lo richiedono o una disgraziata morte del disco rigido, voglio arrivare a fine 2006 senza mai avere bisogno di riavviare il Mac.<p>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>