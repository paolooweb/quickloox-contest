---
title: "Una lista per passare alla storia<p>"
date: 2005-07-29
draft: false
tags: ["ping"]
---

Visto che la pubblicità non è il mio forte ripiego sulle chiacchiere<p>

È appena uscito il mio libro, pubblicato dagli editori di Macworld Italia in tutte (spero!) le edicole d&rsquo;Italia. Ma è giusto che della pubblicità si occupi l&rsquo;editore.<p>

Io mi occupo delle chiacchiere e per questo ho aperto una mailing list chiamata Storia di Mac, il cui oggetto dovrebbe essere sufficientemente chiaro.<p>

La lista sarà a basso traffico e con un rapporto segnale/rumore alto, nel senso che servirà ad approfondire il tema della storia di Mac il più possibile, non a chiedere come impostare il cellulare e neanche per sapere come mai non funziona la stampante.<p>

Per iscriversi basta una <a href="mailto:storiadimac-subscribe@yahoogroups.com">mail</a>. Tranquillo, non riceverai pubblicità del libro. :-)<p>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>