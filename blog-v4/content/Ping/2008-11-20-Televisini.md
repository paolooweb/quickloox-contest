---
title: "Televisini"
date: 2008-11-20
draft: false
tags: ["ping"]
---

I visini (per modo di dire) sono dello <em>staff</em> dello straordinario <a href="http://www.allaboutapple.com/blog/" target="_blank">museo Apple di Quiliano</a>, che hanno trovato modo di fare i presenzialisti <a href="http://www.la7.it/news/dettaglio_video.asp?id_video=19435&amp;cat=cultura" target="_blank">su La7</a> e anche su <a href="http://webtv.ivg.it/video/2008/11/06/2164/il-museo-apple-piu-grande-del-mondo-e-a-quiliano" target="_blank">Ivg.it</a>.

Se non ci fossero bisognerebbe inventarli. Visto che ci sono, li possiamo&#8230; riprodurre. :-)