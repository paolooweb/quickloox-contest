---
title: "Un altro centro di Guy"
date: 2006-09-15
draft: false
tags: ["ping"]
---

Dal blog di Guy Kawasaki, l'uomo il cui cuore sanguinerà sempre in sei colori, una <a href="http://blog.guykawasaki.com/2006/09/why_smart_peopl.html" target="_blank">spiegazione eccellente</a> della necessità del backup che ho cercato invano per anni di dettagliare almeno con la metà dell'incisività che ci ha messo lui.

Se serve dimmelo e organizzerò una traduzione. Non è da perdere.