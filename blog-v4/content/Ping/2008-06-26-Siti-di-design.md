---
title: "Siti di design"
date: 2008-06-26
draft: false
tags: ["ping"]
---

Per esempio. Si può buttare giù un blog alla brutta, senza gusto, senza attenzione. Oppure leggersi il <a href="http://opentype.info/blog/" target="_blank">blog OpenType</a>, dedicato alla tipografia sul web.

Che si impara anche dai segnali stradali, e da un sacco di altre cose. Che rivela cose incredibili fattibili con Safari e Firefox. E cos&#236; via.

Non vedo l'ora che arrivi il giorno in cui un sito potrà rivaleggiare in valore tipografico con un incunabolo o un codice miniato. Ci vorrà forse un secolo&#8230; ma vale la pena di seguire questo cammino un passo per volta.