---
title: "L’insulto peggiore: fare come Microsoft"
date: 2003-10-10
draft: false
tags: ["ping"]
---

È meglio usare parole grosse solo quando si è capaci di misurarle

Certe persone, se devono sfogare uno o più malumori contro Apple, usano come insulto velato un paragone a Microsoft: Apple, o un certo programma, o una data politica commerciale, sono come o ricordano quelle di Microsoft.

È vero che Apple a volte fa errori, è miope e francamente irrita. Ma chi dice “fa come Microsoft” non ha una idea consistente di come si comporta Microsoft.

<link>Lucio Bragagnolo</link>lux@mac.com

