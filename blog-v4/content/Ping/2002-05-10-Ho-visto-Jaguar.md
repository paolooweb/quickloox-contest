---
title: "Ho visto Jaguar"
date: 2002-05-10
draft: false
tags: ["ping"]
---

Esiste e già funziona (come prerelease). Su qualsiasi macchina attuale

A tutti quanti temono che il prossimo Mac OS 10.2 non funzionerà sui loro Mac per qualche oscura ragione dovuta a ottimizzazioni e aggiornamenti, ho un aggiornamento io.
Jaguar (questo il nome in codice) funziona su qualunque Mac che possa usare ragionevolmente Mac OS X. Ho visto installarne e usarne una versione provvisoria su un iBook, notoriamente sprovvisto di processore G4 e di schede video avveniristiche.
Difficile fare misurazioni realistiche, anche perché ho visto una versione certamente molto provvisoria, ma come minimo non si perde fluidità nell’uso. E la rotellina, oltre a girare molto meno, è anche molto più bella da vedere.

<link>Lucio Bragagnolo</link>lux@mac.com