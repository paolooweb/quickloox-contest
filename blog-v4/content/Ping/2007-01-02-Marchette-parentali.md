---
title: "Marchette parentali"
date: 2007-01-02
draft: false
tags: ["ping"]
---

La <em>marchetta</em>, nel gergo giornalistico, è quando un inserzionista manda un aggeggio da recensire e tu lo recensisci anche se non te frega niente, perché l'editore tiene all'inserzionista.

Le marchette parentali invece sono tutt'altra cosa negli scopi: parli di quello che ti hanno regalato a Natale e cos&#236; il parente scaccia il timore di avere fatto qualcosa di sbagliato, ché io sono l'esperto-di-computer e lui è venuto a chiedere in negozio di nascosto che cosa mi poteva prendere.

Fatto il preambolo dovuto, uno dei regali natalizi è un ottimo <a href="http://www.raindesigninc.com/ilap.html" target="_blank">iLap</a> da 17&#8221;. Risolve più che efficacemente il problema della dissipazione del calore quando si tiene il portatile in grembo, grazie alla piastra in alluminio.

Aumenta l'ergonomia grazie al cilindro imbottito che appoggia verso il busto e sistema più che adeguatamente polsi e avambracci. Effettivamente la posizione di uso migliora.

Verso le ginocchia c'è un appoggio incernierato che poggia efficacemente sulle gambe qualunque sia l'angolazione. Semplice, funzionale e ben pensato.

Togliendo il cilindro imbottito, iLap diventa un rialzo da scrivania. Non ho provato questa funzione estesamente, ma sembra comunque all'altezza del resto del prodotto.

L'oggetto è nell'insieme resistente, leggero, pratico, sicuro. Ho violato le raccomandazioni lasciando più volte l'iLap incustodito, sul tavolo, con sopra il computer, la prima volta con apprensione per la dimenticanza, successivamente senza problemi. Il mio PowerBook ha i piedini&#8230; dovessero essere saltati via, bisogna farci più attenzione e possibilmente usare i piedini adesivi forniti insieme a iLap per assicurarsi che il portatile non scivoli a valle, con effetti catastrofici.

Complessivamente, regalo di Natale gradito, azzeccato e in uso continuo da giorni, ormai. Unica avvertenza, ai possessori di 17&#8221; di statura non altissima: l'iLap da 17&#8221; impone uno stacco di coscia almeno sopra la media. O si è alti minimo 1,75, o si fa fatica a posizionarselo in grembo.

La raccomandazione non si applica, ovviamente, per gli iLap di dimensioni superiori.

L'oggetto non sembra valere i suoi 79 euro, a guardarlo. Invece li vale.