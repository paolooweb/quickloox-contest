---
title: "Convivendo"
date: 2009-08-11
draft: false
tags: ["ping"]
---

Sono passato su <a href="http://sourceforge.net" target="_blank">SourceForge</a> in cerca di software interessante e ho notato una scritta che forse mi era sfuggita in passato: <i>By default, we'll show you software that runs on Mac</i> (come preimpostazione ti mostreremo software che funziona su Mac).

Sempre meno incapaci che scrivono <i>sito ottimizzato per Internet Explorer 6 e tutti gli altri vadano a quel paese</i> e sempre più convivenza e riconoscimento delle scelte di ciascuno: mi piace.

Ho scaricato per riconoscenza <a href="http://legesmotus.cs.brown.edu/" target="_blank">Leges M&#333;tus</a> (è un momento di fissazione per i giochi con possibilità di networking) e me lo provo stanotte prima di andare a dormire. Sotto le stelle la rete funziona meglio.