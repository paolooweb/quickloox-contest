---
title: "Un Arsenale di chiacchiere"
date: 2010-06-29
draft: false
tags: ["ping"]
---

Il sottoscritto si esibirà in una toccata e fuga, anzi due, nell'ambito del Venezia Camp 2010 in programma, chi lo avrebbe mai detto, a Venezia (precisamente all'Arsenale) dal primo al 3 luglio.

I mattinieri mi vedranno sbucare alle 11 a Tese 3 Galeazze per parlare di iPad e dell'accelerazione verso l'editoria digitale.

Ma c'è una <i>chance</i> anche per i dormiglioni, che possono trovarsi a Nappe Bricola per le 14:30 per la chiacchierata <a href="http://www.veneziacamp.it/varie/i-libri-2-0-per-intanto-si-stampano-ancora/" target="_blank">I libri 2.0 intanto si stampano ancora</a>.

Consiglio a tutti, gufi e allodole, di consultare <a href="http://www.veneziacamp.it/varie/i-libri-2-0-per-intanto-si-stampano-ancora/" target="_blank">il programma della manifestazione</a>, che nel giro dei tre giorni ha moltissimo da dire e da dare.

Se anche non interessano le chiacchiere su editoria digitale e iPad, apprezzerò moltissimo un saluto veloce da chi getterà un occhio oltre la porta di ingresso alla sala.