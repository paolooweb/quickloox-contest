---
title: "Questione di preferenze"
date: 2010-04-05
draft: false
tags: ["ping"]
---

Mi era sparito l'audio nello scorrere gli spezzoni della libreria di iMovie '09 e l'apposito Comando-K, che lo abilita e disabilita, non abilitava un bel niente.

L'aggiornamento a Mac OS X 10.6.3 non ha risolto il problema e cos&#236; provato a chiudere iMovie '09 per cancellare il file <code>com.apple.iMovie8.plist</code> nella cartella <code>~/Library/Preferences</code> (cioè nella libreria del mio spazio utente, non in quella che si vede aprendo il disco rigido).

Rilanciato iMovie, l'audio funziona. Servisse mai a qualcuno.