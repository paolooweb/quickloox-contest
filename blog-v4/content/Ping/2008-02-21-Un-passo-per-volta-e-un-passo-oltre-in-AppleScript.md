---
title: "Un passo alla volta, e un passo oltre, in AppleScript"
date: 2008-02-21
draft: false
tags: ["ping"]
---

Il computer è stupido, ma vincente nei lavori ripetitivi. Cambiare il nome a un file è stupido, ma cambiare il nome a mille documenti e farlo a mano è <em>più</em> stupido. Non mi soffermo sui programmi che risolvono l'esempio in questione (c'è anche qualche AppleScript di serie dentro Mac OS X che aiuta, dentro <code>/Libreria/Scripts</code>&#8230;), cos&#236; magari chi non se la sente di fare una ricerchina su Google sta qui e scopre come si crea un ciclo in AppleScript. :-)

Un ciclo è, come direbbe un fornaio o una lattaia, un insieme di istruzioni che si ripetono fino a che non è ora di smettere, ossia non si verifica una certa condizione. Un atto alla portata di un bambino di tre anni, contare fino a dieci, è un ciclo:

1) inizia a contare.
2) aggiungi uno al conteggio.
3) smetti di contare quando sei arrivato a dieci.

Il bambino di tre anni dà già per scontate un paio di cose che per il computer non lo sono. Riscrivo la procedura in modo che sia comprensibile persino a un computer:

0) parti&#8230; da zero.
1) inizia a contare.
2) aggiungi uno al conteggio.
3) se sei arrivato a dieci smetti di contare, altrimenti ripeti il punto 2).

E adesso, AppleScript. Apro Script Editor e scrivo le istruzioni una per una. Confrontale con i passi descritti qui sopra:

<code>set conteggio to 0</code>
(in italiano: imposta la variabile, cioè riempi il cassettino, chiamato <em>conteggio</em> con il valore zero)

<code>repeat until conteggio >= 10</code>
(ripeti fino a quando il valore della variabile <em>conteggio</em> non è maggiore o uguale a 10)

<code>set conteggio to conteggio + 1</code>
(imposta il valore della variabile <em>conteggio</em> allo stesso valore di prima più uno, un altro modo di dire <em>aggiungi uno al conteggio</em>. Questa istruzione è quella che viene ripetuta e infatti Script Editor la mostra indentata, sporgente dentro i confini di <code>repeat until</code> e <code>end repeat</code>)

<code>end repeat</code>
(fine ripetizione, per quando il conteggio avrà raggiunto il valore prefissato)

Pronto da incollare in Script Editor:

<code>set conteggio to 0</code>
<code>repeat until conteggio &#8805; 10</code>
<code>	set conteggio to conteggio + 1</code>
<code>end repeat</code>

Nota che il ciclo, ossia la ripetizione delle operazioni fino a che non viene soddisfatta la condizione posta, comprende tutto quello che sta tra i confini di <code>repeat until</code> e <code>end repeat</code>. Finora l'unico effetto pratico dello script è stato visualizzare il valore 10 nella parte bassa della finestra di Script Editor. Rendo un po' più visibile l'effetto dello script:

<code>set conteggio to 0</code>
<code>repeat until conteggio &#8805; 10</code>
<code>	set conteggio to conteggio + 1</code>
<code>	display dialog "Ora la variabile conteggio vale " &#38; conteggio</code>
<code>end repeat</code>

Il dialogo, se premi ogni volta il pulsante Ok, si ripeterà fino a chiusura del conteggio.

I minitutorial su AppleScript continuano ogni sette giorni.