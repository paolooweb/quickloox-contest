---
title: "Nel regno del fattibile / 6"
date: 2010-05-23
draft: false
tags: ["ping"]
---

<cite>pregasi elencare azioni fattibili su un iPad e non fattibili su un Sony Vaio W da 10&#8221;</cite>.

<a href="http://vimeo.com/11886557" target="_blank">Fissarlo con il Velcro</a> ovunque serva o faccia comodo.