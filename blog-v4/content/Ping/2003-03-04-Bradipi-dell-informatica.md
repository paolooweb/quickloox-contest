---
title: "Bradipi dell’informatica"
date: 2003-03-04
draft: false
tags: ["ping"]
---

Se non lo vedono, non c’è

Un numero clamorosamente alto di persone si lamenta del fatto che Safari, quando si passa con il mouse sopra un link, non mostri che link è.

Safari lo fa, accidenti; basta guardare i menu. View/Status Bar.

Un conto è volere, giustamente, un computer sempre più facile da usare. Un conto è essere bradipi dell’informatica, troppo pigri persino per sprecare tre minuti del proprio tempo a guardare dentro i menu. Non ho detto in un manuale di tremila pagine; ho detto nei menu.

<link>Lucio Bragagnolo</link>lux@mac.com