---
title: "Il treno dei desideri"
date: 2009-11-08
draft: false
tags: ["ping"]
---

<a href="http://www.gamestar.it/blogs/roots/" target="_blank">Paolo</a>, grande estimatore del simulatore di reti ferroviarie <a href="http://www.openttd.org/en/" target="_blank">OpenTtd</a>, mi segnala che gli sviluppatori del progetto hanno qualche problema con le versioni di Mac OS X.

I fatti sembrano indicare il superamento delle difficoltà; ho provato il gioco su Snow Leopard e pare funzionare perfettamente (servono <a href="http://nylon.net/ttd/ott.htm" target="_blank">file del gioco originale</a>, per fortuna liberamente disponibili, da infilare nella cartella del gioco), contrariamente ad alcune perplessità scritte nel blog degli autori a fine settembre.

Spero tuttavia che qualche sviluppatore conoscitore di Mac OS X abbia voglia e tempo di dare una mano al team di OpenTtd, eccellente realizzazione <i>open source</i> sulla scia del vecchio Transport Tycoon Deluxe. E, devo dire, messo a tutto schermo sul 17&#8221;, OpenTtd fa effetto niente male.