---
title: "Darwin e Darwin di più"
date: 2002-06-20
draft: false
tags: ["ping"]
---

Apple cambia strategia per rendere il suo lato OpenSource ancora più efficiente

Già era interessante che Mac OS X fosse il primo sistema operativo a pagamento a basarsi su un nucleo OpenSource. Adesso l’interesse diventa ancora maggiore.
Fino a ieri l’attività open source su Darwin era visibile ma non troppo, perchè una delle caratteristiche dell’open source è il continuo aggiornamento, ma Apple non può testare Mac OS X ogni settimana per vedere se l’ultimissimo cambiamento del kernel funziona come previsto. Così ha raddoppiato i punti di riferimento: adesso c’è una sezione Darwin sull’area del sito Apple dedicata agli <link>sviluppatori</link>http://developer.apple.com/darwin/, più focalizzata su Darwin come nucleo di Mac OS X, e il sito <link>OpenDarwin</link>http://opendarwin.org, dove le cose funzionano in modo più simile all’open source tradizionale.
Se tutto funziona come dovrebbe, Mac OS X dovrebbe sempre poter funzionare sopra la versione più aggiornata di OpenDarwin e questo significherà maggiore velocità di aggiornamento e soluzione dei bug. Brava Apple!

<link>Lucio Bragagnolo</link>lux@mac.com