---
title: "Illuminazione sonora"
date: 2006-04-15
draft: false
tags: ["ping"]
---

La mia casetta &egrave; un (piccolo) <em>open space</em>. Sono sul divano che maltratto il portatile e intanto l&rsquo;iMac di emergenza e di backup, altrimenti inutilizzato, dal tavolo lontano fornisce il sottofondo musicale.

Vorrei alzare il volume su <a href="http://www.jeanmicheljarre.com/" target="_blank">Jean-Michel Jarre</a> e in questo preciso istante ho capito il perch&eacute; di <a href="http://www.apple.com/it/imac/frontrow.html" target="_blank">Front Row</a>.