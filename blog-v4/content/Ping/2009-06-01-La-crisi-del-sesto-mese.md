---
title: "La crisi del sesto mese"
date: 2009-06-01
draft: false
tags: ["ping"]
---

Tra tante statistiche ovvie, una intrigante: Net Applications ha messo a confronto <a href="http://marketshare.hitslink.com/mobile-phones.aspx?qprid=62&amp;sample=38" target="_blank">le curve di crescita</a> del traffico web dei primi sei mesi di iPhone e dei primi sei mesi di Android.

Sono uguali, per cinque mesi. Nel sesto invece iPhone continua ad accelerare, mentre Android fa più fatica.

Diventa interessante ora vedere se Android ha solo goduto di un effetto-novità o se si riprende.