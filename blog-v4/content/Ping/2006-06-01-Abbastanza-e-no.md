---
title: "Abbastanza e no"
date: 2006-06-01
draft: false
tags: ["ping"]
---

Se non ci fosse questo post lo avrei voluto inventare. Un tizio si mette in testa che le chiamate a basso livello di una parte di Mac OS X sono pi&ugrave; lente di quelle di Linux di largo margine. Su Ridicolous Fish <a href="http://ridiculousfish.com/blog/archives/2006/05/16/36/" target="_blank">lo smontano pezzo per pezzo</a> e il tizio <a href="http://sekhon.berkeley.edu/macosx/" target="_blank">riaggiorna la sua pagina</a>, cancellando quello che aveva scritto prima, e naturalmente sostenendo che non ha pi&ugrave; ragione come prima, ma un po&rsquo; ha ragione lo stesso. Fino alla prossima smentita, chiaro.

Il tizio &egrave; uno splendido esempio di fauna descritta in modo mirabile nei commenti: <cite>knowing just enough to be dangerous, but not enough to actually solve the problem</cite>. Saperne abbastanza per essere pericoloso, ma non abbastanza per risolvere veramente il problema.

Ci vorrebbe un premio settimanale da assegnare.

Nel frattempo un grosso grazie a <a href="http://www.rabellino.it/blog/" target="_blank">Gianugo</a> per la segnalazione!