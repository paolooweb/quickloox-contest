---
title: "Il primo Mac portatile della nuova era"
date: 2006-01-10
draft: false
tags: ["ping"]
---

C&rsquo;è anche il portatile: Mac Book Pro. Per ora un modello da 15,4 pollici di schermo, sottile un capello meno del 17&rdquo; attuale. Con iSight incorporata e due chili e mezzo di peso.

Tutte specifiche che non hanno importanza storica, a differenza del fatto che dentro c&rsquo;è un processore Intel Core Duo.

Da qualche parte qualcuno sta aggiornando il suo sito scrivendo <cite>neanche stavolta Apple ha presentato il PowerBook G5 tanto atteso</cite>.