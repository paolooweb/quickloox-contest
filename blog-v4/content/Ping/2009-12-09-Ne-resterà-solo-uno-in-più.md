---
title: "Ne resterà solo uno (in più)"
date: 2009-12-09
draft: false
tags: ["ping"]
---

Curiosando su <a href="http://freshmeat.net/" target="_blank">Freshmeat</a>, scopro che esiste <a href="http://adonthell.linuxgames.com/" target="_blank">Adonthell</a>, giochino ancora imberbe ma in rapida maturazione. È un po' lento e poco rifinito, ma può già incuriosire.

Scopro che <a href="http://adonthell.linuxgames.com/download/osx.shtml" target="_blank">esiste un eseguibile per Mac OS X</a>, scaricabile gratuitamente, compatibile come minimo con Leopard e Snow Leopard.

Guardo su <a href="http://www.macupdate.com" target="_blank">MacUpdate</a> e <a href="http://www.versiontracker.com" target="_blank">VersionTracker</a> e non c'è traccia del programma.

C'è sempre almeno un software in più di quanto si racconti.