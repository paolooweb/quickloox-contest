---
title: "Un anello mancante"
date: 2008-08-07
draft: false
tags: ["ping"]
---

<a href="http://aurelio.net/soft/adiumbook/" target="_blank">AdiumBook</a>. Mette in comunicazione il programma di <em>chat</em> Adium con la Rubrica Indirizzi di Mac OS X. Ed è <em>open source</em> e gratuito come <a href="http://www.adiumx.com" target="_blank">Adium</a>.

Per me è già imprescindibile.