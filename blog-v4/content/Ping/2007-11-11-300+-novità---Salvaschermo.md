---
title: "300+ novità: Salvaschermo"
date: 2007-11-11
draft: false
tags: ["ping"]
---

Fa sorridere trattare come <a href="http://www.apple.com/it/macosx/features/300.html#screensavers" target="_blank">novità di Leopard</a> anche l'inserimento di nuovi salvaschermi. Eppure c'è un pubblico di feticisti (il sottoscritto) che magari ogni tanto ne carica uno nuovo, per vedere l'effetto che fa, specie ora che i salvaschermo riguardano solo il decoro e non salvano proprio niente.

Leopard inserisce gli Arabeschi, le Conchiglie, la Parola del giorno (in inglese&#8230;). Può sovraimporre a qualunque salvaschermo un orologio, oppure attingere agli album di iPhoto per ricavarne collage, oppure farne mosaici.

Ed è tutto. Non dirlo a nessuno, ma adesso vado su <a href="http://www.macupdate.com" target="_blank">MacUpdate</a> e guardo se c'è &#8220;roba&#8221; nuova&#8230;