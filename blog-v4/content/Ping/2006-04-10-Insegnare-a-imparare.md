---
title: "Insegnare a imparare"
date: 2006-04-10
draft: false
tags: ["ping"]
---

<strong>Yoghi Bear</strong> mi ha scritto:

<cite>Sono ricapitato per caso oggi su <a href="http://www.pagetutor.com/pagetutor/makapage/index.html" target="_blank">questa pagina</a> dopo 2 anni. Pu&ograve; sembrare una pagina stupida vedendola cos&igrave; ma &egrave; il miglior tutorial per imparare a scrivere Html e capire quello che si fa per chi non ne sa proprio niente.</cite>

In questo senso, che possa magari sembrare una pagina stupida &egrave; il miglior complimento. Grazie!