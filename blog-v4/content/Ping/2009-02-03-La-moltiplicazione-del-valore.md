---
title: "La moltiplicazione del valore"
date: 2009-02-03
draft: false
tags: ["ping"]
---

Jeff Holden, amministratore delegato di Pelago, si è fatto due conti dietro un fazzoletto di carta.

Chi sia Jeff Holden e che cosa faccia <a href="http://www.pelago.com" target="_blank">Pelago</a> è di importanza relativa. I suoi <a href="http://www.businessweek.com/technology/ByteOfTheApple/blog/archives/2009/01/the_app_store_s.html" target="_blank">conti da fazzoletto</a>, invece spiegano che, considerati gli iPhone esistenti, e il numero di programmi scaricati, e quello che avviene nel mondo cellulare, per un programmatore pubblicare un'applicazione su App Store per 14 milioni di iPhone equivale, come effetto sul pubblico, a pubblicare lo stesso programma per 1,6 miliardi di cellulari.

Appare evidente che il programmatore non ci pensa due volte e prima pubblica su App Store. poi valuta se sprecarsi in giro.

E che App Store è la vera, grande innovazione di tutto il marchingegno, capace di moltiplicarne il valore in maniera perfino esagerata.