---
title: "Chi si chiude si inchioda"
date: 2002-01-02
draft: false
tags: ["ping"]
---

Pensare che la pirateria discografica si combatta proibendo la copia onesta è moralmente disonesto

Le aziende discografiche si illudono che vendere dischi protetti contro le copie ucciderà la pirateria.
Ultimamente Universal Music ha annunciato che entro metà 2002 tutti i suoi Cd saranno protetti contro la copia e illeggibili tra l’altro su lettori Dvd, PlayStation 2 e computer Macintosh.
Piratare è illegale. Creare una copia per uso personale - da sentire, diciamo, su un iPod - è legalissimo, invece.
Se vogliono eliminare il mio diritto alla copia personale mi aspetto che, in cambio, i Cd costino meno.
Se non sarà così, eviterò comodamente di comprare Cd marcati Universal.
E tanti cominceranno a usare Gnutella. Sistema che, al contrario di Napster, non è arrestabile.
Ogni chiusura da parte delle aziende discografiche è un chiodo piantato nella bara del commercio convenzionale di musica, ma ci metteranno un po’ a capirlo.

lux@mac.com