---
title: "Blog alla geek<p>"
date: 2005-06-08
draft: false
tags: ["ping"]
---

C&rsquo;è chi si diverte a fare tutto nel modo più difficile<p>

Un <em>geek</em> è un impallinato della tecnologia e della programmazione; un <em>blog</em> è un diario giornaliero pubblicato su Internet.<p>

Che dire dunque di <a href="http://nanoblogger.sourceforge.net/">Nanoblogger</a>, un sistema completo open source per amministrare blog direttamente dal Terminale?<p>

Io non proverei neanche a cominciare. Ma per qualcuno scegliere la via più scomoda è un modo per imparare e, talvolta, quasi una forma d&rsquo;arte.<p>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>