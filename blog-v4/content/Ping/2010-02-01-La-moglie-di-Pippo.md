---
title: "La moglie di Pippo"
date: 2010-02-01
draft: false
tags: ["ping"]
---

Il vecchio mondo, quelli che l'informatica come era una volta. Una volta lo hardware era un lusso e ogni avanzamento hardware era di per se stesso positivo a prescindere; significava automaticamente migliori prestazioni e migliore esperienza di uso, spesso a incrementi di decine di punti percentuali.

Il nuovo mondo, quelli che lo hardware oramai è un fatto scontato e per niente decisivo. Non è più un lusso, semmai una opportunità, e l'avanzamento hardware è decisivo solo marginalmente. Le prestazioni sono migliori ma di pochi punti percentuali, cioè irrilevanti nel 99 percento degli usi. Quello che conta sono il software, i servizi a disposizione, le possibilità che si aprono.

Conseguenza: molto spesso quelli del vecchio mondo pensano automaticamente che, se lo hardware non va come vogliono loro, allora non c'è progresso. A prescindere.

La riprova è che sento lamentare il fatto che i portatili Mac hanno un limite a otto gigabyte di Ram e che ce ne vorrebbero sedici, perché qualcun altro ne ha sedici e quindi o ne hai sedici o sei &#8220;indietro&#8221;.

Sono famoso per lasciare tutto aperto, accumulare applicazioni, saturare qualsiasi quantità di Ram disponibile. Guardo il mio MacBook Pro 17&#8221; con otto gigabyte di Ram.

Sono aperti ora: Finder, BBEdit, Vienna, Skype, Angband, iChat, Adium, Mailsmith, SpamSieve, Anteprima, Safari, X-Chat Aqua, Terminale, Openoffice.org, Bean, Dizionario, Snippets, Automator, Dashcode, InDesign, iCal, QuickTime Player, Calibre, AppleScript Editor, TextEdit, iTunes, iPhoto, Rubrica Indirizzi e naturalmente Monitoraggio Attività.

Sono ventinove applicazioni, se non erro. Il sistema non si spegne da quasi sei giorni. In questo momento sono ancora liberi seicento megabyte di Ram e lo <i>swap</i> è a livello fisiologico. Prova provata: aggiungo World of Warcraft che, come trentesima applicazione, con tutti i parametri grafici al massimo, funziona fluidamente. Garantisco che se c'è <i>swap</i>, in queste condizioni, WoW scatta e impone la chiusura di qualche applicazione.

Se avessi sedici gigabyte di Ram, in queste condizioni ne starei sprecando otto. E le vedo come condizioni limite. Se davvero avessi bisogno di tutte le risorse, chiuderei ventotto applicazioni per lasciarne in piedi una importante.

Ma su Photoshop, mi dicono, con il video, mi dicono, la Ram non basta mai. Certo, ma siamo su un portatile. Se si sta facendo questione di prestazioni, siamo automaticamente nel posto sbagliato. Dovremmo stare almeno su un iMac o su un Mac Pro, dove si installano tranquillamente molto più che otto gigabyte di Ram (trentadue gigabyte sul modello più avanzato). E garantisco che, uscisse domani un MacBook Pro con sedici gigabyte di Ram, Photoshop andrebbe più piano che su un Mac Pro con otto gigabyte di memoria. Le architetture non sono uno scherzo e la miniaturizzazione ha i suoi compromessi.

Concludo. Tra un anno, due, tre, non lo so, serviranno sedici gigabyte di Ram e i MacBook Pro faranno bene a montarla. Oggi sono inutili. Reclamarne sedici è in nome delle abitudini mentali degli anni novanta è una moglie di Pippo. Non Clarabella.