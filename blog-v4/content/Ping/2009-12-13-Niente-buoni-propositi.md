---
title: "Niente buoni propositi"
date: 2009-12-13
draft: false
tags: ["ping"]
---

Mi hanno regalato il primo volume di <a href="http://www-cs-faculty.stanford.edu/~knuth/taocp.html" target="_blank">The Art of Computer Programming di Donald Knuth</a>, la Bibbia della programmazione.

Regalo fantastico: l'ho sempre sognato e non ho mai osato.

Regalo terribile: a leggerlo e comprenderlo, vale mezza dozzina di esami universitari importanti.

Non ho bisogno di buoni propositi per il 2010. Semmai di tutte le mie (scarse) capacità.