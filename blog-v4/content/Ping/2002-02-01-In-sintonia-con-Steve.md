---
title: "In sintonia con Steve"
date: 2002-02-01
draft: false
tags: ["ping"]
---

Dati interessanti sugli interessati al discorso di Jobs per l’apertura del Macworld di San Francisco

Stando ad Apple, il nuovo iMac presentato dal Ceo di Apple nel suo keynote (discorso di apertura) l’hanno vista simultaneamente in oltre 81 mila, quasi il doppio di quanti avevano fatto lo stesso a luglio per l’edizione di New York. Il webcast, durato due ore, ha finito per erogare undici terabyte di dati (arrotondando, diciamo una ventina di dischi rigidi da sessanta gigabyte) a oltre 160 mila visitatori unici. Nel momento di massimo ascolto, venivano trasmessi più di sedici gigabit per secondo di stream video.
La quantità di pubblico è sicuramente un dato notevole; ma, pensando che di keynote e di Steve Jobs in quel momento su Internet ce n’era sicuramente uno solo, bisogna concluderne che lo share deve essere stato eccezionale. Come dire, tutti in sintonia con Steve. Anche, quasi sempre, sui contenuti.

lux@mac.com