---
title: "Il plurale di aneddoto<p>"
date: 2005-09-30
draft: false
tags: ["ping"]
---

La statistica funziona per numeri abbastanza grandi<p>

Ha deciso che per il momento non passa a Tiger. Non funziona tanto bene.<p>

Gli chiedo che cosa ha scoperto. Uso Tiger da cinque mesi ma c&rsquo;è sempre da imparare.<p>

Risponde che glielo ha detto un suo amico.<p>

Capisco. Quindi Tiger non funziona a qualche milione di persone perché l&rsquo;amico ha detto bla bla bla.<p>

Prossimamente, due righe sugli esperti di dischi rigidi che schifano Maxtor, o Seagate, o Western Digital, o Ibm, o Hitachi, o boh, perché se ne sono rotti due, tre, cinque.<p>

È una reazione umana. Non comprerò mai più un cordless Siemens. Ma dire che i cordless Siemens non funzionano, perché non ha funzionato il mio, è una scempiaggine.<p>

Le statistiche si fanno su campioni significativi. E il plurale di aneddoto non è dati.<p>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>