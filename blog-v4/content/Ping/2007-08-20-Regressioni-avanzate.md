---
title: "Regressioni avanzate"
date: 2007-08-20
draft: false
tags: ["ping"]
---

Nel preparare lo speciale <em>switching</em> di Macworld ottobre ho scritto un tomo lungo come mezza enciclopedia sulle suite disponibili per Mac. Intanto, se il concetto di suite è allargato un minimo, la possibilità di scelta è indecente.

Secondo, mi sono fatto un punto d'onore di riuscire a installare su Mac OS X i fogli elettronici più incredibili possibile. Ho trovato <a href="http://jo.irisson.free.fr/?p=29" target="_blank">una dritta</a> per installare <a href="http://www.gnome.org/projects/gnumeric/" target="_blank">Gnumeric</a>, che non ci credevo.

Non sono ancora riuscito a installare un foglio di calcolo su emacs, ma ho sul disco tre candidati possibili. Sei avvisato. :-)