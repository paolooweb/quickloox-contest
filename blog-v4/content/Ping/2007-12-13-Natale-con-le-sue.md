---
title: "Natale con le sue"
date: 2007-12-13
draft: false
tags: ["ping"]
---

Un Natale senza luci, dalle candele in su, non è.

Mi mancava una cosa in Dashboard e ho finalmente trovato <a href="http://www.interdimensionmedia.com/widgets/?widget=lights" target="_blank">Festive Lights</a>, che è carino e destina metà delle donazioni a una buona causa.

Il bello è che l'ho trovato sul Cd di Macworld dicembre. Attenzione, però: la versione Leopard-compatibile è stata presentata il 2 dicembre, dunque sul Cd non ci poteva essere. I leopardati passino direttamente al sito.

Se ci scappa una donazione, anche piccina, sarà un Natale ancora più luminoso.