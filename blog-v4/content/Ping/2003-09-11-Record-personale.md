---
title: "Record personale"
date: 2003-09-11
draft: false
tags: ["ping"]
---

Anche togliendo il trucco, Mac OS X è di una stabilità da competizione

Lo dico subito: c’è il trucco. Infatti vanno contati una ventina di giorni di vacanze in cui il computer è rimasto in stop e inutilizzato (ma si è svegliato regolarmente al momento giusto). Ma ho fatto ugualmente il record personale.

L’uptime del mio Titanium è in questo momento di 44 giorni, 15 ore e 25 minuti. A causa di un libro che sto per finire di tradurre non monto Mac OS X 10.2, ma una prerelease di 10.3 con vari problemi (per esempio Anteprima funziona solo in inglese), intrinsecamente meno stabile della versione ufficiale e garantita.

Ieri sono arrivato ad avere 33 applicazioni aperte nel Dock, alcune in italiano e alcune in inglese, compreso Classic, intanto che scaricavo shareware dal browser, svolgevo una sessione di ftp dal Terminale e installavo un nuovo package, il tutto ritirando la posta, con aperti Proteus e LimeWire.

Mac OS è sempre stato mediamente abbastanza stabile. Ma questo è il paradiso, Anzi, per paradosso, meglio fare noi almeno un logout ogni tanto.

<link>Lucio Bragagnolo</link>lux@mac.com


