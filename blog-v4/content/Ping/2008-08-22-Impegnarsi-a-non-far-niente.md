---
title: "Impegnarsi a non far niente"
date: 2008-08-22
draft: false
tags: ["ping"]
---

Ho mantenuto una vecchia promessa e ho installato <a href="http://boinc.berkeley.edu" target="_blank">Boinc</a>, il programma che sfrutta il tempo non utilizzato del computer per contribuire a progetti scientifici che richiedono potenza di calcolo indefinita (più ce n'è meglio è).

La vecchia promessa era tornare <a href="http://www.poc.it/stories/continua.php?cod=2073" target="_blank">nel gruppo del Poc</a> che partecipa a <a href="http://setiathome.berkeley.edu/team_display.php?teamid=33423" target="_blank">Seti@home</a>, il progetto di ricerca di segnali intelligenti nel cosmo. Per il momento ho esteso la mia partecipazione anche a <a href="http://einstein.phys.uwm.edu/" target="_blank">Einstein@home</a>, che cerca stelle di neutroni nello spazio, ma non escludo di aggiungere ancora qualcos'altro. L'interfaccia è semplice, i problemi sono tanti e lo stato di nullafacenza del computer è superiore a quanto si pensa, anche per chi spegne e mette in stop.