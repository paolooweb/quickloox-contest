---
title: "L’angelo nei dettagli"
date: 2002-04-16
draft: false
tags: ["ping"]
---

Imac (Flat Panel) è candido come la neve, d’accordo, ma c’è di più

Da ieri la mia fidanzata è felice proprietaria di un iMac (Flat Panel) e ho avuto così il piacere (e il dovere) di installare e configurare il tutto.
Beh; d’accordo il SuperDrive, d’accordo gli Apple Pro Speakers (impressionanti), d’accordo la velocità, d’accordo lo schermo Lcd orientabile. Ma nessuna recensione descrive una serie di piccole cose che dicono molto di chi fabbrica il computer. E di chi lo usa.
Il cavo Usb della tastiera ha una protezione per il connettore che resta attaccata al cavo; i dischi di Software Restore riportano icone speciali perché si possa capirne la successione a prescindere dalla lingua parlata; nella confezione trovano posto un panno per spolverare lo schermo Lcd, due Cd vergini e un Dvd vergine, per provare subito le meraviglie del SuperDrive. E altro.
Il diavolo sta nei dettagli, si dice; invece nei dettagli del nuovo iMac sta tanta, tanta cura. Sarà per questo che è candido come un angioletto.

<link>Lucio Bragagnolo</link>lux@mac.com