---
title: "Il fine settimana di un evangelista"
date: 2006-10-28
draft: false
tags: ["ping"]
---

Il titolo lo ha dato <strong>Piergiovanni</strong> e pure il testo:

<cite>Mentre è iniziato ufficialmente il countdown per il grande ponte di ognissanti &#8212; che qui da noi, complice la festa patronale di san Saturno, inizia domenica &#8212; io sto facendo il Guy Kawasaki della situazione, ovviamente in millesimo. Dopo aver portato sulla retta via due colleghe, una di matematica, che ieri si è presentata con un iBook da 12&#8221; vecchio modello, oggi è toccato al mio amico e collega Antonio, un insegnante di musica. Antonio usa il Pc ma, dopo che gli ho raccontato di come funziona il Mac e dopo aver visto l'iBook della collega, ha preteso che lo accompagnassi al centro Apple a comprare un MacBook Pro. Ma non solo, vuole che mi occupi io stesso di un eventuale ordinativo che la scuola farà allo stesso centro, per un certo numero di MacBook. La scuola dispone infatti già di una sala di montaggio video con diversi G4 Quicksilver e due G5, e ora vogliono prendere i portatili per creare un laboratorio mobile. Merito, o colpa, mia.
</cite>

Non è sempre stato cos&#236; e non sarà sempre cos&#236;. Ma questa è proprio un'epoca in cui a non prendere un Mac si fa per definizione un acquisto sbagliato.