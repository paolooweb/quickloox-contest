---
title: "Interrotto uno, contenti tutti"
date: 2008-06-02
draft: false
tags: ["ping"]
---

Lo segnala <a href="http://multifinder.wordpress.com" target="_blank">Mario</a> e John Gruber mi perdonerà se <a href="http://daringfireball.net/linked/2008/june#mon-02-nack" target="_blank"> rendo in italiano</a> la sua analisi.

<cite><a href="http://blogs.adobe.com/jnack/2008/06/some_thoughts_about_platform_consistency.html" target="_blank">John Nack</a> a proposito del dilemma di Adobe relativamente alle scorciatoie di tastiera in Photoshop come Comando-H: Photoshop ha usato Comando-H per nascondere la selezione dall’inizio degli anni Novanta ma, a partire da Mac OS X 10.0.0, Apple ha definito la stessa scorciatoia come standard di sistema per naascondere l’applicazione in primo piano. Se ti attieni alla tradizione il programma confonde i nuovi utenti (che si aspettano di usare Comando-H per nascondere l’applicazione); se cambi, levi il tappeto da sotto i piedi alla tua base di utenza più longeva.</cite>

<cite>Bare Bones ha risolto questo problema perfettamente con BBEdit, che tradizionalmente usava Comando-H per ritrovare in ricerca il testo selezionato. Quello che fanno è intercettare Comando-H la prima volta che viene usato in Mac OS X e mostrare una finestra di dialogo che chiede in che modo usare il comando, se secondo le regole di Mac OS X o secondo quelle di BBEdit. Una interruzione, una volta sola, tutti contenti.</cite>