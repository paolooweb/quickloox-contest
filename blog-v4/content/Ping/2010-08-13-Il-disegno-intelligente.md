---
title: "Il disegno intelligente"
date: 2010-08-13
draft: false
tags: ["ping"]
---

Ricevo dall'amico <a href="http://web.me.com/radioqueequeg/Il_Peluod/Chiamatemi_Ismaele.html" target="_blank">Pelo</a>:

<cite>Gipi è il fumettista del momento in Italia. Ospite in televisione, vince premi, lo pubblicano in Francia (tipo l'America del fumetto colto&#8230;) e insomma va per la maggiore.</cite>

Beh, Gipi ha pubblicato un pezzo sul Post che ha per sottotitolo <a href="http://www.ilpost.it/2010/07/19/gipi-disegnare-con-ipad-brushes/" target="_blank">Gipi racconta com'è disegnare con iPad, e parecchie altre cose</a>.

Per una volta qualcosa in italiano, tutto da leggere.

Non condivido la conclusione beffarda e ingannatrice, non perché ci sia di mezzo iPad, ma perché i miei pensieri sul digitale sono diversi. Eppure sono contento di averla letta. Spero la leggano tanti, assieme al resto.