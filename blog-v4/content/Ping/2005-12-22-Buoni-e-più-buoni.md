---
title: "Buoni e più buoni<p>"
date: 2005-12-22
draft: false
tags: ["ping"]
---

Storie natalizie di aviazione e di programmazione<p>

Lascio lo spazio di oggi alla storia di <strong>Odino</strong>, che merita in sé ed è anche appropriata per il tempo del Natale. Devo lasciare i riferimenti a me, altrimenti non si capirebbe bene l&rsquo;accaduto. Ma certo non ho alcun merito, tranne l&rsquo;essermi trovato casualmente su iChat in quel momento.<p>

<cite>Il mese scorso ho chiesto aiuto a Lucio perché volevo realizzare un programma in AppleScript che convertisse il mio turno di lavoro mensile dal generico formato testo (fornitomi dall&rsquo;azienda) in iCal, allo scopo di poter gestire i voli come se fossero appuntamenti.</cite><p>

<cite>Lux mi ha consigliato di rivolgermi al Forum di Tevac dove avrei probabilmente trovato qualcuno disposto a darmi una mano; e anche se un po&rsquo; scettico ho seguito il suo consiglio.</cite><p>

<cite>Beh, incredibile; il giorno dopo la mia richiesta era già stata accolta.</cite><p>

<cite>Ho quindi inserito l'indirizzo .mac del mio sconosciuto buon samaritano in iChat e, bingo!, era online. Così ho iniziato una simpatica chat riguardante AppleScript e come fare per risolvere il mio problema.</cite><p>

<cite>Per farla breve, dopo qualche giorno reincontro l&rsquo;amico online e con mio grande stupore scopro che aveva già scritto parecchie pagine di codice risolvendo il mio problema.</cite><p>

<cite>La cosa mi ha lasciato piacevolmente basito perché è stata fatta veramente con lo spirito giusto, oserei dire lo spirito di un Mac Scout.</cite><p>

<cite>Ora i turni di lavoro miei e di parecchi miei colleghi sono su iCal e tramite iSync su telefonini e palmari.</cite><p>

<cite>Grazie, Federico, e Auguri a nome di tutto L&rsquo;AZ Mac Club.</cite><p>

La tecnologia è importante: iChat, AppleScript, iCal, ma la vera morale, credo, è che nel mondo Mac i buoni programmatori sono anche programmatori buoni.<p>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>