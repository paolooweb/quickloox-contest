---
title: "Nessun disco è straniero. O no?"
date: 2002-01-15
draft: false
tags: ["ping"]
---

Fino a nuovo ordine, diffidare dei nomi di disco troppo arzigogolati. Perché Apple...

Mac OS X Hints raccomanda vivamente agli utilizzatori di Mac OS X di fare una certa attenzione ai nomi dei dischi, perché Apple non utilizza le giuste precauzioni per evitare che in rarissimi (ma disastrosi) casi un installer possa arrecare danni al disco rigido.
È ciò che è successo al tremendo installatore di iTunes 2.0, subito ritirato e sostituito (adesso siamo ancora più avanti, alla versione 2.0.3) ma che ha fatto in tempo a fare saltare un paio di dischi rigidi caratterizzati da una combinazione veramente infelice di spazi e vocaboli che si sono rivelati armi a doppio taglio.
Se possibile, evitare di usare spazi e caratteri strani in genere; limitarsi a lettere e numeri.
E intanto chiedere ad Apple di preparare un po’ meglio installer Unix capaci di discriminare pochi dischi. Ma buoni lo stesso per chi li usa.

lux@mac.com

<http://www.macosxhints.com>