---
title: "Trova lo sciacallo"
date: 2007-07-05
draft: false
tags: ["ping"]
---

Un quotidiano inglese dice che, siccome iTunes è partito in tre Paesi europei prima di partire in tutta Europa, <a href="http://www.ft.com/cms/s/63efca12-2a6d-11dc-9208-000b5df10621.html" target="_blank">anche iPhone potrebbe</a> partire in quei soli tre Paesi, tra cui l'Inghilterra. Gli altri, 2008, forse.

Un possibile acquirente inglese di iPhone legge l'ipotesi e pensa: bene, appena arriva iPhone me lo compro.

L'edizione tedesca del quotidiano inglese dice più o meno <a href="http://ftd.de/technik/it_telekommunikation/:L%E4ndern%20Europas/222091.html" target="_blank">la stessa cosa</a>.

Un possibile acquirente tedesco di iPhone legge l'ipotesi e pensa: bene, appena arriva iPhone me lo compro.

Un quotidiano italiano, invece di fare informazione, fa scopiazzamento e riprende il <em>gossip</em>.

Un possibile acquirente italiano di iPhone legge l'ipotesi e pensa: eh, boh, peccato, fa niente, invece di aspettare iPhone vado a comprarmi subito un altro telefono.

Dei tre quotidiani che riferiscono voci invece di fatti, qual è lo sciacallo?