---
title: "Divido et impero"
date: 2009-01-15
draft: false
tags: ["ping"]
---

L'iMac sta ancora andando da Capodanno, in teoria copiando un file da oltre quattro gigabyte su iDisk. Secondo me è tempo perso, ma stavolta voglio vedere se finisce, in qualunque modo, da solo.

Nel frattempo ho provveduto a spezzare il file in cinque parti, ognuna delle quali più piccola di un giga e quindi inviabile con l'edizione gratuita di <a href="http://www.pando.com" target="_blank">Pando</a>.

Per spezzarlo ho deciso di sfruttare il Terminale, che contiene il comando <code>split</code>. L'utilizzo è banale o cos&#236; pare:

<code>split -b 889m nomefile</code>

suddivide il file in pezzi da 899 megabyte l'uno. Come sempre, <code>man split</code> spiega tutte le possibilità a disposizione.

Se tutto funzionerà, al destinatario dovrebbe bastare digitare

<code>cat pezzo1 pezzo2 pezzo3 pezzo4 pezzo5 > nomefile_originale</code>

per avere il file completo e a posto.

<code>cat</code> concatena più file uno dietro l'altro. L'operatore <code>></code> effettua un ridirezionamento, cioè prende il risultato dell'operazione precedente (nel caso quella di <code>cat</code>) e, invece che lasciare che arrivi sullo schermo come fanno normalmente i risultati dei comandi da Terminale, lo dirotta verso un file che si chiama come gli viene detto.

Se si fosse trattato di una immagine disco (purché non di tipo <em>sparse</em>), si sarebbe potuto usare il comando <code>hdiutil</code>, che arriva nel cuore Unix di Mac OS X direttamente da Apple. La sua sintassi è complicata, ma descritta nel solito <code>man hdiutil</code>. Si può vedere <a href="http://developer.apple.com/DOCUMENTATION/Darwin/Reference/ManPages/man1/hdiutil.1.html" target="_blank">anche via Internet</a>.

<em>Divido et impero</em>. Spero.