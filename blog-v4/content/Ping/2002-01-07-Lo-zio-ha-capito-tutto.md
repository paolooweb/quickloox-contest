---
title: "Lo zio ha capito tutto"
date: 2002-01-07
draft: false
tags: ["ping"]
---

Per giudicare un’interfaccia a volte è più che sufficiente usarla. Per esempio, Windows Xp...

Lo zio Giorgio, settant’anni ad aprile ottimamente portati, è appena passato dopo anni di Mac OS 9.1 a un Pc equipaggiato con Windows Xp Home.
Sono stato a casa sua ad attivargli la connessione Internet: lui è bravissimo a gestire la sua vasta collezione di francobolli (prima su AppleWorks e, suppongo, da adesso in poi su FileMaker Pro), ma per il resto si definisce un principiante.
Chiacchierando del più e del meno mi ha confidato: “Certo che questo Windows è più complesso... e nelle finestre di dialogo c’è un sacco di roba inutile in più”.
Non credo sia veramente un principiante; ha già capito tutto.

lux@mac.com