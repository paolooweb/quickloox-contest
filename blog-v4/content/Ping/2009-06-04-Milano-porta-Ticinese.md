---
title: "Jurassic Park"
date: 2009-06-04
draft: false
tags: ["ping"]
---

Milano, porta Ticinese. Per terra, in strada, qualcosa di simile al ritrovamento del cranio di un dinosauro.

Non tutto il software su floppy disk, evidentemente, è andato in fumo.

La Sovrintendenza ai beni culturali ha ignorato le mie segnalazioni.