---
title: "Colpetti di genio"
date: 2008-02-08
draft: false
tags: ["ping"]
---

Non pensavo neanche che fosse possibile. Invece <a href="http://fluidapp.com/" target="_blank">Fluid</a> promette di fare funzionare ogni singola applicazione Web (come Gmail o Flickr) come se fosse sul desktop invece che dentro il browser.

Cos&#236;, se per caso salta in aria una delle applicazioni, non se ne vanno anche le altre, contrariamente a quanto avviene facendo funzionare tutto dentro il browser.

Devo ancora collaudare, ma il principio è geniale.