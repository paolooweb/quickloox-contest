---
title: "Grandi scimmie"
date: 2010-02-25
draft: false
tags: ["ping"]
---

<a href="http://www.ilmacaco.com/" target="_blank">Il Macaco</a> mi informa di avere attivato <a href="http://www.ilmacaco.com/answers/" target="_blank">Il Macaco Answers</a>, un posto dove fare domande e ricevere risposte in ambito Mac più efficace dei disperanti forum e più umano di certi supporti tecnici.

Non posso che segnalarne la partenza e fargli i migliori auguri. :-)