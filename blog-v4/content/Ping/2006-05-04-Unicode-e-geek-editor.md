---
title: "Unicode e geek editor"
date: 2006-05-04
draft: false
tags: ["ping"]
---

Invece di <em>text editor</em>&hellip; comunque sia: ho scaricato <a href="http://www.gigamonkeys.com/lispbox/" target="_blank">Lispbox</a> nel corso di uno dei miei ennesimi tentativi di imparare un po&rsquo; di Lisp.

Nel pacchetto c&rsquo;&egrave; una edizione di Emacs che &egrave; miracolosamente gi&agrave; configurata per accettare tranquillamente tutti i caratteri accentati e tipografici che ho tentato finora.

Per quanto riguarda il Lisp, sto seguendo i tutorial e (defun)gendo, per chi coglie. :-)