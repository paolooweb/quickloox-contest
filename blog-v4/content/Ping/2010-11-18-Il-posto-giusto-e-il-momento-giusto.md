---
title: "Il posto giusto e il momento giusto"
date: 2010-11-18
draft: false
tags: ["ping"]
---

Esco per trascorrere una giornata ricca di appuntamenti e quindi di produzione tendente a zero.

Però mi servirà stendere quattro righe quattro di AppleScript. Quindi produzione zero, ma nello zaino ci va il MacBook Pro 17&#8221; da tre chili e guai a chi lo tocca.

In un'altra riunione viene detta una solenne sciocchezza su indici di borsa, valori di chiusura, percentuali. Pausa caffè di cinque minuti. La passo su Numbers con iPad, che all'atto pratico non so ancora veramente usare al meglio, e ne ricavo un piccolo foglio di calcolo, colorato, con frecce e testo illustrativo, che in una schermata mostra come stanno le cose. In modo del tutto impraticabile per il più leggero dei MacBook Air la tavoletta, a mo' di foglio di carta, fa il giro degli astanti. All'unanimità la sciocchezza viene respinta.

Più tardi, in treno, una ragazza legge veramente, con attenzione, per molte fermate e molte schermate. Con un iPod touch.

Chiunque ti voglia insegnare quale sia lo strumento giusto in assoluto per fare qualcosa è un emerito. Aggiungi un aggettivo a piacere.