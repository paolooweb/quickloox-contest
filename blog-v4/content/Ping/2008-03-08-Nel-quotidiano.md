---
title: "Nel quotidiano"
date: 2008-03-08
draft: false
tags: ["ping"]
---

<a href="http://morrick.wordpress.com" target="_blank">Riccardo</a> ha aumentato notevolmente la frequenza di scrittura e ha mantenuto intatta la qualità e la profondità dell'analisi.

L'ho promosso da settimanale a quotidiano nell’elenco personale delle letture indispensabili.