---
title: "Piccolo è bolla"
date: 2009-03-28
draft: false
tags: ["ping"]
---

Un carissimo amico mi ha chiesto assistenza per installare Mac OS X su un <em>netbook</em> Msi Wind e mi sono prestato. È stata una occasione per toccare con mano la nuova tendenza.

Non è per me. Intendiamoci: piccolo, leggero, perfetto per guardarsi la posta elettronica in tenda durante il <em>weekend</em> di <em>trekking</em>. Mi viene da dire che un iPhone è ancora più piccolo e più leggero e guarda la posta ugualmente, ma non lo dirò. Non è questo il punto.

Il punto è la <em>trackpad</em> dei Puffi, con il pulsantino sottile sottile che devi cercare ogni volta. Lo schermo che qualunque cosa fai resta sempre fuori qualcosa. Il processore che clicchi una casella e passa un piccolo pesantissimo istante prima di vedere il segno di spunta. Ripeto, perfetto da infilare nella borsa della videocamera per la gita fuori porta. Ma appena torno a casa finisce sullo scaffale e datemi un computer vero.

Oltretutto i <em>netbook</em> dovrebbero essere l'apoteosi della portabilità, il <em>computing</em> sempre e dovunque, la soluzione geniale. Se la batteria non fosse da ridere. Leggerissimo nello zainetto il <em>netbook</em>, ma meglio che la gita finisca in fretta vicino a una presa di corrente o hai chiuso. E allora suona un po' come presa in giro. Una macchina con lo schermo minuscolo e il processore giocattolo fatta per la massima portabilità dovrebbe durare una settimana con quattro stilo, non un'ora e mezza se il vento è a favore.

Che abbia un senso, la categoria <em>netbook</em>, è fuori di dubbio. Come terza macchina me ne terrei uno volentieri. Che sia il futuro per noi tutti ho i miei dubbi. Passato l'entusiasmo, metà degli acquirenti si renderà conto e la bolla si sgonfierà, per diventare nicchia. Interessante e meritevole, con un sacco di perché, però nicchia.