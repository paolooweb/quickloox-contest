---
title: "La notte dei morti festanti"
date: 2008-10-31
draft: false
tags: ["ping"]
---

Se improvvisamente ci si ritrova a Halloween, i bimbi vogliono qualcosa di speciale e non c'è stato altro tempo, c'è sempre MacUpdate che <a href="http://www.macupdate.com/search.php?keywords=halloween&amp;starget=google" target="_blank">può dare una mano</a>.