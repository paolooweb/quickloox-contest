---
title: "Il tempo scorre"
date: 2009-11-19
draft: false
tags: ["ping"]
---

E qui, che non ci facciamo scappare un orologi digitale originale quando capita, si segnala questo inutile, ma ingegnoso, <a href="http://toki-woki.net/p/scroll-clock/" target="_blank">trucco in JavaScript</a>.