---
title: "Un conto è un conto, un conto è alla rovescia"
date: 2011-01-19
draft: false
tags: ["ping"]
---

Come si possa definire <a href="http://www.apple.com/it/itunes/10-billion-app-countdown/" target="_blank">conto alla rovescia</a> una progressione di numeri che parte da nove miliardi e arriva fino a dieci miliardi a me è poco chiaro.

Apple è stata peraltro dritta come nessun'altra a inventare l'App Store e nessun altro concorrente sta arrivando neanche vicino a queste vette. Domani tutto potrà cambiare; oggi l'azienda dimostra verso la concorrenza una superiorità in risultati, esecuzione, precisione che quasi spaventa. Per chi si occupa di Apple sono anni davvero indimenticabili e la nostalgia dei bei tempi andati che riscontro in tanti, quando si stava meglio anche se si stava peggio però eravamo giovani e belli, è quanto mai fuori luogo.