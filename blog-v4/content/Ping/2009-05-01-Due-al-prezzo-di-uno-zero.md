---
title: "Due al prezzo di uno (zero)"
date: 2009-05-01
draft: false
tags: ["ping"]
---

Spiegare che Linux è un'ottima alternativa, ma esistono ottime alternative a Linux, è una delle tante battaglie perse che amo condurre.

Ultimamente sono uscite due alternative, entrambe significative, al prezzo di una, ossia zero. Tutto <em>open source</em> del più genuino.

OpenBsd ha fatto uscire la <a href="http://www.openbsd.org/45.html" target="_blank">versione 4.5</a> e, soprattutto, c'è <a href="http://netbsd.org/releases/formal-5/NetBSD-5.0.html" target="_blank">NetBsd 5.0</a>, frutto di oltre due anni di lavoro con una lista chilometrica di miglioramenti e novità.

Come sempre, a OpenBsd hanno festeggiato con una <a href="http://www.openbsd.org/lyrics.html#45" target="_blank">canzone dedicata all'evento</a>.

Senza dimenticare che <a href="http://www.opensource.apple.com/darwinsource/" target="_blank">la parte Unix di Mac OS X</a> è un sistema operativo <em>open source</em> a sé, liberamente scaricabile, e che insomma Linux è sulla bocca di tutti ma non necessariamente è l'unica possibilità di fare qualcosa di diverso.