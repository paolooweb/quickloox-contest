---
title: "Lascia l’Ascii, ma ricorda dove sta"
date: 2004-05-05
draft: false
tags: ["ping"]
---

Un altro piccolo segreto nei meandri di Mac OS X

L’Ascii si avvia lentamente a diventare un ricordo del passato, sostituito da Unicode, un po’ – in termini di prestazioni – come Internet sta prendendo il posto del telex.

Ma l’addio sarà molto lento e l’Ascii (pronuncia: aski) è destinato a restare vivo per un bel po’.

Dentro Mac OS X c’è una utile tabellina riassuntiva dei caratteri Ascii, codificata in ottale, esadecimale e decimale. La si trova a /usr/share/misc/ascii, o anche digitando

man ascii

nel Terminale.

Ogni volta che si apre Mac OS X salta fuori qualcosa. Molto meglio del vecchio baule della nonna!

<link>Lucio Bragagnolo</link>lux@mac.com