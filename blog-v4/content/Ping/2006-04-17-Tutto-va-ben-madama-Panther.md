---
title: "Tutto va ben, madama Panther"
date: 2006-04-17
draft: false
tags: ["ping"]
---

L&rsquo;iMac di emergenza, equipaggiato con Mac OS X 10.3.9, ha superato in perfette condizioni l&rsquo;ultimo aggiornamento di sicurezza e l&rsquo;update di iTunes.