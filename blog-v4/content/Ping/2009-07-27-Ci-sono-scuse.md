---
title: "Ci sono scuse"
date: 2009-07-27
draft: false
tags: ["ping"]
---

Chi si ricorda la faccenda di Kindle di Amazon e delle copie digitali di 1984 <a href="http://www.macworld.it/blogs/ping/?p=2638" target="_self">cancellate dai lettori</a> dei clienti a causa del reclamo di un editore?

Jeff Bezos, amministratore delegato di Amazon, ha porto <a href="http://www.amazon.com/tag/kindle/forum/ref=cm_cd_ef_tft_tp?_encoding=UTF8&amp;cdForum=Fx1D7SY3BVSESG&amp;cdThread=Tx1FXQPSF67X1IU&amp;displayType=tagsDetail" target="_blank">pubbliche scuse</a>:

<cite>Queste sono scuse per il modo in cui abbiamo gestito le copie di <i>1984</i> e altri romanzi per Kindle vendute illegalmente. La nostra &#8220;soluzione&#8221; era stupida, non meditata e dolorosamente fuori luogo per i nostri principi. Ce la siamo interamente cercata e meritiamo le critiche che abbiamo ricevuto. Useremo le cicatrici di questo doloroso errore per aiutarci a prendere da qui in poi decisioni migliori, che corrispondano alla nostra missione.</cite>

<cite>Con profonde scuse ai nostri clienti,</cite>

<cite>Jeff Bezos</cite>
<cite>Fondatore e Ceo</cite>
<cite>Amazon.com</cite>

Quando l'amministratore delegato in persona prende la parola e si scusa pubblicamente, vuol dire che la faccenda è grave e che c'è bisogno di farlo.

Una conferma in più che, quando le aziende dispongono di forme di controllo a distanza sugli apparecchi degli acquirenti, tutto vogliono tranne che usarle.

Le applicazioni su App Store viaggiano verso le settantamila, i download verso i due miliardi, iPhone e iPod touch verso i cinquanta milioni. Apple ha tolto diverse applicazioni da App Store, ma non si è mai azzardata a toccare neanche un iPhone in mano a un acquirente legittimo.

C'è ancora qualcuno che crede alla storia della minaccia della privacy? Semmai è una responsabilità di cui in Apple (e Amazon) farebbero volentieri a meno.