---
title: "Vera partenza"
date: 2009-06-30
draft: false
tags: ["ping"]
---

Sono sempre qui, sulla pagina delle <a href="http://www.apple.com/it/macosx/refinements/enhancements-refinements.html" target="_blank">novità di Snow Leopard</a>, a tradurre una novità al giorno in vece di Apple.

<b>Nuovi punti di partenza per Automator</b>

Creare flussi di lavoro Automator ora è più facile, grazie ai nuovi punti di partenza relativi ad Applicazioni, Servizi, Azioni cartella, avvisi iCal e altro.

Sarà una delle novità più neglette e invece è importante. Se Automator riuscisse a colmare maggiormente il divario tra il semplice uso e lo scripting diventerebbe uno strumento di produttività formidabile.