---
title: "Direzioni di sviluppo"
date: 2008-06-15
draft: false
tags: ["ping"]
---

Come sarà il software tra cinque (meglio dieci) anni?

Guardo a oggi e vedo che si fanno <a href="http://nanoblogger.sourceforge.net/" target="_blank">motori di blog</a> da usare nel Terminale e <a href="http://280slides.com/" target="_blank">editor di presentazioni</a> da usare nei browser. Ecco.

Postilla: due dei tre creatori di 280 Slides arrivano da Apple. Toh.