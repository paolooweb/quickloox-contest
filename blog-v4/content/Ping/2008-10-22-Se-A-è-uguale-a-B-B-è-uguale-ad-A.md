---
title: "Se A è uguale a B, B è uguale ad A?"
date: 2008-10-22
draft: false
tags: ["ping"]
---

Steve Jobs non partecipa mai agli annunci dei risultati finanziari, salvo eccezioni.

Tre mesi fa, come di consueto, Steve Jobs non ha partecipato all'annuncio dei risultati finanziari. Qualche parolaio interpretò l'&#8220;evento&#8221; come un indizio di problemi di salute di Jobs.

Martedì Steve Jobs ha partecipato eccezionalmente all'annuncio dei risultati finanziari.

Però nessuno ha interpretato l'evento come un indizio di buona salute.

Che cosa devo pensare?