---
title: "È tempo di migrare"
date: 2010-02-05
draft: false
tags: ["ping"]
---

La foto, senza nessuna pretesa fuori dalla mera cronaca, sintetizza il passaggio di un mio familiare stretto da un vecchio iMac 17&#8221; a un nuovo iMac 21,5&#8221;.

La migrazione, da PowerPc a Intel, è stata indolore e automatica, grazie all'Assistente Migrazione presente in Mac OS X. La stampante dava un errore anomalo, ma è bastato cancellarla e reinserirla nel sistema per avere tutto a posto.

Velocità nettamente superiore, foto che si guardano in modo veramente diverso da prima e contemporaneamente la scrivania esattamente com'era, al punto che basta riattaccare i cavi per riavere l'ambiente di lavoro cui si era abituati. Penso a chi si ostina a procedere manualmente nella copia da disco a disco e davvero non capisco come non possa avere cose più interessanti da fare nella vita.

Sono uno che si tiene il suo vecchio Mac fino a spremerne l'ultima goccia. Spremuta quella, però, è tempo di passare senza esitazioni e senza troppi pensieri al nuovo. Quando sono passati cinque anni c'è solo da guadagnarne.

