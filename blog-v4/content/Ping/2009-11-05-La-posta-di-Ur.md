---
title: "La posta di Ur"
date: 2009-11-05
draft: false
tags: ["ping"]
---

Il <a href="http://www.macworld.it/blogs/ping/?p=3035" target="_self"><i>crash</i> del disco rigido</a> è un ricordo del passato e l'operatività è ripresa al cento percento. È rimasto uno strascico sulla posta elettronica, però: oltre ad avere perso una ventina di messaggi, arrivati dopo l'ultimo backup e prima del <i>crash</i>, alcune caselle si sono danneggiate in modo apparentemente irrimediabile. Dopo i tentativi di riparazione di prammatica ho ceduto alla tentazione che covava da tempo: adottare Mailsmith 2.2 in luogo di 2.1.5, cosa che avevo rimandato perché la nuova versione trovava errori nel vecchio <i>database</i> e non aggiornava la vecchia struttura del programma a quella odierna.

Ci sono alcuni vantaggi decisivi in Mailsmith 2.2. In primo luogo il programma conserva le capacità superiori di ricerca che ha sempre avuto, ma ora è anche compatibile Spotlight e quindi una ricerca facile diventa ultraveloce. Secondo, <a href="http://www.mailsmith.org" target="_blank">Mailsmith 2.2</a> è nativo Intel e la versione 2.1.5 era l'ultimo programma ereditato da PowerPc, che mi costringeva a usare Rosetta in pianta stabile, per un solo programma.

Tuttavia restava il problema delle caselle danneggiate e cos&#236; ho preso una decisione radicale: taglio netto con il passato. Esportazione di tutte le vecchie caselle ovunque possibile, <i>backup</i> di archivio su due Dvd della vecchia posta e azzeramento totale di tutto, dalle preferenze ai filtri alle signature.

Mi sentivo vagamente temerario ad affondare il bisturi in anni e anni di archivio e invece mi sto rendendo conto di avere preso una decisione azzeccata. Nel ricreare man mano il mio ambiente di posta constato che il mio vecchio database era mesopotamico. Il numero di account di posta attivi ma, di fatto, inutilizzati era quattro volte superiore a ciò che uso davvero; decine di filtri svolgevano una funzione oramai inutile su caselle ormai dormienti, legati a lavori terminati magari anni fa; decine di caselle, appunto, stavano l&#236; ad appesantire la struttura per nessuno scopo, anzi, complicando la gestione complessiva di quelle che invece servono oggi.

Il mio nuovo archivio di posta sarà incredibilmente più snello ed efficiente delle stratificazioni archeologiche di prima e questo è solo un bene.

Nel breve termine ne ricaverò una lentezza maggiore nel rispondere ai messaggi e di questo mi scuso in anticipo. Ovviamente la ricostruzione dei filtri e la creazione di un nuovo parco signature devono avvenire mentre procede il lavoro e dunque nelle pause, quindi tutto tornerà poco per volta.

Ripartire, con il programma libero da vecchiumi e più veloce nell'elaborazione, è tuttavia di grande soddisfazione. Le <a href="http://ecai.org/iraq/sitename.asp?siteid=25" target="_blank">rovine di Ur</a> verranno custodite con grande cura e in compenso ho una nuova possibilità di erigere una città più adatta per i tempi nuovi che arrivano.