---
title: "Di' qualcosa di italiano"
date: 2009-11-29
draft: false
tags: ["ping"]
---

Troppo facile paragonare Internet a un'immensa metropoli e tuttavia ci si dimentica che, vero il paragone, è anche vero che ci sono infinite strade e quartieri da scoprire e che non si finisce mai di conoscere veramente un posto, soprattutto se virtuale.

Cos&#236; ho scoperto che esiste un premio <a href="http://mashable.com/owa/" target="_blank">Open Web Awards</a>, alla terza edizione. Che c'è una categoria <i>Best Social Network iPhone App</i>. Che tra le nomination c'è una applicazione di nome <a href="http://itunes.apple.com/it/app/peoplesound/id317667818?mt=8" target="_blank">Peoplesound</a>. Che Peoplesound è realizzata da un team di stanza a&#8230; Milano.

A questo punto mi sono interessato all'applicazione, che parte da una buona idea: creare minireti sociali di amici, colleghi, sodali, che comunicano direttamente e rapidamente tra loro nei modi tipici del <i>social network</i> senza passare dai <i>moloch</i> che sono diventati Facebook e compagnia, in confidenzialità molto superiore e con interfacce concepite per gli apparecchi mobili, invece che adattate a roba che dovrebbe usarsi sul web.

Insomma, l'idea merita e se una buona applicazione italiana vincesse un premio non mi dispiacerebbe. Cos&#236; <a href="http://mashable.com/owa/votes/category/29?c=29" target="_blank">ho votato</a>.

Per votare è sufficiente avere un <i>account</i> Twitter o Facebook e, partendo dal <i>link</i> qui sopra, la procedura è semplicissima. A volte si inceppa, ma basta riprovare e in genere funziona subito.

Si possono votare anche altre applicazioni in altre categorie, ovviamente; c'è l'imbarazzo della scelta. L'importante è farlo entro il 13 dicembre. Ci si può autopromuovere se si ritiene di avere in mano qualcosa di valido e si può anche rivotare a volontà. Tutto molto libero, tutto molto stile 2.0.