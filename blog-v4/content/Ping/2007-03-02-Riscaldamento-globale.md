---
title: "Riscaldamento globale"
date: 2007-03-02
draft: false
tags: ["ping"]
---

Chi ci scamperà dai moralisti minimi?

Mi scuso in anticipo con chi frequenta la lista <a href="http://liste.accomazzi.net/mailman/listinfo/misterakko" target="_blank">Misterakko</a>, perché il grosso di questo contenuto arriva da l&#236;. Qui aggiungo che sentirsi impegnati sulle grandi cause è cosa nobile, ma pensare che le grandi cause si combattano con le piccole decisioni è, almeno, una contraddizione in termini.

Nello specifico, gira gente convinta che, siccome c'è il riscaldamento globale, abbiamo l'imperativo morale di spegnere i portatili invece che tenerli in stop. L'imperativo morale va sempre bene e per carità, ma questa gente è convinta che spegnere i portatili invece che tenerli in stop avrebbe anche conseguenze effettuali. Se fosse cos&#236;, il riscaldamento globale sarebbe un problema già risolto. Ora smetto di stare sul globale e ritorno al <em>particulare</em> del mondo Mac. Qualcuno, sulla lista suddetta, ha affermato che <cite>in tre minuti di avvio consumo meno che in sette ore di stop</cite>.

Si sono scatenate discussioni astruse sul consumo elettrico di un computer che fa questo e quello. Nel mio piccolo, ho cercato di dare una soluzione certamente empirica, ma comprensibile senza i <em>watt</em> e i <em>kilojoule</em>. Ho scritto quanto segue.

Diamo per convenzione che la capacità della mia batteria valga 1. Diamo per sicuro che abbia ragione Akko, ossia che durante lo startup il consumo di energia sia massimo. Per esperienza, su un PowerBook una batteria nuova sollecitata al massimo regge per circa due ore. Sempre per esperienza, è attendibile quanto affermato nella <a href="http://docs.info.apple.com/article.html?artnum=10571" target="_blank">nota tecnica</a> citata da Akko, ovvero che il mio PowerBook sopravviva in stop per sei settimane prima di esaurire la batteria.

Date le premesse, segue il calcolo.

Per nota tecnica citata da Akko e per esperienza personale, la batteria del mio PowerBook alimenta il PowerBook suddetto in stop per sei settimane, ossia 60.480 minuti. Sette ore di stop, 420 minuti, consumano 420/60.480 della batteria. Se la carica della batteria vale 1, sette ore di stop consumano 0,00694.

Lo startup occupa tre minuti a un'intensità che prosciugherebbe la batteria in due ore, che valgono 120 minuti. Ergo consuma 3/120 della batteria, cioè 0,02500.

Adesso tolgo gli zeri inutili, per rendere più visibile la risposta.

Consumo di uno stop di sette ore: 694.
Consumo di un avvio di tre minuti: 2.500.

L'avvio di tre minuti consuma più del triplo di uno stop di sette ore.

O qualcuna delle premesse data sopra è sbagliata (con spiegazione dettagliata del perché, grazie), o chi spegne il PowerBook la sera per riaccenderlo la mattina è il peggior nemico di Kyoto. Si noti che perfino un avvio di un solo minuto sarebbe di poco svantaggioso (0,00833) rispetto a uno stop di otto ore (0,00794).

Questo chiudeva il mio intervento. A chiosa, tengo da sempre il portatile in stop perché, se proprio voglio combattere il riscaldamento globale, abbasso la temperatura di un grado in casa (l'ho già fatto e comunque sono ugualmente noccioline) e adesso a maggior ragione. Il riscaldamento globale è certamente una minaccia ma, per la testa, basta ancora un cappelluccio.