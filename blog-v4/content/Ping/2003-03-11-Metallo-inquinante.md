---
title: "Metallo inquinante"
date: 2003-03-11
draft: false
tags: ["ping"]
---

Vedere Apple che non rispetta le sue regole mette il prurito

Se leggi le note tecniche Apple relative all’interfaccia utente, ti accorgi che l’aspetto metallico di certi programmi (QuickTime Player, iTunes, iMovie eccetera) ha un suo perché.

Le <link>note</link>http://developer.apple.com/techpubs/macosx/Essentials/AquaHIGuidelines/AHIGWindows/Textured_Windows.html spiegano che le finestre metalliche sono state create per i programmi che fungono da interfaccia con una periferica digitale, o gestiscono dati condivisi con periferiche digitali (Rubrica Indirizzi), oppure mirano a ricreare sullo schermo un oggetto fisico di uso comune (Calcolatrice).

Qualcuno mi spiega che cosa c’entra Safari?

Per quanto beta, Safari funziona già bene ed è diventato il mio browser di elezione. Mi ci trovo bene e mi è molto simpatico. Ma ha l’interfaccia sbagliata. È metallo inquinante che irrita e mette prurito. Alle mani.

<link>Lucio Bragagnolo</link>lux@mac.com