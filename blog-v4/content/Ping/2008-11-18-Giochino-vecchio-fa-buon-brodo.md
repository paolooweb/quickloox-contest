---
title: "Giochino vecchio fa buon brodo"
date: 2008-11-18
draft: false
tags: ["ping"]
---

Certe volte le commistioni tra tecnologia vecchia e nuova sono uno spettacolo.

Su iPhone e iPod touch funziona un <em>porting</em> di <a href="http://itunes.apple.com/WebObjects/MZStore.woa/wa/viewSoftware?id=296563933&amp;mt=8" target="_blank">Adventure</a> (link a iTunes Store), gioco scritto per la veneranda <em>console</em> Atari 2600.

Sul gioco originale c'era un <em>magic dot</em>: un punto invisibile che, trasportato in una zona segreta, apriva una schermata con i nomi dei creatori del gioco. Un <em>easter egg</em>, insomma.

C'è anche nel <em>porting</em>. E non voglio sapere, date le dimensioni dei <em>pixel</em> di allora e quelle di adesso, quanto abbiano sudato quelli che <a href="http://macenstein.com/default/archives/1809" target="_blank">lo hanno trovato</a>.