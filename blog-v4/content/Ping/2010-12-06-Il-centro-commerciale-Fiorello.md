---
title: "Il centro commerciale Fiorello"
date: 2010-12-06
draft: false
tags: ["ping"]
---

Capolavoro di Macity. La pagina è stata cambiata nel frattempo (probabilmente sono arrivati <i>aggiornamenti dell'ultim'ora</i>!). Però è stata <a href="http://www.setteb.it/il-comico-fiordaliso-ed-il-centro-commerciale-fiorello-10168" target="_blank">eternata come merita da SetteB.it</a>.

Ma c'è ancora qualcuno che ci crede?