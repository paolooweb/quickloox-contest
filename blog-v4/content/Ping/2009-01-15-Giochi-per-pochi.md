---
title: "Giochi per pochi"
date: 2009-01-15
draft: false
tags: ["ping"]
---

Zork diventa <a href="http://legendsofzork.com/" target="_blank">gioco di massa online via browser</a> e già questa è una supernotizia, per chi conosca appena appena la storia dei videogame e delle avventure solo testo.

Potrebbe però anche esserci una tendenza in atto, verso forme di intrattenimento meno prevedibili, diverse dal solito <em>fantasy</em>, dal solito sparatutto, dalla solita corsa in pista, dal solito calcio.

Gli stessi di Zork - probabilmente non a caso - reclamizzano anche <a href="http://nationstates2.com/" target="_blank">Nation States 2</a>, gioco di costruzione e confronto di nazioni, e <a href="http://www.trukz.com/" target="_blank">Trukz</a>, simulazione di&#8230; camionisti che a dirla cos&#236; non ispira e invece sembra intrigante.

Sono cose che mai raggiungeranno utenze milionarie. Ma divertite s&#236;.