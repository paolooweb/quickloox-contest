---
title: "Un browser tira l’altro"
date: 2004-03-09
draft: false
tags: ["ping"]
---

Dato chi non sa scrivere, bisogna essere preparati a leggere

Mi stupisce la riluttanza di molti a tenere sul disco più di un browser. Invece spesso è la soluzione all’ignoranza di chi scrive le pagine web.

Ieri la mia banca mi ha proposto un nuovo servizio online. Sul sito, normalmente perfetto se visto da Safari, la pagina di attivazione era un garbuglio incomprensibile di riquadri di testo sovrapposti. Un webmaster ignorante, probabilmente, o la fretta.

Ho caricato la stessa pagina con iCab, che mi lasciava vedere i pulsanti di attivazione. Fatto.

Eppure lo dici a qualcuno e sembra di stare parlando di bigamia, o peggio.

<link>Lucio Bragagnolo</link>lux@mac.com