---
title: "Probabilità e certezze"
date: 2010-07-23
draft: false
tags: ["ping"]
---

Niente di personale con <b>Takky</b>, anzi! Ma una sua osservazione nei commenti mi sollecita a una puntualizzazione importante, che vale ben oltre la questione di iPhone 4.

Takky <a href="http://www.macworld.it/ping/hard/2010/07/20/la-presa-di-sale/comment-page-1/#comment-12749" target="_blank">scrive</a> <cite>mi bastano due video differenti che mostrino il problema per sapere che esiste</cite>.

Come sempre le cose sembrano semplici e invece sono complesse.

Per sapere che il problema esiste, basta un video. Ma a dire il vero basta anche una riga per email. Potrebbe essere falsa? Sicuramente, come potrebbe esserlo il video. Dire <i>l'ho visto su YouTube</i> corrisponde ai nostri vecchi che dicevano <i>l'ho visto in televisione</i> e prima ancora <i>lo ha detto la radio</i> e persino prima della radio <i>lo hanno scritto i giornali</i>. Il punto è fidarsi. Ci fidiamo e quindi basta un video, anche perché è un problema che hanno tutti i cellulari del mondo, quindi basta che qualcuno si prenda la briga di filmare. Due video non equivale a <i>verità</i>; potrebbe benissimo corrispondere a due buontemponi che hanno realizzato un falso indipendente. A riprova frequentare <a href="http://attivissimo.blogspot.com" target="_blank">Paolo Attivissimo</a>, che su Apple ha la <i>verve</i> di un pensionato alla bocciofila, con rispetto per gli uni e le altre, ma sulla sua materia è un grande; i filmati sugli Ufo, i cerchi nel grano, le scie chimiche, i falsi sull'11 settembre e sullo sbarco sulla Luna sono innumerevoli; tuttavia sono anche mistificazioni.

Che cosa dimostra un filmato su iPhone 4 che perde il segnale tenuto in mano in un certo modo? Che un iPhone 4, tenuto in mano in un certo modo, subisce l'attenuazione del segnale. Ineccepibile, solo che questo è il dato di partenza, non il dato di arrivo. Tutti i cellulari subiscono attenuazione del segnale. È un fatto inevitabile, che c'è da sempre e che la tecnologia attuale non è in grado di eliminare. I filmati mostrano l'ovvio e lo confermano. Essendo ovvio, era anche prevedibile.

Adesso arriva la parte impegnativa.

Noi sappiamo che tutti gli iPhone 4 &#8211; e tutti gli iPhone 3GS e tutti i Nokia e tutti i BlackBerry e tutti gli Htc e tutti a proprio tutti &#8211; subiscono attenuazione del segnale. Ma lo sappiamo perché appartiene al senso comune, non certo perché ci sono dieci, cento, mille video su YouTube.

Se io arrivassi da Marte e non sapessi niente di Terra, terrestri e cellulari, vedendo due video che mostrano due monete girare per aria e cadere su <i>croce</i> penserei che le monete lanciate per aria atterrano su <i>croce</i> (qualunque terrestre sa che esistono anche altre possibilità, per esempio <i>testa</i>).

Dopo di che tirerei fuori il mio bigino di statistica marziana.

I due video mi permettono di affermare <i>con certezza</i> che quelle due monete, lanciate in aria, sono cadute su <i>croce</i> e niente altro. Essendo marziano, fino a che non avrà almeno un video con <i>testa</i> neanche lo saprò.

I due video mi permettono di affermare, stavolta <i>con un margine di errore ragionevolmente basso</i>, che il comportamento visto può riguardare altre, improvviso, sette o otto monete. Non di più, a meno di non accettare l'idea di un margine di errore cos&#236; alto da essere irragionevole (difatti, se mi fermassi sulla Terra per un po', scoprirei che i video mostrano nel cento percento dei casi una cosa che avviene sono sul cinquanta percento dei lanci).

E tre milioni di monete? (ricordo, tanti sono gli iPhone 4 là fuori).

La risposta esatta la lascio a chi abbia una istruzione vera; mi limito ad approssimare che servirebbero molte decine di video, o forse anche centinaia e scelti con accuratezza. Se io producessi un video della mia finestra per ogni giorno che piove e solo quello, uno spettatore avrebbe l'impressione che a casa mia piove sempre. I sondaggi elettorali si basano su campioni selezionati con cura certosina &#8211; mica su chi ha voglia di dire su YouTube per chi vota &#8211; e sempre sopra il migliaio di unità, e con un margine di errore.

I video &#8211; o i sondaggi, o i <i>post</i> sui blog, o gli <i>exit poll</i> &#8211; dimostrano qualcosa se sono campioni sufficientemente grandi e sufficientemente rappresentativi della realtà. Se sono troppo piccoli, o non comprendono tutta la realtà, danno risultati sballati.

Chi lo desidera si può allenare con il <i>widget</i> <a href="http://www.apple.com/downloads/dashboard/calculate_convert/statisticalsignificancecalculator.html" target="_blank">Statistical Significance Calculator</a>, dopo avere letto almeno <a href="http://www.matematicamente.it/siti_di_matematica/siti_di_matematica/probabilita_e_statistica_20070626179/" target="_blank">qualcosina a livello di base</a> o magari <a href="http://www.dm.unipi.it/~acquistp/newprob.pdf" target="_blank">una dispensa</a>.

In altre parole: i due video che mostrano l'attenuazione del segnale mostrano, dato per scontato che siano veri, che due iPhone 4 soffrono di attenuazione del segnale. Da questa constatazione è possibile dedurre una marea di altre cose, che però non arrivano dai video; arrivano da nostri ragionamenti, che i video non convalidano né certificano.

Altrimenti, guardando questo video in cui una moneta lanciata in aria mostra <i>testa</i> per <a href="http://www.youtube.com/watch?v=X1uJD1O3L08" target="_blank">dieci volte di fila</a>, concludo che vale per tutte le monete. Come dite? Ci vogliono almeno due video? Ecco <a href="http://www.youtube.com/watch?v=bbu7lxDJ3_A" target="_blank">il secondo</a>.