---
title: "L&rsquo;acqua al mulino"
date: 2006-05-14
draft: false
tags: ["ping"]
---

Cercher&ograve; di non essere polemico e spiegare semplicemente come stanno le cose. Apple non recensisce i widget presenti nell&rsquo;apposita <a href="http://www.apple.com/downloads/dashboard/" target="_blank">sottosezione del suo sito</a>. Se un programmatore invia come descrizione del proprio widget <em>questo &egrave; il miglior widget del mondo</em>, e Apple approva il widget, appare quella descrizione.