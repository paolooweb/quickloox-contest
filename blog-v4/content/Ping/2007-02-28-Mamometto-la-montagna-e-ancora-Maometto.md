---
title: "Maometto, la montagna e ancora Maometto"
date: 2007-02-28
draft: false
tags: ["ping"]
---

Quelli dell'All About Apple Club sono da applauso. Dopo essere stati in visita a Cupertino, hanno ricevuto la <a href="http://www.allaboutapple.com/speciali/enzo_biagini.htm" target="_blank">visita di Apple</a> al museo di Quiliano, nella persona di Enzo Biagini.

O si muovono loro, o fanno muovere gli altri. Qualcosa però si muove sempre e arrivano risultati a catena. Complimenti ammirati.