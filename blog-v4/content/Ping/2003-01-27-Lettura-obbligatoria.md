---
title: "Lettura obbligatoria"
date: 2003-01-27
draft: false
tags: ["ping"]
---

Prima di riempirsi la bocca, rifornire il cervello

Il mito dei megahertz (il megahertz è una misura di frequenza e non di velocità, quindi avere più megahertz non è uguale a essere più veloci) è stato spiegato in abbondanza ma continua a esistere gente che ciancia di processori esattamente come alla televisione si ciancia di rigori e moviole: ognuno dice la sua e tutti hanno sempre ragione.

Da oggi non più. Un decreto del Ministero del Macintosh impone l’apprendimento obbligatorio del funzionamento base dei microprocessori, mirabilmente spiegato da Jon Stokes di <link>Ars Technica</link>http://arstechnica.com/paedia/c/cpu/part-1/cpu1-1.html.

Se proprio bisogna dare aria ai denti, almeno ci sia qualcosa da dire.

<link>Lucio Bragagnolo</link>lux@mac.com
