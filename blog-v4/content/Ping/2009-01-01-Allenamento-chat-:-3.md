---
title: "Allenamento chat / 3"
date: 2009-01-01
draft: false
tags: ["ping"]
---

Buon anno a tutti ancora una volta!

Per il 6 gennaio è confermatissima la solita chiacchiera collettiva in occasione del keynote di apertura di Macworld Expo.

Si aprono le danze alle 18 italiane. Probabilmente io sarò collegato da prima, ma non ci sarà niente da commentare, al massimo si conversa del più e del meno.

Aprirò una stanza iChat (darò notizia del nome) che servirà da buttadentro. iChat è comodissimo, ma ha un limite di capienza e l'anno scorso qualcuno è rimasto fuori. Non voglio più che succeda. (Per aprire una stanza in iChat, <em>Comando-Maiuscole-G</em> su Tiger e <em>Comando-</em>R su Leopard, poi si scrive il nome della stanza stessa). Io sarò attivo anche nella stanza tradizionale del Poc, <code>pocroom</code>, sempre per fare da buttadentro.

L'appuntamento vero è su Irc, la chat libera di Internet. Per fare chat Irc esistono decine di <a href="http://www.macworld.it/ping/soft/2008/12/06/allenamento-chat-1/" target="_blank">programmi, anche gratuiti</a>. Lo scopo è collegarsi al server <code>irc.freenode.net</code> e, una volta collegati, dare il comando <code>/join #freesmug</code>. Ci troviamo l&#236;, sponsorizzati (niente soldi, solo simpatia) da <a href="http://freesmug.org" target="_blank">Gand</a>. Viva il software libero per Mac!

Non c'è davvero altro da sapere per arrivare. Poi, per stare a proprio agio nella stanza, ci regoliamo al volo. I comandi Irc utili sono pochi e semplici e i volonterosi trovano su Internet <a href="http://www.ircbeginner.com/ircinfo/ircc-commands.html" target="_blank">elenchi utili</a>.

Se serve altro da sapere&#8230; sono qui. Commentando qui sotto o <a href="mailto:lux@mac.com?subject=Un%20consiglio%20su%20Irc" target="_blank">per posta</a>.