---
title: "Un blog, un mito"
date: 2007-09-13
draft: false
tags: ["ping"]
---

Aveva già usato un <a href="http://www.bytecellar.com/archives/000113.php" target="_blank">Apple //c come terminale di un Mac mini</a>.

Adesso ha fatto la stessa cosa <a href="http://www.bytecellar.com/archives/000121.php" target="_blank">con un eMate</a>.

I miti, d'altronde, arrivano sempre dal passato. :-)