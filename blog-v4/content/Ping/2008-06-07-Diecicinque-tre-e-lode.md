---
title: "Dieci(cinque-tre) e lode"
date: 2008-06-07
draft: false
tags: ["ping"]
---

Mac OS X 10.5.3 fa una montagna di cose. Tra le quali risolvere un problema di Spaces, che spesso cambiava spazio in modo indesiderato se, passando a un altro programma, questo aveva finestre aperte in più spazi.

Non so che cosa esattamente abbiano fatto i 420 megabyte di aggiornamento. A me è più che sufficiente questo. 