---
title: "Gli estremi non si toccano"
date: 2007-11-26
draft: false
tags: ["ping"]
---

Non capirò mai quelli che si comprano il Mac meno costoso e il giorno dopo vanno in cerca del gioco più costoso, per lamentarsi che le prestazioni del Mac meno costoso non sono all'altezza di quelle del Mac più costoso, quando il mondo è pieno di giochi meno costosi. E forse persin più belli.