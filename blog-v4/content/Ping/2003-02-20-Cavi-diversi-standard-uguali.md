---
title: "Cavi diversi, standard uguali"
date: 2003-02-20
draft: false
tags: ["ping"]
---

I connettori FireWire devono essere diversi, pena la lentezza

Il bello spirito di questa settimana si lamenta che i nuovi Mac hanno connettori diversi tra FireWire 400 e FireWire 800.

Evidentemente lui preferisce che il mondo resti legato per sempre a periferiche da 100 megabit per secondo, quando sarà possibile arrivare a 800 e poi, quando lo standard sarà completato, a 3,2 gigabit per secondo.

Probabilmente, anche se non lo sa, preferisce che il mondo di tutti resti piccolo come il suo. Durante una sua visita a Milano Larry Ellison, il capo di Oracle, ha detto che non sapere le cose costa tantissimo.

Aveva ragione. Il bello è che c’è chi vorrebbe farne pagare il prezzo a tutti.

<link>Lucio Bragagnolo</link>lux@mac.com