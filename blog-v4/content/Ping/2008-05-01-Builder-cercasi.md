---
title: "Builder cercasi"
date: 2008-05-01
draft: false
tags: ["ping"]
---

I realizzatori di <a href="http://ufoai.sourceforge.net/" target="_blank">Ufo: Alien Invasion</a> cercano chi realizzi per loro i package binari del gioco per Mac.

A volte non è che i giochi manchino. Manca solo un po' di tempo/uomo.