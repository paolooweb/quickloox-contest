---
title: "Il coraggio del compromesso<p>"
date: 2005-01-13
draft: false
tags: ["ping"]
---

Del senno di poi son pieni gli shuffle<p>

È facile (adesso!) capire certi ragionamenti che hanno portato Apple a presentare oggetti come Mac mini e iPod Shuffle. Uno di questi è il coraggio di compromettersi e lo si capisce pensando al primo iMac.<p>

iMac ha rappresentato una rivoluzione perché era colorato, ma sotto c&rsquo;era un&rsquo;altra ragione. Per la prima volta un produttore diceva Ok, di questo non c&rsquo;è più bisogno, riferendosi al lettore di floppy disk. iMac ha avuto successo per quello che non c&rsquo;era dentro.<p>

Pensando a Mac mini, e a iPod shuffle, colpisce subito come il realizzare questi prodotti in stile Apple abbia richiesto compromessi, e cose che non ci sono. Esattamente come il primo iMac.<p>

Avrannno successo.<p>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>