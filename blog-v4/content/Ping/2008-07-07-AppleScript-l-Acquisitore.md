---
title: "AppleScript, l'Acquisitore"
date: 2008-07-07
draft: false
tags: ["ping"]
---

Intanto chiedo scusa per i ritardi in cui arriva il pezzo AppleScript. Sono giornatacce, fortunatamente solo parlando di lavoro. Passerà.

Non passa invece la possibilità di mostrare AppleScript semplici e interessanti. Questo arriva da <a href="http://www.tuaw.com/2008/07/07/applescript-taking-screenshots/" target="_blank">The Unofficial Apple Weblog</a>, per mano del solito Cory Bohon.

<code>property N : 0</code>
Cos&#236; si imposta una variabile globale.

<code>set N to N + 1</code>
Cos&#236; se ne incrementa di uno il valore. Una variabile è un cassetto, che contiene un valore, che può essere numerico o di altro genere.

<code>set percorso to ((POSIX path of (path to desktop)) &#38; "Schermata_" &#38; N &#38; ".png") as string</code>
Tutto questo lavoro serve a riempire in modo inattaccabile una variabile con una scritta, che inizia con <em>Schermata_</em> e termina con il numero di cui a inizio script, contenuto nella variabile <em>N</em>. La scritta deve essere in formato testo, ché altrimenti non la si può usare nella prossima istruzione. Il formato testo per AppleScript è <em>string</em>.

<code>do shell script "screencapture -tjpg " &#38; quoted form of percorso</code>
Questo è il succo. Si usa il comando di Terminale <code>screencapture</code> per acquisire la schermata sul Mac.

<cite>Ma non facevi prima a fare Comando-Maiuscole-3?</cite>

Certo, ma avrei registrato in .png. Magari mi interessava un Jpeg. L'istruzione, come appare nello script, cambia temporaneamente il formato di registrazione in Jpeg. <em>Temporaneamente</em> qui è dato da quella <em>t</em> appena prima di <em>jpg</em>. Mi fosse interessato temporaneamente Tiff, avrei fatto <em>-ttiff</em>. Il comando di Terminale <code>man screencapture</code> mostra un sacco di cose interessanti da usare con profitto nello script.

Se registro lo script come Applicazione o come Bundle applicazione, poi posso richiamarlo da Spotlight e farlo partire - e acquisire la schermata su misura - in ogni momento.

Ecco lo script non commentato.

<code>property N : 0
set N to N + 1
set percorso to ((POSIX path of (path to desktop)) &#38; "Schermata_" &#38; N &#38; ".png") as string
do shell script "screencapture -tjpg " &#38; quoted form of percorso</code>