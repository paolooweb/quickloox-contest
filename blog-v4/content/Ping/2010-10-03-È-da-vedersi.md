---
title: "È da vedersi"
date: 2010-10-03
draft: false
tags: ["ping"]
---

L'agenda tiranna mi ha escluso dalla presenza all'inaugurazione dell'Apple Store di Orio al Serio e, per farmi un'idea dell'atmosfera, ho scelto la <a href="http://www.setteb.it/7b/gallery/09751/index.htm" target="_blank">rassegna fotografica di SetteB.It</a>.

Secondo me la migliore, nonostante qualche refuso nelle didascalie, per come riesce a trasmettere l'atmosfera e per un pizzico di sanissima ironia qua e là che alleggerisce. Sempre negozi sono, per quanto speciali.

A margine, tra <a href="http://www.apple.com/it/retail/carosello/" target="_blank">Milano-Carugate</a>, <a href="http://www.apple.com/it/retail/legru/" target="_blank">Torino-Grugliasco</a> e <a href="http://www.apple.com/it/retail/oriocenter/" target="_blank">Orio al Serio</a>, la presenza diretta Apple almeno in Italia settentrionale inizia finalmente ad aspirare alla decenza (il presidio di <a href="http://www.apple.com/it/retail/romaest/" target="_blank">Roma Est</a> per tutto il resto del Paese è francamente frustrante). Possiamo sperare in tempi ancora migliori.