---
title: "Hardware da Marte, software da Venere"
date: 2009-04-05
draft: false
tags: ["ping"]
---

Non ricordo più chi, passeggiando insieme per via Terraggio, mi spiegava che sì, iPhone non era neanche tanto male, ma il futuro era dei telefoni <em>open source</em> come <a href="http://wiki.openmoko.org/wiki/Neo_FreeRunner/it" target="_blank">Neo FreeRunner</a> di <a href="http://wiki.openmoko.org/wiki/Main_Page/it" target="_blank">OpenMoko</a>.

Dopo essermi servito per mostrare che <a href="http://www.macworld.it/blogs/ping/?p=1875" target="_self">il nome di un oggetto non coincide necessariamente con la sua funzione</a>, FreeRunner <a href="http://www.bernerzeitung.ch/digital/mobil/Vorlaeufig-kein-weiteres-LinuxTelefon/story/15176367" target="_blank">non verrà più prodotto</a>.

La logica <em>open source</em> produce risultati eccellenti con il software. Con lo hardware è sempre cosa per pochi (FreeRunner ha venduto tredicimila esemplari) o un fallimento.

L’<em>open source</em> funziona splendidamente grazie alla rete. Tutti possono condividere uno sviluppo software, nessuno una modifica fisica a un connettore.

Ecco perché Apple ha un <a href="http://www.opensource.apple.com/darwinsource/" target="_blank">sistema operativo basato su open source</a> ma i computer se li costruisce lei e perché Google ha un <a href="http://developer.android.com/" target="_blank">sistema operativo open source per smartphone</a>, ma non ha <em>smartphone</em> <em>open source</em>.