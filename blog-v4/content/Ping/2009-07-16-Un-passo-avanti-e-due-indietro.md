---
title: "Un passo avanti e due indietro"
date: 2009-07-16
draft: false
tags: ["ping"]
---

Mentre lavoravo con InDesign è apparso Adobe Updater a segnalarmi l'esistenza di un aggiornamento.

Voleva a tutti i costi che venisse chiusa l'applicazione. Col cavolo: sto lavorando, aspetti. Possibile che un aggiornamento sia cos&#236; sciocco? Più tardi, gli ho risposto.

Il giorno dopo ho scoperto la presenza di un'immagine disco contenente l'aggiornamento. Non stato più lavorando con InDesign e ho tranquillamente aggiornato dall'immagine disco. Sistema intelligente, brava Adobe.

Lanciato l'aggiornamento, bisogna accettare l'accordo di licenza. Ci sono due pulsanti, Accept e Decline, in quest'ordine. Non c'è pulsante preimpostato ed è più o meno da venticinque anni che il pulsante &#8220;giusto&#8221; (in questo caso Accept) è l'ultimo a destra. Chi ha progettato questa finestra di dialogo ha esagerato con i funghi crudi.

Possibile che chi produce applicazioni per i professionisti della grafica non riesca a capirne un po' di più, che so, magari per osmosi?