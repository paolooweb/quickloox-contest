---
title: "La pazzia di re Ballmer"
date: 2008-11-09
draft: false
tags: ["ping"]
---

In ordine cronologico casuale, Microsoft ha cercato di comprarsi Yahoo per tenere testa a Google e non c'è riuscita.

L'amministratore delegato, Steve Ballmer, ha dichiarato a suo tempo che schifava iPhone perché preferiva avere il suo software sull'80 percento dei telefoni invece che avere l'uno percento con il suo hardware. E niente di male, se non fosse che iPhone vende più di tutti gli apparecchi Windows Mobile messi insieme e i conti non tornano più.

Microsoft spende 300 milioni di dollari in pubblicità per rilanciare Vista. I faciloni la criticano perché avrebbe potuto spenderli in sviluppo software, ma sbagliano, dato che Vista si basa su fondamenta inadeguate e dunque l'unico modo per uscirne sarebbe ripartire da zero. La pubblicità ha senso dunque, ma intanto si comincia a battere la grancassa su Windows 7, che ha esattamente le stesse problematiche strutturali di Vista, e Ballmer dichiara che restare su Windows Xp è assolutamente Ok. Il che spinge però a chiedersi se quei trecento milioni di pubblicità non sarebbero stati meglio spesi in vacanze per i manager stressati.

Ultimamente, su domanda, Ballmer ha dichiarato che Microsoft sta considerando l'adozione di WebKit, pur continuando a sviluppare estensioni per Explorer.

Il vertice di Microsoft è ufficialmente allo sbando. Ci sono due tipi di sbando: quando continui a prendere decisioni sbagliate e quando hai perso il contatto con la realtà dei fatti. Il problema non è che Microsoft prenda decisioni sbagliate; uno se ne accorge e si corregge. È che ha perso il contatto con la realtà. Chi vive in un'allucinazione è convinto che sia tutto vero. E non può correggersi.

Da qui a dieci anni Microsoft rischia una crisi molto seria. Detta cos&#236; pare roba trascurabile. Dieci anni fa scrivevo da qui a venticinque anni. Come dire che Ballmer si è portato cinque anni avanti.