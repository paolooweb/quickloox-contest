---
title: "L'obliquità della diagonale"
date: 2010-11-01
draft: false
tags: ["ping"]
---

Sappiamo tutti che iPad ha uno schermo 9,7&#8221; e sappiamo anche che sta uscendo il Galaxy Pad di Samsung, schermo 7&#8221;. Presentato come concorrente di iPad.

Sembra plausibile: gli schermi sono l&#236;, c'è poca differenza. E invece no. Ho visto il Galaxy Tab. È molto più piccolo di un iPad, la differenza che c'è tra il <i>tenere in mano</i> e lo <i>stare in una mano</i>. Sono a tutti gli effetti due oggetti diversi e adesso credo molto di più all'idea che Apple stia considerando l'idea di un iPad 7&#8221;. Sono categorie affini quanto possono esserlo un portatile 13&#8221; e un portatile 15&#8221;. Ma come mai la differenza tutto sommato minima nello schermo si traduce in una clamorosa sproporzione nelle dimensioni?

Perché gli schermi vengono pubblicizzati per la loro diagonale. E perché, più sono quadrati, più lo schermo è grande rispetto alla diagonale. Più sono rettangoli, più lo schermo è piccolo rispetto alla diagonale.

Un esempio stupido e banale e mi si perdoni per l'aritmetica: sapendo che iPad ha lo schermo di 132 punti per pollice e che la risoluzione è 1.024 x 768, il suo schermo è circa 19,7 x 14,8 centimetri. L'area dello schermo di iPad è dunque di circa 292 centimetri quadrati.

Parrebbe che, 7&#8221; contro 9,7&#8221;, Galaxy Tab &#8220;valga&#8221; circa sette decimi di iPad quanto a dimensioni fisiche. Il 72 percento a essere un po' più precisi.

In proporzione, Galaxy Tab ha più pixel: 1.024 x 600 pixel a 169 punti per pollice sono 614 mila pixel, il 78 percento dei 786 mila pixel di iPad. Lo schermo di Galaxy Tab, come pixel, rende più di quello di iPad in proporzione alle dimensioni.

Ma lo stesso schermo è, appunto, 1.024 x 600, ossia molto più rettangolare di quello di iPad. A 169 punti per pollice, lo schermo misura 15,39 x 9 centimetri, ovvero 138,51 centimetri quadrati: meno della metà dello schermo di iPad!

Capito? Il confronto tra le due diagonali è di sette a dieci, ma la dimensione fisica dei due schermi è a malapena di uno a due: un Galaxy Tab è grande meno della metà di un iPad.

A visitare un po' dei soliti sitarelli sparafumo e si vedrà che il confronto fotografico viene sempre fatto riducendo iPad e aumentando Galaxy Tab, come se le dimensioni relative fossero molto più vicine della realtà: <a href="http://www.digitaltrends.com/computing/samsung-galaxy-tab-vs-apple-ipad/" target="_blank">Digital Trends</a> oppure <a href="http://www.redmondpie.com/ipad-vs-galaxy-tab-comparison/" target="_blank">Redmond Pie</a> e perfino <a href="http://www.pcworld.com/zoom?id=204779&amp;page=1&amp;zoomIdx=1" target="_blank">PcWorld</a>.

Più la gente diventa seria, più la vince la realtà: il confronto di <a href="http://www.slashgear.com/ipad-vs-galaxy-tab-16102821/" target="_blank">SlashGear</a> mostra chiaramente la differenza.

Su che tavoletta delle due leggeresti un quotidiano? Quale entrerebbe meglio nella tasca della giacca? Stiamo evidentemente parlando di due oggetti nati per essere usati in modo diversissimo. Altro che concorrenza, parola usata dai sitarelli per cercare di costruire una contrapposizione.

In modo molto obliquo. Pardon, diagonale.