---
title: "Nel regno del fattibile"
date: 2010-05-10
draft: false
tags: ["ping"]
---

Nella <a href="http://liste.accomazzi.net/mailman/listinfo/misterakko" target="_blank">mailing list di Misterakko</a> chiesero provocatoriamente <cite>pregasi elencare azioni fattibili su un iPad e non fattibili su un Sony Vaio W da 10&#8221;</cite>.

Risposi <a href="http://www.youtube.com/watch?v=HvplGbCBaLA" target="_blank">concedere un bis al termine di un concerto</a>.

Ora posso aggiungere <a href="http://www.socialtimes.com/2010/05/finally-a-good-use-for-the-ipad/" target="_blank">usarlo in cucina come elettrodomestico da incasso</a>.