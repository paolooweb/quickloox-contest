---
title: "Numeri alla lettera"
date: 2010-07-26
draft: false
tags: ["ping"]
---

Popolo di AppleScript: puntare a questo <a href="http://www.macosxhints.com/article.php?story=2010060515230735" target="_blank">intervento su Mac OS X Hints</a>, che programma un tastierino numerico sulla tastiera normale e consente di commutare con una semplice combinazione di tasto da lettere a numeri.

La sfida interesssante è che lo script originale prevede una disposizione dei &#8220;numeri&#8221; poco efficiente sulla tastiera italiana. Senza contare che ciascuno potrebbe volere la propria modifica.

Lo script è lungo e però la zona di definizione dei numeri associati ai tasti è di evidente chiarezza. Sotto a chi tocca (i tasti)!