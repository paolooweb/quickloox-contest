---
title: "Vive la difference"
date: 2009-04-23
draft: false
tags: ["ping"]
---

L'azienda di sicurezza Finjan ha individuato una <em>botnet</em> (rete di computer <em>zombi</em>) che comprende <a href="http://www.finjan.com/MCRCblog.aspx?EntryId=2237" target="_self">due milioni di computer</a>, in crescita.

I computer contaminati sono sparsi tra numerose nazioni. Il sei percento di essi, 114 mila macchine, risiede in 52 organizzazioni differenti, tra cui sei enti governativi inglesi e la Bbc, per un totale di oltre 70 enti governativi di nazioni in giro per il mondo.

Non è questione di proteggersi. Il software viene identificato solo da quattro antivirus su trentanove e un sistema &#8220;protetto&#8221; potrebbe, quindi, non esserlo.

I computer <em>zombi</em> contengono software installato clandestinamente grazie a buchi nel sistema operativo o nel <em>browser</em> e possono essere comandati a distanza da una banda di sei pirati ucraini.

La banda utilizza i computer infetti, all'insaputa degli utenti legittimi, per inviare <em>spam</em>, attaccare siti web, registrare schermate, spiare l'attività del proprietario e molte altre cose.

Ne ricava anche 190 mila dollari al giorno, in parte dall'attività diretta, in parte affittando la rete ad altri criminali.

I computer della rete sono cos&#236; assortiti: due milioni Windows, zero Linux, zero Mac.

Si parlava giorni fa di una <em>botnet</em> di ventimila Mac contaminati, non grazie a problemi nel sistema, ma per la stupidità dei proprietari, che hanno volontariamente scaricato software modificato da siti pirata invece che acquistarlo o scaricarlo da siti legittimi.

Per essere colpiti su Mac bisogna essere stupidi. Per essere colpiti su Windows spesso basta esistere.