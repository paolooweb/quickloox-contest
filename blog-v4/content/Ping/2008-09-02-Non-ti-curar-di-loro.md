---
title: "Non ti curar di loro"
date: 2008-09-02
draft: false
tags: ["ping"]
---

Avrei pronto un archivio di opinioni eccellenti sul fiasco di iPhone e sull'impossibilità che Apple vendesse dieci milioni di iPhone nel 2008.

Adesso che dall'analisi degli Imei emerge come Apple abbia venduto più di cinque milioni di apparecchi 3G dall'11 luglio al 30 agosto, e come l'iPhone numero dieci milioni potrebbe arrivare addirittura entro fine mese o poco dopo, lo cestinerò.

A leggere le opinioni dei miopi si affatica la vista e c'è più bisogno di saper guardare lontano.

Per inciso, l'idea di <a href="http://www.macobserver.com/forums/viewtopic.php?t=69155&amp;postdays=0&amp;postorder=asc&amp;start=0&amp;sid=d6831e3da769ea993190bd054ba39eca" target="_blank">raccogliere Imei per stimare le vendite</a> è geniale. I numeri sono anche verificabili in un <a href="http://spreadsheets.google.com/ccc?key=pUwZATIrXuTeCVdJHkQY1Zg&amp;hl=en" target="_blank">foglio di Google Docs</a>.