---
title: "A scuola di quota di mercato"
date: 2007-10-10
draft: false
tags: ["ping"]
---

Mac usati all'università di Princeton come percentuale sul totale nel 2003/2004: 10 percento.

Nel 2004/2005: 16 percento.

Nel 2005/2006: 23 percento.

Nel 2006/2007: 31 percento.

Dall'<a href="http://www.dailyprincetonian.com/archives/2007/10/05/news/18871.shtml" target="_blank">articolo originale</a>: <cite>Per l'inizio dell'anno, la Student Computer Initiative dell'università ha venduto più Mac che Pc. Gli studenti potevano scegliere tra una selezione di computer Apple, Dell e Ibm, e il 60 percento ha scelto un Mac, contro il 45 percento dell'anno prima.</cite>

Mi vengono in mente tutti quei genitori ansiosi che ho sentito dire <cite>ma se compro un Mac a mio figlio poi come farà sul lavoro, dove dovrà usare i Pc?</cite>

Troppo facile la battuta: tutta gente che, certamente, non ha frequentato Princeton.

Grazie a <a href="http://www.kero.it" target="_blank">Neko</a> per la segnalazione!