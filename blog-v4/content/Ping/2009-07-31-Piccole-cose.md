---
title: "Piccole cose"
date: 2009-07-31
draft: false
tags: ["ping"]
---

Non ci avevo fatto caso, ma con iPhone OS 3.0 il <i>boot</i> termina con la schermata da sbloccare mediante il cursore a slitta. Prima terminava con la prima schermata di applicazioni e si doveva bloccare lo schermo, pena il rischio di attivare qualche applicazione per errore. Meglio adesso.

Altro. Il mio <i>jailbreak</i> perde da qualche parte, peggio di un rubinetto gocciolante. Nonostante stia cercando di configurare tutto per evitare connessioni indesiderate, di tanto in tanto qualcosa succede.

(Su un iPhone di prima generazione occorre bloccare l'accesso a Edge se non espressamente voluto, altrimenti si spendono patrimoni nel semplice controllo della rete da parte dell'apparecchio).

Piccole cose, che fanno piacere o irritare. È bello però che ci sia sempre qualcosa cui fare attenzione.