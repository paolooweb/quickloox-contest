---
title: "Lo Zen e l&rsquo;arte della disperazione<p>"
date: 2004-11-11
draft: false
tags: ["ping"]
---

Quando non riesci più a vendere un prodotto non resta che regalarlo<p>

Creative ha avviato una <a href="http://creativex.creative.com/promos/whs/sale.asp?cat=exchange&n=6">campagna</a> cosiddetta promozionale: porta il tuo iPod tra il 19 e il 21 novembre e te lo scambiamo gratis con uno Zen Micro.<p>

Nessun commento. Siamo troppo in basso.<p>

Grazie a Sergio per la segnalazione.<p>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>