---
title: "Voglio pagare"
date: 2003-04-23
draft: false
tags: ["ping"]
---

Chi non dà valore alle cose è difficile che ne abbia uno

“Vorrei un bel programma per risolvere un mio problema, bello, potente, che sia facile da imparare, però in italiano e con un manuale come si deve. Per favore, è urgente! Ah, naturalmente gratis”.

Di frasi così ne avrò lette a centinaia, ognuna con la variazione del caso. Quelli che a “gratis” aggiungono “o shareware” sottintendono palesemente “tanto è lo stesso”.

Non è lo stesso. Derubare Microsoft è un atto di pirateria che non andrebbe commesso mai, ma chi lo fa può addurre un sacco di motivazioni, non è questo il luogo per giudicarle fondate o meno. Derubare un singolo programmatore geniale che chiede solo il giusto corrispettivo per il suo impegno è un atto da poveracci. A cui peraltro non sono i soldi a mancare.

Fatti una visita a vogliopagare.com. È istruttivo. Sì, è gratis.

<link>Lucio Bragagnolo</link>lux@mac.com