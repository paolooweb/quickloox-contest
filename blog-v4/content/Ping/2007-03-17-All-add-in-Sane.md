---
title: "All add-in Sane"
date: 2007-03-17
draft: false
tags: ["ping"]
---

Ricevo, e pubblico, da <strong>Pierfausto</strong>.

<cite>Problema scanner risolto con <a href="http://www.sane-project.org" target="_blank">Sane</a>. L'installazione di suo non è semplicissima, anche affidarsi a Fink non è stata proprio una passeggiata, c'è stata la necessità di scaricare e compilare una marea di roba per poterlo far funzionare. Ho provato anche il port presente su <a href="http://darwinports.com/" target="_blank">Darwin Ports</a>, ma non riusciva a vedere lo scanner.</cite>

<cite>Avevo usato sane per la prima volta poco dopo aver comprato lo scanner più di 8 anni fa sulla mia Mandrake! Ed era fantastico perché aveva tantissimi controlli in più rispetto al driver originale!</cite>

<cite>8 anni! Un'era in termini informatici!</cite>

<cite>Non riesco a risalire ad una data precisa ma a conti fatti dovrebbe essere del 1999 e non credo di averlo pagato più di 100 mila lire.</cite>

<cite>Non potevo non condividere con te questa esperienza: faccio funzionare sul mio Mac Intel uno scanner di 8 anni fa! Con un'applicazione addirittura nativa, impiegando solo un'ora di tempo macchina, sono veramente emozionato.</cite>

<cite>In ogni caso è un altro punto messo a segno dall'Open Source.</cite>

<cite>Grazie Stallman!</cite>

Vorrei ripetere questo passaggio: <strong>faccio funzionare sul mio Mac Intel uno scanner di otto anni fa</strong>. Complimenti, Pier.