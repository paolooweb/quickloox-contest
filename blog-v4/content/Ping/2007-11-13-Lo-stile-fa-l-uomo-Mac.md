---
title: "Lo stile fa l'uomo (Mac)"
date: 2007-11-13
draft: false
tags: ["ping"]
---

<a href="http://web.mac.com/brethil/Brethils_Page/Thoughts/Thoughts.html" target="_blank">Dany</a> segnala (e fa bene) una <a href="http://optica-optima.blogspot.com/2007/11/drawers-icon-1.html" target="_blank">raccolta di icone traslucide</a> da usare per chi non è soddisfatto della gestione dei documenti da parte del Dock e usa il trucchetto di rinominare una cartella in modo che, stando sempre in primo piano, identifichi meglio gli <em>stack</em>.

Si trova anche una pagina con <a href="http://t.ecksdee.org/post/19001860" target="_blank">spiegazione meno giapponese</a> del tutto.

Su Mac OS X conta non solo fare, ma anche fare bene. :-)