---
title: "Tutto sommato un bel Panorama<p>"
date: 2005-02-26
draft: false
tags: ["ping"]
---

Per capire che siamo noi a doverci muovere, non Apple<p>

Il settimanale omonimo ha iniziato non so quale raccolta di dischi ottici, compatibili sia Mac che Windows. Disgraziatamente gli ultimi due dischi si sono rivelati solo compatibili Windows.<p>

Ci sono due scuole di pensiero. Una sostiene che Apple dovrebbe intervenire dovunque e comunque, tempestando di telefonate ogni organizzazione che commette un errore (dato che perde clienti).<p>

L&rsquo;altra dice che chi vende lo fa agli utenti, non ad Apple, e quindi ha senso se si muovono gli utenti.<p>

In questo caso si sono mossi gli utenti, che hanno scritto in massa a Panorama chiedendo cortesemente spiegazioni. Il settimanale ha promesso che a tutti sarà mandata la versione compatibile Mac dei due dischi incriminati.<p>

Uno a zero per la seconda scuola.<p>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>