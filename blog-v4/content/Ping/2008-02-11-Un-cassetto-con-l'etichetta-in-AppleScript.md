---
title: "Un cassetto con l'etichetta in AppleScript"
date: 2008-02-11
draft: false
tags: ["ping"]
---

Abbiamo già parlato di come si assegna una variabile, o di come si infila un valore in un cassetto, di modo che si possa riutilizzare a volontà.

AppleScript e Mac OS X hanno alcuni cassetti un po' particolari. Per esempio, gli Appunti (la Clipboard in lingua originale). La zona di memoria dove finisce la selezione su cui si è fatto Comando-C, o Comando-X.

Questo cassetto in AppleScript ha un nome non modificabile: <code>the clipboard</code>. È esattamente come lo si direbbe in un <em>pub</em> di Londra sorseggiando una birra tra amici. Come se sul cassetto ci fosse un'etichetta impossibile da staccare.

Per mettere qualcosa nella <em>clipboard</em>, si fa come per qualsiasi altro cassetto; è solo il nome, che non può cambiare. Lanciamo Script Editor, apriamo un nuovo documento e scriviamo:

<code>set the clipboard to "Come fare Comando-C sulla tastiera"</code>

Questo è il Copia in AppleScript (ricordo che in Script Editor si esegue uno script facendo Comando-R o un clic sul pulsante Esegui. Per fare l'incolla, bisogna decidere in che programma lo si fa (come con il mouse) e dire incolla, cioè paste, in inglese:

<code>tell application "TextEdit"</code>
	<code>activate</code>
	<code>paste</code>
<code>end tell</code>

In questo caso il programma è TextEdit, ma può essere qualunque altro. In mancanza di altre indicazioni, la finestra di TextEdit su cui si incollerà è quella sopra le altre.

A che serve, imparare a programmare per fare una cosa che si fa tranquillamente con un colpo di mouse? Per esempio, a farlo diecimila volte, se servisse, senza fondere il mouse. Per incollare una cosa diecimila volte, e per fare tante altre cose, serve impostare un <em>ciclo</em>. Lo faremo. :-)

A parte settimana scorsa, questi piccoli <em>tutorial</em> su AppleScript si susseguono ogni sette giorni.