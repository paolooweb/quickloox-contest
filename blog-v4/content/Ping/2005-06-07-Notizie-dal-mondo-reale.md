---
title: "Notizie dal mondo reale<p>"
date: 2005-06-07
draft: false
tags: ["ping"]
---

La differenza tra chi vive dentro le notizie e chi vive e basta<p>

Ho l&rsquo;impressione che la Lunga Marcia verso i processori Intel scatenerà un trimestre di puro caos primordiale e che i veri conti si dovranno fare dopo.<p>

Oggi, dopo l&rsquo;Annuncio, ho ricevuto due messaggi in iChat. Il primo:<p>

<cite>Ho sentito una cosa stranissima su La7. Vogliono mettere i Pentium nei Mac. Hai qualche fonte attendibile?</cite><p>

Il secondo:<p>

<cite>Io avevo intenzione di aspettare il portatile G5 e adesso mi tocca almeno un anno di attesa.</cite><p>

Come se i PowerBook G5 fossero previsti per una data qualsiasi.<p>

È ancora assai presto per dire una parola definitiva, ma per molti cambierà, tutto sommato, assai poco.<p>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>