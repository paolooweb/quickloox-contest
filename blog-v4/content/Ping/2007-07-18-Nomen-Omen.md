---
title: "N&#244;men &#212;men"
date: 2007-07-18
draft: false
tags: ["ping"]
---

Tra pochi giorni inizierà l'esposizione dell'All About Apple Museum a H&#244;ne, in Valle d'Aosta (durerà fino quasi a fine agosto e sabato 4 agosto passerò io a sproloquiare di storia di Apple, ma vabbeh).

Beh, l'organizzazione ha ancora una volta superato se stessa. Ci sarà da vedere e toccare <a href="http://www.allaboutapple.com/speciali/hone.htm" target="_blank">anche un iPhone</a>, subito ribattezzato iPh&#244;ne (pronunciare con la &#8220;o&#8221; lunga).

Veramente bravi. Chi non c'è, veramente, si perde un evento.