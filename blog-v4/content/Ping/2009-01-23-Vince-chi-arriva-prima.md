---
title: "Vince chi arriva prima"
date: 2009-01-23
draft: false
tags: ["ping"]
---

Il 9 gennaio 2007 Steve Jobs disse che Apple puntava a vendere dieci milioni di iPhone nel 2008.

A febbraio 2008 l'analista Toni Sacconaghi di Bernstein Research disse che l'obiettivo era <cite>ottimistico</cite> e che Apple ne avrebbe venduti <a href="http://blog.wired.com/business/2008/02/apple-wont-sell.html" target="_blank">7,9 milioni</a>.

David Bailey di Goldman Sachs a maggio 2008 stimava circa <a href="http://seekingalpha.com/article/79102-apple-iphone-users-to-reach-30-million-by-2010-goldman?source=yahoo" target="_blank">sette milioni di iPhone</a> venduti nel 2008.

Non mi dilungo. durante il 2008 Apple ha venduto 13,7 milioni di iPhone.

Appare evidente che chi parla più lontano dalla scadenza è quello che ha più ragione. O forse si limita a sapere di che cosa parla?