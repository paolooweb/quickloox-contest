---

date: 2009-06-06
draft: false
tags: ["ping"]
---

giugno 6, 2009| In Hard, Life, Nomi| Nessun Commento — Edit
Avanscoperta
Visite guidate per tutti all’All About Apple Museum di Quiliano per il settimo anniversario. In primo piano, protagonista, un PowerBook 100.

Il pubblico al museo di Quiliano.
giugno 6, 2009| In Hard, Life, Nomi| Nessun Commento — Edit
Qualcosa di personal
Sempre dal museo Apple di Quiliano, per la festa del settimo anno, uno scorcio di anni ottanta del personal computing.

Personal degli anni ottanta.
giugno 6, 2009| In Hard, Life, Nomi| Nessun Commento — Edit
Prima della gente
Una rara immagine del museo Apple di Quiliano senza visitatori. Le presentazioni stanno per terminare e tra poco i due cavi di alimentazione intrecciati a protezione dell’ingresso verranno rimossi.

Tra poco gli archeocomputer prenderanno vita.
giugno 6, 2009| In Life, Nomi| Nessun Commento — Edit
Il banco alimentare
All About Apple Museum compie sette anni. Tra una presentazione e l’altra, prima dell’apertura dell’esposizione, c’è anche la pausa-rinfresco a base di focaccia ligure.

Per fortuna almeno la focaccia non arriva dagli anni ottanta!
giugno 6, 2009| In Life, Nomi| Nessun Commento — Edit
Nostalgia canaglia
A Quiliano, per il settimo compleanno dell’All About Apple Museum, Carlo sta parlando della storia del personal computer.
Siamo alla Triade degli anni settanta, che governava il nascente mercato americano.
Si muove troppo per la stanza buia e il flash disturberebbe, quindi mi perdonerà se mi concentro sulla slide.

Regnavano in tre
giugno 6, 2009| In Hard, Life, Nomi| Nessun Commento — Edit
La corazzata Potëmkin
BlogWriter è una c…ata pazzesca!
giugno 6, 2009| In Uncategorized| Nessun Commento — Edit
Il futuro al museo
Alla festa del settimo compleanno dell’All About Apple Museum ci si trastulla con i nuovi lettori di e-book basati su inchiostro elettronico.
Il post è realizzato in via del tutto sperimentale con BlogWriter, 1,59 euro su App Store. Non garantisco dell’efficacia tipografica.