---
title: "Salvati dall’update"
date: 2001-12-06
draft: false
tags: ["ping"]
---

Perfino Punto Informatico si è accorto, dopo settimane, che il Cd di aggiornamento da Mac OS X 10.0 a Mac OS X 10.1 in realtà contiene un 10.1 pieno e che con i trucchetti giusti si può utilizzare il disco dell’update per fare a meno di un disco ufficiale di sistema e risparmiare una bella cifretta.
Nonostante gli sforzi di Apple ormai l’informazione circola e non tornerà sconosciuta.
Se fossi Steve Jobs, autorizzerei l’utilizzo del trucco ai soli (pochissimi) sfortunati che si sono visti cancellare il disco rigido da quello che avrebbe dovuto essere un semplicissimo installatore di iTunes. Per tutti gli altri vale, ovviamente, la regola che rubare è sbagliato, anche quando il padrone dell’auto lascia la portiera aperta.
Vale la pena di chiedersi chi prepari e chi controlli gli attuali installer di Apple; nel frattempo, tra sciocchi e ladruncoli da poco, il motto del giorno è “salvati dall’update”. Ognuno metta pure l’accento dove gli compete.

lux@mac.com