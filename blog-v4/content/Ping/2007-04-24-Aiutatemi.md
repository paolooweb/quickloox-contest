---
title: "Aiutatemi"
date: 2007-04-24
draft: false
tags: ["ping"]
---

Per motivi professionali ho rifatto un giro in Second Life, dopo molti e molti mesi. Anche stavolta mi è sembrato qualcosa per gente con molto tempo libero e un bisogno forte di sentirsi protagonista.

Niente di male, anzi, se <a href="http://www.secondlife.com/" target="_blank">Second Life</a> ha tutto questo successo si vede che è una idea azzeccata. Di mio, mi sono affrettato a ricaricare il mio <em>rogue</em> in <a href="http://www.wow-europe.com" target="_blank">World of Warcraft</a> e mi sono infilato nella prima caverna disponibile, a strisciare nell'ombra.

Il problema è mio, ché evidentemente ancora non capisco qualcosa. Aiutatemi.