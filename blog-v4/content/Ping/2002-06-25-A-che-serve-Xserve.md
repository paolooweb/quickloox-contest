---
title: "A che serve Xserve"
date: 2002-06-25
draft: false
tags: ["ping"]
---

Ora un server Apple è un’opzione da considerare. Almeno per una ragione

Ti salta il tuo economicissimo server Dell e chiami il servizio di supporto. Ti rispondono e ti spiegano molto cortesemente che il problema deve essere certamente del software e ti rimandano a Microsoft, visto che Windows lo fanno loro.
Chiami Microsoft, dove ti spiegano molto cortesemente che si tratta sicuramente di un problema hardware e ti rimandano al costruttore della macchina, cioè Dell.
Se invece hai un problema con un Xserve Apple, chiami Apple e l’assistenza non ha nessuna scusa per scappare.
Quando si parla di reti, avere l’integrazione tra hardware e software è un grande vantaggio. E questa è solo la prima delle ragioni.

<link>Lucio Bragagnolo</link>lux@mac.com