---
title: "La seconda Ping! Wave"
date: 2009-12-11
draft: false
tags: ["ping"]
---

<i>Ladies and Gentlemen</i>, Google mi assegna altri nove inviti per Wave, che metto prontamente a disposizione.

Chi desidera un invito <a href="mailto:lvcivs@gmail.com?subject=Da%20Ping!%20a%20Google%20Wave">mi scriva su Google</a>. Se le richieste sono superiori alla disponibilità, aspetto almeno ventiquattro ore e poi sorteggio. Chi ha già scritto senza ottenere inviti deve riscrivere (<i>sorry</i>, ma gli inviti sono pochi e devono andare a persone minimamente motivate, almeno da fare due clic).

Chi è disposto a offrire inviti propri, commenti questo <i>post</i> pubblicando un proprio indirizzo apposito, cui chi vorrà potrà scrivere. Agli altri ricordo che chi offre un invito lo fa per gentilezza e cortesia, senza impegno e con la possibilità di dire no senza doversi giustificare.

Ne ho guadagnato, finora, diversi contatti con persone piacevoli e interessanti e uno strumento di comunicazione ancora da scoprire pienamente, pieno di potenzialità e stimolante per farsi venire nuove idee. Auguro lo stesso a tutti. :-)