---
title: "Caro babbo Natale"
date: 2003-11-20
draft: false
tags: ["ping"]
---

Scrivere in anticipo assicura il successo. O dovrebbe

Caro babbo Natale,
non mi serve niente; ho già tutto. Quindi ti scrivo non tanto per farmi portare regali, quanto per farne io a te. Non stupirti.

Vorrei regalarti tutti quelli che si lamentano del costo di Mac OS X ma non hanno il coraggio di montarsi Linux, o Darwin, o FreeBSD, che sono gratis, e non dicono che Windows costa di più.

Vorrei regalarti tutti quelli che non possono fare a meno di Office. Perché invariabilmente pensano che sia un problema non solo loro ma di tutti, me compreso. Che vivo benissimo senza Office.

Vorrei regalarti quelli che protestano per la mancanza di una funzione in un programma e non hanno neanche guardato i menu.

Vorrei regalarti quelli che si arrabbiano perché hanno comprato un Mac di cui tre mesi dopo è uscita una versione superiore. E che tre anni fa hanno comprato un Mac di cui tre mesi dopo è uscita una versione superiore. E che sei anni fa…

Vorrei regalarti tutti quelli che propongono modifiche all’interfaccia non perché sia la cosa miglore da fare, ma perché a loro piace così e allora dovrebbero fare tutti come loro.

La lettera potrebbe durare ancora molto a lungo ma sarà già molto difficile così. Buon lavoro e tanti auguri natalizi.

<link>Lucio Bragagnolo</link>lux@mac.com

P.S.: mi raccomando, fai attenzione: lascia stare dove sono tutti quelli che preannunciano il fallimento di Apple. Portano una fortuna pazzesca ad Apple, visto che lo preannunciano da vent’anni e non ne azzeccano una.