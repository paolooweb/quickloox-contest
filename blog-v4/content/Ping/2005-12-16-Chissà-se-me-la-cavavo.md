---
title: "Benedetto il wireless<p>"
date: 2005-12-16
draft: false
tags: ["ping"]
---

A volte lavorare senza fili mette a disposizione un&rsquo;ultima via di scampo<p>

Succede una volta su cinquemila, ma succede. La mail stava partendo, per il destinatario sbagliato.<p>

Mi sono salvato disattivando AirPort in una frazione di secondo. Ci sarei riuscito ugualmente, con il cavo? Viva il wireless.<p>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>