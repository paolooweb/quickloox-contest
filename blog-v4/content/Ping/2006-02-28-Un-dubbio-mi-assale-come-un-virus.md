---
title: "Un dubbio m&rsquo;assale, come un virus"
date: 2006-02-28
draft: false
tags: ["ping"]
---

Ma quelli che montano un antivirus sul Mac per intercettare i virus Windows in transito, almeno si fanno pagare come coamministratori di rete?