---
title: "Allo scettico d&rsquo;occasione<p>"
date: 2005-04-16
draft: false
tags: ["ping"]
---

Prima di parlare, sappi di che cosa stai parlando<p>

Credo lo si sappia, ma sul prossimo numero di <em>Macworld</em> ci sarà in omaggio un simpatico libro di cento pagine interamente dedicato a Tiger e alle sue nuove funzionalità.<p>

Quando esce una nuova versione di Mac OS si assiste all&rsquo;uscita dal letargo di un personaggio tipico: lo scettico d&rsquo;occasione.<p>

Normalmente è un incompetente che ne sa poco o anche meno, e parte con un pistolotto sul costo eccessivo del software, che alla fine è solo un aggiornamento, che se ne può benissimo fare a meno, che Apple dovrebbe trattare meglio la sua clientela, che sarà inutilmente pesante, che sarà troppo lento per la sua macchina eccetera.<p>

Lo scettico d&rsquo;occasione si distingue dal fatto che un critico avanza obiezioni specifiche e documentate, mentre lui se la cava con un&rsquo;alzata di spalle e poche frasi fatte, evidentemente collaudate in innumerevoli discussioni da bar sport.<p>

Prima di bofonchiare, lo invito a leggersi le novità di Tiger così come sono riassunte rapidamente in una <a href="http://www.apple.com/macosx/newfeatures/newfeatures.html">paginetta</a> sul sito Apple. E poi a leggersi Macworld e libro omaggio, per farsi un&rsquo;opinione vera.<p>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>