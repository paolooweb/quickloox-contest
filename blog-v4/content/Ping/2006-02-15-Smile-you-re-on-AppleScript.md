---
title: "Smile, you&rsquo;re on AppleScript"
date: 2006-02-15
draft: false
tags: ["ping"]
---

<a href="http://www.satimage-software.com" target="_blank">Satimage</a> &egrave; alla ricerca di buoni programmatori AppleScript.

La societ&agrave; alimenta da tempo la crescita di Smile, un editor per sviluppare AppleScript che &egrave; diversi chilometri avanti a Script Editor. Sul loro sito si trova appunto Smile, che &egrave; gratis, e diverso software di livello professionale per scienza e matematica.

Gli interessati alla programmazione possono scrivere a satimage@mac.com per descrivere le loro capacit&agrave; e sapere di pi&ugrave; su tempi e modi. Io, interessato pi&ugrave; che altro a buon software, ne approfitto per vedere se ho uno Smile aggiornato.