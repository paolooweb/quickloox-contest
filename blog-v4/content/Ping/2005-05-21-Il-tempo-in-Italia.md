---
title: "Il tempo in Italia<p>"
date: 2005-05-21
draft: false
tags: ["ping"]
---

Arrivano anche i widget in italiano. Basta volerli

Il gentilissimo Andrea Fistetto ha apportato, come dice lui, un piccolissimo contributo alla famiglia italiana degli utenti Mac e ha <a href="http://www.blogmac.it/index.php?Act=tutorial">localizzato</a> il widget (il genietto? Il manzanillo?) di Dashboard dedicato al meteo.<p>

Senza voler sminuire in alcun modo il lavoro di Andrea (anzi, complimenti), italianizzare widget non è difficile e neanche scriverne di nuovi. Più ne abbiamo, più i nostri Mac potranno esserci utili, o divertenti. Forza allora!<p>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>