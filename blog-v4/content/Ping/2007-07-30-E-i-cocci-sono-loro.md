---
title: "E i cocci sono loro"
date: 2007-07-30
draft: false
tags: ["ping"]
---

Chris Pirillo di Lockergnome ha pubblicato questa <a href="http://www.lockergnome.com/nexus/dabrace1984/2007/07/29/windows-operating-system-rant/" target="_blank">tabellina</a>:

<ul compact="compact" type="disc">
	<li>Windows 98 - giugno 1998</li>
	<li>Windows 98 Se - maggio 1999 (<strong>11</strong> mesi dopo)</li>
	<li>Windows 2000 - febbraio 2000 (<strong>9</strong> mesi dopo)</li>
	<li>Windows Me - settembre 2000 (<strong>6</strong> mesi dopo, <strong>13</strong> mesi dopo l'ultima edizione non business)</li>
	<li>Windows Xp - ottobre 2001 (<strong>13</strong> mesi dopo)</li>
	<li>Windows Vista - gennaio 2007 (<strong>62</strong> mesi dopo)</li>
</ul>

Che cosa si è rotto? :-)