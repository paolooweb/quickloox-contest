---
title: "Emula che ti passa<p>"
date: 2005-10-18
draft: false
tags: ["ping"]
---

Soprassalti notturni di adolescenza senile<p>

Ho ceduto a una vecchia passione e mi sono baloccato per un&rsquo;oretta con <a href="http://k1.dyndns.org/">zxsp</a>, perfetto emulatore di Sinclair Spectrum su Mac OS X.<p>

Una volta riuscivano a inserire in meno di 48K un interprete Lisp fatto e finito, con tanto di capacità di richiamare routine in linguaggio macchina. Un prodigio.<p>

Oggi a momenti 48K è la dimensione di un documento vuoto.<p>

Meglio adesso, eh? Una volta c&rsquo;erano i registratori a cassette, per registrare.<p>

Però, quanta magia.<p>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>