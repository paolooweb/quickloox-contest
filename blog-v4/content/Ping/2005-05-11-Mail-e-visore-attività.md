---
title: "Perché cambiare Mail?<p>"
date: 2005-05-11
draft: false
tags: ["ping"]
---

Tiger ha portato grosse novità nella posta elettronica via Mac. Non sempre gradite<p>

Mi scrive Stefano:<p>

<cite>Niente da dire su tiger, ogni giorno una scoperta.</cite><p>

<cite>Ma perché cambiare le cose quando funzionano? in Mail il Visore attività era integrato nella finestra principale. oggi devi aprire Visore attività per valutare a che punto è il tuo invio. Tutto bene, ma è una finestra in più da gestire.</cite><p>

Se c&rsquo;è una cosa che non seguo di Mac OS X è esattamente Mail. Vivo felice con Malismith di Bare Bones da quasi dieci anni e non penso di potere avere di più. Quindi, se ci sono segnalazioni ulteriori e commenti interessanti su Mail, ben vengano.<p>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>