---
title: "E liberaci dai test<p>"
date: 2005-07-14
draft: false
tags: ["ping"]
---

Soprattutto quando si mettono a confronto le mele con i prototipi<p>

Leggo con sgomento di test di velocità effettuati su qualche Mac Intel-based, messo a confronto con le macchine Apple odierne.<p>

I Mac Intel-based di oggi sono computer talmente provvisori che Apple neanche li vende, ma semplicemente li noleggia agli sviluppatori, i quali dovranno restituirle al mittente per fine 2006. Il perché è ovvio: i veri Mac con processore Intel, quelli che arriveranno nei negozi, avranno architetture ottimizzate, processori ben diversi da quelli di oggi e chissà quali altre novità.<p>

I Mac del futuro, oggi, sono poco più che prototipi, messi insieme per consentire ai programmatori di eseguire prove pratiche. Eppure vengono confrontati con macchine mature, collaudate, con i processori di oggi. Da una parte il software è nativo, dall’altra emulato.<p>

Ci si chiede quale sia il significato di queste prove, a parte l’ossessione per il numero. Come cronometrare sui cento metri un atleta in forma da un anno e uno che ha cominciato ad allenarsi il giorno prima. Mah.<p>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>