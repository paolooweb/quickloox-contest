---
title: "Via da Telecom con Sip"
date: 2007-08-07
draft: false
tags: ["ping"]
---

Quanti lavorano in aereo passano molto tempo a distanze siderali da casa e cercano costantemente la soluzione migliore per chiamare mogli e mariti senza spendere capitali.

<strong>Odino</strong> manda questi suggerimenti per usare rapidamente X-Lite in modo integrato con la Rubrica indirizzi:

<cite>Dopo varie ricerche ho trovato la soluzione al mio problema: ovvero usare VoipStunt o similari (i quali permettono di chiamare i telefoni fissi taliani e di altri paesi gratis) senza dover ricorrere a Winzoz.</cite>

<cite>Per prima cosa andate su <a href="http://www.counterpath.com/index.php?menu=download" target="_blank">Counterpath</a></cite>

<cite>e scaricate il vostro videotelefono Sip. Poi, se usate Voipstunt, scaricate i <a href="http://www.voipstunt.com/en/sipp.html" target="_blank">parametri di configurazione</a>.</cite>

<cite>Non contento, ho trovato <a href="http://www.faqintosh.com/risorse/it/othutil/addrbook/xlite/" target="_blank">uno script</a> che permette di chiamare usando la Rubrica Indirizzi</cite>.

<cite>Buone telefonate a tutti!</cite>

Io telefono poco o niente e non ho capito veramente se serve avere hardware o se la soluzione è via software. Ora darò anche un'occhiata a X-Lite. Intanto grazie mille! Il paradosso che a liberarci dalla indecorosa Telecom possa essere un servizio chiamato Sip è grazioso.