---
title: "Un secondo, per favore<p>"
date: 2005-10-08
draft: false
tags: ["ping"]
---

Campagna a favore della biodiversità del software<p>

Sul mio computer ho cinque browser Web: Firefox, iCab, Lynx, OmniWeb e Safari. Vivo nel Mac e ho consistenti deformazioni professionali, per cui spero di essere capito. Non è necessario avere cinque browser per vivere.<p>

Ma sento gente che inorridisce all&rsquo;idea di averne due. Per esempio, uso Safari per quasi tutto e poi certe cose vanno su Firefox. Non solo perché magari funzionano lì e non su Safari; a volte l&rsquo;interfaccia migliore è quella di un programma, a volte è quella di un altro.<p>

L&rsquo;idea di avere due programmi per fare una stessa cosa (e approfittare del meglio che c&rsquo;è da tutte e due le parti) viene vista come fumo negli occhi. <cite>Non ho spazio</cite> (ragazzi, su un disco da cinquanta giga stanno mille programmi da cinquanta mega&hellip; non scherziamo), <cite>troppo difficile</cite> (?), <cite>mi scoccia</cite>, <cite>ne vorrei uno solo</cite> (sì, ma la ragione qual è?).<p>

Poi pranzi a casa loro e apparecchiano con un coltello per la carne, uno per il pesce, uno per il dolce, uno per tagliare il pane eccetera.<p>

L&rsquo;unico posto dove tutto si deve fare con uno strumento solo è il computer. Ma perché?<p>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>