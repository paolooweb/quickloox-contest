---
title: "Corso di salvataggio"
date: 2006-11-15
draft: false
tags: ["ping"]
---

Mi chatta <a href="http://www.rabellino.it/blog" target="_blank">Gianugo</a> e dice <cite>Devo aprire una trentina di presentazioni Keynote ed esportarle in PowerPoint. Non è che c'è un modo per automatizzare la cosa?</cite>

Io sto già caricando il dizionario AppleScript di Keynote e faccio lo splendido. <cite>Gianugo, mi spiace, ma il comando di menu per esportare da Keynote in formato PowerPoint è Esporta, che nel dizionario AppleScript non c'è. C'è solo Registra col nome, che però non ti serve.</cite>

in chat non si vede, ma Gianugo grugnisce pensoso. <cite>Uhm</cite>.

Sparisce per un po', poi torna:

<code>tell application "Keynote"
save ":Users:rabellino:Desktop:presentazione.key" as "powerpoint" in ":Users:rabellino:Desktop:presentazione.ppt"
end tell</code>

Cos&#236; imparo a fare lo splendido. Limitandomi a ripetere la teoria, avrei perso il paziente. Invece, per salvarlo, bastava essere curiosi.