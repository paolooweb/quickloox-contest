---
title: "Di AppleScript non capisco un (You)Tube"
date: 2008-05-14
draft: false
tags: ["ping"]
---

Dato per scontato che tutto si può fare con una applicazione già pronta come <a href="http://stinkbot.com/Tubesock/" target="_blank">TubeSock</a>, ha ugualmente valore darsi la soddisfazione personale di creare tutta da soli un'applicazione che scarica sul Mac i filmati di YouTube. La competenza richiesta è quella di un po' di superversatile AppleScript e buona volontà.

<code>set pageURL to display dialog "Inserisci l'Url della pagina di YouTube con il video che vuoi scaricare:" default answer "</code>

Crea una finestra di dialogo nel quale andrà inserito l'indirizzo della pagina che contiene il video da scaricare (quello nel campo indirizzo della finestra di Safari, niente cose strane).

<code>set AppleScript's text item delimiters to "?v="
set v_id to item 2 of text items of text returned of pageURL
set pageURL to "http://www.youtube.com/v/" &#38; v_id</code>

I <em>text item delimiter</em>, delimitatori, impostano come dei semafori, o dei posti di blocco. Sequenze riconosciute come particolari da AppleScript. Che può fare cose come, per esempio, compiere operazioni su quello che sta prima del delimiter e quello che sta dopo. Se il <em>delimiter</em> compare più volte, AppleScript può contare gli elementi di testo che stanno tra un <em>delimiter</em> e l'altro. In questo blocco, la seconda istruzione lavora sull'elemento di testo numero due. La terza istruzione imposta una variabile che contiene un indirizzo web (un Url) frutto dell'elaborazione del tutto.

<code>set locationURL to do shell script "curl -I " &#38; pageURL &#38; " | grep ^Location | grep -o 'video_id=.*'"
set AppleScript's text item delimiters to "&#38;t="
set t_id to item 2 of text items of locationURL</code>

La prima istruzione è interessante. Con il comando <code>do shell script</code>, AppleScript può azionare la potenza del Terminale e dare comandi Unix, come <code>curl</code>, che scarica documenti dal web. Scrivere <code>man curl</code> nel Terminale spiega tutto (in modo complicato, ma cos&#236; è la vita Unix). La seconda istruzione rifà il trucco dei delimitatori, cambiando delimitatore. La terza estrae un pezzo di testo ottenuto lavorando con il delimitatore di cui sopra.

<code>set downloadURL to "http://youtube.com/get_video?video_id=" &#38; v_id &#38; "&#38;t=" &#38; t_id</code>

Mette insieme un link con quanto ottenuto nelle due fasi di lavoro precedente.

<code>open location downloadURL</code>

Apre l'Url ottenuto con tanta fatica e scarica finalmente il video.

Ecco lo script completo:

<code>set pageURL to display dialog "Inserisci l'Url della pagina di YouTube con il video che vuoi scaricare:" default answer "

set AppleScript's text item delimiters to "?v="
set v_id to item 2 of text items of text returned of pageURL
set pageURL to "http://www.youtube.com/v/" &#38; v_id

set locationURL to do shell script "curl -I " &#38; pageURL &#38; " | grep ^Location | grep -o 'video_id=.*'"
set AppleScript's text item delimiters to "&#38;t="
set t_id to item 2 of text items of locationURL

set downloadURL to "http://youtube.com/get_video?video_id=" &#38; v_id &#38; "&#38;t=" &#38; t_id

open location downloadURL</code>

Registriamo pure lo script come applicazione, in modo che parta direttamente con un doppio clic senza tante storie.

Quando lo facciamo partire, chiede l'indirizzo del filmato YouTube. Poi si mette a lavorare per conto suo per un po' (non è rotto, sta lavorando. La mania di interrompere ansiosamente i programmi che lavorano senza dare feedback è comprensiblle, e deleteria). Potrebbe metterci anche qualche minuto. Alla fine Safari (o Firefox) inizia a scaricare un file chiamato <code>get_video</code>.

È lui.

Scaricato il video, convertiamolo come serve con un programma tipo <a href="http://isquint.org/" target="_blank">iSquint</a>. Missione compiuta.

Lo <a href="http://www.tuaw.com/2008/05/12/applescript-download-youtube-videos/" target="_blank">script originale</a> è opera del magico Cory Bohon dell'Unofficial Apple Weblog.

Il primo filmato che suggerisco di scaricare è quello di David Pogue che <a href="http://www.youtube.com/watch?v=MDNuq94Zg_8" target="_blank">mostra le similarità tra Mac OS X e Vista</a> e conclude ironicamente che Vista non ha copiato da Mac OS X, perché Spotlight è uguale alla ricerca di Vista ma sta in alto a destra e invece la ricerca di Vista sta in basso a sinistra&#8230;

Parlo di AppleScript nei giorni del mese divisibili per sette.