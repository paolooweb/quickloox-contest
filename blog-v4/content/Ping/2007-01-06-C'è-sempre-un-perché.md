---
title: "C'è sempre un perché"
date: 2007-01-06
draft: false
tags: ["ping"]
---

Anche nel 2007, lo scopo di tenere conto dell'andamento degli update ha lo scopo di mostrare, attraverso la coerenza degli esiti, che gli update a Mac OS X sono consigliabili, sicuri e, se e quando dessero effetti collaterali spiacevoli, il problema sta localmente nel computer specifico e non globalmente nell'update.

È, per l'ennesima colta, il caso di <a href="http://docs.info.apple.com/article.html?artnum=304899" target="_blank">iChat Update 1.0</a>, che è stato applicato con pieno successo e fa quello che deve.

E adesso possiamo anche riprendere a fare chat cifrata. :-)