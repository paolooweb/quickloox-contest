---
title: "Le trasmissioni riprenderanno il più presto possibile"
date: 2008-02-18
draft: false
tags: ["ping"]
---

Il Mac non c'entra niente, ma notifico che la pubblicazione di Ping! è fortemente penalizzata da un guasto che ha tolto telefono e Adsl al mio intero quartiere da mercoled&#236; scorso.

Suppongo che sia anche il motivo per il quale non mi si è ancora manifestato l'Aggiornamento Software per Mac OS X 10.5.2.

Mi consolo vedendo il Dock con 38 applicazioni attive e intanto rifletto su come è cambiato il mondo con la rete. Il disagio è quasi pari a quello che si sarebbe se mancasse l'acqua.

Dopo cinque giorni si blackout mi sto anche abbastanza arrabbiando e sono l&#236; l&#236; per cambiare provider, mica perché cambierebbe qualcosa, giusto per punizione. È che, se veramente la linea è guasta, passare a un nuovo fornitore Internet in una situazione di malfunzionamento mi fa temere di entrare in una spirale catastrofica.

In @Work è arrivato MacBook Air e l'interesse della clientela è a dire poco esagerato.

Oggi va un po' cos&#236;, a flusso di coscienza, godendosi FastWeb ini fibra un po' come la maschera a ossigeno dopo avere scalato un ottomila.