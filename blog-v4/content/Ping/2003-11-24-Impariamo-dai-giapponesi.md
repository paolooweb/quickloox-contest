---
title: "Imparare dai giapponesi"
date: 2003-11-24
draft: false
tags: ["ping"]
---

All’Università di Tokyo dovevano cambiare un po’ di computer e hanno scelto…

Le università, si sa, sono mostri di burocrazia. Così può accadere che l’ateneo di Tokyo decida di cambiare oltre un migliaio di vecchi computer Linux ma l’amministrazione imponga una scelta tra Mac OS X e Windows.

Le università, si sa, sono luoghi in cui si fa andare il cervello. Così, dovendo scegliere, hanno ordinato 1.150 iMac. I motivi? Le sue radici Unix sono più familiari e accessibili per l’uso ordinario e la manutenzione, e in giro c’è un mare di programmi Unix gratuiti.

Forse non tutti siamo giapponesi e non abbiamo familiarità con Unix, ma qualcosa da imparare dai nipponici ce l’abbiamo lo stesso.

<link>Lucio Bragagnolo</link>lux@mac.com