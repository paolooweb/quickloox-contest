---
title: "Processi agli occupanti"
date: 2007-04-11
draft: false
tags: ["ping"]
---

In un momento di particolare sollecitazione del PowerBook, ho provato a vedere quali sono i processi, o i programmi, che più richiedono risorse al sistema.

È una cosa che si constata facilissimamente con Monitoraggio Attività senza alcun bisogno di toccare il Terminale.

Tutto quello che contiene una riga di Java succhia memoria virtuale in quantità. Dopo Safari (2,6 giga) c'era infatti Java (2,36 giga). Poi il processo kernel_task (1,23 giga) e poi in successione PcGen, Robocode, soffice (Open Office) e Cyberduck. Tutta o quasi roba con dentro Java.

Nell'occupazione di memoria reale, sempre Safari al primo posto (389,62 mega), davanti a kernel_task (199,99 mega), Vienna (167,23), il processo WindowServer e, pensa, Adium.

Come percentuale di Cpu, Safari (29,5 percento), World of Warcraft (21,1 percento) e WindowServer (9,4 percento).

In conclusione: Java è meraviglioso, almeno quanto è pesante. Safari giustifica quasi da solo la dotazione del Mac. World of Warcraft può anche essere lasciato acceso, a patto di non avere compiti Cpu-intensive per le mani.

Ovviamente tutto ciò rispecchia le mie abitudini d'uso. Il numero di finestre di Safari che tengo aperte tende infatti all'esagerazione. :-)