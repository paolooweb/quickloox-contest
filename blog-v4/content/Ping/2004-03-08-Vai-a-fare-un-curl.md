---
title: "Vai a fare un curl"
date: 2004-03-08
draft: false
tags: ["ping"]
---

Dove si capisce a che cosa serve il Terminale e a che cosa no

Ieri pomeriggio un mio amico, appassionato di manga giapponesi, si è scaricato 125 schermate da un sito dedicato al genere.

Lo ha fatto con un singolo comando da Terminale, curl.

Mi è sembrata una gran dimostrazione della grande utilità dell’interfaccia grafica, nonché del grande vantaggio di avere sotto i piedi il Terminale.

L’interfaccia grafica rende semplici le cose, mentre un comando Unix scatena la potenza quando serve.

<link>Lucio Bragagnolo</link>lux@mac.com