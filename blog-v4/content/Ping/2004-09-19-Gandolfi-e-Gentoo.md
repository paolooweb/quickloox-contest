---
title: "Gentoo (non) di passaggio<p>"
date: 2004-09-19
draft: false
tags: ["ping"]
---

Aumentano e migliorano i sistemi per installare su Mac software open source proveniente da Linux<p>

Alla festa di <a href="http://www.macatwork.net">Mac@Work</a> ho avuto il piacere di fare una bella chiacchierata con Carlo Gandolfi, animatore di <a href="http://www.freesmug.org">FreeSmug</a>, che mi ha segnalato il porting del package manager di <a href="http://www.metadistribution.org/macos/">Gentoo su Mac OS X</a> (una cosa come Fink e OpenDarwin, probabilmente più flessibile) e l&rsquo;apparizione di un eccellente editor visuale Html completamente gratuito come <a href="http://nvu.com">Nvu</a>. Completa l&rsquo;eccellenza testuale di BBEdit, è gratis al contrario di Contributor e fa quello che fanno DreamWeaver o GoLive senza dare l&rsquo;idea che occorra per forza una laurea in ingegneria nucleare.<p>

Riparlerò di software libero perché, se è vero che che la sua user friendliness è relativa e non ha solo pregi ma anche qualche difetto, è anche vero che sta crescendo in sapienza e saggezza. <cite>Open source</cite>, ha detto qualcuno, <cite>is just a matter of time</cite>. E il tempo, pare, non sta passando invano.<p>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>