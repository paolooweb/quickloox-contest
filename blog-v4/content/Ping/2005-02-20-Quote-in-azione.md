---
title: "Curiosità in azione<p>"
date: 2005-02-20
draft: false
tags: ["ping"]
---

Chi dispone dei titoli Apple non è sempre chi ci aspetteremmo<p>

Il maggiore azionista Apple è nientemeno che la società Private Capital Management, con il 5,09 percento delle azioni totali. È interessante, almeno per me, che non esistano altri soci con quote maggiori di questa, e tutti gli altri siano abbondantemente sotto il 5 percento.<p>

Una quota del genere ce l&rsquo;ha anche Steve Jobs, ma lui è in una situazione ben diversa, perché per esempio non ha la possibilitò di vendere a volontà. Insomma, la gente che crede in Apple (finanziariamente parlando) forma un <a href="http://finance.yahoo.com/q/mh?s=AAPL">panorama</a> variegato, eppure ristretto.<p>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>