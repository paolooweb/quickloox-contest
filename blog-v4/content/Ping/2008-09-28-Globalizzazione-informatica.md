---
title: "Globalizzazione informatica"
date: 2008-09-28
draft: false
tags: ["ping"]
---

Un quarto di secolo fa parlavi di un nuovo programma interessante e sicuramente c'era di mezzo un ragazzo californiano.

Oggi il mondo è cambiato e non di poco. Istruttiva, per quanto elementare, <a href="http://macenstein.com/default/archives/1690" target="_blank">l'intervista che Macenstein ha dedicato a Christina K</a>.

Ragazza, estone. Autrice del gradevole Fuzzle per iPod touch.