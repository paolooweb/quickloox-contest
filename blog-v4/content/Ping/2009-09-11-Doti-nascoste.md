---
title: "Doti nascoste"
date: 2009-09-11
draft: false
tags: ["ping"]
---

All'Apple Store Carosello di Carugate sembra di primo acchito che manchino gli accessori, se si eccettua un murale di custodie per iPhone e iPod. Invece sono celati sotto i tavoli di esposizione di accessori più grossi.

Gli spazi sono sfruttati con grande abilità e coerenza.