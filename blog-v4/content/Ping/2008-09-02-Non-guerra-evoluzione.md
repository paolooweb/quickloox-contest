---
title: "Non guerra, evoluzione"
date: 2008-09-02
draft: false
tags: ["ping"]
---

Si è sempre parlato di guerra dei browser.

A me pare più evoluzione.

Vedo Safari, basato su WebKit. Il browser <a href="http://www.S60.com/browser/" target="_blank">N60</a>, basato su WebKit. Ora arriva il browser di Google, <a href="http://www.google.com/googlebooks/chrome/" target="_blank">Chrome</a>, basato su WebKit (chi se non Google poteva annunciare un nuovo browser con un racconto a fumetti?). <a href="http://www.konqueror.org/" target="_blank">Konqueror</a> di Linux è basato su Khtml, il progenitore di WebKit, che finirà per fondersi con WebKit.

<a href="http://webkit.org/" target="_blank">WebKit</a> si è dimostrato adatto per produrre ottimi browser su qualsiasi piattaforma, compresi cellulari e computer da tasca.

<a href="http://www.mozilla.org/newlayout/" target="_blank">Gecko</a>, il motore di Firefox, sta solo su Firefox e sta diventando enorme.

Il motore <a href="http://msdn.microsoft.com/en-us/library/aa741317.aspx" target="_blank">Trident</a> di Internet Explorer lo è sempre stato e ha una storia di inadeguatezza.

Più che alla belligeranza, tutto questo mi fa pensare all'evoluzione. Ai mammiferi e ai dinosauri.