---
title: "Vai dove ti porta Microsoft<p>"
date: 2005-01-15
draft: false
tags: ["ping"]
---

Software che ti guida passo per passo? Ecco quello giusto<p>

È notorio che Microsoft non possa accettare l&rsquo;idea di non essere monopolista in qualcosa, per cui offre anche <a href="http://mappoint.msn.com/DirectionsFind.aspx">mappe e itinerari</a>.<p>

Mi segnala l&rsquo;amico Arnaldo:<p>

<em>Come città di partenza metti Haugesund in Norvegia</em>
<em>Come destinazione metti Trondheim, Norvegia</em><p>

<em>Ora calcola il percorso cliccando su &ldquo;Get Directions&rdquo;&hellip;</em><p>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>