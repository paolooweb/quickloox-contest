---
title: "Per farsi licenziare"
date: 2007-10-15
draft: false
tags: ["ping"]
---

In cerca di una pagina il più possibile piena di software inutile e improduttivo e però talmente stupido da meritare un'occhiata e magari finire per caderci dentro, cos&#236; che la pausa pranzo si estende fino a quella per la merenda? Voto tutta la vita <a href="http://home.comcast.net/~jeff.ulicny/software/entertainment.html" target="_blank">Big, Fat, Stinking Software</a>, in cui brilla la stella AsciiPet, che toglie il mistero a una questione essenziale: come funziona un Tamagotchi?