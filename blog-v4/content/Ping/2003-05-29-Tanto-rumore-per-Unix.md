---
title: "Tanto rumore per Unix"
date: 2003-05-29
draft: false
tags: ["ping"]
---

Tutta questa storia di Sco, Linux e i diritti di Unix fa venire il latte alle ginocchia

Postulato di lux: quando nel mondo informatico tutti iniziano a parlare di una notizia, lo notizia è stata data male, o è stata capita male, o è falsa, o è creata ad arte per ragioni di marketing.

Vale sempre e anche nel caso della questione dei supposti diritti che Sco vanterebbe su Unix, con minacce di cause a Ibm, ipotetici problemi per Linux, Microsoft che alza il ditino come la maestrina.

La cosa si va rapidamente sgonfiando come meritava da subito, per esempio con Novell che ha preso pubblicamente posizione facendo sapere che sono in effetti sue molte delle proprietà intellettuali accampate da Sco.

E ciò introduce il Secondo postulato di lux: nel mondo informatico, più rapidamente si diffonde una notizia, più rapidamente si sgonfia.

E il software libero rimane tale.

<link>Lucio Bragagnolo</link>lux@mac.com