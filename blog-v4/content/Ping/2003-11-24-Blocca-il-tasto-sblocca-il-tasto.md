---
title: "Blocca il tasto, sblocca il tasto"
date: 2003-11-24
draft: false
tags: ["ping"]
---

A volte un tasto di iPod non risponde più al tocco

Può capitare che uno dei tasti a sfioramento di iPod a volte si incanti. Quasi sempre la cosa si risolve bloccando i tasti con il controllo Lock e poi sbloccandolo subito dopo, dato che iPod calibra i tasti a sfioramento tutte le volte che viene tolto il Lock. La soluzione del problema è anche la sua causa; molto probabilmente, l’ultima volta che si è tolto il Lock, uno dei tasti era accidentalmente toccato e iPod lo ha interpretato come stato normale del tasto non azionato, dopo di che ovviamente non riesce più a vedere la differenza corrispondente allo sfioramento volontario.

I curiosi più pignoli possono leggersi la <link>nota tecnica</link>http://docs.info.apple.com/article.html?artnum=93341 di Apple in proposito. iPod non è solo intelligente, come si vede, ma anche sensibile. Quasi un animaletto da compagnia.

<link>Lucio Bragagnolo</link>lux@mac.com