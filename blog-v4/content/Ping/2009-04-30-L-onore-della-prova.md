---
title: "L'onore della prova"
date: 2009-04-30
draft: false
tags: ["ping"]
---

Chattavo con un amico mesi fa e lo consigliai di spedire un curriculum ad Apple. Non ci credeva, pensava di avere davanti un sacco di altra gente, che le strade fossero chiuse.

Gli dissi che provare si deve sempre e bisogna avere paura non di sbagliare, ma dell'inazione. Il vero errore è non fare niente.

Prossimamente l'amico potrebbe diventare dipendente Apple.

Il merito è tutto suo e del suo curriculum.

Penso ad altri amici che meritano ma non stanno raccogliendo il giusto e li invito a provare. Si deve sempre, comunque.

Vedo Apple che si accinge ad aprire l'Apple Store di Milano. E vedo progetti interessanti di cui non posso parlare.

Non è solo crisi. Ma bisogna provare.