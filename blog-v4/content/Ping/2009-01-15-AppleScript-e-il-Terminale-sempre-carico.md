---
title: "AppleScript e il Terminale sempre carico"
date: 2009-01-15
draft: false
tags: ["ping"]
---

Piccola esagerazione, <a href="http://www.entropy.ch/blog/Mac+OS+X/2008/08/07/Open-Terminal-Here-Finder-Keyboard-Command.html" target="_blank">interessante AppleScript</a>. Fa in modo che il Terminale si apra già puntato sulla <em>directory</em> corrispondente alla cartella in primo piano nel Finder. Semplice ma non banale.

Marc Liyanage, il bravissimo sviluppatore autore di questo trucchetto, nota giustamente che è meglio se si dispone di un meccanismo capace di azionare un AppleScript mediante una scorciatoia di tastiera. Lui segnala <a href="http://www.red-sweater.com/fastscripts/" target="_blank">FastScripts</a>. Che potrebbe anche essere <a href="http://blacktree.com/?quicksilver" target="_blank">QuickSilver</a>.