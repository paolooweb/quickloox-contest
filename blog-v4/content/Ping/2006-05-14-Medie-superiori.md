---
title: "Medie superiori"
date: 2006-05-14
draft: false
tags: ["ping"]
---

Ho aggiornato a QuickTime 7.1 e Security Update 2006-003 (se non erro). Anche per tranquillizzare Piergiovanni, nessunissimo problema di riavvio.

S&igrave;, il Security Update ha richiesto ufficialmente un riavvio. I riavvii non desiderati restano due dal primo gennaio. E sono passati 133 giorni, quindi mediamente un riavvio non richiesto ogni 66 giorni. Sono pi&ugrave; di due mesi. Le medie si stanno alzando e, per Mac OS X, sono sempre pi&ugrave; lusinghiere.