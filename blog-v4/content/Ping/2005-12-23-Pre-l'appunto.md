---
title: "Per l&rsquo;appunto<p>"
date: 2005-12-23
draft: false
tags: ["ping"]
---

Quelle utility che sono talmente piccole da cambiare in grande le cose<p>

Ogni tanto <strong>Mario</strong> è talmente gentile che mi segnala un software azzeccato e freeware, come <a href="http://www.abracode.com/ClipMasterCM/">ClipMasterCm</a> di Abracode. Stavolta ha suggerito anche il titolo.<p>

Bisogna compiere un piccolo salto mentale, per passare dalla semplicità di un singolo copia-e-incolla alla versatilità di clipboard multiple accessibili via menu contestuale e, da utente Mac di vecchia data, capisco il mio scetticismo iniziale.<p>

Capisco anche perché, ora, non voglio più tornare indietro.<p>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>