---
title: "L'enigma del menu"
date: 2009-05-01
draft: false
tags: ["ping"]
---

Questa immagine sembra innocente e invece alla seconda occhiata mette inquietudine come certi quadri di De Chirico.

L'ha prodotta <a href="mailto:me+ping@fearandil.com">Fearandil</a>. Si è collegato con <code>ssh</code> al computer, è diventato <em>superuser</em> con <code>sudo su</code>, ha dato il comando <code>open .</code> ed ecco che nella schermata di <em>login</em> del computer controllato appare il menu del Finder, per di più eseguito con permessi di <em>root</em>. Quando c'è la finestra di <em>login</em> non dovrebbe poter succedere nient'altro.

Abbiamo discusso un po' se sia un problema di sicurezza serio o meno e non abbiamo raggiunto un accordo pieno. Di sicuro non è una schermata che si veda tutti i giorni.