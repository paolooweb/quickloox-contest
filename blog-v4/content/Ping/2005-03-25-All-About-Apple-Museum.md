---
title: "Buono anche per il museo<p>"
date: 2005-03-25
draft: false
tags: ["ping"]
---

Non sono molti i marchi degni di tanto onore e tuttora al vertice<p>

Il 14 maggio aprirà il primo museo in Europa dedicato ai prodotti Apple. Non è una notizia nel senso che tutti dovrebbero saperlo già, ma nel mio piccolo vorrei fare le congratulazioni agli appassionati che gravitano intorno al gruppo di utenza <a href="http://www.allaboutapple.com">All About Apple</a>, da cui è partita l&rsquo;iniziativa.<p>

Il museo si trova presso la scuola media A. Peterlin in via Valletta di Vadone a Quiliano, provincia di Savona. Non so ancora se riuscirò a essere presente il giorno dell&rsquo;apertura. Ma mi piacerebbe proprio.<p>

Il bello è che secondo qualcuno Apple era roba da museo già nel 1985. Invece la Mela vive un periodo dorato e però il museo ce l&rsquo;abbiamo ugualmente. Tiè.<p>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>