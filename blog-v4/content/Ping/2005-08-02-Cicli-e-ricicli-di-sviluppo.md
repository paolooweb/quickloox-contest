---
title: "Cicli e ricicli, di sviluppo<p>"
date: 2005-08-02
draft: false
tags: ["ping"]
---

Anche il resto del mondo deve lavorare sulle cose prima di averle pronte<p>

Ogni tanto esce Macworld e il giorno dopo ricevo una lettera, che chiede risposta sul numero successivo della rivista. Lo si farebbe volentieri, ma i tempi redazionali lo impediscono. Quando esce un numero di Macworld, il numero dopo è praticamente già finito.<p>

La situazione ricorda certi delusi da Tiger o, meglio, dall&rsquo;aggiornamento a 10.4.1, che non rimediava a vari bug del sistema. Lo sviluppo di 10.4.1 è iniziato quando quello di Tiger non era ancora finito e non era possibile risolvere big che non erano ancora stati portati alla luce. Per questo Apple ha iniziato i bug fix da 10.4.2.<p>

In generale sono persone che, se gli chiedi qualcosa, ti dicono che ci vuole tempo. Per loro.<p>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>