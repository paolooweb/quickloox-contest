---
title: "Goffo marketing"
date: 2009-09-15
draft: false
tags: ["ping"]
---

Zune Hd di Microsoft ha iniziato le vendite negli Stati Uniti <a href="http://www.engadget.com/2009/09/15/zune-hd-unboxing-and-hands-on/" target="_blank">privo di software</a>.

Chi ha acquistato le prime unità non poteva farci niente, a parte guardare il demo video. Niente interfaccia, niente di niente, solo un pezzo di alluminio.

Tanta goffaggine mi ha ricordato il debutto di iPhone 3G in Italia, quando Tim e Vodafone non erano preparate e mandavano a casa la gente con l'iPhone, ma senza la Sim.

Solo che l&#236; il software c'era e la goffaggine non era di Apple.