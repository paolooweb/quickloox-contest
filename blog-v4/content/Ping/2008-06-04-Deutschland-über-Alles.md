---
title: "Deutschland &#252;ber Alles"
date: 2008-06-04
draft: false
tags: ["ping"]
---

Pochi popoli sono versatili come i tedeschi. <a href="http://www.fscklog.com/2008/05/die-schnittige.html" target="_blank">Affettano il pane con MacBook Air</a> e usano <a href="http://www.bosch-do-it.de/heimwerker/newsundextras/aktuelles/die-bosch-laptop-wasserwaage-software-anwendung-fuer-ihr-apple-ibook-powerbook-und-macbookmacbook-pro-6097.shtml" target="_blank">il portatile come livella</a>.

Grazie a <a href="http://pztake.blogspot.com/" target="_blank">Herr Zellner</a> per la segnalazione!