---
title: "Walk on the ’Book side"
date: 2002-03-21
draft: false
tags: ["ping"]
---

Non è un mistero che Mac sia usato da tanti Vip. Più uno

Giorni fa a Venezia c’era Lou Reed, un grande della musica.
Reed ha tenuto una conferenza al teatro Malibran e alla conferenza c’era anche un giornalista.
Mentre quest’ultimo scriveva sul suo iBook, si è accorto che Reed lo stava osservando. Poco dopo, sempre durante la conferenza, il cantante ha chiesto al giornalista (in inglese, chiaro): “Ti ci trovi bene con quello?” “Sì”, ha risposto il giornalista. “Si, eh? Funziona bene, vero?”, ha ripreso Reed. “Assolutamente”. “È un iBook, giusto?” “Sì, è un iBook”, ha risposto il giornalista. Reed ha sorriso, con l’aria di chi ha appena deciso di comprarsene uno, di iBook.
No, non ero io il giornalista. Era <link>Massimo Benvegnu</link>maxgnu@libero.it.
Lui è una persona seria; io, davanti a Lou Reed, non sarei più riuscito a scrivere una riga.

<link>Lucio Bragagnolo</link>lux@mac.com