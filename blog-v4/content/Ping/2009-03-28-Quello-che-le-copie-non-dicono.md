---
title: "Quello che le copie non dicono"
date: 2009-03-28
draft: false
tags: ["ping"]
---

Un sito italiano ha scopiazzato dai siti americani la notizia per cui <cite>Apple perde posizioni nella classifica dell'affidabilità</cite>, parlando senza mai averne saputo niente prima di una certa <a href="http://www.rescuecom.com/" target="_blank">Rescuecom</a>.

Rescuecom, che offre supporto tecnico a proprietari di computer guasti, rileva la percentuale delle chiamate che riceve e poi le confronta con le quote di mercato.

Praticamente le chiamate riguardanti Apple sono cresciute dall'1,1 percento del totale al 2,1 percento del totale e siccome la quota di mercato di Apple non è praticamente raddoppiata come le chiamate, l'indice di affidabilità sarebbe precipitato.

Se non fosse che Rescuecom fissa di propria iniziativa la quota di mercato Apple al 6,8 percento, mentre quella ufficiale è del 7,8 percento.

Secondariamente, Rescuecom non è un riparatore autorizzato Apple e dunque quello che fa o quello che dice rispetto ad Apple non ha rilevanza, per non parlare della variazione statistica sul totale delle chiamate, che si basa su valori numerici insignificanti.

Non bastasse, Rescuecom non ha esattamente una immagine cristallina e vari ex dipendenti e partecipanti nel franchising <a href="http://www.youtube.com/watch?v=jHsnEh8ySLI&amp;feature=player_embedded" target="_blank">hanno accusato l'azienda</a> di comportamenti poco onesti.

Ma queste cose quando mai potrà scriverle un sito di scopiazzatori?