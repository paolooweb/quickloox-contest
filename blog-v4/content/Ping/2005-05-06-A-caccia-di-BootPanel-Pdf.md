---
title: "Il Pdf sparito<p>"
date: 2005-05-06
draft: false
tags: ["ping"]
---

Affiorano anche i cambiamenti più remoti in Tiger<p>

Dentro Panther non era difficilissimo farsi una finestra di login personalizzata. Si sostituiva il file BootPanel.pdf con una versione propria.<p>

Con Tiger invece diventa più difficile, almeno per ora. Il file si trova inserito all&rsquo;interno dell&rsquo;eseguibile /usr/libexec/WaitingForLoginWindow. Comincia all&rsquo;offset 11000 ed è lungo 30.344 byte.<p>

Una cosa interessante è che WaitingForLoginWindow ha perfino una sua piccola pagina man.<p>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>