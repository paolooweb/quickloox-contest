---
title: "Qual è il suono di un codec solo?"
date: 2007-07-11
draft: false
tags: ["ping"]
---

Devo scusarmi perché il post riguardante Musimac è venuto un po' oscuro e mancano le informazioni essenziali.

Se fossi stato bravo, il post avrebbe avuto due piani di lettura. Il primo era più tecnico: si sente gente sostenere che, se codifichi un brano in Mp3 con Audacity o con iTunes oppure con qualcos'altro, si avvertano le differenze.

Non si intendeva certo sostenere che non si distingue un brano compresso da uno non compresso, ci mancherebbe. Ma la discussione su quel codec che comprime meglio di quell'altro, su un sistema di compressione di cui si sa assolutamente tutto, è ridicola.

Il secondo piano di lettura era più umano. La gente dice <em>suona meglio</em> e di solito intende <em>mi piace di più</em>. Il che è legittimo; diverso quando si assegna un valore assoluto a <em>suona meglio</em>, intendendo <em>ho qualcosa più degli altri</em>.

Persone con l'orecchio assoluto ne ho conosciute un paio. Tutti gli altri, me compreso, non possono andare oltre la soggettività del proprio orecchio. Se il parere personale (mi piace) diventa giudizio (questo è meglio di quello), si fa insulso. Nessuno sente allo stesso modo, come nessuno vede allo stesso modo (pochi lo sanno, ma molti graphic designer sono daltonici&#8230;). Dare giudizi assoluti sulla base di quello che sente il proprio orecchio lascia il tempo che trova.

Devo anche trovare stanze (e abitacoli di auto) dall'acustica perfetta, per poter dare un minimo di valore ai giudizi espressi dai rispettivi proprietari.

Rincaro la dose, infine, e chiudo, rispetto al discorso sull'alta fedeltà. La si dovrebbe chiamare, forse, alta qualità di riproduzione. La fedeltà è un'altra cosa.

Stai un teatro e ascolti l'orchestra, non amplificata. Poi vai ad ascoltare il Cd, registrato con un numero necessariamente limitato di microfoni, che trasformano le vibrazioni dell'aria in segnali elettrici, i quali vengono interpretati da convertitori che assegnano numeri ai segnali e li passano a mixer dove una mano umana altera il risultato. Il tutto tagliando frequenze che gli strumenti, semplicemente, non riescono a captare efficacemente. Il risultato viene suonato attraverso casse che tagliano ulteriormente le frequenze. E sarebbe fedeltà.

È la stessa fedeltà che si ottiene passando da una fotocamera attraverso uno schermo fino a una stampante, che produce una stampa a colori meravigliosa. Ma che i colori siano veramente fedeli all'originale, quando gli spazi di colore di ciascun dispositivo non sono neanche perfettamente sovrapposti, non esiste proprio.

Spero di essermi spiegato (meglio). Rinnovo le scuse. :-)