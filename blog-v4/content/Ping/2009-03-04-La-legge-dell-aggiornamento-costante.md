---
title: "La legge dell'aggiornamento costante"
date: 2009-03-04
draft: false
tags: ["ping"]
---

Chiacchiero dei nuovi Mac mini e mi sento dire che andava rifatto il <em>case</em>.

In effetti il valore aggiunto del nuovo Mac mini sta in altre novità: processore, disco rigido, processore grafico, porta FireWire, doppia uscita video, AirPort 802.11n e quant'altro, ma il <em>case</em> è rimasto lo stesso di prima anche se la macchina è tutt'altra cosa.

D'altro canto, Macworld.com sostiene che i <a href="http://www.macworld.com/article/139139/2009/03/unibody_17_inch_macbook_pro.html" target="_blank">guadagni prestazionali del nuovo MacBook Pro 17&#8221;</a> (modello base) sono minimi, massimo il due percento, rispetto alle capacità del modello 2008. E i MacBook Pro effettivamente hanno cambiato il <em>case</em> e poco più. Non è un cambiamento di poco conto e lo <em>chassis unibody</em> porta vantaggi importanti, ma è pure vero che dentro più o meno le cose vanno come prima.

Il che mi porta a pensare che esista una specie di costante dell'aggiornamento. Se investi nel <em>restyling</em> e nel <em>design</em> non è detto che si riesca a mettere mano alle prestazioni; se rinnovi l'interno di una macchina probabilmente non ti occuperai dell'esterno. <em>La quantità di innovazione che Apple introduce in un aggiornamento periodico della linea prodotti è una costante.</em>

Se è vero, quando compare una macchina veramente nuova, dentro e fuori, vuol dire che è un miracolo. E se ci pensiamo, nella storia ne sono apparse una manciata e mezzo, non di più.