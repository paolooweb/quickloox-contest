---
title: "La saga del Pdf"
date: 2007-06-18
draft: false
tags: ["ping"]
---

Capito il problema dell'incompatibilità con il plugin di Adobe. Adesso <a href="http://www.apple.com/safari" target="_blank">Safari 3 beta</a> è capace di visualizzare i Pdf per conto suo.