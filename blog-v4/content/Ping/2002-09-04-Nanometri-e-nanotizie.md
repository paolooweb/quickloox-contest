---
title: "Nanometri e nanotizie"
date: 2002-09-04
draft: false
tags: ["ping"]
---

Avviso ai naviganti: i siti di pettegolezzi Apple devono spararle grosse

I siti di notizie Apple si reggono sulla pubblicità e quindi devono fare pubblico. La via più semplice a questo obiettivo è spararle grosse e cercare lo scoop anche quando non c’è.

Recentemente Motorola ha annunciato un bel progresso industriale: la capacità di creare circuiti integrati su silicio “larghi” appena 90 nanometri (adesso lo standard industriale è 130). È un avanzamento che consentirà processori ancora più veloci e meno costosi.

Il sito ufficiale di Motorola ha pubblicato il comunicato della società, in cui si dice che entro fine 2002 saranno in grado di produrre wafer con questa tecnica. Un wafer è una grossa tavoletta di silicio che viene affettata in fase di produzione; a ogni fetta corrisponde il processore.

Neanche una parola su quando saranno effettivamente producibili processori a novanta nanometri su vasta scala; un conto è realizzare, un conto è produrre. Tutti possono imparare a fare i giocolieri roteando in aria tre oggetti, ma per viverci occorre una pratica lunga e faticosa.

Bene: un sito italiano ha commentato la notizia annunciando che Motorola inizierà a produrre processori a novanta nanometri entro fine 2002, tout court, e chiedendosi retoricamente se l’“agognato” G5 sarà prodotto a novanta nano.

Niente di esplicitamente scorretto e tutto molto plausibile, solo che si sono saltati con eleganza tanti se e tanti ma. A dimostrazione che in nanometri si può anche misurare l’onestà verso i lettori.

<link>Lucio Bragagnolo</link>lux@mac.com