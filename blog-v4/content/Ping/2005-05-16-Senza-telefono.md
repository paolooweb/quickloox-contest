---
title: "Quelli che tirano i fili<p>"
date: 2005-05-16
draft: false
tags: ["ping"]
---

Telecom Italia, l&rsquo;azienda giusta per le telecomunicazioni del secolo scorso<p>

Chi mi conosce sa che con Telecom non sono mai andato per il sottile e quindi sa che il problema personale è giusto un pretesto ideale. Al tempo stesso è noto che questa rubrica parla di Mac come regola, ma con sporadiche eccezioni. Eccone una.<p>

Neanche tanto personale, il problema, perché oggi è lunedì. Da venerdì, ora di pranzo, è tutto il mio quartiere che si trova senza telefono. E ovviamente senza Internet. Ho chiamato il 187, dove mi hanno trattato molto gentilmente, chiedendo anche un numero di cellulare per <cite>tenermi al corrente</cite>. Ovviamente non si è fatto sentire nessuno, e questa è forse la cosa peggiore.<p>

Secondo contratto, hanno tempo 48 ore per rimettere a posto le cose. Ovviamente giorni lavorativi. Ovviamente venerdì pomeriggio è come se non esistesse. Quindi, se va bene, forse domani.<p>

In concreto: decine di utenze che rimangono senza telefono per cinque giorni, cui Telecom chiederà, senza fare una piega, canone intero. Non parliamo di Internet, per carità, che si va sul tecnologico. Senza <em>telefono</em>. Roba da anteguerra.<p>

Che cosa avrebbe fatto Gandhi se avesse potuto comunicare come oggi? Dipende da dove abita e da chi ha tirato i fili. Potrebbero essere burocrati del secolo scorso.<p>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>