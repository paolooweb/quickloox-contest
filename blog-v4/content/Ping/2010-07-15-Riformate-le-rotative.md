---
title: "Riformate le rotative"
date: 2010-07-15
draft: false
tags: ["ping"]
---

Segnale chiaro di come cambierà prossimamente l'editoria: mentre l'edizione cartacea del prestigioso magazine americano Sports Illustrated usciva come da progetto centrata sull'asso dal basket LeBron James, l'edizione per iPad era dedicata a George Steinbrenner, proprietario dei New York Yankees del baseball, personaggio assoluto dello sport statunitense, morto poche ore dopo l'entrata in stampa di Sports Illustrated.

Lo staff editoriale ha dichiarato che non intende fare un'abitudine di questi cambiamenti, ma che valeva la pena di sperimentare questa novità e che potrebbe valerne la pena di farlo in futuro.

Ci sono più lezioni in questa notizia di tanti discorsi sull'editoria cartacea, digitale e bla bla bla che si sentono in giro e lasciano il tempo che trovano (per gli appassionati di sport americani, tra l'altro, Steinbrenner è un gigante che ha segnato un'epoca).

<i>En passant</i>, anche luglio terminerà senza che sia emerso un concorrente minimamente serio per iPad e, dal 3 aprile, ci si avvia verso il quadrimestre di attesa. Ma non era piena di difetti e mancanze, questa tavoletta?