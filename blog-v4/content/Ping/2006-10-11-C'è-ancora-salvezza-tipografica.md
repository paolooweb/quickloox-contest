---
title: "C'è ancora salvezza (tipografica)"
date: 2006-10-11
draft: false
tags: ["ping"]
---

L'analfabetismo informatico dilaga ma per fortuna esiste ancora <a href="http://alistapart.com/" target="_blank">A List Apart</a>. Per quanto io difficoltà a seguire anche un decimo delle loro <a href="http://alistapart.com/stories/emen/" target="_blank">indicazioni</a>, c'è un patrimonio di cultura che non sta andando perso nonostante la spazzatura diffusa per ogni dove. E da professionista studierò per seguire meglio le regole della tipografia, invece che snobbarle.