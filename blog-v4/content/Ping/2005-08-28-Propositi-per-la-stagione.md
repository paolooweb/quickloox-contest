---
title: "Propositi per la stagione<p>"
date: 2005-08-28
draft: false
tags: ["ping"]
---

Normalmente sono quelli che non si rispettano, ma bisogna provarci<p>

Primo settembre. Voglio arrivare al 31 luglio avendo fatto progressi con AppleScript.<p>

Per cominciare bene mi sono scaricato Smile, un ambiente di programmazione ampiamente superiore a quello dello Script Editor di serie in Mac OS X. Versione base gratis, versione per professionisti a prezzo stellare, ma c&rsquo;è nel caso interessasse a qualcuno.<p>

Migliorare in AppleScript non è né facile né difficile. Ci sono cose ostiche e cose banali. Molti programmi sono registrabili e le azioni compiute nei menu e nei dialoghi diventano automaticamente script pronti da rifinire. Basta cercare AppleScript su Google per ricevere decine di indirizzi validi dove trovare codice, consigli, contenuti.<p>

Certo, c&rsquo;è l&rsquo;inglese. Ci sono persone che con l&rsquo;inglese si sentono in difficoltà. Mi credano; è questione di buona volontà e testa dura.<p>

Gli autori di <a href="http://www.satimage.fr/software/en/index.html">Smile</a> sono francesi. Se ci sono riusciti loro&hellip;<p>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>