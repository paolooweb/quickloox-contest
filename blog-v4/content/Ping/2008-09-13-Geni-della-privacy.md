---
title: "Geni della privacy"
date: 2008-09-13
draft: false
tags: ["ping"]
---

Premesso che la funzione Genius di iTunes 8 è completamente disattivabile e uno potrebbe non vederla mai in funzione per tutta la sua vita, una persona che ha un account su iTunes Store ha depositato nome, cognome, numero di carta di credito.

Quando compra su iTunes, la persona in questione non solo ha depositato quanto sopra, ma ovviamente Apple sa perfettamente che cosa ha comprato, visto che glielo ha venduto. E mica solo Apple: se ha comprato musica Warner, lo sa anche Warner, che l'ha passata ad Apple perché venisse venduta. E via compravendendo.

A partire da <a href="http://www.last.fm" target="_blank">Last.fm</a>, il web abbonda di <em>reti sociali</em> dove lo scopo è esattamente far sapere a tutti tutto sui propri gusti musicali.

Se uno si sente minacciato nella propria <em>privacy</em> da Genius, faccia il favore di disattivarlo e disattivarsi, che la <em>privacy</em> è un concetto sacro, ma tanto per cominciare nel mondo digitale è illusorio e, secondo, se lo stesso sistema non fosse in iTunes ma nel primo <em>shareware</em> che passa non ci farebbe caso nessuno.