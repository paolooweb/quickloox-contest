---
title: "Innovazione e sporcizia"
date: 2007-10-17
draft: false
tags: ["ping"]
---

Si sa (lo so grazie a <strong>Nicola</strong>) che, pochi giorni dopo l'uscita di iPhone sul mercato, Microsoft stava cercando di brevettarne l'interfaccia.

Lo ha mostrato Wired e, davvero, <a href="http://blog.wired.com/gadgets/2007/10/microsoft-paten.html" target="_blank">guardando le immagini</a> c'è proprio poco da dire.

Quello che forse non hanno letto proprio tutti è il commento di Wired. Lo rendo in italiano:

<cite>Siamo al punto in cui depositare un brevetto non è né buono né cattivo. È solo una manovra legale tattica in un gioco insensato d'impresa che non ha niente a che vedere con l'innovazione. O questo, o è una burla.</cite>

Qui si torna a una cosa che ho ripetuto tante volte. Il mio computer non è Microsoft free perché i prodotti Microsoft sono cattivi. Sarebbe pulito anche se i prodotti Microsoft fossero i migliori del mondo.

Perché Microsoft è sporca.