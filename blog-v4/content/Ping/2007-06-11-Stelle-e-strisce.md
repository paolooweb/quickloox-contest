---
title: "Stelle e strisce"
date: 2007-06-11
draft: false
tags: ["ping"]
---

Mi rendo conto che non si riesca a distogliere lo sguardo da quella grande X stellare lanciata alla Conquista del Time (Machine) e degli Spaces.

Eppure bisognerebbe riservare un pizzico di incanto all'interfaccia, nuova, del <a href="http://www.apple.com/" target="_blank">sito Apple</a>. Ha meno elementi di prima ed è più funzionale di prima.

Quella striscia è una lezione di design che non c'è neanche bisogno di attendere iPhone.