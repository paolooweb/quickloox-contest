---
title: "Cos&#236; disse Manuel"
date: 2008-11-16
draft: false
tags: ["ping"]
---

<cite>Ti vorrei segnalare questi due ottimi siti, per <em>plugin</em> di QuickLook:</cite>

<a href="http://www.quicklookplugins.com/" target="_blank">http://www.quicklookplugins.com/</a> e 
<a href="http://www.qlplugins.com/browse/" target="_blank">http://www.qlplugins.com/browse/</a>.

<cite>Personalmente uso <em>plugin</em> per vedere il contenuto di cartelle, file .zip, colorare i file sorgente, avere anteprime di Flash (con <a href="http://perian.org/" target="_blank">Perian</a>) e visionare il contenuto dei file .pkg.</cite>

<cite>QuickLook è una bella invenzione, ed il fatto che sia aperta a <em>plugin</em> lo è ancora di più :-)</cite>

Cos&#236; disse <a href="http://web.mac.com/manuelmagic" target="_blank">Manuel</a> e io sono del tutto d'accordo. :-)
