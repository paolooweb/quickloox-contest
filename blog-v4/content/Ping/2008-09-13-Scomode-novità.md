---
title: "Scomode novità"
date: 2008-09-13
draft: false
tags: ["ping"]
---

Provo a introdurre un piccolo elemento di provocazione nel dibattito sulle novità o non novità di Apple e se si stesse meglio quando si stava peggio bla bla bla.

X11 è una tecnologia scomoda da usare, difficile da capire, rozza rispetto perfino a Mac OS X, che a sua volta ha qualche asprezza in più rispetto a Mac OS classico.

Eppure ci sono software che grazie a X11 sono arrivati su Mac. Giochi fantastici come <a href="http://freeciv.wikia.com/index.php/Freeciv" target="_blank">FreeCiv</a> (uno dei miei maggiori rimpianti per rapporto tra tempo giocato e tempo che vorrei giocare) o gemme di valore assoluto come OpenOffice.org, che solo tra qualche giorno diventerà nativo Mac OS X.

Fosse solo questo. X11 ha avvicinato clamorosamente Apple alla comunità scientifica e a quella universitaria, nonché al movimento <em>open source</em>. È probabile che, nella sua scomodità e difficoltà d'uso, X11 abbia fatto molto per espandere le possibilità di Mac, molto più del mero aumento del numero di programmi.

Prima di Mac OS X, naturalmente, di tutto questo non si iniziava neanche a parlare. Il Mac in università o in laboratorio era una anomalia genetica. Oggi è una anomalia quando qualcuno <a href="http://www.macworld.it/showPage.php?template=notizie&amp;id=15531" target="_blank">lascia fuori il Mac</a>, come gli svaporati del Cern.

Il punto non è che uno usi X11 o meno. Di FreeCiv uno se ne può fregare e pure di OpenOffice.org, grazie agli eroi di NeoOffice. Ci sono centinaia di programmi di grande valore su X11 che, se non siamo accademici o ricercatori, mai useremo.

Il punto è che ho la sensazione di una certa importanza di X11 nei successi attuali di Apple. Anche se nessuno ne parla, pochi lo usano, qualcuno manco lo conosce. Tuttora ho difficoltà a configurare correttamente X11 per lanciare qualcosa e sono felicissimo quando il programma riesce a fare da solo.

Di converso, sospetto che si faccia un gran parlare del Genius di oggi o dello Spotlight di ieri e che il tutto sia assai meno importante di quanto se ne parla.

Si stava meglio o peggio senza X11?