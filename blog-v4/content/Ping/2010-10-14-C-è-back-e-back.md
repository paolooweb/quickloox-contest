---
title: "C'è back e back"
date: 2010-10-14
draft: false
tags: ["ping"]
---

Godiamoci la fase dell'entusiasmo fanciullesco per l'evento <a href="http://www.macworld.it/news/2010/10/14/evento-apple-il-20-ottobre-e-tempo-di-mac/" target="_blank">Back to the Mac</a> distante meno di una settimana e ricordiamo che siamo circondati da lamentosi per i quali Apple vuole uccidere Mac per disinteresse. In questi giorni i lamentosi non sanno bene come muoversi, palesemente disorientati dalla mossa: l'azienda che vuole uccidere i Mac gli dedica un evento.

Ma si riavranno, Apple non presenterà il teletrasporto per Mac OS X e dal giorno dopo riprenderanno a berciare, come gli ufologi e i tifosi del luned&#236; mattina.

Interessa forse altro, per esempio che Apple si disinteresserebbe dei Mac dal 2007, il tempo di iPhone.

Dal 2007, i Mac che si vendono oggi sono circa il doppio e nossignori, il mercato non è cresciuto con altrettanta forza. Questo sarebbe il prodotto che Apple vuole eliminare. Presumibilmente, siccome più lo vuole eliminare più se ne vendono, al momento di annunciare l'abbandono del Mac se ne venderanno infiniti.

In particolare, per quanto avremo i risultati finanziari ufficiali luned&#236;, Idc e Gartner sostengono (valgono come Stanlio e Ollio, ma questo passa il convento ancora per tre giorni) che negli Stati Uniti Apple sia diventato il terzo costruttore per vendite, dopo Hewlett-Packard e Dell, con una crescita del 24 percento contro il 3,8 percento del mercato.

Si badi che per gli altri contano come vendite anche i <i>netbook</i> da 299 dollari, mentre per Apple non contano gli iPad da 499.

Se si contassero gli iPad, Apple sarebbe il primo costruttore di computer negli Stati Uniti, mai accaduto nella storia.

E se contassimo anche gli iPhone, sistemi Unix con un processore che si mangia a colazione quelli dei <i>netbook</i> e a momenti sfiorano la stessa risoluzione di schermo?

Vorrei fare un bel giro di pedate nel <i>back</i> a quanti non perdono occasione di straparlare.

Venendo invece a Back to the Mac, facciamo comunella nella nostra consueta chat? Io ci sto.