---
title: "Che musica questa suite<p>"
date: 2005-01-15
draft: false
tags: ["ping"]
---

L&rsquo;open source sta diventando sempre più facile, anche su Mac<p>

Avvicinarsi all&rsquo;open source senza timori? Inizia a scaricare il <a href="http://www.freesmug.org/fscd">FreeSmug Suite Cd</a>. Contiene alcuni tra i migliori programmi open source disponibili, in versione pronta per installarsi su Mac come programmi normalissimi, e partire con un doppio clic senza mettere in imbarazzo nessuno.<p>

L&rsquo;open source non sostituirà mai interamente programmi professionali a pagamento. Da professionista del testo, pago regolarmente qualunque edizione di BBEdit che esca. Però, per esempio, i miei requisiti di elaborazione grafica rendono più che sufficiente Gimp e quelli authoring Html sono ben sotto quello che può offrire Nvu.<p>

Gimp e Nvu li trovi nella FreeSmug Suite. Insieme a un sacco di altra roba, tutta buona.<p>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>