---
title: "Brevetti lunghetti"
date: 2006-03-03
draft: false
tags: ["ping"]
---

Non avrei mai immaginato che dal 1976 a oggi Apple avesse registrato <a href="http://patft.uspto.gov/netacgi/nph-Parser?Sect1=PTO2&amp;Sect2=HITOFF&amp;u=/netahtml/search-adv.htm&amp;r=0&amp;p=1&amp;f=S&amp;l=50&amp;Query=an/apple&amp;d=ptxt" target="_blank">1.979 brevetti</a>.