---
title: "Sassi bianchi e sassi neri"
date: 2006-10-04
draft: false
tags: ["ping"]
---

Nell'antica Grecia, se ricordo bene, gli ostracismi si decidevano cos&#236;.

Vedo che c'è richiesta e dunque parliamo di Macworld-rivista. Questo spazio è libero e al massimo c'è autocensura mia, su me. Storicamente, ho cancellato un messaggio, che era idiota, e un altro messaggio, per errore. I commenti approvati sono duemilanovecentodue, tutti tranne quei due: sii libero e d&#236; quello che vuoi. Unica regola: niente insulti, se non sono diretti a me.

Avvertenza: non decido io che cosa viene pubblicato su Macworld. Forza con le critiche e con gli elogi, dunque, ma non attribuirmi decisioni che non ho preso. Rischieresti di sentirti rispondere che non le ho prese. :)