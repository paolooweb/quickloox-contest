---
title: "AppleScript al fiore d'arancio"
date: 2008-09-28
draft: false
tags: ["ping"]
---

Pierfausto ha detto che deve questo AppleScript a un suo amico, <a href="http://work.to.it/" target="_blank">Paolo Portaluri</a>, ma c'è chi è artefice del suo destino. E lui, a dimostrarlo, si è felicemente sposato.

Paolo gli ha creato un conto alla rovescia in AppleScript per ricordarglielo.

Il conto è qui. Adesso serve un altro AppleScript, che conti in avanti invece che all'indietro.

Magari ce lo scriverà Pierfausto quando tornerà dalla luna di miele. Per intanto, augurissimi. :-)

<code>set cd to current date
set dd to date "Mercoled&#236;, 24 settembre 2008 18:00:00"

set diff to dd - cd
set giorni to round (diff / days) rounding down
set resto to diff - giorni * days
set ore to round (resto / hours) rounding down
set resto to resto - ore * hours
set minuti to round (resto / minutes) rounding down
set secondi to resto - minuti * minutes

if giorni is 0 and ore is less than 3 then
	display dialog "Che diavolo ci fai ancora qui!" &#38; return &#38; return &#38; "Vedi di darti una mossa. Mancano " &#38; ore &#38; " ore, " &#38; minuti &#38; " minuti e " &#38; secondi &#38; " secondi!"
else
	display dialog "Mancano " &#38; giorni &#38; " giorni, " &#38; ore &#38; " ore, " &#38; minuti &#38; " minuti e " &#38; secondi &#38; " secondi al tuo matrimonio."
end if</code>
