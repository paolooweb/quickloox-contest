---
title: "Come cambia lo studio"
date: 2010-02-10
draft: false
tags: ["ping"]
---

Mi scrive (grazie!) <b>Paolo V</b>:

<cite>Utilizzare il computer per studiare dai Pdf mi è sempre sembrato scomodo, preferendo spesse volte la stampa su carta del documento.</cite>

<cite>Anteprima, ad un uso intenso, mi è parso goffo e poco personalizzabile nella visualizzazione a tutto schermo.</cite>

<cite>Poi ho scoperto <a href="http://skim-app.sourceforge.net/index.html" target="_blank">Skim</a>&#8230;</cite>

<cite>È uno di quei programmi che avrei voluto vedere recensiti in Ping! o da qualche parte su MacWorld (se ne hai già parlato, ti chiedo di scusarmi).</cite>

<cite>Fa cose davvero utili ed inaspettate per un lettore di Pdf gratuito: permette</cite> link <cite>da una pagina del Pdf ad un'altra; realizza</cite> slideshow <cite>delle pagine come se fossero diapositive, controllabili da Apple Remote; per non parlare delle opzioni di visualizzazione (soprattutto della ottima modalità a schermo pieno), e tanto, tanto altro ancora&#8230;</cite>

<cite>Ovviamente è un suggerimento e non vorrei risultare scortese; penso soltanto all'enorme favore che potresti fare ai tanti studenti come me che ti seguono, visto che lo studio potrebbe in futuro diventare sempre più digitale.</cite>

Di Skim si è parlato più volte su Ping!; entra ed esce dalla mia vita a intervalli (adesso è fuori e guarda un po' mi sta venendo voglia di riprovarlo). Più di questo, richiamo l'attenzione al discorso sullo studio digitale. iTunes U c'è da tempo, ma si guardi la pagina di <a href="http://www.apple.com/education/mobile-learning/" target="_blank">Mobile Learning</a> del sito Apple. Manca hardware di annuncio recente&#8230;