---
title: "Il vecchio e il mare<p>"
date: 2005-04-09
draft: false
tags: ["ping"]
---

Che bello guardare al futuro e intanto rivivere il passato<p>

L&rsquo;amico Paolo mi segnala un bellissimo emulatore di <a href="http://www.mcsebi.com/pacman2.php">Pacman 2</a>, un gioco più vecchio di molti lettori di Macworld, perfettamente eseguibile su Mac OS X e persino Mac OS Classic, che ancora tanti usano.<p>

Lo prendo come spunto per segnalare che il Mac è una macchina eccezionale per fare emulazione e anche per fare <a href="http://www.emulation.net">emulazione</a> di giochi e computer di una volta.<p>

Un ennesimo argomento a sfavore dei lamentosi che si lamentano della mancanza di giochi per Mac (ma lo sanno che Mame ne supporta quattromila e oltre?). E motivo di soddisfazione, perché siamo agli sgoccioli con Tiger. Il prossimo sistema operativo di Apple sarà eccezionale e contemporaneamente conterrà tutte le risorse per guardare, e toccare il mare di software che lo ha preceduto. Vecchio, certo, alla stessa maniera di un nonno vitale e allegro come un bimbo.<p>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>