---
title: "Il manuale è la dentro"
date: 2003-10-07
draft: false
tags: ["ping"]
---

Le istruzioni per usare un Mac non sono un X-file; è che le cose sono cambiate

Il Macintosh del 1984 era privo di manuale, ma faceva pochissime cose rispetto a un Macintosh del 2003. Quest’ultimo può essere ancora usato senza un manuale, ma possiede internamente tali e tante tecnologie che di manuali ce ne vorrebbero una cinquantina.

Impossibile, per ragioni economiche e per ragioni pratiche (diecimila pagine di manuali? In quindici o venti lingue? Continuamente aggiornati? ma chi li fa?), pretendere di trovare una biblioteca tecnica dentro ogni scatolone di Macintosh.

Almeno su carta, perché la biblioteca c’è: tra Aiuto Apple, documentazione dei Developer Tools, sito Apple, pagine man e tutto quanto è nascosto nel sistema c’è di che laurearsi in scienze dell’informazione honoris causa.

Il punto è che le cose sono cambiate. Oggi l’informazione è infinitamente più accessibile di una volta. Ma è distribuita e arriva da ogni direzione. Sta a noi sapere imparare da più fonti invece che pretenderne una sola.

<link>Lucio Bragagnolo</link>lux@mac.com