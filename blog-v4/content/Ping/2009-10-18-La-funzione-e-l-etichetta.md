---
title: "La funzione e l'etichetta"
date: 2009-10-18
draft: false
tags: ["ping"]
---

Avevo già accennato all'artista inglese David Hockney, che da qualche tempo realizza (e spedisce ad amici via posta) <a href="http://www.macworld.it/blogs/ping/?p=2291" target="_self">quadri con iPhone</a> e l'applicazione <a href="http://itunes.apple.com/WebObjects/MZStore.woa/wa/viewSoftware?id=288230264&amp;mt=8" target="_blank">Brushes</a>.

Domenica scorsa il ragazzo (settantaduenne) è finito sul Sole 24 Ore, che gli ha dedicato quasi una pagina <a href="http://www.nybooks.com/articles/23176" target="_blank">tradotta dalla New York Review of Books</a>.

Affascinante il resoconto della sua fascinazione per l'alba:

<cite>Dopotutto, quale altra luce più chiara e luminosa ci possiamo permettere? Specialmente qui</cite> [<a href="http://maps.google.com/maps?client=safari&amp;q=bridlington&amp;oe=UTF-8&amp;ie=UTF8&amp;ei=NyTbSvriEIvQmgPujeHmDA&amp;ved=0CAsQ8gEwAA&amp;hq=&amp;hnear=Bridlington,+United+Kingdom&amp;z=12" target="_blank">Bridlington, Regno Unito</a>] <cite>dove la luce arriva sorgendo dal mare, proprio il contrario dei miei rifugi californiani. Ma ai vecchi tempi era impossibile dipingerla, perché, naturalmente, è troppo scuro per vedere il dipinto; e se si accende una luce per vederlo, si perde il raccogliersi delle sottili tonalità del  Sole che arriva. Con un iPhone, invece, non devo neanche alzarmi dal letto; lo prendo in mano, lo accendo e inizio a mescolare e ad accostare i colori, che si stratificano con l'evolvere della situazione.</cite>

Il settantaduenne ha ben chiaro che la funzione di un apparecchio non corrisponde a un'etichetta, o a un elenco di componenti:

<cite>La gente del villaggio arriva e mi punzecchia:</cite> abbiamo sentito che ha cominciato a disegnare sul cellulare. <cite>E rispondo</cite> beh, non proprio, è solo che a volte telefono con il mio blocco da disegno.