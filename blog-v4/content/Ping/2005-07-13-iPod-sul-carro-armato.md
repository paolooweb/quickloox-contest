---
title: "La pace passa da iPod<p>"
date: 2005-07-13
draft: false
tags: ["ping"]
---

Dai cannoni alle canzoni<p>

Non posso dire né come né dove, ma ci sono carristi in forza all&rsquo;Esercito italiano che hanno collegato un iPod al sistema cifrato di comunicazione interno e guidano il loro mezzo a suon di musica. Il bello è che nessuno li può intercettare. Sono gli unici al riparo delle grinfie della Siae!<p>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>