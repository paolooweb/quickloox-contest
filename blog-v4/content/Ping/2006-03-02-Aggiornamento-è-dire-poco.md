---
title: "Aggiornamento &egrave; dire poco"
date: 2006-03-02
draft: false
tags: ["ping"]
---

Per capire la differenza tra fatti e chiacchiere, basta leggere la <a href="http://docs.info.apple.com/article.html?artnum=303382" target="_blank">nota tecnica</a> con cui Apple commenta l&rsquo;aggiornamento di sicurezza appena reso disponibile per Mac OS X 10.4 e 10.3.9.

Da una parte ci si riempie la bocca su uno o due fatti che fanno pi&ugrave; rumore di altri; dall&rsquo;altra parte si lavora a sistemare molto pi&ugrave; di quanto dica la sedicente informazione.