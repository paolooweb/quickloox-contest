---
title: "Le duecentocinquantasei ore"
date: 2003-12-21
draft: false
tags: ["ping"]
---

Panther costa. E il tuo tempo, invece?

I colleghi (e, in Italia, non concorrenti!) di Mac Addict hanno stimato che Panther, grazie alle sue novità rispetto ai sistemi precedenti, faccia risparmiare 256 ore di utilizzo dell’interfaccia ogni anno, tempo che può quindi andare in maggiore produttività.

Di conseguenza, sostengono, perfino se uno guadagnasse solamente un dollaro l’ora avrebbe comunque convenienza ad acquistare Panther.

Aggiungo: anche se le loro stime fossero sbagliate del 50 percento i conti tornerebbero. Gli unici che non capiranno questo discorso sono quelli che vedono un costo solo se vedono uno scontrino. Ma li abbiamo già persi da tempo.

<link>Lucio Bragagnolo</link>lux@mac.com