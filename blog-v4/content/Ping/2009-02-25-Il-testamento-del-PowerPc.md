---
title: "Il testamento del PowerPc"
date: 2009-02-25
draft: false
tags: ["ping"]
---

È possibile che stia già usando il nuovo MacBook Pro. O forse no. La seconda cosa che avrò fatto è comunque scaricare OpenOffice 3.1 per Intel e metterlo al posto della versione provvisoria per PowerPc. La prima è Assistente Migrazione.

Al momento, la migliore versione provvisoria per PowerPc è la <a href="http://ooopackages.good-day.net/pub/OpenOffice.org/MacOSX/3.0.1rc2/" target="_blank">3.0.1rc2</a>. Funziona quasi tutto, ma non tutto; per esempio, il sistema di gestione delle correzioni fa esplodere il programma quando cerchi di salvare in formato .doc. Se invece si applicano direttamente le correzioni, senza registrarle, non ci sono problemi.