---
title: "Non guardarmi, non ti sento"
date: 2009-07-15
draft: false
tags: ["ping"]
---

Cambio di sezione nel percorso delle novità di Snow Leopard elencate da Apple ma non tradotte. La novità tradotta di oggi inaugura la sezione iChat.

<b>Connettività iChat più affidabile</b>

Snow Leopard contiene tecnologia che supera alcune delle incompatibilità di router più comuni che si incontrano cercando di fare videochat. Se iChat non può stabilire una connessione diretta con un contatto, usa il <i>relay server</i> di Aim, che agisce da intermediario noto tra i due computer.

Difficile da verificare, ottimo sulla carta. Speriamo anche a video.