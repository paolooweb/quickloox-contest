---
title: "Fumo da viaggio"
date: 2010-08-06
draft: false
tags: ["ping"]
---

Sul blog di FutureTap, azienda che sviluppa software per App Store, è comparsa una <a href="http://www.futuretap.com/blog/the-patent-case-we-havent-called/" target="_blank">nota preoccupata</a>: apparentemente, Apple starebbe brevettando idee di applicazione mobile rubandole agli sviluppatori.

L'immagine pubblicata, in effetti, pare quella di un furto fatto e finito, quasi pixel per pixel.

È già partito un concerto di reazioni, accuse, allusioni, il solito repertorio del <i>gossip</i> <i>made in Internet</i>.

Poi uno va a vedere, cosa che i fabbricanti di gossip e i commentatori dal grilletto facile non sanno fare.

Il <a href="http://appft.uspto.gov/netacgi/nph-Parser?Sect1=PTO1&amp;Sect2=HITOFF&amp;d=PG01&amp;p=1&amp;u=%2Fnetahtml%2FPTO%2Fsrchnum.html&amp;r=1&amp;f=G&amp;l=50&amp;s1=%2220100190510%22.PGNR.&amp;OS=DN/20100190510&amp;RS=DN/20100190510" target="_blank">brevetto di Apple</a> non riguarda una applicazione identica a quella di FutureTap. Invece riguarda la possibilità di mettere informazioni miste a disposizione di viaggiatori e turisti in modo automatico, mostrando in questo contesto <i>un esempio di applicazione</i> che potrebbe trarre vantaggio delle informazioni contenute nel sistema.

Il brevetto riguarda un ipotetico sistema informativo per viaggiatori su cui potrebbe innestarsi comodamente una <i>app</i> come Where To?, quella <i>rubata</i>.

Si può discutere sull'eleganza o sulla convenienza di mostrare, come esempio, una <i>app</i> esistente. Ma è un esempio e non un furto. Un <i>fanboy</i> Apple potrebbe arrivare a dire che quasi quasi è tanta ottima pubblicità per FutureTap.

Apple è stata accusata di rubare idee in innumerevoli occasioni e non si può escludere che sia anche successo veramente. Questo caso, però, è puro fumo negli occhi. Vediamo se qualcuno si degna di fare un po' d'aria.