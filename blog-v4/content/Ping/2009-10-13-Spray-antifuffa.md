---
title: "Spray antifuffa"
date: 2009-10-13
draft: false
tags: ["ping"]
---

Inspiegabilmente, un analista &#8211; Brian Marshall di Broadpoint AmTech &#8211; si è messo a lavorare seriamente, lavorando sui fatti.

I fatti dicono che <a href="http://brainstormtech.blogs.fortune.cnn.com/2009/10/13/will-windows-7-boost-apple-sales/" target="_blank">non c'è correlazione negativa</a> tra le vendite di hardware Apple e l'uscita dei sistemi operativi Microsoft.

Se proprio se ne volesse cercare una, per assurdo, il software Microsoft potrebbe anzi costituire un accelerante ritardato per le vendite di Mac.

Il <b>link</b> si accompagna a grafici che basta guardare per rendersi conto.

La prossima volta che si legge come Mac sia stato aiutato da Vista, o che Windows 7 potrebbe incidere sulle vendite di Mac, una spruzzatina di fatti ed ecco che la fuffa si volatilizza.

Interessante il fatto che, sempre stando a Marshall, Windows 8 (che esiste sulla carta e poco più) potrebbe rappresentare un fattore negativo per Mac. Perché tra qualche anno, quando uscirà, Mac potrebbe avere raggiunto un livello di diffusione più alto di quelli attuali.