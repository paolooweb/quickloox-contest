---
title: "Aperto e stabile"
date: 2007-01-23
draft: false
tags: ["ping"]
---

Non è un teatro, ma <a href="http://www.openoffice.org" target="_blank">OpenOffice 2.1</a>. Venendo dalla versione 2.0.4 sembra un aggiornamento da poco, invece si guadagna molto in stabilità e amichevolezza con l'ambiente Mac, pur da dentro X11. Aggiornamento perfettamente funzionante e ampiamente raccomandato.

La versione Aqua è ancora indietro ma piano piano cresce. Nel frattempo, chi usa spesso OpenOffice può rivolgersi a <a href="http://www.neooffice.org/" target="_blank">NeoOffice</a>. Io, che lo uso solo per emergenza e compatibilità, vado bene anche con X11. :)