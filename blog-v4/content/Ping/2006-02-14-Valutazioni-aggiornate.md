---
title: "Valutazioni aggiornate"
date: 2006-02-14
draft: false
tags: ["ping"]
---

Sono l&rsquo;ultimo ad averlo scoperto, ma la <strong>Calcolatrice</strong> di Tiger, che all&rsquo;inizio aveva smesso di farlo, ora aggiorna regolarmente i tassi di cambio delle valute.

Prezioso, direbbe Bisio nella pubblicit&agrave;.