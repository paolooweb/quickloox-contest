---
title: "Vorrei avere lo stipendio di quest&rsquo;uomo"
date: 2006-03-14
draft: false
tags: ["ping"]
---

Ogni tanto vengo accusato di prendere soldi da Apple o di scrivere quello che scrivo perch&eacute; sono socio di Mac@Work oppure perch&eacute; scrivo su Macworld.

Sono sempre contento, perch&eacute; &egrave; testimonianza della mancanza di argomenti concreti sull&rsquo;altro lato.

Tanto per essere chiari, da Apple prendo molto meno di quello che prende <a href="http://www.marketwatch.com/News/Story/Story.aspx?guid=%7B33707AC6%2D238D%2D487E%2DAA41%2D9475F06BDE0D%7D&amp;dist=newsfinder&amp;siteid=google&amp;keyword=" target="_blank">Steve Jobs</a>.