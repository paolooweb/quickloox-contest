---
title: "Un paio di sassolini"
date: 2008-01-26
draft: false
tags: ["ping"]
---

Che avevo nella scarpa.

Dopo tutta la pubblicità che ha accompagnato il passaggio di Alice da due megabit a sette megabit, in cui si dice che con sette megabit puoi fare tutto compresa la televisione, dove si sono nascosti quelli che sostenevano la necessità dei venti megabit?

Secondo: infuria ora la pubblicità della Internet Key di Vodafone. Pochi fanno attenzione al fatto che viene venduta con un contratto di due anni. Qualche critico della formula di abbonamento di iPhone in America (due anni), di quelli che <em>in Italia non può andare bene</em>, vorrebbe commentare?