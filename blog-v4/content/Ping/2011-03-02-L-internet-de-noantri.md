---
title: "L'Internet de noantri"
date: 2011-03-02
draft: false
tags: ["ping"]
---

Gustoso, amaramente gustoso, lo sfogo di <b>Massimo</b>, che riporto qui sotto senza commenti.

<cite>Mi sta bene, ma poco, che l'Inps mi scriva nel XXI secolo</cite> sito ottimizzato per Internet Explorer<cite>, segno del cialtronismo di stato che ormai ci ha purtroppo assuefatti.</cite>

<cite>Invece quando vai sul sito del</cite> Corriere<cite>, che si fa la bocca grande con il suo</cite> Corriere per iPad<cite>, applicazione tutta da sistemare, che va in</cite> crash <cite>che è un piacere e con un sito che ci mette tre giorni &#8211; dopo la cazziata &#8211; ad aggiornarti l'abbonamento che hai rinnovato&#8230;</cite> very apple-like approach&#8230; 

Ad abundantiam <cite>trovi anche il sito <a href="http://trovocasa.corriere.it/" target="_blank">Trovocasa.it</a> &#8211; sempre del Corrierone &#8211; e ti imbatti in un sito</cite> ottimizzato <cite>(ma per quale</cite> browser<cite>?), pieno di</cite> link <cite>che non funzionano  e che, anche lui, non si accorge di avere ricevuto un pagamento&#8230;</cite>

<cite>Poi scopri che il Corrierone oltretutto paga una società nel Trevigiano per fare l'assistenza tecnica del sito Trovocasa&#8230; meno male che a Treviso hanno un numero di telefono, se no come fai a segnalare il problema?</cite>

<cite>Scusa lo sfogo, ma queste incazzature sono i problemi del popolo, altro che alti temi come Flash contro Html5; qui siamo alle scarpe di cartone&#8230;</cite>

<cite>P.S.: uso un Mac Intel con Snow Leopard e Firefox; pensavo di avere una configurazione per poter navigare senza grossi problemi, invece ecco&#8230; quasi quasi rischio la multa dall'Inps&#8230;</cite>