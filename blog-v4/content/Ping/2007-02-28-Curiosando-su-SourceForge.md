---
title: "Curiosando su SourceForge"
date: 2007-02-28
draft: false
tags: ["ping"]
---

<a href="http://todoom.sourceforge.net/" target="_blank">Warp Rogue</a>. Grafica quasi zero, fantasia a mille. Se ti appassioni, rischi di passarci sopra qualche mese.

Per chi apprezza il genere, chiaro. Requisiti di sistema insulsi, gira dovunque. Ma si trova qualcosa di paragonabile per qualunque genere. Open source, nativo Mac, installazione indolore.

Chi si lamenta dei pochi programmi dovrebbe parlare meno e cliccare di più.