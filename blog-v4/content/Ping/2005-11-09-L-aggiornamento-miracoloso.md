---
title: "L&rsquo;aggiornamento miracoloso<p>"
date: 2005-11-09
draft: false
tags: ["ping"]
---

Funzioni avanzate è una cosa, Classic è un&rsquo;altra. Eppure&hellip;<p>

Ho avuto da <strong>Sergik</strong> il permesso di riprodurre una sua mail:<p>

<cite>Sono assolutamente stupefatto! Premetto, non sono tecnico, non so di linguaggio macchina&hellip; ci giro intorno molto alla larga, non perché non mi interessi, ma a quasi 47 anni si impara a gestire al meglio il proprio tempo. Faccio il grafico e ieri sera, dal mio PowerBook scopro l'aggiornamento di sistema 10.4.3. Guardingo, cavalco qualche forum e mi deprimo. Un primo coro di lamentele varie&hellip; poi leggo il &ldquo;consigliato a tutti&rdquo; della Apple e mi dico: &ldquo;al diavolo, lo fo&rdquo;. Anche perché avevo notato un certo appesantimento nel passare da Panther a Tiger ed ero desideroso da tempo di un riscatto. Penso, tra l'altro, che parecchia gente ci pasticci molto col Mac, e se le tiri addosso.</cite><p>

<cite>Riavvio timoroso e&hellip; va tutto! Ma molto meglio! :-) Ripeto, tutto! Poi la magia. Premetto che per questioni lavorative sono obbligato a utilizzare ancora XPress 4.1.1 (in ambiente Classic, ovviamente).</cite><p>

<cite>Ebbene, apro una copertina e vedo tutto più sveglio; possibile? Suggestione? Ma poi sgrano gli occhi! Quel marchio fatto in Photoshop ma con l'ombrina per cui con il fondo verde, con le stesse percentuali di colore del fondo verde in XPress, salvato in Eps e importato&hellip; solitamente si vedono due colori molto distanti tra loro e finché non si fa un bel Pdf, non si ha la certezza del colore uniforme. E invece&hellip; ora il colore è perfettamente uniforme! È possibile che sia un beneficio dell'aggiornamento? O devo pensare che qualcuno mi abbia drogato di nascosto? :D</cite><p>

<cite>Adoooooooro questo sistema operativo, adoro il Mac, adoro il PowerBook!</cite><p>

È possibile, sì. Anch&rsquo;io ho aggiornato, del resto, e funziona tutto.<p>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>