---
title: "Incontri quasi ravvicinati"
date: 2009-02-25
draft: false
tags: ["ping"]
---

I negoziati per arrivare a usare il MacBook Pro 17&#8221; sono cominciati.

Ieri il corriere è arrivato a due traverse da casa mia e poi ha deciso che era abbastanza. Indirizzo inesistente, ha deciso.

Oggi, mi dicono, <em>forse</em> arriva. L'indirizzo, naturalmente, è ineccepibile.

Credo sia una sorta di danza di corteggiamento.