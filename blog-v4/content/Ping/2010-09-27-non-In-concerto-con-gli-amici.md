---
title: "(Non) in concerto con gli amici"
date: 2010-09-27
draft: false
tags: ["ping"]
---

Dopo avere segnalato la pubblicazione del <a href="http://www.macworld.it/ping/nomi/2010/09/19/linsolita-sviolinata/" target="_blank">lavoro di Christian</a> su iTunes Store, non posso esimermi dal notificare anche <a href="http://itunes.apple.com/it/album/tolkieniana-musical-journey/id394640597" target="_blank">la presenza di Evk</a>.

La sua musica è di ispirazione tolkieniana, da caminetto e racconti epici. Basta un'occhiata ai titoli ancora prima di ascoltare le anteprime.

È un amico, ma se fosse questo il motivo della segnalazione gli farei un torto. È uno che sa il proprio mestiere.