---
title: "Forma e sostanza"
date: 2011-01-08
draft: false
tags: ["ping"]
---

Curioso che, nel Paese dove ogni giorno dispari si parla di conflitto di interessi, questa storia meriti cos&#236; poca attenzione.

Quasi certamente sul tuo Mac c'è Vlc, <a href="http://www.videolan.org/" target="_blank">VideoLan Client</a>, e se non c'è è il caso di scaricarlo, perché è un ottimo riproduttore di video in pressoché qualunque formato, gratuito, libero e con ampie ricadute anche per chi fa video professionale.

Tempo fa <a href="http://applidium.com/" target="_blank">Applidium</a> ha preso il codice di Vlc, che è universalmente libero e accessibile, e ne ha realizzata una versione per iOS. In questo modo anche chi usava iOS con iPhone, iPad e iPod touch poteva usare Vlc, che è software libero e gratuito.

Apple ha impiegato tempo relativamente lungo ad accettare il programma su App Store, perché il software libero presenta problematiche particolari in un sistema come iOS. Però poi ha approvato la pubblicazione e il progetto VideoLan non ha avuto alcunché da ridire.

Purtroppo esisteva già un precedente, il gioco <a href="http://www.gnu.org/software/gnugo/" target="_blank">Gnu Go</a>. Che, a seguito delle pressioni di Free Software Foundation, <a href="http://www.fsf.org/news/blogs/licensing/more-about-the-app-store-gpl-enforcement" target="_blank">è stato rimosso da App Store</a>. Go è un gioco affascinante e splendido che vale assolutamente la pena di scaricare e provare su Mac. Ma è altra questione.

Ad accusare Apple di violazione delle licenze di software libero per Vlc non è stato il progetto VideoLan, ma un suo singolo sviluppatore, Rémi Denis-Courmont, che ha agito a nome proprio e non del progetto.

Guardando alla pura forma, il reclamo è corretto: Apple viola la licenza di Vlc nel momento in cui ne rende disponibili copie che non possono essere liberamente distribuibili. Per cui Apple ha rimosso Vlc da App Store.

Nella sostanza, tipo centocinquanta milioni di apparecchi che potevano scaricare Vlc non possono più farlo. Il che non è esattamente lo spirito del software libero.

Come mai proprio Rémi Denis-Courmont e non altri, dal momento che il progetto raduna quasi una decina di sviluppatori regolari? A pensare male si fa peccato ma si azzecca, disse Andreotti. A pensare e approfondire invece si fa giornalismo e salta fuori che Denis-Courmont è <a href="http://www.remlab.net/CV.pdf" target="_blank">dipendente Nokia</a> dal 2007, dopo due anni di apprendistato in Nokia.

La riassume meglio di me <a href="http://obamapacman.com/2011/01/vicious-nokia-employee-gets-vlc-removed-from-apple-app-store/" target="_blank">Obama Pacman</a>:

<cite>Nel 2007, l'anno in cui Apple ha presentato iPhone, la quotazione delle azioni Nokia era al massimo di sempre, quasi 40 dollari per azione. Ma da allora le azioni sono continuamente diminuite e recentemente hanno toccato gli otto dollari per azione.</cite>

<cite>Viene da chiedersi quante azioni Nokia possieda Denis-Courmont, magari ottenute al momento dell'assunzione, o se non sia il suo capo in Nokia a pilotarlo.</cite>

Ars Technica ha <a href="http://arstechnica.com/apple/news/2010/11/the-vlc-ios-license-dispute-and-how-it-could-spread-to-android.ars" target="_blank">raccontato nei dettagli</a> tutta la vicenda e riporta una dichiarazione di Romain Goyet, cofondatore di Applidium:

<cite>Abbiamo lavorato gratis, aperto tutto il nostro <a href="http://git.videolan.org/" target="_blank">codice sorgente</a> e la</cite> app <cite>è disponibile gratis per chiunque. Per come la vedo non abbiamo violato la libertà di alcuno.</cite>

In nome della libertà di distribuzione del software, quelli con un apparecchio costruito da un concorrente di Nokia non possono più scaricare Vlc. Su iniziativa di un dipendente di Nokia.

Viva la forma. Però un occhio alla sostanza ogni tanto ci vuole.