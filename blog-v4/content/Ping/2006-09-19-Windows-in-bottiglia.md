---
title: "Windows in bottiglia"
date: 2006-09-19
draft: false
tags: ["ping"]
---

<strong>Alan Dragster</strong> sta provando <a href="http://www.codeweavers.com/products/cxmac/bottles/" target="_blank">CrossOver Mac</a> e ho il privilegio dell'esclusiva delle sue prime impressioni:

<cite>Pro:</cite>

<cite>- avere la possibilità di fare doppio clic su &#8220;MSProject.app&#8221; o &#8220;Lotus Administrator.app&#8221; e vederlo partire nella sua finestra (non molto Mac-like, ma sicuramente carino e VELOCE: più di Boot Camp e di  
Parallels che comunque uso e apprezzo) è IMPAGABILE;</cite>
<cite>- non devi preoccuparti dell'ambiente Windows (il mio problema più grosso è stato &#8220;a cosa corrisponde c:\Programfiles\ecc&#8230;&#8221;?): usi il programma che ti interessa e basta!</cite>

<cite>Contro:</cite>

<cite>- hanno una lista di programmi supportati (quasi tutti Microsoft) e funzionano benissimo (installare Project 2003 da Cd su MacBook Pro è durato tre minuti, in Parallels otto), ma se hai qualcosa al di fuori occorre penare e non sempre si riesce&#8230; (Sono stato cattivo: ho cercato di installare Hyperware di Kynetics nella versione appena repackaged by autodesk nel 1999&#8230; Ma su Parallels funzionava&#8230;). Farò  
altri test&#8230;</cite>

<cite>In conclusione:</cite>

<cite>- Boot Camp ha il grosso difetto di &#8220;dover lavorare su una macchina per volta&#8221; anche se i dati sono gli stessi (credo al momento attuale sia consigliabile solo a chi gioca su prodotti non ancora per Mac);</cite>
<cite>- Parallels è grande nel poter far girare più OS (anche contemporaneamente&#8230; provato di persona: boot da OSX, lancio parallels e sessione Gentoo, poi Windows Xp e infine Solaris 10&#8230; con 2Gb di Ram&#8230; ma i clienti avevano workstation allucinanti&#8230; per prezzo&#8230;;</cite>
<cite>- Crossover ha parecchi limiti di gioventù (spero) ma, come detto prima, il fatto di avere un Microsoft Visio.app di 300Kb su cui poter cliccare e fregarsene di Win, emulatori, eccetera è notevole!</cite>

<cite>To be done</cite>

<cite>- devo ancora valutare bene in termini di occupazione fisica su disco se Crossover abbia vantaggi o svantaggi&#8230;</cite>

Chissà se basterà a calmare i complessi di inferiorità di chi, se non usa Windows, non si sente alla pari del cognato della sorella del cugggino.