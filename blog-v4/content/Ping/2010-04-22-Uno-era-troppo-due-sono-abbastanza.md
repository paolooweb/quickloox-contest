---
title: "Uno era troppo, due sono abbastanza"
date: 2010-04-22
draft: false
tags: ["ping"]
---

Ho messo una croce sopra Gizmodo il giorno in cui il sito annunciò più o meno che Steve Jobs non avrebbe vissuto abbastanza da vedere Natale 2008.

Non perché fosse vero o falso; perché fare soldi sulla salute di altri, anche se quegli altri sono talmente celebri che viene facile disconoscerli come persone, è repellente.

Cos&#236; ho smesso di consultare Gizmodo e, se una notizia gli fa riferimento, non seguo il link e la cerco altrove, o ne faccio a meno. Chi mi ascolta di persona parlarne mi guarda un po' strano. Ma non è un problema.

Pronto eventualmente a ricredermi, ho invece un motivo in più.

Gizmodo ha pubblicato giorni fa lo <i>scoop</i> su un prototipo di iPhone che è stato (probabilmente) perso o smarrito in un bar da un incauto ingegnere Apple.

Ancora una volta non ha importanza il fatto in sé, né che Gizmodo abbia pagato cinquemila dollari per appropriarsi del prototipo da una persona che non era l'ingegnere e che ne è venuta in possesso. Apple vuole il segreto, c'è chi ci campa sopra. &#192; la guerre comme à la guerre, nessun problema.

Ma Gizmodo ha pubblicato il nome dell'ingegnere.

Questa persona l'ha fatta grossa, aziendalmente, e potrebbe perdere il lavoro. Ma dovrebbe finire l&#236; e, come tutti, dovrebbe avere diritto a una seconda <i>chance</i>. Ma chi gliela offrirà, dopo lo sputtanamento globale? Gizmodo è probabilmente già pronta a titolare pure su questo, sulla ditta capace di assumere il fesso, tra le righe fessa pure lei.

Oltretutto, siccome l'ingegnere ha solo parte involontaria nella vicenda, farne il nome non serve a nulla per Gizmodo, se non per incassare soldi ulteriori sullo scandalo.

Non era neppure una persona cos&#236; famosa da poterne trascurare l'umanità.

Per questo, se avevo messo una croce sopra Gizmodo, da adesso è croce doppia.