---
title: "L'isolamento è finito"
date: 2008-10-07
draft: false
tags: ["ping"]
---

Ripenso a qualche anno fa, quando avevo sudato settimane per rendere funzionante non ricordo più che simulatore di circuiti su Mac, a colpi di librerie Qt e codice da compilare.

Adesso <a href="http://www.macspice.com/" target="_blank">MacSpice</a> è a disposizione di chiunque, gratis. Sul sito abbondano pure i <em>tutorial</em> per scalare la curva di apprendimento, effettivamente un po' impegnativa.

Lo considero un buon esempio della crescita dell'offerta di software per Mac, in un settore dove una volta la disponibilità era pressoché inesistente. Mac non è più mondo a sé e se ne accorgono persino i programmatori.