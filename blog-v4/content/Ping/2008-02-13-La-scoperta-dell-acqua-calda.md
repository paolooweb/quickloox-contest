---
title: "La scoperta dell'acqua calda"
date: 2008-02-13
draft: false
tags: ["ping"]
---

Ho scoperto <a href="http://matrex.sourceforge.net/" target="_blank">Matrex</a>, foglio di calcolo vettoriale, ossia fatto per operare su grossi blocchi di dati e memorizzare selezioni particolari dei dati stessi nell'ambito di ogni progetto.

Suona fumoso e invece merita. Più semplice: un foglio fatto per lenzuolate molto grandi di dati, specie se arrivano da database esterni.

Matrex è <em>open source</em> e quindi è gratis. Non è facile; consiglio la documentazione e i <em>tutorial</em> video che si trovano sul sito. Ma chi ha a che fare con dati che meritano un trattamento del genere non si spaventerà (in alcuni casi il programma non si autoassembla e si presenta come una cartella di file Java; se succede, bisogna eseguire da Terminale il comando <code>./matrex_macosx.sh</code>, dopo essersi portati sulla cartella mediante il comando <code>cd</code>, accompagnato dal trascinamento della cartella di Matrex sulla finestra del Terminale.

Matrex importa anche file Excel e bisogna dirlo, altrimenti un programma di grande valore passerà inosservato per non avere timbrato il cartellino.

Sembra che un foglio elettronico possa essere tale solo se è Excel. Invece, come mostra superficialmente Numbers di Apple e in profondità Matrex, è possibile escogitare ottime soluzioni software alternative e, se ci fosse più libertà dal monopolio Microsoft, di soluzioni ce ne sarebbero assai di più.

L'acqua calda, come dicevo.