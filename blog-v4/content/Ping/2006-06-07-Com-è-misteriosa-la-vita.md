---
title: "Com&rsquo;&egrave; misteriosa la vita"
date: 2006-06-07
draft: false
tags: ["ping"]
---

<strong>Stefano</strong> mi ha fatto notare il problema di uno che all&rsquo;improvviso non sentiva pi&ugrave; l&rsquo;audio nei filmati Flash e si &egrave; &ldquo;rivolto ad Apple&rdquo;, che gli ha consigliato di <a href="http://www.applematters.com/index.php/forums/viewthread/768/" target="_blank">aprire e chiudere GarageBand</a>.

Problema risolto. Anche se per me &egrave; un mistero che si apre.