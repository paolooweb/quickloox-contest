---
title: "Il fine non giustifica proprio tutti i mezzi"
date: 2008-11-03
draft: false
tags: ["ping"]
---

Faccio colazione al bar e due studentesse lavorano insieme sedute a un tavolo vicino. Una ha un Dell, una un MacBook.

Emerge l'esigenza di fare di conto. Una apre la calcolatrice sul portatile. L'altra estrae una calcolatrice dalla borsa. Non sto neanche a dire chi. Coincidenza, naturalmente.

Vedo un limite e sono incerto se porlo sulla macchina o su chi la utilizza. O forse le inadeguatezze si sostengono a vicenda.