---
title: "Scoccano le venti"
date: 2010-08-09
draft: false
tags: ["ping"]
---

Sempre parlando di passatempi agostani ricreativi e che tuttavia insegnano qualcosa, c'è l'economicissimo e intelligente Cubo di Rubik.

C'è ovviamente <a href="http://www.geometer.org/rubik/index.html" target="_blank">l'applicazione su Mac</a> (bruttina e tuttavia strepitosa a qualsiasi livello di padronanza) e ci sono le <i>app</i> gratis e a pagamento per iOS (basta cercare la parola <i>rubik</i> in iTunes Store).

C'è anche una ricorrenza storica abbastanza intrigante: è stato appena dimostrato quello che i matematici supponevano da vent'anni e non erano ancora riusciti a confermare: se si opera nel modo più efficiente possibile &#8211; talmente lontano dalle nostre possibilità che è stato chiamato l'Algoritmo di Dio &#8211; nessuna mescolatura del cubo <a href="http://www.cube20.org/" target="_blank">richiede più di venti mosse</a> per essere risolta. Garantisco però che, la prima volta che ci si riesce, anche cinquecento mosse sono una bella soddisfazione!