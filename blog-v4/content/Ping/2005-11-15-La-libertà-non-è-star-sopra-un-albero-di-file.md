---
title: "La libertà non è star sopra un albero (di file)<p>"
date: 2005-11-15
draft: false
tags: ["ping"]
---

Non so se fuori da un Mac si possa davvero lavorare in questo modo<p>

Ogni tanto trovo quello che Windows fa le stesse cose e costa meno. Che costi meno è molto, molto relativo, ma glissiamo.<p>

Oggi è stata una giornata massacrante. I problemi si sono succeduti uno dietro l&rsquo;altro e il tempo per guardare alle esigenze della macchina è stato proprio poco.<p>

Risultato: il Dock conta 35 applicazioni aperte, per un totale di almeno duecento finestre di ogni genere. Il PowerBook non è esattamente un fulmine di reattività, ma da mattina a sera non ha battuto ciglio.<p>

Windows farà le stesse cose, ma vorrei vederlo funzionare con trentacinque applicazioni aperte, giusto per togliermi il pensiero.<p>

Come diceva Gaber la libertà non è uno spazio libero (ho disco a volontà, infatti). Libertà è partecipazione.<p>

Il Mac è presissimo, ma è tuttora al mio servizio. Su altri sistemi, sarebbe già arrivato il momento in cui il sistema si imbizzarrisce, e non si partecipa più.<p>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>