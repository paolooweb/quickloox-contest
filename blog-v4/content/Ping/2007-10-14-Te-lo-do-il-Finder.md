---
title: "Dare l'addio al Finder"
date: 2007-10-14
draft: false
tags: ["ping"]
---

Per la serie <em>vivere pericolosamente</em>, ecco in poche righe <a href="http://waferbaby.com/posts/waferbaby/2007/10/11/ditching-finder-app" target="_blank">come si sostituisce il Finder</a> con <a href="http://quicksilver.blacktree.com/" target="_blank">QuickSilver</a>.

Non dire che te l'ho segnalato io. Soprattutto, se fai casino, non chiedermi come tornare indietro. Nel link ci sono le istruzioni anche per quello. :-)