---
title: "Tre paginette per un dubbio"
date: 2009-05-22
draft: false
tags: ["ping"]
---

Sono il <a href="http://www.odfalliance.org/resources/fact-sheet-Microsoft-ODF-support.pdf" target="_blank">documento Pdf</a> rilasciato da <a href="http://www.odfalliance.org/blog/index.php/site/microsofts_odf_support_falls_short/" target="_blank"></a>Odf Alliance sul supporto del formato Open Document (quello di OpenOffice) da parte di Microsoft, la quale ha promesso la compatibilità e però fatica (o rema contro, si maligna) a mantenere la promessa.

Premesso che Odf Alliance rappresenta solo se stessa e ovviamente i propri aderenti, le sue conclusioni sono difficilmente contestabili e la lettura delle tre paginette è estremamente istruttiva.

In estrema sintesi, i punti qualificanti sono due.

Primo, un foglio di calcolo Odf di prova creato in Google Docs, KSpread, Lotus Symphony, StarOffice, OpenOffice, NeoOffice e Sun Plug-In 3.0 per Office, non funziona correttamente su Excel 2007. Excel è l'unica di queste applicazioni che non ce la fa a leggere il foglio creato da un'altra applicazione.

Secondo, il <a href="http://www.sun.com/software/star/odf_plugin/" target="_blank"><em>plugin</em> di Sun per Office</a> e il progetto <em>open source</em> <a href="http://odf-converter.sourceforge.net/" target="_blank">OpenXml/Odf Translator Add-ins for Office</a>, che permettono di portare documenti Odf in Office, sono più compatibili Odf di Office 2007 con Service Pack 2. Microsoft avrebbe tempi molto lenti di sviluppo, ma Odf 1.1 è stato approvato a febbraio 2007 e Odf 1.0 a maggio 2005, divenuto standard Iso nel maggio 2006.

(Open Alliance sostiene che il problema sia dovuto a una semplice questione di parentesi quadre e rimanda alla sezione 8.3.1 della <a href="http://docs.oasis-open.org/office/v1.1/OpenDocument-v1.1.html" target="_blank">specifica Odf 1.1</a>, ma una veloce occhiata mi lascia dubbi e non lo prenderei per oro colato.)

A margine, Office non supporta la cifratura con password dei documenti Odf e neanche la registrazione delle modifiche.

Microsoft, ricorda il documento, ha una ricca tradizione alle spalle di standard &#8220;rispettati&#8221; in modo penalizzante, in modo da svalutarli e complicarne l'adozione. Un esempio per tutti è Java in Internet Explorer. Microsoft preinstallò una versione incompatibile con quella autentica e inser&#236; estensioni proprietarie, per poi lasciare tutto a languire senza aggiornarlo seguendo i progressi di Java. Lo scopo era diffondere pessime versioni di Java su milioni di macchine, in modo che Java diventasse una cosa sconsigliabile da usare. Office 2007 creerà milioni di documenti Odf malfunzionanti, che non gioveranno a un formato che, ricordo, è standard Iso.

Il dubbio è: Microsoft ci è o ci fa?