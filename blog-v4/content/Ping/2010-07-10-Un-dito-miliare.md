---
title: "Un dito miliare"
date: 2010-07-10
draft: false
tags: ["ping"]
---

L’argomento è ludico, ma le tematiche sottostanti vanno piuttosto in profondità.

Sono giocatore di ruolo da decenni e ho una qualche esperienza in termini di praticità degli strumenti… in gioco.

L’altra sera ho usato iPad come supporto per utilizzare la scheda personaggio, l’elenco dei poteri a disposizione, il manuale di gioco. E per la prima volta, in decenni, la consultazione sul sistema informatico è stata più efficiente di quella manuale.

Significa che il Pdf della scheda e dei poteri, scorso da Safari, mi mostrava le cose più in fretta della tradizionale stampata. E il Pdf del manuale, grazie a GoodReader e alle sue funzioni, mi portava più rapidamente alle pagine desiderate che l’edizione su carta.

Carta, scheda e manuale di gioco, che avevo con me e che non ho minimamente aperto. Quando si gioca di ruolo il puntiglio sta a zero: si va per la via più veloce e non esiste un’idea tipo usare a tutti i costi il computer anche se è meno efficiente.

Non mi era mai successo prima. È una pietra miliare nella mia insignificante storia di giocatore e tuttavia, nella sua banalità, l’episodio traduce la rivoluzione di iPad in pratica più di mille analisi.

Si noti infine che la tavoletta è intrinsecamente più amichevole. Al tavolo con gli altri sparisce ogni problema di spazio e non c’è l’effetto-barriera del portatile aperto.