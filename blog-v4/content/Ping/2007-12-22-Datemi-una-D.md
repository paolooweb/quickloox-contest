---
title: "Datemi una D"
date: 2007-12-22
draft: false
tags: ["ping"]
---

Per la Vigilia mi concedo una piccola celebrazione tutta privata.

Come forse sai, memorizzo internamente i Ping con un codice di tre lettere. Da pochissimi giorni è finita la serie iniziata con <em>caa</em>.

Andare da <em>caa</em> a <em>czz</em> significa scrivere 676 pezzi. Ben più di un anno. In termini informatici è una distanza corposa. Di iPhone non c'era neanche l'idea. Leopard lo conoscevano ben pochi e nessuno fuori da Apple.

Questo pezzo è <em>dad</em>. Mi tocca piacevolmente salutare mio papà che è in ascolto, per via del doppio senso inglese; siccome sta per essere Natale, mi toccano altrettanto piacevolmente gli auguri.

Ne ho uno solo, semplice: festeggia.

Mica la D, hai certamente cose più importanti. Basta solo che lo facciamo tutti insieme.