---
title: "La sfiga insiste"
date: 2007-06-02
draft: false
tags: ["ping"]
---

Devo chiedere umilmente scusa per la seconda volta, e consecutiva. Devo fare saltare il venerd&#236; del gioco di oggi. Come venerd&#236; scorso, si rinvia a mercoled&#236; prossimo, 6 giugno, dalle 13 alle 14, stanza di iChat <code>gamefriday</code>, gioco <a href="http://www.freeciv.org" target="_blank">FreeCiv</a>.

Abbi pazienza, è un momento contingente e passerà.

Il guaio è che il mercoled&#236; perdo, come è successo l'altroieri a <a href="http://jrisk.sf.net" target="_blank">Jrisk</a>. Mannaggia.