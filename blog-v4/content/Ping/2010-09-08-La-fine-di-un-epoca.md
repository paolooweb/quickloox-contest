---
title: "La fine di un'epoca"
date: 2010-09-08
draft: false
tags: ["ping"]
---

VersionTracker, il sito per trovare gli aggiornamenti software di tutti i programmi per Mac, ora porta a <a href="http://download.cnet.com/mac/?tag=vtredir" target="_blank">una pagina di Cnet Download</a>. Cnet, acquistata un paio di anni fa da Cbs, si era già annessa in precedenza anche MacFixit.

In retrospettiva, forse non è un caso che da anni trovassi più adeguato ai miei gusti <a href="http://www.macupdate.com" target="_blank">MacUpdate</a>.

Altre alternative sono, in parte, <a href="http://osx.iusethis.com/" target="_blank">I Use This</a> e <a href="http://www.eagle-of-liberty.com/logicielmacupdate/" target="_blank">LogicielMacUpdate</a>. Ricordo naturalmente che, per l'open source, spesso si fa prima e si trova anche meglio su <a href="http://sourceforge.net" target="_blank">SourceForge</a>.

I più temerari potrebbero voler collaudare la versione Development di <a href="http://metaquark.de/appfresh/" target="_blank">AppFresh</a>, che come Logiciel mira a creare un meccanismo simile a quello di Aggiornamento Software, per tutti i programmi residenti sui Mac, ma sembra più evoluto&#8230; e più in corso di lavorazione, per ora.

E vorrei dire che oramai moltissimi programmi Mac si autoaggiornano. Forse è proprio terminata un'epoca.