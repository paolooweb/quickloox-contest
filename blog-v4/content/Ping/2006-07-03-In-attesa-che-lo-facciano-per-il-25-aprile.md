---
title: "In attesa che lo facciano per il 25 aprile"
date: 2006-07-03
draft: false
tags: ["ping"]
---

Festeggiamo il 4 luglio con un po' di <a href="http://seiryu.home.comcast.net/savers.html" target="_blank">fuochi artificiali</a> in forma di salvaschermo.

Anche per scoprire quanti passi avanti si sono fatti nell'animazione dai tempi di <a href="http://www.ee.surrey.ac.uk/Personal/L.Wood/screensavers/" target="_blank">Pyro!</a>.
