---
title: "Kit con sorpresa"
date: 2006-08-16
draft: false
tags: ["ping"]
---

Non so quando potremo vederne in versione finale prima di Leopard. Però ho la sensazione che WebKit, il motore di Safari, stia facendo grandi progressi, a giudicare da quanto dicono su <a href="http://webkit.opendarwin.org/blog/" target="_blank">Surfin' Safari</a>.

Per gli avventurosi c'è sempre la possibilità di provare in anteprima una versione in sviluppo, naturalmente, sia tramite i <a href="http://nightly.webkit.org/" target="_blank">build notturni</a> che <a href="http://webkit.opendarwin.org/building/checkout.html" target="_blank">abbonandosi agli aggiornamenti</a> del codice via subversion.

WebKit sta diventando una bella cosa. Non per niente gli snobboni linuxiani di Khtml hanno avviato il progetto <a href="http://dot.kde.org/1152645965/" target="_blank">Unity</a> per ricongiungere le due basi di codice a tutto vantaggio nostro. L'open source scorre forte in questi uomini. :)