---
title: "10.2.2 soddisfa anche BBEdit"
date: 2002-11-13
draft: false
tags: ["ping"]
---

Sarà una coincidenza, ma c’era una cosa che poteva funzionare meglio, e anche quella...

Passare a Jaguar prima e a Mac OS X 10.2.1 poi è stata per me una mossa vincente su tutti i fronti. Unico problema, un effetto grafico indesiderato che moltiplicava a video la selezione di un Url su cui facevo Comando-clic in BBEdit e Mailsmith.

Ho installato Mac OS X 10.2.2 e il problema è stato risolto, come risulta anche dalla <link>nota tecnica</link> http://docs.info.apple.com/article.html?artnum=107140 dell’aggiornamento.

Sono contento perché avevo segnalato il (piccolo) problema al feedback e, anche se nella realtà glielo avranno detto in cinquantamila, posso pensare di avere messo il mio piccolo mattone per costruire un Mac OS X migliore.

È contento anche BBEdit, per quanto nel suo stile austero si noti poco. Siamo contenti un po’ tutti, quando Apple ci ascolta. :-D

Lucio Bragagnolo
lux@mac.com