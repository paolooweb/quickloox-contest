---
title: "Presto e bene possono andare insieme"
date: 2002-04-03
draft: false
tags: ["ping"]
---

Ma è possibile o no cambiare la batteria a un PowerBook con Mac OS X in Stop?

Apple raccomanda di non cambiare batteria a un PowerBook in Stop e non alimentato esternamente se si sta usando Mac OS X (con Mac OS 9 vengono assicurati dieci secondi a disposizione). Ma qualcuno afferma di esserci riuscito.
Cominciano ad arrivare segnalazioni e sembra, pare, che se la batteria viene cambiata quando è a fine corsa - e magari il PowerBook si è già messo in Stop da solo per risparmiare energia - il computer si spenga e resetti financo la Pram.
C’è però chi assicura di avere provato a cambiare (rapidamente) batteria prima dell’esaurimento della carica, con un 5% circa di autonomia disponibile, e di avere poi risvegliato regolarmente il computer dallo Stop.
Sembra, pare.

<link>Lucio Bragagnolo</link>lux@mac.com