---
title: "Quanta arroganza nell’essere migliori"
date: 2010-07-26
draft: false
tags: ["ping"]
---

Illuminante questo <a href="http://kensegall.com/blog/2010/07/the-ever-arrogant-apple/" target="_blank">post</a>, al punto che voglio riportarne una buona parte.

<cite>A seguito della conferenza stampa sull’Antennagate, certi critici hanno concluso velocemente che Apple si sia comportata nel modo arrogante che le è consueto.</cite>

<cite>Non potrei concordare maggiormente.</cite>

<cite>Come può pensare Apple di fare sparire un problema con una custodia gratis che fa sparire il problema? Devono soffrire di più.</cite>

<cite>Questa azienda è stata di fatto fondata sull’arroganza. Immaginarsi, due ragazzotti in un garage che si mettono in testa di detronizzare aziende come Ibm e Hewlett-Packard. Più avanti ci hanno detto di abbandonare l’interfaccia standard dei Pc e controllare i nostri computer con uno sciocco mouse. Con superiorità sprezzante hanno eliminato i floppy disk che amavamo così tanto. Urgh.</cite>

<cite>Se solo avessimo pensato a fermarli allora.</cite>

<cite>Perché non c’è voluto molto prima che Apple – priva di qualsiasi vera esperienza nell’elettronica di consumo – avesse l’impudenza di costruire il lettore musicale che avrebbe dovuto realizzare Sony o qualche altra azienda più qualificata. […]</cite>

<cite>Con iPhone, Apple ha portato la sua arroganza all’estremo. Hanno marciato su un mercato dominato da aziende di succsso globale come Nokia e Motorola, credendo di poter insegnare loro come reinventare lo smartphone. Ma quanto può essere egomaniaca un’azienda così?</cite>

<cite>Poi è arrivato iPad, dove l’arrogante arroganza di Apple è stata squadernata a chiunque. Apple all’ennesima potenza, che ci spiega di poter fare quello che Microsoft e altri non riescono a fare da un decennio. Dal giorno alla notte creano una nuova categoria di prodotto e si aspettano che qualcuno dia retta alla loro opinione sul futuro dei computer? […]</cite>

<cite>Siamo al punto che Apple non si perita neanche di nascondere la propria arroganza. Crea apparecchi che altre aziende avrebbero dovuto creare, seguono gli standard solo quando gli piace, snobbano la ricerca per creare solo prodotti che loro stessi userebbero… e figuriamoci che non lasciano neanche snaturare le loro piattaforme dal primo che arriva!</cite>

<cite>Guardate che cosa hanno fatto alla povera Adobe, privata dei suoi diritti di impiegare più di tre anni a capire come fare funzionare Flash su apparecchi tascabili. E come hanno costretto gli sviluppatori a scrivere software specificamente per iPhone, quando era così comodo limitarsi a reimpiegare software scritto per telefoni inferiori. E per giunta si azzardano perfino a insistere che le applicazioni rispondano a standard elementari di qualità e affidabilità. Con questo atteggiamento “o mangi la minestra o salti la finestra” Apple nega la libertà di scelta ai consumatori, forzandoli ad accontentarsi di sole 234 mila applicazioni tra le quali scegliere. […]</cite>

<cite>Ovviamente, l’eccesso di arroganza sarà la loro nemesi. Si trascuri quanto le loro quote di mercato siano cresciute così tanto e così a lungo in così tanti settori. O che il modello di attività di Apple produca profitti superiori a quello di altre aziende tecnologiche. È solo fortuna.</cite>

<cite>Un giorno il gregge irretito dall’incantesimo di Steve Jobs si sveglierà e chiederà che Apple si comporti come le altre aziende. Finalmente gli iPhone somiglieranno ai Droid, i Mac saranno come i Pc e anche Apple potrà godersi il senso di precarietà economica tipico del fabbricante di Pc. GLi azionisti Apple potranno infine bearsi di un investimento che gli evita di volare in alto come ora.</cite>

<cite>Ecco come dovrebbe essere.</cite>