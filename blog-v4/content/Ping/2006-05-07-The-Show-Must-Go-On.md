---
title: "The Show Must Go On"
date: 2006-05-07
draft: false
tags: ["ping"]
---

Aggiornato con successo a <a href="http://www.videolan.org/vlc/download-macosx.html" target="_blank">VideoLan Client</a>, alias Vlc, versione 0.8.5.

A conferma, ce ne fosse stato bisogno, che il <a href="http://www.videolan.org/pr2006-2.html" target="_blank">pesce d&rsquo;aprile</a> comparso sul sito era un pesce d&rsquo;aprile. Gli inglesi dicono <em>as sure as an egg is an egg</em>.