---
title: "Ho visto un Mac, OS X"
date: 2003-02-24
draft: false
tags: ["ping"]
---

Un indizio nazionalpopolare per capire che la transizione sta avvenendo

Di solito guardo la televisione solo dopo mezzanotte. Domenica scorsa, <i>rara avis</i>, ho assistito a una puntata di Alias, storie di spie e complotti internazionali in salsa Disney.

Orbene, la violazione di un sistema di sicurezza è accaduta usando – nella finzione ovviamente – una finestra Terminale di Mac OS X!

Più avanti c’era anche una schermata di Photoshop travestita da pannello di identificazione biometrico o qualcosa del genere, ma mi ha impressionato meno.

Perché è la prima volta che vedo Mac OS X platealmente utilizzato in una serie Tv e questo è un segno che Mac OS X, vuoi o non vuoi, avanza.

<link>Lucio Bragagnolo</link>lux@mac.com