---
title: "Passato e futuro"
date: 2009-12-13
draft: false
tags: ["ping"]
---

<a href="http://infinite-labs.net/" target="_blank">Emanuele</a> è stato bravo ad avvisarmi e contemporaneamente mi ha fatto perdere tutto il divertimento.

Infatti <a href="http://commandguru.com/" target="_blank">Command Guru</a>, <i>iPhone Reality Show</i>, si è chiuso e il team impegnato a creare una applicazione per iPhone in tempo reale, documentando rigorosamente tutto quanto su web, ha ottenuto l'obiettivo.

Resta il passato. È possibile accedere da iTunes agli <i>highlight</i>, ai momenti salienti della settimana di lavoro, e ai commenti inseriti in diretta via Twitter.

Resta il futuro. L'applicazione c'è davvero e la pagina è un perfetto prototipo di nuova comunicazione, da studiare nelle scuole. Specie quelle di <i>web design</i>.

Nel gruppo mancava un giornalista (non perché sarebbe piaciuto esserci a me). Parlo seriamente; hanno svolto un lavoro incredibile nel mettere insieme gli ingredienti della storia. Manca, però, la storia ed è il limite del sito.

Il resto, tuttavia, è da esempio. Complimenti.