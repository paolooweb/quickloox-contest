---
title: "A caccia di Grapher"
date: 2008-08-02
draft: false
tags: ["ping"]
---

<strong>Gianluigi</strong> commentava <cite>mi piacerebbe sapere che cosa ne pensi tu e i lettori di Ping di questa mia nuova richiesta per Apple (o anche per gli sviluppatori): Grapher o qualcosa del genere su iPhone.</cite>

Gianluigi: iPod touch, non iPhone. Se dici iPod touch, puoi trattarlo come un computer.

ne penso tutto il bene possibile. Prima di App Store circolava una calcolatrice grafica non ufficiale che sembrava promettente.

Non è ancora tornata (i programmi non ufficiali vanno riscritti e ci vuole tempo). Su App Store al momento ci sono <a href="http://www.mpkju.fr/~touchplot/" target="_blank">TouchPlot</a> e Tion. Per uno studente già hanno molto senso.