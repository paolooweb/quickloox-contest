---
title: "Otto giga grandi cos&#236;"
date: 2009-01-11
draft: false
tags: ["ping"]
---

Segue uno scambio di corrispondenza avuto sull'argomento <em>MacBook Pro 17&#8221; è davvero innovativo nel consentire l'installazione di otto gigabyte di Ram?</em>

Il mio amico scrive <cite>Sui Pc si arriva già sino a 16 gigabyte di Ram. Allego esempio tratto dal sito Dell relativo alla loro macchina di punta, Precision Mobile Workstation M6400 COVET Advanced (W11M6403).</cite>

Io verifico. Confesso che ero preparato a riscontrare una mia inesattezza. Dopo la verifica ho risposto, cos&#236;.

<cite>Su Apple Store è possibile preordinare un <a href="http://store.apple.com/it/configure/MB604T/A?mco=MzA3MTQ0OA" target="_blank">MacBook Pro 17" con otto gigabyte di Ram</a>, consegna in 3-4 settimane.</cite>

<cite>Su Dell online la configurazione standard del sistema <a href="http://configure2.euro.dell.com/dellstore/config.aspx?c=it&cs=itbsdt1&kc=305&l=it&oc=W11M6403&s=bsd&sbc=workstation-precision-m6400-cov" target="_blank">non va oltre i quattro gigabyte</a>, perché prevede Windows a 32 bit.</cite>

<cite>Se si sceglie una versione di Windows a 64 bit, effettivamente la Ram ordinabile arriva fino a sedici gigabyte&#8230; solo che qualunque versione di Windows a 64 bit offerta da Dell sulla pagina di configurazione è incompatibile con lo schermo Lcd in dotazione al portatile, come da avviso che appare dinamicamente sulla pagina stessa. Avviso che, se ignorato, impedisce di completare l'acquisto.</cite>

<cite>La pagina invita a cambiare l'opzione schermo o l'opzione sistema operativo. Ma l'ozpione schermo non lo è; c'è solo quello schermo l&#236;. Si può solo cambiare sistema operativo. Se si selezionano 16 gigabyte di Ram e poi si sceglie Windows a 32 bit, l'unica versione che elimina l'incompatibilità con lo schermo, la Ram viene riportata a quattro gigabyte.</cite>

<cite>Riassumendo: lo si può avere con sedici gigabyte di Ram a patto di scegliere un sistema operativo con cui non funziona.</cite>

Messo il punto di fine paragrafo, ho provato un amore per i miseri otto gigabyte di Ram di MacBook Pro 17&#8221; cos&#236; grande che mi sembravano quasi ottocento.