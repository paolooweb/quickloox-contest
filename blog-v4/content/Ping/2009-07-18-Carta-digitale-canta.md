---
title: "Carta digitale canta"
date: 2009-07-18
draft: false
tags: ["ping"]
---

Sa di formaggio tradurre ogni giorno una novità di Snow Leopard dalla pagina del sito di Apple che tradotta dovrebbe già essere. Infatti è un'attività&#8230; certosina.

<b>Visione automatica delle conversazioni passate</b>

Se vengono registrate le trascrizioni delle chiacchierate, si può configurare iChat in modo da mostrare automaticamente gli ultimi 5, 25, 100 o 250 messaggi scambiati con un contatto, non appena si riapre una chat con il contatto in questione.

E sarà la sagra de <i>L'avevo detto, io</i>&#8230;