---
title: "Come si cambia per non annoiare"
date: 2006-05-21
draft: false
tags: ["ping"]
---

Stimolato da <strong>Fearandil</strong>, sto cercando di approfondire nel poco tempo a disposizione la questione di Darwin, la parte open source di Mac OS X.

Ho finalmente (finalmente per me) appurato che Apple ha pubblicato e continua a pubblicare i sorgenti integrali di Darwin per PowerPc, ma non ha mai pubblicato la parte strettamente inerente il kernel di Darwin per Intel (mentre viene pubblicata la parte non-kernel di Darwin per Intel).

Questo &egrave; sufficiente per fare scrivere al Tom Yager di turno che <a href="http://www.macworld.co.uk/news/index.cfm?NewsID=14663&amp;Page=1&amp;pagePos=8" target="_blank">Apple chiude Mac OS X</a>. &ldquo;Notizia&rdquo; ripresa dai soliti disinformatori. Apple non ha chiuso niente. Semmai deve ancora aprire, che &egrave; un concetto diverso. Per&ograve;, spiegata com&rsquo;&egrave;, non fa abbastanza clamore.

In realt&agrave; Apple non ha ancora <a href="http://lists.apple.com/archives/Fed-talk/2006/May/msg00105.html" target="_blank">annunciato niente a riguardo</a> e si pu&ograve; benissimo fantasticare su quello che intende fare o non fare ma, fino a che non lo fa, sono fantasticherie pure, in un senso o nell&rsquo;altro.