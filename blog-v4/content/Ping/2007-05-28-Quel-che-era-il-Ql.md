---
title: "Quel che era il Ql"
date: 2007-05-28
draft: false
tags: ["ping"]
---

Momento di nostalgia retrocomputeriale. Eccezione a confermare la regola, stavolta il Mac c'entra solo per il processore.

Il mio primo lavoro liberoprofessionale fu infatti svolto su un Sinclair Ql. L'unico con una tastiera vera che costava abbastanza poco da rientrare nel mio budget e aveva un vero word processor, Quill.

Dopo che un amico disgraziato mi ha perso tutti i microdrive, il Ql è rimasto negli scatoloni. Mentre ferve l'attività di chi non lo ha mai abbandonato. Il Ql aveva un processore Motorola 68008, parente stretto del 68000 del primo Mac, e un sistema operativo mmultitasking in soli 32k!

Non esiste, che io sappia, un sito di riferimento per gli appassionati di Sinclair Ql. <a href="mailto:astrel@telvia.it?subject=Dvd Sinclair Ql">Antonio</a>, però, ha preparato Dvd confezionati benissimo che riassumono film e foto dei loro incontri, tra il 1990 e il 2003, per mezza Italia e altrettanto mezza Europa.

Scrivi pure ad Antonio per avere informazioni e magari vedere qualche Pdf illustrativo. Io aggiungo solo che emulare un Ql su Mac OS X è tuttora non semplicissimo, ma si può provare a partire dalle pagine di <a href="http://www.terdina.net/ql/MacQL.html" target="_blank">Daniele Terdina</a>, o da <a href="http://www.dilwyn.uk6.net/emu/index.html" target="_blank">questa pagina</a>. Per veri esploratori senza paura, <a href="http://linux-q40.sourceforge.net/" target="_blank">Linux-Q40</a>. Se qualcuno lo fa funzionare su Mac, lo ospito a mie spese al Gallia a Milano fino a che non mi ha spiegato tutto per bene.