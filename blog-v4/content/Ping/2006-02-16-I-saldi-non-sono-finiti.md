---
title: "I saldi non sono finiti"
date: 2006-02-16
draft: false
tags: ["ping"]
---

Fino al 19 febbraio, il word processor <a href="https://www.marinersoftware.com/" target="_blank">Mariner Write</a> &egrave; in vendita con lo sconto del 30 percento, a 34,95 dollari.
