---
title: "Buon 1234567890 a tutti"
date: 2009-02-13
draft: false
tags: ["ping"]
---

L'orologio interno di Unix, che conta i secondi anche dentro Mac OS X, tra qualche ora - poco dopo la mezzanotte, <a href="http://coolepochcountdown.com/" target="_blank">alle 00:31:30</a> - <a href="http://www.1234567890day.com/" target="_blank">segnerà il valore 1234567890</a>.

Per quanto ne so non ci sono festeggiamenti ufficiali in Italia. In Europa succede qualcosa a Vienna, a Copenaghen, a Budapest e in Croazia.

Vorrà dire che privatamente, alla luce confidenziale delle nostre tastiere retroilluminate (se non delle nostre <a href="http://us.kensington.com/html/5159.html" target="_blank">Flylight</a>), stapperemo un succo di frutta fresco!