---
title: "Video, quindi suono"
date: 2008-06-15
draft: false
tags: ["ping"]
---

Quello che ha effettuato <a href="http://www.stack.it" target="_blank">Stefano</a> è solo un esperimento fatto in quattro e quattr'otto. Eppure con <a href="http://www.varasoftware.com/products/screenflow/" target="_blank">Screenflow</a> ha creato un <a href="http://www.stack.it/xfer/SteveCast-Miro.mov" target="_blank">tutorial decisamente efficace</a>.

Quelli che cianciano sui programmi che mancano, ogni tanto dovrebbero considerare anche ciò che fanno i programmi che ci sono.