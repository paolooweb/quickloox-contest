---
title: "Come allo zoosafari"
date: 2010-07-13
draft: false
tags: ["ping"]
---

Hai voglia di essere studiato alla ricerca di misteri insoluti e scrutato per rivelare la ragione dei tuoi comportamenti?

Un biglietto aereo e, da domani al 21 luglio, vai a Redmond al <i>campus</i> Microsoft. Cercano gente con iPad da osservare e interrogare per due ore allo scopo di <a href="http://www.facebook.com/event.php?eid=136116846413216" target="_blank">ottenere feedback</a>.

In premio, un <i>Microsoft gratuity item</i>. Probabilmente uno Zune o un Kin, di valore effettivamente zero.

La notizia più sensazionale è che ci sono tre milioni e passa di iPad in giro ma sembra che nessuno in Microsoft sia in grado di prenderne uno in mano e capire perché funziona. Puntano invece ad arrivarci tramite lo studio di strani animali evidentemente diversi da loro.

E poi dicono che siamo tutti uguali.