---
title: "Le nuove avventure dei vecchi"
date: 2004-03-29
draft: false
tags: ["ping"]
---

Che cosa non si fa con un programma per chat e un paio di script

Noi vecchi ci ricordiamo di quando al computer si giocavano le avventure testuali. Sei in una stanza e vedi una porta chiusa. Apri la porta. Sei in una stanza e vedi una porta aperta. Nord. Sei su un corridoio e alle tue spalle c’è una porta aperta. E così via.

A parlarne così sembra roba da deficienti e invece sono stati creati mondi testuali inarrivabili per creatività. I più creativi e bravi di tutti erano quelli di Infocom, tanto che quando si è spento il mercato delle avventure testuali (i Mud su Internet sono popolati di gente vera e intanto i computer hanno iniziato a mostrare anche la grafica) i loro giochi sono stati resi disponibili in mille modi da altrettanti irriducibili.

Così dall’Apple II i giochi Infocom sono migrati al Macintosh e persino al Newton. Oggi sono su... iChat. Come mi ha segnalato Antonio Dini, se apri iChat e vai in chat con l’utente infocombot (o infocombot2 o infocombot3), e saluti con “hello?”, puoi scegliere un’avventura testuale Infocom da giocare nella finestra di iChat, con tanto di save e restore.

I server di Aim non permettono che un utente invii più di tanti messaggi in contemporanea, quindi a volte i bot Infocom sono occupati. Non demordere e riprova più tardi.

<link>Lucio Bragagnolo</link>lux@mac.com