---
title: "Prove tecniche di pubblicazione immagini<p>"
date: 2004-10-12
draft: false
tags: ["ping"]
---

Sito nuovo, collaudi da fare<p>

Non ha giovato che scrivessi un pezzo dedicato al confronto a iMac G5 e Sony Vaio, con immagine, di domenica. Qualcosa è andato storto e l&rsquo;immagine alla fine non è stata pubblicata.<p>

Ci riprovo questa volta. Ricordo che tutto nasceva da una segnalazione di Andrea Fistetto di Manduria (TA), con il quale mi scuso.<p>

Anche riguardandola, l&rsquo;immagine, proprio non c&rsquo;è partita. Spero possa vederla anche tu.<p>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>