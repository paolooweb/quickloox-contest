---
title: "Preferenze, a casa"
date: 2004-04-12
draft: false
tags: ["ping"]
---

Una delle tante novità di Panther, banale ma concreta

Quando Apple ha pubblicato Panther vantava oltre 150 miglioramenti al sistema. Durante una discussione in una mailing list ne avevo contate una settantina in due giorni e, considerate le mie limitazioni tecniche, la dichiarazione di Apple mi sembrava attendibile.

Lo sembra ancora di più oggi, dopo qualche mese di Panther, quando continuano a saltare fuori nuove funzioni, piccole e grandi, un po’ da tutte le parti. una di queste è banale e tutti l’avranno scoperta da un pezzo, ma per me è una novità: nelle Preferenze del Finder c’è la possibilità di impostare la cartella che si deve aprire al momento di chiedere una nuova finestra del Finder con Comando-n.

Così, per esempio, le nuove finestre si possono aprire mostrando direttamente Inizio (Home), la cartella Documenti, o altra a piacere.

Due morali: primo, Panther è stato davvero un salto di qualità; secondo, fare ogni tanto un giro delle varie preferenze non fa mai male.

<link>Lucio Bragagnolo</link>lux@mac.com