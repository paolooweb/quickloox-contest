---
title: "Quando il blog (forse) (non) è (del tutto) in vacanza"
date: 2006-07-21
draft: false
tags: ["ping"]
---

Il mio intento è tenere il blog attivo e aperto anche nel mese di agosto, come in tutti gli altri. Ma non darlo per scontatissimo. Se dovessero esserci ritardi nell'approvazione dei messaggi, o carenza di commenti miei, significa solo che sono momentaneamente lontano dal computer.

Lo stato di provvisorietà durerà fino al 31 agosto (di fatto smetterà molto prima, ma niente è più incerto dei progetti agostani).

Gli auguri di buone vacanze a tutti quanti leggono, scrivono e partecipano come preferiscono a questo spazio sono grandi e sentiti, e ancora una volta grazie per essere qui. :-)