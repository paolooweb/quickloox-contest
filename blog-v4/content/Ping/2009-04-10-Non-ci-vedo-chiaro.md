---
title: "Non ci vedo chiaro"
date: 2009-04-10
draft: false
tags: ["ping"]
---

Leggo su Macworld.com <a href="http://www.macworld.com/article/139908/2009/04/colormanagement.html?lsrc=rss_main" target="_blank">un'intervista</a> al Vice President per le licenze di Pantone, Andy Hatkoff.

Il tema è <em>l'importanza della gestione del colore</em>. Pantone è un'autorità assoluta nel campo e il mestiere di Hatkoff è fare s&#236; che hardware e software di chiunque possano facilmente integrare nei propri flussi di lavoro il sistema di colore Pantone. L'autore dell'intervista, James Dempsey, manda avanti un blog intitolato <a href="http://thegraphicmac.com/" target="_blank">The Graphic Mac</a>. Insomma, conoscono la materia.

I due parlano degli strumenti per la calibrazione del colore sui monitor, dell'illuminazione (usare lampade standard D60 o D65, che simulano la luce media del giorno), del problema dell'output sulle stampanti, dei software di serie di Adobe e Apple, di questo e di quell'altro.

Al termine dell'intervista Dempsey rimanda a un paio di altre pagine interessanti in tema.

Ho letto l'intervista e anche i link di rimando. Da nessuna parte c'è il minimo accenno alla questione degli schermi opachi o lucidi.

Non sono competente e dunque potrei essermi perso qualcosa. È materia centrale nella gestione del colore oppure no? Perché si preoccupano dell'illuminazione e della carta di stampa ma non dello schermo?