---
title: "Firma in forma"
date: 2006-09-17
draft: false
tags: ["ping"]
---

Amo avere signature casuali alla fine di ogni messaggio di posta. <a href="http://www.barebones.com/products/mailsmith" target="_blank">Mailsmith</a> mette a disposizione un sistema rapido di avere signature casuali, attraverso un file apposito.

Solo che il sistema funziona a livello di programma e non a livello di account. Gestisco numerosi account e vorrei che ogni account avesse il suo file di signature casuali separato.

Sto iniziando a lavorarci sopra e ho trovato forse qualcosa via Perl. Terrò informati. :)