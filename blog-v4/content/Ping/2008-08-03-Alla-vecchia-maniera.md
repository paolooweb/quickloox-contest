---
title: "Alla vecchia maniera"
date: 2008-08-03
draft: false
tags: ["ping"]
---

Strepitoso resoconto di <strong>Riccardo</strong>, che <a href="http://systemfolder.wordpress.com/2008/07/28/almost-road-warrior/" target="_blank">ha rimesso in funzionamento un PowerBook 5300</a>. Perseguitato da una cattiva e ingiusta fama (le famose batterie che avrebbero preso fuoco, mentre non è mai successo), dopo tredici anni, non solo funziona ma ci si può anche svolgere lavoro vero.