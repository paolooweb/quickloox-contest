---
title: "Si fa ma non si dice"
date: 2007-01-10
draft: false
tags: ["ping"]
---

Guarda bene <a href="http://www.apple.com/iphone" target="_blank">iPhone</a>. Compulsa le specifiche. Scruta le funzioni a disposizione.

Nel 1997 si sarebbe chiamato Personal Digital Assistant e sarebbe stato un Newton. Solo che oggi non si può più dire&#8230; il riciclo del marketing non è ammesso.

E poi dicono che Apple non è dieci anni avanti.