---
title: "Umani e interfacce"
date: 2007-11-09
draft: false
tags: ["ping"]
---

Quando attacchi al computer una tastiera diversa dalla solita, Windows ragiona come un computer, Mac OS X <a href="http://blogs.msdn.com/michkap/archive/2007/11/05/5892994.aspx" target="_blank">come un essere umano</a> (o almeno ci mette buona volontà che a Windows è sconosciuta).

Per un aggeggio che viene usato da esseri umani, come un computer, facile capire quale sia la scelta migliore.