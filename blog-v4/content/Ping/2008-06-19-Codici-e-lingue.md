---
title: "Codici e lingue"
date: 2008-06-19
draft: false
tags: ["ping"]
---

Sta emergendo chiaramente che il Morse è un patrimonio nazionale. Lo sanno tutti!

Qualcuno magari se la sente di localizzare in italiano il <a href="http://chch.cc/widgets/morse.htm" target="_blank">widget per Mac OS X</a>. Disponibile nella lingua di Atat&#252;rk e non in quella di Marconi il che, francamente, grida vendetta al cielo.

Per chiunque altro, una ricerca su Google di <em>morse code</em> oppure <em>codice morse</em> mostra abbastanza risorse da passarci l'estate.

.. / -.-. --- -.. .. -.-. .. / -- .. / .. -. - .-. .. --. .- -. --- .-.-.-