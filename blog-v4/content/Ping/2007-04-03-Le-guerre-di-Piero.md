---
title: "Le guerre di Piero"
date: 2007-04-03
draft: false
tags: ["ping"]
---

<strong>Piero</strong> è stato lasciato sui due piedi da sua moglie, anni fa. Si è rifatto una vita, ha trovato una nuova compagna ed è padre di un bambino meraviglioso.

Piero è gravemente ipovedente ed è destinato a peggiorare con il tempo. Lavora, gioca, viaggia, affronta la vita in positivo e con filosofia.

Piero si è stufato di Windows. Ha fatto lo <em>switch</em> e da stasera è proprietario di un MacBook Pro nuovo fiammante.

Ho avuto l'onore e il piacere di portarglielo. Era un po' che non installavo un Mac. Rispetto al mio G4, un Core 2 Duo di adesso ha una velocità veramente pazzesca.

Piero è entusiasta e ha voluto capire subito tutto il capibile, per superare alla massima velocità le differenze di uso tra i sistemi e diventare padrone della nuova macchina.

Sono stato fino alle tre di notte a rispondere alle sue domande, ingrandire il cursore, caricare NeoOffice italiano, fargli creare l'account aggiuntivo per la sua compagna, accompagnarlo tra le Preferenze di Sistema, installare AirPort Extreme (installare, parola grossa: l'ho attaccata allo Hag di FastWeb e ha funzionato subito da sola) e mille altre cose.

Sceso in strada per fare ritorno a casa, ho trovato una multa sulla macchina, per il lavaggio strade. Ho guardato l'importo e ho sorriso: per una serata cos&#236;, a fare l'alleato di Piero, avrei speso volentieri tranquillamente il doppio.