---
title: "Scrivere costa poco<p>"
date: 2005-11-20
draft: false
tags: ["ping"]
---

Controllare invece è carissimo, tanto che qualcosa sfugge<p>

Ringrazio tutti delle risposte a seguito del mio Preferenze pubblicato sul Macworld ora in edicola. Nel quale, fa giustamente notare Mario, c&rsquo;è una imprecisione: ho citato <a href="http://www.abisource.com/">AbiWord</a> come word processor a pagamento, quando invece si tratta di open source. Volendo si può ordinare su Cd anziché scaricarlo e il Cd, naturalmente, è a pagamento. Di suo, però, è gratuito, open source (si può prendere il codice e cambiarlo a piacimento, a patto di rendere pubblici i cambiamenti) e ancora più efficace come alternativa a Word.<p>

Bisogna però mancare di amore per i macrovirus e le società condannate in tribunale per concorrenza sleale. Non sarà semplice.<p>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>