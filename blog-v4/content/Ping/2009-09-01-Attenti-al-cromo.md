---
title: "Attenti al cromo"
date: 2009-09-01
draft: false
tags: ["ping"]
---

L'indirizzo cui scaricare le versioni in progress di Chrome per Mac <a href="http://build.chromium.org/buildbot/snapshots/chromium-rel-mac/" target="_blank">è cambiato</a>.