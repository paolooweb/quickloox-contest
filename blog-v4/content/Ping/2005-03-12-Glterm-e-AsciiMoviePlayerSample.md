---
title: "Caratteri forti<p>"
date: 2005-03-12
draft: false
tags: ["ping"]
---

L&rsquo;interfaccia grafica difende il solco, ma a tracciarlo è il testo<p>

Di fatto è sempre stato così. Ma da quando Mac OS X è Unix, la cosa è divenuta evidente. Basta scavare un attimo.<p>

Per il processore, l&rsquo;equivalente dell&rsquo;atomo è il bit. Per l&rsquo;essere umano, l&rsquo;equivalente dell&rsquo;atomo, nel computer, è il carattere. Chi è padrone dell&rsquo;Ascii (o di Unicode, tra poco) è padrone dell&rsquo;informatica. E tutte le meraviglie grafiche che facilitano la vita si reggono su file di testo: descrizioni, preferenze e altro.<p>

Ogni tanto le fondamenta e i tetti si incontrano in modi imprevedibili. Per esempio, provando <a href="http://developer.apple.com/samplecode/ASCIIMoviePlayerSample/ASCIIMoviePlayerSample.html">AsciiMoviePlayerSample</a>, un programmino dimostrativo scaricabile dal sito Apple per sviluppatori, si può osservare come si possa usare il Terminale perfino per visualizzare un&hellip; film. In caratteri Ascii.<p>

Chi trova il suo Terminale un po&rsquo; lento, riprovi con GlTerm. Un Terminale che disegna i caratteri con <a href="http://www.pollet.net/GLterm/">OpenGl</a> e lo fa a velocità smodata, come avrebbero detto nel film Balle spaziali di Mel Brooks.<p>

La grafica arriva dappertutto. Ma il vero padrone è il carattere.<p>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>