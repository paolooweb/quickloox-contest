---
title: "Entomologi e insettoni neri"
date: 2007-06-14
draft: false
tags: ["ping"]
---

Mi scrive ancora <strong>Antonio</strong> a proposito del Sinclair Ql e dei suoi appassionati. La scarsa attinenza al tema Mac è abbondantemente compensata dalla cronaca investigativa e dai misteri che neanche Google forse riesce a chiarire del tutto, oltre che dal fatto che il tema va sollevato fino a quando non salta fuori qualche buona soluzione di emulazione di Ql per Mac:

<cite>Ho visto ieri la persona della quale ti accennavo come tra i maggiori esperti Ql. Si chiama Eros Forenzi.</cite>

<cite>Si è gentilmente preso la briga di andare a cercare informazioni riguardo all’emulatore.</cite>

<cite>Mi ha detto che l'ultimo aggiornamento fatto su <a href="http://www.terdina.net/ql/q-emulator.html" target="_blank">Q-Emulator</a> da Danile Terdina risale al 2005.</cite>

<cite>Forenzi mi ha spiegato che funziona fino all’avvio ma, al momento di attivare un programma, si blocca andando in <em>loop</em> ma mantenendo lampeggiante il cursore.</cite>

<cite>Sembra che il problema risieda nella non completa equivalenza delle istruzioni tra il processore PowerPC, rispetto al quale Terdina aveva fatto ancora qualcosa, e i G4.</cite>

<cite>Poiché Terdina ha aggiornato fino ad un certo punto l’emulatore poi non ne ha più avuto il tempo ecco spiegato perchè per ora ancora non va e comunque non girerebbe sotto MacIntel.</cite>

<cite>Ma Forenzi non si è fermato a questo.</cite>

<cite>Se ricordi ti avevo linkato anche un sito sull'emulatore QL per Linux. <a href="http://linux-q40.sourceforge.net/uqlx/" target="_blank">Uqlx</a>.</cite>

<cite>Da buon appassionato, Eros ha approfondito anche questa strada ed ha scoperto che un Qlista greco ne ha fatto un porting per OS X senza però completarlo.</cite>

<cite>I link che portano al suo server, che Eros ha trovato ancora in giro per la rete, però non permettono di entrare poiché sottoposti a password. L’ipotesi di Forenzi al riguardo è che gli interessi del greco siano cambiati dal momento in cui aveva messo i link e che ora quella parte del server la usi per cose sue personali tale per cui ora quella parte è divenuta riservata, probabilmente dimenticandosi dei link ancora attivi.</cite>

<cite>Da quello che accennavi col Ql ci hai lavorato un po’ anche tu e come te anche altri hanno iniziato la loro storia informatica con l’“insettone nero”. È un po’ come un virus. Anche se smetti di usarlo gli anticorpi restano attivi… :-)</cite>

Il <a href="http://en.wikipedia.org/wiki/Sinclair_QL" target="_blank">Ql</a> è stata la mia prima tastiera professionale. Un computer fantastico. Se qualcuno ha ancora i Microdrive originali funzionanti, mi scriva. :)