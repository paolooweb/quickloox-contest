---
title: "Siti perditempo"
date: 2009-05-18
draft: false
tags: ["ping"]
---

Scelgo apposta una notizia che interessa poco nella vita quotidiana: al governo canadese non piace che le udienze parlamentari, riprese in video, finiscano su Internet e così ha inviato lettere di diffida chiedendo l’eliminazione dei video stessi.

Se <a href="http://yro.slashdot.org/article.pl?sid=09/05/17/2151216&amp;from=rss" target="_blank">la leggi su Slashdot</a>, si tratta delle udienze del Parlamento e i video vanno su YouTube.

Se <a href="http://www.michaelgeist.ca/content/view/3953/135/" target="_blank">la leggi sul blog di Michael Geist</a>, scopri che sono le udienze delle commissioni parlamentari, non dell’aula, e che il sito non è YouTube ma quello dei <a href="http://www.friends.ca/" target="_blank">Friends of Canadian Broadcasting</a>, gruppo che si propone di usare i <em>media</em> per conservare l’idetità e la cultura canadesi.

Proprio perché del Canada ci frega assai poco è facile vedere che la notizia è la stessa, ma i fatti sono davvero diversi.

È il momento di chiedersi come veniamo informati sui fatti che ci interessano davvero.

Regole generali per informarsi su Internet: meno siti consulti, più leggi cose sbagliate (o false). Più un sito è specializzato, più è informato (anche se non necessariamente sincero). Più <em>link</em> (esterni) ti danno dentro una notizia, più la notizia è affidabile. Più passaggi fa una notizia prima di arrivarti, più ha perso valore.

I siti che si propongono di dare tutte le notizie, che allargano il campo di azione invece che restringerlo, che non hanno <em>link</em> nelle notizie e che compilano le notizie di quinta mano fanno solo perdere tempo.