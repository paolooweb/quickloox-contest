---
title: "Un anno come gli altri<p>"
date: 2005-12-31
draft: false
tags: ["ping"]
---

La ripetizione degli stessi gesti scaramantici dà sicurezza<p>

Ero un po&rsquo; preoccupato. Poi finalmente ho letto qualcuno predire che Apple presenterà al Macworld Expo un cellulare. Il mondo non sta per finire.<p>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>