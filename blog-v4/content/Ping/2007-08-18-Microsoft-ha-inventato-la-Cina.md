---
title: "Microsoft ha inventato la Cina"
date: 2007-08-18
draft: false
tags: ["ping"]
---

Prima della nuotata, abbandonati sugli asciugamani, si discute dei massimi sistemi:

<cite>&#8212; Apple ha cambiato il mondo. Ha messo nelle mani della gente strumenti che hanno cambiato le cose, dall'Apple II al mouse all'interfaccia grafica fino, più in piccolo, a Numbers e a iPhone.</cite>

<cite>&#8212; Anche Microsoft ha cambiato le cose. Ha messo in mano Windows a centinaia di milioni di persone.</cite>

<cite>&#8212; S&#236;, ma erano gli stessi che prima di Windows usavano i computer con il Dos. Avrebbero preso un computer anche se si fosse rimasti al Dos.</cite>

<cite>&#8212; Già, però anche Windows era nuovo rispetto al Dos.</cite>

<cite>&#8212; Certo, ma Windows ha copiato il Mac e non aveva niente di nuovo rispetto al Mac. È stata solo una pessima imitazione a prezzo stracciato.</cite>

<cite>&#8212; In pratica, la Cina sta copiando Microsoft, solo su tutto il resto delle merci.</cite>

<cite>&#8212; Qualcosa del genere.</cite>

<cite>&#8212; Vado a prendermi una pizzetta.</cite>

<cite>&#8212; Due, grazie.</cite>