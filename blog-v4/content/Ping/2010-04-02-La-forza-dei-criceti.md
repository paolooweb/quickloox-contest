---
title: "La forza dei criceti"
date: 2010-04-02
draft: false
tags: ["ping"]
---

John Breeden, direttore dei laboratori tecnici di Government Computer News, sedicente &#8220;autorità per i professionisti informatici nelle istituzioni&#8221;, <a href="http://gcn.com/Articles/2010/01/28/FAQ-on-iPad-IPS-display.aspx?Page=2" target="_blank">il 28 gennaio 2010:</a>

<cite>Mi occupo di portatili e tecnologia di batterie da dieci anni e so che cosa può offrire la tecnologia di oggi.</cite>

<cite>Non c'è modo in cui un computer da sette etti possa azionare uno schermo Ips per dieci ore come sostiene Steve Jobs. [&#8230;] Se lo usi davvero, la mia stima è inferiore alle tre ore. [&#8230;]</cite>

<cite>A meno che Apple abbia sviluppato per iPad qualche nuovo tipo di fonte di energia, tipo celle nucleari o criceti magici, non aspettarti che le dichiarazioni sulla batteria risultino vere. La candela che arde più luminosa è la prima a spegnersi.</cite>

David Pogue, New York Times, <a href="http://www.nytimes.com/2010/04/01/technology/personaltech/01pogue.html?ref=technology&amp;pagewanted=all" target="_blank">con iPad in mano</a>:

<cite>Parlando di video: Apple asserisce che iPad ha un'autonomia di dieci ore, ma noi sappiamo che non ci si può fidare del produttore. Eppure, nel mio test, iPad ha riprodotto filmati in continuazione per dalla 7:30 alle 19:53, più di dodici ore. Quattro volte superiore a un tipico portatile o a un lettore Dvd portatile.</cite>

Walt Mossberg, Wall Street Journal, <a href="http://ptech.allthingsd.com/20100331/apple-ipad-review/" target="_blank">con iPad in mano</a>:

<cite>iPad è durato undici ore e ventotto minuti, circa il quindici percento più di quanto sostiene Apple. Ho potuto guardare quattro film, quattro serie Tv e una presentazione aziendale di novanta minuti, prima che la batteria morisse a metà di un episodio di</cite> The Closer<cite>.</cite>

Engadget, <a href="http://www.engadget.com/2010/04/03/apple-ipad-review/" target="_blank">con iPad in mano</a>:

<cite>È quasi impossibile crederci, ma durante i nostri test iniziali, usando pesantamente iPad, scaricando e usando un sacco di nuove applicazioni, praticando giochi in computergrafica, guardando video ad alta definizione, il tutto continuando a ricevere posta in sottofondo, l’autonomia di iPad è stata di dieci ore e 43 minuti.</cite>

Saranno i criceti.