---
title: "Estate terminale"
date: 2010-07-25
draft: false
tags: ["ping"]
---

In agosto o ci si diverte o ci si rilassa o se ne approfitta per studiare un po’.

Oggi il menu prevede la terza possibilità, con un po’ di <a href="http://superuser.com/questions/52483/terminal-tips-and-tricks-for-mac-os-x" target="_blank">comandi non ovvi da dare al Terminale</a> di Mac OS X per ottenere gli effetti più svariati, compreso spaventare la moglie per riprendere possesso del MacBook Pro e giocare un’avventura testuale. Ma c’è davvero molto di più.