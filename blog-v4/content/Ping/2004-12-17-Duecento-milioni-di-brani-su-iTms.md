---
title: "Una nota su duecento milioni<p>"
date: 2004-12-17
draft: false
tags: ["ping"]
---

iTunes Music Store non vivrà in eterno, ma abbastanza<p>

Il 16 dicembre 2004 iTunes Music Store ha venduto la sua duecentomilionesima, a dire di Apple una tra quelle comprese nella raccolta <em>The Complete U2</em>.<p>

È completamente improbabile che il suo successo prseguirà con questi ritmi indefinitamente. Il panorama della musica digitale sta cambiando rapidamente e, per esempio, presto sarà possibile scaricare musica a pagamento direttamente da una emittente radio che trasmette un brano che ti piace.<p>

Con tutto questo, la mia nota sui duecento milioni di canzoni riguarda la concorrenza. Ragazzi: non c&rsquo;è partita. Quando iTunes finirà, non sarà certo a causa di qualcuno di voi nanetti.<p>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>