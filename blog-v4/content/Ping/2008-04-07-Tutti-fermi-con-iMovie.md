---
title: "Tutti fermi con iMovie"
date: 2008-04-07
draft: false
tags: ["ping"]
---

L'ultimo iMovie piace o non piace e sono gusti. <strong>Andrea</strong>, però, mi racconta che il primo evento <em>frozen</em> italiano, tenutosi a Venezia, è stato <a href="http://www.youtube.com/watch?v=IBHJtdI1tO8" target="_blank">documentato su YouTube</a> in tempo record proprio grazie a un montaggio-lampo realizzato proprio con iMovie.

Chiunque può giudicare se il risultato sia professionale o meno.

Anzi, parlando di professionalità, viene fuori che la sede regionale Rai del Veneto non ha a disposizione una macchina con QuickTime Pro e non riesce a leggere un disco con un filmato in formato Dvhd ad altissima qualità.

Forse i più professionisti di tutti sono quelli che ci mettono l'impegno, anche se hanno in mano &#8220;solo&#8221; iMovie.