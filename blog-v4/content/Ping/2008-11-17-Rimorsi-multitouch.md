---
title: "Rimorsi multitouch"
date: 2008-11-17
draft: false
tags: ["ping"]
---

La sera sul treno, dieci minuti scarsi a disposizione. Poco per fare qualcosa di serio, molto per stare senza fare qualcosa.

Ammetto che se ho iPod touch in tasca mi faccio una partita a <a href="http://itunes.apple.com/WebObjects/MZStore.woa/wa/viewSoftware?id=292538570&amp;mt=8" target="_blank">Lux Touch</a> (<em>link</em> ad App Store). Peraltro, stamattina ho dato una sfogliata alla categoria Istruzione di App Store. Con tutto quello che c'è dentro, ora mi vergogno un po'.

Che resti tra noi, però.