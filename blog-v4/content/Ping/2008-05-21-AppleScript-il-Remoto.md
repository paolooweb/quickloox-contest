---
title: "AppleScript il Remoto"
date: 2008-05-21
draft: false
tags: ["ping"]
---

Apple Remote Desktop consente di controllare un Mac che sta da qualche altra parte del mondo. Ci sono vari modi per farlo gratis e pagamento, come si vede su <a href="http://www.pure-mac.com/remote.html" target="_blank">Pure Mac</a>. Una ragione per farlo a pagamento con Apple Remote Desktop è che Ard è scriptabile.

Per esempio, è facile dare comandi Unix a una macchina remota via Ard. Invece è un po' complicato selezionare un Mac remoto e aprire una sessione <code>ssh</code> con esso (<code>ssh</code> trasporta comandi e dati nella sicurezza della cifratura e in modo spietatamente efficiente).

AppleScript semplifica la complicazione. Ecco come, Script Editor alla mano.

<code>set theSSHList to {}</code>
Definiamo una variabile che conterrà una lista, in AppleScript compresa tra parentesi graffe. Al momento la lista è vuota e verrà riempita tra breve.

<code>tell application "Remote Desktop"</code>
Come detto, il programma in gioco è Apple Remote Desktop.

<code>&#160;&#160;&#160;&#160;&#160;set theComputers to the selection</code>
Un'altra variabile, questa volta impostata in modo interessante a qualunque cosa venga selezionata. selection è una parola riservata di AppleScript.

<code>&#160;&#160;&#160;&#160;&#160;repeat with x in theComputers</code>
Fa partire un ciclo che si ripete per ciascun elemento contenuto nella variabile.

<code>&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;set the end of theSSHList to Internet address of x</code>
Per ciascun computer presente nella rete, il ciclo aggiunge alla lista un indirizzo Internet, quello del computer in questione.

<code>&#160;&#160;&#160;&#160;&#160;end repeat</code>
Fine del ciclo.

<code>end tell</code>
Fine della prima parte dello script. Ora abbiamo una lista che contiene tutti i computer della rete, ognuno con il proprio indirizzo Internet. Se la rete è basata su Dhcp, come accade sempre più spesso, gli indirizzi cambiano e dunque il contenuto della lista non va dato per scontato all'infinito. Periodicamente bisogna rieseguire la scansione degli indirizzi.

<code>tell application "Terminal"</code>
Seconda parte dello script. Stavolta si dialogo con il Terminale.

<code>&#160;&#160;&#160;&#160;&#160;repeat with x in theSSHList</code>
Altro ciclo, con gli elementi della prima lista, che sono gli indirizzi Internet di ciascun computer.

<code>&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;do script "ssh username@" &#38; (contents of x)</code>
Il comando <em>do script</em> fa lavorare il Terminale per conto di AppleScript. Quest'ultimo costruisce la sintassi giusta unendo la prima parte del comando <em>ssh</em> all'indirizzo di ciascun computer in rete, che era appunto la parte più difficile.

<code>&#160;&#160;&#160;&#160;&#160;end repeat</code>
Fine del ciclo.

<strong>end tell</strong>
FIne di questa seconda metà dello script.

Lo script completo è il seguente:

<code>set theSSHList to {}
tell application "Remote Desktop"
&#160;&#160;&#160;&#160;&#160;set theComputers to the selection
&#160;&#160;&#160;&#160;&#160;repeat with x in theComputers
&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;set the end of theSSHList to Internet address of x
&#160;&#160;&#160;&#160;&#160;end repeat
end tell
tell application "Terminal"
&#160;&#160;&#160;&#160;&#160;repeat with x in theSSHList
&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;do script "ssh username@" &#38; (contents of x)
&#160;&#160;&#160;&#160;&#160;end repeat
end tell</code>

Eccezionalmente, lo script non arriva da The Unofficial Apple Weblog bens&#236; da <a href="http://www.bynkii.com/archives/2008/05/oh_lord_here_we_go_again.html" target="_blank">Bynkii.com</a>. Ma non ci si formalizza, vero?