---
title: "Il vecchio e il mare"
date: 2008-08-18
draft: false
tags: ["ping"]
---

L'inesauribile <a href="http://www.accomazzi.it" target="_blank">Akko</a> ha scoperto che su App Store è arrivato Frotz e non ha esitato un secondo a scaricarlo sul proprio iPod touch.

Neanch'io ho esitato. Frotz legge i file in formato Z-code, formato inventato da Infocom per le proprie avventure testuali. Negli anni Ottanta Infocom portò il genere al massimo spendore con una collana di capolavori, tra i quali la serie Zork.

L'azienda non esiste più molti anni (fu acquistata da Activision), ma le avventure testuali restano inarrivabili da giocare e sono nati programmi, tra cui appunto Frotz, che leggono gli Z-code e permettono di riprovare le stesse sensazioni, anche se il file originale non gira più sui computer di oggi o se i dischetti sono andati perduti.

Frotz arriva su iPod touch con una dotazione ricchissima di avventure libere da diritti, però mancano quelle Infocom.

Relativamente a Zork, non è un problema. Infocom rende liberamente scaricabili <a href="http://www.infocom-if.org/downloads/downloads.html" target="_blank">gli originali</a> delle prime tre avventure.

Su un Mac che può fare funzionare applicazioni Classic, le versioni Mac delle avventure funzionano perfettamente. Per usarli su iPod touch bisogna scaricare invece la versione Dos, decomprimere il file .zip e, nella cartella Data, isolare il file Z-code, che ha estensione .dat.

Il file Z-code va portato all'interno di Frotz. Chi ha un iPod touch jailbreakato può anche usare una connessione Sftp (serve openSsh installato sul computer più piccolo e un qualunque strumento appropriato, dal Terminale a <a href="http://cyberduck.ch" target="_blank">Cyberduck</a>, sul computer più grosso).

La cartella giusta è Games all'interno di Frotz.app. Il quale sta dentro /User/Applications. Disgraziatamente le cartelle all'interno di questa non portano il nome del programma, ma della codifica App Store. Occorre aprirle con pazienza fino a trovare quella giusta.

Su un iPod touch non jaibreakato (o se si preferisce la strada più costosa e comoda) bisogna usare FileMagnet, Files o una delle altre applicazioni che permettono lo scambio di file tra computer. Akko sostiene che si può scaricare il file .dat direttamente da Frotz se lo si mette su una pagina web e se si aziona il browser interno di Frotz stesso. A me pare che Frotz cerchi solamente nel proprio portale web e non altrove, ma posso sbagliare e, se s&#236;, è sufficiente creare un link Html ai file su una pagina web, al limite anche solo attivando la condivisione web di Mac e creando una pagina in locale, e scaricare. Io non ce l'ho fatta.

Infocom ha pubblicato molte altre avventure oltre alla serie Zork, come la Guida galattica per autostoppisti (The Hitchhiker's Guide To The Galaxy), Bureaucracy, Cutthroats, A Mind Forever Voyaging, Leather Goddesses of Phobos, Sorceror, Plundered Hearts eccetera. I file sono protetti da copyright e non si trovano facilissimamente su Internet, ma in qualche modo saltano fuori (o volendo si può provare a <a href="http://underworld.fortunecity.com/track/946/" target="_blank">pagare</a>). Per intanto, if-archive contiene <a href="http://www.ifarchive.org/indexes/if-archiveXgamesXzcode.html" target="_blank">420 avventure libere</a> e pronte all'uso. Alcune sono di ottima qualità e c'è anche Cave Adventure, la prima nella storia.

Per approfondire (la storia di Z-code è quasi un'avventura in sé), consiglio la <a href="http://en.wikipedia.org/wiki/Z-machine" target="_blank">pagina di Wikipedia</a>, le ultime <a href="http://www.csd.uwo.ca/Infocom/" target="_blank">vestigia di Infocom</a> e quella del <a href="http://frotz.sourceforge.net/" target="_blank">progetto Frotz</a>. Ultima cosa: tutto ciò, a cominciare da Frotz per finire con il mare di vecchie avventure esistenti, funziona anche su Mac.