---
title: "Time Machine test"
date: 2009-03-15
draft: false
tags: ["ping"]
---

Se metti a punto un sistema semplicissimo di <em>backup</em>, che lavori da solo chiederti neanche un clic, che ti libera da ogni pensiero riguardo al <em>backup</em> e che ti fa ritrovare facilmente le cose, in modo da poter dedicare ogni tua energia a lavorare e divertirti e creare nuove cose da backuppare senza doverti preoccupare dell'amministrazione del computer&#8230;

&#8230;stai tranquillo che qualcuno inizierà a perdere quantità di tempo incredibili sul funzionamento del tuo sistema, per ottenere qualche risultato arbitrario e del tutto irrilevante rispetto allo scopo primario di avere i tuoi dati al sicuro dal rischio di perdita.

Time Machine non è un programma di <em>backup</em> ma un test sulla quantità di tempo libero.