---
title: "Icone di stile"
date: 2007-01-06
draft: false
tags: ["ping"]
---

Immagina di mandare gli auguri di buone feste a una software house. E che da quella software house ti rispondano

<cite>Lucio,</cite>

<cite>Thank you very much for your kind iCard, and please accept our best wishes for the season and new year in return. :-)</cite>

Ossia, non con la frasetta copiaincollata, ma con una email vera.

È la stessa software house che a Macworld Expo ti faceva l'upgrade gratuito delle t-shirt.

Per decidere che programma usare, le recensioni contano fino a un certo punto. Gente che vale farà certamente programmi che valgono.

Non lo dico, che software house è. Sia cercarsela da soli, sia scrivere alle software house per vedere che cosa rispondono, sono ottimi esercizi.