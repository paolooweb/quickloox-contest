---
title: "Leggere attentamente le avvertenze"
date: 2009-04-22
draft: false
tags: ["ping"]
---

Sempre grandi, quelli di Mdj hanno pubblicato il <a href="http://www.macjournals.com/news/legalcopytext" target="_blank">testo integrale </a>dell'alluvione di avvertenze che sommerge i personaggi dello <em>spot</em> Apple <a href="http://movies.apple.com/media/us/mac/getamac/2009/apple-mvp-legal_copy-us-20090419_480x272.mov" target="_blank">Legal Copy</a>.

Nello <em>spot</em> il personaggio-Pc spiega che Windows è facile, che tutto funziona, che non dà problemi, e intanto che lo fa appare un lungo elenco di avvertenze e modalità d'uso.

I creativi hanno giocato un po' sporco e ci sono pezzi di testo che si applicherebbero tranquillamente a qualunque computer. Altri invece no:

<cite>Attenzione: non è raccomandato tentare di eliminare elementi dal registro di sistema. Spesso è difficile determinare la corrispondenza tra elemento e applicazione e tentare di eliminare elementi dal registro può provocare la cancellazione di una riga di registro valida, causando errori e blocchi del software. Se un registro di sistema si danneggia a causa di un errore commesso durante la pulizia del registro stesso, seguire questa procedure: eseguire il backup dei dati; eseguire un backup del registro; acquistare, scaricare e installare il programma Registry Repair; chiudere tutti i programmi; scandire il registro; determinare gli elementi da riparare/cancellare/eliminare; selezionare ok e ripetere se necessario. I Pc facili da usare possono dare difficoltà se infettati da virus, spyware o malware. Stando ai rapporti del 2008, sono stati identificati 1,5 milioni di malware diversi e ogni giorno ne emergono 20 mila nuovi. Sebbene alcuni virus siano inevitabili, esistono misure preventive da adottare. Appena acquistato il Pc, configurare i parametri di sicurezza (inclusi elementi come firewall Internet, aggiornamento automatico, antivirus, antispyware e altre protezioni contro il malware, vari altri parametri di sicurezza su Internet e di controllo dell'account).</cite>

Il testo è pubblicitario e non ufficiale, creato per fare effetto visivo e non per essere letto. Curiosamente, <a href="http://www.glarysoft.com/rr.html#" target="_blank">Registry Repair</a> esiste davvero e si scarica gratis. Il sito dei produttori dice di essere anche in italiano; in realtà si limita a buttare la pagina nel traduttore automatico di Google e produce piacevolezze come:

<cite>Per un intero mese lavorato nulla - fino a quando mio fratello raccomandato Registry Repair. Esso ha fissato la stupido errore in circa 5 minuti, (e come un "bonus" ha portato la mia velocità di marcia backup.) --- E l'ultima versione di Firefox scaricato come un sogno.
</cite>

Il che getta luce illuminante sull'immaginario dei clienti tipici di Registry Repair.