---
title: "La prossima killer app"
date: 2004-03-15
draft: false
tags: ["ping"]
---

Nascerà su Mac OS X, si dice in giro. Ma forse non ce n’è bisogno

Secondo <link>The Weekly Read</link>http://www.weeklyread.com/here/2004/03/11/the_next_killer_app_will_be_developed_on_apples_os_x, la prossima killer application nascerà su Mac OS X, perché è naturale conseguenza dell’avere un eccellente sistema Unix con l’interfaccia grafica migliore del mondo, più semplice e veloce da aggiornare di un Windows elefantiaco e che non apparirà, in nuova edizione, prima del 2006.

È probabile che sia vero. Nel blog si parla invece anche di integrazione di Linux nel prossimo Mac OS X e francamente ci si crede assai poco.

<link>Lucio Bragagnolo</link>lux@mac.com