---
title: "Le foto della prozia"
date: 2007-01-26
draft: false
tags: ["ping"]
---

Grazie tantissimo a <a href="http://www.exmachina.it/" target="_blank">Marcello</a> per aver montato e acceso il Lisa perfettamente funzionante da lui acquistato in America.

Le <a href="http://web.mac.com/lux/iWeb/P-Files/Lisa%20e%20Marcello.html" target="_blank">foto</a> non sono un gran che per via dell'ambiente e della fretta, ma se penso che quando esisteva Lisa io trafficavo sullo Spectrum, beh, è stata di nuovo un'emozione.