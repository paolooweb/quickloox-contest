---
title: "Il Lamentoso"
date: 2003-07-24
draft: false
tags: ["ping"]
---

Profilo psicologico da due soldi e un problema

Dice che si sente “tagliato fuori dalla maggioranza dei programmi” perché usa un Mac.
Poi guardi sul suo disco rigido e vedi Word, Excel, PowerPoint, Explorer o Safari. Che te ne fai, della maggioranza dei programmi? Ne usi cinque (o venti, il discorso non cambia).

In realtà della maggioranza dei programmi non gliene frega niente. Tipicamente ha una banca con un sistema assurdo di accesso online che dà problemi con Safari. Lui si sente tagliato fuori dalla maggioranza dei programmi, ma usare due browser invece di uno gli sembra un affronto intollerabile. I dischi rigidi costano poco, ma il suo, di disco rigido, vale molto più di quello degli altri e figurati se si possono investire quattro o cinque megabyte al posto di lamentarsi.

Oppure ha comprato un portatile ed è riuscito a fargli un graffietto nei primi dodici minuti di utilizzo. Così si lamenta perché non glielo cambiano anche se è in garanzia.

Ma una garanzia contro i lamentosi da due soldi e un problema, dove la vendono?

<link>Lucio Bragagnolo</link>lux@mac.com
