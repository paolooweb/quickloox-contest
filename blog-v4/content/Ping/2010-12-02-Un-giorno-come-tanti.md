---
title: "Un giorno come tanti"
date: 2010-12-02
draft: false
tags: ["ping"]
---

Google <a href="http://news.mongabay.com/2010/1129-google_earth_trees.html" target="_blank">crea 80 milioni di alberi virtuali</a>. Non a caso, non tutti uguali.

Qualcuno si fa venire l'idea di un <a href="http://www.theglif.com/" target="_blank">sostegno per iPhone</a>, <a href="http://www.kickstarter.com" target="_blank">va su Internet a chiedere soldi</a> e, chi dieci dollari chi mille, si raccoglie più di venti volte la somma necessaria.

Nasce <a href="http://itunes.apple.com/it/app/project/id404942717?mt=8" target="_blank">una rivista che non verrà mai stampata</a>.

Uno, due, tre anni fa queste cose non potevano accadere.

Viviamo nell'epoca più straordinaria possibile.