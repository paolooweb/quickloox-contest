---
title: "In volo sul Moscone"
date: 2008-01-08
draft: false
tags: ["ping"]
---

Chi ci sta a fare la solita chiacchierata durante e post-<em>keynote</em> di Steve Jobs, per commentare al volo le notizie dal Moscone Center? Ritrovo nella stanza di iChat <code>keynote2008</code>. Io la apro poco prima delle 18 ora italiana, chi vuole farlo prima si trova&#8230; quando vuole.

Per entrare nella stanza servono iChat o client compatibile Aim. Su iChat, <code>Comando-Maiuscole-G</code> (prima di Leopard) oppure <code>Comando-R</code> (su Leopard) e digitare <code>keynote2008</code>.

A stasera!