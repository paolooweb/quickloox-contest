---
title: "Decennio nuovo"
date: 2010-01-20
draft: false
tags: ["ping"]
---

Complimenti a redazione e tecnici di Macworld per il nuovo sito!

Invito a tutti: registrarsi al sito. Permette di commentare non solo Ping, ma tutto il resto di Macworld.it, a piacere.

Respirare aria nuova è sempre buon segno. Prendiamolo come ulteriore auspicio di ottimo inizio di decennio. :)