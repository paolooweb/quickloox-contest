---
title: "Buona San(e)ità&#8230; in meglio"
date: 2010-01-20
draft: false
tags: ["ping"]
---

Nuova versione Mac OS X di <a href="http://www.ellert.se/twain-sane/" target="_blank">Sane</a>, il software libero che permette il collegamento di scanner vecchi, non supportati, male supportati, con driver antichi e quant'altro.

A me Sane <a href="http://www.macworld.it/blogs/ping/?p=2268" target="_self">ha risuscitato</a> un Canon N1220U del secolo scorso, prima, e dopo lo ha fatto partire a 64 bit in Snow Leopard. Non posso che raccomandare l'installazione.