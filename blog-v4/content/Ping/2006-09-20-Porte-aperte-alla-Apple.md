---
title: "Porte aperte alla Apple"
date: 2006-09-20
draft: false
tags: ["ping"]
---

Come i quiz per la patente, dovrebbe essere obbligatoria la conoscenza delle note tecniche fondamentali di Apple per &#8220;guidare&#8221; un Mac.

<a href="http://docs.info.apple.com/article.html?artnum=106439" target="_blank">Questa pagina</a>, per esempio, elenca tutte le porte più usate dal sistema ed è di un prezioso pazzesco per risolvere un sacco di stupide questioni di reti, firewall, sicurezza e quant'altro.