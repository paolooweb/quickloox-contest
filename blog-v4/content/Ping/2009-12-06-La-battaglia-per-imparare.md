---
title: "La battaglia per imparare"
date: 2009-12-06
draft: false
tags: ["ping"]
---

Come si diceva con <b>Carmelo</b>, è uscito <a href="http://itunes.apple.com/it/app/battle-for-wesnoth/id340691963?mt=8" target="_blank">Battle for Wesnoth versione iPhone</a> e iPod touch.

Diversamente dalla <a href="http://www.wesnoth.org/" target="_blank">versione per <i>desktop</i></a>, consigliatissima a chiunque, costa 3,99 euro. Cioè è ultraconsigliatissima; oltre a portare su iPhone il miglior gioco di strategia a turni che si possa desiderare, è possibile sostenere in maniera concreta e immediata lo sviluppo del software <i>open source</i> che merita.

Per essere ancora più chiari, c'è anche il modo di averlo gratis.

Essendo <i>open source</i>, il codice sorgente è liberamente scaricabile e compilabile. Con un po' di impegno e qualche domanda giusta nei <i>forum</i> è possibile renderlo funzionante su iPhone senza spendere un centesimo.

Soldi contro conoscenza è sempre un compromesso valido.