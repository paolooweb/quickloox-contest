---
title: "Per qualche browser in più"
date: 2008-07-08
draft: false
tags: ["ping"]
---

Ci sono varie ragioni a spiegarlo. Il punto è che i <em>browser</em> per me non sono mai abbastanza.

E adesso ne ho scoperto uno eccezionale e inaspettato: <a href="http://www.vienna-rss.org/vienna2.php" target="_blank">Vienna</a>.

S&#236;, il <em>newsreader</em>. Ha una struttura efficientissima, completamente controllabile da tastiera, e per caricare al volo venti pagine da leggere con calma nel prossimo quarto d'ora è spettacolarmente perfetto.

Oltretutto l'animazione dei <em>tab</em> è molto ben fatta, il che non è pertinente all'usabilità, ma il <em>design</em> ci sta tutto. :-)