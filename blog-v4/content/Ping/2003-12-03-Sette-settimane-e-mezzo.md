---
title: "Sette settimane e 1/2"
date: 2003-12-03
draft: false
tags: ["ping"]
---

Nessun sistema operativo sarà mai esente da problemi. L’importante è ripararli

Secondo <link>Mdj</link>http://www.macjournals.com, dalla presentazione di Mac OS X 10.0 nel marzo 2001 fino a oggi, Apple ha pubblicato in media un aggiornamento di sistema ogni 7,5 settimane.

Molti si scandalizzano per i problemi che può presentare un sistema operativo anche nella sua release “finale” e, tanto per stare nell’attualità, è da poco che è uscito Mac OS X 10.3.1.

Il fatto è che, per via di leggi matematiche non aggirabili, non è possibile produrre software assolutamente perfetto. Più il software aumenta in dimensioni, più aumenta il numero degli errori.

Bisognerebbe stupirsi, al limite, del fatto che gli errori sono così pochi! il fatto che ci siano aggiornamenti di sistema non vuol dire che Apple lavora male. Vuol dire piuttosto che è composta da esseri umani, che lavorano indefessamente anche dopo che il sistema è uscito.

<link>Lucio Bragagnolo</link>lux@mac.com