---
title: "C'è chi può e chi può darsi"
date: 2010-09-01
draft: false
tags: ["ping"]
---

Ci si è già <a href="http://www.macworld.it/ping/soft/2010/08/21/flasha-bene-chi-flasha-ultimo/" target="_blank">fatti due risate</a> su chi criticava Apple cattiva che levava la libertà di godersi contenuti Flash sugli apparecchi iOS.

Adesso <a href="http://newteevee.com/2010/08/31/video-flash-on-android-is-startlingly-bad/" target="_blank">ha effettuato un test anche il sito GigaOm</a>, dal titolo <i>Flash su Android sciocca per quanto funziona male</i>. Riporto i contenuti essenziali alla comprensione.

<cite>Il video dimostrativo di Flash su Nexus One Android ripreso da Kevin Tofel, nostro esperto mobile, è stato girato con lo</cite> smartphone <cite>collegato alla sua rete Wi-Fi di casa, alimentata da una connessione FiOs Verizon a venticinque megabit per secondo, per cui la connettività non dovrebbe essere un problema. Nexus One monta inoltre un processore Snapdragon Qualcomm da un gigahertz, per cui caricare video sull'apparecchio non dovrebbe ugualmente rappresentare un problema. Fatte queste premesse, come ha funzionato il video Flash su Nexus One?</cite>

<cite>Male al punto di scioccare.</cite>

<cite>Nel tentare di caricare video da Abc.com, Fox.com e <a href="http://www.metacafe.com/" target="_blank">Metacafe</a>, Kevin ha trovato che i video si caricavano lentamente, quando si caricavano.</cite>

<cite>Cercando di caricare un episodio di</cite> Wipeout <cite>dal sito mobile della rete Abc, ha ricevuto un messaggio di errore</cite> riprova più tardi<cite>. Un tentativo di visionare il nuovo show Abc</cite> Rookie Blue <cite>è stato solo leggermente più positivo, nel senso che si è caricato uno</cite> spot <cite>pubblicitario Toyota prima che il</cite> player <cite>si bloccasse. Non è stato possibile vedere la puntata.</cite>

<cite>Kevin è riuscito a vedere un episodio di</cite> Bones <cite>su Fox.com, ma il video andava a scatti nonostante la connessione e il processore. In effetti sembrava più uno</cite> slideshow <cite>che un video. Non solo: il sonoro era fuori sincrono con il video. Se non altro Fox avvisa che il video non è ottimizzato per il</cite> mobile.

<cite>Il primo tentativo di video in</cite> streaming <cite>da Metacafe &#8211; uno spezzone dalla recente notte dei premi musicali Emmy &#8211; non ha funzionato perché il video arrivava da <a href="http://www.hulu.com/" target="_blank">Hulu</a>, noto per bloccare la visione agli apparecchi mobile. Dopo avere tentato di guardare un video in alta definizione &#8211; non ottimizzato per il</cite> mobile <cite>e ancora una volta più somigliante a uno</cite> slideshow <cite>che a video &#8211; Kevin è riuscito a ottenere uno</cite> stream <cite>semivisibile del</cite> trailer <cite>di</cite> Resident Evil<cite>.</cite>

Ed ecco la conclusione del sito.

<cite>Se in teoria il video Flash potrebbe essere un vantaggio competitivo per chi usa Android, nella pratica è difficile immaginare che qualcuno veramente provi a guardare video da web non ottimizzato su un apparecchio Android, il che porta a credere che può darsi Steve Jobs avesse ragione a scartare Flash preferendo Html5 su iPhone e iPad.</cite>

E sono due che danno ragione a Jobs. Delizioso il <cite>può darsi</cite>.