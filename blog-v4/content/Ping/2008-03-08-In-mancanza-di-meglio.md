---
title: "In mancanza di meglio"
date: 2008-03-08
draft: false
tags: ["ping"]
---

Dimenticavo: avevo pensato di non procurarmi iPhone, bens&#236; ricorrere al simulatore che sta dentro il <a href="http://developer.apple.com/iphone" target="_blank">Software Development Kit</a> appena reso disponibile da Apple.

Pensavo di verificare la completezza e la stabilità dell'Sdk, poi ho trovato una ragione decisiva per procurarmi l'iPhone vero: l'Sdk è solo per Mac con processori Intel.

Il che si giustifica solo se è una situazione temporanea, legata alla fretta di arrivare all'annuncio con l'essenziale. Il processore di iPhone è potente, ma non certo quanto un G4 o un G5. Ho lasciato ad Apple un <em>feedback</em> un po' piccato.