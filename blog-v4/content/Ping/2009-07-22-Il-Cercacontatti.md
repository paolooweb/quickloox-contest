---
title: "Il cercacontatti"
date: 2009-07-22
draft: false
tags: ["ping"]
---

Mentre Apple latita, traduco una al giorno le sue <a href="http://www.apple.com/it/macosx/refinements/enhancements-refinements.html" target="_blank">cento novità di Snow Leopard</a>.

<b>Ricerca di un contatto in Spotlight</b>

Si possono cercare i contatti in Spotlight, con un apposito comando di menu dentro il menu Contatti di iChat.

La scusa <i>ti ho cercato ma non c'eri</i> non vale più. In iChat.