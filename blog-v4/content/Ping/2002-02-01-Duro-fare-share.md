---
title: "Com’è duro fare share quando si vale molto"
date: 2002-02-01
draft: false
tags: ["ping"]
---

Come fa una trasmissione televisiva di culto a conquistare il mercato? E se fosse un costruttore di computer?

Pensa a una trasmissione televisiva di culto. Molto curata, con un seguito fedele e numeroso di pubblico istruito e sopra la media, in onda in zone del palinsesto che non sono certo il sabato sera.
Anche questa trasmissione, come qualsiasi altra, dipende dallo share; nonostante il suo valore a prescindere, perché continui a esistere deve esserci pubblico che la guarda.
Gli autori della trasmissione sanno che devono inventare sempre qualcosa che riesca ad allargare il pubblico, mantenendo però il livello della trasmisione ben sopra la media, per non deludere il loro seguito. Si può fare moltissima audience con nudo e premi miliardari, ma una trasmissione del genere perderebbe subito tutto il suo valore.
La prossima volta che hai la soluzione pronta su “che cosa dovrebbe fare Apple”, pensa alla trasmissione.

lux@mac.com