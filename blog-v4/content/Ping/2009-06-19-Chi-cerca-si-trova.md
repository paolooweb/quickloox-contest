---
title: "Chi cerca si trova"
date: 2009-06-19
draft: false
tags: ["ping"]
---

Sempre per via del fatto che Apple ha <a href="http://www.apple.com/it/macosx/refinements/enhancements-refinements.html" target="_blank">una pagina destinata alle novità di Snow Leopard spiegate in italiano</a>, ma non la traduce, supplisco con una novità al giorno.

<b>Cambio posizioni di ricerca</b>

È possibile cambiare il comportamento predefinito di Spotlight per ordinargli di cercare all'interno della cartella selezionata o nella posizione più recente di ricerca.

Praticamente, invece che puntare l'indice contro Spotlight come accade a volte, potremo puntare l'indicizzazione.