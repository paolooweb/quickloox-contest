---
title: "Duecento sono niente<p>"
date: 2005-04-25
draft: false
tags: ["ping"]
---

Le novità e modifiche di Tiger sono talmente tante che non si farà in tempo a enumerarle tutte<p>

Il libro che troverai nel prossimo Macworld si limita alle cose più importanti, perché è di cento pagine. Ce ne volevano quattrocento (e tre mesi di tempo in più, naturalmente) perché dovunque si alza un sassolino di Tiger emerge un formicaio di novità.<p>

Esempio di cosa che non era essenziale dire ma che si potrebbe lo stesso: su Panther, vista come colonne, clicchi su un filmato molto grosso di cui QuickTime non ha un codec e il Finder lavora come un forsennato per cercare di mostrare un&rsquo;anteprima che in effetti non può dare.<p>

Con Tiger, invece, il Finder conosce i propri limiti e non perde tempo. Né fa perderne a noi.<p>

Più affronto la Tigre, più scopro che è intelligente. E si lascia domare.<p>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>