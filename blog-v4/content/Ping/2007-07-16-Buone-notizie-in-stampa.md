---
title: "Buone notizie in stampa"
date: 2007-07-16
draft: false
tags: ["ping"]
---

Tipicamente Microsoft compra tecnologie per ucciderle o confinarle. O usarle per sé.

Altri fanno diversamente.

Per esempio, <a href="http://www.cups.org/articles.php?L475" target="_blank">Apple ha acquistato il sistema di stampa Cups</a> e assunto il suo autore, ma il software continuerà a essere sviluppato e rilasciato con la licenza <em>open source</em> originaria. Cos&#236; Apple avrà tecnologia di stampa sempre migliore in Mac OS X e chiunque, usi Mac o meno, potrà goderne.

Quando vincono tutti, sono più contento. Quando vince Microsoft, perdiamo tutti.