---
title: "Piccoli grandi regali"
date: 2003-11-25
draft: false
tags: ["ping"]
---

Un abbonamento a .mac contiene più di quello che sembra

Sono abbonato, soddisfattissimo, a <link>.mac</link>http://www.mac.com per ragioni più importanti del fatto di avere in regalo iBlog, che di suo è shareware. Eppure oggi ho provato ad aprirlo, per curiosità, e mi sono trovato davanti un’applicazione semplice, eppure completa ed elegante.

Ammetto di non averne ancora provata la funzionalità, ma sono abituato ad aprire per curiosità un mucchio di programmi, dare loro un’occhiata e buttarli via poco dopo. Su iBlog mi sono fermato e l’ho messo nella cartella Applicazioni, quella da cui si tolgono file solo quando si fanno le grandi pulizie. Ha un che di amichevole.

<link>Lucio Bragagnolo</link>lux@mac.com