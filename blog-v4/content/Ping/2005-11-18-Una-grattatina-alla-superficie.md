---
title: "Una grattatina alla superficie<p>"
date: 2005-11-18
draft: false
tags: ["ping"]
---

E una martellata a qualche luogo comune<p>

Compare sempre lo sprovveduto della settimana a ricordarmi che su Mac ci sono pochi programmi.<p>

L&rsquo;ho invitato a dare un&rsquo;occhiata alla pagina <a href="http://www.opensourcemac.org/">Open Source Mac</a>. Dove, in effetti, ci sono non pochi, ma pochissimi programmi. Tutti rigorosamente open source, quindi gratuiti, e nonostante questo di qualità buona se non ottima.<p>

Si tratta di una microscopica frazione di tutto il software che è disponibile per Mac. Eppure, dimmi se alla fine di questa breve visita (sono veramente due paginette) non ti senti un po&rsquo; come se bene o male ti fossi assicurato il minimo indispensabile per sopravvivere, per fare tutto senza spendere un euro.<p>

Lo sprovveduto ha bofonchiato qualcosa e ha detto che per Mac non ci sono giochi. Per distrarmi intanto che parlava, ho dato una scorsa a <a href="http://www.insidemacgames.com/">Inside Mac Games</a>.<p>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>