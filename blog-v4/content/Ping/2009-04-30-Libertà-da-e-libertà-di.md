---
title: "Libertà da e libertà di"
date: 2009-04-30
draft: false
tags: ["ping"]
---

Solo quando c'è banda in abbondanza per tutti può venire a qualcuno l'idea balzana di scrivere <a href="http://macfreedom.com/" target="_blank">un'applicazione che ti spegne Internet per un tempo prefissato</a>, <em>cos&#236; non ti distrai</em>.

Se hai bisogno di un programma per concentrarti sul lavoro, hai bisogno di ben altro. La vera libertà è quella <em>di</em> scegliere, non quella <em>dalla</em> scelta.