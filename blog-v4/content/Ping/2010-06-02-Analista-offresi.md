---
title: "Analista offresi"
date: 2010-06-02
draft: false
tags: ["ping"]
---

Senza sapere come sarebbe stato iPad, l'analista Gene Munster di Piper Jaffray <a href="http://www.appleinsider.com/articles/09/12/28/apple_seen_selling_1_4_million_600_kindle_killer_tablets_in_2010.html" target="_blank">ha stimato a dicembre</a> che le vendite nel 2010 sarebbero state di 1,4 milioni di pezzi.

Dopo che iPad è stato annunciato, Munster <a href="http://www.tuaw.com/2010/01/27/first-ipad-sales-estimates-four-million-this-year-double-that/" target="_blank">ha alzato la cifra</a> a tre-quattro milioni, ammettendo implicitamente di essersi sbagliato di oltre il cento percento.

Adesso che iPad ha venduto due milioni di pezzi in meno di due mesi e sono partite le vendite internazionali, Munster ha deciso che i pezzi venduti nel 2010 <a href="http://www.appleinsider.com/articles/10/06/01/apple_expected_to_sell_6_2m_ipads_in_2010_after_strong_overseas_debut.html" target="_blank">saranno 6,2 milioni</a>. Più di quattro volte la propria stima originaria.

E solo perché Apple non riesce a fabbricarne di più: secondo Munster, tre quarti degli Apple Store americani avevano esaurito la scorta di iPad al 21 maggio.

Cifre alla mano, Apple vende circa 34 mila iPad al giorno.

C'è nessuno che vuole coprirmi d'oro per farsi inviare quattro volte al mese previsioni assurde basate sul niente pneumatico, che però cambio tutte le volte che mi viene voglia?