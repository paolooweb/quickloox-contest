---
title: "Dove tutto &egrave; cominciato"
date: 2006-06-12
draft: false
tags: ["ping"]
---

Sempre in tema di avventure testuali, ho scoperto con anni di ritardo un ottimo eseguibile di <a href="http://www.lobotomo.com/products/Adventure/" target="_blank">Adventure</a>, la prima avventura testuale in assoluto, pronta per essere lanciata in Mac OS X con un semplice doppio clic.

Di fronte ai giochi di oggi fa sorridere. Eppure ha un suo perverso fascino. :-)