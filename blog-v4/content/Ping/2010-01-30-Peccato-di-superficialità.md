---
title: "Peccato di superficialità"
date: 2010-01-30
draft: false
tags: ["ping"]
---

Paolo Attivissimo ha dedicato attenzione a iPad e lo ha <a href="http://attivissimo.blogspot.com/2010/01/aiped-aipad-aipod.html" target="_blank">stroncato</a>.

Ho <a href="http://attivissimo.blogspot.com/2010/01/aiped-aipad-aipod.html#c1121688604847449795" target="_blank">commentato</a> che le sue argomentazioni fossero, sostanzialmente, una stroncatura di iPhone nel 2007.

Paolo <a href="http://attivissimo.blogspot.com/2010/01/aiped-aipad-aipod.html#c4430648526246144924" target="_blank">mi ha risposto</a>, in parte, ma a questo punto desidero entrare più nello specifico.

Premetto che per me Paolo è un modello di giornalista, che stimo molto. Ha creato il <a href="http://antibufala.info/" target="_blank">Servizio Antibufala</a>, coordina e scrive su <a href="http://undicisettembre.info/" target="_blank">Undicisettembre</a> e su <a href="http://complottilunari.info/" target="_blank">Complotti Lunari</a>; basterebbe questo a renderlo imperdibile e lui fa molto di più. Chi non lo segue ci perde, anche quando il suo punto di vista è criticabile.

Ciò detto, stavolta si è accontentato di poco e la cosa stona ancor più con la sua bravura, perché neanche si è documentato, né ha provato il prodotto di cui parla. Alcune delle sue affermazioni sono opinioni personali, e chi le tocca. Altre sono inesatte o sono supposizioni senza riscontro, o sono posizioni prese in malafede preventiva. Questo non me lo aspetto, da una persona di valore come lui.

Due esempi.

<cite>Retro bombato che rischia di farlo dondolare quando lo appoggi su un tavolo per scrivere.</cite> Ho chiesto espressamente e di persona ad Antonio Dini, giornalista di Nova, se sia cos&#236;. Lui è stato a San Francisco e <a href="http://www.ilsole24ore.com/art/SoleOnLine4/Tecnologia%20e%20Business/2010/01/ipad-caratteristiche.shtml?uuid=fa1807c4-0cac-11df-a8e3-e5834f89f27d&amp;DocRulesView=Libero" target="_blank">ha provato di persona</a> iPad. Smentisce categoricamente. Perché presupporre un rischio che non c'è senza prima essersi accertati che ci possa essere?

<cite>Comunque troppo grande da portare sempre con sé.</cite> Sempre Antonio ha spiegato che iPad stava nella tasca del suo giaccone. Il <i>comunque</i> è fuori luogo. Il <i>sempre</i> è enigmatico: perché mai una tavoletta da dieci pollici dovrebbe essere sempre con noi, come fosse un portafogli? La critica è fondata su un presupposto inesistente, come se io criticassi il mio 17&#8221; perché non lo posso sempre portare con me. Quando mi serve lo porto eccome, quando non mi serve lo lascio a casa. Il perché iPad dovrebbe servire sempre (anche in spiaggia o a sciare, dove nove su dieci portano il cellulare?) non si capisce.

Ho altre obiezioni da muovere e lo farò nei prossimi giorni. Uno è liberissimo di giudicare negativa la mancanza del <i>multitasking</i>, per esempio; lo fa anche Antonio nella sua prova. Però parlare di rischi senza avere verificato o di difetti basati su un presupposto inesistente, davvero, non è corretto e mi dispiace un po' vedere questa superficialità. Scrive Antonio: <cite>si capisce che su una cosa Steve Jobs ha avuto ragione: &#171;Non giudicatelo &#8211; spiega &#8211; prima di averlo provato&#187;.</cite> Paolo titola <cite>iPad *yawn*</cite> (sbadiglio) e neanche lo ha toccato.

Se Paolo mi legge: con stima immutata.