---
title: "Spazi e tempi"
date: 2006-08-07
draft: false
tags: ["ping"]
---

Userò i desktop virtuali per cose interessanti, tipo il lavoro su una scrivania virtuale e il gioco di ruolo su un'altra. Ovviamente erano cose che si potevano avere prima, ma con Leopard, che Steve Jobs sta presentando alla Wwdc, le si avranno in modo semplice, immediato, senza dover scaricare roba esterna e in un colpo solo.

Per chi volesse aggiungersi a una chattata in compagnia, Comando-Maiuscole-G e digitare <code>wwdc2006lux</code> da dentro iChat. :-)