---
title: "300+ novità: networking"
date: 2007-11-05
draft: false
tags: ["ping"]
---

Leopard ha ristrutturato il <em>menulet</em> AirPort, quello grafico in alto a destra che mostra le tacche di connessione.

Basta aprirlo per vedere che le cose non sono più come prima. Per cominciare, le reti protette sono identificate da un lucchetto. Poi il menu è stato razionalizzato e reso più funzionale.

Guardando nel menu Apple potrà sembrare che sia sparito il menu Postazioni. In realtà compare solo quando le postazioni sono almeno due; se c'è una postazione sola non c'è bisogno del menu e cos&#236; questo non appare.

Sotto il cofano, Leopard regola automaticamente la dimensione del buffer Tcp. Questo dovrebbe aumentare la resa della connessione, specialmente in condizioni di banda ampia o alta latenza del segnale.

Quanto sopra, da <a href="http://www.apple.com/it/macosx/features/300.html#networking" target="_blank">pagina Apple</a>.