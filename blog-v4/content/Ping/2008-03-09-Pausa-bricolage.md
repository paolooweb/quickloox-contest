---
title: "Pausa bricolage"
date: 2008-03-09
draft: false
tags: ["ping"]
---

Scrivevo <a href="http://www.macworld.it/blogs/ping/?p=1131">il 3 gennaio 2007</a> di <a href="http://www.raindesigninc.com/ilap.html" target="_blank">iLap</a>, sostegno metallico per tenere il portatile sulle gambe o rialzarlo sulla scrivania.

Quattordici mesi dopo, ho avuto il primo inconveniente da (notevole) usura: si è scollata all'estremità destra la striscia di velcro attaccata alla base di iLap, sul quale si attacca l'imbottitura cilindrica anteriore che funge da appoggio morbido sulle cosce e da area di riposo per i polsi.

Niente di che. Ho applicato il collante, ho tenuto striscia e base pressati per un paio d'ore e adesso tutto funziona come e meglio di prima.

iLap continua a restare una buona raccomandazione per chi abbisogna di un appoggio e non ha voglia di farselo in casa.