---
title: "Meglio non troppo tardi che mai"
date: 2009-02-04
draft: false
tags: ["ping"]
---

Apple mi informa che la consegna dei MacBook Pro 17” <em>unibody</em> sta richiedendo <cite>un qualche po’ più del previsto</cite> e che la precedente forchetta tra 16 e 20 febbraio è diventata <cite>prime spedizioni a partire dal 19 febbraio</cite>.

Auspico vivamente che le consegne avvengano in ordine alfabetico. :-)