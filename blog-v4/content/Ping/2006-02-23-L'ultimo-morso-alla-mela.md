---
title: "L’ultimo morso alla mela"
date: 2006-02-23
draft: false
tags: ["ping"]
---

La <em>tournée</em> della delegazione dell’All About Apple Club in visita ad Apple, San Francisco, Silicon Valley e dintorni si conclude con la terza puntata delle loro avventure.

Ci sono il museo storico del computer, la San Francisco di giorno e di notte, tanta sensibilità e un tocco di <em>humor</em>.

<a href="http://www.allaboutapple.com/speciali/s_francisco_2006_c.htm" target="_blank">Basta un clic</a>.