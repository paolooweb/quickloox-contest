---
title: "Dal grande potere di AppleScript derivano grandi responsabilità"
date: 2008-06-26
draft: false
tags: ["ping"]
---

Oggi niente script e invece una considerazione sull'ultima sedicente minaccia per la sicurezza su Mac, che ha goduto di uno spazio quasi ingiustificato sui soliti siti Mac-Diva-e-Donna (massimo rispetto per Diva e Donna).

Il problema è questo:

<code>osascript -e 'tell app "ARDAgent" to do shell script "whoami"'</code>

<code>osascript</code> è un comando di Terminale. Che permette di eseguire un AppleScript. AppleScript, di suo, può eseguire comandi di Terminale, con <code>do shell script</code>.

Perché allora eseguire un comando di Terminale che aziona un AppleScript che aziona un comando di Terminale, invece che dare direttamente il secondo comando di Terminale?

Perché una imperfezione nei meccanismi di sicurezza di Apple Remote Desktop fa s&#236; che alla fine di questo balletto l'utente che ha impartito il comando si ritrova con i privilegi di <em>root</em>, anche se è partito dal niente. <em>root</em> significa potere di vita e di morte su tutto il sistema.

Lo spazio dedicato dai siti faciloni è ingiustificato perché il comando deve essere dato da qualcuno che si trova fisicamente collegato al sistema Unix del Mac; collegarsi da una macchina remota non funziona. Se hai accesso fisico al Mac infili un disco di sistema, azzeri la password amministratore e poi fai quello che ti pare, senza scomodare AppleScript o il Terminale.

È ancora più ingiustificato in quanto è sufficiente attivare il servizio di Apple Remote Desktop nella Condivisione delle Preferenze di Sistema per chiudere il buco (s&#236;, attivare, lo so che è controintuitivo).

Un pizzico di giustificazione invece sta nel problema, irrisolvibile, dell'utente come anello debole del sistema di sicurezza. Se alla tastiera è seduto un imbecille disposto ad aprire senza pensarci qualsiasi cosa gli arrivi, inserire una password di amministratore senza pensarci, lanciare software di origine dubbia in arrivo da fonte dubbia, il sistema può essere il più sicuro del mondo, ma non c'è verso. Alla fine i computer sono insicuri per i maneggioni, i faccendieri, quelli che guai a fare una cosa nel modo giusto se c'è un modo di escogitare un accrocchio paralegale che ti fa sentire più furbo. Bene cos&#236;. Perché con AppleScript è semplicissimo realizzare cosette del genere. Cito l'amico MisterAkko:

<cite>Apri la cartella AppleScript dentro la cartella Applicazioni e lancia Script Editor. Digli <em>Registra</em>. Butta nel cestino la tua cartella Documenti e vuota il cestino. Registra il programma che Script Editor ti ha creato.</cite>

<cite>Bravo! Hai creato il tuo primo programma &#8220;troiano&#8221;. Adesso salvalo con il nome </cite>Programma che cancella i tuoi documenti<cite> e spediscilo agli amici. Poi fammi sapere quanti amici imbecilli hai che lo hanno davvero eseguito.</cite>

<cite>Oppure incollaci sopra l'icona di Spore, chiamalo</cite> Spore 1.0 beta 3<cite> e mandalo ai medesimi amici.</cite>