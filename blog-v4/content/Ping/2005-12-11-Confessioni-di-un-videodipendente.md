---
title: "Confessioni di un videodipendente<p>"
date: 2005-12-11
draft: false
tags: ["ping"]
---

O meglio, di quello che ogni tanto vorrebbe concedersi una dose<p>

A casa mia non esiste più un televisore. Anche l&rsquo;ultimo, un banale 14 pollici, è morto e non verrà sostituito. In generale è molto più interessante fare cose con un computer che non stare a guardare passivamente un televisore. Secondariamente, con pochissime eccezioni, oramai le cose interessanti da guardare si devono pagare.<p>

Confesso che una manciata di volte ho fatto partire un Dvd sul mio 17&rdquo;: Il Signore degli Anelli, un concerto di Poter Gabriel, poco altro. Se è qualcosa di veramente speciale sono ancora disposto a guardare, per un po&rsquo;.<p>

Questi sono i motivi per cui mi sento particolarmente indignato con Apple, o con la Nbc, non so. In America si possono comprare da iTunes le trasmissioni dello Sci Fi Channel e in Italia ancora no. Non ho ancora vinto la lotteria per il permesso di residenza in USA e quindi sono senza Sci Fi Channel. Apple, parliamone.<p>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>