---
title: "Sensibilità al tocco"
date: 2010-02-06
draft: false
tags: ["ping"]
---

<p>Uno dei motivi per cui iPhone ha dato scandalo nel 2007 è la tastiera virtuale.</p>

<p>Oggi i costruttori fanno a gara a imitarla.</p>

<p>Sugli apparecchi che non hanno le dimensioni per ospitare una tastiera vera, quella virtuale è più efficiente. Il motivo è semplice: cambia in funzione dell'applicazione e perciò è assai più efficace.</p>

<p>La prova arriva da <a href="http://www.wolframalpha.com/" target="_blank">Wolfram|Alpha</a>, il <i>motore di ricerca di conoscenza computazionale</i> (il nome non deve spaventare; può essere utile a tutti, in certe situazioni).</p>

<p>I matematici hanno spesso bisogno di simbologia molto complicata. Ecco, l'<a href="http://itunes.apple.com/it/app/wolframalpha/id334989259?mt=8" target="_blank">applicazione Wolfram|Alpha per iPhone e iPod touch</a> dispone di <a href="http://www.betanews.com/article/WolframAlpha-makes-a-strong-argument-for-virtual-keyboards/1265410708?utm_source=feedburner&amp;utm_medium=feed&amp;utm_campaign=Feed%3A+bn+%28Betanews+Full+Content+Feed+-+BN%29" target="_blank">quattro tastiere differenti</a>, per simboli alfanumerici, lettere greche, funzioni matematiche e simboli vari.</p>

<p>Serve un simbolo &#8704;? Su iPod touch tocchi lo schermo e hai fatto. Prova, ora, con il <i>netbook</i>.</p>