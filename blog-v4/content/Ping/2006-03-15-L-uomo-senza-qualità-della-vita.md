---
title: "L&rsquo;uomo senza qualit&agrave; (della vita)"
date: 2006-03-15
draft: false
tags: ["ping"]
---

Adesso che sono usciti gli iMac, il MacBook Pro e il Mac mini della nuova era, una cosa &egrave; chiara: Apple deve ancora sostituire un processore G4 con un processore Intel senza togliere qualche dotazione, alzare i prezzi, o ambedue le cose.

Giravano un sacco di anime belle convinte che il passaggio a Intel avrebbe fatto diminuire i prezzi, non si sa perch&eacute; n&eacute; come, ma convinte lo erano di sicuro.

Auspico abbiano capito che i cassoni da un tanto al chilo cos&igrave; agognati per risparmiare (sulla qualit&agrave; della vita) non costano due stracci perch&egrave; il processore &egrave; Intel, ma perch&eacute; sono male progettati e male ingegnerizzati, oltre ad avere un sistema operativo malfatto.