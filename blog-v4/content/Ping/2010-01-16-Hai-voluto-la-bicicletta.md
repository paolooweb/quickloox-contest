---
title: "Hai voluto la bicicletta?"
date: 2010-01-16
draft: false
tags: ["ping"]
---

Ho tenuto un corso base di Mac OS X, incontrando molti ragazzi che Mac lo avevano già visto, e molti ancora che invece conoscevano unicamente Windows.

Ho insistito molto, in tempo e in enfasi, sulla cultura del backup, forse la lacuna più grave nell'istruzione informatica di massa.

Time Machine, ancora più dell'essere una soluzione geniale e immediata, è una grande idea perché mette il problema subito sotto gli occhi di ciascuno. Fa parte delle icone preinstallate dentro il Dock di Mac OS X. E oltre a mostrare il problema offre anche la soluzione; un mentore perfetto.

Manca solo il disco rigido esterno e in effetti qualcuno ha mostrato perplessità di fronte alla necessità di una spesa suppletiva per l'uso del computer.

Certo, ho risposto: con tutto quello che ho speso per comprare casa, stai a vedere che mi tocca pure pagare la bolletta della luce. Figurati se per usare bene la lavatrice, con quello che è costata, devo anche comprare il detersivo a parte.

Scrivo di ritorno dall'ipermercato. Un disco rigido da un terabyte costa 89,90 euro e non ci sono neanche le offerte di Natale.

Ogni apparecchio ha un costo di esercizio e se i tuoi dati valgono qualcosa, le scuse per non averne una copia di sicurezza sono veramente terminate. Autorizzo chiunque a trattare con cattiveria e sarcasmo quanti venissero sorpresi a lamentarsi della perdita dei dati. Tutta e unicamente colpa loro, che hanno un grande potere sotto le mani e pretendono che non significhi grande responsabilità.

Hai voluto la bicicletta? Forse è prudente avere il casco.

(A margine: chiavette da quattro gigabyte a 4,90 euro. A Milano, meno di una pizza margherita, l’equivalente di tre colazioni a caffè e cornetto.)