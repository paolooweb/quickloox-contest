---
title: "Tagliato corto"
date: 2009-08-10
draft: false
tags: ["ping"]
---

Non sono mai stato <i>fan</i> dei servizi di accorciamento degli Url. Hanno senso sulla carta stampata perché non si possono cliccare e allora si copiano meglio se sono corti. Hanno senso in un Sms, se non si possono cliccare.

Quando si usano dove è possibile cliccare e copiare e incollare non hanno più senso. Nelle pagine web, nelle email (si mette l'indirizzo tra minore e maggiore, &#60;<a href="http://metamark.net" target="_blank">http://metamark.net</a>&#62;, e qualunque buon programma di posta lo rende cliccabile anche se si spezza su più righe.

Se Twitter ha messaggi lunghi come un Sms la soluzione non è accorciare gli Url con l'orrendo bit.ly ma scrivere meno stupidaggini prima dell'indirizzo. Non sopporto di non sapere che cosa sto esattamente cliccando, anche perché è un rischio di sicurezza considerevole. E il 99 percento delle volte il twitterista non spiega esattamente che sito è e che cosa dice, ma fa battute, la prende alla larga, cita una parte irrilevante della pagina, tutto tranne che presentare efficacemente il link. Ho il vizio di cliccare e aprire tutto, ma Twitter riesce a farmelo passare.

Detto ciò, succede anche che un servizio di accorciamento Url <a href="http://blog.tr.im/post/159369789/tr-im-r-i-p" target="_blank">chiuda per sempre</a>. Tutti gli Url prodigiosamente accorciati con Tr.im smettono di funzionare.

Usa gli Url come sono. Se proprio non ne puoi fare a meno, meglio forse Metamark, che funziona come <i>open source</i> da oltre dieci anni e probabilmente troverà persone disposte a farlo funzionare ancora a lungo.