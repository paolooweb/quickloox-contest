---
title: "Campagna contro l'abbandono di Aggiornamento Software"
date: 2009-05-13
draft: false
tags: ["ping"]
---

<em>Forum</em> e <em>mailing list</em> sono intasati da gente che ha provveduto manualmente a scaricare <a href="http://support.apple.com/downloads/Mac_OS_X_10_5_7_Update" target="_blank">Mac OS X 10.5.7</a> (e magari anche il resto, <a href="http://support.apple.com/downloads/Safari_3_2_3_for_Leopard" target="_blank">Safari 3</a>, Safari 4 e Security Update per Tiger, <a href="http://support.apple.com/downloads/Security_Update_2009_002__Tiger_Intel_" target="_blank">Intel</a> e <a href="http://support.apple.com/downloads/Security_Update_2009_002__Tiger_PPC_" target="_blank">PowerPc</a> più le versioni Server, quelle <em>combo</em> e <a href="http://support.apple.com/downloads/" target="_blank">altro ancora</a>).

Spero che Apple non si accorga di questa frenesia e non tolga Aggiornamento Software. Facendo tutto da solo e automaticamente, Aggiornamento Software libera il mio tempo per fare cose più creative e produttive e divertenti che occuparsi di un aggiornamento di sistema.

Perché rubare al computer il suo mestiere?

Non abbandonare il tuo Aggiornamento Software. Ti ringrazierà.