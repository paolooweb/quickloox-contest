---
title: "Safari storico con AppleScript"
date: 2008-06-30
draft: false
tags: ["ping"]
---

Una cosa che Firefox può fare automaticamente e Safari no è chiudersi ed eliminare automaticamente la cronologia.

Su Mac OS X però c'è AppleScript.

<code>tell application "Safari" to quit</code>
Chiude Safari.

<code>set libraryPath to path to library folder from user domain as text</code>
Imposta una variabile che contiene quasi tutto il percorso dei file da cancellare, cui poi si aggiungerà la parte finale. Questa procedura consente di mettere a punto una procedura che funziona su qualsiasi Mac, senza sapere nome dell'utente o dell'hard disk.

<code>set historyFile to libraryPath &#38; "Safari:History.plist"</code>
Aggiunge l'ultima parte del percorso e cancella il file della cronologia di Safari.

<code>set cookiesFile to libraryPath &#38; "Cookies:Cookies.plist"</code>
Fa la stessa cosa con il file dei cookie, lo si volesse.

<code>tell application "Finder"
	try
		delete historyFile
	end try
	try
		delete cookiesFile
	end try
	empty trash
	beep
end tell</code>
Cestina i file da cancellare e vuota il Cestino.

Questo lo script completo:

<code>tell application "Safari" to quit
set libraryPath to path to library folder from user domain as text
set historyFile to libraryPath &#38; "Safari:History.plist"
set cookiesFile to libraryPath &#38; "Cookies:Cookies.plist"
tell application "Finder"
	try
		delete historyFile
	end try
	try
		delete cookiesFile
	end try
	empty trash
	beep
end tell</code>

Invece che fare Comando-Q per chiudere Safari si farebbe doppio clic sullo script, registrato come applicazione.

L'originale di questo script proviene dai <a href="http://forums.macosxhints.com/showthread.php?t=83986" target="_blank">forum di Macosxhints</a>.

C'è uno script molto più complesso ed elegante di questo su <a href="http://www.oreillynet.com/mac/blog/2005/08/homebrew_safari_updated.html" target="_blank">MacDevCenter</a>.

Una versione minimalista del tutto sta su <a href="http://bbs.macscripter.net/viewtopic.php?id=11307" target="_blank">MacScripter</a>.

Uno script ancora su <a href="http://www.macosxhints.com/article.php?story=20051107093733174&amp;lsrc=osxh" target="_blank">Macosxhints</a> fa qualcosa di più e usa comandi Unix per sbrigarsela alla svelta.

Quando c'è di mezzo AppleScript, le soluzioni a un problema si moltiplicano. :)