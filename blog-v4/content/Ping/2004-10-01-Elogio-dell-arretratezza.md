---
title: "Elogio dell&rsquo;arretratezza<p>"
date: 2004-10-01
draft: false
tags: ["ping"]
---

Nel mondo Mac la gallina vecchia continua a fare buon brodo<p>

Lo confesso. Il mio Mac attuale l&rsquo;ho comprato usato, vecchio di un anno, per quanto in buone condizioni.<p>

Da quando lo uso ha lavorato (troppo) per altri due anni, è caduto due volte, ma fa tutto quello che mi serve senza perdere un colpo. Tre giorni fa ho riavviato per via dell&rsquo;ultimo update di Java e da allora non lo spengo; più o meno starà acceso senza riavviarsi mai per altre tre o quattro settimane, aggiornamenti esclusi.<p>

In questo momento ho aperti trenta programmi, sono su Internet via wireless e perfino in poltrona, perché un portatile consente questi e altri lussi.<p>

Ogni tanto mi chiedono perché non cambio computer, che è così vecchio.<p>

Fa esattamente tutto quello che mi serve, e lo cambierò (all&rsquo;istante) solo quando smetterà di farlo.<p>

Giusto per dire che un Titanium 500 è tuttora una macchina usabile e professionale. Tutti quelli che conosco e hanno comprato, per così dire, altre marche, sono già stati forzati ad aggiornarsi da un bel po&rsquo;&hellip;<p>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>