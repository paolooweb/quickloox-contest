---
title: "Uno schermo e due pollici"
date: 2008-04-27
draft: false
tags: ["ping"]
---

Mi sono ritrovato mio malgrado in una chat dove chiacchieravo attraverso iPhone.

I risultati del test sono che la tastiera virtuale di iPhone è ovviamente meno efficiente di una tastiera vera di un computer vero, ma regge la prova. Si va un po' più lenti con buona precisione.

Devo anche rimangiarmi l'opinione che scrivere con i due pollici a iPhone tenuto in mano fosse meno impreciso. Non è vero.

Dopo dieci minuti non se ne può più e si desidera una tastiera vera. Ma quei dieci minuti sono la dimostrazione che il concetto è giusto e l'implementazione è buona. Avessi avuto un T9 su una tastiera numerica, ora sarei in terapia.