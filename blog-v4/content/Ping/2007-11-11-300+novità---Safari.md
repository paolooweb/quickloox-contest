---
title: "300+ novità: Safari"
date: 2007-11-11
draft: false
tags: ["ping"]
---

Qualcosa di Safari per Leopard si è già visto nella beta uscita anche per Tiger. Non proprio tutto.

C'è un Cerca anni luce migliore del precedente, che individua graficamente l'oggetto della ricerca nella pagina. La Cronologia è ricercabile e Safari indicizza il testo delle pagine visitate.

I <em>tab</em> (a chiamarli pannelli mi viene il prurito, al più sarebbero linguette) sono spostabili con il mouse da una posizione all'altra. Talmente naturale che, adesso, appare ovvio. Il tab trascinato può anche finire a costituire una finestra propria. Un gruppo di tab può diventare un unico bookmark. Estremamente comodo per le ricerche.

Tutte le finestre aperte si possono unire in un'unica finestra a <em>tab</em>.

Soprattutto, si possono riaprire le finestre aperte nella sessione precedente. Prima, al momento di riavviare, mi veniva il patema di tutto quanto dovevo ancora leggere. Adesso esco tranquillo e sereno.

I campi testo di un form si possono ridimensionare, per renderli più comodi. Continuo a pensare che siano i web designer stupidi a fare campi piccoli, più che i programmatori di Safari intelligenti a renderli ridimensionabili. Comunque.

Tornando a Safari, il browser ora contiene alcuni controlli di Anteprima. Vedere un Pdf via browser non più un'esperienza mortificante.

Il limite di conservazione degli elementi nella Cronologia ora è affidato alla decisione dell'utente e non più ad altro.

Con un singolo clic si può adibire a sfondo scrivania qualsiasi immagine trovata su web.

E veniamo avvisati prima di chiudere una finestra che contiene più <em>tab</em>, funzione di cui si riconosce il valore non appena è capitato di chiudere una finestra per sbaglio. A me è capitato.

Non so tu, io passo in Safari molto del mio tempo-Mac. Tra le <a href="http://www.apple.com/it/macosx/features/300.html#safari" target="_blank">novità di Leopard</a>, queste sono di rilievo e benvenute.