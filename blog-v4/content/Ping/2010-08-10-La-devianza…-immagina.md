---
title: "La devianza&#8230; immagina"
date: 2010-08-10
draft: false
tags: ["ping"]
---

Un altro programma di disegno realizzato in Html5 per il quale basta un <i>browser</i>, ma questo è speciale: si chiama Muro ed è stato concepito da <a href="http://www.deviantart.com/" target="_blank">DeviantArt</a>, probabilmente la comunità più vasta di artisti (veri e aspiranti) che si possa trovare in rete.

Ci sono una modalità base e una pro, entrambe gratuite; chi ha un account Deviant può farvi riferimento e, se si trova bene, può persino acquistare pennelli aggiuntivi. C'è compatibilità con le tavolette grafiche Wacom ed è tutto dire.

Questo non è il frutto di qualche programmatore fuori di testa che si rilassa spremendo Html5 per divertimento, ma gente che prima di tutto pensa alla grafica e solo dopo considera lo strumento. Segnale determinante.

Nota personale, rispetto agli altri programmi fin qui visti <a href="http://muro.deviantart.com/" target="_blank">Muro</a> è più amichevole e più affidabile.