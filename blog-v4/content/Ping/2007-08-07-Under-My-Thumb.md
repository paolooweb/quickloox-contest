---
title: "Under My Thumb*"
date: 2007-08-07
draft: false
tags: ["ping"]
---

In Mac@Work ho potuto annusarlo e poco più. A <a href="http://www.allaboutapple.com/speciali/hone.htm" target="_blank">H&#244;ne</a> l'ho pasticciato per molti minuti.

iPhone è eccezionale, ma il punto non è questo. Ho provato a tenerlo con una mano sola e digitare sulla tastiera virtuale con il pollice. Si fa benissimo, con percentuale di errore minima.

Il che contrasta con molti <em>report</em> che ho letto. Per quanto mi concerne, possono finire nel cestino.

<a href="http://www.lyricsfreak.com/r/rolling+stones/under+my+thumb_20117884.html" target="_blank">*</a>