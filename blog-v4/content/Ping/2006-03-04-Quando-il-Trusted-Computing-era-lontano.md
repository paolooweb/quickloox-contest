---
title: "Quando il Trusted Computing era lontano"
date: 2006-03-04
draft: false
tags: ["ping"]
---

Non tutto l&rsquo;archivio di Ping &egrave; stato travasato nel blog. In questi tempi di polemica sulle protezioni hardware e sul Drm mi sento di ripubblicare questo pezzettino scritto il 22 novembre 2002.

Non ho apportato la minima modifica. Tre anni e mezzo dopo, Apple ha cambiato fornitori di processore. Eppure, al massimo, aggiornerei la dizione <em>Palladium</em>, non pi&ugrave; usata.

Tutto il resto lo rifirmo tale e quale, compresa (specialmente) la conclusione.

<em>Io speriamo che passino a Mac</em>

<em>Un domani non lontano Macintosh potrebbe diventare un bastione di libert&agrave; personale</em>

<em>Se non hai ancora sentito parlare di Palladium, meglio.</em>
<em>Quando tutti sapranno di Palladium sar&agrave; pi&ugrave; vicino il giorno in cui i computer conterranno componenti hardware dotati di una chiave cifrata collegata al sistema operativo e accessibile solo al costruttore, o a Microsoft.</em>

<em>Per semplificare, domani un computer Palladium potrebbe dirti &ldquo;No, non puoi suonare questo Mp3 che ti sei fatto per uso personale da un Cd audio che hai regolarmente acquistato, perch&eacute; il file non &egrave; stato autorizzato. Puoi disattivare Palladium, ma al prezzo di non usare il lettore Cd&rdquo;.</em>

<em>Tuttavia restano speranze. La prima &egrave; che Palladium sia la goccia che fa traboccare il vaso e faccia capire finalmente alla gente che sta usando un prodotto inferiore; la seconda &egrave; che Apple non si &egrave; assolutamente pronunciata ma, al contrario di Ibm, Intel e altri costruttori che lo hanno fatto, non sembra incline a trattare i propri clienti in questo modo.</em>

<em>Domani Apple potrebbe produrre gli unici computer liberi esistenti al mondo. Forse &egrave; il caso di fare la cosa giusta da subito.</em>