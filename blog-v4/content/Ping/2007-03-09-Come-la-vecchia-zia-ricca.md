---
title: "Come la vecchia zia ricca"
date: 2007-03-09
draft: false
tags: ["ping"]
---

Per motivi vari ho spippolato dentro i dischi di backup. Recuperato e variamente consultato un Dvd di giugno 2004 (due anni e nove mesi da ora) e un altro Dvd di dicembre 2004 (due anni e tre mesi da ora). Un totale di cinque anni-disco senza problemi.

Continuo a pensare che il problema della mor&#236;a dei dischi ottici sia un problema di scarsa qualità dei dischi acquistata e cattiva conservazione dei dischi stessi, più che debolezza intrinseca dei <em>media</em>. Sono curioso di veder morire almeno un vecchio Cd, ma continuo ad aspettare e niente accade.