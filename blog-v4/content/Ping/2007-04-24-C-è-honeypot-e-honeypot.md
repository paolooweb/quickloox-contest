---
title: "C'è honeypot e honeypot"
date: 2007-04-24
draft: false
tags: ["ping"]
---

Una <em>honeypot</em>, in gergo, è un sistema messo l&#236; apposta per attirare gli <em>hacker</em> in cerca di bersagli.

Al raduno <em>hacker</em> CanSecWest di Vancouver hanno messo in palio due MacBook per chi fosse riuscito a violarli. Le regole erano semplici. I MacBook erano messi in rete wireless, con Mac OS X installato e aggiornato, e nient'altro. Il vincitore avrebbe dovuto riuscire a diventare utente legittimo su una macchina, sfruttando una vulnerabilità qualunque purché fosse inedita. In sostanza, come minimo bisognava registrarsi come utenti amministratori su una macchina non nostra passando da una connessione AirPort. Il premio: portarsi a casa il MacBook.

Non ci è riuscito nessuno. Ci hanno anche provato in pochi. La fatica di violare un MacBook non valeva la ricompensa.

Allora hanno aggiunto al premio anche diecimila dollari.

Non ci è riuscito nessuno.

Allora hanno cambiato le regole. Niente più penetrazione attraverso la AirPort; bastava solamente riuscire a mettere in crisi Safari con Url creati apposta per sfruttare eventuali vulnerabilità.

Uno dei due Mac è stato allora <a href="http://cansecwest.com/post/2007-04-20-14:54:00.First_Mac_Hacked_Cancel_Or_Allow" target="_blank">colpito con successo</a>.

La vicenda, tuttavia, è una sconfitta per gli hacker. Dalla connessione AirPort non è riuscito a entrare nessuno. È stata scoperta una vulnerabilità in più di Safari, ma niente che non si sia già visto in passato e che Apple non abbia regolarmente sistemato in breve tempo. Secondo, penetrare in un Mac e prenderne il controllo sarebbe stato molto diverso da quello che è accaduto, ossia chiedere gentilmente all'utente legittimo di voler scaricare un Url truffaldino.

E il secondo MacBook, per il quale da regolamento serve un attacco diverso da quello usato per il primo, è ancora l&#236;.

Sto scrivendo di domenica, quando i fabbricanti di notizie-spazzatura si comportano come se non ci fossero notizie. Domani, luned&#236;, sulla vicenda ne verranno scritte di tutti i colori. Occhio a queste <em>honeypot</em>, fatte non per gli <em>hacker</em> ma per gli sprovveduti.