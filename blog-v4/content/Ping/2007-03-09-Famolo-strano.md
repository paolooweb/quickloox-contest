---
title: "Famolo strano"
date: 2007-03-09
draft: false
tags: ["ping"]
---

Dopo l'aggiornamento a iTunes 7.1 e QuickTime 7.1.5 è successo qualcosa fuori dall'ordinario. Aggiornamento Software ha confermato il riavvio ma, misteriosamente, il sistema se ne è andato senza registrare alcuni documenti di BBEdit che erano aperti e non si è riavviato, ma si è spento.

Ho riacceso e il boot è avvenuto senza alcun problema. Ho rilanciato BBEdit, che di suo ha riaperto le finestre rimaste in sospeso! Non sapevo che lo facesse e non mi era mai capitato. Complimenti a <a href="http://www.barebones.com" target="_blank">Bare Bones</a>, ancora una volta.

Il riavvio, per quanto anomalo e bizzarro, era richiesto dall'aggiornamento software e in definitiva è andato a buon fine. Per il 2007, ancora nessun riavvio indesiderato. Però sono stato anche salvato tempo fa da un Security Update; lo stack Bluetooth si era ancora perso per strada e non sono riuscito a rianimarlo manualmente. Per riattivare il collegamento con il cellulare non sapevo più che fare se non riavviare&#8230; ma è arrivato il Security Update a salvare la giornata, per dirla all'americana.

Se mi aspetto una cosa non ovvia da Leopard, è una maggiore solidità di Bluetooth. O almeno una migliore gestione da parte dell'utente. Se qualcosa non va, con Bluetooth, non va proprio e basta.