---
title: "Noia e ripetitività"
date: 2001-12-13
draft: false
tags: ["ping"]
---

Un pezzo di questa rubrica che finirà per annoiare a morte

Scrivendo di Mac tutti i giorni ci saranno temi che si ripetono. Uno di questi è la presenza di difetti di funzionamento su Explorer. Usualmente problemi che colpiscono solo su Windows, ma a volte non si può dire. Tendenzialmente, problemi di sicurezza che novantanove volte su cento non devono preoccupare nessuno ma, la centesima volta, magari uno si ritrova l’hard disk distrutto, così, per il gusto di usare Explorer.
Racconta Punto Informatico che in questi giorni è stata scovata quella che “potrebbe trattarsi della più grave falla mai trovata in Explorer”.
Che noia. Basta aspettare una settimana, un mese, una nuova versione. Arriverà una nuova falla ancora più grave, o ancora più subdola. Per alcuni è progresso. Per me, che non uso Explorer, sono solo notizie da pianeti lontani.

http://www.punto-informatico.it/p.asp?i=38400