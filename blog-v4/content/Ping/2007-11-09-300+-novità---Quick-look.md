---
title: "300+ novità: Quick Look"
date: 2007-11-09
draft: false
tags: ["ping"]
---

Su Leopard, Finder, Mail e Time Machine utilizzano <a href="http://www.apple.com/it/macosx/features/300.html#quicklook" target="_blank">Quick Look</a> (Comando-Y o barra spazio per vedere istantaneamente dentro un documento senza dover aprire l'applicazione che lo ha creato).

Quick Look è una delle funzioni più reclamizzate eppure una delle più elementari. Detto che fa anche la visione a schermo e funziona anche con più documenti contemporaneamente, è detto tutto.

Contemporaneamente, è una delle più sottovalutate. All'inizio lo scettico sbuffa, alza le spalle e fa doppio clic. Poi ci prova e non torna più indietro. Ero scettico anch'io e invece non c'è partita.