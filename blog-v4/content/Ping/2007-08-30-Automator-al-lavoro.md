---
title: "Automator al lavoro"
date: 2007-08-30
draft: false
tags: ["ping"]
---

<strong>Dany</strong> lo ha usato per <a href="http://web.mac.com/brethil/Brethils_Page/Thoughts/Entries/2007/8/28_A_%E2%80%98lil_script.html" target="_blank">automatizzare il montaggio</a> del disco esterno su cui tiene la propria libreria di iTunes e lanciare pure iTunes appena il disco è montato.

Da scaricare e analizzare. Automator è forse il software più sottovalutato di Mac OS X.