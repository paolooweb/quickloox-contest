---
title: "In una parola"
date: 2008-01-09
draft: false
tags: ["ping"]
---

La cosa importante e mortalmente difficile in programmazione non è che le cose vadano bene. È che non possano andare male.

Un esempio semplice viene da John Gruber di Daring Fireball, al lavoro per creare un AppleScript capace di <a href="http://daringfireball.net/2003/09/select_word_script_for_bbedit" target="_blank">selezionare una singola parola in BBEdit</a>.

Compaiono quattro iterazioni diverse dello script. La prima funziona, ma è fragile. L'ultima è più lunga e complessa di quanto sembrava potesse essere. Ma praticamente niente può andare male.

Adesso ho uno script, associato a una scorciatoia di tastiera, per selezionare una singola parola. Nel lavorare senza mouse è terribilmente comodo. Con pochissimo sforzo credo che funzioni tranquillamente anche su <a href="http://www.barebones.com/products/textwrangler/" target="_blank">TextWrangler</a>, che è gratis. E ho il sospetto che possa essere facilmente adattato a vari altri programmi compatibili AppleScript.