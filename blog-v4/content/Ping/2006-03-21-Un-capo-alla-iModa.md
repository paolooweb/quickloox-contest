---
title: "Un capo alla iModa"
date: 2006-03-21
draft: false
tags: ["ping"]
---

Devo ringraziare molto <strong>Ale</strong> per avermi regalato un <a href="http://www.ivogue.it/" target="_blank">iVogue</a> che da ieri, assai gradito, veste e protegge l’iPod shuffle di casa. Il connubio tra la tecnologia e il tessuto morbido è davvero apprezzato!