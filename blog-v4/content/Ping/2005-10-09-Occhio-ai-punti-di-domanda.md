---
title: "Il virus di domanda<p>"
date: 2005-10-09
draft: false
tags: ["ping"]
---

Consiglio ai cercatori di notizie<p>

Ci sono siti che spacciano notizie e, con poche eccezioni, non fanno altro che ripetere roba trovata in America.<p>

Fino a qui niente di male, anzi. Le notizie Mac nascono nelle aziende e, fuori da queste, in pochissimi altri posti, tipo News.com, dove hanno loro inviati. Il resto è ripetizione e non c&rsquo;è niente da scandalizzarsi.<p>

Chi ripete ha tuttavia un dovere professionale nei confronti dei lettori: esercitare un giudizio critico su quello che sta per ripetere e, possibilmente, anche un controllo. Invece molti se la cavano ripetendo qualunque cosa, anche una evidente sciocchezza, e parandosi il posteriore con il punto di domanda (Steve Jobs compra casa su Marte?).<p>

Lo scopo è evidente: fai una notizia già così. Poi se è vero te ne vanti, se non è vero lo scrivi e, dal nulla hai fatto due notizie. Squallido, ma abituale.<p>

Morale: quando leggi una cosa con il punto di domanda, tendenzialmente vale poco. Più punti di domanda ci sono, meno vale. Quelli bravi non fanno le domande, danno le risposte. E non si lasciano contagiare dalle sparate che abbondano.<p>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>