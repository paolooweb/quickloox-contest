---
title: "A domanda non rispondo"
date: 2007-02-17
draft: false
tags: ["ping"]
---

Un buontempone sta provando da qualche giorno a entrare nel mio account .Mac, solo che non riesce a resettare la password. La domanda cui rispondere per farlo ha una risposta che conosco solo io e serve in modo eccellente a tenere fuori il buontempone.

Quano mi registro su un sito, e sbuffo davanti alla procedura di registrazione (non sui campi di registrazione, per quelli sbuffare è più che lecito), mi ricordo del buontempone e faccio del mio meglio per compilare decentemente la parte della sicurezza. Ogni tanto serve pure.