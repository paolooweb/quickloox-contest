---
title: "Disco frullante non si tocca"
date: 2002-03-13
draft: false
tags: ["ping"]
---

Ci sono note tecniche Apple per ogni tipo di comportamento, anche bizzarro

Una interessante <link>nota
tecnica</link>http://docs.info.apple.com/article.html?artnum=34566 Apple riguardante iMac (Flat Panel) consiglia vivamente di non spostare la macchina se dentro il lettore ottico c’è un disco. Non può succedere niente, ma in rari casi potrebbe verificarsi un danneggiamento del lettore.
Anche se iMac ha un eccezionale braccio metallico da usare come maniglia di trasporto non credo che verrà spostato così spesso; e comunque togliere un Cd o Dvd prima di spostare la macchina non costa così tanto sforzo. Spero.

<link>Lucio Bragagnolo</link>lux@mac.com