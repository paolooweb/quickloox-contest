---
title: "Sapore di Mac OS 9"
date: 2003-09-28
draft: false
tags: ["ping"]
---

Anche sulle nuove macchine c’è sotto qualcosa che non è solo X

Si sa che il 2003 è l’anno della transizione definitiva dell’hardware Mac verso Mac OS X e che i nuovi modelli, per quanto pienamente compatibili con Classic, non sono più in grado di partire con Mac OS 9.

Però qualcosa sotto c’è ancora: nei dischi consegnati insieme alle nuove macchine, se si parte con il tasto Opzione premuto per invitare il Mac a mostrare i sistemi disponibili, si scopre anche una partizione normalmente invisibile che permette di eseguire Apple Hardware Test, applicazione diagnostica che non è mai stata carbonizzata e quindi deve funzionare sotto Mac OS 9.

Da qui a dire che è possibile aggirare le impostazioni Apple e fare partire un nuovo Mac con Mac OS 9, comunque, la strada è infinita; probabilmente dentro quella partizione c’è effettivamente un file Mac OS Rom o qualcosa che gli somiglia molto. Ma pensare di trasformarsi in hacker per tentare di riuscire a non partire in Mac OS X è una solenne perdita di tempo. Molto meglio impiegarlo per vincere la pigrizia e le incrostazioni mentali, e scoprire la superiorità e i vantaggi di Mac OS X.

<link>Lucio Bragagnolo</link>lux@mac.com