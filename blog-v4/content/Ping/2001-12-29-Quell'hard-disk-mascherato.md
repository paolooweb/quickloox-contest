---
title: "Quell’hard disk mascherato"
date: 2001-12-29
draft: false
tags: ["ping"]
---

Perché spendere una cifra enorme per un player Mp3 quando c’è una fantastica e conveniente unità di backup...?

Posso sbagliarmi, ma ho la sensazione che iPod si stia vendendo benino. Chi si ricorda quando è uscito? “È un player Mp3 troppo caro”, era il commento più comune. Ma qualcuno si è lasciato sedurre dalle dimensioni, dallo stile, dall’ottima interfaccia, e ha scoperto che scaricare cinque giga di musica in quattro minuti e mezzo era molto, ma non tutto.
Per esempio si è trovato nella situazione di dover consegnare un giga di immagini a un cliente. Invece di masterizzare un Cd o muoversi con il portatile, ha messo tutto su iPod ed è uscito con duecento grammi di libertà più un cavetto FireWire.
Qualcun altro ha invece scoperto che con iPod si scia perfettamente, senza che la musica salti o skippi nonstante le cunette. E la musica non finisce dopo tre discese o due code allo skilift.
Benvenuto, iPod.

lux@mac.com