---
title: "Un pianeta giovane (e largamente inesplorato)"
date: 2007-05-20
draft: false
tags: ["ping"]
---

Il programma di cui nessun altro parlerà oggi è <a href="http://www.osxplanet.com/" target="_blank">OsxPlanet</a>.

Non è originalissimo, ma dà un tocco in più a qualsiasi desktop. E poi è scritto da un ragazzo di diciotto anni. Facile perdonargli un errore, facile che facendoglielo notare lui lo corregga in fretta e impari ancora di più e meglio.

Xcode è presente in qualsiasi confezione di Mac OS X in giro per il pianeta. Per imparare a fare cose molto semplici basta un'ora di tempo e un po' di voglia di leggersi un tutorial. Per fare di più è necessario rinunciare. A che cosa, è compito di ognuno, ammesso che interessi, chiaro. Se interessasse a una persona su mille in più, avremmo l'Età dell'Oro, mannaggia.