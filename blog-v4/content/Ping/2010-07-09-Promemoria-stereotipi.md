---
title: "Promemoria stereotipi"
date: 2010-07-09
draft: false
tags: ["ping"]
---

Si trovano in giro cervelli geneticamente modificati per i quali <i>iPhone lo comprano i fighetti</i>.

Prendano nota che Microsoft ha fatto uno <i>smartphone</i> apposta per fighetti, <a href="http://www.kin.com/" target="_blank">Kin</a>. Guardarsi il sito è istruttivo come poche altre esperienze.

È stato un fiasco di proporzioni imbarazzanti. Il <i>provider</i> Verizon piazzerà le scorte come può (i prezzi sono già stati dimezzati), ma Microsoft ha staccato la spina e il lancio di Kin in Europa è stato annullato. Le stime presenti su Internet sul numero di Kin venduti vanno da <a href="http://www.businessinsider.com/microsoft-rank-and-file-felt-embarassment-all-over-campus-from-kin-failure-2010-7" target="_blank">503</a> a <a href="http://www.facebook.com/apps/application.php?id=152094602912" target="_blank">8.837</a> in due mesi.

iPhone 4 ha venduto un milione e mezzo di unità nei primi tre giorni.

I fighetti hanno cose ben più importanti cui pensare che non gli <i>smartphone</i>. Certo, gli iPad. I telefoni vanno fuori produzione, ma i cervelli quelli sono e quelli rimangono.