---
title: "Più lo ami più lo odi"
date: 2010-03-08
draft: false
tags: ["ping"]
---

Non ho mai sollecitato il mio <i>account</i> MobileMe come in questi giorni. Su iDisk stanno transitando diversi gigabyte di video diretti a un cliente e nel contempo, essendosi aggiunto un nipote alla lista, si sono moltiplicate le <i>gallery</i> fotografiche.

Ho capito che la copia di file pesanti centinaia di megabyte o gigabyte è affidabile, a patto di accettare il compromesso e tenere sul disco fisso una copia locale di iDisk. Molto spesso una copia via Finder da disco locale a iDisk si blocca sul più bello. Invece la sincronizzazione tra iDisk locale e iDisk virtuale è ineccepibile e trasparente. Copiare i file su iDisk locale è ovviamente quasi istantaneo; vado avanti a lavorare e intanto la sincronizzazione mi garantisce che tutto arriverà <i>online</i> senza problemi. Il tempo necessario mi sembra anche minore, ma non oso metterci la firma. Forse lo percepisco minore visto che la copia avviene mentre me ne disinteresso.

Allestire una <i>gallery</i> è uno spasso, veloce e semplice, basta iPhoto. Ci sono anche funzioni ottime per la <i>privacy</i> e la collaborazione. C’è ancora però qualche inciampo nella gestione e nel corretto funzionamento.

La <i>gallery</i> del nipotino è accessibile solo via <i>password</i>, che è stata comunicata agli aventi diritto. I quali possono arricchirla con loro foto caricandole da <i>browser</i> oppure via posta, a un indirizzo apposito. Disgraziatamente però queste due funzioni sono tutto tranne che affidabili. Non sono ancora riuscito a vedere portato a termine un aggiornamento da <i>browser</i>. Con la posta ci sono riuscito una volta su due e non è il massimo.

Inoltre è possibile gestire la <i>gallery</i> con una <i>password</i> diversa da quella usata per la cartella Public di iDisk. La gestione però non è semplicissima e, non so ancora se per colpa mia, nell’impostare la password per la <i>gallery</i> è stata disabilitata quella per Public. Avrei capito se quest’ultima fosse rimasta spalancata a chiunque; invece rimaneva bloccata all’accesso esterno, ma chi doveva non poteva accedere neanche con la <i>password</i> buona.

Nel trovare sempre poco equilibrate le critiche rivolte all’uso base di MobileMe, che è una bellezza, rilevo invece che un uso più approfondito non è ancora scorrevole come dovrebbe. Mi riservo di tornare in argomento dopo avere accumulato più dati e più certi. Certo è che la frustrazione deriva non dall'usare MobileMe, bens&#236; dal volerlo usare a fondo. Il che è positivo.