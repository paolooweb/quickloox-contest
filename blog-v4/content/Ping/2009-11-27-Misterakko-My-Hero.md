---
title: "Misterakko My Hero"
date: 2009-11-27
draft: false
tags: ["ping"]
---

Potevo correggere a posteriori i messaggi su Skype e non lo sapevo.

Basta fare freccia in su, o clic destro sul messaggio.

La mia digitazione non sarà più la stessa. Un grazie grosso cos&#236; ad <a href="http://www.accomazzi.it" target="_blank">Akko</a>!