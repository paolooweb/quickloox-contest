---
title: "AppleScript, Mail e Safari"
date: 2008-03-06
draft: false
tags: ["ping"]
---

Un grosso grazie a <strong>Carlo</strong> che ha gentilmente inviato lo script sottostante.

Questo script invia un messaggio di posta contenente un link a una pagina web aperta in Safari (quante volte capita di mandare una mail con dentro un link e fare tutto a mano? A me spesso).

Lo script è abbondantemente commentato e non mi dilungo, se non per aggiungere un paio di nozioni.

La prima è l'uso di un <em>carattere di escape</em>, che serve a rappresentare un altro carattere di tipo speciale. Per scrivere una doppia virgoletta in AppleScript in modo che venga usato come oggetto e non come comando, dobbiamo anticiparla con il <em>backslash</em>, o barra inversa, in altre parole <code>\</code>.

La seconda è il concatenamento, per il quale si usa il carattere <code>&#38;</code>. Lo si è già usato in sordina; qui è il suo trionfo. Concatenare significa dare l'ordine <em>unisci quanto precede e quanto segue il carattere</em> <code>&#38;</code>.

Se non era mai stato detto: un'istruzione AppleScript che inizia con due trattini e uno spazio è un <em>commento</em>, che il programma non considera. Dentro un commento può esserci di tutto, è come se non ci fosse niente.

Ecco lo script:

<code>--Crea un messaggio in Mail con la pagina corrente di Safari</code>
<code>--(c) Notarianni Carlo</code>


<code>-- Per rendere lo script maggiormente leggibile</code>
<code>-- copiamo in una variabile il carattere a capo </code>
<code>-- ovvero il carattere 10 in Codice ASCII</code>
<code>set aCapo to ASCII character 10</code>

<code>-- i doppi apici sono un carattere speciale, </code>
<code>-- un carattere "riservato" come usava dire nei vecchi manuali </code>
<code>-- per inserirlo lo stesso lo dobbiamo far precedere dal carattere \</code>
<code>set doppiapici to "\"</code>

<code>-- OK, Possiamo iniziare</code>
<code>tell application "Safari"</code>
<code>	-- Copiamo il titolo della pagina</code>
<code>	set TitoloInSafari to name of front window</code>
	
<code>	-- Ora copiamo l'indirizzo</code>
<code>	set URLdiSafari to URL of document of front window</code>
<code>end tell -- Safari</code>

<code>-- Ecco cosa inseriremo nel campo "Oggetto" di Mail</code>
<code>set OggettoMail to "Link a " &#38; TitoloInSafari</code>

<code>-- Il Contenuto del messaggio è ottenuto concatenando del testo</code>
<code>set ContenutoMail to "Ecco il link alla pagina: " &#38; doppiapici &#38; TitoloInSafari &#38; doppiapici &#38; aCapo &#38; aCapo &#38; URLdiSafari &#38; aCapo
</code>
<code>tell application "Mail"</code>
<code>	activate</code>
<code>	set newMsg to (make new outgoing message with properties {visible:true, content:ContenutoMail, subject:OggettoMail})
</code>
<code>-- aggiungere eventuale altro codice per Mail qui...</code>
<code>end tell -- Mail</code>

Se ci fossero problemi, lo posso mandare per posta (lux@mac.com). Se vuole farlo Carlo, basta che mi autorizzi a pubblicare il suo indirizzo.

Si parla di AppleScript su Ping! nei giorni del mese divisibili per 7.