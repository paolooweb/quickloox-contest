---
title: "A domanda rispondono"
date: 2009-04-22
draft: false
tags: ["ping"]
---

Servisse a qualcuno, non riuscivo a stampare su una multifunzione da ufficio Ar-M350N Sharp, nonostante la presenza dei <a href="http://www.sharpusa.com/products/business/copiers/business_copiers_drivers/" target="_blank">driver sul sito Sharp</a>.

Quante saranno le stampanti cos&#236; in Italia? Cos&#236; ho chiesto sui forum Apple, sperando nel vantaggio della globalità.

In trentuno minuti è arrivata <a href="http://discussions.apple.com/thread.jspa?threadID=1981526" target="_blank">la risposta risolutiva</a>. Il tema era probabilmente facile, per qualcuno che lo conosce. Il problema era proprio trovare chi lo conoscesse e da questo punto di vista lo strumento si è dimostrato eccellente.