---
title: "Bug University II"
date: 2009-11-01
draft: false
tags: ["ping"]
---

Mentre avevo il <a href="http://www.macworld.it/blogs/ping/?p=3020" target="_self">problema già riferito</a> con BBEdit, usavo le applicazioni in inglese (come è mia abitudine) ma il Finder in italiano, per bisogno di consultare rapidamente certa terminologia.

Rimesso il Finder in inglese, il problema è sparito.

Ciò non significa necessariamente che il problema stia nel Finder italiano: potrebbe essere qualche dato rovinato in un pacchetto di localizzazione, o in un font. S&#236;, perché mentre il problema si manifestava, quella finestra di dialogo di BBEdit usava nei campi testo un font differente da quello normale. Inoltre, dopo avere eseguito una riparazione del disco (consigliata da Utility Disco e coronata da successo, stando ai messaggi del programma), il problema era peggiorato; oltre ai sintomi già descritti era diventato impossibile incollare testo nella finestra dove, per capirci, si inserisce l'Url da associare al link.

Ora proseguirò provando a verificare i font e intanto aspetto la replica del supporto tecnico di Bare Bones. Se loro riescono a riprodurre il problema cambiando la lingua del Finder, mi sono imbattuto in un <i>bug</i> di BBEdit; se invece non ci riescono, significa che c'è qualche file malmesso sulla mia macchina. Ho già reinstallato BBEdit e potrei provare a reinstallare parzialmente Snow Leopard, visto che la causa potrebbe essere un qualche documento o pacchetto di localizzazione italiana che si è danneggiato nella installazione.

Grazie a tutti per i numerosi suggerimenti pervenuti in pubblico e in privato. :)