---
title: "Solo le triglie abboccano al G5"
date: 2001-12-04
draft: false
tags: ["ping"]
---

The Register, sito inglese divertentissimo quanto inattendibile, da tempo sostiene che sia pronto il G5, il nuovo processore Motorola, e che verrà presentato al Macworld di gennaio in un qualche superMacintosh.
Credere ai siti di pettegolezzi, specie se non autorevoli, è il modo migliore per fare danni ad Apple: Fregnacce.com si inventa una storia fantascientifica e disinformata; io abbocco allo storione (ma da triglia) e rimando l’acquisto di un computer che invece mi servirebbe; il pettegolezzo si rivela infondato nove volte su dieci; la stampa, invece di spiegare “Ci sono siti che scrivono sciocchezze”, strilla “Apple delude le aspettative”.
Il bello è che danneggio anche me stesso, perché rimando inutilmente un acquisto utile.
Che il G5 ci sia davvero o meno è quasi lo stesso. Chi aspetta all’infinito le meraviglie promesse dai siti disinformati perde tempo all’infinito. E il tempo, come sa chiunque debba comprare i regali di Natale, è prezioso.