---
title: "Un computer quasi perfetto"
date: 2002-01-22
draft: false
tags: ["ping"]
---

La seconda cosa che avevano chiesto ad Apple gli utenti era uno schermo più grande. La prima...

In un’intervista a Maccentral Phil Schiller, Vice President of Worldwide Product Marketing (in pratica Fantasista Massimo per tutte le presentazioni che Steve Jobs non ha il tempo di tenere), ha scalvalcato momentaneamente il tema del nuovo iMac per parlare di iBook.
Apple ha da tempo l’abitudine di ascoltare le richieste dell’utenza, per valutare che tipo di migliorie apportare alle generazioni successive di un certo modello. Ebbene, ha detto Schiller, il cambiamento principale richiesto su iBook è stato... nessuno.
Certo, poi è stato anche chiesto uno schermo più grande (che è arrivato). Ma di certo qualche progettista in questo momento sta festeggiando una vacnza premio alle Hawaii.

lux@mac.com

http://maccentral.macworld.com/news/0201/08.schiller.php
