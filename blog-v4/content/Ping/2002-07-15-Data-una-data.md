---
title: "Data una data"
date: 2002-07-15
draft: false
tags: ["ping"]
---

Un sistema difficile e insicuro per circoscrivere brillantemente eventuali problemi in Mac OS X

Mac OS X fa qualcosa di strano? Un comportamento inusuale? La videocamera che il tuo amico collega in mezzo secondo da te non viene riconosciuta?
Nel cercare di risolvere questo e altri problemi ricordati che Mac OS X non è un sistema specifico, nel senso che – a parità di versione – i file installati su una macchina sono identici a quelli installati su un’altra macchina.
O meglio, dovrebbero esserlo. È già il secondo consulente che mi racconta di avere risolto comportamenti anomali del sistema confrontando le date dei file di sistema su una macchina con quelle di un’altra macchina che non esibisce quel problema.
Cerrto, prima bisogna avere un’idea seppur vaga delle possibili ragioni del bug; ma questo distinguo vale dalla notte dei tempi informatici.

<link>Lucio Bragagnolo</link>lux@mac.com