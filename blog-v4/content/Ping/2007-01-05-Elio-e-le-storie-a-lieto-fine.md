---
title: "Elio e le storie a lieto fine"
date: 2007-01-05
draft: false
tags: ["ping"]
---

<strong>Elio</strong> mi scrive: ha installato <a href="http://www.wesnoth.org" target="_blank">Battle for Wesnoth</a> sul computer del figlio, ma il programma si chiude appena prova ad aprirlo.

Acclude in allegato un frammento di un log di errore della Console:

<code>2007-01-03 12:23:17.367 Battle for Wesnoth[350] Unable to load nib file: SDLMacOSXMain, exiting</code>

Il programma non riesce a caricare un file Sdl. Sdl è una libreria di software open source molto usata per i giochi. Battle for Wesnoth certamente la usa. Ma non ne trova un pezzo e quindi, con tutta probabilità, forse per uno scaricamento andato male, forse per un piccolo errore nella copia, forse chissà perché, al programma manca un pezzo. Diagnosi: butta via, riscarica e riprova. Funziona!

Morale: le cose accadono con una ragione. A volte Console aiuta. Bastano un pizzico di fortuna e un pizzico di attenzione.

Elio ha un altro problema: iTunes 7.02 non gli vede gli altoparlanti remoti. E ha provato a buttare e reinstallare, ma senza esito. In attesa che salti fuori un buon messaggio di Console, magari qualcuno lo può aiutare più di me.