---
title: "A scuola di interfaccia"
date: 2003-07-29
draft: false
tags: ["ping"]
---

Perché la strada verso il software umano è ancora lunga

Parlo con Cristiano, promettente programmatore, che mi mostra una delle sue ultime creazioni. Ha scritto anche un installatore per il programma, che mostra la consueta barra di progresso e, in mezzo, l’andamento in percentuale dell’installazione.

Gli faccio notare che la barra di progresso è di lunghezza fissa e che la lunghezza stessa è ben superiore ai cento pixel; quindi la barra si aggiorna più spesso dell’indicatore percentuale. Quindi la barra è intrinsecamente più informativa dell’indicatore percentuale, che è magari un abbellimento estetico ma non aggiunge alcuna informazione e anzi invia un messaggio di troppo all’utente.

Ma così è molto più figo, spiega Cristiano, che è giovane e che a queste cose ci tiene. Concordo con lui, ma gli ribadisco che, per quanto figo, è inutile e dannoso. Piuttosto, se vuoi essere davvero utile, gli dico, spiega sopra o sotto la barra di progresso che cosa accade e che file vengono installati. Cose che la barra di progresso non spiega.

Così gli sembra più figo ancora e si rimette al lavoro.

Anche per oggi siamo riusciti a non peggiorare la situazione mondiale delle interfacce grafiche. Se solo non fosse una lotta così impari.

<link>Lucio Bragagnolo</link>lux@mac.com
