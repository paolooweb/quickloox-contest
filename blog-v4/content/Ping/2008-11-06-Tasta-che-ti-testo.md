---
title: "Tasta che ti testo"
date: 2008-11-06
draft: false
tags: ["ping"]
---

Periodo di tastiere. Oggi sono stato per una buona mezz'ora sulla tastiera standard di un iMac. Si diteggia alla grande e il <em>feeling</em> è veramente buono.

I tasti sono particolarmente elastici e questo incoraggia una certa abitudine (o vizio) che ho di dattiloscrivere con energia. Non ho il tocco vellulato, diciamo.