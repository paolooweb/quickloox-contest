---
title: "Apple mica tanto Store<p>"
date: 2005-04-13
draft: false
tags: ["ping"]
---

Appena trattano un prodotto difficile si dimostrano molto sotto la qualità del loro software<p>

Giorno più giorno meno, sono sei settimane che ho ordinato World of Warcraft sull&rsquo;Apple Store. Non è ancora arrivato niente. Quando è stato effettuato l&rsquo;ordine, la data di consegna prevista era a tre settimane.<p>

Il punto non è questo. World of Warcraft sta conoscendo un successo siderale e per metà la stessa produttrice Blizzard si è trovata colta di sorpresa, non avendo abbastanza scatole; per metà ho la sensazione che non vogliano neanche venderle troppo in fretta, per non mettere a rischio la stabilità del mondo online con un flusso eccessivo di immigrati; per la terza metà (scherzo), sono un po&rsquo; imbecilli per la decisione di obbligare a comprare tramite distributore tutti quelli che stanno fuori da Stati Uniti e Regno Unito.<p>

Ci sono validissime ragioni per cui l&rsquo;Apple Store non riesca a consegnarmi World of Warcraft tre settimane dopo le loro tre settimane. Il punto è che un vero venditore online è capace di mandare una mail al cliente, spiegare la situazione, scusarsi, assicurare che stanno facendo il possibile, offrire un buono spesa di dieci centesimi a risarcimento del danno biologico, indicare una nuova data possibile di inizio consegne pregando di avere pazienza e cose così.<p>

Invece niente del genere. Prova ad acquistare con Amazon: certe volte hai l&rsquo;impressione di essere l&rsquo;unico acquirente, da quanto sono solleciti. L&rsquo;Apple Store a confronto è popolato da morti viventi.<p>

Dammi retta. Specie se il prodotto è difficile, prima prova a vedere se lo trovi tu. L&rsquo;Apple Store è uno store sui generis, e non è neanche tanto Apple. Chi è capace di tirare fuori una meraviglia come Tiger non è certamente lo stesso gruppo di chi non sa neanche mandarti una mail per farti sapere che i tuoi soldi contano qualcosa anche per loro.<p>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>