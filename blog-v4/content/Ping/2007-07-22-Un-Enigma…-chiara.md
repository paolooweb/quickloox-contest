---
title: "Un Enigma&#8230; chiara"
date: 2007-07-22
draft: false
tags: ["ping"]
---

Non so quanti si interessino di crittografia, tuttavia voglio segnalare per loro l'<a href="http://www.bubbletub.com/terrylong.html" target="_blank">Enigma Simulator di Terry Long</a>.

Di simulatori di Enigma è piena Internet, questo però è ben realizzato anche graficamente ed è piacevole da guardare. Poi, feature non secondaria, funziona a perfezione. :-)