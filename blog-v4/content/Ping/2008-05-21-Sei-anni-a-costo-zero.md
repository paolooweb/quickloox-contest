---
title: "Sei anni a costo zero"
date: 2008-05-21
draft: false
tags: ["ping"]
---

Sembra che riuscirò a regalare alla nipotina per il suo sesto compleanno un meraviglioso iBook conchiglia, recuperato a costo zero da una signora che cambiando macchina intendeva semplicemente buttare quella vecchia.

Panther è già stato portato a 10.3.9 e asciugato di tutto il superfluo per assicurare spazio di manovra anche con poco disco e poca Ram.

Il computer costerà zero ma ci passerò tutto il tempo disponibile per riempirlo di giochi, programmi educativi e piccoli stimoli adatti alla sua età per scoprire strade nuove da sola.

Ho scoperto <em>en passant</em> che adesso Apple propone il voto da uno a cinque stelle sulle note tecniche (per la precisione su <a href="http://support.apple.com/kb/TS1537" target="_blank">questa nota</a> relativa a Mac OS X 10.3.9 e l'aggiornamento Java).

Il fatto tecnico è però del tutto marginale. Le mie scarse competenze sono tutte concentrate nello scegliere i migliori sfondi scrivania dalle foto di famiglia, i salvaschermo più curiosi e nel decidere come farla salutare a voce dal Mac tutte le volte che viene avviato