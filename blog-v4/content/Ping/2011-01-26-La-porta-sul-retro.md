---
title: "La porta sul retro"
date: 2011-01-26
draft: false
tags: ["ping"]
---

Un aspetto affascinante della venuta di iOS è che permette e favorisce il riciclo, preferisco il rilancio, di software che appartiene alla tradizione e sui desktop non c'è più tutto questo interesse a proseguire, mentre su un apparecchio da tasca può ancora rappresentare un incentivo. iPhone e compagni sono non solo alfieri di una nuova era informatica, ma anche porte efficaci per accedere al mondo del <i>retrocomputing</i>.

La tendenza si è fatta evidente con <a href="http://itunes.apple.com/it/app/myst/id311941991?mt=8" target="_blank">Myst</a> e <a href="http://itunes.apple.com/it/app/riven-the-sequel-to-myst/id400293367?mt=8" target="_blank">Riven</a> per iPhone. È proseguita con il porting di Space Quest, Leisure Suit Larry e altri classici degli anni ottanta in Html5, giocabili su un iPad (chi li ha scaricati li tenga stretti, perché l'iniziativa è risultata <a href="http://sarien.net/" target="_blank">sgradita ad Activision</a>).

Ora ricevo notizia di <i>Oregon Trail</i>, gioco educativo gestionale nato quasi quarant'anni fa, dove alla guida di un carro da pionieri si viaggiava verso il selvaggio Ovest e bisognava superare malattie, guasti meccanici, maltempo e insidie assortite per arrivare con tutta la famiglia a iniziare una nuova vita. Non so se esista un americano, che bimbo dell'età giusta all'epoca, non abbia giocato a <i>Oregon Trail</i> (la pista verso l'Oregon).

Beh, ora il gioco è tornato disponibile per <a href="http://itunes.apple.com/it/app/the-oregon-trail-avventura/id307519882?mt=8" target="_blank">iPhone e iPod touch</a> a 0,79 euro e anche in edizione <a href="http://itunes.apple.com/it/app/the-oregon-trail-avventura/id323108912?mt=8" target="_blank">gratuita</a>. In più, previa installazione di un <i>plugin</i>, si può giocare nativamente dentro il browser di un Mac Intel, <a href="http://itunes.apple.com/it/app/the-oregon-trail-avventura/id323108912?mt=8" target="_blank">come se Safari diventasse un Apple II</a>.

Certo oggi i giochi hanno grafica e complessità diversa. A qualche dodicenne però farebbero ancora bene quelli di una volta.