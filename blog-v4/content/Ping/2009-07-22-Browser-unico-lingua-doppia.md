---
title: "Browser unico, lingua doppia"
date: 2009-07-22
draft: false
tags: ["ping"]
---

Non ho ancora informazioni definitive, ma pare che i corsi di inglese <i>online</i> del <a href="http://www.wallstreet.it" target="_blank">Wall Street Institute</a> siano riservati a chi usa Internet Explorer su Windows e nessun altro.

Ironia della sorte, proprio chi insegna la seconda lingua insiste per avere un unico <i>browser</i>.

Una buona occasione per evitare il suddetto istituto come la peste e, capitasse di averci rapporti, spiegare bene perché si preferisce la concorrenza.

Non è più come una volta, quando si protestava e si era in due, compreso il gatto. Oggi, dice Market Share, <a href="http://marketshare.hitslink.com/report.aspx?clearaf=1&qprid=0&qptimeframe=Q&qpsp=41&qpmr=100&qpdt=1&qpct=0&clearaf=1" target="_blank">dati di primavera 2009</a>, a usare Explorer non è più un bulgaro novanta percento; è il sessantasei.

Gli astuti rinunciano in partenza a un cliente su tre.

Comportamento ottuso, come minimo. Sanno curare la pronuncia, ma gli manca lo stile.