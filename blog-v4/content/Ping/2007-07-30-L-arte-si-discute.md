---
title: "L'arte si discute"
date: 2007-07-30
draft: false
tags: ["ping"]
---

Sulla mailing list <a href="http://listserv.dartmouth.edu/scripts/wa.exe?A0=MACSCRPT" target="_blank">Macscrpt</a> hanno fatto una innocente domanda su come cucire insieme due liste in AppleScript.

Gli hanno risposto in tutti i modi possibili. È solo codice, ma vederlo cos&#236; è quasi arte.

Intendiamoci: <a href="http://www.lisp.org/alu/home" target="_blank">Lisp</a> è più elegante. AppleScript può però fare cose belle, anche se in modalità <em>de noantri</em>.

Il problema era: partendo dalle liste <code>{1, 2, 3}</code> e <code>{"a", "b", "c"}</code>, arrivare a una lista sola, <code>{{1, 2, 3}, {"a", "b", "c"}}</code>.

Le soluzioni:
<code>
set questaLista to {1, 2, 3}
set quellaLista to {"a", "b", "c"}
set coppiaListe to {}
set end of coppiaListe to questaLista
set end of coppiaListe to quellaLista
--> {{1, 2, 3}, {"a", "b", "c"}}
</code>

<code>
set questaLista to {1, 2, 3}
set quellaLista to {"a", "b", "c"}
set coppiaListe to {}
set coppiaListe to ((coppiaListe &#38; {questaLista}) &#38; {quellaLista})
--> {{1, 2, 3}, {"a", "b", "c"}}
</code>

Ma c'è chi ha scritto praticamente un manuale della gestione delle liste:

<code>
set _lista1 to {1, 2, 3}
-->  {1, 2, 3}
set _lista2 to {"a", "b", "c"}
-->  {"a", "b", "c"}
</code>

<code>
set _lista3 to _lista1 &#38; _lista2 -- crea una lista da due liste
-->  {1, 2, 3, "a", "b", "c"}
</code>

<code>
set _lista3 to _lista1 &#38; {_lista2} -- aggiunge una lista a una lista
-->  {1, 2, 3, {"a", "b", "c"}}
</code>

<code>
copy _lista2 to end of _lista1 -- aggiunge una lista a una lista
-->  {1, 2, 3, {"a", "b", "c"}}
</code>

<code>
copy {"x", "y", "z"} to item 2 of _lista1 -- sostituisce un elemento di una lista con una lista
-->  {1, {"x", "y", "z"}, 3, {"a", "b", "c"}}
</code>

<code>
set coppiaListe to {_lista1, _lista2} -- crea una lista da due liste
-->  {{1, 2, 3}, {"a", "b", "c"}}
</code>

<code>
set beginning of _lista1 to _lista2 -- aggiunge una lista all'inizio di una lista
-->  {{"a", "b", "c"}, 1, 2, 3}
</code>

<code>
set 1 to {{"a", "b", "c"}}
set listaNidificata2 to {{"uno", "due", "tre"}}
set listaNidificata3 to {{"do", "re", "mi"}}
set listaNidificata4 to listaNidificata1 &#38; listaNidificata2 &#38; listaNidificata3
--> {{"a", "b", "c"}, {"uno", "due", "tre"}, {"do", "re", "mi"}}
</code>

Se vuoi saperne di più su AppleScript, è il caso di&#8230; mettersi in lista.