---
title: "Quanto costa il troubleshooting"
date: 2003-09-28
draft: false
tags: ["ping"]
---

La gente guarda i listini e poi non si fa domande

È singolare come quasi nessuno, al momento di comprare qualunque aggeggio, fosse pure il frullatore, non si faccia due domande che incidono sul prezzo in maniera mostruosa: quanto durerà e quanto tempo farà perdere in riparazioni.

Sul frullatore non so, ma sui computer si tratta di elementi cruciali, quelli che – per inciso – alla fine, dinanzi a qualsiasi comparazione di listino, mi fanno scegliere un Mac.

I miei Mac sono durati mediamente qualcosa più di tre anni, con punte di cinque, non perché si siano rotti ma perché c’era bisogno di cambiarli (piuttosto mangio sei mesi pane e salame, ma compro il Mac giusto per il mio lavoro e i miei hobby, e quando non va più bene mangio pane e salame ancora ma lo cambio). E il tempo di troubleshooting, di fermo macchina, di complicazioni è stato infinitamente basso.

Un consiglio: quando devo comprare il computer, poniti le due domande insieme alle altre. Prenderai un Mac.

<link>Lucio Bragagnolo</link>lux@mac.com
