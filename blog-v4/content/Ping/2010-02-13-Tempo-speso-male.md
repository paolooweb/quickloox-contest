---
title: "Tempo speso male"
date: 2010-02-13
draft: false
tags: ["ping"]
---

Ho acquistato, giusto in tempo, <a href="http://store.apple.com/it/browse/home/applecare" target="_blank">AppleCare</a> per il mio MacBook Pro 17&#8221;.

La procedura per attivare il prolungamento della garanzia non è complicata, ma è tanto vecchio stile. L'indirizzo della pagina giusta si trova sul manualetto di carta, si passa attraverso vari clic prima di arrivare, la necessità di avere una Apple Id è comprensibile ma fastidiosa (lo dico avendone già una, per me il problema non si pone) eccetera.

Se davvero sono necessari il numero di serie di Mac e il numero di registrazione di AppleCare, è semplice mettere in piedi un meccanismo per cui vado su una pagina ben pubblicizzata su Apple Store o dove altro, inserisco il numero di serie, inserisco quello della carta di credito e ottengo il numero di registrazione esclusivo. E magari vengo invitato a installare qualcosa che, dovesse verificarsi un guasto, spiega subito al tecnico che c'è AppleCare attiva, senza bisogno di fornire altre pezze di appoggio.

Giusto perché ci si lamenta che i Mac non hanno il lettore Blu-ray oppure che gli iPad non avranno la fotocamera. Io vorrei garanzia estesa acquistabile in tre secondi in modo scandalosamente semplice. Forse se ne venderebbero pure di più e pure a un margine maggiore per chi vende.