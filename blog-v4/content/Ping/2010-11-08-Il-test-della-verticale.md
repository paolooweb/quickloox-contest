---
title: "Il test della verticale"
date: 2010-11-08
draft: false
tags: ["ping"]
---

Carico la <a href="http://www.apple.com/it/" target="_blank">pagina attuale di Apple Italia</a> su tutti i computer <em>mobile</em> di Apple.

MacBook Pro 17&#8221; mostra tutta la pagina.

Tutti gli altri MacBook Pro e MacBook, più MacBook Air 13&#8221;3, mostrano lo strillo di MacBook Air e almeno parte dei riquadri inferiori.

MacBook Air 11&#8221;6 mostra solo lo strillo e neanche la striscia di news immediatamente sotto.

La risoluzione verticale è importante per la produttività e MacBook Air 11&#8221;6, altrimenti incantevole per leggerezza e compattezza, è il computer meno produttivo in assoluto a listino di Apple.

Si noti che iPhone 4, a schermo orizzontale, mostra quanto MacBook Air 11&#8221;6, ma sta in tasca e in verticale mostra tutta la pagina, per quanto da zoomare.

Non parliamo di iPad, che mostra quanto i portatili intermedi in orizzontale, ma in verticale mostra tutta la pagina. E oltretutto la pagina intera è leggibile in ogni sua parte senza necessità di zoomare.