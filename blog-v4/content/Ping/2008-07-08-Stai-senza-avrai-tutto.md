---
title: "Stai senza, avrai tutto"
date: 2008-07-08
draft: false
tags: ["ping"]
---

Su una mailing list di quelle che frequento si lamenta l'impossibilità di usare iPhone come modem.

Sempre più gente dimostra una preoccupante inabilità a considerare variazioni dei propri schemi di comportamento, ma è un problema più complesso di quanto posso trattare qui.

Qui voglio, ancora una volta, mettere in ordine prima dell'11 luglio. Quello che succederà dopo lo si scoprirà solo tra qualche giorno. Qui e ora, con iPhone Edge, si può seguire <a href="http://lifehacker.com/software/feature/use-your-iphones-internet-connection-on-your-laptop-327066.php" target="_blank">il metodo descritto da Life Hacker</a> e iPhone funzionerà come modem del portatile.

Altrimenti è possibile seguire <a href="http://www.zdziarski.com/papers/tethering.txt" target="_blank">il metodo di Zdziarski</a>.

Sennò si può seguire <a href="http://www.hackint0sh.org/forum/showthread.php?t=34143" target="_blank">il metodo di Hackintosh</a>.

E adesso speriamo di non sentire più questa stupidaggine dell'iPhone che non fa da modem.

Anche perché, è vero, questa è roba vagamente complicata. Però c'è anche quella elegante, che mi ha segnalato <strong>Alberto</strong> (mica per niente Vip dell'<a href="http://www.allaboutapple.com" target="_blank">All About Apple Museum</a>).

La sostanza è: installi <a href="http://www.joiku.com" target="_blank">Joiku</a>, trasformi iPhone in uno hotspot wi-fi, ecco fatto.

Quattro modi diversi, di cui uno elegante. Da un po' di tempo, con i computer, finisce sempre che puoi avere tutto appena ti decidi a stare senza. Fili.