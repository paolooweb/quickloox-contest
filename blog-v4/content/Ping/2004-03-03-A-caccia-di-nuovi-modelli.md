---
title: "A caccia di nuovi modelli"
date: 2004-03-03
draft: false
tags: ["ping"]
---

Un sito che aiuta nelle decisioni (e non) di acquisto

L’hanno capito anche i sassi che periodicamente escono nuovi modelli di Mac, ma si trova sempre qualcuno che cerca informazioni sulle possibili nuove uscite per non ritrovarsi con un computer “svalutato” da un modello più recente.

In un mondo dove il ciclo di vita dei prodotti è ormai inferiore ai dodici mesi non si capisce quale bisogno spinga questo comportamento, a parte un superficiale conforto psicologico. Comunque, per chi ne ha bisogno, si può provare <link>Mac Buyer’s Guide</link>http://buyersguide.macrumors.com/.

Fanno quello che dovrebbe fare qualsiasi persona interessata: guardano da quanto tempo è uscito l’ultimo modello e ipotizzano il momento di uscita del nuovo.

Se si potrà arrivare all’acquisto di Mac “rivalutati”, ben venga. I miei migliori acquisti li ho sempre fatti con macchine prossime all’uscita di produzione.

<link>Lucio Bragagnolo</link>lux@mac.com