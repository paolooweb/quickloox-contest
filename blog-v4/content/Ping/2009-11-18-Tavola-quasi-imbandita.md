---
title: "Tavola (quasi) imbandita"
date: 2009-11-18
draft: false
tags: ["ping"]
---

Stamattina avevo appuntamento in Vodafone. Tra le altre cose ho potuto visitare il bellissimo Learning Center aziendale e vedere una sala perfettamente attrezzata, con un Pc per postazione. Ma quello collegato al videoproiettore era un Mac, a conferma che quando le cose devono necessariamente funzionare la scelta migliore è sempre la solita.

Ma divago. Nella giornata intensa di oggi l'agenda prevede appuntamenti e commissioni, ma tutto procede in orario e stasera sarò puntualissimo per la Pingpizza.

Come sempre, appuntamento tra le 19:30 e le 20 fuori da @Work in via Carducci angolo galleria Borella a Milano, poi tutti a Bio Solaire (via Terraggio 20, a duecento metri) e chiacchiera libera. La forza delle Pingpizze sta nella qualità e non nel numero, quindi mi raccomando: è richiesto il buonumore. :-)