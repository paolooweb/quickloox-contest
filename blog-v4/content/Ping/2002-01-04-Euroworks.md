---
title: "Euroworks"
date: 2002-01-04
draft: false
tags: ["ping"]
---

Fa proprio effetto cambiare in un attimo un’abitudine consolidata da anni

Oggi ho impostato il foglio di calcolo delle spese di casa del 2002.
Prima, ho aperto il vecchio pannello di controllo di Mac OS 9.1 nel menu Apple del vecchio iMac Bondi (tre anni abbondanti e non li dimostra) e ho scelto la voce Numeri, per cambiare l’indicazione della valuta in un vecchio Opzione-i (su un Mac moderno sarebbe Opzione-e).
Poi ho aperto il vecchio foglio di calcolo nel vecchio AppleWorks 5, et voilà! Era già tutto pronto per la nuova moneta.

lux@mac.com