---
title: "Apple non farà"
date: 2003-12-30
draft: false
tags: ["ping"]
---

Che bello il silenzio prima di Macworld Expo

In redazione sono scansafatiche e chissà quando verrà pubblicato questo Ping, ma posso giurare di stare scrivendolo il 30 dicembre, a una settimana scarsa dall’inizio di Macworld Expo.

La cosa più bella di questo scorcio di fine 2003 è la quasi totale assenza di siti di pettegolezzi fuori luogo, incompetenti che creano false aspettative, chiacchiere disinformate e ondate di entusiasmo e di panico secondo la scempiaggine più recente che appare nelle mailing list rispetto a che cosa presenterà Steve Jobs.

Per quanto mi riguarda mi sto godendo una discussione molto interessante – e del tutto teorica – sulla possibilità tecnologica o meno di inserire un G5 negli attuali XServe, e niente più.

Molto meglio aspettare e vedere che speculare e alzare così tanto fumo che non si capisce più niente neanche quando si accendono le luci.

No, a Macworld Expo San Francisco 2004 Apple non presenterà un palmare.

<link>Lucio Bragagnolo</link>lux@mac.com