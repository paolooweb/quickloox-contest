---
title: "Impossibile archiviarla"
date: 2010-02-02
draft: false
tags: ["ping"]
---

La comunicazione dell'inizio delle iscrizioni a Fm Devcon 2010, la Conferenza italiana per gli sviluppatori e gli utenti di FileMaker.

Lasciando la parola al comunicato ufficiale, <cite>FM Devcon 2010 si terrà a Rimini dal 5 al 7 maggio. La conferenza vera e propria si terrà nei giorni 6 e 7, mentre per il giorno 5 abbiamo previsto ben due corsi di formazione di livello introduttivo-intermedio, uno su FileMaker Pro e uno su Php e la Pubblicazione Web Personalizzata. Per entrambi i corsi è necessaria una prenotazione separata.</cite>

<cite>Le sessioni copriranno le tematiche più importanti emerse negli ultimi anni e i nuovi orizzonti aperti dalle versioni più recenti di FileMaker. Verranno affrontate da esperti riconosciuti del mondo FileMaker, che metteranno a disposizione dei partecipanti le loro competenze e la loro pluriennale esperienza nel settore. FileMaker, Inc. sarà presente in via ufficiale presenziando sessioni tecniche ed informative.</cite>

Ci sono già una <a href="http://www.fmdevcon.com/iscrizione/" target="_blank">pagina per iscriversi</a> e un <a href="http://twitter.com/fmws" target="_blank">feed Twitter</a>.

FileMaker è uno dei software più sottovalutati che esistano, seduto su un motore di incredibile versatilità e in grado di risolvere problemi importanti di tanta gente che neanche lo sospetta. È un'occasione consigliata. Da non archiviare subito, neanche con FileMaker.