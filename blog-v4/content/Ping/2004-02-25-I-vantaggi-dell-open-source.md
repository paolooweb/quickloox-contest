---
title: "I vantaggi dell’open source"
date: 2004-02-25
draft: false
tags: ["ping"]
---

Ipotetica, ma non troppo, gara tra Safari e OmniWeb

Esce Safari, browser il cui motore è open source e renderizza brillantemente le pagine Html.

Esce OmniWeb, che usa il motore open source di Safari e renderizza ugualmente bene, aggiungendo nuove funzioni.

Siccome anche OmniWeb si basa su un motore open source, il team di Safari può vederne le innovazioni e introdurle in una nuova versione di Safari, aggiungendo sue novità.

Che, per lo stesso motivo, anche OmniWeb può utilizzare.

È uno scenario non troppo ipoteticodi due browser che, grazie alla forza del concetto open source, crescono e si rafforzano a tutto vantaggio dell’utenza, cioè ti di tutti noi.

Adesso pensa a Microsoft Proprietario Chiuso Abusatore di Monopolio Internet Explorer. Secondo te qual è la strada migliore?

<link>Lucio Bragagnolo</link>lux@mac.com