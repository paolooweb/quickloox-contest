---
title: "AppleScript e roba da cani"
date: 2008-06-02
draft: false
tags: ["ping"]
---

Scrive <strong>Phil</strong>:

<cite>Carissimo Lux,
dato che stai <em>contagiando</em> un bel po' di gente, me compreso, con il tuo entusiasmo per AppleScript, mi permetto di segnalarti <a href="http://www.latenightsw.com/sd4/index.html" target="_blank">Script Debugger 4.0</a> e i relativi <a href="http://www.latenightsw.com/scripting.html" target="_blank">links to useful information about scripting the Macintosh</a></cite>

<cite>Tra essi merita senza dubbio una visita <a href="http://www.scriptweb.com/" target="_blank">ScriptWeb</a>.</cite>

<cite>Un salutone da Filippo Cervellera (cioè dal sottoscritto) e da Mac (<em>Macchino</em>, il cagnolino super-furbo!), in allegato!</cite>

Di fronte alla prospettiva di pubblicare una foto di Filippo, ho preferito quella di Macchino. :-P