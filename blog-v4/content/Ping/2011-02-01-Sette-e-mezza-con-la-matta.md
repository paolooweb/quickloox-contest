---
title: "Sette e mezza con la matta"
date: 2011-02-01
draft: false
tags: ["ping"]
---

Samsung annuncia di <a href="http://www.engadget.com/2010/12/03/samsung-sells-one-million-galaxy-tab-units-throws-an-android-pa/" target="_blank">avere venduto un milione di Galaxy Tab da sette pollici</a>.

Steve Jobs <a href="http://www.tipb.com/2010/10/18/steve-jobs-7inch-tablets-terrible/" target="_blank">spiega</a> che i concorrenti costruiscono tavolette da sette pollici solo perché riescono a stare nei costi per arrivare sul mercato e non perché siano ergonomicamente valide. Uno pensa che 7&#8221; sia solo un po' meno di 10&#8221;, ma poi scopre che la superficie è solo il 45 percento e che quella dei suoi polpastrelli è invariata.

Apple <a href="http://www.apple.com/pr/library/2011/01/18results.html" target="_blank">vende 7,33 milioni di iPad prima di Natale</a>, per un totale di 14,8 milioni di iPad nel 2010.

Samsung annuncia <a href="http://arstechnica.com/gadgets/news/2011/01/samsung-moves-2-million-galaxy-tabs-announcing-a-tab-2-on-february-13.ars" target="_blank">due milioni di Galaxy Tab</a>.

Poi salta fuori che non sono vendite al compratore finale, ma <a href="http://blogs.wsj.com/digits/2011/01/31/samsung-galaxy-tab-sales-actually-quite-small/" target="_blank">forniture ai rivenditori</a>. Per Samsung sono vendite che non riprenderà certo indietro e poi i rivenditori si arrangino.

I rivenditori non li stanno vendendo tutti, i Galaxy Tab. In più pare, si dice, stime, c'è il 15 percento di unità vendute <a href="http://www.nypost.com/p/news/business/galaxy_tab_dim_bulb_KbD4K6OUjTC99SQn2efrsJ" target="_blank">e poi restituite per diritto di recesso</a>. Il tasso di restituzione degli iPad sarebbe del due percento, sette volte e mezzo inferiore.

Matta Samsung a sparare cifre per guadagnare spazio sui <i>media</i> senza pensare che la mezza verità sulle vendite sarebbe venuta a galla.