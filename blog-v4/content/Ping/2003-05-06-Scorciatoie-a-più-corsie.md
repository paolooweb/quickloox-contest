---
title: "Scorciatoie a più corsie"
date: 2003-05-06
draft: false
tags: ["ping"]
---

Nel campo delle shortcut non è che chi lascia la via vecchia per la nuova…

E poi uno dice che non si vede il progresso nella facilità d’uso. Basta guardare le scorciatoie.
Prendiamo un programma preistorico, tipo MacWrite. Il progresso, parlando di scorciatoie da tastiera, è che c’erano, valorizzando l’interfaccia utente grafica.
Prendiamo un <i>eccellente</i> programma di oggi, tipo BBEdit. Tutte le scorciatoie sono personalizzabili, essenzialmente premendo la scorciatoia desiderata davanti a un elenco gerarchico dei menu grafici. Facile e rapido.
Ora prendiamo un programma tipo Final Cut Pro 4: disponibile a brevissimo, ma in fatto di scorciatoie è un programma di domani. Tutto è grafico, con una tastiera virtuale sullo schermo, e gestibile da mouse.

BBEdit ha un approccio completo ed efficace; ma Final Cut Pro 4 definisce le scorciatoie di tastiera come si useranno da domani.

La strada delle scorciatoie ha più corsie e Apple ha imboccato palesemente quella di sorpasso. Brava. :-)

<link>Lucio Bragagnolo</link>lux@mac.com