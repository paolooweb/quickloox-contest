---
title: "Il Nuovo Mondo"
date: 2010-03-09
draft: false
tags: ["ping"]
---

Negli uffici di Panic, <i>software house</i> che produce programmi Mac come <a href="http://panic.com/transmit/" target="_blank">Transmit</a>, <a href="http://panic.com/unison/" target="_blank">Unison</a> e <a href="http://panic.com/coda/" target="_blank">Coda</a>, compare questo <a href="http://www.panic.com/blog/2010/03/the-panic-status-board/" target="_blank">tabellone elettronico</a>, in realtà una pagina web moderna mostrata su un monitor professionale appeso a un muro.

Gli istogrammi in alto mostrano la quantità di mail di supporto da smaltire in funzione dei giorni trascorsi (quando tutto è a zero compare una bottiglia di champagne&#8230; e ne stappano una).

Sotto appare lo stato dei progetti.

Sotto ancora si vedono eventuali conteggi alla rovescia importanti, per esempio quello di iPad.

I tre riquadri inferiori riguardano la variazione quotidiana del fatturato (sinistra), il calendario interno di Panic (destra) e, al centro, la situazione degli autobus! Cos&#236; chi torna a casa con i mezzi pubblici si può regolare.

Le ultime due righe sono dedicate al Twitter dei dipendenti e a quello dei clienti (chi <i>tweeta</i> un messaggio @Panic lo fa apparire l&#236;).

Niente fantascienza; tecnologia a disposizione di qualunque programmatore al mondo e <i>hardware</i> liberamente acquistabile su Internet. Tutto WebKit e neanche una riga di Flash o altre sconcezze. I più acuti possono distinguere un <i>bug</i> tipografico dovuto al browser.

Panic dice che il progetto è stato realizzato in tre settimane, senza rubare spazio allo sviluppo dei prodotti (cioè nel tempo libero).

In parte si sono ispirati a una <a href="http://culturedcode.com/status/" target="_blank">pagina analoga</a>, però pubblica, di Cultured Code, in cui ogni progetto è stato trattato come un volo aereo.

Non sappiamo se a Cultured Code abbia portato benefici. Sempre in Panic scrivono che il supporto ora funziona molto meglio. Nei commenti, tra il serio e il faceto, appaiono diverse autoproposte di impiego. Si suppone anche che avere gli orari degli autobus a disposizione sia una mano santa. L'azienda degli autobus locale <a href="http://developer.trimet.org/ws_docs/" target="_blank">mette a disposizione di chiunque una interfaccia di programmazione pubblica</a>.

Cos&#236; funziona nell'Oregon. Il Nuovo Mondo è anche quello del lavoro e delle infrastrutture. Parlane al tuo capo. E all'azienda degli autobus.