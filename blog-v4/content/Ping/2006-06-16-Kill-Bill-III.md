---
title: "Kill Bill III"
date: 2006-06-16
draft: false
tags: ["ping"]
---

Visto che &egrave; appena passato l&rsquo;entusiasmo planetario per la notizia che Bill Gates intende dedicare da qui al 2008 pi&ugrave; tempo alla beneficenza e meno a Microsoft, commenterei con l&rsquo;attenzione su qualcosa di molto piccolo: la chat di <a href="www.scubaportal.it/" target="_blank">Scubaportal</a>.

Funziona solo in Internet Explorer. Con Safari non funziona, con Firefox non funziona, con Omniweb non funziona, solo con iCab &egrave; vagamente usabile (ma compare solo un messaggio per volta, almeno con le impostazioni di default).

Ecco, di questo tipo &egrave; il lascito di Gates all&rsquo;informatica. Ora lo attendono bambini, poveri e malati. Saranno scintille.