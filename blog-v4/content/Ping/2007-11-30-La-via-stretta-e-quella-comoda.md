---
title: "La via stretta e quella comoda"
date: 2007-11-30
draft: false
tags: ["ping"]
---

Mi è arrivato un allegato una immagine codificata in base64 di cui vedevo solo la conversione in testo. Per qualche motivo, trascinandolo su StuffIt Expander, non viene ritrasformato in immagine, come mi aspettavo succedesse.

Mi sono messo a cercare e ho trovato <a href="http://www.etresoft.com/" target="_blank">Decoder 4.0.3</a> di Etresoft. Dovrebbe risolvere facile, ma non l'ho provato. Mentre scaricavo, infatti, ho trovato anche un comando da Terminale:

<code>openssl base64 -in</code> <em><originale></em> <code>-out</code> <em><risultato></em>

(ci sono anche un sistema per farlo <a href="http://face.centosprime.com/macosxw/?p=79" target="_blank">in Perl</a>) e il <a href="http://www.fourmilab.ch/webtools/base64/" target="_blank">comando Unix</a> apposta, da compilare e installare)

Il comando da Terminale ha già funzionato a perfezione. L'unica cosa è che, essendo Unix, si limita a ricostruire il file, senza inventarsi attributi, risorse, icone e compagnia bella. Cos&#236; ho sul computer il caso singolare di una immagine a postissimo, ma che non si può guardare con QuickLook.

Questa doppia corsia, con la strada facile e comoda per tutti e quella difficile e potente per chi se la sente, è una cosa meravigliosa di Mac OS X che continua ad affascinarmi.