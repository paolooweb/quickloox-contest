---
title: "È la mia volta?"
date: 2006-06-01
draft: false
tags: ["ping"]
---

Apparentemente no. QuickTime 7.1.1 e soprattutto SuperDrive Firmware Update aggiornati con successo, o cos&igrave; sembra. Sto rippando <a href="http://marok.org/Elio/Discog/thelos.htm" target="_blank">The Los Sri Lanka Parakramabahu Brothers</a> e almeno in lettura tutto procede.

Doppio riavvio, a causa del firmware, ma in regola. Quelli indesiderati restano due dal primo gennaio: sono passati 151 giorni, quindi settantacinque e mezzo tra un riavvio non voluto e un altro.

E i computer sarebbero tutti uguali? Nah.

