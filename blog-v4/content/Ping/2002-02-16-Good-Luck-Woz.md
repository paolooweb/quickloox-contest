---
title: "Good Luck Woz"
date: 2002-02-16
draft: false
tags: ["ping"]
---

Il cofondatore di Apple diventa missionario del wireless per tutti, e con ragione

Steve “Woz” Wozniak, cofondatore insieme a Steve Jobs di una piccola società chiamata Apple, ora sta fondando una piccola società che intende portare la tecnologia wireless sui prodotti di elettronica di consumo.
Chi scrive ha installato in casa una base Airport e adesso digita in allegria sdraiato in poltrona, ad almeno sette metri dalla presa del telefono, e senza neanche un filo (viva le batterie dei PowerBook!). La tecnologia wireless, davvero, cambia la vita.
Buona fortuna Woz! Se la sua Wheels of Zeus (notato l’acronimo?) farà anche solo un cinquantesimo di quello che ha fatto Apple, vivremo tutti molto meglio.

<link>Lucio Bragagnolo</link>lux@mac.com