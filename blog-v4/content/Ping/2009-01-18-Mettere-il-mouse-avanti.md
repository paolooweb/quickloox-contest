---
title: "Mettere il mouse avanti"
date: 2009-01-18
draft: false
tags: ["ping"]
---

Non ho ancora acquistato iWork '09 e neanche installato la versione di prova.

Tuttavia mi è già successo di essere invitato a commentare e annotare un progetto su <a href="http://www.apple.com/it/iwork/iwork-dot-com/" target="_blank">iWork.com</a>.

Semplice, davvero utile e funziona. La lezione di MobileMe sembra essere servita.