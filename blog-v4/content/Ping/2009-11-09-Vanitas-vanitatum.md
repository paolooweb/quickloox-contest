---
title: "Vanitas vanitatum"
date: 2009-11-09
draft: false
tags: ["ping"]
---

Occupandomi di Wikipedia per un cliente, mi sono reso conto che la pagina dedicata al sottoscritto è stata cancellata.

Ho scoperto ora che è stata cancellata a marzo, il che spiega abbondantemente il mio interesse per la materia.

Voglio solo rassicurare il cliente; proprio perché era dedicata a me, non ci ho mai scritto neppure una virgola. :-P