---
title: "Le parole sono bigrammi"
date: 2011-01-13
draft: false
tags: ["ping"]
---

Assolutamente affascinanti <a href="http://www.chrisharrison.net/projects/wordassociation/index.html" target="_blank">questi grafici</a> che mostrano la frequenza di utilizzo su web di due concetti opposti (per esempio, pace e guerra) e di tutti i concetti intermedi che li separano, al tempo stesso congiungendoli.

Nella pagina ci sono tutti i <i>link</i> per approfondire. Per parte mia credo che se si litigasse meno sugli standard e si collaborasse di più sulla comprensione automatica del linguaggio potremmo assistere a progressi straordinari, ancora più di questi.