---
title: "Regali regali"
date: 2007-02-27
draft: false
tags: ["ping"]
---

Ho ricevuto un regalo su iTunes e l'ho estremamente gradito (non rigrazio pubblicamente solo per pudore). Mi sono sentito come un (piccolo) re.

Mi è capitato di ricevere regali su Amazon. Molto graditi anche quelli, però non è stata la stessa cosa e non ho avuto la stessa sensazione.

Alla fine, se c'è una cosa che Apple ha azzeccato con tutta la faccenda di iPod, è mettere l'accento sulla musica. È come occuparsi del proprio cuore. È proprio parte di noi.