---
title: "Estremo al quadrato"
date: 2003-03-05
draft: false
tags: ["ping"]
---

Perché AirPort Extreme può aumentare vertiginosamente il valore di un network

AirPort Extreme fornisce connessione wireless, come faceva AirPort. Sì, con più banda. Sì, si può attaccare una stampante alla base e condividerla su tutta la rete e prima invece no. Ma sono evoluzioni, non rivoluzioni.

Una delle funzioni più interessanti di AirPort Extreme viene però trascurata da molti. In una situazione di vicinanza tra più reti locali, una delle basi AirPort Extreme può essere configurata come bridge e realizzare così l’interconnessione tra le reti in questione.

Rispetto ad AirPort questo sì che è un vero grande salto. Il valore di una rete equivale al quadrato dei nodi che la compongono e, se più reti improvvisamente diventano una sola più grande, il loro valore decolla. Tutto con una base che costa meno di quella di prima.

<link>Lucio Bragagnolo</link>lux@mac.com