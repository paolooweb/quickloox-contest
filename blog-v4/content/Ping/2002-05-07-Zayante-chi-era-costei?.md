---
title: "Zayante, chi era costei?"
date: 2002-05-07
draft: false
tags: ["ping"]
---

A volte Apple fa grandi mosse dando loro piccolo risalto

Per chi leggesse solo riviste poco aggiornate o siti Web scandalistici, Apple ha usato una minima parte delle proprie disponibilità liquide per acquistare Zayante.
Zayante è probabilmente il migliore produttore di prodotti Ieee 1394: vale a dire FireWire.
Se Apple farà buon uso dei propri acquisti arriverà prima e meglio a incorporare FireWire 2 nei Mac del futuro, e darà nuovo impulso alla diffusione del bus dati più veloce che sia stato finora portato sul mercato.
Insomma, buona apertura. Adesso resta solo da sperare che le prossime mosse della partita siano altrettanto ben studiate.

<link>Lucio Bragagnolo</link>lux@mac.com