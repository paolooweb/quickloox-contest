---
title: "O cavo, cavo cavo, cavo cavo*&hellip;<p>"
date: 2004-12-08
draft: false
tags: ["ping"]
---

L&rsquo;attenzione di Apple ai particolari&hellip; e agli accessori<p>

Ho appena aperto, per curiosità, la scatola di un iPod photo, e ho constatato la presenza di un cavo video. Sembra ovvio, a posteriori, dato che iPod photo è previsto anche per proiettare uno slideshow.<p>

Ma il cavo video è proprio bello. Non comprerei un iPod photo a minimo 559 euro per avere il cavo, ma insomma, persino quello dà soddisfazione.<p>

<em>*Elio e le Storie Tese. Hanno un iPod ciascuno.</em><p>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>