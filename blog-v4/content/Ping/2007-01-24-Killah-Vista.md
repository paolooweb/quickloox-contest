---
title: "Killah Vista"
date: 2007-01-24
draft: false
tags: ["ping"]
---

<a href="http://www.accomazzi.it" target="_blank">Akko</a> mi ha linkato gentilissimamente a una serie di documenti decisamente interessanti, riguardanti Windows Vista e l'obbligo per tutto il software che agisce a livello di kernel di Vista stesso di ricevere una signature, in pratica una autorizzazione preventiva a funzionare da parte di Vista stesso.

E se a Vista non piace quel certo pezzo di software, perché è nuovo e non ancora certificato, o perché è scritto per fare qualcosa di diverso da quello che ha deciso Microsoft, Vista lo termina a capriccio suo e l'applicazione che lo usa si arrangia.

Come diceva quello, se non è vera, è ben trovata. Il lunghissimo <a href="http://download.microsoft.com/download/9/c/5/9c5b2167-8017-4bae-9fde-d599bac8184a/KMCS_Walkthrough.doc" target="_blank">documento ufficiale di Microsoft</a> (occhio, arrivano due mega di .doc) devo ancora trovare il tempo di leggerlo; intanto si possono leggere una <a href="http://www.neowin.net/index.php?act=view&amp;id=32130" target="_blank">notizia sintetica</a>, con numerosi commenti a contorno, una <a href="http://www.cs.auckland.ac.nz/~pgut001/pubs/vista_cost.html" target="_blank">analisi dettagliata</a> ma ancora leggibile e un <a href="http://www.msfn.org/board/index.php?showtopic=89464" target="_blank">parere opposto</a> presente sui forum di Microsoft Software Forum Network.

Comunque vada, è una bella avventura. Se penso che nel mondo Mac si litiga intorno al fatto che qualche pezzo di sistema operativo sia o non sia completamente esposto al mondo a livello di codice sorgente, direi che con il mio Mac mi trovo nel migliore dei mondi possibili.