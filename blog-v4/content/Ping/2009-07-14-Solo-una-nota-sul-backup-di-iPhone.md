---
title: "Solo una nota sul backup di iPhone"
date: 2009-07-14
draft: false
tags: ["ping"]
---

Credo che tre quarti delle problematiche di backup di iPhone e iPod touch espresse su forum e mailing list si risolvano con questa <a href="http://support.apple.com/kb/HT1766?viewlocale=it_IT" target="_blank">nota di supporto</a> di Apple.