---
title: "La forza di gravatàr"
date: 2009-05-09
draft: false
tags: ["ping"]
---

Gradita sorpresa dei tecnici di Nuov@ Periodici, che hanno aggiornato il motore di Ping! Grazie mille, pubblicamente.

Qualcuno mi ha già chiesto come fare ad avere la <em>faccina</em> nei commenti. Bisogna aprire un <em>account</em> (e l&#236; provvedere a caricare la <em>faccina</em>) su <a href="http://www.gravatar.com" target="_blank">Gravatar</a>.