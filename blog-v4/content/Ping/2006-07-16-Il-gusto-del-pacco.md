---
title: "Il gusto del pacco"
date: 2006-07-16
draft: false
tags: ["ping"]
---

La vecchia base AirPort è giunta a fine vita e l'ho sostituita con una AirPort Express. Non è il package di iPod, ma ad aprirlo dà ugualmente soddisfazione, per la cura e l'ingegno, per lo stile del tutto, insomma, mica male.

Ignoro quanto del prezzo serva a pagare chi ha studiato l'imballaggio di <a href="http://www.apple.com/it/airportexpress/" target="_blank">AirPort Express</a>. Vale la pena, però.