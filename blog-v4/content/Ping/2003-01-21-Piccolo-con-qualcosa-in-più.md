---
Ètitle: " piccolo ma ha qualcosa in più"
date: 2003-01-21
draft: false
tags: ["ping"]
---

Dettagli da sapere sul PowerBook 12”

Erik Stannow, Product Marketing Director di Apple Emea (Europa, Mediterraneo e Africa), sta mostrando alcuni dettagli del nuovo PowerBook G4 12”.

È sensibilmente più piccolo di un iBook. È anche più leggero di un iBook.

Sotto la tastiera nasconde un altoparlante dedicato ai bassi in modo che la nuova posizione degli altoparlanti standard (ottimi per gli alti ma non per i bassi) non sacrifichi la qualità dell’audio.

A parità di prestazioni, pesa clamorosamente meno del più recente concorrente di Sony.

È fabbricato in alluminio che, a differenza del titanio, non ha bisogno di verniciatura. Quindi è pressoché insensibile dal punto di vista estetico ai graffi e ai maltrattamenti.

Non fa ancora il caffè. Ma non ci conto. Per ora, chiaro.

<link>Lucio Bragagnolo</link>lux@mac.com