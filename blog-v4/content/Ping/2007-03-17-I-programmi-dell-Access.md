---
title: "I programmi dell'Access"
date: 2007-03-17
draft: false
tags: ["ping"]
---

L'amico <strong>Jida</strong> mi ha chiesto come interfacciarsi a un database Access attraverso OpenOffice.

Non ho saputo che dirgli. Sul sito OpenOffice ovviamente <a href="http://user-faq.openoffice.org/faq/ar01s10.html#id2875982" target="_blank">c'è una Faq apposita</a>, che porta con sé una serie di dettagli piuttosto impegnativa da approfondire per me in questo momento.

Mi ricordo inoltre che, durante un incontro in Mac@Work, il Venerandi in persona aveva dichiarato di averci sudato un po' sopra, ma di avere risolto il problema. Ma ricordo solo questo. :-)

Se qualcuno aiuta a mettere insieme i pezzi del puzzle, Jida ne sarà grato. E io con lui, chiaro.