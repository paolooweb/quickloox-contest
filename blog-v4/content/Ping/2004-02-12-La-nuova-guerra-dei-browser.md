---
title: "La nuova guerra dei browser"
date: 2004-02-12
draft: false
tags: ["ping"]
---

Non ci sono guerre giuste, dirà qualcuno. Nel mondo dei browser invece sì

Qualcuno ricorderà la Prima guerra dei browser, quella in cui Netscape conduceva un business e Microsoft l’ha buttata fuori dal mercato con mezzi illegittimi e abusando del suo monopolio.

Nel mondo Windows quella guerra si è conclusa con una dittatura. Poca e difficile libertà di scelta e, per i coraggiosi che osano, il rischio che un sospiro di Microsoft renda inservibile il browser alternativo.

Ora è cominciata la Seconda guerra dei browser, ma l’esito sarà diverso. Perché, a differenza del codice di Explorer, il motore Html di Safari è open source e tutti gli sviluppatori possono usarlo. Così, per esempio, OmniWeb, che aveva una stupenda interfaccia ma un pessimo motore, è tornata a sfidare Safari con OmniWeb 5. Entrambi possono usare lo stesso motore e sfidarsi sul piano delle funzioni e della facilità di uso.

E arriveranno altri contendenti, come Firefox, per esempio. Ma è ancora presto per raccontare questa storia, mentre una previsione posso farla subito: la Seconda guerra dei browser avrà solo vincitori, e tanti. Tutti noi.

<link>Lucio Bragagnolo</link>lux@mac.com