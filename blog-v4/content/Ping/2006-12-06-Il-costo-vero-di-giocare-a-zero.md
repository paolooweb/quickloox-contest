---
title: "Il costo vero di giocare a zero"
date: 2006-12-06
draft: false
tags: ["ping"]
---

È unicamente quello di rintracciare i giochi per Mac belli e appassionanti eppure gratuiti. Ce n'è per tutti i gusti, dai miei giochi di ruolo agli arcade prediletti da <strong>Piergiovanni</strong>.

<strong>Paolo</strong> mi ha segnalato questa eccellente pagina di <a href="http://forum.macbidouille.com/index.php?showtopic=89760" target="_blank">MacBidouille</a>.

Io ci metto di mio la directory giochi di <a href="http://www.freesmug.org/review/games" target="_blank">Freesmug</a> (il bravissimo Gand non riceve mai abbastanza elogi per tutto il lavoro che svolge).

Sai qual è il bello? Che la lista totale è ben lungi dall'essere completa. Scovare altri titoli è un gioco nel gioco. Manca Angband, manca Planeshift&#8230;