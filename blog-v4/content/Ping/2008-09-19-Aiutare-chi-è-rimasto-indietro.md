---
title: "Aiutare chi è rimasto indietro"
date: 2008-09-19
draft: false
tags: ["ping"]
---

Manca qualche giorno al mio ritorno dalle vacanze, periodo in cui per ragioni di connettività mi astengo dai <em>download</em> pesanti.

Nel giro di due settimane ho inserito nei promemoria del software da scaricare iTunes 8, iPhone 2.1, Mac OS X 10.5.5, QuickTime 7.5.5, i nuovi <em>driver</em> per stampanti Hewlett-Packard. Non mi toccano, ma ci sono un Security Update per Mac OS X 10.4.11, una nuova versione di Apple Remote Desktop e una nuova versione di WebObjects. Mi tocca, per quanto pochissima cosa, l'aggiornamento di Remote per iPod touch e iPhone. Aggiungo alla lista, pur ignorandone il significato, Pro Applications Update, Final Cut Server Update e iPhone Configuration Utility.

Certamente si tratta di una coincidenza. Però, quando leggo che Apple ha problemi di sviluppo software per la troppa carne al fuoco, mi chiedo quanta roba dovrei scaricare se lo sviluppo software invece prosperasse.