---
title: "A magra consolazione"
date: 2009-12-24
draft: false
tags: ["ping"]
---

Non so se questo Ping verrà pubblicato; dal 21 dicembre il motore indica <i>programmazione mancante</i> per ogni cosa che avrebbe dovuto andare in onda e la programmazione, invece, c'è.

Solo, si sappia che non è stata colpa mia. :-)