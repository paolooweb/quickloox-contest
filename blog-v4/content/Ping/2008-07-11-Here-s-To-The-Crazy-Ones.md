---
title: "Here's To The Crazy Ones"
date: 2008-07-11
draft: false
tags: ["ping"]
---

<cite>Questo film lo dedichiamo ai folli.
Agli anticonformisti, ai ribelli, ai piantagrane&#8230;</cite>

Sono arrivati prima quelli di <a href="http://www.tuaw.com/2008/07/10/heres-to-the-crazy-ones-thanks-to-the-unofficial-iphone-develo/" target="_blank">The Unofficial Apple Weblog</a>, ma non gli rubo niente; stavo masticando un po' gli stessi pensieri.

Adesso che c'è il dannatissimo iPhone 3G che ha mandato Apple persino sulla Gazzetta dello Sport e sui telegiornali di prima serata. Adesso che c'è il nuovo software di sistema. Adesso che c'è un sistema di distribuzione ufficiale del software. Adesso che scrivere software è attività accettata e supportata attivamente da Apple. Adesso che la biblioteca di programmi ufficiali disponibili per iPhone diventerà smodatamente grande&#8230;

&#8230;tutti quei pazzi, spesso ragazzini, che per un anno hanno creato software non ufficiale, scritto alla cieca senza documentazione, su una piattaforma che era fatta per non fare girare programmi, dentro una partizione troppo piccola, appoggiato su un accrocchio software di inaudita ingegnosità che viene da chiedersi come faccia a funzionare veramente, ogni volta mettendo a rischio hardware da centinaia di dollari e di euro.

Grazie.