---
title: "Nel dubbio"
date: 2010-07-29
draft: false
tags: ["ping"]
---

<i>Nel dubbio usa Caslon</i>, si dicevano i tipografi inglesi e americani del diciottesimo secolo: il font migliore per risolvere le incertezze, una soluzione che, direbbero certi oggi, va con tutto.

Caslon in realtà è stato William Caslon, tipografo morto nel 1766 dopo avere pubblicato non un font con il suo nome &#8211; non erano ancora i tempi &#8211; ma una gamma completa di caratteri tondi e corsivi dallo stile inimitabile.

Sufficientemente inimitabile da fare s&#236; che Adobe, oggi, venda un Caslon piacevole e adatto a un'ampia serie di impieghi tipografici. Solo che a guardarlo non è Caslon neanche un po'.

Non abbastanza inimitabile perché un font designer più bravo di quelli di Adobe, William Berkson, non possa essere arrivato a studiare gli esempi storici di Caslon e le sue riprese avvenute nel corso dei secoli per dare vita a una nuova incarnazione del Caslon per il XXI secolo. Una che di Caslon non porti solo il nome.

Berkson ha scritto <a href="http://ilovetypography.com/2010/07/26/reviving-caslon-the-snare-of-authenticity/" target="_blank">un pezzo affascinante</a> &#8211; anche da guardare &#8211; sul lavoro fatto e The Font Bureau mostra <a href="http://www.fontbureau.com/fonts/WilliamsCaslonText/" target="_blank">un campione più che adeguato</a> a farsi un'idea del risultato.

Nel dubbio, e se il lavoro giustifica il prezzo ahimé, butta via quella schifezza di Adobe e usa Caslon.