---
title: "Ipersensibilità"
date: 2009-02-18
draft: false
tags: ["ping"]
---

Il cavo dell'alimentatore del PowerBook G4 ha appena fatto cortocircuito nel suo noto punto debole, quello dove si salda al corpo dell'alimentatore stesso. Fumatina, visione del rame rovente attraverso la plastica e passaggio sollecito a un alimentatore di emergenza.

Ho capito il perché aprendo la posta: nello stesso momento è avvenuta la spedizione del MacBook Pro 17&#8221;, che arriverà <cite>entro e non oltre il 25 febbraio</cite>.

L'alimentatore lo ha saputo e per l'emozione non ha retto.

Per conoscenza sull'andamento dei tempi, metti mai che fossimo in più di due ad avere ordinato un 17&#8221;.