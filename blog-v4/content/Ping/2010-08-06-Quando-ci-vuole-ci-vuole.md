---
title: "Quando ci vuole ci vuole"
date: 2010-08-06
draft: false
tags: ["ping"]
---

Dedicato a quelli che tanto i computer sono tutti uguali, tanto dentro ci sono gli stessi componenti, tanto quello che costa meno va sempre bene e gli altri costano di più per fregarti.

Samsung Electronics e Lg Electronics, secondo e terzo produttore mondiale di cellulari, hanno annunciato l'assunzione di esperti di design, rispettivamente Jung Ji-hong, docente di Visual Communication Design all'università di Kookmin, come <i>vice president</i> del Mobile Design Group, e Lee Kun-pyo, docente all'Istituto coreano per le tecnologie avanzate e decano della facoltà di Industrial Design, in qualità di <i>executive vice president</i> del Design Management Center Lg.

Al candidato è richiesto di spiegare con parole proprie la possibile ragione del potenziamento dei reparti di <i>design</i> di due <i>leader</i> in un campo nel quale, da tre anni, nel bene e nel male, l'obiettivo è sempre puntato su iPhone, che prima nemmeno c'era.