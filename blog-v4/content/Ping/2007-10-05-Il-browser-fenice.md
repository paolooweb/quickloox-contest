---
title: "Il browser-fenice"
date: 2007-10-05
draft: false
tags: ["ping"]
---

E adesso è tornato persino <a href="http://browser.netscape.com/" target="_blank">Netscape</a>.

L'effetto è un po' quello di Firefox, però funziona e ha qualche cosetta interessante. A parte il miniwidget incorporato per avere le previsioni del tempo.