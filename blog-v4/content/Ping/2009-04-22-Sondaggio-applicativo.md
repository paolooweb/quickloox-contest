---
title: "Sondaggio applicativo"
date: 2009-04-22
draft: false
tags: ["ping"]
---

Per purissima e pigra curiosità: quanti elementi hai nella cartella Applicazioni e nella cartella Utility?

Io sono rispettivamente a 125 e 41.

Guarda giusto il numero di elementi senza perdere tempo in distinguo (lo so che un elemento di Applicazioni è la cartella Utility e non un'applicazione e chissenefrega).

Grazie in anticipo!