---
title: "Un guasto al gusto"
date: 2008-11-17
draft: false
tags: ["ping"]
---

Vedo scorrere entusiasmo rispetto a <a href="http://www.livescribe.com" target="_blank">Pulse</a> e all'annunciata beta per Mac del software. A me sembra la vaccata dell'anno, bellissimo <em>gadget</em> per divertirsi un sacco e fare scena, sommamente inutile.

L'idea di registrare l'audio e collegarlo agli scarabocchi che fai sul foglio è bellissima, a sentirla. Poi scopri che l'aggeggio usa una carta speciale, per cominciare. Ti voglio vedere, non tanto a creare il <em>pencast</em> della lezione, o della riunione; a usarlo dopo. Scrivi con una mano alla tastiera e con l'altra alla penna? O posi la pena e stai sentire l'audio, che a quel punto tanto vale avere un qualunque registratore audio che costa un terzo?

Sono sempre ansioso di vedere nuova tecnologia. Purché non nasca per accelerare la soddisfazione. Si rovina il risultato. Si perde il gusto.