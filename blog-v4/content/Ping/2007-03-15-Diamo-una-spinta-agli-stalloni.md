---
title: "Sproniamo gli stalloni"
date: 2007-03-15
draft: false
tags: ["ping"]
---

Dei due ne conosco solo uno, gran brava persona e per giunta bravo Mac user. Mi sento di garantire sulla serietà e sul buon esito della <a href="http://web.mac.com/columdonnelly/iWeb/Colum%20%3A-%29/Blog/69CB1377-E34C-49AE-8364-7328C312A819.html" target="_blank">raccolta fondi</a>, in un panorama dove è legittimo dubitare anche degli insospettabili, e non è poco (per me). :-)