---
title: "Un ritorno per Tiger<p>"
date: 2005-08-24
draft: false
tags: ["ping"]
---

Fine dell&rsquo;incompatibilità tra Mac OS X e uno dei suoi antivirus<p>

Dal 29 agosto sarà di nuovo disponibile <a href="http://www.mcafeesecurity.com/it/products/mcafee/smb/antivirus/virex_smb.htm">McAfee Virex</a> compatibile Mac OS X. O meglio, lo è sempre stato, solo che il programma non funzionava più con Tiger, al punto che Apple lo ha tolto dalla dotazione di base del servizio .Mac.<p>

Ora ritorna e si può solo esserne contenti.<p>

Non sto dicendo che tornerà anche su .Mac. Apple non ha dichiarato niente e non si sono ragioni evidenti per cui dovrebbe avvenire.<p>

Se siamo fortunati, basterà questa frase per fare partire un telefono senza fili pazzesco sui siti-spazzatura.<p>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>