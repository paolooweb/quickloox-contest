---
title: "Solo più grosso"
date: 2010-04-05
draft: false
tags: ["ping"]
---

Ancora sul rumoroso lancio di iPad, per mettere l'accento su qualcosa che non si leggerà facilmente altrove.

Craig Hockenberry, autore di Twitterrific, ha confrontato le p<a href="http://furbo.org/2010/04/03/benchmarking-in-your-lap/" target="_blank">restazioni di elaborazione pura</a> di iPad con quelle di iPhone 3GS. iPad è quasi due volte più veloce.

Rispetto a un iPhone originale (del 2007, mica della nonna), la superiorità di prestazioni di iPad varia in misura notevole, che però va da 8,75x a 428x.

Chi ha definito spregiativamente iPad <i>un iPhone più grosso</i> si è dimenticato di aggiungere <i>e un bel po' più veloce</i>.