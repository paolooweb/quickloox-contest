---
title: "Azione dimostrativa"
date: 2006-01-11
draft: false
tags: ["ping"]
---

Ci sar&agrave; tempo per dissezionare fino all&rsquo;ultimo chip MacBook Pro. La prima impressione che ho, a caldo ma dopo qualche ora di riflessione, &egrave; che si tratti di un&rsquo;azione dimostrativa.

Mentre iMac &egrave; effettivamente la gamma di macchine scelta per iniziare la transizione (sorry, siti di rumor. Notato che, tra l&rsquo;uno e l&rsquo;altro, sono state candidate a iniziare la transizione <em>tutte</em> le macchine? Come gli oroscopi, in qualche modo ci si prende sempre&hellip;), MacBook Pro serve ad Apple per mostrare che la tecnologia &egrave; pronta, che la stasi causata dal mancato (e mai esistito) G5 &egrave; terminata, che un Mac portatile con tecnologia Intel &egrave; competitivo eccetera.

Non darei troppo peso alle prime notizie, che non sono notizie ma sciocchezze. Sapere che l&rsquo;autonomia dei prototipi in mostra all&rsquo;Expo dia due ore non serve a niente. Potrebbe essere, oppure no. Sono prototipi.

Prosegue l&rsquo;eliminazione delle tecnologie secondarie. Non c&rsquo;&egrave; pi&ugrave; il modem esterno. Non c&rsquo;&egrave; pi&ugrave; la porta S-video. Non c&rsquo;&egrave; pi&ugrave; FireWire 800. Non c&rsquo;&egrave; pi&ugrave; la porta Pcmcia, sostituita da una tecnologia nuova. Ma sembra avere un senso. &Egrave; dal 1997 che non uso la porta modem; certamente ci sar&agrave; chi ne ha bisogno, ma non &egrave; pi&ugrave; indispensabile.

Chi parla di abbandono di FireWire da parte di Apple, perch&eacute; manca la porta FireWire 800, non sa quello che dice.

E tu che ne pensi? Facciamo un bel giro di commenti sul MacBook Pro?