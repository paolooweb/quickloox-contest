---
title: "Un'occasione da perdere"
date: 2010-09-20
draft: false
tags: ["ping"]
---

Per oggi, fino alle prime ore della notte, l'antispyware <a href="http://macscan.securemac.com/" target="_blank">MacScan</a> è gratis, anziché a 29,99 dollari.

Basta scaricare e installare la versione di prova, per poi &#8220;comprare&#8221; l'applicazione inserendo il codice-coupon PIRATE. Il prezzo cambia da 29,99 dollari a zero.

Una meravigliosa occasione di cui, basta avere un minimo di testa sulle spalle, è possibile non approfittare. Mac è assai più sicuro.