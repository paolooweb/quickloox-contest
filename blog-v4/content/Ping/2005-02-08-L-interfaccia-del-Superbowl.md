---
title: "Per una volta la tv<p>"
date: 2005-02-08
draft: false
tags: ["ping"]
---

Spunti da cogliere per gli interessati di interfaccia umana<p>

Normalmente la televisione insegna ben poco, a parte qualche documentario sulla natura selvaggia e i deliri parauniversitari della notte profonda.<p>

Ma ogni regola ha la sua eccezione. Spero che chi lavora in Apple sulle <a href="http://developer.apple.com/documentation/mac/HIGuidelines/HIGuidelines-2.html">interfacce umane</a> abbia deciso di guardarsi in tv il Superbowl, la finalissima del campionato di football americano. Chi ha pensato la sovraimpressione a commento delle azioni ha molto da insegnare.<p>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>