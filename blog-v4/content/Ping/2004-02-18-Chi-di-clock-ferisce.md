---
title: "Chi di clock ferisce"
date: 2004-02-18
draft: false
tags: ["ping"]
---

La propaganda sleale nasconde i dati imbarazzanti. Per esempio Intel...

Il mito che la velocità del processore dipenda dal suo valore di clock è una stupidaggine che il marketing di Intel è riuscito a inculcare nei cervelli delle persone non attente. O almeno in tutti quanti pensano che la velocità di una automobile dipenda strettamente dal numero di giri del motore.

I nuovi processori G5 di Mac hanno clock piuttosto alti, che vanno verso i due gigahertz. Sembra ancora poco rispetto a un Pentium. Ma i soliti approssimativi non contano che G5 è un processore a 64 bit e Pentium, invece, va a 32.

Intel ha un processore a 64 bit; si chiama Itanium. Ora, prova a recuperare le informazioni su Itanium dal sito Intel, per scoprire che clock ha.

Constaterai che questa informazione non è immediata da trovare. Un malizioso direbbe perfino che è ben nascosta. La sorpresa è che Itanium tocca 1,25 gigahertz, molto meno di G5.

Sai che questo valore non è strettamente legato alla velocità, ma il tuo amico so-tutto con il suo Windows a casa non lo sa mica. Faglielo notare.

<link>Lucio Bragagnolo</link>lux@mac.com