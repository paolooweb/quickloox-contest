---
title: "Buona San(e)ità"
date: 2009-04-12
draft: false
tags: ["ping"]
---

Ho uno scanner Canoscan N1220U talmente vecchio che la ricerca sul sito Canon italiano non lo mostra e quella <a href="http://www.usa.canon.com/consumer/controller?act=ModelInfoAct&amp;tabact=DownloadDetailTabAct&amp;fcategoryid=351&amp;modelid=8007" target="_blank">sul sito americano</a> non mostra <a href="https://web4.hardwarezone.com/articles/view.php?cid=13&amp;id=208&amp;pg=2" target="_blank">l'immagine del prodotto</a>.

Per le esigenze di casa e lavoro è sempre stato più che adeguato. Canon fornisce un <em>driver</em> sotto forma di <em>plugin</em> funzionante per Photoshop e gli altri programmi compatibili con l'architettura Twain, ma sul mio MacBook Pro non intendo installare Photoshop (dopo essermi purificato dalle schifezze Microsoft ora sto lavorando per liberarmi da quelle Adobe). Unico problema, sui PowerBook precedenti non ero mai riuscito a fare funzionare il plugin con altre applicazioni, tipo <a href="http://www.graphicconverter.com" target="_blank">GraphicConverter</a>. Sostanzialmente eseguivo le scansioni con l'iMac di <em>backup</em>, dove una vecchissima versione <em>beta</em> di Photoshop 4 (!) era perfetta per fare funzionare il <em>plugin</em> e poi trasferivo i file da un Mac all'altro.

Oggi l'iMac di <em>backup</em> ha cominciato a dare problemi e dovevo effettuare una manciata di scansioni. Ho deciso che con l'avvento del MacBook Pro potevo tentare di rompere il tabù dello scanner.

Ho scelto un approccio diverso e, invece che riscaricare per l'ennesima volta il <em>plugin</em> di Canon e per l'ennesima volta verificare se funzionasse con GraphicConverter, ho scommesso sull'esistenza di <em>driver open source</em> per vecchi scanner.

Ho vinto, con il <a href="http://www.sane-project.org/" target="_blank">progetto Sane</a>. Sono ancora indietro con le sue interfacce grafiche, ma GraphicConverter vede il <em>driver</em> e permette una ragionevole facilità di operazione. Acquisizione Immagine apparentemente ha qualche problema, su cui mi riprometto di approfondire. Nel Terminale si è aggiunto un comando <code>scanimage</code>, come sempre assai più scomodo e più completo dell'interfaccia grafica.

Il tabù è stato infranto. Photoshop resta nel cassetto. Missione compiuta.

<em>Aggiornamento</em>: come segnalato da <a href="http://www.teleart.it/sito/home.html" target="_blank">Andrea Ack</a>, avevo parlato di Sane già grazie a <a href="http://www.macworld.it/blogs/ping/?p=1210" target="_self">Pierfausto</a>. Ogni tanto basterebbe che io mi rileggessi. :-)