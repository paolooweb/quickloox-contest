---
title: "Arrivano i mostri"
date: 2006-07-03
draft: false
tags: ["ping"]
---

Ma possiamo difenderci, come mi segnala <strong>Mario</strong>, quanto meno con l'ironia e la sana cattiveria di <a href="http://www.riotgames.se/riotgames-se/index.html" target="_blank">WindowS InvaderS</a>.

Il gioco non è gran che, avviso subito. D'altronde neanche Windows.