---
title: "Ci vuole un minuto"
date: 2008-07-28
draft: false
tags: ["ping"]
---

Come è noto sono un estimatore di iTunes U. Quando si parla di iTunes Store sono tutti molto pronti a sparare a zero sulla musica onnipresente e sulla gente che si rincoglionisce auricolari in testa.

Quella gente, peraltro, potrebbe avere in testa non l'ultimo dei rockettari ma una delle 60 Seconds Lectures della University of Pennsylvania. (iTunes -> iTunes U -> University of Pennsylvania -> School of Arts and Sciences -> 60 Second Lectures - Video)

Provare per credere. Non è vero che durano un minuto; alcune anche quasi due minuti e mezzo. Eppure si può imparare qualcosa anche in cento secondi. Poi magari viene anche voglia di provare qualcosa lungo cento minuti. E improvvisamente cambia la prospettiva dell'istruzione personale.

Mi chiedo sempre perché su iTunes U non si possa vedere produzione regolare del Politecnico di Milano, o della Normale di Pisa, solo due nomi a caso. Non mi so rispondere, ahimé.