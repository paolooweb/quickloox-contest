---
title: "Otto e mezzo"
date: 2006-09-09
draft: false
tags: ["ping"]
---

<a href="http://www.barebones.com/products/bbedit/" target="_blank">BBEdit</a> era uno straordinario tuttofare. Con la nuova versione 8.5 è diventato una vera macchina da guerra per il trattamento del testo, lo sviluppo web e la programmazione.

Non può né deve servire a tutti. Roba professionale. Al professionista cui serve, però, costa di più non comprarlo.