---
title: "Cammina cammina"
date: 2008-11-26
draft: false
tags: ["ping"]
---

Ho un impegno immediatamente prima della pizzata-Ping di stasera e quindi chi arriverà in Mac@Work in anticipo potrebbe non trovarmi. È tutto sotto controllo; per le 19:30, massimo 19:45, arriverò più che regolarmente.

A stasera!

P.S.: chi avesse desideri speciali su un libro da ricevere in regalo (che so, Php, Xml, crittografia, Unix, fantascienza) può provare a manifestarlo. Ovviamente non garantisco che nella mia piccola biblioteca ci sia esattamente qualcosa di quel tipo. O che non sia scritto in inglese. :)