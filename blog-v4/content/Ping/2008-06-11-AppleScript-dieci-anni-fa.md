---
title: "AppleScript, dieci anni fa"
date: 2008-06-11
draft: false
tags: ["ping"]
---

Undici, per la precisione. Questo <a href="http://www.colepapers.net/TCP.archive/Cole_Papers_97/TCP_97_10/tvlistings.HTML" target="_blank">vecchio articolo</a> descrive per sommi capi come faceva (e fa ancora, credo) il Los Angeles Times a pubblicare settimanalmente 26 edizioni diverse della programmazione televisiva senza muovere un dito e con margine di errore praticamente zero.

Tutto fatto con AppleScript. Nell'articolo ci sono anche alcuni pezzi del codice che fa impaginare automaticamente XPress.

Il bello è che, a sentire quelli che ne capiscono, Apple nel 1997 era spacciata. Probabilmente il Los Angeles Times sarebbe andato avanti lo stesso fino a fine vita delle macchine.