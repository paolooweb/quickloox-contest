---
title: "Uno per tutti, tutti su Internet"
date: 2009-02-11
draft: false
tags: ["ping"]
---

Dal 10 marzo Ccp Games non <a href="http://myeve.eve-online.com/devblog.asp?a=blog&amp;bid=625" target="_blank">supporta più Linux</a> sul loro gioco di massa <a href="http://www.eve-online.com/" target="_blank">Eve Online</a>. Non ci sono abbastanza giocatori a giustificarlo. Il supporto per Mac, arrivato insieme a quello Linux, continua. I giocatori continuano a crescere.

Il difetto sta però nel manico. Ccp Games ha realizzato Eve Online solo per Windows, spendendo soldi. Poi ha voluto portarlo su Mac e Linux dovendo usare la tecnologia Cider di Transgaming, dato che il gioco era nato solo per Windows. E ha speso soldi.

Adesso si sono accorti che i soldi spesi su Linux non valgono la candela. Cioè li hanno sprecati.

Avessero scritto da subito per tutti, il gioco funzionerebbe per tutti. E avrebbero speso meno, facendo contenti tutti.

Se fai un gioco per Internet, fai un gioco per tutti. Altrimenti spendi di più e sprechi pure.