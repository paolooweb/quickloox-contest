---
title: "A pensar male"
date: 2010-11-19
draft: false
tags: ["ping"]
---

Approdo solo ora alla scoperta che oltre due settimane fa il Venerd&#236; di Repubblica <a href="http://rassegna.camera.it/chiosco_new/pagweb/pdf/rad2CD16.tmp.pdf" target="_blank">ha pubblicato un trafiletto</a> riguardante un partito italiano ai cui cento e passa senatori verrà regalato un iPad in occasione del Natale, attingendo al contributo annuale di quattromila euro a disposizione di ogni parlamentare per apparecchiature elettroniche e informatiche.

È interessante il modo in cui il web, normalmente avido di <i>gossip</i> e <i>scoop</i>, dove qualsiasi voce e qualunque <i>si dice</i> viene rilanciato senza sosta, ha assorbito la notizia. La quale certamente è stata pubblicata: oltre al Pdf visibile nella rassegna stampa della Camera e linkato sopra, sul <a href="http://forum.spinoza.it/viewtopic.php?f=3&amp;t=18243" target="_blank">forum di Spinoza.it</a> è stata pubblicata una foto <a href="http://forum.spinoza.it/download/file.php?id=1317&amp;sid=f05f90420517f3aca4093b2013492755&amp;mode=view" target="_blank">che sembra decisamente autentica</a>.

Per quanto mi risulta, la notizia è stata riportata da iPadItalia.com. Ma ad andare sul sito, si trova che <a href="http://www.ipaditalia.com/tutti-i-senatori-del-pd-avranno-un-ipad-in-regalo-per-natale-26651.html" target="_blank">la pagina è stata cancellata</a>.

Altrimenti ho trovato solo siti-spazzatura, come <a href="http://smillamagazine.com/legge/www.ipaditalia.com/tutti-i-senatori-del-pd-avranno-un-ipad-in-regalo-per-natale-26651.html" target="_blank">Smilla Magazine</a>, <a href="http://www.apple-ipad-italia.it/ipad-italia/2010-11-19/tutti-i-senatori-del-pd-avranno-un-ipad-in-regalo-per-natale/" target="_blank">iPad Italia</a>, <a href="http://radioyoga.wordpress.com/2010/11/19/tutti-i-senatori-del-pd-avranno-un-ipad-in-regalo-per-natale-ipad-italia-%E2%80%93-il-blog-italiano-sullapple-ipad/" target="_blank">YogaNetwork</a>, <a href="http://www.mutantitv.net/wordpress/tutti-i-senatori-del-pd-avranno-un-ipad-in-regalo-per-natale-ipad-italia-%E2%80%93-il-blog-italiano-sullapple-ipad.html" target="_blank">iPad Italia</a>, <a href="http://www.bestnotizie.com/95203/tutti-i-senatori-del-pd-avranno-un-ipad-in-regalo-per-natale/" target="_blank">BestNotizie</a>, <a href="http://messynotes.tk/post/1616362056/tutti-i-senatori-del-pd-avranno-un-ipad-in-regalo-per" target="_blank">MessyNotes</a> e <a href="http://www.ilfazioso.com/siamo-contenti-nostri-soldi-senatori-pd-ricevono-ipad-regalo.html" target="_blank">Il Fazioso</a>.

A parte Il Fazioso, tutti linkano iPadItalia e dunque una pagina che non esiste più.

Ma il Venerd&#236; di Repubblica è davvero cos&#236; poco letto? E nessuno di quelli che lo leggono poi tiene un blog? E nessuno di quelli che lo leggono e tengono un blog ha un iPad?

Più ci penso e più mi viene da pensare male. È che ogni direzione di pensiero è peggio della precedente.