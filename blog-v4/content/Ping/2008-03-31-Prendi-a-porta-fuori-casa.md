---
title: "Prendi e porta fuori casa"
date: 2008-03-31
draft: false
tags: ["ping"]
---

Sta diventando rapidamente interessante l'offerta di <a href="http://www.powerfolder.com/" target="_blank">PowerFolder</a>, ennesimo sistema per trasferire file molto grandi via Internet.

Funziona anche sui sistemi operativi svantaggiati (Windows) e su Linux oltre che su Mac, e soprattutto è sia gratis che a pagamento. Sto cominciando a considerarlo concorrente possibile del mio attuale preferito, <a href="http://www.pando.com" target="_blank">Pando</a>.

I servizi di questo tipo, quando sono solo gratis, non funzionano o funzionano male o sono mal progettati.