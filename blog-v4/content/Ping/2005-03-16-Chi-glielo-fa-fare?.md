---
title: "Chi glielo fa fare?<p>"
date: 2005-03-16
draft: false
tags: ["ping"]
---

Il ragionamento logico viene sostituito da forze difficilmente comprensibili<p>

All&rsquo;ultima riunione di lavoro eravamo in due con il portatile. Il mio pesa tre chili, il suo almeno cinque; il mio schermo è perfetto per i Dvd e le fotografie, il suo è quadrato; il mio è spesso un pollice, il suo due; il mio ha consumato un quarto di batteria durante la riunione, mentre il suo ha avuto bisogno della corrente, e il mio alimentatore è uno scatolino discreto, il suo se fosse marrone potrebbe passare per un mattone.<p>

Potrei dirne molte altre, magari inutili. Per esempio il mio sa che orientamento ha nello spazio e parcheggia le testine del disco rigido in caso di caduta, il suo no; il mio ha Gigabit Ethernet, il suo no; il mio può pilotare un monitor da trenta pollici, il suo no; il mio si è svegliato dallo stop in mezzo secondo, il suo in un minuto pieno. Il mio ha la tastiera retroilluminata, il suo no. Ma il punto non è questo.<p>

Il punto è che, euro più euro meno, il mio e il suo costano uguale.<p>

Chi glielo fa fare?<p>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>