---
title: "La pagina che non c&rsquo;è<p>"
date: 2005-06-18
draft: false
tags: ["ping"]
---

A metà strada tra l&rsquo;easter egg e lo scoop da dimenticanza altrui<p>

Mi segnala Riccardo un vicolo buio del sito Apple italiano.<p>

Nella pagina italiana riservata alle <a href="http://www.apple.com/it/macosx/features/unix">caratteristiche Unix di Mac OS X</a>, nella colonna di destra, all&rsquo;altezza dell&rsquo;immagine <em>Come in It&rsquo;s Open</em>, c&rsquo;è un riquadro intitolato Informazioni tecniche, che contiene un link dal nome intitolato Ulteriori informazioni sulla base Unix.<p>

Dalla grafica e dal link sembrerebbe lo scaricamento di un file Pdf. Invece il link porta a una pagina contenente un elenco di Pdf dal sapore decisamente tecnico, con vari titoli che appaiono interessanti. L&rsquo;insieme lascia credere che non sia una pagina pensata per essere vista dal pubblico, o quanto meno che si siano dimenticati di darle uno stile.<p>

Perché tutto questo giro di parole? Perché il link funziona solo se vi si accede nel modo che ho detto. Farne un bookmark, o caricare direttamente quell&rsquo;Url in Safari, non funzionerà.<p>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>