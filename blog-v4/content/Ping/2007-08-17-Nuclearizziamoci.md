---
title: "Nuclearizziamoci"
date: 2007-08-17
draft: false
tags: ["ping"]
---

I venerd&#236; del gioco sono una sciocchezza che mi è venuta in mente cos&#236; e non hanno niente a che vedere con chi fa veramente sul serio. Come Ambrosia Software.

Dalle 19 di venerd&#236; 24 agosto alle 7 di luned&#236; 27 agosto (ore italiane), Defcon online (gioco strategico di guerra termonucleare globale) è gratis per tutti.

Si può <a href="http://www.everybody-dies.com/downloads/" target="_blank">scaricare la versione trial</a> e giocare in rete liberamente, senza le restrizioni previste di solito.

Ambrosia lo fa in occasione del Penny Arcade Expo di Londra. Io faccio eccezione alla mia autosospensione dei venerd&#236; del gioco per agosto e dico: non so quando e quanto, ma sarò dentro di sicuro.