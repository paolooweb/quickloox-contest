---
title: "Dvd et impara"
date: 2004-02-20
draft: false
tags: ["ping"]
---

Perché Apple complica le cose quando potrebbe farle semplici?

Il mio amico G., informatico di eccellenza, superesperto di Java, autorità nel gruppo di lavoro di Cocoon, che tutti gli anni vola negli Usa a vedersi ApacheCon, da quando è arrivato Mac OS X non vedeva l’ora di passare a Mac.

Lo ha fatto. Ha acquistato un fiammante PowerBook 15”, dotato non dei consueti Cd di sistema ma di un Dvd di sistema. Ottima cosa.

Peccato che, essendo un tecnico, reputi giustamente indispensabile l’installazione degli Xcode Tools. Con i Cd di sistema basta inserire il Cd giusto e avviare l’installazione. Con il Dvd, invece, bisogna avviare un software restore e, a detta di G., si capisce veramente poco.

Immagino che G. sarà stato distratto o sbadato e che le cose siano più semplici di come le fa lui. Ma se ha avuto problemi lui, mi immagino che può accadere a una persona meno preparata.

Cara Apple, avere un solo disco al posto di quattro dovrebbe semplificare le cose, non complicarle.

<link>Lucio Bragagnolo</link>lux@mac.com