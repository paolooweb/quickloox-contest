---
title: "Questioni in sospeso"
date: 2009-01-06
draft: false
tags: ["ping"]
---

Solo un paio.

La <a href="http://www.macworld.it/blogs/ping/?p=2099" target="_self">supercopia su iDisk del file da oltre quattro gigabyte</a> è andata non so come, nel senso che dopo più di una settimana da quando era partita era (sembrava) ancora in corso, ma ho sospeso tutto. Nel dubbio, anno nuovo vita nuova, ho riprovato ad avviare un altro tentativo a Capodanno. È l&#236; che va (sembra) e saprò dire.

La <a href="http://www.m-audio.com/products/en_us/Keystation61es.html" target="_blank">Keystation 61es</a> funziona meravigliosamente e mi sto divertendo un sacco. Però, dopo avere reinstallato da zero GarageBand e tutti gli aggiornamenti, non si vedeva. Ho nuovamente tolto <a href="http://www.macworld.it/blogs/ping/?p=2109" target="_self">il plugin Emagic di cui avevo parlato</a> e la tastiera si vede perfettamente.

Tanto dovevo. :-)