---
title: "Compagni di merenda<p>"
date: 2005-10-02
draft: false
tags: ["ping"]
---

Ogni tanto ci si lascia prendere un po&rsquo; la mano<p>

Scrive <strong>Alessandro</strong>:<p>

<cite>L&rsquo;altro giorno da un cliente ho visto un esempio di tecnologia informatica applicato alla merenda: nella saletta di ricreazione c&rsquo;erano macchinette del caffè e distributori di patatine e merendine collegate in rete. La gettoniera solo sulla prima macchina, che pilota il credito delle altre.</cite><br>
<cite>Mi sono ammazzato dalle risate&hellip;</cite><br>
<cite>Ah, l&rsquo;informatica&hellip;</cite><p>

In effetti ogni tanto si esagera. Anche a me è capitato, nell&rsquo;altro senso, di collegare allo stesso Mac due tastiere per giocare in due. Ma sono cresciuto e da allora prediligo il gioco online. :-)<p>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>