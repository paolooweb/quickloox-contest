---
title: "Provaci ancora, Sam (o chiunque tu sia)<p>"
date: 2006-01-01
draft: false
tags: ["ping"]
---

Due attitudini diverse ed è difficile dire quale sia la peggiore<p>

Un amico mi chiede se abbia mai usato Skype e, se sì, come mi sembri. Non ho problemi a rispondere (gran cosa per telefonare in VoIp, per il resto preferisco iChat); peccato che la mia risposta sia di nessuna importanza per lui, visto che i nostri bisogni e relative aspettative riguardo a Skype sono assai diverse. Dovrebbe semplicemente scaricarlo e provarlo. Non ti piace? Lo butti. La riluttanza a installare e provare un software significa spesso perdere inutilmente una buona occasione.<p>

Che fa il paio con l&rsquo;attitudine opposta. Quelli che installano qualsiasi cosa e dopo si chiedono il perché. Io sono molto vicino a questa attitudine e ogni anno installo decine di programmi che butto via subito dopo.<p>

Ma quando l&rsquo;ho installato, prima di lanciarlo, mi assicuro sempre di sapere come fare a buttarlo. Non so se mi sono spiegato.<p>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>