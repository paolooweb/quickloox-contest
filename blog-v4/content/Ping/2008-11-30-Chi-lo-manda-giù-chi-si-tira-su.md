---
title: "Chi lo manda giù, chi si tira su"
date: 2008-11-30
draft: false
tags: ["ping"]
---

Alla collezione mi mancava: una bella innaffiata di caffè sulla tastiera.

Fortunatamente c'era il tempo per correre ai ripari e nel giro di un'ora sono entrato in possesso del muletto da cui vergo - simbolicamente - queste note.

Capitasse, per prima cosa spegni SUBITO il portatile. Io non sono riuscito a evitare il corto, ma si può avere maggiore fortuna.

Secondo, sempre capitasse, la scheda logica del PowerBook 17&#8221; è più o meno sotto la metà sinistra della tastiera. Se il caffè si versa sulla metà destra, dopo avere spento, inclina il portatile verso destra (tenendolo in mano, la destra si abbassa, la sinistra si alza). Ci sono buone probabilità che, inondazioni a parte, il liquido eviti la scheda logica.

Va da sé che bisogna subito aprire la macchina e disassemblare fino a che non si riesce a eliminare con certezza tutto. Asciugare e ripulire, a-s-p-e-t-t-a-r-e che tutto sia totalmente asciutto e pulito, rimontare e incrociare le dita.

Smontare un PowerBook 17&#8221; non è difficile; basta togliere tutte le viti. Tuttavia, come uno ci si mette, capisce immediatamente che grande progresso tecnologico sia l'<em>unibody</em>.