---
title: "Consigli antispam"
date: 2003-10-17
draft: false
tags: ["ping"]
---

Parere personale: il programma giusto e via

Ricevo più di cento mail al giorno. Ovviamente non manca lo spam.
Filtrarlo manualmente con Mailsmith - il mio programma di posta - è un ottimo sistema, ma porta via molto tempo.

Ci sono servizi che richiedono autenticazione preventiva degli umani che mi vogliono scrivere, ma non sono un sadico e nessuno deve chiedere il permesso a chicchessia per potermi scrivere.

Cambiare indirizzo di posta elettronica è un palliativo, che dura per un po’.

Parere personale: filtri bayesiani. Quelli di Mail, per intenderci, oppure quelli che programmi come SpamSieve o SpamFire sono in grado di applicare a qualunque programma di posta. Sto usando SpamSieve da un po’ e devo dire che, di tutti, per me è il rimedio più efficace in assoluto, anche senza addestrarlo e contando solo sulle sue capacità.

Poi c’è anche chi non vorrebbe proprio riceverla, la posta pubblicitaria non richiesta. Ma sono curioso di sapere se la casella postale di casa, quella fisica, l’hanno tolta, sigillata oppure riempita di acido muriatico.

<link>Lucio Bragagnolo</link>lux@mac.com