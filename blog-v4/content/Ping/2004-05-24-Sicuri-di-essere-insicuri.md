---
title: "Sicuri di essere insicuri"
date: 2004-05-24
draft: false
tags: ["ping"]
---

Ma dove è finito il buon senso del buon padre di famiglia?

Sono giorni di Uri, protocolli a rischio, falle di sicurezza, update della stessa. Fa specie il numero di persone che, nella vita normale, stanno bene attente a non comprare cellulari a prezzo stracciato nelle aree di servizio delle autostrade e si fanno in quattro per avere in vista il proprio figliolo ogni minuto della giornata, perché i pericoli non mancano; ma dagli in mano un computer e scaricano come forsennati qualunque cosa, senza ritegno e senza alcun freno, nemmeno inibitorio.

La sicurezza assoluta non esiste, né in informatica né altrove. L’ultima difesa è anche la più efficace: tenere acceso il cervello. Sostituirla con il clic compulsivo può avere conseguenze.

<link>Lucio Bragagnolo</link>lux@mac.com