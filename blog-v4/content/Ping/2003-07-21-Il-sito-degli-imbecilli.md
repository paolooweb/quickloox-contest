---
title: "Il sito dei mentecatti"
date: 2003-07-21
draft: false
tags: ["ping"]
---

Le teste più vuote della comunità informatica mondiale hanno una casa

Avevo già letto che Apple dovrebbe mettersi a vendere a prezzo stracciato macchine G3 senza il marchio Apple, per conquistare quote di mercato (e strangolarsi senza motivo).

Ora leggo una grande idea per aumentare il mercato dei giochi Mac: Apple dovrebbe mettere in piedi una divisione giochi che di routine esegua porting dei migliori giochi per Windows, per poi inserire gratis i giochi stessi dentro i Macintosh che la gente acquista.

Il sito è <link>OsNews</link>http://www.osnews.com e, per le due notizie che ho letto, mi sembra scritto da mentecatti. Non posso pronunciarmi sul resto del sito, che mi guarderò bene dal leggere.

<link>Lucio Bragagnolo</link>lux@mac.com
