---
title: "Vedere lontano, guardare vicino<p>"
date: 2005-07-28
draft: false
tags: ["ping"]
---

Ogni tanto Apple punta il telescopio sul futuro ma dimentica gli occhiali da vista<p>

Ricordi Konfabulator? I widget, prima versione.<p>

Ad Apple l&rsquo;idea è piaciuta e hanno deciso di adottarla in Mac OS X. Però volevano fare qualcosa di meglio. Inesistente qualsiasi problema economico, con cinque miliardi di dollari in denaro liquido, la società poteva adottare liberamente una di queste linee di comportamento:<p>

a) comprarsi Konfabulator e farlo diventare Dashboard;<br>
b) assumere i progettisti di Konfabulator e metterli a lavorare su Dashboard;<br>
c) fare Dashboard da sola e lasciare Konfabulator al suo destino.<p>

Le soluzioni a) e b) erano le più complesse, perché acquisire un&rsquo;azienda e le persone relative, anche se l&rsquo;azienda è piccola, richiede impegno e a volte qualche compromesso.<p>

La soluzione c) era la più comoda. Me lo faccio io come pare a me.<p>

Peccato che intanto il team di Konfabulator si sia fatto comprare da Yahoo, che offrirà il programma gratuitamente a tutti. Girerà anche su Windows, dove quasi tutti i widget esistenti funzioneranno da subito (non vanno quelli che si appoggiano a Unix, Perl o altre tecnologie evolute che a Microsoft danno fastidio).<p>

Così Mac OS X non sarà più l&rsquo;unica piattaforma a usare i widget e Dashboard non sarà più un fiore all&rsquo;occhiello. Gli sviluppatori penseranno che la soluzione per non dipendere da Apple sia sviluppare anche per Windows, e il numero di esclusive software di Mac OS X potrebbe anche diminuire. Dashboard, costato tempo e denaro oltre che pensiero, potrebbe passare da avanguardia a cosa qualsiasi.<p>

Così capace di guardare di guardare lontano eppure tanto miope da fregarsi con le sue mani. Apple, l&rsquo;azienda che farebbe impazzire qualunque oculista.<p>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>