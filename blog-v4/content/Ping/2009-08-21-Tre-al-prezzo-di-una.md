---
title: "Tre al prezzo di nessuna"
date: 2009-08-21
draft: false
tags: ["ping"]
---

L'esagitato ha pubblicato un altro pezzo, in cui constata che l'indicazione per l'uscita di Snow Leopard <a href="http://store.apple.com/it/product/MC204T/A?afid=p204%7C403275" target="_blank">è settembre</a> e ne ricava la conclusione che il lancio sarà il 28 agosto.

Scopo raggiunto. Non è cambiato niente rispetto a quello che c'era scritto sul sito Apple a giugno. Intanto sono comparse tre storie acchiappagonzi.