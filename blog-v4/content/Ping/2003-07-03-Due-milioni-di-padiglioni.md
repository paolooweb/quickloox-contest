---
title: "Due milioni di padiglioni"
date: 2003-07-03
draft: false
tags: ["ping"]
---

Il concetto di digital life non è solo marketing

In questi giorni, uno più uno meno, Apple ha venduto il milionesimo iPod. Quando è stato presentato tanta gente ha storto il naso: aspettavano il palmare da sogno che nessuno aveva mai promesso (memento: Apple non produrrà un palmare) e che a tutti gli effetti era cinque anni in ritardo sull’avanguardia di adesso: la vita digitale.

La frontiera dell’elettronica di consumo, oggi, è anche un concetto semplicissimo: alle persone piace ascoltare la propria musica, dovunque, facilmente.

Talmente semplice che, come al solito, solo Apple lo ha saputo capire fino in fondo e fare qualcosa di diverso dal coro.

Complimenti iPod! Vincente e moderno come una grande fiera futuristica dell’audio digitale, con due milioni di padiglioni. Auricolari.

<link>Lucio Bragagnolo</link>lux@mac.com