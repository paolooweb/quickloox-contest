---
title: "Il browser aperto<p>"
date: 2005-12-28
draft: false
tags: ["ping"]
---

Safari (e quanto gli sta sotto) non è esclusiva Apple<p>

Nel team che lavora a Web Kit, il progetto open source di motore Web su cui è seduto Safari, stanno tre persone estranee ad Apple. Fino a poco fa erano una sola e da poco sono <a href="http://webkit.opendarwin.org/blog/?p=36">entrati nel giro</a> altri due programmatori, George Staikos e Alexey Proskuryakov.<p>

La prossima volta che ti scappa da pensare che Apple e Microsoft possano funzionare allo stesso modo, chiediti quanti programmatori non stipendiati da Microsoft possono autorizzare modifiche al codice di Explorer.<p>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>