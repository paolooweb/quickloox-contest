---
title: "Striscia, la notizia"
date: 2008-10-07
draft: false
tags: ["ping"]
---

Alla tivù passano gli <em>spot</em> di un aggeggio Samsung che, in virtù di uno schermo sensibile al tocco, si proclama <cite>molto più di un telefono</cite>.

John Gruber scrive su <a href="http://daringfireball.net/" target="_blank">Daring Fireball</a> <cite>I think it's finally starting to hit people that the iPhone is the first mainstream consumer mobile computing platform</cite> (<em>credo che la gente stia finalmente iniziando a capire che iPhone sia la prima piattaforma di elaborazione da tasca di grande diffusione per i consumatori</em>).

Lentamente, un centimetro per volta, si arriva a capire che non sono cellulari.