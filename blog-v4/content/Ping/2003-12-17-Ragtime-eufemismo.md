---
title: "RagTime eufemismo"
date: 2003-12-17
draft: false
tags: ["ping"]
---

Potrebbe essere una buonissima alternativa al vecchio AppleWorks

Chi lo sa se Apple ha voglia di rimettere a nuovo AppleWorks, che è tanto essenziale e svelto ma avrebbe bisogno di una buona ristrutturazione.

Nel frattempo ho scoperto che esiste anche RagTime Solo, evoluzione di un programma antichissimo e che si credeva ormai perduto. RagTime Solo fa tutto quello che dovrebbe fare AppleWorks, sembra farlo abbastanza bene ed è gratuito se non viene usato a scopo professionale.

Insomma, è <link>RagTime Solo</link>http://www.ragtime-online.com, non solo RagTime.

<link>Lucio Bragagnolo</link>lux@mac.com