---
title: "Non è mai troppo tardi per AppleScript"
date: 2008-06-07
draft: false
tags: ["ping"]
---

Il mio Mac non si spegne mai, al massimo sta in stop. Molti, invece, preferiscono spegnerlo la sera andando a dormire e riaccenderlo quando tornano dal lavoro il giorno dopo.

Di questi, a una buona percentuale è capitato almeno una volta di dimenticarsi il Mac acceso e di ricordarsene esattamente un attimo dopo che è troppo tardi per fare qualcosa.

Invece non è mai troppo tardi.

La prossima volta che siamo sul Mac prepariamo alcuni script banalissimi, come questi:

<code>tell application "Finder"
shut down
end tell</code>

<code>tell application "Finder" to restart</code>

<code>tell application "Finder"
sleep
end tell</code>

Sono tre script diversi, non uno solo. Li potremmo chiamare con grande fantasia <em>Spegni</em>, <em>Riavvia</em> e <em>Stop</em>.

Apriamo il programma di posta. Si suppone che, come Mail o <a href="http://www.barebones.com/products/Mailsmith" target="_blank">Mailsmith</a>, abbia la possibilità di impostare nelle regole di filtraggio l'esecuzione di script (se un programma di posta non ha filtri che possono eseguire automaticamente script è un pessimo programma di posta). Si suppone anche che il programma sia in funzione e ritiri la posta automaticamente.

Adesso impostiamo una regola di filtraggio che dica (la forma non importante, basta che la sostanza sia questa): se arriva un certo messaggio, esegui un certo script.

Per esempio: <em>se arriva un messaggio scritto dal mio indirizzo di ufficio, che si intitola Spegniti, esegui lo script Spegni</em>.

Fatto. Mac dimenticato acceso a casa? Appena arrivati in ufficio, pausa caffè e messaggio di posta intitolato Spegniti. Il Mac lo riceverà, eseguirà lo script a seguito del filtro e si spegnerà.

Avvertenze: nel caso di ravvii e spegnimenti, occorre che non ci siano documenti aperti e non salvati, altrimenti il Mac si fermerà a chiedere se si vuole salvare il documento e non procederà nello spegnimento.

Attenzione a non creare filtri che si prestino a troppi equivoci (tipo che la suocera manda una mail intitolata <em>Spegni sotto l'arrosto</em> e il Mac si chiude).

Con lo stesso sistema si può creare uno script che utilizza comandi da Terminale per spegnere il Mac qualunque cosa ci sia attiva sopra. Basta essere bravini e approfondire il tema. :-)

Questa procedura viene <a href="http://www.tuaw.com/2008/04/07/applescript-control-your-mac-with-an-e-mail/" target="_blank">descritta in originale</a> su The Apple Unofficial Weblog.