---
title: "300+ novità: Imaging"
date: 2007-11-02
draft: false
tags: ["ping"]
---

Su questo ce la caviamo in fretta, ché <a href="http://www.apple.com/it/macosx/features/300.html#imaging" target="_blank">la pagina Apple</a> con le modifiche evidenti di Leopard su questo non si dilunga. Trattasi di Acquisizione Immagine, che nessuno usava e forse adesso qualcuno tornerà a usare.

I modelli di fotocamera supportati sono aumentati e adesso, se l'apparecchio lo consente, si può fare acquisizione anche wireless, wi-fi e Bluetooth. L'acquisizione si fa anche via rete e Leopard se la cava bene (almeno dovrebbe) grazie a Bonjour.