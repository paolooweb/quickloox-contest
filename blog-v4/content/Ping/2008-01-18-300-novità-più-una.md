---
title: "300 novità, più una"
date: 2008-01-18
draft: false
tags: ["ping"]
---

Riporta <a href="http://multifinder.wordpress.com" target="_blank">Mario</a>:

<cite>Con Mail in Leopard è possibile trascinare una singola mail e copiarla dove più fa comodo (io lo faccio per esempio con MacJournal, che mi serve per tenere traccia di alcune delle tante cose che faccio). Il clic sull'icona della mail (estensione .eml) apre il messaggio stesso in Mail. Con Tiger non si poteva</cite>.

Se ci facessimo la stessa attenzione tutti i giorni, aggiungendo un briciolo di curiosità e ficcanasamento, <em>con Tiger non si poteva</em> diventerebbe un <em>mantra</em>.