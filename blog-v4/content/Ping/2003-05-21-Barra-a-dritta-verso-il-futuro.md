---
title: "Barra a dritta verso il futuro"
date: 2003-05-21
draft: false
tags: ["ping"]
---

In silenzio, l’interfaccia utente evolve

Qualcun altro comincia a farci caso? Io me ne sono reso conto da poco, più o meno con l’arrivo di iLife.

Quando uso i programmi è sempre più drag and drop, pulsanti, script, tasti freccia, e sempre meno menu.

Una volta non era così: i menu erano la forma più semplice di interazione e poi, quando uno diventava bravo, aveva imparato le scorciatoie di tastiera.

Ho l’impressione che l’interfaccia utente stia evolvendo e superando la barra dei menu. Che di questo passo un Mac OS XX potrebbe anche non avere, naturalmente in favore di qualcosa di meglio.

Mentre chi diventa bravo, invece che le scorciatoie di tastiera, coltiva il Terminale.

Think about it. :-)

<link>Lucio Bragagnolo</link>lux@mac.com