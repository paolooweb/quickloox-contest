---
title: "Ad Azeroth lo fanno strano"
date: 2006-04-12
draft: false
tags: ["ping"]
---

Si chiacchiera della possibilit&agrave; che, per via della possibilit&agrave; di avviare Mac con Windows, i produttori di giochi smettano di produrre giochi nativi per Mac.

Nel lontano reame di Azeroth, invece, una casa produttrice sconosciuta, evidentemente ignara di tutto per via della distanza che ci separa, ha aggiornato le <a href="http://www.worldofwarcraft.com/info/faq/technology.html" target="_blank">Faq</a> di un suo giochino marginale, praticato solamente da una decina di milioni di persone disposte a pagare un canone mensile per farlo, scrivendo all&rsquo;incirca <cite>C&rsquo;&egrave; evidenza riconosciuta del nostro continuo supporto a Mac OS nativo e non abbiamo intenzione di abbandonare questa tradizione. Vediamo che la nostra base di giocatori Mac preferisce il software nativo non appena sia possibile e la nostra abitudine di sviluppare in modo multipiattaforma va loro incontro</cite>.

Gli altri chiacchiere, in Blizzard distintivi.