---
title: "Non ci sono"
date: 2010-03-05
draft: false
tags: ["ping"]
---

Ma solidarizzo spiritualmente con il <a href="http://www.worldofwarcraft.com/wowanniversary/battlecry/" target="_blank">mosaico BattleCry</a> di World of Warcraft, realizzato con le immagini di ventimila partecipanti reali.