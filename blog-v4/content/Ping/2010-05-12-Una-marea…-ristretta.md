---
title: "Una marea&#8230; ristretta"
date: 2010-05-12
draft: false
tags: ["ping"]
---

Per chi potesse essere interessato, sta per iniziare il beta test di <a href="http://www.playdarktide.com/" target="_blank">Captains of DarkTide</a>, ennesimo gioco di ruolo di massa fantasy su Internet, però basato sulla strategia navale. Niente boschi foreste e castelli con segrete e torrioni, ma vascelli, abbordaggi e isole.

Come è intelligente a XXI secolo solidamente iniziato, il gioco sarà multipiattaforma, Windows e Mac.