---
title: "Le certezze non bastano mai"
date: 2010-05-29
draft: false
tags: ["ping"]
---

Sono titolare di una Internet Key E1750 Huawei con connessione Wind.

Ci ho messo del bello e del buono per averla. Come ho detto <cite>Mac</cite> e ho aggiunto <cite>Snow Leopard</cite>, il commesso ha cercato di scoraggiarmi in ogni modo, perché la settimana scorsa l'ha venduta a un cliente che gli ha dato problemi, non è sicuro che funzioni, gliela cambiamo se non va ma non si arrabbi eccetera eccetera.

Funziona perfettamente e non c'è stato nemmeno bisogno di impostare manualmente il numero di chiamata come si fa di solito con le chiavette riottose; lanciato il suo software, ha configurato tutto e in capo a pochi minuti navigavo a perfezione.

In confidenza, edulcorerò la pillola, il software è vomitevole a vedersi e usarsi. Conta comunque che funzioni e funziona.