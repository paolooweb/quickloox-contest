---
title: "Cambio automatico o manuale?"
date: 2004-02-27
draft: false
tags: ["ping"]
---

Euro forte, dollaro debole e banalità aziendali

Continua a dire che non capisce perché, con l’euro così forte, Apple non abbassi i prezzi in Europa. E che se l’euro fosse debole Apple, invece, i prezzi li alzerebbe subito.

Continuo a spiegare che Apple, alla fine, fa i conti in dollari. Con l’euro forte Apple non fa che incassare (in dollari) più del previsto ed è tutta felice. Che abbasserebbe i prezzi se vendesse di meno; ma non sta accadendo. Che abbasserebbe i prezzi se la componentistica la pagasse in euro, mentre invece la paga in dollari. E che alzerebbe i prezzi subito se diventasse forte il dollaro, perché in dollari Apple incasserebbe assai meno del previsto.

Ma non riesco a farmi capire. Pensa che il profitto a Cupertino debba dipendere dalla moneta di Roma. E perché non da quella di Kuala Lumpur? O Ulan Bator? O Johannesburg?

<link>Lucio Bragagnolo</link>lux@mac.com