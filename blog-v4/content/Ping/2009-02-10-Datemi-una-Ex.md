---
title: "Datemi una Ex"
date: 2009-02-10
draft: false
tags: ["ping"]
---

Per quanto superato, ci sono momenti in cui il vecchio correttore ortografico libero <a href="http://excalibur.sourceforge.net/" target="_blank">Excalibur</a> torna ancora utile.

Vederlo aggiornato, anche ogni quindici mesi, anche minimamente, riscalda il cuore. Se poi servisse una correzione ortografica della Clipboard, è sempre l&#236;.