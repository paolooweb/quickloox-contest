---
title: "Il futuro delle riviste"
date: 2008-09-16
draft: false
tags: ["ping"]
---

O almeno di alcune, o di quelle nuove, o forse un vicolo cieco, o varie ed eventuali.

<a href="http://www.teleart.org/sito/home.html" target="_blank">Andrea Ack</a> mi ha segnalato <a href="http://issuu.com/" target="_blank">Issuu</a> e mi ha fatto molto pensare. Mi piace l'idea, mi piace il sito, non mi piace l'infrastruttura tecnica, sono incerto sul concetto di riuscire a proporre contenuti non pienamente fruibili (in modo che non siano piratabili).

Ognuno di noi potrebbe avere opinioni diverse su ciascuno dei quattro punti. Parliamone, ne merita.