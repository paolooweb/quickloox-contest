---
title: "Questa gestione fa testo"
date: 2007-11-19
draft: false
tags: ["ping"]
---

La critica più ridicola a Leopard che ho sentito è <cite>non riesco a fare funzionare NetInfo</cite>.

Non c'è da meravigliarsi, dato che NetInfo non esiste più.

Adesso, per la gestione delle utenze e delle reti, si usano le Preferenze di Sistema alla voce Account e l'utility Accesso Directory. I vecchi comandi da Terminale come <code>nicl</code> non esistono più; ora ci sono <code>dscl</code>, <code>dsmemberutil</code>, <code>dseditgroup</code>, <code>dscacheutil</code>, <code>dsenableroot</code>.

La cosa più importante è che l'inaccessibile database di NetInfo è stato sostituito, come racconta John C. Welch in un <a href="http://www.macworld.com/news/2007/11/16/netinfo/index.php?lsrc=mwrss" target="_blank">eccellente articolo</a>, da una serie di file Xml in formato testo dentro <code>/var/db/dslocal/</code>, molto più facili da interpretare e maneggiare a tutti i livelli, compreso quello degli amministratori di rete, che ora dovranno sudare assai meno. A patto che smettano di provare a lanciare Gestione NetInfo.