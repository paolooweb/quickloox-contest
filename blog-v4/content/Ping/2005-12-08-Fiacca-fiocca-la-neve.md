---
title: "Fiacca fiocca la neve<p>"
date: 2005-12-08
draft: false
tags: ["ping"]
---

In analogico e in digitale<p>

Ogni tanto prendo una piccola licenza dai temi strettamente Mac e questa volta il pretesto è la mia finestra, fuori dalla quale ha ricominciato a nevicare.<p>

Se deve essere coltre bianca, che sia. Ma Internet funziona ancora e allora <a href="http://snowflakes.lookandfeel.com/">divertiamoci tutti</a>, compresi i bambini, che potranno provare a rifare con la carta gli esperimenti più riusciti del Web. Per loro analogico è ancora bello!<p>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>