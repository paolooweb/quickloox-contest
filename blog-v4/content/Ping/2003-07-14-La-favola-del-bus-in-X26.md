---
title: "Come il programmatore bravo diventò cattivo"
date: 2003-07-14
draft: false
tags: ["ping"]
---

Il buon funzionamento di una periferica non dipende solo e sempre da Apple

Siccome è una cosa lunga, la morale arriva subito: se una periferica non funziona magari è colpa della periferica e non di Mac OS X. Ora, ecco la fiaba: serve a capire uno dei motivi per cui è uscito Mac OS X 10.2.6.

C’era una volta in Apple un programmatore che scriveva driver per perfieriche Usb. Siccome i driver sono estensioni del kernel e una estensione malfatta può fare crollare il kernel, il codice dei driver deve essere assai solido e collaudato.

Così lavorava il programmatore, con grande attenzione e concentrazione. Programma programma, arrivò a scrivere il codice da eseguire quando il bus Usb viene resettato. È una cosa che accade spesso, per esempio quando si attacca o si stacca un apparecchio Usb. Le specifiche dello standard Usb prevedono che, dopo un reset, tutte le periferiche rispondano a una cosiddetta richiesta di enumerazione, in pratica un ricapitolare chi è attaccato al bus. A volte possono verificarsi problemi che portano a mancate risposte o altro.

Il programmatore aveva inizialmente scritto codice che, nel caso un apparecchio non avesse risposto alla richiesta di enumerazione, lasciava correre. Ma era un programmatore scrupoloso e rilesse una volta di più lo standard Usb. Vide che gli apparecchi devono rispondere obbligatoriamente alla richiesta di enumerazione, altrimenti non rispettano lo standard. Chiarita una volta di più la questione, il programmatore sistemò il suo codice in modo che rispettasse anch’esso lo standard Usb e inviasse un messaggio di errore quando un certo apparecchio disobbediva. Un errore in questa situazione significa un kernel panic, ma il programmatore era tranquillo. Gli apparecchi Usb devono rispettare lo standard Usb, altrimenti sono malfatti.

Il programmatore terminò di scrivere il suo codice e lo inviò ad Apple. Gli addetti ai collaudi controllarono il nuovo codice su qualche centinaio di apparecchi Usb, senza problemi. Lo stesso codice venne poi diffuso ai principali sviluppatori software, che non trovarono alcun problema. Passati tutti i controlli, quel codice divenne parte di Mac OS X 10.2.5.

C’è sempre qualcuno che si fa sentire lamentando problemi dopo un upgrade, ma normalmente sono casi isolati, con configurazioni particolari o un disco danneggiato. Invece, stavolta, tutti lamentavano lo stesso problema: un kernel panic vari minuti dopo l’avvio, ma solo se erano connessi certi apparecchi Usb.

Il supporto Apple iniziò a capire che gli apparecchi problematici erano alcuni precisi modelli ed effettuarono le loro prove. Gli apparecchi non funzionavano più con il nuovo codice. Cominciò un lungo e faticoso lavoro di debugging per capire in che cosa sbagliava il nuovo codice.

Debbuging debugging, si scoprì che il codice del programmatore era perfetto. Invece quei certi apparecchi Usb non rispettavano lo standard, come accadrebbe a una vite che si avvita in senso antiorario o a un interruttore senza salvavita.

Il servizio di supporto cominciò a rispondere chi si lamentava che la colpa era degli apparecchi Usb che non rispettavano lo standard. Ma i proprietari degli apparecchi non volevano sentire ragione: “c’è scritto Usb sulla scatola e quindi questo è Usb! Siete voi a fare codice che non funziona!” Non serviva spiegare che il codice era a posto e rispettava lo standard al contrario degli apparecchi difettosi; la gente insisteva perché voleva continuare a usare il proprio apparecchio fintamente Usb.

Un giorno Apple fece uscire un nuovo upgrade, chiamato 10.2.6, che usciva dallo standard per riuscire a fare funzionare anche gli apparecchi anomali. Gli apparecchi ripresero a funzionare e nessuno più lamentò, almeno per quanto riguarda gli apparecchi Usb.

Fu così che Apple venne criticata per avere rispettato gli standard e venne criticata per avere poi fatto uscire un upgrade che andava oltre lo standard, upgrade scritto apposta per rimediare alle pecche altrui.