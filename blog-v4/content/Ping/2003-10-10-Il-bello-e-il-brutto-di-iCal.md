---
title: "Il bello e il brutto di iCal"
date: 2003-10-10
draft: false
tags: ["ping"]
---

Il programma diventa più intelligente ma resta ostinato su un punto

iCal per me ha un valore professionale e personale notevole, nonostante il programma sia gratuito, e quindi mi sono affrettato a installare il nuovo <link>iCal 1.5.1</link>http://www.apple.com/ical/download.

Tra le varie modifiche, una cosa molto buona è che finalmente il programma si comporta educatamente quando manca la connessione Internet, invece di seccare in continuazione con avvertimenti intrusivi e inutili (sì, accade a volte di essere senza connessione; sì, per me è importante avere un aggiornamento frequente dei calendari; sì, un AppleScript credo potrebbe modificare il ritmo degli aggiornamenti quando manca la connessione, ma è più corretto che se ne occupi il programma).

La cosa brutta, che rimane, è questa stupida riottosità a dialogare con un programma di posta che non sia Mail. Un AppleScript risolve tutto, ma bisogna sapere dove metterlo e annche scriverlo. A scriverlo ci pensano gli utenti più bravi delle varie comunità, ma Apple dovrebbe prevedere un modo facile di permettere a ciascuno di noi di usare il programma di posta che preferisce. Per tutelare la stessa libertà con cui decido di usare iCal, che di suo è ottimo.

<link>Lucio Bragagnolo</link>lux@mac.com