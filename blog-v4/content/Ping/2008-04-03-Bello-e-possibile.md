---
title: "Bello e possibile"
date: 2008-04-03
draft: false
tags: ["ping"]
---

Lo avrei scritto io, solo che <a href="http://www.LoScrittoio.it" target="_blank">Flavio</a> lo ha fatto prima di me e benissimo, cos&#236;:

<cite>Quindi i <a href="http://docs.google.com" target="_blank">Docs di Google</a> <a href="http://www.macworld.it/showPage.php?template=notizie&amp;id=14596">possono lavorare anche offline</a>.</cite>

<cite>Per dimostrare come sia immediato hanno pensato: facciamo un video, <a href="http://googleblog.blogspot.com/2008/03/offline-access-to-google-docs.html" target="_blank">lo pubblichiamo su YouTube</a> e usiamo un Mac.</cite>

<cite>Che cosa vuoi di più semplice ed avvincente?</cite>

<cite>E non ha manco timore di viaggiare su un cesto da bici senza guanciale&#8230;!</cite>

Rispetto all'ultima considerazione, aggiungo solamente che le ragioni per scegliere Mac sono molte più della presenza di Mac OS X.

Possibile, con Google Docs, e pure bello, con un Mac.