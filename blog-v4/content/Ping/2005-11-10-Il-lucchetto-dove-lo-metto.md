---
title: "Il lucchetto dove lo metto<p>"
date: 2005-11-10
draft: false
tags: ["ping"]
---

Buone notizie per i paranoici. Con buone ragioni<p>

Mentre scrivo, sono in chat con un amico. Siamo tutti e due abbonati a .mac. Come tali, l&rsquo;aggiornamento di sistema a Mac OS X 10.4.3 ci consente di cifrare automaticamente il testo delle nostre chiacchierate via iChat.<p>

La meccanica del tutto è assurdamente semplice. Dopo l&rsquo;aggiornamento, al primo avvio di iChat viene chiesto se abilitare la cifratura. Che lo si faccia o meno, in seguito è solo questione di cliccare un pulsantino dentro le preferenze del programma.<p>

Se si parla tra abbonati a .mac. sulla finestra appare un lucchetto, a indicare la cifratura. Altrimenti non appare e la connesssione è in chiaro come è sempre stata. Nessun bisogno di modificare alcunché. Di fatto, conviene abilitare la cifratura perché tanto si attiva automaticamente dove può, e altrettanto automaticamente non funziona se non può.<p>

Il bisogno di sicurezza in una applicazione come la chat è relativo, ma c&rsquo;è chi ne ha bisogno. E ora Mac OS X mette a disposizione una opzione, facile e precisa, in più.<p>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>