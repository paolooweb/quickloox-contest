---
title: "Giochi da grandi"
date: 2008-07-20
draft: false
tags: ["ping"]
---

<strong>Anidel</strong> ne ha scovata un'altra e davvero forte: <a href="http://www.p01.org/releases/DHTML_contests/files/DEFENDER_of_the_favicon/" target="_blank">un'edizione di Defender</a> dentro la <em>favicon</em> della pagina.

Ora dovrei spiegare che cosa è Defender per i giovani, spiegare che cos'è una <em>favicon</em> per i vecchi e spiegare quale sia il valore di ciò a tutti (indizio: non è il gioco in sé, ma la possibilità di allargare i propri orizzonti mediante JavaScript).

Mi limiterò a dire che è molto meglio provare con Firefox e che serve anche una buona vista, esperienza <em>arcade</em> a parte. :-)