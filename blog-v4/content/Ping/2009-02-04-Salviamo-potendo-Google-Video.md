---
title: "Salviamo, potendo, Google Video"
date: 2009-02-04
draft: false
tags: ["ping"]
---

Sono visceralmente contrario alle petizioni via Internet, che sono ridicole e non portano a niente di niente. Però.

Chiacchieravo con <a href="http://freesmug.org" target="_blank">gand</a> e lui mi ha rimandato a una <a href="http://www.petitionspot.com/petitions/SaveGoogleVideo" target="_blank">petizione per salvare Google Video</a>, di cui Big G ha annunciato la prossima chiusura.

Senza farmi vedere da lui ho sbuffato, ma sono andato a vedere l'indirizzo (visito <em>tutti</em> gli indrizzi che mi arrivano).

Il secondo firmatario ha scritto <cite>Aderisco alla petizione. Usiamo Google Video per pubblicare informazioni mediche potenzialmente salvavita. YouTube non permette video di lunghezza equivalente</cite>.

Lo stesso <strong>gand</strong>, che insegna, ha rincarato la dose: i <em>videotutorial</em> che lui inserisce nelle sue lezioni su YouTube non li può mettere, per questioni di spazio.

La concorrenza chiama la concorrenza e da qualche parte in rete ci sarà un servizio con le prestazioni di Google Video. Però va trovato, bisogna migrarvi tutta la propria filmoteca e comunque vada c'è gran tempo da perdere sulla testa di persone che non lo meritano.

Fermo restando che firmare la petizione non serve a niente&#8230; io la firmerei lo stesso. E poi, se conosco qualche centralinista, fattorino, garzone, impiegato di Google, passaparola. In modo molto rispettoso, cordiale, aperto, tranquillo.

Più il dipendente è di basso rango, più capisce ed è utile. Inutile avere appoggi in alto: là c'è chi ha deciso la chiusura.