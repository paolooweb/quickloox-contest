---
title: "Chi fermerà Repubblica<p>"
date: 2005-07-24
draft: false
tags: ["ping"]
---

I Pooh si chiedevano chi fermerà la musica. Qui si parla più della deriva dei contenuti<p>

Non entro nel merito dell&rsquo;<a href="http://www.repubblica.it/2005/g/sezioni/scienza_e_tecnologia/affin3/affin3/affin3.html">articolo</a> su Longhorn (ormai Windows Vista) che Giuseppe Turani ha scritto per Repubblica.it. Il bravo <a href="http://attivissimo.blogspot.com/">Paolo Attivissimo</a> lo ha <a href="http://italy.peacelink.org/cybercultura/articles/art_12024.html">fatto a pezzettini</a> come meritato ed è stato fin troppo gentile. Di mio ho scritto una breve e spero educata <a href="http://www.repubblica.it/servizi/scrivi.html">mail</a> alla redazione e per conoscenza al direttore (si dice che l&rsquo;indirizzo in realtà non funzioni, ma provare non costa nulla), segnalando l&rsquo;inadeguatezza dell&rsquo;articolo in questione.<p>

Fino a qui pazienza, il giornalista ignorante di informatica è la regola e non l&rsquo;eccezione.<p>

Ma bisogna chiedersi che cosa succederà dopo. Dal punto di vista di Microsoft, è stato un successo. Ha organizzato (presumo) un elegante evento stampa, con un bel rinfresco o un signor buffet, magari con un simpatico gadget in regalo, e ha ottenuto quello che voleva: una incensata. I giornalisti, nel loro gergo, usano anche altri termini, ma il concetto è sempre valido.<p>

Dal punto di vista di Repubblica, potrebbe essere stato un successo. L&rsquo;articolo ha fatto felice un potenziale inserzionista ed è stato certamente visto da un mare di persone. In fondo persino ora sta beneficiando, in traffico, di questo piccolo passaparola.<p>

Per Turani è stata una bella occasione di (re)incontrare i vertici di un&rsquo;azienda potente, rafforzare legami importanti professionali e/o di amicizia, godersi il rinfresco o il buffet. Il tutto in cambio di un articolo pubblicato con un notevole risalto e che va bene a tutti, potenziale inserzionista ed editore attento ai risultati. Al prossimo evento Vip di Microsoft sarà certamente reinvitato con tutti gli onori e presumibilmente Repubblica potrebbe accreditarlo più che volentieri.<p>

A chi può interessare minimamente cambiare questo stato di cose?<p>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>