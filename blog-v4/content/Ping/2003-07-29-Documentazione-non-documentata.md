---
title: "Documentazione non documentata"
date: 2003-07-29
draft: false
tags: ["ping"]
---

Il paradosso dei Developer Tools

Più qualcuno si lamenta ad alta voce della mancanza di documentazione di AppleScript o di come programmare su Mac, più è probabile che abbia installato sul suo disco i Developer Tools ma non abbia mai aperto la cartella Documentation, contenente centinaia di megabyte di informazioni al livello più approfondito possibile.

<link>Lucio Bragagnolo</link>lux@mac.com