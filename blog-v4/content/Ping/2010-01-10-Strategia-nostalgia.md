---
title: "Strategia nostalgia"
date: 2010-01-10
draft: false
tags: ["ping"]
---

Un team di programmatori <i>open source</i> ha rifatto <a href="http://www.planetmule.com" target="_blank">M.U.L.E.</a>, un gioco di strategia e gestione economica ad ambientazione fantascientifica che data al 1983.

Mi piace molto che si possa giocare in locale, però ci sia bisogno di almeno due esseri umani. La registrazione sul sito permette di giocare in rete con chiunque altro e dunque trovare umani su tutto il resto del pianeta.

Il gioco si impara subito, ma <i>sembra</i> semplice. Per questo è interessante. E gratis. Non tutto quello che arriva dagli anni ottanta è per forza vecchio.