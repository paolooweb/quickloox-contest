---
title: "Un Safari complicato"
date: 2009-06-16
draft: false
tags: ["ping"]
---

Mi scrive <b>mAx</b> che, <cite>parlando con alcuni amici, l'impressione generale è che Safari 4 definitivo sia più lento della</cite> beta.

Devo dire che non condivido l'impressione. D'altronde non sono neanche il Padre di Tutti i Navigatori. Come funziona, l&#236;, Safari 4?

(a me piacerebbe fare queste discussioni avendo in mano un benchmark o per esempio indicazioni precise di siti precisi da visitare, ma per stavolta ci si affida all'istinto personale)