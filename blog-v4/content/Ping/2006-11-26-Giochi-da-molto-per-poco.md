---
title: "Giochi da molto, per poco"
date: 2006-11-26
draft: false
tags: ["ping"]
---

I giochi come <a href="http://www.spiderwebsoftware.com/geneforge4/index.html" target="_blank">Geneforge 4: Rebellion</a>. Costano come una pizza, ci puoi giocare per mesi senza stancarti oppure provare il demo gratis e buttarlo via un'ora dopo senza averci rimesso niente. Con buona grafica, una trama vera, supoorto se serve, certezza che arriveranno sequel e aggiornamenti.

La gente che si lamenta della carenza di giochi dovrebbe accorgersi dell'esistenza dello shareware (e dell'open source, volendo approfondire).

Ah: Geneforge per Mac è già qui. Quello per Windows, 2007.