---
title: "A cominciare. Dal boot"
date: 2003-10-14
draft: false
tags: ["ping"]
---

Un decimale, se è quello tra Mac OS X 10.2. e 10.3, vuol dire davvero molto

Non ho firmato nessun accordo di riservatezza con Apple, almeno per ora, e quindi mi sento libero di dire che Mac OS X 10.3, in versione definitiva, è un salto clamoroso rispetto a Mac OS X 10.2 esattamente come questo si staccava nettamente dalla versione 10.1.

Chi dice che “è solo un aggiornamento” non ha proprio capito e si perderà un bel po’, dal boot in avanti. Boot che, per inciso, è segnatamente più rapido. Tanto, appunto, per cominciare

<link>Lucio Bragagnolo</link>lux@mac.com