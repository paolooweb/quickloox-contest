---
title: "Il contorno non arriva per primo"
date: 2008-03-25
draft: false
tags: ["ping"]
---

Sono patito di <a href="http://www.wow-europe.com" target="_blank">World of Warcraft</a>, ma giustamente non tutti lo praticano. Però esistono altri modi per capire di che cosa si parla, come provare il demo di Clan Lord, o giocare gratis con <a href="http://www.prairiegames.com/" target="_blank">Minions of Mirth</a>, o caricare <a href="http://www.runescape.com/" target="_blank">Runescape</a> dentro un browser, o chissà che.

Ecco. Adesso, se provi <a href="http://lotgd.net" target="_blank">Legends of The Green Dragon</a>, scopri un giochino semplice e schematico. Eppure la dinamica è esattamente la stessa di World of Warcraft. Semplicemente ridotta all'osso (eufemismo).

Proprio come le trame possibili sono tipo ventisette (e ci sono arrivati i Greci secoli e secoli fa), i giochi sono sempre quelli, cambia solo l'interpretazione.

Tra l'altro, Legends of The Green Dragon è l'esempio di un sito pulito e organizzato, per giunta navigabile in modo eccellente da tastiera. Da provare: tra l'altro, si vedrà come sia impossibile giocarlo oltre un tempo definito. Neanche la paura dell'assuefazione è più giustificata.