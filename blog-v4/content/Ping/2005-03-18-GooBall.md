---
title: "Giocare fa bene<p>"
date: 2005-03-18
draft: false
tags: ["ping"]
---

E uno dei più simpatici produttori di giochi ne ha appena realizzato uno diverso dal solito<p>

Si potrebbero citare libri serissimi e fior di pensatori per spiegare che il gioco fa bene all&rsquo;uomo come il riposo, il riso, l&rsquo;amore e quant&rsquo;altro, e che una persona incapace di giocare è una persona incapace di vivere.<p>

Per questo tengo a segnalare <a href="http://www.ambrosiasw.com/games/gooball/">GooBall</a> di Ambrosia Software, appena uscito. È un gioco diverso dal solito, che merita un po&rsquo; di registrazioni di shareware. Ambrosia ha sviluppato sempre eccellente software ludico per Mac e non è sbagliato sostenerla. Inoltre GooBall è proprio carino e divertente, pur non essendo impegnato né profondo.<p>

Ma anche lui fa bene allo spirito, se usato bene.<p>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>