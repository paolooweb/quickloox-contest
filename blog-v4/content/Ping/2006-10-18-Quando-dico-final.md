---
title: "Quando dico final"
date: 2006-10-18
draft: false
tags: ["ping"]
---

Il 31 ottobre mi scade il possesso di un dominio. Network Solutions mi manda avvisi da maggio. Settimana scorsa mi ha inviato un <em>final renewal notice</em>. Precisi, ho pensato.

Oggi ne è arrivato un altro, neanche <em>final</em>. Confusi, più che altro.