---
title: "La lunga mano della censura di regime"
date: 2006-06-23
draft: false
tags: ["ping"]
---

Ho cancellato un commento completamente inutile, totalmente scollegato dall&rsquo;argomento e pieno di insulti, non a me. Lo considero un successo, nel senso che &egrave; il secondo dopo mesi e ci sono quasi tremila commenti intelligenti e centrati a osservarlo, ma al tempo stesso mi spiace un po&rsquo;.

Si pu&ograve; benissimo sostenere che una idea &egrave; stupida. Affermare che una persona &egrave; stupida, invece, no. Qui almeno.