---
title: "Dalle tavole della legge di iDisk/3"
date: 2006-02-24
draft: false
tags: ["ping"]
---

La prima cosa da fare per sfruttare al massimo il tuo iDisk &egrave; proporzionare secondo le tue esigenze lo spazio per il disco e lo spazio per la posta.