---
title: "L'anello mancante"
date: 2010-11-26
draft: false
tags: ["ping"]
---

L'italianissimo (anche se per l'abitudine i suoi lineamenti sembrano sempre più orientali!) amministratore delegato di Acer Gianfranco Lanci <a href="http://www.digitimes.com/news/a20101125PD210.html" target="_blank">ha dichiarato</a> di mirare al 10-20 percento del mercato <i>tablet</i> nel breve termine e puntare alla <i>leadership</i> nel giro di due o tre anni.

A parte il fatto che il dieci percento in tre anni è una cosa molto diversa dal venti percento in due anni (sono margini molto, molto ampi e vaghi), va notato che Acer non ha ancora materialmente messo in vendita le proprie tavolette, appena annunciate.

È il mercato che più di tutti ha visto retromarce, promesse mancate, prodotti abbandonati, flop immediati e ambizioni di carta. iPad è arrivato ad aprile scorso e la concorrenza, in sostanza, non esiste. Strategy Analytics sostiene che iPad <a href="http://www.readwriteweb.com/archives/tablet_market_95_ipad_5_everyone_else.php" target="_blank">detenga il 95 percento del mercato</a>, un dato che neanche Windows ha mai raggiunto.

È un dato che non significa niente, se non appunto che in questi mesi la concorrenza è stata inesistente, nonostante fosse stato annunciato un po' di tutti un po' da tutti in arrivo tra estate e autunno.

Ma significano niente anche le dichiarazioni di Acer se non si mettono in conto i profitti. Nel 2009 l'azienda taiwanese ha realizzato, in termini di percentuale di mercato, circa quanto Apple; ma quest'ultima <a href="http://www.businessinsider.com/chart-of-the-day-revenue-vs-operating-profit-share-of-top-pc-vendors-2010-3" target="_blank">ha guadagnato sei volte di più</a>.

Conquistare la <i>leadership</i> quantitativa sul mercato non significa niente, perché uno può raggiungerla anche vendendo a prezzi stracciati. Samsung ha potuto vantare seicentomila Galaxy Tab venduti nel primo mese, ma poi ha <a href="http://www.infocera.com/iPad_vs.Galaxy_Tab:_Samsung_slashes_price_further_competition_gears_up_10889.htm" target="_blank">abbattuto i prezzi in diversi Paesi europei</a>. Se vendi guadagnando molto meno del previsto, o poco comunque, non è tutto questo gran successo.

Se non si fanno queste considerazioni sembra che uno annunci che il suo nuovo prodotto, per ora comprato e provato da nessuno, farà grandi risultati e che gli si crede cos&#236; d'acchito, come se si avesse l'anello al naso.