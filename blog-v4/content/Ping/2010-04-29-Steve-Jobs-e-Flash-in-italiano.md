---
title: "Steve Jobs e Flash (in italiano)"
date: 2010-04-29
draft: false
tags: ["ping"]
---

Ignoro se qualcuno abbia già tradotto integralmente in italiano la <a href="http://www.apple.com/hotnews/thoughts-on-flash/" target="_blank">lettera aperta</a> scritta dall'amministratore delegato di Apple a proposito di Flash.

<h1>Pensieri su Flash</h1>

<cite>Apple ha con Adobe rapporti di lunga data. In effetti abbiamo incontrato i fondatori di Adobe quando stavano ancora nel loro proverbiale</cite> garage<cite>. Apple è stata il loro primo grande cliente, con l'adozione del loro linguaggio PostScript per la nostra nuova stampante LaserWriter. Apple ha investito in Adobe e per molti anni ha posseduto circa il 20 percento dell'azienda. Le due aziende hanno collaborato alla promozione del</cite> desktop publishing <cite>e ci sono stati molti buoni momenti. Da quell'epoca le due aziende sono cresciute separatamente. Apple ha sperimentato il rischio di morire e Adobe è stata attirata verso il mercato</cite> corporate <cite>con i loro prodotti Acrobat. Oggi le due aziende lavorano ancora insieme al servizio dei loro clienti creativi comuni &#8211; gli utilizzatori di Mac comprano circa metà dei prodotti Creative Suite &#8211; ma ci sono pochi altri interessi comuni oltre a questo.</cite>

<cite>Volevo annotare alcuni pensieri sui prodotti Flash di Adobe, cos&#236; che critici e clienti possano comprendere meglio perché non permettiamo Flash su iPhone, iPod e iPad. Adobe ha caratterizzato la nostra decisione come guidata primariamente dall'interesse commerciale &#8211; dicono che vogliamo proteggere il nostro App Store &#8211; ma in realtà si tratta di questioni tecnologiche. Adobe sostiene che il nostro sia un sistema chiuso e che Flash sia aperto, mentre è vero l'opposto. Lasciatemi spiegarlo.</cite>

<cite>Primo punto, la questione dell'</cite>apertura<cite>.</cite>

<cite>I prodotti Flash di Adobe sono proprietari al 100 percento. Sono disponibili solo presso Adobe e solo Adobe ha autorità sul loro progresso futuro, sul prezzo eccetera. I prodotti Flash di Adobe sono ampiamente disponibili, ma questo non significa che siano aperti, in quanto sono controllati per intero da Adobe e resi disponibili solo da Adobe. Flash è un sistema chiuso sotto quasi ogni definizione.</cite>

<cite>Anche Apple ha molti prodotti proprietari. Per quanto il sistema operativo di iPhone, iPod e iPad sia proprietario, riteniamo con forza che tutti gli standard di pertinenza del web debbano essere aperti. Invece di usare Flash, Apple, ha adottato Html5, Css e JavaScript &#8212; tutti standard aperti. Tutti gli apparecchi</cite> mobile <cite>di Apple contengono implementazioni veloci e a basso consumo di questi standard aperti. Html5, il nuovo standard del web che è stato adottato da Apple, Google e molti altri, permette agli sviluppatori web di creare grafica, tipografia, animazioni e transizioni evolute senza dipendere da</cite> plugin <cite>esterni come Flash da utilizzare con il</cite> browser<cite>. Html5 è completamente aperto ed è controllato da un comitato per gli standard, del quale Apple è un membro.</cite>

<cite>Apple, inoltre, crea standard aperti per il web. Per esempio Apple è partita da un piccolo progetto</cite> open source <cite>per creare WebKit, un motore di</cite> rendering <cite>Html5 completamente aperto e libero che è il cuore del</cite> browser <cite>Safari usato in tutti i nostri prodotti. WebKit è stato largamente adottato. Google lo usa per il</cite> browser <cite>di Android, Palm lo usa, Nokia lo usa e anche Rim (BlackBerry) ha annunciato che lo userà. Quasi ogni</cite> browser <cite>di</cite> smartphone <cite>diverso da quello di Microsoft usa WebKit. Aprendo la propria tecnologia WebKit, Apple ha definito lo standard dei</cite> browser mobile <cite>per il web.</cite>

<cite>Secondo punto, c'è</cite> la pienezza del web<cite>.</cite>

<cite>Adobe ha ripetutamente affermato che gli apparecchi</cite> mobile <cite>di Apple non possono accedere alla</cite> pienezza del web <cite>perché il 75 percento del video su web è in Flash. Ciò che non dice è che quasi tutto questo video è disponibile anche in un formato più moderno, H.264, visibile su iPhone, iPod e iPad. YouTube, che si stima contenere il 40 percento di tutto il video su web, brilla in una</cite> app <cite>di serie in tutti gli apparecchi</cite> mobile <cite>di Apple, con iPad che forse offre la migliore esperienza possibile di sempre di scoperta e visione di YouTube. Si aggiunga a questo il video di Vimeo, Netflix, Facebook, Abc, Cbs, Cnn, Msnbc, Fox News, Espn, Npr, Time, The New York Times, The Wall Street Journal, Sports Illustrated, People, National Geographic e molti, molti altri. Gli utilizzatori di iPhone, iPod e iPad non stanno perdendo molto video.</cite>

<cite>Un'altra tesi di Adobe è che gli apparecchi Apple non possano eseguire giochi Flash. È vero. Per fortuna ci sono oltre 50 mila titoli ludici e di intrattenimento su App Store, molti dei quali gratuiti. Esistono più giochi e titoli di intrattenimento disponibili per iPhone, iPod e iPad che per qualsiasi altra piattaforma al mondo.</cite>

<cite>Terzo, ci sono affidabilità, sicurezza e prestazioni.</cite>

<cite>Symantec ha recentemente associato Flash a uno dei peggiori andamenti del 2009 in fatto di sicurezza. Sappiamo anche di prima mano che Flash è la ragione numero uno dei</cite> crash <cite>di Mac. Abbiamo lavorato assieme ad Adobe su questi problemi, che però persistono da diversi anni. Non vogliamo ridurre l'affidabilità e la sicurezza dei nostri iPhone, iPod e iPad a causa dell'aggiunta di Flash.</cite>

<cite>In aggiunta, Flash non ha reso bene sugli apparecchi</cite> mobile<cite>. Abbiamo chiesto regolarmente ad Adobe di mostrarci Flash funzionare bene su un apparecchio mobile, uno qualunque, per vari anni. Non lo abbiamo mai visto. Adobe ha dichiarato pubblicamente che Flash sarebbe arrivato su uno</cite> smartphone <cite>a inizio 2009, poi nella seconda metà del 2009, poi nel primo semestre del 2010 e ora parla della seconda metà del 2010. Crediamo che prima o poi arriverà, ma siamo lieti di non avere trattenuto il respiro aspettandolo. Chi può sapere come funzionerà?</cite>

<cite>Quarto, la vita delle batterie.</cite>

<cite>Per ottenere lunga autonomia durante la riproduzione di video, gli apparecchi mobile devono decodificare il video via</cite> hardware<cite>; farlo via</cite> software <cite>consuma troppa potenza. Molti dei</cite> chip <cite>usati negli apparecchi</cite> mobile <cite>moderni contengono un decodificatore chiamato H.264 &#8212; uno standard industriale che viene usato in qualunque lettore di Dvd Blu-ray ed è stato adottato da Apple, Google (YouTube), Vimeo, Netflix e molte altre aziende.</cite>

<cite>Sebbene Flash abbia recentemente aggiunto il supporto di H.264, su quasi tutti i siti in Flash il video richiede un decodificatore di vecchia generazione, che non è implementato nei</cite> chip mobile <cite>e deve essere eseguito via</cite> software<cite>. La differenza colpisce: su un iPhone, per esempio, la riproduzione di video H.264 dura fino a dieci ore, mentre per i video decodificati via</cite> software <cite>la durata scende a meno di cinque ore prima che la batteria sia esaurita.</cite>

<cite>Quando i siti ricodificano i propri video con H.264, possono offrirli senza usare Flash. Vengono riprodotti perfettamente in</cite> browser <cite>come Safari di Apple e Chrome di Google senza bisogno di</cite> plugin <cite>e si vedono ottimamente su iPhone, iPod e iPad.</cite>

<cite>Quinto, il Touch.</cite>

<cite>Flash è stato progettato per i computer che usano mouse, non per schermi a tocco dove si usano le dita. Per esempio, molti siti Flash si basano sui</cite> rollover<cite>, che fanno comparire menu o altri elementi quando il puntatore del mouse passa sopra uno punto specifico. La rivoluzionaria interfaccia</cite> multitouch <cite>di Apple non utilizza un mouse e non utilizza il concetto di</cite> rollover<cite>. La maggior parte dei siti Flash andrà riscritta per supportare gli apparecchi a tocco. Se gli sviluppatori devono riscrivere i propri siti Flash, perché non usare tecnologie moderne come Html5, Css e JavaScript?</cite>

<cite>Persino se iPhone, iPod e iPad eseguissero Flash, ciò non risolverebbe il problema della riscrittura di gran parte dei siti Flash allo scopo di supportare gli apparecchi con interfaccia a tocco.</cite>

<cite>Sesto, la ragione più importante.</cite>

<cite>Oltre al fatto che Flash è chiuso e proprietario, presenta svantaggi tecnici e non supporta gli apparecchi basati sul tocco, non permettiamo Flash su iPhone, iPod e iPad per una ragione ancora più importante. Abbiamo discusso degli svantaggi di usare Flash per riprodurre video e contenuti interattivi dai siti web, ma Adobe desidera anche che gli sviluppatori adottino Flash per creare applicazioni che funzionano sui nostri apparecchi</cite> mobile.

<cite>Sappiamo per dolorosa esperienza che permettere a uno strato di</cite> software <cite>altrui di interporsi tra la piattaforma e lo sviluppatore risulta in applicazioni di livello inferiore e compromette il miglioramento e il progresso della piattaforma stessa. Se gli sviluppatori diventano dipendenti da strumenti e librerie di altri soggetti, possono trarre vantaggio dai progressi della piattaforma solo se gli altri soggetti decidono di adottarne le nuove funzioni. Non possiamo rimanere alla mercé di un soggetto terzo che decide se e quando intenderà rendere disponibili ai nostri sviluppatori i progressi della nostra piattaforma.</cite>

<cite>La situazione peggiora ulteriormente se il soggetto terzo fornisce uno strumento per lo sviluppo interpiattaforma. Potrebbe non adottare i progressi di una piattaforma a meno che non siano disponibili per tutte le piattaforme da esso supportate. Di conseguenza gli sviluppatori hanno accesso solo all'insieme di funzioni che costituisce il minimo comune denominatore. Ancora, non possiamo accettare un esito nel quale gli sviluppatori sono impossibilitati a utilizzare le nostre innovazioni e i nostri progressi dato che non sono disponibili sulle piattaforme dei concorrenti.</cite>

<cite>Flash è uno strumento di sviluppo interpiattaforma. L'obiettivo di Adobe non è aiutare gli sviluppatori a scrivere le migliori app per iPhone, iPod e iPad. È aiutarli a scrivere applicazioni interpiattaforma. E Adobe è stata tremendamente lenta nell'adottare le migliorie alle piattaforme di Apple. Per esempio, sebbene Mac OS X esista da quasi dieci anni, Adobe lo ha adottato pienamente (Cocoa) due settimane fa, con la pubblicazione di Creative Suite 5. Adobe è stata l'ultimo grande sviluppatore ad adottare pienamente Mac OS X.</cite>

<cite>Le nostre motivazioni sono semplici &#8212; vogliamo offrire ai nostri sviluppatori la piattaforma più avanzata e innovativa, e vogliamo che si appoggino direttamente alla piattaforma e creino le migliori</cite> app <cite>che il mondo ha mai visto. Vogliamo migliorare continuativamente la piattaforma, in modo che gli sviluppatori possano creare applicazioni ancora più straordinarie, potenti, divertenti e utili. Vincono tutti &#8212; noi vendiamo più apparecchi perché abbiamo le migliori app, gli sviluppatori raggiungono pubblico e utilizzatori in misura sempre maggiore e gli questi ultimi hanno sempre a disposizione la selezione migliore e più ampia di applicazioni su qualunque piattaforma.</cite>

<cite>Conclusioni.</cite>

<cite>Flash è stato creato durante l'era del personal computer &#8212; per computer e mouse. Flash è un'attività proficua per Adobe e possiamo comprendere perché vogliano spingerlo oltre i personal computer. Ma l'era</cite> mobile <cite>è fatta di apparecchi a bassa potenza, interfacce al tocco e standard web aperti &#8212; tutte aree dove Flash mostra la corda.</cite>

<cite>La valanga di distributori di media che offrono i propri contenuti per gli apparecchi</cite> mobile <cite>di Apple dimostra che Flash non è più necessario per guardare video o consumare qualsiasi genere di contenuto web. E le 200 mila</cite> app <cite>su App Store di Apple provano che Flash non è necessario a decine di migliaia di sviluppatori per creare applicazioni graficamente ricche, giochi compresi.</cite>

<cite>I nuovi standard aperti creati nell'era del</cite> mobile<cite>, come Html5, vinceranno sugli apparecchi</cite> mobile <cite>(e anche sui personal computer). Forse Adobe dovrebbe focalizzarsi maggiormente sulla creazione di grandi strumenti Html5 per il futuro e meno sul criticare Apple per lasciare indietro il passato.</cite>

<cite>Steve Jobs, aprile 2010</cite>