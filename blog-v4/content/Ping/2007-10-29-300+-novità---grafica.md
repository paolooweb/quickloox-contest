---
title: "300+ novità: grafica"
date: 2007-10-29
draft: false
tags: ["ping"]
---

<a href="http://www.playmediacompany.it/edicola/edicola-01.asp?Id=2115" target="_blank">Andre</a> mi diceva che la mia resa in italiano della pagina dedicata alle <a href="http://www.apple.com/it/macosx/features/300.html#graphics" target="_blank">novità di Leopard</a> non sarebbe certamente stata completata prima del lancio ufficiale di Leopard&#8230; e ha perfettamente ragione. Le modifiche grandi e piccole, infatti, sono fin troppo numerose.

Core Animation è una di quelle cose che nessuno vede, fino a che non se ne accorge. Vista la fioritura di programmi grafici di ogni genere? È perché in Tiger c'è Core Image. Tutti i programmatori lo possono usare. In Leopard è arrivato Core Animation. Se ne vedranno delle belle, dalle icone animate in poi. Di suo, Core Image si arricchisce di oltre venti filtri nuovi, tra cui sfocatura discoidale, effetto fumetto, puntinatura esagonale e altri.

OpenGl, la libreria grafica open source, è stata aggiornata. Sia OpenGl che Core Image che Core Animation sanno approfittare di un eventuale processore <em>multicore</em> a bordo. Più efficienza, più prestazioni.

Per chiudere, Leopard riconosce lo spazio di colore Exif, che significa migliore riproduzione a video delle immagini riprese dalle videocamere.