---
title: "Dell'usabilità"
date: 2009-03-13
draft: false
tags: ["ping"]
---

Scrive <a href="http://graficaedeliri.blogspot.com" target="_blank">Stefano</a>:

<cite>ti linko <a href="http://www.spoonfeddesign.com/usability-analysis-of-applecom-why-is-it-so-good" target="_blank">un'interessante analisi</a> (in inglese) uscita da poco, su un blog che seguo molto, dedicato alle caratteristiche che rendono usabile, bello da navigare e perfettamente organizzato il sito di Apple.</cite>

<cite>Dagli una letta, ne vale la pena!</cite>

Rigiro collettivamente l'invito. L'usabilità è una scienza negletta, perché serve a fare usare bene un sito agli altri. Invece lo sport preferito dalla maggioranza è fare siti divertenti da mettere in piedi, senza nessun rispetto per i visitatori.