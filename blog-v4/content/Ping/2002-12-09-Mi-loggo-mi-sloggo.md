---
title: "Mi loggo, mi sloggo"
date: 2002-12-09
draft: false
tags: ["ping"]
---

A lezione di troubleshooting in Mac OS X

Come Mac Os aveva qualche idiosincrasia (chi non ricorda, per esempio, l’errore 11, alla cui eliminazione fu praticamente dedicata una release di sistema?), ce le ha anche Mac OS X. Come con uno zio brontolone, ci si convive pacificamente, a patto di mettersi nei suoi panni.

La rotella, se gira, è meglio lasciarla girare. Molto spesso si può continuare a lavorare con un altro programma. Il fatto che possa girare anche per minuti interi non è sinonimo certo di blocco del sistema, che in genere continua a funzionare.

Se c’è qualcosa di strano, sovente un Log Out risolve tutto. Se non funziona, spesso uno spegnimento risolve tutto.

Se c’è un problema, creare un nuovo utente-cavia, effettuare il login come utente-cavia e provare a compiere la stessa operazione. Spesso non ci sono problemi e allora è qualche file di preferenze della nostra configurazione che non va. Se ci sono problemi anche per l’utente-cavia, invece, il problema riguarda il sistema, e così via.

Insomma, tanti problemucci si risolvono con un logout-login, o equivalente. Prima di disperarci, proviamolo.

<link>Lucio Bragagnolo</link>lux@mac.com