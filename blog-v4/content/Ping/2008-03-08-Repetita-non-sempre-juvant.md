---
title: "Repetita non semper juvant"
date: 2008-03-08
draft: false
tags: ["ping"]
---

Pensavo che il punto più basso lo avessi toccato quella volta che Chiara mi ha portato nel suo liceo artistico a fare lezione di Internet. Mi ero preparato tutta una cosa e poi ho scoperto che stavano lavorando al sito della classe con Word. Ho buttato tutto all'aria e mi sono messo a spiegare che cos'è l'Html e come funziona, ordinando a tutti di chiudere Word e aprire il Blocco Note. L'insegnante mi guardava come se avessi appena dichiarato guerra all'umanità a bordo del mio disco volante.

Sono passati almeno dieci anni. Oggi Chiara mi ha chiesto se potevo aiutarla a risolvere un problema su un sito che sta cercando di mettere in piedi. Ho guardato il codice. Fatto con Word. un disastro.

Dieci anni dopo, non è la stessa Chiara. La catastrofe è che è sempre lo stesso Word.