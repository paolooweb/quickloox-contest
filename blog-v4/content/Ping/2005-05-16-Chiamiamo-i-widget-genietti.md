---
title: "Il nome giusto per i widget?<p>"
date: 2005-05-16
draft: false
tags: ["ping"]
---

Questo suggerimento finora mi sembra il più carino. Ma la caccia continua<p>

Come chiamare i widget in italiano?<p>

Filippo Cervellera mi manda una proposta che a me piace particolarmente: <em>genietti</em>.<p>

Oltretutto evoca anche i daemon di Unix e, insomma, mi piace.<p>

Metodo scientifico: voto questo e vediamo se arriva di meglio!<p>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>