---
title: "Rss alternativi"
date: 2006-09-06
draft: false
tags: ["ping"]
---

Ricevo e con piacere pubblico da <strong>Mario</strong>:

<cite>Ho seguito il tuo consiglio e ho sostituito <a href="http://www.newsgator.com/NGOLProduct.aspx?ProdId=NetNewsWire&amp;ProdView=lite" target="_blank">NetNewsWire Lite</a> con <a href="http://www.opencommunity.co.uk/vienna2.php" target="_blank">Vienna</a>, che ho usato per un bel po'. Ha diversi lati positivi, ma uno negativo, almeno per il mio vetusto Mac: è molto lento nella selezione delle notizie, nel passare dall'una all'altra, nel segnarle come lette eccetera.</cite>

<cite>Ho cercato un po' su <a href="http://www.versiontracker.com/macosx" target="_blank">VersionTracker</a> e ho trovato diverse cose interessanti. Ho deciso però di restare solo sul freeware e la scelta si è un po' ridotta.</cite>

<cite>Ho provato e scartato subito <a href="http://www.rssowl.org/" target="_blank">RSSOwl</a>, con un certo dispiacere (è open source): è piuttosto macchinoso nell'interfaccia e non permette di ordinare i feed come si vuole, mantenendo rigorosamente l'ordine alfabetico.</cite>

<cite>Alla fine sono finito su <a href="http://pyxis-project.net/ensemble/index.php" target="_blank">Ensemble</a>. La homepage è un capolavoro di comicità, scritta in giapponese e tradotta in inglese da Altavista con risultati al di là di ogni immaginazione. Il programma però mi sembra ottimo, veloce e leggero. Permette di ordinare i feed in cartelle, ha le cartelle smart, permette di contrassegnare le notizie e anche di assegnare loro dei tag. Non è in italiano, ma penso che a te interessi poco. È freeware. Lo sto usando da qualche giorno e ne sono molto soddisfatto.</cite>

Di programmi non se ne hanno mai abbastanza. Grazie mille!