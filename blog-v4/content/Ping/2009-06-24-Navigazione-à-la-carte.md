---
title: "Navigazione à la carte"
date: 2009-06-24
draft: false
tags: ["ping"]
---

Un'altra novità dalla pagina delle novità di Snow Leopard che Apple non ha ancora voglia di tradurre.

<b>Navigare le cartelle nelle pile</b>

Si possono aprire cartelle dentro le pile e vederne tutti i documenti. Si torna rapidamente alle cartelle precedenti con un clic su un'icona di ritorno nell'angolo superiore sinistro della pila.

Abbiamo finito con il Dock. Da domani, novità a livello di sistema.