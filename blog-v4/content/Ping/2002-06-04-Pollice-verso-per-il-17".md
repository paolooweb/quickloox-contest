---
title: "Pollice verso per il 17”"
date: 2002-06-04
draft: false
tags: ["ping"]
---

Non crederci, non è realistico

Sono iniziati ufficialmente i rumor (pettegolezzi) via Web per gli annunci del prossimo Macworld Expo e uno dei più sugosi riguarda un nuovo iMac (Flat Panel) con schermo da 17”.
È anche uno dei più stupidi. In primo luogo Apple sta vendendo gli iMac attuali come il pane e non ha nessun bisogno, almeno ora, di rivedere tutta la linea di produzione per produrre un modello così diverso. Secondo, uno schermo Lcd da 17” ha costi improponibili per una linea consumer. Terzo, è dal 1998 che Apple ripete con coerenza che chiunque voglia uno schermo grande è liberissimo di acquistare un Power Macintosh G4 e prendersi il monitor che vuole.
Morale: se stai pensando a un iMac con schermo Lcd, sappi che lo schermo non cambierà. A dispetto dei rumoristi.

<link>Lucio Bragagnolo</link>lux@mac.com