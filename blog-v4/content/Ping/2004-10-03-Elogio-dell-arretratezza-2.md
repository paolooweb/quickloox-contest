---
title: "Elogio dell&rsquo;arretratezza/2<p>"
date: 2004-10-03
draft: false
tags: ["ping"]
---

A sostenere l&rsquo;adeguatezza dei vecchi Mac siamo almeno in due!<p>

Mi scrive l&rsquo;amico Daniele Savi:<p>

<cite>Ho preso settimana scorsa un &ldquo;vecchio&rdquo; PowerMac Dual 867 MHz MDD, macchina che a detta dei PCisti dovrebbe essere arretrata e inadatta ormai!</cite><p>

<cite>Invece mi ha sorpreso in positivo, è più potente di quanto pensassi, non è un G5 ma comunque con Final Cut Pro 4 e i vari plugin della Apple (come Compressor, che amo!) è una scheggia.</cite><p>

<cite>E non devo più installare, nell&rsquo;ordine: antivirus, antispyware, antiworm e cavolate varie che rendevano un 2GHz veloce quanto un 386.</cite><p>

<cite>L&rsquo;avevo già deciso due anni fa quando ho preso il mio primo Mac (un &ldquo;arretrato&rdquo; iBook G3 700 che fa ancora il suo lavoro più che egregiamente), ma questo lo conferma: non prenderò più un pc, a meno di non doverlo usare come router/server &ldquo;da poco&rdquo;, con Linux ovviamente. ;-)</cite><p>

Quando la gente ciancia dei prezzi di listino dei Mac non ne considera mai la maggiore durata. La verità è che chi usa Mac sa perché lo usa; chi usa Windows, spesso, non lo sa.<p>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>