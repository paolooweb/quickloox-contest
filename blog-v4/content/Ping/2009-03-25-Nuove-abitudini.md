---
title: "Nuove abitudini"
date: 2009-03-25
draft: false
tags: ["ping"]
---

Chiedo scusa a chi ha già da tempo uno unibody e probabilmente sa già tutto, ma mi sento di riferire certe sensazioni proprie del passaggio da PowerBook G4 a MacBook Pro.

Prima di tutto, l'interfaccia esterna allo schermo e alla tastiera. Quando si distacca o si inserisce l'alimentatore, i Led della batteria mostrano per un istante la carica. Quando il Mac è in stop c'è la consueta spia a segnalarlo con luminosità variabile. Adesso, però, quando il Mac è acceso e lo schermo è spento la stessa spia è accesa, fissa. È davvero utile.

La batteria. Non cambia il lavoro, cambia lo stile. Se sai che hai mezz'ora, tiri fuori il Mac e ti dimentichi dell'alimentatore. Se in una giornata vedi quattro clienti diversi e stai un'ora per ciascuno a computer acceso, nessun problema e nessun imbarazzo. Non entro più in una stanza cercando subito la posizione della presa più vicina.

Le <em>gesture</em> sulla <em>trackpad</em>. Le ho snobbate fino a quando non mi sono ritrovato a mostrare un giro di foto in Anteprima. Un gesto, non più un clic, e le foto scorrono naturalmente. Se una foto è girata di novanta gradi, due dita e il problema è risolto. Se serve l'ingrandimento di un particolare, altre due dita ed ecco fatto. È una questione umana; girare la foto con Comando-R è più facile che farlo ruotando le due dita. In compagnia di un'altra persona, ruotare le dita (senza unire le falangi, direbbe Elio!) rende tutto più umano e meno meccanico.

Quando il Mac si umanizza mi piace sempre un sacco, tutto qui.