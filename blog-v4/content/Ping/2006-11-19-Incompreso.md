---
title: "Incompreso"
date: 2006-11-19
draft: false
tags: ["ping"]
---

Al termine di una velocissima disamina degli avvenimenti della settimana, concludo che iCal si aggiudica con netta supremazia il titolo di programma più sottovalutato di Mac OS X.

Ha una quantità di menu veramente bassa rispetto ad altre applicazioni, ma può fare cose importanti, per esempio temporizzare un AppleScript oppure inviare mail di notifica di un evento, o invitare via email altri partecipanti a un evento eccetera eccetera.

Eppure l'utente medio non va oltre la normale impostazione degli eventi. Fosse un programma tipico direi che è colpa del programma, troppo difficile. Ma il numero di comandi di iCal è veramente ridottissimo.