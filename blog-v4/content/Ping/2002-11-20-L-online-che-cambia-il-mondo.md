---
title: "L’online che cambia il mondo"
date: 2002-11-20
draft: false
tags: ["ping"]
---

Chi sbaglia bersaglio rischia di colpirsi. Per una volta si fa politica

Per una volta, dopo centinaia di Ping, non si parla troppo di Macintosh, perché mio suocero ha un vecchio computer con Windows 98. È anche discretamente appassionato di calcio e odia la televisione accesa a cena.

Così, una sera, terminata la cena ha acceso il televisore per sapere i risultati, e si è accorto che c’era uno sciopero dei giornalisti sportivi. Che, per qualche motivo, quella sera hanno deciso di prendersela con mio suocero e tutti quelli come lui, per motivi che mi sfuggono.

“Guardiamo su Internet”, ho suggerito. Mi ha fissato un po’ dubbioso, perché lui non va oltre la posta elettronica elementare e il Web lo guarda con sospetto. Però mi ha ascoltato e nel giro di pochi minuti aveva i risultati, la cronaca dell’incontro, le foto salienti e i commenti dei tifosi. Molto più di quello che avrebbe trovato su Televideo.

“È la prima volta che uso Internet per avere notizie fresche”, ha detto. Attenzione, cari giornalisti scioperanti contro chi non c’entra, perché potrebbe non essere l’ultima. Nell’era della Rete non siete indispensabili. Neanche per mio suocero.

<link>Lucio Bragagnolo</link>lux@mac.com