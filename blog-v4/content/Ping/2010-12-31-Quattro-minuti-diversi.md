---
title: "Quattro minuti diversi"
date: 2010-12-31
draft: false
tags: ["ping"]
---

Ok, di test di velocità del <i>browser</i> ce ne sono molti. Quello di <a href="https://clubcompy.com/rwBench.jsp" target="_blank">ClubCompy</a> è particolarmente bizzarro. Per gli interessati, invito a esplorare il codice sorgente e scoprire nientemeno che <i>routine</i> grafiche in un antico Basic, di quando ancora si usavano le istruzioni numerate.

Per la cronaca, ho fatto 8.871 su Safari su MacBook Pro 17&#8221; (Early 2009).