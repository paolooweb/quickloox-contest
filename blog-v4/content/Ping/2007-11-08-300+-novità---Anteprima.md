---
title: "300+ Novità: Anteprima"
date: 2007-11-08
draft: false
tags: ["ping"]
---

Sono una novità, gli scorrimenti più fluidi? Per Apple s&#236; e non del tutto a torto, se non altro perché arrivano grazie a Core Animation, che è novità bella grossa.

Le annotazioni ai Pdf sono più versatili, con l'aggiunta di link ed evidenziazione del testo, e con l'inserimento del nome dell'autore delle annotazioni dentro quest'ultime.

Le ricerche all'interno dei Pdf sono fatte con la tecnologia di Spotlight, brava a calcolare le rilevanze.

I controlli di immagine di iPhoto sono percolati anche in Anteprima. le immagini si possono anche ritagliare e ridimensionare, oltre a poterne selezionare e copiare-incollare singole porzioni. L'applicazione, di suo, regola intelligemente i livelli di bianco e nero.

I comandi di rimozione sfondo ed estrazione contorno rendono facile isolare una forma rispetto al resto dell'immagine.

Si stampano rapidamente più immagini, o più copie di una immagine, su un singolo foglio.

Tornando ai Pdf, le singole pagine si possono spostare all'interno del documento, o eliminare. Per unire due Pdf in un documento unico basta un <em>drag and drop</em>.

Nella barra laterale, cliccare più immagini permette di applicare le stesse operazioni a tutte le immagini contemporaneamente. Le immagini si possono inviare a iPhoto o Aperture.

Se infine una foto contiene informazioni geografiche su dove è stata scattata, Anteprima è capace di mostrare la località su Google Maps.

Quanto sopra per le modifiche al programma elencate nella <a href="http://www.apple.com/it/macosx/features/300.html#preview" target="_blank">pagina riassuntiva</a> di Apple.