---
title: "Dentro e fuori"
date: 2009-12-14
draft: false
tags: ["ping"]
---

Chi lavora dentro l'animazione su Web potrebbe benissimo ricorrere a tecnologie di creazione più efficaci, per esempio <a href="http://www.toonboom.com/" target="_blank">Toon Boom Animate</a>.

Toon Boom Animate costa mille dollari ma offre all'animatore un sacco di possibilità che Flash, a settecento dollari, <a href="http://www.macsimumnews.com/index.php/archive/macsimum_review_flash_for_web_animation_try_toon_boom_animate_instead/" target="_blank">non ha</a>. Oltretutto permette di esportare in Flash, se proprio non è possibile farne a meno.

Contemporaneamente, chi non sa rinunciare ai giochini Flash se li potrebbe giocare gratis e con tutta comodità fuori dal <i>browser</i>, previo <a href="http://echoone.com/iswiff/" target="_blank">iSwiff</a>.