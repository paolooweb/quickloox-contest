---
title: "In parallelo con il passato"
date: 2008-07-28
draft: false
tags: ["ping"]
---

Paolo segnala la rassegna fotografica dei cugini di Computerworld Italia, che hanno <a href="http://www.cwi.it/gallery/14993/1" target="_blank">documentato la KansasFest</a> in cui si celebrano i fasti di Apple II.

Già la prima foto è notevole: diciassette schede logiche di Apple II (roba di trent'anni fa) messe in parallelo a creare un affascinante <em>cluster</em> di elaborazione.

L'altoparlante in primo piano però è moderno. Servirà a datare il servizio? :-)