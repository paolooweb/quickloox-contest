---
title: "Grandi e piccini"
date: 2011-01-03
draft: false
tags: ["ping"]
---

Leggo che Apple <a href="http://www.9to5mac.com/45660/aapl-market-cap-breaks-300-billion-barrier" target="_blank">ha superato i trecento miliardi di dollari</a> di capitalizzazione di mercato. Sopra di lei c'è solo Exxon.

Intanto scarico <a href="http://www.mailsmith.org/support/mailsmith/updates.html" target="_blank">Mailsmith 2.3</a>, nuova versione di un programma di posta che è divenuto gratuito per via dell'insostenibile concorrenza con Mail e però continua lo sviluppo e l'aggiornamento.

Leggo di una <i>app</i> per iPhone e iPad <a href="http://itunes.apple.com/it/app/barmax-ca-for-ipad/id409462195?mt=8" target="_blank">destinata agli aspiranti avvocati americani</a> dal costo di 799,99 euro.

Intanto scarico <a href="http://itunes.apple.com/it/app/typelink/id412347169?mt=8" target="_blank">TypeLink</a>, una delle tante <i>wiki</i> portatili su App Store, perché ritengo possa tornarmi utile. È gratis.

Mi piace questo mondo dove c'è spazio per cose enormi e incredibili e contemporaneamente si può progredire a piccoli passi, e può fare la differenza in mille modi tanto un miliardo di dollari quanto un <i>download</i> gratuito.