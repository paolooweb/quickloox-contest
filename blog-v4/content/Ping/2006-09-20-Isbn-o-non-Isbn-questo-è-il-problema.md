---
title: "Isbn o non Isbn, questo è il problema"
date: 2006-09-20
draft: false
tags: ["ping"]
---

Arranger1044 mi segnala con entusiasmo <a href="http://www.deepprose.com/" target="_blank">Booxter</a>, applicazione per tenere traccia della propria libreria personale, con riferimenti ai cataloghi Isbn accurati, possibilità di interfacciarsi con Amazon e molto altro, shareware a 19,95 dollari.

Adesso me lo guardo, per ora gli credo. :-)