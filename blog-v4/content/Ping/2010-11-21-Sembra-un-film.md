---
title: "Sembra un film"
date: 2010-11-21
draft: false
tags: ["ping"]
---

Se cinquant'anni fa avessero raccontato a qualcuno che le <i>linotype</i> sarebbero diventate il soggetto potenziale di un film (<a href="http://linotypefilm.com/" target="_blank">film indipendente che è possibile sovvenzionare</a>), sarebbe stato ben difficile crederci.

Se cinquanta settimane fa avessero raccontato a qualcuno che negli uffici dell'editore più grande del mondo sarebbe nato da zero un quotidiano studiato appositamente per la consultazione su tavoletta elettronica, sarebbe stato ancora più difficile.

Invece <a href="http://www.wwd.com/media-news?module=tn#/article/media-news/murdoch-does-another-daily-3385820" target="_blank">lo stanno raccontando</a>. Forse è vero e forse no, ma un giorno nei libri di storia il capitolo sull'evoluzione dell'editoria avrà pagine decisamente frizzanti.