---
title: "Esche d&rsquo;aprile<p>"
date: 2005-03-29
draft: false
tags: ["ping"]
---

Alternativa low-cost alla mancanza di idee e di umorismo altrui<p>

Una volta il pesce d&rsquo;aprile era una cosa seria. Byte pubblicava recensioni del nuovo word processor che avrebbe cambiato il mondo (una matita). Enrico Colombini annunciava la nascita del linguaggio ibrido tra Lisp e C: il Lisc. Per tacere dei collaboratori attuali di Macworld.<p>

Oggi non c&rsquo;è più lo spirito. La gente si incazza se perde sei minuti per uno scherzetto. Gli racconti che Tiger ha spinto Aqua al punto che non solo butti un widget sul desktop e quello galleggia, ma se ha lo <em>hook</em> programmatorio giusto puoi anche agganciarti a un processo che sta sotto, e ti danno dello stupido perché non gli hai dato una cosa utile, subito, gratis. Come sentono pesce pensano alla dieta e non sono disposti ad approfondire tematiche come l&rsquo;evoluzione di Aqua per Mac OS X 10.5 (nome in codice Deuterium).<p>

Fai così. Con la scusa del pesce, installa sul tuo Mac il demo di <a href="http://www.prolific.com/product/index.php?pltid=2">Goldfish Aquarium OSX</a> (anche <a href="http://www.prolific.com/product/index.php?pltid=3">Mac OS 9</a>) e intanto metti nell&rsquo;avvio automatico del vicino <a href="http://www.titanium.free.fr/english.html">System Prank</a>. E se si arrabbia fatti suoi, basta che non sia il capo.<p>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>