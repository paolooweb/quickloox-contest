---
title: "La pubblicità non è l'anima del download"
date: 2008-06-18
draft: false
tags: ["ping"]
---

Mi sono messo anch'io in coda assieme a non so quanti milioni di browser per scaricare Firefox 3 e contribuire a battere il record.

Perché è un ottimo browser alternativo; perché spesso mi serve tenere in linea due account Gmail diversi; perché il software open source è sempre un'ottima cosa; perché un browser di riserva serve sempre; perché c'è sempre un pizzico di gusto presenzialista che ti fa dire <em>c'ero anch'io</em> alla pausa caffè.

Poi qualcuno l'ha interpretata come una sorta di battaglia contro Microsoft e contro Explorer.

Il sottoscritto è una persona che si sveglia la mattina ed è contro Microsoft ancora prima di colazione, e rimane contro Microsoft anche la sera dopo essere andato a dormire.

E dice: non c'entra niente. Era un bel colpo pubblicitario per finire sui giornali. Perfettamente riuscito.

Alla fine sto scaricando <a href="http://www.mozilla.com/firefox" target="_blank">Firefox 3</a> ora, dopo averci provato del tutto inutilmente ieri sera causa saturazione dei server. La quale, da un punto di vista di lotta contro Explorer, è la cosa più controproducente del mondo.

Non serviva a fare scaricare, ma a fare (giustamente e positivamente) rumore.