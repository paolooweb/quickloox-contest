---
title: "Suonala ancora iTunes"
date: 2010-11-10
draft: false
tags: ["ping"]
---

Ma poi basta.

Se si fa doppio clic su una canzone nella libreria di iTunes, la riproduzione continua anche dopo che il brano è terminato e passa al brano successivo. L'unico modo per fermare la riproduzione è farlo manualmente.

Altrimenti si può cercare il singolo brano nel campo apposito, cos&#236; che solo lui compaia nella lista. Oppure infilarlo in una playlist apposita, che comprende solo lui. Tutta manualità un po' macchinosa e scomoda.

La soluzione è ugualmente macchinosa, ma si fa una volta sola. Si apre AppleScript Editor (nella cartella Utility dentro la cartella Applicazioni) e si inserisce nella finestra di lavoro il seguente <i>script</i>:

<code>tell application "iTunes" to play selection with once </code>

Poi si registra su disco lo <i>script</i> cos&#236; ottenuto con un qualunque  nome appropriato (per esempio l'ho chiamato <i>Play Once</i>, <i>suona una volta</i>; chi usa il sistema in italiano potrebbe chiamarlo, che so, <i>Suona una volta</i>).

Nel menu di iTunes si vede un'icona a forma di pergamena, contenente il comando <i>Open Scripts folder</i>. Si apre la cartella Scripts e gli si copia dentro lo <i>script</i>.

Fatto. Se un brano di iTunes ha nella vista libreria l'icona-altoparlantino di fianco al titolo e si seleziona il comando <i>Suona una volta</i>. Terminata la riproduzione, iTunes si ferma.

Certo che farlo da mouse e da quell'elenco di menu è un filo scomodo. Se ci fosse un comando da tastiera che aziona la riproduzione?

Detto fatto: si aprono le Preferenze di Sistema, zona Tastiera, sezione Abbreviazioni da tastiera. Nel riquadro di sinistra si clicca Abbreviazioni da tastiera e, sotto i due riquadri, il pulsante <i>+</i>.

Nel riquadro si indica che l'abbreviazione vale solo per iTunes selezionando il nome del programma dal menu, che il comando cui assegnare la scorciatoia di tastiera è <i>Suona una volta</i> (niente errori di digitazione!) e che la scorciatoia di tastiera è quella che verrà digitata mentre il cursore si trova nell'apposito riquadro.

Se tutto ha funzionato, la scorciatoia apparirà descritta nel riquadro di destra della finestra in Preferenze di Sistema e naturalmente apparirà accanto al comando di menu dentro l'elenco di comandi che parte dall'icona a forma di pergamena nella barra di menu di iTunes.

Effetti collaterali: questo modo di espandere e rendere più comode le funzioni di un programma si adatta a moltissimi altri programmi di Mac OS X e le cose che si possono fare sono letteralmente senza fine.

Ho usato con liberalità il materiale originale <a href="http://hints.macworld.com/article.php?story=20101109044926101" target="_blank">descritto da Mac OS X Hints</a>. Una descrizione visiva di tutto l'affare <a href="http://dougscripts.com/itunes/itinfo/shortcutkeys.php" target="_blank">è presente in Doug's AppleScripts for iTunes</a>, dove peraltro sta anche una miniera di script e cose interessanti riguardanti iTunes.

Con tutto questo meravigliarci di tavolette e interfacce al tocco viene naturale dimenticarsi della magia dell'automazione e di AppleScript, roba che sotto Windows si può realizzare solo da persone molto più esperte e disposte a smoccolare per mezze ore.