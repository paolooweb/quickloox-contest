---
title: "Uno sconto che (ri)anima"
date: 2006-12-05
draft: false
tags: ["ping"]
---

Me ne accorgo ora: Toon Boom Studio è in <a href="http://www.mupromo.com/?ref=3119" target="_blank">offerta</a> al 38 percento di sconto ancora per qualcosa più di dodici ore. Visto che il prezzo normale è 399 dollari, chi fa animazione ha solo da affrettarsi. :))