---
title: "Il piacere della matematica"
date: 2008-11-27
draft: false
tags: ["ping"]
---

Ringrazio di cuore i partecipanti alla terza pizzata Ping. Il mio pronostico di affluenza in numero primo è stato smentito. Però eravamo in numero (matematicamente) perfetto e forse è persino meglio cos&#236;. In numero primo sono invece andati distribuiti, tra l'altro, una raccolta di saggi di Arthur Clarke introvabile in Italia, un libro di introduzione alla crittografia scritto dai migliori autori italiani in tema e l'edizione originale americana da cui è stato tratto a suo tempo un libro di cento trucchi per Mac OS X.

In questi tempi di comunicazione di massa, diversi da quelli di un tempo solo perché adesso è la massa che comunica mentre prima si comunicava alla massa, il fatto che stessimo tutti allo stesso tavolo a portata di voce continua ad avere per me un valore inestimabile. Il giorno che faremo una pizzata-Ping e saremo in numero abbastanza elevato da fare s&#236; che qualcuno non riesca a parlare con qualcun altro per via della distanza, sarà ora di chiudere.

Intanto, grazie moltissime a tutti per l’estremamente piacevole serata. :-)