---
title: "Tiger contro Photoshop<p>"
date: 2005-05-01
draft: false
tags: ["ping"]
---

La concorrenza tra Adobe e Apple c&rsquo;è, ma non è quella semplice<p>

Numerose persone espertissime nel loro campo ma un po&rsquo; <em>na&iuml;f</em> nell&rsquo;informatica credono che Apple possa volere fare concorrenza ad Adobe e in particolare a Photoshop, mediante un&rsquo;evoluzione di iPhoto o cose simili.<p>

Un&rsquo;altra categoria di persone spera che Apple tiri fuori un concorrente di Photoshop di costo zero, così loro non dovranno più comprarsi Photoshop.<p>

Se a questi incorreggibili si raccomanda di usare <a href="http://gimp-app.sourceforge.net/">Gimp</a> che è già gratis, anche se non dà la soddisfazione di essere stati più furbi degli altri, ai na&iuml;f si fa notare che la concorrenza a Photoshop Apple la fa in altro modo. Tiger contiene Core Image, un set completo di direttive per la grafica. In questo modo un&rsquo;azienda può sfruttare tutto questo patrimonio di conoscenza di base in un suo programma e concentrare gli sforzi su funzioni più complesse e importanti, senza dover reinventare la ruota e riscoprire l&rsquo;acqua calda.<p>

Guarda un po&rsquo;, come è uscito Tiger è arrivato <a href="http://www.stone.com/iMaginator/iMaginator.html">Imaginator</a>. Un programma che prima di Mac OS X 10.4 pochissime software house avrebbero avuto le risorse per realizzare. Adesso è alla portata di tante.<p>

Da qui a tre anni Adobe, ma più che altro Photoshop, ne vedrà delle belle.<p>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>