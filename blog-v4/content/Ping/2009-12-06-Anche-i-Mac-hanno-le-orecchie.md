---
title: "Anche il Mac ha le orecchie"
date: 2009-12-06
draft: false
tags: ["ping"]
---

Ho scoperto con piacere che è finalmente disponibile una versione facilmente installabile e doppiocliccabile di <a href="http://www.wireshark.org/" target="_blank">WireShark</a> per Mac OS X (Intel).

WireShark è l'erede di Ethereal, uno degli analizzatori di traffico di rete <i>open source</i> storicamente più usati su Internet.

Difetti: parte in X11 e non cattura proprio tutti i protocolli come fa l'edizione Linux (l'unica che lo fa). Pregi: una interfaccia grafica, per quanto rudimentale, è molto meglio che mettere mano a <a href="http://www.macports.org/" target="_blank">MacPorts</a> o <a href="http://www.finkproject.org/" target="_blank">Fink</a>.