---
title: "A tirare troppo la corda"
date: 2008-06-27
draft: false
tags: ["ping"]
---

<a href="http://www.apple.com/it/macbookpro/features.html" target="_blank">Magsafe</a>, l'alimentatore magnetico di MacBook e MacBook Pro, è una bella idea che ha salvato molte vite (e magari ci avessero pensato ai tempi del mio Titanium).

<strong>Federico</strong> segnala che ci sono altri campi cui l'idea si applica vantaggiosamente, per esempio i <a href="http://www.replug.com/whatisit.php" target="_blank">jack audio di iPod e iPhone</a>.