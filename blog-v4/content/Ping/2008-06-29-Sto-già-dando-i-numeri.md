---
title: "Sto già dando i numeri"
date: 2008-06-29
draft: false
tags: ["ping"]
---

Non c'entra niente con il Mac e però devo ringraziare <strong>Carmelo</strong> del regalo graditissimo che mi ha consegnato in occasione della pizzata del Poc di venerd&#236; scorso.

Facciamo che ne cito un pezzo:

Si parte da zero. Carmelo aggiunge un numero da 1 a 10, e dice per esempio 7. Lucio aggiunge anche lui un numero da 1 a 10, e dice per esempio 15 (avrebbe potuto dire da 8 a 17). Carmelo aggiunge un numero da 1 a 10 e dice, sempre per esempio, 18. E avanti cos&#236;.

Vince chi può dire 100.

Come fa Carmelo a giocare in modo da essere sempre sicuro di vincere?

(che poi il Mac non c'entra, ma io aprirei Numbers&#8230;)