---
title: "Tesori esotici"
date: 2009-07-14
draft: false
tags: ["ping"]
---

Sempre a seguito della traduzione mancata di Apple delle <a href="http://www.apple.com/macosx/refinements/enhancements-refinements.html" target="_blank">novità di Snow Leopard</a>, anche oggi una di quest'ultime.

<b>Un tesauro tutto nuovo</b>

Snow Leopard comprende la seconda edizione dell'Oxford American Writer's Thesaurus. Nuove funzioni aiutano a distinguere tra parole facilmente confondibili, trovare la giusta sfumatura di significato, fornire contestualizzazione per scegliere la parola giusta e trasmettere informazioni sul retroterra delle parole attraverso le voci di autori conosciuti.

Ahimé, sarebbe molto bello trovare anche un dizionario italiano. Dovendo accontentarsi, è un buon accontentarsi: se questo Paese ha una piaga è l'indifferenza e la superficialità verso le lingue straniere, dall'inglese in poi.