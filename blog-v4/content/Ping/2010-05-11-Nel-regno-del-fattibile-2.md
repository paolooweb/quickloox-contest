---
title: "Nel regno del fattibile / 2"
date: 2010-05-11
draft: false
tags: ["ping"]
---

<cite>pregasi elencare azioni fattibili su un iPad e non fattibili su un Sony Vaio W da 10&#8221;</cite>.

Dare fino a <a href="http://mattgemmell.com/2010/05/09/ipad-multi-touch" target="_blank">undici comandi simultanei</a>.

Il gioco <a href="http://itunes.apple.com/us/app/plants-vs-zombies-hd/id363282253?mt=8" target="_blank">Plants vs. Zombies Hd</a>, per esempio, li usa tutti. Oggi si fa per un gioco, domani potrebbe essere qualsiasi altra cosa.

L’undicesimo comando, per il curioso, è l’appoggio del palmo della mano. Apple <a href="http://patft.uspto.gov/netacgi/nph-Parser?Sect1=PTO1&amp;Sect2=HITOFF&amp;d=PALL&amp;p=1&amp;u=%2Fnetahtml%2FPTO%2Fsrchnum.htm&amp;r=1&amp;f=G&amp;l=50&amp;s1=7,705,830.PN.&amp;OS=PN/7,705,830&amp;RS=PN/7,705,830" target="_blank">ha brevettato</a> quasi un centinaio di <i>gesture</i> <i>multitouch</i>.