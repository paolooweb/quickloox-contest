---
title: "Ogni moneta ha due facce"
date: 2008-05-26
draft: false
tags: ["ping"]
---

I nostalgici di HyperCard dovrebbero leggersi questo <a href="http://www.wired.com/gadgets/mac/commentary/cultofmac/2002/08/54370" target="_blank">pezzo di Wired</a> su Bill Atkinson, l'inventore del programma.

Sia per capire che HyperCard è finito ma non totalmente per colpa di Apple; lo stesso Atkinson riconosce che, se avesse pensato alla possibilità che uno <em>stack</em> linkasse schede presenti su diversi computer invece che su uno solo, avrebbe inventato il <em>browser</em> e il web anni prima di Tim Berners-Lee. Il World Wide Web è il vero <em>killer</em> di HyperCard.

Sia per capire che HyperCard è tuttora eccezionale: oggi Atkinson è apprezzato <a href="http://www.billatkinson.com/" target="_blank">fotografo naturalista</a> e tutta la sua attività professionale viene gestita da uno stack di HyperCard.

Ogni moneta ha due facce: Atkinson dà comprensibilmente parte della colpa della sua miopia dell'epoca all'ambiente di Apple, dove - parole sue - prima si progettavano i computer e poi si pensava alla rete. Se fossi stato in Sun, ha dichiarato, dove prima si progetta la rete e poi si pensa a che computer metterci, mi sarei trovato in un ambiente più favorevole.

È anche vero che, se fosse stato in Sun, col cavolo che gli avrebbero lasciato progettare un database amichevole grafico programmabile destinato a persone normali.

Oggi il link alla pagina di Apple su HyperCard <a href="http://en.wikipedia.org/wiki/HyperCard" target="_blank">rimanda direttamente a Wikipedia</a>. Occhio all'elenco finale dei software, c'è anche materiale interessante e moderno.