---
title: "Non c’è giustizia per il G5"
date: 2003-09-09
draft: false
tags: ["ping"]
---

Per credergli bisogna vederlo, per vederlo crederci

Il G5 avrà un brutto decollo e un viaggio fantastico. Quando è stato presentato molti lo hanno dato per brutto, salvo svenirgli davanti appena hanno avuto modo di toccare con mano.

Ora qualcuno tratta il nuovo processore come un qualsiasi upgrade dell’hardware, senza capire che è proprio tutta un’altra cosa e raramente è valsa così tanto la pena di pensare a un Mac, o a un nuovo Mac.

Senza contare che iniziano ad arrivare le tecnologie che Apple non se la sentiva di supportare da zero su Mac OS 9, come Usb 2.0.

Comincia, letteralmente, una nuova epoca e il G5, come certi protagonisti di feuilleton ottocenteschi, avrà giustizia. Con il tempo.

<link>Lucio Bragagnolo</link>lux@mac.com
