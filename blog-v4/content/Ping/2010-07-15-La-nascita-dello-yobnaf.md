---
title: "La nascita dello yobnaf"
date: 2010-07-15
draft: false
tags: ["ping"]
---

Una giornata spassosa per quanto sprecata nei commenti del <a href="http://attivissimo.blogspot.com" target="_blank">Disinformatico</a> mi ha fatto coniare il termine <i>yobnaf</i>.

Le persone che si autodefiniscono obiettive trovano conforto e divertimento nel denominare <i>fanboy</i> chi non la pensa come loro e, per definizione, i <i>fanboy</i> sono quelli che apprezzano un prodotto Apple oppure mettono in discussione le messe cantate dei siti-spazzatura che le sparano grosse per guadagnare di più. Il <i>fanboy</i> è un po' ragazzino brufoloso e stupido, un po' fanatico, in pratica uno che sbava senza bagliori di intelligenza dietro a qualche prodotto. L'uso comune del termine serve a etichettare chi compra Apple e si azzarda a violare qualche Sacro Luogo Comune.

Più queste persone &#8211; tutte laureate all'università del banale &#8211; si confrontano con i fatti, più si radicano nelle loro convinzioni, usualmente all'altezza della chiacchiera al bancone del bar.

Ossia diventano esattamente tali e quali ai <i>fanboy</i> che vedono dappertutto (quando uno crede di capire che scrivere <i>fanboy</i> dia automaticamente ragione, per questi cervelli sotto lo standard la tentazione di usare la parola per tacitare qualsiasi discussione diventa inevitabile).

Insomma, <i>fanboy</i> allo specchio. <i>yobnaf</i>.