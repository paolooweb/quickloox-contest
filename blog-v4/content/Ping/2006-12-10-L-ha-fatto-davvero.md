---
title: "L'ha fatto davvero"
date: 2006-12-10
draft: false
tags: ["ping"]
---

<strong>Giuseppe</strong> ha effettivamente mostrato il PowerBook a un solo-Windows. Ecco la sua cronaca.

<cite>La prima nota è lo stupore del mammifero per la cura estetica del PowerBook: era la prima volta che vedeva un Mac&#8230; pur avendone parlato male da quando lo conosco. [&#8230;]</cite>

<cite>Il secondo step è stato attivare la funzione di ingrandimento del Dock e mostrargli il suo futuro&#8230;</cite>

<cite>Quando gli mostro la funzione di contrazione delle finestre ed il suo slow motion rimane stupefatto&#8230;</cite>

<cite>Io gli spiego che posso lanciare le applicazioni con diversi metodi e che posso automatizzare parecchie cose affinchè sia il Mac a lavorare per me. Lui comincia ad essere consapevole della propria schiavitù nei confronti del suo PC.</cite>

<cite>Il terzo dettaglio è la domanda: &#8220;Hai Word?&#8221;</cite>

<cite>E ci può anche stare&#8230; gli rispondo aprendo Nisus Writer Express e salvando un documento in .doc ; poi apro altri editor di testo&#8230; [&#8230;]</cite>

<cite>La quarta nota è la domanda: &#8220;Hai Excel?&#8221;</cite>

<cite>Gli rispondo lanciando Mariner Calc.</cite>

<cite>Poi, come resumé, gli mostro OpenOffice&#8230;</cite>

<cite>Gli faccio notare che tutte le applicazioni che gli ho mostrato salvano in più formati, non solo .doc o .xls o .billgates!</cite>

<cite>Gli domando se vuole vedere dell'altro o se lui usa solo quei due programmi, cos&#236; gli mostro tutte le applicazioni del PowerBook: una sequenza molto vasta e gli faccio notare che in totale occupano meno di un giga e che si tratta di freeware o shareware. Lui viene incuriosito da Nti Dragon Burn e lo cerca sul suo peecee attraverso Emule. Gli ho detto di averlo pagato 10 dollari, ma lui pretende di trovarlo crackato. Trova solo file .dmg e non pensa che .exe per lui è una regola, ma - vabbè - del resto lui fa parte della &#8220;Maggioranza Intelligente che usa PC&#8221;&#8230;</cite>

<cite>Gli dico che avrebbe fatto prima cercando su http://www.versiontracker.com/windows, ma&#8230; niente: lui è ostinato. Dice che ha sempre fatto cos&#236;&#8230; [&#8230;]</cite>

<cite>Dopo 10 minuti di insistenza, cede e cerca su VersionTracker. Dopo pochi secondi trova l'analogo per PC, che però costa uno sproposito&#8230; ;-) [&#8230;]</cite>

<cite>Parliamo di virus e di intercomunicabilità tra sistemi operativi. Qui è Mac 10 - PC 0. [&#8230;]</cite>

<cite>Devo confessare che pensavo di divertirmi di più, ma le forme di vita obsolete non mi trasmettono più grandi sensazioni.</cite>

A Natale siamo veramente tutti più buoni. :))