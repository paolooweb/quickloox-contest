---
title: "Il contrario di Pdf"
date: 2003-12-18
draft: false
tags: ["ping"]
---

Da qualunque cosa a Pdf: fatto. Da Pdf a qualunque cosa, invece…

Se c’è una cosa che amo di Mac OS X è che tutto diventa un Pdf nel giro di due clic.
Ma non c’era niente di usabile da tutti per partire da un Pdf e finire con un documento di altro tipo.

Bene, è arrivato PDF2Office 1.0, di Recosoft. Costa 99 dollari e converte i file Pdf in documenti Word, AppleWorks, Html o Rtf, pienamente editabili, resi il più possibile identici al Pdf di partenza, comprese immagini, tabelle, intestazioni, pié di pagina, colonne e altro. Devo ancora provarlo ma sono già contento.

<link>Lucio Bragagnolo</link>lux@mac.com