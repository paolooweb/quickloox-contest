---
title: "Il Web si antievolve"
date: 2006-11-12
draft: false
tags: ["ping"]
---

Un sito sta cercando tre collaboratori esterni <cite>per la collaborazione</cite> (non fosse stato scontato) <cite>alla redazione delle</cite> proprie <cite>edizioni quotidiane</cite>.

<cite>Tutti i candidati</cite>, tra l'altro, <cite>dovranno avere indispensabilmente</cite>:

- <cite>proprietà nella scrittura e nel linguaggio giornalistico</cite> (grazie al cielo);

- <cite>disopponibilità a scrivere articoli giornalistici con frequenza e costanza</cite>.

Stante che il pollice opponibile è proprietà dell'uomo e delle scimmie, sono candidati ideali mammiferi inferiori, o rettili.

Darwin non è solo la parte open source di Mac OS X. :-)