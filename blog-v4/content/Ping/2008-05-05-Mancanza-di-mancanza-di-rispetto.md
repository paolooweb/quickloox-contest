---
title: "Mancanza di mancanza di rispetto"
date: 2008-05-05
draft: false
tags: ["ping"]
---

John Gruber ha cambiato la <a href="http://daringfireball.net/2006/02/bedecked" target="_blank">gestione della pubblicità</a> sul proprio sito Daring Fireball.

Le sue nuove regole sono le seguenti:

- solo una inserzione per pagina, con grafica di 190x120;
- solo inserzioni relative a prodotti che ha acquistato o che usa;
- numero di inserzionisti limitato e nessun inserzionista privilegiato;
- vietata qualsiasi animazione.

Sembrava impossibile trovare un sito con un minimo di rispetto per i lettori e tuttavia capace di mantenersi. Il vento sta cambiando. Vuoi vedere che il web inizia a maturare e tra vent'anni i siti-spazzatura finiranno dove meritano?