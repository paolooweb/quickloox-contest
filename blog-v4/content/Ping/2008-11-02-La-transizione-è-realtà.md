---
title: "La transizione è realtà"
date: 2008-11-02
draft: false
tags: ["ping"]
---

A fine settembre 2008, <a href="http://apple20.blogs.fortune.cnn.com/2008/11/01/october-internet-use-vista-up-mac-down/" target="_blank">traffico web</a> alla mano, i Mac con processore Intel erano il 5,8 percento del totale; i Mac con processore PowerPc il 2,43 percento.

A fine ottobre, i Mac Intel erano saliti al 5,94 percento (+0,14 punti percentuali) e i Mac PowerPc erano scesi al 2,27 percento (-0,16 punti percentuali).

Se fosse un <em>trend</em>, a fine novembre i Mac PowerPc sarebbero meno di un quarto della base installata che va regolarmente su web, direi poco distante dalla base installata effettiva. Sempre se fosse un <em>trend</em>, da qui a - tipo - settembre 2009 i Mac PowerPc sarebbero una frazione irrilevante del totale.

Da fiero proprietario e utilizzatore di Mac PowerPc, che senza World of Warcraft sarebbe ancora al Titanium, anche abbastanza irritato per la decisione di pubblicare il Software Development Kit di iPhone solo per Intel, devo ammettere che, se davvero Snow Leopard funzionasse solo per Intel come sembra, sarebbe sensato.