---
title: "Bug University"
date: 2009-10-31
draft: false
tags: ["ping"]
---

Al termine di un brutto <i>crash</i> di Mac OS X mi ritrovo con BBEdit che funziona perfettamente, con la sola eccezione della finestra di dialogo Anchor, fatta per applicare <i>link</i> ai testi in modo semiautomatico.

Nella finestra in questione funziona tutto, ma l'impostazione del parametro Target viene ignorata. Il parametro è quello che, nell'esempio più comune, fa s&#236; che il clic su un <i>link</i> apra la pagina in una nuova finestra.

È una cosa assurda e stiamo ridendo come matti, io e il supporto di Bare Bones che ovviamente non sa che pesci pigliare. Ho installato da capo il programma prima che me lo consigliassero loro, la cosa più logica da tentare, e nessun risultato.

Sto effettuando prove su prove per riuscire a decifrare l'enigma e di cose strane ne ho viste, ma come questa nessuna.

Credo che alla fine avrò anche imparato molto. Può essere positivo anche un <i>bug</i> inspiegabile.