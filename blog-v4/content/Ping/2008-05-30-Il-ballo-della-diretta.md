---
title: "Il ballo della diretta"
date: 2008-05-30
draft: false
tags: ["ping"]
---

Sono in @Work a sentire Patrick Luby e Ed Peterlin di NeoOffice, che parlano della loro creatura.

L'evento viene trasmesso <a href="http://mogulus.freesmug.org" target="_blank">in diretta via Internet</a>, con audio e video, e chat a margine.

È affascinante come si stia creando l'interazione tra la gente in sala e quelli che vedono via browser. Si chatta e si parla e improvvisamente essere seduti a un metro dai programmatori, o diecimila chilometri, conta pochissimo.

Mentre si fanno le diciotto intanto il pubblico aumenta. Stanno anche arrivando i pezzi grossi come Antonio di Nova (Sole 24 Ore) e Italo, Pr di OpenOffice. Paolo Attivissimo ha rotto la macchina ma è in chat.

Stasera alla pizzata ci sarà da divertirsi.