---
title: "P2P, C2C<p>"
date: 2005-03-29
draft: false
tags: ["ping"]
---

Peer-to-peer e malignità sull&rsquo;erba del vicino (e sulle ragazze)<p>

Dietro ci sarà anche un dato tecnico, ma mi sento di dubitarne, almeno data la frequenza con cui noto il fenomeno.<p>

Che è il seguente. Se c&rsquo;è un tipo di programma che la gente cambia con uan frequenza allucinante, è il suo peer-to-peer. Quelle cose tipo Acquisition, LimeWire, Xnap, aMule, eDonkey eccetera.<p>

Sarò cattivo, ma mi sembra poco questione di tecnologia e più questione di mode. È uscito un nuovo client e tutti ne parlano bene, e poi &ldquo;si trova di più&rdquo;. Allora bum, tutti lì sopra. Non è escluso che passando in massa da un programma all&rsquo;altro effettivamente la rete del programma nuovo possa anche diventare più redditizia nel breve periodo, ma viene da essere scettici.<p>

Il comportamento mi ricorda quello di certe ragazze in macchina, quando sentono un motivo sgradito sulla radio e chiedono a gran voce di cambiare stazione fino a trovare una canzone gradita. Solo che le canzoni sono sempre quelle e l&rsquo;effetto finale è perdere un sacco di tempo con pochi risultati.<p>

Le ragazze però si sono fatte furbe e hanno un iPod da collegare allo stereo più vicino. Gli adepti del P2P mi sembrano invece ancora inclini al C2C: cambiare tanto per cambiare.<p>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>