---
title: "If ain&rsquo;t broken, don&rsquo;t fix it<p>"
date: 2004-10-10
draft: false
tags: ["ping"]
---

È il computer che deve lavorare per noi, non il contrario<p>

Se si somma tutto il tempo buttato a pensare alla deframmentazione, alla riparazione dei privilegi, alla ricostruzione delle directory, alla cancellazione delle cache e ad altre tipiche attività cosiddette di manutenzione mi sa che viene fuori un punto di Pil.<p>

Sono ottime attività; ma vanno praticate quando c&rsquo;è un problema, per risolvere il problema. Considerarle alla stregua della pulizia dello schermo significa, i problemi, portarseli in casa.<p>

Il computer è in grado di badare a sé stesso per la gran parte del suo tempo. Occasionalmente può avere bisogno di intervento, sì. Occasionalmente.<p>

Consiglio: abbina il computer alla lavatrice. Fai la messa a punto dell&rsquo;uno quando la fai dell&rsquo;altra. Non sbagli.<p>

Come dicono gli americani, se non è rotto, non va riparato.<p>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>