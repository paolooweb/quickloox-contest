---
title: "Bel voto, cinque"
date: 2009-11-08
draft: false
tags: ["ping"]
---

Non c'è ancora la sufficienza e però l'alunno si dimostra sempre più brillante.

È stato realizzato <a href="http://neosmart.net/YouTube5/" target="_blank">un visore di YouTube in Html 5</a>. Funziona in Safari e persino nel <i>browser</i> di Vienna.

Esiste già <a href="http://neosmart.net/YouTube5/youtube5.user.js" target="_blank">uno script GreaseMonkey/UserScript</a> che, aggiunto a una pagina web, mostra il filmato tramite Html 5 e rende inutile il <i>plugin</i> Flash.

Trascinare <a href="javascript:void(location.href='http://neosmart.net/YouTube5/index.php?url='+location.href)" target="_blank">questo link</a> nella barra dei pulsanti di Safari semplifica ulteriormente il tutto (trascinare, non cliccare!). Quando c'è un video YouTube da guardare, se proprio non si può fare a meno di buttare il tempo, si usa il pulsante cos&#236; ottenuto <i>et voilà</i>, le gioie di YouTube senza l'influenza flashina.