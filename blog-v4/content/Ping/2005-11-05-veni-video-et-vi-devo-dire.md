---
title: "Veni, video, et vi devo dire<p>"
date: 2005-11-05
draft: false
tags: ["ping"]
---

Ci credo quando lo vedo, ma intanto lo sento e lo devo dire<p>

Quando Steve Jobs ha presentato i nuovi iPod, fatti per memorizzare anche il video, sono rimasto molto scettico di fronte al fatto che la risoluzione video supportata non va oltre i 320 x 240 pixel.<p>

A quanto pare peccavo di ignoranza. La risoluzione reale dei vari segnali televisivi e filmici  è tutto sommato paragonabile. Se Apple ha istituzionalizzato finora solo il commercio di videoclip e trasmissioni televisive, persone degne della massima fiducia mi garantiscono di avere ripetutamente codificato film veri e propri in 320 x 240 con il codec H.264 e di averlo proiettato con buonissimo risultato qualitativo su televisori di dimensioni annche molto grandi.<p>

Continuo a essere scettico e credo che non sia questa la strada, considerato che bene o male si viaggia, per il futuro, verso la tivù ad alta definizione. Tuttavia sono molto curioso di verificare di persona come rende il video di iPod su grande schermo, pronto a riconoscere il giudizio affrettato se sarà il caso.<p>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>