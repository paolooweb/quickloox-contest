---
title: "Ergonomia da marciapiede"
date: 2002-03-19
draft: false
tags: ["ping"]
---

C’è gente disposta ad acquistare veramente qualsiasi cosa, anche quando è scomoda

Passo dal supermercato a fare la spesa e vedo, a prezzo da svendita dei tappeti, un portatile Windows, in mezzo ai salami e ai prosciutti.
Basta guardarlo per capire che vale quanto costa, cioè niente, e per soprammercato noto la trackpad. Per ragioni incomprensibili è clamorosamente spostata verso sinistra, cioè nella posizione più scomoda da usare per il 90% degli utenti. Apple, piuttosto attenta a certi dettagli, ha sempre posizionato le trackpad e le trackball al centro; nel mondo Wintel invece c’è il più totale disinteresse per chi dovrà poi usare veramente i disastri che escono dalle fabbriche.
Trattandosi di design di portatili, è proprio il caso di dire che è ergonomia sì. Ma da marciapiede.

<link>Lucio Bragagnolo</link>lux@mac.com