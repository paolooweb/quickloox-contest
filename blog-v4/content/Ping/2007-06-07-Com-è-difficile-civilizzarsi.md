---
title: "Com'è difficile civilizzarsi"
date: 2007-06-07
draft: false
tags: ["ping"]
---

Gente, <a href="http://www.freeciv.org/index.php/Freeciv" target="_blank">FreeCiv</a> è proprio complesso. E appassionante. All'inizio non ci capisci niente, poi inizia a capire e a combinare qualcosa. Fondi le prime città senza capire troppo bene che stai combinando. Poi ti rendi conto che le città sono da amministrare. Che gli obiettivi di produzione sono tantissimi e diversificati. Che i rapporti diplomatici con le nazioni confinanti sono una variabile cruciale. Che le strade sono importanti. Che il progresso scientifico pure.

A un certo punto vieni spazzato via dai barbari, che non sviluppano la tecnologia e non pensano all'irrigazione dei campi, ma hanno una meta ben precisa e la perseguono spietatamente. E ti accorgi quanta fatica ci vuole perché la civiltà fiorisca.

Non amo particolarmente le simulazioni. Oggi, però, scaduto il mio tempo a disposizione, ho chiuso la partita e avrei voluto proprio tanto continuarla. Mi sa che avrei potuto andare avanti fino a sera. Magari avrei anche attraversato prosperando l'Età del Ferro, chissà. Avessi figli alle medie, cercherei di giocarci insieme a loro.