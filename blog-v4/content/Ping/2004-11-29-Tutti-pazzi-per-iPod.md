---
title: "Tutti pazzi per iPod<p>"
date: 2004-11-29
draft: false
tags: ["ping"]
---

Spudorato spot pubblicitario e una considerazione sull&rsquo;effetto-iPod<p>

È finalmente in tutte le librerie il mio libro su iPod, che si chiama come vedi nel titolo e viene edito da <a href="http://www.tecnichenuove.com">Tecniche Nuove</a>.<p>

A traino del libro ho aperto una mailing list dedicata a iPod e dintorni, chiamata prevedibilmente Tuttipazziperipod. Tutto gratuito e non c&rsquo;è bisogno di comprare niente; semplicemente ci si può iscrivere inviando una <a href="mailto:tuttipazziperipod-subscribe@yahoogroups.com">mail</a> qualsiasi.<p>

Già che siamo qui, uno studio di News.com è arrivato alla conclusione che il successo di iPod potrebbe fare vendere ad Apple la bellezza di 700 mila Mac in più nel 2005. La gente che usa Windows e compra iPod si rende conto, e passa a Macintosh.<p>

Altro che tutti pazzi; iPod fa ragionare.<p>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>