---
title: "Gita fuori porta (su Giove)"
date: 2007-04-20
draft: false
tags: ["ping"]
---

Posso pubblicare per Pasquetta poco più che la cronaca di una parte della Pasqua. Il papà appassionato di astronomia e viaggi e lo zio attento alla tecnologia sono stati sollazzati da Celestia.

<a href="http://earth.google.com" target="_blank">Google Earth</a> e <a href="http://stellarium.sourceforge.net/" target="_blank">Stellarium</a> fanno già parte del corredo e Google Earth è servito in particolare per intraprendere una scampagnata fuori porta. A Nazca, a vedere i famosi disegni visibili da altezze inconsuete.

Non certo inconsuete per il satellite, però. I disegni più grandi e meno caratterizzati si vedono facile. Ma le scimmie, i colibr&#236;, le spirali devi cercarle con il lanternino e se non sai esattamente dove sono, e non ingrandisci al massimo (prima di sgranare) esattamente l&#236;, ti attacchi.

All&#236;'orbita di Giove, chiaro. Con <a href="http://celestia.sourceforge.net/" target="_blank">Celestia</a>.