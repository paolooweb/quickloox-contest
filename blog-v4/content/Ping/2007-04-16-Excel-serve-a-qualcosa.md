---
title: "Excel serve a qualcosa"
date: 2007-04-16
draft: false
tags: ["ping"]
---

Me lo ha segnalato <strong>Pierfausto</strong>. Ho provato qualcosa su OpenOffice e funziona e non funziona, quindi il mio interesse individuale è rapidamente scemato.

Un sacco di altra gente, invece, ha sicuramente il problema di fare rendere tutti i soldi messi in quel foglio di calcolo onnipotente e superfluo. La risposta c'è: <a href="http://excelgames.org/" target="_blank">giocare</a>.