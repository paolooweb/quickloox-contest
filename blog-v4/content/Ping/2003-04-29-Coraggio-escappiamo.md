---
title: "Coraggio, escappiamo"
date: 2003-04-29
draft: false
tags: ["ping"]
---

L’interfaccia utente si insinua in qualsiasi interstizio

Confesso che non ci credevo, quando l’ho scoperto. Confesso altresì che non riesco a capire a che serva, se non in casi davvero particolari.

Tuttavia confermo che su Mac OS X si può interrompere un trascinamento in corso, premendo il tasto Esc. unico requisito: il trascinamento non deve essere stato completato, ossia il tasto del mouse deve essere ancora premuto, con l’icona del o dei file ancora attaccata al puntatore del mouse.

È inutile, più cerco di spiegarlo con precisione più faccio confusione. Si fa prima a toccare con mano, pardon, con tasto Esc.

<link>Lucio Bragagnolo</link>lux@mac.com