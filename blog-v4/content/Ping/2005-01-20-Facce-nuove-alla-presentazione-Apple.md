---
title: "Vento di evento<p>"
date: 2005-01-20
draft: false
tags: ["ping"]
---

Le presentazioni di Apple alla stampa non sono più le stesse<p>

Apple ha presentato alla stampa italiana gli annunci del Macworld Expo, da iLife &rsquo;05 a iWork al nuovo hardware.<p>

Anni e anni fa, chi più chi meno, le facce erano sempre quelle.<p>

Stavolta c&rsquo;era la solita gente ma anche un sacco di facce nuove e molto più interesse genuino del solito. Gente che toccava un po&rsquo; incredula la grande piccolezza di Mac mini e rimaneva ipnotizzata da iPod shuffle, che fa impressione e sembra quasi magico per quanta raffinatezza è stata messa in un oggetto grande come un pacchetto di gomme da masticare.<p>

Il reparto pr di Apple è anche molto più sveglio che nel recente passato. Un voto bello alto va a Carla Targa e Tiziana Scanu per il gran lavoro che stanno svolgendo nel fare conoscere, e conoscere meglio, Apple ai media italiani.<p>

Il vento, per la Mela, sta soffiando dalla parte giusta.<p>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>