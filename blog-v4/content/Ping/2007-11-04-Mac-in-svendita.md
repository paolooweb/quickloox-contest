---
title: "Mac in svendita"
date: 2007-11-04
draft: false
tags: ["ping"]
---

Tutti a comprare un iMac o un MacBook alla Fnac. Si risparmiano ben… <a href="http://www.fnac.it/it/Default.aspx?cIndex=7&amp;catalog=hardware&amp;categoryN=Informatica" target="_blank">90 centesimi</a>.

Grazie a <a href="http://www.gamestar.it/blogs/roots/" target="_blank">Paolo</a> per la segnalazione.