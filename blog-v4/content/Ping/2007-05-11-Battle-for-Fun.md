---
title: "Battle for Fun"
date: 2007-05-11
draft: false
tags: ["ping"]
---

Il primo venerd&#236; del gioco è andato brillantemente deserto. :-)

Ringrazio moltissimo <strong>elrond1984</strong> che è arrivato su un altro fuso orario, quando oramai mi toccava lavorare, e rassicuro quanti temessero per la mia salute psichica: sul server ufficiale di <a href="http://www.wesnoth.org" target="_blank">Battle for Wesnoth</a> c'erano decine di persone e divertirsi per un'ora in compagnia non è stato affatto un problema.

Lo scopo comunque è quello di giocare, giocare, giocare con un gioco diverso ogni volta, e vediamo quando finiscono, visto che su Mac ce ne sono cos&#236; pochi (a proposito: sono sempre benvenute le segnalazioni, qualunque genere giocabile online multiplayer).

Il gioco di venerd&#236; prossimo - dopo <strong>Tommaso</strong>, do retta a <strong>Riccardo</strong> - è <a href="http://www.prairiegames.com/" target="_blank">Minions of Mirth</a>. Essendo un gioco di ruolo online la competizione sarà sui generis; l'obiettivo di massima è fare un po' comunella su un mondo diverso da questo, e vedere fino a dove si riesce ad arrivare.

Occhio che il download è di 260 mega. Venerd&#236; 18 maggio 2007 il primo che arriva accende la stanza di iChat gamefriday e da l&#236; ci si coordina (e si chiacchiera pure intanto che si gioca). Orario di gioco, dalle 13 alle 14. Chi c'è c'è, chi non c'è gli auguro di divertirsi comunque. Chi c'è lo fa sicuro.