---
title: "Un&rsquo;era alla fine<p>"
date: 2005-08-25
draft: false
tags: ["ping"]
---

Che cosa non si fa per un bel gioco<p>

Il mio tempo ludico (invero poco) è ultimamente dedicato a World of Warcraft, ma una diversione me la prendo, per vedere il demo di <a href="http://mystworlds.ubi.com/us/">Myst V: End of Ages</a>.<p>

Della serie Myst ho giocato il primo episodio. Bellissimo, tanto che conquistò anche mio padre. Non era interessato al gioco, ma solo a gironzolare per quel mondo affascinante e fantastico. Non sarei mai riuscito a risolvere l&rsquo;enigma dell&rsquo;ascensore senza mio fratello, ma è un&rsquo;altra storia.<p>

Poi il tempo non è più bastato, per giochi così impegnativi. Ricordo che in Riven ha una grossa importanza il numero cinque, e il rumore di chi bussa su una porta di legno, e una bambina che compariva di tanto in tanto.<p>

Non avrò tempo per giocare Myst V, ma dalle premesse sembra proprio bello e almeno il demo lo voglio vedere. Per la Fine delle Ere ci vuole pazienza: pesa mezzo gigabyte e per scaricare il demo ci vuole, sì, un&rsquo;era. Ma la mia Adsl non ha nient&rsquo;altro da fare e quindi le ho affidato il compito senza sentirmi in colpa.<p>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>