---
title: "AppleScript e il tempo presente"
date: 2009-10-29
draft: false
tags: ["ping"]
---

Non in risposta ma a completamento del <a href="http://www.macworld.it/blogs/ping/?p=3005&amp;cp=1#comment-37534">commento di Paolo</a>, Xcode non contiene più AppleScript Studio, ma in compenso offre la possibilità di creare applicazioni cosiddette AppleScript-Cocoa. Sempre roba da programmatori che sanno programmare e sanno usare Xcode, ma anche AppleScript Studio non era cos&#236; diverso.

Con Snow Leopard, Apple ha puntato molto su Automator, per via del fatto che è possibile crearsi da sé i Servizi per Mac OS X. Automator supporta naturalmente AppleScript.

Insomma, con Snow Leopard non è che Apple abbia rivoluzionato AppleScript o abbia introdotto chissà cosa, a parte consolidare Script Editor e Utility AppleScript in AppleScript Editor. Però, in un certo senso, tramite Automator e i nuovi Servizi ha messo un minimo di incentivo in più a capirne qualcosa.

Considerato che lo stesso Snow Leopard è un raffinamento di Mac OS X 10.5 più che un'edizione completamente diversa del software di sistema, non direi che AppleScript è stato messo da parte. Non questa volta, almeno.