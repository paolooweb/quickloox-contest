---
title: "Accetti il Risk?"
date: 2007-05-25
draft: false
tags: ["ping"]
---

Il gioco di venerd&#236; prossimo è <a href="http://jrisk.sourceforge.net/" target="_blank">Jrisk</a>. Strategia a turni. Gratis.

Le regole sono le solite: appuntamento venerd&#236; 25 maggio 2007 dalle 13 alle 14, stanza <em>gamefriday</em> su iChat per coordinarsi (Comando-Maiuscole-G per aprire la stanza o per entrare).

Le regole di Jrisk, invece, non sono esattamente quelle del Risiko italiano e privilegiano il gioco offensivo. Sei avvisato e hai tempo per farci la mano. :-)

Jrisk non ha server centrali e potrebbero sorgere le difficoltà legate al farsi vedere (e vedere gli altri) da FastWeb. Non so ancora dove mi troverò in quel momento e a quell'ora. Se ci sono suggerimenti o proposte sono benissimo accettate, altrimenti vedremo al momento.