---
title: "Finalmente uno"
date: 2007-07-10
draft: false
tags: ["ping"]
---

Finalmente anche a me è morto un Cd-Rom. Era stato masterizzato nel 2001 o nel 2002, vecchio backup di una Creative Suite Adobe.

Ho preso atto e ho installato (per fortuna non sul mio Mac) dal disco di backup della Creative Suite più nuova.

Sapere che un backup è morto, ma che tanto era inutile, rilassa. :-)