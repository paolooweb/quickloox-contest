---
title: "Soluzioni nascoste"
date: 2009-05-10
draft: false
tags: ["ping"]
---

Sono passato per tutt'altra ragione su <a href="http://macscripter.net/" target="_blank">MacScripter</a> e ho cliccato su <a href="http://scriptbuilders.net/" target="_blank">ScriptBuilders</a>.

Solo nella prima pagina, che mostra gli script più recenti, ne ho trovati due che mi servivano e uno interessante per curiosità.

Me ne dimentico e invece AppleScript è sempre un serbatoio di soluzioni da non sottovalutare.