---
title: "In questo mondo di ladri<p>"
date: 2004-10-05
draft: false
tags: ["ping"]
---

Quando la classe non è acqua, perché fa acqua<p>

Steve Ballmer, Ceo di Microsoft, ha dichiarato in un&rsquo;occasione pubblica che gli utilizzatori di iPod sono ladri di musica.<p>

Forse il monopolista colpevole di concorrenza sleale e abuso di posizione dominante credeva che la musica fosse sua.<p>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>