---
title: "Monitoraggio attività"
date: 2004-06-02
draft: false
tags: ["ping"]
---

Con Panther è passato dall’inutilità totale all’utilità assoluta

Già detto tutto qui sopra. Cartella Utility, Monitoraggio Attività (o Activity Monitor). Mostra il carico di lavoro del processore, l’uso della Ram, quello del disco, i processi in corso, l’efficienza del sistema. Se prima di Panther era sostanzialmente una ciofeca, adesso è davvero utile e permette di decidere al volo se conviene aggiungere più Ram, se c’è un programma che sta congestionando il sistema e un sacco di altre informazioni.

Complimenti Apple.

<link>Lucio Bragagnolo</link>lux@mac.com