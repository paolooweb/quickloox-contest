---
title: "Addizioni a poco prezzo<p>"
date: 2005-07-10
draft: false
tags: ["ping"]
---

Quando gratis significa scarsa cura. Come minimo.<p>

In un <a href="http://www.forbes.com/business/free_forbes/2005/0725/058.html">articolo</a> sulla sezione gratuita del sito della rivista finanziaria Forbes si fanno i conti in tasca ai venditori di musica online, come l&rsquo;iTunes Music Store, e si afferma che <cite>i distributori ricavano circa 66 centesimi di dollaro da ogni canzone da 99 centesimi scaricata da iTunes, per un margine lordo di 43 centesimi. Guadagnano dieci dollari per Cd, con un margine lordo di 5,40 dollari.</cite><p>

Se adesso gentilmente qualcuno volesse spiegarmi come fare tornare i conti gliene ne sarei grato.<p>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>