---
title: "Alla faccia della tecnologia"
date: 2007-05-04
draft: false
tags: ["ping"]
---

Ho avuto a che fare con una persona che la email non la legge e soprattutto non ne scrive. Per lavoro ci dobbiamo sentire con una certa frequenza. Gli ho messo a disposizione tutti i miei canali di chat (Yahoo, Aim, Google Talk, iChat, ovviamente Irc), compreso Skype, e grazie alla iSight anche la videoconferenza quando servisse. Ma non va bene; non si conclude niente se non si fa per telefono.

Da notare che il nostro rapporto di lavoro riguarda l'interscambio di quantità notevoli di dati digitali. Ma, se non ha il telefono in mano, non si quaglia.

Il fatto che sia un idiota conclamato gioca a favore dell'uso della tecnologia oppure è semplicemente coincidentale?