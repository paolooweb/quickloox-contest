---
title: "Datemi un criterio"
date: 2010-02-23
draft: false
tags: ["ping"]
---

Apple ha battuto tutti i propri record di <a href="http://www.apple.com/it/pr/library/2010/01/25results.html" target="_blank">vendita di Mac</a>. In un ambito di crisi economica, senza cedere alla tentazione del netbook a basso costo, a me pare un buon risultato.

Visto che l'andamento dei Mac è in crescita, viene da pensare che la gente sia tutto sommato soddisfatta. L'idea che ogni trimestre si trovino gonzi sempre nuovi disposti a farsi fregare da una cattiva offerta, e in numero sempre crescente, è poco credibile.

Leggo che secondo Rescuecom, fornitore indipendente americano di assistenza informatica, i Mac sono <a href="https://www.rescuecom.com/2010-annual-computer-reliability-report.html" target="_blank">al primo posto per affidabilità</a>.

Inoltre vedo che, <a href="http://bwnt.businessweek.com/interactive_reports/customer_service_2010/" target="_blank">dice BusinessWeek</a>, Apple è al terzo posto assoluto per servizio al cliente, dietro a un'azienda di vendita al dettaglio e a un gruppo bancario assicurativo (che per giunta ha migliorato la propria posizione grazie anche&#8230; <a href="http://www.businessweek.com/magazine/content/10_09/b4168040782858.htm" target="_blank">a iPhone</a>). Il primo concorrente, Dell, è venti posti più indietro.

Contemporaneamente sento gente lamentarsi che la qualità Apple non è più quella di un tempo. Il buon Paolo Attivissimo, nel non-recensire iPad, <a href="http://attivissimo.blogspot.com/2010/01/aiped-aipad-aipod.html#c4430648526246144924" target="_blank">scrisse</a> la storica frase <cite>Continuo a non capire perché il successo commerciale di un prodotto sia citato come metro assoluto di soddisfazione del cliente</cite>.

Ci sta tutta, che non sia il metro assoluto: ma come ci facciamo un'idea di questa soddisfazione? Paolo su questo punto si dilegua. Quanti lamentano che la qualità non sia più quella di un tempo non portano uno straccio di argomento che vada oltre <i>mi si è guastato l'iMac</i> (e se anche fossero cinque non vorrebbe dire niente; mai provato a lanciare una moneta per aria più volte e segnare le teste e le croci?).

La classifica di Rescuecom (che mostra Apple al comando da nove mesi filati) è fatta con criteri assurdi che non stanno in piedi e sono il primo a dirlo. Le inchieste di BusinessWeek sembrano i voti del pattinaggio artistico, dove alla fine il merito effettivo degli atleti deve sempre sottostare al capriccio di qualche giudice.

Sono cose che non prenderei per oro colato. Però qualcuno mi mostra un criterio, indagini o statistiche serie, che evidenzino un calo della qualità negli anni, o pesino l'insoddisfazione dei clienti? Altrimenti è solo aria.