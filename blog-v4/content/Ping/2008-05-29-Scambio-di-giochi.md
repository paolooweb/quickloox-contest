---
title: "Scambio di giochi"
date: 2008-05-29
draft: false
tags: ["ping"]
---

Mi ha scritto Tommaso Fantini: <cite>solo oggi mi è venuto in mente di farti conoscere <a href="http://www.eyezmaze.com/" target="_blank">Eyezmaze</a>. Il sito è pieno zeppo di giochini davvero stimolanti e niente affatto scontati: ti consiglio di provarli tutti.</cite>

<cite>Tutti i link sulla colonna di sinistra sono giochi o varianti di questi ultimi: controlla tutte le sezioni (GAMEs, miniGAMEs, JOBs, perfino Old EYEZMAZE merita un'occhiata)&#8230;
</cite>
<cite>&#8230;e lascia DWARF COMPLETE per ultimo (non cancellare i cookie memorizzati dal browser, se vuoi riprendere la partita!).</cite>

Gli ho risposto: <cite>conosco quello del diavoletto che salta sul mondino, ma ci ero arrivato da un'altra parte e non avevo fatto il collegamento con il sito. C'è molta roba davvero!</cite>

<cite>Ringrazio e ricambio con <a href="http://youplay.it/play/main.asp" target="_blank">YouPlay</a>, molto diverso per il tipo di giochi, ma una sorpresa.</cite>

Abbiamo fatto scambio di giochi. Internet è proprio bella.