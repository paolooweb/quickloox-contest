---
title: "Gamma di gemme"
date: 2008-06-16
draft: false
tags: ["ping"]
---

Da qualche tempo, e fino a metà agosto, Macworld Usa dà notizia di programmi gratuiti o a basso costo, ma di alta qualità, per Mac OS X, sulla <a href="http://www.macworld.com/weblogs/macgems.html" target="_blank">pagina MacGems</a>. Da seguire assolutamente. Oggi ho scoperto <a href="http://lingon.sourceforge.net/" target="_blank">Lingon</a>.