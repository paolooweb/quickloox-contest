---
title: "Cheap and choc"
date: 2006-03-17
draft: false
tags: ["ping"]
---

Gente che fa fatica ad aprire le Preferenze di Sistema per impostare il salvaschermo si sta eccitando all&rsquo;idea di poter passare un pomeriggio a compiere acrobazie informatiche per riuscire a fare girare in modo approssimativo su Mac il peggiore sistema operativo esistente.

Spararsi in un ginocchio, ma in modo complicato a sufficienza da potersi guardare allo specchio e credersi pi&ugrave; furbi degli altri.

Prossimo passo: un sistema pesantemente elaborato per mandare in fumo il contenuto del disco rigido. Basta che la procedura sia pi&ugrave; lunga di tre pagine e abbastanza confusa da sembrare astuta. Ci sar&agrave; la coda.