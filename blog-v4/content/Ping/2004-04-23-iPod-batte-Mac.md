---
title: "iPod batte Mac, ma non è vero"
date: 2004-04-23
draft: false
tags: ["ping"]
---

Si vendono più player musicali che computer, ma dovrebbe essere ovvio

Si è già cominciato a dare risalto ai risultati finanziari di Apple. Ma, più che sottolineare un ennesimo trimestre in attivo, 4,6 miliardi di dollari in cassa e nessun debito (vogliamo parlare delle aziende italiane?), si fa attenzione al fatto che sono stati venduti “solo” 749 mila Macintosh e ben 807 mila iPod.

Apple sta cambiando attività? No. I Macintosh si vendono da vent’anni e, per giunta, anche la vendita di Mac è cresciuta, del 5 percento in termini di unità. Le unità vendute di iPod sono aumentate del 909 percento, certo; ma iPod appartiene a un mercato in esplosione, ben diverso da quello quasi maturo dei computer.

E non dimenticare che, se il Mac fosse costato quanto un iPod, se ne sarebbero venduti molti, molti di più.

<link>Lucio Bragagnolo</link>lux@mac.com