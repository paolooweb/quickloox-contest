---
title: "Un grazie collettivo<p>"
date: 2005-09-04
draft: false
tags: ["ping"]
---

E se qualcuno pensa che sia pubblicità, grazie lo stesso<p>

Sto cominciando a ricevere posta da chi ha comprato il mio libro Macintosh Story e, oltre a comprarlo, lo ha letto.<p>

Fino a dove riesco rispondo a tutti personalmente e continuo a farlo. Però qualcuno non mi scriverà per mancanza di meglio, o per mancanza di voglia, o perché pensa che non gli risponderei, o per qualsiasi altro motivo.<p>

Beh, un grosso grazie anche a tutti loro. Non per avere comprato il libro, ma per averlo letto e per avere pensato di scrivermi, anche per un singolo istante, anche per dirsi in disaccordo con me. Non c&rsquo;è niente di più bello che confrontarsi.<p>

Qualcuno penserà che sia pubblicità. Notizia uno: non ce n&rsquo;è bisogno. Notizia due: non importa. Grazie comunque.<p>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>