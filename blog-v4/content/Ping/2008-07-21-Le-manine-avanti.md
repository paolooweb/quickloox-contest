---
title: "Notizie che non lo sono"
date: 2008-07-21
draft: false
tags: ["ping"]
---

Stasera alle 23 ci sono <a href="http://www.apple.com/quicktime/qtv/earningsq308/" target="_blank">i risultati finanziari di Apple</a>.

Questo blogghino ti avvisa in anticipo. I siti grossi e grassi, per i quali fa notizia tutto, compresa l'assenza di notizie, scriveranno in termini deludenti della <em>performance</em> finanziaria del nuovo computer di Apple (non il Mac, appunto, che invece avrà venduto alla grande).

Il fenomeno è dovuto al fatto che per fare soldi questi siti devono farsi cliccare e più la si spara grossa più la plebe clicca, per metà; per l'altra metà, semplicemente, non si documentano, abituati a vivere di copia e incolla.

La spiegazione è unicamente contabile e si deve al modo peculiare con cui Apple mette a registro le vendite del prodotto. Se serve davvero lo scrivo in italiano ma suppongo che interessi a pochi, quindi per ora mi limito a <a href="http://macjournals.com/news/iPhonerevenues.html" target="_blank">linkare Matt Deatherage</a>.