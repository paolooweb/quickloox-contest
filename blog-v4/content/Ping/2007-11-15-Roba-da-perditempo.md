---
title: "Roba da perditempo"
date: 2007-11-15
draft: false
tags: ["ping"]
---

Girando per tutt’altri motivi sul sito Apple ho trovato le pagine dei calendari iCal. Praticamente ci sono quelli di tutte le festività nazionali e di tutte le squadre professionistiche americane.

Ho provato a scaricare quello della della mia <a href="http://wsidecar.apple.com/cgi-bin/nph-reg3rdpty2.pl/product=15820&amp;cat=98&amp;platform=osx&amp;method=sa/Suns.ics" target="_blank">squadra Nba preferita</a> e, sorpresa, Safari si è rifiutato. Lo stesso link, dato in pasto a Netscape Navigator, ha funzionato a perfezione.

Guardare le partite è roba da perditempo, ma farei due chiacchiere anche con i <em>webmaster</em> del sito Apple.