---
title: "Forti con i deboli"
date: 2004-02-11
draft: false
tags: ["ping"]
---

Pensare in piccolo fa diventare grandi

Ha una bellissima foto con uno sgorbietto sopra e vuole Photoshop, quando basta GraphicConverter (se non iPhoto).

Gli capita di scrivere il curriculum e non sa che pesci pigliare senza Word. Basta TextEdit (che salva in Rtf e anche in .doc).

Deve tenere i conti di casa, o fare il preventivo delle vacanze, e si preoccupa di reperire Excel. AppleWorks fa queste cose con una mano sola.

Ha deciso di fare la sua pagina Web (non il suo sito; la sua pagina) e cerca GoLive, o DreamWeaver. Una qualsiasi vecchia copia di BBEdit Lite è sufficiente, o il Composer di Netscape.

Il mondo si sta riempiendo di persone che devono fare cose estremamente semplici e, fuorviati dal marketing, chiedono strumenti enormi, costosi, inefficienti, inutili per il compito che li attende.

Perché poi è più difficile imparare DreamWeaver che imparare l’Html. Ma vallo a spiegare a chi, per sentirsi all’altezza, deve riempirsi lo schermo di barre strumenti di cui ignora il 90 percento del significato.

Diceva un mio professore: imparate a usare gli strumenti deboli, prima. A quelli forti si arriva a suo tempo.

<link>Lucio Bragagnolo</link>lux@mac.com