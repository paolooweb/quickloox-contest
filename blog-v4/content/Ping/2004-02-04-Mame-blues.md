---
title: "Mame Blues"
date: 2004-02-04
draft: false
tags: ["ping"]
---

L’emulatore per eccellenza e una esperienza sorprendente

Non dico per chi e perché, ma ho scritto recentemente due paginette su Mame, l’emulatore di videogiochi da bar che non può mancare nel bagaglio ludico di alcuno (parlo specialmente ai nati dopo il 1985, che non sanno).

Beh, dovevo coprire sia Mac che Windows e mi sono preparato. Sicuramente, mi sono detto, la versione per Mac, che è più recente, avrà qualche problema, mentre quella per Windows sarà supercollaudata.

Come volevasi dimostrare. Mame per Mac è partito regolarmente appena installato. Su Windows, su un sistema praticamente vergine e privo di pasticci, non c’è stato verso. Non ha voluto funzionare né con le buone né con le cattive.

Quando mi dicono che “su Pc ci sono molti più giochi”, contano anche quelle quattromilasettecento Rom, che su Mac si avviano con uno schiocco di dita e su Windows ci perdi la giornata, o no?

<link>Lucio Bragagnolo</link>lux@mac.com