---
title: "Browse different"
date: 2003-01-21
draft: false
tags: ["ping"]
---

Microsoft-Apple, Explorer-Safari: che differenza di stile

Un giorno a Microsoft è venuta voglia di mettere un pulsante Indietro nelle finestre di Windows. Hanno incastrato Explorer dentro Windows e si sono beccati una condanna al processo antitrust.

Un giorno a NeXT, pardon, ad Apple è venuta voglia di mettere un pulsante Indietro nelle finestre di Mac OS X. Ce lo hanno messo. In più hanno creato un browser, Safari.

La differenza è che in Windows, per una ragione o per l’altra, non si può fare a meno di Explorer. Mentre in Mac OS X c’è piena libertà di non usare Safari, se uno preferisce.

Ma Explorer permette di interagire con il sistema, dice uno da fondo sala.

Anche Safari. Via <link>AppleScript</link>http://www.apple.com/macosx/safari/applescript, che è cosa ben più elegante. E arricchisce invece di costringere.

<link>Lucio Bragagnolo</link>lux@mac.com