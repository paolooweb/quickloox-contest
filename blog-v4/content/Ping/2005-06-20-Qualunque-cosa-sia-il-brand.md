---
title: "Qualunque cosa sia il brand<p>"
date: 2005-06-20
draft: false
tags: ["ping"]
---

Google e Coca-cola non sono nessuno, di fronte alla Mela<p>

Secondo il sito <a href="http://www.brandrepublic.com/bulletins/br/article/479939/apple-tops-list-fastest-growing-brands-digital-overtakes-traditional/">BrandRepublic</a>, Apple è la società che negli ultimi quattro anni ha fatto crescere meglio il proprio brand: 38 percento, contro il 36 percento di Google e Blackberry, Amazon al 35 percento e via così.<p>

La classifica a squadre la vince certamente Steve Jobs, dal momento che Pixar occupa il nono posto, con una crescita del 23 percento.<p>

Il brand è una cosa da pubblicitari e uomini marketing più grande del marchio, che comprende l&rsquo;immagine presso il pubblico, le sensazioni suscitate da chi ci pensa e un sacco di altro fumo. Mettiamola così: Apple è riuscita a farsi conoscere alla grande, e meglio che in passato.<p>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>