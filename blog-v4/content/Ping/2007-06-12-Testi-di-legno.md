---
title: "Testi di legno"
date: 2007-06-12
draft: false
tags: ["ping"]
---

Non ricordo chi si sia lamentato per la scarsa estetica dei grafici con <em>texture</em> legnosa presentati da Steveo Jobs alla Wwdc. Ma non sono una novità; Jobs li ha usati anche durante il <em>keynote</em> di <a href="http://www.apple.com/quicktime/qtv/wwdc07/" target="_blank">Macworld Expo 2007</a>, lo scorso gennaio.