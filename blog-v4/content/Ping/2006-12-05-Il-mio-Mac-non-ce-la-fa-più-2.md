---
title: "Il mio Mac non ce la fa più/2"
date: 2006-12-05
draft: false
tags: ["ping"]
---

Quando Safari ha talmente tante finestre aperte che, se le guardo con l'F10 di Exposé, non si riesce a leggere neanche una parola.