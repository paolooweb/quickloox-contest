---
title: "So' ragazzi"
date: 2007-05-04
draft: false
tags: ["ping"]
---

<strong>Daniele</strong> in questo periodo è poco impegnato a scuola e si è messo a maneggiare un po' Mac OS X, in special modo gli strumenti collegati a Xcode e che si trovano, ove sia installato Xcode, dentro /Developer/Applications/Graphic Tools.

Dall'oggi al domani si è messo a preparare un salvaschermo fatto con Quartz Composer. Mi sta facendo vedere le prime bozze e non è tanto il salvaschermo in sé, che pure è bellino, quanto l'interfaccia dell'editor di Quartz Composer. Non ci ho capito un'acca ma ho sub&#236;to lo shock culturale. Devo capirne di più.