---
title: "Evviva i cartoni, abbasso la Siae"
date: 2004-04-29
draft: false
tags: ["ping"]
---

Un bellissimo annuncio e una spiacevole attesa

Fino al 30 settembre l’iTunes Music Store ha in esclusica l’intero catalogo di colonne sonore e altra musica prodotta dalla Disney.

Peccato che sia ancora impossibile avere la versione italiana dello store e non raccontarmi che è colpa di Apple.

Se qualcuno vede passare per strada un dirigente Siae, può fargli lo sgambetto a mio nome? Grazie.

<link>Lucio Bragagnolo</link>lux@mac.com