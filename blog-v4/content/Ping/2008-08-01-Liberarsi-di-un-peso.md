---
title: "Liberarsi di un peso"
date: 2008-08-01
draft: false
tags: ["ping"]
---

Sentita durante i rituali saluti per le ferie.

<cite>Quest'anno, che ho iPhone, posso partire e tenere aggiornato il blog senza bisogno del portatile e sono felicissima, tutto peso in meno. Non mi sono mai piaciuti i cellulari, ma questo è tutto tranne che un cellulare.</cite>

Giuro che è stata una dichiarazione spontanea e che è riportata in forma pressoché testuale.