---
title: "Il gigante e la bombina"
date: 2009-09-27
draft: false
tags: ["ping"]
---

Trovo ingiusto che le prime pagine dei giornali e dei siti non siano dedicate a <a href="http://code.google.com/chrome/chromeframe/" target="_blank">Google Chrome Frame</a>.

Per chi non lo sapesse, è un <i>plugin</i> di Chrome, il <i>browser</i> di Google, che si installa in Internet Explorer e lo trasforma in Chrome. Meglio, fa passare la navigazione e l'esecuzione di JavaScript attraverso il motore di Chrome, anche se si usa Explorer.

Non è un <i>divertissement</i>; la mossa influisce sui dati di traffico e se appena appena il pubblico si accorge che le prestazioni del <i>plugin</i> (mica del <i>browser</i> concorrente, del <i>plugin</i>) sono quasi dieci volte quelle del <i>browser</i>, e la cosa prende piede sui siti, Internet Explorer rischia di diventare un'arma strategica nelle mani di&#8230; Google.

Se uno pensa che Microsoft è arrivata a fondere Explorer nel sistema operativo e ha sopportato multe per un miliardo e passa di euro pur di continuare a farlo, si capisce che la mossa di Google è ultraintelligente e Microsoft invece è rimasta ferma per ben più di un giro.

L'azienda più ricca e potente del mondo per ora ha saputo solamente sconsigliare l'adozione del <i>plugin</i>, per ragioni di&#8230; sicurezza. Google ha avuto buon gioco nel mostrare che Chrome Frame, semmai, aumenta la sicurezza di Explorer. Qualcuno ha ironizzato: se i <i>plugin</i> sono pericolosi per la sicurezza, Microsoft dovrebbe suggerire ai propri utenti di disinstallare Silverlight!

È una vicenda da seguire perché diventerà vieppiù interessante. Nel frattempo noto che Chrome Frame arriva da Google, che ha usato WebKit di Apple, la quale lo ha creato a partire da Khtml, <i>open source</i>.

La miccia accesa da programmatori volontari ed entusiasti è attaccata a un ordigno nel giardino del gigante Golia del software, quello che ha chiamato il software libero <i>cancro della proprietà intellettuale</i>.

Vediamo che succede. Mi vengono in mente quei palazzi sottoposti a demolizione controllata.