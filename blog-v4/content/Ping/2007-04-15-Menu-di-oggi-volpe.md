---
title: "Menu di oggi: volpe"
date: 2007-04-15
draft: false
tags: ["ping"]
---

Il comando da tastiera per fare Back e tornare indietro di una pagina è lo stesso su Safari e su Firefox (Comando-Opzione-[). Per qualche bizzarro motivo, però, sul mio <a href="http://www.mozilla.com/firefox" target="_blank">Firefox</a> funziona solo dopo che ho aperto una volta il menu History, quello che contiene il comando Back. Ma è sufficiente aprire il menu e chiuderlo, senza neanche selezionare il comando.

Bachi (o feature?) talmente sottili che quando capisci vorresti essere un maestro Zen e poter improvvisare dei versi per celebrare l'evento.

<em>Vado indietro</em>
<em>aprendo il giusto menu;</em>
<em>vado avanti.</em>

