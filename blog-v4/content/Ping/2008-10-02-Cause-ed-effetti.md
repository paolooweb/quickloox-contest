---
title: "Cause ed effetti"
date: 2008-10-02
draft: false
tags: ["ping"]
---

Non tragga in inganno la collocazione provinciale: <a href="http://www.cesanofotoephoto.it/ita/mostre/giulia_diegoli.htm" target="_blank">Foto&#38;Photo</a> è una manifestazione piuttosto importante per il genere.

Ho conosciuto Giulia quando era una studentella di belle speranze, appena entrata all'Accademia di Brera, senza idee particolari sul suo futuro e con un Pc.

Oggi è una donna, sta affermandosi come fotografa, con risultati degni di nota, e usa un Mac.

Il Mac non è la causa; Giulia è brava per suo merito. Ma un effetto, s&#236;.