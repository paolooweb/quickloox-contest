---
title: "Crick e crack"
date: 2009-08-05
draft: false
tags: ["ping"]
---

Non sono ancora riuscito ad aggiornare il mio iPhone 2G di prima generazione a iPhone OS 3.0.1.

Ennesimo tentativo ieri. Il Dev Team dice che è semplicissimo, si fa l'aggiornamento con il 3.0.1 e poi si <a href="http://blog.iphone-dev.org/post/153409604/recycling-goodness" target="_blank">esegue il <i>jailbreak</i> con redsn0w</a> a partire dalla copia non aggiornata 3.0. redsn0w, dicono, modifica parti del sistema che l'aggiornamento 3.0.1 non tocca e quindi in sostanza si finisce per avere un sistema 3.0 sovrapposto a un 3.0.1. I file 3.0.1 non sono toccati dalla sovrascrittura a opera di 3.0 e cos&#236; tutto funziona.

Il problema è che sbaglio regolarmente qualcosa e anche stavolta redsn0w ha <i>brickato</i> iPhone, trasformandolo in mattone.

Il piano B era l&#236; pronto: reinstallazione di 3.0 dopo <a href="http://blog.iphone-dev.org/post/126465561/trois-drei-h-rom" target="_blank">trattamento con PwnageTool</a>.

Tutto si è risistemato e, meglio di tutto, pare che adesso il blocco di Edge funzioni: prima avevo installato sia BossPrefs sia Sbsettings, stavolta solo l'ultimo. Apparentemente l'apparecchio non riesce ad accedere da solo a Edge come faceva prima ed è sotto stretto controllo oramai da molte ore. Speriamo bene. Odio i sistemi che scricchiolano, meglio quelli che proprio non funzionano. Almeno si sa che cosa fare.

Nota per <b>giovitave</b>: la scarsa durata della batteria dopo il jailbreak è <a href="http://blog.iphone-dev.org/post/144907200/ultratips" target="_blank">forse risolvibile</a>.