---
title: "Un amico ritorna"
date: 2006-02-07
draft: false
tags: ["ping"]
---

<a href="http://www.bluebottazzi.com" target="_blank">Blue Bottazzi</a> ha finalmente rimesso mano alle sue pagine personali.

Sono personali ma ci riguardano tutti, perch&eacute; parlano di musica, di vita, di Mac. E, come si dice spesso, quella volta lui c&rsquo;era.
