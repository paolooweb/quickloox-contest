---
title: "Chi troppo vuole va in timeout"
date: 2009-06-01
draft: false
tags: ["ping"]
---

Ho provato a montare contemporaneamente 34 immagini disco tra Cd e Dvd.

Se ne sono aperte trentadue, mentre per due immagini Mac OS X ha dato avviso di <em>timeout</em>.

Le due immagini mancanti si sono normalmente aperte con un comando successivo.

Sarebbe interessante capire se il dato di trentadue immagini montate contemporaneamente, e non più di trentadue, è un limite intrinseco di Mac OS X oppure semplicemente un risultato occasionale della combinazione di risorse a disposizione e dati da elaborare.

Cosa forse più grave: la barra laterale di Leopard non riesce assolutamente a visualizzare tutte le immagini disco.