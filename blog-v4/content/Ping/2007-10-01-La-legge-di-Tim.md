---
title: "La legge di Tim"
date: 2007-10-01
draft: false
tags: ["ping"]
---

Oggi a pranzo ho sentito un amico. Mi diceva che conosce uno in Tim e che prova a sondarlo per sapere se girano voci su iPhone.

La sera sento un altro amico. Conosce una in Tim, che magari in ufficio ha sentito qualcosa riguardo iPhone.

Domani sera sono a cena con un amico, che lavora in Telecom. So già che finiremo per parlare di iPhone e farci due risate. È uno dei due amici che conosco, che lavora in Telecom.

Morale.

Non so quanta gente lavori in Telecom/Tim. Però sono certamente migliaia o decine di migliaia di persone.

Siccome con sei gradi di separazione si arriva da qualunque parte nel mondo, è evidente che chiunque di noi arriva a una persona che lavora in Telecom/Tim nel giro di un contatto o due.

Se bastasse avere un contatto in Telecom/Tim per sapere qualcosa di più su iPhone, l'Italia intera saprebbe già tutto.

Il problema non è sapere qualcosa di più su iPhone. È sapere qualcosa <em>di vero</em> su iPhone.

Credo che la centralinista media di Telecom/Tim non sarà di molto aiuto a riguardo.