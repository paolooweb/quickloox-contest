---
title: "Altre due idee per l'Avvento"
date: 2010-12-02
draft: false
tags: ["ping"]
---

Per chi ha pargoli cos&#236; sciocchi da dubitare dell'esistenza di Babbo Natale: anche quest'anno, proseguendo una tradizione iniziata nel 1955 a causa di <a href="http://news.cnet.com/8301-13772_3-20024299-52.html?part=rss&amp;subj=news&amp;tag=2547-1_3-0-20" target="_blank">un annuncio pubblicitario contenente un numero di telefono sbagliato</a>, il comando strategico di difesa aereospaziale nordamericano (Norad) ha allestito <a href="http://www.noradsanta.org/" target="_blank">il sito che traccia i percorsi seguiti dalla slitta volante</a>, con giochi e iniziative di ogni tipo, anche in italiano. Sarà possibile seguire il volo di Babbo Natale da Mac e anche via cellulare, purché abbia installato Google Maps (ho provato con iPhone e funziona), visitare il paese di partenza del Babbo suddetto eccetera eccetera.

Per quando i pargoli dormono felici di regali, dolcetti e festoni e ci si può godere il silenzio della festa, magari ovattato dalla neve: seguire il <a href="http://perladvent.pm.org/2010/" target="_blank">Perl Advent Calendar 2010</a>. Per ogni giorno, un (impegnativo) consiglio sul linguaggio di programmazione e <i>scripting</i> <a href="http://www.perl.org/" target="_blank">Perl</a> (anche con un <a href="http://www.perl.it//" target="_blank">punto di riferimento in italiano</a>).

S&#236;, difficile. Ma si avvicinano i propositi per il nuovo anno e forse qualcuno sta cercando una marcia in più per il lavoro, o anche &#8220;solo&#8221; per le meningi&#8230;