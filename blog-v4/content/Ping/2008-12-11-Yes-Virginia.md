---
title: "Yes, Virginia"
date: 2008-12-11
draft: false
tags: ["ping"]
---

<cite>Caro direttore, ho otto anni. Alcuni dei miei amici dicono che Babbo Natale non esiste. Mio papà mi ha detto: &#8216;Se lo vedi scritto sul Sun, sarà vero'. La prego di dirmi la verità: esiste Babbo Natale?</cite>

<cite>Virginia O'Hanlon</cite>

La <a href="http://en.wikipedia.org/wiki/Yes,_Virginia,_There_is_a_Santa_Claus" target="_blank">risposta del direttore Francis Pharcellus Church</a>, sul <em>Sun</em> del 20 settembre 1897:

<cite>Virginia, i tuoi amici si sbagliano. Sono stati contagiati dallo scetticismo tipico di questa era piena di scettici. Non credono a nulla se non a quello che vedono. Credono che niente possa esistere se non è comprensibile alle loro piccole menti. Tutte le menti, Virginia, sia degli uomini che dei bambini, sono piccole. In questo nostro grande universo, l'uomo ha l'intelletto di un semplice insetto, di una formica, se lo paragoniamo al mondo senza confini che ci circonda e se lo misuriamo dall'intelligenza che dimostra nel cercare di afferrare la verità e la conoscenza.</cite>

<cite>S&#236;, Virginia, Babbo Natale esiste. Esiste come esistono l'amore, la generosità e la devozione, e tu sai che abbondano per dare alla tua vita bellezza e gioia. Cielo, come sarebbe triste il mondo se Babbo Natale non esistesse! Sarebbe triste anche se non esistessero delle Virginie. Non ci sarebbe nessuna fede infantile, né poesia, né romanticismo a rendere sopportabile la nostra esistenza. Non avremmo altra gioia se non quella dei sensi e della vista. La luce eterna con cui l'infanzia riempie il mondo si spegnerebbe.</cite>

<cite>Non credere in Babbo Natale? È come non credere alle fate! Puoi anche chiedere a tuo padre che mandi delle persone a tenere d'occhio tutti i comignoli del mondo per vederlo, ma se anche nessuno lo vedesse venire giù, che cosa avrebbero provato? Nessuno vede Babbo Natale, ma non significa che non esista. Le cose più vere del mondo sono proprio quelle che né i bimbi né i grandi riescono a vedere. Hai mai visto le fate ballare sul prato? Naturalmente no, ma questa non è la prova che non siano veramente l&#236;. Nessuno può concepire o immaginare tutte le meraviglie del mondo che non si possono vedere.</cite>

<cite>Puoi rompere a metà il sonaglio del bebè e vedere da dove viene il suo rumore, ma esiste un velo che ricopre il mondo invisibile che nemmeno l'uomo più forte, nemmeno la forza di tutti gli uomini più forti del mondo, potrebbe strappare. Solo la fede, la poesia, l'amore possono spostare quella tenda e mostrare la bellezza e la meraviglia che nasconde. Ma è tutto vero? Ah, Virginia, in tutto il mondo non esiste nient'altro di più vero e durevole. Nessun Babbo Natale? Grazie a Dio lui è vivo e vivrà per sempre. Anche tra mille anni, Virginia, diecimila volte diecimila anni da ora, continuerà a fare felici i cuori dei bambini.</cite>

È l'editoriale più riprodotto nella storia del giornalismo, adesso riprodotto una volta di più.

Tanti auguri di Buon Natale con infinta riconoscenza, affetto e amicizia per quanti frequentano questa paginetta.