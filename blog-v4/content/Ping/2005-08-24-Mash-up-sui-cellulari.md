---
title: "Mash-up sui cellulari"
date: 2005-08-24
draft: false
tags: ["ping"]
---

Notizie interessanti da un blog interessante

Come puoi leggere su <strong>Mash-ups in italy</strong>, il blog di <em>Lele Dainesi</em>, la telefonia via Internet sta arrivando anche sui cellulari.<p>

Ci metterà del tempo e per ora non si parla di Mac, ma la cosa è relativa. Primo, perché prima o poi le cose arrivano anche su Mac e si tratta solo di tenere le antenne belle ritte per captare i segnali; secondo, perché il blog di Lele merita di essere letto con regolarità.<p>

Il taglio è per persone molto orientate al marketing e i non addetti ogni tanto si sentiranno un po’ tagliati fuori, come in certe feste quando tutti gli altri arrivano da un ambiente preciso e parlano delle loro cose con il loro gergo.<p>

Ma un’occhiata ogni tanto fa molto bene, più o meno come tuffare la testa in un fontanile d’acqua fresca al termine di una lunga e sudata passeggiata in montagna.<p>

In <a href="http://www.macatwork.net/daino">Mash-ups in italy</a> Lele ha parlato anche di me, ma non so che farci. I maligni penseranno male comunque, dato che notoriamente il peccato sta negli occhi di chi guarda. Gli altri godranno di una risorsa in più. :-)<p>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>