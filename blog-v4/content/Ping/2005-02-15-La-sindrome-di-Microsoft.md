---
title: "La sindrome di Billusconi<p>"
date: 2005-02-15
draft: false
tags: ["ping"]
---

Trovo l&rsquo;indifferenza assai più produttiva dell&rsquo;accanimento<p>

Tempo fa Sandro Viola scrisse su Repubblica un eccellente articolo dedicato alla sindrome di Berlusconi.<p>

No, niente politica! Semplicemente, era un esempio eccellente dell&rsquo;attitudine che alcuni hanno a diventare ossessionati dall&rsquo;oggetto della loro ostilità/antipatia/odio/avversione/fastidio, al punto che ma non riescono a stare cinque minuti di seguito senza parlarne.<p>

Ecco: Microsoft. Un sacco di gente non la sopporta, eppure non fa che parlarne. E il baco nel sistema di mappe, e il crash in faccia a Gates, e l&rsquo;ultimo virus su Windows, e l&rsquo;ennesima causa contro qualche ragazzino eccetera eccetera. Sembra che non esista altro. Invece sì.<p>

La mia modesta impressione è che sia molto meglio l&rsquo;indifferenza e che ci siano cose ben più interessanti cui dedicare il proprio tempo. Prometto, di mio, che parlerò di Microsoft sempre meno e comunque il meno possibile.<p>

Lo so che alla fine stavolta ho parlato di Microsoft. Ma sarebbe stato molto peggio se mi fossi messo a fare politica.<p>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>