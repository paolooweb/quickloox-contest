---
title: "Ragazzi che curve"
date: 2006-02-17
draft: false
tags: ["ping"]
---

La raccolta dati sulle batterie dei portatili ha superato quota cinquanta. Comincia a esserci la massa critica per elaborare le informazioni in modo vagamente intelligente.

L&rsquo;idea &egrave; arrivare a una curva che descriva con precisione ragionevole il rapporto tra et&agrave;, cicli di carica e capacit&agrave; residua della batteria. Non ripubblico ora la tabella aggiornata per mantenere la leggibilit&agrave; del blog, ma lo far&ograve; luned&igrave;. Nel frattempo chi vuole pu&ograve; comunque inviare i suoi dati.

&Egrave; gi&agrave; detto nei post precedenti, ma mi raccomando ulteriormente di usare <a href="http://www.coconut-flavour.com/coconutbattery/" target="_blank">Coconut Battery</a> con Tiger e <a href="http://www.branox.com/Battorox/Battorox.html" target="_blank">Battorox</a> per Panther e altre versioni di Mac OS X.

Un grosso grazie a tutti quelli che hanno partecipato e a quanti lo faranno. :-)