---
title: "Gran carro e piccolo carro"
date: 2009-10-21
draft: false
tags: ["ping"]
---

È nota la tendenza generale italiana a salire sul carro del vincitore.

Linguisticamente esiste la tendenza a salire sul carro di quello che si suppone sarà il vincitore.

Per questo, negli anni ottanta, sembrava che senza sapere il russo o quanto meno il serbo-croato uno non avrebbe contato nulla. Poi il blocco sovietico si è sciolto e oggi i corsi di russo non vanno più come prima. Oggi chi non ha in tasca il cinese mandarino, o almeno un po' di arabo, porta impresso il marchio del perdente.

Forse è vero in generale, ma nel campo del software starei attento a dati come quelli <a href="http://wilshipley.com/blog/2009/10/pimp-my-code-part-17-lost-in.html" target="_blank">forniti da Wil Shipley</a>, attinenti a <a href="http://www.delicious-monster.com/" target="_blank">Delicious Monster</a> e <a href="http://www.omnigroup.com/" target="_blank">Omni Group</a>.

Salta fuori che si farebbe molto meglio a imparare tedesco, francese, giapponese. E che ben più del cinese conta l'italiano.

Campo di applicazione ristrettissimo, sono il primo a convenirne. Tuttavia c'è da riflettere sulla situazione che descrivono i media, e quella che uno si sente intorno, e quanto questa sia differente dalla realtà concreta. Il carro del vincitore a volte è proprio piccolino.