---
title: "Quel che serve in pausa caffè<p>"
date: 2005-06-26
draft: false
tags: ["ping"]
---

Guai a rimanere sguarniti sull&rsquo;argomento del giorno<p>

Ho appena assistito a un servizio del tiggì su quella che sarà la sedicente nuova moda dell&rsquo;estate: un giochino semienigmistico non del tutto fatuo di nome <em>Sudoku</em>.<p>

In previsione delle cene fra amici, le pizzate di fine anno, le rimpatriate, le feste in famiglia, le serate al villaggio, i dialoghi tra pendolari, la pause alla macchinetta del caffè e qualunque altra situazione sociale, sappi che esiste già almeno un <a href="http://www.madoverlord.com/projects/sudoku.t">programma per Mac</a> in grado di aiutarti in materia.<p> Una ricerca su VersionTracker porterà alla luce altri programmi e persino un nosotto per Dashboard.

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>