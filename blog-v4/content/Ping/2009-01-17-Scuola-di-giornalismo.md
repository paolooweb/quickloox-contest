---
title: "Scuola di giornalismo"
date: 2009-01-17
draft: false
tags: ["ping"]
---

Niente a che vedere con il Mac. Però&#8230; dai un'occhiata a <a href="http://www.nytimes.com/interactive/2009/01/15/nyregion/20090115-plane-crash-970.html" target="_blank">come il New York Times riassume</a> la vicenda dell'aereo atterrato nel fiume Hudson a New York dopo che uno stormo di volatili aveva messo fuori uso ambedue i motori.

Le storie si raccontano (anche) cos&#236;.