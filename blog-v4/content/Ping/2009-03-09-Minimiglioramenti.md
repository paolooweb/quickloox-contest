---
title: "Minimiglioramenti"
date: 2009-03-09
draft: false
tags: ["ping"]
---

Nel quadro della discussione sui costi del nuovo Mac mini, sono interessanti i risultati dei <a href="http://www.macworld.com/article/139267/2009/03/mac_mini_2009.html?t=109" target="_blank">test di Macworld.com</a>.

Come era prevedibile, il nuovo mini permette di giocare in maniera decente.

Come era meno prevedibile, il disco presente sul modello più costoso impatta sulle prestazioni e migliora percettibilmente il risultato.

Il punteggio Speedmark del Mac mini costoso è il sette percento superiore a quello del modello base. Nelle operazioni basate su disco rigido ci sono divari del 20-25 percento.

In quei duecento euro, tanti o pochi che siano, c'è anche un vantaggio di velocità. A ognuno naturalmente valutare se valga o meno il prezzo.