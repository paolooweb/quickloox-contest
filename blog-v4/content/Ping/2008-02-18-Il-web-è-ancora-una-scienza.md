---
title: "Il web è ancora una scienza"
date: 2008-02-18
draft: false
tags: ["ping"]
---

Ho avuto la luminosa idea di provare ad avviare un blog scritto con iWeb e .Mac, cos&#236;, per vedere l'effetto che fa.

Ho registrato il mio bravo dominio con Network Solutions. Ho letto le istruzioni su .Mac: perché il mio dominio punti alla home su .Mac, occorre creare un alias a web.mac.com come host www (se ho capito bene). In parole povere, uno scrive www.nomedeldominio.com e le impostazioni di Network Solutions inviano il browser a web.mac.com/nomedeldominio, dove lo aspetta la paginetta da me creata.

Il sito di Network Solutions però è veramente criptico e neanche frugare tra i forum di mezzo mondo ha sortito grandi risultati. Non ci ho capito un accidente.

Avremo Internet veramente universale quando un'operazione cos&#236; si potrà fare con un clic.