---
title: "La grande illusione"
date: 2008-07-22
draft: false
tags: ["ping"]
---

È ora di mettere le cose a posto prima dell'11 luglio.

iPhone non è un cellulare e neanche uno <em>smartphone</em>. E chi si mette a fare confronti di funzionalità vive una grande illusione.

Gli svedesi di InUse hanno <a href="http://www.inuseful.se/files/so_does_the_iphone_live_up_to_its_hype.pdf" target="_blank">testato l'usabilità di iPhone</a> (quello vecchio, non 3G, non Gps, non software 2.0, non <em>craccato</em> con quattrocento programmi in più, non App Store. Il link è a un file Pdf) contro quattro concorrenti, compreso il Nokia N95. Risultato: iPhone era quello che faceva meno cose di tutti&#8230; e quello che l'80 percento dei collaudatori si sarebbe portato a casa. In italiano: iPhone è più piacevole da usare e quello che fa lo fa meglio degli altri.

iPhone genera quasi <a href="http://marketshare.hitslink.com/report.aspx?sample=4&qprid=10&qpmr=14&qpdt=1&qpct=0&qpcal=1&qptimeframe=M&qpsp=113" target="_blank">il triplo del traffico web</a> che genera Windows Mobile, lo 0,16% del totale contro lo 0,06% del totale. Eppure il numero di iPhone è una minuscola percentuale del numero di apparecchi Windows Mobile! iPhone è il quarto sistema operativo a generare traffico web, dopo Windows, Mac OS X e Linux. Symbian, che in base installata surclassa iPhone, neanche è in classifica. In italiano: con iPhone Internet si usa veramente, sugli altri è poco più che un modo di dire; tutti aggeggi che potrebbero usare Internet e lo fanno cos&#236; male che la gente lo fa poco e probabilmente anche controvoglia. Non dimentichiamo che questi dati sono di iPhone Edge, non di iPhone 3G. (Dati di giugno 2008, ma è facile ricavarli per qualsiasi altro periodo).

Il 12,1% del tempo di uso di iPhone <a href="http://blogs.zdnet.com/ITFacts/?p=14333" target="_blank">è navigazione Internet</a>. Il 2,4% del tempo di uso di un telefono è navigazione Internet, cioè cinque volte meno. L'11,9% del tempo di uso di iPhone è ascolto di musica e audio. Il 2,5% del tempo di uso di un telefono è ascolto di musica e audio, cioè quattro volte meno. Il 10,4% del tempo di uso di iPhone è posta elettronica. Il 2,8% cento del tempo di uso di un telefono è posta elettronica, cioè quattro volte meno. In italiano: Safari su iPhone è cinque anni avanti a qualsiasi <em>browser</em> cellulare. La posta su iPhone è nettamente superiore a quella dei cellulari, dove scrivere una mail è piacevole come una punizione. Le funzioni di iPod di iPhone eclissano quelle dei telefoni.

Quando i divari sono cos&#236; impietosi, i confronti evidentemente non hanno senso.

iPhone infatti è un computer. Gli altri sono telefoni. E, più che <em>smart</em>, al massimo li chiamerei <em>large</em>. Mettiamoci pure a confrontare telefoni con computer. A quando il confronto dei <em>windsurf</em> con i gommoni? O dei maratoneti con i lottatori di <em>sumo</em>?