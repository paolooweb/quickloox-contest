---
title: "Le dodici scimmie"
date: 2008-12-31
draft: false
tags: ["ping"]
---

Anche in questi giorni il titolo Apple fa su e giù per motivi di cui non intendo parlare.

Vedo però abbondanza di gente che ne parla. Che usa la salute altrui per profitto personale.

Non valgo niente. Ma il mio clic che non vale niente, Gizmodo non lo vedrà mai più. E lo stesso vale per tutte le scimmie che hanno copiato Gizmodo e le scimmie delle scimmie che hanno copiato le copie.