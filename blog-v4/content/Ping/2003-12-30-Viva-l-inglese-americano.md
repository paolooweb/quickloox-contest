---
title: "Evviva l’inglese americano"
date: 2003-12-30
draft: false
tags: ["ping"]
---

Abbasso i localizzatori globalizzati

Da sempre uso Mac OS X in inglese. Prima di tutto i messaggi occupano meno spazio a video; secondo, a volte le installazioni danno problemi se la lingua è diversa dall’inglese (con il computer impostato in italiano non riuscivo neanche a lanciare l’installazione di Panther senza subire un kernel panic). Terzo, a volte la traduzione inserisce ambiguità lessicali che complicano la vita invece di semplificarla.

Quarto, non so chi siano i realizzatori dell’edizione italiana di Mac OS X, né dove stiano; ma si vede che la globalizzazione ha effetti perversi sulle localizzazioni.

Sono perverso anch’io, nel senso che quasi (quasi) preferisco vedere un kernel panic di un refuso. Oggi mi sono collegato a Internet via modem e, per ragioni di lavoro, il sistema doveva essere in italiano.

Appena è apparso il messaggio “Connettoo l’ISP” mi è venuta la voglia irrefrenabile di tornare subito, immediatamente all’inglese americano. Dove, se non altro, sanno rileggere quello che scrivono.

Ma sono incapaci i localizzatori in italiano di Mac OS X oppure vengono pagati troppo poco per lavorare seriamente? Nel primo caso mi candido. Sono pignolo abbastanza. Nel secondo, beh, io uso Panther in inglese.

<link>Lucio Bragagnolo</link>lux@mac.com