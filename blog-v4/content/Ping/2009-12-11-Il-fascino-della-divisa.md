---
title: "Il fascino della divisa"
date: 2009-12-11
draft: false
tags: ["ping"]
---

Dopo <a href="http://www.macworld.it/blogs/ping/?p=3154" target="_self">la polizia danese</a>, la Gendarmerie Nationale francese.

Non è una storia di adozione di Mac e la questione è più sottile, eppure sostanziale: Thunderbird, il <i>client open source</i> per la posta elettronica, dispone di una estensione di cifratura chiamata Trustedbird, nel cui codice &#8211; sempre <i>open source</i> &#8211; c'è appunto lo zampino dei gendarmi d'Oltralpe, assimilabili ai nostri Carabinieri.

Viene fuori, spiega <a href="http://www.reuters.com/article/idUSTRE5B91G020091210?type=technologyNews%3FfeedType%3DRSS&amp;feedName=technologyNews&amp;utm_source=feedburner&amp;utm_medium=feed&amp;utm_campaign=Feed%3A+reuters%2FtechnologyNews+%28News+%2F+US+%2F+Technology%29" target="_blank">l'articolo di Reuters</a>, che Thunderbird con relativa estensione è stato preferito a Outlook di Microsoft, proprietario e impossibile da complementare avendo la completa conoscenza di che cosa sta dentro il suo codice.

In Francia <a href="http://it.www.mozillamessaging.com/it/" target="_blank">Thunderbird</a> e talvolta Trustedbird girano su non meno di ottantamila computer statali. L'esperienza, dalla Gendarmerie, sta estendendosi ai ministeri di Cultura, Finanze e Interni.

Hanno mostrato <a href="http://www.trustedbird.org/tb/Main_Page" target="_blank">Trustedbird</a> ai dirigenti della Nato. Sai mai che si rendano conto tutti che la sicurezza più certa si ha quando si sa come funziona.