---
title: "Per rispondere ad Alex"
date: 2006-07-16
draft: false
tags: ["ping"]
---

Che era curioso di sapere quale fosse il Ping classificato <em>alx</em>. Segue qui sotto. Nella base dati odierna manca ed è una buona scusa per inserirlo. Data intorno a mercoled&#236; 30 aprile 2003. Quando Ping aveva ancora i sottotitoli!

<strong>Bufale universali</strong>

<em>Quando ti dicono che hanno detto, nessuno lo ha detto, anche se te lo dicono</em>

Adesso che è nato Applemusic.com (il che tra l'altro conferma come non esistano più contenziosi tra Apple e i Beatles) si può guardare con occhio più sereno alla clamorosa bufala secondo la quale Apple avrebbe speso sei miliardi di dollari per comprare Universal Music.

Chissà se è ancora vivo appleuniversal.com, il sito civetta che era stato creato esattamente per dare credito alla bufala.

Due persone normali sedute al bar a prendersi un caffè si direbbero: figurati se un affare di questa portata viene trattato in maniera da generare voci, si dice e rumor vari.

Invece metti due persone sedute davanti a un browser: iniziano a credere a qualsiasi cosa.

La verità continua a essere una sola: se è un rumor, o è estremamente circostanziato, o è un rumor che dista al massimo ventiquattr'ore dall'evento cui si riferisce, o è una bufala. In questo caso, universale.