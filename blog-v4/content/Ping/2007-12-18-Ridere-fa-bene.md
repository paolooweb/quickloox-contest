---
title: "Ridere fa bene"
date: 2007-12-18
draft: false
tags: ["ping"]
---

Quando mi dicono che quelli che scelgono Apple sono fanatici in genere mi scaldo abbastanza, soprattutto perché gli altri non scelgono e avrei in mente qualche parola più forte di fanatico.

Fortunatamente c'è chi sdrammatizza, per esempio con questo quiz di <a href="http://www.justsayhi.com/bb/apple_addiction" target="_blank">Dipendenza da Apple</a>.

<strong>Stefano</strong>, che me lo ha segnalato, ha fatto 73 percento. Io ho fatto 64 percento. Più importante di tutto, mi sono fatto tre minuti di divertimento, alla faccia dei luoghi e delle lingue comuni.