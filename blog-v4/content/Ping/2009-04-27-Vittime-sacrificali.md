---
title: "Vittime sacrificali"
date: 2009-04-27
draft: false
tags: ["ping"]
---

Ha suscitato scalpore l’amministratore delegato supplente di Apple, Tim Cook, <a href="http://www.macworld.it/blogs/ping/?p=2280" target="_self">parlando delle tastiere</a> <em>cramped</em> (sacrificate, ristrette, costrette, limitate) dei <em>netbook</em>.

Ora sembra sia un problema di <a href="http://www.macworld.it/blogs/ping/?p=2280&amp;cp=1#comment-36001" target="_self">denigrazione</a> o magari di rapporto prestazioni/prezzo <a href="http://www.macworld.it/blogs/ping/?p=2280&amp;cp=1#comment-36012" target="_self">che spaventa qualcuno</a> (certo! <em>in primis</em> Microsoft, che è costretta a metterci sopra Windows Xp a prezzo stracciato e ci rimette un sacco di soldi), comunque una polemica, ossia una divergenza di opinioni. È invece un problema pratico ed evidente.

C'è una bella <a href="http://www.anandtech.com/mobile/showdoc.aspx?i=3399&amp;p=4&amp;cp=3" target="_blank">recensione delle tastiere dei netbook su AnandTech</a>. Per chi teme l'inglese raccomando le foto di comparazione a fine articolo, quando la stessa monetina viene appoggiata su tre computer diversi e mostra benissimo le proporzioni tra una tastiera e un'altra.

La lettura è istruttiva. Riporto alcuni brani significativi:

<cite>Tutti questi</cite> netbook <cite>hanno uno spazio insufficiente per ospitare una tastiera normale, quindi ci vogliono sacrifici.</cite>

<cite>Sull'Eee Pc, Asus ha tentato di conservare la disposizione standard dei tasti facendo tasti molto piccoli. Il vantaggio è che sai dove si trova tutto e che passare da una tastiera normale non è difficile, lo svantaggio è che spesso si premono involontariamente due tasti al posto di uno.</cite>

<cite>Dell ha scelto una strada diversa. Ha eliminato i tasti funzione in altro e riorganizzato parte della punteggiatura, ingrandendo i tasti principali. Sull'Inspiron Mini 9 è molto meno probabile digitare con errori, ma appena c'è di mezzo la punteggiatura velocità e precisione scendono considerevolmente. Dell dichiara che dopo un quarto d'ora uno ci si abitua ed è vero. Il problema è che prima o poi bisogna tornare su una tastiera normale e, in una direzione o nell'altra, ci si sentirà a disagio.</cite>

Se queste non sono tastiere sacrificate, che cosa sono?