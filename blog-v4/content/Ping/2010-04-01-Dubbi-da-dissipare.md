---
title: "Dubbi da dissipare"
date: 2010-04-01
draft: false
tags: ["ping"]
---

Matt Deatherage di <a href="http://www.macjournals.com/mdj/" target="_blank">Mdj</a> (in letargo pubblicativo, ma sempre vigile) ha scritto varie cose interessanti sulla propria mailing list. Riassumo alcune note fondamentali:

<cite>Aggiornamenti Mac: beh. Tutto prima o poi viene aggiornato. Apple non ha la mentalità da branco dei fabbricanti di Pc generici e non sente il bisogno di spingere una macchina sul mercato nel giorno stesso in cui diventa disponibile un</cite> chip <cite>Intel. Il mercato finora va bene e le vendite continuano ad aumentare. [&#8230]</cite>

<cite>Nonostante il piagnisteo pre-iPad di quelli che volevano un</cite> netbook <cite>Mac, Steve Jobs ha detto chiaramente che un computer sottodimensionato in potenza e e dimensioni &#8211; praticamente il MacBook di tre anni fa in un telaio più piccolo &#8211; non è un prodotto che Apple vuole propinare ai propri clienti, neanche se i clienti lo chiedono. Apple ritiene che l'&#8220;ultramobilità&#8221; stia andando in una direzione diversa, come qualcuno potrebbe essersi accorto da quei due miliardi e mezzo di articoli su qualcosa chiamato iPad. [&#8230;]</cite>

<cite>Non credo che Mac stia &#8220;estinguendosi&#8221;. L'intera strategia di Apple si basa sul disporre delle proprie piattaforme, hardware e software, e non c'è alcun segno che la crescita di Mac stia rallentando, né che la tendenza si stia invertendo. Ma l'attenzione interamente dedicata a Mac che vedevamo dieci anni fa, con versioni fondamentali del software di sistema quasi ogni anno e tutte le gamme di prodotto rinnovate ogni 8-10 mesi, è probabilmente terminata per il futuro prevedibile.</cite>

Sono un po' stanco di sentire gente che pensa a Mac come se fosse il 1999. È il 2010 e il panorama dell'informatica è cambiato. I progressi tecnologici sono diventati incrementali e non sono più i salti quantici di una volta; avere <i>subito</i> il nuovo processore o la nuova scheda video fanno realmente la differenza per pochissimi. Pochissimi che, se le prestazioni sono realmente la prima cosa, dovrebbero puntare a un Mac Pro invece che su un portatile dagli inevitabili compromessi.

Ugualmente, la nozione che Apple non abbia interesse su Mac perché c'è da lanciare iPad è, al minimo, infantile.