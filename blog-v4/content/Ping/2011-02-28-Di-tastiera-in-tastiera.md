---
title: "Di tastiera in tastiera"
date: 2011-02-28
draft: false
tags: ["ping"]
---

Quando ho acquistato iPad non avrei mai pensato che ci avrei scritto cos&#236; tanto. Su Mac uso Pages pochissimo, il mio ambiente di scrittura è BBEdit. Ho già prodotto molto più testo su <a href="http://itunes.apple.com/it/app/pages/id361309726?mt=8" target="_blank">Pages iPad</a> di quanto non ne abbia mai fatto su <a href="http://itunes.apple.com/it/app/pages/id409201541?mt=12" target="_blank">Pages Mac</a>.

Per l'Html la mia scelta resta <a href="http://www.textasticapp.com/" target="_blank">Textastic</a>, che ora è arrivato all'edizione 2.1. Si integra con Dropbox, ha l'autocorrezione e l'autocompletamento di sistema, una tastiera ausiliaria per aiutare programmatori e accatiemmellisti, un sito dove è straordinariamente efficiente chiedere <i>feedback</i> e un autore che ascolta (una funzione della versione 2.0, ho controllato, l'ho chiesta io e solo io ed è arrivata. Non ci potevo credere). Non tutto va ancora come vorrei e devo ammettere che un vero emulo di BBEdit per iPad non esiste ancora. Però si migliora e quando leggo critiche sulla tastiera virtuale di iPad mi viene da ridere. Non può emulare al cento percento quella fisica; ma all'ottanta percento s&#236; e in mobilità estrema oramai scrivo a una velocità che fa impressione anche a me.

Avevo già scritto più o meno queste cose, ma oggi le riscrivo con più convinzione e con ulteriore raccomandazione. Tra l'altro si approssima iPad 2 e per gli scrittori mobili potrebbe essere una occasione ancora migliore.