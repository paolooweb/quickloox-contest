---
title: "Anche stavolta è andata"
date: 2009-02-04
draft: false
tags: ["ping"]
---

Utile solo per i pionieri di iPhone, quelli con la versione Edge: nessun problema nell'aggiornarsi al firmware 2.2.1 ed effettuare l'obbligatorio <em>jailbreak</em> con <a href="http://blog.iphone-dev.org/" target="_blank">QuickPwn 2.2.5</a>.