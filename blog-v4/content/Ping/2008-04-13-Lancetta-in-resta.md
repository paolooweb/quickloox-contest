---
title: "Lancetta in resta"
date: 2008-04-13
draft: false
tags: ["ping"]
---

L'orologio digitale è perfetto nello spiegare che ora è. Quello analogico spiega lo scorrere del tempo.

Il bravo <a href="http://freesmug.org" target="_blank">Gand</a> lamentava giustamente <a href="http://www.macworld.it/blogs/ping/?p=1734&amp;cp=1#comment-33760">la mancanza</a> di un orologio analogico da scrivania su Mac OS X. C'è stato fino a Jaguar e poi è stato eliminato.

Ma ci sono le soluzioni.

Quella ordinaria è mostrare in modo analogico l'orologio nella barra dei menu, come faccio io. L'icona si integra esteticamente meglio con le altre.

Quella scontata è andare a scaricare uno dei vari orologi presenti su Internet. <a href="http://www.furrysoft.de/?page=timedisc" target="_blank">TimeDisc</a> è solo un esempio, diverso dal solito.

Poi c'è quella elegante: grazie ai Maccanici, possiamo scaricare <a href="http://www.furrysoft.de/?page=timedisc" target="_blank">l'orologio originale di Jaguar</a>. Funziona perfettamente anche su Leopard.

Poi c'è l'accanimento terapeutico, che consiste nel mostrare un <em>widget</em> di Dashboard sul desktop. Gli orologi analogici per Dashboard sono numerosissimi, da quello delle <a href="http://www.macprime.ch/widgets/info/widgetid100013" target="_blank">ferrovie svizzere</a> a <a href="http://www.macupdate.com/info.php/id/24819/big-analog-clock" target="_blank">quello enorme</a> per essere visto anche da lontano.

Per vedere un <em>widget</em> sul desktop si apre il terminale e si dà il comando

<code>defaults write com.apple.dashboard devmode YES</code>

e si fa logout-login, per fare ripartire Dashboard.

Poi si entra con F12 in Dashboard. Si trascina il <em>widget</em> con il mouse e, mentre lo si tiene trascinato, si preme ancora F12.

Per riportare il <em>widget</em> dentro Dashboard, lo si tiene trascinato nel Finder e si preme F12.

Per annullare la preferenza impostata nel Terminale, si dà il comando

<code>defaults write com.apple.dashboard devmode NO</code>

Quanto tempo si può passare, a parlare di orologi!