---
title: "Il pesce di novembre"
date: 2009-11-16
draft: false
tags: ["ping"]
---

Uso il Terminale per poche cose di piccola amministrazione e qualche esperimento per espandere un tantino i miei angusti orizzonti.

Era <a href="http://www.macworld.it/blogs/ping/?p=1949" target="_self">da tempo</a> che leggevo di <a href="http://fishshell.org/index.php" target="_blank">fish</a>, una <i>shell</i> (un motore di riga di comando per il Terminale) che si propone come più moderna e amichevole di <code>bash</code>, lo standard di Mac OS X, e che volevo installarla.

L’installazione a mano, con relativa compilazione, era fuori discussione, specie quando per Mac OS X esistono ben due sistemi di facilitazione delle installazioni Unix: <a href="http://www.finkproject.org" target="_blank">Fink</a> e <a href="http://www.macports.org" target="_blank">MacPorts</a>.

Entrambi avevano bisogno di aggiornamento, perché la loro installazione in Leopard non funziona automaticamente in Snow Leopard, per varie faccende legate al passaggio del <i>software</i> ai 64 bit.

Così ho organizzato l’aggiornamento e la (piccola) sfida: vedere chi tra MacPorts e Fink sarebbe stato in grado di installare <code>fish</code> su un sistema dove è impostato l’avvio a 64 bit.

Non ho perso tempo nella preparazione all’aggiornamento: ho buttato nel Cestino senza complimenti le cartelle esistenti di Fink e MacPorts. Poi ho reinstallato i due sistemi da zero, scaricando le opportune versioni.

Inizialmente MacPorts si è portato in vantaggio. Infatti la versione per Snow Leopard è un normale <i>package</i> di installazione, mentre per avere Fink a 64 bit bisogna scaricare una <i>tarball</i> Unix che va decompressa e seguita da un comando a mano che bisogna per forza leggere sulla pagina del loro sito.

Però poi Fink ha installato <code>fish</code> a perfezione e invece MacPorts si blocca nel corso dell’operazione con errori di vario genere.

I 64 bit oggi sono uno sfizio essenzialmente inutile, ma domani saranno la regola. Oggi, sui 64 bit (e con <code>fish</code>), Fink parte male ma arriva meglio di MacPorts.