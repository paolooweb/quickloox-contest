---
title: "Il dono della sintesi"
date: 2010-06-08
draft: false
tags: ["ping"]
---

John Gruber di Daring Fireball ce l’ha, io molto meno. Per cui gli rubo questa <a href="http://daringfireball.net/2010/06/one_year" target="_blank">chiosa</a> al convegno 2010 degli sviluppatori Apple, scritto alla vigilia del <i>keynote</i> di Steve Jobs.

<cite>Mancano poche ore al</cite> keynote <cite>e quello che mi colpisce della Wwdc di quest’anno non è alcun</cite> rumor <cite>o annuncio specifico. […]</cite>

<cite>Mi colpisce questo. Un anno fa ero nello stesso albergo aspettando lo stesso</cite> keynote <cite>dello stesso convegno. iPad era un rumore lontano e Steve Jobs era in permesso malattia, reduce da un trapianto di fegato di due mesi prima. Ora eccoci qui e iPad è un successo straordinario che cambia il mercato (sto scrivendo queste parole usandone uno), la capitalizzazione di mercato di Apple ha sorpassato quella di Microsoft e Steve Jobs si sta preparando a salire sul palco, in salute e all’apice della sua partita.</cite>

<cite>Accidenti, che anno.</cite>