---
title: "La tigre e il giocone<p>"
date: 2005-07-08
draft: false
tags: ["ping"]
---

Altra figuraccia dei siti di rumor che oramai contano come il due di picche<p>

Scrivo queste righe che è appena passata la mezzanotte e da pochi minuti è l&rsquo;otto di luglio.<p>

Avevo la tentazione di riportare i link di tutti i siti che hanno preannunciato il cellulare congiunto di Apple e Motorola, con tanto di foto, specifiche, commenti, critiche e quant&rsquo;altro.<p>

Per quanto mi riguarda, ho passato l&rsquo;ora prima di cena a iniziare il <a href="http://www.macdevcenter.com/pub/a/mac/2005/05/20/terminal1.html">corso di Terminale per Tiger</a> presente su MacDevCenter. Ho passato l&rsquo;ora dopo cena nelle <a href="http://www.worldofwar.net/cartography/worldmap/badlands.php">Badlands</a> di World of Warcraft (e sono ancora vivo, Bol).<p>

Ho imparato qualcosa e mi sono divertito. Ora, a mezzanotte passata, c&rsquo;è in rete gente che mi chiede dove trovare le informazioni del nuovo annuncio Apple, che non c&rsquo;è stato. Hanno seguito troppo i siti dei pettegolezzi, dove non si impara niente e nemmeno ci si diverte.<p>

Non ne azzeccano più una. Battuti perfino dal Wall Street Journal.<p>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>