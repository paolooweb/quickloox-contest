---
title: "La forza del passapaura"
date: 2009-09-26
draft: false
tags: ["ping"]
---

Ho superato molte delle mie paure da Terminale grazie ai <i>worksheet</i> di BBEdit.

Il <i>worksheet</i> è un documento di testo in cui si possono dare comandi Unix. Altrimenti, visto dall'altra parte dello specchio, è un Terminale che permette l'<i>editing</i> delle righe come se fossimo in un documento di testo.

In un modo o nell'altro, è utilissimo, a partire dal salvataggio di comandi Terminale strani e lunghi che non c'è più bisogno di ricordare, ma sono salvati nel <i>worksheet</i> in attesa che vengano utili.

Adesso qualcuno dirà che BBEdit costa 125 dollari e che non si può permettere i <i>worksheet</i>.

In effetti <a href="http://www.barebones.com/products/bbedit" target="_blank">BBEdit</a> fa mille altre cose ed è indicato a un professionista. Adesso però c'è Eddie, un <i>worksheet</i> gratuito, per giunta ispirato al Macintosh Programmer's Workshop con cui si programmava negli anni ottanta.

Con <a href="http://www.el34.com/" target="_blank">Eddie</a> la paura del Terminale passa più facilmente.