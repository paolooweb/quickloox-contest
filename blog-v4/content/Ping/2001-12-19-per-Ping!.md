---
title: "Cofondatore insieme a Paolo Capobussi di Internet On-Line, la prima rivista cartacea italiana in assoluto a Internet, Lucio Bragagnolo lavora nell’informazione e nell’informatica dal 1982."
date: 2001-12-19
draft: false
tags: ["ping"]
---

Attualmente si occupa di informatica e Web, con particolare riferimento alle tematiche di Search Engine Optimization e Community Building.
Lavora con computer Apple da quando esiste Apple e, oltre alla collaborazione con Macworld, scrive di scienza e tecnologia per il quotidiano svizzero Il Corriere del Ticino.
Nel tempo libero gioca a basket, va al cinema, pratica giochi di ruolo, ascolta musica e legge. E' inoltre membro dello staff del PowerBook Owners Club, <http://www.poc.it>.
Ama ricevere posta elettronica e può essere sempre raggiunto all’indirizzo lux@mac.com.