---
title: "Retrocollegamenti"
date: 2008-08-28
draft: false
tags: ["ping"]
---

Il preziosissimo <a href="http://www.tiphones.com" target="_blank">Stefano</a> notifica che <a href="http://www.manhattan-products.com/driver-usb-rs232.shtml" target="_blank">uno di questi cavi Usb-Rs232</a> era in vendita presso un normale ipermercato Auchan al prezzo accessibilissimo di 16 euro.

La Rs232 era la porta seriale tipica dei computer prima che Apple cambiasse tutto mettendo Usb sul primo iMac. E all'inizio questi convertitori avevano un prezzo fuori dal mondo. Ora non più.

Se lo chiedi al commesso, ti dirà che non funziona su Mac. Perché non sa che cos'è il Mac, più che per il cavo.