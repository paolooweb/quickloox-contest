---
title: "Come passa il tempo"
date: 2010-02-07
draft: false
tags: ["ping"]
---

Bello che l'informatica diventi sempre più sicura. Molto probabilmente domani Microsoft tapperà un buco di Windows.

Che se ne stava l&#236; <a href="http://seclists.org/fulldisclosure/2010/Jan/341" target="_blank">da diciassette anni</a>.

Poi uno dice che non ci sono virus per Mac perché è poco diffuso.

Grazie a <a href="http://www.accomazzi.net" target="_blank">Misterakko</a> per la segnalazione!