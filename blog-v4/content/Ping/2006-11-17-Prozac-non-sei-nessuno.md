---
title: "Prozac non sei nessuno"
date: 2006-11-17
draft: false
tags: ["ping"]
---

Giornata faticosa, un po' frustrante, tempo brutto, poca voglia. Perfino di aprire World of Warcraft. Nei casi veramente disperati, ho un rimedio infallbile: i <a href="http://www.versiontracker.com/macosx/screensavers" target="_blank">salvaschermo</a> da VersionTracker. 