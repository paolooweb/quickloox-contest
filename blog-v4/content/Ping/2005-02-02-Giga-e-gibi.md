---
title: "Giga e gibi<p>"
date: 2005-02-02
draft: false
tags: ["ping"]
---

E ora un momento di insostituibile alfabetizzazione informatica<p>

Il numero di persone che ignorano questa cosa è imbarazzante. Tutti convinti di avere un disco rigido difettoso o che gli abbiano rubato qualcosa, dal momento che sulla scatola è da 30 gigabyte ma il conto dei byte è 28.943.745 o chissà che altra cosa.<p>

Un byte, direbbe Shakespeare, è un byte è un byte è un byte. Otto bit.<p>

Da lì in poi, se si conta in base dieci si va per kilobyte, megabyte, gigabyte eccetera. Se si conta in base due si totalizzano kibibyte, mibibiyte, gibibyte e via moltiplicando. Un gigabyte è il cubo di mille; un gibibyte è il cubo di 1.024.<p>

Tutto ciò senza contare il fatto che la formattazione del disco, creando una struttura per accogliere i dati, riduce la capienza teorica, come dovrebbe sapere chiunque abbia una libreria in casa. O gli scaffali della sua libreria occupano spazio zero?<p>

Non è un mio mal di testa o una subdola invenzione di Apple per rubare byte al povero cliente truffato, ma una convenzione internazionale. Fai pure una ricerca con Google, se è già arrivato nella tua città.<p>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>