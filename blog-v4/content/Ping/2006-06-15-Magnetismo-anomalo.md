---
title: "Magnetismo anomalo"
date: 2006-06-15
draft: false
tags: ["ping"]
---

<strong>Fearandil</strong> segnala che il MacBook, trasportato nella borsa senza una custodia, gli si &egrave; aperto accidentalmente.

Per quanto sento &egrave; un episodio isolato. Ma &egrave; il caso di starci attenti e, come lui stesso commentava, sperare che diventino disponibili al pi&ugrave; presto buone custodie come quelle esistenti a legioni per i modelli meno recenti.