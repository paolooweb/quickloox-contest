---
title: "Estasi mystica<p>"
date: 2005-10-22
draft: false
tags: ["ping"]
---

Quando giocare vuol dire stare fermo<p>

Dalla redazione di Macworld mi è arrivata una copia di <a href="http://www.mystvgame.com/">Myst V: End of Ages</a>, di cui ho già parlato in due occasioni.<p>

Molto probabilmente ne risulterà una recensione su Macworld. Il problema è che cosa dire.<p>

Il gioco contiene una storia ricchissima, enigmi logici affascinanti, una trama profonda e complessa che aspetta solo di immergervisi.<p>

Ma l’unica cosa in cui finora vorrei immergermi è il mare di quella spiaggia da dove si comincia, incantato dal suono della risacca, con i riflessi che potrei guardare per ore.<p>

Come si fa a trovarsi in un mondo così incredibilmente attraente e farsi venire voglia di risolvere misteri?<p>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>