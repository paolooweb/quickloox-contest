---
title: "Lei non sa che dico io"
date: 2007-09-11
draft: false
tags: ["ping"]
---

<cite>Non c'è possibilità che iPhone otterrà una qualsiasi quota significativa di mercato. Nessuna possibilità.</cite>
&#8212; Steve Ballmer, amministratore delegato di Microsoft, 30 aprile 2007

iPhone ha venduto <a href="http://www.macdailynews.com/index.php/weblog/comments/apple_sells_one_million_iphones_in_74_days/" target="_blank">un milione di pezzi</a> con venti giorni di anticipo (su tre mesi) rispetto all'obiettivo dichiarato sei mesi prima della sua presentazione. Fino a qui si sapeva.

Confrontando i dati delle <em>vendite estive in Usa</em> di iPhone 2007 e delle <em>vendite natalizie mondiali</em> degli smartphone 2006, emerge quanto segue:

- iPhone batte Sony Ericsson;
- iPhone batte Palm;
- iPhone batte Motorola.

Resta inavvicinabile Nokia, che ha il 50 percento del mercato, e resta sopra Rim, quella del Blackberry. Per il resto, iPhone è già il terzo incomodo, dopo neanche tre mesi di vita.

Speriamo nella loquacità di Ballmer.