---
title: "Parlo di Carlo"
date: 2007-05-20
draft: false
tags: ["ping"]
---

<strong>Carlo</strong> è il tenutario del podcast Tecnica Arcana. È anche socio dell'All About Apple Club.

Per completare il sillogismo, Carlo ha creato una puntata extra del podcast dedicata all'Open Day che ha festeggiato i primi cinque anni del museo Apple.

Consiglio di ascoltarla (ci sono un sacco di cose, purtroppo anche la mia voce, ma il resto merita) e anche di seguire il podcast. Si conoscerà anche meglio Carlo, che dal vivo è persona godibile, aperta e interessante come non sempre accade.

Direi che la partenza ideale è la <a href="http://www.deathlord.it/court/articolo.asp?articolo=220" target="_blank">Court of Misanthropy</a>.