---
title: "Fai la cosa giusta"
date: 2007-07-19
draft: false
tags: ["ping"]
---

Fai come <strong>Federico</strong>. Che ha trovato incantevole Delicious Library e gli ha trovato pure un difetto sostanziale. Non gestire i libri italiani.

Che ha fatto Federico? Si è lamentato alla fermata del tram? Ha scelto di soffrire in silenzio? No: ha scritto agli autori.

I quali gli hanno risposto <cite>aggiungiamo il tuo voto alla lista di funzioni da inserire</cite>. Significa che hanno un elenco e che ne tengono conto.

Curiosando nei forum, Federico ha scoperto inoltre che Delicious Library è scriptabile. In linea di principio, dunque, è possibile inserire le funzioni che mancano scriptandole.

La domanda originale di Federico era se io conoscessi qualcun altro interessato e voglioso di scrivere agli autori per fare sano <em>lobbying</em>. Non gli rispondo ma rilancio. Ci sono almeno due modi per rendere <a href="http://www.delicious-monster.com/" target="_blank">Delicious Library</a> più interessante di quanto non sia già. Uno è scrivere in tanti (interessati) e un altro è scriptare. Dove basta uno capace.