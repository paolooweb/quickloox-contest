---
title: "Go Girls Go Tech<p>"
date: 2005-08-02
draft: false
tags: ["ping"]
---

Un sito per tutta la famiglia, possibilmente là dove&hellip;<p>

Saltuariamente questo spazio, per i nuovi arrivati, tratta temi che vanno oltre il Mac. Stavolta la cosa è semplice: vai su <a href="http://www.girlsgotech.org">Girls Go Tech</a>. Soprattutto se hai una figlia dell&rsquo;età giusta, o se la figlia sei tu.<p>

Nel secondo caso non avrai paura di niente; nel primo potresti avere paura dell&rsquo;inglese. Beh, è sbagliato. Via la paura e sotto con un buon dizionario, se proprio.<p>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>