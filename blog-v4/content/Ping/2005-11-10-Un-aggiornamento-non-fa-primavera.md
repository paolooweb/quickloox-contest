---
title: "Un aggiornamento non fa primavera<p>"
date: 2005-11-10
draft: false
tags: ["ping"]
---

Potrebbe, potrebbe non, ma alla fine non saranno cose estranee?

Questo messaggio arriva, piacevolmente, da <a href="http://homepage.mac.com/matteo.discardi">Matteo</a>:<p>

<cite>Ti scrivo riguardo al tuo Ping &ldquo;L&rsquo;aggiornamento miracoloso&rdquo;.</cite><p>

<cite>Non conosco Sergik, ma ho abbastanza ore davanti a XPress per fare due conti. La versione 4.11 gira sotto Classic, quindi non credo che il 10.4.3 abbia apportato modifica alcuna in un ambiente che non cambia da un paio d'anni. La visualizzazione, inoltre, non è data da un motore di Apple, ma da un engine proprietario Quark.</cite><p>

<cite>Inoltre, molto dipende anche dal formato con il quale è salvata l'immagine: al cambio di formato, cambia anche di molto l'anteprima di visualizzazione di Quark.</cite><p>

<cite>Personalmente, trovo che 10.4.3 funzioni né più né meno come la precedente versione. Non è né il demonio, né il salvatore. Ho un piccolo dubbio sul supporto AC3 di QuickTime, ma non sono abbastanza esperto per accusare una parte o l'altra. </cite><p>

<cite>Se Sergik ha notato miglioramenti, meglio per lui. Sono convinto che può aver notato alcuni effetti su applicazioni che io non ho notato, in particolare applicazioni Apple, o altre che sfruttano librerie apple. Ma resto dubbioso per Quark. E anche per Classic.</cite><p>

A chiarezza, non ho compiuto uno studio approfondito sull&rsquo;aggiornamento e quindi non sono in grado di dare una parola definitiva. Potrebbe, oppure no.<p>

È che un sacco di gente accusa sistematicamente gli aggiornamenti di provocare i più svariati malfunzionamenti nel sistema. Dare a un aggiornamento, per una volta, la responsabilità di un miglioramento mi sembra un buon contrappasso.<p>

E se la responsabilità fosse ingiustificata, la cosa direbbe molto su quanto siano giustificate le accuse.<p>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>