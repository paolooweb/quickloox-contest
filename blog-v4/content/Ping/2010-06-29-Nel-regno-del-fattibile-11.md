---
title: "Nel regno del fattibile / 11"
date: 2010-06-29
draft: false
tags: ["ping"]
---

<cite>pregasi elencare azioni fattibili su un iPad e non fattibili su un Sony Vaio W da 10&#8221;</cite>.

<a href="http://www.wired.it/news/archivio/2010-06/24/la-messa-la-celebro-con-l-ipad.aspx" target="_blank">Dire messa</a>.

Grazie <b>Phil</b>!

(e aggiungerei i <a href="http://www.youtube.com/watch?v=5OLP4nbAVA4&amp;feature=player_embedded" target="_blank">ritratti su iPad</a> dipinti da David Kassan)