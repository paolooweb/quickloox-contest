---
title: "Come piove si ritirano"
date: 2010-05-05
draft: false
tags: ["ping"]
---

Leggo che iPad è pieno di difetti. Niente Flash, niente porte Usb, niente <i>webcam</i> e via di questo passo, perché oramai il giudizio non lo si dà più sul piatto fumante in tavola, ma si legge la ricetta e si sentenzia <i>manca la noce moscata</i>.

Mi sta bene, a patto che funzioni la conseguenza logica. Sarà sufficiente pubblicare una tavoletta concorrente con Flash, porte Usb e webcam per fare valida concorrenza a iPad.

A gennaio Steve Ballmer, amministratore delegato di Microsoft, ha presentato sul palco del Consumer Electronics Show di Las Vegas una tavoletta di Hewlett-Packard, chiamata familiarmente <i>slate</i> (lavagna). Pare, si dice, che <a href="http://techcrunch.com/2010/04/29/hewlett-packard-to-kill-windows-7-tablet-project/" target="_blank">sia stata cancellata</a>, forse definitivamente, forse per ripartire con WebOS di Palm invece che con Windows 7, non si sa. Comunque, tempi lunghi.

A settembre Microsoft ha presentato i <i>concept video</i> di Courier, il quaderno interattivo che si usa con le mani e con lo stilo, fatto di due schermi affiancati. Progetto interessantissimo, potenziale di innovazione molto alto. Microsoft ha confermato ufficialmente che almeno per ora <a href="http://www.engadget.com/2010/04/29/microsoft-confirms-kills-courier-in-one-fell-swoop/" target="_blank">non se ne fa niente</a>. Tanto video, niente fatti.

Ha deciso di fare una tavoletta anche Rim, quella di BlackBerry, con Android. Pensavano di fare BlackPad entro fine anno. Adesso <a href="http://www.thestreet.com/_yahoo/story/10746765/1/rim-chokes-on-its-ipad-killer.html" target="_blank">ci hanno ripensato</a>, se ne parla per l'anno prossimo.

Sembra che iPad abbia l'effetto di un acquazzone: si ritirano tutti.

Wired suggerisce <a href="http://www.wired.com/gadgetlab/2010/05/how-to-make-an-ipad-beating-tablet/?utm_source=feedburner&amp;utm_medium=feed&amp;utm_campaign=Feed%3A+wired%2Findex+%28Wired%3A+Index+3+%28Top+Stories+2%29%29" target="_blank">come battere iPad</a>. <cite>È sufficiente fare qualcosa che sia meglio di un personal computer</cite>, argomento. <cite>Ci vuole tanto?</cite>

Apparentemente non è come dirlo, neanche per una Microsoft o per una Hewlett-Packard. Forse mettere una porta Usb non è facile come sembra. Forse non è la webcam a fare la differenza.

Forse gli sputasentenze dovrebbero chiedersi come mai multinazionali grandi come città non sono arrivate ad avere le loro preziose intuizioni da aperitivo.