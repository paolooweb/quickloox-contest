---
title: "Docente per caso"
date: 2010-06-03
draft: false
tags: ["ping"]
---

Sono un po' lusingato e imbarazzato: terrò un laboratorio alla <a href="http://almed.unicatt.it/it/summer_school/summer_school_condividere_e_comunicare_la_conoscenza_in_azienda_il_knowledge_management_in_esperienz" target="_blank">Summer School</a> dell'Alta scuola in media, comunicazione e spettacolo dell'Università Cattolica del Sacro Cuore, con sponsorizzazione Vodafone.

Si parlerà &#8211; e lavorerà &#8211; di <i>scrittura per i media digitali</i>, mercoled&#236; 30 giugno dalle 9 alle 13 presso il Vodafone Learning Center di via Caboto 15 a Corsico (si arriva in auto dalla Tangenziale, o da Milano con una breve passeggiata dal capolinea della 58).

La Summer School contiene molto di più e dura dal 21 giugno al 2 luglio, tra Cattolica e Learning Center, con un programma notevole sia qualità dei contenuti che, esclusi i presenti, dei docenti.

Normalmente non amo pubblicizzare troppo le cose che faccio, però Vodafone tiene molto alla massima diffusione delle proprie iniziative (e fa una cortese ma costante pressione in tal senso) e inoltre, per un laureato triennale, rappresenta una bella occasione.

Tutti i dettagli relativi a partecipazioni e costi si trovano nella <i>brochure</i> liberamente scaricabile dal sito. Io sto lavorando già da ora alla mattinata e non escludo di mostrare qualche pezzetto di Ping, se nessuno si offende.

Per gli interessati, i colloqui di ammissione alla Almed si tengono mercoled&#236; 9 giugno mattina. C'è da affrettarsi.