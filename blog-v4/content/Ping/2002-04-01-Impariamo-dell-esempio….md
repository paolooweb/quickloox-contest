---
title: "Impariamo dall’esempio altrui"
date: 2002-04-01
draft: false
tags: ["ping"]
---

Intanto che noi ci perdiamo nelle chiacchiere politiche, con la scuola c’è chi fa sul serio

Qualcuno si ricorda delle manifestazioni studentesche contro il Ministro della pubblica istruzione?
Giuste o sbagliate che fossero, mi fa impressione metterle a confronto con la notizia che lo stato americano del Maine quest’autunno fornirà gratuitamente a tutti gli studenti del settimo grado (equivalente della seconda media nostra) un iBook.
Giusto o sbagliato che sia, da una parte cresceranno ragazzi informatizzati e un po’ più pronti al futuro; dall’altra un branco di semianalfabeti tecnologici la cui preoccupazione sarà recuperare in un modo qualunque la patente europea del computer (una ridicolaggine che solo a Bruxelles si poteva concepire).
Capire dove succederà ciascuna di queste cose viene lasciato come esercizio al lettore.

<link>Lucio Bragagnolo</link>lux@mac.com