---
title: "L'ignoranza è forza"
date: 2010-07-22
draft: false
tags: ["ping"]
---

<cite>&#8230;un momento dopo, dal grande teleschermo in fondo alla sala esplodeva un macinare disgustoso di parole, come di una mostruosa macchina dagli ingranaggi rimasti senza olio. Un rumore che faceva digrignare i denti e rizzare i peli alla base del collo. Era iniziato l'Odio.</cite>

&#8212; George Orwell, <a href="http://www.orwell.ru/library/novels/1984/english/en_p_1" target="_blank">1984</a>

E l&#236; ho deciso di uscire per sempre da un paio di forum.