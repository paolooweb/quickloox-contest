---
title: "Il fuso di Roma"
date: 2008-06-16
draft: false
tags: ["ping"]
---

<strong>Filippo Cervellera</strong> (quello di Macchino) invia un vero e proprio speciale sulla numerazione romana in digitale:

Sul sito di ChronArt sono mostrati diversi tipi di <a href="http://homepage.mac.com/chronart1/index.html" target="_blank">orologi artistico-futuristici</a>.

Secondo me merita di essere segnalato l'orologio digitale ma <a href="http://www.sonic.net/chronart/romdig.html" target="_blank">indicante l'ora con numeri romani</a>.

Per impostarlo bisogna inclinarlo di un certo numero di gradi (cosa che ricorda l'accelerometro di iPhone).

Sempre per i patiti del latino, segnalo l'equivalente dell'orologio <a href="http://www.haubergs.com/widgets.php#Roman" target="_blank">in forma software</a> come <em>widget</em> di Dashboard  e una <a href="http://sourceforge.net/projects/ratio-mercator/" target="_blank">calcolatrice multipiattaforma</a> i cui menu sono anch'essi scritti nell'antica grafia.

Ultimo &#8220;ma non ultimo&#8221;, segnalo che anche lo spettacolare, gratuito e <em>platform-independent</em> (è scritto in java) <a href="http://www.jonelo.de/java/nc/index.html" target="_blank">NumericalChameleon</a> 1.6.0, in grado di trattare le conversioni di numeri romani.

Mai visto un convertitore di unità cos&#236; completo!!! Un vero <em>must</em> da avere su qualsiasi computer!

Guarda la descrizione delle caratteristiche:

Qui Filippo aveva accluso il testo originale inglese. Lo fornisco in italiano per dare anch'io il mio bel contributo:

<em>NumericalChameleon converte numeri con precisione fino a mille cifre significative. Riconosce più di 3.200 unità in 82 categorie, comprese non solo quelle importanti della fisica come lunghezza, superficie, volume, elettricità, magnetismo, forza, potenza, energia, pressione, massa, temperatura, velocità, accelerazione, radioattività e tempo, ma anche tassi di cambio, fusi orari, numeri parlati e pronunciati, numeri romani, radici fino alla trentacinquesima, bit e byte, risoluzioni di schermo, codici colore, Unicode, prefissi telefonici internazionali, calcolo di calendari e vacanze, alfabeti fonetici e altro.</em>

Grazie mille. Anzi, M.