---
title: "Browser chiama TextEdit"
date: 2003-11-22
draft: false
tags: ["ping"]
---

Salvare pagine Web in modo più utile del solito

Salvare pagine Web. Beh, si usa iCab, o Web Devil, o qualche altro programma succhiapagine. Però l’impaginazione resta quella della pagina Web e magari è assai antieconomica da stampare; l’Html non è fatto per finire sul formato A4.

Ma da Panther c’è una soluzione in più. Aperta la pagina dentro il browser, Seleziona Tutto. Poi, Incolla dentro TextEdit (c’è anche un servizio che svolge questa funzione).

TextEdit fa un lavoro semplice ma incredibile a pensarci, perché mantiene attivi i link (cliccando si naviga, come nel browser) eppure rende perfettamente editabile il testo. Insomma, si può ottimizzare la pagina Web a piacimento per stamparla come ci pare, e intanto mantenerla viva.

TextEdit è una vera utility: impossibile da usare professionalmente come word processor o browser, ma in grado di risolvere una situazione intricata con semplicità ed eleganza.

<link>Lucio Bragagnolo</link>lux@mac.com