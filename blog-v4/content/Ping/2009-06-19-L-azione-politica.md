---
title: "L'azione politica"
date: 2009-06-19
draft: false
tags: ["ping"]
---

<a href="http://www.twitter.com" target="_blank">Twitter</a> ha rimandato un aggiornamento software, in modo che il servizio non venisse interrotto mentre dall'Iran i contestatori delle ultime controverse elezioni usavano il servizio di <i>microblogging</i> per sfuggire alle maglie della censura.

Google ha accelerato l'inserimento del <i>farsi</i> tra le lingue servite dal proprio <a href="http://www.google.it/language_tools?hl=it" target="_blank">software di traduzione automatica</a> (che sono 42, annoto).

Da noi siamo ai faccioni sui muri e alla polemica sul giornale.