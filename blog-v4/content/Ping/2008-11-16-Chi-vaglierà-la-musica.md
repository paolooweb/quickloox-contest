---
title: "Chi vaglierà la musica"
date: 2008-11-16
draft: false
tags: ["ping"]
---

In emergenza assoluta di spazio su disco e congestione assoluta del sistema, lentissimo su qualsiasi cosa, ho affrontato un pomeriggio di purga il più possibile spietata e, tra le altre, ho cestinato una parte consistente del corredo musicale di GarageBand.

Cestinare parti dell'offerta ufficiale Apple, nel mio caso, è sempre cosa cui fare attenzione, perché lavorativamente devo sempre essere in grado di riprodurre ragionevolmente situazioni in cui può trovarsi l'utente tipico. È la ragione, per dire, per la quale non posso risparmiare 250 mega di disco buttando via Mail, programma che appunto apro tre volte l'anno per lavoro e non per leggere la posta.

Cos&#236; ho dato un'occhiata al corredo musicale suddetto, dove una persona con esigenze diverse dalle mie potrebbe fare un bel pacco e buttare via tutto, due-tre giga di risparmio.

L'offerta musicale di GarageBand è semplicemente sterminata. Vero che bisogna avere un qualche interesse musicale; altrettanto vero che probabilmente è il programma più sottovalutato di iLife.

Tra parentesi, confermo la mia teoria che spendere tempo a fare pulizia è uno spreco e non una spesa. Non fossi in attesa del cambio di Mac, avrei fatto molto meglio a prendermi un disco rigido grande il doppio, lasciare fare tutto ad Assistente Migrazione e godermi un pomeriggio (abbondante), invece che grattare tutti i fondi di tutti i barili. Avrei guadagnato il 100 percento di spazio invece che l'otto percento. Fa differenza.