---
title: "A Microsoft conviene"
date: 2002-04-15
draft: false
tags: ["ping"]
---

Tra poco scade l’accordo di collaborazione firmato anni fa. Ma è un po’ lo stesso

Nell’agosto del 1997 Microsoft e Apple strinsero un accordo che fece rumore. Secondo gli ignoranti Microsoft “salvò” Apple e c’è ancora gente convinta che l’abbia comprata (!). Più o meno, in termini di denaro, fu come se io avessi guadagnato mille euro al mese e la ditta mi avesse offerto un premio extra di settantacinque euro. In termini di pubblicità e credibilità di Apple, al momento in difficoltà di immagine, fu importante, perché tanti pensano di non poter fare a meno di Microsoft e Microsoft si impegnava per cinque anni a lavorare su Office per Mac.
L’accordo sta per scadere e ambedue le parti assicurano che, comunque, tutto continuerà come prima. Per forza: Microsoft fa un sacco di soldi propinando Office Mac a gente che non ne ha bisogno. Ma Microsoft continuerà a produrre Office mica per “salvare” Apple; perché gli fa comodo.

<link>Lucio Bragagnolo</link>lux@mac.com