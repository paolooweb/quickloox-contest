---
title: "D&rsquo;accordo con Bynk<p>"
date: 2005-12-20
draft: false
tags: ["ping"]
---

Ogni tanto si trova una persona sensata<p>

Usi Delocalizer per togliere le lingue straniere dopo l&rsquo;installazione, invece che toglierle durante. Usi Cocktail per svuotare cache in modi e tempi imprevisti. Usi decine di altre utility che non si basano sul sistema operativo, ma vanno a scavare nel sistema operativo alla ricerca di scorciatoie facili a basso prezzo.<p>

Poi arriva un aggiornamento di sistema e qualcosa smette di funzionare. Forse perché il sistema è stato manipolato in modi non chiari, probabilmente imprecisi e troppo in profondità.<p>

E la furbata si trasforma in problema.<p>

Ecco perché non sarò mai abbastanza d&rsquo;accordo con <a href="http://www.bynkii.com/archives/2005/12/why_i_hate_haxi.html">Bynk</a>.<p>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>