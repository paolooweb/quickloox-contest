---
title: "Il world processor"
date: 2003-07-10
draft: false
tags: ["ping"]
---

Chi sia disposto a fare fatica può avere grandi sorprese

Negli anfratti più oscuri di Mac OS X è nascosto un word processor che in realtà fa molto di più: emacs.

Prova ad andare a vedere che cosa c’è in /usr/share/emacs/ e ti accorgerai che sembra un programma, invece è un mondo, con tanto di interprete Lisp, una versione di Eliza e perfino giochi.

Che poi sono la soluzione, per tutti quanti hanno chiesto la risposta, al piccolo quiz di qualche giorno fa (come giocare a Tetris avendo a disposizione solo i dischi di sistema e i Developer Tools? Che questi ultimi non so neanche se servano).

Dentro la directory del numero di versione (per esempio 21.2) c’è la directory play. Dentro quella c’è una serie di giochi.

Lanci emacs, premi Esc, scrivi x e poi scrivi il nome del gioco, senza estensioni. Et voilà.

<link>Lucio Bragagnolo</link>lux@mac.com