---
title: "Di cui parlare"
date: 2009-08-31
draft: false
tags: ["ping"]
---

Dopo uno iato di oltre un anno, è ripreso lo sviluppo di <a href="http://www.uplinklabs.net/~tycho/projects/xchat-aqua/" target="_blank">X-Chat Aqua</a>, client di chat Irc per Mac.

X-Chat Aqua è più spartano e meno moderno di altre alternative, come <a href="http://colloquy.info/" target="_blank">Colloquy</a> e <a href="http://homepage.mac.com/philrobin/conversation/" target="_blank">Conversation</a>. Tuttavia ha un proprio pubblico affezionato (me compreso) e comunque la diversità di opzioni va sempre perseguita e incoraggiata.

I programmatori cercano uno sviluppatore Cocoa. Se qualcuno avesse voglia di dare una mano&#8230;