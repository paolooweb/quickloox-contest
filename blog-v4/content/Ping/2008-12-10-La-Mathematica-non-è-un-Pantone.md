---
title: "La Mathematica non è un Pantone"
date: 2008-12-10
draft: false
tags: ["ping"]
---

Esulo per qualche momento dai temi strettamente Mac e ritorno sul tema dell'elaborazione matematica delle immagini, travestita da interfaccia utente grafica nel caso di Photoshop e invece <a href="http://www.macworld.it/blogs/ping/?p=2077" target="_self">allo scoperto in Mathematica 7</a>.

Come giustamente diceva Paolo Bertolo, Mathematica ha solo pubblicizzato maggiormente cose che altri ambienti, come Matlab, fanno da tempo (qualcuno che ne sa può dirmi se lo stesso vale per l'<em>open source</em> <a href="http://www.sagemath.org/" target="_blank">Sage</a>?).

Aggiungo argomenti al dialogo, o forse benzina sul fuoco, con il tema della programmazione genetica: software capace di evolvere, parola grossa dei suoi autori, o piuttosto di apprendere per iterazioni, direi.

Con la programmazione genetica tale Roger Alsing è riuscito ad <a href="http://rogeralsing.com/2008/12/07/genetic-programming-evolution-of-mona-lisa/" target="_blank">approssimare in modo mirabile la Gioconda</a> usando cinquanta poligoni trasparenti e facendo confrontare al programma il risultato ottenuto con l'originale.

E qui si aprono abissi filosofici: Alsing non ha fatto niente ed è il software che ha pensato a tutto. Oppure Alsing in realtà è capace di approssimare una Gioconda per mezzo di poligoni, non direttamente ma per mezzo di uno strumento che è la programmazione, non diversamente da come tutti sappiamo piantare un chiodo nel muro a patto di avere in mano un martello? Se fosse la seconda, imparare a programmare sarebbe una forma d'arte. E ci credo un po'.