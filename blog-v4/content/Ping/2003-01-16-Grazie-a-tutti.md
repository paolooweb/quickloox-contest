---
title: "Macworld-Mac@Work: grazie a tutti"
date: 2003-01-16
draft: false
tags: ["ping"]
---

Abuso di rubrica a scopo personale

Approfitto indebitamente dello spazio Ping! per ringraziare di cuore tutti coloro che hanno partecipato all’incontro con [Misterakko](http://www.accomazzi.net), Enrico Lotti Direttore Magno di Macworld e Franco Scuri del [PowerBook Owners Club](http://www.powerclub.org) presso il nuovo ["Mac@Work"](http://www.macatwork.net) di via Carducci a Milano.

Mi sono divertito, si sono dette cose interessanti e tutti gli intervenuti, da quelli in sala a chi ha visto lo stream video su Internet e quelli che hanno partecipato in chat, sono stati eccezionali per pazienza, gentilezza, curiosità, competenza eccetera eccetera.

Abbiamo dimostrato che la comunità Mac non solo esiste, ma è un bel metro sopra la media. E anche che a Milano c’è un posto bellissimo in cui trovarsi a parlare (e non solo) di Macintosh.

Lo rifacciamo? Ci saranno più sedie, prometto.

<link>Lucio Bragagnolo</link>lux@mac.com
