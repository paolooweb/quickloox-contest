---
title: "Passando per il New York Times"
date: 2009-03-21
draft: false
tags: ["ping"]
---

<cite>È assolutamente chiaro che da qui a cinque anni Apple avrà il tre-cinque percento del mercato dei lettori digitali.</cite>

Rob Glaser, amministratore delegato di RealNetworks, <a href="http://www.nytimes.com/2003/11/30/magazine/30IPOD.html?ex=1386133200&amp;en=750c9021e58923d5&amp;ei=5007&amp;partner=USERLAND&amp;pagewanted=6" target="_blank">al New York Times</a>, il 30 novembre 2003.

Articolo splendido per tutt'altre ragioni, su iPod, design, traiettoria di Apple, da rileggere assolutamente anche se vecchio di cinque anni e passa. Splendido esempio di giornalismo, pure.