---
title: "Un tigrotto in più"
date: 2006-11-24
draft: false
tags: ["ping"]
---

Per verificare il comportamento del <a href="http://tven.terratec.net/modules.php?op=modload&amp;name=News&amp;file=article&amp;sid=236" target="_blank">ricevitore Usb Terratec per il digitale terrestre</a> anche su una macchina sottodimensionata, ho aggiornato a Tiger l'iMac Lcd 14&#8221; che a casa fornisce il disco di backup e la condivisione della stampante, e che era comodamente rimasto su Panther fino a oggi.

L'iMac ha porte Usb 1.1 ed effettivamente EyeTv di <a href="http://www.elgato.com" target="_blank">Elgato</a> si sarebbe anche installato, ma l'hardware proprio non reagisce e non è questione di prestazioni ridotte o di arrangiarsi; non funziona e basta. Il passaggio a Tiger, d'altro canto, si è svolto come aggiornamento del sistema esistente e tutto continua a funzionare perfettamente ed esattamente come prima.

Una volta ero un po' diffidente nei confronti dell'aggiornamento e preferivo installare sempre un sistema vergine. Oramai ho cambiato idea; ho effettuato svariati aggiornamenti senza mai avere problemi. Se il Mac è sano, è inutile trattarlo da malato.