---
title: "Le tre strade alla civilizzazione"
date: 2010-08-09
draft: false
tags: ["ping"]
---

Non ci sono più i deserti agostani di una volta, fisicamente. Certe sere di agosto, specie quelle di temporale, lo sono ancora e ho rispolverato l'antico amore per l'ottimo strategico <em>open source</em> FreeCiv (oltre a trovare in rete più di qualcuno disposto a giocare!).

L'installazione del gioco è facilissima ma non banale: bisogna prima installare il file in formato .pkg presente nell'immagine disco scaricata, poi copiare FreeCiv.app nella cartella Applicazioni e da l&#236; lanciare il gioco.

Che si lancia solo se è stato scelto il pacchetto giusto. Il server del gioco offre tre opzioni e <a href="http://freeciv.wikia.com/index.php?title=Special:Outbound&amp;f=Download&amp;u=http%3A%2F%2Fprdownloads.sourceforge.net%2Ffreeciv%2Ffreeciv-x11-2.2.2-tiger-powerpc.dmg%3Fdownload" target="_blank">quella per Tiger</a> è la più semplice, essendo anche obbligata.

Da Leopard in poi le cose si fanno più complicate. Su Snow Leopard aggiornato, con boot a 64 bit (ma non dovrebbe fare differenza), a me funziona la <a href="http://freeciv.wikia.com/index.php?title=Special:Outbound&amp;f=Download&amp;u=http%3A%2F%2Fprdownloads.sourceforge.net%2Ffreeciv%2Ffreeciv-sdl-2.2.2-leopard-intel.dmg%3Fdownload" target="_blank">versione Sdl</a>.

C'è anche una <a href="http://freeciv.wikia.com/index.php?title=Special:Outbound&amp;f=Download&amp;u=http%3A%2F%2Fprdownloads.sourceforge.net%2Ffreeciv%2Ffreeciv-gtk-2.2.2-leopard-intel.dmg%3Fdownload" target="_blank">versione Gtk</a> (sono le diverse interfacce a video e audio), che a me su Snow Leopard non funziona e di più non saprei dire, per ora.

Dentro il file Read Me First ci sono le istruzioni per liberarsi dell'eventuale pacchetto non funzionante per installare l'altro, dato che sono mutuamente incompatibili.

Chi me lo fa fare, dirà qualcuno. Posso solo rispondere: se piace il genere, un gioco ricco e avvincente. E magari imparare a cancellare una cartella da Terminale, ammesso che si renda necessario, prelude a una maggiore conoscenza del proprio Mac, sai mai.

Per una vita più semplice, comunque, ora funziona anche il <a href="http://www.freeciv.net" target="_blank">gioco via browser</a>.

Sulla stessa falsariga raccomando vivamente anche <a href="http://www.freecol.org/" target="_blank">FreeCol</a>, ispirato al vecchio Colonization e vagamente reminiscente di Civilization, ambientato nell'America appena scoperta. Si scoprirà che c'erano di mezzo anche danesi e russi, nonché il gusto di giocare dalla parte degli europei oppure da quella degli indiani.