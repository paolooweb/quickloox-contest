---
title: "Pile scorrevoli"
date: 2009-06-24
draft: false
tags: ["ping"]
---

Sempre avanti con la traduzione delle <a href="http://www.apple.com/it/macosx/refinements/enhancements-refinements.html" target="_blank">novità di Snow Leopard</a>, una al giorno, in supplenza di Apple che non provvede.

<b>Scorrere attraverso le pile</b>

Le pile ora sono scorrevoli in vista come griglia, cos&#236; è facile visionarne tutti gli elementi.

Piccoli aggiustamenti, sempre qualcosa in più.