---
title: "Backup finale<p>"
date: 2005-02-25
draft: false
tags: ["ping"]
---

Se hai una flat risparmi in Dvd<p>

Consapevole di quanto serva il backup, ho sempre fatto i backup. Da un po&rsquo;, però, faccio solo quelli dei dati che creo io: lavoro, fotografie, testi e nient&rsquo;altro.<p>

Shareware e freeware? Sono su Internet. E se mi servono? Vuoi che abbia un problema ai dati e non ci sia neanche Internet?<p>

I bookmark? Ne ho a centinaia. Mai guardati. I siti che mi servono veramente li ricordo a memoria e gli altri, beh, fa prima Google. Salvo pochissime eccezioni.<p>

Vedo gente che compra dischi monumentali per memorizzare centinaia di film scaricati da Internet. Fanno bene, ma io ho smesso di farlo da un pezzo. Se rivoglio quel film, me lo riscarico. Se era su Internet, ci è rimasto, anzi, rischi di essercene in giro una versione migliore.<p>

Una Adsl flat mette a portata di mano qualunque cosa e l&rsquo;ossessione di mettere tutto sui tuoi dischi, per molte cose, non ha più ragione d&rsquo;essere.<p>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>