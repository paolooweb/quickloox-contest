---
title: "Tra parentesi: basta l'interprete"
date: 2006-06-22
draft: false
tags: ["ping"]
---

Non gli ho chiesto autorizzazione e quindi non dir&ograve; chi &egrave;, ma &egrave; un amico che cercava un programma per risolvere espressioni, di quelle da scuola media.

Ha scoperto che, a parte naturalmente il foglio elettronico, per avere un risolutore di espressioni capace di rispettare le priorit&agrave; delle operazioni e le parentesi &egrave; sufficiente il Terminale e uno dei linguaggi interpretati, come Python o Perl.

Non si tratta di tradire l&rsquo;interfaccia grafica per il malvagio Unix: ci sono solo da inserire numeri, segni e parentesi, e quello &egrave; il sistema pi&ugrave; veloce e risparmioso.

Alla fine si &egrave; optato per Python, che &egrave; il pi&ugrave; immediato (a parte scoprire che l&rsquo;elevazione a potenza per lui non &egrave; <code>^</code>, ma <code>\*\*</code>). Ed &egrave; anche venuto fuori che c&rsquo;era un errore nel libro.

Io ho provato privatamente a riprodurre l&rsquo;espressione in Lisp e ho capito perch&eacute; lo amo. Ti costringe a capire la struttura globale dell&rsquo;espressione, se non vuoi barare. Pi&ugrave; difficile e, al solito, si impara qualcosa in pi&ugrave;.

