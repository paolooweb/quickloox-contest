---
title: "Stelle a metà"
date: 2009-01-17
draft: false
tags: ["ping"]
---

Trovi che un voto da uno a cinque non basti a esprimere le preferenze musicali all'interno della libreria di iTunes?

Apri il Terminale e digita

<code>defaults write com.apple.iTunes allow-half-stars -bool TRUE</code>

Dopo avere chiuso e riaperto iTunes, potrai assegnare anche i mezzi voti.

Per tornare a com'era, stesso comando, con <code>FALSE</code> al posto di <code>TRUE</code>.