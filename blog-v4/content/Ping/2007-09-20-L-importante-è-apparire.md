---
title: "L'importante è apparire"
date: 2007-09-20
draft: false
tags: ["ping"]
---

Non è tanto il software in sé, anche se un <em>frontend</em> a <code>rsync</code> fa solo del bene. È <a href="http://arrsync.sourceforge.net/" target="_blank">l'icona</a> che mi fa impazzire.