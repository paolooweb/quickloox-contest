---
title: "Libertà di movimento"
date: 2002-12-04
draft: false
tags: ["ping"]
---

Un po’ meno Unix e un po’ più Mac, anche se ancora non ce ne siamo resi conto

Non è lontano l’arrivo di Mac OS X 10.2.3. Non ricordo più se l’uccellino mi ha detto prima di Natale o prima di Macworld Expo a San Francisco, ma i tempi non vanno più in là.

Per la prima volta l’installatore sarà in grado di aggiornare correttamente le applicazioni anche se queste si trovano in una posizione diversa da dove erano state installate originalmente.

Non è un cambiamento da poco: è la Mac way, quella che rende le cose più facili e veloci, che ritorna piano piano, anche in Mac OS X. Thanks, Apple.

<link>Lucio Bragagnolo</link>lux@mac.com