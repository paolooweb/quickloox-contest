---
title: "Il battito di Internet"
date: 2008-05-09
draft: false
tags: ["ping"]
---

È tornato l'<a href="http://yellosoft.us/index.php?id=49" target="_blank">Internet Time</a> inventato da Swatch con la collaborazione di Nicholas Negroponte del Mit e caduto nel dimenticatoio.

Non era nemmeno un'idea completamente sbagliata, se solo la percezione del tempo non fosse irrimediabilmente locale.

Un <em>menulet</em> nella barra di Mac OS X, comunque, al massimo porta via qualche pixel e permette di scambiare una battuta in più con gli amici.