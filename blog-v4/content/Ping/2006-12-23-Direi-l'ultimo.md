---
title: "Direi l'ultimo"
date: 2006-12-23
draft: false
tags: ["ping"]
---

Completato con pieno successo l'aggiornamento all'<a href="http://docs.info.apple.com/article.html?artnum=304916" target="_blank">ultimo Security Update</a>.

C'è stato il consueto riavvio desiderato.

Con altissima probabilità, non avrò altri riavvii indesiderati e dunque il conto del 2006 si ferma a sei. Uno ogni due mesi in media.

Adesso sarebbe interessante trovare un utente Windows che abbia fatto lo stesso conteggio. Ma sono tutti troppo impegnati con gli antivirus. :)