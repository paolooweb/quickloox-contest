---
title: "Questione di pixel"
date: 2002-03-18
draft: false
tags: ["ping"]
---

Pixel morto? Meglio non drammatizzare troppo

Accade, può accadere di acquistare un Mac con schermo Lcd e scoprire che uno dei maledetti pixel è morto. E di pretendere la sostituzione della macchina.
È sicuramente il caso di provarci, ma non è detto che sia sufficiente. Ci sono molte probabilità di successo se i pixel morti sono almeno tre, e ancora di più se almeno due pixel morti giacciono affiancati.
Altrimenti è meglio mettere le cose in prospettiva, perché è purtroppo statisticamente inevitabile che succeda. Per consegnare il 100% dei pixel perfetti al 100% dell’utenza, Apple dovrebbe praticamente raddoppiare il prezzo dei Mac.
In barba al fatto che dovrebbe essere perfettamente inutile, c’è chi sostiene come a volte i pixel morti possano resuscitare massaggiandoli delicatamente con un polpastrello. E se non funziona anche così, abituiamoci. Dopo un po’ ci scorderemo che è lì.

<link>Lucio Bragagnolo</link>lux@mac.com