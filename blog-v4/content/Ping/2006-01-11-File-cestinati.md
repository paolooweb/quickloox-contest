---
title: "Il Cestino più grande del mondo"
date: 2006-01-11
draft: false
tags: ["ping"]
---

Ho l&rsquo;abitudine di non vuotare mai il Cestino fino a quando non è necessario. Oggi ho vuotato il Cestino. 136.722 file. Nuovo record personale. :-)