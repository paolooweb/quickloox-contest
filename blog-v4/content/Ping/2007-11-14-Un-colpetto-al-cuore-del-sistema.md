---
title: "Un colpetto al cuore del sistema"
date: 2007-11-14
draft: false
tags: ["ping"]
---

Assai interessanti le scoperte di Dany. L'aggiornamento a iPod touch 1.1.2 alza, sembra, <a href="http://web.mac.com/brethil/Brethils_Page/Thoughts/Entries/2007/11/14_Speed_Bump.html" target="_blank">il clock del processore interno</a>. Non solo software insomma.