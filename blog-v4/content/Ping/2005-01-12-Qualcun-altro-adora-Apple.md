---
title: "Qualcun altro adora Apple<p>"
date: 2005-01-12
draft: false
tags: ["ping"]
---

Se c&rsquo;è gente soddisfatta degli annunci di Macworld Expo, ci sarà un motivo<p>

Scrive Jakaa:<p>

<em>Ho 17 anni e adoro Apple. Il perché? Sono le 22.31 di martedì 11 gennaio 2005, giornata clou del Macworld Expo, e ho appena visitato il sito ufficiale di Apple. Consiglio a tutti di farlo, sono rimasto senza parole&hellip; buon lavoro e tanti saluti.</em><p>

Ricambio di cuore e penso che tanto entusiasmo sia una cosa positiva. Vuol dire che il verso delle cose è quello giusto.<p>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>