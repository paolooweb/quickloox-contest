---
title: "Update vuole dire speranza"
date: 2006-07-13
draft: false
tags: ["ping"]
---

Me lo ha scritto <strong>Giuseppe</strong>, A proposito di errori e problemi con Bluetooth. Lui si è dato da fare ed ecco che cosa ha trovato:

<cite>Da Console:</cite>

<code>Jul 11 21:57:35 Kayoshin pppd[1264]: Failed to open /dev/cu.Bluetooth-Modem: Resource busy</code><cite>, che poi proprio <em>busy</em> non era&#8230; Direi, invece, svogliato.</cite>

<cite>Soluzione: elimino il cellulare (Nokia 6630) dai preferiti in Preferenze di Sistema>Bluetooth, poi lo spengo. Lo riaccendo e tutto magicamente funziona. Altro che tecnologia! Ci vuole un druido!</cite>

Provo a crearne uno in <a href="http://www.wow-europe.com/en/" target="_blank">World of Warcraft</a>. Vediamo se funziona. :-)