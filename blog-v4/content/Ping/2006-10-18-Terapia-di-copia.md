---
title: "Terapia di copia"
date: 2006-10-18
draft: false
tags: ["ping"]
---

Ho compiuto esperimenti non statisticamente significativi (e per necessità) sulla copia di file piuttosto grandi tramite iDisk. Un file da 460 mega ha richiesto, un upload, circa quattro ore. Sempre in upload, un altro file, da 600 mega, ha richiesto un tempo imprecisato ma superiore alle sette ore.

Entrambi i file sono stati copiati a perfezione. Ma la copia iniziata al mattino ha richiesto molto meno tempo di quella cominciata nel pomeriggio. Primo sospetto: iDisk si usa meglio prima che si svegli l'America. Naturalmente potrebbe essere semplicemente la mia Adsl che era in crisi, o un qualunque server lungo la strada.