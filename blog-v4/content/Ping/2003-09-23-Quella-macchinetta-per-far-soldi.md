---
title: "Quella macchinetta per far soldi"
date: 2003-09-23
draft: false
tags: ["ping"]
---

Apple è più diversificata di come siamo abituati a pensare

Nelle nostre teste Apple fabbrica personal computer (e il migliore sistema operativo al mondo). Peraltro, non bisogna dimenticare fatti come questo: nel suo scorso trimestre fiscale, Apple ha venduto 304 mila iPod, che hanno contribuito per il 18% dell fatturato totale.

Più degli iBook, più dei Power Macintosh G4.

In totale Apple ha venduto più di un milione di iPod nella storia del prodotto e, in termini di fatturato, tre o quattro di quelle macchinette equivalgono al prezzo di un Mac di medio valore. Sono l’equivalente di 250-300 mila computer.

Apple vende certamente personal computer (i migliori sul mercato); ma è anche e forse soprattutto un’azienda di tecnologia, per lo più innovativa.

<link>Lucio Bragagnolo</link>lux@mac.com