---
title: "It Ain't No Think Like a Free Lunch&#8230; Again"
date: 2006-07-12
draft: false
tags: ["ping"]
---

Non esistono pasti gratis. Come fa questo sedicente <a href="http://search.ebay.it/_W0QQfgtpZ1QQfrppZ25QQsassZtradeblujoker" target="_blank">negozio informatico online</a>, segnalatomi da <a href="http://www.hardclicker.com" target="_blank">Stefano</a>, a offrire un MacBook a 499 euro? Se lo fa per lavoro, deve essere ricco di nascita.