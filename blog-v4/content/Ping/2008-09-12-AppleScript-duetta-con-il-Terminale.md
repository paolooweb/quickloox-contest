---
title: "AppleScript duetta con il Terminale"
date: 2008-09-12
draft: false
tags: ["ping"]
---

Nei libri di fantascienza c'è scritto che il Terminale serve anche per creare script e non solo per dare singoli comandi.

Io faccio già fatica a occuparmi di AppleScript&#8230; gli script della shell spero me li insegni la nipotina quando sarà cresciuta.

Nel frattempo, perché non usare AppleScript per realizzare gli script e darli in pasto al Terminale? Dà soddisfazione. Lo sfrutti senza doverlo studiare. Fantastico.

Per farlo, questa volta creiamo un piccolo script che aggiunge una cartella piena di musica alla libreria di iTunes. Lo script (cortesia di <a href="http://www.macosxhints.com/article.php?story=20080902044037850" target="_blank">MacOSXHints</a>) diventerà un comando di Terminale ed è questo:

<code>on run nome_cartella
  set alias_cartella to POSIX file nome_cartella
  tell application "iTunes"
    add alias_cartella to library playlist 1
  end tell
end run</code>

Lo script non ha trucchi particolari, a parte l'uso del parametro <code>POSIX file</code>, un trucchetto per fare scrivere a Mac OS X il percorso completo di una cartella senza doversi impegnare.

Fino a qui è un normale AppleScript. Adesso, prima delle istruzioni aggiungiamo una riga magica:

<code>#!/usr/bin/osascript</code>

La riga spiega al Terminale che si tratta di uno script realizzato con qualcosa compatibile con la Open Scripting Architecture (AppleScript, ma potrebbe essere altro).

Registriamo il file <em>come testo</em>, non come script. Chi fa queste cose con Word faccia attenzione e registri come testo, non come .doc o altro materiale nocivo. E si scarichi <a href="http://www.barebones.com/products/textwrangler" target="_blank">TextWrangler</a>. Diamogli un nome tipo AggiungiLibreriaiTunes, per esempio. Anche più corto, basta che sia chiaro.

Nel Terminale, portiamoci nella cartella che contiene lo script registrato come testo. Se fosse dentro una cartella Programmazione dentro la cartella Documenti dentro la cartella Inizio, il comando sarebbe

<code>cd ~/Documents/Programmazione</code>

(<code>cd</code> sta per Change Directory, cioè cambia cartella, <code>~</code> si chiama <em>tilde</em>, si digita con Opzione-5 e nel Terminale è un'abbreviazione per la cartella Inizio)

Un altro comando da Terminale, ora:

<code>chmod a+x AggiungiLibreriaiTunes</code>

Questo rende eseguibile il file. Dal punto di vista del Terminale lo trasforma in un programma a tutti gli effetti. Pertanto potremo scrivere

<code>AggiungiLibreriaiTunes ~/Musica/Compact/Classica/JSBach</code>

(bisogna trovarsi ancora nella stessa <em>directory</em> di prima e la cartella oggetto del comando ha un percorso inventato, che va sostituito con quello vero. Se il nome di una cartella contiene spazi o altri caratteri non alfanumerici, è meglio trascinare la cartella stessa dal Finder dentro il Terminale per averlo già in forma utilizzabile)

Un gran lavoro. Però abbiamo imparato a dare comandi nuovi da Terminale, usando AppleScript per crearli. Mica male.

Per chi usa Tiger, due piccole limitazioni: lo script va registrato come AppleScript e senza la famosa prima riga. Secondo, la sintassi diventerebbe

<code>osascript AggiungiLibreriaiTunes.scpt ~/Musica/Compact/Classica/JSBach</code>

Per chiamare un normale AppleScript da Terminale (si noti che il file ha l'estensione, stavolta) si usa il comando di Terminale <code>osascript</code>.

Più di cos&#236; non riesco a spiegare. Se qualcosa non è del tutto chiaro, sono qui. :)

<em>P.S.:</em> l'originale di tutto ciò si trova su <a href="http://www.macosxhints.com/article.php?story=20080902044037850" target="_blank">MacOSXHints</a>.

<em>P.P.S.:</em> da Terminale si può fare tutto ciò in modo assai più semplice:

<code>open -a iTunes</code> <em>file</em>

Non c'è niente da imparare qui, però.