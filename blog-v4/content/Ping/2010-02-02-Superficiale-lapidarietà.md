---
title: "Superficiale lapidarietà"
date: 2010-02-02
draft: false
tags: ["ping"]
---

Come è prevedibile, si moltiplicano le reazioni alla presentazione di iPad. Paolo Attivissimo si è distinto, negativamente per quanto sono abituato a leggere da lui, per <a href="http://attivissimo.blogspot.com/2010/01/aiped-aipad-aipod.html" target="_blank">liquidare l'intera questione</a> con qualche frase fatta. Il problema è che sono frasi non tanto solide.

Quando Paolo si chiede retoricamente <cite>E cos'è questa storia della Micro-SIM? Quanti operatori cellulare la offrono?</cite> fa finta di non sapere che già oggi ci sono operatori, e cellulari, con offerte di microSim. Chiedere a qualche amico con contratto Tre, per non fare nomi.

Gliel'ho fatto <a href="http://attivissimo.blogspot.com/2010/01/aiped-aipad-aipod.html#c1121688604847449795" target="_blank">velatamente notare</a> e lui mi <a href="http://attivissimo.blogspot.com/2010/01/aiped-aipad-aipod.html#c4430648526246144924" target="_blank">ha risposto</a> <cite>Hai annunci di supporto per le micro-Sim entro marzo da parte di qualche operatore italiano o europeo?</cite>

Ovviamente non ci sono. iPad wifi arriverà, dice Apple, a fine marzo. iPad 3G arriverà in aprile. Sul sito Apple si dice a chiare lettere che gli accordi con gli operatori telefonici verranno annunciati <a href="http://www.apple.com/it/ipad/" target="_blank">in seguito</a>.

Può essere che gli accordi con gli operatori, specie quelli italiani, faranno schifo. Specie conoscendo gli operatori italiani. Può essere che siano accordi fantastici. Oggi nessuno lo sa.

Neanche Paolo, che però ha già calato la lapide su <i>questa storia</i> della microSim. Forse un po' esageratamente, no?.

Con stima immutata.