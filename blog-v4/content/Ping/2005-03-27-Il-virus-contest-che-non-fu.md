---
title: "Il virus contest che non fu<p>"
date: 2005-03-27
draft: false
tags: ["ping"]
---

Peccato non avere visto buttare venticinquemila dollari nel Cestino<p>

Grazie a Daniele per avermi tenuto aggiornato sulla cosa.<p>

È andata circa così. Symantec ha pubblicato un articolo terroristico riguardo al pericolo virus per Mac OS X, secondo l&rsquo;azienda in aumento, per via della popolarità crescente del sistema. Guarda caso, Symantes vende software e servizi antivirus.<p>

DVForge (punto com) raccoglie la sfida e annuncia un premio di venticinquemila dollari per chi riuscirà a inserire un virus, per quanto innocuo, dentro due Mac equipaggiati con Mac OS X.<p>

Infine <a href="http://www.dvforge.com/virus.shtml">DVForge</a> annuncia che il premio non verrà assegnato, né la gara verrà mai aperta.<p>

Pensare che Mac OS X sia sicuro al cento per cento è stupido. La sicurezza al cento per cento non esiste e non può esistere. E sicuramente qualcuno prima o poi riuscirebbe a prendersi i venticinquemila dollari, senza per questo dimostrare niente sulla sicurezza o meno del sistema. Che non è sicuro perché è blindato, ma perché è sufficientemente deterrente. Come è noto, non esiste l&rsquo;antifurto perfetto, ma se il dispositivo scoraggia il ladro abbastanza a lungo è probabile che il furto non avvenga.<p>

Sono osservazioni di una banalità che sconcerta, eppure vanno fatte, perché nessuno pare arrivarci.<p>

Andrà a finire che un minuscolo virusino qualsiasi dovesse nascere per Mac OS X otterrà un&rsquo;attenzione spropositata, per folklore invece che per reale pericolosità. Di là, intanto, sono nati tre nuovi virus nel tempo in cui ho scritto queste righe, ma non fanno più notizia. Peccato.<p>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>