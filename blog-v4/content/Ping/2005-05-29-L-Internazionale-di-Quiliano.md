---
title: "L&rsquo;Internazionale di Quiliano<p>"
date: 2005-05-29
draft: false
tags: ["ping"]
---

Piovono riconoscimenti per il primo museo Apple in Europa<p>

Alessio Ferraro dell&rsquo;All About Apple Club segnala la pubblicazione di una bella <a href="http://www.reuters.com/locales/c_newsArticle.jsp?type=internetNews&localeKey=it_IT&storyID=8589745">intervista</a> riguardante l&rsquo;<a href="http://www.allaboutapple.com/museo/museo.htm">All About Apple Museum</a>, pubblicata da Reuters. E anche un articolo apparso su Diario, sempre relativo al museo.<p>

Personalmente non vedo l&rsquo;ora che a Quiliano finisca per arrivare Steve Jobs a rendere al museo, nonché a tutti quelli che ci hanno lavorato dietro come matti, l&rsquo;onore meritatissimo.<p>

Nel frattempo, tanti complimenti ancora. <em>Seriously</em>.<p>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>