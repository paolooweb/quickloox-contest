---
title: "Fiocco azzurro vecchia maniera<p>"
date: 2005-04-27
draft: false
tags: ["ping"]
---

Formula giornalistica abusata, sito nuovo nuovo<p>

Nella preistoria informatica i giornalisti scrivevano stupidaggini tipo <em>fiocco rosa in casa Hp</em> per annunciare una nuova stampante. Fortunatamente oggi abbiamo perso in preparazione media, ma guadagnato qualcosa in senso del ridicolo, e nessuno si azzarderebbe più a scrivere seriamente qualcosa del genere.<p>

Tutto questo per dire che però un sito nuovo è nato davvero: <a href="http://www.mactutorials.it">mactutorials.it</a>. Da seguire. Grazie a Redeye di <a href="http://www.devilsgames.it">DevilsGames</a> per la segnalazione!<p>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>