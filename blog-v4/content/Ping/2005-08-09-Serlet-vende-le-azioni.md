---
title: "Soldi meritati<p>"
date: 2005-08-09
draft: false
tags: ["ping"]
---

Qualche protagonista dei successi di Apple passa a riscuotere<p>

L&rsquo;anno scorso Bertrand Serlet ha guadagnato 7,6 milioni di dollari rivendendo le azioni Apple che ha diritto di acquistare dalla società a prezzi di favore, come è abitudine nelle aziende americane. Quest&rsquo;annno sta ripetendo l&rsquo;operazione, a condizioni ancora più vantaggiose.<p>

Grazie al buon andamento delle azioni Apple in questo periodo, Serlet le può rivendere a quattro volte il costo, con prevedibile beneficio personale.<p>

Considerato però che Serlet è il responsabile dello sviluppo software di Apple e che è lui ad avere programmato la crescita e lo sviluppo di Mac OS X, nonché tutti i suoi legami con l&rsquo;open source, e che se Apple sta navigando con il vento in poppa è pure merito suo, insomma, ben venga.<p>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>