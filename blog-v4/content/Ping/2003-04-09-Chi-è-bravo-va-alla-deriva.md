---
title: "Chi è bravo va alla deriva"
date: 2003-04-09
draft: false
tags: ["ping"]
---

La chiave per capire iLife, il Terminale e la vita su Mac

Avvertenza: trattasi di riflessione filosofica.
Mi sono reso conto che uso il software Apple (iLife e il resto) quando sono incompetente in materia. È un complimento per Apple; vuol dire che davvero sono programmi <i>for the rest of us</i>, prima di ogni altra considerazione facili da usare.

Quando sono competente in materia, uso il migliore programma possibile. Chi deve misurarsi con il testo e talvolta con l’Html, e non usa <link>Tex-Edit Plus</link>http://www.tex-edit.com oppure <link>BBEdit</link>http://www.barebones.com, ama inutilmente la vita difficile.

Se fossi un genio del computer, quelli che scrivono programmi Java alla stessa velocità con sui io digito i Ping, andrei alla deriva (in senso positivo) verso Unix e verso il software open source.

Infine, se riuscissi a staccarmi dalla vita digitale terrena e diventare un monaco Zen del Macintosh, avrei una scrivania completamente vuota, tranne tre o quattro finestre di shell.

Shell, non Terminale. Che è una shell, ma ha un nome più umano per gli illetterati come me che a malapena sanno scriverci dentro uptime.

<link>Lucio Bragagnolo</link>lux@mac.com