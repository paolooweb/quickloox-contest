---
title: "Un vero simbolo"
date: 2009-09-01
draft: false
tags: ["ping"]
---

Pochi lo sanno, ma il primo nome di dominio che sia mai apparso su Internet è symbolics.com.

Lo aveva usato Symbolics, azienda che ai tempi vendeva Lisp machine, computer costruiti apposta per funzionare in Lisp, dedicati a laboratori di ricerca, università e studiosi di intelligenza artificiale.

Assegnato il 15 marzo 1985, dopo quasi un quarto di secolo è stato venduto dal proprietario originale alla società di investimenti Xf.com per una somma non resa nota.

Da un punto di vista Mac non frega niente a nessuno, ma quando si sposta una pietra miliare è d'obbligo almeno accorgersene.

Apple è stata piuttosto sollecita a registrare il dominio apple.com (19 febbraio 1987). Microsoft è arrivata un po' più tardi, il 2 maggio 1991.

Per capire quanto valgano i quattro anni abbondanti di divario, si pensi che il 21 marzo 1990 ci era arrivata perfino Disney, a registrare disney.com. Il web neanche esisteva. Per capire l'importanza di un dominio ci voleva per forza l'intelligenza, non quella artificiale.