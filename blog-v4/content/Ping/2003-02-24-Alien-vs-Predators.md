---
title: "Alien vs. Predators"
date: 2003-02-24
draft: false
tags: ["ping"]
---

Attenzione, perché quando arriva il deserto c’è chi ha un altro pianeta

Imperano i predatori. Entrano nelle mailing list, nei forum, nei siti. Una volta potevano scrivere solo sui giornali e restavano sotto controllo, ora nessuno li può fermare.

Praticano l’ignoranza come credo, l’abuso di tutte le risorse possibili, l’arroganza di domandare continuamente e di pretendere senza mai dare. Si svegliano e pensano “voglio questa cosa, la voglio subito, la voglio gratis”.

Dove la gente studia per anni i predatori esigono un programma semplice, immediato, senza pagare, naturalmente potentissimo. Mettono le mani nel software di sistema con la stessa grazia di un bimbo di due anni alle prese con i giocattoli infrangibili e poi, ovvio, se qualcosa non va la colpa è di Apple.

Se un problema si risolve cercando una singola parola su Google, loro preferiscono scriverne duecento su una mailing list, omettendo con cura qualunque indizio serio che possa aiutare chi cerca di aiutarli.

Io ho smesso di aiutarli. Tra tutti questi predatori mi sento un alien: brutto, cattivo e feroce, ma al tempo stesso capace di sopravvivere in ambienti ostili e anche nel vuoto.

Attenzione, predatori: io non conto niente e mi potete ignorare. Ma ricordate che se l’ambiente viene depredato senza rispettarlo arriva il deserto. Tutti gli alien sopravviveranno su qualche altro pianeta e voi rimarrete da soli alle prese con la vostra arroganza di chi pensa che la Netiquette sia una specielità francese e il give back un parco-zoo per canguri.

<link>Lucio Bragagnolo</link>lux@mac.com