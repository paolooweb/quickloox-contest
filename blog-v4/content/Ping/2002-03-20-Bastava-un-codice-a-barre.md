---
title: "Bastava un codice a barre"
date: 2002-03-20
draft: false
tags: ["ping"]
---

L’Ufficio Complicazioni Affari Semplici colpisce ancora. In treno

Era tanto che non prendevo un treno. Il biglietto era una specie di mazzetta infarcita di carta carbone. Uno dei tanti lenzuoletti era da obliterare (il significato me lo ha spiegato un controllore-interprete) ma aveva la freccia “obliterami per favore” su tutti e due i lati. E uno dei due lati (ma lo abbiamo scoperto solo dopo) era sbagliato.
Il controllore ha rampognato duramente me e il mio collega, il tutto su un treno che da Genova Brignole a Genova Piazza Principe aveva accumulato venti minuti di ritardo e non certo per colpa delle nostre obliterazioni.
Intanto pensavo che sarebbe bastato un bigleittino con sopra un codice a barre e, in mano al controllore, un lettorino capace di leggere il codice e fare beep. Magari attaccato a un iPod per memorizzare tutti i dati e intanto un po’ di musica rilassante.

<link>Lucio Bragagnolo</link>lux@mac.com