---
title: "Serie&#8230; e facete"
date: 2009-10-29
draft: false
tags: ["ping"]
---

Il <a href="http://www.macworld.it/blogs/ping/?p=3008&amp;cp=1#comment-37540">commento di Stefano</a> sulla crescita della <a href="http://www.macworld.it/blogs/ping/?p=3008">lunghezza dei numeri di serie dei prodotti Apple</a> è sacrosanto.

Il presupposto è che non viviamo in un'epoca nella quale i numeri di serie sono ragionevoli. Praticamente tutti i codici alfanumerici che ci toccano non sono concepiti come un tutto, ma sono sigle-Frankenstein che cuciono insieme tanti frammenti di codifica ognuno riferito a un sottoinsieme specifico.

Scrivevo su Macworld cartaceo di giugno, credo, che vorrei conoscere quello che ha inventato l'Iban. I nuovi demenziali codici bancari sono costruiti per accogliere oltre quattordicimila miliardi di numeri di conti correnti per ciascuno di noi. S&#236;, per ciascuno.

Il codice fiscale italiano identifica oltre due miliardi di miliardi di italiani, cifra che non verrà mai raggiunta neanche se l'Italia durasse quanto durerà l'universo. Un codice fiscale serio potrebbe essere di sette o otto cifre senza problemi, o magari dieci. L'imperfezione sta nella costruzione: tre caratteri indicano solo e per forza il cognome, cinque la data di nascita e cos&#236; via. Il numero di serie Apple evidentemente è fatto con gli stessi presupposti, quelli che mettono una cifra astronomicamente lunga in fondo ai moduli di conto corrente postale e in mille altre situazioni.

Sempre nello stesso articolo, proponevo un esperimento: scrivere dentro un file di testo nome, cognome, indirizzo, telefono, cellulare, iban, tessera sanitaria, assicurazione auto, partita Iva eccetera eccetera, tipo:

<code>anna bianchi castelvolturno 14 febbraio 1981 IT05K1402907940000000233896 081-234566 338-9847382&#8230;</code>

Salvare il file con un nome qualunque. Poi aprire il Terminale e scrivere <code>md5</code>, più uno spazio, più trascinare nel Terminale l'icona del file in questione.

Il risultato sarà un codice di trentadue cifre, sufficiente a chiunque da qui all'eternità e in grado di contenere qualsiasi coordinata in modo molto più sicuro e univoco di qualsiasi altro sistema.

È solo un esempio. Un lavoro serio potrebbe portare praticamente al dimezzamento, se non oltre, di qualunque codice alfanumerico, senza pregiudicarne l'efficienza. Ma evidentemente quelli che lavorano ai codici e ai numeri di serie odiano l'umanità. O hanno voglia di prenderci in giro.

Se non bastasse, si pensi ai numeri di telefono. Con quindici o sedici cifre, neanche alfanumeriche, si raggiunge qualunque utenza nel mondo.