---
title: "Servizio d&rsquo;ordine<p>"
date: 2005-06-07
draft: false
tags: ["ping"]
---

Tidy diventa anche un servizio<p>

Mi segnala Mario che è uscito <a href="http://www.pixelfreak.net/tidy_service/">Tidy Service</a>, un piccolo ma brillante servizio di Mac OS X (quelli presenti nel menu Applicazione) che applica istantaneamente i servizi di Tidy a qualunque codice Html abbiamo selezionato.<p>

Non spiego che cosa sia sia Tidy perché è inutile. La prova sta nel codice disastrosamente dilettantistico di milioni di pagine Web sparse per il mondo. C&rsquo;è modo di avere stile anche quando si fa Web authoring.<p>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>