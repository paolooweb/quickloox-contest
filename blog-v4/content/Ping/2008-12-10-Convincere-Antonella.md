---
title: "Convincere Antonella"
date: 2008-12-10
draft: false
tags: ["ping"]
---

È tutta colpa mia. Ho scritto un articolo su Macworld, lei lo ha letto e ha <a href="http://ferris60.wordpress.com/" target="_blank">iniziato un blog</a>.

Poi però mi ha scritto, la mail è finita in un angolo sbagliato del mio database e le ho risposto con un ritardo indicibile.

Adesso il suo blog non è più aggiornato. Non perché le abbia risposto in ritardo, certamente Antonella avrà avuto ragioni ben più valide.

Il fatto è che, a parte gli inevitabili difetti di gioventù, c'era la stoffa e valeva la pena che proseguisse. Auspico che prosegua, se possibile. :-)