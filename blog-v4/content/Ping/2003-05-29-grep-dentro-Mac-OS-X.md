---
title: "Impara grep e mettilo da parte"
date: 2003-05-29
draft: false
tags: ["ping"]
---

Un disco con Mac OS X è una miniera inesauribile di sorprese

Vuoi imparare a usare grep?

<file:///Library/Documentation/Commands/grep/grep_toc.html>

Bello pronto da leggere dentro Mac OS X. Non escludo che per ritrovarselo occorra avere installato i Developer Tools, ma è lo stesso una bella cosa.

Per inciso, grep è uno strumento vagamente esoterico e per niente facile da usare ma molto molto utile, di cui basta leggere una mezz’oretta per cominciare a farci cose utili. Appunto.

Certo, se per te scrivere un file testo significa aprire Word, allora grep non ti serve. Ma non c’è speranza su tutta una serie di altre cose più importanti.

<link>Lucio Bragagnolo</link>lux@mac.com