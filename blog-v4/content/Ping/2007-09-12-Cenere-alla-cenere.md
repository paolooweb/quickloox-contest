---
title: "Cenere alla cenere"
date: 2007-09-12
draft: false
tags: ["ping"]
---

<cite>Ashes to ashes, funk to funky
We know major tom's a junkie&#8230;</cite>

Lo so che è difficile da credere. Ma esiste una lotta di potere intorno a <a href="http://www.macserialjunkie.com/" target="_blank">MacSerialJunkie</a>, con congiure sotterranee, alleanze, fazioni e litigi. Tutto per fare andare avanti un sito di password.

Il bello è che sembrano pure adulti.

È un segno chiaro del successo del Mac. Ne arrivano veramente di ogni varietà. :-)

Ma non sarà neanche mai più come prima.

<cite>Strung out in heavens high
<a href="http://www.lyricsfreak.com/d/david+bowie/ashes+to+ashes_20036971.html" target="_blank">Hitting an all-time low&#8230;</a></cite>