---
title: "Bundle monomarca"
date: 2009-12-06
draft: false
tags: ["ping"]
---

Le superofferte di software provenienti da una azienda sono rarucce. Ambrosia Software <a href="http://www.ambrosiasw.com/promos/xmas09/" target="_blank">ne ha due</a>, una per lavoro e una per gioco, ciascuna declinata in tre varianti a prezzi diversi e con sconti non da poco.

Se i programmi attirano, c'è tempo fino a Natale. :)