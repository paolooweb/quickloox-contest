---
title: "It’s being a hard day’s night"
date: 2003-01-10
draft: false
tags: ["ping"]
---

E, per parafrasare i Beatles, sto lavorando come un cane

Ma mi trovo mollemente adagiato sul divano di casa, con il PowerBook in grembo (che fa anche un po’ di tiepido, sempre apprezzato nella stagione). La connessione Internet è gentilmente fornita dalla base AirPort nascosta in un angolino del salotto.

Per non disturbare nessuno ho attaccato un paio di cuffie e, con iTunes, mi sono creato una playlist giusta per rilassare un po’ ma anche dare ritmo alla (sedicente) creatività.

Essendo piena notte – la consegna è urgente – la casa è buia, ma ho attaccato a una porta Usb il mio regalo di Natale preferito: la Flylight di Kensington, che prende energia dalla porta Usb e proietta sulla tastiera l’illuminazione giusta per vedere i tasti senza tenere luci accese.

La batteria del Titanium non è longeva quanto quella di un iBook, ma anche con la Flylight e iTunes posso contare su due ore buone di autonomia. Se proprio serve, collegherò l’alimentatore più tardi. Per ora mi godo la libertà da qualsiasi cavo.

Il digital lifestyle è una gran cosa e Steve Jobs ha ragione su tutto. Ora, se solo potessi giocare un po’ invece che dover finire quel dannato articolo...

<link>Lucio Bragagnolo</link>lux@mac.com