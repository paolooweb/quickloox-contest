---
title: "C'è chi non si smentisce mai"
date: 2006-08-09
draft: false
tags: ["ping"]
---

Notizia di attualità: Apple rende disponibile il <a href="http://lists.apple.com/archives/Darwin-dev/2006/Aug/msg00067.html" target="_blank">codice del kernel di Darwin per x86</a>.

Altra notizia di attualità. Apple crea <a href="http://www.macosforge.org/" target="_blank">Mac OS Forge</a>, sito dedicato a progetti open source e in particolare ai progetti profughi da OpenDarwin, di cui è stata annunciata la chiusura.

(mentre scrivo è anche arrivato in chat, graditissimo, <strong>Federico</strong> a dirmi <cite>come volevasi dimostrare</cite>)

Per qualcuno è di cattivo gusto prendersela con i siti che raccontano bugie.

Chissà com'è, un sito disinformatico aveva parlato di progressiva riduzione del codice open source messo a disposizione da Apple. Aveva parlato di eliminazione del sorgente di Darwin per x86.

Se quel sito ha pubblicato le notizie di cui sopra, dando conto del proprio errore e smentendo le sparate fatte, ho torto marcio ed è di cattivo gusto prendersela con lui.

Non le ha pubblicate. Certa gente non si smentisce mai.