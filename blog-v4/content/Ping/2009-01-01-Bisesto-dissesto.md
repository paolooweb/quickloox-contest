---
title: "Dissesto bisesto"
date: 2009-01-01
draft: false
tags: ["ping"]
---

Microsoft non si cura che i propri prodotti <a href="http://www.macworld.it/blogs/ping/?p=2115" target="_blank">funzionino a dovere con i secondi bisestili</a> e tutto sommato si vive lo stesso (anche se non vorrei dipendere strettamente da un Gps prima della risincronizzazione).

Adesso però ho scoperto del <a href="http://tech.yahoo.com/news/afp/20090101/tc_afp/usmusicinternetzunemicrosoft" target="_blank">suicidio di massa</a> degli Zune 30 gigabyte del 2006.

Quella versione del lettore Mp3 di Microsoft dò problemi l'ultimo giorno di un anno bisestile. Cos&#236;, a mezzanotte del 30 dicembre, tutti gli Zune 30 giga del 2006 accesi si sono bloccati. Fantastico.

Dopo un giorno di attesa è arrivata <a href="http://www.zune.net/en-us/support/zune30.htm" target="_blank">la risposta di Microsoft</a>. C'è effettivamente un problema e la <em>soluzione</em> consiste nello scaricare la batteria, ricaricarla e riaccendere l'aggeggio. Siccome è già arrivato il primo gennaio, alla prima risincronizzazione si sistemerà tutto. Fino al 30 dicembre 2012, quando succederà di nuovo.

Quegli Zune sono vecchi di oltre due anni, ma non di cinque o di sette. Tutto questo dice molto sull'idea di supporto che ha Microsoft. In più, pare che per effettuare un azzeramento dell'apparecchio ci sia da lasciare scaricare la batteria, che come procedura è stata ideata dai progettisti delle caverne. Su iPod si schiacciano due tasti.

Se tutto questo fosse successo su un apparecchio Apple sarebbe scoppiata una mezza rivoluzione.

E francamente mi viene il dubbio su che cosa Microsoft stia infilando dentro Office, se tanto mi dà tanto. Il controllo qualità della prima azienda di software al mondo sembra, come minimo, dissestato.