---
title: "Consigli per la stampa"
date: 2011-02-19
draft: false
tags: ["ping"]
---

Tale Paul J. Miller se ne va da Engadget, acquistato da Aol, e scrive la <a href="http://pauljmiller.com/?p=5" target="_blank">lettera d'addio</a>, comprendente il seguente paragrafo:

<cite>Mi piacerebbe proseguire questo lavoro per sempre, ma sfortunatamente Engadget è proprietà di Aol e Aol si è dimostrata</cite> partner <cite>poco disponibile nell'evoluzione di questo sito. Non ci vuole un veterano dell'editoria per capire che Aol ha il cuore nel posto sbagliato rispetto ai contenuti. [&#8230;] Aol vede il contenuto come pretesto per vedere pubblicità. Potrebbe rendere (sebbene ne dubiti), ma non promuove buon giornalismo né buon intrattenimento.</cite>

Il buon giornalismo e la buona informazione sono risultati di un connubio equilibrato e di qualità tra contenuti e informazione.

Leggi quello che ti pare. Ma se è solo un pretesto per vendere pubblicità, realizzato alla svelta e male, sappi che vale poco. E per loro vali poco anche tu.