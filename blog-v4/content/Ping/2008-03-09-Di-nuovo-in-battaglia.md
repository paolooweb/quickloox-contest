---
title: "Di nuovo in battaglia"
date: 2008-03-09
draft: false
tags: ["ping"]
---

Dopo oltre un anno dall'ultima versione principale (1.2), è pronto <a href="http://www.wesnoth.org" target="_blank">Battle For Wesnoth 1.4</a>.

Sono arrivate un sacco di cose in più. Ci sono campagne nuove, un sacco di nuove unità, nuovi livelli di crescita per le unità vecchie, traduzione ancora più accurata, animazioni più utili e interessanti di prima, nuova grafica e quant'altro.

È uno dei giochi <em>open source</em> più belli che ci siano, da giocare da soli, in famiglia, in rete, come si vuole, con chi si vuole, senza vincoli di piattaforma. Lo si può anche giocare un minuto per volta, una mossa per volta, per una serata intera o in dieci secondi che avanzano. Non averlo su disco è perdere qualcosa.

Su Macworld maggio Il Cd conterrà la versione Lite (quella essenziale per giocare). Chi invece ha banda e disco per i 155 mega della versione completa, faccia clic.