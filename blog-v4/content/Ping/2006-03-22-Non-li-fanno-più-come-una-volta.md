---
title: "Non li fanno più come una volta"
date: 2006-03-22
draft: false
tags: ["ping"]
---

Credo che Riccardo lo conoscerà già sicuramente, ma per me <a href="http://www.system7today.com/" target="_blank">system7today</a> è stata una sorpresa. Non avrebbe dovuto. Con un po’ di ingegno è possibile usare con profitto tuttora Mac anche molto vecchi. E non farò confronti; sarebbero ingenerosi.