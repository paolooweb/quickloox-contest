---
title: "W il tasto T"
date: 2004-08-04
draft: false
tags: ["ping"]
---

Uno degli avvii meno usati può divenire uno dei più utili

Molti sanno collegare due Mac con un cavo FireWire e riavviarne uno tenendo premuto il tasto T. Il Mac riavviato in questo modo diventa a tutti gli effetti un disco rigido in dotazione all’altro Mac.

In pochi però sanno che, se il Mac riavviato ha altre unità disco interessanti, anch’esse diventano disponibili. Per esempio, un eventuale masterizzatore o SuperDrive diventa utilizzabile dal Mac remoto.

Viene in mente il reimpiego intelligente dei computer più vecchi. Per esempio, un potente e veloce PowerBook 12” 1,33 GHz potrebbe approfittare di un PowerMac G4 con poca Ram e processore scarsamente dotato... però munito, per dire, di due dischi rigidi belli grandi, un SuperDrive e forse ancora qualcos’altro.

Un bel modo di sfruttare ancora meglio il tasto T.

<link>Lucio Bragagnolo</link>lux@mac.com