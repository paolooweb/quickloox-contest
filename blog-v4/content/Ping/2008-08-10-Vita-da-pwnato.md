---
title: "Vita da pwnato"
date: 2008-08-10
draft: false
tags: ["ping"]
---

Usato con pieno successo <a href="http://blog.iphone-dev.org/" target="_blank">PwnageTool 2.0.2</a> per sbloccare il firmware 2.0.1 di iPhone Edge.

Tutti i dati veri, compresi i programmi presi su App Store, ritornano senza problemi. Però le rete <em>wireless</em> vanno reimpostate, i programmi installati via Cydia vanno reinstallati e cos&#236; via. Vita da cladestini.

C'è anche una beta di Installer 4.0, ma senza programmi, per ora.