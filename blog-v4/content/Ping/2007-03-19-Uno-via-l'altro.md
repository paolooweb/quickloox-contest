---
title: "Uno via l'altro"
date: 2007-03-19
draft: false
tags: ["ping"]
---

Anche il vecchio iMac Lcd 15&#8221; di backup è passato indenne dagli ultimi aggiornamenti. Siccome viene acceso saltuariamente si è fatto in un colpo solo gli ultimi cinque o sei. Nessun problema.

Posso aggiungere l'aggiornamento software di iPod effettuato sul portatile della fidanzata. Sono un uomo fortunato. Per la fidanzata&#8230; negli aggiornamenti non è questione di fortuna.