---
title: "Argini e margini<p>"
date: 2005-09-03
draft: false
tags: ["ping"]
---

Considerazioni tristi e ciniche su gente triste e cinica<p>

Brutto doverlo constatare ancora e vorrei non constatarlo più, ma le catastrofi moderne, siano terroristiche o naturali, hanno una costante: si può stare sicuri che il sito Apple mostrerà in prima pagina un link di solidarietà con almeno un giorno di anticipo rispetto al sito Microsoft.<p>

Arrivare in ritardo per Microsoft è una missione, ma se si tratta di programmi insicuri e di cattiva qualità ne risente &ldquo;solo&rdquo; il progresso tecnologico generale.<p>

Quando ci sono in gioco vite umane, constatare quali siano le loro priorità mette &ldquo;solo&rdquo; una gran tristezza.<p>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>