---
title: "Sciocco chi clicca"
date: 2003-04-15
draft: false
tags: ["ping"]
---

Peggio dei venditori di mattoni all’autogrill

Non se se siano ancora in auge, ma una volta nei piazzali degli autogrill potevi incontrare il venditore di mattoni. Ti proponeva videoregistratori, impianti stereo, televisori e altra mercanzia di elettronica di consumo a prezzo da favola.

Se qualcuno, assetato di sconti, Più Furbo Degli Altri, ci cascava, il venditore e i suoi complici organizzavano un ingegnoso scambio tale che alla fine il malcapitato pagava un prezzo sì da favola, per uno scatolone identico a quello che lui aveva visto, con tutti i marchi a posto, ma con dentro un mattone o variazioni sul tema.

Chiaro che solo uno sprovveduto può pensare di poter pagare un prodotto molto meno del suo prezzo attraverso un canalo non ufficiale senza che sotto non ci sia qualcosa di illegale o di disonesto.

Ultimamente è salito alla ribalta applediscount.com, un sito gestito da chissà chi che promette Macintosh a prezzo molto più basso del listino; ma bisogna pagare in denaro liquido tramite Western Union a un indirizzo chissà quale, se ricordo bene, di una repubblica baltica.

Ho già visto segnalare La Grande Occasione su diverse mailing list. Ora aspetto solo il primo turlupinato che pensava di essere più furbo degli altri e ha pagato chissà chi con chissà che cervello.

<link>Lucio Bragagnolo</link>lux@mac.com