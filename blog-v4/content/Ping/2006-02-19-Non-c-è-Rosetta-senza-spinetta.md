---
title: "Non c&rsquo;&egrave; Rosetta senza spinetta"
date: 2006-02-19
draft: false
tags: ["ping"]
---

Pi&ugrave; che dei worm sono preoccupato dai siti che girano come gli organetti di una volta e scrivono per riflesso condizionato, con le frasi fatte.

Quando vedi una frase tipo <em>Pi&ugrave; compatibilit&agrave; con Rosetta</em>, vuol dire che la compatibilit&agrave; era zero ed &egrave; salita a cento. Un programma non pu&ograve; essere pi&ugrave; compatibile con Rosetta, o meno; o funziona, o non funziona.