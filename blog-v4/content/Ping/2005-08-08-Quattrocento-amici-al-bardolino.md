---
title: "Quattro(cento) amici al bar(dolino)<p>"
date: 2005-08-08
draft: false
tags: ["ping"]
---

Mac-raduno da non perdere in località incantevole<p>

Il 17 e 18 settembre si terrà <a href="http://www.macbardolino.com/">MacBar</a>, manifestazione Mac patrocinata dal Comune di Bardolino, cittadina oltremodo graziosa situata in zona Garda.<p>

Parteciperanno numerosi esponenti del mondo-Mac-che-conta, tra cui per esempio le nostre vecchie (ma buonissime) conoscenze di <a href="http://www.devilsgames.it">Devilsgames</a>.<p>

Mi impegno a ricordarlo ancora, ma chi ha iCal a portata di mano segni sull&rsquo;agenda. È una bella occasione di respirare Mac e divertirsi pure!<p>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>