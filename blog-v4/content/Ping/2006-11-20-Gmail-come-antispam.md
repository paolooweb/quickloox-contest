---
title: "Gmail come antispam"
date: 2006-11-20
draft: false
tags: ["ping"]
---

Me lo ha segnalato <strong>Mario</strong>. In effetti l’idea di <a href="http://johnzeratsky.com/archives/000715.php" target="_blank">inoltrare tutta la propria posta verso Gmail</a> e da lì farsela inoltrare nuovamente verso la casella ordinaria, debitamente filtrata dai meccanismi antispam di Gmail, è ingegnosa.

Un bell’esercizio di stile, che si inserisce bene nell’eterno dilemma tra avere l’antispam sul server (non puoi controllare il tasso di falsi positivi) e l’averlo sul client (scarichi comunque tutto lo spam).