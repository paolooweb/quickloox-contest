---
title: "Il bello addormentato"
date: 2006-10-23
draft: false
tags: ["ping"]
---

Sono arrivato alla presa, ho collegato il Mac, l'ho aperto e lui, già in stop, <em>non</em> si è svegliato.

Ho provato a schiacciare un tasto e niente. Ho provato a chiudere e riaprire il computer ma niente. La spia apposita indicava chiaramente lo stato di stop.

Infine ho provato, a Mac chiuso, a togliere e rimettere il cavo di alimentazione. Ho riaperto il portatile e lui si è risvegliato allegramente.

Non sono assolutamente riuscito a riprodurre la cosa e dunque non la trovo preoccupante. Curiosa però, s&#236;.