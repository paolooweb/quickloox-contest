---
title: "Norton vs. Disk Warrior"
date: 2003-08-04
draft: false
tags: ["ping"]
---

Se proprio non puoi farne a meno, un consiglio, anzi, quattro

Ci sono quelli che devono occuparsi maniacalmente del computer perché non hanno veri impegni di lavoro o di divertimento. Questa specie si distingue per l’ossessivo bisogno di passare le Norton Utilities su dischi che non hanno problemi, deframmentare, ottimizzare, ripulire e altro ancora.

Consiglio uno. Imparare dagli americani che dicono “If ain’t broke, don’t fix it”. Se non è rotto, non aggiustarlo.

Consiglio due. Deframmentare non serve. È inutile. Molto più produttivo, se il disco è praticamente pieno, liberare un po’ di spazio.

Consiglio tre. Se c’è una probabilità di migliorare qualcosa dopo una passata di utility, appartiene a Disk Warrior. Disk Warrior può migliorare le prestazioni di un disco molto maltrattato. Norton no.

Consiglio quattro. Usa Norton solo quando si è persa ogni altra speranza. Conosco molta più gente che si è fatta rovinare un disco sano dalle Norton di quanta se ne sia fatto salvare uno malato.

<link>Lucio Bragagnolo</link>lux@mac.com