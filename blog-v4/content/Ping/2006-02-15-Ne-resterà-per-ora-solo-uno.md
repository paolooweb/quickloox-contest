---
title: "Ne rester&agrave; (per ora) solo uno"
date: 2006-02-15
draft: false
tags: ["ping"]
---

Installato Mac OS X 10.4.5, e Backup 3.1 gi&agrave; che c&rsquo;ero, nessun problema.

L&rsquo;aggiornamento mi ha richiesto di riavviare il Mac e ci&ograve; mi ha riportato al vecchio proposito di inizio anno. Di riavvii non richiesti, provocati da bug e non da aggiornamenti di sistema, finora solo uno. E siamo a quarantasei giorni, circa il 13 percento dell&rsquo;anno.