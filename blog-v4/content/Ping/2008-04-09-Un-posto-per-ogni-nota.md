---
title: "Un posto per ogni nota"
date: 2008-04-09
draft: false
tags: ["ping"]
---

Nessun sistema operativo contiene di serie un programma che contenga, organizzi e ritrovi facilmente tutte quelle informazioni che non trovano spazio nei programmi canonici e non si ha il tempo di organizzare a dovere. Non subito.

Secondo, solo adesso (ripeto, solo adesso) la sincronizzazione delle informazioni tra più computer e online <em>inizia</em> a funzionare.

Io tifo <a href="http://www.barebones.com/products/yojimbo/" target="_blank">Yojimbo</a>, ma ci sono anche <a href="http://evernote.com/" target="_blank">Evernote</a>, <a href="http://www.eastgate.com/Tinderbox/" target="_blank">Tinderbox</a>, <a href="http://www.devon-technologies.com/" target="_blank">DevonThink</a>, <a href="http://reinventedsoftware.com/together/" target="_blank">Together</a>, <a href="http://flyingmeat.com/voodoopad/" target="_blank">VoodooPad</a>, <a href="http://www.chronosnet.com/Products/sohonotes.html" target="_blank">Soho Notes</a>, <a href="http://www.circusponies.com/" target="_blank">Circus Ponies Notebook</a> e non è ancora finita (come p noto, per Mac non ci sono programmi). Fate il vostro gioco, sapendo che in questo caso la spesa si ripaga nel giro di un quarto d'ora. Il tempo perso a cercare le cose è senza prezzo.