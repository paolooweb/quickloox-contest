---
title: "Tutto è politica"
date: 2010-10-13
draft: false
tags: ["ping"]
---

Dopo il parlamentare tedesco e il Guardasigilli italiano, adesso il Parlamento danese <a href="http://politiken.dk/politik/ECE1078079/alle-folketingsmedlemmer-har-faaet-en-iphone-foraeret/" target="_blank">ha dotato tutti gli onorevoli</a> e gran parte dello staff di complessivi mille iPhone 3GS da sedici gigabyte.

A questo punto mancano solo le Apple Tv!