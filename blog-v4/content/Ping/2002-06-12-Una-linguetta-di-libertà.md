---
title: "Una linguetta per la libertà"
date: 2002-06-12
draft: false
tags: ["ping"]
---

All’assalto del 90% coatto del mondo informatico

In genere si dà per scontato che la gente scelga di comprarsi un Mac piuttosto che un computer Wintel. Non è assolutamente vero; la stragrande maggioranza degli acquirenti di un Pc non è neanche minimamente al corrente delle differenze di piattaforma, o è pesantemente disinformata, o addirittura non sa che esiste Mac.
Per questo motivo la nuova linguetta che compare nell’interfaccia del sito Apple è incoraggiante: vuol dire che finalmente si sta andando con decisione all’assalto di tutta quella gente che non sa di avere l’alternativa.
L’obiettivo alla fin fine è sempre vendere; ma è anche dare a tante persone la libertà di scegliere. Importante anche se sono “solo” computer.

<link>Lucio Bragagnolo</link>lux@mac.com