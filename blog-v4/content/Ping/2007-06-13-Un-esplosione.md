---
title: "Un'esplosione"
date: 2007-06-13
draft: false
tags: ["ping"]
---

Oggi è esploso <a href="http://www.apple.com/safari" target="_blank">Safari 3</a>, ma una volta sola, quando gli ho chiesto di scaricare un Pdf. Ci deve essere qualche incompatibilità brutta con il plugin di Adobe, perché il tentativo di aprire il file mandava comunque in crash Safari. Non ho provato con altri Pdf.

Per un quarto d'ora dopo il crash, Google Groups non ha voluto saperne di funzionare. Poi, nessun problema.

Avevo pensato di abusare dei Nightly Builds, però se crasha sui Pdf e basta non ce n'è bisogno e posso aspettare un aggiornamento più semplice.