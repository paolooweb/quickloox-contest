---
title: "Il test del semplicione<p>"
date: 2005-05-24
draft: false
tags: ["ping"]
---

Come riconoscere i media di contenuto da quelli di chiacchiere<p>

Consulta un sito, una rivista, un quotidiano, quello che vuoi.<p>

Se anche solo solleva il dubbio che Apple possa sostituire i processori di Macintosh attuali con processori Intel, i casi sono due: o non ha capito che cosa ha letto, o non sa che cosa sta scrivendo.<p>

In ambedue i casi si può apporre il timbro <em>semplicione</em> e passare a fonti di informazione più affidabili.<p>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>