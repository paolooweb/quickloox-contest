---
title: "Lavorare informati"
date: 2010-07-05
draft: false
tags: ["ping"]
---

Scrive (e pubblico) <b>paolomac</b>:

<cite>Una piccola segnalazione, su un articolo di sabato scorso, pagina 23, di un giornale di solito (per merito di Antonio Dini) molto equilibrato sulle faccende Apple.</cite>

<cite>Titolo:</cite> Gli italiani scettici sull'iPad<cite>.</cite>

<cite>Svolgimento: il giornale ha commissionato un sondaggio a Ipr marketing dal quale è emerso</cite> un certo scetticismo verso nuovi strumenti, come l'iPad della Apple<cite>.</cite>

<cite>E donde viene tale consapevolezza? Perché, mentre la maggioranza lo conosce, il 72 percento si dichiara</cite> non interessata all'acquisto<cite>: a parte il fatto che ciò vuol dire che il 27 percento del campione è interessato, il che non pare poco, è interessante vedere i motivi del</cite> non interesse<cite>. Per il 50% del campione, ciò dipende da prezzo troppo alto: secondo l'aritmetica, il 77% del campione</cite> è interessato <cite>all'oggettino, anche se per il 50% il prezzo è ancora troppo alto: in che non mi sembra esattamente</cite> scetticismo<cite>.</cite>

<cite>In conclusione, mi sembra che il sondaggio concluda che solo il 21% del campione definisce l'iPad</cite> di scarsa utilità<cite>.</cite>

<cite>Ben venga un simile scetticismo!</cite>

<cite>Ti segnalo questo, perché mi pare un'efficace sintesi di come un contenuto possa essere del tutto capovolto da un titolo sbagliato, fuorviante e &#8211; in un certo senso &#8211; denigratorio; piuttosto divertente&#8230;</cite>

<cite>P.S.: finalmente ieri passo all'apple store di Roma est, gioco un po' con l'iPad, e manifesto un palese</cite> non scetticismo<cite>&#8230;

Cose da ricordare.

I sondaggi vengono normalmente commissionati dai giornali non per capire come vanno le cose, ma per mostrare che le cose vanno in un certo modo, predefinito; il sondaggista piloterà il sondaggio in modo da ottenere i risultati più concilianti possibile. Normalmente il sondaggio viene commissionato da qualcosa di esterno al giornale, che può essere l'editore, il partito di riferimento, una software house molto grande e influente eccetera.

Secondo, i giornali non sono minimamente interessati a come stanno le cose, ma a vendere più copie. Che si debbano raccontare stupidaggini per vendere più copie viene visto al più come inconveniente del mestiere.

Terzo: quando un giornale pubblica un articolo con un titolo che contraddice l'articolo, punta sul fatto che la maggior parte dei lettori guarda il titolo e passa via.