---
title: "Eravamo quattro amici in pizzeria"
date: 2007-11-25
draft: false
tags: ["ping"]
---

Grande succcesso per la pizzata-Ping numero due, con un aumento nel numero dei partecipanti di oltre il 33 percento.

Si è chiacchierato amabilmente di tutto lo scibile umano e ho potuto distribuire non solo i libriccini che avevo anticipato, ma anche un paio di tomi di iPod e iTunes, una copia delle Internet Yellow Pages 2006 (non ancora cos&#236; vecchie) e una copia della Guida completa a Leopard che ho scritto a quattro mani con Akko.

Grazie di cuore agli intervenuti, a quanti avrebbero voluto venire ma non ce l'hanno fatta e a quanti non avrebbero voluto venire e non sono venuti. :-)