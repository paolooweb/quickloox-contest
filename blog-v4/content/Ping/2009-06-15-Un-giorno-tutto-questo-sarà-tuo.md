---
title: "Un giorno tutto questo sarà tuo"
date: 2009-06-15
draft: false
tags: ["ping"]
---

Stavo pensando che non si sentono più in giro quelli che dicevano <i>per mio figlio compro un computer Windows, cos&#236; sarà abituato per quando lavorerà</i>.

Questi due scatti arrivano dalla metropolitana di Milano, da manifesti dell'<a href="http://www.accademialascala.org/" target="_blank">Accademia della Scala</a> e dell'<a href="http://ied.it/" target="_blank">Istituto Europeo di Design</a>.

