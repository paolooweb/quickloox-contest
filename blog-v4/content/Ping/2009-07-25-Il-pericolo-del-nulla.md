---
title: "Il pericolo del nulla"
date: 2009-07-25
draft: false
tags: ["ping"]
---

<cite>E l'unico pericolo che sento veramente / è quello di non riuscire più a sentire niente.</cite> (Lorenzo Jovanotti)

In questi giorni stai leggendo sul web che Apple ha ottenuto eccellenti <a href="http://images.apple.com/pr/pdf/q309data_sum.pdf" target="_blank">risultati finanziari per il trimestre aprile-giugno 2009</a> &#8211; 8,34 miliardi di fatturato, record per un trimestre non prenatalizio &#8211; e che il merito di questo risultato, oltre che alla vendita anch'essa record di 2,6 milioni di Mac, spetta alla vendita di 5,2 milioni di iPhone.

<i>È totalmente falso</i>.

Per via di certa legislazione americana, Apple spalma l'incasso delle vendite di ogni iPhone su ventiquattro mesi. Sempre per la stessa ragione, a causa dell'arrivo di iPhone OS 3.0, nel fatturato del trimestre figurano solo gli iPhone venduti tra il 18 giugno e il 30 giugno, per una cifra che &#8211; appunto &#8211; va divisa per ventiquattro.

Qualunque stima si possa fare vengono fuori briciole, molto meno dell'uno percento degli 8,34 miliardi di fatturato. La vendita eccezionale di iPhone avrà effetti sui prossimi trimestri, ma non su questo.

Quello che angoscia di tutto ciò non è tanto che sul web pulluli gente ignorante e incapace che si picca di dare le notizie sul Mac. È che chi guarda quelle news ci crede, le beve, si fida. Fuori da <a href="http://twitter.com/macjournals" target="_blank">macjournals</a> su Twitter, non ho ancora trovato nessuno che abbia scritto la verità sulla faccenda. E nessuno che abbia alzato la mano per dire <i>guardate che non sapete di che cosa parlate</i>.

A nessuno più interessa sapere come stanno veramente le cose. Va bene tutto, non conta più niente. Domani quei siti pubblicheranno altre falsità e tutti le leggeranno come se niente fosse.

La trovo una cosa tremenda. Ma mica per iPhone. Prendi le persone, una per una, e per esempio ti diranno di avere una idea politica precisa. Perché <i>si informano</i>. Il dubbio che possano avere letto o sentito notizie false neanche li sfiora.

Gente che vive nel nulla.