---
title: "Il bacio della donna lagno<p>"
date: 2005-12-21
draft: false
tags: ["ping"]
---

Quello tra Internet e giornalisti di Repubblica continua a essere un rapporto difficile<p>

Mille grazie al grande <a href="http://www.accomazzi.it">Luca Accomazzi</a> per la segnalazione di un articolo su Repubblica online che, aspettando il 2006 imminente, ci fa tutti dieci anni più giovani.<p>

Nel senso che un coacervo di banalità, luoghi comuni e inesattezze come questo ricorda certe cose del 1995, quando Internet era roba per pochi e sui mezzi di informazione si leggeva proprio di tutto.<p>

L&rsquo;ironia è che il coacervo comprende anche una sonora dose di omissioni: stranamente, non vengono ricordate le numerose bufale in cui è caduta Repubblica, alcune arrivate financo sulla carta. A memoria ricordo, tra le altre, una paginata intera dedicata a un supposto (inesistente) <a href="http://www.nettime.org/Lists-Archives/nettime-l-9610/msg00077.html">catalogo per pedofili</a> venduto via posta e, in tempi più recenti, un intervento assai critico sull&rsquo;hobby di sparare palle di vernice su ragazze nude in fuga per il deserto di Las Vegas, praticato unicamente nella fantasia di qualche buontempone e ridicolmente <a href="http://www.repubblica.it/online/esteri/lasvegas/lasvegas/lasvegas.html">preso per autentico</a> da una cronista sprovveduta.<p>

Un articolo veramente inutile, e almeno fosse frizzante: è una <a href="http://www.repubblica.it/supplementi/af/2005/12/19/multimedia/013letruffe.html">lagnosa paternale</a> a firma Laura Kiss. Sperando non sia uno pseudonimo.<p>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>