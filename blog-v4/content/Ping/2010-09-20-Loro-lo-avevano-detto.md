---
title: "Loro lo avevano detto"
date: 2010-09-20
draft: false
tags: ["ping"]
---

Il 9 gennaio avevo dato conto di <a href="http://www.macworld.it/ping/blob/2010/01/09/sottotitoli-nokia/" target="_blank">una intervista a un pezzo grosso di Nokia</a>.

Riporto solamente la dichiarazione <cite>Nel 2011 i nostri sforzi inizieranno a produrre risultati, perché giocheremo alla pari con Apple e Rim sugli</cite> smartphone<cite>.</cite>

Peccato che Olli-Pekka Kallasvuo, l'amministratore delegato che ha progettato la strategia di Nokia, <a href="http://www.allaboutsymbian.com/news/item/12096_Stephen_Elop_replaces_Olli-Pek.php" target="_blank">sia stato messo alla porta</a> giusto ieri. Non vedrà i risultati per i quali ha tanto lavorato.

Li vedrà invece Stephen Elop, il nuovo amministratore delegato, che proviene da Microsoft. Non esattamente, a oggi, una azienda vincente in campo cellulare.

In una cosa Kallasvuo aveva ragione: a ottobre 2007 si defin&#236; <a href="http://www.pcworld.com/article/138631/nokia_ceo_paranoid_about_iphone_competition.html" target="_blank">paranoico</a> rispetto all'arrivo di iPhone.

Purtroppo si è dimenticato di chiudere il mercato a doppia mandata prima di andare a dormire.