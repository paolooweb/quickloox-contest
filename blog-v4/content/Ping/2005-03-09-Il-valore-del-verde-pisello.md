---
title: "Il piacere della scatola<p>"
date: 2005-03-09
draft: false
tags: ["ping"]
---

Anche il verde pisello ha un valore aggiunto, se riveste un iPod shuffle<p>

Oggi ho visto aprire un iPod shuffle. Vabbeh, Apple conta di venderne cinque milioni quest&rsquo;anno, dov&rsquo;è la notizia? Che a farlo era una persona scettica, di quelle che alla fine conta quanto ti serve, e se risparmi cinque euro sono cinque euro guadagnati, e non bisogna farsi abbindolare dai lustrini eccetera eccetera.<p>

Questa persona, tutta d&rsquo;un pezzo, apriva la scatolina verde pisello dello shuffle con lentezza non abituale. Di fronte alla pellicola che sigilla ermeticamente l&rsquo;oggetto ha avuto uno sbandamento, ma si è ripreso. Solo che poi ha visto i copriauricolari sigillati ermeticamente coppia per coppia, la guida rapida all&rsquo;uso sul cartoncino plastificato, e gli è scappato.<p>

<em>Certo che con queste cose è un piacere anche aprire la scatola</em>, gli è sfuggito a mezza bocca.<p>

È un po&rsquo; come se Michael Dell avesse borbottato in conferenza stampa che non è poi così male, questo Macintosh.<p>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>