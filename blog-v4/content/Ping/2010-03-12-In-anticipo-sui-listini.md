---
title: "In anticipo sui listini"
date: 2010-03-12
draft: false
tags: ["ping"]
---

Capisco la volontà e l'ordine di allineare per quanto possibile la comunicazione tra sito Apple americano e siti internazionali.

Però fa un po' ridere leggere che in Italia iPad avrà <a href="http://www.apple.com/it/ipad/" target="_blank">un prezzo incredibile</a> quando il prezzo si saprò tra tre settimane.