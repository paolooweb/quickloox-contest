---
title: "Un passato trasparente<p>"
date: 2005-10-06
draft: false
tags: ["ping"]
---

Ci sono cose vecchie che attendono solo di divenire nuove<p>

A Quiliano, provincia di Savona, sorge l&rsquo;All About Apple Museum: il museo Apple più grande e organizzato del mondo.<p>

Umberto Valsasina e signora hanno donato al museo un Macintosh Portable trasparente, molto probabilmente l&rsquo;unico esistente in Italia e uno di pochissimi in Europa.<p>

Da dove arriva? Come può esistere? Ai più anziani (informaticamente) il cognome Valsasina ricorderà una certa Iret Informatica e tutta una serie di altre nozioni.<p>

Non è tanto questo il punto, quanto il fatto che il successo del museo Apple può dipendere anche da noi. Non è necessario che sia un Mac trasparente; ma, piuttosto (o prima) che pensare alla discarica, pensiamo a Quiliano e mandiamo una email o facciamo una telefonata.<p>

Abbiamo tutti qualcosa da dare per rendere più ricco l&rsquo;<a href="http://www.allaboutapple.com/museo/museo.htm">All About Apple Museum</a>. Patrimonio che tornerà indietro con gli interessi, in cultura, storia, tradizione, informazione.<p>

Beh. Facciamolo.<p>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>