---
title: "Prima alla savonese"
date: 2010-04-19
draft: false
tags: ["ping"]
---

Gli amici dell'All About Apple Museum presentano iPad in Italia sabato 24 pomeriggio, a partire dalle 15.

Il programma completo è presente <a href="http://www.allaboutapple.com/" target="_blank">sul loro sito</a>.

Non perdono mai un colpo. :-)