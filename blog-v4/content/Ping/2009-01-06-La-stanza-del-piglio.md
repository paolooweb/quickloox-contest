---
title: "La stanza del piglio"
date: 2009-01-06
draft: false
tags: ["ping"]
---

Fuori dalla finestra <a href="http://www.wunderground.com/cgi-bin/findweather/getForecast?query=zmw:00000.1.16080&amp;hourly=1&amp;yday=5&amp;weekday=Tuesday" target="_blank">cade neve in abbondanza</a> e lo spettacolo mi ha fatto superare la consueta pigrizia.

Ho aperto tutte le danze: in iChat mi trovo nella stanza <code>macworldexpo09</code> (sono anche in <code>pocroom</code>, la stanza storica del <a href="http://www.poc.it" target="_blank">Poc</a>). In entrambi i casi faccio da buttadentro, se necessario fornendo istruzioni.

L'appuntamento vero è infatti sul server <code>irc.freenode.net</code>, canale <code>#freesmug</code>. Serve un programma di <em>chat</em> Irc come <a href="http://xchataqua.sourceforge.net/twiki/bin/view/Main/WebHome" target="_blank">X-Aqua Chat</a>, <a href="http://colloquy.info/" target="_blank">Colloquy</a> o <a href="http://homepage.mac.com/philrobin/conversation/" target="_blank">Conversation</a>. Ce ne sono <a href="http://www.macupdate.com/search.php?keywords=irc&amp;starget=google" target="_blank">molti altri</a> per ogni gusto.

Mi trovo già dentro il canale Irc e tutto insomma è pronto. Nei giorni scorsi è apparsa complessivamente una decina di visitatori che effettuavano le prove tecniche, quindi arrivarci ci si arriva.

Chi ha voglia di farsi una chiacchierata sui temi del <em>keynote</em>, beh, le danze si aprono alle nove californiane, quindi le 18 italiane. Si può arrivare prima o dopo, a qualunque ora. Io resto fino a che c'è gente.

Al Moscone Center di San Francisco <a href="http://www.accuweather.com/us/ca/san-francisco/94101/city-weather-forecast.asp?partner=accuweather&amp;traveler=0" target="_blank">non nevicherà</a>; sono previsti otto gradi e cielo parzialmente nuvoloso, quasi certamente senza pioggia, solo tanta umidità.