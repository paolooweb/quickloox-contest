---
title: "Pro e contro il mouse al laser<p>"
date: 2005-07-29
draft: false
tags: ["ping"]
---

Arrivano i primi commenti sull&rsquo;MX1000 Logitech<p>

Questi mouse a tecnologia laser sono una rivoluzione veramente oppure no? Iniziano ad arrivare i primi pareri. Siccome li trovo sensati, lascio più che volentieri la parola a Luca e a  Antonio.<p>

(Luca)<p>

<cite>PUNTAMENTO</cite><p>

<cite>L'ho acquistato nel dicembre scorso, appena uscito in Italia. La differenza sulla qualità di puntamento non è abissale come vogliono farci credere quelli della Logitech, ma c'è, e si sente eccome. A volte cazzeggio in iTunes a due metri dallo schermo col mouse in mano, e il laser non fa una piega, sia che appoggio il mouse sulle gambe, sulle mani, sulle braccia, sulla sedia, sulla barba&hellip; il laser è destinato a rivoluzionare tutto. In futuro sarebbe addirittura possibile avere mousepad non più piani (!) se tutti i mouse fossero come questo. Si potrebbero fare in peluche - anzi ora deposito il brevetto di questa illuminazione splendidamente inutile così diverrò ricco appena la Logitech conquisterà il mondo.</cite><p>

<cite>ERGONOMIA</cite><p>

<cite>Ergonomia perfetta nell'impugnatura (se non ti azzardi ad essere mancino, naturalmente), ma perde dei punti appena devi cliccare: i tasti sono infatti un tutt'uno con la superficie superiore, e se per caso ti trovi con la mano appoggiata non perfettamente (ovvero sempre) la pressione da esercitare per ogni clic è tre/quattro volte superiore a quella dei mouse in cui il tasto si stacca dal resto.</cite><p>

<cite>Rotella classica, fa tic tic per intenderci. Con questo prezzo avrei voluto una rotella silenziosa, fluida e senza scatti, come quelle della Microsoft.</cite><p>

<cite>TASTI PROGRAMMABILI</cite><p>

<cite>I tasti programmabili sono esattamente dove dovrebbero essere. Ad esempio io lancio il browser, apro nuovi pannelli e navigo fra i diversi pannelli aperti senza staccare le mani dal mouse, senza toccare la tastiera. Poi in Anteprima se ho un pdf di milleottocento pagine me lo sfoglio tutto coi tasti programmabili. Eccetera. Lavoro col video, quindi in passato mi è tornato utile anche l'acquisto dello ShuttlePro. Ci sono dei giorni che faccio ore di lavoro senza toccare la tastiera, destra sul mouse e sinistra sullo Shuttle. Il contrario di te <em>[me. N.d.lux]</em>, che usi la tastiera per più cose possibili :)</cite><p>

(Antonio)<p>

<cite>Ebbene, come ha detto <em>[io. N.d.lux]</em>, questo mouse ha reso tutti gli altri "non più trendy" ma mi permetto di dissentire sulla sua effettiva bontà&hellip; Certo, è ottimo, ergonomico e veloce, un'altro pianeta rispetto alla supposta monotasto made in Apple, ma avendo un amico in possesso del MOLTO più economico MX 700 (o 800, non ricordo bene&hellip; uguale ma con tecnologia ottica) posso solo dire che non sia poi tutta questa novità! Anzi, se avessi resistito alla "febbre da novità" avrei risparmiato un buon gruzzoletto per avere un qualcosa di praticamente UGUALE!</cite><p>

<cite>Mi rode più questo che il resto, in quanto come ho già detto il prodotto è ottimo&hellip; ma un tantino <em>overpriced</em> rispetto a quello che effetivamente fa!</cite><p>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>