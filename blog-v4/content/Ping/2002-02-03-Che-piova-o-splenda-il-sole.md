---
title: "Che piova o splenda il sole"
date: 2002-02-03
draft: false
tags: ["ping"]
---

C’è chi crede che QuickTime esista solo per Mac. In realtà è l’architettura mediale più diffusa nell’universo informatico

Scrivo di domenica, sdraiato in poltrona, con un po’ di musica dopo un pranzetto e una pennichella.
Intanto che scribacchio e mi godo il pomeriggio, freddo ma soleggiato, anche oggi il sito Apple sta erogando le oltre trecentomila copie di QuickTime che vengono scaricate ogni giorno, stando a Jobs e compagnia.
Se fossero solo utenti Mac, entro un mesetto si sarebbe terminata la distribuzione. Evidentemente c’è anche qualcun altro che apprezza QuickTime. Al ritmo di un milione ogni tre giorni.
Lo slogan potrebbe essere: mica è solo uno stupido player.

lux@mac.com