---
title: "Troppo avanti"
date: 2007-02-01
draft: false
tags: ["ping"]
---

Mentre scrivo, la home SourceForge di <a href="http://scribus.sourceforge.net/" target="_blank">Scribus</a> risulta aggiornata al 10 novembre 2007. Ma è un bene: fino a che rimane la pagina proiettata nove mesi nel futuro, si può finalmente scaricare una versione che si installa senza Fink!

Finalmente un impaginatore su mille comincerà a porsi qualche domanda sul software open source. Per ora è abbastanza.