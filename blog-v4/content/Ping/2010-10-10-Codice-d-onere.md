---
title: "Codice d'onere"
date: 2010-10-10
draft: false
tags: ["ping"]
---

Con il crescere dell'opportunità di avere strumenti efficaci per gestire testo codificato (da Html in avanti), <a href="http://barebones.com/products/bbedit/" target="_blank">BBEdit</a> sta acquistando sempre più spazio nella mia produzione di materiale su Mac. So che molti usano con profitto <a href="http://macromates.com/" target="_blank">TextMate</a>, ma mi trovo talmente bene con BBEdit che non ho alcuna motivazione a considerare un cambiamento e devo ancora individuare lacune del programma rispetto alle mie esigenze, inevitabilmente soddisfatte.

Né Bare Bones né Macromates però mostrano la minima intenzione di pubblicare una versione per iOS dei loro editor e cos&#236; ho passato una mezza serata a cercare di capire quale fosse la app che si avvicina di più a fornirmi gli stessi servizi su iPad.

Principalmente cerco funzioni di manipolazione del testo di una minima potenza, semplificazione di inserimento e di gestione dei <i>tag</i>, possibilità di lavorare con file residenti altrove ed evidenziazione della sintassi in Html, Css e via dicendo.

Cos&#236; ho effettuato qualche ricerca in App Store, leggendo con attenzione le descrizioni, guardando le schermate, aprendo i siti degli autori, facendo attenzione ai dettagli (per esempio ho escluso subito quelli che vendevano una <i>app</i> promettente ma non avevano uno straccio di pagina web a sostegno).

Alla fine, dopo un difficile ballottaggio tra <a href="http://itunes.apple.com/it/app/id372231108?mt=8#" target="_blank">Editor for iPad</a> e <a href="http://itunes.apple.com/us/app/for-i/id363493710?mt=8#" target="_blank">for i:</a>, ho scelto il primo, per la maggiore chiarezza nel mostrare l'interfaccia utente.

Il non poter collaudare preventivamente una <i>app</i> di App Store è un onere che pesa sulle scelte. D'altra parte ho speso 2,99 euro; a Milano è l'equivalente di caffè, brioche e succo di frutta. Se editor svolgesse con successo una frazione minima di lavoro, si sarebbe abbondantemente ripagato.

Se anche funzionasse diversamente da come desidero, non sarà un dramma anche acquistare for i: oppure un eventuale nuovo programma. for i: costa 7,99 euro. Avrei speso un totale di 10,98 euro. TextMate costa 48,75 euro e BBEdit costa 125 dollari (da zero), trenta dollari (aggiornamento) o 49 dollari (<i>educational</i>). Non c'è confronto tra prestazioni, sicuramente; neanche tra prezzi, però.