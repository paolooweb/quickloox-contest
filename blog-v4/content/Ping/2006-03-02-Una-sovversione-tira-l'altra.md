---
title: "Una sovversione tira l&rsquo;altra"
date: 2006-03-02
draft: false
tags: ["ping"]
---

Ho gi&agrave; raccontato di come mi &egrave; toccato scoprire il sistema di <em>versioning</em> <a href="http://subversion.tigris.org/" target="_blank">subversion</a>.

Oggi ho sbagliato ad applicare un comando e (giustamente, come ho scoperto dopo) mi si &egrave; aperto <strong>vim</strong> nel Terminale. Terrore e angosciata chiusura del Terminale come reazione immediata; a bocce ferme, timidamente come quando si saggia con l&rsquo;alluce l&rsquo;acqua del torrente di montagna ho riaperto il Terminale, digitato <code>man vim</code> e passato cinque minuti a imparare qualcosa di nuovo. Come minimo, a uscire da vim in modo civile.

Non ci si crede, ma leggere con calma cinque minuti di help rivela che il Terminale non &egrave; poi questa brutta bestia. &Egrave; solo questione di ignoranza, mia.