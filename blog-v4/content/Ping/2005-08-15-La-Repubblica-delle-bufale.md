---
title: "La Repubblica delle bufale<p>"
date: 2005-08-15
draft: false
tags: ["ping"]
---

Non se ne può più di gente che scrive senza leggere<p>

Non si è ancora spenta l&rsquo;eco del penoso pezzo sulle presunte novità di Windows Vista, che Repubblica.it ne fa un&rsquo;altra delle sue.<p>

Apple si è vista rifiutare provvisoriamente un <a href="http://appft1.uspto.gov/netacgi/nph-Parser?Sect1=PTO2&Sect2=HITOFF&p=1&u=%2Fnetahtml%2FPTO%2Fsearch-adv.html&r=23&f=G&l=50&d=PG01&S1=Robbin.IN.&OS=IN/Robbin&RS=IN/Robbin">brevetto sull&rsquo;utilizzo di input tramite interfaccia con disco rotante</a>. Microsoft, dopo un rifiuto provvisorio iniziale, ha ricevuto l&rsquo;approvazione di un <a href="http://appft1.uspto.gov/netacgi/nph-Parser?Sect1=PTO1&Sect2=HITOFF&d=PG01&p=1&u=/netahtml/PTO/srchnum.html&r=1&f=G&l=50&s1=20030221541.PGNR.&OS=DN/20030221541&RS=DN/20030221541">brevetto sulla creazione automatica di playlist</a>.<p>

Qualunque persona dotata di buon senso capisce che sono due cose diverse, ma a Repubblica non ci arrivano. E titolano di <a href="http://www.repubblica.it/2005/h/sezioni/scienza_e_tecnologia/brevipod/brevipod/brevipod.html">battaglia per il brevetto dell&rsquo;iPod</a>, arrivando a ipotizzare che Apple potrebbe pagare royalty a Microsoft su ogni iPod venduto.<p>

A tutti gli effetti, gli iPod sono arrivati sul mercato nell&rsquo;ottobre 2001 e Microsoft ha presentato il suo brevetto nel maggio 2002. Quindi, relativamente a iPod, non conta niente perché esiste la cosiddetta prior art, ovvero un&rsquo;applicazione concreta del concetto precedente alla presentazione del brevetto stesso.<p>

Se anche esistesse una battaglia per il brevetto di iPod, è finita prima ancora di cominciare. Purtroppo ho visto già lo scatenarsi del passaparola da parte di sprovveduti che leggono e danno ulteriore seguito a queste sciocchezze.<p>

Quando Repubblica.it scrive di informatica, è da lasciar perdere. Come minimo, è inesatta. Scrivono senza leggere. Basterebbe leggerli, i brevetti.<p>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>