---
title: "Non vedo ragioni per non farlo"
date: 2009-11-07
draft: false
tags: ["ping"]
---

Il nuovo <i>bundle</i> software di <a href="http://www.macheist.com" target="_blank">MacHeist</a> regala sei applicazioni e nient'altro. Niente soldi, niente sconti, niente offerte, solo sei applicazioni in regalo. Ho cliccato.