---
title: "Distrazioni estive / 2"
date: 2007-08-12
draft: false
tags: ["ping"]
---

Imperdibile articolo di Freeman Dyson sulla <a href="http://www.edge.org/documents/archive/edge219.html#dysonf" target="_blank">necessità degli eretici nella scienza</a> e, per molta parte del pezzo, sul riscaldamento globale.