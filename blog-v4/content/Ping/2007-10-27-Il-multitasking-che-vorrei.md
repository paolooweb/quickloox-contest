---
title: "Il multitasking che vorrei"
date: 2007-10-27
draft: false
tags: ["ping"]
---

Sono appena arrivato a casa e alla connessione, e sento il bisogno di salutare e ringraziare tutti quanti hanno partecipato alla festa-Leopard, che poi era festa-Poc, che poi era festa-Mac@Work (ora @Work Group), che poi era anche un po' festa-Ping e che è stata festa veramente, o almeno cos&#236; io l'ho sentita.

Sono i momenti in cui invidio fortemente Mac OS X per quella sua capacità di dare l'illusione di di fare procedere centinaia di attività contemporaneamente. Sono invece umano e, se parlo con Carlo, non riesco a parlare con Carmelo e viceversa, cos&#236; come intrattenendomi con Antonio mi tocca trascurare Anita e cos&#236; via.

Vorrei scusarmi con tutte le persone per le quali ho avuto poco tempo, ringraziare quanti me ne hanno concesso, salutare tutti quelli che sono intervenuti, sperare che arrivi presto Lynx o quello che sarà. Mica perché Leopard non valga (tutt'altro); è che come pretesto per ritrovare giovani e vecchi amici funziona molto bene. :-)