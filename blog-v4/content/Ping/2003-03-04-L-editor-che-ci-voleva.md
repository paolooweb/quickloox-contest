---
title: "L’editor che ci voleva"
date: 2003-03-04
draft: false
tags: ["ping"]
---

Tex-Edit Plus è il miglior compromesso possibile tra un text editor e un word processor. Invece BBEdit è sempre stato il miglior text editor in assoluto.

Il problema è che BBEdit è diventato di una potenza pazzesca. Più che un text editor, è un programma di authoring Web, un ausilio per il programmatore, un magazzino di worksheet per avere i vantaggi del Terminale senza sudare sul Terminale, e mille altre cose.

Bare Bones, la società che produce BBEdit, se ne è resa conto e ha fatto, come sempre, una cosa assai intelligente. Ha spogliato BBEdit delle funzioni da Html, da programmazione, da Unix, fino a lasciare un eccellente text editor puro.

La cosa assai intelligente, un text editor puro, si chiama TextWrangler e ha un prezzo assolutamente accessibile. Così ora ci sono <link>Tex-Edit Plus</link>http://www.tex-edit.com, che è ultraversatile, <link>BBEdit</link>http://www.barebones.com/products/bbedit/index.shtml, che è ultrapotente, e <link>TextWrangler</link>http://www.barebones.com/products/textwrangler/index.shtml, che è semplice, completo e accessibile. L’editor che ci voleva.

E poi c’è chi dice che su Mac mancano i programmi.

<link>Lucio Bragagnolo</link>lux@mac.com