---
title: "Entra in iTunes se il disco non esce"
date: 2002-07-16
draft: false
tags: ["ping"]
---

Un piccolo trucco per risolvere certi problemi con dischi protetti da copia

A volte i Cd o Dvd protetti da copia possono creare problemi di intepretazione al sistema operativo, tali che magari il disco è a posto ma resta incastrato nel lettore. Prima di ricorrere alla ventennale clip aperta e infilata nell’apposito forellino per forzare l’uscita, vale la pena di compiere un ultimo tentativo solo software: usare il tasto Espelli (Eject) di iTunes o Dvd Player. In molti casi il disco uscirà senza ulteriori problemi.

Dopo di che, evitare come la peste i dischi e le case produttrici che ritengono lecito privarci del diritto di copia a legittimo uso personale e oltretutto non sono neanche capaci di farlo come si deve.

<link>Lucio Bragagnolo</link>lux@mac.com