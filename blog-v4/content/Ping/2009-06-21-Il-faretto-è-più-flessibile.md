---
title: "Il faretto è più flessibile"
date: 2009-06-21
draft: false
tags: ["ping"]
---

Mentre scrivo, Apple non ha ancora tradotto in italiano la pagina che riassume le novità di Snow Leopard. Ne riferisco un pezzettino al giorno.

<b>Regolazione opzioni vista</b>

Le opzioni di vista di Spotlight si regolano esattamente come quelle di qualsiasi altra finestra del Finder. Si possono modificare la vista predefinita cos&#236; come le dimensioni, le etichette e la disposizione delle icone.

Sono le rifiniture che fanno comodo a tante persone che hanno trovato Leopard troppo sbrigativo su certe funzioni. Spotlight diventa un faretto più efficiente.