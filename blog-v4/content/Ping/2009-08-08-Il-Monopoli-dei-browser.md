---
title: "Il Monopoli dei browser"
date: 2009-08-08
draft: false
tags: ["ping"]
---

I cinque <i>browser</i> più diffusi girano il 29 percento più veloci su Windows Xp rispetto a Vista.

Poco male, si dirà: arriva Windows 7.

C'è un progresso infatti. I cinque <i>browser</i> più diffusi girano il 13 percento <a href="http://www.betanews.com/article/Windows-XP-SP3-runs-browsers-13-faster-than-Windows-7-RTM/1249687071" target="_blank">più veloci su Windows Xp rispetto a Windows 7</a>. Su Xp, Chrome ha la potenza relativa di <i>diciannove copie</i> di Explorer 7 in funzione su Vista.

Ci sarà da divertirsi. Com'era la <a href="http://it.answers.yahoo.com/question/index?qid=20081002101754AAM2iQ4" target="_blank">carta Imprevisti su Monopoli</a>? <cite>Fate tre passi indietro (con tanti auguri!)</cite>.