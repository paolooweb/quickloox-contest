---
title: "Un bel tacer non fu mai script"
date: 2004-03-16
draft: false
tags: ["ping"]
---

Quelli che non vedono un palmo oltre il loro naso e così non arrivano fino allo schermo

Una volta fare consulenza su Mac era individuare problemi (e soluzioni) che spesso erano affascinanti, pur nella loro fastidiosità.

Oggi nove interventi su dieci consistono nello spiegare alla vittima che non è una vittima, ma solo una persona che, per qualunque ragione, ha deciso di non guardare quello che ha a disposizione nel sistema.

Oggi mi è stata posta una domanda accompagnata da quella mezza isteria tipo le ho provate tutte ma proprio non riesco, perché cavolo non si fa software più facile, Apple dovrebbe pensare alle nostre esigenze eccetera.

In risposta ho aperto il menu Script del suo Finder (quello grafico con l’icona a forma di pergamena), e dentro una cartella dal nome inequivocabile gli ho mostrato la soluzione al suo impossibile problema.

Oltre che costare denaro, ti costerà imbarazzo: perfino nell’alto dei cieli, i primi a essere aiutati sono quelli che si aiutano.

<link>Lucio Bragagnolo</link>lux@mac.com