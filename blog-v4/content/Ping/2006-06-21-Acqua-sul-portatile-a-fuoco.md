---
title: "Acqua sul portatile a fuoco"
date: 2006-06-21
draft: false
tags: ["ping"]
---

L&rsquo;informazione Mac italiana sembra ignorare la notizia del <a href="http://www.theinquirer.net/?article=32550" target="_blank">portatile che ha preso fuoco</a> in Giappone. Forse perch&eacute; &egrave; un Dell. Fosse stato un Mac, sai che concerto?

Grazie a <strong>Nero</strong> per la segnalazione!