---
title: "Tutti pazzi fino alla pizza"
date: 2010-06-10
draft: false
tags: ["ping"]
---

Ho imparato un sacco dalla presentazione del mio libriccino <a href="http://www.tilibri.com/libri/tutti_pazzi_per_ipad.html" target="_blank">Tutti pazzi per iPad</a> e ho ricevuto davvero tanto in calore e simpatia da quanti hanno partecipato. Un grazie e un abbraccio collettivo, per la presentazione e per la pizza che ho condiviso di cuore con gli irriducibili.

Ho pensato di condividere i contenuti e cos&#236; esiste la <a href="http://public.iwork.com/document/?a=p38442574&amp;d=Tutti_pazzi_per_iPad.key" target="_blank">presentazione liberamente accessibile su iWork.com</a>, più <a href="http://files.me.com/lux/od3a0j.mov" target="_blank">l'audio registrato</a> in modo del tutto artigianale con iPhone in forma di memo vocale. L'audio è in forma grezza, niente tagli e niente elaborazioni. Il link all'audio scadrà tra un anno.

La presentazione senza sentire l'audio dice pochino, mentre ascoltando assume un qualche significato e auspicabilmente risulta più utile.

Se qualcuno trova vagamente utile o piacevole il tutto, me lo faccia sapere. È una soddisfazione. :)