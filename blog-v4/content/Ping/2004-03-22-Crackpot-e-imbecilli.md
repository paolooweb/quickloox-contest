---
title: "I crackpot e la gente perbene"
date: 2004-03-22
draft: false
tags: ["ping"]
---

Mitomani e imbecilli sono sempre esistiti, la Rete no

Gli americani chiamano crackpot gli scienziati pazzi sostenitori della piattezza della Terra, gli scopritori delle Grandi Congiure Mondiali, quelli che non-siamo-mai-andati-sulla-Luna e tutti quelli che in generale sfidano le convinzioni comuni con entusiasmo lodevole ma poche evidenze scientifiche.

Ebbene, un imbecille scrive su un forum che il PowerBook 12” si deforma a cusa del calore, affermando che “lo dice una fonte autorevole” (ma non dice quali). Una persona perbene mi scrive preoccupatissima perché, effettivamente, il suo PowerBook ha un piedino che non tocca la scrivania al contrario degli altri tre!

Io cerco di calmarla, ma la persona perbene è inquieta.

Il giorno dopo la persona perbene scopre che, posto su un’altra scrivania, il PowerBook poggia perfettamente e quindi molto probabilmente è uno dei due tavoli a essere curvo. Nel frattempo il crackpot è scomparso nella melma da cui era emerso.

Morale: i mitomani e i complessati, che hanno bisogno di inventarsi storie per rendere più interessante la loro vituzza, esistono da sempre e adesso usano Internet come un megafono universale. Le persone perbene non dovrebbero credere a tutto quello che leggono su Internet, ma almeno chiedere un secondo parere a qualcuno che le fonti autorevoli le consulti, e possa spiegare che una notizia del genere non solo è assurda ma neanche compare da qualsivoglia parte.

<link>Lucio Bragagnolo</link>lux@mac.com