---
title: "I dubbi&#8230; contano"
date: 2008-08-20
draft: false
tags: ["ping"]
---

Anche in questo scorcio d'estate riesco a trovare uno che sostiene la scarsità di software per Mac OS X.

Cerco i progetti <em>open source</em> su <a href="http://sourceforge.net" target="_blank">Sourceforge</a> e vedo 5.434 risultati, con limitazione della ricerca ai soli software che soddisfano la condizione <em>Operating System</em> uguale a <em>OS X</em>.

Questo è solo l'<em>open source</em>. Vero che non tutto gira comodamente su Mac. Però nel conteggio non sono inclusi i software <em>open</em> che girano su Mac ma fuori da Aqua (X11, Terminale). Quelli che girano su Mac anche se non lo dicono (molti programmi Java, per fare un solo esempio). E poi tutti i <em>freeware</em> che non sono <em>open source</em>. E poi tutti gli <em>shareware</em>. E poi tutti i commerciali&#8230;