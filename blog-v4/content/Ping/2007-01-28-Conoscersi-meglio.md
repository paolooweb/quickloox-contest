---
title: "Conoscersi meglio"
date: 2007-01-28
draft: false
tags: ["ping"]
---

Mi ritrovo nella chatroom del Poc (Comando-Maiuscole-G in iChat, serve Aim, nome pocroom) e inizio a chiacchierare del più e del meno con un perfetto sconosciuto.

Dopo un po' salta fuori che è un autore di plugin per Spotlight, per esempio <a href="http://tarimporter.sourceforge.net/" target="_blank">tarimporter</a> e <a href="http://81-174-13-243.f5.ngi.it/manimporter/" target="_blank">manimporter</a>, e un altro po' di software per Mac. Lui scopre che tendo a frequentare le pizzate del Poc dell'ultimo venerd&#236; del mese.

Alla fine ci si rischia pure di conoscere a una delle prossime pizzate.

Un sacco di gente non fa che sottolineare i pericoli e i rischi di Internet e della chat. Ci sono anche i vantaggi, però.