---
title: "Recensione vera"
date: 2006-06-13
draft: false
tags: ["ping"]
---

Pareri di utilizzo da parte di <strong>Alan Dragster</strong>, che &egrave; passato a un MacBook Pro 17&rdquo; e lo sta usando davvero.

<cite>Da buon macuser ho abbandonato subito l&rsquo;idea di bootcampare (se giro dai clienti con un mac che fa il boot su windoze ke &ldquo;vantaggio&rdquo; avrei?) e mi sono comprato Parallels. L&rsquo;ho installato ieri sera e in 40 minuti (sic) e senza alcun reboot ci ho installato (per necessit&agrave; lavorative) Win2K server, Win XP Pro e Ubuntu Linux&hellip;
</cite>
<cite>Considerazioni pro e contro (dando per scontato che tutto lo standard per un mac funziona a dovere):
</cite>
<cite><ul type="disc">
	<li>non sono riuscito a trovare un modo di importare le VM che &ldquo;usavo&rdquo; sul vecchio Virtual PC (ma guardando sul sito di Parallels sembra abbiano un sacco di idee in proposito);</li>
	<li>l&rsquo;animazione tipo &ldquo;switch user&rdquo; per passare da una VM all&rsquo;altra ha &ldquo;venduto&rdquo; il Mac a tre amici/clienti, stufi di fare reboot per passare da un OS all&rsquo;altro e (strano ma vero) innamorati di Garageband, PhotoBooth e del telecomando (Windows Media Center Addio!);</li>
	<li>la facilit&agrave; nel collegarsi a domini Windows per beccare file/stampanti condivise (99% della necessit&agrave; nei network aziendali) ha conquistato il responsabile IT del gruppo ******;</li>
	<li>anche le applicazioni &ldquo;rosettate&rdquo; mi danno l&rsquo;impressione di essere uguali o pi&ugrave; reattive che sul &ldquo;vecchio&rdquo; G5 dual 2Ghz&hellip; (considerando che non ho fatto una installazione &ldquo;pulita&rdquo; ma ho importato da quello tutto quanto tranne la libreria iTunes di 70Gb. E quindi ho importato qui tutto, inclusi preferencepanes eccetera incompatibili), Photoshop/Dreamweaver/Office inclusi!;</li>
	<li>tramite .mac ci sincronizzo il pc di ufficio (il G5 di cui sopra che conosci) e tramite Missing Sync sincronizzo il mio Treo650; unica mancanza, un &ldquo;vero&rdquo; modo di sincronizzare la posta tra i pc senza armeggiare con i parametri degli account;</li>
	<li>mentre ti scrivo sono collegato a Skype, iChat, sto usando Photoshop, Illustrator e GoLive per un sito e (<a href="http://www.parallels.com/" target="_blank">Parallels</a> l&rsquo;ho chiuso ma credo ci sarebbe stato pure lui) guardando l&rsquo;Argentina con <a href="http://www.elgato.com/" target="_blank">EyeTV</a>&hellip;;</li>
	<li>nonostante tutto quello che si legge su overheating ce l&rsquo;ho sulle ginocchia e, caldino s&igrave;, non ha nulla a che vedere con l&rsquo;effetto bistecchiera di certi pc windoze che ho avuto&hellip;;</li>
	<li>unico rammarico &egrave; Airport (ti ricordi come &egrave; fatta casa mia, no?): ho la base in ufficio e il MBP in salotto, ma se non sto verso la porta perde il segnale (muri a parte saranno 8 metri scarsi&hellip;). Speravo fosse pi&ugrave; potente, ma devo lavorarci.</li>
</ul>
</cite>
Un grosso ringraziamento e una morale: l&rsquo;unica recensione buona &egrave; la recensione di uno che ci lavora veramente.