---
title: "Sophia compie cinque anni<p>"
date: 2005-05-09
draft: false
tags: ["ping"]
---

E la cosa si commenta da sé. O dovrebbe<p>

Piergiovanni ha annunciato il quinto anniversario dell&rsquo;entrata in funzione di Sophia. Che, a questo punto va rivelato, non è una bimba né una gatta siamese, ma un iMac Bondi.<p>

Chi si lamenta dei prezzi, o dell&rsquo;uscita continua di nuovi modelli, dovrebbe guardare i propri reali requisiti. Può darsi che anche un computer di cinque anni fa sia assolutamente ideale.<p>

Spalmandone il costo su cinque anni, inoltre, voglio vedere chi avrà il coraggio di dire che forse, chissà, magari, rispetto a un cassone grigio brutto impersonale e tecnologicamente inferiore, Sophia costava qualcosina in più.<p>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>