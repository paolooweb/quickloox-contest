---
title: "Il link come lo vuoi"
date: 2003-06-06
draft: false
tags: ["ping"]
---

Un trucchetto simpatico e semplice per personalizzare Safari

Sarebbe bello poter cambiare in Safari il colore dei link già visitati in qualcosa che soddisfa praticità e gusto estetico, vero? Beh, è bello, nel senso che si può fare.

Safari infatti supporta file Css (Custom Style Sheet, fogli stile personalizzati) realizzati ad hoc, che consentono questa e molte altre migliorie.

Crea un file di testo che contenga questo:

a:visited { color:yellow; background-color:black }

Salvalo con il nome safari.css. Poi usa l’opzione Advanced del menu Preferences di Safari e usa il popup Style Sheet per selezionare il file Css appena creato.

I colori dell’esempio sono banali ma in realtà si può scegliere quello che si preferisce, fino ai valori arbitrari nel codice esadecimale di Html come #BB9922.

Mi raccomando: che ci sia uno stile e non solo il foglio. :-)

<link>Lucio Bragagnolo</link>lux@mac.com