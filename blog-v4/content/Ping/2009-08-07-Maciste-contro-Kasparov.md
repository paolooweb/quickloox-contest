---
title: "Leonardo da Vinci contro Kasparov"
date: 2009-08-07
draft: false
tags: ["ping"]
---

Su <i>The Mac Observer</i> hanno <a href="http://www.macobserver.com/tmo/article/garmins_nuvi_vs._iphones_maps_the_ultimate_showdown/" target="_blank">confrontato il navigatore n&#252;vi di Garmin con Mappe di iPhone</a>.

Il confronto tra un apparecchio generalista e un apparecchio specializzato è sempre un po' fuori luogo, come fare una sfida tra un geniale tuttofare e un geniale scacchista: il tuttofare, a scacchi, perderà sempre. Ma forse dopo la partita si prendono un caffè e lui lo sa fare, lo scacchista no.

Prossima puntata: confronto tra n&#252;vi e iPhone sulla gestione dei calendari. Ho il sospetto che vincerà iPhone. E che nessuno scriverà il pezzo, essendone evidente l'assurdità.

Di questo, invece, non abbastanza.