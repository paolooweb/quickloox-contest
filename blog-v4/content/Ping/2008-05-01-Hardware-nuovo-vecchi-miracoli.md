---
title: "Hardware nuovo, vecchi miracoli"
date: 2008-05-01
draft: false
tags: ["ping"]
---

Con iPhone e iPod touch sembra di essere tornati ai tempi in cui lo hardware aveva limiti intrinseci e cos&#236; i programmatori dovevano fare miracoli con il software.

Un esempio è <a href="http://www.psiloc.com/en/Company/Blog/1292,iQuickBlock_for_iPhone" target="_blank">iQuickBlock</a>, gioco semplicissimo (e però ora c'è) creato da un programmatore polacco nel giro di&#8230; sei ore.

Un altro esempio? <a href="http://iphone.donaldhays.com/sunset/" target="_blank">Sunset</a>. Il programmatore voleva dimostrare che era possibile realizzare un gioco di ruolo grafico usando solo JavaScript. Lo ha fatto.

Sunset si può anche provare su Mac. Ovviamente, per le risorse di un Mac, fa ridere. Ma dà un'idea dei prodigi che può compiere un programmatore motivato.