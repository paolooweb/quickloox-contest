---
title: "Pillola rossa e pillola touch"
date: 2010-10-01
draft: false
tags: ["ping"]
---

Non ho fatto in tempo ad avvisare perché il tutto è accaduto molto velocemente: il sottoscritto è comparso alla manifestazione <a href="http://www.insidesrl.it/creare-stampare/2010/index.cfm" target="_blank">Gutenberg Reloaded</a> che si è svolta a Milano lo scorso 28 settembre.

Ho moderato un paio di seminari e ho tenuto un intervento di un'oretta abbondante sul perché di iPad e su come ragionare in vista dell'intenzione di pubblicare qualcosa sull'oggetto.

Per il momento è visibile in rete la <a href="http://public.iwork.com/document/?a=p38442574&amp;d=Logica_iPad.key" target="_blank">presentazione</a> che ho tenuto. A breve renderò disponibile anche l'audio dell'intervento (mancano un paio di minuti iniziali, più che altro introduttivi).

Il pubblico era di giornalisti, professionisti della stampa, operatori pubblicitari, grafici, impaginatori e in generale tutte persone che lavorano in relazione alla carta, tutte comprensibilmente interessate a una forma di &#8220;carta&#8221; decisamente nuova, che ci si diverte a toccare per ottenere risultati sempre nuovi.

Un conto è il digitale alla Matrix, che crea universi paralleli; un conto è quello alla iPad, che arricchisce quello in cui siamo stato cos&#236; fortunati da venirci a trovare.