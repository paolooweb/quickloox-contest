---
title: "Causa. Ed effetto"
date: 2010-03-31
draft: false
tags: ["ping"]
---

Non mi aspettavo che accadesse ed è sempre un piacere ricevere comunicazioni dirette da Apple, ma questa è abbastanza insolita.

Ho ricevuto infatti una lettera di <i>cease and desist</i> direttamente da Cupertino.

<i>Cease and desist</i> è sinonimo di <i>o la finisci o si va in tribunale</i> e Apple ha problemi con il fatto che io alimenti un blog di nome Ping.

<cite>La parola ping,</cite> recita infatti la lettera, <cite>appartiene al sistema operativo Unix, sulle cui fondamenta Apple ha creato il rinomato sistema operativo Mac OS X. Apple,</cite> continua l'ufficio legale, <cite>fa rispettare i propri</cite> copyright <cite>e invita il destinatario della presente a chiudere immediatamente il blog Ping, nell'attesa di capire se il suo titolo costituisca una violazione dei marchi registrati dell'azienda.</cite>

Ho il forte sospetto che Apple non abbia nessun genere di <i>copyright</i> sui comandi Unix e che si tratti di un'esca che mi hanno lanciato gli avvocati, sperando che io abbocchi e chiuda comunque il blog, o almeno gli cambi il titolo.

D'altronde sono un pesce piccolo e, se si finisse davanti al giudice, finirei schiacciato come una sogliola.

Se qualche lettore fosse avvocato, specie una vecchia lenza della pratica forense, pendo dalle sue labbra.

Altrimenti cambierò il nome del blog. Attualmente il candidato più probabile è fish, in omaggio alla shell di nuova concezione di cui <a href="http://www.macworld.it/uncategorized/2009/11/22/il-pesce-di-novembre/" target="_self">ho già parlato in passato</a> e che <a href="http://www.macworld.it/unix/2009/04/01/a-pesca-di-gusci/" target="_self">ho adottato proprio un anno fa</a>.

Quello che non cambierà, certamente, è il tono. Non so se ci sarà ancora Ping o se aprile comincerà all'insegna di <a href="http://fishshell.org/index.php" target="_blank">fish</a>; di certo, questo blog non sarà mai né carne né pesce, come molti altri. Qui abbiamo le scaglie dure.