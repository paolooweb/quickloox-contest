---
title: "Chi fermerà Mogulus"
date: 2008-06-18
draft: false
tags: ["ping"]
---

Devo molte scuse a quanti si sono lamentati (<em>in primis</em> <strong>Fearandil</strong> ed <strong>Ernesto</strong>) per il fatto che la diretta dell'evento-NeoOffice parte da sola appena caricata la pagina di <em>Ping!</em> e, effettivamente, la decima volta che uno se la sente in cuffia ci si chiede il perché.

Me ne sono accorto molto tardi e le scuse riguardano soprattutto questo. Poi mi sono trovato di fronte a una decisione di interfaccia problematica e non sono riuscito a escogitare qualcosa di geniale. A mia conoscenza è impossibile caricare l'interfaccia di Mogulus senza che parta in automatico (e se ci fosse il sistema questa sarebbe certo stata la soluzione migliore). Avrei altrimenti potuto limitare il numero di <em>post</em> mostrati nella pagina, tagliando via quello l&#236;. Però l'interesse generale della pagina ne avrebbe risentito.

Molto all'italiana, il problema si sta risolvendo da solo. Ossia il <em>post</em> dell’evento, passato tempo a sufficienza, sta uscendo dalla pagina per entrare negli archivi. Può darsi che proprio questo messaggio sia quello decisivo, altrimenti succederà domani.

Chissà se qualcuno ha mai avuto l'idea di produrre <em>stream</em> di testo anziché di video. Di sicuro non avrebbe disturbato nello stesso modo.