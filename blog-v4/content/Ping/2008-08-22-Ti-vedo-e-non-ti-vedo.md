---
title: "Ti vedo e non ti vedo"
date: 2008-08-22
draft: false
tags: ["ping"]
---

Molto carino e anche utile il trucchetto Unix <a href="http://www.tuaw.com/2008/08/22/terminal-tips-make-hidden-dock-icons-transparent/" target="_blank">pubblicato da MacOSXHints</a> sulle applicazioni nascoste e la loro rappresentazione nel Dock.

Da Terminale si digita

<code>defaults write com.apple.Dock showhidden -bool YES</code>

e si preme Invio per confermare.

La modifica si vede dopo avere dato il comando <code>killall Dock</code>. E consiste nel vedere trasparenti le icone nel Dock delle applicazioni che sono nascoste.

Per riportare le cose allo stato originale si ripete la procedura con NO al posto di YES.

Come <a href="http://www.sisde.it/sito/Rivista28.nsf/efbc5228d556d6a9c1256b650038241e/fc9f0eb5e255f14bc1256eda005bac78!OpenDocument" target="_blank">ha scritto Edgar Allan Poe</a>, le cose meglio nascoste sono quelle in piena luce. :)