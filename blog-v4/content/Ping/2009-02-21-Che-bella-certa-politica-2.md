---
title: "Che bella certa politica / 2"
date: 2009-02-21
draft: false
tags: ["ping"]
---

Non so se Obama stia lavorando bene o male, meglio o peggio.

Tuttavia le agenzie governative americane che erogano i fondi di aiuto all'economia e alla finanza in crisi sono comunque tenute a documentare dove finiscono i soldi anche <a href="http://www.vienna-rss.org/vienna2.php" target="_blank">attraverso feed Rss</a> (pagine 57-59).

A quando la possibilità di sapere tramite <a href="http://www.vienna-rss.org/vienna2.php" target="_blank">Vienna</a> come usa i nostri soldi lo Stato, a partire dal governo fino all'ultimo degli assessori comunali?