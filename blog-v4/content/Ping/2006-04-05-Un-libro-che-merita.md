---
title: "Un libro che merita"
date: 2006-04-05
draft: false
tags: ["ping"]
---

Sono passato l&rsquo;altroieri nella sede della casa editrice Tecniche Nuove per parlare delle mie <a href="http://www.tecnichenuove.com/epages/Store.sf/?ObjectPath=/Shops/TN/Products/88-481-1727-9" target="_blank">Internet Yellow Pages</a> appena uscite e ho scoperto e abbondantemente sfogliato <a href="http://www.tecnichenuove.com/epages/Store.sf/?ObjectPath=/Shops/TN/Products/88-481-1949-2" target="_blank">Final Cut Pro 5 per Mac OS X</a>.

Non posso che raccomandarlo di tutto cuore. Final Cut Pro 5 &egrave; un programma totalmente professionale e, una volta tanto, sembra esserci un libro altrettanto professionale.