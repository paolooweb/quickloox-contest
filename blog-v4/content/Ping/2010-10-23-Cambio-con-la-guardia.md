---
title: "Cambio con la guardia"
date: 2010-10-23
draft: false
tags: ["ping"]
---

Le batterie di <a href="http://www.apple.com/it/magicmouse/" target="_blank">Magic Mouse</a> sono arrivate a fine vita dopo 161 giorni, più di cinque mesi.

Per gli amanti dei dibattiti sulle marche: le batterie fornite di serie con il mouse all'acquisto erano Energizer, mentre quelle appena esaurite erano Duracell. La durata delle Energizer era stata di 141 giorni, venti in meno, il che significa una durata maggiore del 14 percento a favore delle Duracell.

Naturalmente un campione statistico insignificante, un esempio contro un esempio, non contiene alcun insegnamento da seguire o consiglio da dare.