---
title: "Rispetto per la gerarchia"
date: 2008-12-23
draft: false
tags: ["ping"]
---

Giorni fa notavo come il Finder sia comodo e utile e a volte <a href="http://www.macworld.it/blogs/ping/?p=2093" target="_blank">il Terminale sia più rapido</a> per sbrigare certi compiti un po' al limite.

Ecco un altro esempio. Devo creare una cartella <code>libro</code> che contiene le cartelle <code>capitolo01</code>, <code>capitolo02</code>, <code>capitolo03</code>, <code>capitolo04</code> e <code>capitolo05</code>.

Con il Finder, Comando-Maiuscole-N e rinomina la prima cartella. Poi apri la cartella. Poi Comando-Maiuscole-N per la prima sottocartella&#8230;

con il Terminale: <code>mkdir -p libro/capitolo{01,02,03,04,05}</code> ed è fatta. È facile capire che basta una singola riga per creare un albero gerarchico intero.