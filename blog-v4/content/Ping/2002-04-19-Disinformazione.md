---
title: "Disinformazione"
date: 2002-04-19
draft: false
tags: ["ping"]
---

L’ignoranza è una brutta bestia, la paura peggio. Ecco come si vendono tanti Pc

Mi scrive l’amico Roberto, appassionato di montaggio video amatoriale, dimenticandosi che abbiamo gli euro, ma fa lo stesso:

“Vuoi sapere l’ultima? L’anno scorso ho acquistato una scheda per collegare una telecamera mini Dv al Pc e ho speso Lit.1.200.000. Ora che ho installato Windows Xp  la scheda non è riconosciuta. Mi sono informato e mi è stato detto che non funziona perchè non ci sono i driver per Xp e che non è previsto che vengano fatti; bisognerebbe prendere il modello nuovo della scheda, progettato per Xp.
Problema: il modello nuovo costa Lit 2.500.000. Quindi o reinstallo una versione vecchia di Win che di solito si pianta o non uso una scheda praticamente nuova”.

Roberto pensava se comprarsi un iMac con SuperDrive e iMovie di serie. Non lo ha fatto perché “costava un po’ di più” e per il suo rivenditore “poi non è compatibile”.
Ciak. Motore. (disinform)Azione.

<link>Lucio Bragagnolo</link>lux@mac.com