---
title: "Arrivano i gigapixel"
date: 2006-10-29
draft: false
tags: ["ping"]
---

<strong>Pierfausto</strong> mi segnala (grazie!) questo <a href="http://haltadefinizione.deagostini.it" target="_blank">progetto</a> di De Agostini per realizzare fotografie di soggetti artistici (per cominciare) a 8,6 gigapixel, tremila volte la mia fotocamera di casa.

Il progetto è molto interessante, l'interfaccia fa schifo (li obbligherei tutti a studiare per un mese come funziona <a href="http://maps.google.com" target="_blank">Google Maps</a>) e il digitale sta iniziando davvero a cambiare il mondo. Da tenere presente.