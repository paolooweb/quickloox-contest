---
title: "Il vecchio &egrave; ancora nuovo"
date: 2006-01-11
draft: false
tags: ["ping"]
---

Forse in seguito lo spiegher&ograve; con maggiore dettaglio, ma in estrema sintesi, posseggo un PowerBook G4 17&rdquo; acquistato a gennaio 2005.

Primo, non ho la minima intenzione di venderlo. Secondo, tutto il software che Apple presenter&agrave; nei prossimi anni potr&agrave; essere usato sul mio portatile.