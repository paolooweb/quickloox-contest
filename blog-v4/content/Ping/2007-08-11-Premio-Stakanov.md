---
title: "Premio Stakanov"
date: 2007-08-11
draft: false
tags: ["ping"]
---

Giravano commenti per cui Apple si era distratta con iPhone e trascurava il versante Mac.

A parte una beta di Leopard, sono arrivati i nuovi iMac e una raffica di aggiornamenti software che non si riesce a tenere il conto, più iLife '08 e iWork '08. Software vero, non aggiornamenti di maniera. iMovie '08 è una mezza rivoluzione, che sia vera la storia dell'ingegnere o meno.

La cosa che è successa meno è stato lo <em>speed bump</em> dei Mac mini, che in altri tempi avrebbe acceso il dibattito per giorni. A malapena ce ne siamo accorti.

E Apple Italia ha indetto un evento stampa per il 23 agosto. Sempre in altri tempi, Apple Italia sarebbe stata ultrachiusa per ferie.

Se questa è distrazione.