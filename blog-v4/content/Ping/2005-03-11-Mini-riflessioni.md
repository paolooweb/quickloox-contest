---
title: "Mini riflessione<p>"
date: 2005-03-11
draft: false
tags: ["ping"]
---

Qualche utilizzatore vero, invece del solito sciocco noioso giornalista<p>

Cedo la parola all&rsquo;amico Sergio.<p>

<cite>Ieri è arrivato il Mac Mini che ho preso per mio figlio.</cite><p>

<cite>Piccola riflessione mattutina (da Day After).</cite><br>
<cite>Non importa se un Dell costa meno o se col classico bricolage delle componenti si riesce ad assemblare qualcosa di più economico. È un Mac ed ha un prezzo accessibile, molto. Ma soprattutto è&hellip; BELLISSIMO. Da non credersi, le foto non rendono nemmeno lontanamente l'idea.</cite><p>

<cite>Ho visto mio figlio commosso (ha 20 anni, tempesta ormonale adolescenziale, far apparire un sorriso è un'impresa che non riesce facilmente) e se lo rigirava tra le mani quasi coccolandolo. È solo un computer, è solo merce, un oggetto, ma con un c**** di PC non sarebbe (e non è) successo.</cite><p>

<cite>Dipende da cosa uno intende con &ldquo;costare meno&rdquo;, non ho molti soldi, ma buttarli via per (l&rsquo;illusione di) risparmiare 13 euro mi sembra&hellip; uno spreco.</cite><br>
<cite>Vecchio discorso, lo so.</cite><p>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>