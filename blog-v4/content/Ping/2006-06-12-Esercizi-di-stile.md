---
title: "Esercizi di stile"
date: 2006-06-12
draft: false
tags: ["ping"]
---

Mi chiama in iChat <strong>Nero</strong> e mi chiede <cite>Devo generare un file di testo con una sequenza di numeri da 1 a mille, ogni numero deve essere seguito da return. Se avessi un Basic saprei perfettamente come fare. Ma come lo faccio sul Terminale?</cite>

Rispondo <cite>Do per scontato che tu non lo voglia fare con un foglio di calcolo qualsiasi. </cite>

Lui replica <cite>Oddio, ho Excel ma se risolvo da Terminale miglioro anche le mie conoscenze</cite>.

Ci lavoriamo su un quarto d&rsquo;ora. Il problema vero non &egrave; come scrivere un ciclo in Python o Perl ma registrare il lavoro in un file su disco.

Alla fine Nero scopre che in Python si pu&ograve; scrivere <code>print range(1,1000)</code> per avere tutti i numeri da uno a mille, in sequenza, separati da virgole. Io inizio a spulciare una <a href="http://docs.python.org/tut/node9.html" target="_blank">pagina della documentazione</a> Python e alla fine Nero arriva a determinare che si apre il file su disco con <code>testo=open('/Users/nomeutente/Desktop/testo.txt', 'w')</code>, si imposta la variabile contenente l&rsquo;intervallo dei numeri con <code>value = (range(1,500))</code>, si converte il valore numerico in testo con <code>t = str(value)</code>, lo si scrive nel file con <code>testo.write(t)</code> e si chiude il file con <code>testo.close</code>.

Problema risolto e un po&rsquo; di sana ginnastica per il cervello. Certo si poteva fare con un foglio di calcolo in quattro secondi. Mica bisogna essere programmatori. Ma mi &egrave; gi&agrave; capitato, in treno o in vacanza, di misurarmi con uno schema di parole crociate o con qualche quiz paraaritmetico. Eppure mica sono un enigmista. Sono tutti esercizi di stile. Mentale.