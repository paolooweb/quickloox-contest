---
title: "Beato chi ci crede (ancora)"
date: 2002-01-04
draft: false
tags: ["ping"]
---

Non è più come una volta e non bisogna credere proprio a tutto quello che si legge (o si vede)

Questo pezzo si legge dopo Macworld Expo ma lo scrivo prima; la situazione apparirà ancora più chiara.
Ieri Apple era un colabrodo. I nuovi prodotti erano noti con ampio anticipo e con danni enormi per l’azienda. Si diffusero i cosiddetti rumor site, specializzati nella pubblicazione di pettegolezzi e notizie non ufficiali sulla Mela.
Un giorno Steve Jobs presentò iMac e fu una sorpresa totale: nessuno aveva saputo dirlo in anticipo. Da allora la segretezza è tornata seria e i siti di rumor sono diventati totalmente inattendibili.
L’iMac con schermo piatto viene dato per certo da ormai tre Expo; chiaro che prima o poi apparirà. In questi giorni un sito tedesco, Spymac.com, ha pubblicato filmati falsi, molto ben fatti, che mostrano un fantomatico iWalk.
Credere alle fiabe è una bellissima cosa, a patto di non perdere il contatto con la realtà.

lux@mac.com