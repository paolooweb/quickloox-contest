---
title: "Vedo, prevedo, stravedo"
date: 2008-09-15
draft: false
tags: ["ping"]
---

Tutti i MacBook/Pro nuovi che sarebbero stati certamente presentati il 9 settembre, ora saranno presentati certamente a metà ottobre. Nel caso occorresse inventarsene un'altra, chiaramente a fine novembre. O a Macworld Expo di inizio gennaio, sicuro.

Le previsioni serie sono ben altre. Per esempio, adesso che l'espansione <a href="http://www.worldofwarcraft.com/wrath/" target="_blank">World of Warcraft: Wrath of The Lich King</a>, è stata annunciata per il 13 novembre, so per certo che il mio <a href="http://eu.wowarmory.com/character-sheet.xml?r=Stormrage&amp;n=Ithilgalad" target="_blank">irregolare elfo</a> avrà raggiunto il livello 70 entro quella data.