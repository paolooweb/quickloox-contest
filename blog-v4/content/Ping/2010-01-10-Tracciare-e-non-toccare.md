---
title: "Tracciare e non toccare"
date: 2010-01-10
draft: false
tags: ["ping"]
---

<cite>Lo schermo</cite> touch <cite>oramai ce lo hanno tutti.</cite>

Come no. In compenso, non tutti hanno uno schermo <i>touch</i> fedele al tocco. Guarda un po', basta <a href="http://labs.moto.com/diy-touchscreen-analysis/" target="_blank">una semplice prova</a> per capire che iPhone ha uno schermo <i>touch</i> e gli altri imitano a basso costo, con risultati inferiori.