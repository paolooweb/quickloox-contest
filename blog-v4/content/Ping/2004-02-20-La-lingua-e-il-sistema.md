---
title: "La lingua e il sistema"
date: 2004-02-20
draft: false
tags: ["ping"]
---

Fare un salto di qualità nella conoscenza del sistema richiede un salto di qualità nel nostro bagaglio culturale

Metà del mondo vuole sapere qualcosa in più su Unix e metà della metà del mondo chiede con insistenza libri e manuali in italiano.

È un errore. Per sapere le dieci cose indispensabili alla sopravvivenza rispetto al lato Unix di Mac OS X, un libro è perfettamente inutile. Si fa qualche domanda in una buona mailing list, si frequenta un po’ Internet e si trovano tutte le informazioni.

Per approfondire il discorso e diventare esperti, l’italiano è inutile. Tutto Unix parla inglese, tutti i file di configuazione sono in inglese, tutte le pagine man sono in inglese, tutti i siti importanti sono in inglese. Chiedere cose in italiano serve solo a ritardare l’inevitabile, cioè accorgersi che per Unix è indispensabile l’inglese.

Si fa più fatica, ma rende di più. Dammi retta: prendila come una opportunità preziosa, procurati un buon dizionario di inglese e impegnati a usare <link>OneLook</link>http://www.onelook.com. Nel dubbio, scrivimi. Imparerai il doppio (inglese e Unix), meglio, in metà tempo.

<link>Lucio Bragagnolo</link>lux@mac.com