---
title: "Lampista cercasi"
date: 2010-04-28
draft: false
tags: ["ping"]
---

Mentre la direzione giusta è Html5, non c'è dubbio che la base installata di Flash sia enorme e che generi tuttora lavoro e opportunità.

Un carissimo amico, che produce contenuti multimediali ad alto livello, mi informa che:

<cite>siamo sempre più alla ricerca di una persona Flash moderatamente</cite> junior <cite>che abbia voglia di trovarsi per le mani dei bei lavori (</cite>web gaming <cite>leggero per nomi come Disney) e soprattutto abbastanza continuativi.</cite>

Gli interessati possono <a href="mailto:lux@mac.com?subject=Lampista%20cercasi>scrivere a me</a> e provvederò a stabilire il contatto.