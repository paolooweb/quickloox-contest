---
title: "Robot di quart'ordine"
date: 2010-01-20
draft: false
tags: ["ping"]
---

Premesse per capire che cosa succede nella traduzione che sto per fornire del <a href="http://blogs.perl.org/users/cpan_testers/2010/01/msnbot-must-die.html" target="_blank">blog di Perl.org</a>: Cpan è un deposito di software libero per Perl. Un <i>bot</i> è, anche, codice che perlustra i siti e li indicizza a beneficio di un motore di ricerca. <i>robots.txt</i> è un file che si mette sui <i>server</i> e illustra le regole di galateo per i <i>bot</i> che vengono in visita. Un attacco <i>denial of service</i> mette in ginocchio un server saturandolo di richieste di connessione.

Ecco la traduzione.

<cite>Se la scorsa sera avete riscontrato problemi nell'accesso ai</cite> server <cite>Cpan Tester, arrabbiatevi con Microsoft. Questa notte</cite> msnbot [il bot del motore di ricerca Bing di Microsoft] <cite>ha scatenato un attacco</cite> denial of service dedicato. <cite>[&#8230;]</cite>

<cite>Sembra che i loro</cite> bot<cite>, 20-30 ogni pochi secondi, ignorino completamente le regole di</cite> robots.txt<cite>. [&#8230;]</cite> I bot <cite>corretti, come quelli di Google, mandano un solo</cite> bot <cite>per volta su un sito.</cite>

<cite>Se qualcuno di Microsoft legge queste parole, sappia che normalmente gli autori degli attacchi</cite> denial of service <cite>vengono denunciati e arrestati. Se Cpan Testers fosse una persona giuridica, lo avremmo già fatto.</cite>

Nei commenti si evince che le incursioni dei <i>bot</i> creano problemi esclusivamente alle aree del <i>server</i> dedicate alle estensioni per Firefox, guarda caso il concorrente più fastidioso per Microsoft nel campo dei <i>browser</i>.

Certo si scoprirà che nessuno ha fatto apposta. Cos&#236;, invece della malafede, potremo pensare all'incompetenza. Aziende di quart'ordine.