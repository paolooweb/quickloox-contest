---
title: "Sei in condotta"
date: 2008-10-14
draft: false
tags: ["ping"]
---

C'è qualcuno che riesce ad aiutare <strong>Marco</strong> nell'installazione di Java 6 su un Mac che non è Intel 64 bit (ovvero Intel 32 bit o PowerPc)?

Marco ha individuato due <em>tutorial</em>, <a href="http://wiki.netbeans.org/JavaFXAndJDK6On32BitMacOS" target="_blank">uno sul sito NetBeans</a> e un altro su <a href="http://landonf.bikemonkey.org/static/soylatte/" target="_blank">una pagina dedicata a SoyLatte</a>. Però è roba complicata e non sono ancora riuscito a effettuare l'installazione (su una macchina di collaudo, perché se mi si fuma Java sulla macchina principale sono perduto).

Ho anche erroneamente creduto che l'<a href="http://support.apple.com/kb/HT2733" target="_blank">aggiornamento Java 2</a> portasse Java 6 annche sulle vecchie macchine, ma non è cos&#236;. E alla fine Apple non ci fa una bellissima figura, perché l'abbandono delle vecchie macchine ha un sacco di buone ragioni, ma non quando si parla di sviluppatori. Si può scrivere un programma straordinario per Mac OS X anche lavorando sopra un vecchio mulo da soma, e allora che il software da sviluppo giri dappertutto, uffa. Su queste cose la pagella Apple è insufficiente.