---
title: "Lasciamolo dormire"
date: 2006-10-11
draft: false
tags: ["ping"]
---

Una grande idea per evitare che il portatile si apra inavvertitamente in borsa e, nell'aprirsi, si accenda con tutti i rischi del caso: disabilita, almeno temporaneamente, il risveglio automatico.

Il comando <a href="http://developer.apple.com/documentation/Darwin/Reference/ManPages/man1/pmset.1.html" target="_blank">pmset</a>, da Terminale, è abbastanza potente per farlo:

<code>sudo pmset lidwake 0</code>

E il portatile, anche se si apre per sbaglio, resta in sonno.

Si sveglia normalmente invece con la pressione di un tasto e basta ripetere il comando, con il parametro <code>lidwake</code> a <code>1</code>, per rimettere tutto come stava prima.