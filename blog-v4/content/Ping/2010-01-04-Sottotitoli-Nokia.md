---
title: "Sottotitoli Nokia"
date: 2010-01-04
draft: false
tags: ["ping"]
---

The Economic Times pubblica una <a href="http://economictimes.indiatimes.com/Opinion/Interviews/Nokia-targets-to-have-115-m-active-users-by-first-half/articleshow/5408409.cms?curpg=1" target="_blank">lunga intervista</a> a Rick Simonson, <cite>vicepresidente esecutivo e responsabile dell'entità telefoni cellulari</cite> di Nokia, più che altro sui rovesci che l'azienda finlandese ha sopportato nel 2009 in campo smartphone e come la stessa pensa di rifarsi.

Una cosa l'ha capita: sa come funzionano i siti di notizie Mac.

<cite>Sentiamo da molto tempo</cite> [che stiamo per comprare Palm]. <cite>Può darsi sia una di quelle cose che si predicono in continuazione sperando che nel 2010, o nei prossimi dieci anni, si avverino. È come ripetere</cite> pioverà, pioverà. <cite>Un giorno piove davvero e si dice</cite> lo avevamo predetto!

Per tutto il resto l'impressione è di nebbia fitta. Secondo lui, batteranno Rim &#8211; BlackBerry &#8211; offrendo nei Paesi emergenti soluzioni migliori di posta elettronica di massa su cellulare, lavorate in partnership con Microsoft, cioè con l'azienda che con Windows Mobile ha avuto il 2009 peggiore di tutte sugli <i>smartphone</i>. E in tema di crescita si lancia in questa predizione:

<cite>Nel 2011 i nostri sforzi inizieranno a produrre risultati, perché giocheremo alla pari con Apple e Rim sugli</cite> smartphone<cite>. Non solo saremo allo stesso livello ma vinceremo anche la guerra perché, in aggiunta alla posta, aggiungeremo contenuti, chat, musica, intrattenimento e molte altre funzioni che presto diventeranno estremamente critiche per il successo di qualunque azienda in questo spazio.</cite>

A me pare un modo elegante per dire <cite>siamo in ritardo di due anni e non abbiamo alcuna idea migliore di quelle che si trovano già ora su venticinque milioni di iPod touch, che neanche telefonano. Cos&#236; proviamo a fare numeri andando in Brasile e in India dove ancora si accontentano di tecnologia inferiore, visto che nei Paesi progrediti non siamo competitivi.</cite>

Aggiungeranno contenuti. E le applicazioni? La risposta è:

<cite>Non pensiamo che le persone nei mercati emergenti abbiano bisogno di centinaia di migliaia di applicazioni, ma piuttosto di quelle che sono rilevanti per loro e possono migliorare la qualità delle loro vite.</cite>

Una strategia sottotitolata <cite>andiamo nei Paesi più poveri a offrire meno degli altri</cite> mi suscita qualche dubbio.