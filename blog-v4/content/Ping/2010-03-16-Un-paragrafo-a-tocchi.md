---
title: "Un paragrafo a tocchi"
date: 2010-03-16
draft: false
tags: ["ping"]
---

Grazie infinite a <a href="http://www.macosxhints.com/article.php?story=20100312090805523" target="_blank">Mac OS X Hints</a> per avere svelato qualcosa che sarà ovvio per tutti, ma non lo era per me e non è neanche presente negli aiuti, che io sappia.

Quattro tocchi su un paragrafo di testo in iPhone o iPod touch e quel paragrafo viene selezionato interamente e automaticamente, pronto da tagliare, copiare, cancellare.

Nel rispondere alla posta, per esempio, è utilissimo.