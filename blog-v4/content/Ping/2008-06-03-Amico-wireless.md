---
title: "Amico wireless"
date: 2008-06-03
draft: false
tags: ["ping"]
---

Ti sarai reso conto che nel bar del palazzo si prende la tua rete wireless, perché malissimo che vada sarai al secondo piano e non è possibile che tu non ci abbia pensato.

Già che c'eri, potevi anche abilitare la navigazione Internet da parte degli sconosciuti, anche se hai fatto doverosamente benissimo a nascondere la rete interna.

In tutti i casi, una password di rete <em>pippo</em> non è esattamente una garanzia di sicurezza. Fossi stato uno cattivo, mi sarei preso un altro caffè e avrei saggiato per bene la consistenza del tuo <em>firewall</em>.