---
title: "Lavorare come negri<p>"
date: 2005-10-28
draft: false
tags: ["ping"]
---

Ancora una volta, Think Different<p>

Ero già stato su questo tema e si capisce come le cose non cambino.<p>

Mentre scrivo, in evidenza sul sito Apple: Rosa Parks, 1913-2005.<p>

Mentre scrivo, in evidenza sul sito Microsoft: Exchange Server 2003 - Download SP2 with improved mobility, spam protection, and more.<p>

Cari amici monopolisti e colpevoli (in tribunale) di Microsoft: tra un posto che non riesce a smettere di vendere e uno capace di ricordare una donna importante, io vado a comprare sul secondo.<p>

Lì hanno un&rsquo;idea di come si lavora. Voi lavorate senza idee.<p>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>