---
title: "Piccola teologia da Unix<p>"
date: 2005-12-11
draft: false
tags: ["ping"]
---

Può root creare un file talmente protetto da non riuscire più a cancellarlo?<p>

Non intendo essere blasfemo nel rievocare certe questioni da filosofia medievale, ma solo vedere un parallelo, in piccolo per carità, con quanto avviene in Mac OS X. Quando penso di avere capito tutto sulla gerarchia delle utenze Unix, scopro ogni volta qualcosa di nuovo.<p>

Non posso farla lunga, ma ho scoperto il file (che poi è una directory) <code>????????????HFS+ Private Data</code>. È impossibile cancellarlo, spostarlo, rinominarlo, scriverci dentro per risalvarlo, qualsiasi cosa. Dici che basta andare in root. Prova quanto vuoi. Neanche root può fare niente.<p>

L&rsquo;unico modo per liberarsene è effettuare il boot in <em>single user mode</em>, dove il controllo è un mano a un utente root, che però di fatto è un root un po&rsquo; più potente. Per parlare oscuro, opera a livello 0, mentre il root abituale opera a livello 1 nella gerarchia Unix.<p>

Alla fine il problema è che quella cartella ha un flag <em>immutable</em> attivato a livello di sistema, il flag schg. Detto flag è eliminabile solo partendo da un boot in single user mode.<p>

Andando a fondo della questione si scopre che la cartella è piena di file che iniziano con iNode e proseguono con un numero, e che praticamente trattasi della Madre di Tutte le Directory, tanto che Mac OS X fa tutto il possibile per evitare che possa essere toccata. I punti interrogativi visualizzati nel nome file sono in realtà caratteri <em>null</em>&hellip; e non voglio, né sono capace di scrivere l&rsquo;enciclopedia di Unix. Certo che, a voler scavare nel sistema, ce n&rsquo;è per una vita.<p>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>