---
title: "L'insolita sviolinata"
date: 2010-09-15
draft: false
tags: ["ping"]
---

Mi informa <a href="http://www.christiancasini.com" target="_blank">Christian</a> di avere debuttato su iTunes Store con <a href="http://itunes.apple.com/it/album/out-single/id392160557" target="_blank">Out - Single</a>, sotto la firma <a href="http://www.myspace.com/chaosventure" target="_blank">Chaos Venture</a>.

Christian è un tastierista/compositore/arrangiatore che usa Mac con profitto da sempre e i due singoli usciti su iTunes Store, glielo auguro, potrebbero essere l'inizio di una bella storia.

Le sue/loro sono atmosfere di <i>rock</i> psichedelico che ricordano <a href="http://www.jeanmicheljarre.com/" target="_blank">Jean-Michel Jarre</a>, con più ritmo e meno melodia, e qualche eco lontana dei <a href="http://www.tangerinedream.org/" target="_blank">Tangerine Dream</a>, che però sono più lenti e meno attuali.

Nel passato sarebbe stato impossibile annunciare l'uscita di due singoli di un nuovo artista desideroso di farcela; oggi iTunes Store consente questa, che non può essere la solita sviolinata, semmai un segno dei tempi.