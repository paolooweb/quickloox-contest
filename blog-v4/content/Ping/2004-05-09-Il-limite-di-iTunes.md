---
title: "La musica non è infinita"
date: 2004-05-09
draft: false
tags: ["ping"]
---

iTunes ha un limite alle canzoni che può ospitare. Alto

Uno dei più bei tributi all’eleganza e alla semplicità di iTunes è che qualcuno lo ha usato talmente tanto da riuscire a arrivare fino al limite delle capacità del programma.

Che è, pare, si dice, di 28.899 brani. Non ho verificato di persona e non so neanche se ho un disco rigido abbastanza grande per queste cifre; a dodici brani per album si parla di circa duemilaquattrocento album e nella mia biblioteca devo già mettere i libri.

Qualcuno riuscirà per breve tempo a mantenere un numero di brani appena superiore per effetto del caching, ma non durerà.

Tuttavia iTunes è davvero uno strumento elegante, che fa venire voglia di usarlo. Per qualcuno, fino al limite.

<link>Lucio Bragagnolo</link>lux@mac.com