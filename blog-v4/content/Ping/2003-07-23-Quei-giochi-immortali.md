---
title: "Quei giochi immortali"
date: 2003-07-23
draft: false
tags: ["ping"]
---

Ritorna Oids, più accattivante che mai e a misura di Mac OS X

Da giovane, insieme ai colleghi/amici, sprecai incosciente e soddisfatto ore e ore di produttività perdendomi nel caotico spazio esterno di Maelstrom. Fui strafelice di vederlo ritornare, per di più freeware, su Mac OS X (ora si trova sul sito di <link>Ambrosia Software</link>http://www.ambrosiasw.com/games/maelstrom/).

Sono quei giochi facili da imparare ma che inducono rapidamente dipendenza, e che ti fanno dimenticare veramente tutto quello che sta intorno nonostante la loro semplicità.

Oggi festeggio un altro ritorno: quello di Oids. In un fantastico pacchetto nativo Mac OS X con alcuni tocchi di avanguardia. Per esempio, <link>Oids X</link>http://www.xavagus.com può funzionare in sincronia con la frequenza di refresh del monitor e così offrire un’animazione ancora più fluida e naturale. In più quelli veramente bravi possono inviare il loro punteggio a Xavagus ed entrare in una classifica globale. Non è per me; dopo tanti anni posso solo sperare di riprendere un po’ di smalto nella galassia di Novoids, quella degli ultimi arrivati.

L’unica cosa è che sprecare produttività diventa una possibilità sempre più rara. :)

<link>Lucio Bragagnolo</link>lux@mac.com