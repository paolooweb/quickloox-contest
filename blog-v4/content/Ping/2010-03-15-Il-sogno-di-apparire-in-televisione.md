---
title: "Il sogno di apparire in televisione"
date: 2010-03-15
draft: false
tags: ["ping"]
---

Superare le selezioni di qualche quiz è robetta. Complimenti invece a <a href="http://infinite-labs.net/" target="_blank">Emanuele</a>, il cui <a href="http://infinite-labs.net/mover/download-plus" target="_blank">Mover+</a> viene mostrato in uno dei nuovi spot di Tre dedicati a iPhone.

Superare una selezione tra <a href="http://148apps.biz/" target="_blank">168 mila candidati</a> richiede gran valore. Complimenti!