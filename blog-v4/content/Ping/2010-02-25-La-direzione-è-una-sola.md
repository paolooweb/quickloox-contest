---
title: "La direzione è una sola"
date: 2010-02-25
draft: false
tags: ["ping"]
---

<cite>In che direzione sta andando il mercato dei sistemi operativi? Non lo la vedo come di qui o di l&#236; &#8211; iPhone contro Mac &#8211; o questo sopra quello Credo ci sia spazio per entrambi. Quello che state vedendo per Apple è che Mac OS è molto scalabile. Enorme vantaggio competitivo per Apple. Uso di Mac OS in un sacco di prodotti. Non credo che ci sia un'altra azienda che può usare in questo modo le fondamenta del proprio sistema operativo. Ci muoviamo ad alta velocità con molte meno persone di quelle che ci vorrebbero se ci trovassimo geograficamente a nord.</cite>

&#8212; Tim Cook, Chief Operating Officer di Apple, alla recente <a href="http://news.worldofapple.com/archives/2010/02/25/apple-coo-tim-cook-speaks-at-goldman-sachs-conference/" target="_blank">conferenza Goldman Sachs</a>

Per chiarire un'allusione e una circostanza tecnica, la sede di Microsoft è molto più a nord di quella di Apple. E Windows Mobile, ora Windows Phone Classic, non ha tecnicamente niente a che vedere con Windows, anche se porta lo stesso nome.

A parte tutto ciò, quando leggo che Apple trascura Mac o che vuole ucciderlo a favore di iPhone, mi scappa da ridere.