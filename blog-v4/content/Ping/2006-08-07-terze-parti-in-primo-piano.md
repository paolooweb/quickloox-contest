---
title: "Terze parti in primo piano"
date: 2006-08-07
draft: false
tags: ["ping"]
---

Negli ultimi dodici mesi si è venduto hardware e software indipendente per Mac pari a mezzo miliardo di dollari. E c'è chi sostiene che il passaggio a Intel sia stato negativo per il software.