---
title: "Opinioni in fiera"
date: 2010-09-06
draft: false
tags: ["ping"]
---

Durante l'ultima presentazione di iPod, iPod touch, iTunes 10, Apple Tv e quant'altro, Steve Jobs ha notato che Apple ha aperto 300 Apple Store fisici in tutto il mondo, frequentati da un milione di persone al mese, con 80 mila sessioni faccia a faccia alla settimana, per chi ha bisogno di saperne di più su un prodotto.

Di recente mi sono imbattuto nell'augusto parere di chi riteneva che Apple abbia fatto male a non presenziare più alle fiere di settore, perché era <cite>l'unica occasione che aveva di contatto diretto con il pubblico</cite>.

Se adesso uno mi cita una fiera con un milione di ingressi al mese tutti i mesi e trecento sedi nel mondo, me la segno volentieri.