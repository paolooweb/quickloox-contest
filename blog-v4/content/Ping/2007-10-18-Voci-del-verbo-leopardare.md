---
title: "Voci del verbo leopardare"
date: 2007-10-18
draft: false
tags: ["ping"]
---

Appuntamento da non perdere: venerd&#236; 26 ottobre ci saranno feste-Leopard un po' dappertutto. Io sarò, manco a dirlo, in <a href="http://www.macatwork.net" target="_blank">Mac@Work</a> tutto il giorno, dalla tarda mattinata: buffet, musica diffusa, tanta gente e tante occasioni per vedersi di persona e fare due chiacchiere (se non compri niente e ti abbuffi lo stesso non ti dice niente nessuno). Alla fine, in un momento imprecisato dopo le 19:30, pizzata alla Bio Solaire, via Terraggio 20, tre minuti a piedi da Mac@Work.

Appuntamento da non perdere: il primo libro su Leopard a uscire in Italia in italiano lo ha scritto Luca <em>Misterakko</em> Accomazzi, insieme con il sottoscritto. Sul sito di Akko è intanto possibile <a href="http://www.accomazzi.net/TlibriI107.html" target="_blank">leggere alcune pagine</a> di esempio, in attesa del 30 ottobre, in cui le pagine arriveranno tutte quante e tutte insieme in libreria.

Cose da sapere che non tutti i siti considereranno: visto che Apple non ha ancora reso in italiano la <a href="http://www.apple.com/it/macosx/features/300.html" target="_blank">pagina con tutte le modifiche</a> di Mac OS X, proseguo nell'elenco sommario: la seconda voce alfabetica è AppleScript. Che ora supporta pienamente Unicode; una nuova architettura detta Scripting Bridge lo fa parlare facilmente con Objective-C, Ruby, Python e altri linguaggi; è diventato ancora più agevole identificare le applicazioni e il loro stato; c'è una Language Guide aggiornata (finalmente); i messaggi di errore sono (finalmente) più precisi e puntuali; le Azioni Cartella hanno un loro server; varie Preferenze di Sistema ora sono scriptabili; e, ciliegina, AppleScript ora può lavorare sulle property list (i file <code>.plist</code>). Anche iChat è molto più scriptabile di prima.