---
title: "Ricchi premi e chiacchierons<p>"
date: 2005-05-25
draft: false
tags: ["ping"]
---

Ci troviamo a parlare un po&rsquo; di iPod e dintorni?<p>

Conflitto di interessi a raffica: stasera alle 18:30 in Mac@Work, per chi vuole, incontro con il sottoscritto a chiacchierare di iPod e argomenti collegati.<p>

Più importante, estrazione a sorte di dieci copie del mio libruzzo <em>Tutti pazzi per iPod</em> e, più importante dell&rsquo;importante, estrazione a sorte di un iPod mini!<p>

La serata trascorrerà, per chi vuole, alla pizzata mensile del <a href="http://www.poc.it">PowerBook Owners Club</a>, a tre minuti a piedi da <a href="http://www.macatwork.net">Mac@Work</a>.<p>

Non amo farmi pubblicità, ma undici regali sono undici regali. Se arrivano meno di undici persone ci resto male.<p>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>