---
title: "Arriverà il G5"
date: 2003-08-04
draft: false
tags: ["ping"]
---

Quello che ti puoi aspettare dagli incompetenti

Qualcuno interpreta già la sigla G5 come “il G4 con qualcosa in più”. No, spiacente; è un computer ben diverso, soprattutto con un processore interamente nuovo, con ascendenze Ibm invece che Motorola, con pipeline più lunga del precedente PowerPC (altrimenti sarebbe ben difficile alzare il clock) e il bus principale più ampio che si sia mai visto su un personal computer.

Ecco, il bus. Si alza già qualcuno a lamentarsi che nei PowerPc 970 non c’è una cache di livello 3 e che Apple ricorre ai soliti compromessi che limitano le prestazioni per tenere bassi i prezzi eccetera eccetera.

La verità è che, per esempio, il bus principale del 970 è talmente ampio (un gigahertz di bus nel modello con clock di processore a due gigahertz) che la cache level 3 non ha più ragione di essere: non sarebbe veloce abbastanza da migliorare le prestazioni.

Aspettati, oltre a questa, numerose altre incompetenze.

<link>Lucio Bragagnolo</link>lux@mac.com