---
title: "Un colpo di fortuna"
date: 2008-03-16
draft: false
tags: ["ping"]
---

Entro nell'aula dove devo tenere il corso. C'è un tecnico che armeggia. Mi presento.

<cite>Salve, sono il consulente, devo tenere il corso.</cite>

<cite>Bene, le ho preparato il Pc.</cite>

<cite>Veramente userò il mio Mac.</cite>

<cite>È sicuro che funzioni?</cite> (lo guarda)

<cite>Certo, è un Mac.</cite>

<cite>Ma come fa a collegarsi al videoproiettore senza porta Vga?</cite>

<cite>Ha una porta Dvi, qui c'è l'adattatore.</cite>

<cite>Guardi che questo videoproiettore è un po' un casino, meglio che usiamo il Pc.</cite>

(collego l'adattatore al Mac e attacco il videoproiettore. Scelgo Rileva Monitor dentro le Preferenze di Sistema. Un istante e sul megaschermo compare la scrivania del Mac).

<cite>Non avrei mai detto che avrebbe funzionato.</cite>

<cite>Un attimo, che la risoluzione è un po' bassa. La imposto al massimo possibile per il videoproiettore.</cite>

<cite>Glielo sconsiglio, è già molto che funzioni cos&#236;.</cite>

(cambio la risoluzione e il videoproiettore va a risoluzione massima).

<cite>Ecco fatto.</cite>

<cite>Ma adesso i Mac funzionano cos&#236; bene? Mi hanno sempre detto che non funzionavano&#8230;</cite>

(sorrido)

<cite>Un colpo di fortuna capita a tutti.</cite>