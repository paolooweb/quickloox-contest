---
title: "La rivoluzione di Dashboard"
date: 2006-08-07
draft: false
tags: ["ping"]
---

Diventerà ancora più facile creare widget per Dashboard, con Dashcode e varie altre funzioni, per sviluppatori e utenti finali. Tecnologia attualmente sottovalutata cui fare attenzione.