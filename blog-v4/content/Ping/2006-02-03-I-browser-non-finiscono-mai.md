---
title: "I browser non finiscono mai"
date: 2006-02-03
draft: false
tags: ["ping"]
---

<strong>Fede</strong> (grazie mille!) segnala <a href="http://hmdt-web.net/shiira/en" target="_blank">Shira</a>, browser costruito sulla base di WebKit e che mira a offrire un browser migliore di Safari pur basandosi sullo stesso motore.

Ci riesca o meno, di fatto la possibilit&agrave; di scelta aumenta. Per quanti amano fare paralleli tra Apple e Microsoft, una constatazione: dal motore di Safari, che &egrave; open source, nascono nuovi programmi indipendenti. Dal motore di Explorer, chiuso e proprietario, nasce niente.

E aggiungo: visto Shiira Mini, il browser in forma di widget?