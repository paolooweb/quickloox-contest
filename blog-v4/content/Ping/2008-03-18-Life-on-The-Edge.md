---
title: "Life on The Edge"
date: 2008-03-18
draft: false
tags: ["ping"]
---

Vita sul filo del rasoio. Più propriamente, la mia carta di credito.

Ho stipulato un contratto telefonico che prevede la connessione Edge per quelle tre volte al mese che per puro caso mi serve andare su Internet con iPhone e non ho a disposizione una rete wireless.

Con la connessione Edge, però, iPhone si comporta da computer e non da telefonino. Ovvero esegue un <em>keep alive</em>, un mantenimento in vita: ogni quindici-trenta minuti esegue in automatico un brevissimo accesso alla rete Edge&#8230; <cite>e io pago</cite>, come avrebbe commentato Totò.

Fortunatamente, nello smisurato parco software clandestino a disposizione degli iPhone clandestini, esiste anche un programmino chiamato BossPrefs che consente di disattivare il keep alive e pagare Edge solo e veramente quando serve.

Il che spiega anche perché Apple cerca di raggiungere accordi di traffico dati illimitato con i <em>provider</em>.

Grazie a <strong>Jacopo</strong> per la consulenza decisiva e puntuale.