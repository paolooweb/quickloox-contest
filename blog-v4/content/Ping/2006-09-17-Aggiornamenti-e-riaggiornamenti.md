---
title: "Aggiornamento e riaggiornamento"
date: 2006-09-17
draft: false
tags: ["ping"]
---

Installati con successo l'update di QuickTime e iTunes 7, ho riavviato come richiesto e tutto procede come deve.

Mi sono reso conto però di non avere documentato un riavvio non voluto di settimana scorsa. PowerBook lasciato con la sola batteria (piena) che dopo qualche minuto ha perso qualunque contatto con l'esterno (neanche il login remoto è servito). L'unico comando che ha risposto è stato il pulsante di accensione.

Dunque, 260 giorni a oggi, cinque riavvii indesiderati, uno ogni 52 giorni.