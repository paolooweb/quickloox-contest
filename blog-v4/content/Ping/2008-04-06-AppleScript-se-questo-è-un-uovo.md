---
title: "AppleScript: se questo è un uovo"
date: 2008-04-06
draft: false
tags: ["ping"]
---

Qualcuno avrà letto della particolarità della Pasqua appena trascorsa, insolitamente anticipata nel calendario.

<a href="http://www.bottegascientifica.it" target="_blank">Carlo</a> ha inviato uno script che calcola la data della Pasqua per un anno qualsiasi.

È molto semplice; usa quasi solo comandi elementari, che abbiamo già visto. È molto complicato; la formula per calcolare la Pasqua è a dire poco elaborata. Per gli interessati, rimando a Wikipedia per <a href="http://it.wikipedia.org/wiki/Calcolo_della_Pasqua" target="_blank">tutti i dettagli</a> e a God Plays Dice per una <a href="http://godplaysdice.blogspot.com/2008/03/easters-early-this-year-deal-with-it.html" target="_blank">formula vera e propria</a>. Per i fanatici del calendario il testo definitivo è <a href="http://www.tondering.dk/claus/calendar.html" target="_blank">The Calendar Faq</a>, ma sto divagando.

Prima vediamo alcune istruzioni che non conosciamo ancora e il loro significato. Le presenterò avulse dallo script. Poi arriverà lo script vero e proprio, che si &#8220;limita&#8221; a tradurre la formula in AppleScript. Visto da lontano lo script sembra di una difficoltà pazzesca e se invece lo si guarda istruzione per istruzione si tratta solo di tanto lavoro sulle variabili e tante operazioni aritmetiche.

<code>on EasterDate(aYear)</code>

In gergo programmatorio, <code>on</code> è una <em>handle</em>, una maniglia. In pratica è un contenitore. Tutto quello che sta tra <code>on</code> e <code>end</code> fa parte di un blocco di operazioni che riguarda una attività specifica. È una definizione di funzione. EasterDate (data Pasqua) è il nome dato da Carlo alla funzione. aYear (un anno) è il parametro. Avrebbe potuto chiamare la funzione Pasqua(anno) e sarebbe stata la stessa cosa. A un certo punto nello script apparirà una riga

<code>end EasterDate</code>

La cosa interessante è che un blocco di istruzioni <code>on</code> non viene eseguito quando compare nello script, bens&#236; quando c'è un'istruzione che lo chiama (afferra la maniglia, per cos&#236; dire). Si vedrà che nello script il blocco on compare all'inizio, ma viene eseguito solo alla penultima riga dello script.

<code>copy aYear to J</code>

<code>copy</code> è un altro modo di assegnare un valore a una variabile (inserire un contenuto in un contenitore), analogo a <code>set</code>, che conosciamo già. È come dire <em>copia questo valore in questa variabile</em> anziché <em>imposta questa variabile a questo valore</em>.

<code>copy (19 * a + 24) mod 30 to d</code>

<code>mod</code> è il resto di una divisione intera e si attacca a un'altra operazione. Qui, qualunque cosa accada dentro la parentesi viene divisa per 30 e il resto dell'operazione viene copiato dentro la variabile <code>d</code>. Facciamo finta che <code>a</code> valga 7. Succederebbe che <code>19 * 7 = 133</code> e che <code>133 + 24 = 157</code>. Per fare <code>157 mod 30</code> si divide in modalità intera 157 per 30. Il risultato è 5 e il resto è 7. <code>157 mod 30 = 7</code>.

<code>set Easter to ((OT as text) &#38; "/" &#38; OM as text)</code>

<code>as text</code> è un esempio di <em>coercion</em>, coercizione. Si verifica quando abbiamo dati di un tipo (per esempio numeri) e vogliamo gli stessi dati in un'altra forma (per esempio caratteri). Se scrivo 44 dentro la Calcolatrice è diverso dallo scrivere 44 dentro TextEdit, giusto? Qui è la stessa cosa. Nello script in questo momento OT contiene un numero, ma per compiere l'operazione successiva è necessario che quel numero venga interpretato come serie di caratteri, appunto <code>as text</code>, come se fosse testo.

<code>set ThisDate to current date</code>

<code>current date</code>, la data di adesso, è una frase propria di AppleScript, che è a nostra disposizione quando serve, appunto, la data odierna.

<code>set ThisYear to result</code>

Anche <code>result</code> è una parola riservata di AppleScript. Serve a contenere il risultato di un'operazione che vogliamo usare subito dopo, senza doverlo mettere per forza in una variabile.

<code>set dialogresult to display dialog "Inserisci un anno (a quattro cifre)" &#38; return default answer ThisYear buttons {"OK", "Annulla"} default button 1</code>

Finora abbiamo pubblicato qualche finestra di dialogo al minimo sindacale. Questa istruzione mostra che in effetti una finestra di dialogo può fare di più. Si può assegnare una finestra di dialogo a una variabile; si può stabilire il numero dei pulsanti e il loro contenuto, nonché indicare qual è il pulsante di default, quello che è evidenziato e si attiva se premiamo Invio; eccetera. Non mi dilungo perché ci si può giocare facilmente e imparare giocando: come creare una finestra di dialogo con cinque pulsanti? come avere evidenziato il quarto pulsante dei cinque? Le risposte sono tutte dentro questa riga. (<a href="mailto:lux@mac.com?subject=Ho%20un%20dubbio%20sulle%20finestre%20di%20dialogo">Scrivimi</a> se non sono chiare).

<code>set ButtonPressed to button returned of dialogresult</code>

Si può assegnare a una variabile il pulsante premuto in una finestra di dialogo&#8230;

<code>set MyYear to text returned of dialogresult</code>

&#8230;e si può assegnare a una variabile il testo che è stato inserito in una finestra di dialogo. <code>button returned</code> e <code>text returned</code> sono frasi del vocabolario di AppleScript.

<code>set ThisEaster to EasterDate(MyYear) of me</code>

Qui la variabile è <code>ThisEaster</code> (questa Pasqua) e il suo contenuto è l'intera funzione <code>EasterDate</code>, che ha come parametro la variabile <code>MyYear</code>. Questo passaggio è concettualmente un po' difficile lo si capisce solo studiando per bene l'andamento dello script. of me è un'altra frase del vocabolario di AppleScript. Siccome bisogna spiegare allo script dove sta la funzione, in questo caso si può dire semplicemente <code>of me</code>, di me; la funzione sta qui dentro questo stesso script. Questo è concettualmente molto difficile&#8230; ma va affrontato, prima o poi. È un po' come se uno dicesse all'amico <cite>mi prendi il telefonino, per favore? Sta qui nella tasca interna della mia giacca</cite>. Uno sa dove sta il suo telefonino, ma se ha le mani occupate deve chiarire anche l'ovvio.

Adesso lo script nella sua interezza. Lo si dovrebbe poter copiare e incollare comodamente in Script Editor. Se ci fossero problemi, lo mando via mail a chi <a href="mailto:lux@mac.com?Subject=Mi%20mandi%20lo%20script%20per%20favore?">mi scrive</a>.

<code>on EasterDate(aYear)
	copy aYear to J
	copy J mod 19 to a
	copy J mod 4 to b
	copy J mod 7 to c
	copy (19 * a + 24) mod 30 to d
	copy (2 * b + 4 * c + 6 * d + 5) mod 7 to e
	copy 22 + d + e to OT
	copy 3 to OM
	if OT > 31 then
		copy d + e - 9 to OT
		copy 4 to OM
	end if
	if OT = 26 and OM = 4 then
		copy 19 to OT
	end if
	if OT = 25 and OM = 4 and d = 28 and e = 6 and a > 10 then
		copy 18 to OT
	end if
	
	set Easter to ((OT as text) &#38; "/" &#38; OM as text)
	
	return Easter
end EasterDate

set ThisDate to current date
get year of ThisDate
set ThisYear to result

set dialogresult to display dialog "Inserisci un anno (a quattro cifre)" &#38; return default answer ThisYear buttons {"OK", "Annulla"} default button 1
set ButtonPressed to button returned of dialogresult
set MyYear to text returned of dialogresult

set ThisEaster to EasterDate(MyYear) of me

display dialog "La Pasqua per l'anno " &#38; MyYear &#38; " si festeggia il " &#38; ThisEaster
</code>

Un grosso grazie a Carlo!