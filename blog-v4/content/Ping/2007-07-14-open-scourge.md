---
title: "Open scourge*"
date: 2007-07-14
draft: false
tags: ["ping"]
---

È stato presentato il progetto che, chissà come chissà quando, <a href="http://www.portatili-oggi.it/archives/000268.html" target="_blank">farà passare a Linux</a> e all'<em>open source</em> l'infrastruttura informatica della Camera dei deputati. Bene (anche se Microsoft <a href="http://punto-informatico.it/p.aspx?i=2037753" target="_blank">ha espresso soddisfazione</a> e mi suona un campanellino in testa. Nelle altre nazioni Microsoft sbraita, urla, pesta i piedi e minaccia).

Ci sono, pare, 3.500 computer alla Camera, tra uffici e portatili. E ogni combinazione di Windows/Office, pare, costa (e passando a Linux <a href="http://www.e-linux.it/news_detail.php?id=3238" target="_blank">non costerebbe più</a>) circa 900 euro.

Fin qui viene da fare il qualunquista semplice e sbottare: questa gente prende 13 mila euro al mese e ha pure i computer (ben più di uno) a spese nostre?

Ma c'è di peggio. I deputati potranno chiedere di passare a Linux. Perché, oggi farlo è vietato? Se un deputato decidesse di passare a Linux domani mattina, glielo impedirebbero? E se decidesse di usare Mac OS X, verrebbe espulso dall'aula?

Ma uno pensa ai risparmi. Dieci o quindici deputati passeranno a Linux. Saranno trenta computer, per 900 euro a computer qualcosa si risparmia.

Non c'è problema: è stata appena deliberata la fornitura ai deputati di cellulari cifrati antiintercettazione. Uno a testa. 999 euro a testa.

Uno ha la sensazione di essere menato per il naso. Poi legge le cronache e gli passa. Cioè diventa certezza.

* = <cite>Una persona o una cosa che causa grandi problemi o sofferenze</cite>.