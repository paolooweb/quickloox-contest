---
title: "La paginetta dello zio Tom"
date: 2010-08-04
draft: false
tags: ["ping"]
---

Segnalo questa interessante <a href="http://www.tomx-applescripts.hostzi.com/" target="_blank">raccolta di AppleScript</a>.

Particolarmente interessante il primo script, che fa funzionare gli script, volendolo, in background, senza che il Mac ne segnali l'attività.

Purtroppo gli script non sono smontabili. Tuttavia tentare di realizzare uno script simile con i propri mezzi è un bell'esercizio.