---
title: "Due graffietti alla Pantera"
date: 2003-11-13
draft: false
tags: ["ping"]
---

Informazioni di servizio per smanettoni apprensivi

Ad alcuni utenti è successo di effettuare un upgrade da Mac OS X 10.2 a Panther e scoprire che alla fine mancavano alcuni file. Per esempio, digitando

man malloc

nel Terminale non compare la pagina man del comando malloc.

Serve solo agli smanettoni, ma - nel dubbio - per rimediare c’è il sistema: andare in /Library/Receipts, cancellare o rinominare il file BSD.pkg, prendere il Cd numero di installazione di Panther e selezionare la sola installazione dei sottosistemi Bsd.

Un altro errore di installazione è verificabile dando il comando

java -version

Se il risultato è diverso da

java version "1.4.1_01"
Java(TM) 2 Runtime Environment, Standard Edition (build 1.4.1_01-99)
Java HotSpot(TM) Client VM (build 1.4.1_01-27, mixed mode)

C’è un problema, risolvibile consultando <link>questa nota tecnica</link>http://developer.apple.com/technotes/tn2002/tn2099.html.

Se qualcuno non capisce quanto scritto qui sopra, non si preoccupi: molto probabilmente non gli serve capirlo.

<link>Lucio Bragagnolo</link>lux@mac.com