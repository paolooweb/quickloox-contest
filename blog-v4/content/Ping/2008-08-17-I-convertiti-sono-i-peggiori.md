---
title: "I convertiti sono i peggiori"
date: 2008-08-17
draft: false
tags: ["ping"]
---

David Alison è uno sviluppatore web e non è precisamente l'ultimo arrivato.

A un certo punto ha affiancato un MacBook alla sua collaudatissima piattaforma Windows.

In capo a sei mesi, sviluppa più e meglio di prima ed è passato integralmente a Mac, e da un MacBook a un MacBook Pro (il MacBook è passato alla figlia).

Alison ha un sacco di buonissime ragioni da esporre e la cosa interessante è che non sono opinioni, ma fatti sonanti e concreti.

Se riesco, tradurrò un po' del suo <em>post</em> riassuntivo dei <a href="http://www.davidalison.com/2008/08/six-months-after-my-switch-update.html" target="_blank">primi sei mesi</a> posteriori allo <em>switch</em>.

Non si è mai visto un convertito tanto razionale e tanto obiettivo (non mancano i <em>post</em> sui difetti e sui problemi incontrati). Non sarà che la decisione è stata giusta?