---
title: "Un’immagine vale mille parole"
date: 2002-05-23
draft: false
tags: ["ping"]
---

Perché ai preoccupati della propria privacy basta avere un Mac per stare tranquilli

Il programmino si chiama Pict Encrypt 2.0, è gratuito e permette di creare steganografie: messaggi cifrati in cui il messaggio è abilmente nascosto all’interno di una immagine all’apparenza innocua.
Programmini come questo sono un investimento sulla tenuta della nostra privacy. Se un domani avessimo un’urgenza di farci leggere solo da chi vogliamo, basterebbe veramente poco. E non ho neanche toccato il discorso di Pgp.
Forse non mi servirà mai, ma una copietta di <link>Pict Encrypt</link>http://www.pariahware.com/pictencrypt.html nel mio hard disk troverà sempre posto.

<link>Lucio Bragagnolo</link>lux@mac.com