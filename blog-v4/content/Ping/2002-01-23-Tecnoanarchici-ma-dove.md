---
title: "Anche i grandi editori sbagliano"
date: 2002-01-23
draft: false
tags: ["ping"]
---

Un giornalista serio e competente non può firmare un articolo sbagliato. Errore clamoroso a Panorama Next

Su Panorama Next online è comparso un articolo che sicuramente è il frutto di un errore.
Firmato, penso proprio erroneamente, Luca Panerai (che conosco e non scriverebbe mai inesattezze del genere), tratta di una fantomatica “setta” di nome Open World, composta da hacker che “comunicano su speciali chat line private e usano messaggi in codice” per arrivare a una “rete libera, senza padroni, accessibile a tutti” (e che ci sarebbe di male?).
Tra questi “tecnoanarchici”, capaci di “compiere veri e propri attentati”, addirittura viene annoverato Linus Torvall (sic), ideatore di Lynux (strasic), che verrebbe fantascientificamente “distribuito a prezzi stracciati per fare concorrenza a Windows Xp”, quando tutti sanno che è completamente gratuito e non vuole fare concorrenza a nessuno.
Tutto ciò ci interessa perché da un anno e più su Mac esiste Mac OS X, che è basato su Darwin: un sistema operativo open source gratuito che si basa sul libero contributo della comunità mondiale dei programmatori. Apple ha tanti difetti, ma non cerca sicuramente il terrorismo né i compratori di un Mac sono “tecnoanarchici”.
Sicuramente la pubblicazione di questo articolo è frutto di un brutto errore; una casa editrice prestigiosa come Mondadori si mantiene abitualmente su livelli di serietà elevati.
Ricordiamoglielo con una email educata e gentile.

lux@mac.com

<http://panoramanext.mondadori.com/panext/computer_palmari/art006005000163.jsp>
