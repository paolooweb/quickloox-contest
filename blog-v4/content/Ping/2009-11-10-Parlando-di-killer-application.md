---
title: "Parlando di killer application"
date: 2009-11-10
draft: false
tags: ["ping"]
---

La parola a <a href="http://www.sinapsistudio.it/" target="_blank">Giulio</a>:

<cite>Quando ho preso l'oggetto, non credevo che l'avrei trovato indispensabile per questa ragione.</cite>

<cite>Ma con due figli piccoli, è una luce di emergenza eccezionale. Non acceca, illumina diffusamente gli ambienti e non dà fastidio ai bambini.</cite>

<cite>Qualcuno dirà: ci sono oggetti da dieci euro che fanno la medesima cosa. Si. Ma</cite> questo <cite>è configurabile, posso scegliere colore e intensità e soprattutto non è un'altra cosa fra le scatole, con le sue batterie, il suo alimentatore e che non trovi mai al buio quando serve. È un dispositivo che posso tenere ovunque e fa (quasi) tutto bene. Il vero coltellino svizzero dell'era digitale.</cite>

<cite>Unico problema: non sono riuscito a trovare una</cite> app <cite>che cambi i pannolini.</cite>

Io sono soddisfattissimo di come ci <a href="http://itunes.apple.com/it/app/shopper/id284776127?mt=8" target="_blank">faccio la spesa</a>. Banalità, si dirà. Nessun dubbio; prima, però, era un <i>post-it</i> scarabocchiato appiccicato al carrello. Adesso c'è più gusto perfino l&#236;.