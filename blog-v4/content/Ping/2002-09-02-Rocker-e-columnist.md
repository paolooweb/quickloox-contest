---
title: "Rocker e columnist"
date: 2002-09-02
draft: false
tags: ["ping"]
---

Non capita tutti i giorni che a fare il divulgatore Macintosh sia…

Come qualcuno si sarà accorto, su MacCentral c’è una nuova <link>rubrica</link>http://maccentral.macworld.com/news/0206/12.ellefson.php Mac, dedicata al “dominio di Macintosh nel mondo dell’intrattenimento”, firmata non da un Bragagnolo qualsiasi ma da David Ellefson, bassista del gruppo hard rock Megadeth.

Lui, tra l’altro, è un utente fortunato: quando suona il basso, non ha mai problemi con la batteria. :-)))

<link>Lucio Bragagnolo</link>lux@mac.com