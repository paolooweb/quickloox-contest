---
title: "Quanto costa un giorno"
date: 2006-02-02
draft: false
tags: ["ping"]
---

Leggo dal Corriere della Sera: il Comune di Milano <a href="http://www.corriere.it/vivimilano/speciali/2006/02_Febbraio/01/tilt.shtml" target="_blank">bloccato per 36 ore da un virus</a>.

Quanto costano trentasei ore di fermo del Comune di Milano?

Se il Comune di Milano usasse Linux, non si sarebbe fermato.

Se il Comune di Milano usasse Macintosh, non si sarebbe fermato. E il costo di un solo giorno di fermo &egrave; sufficiente a stroncare alla nascita qualunque discussione sui prezzi.

Scriver&ograve; al <a href="http://www.comune.milano.it/posta/index.html" target="_blank">Comune di Milano</a> (adesso, guarda caso, il form d&agrave; errore) e intendo scrivere ai quotidiani. Se hai tempo, fallo anche tu.

Dedicato a chi sostiene che i computer sono tutti uguali. Spero che ieri si sia trovato in coda all&rsquo;anagrafe.