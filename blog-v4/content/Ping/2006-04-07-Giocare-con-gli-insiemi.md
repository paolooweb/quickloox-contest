---
title: "Giocare, con gli insiemi"
date: 2006-04-07
draft: false
tags: ["ping"]
---

Uno dice <em>il boot da Windows è una cosa bella perché così ci girano i giochi Windows</em>.

Uno dice <em>il boot da Windows è una cosa brutta perché nessuno farà più giochi per Mac</em>.

Ma appartengono allo stesso insieme?

Nel frattempo, non c’entra niente, ma <a href="http://www.bigfishgames.com/mac/games.html" target="_blank">Big Fish Games</a> ha rilasciato un bel po’ di versioni Mac dei suoi giochini. Francamente, Mac ha sempre avuto troppi giochi per il mio tempo disponibile. Partendo dal fatto che rubo al sonno diverse ore ogni settimana esattamente per giocare.