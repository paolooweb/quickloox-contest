---
title: "Servire serve"
date: 2007-10-17
draft: false
tags: ["ping"]
---

Da qualche tempo ho abbandonato la ricerca di buoni sostituti di GraphicConverter e ho speso più che volentieri i 19,95 euro di aggiornamento per avere diritto alla versione 6.x.

Li ho spesi attraverso Element 5, il negozio elettronico usato da Lemkesoft.

Non mi hanno inviato la fattura.

Sono andato nella sezione Customer Service e ho trovato il supporto più furbo ed efficace che abbia mai visto in un negozio online.

Un minuto e avevo la fattura su disco, e un sacco di soddisfazione per il problema risolto.

Come diceva non so che programmatore, la cosa difficile non è fare andare bene le cose, ma fare in modo che non possano mai andare male. In tutti gli altri negozi online che ho visto, difficilmente me la sarei potuta cavare senza inviare almeno una mail a qualcuno, o compilare un form. Qui ho fatto tutto da solo in un attimo. Questo s&#236; che è servizio. E di sicuro la prossima volta ci passerò più che volentieri.

Viva <a href="http://www.lemkesoft.de" target="_blank">GraphicConverter</a>. Ed <a href="http://element5.com" target="_blank">Element 5</a>.