---
title: "La pubblicità è l'anima"
date: 2010-03-23
draft: false
tags: ["ping"]
---

Sono molto soddisfatto dal bloccare Flash su Mac, con <a href="http://rentzsch.github.com/clicktoflash/" target="_blank">ClickToFlash</a>.

Invece non ho mai effettuato tentativi di bloccare pubblicità e popup. La pubblicità può essere sgradita e invadente, certo; vuol dire che è un sito malfatto. La misura giusta da adottare è ignorarlo e andare su altri siti, non boicottare la pubblicità.

I siti buoni hanno buona pubblicità. La prova è quella inversa: sulla pagina di accoglienza della concessionaria indipendente <a href="http://decknetwork.net/" target="_blank">The Deck</a> c'è l'elenco dei siti clienti, che ne pubblicano le inserzioni.

Come minimo l'ottanta percento di quei siti è eccellente, il novanta per cento ottimo, il cento percento buono.

Buoni siti, buona pubblicità. E viceversa.