---
title: "Fiducia e sicurezza"
date: 2008-05-14
draft: false
tags: ["ping"]
---

La mail ha oggetto <em>Occhio ti hanno scoperto</em>, a nome di una non meglio identificata Roberta1979.

Il testo:

<cite>Visto che sei tu la persona nelle foto in allegato...,
guardati dalle persone che ti stanno vicine.

un'amica</cite>

Interessante. Guardo l'allegato, DSC0027-31.zip. Lo scompatto.

Dentro ci sono un file DSC0027.dat.jpg, completamente vuoto, e un sedicente NokiaPhotoReader.exe, per Windows.

Se avessi usato Windows, forse il &#8220;Nokia Photo Reader&#8221; sarebbe partito automaticamente. Se anche non fosse, basterebbe un attimo di distrazione o un clic sbagliato per farlo partire.

Invece ho un Mac.