---
title: "Forchetta o forcella"
date: 2008-08-13
draft: false
tags: ["ping"]
---

Cedo subito la parola a <a href="http://www.marcorotatori.com" target="_blank">Marco</a>:

<cite>Saprai sicuramente la differenza fra</cite> data fork <cite>e</cite> resource fork <cite>di un file e il fatto che da Terminale, per esempio, si riesca ad accedere alla sola</cite> fork <cite>dati.</cite>

<cite>Un esempio è il risultato del comando</cite> <code>ls</code> <cite>che riporta il peso della sola</cite> fork <cite>dati o del comando</cite> <code>cat</code> <cite>che ugualmente legge solo quella parte di file.</cite>

<cite>Bene, arrivo al dunque: per accedere alla</cite> <code>fork</code> <cite>risorse è sufficiente aggiungere al</cite> path <cite>(percorso) di un file (file, non cartella) la seguente stringa:</cite>

<code>/..namedfork/rsrc</code>

<cite>Un esempio: il semplice comando</cite> <code>ls -l ./immagine.tif</code> <cite>diventerebbe</cite> <code>ls -l ./immagine.tif/..namedfork/rsrc</code><cite>.</cite>

Dell'eccellente intervento di Marco ho cambiato solo la sua traduzione della parola <em>fork</em> perché in italiano ha principalmente i due significati di cui al titolo e nessuno dei due mi sembra adeguato per descrivere colloquialmente una diramazione di una biforcazione.