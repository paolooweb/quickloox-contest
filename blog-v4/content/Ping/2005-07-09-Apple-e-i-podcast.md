---
title: "Bassa tecnologia, alto stile<p>"
date: 2005-07-09
draft: false
tags: ["ping"]
---

Ricetta per prevedere se una cosa che fa Apple avrà successo<p>

Stavo pensando al podcasting.<p>

Nella tecnologia in sé non c&rsquo;è assolutamente niente di eccezionale o incredibile. È giusto uno sviluppo ragionevole intorno al concetto di Xml e di Rss.<p>

Non è semplicissima da spiegare e però ci vuole un attimo per sviluppare dipendenza.<p>

Per fruirne non serve praticamente niente se non il programma giusto.<p>

Apple l&rsquo;ha inserita dentro un programma di uso comune (iTunes) in modo immediato e facile (non ancora perfettamente facile, ma è già molto).<p>

Avrà un successone.<p>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>