---
title: "Lo sapevano tutti"
date: 2008-06-17
draft: false
tags: ["ping"]
---

Però ho scoperto solo ora la possibilità di selezionare il testo sulla singola colonna di un Pdf grazie alla pressione del tasto Opzione.

Lo sapevano tutti, tranne me. :-)