---
title: "Lista gadget"
date: 2007-11-15
draft: false
tags: ["ping"]
---

Ping-pizzata 2.0 del 23 prossimo venturo: porto sicuramente il libro <a href="http://www.sperling.it/scheda/978882004429" target="_blank">Semplicemente Perfetto di Steven Levy</a>, che mi è piaciuto.

Lo regalo a chi c'è e lo vuole. Se lo vuole di uno solo, visto che ne ho una copia sola, estrazione a sorte. :-)