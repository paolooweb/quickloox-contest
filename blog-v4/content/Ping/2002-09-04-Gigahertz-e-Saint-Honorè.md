---
title: "Gigahertz e Saint Honorè"
date: 2002-09-04
draft: false
tags: ["ping"]
---

Non sono stupidi, sono solo ignoranti. Non facciamo gli ignoranti anche noi e mangiamo meglio

Ieri ho sentito per l’ennesima volta l’ennesima sciocchezza intorno al concetto “più gigahertz = più velocità”.
Basterebbe mangiare più dolci. Prendi la Saint Honorè, ma va benissimo anche una banale margherita: è più gustoso mangiare una torta tagliata in dodici gigafette o in ventiquattro gigafette?

Anche il più ignorante degli utenti Windows obietterebbe subito che dipende da quanto sono grandi le torte, da dove sono i bignè, dalla loro temperatura, se le fette sono troppo grandi e troppo piccole per il piatto, da quello che si è mangiato prima. Può essere allettante anche avere due torte da dodici fette piuttosto che una da ventiquattro, perché possono essere affettate da due persone e quindi servite quasi al doppio della velocità.

La prossima volta che lo senti straparlare, offrigli da bere: ma deve berlo alla maniera dei suoi processori, in una successione frenetica di microscopici e rapidissimi sorsetti. Scoprirà presto, se non è completamente irrecuperabile, che l’efficienza del sistema non dipende solo dalla frequenza del processore ma da come elabora i dati e anche da quello che gli sta intorno.

E se non capisce, tiragli pure la torta in faccia, perché non rimane che riderci su.

<link>Lucio Bragagnolo</link>lux@mac.com