---
title: "Un iPhone per riunirli"
date: 2010-03-21
draft: false
tags: ["ping"]
---

E speriamo farli divertire.

Interessante l'approccio di Plastikman, <i>deejay</i> americano che ha creato <a href="http://itunes.apple.com/us/app/synk/id354695489?mt=8" target="_blank">una applicazione apposta</a> per quanti si troveranno ai suoi concerti quest'estate.

Di musicisti con la loro applicazione ce ne sono infiniti. Che io sappia, però, nessuno ha legato un'applicazione a luoghi e occasioni specifiche. Questa infatti avrà funzioni particolari durante i concerti, con iPhone in collegamento a una rete <i>wi-fi</i> locale. Funzioni assenti fuori dai concerti e lontano dalla loro sede.

L'episodio segna un salto di qualità nella concezione delle <i>app</i>: possono nascere e magari scomparire al volo, quando serve. Uno dei contrassegni del mondo digitale è la possibilità di impiegare i bit in modo effimero, quando nel mondo reale dobbiamo sempre essere oculati.

Sulla qualità della musica di Plastikman non mi pronuncio, né essa dipende da App Store. :-)