---
title: "Il colpevole era&#8230;"
date: 2009-12-24
draft: false
tags: ["ping"]
---

&#8230;il file <code>wp-cron.php</code> di Wordpress. Se ha problemi, o se il server ha un orario che non è impostato correttamente, o se per qualunque motivo gli si sono alterati i permessi di accesso, i post non vengono pubblicati  e compare nel motore l'indicazione <i>Programmazione mancante</i> (<i>Missing Schedule</i>).

Non metto <i>link</i>, una ricerca di <i>programmazione mancante wordpress</i> o <i>missing schedule wordpress</i> offre pagine valide a decine.

Magari aiuta qualcuno. Se ha accesso al server. :-)