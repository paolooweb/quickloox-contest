---
title: "Chi si contenta di Vista non gode"
date: 2007-03-19
draft: false
tags: ["ping"]
---

Comincio a vedere e sentire racconti dell'orrore di gente che ha installato Vista e, non avendo uno schermo compatibile Hdmi e non so che altro requisito, non può vedere i Dvd a piena risoluzione perché il sistema di protezione dei contenuti digitali di Vista non glielo lascia fare.

La cosa che lascia veramente sgomenti è che una roba del genere dovrebbe scatenare la rivoluzione. Invece, a quanto pare, al massimo ci guadagna un po' Apple grazie ai nuovi switcher e niente più.

Non riesco a immaginare la prossima versione di Windows. Ci sarà da chiedere il permesso a Redmond per poter accendere il computer.