---
title: "Mancano scuole"
date: 2008-12-13
draft: false
tags: ["ping"]
---

Dove si insegni l'arte del nuovo web. 

Progettare un sito è diventato importante come progettare un'applicazione, tanto che l'impatto di una pagina mal scritta sulle prestazioni di un sistema diventa significativo persino nell'era dei <em>multicore</em> e dei <em>gigahertz</em>.

Il problema è che la gente ha in mano strumenti potentissimi per infilare di tutto e di più dentro il codice Html. Ma non ha alcuna cultura di come farlo in modo efficiente, a beneficio del visitatore e non solo di sé.

Se succedesse cos&#236; con le applicazioni sarebbe un disastro. Su Internet invece passa tutto.

Quando ho letto <a href="http://inessential.com/?comments=1&amp;postid=3570" target="_blank">questa pagina</a> non ci volevo credere. Non del tutto almeno. Basta però toccare con mano, Monitoraggio Attività e <a href="http://mashable.com/2008/12/07/googles-plan-for-world-domination/" target="_blank">quest'altra pagina</a> aperta. Poi togliere i plugin, poi togliere JavaScript, poi togliere le immagini&#8230;