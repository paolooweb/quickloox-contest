---
title: "Scegli l’evoluzione"
date: 2003-07-08
draft: false
tags: ["ping"]
---

Perché andare per Linux quando c’è Darwin?

Ogni tanto sento di utilizzatori di Mac OS X che, “per imparare Unix”, cercano consigli per l’installazione di Linux su una partizione che avanza o un disco altrimenti inutile.

È inspiegabilmente difficile chiarirgli che sono letteralmente seduti sopra Unix e, a parte il fatto che già solo recuperare tutta la documentazione Unix nascosta nel sistema sarebbe un ottimo esercizio per il principiante, basta installare i Developer Tools per disporre di documentazione sufficiente a fare schiantare di sovraccarico uno sviluppatore professionista.

Evidentemente si tratta di persone che, essendo Mac OS X facile da gestire, cercano un altro sistema operativo per impiegare tempo che altrimenti non saprebbero come riempire.

Ma allora, perché non scegliere Darwin? È un ottimo esercizio, perché lo Unix è esattamente quello di Mac OS X, così imparare serve anche a qualcosa. È un sistema operativo solido e stabile. Ci gira sopra praticamente tutto il software che gira sopra Linux. È gratuito e si trova subito sul sito Apple (apple.com/macosx, cliccare su Darwin).

Linux è un bellissimo sistema, intendiamoci. Ma non è l’unico a esserlo.

<link>Lucio Bragagnolo</link>lux@mac.com