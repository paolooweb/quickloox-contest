---
title: "Un&rsquo;ipotesi sul Think Different"
date: 2006-05-29
draft: false
tags: ["ping"]
---

<strong>Gabriele</strong> chiedeva (grazie!) un mio punto di vista sul concetto di <em>Think Different</em>. Che &egrave; un punto di vista, chiaro; qui non si sono datasheet e specifiche tecniche.

Una cosa che ritengo importante &egrave; quella impercettibile differenza tra <em>Think Different</em> e <em>Think Differently</em>, che &egrave; la stessa in italiano tra pensare differente e pensare differentemente dagli altri. Non &egrave;, ovvero, cos&igrave; automatico che <em>Think Different</em> sia fare diversamente da quello che fanno gli altri.

Per me un esempio di <em>Think Different</em> &egrave; esattamente la cosa che sembra la pi&ugrave; commerciale di tutti: iPod. Non passa giorno senza sentire una critica su quello che manca a iPod. Chi vuole il cellulare, chi la radio, chi il registratore, chi questo, chi quello. E intanto iPod fa il vuoto.

Che cosa meglio di iPod riassume il senso dello spot? I <em>misfits</em>, i <em>rebels</em>, i <em>troublemakers</em>, i <em>round pegs in the square holes</em>. Non &egrave; forse iPod? Non &egrave; forse il grattacapo, la seccatura, il problema pi&ugrave; grosso per Bill Gates, per Microsoft, per il 90 percento del software mondiale, per Sony?

Gi&agrave;, e Macintosh?

Mac &egrave; stata per l&rsquo;appunto creato da gente <em>not fond of rules</em> senza <em>respect for the status quo</em>. Hanno inventato, immaginato, ispirato, creato e soprattutto spinto in avanti l&rsquo;umanit&agrave;. Non sembri una cosa piccola, l&rsquo;interfaccia grafica. Ha davvero cambiato il mondo.

Lo spot dice <em>We make tools for these kinds of people</em>. Facciamo strumenti per queste persone. <em>Because the ones who are crazy enough to think that they can change the world, are the ones who do</em>.

C&rsquo;&egrave; gente che crede che cambiare il mondo significhi occuparsi con molte parole e pochi fatti di cause lontanissime nel tempo e nello spazio e che pi&ugrave; grandi sono le cause, pi&ugrave; si riuscir&agrave; a incidere. &Egrave; l&rsquo;opposto, sorry.

Si cambia il mondo a partire dalla propria famiglia, dal proprio lavoro, dal proprio divertimento, dal proprio sapere. A partire dalla cose pi&ugrave; piccole, da cui nasce l&rsquo;effetto valanga. Apple fa strumenti per questa persone. Chi vuole un cellulare su iPod non vuole cambiare niente; vuole un ennesimo cellulare. Come se avesse una vera utilit&agrave;.

Chi di noi usa un Mac e lavora o ha lavorato in ambienti misti sa benissimo che cosa voglia dire essere un piolo rotondo in un foro quadrato. Un ribelle. Un rompiscatole. <em>Think different</em> &egrave; diventare pi&ugrave; produttivi con Mac OS X pi&ugrave; di quanto chiunque potr&agrave; mai essere con Windows. Chi vuole Windows per accontentarsi di quello che sa invece che scoprire qualcosa di nuovo, beh, forse <em>thinks</em>, ma sicuramente non <em>different</em>. Il lamentoso <em>perch&eacute; non ho il programma cos&igrave; bello che ha il mio amico?</em> non &egrave; certo Think Different. &Egrave; solo incapacit&agrave; di usare un browser e consultare <a href="http://www.versiontracker.com" target="_blank">Versiontracker</a>, oltre che un triste complesso di mancata omologazione al gregge.

Questo &egrave; il mio punto di vista.

Una piccola annotazione finale di puro marketing, che nessuno si ricorda. Lo slogan tipico di Ibm, ai tempi, era <em>Think</em>. <a href="http://www.edork.com/Words/ThinkDifferent.asp" target="_blank">Think Different</a> riprende lo slogan del gigante dell&rsquo;informatica di una volta, e fa notare che c&rsquo;&egrave; qualcosa di diverso. :-D