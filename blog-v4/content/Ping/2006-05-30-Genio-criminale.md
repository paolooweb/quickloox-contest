---
title: "Genio criminale"
date: 2006-05-30
draft: false
tags: ["ping"]
---

Non riesco a guardare la tiv&ugrave;, o i Dvd, o i film sul computer; avere davanti una tastiera e stare a guardare per due ore roba fatta da altri &egrave; un delitto.

Per&ograve; non posso che ammirare la realizzazione ingegneristica di certi aggeggi che ho visto e provato in <a href="http://www.macatwork.net" target="_blank">Mac@Work</a>. Poco pi&ugrave; grandi di un iPod shuffle e guardi il digitale terrestre, con possibilit&agrave; di videoregistrare su disco rigido, a prezzi pi&ugrave; che accessibili. Guardare la tiv&ugrave; sul computer &egrave; un crimine, ma questi sono geni del crimine e quindi meritano riconoscimento per la parte geniale.

ovviamente Mac@Work &egrave; solo pretesto pratico; si trova tutto dovunque e anche <a href="http://www.miglia.com/" target="_blank">su Internet</a>. Ma era per dire che il concetto di televisione come apparecchio ingombrante sta per tramontare definitivamente.