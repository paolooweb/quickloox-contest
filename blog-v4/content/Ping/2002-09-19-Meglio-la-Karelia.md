---
title: "Meglio la Karelia"
date: 2002-09-19
draft: false
tags: ["ping"]
---

Il nuovo Sherlock è solo americano? Esiste un’alternativa

Sherlock 3 non è la terza versione di Sherlock, ma la beta pubblica di Sfrutta In Modo Astuto i Web Services 1.0.

Come tutti i neonati ogni tanto la fa fuori dal pannolino e imparerà le lingue molto in fretta, ma per ora – come tutti i neonati – dorme e strilla.

Fuor di metafora, Sherlock 3 è bellissimo ma non è localizzato o, meglio, non esistono ancora in italiano servizi così efficienti come quelli cui punta la versione USA. O Apple Italia non ha ancora allacciato le necessarie relazioni con i fornitori di contenuti.

Se vuoi approfondire comunque il concetto, ti segnalo <link>Watson</link>http://www.karelia.com/watson, un eccellente shareware di Karelia; è ciò che avrebbe potuto più utilmente essere la prima release di Sherlock 3.

Forse non è stato considerato perché non arriva da NeXT? :)

<link>Lucio Bragagnolo</link>lux@mac.com