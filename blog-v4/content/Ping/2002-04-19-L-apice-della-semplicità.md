---
title: "L’apice della semplicità"
date: 2002-04-19
draft: false
tags: ["ping"]
---

Non tutto è perfetto su Macworld online; ma spesso è una buona notizia

Mi scrive Mirella per farmi notare che:

“Ho notato un paio di volte volte che nel testo on line di ‘Ping’ al posto dell’apostrofo vedo un piccolo ‘1’”.

Cara Mirella,
come tutte le persone che hanno lavorato nell’editoria, preferisco usare le cosiddette virgolette tipografiche, cioè curve, o a ricciolo, o comunque uguali a quelle che vedi sui quotidiani e nei libri. Sono molto più eleganti e appropriate dell’apostrofo dritto tipico della posta elettronica.
Per averle in Html - cosa che tanti sedicenti accatiemmellisti non sanno e potrebbero imparare in due minuti - è sufficiente usare le cosiddette entità: da &#145; a &#148; c’è tutto quello che serve.
La redazione di Macworld online, semplicemente, non ha ancora iniziato ad applicare le entità al testo.
Sembra una mancanza, invece è un buon segno: Macworld online è cresciuto fantasticamente da un anno a questa parte e il fatto che i problemi si limitino agli apostrofi è solo la prova che, dopo avere fatto le cose grandi, con pazienza e impegno si arriverà anche a quelle piccole.
Sono bravi. :-)

<link>Lucio Bragagnolo</link>lux@mac.com