---
title: "Un Wi-Fi per tutti"
date: 2011-01-26
draft: false
tags: ["ping"]
---

Ho qualche ragione di credere che le reti wireless liberamente accessibili all'interno di qualunque Apple Store abbiano tutte lo stesso nome in tutto il mondo. Cos&#236;, se sei stato nell'Apple Store di Sidney, usando il suo Wi-Fi, e poi entri in quello di Toronto, il tuo apparecchio si collega al volo senza bisogno di altri aggiustamenti.

Qualcuno che è stato in due o più Apple Store e si ricorda è in grado di confermare o smentire? Appartengo al primo insieme ma sono fuori al momento dal secondo.

La qualche ragione di credere proviene da questo simpatico <a href="http://db.tidbits.com/article/11907" target="_blank">articolo su Tidbits</a>.