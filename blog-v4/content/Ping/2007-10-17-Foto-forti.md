---
title: "Foto forti"
date: 2007-10-17
draft: false
tags: ["ping"]
---

Dimenticavo: le foto che ho scattato molto al volo a Forte dei Marmi, durante l'esposizione in trasferta al museo Apple, sono visibili su una <a href="http://web.mac.com/lux/iWeb/P-Files/Forte%20dei%20Marmi.html" target="_blank">paginetta del mio .Mac</a>.

È tutto buttato l&#236; e grezzo, dovrei titolare le foto e buttare via quelle non buone. Forse, un giorno. :-)

Piuttosto: sono particolarmente orgoglioso di avere fornito in tempo reale l'Olivetti M10 e di essere finalmente riuscito a mettere fianco a fianco il mio Newton e iPhone.

Con la differenza che l'M10 l'ho lasciato al museo, ma il Newton è tornato insieme a me.