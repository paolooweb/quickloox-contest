---
title: "Figli e figliastri?"
date: 2008-11-21
draft: false
tags: ["ping"]
---

Spiega John Gruber su Daring Fireball come <a href="http://itunes.apple.com/WebObjects/MZStore.woa/wa/viewSoftware?id=284815942&amp;mt=8" target="_blank">Google Mobile App</a> (link a iTunes Store) per iPhone, che permette di impostare la ricerca a voce invece che digitarla, <a href="http://daringfireball.net/2008/11/google_mobile_uses_private_iphone_apis" target="_blank">utilizza una interfaccia di programmazione di iPhone privata</a>.

Accordo sopra le regole tra Apple e Google, oppure semplicemente Google ha chiesto e Apple ha approvato, come chiunque altro potrebbe? Sarà interessante osservare l'evoluzione dello sviluppo software di iPhone. È dai tempi del primo Macintosh che non nasceva una piattaforma cos&#236;.