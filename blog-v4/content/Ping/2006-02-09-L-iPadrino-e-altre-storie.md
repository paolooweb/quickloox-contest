---
title: "L&rsquo;iPadrino e altre storie"
date: 2006-02-09
draft: false
tags: ["ping"]
---

<strong>Jacopo</strong> mi ha segnalato (grazie!) il <a href="http://www.mikeindustries.com/blog/archive/2006/01/ipod-winner-7" target="_blank">concorso</a> a chi creasse la migliore falsa locandina cinematografica avente come tema iPod.

La gara si &egrave; conclusa da tempo ma riguardarsi le opere &egrave; un vero piacere. Davanti all&rsquo;iPodfather (Godfather nell&rsquo;originale) non riesco a trattenere le risate, ma c&rsquo;&egrave; tanto altro materiale interessante.