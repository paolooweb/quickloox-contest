---
title: "Il succo della comunicazione"
date: 2006-11-24
draft: false
tags: ["ping"]
---

Quelli che ti danno tutto, ti fregano. Il favore te lo fa chi seleziona il meglio per te e ci riesce bene.

Questa lezione arriva da <strong>Pierfausto</strong>, che segnala una pagina molto ben fatta di <a href="http://www.opensourcemac.org/" target="_blank">software open source</a> per Mac. Poca roba, ma quella giusta, che chiunque dovrebbe provare. Benissimo reclamare che il software per Windows è più numeroso, ma ha senso sostenerlo perlomeno quando si è provata seriamente almeno una alternativa per ciascuna applicazione.