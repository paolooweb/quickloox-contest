---
title: "Che cosa non fa un&rsquo;Olimpiade"
date: 2006-03-23
draft: false
tags: ["ping"]
---

Su <a href="http://maps.google.com/" target="_blank">Google Local</a> L&rsquo;Italia non &egrave; ancora mappata fuori dalle immagini satellitari, con l&rsquo;eccezione del Piemonte e di met&agrave; Liguria. &Egrave; interessante chiedersi quale evento potrebbe portare al completamento di altre porzioni di mappa. :-)