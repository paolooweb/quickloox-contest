---
title: "Te la faccio io la ricerca di mercato"
date: 2006-10-19
draft: false
tags: ["ping"]
---

Se penso alle chiacchiere spese in questi anni sulla fatidica quota di mercato, viene davvero da ridere.

I risultati finanziari di Apple sono stati per l'ennesima volta i migliori della storia, con fatturato trimestrale che nel 1997 l'azienda faceva a malapena in dieci mesi. L'augusta Idc viene a dire che in Usa la quota di mercato di Apple è il 5,8 percento.

Interessante? Boh. Per Gartner è il 6,1. Nel mondo, Idc colloca Apple al <a href="http://www.macworld.it/showPage.php?template=notizie&amp;id=11322" target="_blank">2,7 percento, Gartner al 2,8</a>. O forse è il contrario. Fosse il 2,5, o il 2,9, cambierebbe qualcosa? Non cambia niente. Un dato irrilevante. Che conta solo per la gente che, come dice la suocera, ha tempo a sufficienza per stare a pettinare le bambole.