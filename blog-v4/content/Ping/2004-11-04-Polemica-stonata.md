---
title: "Polemica stonata<p>"
date: 2004-11-04
draft: false
tags: ["ping"]
---

La Fimi sbotta per l&rsquo;iTunes Music Store ed è una sonora balla<p>

Me ne occupo un po&rsquo; in ritardo, ma è gustosa. Secondo <a href="http://webnews.html.it/news/2406.htm">Webnews</a>, La Fimi (Federazione Industria Musicale Italiana) avrebbe attaccato Apple subito dopo l&rsquo;apertura dell&rsquo;iTunes Music Store per &ldquo;non avere aperto alcuna trattativa con le etichette indipendenti&rdquo;.<p>

A parte l&rsquo;assurdità della situazione per cui una federazione parla a nome degli indipendenti (che indipendenti sono, allora? Ma in Italia sono cose normali), ci sono collaboratori di Macworld che stanno facendo esattamente consulenza a etichette indipendenti che vogliono entrare nell&rsquo;iTunes Music Store, ergo la sparata della Fimi è una balla.<p>

A che scopo? Un quarto d&rsquo;ora di visibilità in più, approfittando di un nome più grosso del tuo. Poca roba, ma evidentemente si accontentano.<p>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>