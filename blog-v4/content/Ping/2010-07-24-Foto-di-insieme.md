---
title: "Foto di insieme"
date: 2010-07-24
draft: false
tags: ["ping"]
---

Traduco quasi integralmente questo <a href="http://www.tuaw.com/2010/07/23/the-iphone-4-and-a-mac-on-a-photography-trek/" target="_blank"><i>post</i> da The Unofficial Apple Weblog</a> per il suo significato in giorni dove sembra che tutto si riduca a quanto prende l'antenna e alle custodie gratis. Inoltre si legge qualche trucco interessante per gli appassionati di fotografia, probabilmente anche per i fotografi non appassionati.

<cite>Fino a un recente viaggio nel <a href="http://en.wikipedia.org/wiki/Canyon_de_Chelly_National_Monument" target="_blank">Canyon de Chelly</a>, Arizona nordorientale, non avevo compreso pienamente come i prodotti Apple siano diventati importanti nel mio flusso di lavoro [&#8230;] e di come le cose siano radicalmente cambiate da quando avevo una fotocamera Canon a pellicola, uno zaino pieno di obiettivi e pellicola Fuji a volontà. [&#8230;]</cite>

<cite>La gran parte delle mie immagini transita da</cite> hardware <cite>Apple e programmi Apple o indipendenti. Porto in viaggio con me il mio MacBook Pro e scarico le immagini da una scheda Compact Flash; posso scorrere le immagini in <a href="http://www.apple.com/it/aperture/" target="_blank">Aperture</a> o <a href="http://www.apple.com/it/ilife/iphoto/" target="_blank">iPhoto</a>.</cite>

<cite>In questo viaggio, siccome i</cite> canyon <cite>erano pieni di ombre profonde, ho deciso di scattare alcune immagini con la tecnica <a href="http://www.stuckincustoms.com/hdr-tutorial/" target="_blank">Hdr</a>: si combinano tre o più immagini riprese con diversi tempi di posa. Uso <a href="http://www.hdrsoft.com/" target="_blank">Photomatix</a> per Mac, che svolge un ottimo lavoro di allineamento delle immagini e <a href="http://www.secondpicture.com/tutorials/photography/tone_mapping.html" target="_blank">mappatura delle tonalità</a> per darmi una eccellente gamma dinamica. Non è una tecnica per tutti, ma usata con mano leggera può rendere in modo davvero eccellente paesaggi straordinari.</cite>

<cite>La qualità fotografica di iPhone 4 è stata una sorpresa. Chiaramente non lo uso come fotocamera principale, ma sono rimasto sorpreso da alcuni scatti presi con Pro Hdr,</cite> app <cite>da 1,59 euro che combina due immagini con differente esposizione. Sono rimasto impressionato e si possono vedere i risultati nella</cite> gallery <cite>dell'articolo.</cite>

<cite>Ho ricevuto una sorpresa piacevole anche da quanto sia risultata utile l'</cite>app <cite>Navigon. Nel</cite> canyon <cite>ha scelto un percorso migliore di quello del mio navigatore. Mi sono piaciuti i panorami 3D delle</cite> mesa <cite>e delle</cite> butte <cite>che scorrevano sullo schermo e ho anche usato la funzione Navigon Google per localizzare i luoghi più prossimi per rifornimento e cibo.</cite>

<cite>Una delle cose che mi piacciono di iPhone è l'inserimento dei dati di posizione geografica e altitudine nei metadati dell'immagine. La mia Canon 5D ha più di cinque anni e non lo fa. Ma ancora una volta ci ha pensato iPhone 4. Un'</cite>app <cite>chiamata PlaceTagger mi ha permesso di tenere nota della mia posizione durante il giorno e gli orari in cui mi trovavo in ciascuna località. Sincronizzato l'orologio interno della Canon con quello di iPhone, ho architettato un trucco. Tornato a casa, ho acceso PlaceTagger su iPhone, che grazie alla rete Wi-Fi si è collegato alla copia di PlaceTagger su Mac. Ho aperto la cartella delle immagini e il software ha inserito nelle foto la giusta località in funzione dell'ora dello scatto. Anni dopo posso consultare i dati dell'immagine e risalire al punto esatto in cui è stata ripresa. Ci sono soluzioni hardware a questo problema, tutte però più costose dei 7,99 euro di PlaceTagger, che nella nuova versione supporta pure il multitasking di iOS 4. [&#8230;]</cite>

<cite>Esistono altre</cite> app <cite>per tenere nota della posizione geografica e non mancano le soluzioni Hdr per iPhone e Mac. Ho parlato delle soluzioni che uso io e con cui mi trovo molto bene.</cite>

<cite>Tutto sommato, la fotografia ha attraversato proprio una rivoluzione e devo ringraziare Apple, cos&#236; come i produttori indipendenti, per avere cambiato le cose in meglio.</cite>

Mica per altro, ma ancora dopo tre anni si parla di iPhone come se contasse solo l'aspetto cellulare.