---
title: "L'Impero subisce ancora"
date: 2010-02-11
draft: false
tags: ["ping"]
---

Strepitoso, ancorché non semplicissimo, trucco in arrivo da Mac OS X Hints.

Supponiamo di volere entrare in <a href="http://www.startrekonline.com/frontpage" target="_blank">Star Trek Online</a> e restarci male perché i suoi programmatori sono del secolo passato e conoscono solo Windows.

Se il Mac è Intel, esiste una straordinaria soluzione <i>open source</i> chiamata <a href="http://wineskin.sourceforge.net/" target="_blank">Wineskin</a>, praticamente un imballaggio virtuale che permette di prendere un programma Windows e farlo funzionare su Mac OS X senza altri fastidi (niente virtualizzazioni, niente partizioni, niente Boot Camp eccetera).

Si scarica il <a href="http://dl.dropbox.com/u/4486028/STO/STO%20Unofficial%20Mac%20Beta4.zip" target="_blank">file di configurazione di Star Trek Online per Wineskin</a> e il gioco è quasi fatto.

Raccomandazione necessaria: il tutto è abbondantemente <i>beta</i> e, se qualcosa va storto, bisogna andare sul tecnico. Il <i>forum dedicato</i> aiuta ma risolve solo se si è all'altezza del problema.

Solo questione di tempo, però, perché si possa efficacemente bagnare il naso ai limitati programmatori di Star Trek Online e dare un altro colpetto alla Morte Nera che vuole un solo sistema per tutti e giochi solo per quel sistema.

Il <a href="http://www.macosxhints.com/article.php?story=20100206160723992" target="_blank">pezzo originale</a> è a disposizione per chi volesse accusarmi di non avere aggiunto niente di mio, cos&#236; c'è anche la prova. :-)