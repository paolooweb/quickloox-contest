---
title: "Bug university III"
date: 2009-11-03
draft: false
tags: ["ping"]
---

A seguito del <a href="http://www.macworld.it/blogs/ping/?p=3031" target="_blank">problema con BBEdit</a> di cui ho raccontato, ho chiesto a  Libro Font la validazione di tutti i font installati.

Mi aspettavo qualcosa, ma non a questo livello. Se Leopard dava un giro di vite sulla qualità dei font, segnalando difetti e problemi in molti caratteri che Tiger digeriva senza fare un plissé, Snow Leopard è ancora più rigoroso.

Su quattrocento font abbondanti, circa il dieci percento dava problemi non gravi, da semaforo giallo, e una dozzina invece sono stati segnalati come da semaforo rosso e assolutamente non graditi.

Ho purgato tutti, gialli e rossi, e alla prima occasione riproverò a fare funzionare BBEdit con le Preferenze di Sistema sulla lingua inglese con il Finder impostato in lingua italiana, la stessa combinazione che causava il problema dopo il <i>crash</i> del disco.

Se viene fuori che era colpa di un font malandato, significherà che è ora di starci ancora più attenti del consueto.