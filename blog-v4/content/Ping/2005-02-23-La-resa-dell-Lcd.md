---
title: "La resa dell&rsquo;Lcd<p>"
date: 2005-02-23
draft: false
tags: ["ping"]
---

Un esperimento di luddismo intenzionale giunge a conclusione<p>

Ho ceduto. Mi serviva la condivisione della connessione Internet, che Panther ha ma Puma no, e ho aggiornato a Panther l&rsquo;iMac Lcd 15&rdquo; di casa.<p>

Da almeno due anni l&rsquo;iMac in questione funzionava tranquillo, regolarmente aggiornato&hellip; a Mac OS X 10.1.5. Ciononostante ha masterizzato tutti i miei backup (lui è il SuperDrive ufficiale) e ha svolto perfettamente tutte le sue funzioni. Accessorie, certo, ma non era certo una macchina trascurata.<p>

In quanto macchina secondaria aveva uptime formidabili, di mesi addirittura, escludendo gli update di Apple.<p>

Dopo avere installato Panther ho scaricato anche quei 150 mega di aggiornamento che si sono accumulati e l&rsquo;iMac va tranquillo come sempre, forse anche un pizzico più veloce.<p>

Morali: macchina vecchia fa comunque buon brodo; aggiornare una macchina non pasticciata è un&rsquo;operazione di routine.<p>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>