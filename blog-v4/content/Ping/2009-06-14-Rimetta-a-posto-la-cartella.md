---
title: "&#8220;Rimetta a posto la cartella&#8221;"
date: 2009-06-14
draft: false
tags: ["ping"]
---

Apple ha approntato la pagina che elenca <a href="http://www.apple.com/it/macosx/refinements/enhancements-refinements.html" target="_blank">le novità di Snow Leopard</a>, solo che non l'ha tradotta. Faccio il supplente, una novità per volta.

<b>Riporta gli elementi cancellati nelle cartelle originarie</b>

Se cestiniamo un elemento e poi cambiamo idea, possiamo rimetterlo nella sua posizione originale. È sufficiente selezionarlo nella cartella Cestino e scegliere Indietro dal menu Archivio.

Era uno dei principali motivi di insoddisfazione dei nostalgici di Mac OS 9. Ora è&#8230; cestinato.