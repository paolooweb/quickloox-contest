---
title: "Il Jobs che ride<p>"
date: 2005-10-13
draft: false
tags: ["ping"]
---

Sembra quasi umano e fa pure le cose giuste<p>

Se non l&rsquo;hai ancora fatto, guarda il <a href="http://stream.apple.akadns.net/">discorso di Steve Jobs</a> in cui sono stati annunciati il nuovo iMac, il nuovo iPod e quant&rsquo;altro.<p>

Uno dei migliori degli ultimi anni. Jobs tranquillo ed energetico come sempre, capace di sorridere e fare battute a un livello credo senza precedenti.<p>

Il momento in cui si mette in ridicolo con PhotoBooth vale dieci anni di chiacchiere su Apple e Microsoft. Bill Gates non lo farebbe mai, né avrebbe il fegato di definire un programma <cite>l&rsquo;applicazione più divertente che abbiamo mai scritto</cite>.<p>

Oltretutto sta facendo le cose bene. Apple ha fatto un altro trimestre record, vende Macintosh e iPod in quantità sempre maggiori, ha superato gli otto miliardi di dollari di denaro in cassa o a breve termine. Ha anche mandato a quel paese gli analisti che avevano fatto previsioni surreali sull&rsquo;andamento dell&rsquo;azienda. Le ha definite <cite>nutso</cite>, che dovrebbe suonare anche se non sai l&rsquo;inglese, ed è stato fin troppo diplomatico.<p>

Il signore che ha fatto produrre il mio computer è un essere umano, che sa ridere ed essere umano anche sul palcoscenico. Io sono contento.<p>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>