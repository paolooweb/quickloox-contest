---
title: "Un unico Neo"
date: 2008-05-26
draft: false
tags: ["ping"]
---

A chi interessa: venerd&#236; prossimo 30 giugno, a Milano, in <a href="http://atworkgroup.net" target="_blank">@Work</a>, giornata interamente dedicata a <a href="http://www.neooffice.org/neojava/it/index.php" target="_blank">NeoOffice</a>, con la presenza di Patrick Luby e Ed Peterlin, i due autori del software.

Si comincia dalle 11 e, pausa pranzo a parte, si va avanti fino alle 20. Sessioni sulla <em>roadmap</em> di NeoOffice, chiacchierate (con <a href="http://www.mogulus.com/freesmug" target="_blank">diretta video</a> per il resto del mondo) con Patrick e Ed, anticipazioni sulle funzioni future del programma, domande e risposte, <em>merchandising</em> e altro ancora. Naturalmente nei momenti di noia o per fare una pausa c'è intorno il punto vendita, con un bel MacBook Air esposto e un sacco di altra roba.

Alle 18 è previsto un evento speciale con la partecipazione del sottoscritto, di Paolo Attivissimo, di Fabrizio Venerandi, di Carlo Gandolfi di <a href="http://freesmug.org" target="_blank">FreeSmug</a> e non solo. Non so che cosa succederà, ma credo sarà divertente.

Alle 20, per chi ha voglia, pizza a <a href="http://maps.google.com/maps/ms?ie=UTF8&amp;hl=en&amp;msa=0&amp;msid=118384393375884126533.00043d1ca52504375bd64&amp;z=17" target="_blank">Bio Solaire</a>, via Terraggio 20, a duecento metri da @Work.

Se ho mancato qualche dettaglio, c'è una <a href="http://neowiki.neooffice.org/index.php/NeoOffice_Event_-_Milan%2C_Italy_-_30_May_2008" target="_blank">pagina ufficiale dell'evento</a>.

Io sarò l&#236; credo da ora di pranzo, se non prima ancora. Non capita tutti i giorni di avere a che fare con due sviluppatori veri!