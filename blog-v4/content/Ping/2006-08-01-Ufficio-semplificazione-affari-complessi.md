---
title: "Ufficio semplificazione affari complessi"
date: 2006-08-01
draft: false
tags: ["ping"]
---

Questo periodo agostano mi sembra ideale per segnalare <a href="http://hresume.org/" target="_blank">hResume</a>, grazie a <a href="http://www.rabellino.it/blog" target="_blank">Gianugo</a> e <a href="http://www.afhome.org/" target="_blank">Francesco</a>. Si tratta ancora di una soluzione più complicata di quella che mi piacerebbe, per trattare problemi come un formato condiviso per i curriculum. In tutti i casi è molto più sensato delle cose terribili partorite con i nostri soldi dall'Unione Europea e ha un grande potenziale di crescita.