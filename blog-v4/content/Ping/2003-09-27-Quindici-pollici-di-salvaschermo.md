---
title: "Quindici pollici da salvaschermo"
date: 2003-09-27
draft: false
tags: ["ping"]
---

Toccato con mano il nuovo PowerBook 15”, è veramente bello

Il PowerBook 17” non è affatto ingombrante come si dice, anche se è cinque centimetri più largo del PowerBook 15”. E fino a ieri per me era il portatile Apple più bello (de gustibus, ovvio).

Ma ho visto, toccato, preso in mano, usato il nuovo PowerBook 15” in alluminio, che ha le stesse linee del 17” (e ovviamente ha le dimensioni da 15”). Il mio credo nel 17” – che nelle intenzioni, un giorno, sarà il mio prossimo portatile – è ancora solido ma ha subito un grosso scossone.

Il 15” alluminio è proprio bello, elegante e anche veloce.

Per vedere in anteprima alcuni particolari, un trucco: da Preferenze di Sistema, impostare il salvaschermo su .Mac; cliccare su Configura; dove si scrive il nome dell’utente di .Mac, scrivere “fabol”.

Courtesy of <link>Mac@Work</link>http://www.macatawork.net (su cui vanto un orgoglioso conflitto di interessi)

<link>Lucio Bragagnolo</link>lux@mac.com