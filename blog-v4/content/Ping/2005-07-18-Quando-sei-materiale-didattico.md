---
title: "Quando sei materiale didattico<p>"
date: 2005-07-18
draft: false
tags: ["ping"]
---

Ritornare ragazzi per un attimo, con la tecnologia in mano, è stato divertente<p>

Non vorrei scendere eccessivamente nei dettagli, ma nel corso di una sessione d&rsquo;esame all&rsquo;Accademia di Brera di Milano sono stato chiamato in causa come materiale di contorno alla dispensa dell&rsquo;esame stesso.<p>

E così mi sono presentato nei locali della prestigiosa istituzione armato di PowerBook.<p>

A un certo punto i corridoi si sono completamente svuotati; tutti gli studenti si sono buttati nell&rsquo;aula a vedere che cavolo stava succedendo, attirati dall&rsquo;audio a tutto volume e poi dal resto degli effetti speciali.<p>

Non era certo merito mio (chiunque avrebbe ottenuto lo stesso effetto) ma posso garantire che l&rsquo;hardware ha suscitato interesse almeno quanto l&rsquo;oggetto della dimostrazione.<p>

A dire che Apple, o chi per lei, dovrebbe stipendiare gente che vada in giro per le università con un PowerBook in mano.<p>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a><a href="mailto:lux@mac.com">Lucio Bragagnolo</a>