---
title: "Dolce giocare"
date: 2006-09-24
draft: false
tags: ["ping"]
---

E per qualcuno i giochi online come <a href="http://www.wow-europe.com" target="_blank">World of Warcraft</a> <a href="http://www.flickr.com/photos/ggranieri/198666851/" target="_blank">alienerebbero</a> la gente dal mondo reale. Sono un <em>di più</em>, non un <em>invece</em>.

A proposito: con il mio rogue sto per arrivare al livello 60. Mi piace il cioccolato&#8230; :)

Grazie a <a href="http://www.leledainesi.com/" target="_blank">Lele</a>!