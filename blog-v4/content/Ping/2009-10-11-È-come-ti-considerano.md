---
title: "È come ti considerano"
date: 2009-10-11
draft: false
tags: ["ping"]
---

Steve Jobs e Steve Ballmer sanno benissimo che, se scrivono una email ai dipendenti, diventa rapidamente pubblica. È un modo di comunicare cose al grande pubblico senza il crisma della comunicazione ufficiale.

Dopo la partenza pessima di MobileMe, l’11 luglio 2008, a seguito della quale molti ebbero problemi e per qualcuno vi furono perdite di dati, Jobs scrisse <a href="http://www.mobilecomputermag.co.uk/20080806777/steve-jobs-mobileme-failure-email-full-text.html" target="_blank">una email ai dipendenti</a>. Riconobbe che il lancio di MobileMe non era stato il momento migliore di Apple, che il prodotto non era all’altezza degli standard dell’azienda e che il lancio avrebbe dovuto avvenire in modo diverso.

Annunciò inoltre un nuovo responsabile del servizio e concluse che Apple aveva molto da imparare sui servizi Internet. E che avrebbe imparato.

Comunicazione ai dipendenti, che però era diretta al mondo.

Microsoft sta avendo grossi problemi con il servizio online Sidekick e ha diramato <a href="http://forums.t-mobile.com/tmbl/?category.id=Sidekick" target="_blank">un annuncio</a> in cui chiede ai proprietari di non spegnere il loro apparecchio, né resettarlo, né levargli la batteria fino al termine della buriana.

Infatti il servizio online è saltato e lo stesso annuncio spiega che, per molti, i dati sono da considerarsi quasi certamente persi.

Adesso aspettiamo la <i>email</i> di Ballmer e vediamo che cosa Microsoft spiegherà ai dipendenti per parlare in realtà agli utilizzatori.

Perché i dati li perdono tutti. La differenza sta nel come vieni considerato.