---
title: "Tra il dire e il fare"
date: 2009-09-02
draft: false
tags: ["ping"]
---

Gli sviluppatori di applicazioni Android (il sistema operativo <i>open source</i> di Google per computer da tasca e <i>smartphone</i>) si lamentano: non fanno abbastanza soldi.

Google ha aperto Android Market a imitazione di App Store. <a href="http://metrics.admob.com/wp-content/uploads/2009/08/AdMob-Mobile-Metrics-July-09.pdf" target="_blank">AdMob sostiene</a> che il negozio online vende per cinque milioni di dollari al mese; App Store ne fa duecento (125 su iPhone, 73 su iPod touch, totale 198). Android Market fa un quarantesimo delle vendite di App Store, ma avrebbe un quindicesimo degli utenti (tre milioni contro 45). In breve, <i>App Store è quasi tre volte meglio di Android Market nel vendere applicazioni</i>. Quando un negoziante è tre volte meglio di un altro, l'altro ha qualche problema.

In due mesi su App Store, <a href="http://itunes.apple.com/WebObjects/MZStore.woa/wa/viewSoftware?id=284653044&amp;mt=8" target="_blank">Trism</a> ha incassato 250 mila dollari. In tre mesi su Android Market, non più di 1.500 dollari, centosessanta volte meno, dieci volte peggio considerando il bacino di utenza. La merce è la stessa; c'è qualche problema con lo scaffale.

LarvaLabs, sviluppatore Android con due giochi in catalogo situati in ottima posizione nell'elenco delle applicazioni a pagamento (numero 5 e numero 12, quest'ultima al primo posto per molto tempo e valutata cinque stelle su cinque), <a href="http://larvalabs.com/blog/iphone/android-market-sales/" target="_blank">ha rivelato</a> di vendere complessivamente per 62 dollari al giorno. Secondo Tap Tap Tap, un'applicazione al quinto posto su App Store <a href="http://www.taptaptap.com/blog/convert-1-0-1-in-review-holy-shit/" target="_blank">può incassare 3.500 dollari al giorno</a>.

Sembra che aprire un negozio <i>online</i> di applicazioni non sia facile come dirlo, nel momento in cui deve anche funzionare. App Store ha molti lati criticabili, ma ha aperto un anno prima di tutti e altri, pur potendo copiare il già fatto, stentano. Forse Apple non solo ha fatto, ma ha fatto meglio.