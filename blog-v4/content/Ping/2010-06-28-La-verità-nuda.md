---
title: "La verità nuda"
date: 2010-06-28
draft: false
tags: ["ping"]
---

<cite>Html5 è il futuro. [&#8230;] I</cite> browser mobile <cite>eseguono Html5 molto bene. Flash rallenta tutto e impatta sulla durata delle batterie. [&#8230;] Stiamo aspettando che i</cite> browser <cite>siano tutti pronti e poi sposteremo tutto su Html5. [&#8230;] È solo questione di tempo [&#8230;] È il prossimo passaggio di testimone.</cite>

A <a href="http://www.conceivablytech.com/1553/business/an-unexpected-apple-ally-porn-industry-to-drop-flash/" target="_blank">dichiarare tutto questo</a> è Ali Joone, fondatore e direttore di Digital Playground, uno dei maggiori produttori americani di pornografia.

Qualunque sia l'atteggiamento personale verso i materiali per adulti, su una cosa non c'è il minimo dubbio: è un'industria cinica e pragmatica che va diritta al punto, non fa sconti a nessuno e mira a raggiungere il maggior numero di clienti nel modo più efficiente possibile.

Ironia della sorte, Steve Jobs <a href="http://techcrunch.com/2010/04/19/steve-jobs-android-porn/" target="_blank">ha scritto</a> <cite>chi vuole il porno può comprarsi un telefono Android</cite>.