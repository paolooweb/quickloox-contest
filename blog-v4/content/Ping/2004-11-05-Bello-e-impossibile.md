---
title: "iPod da fantascienza<p>"
date: 2004-11-05
draft: false
tags: ["ping"]
---

Non basta che sia più bello, deve essere impossibile<p>

Esiste una categoria di persone abituata a chiedere sempre l&rsquo;impossibile sperando di ottenere qualcosa più degli altri. Atteggiamento del tutto legittimo nella vita, diventa insopportabile nel mondo della tecnologia.<p>

Prendiamo iPod. Quando è uscito costava troppo. Quando i prezzi si sono ridotti, era troppo pesante. Quando è uscito iPod mini, leggerissimo, era troppo costoso rispetto alla capienza (lo stesso identico disco rigido, acquistato da solo sul mercato, costava più di un mini tutto intero).<p>

Adesso sono arrivati gli iPod Photo, che hanno lo schermo a colori, per poter visionare le foto contenute. I soliti amanti dell&rsquo;impossibile hanno già iniziato a lamentarsi. Costa troppo, le batterie durano meno (certo; lo schermo usato a colori consuma di più, perché siamo nel mondo reale).<p>

Ma il massimo arriva quando si lamentano che vorrebbero usarlo per vedere anche i film. L&rsquo;idea di guardarsi un film sullo schermo di iPod è stupida almeno quanto quella di guardarlo sullo schermo di un cellulare.<p>

Ma l&rsquo;amante dell&rsquo;impossibile ha la risposta pronta: basterebbe equipaggiare iPod di una scheda video da computer, di una porta monitor esterna, di un disco rigido più grande e di batterie più potenti per risolvere il problema.<p>

Ovviamente il prezzo dovrebbe essere inferiore a quello attuale.<p>

Se di iPod se ne comprassero uno. Almeno sarebbero impegnati a usarlo.<p>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>