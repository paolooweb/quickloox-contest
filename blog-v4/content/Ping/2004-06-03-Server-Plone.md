---
title: "Cloni e Ploni"
date: 2004-06-03
draft: false
tags: ["ping"]
---

Il mondo open source inizia a muoversi seriamente anche su Mac

Fabio D’Amico, del network SiR - Scuole in Rete, segnala la nascita di un nuovo <link>server Plone</link>http://freesmug.mine.nu/ che si candida a punto di riferimento per chi si interessa di free software e open source.

Due campi che, prima di Mac OS X, nella sfera Apple ristagnavano a dire poco. Ora, con Mac OS X, sono in piena fioritura e non solo perché è bella stagione.

Ed è bello vedere sorgere anche iniziative originali italiane.

<link>Lucio Bragagnolo</link>lux@mac.com