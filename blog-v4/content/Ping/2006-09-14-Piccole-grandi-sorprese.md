---
title: "Piccole grandi sorprese"
date: 2006-09-14
draft: false
tags: ["ping"]
---

Vedendo dimostrare iTunes 7, che per pigrizia non ho ancora messo alla prova di persona, si constata che non è più un programma, ma sta diventando un vero centro di gestione dei media (il primo che dice <em>media center</em> gli faccio usare il Terminale e basta per un mese). Non mi stupirei se in tre anni da ora la next big thing fosse un oggetto intermedio tra un Mac e un iPod, come un computer specializzato che esegue solo iTunes ed è adibito esclusivamente ai media.

Secondo, si possono spostare i brani da iPod a iTunes e non lo avrei mai detto, prima.

Terzo, il dimostratore di Apple sta confermando in questo momento che nel 2007 l'iTunes Store inizierà a vendere film anche fuori dagli Stati Uniti.