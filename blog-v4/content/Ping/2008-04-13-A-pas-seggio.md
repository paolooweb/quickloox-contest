---
title: "A pas-seggio"
date: 2008-04-13
draft: false
tags: ["ping"]
---

Tornando dall'esercizio del diritto del voto, pensavo che Mac resta l'unico computer su cui funzionano tutti i sistemi operativi più diffusi.

Il più libero, il più democratico.

Un motivo per cui ora ce l'ho in grembo in poltrona e mi sento di avere acquistato secondo coscienza. :-)