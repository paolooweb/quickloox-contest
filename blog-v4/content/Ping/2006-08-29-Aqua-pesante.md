---
title: "Aqua pesante"
date: 2006-08-29
draft: false
tags: ["ping"]
---

Un programma richiesto ma che sicuramente non sarà nel Cd di Macworld ottobre è la <a href="http://www.planamesa.com/neojava/it/index.php" target="_blank">beta 3 di Neoffice Aqua</a>. Non è in formato Universal ma in due installazioni separate PowerPc e Intel, a totalizzare circa 248 mega. A questo andrebbero aggiunti i language pack per l'italiano, anche loro in doppia versione, per altri 29 mega. Totale, 277 mega e oltre.

Farò il possibile, ma credo che non ci starà proprio. Direi che il candidato naturale per una inclusione nel Cd è una versione <em>final</em> del software, oppure un'uscita straordinaria di Macworld con Dvd.

Siccome una o l'altra cosa accadranno prima o poi, se non ce la si fa non è un addio, ma solo un arrivederci.