---
title: "Grazie del fior"
date: 2011-02-13
draft: false
tags: ["ping"]
---

<a href="http://www.allaboutapple.com/blog/" target="_blank">All About Apple</a>, l'associazione ligure che ha creato il museo Apple più straordinario del mondo, partecipa all'esposizione <i>Comunicazione in Mostra - da Gugliemo Marconi a Steve Jobs</i> con una selezione della propria collezione e, in una prima assoluta per una manifestazione pubblica, un sempre più raro Apple 1 autentico.

Per ammirarlo c'è tempo fino a domenica 20 febbraio, al Palafiori di Sanremo in corso Garibaldi. L'orario è continuato tutti i giorni dalle 10 alle 19. Su Google Maps bisogna scrivere assolutamente <i>corso giuseppe garibaldi</i>, altrimenti si viene spediti in un paesino a ponente che non c'entra niente.

L'evento si svolge nell'ambito di <a href="http://www.sanremoexpomusic.it" target="_blank">sanremoExpomusic</a>, manifestazione dal sito orrendo che però contiene tutte le informazioni del caso.

Per averne di più specifiche si può <a href="mailto:info@allaboutapple.com">scrivere</a> allo <i>staff</i> di All About Apple o addirittura telefonare loro, al 3475376062. Sono persone amiche e simpatiche; trovarsi intorno qualcuno di loro durante una visita è molto meglio che fare da soli.

Duecento metri più lontano c'è gente che strimpella, niente di importante; l'anno prossimo saranno quasi tutti dimenticati. Un Apple 1 invece è una reliquia.