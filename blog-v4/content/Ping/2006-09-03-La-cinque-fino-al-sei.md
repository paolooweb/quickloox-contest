---
title: "La cinque fino al sei"
date: 2006-09-03
draft: false
tags: ["ping"]
---

Grazie a <strong>ubik</strong> per la segnalazione:

<cite>Magari interessa a qualcuno. <a href="http://www.daz3d.com/program/bryce/bryce5free.php" target="_blank">Bryce 5 gratis</a> fino al 6 settembre.</cite>

Mi sa che interessa s&#236;. I paesaggi artificialmente naturali che sa creare Bryce sono qualcosa di veramente fantastico.