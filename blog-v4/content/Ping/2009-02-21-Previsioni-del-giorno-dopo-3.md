---
title: "Previsioni del giorno dopo / 3"
date: 2009-02-21
draft: false
tags: ["ping"]
---

<a href="http://www.live.com/" target="_blank">Live Search</a> di Microsoft mette ogni giorno una diversa immagine di sfondo sulla pagina.

Oggi il tema è astratto; ieri invece c'era un leopardo delle nevi. S&#236;, uno Snow Leopard, come il prossimo sistema operativo Apple.

Su <a href="http://news.cnet.com/8301-13860_3-10168102-56.html?part=rss&amp;subj=news&amp;tag=2547-1_3-0-20" target="_blank">Cnet</a> hanno iniziato una complessa analisi. Vuoi vedere che Apple sostituisce Google con Live Search in Safari?

Su <a href="http://www.techflash.com/Microsofts_Snow_Leopard_puzzler_39892562.html" target="_blank">TechFlash</a> si chiedono se Microsoft sia stata stupida oppure astuta.

<a href="http://www.liveside.net/main/archive/2009/02/19/snow-leopard-twitterverse-abuzz-with-latest-live-search-image.aspx" target="_blank">LiveSide.net</a> si domanda se sia una coincidenza oppure no.

La previsione è semplice: non significherà niente, se non che l'informazione via Internet è veramente una cloaca.