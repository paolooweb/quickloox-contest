---
title: "La cenere sotto il tappeto"
date: 2006-07-21
draft: false
tags: ["ping"]
---

Giorni fa mi sono scandalizzato perché un portatile Dell è andato a fuoco e non c'è stato tutto il  macello che si è fatto in passato per portatili Apple che <em>non</em> lo erano andati.

Mi è stato detto tranquillizzati, in realtà se ne è parlato, altrimenti come faremmo a saperlo eccetera.

Ora che emerge come Dell <a href="http://www.personaltechpipeline.com/190600186?cid=rssfeed_pl_ptp" target="_blank">sapesse da due anni</a> che i suoi portatili surriscaldano in modo grave, che a dicembre scorso hanno richiamato 22 mila portatili gravemente difettosi ma che i due anni sono interamente <em>prima</em> di quella data, e nessuno ha mai aperto bocca sulla cosa fino a oggi, vogliamo chiamarla disinformazione?