---
title: "Un tredici sul browser"
date: 2007-08-17
draft: false
tags: ["ping"]
---

Mare. Tempo di letture distratte e leggere, per esempio questo articolo di InsideMacGames su <a href="http://www.insidemacgames.com/features/view.php?ID=525&amp;Page=4" target="_blank">tredici anni di giochi per Mac</a>.

Giusto per insaporire con un pizzico di polemica, alzi la mano chi li ha giocati tutti. Io mi perdo nelle nebbie già a partire da pagina 2. E sarebbero pochi&#8230; a me servivano tre vite extra.