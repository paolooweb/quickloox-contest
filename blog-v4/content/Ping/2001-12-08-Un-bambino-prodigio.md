---
title: "Un bambino prodigio"
date: 2001-12-08
draft: false
tags: ["ping"]
---

Compie dieci anni in questi giorni. È nato che sembrava avviato a un destino per soli ricchi ma invece si è rivelato uno di noi tutti, sempre pronto a dare una mano, con grande discrezione e anche con grande senso del dovere, combinando a volte anche qualche pasticcio ma tutto sommato continuando a farsi benvolere.
Si appassiona alla saga di Guerre Stellari ma non disdegna la musica classica; basta dargli le istruzioni appropriate e fa le cose giuste pure con la più complicata delle videocamere.
Se a dieci anni è così, chissà che cosa diventerà da grande.
Nel frattempo, con affetto, facciamogli tanti auguri.
Buon compleanno, QuickTime.

lux@mac.com

<http://www.apple.com/quicktime>