---
title: "Quelle perse sono le battaglie migliori"
date: 2009-04-19
draft: false
tags: ["ping"]
---

Un giorno, nate le stampanti laser e iniziata l'orgia dell'editoria personale, mi guardai intorno e dissi <em>mai più Times e Helvetica</em>.

Ora leggo che di Comic Sans si è stancato <a href="http://online.wsj.com/article/SB123992364819927171.html" target="_blank">persino il suo progettista</a>, simpatizzante di un bizzarro <a href="http://www.bancomicsans.com/home.html" target="_blank">movimento che vuole abolire Comic Sans</a> mettendolo ovunque.

Oggi Comic Sans, scrive il Wall Street Journal, da progetto nato dentro Microsoft a seguito dell'orripilante <a href="http://en.wikipedia.org/wiki/Microsoft_Bob" target="_blank">Bob</a> si è diffuso a toccare <cite>volantini scolastici e newsletter turistiche, pubblicità Disney ed etichette di giocattoli, posta elettronica professionale, segnali stradali, Bibbie, siti porno, lapidi e pubblicistica ospedaliera sui tumori all'intestino</cite>.

È abbastanza. Dichiaro unilateralmente <em>mai più Comic Sans</em>.