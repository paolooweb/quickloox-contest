---
title: "Cover, Quick"
date: 2007-10-31
draft: false
tags: ["ping"]
---

Appare in chat <strong>Monica</strong> e mi chiede un parere su alcuni Pdf da localizzare.

Apro la cartella che li contiene. Comando-4, si apre Cover Flow. Le icone dei Pdf sono in realtà anteprime della prima pagina e, sfogliando, capisco in un attimo a quale Pdf devo fare riferimento.

Ma c'è un dettaglio di testo che nell'anteprima appare troppo piccolo. Comando-Y, il Pdf appare in una finestra. Clic sull'apposito pulsante e ce l'ho a pieno schermo. Facile individuare il brano problematico e rispondere a Monica.

Chiudo tutto e capisco improvvisamente che non ho avuto bisogno di aprire Anteprima.

Quanti ne ho sentiti lamentarsi dei requisiti di memoria di Leopard e nessuno che obiettasse <em>s&#236;, ma se non devi aprire programmi c'è caso che convenga</em>.