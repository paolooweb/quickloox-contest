---
title: "Chiuso per mancanza di rispetto"
date: 2008-12-31
draft: false
tags: ["ping"]
---

Uno dei tanti giochi di massa <em>online</em>, il fantascientifico <a href="http://eu.playtr.com/en/" target="_blank">Tabula Rasa</a>, chiude il 28 febbraio.

L'azienda dice che lo sviluppo è stato troppo lento e non tecnicamente adeguato.

Qualche osservatore ha, appunto, osservato che i giochi <em>online</em> iniziano a essere tanti e, parlando di ambientazione fantascientifica&#8230; spazio per tutti non ce ne può essere.

Io osservo le specifiche e vedo che il gioco è solo per Windows. Internet è per tutti; se ci metti un prodotto solo per alcuni significa che non rispetti la tua clientela e non c'è da stupirsi se anche qualcos'altro non funziona.

Se qualcuno fosse costretto a sorbirsi Windows anche nel 2009, dal 10 gennaio Tabula Rasa si potrà giocare gratis, fino alla meritata chiusura.