---
title: "Facciamoci sentire"
date: 2010-07-24
draft: false
tags: ["ping"]
---

Il software <i>open source</i> si può aiutare in diverse maniere e non è necessario essere programmatori o analisti.

In almeno un caso è sufficiente&#8230; parlare: il progetto VoxForge raccoglie campioni audio per realizzare modelli acustici destinati ai motori di riconoscimento del parlato.

Finora le raccolte di parlato usate per il compito hanno problemi di licenza e sono costosi. Perché non leggere un po' di testo qualunque e inviarlo a <a href="http://www.voxforge.org/" target="_blank">VoxForge</a>, dove verrà impiegato molto meglio di quello che trasmettiamo usualmente alla suocera?

Creare un campione di audio con Mac è un giochetto da ragazzi: basta QuickTime Player, o GarageBand, o il gratuito <a href="http://audacity.sourceforge.net/" target="_blank">Audacity</a>.

Si troverà che il progetto è tutto in inglese. Dovrebbe essere ulteriore motivazione per fornire campioni in italiano. Più ce ne saranno, più è probabile che compaiano soluzioni buone anche per la nostra lingua. O per le nostre orecchie.