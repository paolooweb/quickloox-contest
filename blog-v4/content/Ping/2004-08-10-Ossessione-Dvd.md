---
title: "Che ossessione il Dvd"
date: 2004-08-10
draft: false
tags: ["ping"]
---

Esiste una soluzione più semplice ed economica

Continuo a sentire persone che, a loro dire, impazziscono per crearsi una biblioteca di film su Dvd. Problemi di codifiche, masterizzazioni, dimensioni dei file e quant’altro.

I film ovviamente sono di provenienza dubbia, scaricati qua e là, noleggiati sperando di fare il colpaccio, passati dall’amico dell’amico e così via. C’è un modo estremamente semplice di creare una biblioteca di Dvd pronti da vedere in Tv: comprare i Dvd.

Considerati tempi, costi e complicazioni inaspettate di tutto questo febbrile lavoro di codifica e masterizzazione mi chiedo: ma perché non comprarsi un disco rigido dedicato? Ormai hanno costi bassissimi, la velocità è quella di FireWire e comprarsi una decina di scatole di Dvd, senza pensare a tutto il tempo perso, richiede la stessa spesa. E se il computer non è collegato al televisore, vivaddio, lo si colleghi. In tutta evidenza, per tante persone, dovrebbe essere più facile.

<link>Lucio Bragagnolo</link>lux@mac.com