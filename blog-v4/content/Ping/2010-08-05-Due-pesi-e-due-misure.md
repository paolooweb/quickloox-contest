---
title: "Due pesi, due misure"
date: 2010-08-05
draft: false
tags: ["ping"]
---

Questo Ping compare in tempo leggermente differito rispetto a quando viene scritto ed è probabile che la <a href="http://daringfireball.net/linked/2010/08/02/jailbreakme" target="_blank">falla di sicurezza</a> nel motore di gestione dei Pdf di iOS sia già stata tappata, visto che è, come dire, cospicua.

La cosa incredibile è che, fosse stata una falla di sicurezza e basta, sarebbero usciti titoli piuttosto preoccupati, e a ragione (chi non è interessato al <i>jailbreak</i> del proprio apparecchio iOS non carichi la pagina jailbreakme.com, che non linko apposta). Invece la gran parte della stampa ha titolato allegramente di un sistema semplicissimo per avere il <i>jailbreak</i>, evviva evviva.

Per gli interessati al <i>jailbreak</i> è effettivamente un sistema semplicissimo. Ma è anche una falla di sicurezza notevole e va notato come le due cose si intersechino in modo intrigante.

Uso iPhone 2G e di conseguenza il mio apparecchio è <i>jailbreakato</i> obbligatoriamente, altrimenti non può funzionare. Al tempo stesso uso iPad, regolare, e mi guardo bene dal modificare la situazione.