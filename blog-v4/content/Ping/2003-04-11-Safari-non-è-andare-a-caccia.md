---
title: "Safari non è andare a caccia"
date: 2003-04-11
draft: false
tags: ["ping"]
---

Chi non sa stare al suo posto danneggia tutti gli altri

La caccia rimane un passatempo discutibile, ma numerosi cacciatori in realtà non uccidono nessuno. Si fanno una passeggiata in un posto tranquillo, magari sparano un paio di colpi senza neanche mirare troppo, per tenere in esercizio il fucile, e tornando a casa si fermano a prendere i pasticcini per pranzare in allegria. Per molti pescatori, che passano una mattinata rilassante senza preoccuparsi che abbocchi, è la stessa cosa.

Poi ci sono i frustrati che pescano con la dinamite e sparano a tutto quello che si muove e rovinano il divertimento, l’ambiente e anche qualche famiglia quando ci scappa l’incidente.

Più in piccolo, guardiamo Safari. Una beta pubblica che Apple ha reso tale per avere feedback e consentire alla comunità di provarlo, inviare bug, chiedere nuove funzioni, insomma farne un gran browser.

Inizialmente, oltre alla beta pubblica, c’era anche un programma mirato per cui alcuni sviluppatori di fiducia ricevevano beta non pubbliche, più avanzate, per avere feedback ancora più qualificato.

Ma qualcuno si è messo a pescare con la dinamite e sparare a tutto quello che si muove, pur di avere in mano la beta 1.00000000000000029 invece della 1.00000000000000028, per sentirsi più importante o meno frustrato.

Così Apple ha sospeso il programma avanzato. Chi non sa usare un’arma in modo rispettoso del resto del mondo, neanche per tenerla scarica, non dovrebbe andare a caccia.

<link>Lucio Bragagnolo</link>lux@mac.com