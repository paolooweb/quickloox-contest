---
title: "L'inganno per Babbo Natale"
date: 2009-10-22
draft: false
tags: ["ping"]
---

I nullapensanti erano convinti che <i>unibody</i> fosse sinonimo di <i>alluminio</i>, non si capisce in base a che logica diversa dal non averne una. Cos&#236; oggi sono sconcertati dal fatto che il nuovo MacBook possa essere <i>unibody</i> ma realizzato in materiale plastico.

Invece è cos&#236;. Il nuovo telaio di MacBook è realizzato in un pezzo unico, senza segni di stampo e senza giunture. Per consentire l'accesso alla logica è vuoto nella parte inferiore, dove gli si avvita un pannello di alluminio rivestito sul lato esterno di materiale antisdrucciolo.

Il nuovo <a href="http://www.apple.com/it/macbook/" target="_blank">MacBook</a> è più leggero del precedente e molto più resistente alle torsioni. Tenerlo in mano è sensazione ben diversa da prima. Per il resto rimane un MacBook, anzi, hanno tolto ancora qualcosa dalla dotazione precedente. Ma hanno tolto qualcosa anche al prezzo e, come macchina di ingresso, sembra ben posizionata.

Anche i cattivi potranno avere un Natale felice, se saranno furbi abbastanza da farsi portare non carbone, ma policarbonato&#8230;