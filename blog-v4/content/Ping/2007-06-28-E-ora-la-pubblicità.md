---
title: "E ora la pubblicità"
date: 2007-06-28
draft: false
tags: ["ping"]
---

In questo spazio si parla poco o niente di Macworld edizione cartacea.

L'eccezione che conferma la regola: sta arrivando, o è arrivato, in edicola Macworld luglio. Nel Cd c'è uno speciale programmi per scrivere e a) se c'è una cosa che mi interessa, è scrivere; b) se nell'informatica c'è un settore piagato da un programma infernalmente nocivo, è la scrittura.

Lo scopo di questo Cd è fare capire al mondo che per scrivere cose belle non basta uno strumento. Ma ci vuole comunque uno strumento buono e veramente utile. Se non riesci a sostituire Word dopo questo Cd, vabbè. Il destino è il destino.

Siccome non siamo qui per vendere, ma per regalare, l'elenco dei programmi è questo:

<ul compact="compact" type="disc">
	<li><a href="http://www.abisource.com/" target="_blank">AbiWord 2.4.5</a></li>
	<li><a href="http://returnself.com/" target="_blank">Avenir 2.3.4</a></li>
	<li><a href="http://www.barebones.com/products/bbedit/index.shtml" target="_blank">BBEdit 8.6.2</a></li>
	<li><a href="http://www.bean-osx.com/Bean.html" target="_blank">Bean 0.9.3b</a></li>
	<li><a href="http://www.jesusreigns.co.uk/downloads.htm" target="_blank">BookWorm 5.6.3</a></li>
	<li><a href="http://www.bartastechnologies.com/products/copywrite/" target="_blank">CopyWrite 2.2.9</a></li>
	<li><a href="http://excalibur.sourceforge.net/index.html" target="_blank">Excalibur 4.0.4</a></li>
	<li><a href="http://www.finaldraft.com/products/final-draft/" target="_blank">Final Draft 7.1.3</a></li>
	<li><a href="http://www.finaldraft.com/products/final-draft/" target="_blank">Final Draft AV</a></li>
	<li><a href="http://www.kudlian.net/products/freetext/" target="_blank">FreeText 1.0</a></li>
	<li><a href="http://www.artman21.net/product/JeditX/index_E.html" target="_blank">Jedit X 1.40</a></li>
	<li><a href="http://www.jerssoftwarehut.com/" target="_blank">Jer's Novel Writer 1.0.0.2</a></li>
	<li><a href="http://www.iol.ie/~scideas/Software/Manuscript.htm" target="_blank">Manuscript 1.5.5</a></li>
	<li><a href="http://www.marinersoftware.com/sitepage.php?page=12" target="_blank">Mariner Write 3.7.2</a></li>
	<li><a href="http://www.mellel.com/" target="_blank">Mellel 2.2.5</a></li>
	<li><a href="http://www.neooffice.org/neojava/it/index.php" target="_blank">NeoOffice 2.1 Patch 5</a></li>
	<li><a href="http://www.nisus.com/" target="_blank">Nisus Writer Express e Pro</a></li>
	<li><a href="http://www.syndicomm.com/" target="_blank">OK-Writer 1.2</a></li>
	<li><a href="http://www.rom-logicware.com/index.htm" target="_blank">Papyrus 12.56</a></li>
	<li><a href="http://www.literatureandlatte.com/scrivener.html" target="_blank">Scrivener 1.055b</a></li>
	<li><a href="http://www.skti.org/" target="_blank">skEdit 3.6.1</a></li>
	<li><a href="http://smultron.sourceforge.net/" target="_blank">Smultron 3.1</a></li>
	<li><a href="http://www.codingmonkeys.de/subethaedit/index.html" target="_blank">SubEthaEdit 2.6.3</a></li>
	<li><a href="http://www.tex-edit.com/" target="_blank">Tex-Edit Plus 4.9.7</a></li>
	<li><a href="http://trancesoftware.com/software/textforge/" target="_blank">TextForge 2.0</a></li>
	<li><a href="http://macromates.com/" target="_blank">TextMate 1.5.5</a></li>
	<li><a href="http://www.barebones.com/products/textwrangler/index.shtml" target="_blank">TextWrangler 2.2.1</a></li>
	<li><a href="http://www.blue-tec.com/" target="_blank">Ulysses 1.5</a></li>
	<li><a href="http://supertart.com/qisoftware/Writer.html" target="_blank">Writer 1.2.2</a></li>
	<li><a href="http://www.hogbaysoftware.com/product/writeroom" target="_blank">WriteRoom 2.1</a></li>
	<li><a href="http://getxpad.com/" target="_blank">xPad 1.2.5</a></li>
	<li><a href="http://www.stonetablesoftware.com/z-write/index.html" target="_blank">Z-Write 1.5</a></li>
</ul>
