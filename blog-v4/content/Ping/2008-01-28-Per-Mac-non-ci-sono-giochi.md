---
title: "&#8220;Per Mac non ci sono giochi&#8221;"
date: 2008-01-28
draft: false
tags: ["ping"]
---

Ho trovato un articolo sui <a href="http://apcmag.com/7890/best_open_source_games" target="_blank">migliori giochi open source</a> a opinione dell'autore.

Mi sono messo a vedere quali di questi giochi sono compatibili con Mac <em>out of the box</em> (niente compilazioni, niente complicazioni, giusto un <em>download</em>, sennò è troppo facile: sudandoci sopra, gira tutto e la risposta è s&#236; al cento percento).

<a href="http://www.secretmaryo.org/" target="_blank">Secret Maryo Chronicles</a>: no.
<a href="http://ufoai.sourceforge.net/" target="_blank">Ufo: Alien Invasion</a>: s&#236;.
<a href="http://freeciv.wikia.com/wiki/Main_Page" target="_blank">Freeciv</a>: s&#236; (ora anche Aqua!).
<a href="http://red.planetarena.org/" target="_blank">Alien Arena</a>: no.
<a href="http://www.wesnoth.org" target="_blank">The Battle for Wesnoth</a>: s&#236;.
<a href="http://www.scorched3d.co.uk/" target="_blank">Scorched 3D</a>: s&#236;.
<a href="http://www.nethack.org/" target="_blank">NetHack</a>: s&#236;.
<a href="http://supertuxkart.sourceforge.net/" target="_blank">Super Tux Kart</a>: s&#236;.
<a href="http://crossfire.real-time.com/" target="_blank">Crossfire</a>: s&#236;.
<a href="http://www.freecol.org/" target="_blank">FreeCol</a>: s&#236;.
<a href="http://www.frozen-bubble.org/" target="_blank">Frozen Bubble</a>: no (ci sarebbero dei distinguo, ma taglio corto).
<a href="http://www.flightgear.org/" target="_blank">FlightGear</a>: s&#236;.
<a href="http://armagetronad.net/" target="_blank">Armagetron Advanced</a>: s&#236;.
<a href="http://www.openarena.ws/" target="_blank">OpenArena</a>: no (la roba Mac è Coming Soon o Engine Only, taglio corto).
<a href="http://www.freeorion.org" target="_blank">FreeOrion</a>: s&#236;.

(nei commenti all'articolo c'è un sacco di altra roba interessante)

Undici su quindici sono per Mac gratis e pronti da scaricare. Tredici su quindici sono pronti o semipronti. Quindici su quindici, in un modo o nell'altro, funzionano.

Prima di dire che per Mac non ci sono giochi, sarebbe bello che uno avesse minimamente padroneggiato almeno cinque o sei di questi undici, gratis e pronti, giusto per dire che il fatto di avere o non avere giochi va anche applicato alla vita. Se posso giocare per un'ora al giorno, avere a disposizione un gioco al giorno è già fin troppo.