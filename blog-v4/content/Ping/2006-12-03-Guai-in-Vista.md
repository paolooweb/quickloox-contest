---
title: "Guai in Vista?"
date: 2006-12-03
draft: false
tags: ["ping"]
---

No. È già successo con Windows 95. Sembrava l'apocalisse dei sistemi operativi, Windows era diventato uguale al Macintosh. Invece era la solita ciofeca, con la facciata ridipinta.

Con Vista è la stessa cosa. La macchina del marketing Microsoft sta già iniziando il bombardamento e per un po' ci si sentirà facilmente circondati. Poi comincerà l'elenco delle falle nella sicurezza del sistema e tutto tornerà sui binari consueti.

Semmai, se qualcosa deve portare Vista, sia un po' di sano odio nei confronti dei portavoce di Microsoft: gente consapevole di raccontare balle sesquipedali, che si guadagna lo stipendio combattendo per l'arretratezza del mercato con una faccia di bronzo totale.

Le loro uniche azioni che hanno qualche valore sono quelle in Borsa.