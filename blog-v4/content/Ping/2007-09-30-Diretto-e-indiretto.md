---
title: "Diretto e indiretto"
date: 2007-09-30
draft: false
tags: ["ping"]
---

La domanda di <strong>Goldmund100</strong> è interessante:

<cite>Sono un utente Apple, leggendo <a href="http://marco.boneff.ch/blog/?p=452" target="_blank">questo pezzo</a> mi sono sorti dei dubbi sul perché Apple non abbia un rapporto più diretto con Linux. Ne vuoi parlare?</cite>

Penso che il modo migliore per andare diritto al punto sia girarci intorno.

Quando Apple si trovò in crisi di leadership e di immagine, e iniziò a chiudere trimestri in rosso, molti progetti vennero cancellati. E venne tolto di produzione Newton, che costava molto e vendeva poco. Una delle motivazioni addotte per la chiusura di Newton fu che Apple doveva focalizzare gli sforzi e occuparsi di due sistemi operativi completamente diversi era troppo.

A inizio 1996, Apple e Osf Research Institute (oggi <a href="http://www.ri.silicomp.fr/" target="_blank">Silicomp Research Institute</a>) diedero vita a <a href="http://www.mklinux.org/" target="_blank">Mk-Linux</a>, un <em>porting</em> di Linux su PowerPc. Nel 1998 lo sviluppo venne affidato alla comunità dei programmatori <em>open source</em>, e tuttora prosegue.

Il kernel di Mac OS X è un sistema operativo <em>open source</em>, che si chiama Darwin. Premendo due tasti all’avvio (mi pare Comando-S oppure Comando-V) si entra in <a href="http://developer.apple.com/opensource/index.html" target="_blank">Darwin</a>.

Esiste una raccolta di software <em>open source</em> che si chiama <a href="http://www.gnu-darwin.org/" target="_blank">Gnu-Darwin</a>. Contiene il sistema operativo e una raccolta di 250 tra programmi, utility e librerie, tra cui per esempio l’interfaccia grafica Gnome, molto usata su Linux.

iPod non è stato mai aperto a Linux, fin dal 2001. Da sempre, i programmatori Linux hanno retroingegnerizzato iPod e hanno trovato modi per usarlo. Sono stati sviluppati <a href="http://ipodlinux.org/Main_Page" target="_blank">iPodLinux</a>, un sistema operativo indipendente per iPod, e <a href="http://www.gtkpod.org/about.html" target="_blank">gtkpod</a>, un’interfaccia per l’uso di iPod sotto Linux.

Per rendere disponibile iTunes su Linux bisognerebbe svelarne il codice e diventerebbe semplicissimo sviluppare soluzioni che aggirano la protezione della musica. Senza protezione della musica, addio accordi con le case discografiche. Senza accordi con le case discografiche, addio iTunes Store. Come minimo una seccatura.

Linux si installa su Mac semplicemente scaricando una versione che supporta Mac, masterizzandosi i dischi e installandolo.

Spero di avere risposto. Anche se non direttamente.