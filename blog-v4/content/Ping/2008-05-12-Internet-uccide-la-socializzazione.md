---
title: "Internet uccide la socializzazione"
date: 2008-05-12
draft: false
tags: ["ping"]
---

Avere un sito che ti dice se un altro sito è <a href="http://downforeveryoneorjustme.com/" target="_blank">veramente spento</a>, oppure se è solo un problema locale, in effetti leva la possibilità di spedire mail e messaggini a mezzo mondo per chiedere se tutti quanti hanno un problema con una pagina il più delle volte insignificante.

Tra l'altro: eccettuato Apple Store (per il quale esiste <a href="http://royal.pingdom.com/?p=266" target="_blank">l'apposito widget</a>, altro killer della socializzazione, la risposta è <em>no, gli altri non hanno problemi</em>.

Grazie <strong>Ci</strong>!