---
title: "La maledizione di root<p>"
date: 2005-02-12
draft: false
tags: ["ping"]
---

La madre di tutte le sciocchezze che si possono fare su Mac OS X<p>

Quando arrivo a scrivere una cosa su Macworld cartaceo significa che il livello di guardia è stato raggiunto e superato. Oramai siamo a livello che più si ripete e meglio è.<p>

In una installazione standard di Mac OS X l&rsquo;utente root è disabilitato. Abilitarlo non serve a  niente, apre la porta a un sacco di complicazioni e aumenta il rischio di insicurezza del computer, oltre a fornire agli inetti un ottimo sistema per fare danni irreparabili ai dati.<p>

Gli unici che hanno facoltà di abilitare root sono quelli che lo hanno già abilitato una volta su un altro computer, sanno farlo da Terminale e conoscono a memoria i comandi da dare.<p>

Chi non risponde ai requisiti (il sottoscritto, per dire) pensi a godersi Mac OS X e non stia a crearsi problemi di cui non ha bisogno e che soprattutto non saprà come risolvere. Grosso modo come nessuno sente la necessità di risintonizzare il magnetron del suo forno a microonde.<p>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>