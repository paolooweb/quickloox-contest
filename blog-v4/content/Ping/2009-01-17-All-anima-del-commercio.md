---
title: "All'anima del commercio"
date: 2009-01-17
draft: false
tags: ["ping"]
---

<a href="http://ipodpalace.com" target="_blank">Stefano</a> mi segnala un <a href="http://news.cnet.com/8301-13579_3-10130908-37.html?tag=nl.e729" target="_blank">pezzo di Cnet</a> in cui tale Rafe Needleman racconta che tutto sommato passare da Window a Mac non è cos&#236; facile.

Inizia a raccontare, sommariamente, che Mac OS X non è totalmente e completamente uguale a Windows e quindi è difficile. Tra le inesattezze, l'ignoranza e la malainformazione c'è spazio anche per la malafede, quando parla dello spostamento della musica da iTunes Pc a iTunes Mac. Non ci ha neanche provato, <cite>spaventato</cite> dalle istruzioni, quando - commenta Stefano - <cite>basta spostare le librerie</cite>.

Insomma, passare a MacBook non è questo gran che, dice Needleman.

L'articolo è superficiale, senza approfondimento. Sembra anche scritto un po' di fretta.

Di fianco al testo, inserzione pubblicitaria. Sui portatili Dell.

Coincidenza, certo. Ci saranno altre mille inserzioni diverse a rotazione, certo. Intanto c'è anche quella, guarda caso proprio di fianco a un pezzo su come sa di sale il pane di un MacBook.