---
title: "Nascono nuovi untori"
date: 2003-05-03
draft: false
tags: ["ping"]
---

Le nuove versioni dei software mediali di Apple ti fanno ammalare di voglia

Sono stato gentilmente invitato da Apple alla presentazione italiana di Dvd Studio Pro 2, Shake 3 e Final Cut Pro 4 (no, nessun 1).

Da completo incompetente di tecnica cinematografica e tecnologie di montaggio e compositing sono rimasto impressionato. In parte dalle funzioni, ma parte assai più larga dal grande lavoro che sta facendo Apple in termini di interfaccia utente e facilità di utilizzo. Sono programmi talmente densi di funzioni che, davanti allo schermo di un PowerBook 17”, credi di morire di confusione da un momento all’altro. Invece muovi il mouse, schiacci un pulsante, fai scendere un menu e ti accorgi che non solo esiste una logica, ma è anche straordinariamente lineare e comprensibile, data la complessità di partenza del tema. Lineare e comprensibile anche per un incompetente.

Non che le nuove funzioni siano poca cosa, eh? Soundtrack (uno dei nuovi pacchetti integrati in Final Cut Pro) per me vale da solo il prezzo di tutto il software.

Sono tornato a casa talmente entusiasta da fare una cosa che non era mai successa prima: ho aperto iMovie 3 e ci ho trascinato sopra quindici secondi di filmato completamente amatoriale, preso con la fotocamerina altrettanto amatoriale, della cognata fresca di laurea.

Ha funzionato. Sento di rischiare il contagio del virus da cineasta. Nel caso, diverrò untore.

<link>Lucio Bragagnolo</link>lux@mac.com