---
title: "Un nuovo vicino"
date: 2010-02-03
draft: false
tags: ["ping"]
---

Sono proprio contento di avere visto Italo comparire tra le pagine di Computerworld online a <a href="http://www.cwi.it/blogs/sistemaperto/2010/02/01/perche-sistemaperto/" target="_blank">bloggare</a> di <i>open source</i>.

Italo è un amico, il presidente del Progetto Linguistico Italiano OpenOffice.org, ex giocatore di baseball e persona di un'umanità rara, dotata di intelligenza ancora più rara (verso l'alto). Non ci sentiamo spesso e, quando succede, capisco che preferirei accadesse.

L'<i>open source</i> a livello di mercato e di aziende è tema ostico, ma se ne scrive lui non può che uscirne materiale interessante e imperdibile.

Congratulazioni per il debutto e millecento di questi <i>post</i>. :)