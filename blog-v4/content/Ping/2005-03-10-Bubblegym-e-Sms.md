---
title: "Tre nomi, un sensore<p>"
date: 2005-03-10
draft: false
tags: ["ping"]
---

Nella novità hardware dei portatili Apple c&rsquo;è più di quello che si dice<p>

Sto studiando il Sudden Motion Sensor, il sistema presente nei nuovi PowerBook Apple capace di spegnere il disco rigido in caso di caduta. Ci sono cose interessanti.<p>

Intanto, accidenti a loro, la cosa ha tre nomi diversi. Viene chiamato anche Mobile Motion Module e, nel kernel di Mac OS X, Apple Motion Sensor (ams).<p>

Nella documentazione ufficiale Apple sostiene che non è possibile accedere al sistema, ma non è vero. Il comando pmset nel Terminale permette di attivarlo e disattivarlo, per esempio. Ancora, è possibile leggere i parametri presenti nel sistema e questo permette a un programma di sapere per esempio in che posizione si trova il Mac.<p>

È anche già uscito un giochino, <a href="http://www.balooba.se/baloobasoftware/">Bubblegym</a>, che sarebbe impossibile senza Sudden Motion Sensor, o comunque lo si voglia chiamare.<p>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>