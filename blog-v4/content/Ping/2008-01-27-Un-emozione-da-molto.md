---
title: "Un'emozione da molto"
date: 2008-01-27
draft: false
tags: ["ping"]
---

Ho letto il libro di Antonio Dini <a href="http://antoniodini.nova100.ilsole24ore.com/2007/11/emozione-apple.html" target="_blank">Emozione Apple</a>.

Ci ho messo poco, perché è ben scritto, e ci ho trovato molto, perché Antonio ha fatto i compiti molto, molto bene e ha condensato in queste pagine non tanto l'ennesima storia di Apple, quanto una analisi attenta e finalmente intelligente dei fattori che hanno portato Apple dove sta oggi, ovvero al successo nelle vendite e nell'immagine.

C'è anche storia di Apple, meglio preistoria di Apple. Il consueto racconto racconto di Jobs &#38; Woz &#38; Wayne passa via veloce. Invece c'è un bello spazio che permette di capire che cosa c'era davvero al Parc Xerox, per esempio. Soprattutto, si spiega che cosa funziona nel meccanismo attuale di Apple e qual è il segreto per conquistare il mercato con macchine che sono preziose perché arrivano prive del superfluo. Basta guardare MacBook Air per capire che le conclusioni di Antonio sono azzeccate e la sua analisi corretta. Non sono d'accordo su tutto, ma è questione di sfumature.

La cosa bella del libro è che tra il tecnologico e il sociologico ci sono le pagine giuste per trasmettere l'emozione Apple di cui al titolo. Tra l'altro, non è da tutti scattare da sé la foto di copertina. Ho da criticare, in piccolo, la scarsa cura sulla grafia dei nomi stranieri e, meno in piccolo, una certa minore scorrevolezza della seconda metà del libro rispetto alla prima, che letteralmente si divora.

Nel complesso gli do un otto pieno.