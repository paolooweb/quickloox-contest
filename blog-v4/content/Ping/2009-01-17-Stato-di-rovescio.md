---
title: "Stato di rovescio"
date: 2009-01-17
draft: false
tags: ["ping"]
---

Filippo <em>Phil</em> Cervellera mi segnala <a href="http://www.gazzettaufficiale.it/guida2.jsp" target="_blank">una pagina del sito della Gazzetta Ufficiale</a> (non l'assessorato alle pigne verdi di Sant'Anselmo in Cima al Picco, che sarebbe ugualmente grave, ma almeno circoscritto; la Gazzetta Ufficiale).

Si legge testualmente che <cite>non tutti i "browser" in commercio permettono di visualizzare il formato in oggetto ed allo stato attuale quello che, in base alle prove effettuate, ne consente una migliore fruizione risulta essere "MS IEXPLORER"</cite>.

Uno chiude gli occhi e subito appare <a href="http://it.wikipedia.org/wiki/Policarpo,_ufficiale_di_scrittura" target="_blank">Policarpo de' Tappetti ufficiale di scrittura</a>.

Gente che non sa niente, neanche il nome dell'oggetto di cui parla. In uno Stato di diritto dovrebbero almeno fare un corso ed essere valutati seriamente alla fine dello stesso.