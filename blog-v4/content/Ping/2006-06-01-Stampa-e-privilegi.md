---
title: "Stampa e privilegi"
date: 2006-06-01
draft: false
tags: ["ping"]
---

Questa <a href="http://docs.info.apple.com/article.html?artnum=303593" target="_blank">nota tecnica</a> di Apple mette la riparazione dei privilegi come passo numero dieci da effettuare nel tentativo di fare funzionare una stampante che non va.

Dedicato a quelli che ne sostengono la necessit&agrave; settimanale, come fosse un&rsquo;aspirina.