---
title: "È giovane, ma crescerà"
date: 2007-04-21
draft: false
tags: ["ping"]
---

Mi scuso per il ritardo con cui arriva questo aggiornamento di Ping, ritardo parzialmente giustificato dal fatto che sono partiti tecnicamente <a href="http://forum.macworld.it/" target="_blank">i forum di Macworld online</a>.

Non sono ancora stati annunciati e comprensibilmente, nessuno sapendo che esistono, sono vuotini anzichenò. Quindi si può mirare alla gloria imperitura di essere i primi audaci esploratori a mettere il mouse nel Nuovo Mondo. :-)

Scherzi a parte, torneranno utili per chiacchierare in modo ancora più esaustivo e interessante. È solo un forum. Meglio averlo che no, però.

