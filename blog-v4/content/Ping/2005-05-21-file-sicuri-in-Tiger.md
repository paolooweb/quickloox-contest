---
title: "Sicurezza è saperlo<p>"
date: 2005-05-21
draft: false
tags: ["ping"]
---

File sicuri. Ma esistono? Quali sono?<p>

Come è noto, nelle preferenze di Safari esiste la possibilità di inibire o meno l&rsquo;apertura automatica dei file sicuri. Ma che concetto ha Safari di file sicuro?<p>

La risposta è relativamente semplice:<p>

/System/Library/CoreServices/CoreTypes.bundle/Contents/Resources/System<p>

È un file Xml molto istruttivo da cui si evincono mille cose, compresi i tipi di file considerati sicuri. In Tiger originale la lista è la seguente:<p>

com.apple.mach-o-binary<br>
com.apple.pef-binary<br>
public.elf-binary<br>
com.apple.application<br>
com.apple.framework<br>
public.shell-script<br>
com.microsoft.windows-executable<br>
com.sun.java-class<br>
com.sun.java-archive<br>
com.apple.quartz-composer-composition<p>

Mac OS X 10.4.1 aggiunge alla lista un elemento in più:<p>

com.apple.dashboard-widget<p>

Altre due variazioni sono aggiungere .key e .pages all&rsquo;elenco delle estensioni relative a file sicuri.<p>

Chi se la sentisse potrà anche personalizzare l&rsquo;elenco a suo completo piacere. A patto di non lamentarsi se, non sapendo quello che sta facendo, combina guai.<p>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>