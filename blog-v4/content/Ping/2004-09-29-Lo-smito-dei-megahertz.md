---
title: "Lo smito dei megahertz<p>"
date: 2004-09-29
draft: false
tags: ["ping"]
---

Sono uno sprovveduto può credere ancora che la velocità di un processore dipenda solo dal clock<p>

Il clock di un processore non misura la velocità (di che, poi?), ma la frequenza con cui vengono eseguite le istruzioni. Prova a spostare un mucchio di riso a un chicco per volta, molto velocemente (altissimo clock) oppure con una singola badilata: clock penoso, ma altissima efficienza.<p>

Fermo restando poi che testare processori diversi ha sempre un significato relativo, dai un&rsquo;occhiata a questi <a href="http://www.barefeats.com/pentium4.html">test</a> di Bare Feats e poi sappimi dire se pensi ancora che il processore più veloce sia quello con il clock più veloce.<p>

Questo mito dei megahertz, prima lo si smitizza e meglio è.<p>

Un grosso grazie a Sergio Leone per la segnalazione.<p>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>