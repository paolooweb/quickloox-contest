---
title: "Donare il Mac alla scienza"
date: 2007-02-12
draft: false
tags: ["ping"]
---

Invece che dare inutilmente la <a href="http://setiathome.berkeley.edu/" target="_blank">caccia agli extraterrestri</a>, accendiamo i nostri Xgrid. Potremo dare i cicli inutilizzati del nostro Mac in dono a progetti scientifici.

Non c'è neanche da installare software, ma solo azionare Xgrid nelle Preferenze di Sistema, secondo le istruzioni del progetto <a href="http://www.macresearch.org/openmacgrid" target="_blank">OpenMacGrid</a>.

Requisiti: un Mac e Mac OS X 10.4. Noterai una cosa piuttosto interessante. Mentre Seti@Home, o i progetti di <a href="http://einsteinathome.org/" target="_blank">elaborazione di onde gravitazionali</a> o <a href="http://folding.stanford.edu/" target="_blank">piegatura delle proteine</a> hanno client specifici per quel progetto e basta, con Xgrid è possibile mettere semplicemente a disposizione la macchina. Poi sta a chi sviluppa il progetto sfruttarla al meglio.

Prova a mettere in piedi elaborazione distribuita su Windows. Su Mac basta configurare una preferenza. Su Windows&#8230;?