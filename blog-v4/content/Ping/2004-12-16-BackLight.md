---
title: "Due lezioni in una spremuta<p>"
date: 2004-12-16
draft: false
tags: ["ping"]
---

Più tante utility, gratis, con buoni motivi<p>

<strong>BackLight</strong> fa una cosa che ho visto fare a quindici altri programmi: trasforma il salvaschermo in uno sfondo scrivania. Controllare la funzione tramite un menu extra, però, l&rsquo;ho visto fare solo a BackLight. Che è freeware.<p>

Detto tra parentesi che se il tuo Mac non supporta Quartz Extreme non è proprio il caso, ho visto poche utility semplici ed eleganti come BackLight. Poi vado sul sito e scopro un sacco di altre utility, grafica giusta come BackLight, tanto buon <a href="http://freshlysqueezedsoftware.com/products/freeware">freeware</a> e un paio di ragionamenti assai sensati.<p>

Più la piccola ma azzeccata trovata del software appena spremuto. Per sostenere che manca bel software per Mac bisogna proprio stare eccessivamente lontani da Internet.<p>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>