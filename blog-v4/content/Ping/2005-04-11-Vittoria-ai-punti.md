---
title: "Vittoria ai punti<p>"
date: 2005-04-11
draft: false
tags: ["ping"]
---

Dinamiche psicologiche che Freud neanche le sognava<p>

Ricevo e pubblico da Massimo:<p>

<cite>Una famigliola in un punto vendita Pc/Mac osserva il Mac mini per il figlio adolescente, indecisi tra questo ed un nuovo Pc; discutendo di pro e contro mi immischio e partecipo alla discussione in modo molto neutrale, diciamo <em>politically correct</em>, senza partigianerie.</cite><p>

<cite>Il ragazzo pare molto orientato al Pc (i giochi prima di tutto&hellip;), ma poi, nel ravvedimento finale, gli scappa di dire: &ldquo;Mah! Se però prendiamo questo (il mini) gli metterò comunque una emulazione Pc&rdquo;.</cite><p>

<cite>Forse un segno di timore reverenziale.</cite><p>

<cite>Ho commentato: stai dicendo che compri la Bmw e gli metti l'emulazione della Brava (con il rispetto dovuto alla umile e capace Brava, che almeno non se la tira come la Bmw!)</cite><p>

<cite>Non so se questa storiella (vera!) può insegnare qualcosa, comunque potrebbe essere uno spunto per il marketing Apple e per i suoi distributori.</cite><p>

Più che altro, per gli scettici.<p>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>