---
title: "Protezione dell&rsquo;investimento<p>"
date: 2004-10-31
draft: false
tags: ["ping"]
---

Quando un computer è fatto per durare<p>

Mi dicono che le specifiche di Tiger (Mac OS X 10.4, pronto entro la prima metà del 2005) permettono perfino a un Pismo (PowerBook G3) di farlo funzionare.<p>

Significa che un sistema operativo fatto nel 2005 funzionerà su un portatile fatto nel 2000. Il bello è che, finora, le nuove versioni di Mac OS X sono sempre state più veloci delle precedenti, persino sulle macchine più vecchie.<p>

Prova a spiegarlo a uno che usa Windows, dopo averlo invitato a installare Service Pack 2 sul suo Pc fabbricato nel 2000&hellip;<p>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>