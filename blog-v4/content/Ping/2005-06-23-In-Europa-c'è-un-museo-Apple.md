---
title: "In Europa c&rsquo;è un museo Apple<p>"
date: 2005-06-23
draft: false
tags: ["ping"]
---

Se n&rsquo;è accorta perfino Apple. A Cupertino<p>

Giovedì scorso un senior manager di Apple ha telefonato da Cupertino, California, alla sede dell&rsquo;<a href="http://www.allaboutapple.com/museo/museo.htm">All About Apple Museum</a>, primo museo Apple in Europa, per congratularsi e chiedere un contatto per poter spedire materiale.<p>

I ragazzi del club omonimo non volevano crederci, ma la chiamata è rimasta registrata in segreteria e hanno dovuto arrendersi all&rsquo;evidenza. Anche il file audio della telefonata è diventato parte del patrimonio museale.<p>

Hanno messo in piedi una cosa dannatamente bella, che ora è diventata dannatamente seria.<p>

Se tanto mi dà tanto, tempo due anni e arriva Steve Jobs in persona in visita a Quiliano, Savona.<p>

Precediamolo.<p>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>