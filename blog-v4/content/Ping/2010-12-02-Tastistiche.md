---
title: "Tastistiche"
date: 2010-12-02
draft: false
tags: ["ping"]
---

Batterie nuove nella mia Apple Wireless Keyboard di seconda generazione, quella che usa due batterie AA invece che tre.

L'accensione era avvenuta il 29 dicembre 2009 con le pile fornite di serie, marca Energizer. Senza stare a contare i giorni, sono undici mesi abbondanti di autonomia.