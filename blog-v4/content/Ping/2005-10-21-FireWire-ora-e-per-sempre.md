---
title: "FireWire ora e per sempre<p>"
date: 2005-10-21
draft: false
tags: ["ping"]
---

Chiarimento definitivo sulla questione dei nuovi iPod e dei cavi di connessione<p>

Quando ho ricordato che i nuovi iPod non possono essere usati per avviare un Mac come dischi di boot e che Apple raccomanda dal marzo 2004 di non farlo, ho ricevuto varie contestazioni, tra cui la segnalazione di <strong>blob dc</strong> di un cavo FireWire, disponibile sull&rsquo;Apple Store, compatibile con il dock per iPod nano.<p>

blob dc ha perfettamente ragione con il cavo (e lo ringrazio per la cortesia), ma il boot da FireWire non si fa. I nuovi iPod non possiedono un controller FireWire. Al massimo, con quel cavo, si può ricaricare l&rsquo;apparecchio. Ma non si montano dischi sulla scrivania, né tantomeno si fa il boot.<p>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>