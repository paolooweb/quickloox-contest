---
title: "Il vecchio e il nuovo"
date: 2010-12-20
draft: false
tags: ["ping"]
---

Per qualcuno Mac OS X sta cedendo il passo a iOS e Mac non conta più.

Qualcuno lo spieghi a Rasmus Andersson, programmatore che si appresta a lanciare la <i>beta</i> di <a href="http://kodapp.com/" target="_blank">Kod</a>, <i>editor</i> di testo per programmatori modernissimo nella realizzazione.

Bisognerà vedere se manterrà le promesse. Detto questo, è abbastanza diverso rispetto a tutto quello che c'è in giro da meritarsi la segnalazione anche se non è ancora scaricabile. Andersson avviserà chi si registra, nel momento in cui sarà effettivamente pronta la <i>beta</i>.

Non tutti sono ancora pronti per strumenti nuovi, naturalmente. Intanto, qualcuno <a href="http://www.macworld.com/article/156533/2010/12/pagesspecialcharacters.html" target="_blank">ha scoperto</a> come indicare caratteri anomali come la tabulazione o Invio dentro i campi di ricerca e sostituzione in Pages, TextEdit, <a href="http://www.filemaker.com/it/" target="_blank">FileMaker Pro</a>, <a href="http://panic.com/coda/" target="_blank">Coda</a>, <a href="http://www.bbedit.com" target="_blank">BBEdit</a>, Promemoria (utilissimi anche se nessuno li usa!) e altri: <code>Opzione-Tab</code> e <code>Opzione-Invio</code>.

A parte Word, che come sempre reinventa la ruota e vuole <code>^t</code> e <code>^p</code>. Per chi ha voglia di complicarsi la vita.