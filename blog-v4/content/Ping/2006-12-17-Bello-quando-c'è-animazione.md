---
title: "Bello quando c'è animazione"
date: 2006-12-17
draft: false
tags: ["ping"]
---

<strong>Carlo</strong> del <a href="http://freesmug.org" target="_blank">Freesmug</a> sente il Natale, credo. Manda un sacco di segnalazioni interessanti e divertenti. La più recente è <a href="http://www.pulpmotion.com/" target="_blank">Pulpmotion</a>, brillante e semplice programma di animazione basato sulla tecnologia di Mac OS X 10.4 Tiger e sul nostro bagaglio di musica, foto e video.

Non è gratis, ma se servisse a vivacizzare un pomeriggio di Natale, e regalare due ore di allegria e serenità, sarebbe senza prezzo, credo.