---
title: "Va' pensiero"
date: 2008-10-25
draft: false
tags: ["ping"]
---

Sto seguendo la telenovela della Carta Regionale dei Servizi della Regione Lombardia.

In teoria è una cosa buonissima, che permette di usufruire di servizi in quantità in modo sicuro dal computer di casa.

Poi scopri che sul Cd non c'è il software per Mac. E devi scoprire (altrimenti ciccia) che il software per Mac lo devi scaricare dal sito (chissà quanto costava, metterlo sul Cd). Però attenzione, perché su Tiger funziona sia con Safari che con Firefox, su Leopard solo con Firefox (notare che Safari di Leopard è più rispettoso degli standard).

Non è esattamente vero nemmeno questo, perché a funzionare sono <a href="http://www.crs.lombardia.it/cm/pagina.jhtml?param1_1=N11d1ac60ecc65c44c13" target="_blank">solo alcuni servizi</a> e non tutti.

In molti casi, spiega la pagina, sono stati utilizzati componenti ActiveX. In altre parole, si sono fatte cose che possono funzionare solo con Explorer sotto Windows. Forse neanche apposta. Forse è solo carenza di facoltà mentali. O forse uno si merita l'assistenza regionale solo se usa Windows, altrimenti è cattivo. Chissà.

I servizi, recita sempre la pagina, <cite>sono in corso di aggiornamento per essere compatibili con tutti i</cite> browser<cite>.</cite>

Il pensiero vaga al concetto che questi buontemponi prendono lo stipendio grazie alle tasse di tutti, indipendentemente dal sistema operativo o dal <em>browser</em>. Se a pagare fossero solo quelli cui la carta funziona?