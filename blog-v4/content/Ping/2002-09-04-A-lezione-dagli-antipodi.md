---
title: "A lezione dagli antipodi"
date: 2002-09-04
draft: false
tags: ["ping"]
---

La smentita al luogo più comune del tipico amministratore di rete incompetente

Il Gartner Group (scusa se è poco) ha effettuato uno studio sulla facoltà di Arts dell’Università di Melbourne, dove sono installati all’incirca 4.700 Mac e 5.300 macchine Wintel (ancora, scusa se è poco).

Dalla <link>ricerca</link>http://www.macworld.co.uk/news/main_news.cfm?NewsID=4795 risulta che, considerati i costi diretti (hardware e software, server e periferiche) più i costi indiretti (supporto agli utenti), un Mac costa il 36% in meno ogni anno di un Wintel, per l’esattezza 1.114 dollari australiani contro 1.438 dollari australiani.

Sarà gente che vive a testa in giù, ma fa i conti con i piedi per terra. :-)

<link>Lucio Bragagnolo</link>lux@mac.com