---
title: "La programmazione è arte?"
date: 2009-05-04
draft: false
tags: ["ping"]
---

Il sottoscritto modererà una <a href="http://www.all4webday.com/jsp/Wiki?Agenda" target="_blank">tavola rotonda sul tema</a> venerd&#236; prossimo 8 maggio, dalle 16:20 alle 18:00, presso il <a href="http://www.all4webday.com/jsp/Wiki?Dove+Siamo" target="_blank">dipartimento di Informatica dell'Università di Milano Bicocca, edificio U6, piazza dell'Ateneo Nuovo 1, aule 4, 5 e 10.</a>.

Tra i partecipanti figurano Gianugo Rabellino di <a href="http://www.sourcesense.com/" target="_blank">Sourcesense</a>, amministratore delegato di un'azienda di livello internazionale nel campo dello sviluppo di soluzioni <em>open source</em>; Marisa Addomine, cotitolare dello studio di progettazione integrata <a href="http://www.pfmstudio.it/" target="_blank">Pfm</a> e già programmatrice a livello di kernel; Marco Foco, <em>demoer</em> degli <a href="http://www.spinningkids.org/" target="_blank">Spinning Kids</a>, Emanuele Vulcano di <a href="http://infinite-labs.net/" target="_blank">Infinite Labs</a> e altri in via di conferma.

Chi conta di partecipare fa meglio a <a href="http://www.all4webday.com/subscribe/event/2" target="_blank">registrarsi</a> (i posti sono superabbondanti, però perché rischiare?).

Uno degli obiettivi che non ho (ancora) raggiunto è avere al tavolo anche un bravo programmatore Lisp. Se è in ascolto ed è interessato, mi può <a href="mailto:lux@loox.net?subject=Evento Bicocca">scrivere</a>.

Mi sa che parleremo anche di <a href="http://www.macworld.it/blogs/ping/?p=2291" target="_self">David Hockney</a>.