---
title: "Un editore che pensa a Mac"
date: 2006-03-09
draft: false
tags: ["ping"]
---

Occhio alla homepage del sito di <a href="http://www.fag.it" target="_blank">Edizioni Fag</a>: c&rsquo;&egrave; in bella evidenza il link al libro Video Digitale con il Mac dell&rsquo;amico <a href="http://www.spobe.com" target="_blank">Jida</a>.

Fag &egrave; una casa editrice piccola ma emergente, cui fare attenzione. Hanno rinnovato il sito da poco e sembrano fare (positivamente) sempre pi&ugrave; sul serio. Speriamo compaiano tanti altri libri per lettori con un Mac. :-)