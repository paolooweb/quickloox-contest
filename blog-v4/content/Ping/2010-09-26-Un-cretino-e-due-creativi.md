---
title: "Un cretino e due creativi"
date: 2010-09-26
draft: false
tags: ["ping"]
---

Premessa di Paul Thurrott su SuperSite for Windows, <a href="http://www.winsupersite.com/alt/ipad_whatisit.asp" target="_blank">31 maggio 2010</a>: <cite>Quando vai in giro solo con un iPad, stai comunicando di non essere l&#236; per contribuire, ma solo per consumare.</cite>

Mike Krahulik, disegnatore della striscia a fumetti <a href="http://www.penny-arcade.com/comic/" target="_blank">Penny Arcade</a>: <cite>Oggi sono a casa malato ed è per questo che la striscia ha un aspetto un po' strano. Cercavo di trovare un modo di disegnare la striscia da casa pur avendo tutta la mia roba in ufficio. Mi sono ricordato di avere scaricato <a href="http://itunes.apple.com/it/app/sketchbook-pro/id364253478?mt=8" target="_blank">Sketchbook Pro</a> per iPad [5,99 euro]. Cos&#236; la striscia di oggi è stata interamente disegnata con il mio indice. Kiko ha aggiunto i miei disegni nei riquadri e ha aggiunto il testo per me.</cite>

Si noti che non è un esempio fine a se stesso, della serie <i>voglio dimostrare che posso e tanto anche se non ci riesco non rischio niente</i>; è un esempio concreto di soluzione a un problema contingente di lavoro. Solo fumetti, sicuro. Si sarebbe potuto anche mandare un fax, per quanto in bianco e nero. È che gli standard si sono alzati, anche per i fumetti.

Adesso entra in scena un bambino ed esce di scena iPad. Scrive sempre Krahulik: <cite>La mia serata di <a href="http://wizards.com/DnD/" target="_blank">Dungeons &#38; Dragons</a> del luned&#236; si è giocata a casa mia per via dell'arrivo del nuovo <a href="http://www.geekchichq.com/Co_Store/The_Showroom/The_Emissary/The_Emissary.html" target="_blank">tavolo da gioco</a>. [mio figlio] Gabe doveva essere a letto per le otto di sera, ma mi ha pregato di poter stare sveglio e guardarci giocare. Ma la mattina dopo doveva andare a scuola e sapevo che il linguaggio di noi adulti non sarebbe stato appropriato per un bambino di sei anni. Gli ho spiegato che era un gioco per i papà e che quando sarebbe stato più grande avrebbe potuto giocare anche lui.</cite>

<cite>Durante la serata [mia moglie] Kara ha portato un biglietto scritto da Gabe. Era scritto a pastello su un foglietto di carta:</cite>

<cite>WEN I AM 7 CAN I PLA DUNJNS AND DRAGINS</cite>
[quando avrò sette anni potrò giocare a Dungeons and Dragons?]

<cite>Fantastico.</cite>

Dei tre, chi è il cretino?