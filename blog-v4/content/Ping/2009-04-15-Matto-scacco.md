---
title: "Matto scacco"
date: 2009-04-15
draft: false
tags: ["ping"]
---

Con Chess a disposizione di serie gratis in Mac OS X, solo un folle potrebbe pensare di voler giocare a scacchia via Terminale. Eppure Macworld.com ha mostrato il sistema per azionare via Unix il motore di Chess e giocare in Ascii.

Giunti nel Terminale (dentro la cartella Utility), si dà il comando:

<code>cd /Applications/Chess.app/Contents/Resources/</code>

Fatto ciò, su Tiger si dà il comando:

<code>./sjeng</code>

(non l'ho provato)

e su Leopard invece si dà il comando:

<code>./sjeng.ChessEngine</code>

Viene fuori una schermata veramente d'altri tempi&#8230; e si può giocare.

Il motore Sjeng ha anche un sacco di opzioni, ma visto che tanto è tutto inglese rimando alla lettura dell'<a href="http://www.macworld.com/article/139946/2009/04/termchess.html?t=109" target="_blank">articolo originale</a>.

Perché farlo? A parte il gusto dell'esplorazione, effettivamente un merito c'è: è la scacchiera (funzionante) più piccola che abbia mai visto sullo schermo di un Mac.