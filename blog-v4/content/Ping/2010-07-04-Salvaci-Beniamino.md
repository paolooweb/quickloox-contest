---
title: "Salvaci Beniamino"
date: 2010-07-04
draft: false
tags: ["ping"]
---

Affascinante l'obiettivo del <a href="http://jrcbenfranklinproject.wordpress.com/" target="_blank">Benjamin Franklin Project</a>: lavorare per fare s&#236; che il 4 luglio, festa dell'indipendenza americana, sia possibile fare uscire almeno una testata usando unicamente strumenti <i>open source</i>.

Quest'anno <a href="http://sar.jrcbenfranklin.com/top-news/07/04/ben-franklin-day-at-the-saratogian-a-declaration-of-independence-from-newsroom-software-with-video/" target="_blank">è toccato al quotidiano The Saratogian</a>.

Se le cose saranno andate come speravano i sostenitori del progetto, l'edizione del 4 luglio del Saratogian sarà stata indistinguibile da quelle degli altri giorni.

Solo che l'impaginazione sarà stata eseguita con <a href="http://www.scribus.net/" target="_blank">Scribus</a>, gli articoli scritti con <a href="http://www.openoffice.org/" target="_blank">OpenOffice</a> o <a href="http://docs.google.com" target="_blank">Google Docs</a> invece che con Word, le immagini trattate con <a href="http://gimp.lisanet.de/Website/News/News.html" target="_blank">Gimp</a> al posto di Photoshop e cos&#236; via.

Un gran lavoro. Ricostruire la gabbia editoriale di un quotidiano in Scribus ha richiesto varie notti di lavoro.

Il Saratogian del 4 luglio non l'ho visto, ma mi aspetto che ce l'abbiano fatta. Sono giorni in cui mi pressano perché io acquisti InDesign 5, sostanzialmente in quanto tale visto che il lavoro riguarda un bimestrale, e non sarei io che quello Beniamino dovrebbe convincere.