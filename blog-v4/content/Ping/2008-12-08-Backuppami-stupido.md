---
title: "Backuppami, stupido"
date: 2008-12-08
draft: false
tags: ["ping"]
---

Su <em>The Unofficial Apple Weblog</em> <a href="http://www.tuaw.com/2008/12/08/terminal-tip-change-time-machine-backup-interval/" target="_blank">hanno pubblicato il trucchetto da Terminale</a> per cambiare stabilmente la frequenza di <em>backup</em> di Time Machine, solitamente un'ora.

Il comando da digitare è

<code>sudo defaults write /System/Library/LaunchDaemons/com.apple.backupd-autoStartInterval -int 3600</code>

Il valore <code>3600</code> è quello originale e si riferisce a secondi. 3.600 secondi fanno appunto un'ora.

Cambiare il valore a 1.800 fa partire il <em>backup</em> ogni mezz'ora; 7.200 lo effettua ogni due ore e cos&#236; via. Basta ricordarsi che parliamo di secondi e avere una <em>password</em> da amministratore a portata di tastiera.

Come diceva la pubblicità? Salvare i propri dati non ha prezzo. Tutto il resto, chissenefrega.