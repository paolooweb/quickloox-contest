---
title: "La mia banca è differente"
date: 2007-01-16
draft: false
tags: ["ping"]
---

No, non è quella banca, è un'altra. Ha avuto la grande idea di mettere in pagina home del proprio sito la Coppia Felice. Lui guarda estatico lei, che estasiata manovra con perizia un portatile.

Sarebbe un quadretto idilliaco, se non fosse che da un particolare ben preciso, visibile al centro del retro dello schermo, si capisce lontano un chilometro che il portatile è spento.

Da qui in poi si scatenano le ipotesi. Se lui è felice perché lei è deficiente, o se lei preferisca un computer spento a lui acceso, o se il fotografo doveva risparmiare e cos&#236; via.

L'unica cosa certa è la marca del portatile. :-)