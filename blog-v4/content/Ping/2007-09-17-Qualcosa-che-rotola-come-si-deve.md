---
title: "Qualcosa che rotola come si deve"
date: 2007-09-17
draft: false
tags: ["ping"]
---

Sono sempre alla ricerca di programmi migliori per le serate di <a href="http://www.wizards.com/dnd" target="_blank">D&#38;D</a> con gli amici e <a href="http://dicex.sourceforge.net/" target="_blank">DiceX</a> è cresciuto un bel po'. È <em>quasi</em> quello che mi serve.