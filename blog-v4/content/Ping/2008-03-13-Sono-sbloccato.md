---
title: "Sono sbloccato"
date: 2008-03-13
draft: false
tags: ["ping"]
---

Ho affrontato la belva. Ho liberato iPhone dalle sue pellicole, ho scaricato Ziphone sul Mac e ho collegato l'oggetto al Mac stesso.

È partito iTunes, che mi ha invitato prevedibilmente ad attivare un abbonamento telefonico con At&#38;T.

Ho fatto partire la procedura di sblocco totale su <a href="http://www.ziphone.org" target="_blank">Ziphone</a> (un clic). Lo schermo ha iniziato un emozionante <em>dump</em> (discarica) di messaggi Unix incomprensibili e un po' angoscianti (della serie: se si blocca a quel punto, sarà <em>molto</em> più lunga di cos&#236;).

Quattro minuti veri (il tempo dichiarato da Ziphone) e iPhone si è sbloccato. iTunes lo ha visto regolarmente e lo schermo, invece che segnalare solo chiamate di emergenza in quattro lingue, è passato a spiegare nessuna Sim inserita in italiano.

Il software di sistema originale del telefono, però, era alla (vecchia) versione 1.1.2. Inevitabile che lo fosse, dal momento che iTunes non avrebbe mai aggiornato un iPhone illegale (s&#236;, è illegale e se non ci fosse di mezzo il lavoro avrei aspettato).

Cos&#236; ho chiesto a iTunes di aggiornare alla moderna 1.1.4. iTunes ha obbedito e, cos&#236; facendo, ha ribloccato il telefono.

Ho rilanciato Ziphone, che ha ripetuto l'operazione, sbloccando nuovamente il telefono, questa volta con il software aggiornato.

Un altro clic, altri quattro minuti, nuovo sblocco.

Ho inserito la Sim del vecchio telefono. Un tocco sullo schermo e stavo già usando la rete <em>wireless</em> di casa. Lo sblocco installa un programmino di nome Installer, che fa accedere a un parco software di dimensioni sbalorditive. C'è di tutto. Lo scoprirò gradualmente (inutile scaricare cento cose tutte insieme quando è molto più gustoso scoprirne una per volta).

Totale: otto minuti di Ziphone, sei di aggiornamento software, dieci di trasferimento della musica. E due clic. Perfino lo sblocco clandestino è a prova di scemo (almeno questa volta).

Prima morale: si è sempre detto che le cose belle della vita sono illegali, immorali o fanno ingrassare. Effettivamente, questa è illegale. È bellissimo.

Seconda morale: si capisce perché l'America è dieci anni avanti. Là questo è <em>the best iPod ever</em>, il miglior iPod mai prodotto. Qua è un telefono. Dopo avere visto che cosa fa un iPhone programmato da ragazzini senza alcun aiuto da Apple e che cosa farà iPhone 2.0 grazie al kit di programmazione di Apple, ragazzi, non c'è partita. Il telefono non è il fine, è il mezzo. Difficile spiegarlo nel Paese della pubblicità dove tutti stanno piegati in due e si raddrizzano appena arriva il trillo.