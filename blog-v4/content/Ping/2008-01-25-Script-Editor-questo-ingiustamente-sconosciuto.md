---
title: "Script Editor, questo ingiustamente sconosciuto"
date: 2008-01-25
draft: false
tags: ["ping"]
---

Per scrivere AppleScript prima di tutto bisogna sapere che cosa vuol dire <em>scriptare</em> un programma e come si usa Script Editor.

Un programma <em>scriptabile</em> è un programma in cui gli autori hanno inserito comandi che Script Editor capisce e può usare (e noi possiamo inserire dentro Script Editor). La maggior parte dei programmi più noti è <em>scriptabile</em> e Mac OS X è scriptabile anch'esso.

Script Editor si trova nella cartella <code>/Applicazioni/AppleScript</code>. La sua interfaccia mostra un riquadro bianco e, sopra, alcuni pulsanti. Per ora consideriamo solo il pulsante Esegui.

Il primo comando da imparare in AppleScript è <code>tell</code>, <em>d&#236; a</em>. In inglese potrei dire a qualcuno, indicando una ragazza, <em>tell her to use a Mac</em>, <em>dille</em> (di' a lei) <em>di usare un Mac</em>. In AppleScript è lo stesso, solo che lo diciamo ai programmi. Per esempio,

<code>tell application "iCal" to activate</code>

(in inglese, <em>di' a iCal di attivarsi</em>)

Scriviamolo (o facciamo <em>copiaeincolla</em> da qui sopra!) nel riquadro bianco di Script Editor e clicchiamo sul pulsate Esegui. Vedremo partire iCal. Va bene con qualsiasi programma, basta che il suo nome sia scritto correttamente e altrettanto correttamente tra virgolette.

Adesso cambiamo <code>activate</code> con <code>quit</code>:

<code>tell application "iCal" to qui</code>t

(in inglese, <em>di' a iCal di chiudersi</em>)

Rifacciamo clic sul pulsante Esegui. iCal si chiuderà.

Ecco. Script Editor serve a dare ordini ai programmi sul Mac.

I documenti di Script Editor si chiamano AppleScript. Gli AppleScript, piccoli programmi scritti quasi come nel linguaggio parlato, sono un sottoinsieme della classe più ampia degli <em>script</em>.

Questo minitutorial è ispirato a <a href="http://www.tuaw.com/2007/12/27/applescript-the-script-editor/" target="_blank">quest'altro</a>, comparso su The Unofficial Apple Weblog.

Domande? :-D