---
title: "Video Killed the Radio Star"
date: 2007-04-18
draft: false
tags: ["ping"]
---

<strong>Mandi</strong> voleva una schermata con le mie finestre di Safari. Intanto provo a metterne una del mio desktop, via Exposé. È assolutamente genuina ed estemporanea, presa un minuto fa.

Se si vede, è anche la prima immagine che appare su Ping. Un po' come ai tempi del passaggio dal muto al sonoro. :-)

Aggiornamento: non si vede un accidente. In attesa di arrivare a capire, l'immagine l'ho messa <a href="http://homepage.mac.com/lux/desklux.png" target="_blank">sull'iDisk</a>.