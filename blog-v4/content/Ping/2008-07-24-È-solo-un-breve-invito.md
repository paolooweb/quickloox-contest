---
title: "È solo un breve invito*"
date: 2008-07-24
draft: false
tags: ["ping"]
---

Ogni tanto incontro qualcuno che vorrebbe <em>provare Linux</em>.

Gli spiego che sta seduto su un sistema operativo open source e che gli basterebbe fare il <em>boot</em> con Comando-S per iniziare a provare gli stessi identici brividi e che, a parte minime differenze di linguaggio, è esattamente la stessa cosa.

Ma non basta.

Allora gli spiego che, lavorando su un disco esterno o una partizione, scaricarsi <a href="http://developer.apple.com/opensource/index.html" target="_blank">Darwin</a>, gratuito e <em>open source</em>, è anche più istruttivo, perché uno può anche montarsi l'interfaccia grafica che desidera, invece che trovarsene una precotta.

Ma non basta.

Occorre proprio Linux. Evidentemente c'è qualcosa nel nome, dal momento che la sostanza è identica. Se non è Linux, non è <em>open source</em> e non è libero (<a href="http://www.freebsd.org/" target="_blank">FreeBsd</a> o <a href="http://openbsd.org/" target="_blank">OpenBsd</a> non godono di stampa amica).

Dopo un supplemento di veglia ieri notte, sono in grado di annunciare l'arma totale. Fai il <em>jailbreak</em> di un iPod touch con software 2.0 e ti ritrovi installato Cydia, che è niente più e niente meno che un porting dell'installazione di Debian. Puoi giocare con Linux e con l'<em>open source</em> anche in autobus, persino con una mano sola. Ed è proprio Linux.

Più Linux di cos&#236; si muore. E il modello base di iPod touch costa 279 euro.

*<small>Battiato &#38; Sgalambro</small>

P.S.: la stessa cosa si può fare uguale uguale su un altro computer similare, un po' più costoso. Ma non si può chiamarlo <em>computer</em>, perché telefona e le cose che telefonano sono cellulari. Le cose dove si installano distribuzioni di sistemi operativi <em>open source</em> alternative al sistema operativo originale sono computer.