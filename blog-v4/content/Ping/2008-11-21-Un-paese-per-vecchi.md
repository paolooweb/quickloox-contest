---
title: "Un Paese per vecchi"
date: 2008-11-21
draft: false
tags: ["ping"]
---

Voglio esprimere tutta la mia disapprovazione per <a href="http://www.europeana.eu" target="_blank">Europeana</a>.

Un progetto vecchio e malfatto (tanto che i server sono andati giù subito).

Si poteva spendere un decimo dei nostri soldi per finanziare a pioggia il <a href="http://www.gutenberg.org/wiki/Main_Page" target="_blank">Project Gutenberg</a>, che esiste da secoli. O iniziative analoghe come <a href="http://www.liberliber.it/" target="_blank">Liber Liber</a>, in attività da lustri. Si poteva avere, con un decimo dei soldi, un risultato dieci volte migliore e premiare il lavoro di tanti volontari, invece che creare una ennesima burocrazia intorno a una cosa che non ne aveva bisogno ed erigere un castello di server che neanche ha saputo stare in piedi.

Solo dei vecchi stupidi potevano concepire qualcosa del genere. Lo dico con grande ammirazione e rispetto per tutti i vecchi intelligenti, che sanno distinguere, appunto, le cose vecchie da quelle nuove. Questa è roba nuova talmente vecchia che non si può nemmeno dire.