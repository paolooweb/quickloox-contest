---
title: "C&rsquo;&egrave; vita oltre Newton"
date: 2006-06-19
draft: false
tags: ["ping"]
---

<strong>Anidel</strong> mi segnala che il <a href="http://www.kallisys.com/newton/einstein/" target="_blank">progetto Einstein</a>, quello che mira a sganciare il software Newton dal suo hardware oramai fuori produzione da un decennio, &egrave; vivo e sta portando <a href="http://homepage.mac.com/dillera/PhotoAlbum6.html" target="_blank">risultati</a>!