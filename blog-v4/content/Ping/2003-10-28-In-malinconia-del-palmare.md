---
title: "In malinconia del palmare"
date: 2003-10-28
draft: false
tags: ["ping"]
---

Un piccolo oggetto che non ha saputo ancora diventare grande

Un grande amico mi ha fatto un grande regalo: un fax/modem originale per Newton MessagePad 2100.

Il MessagePad ha dimensioni enormemente diverse da quelle di un palmare odierno e il fax/modem in questione è un altro oggetto a parte. Ma la linea Newton è stata progettata più di dieci anni fa e la miniaturizzazione era quella ottenibile ai tempi.

Oggi guardo i palmari, miniaturizzatissimi, e mi viene una profonda malinconia. Sono molto più piccoli e leggeri, certo. Il modem ce l’hanno incorporato. Lo schermo è a colori.

Per il resto, fanno persino meno del Newton di dieci anni fa e sono il ritratto di un’evoluzione tecnologica morta dal secolo scorso. Ai tempi, avevo venduto il mio PowerBook Duo 2300 e il MessagePad era diventato il mio portatile. Raggiunsi l’apice durante un volo Los Angeles-Milano, in cui tradussi oltre centomila caratteri di un libro con la sola batteria ricaricabile standard.

Senza contare la differenza tecnologica tra un oggetto che capisce la tua scrittura e uno che ti costringe a imparare la sua.

Un palmare di oggi non potrebbe diventare il  mio portatile neanche per sbaglio.

<link>Lucio Bragagnolo</link>lux@mac.com