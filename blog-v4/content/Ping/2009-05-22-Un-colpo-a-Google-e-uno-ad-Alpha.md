---
title: "Un colpo a Google e uno ad Alpha"
date: 2009-05-22
draft: false
tags: ["ping"]
---

La migliore dimostrazione possibile che <a href="http://www.wolframalpha.com" target="_blank">Wolfram|Alpha</a> non è un rivale di Google, ma una direzione diversa e promettente di elaborazione di informazioni, è che hanno creato <a href="https://addons.mozilla.org/en-US/firefox/addon/12006" target="_blank">l'estensione per Firefox</a>.

I risultati di Google in una colonna e quelli di Wolfram|Alpha in un'altra. Quasi da adottare Firefox in pianta stabile.