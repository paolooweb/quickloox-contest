---
title: "Da un Forte all'altro"
date: 2007-09-20
draft: false
tags: ["ping"]
---

Sembra ieri che era agosto e ci si trovava a H&#244;ne in Valle d'Aosta per una trasferta straordinaria dell'All About Apple Museum, sotto le severe mura del <a href="http://www.naturaosta.it/fortedibard.htm" target="_blank">Forte di Bard</a>.

Adesso si replica, sempre al Forte. Quello <a href="http://www.allaboutapple.com/speciali/forte_dei_marmi_2007.htm" target="_blank">dei Marmi</a>.