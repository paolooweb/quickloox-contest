---
title: "Mi ha detto mio cuggino"
date: 2002-01-01
draft: false
tags: ["ping"]
---

Su Mac OS X è facile dire sciocchezze, ma dovrebbe essere difficile crederci

A terzo millennio ormai in corso da un pezzo (e il 2002 è anche un anno palindromo, cosa che non capiterà più fino al 2112) le leggende urbane dovrebbero restare confinate agli instant book e ai racconti del caminetto.
Invece qualcuno crede ancora che Mac OS X sia un sistema “infallibile” e si scandalizzano se una volta al mese si blocca o se un programma va in crash. È almeno dieci volte più stabile di Mac OS 9, ma memoria protetta non significa fault tolerance; caratteristica, questa, che se va bene può permettersi a suon di miliardi il sistema informativo centrale di una banca, non certo un sistema operativo evoluto ma pensato per computer personali nei quali può entrare ogni e qualsiasi tipo di software, scritto in ogni e qualsiasi modo, anche pessimo. Nessun sistema operativo può resistere a un programmatore sufficientemente incapace.

lux@mac.com