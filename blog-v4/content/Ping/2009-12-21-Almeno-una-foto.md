---
title: "Almeno una foto"
date: 2009-12-21
draft: false
tags: ["ping"]
---

Buon Natale a ognuno e un abbraccio speciale.

L'albero di Natale sta a Parigi, di fronte a Notre Dame. Non lo inserisco per snobismo. È quello che, fuori da casa, mi ha emozionato di più e ha avuto il significato più profondo. Se potessi, regalerei a ciascuno un pizzico di emozione; mi si scuserà se non sono riuscito ad andare oltre qualche invito Google Wave. Cos&#236;, almeno una foto.

E adesso faccio spazio sotto l'albero.