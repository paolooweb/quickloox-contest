---
title: "Normale o ormonale?"
date: 2007-06-02
draft: false
tags: ["ping"]
---

Mi è stato chiesto a che cosa possa servire avere aperte 35 applicazioni, <cite>oltre che a sfoggiare una insolità quantità di testosterone informatico</cite>. Ho guardato nel Dock com'è la situazione in questo momento esatto:

Finder - si attiva da solo.
<a href="http://www.barebones.com/products/bbedit" target="_blank">BBEdit</a> - Web writing e text processing.
<a href="http://skim-app.sourceforge.net/index.html" target="_blank">Skim</a> - ci leggo i Pdf meglio che con Anteprima.
<a href="http://www.wow-europe.com" target="_blank">World of Warcraft</a> - è World of Warcraft.
<a href="http://www.opencommunity.co.uk/vienna2.php" target="_blank">Vienna</a> - newsreader.
Terminale - sul mio Mac è sempre aperto.
X11 - serve per i programmi X11.
<a href="http://www.tex-edit.com" target="_blank">Tex-Edit Plus</a> - articoli e word processing.
<a href="http://www.bean-osx.com/Bean.html" target="_blank">Bean</a> - ci converto in .rtf i testi per i clienti che vogliono documenti strutturati e per aprire i file .doc semplici che arrivano, invece che TextEdit.
<a href="http://www.openoffice.org" target="_blank">OpenOffice</a> - tipicamente foglio di calcolo e file in formato OpenDocument.
Anteprima - per esaminare velocemente le immagini.
<a href="http://www.barebones.com/products/mailsmith" target="_blank">Mailsmith</a> - posta elettronica.
<a href="http://c-command.com/spamsieve/" target="_blank">SpamSieve</a> - antispam.
iChat - chat Aim (con la mia identità principale) e Jabber, audio e videoconferenza.
<a href="http://excalibur.sourceforge.net/" target="_blank">Excalibur</a> - controllo ortografico.
<a href="http://cyberduck.ch/" target="_blank">Cyberduck</a> - Ftp.
<a href="http://www.skype.org" target="_blank">Skype</a> - ho un cliente che vuole usare per forza Skype.
Rubrica Indirizzi - indirizzario.
QuickTime Player - lettore video.
iCal - agenda e <em>to do</em>.
<a href="http://www.rhapsoft.com" target="_blank">LiveQuartz</a> - editing rapido immagini.
iTunes - lettore musicale.
<a href="http://www.wikia.com/index.php/Freeciv" target="_blank">FreeCiv</a> - è il gioco di mercoled&#236; prossimo, dalle 13 alle 14, stanza iChat <code>gamefriday</code>.
<a href="http://pcgen.sourceforge.net" target="_blank">PcGen</a> - gestione della mia campagna di Dungeons &#38; Dragons.
Safari - web.
<a href="http://www.mozilla.com/en-US/firefox/" target="_blank">Firefox</a> - Google Docs, più una seconda casella di posta Gmail.
<a href="http://www.adiumx.com/" target="_blank">Adium</a> - chat Icq, Yahoo e Aim (alternativo a iChat).
iPhoto - ho appena scaricato gli ultimi scatti dalla fotocamera.
Backup - lo sto facendo. :-)
Utility Amministrazione AirPort - l'iMac di backup non vedeva la base e l'ho usato per riavviarla (tutto ok).
Calcolatrice - aperta quando rispondevo al commento di Paolo.

Se non erro sono 31. Effettivamente la Calcolatrice non mi serve. Potrebbero essere 30. Dov'è il testosterone?