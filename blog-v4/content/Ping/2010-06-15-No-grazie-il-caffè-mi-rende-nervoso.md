---
title: "No grazie, il caffè mi rende nervoso"
date: 2010-06-15
draft: false
tags: ["ping"]
---

L'amministratore delegato di Starbucks ha annunciato che dal primo luglio i 6.736 punti vendita americani di Starbucks <a href="http://www.computerworld.com/s/article/9178034/Free_Wi_Fi_digital_content_coming_to_Starbucks_July_1" target="_blank">offriranno connessione wifi illimitata e gratuita</a>. L'accesso, dicono, avverrà con un clic, senza procedure lunghe e complicate o moduli da riempire.

Fino al 30 giugno, dopo le prime due ore gratuite, si comprano pacchetti di due ore a 3,99 dollari. Dal primo luglio è gratis.

A inizio anno, McDonald's ha portato <a href="http://www.computerworld.com/s/article/9142402/McDonald_s_free_Wi_Fi_part_of_growing_trend" target="_blank">wifi libero e gratuito in 11.500 punti vendita americani</a> e punta di arrivare a quattordicimila.

Quando sento dire che noi italiani siamo furbi, vorrei tanto vedere aprirsi ogni volta in quel momento un punto di accesso <i>wifi</i> illimitato e gratuito in un ufficio postale, piazza di paese, filiale di banca, che so, Pastarito &#38; Pizzarito.

Certo, il nostro espresso non è secondo a nessuno. La fiorentina eclissa qualunque hamburger. Forza, allora, caffè e ristoranti, mostratemi che oltre al coperto potete offrire anche la copertura!