---
title: "Popolo sovrano"
date: 2006-10-05
draft: false
tags: ["ping"]
---

Referendum: volete voi che i link su questo blog aprano sistematicamente un'altra finestra del browser?

<strong><s&#236;> <no> <annulla></strong>

Il quorum per ritenere valida la consultazione è di undici voti.