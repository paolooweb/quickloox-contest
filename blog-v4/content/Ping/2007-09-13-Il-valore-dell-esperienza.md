---
title: "Il valore dell'esperienza"
date: 2007-09-13
draft: false
tags: ["ping"]
---

Se una persona ha diretto Macworld dalla preistoria fino a ieri, e ha vissuto una parte consistente della propria vita professionale immerso fino al collo nel mercato Mac, ne sa un bel po'.

Da qualche giorno, Enrico Lotti ha aperto <a href="http://applemania.blogosfere.it/" target="_blank">un proprio blog</a> su Apple. In bocca al lupo. :-))))