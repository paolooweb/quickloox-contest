---
title: "Il vecchio e il mare"
date: 2007-12-09
draft: false
tags: ["ping"]
---

Cito dalla mail di <strong>Ivo</strong>:

<cite>Sono sul treno.</cite>

<cite>Sto usando TextWrangler. Ho una frase, presa da un sito web, scritta con tutte le lettere in maiuscolo. So che TextWrangler ha una funzione per cambiare quello che gli inglesi chiamano “case” delle lettere.</cite>

<strong>Siccome non mi capita spesso di utilizzarla, non ho idea di dove si trovi questa funzione: Edit? Text? Tools?</strong>

<cite>Ho due possibilità: mettermi a cercarlo oppure ricopiare il testo. Con Leopard ho una possibilità in più: usare il menu Aiuto. Scrivo “case” nel riquadro di ricerca e lui subito mi trova la funzione “Change Case&#8230;” (che si trova nel Menu Text).</cite>

<cite>Il mio iBook G4 è sicuramente più lento del notebook nuovo fiammante che sta usando il mio vicino di posto sul treno. Ma grazie a Leopard il mio lavoro è più veloce e meno noioso.</cite>

Rispetto ai precedenti Mac OS X, Leopard ha davvero un mare di diversità; una, che ho avuto più occasioni di rimarcare e quindi mi farà sembrare ripetitivo (ma <em>iuvat</em>), è che per la prima volta da molti, molti anni, su un sistema operativo c'è un meccanismo di aiuto che talvolta serve davvero a qualcosa.