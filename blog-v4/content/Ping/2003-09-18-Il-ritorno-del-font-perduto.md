---
title: "Il ritorno del font perduto"
date: 2003-09-18
draft: false
tags: ["ping"]
---

Anche quando Mac OS X non sembra Mac, è solo questione di tempo

Quando System 7 ha reso possibile fare doppio clic su un font TrueType per mostrare una finestra di testo in cui visionare l’aspetto del font, per tutti fu un gran progresso.

Allo stesso modo, quando si vide che in Mac OS X 10.0 questo non era più possibile, ci fu chi protestò a gran voce. Sì, in Mac OS X 10.2 c’è nascosto tra le utility un programmino che bene o male arriva allo stesso risultato, ma non era proprio la stessa cosa, e nessuno ha voglia di mettersi a tirare giù menu o lanciare programmi solo per poter aprire il pannello Font.

Poco fa ho cliccato distrattamente su un font mentre usavo la Developer Preview di Mac OS X 10.3 ed è partito un nuovo programma, Font Book, che risolve il problema e fa anche più di quello.

Bisogna lamentarsi, ci vuole tempo, ma Apple ascolta.

<link>Lucio Bragagnolo</link>lux@mac.com
