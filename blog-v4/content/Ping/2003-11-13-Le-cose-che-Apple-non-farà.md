---
title: "Le cose che Apple non farà"
date: 2003-11-13
draft: false
tags: ["ping"]
---

Aspettative irragionevoli fondate su motivazioni irrazionali

Ogni tanto molti si convincono che Apple si imbarcherà in imprese improbabili per motivi storici, strategici, tecnici o semplicemente perché non conviene.

È il momento di fare il punto per il prossimo futuro prevedibile.

Apple non farà palmari. Ci ha perso soldi e la tecnologia attuale ristagna. Non è un mercato veramente profittevole. Gli iPod, per fare un esempio diverso, si vendono come il pane.

Apple non farà una suite da ufficio, iOffice o come la si voglia chiamare. L’utente medio vuole una cosa che non sia Word/Excel e tuttavia legga e scriva perfettamente i file Word/Excel, reggendo a qualsiasi livello di delirio compositivo dell’autore. La perfezione assoluta sull’elaborazione di documenti Office di complessità demenziale è impossibile. Ci sono già OpenOffice, ThinkFree Office, la suite Abi, AppleWorks, MacLinkPlus DeLuxe. Apple non ha alcun motivo per disperdere energie su questo tema.

Apple non passerà Mac OS X su processori Intel, a meno che non stia per annunciare il proprio suicidio commerciale. Che Darwin funzioni su Intel è completamente un’altra cosa.

Parla uno che riteneva impossibile la creazione di un iPod per Windows intanto che andava alla conferenza stampa in cui veniva presentato. Eppure mi sa tanto che stavolta non sbaglierò.

<link>Lucio Bragagnolo</link>lux@mac.com