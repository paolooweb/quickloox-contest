---
title: "Vorrei ma non passa<p>"
date: 2005-12-07
draft: false
tags: ["ping"]
---

Quelli che scaricano Broadband Tuner per convincersi di avere la banda larga, oh yeah<p>

Quando Apple ha fatto uscire Broadband Tuner, un sacco di gente lo ha installato senza leggere le avvertenze.<p>

Broadband Tuner è indicato per linee dati ad alta velocità e alta latenza, come quelle in tecnologia FiOs.<p>

Di tutte le nostre connessioni Adsl domestiche o piccolo professionale, praticamente nessuna ha necessità di approfittare di Broadband Tuner. Forse forse, FastWeb, se è la fibra e non la Adsl.<p>

Peraltro, tutto quello che <a href="http://www.apple.com/support/downloads/broadbandtuner10.html">Broadband Tuner</a> fa è aumentare i valori di tre buffer nel file /etc/sysctl.conf. Se il file non c&rsquo;è, lo crea.<p>

Nell&rsquo;informatica i miracoli non esistono. Confrontiamo la nostra linea, per esempio, con le offerte di <a href="http://www22.verizon.com/FiOSforhome/channels/FiOS/root/package.aspx">Verizon</a> negli Stati Uniti. È da guardare anche l&rsquo;upstream, i dati che partono da noi, non solo il downstream, quelli che arrivano.<p>

Non basta volere più banda per averla. Né installare Broadband Tuner. Se i dati che passano sono quelli, sono quelli.<p>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>