---
title: "Il primate va al sociale"
date: 2010-07-21
draft: false
tags: ["ping"]
---

Da questo blogghino modesto e senza pretese si sono sempre apprezzata l'intraprendenza e l'irriverenza del Macaco, che è partito dal niente e sta diventando con costanza e intelligenza un bel tutto.

Adesso, come si dice negli ambiti intellettuali, il Macaco si butta anche nel sociale e <a href="http://www.ilmacaco.com/un-nuovo-albero-per-i-macachi/" target="_blank">apre una sezione interamente nuova</a>, assai interattiva e collaborativa, delle sue scimmiesche iniziative, grazie alla collaborazione con il social network <a href="http://meemi.com/" target="_blank">Meemi</a>.

Da parte mia, i migliori auguri e un pronostico: il propulsore del Macaco, che tutti leggono senza accorgersene nei titoli di coda di certi canali televisivi italiani, finirà per diventare un grande imprenditore internettiano. <i>Ad maiora</i>!