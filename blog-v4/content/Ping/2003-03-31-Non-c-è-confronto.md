---
title: "Non c’è confronto"
date: 2003-03-31
draft: false
tags: ["ping"]
---

Un’architettura non è un player

Sento un sordo che dice “RealPlayer suona meglio di QuickTime”. Vedo un cieco spiegarmi che i file Flash sono migliori di quelli ci QuickTime.

Facciamola breve: QuickTime non è un player, o un formato di file: è un’architettura completa per la gestione di filmati, immagini, animazione e audio. Effettua digitalizzazione e riproduzione, streaming, elaborazione dei media. Contiene migliaia di routine utilizzabili e utilizzate da centinaia di programmi, che possono essere belli o brutti, ma non per colpa o merito di QuickTime. Praticamente tutti i programmi che usi si appoggiano a QuickTime in un modo o nell’altro.

Per fare confronti bisognerebbe almeno sapere che cosa si deve confrontare. Un po’ di rispetto per ciò che non si conosce, perbacco.

<link>Lucio Bragagnolo</link>lux@mac.com