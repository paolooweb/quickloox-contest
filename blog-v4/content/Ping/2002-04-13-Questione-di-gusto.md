---
title: "Questione di gusto"
date: 2002-04-13
draft: false
tags: ["ping"]
---

L’appeal tutto speciale del Mac emerge da dove meno te l’aspetti

Su una rivista qualunque ho visto distrattamente una pubblicità dell’ultima moda: i loghi per i telefonini. Decine di animaletti, omini, donnine, scrittucce, cantantini, fiorellini, cuoricini, bandierine, palloncini e varia altra amenità.
In mezzo, sempre distrattamente, ho notato una mela morsicata e una bella scritta “Apple”.
Allora, attentissimamente, ho provato a cercare “Windows”, piuttosto che “W Bill”, oppure “Office”. Niente. Zero. Nil.
Per un logo Windows non c’è proprio mercato.
Forse è perché non c’è gusto.

Allora <link>Lucio Bragagnolo</link>lux@mac.com