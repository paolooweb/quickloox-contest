---
title: "Saldi di stagione"
date: 2011-02-06
draft: false
tags: ["ping"]
---

Per salutare adeguatamente l'inverno, un'ultima nevicata. Aprire il Terminale nella cartella Utility del Finder e scrivere

<code>while true ; do NUM=$(( $RANDOM % 80 )) ; for i in $( seq 1 $NUM ) ; do echo -n " " ; done ; echo \* ; done</code>

Dare Invio. <code>Ctrl-C</code> per lasciare spazio alla primavera incipiente.