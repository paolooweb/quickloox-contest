---
title: "La non invenzione della videochat"
date: 2003-07-04
draft: false
tags: ["ping"]
---

Quando Apple non inventa, divulga

È uscito iChat AV beta pubblica. Vedo Salvia davanti al suo PowerBook 12”, con una Webcam davanti, che chiacchiera con un amico in viaggio dietro l’angolo, a Rio de Janeiro.

Non solo possono chattare, ma possono vedersi e sentirsi. La stessa differenza tra un disegno a matita e una foto tridimensionale.

In tutto questo non c’era niente che non si potesse fare prima. Solo che adesso ci ha messo le mani Apple.

Diventerà, anzi, è diventata una cosa facile. Attacchi la Webcam (magari iSight), lanci il programma e fai videoconferenza.

Apple ha inventato molte cose. Ma sono ancora di più quelle, già esistenti, che ha reso utilizzabili da tutti noi.

<link>Lucio Bragagnolo</link>lux@mac.com