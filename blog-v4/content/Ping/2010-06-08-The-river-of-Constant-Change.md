---
title: "The River of Constant Change"
date: 2010-06-08
draft: false
tags: ["ping"]
---

L'appuntamento in chat per l'apertura della Wwdc 2010 è stato godibile e succoso. Grazie a tutti per avere partcipato con allegria, creatività, intelligenza e rispetto. Grazie a <b>gand</b> per avere ancora una volta concesso lo spazio di <a href="http://freesmug.org" target="_blank">Freesmug</a>. Connettivamente svantaggiato, mi sono affidato a Macrumorslive, la pagina più veloce e leggera ad aggiornarsi, con ottimi risultati.

Come spesso accade, sono successe cose più interessanti di quanto possiamo renderci conto nell'istante. La più eclatante è il prossimo arrivo di un computer da tasca con lo schermo talmente definito che sarà quasi impossibile distinguerne i pixel, tanto mostrerà testo definito a livello di stampa <i>inkjet</i> su carta, su cui i programmi che mostrano testo si useranno meglio di prima, anche se nulla cambia all'osservatore distratto, corpo in vetro e funzione di cancellazione dei rumori molesti (cosa che per qualcuno, ho scoperto, vale l'acquisto da sola). Ce sono molte altre, tuttavia. Il mondo, una persona per volta, continua a essere cambiato.

Il titolo (<i>il fiume del costante cambiamento</i>) è una citazione dei vecchi Genesis e vediamo chi la trova per primo. :-)