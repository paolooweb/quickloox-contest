---
title: "La grande truffa del tablet"
date: 2010-01-27
draft: false
tags: ["ping"]
---

Si è aperta, ovviamente, la diga che tratteneva i commenti dei soloni incapaci di perdere una buona occasione di tacere.

Facile distinguere il grano dal loglio e il caffè dalla cicoria: quelli che pontificano di <i>hardware</i> non hanno capito niente e possono essere comodamente ignorati.

Il problema non è lo <i>hardware</i>, a parte ovviamente il fatto che funzioni e sia ergonomico, ma l'insieme, o l'ecosistema se si preferisce.

Di <i>tablet</i> ne sono stati costruiti a bizzeffe e tutti sono stati bellamente ignorati. Quando Microsoft annunciò anni fa il <i>tablet Pc</i> ebbe grande eco tra i media e fece un buco nell'acqua totale con le vendite.

Traduco un pezzettino da una recente <a href="http://news.cnet.com/2300-31021_3-10002287.html?part=rss&amp;subj=news&amp;tag=2547-1_3-0-20" target="_blank">rassegna grafica di Cnet</a>:

<cite>Proprio come con iPod e iPhone, Apple non ha inventato queste categorie; ha solo preso qualcosa che esisteva già e ha migliorato il suo utilizzo grazie a buono</cite> hardware <cite>che funziona in concerto con buon</cite> software<cite>. È il modo in cui iTunes funziona a rendere speciale iPod e sono iPhone OS e App Store le componenti più innovative di iPhone, per cui saranno contenuti e sistema operativo le chiavi</cite> [del successo] <cite>per il</cite> tablet <cite>di Apple.</cite>

L'articolo è corredato da una galleria di <i>tablet</i> o similari giustamente ignorati dai più.

A parlare di <i>hardware</i>, come se contasse quello, sono gli sprovveduti.

