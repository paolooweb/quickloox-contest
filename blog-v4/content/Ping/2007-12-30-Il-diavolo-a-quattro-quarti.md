---
title: "Il diavolo a quattro (quarti)"
date: 2007-12-30
draft: false
tags: ["ping"]
---

Lo avevo letto e ora l'ho vissuto: la compressione Apple Lossless di iTunes comprime molto più la musica classica di quella moderna. Causa feste sono arrivati in casa dischi di Bach e Satie e la dimensione finale compressa degli album risulta ben sotto il cinquanta percento dell'originale.

Senza perdere neanche una nota.

La compressione lossless vorrà sempre (molto) più spazio di una Aac, ma la musica resta tutta. Nel frattempo l'industria discografica <a href="http://www.rollingstone.com/news/story/17777619/the_death_of_high_fidelity/print" target="_blank">fa il diavolo a quattro</a> (quarti) per riuscire a fare suonare decentemente gli album quando vengono ridotti a Mp3. Dopo quanto visto per le foto, bisogna sentire queste cose sulla musica.