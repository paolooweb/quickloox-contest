---
title: "Servizi opzionali"
date: 2009-10-13
draft: false
tags: ["ping"]
---

Apple ha iniziato a popolare le proprie pagine di automazione Mac con <a href="http://www.macosxautomation.com/services/download/index.html" target="_blank">Servizi pronti da scaricare</a>, modificare se necessario, installare.

Da tenere d'occhio. Salta sempre fuori qualcosa di utile.