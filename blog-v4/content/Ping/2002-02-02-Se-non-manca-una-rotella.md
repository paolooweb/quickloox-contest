---
title: "Se non manca una rotella"
date: 2002-02-02
draft: false
tags: ["ping"]
---

Come liberarsi del cursore a rotazione infinita di Mac OS X

Prima di tutto: il cursore a rotazione infinita, come lo chiamano gli americani. è un segno che il sistema funziona! Non sono più i tempi di Mac OS 9. Semplicemente, è quella singola applicazione, che da un po’ non risponde ai comandi. Ma nel 99% dei casi basta aspettare e tornerà il controllo. Giurin giuretta.
Nessuna voglia di aspettare? Abituarsi a lavorare con la potenza di Unix. Un programma fa attendere? Si passa a un altro programma e si fa qualcos’altro, in attesa che le cose si sistemino.
Se invece la rotella dà proprio fastidio, c’è un trucchetto. Premere Comando-Opzione-Esc. Appare la finestra di Mac OS X che consente di uccidere i programmi a piacere. Perfetto. Dopo averla chiusa (senza bisogno di uccidere alcunché) è molto probabile che il cursore maledetto abbia smesso di ruotare.
Basta che non mi si chieda perché.

lux@mac.com