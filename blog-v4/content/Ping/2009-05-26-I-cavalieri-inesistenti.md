---
title: "I cavalieri inesistenti"
date: 2009-05-26
draft: false
tags: ["ping"]
---

Devono essere per forza fittizi e però ragionevolmente realistici.

I nomi di persona utilizzati negli <em>spot</em> televisivi di iPhone sono spesso piccoli capolavori di umorismo involontario. Riguardando le pubblicità su YouTube alla fine mi restano in mente i nomi più di ogni altra cosa.

Onore e gloria imperitura a questi cavalieri inesistenti (e un omaggio anche <a href="http://it.wikipedia.org/wiki/Il_cavaliere_inesistente" target="_blank">a Calvino</a>, via), come Luca Capsoni, Mara Rizzetto, Anna Donati, Patrizia Teti, Luca Vergano, Antonio Marani, Elisa Fiorillo, Fabio Raimo, Paolo Donati, Giuliana Smeraldi, Franco Marrari, Patrizia Fortis, Roberta Bettini, Matteo Di Giacomo, Franco Bertolini, Stefano Bonu, Camillo La Rovere, Raffaella Lillini, Fabrizia Marchi, Francesco Meglio, Benedetta Melli, Silvia Moroni e quanti altri (mi sa di non averli tutti&#8230; chi ne ha altri?) che con le loro surreali imprese balneari e di intrattenimento sociale contribuiscono al successo di iPhone. :-)