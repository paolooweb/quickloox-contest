---
title: "Tempo di saldi"
date: 2008-07-21
draft: false
tags: ["ping"]
---

Stavo per dimenticarmi di scaricare <a href="http://www.blambot.com/fonts.shtml" target="_blank">il font gratuito di luglio</a> dal sito di Blambot. :-)

La parte interessante è che la versione OpenType di Sanitarium ha le legature automatiche&#8230; cos&#236; non ci sono coppie di lettere esattamente uguali ad altre coppie.