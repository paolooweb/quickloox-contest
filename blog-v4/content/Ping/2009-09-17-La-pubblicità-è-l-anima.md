---
title: "La pubblicità è l'anima"
date: 2009-09-17
draft: false
tags: ["ping"]
---

Mille grazie a <a href="http://www.onicedesign.it/" target="_blank">Stefano</a> per avermi segnalato questa <a href="http://www.webdesignerdepot.com/2009/09/the-evolution-of-apple-ads/" target="_blank">compilation di pubblicità Apple</a> dalla nascita ai giorni nostri.

Segnalo solo, più o meno a metà della lunga pagina, la pubblicità che titola <cite>Introducing MessagePad 2000, the only handheld computer you can actually use</cite>.

La traduzione (<i>il solo computer palmare che tu possa realmente usare</i>) spiega tutto. È roba di dodici anni fa, io l'ho usato davvero, come faccio ad abboccare ai <i>netbook</i>? :-)

La <i>gallery</i> è presa dalla <a href="http://www.macmothership.com/gallery/gallerytextindex.html" target="_blank">Mothership Apple Advertising and Brochure Gallery</a>, molto più completa ma più impegnativa. Il tutto fa parte di <a href="http://www.macmothership.com/" target="_blank">The Mothership</a>, una della parole definitive sulla storia di Apple.