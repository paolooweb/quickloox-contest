---
title: "Sondaggio d’opinione"
date: 2003-06-16
draft: false
tags: ["ping"]
---

Come se non si scrivessero già abbastanza fregnacce

Ho scritto una piccola storia di Apple per Macworld cartaceo. Ne è venuto fuori il quadruplo di quello che Macworld aveva chiesto ed è bastato un weekend neanche tanto intenso.

A questo punto mi sto chiedendo se non sia il caso di scrivere una storia di Apple a tutto tondo, considerato che nelle quarantamila battute di Macworld non inizia neanche a starci tutto quello che si dovrebbe scrivere.

Ma interesserà a qualcuno? Sapere che la prima sede di Apple era in Mariani Avenue a Cupertino e la nuova sede di Infinite Loop è lì a due passi ha una importanza qualunque, seppure vaga? O, per parafrasare Venditti, sei finito a scaricare Mp3 anche tu?

<link>Lucio Bragagnolo</link>lux@mac.com