---
title: "Di nuovo lo sbarco in Normandia"
date: 2004-06-24
draft: false
tags: ["ping"]
---

L’iTunes Music Store funziona anche qui. No, là

Pare che nella prima settimana di operatività l’iTunes Music Store europeo abbia venduto la bellezza di ottocentomila canzoni. Nelle sole Francia, Germania e Gran Bretagna, che in popolazione non pareggiano il Noradamerica

Ancora una volta sono arrivati gli americani, come in Normandia nella seconda guerra mondiale, a portare libertà. Liberta musicale, che sembrerà poco ma non lo è e mostra come in effetti alla pirateria musicale ci sia una soluzione efficacissima, che si chiama prezzi giusti e cose fatte bene.

L’Italia, come nel 1944, ancora una volta va nella direzione giusta ma nel modo sbagliato. Speriamo che l’iTunes Music Store sbarchi presto ad Anzio.

<link>Lucio Bragagnolo</link>lux@mac.com