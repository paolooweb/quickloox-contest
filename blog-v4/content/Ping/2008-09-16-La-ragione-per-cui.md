---
title: "La ragione per cui"
date: 2008-09-16
draft: false
tags: ["ping"]
---

<a href="http://ipodpalace.com" target="_blank">Stefano</a> mi ha fatto notare l'esistenza di <a href="http://www.savorydeviate.com/pocketgnome/" target="_blank">Pocket Gnome</a>, programma per barare a World of Warcraft.

Non è tanto la funzione, quanto la realizzazione eccellente del programma stesso, che è <em>shareware</em> semiclandestino eppure levigato con la cura e la professionalità di un programma commerciale.

Di programmi per barare a World of Warcraft con Windows ce ne sono numerosi e fanno mediamente schifo.

Questo è favoloso e, spiega l'autore sul sito, una versione Windows non ci sarà mai.

Conseguenze della distribuzione gratuita di Xcode in tutte le copie di Mac OS X.