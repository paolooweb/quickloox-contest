---
title: "L'arte e la massa"
date: 2010-08-11
draft: false
tags: ["ping"]
---

Sono rimasto veramente sorpreso della qualità di gioco e di interfaccia offerta da <a href="http://itunes.apple.com/us/app/pocket-legends-3d-mmo/id355767097?mt=8#" target="_blank">Pocket Legends</a>, gioco di ruolo di massa su Internet disponibile per iOS, realizzato in computergrafica 3D e gratuito nella versione base.

Se si pensa che si può giocare gratis, c'è da rimanere sbalorditi. L'interazione con altri giocatori umani funziona, il gioco è semplice ma non stupido, l'interfaccia molto buona e su iPad è vero divertimento.

Se piace il genere, è uno scaricamento assolutamente obbligato, lo stato dell'arte per prestazioni e prezzo su iOS.

Il mondo di massa, con buona presenza di giocatori umani per quanto ho visto finora, diventa accessibile dopo avere completato il primo addestramento (io, imbranatissimo, ci ho messo dieci minuti). Chi vedesse un personaggio di nome Avialux gli faccia pure un salutino da parte mia. :-)