---
title: "Grandi piccolezze"
date: 2011-01-30
draft: false
tags: ["ping"]
---

Una lezione universitaria di design di interfacce? Visitare <a href="http://littlebigdetails.com/" target="_blank">Little Big Details</a>, raccolta di dettagli spesso minuscoli che nobilitano il software e lo portano a un livello di qualità e raffinatezza superiore.

C'è di tutto, da Mac OS X a Cnn, Google, Twitter, Chrome. Qualcuno faccia un fischio quando trova un riferimento a Windows.