---
title: "Il signore dei calendari"
date: 2004-08-02
draft: false
tags: ["ping"]
---

Dalla Terra di Mezzo al Terminale con un solo comando

Il comando (da dare nel Terminale) è

cat /usr/share/calendar/calendar.history | grep "LOTR"

Si vedrà che tra i programmatori Unix ci sono anche fedelissimi tolkieniani.

<link>Lucio Bragagnolo</link>lux@mac.com