---
title: "Va scritto chiaro"
date: 2009-02-12
draft: false
tags: ["ping"]
---

Nella sede dell'<a href="http://www.calligrafia.org/" target="_blank">Associazione Calligrafica Italiana</a>, che eredita secoli di storia e tradizione, si vedono in giro solo MacBook Pro.

È&#8230; un segno.