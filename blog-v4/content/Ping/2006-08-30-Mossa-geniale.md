---
title: "Mossa geniale"
date: 2006-08-30
draft: false
tags: ["ping"]
---

Cito integralmente <strong>Mario</strong>:

<cite>Una delle prime cose che ho fatto dal lato Mac appena tornato dalle vacanze è stato acquistare la licenza educational di BBEdit.</cite>

<cite>Probabilmente è una delle cose migliori che abbia fatto in vita mia per quanto riguarda il Mac.</cite>

<cite>Mi sono anche iscritto alla mailing list di BBEdit e mi si sta spalancando un nuovo universo.</cite>

<cite>Se vuoi ti tengo aggiornato (della serie: BBEdit for Dummies).</cite>

Io voglio e, se Mario mi aggiornerà davvero, finirà citato ancora, spesso e volentieri. <a href="http://www.barebones.com/products/bbedit/" target="_blank">BBEdit</a> è una mossa geniale, di quelle che nel resoconto di una partita a scacchi hanno il punto esclamativo a seguire.

Per la precisione, penso che Mario si sia iscritto a <a href="http://www.barebones.com/support/lists/bbedit_talk.shtml" target="_blank">BBEdit-Talk</a>, e c'è anche <a href="http://www.barebones.com/support/lists/bbedit_script.shtml" target="_blank">BBEdit-Scripting</a>.