---
title: "Un browser per crearli, un browser per gestirli, un browser per domarli&#8230;"
date: 2006-07-12
draft: false
tags: ["ping"]
---

Se ho capito bene, Safari è incompatibile con Gmail al momento di creare un nuovo account. Ho appena provato e non c'è stato verso, ho dovuto usare Firefox