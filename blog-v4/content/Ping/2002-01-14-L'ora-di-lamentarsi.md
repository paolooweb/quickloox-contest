---
title: "L’ora di lamentarsi"
date: 2002-01-14
draft: false
tags: ["ping"]
---

La più coraggiosa transizione dopo il passaggio da 680x0 a PowerPc si sta compiendo: tra poco il sistema operativo attivo predefinito sui Mac sarà Mac OS X.
All’inizio Mac OS X era l’obiettivo naturale dei coraggiosi; poi sono arrivati i precursori. Da qualche tempo ci sono arrivati anche i cosiddetti early adopter, quelli che che tendono a usare la tecnologia appena è pronta.
Qualche giorno e Mac OS X sarà un prodotto per utenti finali.
Un breve invito, come direbbe il filosofo Manlio Sgalambro: lamentatevi.
Qualcosa non va? Ditelo. Manca il supporto di una periferica? Chiedetelo. Trovate che qualche aspetto sia da migliorare, aggiungere, togliere, cambiare? Fatevi sentire.
L’indirizzo è http://www.apple.com/macosx/feedback. Non lasciatevi intimidire dall’interfaccia in inglese e, se non avete alternative, scrivete in italiano.
Più si scrive ad Apple, più Apple ascolterà.

lux@mac.com