---
title: "Medio termine"
date: 2011-02-19
draft: false
tags: ["ping"]
---

<a href="http://www.macworld.it/ping/life/2011/02/18/sdegno-damante-poco-dura/comment-page-1/#comment-14093" target="_blank">Commenta</a> <b>iSimone</b>:

<cite>Me la segno e fra sei mesi, se non è vero, vengo qui a rinfacciarglielo ogni giorno&#8230;
</cite>

Sarebbe bellissimo se accadesse davvero. Senza scherzi; il problema vero dell'informazione attuale &#8211; mica solo Mac &#8211; è proprio che si può scrivere qualsiasi cosa, tanto non si paga mai pegno.

Per quanto mi riguarda, avere alla calcagna persone che mi chiedono conto delle cose che ho scritto è ricevere un grande favore e incoraggio con amicizia chiunque a farmi le pulci sempre e comunque su quello che affermo.

Non so chi ricorda l'<i>antennagate</i>, il grave problema all'antenna che affliggeva iPhone 4 e che forse avrebbe costretto Apple a richiamare tutti gli apparecchi venduti&#8230; non sto a linkare, ma tra agosto e settembre si parlava a malapena di qualsiasi altra cosa.

Non se ne parla più e gli iPhone 4 si vendono a carrettate. Se è un problema è cos&#236; grave, perché nessuno più ne parla, quando si vendono molti più iPhone di prima e dovrebbe anzi emergere maggiormente? Se non è un problema o almeno non è cos&#236; grave, perché se ne parlava in quei termini apocalittici?

Adesso, ribadisco, si fanno le polemiche sugli abbonamenti via App Store. E ribadisco che tra sei mesi non ne parlerà più nessuno.

S&#236;, spero che tutti se la segnino sull'agenda e si ripromettano di controllare.

Nel frattempo Advertising Age riporta che Elle, Nylon e Popular Science <a href="http://adage.com/mediaworks/article?article_id=148920" target="_blank">hanno aderito al meccanismo di App Store</a>. La seconda è ignota a noi italiani, ma la terza - seppure inglese &#8211; è vendutissima e Elle è un periodico multinazionale a tutti noto.