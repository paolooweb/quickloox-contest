---
title: "Three (is a magic number)"
date: 2007-06-30
draft: false
tags: ["ping"]
---

iTunes 3: nessun problema.

Safari 3(.0.2): nessun problema.

Mac OS X 10.4.10 (il terzo della lista): nessun problema.

A Piero non parte <a href="http://www.kloogeinc.com" target="_blank">Klooge</a> sul suo MacBook Pro 15&#8221; e ci sto impazzendo sopra. Non partiva neanche prima, però.