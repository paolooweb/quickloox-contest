---
title: "Quando è arte la vedi"
date: 2009-05-01
draft: false
tags: ["ping"]
---

David Hockney, settantunenne artista inglese, <a href="http://www.dailymail.co.uk/news/article-1175521/iHockney-Artist-David-uses-Apple-phone-paint-mini-masterpieces.html" target="_blank">dipinge fiori su iPhone</a> per mandarli agli amici. E ha pure il cavalletto apposta.

Quando comanda l'artista e non lo strumento si vede subito.