---
title: "Tre in un colpo"
date: 2007-02-16
draft: false
tags: ["ping"]
---

<a href="http://docs.info.apple.com/article.html?artnum=304586" target="_blank">Aggiornamento Java</a>, Security Update e aggiornamento ora legale: installati, riavviato il Mac, tutto funziona.

Nessun riavvio indesiderato da inizio anno. Sono in vantaggio sul 2006. Più di questo, nessun aggiornamento, l'anno scorso o questo, ha dato il minimo problema.