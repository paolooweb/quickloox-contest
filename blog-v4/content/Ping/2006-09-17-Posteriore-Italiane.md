---
title: "Poste(riore) italiane"
date: 2006-09-17
draft: false
tags: ["ping"]
---

In fondo Mac non c'entra molto con questa vicenda. O forse s&#236;.

Non avevo mai avuto l'onore di inviare una raccomandata digitale via <a href="http://www.poste.it" target="_blank">Poste.it</a>.

Tra i formati ammessi dalle Poste per il file contenente la raccomandata, non c'è il Pdf. C'è però il formato Word.

Poco male, penso. Sono vittime di Microsoft, ma per venirne a capo mi basta lanciare TextEdit.

Salvo il file con il suo bel .doc e lo invio alle Poste.

Che, prima di inoltrarlo, lo trasformano in Pdf.

Ma non permettono di inviare un Pdf in prima battuta.

Certamente questa strabiliante procedura è stata pensata da qualcuno. A ognuno stabilire con che parte del corpo.