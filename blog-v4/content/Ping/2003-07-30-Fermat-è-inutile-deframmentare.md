---
title: "Fermat: è inutile"
date: 2003-07-30
draft: false
tags: ["ping"]
---

Una risposta definitva: deframmentare non serve

Ho sempre sospettato che la deframmentazione dei dischi fosse un eccesso di attenzione, un po’ come quelle persone che lucidano la carrozzeria dell’auto tre volte a settimana.

Ora ne sono sicuro. La frammentazione, manco a dirlo, è un difetto del sistema operativo del disco in uso sui PC. Su Mac OS classico e Mac OS X, HFS e HFS Plus svolgono il loro lavoro meglio e il problema della frammentazione dei file è praticamente inesistente se non quando il disco è pieno al 99% (punto in cui comunque deframmentare sarebbe impossibile per mancanza di spazio di manovra).

Come avrebbe forse detto il grande matemetico Fermat, posso dimostrarlo, ma non ho spazio sufficiente in questa rubrica.

Fidati e non deframmentare. Risparmi tempo e magari denaro.

<link>Lucio Bragagnolo</link>lux@mac.com

