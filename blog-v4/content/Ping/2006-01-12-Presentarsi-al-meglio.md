---
title: "Presentarsi al meglio"
date: 2006-01-12
draft: false
tags: ["ping"]
---

Rubo e rendo in italiano alcune considerazioni dal <a href="http://blog.guykawasaki.com/" target="_blank">blog di Guy Kawasaki</a>, uno che se sai chi lo leggi sempre, se non lo sai &egrave; meglio che cominci, riguardo allo stile di presentazione di Steve Jobs durante il keynote del Macworld Expo.

<ul type="circle">
	<li>Testo al minimo. Molte slide contengono al massimo una o due parole.</li>
	<li>Font enorme. Anche lo spettatore numero tremila, in fondo alla sala, pu&ograve; leggere lo schermo.</li>
	<li>Pochi bullet e inseriti uno per volta.</li>
	<li>Molte schermate di effetto. Un sistema operativo bello, celia Guy, aiuta.</li>
	<li>Demo del software, eseguite di persona; l&rsquo;amministratore delegato <em>sa</em> usare i prodotti della sua azienda.</li>
	<li>Uso accattivante del video. Filmati brevi che catturano l&rsquo;attenzione e ben dispongono. Normalmente le presentazioni includono clip da dieci minuti pieni di marketing mortalmente noioso e discorsi incomprensibili.</li>
	<li>Aspetta, c&rsquo;&egrave; ancora qualcosa: il colpo finale (MacBook Pro, in questo caso) viene annunciato e preparato.</li>
	<li>Il tocco umano: ringraziamenti per chi ha lavorato, in Apple e perfino in Intel, ai prodotti presentati.</li>
</ul>

Non mi tocca, grazie al cielo, trovarmi su un palco davanti a tremila persone sapendo che ogni strizzata d&rsquo;occhio vale milioni di dollari in Borsa. Per&ograve; girano talmente tante presentazioni obbrobriose che qualche consiglio aiuta sempre.