---
title: "Complessità personali"
date: 2009-03-19
draft: false
tags: ["ping"]
---

A proposito di Time Machine, della semplicità, della personalizzazione, delle opzioni, c'è un complicatissimo esempio su MacOSXHints di come farsi  <a href="http://www.macosxhints.com/article.php?story=20090315132133858" target="_blank">un sistema di backup su misura</a> con SuperDuper e un corredo di script vari.

Non è un invito a provarci - è veramente complicato - ma a cercare di capire il principio. Anche solo studiarlo sarebbe una occasione eccezionale di imparare molto.

La vera lezione, tuttavia, è su come funziona filosoficamente un computer. Un computer, da solo, non fa niente. Più software ci metti, più cose fa. L'obiettivo massimo è avere un computer che fa esattamente tutto quello che vogliamo e non di più e non di meno.

Significa che il suo software di base deve essere quello essenziale e nient'altro, per non correre il rischio di metterci in casa cose inutili.

Questo perché grazie al lavoro degli sviluppatori indipendenti, grazie ai <em>plugin</em>, grazie ai tutorial, grazie alle utility, grazie ai sistemi di scripting, grazie all'inventiva personale, grazie a Internet ciascuno può scegliersi il grado di personalizzazione e di approfondimento che desidera.

Per questo TextEdit è TextEdit. Il minimo indispensabile e quello che c'è di più è anche troppo. Ci sarà a chi piace TextEdit e a chi no. Per chi no, si possono scegliere infinite modalità di personalizzazione, tra Word, OpenOffice, Pages, BBEdit, TextMate, LaTeX, Mellel, Emacs e potrei proseguire per sempre o quasi.

Lascio la parola a Steve Jobs.

<cite>So che avete mille idee su tutte le fantastiche funzioni che iTunes potrebbe avere. Anche noi ne abbiamo. Ma non vogliamo mille funzioni. Sarebbe orribile. L'innovazione non è dire s&#236; a tutto. È dire NO a tutto fatta eccezione per le funzioni più critiche.</cite>