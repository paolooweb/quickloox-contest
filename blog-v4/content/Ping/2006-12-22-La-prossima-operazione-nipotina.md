---
title: "La prossima operazione nipotina"
date: 2006-12-22
draft: false
tags: ["ping"]
---

La nipotina aspetterà. Il Tangerine si è rifiutato di andare oltre Mac OS X 10.1. Era aggiornato nel firmware, ma ogni tentativo di aggiornare a Jaguar è stato vano. L'aggiornamento a Panther non è riuscito, causa errori non meglio specificati, e il Tangerine non bootava più né in Mac OS 9 né in Mac OS X.

Il tentativo di reinstallare da zero Mac OS X 10.1 non ha portato a risultati utili. Ora il Mac accenna al boot ma non va oltre mostrare l'icona di una cartella sistema rotta in due (tra l'altro non l'avevo mai vista prima).

Teoricamente le possibilità a disposizione sarebbero ancora molte, ma il tempo non vale il risultato e ho anche il sospetto che qualcosa non andasse per il verso giusto dall'inizio.

La nipotina non avrà il Tangerine, non a Natale almeno. Terrò gli occhi aperti nel caso arrivasse un iBook conchiglione.