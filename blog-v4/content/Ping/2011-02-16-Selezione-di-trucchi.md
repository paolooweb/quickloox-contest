---
title: "Selezione di trucchi"
date: 2011-02-16
draft: false
tags: ["ping"]
---

Due carinerie su Mac OS X Hints.

In iOS si seleziona un paragrafo di testo non solo toccandolo due volte con due dita (tocco al quadrato?), ma anche <a href="http://hints.macworld.com/article.php?story=20110213113700717" target="_blank">passando le due dita in questione</a> sul paragrafo suddetto. Trovo più azzeccato il tocco al quadrato, però ognuno ha le proprie preferenze e voglio effettuare qualche test in più dentro Pages su iPad (dal quale continuano a uscirmi articoli e pezzi vari; chi ha ancora il coraggio di sostenere che con la tastiera virtuale non vale la pena di scrivere?).

Per chi usa con grande frequenza Spaces su Mac OS X, ecco il sistema per <a href="http://hints.macworld.com/article.php?story=20110214074550501" target="_blank">disabilitare le animazioni di passaggio da uno spazio a un altro</a>. Si apre il Terminale (sta nella cartella Utility) e si scrive

<code>defaults write com.apple.dock workspaces-swoosh-animation-off -bool YES &#38;&#38; killall Dock</code>

Dopo avere premuto il tasto Invio, è fatta.

Per riportare le animazioni si scrive

<code>defaults delete com.apple.dock workspaces-swoosh-animation-off &#38;&#38; killall Dock</code>

Uno dei rari casi in cui non si inverte una selezione dando un <i>NO</i> al posto di uno <i>YES</i> come usa quasi sempre.