---
title: "Mancava solo il karaoke"
date: 2004-05-09
draft: false
tags: ["ping"]
---

Certo cose si fanno (facilmente e al volo) solo con i Mac

Notte a Webbit, nell’arena aperta 24 ore su 24, e decine di irriducibili che nonostante il sonno si guadagnano il Nutella party, tempo extra per fare quello che credono con i loro computer e magari un megaschermo abbandonato su cui proiettare l’ultimo Dvd.

A un tavolo si ritrova il team del PowerBook Owners Club, quelli di MacUsato e un po’ di indipendenti, tutti con un Mac.

Solo uno ha le casse, ma non è un problema. Tutti aprono i loro iTunes e condividono la propria libreria. Si apre una stanza di iChat dove ognuno può suggerire al deejay di turno che cosa suonare e da che libreria prenderlo, ed ecco risolto in trenta secondi il problema del sottofondo musicale!

<link>Lucio Bragagnolo</link>lux@mac.com