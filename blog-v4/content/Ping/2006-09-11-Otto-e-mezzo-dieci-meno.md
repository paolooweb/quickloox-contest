---
title: "Otto e mezzo, dieci meno"
date: 2006-09-11
draft: false
tags: ["ping"]
---

Alla scoperta di <a href="http://www.barebones.com/products/bbedit" target="_blank">BBEdit 8.5</a>. Non mi piace esteticamente la nuova toolbar (sono un minimalista ed è diventata più grafica, per quanto il numero dei pulsanti sia stato beneficamente ridotto). Funzionalmente, c'è un oceano di cose da scoprire.

Mi sono già imbattuto nel <em>code folding</em>, la possibilità di contrarre paragrafi o sezioni di testo per renderle momentaneamente invisibili. Mi riservo di investigare che cosa è cambiato in seguito al passaggio concettuale da <em>Glossary</em> a <em>Clippings</em>.

L'aiuto è veramente di aiuto, forse uno dei pochi a risultare interessante da leggere di per sé.

Il <em>gutter</em> e le sue possibilità di visualizzazione sono piuttosto utili per vedere bene la strutturazione dei tag in Html. Forse anche per testi tecnici, dovrò scoprirlo.

Impressionante il controllo immediato di tutti i menu e tutti gli elementi dei menu. Da questo punto di vista l'interfaccia è totalmente sistemabile a piacere, in modo molto pratico, scorciatoie comprese.

Darò una seconda possibilità all'integrazione con l'Ftp, provata secoli fa e improponibile. Qualcosa mi dice che le cose sono migliorate.

Complessivamente e superficialmente, volto alto.