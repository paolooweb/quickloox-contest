---
title: "Il lampo del cigno"
date: 2009-07-22
draft: false
tags: ["ping"]
---

Bisogna scaricare la versione più recente di WebKit e ci si ritrova sulla scrivania un <i>browser</i> che funziona come Safari, ma è la versione in via di sviluppo di Safari, con un'icona uguale a quella di Safari, solo che la bussola è dorata.

Fatto ciò, con WebKit si caricano le pagine con i <a href="http://webkit.org/blog-files/3d-transforms/poster-circle.html" target="_blank">circoli rotanti</a> e le <a href="http://webkit.org/blog-files/3d-transforms/morphing-cubes.html" target="_blank">transizioni e trasformazioni tridimensionali</a>.

La <a href="http://webkit.org/blog/386/3d-transforms/" target="_blank">spiegazione dell'interesse per queste pagine</a> è la loro realizzazione in Html 5 con Css e JavaScript. E nient'altro.

Le stesse cose, dirà uno, si possono fare in Flash.

Certo, ma Flash consuma molta più Cpu.
Html 5 è aperto, Flash è chiuso.
La stessa pagina Html 5 funziona su tutti i computer dell'universo, Flash vuole un <i>plugin</i> per sistema operativo.
La pagina Html 5 pesa molto meno.
Non serve neanche un <i>plugin</i>.
La stabilità del <i>browser</i> senza il <i>plugin</i> Flash è matematicamente migliore di quella con il <i>plugin</i> Flash.

La mia sensazione è che Flash stia iniziando il passo d'addio. Tempo tre anni e lo si potrà togliere dalle scrivanie. Quasi quasi sono tentato di farlo da subito&#8230;