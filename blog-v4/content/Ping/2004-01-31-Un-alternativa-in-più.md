---
title: "Un’alternativa in più"
date: 2004-01-31
draft: false
tags: ["ping"]
---

Piccola, elementare, fondamentale aggiunta a TextEdit in Panther

TextEdit di Mac OS X 10.3 apre e registra file anche in Formato Word.
So già che adesso qualcuno si metterà a questionare la “compatibilità”. So ugualmente che due giorni fa mi è arrivato un file Word che AppleWorks non apriva. Invece di trascinarlo su MacLinkPlus Deluxe l’ho trascinato su TextEdit, e si è aperto.

Poi dicono che non ci sono alternative a Word. Ce n’è una in più, invece.

<link>Lucio Bragagnolo</link>lux@mac.com