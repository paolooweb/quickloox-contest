---
title: "Senso vietato"
date: 2010-06-05
draft: false
tags: ["ping"]
---

Due le osservazioni più sciocche raccolte tra le numerose (e spesso sensate) su iPad.

Quella che vince in assoluto è la domanda <i>ma in quanto tempo si opacizza lo schermo a causa dello sfregamento delle dita?</i> Al che uno risponde che lo schermo è di vetro e non di marmo e le scritte che appaiono a video non sono serigrafate ma corrispondono a cristalli liquidi e si rende conto come la risposta sia talmente sciocca che la domanda doveva esserlo un bel po.

Secondi in classifica arrivano gli <i>arbiter elegantiarum</i> che si stracciano le vesti perché su un iPad utilizzato compaiono, ohibò, i segni delle ditate, che uno poi ci fa brutta figura in salotto con gli amici.

È gente nelle cui tastiere dei di cui computer vivono colonie di batteri &#8211; se son solo batteri &#8211; che fanno invidia a uno zoosafari, solo che non si vedono e allora va bene tutto.

iPad è una versione 1.0 e ci sono evidentemente cose migliorabili, numero uno il trasferimento di file tra iWork su Mac e iWork su iPad. Tuttavia non è obbligatorio dire cose insensate solo perché sono venute in mente, ecco.

Semmai, qualcuno dica a quelli di Wordpress che la <i>app</i> omonima ha bisogno di una rinfrescata; sulla tastiera di iPad viene voglia di sprecarsi ad aggiungere un corsivo o un grassetto, ma l'interfaccia, per quanto mi sembra di capire, non ci arriva ancora. 