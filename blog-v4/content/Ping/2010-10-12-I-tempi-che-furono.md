---
title: "I tempi che furono"
date: 2010-10-12
draft: false
tags: ["ping"]
---

Su Slashdot è comparso un commento che ho trovato esilarante. La <a href="http://apple.slashdot.org/story/10/10/11/2144221/iPhone-Opens-Up-Bluetooth-For-Data?from=rss" target="_blank">notizia</a> si riferisce a un prodotto <i>hardware</i> che, pare, per la prima volta riesce a scambiare dati con un iPhone via Bluetooth.

Il commento si riferisce al fatto che i primi <i>smartphone</i> Windows Phone 7 non hanno il copia e incolla:

[digita &#8216;copia e incolla' nella schermata di aiuto di Windows Phone 7]
<i>Ciao! Sono Clippy! Sembra che ti piacerebbe il &#8216;copia e incolla', è corretto?</i>
[tocca &#8216;S&#236;']
<i>Mi dispiace, Windows Phone 7 di Microsoft non supporta il &#8216;copia e incolla' per ora, ti piacerebbe il &#8216;copia e incolla'?</i>
[tocca ancora &#8216;S&#236;']
[clessidra]
<i>Ciao! sono Bob! Dove vuoi andare oggi?</i>
[digita &#8216;a comprare un altro telefono']
[clessidra]
<i>Ciao! Sono Clippy! Sembra che ti piacerebbe comprare un altro Windows Phone 7 di Microsoft, è corretto?</i>
[tocca &#8216;No']
[clessidra]
<i>Ciao! Sono Bob! Ti piacerebbe comprare un Kin di Microsoft?</i>

Qualche spiegazione per capire meglio.

<a href="http://www.google.com/images?client=safari&amp;rls=en&amp;q=clippy&amp;oe=UTF-8&amp;um=1&amp;ie=UTF-8&amp;source=univ&amp;ei=ooK0TODBOcLIswbag_HFBg&amp;sa=X&amp;oi=image_result_group&amp;ct=title&amp;resnum=2&amp;ved=0CC0QsAQwAQ&amp;biw=1036&amp;bih=1109" target="_blank">Clippy</a> era un fermaglio animato caratteristico dell'aiuto contestuale di Office, noto per comparire non sempre a proposito e non sempre con suggerimenti appropriati.

La clessidra è stata a lungo il simbolo di attesa su Windows.

<a href="http://toastytech.com/guis/bob.html" target="_blank">Bob</a> è stato uno dei maggiori fallimenti di Microsoft in tema di interfaccia utente, datato al primo 1995.

<i>Dove vuoi andare oggi?</i>, <i>Where Do You Want to Go Today?</i>, è stato slogan pubblicitario Microsoft di metà anni novanta.

<a href="http://arstechnica.com/microsoft/news/2010/06/inform-the-next-of-kin-microsofts-tweenager-phone-is-no-more.ars" target="_blank">Kin</a> è uno <i>smartphone</i> apparso quest'estate negli Stati Uniti, venduto dall'operatore Verizon e prodotto da Microsoft (che aveva comprato l'azienda produttrice), con software ibrido tra il vecchio Windows Mobile e il nuovo Windows Phone 7. Ne sono stati venduti pochissimi (si dice al massimo undicimila in circa due mesi) e, dopo un disperato tentativo di piazzarne qualcuno in più a metà prezzo, Verizon ha addirittura restituito l'invenduto a Microsoft. L'esito ha contribuito a costare a Steve Ballmer, amministratore delegato, circa mezzo milione di dollari di <i>bonus</i> di produzione.

Anche iPhone ha esordito senza copia e incolla. Ma era il 2007.