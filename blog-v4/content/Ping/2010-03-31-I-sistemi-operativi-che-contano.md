---
title: "I sistemi operativi che contano"
date: 2010-03-31
draft: false
tags: ["ping"]
---

Quando Snow Leopard <a href="http://support.apple.com/kb/TS2419" target="_blank">ha introdotto</a> le <a href="http://physics.nist.gov/cuu/Units/binary.html" target="_blank">unità corrette di capacità</a> dei dischi rigidi se ne sono lette di tutti i colori, compresa l'idea che Apple intendesse imbrogliare facendo apparire più grandi i dischi rigidi(!).

Adesso Ubuntu, una delle versioni fondamentali di Linux, <a href="https://wiki.ubuntu.com/UnitsPolicy" target="_blank">imita</a> Snow Leopard (e Debian, altro Linux fondamentale, <a href="http://wiki.debian.org/ConsistentUnitPrefixes" target="_blank">è sulla buona strada</a>).

L'informatica si avvicina sempre più al mondo delle persone normali ed è solo un bene.