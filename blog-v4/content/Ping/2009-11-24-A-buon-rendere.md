---
title: "A buon rendere"
date: 2009-11-24
draft: false
tags: ["ping"]
---

Iniziano ad arrivarmi gli inviti per Google Wave. Essendoci arrivato grazie a un gentilissimo invito, il meno che possa fare è mettere a disposizione i miei.

Siccome sono limitati (al momento dispongo di <i>sette</i> &#8220;ingressi&#8221;) e siccome dall'ultima chiacchiera è passato tempo (non è detto che chi aveva chiesto abbia ancora bisogno e viceversa), funziona cos&#236;.

Fase uno: chi desidera un invito <a href="mailto:lvcivs@gmail.com?subject=Da%20Ping!%20a%20Google%20Wave">mi scriva su Google</a>.

Fase due: per non penalizzare chi ha poco tempo, aspetto ventiquattro ore da adesso. Poi, se posso invitare tutti, invito tutti; se ci sono richieste in eccesso, sorteggio e invito i fortunati fino a esaurire la disponibilità. Gli inviti vengono inviati all'indirizzo che è stato usato per scrivermi e a nessun altro, senza eccezioni.

Quando arrivano altri inviti, rinnovo l'avviso e ripeto la procedura. Chi non avesse avuto l'invito dovrà riscrivere (magari nel frattempo ne ha avuto uno altrove).

Quando non mi arrivano più richieste, smetto di rinnovare l'avviso.

Più facile a farlo che a raccontarlo. Attendo. :-)