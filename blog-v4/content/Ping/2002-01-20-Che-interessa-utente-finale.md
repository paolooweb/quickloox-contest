---
title: "Che cosa interessa al mitico utente finale"
date: 2002-01-20
draft: false
tags: ["ping"]
---

Ognuno ha la sua idea meravigliosa per il successo di Apple. La verità è che...

Italia: il Paese di quaranta milioni di commissari tecnici della nazionale, altrettanti Presidenti del consiglio e altrettanti Risolutori dei Grandi Problemi Mondiali con una Idea Semplice ed Efficace.
Ci sono anche quaranta milioni di Ceo di Apple, illuminati da ciò che Steve Jobs ancora non è riuscito a capire e che evidentemente porterebe Apple al successo planetario.
Quelli che pensano Apple come un’azienda solo software, quelli che vogliono il palmare a tutti i costi, quelli che vogliono il mouse a tre pulsanti, quelli che ci vogliono i processori a un gigahertz.
Nessuno di questi talenti ha mai passato una giornata a fare il dimostratore Apple in un centro commerciale.
Nessuno chiede i megahertz, nessuno chiede dei pulsanti: vogliono fare, subito, facilmente, e che costi poco, o che faccia molto bene.
Apple ha scelto la seconda strada e non potrà mai produrre oggetti superiori a costi inferiori
I commissari tecnici se ne rendano conto. E pensino che Apple paga decine di persone per capire che prodotti vuole la gente. Forse non ci sono soluzioni così semplici da essere spiegate in tre righe.

lux@mac.com