---
title: "Un passo economico per l'umanità"
date: 2009-07-22
draft: false
tags: ["ping"]
---

Ricordarsi che, in occasione del quarantesimo anniversario dello sbarco sulla Luna, <a href="http://www.carinasoft.com" target="_blank">Carina Software</a> offre gratis oppure al costo di un centesimo di dollaro tutta la propria produzione software. Solo per oggi.

Per l'appassionato di astronomia è materiale interessante e i prezzi normali arrivano anche fino a cento dollari.

Ho già provveduto per mio papà. :-)