---
title: "Pagare moneta, vedere cammello"
date: 2008-10-16
draft: false
tags: ["ping"]
---

Una particolarità che sarebbe bello vedere su molto più software: gli appassionati di gioco di ruolo possono provare <a href="http://markdamonhughes.com/Perilar/" target="_blank">Perilar</a> su iPod touch.

Solo che il gioco costa 4,99 dollari. E se non piacesse?

Sul sito dell'autore esiste una <a href="http://markdamonhughes.com/JPerilar/" target="_blank">versione in Java</a> che è possibile provare gratis su Mac.

È meno aggiornata di quella per iPod touch. Però, per provare il gioco e vedere se piace, ha il costo ideale.