---
title: "Fuori la quota"
date: 2006-06-21
draft: false
tags: ["ping"]
---

Apple ha inanellato numerosi trimestri positivi. La crescita &egrave; complessivamente superiore alla media dell&rsquo;industria, iPod &egrave; il player musicale per definizione, la transizione a Intel per ora sta andando bene, fatturato in crescita, utili sostanziosi, piani di crescita per il futuro. Sembra che le cose stiano andando bene.

Eppure la quota di mercato di Apple &egrave; praticamente la stessa di pochi anni fa, punto pi&ugrave;, punto meno.

C&rsquo;era un tempo in cui non si parlava d&rsquo;altro. Senza quota di mercato Apple doveva scomparire, non aveva futuro, era condannata. Alla prova dei fatti, sembra che Apple possa durare o scomparire, ma certamente non per via della quota di mercato.

Dove &egrave; finita, la quota di mercato?