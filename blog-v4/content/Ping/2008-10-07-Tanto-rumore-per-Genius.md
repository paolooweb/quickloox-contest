---
title: "Tanto rumore per Genius"
date: 2008-10-07
draft: false
tags: ["ping"]
---

Ho finalmente azionato Genius sul mio iTunes.

Ne avevo sentite di tutti i colori. Chi pronosticava l'eliminazione della diversità dei gusti musicali; chi prediceva la prossima futura omologazione degli acquisti; chi denunciava l'ennesima intollerabile invasione della <em>privacy</em>; chi di qui e chi di là.

È una barra di consigli musicali spesso plausibili, che si spegne con un clic.

Il divario tra la realtà e la percezione della realtà continua spiacevolmente ad allargarsi. Non darei la colpa alla realtà.