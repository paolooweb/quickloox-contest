---
title: "Uno per tutti, tutti per uno"
date: 2007-10-31
draft: false
tags: ["ping"]
---

Non avevo ancora accennato a questo dettaglio di Leopard.

Se è montato un disco esterno con più partizioni, ed espello una sola partizione, appare una finestra di dialogo per annullare, espellere la partizione oppure (rullo di tamburi) espellere tutte le partizioni.

Cos&#236; basta espellere una partizione per toglierle tutte. Prima era la caccia alle icone per selezionarle ed espellerle insieme.

Un dettaglio, certo. Ben fatto e che mancava.