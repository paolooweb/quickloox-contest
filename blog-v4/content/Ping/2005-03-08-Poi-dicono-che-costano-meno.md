---
title: "Poi dicono che costa meno<p>"
date: 2005-03-08
draft: false
tags: ["ping"]
---

Dove si può farlo, chi compra certi Pc va dall&rsquo;avvocato<p>

In America esistono le <em>class action</em>: cause condotte da decine, centinaia, migliaia di cittadini che fanno fronte comune contro le grandi aziende, in modo che queste non possano approfittare dei loro soldi per spuntare la ragione anche quando hanno torto.<p>

In California è stata intentata una <a href="http://news.zdnet.com/2100-1040_22-5587443.html">causa</a> di questo tipo contro Dell. Sarebbero centinaia i privati che si lamentano di ogni genere di angherie. Uno di essi avrebbe acquistato computer e stampante per 688 dollari, solo che se ne sarebbe visti addebitati 1.352. Un altro sostiene di avere ricevuto componenti di qualità inferiore a quelli ordinati. C&rsquo;è chi ha richiamato l&rsquo;attenzione su finanziamenti che, strada facendo, peggiorano nelle condizioni e portano alla luce spese non dichiarate.<p>

Secondo gli avvocati che curano la class action, i reclami sarebbero centinaia.<p>

Il mito è che i Pc costino meno. Dipende anche da che cosa succede dopo l&rsquo;acquisto.<p>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>