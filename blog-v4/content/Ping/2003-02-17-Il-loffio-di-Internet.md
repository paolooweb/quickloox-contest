---
title: "Il loffio di Internet"
date: 2003-02-17
draft: false
tags: ["ping"]
---

Bisogna avere i calzini blu per guidare la macchina?

C’è gente incapace di lavorare su Internet che pensa la Rete non come una porta, studiata per accogliere tutti, ma come un crivello, dove passa solo chi non subisce la loro incapacità.

Prendi per esempio Englishtown di Virgilio. Un mondo fantastico dove si può entrare solo se usi Windows ed Explorer. Come dire che si può guidare la macchina solo con i calzini blu, o che è permesso bere acqua minerale solo nei bicchieri di vetro trasparente. Demenziale.

Poi guardi la <link>schermata di errore</link>http://englishtown.virgilio.it/error/browser.asp (loro) e chi ti trovi? Una coppietta e un Titanium (foto invertita, per giunta, ma fa niente)!

Una volta Virgilio era il bello di Internet. Ora è rimasto solo il loffio.

<link>Lucio Bragagnolo</link>lux@mac.com