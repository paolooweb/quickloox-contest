---
title: "Dieci nomi per i widget<p>"
date: 2005-05-21
draft: false
tags: ["ping"]
---

La cosa ha preso piede oltre ogni ragionevole previsione<p>

Il dibattito su che nome italiano dare ai widget di Dashboard ha suscitato una notevole mole di traffico postale.<p>

In fondo a queste righe trovi, a mio insindacabile giudizio, la lista delle dieci migliori proposte. Grazie a tutti quelli che hanno scritto e, se vogliono sottoporre altre alternative, la casella è sempre aperta.<p>

Proverò a chiedere alla direzione di Macworld se sono disposti a pubblicare un sondaggio sul sito. Ma non ci sperare. :-)<p>

<em>Come chiamare i widget in italiano?</em><p>

1. Agenti<br>
2. Cazzilli<br>
3. Codelet<br>
4. Genietti<br>
5. Magheggi<br>
6. Manzanilli<br>
7. Nosotti<br>
8. Servilli<br>
9. Spie<br>
10. &hellip;con F12<p>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>