---
title: "La curiosità uccise agosto"
date: 2009-07-29
draft: false
tags: ["ping"]
---

Rispondevo a una gentilissima lettrice di Macworld che ha risolto il <a href="http://www.macworld.it/blogs/ping/?p=2693" target="_self">messaggio impossibile composto sulla macchina Enigma</a> e che, appassionata di crittografia e curiosa di AppleScript, chiedeva punti di partenza per lavorarci su.

Ho un debole per ambedue gli argomenti e cos&#236; mi sono trovato a darle una risposta certo sintetica eppure articolata, con qualche link interessante. Avrebbe potuto benissimo essere un Ping.

Per questo, essendo che inizia agosto, le notizie vere si diradano, c'è più voglia di rilassarsi che di altro, oltre alle macedonie di mele si fanno anche belle grigliate di salsicce&#8230; chi ha richieste particolari, dubbi da togliersi, curiosità che gli rodono, voglia di fare e poche idee su come iniziare a farlo eccetera eccetera?

Se ci sono domande carine cerco di dare risposte quantomeno dignitose e magari fare scoprire direzioni inaspettate, o fare nascere una passione anche balneare per qualcosa (dopotutto gli amori di agosto spesso finiscono a settembre&#8230; e va bene cos&#236;).

Il tutto sempre nello spirito e negli spazi di Ping, niente inchieste oceaniche o esposizioni enciclopediche. Con un <i>post</i> al giorno però si può fare molto. Se vuoi condividere una curiosità, sono pronto a provarci. :)

Per la cronaca, un Rot-13 in AppleScript lo si vede <a href="http://lists.apple.com/archives/applescript-users/2003/may/msg00847.html" target="_blank">nelle liste di discussione Apple</a>, in AppleScript e in una versione diabolicamente corta che fa uso di un comando da Terminale.

<a href="http://www.terrylong.org/" target="_blank">Enigma Simulator di Terry Long</a> supporta comandi AppleScript.

<a href="http://www.bytereef.org.nyud.net:8080/enigma-suite.html" target="_blank">enigma-suite</a> contiene il codice sorgente in C del funzionamento di una macchina Enigma.

Qualcuno abbastanza folle potrebbe decidere di arrivare a scrivere un motore Enigma in AppleScript (folle entro un limite; io ci metterei un mese a tempo pieno, una persona un po' scafata che ha studiato da programmatore in un <i>weekend</i>, forse meno). Alla curiosità non c'è mai fine.