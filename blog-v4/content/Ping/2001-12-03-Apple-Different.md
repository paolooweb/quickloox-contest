---
title: "Apple Different"
date: 2001-12-03
draft: false
tags: ["ping"]
---

Qualcuno avrà notato la scomparsa di George Harrison, secondo dei Beatles a lasciare lo Yellow Submarine in cui tutti viviamo.
Qualcuno avrà notato che la pagina home del sito Apple, per un giorno, non ha ospitato né prodotti, né annunci, né promozioni, né iniziative di marketing, bensì un semplice, sobrio ricordo di George.
Il cinico di turno osserverà che certamente anche così si fa marketing e non ho nulla da eccepire all’obiezione.
Tuttavia, non so perché, ma da adesso mi sento ancora un pochino più contento di avere un computer Apple e di sapere che, con tutto il cinismo del caso, è ancora possibile fermarsi per un istante su qualcosa di diverso.
Tanto più che nella preistoria di Apple i rapporti con l’altra mela (Apple Records) non erano certo stati dei migliori.
George and Steve, they Play Different.