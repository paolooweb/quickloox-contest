---
title: "Coraggio da cangura<p>"
date: 2004-10-16
draft: false
tags: ["ping"]
---

Una persona di cui non so assolutamente niente e a cui penso spesso<p>

Il mio programma di posta è Mailsmith di <a href="http://www.barebones.com">Bare Bones</a> e per questo frequento la lista <a href="http://www.barebones.com/support/lists/mailsmith_talk.shtml">Mailsmith-Talk</a>.<p>

Nella lista scrive anche Erica Mackenzie. Non so chi sia, a parte che vive nel Queensland, Australia.<p>

Erica soffre di una patologia del sistema immunitario che ha colpito i suoi nervi e a volte la tiene lontana dal computer, costringendola inoltre a scrivere mediante un sistema di riconoscimento del parlato. Lo so unicamente perché lo scrive nella signature e ci scherza pure sopra.<p>

Di questa donna (donna? Ragazza?) so solo questo. Ma so pure che usa un programma di posta superiore, su un Macintosh, e ha un coraggio da leonessa. Pur essendo cangura.<p>

<em>Take care</em>, Erica.<p>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>