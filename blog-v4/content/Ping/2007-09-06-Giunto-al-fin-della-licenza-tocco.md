---
title: "Giunto al fin della licenza, tocco"
date: 2007-09-06
draft: false
tags: ["ping"]
---

Scrivo da un hotel di Milano in compagnia di Erik Stannow e Tiziana Scanu di Apple, per la presentazione alla stampa dei nuovi iPod.

Come al solito, toccare e vedere dal vivo non è la stessa cosa. Il web è fatto per l'informazione, non per l'esperienza.

Intanto Apple continua a creare oggetti sempre più sottili e questo in generale fa bene all'umanità nel suo insieme.

Sempre nel vedere e toccare le cose si capiscono tanti perché. Facile dire, <em>prima</em>, che la tecnologia <em>multitouch</em> di iPhone sarebbe percolata sugli iPod. Chiarissimo, <em>dopo</em>, che il <em>multitouch</em> ha una serie di vantaggi, e l'interfaccia tradizionale degli iPod ne ha un'altra, e sacrificare l'interfaccia tradizionale degli iPod sarebbe stato stupido. Ecco perché iPod touch è un altro iPod, non è iPod.

Gli schermi sono sempre più luminosi. Mi sa che lo sarà anche il destino dei nuovi iPod nano.