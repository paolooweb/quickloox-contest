---
title: "ÈMac"
date: 2007-04-15
draft: false
tags: ["ping"]
---

Domenica sono stato all'assemblea annuale del <a href="http://www.giovanninoguareschi.com/23club2.htm" target="_blank">Club dei Ventitré</a> e, a parte tutti gli amici ritrovati e l'ottimo pranzo, per quanto attiene a questo spazio devo rimarcare che l'eMac comprato da Alberto e Carlotta Guareschi per l'acquisizione via scanner di ritagli e disegni, sottoposto a intensa sollecitazione quotidiana, lavora che è una bellezza da ormai diversi anni.

Un G4 a 700 megahertz con 348 mega di Ram e Mac OS X 10.4.9.

