---
title: "È ancora da vedersi"
date: 2010-10-07
draft: false
tags: ["ping"]
---

Dopo <a href="http://www.macworld.it/ping/nomi/2010/10/04/da-vedersi/">la mia segnalazione</a> della gallery fotografica per l'inaugurazione dell'Apple Store di Orio al Serio, Fabio, anima di SetteB.it, mi ricorda l'esistenza di <a href="http://www.setteb.it/?p=9750" target="_blank">un'altra gallery</a>, precedente all'inaugurazione, e financo di un <a href="http://www.youtube.com/watch?v=xuJmtkqzNMg" target="_blank">filmato</a> (piacevole) dell'inaugurazione vera e propria.

Ero all'inaugurazione dell'Apple Store Carosello a Carugate ed è stata divertente, niente più. Mi spiace un po' avere perso Orio; a vedere le testimonianze, certo, era l'inaugurazione di un negozio, mica lo sbarco degli alieni. Però si percepisce, ecco, qualcosa in più.