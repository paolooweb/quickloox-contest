---
title: "La stele di robetta"
date: 2009-01-08
draft: false
tags: ["ping"]
---

AppleScript non è più un appuntamento fisso. Però mica sparisce. È talmente utile su Mac che prima o poi torna.

Più di questo, <a href="http://www.macworld.it/blogs/ping/?p=2124" target="_self">la chiacchierata</a> via <em>chat</em> Irc in occasione del <em>keynote</em> mi ha fatto scoprire un pezzettino di programmazione interessante.

Mentre usavo <a href="http://xchataqua.sourceforge.net/twiki/bin/view/Main/WebHome" target="_blank">X-Chat Aqua</a> ho trovato un <em>plugin</em> che aggiunge il comando <code>/np</code>, per <em>now playing</em>, a indicare automaticamente nella finestra di <em>chat</em> il brano di iTunes in esecuzione. Pochissima cosa, <a href="http://robinadr.com/projects/now_playing_plugin" target="_blank">un file di testo cortissimo</a>. Eppure è un po' come la famosa stele di Rosetta, che permise di associare i geroglifici egizi al greco e al demotico e avviare la comprensione della lingua dei Faraoni.

Infatti, lo script:

- contiene un AppleScript interessante che mostra come leggere i dati da iTunes sul brano in esecuzione;
- come una <em>matrioska</em>, l'AppleScript è contornato da uno script in Perl. A volte AppleScript è utile per portare a termine un compito ma non per parlare ad altre parti del sistema. L'involucro Perl serve a questo ed è un ottimo esempio di come incapsulare AppleScript in Perl;
- il tutto è infine esempio di come espandere l'interfaccia di X-Chat Aqua mediante script realizzati in Perl&#8230; o AppleScript.

Cose piccole, robetta. Per un aspirante programmatore o per chi vuole approfondire, però, è materiale per serate intere.