---
title: "I migliori videoprofessionisti"
date: 2007-07-14
draft: false
tags: ["ping"]
---

Scrive <strong>Daniele</strong>:

<cite>Ho appena finito di girare un film, con una piccola <em>crew</em> indipendente guidata da un amico regista messicano. Tralasciando gli aspetti riguardanti il suono, che non mi competono, l’attrezzatura video era composta da una videocamera ad alta definizione (DVCPRO 1080p, per chi ne sa) e un MacBook Pro collegato via FireWire che registrava in diretta con Final Cut tutto quello che veniva girato, oltre a un  
Cinema Display 23” usato come monitor di controllo e collegato anch’esso alla videocamera.</cite>

<cite>Il MacBook Pro ha sopportato per ben sei settimane condizioni di lavoro estenuanti (anche 16 ore al giorno), in ambienti interni ed esterni spesso sporchi, caldi o sotto il sole. Un amico una volta mi ha detto “technology always fails”, la tecnologia fallisce sempre, si rompe.</cite>

<cite>È certamente vero, ma quando le cose si fanno serie, c’è un tipo di tecnologia più affidabile di altre.</cite>

<cite>E molto spesso ha una mela morsicata “tatuata” addosso.</cite>

Non aggiungo altro, se non il titolo del film: <a href="http://www.spiralia.com/imigliorisentimenti.html" target="_blank">I migliori sentimenti</a>. Non c’è molto, per ora, ma una produzione richiede tempo.