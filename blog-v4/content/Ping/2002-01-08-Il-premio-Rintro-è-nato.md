---
title: "Il premio Rintro è nato"
date: 2002-01-08
draft: false
tags: ["ping"]
---

Bisogna riconoscere il talento di chi abbocca a qualsiasi bufala su Internet, soprattutto in occasione di Macworld Expo

Questa rubrica inaugura il premio Rintro.
Ogni volta che si approssima il Macworld i siti di pettegolezzi e speculazioni fanno a gara per predire che meraviglie presenterà Apple e, da quando è iniziata la seconda era Jobs, sparano comunque grandi sciocchezze nel vuoto cosmico. Il premio Rintro nasce per riconoscere un talento davvero non comune agli sprovveduti integrali che credono a qualsiasi cosa gli venga fatta vedere.
La prima edizione del premio Rintro viene trionfalmente assegnata a coloro che hanno dato retta per più di trenta secondi al fantomatico iWalk assemblato dai tedeschi di Spymac.com.
Il premio Rintro è nato e la mamma dei premiati, lo si sappia, è sempre incinta.

lux@mac.com