---
title: "Copiano perfino il Terminale"
date: 2004-07-16
draft: false
tags: ["ping"]
---

A suo tempo, naturalmente

Leggo ora che Longhorn, la prossima edizione di Windows prevista per il 2006 se andrà bene ma forse anche oltre, disporrà, udite udite, di una shell per controllare il sistema tramite comandi testuali.

Il punto non è ovviamente la shell in sé. Tutti i computer prima di Macintosh ne avevano una, in un modo o nell’altro. È che Apple è stata la prima a fare qualcosa in più, come sempre. Se si va a guardare il Terminale si scopre che accetta i drag and drop per vedere i percorsi di file e cartelle, riconosce copia e incolla, accetta font diversi da quelli preimpostati ed è integrato con il resto dell’interfaccia grafica in un sacco di altri modi.

Scommettiamo che vedremo le stesse cose su Longhorn? Ironico, e un po’ triste, come un’azienda che non è ancora riuscita del tutto a superare il suo Dos degli anni Ottanta adesso debba annunciare come novità una funzione esistente dagli anni Sessanta, copiando roba fatta da Apple tre o quattro anni fa. Squallore.

<link>Lucio Bragagnolo</link>lux@mac.com