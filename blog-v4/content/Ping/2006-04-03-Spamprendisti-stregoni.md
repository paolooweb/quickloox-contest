---
title: "Spamprendisti stregoni"
date: 2006-04-03
draft: false
tags: ["ping"]
---

N. mi ha risposto <cite>scusa ma non ho letto il tuo messaggio perch&eacute; &egrave; finito dentro un filtro antispam troppo zelante</cite>.

Gli ho risposto a mia volta <cite>se il filtro &egrave; disponibile, fammici parlare che lavoro con lui, invece che con te</cite>.

C&rsquo;&egrave; gente che &egrave; disposta a non ricevere i messaggi dalle persone vere pur di non riceverli dallo spam. Capisco che qualcuno possa volere filtrare precisamente i <em>miei</em> messaggi, ma se poi invece di dirlo chiaramente tira fuori una scusa, io continuo a scrivere&hellip;