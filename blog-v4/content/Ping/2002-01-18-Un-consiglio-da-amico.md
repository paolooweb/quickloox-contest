---
title: "Consiglio da amico (di Applescript)"
date: 2002-01-18
draft: false
tags: ["ping"]
---

Una opzione di Mac OS 9 è ritornata su Mac OS  X, ma come optional

C’era una volta Mac OS 9 e c’era il menu Script; un sistema prezioso per richiamare al volo gli Applescript più utili per automatizzare operazoini ripetitive oppure scrivere piccoli programmi utili.
Il menu Script cìè anche su Mac OS X, ma non tutti lo sanno. Occorre infatti andare a scaricarlo dal sito Apple.
Ci vuole pochissimo - un paio di centinaia di chilobyte - e l’installazione è decisamente facile: si trascina l’icona del file sopra la barra dei menu.
Contrariamente a Mac OS 9, in questo caso è facile anche la disinstallazione: si trascina l’icona fuori dalla barra tenendo premuto il tasto Comando.
Un consiglio da amico (del lettore): approfondire AppleScript sarà tutto, ma mai tempo perso.

lux@mac.com

<http://www.apple.com/applescript/macosx/script_menu/>