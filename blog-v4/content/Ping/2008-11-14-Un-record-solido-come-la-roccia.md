---
title: "Un record solido come la roccia"
date: 2008-11-14
draft: false
tags: ["ping"]
---

Avrai letto che il record mondiale di Asteroids <a href="http://www.macworld.it/showPage.php?template=notizie&amp;id=15907" target="_blank">ha compiuto un quarto di secolo</a>.

Credo che non verrà mai più battuto, nemmeno mediante il <a href="http://www.chrismarks.com/widgets/" target="_blank">widget</a>. Tanto vale giocare a <a href="http://www.devolution.com/~slouken/Maelstrom/binary.html" target="_blank">Maelstrom</a>, che è più vario.