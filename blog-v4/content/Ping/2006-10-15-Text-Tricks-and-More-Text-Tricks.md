---
title: "Text Tricks and More Text Tricks"
date: 2006-10-15
draft: false
tags: ["ping"]
---

Per chi non lo conosca, MacDevCenter di O'Reilly è uno dei posti migliori per approfondire la conoscenza del Mac.

Ma <a href="http://www.macdevcenter.com/pub/a/mac/2006/10/10/text-tricks.html?CMP=OTC-13IV03560550&amp;ATT=Text+Tricks+and+More+Text+Tricks" target="_blank">questo articolo</a> veramente supera qualsiasi traguardo raggiunto finora. Sto quasi pensando di ispirarmici e scrivere un Preferenze in materia, da quant'è bello.

A proposito di testo, anche BBEdit (dopo che ci aveva pensato <strong>Mario</strong> giorni fa) mi ha segnalato dell'arrivo dell'aggiornamento alla versione 8.5.1. Leggere le <a href="http://www.barebones.com/support/bbedit/current_notes.shtml" target="_blank">note sull'aggiornamento</a> spiega meglio di tutto perché un programma che vale. Altri, di prezzo equivalente, non riuscirebbero a mettere in piedi un elenco simile neanche elencando i cambiamenti apportati dalla versione 1.0.