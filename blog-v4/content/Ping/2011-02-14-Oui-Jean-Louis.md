---
title: "Oui, Jean-Louis"
date: 2011-02-14
draft: false
tags: ["ping"]
---

Nel <i>weekend</i> sono transitato accidentalmente da un Apple Store ufficiale e due rivenditori indipendenti. La prima cosa che si nota entrando in negozio non è più per forza iPad, ma ci sono in bella evidenza anche MacBook Air.

Non me ne sono reso conto. Poi però ho letto la <a href="http://www.mondaynote.com/2011/02/13/what-future-for-the-macintosh/" target="_blank">Monday Note di Jean-Louis Gassée</a>, il cui sistema operativo BeOS fu per un giorno l'acquisto software su cui Apple avrebbe rilanciato Mac OS (poi venne acquistata NeXt, Steve Jobs <i>included</i>).

Un breve estratto:

<cite>La <a href="http://www.bizsum.com/articles/art_why-we-buy.php" target="_blank">scienza dello shopping</a> afferma che l'area dei prodotti</cite> ad alto valore <cite>deve essere il primo tavolo a sinistra, perché statisticamente è cos&#236; che la gente si comporta nei negozi.</cite>

Allora ho fatto mente locale e ho ricordato i MacBook Air in bella evidenza.

La Note è molto più lunga e spiega come le vendite di portatili Mac siano cresciute molto più di quelle dei Mac desktop e di come, segnali alla mano, sia merito anche di MacBook Air.

Dico tutto ciò perché nelle medesime circostanze ho incontrato un vecchio amico convinto che Apple stia <cite>trascurando Mac perché gli interessano solo gli iPhone e gli iPad</cite>.

È da scusare; è entrato nell'Apple Store come me, ma evidentemente non ha letto <i>Monday Note</i>.