---
title: "Uno su mille non ce la fa"
date: 2007-03-23
draft: false
tags: ["ping"]
---

Quelli di The Unofficial Apple Weblog sono molto bravi e, pensando a che cosa siamo abituati qui, quasi magici.

Però una volta su mille toppano anche loro e <a href="http://www.tuaw.com/2007/03/20/apple-store-to-open-in-rome/" target="_blank">questo post</a> sull'apertura dell'Apple Store di Roma è proprio vuoto. Se penso che io metto solo T-shirt&#8230;

Grazie a <strong>Giuseppe</strong> di <a href="http://www.bonsai-studio.net/" target="_blank">Bonsai Studio</a> per la segnalazione!