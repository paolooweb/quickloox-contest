---
title: "Errore sconosciuto (ai più)"
date: 2004-03-02
draft: false
tags: ["ping"]
---

Mai avere fiducia al 100 percento nella tecnologia

Non c’è niente che funzoni al 100 percento. Mai. Hai presente la Ram? Pensa di avere un giga di Ram. Funzionante il 99,999 percento delle volte. È un livello di affidabilità eccezionale, eppure significa che di tanto in tanto la Ram darà un problema. In un miliardo di bit (un miliardo! Se fossero capelli riempirebbero casa tua e non potresti neanche entrarci) può essercene uno debole, difettoso. Se viene usato di rado, il problema che crea sarà occasionale e dannatamente difficile da identificare.

<link>Memtest</link>http://friskythecat.tripod.com/ Collauda la memoria attraverso veloci test computazionali, scoprendo problemi che Apple Hardware Test o qualunque altra utility non troveranno mai.

Purtroppo è ancora una utility da riga di comando e richiede il Terminale. Ma non può mancare in nessuna dotazione di emergenza. Tanti piccoli, stupidi, intermittenti bug di cui non si capisce la causa potrebbero essere responsabilità di quel bit difettoso, nascosto là dentro, in quel chip.

<link>Lucio Bragagnolo</link>lux@mac.com