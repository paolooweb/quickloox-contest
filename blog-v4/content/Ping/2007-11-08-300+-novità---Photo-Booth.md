---
title: "300+ novità: Photo Booth"
date: 2007-11-08
draft: false
tags: ["ping"]
---

Per finire la resa italiana dell'<a href="http://www.apple.com/it/macosx/features/300.html#photobooth" target="_blank">elenco di tutte le novità</a> (quelle visibili, non quelle sotto il cofano) di Leopard ci vuole ancora un po'.

Gli effetti di distorsione di Photo Booth (quel programmino stupido che serve a fare vendere carrettate di Mac alla gente che entra nei negozi solo per divertirsi a scattare le foto buffe e finisce che si vuole portare a casa il computer) sono stati diversificati e, per esempio, ora si controlla il centro di una distorsione con il mouse, o cursori a slitta permettono di graduare un effetto (se appaiono nella finestra).

Naturalmente c'è l'effetto chroma key, sarebbe stato più faticoso tenerlo fuori da Photo Booth che includerlo in tutti i programmi parenti della webcam.

Sono arrivati i provini, o almeno una volta si chiamavano tali.

Si registra anche video, non solo fotografie.

In compenso, le fotografie si possono scattare a raffiche di quattro. E le raffiche si possono esportare come filmati, come icone animate di iChat su iChat ci sono le icone animate!) o come Gif animate per il proprio sito.

Le fotografie sono presentabili come <em>slideshow</em>, eventualmente. E che altro si vuole, da Photo Booth?