---
title: "Mailsmith becomes more mature"
date: 2003-07-07
draft: false
tags: ["ping"]
---

Version 2.0 is to be strongly considered

I sometimes archive the oldest emails, so my current email database is “only” as big as a couple of gigabyte. I use Mailsmith from Bare Bones <link> since 1996 and it always worked gracefully.

Version 2.0 of the program came out recently, and it marks some real leap forward. There are two systems of spam control (SpamCop and integration with SpamSieve); it’s integrated with Pgp 8 to encrypt confidential messages; Mailsmith is faster, manages sending and receiving errors better, the interface has been revised in more instances (now it’s possibile to search in an arbitrary range of mailboxes) and the very good functions that it had are always there, like grep-powered searches inside text or filtering capabilities (made even better).

I wholeheartedly recommend Mailsmith. It’s a fast, versatile, effective program, made by a company that has always bought to the market rock-solid software for Macs only. And one more thing: it works (albeit with some minor glitch) on the WWDC prerelease of Panther.

For any information, write me and I’ll write back to you. With Mailsmith, of course.

Lucio Bragagnolo
lux@mac.com