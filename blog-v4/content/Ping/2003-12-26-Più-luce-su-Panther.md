---
title: "Più luce su Panther"
date: 2003-12-26
draft: false
tags: ["ping"]
---

Tante modifiche anche nei minimi dettagli

Ti sei accorto che, rispetto a Jaguar (Mac OS X 10.2), Panther (Mac OS X10.3) ha l’icona del Finder riveduta e corretta, con un aumento della luce che lo illumina dalla parte superiore?

Beh, basta guardare. :-)

<link>Lucio Bragagnolo</link>lux@mac.com