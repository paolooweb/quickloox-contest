---
title: "Il montaggio tra fiction e realtà"
date: 2010-10-23
draft: false
tags: ["ping"]
---

Facile guardare <a href="http://www.apple.com/it/ilife/imovie/" target="_blank">le meraviglie di iMovie '11</a> pubblicizzate da Apple e concludere che una <i>demo</i> è una <i>demo</i>.

Adesso, guardare <a href="http://www.youtube.com/watch?v=wNqEEvhh7y4" target="_blank">che cosa ha combinato</a> <b>Flavio</b> con iMovie '11. Uscito pochi giorni fa, dunque non era possibile studiarlo per settimane alla ricerca di un modo per fare veramente tutte quelle cose l&#236; che mostrano per indorare la pillola e fare abboccare chi ci casca, secondo gli scettici a oltranza.

(bonus: la Croce Ambrosiana è un'associazione benemerita.)