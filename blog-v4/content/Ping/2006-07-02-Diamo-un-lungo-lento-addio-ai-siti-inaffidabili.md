---
title: "Non accettare caramelle dagli inaffidabili"
date: 2006-07-02
draft: false
tags: ["ping"]
---

A leggere PowerPage di Jason O'Grady dell'8 dicembre 2005 viene proprio da ridere. <a href="http://www.powerpage.org/archives/2005/12/the_apple_core_firewire_not_dead_but_its_on_life_support.html" target="_blank">FireWire non è morta, ma è tenuta in vita artificialmente.</a>

La <a href="http://blogs.zdnet.com/Apple/?p=57" target="_blank">sparata</a> è proseguita sul suo blog presso Zdnet, dove ha scritto che <cite>FireWire è completamente sparita dai nuovi iBook Intel in arrivo a gennaio</cite> e che <cite>i nuovi PowerBook Intel perderanno FireWire 400 completamente, conservando solo una porta FireWire 800</cite>.

I fatti: a gennaio non è arrivato nessun iBook Intel. Non esistono MacBook privi di porta FireWire. Tutti i MacBook Pro hanno come minimo una porta FireWire 400 (sarebbe dovuta sparire). Il MacBook Pro con porta FireWire 800 ha anche una porta FireWire 400. Parole in libertà su tutta la linea.

Fino a qui, nessun problema. Ognuno è libero di raccontare le storie che vuole.

Purtroppo abbiamo fior di siti italiani che recuperano questa spazzatura e la ripetono, condita di punti interrogativi e di prese di distanza, a dimostrazione che non solo non hanno idea di quanto possa essere vero ciò che scrive O'Grady, ma mancano pure di coscienza critica e di coraggio nel prendere posizione.

Vogliamo dire loro che su PowerPage vengono scritte sciocchezze in quantità e che sarebbe opportuno pubblicare notizie vere invece di pettegolezzi da parrucchiera?