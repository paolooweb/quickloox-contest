---
title: "Un gioco a somma zero"
date: 2007-07-05
draft: false
tags: ["ping"]
---

Non ho grande esperienza di discoteche, però a suo tempo ho fatto abbastanza la mia parte.

Se c'era una costante fissa, è che non si va mai nella discoteca o nel locale che sta sotto casa. Bisogna andare per forza in qualche altro posto.

Un'estate l'ho passata casualmente in Versilia, tra Forte dei Marmi, Marina di Pietrasanta e Viareggio. I tre luoghi sono al limite dello spostamento a piedi, nel senso che distano non più di qualche chilometro.

Ogni sera c'era la transumanza delle compagnie che, letteralmente, si scambiavano paese. Chi stava al Forte andava a Viareggio, chi stava a Viareggio andava a Marina, chi stava a Marina andava al Forte e tutti i viceversa possibili.

Con gli occhi dell'adolescente, in un certo senso parti verso l'avventura. Con quelli della logica, pratichi un gioco a somma zero. Nessun paese ha locali nettamente migliori di quell'altro, tanto che quelli del paese dove stai andando tu si spostano nel tuo paese o in un altro ancora. Con gli occhi della letteratura, è una situazione gattopardesca: si muove tutto e, globalmente, non cambia niente.

Gli italiani che erano adolescenti allora, più tutti quelli arrivati più tardi, stanno discutendo animatamente di iPhone. E sembra che il provider sia un fattore cruciale. Quello che se non sarà Tim allora non comprerà iPhone, quello che speriamo che sia Vodafone, quello che o Wind o morte, e poi Tre (ma non è il quarto?) eccetera.

Non manca quello terrorizzato dalla (del tutto ipotetica) possibilità che iPhone sia legato a un singolo provider. Che possa essere un telefono migliore, un iPod più furbo, un assistente digitale personale più utile non conta; l'unica cosa è la presunta libertà di poter cambiare in continuazione schema tariffario, nell'illusione di risparmiare, o di fare l'affare, o chissà che.

Rivedo l'adolescente brufoloso e carico di ormone che si sposta da Marina al Forte a Viareggio e ritorno, solo che ora si muove tra Windafone, Vodacom e Telewind. L'importante è andare da qualche altra parte, certamente non perché sia migliore. Ma stavolta, di che ormone si tratta?