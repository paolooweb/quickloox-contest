---
title: "PostScript, Panther, Pdf"
date: 2003-12-15
draft: false
tags: ["ping"]
---

In mezzo c’è l’anello di congiunzione

Prima, come minimo, dovevi avere installato GhostScript, o Photoshop: o un software open source di configurazione non elementare oppure un software costosissimo.

Oggi, su Panther, mi è capitato sul disco un file PostScript. Aveva l’icona di Anteprima. Ho fatto doppio clic e l’ho visualizzato.

A molti piacciono i progressi spettacolari del sistema, con gli effetti speciali e le Nuove Rivoluzionarie Funzioni. A me piacciono progressi come questo.

<link>Lucio Bragagnolo</link>lux@mac.com