---
title: "Via radio nel parco"
date: 2002-07-16
draft: false
tags: ["ping"]
---

Milano e Roma non si sono ancora svegliate, mentre in America persino Pittsburgh...

A Pittbursgh la municipalità fornisce gratuitamente – per ora, poi diventerà dietro pagamento di una cifra modica mensile – il segnale AirPort in due parchi della città.
Ora bisogna comprendere che Pittsburgh non è esattamente la città più tecnologica e innovativa degli Stati Uniti e che a fare un parallelo con l’Italia, con tutto il rispetto e senza voler offendere nessuno, si potrebbe pensare a una Torino fra tante.

Adesso che abbiamo un ennesimo esempio, cari sindaci, ci vogliamo svegliare anche noi?

<link>Lucio Bragagnolo</link>lux@mac.com