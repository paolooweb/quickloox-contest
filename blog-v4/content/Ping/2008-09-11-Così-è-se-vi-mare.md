---
title: "Cos&#236; è se vi mare"
date: 2008-09-11
draft: false
tags: ["ping"]
---

La notizia: il tuo <em>blogger</em> preferito è finalmente in vacanza vera, dotato di iPhone per bloggare in mobilità estrema e di modemino 3G E220 Huawei per collegarsi al massimo durante le serate di relax (se l'iPhone fosse stato 3G sarebbe bastato lui, ma è Edge).

La seconda notizia. Oltre al tuo <em>blogger</em> preferito, sono in vacanza anch'io. :)