---
title: "La prevalenza di Zambardino<p>"
date: 2005-09-25
draft: false
tags: ["ping"]
---

Non so se siamo setta, ma noi ci divertiamo<p>

È passato un po&rsquo; di tempo e se ne può parlare a mente fredda. Vittorio Zambardino, su un podcast di Repubblica, dice qualcosa sul genere (è lui a sintetizzare, sul suo <a href="http://vittoriozambardino.blog.kataweb.it/">blog</a>) <cite>gli utenti Apple sono una setta, la fedeltà ai Macintosh è la più grande operazione di marketing spontaneo, ma si deve stare attenti, perché magari a un utente pc potrebbe anche capitare di essere picchiato.</cite><p>

Questa, dice sempre lui, è la sintesi scherzosa di un intervento ironico. Dopo il quale commenta sarcastico la posta dei sette che hanno abboccato e gli hanno scritto in toni poco urbani. Si lancia anche in una citazione letteraria, <em>La prevalenza del cretino</em> di Fruttero e Lucentini. Occhio, che a me stavano venendo Dostoevskij e Musil&hellip;<p>

Potrebbe fare la prova del nove. Prodursi in un intervento ironico e scherzoso tipo, per dire, spiegare che gli utenti Windows sono dei malati, vista la facilità con cui si beccano i virus. Ah ah ah! E stare a vedere che succede.<p>

Ma non lo farà. Zambardino è di Repubblica. Il quotidiano che, appena Microsoft sbadiglia, stende il tappeto rosso. Il quotidiano di Giuseppe Turani che va in <a href="http://www.repubblica.it/2005/g/sezioni/scienza_e_tecnologia/affin3/affin3/affin3.html">estasi mistica</a> alla Vista di Windows. Leggasi <a href="http://attivissimo.blogspot.com/">Paolo Attivissimo</a>, che lo ha <a href="http://italy.peacelink.org/cybercultura/articles/art_12024.html">distrutto</a>, ed è stato fin troppo gentile.<p>

Zambardino è di Repubblica. Quella che, per farti ascoltare il suo podcast, vuole farti scaricare, oh che caso, Windows Media Player. Per sentire chissà che incredibile formato? No, un file Mp3.<p>

Coraggio, Zambardino. Noi della setta passiamo il tempo a divertirci. A te tocca annoiarti per altri quindici mesi. Capiamo i tuoi bisogni. Mi raccomando, non te la prendere. Questo messaggio è ironico e scherzoso.<p>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>