---
title: "Non c’è trippa per Intel"
date: 2002-02-25
draft: false
tags: ["ping"]
---

Ogni tanto salta fuori un buontempone a chiedere Mac OS X per Intel. Non esiste proprio

Sessanta milioni di commissari tecnici della Nazionale, sessanta milioni di amministratori di Apple. Qualcuno pensa che Apple potrebbe dominare il mercato con un’edizione di Mac OS X su Intel, pronta a fare concorrenza a Windows.
Non si pensa che nessuno comprerebbe più computer Mac, se il sistema operativo Mac fosse disponibile su qualsiasi catorcio Intel a prezzo stracciato. E Apple vive sulle vendite dell’hardware.
Non si pensa che non esistono programmi per un’edizione Intel di Mac OS X. Tutto andrebbe ricompilato e sarebbe solo l’inizio di un problema.
Non si pensa che Microsoft è stata condannata per abuso di posizione dominante e farebbe di tutto per conservare la sua posizione dominante, con venti volte i soldi che ha Apple per pagarsi gli avvocati.
Non si pensa. Allora si stia zitti, per favore.

<link>Lucio Bragagnolo</link>lux@mac.com