---
title: "Tocca alla Bicocca"
date: 2009-05-09
draft: false
tags: ["ping"]
---

Il dibattito su programmazione e arte che ho moderato venerd&#236; è stato divertente e davvero interessante. È stato visto da pochissimi e del resto poco importava; quando sei l'ultimo della giornata ed è un venerd&#236; di maggio con il sole, in università, è nel novero delle possibilità.

<a href="http://www.spinningkids.org" target="_blank">Pan</a> ha mostrato demo software assolutamente spettacolare, <a href="http://www.pfmstudio.it/" target="_blank">Marisa</a> e <a href="http://www.sourcesense.com/it/home" target="_blank">Gianugo</a> hanno riflettuto in profondità con considerazioni davvero sopra la media, <a href="http://infinite-labs.net/" target="_blank">Emanuele</a> ha portato idee fresche e la propria pratica nella materia e <a href="http://www.spobe.com/" target="_blank">Andrea</a> ha risolto brillantemente le problematiche tecniche e logistiche.

Per parte mia, sono riuscito a seguire un filo logico di buon livello, che desse spazio a tutti rispettandone le specificità e sono davvero contento.

Due parole invece sull'università di Milano Bicocca, dove non passavo da anni. La giornata <a href="http://www.all4webday.com/jsp/Wiki?Welcome" target="_blank">All4Web</a> in quanto tale è stata semplicemente ospitata. L'ateneo, a parte offrire i salatini, non ha fatto assolutamente nulla per pubblicizzare o anche solo comunicare l'evento.

E fin qui. Ma qualcosa stona anche nella struttura. In aule dove si insegna anche informatica le uniche prese elettriche sono accanto alle porte di ingresso, per le pulizie, e alla cattedra, per i docenti. Che ci siano anche studenti è evidentemente un <em>optional</em>.

L'edificio è coperto da <em>wifi</em>. Per potervi accedere ho dovuto fornire nome, cognome, luogo e data di nascita, nazionalità, documento di identità. E non ho avuto la rete <em>wifi</em>, perché l'ho fatto quarantott'ore prima. Avrei dovuto farlo forse con più giorni o settimane di anticipo, come si conviene in tutte le burocrazie stupide e parassite. In compenso la rete è blindatissima e sicura: ogni tentativo di condividere la connessione con qualcuno che il certificato lo aveva è fallito.

La nota positiva è che Mac equipaggiava la grande maggioranza dei relatori.

Quella negativa è che a me non sembrano luoghi dove si possa studiare motivati e finire di studiare preparati. Alle sei del pomeriggio, terminata la manifestazione, non c'erano virtualmente segni di vita intelligente, con pochi ragazzi a prendere l'ultimo sole sulle panchine della piazza e tutto vuoto, spento, freddo. Somiglia più a un impianto per la produzione di carne in scatola che a un <em>campus</em>.

L'informatica corre a velocità da brivido e lo studio dell'informatica italiano è all'età della pietra. Ragazzi, compratevi un Mac e prendetevi la banda larga, poi passate le giornate e fare programmazione su e con e attraverso Internet. Il pezzo di carta non lo avrete e neanche il certificato per la connessione <em>wifi</em>. In compenso il vostro tempo servirà davvero a qualcosa.