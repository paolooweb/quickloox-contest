---
title: "Auguriamocelo / 3"
date: 2010-12-24
draft: false
tags: ["ping"]
---

Oggi festa. Un piccolo regalo a tutti: le <a href="http://spacelog.org/" target="_blank">trascrizioni dei dialoghi degli astronauti dell'Apollo 13</a> (e pure della Mercury 6). È un bon giorno per guardare in alto.

È un buon giorno anche per guardare lontano: il mio consiglio di lettura è <i>La ballata del cavallo bianco</i> di Gilbert Keith Chesterton, uno dei più bei poemi che siano stati scritti e, in proporzione, uno dei meno conosciuti.

Disponibile in italiano, che io sappia, <a href="http://raffaellieditore.com/libri/product_info.php?products_id=468" target="_blank">a pagamento</a> (ma li vale tutti) e in originale inglese <a href="http://www.gutenberg.org/ebooks/1719" target="_blank">gratis</a>, immediatamente a disposizione di un Mac o un iPhone.

Se qualcuno vuole commentare con i propri consigli di lettura, è il benvenuto. Magari il libro migliore arriva non sotto l'albero, ma nella calza della Befana.