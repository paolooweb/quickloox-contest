---
title: "Che cosa significa design"
date: 2010-07-05
draft: false
tags: ["ping"]
---

Il font di sistema di iPhone 4 è Helvetica Neue.

Il font di sistema degli altri iPhone invece Helvetica. E basta. Anche installando iOS 4.

La differenza tra i due font è molto sottile, abbastanza però da rendere Helvetica Neue più gradevole di Helvetica su iPhone 4, con il suo spettacolare Retina Display, e il contrario sui vecchi iPhone.

I pulsanti di iPhone 4 sono migliorati nella risposta rispetto a quelli degli iPhone precedenti.

E molte altre cose nella <a href="http://daringfireball.net/2010/06/4" target="_blank">recensione di iPhone 4 realizzata da John Gruber</a>, che spiega il vero significato della parola design: molto più che <i>bello da vedere</i>.

Sono anche trattati i temi che riempiono le prime pagine dei siti, come la questione dell'abbassamento del segnale quando si tiene iPhone 4 in mano, e cos&#236; via. Spiegazioni chiare, evidenti, equilibrate. Che ovviamente, in quanto scritte con cervello, non possono fare notizia.