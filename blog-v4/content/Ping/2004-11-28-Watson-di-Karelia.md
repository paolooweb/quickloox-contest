---
title: "Non supportato, Watson<p>"
date: 2004-11-28
draft: false
tags: ["ping"]
---

Per i nostalgici del progenitore di Sherlock 3 si può ancora fare qualcosa<p>

Molto brevemente. Prima di Sherlock 3 esisteva Watson di Karelia Software. Poi Apple è uscita con Sherlock 3, che toglieva il terreno sotto i piedi a Karelia. La quale ha venduto il progetto a Sun Microsystems, che fino a questo momento non ne ha fatto niente di interessante e, se lo facesse, probabilmente non lo farebbe per Mac.<p>

Karelia non può neanche più fornire Watson a chi lo richiede, ma ci sono sono ugualmente modi legali per farlo. Se qualcuno fosse interessato a prendersi Watson, visiti la <a href="http://weblog.karelia.com/MacOSX/WatsonStatus.html">pagina giusta</a> del blog di Dan Wood, che ha creato il programma, e segua le istruzioni.<p>

Ricordati però che il programma è assolutamente privo di supporto e che cambiamenti nella struttura di Mac OS X potrebbero mettere fuori uso parti del programma, o anche tutto il programma.<p>

Detto questo, è interessante vedere come sia difficile eradicare veramente un bel programma dalla faccia di Internet.<p>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>