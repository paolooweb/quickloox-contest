---
title: "La differenza italica<p>"
date: 2005-04-04
draft: false
tags: ["ping"]
---

Il problema è fare le cose senza interesse materiale<p>

Sto piluccando novità in una beta di Tiger e guardo Sherlock, tanto per capire se ci sono cose interessanti. Nel passato recente Sherlock è diventato praticamente inusabile, anche se sarebbe utilissimo, per la mancanza di contenuti italiani di qualità.<p>

È rimasto inusabile e però si è arricchito di alcune decine di servizi, tutti stranieri. Non solo americani, ma anche tedeschi o inglesi.<p>

Creare un canale di informazioni Sherlock è, direbbe un programmatore, triviale. Ci vuole poco o niente, tecnicamente.<p>

In Italia manca solo la voglia. Che però vuol dire tanto e fa la differenza.<p>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>