---
title: "Rompere gli schemi<p>"
date: 2005-07-18
draft: false
tags: ["ping"]
---

Perché iWork ti si ripaga nel giro di venti minuti<p>

Mi tocca occuparmi di sudoku, per ragioni professionali. Mi tocca creare schemi perfettamente impaginati, di qualità editoriale. Come faccio?<p>

Il primo istinto è il foglio di calcolo, ma ben presto si vede che creare celle perfettamente quadrate è un po&rsquo; un problema. La gestione dei contorni delle celle è noiosa. Centrare un numero in mezzo alla cella non viene preciso come farlo in un programma di impaginazione.<p>

Alla fine ho usato Pages. Creare una tabella di celle quadrate è un attimo. La gestione del testo è un piacere. Risultato perfetto in pochi minuti. Pdf impeccabile e bella figura.<p>

Se mi danno la prossima versione con dentro la compatibilità AppleScript mi offro di pagarlo due volte. Ci guadagno comunque.<p>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>