---
title: "Ci manca troppo un Mac user"
date: 2002-03-14
draft: false
tags: ["ping"]
---

Di fronte alla tragedia non resta che essere brevi

Poco tempo fa l’ho citato su Macworld di carta, come reporter di guerra e Mac user. “Gente dotata di grande coraggio e il cui lavoro dipende da un portatile, ma in modo che non ci immaginiamo nemmeno”, scrivevo di lui e degli altri.
Oggi <link>Raffaele Ciriello</link>http://www.ciriello.com non c’è più e neanche il più robusto dei Titanium avrebbe potuto proteggerlo dalla raffica che lo ha falciato.
Ci mancherà. Troppo.

<link>Lucio Bragagnolo</link>lux@mac.com