---
title: "Aggiungi un posto a tavoletta"
date: 2008-01-06
draft: false
tags: ["ping"]
---

La mia signora aveva bisogno di una tavoletta grafica essenziale e ha optato per una <a href="http://www.wacom-europe.com/bamboo/index.asp?pid=210" target="_blank">Bamboo Wacom</a>, prezzata a 79 euro in <a href="http://atworkgroup.net" target="_blank">@Work</a>.

Non frequentavo tavolette grafiche da molto tempo e sono rimasto ben sorpreso dalle capacità che si trovano tranquillamente anche in esemplari a basso costo come questo.

Il tocco è ottimo e la sensibilità alla pressione fa&#8230; impressione. È tutto molto naturale e basta un PowerBook 12&#8221; per seguire perfettamente qualsiasi movimento della penna, a qualunque velocità, su un Photoshop Cs2. I tasti supplementari e le opzioni di personalizzazione sono ottime e, volendo, ci si può comprare opzionalmente anche un aerografo virtuale da usare al posto della penna.

L'installazione si limita al driver e tutto funziona dal primo momento senza difficoltà. La confezione non è curata allo stremo come quella di un iPod, ma si vede che Apple ha fatto scuola e qui c'è una certa attenzione, diversa dal consueto poco-più-che imballo.

Mi fermo qui perché non so disegnare e con la tavoletta grafica al massimo posso fare scarabocchi. La mia signora sa disegnare e dopo tre minuti, senza personalizzazioni e configurazioni particolari, già produceva meraviglie. Le ho raccomandato di usare il tutorial incluso nel Cd di installazione, che c'è anche quello.