---
title: "Inventori da gabinetto"
date: 2009-10-06
draft: false
tags: ["ping"]
---

Tale Joshua Strickton, ex ingegnere Apple cofirmatario di vari brevetti depositati dall'azienda in merito al <i>multitouch</i>, <a href="http://www.nytimes.com/2009/10/05/technology/05tablet.html?_r=2" target="_blank">ha dichiarato al New York Times</a> che Apple aveva un prototipo di <i>tablet</i> pronto nel 2003, che però consumava le batterie alla velocità della luce e, di fatto, <cite>non poteva essere costruito</cite>.

<cite>La batteria non durava abbastanza, le prestazioni grafiche erano insufficienti e il solo costo dei componenti superava i 500 dollari.</cite>

Non solo: ad affossare definitivamente il progetto fu Steve Jobs, che chiese quale utilità potesse avere l'apparecchio oltre a <cite>navigare in Internet dal gabinetto</cite>.

Sono andato a rivedere l'Internet del 2003. Molti siti parlavano del <i>tablet</i> e lo davano per imminente.

Nessuno però scrisse che era impossibile da costruire e che Jobs lo aveva stroncato. Ignari dei fatti, inventavano.

Oggi molti siti danno il <i>tablet</i> per imminente.

Che arrivi o meno, ignari dei fatti, inventano.