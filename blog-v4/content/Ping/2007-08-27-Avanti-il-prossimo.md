---
title: "Avanti il prossimo"
date: 2007-08-27
draft: false
tags: ["ping"]
---

<cite>Mtv ha i muscoli che servono per competere con Apple. Possiedono tutti i tipi di media, dalla tivù a Internet. E sanno come vendere musica e servizi legati alla musica.</cite>
<em>&#8212; Nitin Gupta, analista dello Yankee Group, 17 maggio 2006</em>

Urge, il negozio di musica online di Mtv, ha chiuso. La sua carcassa si fonderà con Rhapsody, il servizio di vendita di musica su abbonamento di RealNetworks.

<cite>Rhapsody finora non ha saputo guadagnare terreno su iTunes, che ha recentemente annunciato i tre miliardi di brani venduti.</cite>
<em>&#8212; Greg Sandoval, Cnet</em>

Avanti il prossimo.