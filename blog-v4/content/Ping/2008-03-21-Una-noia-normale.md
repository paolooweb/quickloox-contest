---
title: "Una noia normale"
date: 2008-03-21
draft: false
tags: ["ping"]
---

Avevo annunciato la grande sfida. Provare ad aprire i file di Office con iWork e vedere se c'erano problemi, e quanti.

Mentre rimane perfettamente possibile creare un file di Excel cos&#236; complicato che Numbers non riuscirà ad aprirlo, e naturalmente non ci sono le macro, blablabla blablabla, nel frattempo ho aperto alcune decine di file normali provenienti da gente normale del mondo reale, che lavora studia e si diverte. Tra l'altro ho tenuto un corso su come tenere presentazioni, dove aprire documenti PowerPoint con Keynote è stato all'ordine del giorno.

A parte la presentazione in un formato che non riuscivo ad aprire (e con il mio Mac non ci riuscivano neanche tutti i Pc Windows che non erano quello dell'autrice), è una noia mortale. Il peggio che possa capitare è che molti documenti arrivano da Windows con dentro un font ArialMt. Io invece ho solo un noioso Arial e quindi ricevo una noiosa notifica. Ovviamente il documento non ha alcun altro problema.

C'era chi manifestava virtù eroiche pur di mantenere il dialogo con Windows anche sulle cose inutili. Oggi è normalità persino noiosa.