---
title: "A tu per tu con Tiger<p>"
date: 2004-12-09
draft: false
tags: ["ping"]
---

Cose non comprese nel <em>nondisclosure agreement</em><p>

Scrivo questa nota da un hotel di Milano in cui Apple sta presentando agli sviluppatori un&rsquo;anteprima completa delle nuove possibilità di Tiger, altrimenti identificabile come Mac OS X 10.4, realtà entro il primo semestre 2005.<p>

Un accordo di confidenzialità mi impedisce di descrivere tutto ciò che vedo e sento, ma sono libero di meravigliarmi della quantità di sviluppatori presenti, che è un buon segno. C&rsquo;è molta più gente che in passato disposta a creare software su Mac.<p>

La seconda configurazione è che le premesse di Tiger, viste con l&rsquo;occhio dello sviluppatore, sono davvero notevoli e che, se tutto ciò si trasformerà in software per noi persone normali, ne vedremo veramente delle belle.<p>

Forse questo lo posso dire: dopo Tiger, l&rsquo;espressione <em>buttare un sasso nello stagno</em> acquisisce tutt&rsquo;altro significato.<p>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>