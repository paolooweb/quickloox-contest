---
title: "Artisti della ricerca"
date: 2010-03-19
draft: false
tags: ["ping"]
---

A volte cercare qualcosa su iTunes Store è frustrante.

Mi ci sono soluzioni, per esempio <a href="http://ax.phobos.apple.com.edgesuite.net/WebObjects/MZStoreServices.woa/wa/itmsLinkMaker" target="_blank">iTunes Link Maker</a>.

Ragionando sulla struttura dei link di iTunes, vi si può lavorare attivamente per cercare generi, o artisti, o altro che non sia un elemento singolo. In Bjango ci hanno ragionato sopra e <a href="http://bjango.com/articles/ituneslinks/" target="_blank">un loro articolo</a> contiene numerosi esempi pronti all'uso, per esempio la ricerca dentro una categoria specifica di applicazioni o altro ancora.