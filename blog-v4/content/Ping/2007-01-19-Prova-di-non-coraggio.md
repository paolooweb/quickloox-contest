---
title: "Prova di non coraggio"
date: 2007-01-19
draft: false
tags: ["ping"]
---

La mia procedura di backup (home riversata sul megahard disk Usb ogni fine settimana) è appena passata da manuale a Backup. Backup è semplice. Un gioco da ragazzi.

L'appetito però viene mangiando e ora sto pensando ad automatizzarla ancora di più. Tramite <code>rsync</code>.

Chissà se oso.