---
title: "Cinque per mille"
date: 2007-05-12
draft: false
tags: ["ping"]
---

Scrivo in diretta dalla festa per il quinto anno dell'<a href="http://www.allaboutapple.com" target="_blank">All About Apple Museum</a> a Quiliano. Non ci sono ancora mille persone, ma ragioniamo già sulle centinaia.<!--more-->

Ci sono un sacco di cose da raccontare, colo che per ora è tempo di raccoglierle tutte; mi sdebiterò nei prossimi giorni. Intanto, se qualcuno volesse destinare il cinque per mille al museo, beh, merita.