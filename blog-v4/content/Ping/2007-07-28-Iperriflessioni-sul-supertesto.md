---
title: "Iperriflessioni sul supertesto"
date: 2007-07-28
draft: false
tags: ["ping"]
---

Piccola bugia, solo qualche pensiero in libertà, suscitato da qualche commento relativo a <a href="http://www.ihug.org/" target="_blank">HyperCard</a>, <a href="http://www.supercard.us/index.html" target="_blank">SuperCard</a> e la metafora degli <em>stack</em> e delle schede.

Qualcuno, forse Alan, diceva che forse una volta la programmazione era più facile e ha perfettamente ragione. Il fatto è che si sta uscendo dalla preistoria. Per fare un esempio banale, chiunque di noi avrebbe potuto guardare una schermata del Mac del 1987 e abbozzare un'icona con lo stesso aspetto. Bastava MacPaint.

Oggi, prova a guardare una schermata di Leopard. Come disegnare un'icona senza saperne qualcosa di <em>graphic design</em> e forse pure di 3D? Impossibile. La resa grafica dell'interfaccia è uscita dalla preistoria.

È uscita dalla preistoria anche la programmazione. Con il Basic ci si poteva provare tutti. Oggi, con Ruby o con Python, ci si può provare tutti lo stesso, solo che la complessità dei sistemi sottostanti è esplosa. Per fare uno scriptino, una cazzatella, ci vuole poco. Per un risultato professionale, bisogna essere professionisti.

HyperCard è stato un'idea luminosa: programmazione facile, in un ambiente che schermava dal complesso sistema sottostante.

Il fatto è che era un'idea di Prima Delle Reti. Oggi l'ambiente sottostante è Internet. Come schermare Internet? Sembra impossibile. O deve spuntare un nuovo genio con un nuovo HyperCard.

Se spunta, tuttavia, si troverà davanti alcuni milioni di ragazzini che pasticciano con JavaScript sulle pagine web. Il tutto è meno elegante, certamente, ma tremendamente efficace. E, se si vede ogni pagina web caricata dal browser come una scheda, contenente link ad altre schede, e un sito come uno stack, beh, forse il web è un filino più disordinato. Ma come confini ha l'universo. Ho l'impressione che HyperCard sia stato un progresso eccezionale. Come lo è stato, che so, il tubo catodico. Ma adesso è tempo di Lcd e stanno già diventando vecchi anche loro.