---
title: "Sign O' The Times"
date: 2008-01-20
draft: false
tags: ["ping"]
---

Segno dei tempi (e canzone di Prince, ma non c'entra).

<cite>Oh, Word. Per vent'anni mi hai supportato e tiranneggiato. Mi hai dato una frugale lavagnetta su cui comporre, un posticino angusto nella catena di montaggio delle frasi&#8230; e poi mi hai arringato dandomi ordini di salvare o riformattare quando eri</cite> tu <cite>a bloccarti, a farfugliare assertivo argomenti da ridicolo sciovinismo aziendale (&#8220;Chiave di prodotto non valida&#8221;! &#8220;Formato di database non riconosciuto&#8221;!)</cite>

<cite>E proprio quando ho bisogno di restare da sola con i miei pensieri e il mio Mac, mi trattieni enfatizzando la mia estrema dipendenza da te, che melodrammaticamente &#8220;recuperi&#8221; documenti che</cite> tu <cite>hai perso nei tuoi</cite> crash <cite>ricreativi</cite>.

Il resto è troppo lungo per quanto è difficile (l'inglese letterario richiede tempo e già questa traduzione dovrebbe essere migliore). La sostanza è che Virginia Heffernan annuncia sul New York Times di <a href="http://www.nytimes.com/2008/01/06/magazine/06wwln-medium-t.html?_r=1&oref=slogin" target="_blank">smettere di usare Word</a> per passare a <a href="http://www.literatureandlatte.com/" target="_blank">Scrivener</a>.

Passerò il link a tutti quelli con un datore di lavoro potentissimo che li obbliga a usare Word. Il New York Times ha centinaia di collaboratori che mandano articoli, eppure uno può scrivere con il programma che crede.