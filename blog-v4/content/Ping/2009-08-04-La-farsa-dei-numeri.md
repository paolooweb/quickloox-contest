---
title: "Il solito tasto"
date: 2009-08-04
draft: false
tags: ["ping"]
---

È passata anche questa edizione di <a href="http://www.blackhat.com/" target="_blank">Black Hat</a>, uno dei più celebri raduni mondiali di hacker.

Hanno ricevuto spazio e attenzione le imprese su Mac OS X e, come di consueto, stanno apparendo le solite balle sui siti che vanno per la maggiore ma, invece di capire le cose, le ripetono.

Si dice che Mac ora è meno infestato di Windows perché è questione di numeri; se i Mac fossero quanto i Pc, si dice, avrebbero gli stessi problemi.

Questa leggenda urbana era viva già molti anni fa. A Black Hat Mac OS X ha ricevuto molta più attenzione che nel passato. Hanno piratato perfino il <i>firmware</i> <a href="http://www.blackhat.com/presentations/bh-usa-09/CHEN/BHUSA09-Chen-RevAppleFirm-PAPER.pdf" target="_blank">delle tastiere Apple</a>, di fatto i Mac in giro per il mondo non corrono pericoli che non dipendano dalla scarsa intelligenza di chi li usa. Per i Pc Windows esistono oltre centomila pericoli documentati.

Se poi dovesse valere l'argomento del numero, beh, qualcuno si è dato la pena di scrivere un <a href="http://www.viruslist.com/en/weblog?weblogid=208187356" target="_blank">virus per iPod con sopra Linux</a>.

Quanti sono gli iPod con sopra Linux? Poche migliaia a essere buoni. Nessuno spiega perché ci sono in giro oltre trenta milioni di Mac e neanche un virus.