---
title: "Io l'avevo detto"
date: 2007-07-21
draft: false
tags: ["ping"]
---

Adesso si comincia a esagerare. Un analista di mercato ha indicato il nuovo target di valore delle azioni Apple a 200 dollari e va trattato come quell'altra analista che, a 111 dollari, aveva detto è ora di vendere: dare ascolto a chiunque, compresi parapsicologi e dadi a cento facce, tranne che a loro.

Gli analisti sono la prova che si possono fare soldi lavorando male e, in certi casi, non lavorando affatto. Come ho già detto, si chiamano come la parte del corpo che usano principalmente in ufficio.

Detto questo, ricordo quando a fine 1997 le azioni Apple erano a undici dollari, l'azienda era data per spacciata da tutti i parolai più ascoltati, ero squattrinato peggio di adesso e non potevo farlo di mio, ma ripetevo dovunque ne avessi l'occasione <em>comprate, comprate azioni Apple</em>.

Francamente, avessi i soldi, comprerei pure ora. Anche se duecento dollari mi sembrano fuori dal mondo.