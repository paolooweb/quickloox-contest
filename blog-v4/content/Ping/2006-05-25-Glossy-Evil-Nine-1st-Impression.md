---
title: "Glossy Evil Nine, 1st Impression"
date: 2006-05-25
draft: false
tags: ["ping"]
---

(ognuno ha i suoi vizi musicali)

Arrivati in Mac@Work MacBook Pro 17&rdquo; e i modelli bianco e nero di MacBook. Sul 17&rdquo; c&rsquo;&egrave; poco da dire, si sa gi&agrave; tutto. Le prestazioni sono quelle che ci si aspetta, ampiamente superiori, e via.

I MacBook. Test di un&rsquo;oretta, sulle cose essenziali, senza pretese. Prima di tutto: chi contesta la differenza di prezzo non ha idea. Il MacBook nero &egrave; <em>mooolto</em> pi&ugrave; bello e si vede a prima vista che il materiale dello chassis &egrave; diverso rispetto al MacBook bianco.

La tastiera &egrave; positiva. Apparentemente il meccanismo di pressione dei tasti &egrave; rimasto lo stesso. Familiarizzare &egrave; questione di pochi minuti. Il tocco &egrave; buono, la risposta &egrave; giusta, il leggero rimbalzo superfluo che constato sul mio 17&rdquo; e ogni tanto mi triplica una doppia qui sembra mancare. Sempre al primo test ho l&rsquo;impressione che il nuovo arrangiamento dei tasti protegga l&rsquo;interno della tastiera molto pi&ugrave; di prima. Ma non avevo briciole per fare la prova.

Lo schermo glossy. Frontalmente, nella posizione su cui si lavora, con la direzione dello sguardo perpendicolare al piano dello schermo, i riflessi sono praticamente assenti e di fatto non esiste problema. Pi&ugrave; la direzione dello sguardo tende a mettersi in parallelo con il piano dello schermo, pi&ugrave; c&rsquo;&egrave; riflessione. Una persona che guarda lo schermo da posizione molto laterale rispetto a chi sta lavorando osserva una riflessione notevole.

I problemi dello schermo glossy per chi ci lavora vengono dalla presenza di illuminazione puntiforme alle proprie spalle. Non so se ci si abitua o se ero prevenuto, ma di fatto il riflesso c&rsquo;&egrave; e a me dava fastidio. Per avere una impressione definitiva dovrei lavorarci seriamente e fare anche qualche prova <em>en plein air</em>.

Le prestazioni. L&rsquo;effetto &egrave; quello del Mac mini Intel: veloce che fa impressione, filmati in alta definizione e Dvd senza problemi, l&rsquo;assenza della scheda grafica in hardware non si nota. Tranne che per il 3D, naturalmente. World of Warcraft &egrave; giocabile, intorno ai 17 fotogrammi per secondo, ma niente di pi&ugrave;.

La batteria &egrave; sorprendente. Fab, che ne ha adottato uno e ci sta lavorando da un paio di giorni, riporta tre ore tranquille di utilizzo intenso e ben oltre cinque ore di utilizzo normale, sulle sei dichiarate da Apple. Dubito fortemente che esistano Pc di pari configurazione con la stessa autonomia effettiva.

&Egrave; tutto. Se ci sono domande specifiche, nei limiti della mia presenza in Mac@<a href="http://www.macatwork.net" target="_blank">Work</a>, nel giro di qualche giorno rispondo a tutto quello che posso.