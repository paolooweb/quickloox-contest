---
title: "Biglietti, (ti) prego"
date: 2006-02-09
draft: false
tags: ["ping"]
---

Chi mi segue sa che saltuariamente mi permetto un post non propriamente in argomento Mac. Come questa segnalazione, che arriva da <a href="http://www.accomazzi.net" target="_blank">MisterAkko</a> e che &egrave; davvero saporita, se si vuole assaggiare l&rsquo;incompetenza che ancora regna sovrana riguardo a Internet. Inoltre a chiunque di noi potrebbe capitare l&rsquo;esperienza. Ma sto divagando.

Chi bazzica Milano sa (e adesso lo si sa tutti) che il biglietto base per i mezzi pubblici costa un euro. Sono poi disponibili varie forme di acquisto; una di esse &egrave; il <em>carnet</em> da dieci biglietti al costo di 9,20 euro (92 eurocent a biglietto).

Ora vai su <a href="http://www.atm-mi.it/giromilano/" target="_blank">questa pagina</a> del sito dell&rsquo;Azienda Trasporti Milanesi e clicca, in basso a destra, su <code>Biglietti >></code>.

Guarda quanto costano i biglietti e fai bene attenzione alle spese di spedizione. Poi sappimi dire quanti se ne vendono online, secondo te.