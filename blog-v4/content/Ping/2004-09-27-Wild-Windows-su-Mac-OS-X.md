---
title: "I dritti ce l&rsquo;hanno storte<p>"
date: 2004-09-27
draft: false
tags: ["ping"]
---

Un piccolo trucco per constatare la flessibilità del motore grafico di Mac OS X<p>

Apri il Terminale e scrivi <code>killall Dock</code>, ma <em>non</em> premere Return.<br>
Prendi una finestra (per esempio di Safari).<br>
Tieni il Terminale in primo piano e manda la finestra di Safari nel Dock al rallentatore, ossia tenendo premuto Maiuscole intanto che clicchi sul pulsante giallo della finestra.<br>
Appena la finestra inizia a scivolare lentamente nel Dock, premi Return e conferma il comando nel Terminale (che era rimasto in primo piano, ricordi?).<p>

La finestra rimarrà mezza dentro e mezza fuori dal Dock, ma continuerà a funzionare come se niente fosse.<p>

È una bella demo da mostrare all&rsquo;amico windowsista che magnifica, poveraccio, la grafica di Windows Xp.<p>

Mica finisce qui. Una finestra di Mac OS X può distorcersi, ma anche ruotare e continuare a funzionare. Per chi avesse voglia di approfondire, c&rsquo;è la pagina <a href="http://atzenbeck.de/research/wildWindows/">Wild Windows on Mac OS X</a>.<p>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>