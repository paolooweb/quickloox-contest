---
title: "Cromo inossidabile"
date: 2009-06-05
draft: false
tags: ["ping"]
---

Perché lasciare arrugginire Chromium, che è in costante progresso?

Già segnalata la <a href="http://build.chromium.org/buildbot/snapshots/chromium-rel-mac/" target="_blank">pagina che contiene l'aggiornamento continuo dei <em>build</em></a>, richiede però attenzione quotidiana e noi, a imitazione dei programmatori, siamo per la pigrizia.

Mac OS X Hints spiega come <a href="http://www.macosxhints.com/article.php?story=20090604081030791" target="_blank">scaricare il <em>build</em> più aggiornato con uno <em>script</em> da Terminale</a>.

Lo <em>script</em> va copiato e salvato con un <em>editor</em> di testo (tipo <a href="http://www.barebones.com/products/textwrangler" target="_blank">TextWrangler</a>, gratis) dovunque nel sistema.

Poi, nel Terminale, si scrive <code>chmod a+x</code> e si trascina sulla finestra l'icona del file. Si dà Invio ed ecco fatto, in futuro basterà fare doppio clic sull'icona del file per scaricare il <em>build</em>.

Su Mac OS X Hints propongono di farne un evento automatizzato, ma mi pare troppo. E però, sempre all'insegna della pigrizia, spiegano che mettendo insieme in Automator lo <em>script</em> di cui sopra e un AppleScript corto e semplice è possibile prima chiudere Chromium se è attivo, e poi scaricare il nuovo <em>build</em>. Un'altra seccatura manuale in meno.

Tutto ciò è relativamente facile da realizzare e rappresenta un ottimo esercizio. Se qualcuno ha difficoltà, la commenti e lo aiuterò.

La versione di bordo di Chromium sarà sempre luccicante.