---
title: "Compiti per le vacanze"
date: 2010-07-10
draft: false
tags: ["ping"]
---

Surface è il noto sistema di Microsoft basato su un tavolo con superficie multitouch capace di riconoscere lo spostamento di oggetti posati sullo schermo.

Qust’ultima funzione è affidata a un sistema di telecamere nascoste sotto il tavolo e a relativo software di riconoscimento.

Ora è evidente che Surface è in grado di fare molto più rispetto a quello che sto per dire e che ci sono di mezzo anche design, ricerca e sviluppo, bla bla bla. Però.

Un tizio qualsiasi ha realizzato un sistema fai-da-te per giocare a scacchi su Internet <a href="http://www.pcb-dev.com/index.php?option=com_content&amp;task=view&amp;id=35&amp;Itemid=1&amp;lang=english" target="_blank">partendo da una scacchiera vera</a>.

Il movimento dei pezzi viene captato da una webcam e affidato a software di riconoscimento degli spostamenti, che traduce le mosse in notazione scacchistica per inviarla sul server. Quando arriva la risposta dal giocatore remoto su Internet, il software riceve l’informazione e mostra una indicazione visiva del pezzo da spostare. Il video sul sito è chiarissimo e il costo complessivo dell’installazione è minimo.

<a href="http://www.microsoft.com/surface/en/us/default.aspx" target="_blank">Surface</a> impiega uno schermo da trenta pollici, che messo in orizzontale corrisponde circa a un tavolino da bar. La sua risoluzione però è di 1.024 x 768 pixel, esattamente uguale a quella di un iPad, che sicuramente non riconosce gli oggetti posati sulla propria superficie, ma fa multitouch come si vuole e ha varie altre prerogative, come il sapere dove si trova e se sta in posizione inclinata.

Stante che il prezzo medio di iPad è di 649 dollari e quello di Surface è 12.500 dollari, diciannove volte superiore, i compiti per le vacanze consistono nel formarsi una valutazione personale del valore di Surface rispetto al prezzo.