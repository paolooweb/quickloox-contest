---
title: "Da mercato a meritocrazia"
date: 2011-01-23
draft: false
tags: ["ping"]
---

Mentre si discute se un App Store per Mac sia appropriato, iniziano a parlare i produttori di software, nel caso Phil Libin, amministratore delegato di <a href="http://evernote.com/" target="_blank">Evernote</a>.

Evernote è certamente un caso fortunato e anche una applicazione abbastanza particolare. Tuttavia, queste sono le sue parole. Nella pagina originale dell'<a href="http://techcrunch.com/2011/01/19/evernote-mac-app-store/" target="_blank">articolo</a> si trova anche un grafico molto esplicativo.

<cite>L'anno scorso circa il 70 percento dei nuovi utenti di Evernote veniva dagli</cite> app store mobile<cite>, per lo più iOS e Android. Questo ci ha condotto alla comprensibile conclusione che il fattore cruciale era rendere una una piattaforma attraente per gli sviluppatori indipendenti. Settimana scorsa abbiamo capito che la realtà è più sfumata. Non è il</cite> mobile <cite>a essere sommamente importante, ma l'</cite>app store<cite>. Fino a una settimana fa tutti gli</cite> app store <cite>erano su apparecchi</cite> mobile<cite>, ma ora qualcuno con un MacBook nuovo fiammante ha la stessa voglia di recuperare i programmi migliori che ha qualcuno con un iPhone nuovo fiammante.</cite>

<cite>Una piattaforma senza un</cite> app store <cite>come si deve rappresenta una grossa sfida per gli sviluppatori. Per avere successo bisogna spendere molto tempo e denaro su canali, logistica,</cite> partnership <cite>e pubblicità quanto se ne spende per realizzare un grande prodotto. Una volta che prende piede un</cite> app store<cite>, il mercato del software inizia a muoversi verso una meritocrazia. Naturalmente imperfetta, ma dove c'è un</cite> app store <cite>la strategia migliore per avere successo è costruire un grande prodotto. [&#8230;]</cite>

<cite>I risultati parlano da soli. Circa 320 mila persona hanno scaricato Evernote nella prima settimana di Mac App Store. Di questi, circa 120 mila non avevano mai usato Evernote. Questo rappresenta più del 50 percento di tutti i nuovi</cite> account <cite>Evernote creati settimana scorsa. Per noi Mac, che in registrazioni di nuovi utenti arrivava dietro iOS, Android e Windows, ora è balzato al primo posto. [&#8230;]</cite>

<cite>Spero che Windows abbia presto un buon</cite> app store<cite>.</cite>