---
title: "Chi viene a Quiliano il 14?<p>"
date: 2005-04-27
draft: false
tags: ["ping"]
---

La data corretta per l&rsquo;inaugurazione del primo museo europeo di Apple è questa<p>

Ieri ho scritto una scempiaggine. La data esatta dell&rsquo;inaugurazione dell&rsquo;All About Apple Museum in quel di Quiliano (Savona) è sabato 14 maggio 2005.<p>

Potevo guardare iCal, ma anche scrivere <code>cal 5 2005</code> nel Terminale, o usare il menu grafico di data e ora. La precisione è tutto.<p>

Ah&hellip; provato a scrivere nel Terminale <code>cat /usr/share/calendar/calendar.computer</code>?<p>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>