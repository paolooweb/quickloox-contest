---
title: "Aprire alla Grand"
date: 2009-09-11
draft: false
tags: ["ping"]
---

<a href="http://www.apple.com/it/macosx/technology/#grandcentral" target="_blank">Grand Central Dispatch</a> è una nuova tecnologia <i>software</i>, messa a punto da Apple per sfruttare al massimo le possibilità di multielaborazione dello <i>hardware</i> moderno (leggi: impegnare contemporaneamente tutti i processori e tutti i nuclei di calcolo per guadagnare tempo e prestazioni, là dove i programmi erano pigramente abituati a usarne uno solo e via).

Nessun altro ce l'ha. Esiste solo in Snow Leopard.

Apple l'ha appena <a href="http://libdispatch.macosforge.org/" target="_blank">resa open source</a>. Scaricabile, curiosabile, modificabile, migliorabile da chiunque, gratis.

E poi stiamo a parlare di chiuso e proprietario.