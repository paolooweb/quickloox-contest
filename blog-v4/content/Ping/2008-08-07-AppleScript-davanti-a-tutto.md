---
title: "AppleScript davanti a tutto"
date: 2008-08-07
draft: false
tags: ["ping"]
---

Lo script di oggi è molto banale:

<code>tell application "System Events"
	set frontmostApplication to name of the first process whose frontmost is true
end tell</code>

E non dirò che cosa fa. Basta provarlo, o leggerlo con attenzione. :-)

La parte interessante, che è poi l'istruzione in mezzo, segnala varie caratteristiche di valore del linguaggio e dell'applicazione System Events (invisibile e insostituibile per parlare, per cos&#236; dire, al Finder).

<code>frontmostApplication</code> è una variabile generale, che identifica sempre automaticamente il programma in primo piano. I programmi in funzione sul Mac si identificano (grossolanamente) con i <code>process</code>. <code>Frontmost</code> in inglese vuol dire <em>quello più avanti di tutti</em> e <code>true</code> è logica booleana (<em>true</em>=vero, <em>false</em>=falso). Se la proprietà <code>frontmost</code> è vera, accade quello che deve accadere, altrimenti no.

Che cosa deve accadere? Fare il nome (<code>to name</code>) del <code>process</code> che risponde al requisito.

L'ho preso a prestito da <a href="http://snippets.dzone.com/tag/applescript" target="_blank">Dzone</a>. Da visitare, per chi si interessa ad AppleScript.