---
title: "Il mistero di Eurodisney<p>"
date: 2005-07-01
draft: false
tags: ["ping"]
---

Schermi uguali a quelli di iMac, o viceversa?<p>

La parola a Roberto:<p>

<cite>Di recente sono stato a parigi ai Walt Disney Studio. Nell&rsquo;attrazione Tram Studio (un bus di circa otto elementi che percorre un itinerario tra scenografie e riproduzioni di film oltre a far assistere ad effetti speciali) sono installati in ogni modulo bus dei monitor Lcd.</cite><p>

<cite>Orbene, questi monitor lcd sono rinchiusi in una scatola metallica il cui aspetto è identico al monitor dell&rsquo;iMac G5&hellip; in pratica sembra di vedere un monitor di un iMac G5 da 20&rdquo;. Non so se nessuno ci abbia mai fatto caso.</cite><p>

<cite>L&rsquo;attrazione ha riaperto il 4 giugno dopo un restyling, quindi non so se i monitor sono installati di recente, oppure sono quelli originali, nel qual caso sarebbe la Apple ad averli copiati dalla Disney&hellip;</cite><p>

Qualcuno più addentro di noi a Disneyland Paris è in grado di risolvere il mistero?<p>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>