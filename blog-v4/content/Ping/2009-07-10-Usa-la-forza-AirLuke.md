---
title: "Mostra la forza, AirLuke"
date: 2009-07-10
draft: false
tags: ["ping"]
---

Dalla <a href="http://www.apple.com/it/macosx/refinements/enhancements-refinements.html" target="_blank">pagina di novità di Snow Leopard</a> che Apple non traduce ancora in italiano ecco un'altra novità, tradotta in italiano.

<b>Forza del segnale AirPort nella barra dei menu</b>

Il menu grafico AirPort ora mostra l'intensità del segnale per tutte le reti che trova, cos&#236; è possibile selezionare il segnale più potente tra quelli a disposizione.

È un tocco piccolo, di cui qualcuno neanche si renderà conto. Una costante di Snow Leopard, che di queste rifiniture è davvero pieno.