---
title: "Italia punto edu"
date: 2007-05-20
draft: false
tags: ["ping"]
---

Esistono situazioni come <a href="http://meshlab.sourceforge.net/" target="_blank">MeshLab</a>. Non avevo mai visto cos&#236; tanti sviluppatori italiani, in assoluto e come percentuale del totale.

Lo Stato, intanto, invece che strafinanziare la ricerca e lo sviluppo, taglia i fondi alle università e agli istituti. In compenso spende decine di milioni per l'angosciante portale Italia.it, roba che sarebbe stata vecchia già cinque anni fa.

Io farei piuttosto il contrario. Purtroppo l'Italia è condannata a rimanere .it ancora a lungo.