---
title: "Imparare giocando"
date: 2007-12-31
draft: false
tags: ["ping"]
---

I cruciverba sono un gran modo per fare pratica con una lingua straniera. Figuriamoci giocare a Scarabeo.

Specie se lo si può fare in 42 lingue diverse e avere le definizioni in una lingua e le parole nell'altra.

Non ho ancora finito di scaricare <a href="http://www.asymptopia.org/index.php?topic=TWS" target="_blank">Tux WordSmith</a>, ma non riuscivo a stare zitto. È persino <em>open source</em>.