---
title: "Venendo al punto"
date: 2009-02-24
draft: false
tags: ["ping"]
---

Pare che il prossimo album degli U2 sia stato messo su Internet senza permesso e c'è in corso l'indagine per scovare i responsabili.

TechCrunch titola <a href="http://www.techcrunch.com/2009/02/20/did-lastfm-just-hand-over-user-listening-data-to-the-riaa/#comment-2632012" target="_blank">Last.fm ha messo in mano alla Riaa i dati degli ascoltatori?</a>

Probabile che TechCrunch se la sia inventata di brutto. Perché Last.fm si arrabbia non poco e li definisce <a href="http://blog.last.fm/2009/02/23/techcrunch-are-full-of-shit" target="_blank">pieni di m&#38;*#@</a>, oltre a smentire la cosa dovunque può.

Roba che non succede tutti i giorni. John Gruber di Daring Fireball commenta cos&#236;: <a href="http://daringfireball.net/linked/2009/02/23/techcrunch-lastfm" target="_blank">TechCrunch pubblica volgari sciocchezze prive di fondamento e cerca inutilmente di coprirsi il posteriore mettendo un punto interrogativo alla fine dei loro titoli a sensazione? S&#236;</a>.

Quando vedi un titolo con il punto interrogativo, pensaci.