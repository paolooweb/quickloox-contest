---
title: "Una funzione venuta dal passato"
date: 2002-08-27
draft: false
tags: ["ping"]
---

Il premio “Feature più dimenticata con il miglior futuro davanti” va a...

Una delle novità migliori di Apple: iCal.
Finalmente un vero organizer, potente come lo vogliamo e facile da usare come lo sa fare solo Apple.

iCal consente di pubblicare la propria agenda appuntamenti in rete per chi desidera o ha bisogno di tenerne conto. Questa è a tutti gli effetti un’applicazione intelligente e funzionale di Publish & Subscribe, funzione presentata in System 7 più di dieci anni fa e che in seguito venne cancellata perché di fatto nessuno, tranne pochi illuminati, la usava.

Ora Publish & Subscribe (che in Italia era Pubblica e Sottoscrivi; forse questo spiega perché nessuno da noi l’abbia capita) fa un ritorno alla grande in un prodotto lungimirante.

Quando ci si lamenta con Apple perché non svende le tecnologie inutilizzate (un esempio per tutti: il riconoscimento della grafia su Newton) è il caso di pensare a esempi come questo. Se Apple si tiene stretta una tecnologia, in genere c’è un motivo.

<link>Lucio Bragagnolo</link>lux@mac.com