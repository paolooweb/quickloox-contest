---
title: "Disinformazione sempre aperta"
date: 2006-07-27
draft: false
tags: ["ping"]
---

Grazie a <strong>Monica</strong>, ho constatato per un’ennesima volta che a <a href="http://punto-informatico.it/p.aspx?id=1590880&amp;r=PI" target="_blank">Punto Disinformatico</a> non poteva sembrare vero di poter commentare una notizia come la chiusura di <a href="http://opendarwin.org/" target="_blank">OpenDarwin</a> per poterla dare a suo modo quando c'è Apple di mezzo, ossia un tanto al chilo.

La sciocchezza più gustosa è [l'eliminazione dal repository di Apple del] <cite>codice della versione x86 di Darwin</cite>.

Ho provato ad andare sul link di <a href="http://www.opensource.apple.com/darwinsource/10.4.6.x86/" target="_blank">Darwin 10.4.6 x86</a> e il codice c'è. Come la mettiamo? Prima di scrivere una cosa, si fa troppa fatica a guardare se è vera?

Parlano di <cite>progressiva riduzione</cite> del codice messo a disposizione. Noi ne parlavamo qua  mesi fa e sappiamo che una parte del codice x86 non è stata ancora messa a disposizione, ma nessun pacchetto presente sul repository è stato mai tolto. Dove sia la progressività, sfugge. Una riduzione rispetto al passato al momento c'è, ma che c'entri con i progetti OpenDarwin attuali, sfugge di nuovo.

Il resto delle sciocchezze è più sottile e non merita di perderci tempo (anche se vorrei sapere dove hanno trovato scritto <cite>multipiattaforma</cite>, <cite>inedite</cite>, <cite>innovative</cite> dentro il sito. Avranno una macro apposta per aggiungere automaticamente gergo alle notizie).

La verità è che sostanzialmente OpenDarwin, partito con obiettivi ambiziosi, era finito per diventare un analogo di Fink e, per quel tipo di cose, <a href="http://fink.sourceforge.net" target="_blank">Fink</a> è meglio. Punto.

Purtroppo a volte, messo il punto, poi si aggiunge l'informatico.