---
title: "I testi nelle nuvole"
date: 2010-06-19
draft: false
tags: ["ping"]
---

È l'ora di MobileMe.

I Mac sono consolidati, potenti, convenienti, versatili, efficienti (il nuovo Mac mini vale tutto l'aumento di prezzo in diminuzione di ingombro, scomparsa dell'alimentatore esterno senza surriscadalmenti della macchina, i minori consumi della categoria e adesso c'è perfino la porta Hdmi in più, in dimensioni ridotte).

iPhone da tempo ha smesso di essere un carneade. Nokia ha preannunciato un trimestre insoddisfacente a causa della forte competitività del mercato. Il gigante dei cellulari in difficoltà per via di un singolo prodotto, partito da zero anni fa; che cos'è, fantascienza? Ovviamente i guai per Nokia giungono anche dai fronti BlackBerry e Android, ma questo giro di valzer lo ha iniziato proprio iPhone. Qualcuno ricorda il mondo cellulare com'era prima?

iPad lo lascio a Paolo Attivissimo. Chi se lo ricorda il 29 gennaio? <cite>A che cosa serve iPad? Forse a gente che odia il computer e che ha soldi da spendere. Boh.</cite> Due milioni di unità vendute in meno di due mesi, la tavoletta più venduta in un mese di tutte quelle mai esistite da quando esistono i microprocessori, messe insieme. Invece di ammettere onestamente che a tutti capita di scrivere una scemenza, Paolo sta continuando a scendere la china di giudizi che difettano di comprensione dell'apparecchio. Problemi suoi.

Basta divagare, arrivo al punto. Lo schieramento hardware di Apple è forte e valido come veramente è mai accaduto. A questo punto manca una presenza altrettanto forte e valida su web. Google docet.

Su questo la proposta di Apple è molto migliorabile. MobileMe è un buonissimo servizio, che però ora deve valorizzare appieno le peculiarità diverse di Mac, iPhone e iPad nel momento in cui si collegano alla rete. Sono uno dei fortunati con tutti e tre gli apparecchi, ma non sono certo l'unico e si capisce al volo come sincronizzazione, <i>web application</i> e spazi condivisi darebbero una marcia in più a tutta l'offerta.

iWork dovrebbe dialogare con MobileMe, uscire dallo stadio di <i>beta</i>, diventare il centro di scambio dei documenti prodotti cos&#236; come MobileMe è quello delle informazioni.

Si vede qualche indizio. MobileMe ha una nuova versione della posta e sta accumulando nuove funzioni; iWork viene citato esplicitamente in vari documenti relativi a iPad (è uno dei modi di risolvere il problematico passaggio di documenti tra iPad e Mac), mentre finora era ignorato.

Se Apple riesce anche in questo, tanto di cappello. E la nostra vita, reparto digitale, vedrebbe un ulteriore salto quantico di qualità. Incrocio le dita, sul <i>trackpad</i>, augurandomi grandi annunci riguardanti il <i>cloud computing</i>.