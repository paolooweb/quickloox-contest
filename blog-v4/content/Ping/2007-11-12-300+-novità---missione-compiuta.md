---
title: "300+ novità: missione compiuta"
date: 2007-11-12
draft: false
tags: ["ping"]
---

Non è che abbia finito, ce ne sarebbe ancora; è che la <a href="http://www.apple.com/it/macosx/features/300.html" target="_blank">pagina delle novità di Leopard sul sito Apple</a> è stata finalmente tradotta in italiano.

Forse, a questo punto, avrei dovuto titolare <em>mission accomplished</em>.