---
title: "Trimestre fiscale, giudizio geniale"
date: 2004-01-19
draft: false
tags: ["ping"]
---

Il commento migliore possibile agli ultimi risultati finanziari di Apple

Dopo che Apple ha annunciato i suoi (positivi) risultati finanziari del trimestre, i commenti si sono sprecati, soprattutto da parte di chi faceva meglio a stare zitto.
Invece c’è chi ha capito tutto e lo ha spiegato come meglio non si poteva: l’amico Sergio Leone (sergioleone@katamail.com), che è il protagonista di questo Ping! Eccogli la parola:

Sembra che i risultati fiscali appena presentati daranno da pensare a quelli che da 20 anni pronosticano la sempre data per imminente chiusura di Apple. In particolare avranno di che riflettere quelli che considerano il Mac un prodotto di nicchia e quindi ogni minchiata la fanno solo per Windows blindato con Exploder. In particolare sarebbe carino far loro confrontare i numeri dello store musicale di Cupertino con i tanti esperimenti miseramente falliti (o aspiranti tali), iniziative di gente che confida troppo nel marketing un tot al chilo.
È proprio vero, Windows porta sfiga.

<link>Lucio Bragagnolo</link>lux@mac.com