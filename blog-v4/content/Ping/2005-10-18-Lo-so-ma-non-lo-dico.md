---
title: "Lo so ma non lo dico<p>"
date: 2005-10-18
draft: false
tags: ["ping"]
---

Le chiacchiere sulle prossime mosse di Apple dovrebbero rispondere a questo principio<p>

Scrivo questa nota la sera di martedì 18 ottobre. So perfettamente, da una settimana, che cosa annuncerà Apple domani.<p>

Non è questione del come. Se hai la fonte giusta, sapere le cose qualche giorno prima è una sciocchezza.<p>

È questione di che cosa fare. Non dico niente. Ci sarebbero numerose ragioni, ma ne scelgo una sola. Tante persone in Apple si sono adoperate per dare un annuncio domani. Il loro lavoro va rispettato. Fine. Niente marketing, niente deontologia, niente alta finanza. Rispetto.<p>

Se qualcuno avesse anticipato ieri che cosa volevo scrivere in questa pagina, ci sarei rimasto male. È così strano riconoscere questo diritto anche agli altri?<p>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>