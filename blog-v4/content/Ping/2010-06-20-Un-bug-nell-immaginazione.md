---
title: "Un bug nell'&#8220;immaginazione&#8221;"
date: 2010-06-20
draft: false
tags: ["ping"]
---

La chiavetta mi impedisce di installare Mac OS X 10.6.4. Leggo però nel frattempo da The Unofficial Apple Weblog che <a href="http://www.tuaw.com/2010/06/20/mail-app-in-10-6-4-pastes-screenshots-as-tiff/" target="_blank">pare esistere un <i>bug</i></a> riguardante le immagini portate nella Clipboard e poi incollate in Mail.

Le immagini in questione diventano di formato Tiff. Niente di male, solo che la loro dimensione aumenta inutilmente e la cosa, dentro un programma di posta, è disdicevole.

<i>Comando-Maiuscole-3</i> e <i>Comando-Maiuscole-4</i> registrano sulla Scrivania schermate in formato Png (3 è per lo schermo pieno, 4 per una finestra dimensionabile o, se si preme una volta la barra spaziatrice, per elementi distinti dell'interfaccia, come una finestra).

Se a <i>Comando-Maiuscole-numero</i> si aggiunge Control, la schermata non viene registrata sulla scrivania, ma caricata in memoria negli Appunti (la Clipboard), pronta da incollare dove serve.

Se incollata in Anteprima, o in qualunque altro programma, è normalmente un Png; in Mail diventa invece Tiff.

Meglio verificare e saperlo, per evitare di mandare allegati più grandi di quello che dovrebbero. Se accade, basta fare un passo in più e produrre l'immagine da accludere a Mail in qualsiasi altro modo.

Anteprima converte un'immagine qualsiasi in uno qualsiasi di questi formati.

Dai commenti nel <i>post</i> di The Unofficial Apple Weblog si capisce che la cosa non capita a tutti, ma a pochi. Una verifica ci sta bene.