---
title: "Gli Ultima sono i primi"
date: 2010-04-22
draft: false
tags: ["ping"]
---

Zork non è più l'unico grande gioco della generazione pionieristica a essersi trasformato in esperienza da browser, con <a href="http://legendsofzork.com/" target="_blank">Legends of Zork</a>.

Ultima Online, il primo grande gioco di ruolo di massa grafico su Internet, ha generato infatti <a href="http://www.lordofultima.com/en" target="_blank">Lord of Ultima</a>.

Il gioco, appena esplorato, è un tipico strategico di gestione risorse, con la propria città da fare prosperare e intorno le risorse naturali da raccogliere e trasformare, meglio e più in fretta degli altri signorotti che risiedono nelle aree circostanti.

Confesso che il mio interesse per lo strategico di gestione è relativo e, per lo strategico in generale, continuo ad adorare <a href="http://wesnoth.org" target="_blank">Battle for Wesnoth</a> su Mac e iPhone.

Consiglio tuttavia la visita per constatare l'impegno messo da progettisti e grafici nell'interfaccia del gioco, che davvero alza nuovamente l'asticella dello stato dell'arte nella programmazione del <i>browser</i>.