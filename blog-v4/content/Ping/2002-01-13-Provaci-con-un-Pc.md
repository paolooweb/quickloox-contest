---
title: "Provaci con il Pc"
date: 2002-01-13
draft: false
tags: ["ping"]
---

È molto tardi e sto lavoricchiando senza impegno, mentre chiacchiero in Icq con il mio amico Andrea. Ecco uno dei suoi messaggi:

“Per la cronaca... sto lavorando (un po’ a fatica in verità, ma va...) con Flash 5 sotto Classic mentre sotto in Mac OS X ho aperto Explorer, ICQ, Entourage... e il DVD di Matrix!
Non va come XP, ma fatelo con un pc! :-)”

Già... vorrei proprio vedere un Pc che fa funzionare due sistemi operativi intanto che gestisce un Dvd.

lux@mac.com