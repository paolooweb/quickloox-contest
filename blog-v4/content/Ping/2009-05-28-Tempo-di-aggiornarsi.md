---
title: "Tempo di aggiornarsi"
date: 2009-05-28
draft: false
tags: ["ping"]
---

Una risposta alla frenesia da combo: da adesso il mio Aggiornamento Software è impostato sul controllo automatico con frequenza Mensile.

Ancora meno tempo passato a occuparsi degli aggiornamenti. Altro che lo scaricamento manuale.