---
title: "Quel che non fa il mercato"
date: 2007-06-21
draft: false
tags: ["ping"]
---

Lo fa il marketing. Se Apple non vendesse neanche un iPhone, ci avrebbe guadagnato ugualmente una cifra in pubblicità gratuita.