---
title: "Adockiare spazi"
date: 2009-06-22
draft: false
tags: ["ping"]
---

Secondo me Apple tradurrà la pagina delle <a href="http://www.apple.com/it/macosx/refinements/enhancements-refinements.html" target="_blank">novità di Snow Leopard</a> quando il sistema operativo è già uscito. Credo sia meglio farlo prima che esca, invece, per sapere a che cosa si va incontro. Anche una novità al giorno, c'è tempo.

Fino a ieri parlavamo del Finder; adesso siamo in zona Dock.

<b>Assegnare applicazioni agli spazi</b>

Assegnamo un'applicazione a qualunque spazio dalla sua icona nel Dock con un clic. Distribuiamo rapidamente il lavoro su spazi differenti assegnando un'applicazione a uno spazio direttamente dal Dock. È sufficiente fare clic destro sull'icona dentro il Dock e selezionare lo spazio che riguarda l'applicazione.

Di questo passo finirò persino io per usare il Dock!