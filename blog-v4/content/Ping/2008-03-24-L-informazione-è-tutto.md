---
title: "L'informazione è tutto"
date: 2008-03-24
draft: false
tags: ["ping"]
---

Ogni tanto aggiungo un salvaschermo alla mia raccolta. L'ultimissimo è <a href="http://zuurdesign.com/downloads/" target="_blank">MoodSaver</a>: reagisce alla quantità di lavoro del processore.

Quindi, oltre che ad animare lo schermo, è un eccellente segnalatore di quando è finito un encoding, o un lungo trasferimento di file eccetera eccetera.

45 kilobyte, freeware, è una spesa non averlo. :-)