---
title: "Sta per cambiare tutto"
date: 2009-12-30
draft: false
tags: ["ping"]
---

Niente meglio di un inizio anno è opportuno per segnalare tendenze importanti.

<a href="http://www.onlive.com/" target="_blank">OnLive</a> ha messo a punto un sistema che permette di giocare su un server remoto. E fin qui, chi se ne frega.

Il fatto è che tutto quello che serve su Mac e su Pc è un <i>plugin</i> per il <i>browser</i>. Niente attese di scaricamento, niente ingombro sul disco rigido.

Il fatto è che la stessa cosa si fa con il televisore, previa connessione a un aggeggino da due soldi grande come un disco rigido portatile e un <i>controller</i> stile <i>console</i> (se questa cosa attecchisce, le <i>console</i> dovranno reinventarsi e non poco).

Il fatto è che la qualità è sempre massima, indipendentemente dalla configurazione locale (mica per niente è sufficiente un televisore).

Il fatto è che ci sono modi per partecipare anche da un iPhone o altro apparecchio <i>mobile</i>.

Il fatto è che si può guardare sulla propria tivù quello che stanno facendo altri giocatori, come se fosse uno spettacolo.

Il fatto è che esiste interazione tra giocatori e anche tra spettatori.

Il servizio potrebbe diventare operativo già quest'anno, ovviamente negli Stati Uniti, per il fatto che l&#236; il 70 percento delle case ha già una due megabit vera e il ventisei percento ha una cinque megabit vera, che serve per l'alta definizione.

Consiglio vivamente di sorbirsi il lunghissimo <a href="http://www.viddler.com/explore/gamertagradio/videos/160/" target="_blank">video di presentazione del servizio</a>. Se io mi sono sorbito un video di quarantotto minuti, vuol proprio dire che è il caso.

A margine, Steve Perlman, l'animatore di OnLive, è un ex QuickTime Guru di Apple. Sempre a margine, è da notare il portatile con cui tiene la propria presentazione.

Sempre a margine, Perlman tiene la propria dimostrazione in una auletta della Columbia University, davanti a una platea di studenti, invece che in un centro congressi o in un albergo davanti a una platea di giornalisti snobboni annoiati e incompetenti, in camicia invece che in completo sartoriale. Motivi in più per i quali una cosa del genere nasce per forza negli Stati Uniti.