---
title: "Due righe di video"
date: 2010-03-11
draft: false
tags: ["ping"]
---

Accatiemmellisti di tutto il mondo, sappiate che per <a href="http://code.google.com/p/html5media/" target="_blank">inserire video Html5 nelle vostre pagine</a>, invece che quel pesante e instabile Flash, vi basta aggiungere una riga di codice nella sezione <head> e una seconda riga dove deve andare il video vero e proprio.

Gran comodità e risparmio di processore.