---
title: "Caratteri da distinguersi"
date: 2006-05-21
draft: false
tags: ["ping"]
---

Come forse raccontavo, nella campagna di gioco di ruolo che sto conducendo abbiamo affrontato un tratto di gioco per posta, o play-by-email, o Pbem. Avevo bisogno di disegnare mappe in formato Ascii, per poter aggiornare rapidamente gli spostamenti dei personaggi e il resto.

Ho scoperto <a href="http://www.jave.de" target="_blank">JavE</a>, che &egrave; semplicemente fantastico per disegnare in Ascii, e fa anche molto di pi&ugrave;. Per guardare fino a dove si possa arrivare, per ora obbligatoriamente con Firefox, non c&rsquo;&egrave; che provare <a href="http://www.asciimaps.com/" target="_blank">Google Maps in Ascii</a>!