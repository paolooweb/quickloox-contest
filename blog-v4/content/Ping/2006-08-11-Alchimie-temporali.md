---
title: "Alchimie temporali"
date: 2006-08-11
draft: false
tags: ["ping"]
---

Sto iniziando (in modalità vacanze, confesso) a investigare su Time Machine. Finora ho capito ben poco, ma quel poco è sicuro: Apple non si affida a una semplice implementazione dei vari comandi diff di Unix. Da una parte i backup potrebbero avere dimensione minima, dall'altra però un sistema senza altre infrastrutture rischia di divenire presto ingestibile.

Time Machine è una tecnologia in via di affinamento e non un comando qualunque con sopra un po' di interfaccia grafica. Quali e quanti saranno i compromessi lo dirà il&#8230; tempo.