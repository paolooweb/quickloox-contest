---
title: "Fatti per immagini"
date: 2010-01-21
draft: false
tags: ["ping"]
---

Scrive <b>Marco</b>:

<cite>Ciao Lucio,
conosci <a href="http://www.artrage.com/" target="_blank">ArtRage</a>?</cite>

<cite>Ecco <a href="http://www2.ambientdesign.com/gallery/" target="_blank">i risultati che ci si possono ottenere</a> senza andare a spendere follie per Photoshop (e finire con l’usare un quinto del programma).</cite>

<cite>E magari unendoci <a href="http://tenonedesign.com/inklet.php" target="_blank">Inklet</a>…</cite>

La storia di ArtRage è quella di numerosi altri programmi e tuttavia spesso rimangono solo parole. Vedere i fatti aiuta a capire. Grazie mille. :)