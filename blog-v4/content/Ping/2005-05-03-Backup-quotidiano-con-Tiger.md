---
title: "Tiger e il backup quotidiano<p>"
date: 2005-05-03
draft: false
tags: ["ping"]
---

Devo ancora verificare se funziona. Ma se funziona&hellip;<p>

Le mie politiche di backup sono molto semplici. Dvd del lavoro fatto e in corso, uno al mese.<p>

In più approfitto dell&rsquo;abbonamento a .Mac per effettuare un backup automatico su Internet del lavoro effettuato nella giornata. Anche questo è semplice, ma a volte un po&rsquo; macchinoso.<p>

Tiger e la smart folder, però, cambiano tutto. Se posso creare una smart folder che si aggiorna dinamicamente, giorno per giorno, con il lavoro di &ldquo;oggi&rdquo;. E la collego a Backup, per dire, posso ritrovarmi automaticamente backuppato il lavoro delle 24 ore.<p>

Per quanto riguarda il Dvd è ancora più facile: attivo una cartella di masterizzazione selezionando i documenti salvati nell&rsquo;ultimo mese e fa tutto lui.<p>

Tiger mi piace proprio. :-)<p>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>