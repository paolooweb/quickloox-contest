---
title: "Paghi uno prendi tre"
date: 2007-09-20
draft: false
tags: ["ping"]
---

Tre statistiche per capire come sta andando iPhone:

- Nel primo mese di vendita piena,  cioè luglio, la <a href="http://www.reuters.com/article/technologyNews/idUSL0432369320070904" target="_blank">quota di mercato</a> (criterio imbecille, ma sembra che non se ne possa prescindere) di iPhone è stata l'1,8 percento;

- nel primo mese di vendita piena, molti negozi <a href="http://www.blackfriarsinc.com/blog/2007/09/one-million-iphones-sold" target="_blank">sono stati sprovvisti</a> di iPhone per molti giorni e dunque il risultato avrebbe potuto essere superiore;

- sono stati diffusi i risultati di <a href="http://gigaom.com/2007/09/07/symbian-vs-apple-google/" target="_blank">vendita dei cellulari nel mondo nel primo semestre del 2007</a>, divisi per sistema operativo e per aree geografiche. In America, unico mercato dove iPhone era presente, la quota di mercato è visibilmente molto superiore all'uno percento che Steve Jobs ha dichiarato pubblicamente come obiettivo.

Si noti che i dati degli altri sistemi operativi si riferiscono a vendite per <em>sei mesi</em>, dove i dati Apple valgono invece <em>un weekend</em>. Nel primo semestre 2007, iPhone è stato in vendita il 29 e il 30 giugno. Approssimativamente, gli altri telefoni sono stati in vendita per un tempo ottanta volte superiore.

Finirà che la quota di mercato di iPhone dovremo dividerla per tre (cellulare, iPod, successore di Newton), cos&#236; non si spaventa più nessuno.

Grazie a <strong>Luca</strong> che mi ha incoraggiato a raccogliere i link.