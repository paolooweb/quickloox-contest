---
title: "Neanche me ne sono accorto"
date: 2007-12-06
draft: false
tags: ["ping"]
---

Il salvaschermo che mostra a mosaico tutte le copertine degli album di iTunes, tempo fa, per mia pigrizia, mostrava solo una manciata di copertine.

Adesso guardavo per combinazione il salvaschermo (ne ho decine e la scelta è casuale) e le copertine ci sono tutte, senza alcun intervento manuale.

Non so se ringraziare Leopard o iTunes. Quando il computer lavora per te è esattamente come deve essere.