---
title: "Magic, l'Adunanza"
date: 2009-01-08
draft: false
tags: ["ping"]
---

A me la chiacchierata in <em>chat</em> per il <em>keynote</em> di Phil Schiller è proprio piaciuta e ringrazio di vero cuore quanti hanno voluto condividere almeno un minuto del loro tempo.

A dire il vero c'era una pattuglia di anticipatori già a ora di pranzo, con l'appuntamento alle 18, e gli ultimi hanno salutato la compagnia ben dopo mezzanotte.

In mezzo, gran casino, pareri di tutti, scherzi, discussioni, attese, delusioni, entusiasmi, spiegazioni, dubbi, chiarimenti, ritrovi, saluti, abbracci e tutti con una gentilezza e una tranquillità meravigliose.

La scelta di spostarsi su Irc è stata saggia. Numeri alla mano, fossimo rimasti su iChat sarebbero rimaste chiuse fuori molte persone per molte ore. Invece c'è stato spazio per tutti. Chi non lo sapeva, invitato a passare in Irc in tempo reale, ce l'ha fatta nel giro di pochi istanti: la procedura era semplice e i convenuti persone intelligenti. Chi è rimasto fuori ha avuto problemi più seri di cui occuparsi oppure era sul lavoro ed è stato pienamente giustificato.

Ne approfitto per ringraziare ancora una volta anche <strong>gand</strong>, che ha gentilissimamente prestato il canale Irc di <a href="http://freesmug.org" target="_blank">Freesmug</a>. Spero che sia servito a fare conoscere un po' di più la sua ottima iniziativa.

Insomma, non voglio parlare di successo. Non c'era niente da vendere né alcuno da convincere. Sono invece andato a dormire contentissimo di esserci stato e se qualcun altro ha avuto la stessa sensazione, sono più contento al cubo.

Detto ciò, anche se pare dia qualche problema di codifica, confesso di preferire per ora il vecchio <a href="http://xchataqua.sourceforge.net/twiki/bin/view/Main/WebHome" target="_blank">X-Chat Aqua</a> allo scintillante <a href="http://colloquy.info" target="_blank">Colloquy</a>.