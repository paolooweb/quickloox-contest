---
title: "La corretta proporzione"
date: 2010-06-29
draft: false
tags: ["ping"]
---

Nel corso di una causa in tribunale sono stati resi pubblici documenti che riguardano la condotta commerciale di Dell. <a href="http://www.nytimes.com/2010/06/29/technology/29dell.html" target="_blank">Ne parla</a> il New York Times.

Questa fa ridere.

<cite>Dopo che la facoltà di matematica dell'università del Texas si è lamentata dei malfuzionamenti delle macchine Dell, l'azienda le ha esaminate ed è arrivata alla conclusione che l'istituto aveva sovraccaricato le macchine con calcoli matematici molto difficili.</cite>

<cite>In realtà Dell aveva inviato alla sede di Austin dell'università Pc desktop pieni di componenti elettrici difettosi, che perdevano sostanze chimiche e causavano i problemi.
</cite>

<cite>I dipendenti dell'azienda sapevano che i computer probabilmente si sarebbero rotti, ma comunque cercavano di minimizzare con i clienti.</cite>

Questa fa ancora più ridere.

<cite>Perfino lo studio legale che difende Dell nella causa ci ha rimesso, quando Dell ha esitato a riparare mille computer sospetti di essere difettosi.</cite>

Questa fa ridere veramente.

<cite>Uno studio di Dell rilevava che i computer OptiPlex contenenti i capacitori difettosi si sarebbero guastati entro tre anni nel 97 percento dei casi. In altri documenti riguardanti il trattamento della questione emerge che ai venditori veniva detto di</cite> non portare attivamente il tema all'attenzione dei clienti <cite>e di</cite> dare enfasi all'incertezza<cite>.</cite>

Questa fa un po' arrabbiare.

<cite>Nel 2007 Dell ha riaggiustato i propri guadagni dal 2003 al 2006 e del primo trimestre 2007, abbassando i totali di fatturato e utile netto. Una verifica ha rivelato che i dipendenti Dell avevano manipolato i risultati finanziari per rispettare gli obiettivi di crescita.</cite>

Da tenere presente per il prossimo scandalo mediatico dovuto a un Mac con un capello fuori posto.