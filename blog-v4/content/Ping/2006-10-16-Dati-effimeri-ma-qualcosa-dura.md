---
title: "Dati effimeri ma qualcosa resta"
date: 2006-10-16
draft: false
tags: ["ping"]
---

La <a href="http://random-international.squarespace.com/untitled-temporary-printing-ma/" target="_blank">Temporary Printing Machine</a>, creata da Random International.

Immagini e testo non vengono stampate con inchiostro, ma impressi mediante luce ultravioletta su una superficie reattiva, sulla quale il contenuto rimane visibile per quasi un minuto e poi svanisce lentamente, a significare il carattere effimero dei dati digitali.

Non so se sia la migliore lezione sul backup mai fatta o se ci sia da riflettere sul fatto che per l'installazione dell'effimero digitale è stato scelto il computer più duraturo. :-)