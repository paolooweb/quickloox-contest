---
title: "Cambiamento atipico"
date: 2009-01-20
draft: false
tags: ["ping"]
---

Se c'è una cosa che si fa fatica a cambiare è l'uso dei font. Da sempre, per elaborare testo puro, ho usato Monaco.

<a href="http://www.barebones.com/products/bbedit" target="_blank">BBEdit 9.1</a> contiene Consolas Regular, un font di Ascender Corporation concepito apposta per il testo puro e il codice di programmazione.

Ho voluto provare e all'inizio mi sembrava di essere stato cacciato da casa. Il giorno dopo, però, ho trovato naturale vederlo e il giorno dopo ancora è divenuto il mio nuovo font di lavoro sul testo puro.

Se c'è una cosa che si fa ancora più fatica a cambiare è l'idea che la prima, naturale, reazione ai cambiamenti sia sufficiente a lasciarci nell'abitudine.