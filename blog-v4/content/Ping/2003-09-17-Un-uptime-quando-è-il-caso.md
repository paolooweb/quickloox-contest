---
title: "Un uptime quando è il caso"
date: 2003-09-17
draft: false
tags: ["ping"]
---

Piccola dimostrazione pratica della grandezza di Mac OS X

Sono piuttosto fiero dell’uptime del mio Mac e mi piace l’idea di comunicarlo al mondo, di tanto in tanto, per non scocciare. Ho deciso allora di inserire tra le mie signature casuali di posta elettronica una che notifica l’uptime.

Con il mio programma di email (Mailsmith, <link>http://www.barebones.com</link>) le signature casuali stanno tutte dentro un file di testo e il programma, al momento di spedire un messaggio, pesca a caso una signature tra quelle presenti.

Mailsmith supporta anche un glossario: ossia, si possono codificare abbreviazioni che corrispondono a testi più lunghi (se scrivo sempre “cordiali saluti” posso codificare una voce di glossario “co” che, inserita nel messaggio, si espanda in “cordiali saluti”; si risparmia tempo).

Il glossario di Mailsmith può anche contenere voci corrispondenti a comandi del Terminale. Quindi è facile impostare una voce di glossario che esegue il comando uptime e riporta il risultato nel messaggio.

In definitiva, nella versione 1.0, ho inserito nel file delle signature casuali un oggetto di glossario che richiama il comando uptime.

Ma si può fare meglio. uptime, infatti, restituisce informazioni che in una signature sono inutili, come il numero di utenti sul sistema e i carichi di lavoro dello stesso. Io volevo che venisse riportato solo l’uptime.

Siccome sono ignorante e non conosco Unix, sono ricorso ad AppleScript, che è una via alla programmazione assai più facile del Terminale. In AppleScript ho copiato e adattato, dai milioni di esempi rintracciabili su Internet, un piccolo script che applica una espressione regolare al messaggio di uptime e ne conserva solo le parti che interessano a me.

Così ho modificato la voce di glossario in Mailsmith, in modo che esegua via Terminale non uptime, ma un AppleScript che esegue uptime via Terminale e lo lavora. Già che c’ero, ho fatto iniziare la mia signature con la maiuscola (uptime contiene solo minuscole) e l’ho fatta terminare con un punto (che uptime non ha).

Detta così sembra una cosa lunga, ma in realtà anche a un incapace come me per realizzare l’accrocchio basta mezz’ora.

Vengo al punto. Qualsiasi programmatore potrebbe fare meglio di me, in meno tempo e con più eleganza. Ma la soluzione a portata di essere umano che ho usato io può essere realizzata solo su Mac. Niente Windows, che non ha AppleScript; niente Linux, che non ha AppleScript.

Mac è un gran computer e Mac OS X un fior di sistema operativo.

<link>Lucio Bragagnolo</link>lux@mac.com
