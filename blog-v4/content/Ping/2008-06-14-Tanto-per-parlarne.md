---
title: "Tanto per parlarne"
date: 2008-06-14
draft: false
tags: ["ping"]
---

Anche <a href="http://mumble.sourceforge.net/" target="_blank">Mumble</a> consente di chattare audio liberamente tra gente con Mac OS X, Windows e Linux. È studiato per i giochi, quindi è molto efficiente e leggero. Nonché <em>open source</em>.