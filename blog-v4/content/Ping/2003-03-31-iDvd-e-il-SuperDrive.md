---
title: "Programmi fatti per vendere"
date: 2003-03-31
draft: false
tags: ["ping"]
---

Un programma scritto per una stampante deve funzionare su tutte le stampanti?
Un programma scritto per uno scanner deve funzionare su tutti gli scanner?
Il software di consultazione di un’enciclopedia deve funzionare per tutte le enciclopedie?
Google deve entrare in azione anche quando consulto Altavista?

Ovviamente no; sono tutti programmi che esistono per promuovere un prodotto, hardware (una stampante, uno scanner) o software (il sito Google, che genera una serie di proventi che servono a tenere in vita il motore.

Allora, perché stracciarsi le vesti se iDvd funziona solo sui SuperDrive? iDvd serve a vendere i SuperDrive.

<link>Lucio Bragagnolo</link>lux@mac.com