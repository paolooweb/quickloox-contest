---
title: ".Mac Mail Junk"
date: 2006-10-29
draft: false
tags: ["ping"]
---

La nuova interfaccia per la posta da web di <a href="http://www.apple.com/dotmac" target="_blank">.Mac</a> è semplicemente ottima, quasi a livello di quella di <a href="http://mail.google.com" target="_blank">Gmail</a>. L'unica cosa non immediata: per avere a disposizione il filtro antispam, bisogna avere impostato da Mail, quello sul computer, la conservazione su .Mac dei messaggi di spam. In questo modo, tra le cartelle a disposizione apparirà anche Junk e sarà possibile contrassegnare i messaggi di posta indesiderata come tali anche su .Mac Mail.