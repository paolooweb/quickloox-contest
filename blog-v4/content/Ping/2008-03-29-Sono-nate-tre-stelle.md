---
title: "Sono nate tre stelle"
date: 2008-03-29
draft: false
tags: ["ping"]
---

Gli impuniti (e sempre più bravi) dell'All About Apple Museum sono anche <a href="http://www.allaboutapple.com/movies/telecitta.htm" target="_blank">andati in tivvù</a>, pure in diretta.

Aborro il tempo-spazzatura buttato su YouTube e compagnia e invece amo quarti d'ora come questo. :)