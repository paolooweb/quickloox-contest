---
title: "Cellulare Connection<p>"
date: 2005-08-12
draft: false
tags: ["ping"]
---

Mi ricredo, a denti stretti, su Bluetooth<p>

Fino a ieri il rapporto quotidiano tra Mac e cellulare, nella mia vita, era zero assoluto. Odio i cellulari e non mi piace interfacciarli con i computer. So di sbagliare, ma lo vedo necessario quanto l&rsquo;interfacciamento tra frigoriferi e ferri da stiro.<p>

Ieri la pigrizia ha generato un&rsquo;esigenza nuova e mi sono ritrovato in casa un cellulare Bluetooth, preso apposta perché si collegasse al Mac.<p>

È stata una sorpresa. Computer e telefonino si sono visti al volo tramite la connessione Bluetooth. Un clic e iSync ha portato sulla rubrica del cellulare tutti i miei contatti. Due clic ed ero collegato a Internet via cellulare.<p>

Ancora non mi piace, ma devo ammettere che è diventato maledettamente facile.<p>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>