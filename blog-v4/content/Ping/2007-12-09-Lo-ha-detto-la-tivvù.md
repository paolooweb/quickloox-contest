---
title: "Lo ha detto la tivvù"
date: 2007-12-09
draft: false
tags: ["ping"]
---

C'è gente che crede di essere informata su Apple avendo letto un libro: per esempio, <a href="http://www.amazon.com/Apple-Intrigue-Egomania-Business-Blunders/dp/0812928512" target="_blank">Apple: The Inside Story of Intrigue, Egomania, and Business Blunders</a> di Jim Carlton.

In realtà si è preparati quando si leggono più fonti, si analizzano più punti di vista e si arriva a poter formulare una sintesi approfondita. Qui sotto trascrivo, tradotto, un frammento della <a href="http://www.gcsf.com/pages/mwj/mwj_samples/MWJ_19971103.html" target="_blank">recensione del libro</a> in questione scritta da Mdj:

<cite>Il libro interpreta gli stessi fatti in modo differente in pagine differenti, a seconda del modo in cui Apple fa la figura peggiore; fa malamente cilecca quando discute di nozioni che non venivano da più fonti diverse; Carlton fa pip&#236; fuori da vaso sulle questioni tecniche e lo si vede chiaramente; in alcuni casi la ricerca è cos&#236; trascurata che chiunque potrebbe verificarlo con una telefonata. Il libro mescola in modo irritante le previsioni con il senno di poi, nell'assunto che quanto è chiaro oggi era ugualmente chiaro dieci anni fa, a essere stati abbastanza intelligenti. Ma, peggio di tutto, [&#8230;] si accosta a tutto il materiale presupponendo che Apple sta fallendo e Apple non ha possibilità di farcela. [&#8230;]</cite>

<cite>Quelli che conoscono Apple dovranno vedersela con tutti questi problemi prima di trovare informazioni nuove o utili. [&#8230;] Dobbiamo assolutamente raccomandare a quelli che non conoscono Apple di stare lontani da questo libro. [&#8230;]</cite>

<cite>Carlton reputa necessario distorcere i fatti o sorvolare su un aspetto della vicenda per mettere Apple in cattiva luce. Unito [ciò] a imprecisioni di base, sia tecniche che non tecniche, e evidenti incomprensioni che crediamo nascano dalla mancanza di prospettiva [&#8230;] il libro lascerà al lettore non informato una prospettiva grossolanamente imprecisa dell'azienda.</cite>

Cos&#236; come è sbagliato prendere per oro colato qualsiasi cosa dicano al telegiornale, è altrettanto sbagliato credere ciecamente a qualunque cosa venga stampata su un libro.