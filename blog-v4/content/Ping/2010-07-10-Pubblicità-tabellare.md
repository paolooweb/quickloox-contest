---
title: "Pubblicità tabellare"
date: 2010-07-10
draft: false
tags: ["ping"]
---

Per gli interessati agli andamenti generali di Apple, c'è uno <a href="http://seekingalpha.com/article/213656-2010-the-year-apple-enters-a-new-golden-age" target="_blank">splendido articolo</a> condito da tabelle e diagrammi in abbondanza su Seeking Alpha.

L'articolo è lunghetto; i grafici però sono immediati e di ottima comprensione.