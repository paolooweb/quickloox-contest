---
title: "Ironie su Mail<p>"
date: 2005-05-16
draft: false
tags: ["ping"]
---

Ma sempre con sotto verità su cui riflettere<p>

Il dibattito su Mail continua e faccio da spettatore, non usandolo. Ecco quanto manda Sergio Bierti:<p>

<cite>&hellip;infatti questa mail la sto scrivendo con Entourage&hellip;</cite><p>

<cite>Boh, io proprio non li capisco, questi di Apple. Ma non testano i programmi che mandano fuori? Probabilmente sì, e allora vi dico: cambiate beta (e alfa) tester!</cite><p>

<cite>Su invito di un amico che vuole mantenere Microsoft-free il suo computer, convinto dell&rsquo;utilità di avere tutti gli indirizzi in un posto solo, e non su Rubrica Indirizzi, Entourage, antico e mitico file di FileMaker eccetera, ci ho provato.</cite><p>

<cite>Due giorni di casino per portare gli indirizzi da FileMaker a Indirizzi, un&rsquo;oretta da Entourage (soprattutto i gruppi, da rifare daccapo&hellip;), tutto l&rsquo;ambaradan di riconfigurazione degli account&hellip; E poi Mail non si connette al server di posta dell&rsquo;ospedale dove lavoro. Sì, ho settato tutto per bene: infatti Safari funziona alla grande, ed Entourage con gli stessi settaggi va come una scheggia.</cite><p>

<cite>Ehi, dico a voi: gran parte del mondo funziona con reti basate su Unix o su NT, diamoci una mossa, proviamoli i programmi anche in condizioni &ldquo;difficili&rdquo;, specie se i casini si verificano anche con le versioni nuove di stecca, come Mail di Tiger!</cite><p>

<cite>Per di più, non si riesce a far funzionare la finestra che in teoria ti dovrebbe permettere di bloccare un&rsquo;azione. Lo so, c&rsquo;è il bottone rosso, ma intanto che si accorge che l&rsquo;hai premuto i ghiacci si sono ritirati e il livello del mare è salito di un paio di metri.</cite><p>

<cite>Mi propongo come gamma-tester: quello che usa il computer come l&rsquo;automobile, e non gli frega di sapere che con una riga di terminale si cambia la vita: perché mi sarei comprato un Mac nel 1991, se non per sfuggire a C>? Se la macchina non va la porto dal concessionario, mica mi ci sdraio sotto per scoprire non so cosa (appunto: non so cosa, e non lo voglio sapere).</cite><p>

Su alcune affermazioni mi viene da alzare un sopracciglio (Mail non lo uso, ma l&rsquo;ho visto usare e indubitabilmente, se le impostazioni sono corrette, funziona) ma lascio che il dibattito prosegua e possibilmente che sia qualche sostenitore di Mail a dire la sua.<p>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>