---
title: "Cose di vetro"
date: 2008-11-20
draft: false
tags: ["ping"]
---

La ricerca e sviluppo di Apple non va solo in computer e microelettronica.

Mi fa notare <a href="http://www.sokirra.com" target="_blank">Irina</a>, per tramite di <a href="http://www.spobe.com/" target="_blank">Jida</a>, che Apple detiene vari brevetti riguardante <a href="http://www.ifoapplestore.com/stores/glass_staircase.html" target="_blank">le scale di vetro</a> che corredano mirabilmente gli Apple Store.

Mi sa che ne parlerò uno di questi mesi anche su Macworld cartaceo. È informazione che merita perché non è solo business e qualsiasi altra azienda avrebbe semplicemente chiesto a un architetto.