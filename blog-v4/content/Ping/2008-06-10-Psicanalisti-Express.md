---
title: "Psicanalisti Express"
date: 2008-06-10
draft: false
tags: ["ping"]
---

Stanno già partendo le prime mail sull'epidemia delle basi wireless AirPort Express. MacBidouille ha invitato gli utenti di tutto il mondo a segnalare una base morta e <a href="http://www.macbidouille.com/news/2006-06-16/#12952" target="_blank">ne ha raccolti</a> ben&#8230; 225.

Secondo loro, la AirPort Express tipica vive da 13 a 18 mesi, con una vita media di 15,35 mesi. Come facciano ad affermare queste cose non si capisce. Tra l'altro AirPort Express è arrivata a metà del 2004 ed è ampiamente probabile che una buonissima parte delle unità vendute sia stata comprata da meno di tredici mesi, il che impedisce di misurare la loro vita media, dal momento che stanno funzionando.

Sul sito appare un grafico che parla di difetti di produzione e di componenti difettosi, con statistiche risalenti al 2004. Ora, mettiamo che sia vero. Se è vero, tutte quelle basi sono già morte e di conseguenza non c'è motivo di allarmarsi!

Sarebbe più interessante sapere quante AirPort Express sono state vendute in totale, per capire quanto è significativo il campione. Sarebbe interessante sapere se i componenti accusati sono ancora utilizzati o se si stanno utilizzando partite differenti di materiali.

Praticamente, tutte le cose interessanti non si sanno. Da quelle non interessanti, non si capisce come si possano tirare conclusioni. Si capisce invece benissimo come nascerà il passaparola. Miracoli dell'inconscio. Ti si è fusa la base? Non è colpa tua, che dovresti guardarti allo specchio. Non è colpa del tuo negoziante, dal quale devi tornare. È colpa di Apple, una entità sufficientemente concreta perché accusarla dia soddisfazione e abbastanza astratta per non avere sensi di colpa.

Se qualche psicanalista vuole scrivere un saggio sui comportamenti collettivi rispetto ai guasti dello hardware, sono disponibile per la prefazione.