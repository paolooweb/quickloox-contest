---
title: "Risposte (e nuove domande) da AppleScript"
date: 2008-11-14
draft: false
tags: ["ping"]
---

Il nanoquiz di settimana scorsa era facile ma non troppo, proprio come pensare da programmatore.

Come promesso, ecco la risposta di <strong>MacMomo</strong> alla domanda lasciata in sospeso. È presente nel commento al messaggio:

<code>set questaLista to {1, {3}, 4, "Cinque", 6, {"7", 8, 9}}
-- per ottenere il numero degli elementi
set numeroElementi to count of questaLista
-- per sostituire l'elemento n. 2
set item 2 of questaLista to "2"
-- o eventualmente per inserire il nuovo elemento alla posizione n. 2
set questaLista to item 1 of questaLista &#38; "2" &#38; items 2 thru -1 of questaLista
return questaLista</code>

La risposta all'altra domanda (<em>quanti sono gli elementi dentro la lista?</em>) è stata ugualmente corretta; gli elementi sono sei. Il sesto e ultimo elemento della lista è infatti un'altra lista. Come cartelle e sottocartelle; una cartella che contiene più sottocartelle è, appunto, una cartella sola. L'elemento {3} è un indizio alla risposta, in quanto lista, seppure composta da un elemento solo. Anche una lista vuota avrebbe contato come un elemento.

Termino il giro con un esercizietto abbastanza semplice: scrivere il proprio nome di battesimo prima creando una lista che ha come elementi le singole lettere che lo compongono e poi facendo scrivere il nome completo da AppleScript in una finestra di dialogo. Per esempio, creare una lista contenente &#8220;U&#8221;, &#8220;g&#8221; e &#8220;o&#8221; per poi fare scrivere in una finestra di dialogo il nome Ugo.

Quanto sopra dovrebbe bastare più che a sufficienza per risolvere il primo problema e basta compiere una ricerchina - anche in questo blog - per risolvere il secondo. Inoltre qualcuno commenterà sicuramente con la risposta.

Se ti chiami Pierfrancesco oppure Maria Antonietta la difficoltà è uguale, ci vuole solo un po' più di tempo. :-)