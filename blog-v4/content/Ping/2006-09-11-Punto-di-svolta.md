---
title: "Punto di svolta"
date: 2006-09-11
draft: false
tags: ["ping"]
---

Un mio amico sta trattando su eBay un Lisa funzionante per la bella cifra di 1.600 dollari.

Forse è arrivato il punto di svolta in cui i computer vecchi non costano più di quelli nuovi - è sempre accaduto - ma valgono di più.

Oppure è il computer a essere speciale. Vedere un Lisa acceso, effettivamente, fa pensare.