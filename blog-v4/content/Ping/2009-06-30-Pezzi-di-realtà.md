---
title: "Pezzi di realtà"
date: 2009-06-30
draft: false
tags: ["ping"]
---

Anche oggi una voce tradotta dalla pagina delle novità di Snow Leopard, che Apple tradurrà in ritardo sul necessario.

<b>Supporto di Digital Asset Exchange</b>

Il file Digital Asset Exchange (.dae) di Collada sono un modo diffuso di condividere tra applicazioni modelli e scenari tridimensionali. Ora Anteprima mostra questi file mediante grafica OpenGl, permettendo di zoomare e ruotare scenari 3D e riprodurre animazioni. Si puà anche stampare lo scenario o registrarlo come immagine o filmato. E si può visionarlo anche in Quick Look, Visualizzazione rapida.

Un'Anteprima ancora più realistica, insomma. E anche da qualsiasi punto di vista.