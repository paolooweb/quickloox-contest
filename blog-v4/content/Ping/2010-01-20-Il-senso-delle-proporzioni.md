---
title: "Il senso delle proporzioni"
date: 2010-01-20
draft: false
tags: ["ping"]
---

Una azienda mi ha appena detto che non le serve una applicazione iPhone e iPod touch, perché ritiene di raggiungere più che adeguatamente la propria clientela.

Nel suo piccolo, la Casa Bianca &#8211; sebbene ogni sospiro del Presidente venga già rimbalzato in tempo reale per il globo &#8211; <a href="http://itunes.apple.com/it/app/the-white-house/id350190807?mt=8" target="_blank">pensa che sia utile</a>.

Parlando di Casa Bianca, viene da pensare ai nostri politici. Agli altri; quando esisterà una applicazione iPhone per sapere che cosa fanno e dicono, quelli attuali si saranno già estinti da molto.