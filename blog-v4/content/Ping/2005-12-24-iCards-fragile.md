---
title: "Cartoline dall&rsquo;inferno<p>"
date: 2005-12-24
draft: false
tags: ["ping"]
---

Più che altro dal purgatorio, quello di iCards<p>

Il servizio di cartoline virtuali di iCards, parte dell&rsquo;abbonamento a .Mac, è molto carino ed elegante. Ma mostra la corda non appena lo si cerca di sfruttare oltre la dimensione strettamente casalinga.<p>

Non ne sono sicurissimo, ma con un indirizzario di qualche centinaio di nomi la sincronizzazione tra Mac locale e disco virtuale non è perfetta. L&rsquo;interfaccia è spartana e va anche bene, così scoraggia lo spam, ma che fatica mandare qualche decina di copie di una stessa cartolina! A volte la Web application si blocca con un errore criptico e non sai se la cartolina è partita davvero. A volte lo stesso invio non genera lo stesso errore, a volte sì.<p>

Risultato: a qualcuno ho inviato, senza volerlo, gli stessi auguri per otto volte. Rispedivo nell&rsquo;incertezza, senza poter sapere che nonostante l&rsquo;errore il messsaggio era partito. Qualcun altro, per quanto certamente nella mia rubrica indirizzi, mi sembra non presente nell&rsquo;Address Book di iDisk nonostante la sincronizzazione. Così a qualcuno non saranno neanche arrivati i miei auguri.<p>

Bello, iCards. Anche un po&rsquo; fragile, però.<p>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>