---
title: "Caricarsi scaricando"
date: 2009-04-12
draft: false
tags: ["ping"]
---

Per partecipare all'estrazione dei premi per festeggiare <a href="http://www.macworld.it/blogs/ping/?p=2264" target="_self">la miliardesima applicazione scaricata da App Store</a> contano anche gli scaricamenti delle applicazioni, fino a venticinque al giorno, gratuite o a pagamento.

Scaricare venticinque applicazioni al giorno non è certo il sistema per aumentare in modo significativo le proprie <em>chance</em> di vincita. Però forse può esserlo per rendersi conto di che cosa significa App Store.

Ho voluto provare. Aperto iTunes, ho girato per tutte le categorie, con le applicazioni visualizzate in ordine di uscita, e ho scaricato venticinque applicazioni gratuite.

Ci vogliono pochi minuti. Anche solo guardando da iTunes, si raggiunge una consapevolezza superiore. E alcune di quelle applicazioni, scaricate anche da categorie che abitualmente ignoro, andrò veramente a guardarmele.