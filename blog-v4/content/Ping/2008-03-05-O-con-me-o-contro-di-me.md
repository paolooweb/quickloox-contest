---
title: "O con me o contro di me"
date: 2008-03-05
draft: false
tags: ["ping"]
---

<a href="http://web.mac.com/brethil/Brethils_Page/Thoughts/Thoughts.html" target="_blank">Brethil</a> sta dandosi da fare per risolvere un curioso problema che le persone con indirizzi @mac.com hanno su Msn Messenger.

Una persona con indirizzo @mac.com può iniziare una conversazione con Messenger e inviare un messaggio a qualcuno. Ma nessuno può iniziare una conversazione con Messenger mandando un messaggio a qualcuno con un indirizzo @mac.com.

Di riscontro in riscontro la cosa è finita su <a href="http://macenstein.com/default/archives/1188" target="_blank">Macenstein</a> e sui <a href="http://discussions.apple.com/thread.jspa?threadID=1414803&amp;tstart=0" target="_blank">forum di supporto Apple</a>.

Si risolverà&#8230; fino alla prossima volta. Già a settembre scorso era successo che ci fossero difficoltà analoghe.

Di trucco in trucco, il messaggio è sempre quello: se proprio ti ostini a usare strumenti Microsoft, e non ti leghi mani e piedi a Microsoft, Microsoft te le farà pagare.