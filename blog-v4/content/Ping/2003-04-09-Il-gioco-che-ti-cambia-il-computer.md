---
title: "Il gioco che ti cambia (la macchina)"
date: 2003-04-09
draft: false
tags: ["ping"]
---

Tutti abbiamo una killer application

Sono onesto. Quando scelgo un Mac la velocità non è il primo dei requisiti. Deve essere un portatile, pesare poco, avere il massimo della Ram installabile e così via.

Se qualcosa può farmi cambiare la scala delle priorità, questo è un bel gioco. Da tempo sto per l’appunto attendendo la versione Mac di <link>Neverwinter Nights</link>http://www.neverwinternights.com.

Il mio Titanium attuale è adeguato al 100% per tutto il resto della mia vita digitale, ma ha una scheda video che, a naso, non sarà sufficiente per fare funzionare il gioco. Ed è probabile che, quando uscirà, farò i salti mortali se si tratterà di rimediare a questa incresciosa situazione.

Dici che non si cambia il computer solo per giocare? Hai ragione. In generale non si cambia il computer se non per ragioni imprescindibili. Una volta la gente comprava l’Apple II solo ed esattamente per usare Visicalc e quei tempi sono passati.

Ma, da appassionato di giochi di ruolo, posso solo dirti che tutte le regole hanno la loro eccezione. :)

<link>Lucio Bragagnolo</link>lux@mac.com