---
title: "La musica non cambia"
date: 2008-04-09
draft: false
tags: ["ping"]
---

Giusto come promemoria, Bill Gates ha iniziato a parlare di Windows Seven, la prossima versione di Windows. È appena uscito Vista, ma è un <em>flop</em> pazzesco e bisogna farlo dimenticare alla svelta.

Gates accenna al fatto che potrebbe uscire già nel 2009. Microsoft <a href="http://www.macworld.it/showPage.php?template=notizie&amp;id=14654">lo corregge</a> e dice 2010. Intanto, a una convention di commerciali Microsoft, vengono date slide che dicono 2011. Ammesso che non ci siano problemi e che la qualità sia adeguata, cosa che non è detta.

Quandunque uscisse, Windows Seven uscirà in versioni distinte da 32 bit e 64 bit. Mac OS X è già in edizione unica dall'anno scorso e Tiger era già parzialmente pronto per i 64 bit.

Mac OS X è dieci anni avanti. Sui 64 bit sembrano cinque, ma bisogna vedere per quanto tempo durerà Windows Seven&#8230;