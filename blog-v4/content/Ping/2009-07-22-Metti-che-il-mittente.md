---
title: "Metti che il mittente"
date: 2009-07-22
draft: false
tags: ["ping"]
---

Quasi finita la parte iChat della <a href="http://www.apple.com/it/macosx/refinements/enhancements-refinements.html" target="_blank">pagina Apple</a> che Apple non traduce; ancora altre due novità di Leopard e poi si cambia. Intanto però iChat non è finito.

<b>Nuovo menu Da/A</b>

Da un comodo menu dentro la finestra di chat è possibile scegliere l'identificativo con cui inviare i nostri messaggi, o quello del destinatario.

Metti mai che uno voglia usare lo pseudonimo.