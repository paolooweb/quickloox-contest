---
title: "Meglio vendere che spegnere"
date: 2009-03-26
draft: false
tags: ["ping"]
---

Tripod, di cui parlavo <a href="http://www.macworld.it/blogs/ping/?p=2148" target="_blank">a proposito di crisi e compitini</a>, è riuscita a sopravvivere, meglio, a vendersi a un altro operatore.

Felice per tutte le persone che potranno continuare a lavorarci sopra e per quelle che hanno una alternativa in più al momento di costruire il proprio sito, le argomentazioni a margine restano valide. Se il nuovo Tripod vuole sopravvivere e magari pure crescere, a parte gli scontati tagli di costi e recuperi di efficienza, dovrà mettere in modo la creatività.

Quanti di noi si sono interrogati sul loro compitino?