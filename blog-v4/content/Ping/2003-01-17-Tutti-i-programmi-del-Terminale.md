---
title: "Tutti i programmi del Terminale"
date: 2003-01-17
draft: false
tags: ["ping"]
---

Come avere subito e facilmente l’elenco di tutti i binari eseguibili a disposizione della command line

Apri il Terminale e digita Ctrl-x, e poi tab. Oppure Ctrl-x e poi Ctrl-d.

Facile, no?

<link>Lucio Bragagnolo</link>lux@mac.com