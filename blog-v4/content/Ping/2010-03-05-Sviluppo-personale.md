---
title: "Sviluppo personale"
date: 2010-03-05
draft: false
tags: ["ping"]
---

Una delle decisioni migliori di Apple in questo periodo è sicuramente avere abbattuto a 99 dollari annui il prezzo dell'ingresso nel <a href="http://developer.apple.com/programs/mac/" target="_blank">programma sviluppatori Mac</a>, parificandolo con quello dell'analogo programma sviluppatori iPhone.

Se non fosse Ping, partirei con una lunga filippica sulla ricchezza unica costituita da un buon parco sviluppatori, non per il numero dei programmi che si generano (i famosi 160 mila di App Store), ma per la qualità dei programmi migliori che emergono da una base sviluppatori ampia (i dieci programmi imperdibili che ognuno di noi ha su iPhone e che rendono iPhone superiore alle alternative).

Xcode resta gratuito e continua a stare in tutti i Mac. Iniziare a programmare su Mac è e rimane gratis.

Avere assistenza e risorse importanti per fare il prossimo passo, invece, oggi costa molto meno (prima i dollari erano quattrocento in più).

Esorto infine chiunque ad avvicinarsi alla programmazione di Mac, o di iPhone se crede. Anche stando largamente fuori dalla fascia con l'esigenza di pagare. Il mondo va avanti e, per esempio, sapere qualcosina di montaggio video trent'anni fa era ininfluente. Oggi fa parte della cultura generale.

Sapere qualcosina di programmazione aiuta lo sviluppo armonico di tutte le persone e ha smesso di essere ininfluente da un bel po'. Per paradosso, nonostante il proliferare di strumenti per fare cose sempre più difficili sempre più in fretta e sempre più rapidamente, avere un'infarinatura di programmazione diventerà sempre più importante. Il momento in cui diventerà cultura generale non è cos&#236; lontano.