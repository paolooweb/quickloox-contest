---
title: "Video iOS quant'è bello"
date: 2010-12-16
draft: false
tags: ["ping"]
---

Stando a Encoding.com, gente che di questo vive e ha dati su materiale di prima mano, il 78 percento del video che viene codificato per l'uso su Internet <a href="http://www.razorianfly.com/2010/12/16/78-of-websites-now-encode-video-in-formats-playable-by-ios-video/" target="_blank">è compatibile iOS</a>.

La percentuale per Android è il 4 percento.

YouTube e Vimeo, per non fare nomi, sono compatibili iOS.

iPad, iPhone e iPod touch davvero non si stanno perdendo poi cos&#236; tanto.