---
title: "Musica per le nostre pagine"
date: 2010-11-27
draft: false
tags: ["ping"]
---

Chi blogga, scrive, pubblica di musica e tiene un sito, una rubrica online, vuole mostrare qualcosa agli amici lontani, è già incocciato per caso in <a href="http://www.vexflow.com/" target="_blank">VexFlow</a>?

Se no, dovrebbe farlo. Mi sembra una delle migliori dimostrazioni della tesi per la quale, volendo mostrare informazione complessa dentro il <i>browser</i>, sia più elegante e corretto lavorare nel codice Html e non invece con un <i>plugin</i> che consuma energia, è poco efficiente e anche meno stabile.