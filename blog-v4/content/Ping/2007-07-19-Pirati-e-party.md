---
title: "Pirati e party"
date: 2007-07-19
draft: false
tags: ["ping"]
---

Ho passato una serata in discoteca, alla presentazione del singolo Italy di <a href="http://www.rikicellini.it/" target="_blank">Riki Cellini</a>. Mi avevano chiesto di tenere un breve discorso sullo stato della musica online e l'ho fatto.

Ho spiegato che gli editori discografici stanno affrontando Internet come hanno fatto gli editori cartacei: senza capirne niente. E ne stanno per affrontare le conseguenze, proprio come gli editori cartacei.

Non c'è alternativa al vendere la musica digitalmente e consentire certo di acquistare album, ma anche di scegliere esattamente le tracce che si vogliono.

Che poi chi ha comprato buona musica voglia condividerla con gli amici, l'amato bene, suonarla durante una festa è cosa ovvia e inevitabile. Negli anni Settanta c'è chi si è svenato a comprare audiocassette su cui riprodurre i vinili. E nessuno abbaiava alla &#8220;pirateria&#8221; come oggi.

Se poi decollano siti come <a href="http://www.seeqpod.com/" target="_blank">SeeqPod</a>, per chi insiste a spendere soldi in repressione invece che in buon marketing e buona musica è proprio finita. Chi vuole stringere forte il pugno vedrà la sabbia scivolare da ogni interstizio.

A margine: l'unico computer presente era un Titanium. E il popolo della discoteca non dice <cite>compro su Internet</cite>, bens&#236; <cite>scarico da iTunes</cite>. Che è la stessa cosa, ma la diversità di espressione spiega bene l'intuizione geniale di Apple.