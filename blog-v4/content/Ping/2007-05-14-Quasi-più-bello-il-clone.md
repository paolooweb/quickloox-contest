---
title: "Quasi più bello il clone"
date: 2007-05-14
draft: false
tags: ["ping"]
---

Il primo argomento su cui devo tornare in occasione della festa per <a href="http://www.allaboutapple.com/speciali/lustro.htm" target="_blank">il primo lustro del museo Apple</a> di Quiliano è Paolo di Leo.

Paolo si è procurato da un <a href="http://www.achatz.nl/catalog/" target="_blank">sito olandese</a> un kit di montaggio che consente di costruire una copia funzionalmente equivalente all'Apple I originale, quello venduto a 666 dollari e assemblato tra il famoso garage e una stanza di casa Jobs.

Se l'è montato, se l'è programmato, ha scritto un manuale apposta e ha costruito un case di legno il più possibile somigliante a quello dell'Apple I.

Poi si è sciroppato otto ore di auto da Termoli per arrivare a Quiliano. E ha donato la macchina al museo.

I componenti per costruire quel kit vanno esaurendosi. Se ho capito bene, non è già più possibile costruire altri cloni uguali.

Vuol dire che anche <a href="http://home.tele2.it/appleone/AppleOne.html" target="_blank">quello di Paolo</a> resterà nella storia. E se lo merita tutto. Complimenti.