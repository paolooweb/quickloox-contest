---
title: "Specchietti per le allodole"
date: 2007-12-16
draft: false
tags: ["ping"]
---

Specchietto uno: qualcuno ricorderà la corsa all'oro per accaparrarsi la magica Adsl a venti megabit di Alice. Fu inutile, conti alla mano, mostrare che trattavasi di truffa oppure di riempire un disco rigido a settimana (e di che cosa poi?). Avere l'Adsl a venti mega era uno <em>status symbol</em>, ce l'ho quindi conto.

Oggi la pubblicità di Alice magnifica il passaggio da due a sette megabit e spiega che con i sette megabit <cite>si fa tutto</cite>. In altre parole, anche loro finalmente hanno fatto i conti. Vorrei trovare uno di quelli che hanno difeso a spada tratta i loro trendissimi venti megabit, per capire che cosa ci fanno in più.

Specchietto due: ci sono quelli che snobbano l'iPhone perché non è (ancora) 3G, ma fa Edge. Ora, che 3G consenta velocità teoricamente superiori è evidente. Ma iPhone non è fatto per scaricare tonnellate di gigabyte dalla rete via P2P o Ftp, bens&#236; per consultare pagine web. Per come funziona Http, il protocollo di comunicazione del web, una buona connessione Edge offre (sul web) risultati del tutto paragonabili a 3G e alla fine tutta questa differenza pratica, parlando di web, non c'è. Del resto in Apple non sono esattamente deficienti e non avrebbero fatto uscire un arnese con dentro Safari se poi questo non è capace di navigare decentemente.

Qualcuno ha fatto una prova, mettendo <a href="http://www.applephoneinfo.de/2007/11/edge-gegen-umts.html" target="_blank">fianco a fianco un iPhone e un 3G</a>. Il filmato è in tedesco, ma basta guardare per capire. E dimmi, a fine caricamento, quale delle due pagine è più usabile da subito.

Ma vai a spiegarlo a chi si è preso un 3G. Non c'è verso. Esattamente come quelli dell'Adsl a venti megabit.