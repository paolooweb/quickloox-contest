---
title: "Arrivano i nomi per i widget<p>"
date: 2005-05-06
draft: false
tags: ["ping"]
---

Rivincita dell&rsquo;italiano o puro divertimento? Forse ambedue<p>

Arrivano le prime risposte alla sfida lanciata l&rsquo;altroieri: come li chiamiamo, in italiano, quei cosi che grazie a Dashboard di Tiger galleggiano sull&rsquo;Aqua di Mac OS X sotto il nome di widget?<p>

Giuseppe Saltini propone magheggi. Non male!<p>

L&rsquo;amico Luca Pedrone propone una intera batteria di alternative: pippolini, aiutilli, servilli, spie, spioncini, oblò, finestrelle&hellip; ma il suo preferito è (<cite>al maschile, mi raccomando!</cite>) sveltini.<p>

Chi ha nuove idee è benvenuto! Nel frattempo, ci sono già almeno novanta cosi pronti sul <a href="http://www.apple.com/downloads/macosx/dashboard/">sito Apple</a>.<p>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>