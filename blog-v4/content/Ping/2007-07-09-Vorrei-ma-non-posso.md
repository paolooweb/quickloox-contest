---
title: "Vorrei ma non posso"
date: 2007-07-09
draft: false
tags: ["ping"]
---

Uno dei giochi che mi sarebbe piaciuto assai includere nei venerd&#236; del gioco è <a href="http://www.freecol.org/" target="_blank">FreeCol</a>, un clone open source di <a href="http://www.colonization.biz/" target="_blank">Colonization</a>.

Il gioco è eccezionale, peccato che non sia ancora cos&#236; rifinito. È giocabilissimo, ma i menu hanno qualche problema di visualizzazione, per esempio.

Peccato, perché è davvero bello. Ho problemi di ufficialità nel proporlo, ma non stare ad aspettare me. Facci un giro, merita.