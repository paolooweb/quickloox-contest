---
title: "La verità dopo dieci anni"
date: 2010-02-22
draft: false
tags: ["ping"]
---

Abbiamo tutti un debito bello grosso verso Rob Griffiths, che ha aperto <a href="http://www.macosxhints.com/article.php?story=20100219173105746" target="_blank">Mac OS X Hints</a> dieci anni fa e ora lascia (per ragioni professionali; va a lavorare in <a href="http://www.manytricks.com/" target="_blank">Many Tricks</a>).

Ha pubblicato 13.250 trucchi per Mac, oltre tre milioni di parole dice, da novembre 2000 a oggi. E ci ha fatto capire quanto sia vero che abbiamo sotto le mani macchine, hardware e software, fuori dal comune.