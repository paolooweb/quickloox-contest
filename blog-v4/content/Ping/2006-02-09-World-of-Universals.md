---
title: "World of Universals"
date: 2006-02-09
draft: false
tags: ["ping"]
---

Ieri notte ho scaricato la patch di <a href="http://www.wow-europe.com" target="_blank">World of Warcraft</a> che rende ufficialmente il gioco Universal.

Non sembra una gran notizia, ma posso fare il nome di almeno due persone che stavano aspettando solo ed esattamente questo per ordinare un MacBook Pro.