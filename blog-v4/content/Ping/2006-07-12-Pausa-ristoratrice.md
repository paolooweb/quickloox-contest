---
title: "Pausa ristoratrice"
date: 2006-07-12
draft: false
tags: ["ping"]
---

Non c'entra niente con il Mac, ma voglio segnalare <a href="http://www.duespaghi.it/" target="_blank">DueSpaghi</a> per genuina invidia. È un progetto che ho in mente da anni e che ho sempre trascurato&#8230; fino a quando qualcuno lo ha fatto veramente. Ha ragione <a href="http://blog.guykawasaki.com" target="_blank">Guy Kawasaki</a>, quando dice che la vera differenza la fa l'<em>execution</em>, ossia il fare.