---
title: "Tradurre la versione (di AppleScript)"
date: 2008-12-05
draft: false
tags: ["ping"]
---

Questo simpatico script (il cui originale si ritrova <a href="http://forum.soft32.com/mac/Applescript-version-problem-newbie-ftopict39802.html" target="_blank">in un vecchio forum</a>) annuncia il numero di versione di AppleScript che stiamo usando.

Come sempre lo scopo non è sapere il numero di versione di AppleScript, ma scoprire cose nuove.

Per esempio, nello script si usano due modi diversi di indicare i commenti. Non cambia niente, ma uno può almeno scegliere quello che preferisce esteticamente. Prima domanda: esiste un terzo modo? E un quarto?

Secondo, il meccanismo di informazione all'utente è quantomeno macchinoso. Il perché è semplice: l'autore non riesce a trattare come si dovrebbe il dato sulla versione di AppleScript che ha a a disposizione. Tanto che, se si elimina il ciclo <code>if</code> / <code>end if</code> e si lascia solo il secondo <code>display dialog</code>, viene fuori un messaggio curioso, e sbagliato.

La parte tosta di oggi è proprio sistemare lo script in modo che funzioni meglio da questo punto di vista. Ecco lo script.

<code>set ASversion to version as string -- initializza (prepara per l'uso) la stringa ASversion 
set ASversion to (characters 1 thru 3 of ASversion as string) as real (* trasforma ASversion in un numero reale *)
if ASversion is greater than or equal to 2.0 then (* controlla se il numero è 2.0 *)
	display dialog "Ottimo, come minimo stai usando AppleScript 2.0." (* informa l'utente *)
else
	display dialog "Stai usando AppleScript " &#38; ASversion &#38; "."
end if</code>