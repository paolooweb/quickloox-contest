---
title: "Schizofrenia da account"
date: 2009-07-22
draft: false
tags: ["ping"]
---

La voglia di tradurre una novità di Snow Leopard per volta dal sito Apple, visto che loro non lo vogliono fare, è sempre viva.

<b>Status differenti per ciascun account</b>

Durante un collegamento in iChat con più account simultaneamente, è possibile avere un messaggio di stato diverso per ciascuno di essi.

Non è ancora possibile avere più messaggi per un solo account, tipo quelli dell'amico segreto. :-)