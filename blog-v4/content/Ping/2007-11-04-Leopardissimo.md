---
title: "Leopardissimo"
date: 2007-11-04
draft: false
tags: ["ping"]
---

<a href="http://morrick.wordpress.com" target="_blank">Riccardo</a> ha già spiegato del baco della <em>È</em> su Mailsmith e Tex-Edit Plus. Ho già detto del ridisegno della barra di scorrimento con Vienna ed è tutto. Ho provato anche PcGen, GraphicConverter, BibDesk, Cyberduck.

Spaces è affascinante e man mano sto arrivando alla configurazione per me ottimale. Con il mouse è un <em>bijoux</em>, con la tastiera è un cavallo da domare. Ma ci sto arrivando. Un consiglio: ragionare per righe. Se si cancellano colonne, Spaces cambia la numerazione degli spazi e non ci si raccapezza più. Quindi, pensare a un proprio standard ragionevole di numero di colonne e poi aggiungere o togliere solo righe, in modo che la numerazione degli spazi non cambi mai. Per me tre colonne si stanno rivelando l'ideale, ma - specie su schermi molto grossi - anche quattro o cinque possono essere il numero giusto. Dipende anche da come si lavora. Per esempio, definisco gli spazi in funzione di quello che <em>non</em> mi serve nello spazio principale. Qualcuno potrebbe partire dal ragionamento opposto. C'è chi va per applicazioni. Dipende.

I miglioramenti alla memoria virtuale sono spaziali. Per varie ragioni temporanee ho poco più di un giga libero su disco. Non c'è problema. Con Tiger sarebbe stata la morte.

La funzione di consolidamento delle chat in un'unica finestra di iChat è una mano santa. Le scorciatoie di tastiera per passare da una chat all'altra sono però indecenti sulle nostre tastiere. Le ho cambiate tramite Preferenze di Sistema e adesso sono a mio agio.

Una bizzarria: F8 mostra gli Spaces. F9 riassume nello schermo tutte le finestre di Exposé. Dando F9 seguito da F8 si possono raggiungere nuovi livelli di follia visiva. :-)