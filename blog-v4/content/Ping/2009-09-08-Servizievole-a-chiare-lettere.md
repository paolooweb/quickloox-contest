---
title: "Servizievole a chiare lettere"
date: 2009-09-08
draft: false
tags: ["ping"]
---

Un altro esempio di <a href="http://seansperte.com/entry/creating_a_make_title_case_service_in_snow_leopard/" target="_blank">come creare in pochi minuti un Servizio su Snow Leopard</a>, sempre con Automator e questa volta partendo da uno script in Perl, già pronto e disponibile.

Stavolta Apple l'ha fatta giusta.