---
title: "Questione di ore"
date: 2009-02-23
draft: false
tags: ["ping"]
---

Sempre per informazione generale a beneficio di quanti stessero valutando l'acquisto, è appena arrivata la telefonata del corriere che preannuncia l'arrivo del MacBook Pro 17&#8221; per domani.

È un giorno in anticipo sulla scadenza ultima annunciatami da Apple. Contemporaneamente su Apple Store il tempo di consegna, prima tre-quattro settimane, è passato a sette-dieci giorni lavorativi.

Stanno arrivando davvero.