---
title: "Narciso il mio Mac"
date: 2003-07-24
draft: false
tags: ["ping"]
---

Pausa di riflessione per genuflettersi davanti a Mac OS X

Mi fermo un attimo per rendere doveroso omaggio al mio vecchio piccolo Titanium 500.

Monta la prerelease di Mac OS 10.3 che non è esattamente stabilissima. In questo momento ci sono ventisei applicazioni attive nel Dock, con un uptime di oltre sei giorni, che sarebbe assai più lungo se non avessi installato un update.

La sua scheda video è obsoleta e non è all’altezza degli ultimissimi giochi, ma ho potuto ugualmente provare e valutare la beta di Everquest per Mac e il Tech Trial Demo dello splendido Neverwinter Nights.

Ora sto installando PostgreSql e altro software open source che mi serve per un progetto. Tutto software che una volta sarebbe stato inaccessibile.

C’è moltissimo acora da fare e altrettanto ancora da migliorare, ma in questo momento tengo sulla ginocchia una macchina vecchia, stanca e sovraccarica eppure la trovo straordinaria.

Grazie Apple.

<link>Lucio Bragagnolo</link>lux@mac.com
