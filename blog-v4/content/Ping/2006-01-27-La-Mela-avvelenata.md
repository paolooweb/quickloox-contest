---
title: "La Mela avvelenata"
date: 2006-01-27
draft: false
tags: ["ping"]
---

Non &egrave; una nemesi preziosa, che dopo avere predetto o dedotto o sparato per anni che Disney avrebbe comprato Apple (a caso dall&rsquo;archivio, <a href="http://www.drudgereport.com/mnd.htm" target="_blank">Drudge Report nel 2000</a>, <a href="http://www.macobserver.com/article/2003/08/15.4.shtml" target="_blank">MacObserver</a> e <a href="http://www.forbes.com/2003/12/15/sp04_markets_4.html" target="_blank">Forbes</a> nel 2003), adesso l&rsquo;amministratore delegato di Apple sia il maggiore azionista di Disney?

Come minimo bisogner&agrave; cambiare l&rsquo;oroscopo da <em>Disney compra Apple</em> a <em>Apple si vende a Disney</em>. Sar&agrave; Biancaneve ad adescare la strega con un frutto goloso&hellip;