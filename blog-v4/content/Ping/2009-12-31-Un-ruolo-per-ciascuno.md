---
title: "Un ruolo per ciascuno"
date: 2009-12-31
draft: false
tags: ["ping"]
---

Ho scoperto l'esistenza di <a href="http://www.auteria.com/" target="_blank">Auteria</a>, gioco di ruolo di massa su Internet gratuito, in versione <i>beta</i>.

Si vede bene perché è gratuito: i personaggi sono semplici, il terreno spoglio (ancorché completo e realistico quanto basta). L'altra faccia della medaglia: i comandi sono essenziali, iniziare a giocare è semplice, la banda non è affatto un problema.

World of Warcraft è proprio un altro mondo. Tuttavia l'approccio è interessante, ci sono già contenuti utilizzabili e non c'è niente di eccessivamente complicato a intimidire.

Consigliatissimo per una prima esperienza, per giocare con tutta la famiglia, per passare un'ora a spiegare a un amico scettico di che cosa si tratta, perché può solo crescere.