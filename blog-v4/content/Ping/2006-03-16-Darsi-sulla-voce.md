---
title: "Darsi sulla voce"
date: 2006-03-16
draft: false
tags: ["ping"]
---

Da una chat assai interessante con <a href="http://www.tolkieniana.net/" target="_blank">Edoardo</a> a cui devo mille grazie per lo spunto:

<cite>Chi consulta una sequenza di dodici file audio diversi pieni di incertezze, pause, sgrammaticature? E come si va a cercare una cosa importante che &egrave; stata detta un mese fa? Boh. Mi sembra un allargamento delle possibilit&agrave; di chi parla. Gli altri hanno un onere di ascoltare ben pi&ugrave; pesante di quello di leggere e tutti i benefici dell'archiviazione se ne vanno in fumo.</cite>

Questo il mio parere provvisorio ed estemporaneo sui podcast.

Mi sono ritrovato a registrarne (uno fantastico, in particolare, grazie a <a href="http://www.leledainesi.com" target="_blank">Lele</a>, che mi ha fatto divertire un bel po&rsquo;), ma non li vedo come una sostituzione dei blog o dei siti, ma come altro medium, che serve per comunicare altre cose in altro modo rispetto alla parola scritta.

Ammetto che l&rsquo;idea di registrare un podcast su base regolare mi intriga, per&ograve;. Ci penso su.