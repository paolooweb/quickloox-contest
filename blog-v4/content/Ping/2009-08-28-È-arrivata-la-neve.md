---
title: "È arrivata la neve"
date: 2009-08-28
draft: false
tags: ["ping"]
---

Ho installato Snow Leopard. Mi ha segnalato un paio di font secondo lui in conflitto con il sistema operativo e li ho cestinati. Non sapeva quale rete wireless scegliere tra le due disponibili ed è bastato dirglielo. Per il resto, tutto di nuovo funzionante e a pieno regime. La reattività del sistema è decisamente maggiore.

Ho deciso di partire a 64 bit, senza Rosetta e senza QuickTime 7, per vedere se e quando arriva il bisogno di ripiegare sui 32 bit e sulle tecnologie precedenti. Di Rosetta, subito; Mailsmith 2.1.5 ha porzioni di codice PowerPc. Si è palesato Aggiornamento Software e ha proposto di installarlo da Internet. Una delle novità del leopardo delle nevi è proprio che certi componenti presenti sul disco di sistema si possono installare in un secondo momento anche senza un disco di sistema.

Di solito nel nuovo sistema operativo trovo una tecnologia cambiavita (per esempio Quick Look in Leopard, Spotlight in Tiger). Ho la sensazione che stavolta sia l'intero sistema a contare. Tutto sembra più o meno come prima e invece è più veloce, è più pulito, più affidabile, intelligente, ottimizzato.

Benvenuto a bordo, leopardo delle nevi.