---
title: "Già mi manca"
date: 2009-06-15
draft: false
tags: ["ping"]
---

In questi giorni faccio spesso avanti e indietro tra Leopard e Snow Leopard per ragioni di produzione. L'<i>uptime</i> ne risente molto, ma devo dire che quando torno a Leopard sento già la mancanza di due o tre cose presenti nel nuovo sistema operativo. Buon segno.