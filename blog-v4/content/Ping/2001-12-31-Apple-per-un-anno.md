---
title: "Apple per un anno"
date: 2001-12-31
draft: false
tags: ["ping"]
---

A conti fatti il 2001 di Apple è stato positivo, soprattutto se ci porterà a un 2002 migliore

Oggi mi va di salutare il 2001 appena trascorso con un’occhiata generale a ciò che Apple ha saputo (e non saputo) fare durante quest’anno.
Poco da dire sui desktop: complessivamente il 2001 lo ricorderemo per il SuperDrive.
Pochissimo da dire su iMac, che ha proprio bisogno di un redesign.
Ottimi i portatili. Titanium si è rimesso da certi problemucci iniziali e adesso guadagna consensi dovunque; iBook è forse il miglior prodotto marcato Apple dopo l’avvento di iMac. E c’è Airport.
iPod è una bella idea, di nicchia, ma perfetta per la nicchia. Ho sentito molte Cassandre in merito e hanno torto.
E il software? Mac OS X. La potenza di Unix c’è tutta; la semplicità di Mac  sta arrivando però manca ancora qualcosina. Mi piace avere Unix sotto il sedile, ma il cruscotto deve essere quello di un Mac. È possibile e ho fiducia. Forza Apple.

lux@mac.com