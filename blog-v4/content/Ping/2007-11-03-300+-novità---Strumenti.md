---
title: "300+ novità: strumenti"
date: 2007-11-03
draft: false
tags: ["ping"]
---

La <a href="http://www.macworld.com/news/2007/11/02/eveonline/index.php" target="_blank">pagina sul sito Apple</a> non spiega molto bene che si tratta di strumenti per sviluppatori e programmatori. Però sono novità di Leopard lo stesso.

Xcode ora dispone di schemi di Instruments (strumenti) che svolgono compiti specifici di analisi del codice, per renderlo più stabile ed efficiente. Gli schemi possono essere personalizzati, registrati e riutilizzati.

Lo sviluppatore può registrare eventi di interfaccia utente e poi sottoporre la registrazione al proprio codice. In pratica può eseguire un collaudo automatico di quanto accadrebbe se qualcuno usasse mouse e tastiera per parlare all'applicazione.

Apple ha preso una tecnologia open source di Sun, DTrace, e l'ha adattata a Mac OS X. DTrace è un linguaggio di programmazione che permette di approntare molto velocemente e facilmente sistemi di monitoraggio e debugging da inserire all'interno del codice in via di sviluppo.

I risultati delle analisi possono essere visualizzati in forma grafica e immediata, per capire quanto un programma consuma memoria, processore e spazio su disco, o banda su rete.

Tutto ciò è di poca utilità diretta per noi comuni mortali oggi, ma nel medio termine porterà a software migliori.