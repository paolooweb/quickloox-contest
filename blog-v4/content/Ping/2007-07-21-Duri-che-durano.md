---
title: "Duri che durano"
date: 2007-07-21
draft: false
tags: ["ping"]
---

Partendo dal <em>post</em> di ieri, mi sono posto la domanda e direi che il programma che uso da sempre, e che ho sempre continuato a usare, e continuerò a usare per molto altro tempo, penso, sia <a href="http://www.lemkesoft.de" target="_blank">GraphicConverter</a>. Semplice, efficace. Il tuo qual è?