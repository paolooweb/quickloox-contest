---
title: "Porte aperte e colossei"
date: 2002-03-29
draft: false
tags: ["ping"]
---

Chi ne sa di più vede più lontano. E ha capito che...

La mia amica <link>Marina</link>aniram4367@mac.com, da anni praticante i sistemi open source e in particolare Linux, ha da poco acquistato un iBook, felice di poter finalmente usare insieme Unix e l’interfaccia Mac. Ecco che cosa mi ha scritto giorni fa. Non faccio commenti; non ne servono.

“Hai mai curiosato dentro il file /etc/inetd.conf (anzi: /private/etc/inetd.conf)?
Se lo fai, scopri  che tutte le righe relative ai  vari servizi sono commentate (sono precedute da un #)! Di solito, invece, su altri sistemi c’e' sempre qualche servizio disponibile.
Windows Xp invece ha un sacco di porte aperte... sto cercando di capire come fare a intrufolarmi (fingendo di non sapere che ha delle cartelle condivise ed evitando i banali comandi .net) nel computer del mio collega ‘Microsoft Man’ ;-)
Diciamo che Mac OS X è paragonabile a un appartamento con una porta blindata; Windows... beh, oserei dire al Colosseo... :-D”

<link>Lucio Bragagnolo</link>lux@mac.com