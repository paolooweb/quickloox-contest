---
title: "Andiamoci piano con Dashboard<p>"
date: 2005-06-02
draft: false
tags: ["ping"]
---

Solo un piccolo trucco scenografico. Ma suggerisce qualcosa<p>

Mi segnala Sergio che, a lanciare o chiudere Dashboard con il tasto Maiuscolo premuto, l&rsquo;azione avviene al rallentatore, con effetto certamente scenografico destinato ad aumentare i complessi di inferiorità latenti di quei poveracci che aspettano il 2007 per avere le schermate di errore rosse invece che blu.<p>

Sono solo effetti speciali, ma è la stessa cosa che succede quando si manda una finestra nel Dock con il tasto Maiuscolo.<p>

Piccola cosa, ma la dice lunga sul livello di integrazione e di efficienza interno a Mac OS X.<p>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>