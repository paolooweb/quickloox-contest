---
title: "Giudizi opachi, mente lucida"
date: 2008-10-22
draft: false
tags: ["ping"]
---

Io sono per lo schermo <em>matte</em>, quello opaco.

Spero di poterne avere uno per quando esce - spero che esca - il mio 17&#8221;.

Detto ciò, noto un paradosso interessante nella discussione.

I professionisti sono i più rumorosi nel contestare gli schermi <em>glossy</em>. Eppure, <a href="http://arstechnica.com/reviews/hardware/2008-macbookpro-review.ars/3" target="_blank">scrive un esperto del settore su Ars Technica</a>, <cite>in un ambiente dall'illuminazione controllata in modo appropriato (senza il quale non si inizia neanche a parlare di lavoro professionale, a partità di specifiche tecniche, con schermi ben calibrati, uno schermo Lcd</cite> <em>glossy</em> <cite>è sempre superiore a uno schermo opaco.</cite>

I non professionisti sono quelli che ho sentito più spesso dire che alla fine lo schermo lucido si usa tranquillamente. Del resto, credo che gli schermi <em>glossy</em> sopravanzino in vendite gli schermi opachi tipo cinquanta a uno, quindi c'è poco da discutere. Eppure un altro esperto, sempre alla stessa pagina, afferma che <cite>si può avere uno schermo lucido con neri profondi, colore accurato e</cite> <em>gamut</em> <cite>allargato, per cui il problema reale è il riflesso. È una cosa che distrae e per uno come me, che lavora facendo fotoritocco ovunque e anche seduto al bar, diventerebbe fastidiosa. Apple dovrebbe vendere i MacBook con un drappo nero in dotazione</cite>.

Inizio a non capire se abbiano tutti torto o se abbiano tutti ragione, ma per i motivi sbagliati.