---
title: "L&rsquo;orologio rotto<p>"
date: 2005-02-09
draft: false
tags: ["ping"]
---

Tra Lewis Carroll e Mdj una vecchia battuta acquista attualità<p>

Purtroppo non è mia, neanche di terza mano, ma non so trattenermi.<p>

È salita alla ribalta una vulnerabilità dei browser moderni, il cui il supporto dei set di caratteri internazionali (con accentate, cediglie, pallini, barre, trattini e via glifando) potrebbe permettere a un malintenzionato molto capace e molto ingegnoso di creare finti siti per attirare sprovveduti ansiosi di alleggerire la carta di credito.<p>

Tutti browser basati sui motori di rendering Gecko (per esempio FireFox) e Khtml (per esempio Safari) sono per ora vulnerabili. L&rsquo;unico browser immune è Internet Explorer, perché non si è mai sognato di aderire ai moderni standard di supporto delle lingue di tutto il mondo e quindi, nella sua obsolescenza, vive tranquillo.<p>

Come dire: perfino un orologio rotto segna comunque l&rsquo;ora giusta due volte al giorno.<p>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>