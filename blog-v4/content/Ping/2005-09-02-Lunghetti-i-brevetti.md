---
title: "Lunghetti i brevetti<p>"
date: 2005-09-02
draft: false
tags: ["ping"]
---

Perché non basta ripetere le notizie come i pappagallini<p>

Ti ricordi di un&rsquo;epoca lontana, in cui i siti di sedicente informazione sbraitavano di una guerra tra Apple e Microsoft per il brevetto di iPod (nientemeno)? E Microsoft aveva battuto Apple? E il brevetto Apple che era stato respinto perché c&rsquo;era già quello Microsoft<p>

Adesso si legge di un brevetto Creative che metterebbe nei guai Apple.<p>

Ma perché nessuno ne parlava prima? Non era una guerra tra Apple e Microsoft? Se Microsoft è arrivata prima di Apple, perché il brevetto di Creative mette nei guai Apple, e non invece Microsoft?<p>

Forse che quelle scritte prima erano sciocchezze? E allora, perché quelle scritte adesso dovrebbero essere più sensate?<p>

C&rsquo;è troppa gente in giro che scrive ripetendo cose dette da altri, senza capirle. Sbagliare per sbagliare, meglio farlo in modo originale.<p>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>