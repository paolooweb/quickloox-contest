---
title: "L'oroscopo finale"
date: 2007-02-22
draft: false
tags: ["ping"]
---

La mia polemica contro i siti che sparano pronostici sulle uscite dei prossimi Mac deriva dal fatto che si definiscono siti di informazione e invece (s)vendono intrattenimento di basso valore. Non avrei nessun problema se dicessero chiaramente ci divertiamo e basta, ma devo anche accettare che il mondo è cos&#236;: se si va sulla <em>home</em> dei grandi quotidiani tradizionali, per esempio, il sesso e lo <em>splatter</em> si sprecano. Morale: per un clic in più non si guarda in faccia a nessuno neanche al Corriere o a Repubblica, figuriamoci la serie C2.

Questa lunga premessa per dire che, comunque, abbiamo finalmente toccato il fondo con <a href="http://mactactic.com/" target="_blank">Mactactic</a> il quale, facendo calcoli sulle date di uscita dei modelli precedenti, si lancia in consigli di acquisto o rimando dello stesso.

Alla fine Mactactic è una pagina dedicata al principio <em>se compri un Mac appena esce, prima che esca il modello successivo passerà il tempo massimo possibile</em>. Dovrebbe essere senso comune, più che un sito, ma farà grande successo, considerata la quantità di gente che ha smesso di pensare.

Certo, rispetto ad altri siti, dove invece che sparare una percentuale ci si perde in chiacchiere, Mactactic fa comunque la figura dell'oroscopino da due righe, a confronto degli almanacchi di Branko.