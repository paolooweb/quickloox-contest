---
title: "Sono contento di essere arrivato uno<p>"
date: 2005-10-08
draft: false
tags: ["ping"]
---

Non è (sempre) vero che il software libero rimane provvisorio a vita<p>

Giuro che da oggi, qui, non ne parlerò mai più. Ma devo rimarcare che Battle for Wesnoth è arrivato alla versione 1.0.<p>

Per il software open source non è una cosa garantita. Tanti programmi rimangono a vita in uno stato usabile ma provvisorio.<p>

<a href="http://www.wesnoth.org">Battle for Wesnoth</a> è un gioco facile da imparare e con una vita da dedicarci per padroneggiarlo, gratis, con funzioni di gioco in rete e via Internet, un editor di scenari per creare mondi propri e manuale in italiano.<p>

Il gioco oltretutto ha una leggerezza e una raffinatezza grafica tale che va bene per tutti, grandi e piccoli. Anzi, come strumento di logica paragonabile a una partita a scacchi su terreno misto, lo vedrei molto bene specialmente nelle mani di ragazzi delle medie inferiori. Non richiede computer particolarmente potenti e può essere giocato anche per pochi minuti, per poi registrare la situazione. Insomma, è veramente azzeccato.<p>

Ripeto, non lo citerò mai più. Ma se non lo scarichi e non lo provi una volta perdi veramente un&rsquo;occasione.<p>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>