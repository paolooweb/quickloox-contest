---
title: "Io lo so ma non lo dico"
date: 2010-07-23
draft: false
tags: ["ping"]
---

Sconcertante situazione, in cui iPhone 4 &#8211; stando alla chiacchiera selvaggia su Internet &#8211; ha un problema che i suoi concorrenti non hanno, ma i concorrenti in questione non si sognano neanche di affondare il coltello nella piaga.

Il giornalista Farhad Manjoo di Slate <a href="http://www.slate.com/id/2261251/pagenum/all/" target="_blank">ha cercato di intervistare</a> i produttori di smartphone che hanno reagito con energia alle affermazioni di Apple dicendo che loro no, fanno meglio, non tirateci in ballo, i nostri telefoni funzionano bene.

<cite>Ho chiesto per email marted&#236; a Rim (BlackBerry) i dati. [&#8230;] Mi hanno confermato la ricezione della mail, ma a gioved&#236; mattina non mi hanno ancora risposto.</cite>

<cite>Ho anche chiamato Motorola, per chiedere il dato delle chiamate perse dai suoi telefoni. [&#8230;] L'ufficio relazioni con i media non ha dato seguito alle mie richieste.</cite>

<cite>Ho anche contattato Htc [&#8230;] e il suo portavoce mi ha dichiarato che non rendono pubblici questi dati.</cite>

Che strano, potrebbero affondare la corazzata iPhone 4 con un comunicato stampa e se ne guardano bene.