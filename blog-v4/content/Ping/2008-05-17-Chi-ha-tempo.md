---
title: "Chi ha tempo"
date: 2008-05-17
draft: false
tags: ["ping"]
---

Parlavamo di orologi, tempo fa. Mi sono imbattuto in <a href="http://computergenerateddreams.com/diskclock/index.html" target="_blank">Disk Clock</a>, un widget veramente bizzarro dove il fatto che a ruotare sia l'orologio, invece delle lancette, è proprio la cosa più innocente. A girare sono anche le stagioni, le fasi lunari, perfino gli anni. Ci sono diverse funzioni seminascoste e insomma ora che ti sei divertito a configurare l'orologio di tempo ne è passato fin troppo.

La baldoria intellettuale continua sulla <a href="http://computergenerateddreams.com/diskclock/unusualtime.html" target="_blank">Gallery of Unusual Time</a>. Orologi a dieci ore, frattali, Unix, L'ovvio Internet Time di Swatch, l'orologio esadecimale, quello diviso in gradi, quello binario, quello Maya e ancora.

Ci sono ricadute Mac, in termini di <em>widget</em> o programmini, quasi per tutto. Chi ha tempo può perderne (e intrigarsi) un sacco.