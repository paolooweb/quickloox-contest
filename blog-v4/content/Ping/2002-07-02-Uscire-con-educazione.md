---
title: "Uscire con educazione"
date: 2002-07-02
draft: false
tags: ["ping"]
---

Anche un programma rude come il Terminale ha il proprio galateo

Quando uscì Macintosh (era il 1984 e potrei essere suo padre, almeno per l’anagrafe) un ingegnere buontempone uscì sulla prestigiosa Byte con un articolo titolato “Real men don’t use icons” riferendosi alla supposta superiorità delle interfacce testuali su quella grafica.
Oggi Byte non se lo fila più nessuno ma le shell ci sono ancora e, su Mac, l’ambiente per “real men” (e women) è il Terminale.
Che è per duri, ma ha il suo codice di buona condotta. E una delle regole più disattese è questa: non uscire dal terminale senza prima avere dato il comando “exit” e avere ricevuto come risposta [process complete] oppure addirittura la chiusura del programma senza bisogno di Mela-Q.
Si può ignorare la regola novecentonovantanovemilanovecentonovantanove volte su un milione, se non vivi nel Terminale. Ma io preferisco non rischiare neanche un po’.

<link>Lucio Bragagnolo</link>lux@mac.com