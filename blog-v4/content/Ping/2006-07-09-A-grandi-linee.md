---
title: "A grandi linee"
date: 2006-07-09
draft: false
tags: ["ping"]
---

Sto disegnando un paio di sciocchezze in vettoriale e ho una licenza di Adobe Illustrator, ma mi sono messo in testa di non usarlo. Il mio obiettivo richiede l'uso di circa l'uno per cento delle possibilità di Illustrator e non sono un professionista, dunque voglio valutare la possibilità di abbandonare per sempre il software a pagamento a favore dell'open source.

<a href="http://www.inkscape.org" target="_blank">Inkscape</a> mi sta piacendo molto più di <a href="http://www.cenon.info" target="_blank">Cenon</a>. Va detto che però il secondo ha lo svantaggio di una interfaccia meno familiare.

Inkscape registra in formato Svg e Cenon no. Quest'ultimo però esiste solamente per Mac OS X, OpenStep (!) e Linux. Ideale da tenere sul disco per spiegare al windowsista di turno che su Windows non c'è mica tutto&#8230;

Saprò dire.