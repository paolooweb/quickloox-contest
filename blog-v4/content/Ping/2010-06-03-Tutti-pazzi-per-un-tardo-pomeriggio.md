---
title: "Tutti pazzi per un tardo pomeriggio"
date: 2010-06-03
draft: false
tags: ["ping"]
---

Effettivamente, a chi può venire in mente di intrupparsi dentro un negozio di Mac due giorni prima che inizino i Mondiali per chiacchierare di iPad e dintorni con il pretesto di un libro?

Bisogna essere pazzi. Appunto.

Mercoled&#236; 9 giugno a partire dalle 18:30 in @Work Milano, via Carducci angolo galleria Borella (MM1 Cadorna, MM2 S. Ambrogio, bus 50-58-94, tram 14-16-18 se ricordo bene), presento il mio ultimo libriccino Tutti pazzi per iPad.

Sarà mio complice Francesco Pignatelli, vicedirettore e pontefice massimo di Macworld Italia.

È assai probabile che a fine dei giochi si finisca in pizzeria, chi ne ha voglia; in via Terraggio 20, a pochi minuti a piedi, c'è la collaudatissima Bio Solaire. Non fa parte del programma ufficiale, però.