---
title: "Risalire dalle fonti"
date: 2010-01-05
draft: false
tags: ["ping"]
---

La nuova rivoluzione del web mi sta piacendo. Finalmente torna la tipografia degna di questo nome, finalmente avere un <i>browser</i> fuori standard è una responsabilità invece che un ricatto per fare giocare tutti al ribasso.

Una testimonianza efficace ed evidente a tutti da subito: i font <i>open source</i> della <a href="http://www.theleagueofmoveabletype.com/" target="_blank">League of Moveable Type</a>. Insiemi di caratteri di ottima fattura, studiati guardando al web, gratuiti e soprattutto <i><code>@font-face</code> ready</i>.

Che cosa significhi l'ultima espressione lo possiamo ignorare; a patto di non creare siti. Nel qual caso diventa responsabilità saperlo, perché l'ignoranza è un diritto. Ma il diritto di affliggere gli altri con il brutto non esiste; è una finzione architettata dagli ignoranti più scaltri e niente più.