---
title: "Il battimani della discordia"
date: 2009-09-07
draft: false
tags: ["ping"]
---

Lo <i>staff</i> di Apple Store Carosello sta per aprire il punto vendita e saluta il pubblico in attesa.

Se sembra numeroso, non sono tutti. Ne manca almeno il venti percento.