---
title: "Mac Around the Clock"
date: 2007-12-11
draft: false
tags: ["ping"]
---

<strong>Filippo</strong> ne ha scovata un'altra delle sue: una intrigante raccolta di <a href="http://vladstudio.com/wallpaperclock/browse.php" target="_blank">sfondi scrivania con orologio incorporato</a>.

Sono davvero affascinanti e, se la mia scrivania non fosse perennemente ricoperta da uno spesso strato di finestre, ne avrei già fatta ma bassa.

Imperdibili, soprattutto, per il conteggio di Capodanno.

Prima di chiederti come un patatone (cioè come me) se il programma funziona o meno, dai un'occhiata alla barra dei menu. Appare un'iconcina l&#236;, senza nessun altro avviso.