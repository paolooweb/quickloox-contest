---
title: "Conteggio (in)calorie"
date: 2007-08-30
draft: false
tags: ["ping"]
---

Lapidario, ed esaustivo, <strong>Luca</strong>:

Lo stesso documento, testo e tabelle, in .doc pesa <em>cinque volte tanto</em> rispetto a un .pages. La mia esperienza non può essere assunta a statistica, ma è sufficiente. A farmi incazzare.