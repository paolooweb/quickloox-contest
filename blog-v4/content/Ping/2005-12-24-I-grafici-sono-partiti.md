---
title: "I grafici sono partiti<p>"
date: 2005-12-24
draft: false
tags: ["ping"]
---

Che sia software rubato o meno, c&rsquo;è un dato interessante<p>

Le cose più intelligenti le ha scritte, come accade molto spesso, Paolo Attivissimo nel suo <a href="http://attivissimo.blogspot.com/2005/12/forza-italia-usa-software-pirata.html">blog</a>. Forza Italia, Rifondazione Comunista e altri partiti useranno veramente software piratato per produrre i loro manifesti?<p>

La questione sembra proprio di lana caprina, se non altro perché esistono il beneficio del dubbio, l&rsquo;impossibilità della prova inversa (come fare ad appurare che un software apparentemente in regola lo sia veramente?), almeno una spiegazione attendibile e già vista in pratica (vecchie copie di Xpress con chiave hardware Adb che non possono girare sui nuovi Mac con la sola Usb, a meno di non usare qualche trucco software). Ha ragione Paolo: chi sente la questione faccia una denuncia alla Bsa. Tutto il resto, lo dico io, è solo carenza di pretesti migliori per fare polemica politica.<p>

La cosa che mi interessa di più è l&rsquo;evidenza del fatto che molti partiti italiani, direttamente o indirettamente, si affidano a un Mac. Sarebbe meglio se non lo pagassero con i soldi degli elettori in spregio a un referendum popolare, ma non si può avere tutto.<p>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>