---
title: "MoM non è WoW"
date: 2007-05-20
draft: false
tags: ["ping"]
---

La seduta ludica del venerd&#236; con Minions of Mirth è stata, dal mio punto di vista, un successo. Il gioco è molto più che dignitoso, pur praticato nella versione <em>gratuita</em>.

Hanno partecipato <strong>Eugenio</strong> e <strong>Lucia</strong>. Abbiamo avuto un po' di problemucci tecnici e di setup, ma siamo riusciti a ritrovarci nel gioco e fare giustizia di scheletri, vedove nere e orsi grigi che affollano la zona di ingresso nel mondo.

Siamo morti più volte in modi assolutamente creativi, andando incoscientemente incontro a creature muscolose senza avere idea di come condurre un attacco o, come il sottoscritto, indietreggiando davanti a un orso più manesco degli altri fino a cadere da un dirupo di altezza superiore a quanto avrei preferito.

Pur senza fare grandi cose (passare un'ora scarsa in un gioco di ruolo online è poco più che prendere atto che esiste) mi sono divertito, ho avuto buonissima compagnia e penso anche di fare qualche altro giretto in <a href="http://prairiegames.com" target="_blank">Minions of Mirth</a>. <a href="http://www.wow-europe.com" target="_blank">World of Warcraft</a>, WoW, resta un'altra cosa. MoM, tuttavia, è qualcosa.