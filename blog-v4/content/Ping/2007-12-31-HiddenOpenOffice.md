---
title: "HiddenOpenOffice"
date: 2007-12-31
draft: false
tags: ["ping"]
---

Il suggerimento di <strong>Italo</strong> si è rivelato davvero positivo e bisogna dirgli un bel grazie.

La versione di OpenOffice Aqua <em>in progress</em> che si scarica dalla <a href="http://ooopackages.good-day.net/pub/OpenOffice.org/MacOSX/" target="_blank">zona sviluppatori del sito</a> (opportunamente poco enfatizzata) è parente molto lontana, in meglio, di quella <a href="http://porting.openoffice.org/mac/download/aqua.html" target="_blank">ufficialmente presentata</a> sul sito stesso.

Quest'ultima è grezza, ha un sacco di problemi grafici, si vede che è indietro di cottura. Quella più recente ha messo a posto un bel po' di cose, inserisce tutta una serie di nuove funzioni basate su Java, e si possono pure aprire i file con il drag and drop. Non ultimo, è anche in italiano, con numero di versione allineato, non indietro di tre passi come con OpenOffice X11.

Ribadisco, a ripetere ciò che diceva Italo, che è software in lavorazione. l'operatività è talora lenta, a volte un comando cade nel vuoto, ci sono ancora ottimizzazioni da fare. Non è un programma cui affidare il proprio lavoro e chi lo vuole fare è caldamente invitato a ripiegare sulla (ottima) versione X11.

Questione di tempo, però. I progressi sono straordinari ed evidenti. OpenOffice Aqua sta arrivando.