---
title: "Foto fuori dal tempo"
date: 2009-02-22
draft: false
tags: ["ping"]
---

Non volevo che il più bel <em>post</em> sul venticinquesimo anniversario del Mac andasse sprecato nella caciara, quindi ho aspettato.

Sono le foto scattate da e per Guy Kawasaki alla <a href="http://blog.guykawasaki.com/2009/01/twenty-five-yea.html" target="_blank">rimpatriata del team originale che ha prodotto il Mac</a>.

Facce vere, storie vere, pochissimo <em>hardware</em> e tanta gente che il mondo accidenti se lo ha cambiato.

Una citazione: <cite>Una volta ho acquistato software per circa un milione di dollari, da distribuire ai commerciali e ai rivenditori per convincerli che s&#236;, c'era software per Mac. La cifra era superiore di appena 995 mila dollari al mio</cite> budget <cite>di spesa&#8230;</cite>