---
title: "Generazioni di fenomeni"
date: 2008-05-21
draft: false
tags: ["ping"]
---

Arrivo in ritardo, ma non posso esimermi dal segnalare che l'<a href="http://www.allaboutapple.com/blog/" target="_blank">All About Apple Museum</a> è comparso anche sul Magazine del Corriere della Sera del 15 maggio.

Grandi quelli che hanno inventato i computer di una volta, ma ancora più grandi quelli che oggi hanno la passione, l'entusiasmo e la (straordinaria) capacità di farli rivivere. <em>Chapeau</em>.

L'occasione è imperdibile per segnalare che il museo sta per rinascere in versione totalmente rinnovata e che il primo atto è domani, sabato 24 maggio, in concomitanza con l'apertura straordinaria dall'alba fino a notte fonda e gratis dei musei di tutta la provincia di Savona.

I dettagli sono sul blog. Purtroppo non riuscirò a esserci per (lieti) impegni familiari, ma mi auguro che in tanti faranno le mie veci e pure meglio. :-)