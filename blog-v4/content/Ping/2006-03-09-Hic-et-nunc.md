---
title: "Hic et nunc"
date: 2006-03-09
draft: false
tags: ["ping"]
---

Sono limitato. Invece di vivere per scoprire quale sar&agrave; il prossimo annuncio di Apple, mi godo il Mac che ho davanti.

La mia soddisfazione non viene dal sapere che cosa potr&ograve; usare domani, ma da ci&ograve; che uso oggi. Qui e ora. Sono malato, vero?