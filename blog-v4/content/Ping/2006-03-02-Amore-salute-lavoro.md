---
title: "Amore, salute, lavoro"
date: 2006-03-02
draft: false
tags: ["ping"]
---

Arrivati a marzo, sembra chiaro che gli iBook Intel <a href="http://www.thinksecret.com/news/0511intelibook.html" target="_blank">annunciati in esclusiva da Think Secret in novembre</a> siano in leggero ritardo.

D&rsquo;altronde le sue fonti erano <em>highly reliable</em>. Branko o Van Wood?