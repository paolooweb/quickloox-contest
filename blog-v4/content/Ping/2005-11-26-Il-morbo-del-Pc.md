---
title: "Il morbo del Pc<p>"
date: 2005-11-26
draft: false
tags: ["ping"]
---

Alla regola del minuto si aggiunge un principio incontestabile<p>

Ogni tanto a uno viene da chiedersi che senso abbia usare Windows. Ci sono centomila programmi invece di trentamila, ma l&rsquo;utente medio ne usa meno dell&rsquo;utente medio Mac. Ci sono centomila virus contro sette o otto e questo si commenta da solo. La verità è che per usare Windows bisogna pensarla in un certo modo. Quale sia, lo descrive benissimo <strong>Giacomo</strong>:<p>

<cite>Tu hai introdotto la regola del minuto. Bene, io vorrei parlarti del morbo del pc.</cite><p>

<cite>La maggior parte degli utilizzatori di computer ne è affetta e anche io, cresciuto con Windows, devo ammettere che, purtroppo, ogni tanto ne soffro. Potremmo sintetizzarlo così:</cite><p>

<cite>Se entri nel mondo dell'informatica con dei software che sono programmati in modo controintuitivo, sarai di necessità portato a scartare a priori la soluzione al problema che si presenta come più ovvia.</cite><p>

<cite>Ti faccio un esempio. Passato da Panther a Tiger mi sono accorto che a causa dell'inizializzazione era scomparso AppleWorks. Bene, la prima cosa che ho fatto è stata connettermi alla rete, indagare nei forum, scaricare utility per vedere file invisibili&hellip; in realtà Appleworks era dove doveva essere; ovvero nei dischi di restore di Panther. Inserito il disco ho scelto di reinstallare solo AppleWorks e tutto ha funzionato come doveva.</cite><p>

<cite>Per chi ha lavorato tanto con il mondo Microsoft è una sensazione tanto spiazzante quanto piacevole; ci si trova a dirsi: ma è ovvio, non poteva che essere lì! Non poteva che essere lì perché lì l'avrebbe messo ogni persona dotata di buon senso!</cite><p>

<cite>Purtroppo chi inizia con Windows non inizia con il buon senso e lungo è il cammino per disintossicarsi.</cite><p>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>