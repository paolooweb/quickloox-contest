---
title: "300+ novità: Internazionale"
date: 2007-11-04
draft: false
tags: ["ping"]
---

Ci riguarda poco, e molto. Il mondo sta diventando globale e sempre più popolazioni si affacciano alla civilizzazione, quindi all'informazione, quindi all'informazione.

Leopard, come da <a href="http://www.apple.com/it/macosx/features/300.html#international" target="_blank">pagina descrittiva</a>, aggiunge alle lingue ufficiali il russo, il polacco e il portoghese iberico (quello portoghese, diverso da quello brasiliano).

I font hanno ricevuto aggiornamenti per il russo, il polacco, il coreano, il persiano, e gli script (i sistemi generali di scrittura, che comprendono la direzione delle parole scritte, per esempio) hanno ampliato la scelta a tre nuovi sistemi di scrittura arabi e derivati, tra cui il curdo. I font giapponesi supportano Hyogaiji, il nuovo standard di caratteri giapponesi definito dall'ente nazionale degli standard nipponico.

Tutta l'interfaccia utente di Leopard rispetta la specifica Jis2004.

Ci sono quindici nuove disposizioni di tastiera, tra cui tibetano, kazako e iraniano.

Sono stati introdotti nuovi sistemi di inserimento di caratteri per lingue cinesi, giapponesi e arabi.

Nonché i controllori ortografici danese e russo.