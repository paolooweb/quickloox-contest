---
title: "Il mosaico di Ravenna"
date: 2010-10-05
draft: false
tags: ["ping"]
---

Ravenna, al secolo Italo, è docente di scuola primaria presso l'Istituto Comprensivo Manara di Borgotaro.

Ha assemblato non tesserine colorate ma bambini di quinta elementare, un pizzico di aiuto esterno e tanta immaginazione.

Il risultato è una fiaba interattiva per iOS, <a href="http://itunes.apple.com/it/app/la-marcia-dei-folletti/id391258663?mt=8" target="_blank">La marcia dei folletti</a>, disponibile gratuitamente anche sugli App Store di lingua inglese come <a href="http://itunes.apple.com/it/app/the-march-of-the-pixies/id391270650?mt=8" target="_blank">The March of the Pixies</a>.

Gli interessati a come è stato realizzato il progetto, oltre che al progetto stesso, hanno a disposizione pagine di <i>making of</i> e <i>backstage</i> per la <a href="http://iphone.manualipc.com/lamarciadeifolletti.php" target="_blank">versione italiana</a> e per la <a href="http://iphone.manualipc.com/themarchofthepixies.php" target="_blank">versione inglese</a>.

Stante che le risorse per i bambini e per le loro scuole dovrebbero essere illimitate, stante che la scuola italiana ha problemi e carenze riconosciute, stante che tanti maestri fanno tanta fatica&#8230; a me pare che i maestri bravi sappiano fare cose straordinarie e che tanti altri siano incapaci di fare anche le cose normali.

A lamentarsi dei tagli o della crisi sono tutti capacissimi; a fare lavorare la testa, propria e dei propri alunni, sono capaci pochi e meritano la menzione d'onore.