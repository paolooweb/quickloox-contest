---
title: "Peccato che sia gratis"
date: 2009-05-03
draft: false
tags: ["ping"]
---

Anche solo novantanove centesimi a <a href="http://infinite-labs.net/" target="_blank">Emanuele</a> li avrei dati più che volentieri. Per <a href="http://itunes.apple.com/WebObjects/MZStore.woa/wa/viewSoftware?id=287322475&amp;mt=8" target="_blank">Diceshaker</a>, ennesimo - e però più acuto della media - lanciadadi per iPhone e iPod touch.

Ma ancora più per <a href="http://itunes.apple.com/WebObjects/MZStore.woa/wa/viewSoftware?id=312165666&amp;mt=8" target="_blank">Mover</a>. Il gusto - e la comodità - di passare una foto o un contatto a un tuo vicino di scrivania, lanciandolo dal tuo iPhone verso il suo, avrebbe più che giustificato la spesa.

Purtroppo l'uno e l'altro programma sono gratis.