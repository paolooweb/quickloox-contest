---
title: "Folla binaria"
date: 2009-09-09
draft: false
tags: ["ping"]
---

Di foto dell'Apple Store Carosello invaso e assediato dalla folla in delirio ce ne sono ovunque. Meglio qualcosa di più tranquillo, che permette anche di farsi un'idea di come è pensato il punto vendita.