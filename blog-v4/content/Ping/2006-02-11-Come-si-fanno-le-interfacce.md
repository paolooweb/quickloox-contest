---
title: "Come si fanno le interfacce"
date: 2006-02-11
draft: false
tags: ["ping"]
---

Il mitico <a href="http://www.accomazzi.net" target="_blank">Misterakko</a> mi segnala (e lo ringrazio di cuore) un <a href="http://www.asktog.com/columns/069ScottAdamsMeltdown.html" target="_blank">articolo eccellente</a> sul sito di Bruce Tognazzini, riguardante un incidente accaduto a Scott Adams (il disegnatore di <a href="http://www.dilbert.com" target="_blank">Dilbert</a>) nel moderare il suo blog con disastrosa perdita dei dati, e considerazioni di design delle interfacce utente.

Per capirci, Tognazzini ha lavorato per lungo tempo in Apple ed &egrave; uno dei massimi esperti mondiali di interfacce.

L&rsquo;articolo &egrave; molto impegnativo, ma ne raccomando in tutti i casi la lettura a chi se la sente. Per capire veramente che differenza c&rsquo;&egrave; tra Mac OS X e Windows, a parte le sensazioni di pelle e l&rsquo;ovvio, occorre un retroterra di questo livello. Mi sa che ci potrebbe persino scappare un articolo in futuro.