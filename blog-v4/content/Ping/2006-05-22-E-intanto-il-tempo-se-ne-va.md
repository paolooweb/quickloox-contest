---
title: "E intanto il tempo se ne va"
date: 2006-05-22
draft: false
tags: ["ping"]
---

L’amico <strong>Stefano</strong> ha indagato sulle faccende dell’uptime e dei tempi di utilizzo macchina, scoprendo <a href="http://www.macswitching.com/2006/05/21/the-last-reboot-how-much-time-has-passed-since-the-last-rebooting-a-hands-on-last-tutorial/" target="_blank">cose mooolto interessanti</a>.