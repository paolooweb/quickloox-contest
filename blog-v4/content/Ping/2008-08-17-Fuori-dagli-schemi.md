---
title: "Fuori dagli schemi"
date: 2008-08-17
draft: false
tags: ["ping"]
---

Nessuno mi tocchi <a href="http://www.barebones.com/products/mailsmith" target="_blank">Mailsmith</a>.

Però, dovessi scegliere oggi un nuovo client di posta da provare, andrei su <a href="https://www.outspring.com" target="_blank">Outspring Mail</a>. Ancora maturo ma promettente, con il coraggio di eliminare l'addestramento manuale antispam.