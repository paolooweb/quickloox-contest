---
title: "Novità in Borsa"
date: 2009-10-07
draft: false
tags: ["ping"]
---

Non so quanti daranno risalto alla notizia (scommetto su <i>nessuno</i>) ed è bene colmare la lacuna: la Borsa di Londra ha iniziato la migrazione a software di gestione <a href="http://www.ibspublishing.com/index.cfm?section=news&amp;action=view&amp;id=13440" target="_blank">basato su Linux e Solaris</a>.

Lo stesso cambiamento interessa <a href="http://www.borsaitaliana.it/homepage/homepage.en.htm" target="_blank">Borsa Italiana</a>, che è proprietà di London Stock Exchange. E anche il norvegese Oslo B&#248;rs.

È una gran bella vittoria dell'<i>open source</i> in termini di efficienza, risparmi di costi e, anche se il tema non viene sollevato per motivi diplomatici, prestazioni.

In Italia, poi, vedere l'<i>open source</i> nell'alta finanza è più raro delle eclissi solari. Speriamo di vederne sempre di più.