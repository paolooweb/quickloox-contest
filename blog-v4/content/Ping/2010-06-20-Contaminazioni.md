---
title: "Contaminazioni"
date: 2010-06-20
draft: false
tags: ["ping"]
---

Rimango sempre irresistibilmente conquistato dalle commistioni tra vecchio e nuovo e come tale mi piace molto l'idea di avere <a href="http://code.google.com/p/googlecl/" target="_blank">i servizi di Google disponibili via Terminale</a>.

L'applicazione unica di domani, insomma, non sarà necessariamente il <i>browser</i>.

Avvertenza: causa difficoltà di connessione, non sono ancora riuscito a installare e collaudare il pacchetto. Potrebbe richiedere familiarità tecnica con il Terminale.