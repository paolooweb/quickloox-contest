---
title: "Il vero digital lifestyle"
date: 2003-01-26
draft: false
tags: ["ping"]
---

Diventare registi non è un sogno impossibile. Neanche avere un Mac

Ogni tanto si legge che, per esempio, Macintosh ha avuto un ruolo importante nella realizzazione di un grande film come Le Due Torri (è vero; visti i titoli di coda?). Ma si tratta di storie che riguardano professionisti di autorevolezza mondiale, che viaggiano un chilometro sopra la testa di chiunque altro.

Parliamo invece di persone normali, con lavori normali, e una grande passione. <link>Skarr</link> http://www.progettoskarr.net è un film vero, creato da persone vere, che non vivono di cinema. Tutto, ovviamente, su Macintosh.

<link>Lucio Bragagnolo</link>lux@mac.com
