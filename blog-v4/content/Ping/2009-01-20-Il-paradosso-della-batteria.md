---
title: "Il paradosso della batteria"
date: 2009-01-20
draft: false
tags: ["ping"]
---

Duecento milioni di esemplari tra iPod e iPhone dovrebbero avere chiarito che il problema della batteria integrata è un non problema.

Piuttosto, uso portatili da quindici anni e passa. Ho parlato con varie centinaia di persone che usano portatili. Ho visto vendere centinaia di portatili. Ho sempre sentito auspicare da tutti che la batteria durasse di più e vivesse più a lungo.

Adesso che Apple ha messo dentro MacBook Pro 17&#8221; una batteria con autonomia doppia e vita tripla rispetto a quello che c'è stato finora, arriva puntuale <a href="http://www.macobserver.com/tmo/article/who_needs_an_8_hour_battery_anyway/" target="_blank">un articolo</a> a sostenere che una batteria da otto ore non serve a nessuno se non a una piccola minoranza. Complimenti.