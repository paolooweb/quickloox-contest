---
title: "Record mondiale"
date: 2008-07-13
draft: false
tags: ["ping"]
---

Voglio ringraziare di gran cuore quanti hanno partecipato alla chiacchierata di venerd&#236; scorso. Mi è piaciuto molto poter raccontare qualcosa su iPhone 3G che fosse almeno non troppo scontato e provare a tratteggiare un ritratto del mondo delle informazioni personali che ci aspetta a breve termine, e in cui Apple potrebbe avere un ruolo importante.

L'intenzione non era vendere iPhone (non ce n'è proprio bisogno) né altro; semplicemente trasmettere idee stimolanti in compagnia di persone simpatiche e interessanti.

Del primo obiettivo, non saprei. Del secondo, le persone intervenute erano tutte simpatiche e/o interessanti e questo è un record mondiale.

Grazie davvero a tutti.