---
title: "Chi ha bisogno di Photoshop?"
date: 2008-05-12
draft: false
tags: ["ping"]
---

Un bel duello, <a href="http://www.pixelmator.com/" target="_blank">Pixelmator</a> contro <a href="http://www.flyingmeat.com/acorn/" target="_blank">Acorn</a>. A comprarli tutti e due si spende un quarto di quello che costa il programma di Adobe.