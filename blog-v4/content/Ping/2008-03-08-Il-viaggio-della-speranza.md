---
title: "Il viaggio della speranza"
date: 2008-03-08
draft: false
tags: ["ping"]
---

In @Work sono circondato da gente con iPhone che passa metà della sua esistenza informatica a occuparsi di <em>unlock</em>, <em>jailbreak</em> e <em>springboard</em>, gli unici della galassia Apple che vivono un aggiornamento di sistema come una crisi ambientale invece che un respiro di sollievo.

Non avevo nessuna intenzione, ma per ragioni professionali mi tocca possedere un iPhone.

Rivolgersi ad Apple (ehilà, sono un giornalista, prestatemene uno per un mese) è inutile: mentre per avere in prestito un iPod touch non ci sono stati problemi (il problema sarà restituirlo, adesso che per un giorno lo ha provato la mia signora), non si può avere un prodotto che ufficialmente non è in commercio.

Cos&#236; ho compiuto il viaggio della speranza (mezz'ora di treno) e adesso sono felice, credo, possessore di un iPhone tutto mio, clandestino e non garantito. Lo scambio, contanti contro merce, è avvenuto in stazione, come vere spie internazionali. Il mio fornitore (grazie ancora, se legge!) è stato una persona intelligente e capace, con interessi e passioni. Dovevamo prendere un caffè e siamo stati per mezz'ora a scambiarci esperienze di vita, di informatica, di mondo Apple. Nel mentre è passato un suo amico, che aveva iPhone in tasca. Altri dieci minuti a parlare di <em>crack</em>, di <em>firmware</em>, di programmi che inizierò a conoscere da domani.

Intanto che chiacchieravamo, con la coda dell'occhio ho visto passare vari auricolari bianchi, a bordo di persone dall'aspetto più vario possibile. Mi sono chiesto quanti iPhone stanno silenziosamente colonizzando il Paese e adesso che faccio parte anch'io della cospirazione ammetto che guardo quegli auricolari in modo diverso.

Sarà interessante. Mi rendo conto che sono passati solo pochi mesi e, se il <em>trend</em> prosegue, nel giro di un anno conoscerò più gente con un iPhone che gente con un Mac (è da <em>molto</em> tempo che accumulo conoscenza di gente con un Mac e i Mac si stanno spargendo a macchia d'olio. E qualunque rivenditore Apple può vendere un Mac alla luce del sole).

Dove voglio arrivare? Da nessuna parte. Mi rendo conto che questa, non so ancora bene in che modo, è una partenza.