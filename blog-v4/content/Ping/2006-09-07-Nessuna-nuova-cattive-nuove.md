---
title: "Nessuna nuova, cattive nuove"
date: 2006-09-07
draft: false
tags: ["ping"]
---

Il sito di Macworld si sta riavviando, ma da come posso osservare le cose dall'esterno ci vorrà un po' di tempo prima che riparta a pieno ritmo.

I problemi sono opportunità, però. Se c'è mai stato un momento in cui fare sapere che tipo di news, e di che tenore, si vogliono vedere su Macworld online, prometto di annotare tutto e passarlo a chi di dovere. :)