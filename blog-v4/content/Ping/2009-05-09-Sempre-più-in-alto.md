---
title: "Sempre più in alto"
date: 2009-05-09
draft: false
tags: ["ping"]
---

Tornando al tema delle <em>botnet</em> (reti di computer infetti controllate remotamente da criminali informatici) si segnala una novità: il <em>trojan</em> Zeus ha una funzione <em>kill operating system</em>, che non c'è bisogno di tradurre. È già stata usata per <a href="http://voices.washingtonpost.com/securityfix/2009/05/zeustracker_and_the_nuclear_op.html" target="_blank">fare autodistruggere centomila installazioni Windows</a> perlopiù polacche e spagnole.

Il <em>trojan</em> Zeus colpisce solo Windows.

E adesso avanti con le installazioni cieche di Windows negli ospedali, nelle centrali elettriche, sui sommergibili nucleari, nelle reti di monitoraggio. Tanto per scamparla basta contare su personale competente e sollecito, come tutti sappiamo essere la regola ovunque&#8230;

Per conto mio, quando sono in un ufficio pubblico, ho preso l'abitudine di avvisare. Vengo guardato strano, guardato male o semplicemente ignorato. Magari però uno su mille si salva.