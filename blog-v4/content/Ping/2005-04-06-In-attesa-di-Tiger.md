---
title: "L&rsquo;interfaccia di domani<p>"
date: 2005-04-06
draft: false
tags: ["ping"]
---

A spiare Tiger si intravedono direzioni che&hellip;<p>

Viaggiando per le novità di Tiger si prova una sensazione strana. Da una parte si ha la sensazione di tanto lavoro sotto il cofano, che rende il sistema ancora più veloce e affidabile, ma che poco sia cambiato.<p>

Poi clicchi qui, digiti là ed ecco che il paradigma cambia completamente. Viene da pensare a Tiger come a un grande ponte, che anticipa e prelude a cambiamenti che saranno epocali, quasi come quando sono arrivati il mouse e le finestre a cambiare tutto.<p>

Ecco: non mi meraviglierei se 10.5, o forse 10.6, non relegassero il Finder e la metafora della scrivania a un onorato dimenticatoio.<p>

Perché? Beh, penso che entro fine mese verrà annunciato. Tiger. Ciascuno, nello scoprirlo, potrà tirare le sue conclusioni. :-)<p>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>