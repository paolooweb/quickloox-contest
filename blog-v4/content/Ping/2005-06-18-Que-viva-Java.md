---
title: "Que viva Java<p>"
date: 2005-06-18
draft: false
tags: ["ping"]
---

Programmi che passeranno a processori Intel senza alcun problema<p>

Alle buone notizie sulla transizione dei programmi Mac esistenti in modo che possano funzionare senza problemi sui processori Intel quando usciranno, si aggiunge una notizia che in realtà non lo è. I programmi scritti in linguaggio Java, notoriamente fatto per essere indipendente dalla piattaforma, gireranno senza bisogno di modifiche e senza alcun problema.<p>

Di software Java per Mac ce n&rsquo;è in giro una discreta quantità e per tutti questi programmi la transizione sarà immediata e indolore.<p>

Giusto per non dimenticare niente per strada!<p>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>