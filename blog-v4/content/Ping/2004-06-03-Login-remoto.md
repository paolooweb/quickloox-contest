---
title: "Login remoto, riavvio scongiurato"
date: 2004-06-03
draft: false
tags: ["ping"]
---

Come aumentare ancora di più l’uptime di Mac OS X

Ne avevo letto più volte, ma non avevo mai capito come fare.
Oggi ci sono riuscito. Il Titanium, a schermo spento ma teoricamente attivo, non dava alcun segno di voler rispondere ai comandi.

Dall’iMac presente sulla rete di casa ho aperto il Terminale e ho digitato

ssh lucio@10.0.1.2

lucio è il nome utente; 10.0.1.2 è l’indirizzo del Titanium sulla rete locale.

Sono entrato nel portatile, ho ucciso da Terminale un paio di processi che si erano bloccati e il Titanium si è risvegliato come per incanto, senza bisogno di riavviare, come avrei altrimenti fatto.

Ingredienti: Login Remoto abilitato (pannello Condivisione nelle Preferenze del Sistema) e sapere l’indirizzo di rete del Mac.

Già prima si riavviava di rado. Ora il mio uptime rischia di crescere ulteriormente, e così la mia soddisfazione per Mac OS X.

<link>Lucio Bragagnolo</link>lux@mac.com