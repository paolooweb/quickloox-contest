---
title: "Disonestamente AppleScript"
date: 2008-12-13
draft: false
tags: ["ping"]
---

Come tutti gli strumenti di grande potenza, AppleScript può essere utilizzato per scopi poco etici.

Un <a href="http://www.maclife.com/article/howtos/use_applescript_get_ahead_webbased_games" target="_blank">articolo di MacLife</a> fornisce un esempio efficace di come si possa barare in modo semplice su certi giochi web proprio grazie a uno script fatto a puntino.

Non mi metterò a dissezionare lo script: già lo fa l'articolo. Dirò solo che mettere il tutto in pratica aumenterà non poco la conoscenza rispetto a certi meccanismi del web e naturalmente alle possibilità di AppleScript.

Lo scopo ovviamente non è barare su Generic Rpg, ma aggiungere una ennesima freccia al nostro arco di programmatori. Attendo risultati. :-)