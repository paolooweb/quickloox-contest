---
title: "Guai a sottovalutare"
date: 2008-04-07
draft: false
tags: ["ping"]
---

Macworld Usa è tornato sul tema Leopard, con un elenco di <a href="http://www.macworld.com/article/132858/2008/04/25leopardfeatures.html" target="_blank">25 novità sottovalutate</a>.

Sottovalutato vuol dire che già lo sai, ma ci fai poca attenzione o non ne fai l'uso che varrebbe la pena.

Complessivamente l'elenco è azzeccato. Non uso Automator, ma la funzione di registrazione è notevole. E iChat Theater, a dispetto del nome, è molto pratico (non hai idea di quanti problemi remoti si risolvono con una condivisione di schermo).

Gli account ospite e condivisione sono ugualmente utili, cos&#236; come la condivisione del lettore ottico.

Dei tab spostabili nel Terminale o in Safari sapevo già, cos&#236; come dei Data Detector (c'è una data dentro una mail e Mail capisce che è una data).

Certe cose però non le sapevo. Si possono fare scorrere le finestre mentre sono in secondo piano; si possono inserire allegati in iCal; c'è una nuova voce sintetica, Alex.

Di Alex non me ne può fregare meno, ma dello scorrimento delle finestre in secondo piano s&#236;, e un sacco.