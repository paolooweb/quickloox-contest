---
title: "Nessun uomo è un’insulina"
date: 2004-08-02
draft: false
tags: ["ping"]
---

Tanti auguri a una persona che ha fatto molto e vorremmo lo facesse ancora a lungo

Ma a Steve Jobs, reduce di un’operazione di asportazione di un rarissimo tumore al pancreas, non vogliamo fare tantissimi auguri?

È tra quelli che ha fatto avanzare l’informatica invece che farla arretrare (e non faccio nomi). E certe volte sta antipatico, ma mi è simpatico anche in quelle occasioni.
Forza Steve!

<link>Lucio Bragagnolo</link>lux@mac.com