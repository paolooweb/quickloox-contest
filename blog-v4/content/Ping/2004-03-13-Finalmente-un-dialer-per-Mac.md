---
title: "Un dialer per invidiosi"
date: 2004-03-13
draft: false
tags: ["ping"]
---

Finalmente anche un utente Mac può mandare a catafascio la sua configurazione Internet navigando su un sito porno

Ma perché solo gli utenti Windows devono provare il brivido di vedere alterate le proprie impostazioni di collegamento Internet e scoprire improvvisamente che stanno telefonando chissà dove a chissà che tariffa?

Come segnala Gabriele Polidori, webmaster*onenet.it (leggere @ al posto di *, lui ci tiene come misura antispam), andando incautamente su sessogratis.net e cliccando si riesce a scaricare un file xxx.bin.sit che, se scompattato ed eseguito, tenta – credo – di cambiare i parametri di connessione telefonica a Internet.

Dico “credo” perché il dialer è per Classic e non ho avuto modo di fare una prova seria.

Tuttavia potremo smettere di invidiare l’utente Windows. Lui ha a disposizione migliaia di siti dove scaricare centinaia di programmi dannosi e ostili, è vero. Ma uno lo abbiamo anche noi.

<link>Lucio Bragagnolo</link>lux@mac.com