---
title: "Un dollaro a settimana"
date: 2004-06-09
draft: false
tags: ["ping"]
---

Nella classifica degli amministratori delegati della California...

Il Los Angeles Times ha fatto la classifica dei Ceo più pagati in California nel 2001 e Steve Jobs, amministratore delegato di Apple, ha battuto tutti di oltre una lunghezza.

La cosa curiosa è che tutta la ricchezza di Jobs deriva dal possesso di azioni e da compensazioni di varia natura, come il jet Gulfstream che gli ha regalato Apple.

Se si dovesse guardare allo stipendio soltanto, il mondo Macintosh dovrebbe preoccuparsi, dal momento che in Pixar Jobs è pagato cinquanta volte pià che in Apple.

Anzi, 52: un dollaro a settimana, contro un dollaro l’anno. Avrà problemi per la pensione?

<link>Lucio Bragagnolo</link>lux@mac.com