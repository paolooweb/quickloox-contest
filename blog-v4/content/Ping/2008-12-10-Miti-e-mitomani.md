---
title: "Miti e mitomani"
date: 2008-12-10
draft: false
tags: ["ping"]
---

<cite>Mito: secondo l'intera Internet, subito dopo natale Apple venderà negli ipermercati </cite>low cost Wal-mart <cite>un iPhone da cento dollari per quattro gigabyte. Fatto: Apple venderà certamente iPhone presso Walmart, ma saranno gli stessi iPhone che si trovano altrove, allo stesso prezzo. Perché Apple, che vende con successo iPhone da 200 o 300 dollari, dovrebbe mettersi a venderne uno da cento?</cite>

<cite>Mito: Prima o poi Apple presenterà un Mac</cite> tablet touchscreen <cite>che sarà qualcosa di simile a un Tablet Pc Windows, però bello. Il fatto è che Apple produce già un</cite> tablet Pc<cite>, che si chiama iPod touch.</cite>

<cite>Mito: Apple sta per presentare un</cite> netbook <cite>superleggero e supereconomico. Sono sicuro che un giorno vedremo un piccolo MacBook con una tastiera come si deve e un</cite> trackpad <cite>effettivamente usabile. Ma ci vorrà un po' e probabilmente costerà più della concorrenza.</cite>

Questo è <a href="http://blog.wired.com/gadgets/2008/12/why-apple-wont.html" target="_blank">Charlie Sorrel su Wired</a>. Sfido a trovare una analisi di questo tipo, magari sbagliata ma motivata e precisa, cose di cui uno può prendersi la responsabilità, tra i produttori italiani di notizie-spazzatura che per motivi incomprensibili continuano a irretire un sacco di gente.