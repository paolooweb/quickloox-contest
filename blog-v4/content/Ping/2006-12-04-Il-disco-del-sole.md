---
title: "Il disco del sole"
date: 2006-12-04
draft: false
tags: ["ping"]
---

Mi scrive (e lo ringrazio) <strong>Giuseppe</strong>:

<cite>Qui ad Alghero, anche se è il 2 di Dicembre, la giornata è splendida e si può prendere il sole in compagnia del PowerBook.</cite>

<cite>Dettaglio non insignificante, perché il relax mi ha invogliato a fare un salto dalle parti di America Online, su cui sono registrato, che mi scrive per informarmi di un <a href="http://www.xdrive.com/partners/?p=aolgen&amp;promo=766518&amp;gcid=aisoftware1&amp;hat=true" target="_blank">servizio di online storage da 5GB</a> che ad alcuni dei tuoi lettori forse potrebbe interessare, se non addirittura essere un &#8220;punto di svolta&#8221;. Pare che sia anche gratuito&#8230;</cite>

<cite>Credo che sia meraviglioso fare un'immagine disco cifrata e tenerla su Xdrive. Della serie: non si sa mai&#8230;
</cite>

Un po' lo invidio anche Giuseppe, perché il sole della Sardegna è unico anche a dicembre. Per Xdrive no, non lo invidio, lo ringrazio e basta; perché funziona e mi sono già registrato anch'io. Potrebbe essere forse la soluzione per i miei malanni di trasferimento di grossi file? Non credo che possa battere un sftp diretto. Ma sono comunque 5 gigabyte in più.