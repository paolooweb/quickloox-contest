---
title: "Tutto è bene ciò che finisce Telecom"
date: 2007-04-26
draft: false
tags: ["ping"]
---

FastWeb è ancora di là da venire, ma nel frattempo Tele2 ha rilevato il mio contratto telefonico.

Stringo nelle mani quella che con tutta probabilità è l'ultima bolletta Telecom della mia vita (e non corro pericoli immediati). Mi viene voglia di incorniciarla.

Telecom continuerà a rovinare l'esistenza telematica mia e di tutti gli italiani ma, per quanto mi riguarda, solo indirettamente. Yeah!