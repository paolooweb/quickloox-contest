---
title: "Accessibilità sul campo<p>"
date: 2005-03-13
draft: false
tags: ["ping"]
---

Un esercizio di produttività diventa verifica dell&rsquo;efficienza dei programmatori<p>

Mi ritrovo a usare un PowerBook 17&rdquo; e sto educandomi a usare il più possibile la tastiera, trascurando il trackpad, per due motivi: sullo schermo così largo è meno efficiente e mantenendo le mani sulla tastiera sono più veloce.<p>

Bisogna ovviamente attivare la navigazione completa via tastiera da preferenze di sistema, ma con un pizzico di abitudine è pià facile di quello che pensavo. Deve cambiare qualche automatismo, ma si riesce a fare quasi tutto.<p>

Unici ostacoli per i quali il trackpad è inevitabile: siti fatti male e programmi fatti male. Ci sono siti (per esempio 190.it) dove certi pulsanti sono percorribili con il tasto Tab e altri no. Lo stesso vale per certi programmi, là dove altri invece sono impeccabili.<p>

Puoi prendermi per pazzo, se vuoi, ma garantisco che può valere la pena di minimizzare l&rsquo;uso del mouse o dei suoi equivalenti. E poi ci sono disabili che non possono usare un mouse. Il mio piccolo esperimento è anche un test molto empirico, ma rivelatore, sull&rsquo;accessibilità di software e web.<p>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>