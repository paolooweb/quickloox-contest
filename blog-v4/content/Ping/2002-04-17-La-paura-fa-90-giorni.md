---
title: "La paura fa novanta (giorni)"
date: 2002-04-17
draft: false
tags: ["ping"]
---

Certe abitudini di informazione all’utenza sono quanto meno discutibili... troppo in là

Tre mesi fa alcuni esperti di auto hanno scoperto un problema nelle serrature delle Ford. Ma Ford non ha detto niente a nessuno. Ha annunciato tutto tre mesi oggi, quando è cominciata la consegna di auto più sicure. Ford ha spiegato che, tacendo i difetti fino a quando è pronta la soluzione, i delinquenti ignorano il problema e quindi non possono approfittarne.
Però i ladri che già lo sanno hanno tre mesi di tempo in più per rubare una Ford. E non tutti i proprietari faranno sostituire la serratura, per scarsa comunicazione o pigrizia.
Stai pensando di non comprare mai più Ford? Ripensaci.
L’azienda non è Ford; è Microsoft. Un problema di sicurezza di Office per Mac è stato annunciato tre mesi dopo essere stato scoperto, quando era pronta una patch su Internet.
Stai pensando di non comprare mai più Microsoft?

<link>Lucio Bragagnolo</link>lux@mac.com