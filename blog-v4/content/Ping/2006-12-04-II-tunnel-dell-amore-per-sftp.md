---
title: "Il tunnel dell'amore (per sftp)"
date: 2006-12-04
draft: false
tags: ["ping"]
---

Ritorno sull'argomento della spedizione di grossi file attraverso la rete.

iDisk funziona, ma è lento. Ho provato <a href="http://www.sendthisfile.com/" target="_blank">SendThisFile</a>, con risultati orripilanti. La tremenda cosa non prevede un resume dei file. Ho provato a trasferire un file da mezzo giga ma per due volte ho sottovalutato il tempo necessario, dovendo interrompere la connessione e perdendo lo sforzo fatto. Ci ho riprovato durante un weekend e, arrivato al 100 percento dell'upload, SendThisFile non ha chiuso il file. Sedici ore (di tempo macchina, per fortuna) buttate.

Per il momento vince a mani basse il trasferimento organizzato insieme a <strong>Mario</strong>, via <code>sftp</code>. Pochi comandi banali da terminale e nel giro di sei ore sono passati seicento mega, per giunta in modo sicuro.

Una volta di più: il Terminale non serve all'utente normale. Ma spesso, per fare cose speciali, è una potenza. Lunga vita a sftp.