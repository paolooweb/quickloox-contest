---
title: "Navigazione a svista"
date: 2010-05-30
draft: false
tags: ["ping"]
---

Sono entrato in un qualsiasi negozio Vodafone, ho chiesto una microSim per iPad e dopo una dozzina di firme e compilazioni varie &#8211; viva l'Italia &#8211; me l'hanno data.

Ho infilato la microSim in iPad, ho inserito la stringa <i>web.omnitel.it</i> nel campo <i>Apn</i> e stavo già navigando il web con connessione 3G.

Lontani i tempi in cui si scriveva <cite>E cos'è questa storia della micro-SIM? Quanti operatori cellulare la offrono?,</cite> io me la prendevo per l'assoluta vacuità dell'affermazione e venivo criticato perché l'autore della scempiaggine <i>informava i lettori</i>.

Si pensava veramente che la prima o la seconda azienda di tecnologia al mondo avrebbe presentato un apparecchio che funziona a microSim senza che qualcuno le vendesse?