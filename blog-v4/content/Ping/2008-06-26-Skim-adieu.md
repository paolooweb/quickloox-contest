---
title: "Skim adieu"
date: 2008-06-26
draft: false
tags: ["ping"]
---

Skim (il <a href="http://skim-app.sourceforge.net/" target="_blank">lettore di Pdf</a>, non il <a href="http://skim.sf.net" target="_blank">messenger scritto in Java</a>) è tanto bello, ma non modifica le note presenti nei Pdf che mi mandano e non capisco se è colpa mia o colpa sua. Fino che non capisco dove sbaglio, torno ad Anteprima.