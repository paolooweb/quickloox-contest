---
title: "Semplice è bello"
date: 2008-10-04
draft: false
tags: ["ping"]
---

Era da tempo che non tenevo sul disco rigido programmi P2P. Poi ho acquistato i manuali cartacei di <a href="http://www.wizards.com/dnd" target="_blank">Dungeons and Dragons</a> quarta edizione e, per comodità di consultazione, mi sono posto il problema di cercare in rete versioni Pdf dei manuali stessi.

<a href="http://acqlite.sourceforge.net/" target="_blank">Acqlite</a>, <em>open source</em>, ha risolto il problema brillantemente, con ingombro minimo e interfaccia semplicissima. A fine operazione l'ho anche tenuto sul disco.