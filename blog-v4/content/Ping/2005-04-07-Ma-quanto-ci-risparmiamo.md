---
title: "Ma quanto ci risparmiamo?<p>"
date: 2005-04-07
draft: false
tags: ["ping"]
---

I computer non costano in euro, ma in anni

Mi scrive Elio:<p>

<em>Approposito di &ldquo;quanto ti costi?&rdquo;</em> [pubblicato nella rubrica Preferenze dell&rsquo;ultimo Macworld cartaceo. N.d.L]<em>, adopero dal &rsquo;98 un iMac Bondi Blue Revision A con 160 Mb di Ram, aggiornato alla 10.3.8, dotato di Bluetooth, masterizzatore esterno, HD160Gb esterno. E devo dire che apparte le prestazioni &ldquo;bradipesche&rdquo; mi soddisfa e si ripaga alla grandissima!</em><p>

Quelli come Elio sono tanti. Il mio iMac Bondi Revision A fa ancora un degnissimo lavoro in una galleria d&rsquo;arte nel centro di Milano, tanto per dire. Sono computer letteralmente del secolo scorso ma funzionano ancora a pieno ritmo. Non ricordo più quanto costassero. Oggi posso dire che sono costati pochissimo.<p>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>