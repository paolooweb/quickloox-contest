---
title: "tuncfg ha detto stop"
date: 2008-10-02
draft: false
tags: ["ping"]
---

Grazie di cuore a quanti hanno passato i suggerimenti per risolvere il <a href="http://www.macworld.it/blogs/ping/?p=1979" target="_self">mistero della continua ripartenza di <code>tuncfg</code></a>.

I suggerimenti erano tutti centrati. Ho aggiornato <a href="hamachix.spaceants.net/" target="_blank">HamachiX</a> alla versione più recente e, passando dal menu di Help, ho disinstallato e poi reinstallato i suoi componenti di sistema.

Inoltre ho disattivato il <em>login</em> automatico al <em>rootserver</em> in caso di accensione di Hamachi. Devo ancora vedere se attivare questa funzione provoca il ritorno del fastidio, ma nel frattempo i <em>log</em> di sistema sono tornati all'ordinaria amministrazione.

Sono soddisfatto soprattutto di poter tenere tranquillamente installato HamachiX,  macchinoso e però comodissimo per le reti virtuali.