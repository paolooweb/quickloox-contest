---
title: "AppleScript, sei Unix"
date: 2008-11-25
draft: false
tags: ["ping"]
---

Sappiamo bene che tra Terminale e AppleScript c'è un ponte a due carreggiate.

La prima porta da AppleScript verso il Terminale. Si chiama <code>do script</code>. Uno script come questo:

<code>
tell application "Terminal"
   do script "uptime"
end tell
</code>

esegue il comando <code>uptime</code> nel Terminale.

L'altra carreggiata porta dal Terminale ad AppleScript e porta il nome <code>osascript</code>. Se scriviamo nel Terminale

<code>osascript -e 'tell application "/Applications/iChat.app" to open'</code>

(attenzione alle virgolette singole e doppie, e al parametro <code>-e</code> che è necessario)

possiamo eseguire script dal Terminale.

La prima carreggiata però si può sfruttare un po' meglio. Un esempio:

<code>set stringaComandi to "/bin/ls -AelO" as text
tell application "Terminal" to do script stringaComandi in window 1</code>

In questo modo, per esempio, possiamo memorizzare dentro un AppleScript certi comandi di Terminale utili e difficili da ricordare.

Su MacOSXHints si trova <a href="http://forums.macosxhints.com/showthread.php?t=94071" target="_blank">uno script ancora più lungo</a>, che fa la stessa cosa e sta attenta a che niente possa andare male. È troppo lungo per essere ripetuto qui con profitto e anche estremamente interessante da studiare.