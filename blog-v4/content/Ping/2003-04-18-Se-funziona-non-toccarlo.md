---
title: "Se funziona, non toccarlo"
date: 2003-04-18
draft: false
tags: ["ping"]
---

L’uso male inteso di certe utility porta alla cecità (informatica)

C’era quello che a suon di lucidare la carrozzeria dell’auto ha consumato la vernice.

Io le chiamerei Morton Utilities, loro e i loro succedanei. Sento ogni settimana di qualcuno che “ho fatto girare le Norton e adesso ho più problemi di prima”.

Il bello è che spesso non avevano nessun problema. Semplicemente, in assenza di altre cose migliori da fare, hanno pensato che la carrozzeria non era abbastanza lucida.

Ok. Io vivo senza Norton da anni, sollecito il Mac al massimo di quello che può dare, ma questo è Mac OS X. Un logout-login risolve quasi sempre tutto. Lascio uno o due giga liberi sul disco e una volta l’anno riparo i privilegi. Non ho antivirus e non ho i fastidi degli antivirus, oltre a non avere virus. Funziona tutto. Sono fortunato? Non credo. Sono più bravo degli altri? Neanche un po’.

La mia carrozzeria non è sempre impeccabile, ma la verniciatura è ancora perfetta. Non parlo del case, ovvio; ho un Titanium. :-)

<link>Lucio Bragagnolo</link>lux@mac.com