---
title: "Profitti e perdite"
date: 2010-07-23
draft: false
tags: ["ping"]
---

Termino la trilogia telefonica di oggi con questi estratti da <a href="http://www.electronista.com/articles/10/07/22/ms.confirms.kin.cost.quarter.billion.dollars/" target="_blank">un articolo di Electronista</a>.

<cite>Microsoft ha riconosciuto che i telefoni Kin sono stati a oggi uno dei suoi errori più costosi. La sola cancellazione del progetto è costata 240 milioni di dollari, senza considerare i costi di sviluppo.</cite>

<cite>Il progetto doveva utilizzare i risultati diretti dell'acquisizione di Danger, costata 500 milioni di dollari.</cite>

<cite>Microsoft ha anche orchestrato una elaborata campagna di lancio che comprendeva un evento speciale per la stampa e una ampia campagna di marketing in televisione e su altri media.</cite>

<cite>In totale le vendite sono state 9.705.</cite>

<cite>L'operatore Verizon è stato deluso a sufficienza da restituire l'invenduto dopo appena due settimane, anziché cercare di venderlo in qualche modo.</cite>

Nei commenti si stima che ciascun Kin sia costato a Microsoft da 75 mila a centomila dollari. (<cite>iPhone è il telefono più costoso del mondo.</cite> &#8212; Steve Ballmer, amministratore delegato Microsoft, <a href="http://www.businessinsider.com/flashback-steve-ballmers-first-take-on-the-iphone-september-22-2007-2010-6" target="_blank">gennaio 2007</a>)

Intanto si disquisisce sull'attenuazione del segnale e sulle custodie gratis.

Non basta presentare una cosa per venderla e se gli iPhone 4 non funzionassero (tre milioni venduti finora, trecento iPhone per ciascun Kin) non si venderebbero.