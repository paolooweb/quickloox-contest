---
title: "Come nelle migliori pubblicazioni"
date: 2006-07-31
draft: false
tags: ["ping"]
---

Una volta poteva accadere che la redazione scrivesse, per presentare una lettera, <em>riceviamo e pubblichiamo</em>. E questo faccio.

<cite>Salve, mi chiamo Daniele Margutti e sono un programmatore Mac.</cite>

<cite>Innanzitutto complimenti per il vostro sito. Vi scrivo per farvi partecipi del rilascio della prima versione beta del mio programma, nella speranza che possa interessare gli utenti Mac nostrani.
</cite>

<cite><a href="http://www.malcom-mac.com/projects/nemo/blog/nemo-01-released/" target="_blank">Nemo</a>, questo è il nome, è un programma per seguire i newsgroup sulla rete Usenet. Nemo è stato sviluppato interamente in Cocoa e benché si tratti della prima versione beta riservata agli sviluppatori è già piuttosto completo e ben integrato col sistema. Sono supportate le ricerche veloci come in Mail, le cartelle smart, differenti profili e connessioni - quindi multiserver - e ha integrato un browser webkit per navigare in internet senza muoversi dal programma.
</cite>

<cite>Inoltre è customizzabile nella maniera in cui lo è anche Adium; è possibile infatti personalizzare (a patto di conoscere i Css) sia lo stile in cui vengono visualizzati i messaggi che la mappa dei thread.
</cite>

<cite>Come dicevo, il programma è in beta e questa prima versione è riservata soltanto a coloro che si iscriveranno entro oggi (data in cui lo rilascerò) al <a href="http://www.malcom-mac.com/projects/nemo/blog/beta-testing-program/" target="_blank">programma di testing</a>.
</cite>

<cite>La versione finale, dal costo di 15$, verrà invece distribuita ad ottobre; il programma è ovviamente universal binary e supportato dal 10.4 in poi.
</cite>

Letto bene? C'è scritto <cite>entro oggi</cite>.