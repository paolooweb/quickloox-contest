---
title: "Guardare la vita"
date: 2006-10-16
draft: false
tags: ["ping"]
---

L'altra sera stavo pestando sui tasti come al solito quando è arrivata una chiamata video one-way dall'amico <strong>Francos</strong>. La mia iSight era fuori portata e quindi  il video arrivava solo da lui.

Mi ha salutato e poi mi ha mostrato la sua bimba nata da meno di un mese. Ho sentito il bisogno irresistibile di ingrandire la finestra video a tutto schermo. Lui era felice e sereno, la bimba incantevole.

Mi ha regalato un momento molto bello che mi è rimasto nel cuore.

E c'è gente che vuole la videocamera Usb per risparmiare qualche euro. Forse non ha figli. O non ha amici. O non valgono abbastanza.