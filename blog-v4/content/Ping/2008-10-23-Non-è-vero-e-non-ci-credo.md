---
title: "Non è vero e non ci credo"
date: 2008-10-23
draft: false
tags: ["ping"]
---

John Gruber ha raccolto una piccola <a href="http://daringfireball.net/2008/10/todays_claim_chowder" target="_blank">lista di predizioni</a> relative all'evento di presentazione dei nuovi portatili Apple.

<a href="http://www.inquisitr.com/4834/exclusive-apple-to-launch-800-laptop/" target="_blank">Un portatile da 800 dollari</a>.

<a href="http://www.edibleapple.com/kevin-rose-drops-macbook-blu-ray-rumor-at-live-diggnation-event/" target="_blank">Il supporto di Blu-ray.</a>

MacBook in plastica nera.

iLife e iWork '09.

E no, neanche stavolta c'era il <em>tablet</em>.