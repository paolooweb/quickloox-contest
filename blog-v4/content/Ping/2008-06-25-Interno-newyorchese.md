---
title: "Interno newyorchese"
date: 2008-06-25
draft: false
tags: ["ping"]
---

Qualche giorno fa il mio quotidiano di riferimento ha dedicato un articolo alla nuova scena musicale di New York e in particolare a una casa di produzione indipendente che si chiama Social Registry. Ne condivido un brano.

<cite>Il Social Registry è il</cite> loft <cite>quattro zero tre di un vecchio palazzone al 61 di Greenpoint. Non c'è il citofono e si sale con un montacarichi. Un adesivo sulla porta dice</cite> musica moderna, idee antiquate<cite>. Zerbo è arrivato qui nel 2005 con un socio, Joe Gaer, e qualche migliaio di dischi.</cite> Prima avevamo altri lavori<cite>, dice,</cite> non ci potevamo permettere di vivere producendo <cite>band</cite> la cui fama arriva a malapena a Manhattan. Io avevo 28 anni e lui trenta, tornavamo a casa e passavamo la notte a impacchettare cd e rispondere a messaggi email. Quando i nostri coinquilini si sono stancati di vedere la cucina costantemente sepolta da montagne di dischi, abbiamo capito che era giunto il momento di avere qualcosa che sembrasse un ufficio, capisci cosa intendo?<cite>. La loro stanza è piccola, ci sono quattro Mac uno di fronte all'altro, sui muri le locandine di concerti e qualche fotografia.</cite>