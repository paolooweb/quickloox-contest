---
title: "Ringraziamenti a Babbo Natale / 3"
date: 2011-01-03
draft: false
tags: ["ping"]
---

Dal sacco dei regali è uscito anche un eccellente disco rigido LaCie <a href="http://www.lacie.com/it/products/product.htm?pid=11560" target="_blank">modello Minimus</a>, versione da un terabyte.

Il <i>marketing</i> aziendale lo qualifica come il modello da tre pollici e mezzo piccolo sul mercato e potrebbe essere vero: è appena meno spesso, un po' più stretto e molto meno lungo del cinquecento gigabyte Iomega che da qualche anno costituisce il mio archivio.

Il primissimo utilizzo del Minimus è stato proprio eseguire un ennesimo <i>backup</i> del disco di archivio, con il vantaggio di avere ancora ulteriori seicento gigabyte liberi per le prossime necessità.

Minimus è un disco Usb 3.0, retrocompatibile Usb 2.0. Infatti l'ho collegato senza problemi a MacBook Pro 17&#8221; (che, volendo, potrebbe montare una interfaccia Usb 3.0 nello slot ExpressCard/34) direttamente a una porta Usb, ovviamente 2.0.

Al momento di andare a dormire ho avviato il <i>backup</i> e al mattino era tutto fatto. Non ho idea del tempo impiegato, solo che una notte è stata sufficiente. Mi dicono che Usb 3.0 è importante ed essenziale perché è più veloce e riconosco l'esigenza di chi deve gestire molti dati in tempo reale mentre lavora, ma rafforzo la mia opinione che la velocità del disco di backup sia un fattore completamente ininfluente. Durante il giorno cerco di lavorare, non di pensare al <i>backup</i>.

Del disco in sé si apprezza la linea, squadratissima ed essenziale, con un vero pannello posteriore, elegante ed essenziale, che lascia aperti i soli fori per il cavo Usb (proprietario, più spesso di un cavo Usb 2.0, più corto del solito), l'alimentazione esterna (anche qui con un cavo un po' più corto di quelli che ho visto finora) e l'interruttore di accensione.

Il Led di accensione è curiosamente situato sul piano superiore dell'unità ma posteriormente, non anteriormente come accade agli altri dischi. La cosa è solo estetica e non pone problemi.

Direi che è tutto. È passata l'epoca in cui un disco rigido andava sviscerato e soppesato. Oggi è un comune oggetto di consumo, che può perfino essere regalato per Natale. Lo si attacca, lo si formatta (basta fare doppio clic su un'icona che appare a video e indicare la formattazione per Mac) e via. C'è anche una opzione di formattazione che lascia un piccolo spazio di interscambio formattato non Mac ma Fat, per la compatibilità eventuale con macchine Windows.

Oggigiorno lo spazio su disco è come la Ram, più ce n'è meglio è. Un terabyte in più fa solo piacere e Babbo Natale ha tutta la mia gratitudine.