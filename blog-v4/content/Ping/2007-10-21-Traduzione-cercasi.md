---
title: "Traduzione cercasi"
date: 2007-10-21
draft: false
tags: ["ping"]
---

Me ne sono reso conto scrivendo un commento al blog di Wired, in un <em>post</em> in cui si chiede retoricamente ad Apple di <a href="http://blog.wired.com/gadgets/2007/10/dear-steve-plea.html?cid=87146530#comment-87146530" target="_blank">entrare nel mondo dei videogiochi</a>.

I giocatori tipici su Windows, quelli che ci-sono-più-giochi-su-Windows, sono <em>gamer</em>. I giocatori tipicamente su Mac, quelli senza l’ossessione della scheda grafica e dei fotogrammi per secondo, sono <em>player</em>.

Su Mac finiscono giocatori interessati a giocare. Su Windows giocatori interessati al possesso del titolo, o al fatto di poterlo giocare con la scheda video X, o al piacere fisico di maneggiare un controller complicato abbastanza da potersi sentire speciali.

Purtroppo non ho una traduzione per esprimere la differenza in italiano.