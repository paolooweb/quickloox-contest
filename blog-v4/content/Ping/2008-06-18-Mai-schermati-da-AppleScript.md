---
title: "Mai schermati da AppleScript"
date: 2008-06-18
draft: false
tags: ["ping"]
---

Un po' lungo questo AppleScript, più da capire che ripetere però. Lo ha scritto <a href="http://slaunchaman.wordpress.com/2008/05/20/resize-your-windows-automatically-for-different-resolutions/" target="_blank">Jeff Kelley</a>, professionista che spesso deve cambiare risoluzione di schermo e si è stufato di aggiustare in continuazione le finestre su scrivanie sempre diverse. C'è AppleScript, perché faticare più di una volta?

Il modo in cui Jeff sistema le proprie finestre non è detto sia il meglio per tutti. Lo scopo però è capire come ragiona, per modificare lo script secondo come si ragiona noi. Se riusciamo a pensare nel modo giusto per impostare un cambiamento, lo scopo è raggiunto!

Sotto con Script Editor e iniziamo.

<code>tell application "Finder"
	set risoluzione_schermo to bounds of window of desktop</code>

<code>risoluzione_schermo</code> è una variabile, un contenitore, un cassettino, contenente una lista di quattro elementi che descrivono la risoluzione dello schermo. Se pensiamo al piano cartesiano di vecchia memoria liceale, il primo elemento è l'origine, l'inizio, dell'asse x dell'elemento rispetto allo schermo. Il secondo elemento è l'origine dell'asse y sempre rispetto allo schermo. Il terzo elemento è il numero di pixel orizzontali disponibili e il quarto elemento conta i pixel verticali. Sul mio PowerBook, <code>risoluzione_schermo</code> vale <code>{0,0,1440,900}</code>. Siccome la <code>window of desktop</code> è un caso particolare e non riguarda una finestra, bens&#236; tutto lo schermo, il suo punto di origine è 0 in verticale e in orizzontale (attenzione, liceali: su un Mac l'origine è in alto a sinistra, non in basso a sinistra!), i pixel sono 1.440 in orizzontale e 900 in verticale. Se volessimo una finestra di 500 x 200 pixel situata a 100 pixel dal bordo sinistro e a 150 pixel dal bordo superiore, i suoi quattro elementi sarebbero {100,150,500,200}. All'inizio confonde, però dopo qualche prova diventa tutto molto chiaro. Le piccole magie di Jeff riguardano non tanto posizionare una finestra in un punto fisso dello schermo, che a questo punto dovrebbe essere semplice, quanto posizionarle in modo relativo, per esempio <em>in mezzo allo schermo</em> o roba del genere.

<code>	set larghezza_schermo to item 3 of risoluzione_schermo
	set altezza_schermo to item 4 of risoluzione_schermo
end tell</code>

Questi comandi sono conseguenza di quanto appena detto e inseriscono in due variabili diverse due elementi diversi dell'elenco di cui sopra.

<code>tell application "System Events" to tell process "Dock"
	set dimensioni_dock to size in list 1
	set altezza_dock to item 2 of dimensioni_dock
end tell</code>

Questa è già piccola magia. Il Dock viene dimensionato in funzione della grandezza del desktop. Non è elegante?

<code>set larghezza_desiderata to 1400</code>

Questa variabile definisce un'area di lavoro che piace a Jeff, dalla quale possono discendere un sacco di conseguenze che stiamo per vedere. Non commento le operazioni aritmetiche che si vedono qui sotto; sono appunto operazioni aritmetiche elementari. Invece è stimolante capire che cosa fanno accadere. Se proprio non battiamo chiodo, lanciamo un altro utente su Mac OS X (cos&#236; il nostro desktop non si modifica) ed eseguiamo lo script, cos&#236; vediamo che cosa avviene e possiamo collegarlo alle istruzioni!

<code>set spazio_laterale to larghezza_schermo - larghezza_desiderata
set bordo_sinistro to (spazio_laterale / 2)
set bordo_destro to bordo_sinistro + larghezza_desiderata
set bordo_inferiore to altezza_schermo - altezza_dock
set bordo_superiore to 22 (* per la barra dei menu *)</code>

Ecco. E adesso si definiscono le finestre applicazione per applicazione.

<code>try
	tell application "iTunes"
		activate
		set the bounds of the first window to {bordo_sinistro, bordo_superiore, bordo_destro, bordo_inferiore}
	end tell
end try</code>

<code>try
	tell application "Firefox"
		activate
		set the bounds of the first window to {bordo_sinistro, bordo_superiore, bordo_destro, bordo_inferiore}
	end tell
end try</code>

<code>try
	tell application "Mail"
		activate
		set the bounds of the first window to {bordo_sinistro, bordo_superiore, bordo_destro, bordo_inferiore}
	end tell
end try</code>

<code>try
	tell application "Vienna"
		activate
		set the bounds of the first window to {bordo_sinistro, bordo_superiore, bordo_destro, bordo_inferiore}
	end tell
end try</code>

Eccetera eccetera. Le finestre iniziali dei vari programmi sono definite tutte con le stesse dimensioni, ma non sarebbe difficile (ora) impostarle a piacere.

Come di consueto, ora arriva lo script integrale.

<code>tell application "Finder"
	set risoluzione_schermo to bounds of window of desktop
	set larghezza_schermo to item 3 of risoluzione_schermo
	set altezza_schermo to item 4 of risoluzione_schermo
end tell

tell application "System Events" to tell process "Dock"
	set dimensioni_dock to size in list 1
	set altezza_dock to item 2 of dimensioni_dock
end tell

set larghezza_desiderata to 1400

set spazio_laterale to larghezza_schermo - larghezza_desiderata
set bordo_sinistro to (spazio_laterale / 2)
set bordo_destro to bordo_sinistro + larghezza_desiderata
set bordo_inferiore to altezza_schermo - altezza_dock
set bordo_superiore to 22 (* for the menu bar *)

try
	tell application "iTunes"
		activate
		set the bounds of the first window to {bordo_sinistro, bordo_superiore, bordo_destro, bordo_inferiore}
	end tell
end try

try
	tell application "Firefox"
		activate
		set the bounds of the first window to {bordo_sinistro, bordo_superiore, bordo_destro, bordo_inferiore}
	end tell
end try

try
	tell application "Mail"
		activate
		set the bounds of the first window to {bordo_sinistro, bordo_superiore, bordo_destro, bordo_inferiore}
	end tell
end try

try
	tell application "Vienna"
		activate
		set the bounds of the first window to {bordo_sinistro, bordo_superiore, bordo_destro, bordo_inferiore}
	end tell
end try</code>