---
title: "E i fatti mi cosano"
date: 2010-02-15
draft: false
tags: ["ping"]
---

Mi chiedono perché <i>ce l'ho cos&#236; tanto con Flash che è cos&#236; tanto comodo</i>.

Non ce l'ho con Flash. Ha fatto il suo tempo, semplicemente. Soluzioni come Html5 sono molto più efficienti, performanti e soprattutto standard comune, mentre Flash lo controlla Adobe e male, perché il <i>plugin</i> è poco stabile, inefficiente e anche con qualche baco di sicurezza. E su Mac è scritto peggio che su Windows.

Html5 è vantaggioso da tutti i punti di vista e s&#236;, lo so, tutti i grafici che si sono inventati un mestiere imparando Flash dovranno reinventarselo imparando Html5. Peraltro, un sacco di gente ha dovuto imparare Html quando è arrivato il web; non è morto nessuno. L'aggiornamento professionale fa parte dei rischi e delle opportunità di tutte le professioni.

Ma divago. Il punto fondamentale è che Adobe sostiene Html5, a parole; nei fatti, <a href="http://ln.hixie.ch/?start=1265967771&amp;count=1" target="_blank">sta sabotando il processo decisionale</a> del comitato W3C che deve completare la specifica dello standard, sollevando obiezioni formali anonime che impediscono ogni approvazione fino a che non sono state superate. Ma non lo dice.

Meglio, nega con decisione una volta emerso il fatto. Al che uno dei responsabili del W3C <a href="http://lists.w3.org/Archives/Public/public-html/2010Feb/0439.html" target="_blank">risponde</a> <i>ok, forse mi sono sbagliato. Posso preparare la bozza da pubblicare, visto che non ci sono obiezioni formali?</i> E la voce di Adobe, così aggressiva nel negare, svanisce.

Non ce l'ho con Flash. L'azienda che lo produce, però, lavora per ritardare l'arrivo di Html5, che è un progresso generale.

Tradotto: l'interesse di Adobe in questo momento è contrario agli interessi della comunità.

Facendo parte della comunità e non di Adobe, so automaticamente da che parte stare.

E gli mando <a href="http://www.paolocevoli.com/" target="_blank">l'assessore di Roccofritto</a> a cantargliele chiare. Perfino lui, che si è fatto il sito in casa, tiene a scrivere che è privo di <cite>flesciate varie</cite>.