---
title: "iPotus*"
date: 2008-11-05
draft: false
tags: ["ping"]
---

Tra le mille iniziative organizzative che hanno portato alla vittoria di Obama nelle presidenziali Usa c’è anche l’applicazione gestionale <a href="http://www.macworld.it/blogs/ping/?p=1985" target="_self">sviluppata per iPhone e iPod touch</a>.

Come avrà fatto, senza gli Mms?

<h7>*Potus è l’acronimo di <em>President of the United States</em>, nel gergo dello <em>staff</em> governativo americano.</h7>