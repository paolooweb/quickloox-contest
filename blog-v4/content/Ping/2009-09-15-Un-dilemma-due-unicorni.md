---
title: "Un dilemma, due unicorni"
date: 2009-09-15
draft: false
tags: ["ping"]
---

Il primo <i>spot</i> che reclamizza Windows 7, protagonista la bimba Kylie, <a href="http://www.youtube.com/watch?v=ssOq02DTTMU" target="_blank">mostra un unicorno</a> (a 00:18).

Il 4 aprile David Webster, <i>general manager</i> per il <i>brand marketing</i> in Microsoft, <a href="http://www.newsweek.com/id/192459" target="_blank">fece un commento</a> su chi usa Mac: <cite>Non tutti vogliono una macchina lavata con lacrime di unicorno</cite>.

Il significato della frase era più o meno che alla maggior parte della gente vano bene computer qualsiasi, come tutti gli altri, in opposizione ai Mac, che si distinguono cos&#236; ossessivamente dagli altri al punto di fare la figura degli unicorni, creature rarissime e inavvicinabili.

Kylie ha sbagliato computer. O ha sbagliato animale.