---
title: "Sogno e realtà"
date: 2003-11-13
draft: false
tags: ["ping"]
---

I personal computer migliori del mondo vengono tuttora fabbricati a Cupertino

I migliori creduloni del Paese stanno sbavando davanti a <link>macchine davvero meravigliose</link>http://www.go-l.com/desktops/machl38/features/.

Peccato che non esistano e che l’intera operazione sia una burla, nella migliore delle ipotesi, e una truffa nella peggiore.

I Macintosh hanno i loro difetti, ma restano i personal computer migliori che esistano. Poi, appunto, nel reame del non esistente ci sono un sacco di alternative.

<link>Lucio Bragagnolo</link>lux@mac.com