---
title: "La superficialità che spegne gli entusiasmi"
date: 2010-02-02
draft: false
tags: ["ping"]
---

Rispetterei certamente il <a href="http://attivissimo.blogspot.com/2010/01/aiped-aipad-aipod.html" target="_blank">giudizio che Paolo Attivissimo dà di iPad</a> a livello di caratteristiche tecniche se si basasse su qualcosa.

<cite>Non entusiasmanti</cite>, &#8220;spiega&#8221;. Ed è tutto ciò che è dato sapere.

Paolo rimanda alla <a href="http://www.apple.com/it/ipad/specs/" target="_blank">scheda tecnica di iPad</a>, da cui evidentemente dovremmo desumere da soli un identico giudizio.

Potrebbe essere magari il processore? Forse, ma è stato creato apposta. Non se ne mai visto uno analogo prima del 27 gennaio. Come facciamo a sapere se è entusiasmante e funziona alla grande o non lo è e dà problemi?

Forse lo schermo. Sarà non entusiasmante perché da 9,7 pollici e quindi troppo piccolo oppure perché, racconta Paolo, è impossibile portare iPad sempre con sé e quindi troppo grande? Mah. Forse c'entra la risoluzione, ritenuta inadeguata. Strano, però, dato che a 132 punti per pollice si tratta di un ottimo valore di densità. Inoltre la tecnologia Ips rende lo schermo particolarmente leggibile anche ad angoli di visione altrimenti proibitivi.

Se è vero quanto afferma Apple, dieci ore di batteria in riproduzione video sono un dato lusinghiero. E nessuno ha ancora potuto confermarlo o smentirlo, complicandone una definizione di <cite>non entusiasmante</cite>.

Il wifi è 802.11n, cioè lo standard più evoluto in commercio; un componente cruciale dell'apparecchio è ovviamente lo schermo sensibile al tocco, dove Apple sostiene di avere svolto un lavoro qualitativamente ai vertici. Può darsi che sia vero oppure una sporca bugia, solo che possiamo dirlo solo dopo una prova intensiva, che nessuno ha ancora potuto eseguire.

Paolo: immutata stima. Ma non si potrebbe, perlomeno, sospendere il giudizio sulle caratteristiche tecniche di un aggeggio che abbiamo visto per una volta sola dentro una finestrella, da diecimila chilometri di distanza?