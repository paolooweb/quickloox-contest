---
title: "Nuovo sembra più bello"
date: 2003-05-11
draft: false
tags: ["ping"]
---

Cambia nome e fa impressione. Ma la solidità è un’altra cosa

Ha fatto veramente impressione la demo di BeOS che ho visto a Webbit 2003.
Il sistema operativo della società fondata da Jean-Louis Gassée (autore di Mac Portable) e poi defunta è diventato open source, raccogliendo il lavoro di programmatori di straordinario talento e cambiando nome in Zeta.

Le demo fanno spavento: multitasking e multithreading velocissimi, grande flessibilità e versatilità, compatibilità straordinaria con una lista interminabile di formati di ogni tipo.

Molto bello, anche se la solidità è un’altra cosa. Quando il sistema operativo e le applicazioni sono scritte dalle stesse persone è tutto facile. Quando ti ritrovi migliaia di sviluppatori, anni di upgrade e decine di applicazioni vitali per un qualche settore professionale, il fenomeno dell’accumulazione delle pezze, dei compromessi, delle soluzioni quick but dirty porta a sistemi solidi. E irrimediabilmente meno efficienti.

Zeta è stupendo; ma se fosse concretamente utilizzato perderebbe smalto.

<link>Lucio Bragagnolo</link>lux@mac.com