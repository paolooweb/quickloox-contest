---
title: "Un sito fuoriserie"
date: 2010-10-03
draft: false
tags: ["ping"]
---

L'ho scoperto solo oggi, fantastico <a href="http://www.powerbookmedic.com/identify-mac-serial.php" target="_blank">PowerBook Medic</a>: digiti il numero di serie e ti dice che Mac è (anche se è MacBook Pro!). Perfetto per tutti quanti hanno da gestire Mac di famiglia, amici e colleghi.

Se poi si fa uno sforzo per scarica pure <a href="http://mactracker.dreamhosters.com/" target="_blank">MacTracker</a>, il combinato è formidabile.