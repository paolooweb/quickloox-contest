---
title: "In fila per X(phile)"
date: 2007-05-11
draft: false
tags: ["ping"]
---

Molto, molto tempo fa si era finito di parlare a lungo di <a href="http://homepage.mac.com/rossetantoine/osirix/" target="_blank">OsiriX</a>, il lettore open source di radiografie per Mac.

Adesso ho scoperto che esiste anche <a href="http://homepage.mac.com/d2p/xphile/" target="_blank">Xphile</a>, le cui tecnicalità mi sfuggono, ma ho l'impressione che c'entri anche lui.

Ho anche l'impressione che la copertura Mac del settore tenda all'aumento. :)