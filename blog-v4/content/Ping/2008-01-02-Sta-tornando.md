---
title: "Sta tornando"
date: 2008-01-02
draft: false
tags: ["ping"]
---

Il mio primo programma di <em>chat</em> multiprotocollo è stato Proteus, che era shareware. 

Dopo una chiamata alle armi lo scorso febbraio, ora Proteus sta tornando davvero, dopo un lungo silenzio. Ora è <em>open source</em>.

Le prove: la <a href="http://code.google.com/p/proteusim/" target="_blank">pagina del progetto</a> su Google Code e la <a href="http://www.proteusx.org/" target="_blank">home del sito</a> che sta per andare online. Da ambedue si può scaricare una nuova versione.

Non è che <a href="http://www.adiumx.com" target="_blank">Adium</a> abbia problemi, anzi, è favoloso. Proteus, però, era elegante. Chissà com'è ora.