---
title: "Intervista tendenziosa"
date: 2009-11-11
draft: false
tags: ["ping"]
---

Le domande me le sono inventate io. Le <a href="http://www.pcr-online.biz/features/328/Microsofts-new-vision" target="_blank">risposte (ad altre domande)</a> le ha date Simon Aldous, <i>partner group manager</i> di Microsoft.

<i>Domanda: Windows 7 è un cambiamento significativo oppure è, come ha dichiarato Phil Schiller di Apple,</i> sempre il solito Windows?

Risposta: [Windows 7] è sostanzialmente la versione successiva di Vista.

<i>Domanda: È vero o no che Windows non fa che copiare Mac OS X?</i>

Risposta: Quello che abbiamo cercato di fare con Windows 7 [&#8230;] è creare un <i>look and feel</i> Mac in termini grafici.