---
title: "Sorridi, sei su Mac OS X"
date: 2002-09-04
draft: false
tags: ["ping"]
---

Cercasi nome per una funzione tanto semplice eppure così evoluta

Una mia amica sente su Mac OS X la mancanza dello screen capture in modalità finestra che c’è su Mac OS 9. Poi non ricorda mai la combinazione dei tasti, ma è un dettaglio.

Le ho consigliato di non lamentarsi e provare Comando-maiuscolo-barraspaziatrice-4 (su Mac OS X 10.2). Lei ora è felice più di prima (è una istintiva!) ma adesso non sa più come chiamare la funzione. Screen capture, dice, è veramente riduttivo.

Tu che cosa diresti?

<link>Lucio Bragagnolo</link>lux@mac.com