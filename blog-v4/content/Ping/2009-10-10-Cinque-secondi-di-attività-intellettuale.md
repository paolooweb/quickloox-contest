---
title: "Cinque secondi di attività intellettuale"
date: 2009-10-10
draft: false
tags: ["ping"]
---

Si è parlato nei giorni scorsi di <a href="http://www.macworld.it/blogs/ping/?p=2888" target="_self">velocità di avviamento</a> di Mac OS X e di Windows, con Apple che sostiene di avere ridotto i tempi di <i>boot</i> fino al 50 percento da Leopard a Snow Leopard e Microsoft che, per bocca di un ingegnere, ha rispettato l’obiettivo di migliorare del cinque percento tra Vista e Windows 7.

Adesso il fatto che Windows 7 sia più veloce di Vista nel <i>boot</i> viene messo in discussione, da <a href="http://reviews.cnet.com/windows/microsoft-windows-7-professional/4505-3672_7-33704140.html" target="_blank">Cnet</a> (secondo i cui test dipende dalla versione e comunque Windows Xp è più veloce) e da <a href="http://ioiotechnologies.com/" target="_blank">Iolo Technologies</a>, azienda californiana i cui esperimenti descrivono un <a href="http://www.iolo.com/promo/labs/app/docs/iolo_Labs_Study-Windows_Startup_Over_Time.pdf" target="_blank">clamoroso peggioramento</a> nei tempi di boot di Windows con il passare del tempo. Interessante anche lo studio sulla diminuzione di reattività e sull’<a href="http://www.iolo.com/promo/labs/app/docs/iolo_Labs_Study-PC_Responsiveness_Over_Time.pdf" target="_blank">aumento nel tempo di spreco di processore</a>.

Ora però si è creato un grosso equivoco. Il tempo di avviamento del sistema viene infatti impiegato come argomento di <i>marketing</i> e come elemento di valutazione delle prestazioni. Lenovo ha iniziato a reclamizzare <i>boot</i> di Windows 7 <a href="http://www.computerworld.com/s/article/9138531/Lenovo_vows_unbelievably_fast_Windows_7_boots" target="_blank">fino al 56 percento più veloci</a> e <a href="http://www.phoenix.com" target="_blank">Phoenix Technologies</a> ha mostrato una <i>demo</i> di <i>boot</i> <a href="http://www.viddler.com/explore/engadget/videos/645/" target="_blank">in soli dieci secondi</a>.

Tutto lavoro ragguardevole, ma chi se ne frega? Il mio ultimo <i>uptime</i> è stato di ventiquattro giorni. Se togliessero ipoteticamente due minuti al tempo di <i>boot</i> del mio sistema (che peraltro ci mette molto meno), avrei guadagnato ben <i>cinque secondi</i> di produttività quotidiana. I test di Geekbench <a href="http://www.primatelabs.ca/blog/" target="_blank">mostrano</a> che Snow Leopard guadagna il tre percento di prestazioni globali su Leopard. I secondi in gioco ogni giorno sono molti di più e, qualunque sia il vantaggio di Snow Leopard in termini di velocità di <i>boot</i>, sulle prestazioni complessive è irrisorio.

Guardare al <i>boot</i> in termini di prestazioni è uno specchietto per allodole per chi vende e masturbazione intellettuale per chi compra. Invece ci si deve guardare in termini di rifinitura del sistema operativo. Da come si comporta il <i>software</i>, capisci molto di come ti considera chi lo ha programmato.