---
title: "Sempre quel tasto"
date: 2011-02-23
draft: false
tags: ["ping"]
---

Erica Sadun è stata pioniere del <i>jailbreak</i> di iPhone e scrive regolarmente su <i>The Unofficial Apple Weblog</i>. Ultimamente <a href="http://www.tuaw.com/2011/02/23/i-surrender-joining-the-ranks-of-the-airhead-apocalypse/" target="_blank">ha annunciato</a> l'acquisto di un MacBook Air a seguito di tre hackintosh, ovvero netbook su cui ha installato Mac OS X (ognuno con una licenza specifica: da amante dei jailbreak Erica viola il contratto di licenza, ma almeno non ruba il software).

E si chiede retoricamente <cite>dopo tre</cite> netbook <cite>trasformati in</cite> hackintosh<cite>, di cui sono del tutto felice, perché sto per comprare Apple? Tre grandi risposte: aggiornamento software, tipologia di utilizzo e tastiera.</cite>

La prima risposta è naturale conseguenza della configurazione: <cite>francamente, mi sono un po' stancata del balletto software implicato in tutte queste soluzioni non standard. Che si tratti di attendere il</cite> jailbreak <cite>di un iPhone per tenere il passo dell'ultimo</cite> firmware <cite>oppure aspettare per scoprire se un aggiornamento può fare secchi i miei</cite> hackintosh<cite>, sono stufa del giochino aspetta-e-vediamo</cite>. Tutto logica conseguenza, non fa una grinza.

La seconda risposta è anch'essa naturale e porta alla terza: <cite>se in passato ho messo insieme soluzioni grazie ai miei</cite> hackintosh, <cite>i compromessi fisici dei</cite> netbook <cite>li rendono poco attraenti per bloggare più di uno o due paragrafi e non parliamo di rivedere capitoli di un libro, o programmare</cite>. Ci sta sempre.

Ed ecco la terza: <cite>devo ancora trovare una tastiera da</cite> netbook <cite>che io ami. Molte le ho subite, alcune le ho tollerate. Ma non sono veramente utilizzabili da parte di un buon digitatore per più di un minuto o due alla volta. [&#8230;] Per quando riguarda il</cite> blogging<cite>, una buona tastiera fa la differenza tra il buttare fuori un post di emergenza e fornire copertura continuativa di un evento.</cite>

E questo è il punto. Un <i>netbook</i> ha una tastiera insufficiente per qualunque uso non occasionale e a basse prestazioni. Ecco perché la moda sta passando, ecco perché si sono venduti oramai venti milioni di iPad (che offre esperienza di tastiera della stessa qualità, ma prova a girare un <i>netbook</i> in verticale&#8230;), ecco perché Apple non fa <i>netbook</i>, ha fatto bene a non farne e spero continui.

Il mestiere di Apple, come scrivevo ad aprile 2009, è <a href="http://www.macworld.it/ping/nomi/2009/04/26/voglio-un-notbook/">fare notbook</a>. I <i>netbook</i> sono fatti per essere venduti, non per essere comprati.