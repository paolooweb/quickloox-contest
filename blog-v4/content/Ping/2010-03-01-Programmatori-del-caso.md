---
title: "Programmatori del caso"
date: 2010-03-01
draft: false
tags: ["ping"]
---

Microsoft ne ha combinate tali e tante con l'imposizione al mondo di Internet Explorer che l'Unione Europea, unico luogo di raccolta di ottusità complessiva comparabile, ha fatto la voce grossa. Cos&#236; ora Microsoft propone su Windows 7 venduto in Europa una <a href="http://www.browserchoice.eu/" target="_blank">pagina di scelta del browser</a> che propone cinque alternative di installazione, tra dodici browser.

La pagina dispone cinque <i>browser</i> diversi in modo diverso ogni volta che la si carica. A caso, dichiara Microsoft.

Non è a caso per niente.

E a questo punto verrà da pensare a una malvagia manipolazione dei risultati.

Peggio, molto peggio: la pagina è male programmata. Dato il problema di disporre a caso cinque elementi, Microsoft &#8211; non il dilettante della domenica &#8211; non è capace di (o non vuole, peggio al cubo) elaborare una soluzione corretta.

Se ne sono <a href="http://www.dsl.sk/article.php?article=8770" target="_blank">accorti per primi</a> in Slovacchia. Posto interessante, gente simpatica, ottima birra, non certo la capitale della programmazione.

Rob Weir <a href="http://www.robweir.com/blog/2010/02/microsoft-random-browser-ballot.html" target="_blank">verifica che l'errore c'è e spiega</a> ai tecnicamente inclinati come andrebbe evitato. Per superprogrammatori si trova anche una <a href="http://sroucheray.org/blog/2009/11/array-sort-should-not-be-used-to-shuffle-an-array/" target="_blank">descrizione teorica e pratica</a> della problematica.

Una buona spiegazione della mediocre qualità di Windows è che Microsoft, prima software al mondo con oltre trent'anni di esperienza, non sa mettere insieme un pezzettino di JavaScript come si deve.