---
title: "Messaggi sconcertanti"
date: 2008-10-01
draft: false
tags: ["ping"]
---

Una volta l'anno mi tocca visitare <a href="http://www.microsoft.com/italy/windows/default.mspx" target="_blank">il sito Microsoft</a>.

Sulla <em>home page di Windows</em> (ma non sarebbe il sito Microsoft?) viene strillato Internet Explorer 8 beta: <cite>Più facile, veloce e sicuro che mai.</cite> È l'opposto di quello che chiunque può apprendere liberamente sul web da mille fonti indipendenti, ma è chiaro che ognuno può vendere come vuole.

Sotto gli strilli principali: <cite>Hai mai pensato a cosa potrebbe nascondersi nel tuo Pc?</cite>

Confesso di no. Il mio Mac è un vantaggio, non una palla al piede o un <em>killer</em> acquattato nello studio.