---
title: "Utility che mancano (e non)<p>"
date: 2005-04-23
draft: false
tags: ["ping"]
---

Cinquecento puntate di Ping e finalmente qualcosa di utile<p>

Mi scrive Attilio Farina:<p>

<cite>Caro Lucio,</cite><br>
<cite>Ho apprezzato il tuo libretto <em>1000 Mac Utility</em></cite>.<p>

<cite>Ti segnalo le seguenti utility che ti sono sfuggite: <a href="http://www.atomicbird.com/">Macaroni</a>, <a href="http://www.macosxcocktail.com/">Cocktail</a>, <a href="http://www.mac4ever.de/invisibles/">InVisibles</a>, <a href="http://www.supermagnus.com/mac/Event_Reminder/index.html">Event Reminder</a>, <a href="http://pubweb.nwu.edu/~zps869/nv.html">Notational Velocity</a>, <a href="http://www.students.uni-mainz.de/bauec002/B2Main.html">Basilisk II</a>.</cite><p>

<cite>Quest&rsquo;ultima mi piace perché mi ricorda i bei tempi passati (sono un Apple fan dal 1983, Apple //e).</cite><p>

Alcune utility (vedi Cocktail) non le ho messe volontariamente, appunto perché ampiamente conosciute da tutti. Altre (vedi Notational Velocity) invece mi sono proprio passate sotto il radar e meritano veramente.<p>

Un grosso grazie ad Attilio e un invito a tutti: in quel libro allegato a Macworld compare più o meno l&rsquo;uno percento del software disponibile per Macintosh. Più mi arrivano segnalazioni di software che manca all&rsquo;elenco, più le pubblico, prima arriviamo a un censimento dei programmi Mac come mai prima. Il che da solo giustificherebbe l&rsquo;esistenza di questa strisciolina quotidiana.<p>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>