---
title: "Si fa con comodo"
date: 2007-06-21
draft: false
tags: ["ping"]
---

Non ho ancora annunciato il prossimo venerd&#236; del gioco unicamente perché sono irreperibile fino a&#8230; sabato. Il venerd&#236; verrà recuperato, come di consueto, mercoled&#236; della prossima settimana, ossia il 27 giugno, dalle 13 alle 14, stanza iChat <em>gamefriday</em> per coordinarsi e salutarsi.

Salva smentite, il gioco sarà <a href="http://assault.cubers.net/" target="_blank">AssaultCube</a>, <em>first-person shooter open source</em>. Dico <em>salvo smentite</em> perché è troppo tardi e non ho tempo, ora, di verificare se con la rete funziona tutto.

Intanto i nottambuli possono divertirsi un po’ cercando di indovinare <a href="http://n.nfshost.com/1.html" target="_blank">qual è l’Url della pagina successiva?</a>