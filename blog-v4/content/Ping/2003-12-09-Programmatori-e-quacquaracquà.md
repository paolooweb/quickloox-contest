---
title: "Programmatori e quacquaracquà"
date: 2003-12-09
draft: false
tags: ["ping"]
---

La differenza tra i programmatori seri e chi improvvisa si vede alla distanza

Prendiamo un programma come BBEdit. È uscita da poco la versione 7.1, con un sacco di migliorie per la versione Mac OS X. Per dirne solo una, ora che Panther contiene WebKit (il motore open source di Safari), BBEdit permette di vedere in anteprima il codice Html chiamando direttamente WebKit e senza bisogno di aprire un browser esterno.

Chi usa BBEdit 7.1 in Mac OS 9 non ha questa funzione, perché WebKit fa parte di Panther e naturalmente Bare Bones (creatrice di BBEdit) non può reinventare la ruota per chi ha deciso di non passare a Panther. Tuttavia BBEdit 7.1 funziona regolarmente in Mac OS 9, dando all’utente Mac OS 9 tutte le funzioni del programma che sono permesse dal sistema operativo in uso.

Giù il cappello, signori. Questi sono professionisti, non quacquaracquà. Altro che “questa versione funziona solo con”.

<link>Lucio Bragagnolo</link>lux@mac.com