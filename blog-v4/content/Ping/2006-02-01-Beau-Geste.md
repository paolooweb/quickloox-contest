---
title: "Beau geste"
date: 2006-02-01
draft: false
tags: ["ping"]
---

Cosa nota ma non notissima e che forse vale la pena di sottolineare.

Quando &egrave; stato annunciato il passaggio a chip Intel nello scorso giugno, Apple offr&igrave; ai programmatori il cosiddetto <strong>Developer Transition System</strong>: praticamente un Pc basato su Pentium dentro il case di un Power Macintosh, equipaggiato con una copia di Mac OS X per Intel. Il tutto al costo di 999 dollari e con l&rsquo;impegno di restituire la macchina entro fine 2005, dal momento che i Dts facevano funzionare il sistema operativo ma non avrebbero avuto attinenza con la reale dotazione hardware dei veri Mac con dentro Intel.

Il termine &egrave; scaduto ma chi ha creduto nell&rsquo;operazione, e ha comprato un Developer Transition System per fare esperimenti e universalizzare il proprio software, &egrave; stato ricompensato. Non solo Apple non ha chiesto indietro le macchine, ma fornisce i proprietari di un iMac Core Duo nuovo di zecca. Alla fine dei conti pagato 999 dollari, per quanto anticipati, quindi tutt&rsquo;altro che un cattivo affare.

Come corollario, va notato che nel momento attuale iMac Core Duo &egrave; una macchina eccellente per i programmatori. Infatti, dall&rsquo;adozione dei processori Intel beneficia soprattutto il software basato su calcoli aritmetici con numeri interi, proprio come Xcode. Il cui incremento in prestazioni dovrebbe essere decisamente marcato rispetto ad altre categorie di software.