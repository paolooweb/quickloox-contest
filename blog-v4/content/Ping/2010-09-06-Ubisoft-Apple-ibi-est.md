---
title: "Ubisoft Apple ibi est"
date: 2010-09-06
draft: false
tags: ["ping"]
---

A prima vista uno legge che esiste <a href="http://mightandmagicheroeskingdoms.ubi.com/en/" target="_blank">Might &#38; Magic Heroes Kingdoms</a> di Ubisoft, un nuovo gioco di massa online giocabile da browser con ambientazione fantasy ben nota a chi ha sperimentato qualche altro gioco della serie.

Poi legge che è disponibile solo per il Nordamerica e si arrabbia un po': perché a me no?

Poi però legge la spiegazione di Ubisoft: <cite>abbiamo scelto di aprire il gioco solo nelle nazioni dove siamo sicuri che la qualità è stata portata al massimo e quando siamo pronti e confidenti</cite>.

E allora viene ancora più voglia di aspettare, perché è possibile che il gioco, quando arriverà, sia di qualità.

A volte Apple si muove esattamente su queste linee e in un prodotto o un servizio non bada a implementare tutto l'implementabile, ma solo quello dove si sentono sicuri del fatto loro.