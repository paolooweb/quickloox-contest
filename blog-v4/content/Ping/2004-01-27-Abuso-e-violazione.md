---
title: "Abuso e violazione"
date: 2004-01-27
draft: false
tags: ["ping"]
---

L’unica azienda capace di non suscitare neanche un sensino di colpa

Sono giorni strani.

Migliaia di persone devono stare attente a come parlano, perché dire “lavoro alla Parmalat” rischia di tirarsi addosso qualche sospetto. Peggio ancora essere fornitori di Parmalat, perché chissà quando ti pagano.

Per mezzo Paese guardare i programmi Mediaset è un segno di collusione con il premier.

Trovi sempre un no global disposto a raccontarti le nefandezze della Nestlé per non farti acquistare i cereali da colazione. E così via, tra multinazionali del detersivo che patteggiano con il demonio e boicottaggi dei formaggi francesi in occasione dei test nucleari a Mururoa.

In questi giorni l’Unione Europea sembra si accinga a multare Microsoft per violazione delle norme europee sulla concorrenza e abuso di posizione dominante. E sono tutti lì con il loro explorerino, unico a vedere chissà che sito assurdo, con il loro Entourage o Outlook, come se non esistesse alcun altro programma di posta, con il loro Office, “perché se no non posso lavorare” (ma a chi la racconti?). Neanche un pensierino, nemmeno un’ombra di cedimento. Anime candide.

<link>Lucio Bragagnolo</link>lux@mac.com
