---
title: "Il movimento fa bene"
date: 2009-05-26
draft: false
tags: ["ping"]
---

Per vie traverse mi sono ritrovato su una <a href="http://support.microsoft.com/kb/168702" target="_blank">pagina del supporto Microsoft riguardante un problema del 2002</a>, riguardante un problema di comunicazione tra Excel 97 e database Oracle.

Il problema: <cite>quando si prova a portare dati da Microsoft Query 97 a un foglio di calcolo Excel 97, può apparire l’icona a globo rotante (a indicare che è in corso una interrogazione) per lungo tempo, dopo di che sul foglio di calcolo non arriva alcun dato.</cite>

Il secondo metodo consigliato per aggirare l’inconveniente: <cite>spostare il puntatore del mouse.</cite>

<cite>Se il puntatore viene spostato in continuazione mentre i dati vengono inviati a Microsoft Excel, l’interrogazione non può fallire. Non smettere di spostare il mouse fino a che non sono arrivati tutti i dati.</cite>

<cite>Nota: secondo l’interrogazione effettuata, prima che i dati arrivino sul foglio di calcolo possono passare diversi minuti.</cite>

Se non riesco a prendere sonno non conto più le pecore; visualizzo un ufficio di operatori che agitano vigorosamente il mouse per tenere viva l’interrogazione e fare arrivare i dati a Excel.

Non credo di avere mai letto una nota di supporto tecnico altrettanto surreale.