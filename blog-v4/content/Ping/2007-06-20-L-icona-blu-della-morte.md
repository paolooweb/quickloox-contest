---
title: "L'icona blu della morte"
date: 2007-06-20
draft: false
tags: ["ping"]
---

Mi segnala <strong>Dany</strong> che, in Leopard, l'icona che raffigura un Pc generico collegato alla rete è un monitor con una schermata blu.

Da <a href="http://rs78.rapidshare.com/files/37860375/Leopard_Stuff.zip" target="_blank">questo link</a> si può scaricare l’icona.

Guardando con attenzione si distinguono piccoli caratteri bianchi. Volendo risolvere a tutti i costi il mistero, è il classico <em>blue screen of death</em> di Windows, compagno fedele di tutti gli utenti omonimi.

È giusto anche da un punto di vista di interfaccia; un po' evangelicamente, è normale riconoscere l'albero dai frutti che porta. :-)