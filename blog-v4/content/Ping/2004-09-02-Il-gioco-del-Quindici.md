---
title: "Il gioco del quindici"
date: 2004-09-02
draft: false
tags: ["ping"]
---

Un parere autorevole sulla velocità dei Power Mac G5

Quelli di Mdj (la migliore pubblicazione digitale Mac-oriented sul globo terracqueo) hanno cambiato Mac. Questo il loro commento. Traduco, ma fedelmente:

<cite>
Sostituisce un G4 dual a 800 Mhz ed è una macchina dual a 2500 Mhz, che ovviamente siginifica un aumento di velocità più o meno 3x. Una volta aggiunto il bus più veloce, più Ram, connessioni migliorate ed elaborazione a 64 bit per parte del sistema operativo, fare i conti è difficile.

Fortunatamente siamo professionisti e abbiamo fogli di calcolo che prendono in considerazione questo tipo di cose, insieme a dimensioni e velocità tipiche dei trasferimenti dati, fattori di compressione, wait state e così via. La formula è collaudatissima e rivela che la nuova macchina è il 1500 PER CENTO PIÙ VELOCE.
</cite>

Il maiuscolo è loro.

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>