---
title: "AppleScript&#8230; al piano di sopra"
date: 2008-04-27
draft: false
tags: ["ping"]
---

Le mie piccole intemerate su AppleScript sono solo l'inizio di un viaggio che potrebbe portare una persona brava e interessata a creare veri programmi per Mac OS X. AppleScript è l'uscio per entrare in AppleScript Studio, che è il pianoterra di Xcode.

A questo riguardo posso sta crescendo una risorsa importante che è <a href="http://www.xcodeitalia.com" target="_blank">Xcode Italia</a>. Importante anche perché è in italiano. Senza un pizzico in inglese la programmazione è impossibile; senza la comodità di porre una domanda e ricevere una risposta nella nostra lingua, è scomoda.