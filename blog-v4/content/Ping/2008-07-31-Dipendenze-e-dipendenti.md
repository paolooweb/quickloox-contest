---
title: "Dipendenze e dipendenti"
date: 2008-07-31
draft: false
tags: ["ping"]
---

Ogni tanto salta fuori qualcuno a dire che Apple ha un problema in più delle altre aziende perché è dipendente da Steve Jobs. Come se le altre aziende non avessero questo problema.

Viene fuori che Yahoo ha richiamato Jerry Yang, cofondatore, per risollevare le sorti dell'azienda. Dell ha richiamato Michael Dell, fondatore, per risollevare le sorti dell'azienda.

Ted Waitt, fondatore di Gateway, è stato richiamato per risollevare le sorti dell'azienda&#8230; <a href="http://news.cnet.com/8301-10787_3-10003334-60.html?part=rss&amp;subj=news&amp;tag=2547-1_3-0-20" target="_blank">e non ce l'ha fatta</a>, perché Gateway è stata venduta ad Acer per la somma relativamente modestissima di 710 milioni di dollari (Microsoft l'altroieri era disposta a cavare 45 miliardi di dollari per Yahoo, per capire le proporzioni).

Gateway è rimasta senza Waitt per un anno ed è andata in rovina. Apple è passata per <em>manager</em> dal medio al pessimo per dodici anni senza Jobs ed è ancora qui.

Che Steve Jobs sia importante per le sorti di Apple è evidente. Forse è meno evidente che Apple, da questo punto di vista, è la regola e non l'eccezione.