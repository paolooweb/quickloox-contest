---
title: "Think Different, Speak Clear"
date: 2006-05-21
draft: false
tags: ["ping"]
---

Scrivo su ispirazione di <strong>frix</strong>, che ha inviato un commento stimolante e interessante. Quella campagna, Think Different, &egrave; rimasta nei cuori di tutti. &Egrave; rimasta anche nei cuori di Apple? Sta <em>pensando differente</em> oppure no? Quali sono i criteri di valutazione per dire <em>s&igrave;</em>, <em>no</em>, <em>forse</em>?

In altre parole, dato il messaggio, dove lo si ritrova (o meno) nell&rsquo;azione concreta?

Cercher&ograve; di parlare il meno possibile e di lasciare spazio ai commenti. Sarebbe bello dare sostanza materiale a quel messaggio, per vedere se si &egrave; dentro o fuori rotta, o altrove, o altro ancora.