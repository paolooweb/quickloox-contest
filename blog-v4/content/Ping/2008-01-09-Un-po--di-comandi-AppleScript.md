---
title: "Un po' di comandi AppleScript"
date: 2008-01-09
draft: false
tags: ["ping"]
---

Quanto segue è opera gradita di <strong>Dario</strong>, che ringrazio. :-)

Ora che abbiamo imparato il comando <code>tell</code>, è ora di introdurre alcuni dei comandi AppleScript più usati ed utili.

<strong>Comando <code>location<code></strong>

Questo comando permette di aprire uno specifico indirizzo (sia sul Mac che su di un server web). È generalmente usato per aprire indirizzi attraverso il Finder (in caso di un indirizzo Internet lo aprirà nel browser predefinito). Ad esempio, per aprire l&#8127;indirizzo <em>www.macworld.it</em>, scriveremo:

<code>tell application "Finder" to open location "http://www.macworld.it"</code>

(In inglese, <em>d&#236; al Finder di aprire l&#8127;indirizzo&#8230;</em>)

È molto semplice, dobbiamo solo ricordare di scrivere tra virgolette il nome dell&#8127;applicazione a cui vogliamo impartire un comando. E quando scriviamo un indirizzo internet, ricordare di includere il prefisso (per esempio http://, ftp://, afp:// eccetera) e di scriverlo sempre tra virgolette.

Possiamo anche dire ad un browser specifico di aprire l&#8127;indirizzo Internet:

<code>tell application "Safari" to open location "http://www.macworld.it"</code>

<strong>Comando <code>say<code></strong>

Il comando <code>say</code> (in inglese, d&#236;) si usa per far  
pronunciare delle frasi al Mac. E&#8127; uno dei comandi AppleScript  
più semplici. Per esempio, se vogliamo che il Mac, con la voce di  
default, dica "Hi, I am a Mac", dobbiamo scrivere:

<code>tell application "Finder" to say "Hi, I am a Mac"</code>

Con questo comando, il Mac può dire (in inglese) tutto quello che si vuole; dobbiamo solo ricordare, ancora una volta, di scrivere il testo tra virgolette. Per usare un&#8127;altra voce rispetto a quella di default del sistema, basta aggiungere <code>using</code> alla fine del comando, seguito dal nome della voce che si vuole utilizzare. Il Mac pronuncerà anche frasi in italiano, ma la resa sarà pessima, poichè userà sempre la pronuncia e la fonetica inglesi.

<code>tell application "Finder" to say "Hi, I am a Mac" using "Zarvox"</code>

Possiamo usare tutte le voci installate nel nostro sistema, basta solo scrivere correttamente il nome, al solito, tra virgolette.

<strong>Comando <code>say<code></strong>

Il comando <code>Display dialog</code> è molto utile poichè lo possiamo usare per dare messaggi all&#8127;utente e permettergli di interagire. Questo comando fa apparire una finestra di dialogo sul tuo schermo, corredata da alcuni pulsanti. Il suo utilizzo è molto semplice: se vogliamo mostrare il messaggio "Ciao mondo!", dovremo scrivere:

<code>tell application "Finder" to display dialog "Ciao mondo!"</code>

(In inglese, <em>d&#236; al Finder di mostrare una finestra di dialogo con scritto&#8230;</em>)

Una piccola finestra di dialogo del Finder apparirà sullo schermo, con il testo che abbiamo deciso.

Possiamo anche scrivere semplicemente:

<code>display dialog "Ciao mondo!"</code>

Per il nostro Mac sarà lo stesso.

Possiamo usare AppleScript per eventi di sistema come Spegni, Riavvia, Stop&#8230; Ecco il formato di questi comandi:

<code>tell application "Finder" to shut down</code> (Spegni)

<code>tell application "Finder" to sleep</code> (Stop)

<code>tell application "Finder" to restart</code> (Riavvia)

Ora, costruiamo uno script collegando i comandi <code>Display Dialog</code> e <code>Say</code>.

<code>tell application "Finder" to display dialog "Ciao, clicca OK e parlerò con te!</code>

<em>(Mostriamo una finestra di dialogo in cui chiediamo all&#8127;utente di cliccare OK)</em>

<code>tell application "Finder" to say "Hello, my name is Macintosh; I like apples."</code>

(Una volta che l&#8127;utente avrà cliccato OK, il Mac pronuncerà una frase)

Questo minitutorial è ispirato a <a href="http://www.tuaw.com/2007/12/27/applescript-the-script-editor/" target="_blank">quest'altro</a>, comparso su The Unofficial Apple Weblog.