---
title: "iPhone senza rivali"
date: 2009-04-02
draft: false
tags: ["ping"]
---

Google (e il <em>provider</em> T-Mobile) <a href="http://www.macworld.it/showPage.php?template=notizie&amp;id=16706" target="_self">proibiscono</a> il <em>tethering</em>, l'uso dello <em>smartphone</em> come modem, su Android.

Apple è stata lungamente criticata perché iPhone non permetteva il <em>tethering</em> (lo fa, ma ci vuole il <em>jailbreak</em>). Adesso il <em>tethering</em> arriva su iPhone OS 3.0, mentre Android lo vieta.

Ma Google non riceve critiche e questo mostra bene quanto, a oggi, Android sia irrilevante.

<em>Aggiornamento</em>: il tethering è tornato, però fuori dagli States. In altre parole, è ok per Google, è ko per T-Mobile.