---
title: "L&rsquo;onere dei prezzi<p>"
date: 2005-12-03
draft: false
tags: ["ping"]
---

Giochi di parole sui film con Jack Nicholson a parte, i prezzi dei Mac&hellip;<p>

Vale la pena di tornare sul discorso prezzi perché arrivano riflessioni intelligenti e centrate. Una è quella di <strong>Marco</strong>. Lascio concludere lui, perché una discussione su quanto costano i Mac si può effettivamente concludere solo come vedrai:<p>

<cite>Anzitutto i prezzi sono crollati. Cinque anni fa comprai un PowerBook 15&rdquo;, un iMac G3 e un palmare di lusso e spesi 9.000 franchi [prezzi svizzeri. N.d.lux]. Oggi ho preso un iBook 12&rdquo;, un iMac G5 e un palmare di lusso. OK, è un iBook piccino piccino mentre quello là era un bel bestione (per l&rsquo;epoca). Però ho speso 4.000 franchi. Fatte tutte le debite proporzioni, mi pare che i prezzi siano crollati a picco.</cite><p>

<cite>In secondo luogo, ieri pomeriggio ero in un discount di elettronica, dove vendono computer di tutte le marche e di tutte le fogge. Moltissimi portatili. Non ho neppure guardato i prezzi, perché tanto il mio ordine ormai l&rsquo;ho inoltrato e comunque voglio un Mac e del resto del mondo non mi frega. Però ho contemplato l&rsquo;aspetto esteriore delle macchine, la pura questione estetica. Orrendi. Non uno che si salvasse. Non uno che sia uno, dico. Poi ho pensato al mio iBook in arrivo, o anche solo al mio vecchio PowerBook ancora sulla mia scrivania, certo invecchiato ma sempre elegante, sobrio, semplice, comodo. E ho sorriso&hellip;</cite><p>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>