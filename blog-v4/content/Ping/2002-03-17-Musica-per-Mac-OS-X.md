---
title: "Musica per Mac OS X"
date: 2002-03-17
draft: false
tags: ["ping"]
---

Arriva Sibelius 2 e mostra perché, per la musica, sarà meglio Mac OS X

Stanno arrivando i programmi Mac OS X davvero importanti per i professionisti della musica. Il primo è <link>Sibelius 2</link>http://www.sibelius.com/products/sibelius/2/.
Si potrebbe pensare che, funzionando Mac OS X in multitasking prelazionale e quindi negando a qualsiasi programma il controllo completo della macchina, un programma musicale - che deve osservare un tempismo perfetto di esecuzione - possa avere problemi.
In realtà è vero il contrario: grazie a Mac OS X Sibelius 2 può lavorare tranquillamente in tutte le situazioni e può anche continuare a lavorare con puntualità mentre si scarica la posta, si stampa un file o si fa qualunque altra cosa. Il sistema operativo non permette ai programmi di prendere il controllo, ma provvede a fare sì che ogni programma abbia le risorse che gli servono.
Un programma musicale, con Mac OS X, non perde mai il ritmo.

<link>Lucio Bragagnolo</link>lux@mac.com