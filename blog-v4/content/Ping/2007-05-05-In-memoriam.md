---
title: "In memoriam"
date: 2007-05-05
draft: false
tags: ["ping"]
---

Anni fa, quando si diceva che la quota di mercato di Mac era al cinque percento, piovevano articoli quotidiani per spiegare che era troppo poco.

Ora pare che questa benedetta quota di mercato sia tornata al cinque percento, tutti glorificano le magnifiche sorti e progressive di Apple, ci sono analisti che fissano tetti astronomici al valore delle azioni, la gente affolla i negozi, i Mac vanno via come il pane&#8230; e nessuno si azzarda a dire che il cinque percento è troppo poco.

Non è che la nozione iniziale era un po' fallata e finalmente anche i cronisti imbecilli lo hanno capito?

P.S.: no. Si chiudesse un trimestre con Apple che perde un centesimo di dollaro, ripartirebbero tutti. :)