---
title: "Il trend positivo"
date: 2004-02-02
draft: false
tags: ["ping"]
---

Il segreto dietro il buon andamento attuale di Apple

Si vendono più Macintosh rispetto a qualche tempo fa. I prodotti ricevono tutti buona accoglienza e le iniziative software come iTunes Music Store viaggiano tutte bene, se non meglio (il Music Store è leader assoluto nella distribuzione legale di musica).

Qual è il segreto? Il filo conduttore.

A fine anni Ottanta era il desktop publishing. A metà anni Novanta era il video, con l’affermazione di QuickTime. Oggi è l’audio, con iTunes e iPod ma anche e soprattutto SoundTrack di Final Cut Pro e GarageBand di iLife.

Apple ha saputo intuire, per prima e meglio degli altri come accade spesso, una domanda da parte del mercato. E ha risposto bene. Naturale che arrivino i risultati.

<link>Lucio Bragagnolo</Link>lux@mac.com