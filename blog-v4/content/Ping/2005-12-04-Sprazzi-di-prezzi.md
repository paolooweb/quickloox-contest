---
title: "Sprazzi di prezzi<p>"
date: 2005-12-04
draft: false
tags: ["ping"]
---

Un parere, se anche non fosse documentato, sicuramente istruito<p>

<strong>Flavio</strong> è un ricercatore scientifico che lavora per enti dei cui al massimo io posso leggere sui libri di fantascienza e il suo intervento mi fa pensare che forse sul discorso prezzi-dei-Mac non ho detto troppe sciocchezze. Eccolo:<p>

<cite>Quando ebbi i soldi per comprare un computer portatile aggiornato al periodo e alla tasca, correva l&rsquo;anno 1997. Ero un felice possessore di un Compaq 386 Lite alla cui memoria sono ancora affezionato, ma l&rsquo;avevo comprato perché nel 1993 non potevo permettermi l&rsquo;Apple di turno.</cite><p>

<cite>Allora, febbraio-marzo 1997, iniziai una peregrinazione fra i marchi e i modelli migliori, tenendo presente quello che mi interessava avere come accessori e la buona qualità a cui non volevo rinunziare. La scelta affettiva ma anche e chiaramente monetaria fu per il PowerBook 3400. Costava circa 500 mila lire meno della migliore concorrenza, avendo già di serie gli accessori per me necessari all&rsquo;epoca.</cite><p>

<cite>Da allora sono passati otto anni, quasi nove, ma la maggior parte della gente continua a vedere Apple come un marchio caro, economicamente non competitivo con il mondo Pc che, se costa meno, è perché offre tecnologia obsoleta, pesi ridicoli, qualità generale non all&rsquo;altezza, oppure meno software. Ma la scheda audio o video, secondo la gente, non valgono nulla? Poi magari si arrabattano per installare altro hardware che va a incasinarsi col sistema operativo e salta fuori la favola che per usare i computer bisogna essere dei maghi.</cite><p>

<cite>Stessa cosa per il software che vanno a installare.</cite><p>

<cite>Ma questa è la realtà, com&rsquo;è la realtà di un &ldquo;esperto&rdquo; software il quale mi dice (convinto) che lui mai lavorerà con sistemi chiusi come il Mac e allora lavora su Windows (non su Linux o Unix come mi aveva fatto pensare).</cite><p>

Windows, che è rimasto l&rsquo;unico sistema operativo proprietario e chiuso, pieno di buchi e progettato nell&rsquo;interfaccia utente da un enigmista pentito. E c&rsquo;è chi è contento di trovarlo sul computer. Pensa di risparmiare.<p>

 <a href="mailto:lux@mac.com">Lucio Bragagnolo</a>