---
title: "Quel ramo del lago di Garda"
date: 2006-06-24
draft: false
tags: ["ping"]
---

Il ramo Macintosh.

Sono partito in ritardo, ho fatto due ore di coda in autostrada, mi sono perso a Peschiera ma sono finalmente arrivato a Bardolino e scrivo da Villa Carrara Bottagisio, splendida sede del <a href="http://www.macbardolino.com/" target="_blank">MacBar 2006</a>. Se la domenica non &egrave; ancora allocata, la gita vale decisamente la pena, grazie alla presenza di reperti straordinari provenienti dall&rsquo;<a href="http://www.allaboutapple.com/present/sede.htm" target="_blank">All About Apple Museum</a>, i giocatori pazzi (nel senso buono) di Devils Games, i <a href="http://www.protoolers.com/" target="_blank">ProToolers</a> all&rsquo;avanguardia dell&rsquo;editing musicale, la comunit&agrave; italiana dello sviluppo web su <a href="http://www.rapidweaver.it/" target="_blank">RapidWeaver</a>, i creatori di mondi tridimensionali di <a href="http://www.c4dzone.com/" target="_blank">Cinema 4D</a> e molto altro.

Lo staff &egrave; eccezionale per entusiasmo e disponibilit&agrave;. La cornice &egrave; incantevole e non si potrebbe davvero cominciare meglio l&rsquo;estate. C&rsquo;&egrave; anche da mangiare all&rsquo;aperto e, se proprio si cambia idea, c&rsquo;&egrave; Gardaland a due passi. Per stasera e domani, tuttavia, mi diverto di pi&ugrave; qui. :)