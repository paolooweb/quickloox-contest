---
title: "Mani a posto"
date: 2006-10-19
draft: false
tags: ["ping"]
---

Quest'uomo <a href="http://www.youtube.com/watch?v=NMi4Aw2QQPw" target="_blank">sta usando un Mac</a>.

Certo, non proprio in quel momento l&#236;. Ma è uno che sa certamente dove mettere le mani anche quando trattasi di computer.

Grande, <a href="http://www.mac-community.it/" target="_blank">Alessio</a>!