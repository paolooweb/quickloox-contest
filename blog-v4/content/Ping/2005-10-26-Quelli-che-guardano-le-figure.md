---
title: "Quelli che guardano le figure<p>"
date: 2005-10-26
draft: false
tags: ["ping"]
---

Qual è la linea di confine tra quelli che leggono e quelli che guardano?<p>

Scrivo queste note dalla sede Apple di Cologno Monzese, a due passi da Milano e un piano sotto la Disney.<p>

Ho letto di critiche ai nuovi PowerBook con risoluzione ulteriormente potenziata (il 17&rdquo;, per esempio, è passato da 1.440 x 960 a 1.680 x 1.050, 36 percento in più di pixel, 46 percento in più di luminosità, 22 percento in più di batteria, prezzo ridotto). Con un sistema operativo ancora legato in parte a misure assolute, come è Mac OS X, certe finestre di dialogo appaiono più piccole, con elementi più piccoli, perché le loro dimensioni  sono fisse, in pixel, e i pixel sono più densi, un maggior numero a parità di superficie.<p>

Anche il testo diventa più piccino. Leggere qualcosa in corpo 12 inizia a richiedere una buona vista, oppure&hellip; un corpo maggiore.<p>

Poi però vedi, sullo stesso schermo, una demo di Aperture, il nuovo software di catalogazione fotografica studiato da Apple per i professionisti, e ti accorgi che di pixel non ce ne sono veramente mai abbastanza.<p>

Qual è la linea di compromesso che consente a tutti di non lamentarsi, chi il testo, chi le immagini?<p>

Ognuno avrò la sua, suppongo. Nel dubbio, dammi retta. Più pixel è meglio. Poi si vedrà. E nel tempo Mac OS X si farà più furbo anche su queste cose.<p>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>