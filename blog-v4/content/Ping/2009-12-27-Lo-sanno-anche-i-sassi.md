---
title: "Lo sanno anche i sassi"
date: 2009-12-27
draft: false
tags: ["ping"]
---

Tempo fa mi è stato chiesto il perché delle <cite>sassate</cite> contro Microsoft. Il bello è che avevo unicamente riportato fatti.

Il sito Groklaw cerca trascrittori che diano una mano a digitalizzare una ampia <a href="http://www.groklaw.net/staticpages/index.php?page=2007021720190018" target="_blank">messe di documenti</a> relativi al processo di un paio di anni fa noto negli Stati Uniti come <i>Comes contro Microsoft</i>.

Durante il dibattimento divennero pubblici – e legalmente pubblicabili – innumerevoli documenti interni di Microsoft, diciamo con un grosso eufemismo a dire poco imbarazzanti per il tipo di politiche interne dell’azienda e delle intenzioni che animano il suo agire.

I documenti finirono online e Microsoft pagò una grossa cifra perché il sito che li pubblicava venisse chiuso. Disgraziatamente, qualcun altro li aveva già (legalmente) duplicati.

La mole di testo è immensa. Qua e là circolano gemme assolutamente esplicative, come quella che riporto (sintetizzata) qui sotto, relativa alla testimonianza dell’esperto informatico Ronald Alepin. <a href="http://www.groklaw.net/article.php?story=20070108020408557" target="_blank">Domande e risposte</a> si riferiscono a una mail scritta da Bill Gates a dirigenti Microsoft, nella quale si spiega che Java è un problema e si suggerisce di combatterlo <cite>supportandolo ed estendendolo</cite> alla maniera di Microsoft/Windows:

Avvocato: <cite>che cosa significa</cite> <cite>embrace (imbracciare, adottare, accogliere), detto da Microsoft, in questo contesto?</cite>

Alepin: <cite>una strategia per cui Microsoft adotta gli standard di un’altra azienda.</cite>

Avvocato: <cite>e che cosa significa</cite> extend <cite>(estendere)?</cite>

Alepin: <cite>una volta adottato lo standard, Microsoft lo estende aggiungendo tecnologia proprietaria.</cite>

Avvocato: <cite>qual è l’impatto tecnologico di questa strategia?</cite>

Alepin: <cite>essenzialmente, Microsoft si appropria di ciò che prima era a disposizione della comunità degli sviluppatori. E le sue tecnologie proprietarie non sono a disposizione.</cite>

Avvocato: <cite>qual è l’impatto sulla capacità di creare prodotti da parte di aziende diverse da Microsoft?</cite>

Alepin: <cite>riduce la loro capacità di creare prodotti e specialmente prodotti interoperabili con quelli Microsoft.</cite>

Così Microsoft ha cercato di uccidere Java: adottando lo standard ed estendendolo in modo che le estensioni su Windows fossero indisponibili altrove. Mentre l’idea di Sun era che che il codice Java potesse girare ovunque senza modifiche.

Che morisse Java era un problema nostro, oltre che di Sun. Significava applicazioni in meno, compatibilità in meno, versatilità in meno. Di cui avremmo dovuto “ringraziare” Microsoft, se il piano avesse avuto successo.