---
title: "Il troppo poco stroppia"
date: 2008-12-05
draft: false
tags: ["ping"]
---

Mi fa specie già riassumere la storia.

Una nota tecnica scritta evidentemente da un programmatore, gente che svolge un altro mestiere, spiega una cosa che dovrebbe essere ovvia e cioè che se uno vuole usare un antivirus è meglio che ne usi più di uno.

La nota è stata scritta a giugno 2007, diciotto mesi fa. Da diciotto mesi a questa parte, in fatto di virus per Mac, non è successo assolutamente niente.

Giorni fa qualcuno ha aggiornato la nota, probabilmente solo per le versioni dei prodotti citati.

Qualcun altro, probabilmente in base a un meccanismo automatico che lo avverte quando si aggiorna una nota, ha titolato che Apple raccomanda gli antivirus.

Qualcuno altro ancora ha tirato le conclusioni e ha deciso che, vista l'esistenza della nota tecnica (di diciotto mesi fa e se ne è accorto ieri), l'epoca dell'oro era finita e che adesso esistono virus per Mac.

Chi ha teorizzato che esistono virus ma li tengono nascosti (e chi li prende che fine fa, lo chiudono in una Time Capsule?).

Alla fine di questa catena indicibile di incompetenti, sciacalli e gente da poco, disposta a scrivere roba da poco in cambio di poco, c'è gente che insiste a leggere roba da poco e che si accontenta di poco. Poi magari ci crede pure e si mette a chiedere consigli per l'antivirus da installare. Per legittimo dubbio di sicurezza? No, per avere letto roba da poco scritta da gente da poco.

Apple ha fatto la cosa giusta e ha cancellato la nota. Se fosse corretta la logica degli stupidi bisognerebbe dedurne che i virus adesso sono spariti&#8230;

È troppo chiedere di smettere di credere alla prima scemenza scritta dal primo che capita? A me sembra poco.