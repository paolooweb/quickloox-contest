---
title: "Mancanze da colmare"
date: 2009-11-27
draft: false
tags: ["ping"]
---

Complimenti a Infoworld, che il 27 novembre <a href="http://www.infoworld.com/d/networking/apple-updates-xsan-software-21-884?source=rss_infoworld_news" target="_blank">ha pubblicato la notizia di Xsan 2.1</a>, aggiornamento al filesystem ad alte prestazioni venduto da Apple.

Due piccole precisazioni.

L'aggiornamento a Xsan 2.1 <a href="http://support.apple.com/kb/DL44" target="_blank">è uscito il 12 giugno 2008</a>, diciassette mesi fa.

Xsan viene venduto alla versione 2.2 e l'aggiornamento a Xsan 2.2 <a href="http://support.apple.com/kb/DL915" target="_blank">è uscito il 14 settembre 2009</a>, due mesi e mezzo fa.

Forse mancavano notizie. O più probabilmente fosforo.