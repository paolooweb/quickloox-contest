---
title: "Che meraviglia, il pollo"
date: 2007-04-25
draft: false
tags: ["ping"]
---

Ho appena finito di localizzare un piccolo tutorial per Macworld giugno riguardante il controllo remoto e sicuro di un Mac usando ssh e <a href="http://sf.net/projects/cotvnc/" target="_blank">Chicken of the Vnc</a>.

Sono estasiato. La teoria la sapevo tutta, ma vederti effettivamente davanti agli occhi lo schermo del Mac che sta a dieci metri (o diecimila chilometri) di distanza provoca ancora meraviglia.