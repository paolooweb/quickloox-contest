---
title: "Là comando io"
date: 2007-07-21
draft: false
tags: ["ping"]
---

Avevo bisogno del controllo remoto delll'iMac di casa. Non l'avevo mai fatto. Mi sono applicato (significa: ho guardato Internet, dove c'è tutto) e adesso ho ben due modi per farlo: <a href="http://hamachix.spaceants.net/" target="_blank">Hamachi</a> e <a href="https://secure.logmein.com/home.asp?lang=en" target="_blank">LogMeIn</a>. Il secondo, in particolare, è veramente a prova di chiunque. E tutto funziona anche quando c'è di mezzo FastWeb.

L'esperienza ha un sapore di magia tale che Harry Potter non è nessuno.