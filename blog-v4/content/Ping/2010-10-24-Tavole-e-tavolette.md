---
title: "Tavole e tavolette"
date: 2010-10-24
draft: false
tags: ["ping"]
---

Il sottoscritto comparirà a Lucca Comics venerd&#236; 29 ottobre a Palazzo Ducale, ore 17:30, come partecipante al dibattito <i>iPad, ultima frontiera: la sfida della progettazione - Ambienti digitali e nuove piattaforme. Scenari dal fumetto del futuro</i>, organizzato nell'ambito dei <a href="http://lucca2010.luccacomicsandgames.com/index.php?id=580" target="_blank">Comics Talks - Idee attorno al fumetto contemporaneo</a>.

Modererà il bravo e amico <a href="http://antoniodini.blogspot.com/" target="_blank">Antonio Dini</a>, esperto di nuove tecnologie per Il Sole 24 Ore.

Assieme a me compariranno anche <a href="http://lrnz.blogspot.com/" target="_blank">Lorenzo <i>Lrnz</i> Ceccotti</a>, arti director e autore; <a href="http://www.mutado.com/#/home/" target="_blank">Lorenzo Manfredi</a>, progettista della <i>app</i> Disney DigiComics; <a href="http://prontoallaresa.blogspot.com/" target="_blank">Roberto Recchioni</a>, autore, <i>blogger</i> e videogiocatore e <a href="http://www.enhancedpress.com/" target="_blank">Alessandro Risuleo</a>, progettista delle <i>app</i> Tunué e delle <i>app</i> Sturmtruppen Book.

Insomma, mi hanno infilato in una roba grossa e cercherò di fare del mio meglio.

È scontato che, fatta sera, sempre il sottoscritto finisca davanti a una pizza, non so quanto &#8211; ma probabilmente &#8211; vincolato da chi ospita, da facce conosciute, da colleghi e quant'altro. Fatti salvi i vincoli (quelli di Lucca Comics comunque, arte fumetti e giochi), tuttavia, sarò lietissimo di condividere la tavolata con chiunque fosse della zona o di l&#236; passasse.

Nel caso, la mia <a href="mailto:lux@mac.com?subject=Sono%20anch'io%20a%20Lucca%20Comics!">email</a> è nota. :-)