---
title: "Come in un film"
date: 2007-05-03
draft: false
tags: ["ping"]
---

Ricordi di ponte.

Improvvisamente l'autostrada si riempie di luci lampeggianti e tutto si ferma, il traffico in primo luogo.

Qualche minuto e c'è chi spegne il motore, poi anche le luci. Scende dall'auto a guardare. Passerà del tempo.

Per altri, più moderni ed evoluti, si tratta di cosa scontata. Per me, che viaggio su un furgonato con duecentomila chilometri sul groppone, sono miracoli.

La fidanzata ha tirato fuori il PowerBook 12&#8221; e un Dvd. L'abbiamo appoggiato sul cruscotto e vai di Nuovo Cinema Monovolume.