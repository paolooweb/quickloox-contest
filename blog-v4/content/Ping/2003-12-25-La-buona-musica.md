---
title: "La buona musica"
date: 2003-12-25
draft: false
tags: ["ping"]
---

A melodia armonia e ritmo si aggiungono altri criteri

Per Natale mi hanno regalato, a me graditissimo, il Cd audio del nuovo album dei Kraftwerk, Tour de France Soundtracks.

Sul frontale della custodia spicca un adesivo “Copy Controlled”. Sul retro un riquadro informativo spiega che il disco è compatibile con lettori Cd e con computer, Windows e Mac (Mac OS da 8.6 in su con CarbonLib e Mac OS X).

Il Cd si legge perfettamente nei Mac di casa mia e iTunes ha rippato alla perfezione la title track, che voglio sempre con me nel mio portatile.

Il Cd contiene inoltre un player software, compatibile Mac, che consente di ascoltare i brani anche in assenza di altro software abilitato.

Questa sì, che è musica.

<link>Lucio Bragagnolo</link>lux@mac.com