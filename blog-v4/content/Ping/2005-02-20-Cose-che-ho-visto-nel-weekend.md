---
title: "Cose che ho visto nel weekend<p>"
date: 2005-02-20
draft: false
tags: ["ping"]
---

Dall&rsquo;esperienza derivano i giudizi. Ecco l&rsquo;esperienza<p>

Ho visto disegnare con Photoshop CS su una tavoletta grafica da 17&rdquo; collegata a un Mac mini modello base con 256 mega di Ram e il programma non ha perso neanche un colpo nel seguire le pennellate.<p>

Ho visto giocare a World of Warcraft con un PowerBook 12&rdquo; attaccato a un Cinema Display 23&rdquo; e mi si è mozzato il fiato di fronte alla grafica, all&rsquo;atmosfera, alla cura dei dettagli, al piacere di giocare e interagire con decine di persone da qualsiasi angolo del mondo.<p>

Ho visto bloccarsi l&rsquo;interfaccia grafica di un Titanium. Ma il Titanium aveva il login remoto attivato e da un iMac sulla stessa rete è stato possibile controllarlo con ssh e risolvere il problema senza bisogno di riavviare (Riccardo: è un altro episodio).<p>

Ho visto usare Pages da una persona inesperta che nel giro di mezz&rsquo;ora stava impaginando la sua tesina con grande piacere, importando immagini direttamente dal browser nel suo documento.<p>

In questo momento il mondo Apple è veramente degno di essere vissuto.<p>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>