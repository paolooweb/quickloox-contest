---
title: "Comp&#236;ti per le vacanze"
date: 2006-07-17
draft: false
tags: ["ping"]
---

Agosto è quasi peggio dell'anno nuovo, quanto a propositi. Riuscissi a fare la metà di quello che mi sono prefisso, mi darebbero un premio Nobel e prorogherebbero il mese di altri venti giorni per farci stare dentro più roba.

Ma sono un caso particolare (anche un po' clinico). Una persona normale in agosto vuole riposarsi&#8230; e comunque fare qualcosa di diverso, non troppo pesante.

Un'idea banale: dedicare qualche ora al software open source.

I modi sono mille. Per esempio, tradurre in italiano un pezzo di programma o di documentazione di programma. Oppure inviare un po' di feedback agli autori per migliorare il software. Oppure, semplicemente, scaricare due o tre programmi open source non strettamente necessari e giocarci un po', farsi un'idea, scoprire utilizzi nuovi di quei settantanove (sul mio 17&#8221;) tasti.

Scoprire mondi nuovi, imparare qualcosa, aiutare una comunità, fare una cosa buona anche se non ce ne viene niente in tasca. Se non è vacanza questa, che cos'è?

Tanto per fare un esempio concreto per quanto banale, provare a sentire il coordinatore italiano della localizzazione di <a href="http://www.wesnoth.org" target="_blank">Battle for Wesnoth</a> per sentire se c'è qualcosa da fare. Ma è una goccia nel mare.