---
title: "Complimenti a Welch"
date: 2007-03-13
draft: false
tags: ["ping"]
---

Ho letto la <a href="http://www.informationweek.com/software/showArticle.jhtml;?articleID=197700391&amp;pgno=2&amp;queryText=" target="_blank">top 22 di John Welch</a> riguardante il miglior software esistente per Mac.

Per metà è roba ovvia, per metà è roba che serve a lui e poi c'è <a href="http://lingon.sourceforge.net/" target="_blank">Lingon</a>, che ignoravo del tutto e sembra mooolto interessante.

Trovo personalmente poco stimolante fare la lista dei programmi preferiti. Al massimo posso raccontare di qualche episodio in cui una delle mille interfacce viste mi ha regalato un momento di empatia. Però, se ci si mette Welch, qualcosa di buono salta fuori. E complimenti.