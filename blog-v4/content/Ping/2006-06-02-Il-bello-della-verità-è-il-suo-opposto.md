---
title: "Il bello della verit&agrave; &egrave; il suo opposto?"
date: 2006-06-02
draft: false
tags: ["ping"]
---

&Egrave; ci&ograve; che mi ha chiesto (molti giorni fa, ma &egrave; tutta colpa mia) <strong>Vittorio</strong> dopo essersi imbattuto in questo <a href="http://vippoblog.blogspot.com/2006/05/non-preoccupiamoci.html" target="_blank">post</a>. Il titolo dell&rsquo;articolo originale, ha aggiunto Vittorio, &egrave; <em>Windows morde la mela</em>. Come se non fosse, e arriviamo alla domanda, l&rsquo;esatto contrario.

Ci si chiede se mai nella storia potremo vedere un titolo sulla mela che morde Windows, senza la genuflessione obbligatoria a santa Microsoft.