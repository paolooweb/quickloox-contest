---
title: "Crescita percentuale"
date: 2010-05-08
draft: false
tags: ["ping"]
---

Ho scritto nell'ultimo mese oltre trecentomila caratteri in testo in formato Html.

Amo pormi domande inutili e mi sono chiesto quanto &#8220;ingombrasse&#8221; l'Html, cioè di quanto si gonfiasse il conteggio dei caratteri.

La risposta è una media del 12-13 percento, con picchi occasionali del 15 percento.

Intendo testo accatiemmellizzato (grassetti, corsivi, link a immagini, titoli e sottitoli) e non pagine web impostate graficamente, dove il dato percentuale può salire di molto.