---
title: "Quando si dice backup<p>"
date: 2005-01-17
draft: false
tags: ["ping"]
---

Una storia a lieto fine, grazie alla tecnologia dei poveri<p>

Spesso uno crede che organizzarsi i backup richieda un programma apposta e procedure complicate. Invece.<p>

Tempo fa, causa un lavoro imprevisto soprattutto negli ingombri, ho dovuto liberare un bel po&rsquo; di spazio su disco e farlo dalla sera alla mattina.<p>

Tra le altre cose ho esportato come testo Ascii circa due giga di posta in uscita e li ho salvati su un Dvd.<p>

Mi scrive Mario e mi chiede se ho ancora in archivio quella certa mail che ho scritto tempo fa su Internet e democrazia.<p>

Ne ho un vago ricordo, abbastanza da lanciare BBEdit e fargli cercare una parola chiave sul Dvd.<p>

Primo tentativo, niente; seconda parola chiave, bingo. Eccola la mail, del 12 ottobre. Non vecchissima, ma neanche attuale.<p>

Alle volte la tecnologia più efficace è quella più povera, la più semplice.<p>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>