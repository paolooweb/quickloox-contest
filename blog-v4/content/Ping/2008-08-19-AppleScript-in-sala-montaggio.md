---
title: "AppleScript in sala montaggio"
date: 2008-08-19
draft: false
tags: ["ping"]
---

Forse non è totalmente noto che AppleScript, per chi se la sente, supplisce all'acquisto di QuickTime Pro. Programmazione in cambio di denaro sembra equo.

Posto allora che si abbia nel sistema qualcosa che mostra un file video Avi, se necessario installando <a href="http://www.divx.com" target="_blank">DivX</a>, possiamo usare AppleScript per effettuare una conversione da Avi a H.264, che altrimenti sarebbe possibile solo con QuickTime Pro.

Prima di tutto si possono registrare i parametri di esportazione Mpeg4, cos&#236; da usarli in altri script:

<code>tell application "QuickTime Player"
    tell first movie
        save export settings for MPEG4 to file "Users:nomeutente:MPEG4 Settings"
    end tell
end tell</code>

Fuori dall'interfaccia grafica la cartella Utenti si chiama sempre Users, anche se usiamo Mac OS X in italiano. <em>nomeutente</em> è il nostro nome utente, per registrare nella cartella Inizio. Non importa in realtà dove registriamo, basta sapere dove lo facciamo. Si noti, anche, che dentro AppleScript il separatore nei percorsi non è la barra, come nel Terminale, ma i due punti, come usava in Mac OS classico.

E adesso, anche grazie ai parametri appena registrati, convertiamo un file .avi in un file .mp4:

<code>tell application "QuickTime Player"
    activate
    close every window
end tell
tell application "QuickTime Player"
    open "percorso:filmato.avi"
    if (can export front movie as MPEG4) then
        set durata to duration of front movie
        with timeout of durata seconds
            export front movie to ("percorso:nuovofilmato.mp4") as MPEG4 using settings file "Users:nomeutente:MPEG4 Settings"
        end timeout
    end if
end tell
quit application "Quicktime Player"
</code>

Leggere l'inglese è difficile, ma programmare in AppleScript lo è di più. Questo è praticamente inglese da leggere e dunque basta leggerlo per capire come esportare filmati .avi in formato Mpeg4. Anzi, per esportare basta copiarsi gli script&#8230;

La competenza per questo <em>post</em> l'ha messa tutta <a href="http://skettle.com/2007/11/19/applescript-code-for-converting-an-avi-to-an-mp4/" target="_blank">Skettle.com</a>.