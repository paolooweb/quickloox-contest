---
title: "Magazzino open source"
date: 2006-04-16
draft: false
tags: ["ping"]
---

Un notevole grazie a <strong>Giuseppe Cannas</strong> per la segnalazione di questo <a href="http://osx.hyperjeff.net/Apps/apps.php?sub=100&amp;w=4" target="_blank">archivio di software open source per Mac OS X</a>, che ho anche aggiunto ai link stabili del blog. :-)