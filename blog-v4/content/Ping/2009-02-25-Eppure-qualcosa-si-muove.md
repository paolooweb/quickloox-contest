---
title: "Eppure qualcosa si muove"
date: 2009-02-25
draft: false
tags: ["ping"]
---

Per ora ho rinunciato ai miei tentativi di copiare un file superiore ai quattro gigabyte su iDisk.

Ciò non toglie che per tagli più piccoli iDisk, con i suoi limiti di tempo, funzioni. Ricevo da <strong>Andrea</strong>:

<cite>A seguito del tuo tentativo di trasferimento file sovradimensionato, ti mando le mie statistiche relative al trasferimento di cinque file effettuato stanotte (nessuno però oltre il limite teorico di iDisk): i cinque complessivamente pesavano 1,2 gigabyte, il più grande 0,6 gigabyte, il più piccolo 70 megabyte (erano tutti Pdf).</cite>

<cite>L'</cite>upload ha<cite> richiesto 7,5 ore (00:44 inizio, 8:16 fine). La velocità media è stata di 45/46 kilobit per secondo (su un massimo teorico di 384 kilobit per secondo di Alice); Il trasferimento è avvenuto senza problemi (io dormivo e stamattina accedendo a MobileMe ho ritrovato tutti i file).</cite>

<cite>adesso il mio iDisk pesa cinque gigabyte (io trovo miglioramenti incredibili rispetto alla precedente versione, quando non avrei nemmeno tentato un</cite> <cite>upload simile).</cite>

Venti gigabyte a disposizione, per un servizio <em>online</em>, non sono pochi e sfruttarli bene può rivelarsi molto produttivo.