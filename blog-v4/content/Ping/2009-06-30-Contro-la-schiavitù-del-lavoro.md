---
title: "Contro la schiavitù del lavoro"
date: 2009-06-30
draft: false
tags: ["ping"]
---

Sappia, la nutrita comunità di amanti di <a href="http://travian.it/" target="_blank">Travian</a>, che hanno creato un bot capace di giocare al posto loro, VilloNanny.

Mentre loro oziano, <a href="http://www.villonanny.net/" target="_blank">VilloNanny</a> coltiva, costruisce, suda, si affanna e produce.

Un po' come per le macro in World of Warcraft, non capisco bene se sono cose che fanno venire ancora più voglia di giocare, liberando dai compiti più ripetitivi, oppure se constatata l'impossibilità di competere ad armi pari con un robot non sia il caso di lasciare perdere (in confidenza: per ora faccio a meno delle macro e gioco).