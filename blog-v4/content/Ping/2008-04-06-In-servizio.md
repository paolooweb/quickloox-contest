---
title: "In servizio"
date: 2008-04-06
draft: false
tags: ["ping"]
---

Prima di rientrare dall'assemblea annuale del <a href="http://www.giovanninoguareschi.com/23club2.htm" target="_blank">Club dei Ventitré</a>, ho dato un'occhiata all'iMac 20&#8221; che il Club ha comprato lo scorso ottobre.

L'iMac è diventato la macchina principale di appoggio per gli archivisti, che stanno catalogando 200 mila documenti e oltre undicimila tra foto e disegni attinenti alla produzione di Giovannino Guareschi.

Ci sono attaccati uno scanner A3 e una stampante A3, entrambi Epson. La macchina viene usata anche per la masterizzazione dei Dvd di backup e consultazione <em>offline</em>.

L'eMac precedente veniva usato solo per acquisire scansioni di foto e il resto era interamente affidato ai Pc. I ruoli stanno cambiando.