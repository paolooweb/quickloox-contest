---
title: "Partizionare è un po' mirare"
date: 2007-07-29
draft: false
tags: ["ping"]
---

Ho avuto una chiacchierata con <strong>Frank</strong> rispetto ai dischi esterni per Mac PowerPc e Mac Intel.

La pratica era la sua, che cercava di preparare un disco esterno in modo che facesse partire il suo MacBook. La teoria era la mia, che guardavo su Internet per capirne di più.

L'esito finale mi ha lasciato un po' confuso. Se non dico castronate:

<ul type="disc">
	<li>un disco dati non ha problemi e viene riconosciuto sia da PowerPc che da Intel</li>
	<li>un disco di boot PowerPc verrà riconosciuto da Intel, ma solo come disco dati e non per fare il boot</li>
	<li>un disco di boot Intel deve per forza essere partizionato con lo schema Gpt, come da Opzioni in Utility Disco, e non viene riconosciuto da PowerPc neanche come disco dati</li>
	<li>per fare il boot con lo stesso disco esterno sia su PowerPc che su Intel, il disco deve avere due partizioni e bisogna fare un po' di <a href="http://theappleblog.com/2006/02/28/booting-an-intel-mac-from-an-apm-partitioned-disk/" target="_blank">giochi di prestigio</a></li>
</ul>

Se qualcuno vuole correggere e integrare, mi sa che ce n'è bisogno. In tempi di transizione bisogna avere più chiaro di una volta quale sia il bersaglio.