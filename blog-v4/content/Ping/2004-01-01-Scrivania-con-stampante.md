---
title: "Scrivania con stampante"
date: 2004-01-01
draft: false
tags: ["ping"]
---

Un grande ritorno grazie a Mac OS X 10.3.2

Tanti, passando da Mac OS 9 a Mac OS X, avevano rimpianto la mancanza delle stampanti di scrivania. Si riusciva a ottenere qualcosa di simile trascinando un documento sopra l’icona dell’Utility di configurazione stampante (Printer Setup Utility), ma onestamente non era un gran che.

Con Mac OS X 10.3.2 si apre l’utility suddetta, si seleziona la stampante, si sceglie Crea la Stampante Scrivania dal menu Stampanti (Comando-Maiuscolo-d) et voilà, ecco tornata la stampante sulla scrivania.

Dal momento poi che Exposé permette di visualizzare la scrivania in un attimo anche se ricoperta da mille finestre, si può dire davvero che ora si sta meglio di prima anche da questo punto di vista.

<link>Lucio Bragagnolo</link>lux@mac.com