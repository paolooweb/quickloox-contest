---
title: "Che errore stare separati"
date: 2002-02-16
draft: false
tags: ["ping"]
---

Mettere in competizione l’hardware contro il software non è una buona idea

Uno dei motivi scatenanti di una delle crisi di Apple, anni fa, fu la separazione dell’hardware e del software Macintosh in due “business unit” distinte, ognuna delle quali trattava l’altra né più né meno come un cliente. La competizione tre le due unit portò alla rinuncia a qualsiasi rischio innovativo e alla stagnazione dia dell’hardware che del software Apple.
L’autore della decisione oggi è a capo della divisione software di Palm, che è stata separata dall’hardware. L’iniziativa dovrebbe anche favorire l’adozione di Palm OS da parte di altri costruttori.
Sarà un caso, ma Palm da tempo è in crisi e negli ultimi diciotto mesi il fatturato delle licenze di Palm OS è intorno al 5% del totale del business Palm. Noccioline.
Morale: le cattive idee prima o poi rispuntano sempre. E Palm non è capace di imparare dagli errori di Apple.

<link>Lucio Bragagnolo</link>lux@mac.com