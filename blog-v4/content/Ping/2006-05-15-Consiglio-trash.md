---
title: "Consiglio trash"
date: 2006-05-15
draft: false
tags: ["ping"]
---

Per buttare via uno o più file e liberare subito quello spazio disco senza passare dallo svuotamento del Cestino, li cancello da Terminale. Non c’è niente di difficile: digito

<code>rm</code>

con uno spazio davanti e trascino lì sopra le icone dei file da buttare. Un colpo di Invio, ed è fatto.

Consiglio veramente banalissimo, ma a volte sblocca situazioni.