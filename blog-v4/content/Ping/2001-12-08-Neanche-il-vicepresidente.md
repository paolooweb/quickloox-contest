---
title: "Neanche il vicepresidente"
date: 2001-12-08
draft: false
tags: ["ping"]
---

Una nota azienda informatica condannata in tribunale per abuso di posizione dominante manda un suo vicepresidente in una importante università italiana a tenere la lezione inaugurale di un corso sponsorizzato dall’azienda in questione.
Il vicepresidente, oratore mediocre, tiene una presentazione smaccatamente commerciale dove perfino lo sfondo delle schermate è quello dell’ultimo prodotto in vendita nei negozi, in cui tra varie perle viene prefigurato un futuro in cui avremo bisogno del computer per ricordare che cosa piace mangiare al nonno di famiglia.
Ma il bello è che, ad aula semivuota prima dell’inizio, il vicepresidente confabula con il tecnico e dice più o meno: “Vediamo se la presentazione funziona perché è da un po’ che il computer mi dà problemi”.
Il livello è questo: neanche il vicepresidente riesce a far funzionare i prodotti della sua azienda. Che azienda possa essere viene lasciato come esercizio al lettore.

lux@mac.com