---
title: "In carica per davvero"
date: 2006-02-11
draft: false
tags: ["ping"]
---

Ho ripensato allo scambio di battute con <strong>Daniele</strong> a proposito dei dati sullo stato di salute delle batterie&hellip; e l&rsquo;ho fatto davvero. Ho iniziato a caricare i dati che sono arrivati dentro un foglio di calcolo.

Chi ha voglia di contribuire, pu&ograve; farlo tranquillamente. Dati richiesti: mesi di vita della batteria, numero di cicli di carica (<em>loadcycle</em> o <em>cycle count</em>), percentuale di status (<em>capacity status</em> o <em>current capacity</em>).

Fino a che restiamo in pochi non servir&agrave; a molto. Ma se la massa dei dati fosse rilevante, si potrebbero trarre conclusioni molto concrete e interessanti. Ben pi&ugrave; oneste delle dichiarazioni di Apple, ben pi&ugrave; utili del vaniloquio di chi si lamenta e non sa perch&eacute;. Prover&ograve; a lanciare un appello anche sul sito del <a href="http://www.poc.it" target="_blank">Poc</a>.

Per Tiger c&rsquo;&egrave; <a href="http://www.coconut-flavour.com/coconutbattery/" target="_blank">Coconut Battery</a>. Per Panther c&rsquo;&egrave; <a href="http://www.branox.com/Battorox/Battorox.html" target="_blank">Battorox</a>, che non considera i mesi di vita, ma indica la data del computer. Bisogna calcolare a mano, per&ograve; mi sembra fattibile.

Una cosa da subito: se provi programmi diversi, <strong>potrebbero dare risultati leggermente diversi</strong>. Per una maggiore omogeneit&agrave; dei dati, sarebbe meglio usare strettamente Coconut Battery o Battorox.

Vediamo come va.