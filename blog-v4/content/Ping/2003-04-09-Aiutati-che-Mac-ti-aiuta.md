---
title: "Aiutati che il Mac ti aiuta"
date: 2003-04-09
draft: false
tags: ["ping"]
---

Chiamata a raccolta dei volonterosi di tutto il mondo (Mac)

Fino a System 6, che Mac avesse o meno un aiuto non interessava a nessuno.
System 7 aveva il Balloon Help, l’aiuto a fumetti, con qualche controindicazione ma tutto sommato abbastanza utile.

Mac OS 8 aveva un aiuto azzeccato, quello con i tutorial integrati che aprivano automaticamente i pannelli di controllo e cerchiava gli oggetti cruciali con un evidenziatore rosso.

Mac OS 9 ha iniziato l’era dell’aiuto attuale, travasato anche in Mac OS X. Non è che sia completamente inutile e contiene particolari interessanti. Per esempio il Visore Aiuto è in effetti un browser rudimentale e ci si possono fare trucchi simpatici.

Per il resto l’Aiuto Mac attuale è snervante. Le domande preconfezionate non risolvono nessun problema e dopo quattro minuti di esplorazione ci si trova invariabilmente davanti a un link che porta su Internet, in genere nel momento in cui manca la connessione.

Da non esperto, mi pare di ricordare che Apple metta a disposizione degli sviluppatori un kit per produrre supplementi all’Aiuto. Di sicuro - la sto guardando ora - chi ha installato i Developer Tools possiede sicuramente tutta la documentazione necessaria.

Mi chiedo: perché non si crea un gruppo di lavoro che produca aiuto di qualità per Macintosh? Che risponda a domande sensate, risolva problemi veri, metta tutte le numerose pezze che mancano?

Gente abbastanza determinata potrebbe creare qualcosa di qualità e forse arrivare a venderlo persino, e magari venderlo persino ad Apple. Apple non sarebbe peraltro l’ultima a guadagnarci, perché avremmo tutti un Aiuto Mac degno di questo nome.

Ecco, il sassolino è partito. Vediamo se fa onde.

<link>Lucio Bragagnolo</link>lux@mac.com