---
title: "Tasti tosti e mani a posto"
date: 2003-05-23
draft: false
tags: ["ping"]
---

Riflessioni pseudofilosofiche sulle interfacce e la posizione delle mani

Quando compi quarant’anni e usi il computer da venti inizi a porti le Grandi Domande sul computer, proprio come a diciotto ci si poneva le Grandi Domande sulla vita.

Ecco: il mouse. È impagabile nel rendere semplici le cose e ha portato il computer nelle case di chi altrimenti non lo avrebbe mai usato. Il suo prezzo: è lento e stacca una mano dalla tastiera. Non si vede ancora un suo sostituto serio e chi straparla di interfacce basate su penna ottica o vocali non ha palesemente mai intrapreso il mestiere di scrivere, né ha provato a lavorare per dieci o dodici ore tenendo costantemente una penna in mano.

La tastiera. È impagabile nel lasciare le mani libere e nella velocità dei comandi. Il prezzo: va imparata, se togli una mano sei fritto ma, più di ogni altra cosa, è limitata. Difficile immaginarsi tastiere molto diverse dalle attuali. Eppure il numero dei caratteri da rappresentare aumenta in continuazione, come quello dei comandi che si possono dare. Assegnare le scorciatoie di tastiera è come gestire il parcheggio dello stadio la domenica: troppo di tutto in troppo poco tempo e troppo poco spazio.

La soluzione? La tastiera ad accordi, forse. Difficile da apprendere ma estremamente potente. Ha solo cinque tasti, uno per dito, e tutto si ottiene da combinazioni dei tasti. Se ne vendeva una tempo fa, e con due tastiere, una per mano, si fanno miracoli. Ma l’ho persa di vista.

Però datemi una interfaccia utente perfettamente pilotabile, perché guai se bisogna spostare le mani.

<link>Lucio Bragagnolo</link>lux@mac.com