---
title: "Nostalgia canaglia"
date: 2008-07-23
draft: false
tags: ["ping"]
---

Solo venti dollari per portarsi a casa <a href="http://cgi.ebay.com/ws/eBayISAPI.dll?ViewItem&amp;item=160261945051#description" target="_blank">l'intera collezione di Macworld Usa</a>, dal primo numero a oggi.

Unici problemini, le spese di spedizione, che valgono undici volte e mezzo il prezzo base, e quei settanta chili di carta.

Scadenza stasera, nei dintorni della nostra mezzanotte.