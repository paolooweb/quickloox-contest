---
title: "Ritocca la Bicocca"
date: 2009-05-09
draft: false
tags: ["ping"]
---

Ho appena finito di raccontare della mia <a href="http://www.macworld.it/blogs/ping/wp-admin/post.php?action=edit&amp;post=2299" target="_self">esperienza all'università di Milano Bicocca</a> che leggo di che cosa accade nello Stato americano del Missouri.

La locale scuola di giornalismo, a partire dal prossimo anno accademico, <a href="http://www.columbiamissourian.com/stories/2009/05/07/school-journalism-requires-ipod-touch/" target="_blank">raccomanda ai nuovi iscritti di possedere un iPod touch o un iPhone</a>.

Non è un capriccio: alcune ricerche mostrano che assistere a una lezione due volte fa imparare tre volte di più che assistere a una lezione una volta sola e dunque la possibilità di rivedere le lezioni è fondamentale per apprendere. Alla State University di Fredonia (Stato di New York) hanno compiuto <a href="http://www.newscientist.com/article/dn16624-itunes-university-better-than-the-real-thing.html" target="_blank">un esperimento</a>: presi sessantaquattro studenti volontari, a metà di essi è stata tenuta una lezione tradizionale con distribuzione di una dispensa su carta. All'altra metà è stato distribuito il <em>podcast</em> video della lezione più la dispensa su carta.

Una settimana dopo gli studenti sono stati interrogati. Chi aveva studiato sul <em>podcast</em> aveva una media di 71 centesimi; gli altri di 62 centesimi. Chi aveva studiato solo sul <em>podcast</em>, ignorando la carta, ha registrato una media di 77 centesimi.

Entro l'estate la scuola di giornalismo del Missouri installerà un sistema automatico di registrazione delle lezioni e le lezioni stesse verranno ridiffuse mediante iTunes U.

I nuovi modi di fare didattica prefigurano nuove possibilità. iTunes U è a portata di qualunque computer personale e molti computer da tasca. La possibilità di vedere e rivedere le lezioni a piacere, fino a comprendere il massimo, è un progresso considerevole.

Ripenso all'aula in Bicocca dove se hai bisogno di una presa elettrica devi stare accanto alla porta di ingresso, o niente.