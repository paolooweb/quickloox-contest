---
title: "Ti spiego una cosa su iPod<p>"
date: 2005-10-15
draft: false
tags: ["ping"]
---

Come funziona la tecnologia per le persone normali<p>

Una cosa che non sapevo assolutamente fino a ieri è che alle Gallerie dell&rsquo;Accademia di Venezia, dal 24 Ottobre 2003 al 22 Febbraio 2004, si tenne una mostra dedicata a Zorzi da Castelfranco, altrimenti detto Giorgione, uno dei più grandi pittori del Rinascimento.<p>

La <a href="http://www.teleart.org">Teleart</a> di Venezia ebbe l&rsquo;idea, e si aggiudicò l&rsquo;incarico, di predisporre un servizio di percorso audioguidato. Utilizzò ottanta iPod, caricati con i percorsi in cinque lingue sotto forma di file Mp3. Gli iPod vennero usati da 35 mila visitatori della mostra.<p>

Gli iPod erano quelli di terza generazione, con la clickwheel e i quattro pulsanti sotto lo schermo. Per alcuni visitatori, specie quelli più anziani, risultarono troppo difficili da usare.<p>

Un sacco di gente si chiede come mai gli iPod vendono così tanto. Dopotutto costano più di altri modelli e contengono pochissime funzioni. Sul mercato ci sono aggeggi dotatissimi, che svolgono decine di funzioni, molte più di qualunque iPod.<p>

Invece Apple crea prodotti semplici.<p>

Ecco perché.<p>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>