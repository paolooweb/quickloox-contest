---
title: "Il succo di Safari 5"
date: 2010-06-14
draft: false
tags: ["ping"]
---

È che, oltre alle cose che si vedono, ce ne sono tantissime che non si vedono subito o proprio passano inosservate.

Le nuove estensioni permettono di <a href="http://www.madeincr.com/notes/unjustify-a-greasemonkey-script-for-safari-5/" target="_blank">cambiare giustificazione del testo in Reader</a> a chi non la gradisce com'è.

Se si ingrandisce la pagina, Safari 5 <a href="http://www.macosxhints.com/article.php?story=20100612150852739" target="_blank">mantiene il centro dell'immagine</a> dove stava, mentre Safari 4 costringeva al riposizionamento. Apprezzatissimo.

Soprattutto, un comando Annulla <a href="http://www.macosxhints.com/article.php?story=2010060915323366" target="_blank">permette di riaprire una scheda</a>, o un <i>tab</i> che dir si voglia, chiusa per errore. Per quanto mi riguarda trattasi di salvavita.

E vogliamo parlare di <a href="http://homepage.mac.com/drewthaler/jsblacklist/" target="_blank">JavaScript Blacklist</a>, che stoppa i comportamenti assurdi e seccanti come i noioissimi link a doppia sottolineatura verde che portano a pubblicità indesiderata e indesiderabile?