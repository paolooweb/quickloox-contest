---
title: "Garantito che è meglio AppleCare"
date: 2004-03-08
draft: false
tags: ["ping"]
---

La legge, la garanzia e le illusioni

Che cosa non fa la malainformazione.
Circola sempre più gente convinta che Apple sia costretta a fornire una garanzia di due anni in base a una direttiva europea, che invece racconta tutt’altre cose. Segnatamente, il secondo anno la garanzia la fornisce il rivenditore, non il costruttore, e tutto ciò non vale per chi acquista con partita Iva.

Detto ciò, entro un anno dall’acquisto si ha la possibilità di fare AppleCare. Che estende la garanzia fino a tre anni dall’acquisto, copre più di quello che copre la garanzia standard ed è pure detraibile per professionisti e aziende.

Una delle due: o la garanzia è importante, e allora fai AppleCare, che è un investimento assai modesto per la copertura che offre, o non te ne frega niente, e allora ricorda al rivenditore le sue responsabilità. Il resto è chiacchiericcio, buono per coprire pessima musica di sottofondo in pizzeria, e niente di più.

<link>Lucio Bragagnolo</link>lux@mac.com