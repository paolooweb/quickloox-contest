---
title: "Curve non pericolose<p>"
date: 2005-11-11
draft: false
tags: ["ping"]
---

Un particolare atteggiamento da frequentatore dei forum<p>

I problemi accadono e ogni tanto anche i Mac hanno problemi. È interessante l&rsquo;atteggiamento di chi ha un problema e decide di parlarne su un forum.<p>

È l&rsquo;applicazione dei principio mal comune mezzo gaudio. Sapere che altri hanno lo stesso problema, a parte la possibilità che qualcuno possieda la soluzione, se non altro consola.<p>

Ma non finisce qui. Si instaura un senso di appartenenza elitaria e, contemporaneamente, l&rsquo;idea che il problema abbia dimensioni maggiori di quelle visibili sul forum (c&rsquo;è una contraddizione, ma fa niente).<p>

Si pensi a uno stadio pieno di gente con un Mac. Cinquantamila, ottantamila, centomila. In una curva ci sono un centinaio di spettatori che fanno molto rumore e sostengono che, in fondo, nello stadio ci debbano essere tantissimi come loro.<p>

Che però sono a godersi la partita ed evidentemente, quel problema, non ce l&rsquo;hanno.<p>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>