---
title: "Peer-to-peer Mac-like"
date: 2004-06-28
draft: false
tags: ["ping"]
---

Finalmente un programma che trova le cose e ti fa anche stare bene

Un po’ di gente considera i programmi di file sharing esattamente come le ragazze (escluse le presenti) considerano le autoradio: continuano a cambiare stazione per trovare una canzone di loro gradimento e non si rendono conto che le canzoni sono sempre le stesse, in rotazione. Questa gente è ossessionata dal pensiero di avere il programma che trova più cose e probabilmente ciò è dovuto al fatto che loro stessi inrealtà non sanno che cosa cercare.

Psicopatologie a parte, i programmi peer-to-peer per Mac hanno sicuramente una cosa in comune: sono brutti. Una delle cose che distingue Mac dal resto del mondo è la soddisfazione estetica che si aggiunge a quella funzionale.

Finalmente il cerchio si rompe, con l’arrivo di <link>Acquisition</link>http://www.acquisitionx.com. Funziona bene, è semplice ed è anche un programma Mac.

Certo, costa quindici dollari. Non mi aspetto che la gente capisca. Specie se il peer-to-peer del vicino è sempre più verde.

<link>Lucio Bragagnolo</link>lux@mac.com