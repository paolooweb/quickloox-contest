---
title: "Sì e no al miniCd"
date: 2002-02-24
draft: false
tags: ["ping"]
---

C’è sempre il dubbio che quei lettori di Cd slot-loading possano usare i Cd di dimensioni più piccole. La risposta è...

Sono poco diffusi, ma ci sono. I miniCd, quelli più piccoli da ottanta millimetri, con su tre canzoni, o materiale dimostrativo di un’azienda, che stanno nel palmo di una mano.
A guardarne uno, nel palmo della mano, e guardare un lettore di Cd del Mac, con quella fessura che non si vede cosa c’è dentro, nasce naturale la perplessità.
OK: prendere nota, che non ripeterò.
Se il miniCd non è rotondo, non funziona. Neanche nei lettori con il vassoietto che esce automaticamente. Niente da fare.
Se il miniCd è rotondo, funziona. Giuro. Cioè, giura <link>Apple</link>http://docs.info.apple.com/article.html?artnum=58641. Tranne che nelle unità combo dei Titanium recenti. Chi ha il Ti con il lettore combo non, ripeto, non inserisca un miniCd rotondo nel lettore. I Titanium più vecchi invece (come il mio) non hanno problemi.

<link>Lucio Bragagnolo</link>lux@mac.com