---
title: "Stile e stalle<p>"
date: 2004-10-09
draft: false
tags: ["ping"]
---

Dedicato ai semplicioni che guardano solo il prezzo<p>

Mi scrive Andrea Fistetto da Manduria (TA), che tiene all&rsquo;indicazione della località perché dice che è splendida, e ha ragione da vendere, sulla loclità e sull&rsquo;argomento:<p>

<cite>Se avessi anche tu amici che dicono che l&rsquo;iMac G5 assomiglia al Vaio, fagli vedere questa immagine. È da far cadere le braccia.</cite><p>

Non riesco a spiegarmi perché la gente giudica il design anche quando si compra l&rsquo;accendino ricaricabile ma, appena si parla di computer, qualsiasi schifezza va bene purché costi un euro in meno.<p>

Non è così. Le cose belle rendono la vita più bella e questo è un valore. Non scriverò <em>ha</em> un valore, tanto se uno non capisce non capisce. Se il bello non conta, perché non vivete in una stalla?<p>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>