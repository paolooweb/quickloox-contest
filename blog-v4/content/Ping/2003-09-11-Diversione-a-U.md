---
title: "Diversione a U"
date: 2003-09-11
draft: false
tags: ["ping"]
---

Per essere un bug, sembra fatto con impegno

Ho appena scoperto che il Mac non riconoscerà una password Open Firmware che contenga la U maiuscola (<link>http://docs.info.apple.com/article.html?artnum=107666&sessionID=anonymous%7C22366174&kbhost=kbase.info.apple.com%3a80%2f</link>).

Mi chiedo se, per provocare un bug del genere, basti una svista di programmazione oppure ci voglia reale impegno.
Saggiamente, Apple consiglia una soluzione assolutamente geniale: non usare la U nelle password di Open Firmware.
Fortuna che non è il bug di un word processor.

<link>Lucio Bragagnolo</link>lux@mac.com
