---
title: "La versione di Gaber"
date: 2002-07-24
draft: false
tags: ["ping"]
---

I giornalisti sono pessime persone, ma chi pirata lo shareware non sa che Dio lo vede

Ogni tanto si discute del prezzo dei grossi programmi commerciali, come Photoshop o Filemaker Pro. Sono discussioni futili: il vero problema è che esistono fior di programmi shareware a due soldi, o freeware-amichevoli (mandami una mail giusto per dirmi che lo usi e ti piace) e il 95% delle persone pirata questi, con aridità e avidità pari solo alla miopia.

Avevo pronta una tirata moralistica e rompimarroni sul &#147;give back&#148; e i fondamenti della filosofia open source (che anche Apple ha abbracciato con Darwin, scaricabile gratis) ma mi è venuta in mente una vecchia canzone di Giorgio Gaber, &#147;Io se fossi Dio&#148;, l&#236; dove parla del piccolo borghese:

&#147;Com&#146;è noioso... lui non commette mai peccati grossi, non è mai intensamente peccaminoso. Del resto, poverino, è cos&#236; piccolo e meschino che, pur sapendo che Dio è più esatto di una sfera, lui crede che l&#146;errore piccolino non lo conti o non lo veda...&#148;

La stessa canzone peraltro maledice i giornalisti &#147;e specialmente tutti&#148; per ragioni validissime. Quindi chi si arrabbia mi insulti pure, ma partendo dall&#146;1-1.

<link>Lucio Bragagnolo</link>lux@mac.com