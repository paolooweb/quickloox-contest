---
title: "Un torrent appicicaticcio"
date: 2009-08-18
draft: false
tags: ["ping"]
---

Era molto che non provavo <a href="http://www.vuze.com/" target="_blank">Vuze</a> e gli ho dato un'occhiata.

Ho installato distrattamente e ho trovato inserita nei miei <i>browser</i> una <i>toolbar</i> fastidiosa, invadente e apparentemente impossibile da eliminare. Per giunta non indispensabile ai fini dell'uso del programma.

Capitasse, niente panico: nella cartella Applicazioni, Vuze piazza una cartella Toolbars, con dentro una cartella Vuze, con dentro un disinstallatore. Semplicissimo, solo che la cosa non è esplicita e bisogna ravanare dentro la documentazione per capirlo.

Se in preda al panico si è cancellata la cartella suddetta, invece che andare a cercare i file a mano conviene&#8230; reinstallare la <i>toolbar</i>. Vuze non vede l'ora e lo propone praticamente ovunque nel programma. Reinstallata la <i>toolbar</i>, comparirà la cartella e sarà facile disinstallare.

Preferirei programmi meno appicicaticci. Sarà l'effetto dell'anguria.