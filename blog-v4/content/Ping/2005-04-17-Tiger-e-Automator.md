---
title: "Ci arriva persino un giornalista<p>"
date: 2005-04-17
draft: false
tags: ["ping"]
---

Dipende dal punto di vista, ma potrebbero essere buone notizie<p>

Sono un fanatico dell&rsquo;automazione e un disastro della programmazione. Con AppleScript impiego un mese a sbrigare compiti che una persona minimamente portata risolve in mezz&rsquo;ora. E il mio codice è di qualità inferiore.<p>

Detto questo, è da un po&rsquo; che nei ritagli di tempo sudavo su AppleScript a insegure il sogno di poter aggiornare automaticamente le pagine di Ping, anziché semiautomaticamente come ora.<p>

Ma ora ho smesso e attendo sereno la venuta di Tiger. Ho potuto toccare con mano che Automator renderà le cose infinitamente più semplici. Persino a me.<p>

Se le cose vanno come immagino, in capo a un fine settimana Automator mi permetterà di raggiungere l&rsquo;obiettivo. Una persona normale ci metterà venti minuti, ma conta anche il principio.<p>

Insomma, con Tiger per me sarà ancora più facile pubblicare i Ping. Non è necessariamente detto che sia una buona notizia. Ma deve contare anche il fatto tecnico, no?<p>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>