---
title: "22 anni indietro, dieci anni avanti"
date: 2008-02-26
draft: false
tags: ["ping"]
---

<cite>Ogni paio d'anni, tiro fuori il mio vecchio Macintosh Plus per avviarlo e giocarci un po'. Molto francamente, è sorprendente come molte cose siano esattamente le stesse in Mac OS X come erano in System 6. Per esempio, non trovo una scorciatoia di tastiera del Finder di Mac OS X che non funzionasse in System 6, eccettuate naturalmente quelle riferite a tecnologie non disponibili nel 1985.</cite>

Da <a href="http://www.appletell.com/apple/comment/macintosh-plus-22-years-later" target="_blank">Appletell</a>, con video del Plus in funzione.

Windows non ha avuto scorciatoie di tastiera degne di questo nome prima di Windows 95, dieci anni dopo.

Macintosh è dieci anni avanti, anche ventidue anni indietro.