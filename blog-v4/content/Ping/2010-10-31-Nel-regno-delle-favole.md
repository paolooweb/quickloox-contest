---
title: "Nel regno delle favole"
date: 2010-10-31
draft: false
tags: ["ping"]
---

Sempre regno del fattibile, ma questo specifico impiego di iPad in modo inimmaginabile per un computer di formato ridotto fa davvero (gran bella) storia a sé.

Complimenti e grazie a <b>Fearandil</b> e non solo per il suo splendido iPad!