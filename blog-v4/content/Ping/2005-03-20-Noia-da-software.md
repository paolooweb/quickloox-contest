---
title: "Noia da software<p>"
date: 2005-03-20
draft: false
tags: ["ping"]
---

Prova a contare fino a trecento e scoprire che c&rsquo;è da continuare<p>

Ho appena terminato un piccolo progetto speciale per Macworld cartaceo. Non dico niente per ora, ma una piccola considerazione la voglio fare.<p>

Prima di dire che per Mac non ci sono programmi, forse uno dovrebbe provare a censirne qualcosa più di trecento, constatando nel mentre che non c&rsquo;è spazio per citarne quaranta volte tanti altri.<p>

Forse, prima di dire che non ci sono programmi per Mac, uno dovrebbe passare la sua vita a provare un decimo di tutti quelli disponibili.<p>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>