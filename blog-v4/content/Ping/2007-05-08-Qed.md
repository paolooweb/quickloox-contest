---
title: "Qed"
date: 2007-05-08
draft: false
tags: ["ping"]
---

Era da molto tempo che non scaricavo con reale interesse del software. Poi, per concorso di cause, ho scoperto il <a href="http://demonstrations.wolfram.com/" target="_blank">Wolfram Demonstrations Project</a>, e l'esistenza del <a href="http://www.wolfram.com/products/player/download.cgi" target="_blank">Mathematica Player</a>.

Ci ho passato dieci minuti per mancanza di tempo. Potrebbero essere giornate. E non posso certo permettermi &#8212; né ho le competenze per usare in modo decente &#8212; Mathematica 6.

<em>Quod Erat Demonstrandum</em>.