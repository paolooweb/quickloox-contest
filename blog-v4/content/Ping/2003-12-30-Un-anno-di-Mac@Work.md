---
title: "Tutta la Ram di Mac@Work"
date: 2003-12-30
draft: false
tags: ["ping"]
---

I dati più entusiasmanti di un anno di vendita di Mac e dintorni

Sai bene che ho un conflitto di interessi quando parlo di Mac@Work perché sono socio (di estrema minoranza) di Mac@Work. Sai anche che me ne frego. A vendere computer non si diventa ricchi; è quasi peggio che scrivere di informatica.

Ho i dati di vendita del primo anno di Mac@Work. Non voglio tediare particolarmente, ma due dati mi piacciono tantissimo: nel 2003 lo store ha installato 280 gigabyte di Ram e 20 terabyte di dischi rigidi.

Non è più questione di soldi. Penso a tutta quella Ram nelle case di professionisti, hobbysti, insegnanti, studenti, persone con mille storie diverse. A tutto quello spazio disco per memorizzare siti, libri, ricerche, foto, filmati, indirizzi, giochi, mille cose diverse.

Apple sta davvero cambiando il mondo una persona alla volta, come recitava un tempo la pubblicità. Ed è entusiasmante parteciparvi in parte minima, per cambiare in modo diverso da come fanno di là, con i loro grigi cassoni da pochi soldi e poco valore, il loro software burocratico e spigoloso, i loro vorrei ma non posso.

Entro nel 2004 entusiasta di tutta quella Ram in giro. Credimi: è diversa.

<link>Lucio Bragagnolo</link>lux@mac.com