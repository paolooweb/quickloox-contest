---
title: "Niente crisi della settima versione"
date: 2004-04-05
draft: false
tags: ["ping"]
---

FileMaker Pro si dimostra più solido di un matrimonio, ma non mancheranno i bisticci

FileMaker Pro 7 è uno degli aggiornamenti più significativi degli ultimi tempi a un prodotto riguardante Apple.

Ci sono grossi cambiamenti: la possibilità di avere più tabelle in un singolo file, il limite per i campi di testo passa da 60 kilobyte a quattro gigabyte, il limite massimo per un database si è spostato da quattro gigabyte a otto terabyte, supporto di Unicode e tante altre piacevolezze.

Ma in una versione così nuova ci sono anche tante piccole modifiche che potranno dare grattacapi a qualcuno, a partire dal fatto che il formato di file è cambiato fino a piccole differenze di comportamento di alcune isctruzioni di scripting.

Morale: FileMaker Pro 7 è un aggiornamento assolutamente imperdibile. Ma installalo solo dopo avere capito bene tutti i cambiamenti introdotti.

<link>Lucio Bragagnolo</link>lux@mac.com