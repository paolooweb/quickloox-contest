---
title: "A ognuno le sue grazie"
date: 2010-12-12
draft: false
tags: ["ping"]
---

Prima ci liberiamo dalla dittatura dei font tutti uguali, prima il mondo diventerà un posto migliore. La Cina di oggi non è un luogo allegro, ma quando dovevano <i>anche</i> vestirsi tutti uguali andava molto peggio.

Su computer, anche se tutti fanno finta di non accorgersene per pigrizia mentale, i font a disposizione sono infiniti.

Adesso si diffondono anche quelli per l'uso su web. La civiltà avanza.

Oggi, grazie a Typedia, <a href="http://typedia.com/blog/post/cure-for-the-common-webfont-part-2-alternatives-to-georgia/" target="_blank">qualche consiglio sui font <i>serif</i></a> (quelli con le grazie, ovvero con piccole estensioni alle estremità). Non solo Georgia; libertà di pensiero è anche libertà di espressione è anche libertà tipografica.

Da segnalare anche il <i>post</i> con le <a href="http://typedia.com/blog/post/cure-for-the-common-webfont-part-1-alternatives-to-arial-and-helvetica/" target="_blank">alternative ad Arial e Helvetica</a>, per i font <i>sans serif</i>.