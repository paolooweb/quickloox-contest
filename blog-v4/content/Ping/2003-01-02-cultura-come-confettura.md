---
title: "Keynote warning"
date: 2003-01-02
draft: false
tags: ["ping"]
---

La cultura è come la confettura, anche quando c’è di mezzo Mac

Anche quest’anno, come direbbe Enzo Biagi, ritorna il Macworld Expo. Steve Jobs annuncerà sicuramente novità, piccole o grandi che siano, nuovi prodotti, segni di vita intelligente e creativa del tutto assenti nel resto del mondo informatico.

Apple è un miracolo: è riuscita ad attraversare un 2002 di crisi globale tenendo a posto i conti di cassa, introducendo nuovi prodotti, creando novità e qualche volta anche sbagliando, ma facendo gli errori di chi lavora e non quelli di chi tira a campare.

È straordinario che una società che produce computer, oggi, possa permettersi di annunciare novità e innovazione almeno una o due volte l’anno; è paradossale che questo sia il catalizzatore di una folla di minus habentes malati di esibizionismo amatoriale, che ne approfitteranno per sputare sentenze, spargere incompetenza e in generale compensare le proprie frustrazioni nella vita.

Nell’ascoltare Steve Jobs, cerchiamo di fare finta che non esistano. Tanto il prodotto ideale per loro non esiste e, se esistesse, non avrebbero più nessuno scopo nella vita.

La cultura è come la confettura, dice un proverbietto francese: meno ce n’è, più la si spalma. In occasione del keynote decine di coltelli stanno tristemente affilandosi.

<link>Lucio Bragagnolo</link>lux@mac.com