---
title: "Aspyranti Intel<p>"
date: 2005-10-22
draft: false
tags: ["ping"]
---

Prosegue la transizione, in un meritato silenzio<p>

Non mi piace presentare materiale promozionale Apple, ma per una volta voglio rimandare all&rsquo;<a href="http://developer.apple.com/business/macmarket/aspyr.html">articolo sul sito Apple</a> dedicato alla transizione come viene vissuta da Aspyr.<p>

Per presentare Aspyr e quello che sta succedendo, basta una frase: la serie di giochi The Sims proseguirà anche nel 2006 e girerà anche su Mac, in formato Universal Binary.<p>

Non si fa un gran parlare della cosa ed è un bene, per due ragioni: primo, manca ancora un sacco di tempo; secondo, non ci sono problemi significativi, per chi voglia metterci un minimo di impegno.<p>

Non fa male la tranquillità, ma fa bene anche avere di tanto in tanto qualche notizia significativa. Questa lo è. Un gioco commerciale non è esattamente un programma così semplice e, che portare il codice su Intel avvenga serenamente, è buono.<p>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>