---
title: "Notizie che non lo erano"
date: 2009-04-12
draft: false
tags: ["ping"]
---

Rubo il titolo a un appuntamento regolare del <a href="http://www.wittgenstein.it/" target="_blank">blog Wittgestein di Luca Sofri</a> e non solo quello.

Il Wall Street Journal ha pubblicato <a href="http://online.wsj.com/article/SB123941988981610781.html" target="_blank">un articolo</a> in cui si racconta che Steve Jobs, amministratore delegato di Apple in permesso malattia, si occupa comunque delle questioni strategiche dell'azienda e dialoga con il resto della dirigenza.

Sembra una notizia: in realtà è esattamente quello che ha scritto Steve Jobs nella sua <a href="http://www.macworld.it/showPage.php?template=notizie&amp;id=16240" target="_self">più recente comunicazione ufficiale</a>, a inizio anno.

Il sito di notizie cellulari mocoNews.net scrive che Research In Motion, quella di BlackBerry, <a href="http://www.moconews.net/entry/419-research-in-motion-scores-big-with-iphone-user-interface-mastermind-hir/" target="_blank">ha assunto Don Lindsay</a>. Spiegano che Lindsay è stato uno dei principali creatori dell'interfaccia grafica di iPhone.

Adesso l'articolo è in forma corretta. Da Apple hanno spiegato che Lindsay ha lavorato l&#236; fino al 2003 e poi se ne è andato a Microsoft. iPhone, presentato nel 2007, ha iniziato a nascere nel 2004 e Lindsay a malapena ne avrà sentito parlare, altro che creare l'interfaccia.