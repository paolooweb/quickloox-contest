---
title: "(Star-spangled) banners"
date: 2006-07-27
draft: false
tags: ["ping"]
---

Già che sono qui: <a href="http://www.opendarwin.org" target="_blank">OpenDarwin</a> ha annunciato la chiusura del progetto marted&#236; 25. Oggi è gioved&#236; 27 quasi sera. Intanto, sulla cosiddetta informazione Mac, si può leggere tutto del nuovo videoregistratore digitale che non esiste in Europa e del nuovo navigatore multimediale DaimlerChrysler che non esiste in Europa.

Tornando a ieri, c'è anche una interessantissima notizia sulla più grande catena di grandi magazzini al mondo che non esiste in Europa.

Forse ho capito perché la notizia della chiusura di OpenDarwin, per loro, non esiste: è perché nell'<a href="http://www.opendarwin.org/en/developers.html" target="_blank">elenco degli sviluppatori</a> ci sono un paio di nomi che sembrano italiani?