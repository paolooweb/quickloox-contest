---
title: "Che anno è, che giorno è"
date: 2009-07-08
draft: false
tags: ["ping"]
---

Non è il giorno in cui Apple ha tradotto la propria pagina di <a href="http://www.apple.com/it/macosx/refinements/enhancements-refinements.html" target="_blank">novità di Snow Leopard</a>. Cos&#236; proseguo nel presentarne una al giorno.

<b>Data nella barra dei menu</b>

Nella barra dei menu possono apparire insieme data e ora.

Piccola ma apprezzata comodità. Personalmente mostro l'ora in analogico (icona di orologio), ma a volte mi serve l'ora precisa. Con Snow Leopard posso aprire il menu e vederla immediatamente, mentre con Leopard dovevo cambiare il formato dell'ora da analogico a digitale, per riportarlo ad analogico.

Insomma, era tempo.