---
title: "La forza dello stilo debole"
date: 2003-06-24
draft: false
tags: ["ping"]
---

Forzare sullo schermo come sulla tela, e dimenticarsene

Con la Wacom Cintiq SX18 che ho in prova da qualche tempo (grazie Wacom e grazie Corel, che mi ha dato una Graphics Suite Corel e Painter 8 per metterla alla prova) si disegna direttamente sullo schermo, che è inclinato verticalmente a mo’ di cavalletto.

Lo schermo grafico è sensibile alla pressione e quindi è possibile simulare una pennellata (un pennarello, un carboncino, un pennino, un dito intinto nel colore) anche nella sua energia. Più si preme con lo stilo sullo schermo, più il tratto risulta energico, come su una tela vera.

Non si rovina lo schermo così? No, si consuma la punta dello stilo, che intelligentemente è più tenera. Ma non c’è da preoccuparsi: nella confezione è compresa una generosa dotazione di punte di ricambio, in modo che l’economia non debba mai andare a scapito della libertà di espressione.

In fin dei conti Caravaggio non doveva preoccuparsi di consumare un pennnello. E neanche noi.

<link>Lucio Bragagnolo</link>lux@mac.com