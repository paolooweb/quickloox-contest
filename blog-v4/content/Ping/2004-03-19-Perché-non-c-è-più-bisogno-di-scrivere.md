---
title: "Perché non c’è più bisogno di scrivere"
date: 2004-03-19
draft: false
tags: ["ping"]
---

Basta leggere nella nuova Reference Library di Apple

Uno dei comportamenti imperdonabili dell’era telematica è chiedere risposte senza prima avere almeno provato a cercarle.

L’imperdonabile non è chiedere; è rifiutarsi di imparare qualcosa, che sul breve frutta una risposta immediata da parte dell’esperto di turno, ma sul lungo rende schiavi, proprio come chi non può fare nient’altro che chiedere.

Apple ha (finalmente) riorganizzato una parte consistente della propria documentazione online, che adesso si può raggiungere tramite un link alla <link>Reference Library</link>http://developer.apple.com/referencelibrary/.

Dammi retta: prima di porre una domanda, fai un giro lì. Può darsi che ci sia una risposta ancora migliore di quella che raccoglierai nella tua mailing list.

<link>Lucio Bragagnolo</link>lux@mac.com