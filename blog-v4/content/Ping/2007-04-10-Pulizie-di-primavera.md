---
title: "Pulizie di primavera"
date: 2007-04-10
draft: false
tags: ["ping"]
---

La mia soglia, sul computer, è l'uno per cento. Quello che occupa almeno un centesimo dello spazio sul disco rigido merita attenzione. Quello che passa sotto, mi fa solo perdere tempo.

Stante che il mio disco rigido è di cento giga, oggi mi sono reso conto che la cartella di World of Warcraft contiene tutti gli aggiornamenti effettuati da quanto ho cominciato a giocarci.

Ho provato a metterli in una cartella a parte, fuori da quella del gioco. Quest'ultimo funziona benissimo.

Prima di buttare via tutto aspetto il prossimo aggiornamento, in modo da essere sicuro di non avere arrecato danni. Quando butterò via tutto, però, sarà un giga e mezzo di roba.

Non voglio fare polemica, ma c'è gente con un disco da 250 giga che passa le serate nella cartella Preferenze. La mia cartella Preferenze è di 66 mega&#8230; un trecentesimo della capienza del disco. E chi se ne frega.