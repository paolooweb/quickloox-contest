---
title: "Ogni riferimento &egrave; puramente variabile"
date: 2006-03-15
draft: false
tags: ["ping"]
---

Mi scrive <a href="http://www.loscrittoio.it" target="_blank">Flavio</a>:

<cite>Ho trascorso buona parte del tempo libero degli ultimi giorni a studiarmi i QuickTime di Apple su Aperture. quello che fa, come lo fa.</cite>

<cite>Ogni riferimento su concorrenza da parte di altri software che io stesso ho definito simili ad Aperture (pur non avendolo sinora adoperato), deve essere cestinato. Il software di Apple dimostra una serie di possibilit&agrave; del tutto al di fuori (almeno al momento) della portata di altri concorrenti.</cite>

E qualcuno dir&agrave; meglio provare il software che vedere i demo. Vero, ma la maggior parte dei giudizi che ho letto sono stati formulati senza neanche guardare quelli.