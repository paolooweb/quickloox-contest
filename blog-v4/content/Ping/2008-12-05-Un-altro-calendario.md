---
title: "Un altro calendario"
date: 2008-12-05
draft: false
tags: ["ping"]
---

Al momento il mio calendario dell'Avvento preferito è <a href="http://www.boston.com/bigpicture/2008/12/hubble_space_telescope_advent.html" target="_blank">quello del Boston Globe</a>, basato su scatti splendidi provenienti dal telescopio spaziale Hubble.

Natale arriva anche là.