---
title: "Tutti in riga con AppleScript"
date: 2008-05-26
draft: false
tags: ["ping"]
---

Lo script di oggi è facile facile. Però spalanca orizzonti interessanti. Il suo compito è selezionare l’intera riga su cui si trova il cursore. Funziona su <a href="http://www.barebones.com/products/textwrangler" target="_blank">TextWrangler</a>, editor di testo gratuito e potente di Bare Bones.

<code>tell application "TextWrangler"</code>
Attiva l’applicazione.

<code>	tell text window 1</code>
Agisce sulla finestra numero 1, la prima che viene creata nella sessione del programma.

<code>		select line (startLine of selection)</code>
Ecco fatto.

<code>	end tell</code>
Chiude il blocco <code>tell</code> interno.

<code>end tell</code>
Chiude il blocco <code>tell</code> esterno.

Lo script completo è:

<code>tell application "TextWrangler"
	tell text window 1
		select line (startLine of selection)
	end tell
end tell</code>

Gli orizzonti evidenti sono due. Il primo è studiarsi per bene il dizionario AppleScript di TextWrangler per capire altri modi semplici di effettuare selezioni e manipolare il cursore. Il secondo è considerare altre applicazioni e vedere se e come permettono la stessa cosa, eventualmente investigando i loro dizionari.

Si possono vedere orizzonti ulteriori e meno evidenti. Il bello (e qualche volta il brutto) di AppleScript è proprio che l’unico ostacolo è quello che mettiamo noi, per capacità o per immaginazione.

Questo script arriva dalla <em>mailing list</em> <a href="http://www.barebones.com/support/lists/bbedit_script.shtml" target="_blank">BBEdit-Script</a> e naturalmente funziona anche su BBEdit, purché si cambi il nome del programma nel codice.