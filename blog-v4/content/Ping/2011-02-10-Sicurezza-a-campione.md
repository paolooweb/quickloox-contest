---
title: "Sicurezza a campione"
date: 2011-02-10
draft: false
tags: ["ping"]
---

L'Istituto Fraunhofer per l'informatica sicura si sta guadagnando pubblicità per una falla di sicurezza di iPhone, dettagliata in un <a href="http://www.sit.fraunhofer.de/en/Images/sc_iPhone%20Passwords_tcm502-80443.pdf" target="_blank">documento Pdf</a>.

La sostanza è che un iPhone rubato, sottoposto a <i>jailbreak</i>, acceduto via <code>ssh</code>, sottoposto a uno <i>script</i> Unix, è in grado di svelare parte dei contenuti del telefono, ammesso che il proprietario non se ne sia accorto e non abbia voluto o potuto ordinare una cancellazione a distanza.

Non è esattamente una situazione che capiti un giorno s&#236; e uno no. Richiede ladri che sanno il fatto proprio, tempo, conoscenze e poi può benissimo darsi che alla fine di segreti contenuti dentro iPhone non ce ne sia neanche uno. Subirei sicuramente un danno se mi rubassero iPhone, ma tutte le informazioni che i ladri riuscirebbero a ottenere dall'apparecchio sarebbero quelle di <a href="http://itunes.apple.com/it/app/shopper-shopping-list-flyer/id284776127?mt=8" target="_blank">Shopper</a>, cioè la lista della spesa.

Per non parlare di tutte le persone che magari hanno segreti su iPhone, ma &#8211; consci di tenere in tasca un apparecchio che per propria natura si presta a furti e smarrimenti &#8211; si sono attrezzati con programmini tipo <a href="http://itunes.apple.com/it/app/1password-for-iphone/id285897618?mt=8" target="_blank">1Password for iPhone</a> oppure <a href="http://itunes.apple.com/it/app/1password-pro/id319898689?mt=8" target="_blank">1Password Pro</a>. Se io fossi una persona minimamente importante con incarichi di responsabilità e informazioni confidenziali nel telefono, userei matematicamente una buona <i>app</i> di cifratura di <i>password</i> e compagnia. Al che qualsiasi attacco sarebbe vano.

Qual è l'informazione che passa? <a href="http://www.macitynet.it/macity/articolo/Bastano-6-minuti-per-rubare-le-password-da-iPhone-e-iPad/aA49199" target="_blank">Bastano sei minuti per rubare le password da iPhone e iPad</a>.

Metto a disposizione il mio iPhone 4 da 32 gigabyte. Lo regalo a qualsiasi campione di Macity che impieghi meno di sei minuti complessivi per fare <i>jailbreak</i>, installare gli <i>script</i> necessari e pronunciare ad alta voce la <i>password</i> del mio <a href="mailto:lvcivs@gmail.com" target="_blank">account Gmail principale</a>. Unica condizione: si filma tutto e lo si mette in rete, con nomi e cognomi, con il mio commento, e Macity lo tiene in evidenza in prima pagina per un mese. Poi lo mettiamo su YouTube, per sempre o finché dura YouTube.

L'Istituto Fraunhofer, per farsi adeguata pubblicità, ha girato anche un suggestivo <a href="http://www.youtube.com/watch?v=uVGiNAs-QbY" target="_blank">video</a>. Che bisogno avrà mai di farsi pubblicità, si chiederà qualcuno? Oh, mi ero dimenticato di dire che <a href="https://shop.mobilesitter.de/" target="_blank">vende una applicazione per la sicurezza dei cellulari</a>. Non la vende su iPhone, ma quale sarà mai l'apparecchio che fa più rumore e frutta più spazio sui <i>media</i>?