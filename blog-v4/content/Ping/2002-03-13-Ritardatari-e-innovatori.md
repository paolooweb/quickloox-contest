---
title: "Ritardatari e innovatori"
date: 2002-03-13
draft: false
tags: ["ping"]
---

Ci pensi? Tra un anno e mezzo il tuo computer potrebbe avere il collegamento wireless incorporato. Uhm, qualcosa non torna...

Durante l’Intel Developer Forum, Intel ha fantasticamente immaginato i Pc della seconda metà del 2003 (giuro) come completi di rete wireless di serie.
I Macintosh hanno AirPort ormai da anni.
Poi si dice che Apple non fa innovazione. Sarà anche vero, ma allora sono gli altri a essere ritardatari.

<link>Lucio Bragagnolo</link>lux@mac.com