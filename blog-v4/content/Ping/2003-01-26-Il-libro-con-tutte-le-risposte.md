---
title: "Il libro con quasi tutte le risposte"
date: 2003-01-26
draft: false
tags: ["ping"]
---

Tutto quello che segue, tranne l’ultimo paragrafo, è scritto sulla copertina, davanti e dietro

Macintosh... la nuda verità

Una prospettiva irriverente, inconsueta, assolutamente prevenuta, sulla vita vera di un utente Macintosh in un mondo dominato da Windows.

di Scott Kelby
Edito da Mondadori Informatica
12,80 euro

Scott Kelby, fondatore della rivista Mac Today, è caporedattore di Mac Design Magazine, la rivista di grafica per gli utenti Macintosh, e presidente della National Association of Photoshop Professionals (NAPP)

Questo libro rivela la nuda verità su:
- perché per gli utenti PC è fondamentale che Apple resti in piedi;
- come resistere all’irrefrenabile tentazione di strangolare il management Apple;
- quello che nemmeno Apple ha mai osato dirti sul tuo Mac;
- qual è il vero segreto di Apple e perché Apple è, e resterà, ancora tra noi.

Ogni tanto si eccede nel tentativo di fare una battuta a tutti i costi, ma il libro è assolutamente da leggere.

<link>Lucio Bragagnolo</link>lux@mac.com
