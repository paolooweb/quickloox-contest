---
title: "Cercalo nel browser"
date: 2003-05-29
draft: false
tags: ["ping"]
---

Safari tratta la history come i bookmark e i bookmark come oggetti ricercabili

Safari, il browser made in Apple, con motore open source. Comando-Opzione-b e si visualizzano i bookmark ma anche la history, o cronologia che dir si voglia.

Comando-f e si può cercare una pagina registrata nei bookmark, o anche visitata e ancora presente nella history. Safari la trova.

Una volta che ci si ricorda di questo trucchetto Safari diventa ancora più utile. Davvero, browsare per credere.

<link>Lucio Bragagnolo</link>lux@mac.com