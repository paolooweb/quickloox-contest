---
title: "Un passetto verso X"
date: 2002-08-26
draft: false
tags: ["ping"]
---

È nato prima lo sviluppatore pigro o l’utente accidioso?

La transizione a Mac OS X sta procedendo con una velocità notevole: Steve Job sottolinea come a fine 2002 il 20% degli utilizzatori attivi starà usando Mac OS X.

C’è tuttavia un doppio fattore che rallenta il processo: da una parte gli sviluppatori mettono controvoglia mano al loro software per convertirlo a X; dall’altra gli utenti attendono, per adottare X, che il loro programma preferito sia disponibile.

Si capisce come questa situazione possa portare a uno stallo in cui due pigri si guardano allo specchio e niente va avanti.

Senza programmi, il valore di X diminuisce; senza utenti, non escono i programmi. Siccome il passaggio a Mac OS X è irreversibile, tanto vale accettare la realtà e muoversi concretamente verso X. Non è solo buona volontà: arrivare tra i primi, in tutti i mercati, conferisce un vantaggio.

<link>Lucio Bragagnolo</link>lux@mac.com