---
title: "Mi chiedo che cosa mi Aspetto"
date: 2008-12-20
draft: false
tags: ["ping"]
---

Non aprivo le Preferenze di Sistema alla voce Aspetto da anni. Anni fa, volevo elencate tra gli Elementi recenti le ultime cinquanta applicazioni utilizzate, gli ultimi dieci documenti e gli ultimi cinque server.

Oggi non voglio alcuna applicazione, ma cinquanta documenti e dieci server.

Sto pensando a quanto è cambiato il mio modo di usare il sistema oppure quanto sia cambiato il mondo attorno a esso.