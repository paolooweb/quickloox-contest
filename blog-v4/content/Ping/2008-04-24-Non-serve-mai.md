---
title: "Non serve mai"
date: 2008-04-24
draft: false
tags: ["ping"]
---

Erano anni che non succedeva.

Ho dato un'occhiata ai miei <em>bookmark</em> su Safari.

Centinaia e centinaia di robe tutte interessanti, tutte importanti, tutte essenziali, in decine di cartelle che non ho mai guardato.

Lo scopo dell'occhiata era aprire un paio di cartelle a tema in più per alcuni lavori in corso. Le ho aperte e, in più, sto buttando via l'impossibile. Entro qualche giorno arriverà a una situazione con poche cartelle e pochi <em>bookmark</em>, che userò veramente.

Se c'è una cosa veramente deleteria nell'usare il computer, è accumulare roba <em>perché magari un giorno serve</em>. Non serve mai.