---
title: "Occhio di lynce<p>"
date: 2006-01-01
draft: false
tags: ["ping"]
---

Il browser che ti fa risparmiare banda e orpelli inutili<p>

La mia connessione cellulare, dal luogo di semivacanza, costa venti euro al mese per un totale di cinquecento mega di traffico. Dovendo visionare molte immagini su web e fare tanto ftp ci sto dentro senza grossi problemi, a patto di evitare gli sprechi.<p>

Per quello, appena posso fare a meno della parte grafica, sto navigando tantissimo con lynx, che carica il solo testo delle pagine (e sta pronto a partire dentro Tiger, semplicemente digitando il nome del programma dal Terminale).<p>

Vedere certi siti familiari in formato solo testo è assai istruttivo. Si capisce la loro vera ed effettiva utilità di consultazione. A volte molto più alta di quello che pareva. A volte, invece&hellip;<p>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>