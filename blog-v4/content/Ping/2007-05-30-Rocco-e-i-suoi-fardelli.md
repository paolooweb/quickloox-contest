---
title: "Rocco e i suoi fardelli"
date: 2007-05-30
draft: false
tags: ["ping"]
---

È arrivato un commento di <strong>Rocco</strong> rispetto a un <a href="http://www.macworld.it/blogs/ping/?p=506" target="_blank">Ping di seicento anni fa</a>.

Nel Ping racconto di avere 35 applicazioni aperte nel Dock senza problemi di stabilità e che su Windows una cosa del genere non è come dirla.

Rocco commenta:

<cite>35 applicazioni insieme?</cite>
<cite>Prova ad aprire lo stesso programma 2 volte senza doverlo installare due volte.</cite>
<cite>Bella liberta`&#8230;</cite>

Ho provato. Per esempio, il comando di Terminale

<code>sudo -u lux /Applications/Safari.app/Contents/MacOS/Safari &#38;</code>

apre una seconda istanza di Safari (che è installato una volta sola e vorrei pure vedere). <code>lux</code> ovviamente è il mio nome utente, che va cambiato da chi usasse il comando.

Povero Rocco. Il peso dell'ignoranza deve essere tremendo.