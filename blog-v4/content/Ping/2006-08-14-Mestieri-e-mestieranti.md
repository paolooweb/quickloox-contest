---
title: "Mestieri e mestieranti"
date: 2006-08-14
draft: false
tags: ["ping"]
---

Nella storia di <a href="http://www.fastforwardsw.com/" target="_blank">Fastforward Software</a>, nuova società di software in cerca di programmatori talentuosi e appassionati esperti in Objective C, Mac OS X e Cocoa, si può leggere una stagione di polemiche sul danno che Boot Camp apporterebbe alla disponibilità di applicazioni per Mac.

Gente che investe sul suo mestiere da una parte, mestieranti ciarlieri dall'altra.