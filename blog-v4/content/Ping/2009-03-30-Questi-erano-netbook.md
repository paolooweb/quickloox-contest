---
title: "Questi erano netbook"
date: 2009-03-30
draft: false
tags: ["ping"]
---

<em>Trekking</em> a Glacier Park, al confine con il Montana e il Canada. Una settimana senza vedere umani, zaini con lo stretto essenziale.

Scrissi un diario di viaggio e qualche altra cosa con uno <a href="http://www.larwe.com/museum/z88.html" target="_blank">Z88 Cambridge</a>. Praticamente un foglio A4 spesso un centimetro, con memoria a stato solido per 128 kilo byte. Aveva uno schermo Lcd da ottanta colonne per dieci righe, sufficiente per capire che cosa succedeva al testo e per fare buon editing. Tastiera di orrenda gomma, stile Spectrum Sinclair, ma di fantastiche dimensioni standard. Un piacere. Scrissi per una settimana, sulla sponda del lago, di notte alla luce clandestina della pila, presso il bivacco di fine giornata. Tornato dal giro, un cavetto e tutto era già dentro il Mac.

Tratta Los Angeles-Milano senza scalo. Diciotto ore di volo e una traduzione critica che avrei dovuto avere pronta e invece no.

Tirai fuori <a href="http://www.flickr.com/photos/obi1kenobi1/2317940730/" target="_blank">il Newton MessagePad 2100 e la sua tastiera-accessorio</a>, ai limiti dell'usabilità ma usabile. Schermo girato per il largo, <em>word processor</em> e via. Fuori dal sonno, dai pasti e dalla pip&#236; non feci altro che scrivere per tutto il viaggio. Arrivato a casa, con l'ultimo respiro della batteria scaricai la traduzione sul Mac.

Questi erano <em>netbook</em>.