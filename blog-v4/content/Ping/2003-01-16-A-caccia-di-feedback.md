---
title: "A caccia di feedback"
date: 2003-01-16
draft: false
tags: ["ping"]
---

Come comportarsi durante i Safari

Steve Jobs ha detto che Safari, il nuovo browser Apple, era rispettoso di tutti gli standard. Subito è cominciato il coro di chi lo provava e scopriva che il sito taleetalaltro.com non funzionava bene.

Steve Jobs non ha detto bugie; semplicemente, non è lui a scrivere i siti di tutto il mondo.

Safari è in beta e ha ampi margini di miglioramento. Nel contempo, il Web è pieno di sedicenti Web author che non capiscono niente di Html e ancora meno di standard. Un sito può essere scritto sufficientemente male da resistere a qualsiasi tentativo di rispetto degli standard.

La soluzione è cliccare lo scarafaggio di Safari e inviare un feedback ad Apple. No, metà soluzione. L’altra metà consiste nello scrivere al sito taleetalaltro.com, non per dire “non siete compatibili con Safari” quanto per dire “il vostro sito non rispetta gli standard del <link>World Wide Web Consortium</link>http://www.w3c.org.

Se siamo in numero sufficiente a cliccare e scrivere, non ci sarà Safari senza golose prede di caccia. :)

<link>Lucio Bragagnolo</link>lux@mac.com