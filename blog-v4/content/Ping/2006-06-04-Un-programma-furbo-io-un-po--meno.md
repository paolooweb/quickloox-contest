---
title: "Un programma furbo (io un po&rsquo; meno)"
date: 2006-06-04
draft: false
tags: ["ping"]
---

Dieci anni che uso <a href="http://www.barebones.com/products/mailsmith/" target="_blank">Mailsmith</a> e mi rendo conto oggi di una cosa utilissima, ancorch&eacute; ovvia.

Il programma possiede funzioni sofisticate di ricerca e permette di scandagliare il mio archivio di posta in modo efficace. Al tempo stesso, qualsiasi mailbox pu&ograve; staccarsi dal visore principale del programma e diventare un visore a s&eacute; stante.

Ma anche i risultati di una ricerca finiscono in un visore separato. Siccome la posta viene indicizzata, se tengo aperta la finestra con i risultati di quella ricerca, la posta in arrivo che rientra nei criteri della ricerca entra a fare parte del visore, che si aggiorna dinamicamente.

Mailsmith, in pratica, aveva le cartelle smart nel 1998. Chiss&agrave; dove sono stato io finora.