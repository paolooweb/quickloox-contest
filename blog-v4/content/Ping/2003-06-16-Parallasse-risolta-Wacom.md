---
title: "Wacom e la parallasse"
date: 2003-06-16
draft: false
tags: ["ping"]
---

Un problema che ha smesso di porsi e ricorda Pda perduti

Tirata fuori dalla scatola, la tela elettronica da 18” Cintiq 18SX di Wacom che sto provando aveva un leggero ma percettibile errore di parallasse. In parole povere, il punto dello schermo in cui “cliccavo” con lo stilo non era esattamente quello che pensavo di puntare. Il problema nasce dal fatto che lo schermo ha uno spessore e in effetti lo stilo incontra lo schermo prima del punto che si considera con lo sguardo, che si trova sotto il vetro protettivo.

È del tutto normale e per questo l’utility di configurazione della tela elettronica possiede un sistema efficace e rapido di calibrazione della parallasse, in modo che si possa puntare lo stilo esattamente dove si ha l’intenzione di farlo.

Sistema praticamente identico a quello del Newton MessagePad, progenitore dei palmari ormai fuori produzione da anni.

La buona tecnologia non sparisce facilmente. Forse è per questo che continuo a usare il Newton e che la Cintiq mi piace proprio.

<link>Lucio Bragagnolo</link>lux@mac.com