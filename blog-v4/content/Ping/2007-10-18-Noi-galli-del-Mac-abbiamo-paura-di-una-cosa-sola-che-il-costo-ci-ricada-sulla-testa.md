---
title: "Noi galli del Mac abbiamo paura di una cosa sola: che il costo ci ricada sulla testa"
date: 2007-10-18
draft: false
tags: ["ping"]
---

Parola di <strong>Massimo</strong>:

<cite>Per poter lavorare con Access mi porto a casa un portatile aziendale Fujitsu-Siemens nuovo di zecca.</cite>

<cite>Appena accendo l'ordigno, questo inizia a farmi compagnia, anche se in standby, con una bella musica di ventole, manco stesse elaborando un file di Photoshop da cinque mega. Poi, sebbene lui abbia installato XP Professional, non riconosce la stampante Usb e lui stesso (il Pc) pare che non sappia di avere due porte Usb. Mi continua a parlare di Lpt1 e via discorrendo. Allora recupero i driver per Windows da un Cd e inizio ad installare la stampante (per quasi dieci minuti sembra un aereo che scalda i motori&#8230;). Il tutto condito ogni tanto da carillon che sa solo lui che vogliono dire&#8230;</cite>

<cite>Poi, dopo vari smanettamenti, si degna di riconoscere la mia base AirPort; prima di entrare in rete i <em>warning</em> sulla sicurezza (con carillon) si sprecano&#8230;</cite>

<cite>E qui viene l'incazzatura finale: ho la fortuna che la mia postazione di casa, al mattino, riceva molto sole; con il PowerBook 15&#8221; non mi ero mai posto il problema della luminosità dello schermo: ci pensa lui a regolarla. Invece il Pc, lui no! L'americo-nippo-tedesco mi imposta una luminosità standard da non vedere un c&#8230; e farmi bruciare gli occhi! Di regolarla a mano non se ne parla.</cite>

<cite>Morale, me lo sono portato da un'altra parte, ma dove non ho un vero tavolo di lavoro, se mi serve qualcosa vado e vengo&#8230; la base AirPort però mi consente di lavorare.</cite>

<cite>Se sommo la perdita di tempo di stamani e la moltiplico solo per una cinquantina di giorni lavorativi e anche per quattro anni, i conti sono presto fatti. Magari un utente meno pratico avrebbe smesso di lavorare o avrebbe chiamato un tecnico, con ulteriori costi.</cite>

<cite>Mio nonno, riferendosi a istituzioni del secolo scorso, ormai soppresse dalla famosa senatrice, diceva che vigeva il detto per chi, risparmiando, si procurava malanni: &#8220;spendere poco è prendere tanto&#8221;. Credo che il paragone calzi, visto che di casini stiamo parlando.</cite>

<cite>Ultima cosa: dopo un paio d'ore di lavoro defatigante ho realizzato che il <em>trackpad</em> ha i due tastini - destro e sinistro, of course - larghi ciascuno circa come due polpastrelli affiancati. Vabbè che non lo uso spesso, ma lavorare su questo coso è come andare in bici e dover guardare i pedali! Non credo che fare due tasti un po' più grandi sarebbe costato una fortuna!</cite>

Ho un solo commento: fare due tasti più grandi non sarebbe costato una fortuna, ma sarebbe costato e costa. In design attento, collaudi, ingegnerizzazione avanzata. Come si vede, risparmiare sul design industriale significa solo che il costo c'è sempre, ma ricade sulle nostre spalle. Dita, occhi, testa&#8230;

