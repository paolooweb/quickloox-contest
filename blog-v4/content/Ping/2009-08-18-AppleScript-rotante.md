---
title: "AppleScript rotante"
date: 2009-08-18
draft: false
tags: ["ping"]
---

Una delle sfide che ho lanciato su Macworld in edizione estiva consisteva nel riscrivere in AppleScript il sistema di cifratura Rot13. Cifratura per ridere (si cambia ogni carattere con il carattere che sta tredici posti più in là nell'alfabeto di ventisei lettere), ma formalmente cifratura e buon esercizio di programmazione di base.

<b>Paolo Falcoz</b> ha raccolto la sfida e si è guadagnato settanta punti mela. Congratulazioni!

Il suo AppleScript segue qui sotto. Tutti i programmi del mondo possono essere migliorati e dunque si può fare buon esercizio a partire dal suo. Primi suggerimenti: un box di dialogo che informi dell'operazione compiuta e di dove si trova il file generato. Più impegnativo e interessante, la possibilità di scrivere direttamente un messaggio da convertire.

Siccome lo script è bello lungo, oltre a metterlo qui sotto l'ho reso disponibile in un <a href="http://www.google.com/notebook/public/11239020180866165520/BDZUU3goQwYXh8rIk?hl=en" target="_blank">Notebook di Google</a>. Forse si copia e incolla meglio.

Non bastasse, il file <i>Rot13 in AppleScript</i> si trova nella cartella Public del mio iDisk, nome utente <a href="http://public.me.com/lux" target="_blank">lux</a>.

Ho detto abbastanza. Adesso manca la macchina Enigma in AppleScript! :-)

<code>-- dichiariamo le tabelle di lookup</code>
<code>global lowerCharactersList</code>
<code>global upperCharactersList</code>
<code>global indexOfLowerA</code>
<code>global indexOfUpperA</code>

<code>set lowerCharactersList to &#172;</code>
<code>	{"a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z"}</code>

<code>set upperCharactersList to &#172;</code>
<code>	{"A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"}</code>

<code>set indexOfLowerA to id of "a"</code>
<code>set indexOfUpperA to id of "A"</code>

<code>try</code>
<code>	-- chiediamo all'utente quale file vuole codificare/Decodificare</code>
<code>	set originalFileName to choose file with prompt &#172;</code>
<code>		"Choose file to encrypt" default location (path to documents folder)</code>
<code>	-- creiamo un handler al file da codificare/decodificare</code>
<code>	set originalFileHandler to open for access originalFileName</code>
<code>	-- leggiamo tutto il file</code>
<code>	-- usa "read <filename> from 1" legge il file dalla prima posizione alla fine</code>
<code>	-- usa "as text" per indicare come interpretare i dati</code>
<code>	set originalContent to read originalFileHandler as text</code>
<code>	-- chiudiamo l'accesso al file</code>
<code>	close access originalFileHandler</code>
<code>	</code>
<code>	-- inizializza output</code>
<code>	set encContent to "</code>
<code>	</code>
<code>	-- sostituiamo le lettere utilizzando la lookup</code>
<code>	-- ciclo su tutti i caratteri</code>
<code>	repeat with i from 1 to length of originalContent</code>
<code>		-- sostituisci i caratteri		</code>
<code>		set encContent to encContent &#38; lookUpChar(character i of originalContent)</code>
<code>	end repeat</code>
<code>	</code>
<code>	-- creiamo un file .out con il testo codificato/decodificato</code>
<code>	set newFileName to (originalFileName as text) &#38; ".out"</code>
<code>	set newFileHandler to open for access newFileName with write permission</code>
<code>	write encContent to newFileHandler as text</code>
<code>	close access newFileHandler</code>
<code>on error errText number errnum</code>
<code>	display dialog "Error [" &#38; errnum &#38; "], " &#38; errText</code>
<code>end try</code>


<code>-- funzione di lookup</code>
<code>on lookUpChar(c)</code>
<code>	</code>
<code>	set lupC to "</code>
<code>	</code>
<code>	considering case</code>
<code>		-- considera solo caratteri alfabetici, case sensitive</code>
<code>		if c is in lowerCharactersList then</code>
<code>			-- prendiamo codice ASCII e lo centriamo in zero</code>
<code>			set intC to (id of c) - indexOfLowerA</code>
<code>			-- regola è (ch+13)mod lunghezza lista +1</code>
<code>			set intC to (intC + 13) mod (count lowerCharactersList) + 1</code>
<code>			-- teniamo conto dello zero</code>
<code>			if intC = 0 then</code>
<code>				set intC to 1</code>
<code>			end if</code>
<code>			set lupC to item intC of lowerCharactersList</code>
<code>		else if c is in upperCharactersList then</code>
<code>			set intC to (id of c) - indexOfUpperA</code>
<code>			set intC to (intC + 13) mod (count upperCharactersList) + 1</code>
<code>			if intC = 0 then</code>
<code>				set intC to 1</code>
<code>			end if</code>
<code>			set lupC to item intC of upperCharactersList</code>
<code>		else</code>
<code>			set lupC to c</code>
<code>		end if</code>
<code>	end considering</code>
<code>	</code>
<code>	return lupC</code>
<code>end lookUpChar</code>