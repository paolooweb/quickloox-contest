---
title: "Causa, effetto"
date: 2007-10-26
draft: false
tags: ["ping"]
---

Cerco di seguire tutti i fenomeni di Internet e anche i <em>social network</em>. Cos&#236; ho aperto <em>account</em> su diversi di essi, tra cui Facebook.

Ora Microsoft è diventata concessionaria globale totale della pubblicità di Facebook. E io ho deattivato il mio <em>account</em>.