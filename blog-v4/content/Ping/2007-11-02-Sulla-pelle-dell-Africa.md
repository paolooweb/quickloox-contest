---
title: "Sulla pelle dell'Africa"
date: 2007-11-02
draft: false
tags: ["ping"]
---

Poi mi si chiede perché ho disattivato il mio account Facebook una volta che Microsoft ne ha preso la gestione pubblicitaria. Questa è la traduzione di una <a href="http://blog.mandriva.com/2007/10/31/an-open-letter-to-steve-ballmer/" target="_blank">lettera aperta</a> scritta dall'amministratore delegato di Mandriva Linux a Steve Ballmer, amministratore delegato di Microsoft:

<cite>Caro Steve,</cite>

<cite>ciao, sono Fran&#231;ois, da Mandriva.</cite>

<cite>Sono certo che siamo troppo piccoli perché tu mi conosca. Sai, siamo una di queste piccole aziende Linux che lavorano duro per il proprio spazio sul mercato. Produciamo una distribuzione Linux, Mandriva Linux. L'ultima versione, Mandriva 2008, è stata percepita come una versione davvero buona e ne siamo orgogliosi. Dovresti provarla, sono sicuro che ti piacerebbe. C'è anche che siamo una delle aziende Linux che non ha firmato un accordo con la vostra azienda (nessuno è perfetto).</cite>

<cite>Di recente abbiamo firmato un contratto con il governo della Nigeria. Magari ne hai avuto notizia, Steve. Stavano cercando una soluzione hardware-software a buon prezzo per le loro scuole. La fornitura iniziale era di 17 mila macchine. Avevamo una buona risposta per le loro esigenze: il Classmate Pc di Intel, con una soluzione personalizzata di Mandriva Linux. Abbiamo mostrato la soluzione al governo, che è piaciuta. Gli è piaciuto il sistema, gli è piaciuta l'offerta, gli è piaciuto il fatto che fosse aperta, che la potessimo personalizzare per la loro nazione e via dicendo.</cite>

<cite>Poi i tuoi uomini sono entrati nella faccenda e l'accordo è diventato più concorrenziale. Non direi che è diventato sporco, ma qualcuno potrebbe averlo detto. I tuoi hanno combattuto e combattuto l'accordo, ma il cliente rimaneva soddisfatto di ricevere i Classmate Pc e Mandriva.</cite>

<cite>Cos&#236; abbiamo firmato l'accordo, abbiamo ottenuto l'ordine, abbiamo finalizzato il software, abbiamo consegnato le macchine. In altre parole, abbiamo fatto il nostro lavoro. Per quanto ne so, le macchine sono proprio ora in consegna.</cite>

<cite>Dopo di che, oggi stesso, sento dal cliente qualcosa di totalmente diverso: &#8220;pagheremo Mandriva come d'accordo, ma dopo lo rimpiazzeremo con Windows&#8221;.</cite>

<cite>Wow! Sono impressionato, Steve! Che cosa hai fatto per questa gente cos&#236; da fargli cambiare idea in questo modo? A me è perfettamente chiaro e sarà chiaro a chiunque. Come chiami quello che hai fatto a casa tua, Steve? A casa mia lo si chiama in diversi modi, sono certo che li conosci.</cite>

<cite>Senti, Steve, come ti senti quando la mattina ti guardi allo specchio?</cite>

<cite>Naturalmente combatterò su questa faccenda e sulla prossima, e su quella dopo. Tu hai i soldi, hai il potere e può darsi anche un senso dell'etica differente dal mio, ma io credo che il lavoro duro, buona tecnologia e buona etica possano vincere ugualmente.</cite>

<cite>Saluti,</cite>

<cite>Fran&#231;ois</cite>

<cite>P.S.: un messaggio per i nostri amici nigeriani: c'è ancora tempo per fare la cosa giusta e la scelta giusta, avrete un mucchio di supporto e servizi eccellenti!</cite>