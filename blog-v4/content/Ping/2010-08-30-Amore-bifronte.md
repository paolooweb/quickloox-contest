---
title: "Amore bifronte"
date: 2010-08-30
draft: false
tags: ["ping"]
---

<a href="http://i35.tinypic.com/jazx2t.jpg" target="_blank">Word 2011 per Mac usa</a> il motore di font <i>open source</i> <a href="http://www.freetype.org/" target="_blank">Freetype</a>, impiegato da tempo anche da Apple in Mac OS X e iOS.

Ma Apple da dieci anni offre in versione <i>open source </i>l'infrastruttura del proprio sistema operativo e promuove progetti open source di rilevanza assoluta come <a href="http://webkit.org" target="_blank">WebKit</a>, dal quale nasce Safari.

Microsoft oggi dichiara di <cite>amare</cite> il software <i>open source</i>, quello realizzato dalla libera comunità dei programmatori, che chiunque può scaricare e migliorare a patto che renda disponibili nella stessa misura i miglioramenti apportati. Jean Paoli, direttore generale del team strategico Microsoft sull'interoperabilità, <a href="http://www.networkworld.com/news/2010/082310-microsoft-open-source.html?page=1" target="_blank">ha dichiarato</a> che l'azienda <cite>ha capito il proprio errore</cite>. S&#236;, perché nel 2001 Steve Ballmer, oggi amministratore delegato di Microsoft, <a href="http://www.theregister.co.uk/2001/06/02/ballmer_linux_is_a_cancer/" target="_blank">ha definito</a> l'<i>open source</i> <cite>cancro della proprietà intellettuale</cite> e non si è mai rimangiato neanche una virgola

E ora un po' di storia recente.

Ad aprile, <a href="http://www.microsoft.com/presspass/press/2010/apr10/04-27mshtcpr.mspx?rss_fdn=Custom" target="_blank">Microsoft ha annunciato</a> di raccogliere <i>royalty</i> da Htc. Microsoft incassa denaro per ogni cellulare venduto da Htc&#8230; con software Android, promosso da Google e <i>open source</i>. L'alternativa, per Htc, era andare in tribunale, accusata da Microsoft di violazione di brevetti.

A febbraio, Amazon ha <a href="http://www.computerworld.com/s/article/9160478/Microsoft_Amazon_strike_patent_licensing_deal" target="_blank">stretto un accordo con Microsoft</a> e le ha pagato una somma non rivelata. In cambio, Microsoft non contesterà legalmente certo software <i>open source</i> impiegato dentro i lettori di <i>e-book</i> Kindle e che violerebbe brevetti Microsoft non specificati.

A novembre 2009 Microsoft <a href="http://blogs.computerworld.com/15074/microsoft_violates_gpl" target="_blank">ha pubblicato dentro Windows 7</a> una utility che faceva uso di software aperto, senza rispettare i termini della licenza. L'&#8220;errore&#8221; è stato corretto solo dopo che il mondo ha iniziato a discuterne su Interet.

Sempre nel 2009 TomTom ha concluso una battaglia legale <a href="http://www.networkworld.com/community/node/40386" target="_blank">accettando di pagare soldi a Microsoft</a> che accusava l'azienda dei navigatori da auto di violazione di brevetti. Relativi a sistemi di gestione file <i>open source</i>.

Nel 2008 <a href="http://www.wired.com/wiredscience/2008/04/bill-gates-what/" target="_blank">Bill Gates spiegava</a> che le licenze <i>open source</i> assicurano <i>che nessuno possa apportare miglioramenti al software</i>.

Nel 2007 Microsoft ha cercato di intimidire la comunità del software libero <a href="http://www.networkworld.com/news/2007/051707-microsofts-patent-claims-jar-open-source.html" target="_blank">affermando</a> che Linux e altro software open source violano 235 brevetti di proprietà Microsoft, senza specificare quali. Molto più efficiente, essere generici: cos&#236;, se hai ambizioni di realizzazione di software, non sai mai se Microsoft ti verrà a cercare o meno.

La cosa divertente è che l'articolo originale dove è comparsa l'affermazione di Ballmer, <cite>Linux è un cancro che si attacca nel senso della proprietà intellettuale a tutto ciò che tocca</cite>, è stato cancellato dal sito del <i>Chicago Sun-Times</i>. Un caso, certamente. L'elenco delle affermazioni fondamentali di Ballmer è però rimasto su <a href="http://lwn.net/2001/0607/a/esr-big-lie.php3" target="_blank">Lwn.net</a>.

Per riassumere: quando l'<i>open source</i> fa comodo, Microsoft lo usa. Quando fa comodo agli altri, Microsoft minaccia causa. Un amore bifronte.