---
title: "Il Poc a Padova"
date: 2004-05-06
draft: false
tags: ["ping"]
---

Foto, notizie e stupidaggini da community

Per chi fosse interessato a sapere, lato Mac e anche per il resto, che cosa sta succedendo al Webbit di Padova, può collegarsi al sito del <link>PowerBook Owners Club</link>http://www.poc.it.

Troverà un sacco di foto, cronache, aneddoti, news e anche un po’ di sano divertimento da fiera con arena di persone che ridiventano campeggiatori per tre giorni e dormono quattro ore a notte in una camerata esattamente a fianco dell’arena dove collegare il proprio computer 24 ore su 24 e conoscere gente, imparare cose nuove e ritrovare il gusto per Internet, l’informatica e magari anche per il Mac.

<link>Lucio Bragagnolo</link>lux@mac.com