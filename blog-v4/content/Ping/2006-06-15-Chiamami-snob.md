---
title: "Chiamami snob"
date: 2006-06-15
draft: false
tags: ["ping"]
---

Guardo poca o nessuna televisione e non riesco a stare fermo davanti al computer a guardare un Dvd. Avevo una collezione sterminata di videocassette. Poi ho visto che registravo un sacco di roba ma poi non la guardavo, o al massimo la guardavo una volta e via. Cos&igrave; ho smesso.

Conosco molti che invece hanno il gusto della collezione del Dvd. Rippare i Dvd, trasferirli su Cd o codificarli DivX, portare un film in formato giusto per un iPod sono attivit&agrave; su cui tanta gente impiega molto tempo e voglia di imparare.

Chiamami snob ma, se avessi quel tempo, piuttosto imparerei quel tanto che basta di <a href="http://sketchup.google.com/index.html" target="_blank">SketchUp</a> per creare un modello 3D del mio quartiere e metterlo a disposizione di tutti quelli che usano <a href="http://earth.google.com/" target="_blank">Google Earth</a>.