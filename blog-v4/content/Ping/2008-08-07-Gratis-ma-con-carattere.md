---
title: "Gratis ma con carattere"
date: 2008-08-07
draft: false
tags: ["ping"]
---

Come tutti i mesi, Blambot ha pubblicato <a href="http://www.blambot.com/fonts.shtml" target="_blank">un altro font gratis</a>. Questo giurerei di averlo visto su qualche fumetto, anche se non riesco a fare una connessione diretta.