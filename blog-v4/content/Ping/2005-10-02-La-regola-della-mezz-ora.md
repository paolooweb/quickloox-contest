---
title: "La regola della mezz&rsquo;ora<p>"
date: 2005-10-02
draft: false
tags: ["ping"]
---

Gli dai un dito e ti prendono il braccio. Con soddisfazione generale<p>

È fresco di stampa (virtuale) il Ping sulla Regola del Minuto (prima di chiedere aiuto, concentrati per un minuto sullo schermo) è già mi scrive <strong>Mario</strong>:<p>

<cite>Qualche giorno fa ho scritto alla lista <a href="http://www.accomazzi.it/TistituzionaleI6.html">Misterakko</a> per chiedere lumi sul backup con Automator, di cui aveva scritto sul libro allegato all&rsquo;ultimo Macworld. Non ho ricevuto risposte. Dopo un paio di giorni, un po&rsquo; seccato, a dire la verità, ho pensato che potevo farcela da solo. Ho aperto Automator e ho spippolato una mezz&rsquo;ora (un po&rsquo; più di un minuto, quindi). Adesso con due clic parte un&rsquo;applicazione che crea una cartella sulla scrivania e la chiama backup, sceglie le cartelle che voglio copiare, le copia nella cartella, fa uno zip del tutto, masterizza lo zip su un Cd al quale applica anche la data del backup, poi prende cartella e zip, che non mi servono più, e le schiaffa nel Cestino.</cite><p>

<cite>Se mi fossi seccato prima non avrei obbligato 900 persone a leggere un messaggio inutile.</cite><p>

<cite>Però ho imparato qualcosa. :-)</cite><p>

Neanche Automator è cosa per tutti e mezz&rsquo;ora è ben più di un minuto. Ma vuoi mettere il gusto di realizzare qualcosa di tuo, con mezz&rsquo;ora (mezz&rsquo;ora, non sei settimane) di applicazione?<p>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>