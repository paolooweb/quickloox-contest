---
title: "Un quarto d'ora di notorietà"
date: 2006-09-14
draft: false
tags: ["ping"]
---

Se qualcuno vuole sapere in tempo reale della presentazione di Apple all'hotel Diana di Milano, ho aperto la stanza di iChat <em>demodiana</em>. Comando-Maiuscolo-G e digitare il nome.

Ecco, non è un keynote, è una roba noiosa da giornalisti. Però, finché ho batteria, si può fare. :)