---
title: "Il vero multithreading"
date: 2010-10-21
draft: false
tags: ["ping"]
---

Apple ha presentato o annunciato <a href="http://www.apple.com/it/ilife/" target="_blank">iLife '11</a>, <a href="http://www.apple.com/it/macosx/lion/" target="_blank">Mac OS X 10.7 Lion</a>, <a href="http://www.apple.com/it/mac/facetime/" target="_blank">FaceTime per Mac</a>, <a href="http://www.apple.com/it/mac/app-store/" target="_blank">Mac App Store</a> e i nuovi <a href="http://www.apple.com/it/macbookair/" target="_blank">MacBook Air</a>.

La cosa impressionante è che, mentre succedeva, è uscito un <a href="http://support.apple.com/kb/HT4297" target="_blank">aggiornamento di Java su Mac OS X</a>, un <a href="http://support.apple.com/kb/DL1315" target="_blank">aggiornamento di Aperture</a>, un aggiornamento di ProKit e in cima a tutto <a href="http://store.apple.com/it/configure/MC024T/A?mco=MTc0NzAwMTI" target="_blank">un processore opzionale più veloce</a> per MacBook Pro 15&#8221; e 17&#8221;.

In altri tempi sarebbero state notizie di cui si discuteva per giorni. Oggi è sottofondo.