---
title: "Lo capisce anche un bambino"
date: 2006-01-27
draft: false
tags: ["ping"]
---

Mi racconta <strong>Mario</strong>:

<cite>Sar&agrave; un caso, ma da quando [suo figlio, N.d.lux] gioca con <a href="http://www.newbreedsoftware.com/tuxpaint/download/macosx/" target="_blank">Tux Paint</a> anche i suoi disegni &ldquo;veri&rdquo; sono migliorati enormemente.</cite>

<cite>Togliere il computer ai bambini perch&eacute; si rimbecilliscono, che idea imbecille!</cite>

Chi non &egrave; d&rsquo;accordo si rivolga pure a me. La frase &egrave; di Mario, ma vorrei proprio averla scritta io.