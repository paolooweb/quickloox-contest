---
title: "Chi li ama li segua"
date: 2007-10-30
draft: false
tags: ["ping"]
---

Durante la presentazione di Leopard presso <a href="http://atworkgroup.net" target="_blank">@Work</a> ho potuto chiacchierare (volentieri) con Carlo di FreeSmug.

Sono io che chiacchiero, lui corre. Mi ha scritto quanto segue:

<cite>Ecco lo <a href="http://www.screencast-o-matic.com/watch/ci6ObVGL" target="_blank">screencast</a> fatto con Screencast-O-Matic su come installare Ubuntu usando Virtual Box, che è <em>aggratis</em> rispetto a Parallels o altri.</cite>

<cite>C'è anche la <a href="http://www.freesmug.org/newsitems/news865" target="_blank">news sul FreeSMUG</a></cite>.

Un <em>post</em>, due link da seguire per scoprire cose assai interessanti. E dare supporto a FreeSmug, che se lo merita.