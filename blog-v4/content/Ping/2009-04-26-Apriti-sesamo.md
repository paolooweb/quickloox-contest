---
title: "Apriti /sesamo"
date: 2009-04-26
draft: false
tags: ["ping"]
---

Qualche volta può tornare utile. Terminale e comando:

<code>open -a Firefox .</code>

La directory cui punta il Terminale diventa navigabile dentro Firefox. Provare per credere!