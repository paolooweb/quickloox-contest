---
title: "Una parentesi felice"
date: 2009-05-12
draft: false
tags: ["ping"]
---

Se hai un editor di testo che usa le espressioni regolari (<em>regular expressions</em> oppure <em>grep</em> oppure <em>regex</em> in un'interfaccia inglese), per esempio il gratuito <a href="http://www.barebones.com/products/textwrangler" target="_blank">TextWrangler</a> sei sulla buona strada. Altrimenti perdi un'occasione.

Certo, vanno imparate. A livello alto sono arte pura, però la base è piuttosto semplice e basta un pochino di esercizio. TextWrangler e BBEdit hanno una eccellente sezione di help dedicata all'argomento, seppure inglese. In italiano si può partire da <a href="http://it.wikipedia.org/wiki/Regular_expression" target="_blank">una pagina di Wikipedia</a>.

A che cosa servono? A renderti felice.

Per esempio, ieri dovevo palleggiarmi un lungo elenco di indirizzi fisici completi di città e provincia, stile <code>Belvedere di Spinello (KR), Montepulciano (SI), Poggibonsi (SI)</code>.

Era necessario radunarli per provincia e rielencarli, cos&#236;:

<code><strong>Crotone</strong></code>
<code>Belvedere di Spinello</code>

<cite><strong>Siena</strong></cite>
<code>Montepulciano</code>
<code>Poggibonsi</code>

È evidente che l'indicazione tra parentesi della provincia non serviva più. Le diverse sigle provinciali presenti nell'elenco erano una cinquantina, ripetute per alcune centinaia di località. Come cancellarle tutte in un colpo solo senza penare manualmente?

Cercando in BBEdit una espressione regolare per sostituirla con niente, ossia cancellarla:

<code> \([A-Z]{2}\)</code>

(inizia con uno spazio)

Bella astrusa, a occhio. Se invece la si analizza con calma, istruzioni alla mano, si capisce che significa <em>trova tutte le sequenze di caratteri che contengono uno spazio, una parentesi tonda aperta, due caratteri alfabetici maiuscoli e una parentesi tonda chiusa</em>.

I <em>backslash</em>, <code>\</code>, indicano che le parentesi tonde vanno considerate come caratteri e non come contenitore (altrimenti sarebbero un comando dentro l'espressione, invece semplici caratteri cercati). La parentesi quadra racchiude un insieme di caratteri da cercare (<code>A-Z</code> è una abbreviazione di <code>ABCDEFGHIJKLMNOPQRSTUVWXYZ</code>) e la parentesi graffa contiene il moltiplicatore dell'insieme precedente (<code>A-Z</code> per due volte).

Le espressioni regolari sono infinitamente più potenti. Ma anche questa applicazione minima mi ha reso più felice per un minuto.