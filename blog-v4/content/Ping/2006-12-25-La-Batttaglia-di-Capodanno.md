---
title: "La Battaglia di Capodanno"
date: 2006-12-25
draft: false
tags: ["ping"]
---

Altro che i <a href="http://seiryu.home.comcast.net/savers.html" target="_blank">botti</a>: è finalmente pronto Battle for <a href="http://www.wesnoth.org" target="_blank">Wesnoth</a> versione 1.2.

Dalla versione 1.0 è passata veramente tantissima acqua sotto i ponti; se fosse un progetto commerciale come minimo passerebbe per una 4.0.

Invece è open source, splendidamente semplice, avvincente e giocabile, da soli o in rete. Chi non lo ha ancora scaricato, o non ci fa più attenzione da tempo, si sta perdendo qualcosa. E l’anno sta per finire. :)