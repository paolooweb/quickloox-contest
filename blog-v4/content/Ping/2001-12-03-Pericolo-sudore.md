---
title: "Pericolo sudore"
date: 2001-12-03
draft: false
tags: ["ping"]
---

Di tutti i miti sul mondo Mac il peggiore è che Office sia necessario per la sopravvivenza della piattaforma. Più che altro è necessario per consolidare il monopolio di Microsoft, se no a che pro ricevere condanne per abuso di posizione dominante?
Comunque coraggiosi bisogna nascere, perché manzonianamente uno non se lo può dare, e quindi Office ce lo dobbiamo tenere. Anzi, secondo Microsoft dovremmo esserne orgogliosi: il nuovo Office per Mac OS X è interamente Aquaticizzato, con trasparenze per ogni dove - almeno facesse più sexy - e il ridisegno totale di oltre settecento finestre di dialogo.
Il che fa pensare. L1utente Office tipico ne userà sì e no venti, per cui paga inutilmente le altre seicentoottanta che non vedrà mai.
Occhio a non usarlo da sudati; con settecento finestre, è fin troppo facile prendersi un virus.