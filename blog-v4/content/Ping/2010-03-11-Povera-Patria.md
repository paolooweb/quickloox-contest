---
title: "Povera Patria"
date: 2010-03-11
draft: false
tags: ["ping"]
---

L'Italia è stata la culla delle arti e delle arti grafiche in primo luogo. Da Aldo Manuzio e Giovambattista Palatino in avanti, anche delle arti tipografiche.

Il pensiero di dover pagare <a href="http://colosseotype.com/" target="_blank">una stampa come questa</a> in dollari avvilisce. Non per il cambio.

Un grazie a Franco Battiato per il titolo (della sua canzone).