---
title: "Tu quoque Ikea"
date: 2009-09-03
draft: false
tags: ["ping"]
---

Le petizioni <i>online</i> non servono a niente. Nondimeno, per pura questione di principio, ho firmato quella che <a href="http://www.PetitionOnline.com/IKEAVERD/petition.html" target="_blank">chiede a Ikea di riconsiderare la decisione</a> di adottare nei suoi materiali di stampa il font Verdana al posto del tradizionale Futura modificato.

I portavoce di Ikea hanno dichiarato che è una mossa per la riduzione dei costi e che l'uso a tappeto di Verdana semplificherà il lavoro di adattamento di cataloghi e altro materiale alle diverse lingue.

Qualunque grafico degno di questo nome sa che Verdana è fatto per lo schermo e non per la stampa e che magari si ridurranno anche i costi, ma certamente la qualità non aumenta.

Se a causa di brutta tipografia Ikea, che pure ha fondato il proprio successo anche sull'innovazione nel <i>design</i>, perdesse qualche vendita, non tanto da dover licenziare ma abbastanza da ripensare alla sua tipografia, ne sarei felice.