---
title: "Floppy modem<p>"
date: 2005-10-15
draft: false
tags: ["ping"]
---

Un altro vecchio protagonista dell&rsquo;informatica si prepara a lasciarci<p>

Era ancora il secolo scorso. Apple presentò un computer che nessuno aveva previsto: tondeggiante, colorato e senza lettore di floppy disk.<p>

Lesa maestà.<p>

Non sarebbe andato da nessuna parte, dicevano. Un grave handicap, scrivevano. Un giocattolo, commentavano.<p>

Se ne vendettero milioni.<p>

È cambiato il secolo. Apple ha appena presentato un iMac che nessuno ha previsto: senza modem interno. È solo opzionale e costa cinquanta euro.<p>

Sacrilegio.<p>

C&rsquo;è il digital divide, stanno dicendo. Da tante parti non arriva l&rsquo;Adsl, protestano. L&rsquo;ennesimo furto di Apple, si lamentano.<p>

Dopo che fu uscito l&rsquo;iMac senza floppy, uscirono vari modelli di lettore esterno Usb.
Adesso che è uscito l&rsquo;iMac senza modem, usciranno vari modelli di modem esterno Usb.<p>

Più che altro, il lettore di floppy disk non ritornò mai più. Segno che Apple aveva ragione. Vediamo se torna il modem. Se non torna, Apple ha ragione anche stavolta.<p>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>