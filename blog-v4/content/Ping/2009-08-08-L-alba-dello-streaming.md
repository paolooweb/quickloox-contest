---
title: "L'alba dello streaming"
date: 2009-08-08
draft: false
tags: ["ping"]
---

Non riesco a capire se la cosa sarà fruibile anche fuori dagli Stati Uniti. Se però qualcuno stanotte facesse notte in spiaggia, griglia, falò e sacco a pelo a guardare le stelle cadenti, sappia che alle sei del mattino di domani, 9 agosto, viene trasmesso in diretta da Oakland, California, e in <i>streaming</i> per iPhone il concerto degli <a href="http://www.myspace.com/underworld" target="_blank">Underworld</a>, <i>band</i> del genere <i>electronica</i>.

Provare non costa niente &#8211; a parte la connessione Internet, se in spiaggia non c'è <i>wifi</i> &#8211; e riuscendoci si parteciperebbe alla prima diretta per iPhone che utilizza il protocollo di <i>streaming</i> via Http appena infilato da Apple dentro QuickTime.

Il tentativo richiede il collegamento al sito <a href="http://iphone.akamai.com/" target="_blank">iphone.akamai.com</a>, oppure <a href="http://underworldlive.com/home.html" target="_blank">underworldlive.com</a>.

Se funziona, bene. Altrimenti si può comunque attendere l'alba. :-)