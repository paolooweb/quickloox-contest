---
title: "Il 17 porta invidia"
date: 2003-01-21
draft: false
tags: ["ping"]
---

Quando si è indietro si mente a chiunque, anche davanti allo specchio

Qualche invidioso ha già sibilato a bassa voce che il nuovo PowerBook 17” di Apple è troppo grande per essere veramente un portatile. Fedro, la volpe e l’uva: la storia si ripete.

Spiacenti, invidiosetti: alla presentazione ufficiale italiana dei nuovi PowerBook è stato anticipato che presto usciranno vari modelli di portatili da 17” dalla concorrenza di Apple.

Solo che arriveranno per secondi, o peggio, peseranno di più, saranno più spessi e via dicendo. Il 17 porta sfortuna ai superstiziosi, ma pare anche che porti un sacco di invidia a chi sceglie tecnologia inferiore ed è disposto, per giustificarsi, a mentire a chiunque. Anche davanti allo specchio.

<link>Lucio Bragagnolo</link>lux@mac.com