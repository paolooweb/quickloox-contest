---
title: "Il grafico, delle funzioni"
date: 2008-12-05
draft: false
tags: ["ping"]
---

Attenzione a questa cosa perché oggi non significa niente e tra cinque anni invece conterà moltissimo. Chi sarà pronto per primo avrà un vantaggio notevole.

<em>Mathematica 7</em> consente di applicare comandi e funzioni alle immagini.

Uno scrolla le spalle e dice lo faccio in Photoshop. Certo; prova a tagliare una immagine in quaranta parti uguali dentro Photoshop. Per non parlare del resto.

Non sapendo niente di matematica, né di Mathematica, né di Photoshop guardo ai <a href="http://blog.wolfram.com/2008/12/01/the-incredible-convenience-of-mathematica-image-processing/" target="_blank">primi segni del cambiamento</a> e resto a mascella pendula. Non so tu. Se lavori da professionista delle immagini forse ci penserai su, magari rispolverando il vecchio libro di algebra del liceo.