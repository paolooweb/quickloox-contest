---
title: "L'anno della pulizia"
date: 2009-12-29
draft: false
tags: ["ping"]
---

Cito da un recente editoriale comparso sulla stampa italiana.

<cite>Informazione. Che il 2010 sia l'anno delle grandi pulizie. Anche nella &#8220;barra dei preferiti&#8221;. Anche nelle scelte che si fanno all'accensione del computer o dello</cite> smartphone (You Got Mail!)<cite>. La mente individuale e collettiva deve tenersi sgombra. La rete imbriglia le facoltà. È un grande strumento di liberazione dai limiti della conoscenza, ma non le è estraneo un elemento di impostura, di vizio, di abbrutimento informativo, educativo, di demenza (e niente è più pericoloso della demenza giovanile, ché quella senile è nel conto da secoli, più o meno sappiamo come regolarci). Fare pulizia significa esercitare un dominio pieno e fermo su qualcosa che per definizione tende a espropriare il tuo tempo, a segmentare la tua intelligenza delle cose, a farti perdere la sfericità umanistica di un vero processo di conoscenza, con il suo ordine di priorità, la sua gerarchia piramidale e non reticolare, la sua essenza orale e conversativa.</cite>

Serve un buon proposito per il nuovo anno? Facile. Meno spazzatura nel <i>browser</i>. Meno vapore con il punto interrogativo spacciato per notizia. Meno spezzoni <i>divertentissimi</i> (il primo, la prima volta) da YouTube e analoghi. Meno fiducia in chiunque non sia padrone di quello che dice, ma lo riporti acriticamente e in modo acefalo da qualcun altro, di terza, quarta, quinta mano. Forse non tutti sono al corrente, ma Google ha sviluppato servizi di traduzione automatica che ora sono buoni e consentono di leggere più che decentemente una notizia scritta in un'altra lingua. Se qualcuno ha da aggiungere di suo solo una cattiva e talvolta inesatta traduzione, il suo lavoro vale come quello dei <i>bot</i> di Google. E la tua attenzione si spreca su roba di pessimo valore.