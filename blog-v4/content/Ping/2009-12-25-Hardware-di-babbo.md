---
title: "Hardware di babbo"
date: 2009-12-25
draft: false
tags: ["ping"]
---

Babbo Natale, informaticamente parlando, si è scatenato; un Magic Mouse, una tastiera <i>wireless</i> Bluetooth e varie chiavette Usb da otto gigabyte in giù.

Era anche benissimo informato, perché tutti e tre gli oggetti hanno trovato ampia applicazione da subito.

C'è inoltre una <a href="http://www.plantronics.com/north_america/en_US/products/cat640035/cat1430032/audio-625-usb" target="_blank">cuffia con microfono</a>, destinata alle sessioni di World of Warcraft con l'amico Piero, ipovedente, di cui sono &#8211; assieme ad altri &#8211; orgoglioso sciamano e guida vocale in quel di Azeroth.

Nel reparto <i>gadget</i> posso elencare fieramente un miniaspirapolvere Usb per pulire la tastiera.

In libreria, dopo la lettura, entrerà con grande piacere il libro <a href="http://oreilly.com/catalog/9781565923478" target="_blank">Learning the bash shell</a>.

Ringrazio pubblicamente Babbo Natale e invito il Norad a essere più buono per l'anno prossimo, cos&#236; da <a href="http://entertainment.slashdot.org/story/09/12/25/0140246/Does-Santa-Hate-Linux" target="_blank">consentire il tracciamento della slitta anche a quanti usano Linux</a>.