---
title: "Tecnologia e nachos"
date: 2011-02-05
draft: false
tags: ["ping"]
---

Tempo di Super Bowl: su <a href="http://www.google.com/earth/index.html" target="_blank">Google Earth</a> si può digitare Cowboys Stadium e si arriva a un notevole modello 3D dello stadio. C'è un video su YouTube che <a href="http://www.youtube.com/watch?feature=player_embedded&amp;v=x3vPfRZoZjw" target="_blank">mostra il risultato</a>.

Chiunque di noi può aggiungere un pezzo di 3D al mondo di Google Earth, con <a href="http://sketchup.google.com/yourworldin3d/getstarted.html" target="_blank">il facile Google SketchUp e l'ancora più facile Google Building Maker</a>.

Chi assisterà alla partita sul posto si troverà letteralmente seduto sopra <a href="http://www.itworld.com/virtualization/135568/super-bowl-xlv-technology-behind-event-and-cowboys-stadium-video" target="_blank">una infrastruttura</a> composta tra l'altro di 884 basi wireless, 70 armadi con oltre 40 mila porte cablate, due chilometri e mezzo di cavi Ethernet, 400 chilometri di fibra, cento terabyte di spazio disco.

Ho provato per curiosità a cercare una trasmissione via Internet della partita. Decine di siti che promettono tutto e mantengono niente, buoni solo a mostrare <i>banner</i> e proporre software di dubbia fiducia, che linkano qualunque cosa pur di salire a galla nella ricerca di Google. Sempre più odio questi siti, odio chi li fa e sollecito il pianeta a darsi una autoregolazione per la quale si mettono pagine su Internet che abbiano almeno un altro scopo oltre al denaro.

Da non dimenticare che la storia dell'informatica è cambiata per sempre proprio durante un Super Bowl di qualche tempo fa, per via di <a href="http://www.youtube.com/watch?v=OYecfV3ubP8" target="_blank">un certo spot pubblicitario di Apple</a> proclamato da più parte il migliore mai girato.

Ho comprato i nachos e la salsa piccante. È tutto pronto.