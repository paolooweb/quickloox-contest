---
title: "Ancora Emergency"
date: 2003-05-05
draft: false
tags: ["ping"]
---

Un altro consiglio banale per cavarsela nel Terminale

Proseguo i piccoli, stupidi ma forse preziosi consigli che l’amico Orio mi ha suggerito di stendere per quelli che me, che bazzicano il Terminale come si fa con una pantera che sospettiamo non addomesticata, e con tutti i denti.

Il comando pwd ci spiega in che directory ci troviamo in un dato momento. È la stessa cosa che dire “quale cartella è aperta sul desktop”. Otteniamo un indirizzo tipo /Users/lux.

Se vogliamo sapere che cosa c’è dentro lux si usa il comando ls, che elenca tutti i file e le cartelle presenti dentro lux. Per passare, poniamo, nella directory (nella cartella) Movies, si scrive cd Movies. Dopo questo comando, pwd darà come risultato /Users/lux/Movies.

Il comando cd serve così per viaggiare verso destra nel percorso delle directory, anche detto path.

Per andare verso sinistra e risalire nel path: cd .. risale di una cartella per volta, cd ~ porta alla home, cd / porta alla radice dell’albero delle directory, che non per niente si chiama root.

Osserva bene come cambia il prompt dopo ogni comando: forninsce indizi preziosi.

<link>Lucio Bragagnolo</link>lux@mac.com