---
title: "L'iPhoto dei Pdf"
date: 2006-08-01
draft: false
tags: ["ping"]
---

<a href="http://www.kero.it" target="_blank">Neko</a> si fa sentire di rado ma sempre per cose utili. Stavolta segnala <a href="http://www.thekip.com" target="_blank">kip</a>, programmino che appare assai simpatico. La definizione del titolo spiega già tutto. Questa versione è gratuita, la prossima sarà a pagamento. Io lo scaricherei, anzi, l'ho già fatto.