---
title: "A chi piacciono i trenini?"
date: 2006-01-29
draft: false
tags: ["ping"]
---

Gli autori del simulatore ferroviario Trainz stanno cercando <a href="http://forums.auran.com/TRS2004/forum/showthread.php?s=dd41851a14053a104d1e9730ca4dc6fc&amp;threadid=106174" target="_blank">collaudatori per la versione Mac</a> del programma. Ci sono oltre cinquanta locomotive fedelissime ai modelli reali&hellip; come si pu&ograve; restare insensibili?