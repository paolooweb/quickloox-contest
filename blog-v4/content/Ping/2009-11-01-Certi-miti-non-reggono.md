---
title: "Certi miti non reggono"
date: 2009-11-01
draft: false
tags: ["ping"]
---

Net Applications ha diffuso i <a href="http://marketshare.hitslink.com/report.aspx?qprid=42&amp;qptimeframe=D&amp;qpcustom=Windows+7&amp;qpsp=3911&amp;qpnp=46&amp;sample=6" target="_blank">dati preliminari di traffico percentuale su Internet</a> relativo a ottobre e la loro variazione sequenziale rispetto a settembre.

Windows passa da 92,77 a 92,54 (-0,23). Mac OS X passa da 5,12 a 5,26 (+0,14).

Windows 7 è uscito ufficialmente il 22 ottobre, ma è <a href="http://marketshare.hitslink.com/operating-system-market-share.aspx?qprid=10" target="_blank">presente nei dati</a> di traffico da mesi, grazie alle versioni preliminari. Windows Xp è al 70,59; Windows Vista al 18,77; Windows 7 al 2,11.

Approssimativamente è passato a Windows 7 un utente Windows ogni 45.

Leopard è al 2,82; Snow Leopard a 1,16; Tiger è a 0,93. Approssivativamente è passato a Snow Leopard un utente Mac ogni cinque.

In termini percentuali, il passaggio a Snow Leopard è nove volte più consistente del passaggio a Windows 7. I quasi due mesi di vantaggio di Snow Leopard significano poco: Apple ha dichiarato che, nelle prime due settimane di disponibilità, il ritmo di adozione del nuovo sistema operativo era veloce il doppio rispetto, ai suoi tempi, a Leopard e quadruplo rispetto, ai suoi tempi, a Tiger.

Sulla <a href="http://marketshare.hitslink.com/os-market-share.aspx?qprid=9" target="_blank">tendenza generale</a>, da dicembre 2008 a oggi Windows continua lentissimamente a diminuire e Mac continua lentissimimamente ad aumentare. Le curve (quasi rette, peraltro) non mostrano alcuna perturbazione.

Un mito vuole che gli insoddisfatti di Vista potrebbbero essere stimolati a passare a Windows 7 invece che considerare uno <i>switch</i> a Mac. Da dicembre 2008 a oggi Vista <a href="http://marketshare.hitslink.com/os-market-share.aspx?qprid=11" target="_blank">continua invece a crescere</a>, dal 14,90 al 18,77.

Un altro mito vuole che i soddisfatti di Windows Xp attendessero Windows 7. Per niente: Xp parte da 76,60 a dicembre 2008 e oggi è a 70,59. Di questi sei punti di differenza, cinque sono stati persi in assenza della versione ufficiale di Windows 7.

Se si parla di <i>switch</i> a Mac, l'arrivo di Windows 7 non ha cambiato niente di niente. Come niente ha cambiato qualsiasi altro sistema operativo di Microsoft, da Windows 3.1 a oggi. <i>Quod erat demonstrandum</i>.