---
title: "Il tocco quasi magico"
date: 2010-03-27
draft: false
tags: ["ping"]
---

Decreto Magic Mouse una delle grandi invenzioni per l'umanità. Almeno relativamente ai mouse.

Con una piccola eccezione, InDesign, e una grande, Google Maps.

Con Magic Mouse, a volte, InDesign fa cose strane. Con <a href="http://maps.google.it/" target="_blank">Google Maps</a>, a volte, <a href="http://www.apple.com/it/magicmouse/" target="_blank">Magic Mouse</a> fa quello che volevi.