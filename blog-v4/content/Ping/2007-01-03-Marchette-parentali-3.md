---
title: "Marchette parentali / 3"
date: 2007-01-03
draft: false
tags: ["ping"]
---

Alla mia signora è stato regalato un iPod shuffle, quello nuovo, a clip.

C'è poco da dire come recensione. Un iPod shuffle è un iPod shuffle. E poi lo usa lei.

Un rilievo però ce l'ho. Aprire un iPod shuffle, anche solo il banale iPod shuffle, è stato reso un piacere.

Alla fine eravamo cos&#236; entusiasti che lei, di impulso, stava appiccicando la mela bianca autoadesiva in dotazione sul sul cellulare nero.

Poi s'è fermata. <cite>Però meriterebbe</cite>, ha detto.

È un gran complimento.