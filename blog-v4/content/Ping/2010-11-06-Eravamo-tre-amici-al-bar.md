---
title: "Eravamo tre amici al bar"
date: 2010-11-06
draft: false
tags: ["ping"]
---

Ogni amico però si trovava in un bar diverso ed eravamo tutti in regime di Wi-Fi.

La differenza tra la nostra videoconferenza a tre in Skype e la stessa videoconferenza in iChat è stata completamente a favore di quest'ultimo. La qualità del video è ampiamente superiore e le funzioni di condivisione di documenti non hanno paragone per risultato.

Non abbiamo cambiato il mondo. Per ora, ovvio.