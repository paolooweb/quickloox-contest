---
title: "Il digiuno che fortifica"
date: 2009-11-30
draft: false
tags: ["ping"]
---

Non può ancora essersene accorto nessuno e probabilmente non se ne accorge nessuno in ogni caso. Però ho iniziato il mio digiuno annuale di <i>mailing list</i>.

Per tutto il mese di dicembre &#8211; e tradizionalmente fino a oltre l'Epifania &#8211; non scriverò niente sulle mailing list che frequento.

Serve a un sacco di cose. A rendermi conto che non c'è strettamente bisogno che io risponda a tutto quello che mi sembra degno di replica; che a volte saper resistere alla tentazione di scrivere ha un effetto molto migliore di scrivere; che, smettendo di scrivere per un po', quando si riprende lo si fa con molta più voglia, motivazione e lucidità.

Lo raccomando vivamente a tutti, anche in altre modalità.