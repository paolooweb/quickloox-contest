---
title: "Booky Woogie"
date: 2007-09-07
draft: false
tags: ["ping"]
---

<cite>Un tip che può essere utile</cite>, mi scrive <strong>Daniele</strong>, <cite>a chi ha ancora come me un vecchio iBook G3 con problemi al chip grafico</cite>.

<cite>Tempo fa il mio &#8220;Booky&#8221; è caduto nel famoso problema del chip grafico che, per un errore di progettazione (eh s&#236;, succede anche in quel di Cupertino) in questo modello si &#8220;staccava&#8221; leggermente dalla scheda logica, in pratica disattivandosi e non permettendo più né di visualizzare l'immagine sul monitor Lcd, né su un monitor esterno.</cite>

<cite>In questi mesi ho continuato a utilizzare Booky, ma come &#8220;server&#8221; senza monitor&#8230; finché non sono incappato in <a href="http://www.powerbook-fr.com/ibook/bricolage/repair_g3_video_en_article797.html" target="_blank">questo how-to</a>.</cite>

<cite>In pratica è bastato smontare la parte inferiore del portatile, inserire un piccolo spessore tra il chip grafico e la base metallica che facesse una LEGGERA pressione sullo stesso, et voilà, una volta riacceso l'iBook era di nuovo perfettamente funzionante.</cite>

<cite>In tempi in cui &#8220;se è rotto butta via e compra nuovo&#8221;, è una bella conquista&#8230; soprattutto dato che l'assistenza tecnica mi aveva preventivato la sostituzione di tutta la scheda logica, operazione ovviamente economicamente assurda. :)</cite>

<cite>Lunga vita ai vecchi Mac :)</cite>
