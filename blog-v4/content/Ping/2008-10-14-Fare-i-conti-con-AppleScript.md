---
title: "Fare i conti con AppleScript"
date: 2008-10-14
draft: false
tags: ["ping"]
---

Oggi non presento uno script vero e proprio, ma AppleScript c'entra lo stesso.

Quando ho abbandonato per sempre Word, credo ormai più di dieci anni fa, un amico mi ricordò che, tra le altre cose, Word poteva essere usato come calcolatrice per eseguire operazioni aritmetiche scritte dentro un documento.

Non che ci manchino calcolatrici, in Mac OS X. Ce n'è una in Dashboard, una in Applicazioni, poi si può usare Spotlight, oppure Google, ce ne sono numerose nel Terminale (il comando <em>bc</em> per esempio, ma lanciare l'interprete Python è un modo meraviglioso per aiutare un bambino a risolvere le espressioni) e poi chissà che cosa ancora.

Se però proviamo ad aprire TextEdit e a fare la magia di Word, ovvero scrivere una operazione aritmetica e selezionarla, vediamo che nei Servizi del menu TextEdit appare una voce, Script Editor, e che il primo menu secondario non fa che calcolare il risultato dell'operazione.

Si può anche digitare Comando-Maiuscole-* per fare tutto da tastiera senza passare dal menu. E funziona come minimo con tutte le applicazioni Cocoa (non ho fatto molte prove, confesso). AppleScript è sempre d'aiuto, anche quando meno te lo aspetti.