---
title: "Notizie e prototipi<p>"
date: 2005-11-27
draft: false
tags: ["ping"]
---

Come stanno le cose con Mac OS X per Intel e chi lo fa girare dove vuole<p>

Passa per notizia il fatto che Mac OS X per Intel venga fatto funzionare su hardware non Apple. È di una certa preoccupazione per Apple che ciò possa accadere, perché un certo numero di persone spera di poter usare il sistema operativo Apple su computer di prezzo infimo e qualità altrettanto.<p>

La stessa gente, se gli venisse proposto di usare il software Nokia dentro un cellulare brutto che costa due euro al discount, ti guarderebbe con schifo. Ma non siamo qui a fare sociologia.<p>

Il punto è che le violazioni attuali delle protezioni esistenti di Mac OS X vengono fatte passare come significative (altrimenti che notizia sarebbe?). Ci vuole più serietà. Traduco di seguito alcune delle <a href="http://www.barebones.com/support/bbedit/current_notes.shtml">note</a> che accompagnano l&rsquo;uscita di <a href="http://www.barebones.com/products/bbedit/index.shtml">BBEdit 8.2.4</a>:<p>

<cite>Questa versione di BBEdit funziona in modo nativo sia sui Mac con PowerPc [&hellip;] sia su hardware con il marchio Apple ed equipaggiato con processori Intel. Ora come ora rientrano in questa descrizione solo i sistemi forniti nel Developer Transition Kit.</cite><p>

<cite><strong>Importante:</strong> [&hellip;] gli utenti di BBEdit su Mac con Intel stanno usando, per definizione, versioni provvisorie di Mac OS X in funzione su prototipi di hardware [&hellip;].</cite><p>

In italiano parlato: c&rsquo;è in giro un sistema provvisorio, funzionante su prototipi di macchine che portano il marchio Apple ma sono Mac solo per modo di dire, distribuito unicamente ai programmatori (i quali dovranno restituire l&rsquo;hardware suddetto, tanto per spiegare quanto quei sistemi siano rilevanti per il futuro).<p>

Il sistema provvisorio, che funziona sui prototipi, lo fanno funzionare su Pc qualsiasi.<p>

Quando il sistema definitivo, che gira sui Mac venduti al pubblico, sarà fatto funzionare sui Pc, allora sarà una notizia. Adesso, chissenefrega.<p>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>