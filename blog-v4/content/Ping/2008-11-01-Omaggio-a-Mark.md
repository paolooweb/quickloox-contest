---
title: "Omaggio a Mark"
date: 2008-11-01
draft: false
tags: ["ping"]
---

Devo ringraziare <strong>Pierfausto</strong> per avere scritto questo ricordo di Mark Hoekstra, improvvidamente e sfortunatamente <a href="http://geektechnique.org/blog/862/mark-hoekstra-has-passed-away" target="_blank">scomparso da qualche settimana</a>. Non riuscirei a fare meglio in alcun modo.

<cite>Mark Hoekstra era un</cite> hacker <cite>di quelli, veri, e non solo dal punto di vista informatico, ma anche della vita di tutti i giorni, sul suo blog niente sfondi neri, teschi o</cite> serial number<cite>, ma idee, tante tante idee su come riutilizzare computer, ipod, schede di rete,</cite> router<cite>, e poi software&#8230;</cite>

<cite>Le sue idee e i sui progetti rimarranno nella rete per un tempo indeterminabile, lui il suo tempo in carne e ossa l'ha finito. Seguivo puntualmente il suo blog, che adesso continua a essere aggiornato da qualche amico per inserire qualche</cite> memorial<cite>. Non ho mai conosciuto questa persona, mai scambiato un solo messaggio, ma incredibilmente mi manca!</cite>

Niente di incredibile, invero.