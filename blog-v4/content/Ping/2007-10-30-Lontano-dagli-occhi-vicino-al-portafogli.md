---
title: "Lontano dagli occhi, vicino al portafogli"
date: 2007-10-30
draft: false
tags: ["ping"]
---

Notato che Spotlight, in Leopard, funziona anche come calcolatrice? Facile. È l&#236; pronto da vedere.

Notato come l'allocazione degli <em>swap file</em> sia dinamica e la gestione della memoria virtuale (meglio: dello spazio occupato dalla memoria virtuale) sia migliorata? Più difficile. Presi dal lavoro, non ossessionati dalla cifra dello spazio libero su disco, capita di non rendersene conto.

Eppure la modifica è importante. A mio modesto avviso, più importante della calcolatrice inserita in Spotlight, che è semplicemente comoda. Però accorgersene è più difficile.

Ecco perché, alla domanda <em>ma che cosa c'è di nuovo in Leopard che non mi posso perdere?</em>, tendo a rispondere sempre più frequentemente <em>le cose che non vedrai e che ti fanno lavorare meglio</em>.