---
title: "Il cluster di Paperone"
date: 2004-04-12
draft: false
tags: ["ping"]
---

Ti lamenti che i Mac costano troppo? Forse valgono qualcosa

Chi sostiene che il prezzo di un Mac sia troppo alto dovrebbe informarsi sull’acquisto di un Apple Workgroup Cluster for Bioinformatics. Un nodo principale basato su un Xserve G5 con 750 gigabyte di dischi, uno switch gigabit Ethernet Asanté, un gruppo di continuità Apc e da uno a quindici nodi Xserve G5 Cluster Node, il tutto in un case antirumore XtremeMac Xrack Pro, completo di cablaggio, AppleCare Premium Support per tre anni, kit di parti di ricambio e vario software preinstallato tra cui Blast, Hmer, fasta e altre duecento applicazioni di bioinformatica. Ha vinto il premio <link>Best of Show</link>http://maccentral.macworld.com/news/2004/03/31/bioitworld/ al recente Bio-It World Conference + Expo e sembra che stia ottenendo un certo successo.

Il prezzo base è di soli 28 mila dollari. Forse vale anche qualcosa, oltre a costare.

<link>Lucio Bragagnolo</link>lux@mac.com