---
title: "Uova e galline"
date: 2008-06-25
draft: false
tags: ["ping"]
---

È un maggiore capolavoro di ingegno sviluppare Wolfenstein 3D nel 1992 oppure produrne nel 2008 <a href="http://www.nihilogic.dk/labs/wolf/" target="_blank">una versione in JavaScript</a>?