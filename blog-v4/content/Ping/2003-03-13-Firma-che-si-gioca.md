---
title: "Firma che si gioca"
date: 2003-03-13
draft: false
tags: ["ping"]
---

Due indirizzi per amare il gioco su Macintosh

Considero le petizioni online pure perdite di tempo. Tutt’altra cosa è invece quando sono le aziende a cercare feedback. Se vengono aiutate, ringraziano e lavorano per chi le aiuta.

In quest’ottica segnalo caldamente il <link>questionario di MacSoft</link>http://www.insidemacgames.com/giveaway/macsoft.php, che vuole conoscere meglio il proprio pubblico per capire meglio che giochi produrre, e There, un nuovo gioco di ruolo online che sollecita un <link>feedback</link>http://survey.prod.there.com:80/xform?app=/qs/qs1 dagli utenti per valutare l’opportunità di una versione Mac e ha approntato una <link>mailing list</link>mailto:WantMacThere@there.com dedicata al tema.

Per fare l’una e l’altra cosa servono poco tempo, nessun impegno e qualche clic. C’è anche caso che il tutto serva veramente.

<link>Lucio Bragagnolo</link>lux@mac.com