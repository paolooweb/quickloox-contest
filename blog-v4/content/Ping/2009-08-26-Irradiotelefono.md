---
title: "Irradiotelefono"
date: 2009-08-26
draft: false
tags: ["ping"]
---

Secondo Cnet, iPhone 3GS è il computer da tasca di Apple il cui modulo cellulare <a href="http://reviews.cnet.com/4520-6602_7-6258775-11.html?" target="_blank">emette meno radiazioni</a>. L'iPhone più irradiante è il modello 3G e quello di prima generazione sta nel mezzo.

Dalla pagina si può risalire a praticamente tutti i cellulari conosciuti, almeno negli Stati Uniti.

Il tema delle radiazioni dei cellulari sta tornando di moda, evidentemente per mancanza di cose più serie. Non trattandosi di radiazioni ionizzanti, se proprio si vogliono prendere precauzioni è sufficiente un utilizzo ragionevole.