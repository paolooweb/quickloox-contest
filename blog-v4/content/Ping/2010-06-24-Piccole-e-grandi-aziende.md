---
title: "Piccole e grandi aziende"
date: 2010-06-24
draft: false
tags: ["ping"]
---

Ricevo dal dottor Giampaolo Agnella di <a href="http://www.elasrl.it" target="_blank">E.L.A. S.r.l.</a> la seguente mail, che pubblico espunta da complimenti vari che non merito, ma di cui ringrazio pubblicamente.

La testimonianza è quella di un utilizzo aziendale di iPad. A ognuno le valutazioni che ritiene adeguate; la mia è che l'aggettivo <i>professionale</i> abbia cessato da molto tempo di significare <i>brutto e con requisiti arbitrari di configurazione</i> per tornare a un più appropriato <i>adatto alla professione</i>.

<cite>Mi chiamo <a href="mailto:agnella@elasrl.it">Giampaolo Agnella</a>, sono un chimico industriale di Asti, ho 49 anni e ho un'azienda di servizi in materia di sicurezza sul lavoro, ambiente e medicina del lavoro. Uso da luglio 2009 un MacBook Pro 13&#8221; (ordinato il giorno della presentazione) dopo aver regalato a mia figlia (oggi diciassettenne) un MacBook bianco tre anni or sono, avendo acquistato di seconda mano da uno studente del Politecnico di Torino un iBook 12&#8221; (ancora PowerPc, che acquistai solo per vederne la compatibilità con Windows), avendo acquistato un iMac per casa a dicembre 2008 e ovviamente utilizzando vari iPod, iPod touch e iPhone.</cite>

<cite>Ho pertanto preordinato il 10 maggio un iPad arrivato gioved&#236; 27. Le dico il mio punto di vista.</cite>

<cite>Al momento ho caricato [su iPad] tutta la normativa specifica di cui dispongo in materia di sicurezza sul lavoro, ambiente, qualità eccetera, usando GoodReader. Posso assicurare che sono tantissimi documenti, sempre con me in formato Pdf. Inoltre ho acquistato Quickoffice e con quello posso leggere ma soprattutto editare relazioni tecniche e fogli di calcolo in Word ed Excel. Da quando uso Mac lavoro con iWork e ovviamente ho installato anche la versione per iPad. Ritengo che non sia importante quando si è in giro definire i documenti: per quello c'è il Pc. Ma [serve] anche solo prendere appunti o elaborare sul posto informazioni, dati e quant'altro sia nel mio lavoro fondamentale. Tutto è agevolato da servizi di</cite> sharing <cite>come iDisk di MobileMe e Dropbox.</cite>

<cite>Sto inoltre partecipando allo sviluppo di un portale di medicina del lavoro che permetta di gestire tutte le informazioni dalle prenotazioni allo svolgimento delle visite, all'archiviazione fino alla redazione e trasmissione dei certificati di idoneità. L'intenzione è sviluppare un'applicazione residente su server dedicato ma esterno all'azienda che si utilizzi con i più importanti browser, tra cui Safari. Si arriverà dunque a utilizzare una tavoletta come iPad per gestire operativamente tutto il servizio.</cite>

<cite>Ritengo che questo strumento, che può cambiare il modo di gestire informazioni, giocare, leggere e guardare film eccetera, possa veramente cambiare il modo di lavorare. E siamo solo all'inizio!</cite>

<cite>Chiedo scusa se ho divagato ma penso possa essere utile, lo spero almeno, avere il parere di un utente che non cerca solo il modo di vedere in alta definizione l'ultimo video di Lady GaGa in qualsiasi parte del mondo, ma di migliorare il proprio lavoro e quello dei propri collaboratori.</cite>

Definire lo strumento a priori è sciocco; conta l'uso che se ne fa. Ci sono piccole aziende che sono grandi nell'intuire le opportunità a loro disposizione e grande aziende piccine piccine che si incartano nel loro pensiero per compartimenti stagni.