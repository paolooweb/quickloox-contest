---
title: "Indovinello a schermo piatto"
date: 2002-02-16
draft: false
tags: ["ping"]
---

Dov’è la porta Usb che, solo apparentemente, manca nel nuovo iMac?

Lo stupendo iMac (Flat Panel), come viene denominato nella documentazione ufficiale, dispone di due bus Usb. Ogni bus ha due porte; solo che gli ingressi visibili sono tre. Dov’è la quarta porta?
La risposta porta alla scoperta di una novità assoluta per il mondo Mac, piccola e nascosta ma, insomma, anche questa è una notizia.
Il primo a scrivermi con la risposta riceverà le mie preziosissime congratulazioni. Per chi non ha voglia di indovinare, un suggerimento: si tratta di un dispositivo di comunicazione che, appunto per la prima volta su Mac, è Usb.

<link>Lucio Bragagnolo</link>lux@mac.com