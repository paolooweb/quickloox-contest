---
title: "Se non ora, quando?"
date: 2006-08-04
draft: false
tags: ["ping"]
---

A cavallo di Ferragosto, chiunque ha potuto, voluto o dovuto avere il Mac con sé potrebbe avere voglia di giocare un po'.

Dei giochi a pagamento e di quelli ufficiali c'è fin troppa trattazione. Il mio consiglio è dare anche un'occhiata a SourceForge.net: tutto open source e abbondanza di giochi gratuiti di ogni tipo.

Ma per Mac ci sono pochi giochi, vero? Questo è l'<a href="http://sourceforge.net/softwaremap/trove_list.php?stquery=&amp;sort=group_ranking&amp;sortdir=asc&amp;offset=0&amp;form_cat=80" target="_blank">elenco</a> che si ottiene applicando il filtro della compatibilità Mac OS X. Vuol dire che ho lasciato fuori tutto quello che si installa solo via Fink, o vuole X11, o va compilato eccetera eccetera. Ossia è un elenco di titoli usabili con una certa facilità praticamente da subito.

La mia sensazione è che la settimana di Ferragosto non basterà.