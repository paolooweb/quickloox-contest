---
title: "A passo di carica<p>"
date: 2004-12-03
draft: false
tags: ["ping"]
---

Non sempre l&rsquo;informazione più interessante è quella che fa più scena<p>

Mi scrive l&rsquo;amico Massimo:<p>

<cite>Vengo al dunque: per iPod esiste una immensità di accessori (forse anche un poco di paccottiglia?), compreso l&rsquo;utilissimo caricatore per auto.</cite><p>

<cite>Ma perché, mi chiedo, nessuno offre il caricatore auto per PowerBook?</cite>  <p>

In questi casi non è colpa di chi non ha trovato l&rsquo;informazione, ma di chi non l&rsquo;ha resa disponibile. In effetti è facile lasciarsi trasportare dalle meraviglie tecnologiche per perdere di vista apparecchiature più prosaiche eppure ugualmente utili, anche se non profumano di fantascienza.<p>

Di caricatori da auto per PowerBook, per fortuna, ne esistono in abbondanza. Per citare due marche qualunque che si possono anche trovare dal rivenditore di fiducia, <a href="http://www.madsonline.com">Madsonline</a> e <a href="http://www.tucano.it">Tucano</a>. Ma ce ne sono varie altre.<p>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>