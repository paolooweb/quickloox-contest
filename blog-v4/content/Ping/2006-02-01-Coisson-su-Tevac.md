---
title: "Sviluppatori, tutto ok<p>"
date: 2006-02-01
draft: false
tags: ["ping"]
---

Con obiezioni così, chi ha bisogno di consensi?<p>

Ferve il dibattito sulla Terza Transizione (io preferisco chiamarla la Lunga Marcia, ma è lo stesso). Su <a href="http://www.tevac.com/article.php/20050610082043514">Tevac</a> è apparso il contributo al dibattito di tale Marco Co&iuml;sson, che si è messo nei panni del programmatore e ha stroncato duramente la strategia di Apple. In breve, i piccoli e medi sviluppatori, sostiene, avranno un sacco di problemi ad aggiornare e supportare il loro software per i processori Intel.<p>

Poi vai a vedere il suo <a href="http://homepage.mac.com/marco_coisson/">sito</a> e scopri che, dei suoi programmi, metà non sono supportati. Quando i processori Intel sono ben di là da venire.<p>

La sensazione dominante è che d&rsquo;ora in avanti avrà una scusa in più per non aggiornare i programmi che già non aggiorna. Se aggiungo le varie imprecisioni tecniche e certe affermazioni totalmente gratuite, penso: con dissenzienti così, Apple non ha proprio bisogno di cercare approvazione.<p>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>