---
title: "La ciambella col buco"
date: 2007-03-21
draft: false
tags: ["ping"]
---

Uno dei punti fermi della configurazione è tenere acceso il login remoto. Dovesse succedere qualcosa, mi collego remotamente con <code>ssh</code> da un'altra macchina e cerco di recuperare la situazione sul PowerBook.

Tutto vero e molto comodo, a patto di controllare a inizio sessione che numero Ip locale si è inventato questa volta il sistema. Al risveglio da uno stop il PowerBook aveva l'interfaccia utente inchiodata. Ho provato da un'altra macchina a connettermi tramite <code>ssh</code> e, non avendo idea del numero Ip, ho provato i primi sette numeri del range 10.0.1.x.

Perché sette? Perché sulla mia rete ci sono tre Mac e una base AirPort, e mi sembrava più che abbastanza. Invece me la sono cavata in altro modo (l'ho richiuso e riaperto), dopo di che sono andato a controllare l'Ip locale: 10.0.1.17.

Sarebbe interessante conoscere il criterio. Altrimenti sa molto di ciambella col buco. Ciambella di salvataggio, col buco nella camera d'aria.