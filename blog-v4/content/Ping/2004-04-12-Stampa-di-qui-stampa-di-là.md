---
title: "Stampa di qui, stampa di là"
date: 2004-04-12
draft: false
tags: ["ping"]
---

Cose che non si possono proprio fare con Mac OS 9

Da Mac OS X 10.3.3 in avanti sono state apportate certe migliorie al sistema di stampa, tali che puoi prendere un documento in stampa e spostarlo su una stampante diversa da quella che era stata indicata in primo luogo.

In certe situazioni fa un gran comodo, e proprio era impossibile farlo in Mac OS 9. Chi dice che non c’è progresso?

<link>Lucio Bragagnolo</link>lux@mac.com