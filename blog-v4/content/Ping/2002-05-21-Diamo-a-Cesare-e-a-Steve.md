---
title: "Diamo a Cesare. E anche a Steve"
date: 2002-05-21
draft: false
tags: ["ping"]
---

Non si può valutare bene Apple senza valutare il mondo intorno a noi

Magie di Internet: abituato a considerare vecchie le notizie di dieci minuti fa, mi sono imbattuto in un <link>articolo</link> http://www.wired.com/news/business/0,1367,49508,00.html di Wired datato addirittura 9 gennaio 2002.
Archeologia telematica; eppure ho trovato una frase di straordinaria attualità, che mi permetto di rubare e riportare qui sotto.

[...] Ma se sei un utente Mac e lo stai pensando, hai ragione: Apple è stata una delle aziende tecnologiche di maggior successo nel 2001. [...] Mentre gli altri fabbricanti di Pc vedevano i prezzi affondare e perdevano posti di lavoro Apple, con un mero 4% di quota di mercato globale, ha fatto avanzare la tecnologia. Ha presentato una mezza dozzina di sorprendenti nuovi prodotti e ha recuperato una buona posizione finanziaria, il tutto nel disprezzo degli osservatori. [...]

Non è per tessere le lodi di Apple, che fa i suoi errori e anche grossi. Ma teniamolo presente: la Mela sta facendo più degli altri, lo sta facendo meglio, lo sta facendo senza essere un gigante come Microsoft e in più riesce a farlo nonostante lo scetticismo di si chiama addetto ai lavori ma in realtà è un incompetente.
Diamo a Steve quello che è di Steve.

<link>Lucio Bragagnolo</link>lux@mac.com