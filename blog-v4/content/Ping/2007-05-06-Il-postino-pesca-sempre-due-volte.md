---
title: "Il postino pesca sempre due volte"
date: 2007-05-06
draft: false
tags: ["ping"]
---

Sulle liste Mac che bazzico sono già in due ad ammettere di essere caduti nel <em>phishing</em> che si propone come un'iniziativa delle Poste italiane e di averci rimesso soldi.

Ironia vuole che uno di essi abbia perso il denaro su Postepay, che teneva <em>per fare acquisti su Internet</em>, in luogo della carta di credito. Preoccupato del fatto che il suo numero di carta di credito potesse viaggiare cifrato su Internet, ha fornito egli stesso in chiaro i dati del suo account alle Poste. Non è per prenderlo in giro (anzi, tanto di cappello per avere avuto il fegato di ammettere in pubblico la vicenda), ma per dire che l'accaduto riassume un intero libro sulla sicurezza digitale.

Altra cosa che si nota, la sorpresa di molti nel constatare che non sono messaggi bloccati massicciamente dai sistemi antispam. Come se l'antispam fosse una misura di sicurezza e analizzasse, invece dei contenuti del messaggio, il suo significato. Purtroppo sono messaggi che nel tono suonano genuini (altrimenti che truffa sarebbe?) e un antispam è fatto per lasciar passare messaggi genuini.

La vera morale è che il sonno della ragione genera la posta in Html, la quale genera mostri. Se quelle mail arrivassero in formato testo tutti vedrebbero con molta più chiarezza che nel link da cliccare c'è qualcosa che non quadra.

Qualche bello spirito mi farà il verso: dopotutto, come considerare due soli casi un <em>campione statistico significativo</em>?

Disgraziatamente, sono due casi su un migliaio, quindi un tasso di conversione dello spam, nel mio limitatissimo campione di esame, equivale allo 0,2 percento.

Nell'attività normale di uno spammer ci si può attendere tuttavia un successo almeno cento volte inferiore, ragione per cui non solo la cosa è pazzescamente significativa, ma somiglia a un allarme nella stessa maniera in cui, quando il pennino del sismografo passa dal tremolio abituale alla vibrazione incontrollata fin oltre i confini del rotolo di carta.