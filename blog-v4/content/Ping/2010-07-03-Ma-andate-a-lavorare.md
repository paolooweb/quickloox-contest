---
title: "Ma andate a lavorare"
date: 2010-07-03
draft: false
tags: ["ping"]
---

Dedicato a quanti hanno definito iPad prodotto per il consumo di informazioni, inadatto al lavoro serio: guardare, prego, come viene pensato da Connexxa per l'<a href="http://www.connexxa.it/" target="_blank">impiego in ambito ospedaliero</a>.

Un grosso grazie a <b>dataghoul</b> per l'imbeccata.