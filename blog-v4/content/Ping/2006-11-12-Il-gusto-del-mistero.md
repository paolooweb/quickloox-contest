---
title: "Il gusto del mistero"
date: 2006-11-12
draft: false
tags: ["ping"]
---

Come si fa a non amare BBEdit dopo che scopri, nel menu Help, una voce <em>Secret Preferences</em>? Non mi sono divertito cos&#236; tanto (relativamente al tema) da quando mi si sono palesati i parametri di configurazione non documentati di Eudora.