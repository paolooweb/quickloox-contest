---
title: "Lento di testo"
date: 2007-09-23
draft: false
tags: ["ping"]
---

La beta 3 di Safari funziona che sembra non sia neanche una beta, con una eccezione: a volte, dopo uso intenso, la gestione del testo all'interno del campo di inserimento degli Url diventa lentissima e i caratteri compaiono alcuni secondi dopo averli digitati.

Spegnere e riaccendere Safari risolve tutto. In più la nuova funzione di riapertura della sessione precedente ha eliminato i problemi pratici legati alla cosa. Però il team di sviluppo ha ancora qualcosa su cui lavorare. :)