---
title: "Per colpa di chi"
date: 2007-12-22
draft: false
tags: ["ping"]
---

<a href="http://www.1802.it/" target="_blank">Matteo</a> pensa che la trascrizione di questa sua mail sia troppo lunga per un blog. No.

<cite>Ciao, ti segnalo questo piccolo scambio di idee avuto questa mattina con il pittore, che entrato in casa, è rimasto ammagliato dall'iMac 24 in salotto.</cite>

<cite>P &#8220;Bello 'sto schermo&#8221;</cite>
<cite>M &#8220;Non è uno schermo, qui c'è tutto il computer&#8221;</cite>
<cite>P &#8220;Come tutto il computer?&#8221; (e comincia a guardare in giro cercando un tower).</cite>
<cite>M &#8220;No, non c'è, è tutto qui&#8221;</cite>
<cite>P &#8220;Ma tu li mangi i computer?&#8221; (il termine &#8216;mangiare' non è a caso, ha usato proprio quello)</cite>
<cite>M &#8220;Beh, s&#236;&#8221; (sorrido) &#8220;Fa parte del mio lavoro&#8221;</cite>
<cite>P &#8220;Allora dimmi, com'è Vista?&#8221;</cite>
<cite>M &#8220;Mah, credo che se hai XP sia comunque un passo avanti&#8221;</cite>
<cite>P &#8220;Perché ho comperato un computer proprio l'altro giorno, e il tizio che me l'ha installato diceva che ha un sacco di problemi&#8221;</cite>
<cite>M &#8220;È&#8230; è difficile a dirsi cos&#236;, per quanto ne so io, saranno i soliti problemi di un sistema relativamente nuovo&#8221;</cite>
<cite>P &#8220;Mah, non so, io non mi trovo, e poi lui ha detto che farei meglio a tornare su XP&#8221;</cite>
<cite>M &#8220;Non mi sembra una buona idea&#8221;</cite>
<cite>P &#8220;Questo è Vista?&#8221; (indicando il mio iMac)</cite>
<cite>M &#8220;No, questo si chiama Mac OS (Leopard, ndr), è un sistema operativo diverso da Vista e non è di Microsoft&#8221;</cite>
<cite>P &#8220;Ah, c'è anche roba che non è di Microsoft? E di chi è?&#8221;</cite>
<cite>M &#8220;Lo fa Apple, come il computer: la stessa ditta che fa l'iPod&#8221; (è triste, lo so, ma funziona sempre)</cite>
<cite>P &#8220;Macchecosa? Adesso chiedo al ragazzo che mi ha installato Vista, magari lo metto su anche io&#8221;</cite>
<cite>M &#8220;Non credo, bisogna avere un computer con la mela&#8221;</cite>
<cite>P &#8220;Perché il mio non va? Guarda che è nuovo&#8221;</cite>
<cite>M &#8220;Non ne dubito, ma questo sistema operativo va solo con questi computer&#8221;</cite>
<cite>P &#8220;Ok, sento il ragazzo che cosa mi dice&#8221;</cite>

<cite>In definitiva, da questo colloquio sono emersi degli aspetti importanti.</cite>

<cite>Il pittore non ha la minima idea di che cosa significhi un sistema operativo, non ha interesse in questo senso. Però ha speso dei soldi per acquistare un computer, dove c'è un sistema operativo che gli è stato detto che &#8220;ha dei problemi&#8221;, e che è meglio fare un downgrade.</cite>

<cite>Non è la prima volta che lo sento, eppure ogni volta mi sembra allarmante. Non conosco il consulente in questione, ma sono convinto che un consiglio del genere sia una offesa all'intelligenza dell'utente.</cite>

<cite>Se l'OS nuovo, quello che rappresenta il futuro, non è pronto, non è valido, la soluzione non è tornare indietro, ma guardarsi attorno. Ma forse, dopo cinque anni di XP, decidere di rimettersi in gioco e imparare un sacco di cose che sino a ieri erano &#8220;inutili frivolezze&#8221; della concorrenza è difficile, e consigliare all'utente di tornare indietro è più facile di cercare una strada migliore. E non sto parlando solo di Mac OS. Esistono altri sistemi, alcuni gratuiti.</cite>

<cite>Se dopo cinque anni di XP e uno di Vista, questo Windows ha ancora dei problemi, sinceramente, non credo che sia colpa di Microsoft, ma degli utenti, e della loro capacità di scelta.</cite>

Ecco, per colpa di chi.