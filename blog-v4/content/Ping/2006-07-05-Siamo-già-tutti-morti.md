---
title: "Siamo già tutti morti"
date: 2006-07-05
draft: false
tags: ["ping"]
---

E poi uno dovrebbe dare retta alle notizie che danno in ambito Mac. S&#236;, se si crede a <a href="http://www.repubblica.it/news/ired/ultimora/2006/rep_nazionale_n_1602491.html?ref=hprepnews" target="_blank">roba del genere</a>.

<strong>Aggiornamento</strong>: sono entrati in azione i responsabili, si fa per dire, del sito, che hanno cancellato l'intera notizia. Probabilmente era troppo faticoso correggerla con i <a href="http://ec.europa.eu/transport/roadsafety/charter/background_en.htm" target="_blank">dati veri</a>. Complimenti.

Grazie a <strong>Gand</strong>.