---
title: "Quell&rsquo;ultimo file<p>"
date: 2004-11-15
draft: false
tags: ["ping"]
---

Il tocco finale a una migrazione tra Mac<p>

Che cosa fa il bravo Mac user quando acquista un nuovo computer? Trasporta diligentemente i suoi bravi dati dal vecchio al nuovo.<p>

E basta spostare tutta la home per conservare preferenze e impostazioni insieme ai documenti, vero? Vero&hellip; con una eccezione.<p>

Il file che contiene le impostazioni di rete (e le postazioni del menu Apple) non è lì. È il file di cui si occupa un checkbox apposta nell&rsquo;installazione di Mac OS X, quando si chiede di conservare le installazioni di rete.<p>

Sta in <code>/Library/Preferences/SystemConfiguration/preferences.plist</code>. È un bel file Xml, apribile con un qualsiasi editor di testo o anche con il Property List Editor che sta in <code>/Developer/Applications/Utilities/Property List Editor</code>. Leggerselo spiega diverse cose, per chi è un curioso.<p>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>