---
title: "La soluzione otto per cento"
date: 2003-01-21
draft: false
tags: ["ping"]
---

Una cosa che Apple fa mentre il resto del mondo mangia a sbafo

Sono alla presentazione ufficiale italiana dei nuovi PowerBook ed Enzo Biagini, Regional Manager Southern Europe di Apple, sta dicendo qualcosa che di solito non fa notizia: Apple, anche in tempi dove non è facile fare profitti e le libertà di spendere sono relative, ha investito oltre l’8% del fatturato in ricerca e sviluppo.

Apple esamina nuove tecnologie, inventa nuovi standard (FireWire, per esempio; il PowerBook 17” è il primo computer in assoluto con FireWire 800, veloce il doppio dell’implementazione precedente) e, direttamente e indirettamente, contribuisce al progresso di tutto il settore informatico.

Mille altre aziende non investono, non ricercano, non inventano, non innovano; come gli sciacalli, aspettano un cadavere da finire di spolpare, quando tutto il lavoro difficile è già stato fatto. E offrono soluzioni inferiori, meno affidabili, che invecchiano troppo in fretta, ma costano (apparentemente) una manciata di euro in meno.

Comprare un Mac significa molto più che portarsi a casa un computer. Significa assicurarsi sul futuro, in termini di innovazione futura e di efficacia presente. Significa fare quell’otto per cento di differenza.

<link>Lucio Bragagnolo</link>lux@mac.com