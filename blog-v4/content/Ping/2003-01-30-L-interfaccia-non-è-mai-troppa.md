---
title: "L’interfaccia non è mai troppa"
date: 2003-01-30
draft: false
tags: ["ping"]
---

La colpa non è mai di chi usa un programma

Sono passato a trovare un amico che, sentito al telefono, non riusciva più a vedere la finestra principale di iTunes.

Dal menu Archivio ho selezionato Mostra Finestra iTunes et voilà, ecco la finestra.

Accontentato il mio amico mi sono messo a pensare. Perché qualcuno bollerà il mio amico come stupido, ma non è vero. La colpa è sempre, sempre, sempre, sempre, sempre dell’interfaccia.

Stupida l’interfaccia, allora? In senso relativo no. L’interfaccia Mac è avanti dieci anni rispetto all’interfaccia Windows.

Ma in senso assoluto, se là fuori qualcuno mi ascolta, abbiamo ancora tanta, tanta strada da fare, non solo per mettere un computer nelle mani di tutti, ma anche per renderlo facile da usare. Dove la parola facile implica il più difficile dei lavori di ricerca e sviluppo.

<link>Lucio Bragagnolo</link>lux@mac.com