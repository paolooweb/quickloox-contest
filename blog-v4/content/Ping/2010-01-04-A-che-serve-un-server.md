---
title: "A che serve un server"
date: 2010-01-04
draft: false
tags: ["ping"]
---

Ars Technica ha spiegato con rara chiarezza e proprietà di argomento i pro e i contro di <a href="http://arstechnica.com/apple/reviews/2010/01/mac-mini-with-snow-leopard-server-review.ars/1" target="_blank">avere in ufficio un Mac mini con Mac OS X Server</a>.

Consiglio la lettura anche a chi abbia più di due computer in casa, perché un piccolo server dotato a basso prezzo del software giusto per fare il server &#8211; non solo l'ovvia condivisione &#8211; può fare miracoli in varie situazioni. Un server, però, è roba più seria di un desktop.

L'articolo è troppo lungo per riportarlo tutto e pubblico solo le conclusioni:

<cite><a href="http://store.apple.com/it/browse/home/shop_mac/family/mac_mini?mco=MTAyNTQzNTk" target="_blank">Mac mini server e Mac OS X Server</a> sono la cosa giusta per il proprio ufficio? Certo dipende. Nelle settimane passate a provarlo, ho trovato molti elementi positivi e molti miglioramenti significativi da 10.5 Per una configurazione semplice, questa combinazione sembra rubata tanto il prezzo è conveniente, nonostante i problemi che ho riscontrato e specialmente se ascoltate i miei consigli sulla sintonia fine dello</cite> spam<cite>.</cite>

<cite>Come con molti prodotti Apple, avrei preferito un'esperienza meno frustrante in certi punti dove l'azienda avrebbe dovuto anticipare i problemi. Nel complesso Apple ha tuttavia smussato la maggior parte degli angoli e nascosto molte delle follie tipiche della configurazione di un server.</cite>

<cite>Se qualcosa dell'articolo non è stato compreso, significa che non siete in grado di usare un Mac mini e Mac OS X Server aspettandovi di fare tutto da soli. Fatevi aiutare. [&#8230;] Il costo sarà comunque di molto inferiore alle alternative disponibili sotto forma di un unico pacchetto hardware/software.</cite>