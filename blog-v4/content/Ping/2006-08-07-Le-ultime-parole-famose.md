---
title: "Le ultime parole famose"
date: 2006-08-07
draft: false
tags: ["ping"]
---

Maurizio Lazzaretti su <a href="http://www.pcworld.it/showPage.php?template=attualita&amp;id=1527" target="_blank">Pcworld.it dell'11 gennaio 2006</a>:

<cite>Un ultimo particolare forse di poca importanza: Core Duo è l'ultimo processore Intel della vecchia generazione, quella a 32 bit. Da settembre nel mondo Windows tutte le CPU saranno a due o più core e a 64 bit, quindi la migrazione a Windows X64 sarà inevitabile anche se non velocissima. Stesso problema per Mac OS X, moltiplicato però per due per il cambio di architettura, quindi la scelta di Jobs spazia dal caos totale allo stop a 32 bit, perdendo però nuovamente la guerra delle prestazioni nella fascia professionale.</cite>

Steve Jobs il 7 agosto 2006 al convegno mondiale degli sviluppatori Apple:

Support for 64 bit applications - previously at Unix layer - in Leopard - giant leap forward - extend support all the way through UI framework into applications - have a fully native 64 bit UI carbon app - did in a completely 32 bit compatible way - can run 32 bit apps side by side with 64 bit - no emulation, no translation - full support, top to bottom

Questo sarebbe il caos oppure lo stop a 32 bit?