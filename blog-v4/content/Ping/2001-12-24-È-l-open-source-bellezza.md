---
Ètitle: " l’open source, bellezza"
date: 2001-12-24
draft: false
tags: ["ping"]
---

Proprio quando uno pensa che non scenderà mai in Darwin, ecco un bug fatto per me

Premessa: Darwin è la parte open source di Mac OS X, di fatto un sistema operativo potentissimo ma privo di qualsiasi interfaccia utente che non sia una riga vuota in cui digitare testo.
Da amante delle interfacce grafiche ho sempre pensato che la cosa non mi riguardasse. Fino a ieri, quando per una serie di cause troppo lunga da spiegare ho digitato nel Terminale

man umask

Doveva apparire lo help Unix del (criptico) comando umask. Invece è apparso quello di tcsh, la shell standard di Darwin (non ti preoccupare, è assolutamente normale non sapere che cos’è una shell. Non dovrebbe esserci mai bisogno di saperlo, se non per un utente molto esperto).
Anch’io ho trovato un bug in Darwin. Il bello è che, teoricamente, niente mi impedisce di mettere a posto il bug e fare un favore ad Apple ma anche a tutti gli altri utenti di Darwin e Mac OS X. Niente mi impedisce, tranne le mie conoscenze. È il Segreto dell’Open Source.
Beh, mi è venuta una voglia di capirne di più che non mi sarei mai immaginato. Fammi gli auguri di buona esplorazione! 