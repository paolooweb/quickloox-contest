---
title: "Non parlarmi di politica<p>"
date: 2005-02-10
draft: false
tags: ["ping"]
---

Vorrei sentire parlare di ragazzi, futuro, investimenti, progresso<p>

Stavolta non posso che rimandare al sito del <a href="http://www.poc.it">PowerBook Owners Club</a>, dove c&rsquo;è la notizia completa.<p>

Che cosa devo pensare del fatto che Apple si prepara a rifornire 63 mila computer a studenti delle medie e che ci sono almeno cinque altre aziende in America in grado di fare la stessa cosa in altrettante scuole?<p>

Non buttarmela in politica, per favore. Sono abbastanza vecchio per avere visto governare tutti i partiti, e ho visto anche abbastanza scuole. Dare la colpa all&rsquo;altro, in questo caso, fa solo ridere. Chi di dovere, invece, faccia il favore di vergognarsi.<p>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>