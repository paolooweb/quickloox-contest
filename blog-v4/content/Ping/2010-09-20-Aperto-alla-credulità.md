---
title: "Aperto. Alla credulità"
date: 2010-09-20
draft: false
tags: ["ping"]
---

Maggio scorso, all'omonimo convegno degli sviluppatori, il <i>vice president for engineering</i> di Google Vic Gundotra, sul <a href="http://www.tomsguide.com/us/Google-Android-Gundotra-Steve-Jobs,news-6875.html" target="_blank">perché Google abbia dato vita ad Android</a>:

<cite>Se Google non avesse agito, si sarebbe trovata di fronte a un futuro draconiano nel quale la nostra scelta sarebbe stata un uomo, un telefono, un provider. Un futuro che non vogliamo.</cite>

Tradotto: quel malvagio di Steve Jobs vuole costringerci tutti a usare iPhone, quel terribile apparecchio chiuso dove è Apple a scegliere che cosa possiamo usare e non c'è libertà.

Skyhook Wireless, l'azienda specializzata in sistemi di determinazione della posizione geografica tramite triangolazione tra torri cellulari, ha presentato <a href="http://daringfireball.net//misc/2010/09/Skyhook-Google%20Complaint%20and%20Jury%20Demand.pdf" target="_blank">l'equivalente americano di un esposto alla magistratura</a> contro Google. Ecco perché:

<cite>La seconda fase [di validazione di un apparecchio Android] prevede l'esame di hardware e software sulla base di una struttura amorfa di requisiti non standard nota come Compliance Definition Document (Cdd).</cite>

Attenzione al seguito.

<cite>Questo esame interamente suggestivo, condotto unicamente da dipendenti di Google con l'autorità definitiva in termini di interpretazione del Cdd, dà a Google la possibilità di definire arbitrariamente</cite> non compatibile <cite>con il Cdd qualsiasi software, caratteristica o funzione.</cite>

Capito, l'apertura di Android rispetto alla chiusura di iPhone? Skyhook Wireless aveva da levarsi un altro sassolino più specifico, che la riguarda da vicino:

<cite>Google ha avvisato i costruttori di apparecchi Android che devono usare il Google Location Service a termini di contratto.</cite>

Il Google Location Service è la parte di codice di Android che permette a un apparecchio di capire dove si trova.

<cite>Sebbene Google sostenga che Android è software libero, nel momento in cui richiede ai costruttori di usare obbligatoriamente il Google Location Service, inestricabilmente inserito nel software a livello di sistema operativo [quindi inesorabilmente avvantaggiato su eventuale software esterno concorrente], sta a tutti gli effetti creando un sistema chiuso per quanto attiene al software di determinazione della posizione.</cite>

Sempre Vic Gundotra durante lo stesso convegno:

<cite>Se credete nell'apertura, se credete nella scelta, se credete nell'innovazione da parte di tutti, benvenuti in Android.</cite>

Certo. Basta credere anche a Google.