---
title: "Test di resistenza passiva"
date: 2008-09-18
draft: false
tags: ["ping"]
---

Sottoporre iPhone a test di autonomia sotto sforzo è cosa cui pensano già tutti.

Ho invece approfittato delle vacanze per mettere alla prova un iPhone fannullone. Il mio (Edge), <em>firmware</em> 2.0.2, circa cinque mesi di vita.

Ho lasciato iPhone acceso tutti i giorni durante il periodo di veglia, circa tra le quindici e le diciotto ore al giorno, usandolo il meno possibile e senza mai collegarlo al Mac. Avere la passività totale su più giorni ed essere ancora vivo è impossibile. Comunque ho dovuto registrare una trentina di minuti di chiamate e circa altrettanti Sms tra scritti e ricevuti. Niente navigazione web, niente posta elettronica, niente App Store, uso delle applicazioni minimo o nullo, una manciata di fotografie. Una cosa, per quanto sporca, il più possibile vicina al puro <em>standby</em>.

Dopo sei giorni, la batteria era visibilmente sopra il cinquanta percento di carica residua.

Dopo nove giorni è arrivato l'avviso di batteria al venti percento della capacità.

La mattina del decimo giorno iPhone si è arreso e ora si sta riprendendo dalla faticaccia, avidamente connesso alla Usb del PowerBook.