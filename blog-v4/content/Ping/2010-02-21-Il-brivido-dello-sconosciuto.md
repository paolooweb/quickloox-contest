---
title: "Il brivido dello sconosciuto"
date: 2010-02-21
draft: false
tags: ["ping"]
---

Molti, molti anni fa scrivevo una rubrica intitolata 20 minuti di Internet. Usavo URoulette, un sito che caricava indirizzi Internet completamente a caso, e commentavo in un articolo la mia navigazione nell'ignoto per un intervallo di venti minuti.

Oggi URoulette <a href="http://www.uroulette.com/" target="_blank">esiste ancora</a>, ma sono altri tempi. Non è più possibile pensare che, per quanto casuale, possa arrivare dappertutto. Forse anche molto del brivido della scoperta è figlio di un'epoca passata.

Un diciassettenne russo ha riportato la stessa sfida in altro campo, con <a href="http://chatroulette.com/" target="_blank">ChatRoulette</a>. L'idea è la stessa, solo che navighiamo attraverso le <i>webcam</i> accese di persone a caso. Ed è accesa anche la nostra, di <i>webcam</i>.

L'esperienza è bizzarra, straniante e degna di essere vissuta almeno per qualche minuto. C'è chi non gradisce la nostra vista, chi attacca bottone, chi aspetta solo di attaccare bottone, chi passa da una <i>webcam</i> all'altra per pura curiosità eccetera eccetera. Non è affatto esclusa la possibilità di incontrare gente simpatica, interessante o inaspettata. Ovviamente è possibile incappare in uno stupido o in un esibizionista; il bello è che basta un clic sul pulsante <i>Next</i> per passare a un'altra <i>webcam</i>. E si può spegnere tutto in qualunque momento.

Da provare.