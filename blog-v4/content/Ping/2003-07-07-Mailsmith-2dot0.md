---
title: "Mailsmith matura"
date: 2003-07-07
draft: false
tags: ["ping"]
---

La versione 2.0 del programma di Bare Bones è da considerare

Siccome ogni tanto faccio un archivio di quella vecchia, la base dati di posta che gestisco è “solo” di un paio di gigabyte. Uso Mailsmith di <link>Bare Bones</link>http://www.barebones.com (quelli di BBEdit) dal 1996 e lui ha sempre svolto il compito in modo eccellente.

Da poco è uscita la versione 2.0 del programma, che compie salti decisi in avanti. Ci sono due sistemi di controllo dello spam (SpamCop e l’integrazione con SpamSieve); c’è l’integrazione con Pgp 8 per cifrare la posta confidenziale; è aumentata la velocità, è migliorata la gestione degli errori di spedizione e ricezione, l’interfaccia è stata riveduta e corretta in più punti (ora è possibile compiere ricerche in un numero arbitrario di mailbox) e rimangono tutte le ottime funzioni che ci sono sempre state, come le ricerche di testo con grep o le - potenziate - capacità di filtraggio.

Lo raccomando di tutto cuore. È un programma veloce, efficiente, versatile e prodotto da una società che da sempre produce software solidissimo esclusivamente per Macintosh. Ah, funziona sicuramente con il nuovo Mac OS X 10.3.

Per qualsiasi altra informazione, scrivimi. Risponderò con Mailsmith, ovvio.

<link>Lucio Bragagnolo</link>lux@mac.com