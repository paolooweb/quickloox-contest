---
title: "Trova le differenze in questo AppleScript"
date: 2008-12-20
draft: false
tags: ["ping"]
---

Ci sono programmi fantastici, gratuiti e stracolmi di funzioni. Uno di questi è <a href="http://www.barebones.com/products/TextWrangler" target="_blank">TextWrangler</a>, editor di testo di Bare Bones Software.

Sono soggetti interessanti per imparare AppleScript, perché contengono funzioni utili che ci possono ispirare. Che cosa più inutile di scrivere un AppleScript capace di imitare una funzione esistente? Contemporaneamente, che cosa più utile di un AppleScript che imita una funzione e quindi ci permette di capire come quella funzione è, letteralmente, costruita?

L'AppleScript che segue attiva il confronto tra due documenti di testo dentro TextWrangler. Funzione che il programma esegue benissimo da solo; proprio per questo è perfetto per verificare che l'AppleScript funzioni bene. Il che ci permetterà di rifare più facilmente le stesse cose, per dire, con un altro programma che ne è sprovvisto.

Mal di testa? Ottimo, si lavora meglio. :-)

<code>tell application "TextWrangler"</code>
<code>	activate</code>

-- fin qui tutto normale, facciamo partire il programma

<code>	set opzioni to {ignore leading spaces:false, ignore trailing spaces:false, ignore extra spaces:false, case sensitive:true}</code>

-- si impostano i parametri di confronto: occhio alle maiuscole, ignora gli spazi superflui eccetera. I parametri si trovano nel dizionario di TextWrangler

<code>	set dataDoc1 to modification date of document 1</code>
<code>	set dataDoc2 to modification date of document 2</code>

-- inserisce in variabili le date di modifica dei due documenti

<code>	if dataDoc1 > dataDoc2 then</code>
<code>		set nuovaFinestra to text window 1</code>
<code>		set vecchiaFinestra to text window 2</code>
<code>	else</code>
<code>		set nuovaFinestra to text window 2</code>
<code>		set vecchiaFinestra to text window 1</code>
<code>	end if</code>

-- assegna il documento a ciascuna finestra secondo le date di modifica

<code>	set risultato to compare nuovaFinestra against vecchiaFinestra options opzioni</code>

-- inserisce in una variabile il risultato del confronto tra le due finestre, che fa parte del dizionario specifico di TextWrangler

<code>	if not differences found of risultato then</code>
<code>		display dialog reason for no differences of risultato buttons {"OK"} default button "OK" with icon note</code>

-- se non ci sono differenze, lo annuncia, sempre attingendo al dizionario AppleScript di TextWrangler

<code>	end if</code>
<code>end tell</code>

Lo script originale era per BBEdit e arriva dalla <em>mailing list</em> BBEdit-talk. TextWrangler però è gratis e un confronto tra due file è sempre un confronto tra due file.