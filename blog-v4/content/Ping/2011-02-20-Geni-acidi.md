---
title: "Geni acidi"
date: 2011-02-20
draft: false
tags: ["ping"]
---

I geni mi raccontano acidamente che a causa di Mac App Store la qualità del software per Mac diminuirà.

Può essere. Fino a ieri tuttavia ignoravo totalmente l'esistenza di <a href="http://www.typedna.com/" target="_blank">TypeDna</a>.

Il modo più sintetico per descriverlo: il dizionario dei sinonimi e dei contrari applicato ai font.

Non so se e quanto funzioni; Macworld.com <a href="http://www.macworld.com/article/157703/2011/02/fontdna2.html" target="_blank">ne parla bene</a>.

Semmai, Mac App Store potrebbe spingere gli autori a produrne una versione nativa per Mac, che non ha bisogno di Flash. Ma software cos&#236; è intelligente e di buon supporto geniale alla creatività.