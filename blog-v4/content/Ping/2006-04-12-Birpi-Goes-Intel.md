---
title: "Birpi Goes Intel"
date: 2006-04-12
draft: false
tags: ["ping"]
---

L&rsquo;amico <em>Birpi</em> ha dichiarato <cite>Per passare a Mac Intel aspettavo solo Apple Remote Desktop, che era l&rsquo;ultimo programma a mancarmi, e adesso finalmente posso decidere</cite>.

Il caso di Birpi non &egrave; ovviamente universale. Ma &egrave; indicativo.

Stando alle dichiarazioni originali di Steve Jobs, a oggi la transizione non doveva neanche essere iniziata. Inizia gi&agrave; a esserci gente per cui si &egrave; conclusa con successo. Non male.