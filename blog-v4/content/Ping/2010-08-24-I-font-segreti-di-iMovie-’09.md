---
title: "I font segreti di iMovie '09"
date: 2010-08-24
draft: false
tags: ["ping"]
---

Se clicchi l'icona di iMovie '09 con il pulsante destro, o comunque in modo che appaia il menu contestuale, puoi scegliere <i>Mostra contenuti pacchetto</i>.

Da l&#236; attraversi le cartelle <i>Contents</i>, <i>Resources</i> e <i>Fonts</i>. Sorpresa!

Nella mia impari lotta contro la beceritudine di Arial e Times New Roman, sapere di avere a portata di mano Coolvetica, Humana Serif (e altri sette) è tutta soddisfazione.

I font non funzioneranno fino a che non vengono copiati (possibilmente un loro duplicato) in una delle cartelle Font di Mac OS X, o aperti con un doppio clic. Parte Libro Font e li mostra in anteprima, con un pulsante Installa Font che è perfetto.

I commenti all'<a href="http://hints.macworld.com/article.php?story=20100824000431412" target="_blank">articolo originale di Mac OS X Hints</a> contengono anche un paio di prodigiosi comandi di Terminale per scoprire in un colpo solo tutti gli altri font nascosti dentro il software presente nella cartella Applicazioni.