---
title: "Nel regno del fattibile / 20"
date: 2010-12-24
draft: false
tags: ["ping"]
---

<cite>Pregasi elencare azioni fattibili su un iPad e non fattibili su un Sony Vaio W da 10&#8221;</cite>.

Simulare <a href="http://electricpig.co.uk/2010/12/20/biggest-iphone-on-earth-hits-london/" target="_blank">l'iPhone più grande di tutti i tempi</a>, realizzato con 56 iPad montati a pannello nella stazione della metropolitana londinese di St. Pancras per pubblicizzare il lancio di href="http://itunes.apple.com/it/app/lara-croft-guardian-light/id394106986?mt=8" target="_blank">Lara Croft and the Guardian of Light</a>.