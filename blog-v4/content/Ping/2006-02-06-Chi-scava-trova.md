---
title: "Chi scava trova"
date: 2006-02-06
draft: false
tags: ["ping"]
---

Svolgo un lavoro in collaborazione con una societ&agrave; di veri <em>geek</em>. Gente che non sa che cosa sia il Terminale perch&eacute; ci lavora dentro da mattina a sera e non lo considera un programma come gli altri, ma la normalit&agrave;. Ci sono file da rivedere e mi dicono di modificarli passando da <strong>subversion</strong>, l&rsquo;ultimo grido dei sistemi di <em>versioning</em> open source. Un sistema di versioning permette di lavorare in tanti sullo stesso file pur lavorando ciascuno su una copia locale dello stesso.

Mi viene il tremore. Oddio, ora ci sar&agrave; una <em>tarball</em> di sorgenti Unix da compilare sa il cielo come&hellip; e sar&agrave; solo l&rsquo;inizio.

&Egrave; stato l&rsquo;inizio. Di qualcosa che non avrei immaginato.

Loro stessi mi passano il link a una <a href="http://metissian.com/projects/macosx/subversion/" target="_blank">immagine disco di subversion</a> pronta da installare su Mac OS X. Semplicissimo.

Mi insegnano i primi comandi, ovviamente da Terminale, e sopravvivo. Con quattro o cinque comandi si pu&ograve; gi&agrave; fare l&rsquo;essenziale.

Poi scopro che O&rsquo;Reilly ha pubblicato online, gratis, un <a href="http://svnbook.red-bean.com/" target="_blank">manuale d&rsquo;uso di subversion</a>.

Un amico, che tocca anche a lui, mi chiede se non ci sia un&rsquo;interfaccia grafica per evitare il Terminale. Trovo <a href="http://esvn.umputun.com/" target="_blank">eSvn</a>, <a href="http://www.einhugur.com/iSvn/index.html" target="_blank">iSvn</a>, <a href="http://www.smartsvn.com/smartsvn/download.html" target="_blank">SmartSvn</a>, <a href="http://subcommander.tigris.org/" target="_blank">subcommander</a> e <a href="http://www.einhugur.com/iSvn/index.html" target="_blank">SvnX</a>.

Infine, <a href="http://roderickmann.org/log/archives/2005/07/textwrangler_as.html" target="_blank">come aprire un file da Terminale direttamente in BBEdit o TextWrangler</a> in modo da poter effettuare le modifiche dentro il miglior editor possibile senza saltare al Finder.

Pensavo a una cosa poco amichevole e invece ci si pu&ograve; lavorare con facilit&agrave; e in un ambiente veramente Mac.

Una lezione. Qualche volta, basta cercare.