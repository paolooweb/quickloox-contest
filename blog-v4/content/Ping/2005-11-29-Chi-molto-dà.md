---
title: "Chi molto dà<p>"
date: 2005-11-29
draft: false
tags: ["ping"]
---

Molto riceverà, in particolare nel paradiso del Mac<p>

Alessio Ferraro, una delle menti (e braccia) instancabili che curano la crescita dell&rsquo;All About Apple Museum, sono di recente stati gratificati da una superdonazione da parte di Giovanni Malacart di Pavia che, riferisce Alessio, li ha l<cite>etteralmente sepolti con un furgone di materiale, soprattutto perzzi di ricambio Appleservice, ma anche numerosissimi pezzi rari e molti monitor funzionanti</cite>.<p>

L&rsquo;<a href="http://www.allaboutapple.com">All About Apple Museum</a> è il museo Apple più grande del mondo. Il merito va ai suoi brillanti ideatori e gestori, ma anche ai tanti che lo alimentano con le loro donazioni.<p>

Non c&rsquo;è bisogno, chiaro, di mandare un furgone per fare la propria parte. Può bastare anche un piccolo Newton. :-)<p>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>