---
title: "Modding per esperti<p>"
date: 2005-01-03
draft: false
tags: ["ping"]
---

Tutti possono sbagliare, ma correggere l&rsquo;errore è ancora per pochi<p>

Quello scioccone di Paolo sta troppo attento alla pubblicità e si è comprato una Xbox, ossia un computer zoppo e limitato prodotto da una azienda colpevole di abuso di monopolio e concorrenza sleale. Si gioca molto meglio con una PlayStation 2 o con un GameCube.<p>

Detto ciò, è ancora in tempo a rimediare. Se sulla sua Xbox installa Xebian Linux e vi compila sopra l&rsquo;emulatore PearPc, può anche <a href="http://www.cc.gatech.edu/~ranma1/mac_install.html">installare Mac OS X</a> sulla Xbox. Se è un computer, che almeno ci sia un sistema operativo degno di questo nome.<p>

Ma ci vuole grande abilità tecnica e non credo che lui sia all&rsquo;altezza (neanche io, se è per quello). Altrimenti non sarebbe neanche cliente Microsoft. :-)<p>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>