---
title: "C&rsquo;è anche in italiano<p>"
date: 2005-11-25
draft: false
tags: ["ping"]
---

Mariner Calc è qualcosa più del solito shareware<p>

Il Direttore Supremo mi ricorda che Mariner Calc (in questi giorni in vendita a prezzo scontato sul sito originale americano) viene anche distribuito in italiano dalla benemerita <a href="<a href="http://www.accessoripc.com/itproducts/story$num=3332&dir=468&tmplt=productsstoryactivelancio">Active Software</a> di Gorizia.<p>

E chi sono io per poterlo trascurare? :-D<p>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>