---
title: "Me li do io i permessi"
date: 2010-05-31
draft: false
tags: ["ping"]
---

Grazie a <b>Sabino</b> per il permesso di riportare qui sotto, come <i>post</i>, il commento da lui inviato giorni fa in merito a una questione di permessi e modifiche di file in arrivo su Mac da sistemi esterni. Cos&#236; ha più visibilità.

Ne consiglio la lettura anche in assenza di idea su perché è nato e a che cosa serva. In ogni paragrafo si trova qualcosa di utile per avvicinarsi agli incantesimi Unix che danno vita al mondo magico di Mac OS X e ciascuna nozione risulta preziosa anche indipendentemente dalle altre.

<cite>Premessa #1: il Terminale in Mac OS X si trova in <code>Applicazioni > Utility</code>. Una volta lanciato può essere configurato in molti modi (tramite le Preferenze) perdendo quell'aspetto triste e serioso che spaventa l'utente normale.</cite>

<cite>Una volta lanciato, il Terminale presenta il cosiddetto</cite> prompt<cite>, che su OS X mostra tipicamente il nome del computer, la cartella dove ci si trova e il nome dell'utente. L'ultimo carattere del</cite> prompt <cite>è</cite> <code>$</code><cite>. Nel Terminale tutti i comandi si inseriscono dopo</cite> <code>$</code><cite>.</cite>

<cite>Premessa #2: perché</cite> bash <cite>invece di Automator o AppleScript? Beh, innanzitutto perché </cite>bash <cite>è multipiattaforma e quindi funziona su Linux e, volendo, anche su Windows. E poi perchè mi piace!</cite>

<cite>Lanciamo quindi il Terminale (magari configurandolo, io consiglio almeno di aumentare le dimensioni della finestra a circa 100x40 per stare comodi) e iniziamo a lavorare.</cite>

<cite>Creiamo innanzi tutto una cartella</cite> <code>~/bin</code><cite>, dove il carattere</cite> <code>~</code> <cite>(</cite>tilde<cite>,</cite> <code>ALT+5</code><cite>) indica per convenzione sui sistemi basati su Unix la cartella Inizio dell'utente. Per farlo da Terminale, digitiamo (</cite><strong>ATTENZIONE</strong><cite>, come già detto</cite> <code>$</code> <cite>indica solo l'ultimo carattere del </cite>prompt <cite>e NON va mai inserito nei comandi seguenti):</cite>

<code>
$ cd
$ mkdir bin
$ chflags hidden bin
$ cd bin
</code>

<cite>(il primo comando serve per essere sicuri di partire dalla cartella Inizio, il secondo crea una nuova cartella</cite> <code>bin</code><cite>, il terzo la nasconde al Finder e l'ultimo comando ci fa spostare nella cartella</cite> <code>bin</code><cite>).</cite>

<cite>In realtà possiamo creare una qualunque cartella dove salvare i nostri comandi</cite> <code>bash</code><cite>, o usarne una già esistente. Però</cite> <code>bin</code> <cite>rispetta le convenzioni Unix e mi sembra preferibile.</cite>

<cite>A questo punto editiamo con</cite> <code>nano</code> <cite>(un</cite> editor<cite> testuale molto semplice) il file</cite> <code>cambia_permessi</code> <cite>che conterrà i comandi veri e propri:</cite>

<code>
$ nano cambia_permessi
</code>

<cite>Non preoccupatevi per l'interfaccia di</cite> <code>nano</code><cite>, qui dobbiamo usare solo due comandi,</cite> <code>CTRL+O</code> <cite>per salvare il file editato e</cite> <code>CTRL+X</code> <cite>per uscire da</cite> <code>nano</code><cite>. Dimenticavo:</cite> <code>CTRL+O</code> <cite>significa premere contemporaneamente il tasto</cite> <code>CTRL</code><cite>, l'ultimo in basso a sinistra sulle tastiere Mac (nei portatili è a fianco di</cite> fn<cite>), e il tasto</cite> O<cite>. Analogamente per</cite> <code>CTRL+X</code><cite>.</cite>

<cite>Torniamo al nostro file</cite> <code>cambia_permessi</code><cite>. A questo punto bisognerebbe inserire a mano le linee mostrate qui sotto. Ma OS X è furbo, e il copia e incolla funziona anche in</cite> <code>nano</code> <cite>all'interno della finestra del Terminale. Quindi basta selezionare le linee qui sotto (fino alla riga con i</cite> <code>#####################</code><cite>), copiare e poi spostarsi nella finestra del Terminale con</cite><code> nano</code> <cite>attivo ed incollare:</cite>

<code>
#!/bin/bash
#
DIR=~/public/scansioni/ 
FILES=* 
PERM=a+rw
# 
if [ -d $DIR ]; then
    cd $DIR
    chmod -R $PERM $FILES
    cd
fi
#########################
</code>

<cite>Salviamo subito lo</cite> script <cite>premendo</cite> <code>CTRL+O</code> <cite>e poi usciamo da</cite> <code>nano</code> <cite>con</cite> <code>CTRL+X</code><cite>.</cite>

<cite>Cosa significano questi comandi? Il più importante è</cite> <code>chmod</code><cite>, che cambia i permessi per tutti gli utenti, in modo che tutti i file nella cartella siano leggibili e scrivibili. Il resto è contorno.</cite>

<cite>Quello che è ancora più importante è che lo</cite> script <cite>è</cite> parametrico<cite>: basta cambiare i valori di</cite> <code>DIR</code><cite>,</cite> <code>FILES</code> <cite>e</cite> <code>PERM</code> <cite>per adattarlo alle proprie esigenze.</cite>

<cite>Per esempio, se la cartella</cite> <code>~/public/scansioni/</code> <cite>contenesse vari tipi di file e si volessero cambiare i permessi dei soli file pdf lasciando gli altri file invariati, la riga</cite> <code>FILES=*</code> <cite>diventerebbe</cite> <code>FILES=*.pdf</code><cite>.</cite>

<cite>Non è finita. Controlliamo prima di tutto di avere veramente fatto tutto bene con il comando:</cite>

<code>
$ cat cambia_permessi
</code>

<cite>che dovrebbe mostrare sullo schermo lo</cite> script <cite>appena inserito.</cite>

<cite>Poi, per semplificarci la vita, rendiamo lo</cite> script <cite>eseguibile con il comando:</cite>

<code>
$ chmod u+x cambia_permessi
</code>

<cite>Da notare che quest'ultimo comando rende eseguibile lo</cite> script <cite>solo all'utente che sta usando il Mac al momento. I motivi li tralascio per brevità, ma credetemi, è meglio fare cos&#236;.</cite>

<cite>Un ultimo passo necessario. In questo momento, per cambiare i permessi dei file nella cartella</cite> <code>~/public/scansioni/</code><cite>, bisogna eseguire a mano il comando da Terminale:</cite>

<code>
$ ~/bin/cambia_permessi
</code>

<cite>Ciò è sicuramente molto più comodo di cambiare manualmente i permessi ai file, ma si può fare di meglio.</cite>

<cite>Qui entra in gioco</cite> <code>cron</code><cite>, uno strumento che esegue comandi stabiliti dall'utente a intervalli di tempo predefiniti. Sembra una cosa cretina, ma permette di far fare al computer un mare di cose automaticamente. Però la sintassi di</cite> <code>cron</code> <cite>è orrenda. E questo</cite> post <cite>sta diventando troppo lungo.</cite>

<cite>Cercherò di sintetizzare ma se qualcuno è interessato posso scrivere qualcosa in proposito.</cite>

<cite>Diciamo che vogliamo che ogni cinque minuti lo</cite> script <code>cambia_permessi</code> <cite>venga eseguito automaticamente. Dal solito Terminale dobbiamo allora eseguire i comandi:</cite>

<code>
$ export EDITOR=/usr/bin/nano
$ crontab -e
</code>

<cite>La prima riga serve a evitare di usare l'</cite>editor <cite>preimpostato,</cite> <code>vi</code><cite>. Un vero reperto archeologico che sarebbe bene dimenticare una volta per tutte.</cite>

<cite>Dovrebbe invece aprirsi il solito</cite> <code>nano</code><cite>, in cui incolleremo <strong>esattamente</strong> la riga seguente:</cite>

<code>
05	*	*	*	*	~/bin/cambia_permessi
</code>

<cite>(prima e dopo ciascun asterisco è presente una tabulazione)</cite>

<cite>Salviamo con</cite> <code>CTRL+O</code> <cite>e usciamo con</cite> <code>CTRL+X</code>.

<cite>Ovviamente se vogliamo che lo</cite> script <cite>venga eseguito più o meno frequentemente, basta cambiare il valore</cite> <code>05</code> <cite>in, diciamo,</cite> <code>02</code> <cite>o</cite> <code>15</code> <cite>(per eseguire lo</cite> script <cite>ogni 2 o 15 minuti).</cite>

<cite>Abbiamo finito. Sembra complicato, ma credetemi, è molto più lungo da leggere (e da scrivere!) che da mettere in pratica.</cite>

Aggiungo solo una piccola nota per gli inesperti di Terminale che si scontrano con <code>cron</code> (e, su Snow Leopard, con <code>launchd</code>). Una scorciatoia artigianale consiste nel definire un evento ricorrente dentro iCal e, nelle informazioni relative, stabilire che in occasione dell'evento va eseguito uno <cite>script</cite>.