---
title: "Vicende di quart'ordine"
date: 2009-11-15
draft: false
tags: ["ping"]
---

Ci si <a href="http://www.macworld.it/blogs/ping/?p=3068&amp;cpage=1#comment-37736" target="_blank">chiedeva</a> qualche giorno fa il perché delle <cite>sassate</cite> a Microsoft.

Rileggendo il mio <i>post</i>, vedo che ho inventato due domande su Windows 7, il prodotto, non su Microsoft, l'azienda. Le risposte sono facilmente verificabili in rete e non mi pare di avere distorto alcunché. Anche la dichiarazione di Phil Schiller sul <cite>solito Windows</cite> è <a href="http://www.businessweek.com/magazine/content/09_43/b4152000782247.htm" target="_blank">un fatto di cronaca</a> e non una invenzione.

Se vogliamo parlare dell'azienda, c'è un aneddoto interessante dei giorni scorsi.

Premessa: Apple crea una certa quantità di <i>software</i> libero, a disposizione della comunità per essere scaricato, installato, migliorato. Il grande dono del software libero è che chi lo migliora poi deve condividere i miglioramenti con tutti; le cose buone che Apple fa su WebKit vanno a beneficio di Google che scrive Chrome, di Linux con Konqueror, dell'ultimo dei programmatori che vuole scrivere un <i>browser</i> e cerca un motore già pronto. Chi vuole <i>software</i> libero con lo zampino di Apple va su <a href="http://www.macosforge.org/" target="_blank">Mac OS Forge</a> oppure sulla <a href="http://www.opensource.apple.com/" target="_blank">pagina Open Source del sito Apple</a> e fa man bassa. Vuoi il cuore di Mac OS X? Prendi e scarichi, gratis.

Adesso vediamo il percorso con cui Microsoft ha creato nei giorni scorsi una <i>utility</i> <i>open source</i>. Mica il cuore di un sistema operativo; una <i>utility</i>.

Prima di tutto si crea l'<i>utility</i> <a href="http://blogs.technet.com/ieitpro/archive/2009/10/26/windows-7-usb-dvd-download-tool.aspx" target="_blank">Windows 7 Usb/Dvd Download Tool</a>, che serve a facilitare l'installazione di Windows 7 su <i>netbook</i> dotati di Windows Xp. L'<i>utility</i> viene distribuita chiusa e proprietaria, con licenza di uso di Microsoft.

Poi un programmatore indipendente <a href="http://www.withinwindows.com/2009/11/06/microsoft-lifts-gpl-code-uses-in-microsoft-store-tool/" target="_blank">scopre pezzi di codice open source</a>, scritto da altri che non sono Microsoft, dentro l'<i>utility</i>. Niente di male, in sé; basterebbe pubblicare l'<i>utility</i> come <i>open source</i>, che appunto consente di beneficiare del buon lavoro altrui. Microsoft invece pubblica l'<i>utility</i> con la propria licenza <i>chi-tocca-muore</i>. Niente di male in sé (è una normale licenza come quelle di qualsiasi altra <i>software house</i>), se il <i>software</i> fosse effettivamente proprietario e non contenesse codice libero.

La notizia si diffonde e Microsoft ritira il <i>software</i>, per verificare. E poi <a href="http://port25.technet.com/archive/2009/11/13/update-on-the-windows-7-download-tool-or-microsoft-to-open-source-the-windows-7-download-tool.aspx" target="_blank">annuncia</a>: è vero, dentro il <i>software</i> c'è codice libero, ma non abbiamo fatto apposta. Lo abbiamo dato da realizzare all'esterno e poi non abbiamo controllato bene. Settimana prossima lo ripubblichiamo come <i>open source</i> e poi staremo più attenti.

Se il <i>software</i> fosse stato pubblicato subito in modo libero, nessuno avrebbe avuto problemi. L'<i>utility</i> è gratis, quindi il peggio che gli possa capitare nell'essere <i>open source</i> è che qualcuno la migliori.

Questa sarebbe Microsoft? <i>Non abbiamo fatto apposta</i>, <i>lo abbiamo dato fuori</i>, <i>staremo più attenti&#8230;</i> sa di azienda furbetta di quart'ordine. Se la mia resa dell'accaduto sembra esagerata o di parte, non c'è che leggere gli originali.