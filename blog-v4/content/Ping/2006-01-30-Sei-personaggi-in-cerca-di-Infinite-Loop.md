---
title: "Sei personaggi in cerca di Infinite Loop"
date: 2006-01-30
draft: false
tags: ["ping"]
---

Il <em>commando</em> dell&rsquo;All About Apple Club ha finalmente messo online la prima puntata del <a href="http://www.allaboutapple.com/speciali/s_francisco_2006.htm" target="_blank">resoconto fotografico</a> dedicato alla loro visita al quartier generale Apple. Mille foto, simpatia al cubo e racconto dettagliato. Imperdibile.