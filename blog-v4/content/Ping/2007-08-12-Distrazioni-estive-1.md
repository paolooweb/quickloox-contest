---
title: "Distrazioni estive / 1"
date: 2007-08-12
draft: false
tags: ["ping"]
---

Non c'entra con il Mac e però l'attenzione ai font fa parte, come dire, del retroterra culturale. Questo articolo sul <a href="http://www.nytimes.com/2007/08/12/magazine/12fonts-t.html?ex=1344571200&amp;en=86b63388e4ee637c&amp;ei=5124&amp;partner=permalink&amp;exprod=permalink" target="_blank">cambio di font nella segnaletica stradale statunitense</a> è curioso e interessante.