---
title: "Troppo modesti in Palm"
date: 2002-04-10
draft: false
tags: ["ping"]
---

Stranamente, le specifiche di Palm Desktop non raccontano che...

Palm disconosce ufficialmente qualsiasi supporto di qualsiasi porta seriale in Palm Desktop 4. Non vanno creduti; sono troppo modesti.
Il programma sa sfruttare un driver chiamato AppleSCCSerial.kext che, se il computer contiene una porta seriale, la riconosce e ci dialoga.
Non potrebbe essere diversamente, d’altronde, perché - a parte le ultimissime macchine di Apple - il modem interno veniva descritto al sistema come una porta seriale attaccata al modem. Se non ci fosse stato un modo di usarle, Mac OS X non sarebbe mai riuscito a usare il modem interno di machine non nuovissime (quelli nuovi sono dispositivi Usb) e invece lo fa.
Ancora una volta non bisogna credere alle software house. Ma eccezionalmente ci guadagnamo.

<link>Lucio Bragagnolo</link>lux@mac.com