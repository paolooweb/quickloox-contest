---
title: "Al (mio) borsino del (mio) software"
date: 2008-12-01
draft: false
tags: ["ping"]
---

BBEdit in salita netta. Tex-Edit Plus ha perso molto del suo valore.

Safari stabile. Skype con tendenza al rialzo. Firefox sospeso per eccesso di lentezza.

Investire sul Terminale. Incetta di iChat, conservare Adium.

Conservativi su World of Warcraft. Mailsmith è una <em>blue chip</em>, un titolo sicuro.

Sto comprando allo scoperto tutte le Snow Leopard che riesco a trovare. :-)