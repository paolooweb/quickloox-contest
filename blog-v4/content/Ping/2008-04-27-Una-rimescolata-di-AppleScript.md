---
title: "Una rimescolata di AppleScript"
date: 2008-04-27
draft: false
tags: ["ping"]
---

Lo script di oggi mescola gli elementi di una lista. Per prima cosa ci vuole una lista:

<code>set unaLista to {"a", "b", "c", "d", "e"}</code>

La lista viene mescolata tramite una funzione. Il nome della funzione sarà <code>mescola</code>. Questo comando serve a fare eseguire la funzione:

<code>mescola(unaLista)</code>

E ora arriva la funzione.

<code>
on mescola(unaLista)
	script o
		property lista : unaLista
	end script
	
	set lunghezzaMeno1 to (count unaLista) - 1
	repeat with i from 1 to lunghezzaMeno1 + 1
		set j to (random number lunghezzaMeno1) + 1
		set v to item i of o's lista
		set item i of o's lista to item j of o's lista
		set item j of o's lista to v
	end repeat
end mescola
</code>

La parte <code>script&#8230; end script</code>, che contiene una <code>property</code>, definisce una proprietà dello script, un elemento su cui lo script agisce: in questo caso è l'argomento della funzione, cioè unaLista (è complicato, ma non serve per fare AppleScript elementare, quindi non preoccupiamoci troppo).

Il resto è un ciclo che fa il lavoro sporco e rimescola la lista. Non lo spiego perché è intricato, ma è un ciclo. È sufficiente seguirlo una parola per volta con il tempo e l'attenzione dovuta per capirlo.

Quest'ultimo comando è strumentale e serve solo a mostra la lista riordinata:

<code>display dialog unaLista as string</code>

Riepilogo l'intero script qui sotto. Buone prove!

<code>
set unaLista to {"a", "b", "c", "d", "e"}

mescola(unaLista)

on mescola(unaLista)
	script o
		property lista : unaLista
	end script
	
	set lunghezzaMeno1 to (count unaLista) - 1
	repeat with i from 1 to lunghezzaMeno1 + 1
		set j to (random number lunghezzaMeno1) + 1
		set v to item i of o's lista
		set item i of o's lista to item j of o's lista
		set item j of o's lista to v
	end repeat
end mescola

display dialog unaLista as string
</code>