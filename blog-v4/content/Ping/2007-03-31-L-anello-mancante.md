---
title: "L'anello mancante"
date: 2007-03-31
draft: false
tags: ["ping"]
---

Una delle curiosità che non ero mai riuscito a togliermi è il perché Apple abbia registrato, e da un bel po', <a href="http://www.mammals.org" target="_blank">mammals.org</a>.

Finalmente ho trovato una gola abbastanza profonda e sufficientemente vicina ad Apple per capire.

Mentre stiamo ad aspettare Leopard (Mac OS X 10.5) e va sui tavoli di progettazione Lion (Mac OS X 10.6), c'è chi pensa a Mac OS XI (Mac OS X 11.0).

Non è un caso che la parte open source si chiami Darwin. Né che l'interfaccia grafica sia Aqua. I prossimi animali in codice saranno appunto <em>mammals</em> (mammiferi), però marini.

Esiste un piano per sostituire Microsoft nel dominio dei desktop. Intorno al 2010, Lion cederà il posto a Sea Lion, per un rinnovamento radicale di tutta l'architettura di sistema, un vero e proprio anello mancante dell'evoluzione di Mac OS X; seguiranno Sea Otter, Manatee e Dolphin, intervallati ciascuno tra diciotto e ventiquattro mesi; sarà quasi il 2020 quando apparirà Whale, con caratteristiche attualmente fantascientifiche di autoriparazione e supporto dinamico attivo alle operazioni quotidiane.

Proprio per il 2020, è previsto che l'ultima versione di Mac OS XI sia quella che segnerà il sorpasso definitivo su Microsoft. Non per niente si chiamerà Killer Whale, l'orca assassina.

Ho cercato di estorcere informazioni in più, senza successo. Apparentemente, neanche in Apple hanno fatto programmi oltre queste date.

Di sicuro, sarà un'epoca in cui nessuno più abboccherà agli ami di Microsoft.