---
title: "L’underrating di iCal"
date: 2003-11-23
draft: false
tags: ["ping"]
---

Adesso si candida a diventare il cron con interfaccia umana

iCal viene snobbato dai più, perché è semplice e diretto. Ma sottovalutarlo è da sciocchi perché tomo tomo cacchio cacchio si arricchisce di funzioni nuove e non di poco conto.

Nessuno lo ha notato ma, per esempio, ora a un evento può essere associato non solo l’invio di una mail o un allarme sonoro, ma anche l’apertura di un documento.

Questo documento può essere un AppleScript, o uno script di shell, o qualunque altra cosa. Vale a dire che iCal ora permette con facilità totale di programmare pressoché qualsiasi tipo di attività sul sistema.

Chi finora gli ha dato un voto basso farà bene a ripensarci.

<link>Lucio Bragagnolo</link>lux@mac.com