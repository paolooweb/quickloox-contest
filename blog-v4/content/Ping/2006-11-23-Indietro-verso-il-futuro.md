---
title: "Indietro verso il futuro"
date: 2006-11-23
draft: false
tags: ["ping"]
---

Macworld mi ha gentilmente passato da collaudare un ricevitore <a href="http://tven.terratec.net/modules.php?op=modload&amp;name=News&amp;file=article&amp;sid=236" target="_blank">Cynergy Hybrid T Xs</a> di Terratec, con software EyeTv di <a href="http://www.elgato.com" target="_blank">Elgato</a>, per la televisione analogico e digitale terrestre, grande quanto basta (cioè piccolo) per collegarlo in una porta Usb e fare fatica a crederci.

Mi ci sto mettendo e ci vorrà un po'. Non è televisione fine a se stessa ma un mondo parzialmente nuovo, dove puoi tornare indietro a piacimento e rivedere una cosa trasmessa il minuto prima, registrare su disco rigido, vedere tutto in una finestrella intanto che lavori&#8230; sarà la curiosità dell'oggetto, suggestivo da vedere e provare, ma devo dire che, fruita in questo modo, anche la bistrattata televisione tradizionale - che a casa mia non ha più voce in capitolo da anni, salvo attacchi alle Torri Gemelle e Olimpiadi - ritrova una qualche attrattiva.