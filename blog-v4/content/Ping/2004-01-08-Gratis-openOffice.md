---
title: "Gratis Open(Office)"
date: 2004-01-08
draft: false
tags: ["ping"]
---

Dove non arriva Mac OS X ci pensa X11

Lavoro con il testo, spesso facendo il collaboratore o gestendo collaboratori. In vent’anni non ho mai avuto il problema dei formati e l’ultima versione di Word che abbia mai usato è la 5.1.

Ieri mi è arrivato un documento in formato StarOffice.

Avevo installato X11 insieme a Panther e così non mi sono perso d’animo. Non avevo modo di scaricare gli ottanta mega dell’installazione, ma su un vecchio Cd avevo una versione un po’ vecchia di OpenOffice.

Ci ho provato ed è andata benissimo. <link>OpenOffice</link>http://www.openoffice.org non ha proprio il look and feel di Macintosh ed è un oggetto tanto potente quanto confuso da usare. Tuttavia aprire alla perfezione il file e salvarlo alla perfezione in un formato pronto per Mac OS X è stata questione di secondi.

Il bello è che, se ricordo bene, i documenti StarOffice non li apre neanche Word. Alla faccia dello standard di fatto e del monopolio.

<link>Lucio Bragagnolo</link>lux@mac.com