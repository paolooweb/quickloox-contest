---
title: "Uno, l'altra"
date: 2007-06-22
draft: false
tags: ["ping"]
---

Oggi <strong>Alessio</strong> aveva bisogno di una mano (non era vero ed è più bravo di cos&#236;, è solo che ancora non lo sapeva) e ho installato al volo <a href="http://interacto.net/" target="_blank">Uno</a>.

Entusiasma e al tempo stesso fa spavento. Altro che interfaccia alternativa, Uno va proprio a sostituire risorse di sistema con le proprie.

È pur vero che conserva un backup di quello che ha spostato e ha un comando per rimettere tutto com'era, ma io non mi azzardo a tenerlo. Come minimo, farei un restore prima di ogni e qualsiasi aggiornamento software.

Cos&#236; l'ho disinstallato al volo.

Già che sono qui: se a qualcun altro piacessero i giochini tipo indovina l'Url, eccone <a href="http://www.weffriddles.com/" target="_blank">un altro</a>.