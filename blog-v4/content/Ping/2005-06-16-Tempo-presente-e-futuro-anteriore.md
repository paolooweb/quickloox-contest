---
title: "Tempo presente e futuro anteriore<p>"
date: 2005-06-16
draft: false
tags: ["ping"]
---

Intel non è una panacea, è una roadmap<p>

Qualcuno si chiede perché Apple non faccia uscire i Mac basati su Intel subito, invece che attendere un anno e più da oggi. Una risposta è stata &ldquo;perché manca il software&rdquo;. Un corollario è &ldquo;i processori Intel sono superiori&rdquo;.<p>

La risposta è parzialissima. Per quanto riguarda il software, Apple potrebbe iniziare la transizione anche tra tre mesi, o tra sei mesi, e non avrebbe certo bisogno di aspettare di più. Le applicazioni fondamentali sarebbero già pronte e il resto seguirebbe a breve.<p>

Solo che i Mac Intel-based offrirebbero prestazioni decisamente inferiori a quelle attuali. Jobs è stato chiarissimo: i processori Intel non sono superiori a prescindere. Saranno superiori da metà-fine 2006 in avanti, non per colpo pubblicitario, ma perché Intel promette nuovi chip assai migliori di quelli attuali in prestazioni e consumi, che invece Ibm e Freescale non possono, vogliono, o sanno promettere.<p>

Oggi, qui, ora, un Mac PowerPC umilierebbe un Mac Intel. Tra diciotto mesi, stando alle previsioni, sarà il contrario. Il software si aggiornerà di conseguenza, come ha sempre fatto.<p>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>