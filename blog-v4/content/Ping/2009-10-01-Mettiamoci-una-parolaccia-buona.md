---
title: "Mettiamoci una parolaccia buona"
date: 2009-10-01
draft: false
tags: ["ping"]
---

Qualcuno mi ha contattato in privato per esprimere la propria opinione rispetto a un commento comparso di recente e contenente linguaggio, diciamo, non ortodosso.

La mia opinione è che con abbastanza ironia e intelligenza si possa fare tutto. Per esempio, a Tla Systems hanno inserito un filtro antiparolacce dentro la loro&#8230; calcolatrice <a href="http://www.pcalc.com/" target="_blank">PCalc</a> per iPhone/iPod touch.

La nuova versione viene venduta con uno sconto di nove dollari per chi acquista anche il programma per Mac OS X e il tutto diventa pubblicità arguta e ingegnosa.

Se qualcuno non fosse mai stato adolescente, con le calcolatrici si possono compiere varie operazioni aritmetiche il cui risultato, capovolgendo il visore e aggiungendo malizia, ricorda una parola allusiva. Senza offendere nessuno, che cosa si legge rovesciando <i>376006</i>? Ecco, stessa cosa.

Se si capovolge il visore a PCalc su iPhone e iPod touch, il numero piccante viene censurato. Nel <a href="http://www.dragthing.com/blog/?p=285" target="_blank">loro blog</a> gli autori ironizzano sulle procedure di approvazione di App Store: cos&#236;, sostengono, evitiamo che la calcolatrice venga vietata ai minori di 17 anni!

Ovviamente è poco più che uno scherzo. Ma è quasi più divertente mostrare la funzione di censura che usarla per lo scontato trucchetto da ufficio ai danni di una collega troppo paziente.

Il senso di tutto ciò è il commento di cui sopra mancava proprio di ironia e di intelligenza. E adesso che tutti lo hanno visto, tutti sanno quanto si possa fare meglio.