---
title: "L’errore di Word"
date: 2001-12-31
draft: false
tags: ["ping"]
---

Un programma talmente pieno di funzioni da dimenticarsi quello che serve davvero

Rubo la parola a un lettore di Misterakko, la mailing list del bravissimo e amicissimo Luca Accomazzi (non la conosci? Vergogna! Subito ad accomazzi.net a imparare) che, di fronte alla richiesta di un parere sul nuovo corso di Macworld (non conosci Macworld? Naaah...), ha detto, più o meno: bello, mi piace, ma state attenti a qualche errore di ortografia che scappa un po’ a tutti.
Ecco: il sottoscritto commette errori ben peggiori ma, quanto all’ortografia, il mio sistema di scrittura dal costo totale di quindici dollari genera testi praticamente perfetti. La maggior parte dei collaboratori di Macworld invece usa Word, programma che ignoro felicemente da anni ma costerà sicuramente palate di euro.
Sono bravissimi: sbagliano solo una cosa.
Usano un elaboratore di testi costosissimo e talmente potente che non riesce a garantire i risultati più elementari.
Bah.

lux@mac.com