---
title: "Vettori di libertà"
date: 2009-09-08
draft: false
tags: ["ping"]
---

Per quanto mi riguarda, sono totalmente d'accordo con la richiesta ad Adobe di proseguire lo sviluppo di FreeHand oppure rendere <i>open source</i> il codice.

Costituirsi in <a href="http://www.freefreehand.org/" target="_blank">organizzazione con sito apposito</a> non servirà a nulla, ma consiglio l'iscrizione alla <i>newsletter</i> a tutti gli interessati. Male che veda, si finisce per incontrare gente valida e trovare sostegno reciproco.