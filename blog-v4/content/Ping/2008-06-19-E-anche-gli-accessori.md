---
title: "E anche gli accessori"
date: 2008-06-19
draft: false
tags: ["ping"]
---

Hai scaricato Firefox. Su un Mac non puoi fare a meno di <a href="http://code.google.com/p/firefox-mac-pdf/" target="_blank">Firefox-Mac-Pdf</a>, se vuoi vedere i Pdf come fa Safari, e <a href="http://codecontortionist.com/software/mac-osx-software/multifirefox/" target="_blank">MultiFireFox 2.0</a>, che ti fa eseguire fino a cinque copie di Firefox contemporaneamente.

Infine, siccome i browser non bastano mai, <a href="http://willmore.eu/software/gmailbrowser/" target="_blank">Gmail Browser</a>. Che non è Firefox ma permette di amministrare un account Gmail in più.

C'è sempre un account Gmail in più.