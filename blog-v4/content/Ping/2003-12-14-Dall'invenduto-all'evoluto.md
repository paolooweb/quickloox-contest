---
title: "Dall’invenduto all’evoluto"
date: 2003-12-14
draft: false
tags: ["ping"]
---

E se iMac 20” fosse una mossa astuta che trasforma un problema in un vantaggio?

Trovo iMac Flat Panel 20” una macchina straordinaria, con quel display, a quel prezzo. Sono possessore felicissimo di un iMac 15” e, se ne avessi bisogno, lo cambierei volentieri con il nuovo nato, per avere la risoluzione maggiorata a 1.680 x 1.050.

Noto però che la risoluzione è indentica a quella del vecchio Cinema Display da 20” e penso: metti che il Cinema Display 20” non abbia venduto secondo le aspettative o che Apple abbia ordinato un numero di schermi superiore al fabbisogno. Trasformare gli schermi invenduti in una nuova e attraente configurazione di iMac potrebbe essere una buonissima mossa, che alleggerisce i magazzini e aumenta la scelta per noi tutti.

Magari non è così, neh? Però il pensiero è intrigante.

<link>Lucio Bragagnolo</link>lux@mac.com