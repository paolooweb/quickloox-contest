---
title: "Intelligenze superiori"
date: 2007-08-07
draft: false
tags: ["ping"]
---

Non chiudo mai le finestre che apro. Odio aspettare che si lanci il programma, andare a cercare i vecchi documenti eccetera. Il risultato è un discreto affollamento sul mio desktop.

Adesso, volendo, <a href="http://www.barebones.com/products/bbedit" target="_blank">BBEdit 8.7</a> riapre automaticamente le finestre che erano rimaste aperte al momento di chiudere il programma. Cos&#236; chiudo le finestre che so di non voler più aprire, per avere solo quelle desiderate, e l'affollamento sulla scrivania diminuisce.

Se usi un programma intelligente, puoi lavorare in modo più intelligente.