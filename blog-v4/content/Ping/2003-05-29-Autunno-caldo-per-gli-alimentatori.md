---
title: "Autunno caldo per gli alimentatori?"
date: 2003-05-29
draft: false
tags: ["ping"]
---

Sembra assurdo, ma certi prezzi potrebbero salire insieme alla febbre

Sperando che la Sars venga debellata in fretta e porti al massimo problemi di questo tipo, c’è una vaga possibilità che questo autunno veda un’impennata dei prezzi di certi componenti, per esempio gli alimentatori dei portatili.

Sembra non ci sia alcun legame, e invece sì. Tra Cina e Taiwan viene prodotto l’85% dei componenti dei computer odierni e, appunto, quasi il 100% dei componenti usati negli alimentatori dei portatili.

Se l’epidemia si aggravasse e portasse al blocco delle fabbriche per evitare contagi, non sarebbe facile né economico predisporre un’alternativa rapida e, inevitabilmente, i prezzi sarebbero destinati a salire.

Speriamo non succeda, pensando alle persone più che agli alimentatori.

<link>Lucio Bragagnolo</link>lux@mac.com