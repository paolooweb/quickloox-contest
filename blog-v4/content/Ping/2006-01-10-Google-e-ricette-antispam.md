---
title: "Il tocco di Google"
date: 2006-01-10
draft: false
tags: ["ping"]
---

Quando controlli la posta in Gmail, il servizio di posta via browser di Google, in cima all’elenco dei messaggi c’è il link a una notizia di attualità.

Se entri nella sezione Spam, in cima all’elenco dei messaggi c’è il link a una ricetta basata sullo Spam (che originalmente è carne pressata in scatola).

<em>Adoro</em> Google.