---
title: "Tutto il resto"
date: 2008-02-04
draft: false
tags: ["ping"]
---

Il <a href="http://www.giovanninoguareschi.com/23club2.htm" target="_blank">Club dei Ventitré</a> ha un nuovo iMac 20&#8221;.

Non avevo ancora installato un iMac. Non è nemmeno un'installazione. L'unico cavo degno di questo nome è quello di alimentazione; il software di sistema è già pronto, basta dare un nome e una password; attaccato un cavo Ethernet momentaneamente rubato a un Pc senza sapere niente della rete, il computer è andato da solo su Internet; pochi minuti dopo che si chiacchierava, aveva già scaricato gli aggiornamenti di sistema ed è bastato riavviare.

La vera soddisfazione, però, è aprire la confezione della tastiera. Sotto, tra l'altro, c'è una busta contenente i dischi di ripristino e le garanzie.

Sopra c'è scritto <em>Tutto il resto</em>.

Una scuola di <em>design</em> in tre parole.