---
title: "Dov'è la ciccia"
date: 2007-06-02
draft: false
tags: ["ping"]
---

<em>Where's the beef?</em>, chiedono gli anglosassoni quando si discute di quale sia il succo di una notizia.

Vedo la homepage di Apple che strilla YouTube su Apple Tv. È l'esca.

Invece, so che è nato iTunes U.

Ecco dov'è.