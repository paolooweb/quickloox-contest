---
title: "Olio di serpente"
date: 2006-07-11
draft: false
tags: ["ping"]
---

Sono veramente preoccupato dall'offerta Adsl di Alice 20 mega. Telecom può offrire quello che vuole, chiaro; e cos&#236; chiunque può scegliere quello che preferisce.

Ma venti megabit sono una dimensione totalmente sproporzionata rispetto alle esigenze dell'informatica attuale. Se fosse sfruttata interamente, questa banda riempirebbe un hard disk a settimana; se non lo fa, si usano meno di venti megabit e dunque si paga per qualcosa che non si usa.

Se fai notare la cosa, però, scende il silenzio ed è questo, che è preoccupante. Come se la gente avesse bisogno di comprare e, facendole notare che il giocattolo è inutile, si creasse uno scompenso. Oddio, e ora che cosa compro?

Qualcuno dovrà pur dirlo, che scaricare film può essere molto divertente, ma per guardare un film da due ore occorrono due ore. A meno che che non sia scaricare per scaricare, sentirsi importanti guardando i numeri che corrono, sul <em>mio</em> cavo.

A me Telecom non piace. Che vendano pure quello che vogliono. Ma prodotti, non rafforzamento dell'ego. Nel Far West lo avrebbero chiamato <em>snake oil</em>.