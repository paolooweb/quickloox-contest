---
title: "C'era una volta"
date: 2010-12-04
draft: false
tags: ["ping"]
---

Il mondo dove <i>Windows ha più programmi</i> e Mac era una piattaforma negletta, che Microsoft aveva salvato <i>comprandosi Apple</i> e altre amenità che i più sciocchi ripetono di quando in quando, con preparazione inversamente proporzionale alla prosopopea.

Adesso ricevo da <a href="http://multifinder.wordpress.com/" target="_blank">Mario</a> quanto segue:

<cite>Il Wwf ha ideato <a href="http://www.saveaswwf.com/en/" target="_blank">un nuovo formato di file</a>, che si chiama per l'appunto .wwf. È un Pdf che non si può stampare, in modo da risparmiare carta. Per ora è solo per Mac, sistema operativo che sta per scomparire.</cite> Windows version coming soon. <cite>:-)</cite>

Contemporaneamente &#8220;scopro&#8221;, buon ultimo, <a href="http://d.pr" target="_blank">droplr</a>. Condivisione fantastica, facilissima, fulminea e furba di file fino a un gigabyte, gratis. Ho guardato lo <i>screencast</i> dimostrativo sulla pagina e l'ho scaricato, per quanto <i>beta</i>, <i>subito</i>. Solo per Mac e iOS.

Se valesse qualcosa, iOS ha più programmi di Windows. In sé non vale niente e questo certifica bene ciò che per anni è stato raccontato da chi usa Windows.

Adesso, di volta, ce n'è un'altra.