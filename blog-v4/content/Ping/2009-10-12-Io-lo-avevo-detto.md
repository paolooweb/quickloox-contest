---
title: "Io lo avevo detto"
date: 2009-10-12
draft: false
tags: ["ping"]
---

PcMag.com ha condotto anche quest'anno il sondaggio <a href="http://www.pcmag.com/article2/0,2817,2352801,00.asp" target="_blank">Cell Phones Service &#38; Reliability Survey</a> tra i propri lettori americani.

Vincitore incontrastato iPhone, con nove decimi di valutazione. Il secondo in classifica fa 7,9, a più di un punto di distanza.

Leggo dall'articolo: <cite>Sorprendentemente, il punteggio complessivo di iPhone è salito dal 2008 al 2009, anche se i voti relativi a copertura cellulare, qualità delle chiamate e qualità dell'audio sono scesi! iPhone non si è comportato in modo spettacolare come telefono, ma ha ricevuto grandi punteggi come apparecchio di messaggistica, lettore musicale, lettore video e </cite>console<cite> da gioco.</cite>

Avevo provato a spiegare che cosa non è iPhone, ma non avevo trovato grande ascolto. Forse PcMag è più convincente. :-)