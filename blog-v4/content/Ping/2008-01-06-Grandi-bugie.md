---
title: "Grandi bugie"
date: 2008-01-06
draft: false
tags: ["ping"]
---

<cite>Ci sono molte più periferiche per Windows che per Mac.</cite>

Certo, a guardare le scritte sulle scatole. Poi, come ha fatto <strong>Riccardo</strong> nel suo <em>post</em> sulle piccole rivincite, ragioni con il buonsenso, vai a vedere il <em>bluff</em> e vedi che a volte sono <a href="http://morrick.wordpress.com/2007/12/30/piccole-rivincite/" target="_blank">gran balle</a>.