---
title: "Pausa di lettura"
date: 2008-03-13
draft: false
tags: ["ping"]
---

Oggi niente script, anche se è il 14. Il motivo è una piacevole eccezione.

Apple ha riveduto e aggiornato la <a href="http://developer.apple.com/documentation/AppleScript/Conceptual/AppleScriptLangGuide/introduction/ASLR_intro.html" target="_blank">AppleScript Language Guide</a>. Una settimana di ripasso per tutti. :-)