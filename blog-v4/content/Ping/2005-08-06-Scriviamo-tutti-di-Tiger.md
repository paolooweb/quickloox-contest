---
title: "Scriviamo tutti di Tiger<p>"
date: 2005-08-06
draft: false
tags: ["ping"]
---

Internet dà il meglio quando scatena la collaborazione. Come in questo caso<p>

Mentre i parolai si avvitano intorno alla supposta scarsa innovatività del Mighty Mouse (ne avessi sentito uno solo proporre, prima, di estendere lo scorrimento a direzioni diverse da quella verticale), c&rsquo;è chi fa cose utili e concrete.<p>

Per esempio, <strong>Lele</strong> segnala <a href="http://www.tigerwiki.com/index.php/Main_Page">TigerWiki</a>. Una pagina wiki dedicata a tutte le novità di Mac OS X 10.4.<p>

Wiki è una delle tecnologie che senza Internet sarebbero state impossibili. Usando un insieme di semplici comandi di testo, chiunque può modificare la pagina e aggiungere i suoi contributi, mantenendo un corretto aspetto in Html, link ipertestuali e tutto quello che fa Web.<p>

Così si può raggiungere il massimo del risultato con il minimo sforzo e le nozioni di ognuno concorrono a creare un vantaggio per tutti.<p>

Capisco che in Italia un concetto del genere faccia sollevare il sopracciglio: a forza di chiedersi <em>e io che ci guadagno?</em> nessuno muove un dito e si resta indietro.<p>

Io posso solo ringraziare Lele e vedere se le mie modeste conoscenze mi permettono di inserire qualche riga per contribuire al progetto.<p>

Per inciso: fare <a href="http://en.wikipedia.org/wiki/Wiki">wiki</a> non è difficile.<p>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>