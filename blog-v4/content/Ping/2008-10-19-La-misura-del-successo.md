---
title: "La misura del successo"
date: 2008-10-19
draft: false
tags: ["ping"]
---

iPhone ha il <em>kill switch</em>, il pulsante assassino, la possibilità per Apple di cancellare a suo piacimento programmi sul mio computer da tasca. Se ne è fatto un caso mondiale di Grande Fratello, invasione della privacy, controllo globale, l'utente come burattino nelle mani della malvagia multinazionale.

G1, quello con il sistema operativo Android di Google, ha un <em>kill switch</em> uguale uguale uguale. Questo rischio mortale per la libertà non se lo fila nessuno.