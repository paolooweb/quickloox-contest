---
title: "Fuori i secondi (bisestili)"
date: 2008-12-31
draft: false
tags: ["ping"]
---

Questo Ping non era previsto. Invece ho scoperto che Mac OS X <a href="http://developer.apple.com/documentation/Darwin/Reference/Manpages/man3/tzset.3.html" target="_blank">tratta senza problemi</a> il secondo bisestile aggiunto alla fine di quest'anno (e che viene aggiunto ogni qualche anno, l'ultima volta nel 2005. Dal 1972 è il ventiquattresimo secondo bisestile).

Windows, <a href="http://support.microsoft.com/kb/909614" target="_blank">invece</a>&#8230;

<cite>Questo articolo descrive come il servizio Windows Time tratta un secondo bisestile.</cite>


<cite>Quando il servizio Windows TIme funziona come client Network TIme Protocol (Ntp)</cite>

<em>Il servizio Windows Time non indica il valore del Leap Indicator quando riceve un pacchetto contenente un secondo bisestile (il Leap Indicator specifica se se bisogna aggiungere o togliere un secondo all'ultimo minuto del giorno attuale). Di conseguenza, dopo che è passato il secondo bisestile, il client Ntp che sta eseguendo il servizio Windows Time è un secondo avanti rispetto all'ora corretta. Questa differenza viene eliminata alla prima sincronizzazione.</em>


<cite>Quando il servizio WIndows TIme funziona come server Ntp</cite>

<cite>Non esiste alcun metodo per inserire un secondo bisestile nel servizio Windows Time</cite>.


Lo stile si vede dai dettagli&#8230; minuti. Primi e secondi.