---
title: "Psichedelia, tutte le teste ti porti via"
date: 2006-09-08
draft: false
tags: ["ping"]
---

Troppo distratto per essere attento e troppo concentrato per rendermi conto della distrazione, ho selezionato tutto il contenuto della cartella <code>Documenti</code> e mi è partito un <code>Comando-freccia in basso</code>.

Sono stati tre minuti che a confronto il <a href="http://www.rockyhorror.com/" target="_blank">Rocky Horror Picture Show</a> è una fiera dell’artigianato.