---
title: "Le non previsioni per Macworld Expo<p>"
date: 2004-12-06
draft: false
tags: ["ping"]
---

Una ovvietà contro molto gossip: chi vince?<p>

Come si avvicina Natale, la gente inizia a regalarti tutte le sciocchezze e le dicerie che girano per la Rete ad anticipare le novità che Apple presenterà o meno a Macworld Expo. Gente seria, concreta, diffidente, che non fa la carta di credito perché &ldquo;non so se su Internet è sicura e poi non si sa mai&rdquo;, beve qualsiasi idiozia scritta da siti qualsiasi. E la ripete in giro, con la finta aria scettica, ma in fondo con la soddisfazione di avere qualcosa da dire.<p>

Bene: a giugno Steve Jobs ha rilasciato un&rsquo;<a href="http://www.brighthand.com/article/Jobs_Says_There-ll_Be_No_Apple_PDA?site=Other">intervista</a>, a ribadire che Apple non ha alcuna intenzione di annunciare uno smartphone o un palmare e che, anzi, era stato messo in progetto un Pda, successivamente cassato.<p>

Se Apple presenterà qualcosa di nuovo a Macworld Expo, sarà qualcosa che ha un senso, non una sciocchezza che gira su Internet.<p>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>