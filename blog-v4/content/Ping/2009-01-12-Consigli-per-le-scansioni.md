---
title: "Consigli per le scansioni"
date: 2009-01-12
draft: false
tags: ["ping"]
---

<a href="http://pztake.blogspot.com" target="_blank">Herr Zellner</a> segnala che ora esiste una versione italiana di <a href="http://www.hamrick.com/" target="_blank">Vuescan</a> di Hamrick Software.

Vuescan risolve la compatibilità Mac con quasi tutti gli <em>scanner</em> apparsi sul pianeta Terra.

Ora ha risolto anche quello linguistico locale, nostro.