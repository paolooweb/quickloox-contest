---
title: "La volontà degli e-lettori"
date: 2009-12-14
draft: false
tags: ["ping"]
---

Finalmente è arrivato <a href="http://itunes.apple.com/it/app/kindle-for-iphone/id302584613?mt=8" target="_blank">Kindle su iPhone e iPod touch</a> anche per noi italiani.

Adesso attendo la disponibilità di Kindle per Mac, che è prevista nei prossimi mesi, e poi intendo farmi una idea della piattaforma.

Una perplessità tuttavia l'ho già maturata. Ero curioso nei confronti di Kindle, l'aggeggio <i>hardware</i>. Quando è uscita l'applicazione (inizialmente solo americana), la mia curiosità è un po' diminuita. Ora che posso teoricamente usare iPhone come lettore di <i>e-book</i>, è vicina allo zero (mi rimane l'interesse per l'interfaccia e poco altro).

Quando ci sarà Kindle per Mac, immagino che l'esistenza dell'aggeggio hadrware mi risulterà praticamente indifferente. Mi chiedo se la cosa piaccia ad Amazon e se questa punti a vendere tanti <i>e-book</i> o tanti Kindle <i>hardware</i>. Nel secondo caso, beh, se iPhone funzionasse bene come e-lettore, questione chiusa. E un iPod touch rivaleggia nel prezzo con il Kindle <i>hardware</i>.

È perfettamente ovvio che a letto sia più comodo reggere un Kindle che un iPhone. Su un autobus o un treno affollato invece è altra questione e quell'iPod touch, oltre a leggere libri, mi fa anche giocare a <a href="http://itunes.apple.com/it/app/nethack/id334281275?mt=8" target="_blank">NetHack</a>.

Questo Natale è esplorativo; nel 2010 si farà un gran parlare di libri elettronici. Ma ci sarà qualcuno ad ascoltare?