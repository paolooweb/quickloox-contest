---
title: "Chi viene a Quiliano?<p>"
date: 2005-04-25
draft: false
tags: ["ping"]
---

Un bel modo di essere in Europa e usare Mac<p>

Sabato 15 maggio si inaugura l&rsquo;All About Apple Museum: il primo museo in Europa dedicato ad Apple e naturalmente anche al Mac.<p>

Avevo detto che non sapevo se sarei riuscito a esserci ma ho due teste, una tutta tesa in avanti e l&rsquo;altra che continua a guardare indietro, e la seconda mi ha fatto un ragionamento talmente convincente che non ho saputo resistere.<p>

Il fatto che io presenzi o meno è ovviamente irrilevante. Ma se presenziassimo in tantissimi la cosa avrebbe invece tutto un altro perché.<p>

Tutte le indicazioni per arrivare alla sede del museo sono nella sezione apposita del sito di <a href="http://www.allaboutapple.com/aaa.htm">All About Apple</a>.<p>

Chi la ama (Apple), le segua!<p>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>