---
title: "L'anno che varrà"
date: 2009-01-01
draft: false
tags: ["ping"]
---

Stavo leggendo di <a href="http://lilypond.org/" target="_blank">LilyPond</a>, di cui è appena uscita la versione 2.12.1-1. Una revisione importante, con la documentazione interamente riscritta al termine di un anno di fatica, un sacco di <em>bug fix</em> e decine di nuove funzioni.

Sul sito LilyPond viene dato come per tutti e invece è doppiamente per pochi. Una volta perché produce notazione musicale di altissimo livello e io faccio fatica a trovare il <em>do</em> centrale sulla Keystation. Una seconda volta perché, in funzione della configurazione, installarlo può essere veramente complicato.

La prima discriminazione è destinata a rimanere, come per LaTeX. Produrre spartiti ineccepibili, cos&#236; come tipografia perfetta, è cosa per individui inevitabilmente aghi nel pagliaio dell'approssimazione e della beceraggine.

La seconda invece, sparisse tanto per LilyPond quanto per dozzine di programmi <em>open source</em> di straordinaria caratura, sarebbe il più bel risultato informatico auspicabile per il 2009. Un anno che diventerebbe di valore inestimabile.

Me lo auspico e dopotutto, se non parliamo di spartiti nel giorno del Concerto, quando?