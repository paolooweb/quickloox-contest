---
title: "Esperienze mistiche"
date: 2007-05-27
draft: false
tags: ["ping"]
---

Seduto davanti a un Cinema Display da 30 pollici, ho preso il controllo remoto prima di un PowerBook 12&#8221; e poi di un MacBook Pro 17&#8221; (entrambi consenzienti).

Le finestre con dentro i desktop remoti dei due portatili mi hanno mostrato perfettamente le proporzioni e le possibilità di uno schermo da 12&#8221; e uno da 17&#8221; rispetto a un 30&#8221;.

Sono fermamente un portatilista e credo di restarlo ancora per un po' (anzi, sono curioso: se uscisse una vera tastiera da collegare a un iPhone&#8230;). Se lavorassi su un desktop e avessi una scrivania degna di questo nome, tuttavia, sarei un folle a non possedere uno schermo da 30&#8221;.

L'incremento di produttività è spaziale. Voglio dire che, al prezzo attuale, per un professionista quello schermo si ammortizza in un anno e forse anche meno. Il resto è tutto benessere tecnologico.