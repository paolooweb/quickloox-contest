---
title: "Parola di Moreno<p>"
date: 2005-02-14
draft: false
tags: ["ping"]
---

Mac OS X rende felici, tant&rsquo;è vero che<p>

Quanto sotto mi è stato inviato da <a href="mailto:morekmac@mac.com">Moreno Bonechi</a>. Lo trovo istruttivo e molto utile per quando si discute di scuola e computer. E sì, sono un po&rsquo; più felice anch&rsquo;io, adesso.<p>

<cite>Ciao Lucio,</cite><p>

<cite>sono un utente Apple solo da quando esiste OS X, quindi non sono in grado di fare confronti con il passato ma sento di poter dire che da quando ho iniziato ad usare Apple il tempo che passo sul Pc è anche più divertente di quello che per me è sempre stato, te lo dice uno che è partito dal Commodore Vic 20&hellip;</cite><p>

<cite>Da due anni ho ottenuto una docenza a contratto presso la facoltà di lettere per la materia di Informatica e Comunicazione, questo mi ha fatto capire sempre di più quanto Apple, se saputa raccontare, sia in grado far usare i computer alle persone e a farli essere felici di questo. Da quando insegno ho &ldquo;convertito&rdquo; al mac tanti studenti e, dopo lo scoglio iniziale, devo dire che non è stato poi così difficile. Le fasi comuni a tutti gli studenti di ogni corso più o meno sono state queste:</cite><p>

<cite>1) hanno visto il mio PB 17&rdquo; e hanno subito immediatamente il suo fascino</cite><br>
<cite>2) hanno seguito le mie lezioni presentate con Keynote e hanno iniziato a chiedermi come avessi fatto a realizzare certe transizioni da slide a slide con Power Point, al che è seguita la mia divulgazione dell&rsquo;esistenza di Keynote</cite><br>
<cite>3) hanno scaricato le dispense da una sezione riservata del mio account .Mac e hanno iniziato a chiedere cosa fosse quello strano indirizzo e che c&rsquo;entrava col Mac</cite><br>
<cite>4) mi hanno tutti chiesto un&rsquo;opinione sul OS X in quanto alcuni loro amici (che poi ho scoperto avere OS 9) dicevano che con Mac si trovavano &ldquo;isolati&rdquo; nello scambio file con tutti gli altri che naturalmente avevano Windows</cite><br>
<cite>5) ho spiegato loro che OS X in questo non gli avrebbe causato nessun problema nel 99,9% dei casi e che soprattutto sarebbero stati virus free e a quel punto si sgranavano gli occhi</cite><br>
<cite>6) li ho indirizzati verso un Apple Center dove lavora un mio amico che gli ha procurato dei buoni usati (ancora il Mac mini non esisteva&hellip;)</cite><br>
<cite>7) li ho assistiti nei primi passi col nuovo sistema, gli ho fatto provare iLife (che secondo me ha favorito anche la loro creatività e visto che seguono il corso di laurea in Comunicazione linguistica e multimediale di più non potevo chiedere) e li ho aiutati nel configurare la connessione ADSL o GPRS (molti di loro sono qui a Firenze in affitto)</cite><br><p>

<cite>Adesso che non perdono più tempo prezioso avendo a che fare con i virus le loro facce sono molto più sorridenti ed i lavori che mi portano più &ldquo;originali&rdquo;. Solo qualcuno di loro si lamenta perché non riesce a vedere le partite con RossoAlice, in quel caso gli esorto a scrivere alla Telecom e a non buttare il vecchio Pc con Windows.</cite><p>

<cite>La settimana scorsa durante il consiglio del corso di laurea ho presentato una richiesta di finanziamento per l&rsquo;iBook Wireless Mobile Classroom, qualcuno mi ha guardato un po&rsquo; storto ma i due professori ai quali ho fatto acquistare due PowerBook hanno sorriso ed uno è il presidente del corso di laurea quindi c&rsquo;è speranza ;-)</cite><p>

<cite>Ho ritenuto che sapere queste cose magari avrebbe fatto più felice 
anche te.</cite><p>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>