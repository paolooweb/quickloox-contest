---
title: "Neologismo d’autore"
date: 2002-03-20
draft: false
tags: ["ping"]
---

Ecco un termine che non mancherà più nel mio vocabolario informatico

Finora è sempre stato un problema, il come chiamarli.
Windowsisti. Windowsiani. Wintelli. Finestrati (Elio docet). Piciisti. Gatesiani. Pentiumari. Celeroni. Windowsari. Utenti Windows. Nipoti di Bill. Beige People. Eccetera.
Poi è arrivata Erica a illuminarmi; a lei si deve l’ispirazione e financo il copyright.

Dossici.

<link>Lucio Bragagnolo</link>lux@mac.com