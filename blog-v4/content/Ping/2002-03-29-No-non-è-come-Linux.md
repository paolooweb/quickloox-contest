---
title: "No, non è come Linux"
date: 2002-03-29
draft: false
tags: ["ping"]
---

Mac OS X è un poco più profondo di quello che appare

Sento un (bravissimo) utilizzatore di Linux che sbotta “Sì, ma alla fine Mac OS X è uno Unix, come Linux, con sopra un’interfaccia utente bella da vedere”.
Non è proprio così. Per esempio Mac OS X comprende QuickTime, che è una straordinaria architettura mediale realizzata da Apple in anni di lavoro. Comprende Quartz, il sistema di rappresentazione grafica bidimensionale che permette di creare un file Pdf in qualsiasi momento a partire da qualunque documento. E non è tutto; a scavare in profondità, si scopre che Darwin (la parte open source di Mac OS X) è una versione di FreeBsd, ma il kernel arriva da quello Mach e dal lavoro che ci hanno fatto sopra in NeXt ai tempi di Steve Jobs. Più completa e performante di quanto si possa trovare in un FreeBsd puro.
Insomma, dire che Mac OS X è come Linux è un po’ come dire che un fuoristrada è una station wagon con le gomme più grosse. Bisognerebbe dare un’occhiata anche al cambio.

<link>Lucio Bragagnolo</link>lux@mac.com