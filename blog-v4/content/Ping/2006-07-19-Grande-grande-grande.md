---
title: "Grande, grande, grande"
date: 2006-07-19
draft: false
tags: ["ping"]
---

Ho la sensazione che non esista un limite assoluto (non pratico, almeno) alla dimensione di un file QuickTime. Ma non riesco a trovare una pezza d'appoggio autorevole e definitiva. Qualcuno sa darmi una mano?

Grazie in anticipo!