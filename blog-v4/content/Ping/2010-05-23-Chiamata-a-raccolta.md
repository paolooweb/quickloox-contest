---
title: "Chiamata a raccolta"
date: 2010-05-23
draft: false
tags: ["ping"]
---

Probabilmente la parola definitiva sugli esempi di programmazione web con Html5 è <a href="http://html5watch.tumblr.com/" target="_blank">Html5 Watch su Tumblr</a>. Ci sono anche un esempio preso dal <a href="http://www.apple.com/why-mac/" target="_blank">sito Apple</a> e <a href="http://diveintohtml5.com/forms.html" target="_blank">un brano</a> dell'imperdibile <a href="http://diveintohtml5.com/" target="_blank">Dive Into Html5</a>.

Uno ha persino <a href="http://general-metrics.com/Safari/" target="_blank">ricreato l'interfaccia di Safari</a> in Html5 e Css3.