---
title: "Il senso delle proporzioni"
date: 2006-11-02
draft: false
tags: ["ping"]
---

Correva l'inizio del 2002 e Apple annunciava risultati 2001 nell'ordine di una perdita di 25 milioni di dollari a fronte di un fatturato annuo di 5,36 miliardi di dollari.

La bolla della New Economy si era sgonfiata, pochissimi avevano voluto il Power Macintosh G4 Cube e i dirigenti passavano il tempo a spiegare ad analisti e giornalisti come avevano ristrutturato Apple in modo che fosse in grado di sopravvivere fatturando sei miliardi di dollari l'anno.

Cinque anni dopo, Apple si aspetta &#8212; il prossimo trimestre &#8212; di incassare questa stessa cifra, in soli tre mesi.

Il senso delle proporzioni pare puntato in direzione della crescita. :)