---
title: "Che bello sbagliare"
date: 2010-02-20
draft: false
tags: ["ping"]
---

A tutt'oggi il problema dell'informatica non è l'interfaccia, lo hardware, il software, il costo; è la paura di sbagliare.

Vedo legioni di persone che stanno lontane da grandi cose che potrebbero fare unicamente per insicurezza, o timore dell'ignoto, o perché <cite>non si sentono all'altezza</cite>. C'è una certa correlazione proporzionale con l'età &#8211; più una persona è anziana più tende a muoversi cauta &#8211; ma il male è diffuso anche tra i giovani e in forma ben più grave, perché se un ragazzo o una ragazza non hanno un pizzico di incoscienza, dove vanno?

Mentre nel mondo materiale lo sbaglio è sempre associato a un danno valutabile, in quello digitale non c'è problema di maltrattare bit. Se qualcosa non funziona, non importa; si riparte da zero. Buttare informazione sbagliata non porta spese né vergogna.

Anzi, nel mondo digitale gli errori sono da accogliere quasi con sollievo. Racconta tale Jimmy Wales che iniziò la propria carriera lavorativa cercando di varare un motore di ricerca. Ma in tre mesi gli hacker cinesi glielo demolirono. Fallimento.

Allora ebbe l'idea di creare una enciclopedia gratuita su Internet, scritta da esperti. Ci spese 250 mila dollari senza combinare niente. Fallimento.

Cos&#236; si affidò a un'idea molto stupida: un'enciclopedia scritta dalle persone normali. Oggi Wikipedia è uno dei primi dieci siti del mondo.

Cos&#236; Wales <a href="http://www2.tbo.com/content/2010/feb/19/wp-wikipedia-creator-had-lots-of-earlier-failures/" target="_blank">consiglia a tutti</a> di fallire senza problemi, a patto di divertirsi lungo la strada. Una sana gestione dei fallimenti porta al successo.

Che c'entra con il Mac? Forse niente, ma c'entra con la metà di quelli che stanno leggendo, me compreso. Sono l&#236; davanti a una finestra di Terminale, a un documento bianco da riempire, a qualcosa da imparare. Forza. C'è solo da guadagnarci.