---
title: "Quando non basta mai"
date: 2008-07-01
draft: false
tags: ["ping"]
---

In questo mondo affascinante e trito della tecnologia, le cose che mi fanno andare fuori di testa sono quelle talmente dense e piene di riferimenti e punti di partenza che, prima ancora di scegliere una direzione di approfondimento, rimango basito al vedere tutta la complessità e meravigliato dalla quantità di nozioni misteriose eppure accessibili che concorrono a formare qualcosa magari di estremamente semplice.

Credo proprio che <a href="http://www.spore.com" target="_blank">Spore</a> diverrà una di queste.

Intanto è un gioco non-gioco, di quelli che ti diverti e intanto ti viene da pensare in direzioni nuove e inaspettate.

Secondo, chi ha iniziato a dissezionare il Creature Creator ha scoperto che nelle immagini delle creature <a href="http://nedbatchelder.com/blog/200806/spore_creature_creator_and_steganography.html" target="_blank">sono presenti informazioni steganografate</a> (leggere anche i commenti), nascoste all'interno dei byte dell'immagine (mai visitato il <a href="http://www.jwz.org/" target="_blank">sito di Jamie Zawinski</a>?). Queste cose si possono vedere con vari strumenti relativamente semplici come un editor esadecimale oppure con un po' di scripting in Python, ed è quasi un ulteriore gioco dentro il gioco.

Di gioco in gioco, ci si avventura anche dentro un file .plist per fare girare il demo di Creature Creator <a href="http://www.macosxhints.com/article.php?story=20080622112907382" target="_blank">anche su Mac OS X 10.4.11</a>.

Da un gioco a un altro e continui a scoprire cose nuove. Questo è il computer.