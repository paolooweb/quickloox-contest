---
title: "Il Prozac non è nessuno"
date: 2007-05-27
draft: false
tags: ["ping"]
---

Lo ha già anticipato <strong>Mario</strong> pochi giorni fa, ma può darsi che a qualcuno sia sfuggito il suo commento.

Attenzione a <a href="http://www.bean-osx.com/Bean.html" target="_blank">Bean</a>. In poco tempo potrebbe diventare <cite>il programma da lanciare se soffri di depressione da Word</cite>. E costa infinitamente meno, cioè zero.

