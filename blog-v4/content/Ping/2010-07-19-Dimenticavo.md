---
title: "Dimenticavo"
date: 2010-07-19
draft: false
tags: ["ping"]
---

Per le persone meno concentrate sull'andamento della Borsa: da quando è stato fatto scoppiare l'Antennagate, il titolo Apple non ha fatto che scendere.

Oggi Apple diramerà i risultati finanziari del primo trimestre 2010. Saranno da buonissimi a ottimi. Il titolo schizzerà in alto.

Un sacco di gente aveva interesse a fare scendere il titolo, per comprare a buon prezzo e rivendere poco dopo con guadagno apprezzabile.

Per chi non ci crede ho pronta una lista chilometrica di link: i rimandi alle segnalazioni di problemi di ricezione di iPhone 3G. Qualcuno è <a href="http://www.macdailynews.com/index.php/weblog/comments/nokias_claim_of_superior_antenna_performance_falls_flat/" target="_blank">qui</a>. Non se ne ricorda nessuno, infatti.