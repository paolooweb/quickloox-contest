---
title: "Piccoli spostamenti del cuore"
date: 2008-12-02
draft: false
tags: ["ping"]
---

Ultimo, credo, aggiornamento sulla mia avventurina da caffè versato sulla tastiera.

Assistente Migrazione di Mac OS X ha portato i dati dal disco vecchio a quello nuovo e lo ha reso tale e quale al precedente. Al termine dell'operazione però è andato in <em>crash</em>.

Ho provato a fare il <em>boot</em> e mi sono ritrovato con il mio <em>account</em>, che però era standard e non di amministrazione. Ed era attivo un account di <code>root</code> con <em>password</em> sconosciuta, presumibilmente quello che usa Assistente Migrazione per consentire il passaggio dei dati.

Con il disco di installazione di Mac OS X ho azzerato le <em>password</em> e ho conferito i poteri di amministrazione al mio <em>account</em>, come avrebbe dovuto essere. Mi è stato detto che tutto ciò non è uno scherzo del destino bens&#236; un <em>bug</em> noto di Leopard e, se è vero, saperlo può forse servire a qualcuno.

Ho disabilitato <code>root</code> con l'aiuto di Utility Directory.

Ultimissima differenza, immagino, ho scoperto che Time Machine era spento. L'ho riacceso. E ho reimpostato il disco di backup, che non era cambiato e ha sempre lo stesso nome. Per il &#8220;nuovo&#8221; Time Machine, evidentemente, è diverso. Infatti apre una nuova cartella. Poco male.

Tutta esperienza. Intanto come misura di sicurezza aggiuntiva ho abolito lo zucchero dal caffè.