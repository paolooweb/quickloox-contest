---
title: "Il tempo è galantuomo"
date: 2010-01-14
draft: false
tags: ["ping"]
---

Hanno <a href="http://digitaldaily.allthingsd.com/20100113/rob-glaser-out-as-realnetworks-ceo/" target="_blank">cacciato Rob Glaser</a> da RealNetworks.

Glaser ha fondato RealNetworks nel 1994 e ne era amministratore delegato.

<cite>È assolutamente chiaro il perché da qui a cinque anni Apple avrà il 3-5 percento del mercato dei lettori digitali di musica.</cite>
&#8212; <a href="http://www.nytimes.com/2003/11/30/magazine/the-guts-of-a-new-machine.html?sec=&amp;spon=&amp;pagewanted=6" target="_blank">Rob Glaser al New York Times</a>, 30 novembre 2003.