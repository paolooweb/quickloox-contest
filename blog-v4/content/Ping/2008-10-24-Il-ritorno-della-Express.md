---
title: "Il ritorno della Express"
date: 2008-10-24
draft: false
tags: ["ping"]
---

Il <em>router</em> di FastWeb ha il wifi incorporato e pertanto avevo impacchettato e pensionato la AirPort Express.

Tuttavia in casa ci sono tre computer e di fatto anche quattro, perché al wifi si abbevera giustamente anche iPhone. E da qui all'anno prossimo è ampiamente probabile l'ingresso di un secondo iPhone o di un iPod touch.

Nel gestire gli Ip in questa quantità, il <em>router</em> FastWeb è inefficiente. Almeno una volta al giorno sovrappone gli Ip di due macchine e di per sé sarebbe poca cosa. Solo che la stampa in casa mia si effettua accedendo via rete alla stampante condivisa attaccata all'iMac di emergenza. Il tutto significa che spesso e volentieri non si riesce a stampare e si perdono quarti d'ora a trovare le macchine con i numeri Ip coincidenti, risistemare la disposizione della rete, rendere la stampante nuovamente visibile a tutti e cos&#236; via.

Quando mi sono stufato, ho recuperato la Express, l'ho collegata via Ethernet al router FastWeb e le ho detto di creare una sottorete. Il <em>router</em> eroga un numero Ip solo ed è contento, i computer della sottorete interna se lo palleggiano senza problemi, la stampante è sempre al suo posto.

Viva AirPort Express.