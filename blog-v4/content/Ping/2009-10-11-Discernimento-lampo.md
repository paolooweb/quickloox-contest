---
title: "Discernimento lampo"
date: 2009-10-11
draft: false
tags: ["ping"]
---

S&#236;, anch'io ho installato <a href="http://rentzsch.github.com/clicktoflash/" target="_blank">ClickToFlash</a>.

Strumento fondamentale che permette di distinguere il Flash inutile da quello utile. Approssimativamente 95 e 5.