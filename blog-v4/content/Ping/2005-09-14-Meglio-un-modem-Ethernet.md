---
title: "Meglio un router<p>"
date: 2005-09-14
draft: false
tags: ["ping"]
---

Per una Adsl più affidabile e sicura<p>

La parola a <em>Jakaa</em>:<p>

<cite>Ho letto tempo fa un Ping in cui parlavi dei modem Adsl Usb per Mac. Dicevi che erano una tragedia. Ho pensato: sarà un caso isolato. Poi ci sono caduto. Prima configurazione: tempo buttato via circa un&rsquo;ora, forse un po&rsquo; di più. E questo prima delle vacanze. Torno a settembre e non va. Seconda configurazione: dalle 21.00 dell&rsquo;11 settembre alle 00.47 del 12 settembre. Se si ribella un'altra volta compro un modem Ethernet e questo lo ficco nel frullatore e lo polverizzo.</cite><p>

Non è partito preso. Ci sarà gente a cui il modem Usb funziona benissimo. E persone il cui modem Ethernet dà problemi. Nei nostri rapporti con le periferiche, l&rsquo;assoluto non esiste. Quello che non funziona a me può essere perfetto per te e viceversa.<p>

Per la limitata esperienza personale che ho, raccomando un modem Ethernet.<p>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>