---
title: "Cibo (per la mente) in scatola"
date: 2009-11-29
draft: false
tags: ["ping"]
---

Guardo con diffidenza ai sistemi di virtualizzazione. Mi sembrano sempre sistemi per sprecare spazio su disco e Ram a disposizione del sistema operativo principale. Tutte fisime mie, beninteso; è roba utilissima. Infatti, nonostante la diffidenza, ho voluto provare a fare girare Chrome Os su VirtualBox.

È stato tutto più facile di quello che pensavo; è bastato scaricare l'immagine disco di Chrome Os <a href="http://gdgt.com/google/chrome-os/download/" target="_blank">messa a disposizione da gdgt.com</a> e indicarla come disco per VirtualBox, impostato su macchina virtuale Linux di tipo Ubuntu.

Un <a href="http://google-chrome-browser.com/installing-google-chrome-os" target="_blank">tutorial visivo</a> mi ha chiarito i pochi dubbi e nel giro di qualche minuto stavo provando Chrome Os, assai provvisorio, su un sistema virtuale con tastiera americana.

Poco importa però; ho abbattuto una barriera psicologica e sto già pensando alla prossima impresa, tipo installare nella scatola, pardon, macchina virtuale FreeBsd o proprio il <a href="http://www.opensource.apple.com/" target="_blank">Darwin</a> <i>open source</i> di Apple.

Non sarà altrettanto facile o veloce, però se ne potrebbe imparare qualcosa.