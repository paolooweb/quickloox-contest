---
title: "Andiamo per il sottile"
date: 2009-09-18
draft: false
tags: ["ping"]
---

Dove altro si potrebbe scoprire che in Snow Leopard le barre di progressione sono molto più precise che non in Leopard?

Ma in <a href="http://finerthingsinmac.com/" target="_blank">Finer Things in Mac</a>. Esistente anche in <a href="http://finerthingsiniphone.com/" target="_blank">edizione per iPhone</a>.

Tutte quelle cose che fanno di Mac un sistema superiore, e di cui quasi nessuno parla, saltano fuori, un po' per volta.