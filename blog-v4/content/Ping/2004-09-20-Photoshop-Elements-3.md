---
title: "Il profitto è l&rsquo;anima del commercio<p>"
date: 2004-09-20
draft: false
tags: ["ping"]
---

Non esistono litigi; esistono conti economici<p>

La vulgata spiega che Adobe ha smesso di fare Premiere per Mac come rappresaglia a seguito dell&rsquo;uscita di Final Cut e fa molti altri esempi del genere.<p>

Ora però vorrei sapere come mai, se Adobe è così arrabbiata per la preinstallazione sui Mac di iPhoto e Graphic Converter, come mai Adobe ha annunciato Photoshop Elements 3 per Mac OS X.<p>

La verità è che Adobe non riusciva a essere competitiva contro Final Cut e i conti non le tornavano. Con Photoshop Elements lo è e i conti tornano.<p>

Le aziende decidono in base ai conti economici e non secondo il piede con cui si sveglia la mattina l&rsquo;amministratore delegato.<p>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>