---
title: "Security Install"
date: 2007-04-21
draft: false
tags: ["ping"]
---

Security Update installato in piena&#8230; sicurezza. Tutto funziona alla grande. Già che c'ero ho anche aggiornato <a href="http://www.barebones.com/products/bbedit" target="_blank">BBEdit</a> e <a href="http://www.opencommunity.co.uk/vienna2.php" target="_blank">Vienna</a>.

Ho anche scoperto che vienna.sourceforge.net (da me digitato distrattamente al posto dell'Url vero) porta a un <a href="http://vienna.sourceforge.net" target="_blank">client Sql</a>. Sai mai che tornasse utile.