---
title: "Finta concorrenza"
date: 2006-06-08
draft: false
tags: ["ping"]
---

Se ne parlava con <strong>Piergiovanni</strong>. Lui notava giorni fa (puntuale sulla notizia; sono io in ritardo) la comparsa di Google Spreadsheet e si chiedeva perch&eacute; Apple non decide di rilasciare un suo foglio di calcolo (chiamiamolo Numbers) per fare concorrenza a Excel.

Il punto &egrave; che Google Spreadsheet non fa concorrenza a Excel, come Gmail non fa concorrenza a Entourage. La gente user&agrave; Excel quando gli serve e Google Spreadsheet quando gli serve. Ma proponi a qualcuno di sostituire Excel con un altro programma, qualunque, e sentirai reazioni sdegnatissime. Manca solo uno che dica <em>i menu devono disegnarsi con le stesse coordinate cartesiane di quelli di Excel</em> e li abbiamo sentiti tutti.

Nel frattempo mi sono <a href="http://www.google.com/googlespreadsheets/try_out.html" target="_blank">prenotato</a> per avere notizie di Google Spreadsheet. A me servirebbe. Ma mica per buttare <a href="http://www.openoffice.org/" target="_blank">Open Office</a> o <a href="http://www.marinersoftware.com/sitepage.php?page=14" target="_blank">Mariner Calc</a>. Non butto via neanche <a href="http://www.apple.com/it/appleworks/" target="_blank">AppleWorks</a> e francamente ieri, proprio partendo dall&rsquo;originale AppleWorks, ho ricostruito il modulo per le ricevute dei diritti d&rsquo;autore, che calcola automaticamente l&rsquo;aliquota di tassazione, in <a href="http://www.apple.com/it/iwork/" target="_blank">Pages</a>.

Mi fermo qui per non esagerare. &Egrave; noto in tutti i peggiori bar di Caracas che per Mac mancano i programmi.