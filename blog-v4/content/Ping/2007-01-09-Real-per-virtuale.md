---
title: "Reale per virtuale"
date: 2007-01-09
draft: false
tags: ["ping"]
---

Virtual Programming sta cercando veri beta tester per un po' di giochi. L'elenco è interessante e copre molti generi, per cui lo lascio alla curiosità degli interessati.

Il link qui rimanda alla <a href="http://www.insidemacgames.com/news/story.php?ID=14546" target="_blank">notizia di InsideMacGames</a> perché il sito di <a href="http://www.vpltd.com/" target="_blank">Virtual Programming</a> richiede una registrazione. Per qualcuno veramente interessato a fare il beta tester, ovviamente, registrarsi sarà il minimo.