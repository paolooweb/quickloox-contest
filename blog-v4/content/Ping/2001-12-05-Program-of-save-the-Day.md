---
title: "Program of (save) the day"
date: 2001-12-05
draft: false
tags: ["ping"]
---

To save the day, per gli anglosassoni, significa avere rimediato la giornata, salvato la situazione, avercela fatta. Ci sono programmi che, nel loro piccolo, “salvano la giornata”.
Un esempio: Mac OS X introduce pesantemente nel sistema questioni di permessi, di amministrazione, di multiutenza che prima non c’erano. Chi sa farlo può gestire tutto via Terminale, a suon di comandi Unix: chown, chgrp e cose così (provare a digitare “ch” nel Terminale e premere il tasto Tab per vedere tutti i comandi che iniziano per “ch”). Ma non è quella che anni fa veniva definita “The Mac Way”. I permessi di accesso a un file sono informazioni sul file; dovrei poterli vedere e cambiare nella finestra Info. Mac OS X invece non lo permette.
Tuttavia c’è Super Get Info, di Bare Bones (gli stessi di BBEdit). Si fa Comando-maiuscolo-i invece di Comando-i però poi c’è tutto, come Mac comanda. Mi ha già salvato qualche giornata.

lux@mac.com

<http://www.barebones.com>
