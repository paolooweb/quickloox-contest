---
title: "Font e tastiere"
date: 2003-01-21
draft: false
tags: ["ping"]
---

Una curiosità perfettamente inane che è proprio... di sistema

Alla presentazione ufficiale dei nuovi PowerBook in Italia ho potuto finalmente mettere le mani su 12” e 17”, accorgendomi di una cosa: il font serigrafato sulle tastiere è cambiato e ora somiglia clamorosamente, se non è proprio uguale, al Lucida Grande utilizzato da Mac OS X.

Per qualcuno è indice di attenzione maniacale ai dettagli, per qualcuno una nota di nessun conto; per me è una concezione davvero allargata di “font di sistema”.

<link>Lucio Bragagnolo</link>lux@mac.com

P.S.: chiedo scusa per l’uso di “inane”, ma quando ci vuole ci vuole.