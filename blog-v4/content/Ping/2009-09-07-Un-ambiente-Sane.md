---
title: "Un ambiente Sane"
date: 2009-09-07
draft: false
tags: ["ping"]
---

<a href="http://www.ellert.se/twain-sane/" target="_blank">Sono già pronti</a> i primi <i>build</i> per Snow Leopard di <a href="http://www.sane-project.org/" target="_blank">Sane</a>, l'insieme di <i>driver open source</i> per scanner altrimenti senza supporto o con supporto insufficiente.

Ho reinstallato tutto e sono già riuscito a eseguire scansioni anche con il <i>boot</i> a 64 bit.

All'inizio non ho installato <code>sane-backends</code>, perché il sistema sosteneva di avere già a bordo una versione più recente. Non funzionava. Ho eseguito ugualmente l'installazione ignorando gli avvisi e la cosa si è risolta.