---
title: "Buoni propositi e cattive compagnie"
date: 2008-08-20
draft: false
tags: ["ping"]
---

Sono sotto a farmi cancellare dalle <em>mailing list</em> indesiderate, di prodotto, di attualità, di gruppi, di tutto.

Pensavo di avere una situazione tutto sommato ottimizzata&#8230; e non è mica vero. Avevo alcune <em>mailing list</em> che comunque venivano filtrate e neanche vedevo, ma continuavo a ricevere.

Poi ci sono le comunicazioni di certe <em>software house</em> che mi avevano interessato ma che non mi interessano più. Poi cose di stretta attualità che hanno smesso di esserlo, poi tentativi generosi che non hanno sortito esito (e cos&#236; arriva una <em>mail</em> al mese di nessun interesse, che è solo una ma non serve a niente) eccetera eccetera.

Di caso particolare in caso particolare ho la sensazione che finirò per ridurre la quota di posta in arrivo di un valore significativo. Sto anche imparando che alcuni, per evitare una cancellazione, complicano l'operazione in modi abbastanza subdoli, come richiedere nome utente e <em>password</em> nella speranza che tu ti sia dimenticato, oppure chiedere conferma della cancellazione con un messaggio facilmente equivocabile da un lettore distratto.

Comunque è un'operazione che raccomando. Alla fine ti senti di avere fatto cosa utile.