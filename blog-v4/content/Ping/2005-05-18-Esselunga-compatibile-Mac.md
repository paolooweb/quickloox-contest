---
title: "Ha capito perfino l&rsquo;ipermercato<p>"
date: 2005-05-18
draft: false
tags: ["ping"]
---

I mentecatti che blaterano del tre per cento dovrebbero prendere esempio da Esselunga<p>

Ricevo e pubblico assai volentieri da Matteo Calvi:<p>

<em>Ti segnalerei questo <a href="http://www.esselungaacasa.it/ecommerce/reg/loginFrameset.do">link</a>:</em><p>

<cite>In buona sostanza la catena di distribuzione da cui ho copiato il link <em>[qui non c&rsquo;è quello originale di Matteo, che riporta a una pagina più generica, ma ugualmente eloquente. N.d.lux]</em> ha cercato la compatibilità anche per Mac, malgrado siamo uno sparuto gruppo di utenti.</cite><p>

<cite>Posso garantire che un anno fa, il sito di e-commerce non funzionava col Mac, invece ultimamente va egregiamente, tanto che mia moglie ha ià acquistato tre volte. Dopo aver letto la pagina segnalata devo confessare che i soldi li ho anche spesi più volentieri.</cite><p>

<cite>Poi è segnalata anche una pagina di MacWorld. :)</cite><p>

Sono cliente Esselunga e sottoscrivo parola per parola l&rsquo;intervento di Matteo.<p>

Intanto, mi sa che il gruppo di utenti Mac non è più così sparuto. Secondo, compito in classe per tutti i mentecatti che blaterano di quote di mercato. Come mai una catena di ipermercati, che vende a milioni di persone, si preoccupa così tanto per un supposto tre per cento?<p>

La verità è che Internet è un mezzo di comunicazione universale. Quindi deve servire a comunicare con tutti. E quando si fa commercio elettronico, è stupido lasciarsi scapapre anche un solo cliente.<p>

A quelli di Esselunga scriverò per congratularmi della loro lungimiranza. E se non fossi l&rsquo;unico?<p>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>