---
title: "In coda di stampa"
date: 2007-12-13
draft: false
tags: ["ping"]
---

In casa ho un iMac Lcd 15&#8221; (Tiger) che condivide per tutta la casa una stampante Hewlett-Packard DeskJet 995C. Il router che mette in rete i Mac è una AirPort Express.

Dal PowerBook 12&#8221; (Leopard) nella rete è partita una stampa. Solo che, per un disguido, il cavo Usb della stampante era staccato dall'iMac.

Riattaccato il cavo della stampante è successa una cosa strana. Nessuno dei portatili (Leopard) riusciva più a stampare. La stampante appariva &#8220;in pausa&#8221; e, dato l'ordine di ripristino, si rimetteva subito dopo nella stessa pausa.

Il <em>troubleshooting</em> normale non ha dato esiti. Tutti i cavi sono stati ricontrollati, staccati, riattaccati. Tutto lo hardware è stato spento e riacceso. È stata provata la stampa diretta da ambedue i portatili, a stampante collegata direttamente sulle rispettive porte Usb, sempre con lo stesso risultato. Le opzioni di condivisione della stampante sull'iMac sono state disattivate e riattivate. Niente.

Allora ho cominciato ad aprire Console sui portatili e a consultare in tempo reale <code>error.log</code> della sezione <code>cups</code> (che riguarda appunto la stampa). Dai messaggi, appariva che in qualche modo erano i portatili stessi a cancellare l'ordine di stampa appena impartito.

Dal log generale di Console ho subito dopo scoperto che, in qualche modo, i portatili non avevano l'autorizzazione a revocare lo stato di pausa.

Cos&#236; ho avuto l'intuizione e mi sono anche reso conto che mi mancava una prova. A rete normalmente dispiegata, ho lanciato una stampa dall'iMac, collegato direttamente alla stampante e &#8220;proprietario&#8221; legittimo della stessa.

Lo stato di pausa è stato revocato e tutto ha ripreso a funzionare regolarmente.

Storia noiosa, certo, ma magari a qualcuno può tornare utile.