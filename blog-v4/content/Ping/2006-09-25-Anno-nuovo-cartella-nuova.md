---
title: "Anno nuovo, cartella nuova"
date: 2006-09-25
draft: false
tags: ["ping"]
---

Era un po' che non passavo sull'iDisk e ho visto cartelle che mi sembrano decisamente nuove: come minimo Desktop, Groups e Library.

Sto chiedendomi che cosa possano significare alla luce del futuro arrivo di Leopard e faccio poca fatica a capire che i Groups servono per .Mac stesso e forse per il prossimo iCal, da cui mi attendo molto. Ma le altre, ancora mi sfuggono. :)