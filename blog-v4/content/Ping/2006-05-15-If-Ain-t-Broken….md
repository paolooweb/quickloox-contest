---
title: "If It Ain&rsquo;t Broken&hellip;"
date: 2006-05-15
draft: false
tags: ["ping"]
---

Se non &egrave; rotto, non lo riparare (<em>Don&rsquo;t Fix It</em>). Se tutta la gente che lancia Utility Disco per verificare un disco che funziona perfettamente facesse invece un backup regolare, risparmierebbe tempo.

Se tutto il tempo speso a &ldquo;ripulire&rdquo; cartelle System, &ldquo;riordinare&rdquo; i font (ma in che ordine?), &ldquo;ripulire&rdquo; le preferenze, andasse in <a href="http://www.oxixares.com/glucas/" target="_blank">elaborazione di numeri primi</a>, la comunit&agrave; dei matematici sarebbe pi&ugrave; contenta.

In alternativa c&rsquo;&egrave; sempre la <a href="http://setiathome.berkeley.edu/" target="_blank">ricerca degli extraterrestri</a>. Obiettivo ben pi&ugrave; a portata che non riorganizzare un disco rigido meglio di quanto sa farlo un sistema operativo.