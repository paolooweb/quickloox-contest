---
title: "AppleScript sempre sul desktop"
date: 2008-11-20
draft: false
tags: ["ping"]
---

Le scuse all'inizio: non trovo più il sito dove risiede lo script originale. Lo reincontrerò, solo non so quando.

E adesso AppleScript. Intuitivamente si pensa a un programma, a uno script, come a <em>un</em> flusso di istruzioni. Mica è detto, potrebbero essere di più. Lo script che segue infatti ne contiene due. Sarebbe più corretto dire che sono due script, però registrabili come unico documento dentro una sola finestra di Script Editor. Documento che, per essere utilizzato davvero, deve essere registrato come applicazione.

La vera novità dello script rispetto alle cose già dette è questa. Il resto è relativamente semplice e commentato a sottolineare i punti di particolare rilievo.

<code>
-- Questo codice viene eseguito se si fa doppio clic sull'icona dello script.
-- È il significato di <code>on run</code>
on run
	display dialog "Trascina su questa icona una immagine per impostarla come sfondo scrivania."
end run
</code>


<code>
-- Questo codice viene eseguito se si trascina una immagine sull'icona dello script
-- È il significato di <code>on open</code>
on open documentiTrascinati
	set fileImmagine to last item of documentiTrascinati
-- Se sono stati trascinati più documenti, si considera solo l'ultimo
	
	tell application "Finder"
		select window of desktop
		try
			set desktop picture to file (fileImmagine as text)
		on error
			display dialog "Non posso usare questo file."
		end try
	end tell
end open
</code>
