---
title: "Updates that (still) don&rsquo;t suck"
date: 2006-06-18
draft: false
tags: ["ping"]
---

&Egrave; sempre un piacere leggere le <a href="http://www.barebones.com/support/bbedit/current_notes.shtml" target="_blank">note</a> che accompagnano un aggiornamento di BBEdit. Senti di avere il controllo su ogni singolo apostrofo del programma. E con BBEdit &egrave; abbastanza vero.