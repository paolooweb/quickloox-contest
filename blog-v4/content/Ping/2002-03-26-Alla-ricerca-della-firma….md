---
title: "Alla ricerca della firma perduta"
date: 2002-03-26
draft: false
tags: ["ping"]
---

Un piccolo aiuto che, se arrivasse, ne varrebbe centinaia

Mi scrive <link>Andrea Omicini</link>aomicini@deis.unibo.it, professore di un dipartimento di ingegneria informatica dell’Università di Bologna, segnalandomi un problema che a mio giudizio dovrebbe fare drizzare qualche antenna anche in Apple.
Accade che l’Università stia attuando un programma di informatizzazione che fa spesso ricorso alla firma digitale su smart card. Non ci sono problemi nel reperire lettori di smart card funzionanti su Mac, ma non si trova software per la firma digitale su smart card che funzioni per Mac. E così i Mac dell’università di Bologna rischiano di restare tagliati fuori.
A me sembra impossibile che non esista niente, ma non ho le competenze specifiche e confesso la mia ignoranza in materia. Se qualcuno però potesse aiutare Omicini aiuterebbe un’università intera, e anche la libertà di scelta informatica. Roba da centodieci e lode.

<link>Lucio Bragagnolo</link>lux@mac.com