---
title: "Nostalgia canaglia<p>"
date: 2006-01-02
draft: false
tags: ["ping"]
---

Quando uno termina le vacanze si aggrappa a tutto<p>

Prima di fare i bagagli e tornare a casa ho voluto dedicare una mezz&rsquo;oretta al vecchio Power Macintosh 8100/80 semiabbandonato che però nessuno ha voglia di eliminare. È partito regolarmente nel suo bel Mac OS 9.1 e ci ho installato sopra un Cd di vecchi giochi di ruolo molto belli e molto retrò.<p>

Quel Mac OS tutto squadrato, oltre a sembrare vecchio, effettivamente lo è. La mezz&rsquo;oretta si è rapidamente quadruplicata e sono partito in netto ritardo sulla tabella di marcia. Ma quelle due ore di roba cosiddetta vecchia mi sono rimaste nel cuore.<p>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>