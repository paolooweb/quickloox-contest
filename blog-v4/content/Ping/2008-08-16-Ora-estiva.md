---
title: "Ora estiva"
date: 2008-08-16
draft: false
tags: ["ping"]
---

Un altro orologio da aggiungere alla raccolta: <a href="http://wetfish.de/BinaryClock/" target="_blank">BinaryClock</a> di WetFish Software.

È binario e abbastanza piccolo per stare graficamente nella barra dei menu.

Voglio vedere, a tradurre l'ora in tempo reale&#8230; :-)