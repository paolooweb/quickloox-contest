---
title: "Idee da pensionare"
date: 2009-10-19
draft: false
tags: ["ping"]
---

L'Inps mi informa che posso avere una casella di posta elettronica certificata.

Fantastico, il progresso è una gran cosa.

È sufficiente richiederla sul sito Inps e andare a farsi riconoscere presso una sede locale.

Una logica elementare evidenzia che, se è necessario uscire di casa, tanto valeva evitare di sprecarsi con il sito.

Uno dirà che magari è una questione di regolamenti. Regolamenti a pera, perché l'Ordine dei giornalisti offre la stessa possibilità e non bisogna andare di persona da nessuna parte (anche se la procedura è solo appena meno stupida di quella dell'Inps).

All'ente delle pensioni si dovrebbe proprio pensionare qualcuno. O almeno le sue idee vecchie e sciocche.