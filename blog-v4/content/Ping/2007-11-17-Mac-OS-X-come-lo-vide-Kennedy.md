---
title: "Mac OS X come lo vide Kennedy"
date: 2007-11-17
draft: false
tags: ["ping"]
---

Una delle critiche che sento più frequentemente rispetto alle nuove infornate di Leopard, iLife, iWork eccetera è <cite>il programma non fa questa cosa esattamente come la vorrei fare io e mi costringe a lavorare in questo modo mentre io vorrei lavorare in quest'altro</cite>.

Apple ha deciso di semplificare il più possibile le applicazioni di base, per ampliare il più possibile la base di utenza (visto <a href="http://www.filemaker.com/products/bento/features.html" target="_blank">Bento</a>?). Raggiungere questo obiettivo e contemporaneamente offrire ai <em>power user</em> la flessibilità che vogliono non è facile, né veloce. Lo dimostra iMovie '08. Molto più facile di prima, certamente lo stanno usando molte più persone di prima. Tuttavia, prima che raggiunga la stessa versatilità di iMovie '06, passeranno molti byte sotto i ponti.

D'altra parte, esistono programmi per Mac OS X a tutti i livelli di complessità. A dirla tutta, non sono mai esistiti cos&#236; tanti programmi come oggi. La scelta è straordinariamente ampia.

A livello di tecnologie di integrazione, c'è tutta la parte Unix, ci sono i linguaggi di scripting di serie, c'è AppleScript 2.0, c'è Automator che adesso registra pure, c'è JavaScript, c'è Dashcode&#8230; non è roba immediata e spesso nemmeno semplice. Tuttavia, lo sforzo necessario è infinitamente minore rispetto a prima. E basta poco per ritrovarsi con possibilità inaspettate.

Vorrei parafrasare Kennedy. Non c'è dubbio, e ci mancherebbe, che non ci chiedessimo che cosa Mac OS X può fare per noi. E dobbiamo chiedere e pretendere sempre di più. Nel contempo, se ci chiediamo anche che cosa possiamo fare noi con Mac OS X, avremo molto di più, con molto più valore e anche più in fretta.