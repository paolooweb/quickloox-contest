---
title: "L’importanza del servizio"
date: 2003-08-04
draft: false
tags: ["ping"]
---

Rischia di passare alla storia come il nuovo Pubblica & Sottoscrivi, ma c’è molto di più

Una delle novità in System 7, all’inizio degli anni Novanta, fu Pubblica e Sottoscrivi. Sarà stata l’orrenda traduzione di Publish & Subscribe, che rendeva difficile capire di che cosa si trattasse; sarà stata una funzione che facilita il dialogo tra applicazioni, data in pasto a tanta gente che di applicazioni ne usa solo una (e naturalmente quando può si lancia nel discorso “ci sono pochi programmi per Mac”); saranno stati gli sviluppatori che non la inserivano in alcuni programmi importanti; chissà che cosa sarà stato. Di fatto, era una funzione assai intelligente e utile, che Apple tolse da Mac OS dopo avere constatato che la usavamo in tre e almeno uno era mio parente.

I servizi ora corrono lo stesso rischio. Mica per Apple, ma per noi. Infatti svolgono un sacco di compiti utili e intelligenti ma non sono pubblicizzati, sono meno intuitivi di altre funzioni e dipendono in parte dalla buona volontà del software.

Adesso, per esempio, userò un servizio di Mailsmith, accessibile dal menu applicazione di Tex-Edit Plus, per trasformare questo pezzo in una mail diretta alla redazione. Basta un comando selezionato da mouse, più efficiente di un copia e incolla, più facile di un drag and drop.

Abituati a pensare a quel menu che sta sotto il nome dell’applicazione. Spesso contiene modi per facilitarsi ulteriormente la vita in Mac OS X.

<link>Lucio Bragagnolo</link>lux@mac.com