---
title: "Non ci metterei la faccia"
date: 2010-10-24
draft: false
tags: ["ping"]
---

Immagino valga solo per il mio Mac. Però <a href="http://www.apple.com/mac/facetime/" target="_blank">la pagina Apple di FaceTime</a>, quando la visito, non mostra il pulsante di scaricamento della <i>beta</i>.

Suppongo sia una faccenda temporanea.

<a href="http://www.macupdate.com/info.php/id/35560/apple-facetime" target="_blank">MacUpdate</a> però mi funziona a meraviglia, sempre.