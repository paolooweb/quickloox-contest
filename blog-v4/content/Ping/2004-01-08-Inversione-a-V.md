---
title: "Inversione a V"
date: 2004-01-08
draft: false
tags: ["ping"]
---

I localizzatori in italiano di Mac OS X non sono neanche capaci di sbagliare come si deve

Per ragioni di lavoro sto usando il Terminale in italiano e non vedo l’ora di ritornare all’inglese.

I localizzatori hanno preso il comando di menu Paste Escaped Text e lo hanno tradotto Copia testo eluso. Eluso, come quando si elude la vigilanza, non invece introdotto da un carattere di escape, e quindi risolto, codificato, con escape o quello che si vuole.

Ma il peggio è che scrivono Copia e lasciano una scorciatoia Comando-Maiuscolo-V. È dal 1984 che la scorciatoia con la V non ha mai copiato niente, in nessun programma.

Gente che non sa l’inglese, non sa l’italiano, non conosce l’informatica e non ha mai usato un Macintosh. Perfetta per questo lavoro.

<link>Lucio Bragagnolo</link>lux@mac.com