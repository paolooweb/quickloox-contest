---
title: "Ci abbiamo anche i libri"
date: 2006-10-20
draft: false
tags: ["ping"]
---

Come promesso. A metà tra la reliquia e il remainder, stasera estrarremo a sorte tra i presenti:

<ul type="disc">
	<li>A Book on C</li>
	<li>iPod &#38; iTunes Hacks</li>
	<li>Mac OS X Help Desk Essentials</li>
	<li>Open Sources 2.0 - The Continuing Evolution</li>
	<li>The Complete Book of HyperTalk 2</li>
</ul>

Se poi nessuno se li fila, andranno ad appesantire ulteriormente gli scaffali della biblioteca del Poc. :)