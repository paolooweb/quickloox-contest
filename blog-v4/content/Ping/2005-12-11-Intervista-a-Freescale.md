---
title: "Male informati o bugiardi<p>"
date: 2005-12-11
draft: false
tags: ["ping"]
---

Quelli che si sono inventati un PowerBook G5 totalmente inesistente<p>

Pochi giorni fa c|net ha pubblicato una interessante <a href="http://news.com.com/Is+the+PowerPC+due+for+a+second+wind/2008-1006_3-5983157.html?tag=st.prev">intervista</a> a Michel Mayer, amministratore delegato di Freescale. Ne riporto un punto saliente:<p>

<cite>News.com: Non c&rsquo;eri durante le discussioni in cui Ibm ha convinto Apple ad adottare il G5?</cite><p>

<cite>Mayer: Il mio lavoro precedente era dirigere l&rsquo;attività Ibm sui semiconduttori. Così ho vissuto la vicenda Apple su ambedue le sponde, perché ho venduto il G5 a Steve (Jobs) la prima volta che voleva passare a Intel.</cite><p>

<cite>News.com: Cinque anni fa?</cite><p>

<cite>Mayer: Sì, all&rsquo;incirca. Così gli vendetti il G5. Prima dissi a Ibm che lo dovevamo fare, poi convinsi Apple che il G5 era buono e rappresentava la prosecuzione dell&rsquo;evoluzione dei processori PowerPc per i desktop. Ha funzionato piuttosto bene. Poi Ibm decise di non portare il G5 sui portatili e di concentrare la sua attività sulle console da videogiochi.</cite><p>

Sono evidenti due cose. Primo: stando a Mayer, Steve Jobs voleva passare a Intel cinque anni fa. Allora non ha raccontato una bugia, dicendo che da un lustro Mac OS X viene sviluppato in parallelo per Intel. Secondo: sempre dando Mayer per persona informata, il PowerBook G5 è stato al massimo una leggenda urbana, come il ragno nel tronchetto della felicità.<p>

Il PowerBook G5 è stato annunciato decine di volte da gente che, stanti così le cose, risulta male informata o bugiarda. C’è da ripensare a tutte le volte che se ne è letto. E fare una scelta.<p>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>