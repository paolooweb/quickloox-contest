---
title: "Altro che museo<p>"
date: 2005-05-16
draft: false
tags: ["ping"]
---

C&rsquo;è più vita in un Apple ][ acceso che in mille G5 spenti<p>

Come promesso, sabato scorso sono stato a Quiliano a presenziare all&rsquo;inaugurazione dell&rsquo;All About Apple Museum, il primo in Europa. C&rsquo;erano anche il sindaco, centinaia di persone, giornalisti ed esperti del settore, ma questo poco conta<p>

A vedere l&rsquo;allestimento ho provato ammirazione per la convinzione, la capacità la genialità con cui Alessio Ferraro e soci del club hanno superato uno dopo l&rsquo;altro tutti gli ostacoli (che non sono stati né pochi né piccoli, dagli spazi ai soldi, al tempo). Poi invidia, perché i miei vecchi Mac ho dovuto darli via, per quanto ad amici e parenti. Infine sollievo per i membri del club, dal momento che le loro mogli non chiederanno più di liberare la cantina!<p>

Battute sceme a parte, lo stupore di vedere le macchine accese e sperimentabili è senza paragoni. Non è un museo, perché nei musei si mettono le mummie, i cocci, i reperti, la storia. A Quiliano è tutto presente, anche se ha trent&rsquo;anni di vita. Vedere dodicenni intraprendenti scoprire Lisa, o saggiare un NeXT funzionante, sorprende e scalda il cuore.<p>

Senza contare le scoperte che chiunque può fare. Gli autori si scherniscono dicendo che manca un sacco di roba e sarà anche vero, ma si vedono pezzi mai arrivati in Italia, il set top box sperimentale costruito da Apple, la console Pippin, eMate e tantissime altre cose.<p>

Stabilito che nessuno mai farebbe un museo di stupidi cassoni grigi senza bellezza e senza creatività dentro, per una persona minimamente interessata alla storia di Mac il museo, chiamiamolo così per comodità, è assolutamente da visitare, almeno una volta nella vita. Museo. È una macchina del tempo, geniale, piena di misteri da scoprire, imperdibile.<p>

Tutti a Quiliano all&rsquo;<a href="http://www.allaboutapple.com/present/sede.htm">All About Apple Museum</a>. E tanti, tanti complimenti.<p>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>