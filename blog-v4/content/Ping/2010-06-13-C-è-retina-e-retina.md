---
title: "C'è retina e retina"
date: 2010-06-13
draft: false
tags: ["ping"]
---

Il marketing di Apple fa da sempre notizia e ancora una volta c'è riuscito, con la storia dello schermo Retina. iPhone 4 ha una densità di pixel tale che, tenuto ai quei circa venticinque-trenta centimetri dall'occhio che sono la distanza tipica di consultazione, l'occhio di una persona media non coglie più la separazione dei singoli pixel.

Come ogni azienda, Apple fa marketing, ovvero comunica nel modo migliore per vendere il prodotto. Ecco come la stessa cosa l'ha raccontata Steve Jobs sul palco della Wwdc:

<cite>C'è un numero magico di circa 300 pixel per pollice tale che, quando si tiene qualcosa a dieci-dodici pollici dagli occhi, corrisponde al limite di differenziazione dei pixel della retina umana.</cite>

Questa affermazione è formalmente inesatta. Una persona dalla vista perfetta è ancora in grado di distinguere i pixel. Tuttavia è sostanzialmente esatta perché la persona media non ha una vista perfetta e la qualità media della vista delle persone dà il risultato di cui ha parlato Jobs. Il quale ha fatto il suo mestiere e ha venduto il prodotto, con una affermazione che risponde al vero per la gran parte di quanti sceglieranno iPhone 4. E, anche se non rispondesse pignolescamente al vero, ci andrebbe gran vicino. Nessun concorrente di iPhone oggi può offrire uno schermo di definizione equivalente.

Un esperto, Raymond Soneira di DisplayMate Industries, ha evidenziato l'inesattezza formale delle parole di Jobs, commentando <cite>lo schermo è ragionevolmente vicino a essere perfetto, ma Steve è andato un po' troppo oltre.</cite> Che è quanto detto finora.

Nel riprendere le parole di Soneira, Wired <a href="http://www.wired.com/gadgetlab/2010/06/iphone-4-retina/" target="_blank">ha parlato di falso marketing</a>, come se Jobs avesse voluto truffare. E qui non ci siamo più.

Phil Plait, creatore del blog Bad Astronomy, è uno che &#8211; parole sue &#8211; conosce qualcosina a proposito di risoluzione, avendo lavorato per dieci anni alla calibrazione di un obiettivo del telescopio spaziale Hubble.

Ha <a href="http://blogs.discovermagazine.com/badastronomy/2010/06/10/resolving-the-iphone-resolution/" target="_blank">spiegato tutta la questione</a> e chiarito la faccenda della risoluzione. La sua conclusione è questa:

<cite>Sia Jobs che Soneira sono nel giusto. Alla peggio si può dire che Jobs abbia esagerato; la sua affermazione non è vera per chi ha una vista perfetta. Ma per un sacco di gente, direi la maggior parte della gente, non si coglierà mai la differenza. [&#8230;] Il titolo di Wired è scorretto; Jobs non ha fatto pubblicità ingannevole delle capacità di iPhone. Si noti che a me Wired piace e che questo è probabilmente solo un caso di eccesso di zelo redazionale. Ma un sacco di gente legge i titoli e questo condiziona la loro visione; qualcuno che leggesse l'articolo potrebbe essere maggiormente indotto a pensare che Jobs, ancora una volta, abbia gonfiato un prodotto per galvanizzare la gente. Non lo ha fatto</cite>.

Ecco. A suon di denunciare gli eccessi del marketing, si può perdere di vista l'esistenza degli eccessi giornalistici e dei titoli, questi s&#236;, falsi. C'è gente che legge solo i titoli, per l'appunto, e questo equivale ad approfittare della loro fiducia (e superficialità, ma è un'altra questione). Chi getta la rete per adescare opinioni facili è molto più falso di chi vende un prodotto. Che poi, al massimo, è una retina. Per piccoli cervelli.