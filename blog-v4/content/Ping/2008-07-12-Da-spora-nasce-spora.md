---
title: "Da spora nasce spora"
date: 2008-07-12
draft: false
tags: ["ping"]
---

<a href="http://www.macworld.it/blogs/ping/?p=1855">Si parlava di Spore</a>, che sembra sempre più il Gioco della prossima stagione.

Mi scrive <strong>Nerino</strong> e confesso che non ci ho capito niente. Però forse a qualcuno più addentro di me serve lo stesso:

<cite>Leggevo il</cite> ping <cite>e per</cite> continuare a scoprire cose nuove <cite>faccio notare un particolare&#8230;</cite>

<cite>Per usare una copia del programma su Pc dopo averlo installato si usa un .exe che genera il seriale, il programma a questo punto</cite> chiama casa <cite>per vidimare il seriale e se la connessione non c'è o è bloccata non si avvia.</cite>

<cite>Allora gli smanettoni del ViTALiTY hanno fatto un</cite> crack <cite>del programma che accetta il seriale senza andare a</cite> chiamare casa<cite>, basta sostituire</cite> <code>SporeCreatureCreator.exe</code>.

<cite>Tutto questo su Pc&#8230; su Mac ecco la sorpresa&#8230; è la stessa cosa: si usa l'</cite><code>exe</code> <cite>per generare il seriale e si va a sostituire l'</cite><code>exe</code> <cite>originale di</cite> <code>SporeCreatureCreator.exe</code> <cite>con quello ViTALiTY dentro.</cite>

<cite>Il file si trova in</cite> <code>SPORE Creature Creator.app/Contents/Resources/transgaming/c_drive/Program Files/EA Games/Spore/SporeBin/SporeCreatureCreator.exe</code>.