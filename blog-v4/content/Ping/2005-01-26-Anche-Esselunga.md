---
title: "Anche quella spesa<p>"
date: 2005-01-26
draft: false
tags: ["ping"]
---

Anche gli ipermercati si accorgono che Internet è fatta per tutti<p>

Non si è dato il giusto risalto alla notizia che ora il sito di spesa a domicilio <a href="http://www.esselungaacasa.it">Esselunga</a> è accessibile e utilizzabile da FireFox&hellip; e da Safari.<p>

Mesi fa lo si poteva frequentare solo con Explorer, solo da Windows.<p>

Si sono resi conto anche loro che Windows (e soprattutto Explorer) non sono proprio quel 99 percento delle favole raccontate su tante testate. Soprattutto, che anche se fosse il 99 percento, Internet è fatta per il 100 percento della cosiddetta utenza. Guarda caso, il target Esselunga.<p>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>