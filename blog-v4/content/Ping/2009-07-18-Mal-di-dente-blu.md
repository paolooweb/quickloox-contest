---
title: "Mal di dente (blu)"
date: 2009-07-18
draft: false
tags: ["ping"]
---

Bluetooth non si poteva più disattivare. Fastidioso, se non altro per il maggiore consumo di batteria.

Non ho proceduto a metafisici reset del sistema, né a reinstallazioni dello stesso. Prima si prova con le cose semplici.

Cos&#236; ho disaccoppiato mouse e tastiera wireless e poi ho spento (spento, non riavviato). L'<i>uptime</i> non ne ha risentito; in questi giorni spostarsi tra Leopard e Snow Leopard è stato un continuo riavviare.

Ho riacceso e Bluetooth ha mostrato il comportamento regolare. Stranamente nella configurazione si vedevano ancora mouse e tastiera. Ancora più stranamente la tastiera funzionava, ma il mouse no.

Ho reimpostato il mouse e ora tutto funziona.

nel caso potesse mai servire a qualcuno.