---
title: "Chi ha più bisogno di vacanze io o lui?"
date: 2006-06-27
draft: false
tags: ["ping"]
---

Nel giro di due giorni, mi sono toccati ben due riavvii indesiderati. Uno per la questione di Bluetooth che non connette più il cellulare (e non mi è riuscita la magia di gennaio), l'altro per un blocco del Finder particolarmente pesante (e rilanciare non è servito).

Aggiorno la statistica: da inizio anno, dice <a href="http://www.iuni.com/en/products/software/mac/iutime/index.html" target="_blank">iuTime</a>, sono passati quasi 178 giorni e quattro riavvii non voluti. Praticamente uno ogni mese e mezzo.

Questa cosa del Bluetooth merita un <a href="http://www.apple.com/feedback/" target="_blank">feedback</a>. È già la seconda volta che succede.