---
title: "Chi ben comincia"
date: 2009-09-05
draft: false
tags: ["ping"]
---

L'inaugurazione dell'<a href="http://www.apple.com/it/retail/carosello/" target="_blank">Apple Store Carosello di Carugate</a> è stata davvero un evento. Coda infinita, pubblico valutabile in migliaia di persone, gli altri negozianti che non sapevano e chiedevano informazioni sul perché girasse molta più gente del solito nel centro commerciale e tutti con il sacchetto-omaggio marchiato con la mela.

Frase da ricordare: <cite>Quando hai comprato da Saturn, hai finito. Quando hai comprato da Apple Store, inizi</cite>.

(c'è un Saturn vicino all'Apple Store).

Cambierei un paio di proverbi, per dire: chi ben comincia gode.