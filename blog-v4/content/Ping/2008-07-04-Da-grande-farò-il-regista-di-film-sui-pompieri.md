---
title: "Da grande farò il regista di film sui pompieri"
date: 2008-07-04
draft: false
tags: ["ping"]
---

La mail che mi ha gentilmente inviato <strong>Italo</strong>:

<cite>Quest'anno con i miei alunni di II elementare ho realizzato un cortometraggio di sei minuti inerente i vigili del fuoco volontari.</cite>

<cite>Il</cite> corto<cite>, dal titolo</cite> Da grande farò il pompiere<cite>, è stato realizzato con i Macintosh della nostra classe e Final Cut. Il</cite> corto <cite>ha riscosso molto successo anche presso gli enti pubblici locali, che ci hanno promesso le risorse necessarie per la realizzazione di un laboratorio</cite> Mac only <cite>per il nostro istituto, visti i risultati.</cite>

<cite>Il video, oltre ad essere un omaggio ai volontari che rischiano la vita per difendere il nostro territorio e aiutarci nelle calamità, vuole essere un esempio di didattica innovativa. Il <a href="http://it.youtube.com/watch?v=3DPTIedKYwY" target="_blank">video sta su YouTube</a>.</cite>

Italo termina con un accenno al fatto che in tanti pensano che nella scuola pubblica non si faccia niente, e invece. Si può pensarla come si vuole, ma certamente ci dovremmo anche chiedere che cosa facciamo noi per la scuola pubblica, nel momento in cui bastano un Mac e il software giusto per cambiare le cose.

E poi girare i titoli di coda è pure divertente. :-)

<object width="425" height="344"><param name="movie" value="http://www.youtube.com/v/3DPTIedKYwY&hl=it&fs=1"></param><param name="allowFullScreen" value="true"></param><embed src="http://www.youtube.com/v/3DPTIedKYwY&hl=it&fs=1" type="application/x-shockwave-flash" allowfullscreen="true" width="425" height="344"></embed></object>