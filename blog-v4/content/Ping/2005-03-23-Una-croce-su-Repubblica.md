---
title: "Libertà di stampa (purtroppo)<p>"
date: 2005-03-23
draft: false
tags: ["ping"]
---

Nuova aggiunta alla lista nera di chi non capisce niente di Mac<p>

Diceva un comico anni e anni fa, <em>brutti giorni quelli in cui la Repubblica si vende tutti i giorni per 1.500 lire</em>.<p>

Oggi si paga in euro ma la battuta resta validissima. Non è questione di politica, neh? Ma di computer. Apple sta facendo notizia e il sito di Repubblica ne tiene debito conto. Ma la copertura è poco informata, inesatta, ai limiti dell&rsquo;ìngenuità dove non c&rsquo;è ignoranza pura.<p>

Se Repubblica ti piace per la sua linea politica, per carità. Se la ami perché sul sito scrive Stefano Bartezzaghi, piace anche a me. Se il sito di Repubblica ti sembra un signor sito, hai ragione.<p>

Solo, quando parlano di Mac, fargli una tara grossa così. Come minimo non hanno capito tutto. A volte non hanno capito proprio niente.<p>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>