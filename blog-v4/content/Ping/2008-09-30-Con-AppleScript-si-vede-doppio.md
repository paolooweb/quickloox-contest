---
title: "Con AppleScript si vede doppio"
date: 2008-09-30
draft: false
tags: ["ping"]
---

MacOSXHints ha presentato <a href="http://www.macosxhints.com/article.php?story=20080917034034587" target="_blank">un interessante AppleScript</a> che duplica i file selezionati nel Finder e aggiunge al nome dei duplicati data e ora, così che sia più semplice tenere traccia dei cambiamenti.

Questo AppleScript è più complesso del solito e anche più lungo. Commentarlo darebbe origine a un <em>post</em> interminabile, difficile da seguire e per stavolta non lo faccio. Invece, copiarlo in Script Editor, registrarlo e provare a eseguirlo. E poi studiarne il funzionamento istruzione per istruzione. È un bell’esempio di intervento seriale sui nomi file.

<code>property extensionhidden : true

tell application "Finder"
    set theselection to selection
    repeat with i from 1 to (count theselection)
        set folderCheck to item i of theselection
        if folder (folderCheck as text) exists then
        else
            set extensionhidden to extension hidden of (item i of theselection)
            set extension hidden of (item i of theselection) to true
        end if
        set theItemName to displayed name of (item i of theselection)
        if folder (folderCheck as text) exists then
        else
            set theItemExtension to name extension of (item i of theselection)
        end if
        set upOneLevelFolder to folder of (item i of theselection) as alias
        
        set {year:y, month:m, day:d, time string:t} to (current date)
        set date_format to (y * 10000 + m * 100 + d) as string
        set time_format to (t) as string
        set t to (do shell script "echo " & "'" & t & "'" & " | sed  's/://g' ")
        set thedate to date_format & " " & t as text
        if folder (folderCheck as text) exists then
            set theNewName to theItemName & " " & thedate as text
        else
            set theNewName to theItemName & " " & thedate & "." & theItemExtension as text
        end if
        log theNewName
        set theDuplicate to duplicate (item i of theselection) to upOneLevelFolder replacing no
        set theDuplicateAlias to theDuplicate as alias
        set name of theDuplicateAlias to theNewName
        if folder (folderCheck as text) exists then
        else
            if extensionhidden then
                set extension hidden of theDuplicateAlias to true
                set extension hidden of (item i of theselection) to true
            else
                set extension hidden of theDuplicateAlias to false
                set extension hidden of (item i of theselection) to false
            end if
        end if
    end repeat
end tell</code>