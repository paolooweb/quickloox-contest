---
title: "Capirci un'acca"
date: 2008-05-16
draft: false
tags: ["ping"]
---

Scoprire la differenza tra gibibyte (il vero numero di byte nel sistema, calcolato secondo le potenze di due) e gigabyte (il numero arrotondato che si usa nel linguaggio comune, calcolato con i multipli di mille) è un attimo, con il Terminale: equivale alla differenza tra 

<code>df -h</code>

oppure

<code>df -H</code>

Ce l'avevo sotto gli occhi e non me ne sono mai accorto prima.