---
title: "Firefox sartoriale"
date: 2008-06-25
draft: false
tags: ["ping"]
---

Una cosa che mi sono dimenticato di fornire tra gli <a href="http://www.macworld.it/blogs/ping/?p=1843">immancabili di Firefox</a> 3 è la possibilità di disporre di <a href="http://www.beatnikpad.com/archives/2008/06/18/firefox3" target="_blank">build ottimizzati per piattaforma e processore</a>, per esempio una versione di Firefox compilata appositamente per un Mac con G5 (la versione standard è ovviamente generica, per girare dovunque, e non ottimizzata).

Qui l'aggettivo <em>sartoriale</em> inizia a mostrare la corda, perché una versione su misura potrebbe dare problemi inaspettati. Il più probabile è che qualche estensione strana non funzioni o funzioni male.

D'altra parte i vantaggi in termini di prestazioni e ingombro potrebbero valere il rischio. <em>Unicuique suum</em>.