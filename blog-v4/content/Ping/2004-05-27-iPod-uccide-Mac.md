---
title: "Lo strabismo antiMacintosh"
date: 2004-05-27
draft: false
tags: ["ping"]
---

No, Apple non si taglierà la gola da sola

Apple ha rimescolato l’organizzazione interna e ha creato una divisione iPod destinata a occuparsi di un prodotto evidentemente di grande successo, ma operante in un mercato assai giovane e ricettivo.

Questo ha condotto numerosi analisti a guardare ad Apple con l’occhio strabico e a identificare un dualismo con Macintosh. C’è chi ha anche supposto che Apple potrebbe abbandonare Macintosh e mettersi a vendere aggeggi di varia natura che partono tutti dal concetto di iPod.

A molti farebbe piacere che Apple mollasse Macintosh, perché non ne capiscono il posizionamento sul mercato. Altri sono semplicemente inabili a usare la calcolatrice.

Due soli dati: primo, Macintosh - che opera su un mercato assai saturo e problematico - ha aumentato le vendite rispetto all’anno scorso; secondo, il fatturato 2003 di Apple è dovuto per il 14 percento alle vendite di iPod e per il 61 percento a quelle di Macintosh.

Se Apple valorizza internamente il 14 percento del suo fatturato, solo uno sprovveduto può pensare che lascerà andare al suo destino il 61.

<link>Lucio Bragagnolo</link>lux@mac.com