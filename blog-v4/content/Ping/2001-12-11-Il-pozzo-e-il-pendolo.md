---
title: "Il pozzo e il pendolo"
date: 2001-12-11
draft: false
tags: ["ping"]
---

Mac OS X, Edgar Allan Poe e l’utente tic-toc

Che cosa c’entrano Mac OS X e il grande scrittore inglese?
Mac OS X è un pendolo: da una parte c’è, come dice Apple, the Simplicity of Mac; dall’altra, the Power of Unix. L’utente non esperto sta da una parte e usa i suoi programmi; l’esperto sta dall’altra e fa come un meccanico esperto quando regola alla perfezione l’acceleratore dell’auto toccando un filo magico che solo lui conosce. Poi ci sono gli utenti come me, un po’ sfruttatori di software, un po’ curiosi di vedere sotto il cofano. Tic, toc, tic, toc.
Unix è, letteralmente, un pozzo, senza fondo. Ma Apple sta pensando anche a quello: oggi ho visto (e imitato) uno studentello francese costruire in dieci minuti un’applicazione Mac che riproduceva le funzioni principali di iMovie, senza scrivere una sola riga di codice e sfruttando i Developer Tools di Mac OS X.
Il che evidenzia una differenza fondamentale rispetto a Poe: Mac OS X è a lieto fine.

lux@mac.com