---
title: "Un'interfaccia selezionata"
date: 2007-07-30
draft: false
tags: ["ping"]
---

Mi serviva mettere una tabella con un fogliettino di calcolo dentro Pages.

Niente di che. Solo, l'interfaccia grafica che mostra le celle selezionate durante l'inserimento delle formule è un gioiello. Spettacolosa. Avessi mai visto un foglio di calcolo pensare a qualcosa del genere.

Sarà che il foglio di calcolo di Pages non lo vede nessuno. :)