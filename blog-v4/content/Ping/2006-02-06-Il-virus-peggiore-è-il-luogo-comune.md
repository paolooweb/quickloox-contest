---
title: "Il virus peggiore &egrave; il luogo comune"
date: 2006-02-06
draft: false
tags: ["ping"]
---

La prossima volta che sento qualche buontempone ripetere, con la stessa intelligenza di una fotocopiatrice, che Mac OS X (o Linux, se &egrave; per quello) hanno meno virus perch&eacute; sono usati da meno gente, metto mano alla <a href="http://www.thottbot.com/?i=3148" target="_blank">Black Menace</a>.

Windows &egrave; pi&ugrave; usato e ha, diciamo, cinquantamila programmi. Mac OS X, meno usato, ne ha ventimila. Il 40 percento.

Per logica, Mac OS X dovrebbe avere il 40 percento dei virus di Windows. Non ne ha neanche il 40 perdiecimila.

Allora sar&agrave; il numero di utenti. Ok. Windows lo usano in novantacinque, Mac OS X in cinque. Per logica, dovrebbe esserci un virus Mac ogni venti virus Windows. Neppure l&rsquo;ombra. Facciamo un centesimo di un ventesimo? Un virus Mac ogni duemila virus Windows. Non c&rsquo;&egrave;.

C&rsquo;&egrave; un mare di gente che vive scrivendo programmi per Mac OS X. Per esempio <a href="http://www.barebones.com" target="_blank">Bare Bones</a>, che fa BBEdit, Mailsmith, Super Get Info, Yojimbo. E li fa solo per Mac.

Pu&ograve; quindi benissimo esserci un programmatore, uno solo, che scrive un virus per Mac OS X. Se non c&rsquo;&egrave;, vuol dire che c&rsquo;&egrave; qualche altra ragione oltre al numero, giusto?

Prova a leggerti questo <a href="http://www.nytimes.com/2003/09/18/technology/circuits/18POGUE-EMAIL.html?ex=1139374800&amp;en=be9fa98f8c649a79&amp;ei=5070" target="_blank">articolo di David Pogue</a> e poi sappimi dire.