---
title: "Un gioco non tanto da ragazzi"
date: 2010-04-06
draft: false
tags: ["ping"]
---

<b>Fearandil</b> mi segnala che un paio di bravi programmatori hanno lavorato su <a href="http://www.idsoftware.com/games/quake/quake2/" target="_blank">Quake 2</a> e sono riusciti a crearne una versione che funziona dentro il <i>browser</i>.

Chi voglia provarci deve possedere abilità tecniche non comuni e partire dal <a href="http://code.google.com/p/quake2-gwt-port/" target="_blank">codice sorgente</a>.

Dal <a href="http://timepedia.blogspot.com/2010/04/gwtquake-taking-web-to-next-level.html" target="_blank">blog</a> di uno dei programmatori si evince che hanno ottenuto risultati variabili in velocità dai 20-25 fotogrammi per secondo su MacBook ai 45 fotogrammi per secondo su MacBook Pro fino a 60 fotogrammi per secondo su un portatile con Linux.

Se è possibile fare funzionare Quake 2 con JavaScript a livelli più che accettabili, significa che le scuse per ricorrere a Flash o a Silverlight sono sempre meno.