---
title: "Come prima, meglio di prima"
date: 2010-12-15
draft: false
tags: ["ping"]
---

A margine della oramai arcinota disponibilità di <a href="http://itunes.apple.com/it/app/myst/id311941991?mt=8" target="_blank">Myst per iOS</a> e della recente notizia dell'arrivo di <a href="http://itunes.apple.com/it/app/riven-the-sequel-to-myst/id400293367?mt=8" target="_blank">Riven</a>, il fortunato e ancora più articolato seguito, non si nota abbastanza che le versioni odierne, viste su un iPhone o un iPod touch, si giocano in condizioni migliori di quelle che erano le originali.

Addirittura l'audio di Myst è migliore dell'originale, grazie al superamento della capacità fissa dell'antico Cd-Rom. Chi mai avrebbe pensato di scaricare un gigabyte di dati negli anni novanta?

E le risorse che consentivano di poggiare sulla scrivania un apparecchio capace di fare giocare Myst, oggi, sono condensate in spessori e dimensioni da tasca.

Con aggeggi tipo <a href="http://itunes.apple.com/it/app/pocket-legends-3d-mmo/id355767097?mt=8" target="_blank">Pocket Legends</a> si può anche giocare <i>online</i> in decine, centinaia, in attesa alla fermata del tram, godendo di grafica tridimensionale, certo non quella di Myst e Riven, ma di buona qualità.

Ogni tanto bisognerebbe spostare l'obiettivo dalle questioni hardware all'insieme. Abbiamo compiuto passi avanti straordinari e si parla di giochi apposta per non fare discorsi più seri, però che voglia di vedere lo svolgersi futuro di questi anni Dieci.