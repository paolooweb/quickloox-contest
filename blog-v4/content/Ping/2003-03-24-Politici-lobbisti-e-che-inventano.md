---
title: "Politici lobbisti e politici che inventano"
date: 2003-03-24
draft: false
tags: ["ping"]
---

Che cosa significa l’ingresso di Al Gore nel consiglio di amministrazione Apple?

Ricorderdai sicuramente che Al Gore si è presentato agli elettori dicendo, più o meno, di avere inventato Internet. Non ha perso le elezioni per quello, ma ha comunque fornito un suo ritratto attendibile di uomo appassionato di tecnologie che, in compenso, ogni tanto le spara grosse.

Scherzi a parte, un Al Gore nel Board of Directors di Apple è un’ottima notizia. Il marito di Tipper, infatti, è uno che conta, con innumerevoli appoggi e influenze che alla percezione sul mercato e ai movimenti strategici di Apple può fare solo assai bene.

Ecco, non stupirti se domani annuncerà di essere il creatore di iPod.

<link>Lucio Bragagnolo</link>lux@mac.com