---
title: "Conflitto di interessi"
date: 2002-11-26
draft: false
tags: ["ping"]
---

Sono socio di una cosa che mi sembra bella. Lo posso dire?

Se tutto va bene, il 6 dicembre aprirà un nuovo negozio Macintosh. Sarà in via Carducci in galleria Borella, quella che porta a Sant’Ambrogio e all’Università cattolica. Avrà un grosso ed elegante spazio espositivo, un reparto assistenza, una zona riservata a corsi e a chi vuole imparare, scaffali con libri e riviste di Mac e tecnologia. Il venerdì resterà aperto fino alle 22 per eventi e feste a sorpresa e l’ultima domenica di ogni mese sarà aperto per chi arriva da fuori o non riesce a venire durante la settimana.

Insomma, dovrebbe essere un negozio Mac dove, sì, si vuole anche vendere, ma dove ci sia il piacere di incontrarsi, chiacchierare, discutere, lasciare i bimbi a giocare in uno spazio sorvegliato ed essere liberi di fare le cose con calma, coltivare la propria passione per Mac, farsi nuovi amici o vedere più spesso quelli vecchi.

Ci sono dentro anch’io, con una quota minuscola. Credo che sia un bel progetto e ho voglia di raccontarlo, a dispetto del conflitto di interessi con questa rubrica. :-)

<link>Lucio Bragagnolo</link>lux@mac.com