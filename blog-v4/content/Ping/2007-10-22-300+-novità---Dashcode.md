---
title: "300+ novità: Dashcode"
date: 2007-10-22
draft: false
tags: ["ping"]
---

Roba più per tecnici, ma mica cos&#236; tecnici. Una volta, con un po' di aiuto dalla documentazione e da Internet, ho creato in un'oretta scarsa una applicazione fatta e finita con la sua brava interfaccia utente. Si poteva fare clic su un pulsante <em>Ok</em> e avere un dialogo <em>Ok</em>. In altre parole: una persona intelligente con un pomeriggio a disposizione impara un sacco di cose.

Comunque, Le novità annunciate per Dashboard <a href="http://www.apple.com/it/macosx/features/300.html" target="_blank">sul sito Apple</a> sono queste.

Ci sono gli <em>snippet</em> di codice: si può avere una raccolta di frammenti di codice, che svolgono funzioni tipiche in molti programmi, e usarli con <em>drag and drop</em> dentro le applicazioni in costruzione.

Sono già pronti i componenti grafici tipici di un <em>widget</em>; li prendi, li metti nel <em>widget</em> e lui graficamente è già pronto. Probabile che anche di codice ne manchi veramente poco per essere completo.

Dashcode (la parte di Xcode per creare <em>widget</em>) ha un debugger istantaneo di JavaScript.

L'editor di codice è stato potenziato, con evidenziazione della sintassi maggiore di prima e una funzione di autocompletamento detta Sense Code.

Dashocode organizza automaticamente i file necessari per un <em>widget</em> e permette di creare il prodotto finale con un clic.

Infine, ci sono schemi pronti all'uso per creare <em>widget</em>.

