---
title: "Chi vuol essere Unix sia"
date: 2010-12-04
draft: false
tags: ["ping"]
---

L'apparente spietata rigidità dei computer spaventa molti.

Suggerisco di imparare il Terminale e poi digitare i comandi (per esempio <code>uptime</code>) solo a seguito di <code>[ $(( $RANDOM % 2 )) -eq 0 ] &#38;&#38;</code> (per esempio <code>[ $(( $RANDOM % 2 )) -eq 0 ] &#38;&#38; uptime</code> e poi premere Invio).

In questo modo il comando verrà eseguito, con frequenza casuale, solo il cinquanta percento delle volte.

Del command (con l'accento sulla <i>a</i>) non v'è certezza!