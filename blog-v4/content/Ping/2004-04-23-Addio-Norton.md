---
title: "Addio Norton, hai fatto il tuo tempo"
date: 2004-04-23
draft: false
tags: ["ping"]
---

Il miglior complimento che si possa a fare a Mac OS X

Symantec ha annunciato di cessare la produzione delle Norton Utilities per Macintosh e di concentrarsi su applicazioni per la sicurezza in Internet, che supporteranno il Macintosh.

Da tempo fare girare le Norton Utilities per risolvere un problema su Mac OS X significava solo perdere ore preziose. Da tempo era chiaro che nel mondo Unix le (notevoli) conoscenze accumulate negli anni dai programmatori delle Norton valgono abbastanza poco.

Chissà dove sarà ora Peter Norton, il creatore originale delle Utilities, scritte per cambiare lavoro nel momento in cui era stufo di fare l’impiegato. Certamente ha guadagnato sufficientemente per non preoccuparsi del passaggio di Mac a un mondo Unix. Migliore del precedente.

<link>Lucio Bragagnolo</link>lux@mac.com