---
title: "Spegnere i nosotti<p>"
date: 2005-06-05
draft: false
tags: ["ping"]
---

Se vogliamo tutte le risorse del Mac a disposizione

Mi scrive Giuseppe:<p>

<cite>A proposito di Dashboard, ho notato che a volte prende possesso fino al 27% della capacità di lavoro della CPU del mio iBook G3 800&hellip; Su Macupdate.com ho trovato <a href="http://www.macupdate.com/info.php/id/18261">DashOnOff 1.0</a>, che permette di scegliere se tenere Dashboard acceso oppure spento. E, quando necessario, mi dà la possibilità di dirottare risorse di calcolo ad altre applicazioni.</cite><p>

Ipse dixit!<p>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>