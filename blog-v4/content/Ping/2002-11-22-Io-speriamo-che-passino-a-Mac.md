---
title: "Io speriamo che passino a Mac"
date: 2002-11-22
draft: false
tags: ["ping"]
---

Un domani non lontano Macintosh potrebbe diventare un bastione di libertà personale

Se non hai ancora sentito parlare di Palladium, meglio.
Quando tutti sapranno di Palladium sarà più vicino il giorno in cui i computer conterranno componenti hardware dotati di una chiave cifrata collegata al sistema operativo e accessibile solo al costruttore, o a Microsoft.

Per semplificare, domani un computer Palladium potrebbe dirti “No, non puoi suonare questo Mp3 che ti sei fatto per uso personale da un Cd audio che hai regolarmente acquistato, perché il file non è stato autorizzato. Puoi disattivare Palladium, ma al prezzo di non usare il lettore Cd”.

Tuttavia restano speranze. La prima è che Palladium sia la goccia che fa traboccare il vaso e faccia capire finalmente alla gente che sta usando un prodotto inferiore; la seconda è che Apple non si è assolutamente pronunciata ma, al contrario di Ibm, Intel e altri costruttori che lo hanno fatto, non sembra incline a trattare i propri clienti in questo modo.

Domani Apple potrebbe produrre gli unici computer liberi esistenti al mondo. Forse è il caso di fare la cosa giusta da subito.

<link>Lucio Bragagnolo</link>lux@mac.com