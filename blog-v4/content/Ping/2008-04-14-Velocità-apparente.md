---
title: "Velocità apparente"
date: 2008-04-14
draft: false
tags: ["ping"]
---

Incuriosito dall'arrivo di una beta migliore, ho provato <a href="http://sourceforge.net/projects/ibench" target="_blank">iBench</a>, l'ennesimo test di velocità per Mac OS X.

Il primo test è la generazione di un frattale in modalità <em>single thread</em> che usa tutta la memoria possibile.

Gli altri non li ho visti.

Dopo che erano passati molti secondi e che il test era ancora nelle sue fasi iniziali, ho pensato che avrei potuto anche passare un quarto d'ora ad aspettare, perché i test sono venti.

Continuare a lavorare intanto che funziona il test è un controsenso, perché sottraggo risorse al test e penalizzo il risultato.

Ho capito che il test per vedere se il Mac è veloce rallenta <em>me</em> fino a velocità zero.

Alla fine uno è contento perché il suo Mac è veloce, ma ha perso tempo. E se devi stare senza fare niente a guardare un Mac velocissimo, a che ti serve che sia veloce?