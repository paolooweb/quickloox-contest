---
title: "Fermate il mondo, voglio salire"
date: 2011-02-01
draft: false
tags: ["ping"]
---

Grandioso <a href="http://www.adweek.com/aw/content_display/news/digital/e3i543ab57159cb298a4d8c4c408b7a539d?pn=1" target="_blank">articolo di Adweek</a> sulla lavorazione di <i>1984</i>, lo <i>spot</i> Apple che annunciò al mondo Macintosh e fece sensazione durante Super Bowl.

Compilato da Steve Hayden, che lavorò allo <i>spot</i> in quanto tale, contiene vari particolari che modificano la vulgata.

Per esempio, non è vero che 1984 andò in onda solo durante il Super Bowl. Invece fu proiettato sui dieci maggiori mercati americani e anche nei cinema, seppure in versione da trenta secondi (al Super Bowl andò la versione da sessanta).

Il Grande Fratello dello spot non era Ibm, come in molti si è supposto, ma piuttosto il generico governo totalitario che opprime la popolazione.

All'inizio la protagonista aveva una mazza da <i>baseball</i> e fu il regista Ridley Scott a volere il martello da guerra.

Lo <i>spot</i> rischiò di non andare in onda. Apple non voleva spendere il denaro necessario per un giorno supplementare di riprese; i consiglieri di amministrazione, dopo averlo visto in sessione privata, scuotevano la testa o se la tenevano tra le mani, mentre qualcuno chiedeva di rompere il contratto con l'agenzia pubblicitaria. E Steve Jobs aveva inizialmente cassato l'idea di trasmetterlo durante il Super Bowl, che blocca l'America per una domenica oggi e ancora di più nel 1984.

Al solito, Steve Wozniak dichiarò che se necessario avrebbe pagato di persona metà della cifra occorrente per mandarlo in onda.

A onore di Jobs va anche detto che fu lui a dare la linea dello <i>spot</i>: <cite>voglio fermare il mondo</cite>, ordinò.

Ci riusc&#236;.

Lo <i>spot</i> arrivò in fondo anche per merito di Hayden, che a un certo punto si prese una responsabilità cruciale, a rischio di licenziamento, in assenza del suo capo.

Appena andato in onda lo <i>spot</i>, Hayden ricevette una telefonata dal suo capo, Jay Chiat <cite>come ci sente a essere una fottuta</cite> star<cite>?</cite>

La sua risposta fu <cite>alla grande. Solo, non chiedermi di rifarlo l'anno prossimo</cite>.

Nell'articolo si trovano molti altri particolari.

(a margine, per la notte tra domenica e luned&#236; ho provveduto a reperire bistecca, patatine e Coca-cola).