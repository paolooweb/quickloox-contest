---
title: "Dura lex, ma si lex"
date: 2002-03-26
draft: false
tags: ["ping"]
---

Come riuscire a leggere i numeri passati di questa rubrica

Ping sta facendo la storia del Mac italiano.
“Bum!”
Vabbeh, ok, è ugualmente la prima striscia quotidiana italiana interamente dedicata a Macintosh. Ha un suo pubblico di fedelissimi (formalmente non più di venti, per deferenza a Manzoni e a Beppe Severgnini) e qualcuno che cerca, di tanto in tanto, di consultare un Ping già pubblicato cercando di trovarlo nei gioiosi meandri di Macworld online.
Daniele Volpin è riuscito a estorcere il segreto alla redazione: bisogna selezionare la sezione Opinioni sulla pulsantiera in alto del sito e inserire la parola Ping nel campo di ricerca. Appariranno tutti i Ping già pubblicati. Scegliendo la ricerca avanzata si potrà anche circoscrivere la ricerca a date precise.
Daniele, sei un benemerito. Non c’ero riuscito neanch’io.

<link>Lucio Bragagnolo</link>lux@mac.com