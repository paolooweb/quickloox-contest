---
title: "Il copiaincolla aperto chiude"
date: 2008-08-26
draft: false
tags: ["ping"]
---

Come ha scritto John Gruber di Daring Fireball, il progetto <em>open source</em> <a href="http://www.openclip.org" target="_blank">OpenClip</a>, che mira a dare agli iPod touch un copiaincolla in assenza di Apple, <a href="http://daringfireball.net/2008/08/raining_on_the_openclip_parade" target="_blank">è una ingegnerizzazione astuta di una idea poco saggia</a>.

Talmente poco saggia che OpenClip sfrutta una particolarità del firmware che non esisterà più a partire dalla versione 2.1, si dice in arrivo per settembre.

Il copiaincolla di OpenClip può funzionare solo tra applicazioni che ne supportino il <em>framework</em>, l'architettura di base. Solo che il <em>framework</em>, cos&#236; com'è oggi, smette di funzionare entro un mese e di conseguenza non si vede quali sviluppatori potranno essere cos&#236; pazzi da adottarlo. Per farlo funzionare nuovamente, tra l'altro, non basterà una <em>patch</em>: OpenClip attualmente funziona grazie al fatto che una applicazione può leggere nello spazio riservato di un'altra applicazione e questo non sarà più possibile ai programmi indipendenti. Ergo occorre proprio un'idea migliore, ammesso che per un programma indipendente sia possibile.

In definitiva, se c'è un'esigenza di copiaincolla su iPod touch, OpenClip è certamente una risposta. Sbagliata.