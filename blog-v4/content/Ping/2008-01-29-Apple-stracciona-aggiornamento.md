---
title: "Apple stracciona / aggiornamento"
date: 2008-01-29
draft: false
tags: ["ping"]
---

Polemiche a non finire perché quella stracciona di Apple fa pagare 17,99 euro di aggiornamento software per gli iPod touch con software 1.1.2, mentre i nuovi iPod touch contengono già il software 1.1.3, con le nuove cinque applicazioni, e il prezzo è esattamente lo stesso.

Il prezzo non è esattamente lo stesso.

Una mail di Apple ai rivenditori autorizzati, datata 16 gennaio (il giorno dopo l'annuncio), abbassa di 18 euro il prezzo degli iPod touch 1.1.2.

Chi compra un iPod touch e ci trova dentro il software vecchio, quindi, deve pagare l'aggiornamento su Internet se vuole il nuovo software, ma alla fine il costo è uguale a quello dell'acquisto di un iPod nuovo.

Chi aveva un iPod touch vecchio comprato prima dell'annuncio deve pagare effettivamente 17,99 euro in più se vuole l'aggiornamento, ma a questo punto si fa un po' diversa. È successo infinite volte che la nuova versione di un computer contenesse più cose allo stesso prezzo della versione vecchia e che i compratori della versione vecchia si trovassero con qualcosa in meno, giusto o sbagliato che sia. Ora, più che altro, si tratta di discutere se l'aggiornamento valga quei soldi oppure no.