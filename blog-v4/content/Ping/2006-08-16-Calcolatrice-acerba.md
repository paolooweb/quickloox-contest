---
title: "Calcolatrice acerba"
date: 2006-08-16
draft: false
tags: ["ping"]
---

Ma molto interessante, in prospettiva. Per cominciare, visualizza numeri fino a 25 cifre, anche se bisogna farlo dalla tastiera perché nelle Preferenze ancora non funziona. Con un po' di tempo e di appoggio potrebbe diventare mica male.

Se qualcuno è direttamente capace di migliorarla, peraltro, di <a href="http://home.swiftdsl.com.au/~mattg/" target="_blank">Magic Number Machine</a> è disponibile il sorgente.