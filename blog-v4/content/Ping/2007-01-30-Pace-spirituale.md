---
title: "Pace spirituale"
date: 2007-01-30
draft: false
tags: ["ping"]
---

C'è un perché in tutte le cose, compreso il fatto che <strong>Mario</strong> mi abbia segnalato questa <a href="http://flickr.com/photos/timsimpson/255420579/" target="_blank">foto del Dalai Lama</a>.

Probabilmente ha a che fare con la storia di Joe Hutsko, reporter di Msnbc. Aveva un Mac ed è passato a Vista. <a href="http://www.msnbc.msn.com/id/16873608/" target="_blank">Ne parla molto bene</a>. E ora è tornato a Mac.

Come ha chiosato MacDailyNews: <em>Microsoft. Your frustration, our fault.</em>