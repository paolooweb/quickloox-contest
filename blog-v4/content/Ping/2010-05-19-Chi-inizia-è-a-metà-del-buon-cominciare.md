---
title: "Chi inizia è a metà del buon cominciare"
date: 2010-05-19
draft: false
tags: ["ping"]
---

C'era da risolvere in casa un banalissimo problema di immagini da ridimensionare a una base prefissata. Immagini in un certo numero, noioso farlo a mano&#8230; compito ideale per Graphic Converter, ma mi piaceva l'idea di sfruttare le capacità di serie di Mac OS X. Con un programma ad hoc è troppo facile.

Cos&#236; ho aperto Automator. Sempre con un po' di ansia, perché quando voglio fare (parola grossa) programmazione entro sempre in agitazione.

Lanciato Automator, ho specificato che volevo una Azione cartella. Ho indicato la cartella e poi si è trattato di trascinare nello spazio di lavoro prima l'azione <code>Ridimensiona immagini</code> e poi l'azione <code>Sposta elementi del Finder</code>.

Registrato il tutto, ecco una cartella che accetta in pasto una o più immagini e le manda dove voglio dopo essere state ridimensionate automaticamente a una stessa misura di base.

A guardarlo dopo averlo fatto è roba che capisce un bambino. Prima di farlo, però, ero molto meno rilassato.

Il che mi insegna a provarci. Imparare ad automatizzare il proprio Mac non è questione di fare cose drammaticamente complicate; semmai di cominciare. Magari aiutarsi un po' con Internet, leggere gli aiuti, chiedere in giro, ma il punto è che la si fa più grossa di quello che è in realtà. Chi inizia è già avanti, la vera difficoltà è questa ed è quasi tutta e quasi solo mentale.