---
title: "Password tira e molla"
date: 2010-07-09
draft: false
tags: ["ping"]
---

Attualità ed esperienza.

Si moltiplicano gli inviti a <cite>cambiare password</cite> in merito alla vicenda degli account di iTunes violati.

La gente che spiega il successo di Apple con il marketing (i prodotti lascerebbero a desiderare, ma la pubblicità è ottima) si dimentica di rilevare che anche i problemi &#8211; di sicurezza o ricezione che siano &#8211; ricevono lo stesso trattamento e vengono ingigantiti rispetto alla loro portata reale.

I server di iTunes non sono stati compromessi. Se lo fossero stati, le segnalazioni non sarebbero qualche decina su centocinquanta milioni, ma si andrebbe direttamente al telegiornale della sera.

Il consiglio di <i>cambiare password</i> è mal posto. iTunes segnala qualsiasi acquisto con una mail; se non sono arrivate mail, l'account è a posto. La password funziona. Se la password sta funzionando, ha senso cambiarla?

Non bisogna <i>cambiare</i> la password; bisogna, semmai, <i>migliorarla</i>. Farla più lunga, aggiungere numeri o segni particolari, aumentarne la complicazione. Cambiarla e basta significherebbe che si rischia di adottare una password peggiore a causa di una paura infondata, cioè il primo stadio dell'insicurezza.

Ho acquistato recentemente un prodotto sul sito Adobe. Ho dovuto creare una Adobe Id e mi è stato chiesto di scegliere un <i>nome della schermata</i>, la loro assurda traduzione italiana di <i>screen name</i> (nome che appare a video). Ho scelto <i>acquacedratario</i>, in omaggio a Leopardi, ed è stato rifiutato per <i>linguaggio inadeguato</i>.

Poi ho inserito l'indirizzo, un normalissimo indirizzo italiano, per vedere l'ammonimento che non segue la <i>struttura standard</i> dei loro database.

Bene o male sono arrivato in fondo. Ho ricevuto la mail di conferma, graficamente abbellita dalla scritta <i>Grazie per avere comprato il prodotto dell'Adobe</i>. Un grafico eccellente, con una licenza elementare strappata a fatica.

È un sentimento del tutto irrazionale e ingiustificato, ma se fatico a prendere sonno la notte non sto pensando alla mia carta di credito su iTunes Store; sto pensando a quella sul sito Adobe, dove se decido di chiamarmi <i>acquacedratario</i> si offendono. Ho pagato, ma domani riscarico <a href="http://www.scribus.net" target="_blank">Scribus</a>.