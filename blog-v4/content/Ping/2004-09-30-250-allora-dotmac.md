---
title: "250, allora<p>"
date: 2004-09-30
draft: false
tags: ["ping"]
---

.Mac diventa sempre meglio e i costi restano gli stessi<p>

Investire nel servizio .Mac si sta rivelando sempre più azzeccato con il tempo che passa. Mi ha scritto ieri Apple, comunicandomi che lo spazio di iDisk passa da 100 a 250 megabyte e che la dimensione massima di un file di email si è alzata a dieci megabyte.<p>

Fosse solo questo&hellip; ma ci sono i programmi in regalo, la promozione sull&rsquo;abbonamento a MacAddict, i tutorial QuickTime sulle tecnologie Apple, la pubblicazione dei calendari iCal e un sacco di altre cose, dalle pagine home in poi. Niente di incredibile, tutto fattibile anche con altri sistemi, ma su .Mac sono più semplici e funzionano.<p>

No, una cosa speciale c&rsquo;è: si possono creare alias di email a piacere. Gran comodità proprivacy e antispam. Che praticamente nessun altro offre con la stessa immediatezza.<p>

E allora? Allora .Mac è un&rsquo;opportunità interessante per lavorare meglio, al passo con i tempi e con qualcosina in più. Ho rinnovato il mio abbonamento con piacere.<p>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>