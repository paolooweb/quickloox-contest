---
title: "Rumor di valore"
date: 2010-12-02
draft: false
tags: ["ping"]
---

<a href="http://www.longbets.org/bets" target="_blank">Long Bets</a> non è un sito di <i>rumor</i>. Chi formula una previsione ci mette soldi. Chi la contesta (e vince) ne può incassare.

Sarebbe bello vedere un sito di <i>rumor</i> che chiede soldi ai lettori per ogni anticipazione esatta e dà soldi ai lettori per ogni anticipazione sbagliata. Sarei in prima fila a leggere.

Altrimenti è fuffa. Mi stava venendo una parola simile in rima, ma si va sul giuridico.