---
title: "O elettromeccanica o morte"
date: 2007-08-15
draft: false
tags: ["ping"]
---

<a href="http://www.macjournals.com/" target="_blank">Mdj</a> consiglia ai limiti della sconsideratezza la tastiera <a href="http://matias.ca/tactilepro2/" target="_blank">Tactile Pro 2.0</a>, fatta con tecnologie elettromeccaniche di controtendenza. E anche TidBits <a href="http://db.tidbits.com/article/7607" target="_blank">ne parlava bene</a> già tre anni fa. Se fossimo nell'antica Roma, avrebbero parlato Marte e Venere direttamente via oracolo.

Non è la tastiera che serve a me (la mia deve essere wireless, con una trackpad o trackball integrata). Però, con queste raccomandazioni, vuol dire che è veramente una bomba, e il prezzo (notevole) non lo guarderei nemmeno.