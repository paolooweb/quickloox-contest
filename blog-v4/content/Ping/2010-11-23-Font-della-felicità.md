---
title: "Font della felicità"
date: 2010-11-23
draft: false
tags: ["ping"]
---

Dopo una giornata di spostamenti ho finalmente ritrovato la strada di casa e messo a sincronizzare iPad, che ha diligentemente installato iOS 4.2 (per la precisione 4.2.1, <i>build</i> 8C148).

E adesso il <i>multitasking</i>, le cartelle, GameCenter, AirPrint, iWork aggiornato, tutto quello che volete. Io apro le impostazioni di Note, vedo che ci sono solo tre font a disposizione, però due in più di prima e diversi da Marker Felt. Mi si allarga il cuore.