---
title: "Non è semplice semplificare"
date: 2009-03-23
draft: false
tags: ["ping"]
---

In <a href="http://www.potionfactory.com" target="_blank">Potion Factory</a> stanno scrivendo The Hit List, una nuova applicazione di task management (organizzazione attività) che promette bene. Dal sito si può scaricare una versione di anteprima.

Si sono posti il problema di come impostare l'interfaccia per l'indicazione delle azioni ripetitive.

Hanno iniziato a lavorare con pulsanti, cursori, menu, crocette, pallini e alla fine hanno scelto la cosa più semplice e complicata di tutte: un campo testo in cui l'utilizzatore scrive ogni quanto ripetere l'azione (ogni due mesi, il terzo e il quarto venerd&#236;, cose cos&#236;). Il programma <a href="http://www.potionfactory.com/blog/2009/03/10/better-software-through-less-ui" target="_blank">fa del proprio meglio</a> per capire esattamente che cosa è stato scritto.

In questo modo l'interfaccia è straordinariamente pulita. Eppure, come ha scritto qualcuno, la complessità visiva è stata trasformata in complessità cognitiva; è l'utilizzatore a dover sapere esprimere esattamente quello che vuole e, per dire, non è neanche cosciente di quanto può fare il programma per lui. Potrebbe perdersi una opzione utile o potente per non sapere che cosa può scrivere e che cosa no.

La vicenda sta sollevando una discussione interessante e naturalmente il primo problema è che l'approccio di The Hit List prescrive la conoscenza dell'inglese oppure la disponibilità dell'azienda a effettuare localizzazioni a raffica, per tutte le nazioni in cui c'è un interesse a vendere.

Non è semplice semplificare. Rispetto, che so, all'interfaccia di iCal per stabilire azioni ripetitive, l'approccio di The Hit List è migliore o peggiore?