---
title: "Connesso a metà"
date: 2007-02-25
draft: false
tags: ["ping"]
---

La cosa peggiore non è che la rete non funzioni. È quando la cosiddetta Adsl appare funzionante e invece fornisce giusto uno sgocciol&#236;o di dati, che a volte una pagina si carica e a volte no, non sai se una mail in partenza parte veramente, non sai se il download procede molto lentamente o se invece è solo un'illusione e cos&#236; via. Passare una giornata cos&#236; (e l'ho passata) fa uscire pazzi e vorrei proprio sapere se è una simpatica peculiarità italiana o se queste cose accadono anche fuori dai confini.