---
title: "Tutti a Webbit da stamattina"
date: 2003-05-09
draft: false
tags: ["ping"]
---

Una fiera informatica davvero diversa e c’è posto anche per Mac

È cominciato Webbit 2003 e bisognerebbe cominciare a farci più attenzione, perché è una formula di fiera più cultura più intrattenimento che sa molto di futuro, diversamente da quel Titanic che è diventato lo Smau.

Ancora una volta l’arena interna è piena di mille appassionati di Web design, grafica, animazione, programmazione e software libero collegati in rete e in Rete 24 ore su 24. A Webbit non si dorme mai e mentre scrivo queste note, intorno alle due del mattino, è appena iniziato il Nutella party.

Perché parlarne su Macworld online? Perché si vedono tanti Mac, più del passato. Perché ci saranno seminari dedicati, per esempio, all’uso della parte Unix di Mac Os X. Perché ci si collega alla rete interna via AirPort da qualsiasi punto dell’arena. Perché sono presenti tutti i gruppi di utenza Mac che contano, dal PowerBook Owners Club a Musimac, per arrivare all’informazione di Macity e a decine di Mac che occhieggiano dai luoghi più impensati.

Ho avuto l’onore di presentare una conferenza stampa di Jon “Maddog” Hall, uno dei maggiori evangelisti Linux al mondo. E tanti fautori del software libero guardano con favore inedito a quegli strani computer con la mela sopra, su cui ora funziona uno splendido sistema operativo basato su Unix.

Un consiglio? Vieni a Padova e fai un giro a Webbit. Entri gratis stampando (tutte le volte che vuoi) il pass che trovi <link>qui</link>http://www.webb.it/static/coupon/270405.

<link>Lucio Bragagnolo</link>lux@mac.com