---
title: "La nemesi del web"
date: 2008-12-08
draft: false
tags: ["ping"]
---

Per anni Microsoft ha cercato di trasformare il web, bello perché cosa di tutti, in cosa sua. Prima ha strangolato Netscape in modo che ci fosse un unico <em>browser</em> sulla piazza. Poi ha applicato la tua strategia <em>embrace and extend</em>, cioè <em>adotta uno standard e poi modificalo in modo che funzioni bene solo con il tuo software</em>. Ci ha provato con Java e chiunque abbia affrontato il problema ricorda le estensioni proprietarie che il software di Microsoft FrontPage inseriva nei siti fatti con quel programma, virtualmente invisibili da ciò che non fosse Internet Explorer.

Siamo stati piagati per anni da siti visibili solo con Internet Explorer e certe volte solo con Internet Explorer usato sotto Windows. Lavori di incapaci ignoranti, al meglio; altrimenti di incivili che lo facevano con intenzione e premeditazione.

Adesso che sono passati anni, che Internet Explorer è rotolato in fondo a tutte le classifiche di rispetto degli standard e che grazie al cielo ci sono più <em>browser</em> che icone nel mio Dock, non stupisce che Microsoft raccolga quanto ha seminato.

Recentemente Jamis Buck, sviluppatore di <a href="http://capify.org/" target="_blank">Capistrano</a> (<em>utility</em> per automatizzare l'invio di comandi a computer lontani) ha ricevuto un messaggio che lamentava l'assenza di supporto per Capistrano su Windows e si concludeva cos&#236;: <cite>non sono un sostenitore di Microsoft ma, che piaccia o meno, sono la presenza ingombrante che nessuno</cite> (letterale: <cite>sono il gorilla da 350 chili nella stanza</cite>).

Buck ha risposto: <cite>può darsi, ma non è il</cite> mio <cite>gorilla e non è nella</cite> mia <cite>stanza. Se qualcuno ha bisogno di tenere buono il gorilla, con tutto il rispetto, è un suo problema</cite>. La <a href="http://groups.google.com/group/capistrano/msg/f5213577eaeadc47?pli=1" target="_blank">risposta completa</a> è ben più articolata e illuminante e ne consiglio la lettura.

John Gruber di Daring Fireball, <a href="http://daringfireball.net/linked/2008/12/07/not-my-gorilla" target="_blank">a commento dell'episodio</a>, afferma: <cite>in modo analogo, ho smesso di preoccuparmi di come Daring Fireball appaia in Internet Explorer. [&#8230;] Molta gente sarebbe molto più felice se smettesse di preoccuparsi dei gorilla altrui</cite>.

Non è esattamente la mia posizione. Sostengo e continuerò a sostenere che il web deve essere per tutti e quindi anche per i poveri di <em>browser</em>, che sanno o possono o (stupidamente) vogliono usare solo Explorer.

La comprensione, però, no. Ho visto per anni i sogghigni arroganti di quelli che un sito andava solo su Explorer e tanto Mac aveva il tre percento, e lo standard di fatto, e basta con queste eccezioni che aumentano i costi, e arrangiatevi che siete quattro gatti. Se la stessa gente adesso arriva a supplicare attenzione perché sono in tanti, beh, la meritano tutti, quindi anche loro.

Però facciamoli pietire almeno un po', prima. Sono lezioni che servono.