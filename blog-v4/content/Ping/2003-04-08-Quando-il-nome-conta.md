---
title: "Quando il nome conta"
date: 2003-04-08
draft: false
tags: ["ping"]
---

Mac OS X mastica Unicode; Mac OS 9, a volte...

Impaginatori e musicisti sono due delle categorie che si trovano ancora spesso e volentieri a lavorare dentro Mac OS Classic. A loro, ma non solo, sarà quindi certamente successo di scoprire che a volte un nome file in Mac OS 9 non appare uguale a se stesso in Mac OS X.

Il problema si deve al fatto che Mac OS 9 ha alcune piccole imperfezioni nel trattamento delle stringhe Unicode. Unicode (.org) è lo standard universale di rappresentazione del testo in grado di codificare decine di migliaia di caratteri in tutte le lingue e tutti i sistemi di scrittura.

Per farla breve, Apple ha messo a punto <link>Apple File Name Encoding Repair 1.0</link>http://docs.info.apple.com./article.html?artnum=86182. Usalo e si risolve tutto. :-)

<link>Lucio Bragagnolo</link>lux@mac.com