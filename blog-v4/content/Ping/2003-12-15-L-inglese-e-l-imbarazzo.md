---
title: "L’inglese e l’imbarazzo"
date: 2003-12-15
draft: false
tags: ["ping"]
---

Avere software localizzato è un dovere dell’azienda. Non rendersi ridicoli è quello dell’utente

Molte volte sento lamentarsi di questo o quel software che non è localizzato in italiano. Lo stesso servizio .mac non ha un’interfaccia Web nella nostra lingua.

Trovo che Apple, come qualsiasi altra azienda, dovrebbe offrire il servizio migliore possibile e in quanto tale rendere disponibile tutto quello che produce, e non solo il software di sistema in italiano.

Trovo anche che bisognerebbe anche metterci un po’ di buona volontà e non rendersi ridicoli. Per essere sostenitori legittimi del software localizzato bisogna prima dimostrare davanti a una giuria competente di saper scrivere Panther, “P-a-n-t-h-e-r”. Sulle mailing list e sui forum si vedono cose veramente imbarazzanti. D’accordo non sapere l’inglese, ma almeno riuscire a imitare i segni che si vedono sulla scatola.

<link>Lucio Bragagnolo</link>lux@mac.com