---
title: "Il fantasma Formaggino"
date: 2009-07-22
draft: false
tags: ["ping"]
---

<a href="http://www.computerworld.com/s/article/print/9003718/Opinion_Why_Microsoft_s_Zune_scares_Apple_to_the_core" target="_blank">Mike Elgan su Computerworld</a>, 28 settembre 2006 (il mio è un brevissimo estratto):

<b>Perché lo Zune di Microsoft spaventa Apple nel profondo</b>

[&#8230;] <cite>Zune sarà supportato e promosso e valorizzerà la potenza collettiva di Windows Xp, Vista, Soapbox (il nuovo</cite> YouTube killer<cite>) e Xbox 360.</cite>

[&#8230;] <cite>Microsoft ha il denaro, la copertura, le </cite>partnership<cite>, la determinazione e la quota di mercato per portare Vista, Soapbox, Xbox e Zune nelle vite di centinaia di milioni di consumatori.</cite>

<cite>iPod regna &#8212; per ora. Ma Microsoft non può essere trattata come l'ennesimo nuovo arrivato. E nessuno lo sa meglio di Apple.</cite>

Impressionante. <cite>Centinaia di milioni di consumatori</cite>.

A oggi Zune potrebbe avere venduto tre milioni di unità, a prezzi di svendita dopo il disastro di Natale. iPod, centocinquanta.

Vista è un mezzo incubo da cui Microsoft non vede l'ora di tirarsi fuori. Xbox 360, bella, numero tre in un mercato con tre contendenti.

Soapbox, <i>YouTube killer</i>, <a href="http://news.cnet.com/8301-10805_3-10292031-75.html?part=rss&amp;subj=news&amp;tag=2547-1_3-0-5" target="_blank">ha annunciato la chiusura per il 31 agosto</a>.

Lo spavento che fa Zune, tre anni dopo la sua uscita, inizia a somigliare a quello del titolo.