---
title: "Come scrive la gente che scrive"
date: 2008-06-03
draft: false
tags: ["ping"]
---

Fare un elenco di tutti i programmi per scrivere è idiota. Nessuno è in grado di provare con coscienza e scegliere con certezza tra tutti i programmi per scrivere che esistono.

Considerare un insieme ristretto permette invece di farsi un'idea molto meglio.

Questo è ciò che usano, e che hanno recensito, <a href="http://www.macworld.com/article/133699/2008/05/vodcast53_writing.html" target="_blank">gli autori di Macworld Usa</a>. La pagina è in inglese, ma ci sono i link ai siti dei programmi e fare clic è attività linguisticamente neutra.