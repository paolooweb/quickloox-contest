---
title: "Pulizie primaverili"
date: 2006-05-14
draft: false
tags: ["ping"]
---

Preso da entusiasmo infantile, ho disattivato tutti i widget di Dashboard che non ho minimamente usato negli ultimi sei mesi. Sono rimasti calcolatrice, indirizzario, dizionario e Coconut Battery.