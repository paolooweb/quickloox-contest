---
title: "Gusto, passione e formaggio"
date: 2007-08-07
draft: false
tags: ["ping"]
---

Due parole sulla mia scampagnata a <a href="http://www.comune.hone.ao.it/it/" target="_blank">H&#244;ne</a> a chiacchierare di storia del Mac.

Il posto è incantevole, già montagna anche se sono tre-quattrocento metri. Paese piccolissimo dove ogni scampolo di spazio è stato sfruttato al meglio con un nitore e una cura che commuovono e fanno pensare a come si potrebbe vivere davvero meglio se le strade fossero di tutti, come succede l&#236;, invece che di nessuno, come in altri luoghi. Il mio pomeriggio si fotografa nel fatto che nel campo da basket ci sono i palloni a disposizione. Sono entrato, ho ritrovato me stesso cinque minuti, ho segnato da tre punti e mi sono rimesso a passeggiare con quindici anni di meno.

L'esposizione dell'<a href="http://www.allaboutapple.com/speciali/hone.htm" target="_blank">All About Apple Museum</a> è filologicamente ineccepibile, benissimo realizzata, se si passa di l&#236; vale la pena della sosta. Ci sono anche un po' di copie di Macintosh Story, lo dico soprattutto per <strong>emaskew</strong>. :-)

La cena gentilissimamente offerta da <a href="http://www.allaboutapple.com/personali/walter_franceschi.htm" target="_blank">Walter</a> è stata deliziosa. I formaggi della Valle sono infiniti e uno più saporito dell'altro. Ho fatto talmente tanti complimenti ai biscotti alle castagne che me ne è stato regalato un sacchetto. Non ho fatto apposta (quando mi ci metto sono un <em>gaffeur</em> nato) ma ho molto gradito.

Al termine della serata il sindaco mi ha coperto di regali, tutti da leggere e anche qualcosa da giocare. Questo è il posto più pubblico che ho per ringraziarlo ufficialmente dopo averlo fatto in privato.

Nella mia prolusione ho girato intorno a <a href="http://www.folklore.org/StoryView.py?project=Macintosh&amp;story=I'll_Be_Your_Best_Friend.txt&amp;sortOrder=Sort%20by%20Date&amp;detail=medium" target="_blank">Burrell Smith</a>, <a href="http://www.pacifict.com/Story/" target="_blank">Ron Avitzur</a> e <a href="http://www.folklore.org/StoryView.py?project=Macintosh&amp;story=Puzzle.txt&amp;sortOrder=Sort%20by%20Date&amp;detail=medium&amp;search=puzzle" target="_blank">Andy Hertzfeld</a>, parlando di gusto e passione nel fare le cose e di come il gusto e la passione facciano la differenza, esattamente come la cucina dell'albergo di H&#244;ne era una cucina normalissima ma la cena è stata la migliore degli ultimi cinque anni.

Quando ti dicono che dentro i computer sono tutti uguali, beh, anche i fornelli. I cuochi non lo sono.