---
title: "Tutti trucchi"
date: 2008-01-02
draft: false
tags: ["ping"]
---

Appletell ha pubblicato una pagina di <a href="http://www.appletell.com/apple/comment/ultimate-leopard-tweaking-guide/" target="_blank">trucchi da usare su Leopard</a> che trovo avere un ottimo rapporto informazione/dimensione.

Hanno anche <a href="http://www.appletell.com/apple/comment/macworld-transcripts-leaked/" target="_blank">un'altra pagina</a> in cui si cita una ipotetica copia dello script che pronuncererebbe Jobs al Macworld Expo. Presa uguale uguale da un <a href="http://forums.macrumors.com/showthread.php?t=407435" target="_blank">forum di MacRumors.com</a>, presentata con le parole <cite>sentiamo che non rappresenti una trascrizione legittimata di che cosa aspettarsi da Macworld Expo 2008</cite>.

Sempre trucchi, per fare ascolto stavolta. Sono più interessanti quelli per Leopard.