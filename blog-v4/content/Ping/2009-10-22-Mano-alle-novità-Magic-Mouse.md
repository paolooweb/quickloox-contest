---
title: "Mano alle novità: Magic Mouse"
date: 2009-10-22
draft: false
tags: ["ping"]
---

Casualmente mi sono ritrovato ieri in <a href="http://www.atworkgroup.net/" target="_blank">@Work</a> proprio mentre arrivavano le primissime consegne delle novità Apple in ambito Mac.

Magic Mouse è affascinante. Molto più basso di Mighty Mouse (che diventa Apple Mouse per questioni di copyright), ha una linea parabolica filante e fantascientifica. Difficile resistere all'attrazione estetica dell'oggetto.

La parte inferiore è realizzata in solido alluminio, che appoggia sul tavolo tramite due binari di materiale da scorrimento. Il pulsante di accensione (Magic Mouse è <i>wireless</i>) è molto più solido è soddisfacente che su Mighty Mouse.

Paradossalmente, non è possibile &#8211; adesso &#8211; collaudarne le funzioni più interessanti, cioè la superficie che oltre a essere cliccabile permette il <i>multitouch</i>: il <i>software</i> apposito per il mouse compare in Mac OS X 10.6.2, ora in corso di approvazione, e poi ci sarà un aggiornamento speciale apposito per i Mac che vogliono usare Magic mouse senza usare Snow Leopard. Al momento, il risultato che si ottiene è un normale mouse a due pulsanti.

I ragazzi dello Store hanno qualche perplessità sul fatto che Magic Mouse possa scorrere troppo su una superficie eccessivamente permissiva. Lo si potrà verificare solo con l'arrivo del <i>multitouch</i>. Paradossalmente, <a href="http://www.apple.com/it/magicmouse/" target="_blank">Magic Mouse</a> è un mouse fatto per lavorare benissimo in movimento, oppure da fermo; il multitouch lo candida più o meno a fare le funzioni di una <i>trackball</i>. Quindi la sua bontà dipende dal delicato equilibrio tra facilità di movimento e sicurezza che resti fermo quando a muoversi devono essere solo le dita in scorrimento.

Il mio <i>feeling</i> è ottimo, ma mi accorgo di non avere la testa educata a questa possibilità di <i>multitouch</i>; ora, quando mi serve, vado verso il <i>trackpad</i>. Domani dovrei invece pensare di averlo già a portata di mano e muovere solo le dita, anziché il braccio. Più o meno pratico? Lo si saprà appena arriva il <i>software</i> e con esso il <i>multitouch</i>. Per ora, a vederlo, viene voglia di comprarlo. Subito.