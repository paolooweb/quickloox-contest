---
title: "Due insicurezze e due misure"
date: 2007-02-16
draft: false
tags: ["ping"]
---

Se per caso il discorso cade sui virus, il so-tutto di turno fornisce la spiegazione infallibile: su Mac non ci sono virus perché i Mac <em>sono pochi</em>.

Poi capita di leggere dei <a href="http://exploit.blogosfere.it/2007/02/la-sicurezza-nei-siti-del-governo.html" target="_blank">buchi nei siti del Governo italiano</a>. Perché vengono <em>defacciati</em>? <cite>Perché sono una facile preda</cite>. Guarda un po', le piattaforme più attaccate non sono quelle più numerose, ma quelle più <em>emmenthaler</em>.