---
title: "Esperimenti di rete"
date: 2006-05-23
draft: false
tags: ["ping"]
---

Ho acceso la condivisione sull'iMac. Ho montato sul PowerBook il disco rigido dell'iMac, la home dell'utente e il Cd audio che c'era dentro l'iMac. Tutto da Finder.

Sul PowerBook ho aperto un paio di cartelle dal volume condiviso giusto per vedere che tutto funzionasse.

Poi ho staccato la spina all'iMac.

Ho atteso qualche istante. Poi, dal PowerBook, ho provato ad accedere a uno dei volumi condivisi. La rotella del Finder si &egrave; messa a girare.

Ho provato da Terminale a navigare nel filesystem del PowerBook. Nessun problema. Ho provato da Terminale ad aprire una cartella del Finder, sempre ovviamente relativa al PowerBook. La cartella si &egrave; aperta regolarmente e la rotella ha smesso di girare.

Ho riprovato ad accedere ai volumi di rete morti. Il Finder ha ripreso a fare girare la rotella.

Me ne sono disinteressato. Mi sono rimesso a lavorare alle mie cose.

Dopo una trentina di secondi, &egrave; arrivato il messaggio di sistema a segnalare che i volumi non rispondevano. C'era una voce "Disconnect All". L'ho scelta.

In un paio di secondi i volumi inesistenti si sono smontati e il Finder ha ripreso a lavorare regolarmente.

Quanto sopra l&rsquo;ho pubblicato anche sulla mailing list del Poc in risposta a un problema di crash di sistema. Magari per qualcuno &egrave; utile.