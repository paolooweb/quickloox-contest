---
title: "EpiPhonia"
date: 2008-03-23
draft: false
tags: ["ping"]
---

L'esibizionismo tecnologico non mi appartiene. Non più di tanto, almeno.

La gente mi ha visto con iPhone in mano solo perché arrivava una telefonata, o mentre lo spegnevo in spogliatoio.

Una manciata di giorni e in tre mi hanno chiesto se posso procurargliene uno (non posso, ma ne parlo agli amici e arriveranno). Altri due sono passati ufficialmente dall'indifferenza al dubbio.

Neanche con i Mac funzionava cos&#236;. E i Mac, nel 1984, erano la fine del mondo.

Buona Pasqua. :-)