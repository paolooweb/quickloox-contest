---
title: "Think Digital (lifestyle)"
date: 2002-01-15
draft: false
tags: ["ping"]
---

Quando una strategia si completa diventa molto facile accorgersi che esiste

Sto ascoltando Pascal Cagni, responsabile Apple per l’Europa, parlare della terza Età dell’oro dell’informatica; dopo Produttività e Internet, inizia il Digital LifeStyle, lo stile di vita digitale, in cui il computer diventa il centro delle attività professionali, didattiche e ricreative di ciascuno di noi.
E che cosa vogliamo? Soluzioni semplici, potenti, da usare subito.
iTunes non è il miglior programma al mondo per gestire Mp3, ma è una soluzione elegante che, magari con un iPod di contorno, è inarrivabile per semplicità ed efficacia.
iMovie è la stessa cosa, per l’editing video. Lo stesso vale per iDvd.
Ora c’è iPhoto: due clic e si crea un album fotografico, anche su Internet.
Si metta in mezzo a ciò un iMac, funzionale eppure splendido da mettere in casa, e si capisce al volo che cosa sia il Digital Lifestyle.
Se ci arrivassero anche i giornalisti... prima, però.

lux@mac.com