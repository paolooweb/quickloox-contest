---
title: "Apple ti ascolta"
date: 2008-01-25
draft: false
tags: ["ping"]
---

<em>La cosa migliore con la casa della Mela è spiegarle gentilmente che cosa vuoi. Funziona
</em>

Mi trovo, gentilmente invitato, nello spazio eventi di Business Press a Milano (Business Press cura le relazioni pubbliche di Apple in Italia) a vedere dal vivo le novità annunciate pochi giorni or sono da Steve Jobs alla Worldwide Developers Conference.

Ci sono meraviglie (Exposé da solo vale il prezzo del biglietto) ma a me ha colpito una cosa in particolare. Semplice, banale: in Mac OS X 10.3 file e cartelle possono avere una etichetta, un colore che le classifica per categorie.

Era una funzione presente in Mac OS classico, poi scomparsa in Mac OS X. Ora è tornata.
Magia? No. Chris Bourdon, Senior Product Marketing Manager di Apple, lo sta spiegando mentre scrivo: <cite>Abbiamo avuto un sacco di</cite> feedback <cite>dagli utenti, che rivolevano questa funzione, e li abbiamo ascoltati</cite>.

Non si ripeterà mai abbastanza: di' ad Apple che cosa vuoi. A Cupertino, e questo è solo l'ennesimo esempio, ascoltano.