---
title: "Sviluppo sostenibile"
date: 2006-03-09
draft: false
tags: ["ping"]
---

<strong>Alessio</strong> mi ha segnalato <a href="http://www.sunrisebrowser.com/en/" target="_blank">Sunrise</a>, browser basato sul motore open source di WebKit sviluppato da Apple e dotato di tutta una serie di funzioni che tornano decisamente utili a chi fa Html per mestiere, o per passione.

Alessio stesso <a href="http://www.mac-community.it/Blog/Centrale/mac-community_blogcentral.html" target="_blank">ne parla</a> più diffusamente sul suo blog.