---
title: "Gli stessi (luoghi comuni)"
date: 2006-02-28
draft: false
tags: ["ping"]
---

<cite>I Mac e i Pc ora sono uguali perché i componenti internamente sono gli stessi.</cite>

Hmmm. Proviamo a cambiare ambito.

<cite>Una frittata di mia suocera e una di Gualtiero Marchesi sono uguali perché i componenti internamente sono gli stessi.
</cite>
Non suona.

<cite>Una foto di Oliviero Toscani e una di Lucio Bragagnolo sono uguali perché i componenti internamente sono gli stessi.
</cite>
Non ci siamo proprio (nessuno mi paga, giustamente, per le mie foto).

<cite>Il mio PowerBook 17” e quello della persona che ho davanti ora sono uguali perché i componenti internamente sono gli stessi.
</cite>
Se è così, perché l’altro PowerBook è guasto e il mio (al momento) no?

<cite>Le persone sono tutte uguali perché i componenti internamente sono gli stessi.
</cite>
Spiacente, siamo tutti diversi e l’aspetto esteriore è solamente l’ultima delle diversità.

Forse la parola <em>uguale</em> ha un altro significato. Oppure sparare un luogo comune è più facile che pensare.