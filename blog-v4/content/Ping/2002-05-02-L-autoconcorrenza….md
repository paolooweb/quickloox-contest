---
title: "L’autoconcorrenza è l’anima della rovina"
date: 2002-05-02
draft: false
tags: ["ping"]
---

Destinata geneticamente al declino, Microsoft ha fatto un passo avanti verso l’inizio della fine

Qualunque monopolio alla fine si infrange contro sé stesso; e restare ancorati a modelli obsoleti di business come il software chiuso e proprietario sono due delle caratteristiche genetiche che un giorno provocheranno il declino di Microsoft.
Intanto però ci si può fare male anche da soli e la decisione di riorganizzare l’azienda potrebbe essere l’inizio della fine.
Sette unità di business, responsabili dei propri business e fatturati: a sentirla è una buona idea. Ma poi accade che le pressioni di budget affossino l’innovazione e la capacità di osare, e che si perda tempo a fare concorrenza ad altre unità di business anziché combattere sul mercato.
Perché dico questo? Perché già successo a molti altri, per esempio ad Apple, circa 1989. Anche in questo caso Microsoft riesce solo a copiare, in ritardo.

<link>Lucio Bragagnolo</link>lux@mac.com