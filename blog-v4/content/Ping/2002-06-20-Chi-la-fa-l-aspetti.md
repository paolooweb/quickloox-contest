---
title: "Chi la fa l’aspetti"
date: 2002-06-20
draft: false
tags: ["ping"]
---

Chi di gratis uccide, di gratis se la fa sotto

Microsoft ha presentato al Senato italiano (e contemporaneamente presso numerosi altri Parlamenti nel mondo) una relazione su quanto sia pericoloso il software open source per l’industria informatica. Nel software open source, è bene ricordare, ricade anche Darwin, il cuore di Mac OS X.
Che dire? Appena nato il Web, il browser si pagava. Poi è arrivata Microsoft e ha deciso che il browser lo regalava. Così Netscape, che il browser lo vendeva, finì presto nella polvere e venne infine acquistata da America On Line.
Che dire? Oggi il software si paga, ma si può decidere – grazie all’open source – di averlo gratis. Microsoft non vuole che si possa scegliere, forse perché tanta gente preferisce software gratis di buona qualità a software a pagamento di pessima qualità.
Non penso che Microsoft finirà nella polvere per merito dell’open source, ma sarebbe una bella nemesi storica: hai usato il software gratis per far fuori la concorrenza, e adesso il software gratis fa fuori te.

<link>Lucio Bragagnolo</link>lux@mac.com