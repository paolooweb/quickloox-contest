---
title: "Niente di nuovo sul fronte dell'update"
date: 2007-05-27
draft: false
tags: ["ping"]
---

Anche l'ultimo <a href="http://docs.info.apple.com/article.html?artnum=305530" target="_blank">Security Update</a> è passato, installato, nessun problema. Già che c'ero, sono passati anche 308 megabyte di aggiornamento di World of Warcraft e sono andati lisci pure loro (a parte che mi hanno tolto due comandi utili, ma non è colpa dell'update).

Ripensandoci, non ricordo neanche più se e quando ho avuto un riavvio indesiderato nel 2007. Comincia giugno. Mica male.