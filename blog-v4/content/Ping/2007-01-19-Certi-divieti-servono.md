---
title: "Certi divieti servono"
date: 2007-01-19
draft: false
tags: ["ping"]
---

Vietato riparare i privilegi. Assolutamente vietato riparare i privilegi. Tassativamente vietato riparare i privilegi. Rigorosamente vietato riparare i privilegi.

Spero di dissuadere almeno qualcuno. Non è una misura di manutenzione. Non va fatto per abitudine. Va fatto solo per risolvere problemi e solo se sai che effettivamente la riparazione dei privilegi potrebbe aiutare.

Peggio della riparazione dei privilegi c'è stata solo la ricostruzione della scrivania con Mac OS 9. Lo si faceva come si prende l'aspirina, cos&#236;, in qualunque situazione. Solo che la riparazione dei privilegi è molto più pesante.

Per spiegare devo avere abbastanza tempo per prepararmi a dovere. Nel frattempo, vai sulla fiducia. Non tutti i divieti sono dannosi.