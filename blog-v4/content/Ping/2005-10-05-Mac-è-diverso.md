---
title: "Mac è diverso<p>"
date: 2005-10-05
draft: false
tags: ["ping"]
---

Può servire in modi fantastici per persone comuni e per questo assai speciali<p>

Riporto le parole di <strong>Piergiovanni</strong>:<p>

<cite>In una delle mie tre prime liceo, dove faccio da coordinatore, c&rsquo;è un ragazzo di 20 anni, con gravi problemi dovuti a traumi neonatali, che gli hanno lasciato una semiparalisi, un ritardo psicofisico e un grave deficit nella sfera attentiva. È seguito da un pedagogista e da un&rsquo;insegnante di sostegno. Ho avuto l&rsquo;idea di creare lezioni speciali, molto semplici, fatte in modo da consentirgli di fare le stesse cose che fanno i suoi compagni. Ho così usato uno dei modelli di Pages, molto colorato, con una foto appropriata già presente nel modello [&hellip;]</cite><p>

Quel <em>Think Different</em> che girava qualche anno fa forse faceva riferimento un po&rsquo; anche a questo. Pages mi si è rivelato in pochi giorni programma ideale per il mio amico Jida, autore di uno splendido libro sull&rsquo;uso facile di Final Cut Pro, e per un ragazzo di vent&rsquo;anni, che non conosco ma è già un po&rsquo; mio amico, se Piergiovanni è d&rsquo;accordo. Siamo tutti diversi&hellip; e tutti uguali.<p>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>