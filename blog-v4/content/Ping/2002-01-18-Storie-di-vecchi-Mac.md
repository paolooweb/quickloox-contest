---
title: "Storie di vecchi Mac"
date: 2002-01-18
draft: false
tags: ["ping"]
---

Anche quando non serve più un Mac “obsoleto” continua a servire. Storie di vita vera

Power Mac 7600/120: regalato ad amico non abbiente che aveva bisogno di un computer per inserirsi nel mondo del lavoro.
Performa 6400/200: smontato per inserire Ram e disco su altri vecchi Mac che avevano bisogno di un potenziamento.
Performa 6200: ceduto gratis da una professionista a un parente che lo usa con profitto per la videoscrittura.
Power Mac 8100/80: diventato il secondo computer di casa, è stato portato nella seconda casa in modo che, quando si va al mare, ci si possa collegare anche da là.
Non sono computer miei, ma storie su cui potrei testimoniare di persona. Un Mac che a noi non ha più niente da dire può essere prezioso per altri.
Per non parlare di scuole, biblioteche, enti benefici... un Mac è una ricchezza anche da anziano. E molti anziani, con un Mac, tornano un po’ più giovani.

lux@mac.com