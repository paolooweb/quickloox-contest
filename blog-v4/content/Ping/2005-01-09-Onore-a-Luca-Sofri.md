---
title: "Onore a Luca Sofri<p>"
date: 2005-01-09
draft: false
tags: ["ping"]
---

Un giornalista che scrive di una cosa che capisce<p>

Su <a href="http://www.ilfoglio.it">Il Foglio</a> di ieri Luca Sofri ha realizzato un pezzo titolato <em>Orgoglio Apple</em>.<p>

Intanto ha scritto correttamente iPod, che nella stampa generalista sembra essere un piccolo pezzo di bravura, tra I-pod, Ipod e altre schifezze (e sì che basta guardare il sito Apple).<p>

Poi ha inserito solo un paio di piccole inesattezze, del tutto veniali, piccolissime in proporzione alla lunghezza del pezzo, che non ne alterano la validità delle tesi e della conclusione. Per l&rsquo;Italia, ramo informatica, da premio Oscar della carta stampata.<p>

Infine ha svolto un ragionamento corretto, basato su fatti concreti, condivisibile. Complimenti e fatelo scrivere di più. Di Apple, chiaro.<p>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>