---
title: "Mettere e togliere"
date: 2008-10-16
draft: false
tags: ["ping"]
---

Infuriano le polemiche sulla mancanza di FireWire 400 a bordo dei MacBook e di Blu-ray a bordo, come minimo, dei MacBook Pro.

Premesso che a proposito di FireWire 400 sospenderei il giudizio, perché esiste il ragionevole dubbio che sia problema ingegneristico e non decisione strategica (come già fu per il primo MacBook Pro), ci sono due modi di guardare alla dotazione di un Mac: il proprio bisogno personale e gli obiettivi di vendita di Apple.

Il secondo punto di vista non merita discussioni. Se togliere FireWire o tralasciare Blu-ray fa vendere più Mac ad Apple è una cosa buona per Apple, che ne godrà i frutti. Se ne fa vendere meno è una cosa cattiva per Apple, che ne pagherà le conseguenze e la prossima volta starà più attenta alla domanda.

Il proprio bisogno personale invece è tutt'altra cosa e diventa, essenzialmente, una variazione del costo di accesso alla tecnologia. FireWire, nei listini Apple, ora costa un po' di più e prima costava un po' meno. Altre tecnologie, per esempio i dischi a stato solido, ora costano meno e prima costavano meno. Ognuno di noi fa i propri conti e decide se l'offerta è interessante oppure no.

E come si dà un giudizio complessivo, fuori da queste categorie? Intanto bisognerebbe evitare di pensare che i propri bisogni coincidano con i bisogni collettivi: a farlo sono quelle persone originali, fuori dal coro, diverse da chiunque altro, che immancabilmente sanno quello che è meglio per milioni di persone. O sei unico, o sei come tutti, ma non ambedue.

Il mio criterio è <em>quanto hanno saputo togliere stavolta?</em>. Perché il design non è accumulare funzioni, ma togliere il non è essenziale. In questo caso hanno tolto viti e guarnizioni, il pulsante della <em>trackpad</em>, la FireWire più vecchia a favore di quella più performante e gli schermi opachi. Di primo acchito non sembra male. Vediamo che cosa succede ai 17&#8221;.