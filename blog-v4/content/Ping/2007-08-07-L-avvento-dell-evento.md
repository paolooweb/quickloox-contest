---
title: "L'avvento dell'evento"
date: 2007-08-07
draft: false
tags: ["ping"]
---

Non so quanto riuscirò a esserci per il tempo dell'evento stampa di stasera, ma una stanza iChat la si apre lo stesso: <em>apple5</em>. Si raggiunge da iChat con <strong>Comando-Maiuscole-G</strong> digitando <code>apple5</code>.

Si fanno due chiacchiere commentando in diretta i nuovi annunci Apple. Io li seguo da <a href="http://www.macosrumors.com/" target="_blank">MacOsRumors</a>, che non è roba di seconda mano scritta da incompetenti e non va in crisi di banda. :-)

L'evento comincia alle 19 italiane. A tra poco!