---
title: "C'è ancora vita su Redmond"
date: 2008-05-13
draft: false
tags: ["ping"]
---

Microsoft <a href="http://www.macworld.it/showPage.php?template=notizie&amp;id=14833">mi ha ascoltato</a>.

Se succede una cosa del genere, c'è da prepararsi veramente a tutto. Nel dubbio, ecco la guida di Wired a <a href="http://howto.wired.com/wiki/Survive_a_Zombie_Apocalypse" target="_blank">come sopravvivere a una invasione di zombi</a>.