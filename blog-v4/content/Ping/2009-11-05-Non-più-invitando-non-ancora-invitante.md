---
title: "Non più invitando, non ancora invitante"
date: 2009-11-05
draft: false
tags: ["ping"]
---

È arrivato il primo invito a <a href="http://wave.google.com/wave" target="_blank">Google Wave</a> e ringrazio ancora tantissimo. Ora devo capire come funziona il servizio e, per il momento, non vedo a disposizione inviti da elargire a mia volta. Appena le cose cambiano, mi adopererò. :-)