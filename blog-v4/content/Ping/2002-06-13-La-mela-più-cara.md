---
title: "La mela più cara"
date: 2002-06-13
draft: false
tags: ["ping"]
---

Chi spenderebbe quattordicimila dollari per un computer con il marchio Apple?

Molte volte si disputa dei prezzi dei computer; troppo cari, però stanno scendendo, ma il prezzo per avere un computer decente resta costante e così via.
Sappi allora che un tale Roger Wagner, sviluppatore di lunga navigazione per computer Apple di ogni risma nonché collezionista, ha vinto un’asta per un Apple I funzionante alla modica cifra del sottotitolo (circa quindicimila euro).
L’Apple I, quando è uscito a metà degli anni Settanta, era un computer avanzatissimo: bastava accenderlo, inserire manualmente uno a uno i codici esadecimali che costituivano il linguaggio Basic e lanciare il Basic stesso per poter scrivere tutti i programmi che potevano stare in otto chilobyte di memoria.
E pensare che oggi c’è gente che si lamenta del prezzo di un iMac.

<link>Lucio Bragagnolo</link>lux@mac.com