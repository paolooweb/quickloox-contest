---
title: "Le bugie hanno il link effimero"
date: 2009-07-15
draft: false
tags: ["ping"]
---

Scrivevo della <a href="http://www.macworld.it/blogs/ping/?p=2234" target="_self">campagna anti-App Store</a>, di cui si doveva annunciare a tutti i costi un rallentamento nel crescere del numero di applicazioni disponibili. Uno dei tirati in ballo era Philip Elmer-Dewitt di Apple 2.0, su Fortune.

Scriveva il 27 marzo 2009: <cite>il flusso di nuovi titoli sta rallentando e ci sono segni che l'infrastruttura di App Store stia logorandosi per stare al passo con quanto c'è già sopra.</cite>

Il 14 luglio 2009 lo stesso Elmer-Dewitt scrive: <cite>la logica suggerisce che questa crescita a rotta di collo non possa continuare all'infinito. Ma il ritmo dei</cite> download <cite>è decollato alla fine dell'anno scorso e, come suggerisce l'inclinazione della curva, non ha ancora rallentato.</cite>

Si parla degli scaricamenti e non del numero di applicazioni. Però, se l'infrastruttura di App Store fosse stata logora, non credo che si sarebbe arrivati cos&#236; rapidamente a 1,5 miliardi di <i>download</i>. Oggi le applicazioni sono 65 mila, più del doppio che a marzo.

La curva di <cite>crescita a rotta di collo</cite> <a href="http://apple20.blogs.fortune.cnn.com/2009/07/14/how-apples-app-store-got-to-1-5-billion-downloads/" target="_blank">è ben visibile sul blog</a> di Elmer-Dewitt.

Ma bisogna fare in fretta: per coincidenza, la pagina del rallentamento delle applicazioni <a href="http://apple20.blogs.fortune.cnn.com/2009/03/26/iphone-app-store-30000-apps-but-slowing/" target="_blank">è sparita</a>. Per ritrovarne il testo bisogna avvalersi di <a href="http://www.textually.org/textually/archives/2009/03/023131.htm" target="_blank">textually.org</a>.

Le bugie hanno le gambe corte anche sui server, a volte.