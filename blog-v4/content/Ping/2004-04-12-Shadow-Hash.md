---
title: "Password ombra"
date: 2004-04-12
draft: false
tags: ["ping"]
---

Per chi ha l’ossessione della sicurezza, ecco la sicurezza

C’era gente che gridava allo scandalo perché Mac OS X, essendo un sistema Unix, usava l’algoritmo base di Unix per codificare le password e quindi guardava solo ai primi otto caratteri della password, indipendentemente da quanto la password fosse lunga.

La stessa gente non ha mai provveduto a smontare il lettore di Cd dal loro Mac, attraverso il quale, inserito un disco di sistema di Mac OS X e riavviata la macchina, si puà cancellare qualsiasi password.

Ora saranno contenti: Mac OS X 10.3 usa il sistema Shadow Hash e le password possono essere di lunghezza arbitraria. Verranno codificate in tutta la loro lunghezza.

Speriamo che si mettano a smontarli, ora, quei maledetti lettori.

<link>Lucio Bragagnolo</link>lux@mac.com