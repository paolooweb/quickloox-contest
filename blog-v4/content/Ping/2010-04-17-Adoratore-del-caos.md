---
title: "Adoratore del caos"
date: 2010-04-17
draft: false
tags: ["ping"]
---

Arrivano i primi <a href="http://blogs.adobe.com/jnack/2010/04/photoshop_cs5_64-bit_benchmarks.html" target="_blank">test di Photoshop Cs5</a>, che è scritto a 64 bit. Sono decisamente lusinghieri per l'applicazione e, di conseguenza, per chi la userà.

Devo ricordare la <a href="http://www.pcworld.it/notizia/107066/2006-01-11/L-Opinione-Apple-Intel-Inside.html" target="_blank">profezia</a> di Maurizio Lazzaretti su PcWorld del 2006:

<cite>Da settembre [2006] nel mondo Windows tutte le CPU saranno a due o più </cite>core<cite> e a 64 bit, quindi la migrazione a Windows X64 sarà inevitabile anche se non velocissima. Stesso problema per Mac OS X, moltiplicato però per due per il cambio di architettura, quindi la scelta di Jobs spazia dal caos totale allo stop a 32 bit, perdendo però nuovamente la guerra delle prestazioni nella fascia professionale.</cite>

Oggi qualunque Mac fa funzionare le applicazioni a 32 bit insieme a quelle a 64 bit. Qualunque Windows funziona <i>solo</i> a 32 bit oppure <i>solo</i> a 64 bit. I Mac venduti sono molti di più di quattro anni fa e, ammesso che ci sia stata una <cite>guerra delle prestazioni nella fascia professionale</cite>, questo esodo di professionisti profughi verso Windows io non l'ho proprio visto.

Se questo il <cite>caos totale</cite>, mi piace.