---
title: "Risparmio condiviso"
date: 2009-07-11
draft: false
tags: ["ping"]
---

Anche oggi mi sono ricordato di tradurre una delle novità di Snow Leopard dalla <a href="http://www.apple.com/it/macosx/refinements/enhancements-refinements.html" target="_blank">pagina apposita</a> del sito Apple (che aspetta stupidamente a tradurre).

<b>Condivisione file più efficiente</b>

Con Snow Leopard e una base wireless AirPort Extreme o Time Capsule, un computer con funzioni di condivisione di file o media può andare in stop e continuare a condividere i propri file, risparmiando cos&#236; energia.

Su questo non hanno risparmiato genialità, però.