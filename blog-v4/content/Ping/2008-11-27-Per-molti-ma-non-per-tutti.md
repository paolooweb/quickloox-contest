---
title: "Per molti ma non per tutti"
date: 2008-11-27
draft: false
tags: ["ping"]
---

Al momento non frequento Facebook. Ero iscritto, poi Microsoft ne ha preso in gestione la pubblicità e mi sono tirato fuori.

Ciò non significa che Facebook sia inutile o dannoso, tutt'altro.

Nello specifico devo ringraziare pubblicamente <strong>Maurizio Codini</strong>, che mi ha gentilmente invitato a fare parte di un gruppo dove, parole sue, <cite>si parla e si discute di Mac con toni seri e soprattutto educati (Quelli che</cite> I'm a Mac<cite>)</cite>.

Se e quando tornerò su Facebook non mancherò di iscrivermi. Ad altri invece piacerà farlo subito e magari Maurizio avrà piacere di vedere il gruppo crescere.

Non ho altri dati e spero che bastino questi. Buone discussioni!