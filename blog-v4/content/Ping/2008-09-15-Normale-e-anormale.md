---
title: "Normale e anormale"
date: 2008-09-15
draft: false
tags: ["ping"]
---

Io difenderei Apple, dice.

Sarebbe verissimo se lui dichiarasse di attaccare Apple. Uno attacca, l'altro difende. Normalità.

Stranamente, no. Io difendo Apple. Lui, invece, <cite>è obiettivo</cite>.

Se è obiettivo lui, perché io no? Che cos'ho che non va?