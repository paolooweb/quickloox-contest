---
title: "Sentirsi maggioranza<p>"
date: 2004-10-14
draft: false
tags: ["ping"]
---

Quando i complessi di inferiorità si capovolgono<p>

Noi che usiamo Macintosh siamo una élite. Quindi siamo in pochi (al massimo una trentina di milioni). I meno accorti, invece che godersi il vantaggio competitivo, si rodono il fegato temendo chissà che cosa perché sai mai, metti che Apple domani abbia problemi&hellip;<p>

Beh, non succederà, almeno per un po&rsquo;. In accordo con i più recenti risultati finanziari, Apple ha passato gli ottocentomila Macintosh venduti nell&rsquo;utimi trimestre fiscale. Più sei percento rispetto all&rsquo;identico trimestre fiscale di un anno prima.<p>

Ma il bello è che Apple, sempre nel trimestre, ha venduto oltre <em>due milioni</em> di iPod. Più cinquecento e passa percento di aumento.<p>

Altro che élite. Se si parla di iPod, il mercato è di massa che più massa non si può. A essere minoranza per una volta sono gli altri, con tutti quegli aggeggi brutti, inefficienti e di poco valore.<p>

Apple non sparisce, tranquilli.<p>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>