---
title: "Essere imperfetto"
date: 2006-08-16
draft: false
tags: ["ping"]
---

Su questo blog non commenterò, mai, notizie sulla salute di altre persone, né ne farò oggetto di materiale da buttare in pagina per fare contento qualche inserzionista o cercare cento clic in più. Sorry, ma mi manca lo stile. Nessuno è perfetto.