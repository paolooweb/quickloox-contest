---
title: "Il signor Bean"
date: 2009-10-03
draft: false
tags: ["ping"]
---

Non credo che tutti gli interessati abbiano potuto leggere il <a href="http://www.macworld.it/blogs/ping/?p=2890&amp;cpage=1#comment-37324" target="_blank">commento di Stefano</a> sulla disponibilità del <i>word processor</i> <a href="http://www.bean-osx.com/Bean.html" target="_blank">Bean</a> localizzato in lingua italiana.

Adesso ha più risonanza e grazie a Stefano per avere condiviso l'informazione.