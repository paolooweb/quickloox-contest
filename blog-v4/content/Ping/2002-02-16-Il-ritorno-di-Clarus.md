---
title: "Il ritorno di Clarus"
date: 2002-02-16
draft: false
tags: ["ping"]
---

Anche se si cambia sistema operativo il buon vecchio cane-mucca non va trascurato

Il cane-mucca si chiamava Clarus e un po’ muggiva un po’ abbaiava, ossia faceva “Moof!”.
Il suo mestiere era mostrare l’effetto dei comandi impartiti all’interno della finestra di dialogo Page Setup (Imposta pagina, Imposta stampa...).
Una delle lacune di Mac OS X è avere accantonato Clarus, preferendogli una sagomina anonima che potrebbe essere veramente chiunque.
Fortunatamente uno dei pregi di Mac OS X è che sta provocando una autentica fioritura di shareware. E qualcuno ha pensato al <link>ritorno di Clarus</link>http://www.interealm.com/clarusx/.
Serve un po’ di <link>ripasso</link>http://developer.apple.com/products/techsupport/dogcow/history.html sulla storia di Clarus? C’è persino un <link>sito ufficiale</link>http://thor.he.net/~stories/view-moof/articles/mim.html.

<link>Lucio Bragagnolo</link>lux@mac.com