---
title: "C'è trippa per gatti"
date: 2006-10-03
draft: false
tags: ["ping"]
---

La chiusura del Cd di Macworld è stata difficile ma ce l'ho fatta.

Ho potuto accontentare veramente pochi: <strong>frix</strong> (cos&#236;, dopo essersi comprato ottobre, compra anche novembre!) e <strong>Giovanni Z</strong>.

La mia disorganizzazione è leggendaria, ma funziono meglio se mi si aiuta. Mi rendo conto che, se ho un suggerimento preciso e magari anche un link da seguire, mi muovo con molta più facilità. Non è un rimprovero ad alcuno, ma una constatazione dettata dall'esperienza.

Le grandi imprese, LaTeX NeoOffice e compagnia, sono in lavorazione. Dovrò sacrificare l'impostazione del Cd e convincere i responsabili che ne vale la pena, ma mi sto chiarendo le idee e sento che ce la si farà.

A parte ciò, qualcosa di bellino nel Cd c'è. Intanto c'è <a href="http://burn-osx.sourceforge.net/" target="_blank">Burn</a>, programmino di masterizzazione open source che non rivaleggia con Toast ma va ben oltre il Finder; c'è un simpatico quanto missconosciuto <a href="http://www.donsgames.com/index.php?page=sf1945" target="_blank">simulatore di volo</a> e combattimento aereo ambientato nel 1945; ho messo praticamente tutte le versioni possibili e immaginabili di <a href="http://audacity.sourceforge.net/" target="_blank">Audacity</a>, compresa una che forse ancora non esiste sul sito, più plugin vari. E un sacco di altra roba.

Gli sviluppatori, al 99 percento, sono di una gentilezza estrema. Ne ho trovati un paio con le pigne in testa e per il resto sono gente straordinaria. Li ringrazio anche da qui.