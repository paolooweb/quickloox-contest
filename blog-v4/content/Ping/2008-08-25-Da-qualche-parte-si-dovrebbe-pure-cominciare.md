---
title: "Da qualche parte si dovrebbe pure cominciare"
date: 2008-08-25
draft: false
tags: ["ping"]
---

È uscita <a href="https://addons.mozilla.org/en-US/firefox/addon/7115" target="_blank">YouTube Comment Snob</a>, estensione di Firefox che nasconde i commenti-spazzatura su YouTube in funzione di parametri come l'uso delle sole maiuscole o l'eccesso di punteggiatura.

Se fosse la prima avvisaglia di un sano movimento che portasse alla fine dell'esibizione dell'ignoranza come orgoglioso marchio distintivo&#8230;