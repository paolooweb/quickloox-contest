---
title: "Mirare all&rsquo;angolino<p>"
date: 2005-08-22
draft: false
tags: ["ping"]
---

Una modifica significativa all&rsquo;interfaccia di Mac OS X<p>

Porta il mouse nell&rsquo;angolo in alto a sinistra. Clicca. Scende il menu Apple.<p>

Porta il mouse sopra la mela del menu Apple e clicca.<p>

Il secondo compito è ben più difficile del primo. Il primo si fa anche senza guardare e non richiede alcuna precisione. È molto più facile.<p>

Non è da tantissimo che le cose stanno come adesso. Prima bisognava centrare la mela.<p>

A dire che Apple continua a lavorare all&rsquo;interfaccia di Mac OS X. Perfino quando è solo questione di un angolo.<p>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>