---
title: "Pdf Workflow, se sai come"
date: 2003-03-25
draft: false
tags: ["ping"]
---

Una funzione piuttosto interessante e piuttosto misconosciuta di Mac OS X

Hai già creato la cartella PDF Services dentro la tua Library di utente o quella generale, vero? (Non la Library dentro System; si toccano solo le Library con l’icona che mostra i libri sul faldone)

Non lo hai fatto? Non ti do torto; non è una cosa su cui l’informazione sia propriamente dilagata.

A questo punto dovrebbe seguire una trattazione lunga e articolata su Pdf Workflow, come funziona e che vantaggi offre, a chi ha aggiornato a Mac OS X 10.2.4, beninteso.

Ma questo è Ping e non l’Enciclopedia di Mac. Magari, un giorno, chissà.

<link>Lucio Bragagnolo</link>lux@mac.com