---
title: "Datemi un Mac mini e sollever&agrave; il livello dell&rsquo;istruzione nazionale"
date: 2006-06-12
draft: false
tags: ["ping"]
---

Su ilmac.net c&rsquo;&egrave; un <a href="http://www.ilmac.net/notizie/visualizza.php?id=4271" target="_blank">articolo esemplare</a> su quanto poco basti per fare molto nella scuola. L&rsquo;arretratezza si pu&ograve; superare con poco pi&ugrave; di un Mac mini.

Il <a href="http://www.ilmac.net/progettoscuola/Sito/Filmato.html" target="_blank">risultato</a> &egrave; deliziosamente fresco e ingenuo come ben si addice a bimbi di quinta elementare. E scalda il cuore, perch&eacute; quei bimbi stanno procedendo con una marcia in pi&ugrave;. Grazie <strong>Palmy</strong>!