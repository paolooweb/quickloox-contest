---
title: "Accentate ed Emacs<p>"
date: 2005-01-06
draft: false
tags: ["ping"]
---

Qualcuno aiuti un povero power user digiuno di Unix avanzato<p>

Come i bambini irresistibilmente attratti da fornelli, prese di corrente e porcellane cinesi, periodicamente mi casca l&rsquo;attenzione su Emacs.<p>

C&rsquo;è anche un motivo. Passando la giornata a scrivere, uno degli editor di testo più potenti che esistano non può non incuriosirmi. E ultimamente è uscita anche una <a href="http://home.att.ne.jp/alpha/z123/emacs-mac-e.html">versione Carbon</a>, con interfaccia grafica, per questo meno esoterica di quella già inclusa nel Terminale ma che a un inesperto di Unix come me fa un bel po&rsquo; gola.<p>

Ma c&rsquo;è una cosa che proprio non riesco a capire: Emacs, tutte le volte che ci ho provato, si è sempre rifiutato di accettare e visualizzare le mie accentate italiane. Me lo merito, d&rsquo;accordo. Ma qualcuno che abbia studiato, sa dirmi se esiste un modo?<p>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>