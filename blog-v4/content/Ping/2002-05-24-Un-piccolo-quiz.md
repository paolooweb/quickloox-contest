---
title: "Un piccolo quiz"
date: 2002-05-24
draft: false
tags: ["ping"]
---

Chi sa dire quale azienda produce effettivamente gli iMac (Flat Panel) di Apple?

Naturalmente non si vince niente se non la mia ammirazione per l’intraprendenza e la capacità di ricerca su Internet. Non è difficile e quindi c’è solo da divertirsi, magari imparando una piccola curiosità in più per stupire il collega o l’amica.
Se anche un solo lettore mi scriverà invocando la soluzione, la pubblicherò in uno dei prossimi Ping.

<link>Lucio Bragagnolo</link>lux@mac.com