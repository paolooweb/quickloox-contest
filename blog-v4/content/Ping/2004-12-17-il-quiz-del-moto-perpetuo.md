---
title: "Il Quiz del Moto Perpetuo<p>"
date: 2004-12-17
draft: false
tags: ["ping"]
---

Caro lettore, scopri con me come si alimentano le tastiere senza fili<p>

Nell&rsquo;Apple Center entra un signore, che curiosa un po&rsquo; in giro e si ferma a contemplare le tastiere Bluetooth. Poi fa <em>belle queste tastiere senza fili&hellip; ma come sono alimentate?</em><p>

Io avrei fornito una delle seguenti risposte:<p>

a) con il suo bel cavo di alimentazione;<br>
b) pannelli solari posizionati sul fondo dell&rsquo;unità;<br>
c) dinamo realizzate in nanotecnologia collegate a ciascun tasto;<br>
d) uranio arricchito, le importiamo dall&rsquo;Iran;<br>
e) volere è potere;<br>
f) nel suo quartiere verrà presto installato un gruppo elettrogeno;<br>
g) a rumore, bisogna cantare Bocelli intanto che si digita;<br>
h) è il nuovo modello con dentro gli gnomi;<br>
i) Apple ha appena brevettato il moto perpetuo.<p>

Tu che cosa avresti risposto?<p>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>