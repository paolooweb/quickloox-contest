---
title: "Minuto per minuto"
date: 2006-07-20
draft: false
tags: ["ping"]
---

Mancava una settimana alla pubblicazione dei risultati finanziari Apple dell'ultimo trimestre, pubblicati ieri sera, e qualcuno diffondeva notizie sulle difficoltà per il trimestre <em>prossimo</em>. A voler pensare male, in Italia, sarebbe roba da Consob.

Il giorno dopo, a sei giorni dalla pubblicazione dei risultati, qualcuno diffondeva voci su vendite basse dell'iPod. Non quelle che ci sono state; quelle che ci saranno nel prossimo trimestre. Su quello che è successo, niente; su quello che deve ancora accadere, invece, spreco di analisi. Prevedono il futuro per settembre ma non sanno che pesci pigliare da un mercoled&#236; a quello dopo.

Due giorni prima dei risultati, <cite>il mercato scommette su vendite e profitti deboli</cite>. Si parla sempre da qui a tre mesi; dei fatti concreti in arrivo nel giro di quarantott'ore, niente. È l'<em>informazione</em>, bellezza.

Sempre lo stesso giorno, <em>più ottimismo sul bilancio Apple</em>. Un colpo al cerchio e uno alla botte, domani c'è il sole, domani piove, comunque sia ci prendiamo. Se non altro, finalmente si parla di dati veri e vendite vere.

Il giorno dei <a href="http://www.apple.com/pr/library/2006/jul/19results.html" target="_blank">risultati</a>, beh, chiaro: <cite>la corsa dei profitti di Apple non si ferma</cite>. Letti i titoli di prima era ovvio, no?