---
title: "I puntini sulle virgole"
date: 2010-10-23
draft: false
tags: ["ping"]
---

Sognare è bello e anch'io mi lancio in frasi tipo <i>vorrei da Numbers i fogli di calcolo a diciotto dimensioni</i>.

Nella realtà le applicazioni che vincono nel tempo sono quelle che si affinano e si levigano. È sempre più difficile inventare funzioni realmente rivoluzionarie e sempre più frequente che si trascurino i dettagli. Cosa che mi infastidisce.

Nella fattispecie, l'aiuto di Numbers insiste nel indicare la virgola come separatore dei parametri (in questo caso specifico la funzione Datedif, che calcola la differenza di tempo tra due date). Perdo istanti stupidi, che sembrano secoli, prima di scoprire empiricamente che invece si ha da usare il punto e virgola.

Roba meno sconfortante di vedere un aggiornamento Adobe che conta il tempo in decimale (mancano <i>3,7 minuti</i>). Ma Adobe ha una attenzione ai dettagli tradizionalmente mediocre. Da Apple ci si aspetta meglio.