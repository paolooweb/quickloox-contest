---
title: "Critici alla carica, software che si scarica"
date: 2002-03-13
draft: false
tags: ["ping"]
---

I criticoni criticano e intanto i download crescono. Il digital hub forse è una buona strategia

Apple ha annunciato che iPhoto è stato scaricato dal sito Apple più di un milione di volte. Se anche venisse usata una copia scaricata ogni dieci sarebbe comunque una comunità di tutto rispetto.
Una bella risposta ai criticoni a tutti i costi, quelli che hanno valutato iPhoto per quello che loro volevano fosse e non per quello che è: un tool semplice per fare le cose bene e velocemente, nell’ottica del computer visto come centro della vita digitale di tutti noi. Come al solito, se c’è una verità è più facile che esca dai fatti più che dalle parole: iPhoto piace e viene usato.
Per i criticoni, nessun problema: almeno gli occhi rossi (per la rabbia), iPhoto li toglie.

<link>Lucio Bragagnolo</link>lux@mac.com