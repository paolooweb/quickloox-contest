---
title: "No More Partitions"
date: 2007-10-14
draft: false
tags: ["ping"]
---

Avevo in mano un disco esterno con un nuovo sistema operativo di cui non farò il nome. Il disco esterno è stato formattato su un G4 e l'installazione del sistema operativo è stata fatta su un G4. Il tutto funziona perfettamente su un G4, anzi due, collaudati.

Ho collegato il disco esterno a un iMac Intel nuovo modello e gli ho detto di fare il <em>boot</em> dal disco esterno.

Lo ha fatto.