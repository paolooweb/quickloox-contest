---
title: "Una risposta punto per punto"
date: 2008-02-14
draft: false
tags: ["ping"]
---

L'altezza totale dei caratteri, cioè il <em>corpo</em>, è espressa in punti tipografici. Un punto tipografico corrisponde a 0,376 millimetri. Un corpo 10, per esempio, è formalmente alto 0,376 x 10 = 3,76 millimetri.

Chi doveva sapere, sa. :-)