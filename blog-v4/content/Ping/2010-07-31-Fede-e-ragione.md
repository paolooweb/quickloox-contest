---
title: "Fede e ragione"
date: 2010-07-31
draft: false
tags: ["ping"]
---

Due docenti dell'università americana Texas A&#38;M hanno pubblicato <a href="http://nms.sagepub.com/content/early/2010/05/11/1461444810362204.abstrac" target="_blank">una ricerca</a> nella quale spiegano che Apple sarebbe diventata una sorta di religione, con devoti che ne seguono le indicazioni per questioni di, sostanzialmente, fede.

Ci sarebbe nei confronti di Apple una devozione di tipo religioso, che costituirebbe l'esempio, dicono gli autori, di una <i>religione implicita</i>.

Avrei due fatti molto laici e razionali da sottoporre: primo, <a href="http://maysbusiness.tamu.edu/index.php/dell-scholarship-recipients-named/" target="_blank">Dell elargisce borse di studio</a> a studenti di Texas A&#38;M. Secondo, Bradley R. Anderson, che ricopre la carica di Senior Vice President, Enterprise Product Group in Dell, fa parte dell'Advisory Council di Texas A&#38;M.

Scommetto che la sua visione teologica in materia è particolarmente ispirata.