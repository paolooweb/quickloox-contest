---
title: "Il lamentino del leopardo"
date: 2009-09-05
draft: false
tags: ["ping"]
---

Roba da poco: tuttavia il servizio che considera il testo selezionato in una applicazione e lo apre dentro una finestra di TextEdit non funziona più come in Leopard.

Se provo ad applicarlo su testo scritto con BBEdit e codificato in Unicode, sbaglia la codifica. Uffa.