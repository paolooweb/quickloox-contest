---
title: "Extreme, ma in locale"
date: 2003-01-21
draft: false
tags: ["ping"]
---

Quando e dove AirPort Extreme fa la differenza

Qualcuno vuole acquistare AirPort Extreme, una delle grandi novità presentate da Apple a inizio anno, per navigare più veloce su Internet.

È un illuso, a meno che non lavori una grande azienda con una grossa linea dedicata: le connessioni Internet a portata di singolo utente, in Italia, sono abbondantemente sotto le capacità del primo AirPort. Montare AirPort Extreme vuol dire solo mettere un tubo ancora più grande dove comunque scorre un rivolo d’acqua.

Nelle reti locali, invece, si passa da una banda wireless di 11 megabit per secondo a una di 54 megabit per secondo. Da fare. Subito.

<link>Lucio Bragagnolo</link>lux@mac.com