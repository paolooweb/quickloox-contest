---
title: "Scripting critico"
date: 2008-10-20
draft: false
tags: ["ping"]
---

È uscito <a href="http://python.org/download/releases/2.6/" target="_blank">Python 2.6</a> (e si approssima la 3.0).

Sul sito sviluppatori Apple spiegano <a href="http://developer.apple.com/mac/articles/scriptingautomation/cocoaappswithmacruby.html" target="_blank">come scrivere applicazioni Cocoa con Ruby</a>, che per cominciare è di una facilità sconcertante.

<a href="http://developer.apple.com/tools/dashcode/" target="_blank">DashCode</a> esiste e significa JavaScript.

<a href="http://www.apple.com/applescript" target="_blank">AppleScript</a> è un'istituzione.

Sento odore di massa critica. Tempo, più o meno, Snow Leopard, e succederà qualcosa, non necessariamente da Apple.