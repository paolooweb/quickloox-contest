---
title: "Il lavoro degli altri<p>"
date: 2005-01-28
draft: false
tags: ["ping"]
---

Della serie: fortuna che ci sono io a pensarci<p>

Sono tutti i bravi a raccontare che cosa presenterà Apple al Macworld Expo e a guardare il codice Html delle pagine del sito Apple in cerca di indicazioni dell&rsquo;arrivo dei PowerBook G5.<p>

Nel frattempo le azioni Apple sono arrivate al massimo storico di tutti i tempi, a quota 72,64 dollari. Oggi.<p>

Guardo Macity, guardo Tevac, guardo Applicando, guardo&hellip; Macworld. Non lo dice nessuno. Non sarà una grande notizia, ma diamine, si occupa spazio per cose ben più piccole.<p>

Se devo mettermi anche a dare le news dimmelo, che mi organizzo. Parafrasando Bennato, <em>io più di tanto non posso fare</em>.<p>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>