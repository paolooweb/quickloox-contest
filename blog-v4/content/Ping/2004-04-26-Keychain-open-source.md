---
title: "La sicurezza funziona se è in chiaro"
date: 2004-04-26
draft: false
tags: ["ping"]
---

A domanda rispondo: chiunque può studiare il Portachiavi di Apple

L’esperienza di anni e anni di open source ha insegnato che la cifiratura più efficace è quella che rende aperto e consultabile il proprio codice. In questo modo, se il codice è insicuro lo si viene a sapere subito, e se qualcuno nel mondo vuole migliorare il codice suddetto è libero di farlo.

Al contrario, la sicurezza ottenuta nasconendo il codice è fallace. I bachi diventano accessibili solo a chi è fortemente determinato a trovarli e tipicamente si tratta di persone poco raccomandabili, mentre gli utenti onesti (per esempio le aziende) non hanno le risorse necessarie per approfondire nel nome della loro sicurezza.

Mi hanno chiesto se il Portachiavi di Apple è open source o meno: <link>sì</link>http://developer.apple.com/darwin/projects/security/, lo è.

<link>Lucio Bragagnolo</link>lux@mac.com