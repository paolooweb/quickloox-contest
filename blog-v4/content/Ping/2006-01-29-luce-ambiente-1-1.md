---
title: "Luce-ambiente 1-1"
date: 2006-01-29
draft: false
tags: ["ping"]
---

Sar&ograve; presuntuoso, ma l&rsquo;adeguamento automatico della luminosit&agrave; dello schermo dei PowerBook alle variazioni dell&rsquo;illuminazione ambientale dovrebbe funzionare in modo leggermente diverso.
Se ho capito bene, mantiene costante il rapporto tra le due illuminazioni. E va bene sui valori medi: se la luce ambiente aumenta o diminuisce leggermente, l&rsquo;illuminazione dello schermo la segue e sta bene.
Sui valori estremi, per&ograve;, ha meno senso. Se la variazione &egrave; notevole (tipo accendere la luce in una stanza buia) lo schermo ha uno sbalzo ugualmente notevole, decisamente superiore all&rsquo;adeguamento effettivo richiesto dalla vista.
Per&ograve; faccio poco testo, dal momento che tendo a lavorare con la luminosit&agrave; al minimo. Visto che parliamo di sensibilit&agrave; personale, beh, parliamone.
