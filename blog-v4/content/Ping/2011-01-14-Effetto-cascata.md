---
title: "Effetto cascata"
date: 2011-01-14
draft: false
tags: ["ping"]
---

Apple non ha diramato dati particolari seguenti alla comunicazione del milione di download al primo giorno di attività di Mac App Store.

Empiricamente posso però testimoniare che il numero di comunicazioni su Internet riguardanti l'arrivo su Mac App Store di nuovi software era (ovviamente) zero prima che partisse lo Store. Immediatamente dopo era minimo. Una settimana dopo è un torrente. Nel mio lettore di feed Rss la variazione in quantità è evidentissima e inoppugnabile.

L'impressione è che la cosa stia iniziando a funzionare. Mica era cos&#236; scontato: per esempio Ping, il <i>social network</i> musicale inserito in iTunes, non dà particolari segni di vita e certo non rappresenta una iniziativa di quelle che sconvolgono il mercato.

Se Mac App Store si afferma davvero, voglio proprio vedere come provano a imitarlo. Vero che porta con sé problematiche ancora da risolvere; vero anche che potrebbe diventare un argomento di vendita formidabile, senza concorrenza paragonabile.