---
title: "Il bingo delle notizie"
date: 2010-09-27
draft: false
tags: ["ping"]
---

Leggo da Macity il 17 gennaio 2010 che Apple potrebbe <a href="http://www.macitynet.it/iphonia/articolo/iPhone_Apple_lascia_Google_per_Bing/aA41731" target="_blank">lasciare Google per Bing</a>. Una <i>clamorosa ipotesi</i>.

Leggo il 20 gennaio 2010 che Apple e Microsoft <a href="http://www.macitynet.it/macity/articolo/Apple_e_Microsoft_trattano_per_sostituire_Google_con_Bing_su_iPhone_/aA41799" target="_blank">trattano per sostituire Google con Bing su iPhone</a>. A me pareva di averlo già letto il 17 gennaio. Ci saranno forse informazioni nuove? No, uno era un analista che lo ha raccontato a BusinessWeek, l'altro è BusinessWeek che lo ha sentito dall'analista. Forse danno le notizie in replica, come i telegiornali della notte, mah.

Il 14 marzo 2010, <a href="http://www.macitynet.it/macity/articolo/Google_ed_Apple_sempre_meno_fratelli_sempre_pi_coltelli/aA42727" target="_blank">Google e Apple sempre meno fratelli sempre più coltelli,</a> <cite>Cupertino che sta esplorando alternative tra cui quella, che sarebbe clamorosa, di un abbraccio con Microsoft per usare Bing, il motore di ricerca di Redmond, in iPhone.</cite>

Leggo da <a href="http://www.macitynet.it/macity/articolo/Bing-e-iPhone-OS-matrimonio-in-vista/aA44039" target="_blank">Macity del 29 maggio 2010</a>: <cite>le possibilità che Bing sostituisca Google come motore di ricerca principale su iPhone, iPod Touch e iPad si fanno sempre più concrete. Non sul breve periodo, comunque: le stesse fonti ci tengono a precisare che, sebbene le trattative tra Microsoft e Apple sembrano proseguire a vele spiegate, Google resterà il punto di riferimento per le ricerche su iPhone ancora per un po' di tempo.</cite>

Cioè s&#236;, però anche no; sta già succedendo, ma non ancora; possibilità sempre più concrete, ma tempi sempre più nebbiosi. Sembra di leggere la vecchia battuta della ragazzina precoce e sprovveduta i cui familiari, per minimizzare, raccontavano che s&#236;, era incinta, ma solo un po'.

Leggo il 24 settembre su Business Insider che <a href="http://www.businessinsider.com/google-iphon-search-deal-2010-9" target="_blank">Google estende il suo accordo con Apple</a>. <cite>Sembra che le speculazioni su Bing come motore di ricerca predefinito su iOS fossero fuffa</cite>, scrive Business Insider.

Macity <a href="http://www.macitynet.it/macity/articolo/Google-continuer-ad-essere-il-motore-di-ricerca-su-prodotti-Apple/aA46286" target="_blank">se ne accorge</a>. Evitando accuratamente di citare nell'articolo tutta la fuffa che ha scritto nei mesi passati.

Corrono voci che potrebbe uscire il quarantadue sulla tombola&#8230; oh, beh, è uscito il trentasette. Noi diamo le notizie.