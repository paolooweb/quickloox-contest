---
title: "Uno, due e centomila"
date: 2009-12-20
draft: false
tags: ["ping"]
---

Ad alimentare il <a href="http://www.macworld.it/blogs/ping/?p=3175" target="_self">dibattito</a> vivace e piacevole in corso sugli <i>e-book</i> e sui lettori, segnalo il parere di Andy Inhatko, giornalista Mac e tecnologico di lunghissima navigazione.

Veloce sunto dal suo <a href="http://www.suntimes.com/technology/ihnatko/1948278,ihnatko-ebook-nook-kindle-itouch-121909.article" target="_blank">articolo</a>, di cui raccomando la lettura, in cui indica tre lettori di e-book ideali per un regalo natalizio dell'ultimo momento:

<cite>Reader Pocket Edition di Sony</cite>

<cite>[Legge molti formati e, al contrario di altri lettori, sta in una tasca.]</cite>

<cite>Kindle 2 di Amazon</cite>

<cite>[Una soluzione con tutti gli ingredienti giusti, compreso un </cite>browser<cite>.]</cite>

<cite>iPod touch di Apple</cite>

Ma iPod touch non è un lettore di <i>e-book</i>! <cite>esclama uno</cite>. È un lettore di musica! E un lettore video! e una <i>console</i> per giocare! E un apparecchio con wi-fi dotato di <i>client</i> per la posta elettronica e di un <i>browser</i> degno di un <i>desktop</i>! E un parco software di&#8230; centomila applicazioni&#8230;

<cite>Questo è l'istante in cui sulla vostra faccia si disegna una espressione pensosa.</cite>

<cite>Non è un lettore di</cite> e-book <cite>dedicato, ma ospita diverse applicazioni per farlo. [&#8230;] Certo, consuma più di un lettore di</cite> e-book <cite>e molti lo eviteranno per il suo piccolo schermo. È un fattore che, ammetto, ha bloccato anche me all'inizio. Ma è bastato poco perché diventasse uno dei vari sistemi con cui leggo libri. E l'interfaccia spesso supplisce alla scarsità di spazio.</cite>

Il punto non è che iPod touch sia meglio o peggio. Invece è: quali sono i compromessi migliori? Per qualcuno sono un iPod touch, per altri un Kindle, per qualcuno un libro di carta come una volta. Bisognerà vedere quale compromesso attrae di più e chi riesce a schierare il <i>marketing</i> più efficace. Il che non sarà automaticamente un bene: ai tempi delle videocassette, vinse il formato peggiore dei tre.