---
title: "Scriptaro allo sbaraglio<p>"
date: 2005-09-07
draft: false
tags: ["ping"]
---

I miei risultati orgogliosamente imbarazzanti con AppleScript<p>

Una cosa che mi serve spesso per una serie di ragioni è avere a disposizione un numero scelto a caso. Di programmi ce ne sono a bizzeffe, però non ho voglia di lanciare un programma per avere un numero casuale, e collegarsi a Random.org è esagerato. Allora mi sono impegnato e ho partorito lo sgorbietto che segue:<p>

<code>display dialog "Limite superiore?" default answer "</code><br>
<code>set pluto to the text returned of result</code><br>
<code>set pippo to (random number from 1 to pluto) as integer</code><br>
<code>display dialog "Il numero è " & pippo & "." buttons {"OK"} default button 1</code><p>

Sono uno scriptaro. Per diventare uno scriptista dovrò impegnarmi per mesi, con costanza e buona volontà. Sono orgoglioso ugualmente però.<p>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>