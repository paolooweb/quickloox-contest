---
title: "Assente (giustificato)"
date: 2007-01-08
draft: false
tags: ["ping"]
---

Mi scuso molto, ma - paradossalmente per un impegno Mac - non potrò seguire il <a href="http://www.macrumorslive.com/" target="_blank">keynote in diretta</a>.

Tornerò a casa verso sera, intorno alle 22-22:30. Appena riesco apro comunque una stanzetta di iChat, di nome <code>expolatenight</code>. Comando-Maiuscolo-G da iChat (scegliere Aim).

i giochi saranno già finiti e suppongo anche la voglia di chiacchierare, ma se trovo qualcuno, beh, qualcosina avrò da fare. Inganniamo il tempo e molliamo qualche commento dell'ultim'ora, no, di quella dopo ancora.