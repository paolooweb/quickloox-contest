---
title: "Trasmettere emozioni<p>"
date: 2005-01-20
draft: false
tags: ["ping"]
---

Non ci sono parole da aggiungere a un commento che dice tutto<p>

Scrive Daniele Savi:<p>

<em>Non so come faccia Apple&hellip; davvero. Ho sempre ritenuto i player mp3 &ldquo;a chiavetta&rdquo; un&rsquo;inutile spesa, principalmente perchè hanno una memoria limitata. Ho sempre pensato che la cosa migliore fosse acquistare un bell&rsquo;iPod da 40 Gb, sul quale traslocare tutta la musica che ho su vari cd e hard disk.</em><p>

<em>Qualche giorno fa è uscito iPod shuffle&hellip; la prima chiavetta USB targata Apple. Ieri sera, l&rsquo;ho ordinata da AppleStore.</em><p>

<em>Scegliamo gli oggetti per le emozioni che sanno trasmetterci. Apple questo lo sa fare.</em><p>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>