---
title: "Contronatura<p>"
date: 2005-09-17
draft: false
tags: ["ping"]
---

Ma tecnicamente possibili, certe connessioni<p>

<em>Nuntio vobis magnum</em> non-so-ancora: ho usato con successo un Motorola V547 come modem connesso al Mac via Bluetooth per collegarmi a Internet. Funziona perfettamente e la banda varia secondo la cella in cui ci si trova, ma si gioca tranquillamente a World of Warcraft. Per una connessione senza pretese di velocità è un test eccellente.<p>

La configurazione non è difficile, ma bizzarra. Ci sono passi fondamentali da compiere che non hanno alcuna ragione tecnica (per esempio il fatto che in Internet Connect la connessione debba non essere quella principale, ma una &ldquo;altra&rdquo;). Però con l&rsquo;aiuto di un paio di forum ci si arriva in breve tempo.<p>

L&rsquo;insieme, mi ripeto, è diabolico. È una connessione contronatura. Però ammetto che è libertà in più. A qualcuno fa piacere, a qualcuno fa comodo, a qualcuno risolve un problema, a qualcuno serve, a qualcuno surroga la virilità. Meglio che ci sia più libertà, in attesa che il collegamento portatile a Internet diventi una cosa adeguata a ciò che dovrebbe essere davvero.<p>

Io sono nel gruppo risolve-un-problema, per la cronaca.<p>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>