---
title: "Semplicità"
date: 2011-01-19
draft: false
tags: ["ping"]
---

La prossima volta che sentirò qualcuno spiegarmi che Mac OS X è meglio di Windows o Windows è meglio di Linux o Linux è meglio di Mac OS X o tutti sono meglio di nessuno lo inviterò a commentare questa fantastica <a href="http://www.makelinux.net/kernel_map" target="_blank">mappa interattiva del kernel di Linux</a>.

Giusto per enfatizzare quel tanto la differenza che c'è tra il vivere una esperienza di utilizzo positiva o negativa e trasformare un'impressione personale in una valutazione tecnica.