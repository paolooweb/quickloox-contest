---
title: "C'è sempre una prima volta"
date: 2007-05-09
draft: false
tags: ["ping"]
---

Non sapevo il significato di una parola dentro una pagina web. Menu contestuale, guarda-nel-dizionario, ecco il significato.

Mai fatto prima. Comodo.

Al prossimo giro: provare l'autocompletamento delle parole via tasto Esc in un programma Cocoa. So che funziona, ma non l'ho proprio mai provato.

Giusto per non lasciare niente di intentato prima che arrivi Leopard. :-)