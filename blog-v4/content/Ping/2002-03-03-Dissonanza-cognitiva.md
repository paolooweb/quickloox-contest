---
title: "Un nuovo concetto"
date: 2002-03-03
draft: false
tags: ["ping"]
---

Certe reazioni di Windowsisti di fronte a Macintosh hanno una spiegazione. Psicologica

“Hai sentito il tale? Così equilibrato, così attento, è andato a sbattere con la macchina”. “Eh, avrà bevuto”.
È un esempio - da spazio minimo - di risoluzione di dissonanza cognitiva. Quando due nozioni che possediamo sono in conflitto (così attento, eppure è andato a sbattere) il comportamento tipico, inconscio, di quasi tutti è tentare di annullare o ridurre la dissonanza (se ha bevuto, è più plausibile che abbia sbattuto).
Pensa a un utente Windows. C’è un Mac che gli piace molto... ma lui è utente Windows. Allora dice che gli utenti Mac sono fanatici, o che sono computer per pochi, o che costano troppo eccetera. Cerca di ridurre la dissonanza.
La prossima volta che senti parlare di Mac un utente Windows, ricorda che ha bisogno di compensare le sue dissonanze. Insomma, chi usa Windows e sotto sotto invidia Mac va un po’ aiutato.

<link>Lucio Bragagnolo</link>lux@mac.com