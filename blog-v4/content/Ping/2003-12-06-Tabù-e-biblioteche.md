---
title: "Biblioteche e tabù"
date: 2003-12-06
draft: false
tags: ["ping"]
---

Di tanti posti dove si fa cultura-Mac mi permetto di segnalarne uno conflittuale

Mac, negli anni, ha fatto storia e cultura, differentemente da altre piattaforme, che hanno fatto solo cassoni. Per questo amo tutte le occasioni in cui il dialogo intorno alla piattaforma riesce occasionalmente a staccarsi da come-faccio-a-far-funzionare-il-cellulare-con-iSync oppure qual-è-il-miglior-programma-per-fare-due-più-due.

Sembra che fare cultura Mac sia un tabù contemporaneo; e allora beccati la biblioteca del Poc. Sta presso la sede di Mac@Work e raccoglie libri da chunque voglia prestarli a termine, prestarli a tempo indefinito o regalarli. Piano piano stanno crescendo gli scaffali e ci sono sia libri nuovi e interessanti, sia libri vecchi e ancora più interessanti, che aiutano a capire chi siamo, come siamo arrivati fino a qui e dove stiamo andando, relativamente all’informatica.

Ho un conflitto di interessi nel segnalare Mac@Work, di cui sono socio, e me frego. Fregatene anche tu e segnalami tutti i posti in cui si fa cultura Mac. Li pubblicherò tutti.

<link>Lucio Bragagnolo</link>lux@mac.com