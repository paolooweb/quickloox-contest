---
title: "Cinquanta dollari di Mac experience"
date: 2006-12-09
draft: false
tags: ["ping"]
---

Sempre relativamente al trasferimento di grossi file tra computer remoti, sto provando <a href="http://www.fivespeed.com/iget/" target="_blank">iGet</a> e mi piace proprio molto.

È un vero programma Mac (leggi: risolve lui i problemi e tu devi solo divertirti a usarlo) e costa cinquanta dollari, ma ho sempre più il sospetto che li valga.