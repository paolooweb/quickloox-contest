---
title: "Universal Nuevos<p>"
date: 2005-08-21
draft: false
tags: ["ping"]
---

Un altro programma su cui potrai contare<p>

Nuevos è un programma che visualizza una sola piccolissima finestra, configurabile per effettuare ricerche su Internet con qualsiasi motore di ricerca si voglia.<p>

È rinato a mo&rsquo; di fenice dalle ceneri di una utility precedente e ora è in beta pubblica. Ma la cosa importante è che è già stato realizzato in formato Universal Binary. Vale a dire che sarà sicuramente funzionante quando arriveranno i Mac con processore Intel, da meta 2006 in poi.<p>

Più <a href="http://wootest.net/nu/">Nuevos</a> di così si muore!<p>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>