---
title: "(Dis)interesse privato"
date: 2008-09-15
draft: false
tags: ["ping"]
---

Un <a href="http://www.macworld.it/blogs/ping/?p=1955&amp;cp=1#comment-34495" target="_blank">commento di Fabrizio</a> mi ha fatto capire una cosa importante e per questo lo ringrazio.

Dieci anni fa, Apple costruiva computer. Se usavi Apple, usavi un computer. Il resto erano <em>marginalia</em>.

Oggi Apple costruisce computer, lettori musicali, <em>pseudopalmari</em> come li chiama Fabrizio e non solo; vende musica, vende software amatoriale e professionale, vende video, fa il distributore per gli sviluppatori mobile, vende sistemi di condivisione di disco per grandi sistemi, vende server, vende lettori musicali, vende servizi web, vende Apple Tv qualsiasi cosa sia e chissà che altro.

Oggi è altissima la probabilità che Apple venda qualcosa che non mi interessa, che non uso e i cui sviluppi mi sono al più indifferenti, quando non mi infastidiscono.

Dieci anni fa, quando Apple faceva qualcosa, nove volte su dieci mi riguardava più o meno direttamente. Oggi la percentuale può essere molto più bassa. Si è alzata, scommetto proporzionalmente la percentuale di persone infastidite da cose che Apple fa e che giustamente non la interessano.

Inevitabile, perché siamo stati tutti abituati a una Apple che in un certo senso lavorava sempre e solo per noi. Non è più cos&#236;.