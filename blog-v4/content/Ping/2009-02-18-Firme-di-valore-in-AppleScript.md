---
title: "Firme di valore in AppleScript"
date: 2009-02-18
draft: false
tags: ["ping"]
---

Ho sempre avuto un debole per le <em>signature</em> in posta elettronica (le firme al termine di un messaggio) e spesso mi piacciono casuali, cioè scelte dal programma tra un archivio a sua disposizione.

Il mio programma di posta preferito da oltre dieci anni, Mailsmith di Bare Bones, gestisce facilmente le <em>signature</em> casuali. Ma il problema è che ne gestisce un insieme solo e vorrei di più. La ragione è che certi contesti vogliono certe <em>signature</em> e altre no. Quando scrivo a indirizzi americani, per esempio, preferisco che le <em>signature</em> siano in inglese, mentre sulle mailing list italiane prediligo cose in italiano. Ma certe <em>signature</em> a sfondo politico, per esempio, sono accettabili solo in certi ambiti mentre in altri sono fuori luogo e via discorrendo. Insomma, voglio più insiemi di <em>signature</em> casuali, per scegliere quello appropriato di volta in volta.

Mailsmith non lo fa&#8230; ma è perfettamente scriptabile. E realizzare script-archivio contenenti le <em>signature</em> appartenenti a un certo insieme è piuttosto semplice.

Mi sono messo alla tastiera e, come l'aspirante musicista che si cimenta in <em>Fra Martino campanaro</em>, ho composto il mio sgrammaticato capolavoro:

<code>set sigdash to "-- 
"</code>
<code>set quotes to {"prima signature", "seconda signature", "terza signature", "quarta signature", "eccetera"}</code>
<code>set chosen to (some item of quotes)</code>
<code>set signature to sigdash &#38; chosen</code>
<code>return signature</code>

Il <em>sigdash</em> è la codifica canonica delle <em>signature</em> usata originalmente nei <em>newsgroup</em> e che sarebbe bene tenere anche nella posta elettronica. È formato da trattino/trattino/spazio (s&#236;, c'è uno spazio bianco dopo i due trattini). Siccome voglio che il testo della <em>signature</em> inizi nella riga sotto il <em>sigdash</em>, nella definizione di quest'ultimo sono andato a capo, come testimoniano le virgolette.

La seconda istruzione definisce una lista di <em>signature</em>. Se le <em>signature</em> sono impaginate, ossia vanno su più righe, contengono a capi e cos&#236; via, meglio prima comporle ordinatamente in un editor di testo come <a href="http://www.barebones.com/products/TextWrangler" target="_blank">TextWrangler</a> e poi incollarle dentro lo script. AppleScript non si preoccupa se vanno su più righe.

La terza istruzione sceglie a caso uno degli elementi della lista, cioè una delle signature. <code>some item</code> è sintassi bellissima di AppleScript per dire <em>prendi a caso</em>.

La quarta istruzione riunisce in una sola variabile il <em>sigdash</em> e la <em>signature</em> scelta.

L'ultima istruzione non fa che inviare la <em>signature</em> dal motore interno di AppleScript all'esterno, verso il programma che la userà.

Il più è fatto. Posso creare più script con la stessa struttura e insiemi diversi di <em>signature</em> e poi scegliere di messaggio in messaggio l'insieme da cui pescare.

Non so come il tuo programma di posta potrebbe accettare l'idea di fare partire uno script al momento di generare la signature. Mailsmith è persino banale: si inserisce nel suo sistema di <em>signature</em> un comando come

<code>#system osascript ~/Library/Scripts/EducatedUptime.scpt#</code>

che lancia semplicemente lo script situato sul percorso indicato. Per ogni script si fornisce a Mailsmith una di queste finte <em>signature</em>, che in realtà eseguono lo script desiderato. Per Mail o per Entourage non saprei che dire se non rimandare a Google o a qualcuno che legge e ha voglia di contribuire alla conoscenza collettiva.