---
title: "Grazie (e anche senza)"
date: 2009-07-06
draft: false
tags: ["ping"]
---

Non è un grazie ad Apple, che neanche oggi ha tradotto la pagina sulle <a href="http://www.apple.com/it/macosx/refinements/enhancements-refinements.html" target="_blank">novità di Snow Leopard</a>, ma l'argomento della novità di oggi.

<b>Nuovi font</b>

Snow Leopard contiene quattro nuovi font: Menlo: un nuovo font monospaziato da usare nelle applicazioni come il Terminale; Chalduster; Heiti (giapponese, coreano, cinese semplificato e cinese tradizionale) e Hiragino Sans (cinese semplificato).

Quattro font sono pochi, per giunta suddivisi tra sistemi diversi. Sono però sempre meglio di zero. Non ci sono mai abbastanza font.