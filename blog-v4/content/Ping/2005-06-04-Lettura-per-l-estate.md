---
title: "Lettura per l&rsquo;estate<p>"
date: 2005-06-04
draft: false
tags: ["ping"]
---

Vale la pena leggere solo quando ci si diverte imparando<p>

Il mito del libro sotto l&rsquo;ombrellone, o letto su un prato di montagna, o al bordo della piscina, prospera. Facciamo finta che sia vero, con una differenza. Il libro che sto per consigliare è obbligatorio.<p>

È <cite>Da Terenzio a Internet - La tecnologia raccontata a un profano</cite> (edizioni Unicopli) di <a href="http://www.accomazzi.it">Luca Accomazzi</a>, alias MisterAkko, presenza ultradecennale su Macworld e di cui non ho spazio a sufficienza per raccontare le gesta (sul suo sito c&rsquo;è tutto).<p>

<cite>Da Terenzio a Internet</cite> è un libro da leggere per una ragione specifica: fa imparare un sacco di cose senza la minima fatica e alla fine uno si è pure divertito. Il contrario di quei tomi che si trovano normalmente in libreria, più noiosi del manuale che intendono sostituire.<p>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>