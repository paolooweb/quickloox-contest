---
title: "L'occasione di una vita"
date: 2007-12-23
draft: false
tags: ["ping"]
---

Riservata a chi voglia veramente capire Apple e il mondo Mac: <a href="http://www.macjournals.com/" target="_blank">Gcsf</a> ha riaperto, per breve tempo, gli abbonamenti a vita a Mdj e Mwj.

Mdj e Mwj sono newsletter a pagamento e un abbonamento a vita costa 995 dollari. È quello che ho fatto io nel 1997 (erano 800 dollari, mi pare, e il dollaro valeva oltre le duemila lire). Altrimenti ci sono abbonamenti mensili e annuali.

Le informazioni sono dettagliate allo spasimo, approfondite come nessun altro al mondo sa o può fare. Niente cazzate di quattordicesima mano mal tradotte in italiano sui <em>rumor</em> o sui borborigmi dei sedicenti analisti; invece, analisi dettagliate su che cosa c'è veramente dentro, per esempio, gli aggiornamenti di sicurezza, o trascrizione capillare della audioconferenza con cui Apple annuncia i risultati finanziari. In inglese ed è inevitabile, perché le notizie nascono l&#236;.

C'è un rischio intrinseco nella cosa. L'editore ha avuto problemi di salute non comuni e nell'ultimo anno la comunicazione è stata largamente inferiore, in quantità, agli impegni presi. Ma ora pare che le cose si stiano risolvendo. Dieci anni fa l'ho corso (erano dei nessuno, potevano sparire un mese dopo) e mi è andata bene. Io lo correrei, ma bisogna avere un interesse specifico.

Se sei indeciso, scrivimi e ti mando un numero da leggere, cos&#236; ti fai l'idea. Se hai già deciso, vai sul sito Gcsf e scrivi a loro. Potrebbe essere il bivio decisivo verso il sapere <em>veramente</em> come vanno le cose. :-)