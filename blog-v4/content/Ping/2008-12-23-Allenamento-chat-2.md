---
title: "Allenamento chat / 2"
date: 2008-12-23
draft: false
tags: ["ping"]
---

Ci troviamo, chi vuole, per commentare in <em>chat</em> il <em>keynote</em> Apple a Macworld Expo, marted&#236; 6 gennaio prossimo (ci sarà da commentare non poco, visto che è l'ultimo, visto che lo tiene Schiller&#8230;). L'anno scorso abbiamo saturato iChat, cos&#236; quest'anno la stanza su iChat ci sarà, ma servirà da buttadentro invitare la gente su Irc, la <em>chat</em> libera di Internet.

Fin qua tutto già detto. Nel primo <em>post</em> abbiamo fatto il nome di <a href="http://www.macworld.it/ping/soft/2008/12/06/allenamento-chat-1/" target="_blank">qualche programma per <em>chattare</em> in Irc</a>, per lo più gratuito.

Fase due. La <em>chat</em> sarà ospitata nel canale <em>#freesmug</em>, normalmente dedicato al software <em>open source</em> per Mac e animato dal fantastico <a href="http://freesmug.org" target="_blank">Gand</a>.

Una volta recuperato il programma, si chiede il collegamento al server <code>irc.freenode.net</code>. Bisogna fornire un nickname iniziale che non esista già e si è dentro. A questo punto si dà il comando

/join #freesmug

(tutti i comandi Irc cominciano per <em>slash</em>).

Il prefisso <em>#</em> indica un canale Irc, l'equivalente di una stanza di iChat. <em>#freesmug</em> è il nome del canale. <code>/join</code> sta per <em>entra in</em>.

Altri comandi utili: <code>/nick</code> per cambiare il nickname in corsa, <code>/leave</code> per uscire dal canale, <code>/quit</code> per uscire dal server.

Nei prossimi giorni mi farò trovare il più spesso possibile su <em>#freesmug</em> per accogliere chi fa collaudi. Se qualcuno ha difficoltà, mi aspetto una mail oppure un commento da queste parti. Farò del mio meglio per aiutare tutti, a patto di conoscere nome del programma usato, procedura seguita e problema incontrato.

Scrivere non riesco a collegarmi è semplice e molto semplicemente impedisce di essere aiutati. Invece avere precisione e dettaglio è la chiave per avere anche una soluzione. :-)