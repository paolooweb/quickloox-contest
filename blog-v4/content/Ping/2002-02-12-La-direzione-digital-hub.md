---
title: "La direzione del digital hub"
date: 2002-02-12
draft: false
tags: ["ping"]
---

Come a scuola: meglio il metodo induttivo o quello deduttivo?

Interessante notare le differenze nella concezione dell’informatica da parte di Compaq e Apple. Per Compaq il Pc va nella direzione di un dispositivo abbastanza piccolo da tenere in tasca e capace di collegarsi alla rete praticamente dovunque. Per Apple Il Pc resta il Pc ma diventa il punto di riferimento che rende più utili e più semplici gli apparecchi tascabili specializzati, come iPod o una videocamera.
Uno è un gigante che fabbrica computer brutti, l’altro è un gigante che punta sull’armonia tra forma e funzione. Uno è in crisi e Hewlett-Packard vuole mangiarselo, uno ha passato con buoni risultati un trimestre per tutti terribile di contrazione dei consumi.
Uno va dal particolare al generale (induttivo), l’altro va dal generale al particolare (deduttivo).
Uhm.

<link>Lucio Bragagnolo</link>lux@mac.com