---
title: "Una alla volta"
date: 2007-09-20
draft: false
tags: ["ping"]
---

Con colpevole ritardo mi associo a <strong>Elio</strong> nel festeggiare:

<cite>L'8 settembre 2007, oltre che per il primo V-Day italiano, verrà ricordato come il giorno della Liberazione&#8230; del mio iBook. :-)</cite>

<cite>Ho avviato, alle 22.24 ora di Belluno, il programma Rimozione Office: mai software di Redmond è stato più azzeccato!</cite>

<cite>Ora mi godo NeoOffice, Keynote, Pages, TextEdit&#8230;</cite>

<cite>Quanto ci sentiamo leggeri, il mio Mac ed io.</cite>

Lo capisco, Elio. A me pesava persino PowerPoint Reader, l'ultimo programma Microsoft che mi è toccato sopportare.

Com'è che diceva quella frase? <cite>Cambiare il mondo. Una persona alla volta</cite>.