---
title: "Chi non conosce il passato<p>"
date: 2005-09-25
draft: false
tags: ["ping"]
---

Chi tira fuori un concorrente di iPod shuffle riesce solo a fare pena<p>

Dell ha annunciato Dj Ditty, un concorrente di iPod shuffle. John Gruber su Daring Fireball ne ha già parlato meglio di chiunque altro.<p>

La sostanza. Per trovarlo bisogna cercarlo seriamente nel sito Dell, altrimenti non salta fuori. Se vai sul sito Apple, ti sparano iPod nano in prima pagina. Un indizio di quanto ci creda Apple e di quanto ci creda Dell.<p>

Forse è perché, se uno trova il player di Dell, vede come è fatto. Rinvio alla <a href="http://daringfireball.net/2005/09/ditty">foto su Daring Fireball</a> perché vale un articolo.<p>

Chi non conosce il passato è condannato a ripeterlo. iPod vince perché è semplice, immediato e comodo, nonché integrato con iTunes Music Store. Poi perché è scandalosamente bello.<p>

Il coso di Dell è&hellip; ecco, fatto da Dell. Uno dei loro argomenti di vendita: se lo carichi con musica compressa a metà qualità, contiene il doppio delle canzoni.<p>

Mai visto scrittori che propagandano il loro libro con slogan tipo si legge male, ma è lungo il doppio.<p>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>