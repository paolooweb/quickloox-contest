---
title: "Non far&ograve; nomi"
date: 2006-05-10
draft: false
tags: ["ping"]
---

A parte che, dando ascolto ai soliti, marted&igrave; dovevano esserci gli &ldquo;iBook nuovi&rdquo;. Ma ieri sera mi occupavo, per tutt&rsquo;altri motivi, della notizia data da un noto sito italiano: un <em>cracker</em> sabota i tabelloni elettronici di una ferrovia canadese e fa ripetere loro la frase <cite>Stephen Harper</cite> (il premier neoeletto) <cite>mangia i bambini</cite>.

Il sito italiano parla di tabelloni elettronici utili ai passeggeri in transito nonch&eacute; al personale ferroviario, che forniscono informazioni sui treni in arrivo e partenza. Quelli, per capirci, che il passeggero vede entrando in stazione, collegati a un importante sistema informativo. <em>Wow</em>. Una bella impresa.

Poi leggi la <a href="http://www.thestar.com/NASApp/cs/ContentServer?pagename=thestar/Layout/Article_Type1&amp;c=Article&amp;cid=1146520226985&amp;call_pageid=968332188492&amp;col=968793972154&amp;t=TS_Home" target="_blank">notizia originale</a> e scopri che si tratta di display a testo scorrevole, installati sulle singole carrozze, che mandano avvisi pubblicitari, annunciano la stazione in arrivo ed eventualmente mostrano aggiornamenti sul servizio. Li vediamo anche noi, in tanti negozi e anche sui treni delle Ferrovie Nord Milano. Il cracker aveva un telecomando a raggi infrarossi che si acquista per due soldi al supermercato. Attraverso l&rsquo;interfaccia ai raggi infrarossi gli bastava stare a due metri dal display, stupidamente non protetto da password, per riprogrammarlo. Bah. una bravata. Non certo quello che ha raccontato il sito.

Che cosa manca? Il rispetto per chi legge? La conoscenza della lingua? La deontologia professionale? Mah.