---
title: "L’angelo e i dettagli, seconda"
date: 2002-04-18
draft: false
tags: ["ping"]
---

Il nuovo iMac è buono anche dentro

Scusa l’insistenza sul tema, ma sono entusiasta del nuovo iMac (Flat Panel). Sì, va veloce, sì, funziona bene, sì, è silenziosissimo. Ma queste cose si leggono in qualunque recensione. Invece, non si legge da nessuna parte che nella confezione c’è un panno per la pulizia dello schermo Lcd; il connettore dell’alimentazione di rete è elegantissimo e la sua spina scompare completamente nell’unità-panettone; aprendo il fondo si scopre che la logica è coperta da un foglio semitrasparente che lascia esposti solo il connettore per la Ram e quello per la scheda AirPort. Sul foglio è disegnata la sagoma che occuperanno le espansioni e persino la curvatura del cavetto da collegare per l’attivazione di AirPort.
Insomma: dettagli, dettagli, dettagli. Dici che sono altre le cose che contano? Sarà; ma uno si sente un re.
Domenticavo: Mac OS X ha riconosciuto tutte le mie periferiche.

<link>Lucio Bragagnolo</link>lux@mac.com