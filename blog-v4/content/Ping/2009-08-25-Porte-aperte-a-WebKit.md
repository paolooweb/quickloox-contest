---
title: "Porte aperte per WebKit"
date: 2009-08-25
draft: false
tags: ["ping"]
---

Si è accennato nei giorni scorsi alla supposta chiusura del software Apple, che sarebbe proprietario.

Research in Motion, in pratica BlackBerry, ha annunciato l’acquisizione di Torch Mobile.

<a href="http://www.torchmobile.com/" target="_blank">Torch Mobile</a> è una piccola azienda canadese di Toronto, nota a chi se ne occupa perché ha scritto un browser per Windows Mobile che si chiama Iris. Iris funziona anche su Qtopia, piattaforma di sviluppo Linux.

Iris è fondato su WebKit e l’annuncio significa di fatto che WebKit arriverà anche su BlackBerry.

Al momento WebKit viene usato da Google per <a href="http://build.chromium.org/buildbot/snapshots/sub-rel-mac/" target="_blank">Chrome</a>, da Nokia per S60, appunto da Torch Mobile per Iris su Windows Mobile, da Omni Group per <a href="http://www.omnigroup.com/applications/omniweb/" target="_blank">OmniWeb</a> e da vari altri, per altri browser minori.

WebKit è stato creato da Apple a partire dall’<i>open source</i> Khtml. WebKit è <i>open source</i> e aziona anche Safari, ma non solo; numerosi programmi di Mac OS X, Mail primo fra tutti ma anche TextEdit e in generale tutto il software che tratta Html, si appoggiano a WebKit.

<a href="http://webkit.org/" target="_blank">WebKit</a> è scaricabile gratis da chiunque, tutti ne possono vedere e scaricare <a href="http://webkit.org/building/checkout.html" target="_blank">il codice sorgente</a> ed è persino possibile scaricare i <a href="http://nightly.webkit.org/" target="_blank">nightly builds</a>, praticamente quello che combinano i programmatori tra una versione ufficiale di Safari e quella successiva.

Dove siano la chiusura e la proprietarietà sfugge. Di converso, qualcuno mi mostra dove scaricare liberamente e gratis il codice sorgente di Internet Explorer?