---
title: "Il bello del nuovo modo di fare le cose male"
date: 2002-03-01
draft: false
tags: ["ping"]
---

Niente spiegazioni per un aggiornamento? Almeno si può vedere che cosa è cambiato

Al momento in cui scrivo Mac OS X 10.1.3 è l’ottavo aggiornamento software in undici mesi di vita di Mac OS X. E Apple ancora non si degna di spiegare bene e chiaramente a tutti che cosa fa e perché un nuovo aggiornamento. Con Mac OS 9 sarebbe stato molto più difficile.
Se non altro, da Terminale si può avere l’elenco dei file cambiati; giusto per farsi una vaga idea di dove sono avvenute migliorie.
Il comando da dare è "lsbom /Library/Receipts/MacOSXUpdate10.1.3.pkg/Contents/Resources/MacOSXUpdate10.1.3.bom".
Nonostante il comando sia da Terminale, l’elenco è veramente... interminabile.

<link>Lucio Bragagnolo</link>lux@mac.com