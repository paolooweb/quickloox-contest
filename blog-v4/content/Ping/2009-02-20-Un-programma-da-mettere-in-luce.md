---
title: "Un programma da mettere in luce"
date: 2009-02-20
draft: false
tags: ["ping"]
---

<a href="http://www.teleart.it/sito/home.html" target="_blank">Andrea Ack</a> e il sottoscritto hanno fatto gara a chi ce l'aveva più iPhone. Ha vinto lui con <a href="http://itunes.apple.com/WebObjects/MZStore.woa/wa/viewSoftware?id=295755987&amp;mt=8" target="_blank">iEasyCamera</a>, eccellente sostituto del software da fotocamera standard.

Per soli 79 centesimi lavora molto meglio in condizioni di luce difficili, facilita gli autoscatti e offre un sistema ingegnoso che limita il rischio di ottenere una foto mossa.

Preparerò la rivincita. :-)