---
title: "E io che Paris Metro 09"
date: 2009-07-16
draft: false
tags: ["ping"]
---

Ho raccontato giorni fa della mia <a href="http://www.macworld.it/blogs/ping/?p=2569" target="_self">improvvisata parigina</a> e di come avessi usato Paris Metro 09 per orientarmi nella loro plurisecolare metropolitana (nei commenti al post sono citati anche altri programmi).

Adesso è il momento di vedere <a href="http://www.youtube.com/watch?v=cH6r2tIaRXU" target="_blank">come si sono organizzati a New York</a>. Sono quasi caduto dalla sedia.