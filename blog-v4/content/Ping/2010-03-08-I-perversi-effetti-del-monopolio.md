---
title: "I perversi effetti del monopolio"
date: 2010-03-08
draft: false
tags: ["ping"]
---

Leggo sul New York Times un articolo intitolato <a href="http://www.nytimes.com/2010/03/08/technology/08pandora.html" target="_blank">Come Pandora è scampata allo smantellamento</a>. Tre brevi stralci:

<cite>Per molti dei suoi dieci anni di vita, Pandora è stata sul punto di morire, in lotta per trovare investitori e battagliare con le case discografiche per le</cite> royalty<cite>.</cite>

<cite>Alla fine del 2009, Pandora ha registrato il suo primo trimestre di profitti e cinquanta milioni di dollari di fatturato annuo. Quest'anno il fatturato sarà probabilmente di cento milioni di dollari.</cite>

Che cosa è cambiato?

<cite>Nel 2008 Pandora ha realizzato una applicazione iPhone che consente alle persone di sentire musica in</cite> streaming<cite>. Quasi immediatamente, 35 mila nuovi utenti al giorno si sono iscritti a Pandora dai propri cellulari, a raddoppiare il numero delle adesioni (in totale 48 milioni).</cite>

Dedicato alle teste vuote che cianciano del <i>monopolio di iTunes</i>. Pandora non fa affluire nelle tasche di Apple neanche un centesimo e a tutti gli effetti potrebbe essere vista come concorrente.

I monopoli sono tutt'altro. Per esempio, in Italia è impossibile ascoltare <a href="http://www.pandora.com" target="_self">Pandora</a>. Provare per credere e garantisco che Apple non c'entra.