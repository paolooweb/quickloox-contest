---
title: "Dalle tavole della legge di iDisk/2"
date: 2006-02-03
draft: false
tags: ["ping"]
---

Lavora quando gli americani dormono. Spesso iDisk &egrave; pi&ugrave; reattivo quando in Nordamerica &egrave; notte e, presumibilmente, una quantit&agrave; di abbonati consistente &egrave; a dormire invece che a usare .mac.