---
title: "Linee di confine"
date: 2007-09-07
draft: false
tags: ["ping"]
---

Ripensando alle polemiche intorno a Second Life, esistono da molti anni molti mondi alternativi. Quelli vecchi sono testuali, quelli (relativamente) nuovi sono spesso grafici.

Ai mondi grafici, ovviamente, non può passare neanche per l'anticamera del cervello di riciclarsi in mondi testuali.

È interessante però che nessuno dei mondi testuali abbia mai compiuto movimenti verso il mondo della grafica.

Con una eccezione. <a href="http://www.bat.org/bat.php" target="_blank">BatMud</a>, nato nel 1990, ha iniziato a sviluppare un client Java fortemente grafico.

L'unico mondo virtuale che stia sulla linea di confine anziché prendere posizione in uno dei due campi. Magari è perfino bello da sperimentare.