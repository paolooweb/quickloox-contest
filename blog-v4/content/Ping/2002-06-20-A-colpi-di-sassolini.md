---
title: "A colpi di sassolini"
date: 2002-06-20
draft: false
tags: ["ping"]
---

È in atto una guerra per il controllo dei media su Internet e anche noi ci siamo dentro

Apple ha fatto causa a Sorenson, la società che produce forse il miglior codec per la compressione dei filmati QuickTime. Il problema è che Sorenson, dopo avere firmato un accordo con Apple per applicare il codec in questione solo a QuickTime, ora sta collaborando con Macromedia per creare una tecnologia che possa fare gestire video a Flash senza bisogno di QuickTime, RealVideo o Windows Media che dir si voglia.
Non è un episodio isolato. Macromedia e Adobe guerreggiano da tempo intorno ai formati vettoriali come Flash e lo Scalable Vector Graphics di Adobe, raccomandato dal World Wide Web Consortium ma ignorato dal resto del mondo.
È in atto una guerra senza quartiere per il controllo del Web attraverso i formati di file che vi passano sopra. Da una parte è molto bello per noi consumatori poter scegliere; dall’altra parte, quando lanciamo un player di qualcosa, mettiamo un sassolino sul piatto della bilancia a favore di quel player lì.
Sono sassolini infinitesimali; ma ognuno ha il loro peso.

<link>Lucio Bragagnolo</link>lux@mac.com