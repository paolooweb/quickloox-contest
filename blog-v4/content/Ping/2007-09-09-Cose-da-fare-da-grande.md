---
title: "Cose da fare da grande"
date: 2007-09-09
draft: false
tags: ["ping"]
---

Il bello dei buoni propositi è che ogni tre mesi si possono rinnovare. Una volta è il ritorno dalle vacanze, poi c'è Natale, poi c'è l'estate, poi c'è ancora qualcos'altro.

Il buon proposito che ho in mente stasera è fare maggiore conoscenza di <a href="http://www.macforge.com/" target="_blank">MacForge</a>. 46 mila e più progetti software open source che funzionano su Mac, o che dovrebbero funzionare su Mac senza troppa fatica. Diversamente da SourceForge che contiene tutto e il suo contrario, qui la visione dell'open source è Mac-centrica.

Solo per l'intelligenza artificiale vedo 1.076 progetti (per una volta non ho contato i giochi, visto?). 1.438 front-end per database. 1.263 progetti attorno all'email. Ribadisco: è un sottoinsieme. I programmi totali a portata di Mac sono ancora di più.