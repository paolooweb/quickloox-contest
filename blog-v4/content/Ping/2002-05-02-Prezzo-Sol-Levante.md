---
title: "Prezzo Sol-Levante"
date: 2002-05-02
draft: false
tags: ["ping"]
---

Qualcuno si è lamentato degli ultimi aumenti di prezzo nel listino Apple. In verità...

Cattiva sorpresa per molti, scoprire giorni fa che i prezzi di iMac o di un lussuoso Titanium sono saliti, invece di scendere come siamo (troppo?) bene abituati.
Qualcuno ha già puntato il dito contro Apple e la sua sete di denaro.
La verità però potrebbe essere altra, più legata agli aumenti di prezzo di componenti come Ram o schermi Lcd. Apple – come qualsiasi azienda – non aumenta i prezzi appena può, bensì fissa un margine ideale di profitto su ogni macchina e poi agisce in modo da riuscire a rispettarlo. E se la Ram costa di più, i prezzi dei Mac possono anche aumentare. O diminuire, in caso contrario.
Un indizio: in Giappone Sony, Nec e Ibm hanno recentemente applicato aumenti medi di prezzi tra 90 e 165 euro per macchina, a causa delle ragioni suddette.
Forse Apple, ancora una volta, non fa che arrivare prima degli altri.

<link>Lucio Bragagnolo</link>lux@mac.com