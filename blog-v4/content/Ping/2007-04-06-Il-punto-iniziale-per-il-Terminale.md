---
title: "Il punto iniziale per il Terminale"
date: 2007-04-06
draft: false
tags: ["ping"]
---

Se avessi un sito per tutte le volte che ho sentito qualcuno chiedere un buon manuale per imparare a usare il Terminale, sarei più visto di YouTube.

Non so se sia un buon manuale, ma io partirei (e partirò) da <a href="http://developer.apple.com/documentation/OpenSource/Conceptual/ShellScripting/index.html" target="_blank">questa pagina</a> del sito Apple per gli sviluppatori.

S&#236;, il documento è in inglese. Se è un problema la lingua, inutile applicarsi sul Terminale. Come <a href="http://www.unix-stuff.de/unix-is-sexy_klein.gif" target="_blank">fare sesso con Unix</a> in italiano? :-)