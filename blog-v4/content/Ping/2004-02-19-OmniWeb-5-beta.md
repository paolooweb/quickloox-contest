---
title: "Nuova vita per un vecchio browser"
date: 2004-02-19
draft: false
tags: ["ping"]
---

OmniWeb potrebbe essere la sorpresa del 2004 e mettere in imbarazzo persino Safari

Con Panther è accaduta una cosa interessante: Apple ha aperto completamente il motore Html di Safari agli sviluppatori indipendenti.

Così <link>OmniWeb</link>http://www.omnigroup.com/applications/omniweb/ che aveva un’ottima interfaccia utente ma un pessimo motore, ha potuto cestinare il suo motore, prendere quello di Safari e concentrare tutti gli sforzi sull’interfaccia.

Mentre scrivo si succedono le beta della versione 5 di OmniWeb ed è già possibile vedere novità che nessun altro browser ha, neppure Safari. Compreso un sistema per avere più pagine nella stessa finestra diverso – e migliore – rispetto alle mortifere linguette a cui siamo stati tristemente abituati.

Certe funzioni di OmniWeb 5 potrebbero seriamente indurmi a usarlo come browser predefinito. E non sono pulsantini più graziosi degli altri o qualche simpatica animazione.

<link>Lucio Bragagnolo</link>lux@mac.com