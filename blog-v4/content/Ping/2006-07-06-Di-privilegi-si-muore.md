---
title: "Di privilegi si muore"
date: 2006-07-06
draft: false
tags: ["ping"]
---

Virtualmente. Se ti sei adirato almeno una volta per una questione di permessi su disco o di file che non si lasciano cancellare, ci sono interfacce grafiche per terminare file o processi nel modo più cruento possibile. <a href="http://www.forchheimer.se/bfm/" target="_blank">Brutal File Manager</a> mette a disposizione cinque armi diverse e i file colpiti sanguinano, <a href="http://psdoom.sourceforge.net/" target="_blank">psDoom</a> non ha bisogno di altre presentazioni oltre al nome, come del resto <a href="http://www.cs.unm.edu/~dlchao/flake/doom/" target="_blank">Doom SysAdmin Tool</a>.

Attenzione, che hanno dei <em>safe mode</em>, ma sono effettivamente gestori di file e di processi. Le cose si possono cancellare davvero, anche se l'ambiente è quello del gioco. A giocare con le armi si rischia sempre. Però se c'è, come al solito, molto da imparare.