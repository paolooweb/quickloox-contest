---
title: "Il massacro di Santo Stefano"
date: 2009-12-11
draft: false
tags: ["ping"]
---

Terminata l'orgia dell'apertura dei regali e giunto il momento di passare a qualcosa di veramente divertente, si potrebbe giocare a qualcosa con i pargoli, i nipoti, i figli dei vicini, i papà dei figli dei vicini, i parenti in età mentale ancora considerabile e qualsiasi altra combinazione.

Se vogliono roba forte, può essere che per allora sia definitivo <a href="http://www.bloodfrontier.com/" target="_blank">Blood Frontier</a>, ennesimo sparatutto in prima persona, individuale e <i>multiplayer</i>.

Il gioco ha appena annunciato la <i>beta</i> 2, che è stabile e giocabile già ora. Il motore è il collaudato Cube Engine 2 (quello di <a href="http://sauerbraten.org/" target="_blank">Sauerbraten</a> e di <a href="http://cube.sourceforge.net/" target="_blank">Cube</a>), ma la grafica è stata rivoltata come due calzini, non uno.

L'insieme, per chi ama il genere, è divertente e c'è meno violenza ostentata rispetto ad altre produzioni. Il prezzo? Un affarone.