---
title: "Da cosa nasce casa"
date: 2008-02-06
draft: false
tags: ["ping"]
---

Ricevo da <strong>Pierfausto</strong>:

<cite>Sto risistemando casa&#8230; la necessità è quella di riuscire a farsi un'idea di quello che verrà fuori prima di buttare giù muri, chiudere porte, comprare mobili</cite> et similia.

<cite>Cos&#236; sono andato alla ricerca di un software che mi consentisse di tracciare i muri con le loro misure e avesse una visualizzazione 3D.</cite>

<cite>In commercio ce ne sono diversi che promettono co(a)se meravigliose, io invece ho trovato un programmino che è meraviglioso: <a href="http://sweethome3d.sourceforge.net/" target="_blank">Sweet Home 3D</a>.</cite>

<cite>Ovviamente open source, distribuito con licenza Gpl, localizzato in Italiano, comprensivo di guida. Sfrutta Java come tecnologia e questo lo rende multipiattaforma.</cite>

<cite>Dentro il programma si trovano diversi accessori e mobili in 3D e se ne possono scaricare centinaia da Internet.</cite>

<cite>Ma soprattutto è  f a c i l e   d a   u s a r e.</cite>

<cite>Basta armarsi di metro per prendere le misure reali e disegnare i muri sullo spazio dedicato al 2D. Istantaneamente viene creato il disegno in 3D.</cite>

<cite>Il tempo necessario per avere la propria abitazione in 3D è direttamente proporzionale alle sue dimensioni; tutto il resto lo si impiega per arredare, spostare, colorare, modificare.</cite>

<cite>Al geometra porterò il Pdf, alla mia compagna farò vedere le nuove idee direttamente in 3D, io (in proporzione) ho perso più tempo a cercare, scaricare, installare altri programmi per il Cad che disegnare la piantina della mia abitazione&#8230;</cite>

Il che dimostra come molto spesso la ricerca del programma potente sia inutile. Molto meglio quella del programma facile, come si cerca di spiegare quotidianamente a chi vuole togliere gli occhi rossi dalle foto e vuole Photoshop, quando basta iPhoto.