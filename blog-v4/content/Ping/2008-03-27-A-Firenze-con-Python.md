---
title: "A Firenze con Python"
date: 2008-03-27
draft: false
tags: ["ping"]
---

Qui parliamo di AppleScript, ma è giusto essere amici di tutti gli scripter e, in quest'occasione, dei seguaci di Python.

Venerd&#236; 9 maggio, sabato 10 e domenica 11 maggio <a href="http://www.pycon.it/" target="_blank">si tiene PyCon Due</a>, seconda conferenza italiana dedicata appunto Python, con <em>keynote</em> di Richard Stallman a Palazzo Vecchio e poi manifestazione vera e propria al Viva Hotel Laurus.

Il sito è in italiano ed è chiarissimo, quindi rimando l&#236; tutti gli interessati. Èd è una cosa tutt'altro che lontana da Mac; basta guardare nella cartella Applicazioni oppure scrivere <code>python</code> dentro il Terminale.

Poi ci sono <a href="http://appscript.sourceforge.net/" target="_blank">appscript</a>, una pagina del sito Python <a href="http://wiki.python.org/moin/MacPython/AppleScript" target="_blank">dedicata ad AppleScript</a> e articoli <a href="http://www.oreilly.com/pub/a/mac/2007/05/08/using-python-and-applescript-to-get-the-most-out-of-your-mac.html" target="_blank">come questo</a>. Insomma, si va sempre più sul complicato, ma il problema è quello della voglia, non quello della possibilità.