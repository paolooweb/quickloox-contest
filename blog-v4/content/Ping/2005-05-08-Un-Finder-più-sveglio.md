---
title: "Un Finder più sveglio<p>"
date: 2005-05-08
draft: false
tags: ["ping"]
---

Adesso si rende conto di stare seduto sopra un sistema Unix<p>

Prima di Tiger accadeva spesso che un cambiamento apportato al sistema attraverso il Terminale passasse inosservato al Finder, anche per alcuni minuti o indefinitamente, salvo logout.<p>

Adesso il Finder è molto più all&rsquo;erta e si accorge sempre e subito di che cosa hai combinato lavorando nella shell. Apparentemente, un file cambiato assume prima l&rsquo;aspetto di un&rsquo;icona generica, mentre in pochi istanti il Finder controlla le informazioni a sua disposizione e assegna un&rsquo;icona specifica.<p>

C&rsquo;è da meravigliarsi che il sistema lavori tanto più di prima, su questa e mille altre cose, eppure Tiger sia più veloce di Panther, anche sulle macchine più vecchie.<p>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>