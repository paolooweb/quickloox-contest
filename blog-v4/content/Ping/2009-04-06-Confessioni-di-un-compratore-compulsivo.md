---
title: "Co.Co.Co."
date: 2009-04-06
draft: false
tags: ["ping"]
---

Confessioni di un compratore compulsivo.

A un giorno dalla scadenza, ho ceduto e ho acquistato <a href="http://www.macheist.com" target="_blank">MacHeist 3</a>.

Da un po' pensavo a Multiwinia, il gioco di strategia online di Ambrosia; posso regalare Cro-Mag Rally alla nipotina (e anche Pop-Pop se trovo un amico che aderisca all'offerta), voglio provare World of Goo, è probabile che WireTap Studio mi serva nel prossimo futuro, ho dato dieci dollari in beneficenza, rischio di ritrovarmi The Hit List già pagato e poi c'è un'altra decina di programmi.

Il valore aggiunto di tutta l'operazione è ampiamente superiore ai trentanove dollari di cui all'oggetto.