---
title: "Tutto in programma"
date: 2007-11-04
draft: false
tags: ["ping"]
---

Prima che sia capace di valutarne la consistenza effettiva e metterci mano passerà del tempo, ma pare che il supporto di <a href="http://www.cmlenz.net/blog/2007/10/python-on-leopa.html" target="_blank">Python</a> e <a href="http://trac.macosforge.org/projects/ruby/wiki/WhatsNewInLeopard" target="_blank">Ruby</a> in Leopard abbia compiuto importanti passi avanti.

Non è roba da utente casuale eppure è molto meno difficile di quanto può apparire a prima vista. Un po' di curiosità potrebbe portare a grandi soddisfazioni.