---
title: "Bello pensare open source"
date: 2006-02-13
draft: false
tags: ["ping"]
---

Ho parlato settimana scorsa del <a href="http://www.sourcesense.com/contest/" target="_blank">concorso lanciato da Sourcesense</a>, neonata software house specializzata in open source, che ha lanciato un concorso per chi voglia creare il logo della società e ha messo in palio un MacBook Pro.

Tempo una settimana e hanno messo su la <a href="http://www.sourcesense.com/contest/gallery/main.php" target="_blank">gallery con gli elaborati pervenuti</a>.

Belli o brutti che siano, una riflessione: vero che c’è un premio e il premio invoglia, ma anche vero che l’open source ha un senso, e che Apple lo ha capito. Non per niente ha <a href="http://webkit.opendarwin.org/blog/?p=44" target="_blank">regalato</a> dodici MacBook Pro ai migliori collaboratori non-Apple al lavoro su Webkit.