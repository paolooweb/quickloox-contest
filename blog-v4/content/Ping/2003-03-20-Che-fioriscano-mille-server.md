---
title: "Lasciate che fioriscano mille server"
date: 2003-03-20
draft: false
tags: ["ping"]
---

Quando la pace si misura da Internet

Scrivo nella notte, mentre comincia il bombardamento sull’Iraq.
In giornata stavo scaricando da Internet vario software open source per Macintosh e ricordo distintamente che, volendo, avrei avuto a disposizione server iraniani, cechi, slovacchi, ungheresi, afghani. Tutti posti dove una volta non sarebbe stato possibile.

Mi auguro di poter vedere il più presto possibile server iracheni.

<link>Lucio Bragagnolo</link>lux@mac.com