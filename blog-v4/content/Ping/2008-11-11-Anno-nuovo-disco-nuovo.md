---
title: "Anno nuovo, disco nuovo"
date: 2008-11-11
draft: false
tags: ["ping"]
---

Per la prima volta Time Machine mi ha avvisato che il disco di backup è pieno e che, come da specifiche, provvede a eliminare il backup più vecchio allo scopo di creare quello più recente.

Per amore di statistica, il mio disco di lavoro è da cento gigabyte e il disco esterno di backup è da 160 giga. Il backup più vecchio che viene cancellato risale all'8 gennaio scorso.