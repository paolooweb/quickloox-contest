---
title: "Perché vogliamo che sia un Mac"
date: 2002-05-20
draft: false
tags: ["ping"]
---

Unix è bello e potente, specie se Apple lo valorizza con una interfaccia superiore

Apple è nota in tutto il mondo non tanto per il Macintosh, ma per la sua capacità di rendere semplice e usabile la tecnologia. Una dimostrazione: prendiamo due sistemi operativi – Linux e Mac OS X – e guardiamo come si installa Netscape.

Mac OS X: scaricare il software. Trascinare il file scaricato su Stuffit Expander. Montare l’immagine-disco. Copiare sull’hard disk la cartella Netscape. Aprire la cartella di Netscape. Fare doppio clic sull’icona del browser.
Linux: Scaricare il software. Digitare tar -zxvf netscape-i686-pc-linux-gnu-installer.tar.gz. Digitare cd netscape-installer. Digitare ./netscape-installer Seguire le istruzioni dell’installatore. Spostarsi nella directory in cui è stato installato Netscape. Digitare ./netscape.

È evidente quale sia la sequenza più semplice. È altrettanto evidente che Mac OS X può diventare fantastico, grazie alla potenza di Unix e alla semplicità di Macintosh.
Purché Apple si ricordi il motivo per cui è nota in tutto il mondo.

<link>Lucio Bragagnolo</link>lux@mac.com