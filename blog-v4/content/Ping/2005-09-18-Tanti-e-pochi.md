---
title: "Tanti e pochi<p>"
date: 2005-09-18
draft: false
tags: ["ping"]
---

Considerazioni statistiche sull&rsquo;umanità<p>

Le persone che conosco e frequento più spesso non hanno problemi con il Mac. Può accadere che non sappiano come ottenere un certo risultato. O che abbiano una buona idea ma non trovino il programma giusto per mancanza di perizia, da <a href="http://www.versiontracker.com/macosx">VersionTracker</a> in poi. Però tutto funziona.<p>

Le persone che conosco ma non frequento sono quelle che incontro via Internet, nei forum, nelle mailing list, durante un evento in <a href="http://www.macatwork.net">Mac@Work</a>, in vacanza o a una conferenza stampa.<p>

Nell&rsquo;ambito di queste persone, ovviamente in numero molto maggiore di quante possa conoscerne direttamente per chiare questioni geografiche e fisiche, si trovano persone che hanno problemi con il Mac. Può succedere.<p>

La cosa che mi impressiona non è questa. È la distribuzione dei problemi. Ti aspetteresti cento persone con un problema ciascuno. Invece sono cinque persone con venti problemi a testa, e gli altri vivono tranquilli. A pensare male si fa peccato, però eccetera eccetera.<p>

Se quei cinque, almeno, leggessero Macworld.<p>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>