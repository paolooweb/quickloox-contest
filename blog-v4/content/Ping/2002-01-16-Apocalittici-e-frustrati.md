---
title: "Gli infelici a finestre"
date: 2002-01-16
draft: false
tags: ["ping"]
---

A che serve sapere che esistono migliaia di programmi, quando ne servono venti?

Parlo con un signore in un superstore dell’elettronica.
Mi dice: bello, il Mac, è che ci sono prochi programmi... gli chiedo: lei che cosa usa per scrivere? Mi cita un programma Microsoft. E per fare calcoli? Un programma Microsoft. E la posta elettronica? Un programma Microsoft. E il suo browser qual è? Risposta scontata.
Torchiandolo emergono anche un player Mp3, sei o sette videogame, un po’ di utility, PowerPoint piratato e tre o quattro altre cose.
Ma scusi, dico, il totale fa meno di trenta. Anche se facesse cento... dove sono tutti gli altri programmi? Allarga le braccia. “Eh, avere il tempo, poi bisogna impararli, poi, poi...”. Guardi, gli dico, che i programmi su Mac sono migliaia, mica trenta o cento. Allarga ancora le braccia.
Flaiano diceva che la felicità è desiderare ciò che si ha. Se è vero, un utente Windows rimane un infelice per la vita.

lux@mac.com