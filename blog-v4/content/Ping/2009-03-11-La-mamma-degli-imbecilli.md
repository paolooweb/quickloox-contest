---
title: "La mamma degli imbecilli"
date: 2009-03-11
draft: false
tags: ["ping"]
---

L'idea che un prodotto abbia un costo e che però sia possibile per vie ingegnose e misteriose averlo senza pagare per quel costo è evidentemente infondata. Però ci sono sempre <a href="http://www.freestuff-now.info/" target="_blank">qualcuno che ci prova</a> e qualcuno che ci casca.

Replico il link solo perché tanto la cosa è riservata agli americani. Ma nessun problema: su eBay si possono trovare miracoli a volontà per tutti i Paesi, basta cercare.