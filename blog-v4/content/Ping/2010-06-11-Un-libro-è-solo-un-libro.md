---
title: "Un libro è solo un libro?"
date: 2010-06-11
draft: false
tags: ["ping"]
---

Uno dei temi dominanti dei prossimi mesi, colpevole pure iPad che nel giro di otto settimane ha preso il 22 percento del mercato dei libri digitali negli Stati Uniti e probabilmente ha già superato le vendite di due anni e mezzo di Kindle, sarà il libro elettronico. Come cambierà ha importanza relativa, al contrario di come finiranno per cambiare le nostre abitudini di lettura, molto interessante.

I libri tradizionali tuttavia non spariscono e gli editori più avveduti hanno iniziato a muoversi.

Me lo ha mostrato <a href="http://www.casinieditore.com" target="_blank">Casini Editore</a>, che ringrazio fin d'ora per avermi gentilmente omaggiato de <i>L'Evocatore</i>, primo atto della <a href="http://www.amonsaga.com/" target="_blank">saga <i>fantasy</i> di Amon</a> firmata da <a href="http://paolaboni.com/" target="_blank">Paola Boni</a>.

Il libro, non a me ma a chiunque acquisti, arriva dentro una scatola di cartone, semplice ma elegante, che con diversa decorazione potrebbe benissimo contenere un prodotto elegante di elettronica di consumo; per la prima volta un editore ha pensato a portare nel libro il piacere di aprire la scatola.

Dentro la scatola emergono due cartoline promozionali dedicate al libro e tre <i>booktrailer</i>, opuscoli ultratascabili di poche pagine con la stessa copertina del libro e dentro passi salienti del libro stesso, impaginati e illustrati nello stesso modo. Il lettore soddisfatto, è il ragionamento, farà promozione ulteriore distribuendo i <i>booktrailer</i> a parenti colleghi e amici.

Trovo anche un catalogo dell'editore. Lo sfoglio per dovere e invece è un piacere; il linguaggio è s&#236; quello del catalogo ma, per capirci, del catalogo Ikea. Diretto, con descrizioni interessanti a prescindere delle trame e degli autori, con foto apprezzabili degli autori stessi e, in mezzo, pagine che spiegano il perché della scatola e il perché, per dire, i libri Casini non hanno la numerazione delle pagine. Scelta condivisibile o meno, ma scelta e con spiegazione. Vale la pena di leggere il catalogo ed è la prima volta in decenni. Oltretutto, dal catalogo si apprende dell'esistenza di canali Casini su Facebook, YouTube, MySpace, Twitter, Skype e mi dimentico qualcosa.

Finalmente arrivo al libro, che è inserito in un cofanetto cartonato interamente illustrato con anta, stile World of Warcraft e altri videogiochi. A questo punto mi è venuta voglia assoluta di sfogliare il libro, fosse anche un manuale di meccanica pesante applicata ai cantieri navali.

Il libro riporta in fondo un codice numerico univoco da fornire al sito stesso per accedere a contenuti supplementari.

Per dire se il libro è bello o meno mi serve qualche serata, l'ho appena assaggiato. Se parliamo della confezione, c'è di che riflettere da subito.