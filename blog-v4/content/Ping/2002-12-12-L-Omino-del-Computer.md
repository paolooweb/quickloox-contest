---
title: "L’omino del computer"
date: 2002-12-12
draft: false
tags: ["ping"]
---

Storia di un piccolo incubo parafamiliare

I miei quasisuoceri sanno pochissimo di computer e hanno a casa un PC Windows di cui sanno azionare non più del minimo indispensabile. Padre e figlia (quasicognata) condividevano lo stesso programma di posta e la stessa password di accesso alla mail. Su richiesta della quasicognata, che desiderava una maggiore privacy e non gradiva Outlook Express, le avevo installato Eudora e contestualmente le avevo creato un account su Tiscali (il quasisuocero sta su Libero).

Giorni fa si è fuso l’alimentatore del PC. Il quasisuocero ha telefonato a un Omino del Computer che svolge l’assistenza per conto della ditta assemblatrice. L’Omino del Computer è arrivato e ha cambiato l’alimentatore. Non contento, ha cancellato Eudora in quanto “due programmi di posta sono troppo difficili da gestire” e ha cancellato l’account Tiscali perché “si sa che Tiscali non funziona tanto bene”.

Sono sicuro di una cosa: un Omino del Computer così verrebbe cacciato a pedate da qualsiasi abitazione con dentro un Mac.

<link>Lucio Bragagnolo</link>lux@mac.com