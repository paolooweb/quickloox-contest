---
title: "Comandi&#8230; appiccicosi"
date: 2008-01-28
draft: false
tags: ["ping"]
---

AppleScript e appiccicosità in due casi semplici semplici.

Un caso di appiccicosità è quello in cui prendiamo un simbolo e gli appiccichiamo addosso un valore. Circa come quando vediamo passare una persona che si distingue e gli assegnamo la categoria <em>elegante</em>.

Un'altra metafora è quella di un cassetto, in cui inseriamo un oggetto. In termini di programmazione, abbiamo una variabile cui si assegna un valore. Fino a che non togliamo l'oggetto dal cassetto, il cassetto continua a contenere l'oggetto, ovvero la variabile continua a conservare il valore che le abbiamo dato.

Per appiccicare un valore a una variabile, si usa (fra gli altri) il comando <code>set</code>:

<code>set scatola to 19</code>

appiccica (assegna) al cassetto di nome <code>scatola</code> il valore <code>16</code>.

Infatti, se scriviamo

<code>display dialog scatola</code>

non appare la parola scatola, ma appunto il suo valore. Proviamo a mettere le due righe appena citate dentro Script Editor:

<code>set scatola to 19</code>
<code>display dialog scatola</code>

e dare il comando Esegui. Una scatola può avere come valore non solo un numero, ma anche una stringa, cioè una sequenza di caratteri, come in

<code>set scatola to "ciao!"</code>

Programmare non è difficile. Imparare il concetto di variabile è una delle cose essenziali eppure basta pensare a cassetti e a roba appiccicaticcia.

Per recuperare gli altri <em>post</em> dedicati a questa serie su AppleScript, basta tornare indietro nel tempo a passi di sette giorni. Questo è il quarto.