---
title: "Più Tiger per tutti<p>"
date: 2005-07-03
draft: false
tags: ["ping"]
---

Contro i riflessi condizionati da luogo comune<p>

Sono reduce da una visita al <a href=" http://www.giovanninoguareschi.com/23club2.htm">Club dei Ventitré</a> dove ho installato Tiger su un eMac a 700 megahertz con 384 megabyte di Ram.<p>

Al termine dell&rsquo;installazione, effettuata senza problemi da un materizzatore Dvd esterno collegato via FireWire, ho mostrato le novità principali del sistema per una mezz&rsquo;oretta. Come mi è già accaduto di vedere in casi simili, il computer era a suo agio e appariva in certi momenti perfino più scattante di prima.<p>

Non lasciarti frenare dalla poca Ram o dal Mac &ldquo;vecchio&rdquo;; Tiger funziona bene e porta un mondo di vantaggi. La differenza tra avere un Mac che vecchio non è affatto (e che un buon sistema operativo riesce a ringiovanire) e credere ai luoghi comuni è che, nel secondo caso, ci si perde.<p>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>