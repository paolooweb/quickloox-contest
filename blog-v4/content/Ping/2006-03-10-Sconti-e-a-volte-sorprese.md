---
title: "Sconti e (a volte) sorprese"
date: 2006-03-10
draft: false
tags: ["ping"]
---

Occhio a <a href="http://www.maczot.com/" target="_blank">MacZot!</a> Ogni giorno offrono un programma shareware diverso a prezzo scontato rispetto a quello ufficiale.

In questo momento stanno offrendo uno sconto a sorpresa: 14,95 dollari per la registrazione di cinque programmi di cui non &egrave; dato sapere.

Ben venga tutto, se serve a fare crescere la frequenza di pagamento dello shareware Mac.