---
title: "Il tempo delle repliche"
date: 2009-05-07
draft: false
tags: ["ping"]
---

Si è visto l'altroieri che in fatto di fogli elettronici <a href="http://www.macworld.it/blogs/ping/?p=2296" target="_self">i formati di Office e OpenOffice sono sostanzialmente incompatibili</a>. L'unico modo di essere sicuri che un file Odf creato su Office funzioni bene è usarlo solo con Office. Film già visto.

La compatibilità salta primariamente perché Odf 1.1, il formato <em>open source</em> standardizzato, ha certe mancanze relative ai fogli di calcolo. Microsoft è l'unica a utilizzarle per creare problemi agli utenti anziché lavorare per minimizzarne l'effetto, però ha buon gioco nel sostenere che secondo lei è la soluzione migliore e se gli altri la pensano diversamente, vabbeh, sono fatti loro. (però perché il <em>plugin</em> Sun usato da Office per registrare in formato Odf <a href="http://www.sun.com/software/star/odf_plugin/" target="_blank">afferma la compatibilità 1.2</a>, la cui standardizzazione ancora in corso fa parte delle scuse di Microsoft?)

Relativamente ai fogli di calcolo.

Veniamo al <em>word processing</em>. Situazione assai meno complicata, dal punto di vista del formato.

Doug Mahugh di Microsoft mostra che <a href="http://blogs.msdn.com/dmahugh/archive/2009/04/28/working-with-odf-in-word-2007-sp2.aspx" target="_blank">l'interoperabilità tra Office e OpenOffice sembra molto buona</a>.

Su Groklaw hanno provato a creare un documento di testo su Word, registrarlo in Odf e aprirlo con OpenOffice. Hanno fatto cose tipo inserire un pié di pagina, una immagine, un riquadro extra di testo&#8230; cose normali. Ma <a href="http://www.groklaw.net/article.php?story=20090503215045379" target="_blank">aprirlo su OpenOffice è un disastro</a>. Chi mente?

I fogli elettronici sono complicati e d'accordo. Nel 2009 i documenti di testo, per quanto formattati, lo sono molto meno. Però l'unico modo di avere un documento Odf creato da Word cos&#236; come lo abbiamo creato consiste nell'usare solo Word. Film già visto.

Microsoft promette che quando sarà standardizzato il formato Odf 1.2 i problemi di interoperabilità saranno risolti. Promesse e intanto oggi le cose non funzionano. Film già visto.

Intanto qualcuno <a href="http://blogs.technet.com/gray_knowlton/archive/2009/05/06/rethinking-odf-leadership.aspx" target="_blank">chiede la testa di Rob Weir</a> per avere sollevato i problemi di cui sopra. L'accusa è che Weir non ha saputo tenere separate le sue opinioni personali dal proprio ruolo all'interno dell'organizzazione che sovrintende al formato Odf.

Rileggendo tutto quanto, è condivisibile che Weir sia andato oltre il proprio mandato. Tuttavia, adesso, a decidere le prossime specifiche di Odf c'è anche Microsoft. Una strategia tipica di Microsoft è entrare nei comitati di specifica e controllarli o condizionarli in tutti i modi possibili, direttamente o indirettamente, in modo lecito o &#8211; si è letto più volte &#8211; illecito. Potrebbe essere un film già visto anche questo.

Ho la sensazione che Microsoft stia facendo a Odf (e OpenOffice) ciò che ha già fatto troppe volte: <em>embrace</em>, <em>extend</em>, <em>extinguish</em>. Spero di essere smentito e di poter vedere almeno stavolta un film con un finale diverso.