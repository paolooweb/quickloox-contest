---
title: "La scomparsa del filesystem"
date: 2010-03-11
draft: false
tags: ["ping"]
---

Ho chiacchierato su Macworld cartaceo di marzo di fuga dal Finder e scrivevo questo.

<cite>Ho conosciuto centinaia di persone assennate che organizzano il loro Mac con la canonica diligenza del Buon Padre di Famiglia.<br /> Dispongono cartelle, file, icone come in un salotto perbene, &#8220;in ordine&#8221;. Un ordine benefico per la mente, che fa sentire di avere il controllo della situazione grazie alla straordinaria metafora della scrivania, a portata di tutti a partire da Macintosh, anno 1984.<br />
Solo che è trascorso un quarto di secolo e la complessità del sistema si è moltiplicata per mille, numero di file compreso. L'ordine resta benefico per la mente ma non procura più vantaggi pratici.<br />
Per fortuna Apple ha creato tecnologie che portano direttamente al cuore del problema &#8211; i file che servono &#8211; sfiorando il Finder e niente più.</cite>

Sei settimane dopo averlo scritto, leggo su <i>Nimble Design</i> quanto segue. Riporto i paragrafi fondamentali e raccomando la <a href="http://nimbledesign.com/post/441423115/the-path-of-most-resistance" target="_blank">lettura integrale</a>.

<cite>Con iPhone OS Apple sta piantando un proiettile nella testa di una longeva convenzione di cui la maggior parte delle persone potrebbe fare a meno. [&#8230;]</cite>

<cite>Anni fa mi sono stufato di offrire supporto informatico ai miei parenti e gli ho fatto un'offerta: se avessero comprato un Mac, li avrei supportati 24/7 senza lamentarmi. [&#8230;] Adesso la gran parte di loro usa Mac e le richieste di supporto sono passate da molte in un mese a molte in un anno. [&#8230;]</cite>

<cite>Siccome ora i miei parenti possono davvero usare il computer invece che stare a riavviarlo, posso osservare meglio come lo usano. Ho visto che nessuno di loro sa come usare il</cite> filesystem<cite>.</cite>

<cite>Sfortunatamente per la persona media, il</cite> filesystem <cite>è cos&#236; complicato che tutto quanto esula dalla Scrivania e dalla cartella Documenti appare come un labirinto che molto probabilmente cela trappole e minotauri. [&#8230;]</cite>

<cite>Uno dei concetti che colpiscono maggiormente dell'estetica giapponese è la focalizzazione sullo spazio negativo. I concetti di</cite> ma <cite>e</cite> wabi-sabi <cite>considerano tanto ciò che viene aggiunto quanto ciò che viene levato. [&#8230;]</cite>

<cite>Apple sta facendo qualcosa di inusitato con iPhone OS. Essenzialmente sta omettendo funzioni che la gente una volta dava per scontate in un tipico computer. E una delle omissioni più grosse è il</cite> filesystem<cite>. In iPhone OS, il concetto di file è essenzialmente sparito, sostituito da</cite> applicazioni e la roba che esse producono<cite>.</cite>

<cite>Il risultato finale è che le persone normali sono più felici con i loro computer e i tecnomani sono irritati dal fatto che Apple ha</cite> instupidito <cite>il computer. I tecnomani, quando criticano iPad per non sostituire un vero computer, lamentano la mancanza di un</cite> filesystem<cite>. Beh, cos&#236; sia.</cite>

<cite>Spero che gli apparecchi come iPad e iPhone contribuiranno al disuso del</cite> filesystem <cite>visivo. La sua morte, negli apparecchi per il consumatore, faciliterà la vita di quanti vogliono semplicemente ottenere risultati senza dover pensare al computer.</cite>

Il Finder è un'interfaccia grafica al <i>filesystem</i>. Ed ecco che il concetto è lo stesso. La metafora della scrivania ha fatto grandi cose per tutti noi, ma sta per arrivare a fine vita.