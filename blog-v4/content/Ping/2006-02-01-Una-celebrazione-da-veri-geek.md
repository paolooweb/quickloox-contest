---
title: "Una celebrazione da veri <em>geek</em>"
date: 2006-02-01
draft: false
tags: ["ping"]
---

Anche un quesito per veri conoscitori <em>profondi</em> della materia.
Penso che ne far&ograve; l&rsquo;argomento del prossimo Preferenze su Macworld carta e quindi riveler&ograve; l&rsquo;arcano solo a tempo debito. Poi, per qualche giorno posso pure giocare al matematico snob che scrive <em>ho una dimostrazione bellissima ma mi manca lo spazio sul foglio</em>.

Si &egrave; recentemente verificato un cambiamento epocale. Il Mac &egrave; entrato nell&rsquo;era della C.