---
title: "Come mai non siamo all&rsquo;otto?<p>"
date: 2004-09-13
draft: false
tags: ["ping"]
---

Ci sono motivi più che a sufficienza per adottare il nuovo BBEdit<p>

Sono innamorato di BBEdit. Certamente non è adatto a scopo letterario (i miei articoli sono scritti con <a href="http://www.tex-edit.com">Tex-Edit Plus</a>, altro programma da innamoramento), ma per trattare il testo, come si fa in una fonderia con l&rsquo;acciaio o in una segheria con i tronchi d&rsquo;albero, non ce n&rsquo;è per nessuno.<p>

Il passaggio dalla versione 6 alla 7 introduceva i worksheet Unix (documenti di testo che si comportano come Terminali e che si possono salvare ed editare, eccellenti per gli ignoranti di Unix come me, che possono salvare worksheet di comandi utili e difficili da ricordare), ma poco altro di eclatante.<p>

Il passaggio dalla versione 7 alla 8 invece è memorabile. Arrivano i worksheet di documenti, la text factory che automatizza certe operazioni senza bisogno di passare da AppleScript, un supporto Unicode assai migliorato e un sacco di altre cose che si apprezzano mentre si usa il programma.<p>

Vale completamente il suo prezzo, upgrade o non upgrade (e le persone con un minimo di raziocinio, leggendo bene le istruzioni di upgrade sul sito, possono risparmiare qualcosa).<p>

Raccomandato, raccomandatissimo. <a href="http://www.barebones.com">BBEdit 8</a>.<p>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>