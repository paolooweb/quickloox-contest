---
title: "Divide et impara"
date: 2002-04-01
draft: false
tags: ["ping"]
---

Per capire meglio che cosa fanno e quanto valgono i prodotti Apple, niente è meglio di...

Non è conosciutissimo ma piuttosto interessante, il programma <link>Learn And Earn</link>http://training.euro.apple.com/. Sono lezioni tenute da Apple relativamente ai propri prodotti, nell’intento di spiegare alla forza vendita, ai tecnici e al supporto quali sono i punti di forza e le possibilità dei prodotti della casa.
Sono lezioni brevi ma esaurienti, con numerosi rimandi di approfondimento e un quiz interattivo che consente di verificare la comprensione della materia. I quiz danno punti e chi accumula più punti riceve periodicamente premi in natura da Apple.
In questa serate di primavera, dove capita di indugiare davanti al computer anche dopo l’uscita con gli amici, vale la pena di fare un giro anche lì.

<link>Lucio Bragagnolo</link>lux@mac.com