---
title: "Il testo &egrave; interfaccia"
date: 2006-05-21
draft: false
tags: ["ping"]
---

Era ora. Uno dei designer web pi&ugrave; autorevoli mondo, Derek Powazek, afferma a chiare lettere <a href="http://www.alistapart.com/articles/learntowrite" target="_blank">A tutti i designer: imparate a scrivere!</a>

Dedicato a chi pensa che il cuore di un sito siano cinquanta secondi di Flash inutile messi all&rsquo;inizio di roba sgrammaticata.