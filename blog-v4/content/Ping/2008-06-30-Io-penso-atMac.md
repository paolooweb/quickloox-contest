---
title: "Io penso @mac"
date: 2008-06-30
draft: false
tags: ["ping"]
---

Lo so che sono <a href="http://www.macworld.it/showPage.php?template=notizie&amp;id=15107">già attivi gli indirizzi @me.com</a>. Io continuo a usare <a href="mailto:lux@mac.com">l'indirizzo originario</a>, che continua a funzionare e registrai tre minuti dopo l'annuncio.