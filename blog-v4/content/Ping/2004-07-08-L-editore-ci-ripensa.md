---
title: "L’editore ci ripensa"
date: 2004-07-08
draft: false
tags: ["ping"]
---

La piccola soddisfazione di avere ragione da sempre o quasi

A riunione nel comitato editoriale di una nota casa editrice. Per diverso tempo – puntualizza l’amministratore delegato – abbiamo abbandonato il mercato Macintosh ed è stato un errore, perché è un’area relativamente piccola ma composta da lettori qualificati che investono in formazione e informazione.

Cinque anni fa gli avevo fatto notare che era un errore. Ma non gliel’ho rinfacciato; ho assentito, con un lieve sorriso.

<link>Lucio Bragagnolo</link>lux@mac.com