---
title: "Ora o per sempre"
date: 2008-02-27
draft: false
tags: ["ping"]
---

Sul mio Mac AppleWorks è totalmente slegato dall'architettura di stampa di Mac OS X. Ha smesso di stampare e anche di produrre Pdf.

Non posso (ancora) dire se sia dovuto a Leopard, all'aggiornamento 10.5.2, a un altro aggiornamento, a un problema temporaneo o a un problema locale (cose che andrebbero verificate, prima di gridare al <em>bug</em> o alla mancanza di supporto o a questo o a quell'altro). Di certo, se non fossi già passato integralmente a Numbers, avrei dovuto iniziare a pensarci.