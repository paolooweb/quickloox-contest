---
title: "A che gioco giochiamo"
date: 2007-03-03
draft: false
tags: ["ping"]
---

<strong>Jfordo82</strong> sta cercando Mac-chinisti che vogliano partecipare con gusto all'<a href="http://www.italianlanparty.com/" target="_blank">Italian Lan Party</a> in programma tra il 13 e il 15 aprile a Bologna.

Il genere è lo sparatutto in prima persona, tra Unreal, Quake, Call of Duty e via mitragliando.

Direi che per avanzare candidature basta lasciare un commento con indicazione della mail. A cena, tra il tipo di giochi e la <em>location</em>&#8230; mortellini al ragù.