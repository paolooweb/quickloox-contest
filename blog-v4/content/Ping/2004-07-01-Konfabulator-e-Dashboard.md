---
title: "Originali e copie"
date: 2004-07-01
draft: false
tags: ["ping"]
---

Le confabulazioni su Dashboard sono sciocchezze

Alla conferenza mondiale degli sviluppatori di Apple annunciano che Mac OS X 10.4, alias Tiger, possiederà un meccanismo detto <link>Dashboard</link>http://www.apple.com/macosx/tiger/dashboard.html che permette la creazione di miniprogrammi galleggianti sulla scrivania.

L’autore di <link>Konfabulator</link>http://www.konfabulator.com/, motore che fa qualcosa del genere, si è messo a gridare allo scandalo e non è proprio giusto. Meno giusto ancora è il fatto che tanti si siano uniti al coro senza saperne niente e senza averne titolo, ripetendo pappagallescamente qualcosa di sentito da altri.

Basta <link>approfondire</link>http://daringfireball.net/2004/06/dashboard_vs_konfabulator di un centimetro per scoprire che Dashboard e Konfabulator si basano su motori completamente diversi. E, vivaddio, Macintosh montava gli accessori di scrivania vent’anni fa.

A volte Apple non si è comportata bene (vedi Sherlock 3 e la sua imbarazzante somiglianza con Watson di Karelia). Qui è chi strepita a sproposito, che si comporta male.

<link>Lucio Bragagnolo</link>lux@mac.com