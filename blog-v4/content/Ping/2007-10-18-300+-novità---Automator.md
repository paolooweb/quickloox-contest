---
title: "300+ novità: Automator"
date: 2007-10-18
draft: false
tags: ["ping"]
---

Come da pagina di Apple, ma non ancora tradotta in italiano, Automator è stato variamente migliorato.

Adesso ci sono gli Starting Points, che appaiono automaticamente nella finestra di flusso di lavoro, un po' come in Pages compaiono i modelli preimpostati tra cui scegliere.

L'interfaccia è stata potenziata, soprattutto moltiplicando i criteri di ricerca delle azioni, cos&#236; da trovare più facilmente quella che si sta cercando. Un clic rivela il log delle azioni compiute; sono accessibili tutti i media gestiti da iLife.

Una nuova azione, <em>Watch Me Do</em> (guardami mentre faccio), registra le nostre azioni e le piazza nel flusso di lavoro.

Automator è accessibile anche da Terminale e può colloquiare con altri linguaggi di programmazione.

Le variabili di flusso di lavoro memorizzano valori utili al flusso stesso, come nei programmi convenzionali, per maggiore versatilità e praticità.

Ora è prevista la possibilità di ripetere un flusso di lavoro per un numero di volte arbitrario.

Infine sono state aggiunte nuove azioni, per interagire con i feed Rss, gestire le istantanee in arrivo da iSight, manipolare Pdf e altro.

Giuro che appena Apple traduce la pagina, smetto. :-)

