---
title: "Rss salutista"
date: 2009-05-11
draft: false
tags: ["ping"]
---

Mi permetto di dare maggiore visibilità alla segnalazione dell'ottimo <a href="http://www.ilmacaco.com" target="_blank">Macaco</a>, che ha postato l'indirizzo del <a href="http://www.macworld.it/blogs/ping/?feed=rss2&amp;cat=0" target="_blank">feed Rss integrale di Ping</a>.

Fa bene alla linea ed è più digeribile. Dal lettore Rss.