---
title: "Universo backup"
date: 2007-06-17
draft: false
tags: ["ping"]
---

Il vecchio iMac Lcd 15&#8221; non vede più la rete AirPort di casa, che gli altri computer vedono tranquillamente. System Profiler &#8220;vede&#8221; la scheda AirPort, ma il menulet AirPort non trova la rete, dicevo. Una reinstallazione di Mac OS X con formattazione del disco non ha dato risultati.

L'unico fastidio è che per il backup settimanale ho dovuto collegare direttamente il disco esterno Usb al PowerBook (di solito è collegato all'iMac, che lo condivide su AirPort).

Nel frattempo <strong>Paolo</strong>, al lavoro sul suo <a href="http://www.gamestar.it/blogs/roots/" target="_blank">Back to the Roots</a>, mi segnalava la possibilità di <a href="http://www.xs4all.nl/~pot/infocom/" target="_blank">giocare online</a> le avventure Infocom più famose (non tutte, ma è molto).

Comprensibile che la mia testa fosse stata abbastanza concentrata sul tema backup. E mi sono chiesto se abbia più senso conservare la mia copia di Bureaucracy, avventura Infocom non compresa nell'elenco, sul suo bravo floppy che è rimasto a casa dei miei genitori, o se invece non sia del tutto indicato disinteressarsi della conservazione di quello che si trova agevolmente su Internet in ogni dato momento, dal software fino alla musica e avanti ancora fino ai film. Gli unici dati che ha senso backuppare sono quelli personali. O no?

Intanto, appena ho un momento, aprirò l'iMac sperando di trovare la scheda AirPort male inserita e che basti quello.