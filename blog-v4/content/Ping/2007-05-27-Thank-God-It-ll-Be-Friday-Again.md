---
title: "Thank God It'll Be Friday, Again"
date: 2007-05-27
draft: false
tags: ["ping"]
---

A parte l'infortunio di venerd&#236;, i venerd&#236; del gioco continuano. Venerd&#236; prossimo, 1&#176; giugno 2007, dalle 13 alle 14, si gioca a <a href="http://www.freeciv.org/index.php/Freeciv" target="_blank">FreeCiv</a>, il clone di Civilization gratis e <em>open source</em>.

Mi raccomando di scaricare la <a href="http://prdownloads.sourceforge.net/freeciv/freeciv-2.0.9.dmg?download" target="_blank">Stable Version</a> (ora è la 2.0.9). Questa gira su Tiger; per Mac OS X 10.3 serve una versione precedente, indicata sul sito.

Altre avvertenze: FreeCiv richiede X11; FreeCiv è discretamente complesso. Si scopre tutto giocando, ma chi teme l'ignnoto si dia un'occhiatina alla documentazione Html.

Aprirò come sempre (o lo farà chi arriva prima di me) la stanza di iChat <code>gamefriday</code>, per coordinarsi e anche chiacchierare intanto che ci civilizziamo a vicenda. :-)