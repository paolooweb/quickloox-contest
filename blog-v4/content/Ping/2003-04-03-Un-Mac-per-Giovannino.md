---
title: "Un Mac per Giovannino"
date: 2003-04-03
draft: false
tags: ["ping"]
---

Il Club dei Ventitré passa a Macintosh. Nasce il club del ventidue?

In sé non è questa gran notizia. Ogni giorno si vendono migliaia di Macintosh.
Ma a passare a Mac sono Alberto e Carlotta Guareschi, i figli del celebre creatore della saga di don Camillo e Peppone.

Il <link>Club dei Ventitré</link>http://www.giovanninoguareschi.com è stato fondato da loro, insieme ad alcuni giornalisti di gran nome e vari altri personaggi illustri, per tenere viva la memoria del padre, proseguire la pubblicazione dei numerosi inediti che ha lasciato e arrivare a catalogare l’opera omnia dello scrittore.

Guareschi si metteva modestamente sotto Manzoni e affermava, quindi, di non poter avere più di ventitré lettori. Quindi questi piccoli Ping non potranno mai superare quota ventidue.

Il che, come diceva Giovannino, è bello e istruttivo.

<link>Lucio Bragagnolo</link>lux@mac.com