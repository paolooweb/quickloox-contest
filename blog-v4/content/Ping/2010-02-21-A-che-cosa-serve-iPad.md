---
title: "A che cosa serve iPad"
date: 2010-02-21
draft: false
tags: ["ping"]
---

Ho già individuato una fascia di utilizzo ideale per iPad. Ristretta ma a colpo sicuro.

Supponi di avere sfortunatamente ricevuto un colpo della strega e di ritrovarti per un paio di giorni con la schiena bloccata.

Devi spostare appuntamenti, avvisare, magari hai comunque una cosa da scrivere o da sistemare. Ma la posizione supina non è l'ideale per usare un portatile.

Il fortunato che avesse tra le mani un iPhone se la caverebbe discretamente. Nel trattare la posta non ci sono problemi e il tagliaeincolla permette anche di quotare nella posizione giusta in pochi istanti.

Avere un iPad, tuttavia, sarebbe tutt'altra questione. Specie nel momento in cui riesci a stare seduto ma il portatile resta comunque pesante e pure caldo.