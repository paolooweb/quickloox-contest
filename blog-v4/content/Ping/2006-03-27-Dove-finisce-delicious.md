---
title: 'Dove finisce <a href="http://del.icio.us/" target="_blank">del.icio.us</a>'
date: 2006-03-27
draft: false
tags: ["ping"]
---

Comincia <a href="http://www.smarking.com/" target="_blank">Smarking</a>, mi segnala l’amico <a href="http://www.afhome.org" target="_blank">Francesco</a>. La cosa particolare è che quest’altro servizio di collezione e condivisione di bookmark sarebbe opera di un singolo studente comasco. Una bella dimostrazione della potenza di Internet avendo in mano una buona idea e un po’ di programmazione in saccoccia.

