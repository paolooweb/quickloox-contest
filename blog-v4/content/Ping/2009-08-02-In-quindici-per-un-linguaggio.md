---
title: "In venti per un linguaggio"
date: 2009-08-02
draft: false
tags: ["ping"]
---

Lo ammetto, ho un amore speciale per AppleScript. Ma mica è l'unico. Per avvicinarsi alla programmazione, anche piccola, anche banale (ma mai stupida), ci sono altri linguaggi interessanti.

Uno di questi è Ruby. Per farne la prima esperienza hanno creato un semplice <a href="http://www.ruby-lang.org/en/documentation/quickstart/" target="_blank">tutorial</a>. Venti minuti e uno ne esce infarinato abbastanza per decidere se prendere o lasciare (serve l'inglese, ma era inevitabile).

Su Mac? Ruby è già installato. Apri il Terminale, scrivi <code>irb</code> e sei dentro. Sapendo che cosa scrivere, chiaro.