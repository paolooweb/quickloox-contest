---
title: "Rinunce e regali"
date: 2008-12-26
draft: false
tags: ["ping"]
---

Sono stato più buono di quello che pensavo. Babbo Natale mi ha ricoperto di regali splendidi.

Tra questi un <a href="http://www.m-audio.com/products/en_us/Keystation61es.html" target="_blank">controller Midi Usb Keystation 61es di M-Audio</a>. Tra noi mortali, una tastiera musicale che da sola sarebbe un soprammobile e collegata a un Mac si trasforma in un piano elettrico, un sintetizzatore, un sassofono, qualunque cosa.

Prima che la vicenda prenda la piega di un insopportabile sfoggio di fortuna personale, dico subito che l'aggeggio, collegato al PowerBook, si è rifiutato di funzionare. Audio Midi Setup, dentro /Applicazioni/Utility, non dava segno di riconoscerlo e addirittura il sistema si diceva impossibilitato a lanciare il server Midi.

Avevo ignorato i Cd in dotazione alla Keystation e ho pensato che fosse questione del <em>driver</em> da installare. Ma fin dalla scatola l'oggetto veniva definito <em>class device</em>, capace di funzionare su Mac OS X senza bisogno di software aggiuntivo (infatti, verificato <em>a posteriori</em>, il Cd installa unicamente le icone dei prodotti M-Audio e nessun <em>driver</em>).

Seguendo una traccia trovata su Internet, ho levato dalla cartella <code>/Libreria/Audio/MIDI Drivers</code> il file <code>EmagicUSBMIDIDriver.plugin</code> e tutto ha iniziato a funzionare senza effetti collaterali. Il file è marcato Apple e <em>credo</em> possa arrivare a seguito dell'installazione di GarageBand.

Per godermi la Keystation dovrò rinunciare al <em>plugin</em>. Ce la farò.