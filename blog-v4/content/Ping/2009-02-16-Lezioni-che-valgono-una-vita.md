---
title: "Lezioni che valgono una vita"
date: 2009-02-16
draft: false
tags: ["ping"]
---

Ho trovato un <a href="http://www.dreamsyssoft.com/unix-shell-scripting/tutorial.php" target="_blank">corso di scripting di shell</a> che sono riuscito a seguire almeno fino alla quinta lezione senza i sudori freddi.

Siccome le lezioni sono nove vuol dire che è anche una cosa frequentabile da una persona interessata, sia allo <em>scripting</em> di <em>shell</em> sia al conservarsi una vita nonostante lo studio del suddetto.