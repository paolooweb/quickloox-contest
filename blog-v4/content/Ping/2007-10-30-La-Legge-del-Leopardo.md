---
title: "La Legge del Leopardo"
date: 2007-10-30
draft: false
tags: ["ping"]
---

Si parla molto di Leopard in questi giorni e, dopo tanta attesa, sembra anche logico. Nelle mailing list impazzano quelli che hanno installato, hanno avuto un qualche errore e immediatamente concludono che l'installazione è bacata.

La legge del leopardo: è cos&#236; una volta su cento. Gli altri novantanove errori sono un problema tuo, locale, dovuto a casini locali, dipendenti da qualche situazione locale da capire.

Nelle sole prime 48 ore di vendita di Leopard sono state vendute due milioni di copie e ovviamente il conto è in crescita. Se tutti avessero tutti i casini che si leggono nelle mailing list scoppierebbe la rivoluzione.

D'altronde, se fosse il contrario e ciascuna macchina di ciascuno che ha un problema fosse rappresentativa del comportamento di milioni di copie installate, ci si potrebbe offrire a Cupertino come laboratorio di test. Cupertino risparmierebbe milioni e sarebbe prontissima a usarli per ricoprire di denaro la Macchina Magica. Certo, ci sarebbe da giustificare il fatto che ognuno ha una Macchina Magica ma ogni macchina ha un problema diverso&#8230;