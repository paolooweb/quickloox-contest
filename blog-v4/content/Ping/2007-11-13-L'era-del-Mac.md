---
title: "L'era del Mac"
date: 2007-11-13
draft: false
tags: ["ping"]
---

In arrivo da <a href="http://morrick.wordpress.com" target="_blank">Riccardo</a>:

<cite>Stanotte ero in vena di esperimenti, cos&#236; ho acceso il mio buon vecchio PowerBook 5300. L'ho collegato a Internet via Ethernet (il PowerBook G4 gli forniva la connessione), e ho provato a navigare qua e là.</cite>

<cite>Sorpresa: con Opera 5.0 sotto Mac OS 8.1 posso controllare la posta Gmail. Nell'allegato puoi vedere una schermata, cos&#236;, giusto per avere un'idea di come si vede su uno schermo 800 x 600 e con un'attrezzatura &#8220;obsoleta&#8221;.</cite>

<cite>Chicca finale: dato che il PowerBook 5300 non si riesce a vedere in rete (suppongo perché il Mac OS 8 è troppo vecchio per X), il modo più rapido di inviarmi lo screenshot era spedirmelo per email. Ho provato direttamente da Gmail via Web, ma facendo clic su Compose Mail, Opera si chiudeva inaspettatamente. Allora mi sono rivolto a Netscape 4.76 ed ecco qua. :)</cite>

<cite>È tutto cos&#236; 1998&#8230; sorridiamo. Ma al tempo stesso c'è da riflettere. Anche con i mezzi più limitati e obsoleti uno, se vuole, se la cava.</cite>

Un professore di matematica al Politecnico diceva <cite>imparate a usare gli strumenti deboli. Poi siete veramente liberi di scegliere lo strumento che volete. Se sapete usare solo uno strumento forte, non siete liberi</cite>.

Avessi un centesimo per tutti quelli che si sono lamentati della connessione a-quattro-megabit-però-lenta e non hanno mai provato a usare lynx.

Secondo, che bello sentire di un computer che ha dieci anni (in termini informatici, un'era) ed esiste ancora, ed è in grado di fare un sacco di cose. Basta volere.