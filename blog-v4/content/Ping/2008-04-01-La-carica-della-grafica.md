---
title: "La carica della grafica"
date: 2008-04-01
draft: false
tags: ["ping"]
---

Se c'era un ostacolo all'uso universale di Internet, è la difficoltà di contattare siti ftp di cui nessuno sa niente con complicati comandi di solo testo.

Ma sta cambiando. In Svizzera, un americano di nome Tim Berners-Lee ha sviluppato un sistema che permetterà di mettere un'interfaccia grafica su Internet: il World Wide Web.

E un'azienda di nome Mosaic Communications Corporation ha messo a punto un programma chiamato <em>browser</em>, che permette di spostarsi da un server all'altro con il mouse.

Per poterlo fare occorre che sui server Internet compaiano documenti scritti in un linguaggio speciale chiamato Html. Ma nel giro di qualche anno possiamo sperare che tutti i <em>server</em> di Internet avranno il loro documento Html.

È un cambiamento importante, cui dobbiamo essere tutti preparati. Partiamo dal <a href="http://home.mcom.com/">server di Mosaic Communications Corporation</a>.