---
title: "Salvataggio alla PowerBook<p>"
date: 2005-04-04
draft: false
tags: ["ping"]
---

Non è risparmiare tempo o soldi, ma vivere meglio<p>

Ricevo assai volentieri da Moreno.<p>

<cite>Ieri mattina mi presento ad un Nokia store per fare l&rsquo;aggiornamento del mio 6230.</cite><p>

<cite>Ad un certo punto la commessa sorridente spiega alle cinque o sei persone in fila di davanti a me che con l&rsquo;aggiornamento software si sarebbero persi tutti i dati che non fossero stati salvati nella Sim del cellulare. In quel preciso istante siamo stati tutti colti dallo 
stesso pensiero: ci sono delle foto e delle registrazioni carine nel cellulare che non vorrei assolutamente perdere!</cite><p>

<cite>Il primo della fila chiede se sia possibile effettuare dei salvataggi e la commessa spiega che ciò richiede del tempo ed ha un costo; mentre sta spiegando la cosa, mi avvicino al bancone facendo presente che non volevo assolutamente passare avanti a nessuno, apro il mio PowerBook, lo connetto al cellulare via Bluetooth e in un minuto faccio il salvataggio di quelle foto e di quelle registrazioni a cui pensavo pochi secondi prima. Per sicurezza ho poi usato anche <a href="http://www.vincenzoazzone.com/it/megacell/megacell.html">MegaCell X</a> per i messaggi (la rubrica la tengo sempre in Sim e ultimamente non avevo inserito nuovi indirizzi).</cite><p>

<cite>Una delle persone che erano in fila, mi ha chiesto se potevo fare lo stesso col suo cellulare (stesso modello del mio) e riversare i salvataggi nella chiave Usb che aveva in tasca. Altri due minuti e tutto si era concluso.</cite><p>

<cite>Fatto questo siamo passati avanti a tutte le persone che erano in fila per farsi fare il salvataggio dati e abbiamo pure risparmiato qualche euro.</cite><p>

<cite>Uscendo dal negozio per primi eravamo sicuramente più felici degli altri che erano rimasti dentro :-)</cite><p>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>