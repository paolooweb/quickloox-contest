---
title: "Inutile riparare ciò che non è rotto<p>"
date: 2005-01-30
draft: false
tags: ["ping"]
---

Altrettanto inutile, ma necessario, tentativo di porre fine a una nuova superstizione<p>

Cito Mdj in traduzione italiana.<p>

<em>Mac OS X effettua già da solo tutta la manutenzione di routine che gli serve, usando il comando cron per eseguire le pulizie periodiche. Se dovesse riparare periodicamente i privilegi, la sua manutenzione incorporata lo farebbe. Invece non lo fa. Questo dovrebbe chiudere la discussione.</em><p>

<em>Tutto ciò che va fatto è lasciare il sistema acceso di notte. La manutenzione quotidiana ha luogo alle 3:15 (ora locale); quella settimanale alle 4:30 del sabato mattina; quella mensile alle 5:30 del primo giorno di ogni mese. Se il Mac resta spento la notte, basta andare nel Terminale e digitare sudo periodic daily, o weekly, o monthly. Oppure riconfigurare crontab.</em><p>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>