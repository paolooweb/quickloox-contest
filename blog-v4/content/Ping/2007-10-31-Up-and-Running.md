---
title: "Up and Running"
date: 2007-10-31
draft: false
tags: ["ping"]
---

Dopo avere lavorato tutta l'estate sulle <em>build</em>, ho finalmente installato l'edizione definitiva di Leopard sul disco di lavoro, dopo un backup e senza lavori in corso, come bisognerebbe sempre fare.

Semplice Aggiorna. Tutto a posto, tutto (finora) funziona. Circa tre giga e mezzo in più sul disco, ma ho tralasciato una notevole quantità di driver per stampanti che credo mai userò, o comunque non domani.

L'impressione è quella di avere guadagnato qualcosina in prestazioni, come del resto già dalle versioni preliminari.

I primi programmi lanciati (BBEdit, Safari, Vienna, Tex-Edit Plus) non danno problemi.

Il Dock (che tengo sul lato destro) si è correttamente autoimpostato sul 2D.

Unica impostazione fuori posto: Bluetooth era attivato. L'ho disattivato.

Stranezze durante l'installazione: dopo avere verificato il Dvd di installazione, il sistema è rimasto fermo mezz'ora prima di iniziare l'installazione vera e propria. Poi, nessun problema.

Dopo il riavvio, è partito Assistente Installazione. Ma mi era scaduto il tempo e dovevo prendere il treno. Ho chiuso il portatile. L'ho riaperto sul treno e il sistema si era piantato alla grandissima. Ho spento e ho riacceso a casa. Tutto regolare, nessun problema, tutte le impostazioni precedenti sono state rispettate (tranne Bluetooth).

Ho installato Xcode 3 e Dashcode, tutto va.

Unico disappunto: Time Machine non mi permette di eseguire il backup via AirPort. Continuerò a usare Backup di .Mac.

In meno di due ore sono tornato nuovamente operativo.

Sono stabilmente su Leopard.