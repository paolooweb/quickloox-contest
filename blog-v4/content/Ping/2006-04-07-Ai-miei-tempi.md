---
title: "Ai miei tempi"
date: 2006-04-07
draft: false
tags: ["ping"]
---

Sono passato davanti all&rsquo;edicola e c&rsquo;era in vendita Topolino.

Non era come ai miei tempi. In allegato alla rivista, c&rsquo;&egrave; un programma per fare computergrafica 3D.

Non era come ai miei tempi. Sulla custodia del programma, c&rsquo;&egrave; scritto Pc/Mac.

&lsquo;sti giovani d&rsquo;oggi.