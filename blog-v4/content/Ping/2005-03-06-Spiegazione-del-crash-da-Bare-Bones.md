---
title: "I competenti risposero<p>"
date: 2005-03-06
draft: false
tags: ["ping"]
---

Perché conviene comprare il software<p>

Il mio programma di posta è <a href="http://www.barebones.com">Mailsmith</a> e l&rsquo;ho acquistato perché per me la posta è un fatto professionale.<p>

Che cosa si guadagna ad acquistare un prodotto? Per esempio, nel migrare i miei dati da un Mac all&rsquo;altro Mailsmith si bloccava al momento di aprire un nuovo messaggio. Le ho provate tutte e alla fine ho risolto facendo un Archivia e Installa, ma nel frattempo avevo scritto al supporto di Mailsmith, riportando il log del crash preso da Console.<p>

Mi hanno risposto e mi hanno spiegato esattamente dov&rsquo;era il problema: nel trasferimento dati si era danneggiata l&rsquo;installazione di StuffIt. Reinstallarla avrebbe risolto il problema (come ha fatto poi il mio Archivia e Installa).<p>

Non so se tu abbia mai visto un log di crash. Ma se lo hai fatto capisci che chiunque sappia guardarlo e spiegarmi che cos&rsquo;è successo merita i miei soldi.<p>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>