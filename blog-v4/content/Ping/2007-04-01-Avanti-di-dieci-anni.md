---
title: "Avanti di dieci anni"
date: 2007-04-01
draft: false
tags: ["ping"]
---

Esattamente avanti fino al 1997, quando c'era il più avanzato palmare del mondo, tuttora insuperato.

È appunto pronto, come da segnalazione di <strong>Phil</strong>, <a href="http://homepage.mac.com/simonbell/connection/" target="_blank">Newton Connection for Mac OS X</a>.