---
title: "Un semplice gesto antispam"
date: 2008-04-04
draft: false
tags: ["ping"]
---

<strong>Pierfausto</strong> stavolta l'ha fatta grossa:

<cite>Per mancanza di tempo e per un</cite> client <cite>di posta configurato male mi sono ritrovato con 20 megabyte di puro</cite> spam <cite>su una casella che uso spesso.</cite>

<cite>Come si dice dalle mie parti,</cite> alli uai li belli rimedi <cite>(ai guai i rimedi fatti bene); non potendomi accontentare di togliere 3.960 messaggi a mano ho approfondito la conoscenza del comando</cite> <code>DELE</code> <cite>del protocollo Pop3 e ho scoperto che non supporta un sistema di cancellazione multipla dei file.</cite>

<cite>Quindi dopo qualche ricerca ho trovato questo scriptino:</cite>

<code>#!/bin/sh</code>
<code>username=&#8220;tizio.caio@sempronio.it&#8221;;</code>
<code>password=&#8220;nonteladiromai&#8221;;</code>
<code>MAX_MESS=$1</code>
<code>[ $# -eq 0 ] &#38;&#38; exit 1 || :</code>
<code>sleep 2</code>
<code>echo USER $username</code>
<code>sleep 1</code>
<code>echo PASS $password</code>
<code>sleep 2</code>
<code>for (( j = 1 ; j <= $MAX_MESS; j++ ))</code>
<code>do</code>
<code>echo DELE $j</code>
<code>sleep 0.1</code>
<code>done</code>
<code>echo QUIT</code>

<cite>da usare in questo modo nel Terminale:</cite>

<code>$ ./clean.pop3 nnn | telnet pop3.myisp.com 110</code>

<code>dove</code> <code>nnn</code> <cite>è il numero di messaggi da cancellare.</cite>

<cite>Con l'aiuto di un mio nuovo amico, Antonio Lorè, dall'<a href="http://www.cyberciti.biz/tips/remove-or-delete-all-emails-message-from-a-pop3-server.html" target="_blank">originale</a> abbiamo modificato solo la temporizzazione dell'ultimo</cite> <code>sleep</code> <cite>(se si ha una connessione veloce ha molto senso).</cite>

<cite>In meno di 10 secondi ho cancellato i 3.960 messaggi di</cite> spam<cite>. Ovviamente ho perso almeno 30 minuti per cercarlo e non so bene quante prove ho fatto perché ero convinto che non funzionasse ed interrompevo la procedura&#8230;</cite>

Aggiungo che lo scriptino va salvato in un file di testo e che prima di tutto occorre avere portato il Terminale, con il comando <code>cd</code>, sulla directory che contiene il file.

Complimenti Pierfausto!