---
title: "I regali ritornano"
date: 2009-12-23
draft: false
tags: ["ping"]
---

Nel giorno di Natale ripenso al minimo &#8220;regalo&#8221; dell'ambaradan degli inviti di Google Wave.

Settimane dopo, si sono aperti canali di comunicazione interessanti con persone nuove e voglio ringraziare in particolare <b>Paolo</b> e <b>Andy</b> per la condivisione delle loro esperienze, dai viaggi in treno (dove la preminenza dei Mac rispetto alla supposta quota di mercato è clamorosa) all'uso di Fortran con scorciatoie di Terminale.

Ho fatto una piccola cosa, ne tornano indietro tante e più più grandi. Mica male; mi aiuta a sentirmi più buono. :-)

Auguri ancora a tutti.