---
title: "Due problemi, un consiglio"
date: 2002-09-17
draft: false
tags: ["ping"]
---

Dove si capisce quale Internet conviene frequentare insieme al proprio Mac

Rubo apertamente questo consiglio a <link>Faqintosh</link>http://www.faqintosh.com sperando che non si offendano.

Quando digiti parte di un comando nel Terminale di Mac OS X 10.1, se premi Tab apaiono tutti i comandi completi che iniziano in quel modo. Comodissimo. In Jaguar, invece, il Tab funziona solo se è solo un comando inizia in quel modo. Quasi inutile. Altro problema, Jaguar non carica più di default gli alias contenuti in ~/Library/init/tcsh (la tilde indica per convenzione la propria cartella Home).

Si può risolvere tutto in due modi: localmente (per la singola utenza) o globalmente (per tutte le utenze del computer). Nel primo caso bisogna dare nel Terminale i seguenti comandi:

echo "source /usr/share/tcsh/examples/rc" >> ~/.tcshrc
echo "source /usr/share/tcsh/examples/login" >> ~/.login
echo "source /usr/share/tcsh/examples/logout" >> ~/.logout

Nel secondo, prima di tutto bisogna fornire la password di root. Poi si danno i comandi

echo "source /usr/share/tcsh/examples/rc" >> /etc/csh.cshrc 
echo "source /usr/share/tcsh/examples/login" >> /etc/csh.login 
echo "source /usr/share/tcsh/examples/logout" >> /etc/csh.logout 

Dalla sessione successiva di shell le cose saranno a posto.
Lo so che do alcune cose per scontate, ma questo è Ping, non un corso di Unix. Frequenta it.comp.macintosh, che è un newsgroup interessante, e leggi Faqintosh, che serve.

<link>Lucio Bragagnolo</link>lux@mac.com