---
title: "QuickTime suona meglio"
date: 2004-05-07
draft: false
tags: ["ping"]
---

Gli update portano anche qualità

Passa pressoché sotto silenzio il fatto che QuickTime 6.5.1 migliori le funzioni di encoding (codifica) dei brani dei Cd musicali per portarli in formato Aac.

Di fatto è vero quelli che alcuni hanno sentito a orecchio: iTunes può effettivamente suonare meglio le canzoni rippate con QuickTime 6.5.1.

Occhio anche al compressore Apple Lossless, che non perde alcuna informazione ma riduce lo spazio occupato dai file. È mirato essenzialmente ai puristi e agli ascoltatori di musica classica. Basta infatti fare qualche prova per accorgersi che la compressione (ripeto, senza alcuna perdita in qualità) è molto più efficace se a suonare è un’orchestra invece che una rock band.

Sempre più opzioni a disposizione dei timpani.

<link>Lucio Bragagnolo</link>lux@mac.com