---
title: "Parla da solo"
date: 2009-03-11
draft: false
tags: ["ping"]
---

Normalmente lascio l'attualità stretta ai siti che scopiazzano in pessimo italiano e pongono domande anziché offrire risposte. Faccio un'eccezione per il nuovissimo <a href="http://www.apple.com/it/ipodshuffle/" target="_blank">iPod shuffle</a>, la cui evoluzione mi sembra davvero straordinaria. È una lezione di <em>design</em> evolutivo che parla da sola, giusto per sfruttare il <em>calembour</em> sulla presenza di VoiceOver.

Qualcuno arriverà e dirà che si trovano aggeggi cinesi che fanno le stesse cose a metà del prezzo. Mi chiedo se il dentifricio con cui si lavano i denti la sera sia il più economico in assoluto.