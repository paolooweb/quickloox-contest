---
title: "La stagione degli acquisti"
date: 2008-03-29
draft: false
tags: ["ping"]
---

È ufficiale. Dopo mesi di trattative Steve Ballmer ha deciso, in accordo con l'ormai pensionando Bill Gates, di fare il grande passo.

Forte delle enormi quantità di denaro, frutto del grandissimo successo di Windows Vista (ben quattordici miliardi di copie vendute*. Sembra che anche da Beta Orionis 7 ne circolino centinaia di milioni di pezzi e che per questo motivo entro il 2009 invaderanno la Terra) il buon (?) Ballmer acquisterà la Apple. Non si conosce ancora l'entità della cifra che Microsoft spenderà nell'acquisto di Apple ma si vocifera che sarà  inferiore alla somma spesa per i nuovi server che serviranno a distribuire il Service Pack 1 di Vista. La prima mossa dei nuovi acquirenti della casa di Cupertino sarà quella di diversificare le linee di prodotto di OS X che sarà distribuito nelle versioni OSX Home Basic, Home Premium, Business e Ultimate al doppio del prezzo della precedente versione ma con in più il registro di sistema.

*Dati forniti dalla stessa società che ha trucc&#8230; elaborato i voti delle elezioni USA 2000 e 2004.

<em>&#8212; Carlo</em>