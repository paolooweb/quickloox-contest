---
title: "AppleScript, tema caldo (o freddo)"
date: 2008-08-14
draft: false
tags: ["ping"]
---

Lo script di oggi è rintracciabile <a href="http://en.wikibooks.org/wiki/AppleScript_Programming/Temp_converter" target="_blank">in originale</a> su WikiBooks, risorsa che consiglio vivamente di andare a visitare per quanto riguarda AppleScript.

Il suo scopo è convertire un valore di temperatura da gradi Celsius (i nostri) a quelli Fahrenheit (americani) e viceversa.

<code>--Apre una finestra di dialogo per ricevere il valore da convertire</code>
<code>display dialog "Digita una temperatura:" default answer "</code>

<code>--Controlla che sia stato inserito un numero e non qualcos'altro, se necessario dichiarando numero quello che è stato digitato</code>
<code>set thetemp to text returned of result as number</code>

<code>--Mostra una seconda finestra di dialogo, con due pulsanti, uno per i gradi Celsius e uno per i gradi Fahrenheit</code>
<code>display dialog "Pick a scale to convert to:" buttons {"F", "C"} default button 2</code>
<code>--Assegna a una variabile il valore come stringa del pulsante premuto; in altre parole, lo memorizza</code>
<code>set thebutton to button returned of result as string</code>

<code>--Se è stato premuto il pulsante Fahrenheit, converte in Fahrenheit; se è stato premuto il pulsante Celsius, converte in Celsius</code>
<code>if thebutton = "F" then
        set thetemp to thetemp * 1.8 + 32
else if thebutton = "C" then
        set thetemp to ((thetemp - 32) / 9) * 5
end if
</code>

<code>--Mostra una finestra di dialogo contenente la risposta</code>
<code>display dialog thetemp</code>

A parte sapere come si convertono gradi Celsius e Fahrenheit (una volta avevamo il Manuale delle Giovani Marmotte, adesso basta fare una ricerca su Google - scrivere <em>32 celsius to fahrenheit</em>, per esempio - o usare la Calcolatrice di Mac OS X, ma non spiegano come), questo script insegna qualcosina di utili in termini di gestione dell'input (come ricevere dati da chi sta alla tastiera e come riutilizzarli) e soprattutto introduce un tema che non si era ancora toccato.

In programmazione non è tanto importante che le cose vadano bene; è più importante che le cose non possano andare male. Il controllo di quello che viene digitato sulla tastiera è solo un esempio banale: se il programma si aspetta un numero, e chi sta alla tastiera digita <em>hjlkh</em>, che succede?

In verità i programmi migliori sono quelli che fanno andare tutto a perfezione e l'utilizzatore mano se ne accorge. Come <a href="http://www.mondaynote.com/?p=783" target="_blank">ha scritto di recente</a> (a sproposito, ma qui non importa) l'ex di Apple Jean Louis Gassée, <cite>Simple is hard. Easy is harder. Invisible is the hardest</cite>.

(Farla) semplice è difficile. (Farla) facile è più difficile. (Farla) invisibile è la cosa più difficile di tutte.