---
title: "Se hai Windows, Mac OS X 10.2 ti vede"
date: 2002-07-08
draft: false
tags: ["ping"]
---

I Pc Windows non sono affatto sicuri e Mac OS X 10.2 ne è la prova

Quanto segue non verrà utilizzato da chicchessia per scopi illegali, perché il popolo Mac è composto da persone più intelligenti, istruite e sagge della media. ma quanto sopra andava detto.
Il meccanismo di connessione in rete via protocollo Smb, quello delle reti Windows, era davvero poco comprensibile, in Mac OS X 10.1. In 10.2 è proprio tutta un’altra cosa e basta un clic per scoprire, per esempio in una rete aziendale, miriadi di Pc che hanno la condivisione attiva, pilotati da utenti che forse neanche lo sanno e, anche quando ne sono consapevoli, scelgono password assolutamente ovvie.
In questo senso Mac OS X 10.2 è ancora più aperto e adatto alle aziende e alle reti miste.
Ora sono fresco di navigazione del Pc di Mad (nome computer Mad, nome utente Mad, password Mad). Solo che lui non lo sapeva. Mad, se mi leggi, cambia password.

<link>Lucio Bragagnolo</link>lux@mac.com