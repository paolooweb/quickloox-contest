---
title: "Rivalutiamo il passato"
date: 2006-10-28
draft: false
tags: ["ping"]
---

Roberto mi invia (mille grazie) questa segnalazione d'<a href="http://cgi.ebay.it/computer-apple-E-II-in-basic-introvabile-da-collezione_W0QQitemZ290042123784QQihZ019QQcategoryZ171QQrdZ1QQcmdZViewItem" target="_blank">asta</a> e commenta <cite>un computer &#8220;ibmcompatibile&#8221; della stessa epoca può al massimo servire a riempire un buco nel muro&#8230;</cite>

In effetti si può essere scettici sul prezzo, ma non sul principio. Un Pc di quella data riempie s&#236; il buco, ma poi uno lo copre pure con la calce, perché non si noti.