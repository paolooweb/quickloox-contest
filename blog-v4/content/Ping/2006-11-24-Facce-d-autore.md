---
title: "Facce d’autore"
date: 2006-11-24
draft: false
tags: ["ping"]
---

Mi sono letto con piacere <a href="http://www.libreriauniversitaria.it/BIT/8804542594/Facce_da_iPod.htm" target="_blank">Facce da iPod</a>, il nuovo libro di Alberto Pucci, già autore di un fortunato <a href="http://www.italica.rai.it/index.php?categoria=multimedia_libri&amp;scheda=faccedamac" target="_blank">Facce da Mac</a>. Nessun impegno particolare è richiesto; le pagine vanno giù una dietro l'altra come brani di una playlist piacevole. Ogni capitolo è legato a una canzone e descrive un episodio particolare nell'epopea di iPod, oppure ne tratteggia in modo rapido e leggero un aspetto particolare. Ci sono playlist giuste per momenti particolari e interviste con Vip di ogni ordine e grado per sapere del loro rapporto con iPod, passando da personaggi come Eros Ramazzotti o Linus.

Alberto &#8212; o chi per lui a Mondadori Informatica &#8212; è stato cos&#236; gentile da regalarmene una copia a sorpresa. Siccome il libro mi è piaciuto, lo regalerò alla prima occasione conviviale che mi tocca, sia una pizzata del Poc oppure altro. Giusto che possa goderne anche qualcun altro.