---
title: "Anche se minore di uno"
date: 2002-04-08
draft: false
tags: ["ping"]
---

Se non ti senti contento del tuo browser, prova mozilla.org

Il mio amico Stefano giorni fa si era sfogato: possibile che su Mac OS X funzioni tutto tranne Explorer?
Gli ho fatto un elenco ddelle possibilità e ora è felice; non per il mio elenco ma perché ha scaricato Mozilla.
“È il browser definitivo”, ha detto.
Forse esagera. Ma se lo ha reso così contento una versione 0.9.9, forse è proprio il caso di dargli un’occhiata. A <link>Mozilla</link>http://www.mozilla.org, certo.

<link>Lucio Bragagnolo</link>lux@mac.com