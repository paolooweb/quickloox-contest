---
title: "Ricchi con MacBook Air"
date: 2008-01-21
draft: false
tags: ["ping"]
---

Scommettiamo che MacBook Air è lungo e largo come MacBook?

Facile. <a href="http://www.apple.com/macbookair/specs.html" target="_blank">MacBook Air</a> misura 32,5 x 22,7 centimetri. <a href="http://www.apple.com/macbook/specs.html" target="_blank">MacBook</a> misura 32,5 x 22,7 centimetri. Fonte: sito Apple.

E adesso, scommettiamo che MacBook Air è più lungo e più largo di MacBook?

Facile. MacBook Air misura 12,8 x 8,94 pollici. MacBook misura 12,78 x 8,92 pollici. Fonte: sito Apple.

Il bello è che il sito Apple non contiene errori.