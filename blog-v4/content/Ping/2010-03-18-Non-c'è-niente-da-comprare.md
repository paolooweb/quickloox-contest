---
title: "Non c'è niente da comprare"
date: 2010-03-18
draft: false
tags: ["ping"]
---

Per un sito italiano di sedicenti notizie su Mac, la <a href="http://www.apple.com/pr/library/2010/03/18york.html" target="_self">morte di Jerry York</a>, consigliere di amministrazione Apple dal 1997, non lo è.