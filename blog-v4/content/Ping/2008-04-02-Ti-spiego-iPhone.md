---
title: "Ti spiego iPhone"
date: 2008-04-02
draft: false
tags: ["ping"]
---

Sono sul tram. Sull'altro lato è seduto un ragazzo. Ha in mano un foglio stampato.

Sono troppo lontano per leggere e non lo farei, ma è impossibile non riconoscere il logo di Gmail. Subito sotto, un titolo cubitale <em>Incontro conoscitivo</em>.

Evidentemente si tratta di lavoro. Roba importante, non giochini da telefonino.

Nell'altra mano si trova appunto il telefonino. Faticosamente, con una mano trascrive sulla faticosa tastierina del telefonino qualcosa che legge sul foglio tenuto con l'altra mano.

Con un iPhone avrebbe già risposto da un pezzo, direttamente dal suo account Gmail.