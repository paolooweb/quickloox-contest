---
title: "Speechless"
date: 2006-09-14
draft: false
tags: ["ping"]
---

La modalità <em>gapless</em> di iTunes 7, che elimina la pausa forzata tra due brani che sono in realtà da suonare uno dietro l'altro senza interruzione, è semplicemente perfetta. Sono senza parole.