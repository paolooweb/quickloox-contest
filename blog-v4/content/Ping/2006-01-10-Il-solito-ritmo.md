---
title: "Il solito ritmo"
date: 2006-01-10
draft: false
tags: ["ping"]
---

Le poltroncine della grande sala del Moscone Center di San Francisco sono vuote e, in California, gli spettatori stanno sciamando verso le novità della fiera.

Anche Ping! ritorna ai suoi ritmi più consoni. Si preferisce l&rsquo;approfondimento alla sparata estemporanea, la ricerca delle notizie al posto del pappagallare sul lavoro altrui.

Però era bello mettere il sistema sotto stress in un&rsquo;occasione interessante e questa lo era.

In più Apple, il ritmo, lo ha tenuto. Grandi novità e un futuro più che promettente a seguito di un presente assolutamente invidiabile.

Grazie a tutti e, comunque, a fra poche ore. Ping non dorme mai. :-)