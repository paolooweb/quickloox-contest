---
title: "A un nanometro dallo switch"
date: 2006-10-16
draft: false
tags: ["ping"]
---

<strong>Alan Dragster</strong> mi ha segnalato (grazie!) questo <a href="http://www.computerpoweruser.com/editorial/article.asp?article=articles/archive/c0611/44c11/44c11.asp&amp;guid=8CF5E5CB35CB42E1857F6F42970E2A2C" target="_blank">articolo</a> di Chris Pirillo (per i non addetti: è un nome, non è uno qualunque). Va letto per intero. Mi limito a rimarcare la frase <cite>sono a un nanometro dallo spostare la mia famiglia su Mac OS X</cite> e la fine dell'articolo: <cite>Microsoft, in ogni e qualsiasi occasione, è stata la peggiore nemica di se stessa</cite>.

L'articolo si intitola <cite>Vista raddoppierà la quota di mercato di Apple</cite>.

C'è chi crede che Windows o Mac (ma vorrei dire non-Windows, nozione più ampia) siano semplicemente scelte equivalenti. Ingenui.