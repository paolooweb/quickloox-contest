---
title: "Catene in libertà"
date: 2007-11-28
draft: false
tags: ["ping"]
---

Ci sono plugin di QuickLook che fanno cose meravigliose, o almeno estendono le possibilità di QuickLook.

C'è quello che guarda dentro <a href="http://homepage.mac.com/xdd/software/zip/" target="_blank">i file Zip</a>. Quello che migliora (non poco) la <a href="http://homepage.mac.com/xdd/software/folder" target="_blank">visualizzazione delle cartelle</a>. Quello che fornisce l'<a href="http://www.eternalstorms.at/utilities/epsqlplg/" target="_blank">anteprima dei file Eps</a>.

Potrei passare per uno esperto, che trova cose interessanti su Internet. Ma non ci sarebbe questo Ping se non mi avesse passato la dritta <strong>Arranger1044</strong>.

Dopo la nostra chattata l'ho ringraziato. Lui ha scrollato le spalle (mi piace pensare che lo abbia fatto) e mi ha riferito che era tutto merito di <a href="http://www.zeronave.it/zn/" target="_blank">Zer0Nave</a>. E a loro magari lo ha detto qualcun altro.

Queste catene mi piacciono. Sono fatte di conoscenza, non di chiacchiere, e ci fanno più liberi. Anche nell'uso del Mac.