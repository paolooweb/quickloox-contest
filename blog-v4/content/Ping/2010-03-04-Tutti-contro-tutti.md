---
title: "Tutti contro tutti"
date: 2010-03-04
draft: false
tags: ["ping"]
---

Parrebbe che la causa di Apple contro Htc per violazione di brevetti sia dovuta alla paura di Apple di perdere mercato a favore di Android, o di tagliare le gambe a Google senza attaccarla direttamente, o frutto di una mentalità difensiva, o di una strategia chiusa eccetera.

Questo <a href="http://bits.blogs.nytimes.com/2010/03/04/an-explosion-of-mobile-patent-lawsuits/" target="_blank">grafico del New York Times</a> sintetizza la situazione delle diatribe sui brevetti in corso nel mercato. Ammesso che Apple faccia causa per una delle ragioni sopra addotte, è il caso di spiegare negli stessi termini anche il resto o almeno farsi venire veramente tante idee alternative.