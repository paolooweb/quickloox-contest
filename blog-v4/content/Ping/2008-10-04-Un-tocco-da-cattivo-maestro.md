---
title: "Un tocco da cattivo maestro"
date: 2008-10-04
draft: false
tags: ["ping"]
---

Ho trovato con facilità in una manciata di secondi la tabella delle <a href="http://mail.google.com/support/bin/answer.py?answer=6594&amp;query=keyboard&amp;topic=&amp;type=f" target="_blank">scorciatoie di tastiera di Gmail</a> (collezionisti? C'è anche <a href="http://documents.google.com/support/bin/answer.py?answer=66280&amp;hl=en" target="_blank">quella di Google Docs</a>). Non sono riuscito a trovare la tabella delle scorciatoie di tastiera di Mail su MobileMe.

Non conta che ci siano, dovunque siano; devono essere immediatamente individuabili anche dal più stupido degli stupidi e perfino da me perché sono scorciatoie, non la più esoterica delle funzioni avanzate.

Le stesse scorciatoie sono mostrate con chiarezza dentro i menu, ma Internet e il web 2.0 non sono ancora una scrivania e le necessità di interfaccia sono diverse: su Internet ci vuole anche una tabella.