---
title: "Tre euro per parlare"
date: 2010-10-15
draft: false
tags: ["ping"]
---

Adesso, ogni volta che sento qualcuno dire che si poteva aggiungere quella funzione, o che manca una casella, o che c'è un menu di troppo, o che ce n'è uno in meno, o che non hanno pensato a quel caso particolare che una volta su un milione, o che manca la compatibilità con il formato che ha deciso di usare l'amico dell'amico, o che comunque sia fatto il software bisogna sempre trovare un pretesto per dire che non va bene allo scopo di sentirsi importanti alla stregua della rana toro, posso consigliare di giocare per un mese su iPhone o iPod touch <a href="http://itunes.apple.com/it/app/game-dev-story/id396085661?mt=8" target="_blank">Game Dev Story</a> di Kairosoft.

2,99 euro per calarsi nei panni di uno sviluppatore. Di AutoCad? No, di giochi. E almeno si parla non dico per esperienza, ma almeno per simulazione.