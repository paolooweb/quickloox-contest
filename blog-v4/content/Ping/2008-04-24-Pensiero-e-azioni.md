---
title: "Pensiero e azioni"
date: 2008-04-24
draft: false
tags: ["ping"]
---

Riassumendo.

Apple fa <a href="http://www.macworld.it/showPage.php?template=notizie&amp;id=14742">i migliori risultati trimestrali di sempre per il secondo trimestre fiscale</a>. Aumenta le vendite dei Mac del 51 percento e sfiora il record assoluto (nel trimestre tradizionalmente più debole). Vende oltre dieci milioni di iPod, in crescita anche se solo dell'uno percento. Vende 1,7 milioni di iPhone il cui fatturato differito inizia a decollare. Supera dell'otto percento le stime degli analisti. Non c'è una singola nota negativa neanche a cercare gli aghi nel pagliaio.

Poi Apple dirama le aspettative per il prossimo trimestre. E dice che crescerà del 33 percento (non il tre o lo zerotre, il trentatré).

Il titolo scende perché gli analisti pensano che debba crescere di più.

A vedere le azioni, mi viene un pensiero. Sono gente malata.