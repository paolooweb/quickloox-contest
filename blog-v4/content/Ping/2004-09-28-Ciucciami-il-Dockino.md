---
title: "Ciucciami il Dockino<p>"
date: 2004-09-28
draft: false
tags: ["ping"]
---

Genio, Scala, tutto ok, ma c&rsquo;è dell&rsquo;altro<p>

Effetto Genio: la finestra minimizzata si infila nel Dock come farebbe il Genio di Aladino nella lampada fatidica.<p>

Effetto Scala: la finestra si rimpicciolisce proporzionalmente fino a quando non ha le dimensioni giuste per infilarsi nel Dock.<p>

C&rsquo;è un altro effetto. Per attivarlo, il comando (da Terminale) è<p>

<code>defaults write com.apple.Dock mineffect suck</code><p>

Per vederlo in azione bisogna fare un logout. Per tornare indietro si può dare lo stesso comando, con <code>scale</code> o <code>genie</code> al posto di <code>suck</code>, oppure aprire le Preferenze del Dock dal menu Apple. O modificare il documento com.apple.Dock.plist in /Home/Library/Preferences, o fare la stessa cosa con Property List Editor che sta in Xcode dentro /Developer/Applications/Utilities, oppure&hellip;<p>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>