---
title: "Pizzata alla terza"
date: 2008-11-12
draft: false
tags: ["ping"]
---

È deciso; la terza pizzata Ping si tiene mercoled&#236; 26 novembre.

Appuntamento fuori da Mac@Work, galleria Borella angolo via Carducci Milano MM1 Cadorna MM2 S. Ambrogio bus 50-58 tram 14-27 e anche dell'altro, per qualunque momento dopo le 19:30 e poi ci si muove verso la pizzeria Bio Solaire di via Terraggio 20, a due passi.

Io porto me stesso, voglia di chiacchierare e qualche libro da regalare per chi lo desidera. Chi c'è è benvenuto in tutte le modalità. :-)