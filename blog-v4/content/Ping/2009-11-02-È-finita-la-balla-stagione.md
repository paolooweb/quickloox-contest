---
title: "È finita la balla stagione"
date: 2009-11-02
draft: false
tags: ["ping"]
---

Adobe, come si dice all’Accademia della Crusca, ha sbroccato.

Se si accede con iPhone alla <a href="http://get.adobe.com/flashplayer/" target="_blank">pagina di scaricamento del plugin Flash</a>, compare un messaggio a spiegare che <cite>Apple ha posto restrizioni all’uso delle tecnologie richieste dall’uso di prodotti come Flash Player. Fino a a quando Apple non eliminerà queste restrizioni, Adobe non potrà fornire Flash Player per iPhone o iPod touch.</cite>

Adobe fa quasi tenerezza: uno guarda la pagina e sembra quasi che tra un mese potrebbero aggiungere, che so, <i>per favore</i>.

E gli imbecilli che hanno annunciato per mesi l’arrivo del <i>plugin</i> Flash per iPhone, non sapendone niente e inventando bufale di sana pianta, dove li mettiamo?