---
title: "Quando compare il riquadro"
date: 2007-11-08
draft: false
tags: ["ping"]
---

Ho visto in Leopard il riquadro semitrasparente che compare per avvisarti che il sistema non trova più i dischi in condivisione dopo che hai tolo la connessione senza smontarli (senza problemi per il Finder).

Ho visto il riquadro che compare quando il sistema si sveglia e vede in giro reti wireless che non sono le tue.

Mi chiedo quanti ce ne siano ancora. Sono contento di vederli.