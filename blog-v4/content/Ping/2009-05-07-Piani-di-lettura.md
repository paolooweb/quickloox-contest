---
title: "Piani di lettura"
date: 2009-05-07
draft: false
tags: ["ping"]
---

<a href="htttp://www.accomazzi.it" target="_blank">Misterakko</a> mi passa un <a href="http://gadgetwise.blogs.nytimes.com/2009/05/04/consumer-reports-takes-a-shine-to-apple/" target="_blank">pezzo del New York Times</a> che merita un po' di traduzione:

<cite>Battere con successo per tutta la partita è un evento raro nel</cite> baseball <cite>e altrettanto raro nelle pagine di</cite> Consumer Reports [una sorta di Altroconsumo americano, ben più temuto dalle aziende]<cite>.</cite>

<cite>Quando si va a segno tre volte su tre &#8211; come ha fatto Apple nel recente numero dedicato ai portatili &#8211; e si consegue un bel punteggio nelle altre categorie, ci si merita l'applauso.</cite>

<cite>Che uno compri affidandosi religiosamente alle opinioni di Consumer Reports o non le condivida &#8211; mi trovo un po' nel mezzo quando parlano di elettronica &#8211; un consumatore avveduto farebbe comunque bene a esaminare le classifiche del numero di giugno.</cite>

<cite>Nei portatili, i MacBook e MacBook Pro sono primi nella categoria 13&#8221;, nella categoria da 14&#8221; a 16&#8221; e tra i modelli 17&#8221;; al secondo posto rispettivamente Dell, Toshiba e Hewlett-Packard. MacBook Pro 15&#8221; ha ottenuto un punteggio di 75 su 100, davanti a un Satellite Toshiba con 64 punti (MacBook Pro costa duemila dollari, il Toshiba 700).</cite>

<cite>Tra i desktop, Mac mini è secondo dietro a un Pavillion Slimline Hewlett-Packard e iMac è secondo dietro a un Xps One Dell.</cite>

<cite>Consumer Reports ha anche sondato i lettori sul supporto tecnico. Apple ha sbaragliato tutti nelle categorie desktop e laptop.</cite>

<cite>A chi sia in procinto di acquistare suggerirei di attendere ancora un mese circa. Si mormora di possibili riduzioni di prezzo di MacBook e iMac.</cite>

<cite>È anche conveniente scommettere che Apple offrirà anche questa estate una promozione back-to-school. Di solito questa promozione va da giugno a settembre. L'anno scorso l'azienda regalava un iPod con l'acquisto di modelli di Mac selezionati.</cite>

Questo articolo potrebbe avere diversi piani di lettura.

Primo piano: i portatili Apple non hanno rivali (altro che <em>netbook</em>, signori).

Secondo piano: un articolo di quotidiano che riporta i fatti come sono, con padronanza dell'argomento, che offre ance informazioni utili al lettore.

Il mio piano di lettura: dimenticare definitivamente i quotidiani italiani relativamente all'informatica. Un articolo cos&#236;, informativo e utile, sui nostri quotidiani, lo si può solo sognare.