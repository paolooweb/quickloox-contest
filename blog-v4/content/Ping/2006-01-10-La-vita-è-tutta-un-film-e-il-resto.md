---
title: "La vita è tutta un film (e il resto)"
date: 2006-01-10
draft: false
tags: ["ping"]
---

iPhoto pieno di nuove funzioni per creare calendari o biglietti di auguri, inviare album su .mac e ad altre persone con avviso in Rss. iMovie con l&rsquo;aggiunta dei temi ed esportazione verso iPod adatti al video e verso podcast video. iDvd con nuovi menu e supporto di masterizzatori esterni.

C&rsquo;è molta attenzione al software. Più di quello, c&rsquo;è tanta attenzione a quello che le persone potrebbero voler veramente fare con il computer. Nella preistoria ci dicevano che erano utili per memorizzare le ricette di cucina. Sì. Ma prima vengono la musica, le foto, i film e la voglia di conservare e maneggiare ricordi e testimonianze.

Gran lezione, Steve.