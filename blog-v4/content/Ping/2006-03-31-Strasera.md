---
title: "Strasera"
date: 2006-03-31
draft: false
tags: ["ping"]
---

Per chi pu&ograve;, vuole e se la sente, stasera alle 18:30 in <a href="http://www.macatwork.net" target="_blank">Mac@Work</a> a Milano seminario sul backup e la gestione dei dati su Mac.

Introduce il sottoscritto, ma tranquillo; parlo al massimo per un quarto d&rsquo;ora. Quelli bravi arrivano subito dopo.

Al termine, pizzata a Bio Solaire, via Terraggio 20, tre minuti a piedi da Mac@Work, aspettando che Apple compia i suoi primi trent&rsquo;anni. :-)