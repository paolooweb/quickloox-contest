---
title: "Power to the Classic People"
date: 2008-12-18
draft: false
tags: ["ping"]
---

Non sono moltissimi quelli che a suo tempo comprarono un Mac Colour Classic, con schermo a colori da nove pollici, dieci chili di peso e soprattutto un processore 68030 a 16 <em>megahertz</em>.

Fatta questa premessa, la percentuale di quanti hanno deciso di trasformare il rissoso, irascibile, carissimo vecchio computer in un <a href="http://www.stuartbell.dsl.pipex.com/PowerCC/" target="_blank">Power Colour Classic</a> è sorprendente.

Grazie a <a href="http://www.gamestar.it/blogs/roots/" target="_blank">OldGen</a> per la segnalazione!