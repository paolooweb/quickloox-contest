---
title: "TimeCapsule Express"
date: 2009-10-12
draft: false
tags: ["ping"]
---

L'idea dei diciotto mesi di vita per un prodotto evidentemente ricorre come la profezia Maya del 2012.

Nel luglio 2006 scrivevo della <a href="http://www.macworld.it/blogs/ping/?p=931" target="_self">cretineria di quanti spacciavano diciotto mesi di vita meda per AirPort Express</a>. Per aggiornamento, quasi quaranta mesi dopo la mia AirPort Express funziona, quindi sono tra i superfortunati.

Ora il gioco si ripete con <a href="http://timecapsuledead.org/" target="_blank">Time Capsule Memorial Register</a>, dove pare legioni di utenti listati a lutto annuncino la morte della loro Time Capsule. In copertina un cimitero di Time Capsule (complimenti al grafico che fatto un lavoro notevole) e in alto, su sfondo rosso-allarme, la vita media: 17 mesi e 14 giorni mentre scrivo.

Dramma, disastro, apocalisse. Poi uno ci pensa: Time Capsule è nata <a href="http://www.apple.com/pr/library/2008/01/15timecapsule.html" target="_blank">a gennaio 2008</a>. Nessuna Capsule vivente può avere più di ventidue mesi di vita.

Molte sono state certamente comprate più avanti. Qualcuna è sicuramente morta in modo prematuro. Contemporaneamente, il sito calcola la vita media solo sulla base di quelle che sono morte, mentre non conta quelle che sono vive.

Con queste premesse, una vita media di diciassette mesi per un prodotto nato ventidue mesi fa è altissima.

Si noti che, se lo stesso sito fosse nato a marzo 2009, la vita media delle Time Capsule sarebbe risultata di dodici mesi&#8230; chi crede a queste panzane può mettersi in contatto con me? Ho da proporgli certi investimenti miracolosi in Borsa. E al Superenalotto.