---
title: "Cessi e processori"
date: 2003-11-05
draft: false
tags: ["ping"]
---

Se lo ha capito persino Microsoft, possono capirlo tutti

Adesso che è stato annunciato l’accordo per cui le future Xbox di Microsoft (da sempre legata a Intel) <link>si baseranno su processori Ibm</link>http://www.ibm.com/news/us/2003/11/031.html (produttrice dei G5 montati nei nuovi Mac), forse chi sproloquia costantemente a base di megahertz e gigahertz capirà che esistono processori, e poi esistono, beh, vedi titolo.

<link>Lucio Bragagnolo</link>lux@mac.com