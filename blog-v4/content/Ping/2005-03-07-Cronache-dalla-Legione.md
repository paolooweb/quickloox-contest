---
title: "Cronache dalla Legione<p>"
date: 2005-03-07
draft: false
tags: ["ping"]
---

Un diverso modo di giocare sta sfondando nel mondo Mac<p>

La Legio Luciferi ha appena completato un raid presso un villaggio di orchi nelle vicinanze del confine con la terra degli elfi. La scorreria ha avuto pieno successo nonostante l&rsquo;arrivo, dopo poco, di orchi di rinforzo. Per domenica prossima la Legio ha in programma una spedizione nelle terre dei non morti. Sarà più difficile, ma si stanno preparando.<p>

La Legio Luciferi è composta da una trentina di (giovani) Mac-people che si incontrano in World of Warcraft, un mondo virtuale di massa popolato, a neanche un mese dall&rsquo;apertura, da oltre cinquantamila persone reali iscritte da tutto il mondo.<p>

I mondi virtuali di massa esistono da prima di Internet e, anche nell&rsquo;era della computergrafica 3D, ne prosperano già numerosi (per esempio Everquest di Sony). Molti funzionano su Mac e alcuni sono esclusivi per Mac (come Clan Lord di Delta Tao).<p>

Ma World of Warcraft sembra avere veramente una marcia in più e farà il botto, anche su Mac.<p>

Ah, la Legio Luciferi si riunisce su <a href="http://www.devilsgames.it">Devils Games</a>.<p>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>