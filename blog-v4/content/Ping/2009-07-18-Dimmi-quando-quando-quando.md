---
title: "Dimmi quando quando quando"
date: 2009-07-18
draft: false
tags: ["ping"]
---

L'ennesima novità di Snow Leopard, zona iChat, dalla <a href="http://www.apple.com/it/macosx/refinements/enhancements-refinements.html" target="_blank">pagina che Apple non traduce</a>.

<b>Marcatura di ora e data sulle trascrizioni</b>

Il menu Composizione di iChat permette di apporre data e ora attuali sulla trascrizione di una chat.

Non sarà solo <i>L'avevo detto io</i>, ma anche <i>quando</i>.