---
title: "Premio migliore copertura keynote"
date: 2007-01-10
draft: false
tags: ["ping"]
---

Assegnato per acclamazione a <a href="http://www.momentosera.com/articolo.php?id=2021#" target="_blank">Momento Sera</a>. Motivazione: loro sì che sanno chi comanda veramente in Apple.

Molte grazie a <strong>mr_k</strong> per la segnalazione!