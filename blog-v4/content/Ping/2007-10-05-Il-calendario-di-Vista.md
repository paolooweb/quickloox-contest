---
title: "Il calendario di Vista"
date: 2007-10-05
draft: false
tags: ["ping"]
---

Ho capito a che punto è Windows Vista.

Chattavo con <strong>Federico</strong> e mi diceva <cite>ma sono cose successe anche su Mac</cite>.

<cite>Ti ricordi la transizione da Mac OS 9 a Mac OS X?</cite>

<cite>E la Ram che non basta mai?</cite>

<cite>E i driver che non ci sono?</cite>

S&#236;, esatto. A parte qualche distinguo, è andata cos&#236;. A inizio secolo.

Ecco la prova provata che il calendario di Vista è cinque anni indietro. E il motore dentro al cofano neanche è cambiato.