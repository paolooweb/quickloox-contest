---
title: "Sempre più servizio<p>"
date: 2004-12-18
draft: false
tags: ["ping"]
---

Sta crescendo lentamente, ma in continuità, .mac<p>

Ogni volta che accenno a qualcuno dell&rsquo;esistenza di <a href="http://www.mac.com">.mac</a> la primissima reazione è che costa 99 euro l&rsquo;anno e quindi, per definizione, non è una buona cosa.<p>

Alcuni mi fanno notare che tutti i servizi offerti da .mac sono ottenibili in altro modo gratuitamente (vero), e in generale si tratta di persone che non sono capaci di farlo.<p>

Nessuno valuta veramente se i soldi valgano i benefici.<p>

Secondo me sì e non solo per i servizi. Ci sono tutorial, programmi gratis, musica gratis, offerte speciali e un sacco di altre cose. In questi giorni .mac regala un gioco e due capitoli di un libro, e in più ha inaugurato una nuova feature: un elenco di programmi che collaborano con .mac, tra cui Macromedia Contribute e Allume StuffIt Deluxe.<p>

Più prosaicamente, quest&rsquo;anno invierò gli auguri di Natale via .mac e sarà più semplice, ed economico, che in qualunque altro modo.<p>

Il mio account me lo tengo ben stretto. Anche a 99 euro.<p>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>