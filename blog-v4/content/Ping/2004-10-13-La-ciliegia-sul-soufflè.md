---
title: "La ciliegia sul soufflè<p>"
date: 2004-10-13
draft: false
tags: ["ping"]
---

L&rsquo;ennesima storia di emulatori-miracolo<p>

Adesso è arrivato Cherry OS, che secondo il suo autore consente di fare funzionare Mac OS X su un Pc all&rsquo;80 della velocità di un Mac originale.<p>

Capiamoci: VirtualPC, che viene ottimizzato da anni e anni per recuperare prestazioni in ogni angolo del codice, forse non arriva a questo traguardo.<p>

Per chi crede a tutto quello che viene scritto e si entusiasma subito, un consiglio: provare Cherry OS.<p>

Quando il processore del Pc si sgonfierà come un soufflè malriuscito, non dire che non eri stato avvisato.<p>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>