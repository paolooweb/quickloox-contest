---
title: "L'add-on è mobile"
date: 2009-12-16
draft: false
tags: ["ping"]
---

Quando sparano scemenze sul futuro, gli analisti tengono perfettamente fede al loro nome. La parte del corpo usata per lavorare è quella.

Quando invece raccolgono i fatti sul passato, svolgono un lavoro preziosissimo.

Cos&#236; Morgan Stanley, nello sparare una vasta serie di scemenze sul futuro dell'Internet <i>mobile</i>, ha raccolto una messe di informazioni impressionante sull'andamento di iPhone e iPod touch e dell'ecosistema che essi formano assieme a iTunes.

L'ecosistema è quello cresciuto più velocemente di tutti, il doppio della giapponese DoCoMo (tutti sappiamo quanto siano frenetici e attaccati ai loro cellulari i giapponesi), il quintuplo di Netscape ai tempi del primo boom di Internet, l'ottuplo di America On Line che praticamente ha dato la prima Internet ai primi americani.

Primo fatto: Apple è <cite>due/tre anni avanti</cite> sulla concorrenza. E questo sistema le chiacchiere sui vari <i>iPhone killer</i>.

Secondo: iPhone e iPod touch rappresentano il 17 percento degli <i>smartphone</i> ma il 65 percento del traffico web e il 50 percento dell'uso di applicazioni <i>mobile</i>. Se questo non spiega chiaramente che con iPhone la gente va e sta su Internet più volentieri che con qualsiasi altro aggeggio, non so proprio che altro fare.

L'uso di applicazioni si sposa bene con un altro fatto: l'americano medio passa 40 minuti al giorno al cellulare, per il 70 percento in chiamate. L'americano medio con iPhone passa 60 minuti al giorno sull'apparecchio e solo il 45 percento del tempo va in chiamate. Palesemente è un apparecchio diverso dagli altri nella sua categoria.

Ultimo fatto: Facebook ha 350 mila applicazioni e anno su anno è cresciuto del 137 percento. iPhone ha centomila applicazioni e anno su anno è cresciuto del 163 percento. Considerato che Facebook è gratis e iPhone costa soldi veri e non pochi, chi sta veramente cambiando il mondo?

iPhone è un <i>add-on</i> alle nostre vite, non un cellulare.

Per i maniaci, ci sono due sintetiche fonti di dati e informazioni, una da <a href="http://www.morganstanley.com/institutional/techresearch/pdfs/Mobile_Internet_Report_Key_Themes_Final.pdf" target="_blank">659 slide</a> e una da <a href="http://www.morganstanley.com/institutional/techresearch/pdfs/MOBILEINTERNET_12_15_09_RI.pdf" target="_blank">424 pagine</a>. Gli analisti vanno forte anche con la sintesi.