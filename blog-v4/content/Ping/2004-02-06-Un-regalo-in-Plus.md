---
title: "Un regalo in Plus"
date: 2004-02-06
draft: false
tags: ["ping"]
---

Iscriversi a .mac ha una ulteriore piccola convenienza in più

Tra giochi, corsi, programmi, utility e servizi il prezzo di .mac si è dimostrato più che giustificato, almeno per quanto mi riguarda. Ieri sera ho ulteriormente approfittato di un’altra offerta: l’abbonamento gratuito per un anno al servizio Plus di VersionTracker.com, che viene commercializzzato a 24,95 dollari.

Non so tu, ma il sottoscritto, senza VersionTracker, si sentirebbe un utente Mac più povero. .mac mi ha fatto sentire un po’... Plus ricco

<link>Lucio Bragagnolo</link>lux@mac.com