---
title: "Materie ed energia"
date: 2009-11-10
draft: false
tags: ["ping"]
---

Parte domani sera il nuovo palinsesto di <a href="http://oilproject.org/" target="_blank">Oilproject</a>, progetto didattico del tutto sui generis che parla di storia delle idee, economia e business, attualità e Internet.

I relatori sono persone di grande spessore professionale e spesso anche umano, come <a href="http://www.robertovacca.com/" target="_blank">Roberto Vacca</a>, <a href="http://blog.quintarelli.it/" target="_blank">Stefano Quintarelli</a> e <a href="http://gianlucadettori.nova100.ilsole24ore.com/" target="_blank">Gianluca Dettori</a> (più molti altri). Non prendono un euro, ma i contributi sono di grande valore

Tutte le lezioni sono gratuite e liberamente scaricabili.

Quattro materie, tanta energia impiegata per diffondere sapere. Merita attenzione.