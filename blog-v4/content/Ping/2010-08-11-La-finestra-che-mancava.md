---
title: "La finestra che mancava"
date: 2010-08-11
draft: false
tags: ["ping"]
---

<cite>È come portare con sé una piccola finestra, da cui sbirciare quel mondo immenso che è Internet.</cite>

Lo dice Susan Orlean, scrittrice e giornalista del New Yorker, a proposito di iPad in <a href="http://www.macworld.com/article/153046/2010/08/orlean_ipad.html" target="_blank">un'intervista</a> concessa a Macworld.com.

Dopo tanti anni di Windows, finalmente è stata aperta la finestra giusta.