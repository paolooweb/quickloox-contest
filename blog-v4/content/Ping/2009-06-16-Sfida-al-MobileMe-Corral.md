---
title: "Sfida al MobileMe Corral"
date: 2009-06-16
draft: false
tags: ["ping"]
---

Sto per inviare questo messaggio alla mailing list <a href="http://liste.accomazzi.net/mailman/listinfo/misterakko" target="_blank">Misterakko</a>:

Lavoro abbastanza di routine con documenti semplici residenti su MobileMe ma non avevo fogli di calcolo e sono rimasto incuriosito dai resoconti sulla difficoltà di lavorarci.

Cos&#236; ho creato un foglio di calcolo Numbers '09 direttamente su MobileMe, editandolo sempre e solo su MobileMe, senza mai toccare il disco rigido.

Non avendo buone idee per creare dal nulla e in fretta un foglio di calcolo verosimile, ho abbozzato l'inizio della scheda-personaggio che uso per giocare a Dungeons and Dragons. Ci ho dedicato non più di un quarto d'ora ieri notte e quindi la cosa è completamente rozza, totalmente incompleta e graficamente inesistente.

Tuttavia, per cominciare da qualcosa più di zero, ricorrono almeno tre funzioni diverse e il tema si presta bene a ogni tipo di complicazione. Esistono schede-personaggio per Excel che pesano più di due megabyte e che risultano largamente incompatibili con OpenOffice da quanto sono intricate.

Non ho il tempo per fare una scheda vera e veramente funzionante, specie entro poco. Tuttavia, cinque minuti prima di andare a dormire, aggiungerò spesso qualcosina. Se qualcuno ha richieste, vuole vedere una funzione, manifesta dubbi, qualsiasi cosa, si faccia sentire e lo accontenterò per quanto possibile. Il file risiederà sempre su Mobile Me e sempre l&#236; sarà modificato.

Il file si chiama ProvaFinestra.numbers, richiede Numbers '09 e si trova nella cartella Pubblica del mio iDisk <i>lux</i>, raggiungibile dal menu <i>Vai->iDisk</i> del Finder oppure <a href="http://public.me.com/lux" target="_blank">da browser</a>.

Attendo <i>feedback</i>. :)