---
title: "Steve passa a Mac<p>"
date: 2005-07-28
draft: false
tags: ["ping"]
---

Non è Jobs e nemmeno Wozniak. Neanche Ballmer. Tuttavia conta, come del resto Jeff<p>

Cara Microsoft,<br>
per anni sono stato un grande fan della vostra azienda [&hellip;] Sfortunatamente sto per scaricarvi a favore di Mac OS X e Web 2.0.<p>

È l&rsquo;inizio di una <a href="http://www.micropersuasion.com/2005/07/dear_microsoft_.html">puntata</a> di Micro Persuasion, blog di Steve Rubel. Steve è un guru delle relazioni pubbliche, personaggio ignoto al grande pubblico, ma addetto ai lavori che conta e il cui parere pesa.<p>

Steve cita in proposito anche Jeff Travis. Già creatore di Entertainment Weekly e attualmente consulente per la New York Times Company presso About.com. Anche lui ha presumibilmente un network personale di gente importante, e influente.<p>

Ha anche lui un blog e anche lui è <a href="http://www.buzzmachine.com/archives/2005_07_03.html#009984">passato a Mac</a>.<p>

Due considerazioni. La prima: apri un blog, che è una cosa buona. La seconda: c&rsquo;è gente che conta, che conta su Mac.<p>

Grazie a <a href="http://www.maestrinipercaso.it">Vanz</a> per la segnalazione!<p>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>