---
title: "Marchette parentali / 2"
date: 2007-01-03
draft: false
tags: ["ping"]
---

Mi è stato regalato nientemeno che un disco esterno Usb 2 da 500 giga.

A parte che fino a ieri non sapevo dove mettere la roba, mentre adesso non so che roba mettere dove posso farlo, ho cambiato radicalmente la strategia di backup: da Dvd grosso modo mensili a una copia radicale della cartella Home ogni settimana&#8230; e Dvd mensili.