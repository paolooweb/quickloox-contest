---
title: "Nel regno del fattibile / 15"
date: 2010-09-07
draft: false
tags: ["ping"]
---

<cite>pregasi elencare azioni fattibili su un iPad e non fattibili su un Sony Vaio W da 10&#8221;</cite>.

Fondare la <a href="http://ipad-orchestra.com/" target="_blank">iPad Orchestra</a> e, soprattutto, esibirsi con <i>app</i> quali <a href="http://itunes.apple.com/us/app/seline-hd-music-instrument/id388640430" target="_blank">Seline Hd</a>.