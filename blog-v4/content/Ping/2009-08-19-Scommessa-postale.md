---
title: "Scommessa postale"
date: 2009-08-19
draft: false
tags: ["ping"]
---

È uscito <a href="http://www.mailsmith.org" target="_blank">Mailsmith 2.2</a> ed è perfino gratuito. Il programma corre sotto le insegne di una società che non è più Bare Bones e questo lascerebbe pensare male. In realtà il nuovo marchio è stato creato dal fondatore e amministratore delegato di bare Bones, Rich Siegel, il quale continua a fare il suo mestiere ma ha dato, come <a href="http://groups.google.com/group/mailsmith-talk/msg/828b147215359462" target="_blank">spiega lui</a>, <cite>una nuova casa</cite> al programma.

Dovrebbe essere una notizia positiva, perché elimina l'ambiguità di una società normalmente orientata a vendere buoni prodotti come Bare Bones, che però ha impiegato moltissimo tempo per riuscire a fare compiere a Mailsmith certi salti di qualità e probabilmente non riusciva più a giustificare il lavoro sul programma rispetto alle entrate. L'avvento di Spotlight ha significato riscrivere da capo l'interno motore di database e questo ha portato a tempi e difficoltà notevoli.

Di certo ho vissuto tranquillamente dal 2005 a oggi con Mailsmith 2.1.5 e dunque non mi sorprenderebbe arrivare più che tranquillamente al 2015 con Mailsmith 2.2.

Basta dare un'occhiata alla <a href="http://www.mailsmith.org/support/mailsmith/current_notes.html" target="_blank">pagina dei cambiamenti</a> per capire che non ci sono davvero più scuse per rinunciare al miglior programma di posta elettronica sulla piazza.