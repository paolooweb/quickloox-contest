---
title: "Non mi resta che contare"
date: 2006-01-26
draft: false
tags: ["ping"]
---

Ahim&eacute;, non ce l&rsquo;ho fatta. Ho dovuto riavviare. Ho avuto un crash irrimediabile; tastiera non attiva, porte non attive, impossibile accedere al sistema via login remoto.

La cosa peggiore &egrave; che non sono riuscito a trovare la causa del crash in Console. <a href="http://www.rabellino.it/blog/" target="_blank">Gianugo</a> mi ha consigliato di attivare il flag COREDUMP in /etc/hostconfig, per trovare log ancora pi&ugrave; esplicativi di quelli di Console nella directory /cores. Quasi quasi.

Vabbeh. Mancato l&rsquo;obiettivo primario (non riavviare), mi resta quello secondario. Contarli. Per ora, uno.