---
title: "Come nasce una leggenda"
date: 2002-09-06
draft: false
tags: ["ping"]
---

Fai una domanda stupida, ricevi una risposta qualsiasi, crea una notizia dal nulla

C’era una volta un giornalista che chiese a Steve Jobs, parola più parola meno, se Apple stesse pensando di cambiare processore nei Mac per adottare processori Intel.

Invece di rispondere “Sei un cretino. Le riscrivi tu, settimana prossima, le migliaia e migliaia di programmi scritti per Macintosh che smetterebbero di funzionare dalla sera alla mattina?”, Steve Jobs, come avrebbe fatto qualsiasi responsabile di un’azienda che dà lavoro a ottomila persone e serve quasi trenta milioni di clienti in tutto il mondo, ha invece risposto molto diplomaticamente che Apple rimane sempre aperta a tutte le soluzioni.
Nella testa del giornalista è andata così: Apple non smentisce, quindi c’è qualcosa di vero. Da lì milioni di intellettuali del computer si pongono un dubbio che basterebbe riflettere cinque minuti per cestinare.

Chiedi a una tua amica “Vieni a cena stasera?” e lei risponde “Forse sì e forse no; posso prendere qualunque decisione”. Secondo te verrà a cena?

<link>Lucio Bragagnolo</link>lux@mac.com