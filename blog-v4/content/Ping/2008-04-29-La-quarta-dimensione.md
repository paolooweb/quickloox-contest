---
title: "La quarta dimensione"
date: 2008-04-29
draft: false
tags: ["ping"]
---

Mi sono reso conto solo ora che sto scaricando contemporaneamente l'immagine del prossimo Cd di Macworld, la quarta beta del <a href="http://developer.apple.com/iphone" target="_blank">kit di programmazione di iPhone</a> e <a href="http://vegastrike.sourceforge.net/" target="_blank">Vega Strike 0.5</a>.

Oltre due gigabyte in un battito di ciglia.

Sono cresciuto a kilobyte. Poi sono arrivati i megabyte. Poi i gigabyte. È quasi tempo di entrare nella quarta dimensione.

E di cambiare disco rigido.