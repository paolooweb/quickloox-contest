---
title: "Codice d'onore"
date: 2011-02-13
draft: false
tags: ["ping"]
---

Mac App Store non permette l'uso di codici di licenza. Il software scaricato da l&#236; può essere installato e usato su tutti i computer di proprietà e usati dal titolare dell'account.

Questo non vuol dire che un singolo iWork possa essere installato su cinquanta computer di un'azienda dal titolare dell'azienda; esiste una licenza di utilizzo da rispettare. Contemporaneamente, niente impedisce tecnicamente al titolare dell'azienda di farlo ugualmente.

In questo modo però è lui il disonesto e finisce l&#236;. Gli utilizzatori onesti non vengono penalizzati da numeri di serie da gestire, aggeggi hardware da attaccare al computer, problemi quando si reinstalla il sistema o si cambia disco rigido e altre piacevolezze tipiche dei sistemi di protezione fino qui usati dai produttori di software. Quando il venditore si fida e l'acquirente si impegna a rispettare il contratto sul proprio onore si fanno gli affari migliori.

Se Mac App Store rendesse più piacevole e trasparente la vita ai compratori onesti &#8211; compro al prezzo giusto e installo dove voglio senza altre preoccupazioni &#8211; già questa sarebbe una buonissima ragione per averlo.

Me lo dice l'esperienza di possessore regolare di InDesign di Adobe, regolarmente acquistato a prezzo pieno, regolarmente aggiornato a prezzo pieno e costretto a fare funzionare il software con un numero di serie recuperato su Internet, perché il numero di serie originale del pacchetto originale non piace all'aggiornamento.