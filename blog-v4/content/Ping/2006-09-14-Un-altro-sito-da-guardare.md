---
title: "Un altro sito da guardare"
date: 2006-09-14
draft: false
tags: ["ping"]
---

La notizia è già nota epperò la persona è gentile e simpatica: Massimo Rotunno, fresco lanciatore di <a href="http://www.macuniverse.eu/" target="_blank">MacUniverse</a> nelle grandi praterie del Web.

Per il mondo Mac, specialmente italiano, è una crescita. In meglio. I migliori auguri a Massimo per il migliore sviluppo del suo nuovo sito, alla faccia delle ripetizioni. :-)