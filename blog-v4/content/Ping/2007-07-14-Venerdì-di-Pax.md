---
title: "Venerd&#236; di Pax"
date: 2007-07-14
draft: false
tags: ["ping"]
---

<strong>Alfium</strong> mi ha fatto compagnia e impartito una sonora lezione di <a href="http://sillysoft.net/pax/" target="_blank">Pax Galaxia</a>.

Altro che &#8220;gestionale&#8221;. Il gioco è intrigante, immediato da capire e infinito da padroneggiare. Sarebbe forse Risiko, se non avvenisse in tempo reale e non ci fosse un concetto sottile e geniale di ritirata strategica. Sarebbe forse la dama, se non si finisse - con la scusa del gioco - per creare reti di trasmissione di massima efficienza.

Complimenti ai programmatori. E ad Alfium, capace pure di aprire una partita via rete al contrario di me. :-)