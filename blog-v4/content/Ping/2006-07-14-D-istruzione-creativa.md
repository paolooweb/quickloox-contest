---
title: "D'istruzione creativa"
date: 2006-07-14
draft: false
tags: ["ping"]
---

<strong>Pierfausto</strong> mi ha segnalato il sistema di apprendimento online messo in piedi con il nome di <a href="http://cssfad.unile.it/" target="_blank">Campus Satellitare del Salento</a>. C'è anche un corso avanzato di Mac OS X e ci sono corsi aperti anche agli ospiti, non necessariamente iscritti all'università.

Non so se sia bello perché c'è un corso su Mac OS X, o perché sia una lodevole iniziativa di apprendimento online di cui tanto c'è bisogno, oppure perché sia un segnale abbagliante del Futuro dell'istruzione avanzata. Forse è bello per tutte e tre le cose.