---
title: "La fine dei mondi"
date: 2009-05-30
draft: false
tags: ["ping"]
---

<a href="http://thematrixonline.station.sony.com/players/" target="_blank">The Matrix Online</a>, gioco di massa su Internet di Sony, <a href="http://forums.station.sony.com/mxo/posts/list.m?topic_id=36300028715" target="_blank">chiude i battenti il 31 luglio</a> (probabilmente questo link richiede la registrazione gratis sul servizio The Station di Sony).

Era solo per Windows (hanno creato anche <a href="http://www.petitiononline.com/mxomac/petition.html" target="_blank">una petizione a proposito</a>).

I giochi scritti per Internet, che non sono compatibili con tutta Internet, sono malscritti, oppure malconcepiti, oppure progettati con logica vecchia di quindici anni (in informatica è come se fossero settantacinque nel mondo reale).

Muoiono e gli sta bene. Non è certo la fine del mondo. Solo quella dei mondi malfatti.