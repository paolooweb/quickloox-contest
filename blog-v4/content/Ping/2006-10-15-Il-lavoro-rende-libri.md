---
title: "Il lavoro rende libri"
date: 2006-10-15
draft: false
tags: ["ping"]
---

Annuncio al colto e all'inclita che sono coinvolto nella realizzazione di due libri: uno su Leopard, a quattro mani con Akko, e uno su iPod, che lo seguirà a ruota e che scriverò da solo.

Suggerimenti, critiche e richieste di ogni tipo sono sempre bene accette. L'unica cosa che non posso più esaudire è la preghiera di astenersi, dato che gli accordi sono stati presi. :-)