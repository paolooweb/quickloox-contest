---
title: "Ogni cosa al suo posto"
date: 2011-01-21
draft: false
tags: ["ping"]
---

Vorrei averlo scritto. Posso <a href="http://andrewsullivan.theatlantic.com/the_daily_dish/2011/01/apple-and-our-culture-ctd-4.html" target="_blank">citarlo</a> però.

<cite>Come persona di</cite> marketing<cite>, so che la gente valuta i prodotti nei fatti ma decide di comprarli in base a che cosa sente. I prodotti Apple tengono viva nelle persone una sorta di folle speranza. È la speranza della modernità: che nuove idee possano renderci migliori. Parte del genio di Apple è offrire queste idee in un modo che festeggia l'umanità e le cose che ci rendono umani (come il piacere che si prova ascoltando una dietro l'altra due canzoni che mai avremmo messo insieme, grazie a iPod in modalità casuale, una cosa possibile perché Apple ha capito che sarebbe stato grande poter portare sempre con sé tutta la propria musica).</cite>

<cite>Poi c'è questo: Apple raggiunge la grandezza senza scusarsi. Quote di mercato e profitti sono importanti solo come conseguenze. Non sono il primo scopo, che è raggiungere l'</cite>insanely great<cite>, il follemente grande. Come se fossero alla ricerca di un Graal (il professor Jones disse al figlio Indiana:</cite> la ricerca del Graal è la ricerca del Divino che sta in tutti noi<cite>).</cite>

<cite>S&#236;, è solo metallo, plastica, silicio. S&#236;, Apple incassa un sacco di denaro. Ma queste due osservazioni mancano completamente il punto riguardo ad Apple. È questione di ispirazione, speranza e di prendere in mano il futuro e il posto che vi avrà l'umanità.</cite>