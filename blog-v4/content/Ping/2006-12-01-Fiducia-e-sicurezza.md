---
title: "Fiducia e sicurezza"
date: 2006-12-01
draft: false
tags: ["ping"]
---

Riavvio in occasione del Security Update, ma richiesto espressamente dal sistema e quindi nessun problema. Quelli non voluti restano sei, dal primo gennaio a oggi. Chissà se riesco ad arrivare al 2007 senza neanche un riavvio indesiderato.