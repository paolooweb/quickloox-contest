---
title: "Giusto un appunto"
date: 2008-11-02
draft: false
tags: ["ping"]
---

<a href="http://www.google.com/notebook/" target="_blank">Google Notebook</a> funziona, sembra, perfettamente con Safari.

Mi ricordavo dell'usabilità di Google Docs, mentre Notebook visualizzava un messaggio di incompatibilità. E il notebook certe volte torna utile.