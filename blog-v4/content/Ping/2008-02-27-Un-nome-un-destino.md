---
title: "Un nome un destino"
date: 2008-02-27
draft: false
tags: ["ping"]
---

I miei interventi da ignorantello su AppleScript sono stati criticati perché <cite>la programmazione interessa a pochi</cite>.

Forse sono pochi, ma è programmazione? La seguente citazione viene da un <a href="http://www.gnu.org/gnu/rms-lisp.html" target="_blank">discorso di Richard Stallman</a>, padre del <em>free software</em>:

<cite>Multics Emacs si dimostrò un grande successo; programmare nuovi comandi di editing era talmente comodo che perfino le segretarie dell'ufficio avevano iniziato a imparare a farlo. Usavano un manuale che mostrava come estendere Emacs, ma non la chiamava programmazione. Cos&#236; le segretarie, che credevano di non essere capaci di programmare, non si spaventavano. Leggevano il manuale, scoprivano di poter fare cose interessanti e imparavano a programmare.</cite>

Facciamo che non sia programmazione, bens&#236; estendere Mac OS X. Resteremo in pochi lo stesso, ma non è un cruccio.