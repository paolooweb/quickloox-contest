---
title: "Era sbagliata proprio la domanda"
date: 2006-06-04
draft: false
tags: ["ping"]
---

Il pallino mi gira da tanto tempo e forse un giorno lo far&ograve; pure. Fare un sito delle previsioni dei cosiddetti esperti e riderci sopra, nel momento in cui si scopre che sono tutte scemate.

Frugando nel box &egrave; saltato fuori uno studio del <a href="http://www.gartner.com/" target="_blank">Gartner Group</a> del 1989, intitolato <em>Apple Macintosh Futures</em>. Devo premettere che il Gartner Group non &egrave; un sito di smandrappati che abboccano a qualunque idiozia scriva <a href="http://www.theregister.co.uk/" target="_blank">The Register</a>, ma una prestigiosa organizzazione che spilla milioni di euro a fior di aziende.

Il documento, a diciassette anni di distanza, &egrave; una mistura di ovviet&agrave; da doposcuola e di minchiate. Lo avrebbe potuto scrivere mio fratello, quello che vive benissimo senza computer.

La prima pagina pone cinque domande, alle quali il resto del documento intende rispondere. La seconda domanda &egrave; <cite>How will OS/2 and Macintosh System 7 affect Apple&rsquo;s competitive position?</cite> Come influiranno System 7 e OS/2 sulla competitivit&agrave; di Apple?

Rispetto a System 7 non sapre dire, cos&igrave; sui due piedi. La mia risposta per quanto riguarda OS/2 &egrave; <em>zero, perch&eacute; la sua esistenza non ha cambiato una virgola nell&rsquo;andamento di Apple, e non solo: OS/2 &egrave; <a href="http://www-306.ibm.com/software/os/warp/" target="_blank">sparito nel quasi nulla</a> (salvo qualche banca) ed &egrave; stato spazzato via dall&rsquo;arrivo di un sistema operativo chiamato Windows che, se se ti chiami Gartner Group e vali veramente i soldi che costi, avresti dovuto almeno considerare come possibilit&agrave;, o &egrave; circonvenzione di incapaci.</em>

Puoi scommettere che la risposta dentro il documento &egrave; ben diversa.

Mi sa che ci torner&ograve; su. Quasi quasi ci scrivo un Preferenze.