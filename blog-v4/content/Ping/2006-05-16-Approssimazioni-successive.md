---
title: "Approssimazioni successive"
date: 2006-05-16
draft: false
tags: ["ping"]
---

Gli iBook, ooops, i MacBook, arrivano marted&igrave; scorso&hellip; no, arrivano il 18 con la conferenza di apertura del nuovo Apple Store di New York&hellip; no, arrivano <a href="http://www.apple.com/macbook/" target="_blank">oggi</a>!

Non c&rsquo;&egrave; che dire, questi siti di sedicenti <em>rumor</em> ci azzeccano sempre. Amore, salute, lavoro&hellip;