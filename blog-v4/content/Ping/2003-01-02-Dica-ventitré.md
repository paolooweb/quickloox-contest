---
title: "Dica ventitré"
date: 2003-01-02
draft: false
tags: ["ping"]
---

Nuovi store, grandi schermi e un proposito irrealizzabile per il 2003

Come sai, ho una piccola quota della società che ha aperto il nuovo store milanese di <link>Mac@Work</link>http://www.macatwork.net e, ignorante come sono del commercio al dettaglio (i soci bravi in questo sono altri), in questa veste faccio esperienze sempre nuove.

Due le ho fatte l’ultimo dell’anno: lo store era chiuso ma ugualmente diverse persone chiedevano di entrare, e poi ho visto giocare a Warcraft sopra un Cinema Display Apple da 23 pollici.

Ne ho tratto due conclusioni: primo, che ci sarà anche la crisi, ma se si fanno le cose con passione arriva anche qualche risultato; secondo, che voglio diventare ricco per permettermi un <link>Cinema Display</link>http://www.apple.com/it/displays/acd23/. Spero solo di poter diventare pure abbastanza bravo da non dovermi vergognare di come gioco a <link>Warcraft</link>http://www.warcraft.com/.

<link>Lucio Bragagnolo</link>lux@mac.com