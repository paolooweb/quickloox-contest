---
title: "Banana split<p>"
date: 2005-02-13
draft: false
tags: ["ping"]
---

I fatti e le opinioni in arrivo<p>

Quando il primo marzo il valore delle azioni Apple sembrerà improvvisamente dimezzarsi, sarà per effetto dello split che il consiglio di amministrazione della società ha deciso l&rsquo;altroieri. Ogni azione sarà convertita in due azioni da metà valore ciascuna.<p>

Non cambierà niente se non che il numero delle azioni esistenti raddoppierà.<p>

Eppure posso stare certo che qualcuno arriverà sulle mailing list o nei forum più frequentati a dare la sconvolgente notizia e a chiedersi il perché dell&rsquo;improvvisa discesa.<p>

Banana people.<p>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>