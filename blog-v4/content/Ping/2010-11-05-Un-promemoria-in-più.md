---
title: "Un promemoria in più"
date: 2010-11-05
draft: false
tags: ["ping"]
---

Mentre scrivo mancano poco più di sedici ore alla scadenza della <a href="http://www.mupromo.com/deal/1346/5801?utm_source=feedburner&amp;utm_medium=feed&amp;utm_campaign=Feed%3A+macupdate+%28MacUpdate+-+Mac+OS+X%29" target="_blank">vendita di Pixelmator scontata del 51 percento</a>.

A 29 dollari è un affare assoluto.