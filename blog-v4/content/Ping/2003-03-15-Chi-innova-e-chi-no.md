---
title: "Cinque anni dopo"
date: 2003-03-15
draft: false
tags: ["ping"]
---

Che chi trascina il gruppo e chi sta nel mucchio. Lo si vede dal floppy

Dell Computer, uno dei maggiori produttori al mondo di Pc, smetterà da Aprile di inserire il lettore di floppy disk nella dotazione standard della sua gamma altoprofessionale Dimension.

Conosco un produttore di computer che ha levato il lettore di floppy disk cinque anni fa. Aveva le idee più chiare, o era più coraggioso, o semplicemente trascina il gruppo, invece di stare nel mucchio?

<link>Lucio Bragagnolo</link>lux@mac.com