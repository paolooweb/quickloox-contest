---
title: "Fa le ondine"
date: 2006-05-30
draft: false
tags: ["ping"]
---

Lo aveva chiesto specificamente <strong>Piergiovanni</strong>, ma forse &egrave; di interesse pi&ugrave; generale: s&igrave;, <a href="http://www.apple.com/it/downloads/dashboard/" target="_blank">Dashboard</a> su MacBook fa le ondine al momento di buttare sullo schermo un nuovo widget.