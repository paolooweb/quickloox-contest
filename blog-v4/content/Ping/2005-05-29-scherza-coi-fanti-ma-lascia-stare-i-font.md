---
title: "Scherza coi fanti, ma lascia stare i font<p>"
date: 2005-05-29
draft: false
tags: ["ping"]
---

Ovvero: chi sistema il sistema viene sistemato<p>

Sono a casa di un&rsquo;amica che mi chiede se posso sistemarle il Mac (neanche stasera la collezione di francobolli, ahimé). Ha installato un programma un po&rsquo; cattivello che ora non si lascia cancellare.<p>

Poco male. Apro il Terminale. Ma la finestra del Terminale è piccolissima e impossibile da ridimensionare.<p>

Provo a cancellare le preferenze, a fare un logout, ma non cambia niente. Creo un altro utente e ha lo stesso problema. Quindi non si tratta di un danno fatto nella cartella utente, ma su tutto il sistema.<p>

Guardo nella cartella Font della cartella System e manca quasi tutto, a partire dal Monaco. Che è il font preimpostato del Terminale e, non essendo l&rsquo;amica una tecnica, sicuramente è rimasto quello.<p>

Il sistema è stato aggiornato, quindi effettuare una installazione parziale dai dischi di sistema è impossibile e per un font non vale la pena di fare Archivia e Installa. Allora scarico <a href="http://www.charlessoft.com">Pacifist</a>, prelevo il Monaco dal disco di sistema originale e lo reinstallo.<p>

Logout, login e il Terminale parte che è una bellezza. I file incancellabili vengono cancellati e tutto è risolto.<p>

C&rsquo;è qualcosa che devo sapere? chiede l&rsquo;amica. Sì. La cartella Font in Libreria serve a tutti gli utenti, e ci puoi pasticciare come vuoi. La cartella Font in Inizio/Libreria serve all&rsquo;utente di quella cartella Inizio e, se sei quell&rsquo;utente lì, ci puoi pasticciare come ti pare.<p>

La cartella Font in Sistema/Libreria serve al sistema. Lasciala stare. Per pasticciare con i font, hai altre due cartelle. Dovrebbero bastare.<p>

Lei ha capito. Un&rsquo;altra volta proverò con la collezione di francobolli.<p>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>