---
title: "Non è la somma che fa il totale"
date: 2008-11-03
draft: false
tags: ["ping"]
---

Facile trovare in giro gente che sostiene che Mac è più sicuro di Windows perché i Mac in giro sono pochi.

Stando ai dati sulla quota di mercato, la presenza del Mac è praticamente raddoppiata negli ultimi due anni, ma i virus sono rimasti zero. La quota di Windows è leggermente diminuita, ma i virus continuano ad aumentare.

C’è una falla da qualche parte nel ragionamento. O nei ragionatori.