---
title: "Perché Apple fa cassa"
date: 2010-02-07
draft: false
tags: ["ping"]
---

La disponibilità di cassa di Apple è arrivata quasi <a href="http://www.businessinsider.com/chart-of-the-day-total-cash-and-st-investments-of-tech-companies-2010-2" target="_blank">a equiparare</a> quella di Microsoft.

Nel 1998 Apple predisponeva un piano di sopravvivenza centrato sul superamento dei sei miliardi di dollari l'anno di fatturato. Nel 2009 sono stati <a href="http://www.wolframalpha.com/input/?i=apple+revenues+2009" target="_blank">quarantadue</a>, sette volte tanto. Come si è arrivati a questo?

Per esempio, leggere <a href="http://www.groklaw.net/article.php?story=20100206170554489" target="_blank">queste mail</a> del 2003. La prima è di Bill Gates ai suoi immediati sottoposti, il titolo inizia con <i>Ancora Jobs</i> e si articola cos&#236;:

<cite>L'abilità di Steve Jobs di focalizzarsi su poche cose che contano, attirare le persone che capiscono l'interfaccia utente e commercializzare cose con l'etichetta di rivoluzionarie è sorprendente.</cite>

<cite>Questa volta ha applicato in qualche modo il tuo talento per ottenere accordi di licenza sulla musica più vantaggiosi di chiunque altro.</cite>

<cite>Questo mi giunge molto strano. Le iniziative delle case discografiche offrono un servizio veramente poco amichevole per l'utente.</cite>

<cite>In qualche modo hanno deciso di dare ad Apple la capacità di fare qualcosa di molto buono. [&#8230;]</cite>

<cite>Non sto dicendo che questa stranezza significa che abbiamo lavorato male &#8212; se non altro, se abbiamo lavorato male, vale lo stesso per Real, Pressplay e MusicNet e chiunque altro. [&#8230;]</cite>

<cite>Credo che ci serva un piano per provare che, sebbene Jobs ci abbia preso di sorpresa un'altra volta, ci muoviamo rapidi e facciamo meglio le cose.</cite>

Jim Allchin inviò un'altra mail all'interno di Microsoft, intitolata <i>il Music Store di Apple</i>:

<cite>1. Come hanno convinto le case discografiche?</cite>

<cite>2. Ci hanno vaporizzati.</cite>

Il piano di Gates, insomma, non c'era.

Microsoft ci ha provato nei cellulari, con Windows Mobile. Apple ha varato App Store. E oggi c'è la stessa situazione.

Al Ces di Las Vegas, Steve Ballmer ha presentato per pochi minuti qualche prototipo di tavoletta con Windows, senza programmi, senza novità particolari. Apple ha indetto un evento apposta per parlare un'ora e mezza di iPad, con un iBookstore appena nato e programmi fatti su misura per un nuovo apparecchio.

Si accettano scommesse su come andrà a finire.