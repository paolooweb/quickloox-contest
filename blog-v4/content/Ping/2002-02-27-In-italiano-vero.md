---
title: "In italiano vero"
date: 2002-02-27
draft: false
tags: ["ping"]
---

Chi tiene ad Apple si lamenta con lei (nel modo giusto però). E può farlo in italiano

Apple ha ricevuto più di centomila tra critiche, suggerimenti e richieste su Mac OS X, tramite la pagina di feedback presente sul sito.
Paddy Garcia (come il suo nickname in mailbox; se vuole comparire con nome e cognome e indirizzo basta saperlo) mi segnala che la pagina del feedback ora esiste anche <link>in italiano</link>http://www.apple.com/it/macosx/feedback/.
Come si è già detto, lamentarsi con Apple è la miglior cosa che si possa fare per Apple e per noi, perché Mac OS X cresca anche un po’ come lo preferiamo.
A patto di farlo nel modo giusto: gentilezza, cortesia, positività e precisione.

<link>Lucio Bragagnolo</link>lux@mac.com