---
title: "Divide et impera"
date: 2009-06-30
draft: false
tags: ["ping"]
---

Un'altra novità in italiano, dalla <a href="http://www.apple.com/it/macosx/refinements/enhancements-refinements.html" target="_blank">pagina di Snow Leopard</a> che Apple non ha ancora tradotto.

<b>Nuovo Terminale con suddivisione della finestra</b>

Chi lavora in riga di comando ora può dividere la finestra in sessioni di Terminale differenti, per semplificare il confronto tra i processi in esecuzione.

Il Terminale lo si odia o lo si ama. Comunque, ora, il doppio.