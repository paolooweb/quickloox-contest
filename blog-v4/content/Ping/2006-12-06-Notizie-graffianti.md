---
title: "Notizie graffianti"
date: 2006-12-06
draft: false
tags: ["ping"]
---

Come mi segnala <strong>Carmelo</strong>, c'è un sito che considera improbabile il lancio di Tiger nel corso del prossimo Macworld Expo, a gennaio 2007.

Da un momento all'altro, felino per felino, attendiamo notizie sulla sorte del <a href="http://it.wikipedia.org/wiki/Paradosso_del_gatto_di_Schro%CC%88dinger" target="_blank">gatto di Schr&#246;dinger</a>.