---
title: "Sulle punte di Bluetooth"
date: 2006-08-28
draft: false
tags: ["ping"]
---

Sto tornando e sono riuscito a connettermi a Internet via Bluetooth per oltre una settimana senza riavviare il Mac. Anche se tre o quattro volte ci sono stati balletti niente male, nel disattivare e riattivare Bluetooth sul computer, farlo sul cellulare, riavviare il cellulare e ripetere le tre operazioni in tutte le combinazioni possibili.

Se Leopard facesse la grazia di un supporto <em>rock-solid</em> a Bluetooth, lo considererei molto più di un semplice miglioramento: una versione finalmente definitiva. Ma sospetto che sia il protocollo in sé a essere giovane e, se questo è il caso, non andrà cos&#236; bene.