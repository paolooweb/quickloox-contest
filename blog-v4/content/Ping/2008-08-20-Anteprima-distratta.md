---
title: "Anteprima distratta"
date: 2008-08-20
draft: false
tags: ["ping"]
---

Da qualche tempo ho smesso di aprire GraphicConverter per ridimensionare le immagini. Con un doppio clic sull'immagine parte Anteprima, che lo fa ugualmente.

Scoccia un po' che la finestra informativa di Anteprima aggiorni le dimensioni dell'immagine solo quando viene registrata. Se la ritagli, le dimensioni restano quelle fino a che non registri, il che è disdicevole.