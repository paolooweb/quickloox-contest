---
title: "Perfino l’incompetente"
date: 2002-03-18
draft: false
tags: ["ping"]
---

I miracoli accadono anche nella professione giornalistica

Charles Haddad, columnist che da anni scrive del mondo Mac su BusinessWeek senza mai averne capito niente (di Mac), ha recentemente avanzato l’ipotesi che possa esistere una <link>congiura antiMac</link>http://www.businessweek.com/bwdaily/dnflash/mar2002/nf20020313_1562.htm nel mondo aziendale, condotta da tutti quei tecnici, amministratori di sistema e dirigenti dei reparti informativi che non capiscono niente di Mac e temono, adottandolo, di perdere potere e denaro, perché i Mac sono più facili da usare, si guastano meno e richiedono meno attenzioni.
Per una volta Haddad ha ragione. Dalla nascita di Mac nel 1984 sono passati diciotto anni, ma c’è arrivato anche lui. È una nozione che può capire perfino un incompetente. Spieghiamola.

<link>Lucio Bragagnolo</link>lux@mac.com