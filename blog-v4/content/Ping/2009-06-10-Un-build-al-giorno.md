---
title: "Un build al giorno"
date: 2009-06-10
draft: false
tags: ["ping"]
---

Nel <em>database</em> di supporto Apple è disponibile una tabella che riassume <a href="http://support.apple.com/kb/HT1633" target="_blank">il numero di <em>build</em> per (quasi) tutte le versioni di Mac OS X uscite finora</a>.

Molto utile per chi fa assistenza e per chi ha dubbi sull'effettivo aggiornamento della macchina.

Il numero di <em>build</em>, ricordo, si ottiene scegliendo <em>Info su questo Mac</em> e facendo clic sul numero di versione.