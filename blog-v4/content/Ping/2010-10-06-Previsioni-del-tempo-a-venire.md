---
title: "Previsioni del tempo a venire"
date: 2010-10-06
draft: false
tags: ["ping"]
---

In questa <a href="http://yfrog.com/mlfvkj">vecchia copertina</a> si può vedere come la più autorevole rivista di informatica del 1991 vedeva i portatili di domani.

Stilo a parte (e tutte quelle icone in fondo sanno più di Newton), era il 1991. Byte non scriveva di fantascienza, era <i>il</i> mensile per professionisti.

Chiunque lavori come <i>designer</i> di prodotti in un'azienda diversa da Apple dovrebbe farsi un esamino di coscienza, nell'imminenza del ventesimo anniversario della loro incapacità conclamata.