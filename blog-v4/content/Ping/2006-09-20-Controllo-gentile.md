---
title: "Controllo gentile"
date: 2006-09-20
draft: false
tags: ["ping"]
---

Che bello BBEdit 8.5.

Sulle ali dell'entusiasmo ho abilitato il controllo ortografico in tempo reale, che solitamente mi infastidisce. Per provare.

<a href="http://www.barebones.com/products/bbedit" target="_blank">BBEdit</a> segnala le parole sospette e fin qui fa come tutti gli altri programmi del mondo. Ma la segnalazione è temporanea. Dopo un tot si spegne. Hai tutto il tempo per intervenire sull'ortografia, se serve, e se non ti serve basta ignorarla, che proseguendo nell’editing sparisce e quei segnacci rossi smettono di disturbare.

Ci sono alcuni distinguo da fare e ancora il sistema non è furbo come potrebbe essere (non riconosce le entità Html, per esempio, e alcuni vocaboli restano inspiegabilmente evidenziati). Però, di fronte a certe paginate di word processor che segnalano qualunque cosa e risultano illeggibili a video, è un progresso epocale.