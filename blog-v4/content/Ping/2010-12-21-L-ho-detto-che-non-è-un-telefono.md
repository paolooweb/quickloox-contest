---
title: "L'ho detto che non è un telefono"
date: 2010-12-21
draft: false
tags: ["ping"]
---

iPhone è un computer da tasca.

Chi non ci crede, tiri fuori il suo telefono e <a href="http://www.gizmag.com/ion-torrent-personal-genome-machine-launched/17330/" target="_blank">piloti una macchina per la sequenziazione di Dna</a>.