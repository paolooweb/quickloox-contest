---
title: "Il punto sui pixel"
date: 2009-06-13
draft: false
tags: ["ping"]
---

L'unico a rispondere in qualche modo alla mia domanda sui monitor opachi e lucidi è stato <a href="http://www.macworld.it/blogs/ping/?p=2465&amp;cpage=1#comment-36401" target="_self">Paolo Bertolo</a>, che ha aggiunto una serie di utili considerazioni ad altri titoli e mi ha fornito l'ispirazione per parlare dei pixel morti.

Con Apple bisogna provare a fare la voce grossa, perché non ha una regola di sostituzione e parla solo di <a href="http://support.apple.com/kb/HT1721" target="_blank">un certo numero di pixel</a>.

Fino a che dura, si trova in rete un <a href="http://bayimg.com/eAkmlAaBc" target="_blf">documento confidenziale</a> che però non va oltre il 2007.

Dell ha una regola <a href="http://support.dell.com/support/topics/global.aspx/support/dsn/document?c=us&amp;cs=19&amp;l=en&amp;s=dhs&amp;dn=1018431" target="_blank">aggiornata a marzo</a>.

Hewlett-Packard ha una <a href="http://h10025.www1.hp.com/ewfrf/wc/document?lc=en&amp;cc=us&amp;docname=c00288895&amp;dlc=en" target="_blank">regola sugli schermi</a> e una <a href="http://h10025.www1.hp.com/ewfrf/wc/document?lc=en&amp;dlc=en&amp;cc=us&amp;docname=c00035844&amp;lang=en#" target="_blank">per i portatili</a>.

Asus Australia usa <a href="http://support.asus.com/service/service.aspx?no=536&#38;SLanguage=EN-US" target="_blank">questa regola</a>.

Questa è la <a href="http://www.philipschannel.com/monitors/pdf/pixel_policy.pdf" target="_blank">regola di Philips</a>.

ViewSonic ha una regola di difetti zero valida trenta giorni, che poi <a href="http://www.viewsonic.com.au/kbase/article.php?id=5" target="_blank">cambia</a>.

Lg Australia ha una regola a zero difetti <a href="http://www.el-soft.com/images/LG_pixel_policy.pdf" target="_blank">su alcuni schermi ma non su altri</a>, con durate diverse.

Samsung ha apparentemente una <a href="http://www.samsung.com/my/support/repairpolicy/servicePolicyWarrantyMain.do" target="_blank">regola a zero difetti</a>, ma certamente una <a href="http://www.samsung.com/se/support/repairpolicy/Samsung_Monitor_Pixel_Policy.pdf" target="_blank">serie di eccezioni</a>.

Ho cercato di trovare pagine ufficiali su siti ufficiali, altrimenti ci si perde ben peggio di cos&#236;. Nei forum è scritto tutto e il suo contrario.

La somma di tutti questi link è che, a parte poche eccezioni che durano al massimo un mese, un singolo pixel morto non viene giudicato degno di sostituzione e bisogna farsi valere superando il doppio ostacolo di produttore e rivenditore.

Su una cosa devo però correggere Paolo, quando dice che per via dell'alto costo il modello dovrebbe essere a difetti zero. Il costo del modello è per forza di cose quello la cui produzione rientra nel numero di difetti considerato accettabile; fare uscire monitor a difetti zero, tutti e sempre, avrebbe un costo molto più alto.

Esisteva una vecchia norma Iso che stabiliva, nel 2001, il <a href="http://en.wikipedia.org/wiki/ISO_13406-2" target="_blank">tasso di difetti considerato accettabile</a> per ciascuna classe di schermi Lcd.

Ora questa norma è stata ritirata e sostituita dalla recentemente aggiornata Iso 9241, di cui però non ho ancora acquisito specifiche.

Prima di dare per scontate certe cose sui pixel morti, meglio vederci chiaro.