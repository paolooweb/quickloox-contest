---
title: "300+ novità - Boot Camp"
date: 2007-10-19
draft: false
tags: ["ping"]
---

Tecnicamente Boot Camp e Boot Camp Assistant sono novità, perché le versioni in mano agli utilizzatori di Tiger sono beta. Comunque: Boot Camp su Leopard consente da Mac OS X di copiare, aprire, modificare o cancellare file salvati nella partizione Windows.

La compatibilità con l'hardware è completa: esistono driver Windows certificati per iSight, trackpad e le altre componenti di Mac.

La tastiera viene automaticamente mappata in modo che sia usabile anche se si tratta di una tastiera Apple sotto Windows.

È possibile riavviare in Mac OS X dando il comando dalla Task Bar di Windows.

La funzione migliore di Boot Camp è sicuramente la cancellazione della partizione Windows e il ritorno a un sistema sicuro e affidabile. :-)

Le altre novità di Leopard sono sempre su <a href="http://www.apple.com/it/macosx/features/300.html" target="_blank">una pagina</a> che vado avanti a riassumere fino a quando non viene tradotta in italiano.