---
title: "Dove vanno i soldi dei Vip<p>"
date: 2005-02-27
draft: false
tags: ["ping"]
---

Un buon indicatore per capire quando Apple aprirà i suoi Store in Italia<p>

Ogni tanto parlo di <a href="http://www.macatwork.net">Mac@Work</a> (l&rsquo;Apple Center milanese di cui sono socio) con qualche scettico a oltranza, che fa il sorrisino e spiega che sì, bello, ma entro breve arriveranno gli Apple Store di Apple&hellip;<p>

Lo scettico non sa che cosa fanno i Vip, le Very Important Person. Nel gergo di oggi, quelli che stanno nei media che fanno tanti soldi, e ci stanno dalla parte giusta.<p>

Il Vip fa il suo lavoro e quando ha soldi da investire diversifica, ma sempre con un tasso di rischio zero o pari a zero, perché piuttosto si compra un altro yacht e lo affitta, per dire.<p>

Bene. Uno che conta molto in tivù, e ha contato molto in radio, sta investendo nell&rsquo;apertura di un Apple Center. Un grosso produttore di musica da consumo vuole aprire un negozio di musica digitale, che per tre quarti della sua attività potrà fare poco altro che vendere iPod. Sulle stesse&hellip; note si muovono un paio di cantanti conosciutissimi e sulla cresta dell&rsquo;onda.<p>

Stai tranquillo che, se ci mettono soldi loro, si sono anche informati.<p>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>