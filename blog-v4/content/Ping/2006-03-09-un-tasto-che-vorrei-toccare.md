---
title: "Un tasto che vorrei toccare"
date: 2006-03-09
draft: false
tags: ["ping"]
---

Quando monto una immagine disco nel Finder e la vedo nella barra laterale della finestra, ha di fianco l&rsquo;icona del tasto Espelli.

La stessa icona che si visualizza a pieno schermo se premo Espelli sulla tastiera.

Ma se premo Espelli sulla tastiera, l&rsquo;immagine su disco non viene smontata, anche se mostra quell&rsquo;icona (che funziona solo se cliccata)

Cari designer di interfaccia in Apple: qualcosa non torna.