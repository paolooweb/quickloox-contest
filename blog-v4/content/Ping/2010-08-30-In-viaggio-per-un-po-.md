---
title: "In viaggio per un po'"
date: 2010-08-30
draft: false
tags: ["ping"]
---

Lo so che il primo di settembre Apple terrà un evento in cui appariranno i nuovi iPod e probabilmente anche dell'altro e che quando Apple tiene un evento è d'uopo una bella chiacchierata, magari in chat Irc.

Sfortunatamente, ci sono novantanove probabilità su cento che in quelle ore mi trovi al volante per alcune centinaia di chilometri e impossibilitato alla cosa.

Quindi è per la prossima. Fermo restando che chi vuole è liberissimo di trovarsi comunque su #freesmug presso il server irc.freenode.net. Se riuscirò a essere in rete al momento giusto, apparirò. :-)