---
title: "La moltiplicazione dei giga"
date: 2007-08-20
draft: false
tags: ["ping"]
---

L'hanno scritto, l'ho letto, lo sapevo.

Fa comunque impressione collegarsi a iDisk e vedere che lo spazio libero è passato da 0,3 giga a 9,3 giga.

Per un bel po', niente remore a pubblicare foto su Internet per questioni di spazio. E poi adesso fa anche hosting dei domini personali. C'è l&#236; giusto MacIntelligence.org che dorme&#8230;

Viva <a href="http://www.mac.com" target="_blank">.Mac</a>.