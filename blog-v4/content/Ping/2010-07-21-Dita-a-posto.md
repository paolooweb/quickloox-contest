---
title: "Dita a posto"
date: 2010-07-21
draft: false
tags: ["ping"]
---

Ci sono applicazioni del computer per le quali il personal computer propriamente detto è stato finora niente più che un inefficiente succedaneo.

Prendiamo iElectribe di Korg, stupenda <i>app</i> per iPad che riproduce fedelmente, a 15,99 euro, una rinomata beatbox per musicisti.

Onestamente: tra usarla con le dita, quasi fosse uno strumento vero, e invece usarla con Comando-questo, Control-quello, centra-il-pulsante-con-il-mouse e digita-il-tuo-input-via-tastiera, chi mai avrebbe l'ardire di scegliere la seconda via?

Tra parentesi, fino al 31 luglio iElectribe è in promozione a 7,99 euro. Se sapessi come usarla, l'avrei già presa: l'apparecchio originale, <a href="http://www.macworld.com/appguide/app.html?id=459842&amp;lsrc=rss_main" target="_blank">leggo</a>, costava 300 dollari.