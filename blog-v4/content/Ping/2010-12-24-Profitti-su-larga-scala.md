---
title: "Profitti su larga scala"
date: 2010-12-24
draft: false
tags: ["ping"]
---

Horace Dediu di Asymco ha realizzato <a href="http://www.asymco.com/2010/12/22/a-self-explanatory-chart/" target="_blank">un bel grafico</a> su come si struttura l'attività di Apple e da dove, e come, arrivano i suoi guadagni.

Vendite, costi, spese di gestione, tasse e finalmente profitti sono rettangoli bianchi o colorati che si collocano al giusto livello. Un'occhiata (beh, due) ed è immediatamente chiaro dove stia la ciccia e quanto contino le varie attività.

Il tutto mettendo a confronto autunno 2009 con autunno 2010.

Istruttivo.