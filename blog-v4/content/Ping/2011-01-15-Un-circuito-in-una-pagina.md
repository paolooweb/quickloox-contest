---
title: "Un circuito in una pagina"
date: 2011-01-15
draft: false
tags: ["ping"]
---

Per capire quanto sia cambiata l'informatica personale dai suoi albori possiamo partire dal processore 6502, quello dentro Apple II. Apr&#236; un mondo e un'epoca.

Oggi è una <a href="http://visual6502.org/JSSim/index.html" target="_blank">simulazione JavaScript</a> dentro una pagina di <i>browser</i>.