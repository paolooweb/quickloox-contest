---
title: "Chiacchierata tra amici"
date: 2006-08-07
draft: false
tags: ["ping"]
---

Ho dovuto chiudere un po' in fretta e non ho fatto in tempo a osservare le buone maniere, però voglio ugualmente ringraziare tutti quanti hanno partecipato alla chattata in compagnia durante il keynote della Wwdc. Le osservazioni di tutti sono state preziose per aggiornare meglio il blog&#8230; e per farlo divertendosi.

Usare un Mac è speciale anche perché si incontra gente piacevole. :-)