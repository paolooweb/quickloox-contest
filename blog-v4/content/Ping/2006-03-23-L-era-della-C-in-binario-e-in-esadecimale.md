---
title: "L&rsquo;era della C&hellip; in binario e in esadecimale"
date: 2006-03-23
draft: false
tags: ["ping"]
---

E cos&igrave; si &egrave; finalmente svelato su Macworld cartaceo il piccolo mistero dell&rsquo;era della C (e sono appena appena orgoglioso di avere dato una notizia curiosa ma concreta, di cui assolutamente nessun altro si &egrave; occupato nell&rsquo;Italietta).

Avevo promesso di pubblicare qui i comandi per il Terminale, ed eccoli:

<cite>Per spiare molto da vicino il conteggio dell&rsquo;orologio del Mac in Mac OS X ci viene in aiuto il Terminale. Un &ldquo;semplice&rdquo; script in bash permette di visualizzare i dati in tempo reale. Si esce dall&rsquo;esperienza con Ctrl-C. Se la nostra shell non &egrave; bash e si ribella al comando, prima di eseguire il comando stesso digitiamo sh seguito da Invio. Il comando &egrave;</cite>

<code>while true; do echo "obase=16;`date +%s`+2082844800-6*3600"|bc;sleep 1;done</code>

<code>per vedere il risultato in esadecimale. Per vederlo in binario lo script diventa</code>

<code>while true; do echo "obase=2;`date +%s`+2082844800-6*3600"|bc;sleep 1;done</code>

Suggestivo.
