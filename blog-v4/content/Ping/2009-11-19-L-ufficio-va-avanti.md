---
title: "L'ufficio va avanti"
date: 2009-11-19
draft: false
tags: ["ping"]
---

Venerd&#236; 4 dicembre 2009 sarò in <a href="http://www.atworkgroup.net/" target="_blank">@Work</a> dalle 18 in avanti, per tenere una chiacchierata insieme a Fab sul tema vita senza Word.

Si chiacchiererà liberamente di quando è meglio usare Office e quando, invece, si può fare di meglio e anche risparmiare denaro.

Lo <i>happening</i> prosegue fino circa alle 20-20:30, poi chi è sopravvissuto (e chi ha scelto di evitare) è calorosamente invitato a <a href="http://maps.google.com/maps/place?cid=2128109218154983408&q=via+terraggio+20+milano+italy&hl=en&cd=1&cad=src:pplink&ei=FlUFS9ySLcfFsgbWwoTvAw" target="_blank">Bio Solaire</a>, via Terraggio 20, per una pizzata. La pizzeria è a tre minuti a piedi, consente di non mangiare necessariamente pizza, è vicinissima a metropolitana rossa e verde e a una pletora di mezzi pubblici di superficie, taxi compresi.

Ci sarà gente del <a href="http://www.poc.it" target="_blank">Poc</a>, interessati a Office e non-Office, simpatizzanti, curiosi, nuovi incontri, vecchie e sempre gradite conoscenze e ogni altra variante immaginabile.

Salteranno fuori iPhone, iPod touch, MacBook, MacBook Pro, applicazioni, <i>trailer</i>, discussioni, consigli, amenità e quant'altro.

Vista la data, a qualcuno verrà pure in mente di fare gli auguri di Natale e l'idea non mi dispiace.

Insomma, invito generale. :-)