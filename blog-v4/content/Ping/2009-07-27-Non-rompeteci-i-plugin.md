---
title: "Non rompeteci i plugin"
date: 2009-07-27
draft: false
tags: ["ping"]
---

Probabilmente Apple tradurrà la <a href="http://www.apple.com/it/macosx/refinements/enhancements-refinements.html" target="_blank">pagina delle novità di Snow Leopard</a> al momento dell'annuncio italiano, che ancora non si è avuto. Intanto provvediamo da soli, una novità al giorno.

<b>Resistente ai crash</b>

Safari 4 in Snow Leopard è più resistente ai <i>crash</i>. I <i>plugin</i> sono la causa numero uno dei <i>crash</i> e per questo ora vengono eseguiti dentro un processo a parte. Se un <i>plugin</i> si blocca, il suo contenuto smette di funzionare, ma Safari continua a farlo e basta ricaricare la pagina per ripristinare l'uso del <i>plugin</i>.

Una delle novità per cui non si ringrazierà mai abbastanza. A volte ho aperte anche cinquanta pagine e basta che ne impazzisca una per tirare giù tutto.