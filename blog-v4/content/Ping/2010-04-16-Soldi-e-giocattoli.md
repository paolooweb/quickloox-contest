---
title: "Soldi e giocattoli"
date: 2010-04-16
draft: false
tags: ["ping"]
---

<a href="http://ycombinator.com/" target="_blank">Y Combinator</a> è un fondo di investimento che finanzia nuove aziende con buone idee. <i>Venture capital</i>, capitale di ventura, insomma.

Paul Graham è uno dei <i>partner</i> di Y Combinator. Ecco <a href="http://www.paulgraham.com/organic.html" target="_blank">che cosa scrive</a> nel proprio blog.

<cite>Non fatevi scoraggiare se quello che producete viene inizialmente trattato da altri come un giocattolo. In effetti è un buon segno. È probabilmente il motivo per cui nessun altro ci ha ancora pensato. I primi microcomputer vennero snobbati e considerati giocattoli. E i primi aerei, e le prime automobili. È un momento in cui, quando arriva qualcuno con un'idea che piace a chi la usa e in giro per i forum viene bollata come giocattolo, è particolarmente probabile che ci investiremo.</cite>

Viene in mente niente di recente, stroncato da molti come giocattolo inadatto a scopi professionali, che mancherebbe di un sacco di cose considerate indispensabili da gente di una generazione passata, che piace a chi la usa e che potrebbe avere passato <a href="http://labs.chitika.com/ipad/" target="_blank">novecentomila pezzi venduti</a> in meno di due settimane?