---
title: "Semplice è bello<p>"
date: 2005-09-25
draft: false
tags: ["ping"]
---

I maestri della facilità di utilizzo non dovrebbero snobbare il galateo di Internet<p>

Una volta ignoravo le comunicazioni dirette da Apple, un po&rsquo; perché c&rsquo;è stato un periodo in cui i comunicati stampa li traducevo io e un po&rsquo; perché erano invariabilmente cose su cui ero già aggiornato.<p>

Ora la situazione è cambiata. La newsletter di .mac è piuttosto utile. Così quella per sviluppatori. E se si compra dall&rsquo;iTunes Music Store, anche quest&rsquo;ultima.<p>

Ma sono quasi tutte in Html. Grrr. I messaggi in Html sono più pesanti, comportano rischi per la sicurezza e oltretutto a me interessano i contenuti, non il loro tasso di glamour.<p>

Non pretendo che il mondo debba essere come lo voglio io. In giro incontro continuamente gente che se riceve una mail solo testo, come è giusto galateo di Internet, si sente sminuita.<p>

Benissimo. Però, cara Apple, dammi almeno la possibilità di scegliere senza andare a frugare sul tuo sito alla ricerca delle configurazioni. Basta una frasetta a fine newsletter.<p>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>