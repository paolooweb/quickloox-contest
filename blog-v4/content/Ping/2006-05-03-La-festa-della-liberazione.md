---
title: "La festa della liberazione"
date: 2006-05-03
draft: false
tags: ["ping"]
---

Ho scoperto ora, aiutando un amico, che Microsoft ha cessato il supporto di Explorer per Macintosh a fine anno e dal 31 gennaio non consente neanche pi&ugrave; lo scaricamento del programma.

Limitatamente all&rsquo;Italia, il 25 aprile mi sembrava una data pi&ugrave; adeguata.