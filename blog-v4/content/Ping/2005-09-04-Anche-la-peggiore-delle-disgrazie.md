---
title: "Anche la peggiore delle disgrazie<p>"
date: 2005-09-04
draft: false
tags: ["ping"]
---

Del perché quello che si dice va anche interpretato<p>

Sembra che Bruce Chizen, amministratore delegato di Adobe, abbia fatto uno sgarbo ad Apple annunciando che Photoshop per Mac con processore Intel non sarà disponibile prima di fine 2006 o inizio 2007. Non è così.<p>

Prima ragione, la data coincide con quella probabile della nuova versione di Photoshop già prevista per l&rsquo;epoca e Adobe, cosa buona o cattiva che sia, ha sempre introdotto i cambiamenti in nuove versioni, senza produrre versioni apposta per qualsivoglia ragione.<p>

Seconda ragione, Chizen è salito sul palco della Wwdc ad annunciare il supporto di Adobe in occasione dello storico annuncio di Steve Jobs e gesti come quello non si fanno a cuor leggero.<p>

Terza ragione, più importante di tutte, Photoshop è un programma vecchio di quindici anni, che conta innumerevoli stratificazioni, toppe, scorciatoie, compromessi accumulatisi nel tempo. Per ragioni che non ho modo di spiegare ora, quella versione di Photoshop dovrà subire un lavoro assai profondo di revisione. E potrebbe essere pertanto una delle più efficienti e performanti mai viste finora su un Mac.<p>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>