---
title: "Un'idea dei tempi"
date: 2009-02-06
draft: false
tags: ["ping"]
---

Ovviamente non è vero: anche su un lento PowerBook G4 ci si mette meno di un'ora. Però è divertente vedere come l'algoritmo di stima del tempo necessario all'inizio la spari proprio grossa.