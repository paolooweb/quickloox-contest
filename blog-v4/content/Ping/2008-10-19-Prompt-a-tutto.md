---
title: "Prompt a tutto"
date: 2008-10-19
draft: false
tags: ["ping"]
---

È una pessima giornata. Aspetto per domani l'omino FastWeb (cioè oggi non c'è rete stabile e mi arrangio a chiavetta), ho un <em>backlog</em> di posta siderale e vorrei molto rispondere ad alcune sciocchezze che passano sulle <em>mailing list</em> ma non ho tempo per farlo.

Mi consolo con il delirio di onnipotenza. Scrivo nel Terminale

<code>PS1="comanda, padrone! "</code>

E se non altro mi illudo di essere autorevole almeno per il <em>prompt</em> della mia <em>shell</em>.

(Il cambiamento è solo temporaneo. Per fare cose più complicate e definitive, ci sono sul web innumerevoli guide complete, come <a href="http://www.freebsddiary.org/prompt.php" target="_blank">questa</a> o <a href="http://dougbarton.us/Bash/Bash-prompts.txt" target="_blank">quest'altra</a>).