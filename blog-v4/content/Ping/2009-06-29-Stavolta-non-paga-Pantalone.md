---
title: "Stavolta non paga Pantalone"
date: 2009-06-29
draft: false
tags: ["ping"]
---

Internet è tutta un gioco di link e a forza di navigare uno si mette a linkare, collegare, qualunque cosa.

Per esempio: è notorio che la Commissione Europea ha multato Microsoft per un totale di <a href="http://news.bbc.co.uk/2/hi/business/7266629.stm" target="_blank">oltre un miliardo di euro</a>.

Da poco è noto inoltre che in Europa Windows 7 costerà molto più che negli Stati Uniti: pare che Windows 7 Professional costerà <a href="http://www.computerworld.com/action/article.do?command=viewArticleBasic&amp;articleId=9134914" target="_blank">praticamente il doppio</a>.

Faccio il link e mi chiedo: chi paga veramente le multe inflitte a Microsoft?