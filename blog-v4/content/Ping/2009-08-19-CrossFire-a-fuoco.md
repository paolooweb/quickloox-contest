---
title: "CrossFire a fuoco"
date: 2009-08-19
draft: false
tags: ["ping"]
---

Credo di essere l'unico italiano interessato a CrossFire, praticamente un <i>roguelike</i> giocabile in rete e quindi con altri giocatori.

Cos&#236; mi aggiorno: ho trovato finalmente un eseguibile dove basta fare doppio clic e usare il tempo per capire come si gioca, non come arrivare a un doppio clic.

L'eseguibile si trova alla voce Precompiled Upgrade Package della <a href="http://crossfire.real-time.com/clients/macosx-intel.html" target="_blank">pagina dedicata a Mac OS X Intel</a>. Funziona solo il secondo mirror e, arrivate le tre varianti del client, funziona solo la prima (cfclient). Ma funziona.

C'è una <a href="http://crossfire.real-time.com/clients/macosx-ppc.html" target="_blank">pagina per PowerPC</a>, ma non posso verificarne il funzionamento.

Le interfacce X11 non esagerano mai per amichevolezza. Sembra non funzioni niente, invece bisogna seguire inizialmente la colonna destra, dove ci sono le informazioni per partire.

Adesso lo so&#8230; anch'io.