---
title: "X11 da Terminale<p>"
date: 2005-02-04
draft: false
tags: ["ping"]
---

Un comando c&rsquo;era. Con il senno di poi<p>

I programmi X11 si possono lanciare anche dal Terminale di Mac OS X, usando il comando<p>

<code>/usr/bin/open-x11 /sw/bin/xgalaga</code><p>

dove tutto è fisso fino a xgalaga, che in questo esempio stupido è un clone open source di un glorioso videogioco da bar.<p>

Lo so che bastava andare a guardare per scoprirlo. Sono fatto così.<p>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>