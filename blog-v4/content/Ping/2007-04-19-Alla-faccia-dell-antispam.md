---
title: "Alla faccia dell'antispam"
date: 2007-04-19
draft: false
tags: ["ping"]
---

La <em>signature</em> del nuovo <em>maintainer</em> di <a href="http://angband.rogueforge.net/" target="_blank">Angband</a>, Andrew Sidwell: <cite>la mia email cambia ogni mese e consiste nelle prime tre lettere del mese, in inglese, seguite dalle ultime due cifre dell'anno in corso, @entai.co.uk</cite>.

Lui mi sa che di spam ne becca ben poco. :-)