---
title: "Un venerd&#236; spaziale"
date: 2007-07-09
draft: false
tags: ["ping"]
---

Il gioco del prossimo venerd&#236; del gioco, 13 luglio, è <a href="http://sillysoft.net/games/macosx/" target="_blank">Pax Galaxia di SillySoft</a>, un <em>boardgame</em> ad ambientazione spaziale.

Partita tra le 13 e le 14, stanza iChat <em>gamefriday</em> per coordinarsi.

Il gioco è <em>shareware</em> ma mi sembra si possa almeno iniziare a giocare, anche in rete, senza problemi. Sembra divertente. Io ci provo. :-)