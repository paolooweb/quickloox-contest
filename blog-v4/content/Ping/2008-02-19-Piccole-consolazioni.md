---
title: "Piccole consolazioni"
date: 2008-02-19
draft: false
tags: ["ping"]
---

Sempre senza rete, pascolo dentro Mac OS X 10.5.2 appena installato e trovo il <em>menulet</em> grafico di Time Machine talmente elegante che, nonostante la mia ferma volontà di limitare la proliferazione delle icone sulla barra dei menu, lo lascio l&#236;.

Finché non mi abituo, come minimo.