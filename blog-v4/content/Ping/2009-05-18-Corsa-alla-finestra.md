---
title: "Corsa alla finestra"
date: 2009-05-18
draft: false
tags: ["ping"]
---

Secondo i test Peacekeeper il più veloce nell’elaborazione JavaScript, tra quelli in <em>beta</em>, è Safari 4. Tra quelli ufficiali, è Safari 3.

Crederci o meno? Beh, sulla <a href="http://service.futuremark.com/peacekeeper/browserStatistics.action" target="_blank">pagina dei risultati</a> c’è il pulsante per eseguirli sul nostro Mac.

I creatori di Peacekeeper consigliano di eseguire il test con una sola finestra aperta del <em>browser</em>. Con Safari 3.2.1 e una quarantina di finestre aperte ho fatto 1.436. Con Firefox 3.0.9 e una sola finestra aperta ho fatto 1.082.