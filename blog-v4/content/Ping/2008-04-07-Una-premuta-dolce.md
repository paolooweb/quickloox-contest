---
title: "Una premuta dolce"
date: 2008-04-07
draft: false
tags: ["ping"]
---

Ci voleva un tasto premuto per errore per accorgermi che Safari, con Comando-1, Comando-2 eccetera, carica il <em>bookmark</em> corrispondente che sta sulla barra.

A volte stanno veramente sotto gli occhi e sciocco a non averci provato prima.