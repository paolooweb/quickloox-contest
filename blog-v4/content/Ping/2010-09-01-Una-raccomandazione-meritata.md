---
title: "Una raccomandazione meritata"
date: 2010-09-01
draft: false
tags: ["ping"]
---

Diceva qualcuno che la raccomandazione, se aiuta uno veramente bravo che merita, è doverosa.

A distanza di mesi dal suo debutto mi sento di iniziare a raccomandare gfxCardStatus, programmino che commuta la scheda grafica in uso sui portatili senza bisogno di un <i>logout</i> come invece vorrebbe Apple.

Pensata per i nuovissimi MacBook Pro, gfxCardStatus è arrivata a coprire anche quelli non più tanto giovani, come il mio. E funzionava, la metà delle volte. Il resto era <i>kernel panic</i> e alla fine uno faceva meglio con il <i>logout</i>.

Dopo gli aggiornamenti di prammatica, constato che in agosto ho totalizzato non meno di cinque cambiamenti di scheda grafica andati a buon fine e nessun <i>kernel panic</i>.

L'utilità di una applicazione come questa &#8211; che on the road moltiplica l'autonomia della macchina &#8211; è fuori discussione. Assodato che tre quarti dei miei impegni all'aperto ora sono affidati a iPad, che libera dalla schiavitù dell'alimentazione, con <a href="http://codykrieger.com/gfxCardStatus/" target="_blank">gfxCardStatus</a> riesco a trascorre un pomeriggio di trasferta con il Mac dimenticandomi ugualmente di cavi e alimentatori, che è essere liberati due volte. Bravo l'autore Cody Krieger.