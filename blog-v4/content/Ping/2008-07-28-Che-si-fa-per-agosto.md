---
title: "Che si fa per agosto?"
date: 2008-07-28
draft: false
tags: ["ping"]
---

In realtà io andrò in vacanza a settembre. Ma molti lettori invece lo fanno in agosto e sia loro che gli altri potrebbero avere qualche desidero particolare, vacanziero, e in agosto magari anch'io avrò qualche possibilità in più di sviscerare meglio le cose.

Desideri per Ping di agosto? Più Terminale? Più giochi? Approfondimenti (Ping-<em>style</em>) di qualsivoglia materia? Sono qui ad ascoltare. Per intanto, continuano AppleScript il 7-14-21-28 del mese e il luned&#236; di iPod touch.

Ovviamente buonissime vacanze a tutti. :-)