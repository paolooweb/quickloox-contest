---
title: "Soldi veri e carta carbone"
date: 2004-03-23
draft: false
tags: ["ping"]
---

Quando un programma se li merita

Mi hanno regalato (la fortuna è cieca, ma gli amici sono meglio) un hard disk grande il 150 percento di quello di serie nel mio portatile. Ho effettuato con gioia la sostituzione dell’hard disk e ho messo il vecchio disco in un vecchio case esterno.

Normalmente, quando succede qualcosa del genere, io reinstallo. Meglio metterci più tempo ma avere la certezza di un sistema pulito e snello. Ma avevo una fretta dannata e così ho trasferito tutto con <link>Carbon Copy Cloner</link>http://www.bombich.com/software/ccc.html.

Sul disco vecchio avevo circa 218 mila file che sono stati trasferiti in un’oretta o poco più. Problemi quasi zero. Statisticamente è inevitabile che qualcosa non vada, ma finora ho solo buttato via un paio di file di preferenze, reinstallato un programma (su trenta o quaranta) e tollerato una finestra di dialogo di AppleWorks con qualche problema di grafica.

Sapendo che non poteva fare un lavoro perfetto, Carbon Copy Cloner ha fatto un lavoro eccellente. E ho fatto la mia donazione, in cambio del tempo risparmiato e della comodità. Hai fatto la tua?

<link>Lucio Bragagnolo</link>lux@mac.com