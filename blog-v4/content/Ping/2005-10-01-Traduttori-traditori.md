---
title: "Traduttori traditori<p>"
date: 2005-10-01
draft: false
tags: ["ping"]
---

Come si fa a dare retta alla sostanza, se la forma è distorta?<p>

Ugo Foscolo sbeffeggiava Vincenzo Monti chiamandolo <cite>gran traduttor dei traduttor d&rsquo;Omero</cite>. Praticamente uno che non traduceva dall&rsquo;originale, ma da un&rsquo;altra traduzione. Roba di terza mano, insomma.<p>

Non dirò che sito è, perché non va di moda. È un&rsquo;epoca in cui gli incapaci vengono difesi come mai prima. Forse per solidarietà di casta.<p>

Comunque, è gente che ha avuto il coraggio di tradurre una dichiarazione inglese nel modo seguente:<p>

[il supposto problema degli schermi di iPod nano] <cite>è un problema reale, ma di scarsa rilevanza, che riguarda problemi di qualità del venditore.</cite><p>

L&rsquo;ipotesi è che iPod nano abbia problemi allo schermo. E dipenderebbe dalla qualità del&hellip; venditore. Come dire che forse, se l&rsquo;insegna dell&rsquo;Apple Center non è abbastanza luminosa, gli schermi del nano non funzionano.<p>

In chiaro: <em>vendor</em> non significa venditore. Peccato che non saperlo renda incomprensibile tutto il resto.

C&rsquo;è gente che non sa che cosa scrive. Questi non sanno che cosa leggono ed è peggio.<p>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>