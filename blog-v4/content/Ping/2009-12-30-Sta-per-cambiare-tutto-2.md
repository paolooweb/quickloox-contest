---
title: "Sta per cambiare tutto / 2"
date: 2009-12-30
draft: false
tags: ["ping"]
---

Chi si ricorda i tempi eroici di Internet? Le cose si facevano perché funzionassero su Internet Explorer e, se qualcuno osava chiedere di meglio, che si arrangiasse. I siti peggiori, addirittura, lavoravano in modo che chi non usava Explorer si ritrovasse la porta sbattuta in faccia.

In tanti anni il progresso è stato smisurato.

Prima di tutto si è entrati in una fase migliore, in cui si è capito che un sito deve funzionare in tutti i <i>browser</i>. Internet è un mezzo di comunicazione e questa dovrebbe essere una nozione ovvia. Invece è costata molto tempo e molta fatica.

Stiamo uscendo da questa fase ed entrando in una fase nuova, capitanata da alcuni <i>designer</i> di avanguardia. Che un sito debba funzionare con tutti i <i>browser</i> è pacifico; ma perché deve ridurre il proprio design al minimo comune denominatore e fare in modo che sia tutto uguale su tutti i <i>browser</i>, ovvero adattarsi al <i>browser</i> peggiore e costringere gli altri a stare a quel livello?

Infatti, progettano per il browser migliore possibile, quello che riconosce gli standard e lavora come si deve con i Css. La tipografia avanzata possibile con questi <i>browser</i> non è disponibile per <i>browser</i> mediocri. Su questi ultimi il sito funzionerà comunque; semplicemente non si vedrà la tipografia, o la grafica, avanzata.

Come avrebbe dovuto sempre essere, la responsabilità di usare <i>browser</i> mediocri ricadrà su chi li usa, invece che su chi usa <i>browser</i> migliori, come è stato finora.

Lascio indovinare quale sia il <i>browser</i> più indietro di tutti.

Non convinti? Leggersi gli interventi su <a href="http://24ways.org" target="_blank">24 Ways</a>.

In omaggio una incredibile <a href="http://media.24ways.org/2009/15/space.html" target="_blank">dimostrazione grafica</a>. In alto a destra nella pagina c'è un link al <a href="http://media.24ways.org/2009/15/assets/css/space.css" target="_blank">codice usato per ottenere l'effetto</a>. Tutto e solo Css.

Fa riflettere, vero? Dovrebbe fare riflettere soprattutto gli amanti di Flash (mi fate vedere il codice Flash necessario per fare la stessa cosa?) e di Internet Explorer, <i>browser</i> che si ostina a restare indietro nel supporto dei Css. Avviso: inizia l'epoca in cui saranno loro a pagare la propria arretratezza e non più a gravare sugli altri.