---
title: "Il venerd&#236; del vecchio gioco"
date: 2007-06-11
draft: false
tags: ["ping"]
---

Mi scuso per arrivare tardi nell'annuncio (anche se forse arrivo per un soffio a stare dentro il luned&#236;).

Il gioco del prossimo venerd&#236; è <a href="http://www.leu.it/" target="_blank">Lumen et Umbra</a>.

È un'esperienza difficile e impegnativa. Qualcuno si troverà disorientato. In un'ora non faremo gran che, tranne che chattare un po' da dentro il gioco.

Eppure si fa lo stesso. Uno, perché è un mondo affascinante, anche se il suo fulgore risale a una generazione fa. Due, perché alla luce del keynote di Steve Jobs alla Wwdc, <a href="http://www.ea.com/" target="_blank">Electronic Arts</a> torna a sviluppare per Mac. Aveva cominciato su Mac. E sostenere che i giochi su Mac non ci sono diventa sempre più imbarazzante.

Senza contare che si è visto Jon Carmack di <a href="http://www.idsoftware.com/" target="_blank">id Software</a> salire sul palco. Se abbiamo Quake, è anche perché è esistito Lumen et Umbra.

Chi ha voglia di trovarsi venerd&#236; 15 giugno, dalle 13 alle 14, stanza di ichat <code>gamefriday</code>, arrivi preparato. Il link per calarsi subito nella realtà d'altri tempi di Lumen et Umbra non è quello sopra, ma <a href="telnet://leu.mclink.it:6000" target="_blank">questo</a>.