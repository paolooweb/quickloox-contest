---
title: "Destino tristo e Rio<p>"
date: 2005-08-27
draft: false
tags: ["ping"]
---

La cecità commerciale paga. Chi vede lontano<p>

D&M Holdings, società giapponese con sede a Tokyo, ha annunciato che dalla fine di settembre cesserà di vendere i suoi lettori di Mp3 della serie Rio.<p>

I Rio sono stati i primi lettori Mp3 ad arrivare sul mercato, ben prima di iPod. Sono sempre costati meno di iPod e avevano un sacco di bellissime funzioni in più di iPod, dalla registrazione dell&rsquo;audio ai sintonizzatori radio incorporati.<p>

Ma, avrebbe detto mia nonna materna, erano brutti come il peccato e non erano quello che la gente voleva, ossia un bel lettore facile da usare.<p>

La logica dell&rsquo;assemblato orrendo pieno di tutto e in vendita a prezzo stracciato, che tanti danni fa tra i computer, fuori da quel mercato ha dimostrato di non valere una cicca.<p>

Sulla pagina dove ho letto la notizia c&rsquo;era un&rsquo;inserzione del negozio di musica online eMusic: vende brani Mp3 per tutti i player.<p>

A contorno dell&rsquo;annuncio, la foto di un iPod. Il lettore è quello.<p>

R.I.P, Rio.<p>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>