---
title: "Il sorpasso del vettore"
date: 2009-10-20
draft: false
tags: ["ping"]
---

Per la serie <i>è ancora più vicino il momento di liberarsi di Flash</i>, ecco nascere una libreria di disegno vettoriale in JavaScript, denominata <a href="http://raphaeljs.com/" target="_blank">Rapha&#235;l</a>.

Una libreria è una serie di <i>routine</i>, di pezzi di programma, già pronti da usare; cos&#236; il programmatore che, poniamo, deve disegnare un cerchio non ha bisogno di preoccuparsi di come disegnare il cerchio, ma usa un comando presente dentro l'elenco di Rapha&#235;l, semplicemente personalizzando i parametri che gli servono.

Il sito è ricco di dimostrazioni (visto che è stato argomento di interesse qui, segnalo l'<a href="http://raphaeljs.com/polar-clock.html" target="_blank">orologio polare</a>). Dopo averne viste una manciata, Flash diviene sempre più evidentemente sorpassato.