---
title: "Previsioni del giorno dopo / 2"
date: 2009-01-22
draft: false
tags: ["ping"]
---

Succederà qualcosa di molto brutto al Mac di uno dei ventimila deficienti che hanno scaricato da BitTorrent una copia pirata di iWork '09, per &#8220;risparmiare&#8221;, e si sono ritrovati su disco <a href="http://www.intego.com/news/ism0901.asp" target="_blank">un <em>trojan horse</em></a> aggiunto ai normali pacchetti di installazione, che acquisisce i privilegi assoluti sul sistema e notifica su Internet la propria presenza.

Il giorno dopo Internet sarà piena di commenti sui Mac che non sono più sicuri come una volta.

Un'altra massa di deficienti andrà a comprarsi un <em>firewall</em> da Intego per &#8220;proteggersi&#8221;, quando bastava comprare una copia originale di iWork. E nessuno protegge dagli effetti di un cervello annebbiato.