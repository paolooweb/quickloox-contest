---
title: "300+ novità: Controlli Censura"
date: 2007-11-06
draft: false
tags: ["ping"]
---

Basta andare nel pannello Account, e nelle Preferenze di Sistema, per rendersi conto che esistono i Controlli Censura.

Da l&#236; si può limitare il tempo che i bimbi troppo piccoli per capire possono passare su Internet da soli, in termini di tempo totale e orari periodici (eventualmente diversi da giorni di scuola e giorni di vacanza).

La tecnologia Apple cerca di evitare che dal browser si possano consultare siti, ehm, inappropriati e accetta suggerimenti e aggiunte, in termini positivi (siti ammessi) e negativi (siti vietati). Alla lunga nessun controllo funziona totalmente, però questo è qualcosa.

Se è dura necessità (e si spera non per gusto per ossessione) i Controlli Censura fanno anche la spia e tengono nota dei siti visitati, dei soggetti con cui sono avvenute chat e una trascrizione di ogni chiacchierata.

Tutto questo può essere attivato anche da un Mac diverso inserito nella rete di casa.

C'è anche un controllo per evitare di leggere parolacce dentro Wikipedia (ignoro se si applichi anche all'italiano; Wikipedia italiana peraltro è di una scarsità che altro che le parolacce).

Di tutte le <a href="http://www.apple.com/it/macosx/features/300.html#parentalcontrols" target="_blank">modifiche a Leopard</a>, sinceramente sono le più malinconiche. Parere personale.