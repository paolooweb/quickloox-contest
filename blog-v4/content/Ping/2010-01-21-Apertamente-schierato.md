---
title: "Apertamente schierato"
date: 2010-01-21
draft: false
tags: ["ping"]
---

A qualcuno potrà sembrare che, nel picchiare sempre sullo stesso tasto, io possa schierarmi ostilmente verso Flash.

È del tutto vero.

Ma non si tratta di opinioni. Vedo fatti, traggo conseguenze. Niente ideologia, niente costruzioni mentali, niente cornici etiche. Fatti concreti, riproducibili e verificabili.

Sono entrato ufficialmente nel <a href="http://www.youtube.com/html5" target="_blank">programma beta Html5</a> di YouTube, quello dove i filmati vengono mostrati in Html5 anziché in Flash. YouTube lo apro una volta a semestre, ma è molto più pulito aprirlo in Html puro anziché sopportare un pesante e instabile <i>plugin</i> estraneo agli standard del web.

YouTube non è tutto, si dice?, Beh, Vimeo <a href="http://vimeo.com/blog:268" target="_blank">ha fatto lo stesso</a>.

C'è chi teme di perdere l'animazione e gli effetti speciali. Basta guardare, a riprova del contrario, l'<a href="http://blog.gesteves.com/post/261593774/im-done-star-wars-opening-crawl-using-only-html" target="_blank">ennesimo esempio</a> di risultati eccezionali ottenuti in Html5, questa volta realizzato da Guillermo Esteves.

È roba avantissima, che funziona solo ed esclusivamente su Snow Leopard e sul Safari più aggiornato. In altre parole, in meno di un anno sarà normalità per il web.

Io faccio il tifo per Html5 e prima Flash sparisce, prima son contento.