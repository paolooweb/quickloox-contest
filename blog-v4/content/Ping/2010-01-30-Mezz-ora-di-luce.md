---
title: "Mezz'ora di luce"
date: 2010-01-30
draft: false
tags: ["ping"]
---

Riccardo <a href="http://morrick.wordpress.com/2010/01/30/linked-stevenf-ipad/" target="_blank">ha tradotto in modo eccelso un articolo memorabile</a>, di quelli che illuminano, su iPad&#8230; beh, no. Sull'informatica&#8230; beh, no. Su come va un mondo. Anzi, due.

Totalmente raccomandato. Dura mezz'ora. Se non la spendi, ci hai perso. Se hai quarant'anni, ci hai perso il doppio. Solo un estrattino:

<cite>Quando penso alla fascia d'età delle persone che rientrano nella categoria dell'Informatica del Vecchio Mondo, il grafico è grosso modo una curva a campana con la Generazione X (ehilà!) posizionata all'incirca nel centro. Per me è qualcosa di estremamente affascinante: gli utenti del Vecchio Mondo sono circondati da utenti del Nuovo Mondo che sono sia più giovani che più anziani di loro.</cite>

Leggi anche il resto. La vita è questo, non piangere sulla <i>webcam</i>. Grazie Rick e grazie a <a href="http://www.evkmusic.com/blog.html" target="_blank">Evk</a> che me lo ha segnalato prima che ci arrivassi da me.