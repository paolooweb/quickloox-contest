---
title: "Verdi e verdoni"
date: 2007-07-22
draft: false
tags: ["ping"]
---

Per via di un <em>thread</em> svoltosi sulla mailing list <a href="http://www.maclovers.com" target="_blank">MacLovers</a> ho pubblicato sul web il testo dell'articolo Green Christmas, pubblicato su Macworld Italia di gennaio 2007 e dedicato a Greenpeace.

Finti verdi, cui interessano solo i verdoni.

La pubblicazione è assai grezza, con i link non cliccabili e poca o nessuna impaginazione. Se avrò tempo, magari, abbellirò il file, ma l'importante sono i contenuti.

Per via di un blackout momentaneo di <a href="http://homepage.mac.com/lux/green.html" target="_blank">.Mac</a> ho pubblicato la pagina anche su <a href="http://lvcivs.googlepages.com/green.html" target="_blank">Google Pages</a>.