---
title: "Il dilemma anagrafico"
date: 2007-01-06
draft: false
tags: ["ping"]
---

Dopo che Apple ha aperto il 2007 sulla propria pagina home dichiarando che i (suoi) primi trent'anni erano solamente l'inizio, non so se preferirei avere sei anni, per vedere tutto quello che arriverà, o la mia età, che mi consente di sapere come ci siamo arrivati e che cosa c'è stato.

È un bel dilemma.