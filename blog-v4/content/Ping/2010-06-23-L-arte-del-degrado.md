---
title: "L'arte del degrado"
date: 2010-06-23
draft: false
tags: ["ping"]
---

Per capire perché Flash è un male per il web e gli standard aperti invece sono un bene, non bisogna guardare al futuro.

Invece si prenda un Newton MessagePad 2100, anno 1995.

MessagePad 2100 non conteneva un <i>browser</i> di serie. Tale Eckhart K&#246;ppen ne scrisse uno chiamato <a href="http://40hz.org/Pages/Courier" target="_blank">Courier</a> ed esiste anche <a href="http://communicrossings.com/html/newton/newtscape.htm" target="_blank">Newt's Cape</a> di Steve Weyer.

Adesso, con un apparecchio vecchio di quindici anni, si carichi la <a href="http://www.apple.com/html5/" target="_blank">pagina dimostrativa di Html5 realizzata da Apple</a>.

Naturalmente Courier e Newt's Cape non sanno nulla di Html5, Css, JavaScript; anzi, Courier lavora con il solo testo! Che però è pienamente leggibile.

I progettisti di lingua inglese parlano, tradotti, di <i>degrado elegante</i>: uso le tecnologie più avanzate e so che i <i>browser</i> vecchi non riusciranno a leggere tutto, ma faccio in modo che possano ugualmente visualizzare e usare tutto il codice che sono in grado di capire.

Chi non abbia un MessagePad sottomano (non sa che si perde), <a href="http://www.flickr.com/photos/splorp/sets/72157624225682388/" target="_blank">consulti Flickr</a> e potrà constatare di persona.

Adesso, replicare l'operazione con una pagina Flash.