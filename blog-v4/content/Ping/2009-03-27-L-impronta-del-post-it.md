---
title: "L'impronta del post-It"
date: 2009-03-27
draft: false
tags: ["ping"]
---

I commenti a un Pdf visibili in Anteprima sono una comodità e trovo più che accettabile l'allargamento della finestra del programma per mostrare i post-it virtuali accanto al documento.

Quando chiudo anche l'ultimo commento, però, cavolaccio, la finestra deve tornare alle dimensioni che aveva prima di aprire il primo commento, non fare di testa sua.