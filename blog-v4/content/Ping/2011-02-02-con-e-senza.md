---
title: "Con e senza"
date: 2011-02-02
draft: false
tags: ["ping"]
---

Sono stato accusato di essere cattivo con Flash e quindi sarò equanime nel segnalare due nuove creazioni di Google: <a href="http://juliamap.googlelabs.com/" target="_blank">Julia Map</a>, un calcolatore e disegnatore di frattali per il <i>browser</i>, e <a href="http://www.googleartproject.com/" target="_blank">Art Project</a>, come dire Street View che incontra i principali musei del mondo.

Il secondo usa Flash, come Street View, e promette molto bene: finalmente un progetto di museo virtuale che non sembra ridicolo in partenza.

Tecnicamente però il primo fa impressione: tutto Html5 e JavaScript, compreso il calcolo bruto.