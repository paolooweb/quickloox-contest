---
title: "Scontro tra titani"
date: 2008-01-04
draft: false
tags: ["ping"]
---

Utente della tastiera come sono, ho ben pochi dubbi che il mouse sia più veloce. Capisco la differenza tra il tenersi occupati e attendere e non c'è dubbio che sentirsi occupati fa sembrare il tempo più veloce. Ha ragione <a href="http://www.asktog.com/TOI/toi06KeyboardVMouse1.html" target="_blank">Tog</a>.

Però nella contesa ora si è inserito anche <a href="http://daringfireball.net/2008/01/where_keyboard_shortcuts_win" target="_blank">John Gruber</a> e la cosa si fa spessa. Nonché più interessante.