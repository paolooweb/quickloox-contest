---
title: "Aderisco all'insurrezione"
date: 2009-02-27
draft: false
tags: ["ping"]
---

Normalmente le offerte speciali di software mi interessano poco. I prezzi sono bassi e la probabilità di sprecare tempo, più che denaro, su roba che in fondo non interessa, pure.

Ci sono però eccezioni. C'è ancora un giorno abbondante di tempo per accaparrarsi l'<a href="http://www.ambrosiasw.com/games/ic/" target="_blank">Insurrection Collection</a> di Ambrosia Software: Darwinia, Defcon e Uplink a 19 dollari (normalmente 79 dollari).

Se piace il genere, il prezzo è favoloso e soprattutto i giochi lo meritano più che tutto. Io&#8230; sono già insorto.