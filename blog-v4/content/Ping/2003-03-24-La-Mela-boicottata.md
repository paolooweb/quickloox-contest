---
title: "La Mela boicottata?"
date: 2003-03-24
draft: false
tags: ["ping"]
---

Ascoltiamo pure le anime belle, ma restiamo coerenti

Come è evidente dando retta alle cronache, ci sono le persone pacifiche, che aspirano realmente e fattivamente alla pace, con l’impegno concreto quotidiano su cose piccole e grandi, e i pacifisti-anime-belle, per cui la guerra esiste solo se la fanno “gli americani” e dopo avere esposto la bandiera d’ordinanza si può andare in discoteca a cuor leggero.

In quest’ultimo ambiente ho già sentito gente che “si vergogna di avere un computer made in Usa”e sogna boicottaggi della Mela, probabilmente pensando che George Bush si faccia consigliare le strategie belliche da Steve Jobs.

A me sta anche bene, purché ci sia coerenza. Da domani, per amor di pace, si usino solo Olivetti Prodest.

<link>Lucio Bragagnolo</link>lux@mac.com