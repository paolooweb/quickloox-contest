---
title: "Chi li loda li imbroda"
date: 2006-02-26
draft: false
tags: ["ping"]
---

Ignorante io a fare l&rsquo;elogio di WordPress. Per fare vero giornalismo di inchiesta avrei dovuto scrivere <em>stupido WordPress che converte un 29 febbraio inesistente in un primo marzo giusto senza bloccarmi nel momento in cui sbaglio la data</em>.

Come mi ha spiegato <strong>$imy</strong>, l&rsquo;operazione non &egrave; prerogativa di WordPress, ma &egrave; il comportamento normale della funzione <a href="http://www.php.net/manual/it/function.mktime.php" target="_blank">mktime</a> di Php.

Quindi, qualsiasi sistema di publishing usi Php e usi mktime, &egrave; a posto.

Beh, bravi lo stesso. Quelli di Php.