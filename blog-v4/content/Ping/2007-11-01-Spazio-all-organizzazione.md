---
title: "Spazio all'organizzazione"
date: 2007-11-01
draft: false
tags: ["ping"]
---

Ieri sera prima sessione di <a href="http://www.wizards.com/dnd" target="_blank">Dungeons &#38; Dragons</a> dall'installazione di Leopard e quindi primo debutto di Spaces, per abilitare una scrivania virtuale dedita al gioco lasciando da un'altra parte il lavoro.

L'impatto è stato positivo. Un conto è capire come funziona il tutto, un conto organizzarlo per la vita quotidiana. L'implementazione non è perfetta, ma molto buona. Ho imparato subito a usare F8 per distribuire le finestre nei posti giusti e mi sto riadattando a pensare in termini di documenti anziché di applicazioni, per andare dalla parte giusta subito, il che comporta passare dall'abuso di Comando-Tab all'abuso di F9 (Exposé) per commutare da un programma all'altro senza rischiare di abbandonare lo spazio giusto.

il mio druido è anche passato di livello, ma Leopard non c'entra; il suo <em>animal companion</em> è infatti&#8230; una lupa.