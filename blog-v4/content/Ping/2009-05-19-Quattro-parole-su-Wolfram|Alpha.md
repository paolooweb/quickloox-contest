---
title: "Quattro parole su Wolfram|Alpha"
date: 2009-05-19
draft: false
tags: ["ping"]
---

Mi è stato chiesto che cosa sia veramente il nuovo <cite>motore di conoscenza computazionale</cite> creato dai creatori di Mathematica.

È stato paragonato a Google e naturalmente non c'entra niente, né ambisce a sostituirlo come ha scritto qualcuno.

In quattro parole. Mentre Google è (soprattutto) un cercatore di citazioni, <a href="http://www.wolframalpha.com/" target="_blank">Wolfram|Alpha</a> è l'<a href="http://it.wikipedia.org/wiki/Almanacco_del_giorno_dopo" target="_blank">almanacco del giorno dopo</a>. Ibridato con il <a href="http://it.wikipedia.org/wiki/Manuale_delle_Giovani_Marmotte" target="_blank">Manuale delle Giovani Marmotte</a>.

Prova a scrivere il nome della tua città (per esempio <em>ancona italy</em>) oppure la tua data di nascita (per esempio <em>4 march 1987</em>) per iniziare a capire.

Naturalmente c'è ben di più e pensare di poter disporre gratis di uno strumento cos&#236; fa venire l'acquolina in bocca.

Un esperimento banale e significativo: prova a cercare <em>earthquake italy</em> <a href="http://www.google.com/search?client=safari&amp;rls=en-us&amp;q=earthquake+italy&amp;ie=UTF-8&amp;oe=UTF-8" target="_blank">in Google</a> e poi <a href="http://www.wolframalpha.com/input/?i=earthquake+italy" target="_blank">su Wolfram|Alpha</a>. Il secondo risultato è decisamente più vicino a qualcosa di utile.