---
title: "C'è del Mac in Danimarca"
date: 2009-12-09
draft: false
tags: ["ping"]
---

Tempo fa si era richiesto di parlare più spesso dell'adozione di Mac da parte delle istituzioni e delle grandi aziende.

Beh, ItPro <a href="http://www.itpro.co.uk/618529/copenhagen-police-turn-to-macs" target="_blank">fa sapere</a> che la polizia danese fa marciare il proprio comando centrale con un sistema di gestione chiamate composto da 25 Mac Pro e 73 Mac mini.

A contatto con gli operativi in sala comando stanno quattordici stazioni di lavoro, dove da sei a otto persone secondo i turni amministrano da 800 a 1.200 chiamate di emergenza (il nostro 113).

Stando all'articolista e alle dichiarazioni che riporta, il sistema permette alle forze danesi di ottenere lo stesso livello di efficienza di altre polizie europee con un terzo degli effettivi, per non parlare di casi limite di inefficienza come quello della scozzese Glasgow, dove per un quantitativo equivalente di chiamate servono cinquanta poliziotti.

L'articolo, che mostra anche alcune foto, riporta che i prezzi di acquisto dei Mac sono stati praticamente gli stessi rispetto a Pc equivalenti, e che i sistemi precedenti, con Windows, non riuscivano a stare dietro alle chiamate, tanto che gli operatori dovevano integrare i computer con carta e penna.

Ambasciator non porta pena; questo è quanto testimonia &#8211; più estesamente &#8211; l'articolo. E non credo che i danesi siano tutti impazziti; l'ultimo che ricordo è <a href="http://shakespeare.mit.edu/hamlet/full.html" target="_blank">Amleto</a> e anche lui faceva solo finta.