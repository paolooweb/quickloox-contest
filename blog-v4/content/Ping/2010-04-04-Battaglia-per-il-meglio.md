---
title: "Battaglia per il meglio"
date: 2010-04-04
draft: false
tags: ["ping"]
---

Dopo oltre un anno di sviluppo, <a href="http://www.wesnoth.org" target="_blank">Battle for Wesnoth</a> è passato dalla versione 1.6 alla 1.8 ed è più raccomandabile che mai.

Perfetto per famiglie ma tutt'altro che bambinesco, ci si può giocare per cinque minuti ma condurre campagne di ore e ore, coinvolgente da soli e fantastico in gruppo (in loco o in rete), giocabile senza impegno per quanto ottimizzare la strategia è estremamente impegnativo.

Si possono anche esaminare i cambiamenti fondamentali <a href="http://www.wesnoth.org/start/1.8/index.it.html" target="_blank">in (migliorabile) italiano</a>.

C'è una campagna intera tutta nuova, ancora più musica, intelligenza artificiale più smaliziata e le campagne esistenti continuano a essere rifinite.

E poi i <i>bug fix</i>, le rifiniture a grafica e interfaccia, le nuove unità eccetera eccetera.

Dargli una possibilità è irrinunciabile. Era un buonissimo gioco, ora è ulteriormente migliorato.