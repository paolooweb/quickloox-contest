---
title: "Viva i disorganizzati"
date: 2002-02-21
draft: false
tags: ["ping"]
---

Dovrebbe esserci una religione a vietare di creare programmi che non possono essere spostati

Molto soddisfacente che sia uscito un ennesimo aggiornamento di Mac OS X, 10.1.3. Arrivano nuovi driver, le cose funzionano meglio e più velocemente, si uccidono bug piccoli e grandi.
Meno soddisfacente è che l’update interessi anche alcuni programmi e che quei programmi debbano trovarsi per forza in una certa cartella, pena il ritrovarsi con due versioni del programma, una vecchia e una nuova, in posti differenti.
Le persone organizzate non spostano mai le applicazioni e, invece, creano alias; ma Apple si è fatta una fama proprio perché per la prima volta il computer potevano usarlo anche i disorganizzati.
Non è impossibile aggiornare un programma dovunque esso si trovi; su Mac OS 9 lo si fa tranquillamente. Che lo faccia anche Mac OS X; è o non è un passo in avanti?
A nome dei disorganizzati di tutto il mondo,

<link>Lucio Bragagnolo</link>lux@mac.com