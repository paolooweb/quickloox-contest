---
title: "Le previsioni del giorno dopo / 1"
date: 2009-01-10
draft: false
tags: ["ping"]
---

Le più difficili. Mica per niente i siti-truffa pubblicano le previsioni <em>prima</em> del <em>keynote</em>, quando si può dire qualsiasi scemenza e i babbei cliccano a più non posso.

Prima previsione: dopo avere visto il nuovo MacBook Pro 17&#8221; (io devo solo decidere da chi acquistare e se lo schermo sarà lucido oppure opaco), c'è chi ha dedotto la fine di FireWire. Prevedo che sia totalmente falso; semmai è un preludio all'arrivo di FireWire 1600.