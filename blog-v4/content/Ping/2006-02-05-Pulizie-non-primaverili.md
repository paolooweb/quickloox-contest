---
title: "Pulizie non primaverili"
date: 2006-02-05
draft: false
tags: ["ping"]
---

Pi&ugrave; o meno una volta al mese do una pulita allo schermo del PowerBook e l&rsquo;ho fatto anche questo mese. Panno in microfibra, acqua corrente, una bella passata ed ecco fatto.

Intanto si &egrave; reso necessario riesumare una vecchia tastiera esterna, sporca e dimenticata. Per rimuovere lo sporco superficiale un comune puliscivetro ha funzionato ottimamente. E per l&rsquo;interno? Idea alternativa: l&rsquo;aspirapolvere di casa. Con davanti una spazzola molto fitta, l&rsquo;ideale per smuovere tutte quelle cose terribili che si nascondono sotto i tasti.

E i nemici dell&rsquo;igiene sono sconfitti. :-)