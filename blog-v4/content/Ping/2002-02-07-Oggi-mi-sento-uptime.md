---
title: "Oggi mi sento up(time)"
date: 2002-02-07
draft: false
tags: ["ping"]
---

Mac OS X deve migliorare su tante cose, ma su una si mangia a colazione qualsiasi Windows

Uso un Macintosh portatile, con cui lavoro come consulente e divulgatore per almeno cinque soggetti diversi, ognuno connesso a Internet alla sua maniera (e poi c’è la mia Isdn di casa). Spesso passo alla tastiera ben più delle otto ore giornaliere canoniche. Spesso uso il computer anche per svago e passatempo, quindi anche la sera e nei fine settimana. Provo tutto il software che trovo interessante e quotidianamente installo, disinstallo, configuro, pasticcio su qualcosa. Faccio anche pasticci, sbaglio, cancello, recupero, insomma, mi complico la vita.
Oggi ho aperto il Terminale di Mac OS X e ho scritto “uptime”, che è come chiedere “da quanto tempo è ininterrottamente in funzione questo sistema?”
La risposta è stata 22 giorni, 23 ore e 22 minuti.
Oramai, quando discuto con un Windowsista, mi limito a chiedergli “Quando hai riavviato l’ultima volta?”. E mi sento subito meglio.

<link>Lucio Bragagnolo</link>lux@mac.com