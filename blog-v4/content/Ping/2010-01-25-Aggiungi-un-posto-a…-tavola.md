---
title: "Aggiungi un posto a&#8230; tavola"
date: 2010-01-25
draft: false
tags: ["ping"]
---

Sollecitato da <b>Ludomaster</b>, con <a href="http://freesmug.org" target="_blank">Gand</a> più che disponibile a ospitare, che manca? Annuncio ufficialmente la chat Irc in occasione degli annunci Apple di mercoled&#236; 27.

Le coordinate sono sempre le stesse, già enunciate <a href="http://www.macworld.it/2008/12/24/allenamento-chat-2/" target="_blank">con tanto di istruzioni</a> in occasioni passate: chat Irc, server <code>irc.freenode.net</code>, canale <code>#freesmug</code>.

Aprirò una chat buttadentro su iChat, a nome <code>ilife2010</code>. Chi avesse problemi con Irc potrà passare al volo a chiedere consigli.

L'evento comincia alle nove di Cupertino, quindi alle 18 italiane. Poi si va avanti, stile Ligabue, <cite>fin quando ce n'è</cite>.

Giusto un avviso: i pronostici andavano fatti sei mesi fa. Oggi vedo gente che si lancia in illuminate &#8220;previsioni&#8221; e fa solo ridere. A leggere <a href="http://blog.flurry.com/bid/30019/Apple-Tablet-The-Second-Stage-Media-Booster-Rocket" target="_blank">Flurry</a> sono buoni tutti.