---
title: "Questione di gusti"
date: 2009-03-28
draft: false
tags: ["ping"]
---

I due Cd originali di <em>Zeit</em>, album dei primi Tangerine Dream, non vengono accettati dal lettore del MacBook Pro.

Sono anche gli unici. Sto ripercorrendo la raccolta dei Cd per vedere se aggiungere altra musica a iTunes, ora che il disco rigido è più capiente, e sono passati decine di Cd senza problemi. Non è neanche un guasto, perché dopo Zeit ho passato numerosi altri Cd e il lettore li accetta tranquillamente.

Ho notato che i due Cd di Zeit hanno il bordo esterno dallo spessore squadrato, quasi tagliente. Gli altri Cd audio commerciali tendono ad avere un bordo arrotondato e forse quella è la causa.

O forse i MacBook Pro hanno un pregiudizio verso l'elettronica eccessivamente sperimentale.