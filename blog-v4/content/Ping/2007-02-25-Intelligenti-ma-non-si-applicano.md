---
title: "Intelligenti, ma non si applicano"
date: 2007-02-25
draft: false
tags: ["ping"]
---

F. pirata Photoshop <cite>per avere i filtri</cite>.

Non pretendo The Gimp, ché X11 è una brutta bestia. O <a href="http://www.lemkesoft.de" target="_blank">Graphic Converter</a>, una delle decine di applicazioni più che adeguate allo scopo. Ma almeno <a href="http://developer.apple.com/macosx/coreimage.html" target="_blank">Core Image Fun House</a>, che è compreso nel prezzo di Mac OS X.