---
title: "Un’azienda che mantiene quello che premette"
date: 2003-03-13
draft: false
tags: ["ping"]
---

Apple non è uguale a Dell e i Mac non sono uguali a Windows

Apple deve investire molto perché il pubblico scopra i vantaggi di Mac; il 95% del mercato non rifiuta Macintosh per comprare Windows, ma compra Windows perché non ha neanche l’idea che esista l’alternativa Mac.

Apple deve investire molto in ricerca e sviluppo per inventare, anticipare, sorprendere. Dell – o qualsiasi altro costruttore di Pc – spende in ricerca e sviluppo praticamente zero. Non inventa, non anticipa, non sorprende.

Apple investe da sempre nel design e nell’interfaccia, perché i Mac siano belli da vedere, semplici da usare e ci facciano sentire anche un po’ speciali. Dell, e quelli come Dell, producono perlopiù scatole squadrate, brutte, noiose e ordinarie.

Apple, nonostante quello che spende per fare prodotti migliori, alla fine vende Mac che costano praticamente come la concorrenza (non parlarmi degli assemblati: paghi le arance all’angolo della strada come quelle del supermercato?) e sul medio termine (diciamo tre anni) offre vita più lunga e costo totale di possesso minore.

Insomma, Apple è un’azienda che si muove secondo una logica commerciale e commette i suoi bravi sbagli. Ma nel prato dell’informatica è un fiore unico e prezioso, quanto meno da rispettare. Perché mantiene le sue premesse.

<link>Lucio Bragagnolo</link>lux@mac.com