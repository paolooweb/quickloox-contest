---
title: "Straordinari al museo"
date: 2006-09-09
draft: false
tags: ["ping"]
---

Durante le vacanze a Quiliano si devono essere riposati, ma il cervello dei fondatori dell'<strong>All About Apple Museum</strong> è rimasto acceso.

Da quando sono tornati <a href="http://www.allaboutapple.com/present/parlano_di_noi.htm" target="_blank">Ingrandiscono il sito</a>, <a href="mailto:alessio@allaboutapple.com">vendono G5 superaccessoriati</a> per professionisti che attendono il completamento della transizione del software a Universal, <a href="http://www.allaboutapple.com/webcam.htm" target="_blank">impiantano una webcam</a> nel museo in modo che si possa vedere tutti che cosa accade e chissà che diavolo stanno inventando di altro.

E poi i Mac sarebbero computer come tutti gli altri. A vedere chi se ne occupa, sono oggetti straordinari.