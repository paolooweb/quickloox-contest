---
title: "L'antiphishing dei poveri"
date: 2009-11-16
draft: false
tags: ["ping"]
---

Mi si <a href="http://www.macworld.it/blogs/ping/?p=3041&amp;cpage=1#comment-37686" target="_self">chiedeva</a> che vantaggi ha <a href="http://www.mailsmith.org" target="_blank">Mailsmith</a> come programma di posta rispetto agli altri.

Un vantaggio notevole è che Mailsmith mostra la scellerata posta Html (inutile e sprecona di spazio) come allegato e gestisce solo il testo.

Oggi mi è arrivata una mail in formato Html che comincia con:


Gentile Cliente, la informiamo che e'disponibile on-line il suo estratto conto&#8230;

e mostra un <i>link</i> a <a href="http://michael-dummel.de/Seiten/index.php" target="_blank">https://titolari.cartasi.it</a>.

Mailsmith, mostrando la parte testo, mi fa leggere:

la informiamo che e'disponibile on-line "https://titolari.cartasi.it <http://michael-dummel.de/Seiten/index.php>" il suo estratto conto&#8230;

Posso vedere istantaneamente che il <i>link</i> non porta dove dice, ma a un sito dall'indirizzo del tutto improbabile, il che mi porta a sospettare di <i>phishing</i> e a classificare il messaggio come posta indesiderata.

Hai provato a cliccare il <i>link</i> a CartaS&#236;? Scommetto che non ti sei accorto subito che non porta dove dice di portare. Invece, con Mailsmith, è evidentissimo.