---
title: "Bugie, grandi bugie e Bill Gates"
date: 2007-02-07
draft: false
tags: ["ping"]
---

<strong>Elio</strong> mi manda questa mail:

<cite>Seduto davanti alla tv con iBook in braccio, passa lo spot di Vista e per magia (o maledizione) Safari si chiude&#8230; maledetta Redmond!</cite>

<cite>Che abbiano inventato un virus che agisce attraverso il teleschermo??? Wow! proprio bbravi!</cite>

Nello scherzare, una grande verità. Windows è il sistema operativo più vulnerabile e bacato che ci sia dato conoscere.

Ora, <a href="http://www.msnbc.msn.com/id/16934083/site/newsweek/page/1/" target="_blank">Bill Gates a Newsweek</a>:

<cite>Oggigiorno, gli esperti di sicurezza bucano il Mac ogni giorno. Ogni giorno, vengono fuori con un exploit totale, la tua macchina può venire totalmente controllata. Sfido chiunque a fare questo una volta al mese sulla macchina Windows.</cite>

E gli credono.

Qualcuno informi la gente che il <a href="http://isc.sans.org/survivaltime.html" target="_blank">tempo medio di sopravvivenza</a> di un Pc Windows lasciato incustodito su Internet è di cinque minuti.