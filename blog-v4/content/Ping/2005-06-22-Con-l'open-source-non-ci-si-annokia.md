---
title: "Con l&rsquo;open source non ci si annokia<p>"
date: 2005-06-22
draft: false
tags: ["ping"]
---

Senza il coraggio di scegliere la comunità non sarebbe mai successo<p>

Ormai non è più una novità, che Nokia abbia deciso di realizzare un browser per i suoi smartphone Serie 60 usando WebCore e JavaScriptCore, le tecnologie usate da Apple a livello di sistema in Mac OS X per Safari e per tutto il trattamento dell&rsquo;Html dove serve.<p>

Per Apple è una bella notizia, che arriva grazie alla decisione di pubblicare come open source WebKit, che è l&rsquo;insieme delle tecnologie di cui sopra. Vuol dire che chiunque può visionare il codice e cambiarlo come vuole, a patto di condividere con il resto del mondo i cambiamenti effettuati, in modo che tutti possano goderne.<p>

È un sistema che se bene applicato vuol dire miglioramento continuo. Miglioramento che un&rsquo;azienda attaccata spasmodicamente ai suoi segreti non può ottenere, lasciando il suo software inefficiente e insicuro. Devo fare nomi?<p>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>