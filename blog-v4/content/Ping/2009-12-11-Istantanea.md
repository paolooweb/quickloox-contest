---
title: "Istantanea"
date: 2009-12-11
draft: false
tags: ["ping"]
---

Giochino che facevo tempo fa: in questo momento girano sul mio MacBook Pro Finder, <a href="http://www.vienna-rss.org/vienna2.php" target="_blank">Vienna</a>, <a href="http://rephial.org" target="_blank">Angband</a>, <a href="http://www.skype.com/intl/it" target="_blank">Skype</a>, iChat, <a href="http://www.apple.com/it/safari/" target="_blank">Safari</a>, <a href="http://www.mailsmith.org/" target="_blank">Mailsmith</a>, <a href="http://c-command.com/spamsieve/" target="_blank">SpamSieve</a>, <a href="http://adium.im/" target="_blank">Adium</a>, Terminale, Anteprima, <a href="http://pcgen.sourceforge.net" target="_blank">PcGen</a>, <a href="http://flock.com/" target="_blank">Flock</a>, <a href="http://www.barebones.com/products/bbedit/" target="_blank">BBEdit</a>, <a href="http://www.apple.com/it/iwork/numbers/" target="_blank">Numbers</a>, Rubrica Indirizzi, <a href="http://freemat.sourceforge.net/" target="_blank">FreeMat</a>, <a href="http://minivmac.sourceforge.net/" target="_blank">Mini vMac</a>, <a href="http://www.openoffice.org/" target="_blank">OpenOffice.org</a>, <a href="http://www.apple.com/it/itunes/" target="_blank">iTunes</a> e <a href="http://www.adobe.com/it/products/indesign/?promoid=BPBPT" target="_blank">InDesign</a>.

Per rendere il giochino più interessante di allora, aggiungo che ho occupato praticamente tutta la Ram (Monitoraggio Attività indica 7,97 gigabyte su otto di memoria impegnata). Gli unici programmi commerciali sono Numbers e InDesign e il resto è tutto di serie su Mac, o <i>freeware</i>, o <i>open source</i>.

Ventuno programmi, una discreta sollecitazione per il Mac. Tuttavia, almeno in linea di principio, c'è veramente poco che non potrei fare senza bisogno di aprire un altro programma.

L'uptime è insignificante, poco più di un giorno. &#8220;Colpa&#8221; dell'aggiornamento di <a href="http://support.apple.com/kb/DL975" target="_blank">firmware</a> e <a href="http://support.apple.com/kb/DL892" target="_blank">SuperDrive</a>.