---
title: "Scambi di esperienze"
date: 2008-04-02
draft: false
tags: ["ping"]
---

Macworld mi ha chiesto che cosa mi piace di più di Leopard, che cosa non uso e che cosa mi piace meno.

Ho risposto che amo alla follia QuickLook e Time Machine. Mi piacciono le stampanti nel Dock e il lavoro fatto con il Terminale per farlo diventare un'applicazione Apple. Non uso assolutamente Cover Flow, né gli <em>stack</em>, né il Dock 3D.

Però i cambiamenti introdotti da Leopard sono tali e tanti che essere esaustivi è difficile. Può darsi che ci siano cose di cui neanche mi sono reso conto.

Com'è la tua esperienza?