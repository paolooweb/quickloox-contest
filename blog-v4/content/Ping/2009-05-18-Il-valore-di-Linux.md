---
title: "Il valore di Linux"
date: 2009-05-18
draft: false
tags: ["ping"]
---

Un programmatore russo ha deciso di elencare i motivi, motivi tecnici e non di interfaccia, per i quali <a href="http://linuxfonts.narod.ru/why.linux.is.not.ready.for.the.desktop.html" target="_blank">Linux non ha ancora sfondato tra i desktop</a>.

Ci tiene a precisare che è solo un elenco di difetti, e che Linux ha anche grandi punti di superiorità verso gli altri sistemi operativi. Tuttavia è interessante vedere alcuni punti e pensare a come è fatto Mac OS X, l'unico sistema operativo di grande diffusione oltre a Linux a venire scelto nella gran parte dei casi (due terzi di chi usa Windows non lo ha scelto, se lo trova l&#236; e basta):

<cite>Mancanza di un sistema audio affidabile.</cite>

<cite>Mancanza di buone interfacce software per sviluppare applicazioni grafiche.</cite>

<cite>Problemi vari con l'antialiasing dei font.</cite>

<cite>Mancanza di sistemi unificati di configurazione e distribuzione del software tra le varie versioni.</cite>

<cite>Troppe situazioni non sono configurabili dall'interfaccia grafica.</cite>

<cite>Abbondanza di bug in tutte le applicazioni.</cite>

<cite>Lentezza.</cite>

<cite>Pessima compatibilità in avanti e all'indietro.</cite>

C'è molto altro e alcune cose sono campate per aria (per esempio: non poter vedere un film Blu-ray a me pare un vantaggio più che un problema). Complessivamente l'elenco è però sensato e tocca punti sensibili.

Apple è criticabile su molti punti ma il lavoro che c'è sotto Mac OS X è, sempre complessivamente, ammirevole. A ben pensarci i due sistemi si sono evoluti quasi in parallelo. Dentro Apple, però, c'è un patrimonio di conoscenze e di talento che altrove non si è ancora accumulato. E che ha valore per tutti.

