---
title: "A .Net con il nemico"
date: 2007-03-03
draft: false
tags: ["ping"]
---

Grazie ad <strong>Alan Dragster</strong> per la segnalazione di <a href="http://www.mono-project.com/Main_Page" target="_blank">Mono</a>. Quando me lo ha indicato gli ho chiesto <cite>che ci faccio?</cite> Lui, lucido come sempre, mi ha risposto <cite>per creare cose .Net senza bisogno di installare Windows</cite>.

A volte bisogna sporcarsi le mani. Ma grazie all'open source si può restare ugualmente puliti.