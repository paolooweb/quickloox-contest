---
title: "Apple non sta per fallire"
date: 2003-01-16
draft: false
tags: ["ping"]
---

L’unica azienda al mondo su cui si è allarmisti senza motivo

Nel primo trimestre fiscale dell’anno scorso Apple aveva guadagnato 11 milioni di dollari.

Nel primo trimestre fiscale di quest’anno Apple avrebbe guadagnato 11 milioni dollari. Solo che ha affrontato alcune spese straordinarie di ristrutturazione che hanno portato a una perdita di 8 milioni di dollari.

Quando Apple va in rosso immancabilmente si scatena un coro di catastrofisti e un mare di gente inizia a pensare angosciata al destino del proprio Mac, come se da un momento all’altro arrivasse la polizia a requisirlo.

Un solo argomento: Apple ha in cassa 4.400 milioni di dollari. Una banale calcolatrice consente di stabilire che l’azienda potrebbe andare avanti a perdere otto milioni di dollari a trimestre e ripianare la perdita con denaro contante per 4.400 / 8 = 550 trimestri, ovvero 137,5 anni.

Chi ha voglia di preoccuparsi inutilmente, lo faccia per i pronipoti.

<link>Lucio Bragagnolo</link>lux@mac.com