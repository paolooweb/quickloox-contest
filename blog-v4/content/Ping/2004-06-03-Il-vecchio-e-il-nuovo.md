---
title: "Nuovi System, vecchi programmi"
date: 2004-06-03
draft: false
tags: ["ping"]
---

Mai tornare al software di prima, o almeno farlo stando attenti

Se fossi il matematico Pierre de Fermat potrei fare una battuta sull’avere una bellissima spiegazione sul perché sostituire componenti nuovi con componenti vecchi di Mac OS X mette a rischio la riuscita degli aggiornamenti software, ma non ho spazio qui per pubblicarla.

Essendo solo un mestierante ti suggerisco di tenere nota dei componenti che hai sostituito e poi una delle due: o reinserisci tutti i nuovi componenti e riavvii appena prima di effettuare l’aggiornamento, oppure evita Aggiornamento Software e utilizza, invece, gli aggiornamenti combo da scaricare manualmente.

Fermo restando che sarebbe meglio astenersi dallo smanettare nel sistema in questo modo. :-)

<link>Lucio Bragagnolo</link>lux@mac.com