---
title: "Pre-giudizi"
date: 2007-02-23
draft: false
tags: ["ping"]
---

<cite>Il Mac è bello, ma ho bisogno del <a href="http://hpc.sourceforge.net/" target="_blank">Fortran</a> e c'è solo per Windows.</cite>

Giuro che mi è stato detto esattamente cos&#236;.

Lancio subito il concorso: qual è il motivo più idiota che hai sentito come giustificazione per non usare Mac?