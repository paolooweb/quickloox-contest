---
title: "E giunto al fin della licenza"
date: 2008-04-01
draft: false
tags: ["ping"]
---

Terminare in questo giorno mi sembra la migliore cosa possibile. Dopotutto è pur sempre il trentaduesimo compleanno di Apple; ieri è stato il decimo di Mozilla e per loro ci sono un sacco di buonissime ragioni per continuare.

Io ne ho per smettere.

Questo blog è nato quando ancora nessuno aveva coniato la parola blog. È passato attraverso migliaia di post pubblicati, decine di migliaia di commenti, infiniti link e (spero) spunti, di divertimento e di riflessione.

È giusto passare la mano e WordPress ad altri, che saranno certamente più bravi di me.

Ping termina qui e sono felice di poterlo fare salutando cos&#236; tanti amici.

Ci rivedremo, naturalmente. Ho altri progetti, non perché questo non sia buono, ma perché le idee sono sempre tante e ogni tanto bisogna avventurarsi su qualche strada nuova.

Arrivederci a prestissimo su un nuovo sito. Non sarà Ping ma qualcosa di molto simile, che mi frulla nel cervello da un bel po'. Sarà dedicato alla natura, in special modo alle conifere.

E finalmente mia mamma avrà avuto ragione, quando per l'ennesima volta, mi dirà che ho le Pign In Testa.