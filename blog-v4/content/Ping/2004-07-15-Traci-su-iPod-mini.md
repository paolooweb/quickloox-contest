---
title: "Quale Traci sarà?"
date: 2004-07-15
draft: false
tags: ["ping"]
---

Speriamo che Steve Jobs non legga le prossime righe

Non vorrei mai causare un licenziamento in Apple, ma va raccontata. A Milano c’è stata da poco una bella ed elegante presentazione degli iPod mini e, durante la proiezione delle slide, ne è passata una con un iPod mini che mostrava la possibilità di incidere una dedica sull’apparecchio, così come si può fare sull’iPod.

L’iPod mini colorato, sulla slide, metteva in ombra la dedica e solo pochi hanno così potuto leggere

Thank you Traci, for all the hard work

Non chiedermi di spiegare perché non è una dedica qualsiasi. :-)

<link>Lucio Bragagnolo</link>lux@mac.com