---
title: "Una beta speciale<p>"
date: 2005-03-24
draft: false
tags: ["ping"]
---

Un programma strategico per Mac alla vigilia di una major release<p>

Il bravissimo Carlo Gandolfi mi ha aggiornato sullo stato di Open Office per Mac con una ottima notizia: è disponibile la beta della attesa versione 2.0.<p>

Un software gratuito e open source sta per fare un salto di qualità decisivo anche su Mac.
Assolutamente da visitare la pagina di <a href="http://www.freesmug.org/newsitems/news098/">FreeSmug</a> per tutti i dettagli del caso e per scoprire un sistema semplice di lanciare Open Office con un doppio clic.<p>

A meno che uno non preferisca sperperare denaro in programmi inutilmente gonfi realizzati da un&rsquo;azienda dai comportamenti assai scorretti.<p>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>