---
title: "Mille e non pi&ugrave; mille"
date: 2006-04-16
draft: false
tags: ["ping"]
---

&Egrave; un Mac PowerPc o un Mac Intel? Non &egrave; vero che basta guardarlo. Intanto non tutte le angolazioni sono buone e poi esiste il controllo remoto, quindi il Mac potrei anche non vederlo.

Soluzione: guardare <a href="http://docs.info.apple.com/article.html?artnum=303315" target="_blank">il numero di build di Mac OS X</a>. Se &egrave; minore di mille, PowerPc. Se &egrave; maggiore di mille, Intel.

Qualcuno dir&agrave; che non si pu&ograve; sempre aprire il menu Apple di un Mac remoto. Vero, ma si pu&ograve; sempre dare da Terminale il comando <code>sw_vers</code>. Gi&agrave; che ci sono, <code>uname -a</code> svela un po&rsquo; di informazioni sulla versione di Darwin.