---
title: "Famolo ibrido"
date: 2007-05-20
draft: false
tags: ["ping"]
---

Non credo molto nei sistemi operativi Web-based. Meglio, sono fantastici, tra vent'anni.

Gli esperimenti sono però interessanti. Non sostituirei mai il mio sistema operativo desktop. <a href="https://www.youos.com/" target="_blank">YouOs</a>, però, è ideale per imparare a programmicchiare (e acquisire una mentalità da sviluppatore, molto più importante che memorizzare le sintassi) senza sprecare nemmeno un byte di disco rigido.

<a href="http://desktoptwo.com/" target="_blank">Desktoptwo</a> invece (grazie <strong>Dany</strong>!) mi ha colpito per la sua interfaccia grafica, miopicamente Windows-dipendente nel desktop e nei comandi, ma tanto Cocoa nell'impostazione. A fatica, il meglio trova sempre modo di affiorare, come certe piantine sui marciapiedi di calcestruzzo.