---
title: "Punto al cinquanta percento"
date: 2008-10-20
draft: false
tags: ["ping"]
---

È arrivato l'omino FastWeb e ha installato la connessione (in breve, ha spostato il filo del telefono dal vecchio router al router FastWeb).

Finalmente ho di nuovo una connessione Internet o cos&#236; pare fino a questo momento.

Fast non ha importanza. Il Web invece ci deve essere sempre.