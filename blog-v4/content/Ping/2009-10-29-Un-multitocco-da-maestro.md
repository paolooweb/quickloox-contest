---
title: "Un multitocco da maestro"
date: 2009-10-29
draft: false
tags: ["ping"]
---

Come si sarà visto, il 27 ottobre Apple ha pubblicato il software di gestione di Magic Mouse <a href="http://support.apple.com/kb/DL950">per Snow Leopard</a> e <a href="http://support.apple.com/kb/DL951">per Leopard</a>.

Senza il software, le funzioni <i>multitouch</i> di Magic Mouse sono inattive, come <a href="http://www.macworld.it/blogs/ping/?p=2991">ebbi a scrivere il 22 ottobre</a>.

Nel frattempo si sono moltiplicate su Internet le recensioni di Magic Mouse, il primo mouse con capacità <i>multitouch</i>. Tutte scritte usando il mouse senza <i>multitouch</i>.

Non c'è bisogno di aggiungere altro.