---
title: "Giochi da pausa pranzo"
date: 2006-03-17
draft: false
tags: ["ping"]
---

<strong>Jacopo</strong>, benemerito, mi segnala <a href="http://www.travian.it" target="_blank">Travian</a>, in italiano. Io conoscevo già <a href="http://www.the-reincarnation.com/" target="_blank">The Reincarnation</a>. Giochi online gratuiti, in cui si compete con persone di tutto il mondo, per i quali è sufficiente il browser e più adatti per giocarci venti minuti tutti i giorni che non un’ora alla settimana.

Essenzialmente giochi di amministrazione delle risorse, ma con ambientazioni suggestive e il gusto di misurarsi con intelligenze non artificiali. Soprattutto, si possono giocare anche con poco tempo a disposizione. Da pausa pranzo, insomma.