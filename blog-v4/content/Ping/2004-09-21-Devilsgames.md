---
title: "Il diavolo sta nel multiplayer<p>"
date: 2004-09-21
draft: false
tags: ["ping"]
---

Se non ci sono giochi per Mac, come mai nascono nuovi siti dedicati?<p>

Tempo fa ho scritto su Macworld che di programmi e soprattutto giochi, per Mac, ce n&rsquo;è più di quanti se ne possa giocare in una vita. Una vita normale, almeno.<p>

Il deus ex machina di <a href="http://www.devilsgames.it">Devilsgames.it</a> mi ha scritto per dirmi che su certe cose non è d&rsquo;accordo con me. Rispetto il suo punto di vista e ne approfitto per invitare alla visita del sito.<p>

Su Mac ci sono giochi, c&rsquo;è gente che gioca e il numero dei giochi è una preoccupazione per chi non ha abbastanza cose importanti a cui pensare. Tant&rsquo;è che esistono siti come Devilsgames.<p>

Dimenticavo: ancora per qualche giorno, i registrati al sito (gratis) possono scaricare in anteprima la versione 1.7 di Smily, con sedici faccine in più.<p>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>