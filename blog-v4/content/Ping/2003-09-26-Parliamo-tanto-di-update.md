---
title: "La questione dell’update"
date: 2003-09-26
draft: false
tags: ["ping"]
---

La logica è più varia di quello che si pensa

Dopo i fuochi dell’update a Mac OS X 10.2.8 è necessario dirimere una questione urgente.

Uno pensa di essere logico quando ragiona che, essendo nato un problema subito dopo l’update, è colpa dell’update.

Un vero logico prenderebbe in considerazione anche l’ipotesi che il problema possa essersi sviluppato ed essere apparso in perfetta coincidenza con l’update. Oppure che l’update lavori correttamente ma, nell’aggiornare correttamente un componente, abbia come sollevato un masso e scoperto un verminaio già presente.

Nel caso di 10.2.8, i problemi iniziali si verificavano solo su certe macchine e solo in certe condizioni (vedi <link>nota tecnica</link>http://docs.info.apple.com/article.html?artnum=107669 Apple). Fuori dalla descrizione della nota tecnica, al 99,999 per cento il problema non è colpa dell’update.

Anche se l’update non impone il riavvio, meglio fare comunque almeno un logout/login. La maggior parte dei “problemi” scompare subito.

<link>Lucio Bragagnolo</link>lux@mac.com