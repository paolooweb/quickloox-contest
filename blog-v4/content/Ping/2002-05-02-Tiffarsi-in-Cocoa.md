---
title: "Tiffarsi in Cocoa"
date: 2002-05-02
draft: false
tags: ["ping"]
---

File Tiff e messaggi criptici? A qualcuno è già successo. Ma non c’è da preoccuparsi.

Prima o poi capiterà che carichi un’immagine Tiff con un programma scritto in Cocoa; dopotutto quasi l’intera grafica interna di Mac OS X è in formato Tiff.
Prima o poi una delle immagini Tiff, in uno dei programmi, farà apparire un messaggio come “Warning: TIFF image with unknown extra samples assumed to have unassociated alpha. RGB values have been premultiplied.”
Non c’è nessun problema e tutto sta andando bene. Solo che il Mac impiega un attimo di tempo in più a caricare il file. Per ottimizzarlo, è sufficiente salvare il file, che non darà più problemi.
Ah, sapevi che il Terminale contiene un piccolo programma per trattare i file Tiff? Prova a scrivere “tiffutil” e appariranno tutti gli argomenti accettati.

<link>Lucio Bragagnolo</link>lux@mac.com