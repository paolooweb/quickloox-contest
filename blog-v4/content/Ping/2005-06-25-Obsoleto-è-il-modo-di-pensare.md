---
title: "Obsoleto è il modo di pensare<p>"
date: 2005-06-25
draft: false
tags: ["ping"]
---

Visto che cambiamo i processori, abbandoniamo anche i luoghi comuni<p>

Lascio la parola a Stefano.<p>

<cite>Questa &ldquo;<a href="http://www.theregister.com/2005/06/22/single_cpu_mac_rip/">news</a>&rdquo; mi informa che il magazzino di Apple è pieno di processori IBM obsoleti da mettere in saldo.</cite><p>

<cite>Obsoleti rispetto a cosa?</cite><p>

<cite>Le mie scarpe non sono obsolete: il mio piede (le mie esigenze) ha smesso di allungarsi da parecchio tempo, quindi le mie scarpe possono solo essere consumate o fuori moda.</cite><p>

<cite>Se sono troppo consumate le devo sostituire, ma se sono fuori moda posso anche fregarmene perché mi consentono comunque di camminare nel modo in cui sono abituato e di dedicare le mie risorse a cose diverse dalla loro sostituzione.</cite><p>

<cite>Vogliamo proprio far finta di non sapere che nel momento in cui si acquista un computer il costruttore sta già mettendo a punto il modello successivo? O qualche associazione di consumatori dovrebbe far causa ad Apple perché, testando segretamente Mac OS X su Intel, sono anni che vende dolosamente hardware obsoleto?</cite><p>

<cite>Posso solo confermare quello che, se non ricordo male, ho letto recentemente su Ping (o sulla rivista?): comprate quello che serve nel momento in cui serve.</cite><p>

<cite>Corollario: se rimando l&rsquo;acquisto per motivi diversi da problemi di budget, vuol dire che non mi serve uno strumento, ma un bel giochino.</cite><p>

<cite>Tutti hanno diritto di desiderare il giochino nuovo, ma non confondiamo i sogni con l&rsquo;inadeguatezza rispetto alle reali esigenze dell'utente.</cite><p>

<cite>Ma se proprio ci dovesse essere una svendita di G5 mi piacerebbe essere avvertito. Arrivo subito e riempio la station wagon con tutto quello che trovo: iMac, PowerMac e XServe.</cite><p>

<cite>Ne metto anche sul tetto (è roba obsoleta, si può strapazzarla). E quando mi ferma la Finanza gli dico che è ferrovecchio, esibendo a riprova l&rsquo;articolo di cui sopra e dicendo: ma non sapete che un PC diventa obsoleto dopo 6 mesi?</cite><p>

<cite>Stefano</cite><p>

<cite>iMac 400 DV SE funzionante da fine Gennaio 2000, 2 GByte su 13 ancora disponibili, mouse originale (sopravvissuto a 2 pargoli che hanno iniziato ad usarlo quando avevano 4 e 6 anni), ADSL collegato dal 2002 all'ethernet di serie, Office, Acrobat (2 versioni), FireWire mai usata, Netscape (2 versioni), Opera, IM, PIM, FaxStf, iTunes, AppleWorks, Internet Explorer, Outlook Express, Norton Antivirus, Windows Media Player, software per compressione/decompressione file, scanner, stampante, 4 programmi di elaborazione immagini, un carretto di utilità, giochini, inutilità, centinaia di file salvati di vario genere, circa 3000 messaggi di posta elettronica, brani per il Karaoke&hellip;</cite>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>