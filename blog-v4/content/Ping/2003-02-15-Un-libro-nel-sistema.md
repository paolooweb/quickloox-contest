---
title: "Un libro nel sistema"
date: 2003-02-15
draft: false
tags: ["ping"]
---

Qualcosa che nessun altro sistema operativo ha

Come si sa, Jaguar riconosce al volo non solo i Mac ma anche i Pc collegati in rete: basta un mela-k.

Qualcuno meno sa che il protocollo per riconoscere i Pc in rete si chiama Smb.

Qualcuno meno ancora sa che il mondo open source ha sviluppato una versione open del software di utilizzo protocollo (da cui ha attinto anche Mac OS X) chiamata Samba.

Pochissimi sanno che dentro Mac OS X c’è una versione completa del libro Using Samba di O’Reilly (prima edizione).

Praticamente nessuno sa che il libro si trova in /usr/share/swat/using_samba/index.html. Se lo apri da un browser, dai il comando <file://usr/share/swat/using_samba/index.html>. È possibile che il tuo browser voglia un terzo slash a inizio Url, quindi <file:///usr/share/swat/using_samba/index.html>.

Indovinello: c’è almeno uno che sappia come è nato il nome Samba?

<link>Lucio Bragagnolo</link>lux@mac.com