---
title: "Cambio della guardia"
date: 2008-02-24
draft: false
tags: ["ping"]
---

Fidel Castro rinuncia al potere a Cuba? Bazzecole.

Richard Stallman <a href="http://lists.gnu.org/archive/html/emacs-devel/2008-02/msg02140.html" target="_blank">lascia la posizione di maintainer di emacs</a>. <a href="http://www.stallman.org/" target="_blank">Stallman</a> è il padre del <em>free software</em> e del <a href="http://www.gnu.org/home.it.html" target="_blank">progetto Gnu</a>. <a href="http://www.gnu.org/software/emacs/" target="_blank">emacs</a> è l'editor di testo e l'ambiente di programmazione per definizione, per tantissimi sviluppatori, da più di trent'anni.

E lo abbiamo tutti dentro il nostro Terminale.