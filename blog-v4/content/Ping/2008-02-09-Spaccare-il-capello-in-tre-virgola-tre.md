---
title: "Spaccare il capello in tre virgola tre"
date: 2008-02-09
draft: false
tags: ["ping"]
---

Chi avesse un MacBook Pro Core 2 Duo <em>non</em> di ultima generazione, leggerà nelle specifiche del proprio sistema che la Ram supportata arriva fino a tre gigabyte e poi basta.

Se però installa quattro gigabyte <em>e</em> Mac OS X 10.5.1, vedrà che il sistema riporta correttamente la cifra. Solo che poi ne userà solo 3,3 e non oltre.

La cosa è dovuta al <em>chipset</em> Intel utilizzato, che usa 32 bit e non 64 per gestire la Ram.

Sull'ultima generazione di MacBook Pro, i cosiddetti Santa Rosa, anche quel chip è a 64 bit e dunque i quattro giga vengono visti tutti quanti.

Ho scoperto la cosa nel laboratorio di assistenza di <a href="http://atworkgroup.net" target="_blank">@Work</a> ma, naturalmente, su Internet ci erano già arrivati tutti, a cominciare da <a href="http://www.macosxhints.com/article.php?story=2008013001563240" target="_blank">Mac OS X Hints</a>.