---
title: "Gli asparagi e l’immortalità dei dati"
date: 2007-11-04
draft: false
tags: ["ping"]
---

Conscio del fatto che i supporti ottici non durano per sempre e vanno replicati periodicamente (mi diverte la gente che pensa di comprare l’immortalità dei dati con due euro spesi all’ipermercato), ho riversato temporaneamente tutti i miei Cd e Dvd di backup sul disco rigido da 500 giga dove tengo il backup settimanale.

Sapendo che da più parti viene sparso terrore a piene mani (ho letto persino come i Dvd durerebbero meno di un anno, che deve essere vero per chi li usa come sottopentola), ho tenuto rapidamente nota di come è andato il riversamento.

Ho copiato 41 dischi, tra Cd e Dvd: 27 Cd e 14 Dvd.

I dischi erano distribuiti tra ottobre 1994 e gennaio 2006. 