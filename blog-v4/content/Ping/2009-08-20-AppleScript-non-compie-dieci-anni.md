---
title: "AppleScript non compie dieci anni"
date: 2009-08-20
draft: false
tags: ["ping"]
---

<a href="http://macscripter.net/" target="_blank">MacScripter.net</a> s&#236;.

Resta uno dei migliori posti in cui accumulare esperienze e codice su AppleScript. E non tutti sanno che il sito supporta direttamente AppleScript Protocol Link. Clicchi e il codice arriva direttamente dentro Script Editor, pronto per lavorarci.

<cite>display dialog "Bella risorsa, MacScripter.net!" buttons {"Vero"} default button 1</cite>