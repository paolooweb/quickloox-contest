---
title: "Il Grande Fratello era un inetto"
date: 2009-07-18
draft: false
tags: ["ping"]
---

Era agosto 2008 e il mondo si contrasse in un sussulto di sdegno: <a href="http://news.digitaltrends.com/news-article/17531/apple-can-kill-iphone-apps" target="_blank">Apple poteva cancellare a distanza le applicazioni su iPhone</a>. Vergogna. Le libertà civili. La privacy. Il controllo totale. Bla bla bla.

Pochi giorni fa centinaia di acquirenti di Kindle (il libro elettronico di Amazon) hanno acceso l'aggeggio e hanno scoperto che dal loro lettore erano stati <a href="http://pogue.blogs.nytimes.com/2009/07/17/some-e-books-are-more-equal-than-others/" target="_blank">cancellati d'autorità due libri regolarmente acquistati</a>.

Ironia della sorte, proprio <a href="http://gutenberg.net.au/ebooks01/0100021.txt" target="_blank">1984</a> e <a href="http://www.gutenberg.net.au/ebooks01/0100011.txt" target="_blank">La fattoria degli animali</a> di George Orwell.

L'editore che detiene i diritti ha ritenuto che le copie digitali fossero illegali e ne ha chiesto il ritiro.

Il bello arriva adesso: Per come funzionava il negozio <i>online</i> di libri per Kindle, Amazon è stata <i>costretta</i> tecnicamente a cancellare anche le copie sui lettori (e rimborsare i clienti).

Sempre Amazon ha dichiarato di avere aggiornato l'infrastruttura e che la cosa non si ripeterà. Se un libro venisse ritirato dal negozio, le copie acquistate resterebbero al loro posto.

Apple ha più volte eliminato applicazioni da App Store, ma le copie scaricate non sono mai state toccate. In altre parole, ha costruito per App Store una infrastruttura più intelligente di quella del negozio <i>online</i> più grande del mondo.

Questo il fatto tecnico. Tornando alle sciocchezze sul controllo a distanza, oggi i fatti mostrano che non c'è la minima intenzione di applicarlo. Chi ha dovuto applicarlo per incapacità tecnica si è affrettato a correggere i propri errori e a fare sapere che non succederà più. Chi invece ha fatto le cose per bene, si guarda bene dall'applicarlo. Scommetto che chi si è visto cancellare due libri regolarmente acquistati non sarà un cliente tanto soddisfatto. E i clienti insoddisfatti comprano altrove.

Si scopre che il Grande Fratello, quello di Orwell, più che essere un tiranno era un cattivo progettista. Avesse fatto le cose come si deve non avrebbe dovuto opprimere nessuno.

Dovremmo inoltre assistere a un gran sciacquarsi la bocca da parte di tanti sedicenti paladini dei diritti civili. Invece non sento niente. Chi li ha cancellati?