---
title: "Weekend alla Mela<p>"
date: 2005-05-13
draft: false
tags: ["ping"]
---

Tra Liguria ed Emilia-Romagna l&rsquo;appassionato Mac ha da perdersi<p>

Invece della solita scampagnata, che tanto c&rsquo;è tutta l&rsquo;estate, domani (sabato 14) ci si trova a Quiliano (Savona) per l&rsquo;<a href="http://www.allaboutapple.com/aaa.htm">inaugurazione</a> della nuova sede del primo museo in Europa dedicato ad Apple. Ci sarò perché proprio non me la voglio perdere e voglio rivedere un sacco di cose della mia infanzia informatica.<p>

Non sarò invece domenica 15 a Casalecchio di Reno, perché non ce la faccio a farcela, come diceva il comico Salvi. Ma ci saranno ugualmente (ci mancherebbe) un sacco di gente interessante e tanti eventi, a Casalecchio di Reno (Bologna), in occasione del <a href="http://www.maclub.it/">Mac Day 2005</a>.<p>

Due considerazioni. La prima è che i computer sono la scusa. Si fanno amicizie, si scoprono posti e persone nuove, e già che ci siamo si apprende qualche dritta-Mac che non fa mai male.<p>

La seconda. Il centronord è coperto. Che si fa, a Roma, Napoli, Foggia, Catanzaro, Palermo? Organizzare, organizzare. Alla fine sembra sempre che siano quattro gatti e poi ci si scopre come minimo il doppio. Otto gatti, alla peggio, possono sempre farsi una partita in rete a Battle for Wesnoth. :-)<p>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>