---
title: "Lo stile per persone normali<p>"
date: 2005-06-28
draft: false
tags: ["ping"]
---

Che cosa serve a gente come Mario e perché<p>

Mario fa tutt&rsquo;altro mestiere, ma per questioni no profit ha voluto dedicarsi al web authoring. Per un insieme di motivi cerca programmi semplici, utili da subito e freeware o shareware a prezzo onesto.<p>

Per i Css (Cascading Style Sheet) ha trovato e mi segnala <a href="http://www.macrabbit.com/cssedit">CSSEdit</a>, o <em>Style for the rest of us</em>. La modalità demo è perfettamente funzionante e si limita a non salvare il foglio stile in caso superi i 2.499 caratteri, che per imparare e fare cose semplici, ma utili, non c&rsquo;è proprio problema.<p>

Con altri due o tre programmi di supporto, tra cui il meraviglioso (e gratuito!) <a href="http://www.barebones.com/products/textwrangler/index.shtml">TextWrangler</a> di Bare Bones, Mario ci ha messo davvero poco per arrivare a produrre codice di buona qualità a beneficio della causa in cui crede. Conosco un paio di altri amici che hanno fatto il diavolo a quattro per una copia illegale di DreamWeaver e, senza manuale o senza voglia di leggerlo, essendo lungo chilometri, fanno poco e male.<p>

I programmi giusti per persone normali sono programmi normali.<p>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>