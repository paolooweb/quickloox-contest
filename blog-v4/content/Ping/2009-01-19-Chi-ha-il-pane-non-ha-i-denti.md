---
title: "Il paradosso di FireWire"
date: 2009-01-19
draft: false
tags: ["ping"]
---

I MacBook senza FireWire sono un errore, leggo. I <em>netbook</em>, con il processore giocattolo, la tastiera da anchilosi e lo schermo sacrificato, invece sono una bomba. Apple, mi si dice, dovrebbe fare un <em>netbook</em>.

Nessuno mi spiega perché tutta questa gente in attesa del <em>netbook</em> Apple farebbe tranquillamente a meno della FireWire di cui non può fare a meno sui MacBook.