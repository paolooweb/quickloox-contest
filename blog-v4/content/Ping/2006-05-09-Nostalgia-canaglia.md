---
title: "Nostalgia canaglia"
date: 2006-05-09
draft: false
tags: ["ping"]
---

In chat ieri sera (grazie!) &egrave; capitato di rimembrare qualche vecchio giochino su Mac. Non sono un gran giocatore, ma ogni tanto mi dedico seriamente a qualcosa e allora passano le notti, i weekend, le pause pranzo. Mi sono tornati in mente alcuni dei giochi su cui ho passato <em>seriamente</em> tempo e ludica fatica.

<a href="http://home.mieweb.com/jsfiles/lrfiles/" target="_blank">Lode Runner</a>, <a href="http://www.zsculpt.com/website/games/darkcastle3/index.php" target="_blank">Dark Castle</a>, <a href="http://members.chello.at/theodor.lauppert/games/wizardry.htm" target="_blank">Wizardry</a>, Rogue e poi <a href="http://thangorodrim.net/" target="_blank">Angband</a>, <a href="http://jrisk.sf.net" target="_blank">Risk</a>, tutte le avventure <a href="http://www.logicalshift.demon.co.uk/mac/zoom.html" target="_blank">Infocom</a>, <a href="http://www.macgamer.com/features/?id=932" target="_blank">Prince of Destruction</a>, <a href="http://www.macupdate.com/info.php/id/6151" target="_blank">Maelstrom</a>, <a href="http://www.ambrosiasw.com/games/cythera/" target="_blank">Cythera</a>, <a href="http://www.macplay.com/games/g.bhaal.php" target="_blank">Baldur&rsquo;s Gate</a>, <a href="http://www.wesnoth.org" target="_blank">Battle for Wesnoth</a>, <a href="http://www.leu.it" target="_blank">Lumen et Umbra</a>, naturalmente <a href="http:://www.wow-europe.com" target="_blank">World of Warcraft</a>. Tutti giochi su cui potrei ritornare senza battere ciglio nell&rsquo;arco di mezzo minuto, dovessi abbandonare WoW. Il bello &egrave; che molti sono ancora giocabili.