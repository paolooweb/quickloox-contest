---
title: "Non toccarmi il pod"
date: 2006-10-13
draft: false
tags: ["ping"]
---

La risposta migliore alle polemiche su Apple che invia lettere di <a href="http://blog.wired.com/music/2006/09/apple_hits_podc.html?entry_id=1561308" target="_blank">cease &#38; desist</a> ad aziende che cercano di scavalcare i suoi marchi registrati me la manda <a href="http://www.accomazzi.net" target="_blank">Akko</a> da <a href="http://www.theonion.com/content/node/53601" target="_blank">The Onion</a> (e grazie mille).

Più seriamente, Apple ha inviato la lettera a Podcast Ready non per crudeltà mentale, ma perché Podcast Ready stessa voleva registrare i marchi <em>Podcast Ready</em> e <em>myPodder</em>. In base alla legge americana, una azienda è <em>tenuta</em> a intervenire attivamente in difesa dei propri marchi registrati, altrimenti ne perde la proprietà. Quando un marchio non viene più difeso attivamente, può essere usato da chiunque in qualsiasi circostanza, come accade da molti anni, per esempio, per Aspirina.