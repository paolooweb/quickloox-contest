---
title: "Ed erano abbastanza"
date: 2007-07-06
draft: false
tags: ["ping"]
---

Ho scoperto <a href="http://www.runescape.com" target="_blank">Runescape</a> in buona compagnia. Eravamo in due, ma <strong>Narbondo</strong> ha una lunga esperienza nel gioco. Io ho sbagliato impostazioni (avrei dovuto impostare la grafica più essenziale) e ho sofferto un po' di lentezza, ma mi sono avventurato nel lungo tutorial di inizio e ho scoperto un gioco profondo e assai frequentato.

I venerd&#236; del gioco stanno aumentando pericolosamente la quantità di posti in cui vorrei avere piacere di tornare. :)