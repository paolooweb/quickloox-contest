---
title: "Conflittone di interessini"
date: 2006-09-22
draft: false
tags: ["ping"]
---

Ai cacciatori di interessi occulti non parrà vero: sto per annunciare che venerd&#236; prossimo la pizzata del Poc, di cui sono socio e che si svolge a partire dal raduno in <a href="http://www.macatwork.net" target="_blank">Mac@Work</a> di cui sono ugualmente socio seppure in diverso modo, si svolge in concomitanza con il <strong>NeoOffice (install) party</strong> indetto dal FreeSmug, con distribuzione di T-shirt e Cd-Rom e forse visione di video come <a href="http://www.freesmug.org/Video/OOoCon2006" target="_blank">questi</a>.

Considerato che uso <a href="http://www.openoffice.org" target="_blank">OpenOffice</a> e che mi piacciono le t-shirt, sono un essere spregevole e venduto. Se aggiungiamo che possiedo alcune decine di Cd per backup e altro, il conflitto si palesa in tutta la sua squassante evidenza.

Qualcuno mi fermi, prima che aggiunga cose destinate a compromettermi definitivamente, tipo che il <a href="http://www.freesmug.org" target="_blank">FreeSmug</a> mi sta molto simpatico oppure che nel <a href="http://www.poc.it" target="_blank">Poc</a> ho un sacco di amici.