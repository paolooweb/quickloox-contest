---
title: "Quello che i blogger non dicono"
date: 2006-02-21
draft: false
tags: ["ping"]
---

L&rsquo;amico <strong>Mario</strong> segnala <a href="http://journler.phildow.net/" target="_blank">Journler</a>, eccellente programma di registrazione organizzazione e archiviazione delle proprie note giornaliere.

No, non &egrave; un software per aprire blog. Serve per le nostre cose, quelle che diremo a qualcuno solo quando sar&agrave; il tempo, testo, audio, video, non importa.

Considerato il prezzo, non scaricarlo e darci un&rsquo;occhiata &egrave; un&rsquo;occasione persa. Potresti scoprire di avere molto pi&ugrave; da dire e da dare di quello che pensavi.