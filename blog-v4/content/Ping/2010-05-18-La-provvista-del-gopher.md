---
title: "La provvista del gopher"
date: 2010-05-18
draft: false
tags: ["ping"]
---

Quando non c'era ancora il web c'era <i>gopher</i>, una interfaccia testuale a menu per visitare i server e visionare il loro contenuto.

Nel 2007 John Goerzen ha iniziato a cercare e a scaricare tutti i siti <i>gopher</i> che riusciva a trovare, con il proprio programma <a href="http://github.com/jgoerzen/gopherbot" target="_blank">gopherbot</a>, e ne ha ricavato 780 mila documenti, per un totale di circa quaranta gigabyte.

Gran parte di questo patrimonio storico è offline e cos&#236; Goerzen ha deciso di rendere disponibile come <a href="http://www.complete.org/~jgoerzen/Full%20Gopher%20Archive%20from%202007.torrent" target="_blank">file torrent</a> l'archivio compresso della sua ricerca, circa 15 gigabyte.

È materiale che sta su qualsiasi disco rigido odierno e praticamente un pezzo intero di storia di Internet; come se chiunque potesse avere in casa, che so, tutti i libri usciti negli ultimi dieci anni. Una testimonianza storica di grande valore.

Anche a non volerla scaricare, è bello sapere che c'è.

Gopher è un roditore detto anche scoiattolo di terra, mascotte dell'University of Minnesota, dove nel 1990 venne inventato il sistema. Quello con le guance che si possono gonfiare di cibo.

A contorno, va segnalata anche la <a href="http://www.dukenews.duke.edu/2010/05/usenet.html" target="_blank">chiusura del primo server Usenet della storia</a>, quello di Duke University. L'inizio data al 1979, grazie agli studenti Tom Truscott e Jim Ellis.