---
title: "Un AppleScript da far ruotare la testa"
date: 2008-04-09
draft: false
tags: ["ping"]
---

Questo minuscolo AppleScript dimostra come si possano controllare le impostazioni del sistema. Nello specifico, e visto che si è cominciata la giornata a colpi di salvaschermo, con lo sfondo scrivania:

<code>
tell application "System Events"
	-- Rotazione casuale di una cartella di immagini
	tell current desktop
		set picture rotation to 1 -- (0=spento, 1=intervallo, 2=login, 3=stop)
		set random order to true
		set pictures folder to file "Mac OS X:Library:Desktop Pictures:Plants:"
		set change interval to 5.0 -- secondi
	end tell
end tell
</code>

Lo presento cos&#236; e invito a provarlo. Va notato che tutte le istruzioni riguardano parole riservate ad AppleScript. Non c'è niente di inventato e le stesse istruzioni potrebbero essere usate in altre situazioni.

L'impostazione della pictures folder (cartella immagini) è su una delle cartelle di serie in Mac OS X, ma non è difficile cambiarla a piacere, ricordandosi che dentro AppleScript le cartelle si separano a colpi di duepunti.

L'originale di questo script si trova, guarda un po', sulle <a href="http://www.apple.com/applescript/features/system-prefs.html" target="_blank">pagine AppleScript del sito Apple</a>.