---
title: "Il serpente tentatore"
date: 2009-08-09
draft: false
tags: ["ping"]
---

Dopo avere raccontato che non c'è solo AppleScript per fare piccola (ma interessante e utile) programmazione su Mac, e avere presentato Ruby con il suo straordinario <a href="http://tryruby.hobix.com/" target="_blank"><i>tutorial</i> via <i>browser</i></a>, mi sono chiesto se non potessero avere dignità anche altre possibilità a disposizione di Mac (e nostra).

Se invece di digitare <code>irb</code> nel Terminale digitassimo <cite>python</cite>, partirebbe non Ruby ma Python. È un altro linguaggio con cui si possono fare cosette oppure erigere cattedrali.

Purtroppo non c'è un tutorial altrettanto entusiasmante come quello di Ruby. Ci sono pagine dedicate a compiti specifici, tipo programmare giochi oppure interfacce a server web, ma l'unico tutorial completo è quello noiosetto del <a href="http://docs.python.org/tutorial/" target="_self">quartier generale di Python</a>.

Un piccolo vantaggio Python ce l'ha: probabilmente nella cartella Applicazioni, in una cartella MacPython, si trova Idle. È una shell dedicata espressamente a Python, più comoda da usare del solito Terminale.

Un amico usa Python per correggere i compiti di matematica della figlia alle prese con le espressioni, quelle da scuole medie. Molto più veloce e se è corretto l'input è certamente corretto il risultato. Bisogna solo ricordarsi che, per elevare a potenza, non si usa ^ ma **. Due elevato alla terza è 2**3.

A qualcuno potrebbe venire la tentazione di andare oltre le espressioni. Su Internet come sempre c'è il mondo e in particolare una <a href="http://vermeulen.ca/interesting-python-cookbook-recipes.html" target="_blank">pagina di programmi grandi e piccoli</a> realizzati con Python, cos&#236; uno li può studiare e provare. Ce ne sono molte decine, su argomenti anche astrusi.