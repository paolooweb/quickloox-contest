---
title: "And the winner is"
date: 2003-01-09
draft: false
tags: ["ping"]
---

Assegnato il premio Rintro 2003

Seconda edizione del premio Rintro, assegnato da una giuria insindacabile (myself) a chi la spara più grossa in occasione del keynote di Steve Jobs a Macworld Expo.

Due categorie, quest’anno: pubblico e critica. Il premio alla critica viene assegnato ex æquo a tutti i siti che hanno speso pagine a descrive i nuovi eccezionali schermi che avrebbe certamente presentato Apple.

Il premio al pubblico, invece, viene vinto a mani basse da Francesco (non, quell’altro) con la sua filippica sul costo delle iApps; avedo letto chissà dove che gli upgrade sarebbero stati pagamento, si è infuriato con anticipo e ha scritto un paio di messaggi di fuoco su una mailing list ben frequentata.

Il pensiero che poteva non essere vero e che prima di parlare bisogna accendere il cervello non lo ha neanche sfiorato. Il premio Rintro, invece, se lo becca tutto.

Al prossimo keynote!

<link>Lucio Bragagnolo</link>lux@mac.com