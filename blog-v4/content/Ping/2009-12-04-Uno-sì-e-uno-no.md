---
title: "Uno s&#236; e uno no"
date: 2009-12-04
draft: false
tags: ["ping"]
---

All'indomani di una visita-lampo parigina, ho provato a tenere il conto dei Mac privati usati in luoghi pubblici, come Starbucks, McDonald's, gli aeroporti o magari semplicemente una panchina in piazza: sette su quattordici, davvero non male.

Ho provato a tenere il conto anche del parco macchine <i>mobile</i>, ma l'impresa si è rapidamente rivelata superiore alle mie forze: troppa gente che estrae qualcosa dalle tasche in qualsiasi situazione e una preponderanza imbarazzante di iPhone/iPod touch (praticamente indistinguibili se non a breve raggio), ché qualcuno avrebbe potuto accusarmi di truccare i dati. Veramente imbarazzante&#8230; per gli altri.

Circostanza singolare: al <a href="http://www.chateauversailles.fr/homepage" target="_blank">castello di Versailles</a> c'è la possibilità di farsi fotografare in costume d'epoca, non più infilando le facce nelle sagome di cartone come usava nel secolo scorso, bens&#236; grazie alle magie del fotoritocco. Ed era un Mac.

Segnalo infine che mi sono tornate utili le applicazioni iPhone/iPod touch di <a href="http://itunes.apple.com/it/app/id337339103?mt=8" target="_blank">Louvre</a> e <a href="http://itunes.apple.com/it/app/le-grand-palais-paris-english/id325673124?mt=8" target="_blank">Grand Palais</a>.