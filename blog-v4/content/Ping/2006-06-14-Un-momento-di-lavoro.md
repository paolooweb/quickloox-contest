---
title: "Sentivo di stare lavorando"
date: 2006-06-14
draft: false
tags: ["ping"]
---

<em>Call conference</em> a tre. G. in ufficio a Sesto San Giovanni, D. a casa a Londra, io in Apple Center a Milano. Accordo comune per usare <a href="http://www.skype.com" target="_blank">Skype</a>. Mezz&rsquo;ora di vera riunione, con trasferimento contestuale di file riguardanti il lavoro e grande produttivit&agrave;.

Sono cose normali da un pezzo, ma quando mi ci trovo dentro sento ancora il brivido della tecnologia che cambia il mondo. Che devo farci? :-D