---
title: "Il peccato dell'accydia"
date: 2010-11-10
draft: false
tags: ["ping"]
---

<cite>Seriamente: in realtà Apple non esclude gran che da App Store. Apple è occasionalmente anticompetitiva e questo dà fastidio, ma la questione si riduce in realtà a una manciata di casi di alto profilo: l'effetto sul mercato è minimo. La maggior parte di quello che proibiscono a) non funziona oppure b) è illegale o moralmente problematico per l'americano medio. Dunque, per favore&#8230;</cite> per favore&#8230; <cite>vi imploro: cessate la battaglia per fare aprire App Store ad Apple. Semmai lavorate perché Apple renda più aperto il suo apparecchio.</cite>

Questo è <a href="http://news.ycombinator.com/item?id=1884262" target="_blank">il sunto di Jay Freeman alias Saurik</a>, il geniale programmatore che ha creato Cydia, il mercato alternativo ad App Store per iPhone, iPod touch e iPad soggetti a <i>jailbreak</i>.

Freeman ha tenuto un <a href="http://www.youtube.com/watch?v=ReKCp9K_Jqw" target="_blank">intervento molto interessante</a> nell'ambito della rassegna Ted, che raccoglie discorsi di personalità di spicco.

Se lo dice lui, forse tante sparate che si leggono sulla chiusura di App Store sono un po' fuori luogo.