---
title: "Mission Impossible: 2"
date: 2009-04-12
draft: false
tags: ["ping"]
---

Sarà più difficile vincere i premi in palio per festeggiare <a href="http://www.apple.com/itunes/billion-app-countdown/" target="_blank">il miliardesimo programma scaricato da App Store</a> (si possono &#8220;aumentare&#8221; le probabilità di vincita scaricando fino a venticinque applicazioni al giorno oppure compilando altrettante volte l'apposito modulo) oppure aggiudicarsi una scheda-regalo iTunes da 200 dollari sopravvivendo per una settimana a <a href="http://www.doomsdayterminal.com/" target="_blank">Doomsday Terminal</a>, previa spesa di 0,79 euro e obbligo di inserire un codice numerico entro 108 minuti?