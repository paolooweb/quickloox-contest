---
title: "Un cracker e una lettura"
date: 2007-04-27
draft: false
tags: ["ping"]
---

Rispetto al concorso penetra-un-Mac al convegno CanSecWest che ha fatto gridare alla perdita di verginità del Mac in fatto di sicurezza, è interessante leggere l'<a href="http://daringfireball.net/2007/04/interview_dino_dai_zovi" target="_blank">intervista di John Gruber di Daring Fireball a Dino Dai Zovi</a>, l'autore dell'<em>exploit</em> (un <em>cracker</em> è uno che penetra nei sistemi, per chiarire il titolo).

Come già detto, la falla non è su Safari ma su Java. Per questo non è specifica di Mac OS X ma potrebbe toccare qualunque sistema su cui gira QuickTime e dunque anche Windows, e Firefox su Windows.

Roba di calibro abbondantemente già visto e già&#8230; patchato alla storia. C'è da scommettere che il prossimo Security Update è già in arrivo. Nel frattempo, i Mac compromessi nella vita reale continuano a tendere allo zero. I sistemi Windows, al centinaio di milioni.