---
title: "L&rsquo;Intel-ligenza di Mac@Work<p>"
date: 2005-07-13
draft: false
tags: ["ping"]
---

Almeno un Mac Intel-based in Italia settentrionale<p>

Qualcuno si ricorderà dell&rsquo;idea che aveva avuto Sergio: raccolta fondi tra gli interessati e acquisto di un Mac con processore Intel, di quelli che Apple riserva agli sviluppatori che vogliono iniziare da subito a convertire il loro software nel formato Universal Binary, che gli permetterà di girare su qualsiasi Mac a prescindere dal processore.<p>

Io avevo aderito. Purtroppo l&rsquo;idea non ha attecchito. Al che l&rsquo;ho girata a <a href="http://www.macatwork.net">Mac@Work</a>. Dove è piaciuta ed è stata ordinata una macchina.<p>

Il Mac è arrivato ed è a disposizione gratuita di tutti gli sviluppatori italiani che desiderassero fare prove ed esperimenti.<p>

Non so quanti Mac Intel-based esistano fuori da Apple, in Italia, in questo momento. Spero tanti. Credo pochissimi.<p>

Complimenti a Sergio per un&rsquo;ottima idea e complimenti a Mac@Work per una iniziativa intelligente e meritevole. Verrà seguita da altri? Io sono a disposizione per chi volesse segnalare.<p>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>