---
title: "Pagati per sbagliare"
date: 2009-04-26
draft: false
tags: ["ping"]
---

Apple 2.0 ha messo a confronto i risultati finanziari di Apple del trimestre con le previsioni degli analisti. C'è anche <a href="http://apple20.blogs.fortune.cnn.com/2009/04/23/apples-q2-analyzing-the-analysts/" target="_blank">un bel grafico chiaro e sintetico</a>.

Si poteva fare meglio con i fondi del caffè. Ma come fa questa gente a restare credibile?