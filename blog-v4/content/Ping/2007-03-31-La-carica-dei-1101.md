---
title: "La carica dei 1.101"
date: 2007-03-31
draft: false
tags: ["ping"]
---

Ping! è invaso dallo spam nei commenti. Da quando è partito il blog ho regolarmente moderato e cancellato alcune decine di commenti-spam al giorno. Ma ultimamente la media è salita ben sopra quota cento e vengo da due giorni con oltre mille commenti-spazzatura ciascuno.

L'edizione di <a href="http://wordpress.org/" target="_blank">WordPress</a> che mi tocca usare mostra tutti i commenti moderati in una pagina unica, in questi casi lunghissima. Un comando permette di contrassegnare automaticamente come spam tutti i commenti in moderazione e poi approvare manualmente quelli provenienti da umani (chi ha già fatto almeno un commento vero viene approvato automaticamente e non passa attraverso questo filtro).

La notizia è che, con un numero consistente di messaggi nella pagina, Safari va in netta crisi JavaScript. Lo script deputato a contrassegnare i messaggi si inchioda e porta con sé il browser. Firefox, invece, non fa una piega.