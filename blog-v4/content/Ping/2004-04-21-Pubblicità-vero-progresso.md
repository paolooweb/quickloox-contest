---
title: "Pubblicità vero progresso"
date: 2004-04-21
draft: false
tags: ["ping"]
---

Iniziativa commerciale per il no profit, e Mac-oriented

Non segnalerei questa iniziativa se non arrivasse da persone che usano Mac e sono anche simpatiche: Creativo@dvertising offre alle associazioni, e in generale a tutti i i soggetti che operano nel no profit, sconti e agevolazioni rilevanti sui propri servizi di hosting e sull’installazione di un sistema portale basato su software open source. In più sempre Creativo@dvertising donerà il 10 percento del prezzo del pacchetto in beneficenza a un’associazione tra Unicef, Amnesty International, Wwf, Children in Crisis e Croce Rossa Internazionale.

Insomma si può fare un affare, fare del bene e lavorare con persone della comunità Mac. Quando ci vuole, ci vuole. :)

<link>Lucio Bragagnolo</link>lux@mac.com