---
title: "Il premio crapware"
date: 2009-09-01
draft: false
tags: ["ping"]
---

Lo assegno a Vuze e alla sua <i>toolbar</i>.

Avevo già raccontato di come <a href="http://www.macworld.it/blogs/ping/?p=2780" target="_self">si appiccichi senza chiedere</a>.

È arrivato un aggiornamento di <a href="http://www.vuze.com" target="_blank">Vuze</a> e l'ho installato, stando bene attento stavolta alle opzioni a disposizione. Non volevo ripetere l'errore iniziale e reinstallare la <i>toolbar</i>.

Non c'erano opzioni e l'aggiornatore ha nuovamente installato la <i>toolbar</i>.

Solo su Firefox però. Safari è rimasto pulito.

Non so che cosa sia successo e perché, ma &#8211; disinstallata la <i>toolbar</i> da Firefox per l'ennesima volta &#8211; adesso Vuze sta nel cestino.

<i>Crap</i> significa <i>schifezza</i>. Nella traduzione meno volgare.