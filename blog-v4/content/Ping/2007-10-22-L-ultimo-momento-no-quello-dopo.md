---
title: "L'ultimo momento, no, quello dopo"
date: 2007-10-22
draft: false
tags: ["ping"]
---

<a href="http://daringfireball.net/" target="_blank">John Gruber</a> dice che Leopard edizione al pubblico non è uguale-uguale-uguale alle <em>build</em> che sono state in mano agli sviluppatori. Per esempio, il Dock messo da un lato non ha lo stesso aspetto che ha nelle <em>build</em> suddette.

La prima considerazione: allora forse un po' di feedback lo ascoltano davvero. La seconda: i cambiamenti oramai sono più veloci della luce.