---
title: "AppleScript e&#8230; l'ora di Napoli"
date: 2008-04-06
draft: false
tags: ["ping"]
---

Aggiunge <a href="http://www.bottegascientifica.it" target="_blank">Carlo</a>:

<cite>Come</cite> addendum <cite>allo <a href="http://www.macworld.it/blogs/ping/?p=1729">script sulla data della Pasqua</a>, potresti aggiungere queste due righe che ci comunicano la data e l'ora esatta in Italia con la precisione del secondo.</cite>

<code>set DataItaliana to do shell script "curl http://www.ien.it/stittime_i.shtml | grep DATA:"
display dialog "La data e l'ora attualmente in in Italia: " &#38; DataItaliana
</code>

<cite>L'ora viene letta dal sito dell'Istituto Galileo Ferraris di Torino tramite il comando <code>curl</code>, purgata dall'Html con il comando Unix <code>grep</code> e infine restituita allo script che la mostra in una finestrella di dialogo.</cite>

<cite>E per chi dirà</cite> perché complicarsi la vita? <cite>consiglio di non aprire nemmeno lo Script Editor ma di andare direttamente alla pagina dove un &#8220;pazzo&#8221; napoletano, ci <a href="http://www.cheorae.it/" target="_blank">pronuncerà l'ora</a>&#8230;</cite>

<cite>Ha preparato 1.440 scenette diverse per ogni minuto della giornata.</cite>

<cite>Si vede che lui di tempo ne aveva&#8230;</cite>

Oppure ha trascurato la programmazione per  concentrarsi sul video. :-)