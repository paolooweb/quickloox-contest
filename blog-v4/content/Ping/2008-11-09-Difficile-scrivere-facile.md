---
title: "Difficile scrivere facile"
date: 2008-11-09
draft: false
tags: ["ping"]
---

Che io sappia, <a href="http://docs.google.com" target="_blank">Google Docs</a> è l'unico software per la videoscrittura che nella finestra di riassunto delle statistiche del testo indichi non uno, ma tre diversi indici di leggibilità.

Chi mi corregge?