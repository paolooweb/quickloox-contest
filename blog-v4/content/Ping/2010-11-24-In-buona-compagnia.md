---
title: "In buona compagnia"
date: 2010-11-24
draft: false
tags: ["ping"]
---

Mi hanno chiesto di stimare il numero degli iPad venduti in Italia, dato che Apple non rilascia ufficialmente neanche sotto tortura.

Ho provato a rispondere come si vede qui sotto. La parte interessante, ce ne fosse una, non è la cifra di arrivo, ma il ragionamento e i presupposti. Riuscire a stimare un dato minimamente certo partendo da una approssimazione totale è lavoro ben fatto.

Non è assolutamente detto che la mia stima sia lavoro ben fatto. Quindi invito chi ha qualcosa da dire a dirlo. Stimo che il suo intervento sarà preziosissimo prima ancora di averlo letto.

<i>Provando ad approssimare: sappiamo con certezza di una popolazione di sette milioni di iPad a livello globale a fine settembre. Possiamo agevolmente supporre che siano oggi dieci milioni.</i>

<i>È plausibile che circa metà degli iPad siano nordamericani. Il punto è come distribuire quei cinque milioni che restano tra Europa e Asia.</i>

<i>Azzardo 3,5 milioni in Europa e 1,5 milioni in Asia, sulla base delle cifre regionali diffuse da Apple (tuttavia non divise per prodotto) e trascuro gli</i> outsider<i>, tipo Australia o Medio Oriente, che dovrebbero avere numeri poco influenti.</i>

<i>L'Italia è per forza tra i primi cinque-sei Paesi Europei, probabilmente con quote non paragonabili a quelle dei Paesi sopra. Se i Paesi dominanti si prendono due milioni di iPad e l'Italia è il primo del gruppo di coda, quanto si prende di quel milione e mezzo?</i>

<i>Duecentomila potrebbe essere attendibile. Ma più o meno cinquantamila ci sta ugualmente.</i>