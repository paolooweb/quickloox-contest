---
title: "Troppo serio per prendersi sul serio<p>"
date: 2005-01-14
draft: false
tags: ["ping"]
---

Chi è il serioso traduttore che si è accanito su iPod Shuffle?<p>

Apple fa cose talmente belle che può permettersi anche di non prendersi sul serio, come quando dice sul Web <a href="http://www.apple.com/ipodshuffle">Non mangiatevi iPod Shuffle</a>.<p>

Ma il traduttore italiano non è evidentemente un uomo Apple: deve essere uno serio, professionale, tutto d&rsquo;un pezzo, che guai a fare un pizzico di ironia, metti mai che il Capo si arrabbi e poi ti guarda male in pausa caffè. Così nella <a href="http://www.apple.com/ipodshuffle">pagina italiana</a> lo ha tolto. Chissà, dopo sarà andato a chiedere un aumento, per evitare la brutta figura che altrimenti avrebbe fatto la sua serissima azienda.<p>

Non è un po&rsquo; triste essere così seri?<p>

Grazie a Dani per la segnalazione!<p>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>