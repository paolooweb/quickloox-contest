---
title: "Presentat&rsquo;&hellip; March"
date: 2006-03-24
draft: false
tags: ["ping"]
---

Non sopporto le presentazioni e i software per le presentazioni. Quando ho potuto ho sempre fatto cose molto semplici in Html, se non addirittura sequenze di immagini in libert&agrave;.

Avevo per&ograve; un problema da poco tempo e molta criticit&agrave; e, disperato (per scrivere dieci righe di Html ci metto dodici ore), ho aperto Keynote 2.

Rimango delle mie opinioni, ma accidenti se &egrave; facile.