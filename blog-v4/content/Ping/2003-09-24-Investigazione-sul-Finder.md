---
title: "Investigazione sul Finder"
date: 2003-09-24
draft: false
tags: ["ping"]
---

Una sciocchezza, una correzione e l’avvio di una indagine sull’Ftp da scrivania

Giorni fa ho scritto una stupidaggine, sostenendo che, quando ci si collega a un sito Ftp dal Finder di Mac OS X (almeno fino a Mac OS X 10.2.x), il fatto di poter fare download ma non poter fare upload dipende dai permessi di accesso impostati sul server.

In linea di principio è esattamente così. Ma il Finder di Mac OS X ha qualcosa di strano e fortunatamente sono stato corretto dal bravissimo Riccardo Ghiglianovich. Mai parlare senza avere verificato in pratica e di persona! Il Finder di Mac OS X non consente di fare upload a un sito ftp, <link>come riconosce Apple stessa</link>http://docs.info.apple.com/article.html?artnum=107415&sessionID=anonymous%7C23760366&kbhost=kbase.info.apple.com%3a80%2f, consigliando di usare altri programmi.

Riccardo ha provato e riprovato e ha avanzato l’ipotesi che la cosa dipenda da un problema nel modulo csmount del sistema. Ora però resta da capire se sia un bug (e se verrà corretto in Mac OS X 10.3) oppure se sia una scelta conscia da parte di Apple. Chi ci aiuta?

<link>Lucio Bragagnolo</link>lux@mac.com