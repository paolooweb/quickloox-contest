---
title: "Il Python in salotto"
date: 2007-02-05
draft: false
tags: ["ping"]
---

Mentre leggo la posta svaccato sul divano leggo in posta un thread su HyperCard e su quanto era bello, facile e via discorrendo.

Tutto vero, ma dopo la versione 2.4.1 (che parte regolarmente sul mio Classic) non c'è più stato niente e se uno vuole fare scripting organizzato con strumenti attuali adatti all'attuale deve prendere altre strade.

Per curiosità ho provato a installare <a href="http://pythoncard.sourceforge.net/" target="_blank">PythonCard</a>, che onestamente della semplicità e della immediatezza di HyperCard ha solo il suffisso.

Se qualcuno prova, occhio a un erroraccio nell'installazione: quello che, stando ai <a href="http://pythoncard.sourceforge.net/macosx_tiger_installation.html" target="_blank">readme sul sito</a>, dovrebbe finire in <code>/Library/Frameworks/Python.framework/Versions/2.4/lib/python2.4/site-packages/PythonCard/</code>, in realtà viene cacciato dentro <code>/System/Library/Frameworks/Python.framework/Versions/2.3/lib/python2.4/site-packages/PythonCard/</code>. Basta prendere la cartella PythonCard e spostarla all'indirizzo giusto perché tutto funzioni. Inoltre tocca installare un Python più aggiornato di quello di serie e va anche bene, solo che il Python più aggiornato possibile è a una versione ancora superiore e non ho verificato che PythonCard, messo nella directory giusta, funzioni ugualmente anche con Python 2.5.

Sotto Python 2.4.3. funziona anche bene. Devo ancora capire quanto sia agevole impostare un'interfaccia (cosa che in HyperCard non era programmazione, ma divertimento) e naturalmente le mie conoscenze su Python sono invero limitate. Ho però la sensazione che leggendo il <a href="http://pythoncard.sourceforge.net/walkthrough1.html" target="_blank">tutorial sul sito</a> si possa fare qualcosina di interessante in tempo ragionevolmente breve.

Il genio di Bill Atkinson non ce lo ridarà nessuno. È il tempo dell'open source. Si può fare tutto, ma in cambio devi metterci sempre un po' del tuo.