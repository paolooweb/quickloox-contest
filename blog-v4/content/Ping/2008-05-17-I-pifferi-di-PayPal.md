---
title: "I pifferi di PayPal"
date: 2008-05-17
draft: false
tags: ["ping"]
---

Poche settimane fa avevano parlato di <a href="http://www.macworld.it/showPage.php?id=14390&amp;template=notizie">respingere all'ingresso</a> i browser, come Safari, che non adottavano il sicurissimo Extended Validation Ssl per le transazioni sicure su Internet.

Oggi lo stesso sistema per cui si doveva snobbare Safari si scopre <a href="http://www.theregister.co.uk/2008/05/16/paypal_page_succumbs_to_xss/" target="_blank">gravemente vulnerabile</a> a un attacco via <em>scripting</em>.

Andarono per suonare.