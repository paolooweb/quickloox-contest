---
title: "Un futuro senza Flash"
date: 2009-08-06
draft: false
tags: ["ping"]
---

Un'altra <i>demo</i> delle possibilità di Html 5 e JavaScript.

<a href="http://9elements.com/io/projects/html5/canvas/" target="_blank">Ogni punto colorato è un <i>tweet</i> su Twitter</a>. Cliccarlo (è un'impresa!) visualizza il <i>tweet</i>.

Html 5 è appena agli inizi e già permette effetti a questo livello. A che cosa servirà Flash?