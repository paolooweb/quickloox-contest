---
Ètitle: " primavera, svegliatevi friulani"
date: 2006-03-22
draft: false
tags: ["ping"]
---

Da stasera iniziano a Udine i <a href="http://nuke.morettibar.it/Eventi/tabid/72/Default.aspx" target="_blank">MacSpritz</a>: gli aperitivi Mac del mercoledì.

Sono quelle volte in cui essere in zona Milano è esattamente uno svantaggio. Spero proprio mi capiti l’occasione di passare al momento giusto e godermi l’ospitalità giuliana, che ho già sperimentato ed è deliziosa.

Così avrò anche l’opportunità di ringraziare di persona <a href="http://www.crisalide-design.net/" target="_blank">Lara</a> per la segnalazione.