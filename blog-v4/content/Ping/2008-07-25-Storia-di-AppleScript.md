---
title: "Storia di AppleScript"
date: 2008-07-25
draft: false
tags: ["ping"]
---

Giusto perché è stato concepito nel 1989 e bisogna godersi come si deve l'ultimo anno da <em>teenager</em>.

Per oggi (eccezione che conferma la regola) niente script e invece calda raccomandazione di scorrere <a href="http://www.cs.utexas.edu/~wcook/Drafts/2006/ashopl.pdf" target="_blank">questo documento in formato Pdf</a> che tratta di AppleScript dalla nascita. Come venne cassata l'idea della modalità normale e quella Professional, gli esercizi per essere sicuri di avere capito bene come funziona il linguaggio, grafici che non si trovano da nessun'altra parte e strutture di <em>parsing</em>.

Adesso che ho scritto <em>parsing</em>, lo leggeranno solo i coraggiosi. Meglio.