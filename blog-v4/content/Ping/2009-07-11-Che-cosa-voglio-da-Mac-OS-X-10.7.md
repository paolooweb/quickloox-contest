---
title: "Che cosa voglio in Mac OS X 10.7"
date: 2009-07-11
draft: false
tags: ["ping"]
---

Snow Leopard oramai è cosa fatta, tempo di pensare al futuro.

La prima cosa che voglio: le preferenze dinamiche.

Apro il file di un capitolo del libro che sto scrivendo e, putacaso, il font predefinito è Palatino 15.

Se apro un documento vuoto da l&#236;, il font predefinito è Palatino 15. Mentre, se apro un Ping sempre scritto con BBEdit ed eseguo la stessa operazione, il font è Consolas 14, perché i Ping sono in Consolas e i capitoli del libro in Palatino.

Oppure: faccio affari con l'Europa e la Cina. Ho una cartella Europa con i fogli di calcolo in euro e una cartella Cina con i fogli di calcolo in renmimbi.

Quando lavoro nella cartella Europa, Numbers e gli altri programmi sono impostati sull'euro. Quando lavoro nella cartella Cina, sono impostati sul renmimbi.