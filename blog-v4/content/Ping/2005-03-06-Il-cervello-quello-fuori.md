---
title: "Quando c&rsquo;è il cervello c&rsquo;è tutto<p>"
date: 2005-03-06
draft: false
tags: ["ping"]
---

La corsa alla Ram e ai gigahertz serve fino a un certo punto<p>

Ci sono categorie dove è importante avere il Mac più veloce e dotato esistente e nient&rsquo;altro conta. Video, 3D e altro ancora. Gli abitanti di queste categorie sono un&rsquo;esigua minoranza rispetto al totale.<p>

Per tutti gli altri il computer resta un meraviglioso amplificatore di intelligenza. Se uno ci sa fare, fa grandi cose. Se uno vabbeh, vabbeh anche se il computer è bello grosso.<p>

L&rsquo;amico <a href="http://homepage.mac.com/riccardo.mori/blogwavestudio/">Riccardo</a> ha recuperato un vecchio Power Macintosh 9500, su cui a rigore Mac OS X non dovrebbe neanche girare, e ci ha installato con successo Puma (10.1.x). La macchina ha 292 megabyte di Ram e, sappilo, è utilizzabile per lavorarci. Certo non è un dual G5. Ma fuori dalle categorie di cui sopra, non conta il cervello dentro il computer. Conta quello fuori.<p>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>