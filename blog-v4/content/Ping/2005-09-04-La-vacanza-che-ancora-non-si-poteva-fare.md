---
title: "La vacanza che ancora non si poteva fare<p>"
date: 2005-09-04
draft: false
tags: ["ping"]
---

Però visitare un mondo veramente diverso costerà poco<p>

La possibilità è sempre esistita, tra Mud e altro, ma è inevitabilmente con la grafica che i mondi alternativi stanno veramente diventando fenomeno di massa.<p>

Se sostituiscono il mondo reale, sono patologici; presi in piccole dosi, come antidoto ai logorii delle vite moderne e alle fatiche più o meno lavorative, sono piacevoli, rilassanti e una bella gratificazione anche se fuori piove e non si può fare una scampagnata.<p>

A parte <a href="http://www.worldofwarcraft.com/">World of Warcraft</a>, sempre raccomandato, a giorni uscirà <a href="http://www.mystvgame.com/us/">Myst V</a> e da poco è stato inaugurato il sito ufficiale. Da visitare.<p>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>