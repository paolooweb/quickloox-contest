---
title: "Programmi da non consigliare"
date: 2008-02-08
draft: false
tags: ["ping"]
---

<a href="http://www.sause.org/folderteint/" target="_blank">FolderTeint</a>. In due parole, le regolazioni di iPhoto a disposizione per le icone del Finder di Leopard.

A parte essere una delle prove che le tecnologie di base di Mac OS X sono una manna per i programmatori (senza Core Animation, FolderTeint non sarebbe mai esistito o giù di l&#236;), è software che modifica file del sistema piuttosto in profondità e l'autore stesso ricorda che roba cos&#236; si usa a proprio rischio.

Però i risultati sono maledettamente eleganti.