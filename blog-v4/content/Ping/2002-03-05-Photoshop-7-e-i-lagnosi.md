---
title: "Photoshop 7 e i lagnosi a oltranza"
date: 2002-03-05
draft: false
tags: ["ping"]
---

L’ignoranza provoca brutti effetti, specie quando riguarda un sistema operativo Unix

È arrivato Photoshop 7. Una bella impresa da parte di Adobe, specie trattandosi di fare convivere nel modo migliore la memoria virtuale di Mac OS X con quella del programma.
Ho già sentito un lagnoso a oltranza lamentarsi che non è veloce come si aspettava. Bene, non può esserlo. Unix non permette a nessun programma di impadronirsi completamente del computer come faceva Mac OS 9. Di conseguenza lavorare alla vecchia maniera, condizionati ad aspettare che Photoshop abbia finito, rende meno.
In Mac OS X, anche mentre Photoshop applica un filtro lentissimo, si può sempre passare a un altro programma, magari Illustrator, o semplicemente l’email.
Il bravo professionista, quindi, sfrutterà al massimo il fatto di non essere più condizionato da Photoshop. Quello cattivo si lagnerà.

<link>Lucio Bragagnolo</link>lux@mac.com