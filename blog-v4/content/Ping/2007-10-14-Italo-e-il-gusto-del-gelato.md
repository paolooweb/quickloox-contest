---
title: "Italo e il gusto del gelato"
date: 2007-10-14
draft: false
tags: ["ping"]
---

Italo era salito alla modesta ribalta di questo blog un paio di anni fa, con <a href="http://www.macworld.it/blogs/ping/?p=857" target="_blank">un bel progetto</a> didattico per la scuola elementare realizzato a base di tanta buona volontà e di un Mac mini.

Italo ci ha preso gusto: è di pochi giorni fa la sua gentile segnalazione del progetto <a href="http://www.ilnarratore.it/index.php?archivio_audio_id=19" target="_blank">Viva il mio gelato</a>: un Cd audio creato da bimbi di prima elementare, venduto per beneficenza (ricavandone denaro donato a un ospedale per acquistare materiale di studio per bambini costretti a imparare da pazienti) e poi regalato in 500 copie allo stesso ospedale, per dare a quegli stessi bambini sfortunati qualcosa in più da ascoltare durante le giornate di degenza che non passano mai.

Italo ha usato mezzi ben più potenti, stavolta; oltre al Mac mini e alla buona volontà, ha aggiunto nientemeno che un paio di cuffie.

Italo è un piccolo grande eroe del nostro tempo, che fa cose concrete e fa progredire il mondo, dimostrando che si possono fare cose belle con poco intanto che alla televisione i suoi capi danno uno spettacolo indecoroso e si curano di tutto, tranne che dei docenti e degli studenti, a cui fanno mancare persino il poco.

Italo si merita un grazie.