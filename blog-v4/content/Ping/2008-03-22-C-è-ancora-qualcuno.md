---
title: "C'è ancora qualcuno"
date: 2008-03-22
draft: false
tags: ["ping"]
---

Interessa credo solo a me in Italia, ma voglio annotare che c'è ancora spazio nel mondo del software Mac per <a href="http://rephial.org/release/3.0.9b" target="_blank">un gioco della preistoria dell'informatica</a> che però riceve ancora l'attenzione e le risorse per diventare Universal.

E nei prossimi cinquant'anni arriverò a fare fuori il malvagio Morgoth senza barare, costi quel che costi.