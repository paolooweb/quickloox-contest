---
title: "Pensieri software"
date: 2008-05-12
draft: false
tags: ["ping"]
---

Trovo un giochino semplice e veloce (non tanto semplice e non tanto veloce), in realtà), deliziosamente retro e antiquato: <a href="http://www.zincland.com/powder/index.php?pagename=news" target="_blank">Powder</a>.

Sono già preso da <a href="http://rephial.org" target="_blank">Angband</a> per dedicarmi seriamente a un altro <a href="http://roguebasin.roguelikedevelopment.org/index.php?title=Main_Page" target="_blank">roguelike</a>, ma è proprio carino.

Da una parte, è un gioco portato su Mac da Gameboy Advance. Penso sia una cosa bella.

Dall'altra, qualcuno mi dirà sicuramente che è un giochino e che non vale la pena di entusiasmarsi tanto.

Qualcuno mi dirà che su Windows ci sono molti più giochi&#8230; contando anche quelli che vengono portati da Gameboy Advance su Windows.

A margine, chi sapeva che esistono <a href="http://roguebasin.roguelikedevelopment.org/index.php?title=7DRL" target="_blank">norme ufficiali</a> per quelli che decidono di scrivere u <em>roguelike</em> in non più di una settimana? In marzo si è perfino tenuto un <a href="http://roguebasin.roguelikedevelopment.org/index.php?title=7DRL_Contest_2008" target="_blank">evento ufficiale</a> e ci hanno provato in 24, di cui dieci con successo. Fuori dall'evento, ci hanno provato in otto e cinque ci sono riusciti.