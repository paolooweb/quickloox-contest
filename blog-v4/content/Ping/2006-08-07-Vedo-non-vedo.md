---
title: "Vedo-non-vedo"
date: 2006-08-07
draft: false
tags: ["ping"]
---

Finalmente si potrà avere una videochat con uno sfondo preordinato, non necessariamente quello naturale.

Siamo a un passo dai videocast personalizzati. Praticamente Hyde Park Corner nella quarta dimensione.