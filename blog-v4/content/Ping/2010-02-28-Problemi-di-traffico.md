---
title: "Problemi di traffico"
date: 2010-02-28
draft: false
tags: ["ping"]
---

Ecco come la Regione Lombardia <a href="http://www.regione.lombardia.it/shared/ccurl/162/560/BLOCCO_TRAFFICO_DOMENICA_28_FEBBRAIO_1750.pdf" target="_blank">crea documenti Pdf</a>.

Per qualcuno il blocco del traffico riguarda quello di ossigeno verso il cervello.

Grazie a <a href="http://boldlyopen.com/" target="_blank">Gianugo</a> per avermi&#8230; guidato alla notizia!