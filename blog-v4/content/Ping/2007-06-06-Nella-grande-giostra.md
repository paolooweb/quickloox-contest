---
title: "Nella grande giostra"
date: 2007-06-06
draft: false
tags: ["ping"]
---

Guy Kawasaki ha compreso l'<a href="http://truemors.com" target="_blank">essenza del gossip</a> nell'era del web 2.0 e tutti abbiamo scoperto di essere almeno un giro indietro.

No, non lo frequenterò. Già mi danno fastidio gli spazzaturai italiani, figuriamoci quelli globali. Però il <em>concept</em> è formidabile.