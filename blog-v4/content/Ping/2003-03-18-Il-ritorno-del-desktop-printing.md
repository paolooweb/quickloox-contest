---
title: "Il ritorno del desktop printing"
date: 2003-03-18
draft: false
tags: ["ping"]
---

Il Finder di Mac OS X non ha il comando Stampa. Però...

Da Mac OS X 10.2.3 è tornata la stampa da scrivania, o almeno qualcosa che le somiglia assai.

Infatti non ci sono le stampanti di scrivania, ma c’è Print Center, ops, Centro Stampa. Se trascini un documento sopra l’icona di Centro Stampa o dentro la finestra della coda di stampa di una stampante, provochi l’invio alla stampante stessa di un evento “print document”. Si apre l’applicazione cui è assegnato il documento e, da quel momento, dipende se l’applicazione è scritta come Apple comanda o meno. Ma stampare, si stampa.

Trascinare una cartella invece che un documento non sortisce ancora una stampa elegante come avveniva con il Finder di Mac OS 9, ma l’elenco dei file viene comunque stampato.

Insomma: come nella vita vera, anche Mac OS X permette di stampare file trascinandoli verso il centro stampa più vicino.

<link>Lucio Bragagnolo</link>lux@mac.com