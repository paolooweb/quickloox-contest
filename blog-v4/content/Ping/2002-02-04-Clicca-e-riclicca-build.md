---
title: "Clicca e riclicca che ti passo il build"
date: 2002-02-04
draft: false
tags: ["ping"]
---

Apple non lo dice apertamente, ma c’è un modo facile per capire che versione di Mac OS X abbiamo installato

Unix, cioè Darwin, cioè Mac OS X, è un animale diverso da Mac OS classico. Per esempio diventa molto più facile andare a modificare a colpo sicuro un solo piccolo minuscolo file da aggiornare, et voilà! Ecco pronta una nuova versione del sistema. In gergo i programmatori la chiamano build, più o meno costruzione.
Può diventare quindi importante capire che numero di build abbiamo installato. Apple fino a poco tempo fa lo riportava nella finestra Info su questo Mac, accessibile dal menu Apple. Adesso non più... ma solo apparentemente.
Infatti è sufficiente cliccare sulla parola Version all’interno della finestra per vedere il numero di build. I più fortunati, con un secondo clic, potranno persino vedere il numero di serie del proprio Mac.
Alla faccia della trasparenza (no, la splendida interfaccia Aqua non c’entra).

lux@mac.com