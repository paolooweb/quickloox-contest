---
title: "Vendesi a peso"
date: 2006-02-23
draft: false
tags: ["ping"]
---

Il mio PowerBook costa, di listino Apple Store, 84 eurocentesimi al grammo.

Da notare che, se fosse costruito peggio, costerebbe di pi&ugrave;. Se, infatti, a parit&agrave; di prestazioni pesasse di pi&ugrave;, il suo costo per grammo sarebbe inferiore.