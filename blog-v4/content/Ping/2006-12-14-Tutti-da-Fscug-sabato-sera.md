---
title: "Tutti da Fcsug sabato sera"
date: 2006-12-14
draft: false
tags: ["ping"]
---

Ricevo e assai volentieri rilancio dall'eccezionale <a href="http://www.finalcutstudio.it" target="_blank">Final Cut Studio User Group</a>:

<cite>Sabato 16 Dicembre saremo tutti insieme a fare una pizzata in compagnia, e per vedere insieme qualche trucchetto sul montaggio video e su come fare un sito web con iWeb.</cite>

<strong>Programma della serata:</strong>
<cite>Ritrovo alle ore 18:00 in via della viale della Repubblica, 21/B Bologna, presso Mac It, che ci ha gentilmente messo a disposizione la sala.</cite>

<cite>Vedremo come si crea un sito internet con iWeb e come si monta un video con iMovie e Final Cut, (partiamo dalle basi quindi comprensibile a tutti).
</cite>

<cite>Alle 20, grazie alla banda di 20 megabit in uscita, apriremo videochat con gli altri presid&#238; in giro per l'Italia dove si stanno svolgendo gli altri meeting organizzati da</cite> <a href="http://www.italiamac.it" target="_blank">ItaliaMac</a>.

<cite>Da non perdere: il mercatino per vendere e comprare Mac e accessori usati&#8230; portate quello che non usate più.</cite>

<cite>Proseguiremo la serata con le solite chiacchiere e lo scambio di Tips&#38;Hints fin che non ci verrà fame, a quel punto (verso le 21) ci sposteremo alla &#8220;pizzeria di pino&#8221; in via Stalingrado 42.</cite>

<cite>Mandate una <a href="mailto:pro@finalcutstudio.it" target="_blank">mail</a> per confermare che ci siete.</cite>

<cite>Vi aspettiamo,
Il Final Cut Studio UG</cite>

Non so se riuscirò a farcela ma, nel dubbio, buona pizzata a tutti!