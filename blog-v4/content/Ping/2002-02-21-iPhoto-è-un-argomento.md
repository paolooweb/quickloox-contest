---
title: "iPhoto è un argomento"
date: 2002-02-21
draft: false
tags: ["ping"]
---

La strategia del digital hub è una soluzione per soddisfare le persone normali. Con un Mac

Sento uno che si scandalizza perché gli avevano promesso chissà che cosa e invece iPhoto, guarda un po’, non sostituisce Photoshop a prezzo zero.
Ma iPhoto non è un programma per fare foto ritocco, come iTunes non è un programma per musicisti.
iPhoto serve alle persone normali che non vogliono preoccuparsi del loro computer ma solo arrivare a fare cose belle con le proprie foto, in modo semplice e veloce.
C’è già gente che quasi quasi, pur di poter creare con comodità il proprio album fotografico, quasi quasi un iMac se lo prende.
iPhoto non è un prodotto; è un argomento. Per vendere più Mac e aumentare il numero di persone che grazie a Mac si facilitano un po’ la vita.
Vuoi Photoshop gratis? Suda un’oretta e installati Gimp. O proprio deve esserti tutto dovuto?

<link>Lucio Bragagnolo</link>lux@mac.com