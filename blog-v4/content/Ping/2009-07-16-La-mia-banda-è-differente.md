---
title: "La mia banda è differente"
date: 2009-07-16
draft: false
tags: ["ping"]
---

Che si fa se Apple ha una pagina di <a href="http://www.apple.com/it/macosx/refinements/enhancements-refinements.html" target="_blank">novità di Snow Leopard</a> ma non la traduce? Ci si pensa noi, una novità al giorno.

<b>Due terzi in meno di banda necessaria</b>

iChat richiede solamente un terzo della banda in trasmissione precedente per ottenere chat video 640 x 480 di qualità quasi Dvd. Leopard richiede banda in trasmissione di 900 kilobit per secondo; Snow Leopard richiede solo 300 kilobit per secondo.

Per le Adsl-truffa italiane è una gran boccata di ossigeno. E io sarò uno zio più nitido per la nipotina.