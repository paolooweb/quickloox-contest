---
title: "Un sistema, tre iPhone"
date: 2009-06-22
draft: false
tags: ["ping"]
---

Apple ha pubblicato <a href="http://support.apple.com/kb/HT3630" target="_blank">una tabella piuttosto utile</a> per capire su che versioni di iPhone funziona ciascuna delle novità principali di iPhone OS 3.0.

Su iPhone originale c'è di mezzo il <i>jailbreak</i> obbligatorio, che potrebbe portare varianti imprevedibili. Sul mio, per esempio, non vedo traccia di Spotlight, che in teoria dovrebbe funzionare. In compenso gli Sms ora partono a velocità decupla.