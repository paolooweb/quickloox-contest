---
title: "Guardami mentre ti parlo"
date: 2009-07-16
draft: false
tags: ["ping"]
---

Il sole scotta, l’estate avanza, del mio Snow Leopard non ho abbastanza. Per dare un poco di varietà, propongo in rima la <a href="http://www.apple.com/macosx/refinements/enhancements-refinements.html" target="_blank">novità</a>.

<b>Visualizzazione rapida in iChat</b>

Quando un contatto invia un file tramite iChat, lo si può vedere istantaneamente in anteprima con la Visualizzazione rapida (Quick Look).

Se lo integrano con iPhoto, toccherà pure di dargli il voto.