---
title: "Umano e bestiale"
date: 2007-12-09
draft: false
tags: ["ping"]
---

Sono cose banali, ma ho scoperto or ora che Lisp riesce a fare calcoli con le frazioni.

Se scrivo

<code>(+ 2/3 3/4)</code>

ottengo come risultato

<code>17/12</code>

Ed è anche furbo, perché se faccio

<code>(+ 2/3 3/4 1/12)</code>

Il risultato è

<code>3/2</code>

già semplificato!

Il calcolo delle frazioni è quello che distingue l'uomo dalla bestia meccanica e Lisp, in questo caso, è un bell'anello di congiunzione.

Che tu sappia, esiste qualche altro linguaggio che esegua nativamente il calcolo con le frazioni? (nativamente, chiaro; un programmatore bravo arriva a tutto)

P.S.: per avere Lisp su Mac OS X ci sono diversi sistemi (una <a href="http://www.macupdate.com/search.php?arch=all&amp;keywords=lisp&amp;os=macosx" target="_blank">ricerca su MacUpdate</a> ne svela qualcuno). Inoltre si può installare Common Lisp con <a href="http://fink.sf.net" target="_blank">Fink</a> o <a href="http://www.macports.org" target="_blank">MacPorts</a>. E <code>emacs</code>, nel Terminale, contiene di suo un ambiente Lisp.