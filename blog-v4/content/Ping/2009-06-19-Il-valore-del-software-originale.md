---
title: "Il valore del software originale"
date: 2009-06-19
draft: false
tags: ["ping"]
---

Una bimba gravemente malata di cancro voleva a tutti i costi vedere <a href="http://disney.go.com/disneypictures/up/" target="_blank">Up</a>, l'ultimo film Disney/Pixar. Ma era allo stremo delle forze e non sarebbe mai arrivata ad alcun cinema, unico luogo dove è attualmente possibile vedere la &#8220;pellicola&#8221;.

Un'amica di famiglia ha chiamato Pixar, aggirando il sistema automatico di risposta e riuscendo a spiegare la situazione.

Da Pixar sono partiti con un Dvd apposta per lei, più <i>peluche</i> del film, <i>poster</i> e <i>merchandising</i>. Sono arrivati a casa della bimba e hanno organizzato una proiezione esclusiva. Lei è stata contenta ed <a href="http://www.ocregister.com/articles/pixar-up-movie-2468059-home-show" target="_blank">è morta sette ore dopo</a> che il suo ultimo desiderio era stato esaudito.