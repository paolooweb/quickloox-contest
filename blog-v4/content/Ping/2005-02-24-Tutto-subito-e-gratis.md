---
title: "Tutto, subito e gratis<p>"
date: 2005-02-24
draft: false
tags: ["ping"]
---

Una Cpu non riesce ancora a sostituirci, ma non perché sia troppo stupida<p>

Leggo su una mailing list qualcosa del genere:<p>

<em>Devo fare un sito amatoriale ma non ne capisco niente. Cercavo un programma capace di fare tutto al posto mio, possibilmente gratis.</em><p>

Giuro che ho fatto qualche modifica, ma lo spirito è esattamente questo.<p>

Secondo me il datore di lavoro di quest&rsquo;uomo sta pensando <em>chissà se c&rsquo;è un programma che fa gratis le stesse cose che sta facendo xxx</em>&hellip;<p>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>