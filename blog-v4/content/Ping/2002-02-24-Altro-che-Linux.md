---
title: "Altro che Linux"
date: 2002-02-24
draft: false
tags: ["ping"]
---

A leggere i giornali sembra che, se si parla di un sistema open source, si stia per forza parlando di Linux. Invece...

Non è ancora evidentissimo a tutti che Mac OS X è costruito su una base open source derivata dal sistema Unix FreeBSD, di nome Darwin.
Siccome Mac OS X viene inserito in tutti i Mac che vengono venduti e il pacchetto in quanto tale è in vendita da un anno, ne consegue che il numero di desktop che montano BSD è ampiamente maggiore di quelli che montano Linux; secondo un responsabile Apple partecipante alla recente BSD Conference, grazie a Mac OS X, BSD batte Linux tre a uno.
E ha anche le icone più grandi delle loro, cicca cicca.

<link>Lucio Bragagnolo</link>lux@mac.com