---
title: "A pesca di gusci"
date: 2009-03-28
draft: false
tags: ["ping"]
---

Quando è ora è ora. Dopo avere sopportato per anni <em>bash</em>, la <em>shell</em> (il motore Unix, anche se la parola sta per <em>guscio</em>) che sta dietro il Terminale di Mac OS X, oggi ho deciso che è ora di cambiare. Di nuotare in acque diverse. Di muovermi controcorrente.

Voglio una <em>shell</em> più moderna e versatile, adatta anche a un non esperto. <em>bash</em> mi ha fatto venire le squame.

Ho deciso che voglio <a href="http://www.fishshell.org/" target="_blank">Fish</a>. Almeno per provarla tre giorni e vedere se puzza.