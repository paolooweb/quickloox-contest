---
title: "La pubblicità è l'anima del popup"
date: 2007-07-27
draft: false
tags: ["ping"]
---

Diavolo di un <strong>Alan Dragster</strong>!

A me sarà capitato mezzo milione di volte di visitare qualche sito e vedere un popup che dice <em>Hai un virus, scarica questo file</em>. Su Internet gira di tutto, tanto ho un Mac, chissenefrega.

Lui, invece, quando gli capita su VersionTracker si mette a scrivere agli autori del sito (e gli rispondono). Poi scopre <a href="http://www.mikeonads.com/2007/07/04/errorsafe-on-careerbuildercom/" target="_blank">materiale tecnico interessante</a> in argomento&#8230; e insomma se facessimo tutti come lui di finta pubblicità che poi ti becchi il virus ce ne sarebbe molta meno.

Se facessimo tutti come lui, d'altronde, avremmo tutti Mac. Complimenti.