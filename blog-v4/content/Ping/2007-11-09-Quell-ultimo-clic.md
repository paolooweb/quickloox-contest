---
title: "Quell'ultimo clic"
date: 2007-11-09
draft: false
tags: ["ping"]
---

Minuterie da Safari: quello di Leopard riesce a cliccare il pulsante Send di Gmail procedendo solo da tastiera, senza toccare il mouse (sempre stato possibile con Firefox, ma non con Safari). Per quanti, come me, minimizzano l'uso del mouse è una mano santa.