---
title: "Mac e Masoch"
date: 2004-01-29
draft: false
tags: ["ping"]
---

Che cosa non si fa pur di imparare qualcosa

Ho il viziaccio di aprire applicazioni, documenti, utility, qualsiasi cosa e non chiudere mai niente. Alla fine ci sono venti-trenta applicazioni aperte, alcune di esse con anche venti o trenta finestre.

Nel bel mezzo di uno di questi deliri è andato in crash il Finder. Il resto del sistema continua a funzionare egregiamente e ci sono aperte troppe cose che sarebbe scomodo andare a riaprire tutte a seguito di un logout. Così ho deciso di andare avanti a lavorare come se niente fosse, usando il Terminale là dove servirebbe il Finder.

Che c’è di strano? direbbe qualcuno che si intende di Unix. C’è gente che vive tutto il giorno nel Terminale senza fare una piega. Già; ma io di Unix so al massimo tre cose, e confuse quanto basta.

Così mi ritrovo a digitare cd e ls per navigare tra le cartelle, usare il tabulatore per risparmiarmi la digitazione integrale dei nomi file, andando a cercare i dischi nella cartella /Volumes e via dicendo. Beh, funziona, e si acquisisce un sacco di familiarità.

È roba da masochisti ed è stata decisa per puro caso, ma si sta trasformando in un piccolo, utile corso di Unix elementare.

<link>Lucio Bragagnolo</link>lux@mac.com