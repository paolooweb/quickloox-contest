---
title: "Una risistematina all'Ufficio"
date: 2006-11-03
draft: false
tags: ["ping"]
---

L'amico <strong>Carlo Gandolfi</strong> di <a href="http://www.freesmug.org" target="_blank">Freesmug</a> se la prenderà a morte (scherzo!), ma uso <a href="http://www.openoffice.org" target="_blank">OpenOffice</a> e non <a href="http://www.neooffice.org" target="_blank">NeoOffice</a>.

Ho installato la versione 2.0.4 <em>final</em> di Open Office e merita decisamente, con un restyling grafico e soprattutto l'uso (opzionale) dei font di sistema del Mac al posto di quelli dell'ambiente X11.

Lo uso giusto per avere la compatibilità con l'Office che si paga, quello dei monopolisti disonesti. Averlo più funzionale e carino, comunque, mica dispiace.