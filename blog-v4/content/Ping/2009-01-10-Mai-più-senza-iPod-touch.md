---
title: "Mai più senza iPod touch"
date: 2009-01-10
draft: false
tags: ["ping"]
---

Mai più senza Bruce Tognazzini, <em>guru</em> delle interfacce. E Grazie ad <a href="http://www.accomazzi.net" target="_blank">Akko</a> che mi ha segnalato questo <a href="http://www.asktog.com/columns/074KindleVsiPhone.html" target="_blank">studio parallelo</a> tra iPhone/iPod touch e Kindle, il libro elettronico di Amazon.

Tog, come si è sempre firmato, la fa lunga ma il pezzo merita assolutamente la lettura. Se Google Translate non aiuta chi ha problemi con l'inglese, fai sapere e darò una mano.

Cito due paragrafi, quelli che compongono la sezione <em>Buying Advice</em> (consigli per l'acquisto).

<cite>Non vorrete più fare a meno di ambedue i prodotti. Proprio per questo, ironicamente, raccomando di non acquistare un Kindle di prima generazione. Voltare pagina è una frustrazione senza fine. Tuttavia sospetto che i modelli di prima generazione siano esauriti e che Amazon stia consegnandone di seconda generazione.</cite>

<cite>Riguardo a iPhone, compratelo adesso. Ogni giorno mi chiedo come io abbia fatto senza, prima di acquistarlo. È un assistente meraviglioso per il gioco e il lavoro e le imperfezioni di cui ho discusso prima sono solo software, quindi spariranno. Pensavo che iPod touch, con la sua connettività via </cite>wi-fi<cite>, fosse abbastanza, mentre avere una connessione praticamente costante a Internet cambia la vita.</cite>

Mai più senza iPhone o iPod touch. Mai più senza Kindle? Boh. Ha venduto meno esemplari di <a href="http://itunes.apple.com/WebObjects/MZStore.woa/wa/viewSoftware?id=284956128&amp;mt=8" target="_blank">Stanza</a>, che è solo uno degli <em>e-reader</em> per iPhone e iPod touch.