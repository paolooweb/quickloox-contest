---
title: "Beccati questa Eddington"
date: 2009-03-04
draft: false
tags: ["ping"]
---

Arthur Eddington è famoso per il teorema delle scimmie infinite. Dato loro tempo infinito, passato a pestare su un numero infinito di tastiere, possono produrre qualunque testo, persino la Divina Commedia o l'Ulisse di Joyce.

Le scimmie di Eddington sono enti matematici e non vanno prese alla lettera. Però mi piace pensare, in tempi in cui si confondono gli oranghi con le persone, che bastino pochi selezionati esseri umani e poche ore per creare quanto, appunto, le scimmie tireranno fuori solo in un tempo infinito e soltanto se sono sufficientemente infinite.

Perciò saluto volentieri il debutto del <a href="http://www.ilmacaco.com/" target="_blank">Macaco</a>, creatura di <strong>Daniele</strong>, cui auguro tutte le fortune.

Prendo dalla sua mail fondatrice gli intenti programmatici che lo hanno spinto ad agire:

- <cite>sono stanco di vedere, nel mio lettore Rss su iPhone, notizie tutte uguali, spesso semplici traduzioni di blog americani o di</cite> veline <cite>di agenzie giornalistiche, pubblicate sui maggiori</cite> blog <cite>Apple;</cite>
- <cite>sono stanco di vedere notizie del tipo</cite> domani Apple presenterà il nuovo iLift, il primo ascensore Apple<cite>, e poi puntualmente il giorno dopo</cite> Apple non ha presentato l'iLift, che schifo!<cite>;</cite>
- <cite>penso che i congiuntivi abbiano una dignità e debbano pur essere difesi da qualcuno&#8230;</cite>

A favore della proliferazione dei macachi e dell'estinzione delle scimmie urlatrici.