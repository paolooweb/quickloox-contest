---
title: "'Sti scienziati"
date: 2010-06-02
draft: false
tags: ["ping"]
---

Se c'è qualcosa che riassume lo spirito di Internet, è una <a href="http://stixfonts.org/" target="_blank">raccolta di font</a> <i>open source</i> completo di tutto il necessario apparato Unicode per gli usi in ambito tecnico, scientifico e medico.

L'<i>open source</i> è una cosa cos&#236; preziosa che la gente dovrebbe essere disposta a pagare per averlo.