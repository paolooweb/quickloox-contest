---
title: "Il quasi ritorno di HyperCard"
date: 2004-02-23
draft: false
tags: ["ping"]
---

Per chi è rimasto affezionato a quella fantastica interfaccia di una volta

Diciamo una verità: HyperCard è superato. Complessivamente, con AppleScript e gli Xcode Tool, Mac OS X offre un ambiente di sviluppo ben più potente, specie considerando altre alternative indipendenti come RealBasci.

Diciamo una verità: esiste una comunità di appassionati che sono rimasti giustamente conquistati dallo strumento e non vogliono abbandonarlo.

Diciamo la verità: è sacrosanto che ognuno possa scegliersi gli strumenti di sviluppo che vuole.

Beh, è arrivato <link>HyperNext</link>http://www.tigabyte.com/default.html. Si compone di un Creator e di un Reader e dispone di un sistema di creazione di plugin. Se non è HyperCard, ci va molto vicino. Chi ha voglia di dargli un’occhiata mi faccia sapere. Magari ritiro fuori i vecchi stack di una volta.

<link>Lucio Bragagnolo</link>lux@mac.com