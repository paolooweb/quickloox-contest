---
title: "Disabile e arruolato"
date: 2007-01-28
draft: false
tags: ["ping"]
---

Mi scrive <strong>Palmy</strong>:

<cite><strong>Carlo Filippo</strong> è un mio amico disabile che ha creato un <a href="http://www.disabiledoc.it" target="_blank">portale</a> per dare voce e visibilità alle esigenze informatiche di chi si trova nella sua stessa condizione.</cite>

<cite>Ha lanciato un <a href="http://www.norisberghen.it/it/?p=336" target="_blank">appello</a> ai siti Mac italiani: se mi dai una mano a diffondere ti ringrazio anche a nome suo. :)</cite>

Nessun bisogno di ringraziare, semplice dovere; felice di poter arruolare nelle fila di Ping un appassionato Mac in più. :)