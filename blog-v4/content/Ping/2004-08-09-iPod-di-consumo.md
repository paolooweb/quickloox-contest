---
title: "Raffinato e di consumo"
date: 2004-08-09
draft: false
tags: ["ping"]
---

Il vero segreto del successo dell’iPod

Le aziende di informatica non sanno fare prodotti per l’elettronica di consumo. Lo ha dimostrato più volte a sue spese Apple, con la console Pippin, il MacTV e qualcos’altro. Microsoft ha messo tutti i suoi muscoli di marketing e concorrenza sleale dietro la Xbox, ma Sony continua a dominare il mercato con la PlayStation 2 dopo avere fatto il vuoto con il primo modello.

iPod, invece, è un oggetto perfetto di elettronica di consumo. Ha una confezione curata e precisa, con gli adesivi messi nel posto giusto, né troppa colla né troppa poca, le protezioni di gomma per gli auricolari dentro bustine sigillate, in una scatola con le giuste dimensioni, dal design elegante e ingegnoso.

Parlato del packaging, del prodotto c’è poco da dire. Funziona, è facile, non c’è bisogno di leggere il manuale. Come un televisore o un frigorifero.

Riuscire a portare un pochino di logica dell’iPod su Mac OS X ne farebbe una piattaforma ancora più formidabile.

<link>Lucio Bragagnolo</link>lux@mac.com