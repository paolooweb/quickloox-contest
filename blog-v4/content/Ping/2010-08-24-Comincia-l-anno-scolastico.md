---
title: "Comincia l'anno scolastico"
date: 2010-08-24
draft: false
tags: ["ping"]
---

Più o meno, chi prima e chi dopo, però insomma a partire da settembre la priorità diventa lo studio.

Se posso permettermi e invitare i miei ventidue lettori, sarebbe bello che fosse priorità anche per chi lavora. Non è più un mondo dove quello che ci è rimasto attaccato dalla scuola di tanti anni fa basta per la vita. Non è neanche più un mondo dove una persona sana e serena possa accettare di trascorrere il resto dei propri giorni a dimenticarsi cose senza almeno pensare di impararne qualcuna in cambio.

È notizia relativamente vecchia, ma questo è il momento giusto: iTunes U a metà agosto aveva superato i 300 mila <i>download</i>.

iTunes U è una fonte straordinaria e inesauribile di contenuti per imparare, di livello anche eccelso. Tutto gratis, senza altro impegno che quello di scaricare quello che arriva e poi scoprire qualcosa di nuovo o qualcosa di più, senza quote di iscrizione, senza esami, senza voti, anche senza vergogna se sono una pera ma provo a cimentarmi lo stesso: ho solo da guadagnarci.

Infuriano in televisione le pubblicità delle collezioni e delle opere a fascicolo: i coltelli da cacciatore, l'automobile in scala uno a cinque, i mazzi di carte della tradizione, le finte stampe antiche, il cimitero degli elefanti.

Se cerchi qualcosa per impegnare una sera ogni tanto, se ancora l'inglese barcolla, se vorresti migliorare un po' il curriculum reale (i pezzi di carta sono quello che sono, ma c'è ancora qualcuno all'ufficio personale che guarda quelli invece di andare su Google e su Facebook?), iTunes U è una risorsa straordinaria, gratis.

Che bello se ognuno di noi si &#8220;iscrivesse&#8221; a un corso e ci ritrovassimo a giugno a scambiarci, a qualsiasi livello, la nostra esperienza, anche solo per dire <i>non ci ho capito un accidente ma è stato bello provare</i>.

Il tutto con un obiettivo diverso da quello di alzare i <i>download</i> di iTunes U: alzare il livello di <i>upload</i> verso la nostra testolina, che avrà solo da ringraziare.

iTunes U è l&#236; che guarda dalla schermata di iTunes Store, in alto nella barra nera. Da l&#236; è un clic, senza impegno.