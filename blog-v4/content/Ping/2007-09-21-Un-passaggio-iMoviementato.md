---
title: "Un passaggio iMoviementato"
date: 2007-09-21
draft: false
tags: ["ping"]
---

Essendo ancora PowerPc, non posso giudicare seriamente il nuovo iMovie di cui molti dicono peste e corna e altri (pochi) dicono che è più adatto alla sua missione (far fare video a chiunque).

Anche se avessi Intel, non sono competente in fatto di video e quindi non potrei giudicare seriamente lo stesso.

Il tutto è pretesto per mostrare <a href="http://gallery.mac.com/odino1#100016" target="_blank">questo filmato</a> montato da Odino a velocità pazzesca. Merito di iMovie '08 o della levitazione magnetica?