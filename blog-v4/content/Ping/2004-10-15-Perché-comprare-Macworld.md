---
title: "Perché comprare Macworld<p>"
date: 2004-10-15
draft: false
tags: ["ping"]
---

C&rsquo;è sempre un motivo, ma la gente preferisce spendere di più e non si capisce, di motivo, il loro<p>

Se c&rsquo;è una cosa veramente pazzesca è che la gente non ha coscienza del valore del suo tempo. Se il tuo tempo è gratis Windows costa meno, uso dire. Tutti calcolano i loro redditi in euro al mese, o all&rsquo;anno, e nessuno che faccia quella divisioncina per capire che ogni giorno, ogni ora, hanno il loro valore.<p>

Questo per dire che nelle mailing list che frequento, in iChat, dovunque mi trovo, brulicano persone che passano il tempo a porre domande, domande, domande. Si sbattono come pazze, inviano messaggi su tutte le liste che trovano, e non sono quasi mai domande stupide o banali.<p>

La mia stima è che, nel giro di un anno, Macworld risponda a tutte le domande che può avere una persona e che, nel contempo, fornisca informazioni sufficienti per un quantitiativo di domande venti volte superiore.<p>

Al lettore calcolare quanto costa un anno di Macworld e quanto costa passare un anno a fare domande nelle mailing list.<p>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>