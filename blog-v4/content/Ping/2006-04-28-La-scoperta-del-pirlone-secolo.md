---
title: "la scoperta del <strike>pirlone</strike> secolo"
date: 2006-04-28
draft: false
tags: ["ping"]
---

Se premo Comando e Opzione posso abilitare o disabilitare con un clic la selezione di tutti i brani di un Cd musicale dentro iTunes.

Lo so che lo sapevi. Io invece no e, siccome spesso importo solo un brano su venti, mi perdevo in diciannove clic.