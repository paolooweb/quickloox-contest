---
title: "Due occhi vedono meglio di uno"
date: 2006-07-13
draft: false
tags: ["ping"]
---

Avevo il dubbio che iChat potesse gestire più di una iSight attaccata al Mac. Ce lo aveva anche <strong>Gianugo</strong>. Ma lui lo ha <a href="http://www.rabellino.it/blog/?p=156" target="_blank">risolto</a>.