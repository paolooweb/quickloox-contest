---
title: "Questione di principio"
date: 2011-02-07
draft: false
tags: ["ping"]
---

La prossima volta che sento parlare di iPad come di piattaforma su cui non è possibile programmare, farò presente che esiste <a href="http://itunes.apple.com/it/app/pixie-scheme-iii/id401023057?mt=8" target="_blank">Pixie Scheme III</a>.

<a href="http://www.schemers.org/" target="_blank">Scheme</a> è un dialetto di Lisp sviluppato al Massachusetts Institute of Technology per scopi didattici.

Programmare per mestiere, chiaramente, è un'altra cosa. Questa però è programmazione e non si discute.

Tra l'altro iPad &#8211; ma anche iPhone! &#8211; permette di usare con successo <a href="https://csel.cs.colorado.edu/~silkense/js-scheme/" target="_blank">Js-Scheme</a>.

Non sono invece riuscito a utilizzare <a href="http://tryruby.org/" target="_blank">Try Ruby</a>. La tastiera non compare e nemmeno una tastiera esterna Bluetooth aggira il problema.

In compenso potrei provare a <a href="http://processingjs.org/learning/ide" target="_blank">imparare Processing</a>. Anche mediante <a href="http://code.google.com/p/pjs4ipad/" target="_blank">pjs4ipad</a>.