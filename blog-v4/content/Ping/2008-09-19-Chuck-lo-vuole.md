---
title: "Chuck lo vuole"
date: 2008-09-19
draft: false
tags: ["ping"]
---

Ho cercato a suo tempo di distrarre l'attenzione dalle battute su Chuck Norris, che nate come satira graffiante ora ripetono stancamente sé stesse, <a href="http://www.macworld.it/blogs/ping/?p=748" target="_self">rimandando</a> al <em>widget</em> di MacGyver.

Inutile cliccare, però: il <em>widget</em> non si trova più o cos&#236; pare.

Cos&#236; cedo alla violenza e a <a href="http://www.macswitching.com/index.php/2008/09/18/chuck-norris-hits-your-mac/" target="_blank">Stefano</a>, che tiene a segnalare <a href="http://a.ltri.us/widgets/chucknorris/" target="_blank">il widget di Chuck Norris</a>. Sigh.