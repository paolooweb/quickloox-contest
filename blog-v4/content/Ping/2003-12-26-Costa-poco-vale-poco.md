---
title: "Costa poco, vale poco"
date: 2003-12-26
draft: false
tags: ["ping"]
---

Spesso i particolari sono decisivi, come nel prezzo di un computer

Per Natale si è comprato un computer nuovo, un fantastico assemblato costato veramente poco, e gli hanno persino scontato centosettanta euro in cambio del computer vecchio.

Sempre per Natale gli hanno regalato un disco Usb da sessantaquattro megabyte, quelli chiamati comunemente portachiavi o chiavette.

Il suo tower dispone di porte Usb in abbondanza, tutte regolarmente situate sul retro dell’unità. Per attaccare o staccare il nuovo discho Usb – comodo,
 ricordo, perché è portatilissimo e sostituisce in modo eccellente floppy e altri sistemi – deve inabissarsi sotto la scrivania, girare il tower di centoottanta gradi, stare attento che i numerosi cavi e cavetti non si stacchino o tirino, agire e finalmente, dopo avere rimesso il tower al suo posto, riemergere trionfante da sotto la scrivania.

Penso al mio iMac, che ha una porta Usb supplementare comodamente situata a fianco della tastiera. Certo, il mio computer di marca è costato qualcosa più del suo assemblato. Ma vale in tutta evidenza qualcosa di più.

<link>Lucio Bragagnolo</link>lux@mac.com