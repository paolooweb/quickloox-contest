---
title: "Un appuntamento che dà lustro"
date: 2007-05-06
draft: false
tags: ["ping"]
---

Sabato sarò anch'io a Quiliano a festeggiare i <a href="http://www.allaboutapple.com/speciali/lustro.htm" target="_blank">cinque anni del museo Apple</a>.

La prima parte del messaggio è ininfluente; la seconda è imperdibile. Un lustro arriva solo&#8230; ogni cinque anni. :-)