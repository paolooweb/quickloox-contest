---
title: "Policarpo de' Tippetti"
date: 2010-06-23
draft: false
tags: ["ping"]
---

Riporto pubblicamente una email scritta a un amico che chiedeva consigli su stilo per iPad, con la sensazione che potrebbero essere indizi utili. Attendo anzi <i>feedback</i> da chi ha già trovato e provato.

Credo che lo stilo giusto sia <a href="http://tenonedesign.com/products.php?application=iPad" target="_blank">Pogo Sketch</a> di Ten 1. Costa 14,95 dollari.

Poi esiste <a href="http://www.daydeal.com/home.php?cat=4863" target="_blank">iPad Stylus</a>, 12,99 dollari.

Amazon vende per 9,99 dollari un certo <a href="http://www.amazon.com/iClooly-Stylus-iPhone-Devices-Version/dp/B0032J1NM0" target="_blank">iClooly</a>.

Sempre Amazon ha in vendita <a href="http://www.amazon.com/Styloid-Precision-Stylus-iPhone-Touch/dp/B001TJ7DK6" target="_blank">Styloid</a> a 14,95 dollari.

In caso di emergenza puoi fabbricare da te uno stilo: qualsiasi oggetto dalla forma giusta, avvolto in una calza antistatica, permette di scrivere sullo schermo. Va bene anche un foglio di alluminio e forse perfino la carta stagnola delle cicche. :-) Se usi la carta stagnola o l'alluminio, metti nastro adesivo sulla parte che tocca lo schermo, perché il metallo è una delle poche cose che possono graffiarlo.

Mi sembra di capire che non avrai mai niente con una punta propriamente detta, perché lo schermo è sensibile a un'area di contatto che grosso modo sia equivalente a quella di un dito. Tutti i <i>tutorial</i> che ho visto usano la parte opposta a quella della punta e uno addirittura <a href="http://www.youtube.com/watch?v=FnDaiRqlRSE" target="_blank">ha allargato la testa</a>, per avvicinare la superficie di tocco a quella del polpastrello.

Un tutorial mostra foto per foto come <a href="http://www.instructables.com/id/Soft-iPhone-and-iPad-Stylus/" target="_blank">fabbricare uno stilo fai-da-te</a>.

Non ho provato di persona niente di tutto ciò e dunque non posso raccomandare seriamente alcunché.

Il tutto, naturalmente, escludendo a priori la salsiccia, sia <a href="http://www.huffingtonpost.com/2010/02/12/iphone-sausage-stylus-kor_n_459845.html" target="_blank">quella originale</a> sia la sua <a href="http://www.ipaccessories.org/sausage-stylus-69.html" target="_blank">versione tecnologica</a>.

(il titolo viene da un <a href="http://www.imdb.com/title/tt0052078/" target="_blank">vecchissimo film con Renato Rascel</a>, associato al fatto che la punta scrivente, in inglese, è <i>tip</i>).