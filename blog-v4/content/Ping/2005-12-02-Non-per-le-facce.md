---
title: "Non per le facce<p>"
date: 2005-12-02
draft: false
tags: ["ping"]
---

Ma per il posto<p>

Insisto con Quiliano e con il suo meraviglioso museo Apple, perché in realtà ci si dovrebbe fare una rubrica quotidiana a parte e invece non posso fare più di una dozzina di interventi l&rsquo;anno.<p>

Lo staff del museo si è divertito a mettere sul Web un <a href="http://www.allaboutapple.com/movies/seratina.htm">filmato</a> di momenti catturati all&rsquo;interno del museo. Le facce, come accade in qualunque filmato di entusiasti, è meglio perderle che trovarle (scherzo!).<p>

Invece lo spirito, l&rsquo;entusiasmo, sono tutti da godere. E soprattutto la <em>location</em>. È perfetto per farsi un&rsquo;idea di che cosa si trova e come è strutturato il museo. Una scusa in meno per rinviare la visita!<p>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>