---
title: "Troppo bello per essere editabile"
date: 2010-11-17
draft: false
tags: ["ping"]
---

Google <a href="http://googledocs.blogspot.com/2010/11/editing-your-google-docs-on-go.html" target="_blank">ha annunciato</a> che i documenti Google Docs saranno direttamente editabili con apparecchi <i>mobile</i> entro i prossimi giorni.

Da soddisfattissimo proprietario di iPhone e iPad, i giorni sono qui che li conto. Ultimamente sono sceso cos&#236; in basso che ho modificato un Google Doc, da iPad&#8230; chiamando il mio Mac remoto con TeamViewer e lavorando l&#236; sopra. Bello come salto mortale senza rete, ma prima smetterà di essere necessario e meglio sarà.