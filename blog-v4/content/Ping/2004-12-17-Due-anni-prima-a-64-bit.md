---
title: "Due anni ad avere ragione<p>"
date: 2004-12-17
draft: false
tags: ["ping"]
---

Come al solito, il resto del mondo si mette in coda<p>

Circa diciotto mesi fa i massimi dirigenti di Intel sostenevano che non ci sarebbe stato bisogno di processori a 64 bit in sistemi desktop circa fino al 2008.<p>

Settimana scorsa hanno dichiarato che inizieranno a fornirli nel 2005.<p>

Nel frattempo una oscura aziendina di Cupertino, California, vende computer desktop a 64 bit da un paio di anni.<p>

E c&rsquo;è ancora gente che fa paragoni.<p>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>