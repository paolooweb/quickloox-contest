---
title: "iWork più uno<p>"
date: 2005-11-23
draft: false
tags: ["ping"]
---

Soluzione conveniente all&rsquo;indigestione di Office<p>

Gli autori di Mariner Calc hanno inventato una formula di marketing ingegnosa: presentano il loro foglio di calcolo come il componente spreadsheet che manca a iWork e vantano il drag and drop e la compatibilità generale con Pages e Keynote.<p>

Sì, c&rsquo;è vita fuori da Excel. Possiedo Mariner Calc e l&rsquo;ho usato per lavori anche pesanti. Certo, non funziona pedissequamente come Excel e questo scoraggerà un sacco di gente. Tutta gente che, quando va a cambiare il microoonde, pretende i comandi messi esattamente nella posizione che avevano nel vecchio modello.<p>

Forse il tempo da passare per padroneggiare un programma nuovo vale la differenza nei costi. Fino al 27 novembre prossimo <a href="http://msw1.net/dbm83/l.htm?6644&63601&page=14">Mariner Calc</a> è in offerta a 39,95 dollari. Non so quanto costi Excel, ma scommetto che non c&rsquo;è partita.<p>

O può darsi che la gente voglia anche spendere sempre e comunque la stessa cifra, chiaro.<p>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>