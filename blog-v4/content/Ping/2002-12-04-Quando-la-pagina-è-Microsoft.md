---
title: "Quando la pagina è Microsoft"
date: 2002-12-04
draft: false
tags: ["ping"]
---

Per una volta che partecipo a un concorso, il sito è fatto con la tecnologia sbagliata

Ho ricevuto nella posta cartacea una interessante promozione Xerox che mi invitava a partecipare a un concorso spedendo una cartolina oppure collegandosi a un sito Web.

Naturalmente mi sono collegato al sito Web, ho inserito il mio codice segreto ed ecceo il risultato:

HTTP 500.100 - Internal Server Error - ASP error
• Error Type:
Microsoft OLE DB Provider for ODBC Drivers (0x80040E14)
[Microsoft][ODBC SQL Server Driver][SQL Server]Line 8: Incorrect syntax near ')'.
/XeroxOPB/Common Code/Logging.asp, line 18
• Browser Type:
Mozilla/4.5 (compatible; iCab 2.8.2; Macintosh; U; PPC) 
• Time:
03 December 2002, 19:39:07 

Sarà sempre un caso, eh? Una coincidenza, un errore di un programmatore. Ma quando c’è un errore c’è Microsoft. O forse è il viceversa.

<link>Lucio Bragagnolo</link>lux@mac.com