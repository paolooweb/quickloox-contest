---
title: "No."
date: 2010-12-11
draft: false
tags: ["ping"]
---

Bella esperienza di <b>Paolomac</b>:

<cite>Ieri ho passato un po’ di tempo a cercare di valutare iPad come lettore di libri digitali.</cite>

<cite>A parte il fatto che quelli acquistabili sullo</cite> store <cite>di iBooks in italiano per ora sono pochissimi, ho fatto un po’ di giri fra i siti che li vendono: sorpresa, dappertutto mi sono imbattuto in Adobe, che sembra avere il monopolio della protezione dei file, e che però di fatto obbliga chiunque a registrarsi come cliente Adobe per poter scaricare il software Adobe che a sua volta permette di “acquisire” i file dei libri acquistati.</cite>

<cite>Libri che poi ovviamente sono illeggibili su iBooks, ma richiedono altre</cite> app <cite>dedicate, il che impone una frammentazione della propria libreria in diverse</cite> app <cite>(biblioteche).</cite>

<cite>Te lo segnalo, perché si ha un bel dire di quanto Apple tenda a monopolizzare i mercati in cui entra, a creare sistemi chiusi eccetera.</cite>

<cite>Ma poi arriva uno (Adobe) e di fatto impone uno standard senza il quale comprare un</cite> ebook <cite>in Italia è quasi impossibile.</cite>

<cite>Mi pare sconcertante, e alla fine di tutto ho preso un libro (cartaceo) che mi sono accorto di non avere ancora letto, l’ho aperto, e… buon pomeriggio, con tanti saluti all’e</cite>book<cite>…</cite>

<cite>Buffo, no?</cite>