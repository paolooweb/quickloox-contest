---
title: "Si presenti"
date: 2006-06-01
draft: false
tags: ["ping"]
---

Lo schermo glossy di MacBook, devo ammettere, è assolutamente spettacolare dando una presentazione con Keynote. Se la presentazione ha fondo scuro e viene data sul MacBook nero, poi, fa davvero una gran scena.