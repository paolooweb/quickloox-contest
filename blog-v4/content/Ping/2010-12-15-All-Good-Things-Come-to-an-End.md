---
title: "All Good Things Come to an End"
date: 2010-12-15
draft: false
tags: ["ping"]
---

<i>Tutte le cose belle finiscono</i>. Dopo quindici anni di vita, <a href="http://www.macorchard.com/" target="_blank">The Mac Orchard</a> ha chiuso.

È stato, cito, <cite>un elenco attentamente coltivato delle più vitali applicazioni Internet e risorse correlate per gli utenti Macintosh</cite>.

Le ragioni per cui ha chiuso sono, tra l'altro, cito:

La stragrande maggioranza di utenti Mac non è più in cerca di un'alternativa per navigare o spedire la posta, perché quello che arriva insieme al Mac è veramente, veramente buono.

La gente non usa più i protocolli di una volta (a parte <a href="http://quillink.wordpress.com/" target="_blank">Riccardo</a>!). Chi si ricorda Gopher? Wais? Archie? Nntp? La maggior parte di quello che vuole la gente oggi è sul web.

Mac non è più scelta difficile. La maggior parte della gente conosce altre persone che usano Mac e non è difficile trovare online o presso un amico la dritta per risolvere un problema. È un cambiamento incredibile che molti non avrebbero pensato possibile quindici anni fa.

Che cos'è un Mac, peraltro? La gran parte degli utenti che conosco ha nel proprio arsenale un iPhone o un iPad e questi strumenti costituiscono un'estensione essenziale dell'esperienza Internet Apple-centrica, il che porta al mio ultimo e forse più saliente argomento.

Il futuro della distribuzione delle applicazioni è chiaramente App Store. Sono sicuro che quanti hanno iPhone e iPad concordano sulla comodità di avere da App Store gli aggiornamenti di tutte le applicazioni. Mi è diventato molto chiaro che Apple avrebbe dovuto estendere questa metafora a Mac. Cos&#236; che, quando lo hanno annunciato, mi sono sentito davvero sollevato.

The Mac Orchard chiude (il dominio rimmarrà però). A leggere, però, sembra una buona notizia.