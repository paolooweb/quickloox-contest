---
title: "Il punto nero"
date: 2006-05-12
draft: false
tags: ["ping"]
---

Sale sul treno una ragazza, probabilmente universitaria. Tiratissima, truccata, pettinata con cura, scarpe alla supermoda, non c&rsquo;&egrave; un accessorio fuori posto, unghie di precisione. &Egrave; studiata anche nell&rsquo;atteggiamento. La camicetta &egrave; chiusa e aperta con criterio scientifico e sicuramente collaudato con attenzione allo specchio.

Con affettazione apre lo zainetto, ovviamente griffato. Tira fuori un portatile. Gonfio, grigio, sformato, anonimo, rozzo, fuori proporzione. Non sto esagerando; se fosse un Thinkpad o un Vaio non ne parlerei certo in questi termini. Una persona che avr&agrave; passato un&rsquo;ora a prepararsi per l&rsquo;uscita e poi, in pubblico, tira fuori un computer <em>brutto</em>. <a href="http://www.mondourania.com/urania/u741-760/urania758.htm" target="_blank">Aldo Palazzeschi scrisse</a> che dentro ogni persona c&rsquo;&egrave; un punto nero, sempre nascosto. Come mai, se &egrave; il computer, la gente lo mostra?