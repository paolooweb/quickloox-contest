---
title: "Quattro tasti e Classic se ne va"
date: 2002-03-28
draft: false
tags: ["ping"]
---

Una scorciatoia poco conosciuta per un ambiente che lo è fin troppo

Come spegnere l’ambiente Classic quando abbiamo smesso di usarlo e vogliamo che tutte le risorse del sistema siano libere di dedicarsi a Mac OS X?
C’è un sistema ovvio: richiamare le Preferenze di Sistema, selezionare Classic e da lì spegnerlo.
C’è un sistema drastico: Comando-Opzione-Escape, scegliere Ambiente Classic e forzare l’uscita.
C’è infine un sistema rapido: Comando-Opzione-Maiuscolo-Q.
Attenzione, però, perché non è un comando stile Mac: non c’è ritorno e non ci sono domande. I file (in Classic) non salvati vengono persi e basta. In compenso si fa in fretta.

<link>Lucio Bragagnolo</link>lux@mac.com