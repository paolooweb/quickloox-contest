---
title: "Internet è un paiolo"
date: 2009-02-04
draft: false
tags: ["ping"]
---

I feed Rss sono bombardati dalla notizia che Learn to Play di Garageband '09 è solo per Intel.

E io non capisco come possano essere letti; <a href="http://9to5mac.com/" target="_blank">9 to 5 Mac</a> lo aveva scritto due settimane fa, <a href="http://www.macworld.it/showPage.php?id=16269&amp;template=notizie" target="_self">ripreso perfino da Macworld Italia</a> e da un sacco di altre testate.

Contemporaneamente, è arrivato puntuale il trucco per usare iMovie '09 (attenti: prestazioni a rischio) <a href="http://www.macosxhints.com/article.php?story=20090130074400511" target="_blank">su un Mac PowerPc</a>.

Proprio come a suo tempo <a href="http://www.wretch.cc/blog/hogarth1985/9616939" target="_blank">si faceva con iMovie '08</a>.

Preferisco naturalmente quelli che aggiornano la ricetta a quelli che riscaldano la minestra. Ma rimestare nel paiolo per cercare le novità non entusiasma. Chi ha la grande idea per fare ripartire tutto?