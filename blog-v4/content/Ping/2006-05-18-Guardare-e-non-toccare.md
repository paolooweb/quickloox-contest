---
title: "Guardare e non toccare"
date: 2006-05-18
draft: false
tags: ["ping"]
---

Leggo commenti di ogni tipo sullo schermo del MacBook e sulla tastiera del MacBook.

Non sono capace di giudicare uno schermo, e una tastiera, senza averli seriamente provati, davanti agli occhi e sotto le dita. Eppure &egrave; diventato lo sport del momento.

Sono malato?