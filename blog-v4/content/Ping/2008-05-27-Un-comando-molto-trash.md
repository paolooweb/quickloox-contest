---
title: "Un comando molto trash"
date: 2008-05-27
draft: false
tags: ["ping"]
---

Sembra incredibile, ma dal Terminale non c'è modo di dialogare in forma immediata con il Cestino di Mac OS X. Certo, si può raggiungere a mano la <em>directory</em> in questione, ma un Mac deve essere Mac anche quando si pasticcia nella riga di comando.

Beh, non c'era. Adesso hanno realizzato <a href="http://www.dribin.org/dave/osx-trash/" target="_blank">osx-trash</a>.