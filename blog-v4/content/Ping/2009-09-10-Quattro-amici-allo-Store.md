---
title: "Quattro amici allo Store"
date: 2009-09-10
draft: false
tags: ["ping"]
---

Come si vede, funziona anche se si affianca il quinto. Interessante la distinzione tra i tavoli promozionali, pieni di offerte, cartellini, spiegazioni, materiale illustrativo e quelli evidentemente per persone che non hanno bisogno di sapere, ma vogliono solo provare e usare Mac.

Ovviamente c'era la rete wireless totale globale, aperta a tutti.

(Ciao Fabio!)