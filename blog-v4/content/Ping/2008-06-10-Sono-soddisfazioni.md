---
title: "Sono soddisfazioni"
date: 2008-06-10
draft: false
tags: ["ping"]
---

Non so a che pizzata mi capitò di dire che Apple è stata per molti anni uno sgabello con una sola gamba, il Mac. Che poi diventò a due gambe, con iPod e ora, con iPhone, intende aggiungere una gamba in più.

Ho visto la <em>slide</em> <a href="http://www.apple.com/quicktime/qtv/wwdc08/" target="_blank">con lo sgabello a tre gambe</a> durante il <em>keynote</em> di Steve Jobs alla Wwdc. Merito suo e non mio, però la cosa mi ha divertito.

Non solo; trovavo strano che, date le premesse, alla Wwdc non ci fosse spazio per il successore di Leopard e difatti c'è l'<a href="http://www.apple.com/it/pr/comunicati/2008/06/9-snowleopard.html" target="_blank">anteprima di Snow Leopard</a>.

E lo trovavo strano che era ancora inverno. Sono soddisfazioni.