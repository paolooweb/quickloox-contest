---
title: "Il gusto della festa"
date: 2007-11-09
draft: false
tags: ["ping"]
---

Time Machine, in sé, è tecnologia piuttosto semplice. Tanto che un bravo utilizzatore di Linux ha pensato di farsene una <a href="http://code.google.com/p/flyback/" target="_blank">versione personale</a>.

Il novantanove percento di noi, tuttavia, solo a leggere la pagina si sente preso dallo sconforto. Il computer dovrebbe servire a raggiungere obiettivi, non essere un obiettivo.

Ecco perché Time Machine merita. È un pacco regalo, che basta scartare per essere usato senza preoccupazioni, come in un piccolo Natale informatico.