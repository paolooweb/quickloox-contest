---
title: "Uno, nessuno e trecentomila"
date: 2008-01-21
draft: false
tags: ["ping"]
---

<a href="http://www.crisalide-design.net/" target="_blank">Lara</a> mi ha mandato l'invito diretto e ho ricevuto numerosissime segnalazioni di questo giro di <strong>MacHeist</strong>. Software per complessivi 498.60 dollari in vendita a soli 49 dollari, con possibilità di devolvere parte della cifra in beneficenza.

Mancano due giorni e quindi ore e sono stati venduti più di 25 mila pacchetti e, se si raggiungono i 300 mila dollari di beneficenza, arriva un programma spettacolare, <a href="http://www.tweakersoft.com/vectordesigner/" target="_blank">VectorDesigner</a>, che si aggiunge a <a href="http://www.ambrosiasw.com/utilities/snapzprox/" target="_blank">Snapz Pro X</a>, <a href="http://www.macrabbit.com/cssedit/" target="_blank">CssEdit</a> e altri.

Mancano 22 mila dollari ai trecentomila, dunque è probabile che ci si riesca.

A parte i programmi, trovo che il fare trecentomila dollari di beneficenza chiedendo alle persone di acquistare software scontato dell'80 percento sia una bella idea e che nessuno dovrebbe trascurarne la valutazione. :-)