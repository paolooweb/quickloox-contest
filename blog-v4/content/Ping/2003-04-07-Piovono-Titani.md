---
title: "Piovono Titani"
date: 2003-04-07
draft: false
tags: ["ping"]
---

È arrivato un computer che cambia la vita più del PowerBook

Sai già che ho un conflitto di interessi e che sono socio (di totale minoranza) di <link>Mac@Work</link>http://www.macatwork.net.

Bene: da qualche tempo, visto che lo store tratta anche l’usato, stanno letteralmente piovendo Titanium di persone che stanno cambiando computer.

Insoddisfatte del Titanium? No. Epidemia di meningite atipica? No. La risposta è un’altra: è arrivato il PowerBook 17”.

Me ne sono innamorato appena è calata la sera e la sua tastiera si è retroilluminata automaticamente. Non so se lo comprerò perché il mio attuale Titanium pesa 2 chili e mezzo e lui pesa tre chili: ma è un computer che, se ne ami il fantastico eccesso di schermo, ti cambia letteralmente la vita di utilizzatore mobile.

Tanto per alimentare un altro conflitto di interessi, visto che sono socio del PowerBook Owners Club ti segnalo - oltre al trattamento su Macworld, ovvio - anche una <link>recensione</link>http://www.powerclub.org hands-on sul sito del Poc.

<link>Lucio Bragagnolo</link>lux@mac.com