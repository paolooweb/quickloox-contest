---
title: "Non è paglia"
date: 2003-07-14
draft: false
tags: ["ping"]
---

Apple ridefinisce per l’ennesima volta anche il movie processing

Un amico che mi ha chiesto di non fare nomi e particolari mi scrive a proposito di un Cd che verrà pubblicato prossimamente su un settimanale di grande tiratura. Il Cd conterrà un trailer di circa novanta secondi, realizzato con Final Cut Pro 4 di Apple. Ecco come è andata:

“Speakerato il tutto e raccolti video e schermate, il trailer è stato montato in mezza giornata con FCP 4, utilizzando le sue caratteristiche di compositing, oltre che di montaggio.

Ma il meglio è stato scrivere in dieci minuti (giuro) la colonna sonora con Soundtrack e soprattutto i titoli animati in sovrimpressione, realizzati in un’ora e mezza con LiveType.
Compressor è stato utilizzato per creare preview a bassa risoluzione/banda per mostrare al committente in tempo reale se le modifiche rispondevano ai requisiti. In tempo reale.

Questi tre software non cambieranno il modo di impostare il video, ed è ovvio che per lavori molto più raffinati si dovrà sempre scrivere da zero la musica con un compositore e realizzare la motion graphic con After Effects.

Ma adesso si può realizzare un prodotto (giudicato ottimo, testualmente, dal committente) in poche ore.

Non è paglia”.

<link>Lucio Bragagnolo</link>lux@mac.com