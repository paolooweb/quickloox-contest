---
title: "Anno nuovo e quel che segue"
date: 2010-01-07
draft: false
tags: ["ping"]
---

Mi scuso con tutti quelli che mi hanno chiesto un rimpiazzo momentaneo di Ping via Wave e che era mia intenzione accontentare; vari disguidi e una mancanza generale di concentrazione da parte mia hanno accumulato abbastanza ritardo da rendere inutile l'iniziativa, dal momento che il motore è ripartito.

Tutto è bene quello che finisce bene, per quanto niente sia bene quando ha tre settimane di disservizio. Scuse a tutti e grazie a chi si è prodigato per ripristinare il funzionamento. Si ricomincia. :)