---
title: "E ora qualcosa di completamente inutile"
date: 2006-05-31
draft: false
tags: ["ping"]
---

<a href="http://janusnode.com/" target="_blank">JanusNode</a>, programma per scrivere testo a caso o casualizzare testo esistente.

&Egrave; talmente privo di senso che posso stare minuti interi a guardarlo&hellip; e a pensare a cose interessanti.