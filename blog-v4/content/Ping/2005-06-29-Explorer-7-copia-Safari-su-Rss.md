---
title: "Scopiazzon de&rsquo; Scopiazzoni<p>"
date: 2005-06-29
draft: false
tags: ["ping"]
---

Tra gli uomini più ricchi del mondo. Non di originalità<p>

Anche gli ingegneri del software di Apple, a volte, hanno un loro blog. Uno di questi è Jens Alfke, che nel suo blog racconta di come è stato <a href="http://mooseyard.com/Jens/2005/06/ive-been-microsofted">microsoftizzato</a>. Ossia, in Microsoft hanno copiato il suo lavoro. Oramai c’è il verbo apposta.<p>

Lui si è occupato, tra le altre cose, del supporto degli Rss in Safari edizione Tiger. Che è stato ripreso praticamente pari pari nelle dimostrazioni che Microsoft dà dell&rsquo;Internet Explorer compreso, a fine 2006 se tutto andrà bene, nella prossima edizione di Windows. Nel blog ci sono i link alla documentazione fotografica.<p>

Jens nota un paio di cose intelligenti. La prima è che l&rsquo;Rss non è una di quelle cose dall&rsquo;interfaccia utente obbligata. Per fare esempi, Omniweb e Firefox gestiscono la stessa cosa in modi del tutto diversi. È ancora possibile avere delle idee, senza dover necessariamente rubare quelle degli altri.<p>

Secondo, Jens lascia un indizio ai Windowsisti: potreste avere questa funzione subito, invece di aspettare diciotto mesi (se va bene). Safari per Tiger è fuori da aprile.<p>

Bill Gates è spesso stato accostato a Paperon de&rsquo; Paperoni. Ma la sua azienda non perde occasione per ricordarci quanto si può scoppiare di soldi ed essere proprio poveri.<p>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>