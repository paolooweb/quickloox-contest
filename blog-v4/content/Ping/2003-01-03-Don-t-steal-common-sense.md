---
title: "Don’t steal common sense"
date: 2003-01-03
draft: false
tags: ["ping"]
---

Difficile capire quali siano le regole giuste quando non esistono regole

Sono in treno accanto a un amico che sta aprendo un iPod nuovo nuovo, regalo di Natale. Nella confezione, un invito gentile ma fermo da parte di Apple: “Don’t steal music”.

Nel frattempo in corridoio passa un ragazzino sbuffante e affrettato. Pochi istanti dopo un controllore donna (controllatrice?), che si rivolge a un passeggero: “È andato di qua?”. “Sì, un momento fa” risponde l’uomo.

Il controllore sparisce, il treno si ferma, apre le porte, le richiude e riparte.

Riappare il ragazzino, che chiede al passeggero di prima “È andata di là?”. “Sì, stai tranquillo”, fa l’uomo.

Un terzo passeggero, sui vent’anni, assiste alla scena intanto che si spunta le unghie, senza preoccuparsi troppo di dove cadano.

Il mio amico scarta il suo iPod, ignaro del tutto, e mi chiede se gli presterò la mia collezione di Cd per fare qualche Mp3. Sì, lo farò. Non rubo musica.

<link>Lucio Bragagnolo</link>lux@mac.com