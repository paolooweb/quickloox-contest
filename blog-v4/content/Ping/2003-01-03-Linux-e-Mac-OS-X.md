---
title: "Sicuri sì, ma di sbagliare"
date: 2003-01-03
draft: false
tags: ["ping"]
---

Mac OS X non è uguale a Linux

Da quando Apple ha abbracciato Unix è comparsa una nuova linea evolutiva di ignoranti: quelli che pensano a Mac OS X e a Linux negli stessi termini e se ne escono con amenità tipo “Essendo tutti e due sistemi Unix, hanno le stesse vulnerabilità”.

Metti un contadino abruzzese a confronto con un pastore altoatesino e falli parlare: molto probabilmente non capiranno una parola l’uno dell’altro. Poi si renderanno conto che sono italiani e allora inizieranno a parlare una lingua comune, che però nelle loro famiglie non usano.

Ecco, appunto. Mac OS X e Linux sono versioni di Unix, ma Mac OS X è basato su Darwin, che è basato su FreeBSD, che ha un kernel Mach; Linux ha un altro kernel e tutt’altre basi di sistema.

Questo non toglie che la gran parte dei programmi Unix possa funzionare da una parte e dall’altra, ma prima bisogna prendere il codice sorgente (comune) e compilarlo in modo da avere due binari (diversi). Il che vuol dire che se da una parte c’è un problema, non è assolutamente detto che dall’altra parte sia lo stesso.

Detto questo, invito gli ignoranti a sciacquarsi la bocca con un bel compilatore, prima di parlare. :)

<link>Lucio Bragagnolo</link>lux@mac.com