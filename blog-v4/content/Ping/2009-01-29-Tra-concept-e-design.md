---
title: "Tra concept e design"
date: 2009-01-29
draft: false
tags: ["ping"]
---

È spuntato su Internet un numero del 1991 della rivista giapponese Axis, dedicato ai <em>concept</em> su cui Apple lavorava al tempo, resuscitato in rete da una <a href="http://www.flickr.com/photos/zacislost/sets/72157612785537412/" target="_blank">raccolta di immagini</a> e da <a href="http://forums.mactalk.com.au/21/65734-whacky-apple-concept-designs-1991-youve-never-seen-before.html" target="_blank">un forum</a> sul sito australiano MacTalk.

Ogni tanto i progetti di ricerca andavano alla ricerca di idee sconvolgenti e il più delle volte irrealizzabili. Il Mac da polso, un cambiavalute fai-da-te, il Mac da bicicletta. Tutti <em>concept</em>, pensieri in libertà.

Oggi i progetti di ricerca si focalizzano su soluzioni che trovino posto in prodotti reali e così abbiamo avuto iMac, iPod, iPhone, i Mac unibody, il <em>multitouch</em>, FireWire e un sacco di altre cose. Tutto <em>design</em>, unione di forma e funzione in un prodotto di alto livello.

Tra i <em>concept</em> del 1991 figuravano già diversi <em>tablet</em>. Sembra che la tentazione di fare un computer piatto e orizzontale rimanga costante. Però Apple finora ha resistito.

Speriamo continui.