---
title: "Guardami, ti sento"
date: 2006-07-02
draft: false
tags: ["ping"]
---

L'inclusione di iSight di serie dentro iMac e portatili sta provocando, nel ristretto campione di persone che ho modo di osservare, un decollo verticale delle chat video, prima praticamente assenti. Io stesso, che ho una iSight vecchio stile, la attacco di rado e chatto in video solo se strettamente necessario.

Sto pensando a come cambiano in generale i modi di fruizione di un oggetto in funzione della sua disponibilità immediata. Forse l'idea di dover montare la iSight implica passare da una accettazione esplicita del farsi vedere dall'altro, mentre la webcam di serie implica un ok implicito. Sulla questione ho ancora le idee poco chiare.