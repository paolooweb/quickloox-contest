---
title: "Come in un film"
date: 2006-03-07
draft: false
tags: ["ping"]
---

In <a href="http://www.macatwork.net" target="_blank">Mac@Work</a> (l&rsquo;Apple Center di cui sono socio) <a href="http://web.mac.com/fabol/iWeb/Fabol/Blog/Blog.html" target="_blank">Fab</a> ha cambiato portatile e ha abbandonato il PowerBook G4 17&rdquo; per passare a MacBook Pro. Per farlo attendeva che <a href="http://www.wow-europe.com" target="_blank">World of Warcraft</a> fosse disponibile come programma Universal. Lo &egrave;.

Nella prima mattinata ha trasferito tutti i dati dal portatile G4 al portatile Intel con l&rsquo;Assistente Migrazione. Nessun problema. Poi, una prova banale.

Fab scarica un <a href="http://www.apple.com/trailers/imax/imaxdeepsea3d/hd/" target="_blank">filmato in alta definizione</a> (Hd), quello pi&ugrave; grande, da 1.080 punti. Poi lo proietta sul MacBook Pro e, con mela-i, guarda le informazioni sul filmato durante la proiezione.

Sul MacBook Pro i fotogrammi effettivi sono sempre 24, senza problemi. Sul PowerBook G4 17&rdquo; che uso io, beh, si vede proprio che &egrave; di due generazioni fa. &Egrave; perennemente sovraccarico, ma non sono mai andato oltre qualche picco di 12 fotogrammi per secondo e una media generale assai inferiore. Non parliamo di World of Warcraft, dove il mio G4 a volte stenta un pochino e il MacBook Pro invece fa faville.

Se qualcuno aveva dubbi sulle prestazioni dei portatili Apple Intel, ci ripensi.