---
title: "Otto bit in una finestrella"
date: 2007-06-03
draft: false
tags: ["ping"]
---

Dai meandri del disco in corso di pulizia è uscito <a href="http://www.viceteam.org/" target="_blank">Vice</a>, emulatore di Commodore 64 anche per Mac OS X (via X11).

Non è il mio genere. Sono del lato Spectrum e vorrei vedere un serio (e usabile) emulatore di Ql. Però, a vedere quella finestrella sul mio 17&#8221;, mi sono un po' commosso. Quella finestrella ha riempito la vita di milioni di persone. La mia era solamente di colori un po' diversi.

Chissà se se ne accorge <strong>Paolo</strong>, che ha fatto partire <a href="http://www.gamestar.it/blogs/roots/" target="_blank">Back to the Roots</a>. :-)