---
title: "L'impatto con U-myx"
date: 2008-12-29
draft: false
tags: ["ping"]
---

Sempre parlando di regali di Natale, a casa è arrivato <a href="http://itunes.apple.com/WebObjects/MZStore.woa/wa/viewAlbum?id=80580226&amp;s=143450" target="_blank">Light + Shade</a>, album musicale di Mike Oldfield.

Dalle note di produzione si evince che Oldfield ha usato, tra l'altro, G5 con Logic 7 (l'album è del 2005).

Nel disco Light è presente anche una parte software, con dentro un software chiamato U-myx e quattro versioni dei brani dell'album in formato compatibile.

U-myx permette di divertirsi in modo molto facile e immediato con funzioni elementari di missaggio. La cose forse più stimolante è la possibilità di capire visivamente i blocchi e le parti con cui viene realizzata una composizione moderna.

Raccomando altres&#236; di provare U-myx in situazione controllata, senza lavoro importante aperto. Mentre provavo il programma era in funzione Time Machine e succedevano varie altre cose, dunque non ho un colpevole certo; però il <em>crash</em> è stato di quelli che si ricordano.

U-myx del 2005 potrebbe anche essere migliorato. Il software ha cambiato nome in <a href="http://www.u-myx.com/" target="_blank">GoMix</a> e promette una ripartenza a breve.

Nel frattempo ascolto ripetutamente First Steps.