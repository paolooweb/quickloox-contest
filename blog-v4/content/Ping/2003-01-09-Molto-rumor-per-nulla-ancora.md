---
title: "Tanto rumor per nulla (ancora)"
date: 2003-01-09
draft: false
tags: ["ping"]
---

Il titolo è sempre uguale perché i fatti rimangono quelli

Per i siti di rumor – lo ha detto pure Steve Jobs in persona – doveva essere il keynote più noioso della storia o quasi. Invece si sono viste un sacco di novità, che nessuno ha azzeccato.

Lo sport di prevedere ciò che Apple presenterà in questa o quella occasione è definitivamente morto, ed è una fortuna, perché a suon di anticipazioni di prodotti inesistenti o impossibili Apple ha passato anche guai pesanti in termini di mancate vendite, e tanta gente ha rinviato inutilmente acquisti necessari sulla base del nulla.

Quanto ai siti di pettegolezzi, li capisco: vivono di pubblicità e devono per forza attirare l’attenzione, a qualsiasi costo.

Basta trattarli per quello che sono: un piacevole diversivo per farsi due risate in pausa caffè.

<link>Lucio Bragagnolo</link>lux@mac.com