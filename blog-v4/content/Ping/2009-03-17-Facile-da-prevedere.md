---
title: "Facile da prevedere"
date: 2009-03-17
draft: false
tags: ["ping"]
---

Se apri le cinque applicazioni di iLife '09 (GarageBand, iDvd, iMovie, iPhoto, iWeb), ci lavoricchi appena appena e guardi le allocazioni di memoria virtuale dentro Monitoraggio Attività, avrai matematicamente tutta iLife dentro i primi dieci posti della classifica.

In compenso, conoscevo spesso iLife più per sentito dire che per esperienza diretta. Fatta un po' di esperienza diretta, tanto di cappello a iMovie e a GarageBand. iPhoto uno se lo aspetta.