---
title: "Non c'è peggior sordo"
date: 2010-07-15
draft: false
tags: ["ping"]
---

Si sa che iPhone e iPad sono apparecchi benvoluti dalla comunità dei non vedenti. Ma c'è qualcosa che finora è stato detto a voce troppo bassa.

Il 7 luglio scorso, Zvrs <a href="http://www.appleinsider.com/articles/10/07/08/apple_att_partner_to_assist_deaf_with_iphone_4_and_facetime.html" target="_blank">ha organizzato un evento</a> presso lo Hard Rock Café di Philadelphia per mostrare il software iZ, realizzato in collaborazione con Apple e At&#38;T per iPhone 4.

Il software usa la tecnologia di videoconferenza istantanea FaceTime di iOS 4 per trasformare il parlato in linguaggio dei segni e permettere a un non udente di comunicare il più normalmente possibile, come nessun altro apparecchio è attualmente in grado di offrire.

Sono cose estremamente importanti per un numero di persone certo ristretto, che però merita tutta l'attenzione possibile. Ma i loro clic non valgono abbastanza per i mercanti di spazzatura, focalizzati su stupidaggini inesistenti purché facciano abbastanza rumore. I più sordi di tutti.