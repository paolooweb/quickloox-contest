---
title: "Niente domande"
date: 2010-09-22
draft: false
tags: ["ping"]
---

Ci sono momenti in cui uno non sta a chiedersi il perché.

Uno di questi è quando scopri che Undercroft, un gioco di ruolo più che carino per iPhone, è scaricabile gratis e che il suo autore è <a href="http://www.jagex.com/" target="_blank">Jagex Games Studio</a>, la software house responsabile di <a href="http://www.runescape.com/" target="_blank">RuneScape</a>.

Se appena hai avuto presente RuneScape, gioco di ruolo di massa su Internet gratuito per qualsiasi browser, <a href="http://itunes.apple.com/it/app/undercroft/id388644935?mt=8" target="_blank">Undercroft</a> lo scarichi proprio senza farti domande. A buttarlo via c'è sempre tempo. E poi neanche lo butti.