---
title: "Là dove la gente si diverte"
date: 2008-08-20
draft: false
tags: ["ping"]
---

Le rilevazioni del traffico web di NetApplications sono uno strumento decisamente interessante.

A parte il constatare che con l’arrivo di iPhone 3G il traffico web generato da iPhone/iPod touch è aumentato (e non poco), si scopre che durante ogni fine settimana <a href="http://marketshare.hitslink.com/report.aspx?sample=17&qprid=42&qpdt=1&qpct=4&qpcustom=iPhone&qptimeframe=D&qpsp=3439&qpnp=79" target="_blank">ha un picco</a> stimabile nell’ordine del 50-70 percento.

Ho cercato dati per Symbian o Windows Mobile per verificare se sia un comportamento abituale, ma non trovo niente. Su NetApplications non arrivano neanche al livello minimo di sensibilità del radar. iPhone e iPod touch, invece, secondo la giornata fanno da un quarto a un terzo di quanto fa Linux, usato su computer veri, con schermi veri, tastiere vere.

La conclusione, per il momento, è che con iPhone e iPod touch la gente fa cose utili. Poi, nel fine settimana, ci si diverte pure. Con i cellulari, non si sa.