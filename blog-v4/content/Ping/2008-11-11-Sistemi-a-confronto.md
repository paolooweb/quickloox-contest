---
title: "Sistemi a confronto"
date: 2008-11-11
draft: false
tags: ["ping"]
---

Per il momento, la comparazione più significativa (ed esilarante) tra Snow Leopard e Windows 7 <a href="http://www.theonion.com/content/infograph/os_x_snow_leopard_vs_windows" target="_blank">l'ha fatta The Onion</a>.