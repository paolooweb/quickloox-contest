---
title: "Essere umani"
date: 2006-12-25
draft: false
tags: ["ping"]
---

È già tempo di cominciare con i propositi del nuovo anno?

Di sicuro vorrei che <a href="http://www.indiehig.com/blog/" target="_blank">questa</a> fosse una delle priorità del 2007. Dopo tutto il vero contributo di Mac al genere umano è, di umana, l'interfaccia.