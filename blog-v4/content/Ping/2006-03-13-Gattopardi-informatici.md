---
title: "Gattopardi informatici"
date: 2006-03-13
draft: false
tags: ["ping"]
---

Sar&agrave; la primavera che incombe e porta la pigrizia, ma si legge di tutto e a me sembra che non stia succedendo veramente niente.

Qualcuno annuncia un ennesimo concorrente di iPod, ennesimo fiasco, per l&rsquo;appunto, annunciato. Per la cinquantesima volta dal secolo scorso, questa volta con la bufala del <em>media center</em>, si cerca di trasformare il computer il televisore (e, come le altre volte, nessuno se la beve). Microsoft annuncia un computer Windows troppo piccolo per avere una tastiera degna di questo nome (ne saranno apparsi venticinque) e sempre in casa Microsoft prosegue lo sviluppo di Vista, una brutta copia di Mac OS X, presentato cinque anni fa. Esattamente come Windows 95 era una brutta copia di System 7.

Cambia tutto, ma non cambia niente.