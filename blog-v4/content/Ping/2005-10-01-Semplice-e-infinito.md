---
title: "Spazio di proprietà<p>"
date: 2005-10-01
draft: false
tags: ["ping"]
---

Sistema semplice, possibilità infinite: questo è Macintosh<p>

Le opzioni di serie di Mac sono configurabili con semplicità relativa. Si può fare meglio, ma nessuno lo ha ancora fatto e tipicamente, quando accade, è Apple che lo fa per prima.<p>

Tuttavia, nulla è impossibile a una persona sufficientemente motivata e capace di pensare a quello che fa mentre lo fa.<p>

Enzo aveva un&rsquo;esigenza insolita: voleva uno spazio come separatore delle migliaia per i numeri. Non è uno standard e Mac OS X non permette di farlo, dall&rsquo;interfaccia.<p>

Ha lanciato Property List Editor (/Developer/Applications/Utilities) e con quello ha aperto il file .globalpreferences.plist in ~/Library/Preferences.<p>

(Sono volutamente stringato. Chi non sta capendo, meglio che non ci provi. Per esempio, si tratta di un file invisibile.)<p>

Nel file ha aggiunto quanto segue:<p>

<code>&lt;key&gt;AppleICUNumberSymbols&lt;/key&gt;</code><br>
<code>&lt;dict&gt;</code><br>
<code>&lt;key&gt;0&lt;/key&gt;</code><br>
<code>&lt;string&gt;,&lt;/string&gt;</code><br>
<code>&lt;key&gt;1&lt;/key&gt;</code><br>
<code>&lt;string&gt; &lt;/string&gt;</code><br>
<code>&lt;key&gt;10&lt;/key&gt;</code><br>
<code>&lt;string&gt;,&lt;/string&gt;</code><br>
<code>&lt;key&gt;8&lt;/key&gt;</code><br>
<code>&lt;string&gt;&euro;&lt;/string&gt;</code><br>
<code>&lt;/dict&gt;</code><p>

E adesso ha il suo separatore su misura.<p>

Un sistema semplice per chi vuole usarlo senza pensieri. Fondamenta aperte per chi non si accontenta mai. Che volere di più?<p>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>