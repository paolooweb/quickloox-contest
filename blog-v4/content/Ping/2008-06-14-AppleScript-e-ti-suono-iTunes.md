---
title: "AppleScript e ti suono iTunes"
date: 2008-06-14
draft: false
tags: ["ping"]
---

La settimana scorsa avevo mostrato <a href="http://www.macworld.it/blogs/ping/?p=1820">alcuni semplicissimi script</a> che, inviati via posta elettronica a un Mac disponibile, lo spengono, lo riavviano o lo mettono in stop.

Non finisce cos&#236;, naturalmente. Il principio può essere esteso a qualsiasi programma. Per esempio iTunes, come con questi script:

<code>tell application "iTunes"
   activate
   play
end tell</code>

<code>tell application "iTunes"
   stop
end tell</code>

<code>tell application "iTunes"
   next track
end tell</code>

<code>tell application "iTunes"
   set la_playlist to user playlist "nome della playlist"
   set view of front window to la_playlist
   play la_playlist
end tell</code>

Ancora una volta, sono più script, non uno solo lungo. Non mi dilungo sul significato degli script; sono brevissimi e basta provarli per capire. Sono anche innocui.

La <a href="http://www.tuaw.com/2008/04/13/applescript-control-itunes-with-an-e-mail/" target="_blank">pagina originale</a> da cui provengono questi ultimi script è sempre opera di Cory Bohon di The Unofficial Apple Weblog.