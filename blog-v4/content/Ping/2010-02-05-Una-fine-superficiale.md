---
title: "Una fine superficiale"
date: 2010-02-05
draft: false
tags: ["ping"]
---

Concludo qui le mie osservazioni su quanto <a href="http://attivissimo.blogspot.com/2010/01/aiped-aipad-aipod.html" target="_blank">ha scritto Paolo Attivissimo</a> nello stroncare iPad in base a osservazioni vecchie di due anni e mezzo, dimostratesi se non irrilevanti almeno fortemente discutibili, considerazioni superficiali e posizioni aprioristiche fino al paradosso (non è noto il prezzo italiano, ma Paolo sa già che è sbagliato).

Non ci meritiamo tutto questo da un giornalista altrimenti bravo e preparato; invito a seguire il suo sito con regolarità, perché vi si trovano contenuti interessanti e sane lezioni di fatti concreti a smontare tanto chiacchiericcio imbecille che si trova su Internet.

Non avremo tuttavia da Paolo una analisi equilibrata e meditata di iPad. Il suo giudizio sarebbe favorevole solo se iPad corrispondesse a una sua qualche idea astratta, che dovrebbe essere migliore delle altre non in base ai fatti, ma perché è la sua idea. Si fa davvero poca strada.

iPad potrebbe essere un fiasco, perché no? Oppure un successone, o un oggetto che rispetta le previsioni dell'azienda che lo vende. Come tutte le cose della vita, però, le posizioni ideologiche sono destinate a perdere di vista la realtà. La posizione di Paolo è ideologica, oltre che superficiale.

Si è scritto molto di più e molto meglio. Non ho modo di tradurre integralmente quanto <a href="http://www.stephenfry.com/2010/01/28/ipad-about/2/" target="_blank">scritto da Stephen Fry</a>, ma quattro righe dovrebbero bastare: <cite>Che cosa ci posso fare che non posso fare con un portatile o con un iPhone?, potrebbe obiettare qualcuno. Troppo grande per stare in tasca, troppo piccolo per usarlo seriamente. Non vedo il bisogno. È una soluzione in cerca di un problema.</cite>

<cite>Ci sono molti rilievi che si potrebbero fare a un iPad. Niente multitasking, niente Flash. Niente fotocamera, niente Gps. Spariscono tutti nel momento in cui lo si usa. Non so dare abbastanza enfasi a questo punto: aspettate a giudicare fino a che non lo avete provato per cinque minuti.</cite>

Paolo si ferma sostanzialmente qui, per dire <cite>no grazie</cite>. Fry va avanti per cinque pagine argomentate e certamente discutibili, ma che aggiungono qualcosa.

Paolo non ha provato iPad neanche per cinque secondi. Rispetterò il suo giudizio quando lo avrà fatto. Per il momento, con stima immutata, è stato tristemente superficiale.