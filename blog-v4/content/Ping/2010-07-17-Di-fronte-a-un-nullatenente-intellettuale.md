---
title: "Di fronte a un nullatenente intellettuale"
date: 2010-07-17
draft: false
tags: ["ping"]
---

Prima ti dice che <cite>il suo cellulare non ha problemi</cite>, come se l’universo ruotasse intorno al suo ombelico. A parte il fatto che significherebbe qualcosa se avesse anche un iPhone 4, significherebbe di più se avesse a disposizione <a href="http://www.apple.com/antenna/testing-lab.html" target="_blank">diciassette camere anecoiche</a>. Il mondo è pieno di mosche cocchiere.

In seconda battuta, insinua che i dati di Apple possano essere falsi o solo parzialmente veri, o che sia stato nascosto qualcosa, o che comunque qualcuno non la racconti giusta. Riesce a sentirsi nel giusto solo partendo dal presupposto che tutto quello che rafforza la sua opinione è verità di fede e tutto quello che la mette in discussione è in qualche modo sbagliato. <a href="http://xenu.com-it.net/txt/cm4.htm" target="_blank">Dissonanza cognitiva</a>.

Infine butta l&#236; qualche frase fatta, tipo <cite>eh beh, ma i problemi ci sono comunque</cite>, come se fosse mai esistito un prodotto industriale privo di problemi o venduto a milioni di esemplari senza averne neanche uno difettoso. Frasi che lasciano il tempo che trovano e potrebbero essere pronunciate identiche in innumerevoli circostanze diverse e non aggiungono niente al discorso.

Quando una persona usa una frase fatta invece che un argomento, hai raggiunto i suoi limiti intellettuali. Da l&#236; in poi è tempo sprecato. Un po' lo era già.