---
title: "Autoreferenza"
date: 2010-04-20
draft: false
tags: ["ping"]
---

Straordinaria questa <a href="http://apirocks.com/html5/html5.html#slide1" target="_blank">presentazione di Html5</a> scritta&#8230; in Html5.

Attenzione all'interfaccia: primo, ho cliccato per minuti come un cretino l'icona a forma di freccia prima di leggere e capire che bisognava premere il tasto freccia. Secondo, sembra un elenco un po' barboso di roba tecnica e invece è disseminata di interattività non sempre evidente, con risultati talvolta da mandibola che cade.