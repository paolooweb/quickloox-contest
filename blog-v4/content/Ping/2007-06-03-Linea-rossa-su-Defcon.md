---
title: "Linea rossa su Defcon"
date: 2007-06-03
draft: false
tags: ["ping"]
---

Mi sarebbe piaciuto dedicare il prossimo venerd&#236; del gioco a <a href="http://www.ambrosiasw.com/games/defcon/" target="_blank">Defcon</a>, simulatore di guerra termonucleare globale portato su Mac da Ambrosia. Ma in ogni partita può esserci solo una versione demo e non posso obbligare ciascuno a comprarsi il gioco solo per venerd&#236; dalle 13 alle 14.

Per restare allora su Ambrosia, e accogliendo il suggerimento di Tommaso, il prossimo venerd&#236; del gioco sarà su <a href="http://www.ambrosiasw.com/games/redline/" target="_blank">Redline</a>, gioco di corse automobilistiche 3D. Mi sembra che sia possibile giocare in multiplayer anche avendo tutti la demo.

Come al solito, si apre la stanza iChat <code>gamefriday</code> per coordinarsi. Però consiglio a tutti di entrare direttamente nella lobby di Redline, dove il gioco mette a disposizione una chat. Io entrerò con nome e cognome. E, accidenti, ci sarò.