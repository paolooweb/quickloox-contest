---
title: "Chi farebbe mai cambio?"
date: 2006-05-10
draft: false
tags: ["ping"]
---

Ho aggiornato i tassi di cambio nella Calcolatrice di Mac OS X, che avevano smesso di funzionare tempo fa e finalmente non so pi&ugrave; quale aggiornamento di sistema aveva ripristinato. Adesso compare perfino un riquadrone che dettaglia quali valute sono state aggiornate e quali no (il dollaro canadese, per esempio, risale a un anno fa).

Se prima non funzionava, adesso fa impressione da quant&rsquo;&egrave; quasi troppo. :)