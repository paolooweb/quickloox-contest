---
title: "Fink vs. rink<p>"
date: 2005-08-18
draft: false
tags: ["ping"]
---

Quando si può essere sbrigativi è meglio non esitare<p>

In un momento di tranquillità mi sono messo ad aggiornare il software su disco. Quando sono arrivato a Fink, ho visto che l&rsquo;aggiornatore non trovava i mirror giusti e, al terzo tentativo, mi sono insospettito.<p>

Ho scoperto, finalmente, che con il passaggio a Tiger Fink funzionava per quanto era già installato, ma necessitava di una nuova versione per tutto il resto.<p>

Sul sito, però, le istruzioni erano di complessità superiore al mio livello di attenzione (o probabilmente al mio livello in assoluto). Dopo avere armeggiato inutilmente intorno a Fink e <a href="http://finkcommander.sourceforge.net">FinkCommander</a>, ho pensato che, dopotutto, Fink installa tutto in una directory /sw separata dal resto del sistema, così che è impossibile fare danni cancellando qualcosa.<p>

Così ho preso il coraggio a due mani e ho cancellato l&rsquo;intera directory /sw. Ho reinstallato il nuovo Fink da zero e tutto è andato benissimo. Poi gli ho detto di reinstallare i programmi Unix che avevo prima (me li ricordavo) l&rsquo;ho lasciato lì a ricaricare tutto per un po&rsquo;.<p>

Alla fine è andato tutto bene. Per due motivi: Fink è pensato per utenti anche un po&rsquo; rink come me e, secondo, sapevo esattamente che cosa dovevo reinstallare.<p>

Per il futuro ho iniziato a scaricare materiale di un concorrente di <a href="http://fink.sourceforge.net">Fink</a>, <a href="http://darwinports.org">DarwinPorts</a>. Sai mai, se la prossima non la azzeccassi.<p>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>