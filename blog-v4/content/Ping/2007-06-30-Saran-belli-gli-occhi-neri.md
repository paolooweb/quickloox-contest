---
title: "Saran belli gli occhi neri"
date: 2007-06-30
draft: false
tags: ["ping"]
---

Gli schermi Apple hanno avuto quasi sempre ottimi riconoscimenti per il rapporto qualità/prezzo.

<em>&#8230;saran belli gli occhi blu&#8230;</em>

Ho visto e usato un MacBook Pro 15&#8221; <em>glossy</em> retroilluminato a Led.

<em>&#8230;ma le gambe, ma le gambe&#8230;</em>

Ho visto e usato un MacBook Pro 17&#8221; High Definition.

<em>&#8230;a noi piacciono di più.</em>

Sono splendidi. Quei due pollici in più però fanno <a href="http://www.apple.com/it/macbookpro/" target="_blank">LA differenza</a>.