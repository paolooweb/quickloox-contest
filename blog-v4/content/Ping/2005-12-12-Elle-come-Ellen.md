---
title: "Elle come Ellen<p>"
date: 2005-12-12
draft: false
tags: ["ping"]
---

Sempre coincidenze, ma sempre in mezzo<p>

Stamattina caso urgente al telefono. L., carissima amica, deve tenere una presentazione in università. Tutto a posto, tutto preparato, tutto perfetto quando, dando gli ultimi ritocchi alla presentazione, un ultimo salvataggio e puff! il file sparisce. Non c&rsquo;è Terminale, non c&rsquo;è utility, non c&rsquo;è niente. Andato.<p>

Che programma stavi usando, L.?<p>

<cite>PowerPoint.</cite><p>

Faresti meglio a usare Keynote.<p>

<cite>Lo so, ma c&rsquo;è qualche problema, la compatibilità, qui chiedono le cose che vanno con il Pc, nella conversione cambiano i font eccetera.</cite><p>

Dovresti provare OpenOffice. Da quello che ho visto mi sembra che la compatibiltà sia ottima.<p>

<cite>Eh, ma sai, qui in università bisogna usare uno sfondo preciso e non so&hellip;</cite><p>

Certo, dico, bisogna stare attenti allo sfondo, se esiste ancora un file in cui metterlo, naturalmente.<p>

<cite>Beh sì&hellip;</cite><p>

Fisicamente L. non potrebbe essere più diversa. Ma in quel momento avevo davanti agli occhi Ellen Feiss, quella dello <a href="http://www.adweek.com/aw/creative/best_spots_02/021014_02.jsp">spot Apple</a>. E, sicuramente per coincidenza, c&rsquo;era sempre di mezzo Microsoft.<p>

<cite>I was writing a paper on the Pc and it was, like, beep, beep, beep, beep, beep, and then, like, half of my paper was gone&hellip;</cite><p>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>