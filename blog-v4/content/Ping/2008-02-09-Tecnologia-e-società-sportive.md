---
title: "Tecnologia e società (sportive)"
date: 2008-02-09
draft: false
tags: ["ping"]
---

Era molto che non esulavo momentaneamente dallo specifico del mondo Mac e oggi me lo concedo.

Ogni tanto si parla dell'Italia come Paese in declino, oppure in ripresa, oppure privo di infrastrutture adeguate, oppure miracolato dalle medie e piccole imprese e bla bla bla.

Sono cose troppo grandi per me e per questo blog. In compenso, per motivi bizzarri, ho passato tempo a vedere come il sito della <a href="http://www.gazzetta.it" target="_blank">Gazzetta</a> copre le partite di calcio e come il sito dell'<a href="http://www.nba.com" target="_blank">Nba</a> copre quelle del basket professionistico americano.

Viene fuori che il video e/o l'audio, in un modo o nell'altro, lo fanno vedere tutti ed è un fatto tecnico acquisito. È sul resto, dove non conta tanto diffondere media quanto avere voglia, che la differenza è pazzesca.

Senza vedere o ascoltare niente, il campo di gioco realizzato in web 2.0 dai programmatori Nba è sorprendente. I quintetti in campo vengono aggiornati in tempo reale e sul campo appaiono le posizioni dei tiri realizzati e di quelli sbagliati, per squadra e per singolo giocatore. Sempre per ogni singolo giocatore ci sono tutte le statistiche aggiornate in tempo reale.

La banda, le infrastrutture, i server necessari a realizzare una cosa del genere sono cose normalissime, a portata di tutti. Non c'è neanche una creatività particolare. La volontà di offrire un servizio competitivo e imperdibile per il visitatore, tuttavia, sul sito Nba è smisurata. Sul sito Gazzetta è, beh, insomma, si fa quel che si può. Non parliamo del sito della <a href="http://www.lega-calcio.it/" target="_blank">Lega Calcio</a>, che dovrebbe essere l'omologo dell'Nba e invece ha l'impaginazione concentrata sui comunicati stampa ufficiali, come se fosse roba che interessa veramente a qualcuno.