---
title: "Un’azienda del secolo scorso"
date: 2004-07-30
draft: false
tags: ["ping"]
---

Che forse sta finalmente cambiando il senso della sua parabola

È roba vecchia, ma nessuno ne ha parlato (congiura, congiura...). Lavorava per Microsoft ed è stato <link>cacciato</link>http://www.michaelhanscom.com/eclecticism/2003/10/of_blogging_and.html perché a ottobre 2003 ha fotografato e pubblicato in un suo blog l’arrivo a Redmond di una partita di G5.

Sono notizie che per un verso riportano al secolo scorso (ma la tecnologia Microsoft in fondo è datata). Per l’altro verso incoraggiano una riflessione. La gente che ha paura della propria ombra non sopporta la vista del sole.

Ci vorranno cinquant’anni, ma forse finalmente Microsoft ha imboccato la via di ritorno in qualche scantinato di Seattle.

<link>Lucio Bragagnolo</link>lux@mac.com