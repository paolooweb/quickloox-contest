---
title: "Il giorno del feedback"
date: 2006-08-16
draft: false
tags: ["ping"]
---

Apro il computer e appare un allarme iCal. Proviene dal calendario condiviso di un mio amico. Come al solito cerco di fare a meno del mouse per disporne&#8230; ma non si può. Ho barato e, terminando da shell il processo <code>iCalAlarmScheduler</code>, me ne sono sbarazzato ugualmente. Ma qui ci scappa un <a href="http://www.apple.com/it/macosx/feedback/" target="_blank">feedback ad Apple</a>.

P.S.: chiaro che bastava cliccare sul pulsante rosso di chiusura. Qui si è più rompiballe.

P.P.S.: Un allarme del tutto irrilevante.