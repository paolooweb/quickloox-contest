---
title: "L’apprendista stregone e il Terminale"
date: 2003-09-04
draft: false
tags: ["ping"]
---

Si può fare tutto, anche casino. Parola di avventuriero

Non ho una formazione Unix e tutte le volte che apro il Terminale lo faccio con l’animo di quello che sta iniziando la prima immersione con le bombole della sua vita.

Giorni fa ho ricevuto un lavoro sotto forma di una cinquantina di piccoli file di testo, che sarebbe stato assai più agevole poter editare sotto forma di un unico file.

Invece che fare cinquanta tediosi apri-seleziona-trascina-salva, ho pensato che sarebbe stato più elegante creare magicamente un file solo servendomi della Magia di Unix.

Ho messo tutti i file in una directory apposta per loro e ho sapientemente scritto nel Terminale:

cat *.txt > unicofile.txt

Sono così riuscito a creare un comando ricorsivo che crea un file infinito, in grado di saturare in pochi istanti tutto il disco rigido.

Il secondo tentativo,

cat *.txt > unicofile_non_elaborato_dal_comando_cat

è andato molto meglio.

Sbagliare nel Terminale e poi riuscire a fare la cosa giusta è inebriante.

<link>Lucio Bragagnolo</link>lux@mac.com
