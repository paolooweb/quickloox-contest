---
title: "Apple è solo arrivata per prima"
date: 2002-04-02
draft: false
tags: ["ping"]
---

Spesso si capisce il perché osservando il chi e il come

Qualcuno si è lamentato del recente aumento dei prezzi di iMac (Flat Panel). Apple sostiene che è dovuto al rialzo dei prezzi dei pannelli Lcd e della Ram; i malpensanti sospettano puri fini di lucro, ritardi nella produzione, incapacità di pianificazione o altro a scelta.
Nel frattempo, giorni dopo la decisione di Apple, Nec ha alzato i prezzi del proprio listino, a causa dei rialzi di prezzo di Ram e Lcd. Dell ha talvolta rialzato i prezzi e talvolta li ha lasciati invariati ma ha impoverito le configurazioni. Un rapporto degli analisti del Gartner Group ipotizza che i rialzi dei prezzi di Apple siano imputabili al rialzo del costo di Lcd e Ram.
Non sarà che Apple ha rialzato i prezzi semplicemente perché lavora meglio e se ne è accorta prima?

<link>Lucio Bragagnolo</link>lux@mac.com