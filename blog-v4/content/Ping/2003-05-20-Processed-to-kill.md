---
title: "Processed to kill"
date: 2003-05-20
draft: false
tags: ["ping"]
---

Altra uscita di sicurezza dal Terminale: come uccidere gli insetti dannosi

Sempre sulla scia dei suggerimenti dell’amico Orio, ecco come chiudere da Terminale un programma che non si chiude in altro modo.

Con il comando ps -auxc ottieni l’elenco di tutti i programmi in funzione. Quando sei nel Terminale non si chiamano programmi, ma processi. Ce ne sono molti più di quelli che si vedono.

Ogni processo ha un suo Pid (process identifier), corrispondente a un numero.

Il comando kill -9 (Pid del processo) fa quello che dice il nome inglese: uccide il processo. Se il processo ha il Pid numero 1291, il comando è kill -9 1291.

Niente salvataggio dei file non salvati, niente Annulla: il processo muore all’istante e senza via di scampo.

Quelli che hanno già scoperto kill possono provare ad approfondire e, per esempio, scoprire la differenza che passa tra kill -9 e kill -15.

<link>Lucio Bragagnolo</link>lux@mac.com