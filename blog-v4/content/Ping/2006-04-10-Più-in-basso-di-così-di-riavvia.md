---
title: "Pi&ugrave; in basso di cos&igrave; si riavvia"
date: 2006-04-10
draft: false
tags: ["ping"]
---

Se qualcuno avesse mai avuto la curiosit&agrave; di sapere perch&eacute; 10.4.6 fa riavviare due volte i Mac PowerPc dopo l&rsquo;aggiornamento, sappia che &egrave; dovuto alla presenza di una versione pesantemente rinnovata della libreria <code>/usr/lib/libSystem.B.dylib</code>.

&Egrave; talmente vitale al funzionamento del sistema che, dati i cambiamenti, non la si pu&ograve; cambiare al volo e serve una sostituzione molto prudenziale della libreria stessa in due passi distinti.

Su Intel non serve perch&eacute; i cambiamenti apportati sono di entit&agrave; molto minore.