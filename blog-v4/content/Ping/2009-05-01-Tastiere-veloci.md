---
title: "Tastiere veloci"
date: 2009-05-01
draft: false
tags: ["ping"]
---

È solo una battuta&#8230; ma qualcuno ha messo davvero <a href="http://crave.cnet.co.uk/cartech/0,250000513,49302140,00.htm?tag=mncol;txt" target="_blank">a confronto le tastiere di netbook e iPhone</a> nella situazione più <em>mobile</em> che è riuscito a trovare. :-)