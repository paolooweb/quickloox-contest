---
title: "La forma e il contenuto"
date: 2010-06-20
draft: false
tags: ["ping"]
---

Le discussioni su iPad che fa morire Mac mi sembrano tachicardiche. Se Mac non ha più senso di esistere, che muoia è una fortuna. Se ha senso di esistere perché fa infinite cose che iPad non può fare, non muore.

È la seconda. Il resto è iperventilazione cerebrale, del tutto inutile.

Altra discussione simile è quella per cui <a href="http://www.slate.com/id/2257495/pagenum/all" target="_blank">il portatile farà morire il desktop</a>. Ci sono un sacco di cose per cui i portatili sono meno indicati dei desktop, quindi i desktop non muoiono. Magari se ne venderanno meno, magari serviranno solo per cose specifiche, ma chi vuole un desktop lo troverà sempre. Chi vuole un Mac pure, salvo catastrofi planetarie: la Apple odierna ha in cassa i soldi per vivere cinque anni anche se a partire da oggi non guadagnasse più neanche un centesimo. E i Mac, stando ai dati degli ultimi cinque anni, si vendono come il pane.

Il problema non è delle persone normali. È dei calcificati che, siccome lavoro con iMac, voglio un iMac da qui fino all'età della pensione. Siccome ho imparato a usare il mouse, non posso né voglio imparare a fare nient'altro.

Prima di protestare contro il cambiamento, tra l'altro prima ancora che il cambiamento avvenga davvero, forse sarebbe il caso di verificare se il cambiamento non possa essere vantaggioso. Anche per un miope attento solo al proprio <i>particulare</i> e incapace di vedere l'insieme delle cose è una prospettiva da considerare.

Non so perché e non so come, ma associo nella mente tutto questo discorso alle persone che si lamentano di avere il disco rigido pieno e, accumulati metri cubi di foto dentro iPhoto, non passano neanche cinque minuti a selezionare le foto ben riuscite e a buttare via il resto. C'è un collegamento che intuisco senza comprendere, legato al fatto che chi parla solo di come le cose appaiono va ignorato, a favore di chi cerca di capirle e vedere che cosa c'è dentro ogni situazione.