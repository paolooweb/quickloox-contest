---
title: "Libertà, fraternità, uguaglianze"
date: 2008-01-25
draft: false
tags: ["ping"]
---

<strong>Carmelo</strong> una ne fa e cento ne trova. Ecco la più recente:

<cite>Accennavo un po' di tempo fa ad un software che si integrava con Office per scrivere simboli ed equazioni matematiche.</cite>

<cite>Il prodotto è <a href="http://www.dessci.com/en/products/mathtype" target="_blank">Mathtype</a>, che è stato utilissimo a mia sorella a Siena per la sua tesi in economia bancaria e in particolare ad un suo docente utente e fanatico Mac di vecchia data (su alcuni computer usa ancora il Classic!).</cite>

<cite>Da un'occhiata rapida vedo che si integra anche con T<sub>e</sub>X e L<sup>a</sup>T<sub>e</sub>X, pertanto spero che possa essere utile anche a quanti si rifiutano di usare Microsoft Office. Io comunque ne ho testato la compatibilità con Office: Mac ed è pienamente soddisfacente.</cite>

Che si possa fare a meno di Office è&#8230; matematico.