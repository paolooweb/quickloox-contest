---
title: "Signori si cambia"
date: 2008-09-30
draft: false
tags: ["ping"]
---

Qualche giorno prima di partire per le vacanze, il mio <em>provider</em> ha cominciato a fornirmi irregolarmente la banda, che spariva per intere ore al giorno per ritornare frequentemente verso sera o di notte.

Ho lasciato una segnalazione al supporto tecnico e non me la sono presa troppo, perché ci sarebbero state di mezzo le vacanze e molto probabilmente quello che credevo un problema momentaneo si sarebbe superato da solo o quasi.

Tornato dalle vacanze, il problema permane ed è persino peggiorato. Di giorno non c'è proprio connessione e la banda torna solo in ore strettamente notturne, per svignarsela al sorgere del sole o anche prima. A questo punto è passato quasi un mese dall'inizio dei malfunzionamenti.

Ho segnalato nuovamente la cosa al supporto tecnico, che mi ha invitato a fare le prove più varie (cavo Adsl direttamente collegato alla presa, staccare il <em>cordless</em> e gli altri apparecchi eccetera). Il punto è che, quando c'è banda, tutto funziona perfettamente; la banda va e viene da sola e l'impianto resta sempre quello. Ci ho scaricato un paio di giga tra aggiornamenti e lavoro e mi risulta difficile credere a un guasto nelle mie apparecchiature.

Ancora un paio di giorni e, se va avanti cos&#236;, si cambia. Per citare un autore che detesto, ma in questo caso l'aveva azzeccata, non sappiamo se la situazione sarà migliore quando cambierà. Ma sappiamo che deve cambiare se vogliamo che sia migliore.