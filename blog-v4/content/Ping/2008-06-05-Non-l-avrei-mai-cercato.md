---
title: "Non l'avrei mai cercato"
date: 2008-06-05
draft: false
tags: ["ping"]
---

Per fortuna c'è <strong>Jacopo</strong>, altrimenti non avrei mai conosciuto <a href="http://goosh.org/" target="_blank">Goosh</a>, ovvero Google con un'interfaccia stile Terminale.

Google quello vero è un'altra cosa. Però il principio di Goosh è ben più azzeccato della scempiaggine che pare alla prima occhiata. Tre minuti per prenderci la mano e in alcune situazioni, provare per credere, si fa prima. Un Goosh 1.0 fatto con dietro un finanziamento, invece che una 0.0.4-beta #1 realizzata da un programmatore con voglia di divertirsi, sarebbe uno strumento impagabile.