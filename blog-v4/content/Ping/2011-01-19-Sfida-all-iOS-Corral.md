---
title: "Sfida all'iOS Corral"
date: 2011-01-19
draft: false
tags: ["ping"]
---

Qualcuno saprebbe dire al volo <a href="http://iosfonts.com/" target="_blank">quanti font esistono su iPhone? E su iPad?</a> E quanti font c'erano su un telefono intelligente a Natale 2006?