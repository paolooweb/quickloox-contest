---
title: "Un amore di sistema<p>"
date: 2005-01-06
draft: false
tags: ["ping"]
---

Non è solo il diavolo a nascondersi nei dettagli<p>

Mi scrive Alex Raccuglia:<p>

<em>La cosa più bella del Mac è che ti diventa amico senza che tu te ne accorga: l&rsquo;icona di iPhoto comprende anche il modello della fotocamera da te utilizzata. Se hai una Canon come me mette la Canon, se hai una Nikon, come te&hellip;</em><p>

<em>Io ADORO questo sistema operativo&hellip; :-)</em><p>

Vorrei solo fare notare che Alex non è un ragazzino in tempesta ormonale ma un apprezzato sviluppatore di software, produttore di un vero lungometraggio - <a href="http://www.progettoskarr.net">Skarr</a> - interamente creato con Macintosh nonché professionista affermato.<p>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>