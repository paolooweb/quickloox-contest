---
title: "Un Comando che mancava"
date: 2010-02-13
draft: false
tags: ["ping"]
---

<a href="http://www.openoffice.org/" target="_blank">OpenOffice.org 3.2</a> finalmente ha capito che, sui Mac, Comando-destra porta a fine riga e Comando-sinistra a inizio riga di un documento di testo.

Di più non posso ancora dire sulla nuova versione, ma ci lavorerò con discreta intensità nei prossimi giorni.