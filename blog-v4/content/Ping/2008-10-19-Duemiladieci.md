---
title: "Duemiladieci"
date: 2008-10-19
draft: false
tags: ["ping"]
---

In qualche data di novembre, non so ancora quando, facciamo la pizzata dei duemila Ping.

Nel frattempo, venerd&#236; 31 ottobre c'è la pizzata dei dieci anni del Poc. Durante la giornata, ci sarà la festa di compleanno di <a href="http://atworkgroup.net" target="_blank">Mac@Work</a> e sempre di Mac@Work la premiazione del concorso fotografico indetto quest'estate.

Sarò l&#236; da metà mattina fino a notte fonda. Se qualcuno ha tempo e voglia, bella occasione per vedersi, chiacchierare, gustarsi la pizza e financo parlare di FireWire, però con in mano i nuovi MacBook e MacBook Pro. Più bello e più gustoso dare giudizi dopo avere toccato, che dopo avere letto.