---
title: "Solitari da tastiera"
date: 2009-07-11
draft: false
tags: ["ping"]
---

I programmatori hanno troppo tempo libero. Questo ha scritto un <a href="http://www.shellscriptgames.com/" target="_blank">Tetris e un Solitario per il Terminale</a>.

Per giocare a Tetris bisogna portarsi nella directory che contiene i file e dare nel Terminale il comando <code>./shelltris.sh</code>. Le (semplici) istruzioni per giocare veramente sono nei file di accompagnamento.

Il Solitario non mi funziona. Ci vuole una finestra Terminale molto grande e non è un problema, ma poi ci sono problemi di visualizzazione.

Fuori dalla curiosità, chi vuole approfondire i temi dello scripting della shell qui troverà pane per i suoi denti. E presumbilmente la fine della propria vita sociale.