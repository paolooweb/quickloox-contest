---
title: "Una prova al giorno"
date: 2008-11-14
draft: false
tags: ["ping"]
---

Ragionevolmente sicuro che l'adattatore Usb-Ethernet di MacBook Air non funzionasse sulla porta Usb di un non-MacBook Air, ho provato e non funziona.

Ragionevolmente dubbioso sui dubbi sentiti da un amico e riguardanti la <em>trackpad</em>-pulsante dei nuovi MacBook (dice che il computer non rileva i clic fisici), ho (ri)provato, sullo stesso MacBook che ha usato lui, e funziona.

Sono matematicamente certo che la pre-beta di Snow Leopard non fa il <em>boot</em> sul mio vecchio PowerBook e quindi non ci provo nemmeno.