---
title: "Disposti quasi a tutto"
date: 2006-09-01
draft: false
tags: ["ping"]
---

Quando uno non si fosse divertito abbastanza a leggere che <a href="http://www.spymac.com/ipod/" target="_blank">cosa non si farebbe pur di avere un iPod</a>, resta sempre la colonna di che cosa si è disposti a dare un iPod pur di avere.

Surreale.