---
title: "Puntare in alto"
date: 2003-03-04
draft: false
tags: ["ping"]
---

Un piccolo trucco tanto per giocare con il Dock (e Unix)

Sono tanti i comandi Unix che fa comodo imparare. Uno di questi è defaults.
Per divertimento, prova a scrivere nel Terminale

defaults write com.apple.dock orientation top

e guarda che cosa succede. Per risistemare le cose si può tranquillamente procedere da menu Apple.

<link>Lucio Bragagnolo</link>lux@mac.com