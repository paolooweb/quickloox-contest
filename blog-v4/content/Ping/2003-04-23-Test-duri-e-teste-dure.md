---
title: "Test duri e teste dure"
date: 2003-04-23
draft: false
tags: ["ping"]
---

La velocità non è tutto e i confronti non valgono nulla

Hai presente i talk show televisivi a tema sentimentale? Per buona parte molti di loro sono inventati ma fanno ugualmente un gran successo. La loro ricetta vincente è partire dall’assunto che in amore esistono regole valide per tutti e che quindi il comportamento di ciascuno possa essere misurato in base alle regole stesse.

Tutti sanno benissimo che non è così. In amore esistono ben poche regole e quelle che ci sono valgono per ciascuno di noi in modo diverso, con priorità differenti. Ma il pubblico cade nella trappola e si sorbisce ore di chiacchiere.

Analogamente, c’è chi fa ascolto, vende copie, crea attenzione con i test di velocità tra computer. Una volta per tutte: nel complesso, mediamente, oltre un certo limite, sono vastamente fregnacce.

I nostri computer sono tutti, a modo loro, innamorati. Le stesse regole valgono per ciascuno in modo diverso. Tant’è vero che, in altro ambito, quando esce un aggiornamento software c’è sempre qualcuno che ha problemi, a parità di configurazione e di sistema aggiornato.

Non ho spazio per entrare nel merito, ma affermo qui e ora che gli unici test di velocità che vale la pena di fare sono sulla propria macchina, rispetto alla propria macchina; per esempio per vedere se Jaguar va più veloce di 10.1 o se la nuova versione di Safari è più o meno performante. E anche questi test vanno presi molto, moltissimo con le molle.

L’unica misura di velocità valida è “fa quello che mi serve alla velocità che mi serve”? Il resto, spiacente per chi ha bisogno di continuare a crederci, sono talk show.

<link>Lucio Bragagnolo</link>lux@mac.com