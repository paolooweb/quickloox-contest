---
title: "Mele d'archivio"
date: 2009-04-10
draft: false
tags: ["ping"]
---

Per accedere in fretta a un emulatore di Apple II e a oltre 1.300 programmi per Apple II e Apple Gs ora è sufficiente <a href="http://www.virtualapple.org" target="_blank">avere un browser</a>.

Ironia della sorte, Mac è la piattaforma meno compatibile con il sito. Ma le cose dovrebbero aggiustarsi abbastanza in fretta.

Al momento si può dire che sia gente Windows che finalmente ha capito, con soli trent'anni di ritardo.