---
title: "Mi faccia la demo"
date: 2006-10-12
draft: false
tags: ["ping"]
---

Non posso fare nomi (spero di poterli fare più avanti) perché stavolta sono veramente grossi. Dico solo che mi sono trovato di fronte a una scrivania di quelle pesanti e chi ci sedeva dietro mi ha detto <cite>per favore, mi faccia vedere come funziona questo World of&#8230;</cite>.

Potrebbe nascerne lavoro importante o forse no. Però si è parlato di software ludico in un ambiente business apparentemente lontanissimo. Il mondo sta cambiando e il gioco in sé è solo un segnale.

Sta diventando &#8212; finalmente &#8212; una necessità, come il movimento e l'imparare.