---
title: "Nove pollici di passione<p>"
date: 2005-07-31
draft: false
tags: ["ping"]
---

Un gruppo musicale Mac-oriented per davvero<p>

Sono cose un po&rsquo; vecchie per chi li segue, ma la segnalazione di <strong>Luca</strong> mi ha spinto a curiosare e così mi ritrovo a parlarne. Anzi, lascio parlare lui:<p>

Non so se li conosci, ma i Nine Inch Nails da tempo rendono disponibili un paio dei loro pezzi anche in formato GarageBand, e ti istigano a modificarli e a rispedirli, oppure a farci quello che vuoi. E questa è storia vecchia. Di nuovo c'è il loro ultimo video, Only, diretto da David Fincher. Tre oggetti in scena: un PowerBook con sullo schermo gli effetti visivi di iTunes, una tazza da caffè che vibra al ritmo della musica, e uno di quei bagagli che creano superfici 3D con dei cilindri metallici. Il risultato? Fantastico.<p>

<strong>Enrico</strong> ha rincarato la dose, ricordandomi che i Nin sono stati intervistati da Macworld e che sul sito della rivista si trova ancora <a href="http://www.macworld.it/showPage.php?id=7740&template=notizie">l&rsquo;introduzione</a> all&rsquo;intervista stessa.<p>

Sono poi andato sul <a href="http://www.nin.com/">sito della band</a>, scoprendo che i browser raccomandati sono Safari e Firefox.<p>

Se serviva a testimoniare una passione genuina per il Mac, beh, questa è proprio la prova del Nine.<p>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>