---
title: "La discussione che mi sono perso"
date: 2006-08-28
draft: false
tags: ["ping"]
---

Sulle liste che frequento l'agosto è stato il prevedibile mortorio, ma una cosa interessante è venuta fuori: la disponibilità (o meno) su Mac di software per il trattamento delle immagini astronomiche.

Il problema era trovare un'alternativa a <a href="http://www.astrosurf.com/buil/us/iris/iris.htm" target="_blank">Iris</a>, che gira solo su Windows. Senza sapere un'acca della materia mi sono messo a cercare e ho trovato per esempio <a href="http://www.chara.gsu.edu/~gudehus/miips.html" target="_blank">Miips</a>, <a href="http://iraf.noao.edu/" target="_blank">Iraf</a> e <a href="http://aips2.nrao.edu/docs/aips++.html" target="_blank">Aips++</a>. Ma non so se siano utili o meno.

Si accettano pareri da chiunque ne sappia e, se è in ascolto, specialmente dall'amico Flavio, che ringrazio in anticipo. :-)