---
title: "In cerca dei salumieri dell&rsquo;iPod<p>"
date: 2004-10-19
draft: false
tags: ["ping"]
---

Pensare un tanto al chilo porta difficilmente a conclusioni condivisibili<p>

Dopo che nell&rsquo;ultimo trimestre fiscale Apple ha venduto due milioni di iPod e che la musica digitale è arrivata a rappresentare oltre un quarto del fatturato di Apple, non è il caso di dilungarsi sulle cifre.<p>

Più che altro, facendo due conti, si scopre – anche se Apple non dirama cifre precise in materia – che gli iPod mini stanno vendendo come pazzi, ancora più degli iPod.<p>

Forte di questo sono in cerca dei salumieri dell&rsquo;iPod: quelli che, all&rsquo;uscita di iPod mini, borbottavano che, a quel prezzo, con quel disco così piccolo, nessuno ne avrebbe voluto uno, che avrebbe dovuto costare meno eccetera.<p>

Gente che sa fare i conti solo al chilo o al gigabyte, solo come i salumieri. Solo che i salumieri veri sono gente intelligente, che sa vendere la propria merce. Qualcosina, in termini di vendite, lo sa fare anche Apple.<p>

I salumieri dell&rsquo;iPod, invece, sanno solo pontificare e poi nascondersi. In giro, infatti, non ne sento più nessuno.<p>

<a href="mailto:lux@mac.com">Lucio Bragagnolo</a>