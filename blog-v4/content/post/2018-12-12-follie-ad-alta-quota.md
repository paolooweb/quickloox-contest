---
title: "Follie ad alta quota"
date: 2018-12-12
comments: true
tags: [Wi-Fi, Vienna, Malpensa, Katowice, Varsavia, Monaco, Cracovia, Bergamo, Orio, Ostrava, Linate, iPad, Pro, metropolitana, Modena, Frecciarossa, Lufthansa]
---
Negli ultimi trenta giorni sono transitato dagli aeroporti di Milano (Malpensa e Orio al Serio), Monaco, Vienna, Varsavia, Cracovia e Katowice (due volte, uno scalo che fa sembrare Linate uno hub intercontinentale).

Non per fare quello che viaggia, ma per dire che ovunque ho trovato Wi-Fi immediato e di buona qualità. Solo a Monaco viene chiesta una registrazione, utile per chi passa frequentemente; altrove è tutto istantaneo. A Ostrava (Repubblica Ceca) abbiamo pranzato in una bettola dove la cameriera a momenti neanche parlava il ceco, ma ha saputo produrre all’istante un bigliettino con la password del Wi-Fi.

A Malpensa chiedono la registrazione e, non bastasse, il Wi-Fi parte con il conteggio alla rovescia: cinque, quattro, tre, due, uno… l’idea è che si dovrebbe rimanere a bocca aperta di fronte a un evento tanto straordinario.

Sembra che a bordo l’atteggiamento generale verso l’elettronica di consumo sia _fate quello che vi pare purché in modalità aereo e tenete chiusi i portatili durante il decollo e l’atterraggio_. A Vienna di fianco a me una coppia guardava un film su iPad Air mentre io scrivevo su iPad Pro. La hostess ha ignorato la coppia e mi ha chiesto di spegnere il portatile, equivoco di cui sono andato molto orgoglioso. E poi dicono che iPad non è un computer.

In una stazione della metropolitana di Milano erano fuori uso tre biglietterie automatiche su tre, con tre diversi errori. Secondo me è record galattico.

Ho anche preso due Frecciarossa, negli intervalli. Due volte ho disattivato il Wi-Fi per impossibilità di connettersi. Sarò sfortunato.

Ah, Lufthansa inizia a offrire il Wi-Fi in volo, con il livello minimo di banda al corso di tre euro.