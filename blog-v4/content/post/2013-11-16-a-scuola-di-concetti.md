---
title: "A scuola di concetti"
date: 2013-11-16
comments: true
tags: [Pov-Ray]
---
A scuola si dovrebbe imparare a scrivere e non a usare Word. Si dovrebbe studiare l’Html e non Dreamweaver. Si dovrebbe studiare il funzionamento dei sistemi operativi invece che Windows.<!--more-->

Sono anni che faccio questi esempi e mi rendo conto che mi danno la ragione che si dà ai matti. Troppo difficile insegnare a imparare, specie se non si è imparato troppo bene a insegnare.

Allora dico una cosa da matto vero: a scuola, ora che arrivano le stampanti tridimensionali e nasce la possibilità di creare oggetti finora inesistenti, frutto dell’incrocio tra software e hardware finalmente miniaturizzabile e sufficientemente economico, dovrebbero essere materia obbligatoria la modellazione e la resa 3D degli oggetti.

Non per imparare Cinema 4D o Maya o chissà che altro: a imparare i fondamenti. Con programmi come [Pov-Ray](http://povray.org), tanto elementare quanto prezioso per capire i meccanismi della disciplina.

Non costa niente e adesso, da una licenza che era più o meno libera e però insomma, è passato a una licenza *open source* a tutti gli effetti.

Funziona su qualsiasi computer di minima decenza e prendere il codice sorgente per compilarlo dovrebbe anch’esso essere materia di studio scolastico.

Chissà se riesco a fare passare il concetto.