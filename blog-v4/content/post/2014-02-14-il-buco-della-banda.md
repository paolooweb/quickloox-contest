---
title: "Il buco della banda"
date: 2014-02-14
comments: true
tags: [Vodafone, Wind, iPad, Keynote, Mac, iOS, Frecciarossa, Internet, Wi-Fi, iPhone]
---
Già che [si parlava di batterie](https://macintelligence.org/posts/2014-02-12-acquisti-che-durano/), le danze per l’iPad da combattimento sono cominciate alle sette in punto e l'oggetto si è definitivamente arreso alle 18:41, undici ore e quaranta. In mezzo, una pausa pranzo e una mezz’ora di relax post-intervento; connessione cellulare sempre attiva, Wi-Fi sempre attivo. In aggiunta, due ore abbondanti di macchina costantemente accesa, collegata al videoproiettore e con Bluetooth attivato per il controllo a distanza da parte di iPhone.<!--more-->

Gran lavoro di Keynote, come al solito. Navigazione varia, posta elettronica, iMessage. Il nuovo Keynote pesa meno sulle risorse rispetto alla versione precedente. Un iPad Air non batterebbe ciglio naturalmente; il mio terza generazione iniziava a soffrire una presentazione pesante (le mie lo sono sempre, cerco video e immagini sempre di qualità superiore a quelle del proiettore che le mostrerà). Keynote nuova generazione è più leggero.

Su Mac, il nuovo Keynote permette nuovamente di variare lo stile di diapositive singole e sarebbe bello che la possibilità arrivasse anche sulla versione iOS.

È stata fatta una cosa intelligente eliminando il bisogno dell’applicazione Remote per pilotare da iPhone una presentazione su iPad o su Mac; adesso basta che ci sia Keynote installato anche su iPhone. Meno intelligente però è che la funzione di telecomando preveda solo lo schermo orizzontale. Lascerò *feedback*.

Una nota su Internet. Lo stato delle connessioni italiane è pietoso e magari, per stare sull’attualità, bastasse rifare il governo. Ho fatto Milano-Torino in Frecciarossa e il Wi-Fi a bordo è stato inesistente, probabilmente perché il treno era gremito. Come non averlo.

Tutti bravi a chiedere connessione gratis, mi si dirà. Vero. Infatti, dopo avere constatato il disservizio, ho acceso la connessione cellulare a pagamento su iPad (Vodafone). Peggio che andare di notte. I tratti dove il servizio è assente o è a livelli da barzelletta non si contano. Per non parlare delle gallerie, dove naturalmente è il deserto.

Il viaggio da Torino a Savigliano non è stato migliore. Quello di ritorno da Savigliano a Torino appena decente; il rientro verso Milano, su un regionale veloce, è stato – parlando di banda – penoso. Viene naturalmente da stigmatizzare Vodafone. Tuttavia i tratti dove proprio non c’era campo neanche in voce, o faceva finta di esserci, erano numerosi e importanti.

Da qualche parte ci si culla ancora con l’idea che Internet in viaggio sia un lusso da ricchi, o peggio che possa essere uno scherzo di Carnevale, ti dico che c’è poi non c’è e giù quattro risate. È uno strumento di lavoro e anche ove fosse uno sfizio per scemi, beh, gli scemi pagano il biglietto come gli intelligentoni.

Lungo il Frecciarossa passa un carrello pieno di quotidiani. Chili e chili di carta. Il treno era pieno e però nessuno prendeva il quotidiano, chissà per quale misteriosa ragione. Per non vedere l’evidenza di certi segnali bisogna impegnarsi. C’è anche da dire che la banda, ci sia o non ci sia, si paga uguale, quindi per chi la vende in Italia diventa rapidamente un investimento trascurarla.

Domani racconto che si è detto, roba più allegra, con dentro gente che lavora invece che fare finta.