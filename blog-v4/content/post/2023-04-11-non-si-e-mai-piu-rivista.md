---
title: "Non si è mai più rivista"
date: 2023-04-11T10:53:34+01:00
draft: false
toc: false
comments: true
categories: [Internet]
tags: [MacFormat, Giacobazzi, Marco Giacobazzi, Macworld, Applicando, MacAddict]
---
In un’epoca altra l’Italia più attenta ai Mac vide nelle edicole qualcosa di anomalo. Un *terzo* mensile a sfidare i campioni di sempre, *Macworld* e *Applicando*. Esistevano piccole testate alternative al duopolio, ma nessuna capace di impensierirlo facendo numeri consistenti.

Per i venditori di spazi pubblicitari era una follia, perchè a loro dire non c’era sufficiente domanda per tutti. Era una follia anche la rivista in quanto tale, che scommetteva su un aumento consistente del numero di lettori di riviste Mac in Italia una volta messo in conto un modesto travaso di lettori dalle due testate tradizionali a quella nuova. Per la vita della rivista era necessario raccogliere un bacino di lettori che, fino a quel momento, proprio non lo erano.

Dietro all’operazione c’era un editore che sfruttava il boom dell’informatica personale e aveva sconvolto il settore con un approccio spregiudicato e aggressivo, mescolato alla semplicità intenzionale del linguaggio, pensata per attirare in edicola quanta più gente possibile. L’editore era antiMac per ragioni molto pratiche (Apple, che non faceva pubblicità sulle altra riviste, non la faceva nemmeno sulla sua), ma i diritti di traduzione sulla rivista originale inglese facevano parte indissolubile di un pacchetto consistente, che alimentava le riviste di alta tiratura e alto valore pubblicitario. Piuttosto che sprecare una licenza, tanto valeva pubblicare la rivista e vedere se riusciva a diventare importante e redditizia.

Date le premesse, non stupisce che le risorse a disposizione della redazione fossero poche. L’editore non era interessato a spingere la rivista, che sarebbe sopravvissuta solo tirandosi da sola per i capelli fuori dalle acque pericolose, esattamente come il mitico barone di Münchausen.

Non era come oggi. *Macworld* e *Applicando* pubblicavano contenuti di qualità, realizzati da persone capaci e preparati per la rivista con autentica professionalità giornalistica. La qualità era un *must* così come lo stare in un budget, lo dico eufemisticamente, compresso.

Non molti erano in grado di occuparsi della produzione di un mensile praticamente da soli, con qualità giornalistica, consci dei vincoli di tempo, denaro e pressione dell’editore.

Finì che chiamammo Marco. Con grande esperienza eppure flessibile come un ragazzo arrivato ieri, pratico, pieno di idee, ingegnoso e pieno di contatti utili. Il suo contributo costituì il fulcro di *MacFormat* e fu decisivo per dare vita a una rivista davvero capace di fare numeri paragonabili a quelli dei concorrenti blasonati e, soprattutto, farli con lettori nuovi, ampliando il bacino di interesse invece che prendere il posto di una concorrente.

*MacFormat* è stata, immagino, un’eccezione alla regola: non penso siano state molte le riviste pubblicate da un editore a esse ostile. Erano altri tempi, ma una testata capace di mantenersi e fare bene in queste condizioni costituiva sicuramente un *unicum*.

Marco portò professionalità ed entusiasmo, impegno e leggerezza, di quella buona, che rallegra la giornata. Per dire del carattere della persona, quando *MacFormat* arrivò alla fine del suo ciclo di vita, provò a portare in Italia *MacAddict*, mensile americano bellissimo e davvero controcorrente. Da solo. E giunse a un passo dal riuscirci.

Un’esperienza analoga a quella di *MacFormat* non si è più vista e non è più ripetibile. La scomparsa delle edicole e della lettura su carta è una ragione molto meno sostanziale della scomparsa, oggi, di Marco.