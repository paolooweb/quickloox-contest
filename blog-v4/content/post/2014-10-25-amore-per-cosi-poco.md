---
title: "Amore per così poco"
date: 2014-10-25
comments: true
tags: [BBEdit, Siegel]
---
Sono un acquirente di [BBEdit 11](http://www.barebones.com/products/bbedit/bbedit11.html). Anzi, un aggiornante, che compie un ennesimo *upgrade* dopo quello a BBEdit 10, tre anni fa.<!--more-->

Sono anni in cui racconti a qualcuno di avere speso 29,94 euro per aggiornare un editor di testo e vieni guardato strano. Ti viene chiesta una giustificazione. Tutti i programmi sono uguali, no? Non c’è un modo di farlo gratis? Perché ti azzardi a premiare un lavoro ben fatto, obbligando gli altri a scovare un modo di spiegare che no, a loro non serve, come se dovessero sentirsi in colpa? Sempre colto di sorpresa farfuglio qualcosa sulle [novità principali](http://www.barebones.com/products/bbedit/bbedit11.html), mentre dovrei rispondere ogni volta con la stessa frase: serve davvero una giustificazione per *non* spenderli, quei trenta dollari. Sì, sentirsi in colpa per non avere BBEdit ha un senso.

Anche [l’ultimo libro su cui ho per ora messo le mani](http://www.apogeonline.com/libri/9788850317264/scheda), come da anni e anni, è stato scritto con BBEdit. Tutti i *post* di questo *blog* sono redatti con BBEdit. Per me estrarre BBEdit quando c’è da scrivere è diventato riflesso condizionato. Se devo rimproverare qualcosa a [Bare Bones Software](http://www.barebones.com) è non produrre BBEdit per iPad. Per ora, spero.

Jason Snell ha enunciato [le ragioni per BBEdit 11 sul suo nuovo blog *Six Colors*](http://sixcolors.com/post/2014/10/bbedit-11-arrives/), molto meglio di come potrei mai fare io.

>Sì, un motivo è che lo uso da sempre ed è diventato parte del mio cervello. Sono sicuro di usare una frazione delle sue possibilità – non sono un programmatore – ma non importa. Quello che uso, lo amo. Per esempio il supporto delle espressioni regolari; non le uso tutti i giorni, ma quando lo faccio, BBEdit mi fa risparmiare secondi, minuti, persino ore di tempo. […]

>Secondo Rich Siegel, l’autore principale di BBEdit, c’è una “intera pila di rilavorazione interna” sotto il cofano di questa versione. Quando sviluppi software per due decenni, avere codice sempre fresco è un problema costante. Siegel sostiene che a ogni sistemazione di un bug o a ogni aggiunta di una nuova funzione in vista di BBEdit 11, è stato verificato se il codice intorno era moderno o andava riscritto. Nel secondo caso è stato fatto, mentre altri avrebbero rammendato il vecchio codice nella speranza che tenesse per un altro paio di anni. […]

>In tutto, a dare retta a Siegel, ci sono 224 modifiche distinte.

L’[elenco delle modifiche](http://www.barebones.com/support/bbedit/arch_bbedit11.html) è da solo ragione sufficiente per mettere mano alla carta di credito. A compensazione di tutti quelli, minuziosissimi, che compaiono regolarmente in occasione di un qualsiasi aggiornamento minore.

A qualcuno sembrerà poco. Per me è amore da una vita.