---
title: "Come non resistere alla tentazione"
date: 2022-11-10T13:58:10+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [Ventura, macOS, Adobe, InDesign, Creative Cloud, Scribus]
---
Adobe vuole venti euro al mese per usare InDesign, che però a me [serve pochi giorni](https://macintelligence.org/posts/2021-11-30-il-buono-il-brutto-e-il-cattivo/), tre volte l’anno. Provo a uscire dall’abbonamento mensile e Adobe chiede centonove euro per riparare allo sgarro.

Uno dice *beh, però il software è sempre aggiornato e migliorato*.

Infatti installo [macOS Ventura](https://www.apple.com/it/macos/ventura/), come chiede Mac mini. L’antimarketing indipendente degli acchiappaclic ha detto di Ventura molto male e chissà, magari hanno ragione, si è appena finito di installare.

Saprò dire; intanto, ho autorizzato l’installazione prima di andare a pranzo e l’ho trovata conclusa al mio ritorno. Un’ora, niente di più; da tempo non vedevo un macOS così rapido a prendere posto.

C’è un solo programma che dà problemi e per Ventura risulta *danneggiato*, non apribile: una utility di Creative Cloud di Adobe, datata 2019. Perché il software in abbonamento è sempre aggiornato.

Non resisterò alla tentazione di pagare i centonove euro e uscire dall’abbonamento. Sì, in quanto Adobe propone, pur di restare sodali, due mesi di tariffa gratis. Nei quali non saprei che farmene, del loro software; ma loro sì, dato che lo usano per tracciare in maniera indecente il mio uso del computer, perché venti euro al mese ancora non gli bastano.

Facile non resistere alla tentazione. Costretto a pagare per usare InDesign, felice di pagare pur di uscirne anche perdendoci, almeno fino alla prossima volta.

Ho proposto a un cliente che usa InDesign, ma tutto sommato non è così costretto, di sostituirlo con software *open source*. Se va, sarò più soddisfatto di essere cliente Adobe e ricavarne motivazione per farla abbandonare ovunque riesco. *[Scribus](https://www.scribus.net), anyone*?