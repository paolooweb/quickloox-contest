---
title: "Il regalo perfetto"
date: 2023-12-03T19:32:36+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [EmacsConf, VLC, Emacs, Lisp, Emacs Lisp]
---
Ho trovato il mio regalo perfetto per Natale: tempo e concentrazione per seguire e dare seguito al *talk* [Una avventura testuale in Org-Mode per imparare le basi di Emcas, dentro Emacs, scritta in Emacs Lisp](https://emacsconf.org/2023/talks/adventure).

Non a tutti potrà piacere l’argomento. Però ci sono alternative: [Creare e presentare corsi universitari con Emacs e uno stack di software libero](https://emacsconf.org/2023/talks/uni), [Chi ha bisogno di Excel? Amministrare i voti dei tuoi studenti con org-table](https://emacsconf.org/2023/talks/table) o [One.el: il generatore di siti statici per programmatori in Emacs Lisp](https://emacsconf.org/2023/talks/one).

Sono esempi. Conto – approssimativamente – [quarantuno interventi che erano in programma oggi a EmacsConf](https://emacsconf.org/2023/talks/#date-2023-12-03). E sono in elenco anche sessioni praticamente per chiunque, come [GNU Emacs: un mondo di possibilità](https://emacsconf.org/2023/talks/world).

Le cose che guarderò davvero mentre faccio finta di studiare, come accade per ogni bravo liceale: [Perché Nabokov userebbe Org Mode se scrivesse oggi](https://emacsconf.org/2023/talks/nabokov) e [Come uso i giochi di ruolo Tabletop dentro Emacs](https://emacsconf.org/2023/talks/solo).

Ah, per gli amanti della ricorsività: [come usiamo Org Mode e Tramp per organizzare e dare vita a una conferenza multisessione](https://emacsconf.org/2023/talks/emacsconf).

Giusto perché qualcuno pensa che Office sia indispensabile o non esista vita fuori da Excel.

Nota tecnica: i video si guardano tranquillamente da Safari, non su iPad però. Lì occorrono competenze speciali, come saper scaricare gratis [Vlc media player](https://apps.apple.com/it/app/vlc-media-player/id650377962).