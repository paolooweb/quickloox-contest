---
title: "Una giornata come tante"
date: 2015-12-22
comments: true
tags: [Go, iPad, Natale, Dna]
---
Poi uno dice che non sono tempi interessanti. Nelle ultime ventiquattr’ore ho fatto *lobby* per ricevere in regalo a Natale una plancia di [Go](http://www.figg.org/index.php?lang=it) (il gioco, non il [linguaggio di programmazione](https://golang.org); sempre in merito a Natale, ho scritto gli auguri per conto di un amministratore delegato.<!--more-->

In tema meno natalizio, ho rintracciato documentazione secondo la quale deve essere possibile per un’azienda distribuire ai dipendenti iPad Pro [contenenti un Apn che prevede il collegamento a Internet tramite un server proxy](https://developer.apple.com/library/ios/featuredarticles/iPhoneConfigurationProfileRef/Introduction/Introduction.html); e ho trovato un negozietto online a prezzi più che modici per chi vuole [manipolare Dna a casa propria](http://www.the-odin.com), in cucina, nel tempo libero.

Se babbo Natale sarà veramente munifico farà trovare sotto l’albero innumerevoli giornate come questa, che per un curioso valgono come per un’ape un prato fiorito.