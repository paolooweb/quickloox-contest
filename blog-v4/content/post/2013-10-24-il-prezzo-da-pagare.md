---
title: "Il prezzo da pagare"
date: 2013-10-24
comments: true
tags: [Mac, OSX, iPhone, iPad, iOS, Windows, iWork, iLife, Office]
---
Riassumendo. Il [sistema operativo](https://itunes.apple.com/it/app/os-x-mavericks/id675248567?l=en&mt=12) è gratis, sempre e comunque. Se domani vado a cambiare il mio vecchio MacBook Pro, non pago per il [*word processor* con funzioni base di impaginazione](https://itunes.apple.com/it/app/pages/id409201541?l=en&mt=12), né per il [foglio di calcolo](https://itunes.apple.com/it/app/numbers/id409203825?l=en&mt=12) e neanche per il programma che crea [presentazioni](https://itunes.apple.com/it/app/keynote/id409183694?l=en&mt=12).<!--more-->

I programmi per [montare filmati](https://itunes.apple.com/it/app/imovie/id408981434?l=en&mt=12), [montare audio](https://itunes.apple.com/it/app/garageband/id682658836?l=en&mt=12) e [organizzare raccolte di fotografie](https://itunes.apple.com/it/app/iphoto/id408981381?l=en&mt=12) erano già gratuiti all’acquisto, ma ora lo sono anche su iOS. Così, se domani mi pungesse vaghezza di lasciarmi ipnotizzare dai nuovi mesmerizzanti [iPad Air](http://www.apple.com/it/ipad/), avrei gratuitamente il *cross-platform* totale, edito di qui, lo riprendo di là, qui aggiungo, lì tolgo, se necessario passo dal *browser* per le emergenze e tutto, tramite [iCloud](http://www.apple.com/it/icloud/), resta fedele a se stesso.

Va aggiunto che resta gratis il [programma per creare libri](http://www.apple.com/it/ibooks-author/). Così come [quello per leggerli](http://www.apple.com/it/apps/ibooks/) (da scaricare solo su iPad; su Mac è compreso nell’installazione di Mavericks).

È diventato gratuito su iOS perfino il [telecomando per il programma di presentazione](https://itunes.apple.com/it/app/keynote-remote/id300719251?l=en&mt=8).

Windows 8 [parte da 119 euro](http://www.microsoftstore.com/store/mseea/it_IT/cat/Windows-8/categoryID.66227200). Office [parte da 99 euro](http://www.microsoftstore.com/store/mseea/it_IT/cat/Office/categoryID.66226700?icid=top_nav_menu_office), abbonamento annuo.

C’è un prezzo da pagare per usare un Mac che tuttavia non cambia: sentire di gente che ha comprato un PC perché *costa meno*. Ci vediamo al prossimo aggiornamento del software. E a quello dopo. E a quello dopo…