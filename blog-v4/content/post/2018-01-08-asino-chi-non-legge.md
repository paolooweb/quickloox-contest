---
title: "Asino chi non legge"
date: 2018-01-08
comments: true
tags: [Rollinz, Esselunga]
---
Ripenso ai moduli anagrafici [bovinamente compilati a mano](https://macintelligence.org/posts/2018-01-03-ancora-carta/) in stampatello con penna biro al momento dell’allargamento della famiglia, quando inquadro con iPad un codice numerico di trenta cifre e lui automaticamente lo legge dando corso alla pratica.<!--more-->

Mi si dirà: certo, grazie, le banche hanno grandi disponibilità di denaro e grandi interessi a creare servizi come una app capace di leggere un assegno o un codice di conto corrente postale. Lo Stato invece deve fare le nozze con i fichi secchi – dopotutto si appropria solamente della metà dei guadagni individuali – e poi manca il personale e poi bla bla bla.

Non è la banca: è la app [Rollinz 2.0](https://www.esselunga.it/cms/star-wars/app-rollinz.html) di Esselunga, legata alla distribuzione di *gadget* di *Guerre Stellari**. Il codice di trenta cifre è un Credito Galattico che apre un nuovo tracciato di gara dentro la app. Roba seria, insomma.

Ma certo, l’idea di automatizzare quello che è inutile fare a mano e ricordare quello che è già stato fornito, in modo che non serva più fornirlo, per qualcuno deve essere fantascienza. Asini, nell’epoca in cui hanno imparato a leggere financo le app.