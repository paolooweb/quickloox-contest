---
title: "La morte sua"
date: 2013-09-16
comments: true
tags: [Dell, Ibm, Apple, Mac]
---
Dice che Apple si disinteressa di Mac e pensa solo a iPhone e iPad.

Però vedo che Apple continua a produrre Mac e ne vende anche un discreto numero. In nessuna epoca della propria storia Apple ha venduto così tanti Mac.<!--more-->

Nel frattempo Ibm, che ha creato il Pc, ha smesso di fabbricarli dal secolo scorso.

Dell, pioniera della vendita online di computer e azienda che ha basato il proprio successo sui Pc, torna nelle mani del proprio fondatore Michael Dell, che si concentrerà [soprattutto sui servizi](http://slashdot.org/topic/cloud/dell-wins-shareholder-approval-to-go-private/) oltre che su Pc e tavolette.

Dell si trasforma in azienda privata uscendo dai listini di Borsa. Dell – il fondatore – la paga 24,9 miliardi di dollari.

Diciamo un paio di mesi di fatturato della Apple attuale.

Che penserà forse solo a iPhone, ma dove Mac è vivo e vegeto. A morire sono i Pc degli altri.