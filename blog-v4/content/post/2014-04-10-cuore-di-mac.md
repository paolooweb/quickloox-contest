---
title: "Cuore di Mac"
date: 2014-04-10
comments: true
tags: [Heartbleed, OSX, Server, QuickLoox, Muut, Sftp, OpenSsl]
---
La notizia quasi buona: OS X e OS X Server, di per loro, [non sono soggetti al bug Heartbleed](http://www.tuaw.com/2014/04/09/why-the-openssl-heartbleed-bug-doesnt-affect-os-x-or-os-x-serve/).<!--more-->

Dopo di che sono tutte notizie cattive o come minimo potenzialmente preoccupanti. Quanto sopra ha un senso solo e unicamente se il sistema funge da server verso Internet: se – vale per il novantanove percento – usiamo qualche altra interfaccia, per esempio il sito ospitato da un *provider* o un suo *server* Sftp, per dire, non v’è certezza.

Intanto c’è un [elenco di siti](https://github.com/musalbas/heartbleed-masstest/blob/master/top1000.txt) che sono stati oppure sono affetti dalla vulnerabilità, o altrimenti non lo sono. Quelli della prima categoria potrebbero avere messo a rischio qualunque tipo di dato, *password*, carte di credito, coordinate bancarie, acquisti, foto compromettenti, qualunque cosa. Se non hanno aggiornato [OpenSsl](https://www.openssl.org) alla versione più recente e riemesso i certificati Ssl, sono tuttora a rischio.

Per controllare se un sito è vulnerabile, il suo Url va inserito in una [pagina test](http://filippo.io/Heartbleed/). [QuickLoox](http://macintelligence.org) non è vulnerabile; [Muut](https://muut.com) (ha cambiato nome!) non è vulnerabile. Questo ha purtroppo valenza limitata, come spiego più avanti.

Chi deve ricorrere a strumenti di controllo più raffinati, si rivolga a [heartbleed-masstest](https://github.com/musalbas/heartbleed-masstest) oppure a [Heartbleed](https://github.com/FiloSottile/Heartbleed).

Si trovano facilmente una [descrizione formale](http://web.nvd.nist.gov/view/vuln/detail?vulnId=CVE-2014-0160) e una [guida completa al problema](http://heartbleed.com). Questa è [la descrizione più sintetica possibile](https://www.openssl.org/news/secadv_20140407.txt), da OpenSsl stesso. 

Per avere una idea generale della situazione, [questo articolo](https://www.yahoo.com/tech/heres-what-you-need-to-know-about-the-heartbleed-bug-82120054478.html) è al momento il migliore.

La domanda da cento milioni: come quantificare il rischio? Non c’è risposta univoca e ognuno deve ragionare per sé.

Anche i siti che sono a posto **potrebbero** essere stati vulnerabili (alcuni lo sono stati certamente). Se sono stati vulnerabili, **potrebbero** essere stati attaccati, oppure no. L’attacco **potrebbe** avere compromesso nostri dati, o anche no, o solo dati privi di importanza. La vulnerabilità è attiva da **due anni** e quindi sulla bilancia devono anche stare le due idee contrapposte che molto probabilmente, se fosse accaduto qualcosa di grave ai nostri dati, ce ne saremmo accorti da un pezzo; dall’altra parte, i cattivi meno astuti sono freneticamente al lavoro per sfruttare la falla che non conoscevano, per portarsi via ancora qualche bue prima che tutte le stalle si chiudano.

Si noti che tutti parlano liberamente di OpenSsl ma tecnicamente il problema coinvolge [circa il 17,5 percento dei siti che usano Ssl](http://news.netcraft.com/archives/2014/04/08/half-a-million-widely-trusted-websites-vulnerable-to-heartbleed-bug.html), per circa mezzo milione di siti. Molti più siti sono a posto di quelli che sono vulnerabili. Il problema chiaramente è sapere quali.

Tutti fanno un gran chiasso perché si aggiornino le *password*; prima di farlo, **controllare la vulnerabilità**: se un sistema non è stato aggiornato ed è ancora vulnerabile, cambiare la *password* serve assolutamente a nulla.

La mia diagnosi personale: nella maggior parte dei casi non succede nulla. Questo però non esclude il caso singolo. La situazione è simile al camminare in una strada affollata e piena di borseggiatori. Le probabilità di tornare a casa indenni sono alte; qualcuno, però, si troverà comunque senza portafogli o con la borsetta aperta.

Potrebbe andare peggio? Certo, potrebbe piovere. Ha già detto tutto [xkcd](http://xkcd.com/1353/).

 ![Xkcd su Heartbleed](/images/heartbleed.png  "Xkcd su Heartbleed")