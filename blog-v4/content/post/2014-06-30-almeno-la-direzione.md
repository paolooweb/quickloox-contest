---
title: "Almeno la direzione"
date: 2014-06-30
comments: true
tags: [Mappe, Reddit, heyyoudvd]
---
Dopo che Apple ha deciso di rendersi indipendente dalle mappe di Google, la partenza non è stata fantastica e Tim Cook, l’amministratore delegato, se ne è [scusato pubblicamente](https://www.apple.com/it/letter-from-tim-cook-on-maps/).<!--more-->

È passato del tempo. L’altra settimana tale **heyyoudvd** [segnalava su Reddit](http://www.reddit.com/r/apple/comments/28nkez/apple_maps_is_finally_improving_poi_data_and/) che, dopo mesi di aggiornamenti deboli e sparsi, le Mappe di Apple hanno cominciato ad aggiornarsi ogni venerdì, con tanto di segnalazioni riportate dagli utilizzatori.

>Mappe sta FINALMENTE migliorando. Ho visto più migliorie ai punti di interesse nella mia zona nell’ultimo mese che nei due anni scorsi TUTTI INSIEME. C’è ancora molta strada da fare, ma dopo anni di frustrazione stiamo infine vedendo del progresso reale.

Una settimana dopo, **heyyoudvd** ha scritto ancora.

>Riassumendo, Apple ora aggiorna veramente i dati sui punti di interesse. Se vedete errori nel vostro quartiere, potete segnalarli con l’apposito pulsante nella schermata di informazioni di Mappe. I nuovi dati vengono trasmessi dai serve Apple *ogni singolo giorno* e Mappe sta migliorando a gran ritmo.

Non ho avuto modo di effettuare esperimenti. Se però è tutto vero come sembra, aggiornamento quotidiano vuol dire una bella organizzazione sottostante e impegno serio. I risultati dovremo giudicarli, ma la direzione presa da Mappe sembra buona.