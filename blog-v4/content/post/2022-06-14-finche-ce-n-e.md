---
title: "Finché ce n’è"
date: 2022-06-14T00:42:03+01:00
draft: false
toc: false
comments: true
categories: [Internet]
tags: [Apple, S&P 500, Nasdaq]
---
Se io facessi *trading*, con [questi chiari di luna](https://macdailynews.com/2022/06/13/apple-microsoft-amazon-other-tech-stocks-tumble-into-the-red/) mi fregherei le mani e inizierei a comprare azioni Apple [finché ce n’è](https://www.youtube.com/watch?v=sURek0ZaupE).

<iframe width="560" height="315" src="https://www.youtube.com/embed/sURek0ZaupE" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>