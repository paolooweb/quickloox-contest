---
title: "Incolla, ultima frontiera"
date: 2016-03-23
comments: true
tags: [LibreOffice, Odt, iOS]
---
Sostengo strenuamente che iPad può sostituire un Mac nella gran parte delle attività computazionali quotidiane, fatto salvo l’ovvio, tipo l’indisponibilità di InDesign o cose così.<!--more-->

C’è però un ostacolo che allo stato attuale delle cose trovo insuperabile.

Ricevo un testo in Open Document Format, contenente link.

Su Mac apro il file con LibreOffice, seleziono tutto, copio e incollo dentro Wordpress su Safari, Chrome, Firefox, con Wordpress in modalità di inserimento Visuale. Il testo arriva dentro Wordpress con i link attivi e funzionanti.

Su iPad avevo una vecchissima *app* di Ibm di nome Symphony. Era neanche l’embrione di una *app*, ma il copia e incolla funzionava.

Ieri ho passato una decina di *app* tra quelle gratis, quelle legate a servizi cloud, quelle a pagamento. Nessuna di quelle scaricate, anche pagando, copia il testo da un file Open Document Format e me lo fa incollare dentro Wordpress in modo da lasciare funzionanti i link.

Non parliamo, per carità di patria, dell’usabilità delle maniglie di selezione. Che non è obbligatorio rendere inusabili, anche fuori da Apple: Editorial, per esempio, ha una selezione che funziona meravigliosamente, persino meglio di Note e Mail.

Non ho ancora finito le *app* da comprare e sono convinto che alla fine troverò una soluzione. È lo stato generale di questi aggeggi software a rendermi perplesso, perché se non sanno fare copia e incolla, chissà il resto…
