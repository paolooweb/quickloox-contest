---
title: "Comandi"
date: 2018-09-20
comments: true
tags: [Siri, Shortcuts, Comandi]
---
C’è stato molto tempo per preoccuparsi dello stato di salute dell’automazione individuale nell’ecosistema di Apple, ma ora che c’è iOS 12 si può almeno essere sicuri che su iOS c’è vita, ottima e abbondante: come [si scriveva a giugno](https://macintelligence.org/posts/2018-06-27-automatismi-e-scorciatoie/), Apple ha completato in modo positivo e incoraggiante il processo di integrazione della app Workflow, comprata apposta.

Workflow è diventata Shortcuts — Comandi in italiano – e Apple ha pubblicato un’ottima [guida all’uso](https://support.apple.com/guide/shortcuts/welcome/ios), semplice e completa.

Di tutte le letture possibili su iOS 12, io mi concentrerei su questa. Spalanca la porta a un’esperienza di utilizzo superiore più di mille nuove funzioni a portata di dito.
