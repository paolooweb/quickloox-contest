---
title: "Chiudere un occhio"
date: 2019-10-30
comments: true
tags: [Pixel, iPhone, Face, ID]
---
Pensieri confusi sulla biometria e sulla sicurezza, dopo avere recuperato dal passato un articolo di Engadget sulla [sedicente facilità di superare Touch ID](https://www.engadget.com/2013/10/05/oh-look-another-easy-way-to-spoof-touch-id-on-the-iphone-5s/): fotografa l’impronta, ritocca la foto, stampala su plastica traslucida, ricavane un circuito, usa carbonato di potassio per incidere l’impronta, spruzzala di grafite, coprila di colla, togli l’eccesso di colla, poi ruba l’apparecchio e viola TouchID prima che il proprietario se ne accorga e prenda le contromisure.

Il pezzo prendeva nettamente le distanze dai siti che parlavano di semplicità e facilità rispetto a procedure come questa e in sostanza si facevano beffe del riconoscimento dell’impronta senza avere la minima idea della sua sicurezza effettiva.

Avanti veloce per sei anni ed eccoci a Face ID e alla sua [descrizione](https://support.apple.com/it-it/HT208108):

>Il Face ID rileva lo sguardo, riconoscendo se gli occhi sono aperti e se lo sguardo è rivolto verso il dispositivo. Questo riduce le probabilità che qualcun altro riesca a sbloccare il tuo dispositivo a tua insaputa (ad esempio, mentre dormi).

A confronto con il [Pixel 4](https://support.google.com/pixelphone/answer/9517039) di Google:

>Il tuo telefono può essere sbloccato da qualcun altro se viene puntato contro la tua faccia, *persino se i tuoi occhi sono chiusi*.

Il corsivo è mio, ma sembra esserlo anche la meraviglia di fronte all’essenziale indifferenza di chiunque davanti a una enormità del genere. Spendere milioni per predisporre un riconoscimento facciale che si arrende a qualcuno che approfitta di un pisolino del proprietario.

Sono basito, ma non per il Pixel 4. Per essere uno dei pochissimi a essere basito.