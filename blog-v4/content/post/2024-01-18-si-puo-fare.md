---
title: "Si-può-fare!"
date: 2024-01-18T01:04:37+01:00
draft: false
toc: false
comments: true
categories: [Hardware]
tags: [iPad, Viticci, Fderico Viticci, MacStories, Hackett, Steve Hackett, 512 Pixels]
---
Sappiamo per esperienza che nessuno al mondo riesce a ottenere da un iPad quello che riesce a Federico Viticci. Stavolta Stephen Hackett di *512 Pixels* lo ha linkato titolando, tra l’ironico e l’ammirato, [Adesso è andato troppo oltre](https://512pixels.net/2024/01/hes-gone-too-far-now/).

Il titolo dell’articolo di Federico è [Come ho truccato il mio iPad Pro con un proteggischermo, un portaiPhone e altoparlanti stereo magnetici](https://www.macstories.net/ipad/how-i-modded-my-ipad-pro-with-a-screen-protector-iphone-holder-and-magnetic-stereo-speakers/) e già si è detto tutto.

Lui è il primo a saperlo e lo scrive in chiaro:

>Lo so, lo so: è completamente e inqualificabilmente ridicolo.

Dietro, comunque, c’è una verità seria e confortante.

>La natura trasformativa di iPad lo rende il computer più flessibile creato da Apple. Dopotutto, questa è precisamente la ragione del perché mi sono innamorato di questo prodotto dieci anni fa e perché non vedo l’ora di scoprire che cosa ha in serbo per il futuro.

Il *modding* estremo presentato nell’articolo non è certo per tutti e anche su questo Federico è il primo a scriverlo; poi però precisa diversi dettagli, come il lavorare spesso in macchina o sul balcone di casa, o in ambienti che non solo il suo, e come alla fine la macchina-Frankenstein che ha creato risponda, e bene, a certe esigenze.

Una cosa in cui mi sono ritrovato in particolare è l’uso di iPhone come secondo schermo durante l’utilizzo di iPad, per spostare lì qualche attività secondaria e concentrarsi sul lavoro. Mi è capitato di farlo tante volte, naturalmente in modo del tutto improvvisato, con gli apparecchi sulla scrivania, senza sostegni magnetici e operazioni raffinate di *modding*.

È proprio così: Mac non si tocca, ci sono occasioni in cui è imprescindibile. iPad, però, è *flessibile* e può adattarsi a un sacco di situazioni e utilizzi non ortodossi. Per cui, confesso, sono innamorato allo stesso modo.

<iframe width="560" height="315" src="https://www.youtube.com/embed/1pHN4vQIkU0?si=vNi21uNbtGmT2Q4B" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>