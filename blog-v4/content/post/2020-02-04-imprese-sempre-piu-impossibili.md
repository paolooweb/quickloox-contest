---
title: "Imprese sempre più impossibili"
date: 2020-02-04
comments: true
tags: [Sap, Mac, Tv, iOS, Lang]
---
Avevamo già parlato di quella oscura e insignificante azienda di nome Sap che si ostina a [usare Mac contro ogni logica aziendale](http://www.macintelligence.org/blog/2019/11/15/imprese-impossibili/), del secolo scorso eh, ma pur sempre logica. 

C’è un aggiornamento, [a firma Martin Lang](https://twitter.com/MartinLang/status/1224368483208704002), figura di mezza tacca come può esserlo un Vice President Enterprise Mobility:

<blockquote class="twitter-tweet"><p lang="en" dir="ltr">The number of SAP colleagues who chose a Mac as their primary computer has doubled over the last 15 months, it&#39;s at almost 26K now with 82% of them running the latest macOS Catalina <a href="https://twitter.com/hashtag/lifeatsap?src=hash&amp;ref_src=twsrc%5Etfw">#lifeatsap</a> <a href="https://t.co/jzSH4LHEUd">pic.twitter.com/jzSH4LHEUd</a></p>&mdash; MartinLang (@MartinLang) <a href="https://twitter.com/MartinLang/status/1224368483208704002?ref_src=twsrc%5Etfw">February 3, 2020</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

>Il numero di colleghi in Sap che scelgono Mac come computer primario è raddoppiato negli ultimi quindici mesi, ad almeno ventiseimila persone, l’ottantadue percento delle quali lavora su macOS Catalina.

Dopo avere sentito le cose peggiori su Catalina per affidabilità, pare che in Sap lo usino in decine di migliaia. Giorno per giorno, con risultati da portare per guadagnarsi uno stipendio, con una reputazione ai vertici del loro mercato. Come possa l’azienda prosperare invece che fallire rovinosamente, nonostante questa serie di scelte improvvide, è tutto da capire.

O forse hanno messo da parte il pregiudizio per giudicare, invece, in base ai fatti.
