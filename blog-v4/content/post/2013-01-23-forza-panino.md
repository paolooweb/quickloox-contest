---
title: "Forza Panino"
date: 2013-01-23
comments: true
tags: [Munster, iSpazio, Apple, TV, Elio, Panino]
---
<b>Iruben</b> mi segnala la sedicente notizia su iSpazio per cui l’analista Gene Munster annuncia la Tv di Apple nel 2013.<!--more-->

Lo stesso iSpazio, informatissimo, scrive *magari potrebbe essere la volta buona. […] va inoltre ricordato che Munster è dal 2011 che predice la messa in commercio di una televisione di Apple.*

Non riesco a trattenermi dal commentare alla vista della foto (<a href="http://9to5mac.com/2013/01/22/apple-analyst-gene-munster-says-apple-hdtv-with-new-remote-coming-in-2013-again/">scopiazzata da 9to5Mac</a> come tutto il resto): un albero di Natale, a fine gennaio. Dadaismo puro, neanche l’<a href="http://www.robertoamadi.it/fontana.htm">orinatoio di Duchamp</a> è così dirompente.

I commenti sono quelli di Facebook, il quale si perita di indicarmi altri contenuti di livello analogo che mi potrebbero piacere:

<cite>Whatsapp diventerà a pagamento se non inoltrerai questo messaggio a 10 persone.</cite>

<cite>McDonald’s interrompe con effetto immediato la promozione per ricevere un panino GRATIS ogni giorno.</cite>

Qui casca l’asino: Elio e le Storie Tese <a href="https://www.youtube.com/watch?v=g6i3JTF8-tA">ci hanno insegnato</a> che, senza Panino, la festa è insoddisfacente.