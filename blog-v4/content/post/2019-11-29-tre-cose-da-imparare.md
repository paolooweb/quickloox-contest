---
title: "Tre cose da imparare"
date: 2019-11-29
comments: true
tags: [Word, Office, Excel, PowerPoint, wiki, Html, Markdown, LaTeX, EtherCalc, EtherPad, Copernicani, Lidia]
---
Dopo la scoperta che alla scuola primaria di mia figlia [le insegneranno a usare Word Excel e PowerPoint](https://macintelligence.org/posts/2019-11-24-un-programma-bacato/) mi sono ritrovato a scrivere sulla mailing list dei [Copernicani](https://copernicani.it) di tema attinente e ho scritto, come concetto, questo:

>Da una scuola primaria, più che Word Excel e PowerPoint, mi aspetto che insegnino linguaggi di marcatura, wiki e collaborazione.

Formattazione, organizzazione, collaborazione.

[Html](https://www.w3.org/html/), [LaTex](https://www.latex-project.org), [Markdown](https://daringfireball.net/projects/markdown/); wiki, appunto, [MediaWiki](https://www.mediawiki.org/wiki/MediaWiki) per dirne uno solo, ma sono millemila; [EtherCalc](https://ethercalc.org), [EtherPad](https://etherpad.org), per esempio.

Formati pubblici, universali, aperti; esigenze di tutti, fondamentali, a prova di qualsiasi futuro, a bordo di un’astronave come in una caverna.

È possibile che avidità e ignoranza siano così sufficienti a trascurare la formazione vera dei ragazzi? Lidia non sta imparando la Bic o la Montblanc; sta imparando a scrivere.

Certo, anche gli insegnanti dovrebbero imparare invece che ripetere. Certo, le scuole dovrebbero scegliere software libero invece che vivere l’illusione del regalo.

Non chiedo di condividere e diffondere questo messaggio, ci mancherebbe; l’idea, però, se appena c’è un minimo di sintonia, sì, grazie.