---
title: "Patti chiari, privacy lunga"
date: 2014-08-06
comments: true
tags: [Google, Stato, Gpg, Gmail, Quintarelli, profilazione]
---
**Stefano** mi ha chiesto di commentare la [notizia](http://www.telegraph.co.uk/technology/google/11010182/Why-Google-scans-your-emails-for-child-porn.html) di Google che ha consentito l’arresto di un pedofilo segnalando alle autorità la presenza nel suo account Gmail di foto *illegali* di una ragazzina.<!--more-->

In altre parole, Google scorre e conosce in varia misura il contenuto della posta che va su Gmail.

**Aggiornamento:** Microsoft [ha fatto la stessa cosa](http://www.bbc.com/news/technology-28682686) con il proprietario di un *account* OneDrive, che teneva immagini pedopornografiche sul disco *online* e ne ha inviate per posta attraverso i sistemi Microsoft.

Una [prima analisi in italiano](http://blog.quintarelli.it/2014/08/google-e-la-mail-spiata.html) è contenuta nel *blog* dell’Onorevole Stefano Quintarelli e non ripeterò cose già dette. Vorrei anche evitare le banalità monofrase, tipo *comunque sia, un pedofilo in meno* oppure *è ora di abbandonare Gmail*. Riguardano altri problemi da questo e, se dobbiamo accontentarci della monofrase, potrebbe anche essere *Google non vuole essere complice di traffico di foto pedopornografiche*. Chi non condividerebbe?

La questione ridotta all’osso: Gmail è un servizio gratuito. Sappiamo che *gratuito* è una convenzione linguistica e altrimenti non esiste; se ti offrono qualcosa, vogliono qualcosa in cambio.

Google offre Gmail e tanto altro in cambio di profilazione: conoscenza di abitudini d’acquisto, siti consultati e quant’altro. Le informazioni accumulate vengono utilizzate per generare pubblicità il più possibile su misura e quindi redditizia.

Fuori da ogni connotazione positiva o negativa, per Google la nostra attività *online* è merce di scambio da piazzare presso gli inserzionisti. Facebook fa lo stesso con il *mi piace* e tutto il resto.

L’accordo è semplice da capire e non c’è obbligo di firma. Si può stare senza Google e senza Facebook. Il problema è che Internet fa realmente parte indispensabile della vita moderna, come l’acqua corrente. Non ci fosse Google, altri farebbero lo stesso al posto suo.

La vera soluzione individuale è sapere che niente di quello che viaggia su Internet va considerato confidenziale. Niente; non conta Google, non conta questo o quel servizio. Se va su Internet, aspettati che qualcuno o qualcosa lo veda. Più qualcosa che qualcuno, perché a leggere la posta di Gmail sono algoritmi, mica persone.

Purtroppo è una soluzione di responsabilità personale, che fa dipendere tutto da noi. Vuoi passare una foto che nessuno possa vedere? Impara a cifrare, cifra e cifra robusto. La [tecnologia](https://gpgtools.org) esiste, sicura, collaudata, gratuita. In cambio – niente è gratis – viene chiesto un minimo di apprendimento, del tempo passato a installare, destinatari disposti a condividere la soluzione.

Ci sono altre soluzioni, sempre individuali: per esempio moltissimi pagano piccoli canoni mensili per avere un proprio spazio Internet e anche un proprio *server* di posta elettronica, che Google non può leggere e il *provider* non legge, perché in cambio gli diamo denaro.

Il problema della profilazione non si risolve mobilitandosi contro Google. Chiunque entri in libreria, discoteca, circolo, supermercato, cinema, biblioteca, stazione ferroviaria, bar, centro benessere, museo, ristorante riceve l’offerta di una tessera fedeltà, che offre agevolazioni e in cambio chiede dati: profilazione, come accade da quando esiste la pubblicità.

Ricordiamo che esiste un altro tipo di profilazione, a titolo personale più preoccupante di quella di Google: quella statale. Ho appena ricevuto la mia tessera sanitaria elettronica, inviata dall’Agenzia delle entrate. Avrei avuto la stessa reazione perplessa nel ricevere una cartella esattoriale dal mio medico.

Lo Stato vuole sapere assolutamente tutto di tutti, dispone di anagrafi immense e riceve costantemente informazioni amministrate in modo non sempre trasparente. È notorio che lo Stato utilizzi in modo pesantissimo le intercettazioni telefoniche, all’insaputa degli intercettati e non è detto che si tratti solo di uomini politici o capitani di industria. La tecnologia dei censimenti è preistorica ma capillare, che si abbia Internet o meno. Se c’è bisogno di sapere di più su qualcuno, in ogni agglomerato sta una caserma dei Carabinieri, forti di *dossier* confidenzialissimi ben più estesi di quanto si immagini.

Tutto per il mio bene, dicono. Così posso avere i servizi essenziali, una sanità su misura, la lotta al crimine, le strade pulite eccetera.

Con una differenza: posso spegnere tutto e sparire dagli occhi di Google ora. Con lo Stato è impossibile. Google ha un interesse naturale a non abusare dei dati che raccoglie, perché vuole vendere merce di prima qualità. Lo Stato è a molti livelli una macchina opaca e le storie di corruzione, malagestione, affarismo sono all’ordine del giorno. Che interesse ha lo Stato a non abusare dei miei dati? Nessuno e ci si può solo fidare. Il bello è che Google ci tratta come merce per ricavare denaro dagli inserzionisti. Mentre lo Stato chiede denaro direttamente. E se nascondo informazioni allo Stato con la cifratura, sono guai.

Google chiede informazioni in cambio di servizi. Lo Stato chiede informazioni e denaro in cambio di servizi.

Se temo Google, posso rivolgermi allo Stato. Se temo lo Stato, non posso rivolgermi ad alcuno.

Mi preoccupa più lo Stato di Google.