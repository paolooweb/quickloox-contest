---
title: "L’influenza asiatica"
date: 2018-01-28
comments: true
tags: [Yagge, Google, Uber, Grab]
---
Ringrazio da ora [Misterakko](https://www.accomazzi.net) per avermi messo sulle tracce di un discorso interessante e complicato.

Il titolo era troppo bello per rinunciarvi; il tema vero, tuttavia, è l’innovazione e tutto parte da un [Articolo di Steve Yagge](https://medium.com/@steve.yegge/why-i-left-google-to-join-grab-86dfffc0be84), per tredici anni ingegnere presso Google, che ha deciso di andarsene per sposare la causa di una *startup* del Sudest asiatico, [Grab](http://grab.com), a farla semplice una copia di Uber.<!--more-->

Mentre da noi il tema del trasporto a richiesta suscita al massimo la solidarietà (o meno) con i tassisti che protestano, nel Sudest asiatico la *app* del servizio è un formidabile grimaldello sociale che provoca una trasformazione radicale, dato che veicola affidabilità delle transazioni e degli accordi in luoghi molto caotici, molto affollati e con pochissime regole. In altre parole, chi vincerà la sfida del trasporto a richiesta nel Sudest asiatico avrà influenza decisiva su passaggi di denaro e profili di fiducia relativi a un paio di miliardi di persone.

Yagge, comunque, ha scelto Grab a partire da una sua motivazione: l’idea che Google abbia smesso di innovare. Come dice lui, che gli ultimi prodotti siano sostanzialmente imitazioni della concorrenza, vuoi Facebook, vuoi Amazon, vuoi altri e, che in Occidente, di fatto l’innovazione si sia fermata.

Vero? Falso? Interessante. Bisogna intendersi su che cosa sia innovazione e si ritornerà sull’argomento, perché Yagge ha avuto almeno una risposta significativa. Per il momento è abbastanza rifletterci sopra.
