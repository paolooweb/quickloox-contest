---
title: "Il digitale alla rovescia"
date: 2022-10-18T03:25:12+01:00
draft: false
toc: false
comments: true
categories: [Web]
tags: [Covershop]
---
Ordini una serigrafia nera ruotata di novanta gradi e te ne danno una bianca girata di duecentosettanta.

I rischi di Internet non sono il porno o le fake news, ma la sciatteria di chi prende il digitale come una seconda scelta dove vale tutto e conta solo esserci, non importa come.