---
title: "Né pico né nano"
date: 2023-08-28T03:11:49+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [pico, nano, mg, emacs]
---
Sono una cattiva persona in quanto, pur avendo [emacs](https://www.gnu.org/software/emacs/) a disposizione, cerco un minieditor *quick and dirty* per quelle numerose circostanze in cui la necessità è correggere un refuso o aggiungere una riga a un file di configurazione più corto di questo capoverso.

Unix mi offre da sempre [pico](https://ts.vcu.edu/askit/research-math-science/unix-for-researchers-at-vcu/unix-text-editors/the-pico-editor/) e [nano](https://www.nano-editor.org), che però sono poco pratici nel caso di righe più lunghe della schermo e non supportano pienamente Utf-8, dalle accentate in avanti.

Così mi si è acceso un barlume di speranza nel leggere di[mg](https://mjtsai.com/blog/2023/08/28/mg-text-editor/): un sottoinsieme di emacs che, per di più, viene ancora installato con macOS a differenza di molti altri pezzi di software libero.

Purtroppo, neanche mg mostra pulitamente le accentate.

Mi conviene forse ammettere la colpa, fustigarmi con vecchi cavi Rs-232 e imparare il manuale di emacs a memoria una volta per tutte.