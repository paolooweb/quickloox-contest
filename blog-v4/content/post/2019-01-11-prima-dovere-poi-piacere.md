---
title: "Prima dovere poi piacere"
date: 2019-01-11
comments: true
tags: [Spider-Mac, iPhone, Cina]
---
Spider-Mac [ha replicato](https://spider-mac.com/2019/01/09/moof-apprendo-di-essere-un-deficiente/) al mio [commento](https://macintelligence.org/posts/2019-01-05-anno-nuovo-deficienti-vecchi/) del suo [editoriale](https://spider-mac.com/2019/01/03/la-tela-del-ragno-una-riflessione-sulle-cause-del-calo-degli-iphone-che-dovrebbe-fare-tim-cook/) sul ribasso delle previsioni di fatturato di Apple.

Sarebbe facile controreplicare, perché la risposta è piena di parecchi spunti gustosi, ma appunto; troppo facile.

Invece ne ho approfittato per leggere un po’ di commenti.

Uno specchio fedele della Rete di oggi, polarizzato, basato sulle simpatie e sul partito preso, dove la razionalità è un *optional* e prevale la reazione istintiva. Con eccezioni, naturalmente.

Avevo accennato alla presenza di pubblicità sul sito ed è sintomatica l’obiezione: un’elencazione del numero di banner presenti nella home o nella pagina dell’articolo, con l’invito a riflettere se siano pochi o tanti.

Il criterio per decidere evidentemente è personale: se *due* suona poco, allora è poco; se suona molto, allora è molto. La mancanza di un criterio terzo di misurazione sul quale confrontarsi è un altro segno dei tempi e suona simile al criterio di giudizio per cui un iPhone a milleduecento euro sembra molto per nessun altro motivo che *milleduecento* pare alto. Quindi è alto.

Da premesse così può arrivare solo una conclusione anch’essa in linea con i tempi: pensare che ci siano soluzioni semplici per situazioni complicate. Vendi meno iPhone? Abbassa i prezzi. Viene da pensare al tempo perso dal consiglio di amministrazione Apple, composto da gente che guadagna milioni di dollari probabilmente anche con qualche merito, quando sarebbe bastato così poco.

Una *readership* di pancia, la pubblicità, il ragionamento spiccio spacciato per [rasoio di Occam](https://www.ideegreen.it/rasoio-di-occam-110018.html): tutto congiura per portare Spider-Mac nel novero dei siti tipici di oggi. Quelli che scrivono per lettori che si aspettano di essere blanditi da opinioni in linea con il loro pensiero.

Spider-Mac ha il dovere di piacere, altrimenti i suoi pochi o pochissimi banner perderanno appiglio e i suoi molti o moltissimi lettori si sentiranno insoddisfatti.

Se questo sembra il prerequisito per svolgere un lavoro di qualità, beh, buon lavoro.

Per alcuni lettori di Spider-Mac il sottoscritto è un pezzo da museo, un relitto del passato *fermo a Mac OS 9*, sostanzialmente anacronistico e irrilevante. Sono sempre tranquillo quando vedo riferimenti alla persona – significa che mancano argomenti concreti – ma, in questo caso, pure contento di avere un’età che mi ha consentito di [leggere Orwell](http://orwell.ru/library/novels/Animal_Farm/english/efp_go):

>If liberty means anything at all it means the right to tell people what they do not want to hear.

Nel mio blogghino, sono libero. Auguro a Spider-Mac e ai suoi lettori lo stesso piacere senza doveri.