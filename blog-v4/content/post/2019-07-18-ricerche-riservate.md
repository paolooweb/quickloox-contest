---
title: "Ricerche riservate"
date: 2019-07-18
comments: true
tags: [DuckDuckGo, Safari, Apple, mappe, privacy, Google, Microsoft]
---
Per cambiare motore di ricerca preimpostato in Safari su iOS si va alla voce Safari nelle Impostazioni. Su Mac, nelle Prefernze di Safari. Vale sempre più la pena di togliere Google e mettere [DuckDuckGo](https://duckduckgo.com/).

DuckDuckGo migliora di giorno in giorno, [lavora con l’infrastruttura delle mappe di Apple](https://macintelligence.org/posts/2019-01-16-nessuno-lo-sapra/) e ha appena pubblicato un resoconto di come l’integrazione delle mappe in questione nel motore di ricerca [stia diventando sempre più raffinata](https://spreadprivacy.com/duckduckgo-apple-mapkit-js-update/).

Il punto è la privacy. A differenza delle altre grandi multinazionali tecnologiche, Apple non ha interesse a sorvegliare il nostro comportamento. E non lo fa, se non dove è strettamente indispensabile allo scopo di fornire un buon servizio.

DuckDuckGo si muove sulla stessa strada e quindi è una scelta ideale se il tema della riservatezza è sentito. Se andiamo al ristorante argentino e usiamo le ricerche e le mappe di Google o (in misura minore) Microsoft, tutti gli inserzionisti interessati a venderci qualcosa lo verranno a sapere; se usiamo DuckDuckGo e le mappe di Apple, no.

La differenza tecnica sta nel fatto che tutti i servizi faranno l’impossibile per capire dove siamo, dove sta il ristorante, quali alternative esistono eccetera; però DuckDuckGo dimenticherà tutto appena fornito il servizio e Apple userà algoritmi sofisticati per conservare meno informazioni possibile, sganciate dalla nostra persona. Senza vendere i dati a chicchessia.

La privacy è la nuova ricchezza, o lo sarà presto. Una settimana di prova a DuckDuckGo come motore di ricerca preimpostato, io proverei a darla.
