---
title: "Uno splendido fallire"
date: 2013-09-30
comments: true
tags: [Apple]
---
Da memorizzare il *post* di John Kirk su *Tech.pinions* a proposito dello [splendido fallire di Apple](http://techpinions.com/apple-the-splendid-failure/23361), perché cose così si leggono raramente.<!--more-->

In modo semplice e completo, coadiuvato da una serie imbarazzante di citazioni a tutti i livelli di profondità, Kirk spiega come funziona Apple, perché ha successo, che cosa sia il [dilemma dell’innovatore](http://www.amazon.com/The-Innovators-Dilemma-Revolutionary-Business/dp/0062060244) e chi ne sia afflitto più di altri.

Come faccio a riassumerlo in poche parole? Per evitare di fare un sacco di soldi ma rimanere indietro e venire sorpassati da cose nuove – viene in mente qualche grande azienda informatica? – occorre destinare una parte delle proprie capacità e risorse a prodotti che non ci sono ancora, mercati che non ci sono ancora, acquirenti che non ci sono ancora. Se tutte le proprie energie si rivolgono ai clienti che ci sono già, si perde di vista il futuro, ovvero il presente di domani.

I clienti che capiscono questo si innamorano di un’azienda che riesce ad accompagnarli nel futuro e chi lavora per l’azienda è motivato quasi più da ciò per cui lavora che dal mero stipendio.

Kirk cita William Faulkner. Abbiamo fallito tutti rispetto al nostro sogno di perfezione, ma ci distinguiamo per lo splendore di ciascun fallimento. Apple a oggi è un fallimento di splendore abbacinante.