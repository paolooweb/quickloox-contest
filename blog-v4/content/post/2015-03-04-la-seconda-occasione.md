---
title: "La seconda occasione"
date: 2015-03-04
comments: true
tags: [Viticci, iPhone]
---
Ogni tanto si legge che su Internet la comunicazione deve essere breve e in effetti pare che l’attenzione dopo il tempo di un *Mi piace* si azzeri o quasi.<!--more-->

Balle. L’articolo di Federico Viticci su *MacStories*, [Vita dopo il cancro: come iPhone mi ha aiutato ad adottare uno stile di vita più salutare](http://www.macstories.net/stories/life-after-cancer-how-the-iphone-helped-me-achieve-a-healthier-lifestyle/) è di lunghezza inusitata.

L’ho letto tutto. Il bello del digitale dovrebbe proprio essere che una pagina può avere potenzialmente nessun limite imposto. A naso ci vogliono tre quarti d’ora, forse un’ora, non ho tenuto il conteggio. È in inglese, perché [@Viticci](http://twitter.com/viticci) ha scelto da tempo di rivolgersi a un pubblico più globale.

Chi non lo legge perde qualcosa. Federico racconta di come si è guadagnato la sua seconda possibilità e di come se la voglia giocare al meglio. Senza smancerie, con umorismo, diritto al punto, documentato quasi all’ossessione. Forse la migliore inchiesta su *health and fitness* declinato in salsa iPhone, se lasciamo per un momento da parte le cose più importanti.

L’articolo non ha bisogno di giudizi. È una storia di vita, una gran bella storia di vita. In secondo piano ci sarebbero anche dei contenuti che riguardano Apple e su questi ha messo il sigillo qualcuno più importante di me.

<blockquote class="twitter-tweet" lang="en"><p>Federico Vittici&#39;s healthier life &#10;<a href="https://twitter.com/viticci">@viticci</a> <a href="https://twitter.com/hashtag/iphone?src=hash">#iphone</a>&#10;<a href="http://t.co/2LjNHM7CiG">http://t.co/2LjNHM7CiG</a></p>&mdash; Philip Schiller (@pschiller) <a href="https://twitter.com/pschiller/status/572864857877299202">March 3, 2015</a></blockquote> <script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>
