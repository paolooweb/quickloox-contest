---
title: "Più del numero"
date: 2022-03-07T01:49:13+01:00
draft: false
toc: false
comments: true
categories: [Internet, Software]
tags: [Ucraina, Daxx, Germania, Stati Uniti, Regno Unito, Olanda, Java, JavaScript, Php, Ruby on Rails, Cnbc, Fortune 500]
---
Secondo Daxx, una società olandese specializzata in outsourcing del software, [in Ucraina si trovano](https://www.daxx.com/blog/outsourcing-ukraine/how-many-developers-in-ukraine) duecentomila sviluppatori, più di quattromila aziende di tecnologia, oltre centodieci centri di ricerca e sviluppo e un migliaio abbondante di eventi dedicati a questi temi.

Nonostante la popolazione ucraina sia nettamente inferiore a quelle di Stati Uniti, Regno Unito o Germania, ci sono per esempio più sviluppatori Php che in ciascuno di questi Paesi. Nello sviluppo Java l’Ucraina supera ancora una volta la Germania, con Ruby on Rails l’Ucraina è seconda solo agli Stati Uniti.

Situazioni simili valgono anche per Python o JavaScript, i linguaggi che veramente vanno per la maggiore. Un quinto delle aziende Fortune 500 ha team di sviluppo in Ucraina.

Quello che Daxx non rileva è il tipo di personalità che hanno gli sviluppatori ucraini; gente che [continua a lavorare anche sotto le bombe russe](https://www.cnbc.com/2022/03/04/ukrainians-are-built-different-the-coders-still-working-under-russian-bombing.html).

Non mi dilungo in citazioni di questo articolo di Cnbc, che invito a leggere per intero; se serve, da qui arriva direttamente la [traduzione in italiano](https://translate.google.com/website?sl=en&hl=it&client=webapp&u=https://www.cnbc.com/2022/03/04/ukrainians-are-built-different-the-coders-still-working-under-russian-bombing.html&tl=it).

Perché i numeri contano, ma alcune cose contano ancora di più e speriamo continuino a contare.