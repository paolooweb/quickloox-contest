---
title: "Il senno di pochi"
date: 2014-02-16
comments: true
tags: [Microsoft, Office, iPad, Surface, Nokia, Thompson, stratēchery]
---
Non c’è da essere orgogliosi di avere visto quello che è evidente. Due anni fa [scrivevo](https://macintelligence.org/posts/2012-02-22-come-fai-sbagli/) che Microsoft era chiusa in un angolo con la faccenda di Office per iPad, perché come si muove manda in fumo la strategia che l’ha nutrita finora.<!--more-->

Ora Ben Thompson, alla fine di una analisi ben più articolata e profonda della mia, [conclude su stratēchery](http://stratechery.com/2014/microsofts-mobile-muddle/) che Microsoft dovrebbe abbandonare i *device* e darsi al *service*: chiudere con i Surface e i telefoni comprati a Nokia, per impegnarsi a essere presente con i propri prodotti ovunque, da iPad a Android a quello che vuole. Accettando di lavorare in onesta e virile concorrenza con altri sistemi operativi e altro software.

Lo stesso Thompson ricorda Steve Jobs appena tornato in azienda, a dire pubblicamente come ci fosse da chiudere con l’idea che Apple potesse vincere solo al prezzo della sconfitta di Microsoft. Il punto era fare grandi prodotti e collaborare dove opportuno, o necessario. Ci toccò Explorer su Mac, ma fu l’inizio della rinascita di Apple.

Così Microsoft, anche se sembra incredibile ora e anche se ci vorranno anni, se non si decide a supportare iOS in modo valido – per non parlare di Android – perderà terreno.

Le parti del 1997, diciassette anni dopo, si sono capovolte e la cosa più incredibile di tutte è il numero tremendamente scarso di persone che coglie la vastità e l’importanza della cosa.