---
title: "Purché di nascosto"
date: 2023-02-23T01:03:12+01:00
draft: false
toc: false
comments: true
categories: [Hardware, Software]
tags: [Newton, Sakoman, Capps, Gassée, Sculley, John Sculley, Jean-Louis Gassée, Steve Sakoman, Steve Capps, Low End Mac]
---
Con la fame che c’è di storie degne di ascolto presso un grande pubblico, mi stupisco di come nessuno – credo – abbia pagato Low End Mac per i diritti cinematografici della sua [storia di Newton](https://lowendmac.com/2013/the-story-behind-apples-newton/).

C’è di tutto: la lotta di potere, l’innovazione, la suspence, la visione di pionieri, la delusione che viene dall’alto, i cambi di rotta, le rivelazioni, i misteri (perché il software di riconoscimento della scrittura a mano venne consegnato a Mosca di nascosto?) e mille altre cose.

Il racconto è veramente avvincente e mostra anche come un progetto hardware di una certa ambizione porti con sé una complessità che non riusciamo neanche a immaginare, quando trattiamo le scelte di progetto come qualcosa che alla fine, bastava volerlo e si faceva diversamente. È un’altra cosa, invece.

Un pezzo interessante tra tanta eccezionalità (il progetto mirava a creare qualcosa di radicalmente diverso da tutto quanto esisteva) è tuttavia quello che si ritrova come costante nella storia di Apple:

>Per tenere in Apple il talentuoso Steve Sakoman, Jean-Louis Gassée propose di creare un progetto segreto di computer palmare. Gassée ricevette da John Sculley il permesso di avviare il progetto ma non gli disse di che cosa si trattasse.

>La prima cosa che fece Sakoman fu trovare un nome al progetto. Visto che il logo originale di Apple mostrava Isaac Newton seduto sotto un albero di mele, decise per il nome Newton.

>Sakoman radunò un team di sviluppatori, compreso il coautore del Finder, Steve Capps, e si installò in un magazzino abbandonato su Bubb Road a Cupertino.

Il progetto Macintosh, quasi dieci anni prima, era decollato verso la sua forma definitiva da un edificio Apple deserto e requisito da Steve Jobs, per trasformarlo in una roccaforte dove nessuno poteva entrare.

Ron Avitzur [realizzò la sua Calcolatrice Grafica](https://macintelligence.org/posts/2020-09-03-talento-non-calcolato/) senza avere il diritto di lavorare negli uffici di Apple, senza un incarico ufficiale, senza un budget approvato, senza un capo.

Molte cose di successo in Apple contengono un elemento di ribellione all’autorità e alla gerarchia, al punto da richiedere segretezza nei confronti dei vertici stessi della società. Un paradosso che non si ritrova facilmente in altre aziende. E la differenza si avverte.