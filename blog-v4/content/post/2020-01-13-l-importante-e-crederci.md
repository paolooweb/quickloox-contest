---
title: "L’importante è crederci"
date: 2020-01-13
comments: true
tags: [iPhone, Daughter]
---
Reduce dalla visione di [Daughter](https://youtu.be/bvtwWhKdxhM), il corto di Apple prodotto in occasione del Capodanno cinese, ho voluto guardare il [making of](https://youtu.be/dj6Lw7jnqBI). A parte la storia, strappalacrime con formula tipicamente orientale ma capace di coinvolgere anche da questo lato della Muraglia, la cosa che mi interessava maggiormente era come fosse stato svolto il compito di magnificare [iPhone 11 Pro](https://www.apple.com/it/iphone-11-pro/), con cui è stato girato *Daughter*.

<iframe width="560" height="315" src="https://www.youtube.com/embed/bvtwWhKdxhM" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Di film fatti con iPhone se ne conoscono a tonnellate e da questo punto di vista non c’è niente da scoprire. Il modo in cui parlarne, invece, può essere interessante.

Di solito i *making of* di questo tipo puntano sulla tecnica, mostrano l’apparecchio in mezzo ad accessori sofisticati, in qualche modo giustificano la presenza dell’apparecchio stesso in mezzo a equipaggiamento professionale.

Stavolta prevale la semplicità. In più occasioni il filmato del cameraman che filma lo mostra aggirarsi sul set con iPhone nelle due mani, camminando. Niente contrappesi, niente armature speciali. Le riprese attraverso il cannocchiale di fantasia della bimba sono realizzate, lato equipaggiamento, in modo quasi amatoriale.

Le frasi di commento sono quelle tipiche, anche se manca enfasi sul lato prestazionale o sulla qualità del girato. Evidentemente si vogliono dare oramai per scontate.

A me hanno colpito proprio quelle sequenze, il cameraman con iPhone in mano che potrebbe essere chiunque di noi, a concentrarsi per tenere stabile la ripresa, attento a non urtare gli oggetti, mezzo curvo per avere l’angolo di visione desiderato (e, bene che si veda in pubblico, con iPhone in orizzontale). La differenza è che il cameraman lavora per una produzione Apple e non è poco.

Quell’iPhone sarebbe nelle mie mani il trionfo della buona volontà. In quelle della troupe, appare dannatamente *credibile*. Non ci si domanda più se sia possibile usare iPhone con risultati professionali, al massimo si indaga sulle modalità in cui accade.

Tutto appare semplice, tranquillo, senza toni trionfalistici. Non deve ingannare. Si vedono pochi fotogrammi di pioggia che cade su iPhone 11 Pro e quelli, ci scommetto, li hanno ripresi novantotto volte per scegliere il meglio e poi farci sopra una sontuosa postproduzione.

Il *making of*, nonostante le apparenze, forse è persino più curato di *Daughter*.

<iframe width="560" height="315" src="https://www.youtube.com/embed/dj6Lw7jnqBI" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
