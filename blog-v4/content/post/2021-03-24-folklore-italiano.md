---
title: "Folklore, italiano"
date: 2021-03-24T23:45:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [Nicola D’Agostino, Mac OS X, Andy Hertzfeld, Storie di Apple, Patreon, Scott Forstall, Cheetah] 
---
Il lungo filo rosso che connette tutta la storia di Apple nel XXI secolo si chiama Mac OS X, [nato ufficialmente proprio oggi](https://twitter.com/forstall/status/1374838580971892736) nel 2001.

<blockquote class="twitter-tweet"><p lang="en" dir="ltr">Happy 20th Birthday Mac OS X! I still remember when we named you. In a small room in IL1. When Steve slashed a large X on the wall and smiled. Look at how far you’ve come from a young Cheetah.</p>&mdash; Scott Forstall (@forstall) <a href="https://twitter.com/forstall/status/1374838580971892736?ref_src=twsrc%5Etfw">March 24, 2021</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

Innestare in modo soddisfacente un’interfaccia utente superiore su Unix è stata un’impresa non secondaria, che nessuno ha saputo compiere altrettanto bene e da cui sono germinati i sistemi operativi per iPhone, iPad, watch, tv e domani chissà che altro. Per seguire l’evoluzione di Mac OS X, John Siracusa ha di fatto scritto un libro fatto di [recensioni straordinariamente approfondite di ciascuna versione del sistema](https://hypercritical.co/2015/04/15/os-x-reviewed#links).

Mac OS X stabilisce già dal nome una continuità con il passato dell’ultimo quinto del XX secolo, l’epopea del primo Macintosh. Il posto migliore per leggerne è di gran lunga [Folklore.org](https://www.folklore.org/), di Andy Hertzfeld, uno che l’ha vissuta sulla pelle e ha di fatto contribuito a sagomare una parte non banale della storia umana degli ultimi quarant’anni.

Sento la mancanza di una fonte analoga per questi anni. Forse è troppo presto, ma certo non sarebbe fuori luogo anche se oggi i pirati sono stati sostituiti dai capitani d’industria.

Nel frattempo ci si può consolare con le [Storie di Apple di Nicola D’Agostino](http://www.storiediapple.it), che da anni si dedica con passione al racconto di tutti i dietro le quinte altrimenti difficili persino da sospettare.

Il suo lavoro è intenso, preparato, partecipato ed è solo giusto che gli articoli più vecchi siano aperti a tutti, ma quelli nuovi si raggiungano [via Patreon](https://www.patreon.com/storiesofapple). Si può dare linfa ed energia vitale a storie di Apple anche per un singolo euro al mese.

Mica per niente, l’articolo più recente di Nicola è dedicato a [Cheetah](https://www.patreon.com/posts/49104092). E si chiude un cerchio fatto di grandi storie, chi le ha generate, chi le sa raccontare in modo attuale e coinvolgente.

*Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile.*