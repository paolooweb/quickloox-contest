---
title: "La riparabilità ha rotto"
date: 2021-11-21T01:13:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [Apple, John Gruber, Daring Fireball, Self Service Repair, iFixit] 
---
Sui fatti come stanno, bastano tre righe [di John Gruber](https://daringfireball.net/2021/11/self_service_repair_follow-up#update-irpp):

> Il [Self Service Repair Program](https://www.apple.com/newsroom/2021/11/apple-announces-self-service-repair/) è esattamente quello che è stato annunciato: un programma per persone realmente intenzionate a riparare i propri apparecchi, pertanto irrilevante salvo che per una piccola fetta dell’utenza.

Molto semplice. Eppure si legge una insistenza ideologica a vederci dietro congiure, rivoluzioni, cambiamenti radicali che fa impressione da quanto riesca a sfidare la logica e il buonsenso.

Esisterebbe per cominciare un diritto alla riparazione, completamente sganciato – come è d’altronde di moda – dal dovere di sapere come fare, teorico e pratico. Negli anni ottanta qualsiasi smanettone si poteva sentire in dovere di aprire il proprio modem, nel caso si fosse desiderato aggiustare o modificare qualcosa. Poteva anche riuscirci. Oggi, quello stesso modem è un chip grande come un’unghia, con tracce che solo un ottimo microscopio può mostrare. È stato tolto un diritto alla riparazione oppure il progresso ci ha permesso di mettere in mano un modem a quattro miliardi di persone?

Ho visto mio papà mettere mano a un televisore per cambiare una valvola termoionica. Le valvole termoioniche sono state rimpiazzate dai transistor. Dentro un M1 Max ci sono cinquantasette miliardi di transistor. Esiste un diritto alla loro riparazione individuale, come quello presunto di mio papà di cambiare una per una le valvole termoioniche del televisore (o del computer, se avesse fatto il tecnico informatico)?

Non esiste alcun diritto alla riparazione. Le condizioni del mercato e della tecnologia rendono invece più o meno consigliabile la riparazione rispetto alla sostituzione. Queste condizioni cambiano e con esse gli oggetti che è globalmente conveniente riparare.

Per qualche rivoluzionario da stanzetta, Apple è stata messa alle corde dalle forze del Bene e costretta a rinunciare a una parte dei suoi profitti miliardari, ottenuti costringendo la gente a cambiare apparecchi tutti gli anni, magari – c’è sempre una congiura da sventolare quando si è a caccia di clic a buon mercato – applicando la mitica obsolescenza programmata.

I rivoluzionari evidentemente conducono vite diverse da quelle degli esperti disillusi, secondo i quali Apple taglieggia i poveri utenti con prezzi fuori mercato delle parti di ricambio. Non si trova una buonanima che spieghi come mai, se Apple ha tutto questo guadagno dalla riparabilità dei suoi prodotti, vi abbia rinunciato finora, per cedere unicamente sotto pressioni e minacce di multe.

I più pittoreschi sono gli apostoli della sostenibilità, sempre pronti a invocare riduzioni ed eliminazioni di qualsiasi cosa a eccezione del loro berciare. Riparare apparecchi Apple li renderebbe più sostenibili.

Pensiamoci. Un miliardo di iPhone attivi. Fino a ieri, semplifico, un milione di iPhone sempre a disposizione per sostituire quelli guasti. Da oggi, un milione di iPhone per sostituire quelli guasti e in più un numero indefinito di parti di ricambio. Che richiedono catalogazione, magazzino, spedizioni, istruzioni, contabilizzazione, confezioni e altre occasioni di consumare risorse ed emettere gas serra. Parti di ricambio che, se sono diverse nel prossimo iPhone e non vengono richieste, non serviranno più e andranno smaltite. Non parliamo di chi si cimenterà nella riparazione fai-da-te e fallirà. Avrà consumato parti di ricambio _e anche_ sostituito la sua macchina. Per la sostenibilità, un successone.

Chi vuole, ripari la sua macchina, come gli pare. Come? Lo fa già con i manuali e i kit di riparazione di iFixit? Ma allora che senso hanno i peana per il programma di riparazione? Come? Sono parti originali? Eh no, incontrati con quell’altro che si vanta da sempre di avere usato parti qualunque, liberamente comprate su Internet, che costano una frazione e vanno benissimo. Se possibile, passa anche da quello che i prodotti Apple sono chiusi e non riparabili, che mi riempie il feed da anni e non mi ha ancora mostrato quali cambiamenti siano stati improvvisamente apportati alle architetture.

Come tema dove tutti vogliono avere ragione con la loro tesi pseudocomplottista, incurante delle contraddizioni e delle incompatibilità con le altre tesi, la riparabilità dei prodotti Apple è seconda solo ai vaccini.

Sono argomenti più fallati delle macchine da riparare.

_Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile._