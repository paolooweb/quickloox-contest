---
title: "Alimentare le supposizioni"
date: 2014-08-27
comments: true
tags: [Apple, HP, Hewlett-Packard, MacBookPro]
---
Per chiudere il discorso proseguito con un [programma di sostituzione di batteria di iPhone](https://macintelligence.org/posts/2014-08-26-come-volevasi-sostituire/) a proposito di un [supposto problema di non tanto vecchi MacBook Pro](https://macintelligence.org/posts/2014-08-23-manca-la-controprova/), guardiamo in casa d’altri.<!--more-->

Hewlett-Packard ha cominciato un [programma di sostituzione di cavi di alimentazione](http://h30652.www3.hp.com) dei propri portatili. Secondo *Cio*, solo tra Stati Uniti e Canada di parla di [oltre sei milioni di cavi interessati](http://www.cio.com/article/2599101/hp-recalls-6-million-laptop-power-cords-that-can-pose-fire-and-burn-hazards.html).

Il tutto per ventinove casi di surriscaldamento o fusione del cavo stesso.

Se una multinazionale ha un problema, non lo ignora, ma lo risolve, perché costa meno del danno di immagine.

E ora possiamo mettere un MacBook Pro 2011 sopra la questione, a chiuderla per sempre.