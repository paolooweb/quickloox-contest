---
title: "Insegnamento a distanza"
date: 2020-07-18
comments: true
tags: [Corea, Wi-Fi]
---
Mi è ancora poco chiaro in che modo le scuole italiane riapriranno a settembre, quanto in aula, quanto fuori, con che orari, con che strumenti, con quanto distanziamento eccetera. Come titolare di figlia regolarmente iscritta al suo primo anno, apprezzo questa _suspence_ che tiene desta l’attenzione e stimola la produzione adrenalinica.

Sono anche confortanti i programmi del governo: 240 mila tablet in arrivo per gli studenti, Wi-Fi in tutte le scuole primarie e secondarie entro il 2022.

I programmi [del governo coreano](https://interestingengineering.com/south-korea-commits-61-billion-for-net-zero-society-by-2025), intendevo.

Sembra che l’insegnamento a distanza sia inevitabile; o l’Italia impara qualcosa dalla Corea, o mandiamo gli studenti a studiare a Seoul.