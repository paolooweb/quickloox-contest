---
title: "Stato, Chiesa e aggiornamenti"
date: 2020-02-01
comments: true
tags: [Xp, Chrome]
---
In questi giorni mi sono misurato con una certa intensità verso due macchine burocratiche: quella statale e quella ecclesiastica.

A parte qualsiasi altra considerazione, semplicemente sul confronto tra due macchine burocratiche, quella ecclesiastica surclassa l’altra a mani bassissime per esperienza utente, tempi e soddisfazione del cliente.

La ciliegina sulla torta, per modo di dire, è quando entro in municipio e il totem che distribuisce i numerini è bloccato.

C’è una piccola umanità che si raduna attorno alla macchina, borbottano, confabulano, sembra che nessuno sappia realmente che cosa fare. Meglio, che nessuno abbia voglia di farlo. Ogni tanto qualcuno tocca il pulsante per verificare se il totem, metti mai, si sia sbloccato da solo. Dietro una porta una voce urla *piantatela di premere il pulsante, non vedete che non funziona? Qui siamo pieni di numeri e non si capisce più niente*.

A onor del vero, si vede che non funziona solo dopo avere premuto il pulsante e avere constatato che il numerino non esce.

In alto c’è lo schermo che mostra l’elenco dei numerini chiamati. Una striscetta gialla ricorda che Chrome è non aggiornato, perché *Windows Xp e Vista non sono più supportati.*

 ![Chrome non più supportato per Windows XP e Vista](/images/comune-cesate.jpg  "Chrome non più supportato per Windows XP e Vista") 

Nel 2020.

Quando mi parlano di stato e informatica, rispondo in astratto qualcosa sulla inderogabile necessità di adottare software *open source* per la pubblica amministrazione.

Mi rendo conto che è come parlare del coronavirus e ribadire l’importanza di una alimentazione equilibrata: un obiettivo stupidamente più elevato di quella che è l’emergenza reale.