---
title: "Prezzi da pagare"
date: 2014-01-19
comments: true
tags: [iOS, Android, Ftc]
---
L’anno scorso Apple si è trovata in tribunale perché denunciata da genitori i cui figli avevano effettuato acquisti *in-app* dentro giochi scaricati da App Store.<!--more-->

Il problema era che, dopo avere autorizzato acquisti *in-app* attraverso la password, iOS permetteva di effettuare altri acquisti senza la password per un periodo di quindici minuti. I ragazzini capiscono in fretta quando possono aggirare il controllo dei genitori.

Apple ha inviato ventotto milioni di email, una per ciascun acquirente americano su iTunes che aveva comprato giochi su App Store e anche effettuato acquisti *in-app*, chiedendo di segnalare un acquisto effettuato per errore, allo scopo di rimborsarlo. A chi non aveva più un indirizzo email funzionante è stata inviata una cartolina postale. Sono arrivate richieste di rimborso da 37 mila persone. Un giudice federale ha approvato l’iniziativa e l’ha ritenuta congrua e sufficiente a chiudere la questione.

Ora la Federal Trade Commission (Ftc) americana ha minacciato Apple di intentare una seconda causa, rispetto alla stessa identica vicenda. Per evitare un secondo andirivieni dai tribunali, per una lite considerata già risolta da un giudice, Apple ha accettato di arrivare a un [accordo extragiudiziario](http://recode.net/2014/01/15/apple-agrees-to-ftc-consent-decree-over-in-app-purchases/) con la Ftc.

L’amministratore delegato Tim Cook ha spiegato l’accaduto in un messaggio ai dipendenti e commentato che il caso sa molto di *double jeopardy*, trovarsi due volte alla sbarra per lo stesso reato. La Costituzione americana impedisce di processare qualcosa o qualcuno due volte per lo stesso reato penale, non in una causa civile. Cook ha inteso dire che l’iniziativa della Ftc è formalmente legittima, ma sostanzialmente ingiusta.

Sia incuria del genitore o del sistema operativo, Apple pagherà comunque in rimborsi un minimo di 32,5 milioni di dollari.

Ai margini di tutto questo, *Consumer Reports* pubblica un articolo intitolato [Google Play lascia spendere tuo figlio come un marinaio ubriaco](http://www.consumerreports.org/cro/news/2014/01/google-play-store-lets-your-kid-spend-like-a-drunken-sailor/index.htm). Titolo probabilmente eccessivo; l’articolo spiega comunque che le *app* per Android, dopo avere autorizzato un acquisto *in-app*, permettono potenzialmente a un bambino curioso e ignaro di fare acquisti senza bisogno di password per trenta minuti.

Nessuno pagherà niente. Famiglie a parte.