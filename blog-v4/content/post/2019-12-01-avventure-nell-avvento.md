---
title: "Avventure nell’Avvento"
date: 2019-12-01
comments: true
tags: [Perl, Raku, Avvento, Advent]
---
A Natale ci is regala qualcosa da imparare e Slashdot ha pubblicato una bella panoramica di [calendari dell'Avvento](https://entertainment.slashdot.org/story/19/12/01/236222/2019-sees-more-geeky-advent-calendars) dedicati alla programmazione.

Stabilito che per fare un giro come si deve dappertutto ci vorrebbe un Avvento di novanta giorni, il mio preferito è per certo [Advent of Code](https://adventofcode.com), con un bel po’ di lunghezze sugli altri. Mi piace l’idea di porre problemi anche piacevoli da leggere e mi piace che la scelta del linguaggio di programmazione per provarci sia lasciata al lettore.

Il design del sito, poi, è spettacolare, mi viene voglia di provarci anche solo a guardarlo.

Prima di buon Natale per leggere, buon Avvento per programmare, nel linguaggio preferito da ciascuno.
