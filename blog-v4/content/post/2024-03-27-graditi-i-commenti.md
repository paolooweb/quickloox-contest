---
title: "Graditi i commenti"
date: 2024-03-27T02:11:18+01:00
draft: false
toc: false
comments: true
categories: [Blog]
tags: [Comma, Mimmo, Muut]
---
Grazie all’enorme lavoro di [Mimmo](https://muloblog2.netlify.app), il blog sta passando a un nuovo sistema di commenti, come [anticipato a suo tempo](https://macintelligence.org/post/2024-03-08-e-i-commenti-muut/) per via dell’imminente chiusura di Muut, il servizio che li ha forniti finora.

In questi giorni potrebbero emergere problemi o fastidi. L’obiettivo è avere tutto funzionante entro la fine del mese, ma dobbiamo anche riuscirci.

È ampiamente possibile che in questo momento l’interfaccia di commento non funzioni o faccia cose strane. Chiediamo pazienza per qualche giorno.

I commenti registrati finora su Muut non sono persi: verranno scaricati domani, ultimo giorno di fornitura del servizio. Si tratta di un file Json di una dozzina di megabyte, a disposizione di chi ne vorrà una copia.

Contiamo di integrare i vecchi commenti nel nuovo sistema e riassociarli ai post originali, ma anche qui ci dobbiamo arrivare. Anzi, se ci sono idee e capacità di elaborazione, una mano è graditissima.