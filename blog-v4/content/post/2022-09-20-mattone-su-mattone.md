---
title: "Mattone su mattone"
date: 2022-09-20T02:49:13+01:00
draft: false
toc: false
comments: true
categories: [Hardware]
tags: [Breakout, Steve Wozniak, Woz, Wozniak, Atari, Steve Jobs, Jobs, GameInformer, Apple II]
---
Per un momento nella vita ho battuto tutti i giochi che ho giocato. È stato quando ho scoperto [Breakout](https://elgoog.im/breakout/?q=SearchByImage&tbs=boee:1&ved=0), qui in una curiosa versione derivata da Google Immagini.

Era il primo videogioco battibile su cui mi sono impegnato e l’ho battuto. Cento percento.

Ero ragazzino, in una sala giochi estiva. Il monitor era monocromatico e i mattoni si vedevano colorati per via di una pellicola apposta sul vetro.

Avevo una modesta paghetta e la dissipai dentro *Breakout*. Ma arrivai in fondo, pure con una certa delusione. La pallina viaggiava più forte, dopo il primo muro la racchetta si restringeva alle stesse dimensioni della pallina, ma un preadolescente ha poteri inimmaginabili per un adulto. Anche con la racchetta puntiforme avevo sulla pallina un controllo praticamente assoluto ed ero in grado di indirizzarla a piacere per un tempo indefinito.

Per questo, dopo avere abbattuto anche il secondo muro, restai per diversi minuti a palleggiare nello schermo vuoto nella speranza che succedesse qualcosa.

*Breakout* durava due livelli e questo era tutto.

Ho recuperato il ricordo dopo avere letto su *GameInformer* una intervista a Steve Wozniak: [Come il Breakout di Steve Wozniak definiì il futuro di Apple](https://www.gameinformer.com/b/features/archive/2015/10/09/how-steve-wozniak-s-breakout-defined-apple-s-future.aspx). Perché fu Jobs a farsi prendere in Atari e a tirare dentro Wozniak. Perché fu Woz a realizzare *Breakout*. Perché il loro sodalizio si rafforzò ed eccoci qui.

Nell’intervista c’è tutto sulla storia iniziale di Apple, dentro le righe e tra esse. Jobs era il visionario, Woz lo smanettone. Jobs aveva un sogno e passava sopra qualsiasi cosa per arrivarci, Woz trattava lo hardware come plastilina e tirò fuori un computer intero, Apple II, dal genio e dalla somma ignoranza di quanto facevano decine di ingegneri prodotti in serie dentro altrettante aziende: semplicemente, senza pregiudizi, scelse tutte le strade migliori.

Al tempo stesso, Woz amava giocare, ma non aveva una mentalità da giocatore e aveva una passione puramente hardware. A un certo punto, lo dice lui stesso:

>Ora [dopo che aveva realizzato i paddle per giocare a Breakout su Apple II] i giochi erano software. Fino ad allora, nelle sale giochi non c’erano giochi software. Ora che i giochi animati sarebbero diventati software… ommioddio.

*Breakout* era progettato e realizzato nello hardware. Era una innovazione clamorosa. Ma il passo successivo era che i giochi sarebbero stati progettati e realizzati nel software. Woz non aveva alcun interesse in questo sviluppo.

Né lo aveva Jobs, che cercava di affermare Apple sul mercato e nelle aziende. I giochi non erano, nella sua visione, il mezzo per arrivarci.

Manca non moltissimo ai cinquant’anni e tuttora Apple fabbrica hardware splendido, software di complemento allo hardware, con poco o nessun interesse per i giochi software. Per dire quanto può essere forte una eredità di idee e approcci da parte di due geni.