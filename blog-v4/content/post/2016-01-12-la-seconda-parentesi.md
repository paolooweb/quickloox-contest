---
title: "La seconda parentesi"
date: 2016-01-12
comments: true
tags: [Penflip, Frix01, Git, BBEdit, BitBucket, Markdown, GitBook, iPad, iPhone]
---
Il secondo strumento di scrittura opzionalmente collaborativa in [Markdown](https://daringfireball.net/projects/markdown/) che [suggeriva frix01](https://macintelligence.org/posts/2016-01-10-metti-una-sera-con-penflip/) è [GitBook](https://www.gitbook.com/).<!--more-->

Nemmeno lui mi convince ad abbandonare il mio flusso di lavoro attuale. A differenza di [Penflip](http://www.penflip.com/), tuttavia, *potrebbe* tentarmi.

GitBook è più complesso di Penflip e i due non sono da paragonare. Passerei a GitBook esattamente per quello che manca a Penflip, ossia una vera integrazione con Git presente nel sistema. Chi ha un account GitHub lo può usare direttamente ma non è obbligato a farlo e trovo il dettaglio prossimo alla perfezione.

Ho apprezzato la presenza del controllo ortografico configurabile in italiano (anche se mi sembra che manchi una opzione multilingue) e il conteggio dei caratteri presente assieme a quello delle parole.

La fase produttiva di GitBook ruota attorno a un editor presente sia su web che come applicazione desktop. L’editor nel *browser* è ben fatto (l’inserimento dei *link* è meno furbo di [come lo vorrei](https://macintelligence.org/posts/2016-01-11-chiudiamo-una-parentesi/), ma buono). La versione desktop tuttavia non rivaleggia (né potrebbe, questione di prezzi) con [BBEdit](http://www.barebones.com/products/bbedit/).

Per chi se ne può avvantaggiare, una opzione eccezionale è l’integrazione dei comandi [LaTeX](https://www.latex-project.org) per inserire equazioni e linguaggio matematico-scientifico. La semplicità di Markdown accoppiata a complessità infinita di qualità assoluta. Da urlo.

Una perplessità che mi resta è come si comportino questi strumenti su iPad e iPhone (non hanno *app* dedicate). Per capire quanto veramente sia affidabile la modalità *browser* su quegli schermi ridotti occorre un tempo di test che non ho dedicato ancora. Qualcuno troverà assurdo che si voglia lavorare su iPad a un libro ma, se questo è il caso, si tratta di persone che non hanno ancora capito che cosa può fare con profitto un iPad. E garantisco che durante la lavorazione dei miei ultimi due libri mi è capitato di fare qualcosina anche su iPhone. Magari una correzione banale, una cosa di trenta secondi; ma in quei trenta secondi tutto deve funzionare in modo da garantire lo stesso risultato che si otterrebbe su desktop. Per sistemi gratuiti a livello personale ma a pagamento su quello professionale (vale sia per BitBook che per Penflip), sarà bene che lavorare su iPad sia comodo, veloce e senza sorprese. Sarò lieto di riceverne conferma.

Per una persona che cominciasse oggi un percorso professionale o comunque evoluto di scrittura su progetti anche complessi, lavorando con Markdown, GitBook potrebbe essere tutto o quasi quello che serve per partire e ne parlerà solo bene.