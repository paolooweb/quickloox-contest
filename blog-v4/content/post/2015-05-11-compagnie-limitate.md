---
title: "Compagnie limitate"
date: 2015-05-13
comments: true
tags: [Comex, watch, Michelangelo, browser, design]
---
A marzo avevamo [scomodato Michelangelo](https://macintelligence.org/posts/2015-03-23-per-via-di-levare/) per parlare dell’assenza di un *browser* dentro watch.<!--more-->

Puntuale, ecco uno smanettone di soprannome Comex che pubblica il video titolato [Ho sempre voluto un browser sul polso](http://www.mobypicture.com/user/comex/view/18097875).

L’esperienza, anche perché raffazzonata e di zero valore fuori dall’ottenere il risultato in quanto tale, è inutile.

E si capisce perfettamente la grandezza del *design* che si sforza di togliere cose che possono non servire, contrapposto a quello che cerca di aggiungere cose che potrebbero servire. Da ricordare guardando quei computer pieni di porte inutili o i coltellini svizzeri, senza pari in campeggio, da tenere in un cassetto nella vita di tutti i giorni.

Uno dei mali dell’epoca è la volontà pervicace di fare qualcosa solo perché potrebbe essere fatto. Ecco un esempio del perché è un male.

Non so se Comex sia in buona compagnia nel volere il *browser* sul polso. Certamente limitata.