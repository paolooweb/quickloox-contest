---
title: "Avventure ineludibili"
date: 2018-10-04
comments: true
tags: [Google, text, adventure]
---
Non ci sarebbero il tempo e volendo nemmeno le condizioni. Però, mi nascondono una avventura testuale nella console JavaScript di Google.com e non ci provo?

Ci ho provato e sono arrivato in fondo.

 ![Il finale dell’avventura testuale nascosta dentro Google](/images/text-adventure.png  "Il finale dell’avventura testuale nascosta dentro Google") 

È semplice, ci mancherebbe. Un *divertissement* da pausa caffè le cui ultime parole sono in effetti *go back to work*. Ciononostante, arrivare in fondo a un’avventura testuale, anche un’avventurina come questa, distende e tonifica l’animo.

(Apri [Google.com](https://www.google.com), cerca *text adventure*, apri la console JavaScript del *browser* che deve essere di sicuro Chrome o forse anche Firefox, rispondi *yes* e inizia a giocare).

(Con [omaggi](/blog/2017/05/29/intelligenza-colossale/) all’avventura originale che ha fatto partire tutto tanti anni fa e continua a vivere).