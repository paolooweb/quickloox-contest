---
title: "Textastic contro Editorial"
date: 2014-01-14
comments: true
tags: [Textastic, Editorial]
---
Finora il mio editor di testo preferito per produrre testo puro e strutturato (leggi: Html) su iPad è stato Textastic: [7,99 euro su iPad](https://itunes.apple.com/it/app/textastic-code-editor-for/id383577124?l=en&mt=8), [7,99 euro su iPhone](https://itunes.apple.com/it/app/textastic-code-editor-for/id550156166?l=en&mt=8) e [7,99 euro su Mac](https://itunes.apple.com/it/app/textastic/id572491815?l=en&mt=12), dove peraltro è impensabile rinunciare a [BBEdit](http://barebones.com/products/bbedit/).<!--more-->

Ultimamente ha guadagnato terreno [Editorial](https://itunes.apple.com/it/app/editorial/id673907758?l=en&mt=8), solo su iPad a 4,99 euro, come [ho già detto](https://macintelligence.org/posts/2013-12-03-una-pagina-dal-futuro/) cinquanta volte [recensito alla perfezione](http://www.macstories.net/stories/editorial-for-ipad-review/) da Federico Viticci di *MacStories* che ne ha tratto persino un [libro](https://itunes.apple.com/it/book/writing-on-ipad-text-automation/id697865620?l=en&mt=11) assolutamente da acquistare, per la bella cifra di 1,99 euro. Possiedo anche Editorial e mi sono messo ad alternare il lavoro sull’uno e sull’altro, per capire quale soddisfi meglio i miei scopi e il mio modo di lavorare. Ne è uscito per ora solo un gigantesco *sì, però*.

Inutile che scriva una recensione di Editorial, che c’è già. Inutile farne una di Textastic, perché è come se si fossero divisi i compiti alternando le genialate prima di qui, ora di là.

Per dire, il supporto di Dropbox su Textastic è manuale (scomodo) e elastico (comodo), nel senso che attivata la connessione è facile sistemare i file come e dove si vogliono. Dropbox su Editorial è automatico (comodo) e rigido (scomodo), perché occorre dargli una cartella precisa da sincronizzare.

Uno dice che allora Textastic sincronizza anche via iCloud. A patto però di avere speso sedici o ventiquattro euro. Mentre Editorial è limitato a Dropbox e non ti permette di spostare un file su Dropbox se lo hai creato nella cartella locale di iPad (bisogna dire da subito che è destinato a Dropbox). Certo, però è programmabile ed esiste un [flusso di lavoro Python](http://www.editorial-workflows.com/workflow/5800069865406464/iw0nEAiIV3I) che risolve il problema, installabile in automatico.

Succede un sacco di volte: si vede una limitazione di Editorial e subito ecco il flusso di lavoro che la supera. Come dovrebbe essere su Mac con AppleScript e Automator. Però è anche vero che Editorial scrive in Markdown e finisce lì, mentre Textastic governa la sintassi di una valanga di linguaggi di programmazione.

Textastic è anche superiore nella gestione degli extra della tastiera, tasti speciali e movimenti del cursore. Ma vuoi mettere il browser incorporato di Editorial? Che tuttavia vale poco senza tutta l’integrazione iCloud di Safari, irrinunciabile dopo averci fatto l’abitudine.

Se ci sono domande specifiche farò di tutto per rispondere in modo approfondito. Se si vuole un consiglio, non lo si può dare. Sono programmi che fanno la stessa cosa eppure diversi come la notte e il giorno, tali che chi si alza presto la mattina ne preferirò uno e chi va a letto tardi la sera quell’altro, entrambi con ragione.

La vera conclusione: che strumento pazzesco, un iPad con il programma giusto tra le mani.