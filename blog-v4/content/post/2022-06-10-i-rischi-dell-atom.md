---
title: "I rischi dell’Atom"
date: 2022-06-10T14:22:22+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [Atom, GitHub, Electron, Visual Studio Code, Microsoft, Mimmo, Zed, BBEdit]
---
In principio era [GitHub](https://github.com), che nel costruire un deposito di software libero aperto a tutti si era creata alcuni in casa, tra i quali l’editor di testo [Atom](https://atom.io): libero, aperto, modulare, estensibile, ci fai quello che vuoi (frase che nel mondo tecnologico porta sempre con sé presagi oscuri).

Microsoft si è comprata GitHub, il deposito principale di software libero. E, dopo avere garantito l’indipendenza del nuovo acquisto, si è data a diverse operazioni ambigue di [censura](https://macintelligence.org/posts/2021-03-12-detenuti-e-tiranni/), [installazioni nascoste di repository](https://macintelligence.org/posts/2021-02-07-il-diavolo-sta-nella-reticenza/) e altro. (Autopubblicità: se scrivi *Microsoft GitHub* nel campo di ricerca del blogghino, escono tanti risultati e almeno i primi otto sono cogenti. Un grazie a **Mimmo** che ci ha lavorato alla grande).

Microsoft possiede [Visual Studio Code](https://code.visualstudio.com), editor di testo orientato allo sviluppo, come Atom. Bellissimo, comodissimo, funziona ovunque, *costruito su software libero*, estensibile e creatore di dipendenza dall’ecosistema Microsoft. Così come il software libero deve finire su GitHub per controllarlo e pilotarlo più che si può, allo stesso modo chi scrive software deve farlo con Visual Studio Code, in modo da poter controllare e pilotare gli sviluppatori.

Guarda la coincidenza a volte, Atom non riceve più aggiornamenti significativi da tre anni e ora [ne è stato annunciato il tramonto](https://github.blog/2022-06-08-sunsetting-atom/) per fine anno, per rifocalizzarsi, chi lo avrebbe mai immaginato, su Visual Studio Code. La mano amichevole del padrone ha stretto la presa.

Ma è tutto software libero, ti dicono, non muore mai. Certo, la memoria di Atom continuerà a esistere nello sforzo attualmente compiuto dagli sviluppatori di [Zed](https://zed.dev). Conta essere vivi, altroché. Conta anche essere rilevanti, però. Zed lo useranno quattro gatti e tutti gli altri si faranno passare i croccantini dalla mano amichevole del padrone, che passa cibo comodissimo e abbondante in cambio della permanenza nel recinto.

Ecco perché sono pronto a pagare soldi veri per il prossimo aggiornamento di [BBEdit](https://www.barebones.com/products/bbedit/), proprietario fino alle mutande. Il software non è libero perché te lo dice il guardiano che non ama essere contraddetto.