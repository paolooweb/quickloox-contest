---
title: "Bisogna provare tutto nella vita"
date: 2014-04-12
comments: true
tags: [Emu, iPhone, TechCrunch, Android, iOS, Cheney]
---
[Raccontano su TechCrunch](http://techcrunch.com/2014/04/06/the-fallacy-of-android-first/) gli autori di [Emu](http://emu.is/download/) che nel 2012 hanno sfidato le convenzioni e realizzato la propria *app* per Android invece che per iOS.<!--more-->

Le informazioni dicevano loro che i possessori di Android pagano meno di quelli di iOS ma scaricano *app* volentieri, ovviamente che sono più numerosi, che il sistema sta diventando più semplice ed efficace da usare, che [personaggi famosi nella stampa informatica avevano abbandonato iPhone](http://www.techhive.com/article/2030042/why-i-switched-from-iphone-to-android.html), che le *app* erano diventate più facili da scrivere.

>Così siamo saltati sul treno. Abbiamo buttato via il prototipo per iPhone su cui lavoravamo, ripulito la ruggine dalla nostra conoscenza di Java e prodotto una versione preliminare (alpha) in febbraio 2013, seguita da una beta pubblica in luglio. In ottobre abbiamo lanciato Emu con un’ottima copertura stampa, compresi alcuni che hanno cantato le nostre lodi specificamente perché avevamo scelto Android su iOS.

Epilogo:

>il 2 aprile 2014 abbiamo lanciato Emu per iPhone e abbiamo ritirato Emu per Android da Play Store. Speriamo di tornare su Android un giorno o l’altro, ma il nostro team è troppo piccolo per innovare e iterare su più piattaforme simultaneamente. Segue un elenco di ragioni per le quali abbiamo deciso che iPhone è un posto migliore su cui stare.

L’articolo cita [Steve Cheney](http://stevecheney.com/why-android-first-is-a-myth/):

>Tutte le mie conversazioni durante l’ultimo anno […] confermano una semplice e solida realtà: sviluppare e pubblicare per Android costa due o tre volte più che per iOS. […] Non è una questione di preferenze o di possibilità; è questione di come facciamo rendere al massimo tempi, energie e capitali limitati, specialmente nella fase di inizio del lavoro, dove è inevitabile attraversare una fase significativa di iterazioni di prodotto.

Gli autori di Emu scrivono che per loro è stata una enorme lezione e che avrebbero preferito impararla più rapidamente. Lo riporto per aiutare altri ad accorciare i tempi. O più banalmente per sapere di che cosa si parla quando al bar la rana dalla bocca larga sentenzia che ormai Android è uguale a iOS e costa meno. Se chi fa le *app* la pensa diversamente, colgo una contraddizione.