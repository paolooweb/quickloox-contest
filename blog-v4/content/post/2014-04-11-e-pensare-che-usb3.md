---
title: "E pensare che Usb3"
date: 2014-04-11
comments: true
tags: [Usb3, Thunderbolt, Mac, MacPro]
---
Anni fa ho polemizzato a lungo con chi sosteneva il supposto ritardo di Mac nell’adottare Usb3.<!--more-->

Adesso guardo foto come [questa](http://instagram.com/p/moAyEzL9Xb/), realizzata con un Mac Pro e connessione Thunderbolt.

<iframe src="//instagram.com/p/moAyEzL9Xb/embed/" width="600" height="696" frameborder="0" scrolling="no" allowtransparency="true"></iframe>

Fatemi vedere un Mac con quattro schermi esterni collegati via Usb3, per non parlare di tutto il resto, e dirò che i miei avversari di polemica avevano ragione.
