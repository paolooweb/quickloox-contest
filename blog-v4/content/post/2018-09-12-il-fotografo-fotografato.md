---
title: "Il fotografo fotografato"
date: 2018-09-12
comments: true
tags: [Dal, Verme, New, Old, Camera, Mac]
---
Ho passato la serata in un negozio di articoli per fotografia largamente frequentato da professionisti, [New Old Camera](http://www.newoldcamera.com) a Milano, ad ascoltare [Enzo Dal Verme](https://www.enzodalverme.com) parlare del libro [Marketing per fotografi](http://www.apogeonline.com/libri/9788850334520/scheda).

(*Disclaimer*: lavoro per l’editore del libro e ho scritto un [articolo](http://www.apogeonline.com/webzine/2018/09/13/insieme-a-parlare-del-mestiere-del-fotografo) sulla serata).

New Old Camera è un vero covo di professionisti, situato in pieno centro senza neanche una vetrina. Questa è la miniregia della saletta in cui si è svolto l’incontro.

 ![Il Mac di New Old Camera](/images/mac-noc.jpg  "Il Mac di New Old Camera") 

Enzo, personaggio affascinante e carismatico, ha vent’anni di carriera sulle spalle. Ha fotografato personaggi dello spettacolo a livello internazionale, è stato fotoreporter in località vicine e lontane, sa spiegarti come è stata fotografata la moda per i cataloghi su Internet e nel contempo promuoversi su Internet da marketing manager di se stesso. Questo è il suo computer.

 ![Il Mac di Enzo Dal Verme](/images/mac-enzo.jpg  "Il Mac di Enzo Dal Verme") 

Che poi le tematiche sollevate da Enzo interessano un sacco di gente ben oltre i confini del pianeta fotografia. Ma è un altro paio di maniche.
