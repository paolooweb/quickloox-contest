---
title: "Il neo di Apple"
date: 2016-05-09
comments: true
tags: [Apple, CDI, dermatoscopio, iPhone]
---
Apple continua a deludere le aspettative dei professionisti.

L’immagine seguente arriva (con mia gratitudine) da **Lorenzo**. Pure il testo:

>Centro Diagnostico Italiano, sede di piazza Gae Aulenti a Milano. Dermatoscopio di fabbricazione tedesca collegato a iPhone 6 per analisi dei nei. Usa la fotocamera collegato ad apposita app…

Un’occhiata alla [pagina del Centro](http://www.cdi.it/it/sedi/cdi_porta_nuova.html) dà un’impressione pochissimo dilettantistica. Peccato per questa imperfezione di Apple, che continua a trascurare il comparto di chi il computer lo usa per fare cose serie.

 ![Dermatoscopio](/images/dermatoscopio.jpg  "Dermatoscopio") 