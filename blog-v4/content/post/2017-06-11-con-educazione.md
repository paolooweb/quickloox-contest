---
title: "Con educazione"
date: 2017-06-11
comments: true
tags: [MacStories, iPad, iOS, programmazione]
---
Conto che il prossimo congresso del partito *iPad non è un computer perché non permette di programmare* metta all’ordine del giorno la questione dell’aggiornamento della [regola 2.5.2](https://developer.apple.com/app-store/review/guidelines/#software-requirements):

>Le app progettate per insegnare, sviluppare o collaudare codice eseguibile possono, in circostanze limitate, scaricare codice purché esso non venga usato per altri scopi. Tali app devono rendere completamente visibile è modificabile da parte dell'utente il codice sorgente fornito.

Prima era vietato anche lo scaricamento di codice, anche sulle app a scopo *education*. Siamo certo lontani da Xcode per iPad eppure, come [scrive MacStories](https://www.macstories.net/linked/apples-app-store-guidelines-now-allow-executable-code-in-educational-apps-and-developer-tools/),

>ė un importante segnale per gli sviluppatori indipendenti […] che porta la promessa di sviluppo app su iOS un passo più vicina alla realtà.
