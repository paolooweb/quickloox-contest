---
title: "Un’intelligenza che mancava"
date: 2021-01-04
comments: true
tags: [Nyt, India, coding]
---
Il problema di trattare le tendenze come mode è che, anziché moderare gli eccessi, si butta via il bambino con l’acqua sporca.

Oggi la moda è dare addosso alla tecnologia per nessuna ragione altra che compiacere il pubblico ignorante. Tecnologia che porta i suoi problemi e le sue storture, ma anche progresso e miglioramento delle condizioni di vita planetarie. In questi giorni si vaccinano a milioni persone fortunate a disporre di tecnologia che, prima di Pasteur, neanche si pensava. E le persone morivano come mosche.

Divago. Vengo al punto. Il *New York Times*, da qualche tempo novello paladino antitech, ospita un pezzo abbastanza fazioso intitolato [I bambini hanno davvero bisogno di imparare a programmare?](https://www.nytimes.com/2021/01/02/opinion/teaching-coding-schools-india.html).

Lo spunto è l’eccessiva insistenza con cui i genitori indiani vengono sollecitati perché la prole si dedichi al *coding*, pena il suo (della prole) futuro fallimento sociale. Il tutto, spiega l’articolo, in un contesto sbilanciato dove già adesso l’India ha un eccesso di ingegneri e in vista di un futuro dove la competenza programmatoria *potrebbe diventare ridondante*.

Tutto l’articolo ruota attorno alla questione del lavoro futuro di quattrenni e cinquenni, nonché dei sacrifici affrontati dalle famiglie indiane perché venga loro insegnato il *coding*. Zero approfondimento su quello che il pensiero computazionale può dare ai ragazzi come disciplina in sé, a prescindere dal fatto che poi porti uno stipendio.

Torno per un attimo alla nostra scuola, ché di quella indiana so poco. Ho una figlia che ha cominciato quest’anno; per quello che può significare per cervelli di sei anni, c’è già una prima diversificazione delle materie. C’è un orario suddiviso in italiano, matematica, scienze, storia, geografia, musica, arte, religione eccetera.

Insegnano italiano a mia figlia pensando al suo futuro da letterata? Non penso. Fa matematica perché da grande sarà una matematica? Può darsi, ma allora non sarà letterata. O farà la storica? O la scienziata? O l’artista, la teologa, la musicista? Quell’ora settimanale ridicola di *motoria* dovrebbe essere significativa per il suo futuro di atleta professionista?

Sarò fuori dal mondo, certo non scrivo per il *New York Times*: mi pare di no. A sei anni quello che si impara a scuola serve per formare una istruzione di base, non per cercare lavoro.

Indovina indovinello, il *coding* a sei anni servirà per fare la programmatrice? Evidentemente no. (Figuriamoci a cinque o a quattro).

Il cervello dei bambini è plastico, flessibile, aperto, dinamico, straordinario. Gli insegnamenti della scuola primaria aprono ai diversi tipi di pensiero e di attività intellettuale. Lavori con il linguaggio e si spalancano aree del cervello; ti cimenti con i numeri e se ne aprono altre. Così per la lingua straniera, lo sport, i colori, la musica, tutto. Ci sono, dicono teorie attuali, numerosi tipi di intelligenza (parola che significa *comprensione*).

E il *coding*? Uno studio recente di neuroscienziati del Massachusetts Institute of Technology, per quanto preliminare, pare mostrare che l’attività cerebrale connessa alla programmazione [sia diversa da quella generata dalle altre attività intellettuali](https://boingboing.net/2020/12/30/study-finds-brain-activity-of-people-coding-isnt-quite-like-when-they-use-language-or-do-math.html).

Il *coding* stimola aree del cervello diverse da quelle stimolate dalla letteratura. Ma anche dalla matematica; anche dal resto.

Se le conclusioni dello studio verranno confermate, significherà che la programmazione non è qualcosa di supplementare o superfluo; che stimola attività cerebrale non sollecitata dagli insegnamenti tradizionali. Che è una intelligenza da aggiungere al conto verso la completezza.

Si capisce abbastanza? Oggi si tende a pensare che dare *coding* ai bambini sia un di più. Quando invece potremmo essere stati tutti noi ad avere avuto una lacuna nella nostra istruzione primaria.