---
title: "Tutto da solo"
date: 2020-09-21
comments: true
tags: [Asymco, Dediu, iPad]
---
C’è sempre da contare su Horace Dediu per avere una lettura degli avvenimenti diversa dal coro e neanche su iPad si è smentito.

Nel ricordare i [dieci anni dal debutto dell’apparecchio](http://www.asymco.com/2020/09/16/the-ipad-at-10/), inserisce anche questa affermazione:

>Quello che iPad è riuscito a fare è crearsi la propria domanda. Si presenta al fianco delle altre piattaforme e non al loro posto. La gran parte degli utilizzatori di iPad possiede anche un iPhone e un PC oppure un Mac. […] iPad ha fatto qualcosa di più grande che sfrattare il PC dal suo trespolo. Gli si è appollaiato accanto e ha trovato la vita assai comoda.

iPad non ha preso il posto di altre piattaforme o standard. Ne ha creato uno nuovo, con piena dignità numerica (i computer a tavoletta c’erano da vent’anni, ma per numero di unità vendute potrebbero stare nel mio box).

Dediu, per quanto possa dire, è il primo ad avere formalizzato questa considerazione. Quanti altri apparecchi sono riusciti prima o dopo iPad a ottenere lo stesso risultato?

Quattrocento milioni di macchine attive, rispetto al traguardo quasi prodigioso di avere riempito una nicchia inedita, sono una bazzecola.