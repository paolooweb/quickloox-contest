---
title: "Cose che non si dicono"
date: 2018-06-26
comments: true
tags: [iOS, privacy, Cook, Wwdc]
---
Durante il mio sabbatico semiinvolontario sono accadute diverse cose, tra le quali [Wwdc](https://developer.apple.com/wwdc/) (Apple è stata indelicata a riunire gli sviluppatori di tutto il mondo mentre avevo il blog fermo) e Dan Moren ha scritto su Macworld proprio un bel pezzo sulle [mosse apparentemente controintuitive di Apple](https://www.macworld.com/article/3279005/wwdc/wwdc-2018-apples-counterintuitive-business-moves.html) annunciate esattamente a Wwdc.

Vi si coglie, in poche righe, il meglio di Apple e il perché i critici non capiscono. E anche i difetti.

L’azienda che raccoglie denaro vendendo apparecchi annuncia la app per controllare il tempo di utilizzo degli apparecchi stessi. Sembra autolesionismo; facile pensare che chi vende iPhone vuole vedere iPhone usati il più a lungo possibile. Proprio per questo; se una cosa è facile, non è da Apple. Ci sono ragioni diverse.

Analogamente, Craig Federighi a Wwdc ha spiegato le strategie per avere il massimo delle prestazioni da iOS 12 sugli apparecchi più vecchi. Gli scontati parlano di obsolescenza programmata, della spinta a cambiare iPhone ogni anno, del consumismo. Poi gli sviluppatori si vedono spiegare come il sistema operativo nuovo vuole andare veloce anche sui modelli vecchi. Facile pensare il contrario e appunto, ancora una volta: non è da Apple.

E poi la privacy. Se anche tutte le specifiche di prodotto fossero assolutamente e completamente pari, sceglierei Apple perché nn monetizza i dati di chi usa i suoi apparecchi. Gli altri lo fanno. La differenza eventuale di prezzo si spiega anche così: su Android o su Windows c’è la tassa invisibile sulla privacy.

Insomma, la Apple migliore è quella che sta lontana dalle cose facili.

Per questo mi danno fastidio *slide* come quella che apre il pezzo di Moren, dove Tim Cook mostra un gigantesco messaggio *il cliente al centro di ogni cosa*. Troppo facile, lo tira fuori anche il più scassato dei copywriter, non c’è azienda dove non salti fuori prima o poi. Non va bene. Mi aspetto altre cose, meno ovvie, meno facili.