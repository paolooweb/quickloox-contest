---
title: "Adesione automatica"
date: 2023-03-25T12:53:03+01:00
draft: false
toc: false
comments: true
categories: [Software, Web]
tags: [Automation April, MacStories, Comandi Rapidi, Shortcuts, Hay, Alex Hay, Toolbox Pro, scripting]
---
Nessun progetto per Pasqua? Rilassati, tranquilli, poltrona e intrattenimento disimpegnato?

Suggerisco vivamente di mettere almeno parte del tempo nella seconda edizione di [Automation April](https://www.macstories.net/news/coming-soon-the-second-annual-automation-april-community-event-featuring-shortcuts-interviews-discord-workshops-and-a-shortcut-contest/), in programma su *MacStories*.

Comandi Rapidi da scoprire, imparare, costruire, raffinare. Nel posto virtuale migliore al mondo per farlo e nessuna esagerazione nel dirlo, perché MacStories nasce da un italiano ma costituisce un’eccellenza globale.

Poi è una seconda edizione. La prima [è andata molto bene](https://macintelligence.org/posts/2022-04-18-la-settimana-automatizzata/) e questa potrà solo fare tesoro di una esperienza positiva precedente.

La cosa bella dello scripting è che si ha sempre una ennesima occasione. Non è mai troppo tardi per provare e finalmente si può usare la frase in modo non scontato.

Tra l’altro, questa edizione [è dedicata alla memoria di Alex Hay](https://www.macstories.net/stories/remembering-alex-hay-the-maker-of-toolbox-pro-during-automation-april/), sviluppatore di talento, mancato a trentasei anni a causa di una malattia crudele.

Approfittare dei workshop, provare a comporre un Comando rapido da soli, anche solo scaricare quelli che verranno resi disponibili è un modo per espandere gli orizzonti e testimoniare solidarietà. MacStories farà beneficenza mirata verso la ricerca contro il cancro.

Non c’è nemmeno da iscriversi, basta aderire con la testa e il cuore.