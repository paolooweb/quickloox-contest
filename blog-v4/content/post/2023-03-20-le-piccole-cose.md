---
title: "Le piccole cose"
date: 2023-03-20T01:25:35+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [Apple Watch]
---
Ogni tanto faccio una scoperta banalissima che dà la svolta alla giornata e semplifica un pezzetto di vita. Ad averne tante, la vita si semplifica bene e la cosa si fa interessante.

L’ultima in ordine di tempo è la complicazione di watch che dà accesso immediato alla app di sistema per impostare la sveglia. In quanto tale, niente di che. Ma poi sul quadrante appare l’orario della sveglia impostata. Il dubbio *ma ho messo la sveglia o no?* è bandito per sempre, basta un’occhiata ed è dissipato.

Non solo: se alzarsi è difficile e si pospone la sveglia, sul quadrante appare un grafico che mostra quanto manca alla suoneria seguente. Tutto sotto controllo. Per un nottambulo, è qualcosa.

Qual è la tua piccolezza preferita nell’ecosistema Apple?