---
title: "Accesso, mai eccesso"
date: 2015-06-24
comments: true
tags: [iPhone, Vittorio, FaceTime]
---
Rinvio il *post* previsto per oggi perché mi ha scritto **Vittorio** in tema di accessibilità.<!--more-->

§§§

Ho letto [questo post](https://macintelligence.org/posts/2015-06-20-vedo-e-non-vedo/) è mi è venuta in mente la spesa al supermercato che ho fatto domenica con mia moglie. Te la riferisco, perché mi sembra in tema.

Ero in coda alla cassa e vicino a me, in fila, c’era una giovane donna, dai tratti asiatici. Aveva davanti al volto un iPhone, con aperta una videochiamata (FaceTime o altro, non saprei).

Ho impiegato qualche secondo a capire cosa non mi quadrava della scena: da un lato il silenzio, perché lei non parlava e neppure dallo schermo usciva alcun suono. Dall’altro lato, il suo gesticolare.

Insomma, una ragazza sordomuta che, chissà, aveva chiamato casa per chiedere cos’altro serviva per la cena. Una cosa che noi facciamo spesso, senza pensare che, per alcuni, è una vera conquista.

Come un telefono ti cambia la vita…

§§§

Ricordiamocene, quando qualcuno ricorda come erano semplici e comodi i telefoni di una volta, che costavano due soldi e la batteria andava avanti per un mese. Si può vederci bene ed essere miopi.