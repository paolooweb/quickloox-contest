---
title: "Nuovi computer crescono"
date: 2019-09-26
comments: true
tags: [iPad, Viticci, iPadOS, iPhone]
---
In un periodo difficile per scrivere (niente di che, puro sovraccarico) è contrappasso che arrivino le novità più ghiotte.

I nuovi iPhone ricevono recensioni definitive rispetto alla superiorità sulla concorrenza. Certo, se venissero lette il mondo sarebbe un posto ancora più noioso di Facebook.

Gli watch serie 5 convincono per la diversificazione dei materiali e le nuove funzioni, soprattutto una: l’*always on* dello schermo. watch è sottovalutato al punto che si accinge a superare Mac nelle vendite e pochissimi danno segno di accorgersi che ha, diciamo, un successo modesto eppure consistente.

La novità che preferisco è iPadOS 13.1. Non è un computer? Parliamone, purché sia dopo la lettura di [Federico Viticci su MacStories](https://www.macstories.net/stories/ios-and-ipados-13-the-macstories-review/). Sì, trenta pagine. Che finiscono così:

>Mentre cala il sipario sui primi dieci anni di iPad, qualunque cosa riservi il futuro, Apple ha deciso che per la tavoletta è tempo di crescere e sta permettendo a iPad di vivere la sua avventura su misura. Ispirato da Mac, con i piedi ben piantati nella tradizione di iOS, senza paura del domani.

iPad è stato il mio computer per tutta l’estate. Ora passo più tempo su Mac ma, quando prendo la tavoletta, non sento alcuna *diminutio* nelle mie possibilità. Dopo dieci anni di iPad, tutto sembra tranne che un progetto al tramonto. E viene da essere positivi e ottimisti più che mai su quello che ci aspetta per la nostra vita di computing. La quale è tangente in più di un momento alla vita in generale.
