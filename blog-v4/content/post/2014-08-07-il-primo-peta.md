---
title: "Il primo peta"
date: 2014-08-08
comments: true
tags: [Microsoft, Google, Apple, Gmail, privacy, OneDrive, Iran, Lolita]
---
Giusto l’altroieri [si chiacchierava](https://macintelligence.org/posts/2014-08-06-patti-chiari-privacy-lunga/) di diritti alla *privacy* e doveri individuali, a proposito di Google che scandisce automaticamente Gmail alla ricerca di foto considerate pedopornografiche e, se ne trova, segnala il fatto alle autorità.<!--more-->

È successo [anche con Microsoft](http://www.thesmokinggun.com/documents/Microsoft-scanning-for-illicit-images-675432), arrestato per la presenza di foto problematiche sul suo *account* OneDrive e per averne fatte circolare mediante il proprio indirizzo live.com (servizio Microsoft).

Rimando alla lunga analisi dell’altroieri per la comprensione del terreno su cui ci si muove. Si possono sollevare anche decine di domande tra l’inquietante e il paradossale (con che tipo di foto si finisce nei guai in Iran? Che altri tipi di dati vengono frugati oltre alle foto? Se metto il testo di [Lolita](http://www.altrestorie.org/libri/Nabokov-Lolita.pdf) su OneDrive, rischio? Le aziende che non cercano attivamente foto cattive sui propri *server* commettono un reato?).

Aspettando che venga tirata in ballo anche Apple, cosa che avverrà tipicamente a sproposito, mi limito a commentare *chi è senza peccato scagli il primo peta*(byte).