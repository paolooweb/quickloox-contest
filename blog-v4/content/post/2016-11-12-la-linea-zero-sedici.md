---
title: "La linea zero-sedici"
date: 2016-11-12
comments: true
tags: [MacBook, Pro, Ram, Ssd]
---
Parliamone con serenità: Jonathan Zdziarski mostra che con un MacBook Pro da sedici gigabyte di Ram [si può fare un numero impressionante di cose](https://www.zdziarski.com/blog/?p=6355). Sono uno sprecone di aggettivi, ma stavolta ci sta tutto. Cito dal suo *post* l’elenco di quello che ha acceso senza che il computer iniziasse a paginare e spostare dati dalla Ram al disco Ssd. A me fa impressione.

* VMware Fusion: tre macchine virtuali in funzione (Windows 10, macOS Sierra, Debian Linux)
* Adobe Photoshop CC: quattro foto da trentasei megapixel multilivello, da oltre un giga ciascuna
* Adobe InDesign CC: un progetto fotografico di ventidue pagine
* Adobe Bridge CC: navigazione di una cartella con 307 immagini per 163 gigabyte totali
* DxO Optics Pro: modifica di una cartella di immagini
* Xcode: cinque progetti in Objective-C, tutti puliti e con un build nuovo di zecca
* Microsoft PowerPoint: una presentazione
* Microsoft Word: quindici capitoli, un file ciascuno, del mio ultimo libro
* Microsoft Excel: un workbook
* MachOView: analisi del binario di un daemon
* Mozilla FireFox: quattro siti differenti in una finestra a testa
* Safari: undici siti differenti in una finestra cadauno
* Anteprima: tre libri in Pdf, di cui uno molto pieno di grafica
* Hopper Disassembler: analisi in corso di un file binario
* WireShark: acquisizione in corso di dati di rete
* IDA Pro 64-bit: analisi di un binario Intel a 64 bit
* Apple Mail: vista di quattro caselle
* Tweetbot: lettura delle menzioni
* iBooks: visualizzazione di un libro
* Skype: collegato e inattivo
* Terminale: una manciata di sessioni inattive
* iTunes
* Little Flocker
* Little Snitch
* OverSight
* Finder
* Messaggi
* FaceTime
* Calendario
* Contatti
* Foto
* Veracrypt
* Monitoraggio Attività
* Path Finder
* Console
* Probabilmente molto altro di cui non mi sono reso conto

*Oversight* potrebbe significare varie cose; per il resto, conosco tutti i programmi menzionati e onestamente, nel complesso, la mascella tende a cadere. Anche perché lo spazio per chiudere qualcosa, dovesse servire Ram, sembra essere abbondante.

Zdziarski non sottovaluta affatto il problema della Ram in un computer moderno:

>Sono sicuro che alcuni utenti con requisiti davvero pesanti si mangeranno più di sedici gigabyte e questo non è in alcun modo un tentativo di minimizzare le loro preoccupazioni. Il lavoro con la produzione di video e audio è un’area in cui posso immaginarmi che sia un problema reale.

Al tempo stesso mette in guardia contro i programmi mal scritti, che si mangiano Ram in quantità eccessiva rispetto alle necessità effettive, e poi scrive una cosa che trovo illuminante:

>Molti che superano il limite di sedici gigabyte potrebbero avere in funzione un sacco di programmi mal scritti e non necessari. Controllate /Library/LaunchDaemons e /Library/LaunchAgents, oltre alla cartella LaunchAgents in ~/Library, e verificate anche gli elementi di login. Potreste anche assicurarvi di non avere in funzione malware, adware e bloatware. Fate attenzione ad avere tutte le applicazioni aggiornate. Le perdite di memoria sono un bug comune e una versione più vecchia e meno efficiente nella gestione della Ram provocherà occupazione eccessiva di memoria qualunque sia l’ammontare di Ram installata.

>Non dubito che esistano casi limite dove il consumo di Ram eccede i sedici gigabyte e Apple dovrebbe certamente considerare l’idea di aggiornare la propria linea di Mac Pro per questa esigenza; MacBook Pro è progettato per essere portatile e consumare poca energia. Non è una macchina da tavolo e non si comporterà come una di esse fino a che segue questi vincoli. Ciò detto, credo che molte (non tutte) le situazioni in cui si usano più di sedici gigabyte di Ram siano causate da fattori di cui l’utente ha il controllo: software male scritto, trascuratezza sui programmi in avvio automatico, configurazione impropria dei programmi, persino malware. Togliete di mezzo queste cose intanto e, anche se rimanete comunque pesanti consumatori di memoria, scommetto che le prestazioni risulteranno molto più tollerabili di quelle attuali.

Ecco: situazioni **sotto il nostro controllo**. È qui che mi suona il campanello. Il consumo della Ram, esperienze come questa alla mano, potrebbe variare molto in relazione all’attenzione che dedichiamo al tema.

Ed è per questo che non voglio tirare conclusioni. Invece, voglio spegnere tutto sul mio MacBook Pro, rivedere con attenzione le cartelle summenzionate, verificare che cosa parte all’avvio e soprattutto perché, per poi accendere un programma per volta e vedere quando arrivo a saturare i miei otto gigabyte di RAM (ho un MacBok Pro del 2009). Lo voglio raccontare e, se qualcuno ha voglia di raccontarmi la propria esperienza, la pubblico volentieri. Voglio arrivare a capire quanto *veramente* avere sedici gigabyte di Ram sia un problema. Tenendo presente che i nuovi MacBook Pro hanno dischi Ssd più prestanti nella categoria e in certi casi persino la paginazione potrebbe essere inavvertibile. Tradotto: in quel momento ove servissero diciotto gigabyte di picco, per dire, potremmo neanche accorgercene.
