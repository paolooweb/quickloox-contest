---
title: "Sostegno al supporto"
date: 2022-02-26T00:58:39+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [MacPaw, The Unarchiver]
---
[MacPaw](https://macpaw.com) produce una serie di app a supporto di chi usa Mac, a vari livelli di profondità e utilità.

Personalmente, per esempio, uso con soddisfazione da molto tempo [The Unarchiver](https://macpaw.com/the-unarchiver).

Oggi è estremamente importante ribadire che l’operatività di MacPaw prosegue; l’infrastruttura e i dati sensibili custoditi dalla società si trovano su Amazon Web Services, in un server posto in un luogo sicuro. Le transazioni sono amministrate da una società del Regno Unito.

Continuare ad avere fiducia nel funzionamento e nel supporto delle app di MacPaw da parte di chi le ha create è una grande occasione per testimoniare un sostegno concreto [in un momento complicato](https://macpaw.com/news/macpaw-amidst-aggression).

**Aggiornamento del 27 febbraio**: anche [Readdle](https://readdle.com/blog/readdle-on-ukraine) si trova a fronteggiare la stessa situazione e promette livelli immutati di attività e servizio. Grazie a [Mario](https://multifinder.wordpress.com).

**Aggiornamento del 28 febbraio**: lo stesso vale per [Skylum](https://skylum.com/blog/act-now-save-ukraine-defend-democracy-24) (grazie a [Sabino](https://melabit.wordpress.com)).