---
title: "Promemoria costante"
date: 2014-11-26
comments: true
tags: [Windows]
---
Ricevo da **Fabio** e pubblico, a memento continuo e imperituro.

>Roma, 23 novembre 2014, ore 13:45, autobus 62 direzione largo Pugliese, monitor informativo Atac.

img ![autobus a Roma](/images/rome.jpg 600  "autobus a Roma")