---
title: "La sentenza in una riga"
date: 2017-11-02
comments: true
tags: [pgrep, Terminale, Gabriele]
---
Occuparsi dei processi di Mac è più facile dal Terminale che da Monitoraggio Attività, posto che uno sappia come fare.

Diciamo che finora mi sono arrangiato, ma si può fare sicuramente meglio in mezzo agli infiniti parametri di configurazione di comandi come `top` e `ps`.

A volte basta una riga, come mi ha insegnato **Gabriele**, che ringrazio, per risparmiarsi tanto lavoro. Per esempio, un problema tipico dell’operare sui processi nel Terminale è recuperare il Pid, l’identificativo numerico del processo stesso, che cambia a ogni lancio.

Sarebbe molto comodo, per esempio volendo terminare un programma bloccato, poterlo identificare per nome invece che per numero. E si può:

`pgrep Mail`

considera subito Mail all’interno dell’intero elenco dei processi. E io sciocco a sudare con `grep` e i *piping*. Sempre per esempio,

`kill -9 \`pgrep Mail\``

giustizia Mail. Pulito, immediato, chiaro.

(Se Markdown facesse pasticci in conversione: *pgrep Mail* deve essere racchiuso tra *backtick*, Opzione-*backslash*.)