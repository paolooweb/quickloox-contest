---
title: "Quando non c’erano i nickname"
date: 2022-07-12T00:09:45+01:00
draft: false
toc: false
comments: true
categories: [Internet]
tags: [Lewis Carroll, Una storia intricata, A Tangled Tale, Stampa Alternativa, Project Gutenberg]
---
Bei tempi quando le persone avevano un nome e un cognome ortodossi e al massimo un nomignolo riconosciuto dalla comunità; oggi questa moda dei *nickname*, degli *avatar*, delle identità fittizie ovunque è un sintomo evidente del degrado. [Su Internet nessuno sa che sei un cane](https://knowyourmeme.com/memes/on-the-internet-nobody-knows-youre-a-dog#fn4) è passato da vignetta divertente a plumbea e deprimente realtà.

O almeno così mi sono sentito raccontare oggi, da una conoscenza occasionale.

A un mercatino balneare dell’usato ho recuperato per cinque euro una copia di *Una storia intricata*, di Lewis Carroll, edito da Stampa Alternativa nel 1998, prezzo originale dodici euro.

Non sono riuscito a trovarlo in vendita in rete. Ho comprato carta, per giunta carta tradotta in italiano, per il piacere immediato della fruizione, senza aspettare di arrivare a casa e scaricare [A Tangled Tale dal Project Gutenberg](https://www.gutenberg.org/files/29042/29042-h/29042-h.htm), dove si trovano i materiali originali del libro, datato 1885.

*A Tangled Tale* è in realtà una raccolta di piccoli racconti contenenti enigmi matematici non troppo difficili (diciamo scuola media), pubblicati a partire dal 1880 su una rivista intitolata *The Monthly Packer*.

Il mese dopo, la rivista pubblicava le risposte ricevute dai lettori e commentate da Carroll. Questi commenti fanno parte del libro e ci permettono di scoprire identità letterarie come *Ragazzo di Marlborough*, *Camminatore di Putney*, *Brezza di Mare*, *Semplice Susan*, *Money Spinner*, *Vecchio Gatto*, *Quercia di Palude*, *Bradshaw del Futuro*, *Crophi and Mophi*, *Cappellaio Matto* (e vabbè), *Stracci e Sfilacci*, *Omega*, *Aspetto il Treno*, *Tizio*, *Sette Anziani*, *Forza Inerte*, *Professoressa che Apprezza*, *Dinah Mite*, *Duckwing* [Ala di anatra], *Euroclidone*, *Magpie* [Gazza ladra], *Nairam* (da leggere a rovescio), *L’Inconnu*, *Tè Pomeridiano*, *Tappezzeria*, *Teseo*, *Bimba*, *Delta*, *Taffy*, *Janet*, *Stella Polare*, *Tokio*, *Zucchero Raffinato* e molti, molti altri.

Perché quelli che si rovinano con gli avatar e i nickname siamo noi, del Ventiduesimo secolo.