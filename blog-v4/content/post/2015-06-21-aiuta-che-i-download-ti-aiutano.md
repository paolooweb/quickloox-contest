---
title: "Aiuta che i download ti aiutano"
date: 2015-06-21
comments: true
tags: [Twitterrific, Hockenberry, Afb, Apple, VoiceOver, Iconfactory]
---
Si parlava di accessibilità con la notizia del [premio assegnato dalla American Foundation for the Blind ad Apple per Voiceover](https://macintelligence.org/posts/2015-06-20-vedo-e-non-vedo/).<!--more-->

I creatori della *app* [Twitterrific](http://twitterrific.com/ios) riferiscono di avere avuto il [picco di download più alto di sempre](http://blog.iconfactory.com/2015/06/a-special-feature/) per la loro creatura nel momento in cui Apple l’ha inserita nella rassegna di *app* compatibili VoiceOver.

Scrive Craig Hockenberry di Iconfactory:

>Non siamo in questo business solo per fare soldi: tutti noi speriamo che i nostri prodotti miglioreranno la vita delle persone. […] Abbiamo lavorato duramente perché Twitterrific funzionasse bene con le funzioni di accessibilità di iOS. Sapere che questi sforzi facilitano le cose ai clienti con disabilità è appagante oltre misura. […] Ma c’è un altro incentivo a pensare all’accessibilità: aiutare gli altri aiuta anche i download. […] Apple rimane impegnata a rendere accessibili i propri prodotti. Gli sviluppatori intelligenti ne seguiranno la strada.

Non c’è altro da dire. A parte magari adottare Twitterrific.