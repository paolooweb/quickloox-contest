---
title: "Vivi che camminano"
date: 2014-11-02
comments: true
tags: [iPad2, iPad, iOS8, iPodtouch, iPhone, A9, A5]
---
Era se non sbaglio il 2012 quando uscì la terza generazione di iPad (cui si devono queste note, affidate a [Textastic](https://itunes.apple.com/it/app/textastic-code-editor-for/id383577124?l=en&mt=8)) che venne seguita dopo pochissimi mesi da un nuovo modello, che eccelleva nel guadagno in prestazioni là dove la terza generazione suddetta si “limitava” a sostenere il lavoro sul primo schermo Retina a sostanziale parità di potenza con iPad 2.<!--more-->

A parte le critiche di chi aveva acquistato un iPad per vederselo deprezzato dopo pochi mesi (e che a posteriori non capisco; l’iPad che ho sotto le dita sarà deprezzatissimo, ma si è ripagato come minimo cinque o sei volte, non sbaglia un colpo e per me casomai è senza prezzo), ci fu chi scrisse di un problema per gli sviluppatori, che dovendo sviluppare per gli iPad improvvisamente rimasti indietro come prestazioni si sarebbero trovati in difficoltà con quelli superveloci.

In un certo senso è accaduto, nel verso contrario: scrivere giochi che spremono il processore fino al’ultimo è problematico perché [anche il recentissimo iOS 8 continua a riconoscere la Cpu](http://www.allenpike.com/2014/the-ipad-zombie/) di iPad, iPod Touch e iPhone sul mercato da tre anni. E i programmatori non hanno un modo per escludere i processori fuori moda dalla compatibilità di un gioco, se quei processori sono ancora ufficialmente riconosciuti dalla versione più recente del sistema operativo.

Si leggono articoli su Apple che progetta l’obsolescenza dei suoi sistemi, alimenta un consumismo deteriore spingendo le persone a cambiare modello ogni anno, manca di considerazione per i sistemi vecchi e di chi si trova ancora bene con una vecchia macchina. Poi parla uno sviluppatore, uno che ci lavora davvero, e scrive così:

>La sola cosa che come sviluppatori possiamo fare per escludere il supporto di un apparecchio è richiedere una versione di iOS che non ci funziona sopra. Sfortunatamente è abbastanza sicuro che Apple continuerà a considerare il processore A5 [di iPad 2] in iOS 9. Se succede, non avremo modo di tagliare fuori vecchi iPad mini e iPod touch fino a quando non ci sarà una ampia diffusione di iOS 10, presumibilmente verso l’inizio del 2017.

Scrivere un gioco che sfrutta al massimo il processore A9 di iPad Air 2 e non si blocca orribilmente sull’A5 iPad 2 o iPad mini originale è difficilissimo. Fino a che iOS aggiornato riconosce tutti e due i processori, più quelli in mezzo, gli sviluppatori non hanno soluzioni.

Vorrei sentire qualcuno del partito dell’obsolescenza programmata, commentare su un processore la cui vita utile con il sistema operativo più aggiornato va dal 2011 al 2017, montato non su un Mac da lasciare in eredità ai figli, ma su un effimero iPad mini regolarmente in vendita a duecentocinquanta euro.