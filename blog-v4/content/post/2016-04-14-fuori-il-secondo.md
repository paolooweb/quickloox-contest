---
title: "Fuori il secondo"
date: 2016-04-14
comments: true
tags: [IE, Explorer, Safari, Chrome, USA, analytics]
---
Gli strumenti di analisi del traffico web del governo americano, [già citati](https://macintelligence.org/posts/2016-02-15-tutti-i-browser-del-presidente/) in passato, ci aiutano a capire e visualizzare quali siano i reali rapporti di forza tra le varie piattaforme software e web.<!--more-->

È facile entrare in azienda tutti i giorni e convincersi che Windows sia predominante, oppure fare ingresso a scuola e credere che nel 1996 il mondo abbia smesso di produrre computer, oppure curiosare in banca e illudersi che Internet Explorer sia l’unico browser che esiste.

È questione di misurare secondo le categorie di questo secolo e non quelle di quello precedente. Per esempio, negli ultimi 90 giorni, su due miliardi abbondanti di visite (due miliardi) ai siti dell’amministrazione Usa, [il *browser* più usato è Chrome](http://www.appleworld.today/blog/2016/4/13/apples-safari-is-the-second-most-popular-web-browser), con il 43,1 percento (fonte: [Digital Analytics Program](https://www.digitalgov.gov/services/dap/)).

Il secondo in classifica è, attenzione attenzione, Safari con il 21,9 percento. Internet Explorer è solo terzo, tutte le versioni contate insieme, con il 20,1 percento.

Ma come? Safari funziona solo sui Mac, dice subito. Non proprio: il codice di Safari funziona su Mac e su iOS. Una parte consistente di quelle visite è stata compiuta dal Safari di un iPhone o un iPad.

Solo che viene spontaneo a molti argomentare che questi ultimi non siano computer. Mentre scommetto che abbiano più difficoltà a sostenere che Safari non sia Safari se lanciato da un iPhone.

Contando per *browser*, trascurando l’hardware sottostante, Internet Explorer è assai meno importante di come lo fa sembrare la banca o il preside che non vuole aggiornare il parco informatico dell’istituto.

Negli Stati Uniti, certo, in un Paese dove il governo fa sapere che cosa succede sui propri siti.