---
title: "Il filesystem di Asimov"
date: 2021-10-03T16:27:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [Obsidian, Miro, Canva, Google Documenti, GitHub, OpenZFS] 
---
Nel mio [gruppo Slack](http://goedel.slack.com) (dove è possibile entrare facendone richiesta) si è discusso per merito di **MailMasterC** del perché i giovani d’oggi [non siano familiari con le opzioni di file, cartelle e alberi gerarchici](https://www.theverge.com/22684730/students-file-folder-directory-structure-education-gen-z) mentre ai miei tempi, signora mia, le informazioni si organizzavano come si deve, con tutte le sottocartelle e le alberature che servivano.

Il tema è meno dopolavoristico di come appare e crea questioni significative, per esempio, in ambito didattico. È anche facile immaginare come conti in ambiti più evidenti come aziende di informatica o aziende *tout court*, nelle quali sapere dove stanno i file cruciali è spesso faccenda impellente.

Da tempo ho personalmente rinunciato all’organizzazione di file e cartelle e ho dato preferenza a un approccio più disordinato, basato su tag, ricerca e – quando occorre – *smart folder* che si popolano da sole.

Si è parlato anche di come il lavoro per app, affermato da filosofie come quella di iOS e iPadOS, incoraggi a perdere di vista la strutturazione dello spazio di lavoro, nel momento in cui apri la app e si aprono i file su cui sei al lavoro.

Ho faticato a contribuire alla discussione mentre era viva e provo a dare un parere oggi, meno puntuale e più buttato avanti dove ancora facciamo fatica a vedere.

Ed è questo: manca un filesystem universale. Qualcosa che uno scrittore di fantascienza come Asimov potrebbe avere concepito se fosse nato venti anni dopo e avesse avuto più familiarità con la rete.

Quando il numero di utenti, di app, di file cresce spropositatamente, come al tempo attuale, la strutturazione gerarchica perde di significato.

*Ma i miei file…* appunto, sono *i tuoi*. Quando eravamo pochi, con pochi programmi e pochi documenti (in senso relativo: oggi migliaia di documenti sono *pochi*), era facile e opportuno che ognuno tenesse ordinato il proprio giardino, per così dire. E i giardini restavano a distanza; tra essi si svolgeva un traffico discreto ed esterno di messaggi di posta, floppy disk o Cd-Rom.

Se oggi guardo il mio giardino, il numero di aiuole si è moltiplicato. Senza contare la famiglia e meno che mai la famiglia allargata, ho non meno di tre computer usati in continuazione, che si sincronizzano, si passano cose, si sovrappongono in parte. un file può essere creato su iPhone, passare da iPad, finire su Mac.

Le aiuole virtuali le vogliamo contare? iCloud Drive, Dropbox e poi tutto il resto. Quei file stanno sul mio computer, *sui miei* computer, o altrove, o tutto insieme? o ancora qualcosa di diverso? Il mio giardinetto somiglia a una giungla più che a una aiuola ben organizzata.

E poi arrivano gli altri, quelli che un tempo potevano al massimo bussare con un floppy o una email. Devo tenere conto di file che esistono solo su [Miro](https://miro.com/app/), [Canva](https://www.canva.com), [Prezi](https://prezi.com), [Google Documenti](https://docs.google.com/) per fare solo pochi nomi. File di cui posso tenere localmente al massimo un backup, perché sono giardini condivisi, dove in più di una persona si lavora sulla stessa aiuola.

In situazioni come [GitHub](https://github.com), capita che il file stia sì in locale, ma che io debba costantemente preoccuparmi di aggiornarlo con il lavoro degli altri che stanno altrove, con una copia su ciascun locale loro, e che tocchi anche a me proiettare aggiornamenti verso la comunità. Dove si trovi il file dentro il mio computer è una cosa profondamente secondaria rispetto alle attività che mi richiede. E se domani mi venisse l’uzzolo di cambiargli posto nel computer, o di cambiargli nome, o di mettere quella cartella dentro una nuova cartella, dovrei stare molto bene attento a non fare disastri.

Dove sta un file che ho creato con Canva, a cui devono accedere colleghi? Sta su Canva. Nella mia perfetta struttura di cartelle e sottocartelle potrei mettere al massimo un segnalibro per lanciare Safari e caricare il giusto Url.

Insomma, non c’è niente di male nelle organizzazioni personali del filesystem. Solo che funzionano meglio con pochi file, con pochi scambi di file, con pochi tipi di file eccetera. Più tutte queste quantità crescono e si diversificano, più il concetto entra in crisi, più arriva la tentazione di saltare a pié pari la cosa. C’è gente oggi che lavora, nel senso pieno e professionale, e non ha mai visto un Mac, o un PC se per quello. Se non ha lo stimolo a crearsi una struttura perfettamente ordinata di sottocartelle, lo capisco.

Diverso sarebbe se operassimo in un fantascientifico filesystem universale, un [OpenZFS](https://openzfs.org/wiki/Main_Page) del Tremila per capirci. Dove il mio giardino è una piccola parte del grande giardino globale. Dove, ovviamente con i permessi e le sicurezze opportune, passare un file a un amico significherebbe aprire la mia cartella e passarlo nella sua. Senza app di trasmissione, con protocolli trasparenti, la fine dell’idea stessa di download o upload, quando un file semplicemente si sposta da una parte all’altra del filesystem come oggi faremmo noi su Mac, solo che l’altra parte del filesystem può trovarsi in Terra del Fuoco. E tutto, tutto, dal file sul mio disco rigido a quello che ho appena creato su Miro, stesse nella stessa struttura di file e cartelle.

Allora le cartelle, i percorsi, gli alberi, avrebbero (di nuovo) un senso.

Ora come ora, ho appena ricevuto una foto dalla famiglia. È arrivata con Messaggi e ho fatto clic su Salva. Se apro Foto la vedo come più recente, mentre silenziosamente la foto si propaga su iPhone e iPad Pro.

Dove sta? Non lo so. Non ne ho la minima idea. Quello che mi interessa è vederla subito.

Non è un caso che su Mac possa trovare software come [Obsidian](https://obsidian.md), *una potente base di conoscenza assisa su una cartella locale di file scritti in Markdown*. La cartella qui esiste solo per dargli un confine e poi ci pensa lui.

In attesa del genio che ci porterà il filesystem universale e che probabilmente sta ancora succhiando il biberon in qualche sobborgo del villaggio globale.

*Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile.*