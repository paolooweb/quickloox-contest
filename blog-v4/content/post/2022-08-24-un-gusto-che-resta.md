---
title: "Un gusto che resta"
date: 2022-08-24T01:20:33+01:00
draft: false
toc: false
comments: true
categories: [Internet]
tags: [elezioni]
---
Avevo la sensazione di ripetermi e così ho cancellato il post. Al suo posto, ripropongo [quello che ho scritto due anni fa](https://macintelligence.org/posts/2020-08-24-per-il-gusto-di-chattare/) sull’anniversario di Internet Relay Chat e che continuo a sottoscrivere in ogni virgola.

Semmai, con enfasi ancora maggiore. C’è ancora più bisogno di quell’attitudine e di quello spirito nell’evoluzione tecnologica.