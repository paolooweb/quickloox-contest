---
title: "A letto con la complicazione"
date: 2018-07-11
comments: true
tags: [Wwdc, iOS, Swift, Apple]
---
Come tutti gli anni, Apple ha fatto cosa buona e giusta e reso disponibili a chiunque i [video – ricercabili – delle sessioni tecniche di Wwdc](https://developer.apple.com/videos/wwdc2018/).

Sono cose dirette principalmente agli sviluppatori, quindi molto tecniche. Eppure a scorrere la lista si trova certamente qualcosa che vale la pena di leggere e invito chiunque ad addomentarsi con qualcosa di almeno vago interesse.

Mi guarderò [Swift Generics](https://developer.apple.com/videos/play/wwdc2018/406/) perché è un concetto che sicuramente meritava più spazio sul mio [libriccino](http://www.apogeonline.com/libri/9788850333387/scheda). (*Disclaimer**: il libriccino rappresenta a oggi il modo più veloce e semplice di avvicinarsi a Swift fuori da [Swift Playgrounds](https://www.apple.com/swift/playgrounds/), però è fermo a una versione vecchia del linguaggio. Il novantanove percento di quello che si legge vale anche quando si fa il salto verso la versione aggiornata di Swift; il linguaggio si è tuttavia straordinariamente evoluto dal tempo della pubblicazione).

Qualche esempio? [Understanding Crashes and Crash Logs](https://developer.apple.com/videos/play/wwdc2018/414/), risposta alla domanda *come faccio a capire che cosa non va?*. [iOS Memory Deep Dive](https://developer.apple.com/videos/play/wwdc2018/416/), come funziona l’amministrazione della memoria dentro iOS. [Advances in Research and Care Frameworks](https://developer.apple.com/videos/play/wwdc2018/205/), aggiornamento sulle architetture di programmazione ResearchKit e CareKit sotto il cofano delle iniziative che mirano a usare iPhone e iPad per aiutare la ricerca e la medicina.

In verità ci sono anche argomenti più semplici, come la serie *What’s New*: in Safari, in tvOS eccetera. Raccomando di lasciarsi piuttosto tentare dalla complessità. Metti anche che non si capisca proprio tutto, ma una cosa rimanga: è già importante.