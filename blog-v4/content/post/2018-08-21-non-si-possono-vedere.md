---
title: "Non si possono vedere"
date: 2018-08-21
comments: true
tags: [Huawei, iPhone, Ars, Technica]
---
Se guardi la pubblicità, vedi che uno Huawei scatta foto più belle di quelle di un iPhone e costa pure meno.

Se leggi *Ars Technica* vedi che le foto sono state scattate [con una fotocamera professionale](https://arstechnica.com/gadgets/2018/08/huawei-was-caught-using-a-pro-camera-to-fake-smartphone-photos-again/).

La qualità autentica dello smartphone, relativamente alla parte fotografica? Non si può vedere. Alla lettera, chiaro.
