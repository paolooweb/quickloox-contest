---
title: "La settimana di Alan"
date: 2023-06-30T23:00:06+01:00
draft: false
toc: false
comments: true
categories: [Internet]
tags: [Copernicani, Turing, Alan Turing, chatbot, intelligenza artificiale, ai, settimana enigmistica, Hinton, Geoffrey Hinton]
---
Sulla mailing list dei [Copernicani](https://copernicani.it) (iscriviti!) è tornata l’idea che le attuali intelligenze artificiali abbiano l’*understanding*, capiscano; e inoltre, che l’idea delle frasi composte come sequenze di estrazioni probabilistiche pesate a partire da una base dati addestrata sia un meccanismo analogo a come funziona il cervello umano.

In particolare viene citato [Geoffrey Hinton](https://www.cs.toronto.edu/~hinton/) con questa frase, che non sono riuscito a ritrovare in rete:

>it would be great if we could convince people that they are not just stochastic parrots.

*Sarebbe grande se potessimo convincere la gente che [i sistemi basati su modelli linguistici di grandi dimensioni] non sono solo pappagalli stocastici*.

Faccio fatica a capire che bisogno ci sia di convincere le persone; se un sistema comprende, basta guardarlo in funzione per constatarlo.

Ho compiuto il mio consueto esperimento. Ho chiesto a ChatGPT:

*Completa la parola con dentro gli asterischi inserendo le lettere corrette al posto degli asterischi: +s+e+is+o*

(Qui ho messo dei segni *+* al posto degli asterischi per non provocare problemi di resa di Markdown).

ChatGPT ha risposto:

*Assessore*

Che è visibilmente sbagliato in almeno due modi. Ne ho segnalato uno:

*Assessore non ha la “i”*

ChatGPT mi ha replicato:

*Mi scuso per l'errore. La parola corretta potrebbe essere "assessore”.*

Nonostante l’avere formalmente riconosciuto di avere sbagliato, il sistema ha sbagliato di nuovo e ha commesso ancora una volta l’errore che avevo segnalato. Ha cioè di fatto ignorato la mia segnalazione.

Nello sbagliare, ha ripetuto la medesima parola di prima. Perché non sa di averla già usata, non sa che gli ho fatto notare l’errore relativo al suo uso e non sa che è sbagliata.

Si noti che nel porre il quesito avevo alluso alla soluzione. Ma ChatGPT non sa che ho usato una parola molto vicina a quella che chiedevo e non sa confrontarla con l’enigma che gli ho sottoposto.

Questa, per Hinton e altri, sarebbe comprensione. Per me è la sua mancanza.

Nel ragionare ozioso sull’episodio ho avuto una illuminazione nazionalpopolare. Sono allo studio test per valutare l’intelligenza effettiva delle presunte intelligenze artificiali. Cose serie, scientifiche, in collaudo e in sviluppo da anni.

Nel frattempo, avanzo la seguente proposta: *il nuovo test di Turing sia la soluzione di un congruo numero di enigmi differenti per tipologia e complessità tratti da un numero qualsiasi de* [La Settimana Enigmistica](https://www.lasettimanaenigmistica.com/).

Secondo me, una sedicente intelligenza artificiale capace di risolvere cruciverba, crittografie, rebus, sciarade, calcoli enigmatici, bersagli, parole crociate senza schema, quesiti con la Susi, indovinelli e palindromi ha il potenziale per passare il test di Turing e dobbiamo certamente riconoscergli le caratteristiche dell’intelligenza quella vera.

Se c’è un milionario in ascolto, mettiamo in palio un premio, che so, di centomila euro per il software in grado di emulare con successo un umano fino alla mitica pagina 46 della rivista. Visibilità globale, attenzione mediatica che vale molto più della cifra e metti mai che arrivi davvero qualcuno con una risposta valida. Il che sarebbe entrare nei libri di storia.

Sono entrato nel sito della *Settimana* e ho caricato il primo cruciverba a disposizione. Prima definizione: *Sono pari nell’esame*, due lettere.

ChatGPT ha risposto:

*La risposta corretta potrebbe essere "NO". La parola "NO" ha due lettere ed è un'abbreviazione di "numero", che indica una quantità pari nell'esame.*

(Ora è possible [linkare una chat](https://chat.openai.com/share/8192580d-0884-4286-b15a-3918fd67b48b) e far vedere input e output in modo semplice).

Chi la chiama comprensione. Io la chiamo disagio.

Post Scriptum: nel link si vedono tutte le definizioni del [cruciverba](https://www.lasettimanaenigmistica.com/gioca-con-noi/parole-crociate/STD_Small/41184/) (occorre registrarsi) che ho posto a ChatGPT. In totale gliene ho poste ventiquattro, tutte quelle che sono servite a me per risolvere lo schema. In otto casi ha dato la risposta esatta. Poi ha servito quattordici risposte sbagliate. In un caso extra ha risposto *steppe* al posto di *steppa*, come avrebbe dovuto fare se avesse compreso la definizione *vasta prateria russa*. Però potrebbe essere un errore di traduzione. Nel secondo caso a sé ha scritto *carriato* al posto di *cariato* e sosteneva che fosse lungo sette lettere. Anche qui potrebbe essere una pessima traduzione interna in italiano.

Complessivamente ha fatto meglio delle mie previsioni.

Se togliamo le definizioni più facili perché parzialmente complete, il lavoro migliore lo ha svolto su *la quarta nota* con la risposta *fa*. Ha anche completato *De Janeiro* con *Rio*.

L’errore forse peggiore è stato su *il regno nelle fiabe*. La soluzione era *reame*, ma lui ha proposto *favol*. Allo *sportivo del remo*, otto lettere, partiva già con *cano* ed è riuscito a mettere lì *canottiere* invece di *canoista*.

Dove c’era *aspirano la c* e si partiva con *fi*, dieci lettere, ha proposto *Filtriaria*, un verme parassita. Mi scuso con i fiorentini.

Post Post Scriptum: Il link riporta solo la definizione presente quando ho linkato e non tutto il resto della conversazione. Chiedo scusa.