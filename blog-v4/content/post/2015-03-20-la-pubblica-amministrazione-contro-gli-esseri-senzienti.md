---
title: "La pubblica amministrazione contro gli esseri senzienti"
date: 2015-03-20
comments: true
tags: [Apogeonline, PA]
---
Lo faccio di rado, solo quando merita. Il mio [pezzo di oggi su Apogeonline](http://www.apogeonline.com/webzine/2015/03/20/il-fu-fattore-umano) merita, non perché sia bello o geniale ma perché riguarda tutti, anche chi non emette fatture. C’è di mezzo un modo di pensare e intendere il servizio pubblico.

Chiedo scusa per l’autoreferenzialità. Quando ci vuole, ci vuole.