---
title: "Istinto e ironia"
date: 2022-05-14T23:50:44+01:00
draft: false
toc: false
comments: true
categories: [Internet]
tags: [Eurovision, Playdate]
---
Oggi c’è stato il primo vero caldo e con il vero caldo una parte di me torna diciottenne, quella che perde tempo, si guarda in giro e dà retta solo all’istinto. Perché a diciotto anni l’istinto ti fa fare cose che sono giuste anche quando sono sbagliate; per il resto sei uno sciocco che sopravvaluta clamorosamente la sua comprensione del mondo. Non che adesso, eh; però, qualche multiplo dopo, miei amici qualcosa del mondo lo hanno capito, almeno loro. A diciotto anni, neanche per errore.

È per questo che invece di scrivere un post ho solo voglia di bighellonare nel sole, di giorno, e in rete, di notte. Vado a rivedere [il sito di Playdate](https://play.date) non perché ci siano novità – so perfettamente che non ce ne sono dopo quelle [annunciate a suo tempo](https://macintelligence.org/posts/2022-04-23-appuntamento-con-il-panico/) – ma per oziare sulla pagina; tutti cianciano del metaverso ma per il momento il sito di Playdate è l’unico che sappia usare la realtà aumentata, nel modo in cui si deve.

Poi passo su Facebook, non dovrei farlo, ma l’istinto. Una voce-fuori-dal-coro-controcorrente-solo-io-conosco-la-verità se la prende con un concorso canoro europeo per prendersela con le vittime della guerra e per fare del cinismo a basso costo, tipo *tutti che fino a un momento fa parlavano della guerra adesso sono dietro alle canzonette*. La classica superiorità esibita del frustrato, che fa tanto social.

Un post dopo, voce-fuori-dal-coro riesce a collegare la guerra con i vaccini e devo dire che era sopravvissuta alle mie pulizie di primavera perché è parente di un amico, perché a volte diventa donchisciottesca, perché a volte nonostante tutto ha qualche ragione da vendere. Oggi, però, sai com’è, l’istinto. La segnalo e la tolgo dalle amicizie, non la leggerò mai più e mi sento di una leggerezza inaudita.

La voglia di scrivere è notevole ma quella di concentrarsi zero, quindi passo su Twitter. La timeline è dominata da un concorso canoro europeo. Quello.

Risalgo la cronologia verso il tempo presente e scopro che il concorso canoro è stato vinto da una nazione alle prese con la guerra. Quella. E che i voti provenienti da tutta Europa sono un mezzo plebiscito. E che un’altra nazione in guerra è nervosa per l’esito, e aveva gruppi di hacker a cercare di complicare la visione dell’evento agli spettatori del mondo libero.

Ironia a secchiate, un momento che neanche i Monty Python. Scatto verso Facebook per fare del sarcasmo verso voce-fuori-dal-coro e farle pesare quanto non abbia capito e come le canzonette siano state questa sera un momento politico e storico unico.

Ma non posso. Ho eliminato l’amicizia. Non la leggerò mai più, mica posso pretendere di scriverle.

Se mi fossi abbassato a fare davvero quel sarcasmo avrei perso tempo, perso stile, perso anche se avessi vinto. Invece, salvato dall’istinto, contemplo immobile e compiaciuto l’ironia della situazione. Un’ironia monumentale, che scalda il cuore e rimfresca la mente. Un’ironia che voce-fuori-dal-coro non potrà mai apprezzare e me ne dispiace per lei, purtroppo non vedo margini di recupero.

Continuo a non avere un post, ma per stasera può andare bene anche così. Me lo dice l’istinto e probabilmente, per stasera, è giusto anche se è sbagliato.