---
title: "Nuove connessioni"
date: 2021-02-03T01:59:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [NetNewsWire, Mac, AirTable, Feedly, Rss] 
---
Al termine della giornata di lavoro più lunga del 2021 mi sono comunque preso due minime soddisfazioni: ho avviato un lavoro su [AirTable](https://airtable.com/), che sembra promettente, e siccome [NetNewsWire](https://netnewswire.com) su Mac dopo un aggiornamento recente accetta account [Feedly](https://feedly.com), mi trovo con gli Rss perfettamente sincronizzati su tutti i miei sistemi per quella che al momento è la mia configurazione preferita.

Non sono esattamente le gratificazioni che segnano una vita (ci sono sempre i commenti da sistemare qui sotto), però è qualcosa di imparato più qualcosa di progredito, e non sono ancora le due. Andassero così tutte le giornate.

*Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile.*