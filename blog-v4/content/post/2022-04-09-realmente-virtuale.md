---
title: "Realmente virtuale"
date: 2022-04-09T02:48:05+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [Apple Silicon, macOS]
---
Non ho ancora capito se si possa lavorare seriamente con l’esempio pubblicato nelle pagine per gli sviluppatori Apple che desiderano [eseguire macOS dentro una macchina virtuale su Apple Silicon](https://developer.apple.com/documentation/virtualization/running_macos_in_a_virtual_machine_on_apple_silicon_macs).

Perché è interessante non poco e abbastanza impegnativo da comprendere.

Qualcuno si è già cimentato?