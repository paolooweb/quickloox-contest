---
title: "Il Signore degli Script"
date: 2020-12-04
comments: true
tags: [MelaBit, R, Cnr]
---
Della trilogia dedicata da MelaBit al Cnr, decisamente con accenti tolkieniani quando si parla della lotta tra Bene e la Burocr… il Male, a me piace tantissimo e soprattutto il capitolo sull’[estrazione del testo da Pdf](https://melabit.wordpress.com/2020/12/01/il-cnr-e-anche-questo-un-po-di-codice/).

Mostra come veramente il computer possa diventare un amplificatore di intelligenza, se decidiamo di investirne un pezzo di nostra invece di limitarci a usare cose fatte da altri, per quanto geniali.

Gli altri due capitoli mostrano altrettanti modi possibili di buttarla via l’intelligenza, e dell’ingegno che va messo per fare comunque il proprio dovere seppure in un ambito dove il pensiero dominante va in direzioni differenti. Fanno da contraltare eccellente all’uso creativo e puntuale di [R](https://www.r-project.org) e [RStudio Desktop](https://rstudio.com), con una spruzzata di [awk](https://www.gnu.org/software/gawk/manual/gawk.html) e [LaTeX](https://www.latex-project.org). Da leggere e studiare, per non diventare preda delle Forze Oscure che abitano empi uffici di enti pubblici.