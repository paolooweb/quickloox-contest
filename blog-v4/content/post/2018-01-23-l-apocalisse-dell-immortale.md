---
title: "L’Apocalisse dell’immortale"
date: 2018-01-23
comments: true
tags: [Knuth, Fantasia, Apocalyptica, Taocp]
---
L’[anticipazione di settembre](https://macintelligence.org/posts/2017-09-22-dall-algoritmo-al-ritmo/) è stata confermata: Donald Knuth [ha festeggiato i suoi ottant’anni in Svezia](https://translate.google.se/translate?hl=sv&sl=sv&tl=en&u=https%3A%2F%2Fwww.dn.se%2Fnyheter%2Fvetenskap%2Fvarldens-framsta-datavetare-donald-knuth-firade-80-arsdagen-i-pitea%2F) con un [simposio a lui dedicato](http://knuth80.elfbrink.se) e l’esecuzione della sua [Fantasia Apocalyptica](https://www-cs-faculty.stanford.edu/~knuth/fant.html) per organo a canne e schermi, ispirata all’Apocalisse di Giovanni.

(Se Safari lancia un allarme sicurezza per quest’ultimo link, è per eccesso di zelo).

Nel frattempo, Knuth [continua nella stesura](https://www-cs-faculty.stanford.edu/~knuth/news.html) (altro link pulito che potrebbe suscitare sospetti in Safari) della [Art of Computer Programming](https://cs.stanford.edu/~knuth/taocp.html) . Un lavoretto da nulla, iniziato cinquantasei anni fa, che gli varrà l’immortalità del ricordo.