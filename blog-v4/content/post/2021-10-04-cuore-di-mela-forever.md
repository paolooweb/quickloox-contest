---
title: "Cuore di Mela forever"
date: 2021-10-04T17:15:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [Cuore di Mela, Luca Accomazzi, Sabino Maggi] 
---
**Sabino** è stato troppo gentile a [riscoprire Cuore di Mela](https://melabit.wordpress.com/2021/10/04/cuore-di-mela/) e trovarlo per tanti versi ancora fresco e utile.

Non lo riprendo per caldeggiare l’acquisto (non lo linko, chi avesse voglia lo troverà); il libro fa benissimo il suo lavoro e ancora oggi porta ogni anno numerosi centesimi di euro nelle tasche di Luca *MisterAkko* Accomazzi e nelle mie.

Solo per dire che ci avevamo visto anche abbastanza lungo sul tipo di direzione da dare alla divulgazione informatica in questi anni non più pionieri. E, per quanto cambi tutto in continuazione a frequenza impressionante, certi fondamentali (e certe burocrazie) restano immobili e immortali.

*Cuore di Mela* cartaceo non è in vendita; era nato per omaggiare degnamente le persone così gentili e generose da supportarci nel lavoro di creazione e scrittura. Ne abbiamo tirate un discreto numero, ma restano come rarità e come omaggio.

Le copie elettroniche rimangono e sarebbe bello riuscire a produrre una edizione aggiornata. Ci sarebbe molto da aggiungere, qualcosa da cambiare, di sicuro non mancherebbero nuovi punti di interesse da trattare. Certamente farò del mio meglio per sistemare quantomeno i refusi, a fronte di una lista degli errori. Di fronte a una lista di proposte non prometto niente; ugualmente, nulla è impossibile dato che abbia tempo sufficiente.

Sul gioco proposto da Sabino a fine post, non posso pronunciarmi ovviamente prima che arrivino tutte le risposte e l’interesse che si merita.

*Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile.*