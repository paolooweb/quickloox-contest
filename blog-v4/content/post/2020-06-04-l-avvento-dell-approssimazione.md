---
title: "L'avvento dell’approssimazione"
date: 2020-06-04
comments: true
tags: [Prezi, Mailchimp, Canva, Roll20]
---
Esiste una qualche attività digitale per la quale non sia pronta una _web application_ sporca, maledetta e subito, gratuita o quasi gratuita, con una interfaccia il più possibile delirante e perfetta per eseguire in fretta un lavoro approssimato? No, non c’è. O almeno non me ne sono accorto.

Capisco che il browser sia una tentazione irresistibile per risparmiare sullo sviluppo, ci sono ottime ragioni lato sviluppatore per fare cose che funzionano lì dentro invece che in una app. Poi però è come comprare Android; tutto quello che hai risparmiato tornerà piano piano sotto forma di scomodità e, appunto, approssimazione.

D’accordo con la web application. Solo che bisogna avere un’idea del significato di *esperienza utente*. Ho visto cursori testo spessi come un alberello, quando dovrebbero inserirsi snelli tra un carattere e un altro; ho visto rotelle di progressione girare all’infinito mentre il browser fa finta di salvare qualche dato da qualche parte; ho visto riquadri di selezione che sfarfallano, si ridisegnano in continuazione, si ridimensionano con intervalli arbitrari, si comportano come pare a loro.

A un certo punto l’autore della web application si mette a fare la app. Mai che la app facesse esattamente quello che fa la web application. Lo scenario più comune è che manchino comandi e funzioni. Oppure che si comportino in maniera diversa; fantastico [Mailchimp](https://mailchimp.com) che via browser ha un bug ben noto sul trattamento dell’ora legale e via app invece non lo ha. Oppure che si comportino in modo diverso a seconda del browser.

Il punto che lega tutto è proprio l’approssimazione. Il posizionamento di un riquadro di testo è un tanto al chilo, i righelli sono pietose menzogne, le scorciatoie da tastiera funzionano ma non sono tutte quelle che servono oppure sono tutte ma non funzionano. Per avere lavoro preciso bisogna ricorrere a qualcos’altro. Su iPad fai metà lavoro nel browser, metà nella app, alla ricerca delle cose che funzionano lì e però non là, così come il viceversa.

È il nuovo mondo, ok, meglio tenersi questo dove le app per fare le cose fioccano, in confronto a quello prima. Ma quando e come agli sviluppatori è apparso che potevano erogare qualità approssimativa e comunque prosperare?