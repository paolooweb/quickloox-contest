---
title: "Cose che stridono"
date: 2019-07-19
comments: true
tags: [Mac, Franklin, Austin, Mann]
---
Mi arriva dalla *newsletter* mensile del grande fotografo [Austin Mann](https://macintelligence.org/posts/2018-09-26-mannaggia-al-fotografo/):

>The bitterness of poor quality remains long after the sweetness of low price is forgotten." 
- Benjamin Franklin

*L’amaro della scarsa qualità rimane per molto tempo dopo avere dimenticato il dolce del basso prezzo*.

Neanche la verifico, è talmente bella che se non fosse di Franklin andrebbe comunque diffusa come invenzione di Mann stesso.

Il prezzo cosiddetto alto che paga chi compra Apple è dover sentire lo stridore di chi ha speso meno e non lo ammetterà mai, ma lo tradisce ogni giorno.
