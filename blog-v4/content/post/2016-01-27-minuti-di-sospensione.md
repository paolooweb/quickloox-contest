---
title: "Minuti di sospensione"
date: 2016-01-27
comments: true
tags: [Surface, Microsoft, Patriots, Broncos]
---
Mi immagino la redazione di *Apple World Today* a fare sforzi disumani per controllarsi nel pubblicare [l’articolo](http://www.appleworld.today/blog/2016/1/25/patriots-fans-blame-microsoft) e intanto slogarsi la mascella dalle risate mentre nessuno li vede.<!--more-->

Il campionato di *football* americano si avvicina alla conclusione. Il campionato nel quale Microsoft ha iniettato quattrocento milioni di ragioni per fare pubblicità smaccata alle tavolette Surface, [con risultati altalenanti](https://macintelligence.org/posts/2015-09-22-basta-pagare/) per dirla con garbo.

I New England Patriots hanno perso la *chance* di rigiocarsi il titolo, [sconfitti dai Broncos di Denver](http://www.nfl.com/news/story/0ap3000000628291/article/brady-gronk-lost-their-connection-on-final-2point-try). La notizia è che nel corso del secondo dei quattro tempi di gioco, i Surface in dotazione ai Patriots [hanno smesso di funzionare per venti minuti](http://www.engadget.com/2016/01/25/patriots-surface-tablets-break-down-during-afc-title-match/).

Microsoft ha successivamente incolpato la rete dello stadio, che peraltro – sottolineano perfidi su *Apple World Today* – ha servito alla perfezione i Surface usati dai Broncos.

Dalla lettura emerge che le tavolette hanno un ruolo marginale perché le squadre possono usarle solo per rivedere la loro disposizione in campo in azioni già giocate. Quando però ti stai giocando la partecipazione al Super Bowl, sei sotto pressione e sei un professionista milionario, vuoi che funzioni tutto a perfezione. E se perdi per due punti all’ultimissima azione, ti arrabbi anche per i dettagli minuti.

Geoff Schwarz dei New York Giants ha così [sintetizzato la situazione](https://twitter.com/geoffschwartz/status/691365722170212352) dal punto di vista di un giocatore. In modo conclusivo.

<blockquote class="twitter-tweet" lang="en"><p lang="en" dir="ltr">Those tablets always malfunction.</p>&mdash; Geoff Schwartz (@geoffschwartz) <a href="https://twitter.com/geoffschwartz/status/691365722170212352">January 24, 2016</a></blockquote> <script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>