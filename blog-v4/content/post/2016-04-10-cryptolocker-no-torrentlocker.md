---
title: "CryptoLocker, no TorrentLocker" 
author: Sabino Maggi
date: 2016-04-10
tags: [software, cryptolocker, ransomware, torrentlocker]
comments: true
---
*Ho l’onore massimo di ospitare un* guest post *di **Sabino**, che ringrazio di cuore e a cui lascio la parola.*<!--more-->

Giorni fa (parecchi giorni fa, ormai!) ho promesso di [raccontare il mio incontro con CryptoLocker](https://macintelligence.org/posts/2016-03-08-famolo-deduplicato/), il primo *ransomware* con cui ho avuto a che fare. Il racconto dettagliato di questa esperienza esce oggi in contemporanea – un po’ per gioco, un po’ per vedere l’effetto che fa – sia su *QuickLoox* che su [Melabit](https://melabit.wordpress.com/2016/04/10/cryptolocker-no-torrentlocker/).

Non ho potuto far molto per evitare l’infezione, a parte reinstallare Windows da zero e recuperare i file distrutti da vari backup più o meno recenti. Ma ho imparato parecchio sui ramsomware (e anche sulle persone).

Mentre scrivevo il post sono rimasto sorpreso nello scoprire che parecchi dettagli del funzionamento di CryptoLocker non coincidevano con i miei ricordi. Dopo un po’ di ricerche mi è tornato in mente di aver affrontato in realtà [TorrentLocker](http://www.bleepingcomputer.com/forums/t/549016/torrentlocker-support-and-discussion-thread-cryptolocker-copycat/) (noto anche come Crypt0L0cker), una [variante successiva](https://en.wikipedia.org/wiki/Ransomware) ma non meno pericolosa del CryptoLocker originale. Il senso del discorso comunque non cambia.

Tutto è successo circa un anno e mezzo fa. Un bel giorno entra in ufficio una collega, chiamiamola Virginia, che pronunzia la fatidica frase:

“Sta succedendo qualcosa di strano al mio computer.”

“Strano? Che significa strano, ti si è bloccato il computer?”

“Forse è un virus.”

Normale, quando non capiscono cosa succede, è _sempre_ un virus.

“Hai provato a riavviare?” (lo so, è banale, ma il supporto tecnico su Windows non ha senso senza almeno un riavvio).

“Certo, due volte, ma non succede niente.” (nel senso che non _cambia_ niente).

“Che hai fatto per beccarti il virus? Hai mica aperto l’allegato di qualche email?” (non ci credo nemmeno io, i virus nei file allegati colpiscono soprattutto i maschietti…).

“Boh, niente, stavo solo guardando la posta… Però ora non riesco più ad aprire i miei file, non riesco più a fare niente. Puoi venire per favore?”

Lo sapevo. Fregatura in arrivo e mattinata persa. Purtroppo, se pensano che tu sia _esperto_ di computer (ed è impossibile non farlo pensare se il 99% delle persone con cui hai a che fare sanno appena appena accenderli), tutti si sentono in diritto di chiederti continuamente aiuto, come se non avessi niente di meglio da fare tutto il giorno.

Purtroppo con Virginia ci lavoro e non posso esimermi. Con chi mi conosce meno posso uscirmene quasi sempre con un “Sai, io uso il Mac, di Windows ne capisco poco, meglio che chiedi al Greco.” (il Greco è il tecnico che, _in teoria_, dovrebbe occuparsi della rete e del supporto tecnico IT). Ma Virginia sa bene che non è così, con lei la scusa non regge.

Mi alzo e seguo svogliatamente Virginia nel suo ufficio. Ha un vecchio Dell ormai più che obsoleto, che dice sempre di voler cambiare. Guardo il monitor e il mio interesse all’improvviso va a mille.

Il desktop di Virginia è come sempre pieno di icone. Lavorandoci assieme lo so bene, l’ho presa spesso in giro per questa abitudine di tenere tutto alla rinfusa sulla scrivania virtuale mentre quella _reale_ è, al contrario, ordinatissima, anzi praticamente sempre vuota.

Ma ora vedo benissimo che tante icone sono cambiate. Non sono più quelle ben riconoscibili dei file pdf o doc, sono diventate bianche, _anonime_, sono le icone standard di Windows quando non sono associate ad un programma definito.

Ad un certo momento, puff!, vedo l’icona di un file di Word cambiare all’improvviso, e diventare bianca come le altre. Poi un’altra e poi un’altra, quasi seguissero un ordine prestabilito. È chiaro che c’è qualcosa che non va, ma non ho la minima idea di cosa sia. Decido di staccare il cavo di rete, isolando il computer dal resto della rete locale e da internet. Non c’è un motivo particolare per farlo, ma per fortuna è la cosa decisiva (anche se ancora non lo so).

Intanto le icone continuano a cambiare davanti a me. 

Apro Windows Explorer e attivo la [visualizzazione delle estensioni dei file](http://windows.microsoft.com/it-it/windows/show-hide-file-name-extensions#show-hide-file-name-extensions=windows-7). Una delle scelte più disgraziate fatte dalla Microsoft è quella di nascondere di default le estensioni dei file conosciuti (cioè dei file associati ad un particolare programma) da Windows. Ciò permette di inviare allegati con nomi come `fattura.pdf.exe` o `guardami.jpeg.exe` che, una volta scaricati sul PC, appaiono come degli inoffensivi `fattura.pdf` o `guardami.jpeg`. Una volta fatto doppio click su uno di questi file (in realtà programmi contenenti qualche virus o altro malware) la frittata è fatta.

Le cose diventano più chiare: ai file con le icone bianche è stata aggiunta una estensione _strana_, con dei caratteri apparentemente casuali. È chiaro che è una cosa seria, non sembra affatto un banale virus.

Inoltre, in tutte le cartelle compare un nuovo file html. Lo guardo e… accidenti, sono le [istruzioni per recuperare i file](https://www.bleepstatic.com/swr-guides/t/torrentlocker/regions/italy/italy-mysda-ransom-note.jpg).

 ![istruzioni CryptoLocker](/images/ransom-note.jpg  "Istruzioni CryptoLocker") 

Ma che diavolo è successo? E, soprattutto, cos’è questo _CryptoLocker_?
(anche se si tratta in realtà di TorrentLocker, il ransomware si autodefinisce CryptoLocker, probabilmente per sfruttare la fama sinistra del nome originale). 

Non c’è altro da fare che cercare informazioni in rete. Ma prima devo cercare di capire cosa ha fatto _veramente_ Virginia.

“Guarda Virginia, come vedi la cosa è grave. Mi dici che hai fatto prima che il computer smettesse di funzionare?” (le parole pensate erano ben diverse).

“Ma niente.”

“Come niente, _devi_ aver fatto qualcosa.”

“Ma no, che vuoi che abbia fatto? Stavo solo guardando la posta.”

“Ok, ma mentre guardavi la posta è successo qualcosa di strano?”

“Niente, leggevo solo le email.”

Alla fine, dopo un numero infinito di tira e molla, Virginia mi fa vedere una email della SDA. O almeno, [una email che _sembrava_ provenire dalla SDA](https://www.bleepstatic.com/swr-guides/t/torrentlocker/regions/italy/phish-email.jpg).

 ![Una finta email da SDA](/images/phish-email.jpg  "Una finta email da SDA") 

Basta una occhiata per capire che è _finta_. Le Poste Italiane hanno tanti difetti, ma non potrebbero mai inviare una email con questi errori.

I dettagli li ho capiti solo dopo, ma era evidente che qualunque cosa avesse infettato il computer di Virginia [arrivava da lì](http://www.bleepingcomputer.com/virus-removal/torrentlocker-cryptolocker-ransomware-information#regions).

“Quindi hai cliccato su questa email?”

“Stavo aspettando un pacco da Amazon, ho visto questa email e ho pensato fosse quello.”

“Scusa, ma se aspettavi un pacco da _Amazon_, perché dovrebbe arrivare da _SDA_?”

“Boh, e che ne so? Avevo fretta, ho visto questa email, ho pensato che fosse di Amazon e ci ho cliccato sopra.”

L’altra frase fatidica: “Avevo fretta”. Fretta, fretta, fretta, cento cose da fare sempre insieme, mai pensare prima di cliccare. Come se il tempo perso a recuperare i danni fatti per la fretta non contasse. Mai.

Mi metto a girare per la rete e scopro l’esistenza dei [ransomware](https://en.wikipedia.org/wiki/Ransomware), di cui fino a quel momento non sapevo nulla, o quasi. Leggo di CryptoLocker, il ransomware che, apparentemente, aveva infettato il computer di Virginia.

Strananamente, però, quello che leggo su CryptoLocker non coincide affatto con quello che sto osservando io, proprio in quel momento. Alla fine, a fatica, mi rendo conto di avere a che fare con una variante più recente di CryptoLocker, denominata [TorrentLocker](www.bleepingcomputer.com/virus-removal/torrentlocker-cryptolocker-ransomware-information), che si sta diffondendo rapidamente in tutta Europa, e che utilizza astutamente dei messaggi di posta elettronica adattati ai singoli paesi.

Leggo anche che la prima versione di TorrentLocker aveva un grosso baco e che [è disponibile un programma](http://www.bleepingcomputer.com/forums/t/547708/torrentlocker-ransomware-cracked-and-decrypter-has-been-made/) il quale, confrontando un file originale con quello crittografato, può determinare la chiave di decrittazione dei file. I dettagli del baco [erano stati pubblicati](http://digital-forensics.sans.org/blog/2014/09/09/torrentlocker-unlocked) e, nella seconda versione di TorrentLocker, l’autore aveva corretto il suo stesso errore, rendendo inviolabile il sistema di crittografia.

Ovviamente il TorrentLocker che aveva infettato il computer di Virginia era quello più recente e inviolabile.

Detto così sembra semplice, ma ci sono voluti due giorni per rendersi conto di tutto questo e per arrivare alla conclusione che non c’era niente da fare. Oltre che per fare il backup su un disco esterno dei file infetti (non si sa mai, magari prima o poi usciva fuori un tool di decrittazione efficace).

Virginia, per fortuna, poteva recuperare quasi tutto dai backup sul NAS dipartimentale e dai file presenti sul suo notebook personale.
TorrentLocker, infatti, infetta anche i dischi di rete [montati con una lettera di unità](http://windows.microsoft.com/it-it/windows/create-shortcut-map-network-drive#1TC=windows-7), come succede normalmente in Windows. Aver isolato istintivamente il suo computer dal resto della rete locale aveva permesso di limitare i danni e di evitare che venissero crittografate anche le copie di backup dei file.

C’è voluto un’altro giorno per reinstallare Windows e le applicazioni principali e per recuperare i file dai backup. Per fortuna TorrentLocker non infetta i messaggi di posta elettronica, che erano rimasti intatti sul disco infettato dal ransomware.

Insomma, un lavoraccio, altro che una mattinata buttata via! E tutto per non perdere qualche secondo a _pensare_, prima di cliccare su un link fasullo di un messaggio di posta elettronica! 

E Virginia, intanto? Nonostante la invitassi a seguire quello che facevo, ad imparare qualcosa per essere meno attaccabile nel futuro e a darmi almeno una mano a recuperare i file dai backup, Virginia continuava tranquillamente a fare le sue cose come se non fosse successo niente. Non aveva tempo: una scadenza qui, un’urgenza lì… Da lasciarla di brutto lì, in mezzo ai guai, a cavarsela da sola.[^1]

Lascio a voi i commenti.

Il mio è: mai, mai, mai fare supporto tecnico _gratuito_ (o al massimo, limitarsi a farlo solo ed esclusivamente con i parenti stretti e gli amici fidati). Perché ormai il valore delle cose è collegato al costo.
Se ti fai pagare, quello che fai vale. Se lo fai gratis, c’è poco da fare, non vale niente. 

“Dai, per favore, puoi venire a vedere? Lo so che non hai tempo, ma che ci vuole, due minuti?”

[^1]: Non l’ho fatto per senso di responsabilità e, forse, perché sono nonostante tutto una persona educata. Ma il sodalizio con Virginia ha cominciato a creparsi ed è finito poco dopo, per motivi ben più seri. Ma questa è un’altra storia.

---

Per approfondire

- [TorrentLocker (fake CryptoLocker) Ransomware Information Guide and FAQ](http://www.bleepingcomputer.com/virus-removal/torrentlocker-cryptolocker-ransomware-information)
- [TorrentLocker Support and Discussion Thread (CryptoLocker copycat)](http://www.bleepingcomputer.com/forums/t/549016/torrentlocker-support-and-discussion-thread-cryptolocker-copycat/)
- [TorrentLocker Ransomware Cracked and Decrypter has been made](http://www.bleepingcomputer.com/forums/t/547708/torrentlocker-ransomware-cracked-and-decrypter-has-been-made/)
- [Analysis of ‘TorrentLocker’ – A New Strain of Ransomware Using Components of CryptoLocker and CryptoWall](http://www.isightpartners.com/2014/08/analysis-torrentlocker-new-strain-malware-using-components-cryptolocker-cryptowall/)
- [TorrentLocker Unlocked](http://digital-forensics.sans.org/blog/2014/09/09/torrentlocker-unlocked)
- [The current state of ransomware: TorrentLocker](https://blogs.sophos.com/2015/12/23/the-current-state-of-ransomware-torrentlocker/)
