---
title: "Condannati a ripetere"
date: 2015-09-26
comments: true
tags: [Hayek, Ballmer, Colligan, Palm, Swatch, Microsoft, Apple, watch, Houle, iOS, Android, pay, GM, Daimler, Zetsche, Arkeson]
---
Nick Hayek Jr., attuale comandante in capo di Swatch, [ha definito](http://www.tagesanzeiger.ch/wirtschaft/die-alleskoenner-fressen-zu-viel-strom/story/15079390) watch *un giocattolo interessante* nel parlare dei prossimi orologi intelligenti dell’azienda svizzera, che avranno una batteria che dura nove mesi e certamente non avranno funzioni riguardanti la salute di chi li indossa:<!--more-->

>Come produttore di orologi, non posso prendermi la responsabilità che il mio apparecchio avvisi per tempo di un attacco di cuore in arrivo oppure no.

Come dire che se Paul Houle avesse indossato uno Swatch al posto di watch, starebbe tuttora rischiando la vita invece di [notare l’alterazione del proprio ritmo cardiaco](http://www.huffingtonpost.com/entry/paul-houle-apple-watch_5601878de4b08820d91a4688) prima del peggio.

Hayek non si contenta di snobbare il sensore cardiaco di Watch e vede ulteriori punti negativi:

>Questi apparecchi consumano così tanto da dover essere ricaricati ogni 24 ore al massimo. In aggiunta, l'utente perde immediatamente il controllo dei propri dati. Personalmente non voglio che la mia pressione sanguigna o la mia glicemia finiscano nel cloud, o su server nella Silicon Valley.

A parte il fatto che niente del genere accade su Watch e probabilmente si sta confondendo iOS con Android, non si sente in sottofondo Steve Ballmer, ex Microsoft, che [stronca il primo iPhone](https://macintelligence.org/posts/2013-09-22-sei-anni-di-differenza/) in quanto mancante di una tastiera fisica?

O a scelta Ed Colligan di Palm, che commentò su [quanti anni servissero](https://macintelligence.org/posts/2014-08-15-promemoria-per-lautunno/) per capire come realizzare un telefono decente e sulla conseguenza impossibilità per *quelli dei Pc* di entrare sul mercato e mettersi a vendere. Il tutto qualche mese prima di iPhone.

Per non farci mancare nulla, abbiamo anche un augusto [parere negativo su Apple Pay](https://macintelligence.org/posts/2014-09-15-ce-chi-dice-no/).

Gran finale: Apple neanche ha lontanamente accennato all’idea di costruire un’auto, ma il capo di Daimler Dieter Zetsche [ha già espresso la sua opinione](http://www.motoring.com.au/news/2015/mercedes-benz-dont-build-an-icar-49356): 

>Non conosco la loro strategia e non so che cosa stiano facendo, ma sarei molto sorpreso se finissero per avere ragione.

In sua compagnia l’ex amministratore delegato di General Motors, Dan Akerson:

>Un sacco di gente che non ha mai lavorato nel settore automobilistico non lo capisce e tende a sottovalutarne le difficoltà. […] Se vogliono occuparsi della fabbricazione di un’auto da zero, faranno meglio a pensarci bene. Prendiamo acciaio, acciaio grezzo, e lo trasformiamo in un’auto. Se vogliono fare questo, non hanno idea di dove si stanno cacciando.

Chi non conosce il passato è condannato a ripeterlo.