---
title: "Sul filo del verderame"
date: 2017-12-03
comments: true
tags: [iPad]
---
Una delle ipotesi che [avanzavo](https://macintelligence.org/posts/2017-11-28-il-cavo-estinto/) rispetto ai miei problemi nel caricare iPad riguardava un danneggiamento del cavo. Ecco la foto più ravvicinata del connettore che sono riuscito a scattare in modo decente.<!--more-->

 ![Connettore danneggiato](/images/connector.jpg  "Connettore danneggiato") 

Si riesce a vedere abbastanza bene che la parte superiore sinistra dell’ingresso ha un aspetto diverso da quella superiore destra. Dalla foto non è evidentissimo, ma si tratta di un fenomeno di corrosione che è visibile anche nella parte inferiore destra dell’ingresso medesimo.

Considerato che un altro cavo, stessa età e ugualmente originale, performa senza problemi, tirerei le conclusioni. Cavo danneggiato. D’altronde è durato cinque anni come minimo.