---
title: "Dovere e potere"
date: 2015-11-27
comments: true
tags: [LibreOffice, LibreItalia, Terni, Savona, AAA]
---
Domani dovrei trovarmi alla seconda [Conferenza LibreOffice LibreItalia](https://www.eventbrite.it/e/biglietti-2-conferenza-italiana-libreoffice-libreitalia-19065934724) a Terni. Purtroppo è stato veramente impossibile conciliare tutte le esigenze e non potrò esserci. Spero non si arrabbino eccessivamente.<!--more-->

Domani (non essendo indispensabile il pernottamento, che ha causato il problema di cui sopra) potrò essere all’[inaugurazione per il pubblico](http://www.allaboutapple.com/2015/11/all-about-apple-apre-al-pubblico-28-novembre/) del nuovo scintillante museo All About Apple a Savona.

Sicuramente non farò una levataccia e arriverò sul posto con una certa comodità, ma segnalo a chiunque trovi interessante la visita che i battenti aprono alle dieci del mattino.

Non che debba esserci per forza (dopotutto l’ho visto nascere dai [lavori in corso](https://macintelligence.org/posts/2015-08-25-lavori-in-corso-in-piazza-in-darsena/) alla [preinaugurazione](https://macintelligence.org/posts/2015-10-25-post-pre-inaugurazione/)). Ma posso, nonostante un mal di gola cattivo, e non voglio mancare.