---
title: "Sconti mai visti"
date: 2014-02-23
comments: true
tags: [3D, Oled, laser]
---
Passo raramente da un Media World e magari quello che visto non sorprenderà nessuno. Mi sono sorpreso comunque.<!--more-->

Ho visto una laser bianco e nero in offerta speciale a 59 euro. Ho visto una laser a colori a 161 euro. Non ricordavo che i prezzi si fossero abbassati così.

Il televisore a schermo Oled costava invece 6.499 euro. Però, che spessore di schermo. Se sembrano sottili i televisori Lcd, questi porranno problemi con la donna delle pulizie.

Soprattutto, per la prima nella storia, ho visto in vendita a Media World una stampante 3D. Poca roba, prezzo elevato. Attendere tre anni.

 ![Una stampante 3D a Media World](/images/hamlet.jpg  "Una stampante 3D a Media World") 