---
title: "La sfida in differita"
date: 2016-08-24
comments: true
tags: [Note, 7, Samsung, iPhone, 6s, Galaxy]
---
Sono sempre cose che valgono poco nella vita reale, ma regala sempre una certa soddisfazione vedere il computer da tasca Apple dell’anno scorso che [strabatte in velocità e tempi di reazione](https://www.youtube.com/watch?v=3-61FFoJFy0&feature=youtu.be) quello Samsung uscito (simbolicamente) l’altroieri e neanche così meno costoso. Per battere un iPhone 6s del 2015 dovremo magari aspettare un Note 8 del 2017 e chissà se basterà.

<iframe width="560" height="315" src="https://www.youtube.com/embed/3-61FFoJFy0" frameborder="0" allowfullscreen></iframe>
