---
title: "Italia Libre"
date: 2014-12-02
comments: true
tags: [LibreOffice, LibreItalia]
---
Dopo la rivelazione-shock del mio stato di [azionista Apple](https://macintelligence.org/posts/2014-12-01-conflitto-di-interessi/), vuoto il sacco per intero: ho accettato di entrare nel Comitato Tecnico Scientifico di [LibreItalia](http://www.libreitalia.it), il *punto di riferimento per gli utenti italiani di LibreOffice*.<!--more-->

Grande onore e grandi oneri, come si evince da un capoverso della mail che ha iniziato tutto:

>È un ruolo squisitamente “consulenziale”, per il quale non prendi nemmeno un centesimo (l’associazione è una ONLUS), in base al quale offri la tua competenza – ovviamente, del mondo MacOS – rispetto a LibreOffice.

[Vaste programme](http://fr.wikipedia.org/wiki/Mort_aux_cons), avrebbe forse commentato il generale De Gaulle. Eppure mi va di mettere in campo energie e incoscienza per l’esatto motivo che di [LibreOffice](http://www.libreoffice.org) sono un utilizzatore scarso.

Sospetto che nel mondo Mac sia più la regola che l’eccezione e questo merita uno sforzo per cambiare le cose.

C’è bisogno di fare più alfabetizzazione e divulgazione, perché tanta gente neanche sa che LibreOffice esiste (e non parliamo del software libero in generale, dato che la parola magica è *gratis* e con quella viene fatto passare).

D’altro canto è evidente all’occhio che LibreOffice, con tutti i suoi meriti, deve ancora fare strada per inserirsi pienamente nel mondo Mac. È qui che mi piace l’idea di mettere impegno. Non so che cosa riusciremo a fare; so che ne vale la pena.

Sommessamente aggiungo che l’[associazione a LibreItalia](http://www.libreitalia.it/diventa-socio/) costa cinque euro per gli studenti, dieci euro per i soci ordinari, cinquanta euro per i soci sostenitori o almeno queste sono le quote per il 2014. Entrati nel 2015, per me sarà un dovere morale figurare nell’elenco soci; per chiunque altro volesse seguirmi sarebbe un piccolo e notevole regalo da fare e farsi, a sé e alla causa del software veramente libero. Anzi, *libre*.