---
title: "Quando il gioco"
date: 2020-05-03
comments: true
tags: [iPad, Sensor, Tower]
---
Che cosa era in calo da quattro anni e improvvisamente si è messo a crescere? Il numero dei download di app su iPad, per esempio.

I dati di Sensor Tower dicono che [nel primo trimestre dell’anno si sono scaricate 1,1 miliardi di app per iPad](https://sensortower.com/blog/ipad-app-revenue-downloads-covid-19-impact) e la spesa ha superato i due miliardi di dollari.

A beneficiarne maggiormente in assoluto è stata la categoria giochi, come al solito al primo posto. Ma la crescita maggiore in percentuale spetta alla categoria Education, che è cresciuta del 78 percento e ha superato ogni suo record precedente.

Il mercato delle app lo si conosce: la spesa di un euro e spiccioli viene considerata un lusso semi-inutile in tempi normali, figuriamoci in questi. Eppure, chi vede la necessità – o l’opportunità – di investire qualche moneta per sé o per un piccolo lo fa. E lo fa su iPad, evidentemente considerato macchina adeguata per beneficiarne.

Che in una situazione incerta ci si affidi a iPad in misura maggiore di prima è un dato interessante. A volte è un computer, a,volte no, ci si programma ma non del tutto, non è il portatile ideale eccetera eccetera eccetera ma ho la sensazione che, se sapessi di dover partire per la classica isola deserta con il bagaglio minimo, prenderei iPad.

Credo anche che non sarei l’unico a farlo. Ognuno sulla sua isola, certo.
