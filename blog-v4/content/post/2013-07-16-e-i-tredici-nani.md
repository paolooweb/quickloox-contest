---
title: "E i tredici nani"
date: 2013-07-16
comments: true
tags: [iPad]
---
Su *Motley Fool* appare un pezzo a firma Evan Niu, dal titolo [Apple non può permettersi di aspettare a presentare un iPad mini retina](http://www.fool.com/investing/general/2013/07/13/apple-cant-afford-to-wait-on-a-retina-ipad-mini.aspx).

Perché gli altri stanno proponendo schermi di risoluzione superiore e *bla bla bla*.<!--more-->

Sarà anche vero.

A maggio, comunque, Chitika ha pubblicato [i risultati delle sue rilevazioni di traffico](http://chitika.com/insights/2013/may-tablet-update). iPad, 82,4 percento; concorrente più vicino, 6,5 percento.

Dietro al secondo, altre dodici tavolette che messe tutte quante insieme fanno briciole meno del 18,6 percento, ovvero in tredici non fanno un quarto di quello che fa iPad.

Un iPad mini retina fa piacere a tutti (pure a John Gruber, che [da Daring Fireball non lo vede probabile](http://daringfireball.net/linked/2013/07/14/ipad-mini-retina)), ma dal punto di vista commerciale non trasparirebbe, tutta questa urgenza.