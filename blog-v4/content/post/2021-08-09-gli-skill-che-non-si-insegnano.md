---
title: "Gli skill che non si insegnano"
date: 2021-08-09T00:25:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [scuola, Lidia, Office] 
---
Sono passati dieci anni da quando parlai di iPad e uso della tecnologia per l’apprendimento ai docenti di un istituto tecnico piuttosto grosso in provincia di Trento.

Erano anni eroici. Avevo un iPad di prima generazione, inadatto a tenere una presentazione in modo adeguato. Chiesi in prestito ad Apple Italia un iPad di seconda generazione, uscito da poco, e andai con quello.

Fu una bella giornata, difficile e stimolante. Difficile per trovarsi di fronte insegnanti innamorati di lavagne e gessetti, stimolante per tutto quello che si faceva nella scuola e l’attenzione che comunque ricevetti. Non sai mai quanto effettivamente delle tue parole rimane veramente quando parli alle persone e quella volta ebbi una delle mie maggiori soddisfazioni lavorative di sempre.

Durante la presentazione e poi la visita alla scuola, in conversazione con il bravissimo dirigente dell’istituto e con gli insegnanti illuminati, capitò di parlare anche di Wikipedia. Mi infervorai e mi cacciai nei guai come altre volte: dissi la verità. Wikipedia Italia era in mano a una congrega di mandarini egoriferiti. (Penso lo sia ancora, ma non lo so e nemmeno mi interessa; l’unica cosa che faccio con Wikipedia è linkarla quando proprio veramente non c’è altro di decente per evitarlo).

Dissi che, in una scuola, l’uso di Wikipedia per cercare informazioni era una follia e una negazione del buon insegnamento (e apprendimento). In una scuola, leggere Wikipedia è il male assoluto. In una scuola, Wikipedia *va scritta*.

Recuperare informazioni dalle fonti autentiche. Distillarne quello che costituisce una buona voce per Wikipedia e organizzarlo. Prendere familiarità con il sistema di *markup*. Collaborare con i coautori e rispettare le regole. Tutto questo è un eccellente metodo per imparare e per imparare a imparare.

*Una scuola deve scrivere Wikipedia, non leggerla*, dissi. Se abbastanza scuole scrivono Wikipedia, arriverà abbastanza materiale da impedire ai mandarini di crogiolarsi nel loro piccolo potere, perché non riusciranno a controllarlo.

Tutto ciò potrebbe svanire come lacrime nella pioggia. Anni dopo, tuttavia, mi imbattei per caso in un telegiornale che dedicava spazio alla didattica digitale nelle scuole, mostrava sequenze di quella scuola e ne parlava per via di come [incoraggiava il lavoro attivo su Wikipedia nell’ambito del processo didattico](https://macintelligence.org/posts/Due-anni-un-secolo.html).

Qualcosa di quanto avevo detto aveva germogliato.

Conservo la presentazione tenuta quel giorno, in cui a un certo punto accenno ai nuovi skill messi all’incrocio di tecnologia e arti liberali, che è necessario insegnare ai ragazzi per il loro futuro e che i programmi scolastici tradizionali ignorano. Questa era la lista dei *tech skill*, che chiamai così per distinguerli dagli *hard skill*, quelli classici, e dai *soft skill* di cui si parla fin troppo e da troppo tempo:

- Ipertesto
- Edizione e montaggio multimediale
- Modellazione 3D
- Condivisione e lavoro di gruppo in rete
- Ricerca e analisi di informazioni e dati

Queste potrebbero essere le basi di un [curricolo di studi alternativo](https://macintelligence.org/posts/Un-curricolo-per-lestate.html) per il triennio che completa il primo ciclo della primaria e per quello che lo segue (le medie inferiori di una volta). Probabilmente va limato qualcosa e immagino che si evidenzierà lavorandoci.

La prossima fase consiste nel delineare gli obiettivi parziali all’interno di ciascuno di questi obiettivi globali. Poi si tratterà di inserire gli obiettivi parziali nell’ambito delle materie tradizionali e, per farlo, trovare progetti ed esercizi che possano inserirsi con profitto nel programma canonico.

Come sto andando?

*Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile.*