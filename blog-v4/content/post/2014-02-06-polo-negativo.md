---
title: "Polo negativo"
date: 2014-02-07
comments: true
tags: [Trackpad, Ikea, Duracell, Energizer]
---
Anche se costano poco, ho registrato una mediocre esperienza delle batterie Ikea.<!--more-->

Messe due AA in Magic Trackpad, sono durate appena cinquantotto giorni. Meno ancora delle pile presenti nella confezione, meno ancora di una ricaricabile. Può darsi sia stato un caso, naturalmente, o che dal cassetto siano uscite pile parzialmente consumate.

Abbassano la media a 142 giorni, su un campione di otto cambi di batteria.

Il prossimo giro è un altro inedito, una coppia di Energizer Ultra. Saprò dire.