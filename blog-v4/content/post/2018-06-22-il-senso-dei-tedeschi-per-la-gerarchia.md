---
title: "Il senso dei tedeschi per la gerarchia"
date: 2018-06-22
comments: true
tags: [Ragtime]
---
C’è stato un tempo in cui ho apprezzato Ragtime, uno di quei programmi di impaginazione tuttofare ideali per un volantino, un pieghevole, una pagina pubblicitaria, capaci di combinare testo ed elementi grafici con grande libertà ed efficacia. Che consentivano di essere creativi senza pensare allo strumento, semplicemente sfruttandone le possibilità.

Nel tempo le mie esigenze sono cambiate e Ragtime non è più stato una priorità. Però ho sempre seguito le loro newsletter, perché quando un prodotto ti lascia una impressione positiva, tendi a ricordartene.

Mi ero dimenticato invece che fossero tedeschi, gente come tutti con tante doti e qualche difettuccio. Nel caso dei tedeschi, un certo qual complesso di superiorità.

E difatti riescono a titolare nell’ultima newsletter una cosa come

>Il nuovo macOS 10.14 (“Mojave”) di Apple è compatibile con RagTime 6.6.

Il senso è chiaro e si coglie anche la soddisfazione di bravi sviluppatori. Però via, si poteva anche scrivere all’inverso. Da un tedesco, dopotutto, ci si aspetta che colga il senso di una gerarchia.