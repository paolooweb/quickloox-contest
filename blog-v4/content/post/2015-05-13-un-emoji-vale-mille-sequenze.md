---
title: "Un emoji vale mille sequenze"
date: 2015-05-15
comments: true
tags: [Instagram, emoji, iPhone, iPad, Android, Python, regex, Unicode]
---
La lettura istruttiva – nel senso dell’istruzione e della cultura moderna – consta di questo [incredibile articolo](http://instagram-engineering.tumblr.com/post/117889701472/emojineering-part-1-machine-learning-for-emoji) sul Tumblr di Instagram e del [suo seguito](http://instagram-engineering.tumblr.com/post/118304328152/emojineering-part-2-implementing-hashtag-emoji).<!--more-->

Instagram è una rete sociale che si basa su fotografie, commentate da brevi messaggi di testo. Da quando è apparsa l’apposita tastiera su iPhone e iPad (toh), la quantità di emoji (le faccine e i pittogrammi) usata nei messaggi di Instagram è schizzata alle stelle. Con l’avvento della tastiera equivalente su Android, due anni in ritardo (doppio toh), c’è stato un altro aumento.

Siccome Instagram è globale e le culture di chi lo usa sono diversissime, per capirne di più gli ingegneri si sono messi a mappare l’uso degli emoji in rapporto al significato che ciascuno gli attribuisce… che non è sempre proprio uguale.

Il secondo articolo mostra come Instagram ha messo in opera gli *hashtag* emoji (praticamente la possibilità di scrivere #🍕 al posto di #pizza e avere lo stesso risultato in termini di trattamento del messaggio da parte dei meccanismi interni di Instagram).

Un sacco di cose da imparare su Unicode, emoji, espressioni regolari, evoluzione del linguaggio, diversità delle culture, programmazione come avviene oggi nelle nuove imprese eccetera. Lettura affascinante, da 👍.