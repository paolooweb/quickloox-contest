---
title: "iPhone segreto"
date: 2015-10-13
comments: true
tags: [iPhone, Android]
---
Ci sono [codici di chiamata per iPhone](https://www.ubertechsupport.com/blog/guide-to-access-secret-menu-of-iphone-and-android-devices/) (e per Android) assenti dal manuale di istruzione e che probabilmente solo un tecnico conosce.<!--more-->

Mi piace molto la cosa perché è sempre interessante scoprire funzioni nascoste, per quanto di utilità quotidiana del tutto marginale.

Ulteriore motivo di soddisfazione è che queste notizie non arrivano mai da quelli che dicono *sul mio computer faccio quello che voglio*. Senza sapere quello che possono fare.

Come livello di approfondimento e curiosità, siamo nei pressi dei [codici di iPod](http://www.methodshop.com/gadgets/ipodsupport/diagnosticmode/) per entrare nella modalita di test. Bello se ne saltassero fuori più spesso, di queste cose.