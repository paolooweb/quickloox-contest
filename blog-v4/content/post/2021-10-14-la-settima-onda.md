---
title: "La settima onda"
date: 2021-10-14T00:31:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [John Gruber Daring Fireball, Love Is the Seventh Wave, Apple Watch Series 7] 
---
Sono in procinto di dotarmi di un watch Series 7 e ho letto con doppia attenzione la [recensione di John Gruber su Daring Fireball](https://daringfireball.net/2021/10/apple_watch_series_7).

Mi è piaciuta perché ha toccato corde cui sono più sensibile. La cura per i dettagli, la differenza che può fare un millimetro nel *case* di un orologio, l’evoluzione dello schermo dal primo modello a oggi, le particolarità del bordo convesso della Series 7.

Mi è piaciuta questa considerazione:

>watch è un orologio cui capita di essere un computer, non un computer cui capita di stare sul polso. Se valuti la decisione di aggiornarti come faresti con un computer o persino un telefono, stai mancando il punto.

In verità ho sempre chiamato watch *computer da polso*. Però questa considerazione di Gruber è indirizzata a chi abbia già un watch e stia misurando l’opportunità di cambiare. Il suo giudizio è che vale la pena di farlo solo se piace il suo aspetto. Cioè, *il criterio usato da chiunque cerchi un nuovo orologio*.

Io arrivo da una prima generazione e per me il salto sarà comunque enorme in fatto di prestazioni e possibilità. Continuo a pensarlo come un computer. E d’altronde lo cambio dopo sette anni, proprio come farei con un computer.

Quest’altra considerazione, che è anche la conclusione, mi è piaciuta moltissimo:

>Sette generazioni dopo, sembra chiaro che Apple abbia perseguito fin dall’inizio un ideale platonico di watch, in termini di forma, dimensioni e schermo. Series 4 ha costituito un progresso importante, con cambiamenti di rilievo che hanno molto avvicinato watch a questo ideale. Series 7 è un progresso minore, non maggiore, ma questo si deve alla riprogettazione di Series 4, che ha portato watch così vicino a ciò che da subito era stato concepito per essere. Forse un giorno Apple produrrà un watch del tutto reimmaginato; un nuovo ideale. Forse no. watch ha sempre saputo che cosa voleva essere e Series 7 gli si avvicina più che mai.

Vado convinto. [Love is the seventh wave](https://www.youtube.com/watch?v=uXZistami3c).

<iframe width="560" height="315" src="https://www.youtube.com/embed/uXZistami3c" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

*Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile.*