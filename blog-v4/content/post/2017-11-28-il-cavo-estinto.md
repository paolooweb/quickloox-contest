---
title: "Il cavo estinto"
date: 2017-11-28
comments: true
tags: [iPad]
---
[Passata la nottata con Mac](https://macintelligence.org/posts/2017-11-26-l-utile-utility/), è il momento di iPad.

Il quale funziona più che bene ma, imprevedibilmente, ogni tanto inizia a scaricarsi e ignorare l’alimentazione. Arriva a zero e solo allora, sempre imprevedibilmente, si sveglia e ricomincia a caricarsi.<!--more-->

Ho fatto prove su prove e perso molto tempo; alla fine *sembra* che la macchina ora reagisca a una precisa combinazione di uno specifico alimentatore e un altrettanto specifico cavetto; altrimenti non si carica. Non sono ancora matematicamente sicuro della cosa; mi servirebbe un altro episodio di scarica e carica, che spero non arrivi.

Potrebbe funzionare così: iPad è vecchiotto (più di cinque anni) e gli accessori pure. Forse il sistema di ricarica è diventato meno sensibile e ha bisogno di un amperaggio leggermente più sostenuto e quell’alimentatore e quel cavetto, insieme, riescono a passare abbastanza corrente da fare rispondere iPad.

Oppure il cavetto, coetaneo di iPad, è danneggiato internamente al punto di non riuscire a trasmettere la corrente necessaria.

Sono situazioni che possono accadere con macchine vecchie e che probabilmente preludono a una sostituzione. Ultimamente sto pensando che, dovessi rinnovare il setup attuale (MacBook Pro 17”, iPad di terza generazione), prenderei un MacBook Pro 13” ben carrozzato più un monitor di grande dimensione per la scrivania; e poi un iPad Pro 12,9”

Vedremo se sarà davvero così quando capita.