---
title: "La seconda Olanda"
date: 2021-02-23T00:19:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [Apple, Steve Jobs, Asymco, Horace Dediu] 
---
Gli analisti si aspettano che Apple nel 2020 abbia fatturato nell’intorno dei 333 miliardi di dollari (che per qualcuno [sono troppo pochi](https://www.investors.com/etfs-and-funds/sectors/sp500-theres-still-one-way-apple-stock-isnt-no-1/)).

Sono numeri vertiginosi. Quando Steve Jobs ritornò come interim Ceo in Apple un quarto di secolo fa, mise a punto un piano che avrebbe consentito la sopravvivenza dell’azienda a patto che fatturasse sei miliardi di dollari l’anno, l’1,8 percento della cifra di oggi.

Ora Horace Dediu di Asymco prevede in un tweet che il *valore delle transazioni dell’ecosistema Apple* [raggiungerà il trilione](https://twitter.com/asymco/status/1363963342050713604) (all’americana, mille miliardi) entro il 2024.

<blockquote class="twitter-tweet"><p lang="en" dir="ltr">The Apple Ecosystem will reach $1 Trillion in yearly transactional value by 2024(*). <br><br>$160 billion of that will be transacted through the App Store, of which $112 billion will be paid to developers by Apple. $840b will be direct revenues for developers.<br><br>(*) GDP of Netherlands</p>&mdash; Horace Dediu (@asymco) <a href="https://twitter.com/asymco/status/1363963342050713604?ref_src=twsrc%5Etfw">February 22, 2021</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

È cosa ben diversa dal fatturato, ma fa ugualmente impressione. È circa dire che il prodotto interno lordo della nazione-Apple pareggerà quello olandese.

Scrivendola Dediu, prendo la cosa sul serio; fosse chiunque altro la definirei una sparata.

Citare l’Olanda è interessante perché è una nazione che ha costruito la propria ricchezza sui commerci e su collegamenti con ogni luogo nel mondo. Una specie di startup del Rinascimento basata sull’Internet delle navi.

È ancora molto presto per parlarne seriamente, ma le organizzazioni come Apple sono la prima avvisaglia di quello che sostituirà gli stati-nazione nei decenni a venire. Chiaramente la solidità economica è uno dei primi parametri da considerare. Un altro è una influenza a livello planetario.

Come Apple sceglierà di esercitare la propria influenza, e che tipo di relazione gli stati-nazione decideranno di stabilire o meno con Apple, sono due macrotemi che è già tempo di iniziare a sviscerare.

*Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile.*