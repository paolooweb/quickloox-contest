---
title: "Viva la differenza"
date: 2020-01-14
comments: true
tags: [Mac, Medium, Kapanoglu]
---
Con un passato nella divisione Core OS di Windows, protagonista della *demoscene* (programmatori che si sfidano a creare cose incredibili con una quantità ridicola di codice e risorse hardware) e startupper, Sedat Kapanoglu si stacca certamente dalla massa dei programmatori comuni.

Quindi tocca ascoltarlo, quando scrive di come è cambiata la programmazione negli ultimi vent’anni e nell’elenco annota anche questa differenza:

>La gente sviluppa software su Mac.

Il pezzo contiene molte altre osservazioni, alcune polemiche, altre di costume, altre ancora ironiche, e merita una scorsa. Per esempio:

>La documentazione è sempre online e si chiama Google. Non esiste più documentazione offline. Anche se esiste, nessuno lo sa.

O anche:

>Siccome abbiamo processori molto più veloci, i calcoli numerici si effettuano in Python, molto più lento di Fortran. Così i calcoli richiedono più o meno lo stesso tempo di vent’anni fa.

E infine:

>Un pixel non è più un’unità di misura rilevante.
