---
title: "Le volpi del marketing"
date: 2017-10-27
comments: true
tags: [iPhone, Face, ID, Samnsung, Core, ML]
---
C’è molta sollecitudine nel criticare iPhone X prima che arrivi in vendita e prima che sia stato possibile collaudarlo. Se fosse pieno di difetti sarebbe più che giusto segnalarlo; è bene sapere che [lo schermo di un Pixel 2 XL non è il massimo](http://bgr.com/2017/10/27/pixel-2-xl-display-issues-fixes-pixel-2/).

Ma appunto, a seguito di una prova con la macchina in mano. Così, sulla carta, somiglia più a sollecitudine di qualche reparto marketing concorrente. Come se gli argomenti a favore dei propri prodotti non bastassero e ci volesse anche del discredito verso quelli degli altri.

Non per niente le critiche più sospette riguardano proprio le funzioni dove iPhone X, sempre sulla carta, è decisamente più avanti. Secondo Bloomberg, Apple [avrebbe abbassato le specifiche dei componenti hardware di Face ID](https://www.bloomberg.com/news/articles/2017-10-25/inside-apple-s-struggle-to-get-the-iphone-x-to-market-on-time) per via delle difficoltà incontrate dai fornitori nel produrre componenti all’altezza.

Se è vero, ce ne accorgeremo e faremo tanto di cappello a Bloomberg. Ora la mancanza di verificabilità della tesi, assai generica nella sua articolazione, insospettisce.

E il modulo di *machine learning* Core ML, che secondo Wired potrebbe essere usato da un aggressore per carpire segreti collegati alle foto? Come [nota iMore](https://www.imore.com/no-apples-machine-learning-engine-cant-surface-your-iphones-secrets), le foto sono a disposizione di qualsiasi app qualificata e quindi un aggressore avrebbe molta più comodità e comfort a usare il *proprio* modulo, su un server fidato, invece di rischiare di farsi scoprire a usare le risorse dell’apparecchio. Ovvero, si solleva un problema inesistente rispetto a un altro fiore all‘occhiello di iPhone X.

Quasi come a cercare di degradare l’immagine di iPhone X in modo preventivo, prima di qualsiasi vero riscontro.

Quasi come se fossero certe piccole volpi del marketing a parlare dell’uva e darla per acerba vista l’impossibilità ad arrivarci.
