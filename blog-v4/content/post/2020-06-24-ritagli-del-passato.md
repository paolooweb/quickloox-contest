---
title: "Ritagli del passato"
date: 2020-06-24
comments: true
tags: [Sadun, App, Clips, iOS\4, Mac]
---
C’è una discreta [attenzione su App Clips](https://ericasadun.com/2020/06/23/app-clips-when-is-an-app-an-app-and-when-should-it-be-a-webpage/) e però nessuno, a mia conoscenza, ricorda che la funzione [era presente nel Mac OS X](https://www.telerik.com/blogs/web-clips-hidden-useful-os-x-feature) di molte edizioni fa.

Su Mac prometteva ma era poco utile. Su iOS potrebbe invece esserlo molto. Non si butta via niente.