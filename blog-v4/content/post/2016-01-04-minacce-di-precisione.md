---
title: "Minacce di precisione"
date: 2016-01-04
comments: true
tags: [watch, Utc, iPhone, Nyt, Ntp, RaiUno, Fantozzi, Stratum]
---
Rispetto a quando [auspicavo che Apple uscisse con una soluzione per i problemi del tempo di rete](https://macintelligence.org/posts/2015-03-22-non-ce-ancora-tempo/) ho capito qualcosa più di come tiene il tempo watch, grazie in primo luogo a una [intervista](http://www.telegraph.co.uk/technology/apple/watch/12074452/Why-the-Apple-Watch-will-be-the-most-accurate-way-to-ring-in-the-New-Year.html) concessa da Kevin Lynch al *Telegraph*.<!--more-->

watch si impegna a stare entro i cinquanta millisecondi dal [tempo universale](http://www.timeanddate.com/time/aboututc.html) grazie a tecnologie che si preoccupano di tenere alla giusta temperatura gli oscillatori interni – se fa troppo freddo diventano meno precisi – e con verifiche come il confronto di due riprese a mille fotogrammi per secondo, una di watch e una di un orologio atomico, per determinare con precisione le differenze.

In questo modo Apple insegue una precisione la più vicina possibile a [Stratum 1](http://www.endruntechnologies.com/stratum1.htm), quella degli apparecchi che parlano direttamente con gli orologi Stratum 0, di riferimento. Entro i cento millisecondi si parla comunque di apparecchi Stratum 2; la sfida è arrivare prima o poi ai microsecondi. Sul sito di Network Time Protocol si trovano [spiegazioni ed elenchi di server](http://support.ntp.org/bin/view/Servers/WebHome).

Il freddo che aumenta l’imprecisione degli orologi è probabilmente il motivo per cui RaiUno ha inaugurato l’anno nuovo [con notevole anticipo](http://www.corriere.it/spettacoli/16_gennaio_01/bestemmia-auguri-anticipo-dimenticare-capodanno-rai-1-d40853ac-b09f-11e5-85c5-ba49db0234b6.shtml).

<blockquote class="twitter-tweet" lang="en"><p lang="it" dir="ltr">Da -3.59 a -2.48 in 30 secondi. Così <a href="https://twitter.com/hashtag/RaiUno?src=hash">#RaiUno</a> ha sbagliato il countdown del <a href="https://twitter.com/hashtag/Capodanno2016?src=hash">#Capodanno2016</a> <a href="https://twitter.com/ilpost">@ilpost</a> <a href="https://twitter.com/repubblicait">@repubblicait</a> <a href="https://t.co/hAt5C9GG9s">pic.twitter.com/hAt5C9GG9s</a></p>&mdash; Bluelake76 (Daniele) (@Bluelake76) <a href="https://twitter.com/Bluelake76/status/682903048197402625">January 1, 2016</a></blockquote> <script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>

Tutti hanno ricordato [Fantozzi](https://www.youtube.com/watch?v=S6mUZCQ1m64) dove l’orchestrina lancia il brindisi alle dieci e mezza visto che ha due ingaggi per la serata; credo più alla precisa volontà di mettere in onda a mezzanotte in punto lo spot di uno sponsor.

In ogni caso, abbiamo un’epoca capace di svalutare perfino l’ora esatta ed ecco un altro motivo per cui watch diventa dirompente rispetto al pensiero comune. Il che porta a un [bell’articolo recente del New York Times](http://www.nytimes.com/2015/12/31/technology/personaltech/is-it-time-for-an-apple-watch.html), dove si appaiano le aspettative e i giudici del primo iPhone con quelle del primo watch.

Come fosse il primo iPhone e che cosa sia diventato è sotto gli occhi di tutti. watch suscita giudizi controversi; intanto si impegna a tenere l’ora meglio degli altri. Neanche iPhone, con la funzione telefonica, ci arrivava.

<iframe width="560" height="315" src="https://www.youtube.com/embed/S6mUZCQ1m64" frameborder="0" allowfullscreen></iframe>