---
title: "Non tutte le frittate escono col buco"
date: 2020-02-19
comments: true
tags: [Word, Pages, iPad, Mac, Safari, WordPress, LibreOffice]
---
Poche danno più fastidio della consegna di un documento che devi elaborare – per un libro, per un sito, per un qualsiasi utilizzo professionale – sotto forma di documento Word, impomatato, impostato, impaginato, bilanciato, definito fino all’ossessione grazie al controllo maniacale di decine di parametri. Controllo di cui resta *niente* un minuto dopo che la consegna è stata effettuata.

Un bravo autore consegna il testo e le immagini (i video, l’audio, gli elefanti, qualunque corredo al testo) a parte. Al limite con l’aggiunta di un documento di anteprima che dà l’idea di come si immagina l’aspetto finale della cosa. Ma testo e immagini sono separati.

Non dovrebbe essere difficile da capire. L’esperienza di preparazione di una frittata dovrebbe essere abbastanza condivisa. Quanto meno, capire che ci sono di mezzo uova, la loro apertura e qualche altra operazione accessoria.

La frittata è una ricetta banale, ma ci sono i piccoli segreti di ciascuno. C’è chi aggiunge il goccio di latte, chi il pizzico di farina, chi controlla la temperatura del piano cottura, chi sbatte le uova rigorosamente nello stesso senso e così via. Ognuno, a modo suo, è un esperto di frittata.

Se venissimo invitati a casa di un amico per godersi insieme una bella frittata, porteremmo le uova già sbattute? No. Intere. A romperle, amalgamarle, armonizzarle, aggiungere l’ingrediente segreto, pensa l’amico. Quando ci invita a cena, l’esperto di cucina è *lui*.

Analogamente, chi consegna un testo da elaborare dovrebbe farlo nella maniera più grezza possibile, proprio per facilitare l’elaborazione.

Altrimenti qualcuno infila dentro il documento Word le immagini. E qui Pages ha un buco importante, perché importa le immagini in bassa risoluzione, bassa per usare un eufemismo. Sono inutilizzabili. Quando mi capita di dover estrarre immagini da un documento Word, lo apro con [LibreOffice](https://www.libreoffice.org).

E mi chiedo perché Pages possieda, per esempio, una tipografia avanzatissima e poi non sia capace di importare come si deve un’immagine. Siamo alla frittata, mica alla cucina molecolare.