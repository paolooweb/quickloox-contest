---
title: "La nascita del libro"
date: 2015-04-27
comments: true
tags: [Lovejoy, 9t05Mac, MacBookPro, iPad, Scrivener, iPad, Word, Anteprima, Skim, Goodreader, ebook, ePub, iBooks, Kickstarter]
---
Ben Lovejoy ha fatto un po’ di pubblicità al romanzo che ha in preparazione con un *post* su *9to5 Mac* dove racconta in breve i [dietro le quinte](http://9to5mac.com/2015/04/15/ipad-novel-editing/) del processo produttivo.<!--more-->

In poche righe si evince che ha usato per scrivere un MacBook Pro e un iPad. Con [Scrivener](https://www.literatureandlatte.com/scrivener.php), [Pages](http://www.apple.com/it/ios/pages/) e (ugh) Word.

Aveva un gruppo di lettori *alpha* e uno *beta*, per identificare gli errori tecnici nella narrazione e quelli narrativi. I lettori-collaudatori hanno ricevuto un Pdf che hanno rispedito annotato (si può fare con Anteprima, [Skim](http://skim-app.sourceforge.net), [Goodreader](http://www.goodreader.com) per iPad e molti altri).

Per avere un’idea del prodotto finito, ha realizzato un *ebook* in formato ePub da visualizzare su iPad con [iBooks](http://www.apple.com/it/ibooks/) e stampato alcune copie su carta mediante un servizio di *print-on-demand*.

Ha infine deciso di affrontare l’autopubblicazione e finanziarla con [una campagna su Kickstarter](https://www.kickstarter.com/projects/1998560946/11-9-a-technothriller-novel), in alternativa a rapportarsi con un editore.

Vanno di moda altri concetti, ma a loro dispetto scrivere un libro vero rimane una cosa terribilmente seria e impegnativa e quello dello scrittore è un lavoro, non una cosa che ci si racconta perché ogni tanto si tira tardi sopra un file residente nel proverbiale cassetto.