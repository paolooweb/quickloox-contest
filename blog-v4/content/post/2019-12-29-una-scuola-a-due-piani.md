---
title: "Una scuola a due piani"
date: 2019-12-29
comments: true
tags: [scuola, coding, iPad]
---
Dopo anni di comunicazione pro-tecnologia, stiamo entrando in un’epoca di comunicazione anti-tecnologia e che questo succeda anche parlando di scuola mi rende perplesso.

Si prenda un articolo come quello di *Mit Technology Review*, mica gente scappata di casa: [Come la tecnologia in classe tiene arretrati gli studenti](https://www.technologyreview.com/s/614893/classroom-technology-holding-students-back-edtech-kids-education/).

*Gli educatori amano gli apparecchi digitali, ma ci sono poche prove che aiutino i bambini, specialmente quelli che ne hanno più bisogno.*

Vero che non siamo in Italia: da noi scrivere che gli educatori amano gli apparecchi digitali è falso almeno nell’ottanta percento dei casi e credo di essere ottimista.

Poi però l’articolo comincia con un esempio, un bambino di sei anni – Kevin – stimolato da una app su iPad a *combinare 8 e 3*.

Kevin non ce la fa. Siccome fatica a leggere, normalissimo a quell’età, attiva la riproduzione audio e ascolta la richiesta. Non ce la fa ugualmente.

L’autrice dell’articolo gli chiede se sappia il significato di *combinare*. Kevin non lo sa e non me ne stupisco. L’autrice suggerisce che significhi *sommare* e Kevin finalmente riesce.

Questo sarebbe un esempio dei problemi dell’applicazione della tecnologia all’istruzione.

A me sembra un esempio di software scritto da deficienti e usato nell’assenza di attenzione da parte di un maestro. Se al posto di iPad ci fosse stato un libro cartaceo, sarebbe andata diversamente?

L’articolo – su cui tornerò – contiene numeroso altro materiale, ma per ora basti una considerazione forse troppo semplice per *Mit Technology Review*.

La tecnologia digitale, per banalizzare all’eccesso, è fatta di due piani: hardware e software.

La scuola italiana, per restare su un piano familiare, fatica a salire al primo piano e ad accettare l’hardware come strumento possibile per insegnare e imparare. Faticosamente, dieci passi avanti e nove indietro, ci arriviamo, anche se il cammino è lungo assai.

Ciò che non è chiaro a molti è che la salita al secondo piano è quella veramente difficile: riempite le aule di apparecchi digitali, ci aspetta – si vedono già i prodromi – una valanga di software totalmente inadatto al contesto, agli scopi, a chi dovrà presentarlo agli studenti, agli studenti.

I risultati saranno modesti o scadenti o negativi, tu pensa. La colpa sarà del digitale.

Tornerò sull’articolo, perché tocca molti altri punti e riesce a distorcere con grande efficacia il problema, per mostrare la tecnologia applicata all’istruzione come causa di tanti problemi quando in realtà, dei problemi, si limita a mostrare l’effetto. *Garbage in, garbage out*.