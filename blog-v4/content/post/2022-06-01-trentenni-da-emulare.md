---
title: "Trentenni da emulare"
date: 2022-06-01T00:43:31+01:00
draft: false
toc: false
comments: true
categories: [Hardware, Software]
tags: [A2, Avvocati e Mac, Filippo, Strozzi, Filippo Strozzi, Roberto, Marin, Roberto Marin, Avvocati e Mac, MArCh, Newton, Ars Technica, Einstein, Apple Pencil, MessagePad, MessagePad 2100, Greg Christie, Christie, Ive, Jonathan Ive, Jony Ive, Jony, Mike Culbert, Culbert, MacOS, Dock, Capps, Steve Capps, NewtonScript, JavaScript, Arm, Acorn Risc Machine, Apple Silicon, Ars Technica, 512 Pixels, Hackett, Steven Hackett]
---
Mentre si [chiacchierava nel podcast A2](https://macintelligence.org/posts/2022-05-31-eravamo-tre-amici-alla-touch-bar/) (raramente mi sono divertito come ieri sera e oggi sono afono per tutto il tempo passato a parlare), è passata la domanda su che cosa pensassi di [Apple Pencil](https://www.apple.com/it/apple-pencil/). *Tutto il bene possibile* ho risposto, l’ho provata e riprovata, è fantastica – Roberto ha commentato che si tratta del futuro per i grafici professionisti, mi pare – ma non fa per me. Sono una persona assai poco grafica, se esprimo qualche creatività lo faccio tramite il testo, se devo prendere appunti preferisco un *outliner* a una app per scrivere a mano, l’audio, le foto, al limite uno schizzo se proprio non si può farne a meno, usando il dito.

Poi ho ripensato a Newton, quando ho venduto il mio PowerBook Duo pur di avere un MessagePad 2100 dopo che Apple aveva annunciato la fine della produzione. Il mio portatile per qualche anno, indimenticabile.

Ho usato tantissimo lo stilo. Conservo nel mio MessagePad (che ancora si accende) gli appunti in inchiostro digitale presi in innumerevoli riunioni degli anni novanta e sfotto gli altri reduci: *ma gli appunti delle riunioni li hai ancora?* Però non si poteva farne a meno, su Newton. Peraltro, il sistema operativo era stato costruito letteralmente attorno all’input via stilo. Era piacevolissimo usarlo, ma era un’altra macchina, di altri tempi.

La lettura consigliata per l’anniversario di Newton è il lungo e accurato [resoconto di *Ars Technica*](https://arstechnica.com/gadgets/2022/05/remembering-apples-newton-30-years-on/). Mi prendo la libertà di una traduzione più lunga del solito.

>Oggi Newton viene ricordato da pochi e considerato un progetto fallito nel giro di pochi anni.

>La verità non è così semplice. Molti che lavorarono su Newton diventarono figure di spicco del team iPhone; Mike Culbert, Greg Christie, perfino Jony Ive hanno lavorato a Newton. Molte delle idee nate lì si sono fatte strada su iPhone e iPad. Alcune minori, come l’animazione a nuvola di fumo che mostra la cancellazione di qualcosa ed è arrivata nel Dock di MacOS, oppure l’icona orologio costantemente aggiornata, che Steve Capps sfidò il team iPhone a ricreare.

>Altre influenze sono state invece ben più profonde. Newton aveva un assistente intelligente che permetteva di svolgere attività usando il linguaggio comune. La base da cui partono Siri e l’assistente vocale di Google. Aveva una ricerca universale su tutti i dati e tutte le applicazioni, ricreata in Spotlight e in certe funzioni dei sistemi di Google. Ha portato avanti da pioniere l’idea dell’input basato su form validati, oggi un componente importante di siti e app. E NewtonScript ha influenzato la creazione di JavaScript […] che è oggi il linguaggio di programmazione più diffuso al mondo.

>Infine ma per niente da ultimo, la piccola società inglese che ha creato il processore Acorn Risc Machine fece confluire il progetto del chip in una azienda separata. I progetti Arm oggi azionano quasi ogni singolo telefono cellulare e tavoletta nel mondo e sono il cuore di Apple Silicon. Non male per un investimento iniziale di tre milioni di dollari!

Qualcosina, Newton lo ha significato. Un trentenne così ispiratore e talentuoso è solo da ammirare e, naturalmente da emulare.

L’emulatore esiste. [Si chiama Einstein](https://512pixels.net/2022/05/einstein/) e richiede purtroppo il *dump* di una Rom originale, di fatto limitando alquanto le possibilità di utilizzo. Però c’è, è capace persino di stampare e consente di rivivere quell’esperienza lontana eppure così tanto anticipatrice e avanti sui tempi.