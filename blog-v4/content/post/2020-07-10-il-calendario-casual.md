---
title: "Il calendario casual"
date: 2020-07-10
comments: true
tags: [Whenever, Moruzzi]
---
Quasi mai trovo veramente degna della fatica una app destinata a sostituire qualcosa della dotazione standard di iOS (il contrario che su Mac, dove è la prima cosa da fare).

Come mi segnala [Massimo](https://www.dotcoma.it), [Whenever](https://whenever.io) potrebbe essere davvero una segnalazione degna di nota. Il primo calendario che, a leggere la pagina di presentazione, mi dice qualcosa di interessante e non del tutto scontato.

Lo provo.