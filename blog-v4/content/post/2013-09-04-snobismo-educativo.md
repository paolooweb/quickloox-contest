---
title: "Snobismo educativo"
date: 2013-09-04
comments: true
tags: [iPad]
---
La tesi sottostante l’[articolo](http://www.theatlantic.com/technology/archive/2013/08/go-ahead-mess-with-texas-instruments/278899/) recentemente pubblicato da Phil Nichols su *The Atlantic* è che esiste un momento di apprendimento in cui si segue il maestro e un altro momento di apprendimento per così dire sovversivo, nel quale si acquisisce conoscenza mediante l’uso non ortodosso degli strumenti a disposizione.<!--more-->

Ed è anche molto condivisibile, tutte le volte che si smonta qualcosa o si cercano nuove strade di utilizzo si impara moltissimo.

Però poi arriva a scrivere che le tecnologie educative dovrebbero somigliare più alle calcolatrici grafiche che non agli iPad. I quali sarebbero chiusi, impossibili da programmare, meno adatti a essere “smontati”. Quando invece l’autore, da giovane, si era appassionato alla sua Texas Instruments che alla fine riusciva persino a eseguire giochi.

L’articolo prosegue su questa falsariga e dice cose molto condivisibili in senso generale.

Però sugli iPad proprio non ci siamo. Mica per difendere iPad, ma perché i tempi sono cambiati. Non voglio fare per l’ennesima volta l’elenco delle *app* con cui si può programmare su iPad (uno su tutti, [Editorial](https://macintelligence.org/posts/2013-08-18-utile-e-inutile/), che non è neanche il punto).

Con un iPad, a parte ogni altra considerazione, posso scrivere *web app* di potenza inaudita, capaci di funzionare su miliardi di *browser*. Ne facciamo ancora un problema di poter vedere la cartella home, se l’obiettivo è imparare? Prova a scrivere una *web app* con una calcolatrice grafica.

Per non parlare del fatto che su iPad girano numerose *app* che lo trasformano in… calcolatrice grafica ed esiste [Sage](http://sagemath.org).

Non è un argomento che si possa affrontare decentemente esordendo con *ai miei tempi…*