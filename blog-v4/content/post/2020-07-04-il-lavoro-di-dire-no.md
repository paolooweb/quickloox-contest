---
title: "Il lavoro di dire no"
date: 2020-07-04
comments: true
tags: [Torvalds, Jobs, Linux]
---
Linus Torvalds tiene [il proprio intervento](https://www.youtube.com/watch?v=H8Gd9t7FQqI) durante l’evento Open Source Summit and Embedded Linux Conference: Europe. A un certo punto gli chiedono come si articoli la sua giornata. Risponde che legge un sacco di posta, ne scrive molta è praticamente non programma più, perché scrive il codice direttamente nel messaggio oppure invia pseudocodice. Poi:

> Leggo molta più posta di quella che scrivo, perché alla fine il mio lavoro è dire no. Qualcuno deve poter dire no alla gente. Perché gli altri sviluppatori sanno che che se fanno qualcosa di sbagliato, io dirò no. […] Tuttavia, per poter dire no, devo conoscere il retroscena. Per questo passo praticamente tutto il mio tempo a leggere posta riguardante quello su cui sono al lavoro le persone.

<iframe width="560" height="315" src="https://www.youtube.com/embed/H8Gd9t7FQqI" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

A me viene in mente un altro personaggio conosciuto in ambito informatico, noto per [avere parlato del dire no](https://www.youtube.com/watch?v=H8eP99neOVs):

> Focalizzarsi è dire no […] alle centinaia di altre buone idee. […] In realtà sono orgoglioso delle cose che non abbiamo fatto così come di quelle che ho fatto. L’innovazione è dire no a mille cose. Bisogna scegliere con attenzione.

<iframe width="560" height="315" src="https://www.youtube.com/embed/H8eP99neOVs" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Se di una cosa possiamo essere certi, è che Torvalds non si è ispirato a Jobs. Se due persone tanto diverse arrivano a un punto di vista comune, c’è da rifletterci.