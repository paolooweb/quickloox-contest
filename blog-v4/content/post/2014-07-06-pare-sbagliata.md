---
title: "Pare sbagliata"
date: 2014-07-07
comments: true
tags: [Apple, Microsoft, Windows, Windows7, Gruber, Moltz, iPad, OSX, iOS]
---
Apple ha scelto la strada di una [esperienza continua](https://www.apple.com/it/ios/ios8/continuity/) tra apparecchi e sistemi operativi, mentre Microsoft ha scelto l’altra strada di un unico sistema operativo valido per tutti, sui cinquanta pollici come sui cinque.<!--more-->

Per di più, sempre sullo stesso sistema operativo. Strada sbagliata, [scriveva](http://daringfireball.net/2011/06/windows_8_fundamentally_flawed) John Gruber su *Daring Fireball*:

>È un’idea fondamentalmente bacata quella di costruire il sistema operativo e l’interfaccia della prossima generazione sopra il Windows esistente. L’idea è che le cose nuove arrivino a fianco di Windows come lo conosciamo. Microsoft sta evidentemente cercando di imparare da Apple, ma chiaramente non ha capito perché iPad usa iOS e non Mac OS X.

Gruber scriveva questo *tre anni fa*. E continua:

>Poter eseguire app Mac OS X su iPad, con pieno accesso a filesystem periferiche eccetera, farebbe iPad peggiore, non migliore. iPad ha avuto successo perché ha eliminato complessità, non perché l’ha coperta con un guscio sensibile al tocco.

Questa la conclusione:

>La nozione radicale di Apple che i personal computer a tocco dovrebbero considerare compromessi grandemente differenti rispetto ai computer tradizionali. E che non si può progettare un sistema che risolve tutto dappertutto. Windows 8 ci prova e non credo si possa fare. Non si può produrre qualcosa di concettualmente leggero se si porta dietro 25 anni di bagaglio Windows.

Era il primo giugno. Il primo luglio di tre anni dopo, John Moltz [commenta])(http://verynicewebsite.net/2014/07/windows-threshold/)la notizia della prossima versione importante di Windows, primavera 2015, con uno degli scopi principali che sarebbe aumentarne il gradimento per chi è ancora rimasto a Windows 7. Così:

>Oltre alla fallacia concettuale di realizzare un sistema operativo buono per desktop e mobile, c’è anche un problema di marketing. Apple ha reso gradevole iOS ai suoi clienti (e molti altri) perché lo ha separato da OS X. Se Apple avesse imposto sul desktop l’interfaccia di iPad, saremmo tutti ancora su Tiger.

Sbagliata l’idea dell’uno per tutti, sbagliata l’idea che sul *desktop* debba affermarsi il *touch*.

>Si può certamente accreditare Microsoft di avere provato qualcosa di nuovo, ma non ha funzionato. E stanno ancora cercando di capire perché.

Tre anni e ancora il buio. Tutto questo serviva peraltro a rendere evidente un’altra questione: quelli che parlano di unificazione tra iOS e OS X non potrebbero trovare smentita maggiore. Dopo avere preso la strada giusta, Apple dovrebbe incamminarsi su quella sbagliata? No. Non questa Apple, per lo meno.