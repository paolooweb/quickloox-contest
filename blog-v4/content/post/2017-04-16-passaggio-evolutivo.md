---
title: "Passaggio evolutivo"
date: 2017-04-16
comments: true
tags: [Roll20, JavaScript, Ruby, GitHub, Firebase, WebGL, Canvas, MediaWiki, NodeJS, PostgreSql, Html, Css]
---
Mi hanno di nuovo messo in relazione Office a scuola con il riuscire a trovare lavoro una volta terminata la scuola stessa.<!--more-->

Come è, se non noto, almeno ripetuto, gioco di ruolo con gli amici e succede di usare [Roll20](http://macintelligence.org/blog/categories/roll20/) per le mappe e l’interazione.

Roll20 ha annunciato di recente la ricerca di personale tecnico con la seguente gamma di competenze:

>Buone capacità con JavaScript, Html/Css e Ruby; familiarità con la conduzione di server Linux; solide capacità di scrittura e comunicazione.

Verrà data priorità a chi possedesse anche i requisiti che seguono:

>Un nutrito portfolio di cose già fatte, visibile su GitHub o sito equivalente; esperienza con Firebase; esperienza con MediaWiki; esperienza con WebGL, Canvas, e/o NodeJS; esperienza lavorativa con PostgreSQL; esperienza di gioco con Roll20 e di programmazione con le relative interfacce di programmazione.

Roll20 certo non è Ibm o Hp, ma è un lavoro vero, in quello che nel secolo passato avrebbero chiamato ambiente giovane e dinamico, con un numero di iscritti paganti significativo.

Non voglio chiedere quale scuola prepari i ragazzi a usare PostgreSQL o Firebase: sarebbe troppo semplice. Invece chiedo quale scuola prepari i ragazzi a muoversi in un contesto come quello appena citato, dove di Office frega niente a nessuno, le tecnologie finiscono tutte o quasi dentro un *browser* e il server è azionato da software libero.

Anche solo pochi anni fa, questa ricerca di lavoro non sarebbe mai esistita per impossibilità pratiche. Oggi è reale.

Pasqua, come è abbastanza noto, festeggia un passaggio; faccio tanti auguri a tutti e soprattutto a chi considera il mondo della scuola, sotto qualsiasi veste, da preside fino a studente.

Che la scuola possa passare dall’assurda dipendenza monomaniaca attuale alla libertà e capacità di mostrare quanto possa essere lavorativamente infinito il novero delle tecnologie hardware e software che, oggi, magari portano a una carriera professionale domani.
