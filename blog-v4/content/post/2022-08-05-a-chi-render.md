---
title: "A chi render"
date: 2022-08-05T01:10:57+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [Mysmartprice, iPad, Cad]
---
Trovo stupendo che spaccino come notizia dei sedicenti [*render Cad* del prossimo iPad](https://www.mysmartprice.com/gear/ipad-2022-cad-renders-exclusive/) che a me sembrano un’esercitazione per gli ultimi arrivati nella classe di modellazione.

Ma la cosa fantastica è che l’unica cosa vagamente interessante su cui si poteva fare qualche affermazione cruciale, tipo l’uso di Usb-C oppure di Lightning, è stata accuratamente evitata. Chissà per quale mistero, la porta di connessione è stata coperta da una pecetta rossa.

Se c’era il vuoto, perché non mostrarlo? Se invece c’era qualcosa, perché coprirlo?

Dico io: perché questo è il render Cad fatto nel sottoscala del sito giusto per riempire una pagina.

*E che connettore ci mettiamo?*

*Fai Usb-C*.

*Ma se poi tengono Lightning?*

*Mettici una roba rossa e copri tutto.*

Ma quanto rende fabbricare notizie false a questo livello? Che questi signori riescano a guadagnare in questo modo mi stupisce ogni volta, molto più dei render.