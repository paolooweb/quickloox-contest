---
title: "C’era una volta il jailbreak"
date: 2017-11-27
comments: true
tags: [Jailbreak, iOS]
---
Quegli anni che mi parlavano del *jailbreak* di iPhone in rapida diffusione, un pericolo per la piattaforma; o meglio un segnale che iOS era troppo chiuso e bisognava aprirlo esponendosi a tutte le problematiche di sicurezza relative, nel nome della libertà assoluta di fare quello che si vuole con il proprio computer. Anche da parte di chi non ha idea di che cosa stia facendo o vuole mettere volontariamente a rischio l’integrità del sistema.<!--more-->

Oggi *MacRumors* scrive dei [repository di jailbreak che chiudono](https://www.macrumors.com/2017/11/23/modmyi-macciti-cydia-repos-shut-down/): i vantaggi reali o presunti sono sempre meno e sempre minori, la procedura è rimasta sempre [poco alla portata dell’utente comune](https://motherboard.vice.com/en_us/article/8xa4ka/iphone-jailbreak-life-death-legacy), fuori da interessi veramente molto specifici ha oramai scarso senso. Fare funzionare un iPhone originale in Italia richiede forzatamente il *jailbreak*, per esempio, e l’ho praticato senza remore. A parte questo, rimane ben poco.

Divertente provarci, la sfida tecnica, certo; la presunta ribellione, la supposta libertà, l’onnipresente *faccio quello che voglio* privo di *conosco quello che faccio* forse sono finiti a raccogliere *malware* su Android, o chissà. Comunque, alla prova del tempo, erano chiacchiere.