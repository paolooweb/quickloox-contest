---
title: "Alla venticinquesima"
date: 2017-12-27
comments: true
tags: [PCalc]
---
L’unica storia degna di essere raccontata oggi è quella della [venticinquesima annata di PCalc](http://tla.systems/blog/2017/12/23/a-long-time-ago-in-a-glasgow-far-far-away/).<!--more-->

Da un quarto di secolo c’è una calcolatrice che mai ha deluso, gira ovunque purché sia Apple ed è sempre aggiornata nonostante contenga ancora al proprio interno qualche pezzo del codice originale del 1992, scritto sufficientemente bene da sfidare il tempo.

Cinque anni fa, in occasione del ventesimo anniversario, l’autore di PCalc James Thompson ha riassunto [la storia del programma](http://www.pcalc.com/english/twenty.html) dalle sue origini. Lettura semplice ma esemplare. L’ho già detto: se un programma indipendente dura venticinque anni, ha dentro qualcosa che trascende il banale *fatto bene*.

Ai prossimi.