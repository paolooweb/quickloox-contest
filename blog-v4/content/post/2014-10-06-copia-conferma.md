---
title: "Copia conferma"
date: 2014-10-06
comments: true
tags: [Samsung, Apple, Microsoft, Jobs, Parc, Xerox]
---
Apple porta in tribunale Samsung per una questione di brevetti e i soliti commentano che è ridicolo, semmai è il contrario, tutti copiano da tutti, però anche Apple copia e si finisce sempre alla famosa visita di Steve Jobs al Palo Alto Research Center di Xerox (dove [non ha copiato niente](https://macintelligence.org/posts/2014-08-25-azioni-che-contano/) e anche se lo avesse fatto, ha pagato).<!--more-->

Nel mentre salta fuori una [lite giudiziaria tra Microsoft e Samsung per via di interessi non pagati](https://www.scribd.com/doc/241842955/Microsoft-v-Samsung-Unsealed-Suit), su somme che i coreani versano agli americani nell’ordine di un miliardo di dollari per il 2013.

La morale è che Samsung forse non copia Apple, ma certamente copia qualcun altro e in modo pesante, se sborsa un miliardo di dollari l’anno.

Oppure che Apple paga pessimi avvocati.

<p  style=" margin: 12px auto 6px auto; font-family: Helvetica,Arial,Sans-serif; font-style: normal; font-variant: normal; font-weight: normal; font-size: 14px; line-height: normal; font-size-adjust: none; font-stretch: normal; -x-system-font: none; display: block;">   <a title="View Microsoft v Samsung Unsealed Suit on Scribd" href="https://www.scribd.com/doc/241842955/Microsoft-v-Samsung-Unsealed-Suit"  style="text-decoration: underline;" >Microsoft v Samsung Unsealed Suit</a> by <a title="View inafried's profile on Scribd" href="https://www.scribd.com/inafried"  style="text-decoration: underline;" >inafried</a></p><iframe class="scribd_iframe_embed" src="https://www.scribd.com/embeds/241842955/content?start_page=1&view_mode=scroll&access_key=key-vB1YBCdIRY6lwYS0im6n&show_recommendations=true" data-auto-height="false" data-aspect-ratio="0.7727272727272727" scrolling="no" id="doc_20063" width="100%" height="600" frameborder="0"></iframe>