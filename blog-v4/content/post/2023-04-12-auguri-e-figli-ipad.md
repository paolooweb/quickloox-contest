---
title: "Auguri e figli iPad"
date: 2023-04-12T12:53:03+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [BBEdit]
---
Oggi [BBEdit](https://www.barebones.com/products/bbedit/bbedit14.html) compie trentuno anni, che per un software, per giunta indipendente, per giunta al quadrato solo Mac, equivarrebbero forse a centocinquanta per un umano.

Però preferisco pensarlo come se fosse un umano, nel fiore degli anni, con un grande avvenire davanti.

Auspico che metta infine la testa a posto, si prenda le sue responsabilità e nel dovuto tempo di gestazione ci faccia nascere una bella versione per iPad, che la aspetto da sempre.

Non vorrà mica altrimenti diventare un bamboccione egoriferito.