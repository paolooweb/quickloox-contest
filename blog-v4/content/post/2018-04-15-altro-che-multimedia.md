---
title: "Altro che multimedia"
date: 2018-04-15
comments: true
tags: [iPad, Caldwell, iMore]
---
Quello che [ho riportato](https://macintelligence.org/posts/2018-04-04-ci-siamo-quasi/) dalla rete sul nuovo iPad *education* [lo ha ripreso](https://www.imore.com/my-97-ipad-2018-review-drawn-written-edited-and-produced-ipad) anche Serenity Caldwell di *iMore*.

Solo che lei ha scritto, disegnato, montato, filmato e animato una recensione di quasi sette minuti, tutta sull’iPad in questione, con l’aiuto di una quindicina di app ampiamente disponibili al pubblico con spesa modica, quando non gratis. E, appunto, ripete:

>Sì, iPad è più costoso di un Chromebook. Sì, ha molta strada da percorrere prima di essere lo strumento perfetto da usare in classe.

Poi aggiunge:

>Ma non posso fare un quinto di quello che ho fatto su questo iPad con Apple Pencil su qualsiasi altra tavoletta dello stesso prezzo, compresi i vecchi iPad. Questa configurazione brucia la concorrenza e l’ho già vista mettere radici in persone che avevano altrimenti escluso l’idea di un iPad.

Se lo può permettere. E il prezzo di iPad c’entra relativamente. Basta guardare il video per capire che il concetto stesso di *multimedia* come mescolanza di media diversi è definitivamente saltato. Il medium è uno solo e incorpora qualsiasi tipo di contenuto si voglia, posto che il proprietario possa occuparsene.