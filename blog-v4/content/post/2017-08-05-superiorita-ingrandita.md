---
title: "Superiorità ingrandita"
date: 2017-08-05
comments: true
tags: [iPhone, Android, globuli, malaria, Sasha]
---
Dopo [quello dell’altroieri](https://macintelligence.org/posts/2017-08-03-il-dono-delle-lingue/), ecco un altro gradito contributo di **Sasha**.

**Quelli nella foto sono globuli rossi.**

 ![Globuli rossi fotografati da iPhone via microscopio](/images/globuli.jpg  "Globuli rossi fotografati da iPhone via microscopio") 

Le frecce ne indicano un paio contagiati dal parassita della malaria (*plasmodium falciparum*).

La foto è stata scattata con iPhone 5S appoggiato al binoculare di un comune microscopio presente in qualunque laboratorio analisi 😁.

La dottoressa presente è stata fortunata per due motivi: anzitutto, non è così comune beccare tre parassiti nello stesso globulo rosso (al centro). In seconda battuta, c’era il mio iPhone in circolazione: il suo dispositivo Android faceva gran fatica a mettere a fuoco e il lag tra l’inquadratura e quanto visualizzato a schermo **era imbarazzante**.
