---
title: "La linea è libera"
date: 2016-12-07
comments: true
tags: [TIM, LibreOffice, Office]
---
*Scoop:* da qui a pochi giorni Tim adotta internamente [LibreOffice](http://www.libreoffice.org).<!--more-->

Bel colpo per il software libero e un bel pedatone a quello proprietario che rende prigionieri.

E fa piagnucolare perché *si è costretti* a usarlo; chissà questa aziendina minuscola e misconosciuta che è Tim, come farà adesso.

Mi vedo i dipendenti smarriti che girano su se stessi in cerca di soluzioni fino a librarsi in volo e svanire verso la stratosfera. O più probabilmente no.

Lo ripeto: Tim adotta LibreOffice e, non fosse chiaro, abbandona Office.

Non è una azienda che amo, però devo ringraziarli per questo bel regalo di Natale. E grazie anche alla mia fonte.