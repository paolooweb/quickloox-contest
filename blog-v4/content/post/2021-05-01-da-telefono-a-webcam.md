---
title: "Da telefono a webcam"
date: 2021-05-01T01:06:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [iPhone, EpocCam] 
---
Un [nuovo monitor più grande di quello prima](https://macintelligence.org/posts/E-naufragar-mè-dolce.html), su cui stanno più cose, mi ha fatto venire voglia di usare iPhone come webcam di Mac.

Normalmente preferisco collegarmi a riunioni e occasioni online direttamente con iPhone, mentre effettuo un secondo collegamento con Mac se devono condividere schermate o comunque lavorare su dati e usare la chat della riunione. Tuttavia non è sempre necessario e, qualche volta, risulta persino controproducente: per esempio, durante le nostre sessioni di [Dungeons & Dragons](https://dnd.wizards.com) su [Roll20](https://roll20.net) è una cosa senza senso. Tutta l’interazione avviene sulla mappa, mentre la stessa piattaforma provvede all’interconnessione audiovideo. Avere una webcam sul Mac basterebbe e avanzerebbe, se il mio Mac non fosse un Mac mini.

Così ho preso una decisione di impulso e ho scaricato [EpocCam](https://apps.apple.com/us/app/epoccam-webcam-for-mac-and-pc/id449133483), software per iPad e iPhone che costruisce un collegamento con Mac, divenendone appunto la periferica audiovideo.

EpocCam funziona grazie allo scaricamento di un driver da installare su Mac, dopo di che l’obiettivo è raggiunto. Il driver può essere scaricato direttamente da dentro la app, via AirDrop, oppure autonomamente da web. Titubavo all’idea di installare un software del genere su Mac e sono stato rinfrancato dall’etichetta della app con i suoi valori di privacy: App Store dice che EpocCam non raccoglie dati dell’utente e allora ho proceduto a cuore leggero.

Ho installato il driver tramite AirDrop in un attimo, proprio in vista della sessione serale su Roll20. Tutto ha funzionato bene nel complesso, con un po’ di confusione mia per trovare la configurazione giusta. Per vari motivi volevo gestire l’audio dagli auricolari collegati direttamente a Mac e avere da iPhone il solo feed video. Alla fine ce l’ho fatta però e l’insieme ha funzionato.

La non-recensione si ferma qui perché ho avuto solo il tempo di installare il software e trovare la quadra prima di mettermi a giocare. Il programma offre una milionata di funzioni, tra cui persino il *chromakey*, che devo approfondire. In più ignoro totalmente a che punto sia la concorrenza e se l’acquisto sia stato azzeccato. EpocCam si installa gratis e si usa seppure con limitazioni; la spesa di otto euro e novantanove centesimi sblocca la situazione.

Complessivamente sono soddisfatto, ma durante la nostra sessione di gioco – circa tre ore – è accaduto che la connessione si perdesse e quindi Mac rimanesse senza feed video. EpocCam si collega via Wi-Fi oppure via cavo Usb. Ho scelto la prima opzione ed è successo che si perdesse il contatto. Riattivare la connessione con intervento manuale è un momento e ha funzionato per le poche volte che è successo. Però, per esempio, mi capita di partecipare a dirette Facebook a scopo professionale; vista questa esperienza, non credo che mi collegherò usando EpocCam via Wi-Fi.

Penso che il collegamento via cavo Usb possa restituire una situazione diversa; lo vedrò una prossima volta.

*Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile.*