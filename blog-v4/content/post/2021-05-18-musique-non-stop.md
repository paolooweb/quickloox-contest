---
title: "Musique Non Stop"
date: 2021-05-18T00:34:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [Apple Music, Dolby Atmos, Spatial Audio, iTunes, AAC, Alac, lossless, Mp3, Flac] 
---
Una delle notizie di oggi è [il passaggio del contenuto di Apple Music a Spatial Audio, Dolby Atmos e Lossless Audio](https://www.apple.com/newsroom/2021/05/apple-music-announces-spatial-audio-and-lossless-audio/), con una versione di audio lossless addirittura a centonovantadue chilohertz per chi avrà banda da nazione evoluta e contratto da abbiente.

A quel tempo, nel secolo scorso, una generazione indietro nel tempo, non avrebbero mai pensato di fare i virologi, però avevano già smesso di allenare la Nazionale; tutti audiofili.

Facevano le pulci ad Aac contestando che fosse meglio di Mp3. Gli snob sbandieravano Flac, vantandosi che averlo su iTunes fosse da difficile a confuso, al contrario del loro player di nicchia che immancabilmente faceva molte più cose ed era più gratis (si sa che le cose Apple costano).

E poi il Drm, la musica non è più tua, se domani si spengono i server, metti che succeda la guerra, le cavallette, l’eclissi, io l’ho comprato – che cosa, non importa – e ci faccio quello che voglio. Il disco rigido era troppo piccolo chiaramente e perché butti via spazio per scaricare in Apple Lossless quando mio cuggino ti passa la chiavetta con dentro un litro e mezzo di musica che suona benissimo, non so che musica sia, ma tanto per ascoltare con le cuffiette va bene tutto.

I server sì, li hanno spenti, [quelli Microsoft](https://www.windowscentral.com/microsoft-shuts-down-groove-music). [Due volte](https://www.cnet.com/news/defunct-msn-music-has-a-drm-controversy-on-its-hands/). [Tre](https://support.microsoft.com/en-us/topic/support-for-rights-managed-purchases-in-zune-69161d07-acae-1fdf-f98f-e0916758e49b), volendo. Gente con una vocazione vera per la musica, nessun dubbio.

Di audiofili che mi abbiano spiegato come esistesse una roadmap che portava in venticinque anni allo streaming ad altissima qualità, non ho avuto la ventura di incontrarne. Tutti espertissimi sul presente. Eppure ho la sensazione che nel cassetto di Eddy Cue, o Steve Jobs, o Craig Federighi (di nascosto), una roadmap scarabocchiata su un foglietto ci sia sempre stata.

Ora mi rilasso, che domani ho da leggere un’onda di commenti sulla superiorità del vinile. Gli ultracinquantenni con orecchio assoluto sono una percentuale insospettabile di popolazione, specialmente il giorno dopo un annuncio di Apple sulla [Musique Non Stop](https://www.youtube.com/watch?v=O0lIlROWro8).

<iframe width="560" height="315" src="https://www.youtube.com/embed/O0lIlROWro8" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

*Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile.*               