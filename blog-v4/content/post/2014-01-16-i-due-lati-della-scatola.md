---
title: "I due lati della scatola"
date: 2014-01-17
comments: true
tags: [Box, iOS]
---
Una scatola, come è noto, ha due lati: dentro e fuori.<!--more-->

Vedo analogamente due modi di commentare la notizia che [Box](https://www.box.com) regala cinquanta gigabyte di spazio disco Internet gratis a chi scarica la [app per iOS](https://itunes.apple.com/it/app/box-for-iphone-and-ipad/id290853822?l=en&mt=8).

Per la prima volta è disponibile in modo immediato un disco su Internet capace teoricamente di contenere l’intera massa di dati possibile su molti iPad e iPhone (salvo [iPad da 128 gigabyte](http://store.apple.com/it/buy-ipad/ipad-air/128gb-grigio-siderale-wi-fi) che rimane un caso particolare).

Altrimenti, cinquanta gigabyte è il prezzo che bisogna pagare per tentare di schiodare la gente da [iCloud](https://www.icloud.com) e [Dropbox](https://www.dropbox.com).

Mi sono iscritto comunque.