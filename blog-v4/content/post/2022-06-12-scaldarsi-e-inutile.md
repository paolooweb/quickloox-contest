---
title: "Scaldarsi è inutile"
date: 2022-06-12T00:36:13+01:00
draft: false
toc: false
comments: true
categories: [Hardware]
tags: [Mac mini, Apple Silicon, M1]
---
Stasera in casa ci sono oltre trenta gradi.

Mac mini M1 è appena tiepidino. È in casa da inizio anno e ancora devo sentire il rumore delle ventole.

L’anno scorso di questo periodo, il mio Mac mini Intel muggiva e dovevo limitare al massimo il numero di app aperte se volevo riuscire a lavorare.

Alla fine Apple ha risolto brillantemente il problema del surriscaldamento dei computer. Ha progettato un chip che non scalda e un computer per farlo funzionare tranquillo.

Era così difficile?