---
title: "Uno sguardo a freddo"
date: 2018-02-01
comments: true
tags: [Surface, Pro]
---
Mi segnala (grazie!) **Fëarandil** di possessori di tavolette Surface Pro 4 fuori garanzia il cui schermo si mette a sfarfallare, che risolvono il problema temporaneamente (*dieci minuti per avere mezz’ora di utilizzo*) con la tecnologia del freddo: [lo mettono in freezer](https://www.theverge.com/2018/2/1/16958954/microsoft-surface-pro-4-screen-flickering-issues-flickergate).<!--more-->

Dalla lettura si evince che attualmente il problema riguarda *meno dell’uno percento* degli apparecchi e che ci sono almeno milleseicento utenti affetti dal problema.

È un numero di poca importanza, di portata irrisoria rispetto al totale delle vendite – per quanto [modesto](https://macintelligence.org/posts/2017-05-02-pre-verita/) – ma è un numero.

Quante differenze rispetto a quando si parla di Mac o iPad. Si quantificano i modelli difettosi (meno dell’uno percento), si contano gli utenti insoddisfatti (mentre di solito siamo a *c’è un forum che raccoglie quelli che protestano*), nessuna petizione da firmare, nessun accenno all’obsolescenza programmata, nessun *quando c’era Gates era diverso*, nessun *la qualità non è più quella di una volta*, nessun *i professionisti abbandonano Surface*.

Non sono a conoscenza di gente che metta iPad in frigorifero per farlo funzionare. Potrebbe essere dovuto all’arretratezza tecnologica di iPad, notoriamente indietro rispetto a Surface, che è l’innovazione.