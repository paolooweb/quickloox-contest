---
title: "Non-recensioni: iPad Pro 12,9”"
date: 2018-11-27
comments: true
tags: [iPad, Pro]
---
Quando la tecnologia ti pone davanti a un dilemma futile quanto insuperabile.

Ho sempre preferito i grandi schermi, perché sono più produttivi. Il mio ultimo portatile è stato un diciassette pollici.

Contemporaneamente ho apprezzato iPad per la possibilità di lavorare tenendolo in mano e, all’occorrenza, digitare con i due pollici e la tastiera spezzata in due tronconi scorrevoli sui lati dello schermo.

L’iPad corrente, in terza generazione del 2012, è una macchina formidabile e per due settimane è stata l’unica macchina a disposizione. Lento, macchinoso nel 2018 per la mancanza del multitasking a schermo, ma all’altezza del compito. Fino al momento in cui mi sono trovato in un progetto collaborativo che utilizza [Canva](https://www.canva.com/app/). La app di Canva compatibile con iPad era troppo vecchia per funzionare bene e Safari, per l’interfaccia web, pure.

Arriva un momento in cui il mondo ti fa sapere che, per quanta sia la buona volontà, è ora di cambiare macchina.

Sì è creato il dilemma di cui all’inizio: scontata la scelta di un iPad Pro per avere il multitasking, scegliere il modello da 12,9", ottimizzato nelle dimensioni e capace di essere preso in mano nonostante le dimensioni, o l’11", ottimizzato nello schermo e perfetto per replicare la mattonella che ho sfruttato all’estremo per sei anni, e che grazie allo schermo maggiorato permette comunque il multitasking?

Cambiavo idea ogni due ore e a volte volevo lo schermo grande, a volte la comodità nel maneggiare l’oggetto. Alla fine sono andato in Apple Store (fosse per il sito sarei ad aspettare) nel momento in cui vinceva lo schermo grande ed eccomi qui a scrivere questo post su un 12,9".

Sono impressionato.

Prima di decidere ho provato più volte e per molto tempo i due modelli in negozio. Avere in mano la tua macchina è però tutt’altra situazione. Abituato a un iPad vecchio di sei anni, ero quasi intimidito già all’apertura della scatola.

(iPad Pro esce in confezione cellofanata, con linguetta adesiva per togliere il cellophane senza bucarlo o strapparlo).

Il pezzo di tecnologia dentro la scatola è un punto di arrivo. È sottile, leggero, la cornice oramai simbolica, l’assenza del pulsante Home. Pesa come il mio vecchio iPad ma, su una superficie tanto più ampia, trasmette una leggerezza quasi sovrannaturale.

Lo schermo è fantastico. Le schermate di attivazione stanno su uno sfondo bianco latte uniforme e denso, che pare organico. L’illuminazione è perfetta in ogni punto dello schermo e TrueTone si offre di calibrare continuamente lo schermo in funzione delle condizioni ambientali. È possibile avere un’anteprima di come sarebbe l’immagine senza TrueTone; al momento preferisco di gran lunga tenerlo attivo. Tendo a tenere l’illuminazione bassa, ma ho provato ad alzarla: il risultato è sontuoso, mai vista una qualità così.

Il tocco è lieve è perfetto, la risposta scattante, gli scorrimenti fluidi: sarà che arrivò da una macchina vecchia, sento di avere in mano l’ideale platonico di iPad, quello nella mia mente, materializzato.

Con un iPad nuovo posso avere Pay, ma sono obbligato a tenere attivo un codice di sblocco, altrimenti non se ne parla. Detesto il codice di sblocco… ma c’è Face ID.

Nove volte su dieci Face ID sblocca iPad in un istante, in modo naturale. E proseguo domani, o non finisco più.