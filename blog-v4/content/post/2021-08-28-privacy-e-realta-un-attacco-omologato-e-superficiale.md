---
title: "Privacy e realtà: un attacco omologato e superficiale"
date: 2021-08-28T00:49:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [Edward Snowden, Apple, Csam, iPhone, Mit Technology Review, WhatsApp, Microsoft, Google, iMore, Ncmec, Imgr, SnapChat, iCloud, Instagram] 
---
Ho illustrato ieri [il contesto dentro il quale Edward Snowden ha duramente attaccato il sistema di Apple per salvare capra e cavoli](https://macintelligence.org/posts/2021-08-27-privacy-e-realta-un-triplo-salto-mortale-carpiato-con-avvitamento/), ossia salvaguardare la privacy delle persone e nel contempo individuare materiale fotografico abusivo di minori.

Lo riassumo rapidamente: Apple è nel mirino dei politici per non avere politiche di scansione becera del proprio cloud e, a causa di questo, effettuare una quantità risibile di segnalazioni alle autorità. Al tempo stesso non ha cifratura completa su iCloud, il che la espone ulteriormente alle richieste delle autorità. Quindi sta sviluppando un metodo per arrivare alla cifratura totale di iCloud e, nel contempo, dare comunque ai politici quello che vogliono e cioè un contributo percepibile all’individuazione di materiale abusivo di minori.

Snowden si inserisce qui, con un articolo intitolato [La “i” che tutto vede: Apple ha appena dichiarato guerra alla tua privacy](https://edwardsnowden.substack.com/p/all-seeing-i). Mi chiedo che cosa avrebbe scritto per commentare i metodi di sorveglianza Csam ([Child Sexual Abuse Material](https://www.adfsolutions.com/news/what-is-csam)) usati da Google o Microsoft o Facebook, che stanno a quello di Apple come un martello pneumatico sta a un cesello, ma da una ricerca abbastanza accurata non sono riuscito a trovare niente di altrettanto mirato e violento. Potrei semplicemente non averlo trovato. Intanto, mi resta l’impressione che il tema sia caldo unicamente perché se ne occupa Apple.

Potrebbe anche andare bene, se ci fosse un’analisi puntuale e precisa di che cosa succede. Andrebbe benissimo se una persona con la sua reputazione e il suo passato dicesse qualcosa di speciale, che aiuti a capire, faccia un passo avanti, proponga soluzioni cui nessuno ha pensato.

Invece il suo pezzo è costruito attorno a giochini verbali, opinioni precostituite, ipotetiche che lasciano il tempo che trovano e il dibattito sulla questione esattamente uguale a prima, solo con più amaro in bocca. C’è anche quella fastidiosa attitudine del tipo *posso usare tecniche retoriche che il mio bersaglio non è legittimato a usare*, irritanti perché inutili e vuote: criticare Apple che *si sforza di parlare da un gradino che vorrebbe essere più in alto, da cui parlare in toni bassi e solenni* va bene… a patto di scrivere un articolo non impostato su toni analoghi. Precisare che si rimane *intenzionalmente lontani da toni tecnici e dettagli procedurali* è OK… solo che poi non sei più legittimato a tenere una lezione sul significato di cifratura end-to-end.

Soprattutto, i dettagli tecnici, in questo caso, sono esattamente quelli che fanno la differenza. Toglierli di mezzo significa equiparare il sistema di Apple a quello degli altri. Potrebbe magari essere peggiore, ma di certo non è uguale. E allora i dettagli contano.

Anche perché, se non contano, finisci per scrivere cose come

>Con il nuovo sistema, è **il tuo telefono** che cerca foto illegali per conto di Apple, prima ancora che le foto abbiano raggiunto i server iCloud, e – bla bla bla – se viene scoperto abbastanza “contenuto proibito”, parte una segnalazione alle autorità.

A voler giocare sui dettagli, 1) è indifferente quando parta la ricerca rispetto al lavoro dei server iCloud. È solo un espediente retorico per calcare la mano; 2) non parte affatto una segnalazione. Due inesattezze in tre righe? Sei Edward Snowden, mica il gelataio in piazza (onore e gloria ai gelatai, senza i quali non potrei esistere d’estate). Se devo essere convinto va bene, purché si usino precisione ed equilibrio.

Equilibrio usato invece per dare il colpo al cerchio e alla botte, per avere ragione da una parte e anche dalla parte opposta. Per avere sempre ragione. Senza scomodare Popper che oramai vien tirato in ballo anche per parlare del rigore della Salernitana, non puoi applicare metodi opposti e avere ragione sempre.

Di che cosa parlo? di frasi come

>Apple intende imporre un nuovo e mai così intrusivo sistema di sorveglianza su molti degli oltre *un miliardo* di iPhone…

Insomma, farla franca è impossibile o quasi. Solo che poi:

>Se sei un pedofilo professionista con una cantina piena di iPhone contaminati da Csam, Apple ti concede gentilmente di esentarti da queste scansioni semplicemente girando l’interruttore “Disabilita iCloud Photos”, un bypass che mostra come il sistema **non sia mai stato progettato per proteggere i bambini**, come vorrebbero farti credere, ma piuttosto il loro marchio. Fino a che tieni quella roba fuori dai loro server, e così tieni Apple fuori dai titoli sui giornali, Apple se ne disinteressa.

Questo argomentare è problematico. Mostra che Snowden conosce perfettamente il contesto che c’è dietro, ma lo ignora intenzionalmente. Peggio di questo, si fa passare il sistema per sorveglianza totale e ineludibile… che eludere è facilissimo, *per i criminali*. Se invece uno è una persona normale, no, l’interruttore non lo può girare. Snowden vuole avere ragione nel dire che è sorveglianza diffusa e capillare, e vuole avere ragione nel dire che è facile uscirne. Che sorveglianza è, allora? Come si può avere contemporaneamente ragione su tutto e sulla sua negazione?

*In cauda venenum*. Snowden si premura di fare sapere che il sistema è gravemente fallato. Peccato che si limiti a dare un paio di link e che il dibattito resti assolutamente dove si trovava prima. E che le obiezioni siano note ad Apple, e che il sistema ne tenga conto. Non è questo il momento di entrare nei dettagli; comunque c’è gente che si affanna a scrivere software per elaborare falsi positivi oppure immagini diverse con lo stesso hashing percettivo. Peccato che, per come ha congegnato le cose Apple, per condurre un attacco realmente pericoloso ci voglia molto di più. E nessuno, neanche Snowden, sappia se sia possibile condurlo nella pratica.

Ma naturalmente, sostiene Snowden, Apple crea un precedente che certamente porterà ad abusi del sistema da parte di governi male intenzionati.

>Che cosa accadrà se, tra pochi anni al massimo, passa una legge che proibisce la disabilitazione di iCloud Photos, costringendo Apple a scandire foto che non sono copiate su iCloud?

A parte che si tratta di una situazione inconcepibile, se fosse proibito disabilitare iCloud Photos, tutte le foto finirebbero su iCloud. Così Apple scandirebbe le foto nel cloud… esattamente come oggi fanno Facebook, Microsoft, Google, TikTok, Amazon, chiunque. Apple avrebbe speso tempo e denaro per nulla. Ma questo significa anche che il sistema proposto da Apple, almeno sulla carta, è *migliore* della becera scansione del cloud.

>Che accade quando un partito indiano domanda che vengano cercati meme associati a movimenti separatisti?

Succedesse, sugli iPhone comparirebbe un database di riferimento diverso dall’attuale. Se ne accorgerebbe il mondo in un quarto d’ora (su tutti gli iPhone del mondo viene installato lo stesso iOS e non esistono versioni personalizzate); la reputazione di Apple colerebbe a picco. Un suicidio commerciale in piena regola. A parte il fatto che, se per assurdo succedesse, Apple potrebbe sostenere ad libitum che nessun iPhone contiene quel materiale, perché nulla viene comunicato direttamente alle autorità.

>Quanto tempo rimane prima che l’iPhone nella tua tasca inizi silenziosamente a compilare rapporti sul possesso di materiale politico “estremista” o sulla tua presenza in una manifestazione non autorizzata? O sulla presenza nel tuo iPhone di un video che contiene, o forse-contiene-forse-no, l’immagine sfocata di un passante che secondo un algoritmo somiglia a una “persona di interesse”?

Domande inquietanti. Che hanno nulla a che vedere con la situazione attuale, perché la loro traduzione in software è fantascienza. Il sistema annunciato da Apple non ha alcuna possibilità di eseguire alcuno di questi compiti. Se un domani lo fosse, sarebbe qualcosa di completamente diverso da quello che è stato annunciato. Vale a dire che questa preoccupazione è indipendente dall’annuncio di Apple.

Su queste premesse – che, con tutto il rispetto, la stima e la comprensione per quello che ha passato Snowden, sono giornalismo di seconda fascia e qualità scarsa – si avvia a nascere, conclude Snowden stesso,

>una i che tutto vede, sotto il cui sguardo ogni iPhone cercherà da solo qualunque cosa Apple voglia o le venga ordinato di volere.

Un mondo in cui, semplicemente, non si venderebbe un iPhone che è uno.

>O può darsi che mi confonda… o che io semplicemente **pensi differente**.

Ecco, io di differente dal dissenso che ho già letto sulla materia, non ho visto una riga.