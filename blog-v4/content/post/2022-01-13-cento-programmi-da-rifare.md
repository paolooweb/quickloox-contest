---
title: "Cento programmi da rifare"
date: 2022-01-13T01:26:33+01:00
draft: false
toc: false
comments: true
categories:
tags:
---
Quanti modi possibili per provare a migliorare le proprie capacità di programmare?

Uno è partecipare all’aggiornamento dei giochi contenuti nel mitico [Basic Computer Games](https://annarchive.com/files/Basic_Computer_Games_Microcomputer_Edition.pdf), libro uscito nel 1973 e contenente appunto una serie di giochi per i computer di allora, utili per imparare a programmare in linguaggio Basic.

Visti oggi sono programmini, tutti con input da tastiera alla riga di comando, indovina il numero, l’allunaggio del Lem, Mastermind, la dama e mille altre cosette che qualcuno reduce dagli anni ottanta avrà magari digitato da qualche rivista dentro un Commodore 64 o uno Spectrum.

Nell’ambito di una iniziativa che poterà a una nuova edizione del libro, su GitHub si trovano [cento giochi di quell’epoca e una sfida](https://github.com/coding-horror/basic-computer-games): riscriverli ciascuno in otto linguaggi moderni che sono Java, Python, C#, Vb.net, JavaScript, Ruby, Delphi/Object Pascal e Perl.

La cosa bella è che non devono diventare giochi di oggi, con interfaccia grafica e quant’altro, ma conservare il sapore di esempi semplici per chi inizia a programmare. Solo, non più in Basic.

Qualcuno storcerà il naso di fronte all'idea di cimentarsi gratuitamente in una attività a beneficio di un'operazione commerciale. Vero, tuttavia per ogni versione in linguaggio moderno che verrà consegnata, gli autori del progetto doneranno cinque dollari a [Girls Who Code](https://girlswhocode.com/). In pratica si tratta di imparare a programmare a scopo benefico.

Oppure, scaricare gratis giochini in diretta dalla preistoria e sfidarsi, che so, a [morra cinese in Perl](https://github.com/coding-horror/basic-computer-games/blob/main/74_Rock_Scissors_Paper/perl/rockscissors.pl).

*Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile.*
