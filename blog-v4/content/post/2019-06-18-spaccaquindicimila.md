---
title: "Spaccaquindicimila"
date: 2019-06-18
comments: true
tags: [Android, UTC, GPS, watch]
---
Non ho modo di verificare, tuttavia sembra vero: [gli apparecchi Android sballano di quindici secondi sull’ora esatta](https://issuetracker.google.com/issues/36911516).

Questa *feature* è nota dal 2009 e pare perdurare. Anche se il consenso sulla spiegazione è parziale e c’è chi sostiene altre motivazioni, il problema parrebbe essere il mancato rispetto dei secondi bisestili.

La rotazione della Terra rallenta impercettibilmente e di tanto in tanto le autorità che si occupano del tempo di riferimento universale (UTC) aggiungono all’anno un secondo in più. È successo finora quindici volte e appunto tanti sono i secondi bisestili.

Il sistema GPS tuttavia non ne tiene conto e il suo orologio interno è sballato di quindici secondi rispetto al tempo universale. Android legge l’ora dal GPS e la utilizza così com’è.

Altre piattaforme, per esempio iOS, mostrano invece il tempo universale. L’ora esatta. Da quando è nato watch, il suo produttore si è dotato di un orologio atomico di proprietà e ha lavorato per dare all’apparecchio una approssimazione massima di cinquanta millisecondi nel fornire l’ora.

L’approssimazione di Android è invece di quindicimila millisecondi. Solo trecento volte superiore.
