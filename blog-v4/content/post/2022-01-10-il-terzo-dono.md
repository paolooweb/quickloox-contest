---
title: "Il terzo dono"
date: 2022-01-10T01:16:02+01:00
draft: false
toc: false
comments: true
categories:
tags:
---
Quindici anni dopo l’[annuncio di iPhone](https://www.youtube.com/watch?v=MnrJzXM7a6o) ([il più grande annuncio di prodotto di tutti i tempi](https://daringfireball.net/linked/2022/01/09/15-years-ago-today), dice John Gruber), va notato l’ordine in cui Jobs elencò i tre famosi componenti: un iPod, un telefono, un *Internet communicator*.

Il primo era l’evoluzione che tutti si attendevano, di un apparecchio consolidato ed esistente. Il secondo, la novità che tutti volevano sentire. Il terzo, una formula strana per un qualcosa di cui nessuno aveva effettivamente una idea chiara.

Oggi le funzioni di un iPod sono scontate e vicine all’irrilevante. Il telefono perde sempre più terreno e tra dieci anni la funzione telefonica avrà la stessa attrattiva che oggi ha il fax.

Tra messaggi, chat, social media, video e audioconferenza, podcast, la parte di *Internet communication* è quella che ha effettivamente trasformato il mondo e oggi il senso preponderante di usare un iPhone o, se è per questo, una sua copia.

Il genio stava in quel terzo componente che, non fosse stato annunciato, avrebbe lasciato perfettamente uguale il clamore, gli applausi, l’effetto sorpresa. Ma sarebbe stato un nuovo prodotto qualunque.

Quell’*Internet communication* era l’intuizione di come sarebbe stato il mondo quindici anni dopo. O anche tra venti, perché nel 2027 sarà cambiato assai poco a questo riguardo, e tutto a favore di un maggiore utilizzo della rete.

Vedere quindici anni nel futuro è un dono per pochi.

<iframe width="560" height="315" src="https://www.youtube.com/embed/MnrJzXM7a6o" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

*Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile.*
