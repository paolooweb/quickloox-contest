---
title: "L’abitudine non fa il monaco"
date: 2021-07-04T00:40:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [Safari, 500ish, iOS, macOS, Monterey, Siegler, M.G. Siegler] 
---
Praticamente tutti hanno criticato con forza il nuovo assetto di interfaccia di Safari per iOS 15 e macOS Monterey, perché la barra in basso non va bene, condensare schede e campo URL in una sola riga svantaggia chi usa tante schede, perché varie funzioni sono più difficili da trovare o più lontane da raggiungere in termini di clic eccetera.

Va segnalata l’eccezione che conferma la regola: M.G. Siegler su *500ish* che si pronuncia [in difesa del nuovo Safari](https://500ish.com/in-defense-of-the-new-safari-4ffc69b7cf8b).

La sua analisi è piuttosto articolata e approfondita, alla pari con le altre di segno opposto. La sostanza è questa:

>All’inizio ho pensato che [spostare la barra strumenti in basso] fosse un errore. Ma l’ho fatto perché ero abituatissimo a come era prima. Dopo poche settimane di uso, mi piace di più averla in basso.

Siegler contesta ai contestatori di essere più contro il cambiamento in generale che contro questi cambiamenti in particolare.

Se ha ragione lui, per quanto le nuove versioni di Safari siano in beta e certe cose possano cambiare, cambierà poco o nulla, a significare che era una questione di abitudine e non di design.

Se hanno ragione gli altri, Safari farà molta marcia indietro (non tutta, non credo proprio, ma molta) e si riavvicinerà a come lo si è sempre usato.

Qualunque cosa succeda delle due, sarà interessante confrontare i giudizi degli esperti di interfaccia con quello che è successo davvero. E ricordarsene.

*Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile.*