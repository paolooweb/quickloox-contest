---
title: "La visione delle app più da vicino"
date: 2023-09-09T10:49:24+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [Vision Pro, MacDailyNews]
---
Apple ha fatto sapere agli sviluppatori che Vision Pro avrà il proprio App Store, qualcosa su cui tutti avremmo scommesso anche un centesimo; e che [conterrà anche le app per iPhone e iPad già compatibili](https://macdailynews.com/2023/09/08/nearly-every-iphone-and-ipad-app-will-run-on-apple-vision-pro-on-day-one/), cosa che invece non era del tutto scontata.

Una dotazione adeguata di software in partenza è cruciale per qualsiasi piattaforma ma qui andiamo oltre, poiché le app iOS funzionanti su Vision Pro sono *tante*. Il Vision Pro App Store potrebbe partire o diventare in breve tempo la piattaforma con più programmi disponibili, se non andarci vicino.

Peccato che i bei tempi in cui si giustificava la scelta di Windows con il numero di programmi a disposizione sia passato e i fautori dell’argomento abbiano tutti cambiato pianeta.

Sarà davvero curioso vedere che impatto avrà Vision Pro. Non è esattamente il tipo di prodotto che si vede tutti gli anni e probabilmente il software di maggior successo per lui deve ancora essere scritto, o è in *alpha* presso qualche laboratorio.