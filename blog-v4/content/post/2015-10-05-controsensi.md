---
title: "Controsensi"
date: 2015-10-05
comments: true
tags: [iPad, D&D]
---
Il pomeriggio nel meeting aziendale, circondato da alti dirigenti che prendono diligentemente gli appunti con le loro stilografiche su eleganti agendine di cuoio.<!--more-->

La sera durante la sessione di Dungeons & Dragons: lanci di dadi complicati risolti in mezzo secondo, scheda personaggio aggiornata in tempo reale durante il combattimento, fotografia della situazione lasciata a mezzo, da riprendere per la prossima sessione, con una fotografia vera. Eliminata la complessità meccanica del gioco, per godersi la sostanza vera del gioco, con lo stesso iPad (e la stessa batteria messa in moto la mattina presto). E lo stato degli strumenti, lo dico un po’ seccato, è tendenzialmente peggiorato rispetto allo scorso anno.

Non so in che direzione vada il mondo. Qualcuno va sicuramente nella direzione opposta.
