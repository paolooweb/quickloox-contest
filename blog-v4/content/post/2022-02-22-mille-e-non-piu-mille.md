---
title: "Mille e non più mille"
date: 2022-02-22T01:24:16+01:00
draft: false
toc: false
comments: true
categories: [Hardware, Web]
tags: [iPod Touch, MacRumors, Blender]
---
Sono capitato per imprevisto sulla home di MacRumors, per scoprire con angoscia che [l’ultimo iPod touch è uscito mille giorni fa senza essere più stato rinnovato](https://www.macrumors.com/2022/02/21/ipod-touch-now-1000-days-old/).

Naturalmente sconvolto dalla rivelazione, ho annullato ogni altro impegno per tuffarmi nell’approfondimento.

Così ho capito che iPod touch non viene aggiornato da mille giorni; l’unico fatto contenuto nell’articolo era già presente nel titolo. Non serviva altro.

Intorno a questa fondamentale notizia, l’idea che possa uscire un MacBook con schermo da venti pollici pieghevole, accompagnata da un disegnino 3D; un altro disegnino 3D dedicato a un casco per la realtà aumentata che arriverà certo, ma ora non esiste; in particolare, l’annuncio tragico che iPhone con schermo pieghevole è stato rinviato al 2025.

Ho passato la sera a rimodulare l’intero piano di acquisti Apple una volta acquisito questo fatto fondamentale. O no?

Sono fortunato; ho un feed Rss che a forza di rifiniture è diventato di buon livello e in pratica mi esclude dall’industria della fuffa Apple. Rientrarci per un attimo mi incoraggia a diventare più bravo con [Blender](https://www.blender.org), sai mai che tornasse domani utile una carriera di renderizzatore di apparecchi Apple ipotetici a contorno di notizie ipotetiche su siti ipotetici per pubblico, a questo punto, ipotetico.

Importante: oggi sono milleuno giorni che non esce un nuovo iPod touch. In pausa pranzo, guardare smarriti verso l’ignoto, come a chi sia venuto a mancare un punto fermo.
