---
title: "Quando conviene cambiare stato"
date: 2022-08-14T01:05:51+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [iOS 16, iOS, batteria, paolofasy, Fëarandil, macmomo]
---
A [domande](https://macintelligence.org/posts/2022-08-13-design-da-scaricare/), rispondo. Con ringraziamenti a **paolofasy**, [Fëarandil](https://www.twitch.tv/fearandil) e **macmomo** che hanno fornito considerazioni sensate di contesto, calando il tema dell’indicatore di carica della batteria nel complesso dell’intero sistema operativo e dell’intera piattaforma.

Ora torniamo a restringere l’osservazione per rispondere alle domande poste: perché la soluzione Apple è la migliore di quelle proposte e come potrebbe ulteriormente migliorare.

Presupposto importante: l’indicazione numerica dentro l’icona della batteria è *opzionale*. Si dà per scontato che chi la vuole sia in grado di leggerla e che preferisca un feedback digitale a quello pseudoanalogico dell’icona più o meno piena. Che sia appropriato o meno in generale mostrare un numero, non ci interessa più a questo punto. Lo vogliamo e vogliamo che funzioni al meglio. Corollario al presupposto: l’indicatore numerico è percentuale, va da zero a cento, perché questo è un dato estremamente familiare a chiunque. Anche se avesse più senso pratico mostrare, che so, numeri da uno a diciannove, li troveremmo disorientanti. Se c’è un indicatore numerico di progressione, è percentuale, per forza.

La batteria che si svuota, come tipo di indicatore, funziona allo stesso modo della barra di progressione che accompagna la copia o il download di un file: il numero dei pixel cambia con il procedere dell’operazione, o dell’energia consumata nel caso della batteria.

Se la barra di progressione è lunga centouno pixel (controllati uno per uno dal software), è più informativa di un eventuale indicatore numerico, perché mostra centouno stati diversi dell’operazione invece di cento. Se ha novantanove pixel, è meno informativa. Se ne ha esattamente cento, è informativa uguale. In tutti e tre i casi, uno dei due indicatori – quello grafico o quello digitale – è superfluo.

Ecco perché la soluzione Apple è la migliore di quelle proposte: l’icona della batteria è certamente lunga meno di cento pixel e un indicatore percentuale è certamente più informativo. Svuotare l’icona sullo sfondo di un numero percentuale è superfluo.

Perché si potrebbe fare meglio? Perché i cento stati diversi in cui ci viene rappresentata la carica della batteria hanno importanza diversa. Guardiamo un *88* a cuor leggero, abbiamo la vita davanti; se guardiamo un *12*, bisogna ricaricare abbastanza alla svelta. È per questo che la batteria attuale in iOS colora l’interno di rosso quando la carica scende sotto il venti percento; quella fascia di valori attiva una comunicazione che è più pressante.

Come fare a conservare questa comunicazione di secondo livello, ma tutt’altro che secondaria? Cambiamo colore al numero? Sembra attraente come idea, ma non è consigliabile per vari problemi, di leggibilità, di accessibilità, di tipografia.

Cambiare il colore del riempimento della batteria complica la visualizzazione del numero ed è un’altra cattiva idea.

Invece si potrebbe tenere la batteria vuota e colorare il contorno. Se come adesso, di bianco tra cento percento e venti percento, o di rosso tra venti e zero.

Se volessimo fare di più, a rischio di perdere sobrietà però, potremmo avere tipo cinque colorazioni diverse, da verde brillante (cento-ottanta) a rosso vivo (venti-zero) passando per giallo-verde, giallo, arancione.

Il *nerd* che si agita in me, per fortuna frequentemente inascoltato, vorrebbe *cento* cambiamenti di colore: un gradiente diverso tra verde e rosso per ciascun numero possibile. Sarei un cattivo designer però, perché il numero da zero a cento risponde a una logica di familiarità ed è questa a giustificarlo, altrimenti non c’è alcuna ragione pratica di distinguere tra cento stati stati diversi di carica della batteria. Venti stati sarebbero più che sufficienti; quelli essenziali potrebbero essere cinque, più qualche extra nella fascia da zero a venti dove ogni variazione diventa di maggiore interesse.

Non abbiamo invece alcuna familiarità con icone di batterie dal contorno che cambia colore in continuazione, né ragioni valide per distrarci a guardare l’icona ogni mezzo minuto per vedere che colore ha preso. Abbiamo già un indicatore percentuale; il contorno dovrebbe complementare questa informazione con una indicazione di massima (carica, ben carica, carica a metà, poco carica, quasi scarica) e cambiare stato di quest’ultima solo quando fosse essenziale.