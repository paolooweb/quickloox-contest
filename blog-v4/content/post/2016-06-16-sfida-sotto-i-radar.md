---
title: "Sfida sotto i radar"
date: 2016-06-16
comments: true
tags: [Kickstarter, Wwdc, Swift, Playgrounds, watchOS, tvOS, iOS, macOS, Sierra, Apfs]
---
La cronaca sommaria delle cose che contano all’indomani dell’[apertura di Wwdc 2016](http://www.apple.com/apple-events/june-2016/) l’ho messa in un’[articolessa su Apogeonline](http://www.apogeonline.com/webzine/2016/06/15/fare-il-differenziale) che ha finito per essere lunga il triplo dello spazio tipicamente a disposizione. Piccola o grande, quest’anno la carne al fuoco è stata davvero tanta, a memoria molta più dell’anno scorso.

E l’anno scorso si arrivava più o meno poco dopo il debutto di watch e della nuova tv. C’erano, tuttavia di fatto stava cominciando tutto. Quest’anno, nella presentazione e nello spazio dedicato, hanno ricevuto dignità di piattaforma seria. Il che porta il totale delle piattaforme serie a quattro, alla faccia di chi ritiene che non ci sia innovazione e si riposi sugli allori.

Le novità sono tutte evolutive e il punto non è sfoderare la *One More Thing* o non averla: è che il tempo delle innovazioni sconvolgenti, quando chiunque si porta addosso due o tre computer e può mettersi in casa una stampante 3D, si è ridotto a una fessura. Contano di più gli ecosistemi e le interfacce: a questo proposito sono ottimi segni la riprogettazione di alcuni percorsi su watchOS e iOS. Per non parlare delle segnalazioni di watchOS su misura per le persone in carrozzina: una parte del progresso è affondare in profondità nella tecnologia per tirare fuori qualcosa di inedito, ma l’altra consiste nel portare la tecnologia veramente a tutti e questa attenzione per una minoranza speciale dice bene.

Siri su Mac è un indizio importante di come stanno andando le cose per il futuro: finestre e icone interesseranno sempre meno, nel momento in cui basta dare un ordine a voce. Le nostalgie per il Finder di una volta si fanno sempre più anacronistiche. Sono anche curioso di vedere come funzioneranno in concreto le opzioni di mettere sul cloud i file vecchi allo scopo di liberare spazio; si tratta di cose marginali, eppure chi altri le offre?

In altri tempi la notizia di [un nuovo filesystem](https://developer.apple.com/library/prerelease/content/documentation/FileManagement/Conceptual/APFS_Guide/GeneralCharacteristics/GeneralCharacteristics.html#//apple_ref/doc/uid/TP40016999-CH2-SW1) avrebbe riempito le prime pagine dei periodici specializzati. Non si muoveva niente in materia da un quarto di secolo. Ed è un compito impegnativo a dire poco, tant’è vero che la fase di collaudo durerà ben oltre l’uscita di macOS Sierra.

È interessante anche l’idea di inserire rumore di fondo nelle transazioni con i sistemi allo scopo di permettere ad Apple, o ad altri, di cogliere la visione di insieme mentre il singolo resta nel più completo anonimato e non è più tracciabile. Una bella sfida per conciliare diritto alla riservatezza con il trattamento dei dati che è necessario se vogliamo che il sistema sappia dirci a che ora uscire di casa in tempo per l’appuntamento, o autorizzare l’acquisto su iTunes Store da parte di un altro membro della famiglia. Anche qui le competenze e la difficoltà del compito sono notevoli e quelli che *non è più come una volta* dovrebbero almeno prima aggiornarsi.

Sono semplicemente entusiasta di [Swift Playgrounds](http://www.apple.com/swift/playgrounds/) e non vedo l’ora che esca. Non è ancora arrivato il momento di Xcode su iPad e tuttavia, per le scuole e le famiglie, è davvero una bella occasione.

Per chi capisce la tematica e l’urgenza. Ed è qui che colgo il filo conduttore del *keynote*: quest’anno Apple ha mostrato agli sviluppatori – e al mondo – una montagna di cose che aumentano di molto il valore di un Mac, un iPad, un iPhone, un watch, una tv.

Tutte cose che, guardando superficialmente, non si vedono. Una sfida che corre sotto i radar dei parolai e dei giudizi sommari un tanto al chilo. Anche difficilissima da vincere. Sarà molto interessante vedere come va.

*[Non voglio mollare l’osso fino a fine luglio e voglio dire pure che a Wwdc le cose interessanti da scrivere in un nuovo più-che-un-libro dedicato al mondo Apple sono fioccate abbondanti. E [sarebbe d’uopo](https://macintelligence.org/posts/2016-06-15-un-kickstarter-per-cambiare-il-libro/) avere un più-che-un-libro a spiegarle. Però [va finanziato e fatto conoscere](https://www.kickstarter.com/projects/295108589/cuore-di-mela-i-segreti-di-apple-iphone-e-mac).]*