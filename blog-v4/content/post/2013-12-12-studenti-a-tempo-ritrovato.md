---
title: "Studenti a tempo ritrovato"
date: 2013-12-12
comments: true
tags: [iPhone, iTunesU, Coursera, Jobs]
---
Il primo pensiero va a [iTunes U](https://itunes.apple.com/it/app/itunes-u/id490217893?l=en&mt=8) che, non avesse preceduto tutti nella distribuzione gratuita di insegnamento *online*, ci sarebbe andata vicina e rappresenta comunque la testimonianza più autorevole e significativa in materia: una delle eredità più preziose e meno appariscenti dell’era di Steve Jobs.<!--more-->

Detto questo, la novità di oggi è [Coursera](https://www.coursera.org/) che ha fatto debuttare la propria [app per iPhone](https://itunes.apple.com/it/app/coursera/id736535961?l=en&mt=8).

La marea sta salendo. Non c’è mai stata così tanta istruzione disponibile *online*, di così alto livello, così gratuita, così largamente accessibile. Qualcosa che potrebbe lasciare il segno sul futuro dell’umanità intera e di miliardi di persone, ciascun singolo individuo.

*The Unofficial Apple Weblog* [scrive](http://www.tuaw.com/2013/12/10/coursera-lets-you-learn-on-the-go-with-new-iphone-app/) con noncuranza di materia potenzialmente esplosiva:

>Se fossi un pendolare, accenderei Coursera per imparare [neuroscienze mediche](https://www.coursera.org/course/medicalneuro) invece che guardare il vuoto oltre il finestrino.

Imparare nel tempo di ripiego e quel tempo, ritrovarlo: non sta nel nostro Dna. Eppure rischia di portarci vantaggi pazzeschi. O perdere terreno, se cediamo all’abitudine e al quieto vivere mentre gli altri si attivano. Chi può, studi. Mai come oggi si poté dire *basta un cellulare*.