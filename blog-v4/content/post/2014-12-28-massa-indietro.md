---
title: "Massa indietro"
date: 2014-12-30
comments: true
tags: [Ibm, Natale, iOS, Android, Amazon, iPad, featurephone, smartphone]
---
[Ibm ha analizzato](http://www-01.ibm.com/software/marketing-solutions/benchmark-hub/alert.html) l’andamento delle vendite via Internet negli Stati Uniti nel giorno di Natale.<!--more-->

Rispetto al Natale 2013, le vendite *online* sono aumentate dell’8,3 percento; il traffico Internet *mobile* (computer da tasca, tavolette, orologi, aggeggi vari) è cresciuto del 18,6 percento e ha rappresentato il 57,1 percento del traffico totale; di tutte le vendite via Internet, quelle effettuate via *mobile* sono il 34,8 percento del totale, in crescita del 20,4 percento sull’anno scorso.

Navigare e ordinare da qualcosa che non è il computer in salotto è più che mai in voga. Ne so qualcosa; con la bambina piccola, gran parte dei regali di Natale è stata ordinata su Amazon e spesso, appunto, dal divano, con un iPad in una mano e un fagottino nell’altra.

Il 39,1 percento del traffico totale su Internet è arrivato da iOS, contro il 17,7 percento di Android.

Il 27 percento delle vendite *online* è stato generato via iOS, mentre per quanto riguarda Android si tratta del 7,6 percento.

Tenuto conto che Android è numericamente soverchiante rispetto a iOS, ancora una volta, e passato ancora un anno rispetto a un 2013 dove la situazione era la stessa, mi chiedo a che serva avere Android per il settanta percento o giù di lì dei suoi possessori. Che non vanno su Internet, non comprano online, in pratica la parte *smart* non la usano. Una massa di gente rimasta indietro.

Mi si dirà che sono persone risparmiose. Benissimo: un *featurephone*, un cellulare di quelli di una volta che telefona e manda gli Sms e va bene così, costa assai meno e consuma meno ancora. Ed è anche in grado di scaricare il giochino tormentone del momento.