---
title: "Molta rete e poche soluzioni"
date: 2017-02-02
comments: true
tags: [Network, Solutions]
---
Non mi è mai piaciuta l’interfaccia dei servizi [Network Solutions](https://www.networksolutions.com), sempre pronti a proporti un *optional* non richiesto in qualsiasi situazione, non importa quanto fuori luogo. Però uno lo sa, sta attento, si regola.

Nei giorni scorsi è accaduto qualcosa di più grave: ho acquistato un dominio per cinque anni, al costo di centotré euro e spiccioli. E sulla mia carta di credito sono stati addebitati centodieci euro e spiccioli.

Uno dirà, hanno sbagliato a compilare l’ordine, succede. Invece l’ordine riporta centotré euro.

Propongono *optional* non richiesti in ogni pagina, ti sarà scappato un clic, dice. Può darsi; ma sull’ordine non ce n’è traccia. Se acquisti una cosa non richiesta per errore, comunque dovrebbe figurare nell’ordine.

Il supporto clienti di Network Solutions lavora soprattutto per telefono, ma è ancora possibile scrivere tramite il sito. Mi ha risposto un operatore gentilissimo: devo telefonare, per motivi di *privacy*.

Non mi metto a telefonare negli Stati Uniti per una faccenda di sette euro spalmati su cinque anni, un euro e quaranta l’anno, dodici centesimi al mese, tre centesimi a settimana.

Né annullerò l’ordine, almeno stavolta: il fastidio e la perdita di tempo che comporterebbe rifare tutto sono enormemente superiori ai sette euro per cinque anni.

Ho lasciato una risposta poco amichevole al supporto, chiedendo nuovamente spiegazioni (che possono arrivare senza problemi di *privacy*) e soprattutto una risoluzione rapida del problema. Vediamo che cosa succede.

Scoccia perché si tratta di pratiche che neanche più i siti porno, al massimo i venditori di suonerie che turlupinano gli anziani con gli abbonamenti settimanali.

Niente da dire sulla *network*, ma le *solutions* lasciano per ora a desiderare.

Tutto questo solo per dire: attenzione. Non pretendo che si faccia girare la voce come se fosse chissà quale appello. Tuttavia, meglio considerare altri fornitori se non è un problema.