---
title: "Fenomenologia di iPhone 6"
date: 2014-09-28
comments: true
tags: [iPhone, iPhone6]
---
Compare.com ha pubblicato una [infografica riguardante la produzione di iPhone 6](http://comparecamp.com/media/uploads/2014/09/how-iphone-is-made.jpg) che, per quanto impegnativa da scaricare, vale la pena.<!--more-->

Si scopre per curiosità che tra i tanti fornitori di Apple vi sono anche tre aziende italiane e soprattutto l’importanza dell’indotto pazzesco generato dalla produzione di iPhone, primariamente in Asia e negli Stati Uniti.

Si capisce che la vera carta vincente della Cina non sono i bassi costi ma la quasi inquietante elasticità del meccanismo: produrre negli Stati Uniti costerebbe appena quattro dollari in più a iPhone, solo che ci vorrebbero nove mesi per fare ciò che in Asia richiede due settimane.

Si parla di posti di lavoro a milioni, miliardi in prodotto interno lordo, destini di macroaree geografiche, quando non nazioni, che cambiano radicalmente grazie a fabbriche e forniture.

Poi, di dieci milioni di unità vendute, se ne sono piegate nove, meno di una su un milione. Nel 2007, con un iPhone 2G costato 450 dollari, stavo attento a non sedermici sopra.

<div style="clear:both"><a href="http://comparecamp.com/how-where-iphone-is-made-secrets-of-apples-manufacturing-process/"><img src="http://comparecamp.com/media/uploads/2014/09/how-iphone-is-made.jpg" alt="compare how much of iphone is made in the usa" width="600" border="0" /></a></div><div></div>Cortesia di <a href="http://comparecamp.com">comparecamp.com</a>, autore <a href="http://comparecamp.com">Alex Hillsberg</a>.