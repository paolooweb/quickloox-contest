---
title: "La sfida finale per l’autoapprendimento"
date: 2023-09-19T16:18:11+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [Boiling Steam, WordPress]
---
Che bello il racconto di [Boiling Steam](https://boilingsteam.com/a-tale-of-outage-and-revival/) su come il blog è tornato online dopo problemi non dettagliate di infrastruttura.

L’autore ha tempo e capacità e ha deciso di uscire da WordPress a favore di un sistema statico di generazione del sito, che dopo avere considerato le possibilità esistenti ha deciso di rifarsi da solo, [in linguaggio R](https://www.r-project.org).

Per creare un blog su un sistema statico non serve per forza un linguaggio vertiginosamente veloce e ha trovato appoggio nelle librerie che gli servivano per trattare con le immagini e tutte le altre cose.

Dentro il racconto, che mostra indubbiamente le cose più facili di quello che sembrano, trovano spazio una disamina efficace di pregi e difetti dei generatori statici e di WordPress, nonché una descrizione sommaria quanto interessante della nuova infrastruttura, basata su [Docker](https://www.docker.com) come contenitore di tutto il software a monte del sito.

Farsi un blog è ancora possibile e forse è un vantaggio; il Facebook della situazione permette di scrivere qualsiasi cosa istantaneamente, ma avere il proprio sistema è un’altra cosa. Quella cosa di grandi poteri e grandi responsabilità.

Probabilmente costruirsi un blog da soli, anche semplice, è la frontiera definitiva dell’autoapprendimento. Le sfide e le difficoltà chiariranno e insegneranno più quanto possa fare ogni altra cosa.