---
title: "Umani dal passato"
date: 2019-05-04
comments: true
tags: [Espinosa, Tesler, Mori, Morrick]
---
L’amico **Riccardo** ne ha fatta una grandiosa: ha cominciato a guardare una [conferenza sulle origini dell’interfaccia umana Apple](https://www.youtube.com/watch?v=OW-atKrg0T4), pubblicata dal Computer History Museum e tenuta nel 1997 da Larry Tesler e Chris Espinosa.

A suon di prendere appunti, [l’ha trascritta pressoché per intero](http://morrick.me/archives/8432#more-8432).

Guardarla è suggestivo, ma leggerla consente di capire e senza avere assimilato quello che si pensava negli Ottanta è inconcepibile credere di poter dire qualcosa di sensato alla fine dei Dieci.

Da guardare, da leggere, da ripetere entrambe le operazioni. Di materiale così ne esiste veramente poco anche dopo lustri di ricerche e retrospettive.

<iframe width="560" height="315" src="https://www.youtube.com/embed/OW-atKrg0T4" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>