---
title: "Dura minga"
date: 2013-10-26
comments: true
tags: [Windows, Microsoft, Surface]
---
In milanese, *non dura*.

Si applica ai [commenti di Frank Shaw di Microsoft](http://blogs.technet.com/b/microsoft_blog/archive/2013/10/23/apples-and-oranges.aspx) in merito alla decisione di Apple di [regalare le proprie app su iOS](https://macintelligence.org/posts/2013-10-24-il-prezzo-da-pagare/). La sostanza: poca roba, il mondo usa Office, le nostre tavolette sono le migliori.<!--more-->

Chiaro che l’oste canti le qualità del proprio vino. Ma anche un’occhiata ai [test di batteria di Anandtech](http://anandtech.com/show/7440/microsoft-surface-pro-2-review/4) non guasterebbe. In classifica ci sono i modelli Windows Phone appena usciti, confrontati con iPad della scorsa generazione. Quello che dalla prossima settimana non si vende più, sostituito da un modello con la stessa autonomia e veloce il doppio.

Per capire se iPad di quarta generazione abbia tenuto botta o meno, sono utili le [rilevazioni di traffico Internet di Chitika](http://chitika.com/insights/2013/Q3-tablet-update) nella seconda metà di settembre. iPad 81 percento, Surface uno percento.

*Dura minga* anche l’attrattiva di chi arriva in ritardo e per giunta male.