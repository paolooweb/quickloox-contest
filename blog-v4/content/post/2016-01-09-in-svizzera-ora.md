---
title: "In Svizzera, ora"
date: 2016-01-09
comments: true
tags: [watch]
---
[Si diceva delle esportazioni in calo](https://macintelligence.org/posts/2015-11-20-a-che-serve-un-apple-watch/) degli orologi svizzeri ed ecco arrivare uno splendido esemplare [dannatamente simile a un watch](https://www.hodinkee.com/articles/the-h-moser-and-cie-swiss-alp-watch-a-not-so-subtle-jab-at-the-apple-watch/).<!--more-->

A concorrenza zero. È “solo” un orologio; ne verranno prodotte non più di cinquanta unità; costa sui venticinquemila euro (molto più della stragrande maggioranza degli watch venduti).

Eppure la linea è esattamente quella. Uno sberleffo? Un tentativo (riuscitissimo) di cavalcare l’onda e guadagnare spazio e attenzione sui media? Uno studio attento con lo scopo di scoprire se la forma e le linee siano più appetite di altre dal pubblico?

Quale che sia la risposta, il segnale chiaro è che watch non viene ignorato nella patria degli orologi di precisione. Il che forse ha una connessione con il calo delle esportazioni di cui sopra.
