---
title: "Uniti dal silicio"
date: 2021-07-02T00:29:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [macOS, iPadOS, iOS, watchOS, iPhone, iPad, Mac, watch, John Voorhees, MacStories, Shortcuts, Comandi rapidi, Live Text, Focus, Quick Notes, Monterey, Catalina, Big Sur] 
---
*La pubblicazione del blog riprenderà, con gli arretrati del caso, martedì 6 luglio.*

È stato confortante leggere John Voorhees su *MacStories* che dà conto delle [prime notizie interessanti sulla beta pubblica di Monterey](https://www.macstories.net/stories/macos-monterey-first-impressions-the-start-of-a-new-era/). Meno rispetto alla mia [tentazione di provare le beta stesse](https://macintelligence.org/posts/Lora-alfa-per-le-beta.html) e più per il quadro di insieme.

Le prime impressioni di Voorhees si intitolano infatti *l’inizio di una nuova era* e fanno cenno a una sorta di trilogia dei sistemi, iniziata con Catalina e proseguita con Big Sur, che segnano la transizione di macOS verso una situazione nuova, in cui la maturazione di tutte le linee di prodotto porta verso la comparsa di migliorie significative in modo trasversale tra Mac, iPad e iPhone; al tempo stesso, è arrivato il momento in cui le migliorie stesse possono impattare in modo interessante anche sulle altre piattaforme, come succede per esempio per SharePlay o per i Comandi rapidi che, su Mac, riconoscono anche AppleScript e Automator, inverando le previsioni più ottimistiche di chi sperava in novità positiva sul fronte dell’automazione.

In più i problemi della beta pubblica sembrano marginali e comunque minori in confronto a quanto si era visto con Big Sur e Catalina, il che depone ulteriormente a favore della voglia di provare.

Certo che però, leggi di come i sistemi Apple si stanno preparando alla nuova epoca di Apple Silicon, di come collaborano insieme, di come le funzioni crescono e si compenetrano da una piattaforma all’altra… se vuoi provare una beta, non ha tanto senso, a meno che le provi tutte insieme. Il che facilita una certa ansia sottintesa.

*Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile.*