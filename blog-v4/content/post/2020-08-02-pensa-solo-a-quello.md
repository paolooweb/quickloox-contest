---
title: "Pensa solo a quello"
date: 2020-08-02
comments: true
tags: [Apple, iPhone, iPad, Mac, watch]
---
Agosto è tradizionalmente _slow news cycle_: se vivi di notizie, devi faticare più del solito. Per questo mi hanno fatto divertire quelli di _Macworld_, con il surreale titolo [Apple si scrolla di dosso il Covid-19 con un trimestre record alimentato dalla vendita di praticamente qualunque cosa](https://www.macworld.com/article/3569368/apple-shrugs-off-covid-19-with-record-quarter-boosted-by-sales-of-basically-everything.html). Però li capisco, da qualche parte un po’ di sensazione bisogna cercare di crearla.

Capisco meno il _Corriere_, che a febbraio titolava [Coronavirus, perché Apple e le altre Big Tech temono il virus cinese](https://www.corriere.it/tecnologia/20_febbraio_03/coronavirus-perche-apple-altre-big-tech-temono-virus-cinese-df0571d0-466a-11ea-a350-c716e6003881.shtml). Surreale anche lui, purtroppo da tutt’altra visuale. Considerato che il virus sta creando problemi a tutti tranne che alle Big Tech, non è un capolavoro di analisi.

Va detto anche come le Big Tech se la passino bene perché si sono organizzate, come scrivevo. Devo riprendere l’argomento in un prossimo post.

Piuttosto, ripensavo anche a quando si scriveva che Apple _pensa solo agli iPhone_. In questo trimestre _tutto_ è cresciuto più di iPhone in termini di fatturato e chissà a che cosa dovrà pensare Tim Cook per stare dietro a chi ha sempre capito tutto.