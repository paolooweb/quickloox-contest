---
title: "Pessimismo e fastidio"
date: 2023-03-24T02:10:58+01:00
draft: false
toc: false
comments: true
categories: [Software, Internet]
tags: [Attivissimo, Paolo Attivissimo, ChatGPT, Midjourney, Stable Diffusion, Pixelmator Pro]
---
Che fastidio la retorica vuota di Paolo Attivissimo quando dal suo blog [proclama](https://attivissimo.blogspot.com/2023/03/podcast-rsi-midjourney-5-le-foto.html) che

>Dal 15 marzo scorso, insomma, non possiamo più fidarci di qualunque foto trovata online, perché esiste un modo facile e a buon mercato per generare migliaia di fotografie false ma estremamente credibili di qualunque persona o evento, reale o di fantasia.

Chissà dove è stato in questi anni, dove Photoshop ha imperato. Come? Né facile né a buon mercato? Beh, [Pixelmator Pro](https://www.pixelmator.com/pro/) costa come pizza e birretta artigianale a Milano e cominciare a usarlo è immediato.

Che pessimismo viene nel constatare l’esistenza di centrali della disinformazione di massa, oramai documentate e localizzate, che nessuno pensa seriamente di smantellare. Di certo occorre continuare a ignorare le foto farlocche; disgraziatamente, con certezza ancora superiore, legioni di asini continueranno ad abbeverarsi alla fonte di truffatori, dittatori e fanatici.

Ogni tanto qualcuno invoca i tempi belli della Internet di una volta, ma non eravamo meglio, solo molto pochi. La verità è una sola: *qualsiasi strumento digitale troverà gente che ne fa abuso per scopi gretti e deprecabili*.

Vale per tutti gli strumenti; su Internet però è tutto più veloce e il potenziale è più alto. Il genio non tornerà nella bottiglia (neanche Attivissimo). Bisogna imparare a riconoscere il nemico, dubitare, colpirlo dove è debole. Nessuna pietà, nemmeno davanti al dolore o alla povertà. Un povero che pensi di riscattarsi o di trovare la rivincita attraverso le *fake news* o il negazionismo di moda, per quanto povero, fa comunque danni ed è comunque responsabile per la sua parte.