---
title: "Da dove veniamo"
date: 2013-10-29
comments: true
tags: [Allaboutapple]
---
Fin troppo interessante chiederci verso dove andiamo. La verità è che, senza sapere da dove veniamo, le risposte risultano inconsequenziali. E inutili.<!--more-->

Da dove veniamo ce lo racconta ogni giorno [All About Apple](http://www.allaboutapple.com) con la fatica quotidiana, insospettabile e pazzesca di allestire il museo Apple più fornito del mondo.

Gli amici – lo dico con orgoglio – di All About Apple hanno messo a punto un’edizione più bella che pria del loro sito, che è un pretesto in più per seguirli, restare conquistati e magari programmarsi una gita a Savona a scoprire una realtà incredibile quanto unica.

Guarda caso, nel dedicare così tanto lavoro al da dove vengono, hanno le idee molto chiare sul dove stanno andando. Tanto di cappello e un grande saluto.