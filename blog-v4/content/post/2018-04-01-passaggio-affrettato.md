---
title: "Passaggio affrettato"
date: 2018-04-01
comments: true
tags: [Pasqua, Nive]
---
Se fosse una festa normale avrei un *post* elaborato di auguri e pure un pesce di aprile.

Invece ho una figlia che entra nel quarto mese e, direttamente e indirettamente, contribuisce a un combinato disposto di potenza formidabile che succhia qualsiasi momento di qualità.

Ho grandi auguri da porgere e un sacco di cose da scrivere. Inizia anche a liberarsi ora del tempo e per chi ha pazienza arriverà tutto.

Ora posso solo auspicare un po’ di fretta che sia una bellissima Pasqua: un passaggio verso un nuovo capitolo di vita, ancora più pieno ed entusiasmante. Siate affamati e folli sì, ma più di questo, gioiosi.
