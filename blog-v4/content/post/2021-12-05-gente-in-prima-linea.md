---
title: "Gente in prima linea"
date: 2021-12-05T00:35:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [LibreItalia, Steve Jobs, Think Different, George Romero] 
---
La delizia dell’informatica è che esiste sempre la possibilità di osare, cambiare, provare, sperimentare. La sua croce è che il novanta percento delle persone vive sperando di trovare un programma e usare sempre quello, sempre uguale, per tutta la vita.

Se poi questo programma arriva da qualche multinazionale, è la fine; la stagnazione, la morte cerebrale dei polpastrelli, l’incapacità di concepire qualsiasi cosa di diverso, quindi qualsiasi cosa di migliore. Il progresso non sempre migliora, ma con il passare degli anni la probabilità che lo faccia tende all’aumento.

[LibreItalia](https://www.libreitalia.org) è una associazione lanciata da visionari e in mano a folli ancora affamati, per [citare Steve Jobs](https://news.stanford.edu/2005/06/14/jobs-061505/); che non hanno alcun rispetto per lo status quo, per citare [lo spot Think Different](https://www.youtube.com/watch?v=mnFQZwGWnUM).

Gente che ha più idee che fondi, più volontà che mezzi e si batte spesso in prima linea nell’arena più proibitiva che ci sia – la pubblica amministrazione, di cui in particolare la scuola – oltre che ovunque, per conservare il diritto a sperimentare cose nuove, a pensare diversamente, a non adagiarsi nella *comfort zone*.

Senza guadagnarci niente, anzi spendendo e spendendosi per amore e passione per il software libero, che ha il potere magico di tenerci liberi di pensare di più e meglio, di immaginare cose fattibili anche se non rientrano nella morsa mentale dei programmi-che-fanno-così-perché-si-è-sempre-fatto-così-e-tutti-fanno-così, attitudine che a me ricorda più che altro i film di [George Romero](https://www.rottentomatoes.com/celebrity/771474732).

LibreItalia ha tenuto ieri la propria convention annuale e almeno una scorsa ai [tweet](https://twitter.com/search?q=%23libreitaliaconf2021&src=typeahead_click) bisognerebbe darla, tutti.

Con due bambine in famiglia non riesco assolutamente a mettere in LibreItalia tutto l’impegno che dovrei, ma tengo stretta la mia tessera di socio sostenitore e a un certo punto le bambine cresceranno.

Davvero, LibreItalia è una associazione che tutela una quantità di libertà inimmaginabile rispetto al pensiero comune, della gente che nemmeno pensa ci possa essere una alternativa al foglio di calcolo usato per il piano ferie, la lista della spesa, la lettera al condominio e la rubrica telefonica.

Pensiamoci a gennaio e teniamo da parte una somma per iscriverci che a malapena a Milano ci si fa colazione.

<iframe width="560" height="315" src="https://www.youtube.com/embed/mnFQZwGWnUM" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

_Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile._