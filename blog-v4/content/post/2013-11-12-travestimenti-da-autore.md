---
title: "Travestimenti da autore"
date: 2013-11-12
comments: true
tags: [LaTeX]
---
Semplice quanto efficace, **Domenico**:

>Mi permetto di segnalarti [questa pagina](http://www.latextemplates.com/template/wiley-book-style). Mi sembra una buona iniziativa.<!--more-->

Iniziativa eccellente e non solo per gli autori Wiley.

Riassunto delle puntate precedenti: Wiley è un editore tecnico americano che conta. E ha messo a punto *template* (maschere di inserimento) per chi vuole consegnare libri in formato LaTeX, di potenza assoluta e difficoltà ragionevole.

La difficoltà è ancora minore, adesso. Basterà seguire le indicazioni della maschera per consegnare libri che rispettano le specifiche dell’editore e sfruttano tutta la potenza del linguaggio.

Più in generale, sostengo da tempo che gli autori del XXI secolo devono avvicinarsi al lato tecnico della produzione dei libri, se vogliono rimanere autori e se intendono esprimersi compiutamente. Un libro è più della somma dei periodi che lo compongono.

Se si muove un Wiley, c’è qualcosa di vero. Lo ricordino quanti pensano che basti avere un’idea e accendere Pages. Studiare LaTeX, piuttosto. O almeno cominciare a scrivere Html.