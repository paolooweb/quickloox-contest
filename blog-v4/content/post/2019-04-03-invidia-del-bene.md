---
title: "L’invidia del bene"
date: 2019-04-03
comments: true
tags: [Microsoft, Pokémon, GO, Mimmo, Byte, Corriere]
---
Avendo letto due persone normalmente intelligenti e acculturate constatare l’apparente assenza di pesci d’aprile per quest’anno  e soprattutto attribuirlo a un effetto alone provocato dalla [ritirata di Microsoft](https://macintelligence.org/posts/2019-04-01-serpenti-di-aprile/), devo tornare in argomento.

Belli o brutti, [ce ne sono stati](https://techcrunch.com/2019/04/01/the-best-and-worst-april-fools-jokes-from-the-tech-world-2019/) e questa è ovviamente una parzialissima rassegna. Il *Corriere* ne elenca [quarantatré](https://www.corriere.it/tecnologia/cards/i-migliori-pesci-d-aprile-2019-audiolibri-pesci-cuffia-due/i-migliori-pesci-d-aprile-2019_principale.shtml), per quanto attraverso una *gallery* di rara scomodità.

Perché il divieto interno di Microsoft? Perché, per esempio, nel 2014 Google [sfoderò un pesce che divenne un business miliardario](https://techcrunch.com/2019/04/01/pokemon-go-and-the-april-fools-joke-that-made-billions/).

Succede una volta su un milione, ma succede e succede quando le cose vengono fatte bene. Una nozione, questa, sostanzialmente estranea a certa cultura aziendale dove la metrica è unicamente quella commerciale. Piuttosto che invidiare quelli più bravi, dunque, meglio stare fermi.

(Onore e gloria a **mimmo** che ha ritrovato il favoloso [pesce d’aprile di Byte](https://archive.org/details/byte-magazine-1986-04) del 1986!)
