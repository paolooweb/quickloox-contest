---
title: "L'arte di non averne"
date: 2013-12-11
comments: true
tags: [iPad, Procreate, Lambert, Rimes, Freeman, Waterdeep]
---
Mi arrabbio quando vedo la gente applicare regole di buonsenso in tutto il resto della vita e, giunta all’informatica, capovolgerle senza motivo.<!--more-->

Ogni regola ha però la propria eccezione. La grandezza di un iPad, per fare solo un esempio, è di non avere uno scopo. E per questo poterne avere infiniti.

Non si spiega altrimenti la possibilità per il britannico [Kyle Lambert](http://kylelambert.co.uk) di creare il suo incredibile [ritratto di Morgan Freeman](https://www.youtube.com/watch?v=uEdRLlqdgA4).

<iframe width="420" height="315" src="//www.youtube.com/embed/uEdRLlqdgA4" frameborder="0" allowfullscreen></iframe>

La stessa possibilità che ha LeAnn Rimes di creare un [videoclip musicale](https://www.youtube.com/watch?v=eVHjvNHRITw) facendo scattare a iPhone ottomila immagini animate a passo uno.

<iframe width="560" height="315" src="//www.youtube.com/embed/eVHjvNHRITw" frameborder="0" allowfullscreen></iframe>

La stessa possibilità che abbiamo noi di usare iPad anche solo per giocare a [Lords of Waterdeep](https://itunes.apple.com/it/app/lords-of-waterdeep/id648019675?l=en&mt=8), oppure fare musica, o scrivere un libro o immortalare il gatto di casa mentre rovescia l’albero di Natale.

È sempre lo stesso identico iPad o iPhone. E questa nozione dà le vertigini.