---
title: "Altre lezioni americane"
date: 2020-12-30
comments: true
tags: [Ditaa, Emacs, Cook, imagick, LaTeX, Markdown, Calvino]
---
C’è bisogno di scrivere una edizione tecnologica delle [Lezioni americane](https://www.oscarmondadori.it/libri/lezioni-americane-italo-calvino/#close-modal) di Calvino, che portino nel computing personale gli stessi valori ritenuti fondamentali dallo scrittore per la letteratura.

Le nuove lezioni porterebbero un lettore ad applicarsi sulla propria attività rapidamente e con profitto attraverso discipline atipiche e fuori dallo schema corrente: esempi che vengono in mente sono [Markdown](https://daringfireball.net/projects/markdown/syntax), [LaTeX](https://www.latex-project.org), [imagick](https://imagemagick.org/index.php), [emacs-org](https://orgmode.org), ma ce ne sono a bizzeffe.

Uno che non conoscevo e voglio assolutamente portarmi nel nuovo anno, come Calvino avrebbe portato i suoi valori letterari nel nuovo millennio, è [ditaa](https://github.com/stathissideris/ditaa), che traduce i diagrammi disegnati in Ascii (!) in rappresentazioni graficamente ineccepibili.

Disegnare diagrammi in Ascii sembra una perdita di tempo ma c’è da rifletterci e magari fare qualche prova, proprio con l’occhio all’orologio; potremmo avere qualche sorpresa. Per avere un esempio di come funziona ditaa, si può guardare questo [post di John D. Cook](https://www.johndcook.com/blog/2016/06/14/ascii-art-diagrams/).