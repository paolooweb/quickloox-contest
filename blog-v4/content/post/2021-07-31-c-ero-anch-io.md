---
title: "C’ero anch’io"
date: 2021-07-31T00:34:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [Tim Cook, Luca Maestri, Apple] 
---
Non capita tutti i giorni che Luca Maestri, Chief Financial Officer di Apple, parli agli analisti di un caso di successo di Apple in azienda avente per protagonista una società italiana.

Capita anche meno spesso che il sottoscritto faccia parte del progetto.

Stavolta è capitato.

Non aggiungo altro per discrezione. La [presentazione dei risultati finanziari agli analisti](https://seekingalpha.com/article/4441901-apple-inc-aapl-ceo-tim-cook-on-q3-2021-results-earnings-call-transcript) è disponibile pubblicamente a chi avesse tempo da perdere per cercare dettagli.

Festeggio orgoglioso con il ritorno a un monitor 4k dopo due mesi di metà HD, un tiramisù non previsto, una to do list da paura e un weekend che eccezionalmente lascerò libero dal lavoro.

*Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile.*               