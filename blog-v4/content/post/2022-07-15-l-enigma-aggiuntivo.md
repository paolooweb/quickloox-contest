---
title: "L’enigma aggiuntivo"
date: 2022-07-15T01:48:49+01:00
draft: false
toc: false
comments: true
categories: [Internet]
tags: [tf42, Una storia intricata, A Tangled Tale, Carroll, Lewis Carroll, Stampa Alternativa]
---
Mi pare che **tf42** abbia ragione: come può [la mia copia usata di Una Storia Intricata](https://macintelligence.org/posts/2022-07-12-quando-non-cerano-i-nickname/) essere uscita nel 1998 con un prezzo in euro?

Al momento dell’acquisto non ci ho proprio fatto caso, né ci ho pensato dopo.

Mostro il corpo del reato (un po’ come viene perché è notte ed è buio). La copertina:

![La copertina di Una storia intricata, di Lewis Carroll](/images/carroll-fronte.jpg "Una storia intricata… anche nel prezzo di copertina.")

Dentro, il frontespizio. Si vede chiaramente la data della stampa.

![Il frontespizio di Una storia intricata, di Lewis Carroll](/images/carroll-dentro.jpg "A meno che l’editore dichiari il falso, questa copia è stata stampata nel 1998.")

Sulla quarta di copertina campeggia altrettanto chiaramente il prezzo in euro, senza etichette sovrapposte o altri pasticci.

![Il retrocopertina di Una storia intricata, di Lewis Carroll](/images/carroll-dietro.jpg "Nonostante la stampa nel 1998, il prezzo è visibilmente espresso in euro.")

La cosa che rende veramente perplessi è che non si vede traccia di prezzo in lire. Avrei capito una doppia prezzatura, come in qualche caso accadde.

Non solo Carroll si dilettava di enigmi, ma evidentemente l’editore italiano ha deciso di riservarne uno per la nostra epoca. Chi lo sa risolvere?