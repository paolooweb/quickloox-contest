---
title: "Caro Babbo Natale"
date: 2018-12-01
comments: true
tags: [Natale]
---
Caro Babbo Natale,

è stato un anno sull’ottovolante, con una (nuova) figlia che ha dato i punti al papà in fatto di non dormire per periodi indefiniti, più un Mac nuovo e un iPad nuovo, per limitarsi al tema.

Al centro di tutto, nel tema e fuori tema, si è ruotato molto attorno allo hardware.

Quindi non ti offenderai se per l’anno che arriva ti chiedo di portarmi tanto software. Tutto quello che gira su un iPad Pro nuovo fiammante e che forse neanche ho in mente, avendo saltato diverse versioni di sistema operativo. Lo stesso vale per Mac, chiaro.

E poi tanto codice. Niente di impegnativo: scripting, scorciatoie, automazioni ingegnose, riga di comando, AppleScript e magari qualche momento in più su quella macchina virtuale con dentro FreeBsd in modalità Terminale, che tengo da parte per essere pronto veramente a tutto.

Non sono ferrato sul lavoro creativo; so (solo) (forse) scrivere. Monto poco video, organizzo pochissime foto di tutte quelle che scatto, le mie capacità di modellare 3D o riempire uno spartito sono veramente all’essenziale, non so se hai presente quelli che al ristorante cinese fanno la prova tecnica con le bacchette per averle lunghe uguali e che chiudano giuste: sul lavoro creativo avanzato sono quello che fa la prova tecnica con le bacchette.

Nondimeno, ti chiedo di portarmi tanta apertura mentale per restare al passo su quello che succede in questi campi. Perché questi strumenti eccezionali spesso sono sottovalutati e sottoutilizzati, intanto che altri ci fanno sopra meraviglie. Portami la capacità di seguire questi ultimi per scoprire ammirato dove vanno.

Per finire, ti chiedo se riesci a farmi trovare sotto l’albero tanti progetti ambiziosi di blogging e comunicazione personale professionale. Non importa se mancherà il tempo o la possibilità, anzi: preferisco arrivare al Natale 2019 con una lunga lista di cose che avrei voluto fare, ma almeno ho pensato, invece che con un piano sequenza annoiato sul mio ombelico.

Grazie per quello che vorrai fare.

P.S.: amici non servono, grazie. Ne ho già in numero enorme per le mie capacità e per i meriti e ho solo da ringraziare per tanta fortuna. Pensa che alcuni di loro, con pazienza e gentilezza, mi leggono perfino. Qui, davvero, quello che c’è è tutto quello che è giusto sperare. Se avanza tempo, un giro di slitta fallo anche da loro.