---
title: "Chiusi per virus"
date: 2016-04-05
comments: true
tags: [virus, Lidia]
---
Ci scusiamo per l’interruzione del blog dovuta a motivi di salute di Lidia, che finalmente si è liberata di un fastidioso virus intestinale, e del padre di Lidia, che – qui non si butta via niente – ha pensato bene di riciclare il virus intestinale che stava per essere dismesso.

In ambedue i casi l’aggravio quotidiano impedisce di andare oltre la stretta urgenza e le priorità lavorative.

Appena possibile riprenderemo le pubblicazioni. Non che non riesca a scrivere dal bagno con iPad, ma è la concentrazione che manca.