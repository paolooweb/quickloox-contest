---
title: "In viaggio con Pay"
date: 2017-08-17
comments: true
tags: [Pay, Autostrade, Autogrill]
---
Non avevo mai considerato il Wi-Fi di Autostrade per l’Italia e ho commesso un errore grave, perché è l’unica rete pubblica che conosco e stia sopra la sufficienza per usabilità. La procedura per utilizzarlo, infatti, è fare clic sul pulsante Naviga. Finito. Pronto.

Ieri stavo acquistando biglietti ferroviari online durante uno spostamento per Milano quando improvvisamente è saltata fuori su iPhone la rete cittadina, alla quale incautamente mi sono iscritto mesi fa. Non ricordo mai user e password e un messaggio avvisava oltretutto del cambiamento dell’infrastruttura, per cui era necessario ripetere la registrazione.

Tutta perdita di tempo e inefficienza strutturale; nel frattempo la mia operazione di acquisto era stata vanificata e ho dovuto riprenderla più tardi quasi da capo.

Non vedo l’ora che sia ufficiale iOS 11, il quale dovrebbe rinunciare di suo a connettersi ai Wi-Fi perditempo, con credenziali di accesso ridicole (quella di MacDonald’s, per esempio), impossibili da memorizzare e da modificare, e per giunta pressoché inusabili (esempio da manuale, [il Centro di Arese](https://macintelligence.org/posts/2017-03-29-il-terrore-corre-senza-fili/)).

Seconda soddisfazione della giornata: alla cassa dell’autogrill Brianza sulla Milano-Venezia, direzione Venezia, ho visto un adesivo di Pay su registratore di cassa e lettore Bancomat. Niente di inedito ma, se comprare in quel modo e in quel contesto, indica una necessità concreta e non solo una adozione di principio.

 ![Pay all’autogrill](/images/apple-pay-autogrill.jpg  "Pay all’autogrill") 