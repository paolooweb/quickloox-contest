---
title: "Il lavoro sporco"
date: 2014-03-31
comments: true
tags: [Microsoft, Office, iPad, Nadella, Gates, Ballmer, Mac, Windows]
---
Ci sono dubbi e perplessità su che cosa significhi la scelta di Microsoft di pubblicare, [alfine](https://macintelligence.org/posts/2014-03-28-fai-un-salto/), Office per iPad. La spiegazione più chiara che ho trovato è [quella di Jesper](http://waffle.wootest.net/2014/03/27/the-day-microsoft-gave-up-world-domination-and-settled-for-relevance/) [**Aggiornamento del 28 maggio 2020**: purtroppo la pagina non è più in rete]. La rendo in italiano.<!--more-->

>Quando Satya Nadella [il nuovo amministratore delegato di Microsoft] afferma che Microsoft deve raggiungere il pubblico attraverso tutte le piattaforme, quello che mi resta in mente è come tutt’e due le persone prima di lui nel ruolo abbiano sostenuto veementi che quelle piattaforme le avrebbero dominate. Il supporto di Mac era una conseguenza del passato e un errore che non sarebbe stato permesso un’altra volta. Ora Microsoft dà priorità ad altre piattaforme rispetto alla propria e supporta meglio altre piattaforme rispetto alla sua.

>Ballmer e Gates [i precedenti amministratori delegati] pensano che perdere la guerra delle piattaforme, non essere più i più grossi e quelli che nessuno-ha-mai-perso-il-posto-per-avere-scelto-Windows, significhi la fine di Microsoft come la conosciamo, e possono avere ragione. È anche l’inizio dell’unica Microsoft che possa tamponare le ferite e ripartire. Il tempo dove si poteva scegliere è passato da molto. Ora è questione di sopravvivenza. Nadella forse è più libero di doverselo figurare come lo sminuire il lavoro di una vita e ha scelto l’accettazione.

Resto convinto che prima di provare compassione per Microsoft, l’azienda debba ridimensionarsi ancora molto, molto, molto. Detto questo, è esattamente così: alla strategia ballmeriana *Windows Everywhere*, Windows ovunque, che avrebbe trascinato l’azienda all’irrilevanza, è stata preferita la strategia di essere rilevanti – o provarci – anche dove Windows non c’è.

Chiamo a testimoniare un [*post* dello scorso anno](https://macintelligence.org/posts/2013-06-14-q-e-d/) che ne riprende [uno di due anni fa](https://macintelligence.org/posts/2012-02-22-come-fai-sbagli/). Cacciato Ballmer per manifesta cecità verso i fatti, ora il suo successore si ritrova il lavoro sporco da fare: tenere rilevante Microsoft in un mondo che ne può fare tranquillamente a meno. Ora dovrò impegnarmi per riuscire a farne a meno pure su iPad, dopo Mac. Ma la volontà non manca.