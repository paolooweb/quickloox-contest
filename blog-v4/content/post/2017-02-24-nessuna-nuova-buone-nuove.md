---
title: "Nessuna nuova, buone nuove"
date: 2017-02-24
comments: true
tags: [Park, Apple, Jobs]
---
Operativo da aprile, il nuovo campus [si chiamerà Apple Park](http://www.apple.com/newsroom/2017/02/apple-park-opens-to-employees-in-april.html) e conterrà un auditorium da mille posti intitolato a Steve Jobs.<!--more-->

Qualcuno ha voluto cogliere una similitudine tra il nome del nuovo campus e quello dei laboratori Xerox dove venne coltivato il germoglio dell’interfaccia grafica:

<blockquote class="twitter-tweet" data-lang="en"><p lang="en" dir="ltr">Historic mimesis. Xerox PARC and Apple Park: <a href="https://t.co/yXCYuwFkdW">https://t.co/yXCYuwFkdW</a> <a href="https://t.co/DPOVlVJsuy">pic.twitter.com/DPOVlVJsuy</a></p>&mdash; Eli Schiff (@eli_schiff) <a href="https://twitter.com/eli_schiff/status/834433705250742272">February 22, 2017</a></blockquote> <script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>

Gli stessi laboratori dove Jobs prese l’idea dell’interfaccia grafica e ottennero, in cambio dell’uso di alcune idee da parte di Apple, [azioni della società di Cupertino](http://www.macintelligence.org/blog/2014/08/25/azioni-che-contano/).

Abbondano i punti di interesse attorno al campus. Apple ha preteso dai costruttori [precisione inusitata](http://www.macintelligence.org/blog/2017/02/12/sta-nei-dettagli/); le pulsantiere degli ascensori sono ispirate all’interfaccia di iPhone; si tratta di uno dei tetti a pannelli solari più importanti al mondo; internamente si potrà fare a meno dell’aria condizionata nove mesi l’anno. Eccetera eccetera.

Quello che piace a me è l’assoluta normalità del nome. Apple Park. Ispira quiete, ordine. L’ideale per lavorare su progetti mai visti prima. E poi lo Steve Jobs Theater, dedicato a chi ha saputo eccellere nelle presentazioni di prodotto. Tanti altri prodotti e tante altre presentazioni acquisteranno un’ombra della sua aura e la sua non sarà solo un’assenza, ma anche un giusto ricordo.

Sono notizie che mi piace sentire a dispetto della loro apparente insignificanza. Promettono bene. E lo scrivo in quello che sarebbe stato il compleanno di Steve Jobs.