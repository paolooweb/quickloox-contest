---
title: "La super razza"
date: 2023-09-08T17:19:09+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [Ars Technica, Super Mario, Niftski]
---
Delle infinite attività umane classificabili come inutili e anche perditempo, cercare il record della [soluzione più veloce di Super Mario](https://arstechnica.com/?p=1966742) sembra meritarsi un posto in prima fila.

Invece continuo a essere affascinato [lungo gli anni](https://macintelligence.org/posts/2021-11-01-la-grande-corsa/) da questa forma pazzesca di sfida contro la macchina. Perché, come in questo caso, quando raggiunge l’eccellenza è come vedere scalare l’Annapurna o circumnavigare il mondo in solitaria: perditempo, ma dentro c’è qualcosa che ci racconta della natura umana.

La corsa contro Super Mario è arrivata a millesimi di secondo dal limite teorico, un dato incredibile quando si pensa che bisogna inseguire la perfezione al pixel per quasi cinque minuti.

Millesimi di secondo. Non riusciamo neanche a percepirli. Eppure sappiamo inseguire la partita perfetta, consci che non la raggiungeremo mai, determinati però a fare un passo in più.

È la razza umana quella intelligente, quella confusionaria, impaziente, pasticciona, ma anche indomabile, proiettata al futuro, imprevedibile, pronta a reinventarsi.

Se devo scommettere tra l’umano imperfetto e qualunque suo succedaneo artificiale, ho la puntata sicura. E ancora per molti, molti anni come minimo.

Intanto qualcuno rosicchierà un altro millesimo.