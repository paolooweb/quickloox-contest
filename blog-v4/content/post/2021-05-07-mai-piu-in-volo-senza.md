---
title: "Mai più in volo senza"
date: 2021-05-07T00:28:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [watch, Raymond] 
---
Raymond è nato fortunato.

È nato inaspettatamente, assai prematuro, undicimila metri sopra il Pacifico, durante un volo Salt Lake City-Honolulu.

Sull’aereo c’erano, fortunatamente, tre infermiere specializzate in terapia intensiva per neonati.

C’era anche un dottore, cosa statisticamente comune tra i passeggeri di un volo a lungo raggio. Questo dottore, però, aveva fortunatamente ricevuto formazione in medicina di emergenza.

Così l’équipe medica improvvisata, solo per circostanze e non certo per competenza, ha mantenuto Raymond stabile e coccolato per tre ore, prima dell’atterraggio alle Hawaii.

Sull’aereo mancava, prevedibilmente, una sala parto o un assortimento di strumenti per ostetricia. Il cordone ombelicale è stato legato con stringhe delle scarpe. Per tenere caldo il bambino sono state usate bottiglie di plastica riempite d’acqua e scaldate nel microonde.

Per monitorare il suo battito, è stato usato un watch.

Non sono più così rari come qualche anno fa. Per fortuna. Non solo di Raymond.

*Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile.*