---
title: "Camera, non vista"
date: 2022-12-31T04:13:00+01:00
draft: false
toc: false
comments: true
categories: [Hardware, Software]
tags: [iPhone 12, Continuity Camera, OBS Studio, OBS]
---
Apple in questo periodo non è sempre al massimo con il software, ma sull’hardware bisogna lasciarla stare. Ne ho avuto conferma provando Continuity Camera, che trasforma iPhone in webcam di un Mac a patto di avere hardware abbastanza aggiornato. L’arrivo di un [iPhone 12](https://macintelligence.org/posts/2022-12-29-un-nuovo-piano-quinquennale/) mi ha abilitato la funzione e ho potuto collaudarla prima che finisca l’anno.

Spettacolare e persino imbarazzante. Continuity Camera è un pulsante nelle Impostazioni di iOS e, se sono rispettati i requisiti scontati, funziona senza altro chiedere. I requisiti scontati sono per Mac e iPhone Bluetooth attivo, Wi-Fi attivo, lo stesso account iCloud.

Dopo di che, con o senza cavo, basta avere iPhone *in assetto orizzontale* per attivare la funzione. La semplicità è stupefacente, la qualità è ottima. Su Mac si possono attivare Center Stsge o Desk View, che inquadra la superficie della scrivania invece di quello che sta davanti all’obiettivo.

Davvero forte. Per paradosso, potrebbe causare qualche disguido a chi vive di webcam virtuali, magari perché usa [OBS Studio](https://obsproject.com) o qualche altra cabina di regia che consenta di maneggiare più apparecchi da ripresa in contemporanea.

È poco più che una curiosità, un caso particolare e null’altro, però a me è capitato. Sono in videoconferenza e come webcam virtuale uso *un altro iPhone*, vecchio, collegato via cavo e controllato da OBS. iPhone 12 è sul tavolo, non collegato, in standby.

Tutto funziona a meraviglia quando, distrattamente, prendo in mano iPhone 12 con l’idea di controllare una informazione. Per come è messo sulla scrivania, nel prenderlo, si mette in assetto orizzontale per un momento e… diventa la webcam della videoconferenza, scavalcando tutto il resto.

Nel mio caso ho messo iPhone 12 sul treppiede al posto dell’altro modello e me la sono cavata, ma in una occasione più ufficiale o con una regia più complessa di quella che serviva a me, una disattenzione di questo tipo può portare a equivoci simpatici. Per la cronaca, portando iPhone in assetto verticale Continuity Camera si interrompe, ma la connessione che c’era prima non si ripristina automaticamente: va riconfigurata. Se il programma è poco amichevole da questo punto di vista, tornare allo *status quo ante* può diventare scomodo.

Suppongo che il novantanove percento delle persone, anche virgola nove nove nove, non abbia per la testa alcuna idea di regia applicata ai feed video disponibili per un Mac. Per loro, Continuity Camera è una boccata d’aria fresca.