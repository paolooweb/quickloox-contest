---
title: "Non per calcolo"
date: 2022-07-03T00:38:14+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [Graphing Calculator, Calcolatrice Grafica, Avitzur, Ron Avitzur, Swift, C++]
---
Adoro la Calcolatrice Grafica per la sua [storia romanzesca](https://macintelligence.org/posts/2020-09-03-talento-non-calcolato/), per il suo spirito autenticamente Apple, per la sua nascita decisa per passione e non certo determinata dal calcolo economico.

A questo punto diventa per certo anche uno dei programmi per Mac più longevi, perché l’autore Ron Avitzur ha completato il *refactoring*, la riscrittura dell’intera base di codice, che [si sposta da C++ più varie ed eventuali a Swift](https://www.swift.org/blog/graphing-calculator/), il nuovo linguaggio ufficiale di Apple.

Anche stavolta la mossa è frutto di passione più che di strategia. Avitzur ha cominciato *per imparare* e, nel momento della pandemia, lo ha scelto come progetto per passare il tempo dei lockdown e delle quarantene.

Un risultato è che la base di codice è passata da centocinquantaduemila righe a ventinovemila, *senza perdere funzioni né prestazioni*. Come metrica è poco rigorosa per valutare la qualità di un software, ma Avitzur stesso scrive che *però è facile da calcolare*.

Più in generale, la nuova versione è più facile da manutenere, più leggibile, più compatta, più coerente internamente, più tutto. Stando ad Avitzur, potrebbe essere un primo passo verso una pubblicazione del codice con licenza open source, che porterebbe l’immortalità all’opera oltre che la già garantita longevità nel medio periodo (Swift accompagnerà Apple per molti anni).

L’articolo riporta i link alle versioni per Mac e iOS della Calcolatrice Grafica, un gioiello che oggi costa una manciata di euro e, sempre grazie alla transizione verso Swift, ora ha aperto alla realtà aumentata come ulteriore dimostrazione di modernità. Non c’è bisogno di calcolatrici per affermare come la resa valga, stravalga la spesa.