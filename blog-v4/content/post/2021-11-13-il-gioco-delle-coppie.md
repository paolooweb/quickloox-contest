---
title: "Il gioco delle coppie"
date: 2021-11-13T01:28:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [iOS 15, iPadOS 15] 
---
Dopo qualche giorno di utilizzo, ho da aggiungere qualcosa alle [primissime note di adozione di iOS e iPadOS 15](https://macintelligence.org/posts/Aggiornamento-lento.html).

Amo moltissimo la nuova gestione della doppia finestra di lavoro su iPad. Organizzare le coppie di programmi al lavoro contemporaneamente sullo schermo è diventato un gioco. Prima non mi dispiaceva, all’opposto di molte critiche negative, ma certamente ora è migliorato e in modo sensibile.

Vale per tutte le mie occasioni di multitasking e vale ancora di più per Safari, con il quale avevo penato settimane prima di interiorizzare il sistema per eliminare un gruppo di pagine non volute. Il sistema c’era e non era neanche complesso; ero io che non lo vedevo. Con iPadOS, in trenta secondi l’ho visto. Forse è un progresso solo per me, però sono proprio soddisfatto.

Ho puramente provato, senza utilità pratica, la funzione che trasforma in testo le scritte presenti dentro una immagine: funziona alla grande e fa meraviglia.

Le note veloci che spuntano dall’angolo inferiore destro, mi mancavano proprio. Le volte che mi dicevano *prenda nota* e mi impappinavo per lanciare qualcosa con cui farlo davvero non si contano; ora faccio uscire una Quick Note e ho risolto il problema.

App Library non mi è mai parsa una idea particolarmente utile. Però c’è una funzione che consente di tenerla nel Dock. App Library nel Dock è assai più utile di quello che pensavo e la uso regolarmente. Conta perché di solito accedo alle app chiamando Spotlight e digitando le primissime lettere del nome; ora, se non le ho davanti o a distanza di una schermata, apro App Library. Sta diventandomi comoda.

Le funzioni di Focus mi lasciano perplesso perché lavoro per più clienti e ricevere comunicazioni miste fa parte della mia consueta routine. Difficile che possa veramente escludere tutti gli altri quando sto lavorando con uno; se li devo abilitare tutti, tanto vale restare come sono. Per ora lascio lì in attesa di ulteriori approfondimenti.

Ancora una volta è tutto, tranne che per un metacommento finale: era da qualche versione di iOS che il mio flusso di lavoro rimaneva praticamente invariato anche all’indomani di un aggiornamento del sistema. iPadOS ha già avuto un impatto significativo sulle mie modalità di lavoro dopo pochi giorni, positivo.

*Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile.*