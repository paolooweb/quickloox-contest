---
title: "Chi non muore si ripete"
date: 2013-11-07
comments: true
tags: [iWork]
---
Quello che fa Apple può piacere e non piacere. Anche e doverosamente in forma mista, perché [Mavericks](https://itunes.apple.com/it/app/os-x-mavericks/id675248567?mt=12) è meritevole, [Server è acerbo](https://macintelligence.org/posts/2013-11-06-una-splendida-giornata/). I prodotti e le combinazioni sono tanti e tali che non può essere tutto allo stesso livello. Qualche volta i risultati sono inferiori alle aspettative. E non è che [quando c’era Steve fosse diverso](https://macintelligence.org/posts/2012-10-06-signora-mia/).<!--more-->

È inaccettabile il vedere e frequentare Apple da anni, da decenni, e fare finta che le cose succedano per la prima volta. Ignorare scientemente come funziona e meravigliarsi ogni volta perché funziona nel mondo in cui funziona.

È apparsa una pagina su [Funzioni e compatibilità del nuovo iWork per Mac](http://support.apple.com/kb/HT6049):

>Queste applicazioni sono state riscritte da capo perché avessero il pieno supporto dei 64 bit e per riconoscere un formato di file unificato tra le versioni Mac e iOS 7, oltre che per la beta di iWork per iCloud.

Questo era lo scopo. Certo, si sono perse funzioni per strada.

>Contiamo di reintrodurne alcune nei prossimi aggiornamenti e continueremo su base regolare ad aggiungere nuove funzioni.

Segue elenco di alcune delle funzioni che torneranno nei prossimi sei mesi.

Può piacere o non piacere.

Chi c’era due anni fa, però, ha certamente visto e sentito ciò che è successo [due anni fa con Final Cut Pro X](https://macintelligence.org/posts/2011-07-05-provocazione-final/). Stessa cosa, con riscrittura del programma e annuncio delle funzioni che torneranno. Se domani arriverà un’altra svolta tecnologica (iOS sta compiendo il balzo verso i 64 bit), il copione si ripeterà identico oppure significherà che Apple è cambiata radicalmente, eventualità per adesso non verificatasi.

Apple funziona così. Prendere o lasciare; criticare è doveroso, affettare meraviglia e indignazione è stucchevole.