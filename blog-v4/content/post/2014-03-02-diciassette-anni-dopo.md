---
title: "Diciassette anni dopo"
date: 2014-03-02
comments: true
tags: [Apple]
---
Omaggio a una delle [grandi campagne pubblicitarie del secolo scorso](http://www.forbes.com/sites/onmarketing/2011/12/14/the-real-story-behind-apples-think-different-campaign/) nel manifesto di una esposizione, fotografato ieri a Milano.<!--more-->

Involontario? Difficile. Hanno conservato perfino il font.

 ![Visit Different](/images/visit-different.jpg  "Visit Different") 