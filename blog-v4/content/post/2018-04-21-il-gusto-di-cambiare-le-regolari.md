---
title: "Il gusto di cambiare le regolari"
date: 2018-04-21
comments: true
tags: [regex]
---
Si sarà capito che è un momento di molto impegno e poco tempo residuo. Se non altro posso applicare qualche espressione regolare. Niente di geniale, ma piccole cose che improvvisamente modificano qualcosa di altrettanto piccolo in cinquanta file e fanno percepire concretamente il risparmio di tempo. Una di queste, nella figura sottostante, trasforma in numero a inizio capoverso in un tag Html che si chiude pure da solo a fine capoverso.

 ![Una piccola ma soddisfacente espressione regolare che cambia un numero in un tag](/images/uno-strong.png  "Una piccola ma soddisfacente espressione regolare che cambia un numero in un tag") 

Ripeto, sono cose piccole, però il risparmio di tempo lo si tocca con mano ed è un sollievo. Ci sono corsivi che devono diventare grassetti? `(</?)em(>)` diventa `\1strong\2` ed ecco fatto.

Spero che qualcuno le stia insegnando a scuola, le *regex*. Qualcuno lo fa?
