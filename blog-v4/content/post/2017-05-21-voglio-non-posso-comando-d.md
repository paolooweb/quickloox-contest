---
title: "Voglio, non posso, comando-D"
date: 2017-05-21
comments: true
tags: [Cmd-D, Soghoian, AppleScript, JavaScript, Automator, Workflow, automazione, Python, Swift, Playgraounds]
---
So per certo che il 9 agosto non sarò in grado di trovarmi a Santa Clara e so per certo che, se l’agenda fosse libera, ci andrei come all’[appuntamento tecnologico più importante dell’anno](http://www.cmddconf.com) (per me).<!--more-->

La presenza di giganti come Sal Soghoian e Jon Pugh semplicemente autorizza a pensare che Cmd-D sarà un momento irrinunciabile, cui rinuncerò.

Più passano i giorni più sono convinto che l’automazione sarà un tema cruciale per i prossimi anni, oltre a [esserlo già ora](https://macintelligence.org/posts/2016-11-30-fallo-di-mano/) e per un sacco di persone che non ne sono consapevoli.

Tirate fuori AppleScript, Swift Playgrounds, [Automator](http://www.macintelligence.org/blog/categories/automator/), Workflow, Python, le Azioni Cartella, JavaScript, quello che credete meglio, ma iniziate. Se non ve la sentite, almeno capite il concetto, è vitale. Ragionateci sopra e scoprirete che in ufficio [vale un sacco di tempo libero](http://www.macintelligence.org/blog/2014/06/21/il-buon-pastore/) in più se va bene, o la probabilità di mantenere il posto di lavoro e avere un aumento di stipendio se ve meno bene.

Dopo il 9 agosto, cosa che non faccio per altre manifestazioni, aspetterò che vengano pubblicate le registrazioni video e guarderò _tutto_. E invito a fare altrettanto. Almeno guardare, se non toccare.