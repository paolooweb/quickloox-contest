---
title: "Blog Lines Matter"
date: 2022-02-21T01:31:17+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [QuickLoox, Muut, Octopress]
---
In questi giorni i commenti sono scomparsi dal blog a causa di un mio intervento maldestro che, nel tentativo di risolvere un problema, ha portato al cambiamento della struttura degli indirizzi dei post.

[Muut](https://muut.com), il motore dei commenti, è sensibile a questo tipo di situazione e ha bisogno che la struttura stessa resti costante, oppure che gli si cambi la sintassi interna per rispondere a un cambiamento di struttura.

Ora la situazione è tornata a posto e mi scuso con chi abbia commentato nell’interregno; adesso sono scomparsi i loro post, mentre sono tornati tutti quelli preesistenti. Non esiste un modo per avere simultaneamente visibili i commenti relativi a post con più strutture di indirizzo diverse.

La problematica diventerà ancora più interessante nei prossimi giorni, dato che i meccanismi principali del blog funzionano tutti in modo accettabile e così la priorità ora diventa importare i vecchi post.

Nei prossimi giorni tutti i post prodotti con [coleslaw](https://github.com/coleslaw-org) diventeranno nuovamente accessibili e sono numerosi, ma neanche tanti rispetto al totale. Poi arriveranno anche quelli prodotti con [Octopress](http://octopress.org), molti, molti di più.

Accade che la struttura che avevano gli indirizzi dei post con Octopress sia diversa dalla struttura attuale. Se teniamo quella nuova, lavoriamo sul presente e sul futuro, e i commenti lasciati riguardo ai post con Octopress non saranno visibili.

Oppure si potrebbe decidere di ripristinare la vecchia struttura dei post di Octopress. Sparirebbero i commenti di queste ultime settimane e tornerebbero tutti quelli di quel periodo. Più di questo, centinaia di post ritornerebbero a essere accessibili su Internet e a essere linkati in modo funzionante sui siti dove questo fosse accaduto.

Mi interessa molto sentire le opinioni di tutti. È impossibile avere visibili tutti i commenti di tutte le epoche e si tratta di scegliere se privilegiare un passato molto popoloso oppure un altro passato molto recente.

Qualunque sarà la scelta, naturalmente non inficerà i commenti lasciati da lì in avanti, che funzioneranno normalmente.

Una cosa estremamente importante e poco evidente: siano visibili oppure non visibili nell’interfaccia di QuickLoox, su Muut [sono memorizzati tutti i commenti inseriti nel blog](https://muut.com/quickloox#!/feed) da quando è stato adottato Muut stesso, anni e migliaia di post fa (se non erro, nel 2013).

Questo è il motivo principale non detto per cui ho sempre fatto orecchie da mercante verso qualsiasi proposta di cambiare sistema di commenti. Qualcosa, tanto o poco, non verrà mostrato, ma nulla è stato cancellato, neanche una riga, e resta accessibile.
