---
title: "Antico e moderno"
date: 2015-11-07
comments: true
tags: [San, Francisco, font, SGI, Silicon, Graphics]
---
Bello il salto dai primordiali (nel senso buono) [font monospaziati di Silicon Graphics](https://macintelligence.org/posts/2015-11-05-per-rinfrancar-lo-spirito/) ai segreti del [San Francisco usato su tutti i prodotti Apple](https://medium.com/@mach/the-secret-of-san-francisco-fonts-4b5295d9a745#.fbno47il8).<!--more-->

C’è da scoprire che il font è lo stesso per tutti ma si comporta in maniera diversa a seconda dell’apparecchio e del corpo. Più una serie di grandi piccolezze come la centratura verticale automatica dei due punti quando si trovano in mezzo a due numeri. Eccetera.

Vanno apprezzate questi approfondimenti perché San Francisco svolge egregiamente il proprio lavoro: ovvero, non ci si accorge minimamente di quello che accade a livello quasi microscopico di pixel, perché tutto fila via liscio e il messaggio che dobbiamo ricevere arriva senza problemi.

Eppure succedono tante cose ed esserne consapevoli fa salire di un gradino la scala della consapevolezza di quello che facciamo davanti allo schermo. Considerate le ore che ci passiamo davanti, non è un male.