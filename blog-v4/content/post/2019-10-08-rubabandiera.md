---
title: "Rubabandiera"
date: 2019-10-08
comments: true
tags: [Cina, Taiwan, Apple, iOS]
---
Ognuno ha la propria sensibilità rispetto a dove tracciare una linea. Riconosco che la mia sensibilità è bizzarra ma me la tengo e voglio dire che [nascondere la bandiera di Taiwan sugli apparecchi iOS in funzione dentro Hong Kong o Macao](https://www.theverge.com/2019/10/7/20903613/apple-hiding-taiwan-flag-emoji-hong-kong-macau-china) è un atto che veramente non mi piace.

Perché è veramente infantile, forse un tentativo di dare un contentino ai cinesi per tenerli buoni. Infatti se digiti *Taiwan* l’emoji viene fuori, è lì solo che non viene mostrato.

Non mi piace l’idea di genuflettersi ai cinesi per salvaguardare il business. La capisco – non la giustifico, la capisco – dal punto di vista, appunto, contabile. Se però cali i pantaloni, li cali e basta. Non c’è una via di mezzo.

Posso anche capire – non giustificare, capire – situazioni come quella di *Quartz*, pubblicazione [sospesa da App Store su richiesta del governo cinese](https://9to5mac.com/2019/10/09/apple-china-quartz-app-store/). È una nazione estera, si voglia o non si voglia ha la sovranità di fatto sul territorio, sono dittatori ma le leggi nel loro Paese le fanno loro e inevitabilmente ci si deve quanto meno confrontare. Disobbedire è coraggioso in teoria, difficile in pratica; si potrebbe anche obiettare che, per la causa della libertà di pensiero, fa molto più un Apple Store operante concretamente a Shanghai che mille boicottaggi; è più facile desiderare maggiore libertà di dialogo e di espressione con un comunicatore in tasca, che senza.

Ok. Cali i pantaloni. Non piace, ma – cinicamente – *business is business*.

Poi però succedono cose come [HKmap.live](https://hkmap.live). Apple aveva ritirato la app del servizio dallo Store su richiesta cinese, dato che i cittadini di Hong Kong la usano per eludere le forze dell’ordine quando le cose si mettono male in piazza.

Ora la app è tornata è i cinesi [si sono infastiditi](http://en.people.cn/n3/2019/1009/c90000-9620878.html).

[**Aggiornamento del 14 ottobre 2019:** [è sparita di nuovo](https://qz.com/1725175/apple-removed-a-hong-kong-protest-map-from-its-app-store/)].

>L’approvazione della app da parte di Apple aiuta ovviamente i manifestanti. […] Significa che Apple intende essere loro complice? […] La app è solo la punta dell’iceberg. Nell’Apple Music Store di Hong Kong, c’era anche una canzone che auspicava la “Hong Kong independence”. Era stata ritirata dallo store ed è tornata.

A poco serve obiettare che, nella realtà, HKmap.live [aiuta i cittadini a rispettare le leggi di Hong Kong](https://www.theregister.co.uk/2019/10/02/apple_hong_kong/). La polizia è obbligata a issare una bandiera blu nei luoghi di disordini, per dare modo ai cittadini ignari di allontanarsi per non rischiare di essere coinvolta. Nel segnalare la situazione sulla cartina aggiornata minuto per minuto, la app non fa che estendere online il servizio, chiamiamolo così, offerto dai poliziotti. Tutti contenti in punta di diritto, ma è chiaro che l’interpretazione cinese vorrà essere diversa.

Tutti stanno calando i pantaloni. Le uniche forme di testimonianza pro-libertà di pensiero di cui sono a conoscenza sono [quella dell’NBA](https://www.npr.org/2019/10/08/768225490/nba-defends-freedom-of-speech-for-employees-as-china-moves-to-block-block-games) la quale ha difeso un po’ a mezza bocca il diritto di parola di chi lavora nel proprio ambito. A mezza bocca, non proprio convinti, ma più o meno. E poi basta, a meno che contiamo la canzone che torna su iTunes anche se inneggia all’indipendenza di Hong Kong.

Tim Cook non esita a schierarsi per i diritti quando lo ritiene opportuno. Può farlo anche adesso e sarebbe una bellissima occasione di fare la cosa giusta, anche se non fa bene al business. Oppure può fare business e ingoiare il rospo, condannabile ma almeno coerente: se non altro, dire *caliamo i pantaloni davanti a una dittatura* rende più chiaro che dall’altra parte c’è una dittatura.

Basta che sia coerente. Il vedo-non vedo della bandierina negli emoji non si può sopportare.