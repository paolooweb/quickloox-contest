---
title: "Collaborare come mai prima"
date: 2021-08-18T01:40:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [curricolo, scuola, BBEdit, JEdit, Atom, Pandoc, Markdown, Etherpad] 
---
Se una scuola decidesse davvero di [ragionare in modo moderno sull’insegnamento interdisciplinare degli skill digitali](https://macintelligence.org/posts/Un-curricolo-per-lestate.html) e chiedesse a me che conta di più in assoluto al momento di decidere il software da usare, risponderei _le funzioni di collaborazione_.

Questo perché il software mainstream odierno fa molto poco oltre al portare in digitale modi di lavorare che erano presenti anche prima dei computer. Chi mi ha rimproverato di pensare troppo presto ai computer nella scuola, dove si è dimostrato nel tempo più producente disegnare a mano più che usare il CAD o imparare a fare i conti prima di affidarsi alla calcolatrice, ha ragione da vendere.

Ma come faceva a *collaborare*?

Come nasceva nel mondo analogico uno scritto a dieci mani?

Qualunque fosse il modo e posto che ve ne fosse uno, le possibilità di collaborare *simultaneamente* attraverso computer e rete sono immensamente superiori sotto qualsiasi metrica, tempo, efficacia, precisione, tutto.

_Non c’è confronto tra collaborazione digitale e collaborazione analogica_.

Le prossime generazioni opereranno in un mondo ancora più digitalizzato dell’attuale. La collaborazione presenta vantaggi decisivi in numerose attività. Ne segue che una scuola coscienziosa deve trasmettere agli studenti le nozioni che servono per collaborare al meglio. Nozioni che, meglio chiarire, sono in gran parte non tecniche e attengono al lavoro in team.

Parlando di testo (siamo ancora a ragionare su come iniziare a insegnare a scrivere in digitale e comprendere l’ipertesto), i word processor nazionalpopolari comprendono tutti funzioni di collaborazione. Peccato che siano tutte un _afterthought_, un’aggiunta a posteriori, e che si veda, con la possibile eccezione dei Google Docs.

Perché allora non scegliere un sistema libero, nato per la collaborazione, semplice a sufficienza per dare zero problemi di accessibilità e supporto?

Ne esistono diversi. A me piace particolarmente [Etherpad](https://etherpad.org). Lavora perfettamente in tempo reale, chiede pochissime risorse, permette di sapere chi sta scrivendo che cosa, può essere provato senza impegno sui molteplici server pubblici che lo mettono a disposizione in mille varianti ed è facile da installare e da amministrare, volendolo, su un server locale, dove può essere utilizzato in una rete circoscritta (per esempio quella di una scuola) anche fuori da Internet. Costo, quello del software libero.

Una azienda astuta adotterebbe Etherpad con risparmi clamorosi rispetto a qualsiasi altra alternativa commerciale. Una scuola lungimirante, pure. Avrebbe l’ulteriore vantaggio di introdurre gli studenti alla collaborazione nella maniera più semplice ed efficace.

Ma gli altri formati? Se ci vogliono Pdf? E che dire di quelli che chiedono per forza Word altrimenti non sono capaci? (Un bel biglietto da visita per i sostenitori di Office, eh).

Prossima volta.

*Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile.*