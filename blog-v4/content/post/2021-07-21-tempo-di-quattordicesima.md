---
title: "Tempo di quattordicesima"
date: 2021-07-21T00:30:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [BBEdit, Coyote Cartography, Anaconda, Notebook, Lisp] 
---
Lo sanno anche i terminali stupidi, che è uscito [BBEdit 14](https://www.barebones.com/products/bbedit/bbedit14.html). Lo sa chiunque al mondo che lo acquisto istantaneamente appena finito questo post (scritto con BBEdit, va da sé).

Non tutti sanno perché dovrebbero mettere da parte le riluttanze e almeno provare la versione trial (che a termine trial resta gratis in forma di eccellente editor di testo, privo delle funzioni utili al coding), se proprio non si ha il buonsenso di effettuare direttamente un investimento in ottimo software [Mac-assed](https://daringfireball.net/linked/2020/03/20/mac-assed-mac-apps).

A questo pensa *Coyote Cartography* con [BBEdit 14, e perché dovrebbe interessarti](https://micro.coyotetracks.org/2021/07/19/bbedit-and-why.html). La sua carrellata sui punti di forza storici di BBEdit è ineccepibile e meritevolmente contenuta, di lettura veloce oltre che facile. La disamina sulle principali [novità della quattordicesima versione di BBEdit](https://www.barebones.com/support/bbedit/notes-14.0.html) si ferma alla sostanza più importante (il supporto dei server di linguaggio, il Notebook che mi metterà davvero in crisi rispetto all’uso che faccio delle Note di macOS, gli ambienti virtuali di Anaconda) e permette di capire subito se sia ora di acquistare oppure no.

Io acquisto, perché senza BBEdit andrei veloce la metà; e poi perché BBEdit 14 aggiunge il modulo per scrivere in linguaggio [Lisp](https://clisp.sourceforge.io). Non posso esimermi.
Scherzi a parte, una nuova versione di BBEdit – soprattutto, *questa* nuova versione – è come ricevere la quattordicesima in termini di soddisfazione. Il dover pagare, invece di incassare, è una piccolezza.

*Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile.*