---
title: "Scampoli d’assenza"
date: 2018-07-17
comments: true
tags: [Python, Bioware, Guido, van, Rossum, Ohlen, Baldur’s, Gate, Jobs]
---
Capita: una persona costruisce un universo, di idee, di prodotti, di creazioni, di immagini, di racconti, di capitali, di qualsiasi cosa. A un certo punto lascia. In un certo senso succede anche con i figli, che quando sono pronti per camminare con le loro gambe fanno da soli. E la grandezza si misura anche per come si è preparato il proprio lasciare.

Capita: Guido van Rossum [abbandona i panni di Benevolent Dictator di Python](https://mail.python.org/pipermail/python-committers/2018-July/005664.html), linguaggio tra i più popolari (su Mac basta scrivere `python` nel Terminale per trovarselo davanti e `quit()` per rimetterlo a dormire. In mezzo, di tutto). Guido ha creato Python a fine anni ottanta e stava nel ruolo da oltre trent’anni: il Benevolent Dictator è figura tipica in un progetto open source, autorità suprema che decide le direzioni da prendere e scioglie le indecisioni.

Capita: Dopo ventidue anni di lavoro straordinario, [James Ohlen abbandona Bioware](https://twitter.com/JamesOhlen/status/1017417416601726977).

<blockquote class="twitter-tweet" data-lang="en"><p lang="en" dir="ltr">After 22 years I have retired from BioWare. I&#39;ve loved my time with Anthem, Star Wars, Dragon Age and Dungeons and Dragons. But I need to take a break from the industry and work on something a little smaller and more personal.</p>&mdash; James Ohlen (@JamesOhlen) <a href="https://twitter.com/JamesOhlen/status/1017417416601726977?ref_src=twsrc%5Etfw">July 12, 2018</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script> 

Personalmente ho adorato la serie *Baldur’s Gate*, disponibile a vario titolo per macOS e iOS. Ohlen ha ricreato perfettamente lo spirito di *Dungeons & Dragons** e scommetto che avrà raggiunto lo stesso risultato con le altre ambientazioni in cui si è cimentato. Sono anche felice che la sua prossima attività sarà proprio di [creazione di contenuti per D&D](https://twitter.com/JamesOhlen/status/1017417894697881600):

<blockquote class="twitter-tweet" data-lang="en"><p lang="en" dir="ltr">The most fun I&#39;ve ever had at BioWare was as the lead designer on Baldur&#39;s Gate 1+2 and NWN. I&#39;ve been a D&amp;D fanatic since I was 10 years old and I want to be a part of it again. Please visit <a href="https://t.co/412NLtStkx">https://t.co/412NLtStkx</a> to see what I&#39;m talking about.</p>&mdash; James Ohlen (@JamesOhlen) <a href="https://twitter.com/JamesOhlen/status/1017417894697881600?ref_src=twsrc%5Etfw">July 12, 2018</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script> 

Chissà se nei prossimi anni qualcuno scriverà nella comunità Python *si stava meglio quando c’era Guido* o si lamenterà dei giochi Bioware perché Ohlen lavora ad altro. Chi ha orecchie intenda.

(Grazie a [Rocco Tanica e Claudio Bisio](https://www.youtube.com/watch?v=Op08-a0Fv10) per l’ispirazione del titolo).

<iframe width="560" height="315" src="https://www.youtube.com/embed/Op08-a0Fv10" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>