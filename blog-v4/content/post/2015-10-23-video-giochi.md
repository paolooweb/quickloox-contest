---
title: "Video-giochi"
date: 2015-10-23
comments: true
tags: [Apple TV, Unity3D, publish, Microsoft]
---
Giorni di mail ricevute. Questa è da [Roberto](mailto:rscarciello@gmail.com), che trovo particolarmente interessante come testimonianza evidente di come i prodotti con attorno un ecosistema – in questo caso TV – possano diventare opportunità.<!--more-->

Ho un’agenda complicata in questo periodo e non so se riuscirò a raggiungere Roberto la sera della sua lezione, ma se qualcuno lo facesse e me lo raccontasse, ne sarei davvero contento. È una bella occasione per scoprire cose nuove. Ecco la mail.

**Inizio con il dirti** che, fortunatamente, sono entrato in possesso di uno dei [Development Kit della nuova AppleTV](https://developer.apple.com/tvos/) e, contemporaneamente, sono diventato alpha tester della prossima versione di [Unity3D](http://unity3d.com/) che la supporterà.

Devo dirti che, dopo il touch 3D, l’annuncio della AppleTV è stato forse quello che ho trovato più interessante all’evento Apple. Quest’apparecchiatura apre, per noi sviluppatori, diverse nuove opportunità e mercati, a patto di saperla sfruttare.

Il costo, decisamente conveniente, se non addirittura contenuto, unito allo store di giochi ed applicazioni la rendono a tutti gli effetti uno strumento di intrattenimento completo, in cui vedo il degno successore della Wii.

Questo a patto di comprendere pregi e difetti del mezzo. AppleTV non è un dispositivo iOS (pur condividendone molti aspetti) e soprattutto non si usa come un dispositivo iOS. Progettare software per questa apparecchiatura significa fare uno studio di usabilità molto differente da un dispositivo touch.

Questo, però, non è frustrante, al contrario è addirittura stimolante, riuscire a mettere le mani su un nuovo dispositivo come questo è stata per me una vera fortuna, che ho intenzione di sfruttare a pieno.

Sto, infatti sviluppando due videogame specifici per il dispositivo.

Il primo è un semplice arcade 2D in cui bisogna guidare un aereo lungo un percorso infinito cercando di raccogliere sacchi della posta da consegnare e taniche di benzina con cui rifornirsi ed evitando, al contempo, grattacieli e nuvole temporalesche. Il gioco si avvale sia dell’accelerometro che del controller touch di AppleTV; entrambi mi hanno sopreso per prontezza ed efficenza di risposta. Ovviamente sono supportanti anche i controller esterni.

Il secondo, il cui titolo è Bath of Math (che tradotto vorrebbe essere Bagno di Matematica), ha lo scopo di rinfrescare le regole della matematica sia di base che un po’ più avanzata (e con quesiti logici) per i bambini delle elementari e medie. Il gioco è ambientato in una vasca da bagno: la schiuma forma, di volta in volta, un quesito matematico, mentre bolle con le risposte (giuste e sbagliate) salgono verso l’alto. Il giocatore deve far scoppiare la bolla con la risposta giusta. Il tempo limite è dato da un termometro; se l’acqua diventa fredda il bagno finisce, mentre rispondendo in modo esatto il termomentro sale. Se invece si seleziona la risposta sbagliata il termometro scende.

La sfida dello sviluppo, oltre che sulla grafica, è principalmente incentrata nel rendere semplice la selezione della bolla con la risposta; l’uso della funzione touch del telecomando in modo differente dal solito – con una seleziona della bolla successiva e precedente – in modo guidato è il fulcro del lavoro affinché il gioco sia divertente e non frustrante. Ho scelto di privilegiare l’AppleTV per il lancio perché questo tipo di giochi si presta molto bene ad essere giocato in famiglia e la TV, ancora oggi, funziona da punto di concentrazione dell’attenzione.

Per entrambi i giochi sto lavorando con l’*alpha* di Unity3D in modo da poterne fare il *porting* molto rapidamente verso gli altri *device* una volta uscita la versione per AppleTV.

A proposito di Unity3D: dopo aver realizzato un videogame che [vinse il primo premio](http://www.polihub.it/blog/polihub-per-publish-hackathon-globale-di-microsoft/) nella sezione innovazione e design un anno e mezzo fa all’evento *//publish/* organizzato da Microsoft, nel corso di quest'anno ho realizzato un videogame per dispositivi *mobile* per l’Unione Europea. il titolo è [Happy OnLife](https://ec.europa.eu/jrc/en/scientific-tool/happy-onlife-game-raise-awareness-internet-risks-and-opportunities) ed il suo scopo è sensibilizzare adulti e bambini sul problema del ciberbullismo.

A seguito di queste mie esperienze ho organizzato un corso, con il patrocinio del Comune di Rescaldina, in provincia di Milano, relativo al *design* e sviluppo di videogame proprio con Unity3D. Venerdì 30 ottobre terrò una lezione gratuita per chiunque desideri parteciparvi e comprendere come si svolgerà. Le lezioni saranno tenute alle 20:30 presso la [biblioteca comunale di Rescaldina](http://www.comune.rescaldina.mi.it/cittadino/uffici/1/).
