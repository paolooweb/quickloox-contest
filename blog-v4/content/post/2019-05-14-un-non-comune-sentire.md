---
title: "Un non comune sentire"
date: 2019-05-14
comments: true
tags: [IA, Writer]
---
A seguito dello [sproloquio di ieri](https://macintelligence.org/posts/2019-05-13-sviluppi-editoriali/) comprendente IA Writer, devo riportare il [dettaglio dei miglioramenti tipografici apportati al programma](https://ia.net/writer/blog/a-typographic-christmas).

Lo trovo entusiasmante da un punto di vista ideale, indipendentemente dal gradimento o meno per i nuovi font. I quali però sono espressamente studiati per consentire variazioni e aggiustamenti in quantità praticamente infinita per figurare al meglio su ogni apparecchio e in ogni configurazione. Ci sono compensazioni automatiche delle spaziature orizzontali e verticali, un lavoro appassionato per arrivare a un font che riunisca le caratteristiche migliori di monospaziato e proporzionale insieme.

Dove si capisce che la strada è interessante, è qui:

>La parte folle è che probabilmente non ti accorgerai di niente. Però lo potresti sentire.

I font [sono su GitHub](https://github.com/iaolo/iA-Fonts), scaricabili gratuitamente.

A una software house indipendente, che potrebbe tranquillamente cavarsela con un menu di otto font abituali, veramente non so che cosa si potrebbe chiedere di più. Anche solo scaricare i font, che non costa niente, è un sostegno per quanto piccolo alla loro causa, che oggi è tangente alla causa per la buona tipografia.

Forse non si vede, ma la tipografia è una causa cruciale. Perché si sente.
