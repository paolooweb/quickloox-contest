---
title: "L’istante del cannibale"
date: 2021-01-08
comments: true
tags: [ App, Store]
---
Apple si è messa di traverso a Facebook e all’intera industria della pubblicità sulla questione della trasparenza del tracciamento. Su App Store vengono pubblicate le [privacy label](https://developer.apple.com/news/?id=3wann9gh) delle app esattamente come i valori nutritivi sulle etichette dei prodotti alimentari.

Gli sviluppatori fremono di fronte all’idea di dover chiedere agli utilizzatori il permesso di essere tracciati in giro per Internet.

Apple si sta facendo numerosi nemici a proposito di un business, App Store, di cui si possono vedere grazie a *Asymco* le [curve di crescita 2020](http://www.asymco.com/2021/01/07/app-story-2/). C’è una crescita esponenziale in atto, ancora dopo dieci anni di attività. Una fabbrica di soldi.

Apple è disposta evidentemente a cannibalizzare vendite di app e rapporti con gli sviluppatori in nome di una maggiore privacy per gli abitanti del suo ecosistema.

L’autocannibalizzazione d’altronde è stata sempre teorizzata, dai tempi di Steve Jobs: prima che un concorrente metta fuori mercato un tuo prodotto, cosa che avverrebbe comunque, fallo tu.

Tutto questo fa capire dove stia il valore autentico per Apple: come dice Horace Dediu appunto su *Asymco*,

>la forza e la resilienza di Apple stanno nella sua base clienti.

Non è il dato di vendita degli iPhone, non è la prossima *one more thing*; è il rapporto con chi ha comprato. Che non finisce all’emissione di uno scontrino, quello semmai è l’inizio.

Da chiedersi, in modo netto, dove stia il valore per le altre aziende che si muovono negli stessi spazi di mercato.
