---
title: "Si fa ma non si dice"
date: 2017-01-24
comments: true
tags: [Xiaomi, Apple, Samsung]
---
Da anni Samsung non divulga più i dati di vendita dei suoi computer da tasca. Adesso [ha smesso di farlo anche Xiaomi](https://techcrunch.com/2017/01/11/xiaomi-2016-to-2017/).<!--more-->

Viene fuori che la crescita lampo degli ultimissimi anni ha rallentato di brutto e l’azienda non riesce più a rispettare gli obiettivi di crescita che si è data.

Era già venuto fuori che Xiaomi non guadagna dalla vendita di computer da tasca. Almeno fino a che vende nelle quantità attuali.

Apple pare l’unico fabbricante importante che divulga regolarmente le proprie cifre di vendita. E si è già ribadito come raccolga la gran parte dei profitti nel mercato dei computer da tasca, se non tutti.

Già detto: fidarsi di una azienda cui conviene produrre apparecchi sempre migliori, perché ci guadagna in modo soddisfacente, o di altre che vendono in perdita e sperano in un futuro indefinito, da raggiungere tagliando qua e là fintanto che permane il rosso nei conti?