---
title: "Un viaggio inaspettato"
date: 2018-06-21
comments: true
tags: [Mac, Octopress, jekyll, Coleslaw, Lisp]
---
Quando salto un giorno di blog vado a dormire di cattivo umore e questo dà la misura di come mi senta dopo due mesi di assenza. La parte positiva è che questo iato mi ha ridato l’idea del valore di quello che faccio (non fosse altro che a titolo personale) e chiarito che la voglia di proseguire è più viva che mai. Subito ringrazio chi mi ha chiesto notizie; ogni mail, ogni messaggio è stato un pungolo importante.

È successo che, di ritorno da un viaggio, abbia trovato il Mac morto. Il giorno prima c’era stato un temporale, forse coincidenza, forse causa. Impossibilitato per tutta una serie di ragioni (fortunatamente non economiche) a mettere lì un altro Mac e semplicemente ripristinare un backup, ho pensato di cogliere l'occasione per rinnovare il motore del blog, cosa covata a lungo e mai avvenuta.

Ed è iniziato il mio piccolo viaggio inaspettato.

Dovevo occuparmi di [Octopress 3](https://github.com/octopress/octopress), di [jekyll](https://jekyllrb.com/); mentre dedicavo il poco tempo disponibile alla questione, mi sono ritrovato a riflettere su che cosa volevo veramente dal blog. Non è mai stato denaro, né autoaffermazione né capriccio; piuttosto un risultato umano – stare in contatto con persone belle e interessanti – e tecnico, imparare cose nuove.

Il primo mi spronava a ripartire prima possibile, abbastanza scontato. Il secondo… più mi addentravo nelle procedure che mi servivano, meno mi interessavano. Il livello di apprendimento che potevo raggiungere non valeva la fatica. Ho scoperto, lentamente, che ero disposto a fare più fatica, a patto di avere molto più da imparare.

Per questo, nel tempo, l’obiettivo si è spostato da *ripartire* a *capire che cosa volevo*. Il tempo, già poco, si è dileguato nell’analisi lenta di alternative e di specifiche. Ora ho le idee molto più chiare e so che per avere quello che ho adesso, alle condizioni che desidero, impiegherò molto e il risultato sarà anche più modesto dell’attuale, almeno inizialmente; tuttavia avrò la soddisfazione di essermi avvicinato al codice come non avrei mai immaginato e sopratutto di farlo in un ambiente software che amo. Il poco che sarò riuscito a fare sarà in parte merito mio.

Cosa importante, starò lontano dalle soluzioni troppo semplici e omologate. Usare Wordpress è facilissimo e per questo è il motore più usato su Internet. [Lo ha adottato persino Seth Godin](https://seths.blog/news/welcome/), *guru* del marketing online.

Massimo rispetto per chi lo sceglie o pratica alternative equivalenti. Preferisco essere maggiormente padrone dei miei strumenti, anche a costo di rinunce. E avere la possibilità di segnare una differenza, disposto a rischiare che sia a mio sfavore.

(Rinunciare ai motori omologati comporta la sostanziale necessità di frequentare GitHub, [da poco uno dei tentacoli di Microsoft](https://blog.github.com/2018-06-04-github-microsoft/). È un caso in cui scegliere il male minore, sperando che la situazione migliorerà).

Contemporaneamente mi sono reso conto che, è questo è un merito, jekyll mi ha consentito di recuperare facilmente la versione di Octopress usata fin qui, risolvendo al mio posto una intricata problematica di conflitti tra versioni software e installazioni di componenti esoterici.

Dunque, si riparte. All’apparenza non è canbiato nulla. In realtà sono affascinato e spaventato il giusto da quello che ho deciso di volere raggiungere. E nel frattempo ho di nuovo il vecchio motore, per quanto obsoleto; posso di nuovo comunicare e intanto studiare, provare, apprendere.

Affascinato e spaventato, più o meno [come Bilbo Baggins nella camera del tesoro e, anche, di Smaug](https://it.m.wikipedia.org/wiki/Lo_Hobbit_-_Un_viaggio_inaspettato). Non possiedo la malizia dello hobbit; però il peggio che mi possa capitare è cestinare un esperimento malriuscito. Ho solo da guadagnarci.

Grazie a chi fosse ancora in ascolto, per la grande pazienza e la fiducia.
