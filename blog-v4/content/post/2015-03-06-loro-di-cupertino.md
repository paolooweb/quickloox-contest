---
title: "L’oro di Cupertino"
date: 2015-03-07
comments: true
tags: [Ive, watch, Vickers, Hublot]
---
Più che un’intervista, [l’articolo del Financial Times su watch](http://howtospendit.ft.com/technology/77791-the-man-behind-the-apple-watch) è un altarino a Jony Ive, capo del *design* in Apple. C’è però questa perla.<!--more-->

>Ive spiega come le molecole nell’oro di Apple siano più vicine del solito e lo rendano due volte più duro dell’oro comune. Se ve lo state chiedendo, l’acciaio forgiato a freddo di Apple dura il 40 percento di tempo in più dell’acciaio regolare.

Sono già partite le ironie nei salotti di allenatori della Nazionale, giudici di Sanremo e naturalmente *masterchef* italiani. Qui si preferisce rinviare a una lettura di approfondimento.

>Si descrive un composto metallico a matrice che usa un metallo prezioso come componente. In un caso, il metallo prezioso consiste in oro e il composto ha una frazione di massa d’oro che rispetta la definizione di 18 k. Il composto può essere realizzato per mezzo della miscelazione di polvere di metallo prezioso (per esempio oro) e polvere di ceramica, con successiva compressione dentro uno stampo […]

[Brevetto numero 20140361670](http://appft1.uspto.gov/netacgi/nph-Parser?Sect1=PTO1&Sect2=HITOFF&d=PG01&p=1&u=/netahtml/PTO/srchnum.html&r=1&f=G&l=50&s1=20140361670.PGNR.) dell’Ufficio marchi e brevetti degli Stati Uniti, depositato il 4 giugno 2014 da quattro dipendenti Apple.

La durezza del composto così ottenuto, illustra il brevetto, è come minimo di 400 Hv sulla scala di Vickers, la cui [pagina italiana su Wikipedia](http://it.wikipedia.org/wiki/Scala_Vickers) è desolante rispetto [all’edizione inglese](http://en.wikipedia.org/wiki/Vickers_hardness_test).

L’oro ha normalmente [durezza 188-216](http://en.wikipedia.org/wiki/Hardnesses_of_the_elements_(data_page)) sulla scala di Vickers.

Non è detto che tra il prodotto e il brevetto ci sia correlazione, tanto meno causa. Però sì, a Cupertino hanno studiato per avere oro più duro dell’oro.

**Aggiornamento:** Hublot – che vende orologi del valore di decine di migliaia di euro – ha un brevetto per la produzione di oro più duro dell’oro. Lo pubblicizzano in un [filmato](https://www.youtube.com/watch?v=wfOV87WUL-8) dove spiegano di avere mantenuto l’esclusiva della procedura di realizzazione, senza cederla a chicchessia. Il filmato mostra valori Vickers diversi da quelli che riporta Wikipedia, ma chiarisce comunque che: a) Apple ha sviluppato autonomamente un procedimento diverso, a meno che stia violando il brevetto Hublot; b) le ironie sono fuori luogo.

<iframe width="650" height="366" src="https://www.youtube.com/embed/wfOV87WUL-8" frameborder="0" allowfullscreen></iframe>