---
title: "La figura del mecenate"
date: 2017-01-25
comments: true
tags: [Magnetic, Media, Network, Airoldi, Paganini]
---
Succede che [Magnetic Media Network](http://mmn.it), da sempre in prima fila per vendita, supporto e servizi Apple alle aziende, abbia raccolto in decenni di attività una quantità apprezzabile di cimeli Apple e non solo. E pertanto decida di destinare una parte di spazi aziendali a una esposizione permanente di retrotecnologia.

Tutto molto diverso da quello che succede al museo [All About Apple](https://www.allaboutapple.com) di Savona, a eccezione della passione. E direi, oltre che diverso, complementare.

Ho avuto il privilegio di essere stato invitato a una serata di inaugurazione e presentazione della raccolta, presentata e organizzata con l’aiuto del fantastico [Stefano Paganini](http://www.stefanopaganini.com), una delle anime italiane del *retrocomputing* e una fucina inesauribile di aneddoti, fattoidi, nozioni sconosciute a chiunque altro.

L’evento, per bocca di [Damiano Airoldi](https://www.linkedin.com/in/damianoairoldi), Ceo di Magnetic Media Network e fonte energetica primaria di passione e competenza per la nascita della collezione, è stato una sorta di beta pubblica della collezione, che ha bisogno di aggiustamenti e rifiniture ma un domani molto prossimo potrebbe valere una visita per qualsiasi appassionato.

Beta o meno, ci sono pezzi estremamente interessanti a partire – letteralmente – da una [Programma 101 Olivetti](http://www.storiaolivetti.it/percorso.asp?idPercorso=630) che è il primo vero esempio di computer da tavolo, metà degli anni sessanta, ed è tutto dire. La P101 campeggiava vicino a Damiano, che maneggiava fiero una unità di memoria come quella usata nella macchina: un lungo cavo conduttore avvolto circolarmente, dove il bit corrisponde a una vibrazione indotta da un campo magnetico. La vibrazione si propaga lungo il cavo e il bit viene memorizzato per tutto il tempo che gli occorre a raggiungere l’altra estremità. Pazzesco.

 ![Damiano Airoldi vicino alla Programma 101](/images/p101.jpg  "Damiano Airoldi vicino alla Programma 101") 

Damiano è un vero mecenate dei tempi attuali e va ammirato per questa realizzazione, che sono sicuro continuerà a crescere nel tempo per quantità e qualità. E ieri sera, ai nostri occhi, ha fatto bellissima figura.

 ![Riviste d’epoca](/images/byte.jpg  "Riviste d’epoca") 