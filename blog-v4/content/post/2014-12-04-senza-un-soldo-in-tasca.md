---
title: "Senza un soldo in tasca"
date: 2014-12-04
comments: true
tags: [Bellroy, NYT, Darlin, ApplePay, Otterbox]
---
O meglio, con venti euro. Geniale l’[articolo di Damon Darlin sul New York Times](http://www.nytimes.com/2014/11/29/upshot/cashless-society-its-already-coming.html) sulla vera rivoluzione di [Apple Pay](https://www.apple.com/apple-pay/), che non si preoccupa di eliminare la carta di credito; bensì il *portafogli*.<!--more-->

È possibile cavarsela egregiamente, scrive Darlin, con in tasca una carta di credito, la patente (almeno per ora, aspettandone la digitalizzazione) e venti dollari liquidi.

Più ovviamente un iPhone e magari una [custodia](http://www.otterbox.com/Commuter-Series-and-Commuter-Series-Wallet-Case-for-iPhone-6/apl4-amp600-set,default,pd.html) che tenga conto delle altre necessità e fornisca lo spazio aggiuntivo.

Ignoro se e come sia possibile investire in [Bellroy](http://bellroy.com); mi sa che i che i fabbricanti austrialiani di portafogli a spessore ottimizzato hanno colto una tendenza importante.