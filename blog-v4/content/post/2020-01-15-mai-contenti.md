---
title: "Mai contenti"
date: 2020-01-15
comments: true
tags: [Arment, Low, Mac, Power, Mode]
---
Così funziona il mondo Mac: dopo anni di critiche ai MacBook Pro che non sono performanti come dovrebbero e sbagliano immancabilmente la scheda grafica, Apple tira fuori [MacBook Pro 16”](https://www.apple.com/it/macbook-pro-16/).

È da quel momento si inizia a chiedere un [Low Power Mode](https://marco.org/2020/01/13/macos-low-power-mode-redux) per farlo andare più piano. Anche spegnendo la scheda grafica, se necessario.

(Che mi trova pure d’accordo, eh. Solo che colgo la dolce ironia della situazione).
