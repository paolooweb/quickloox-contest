---
title: "Scrivere come se fosse il 1979"
date: 2019-12-07
comments: true
tags: [Martin, Clarke, Wordstar, Word]
---
Devo dire ancora qualche cosa sul tema degli [strumenti da usare a scuola primaria](http://www.macintelligence.org/blog/2019/11/29/tre-cose-da-imparare/) per introdurre i bambini al *coding* e al mondo del lavoro.

Piccola autopubblicità: ho scritto anni fa su *Apogeonline* di come George R. R. Martin [scriva la saga delle *Cronache del ghiaccio e del fuoco*, da cui arriva il *Trono di Spade*, su WordStar](https://www.apogeonline.com/articoli/non-basta-la-parola-lucio-bragagnolo/), un programma che compie giusti giusti quarant’anni, lo stesso che ha usato Arthur C. Clarke per scrivere la sceneggiatura del film *2010: Odissea due*.

Mosca bianca? In un altro articolo di due anni fa ho segnalato un autore [vincitore di premi Hugo e Nebula](https://www.apogeonline.com/articoli/lodore-della-tastiera-lucio-bragagnolo/), fedele a WordStar.

Gente fuori dalla realtà? Dipende: nello stesso articolo, scrivo:

>A quanti trovassero anacronistico il tema: interi settori professionali, se non lo richiedono, almeno suggeriscono una forte identificazione dell’atto di scrivere con un programma complesso per farlo (no, Word è bizantino e obeso; complesso è un’altra cosa). Leggere per credere Kieran Healy e la sua [The Plain Person’s Guide to Plain Text Social Science](http://plain-text.co).

Certamente ci sono scrittori, scienziati, giornalisti, ricercatori, studenti che, prima di conoscere i segreti del Ribbon di Word, sanno come si scrive un racconto, come si costruisce un articolo, come si struttura un testo e tante altre cose.

A scuola, insegnerei questo, più che Word. Anche usando WordStar, se mancassero i soldi per la carta igienica. WordStar funziona anche sul computer più scalcinato che si possa trovare al mercato delle pulci. Ci sono infinite scelte ugualmente potenti e persino più economiche, eh.