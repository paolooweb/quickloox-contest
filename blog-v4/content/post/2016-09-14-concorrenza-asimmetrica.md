---
title: "Concorrenza asimmetrica"
date: 2016-09-14
comments: true
tags: [iPhone, Android, Stefano, LG, Htc]
---
Devo a **Stefano** la lettura di un articolo che dovrebbe illuminare le menti di quanti, ne parlavo di recente, confrontano ancora l’hardware a forza di numeri come se farlo avesse un senso: [competere con le specifiche di iPhone è più difficile di quanto sembri](http://www.theverge.com/2016/9/12/12886058/iphone-7-specs-competition). Solo qualche estratto.<!--more-->

>iPhone è dietro la curva di Android su quasi ogni specifica, eppure resta lo smartphone più venduto anno dopo anno. Non è perché le specifiche non contino, ma perché la stessa specifica significa cose diverse negli ecosistemi iOS e Android.

>Per competere con l’autonomia di iPhone – effetto delle specifiche – i costruttori Android devono costruire batterie sistematicamente più grandi per compensare le inefficienze. […] Le batterie sono elementi pesanti e densi che richiedono spazio, per cui progettare uno smartphone Android con l’autonomia di un iPhone e ugualmente sottile e leggero richiede in realtà non di pareggiare l’ingegneria di Apple, ma di superarla.

>La questione della risoluzione è un grande esempio di come sapere quando una specifica cessa di essere importante. Samsung, LG, Htc e altri hanno raggiunto risoluzioni più eclatanti di quella di iPhone, ma nessuno di questi schermi Quad HD si guarda molto meglio di quello di iPhone. Apple raggiunge abbastanza risoluzione […] da superare la soglia dello schermo Retina e poi si ferma. Questa frugalità con i pixel porta a una maggiore autonomia.

>Apple è ossessiva nel terminare i programmi in background su iOS, mentre Google concede più libertà su Android […]. Il risultato è che un iPhone può essere avvertito come superfluido e scattante con metà della Ram che serve su Android. La Ram consuma energia e averne meno è un altro fattore che porta iPhone ad avere una migliore efficienza.

Ops. I computer da tasca non sono tutti uguali.
