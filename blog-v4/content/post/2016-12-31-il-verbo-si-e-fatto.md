---
title: "Il verbo si è fatto"
date: 2016-12-31
comments: true
tags: [Wolfe, Mac, Safari, Darwin, Chomsky]
---
So di chiudere l’anno in modo evanescente e me ne scuso. Di fatto passo praticamente la totalità del tempo di questi giorni lontano dall’attualità e dai flussi di notizie.<!--more-->

Ma almeno gli auguri. Mi hanno regalato il libro [The Kingdom of Speech](https://www.amazon.it/gp/product/0316404624?ie=UTF8&tag=althouse09-20&camp=1789&linkCode=xm2&creativeASIN=0316404624) di Tom Wolfe: un testo agile e scritto magistralmente che prende una posizione ardita su linguaggio ed evoluzionismo.

Sono lontanissimo dal poter dare un giudizio di merito ma accidenti, mi auguro di riuscire un giorno a scrivere con un centesimo di quell’efficacia e quell’energia.

A tutti: nella seconda pagina del libro si può leggere la frase

>so I surfed and Safaried

*per cui ho navigato e Safariato*. Il che significa varie cose. La meno rilevante è che un grande scrittore contemporaneo usa, con altissima probabilità, un Mac (e non ha paura di trasformare il *browser* in forma verbale). Più interessante è l’invito sottinteso a navigare, muoversi, scoprire. Il libro nasce dalla scoperta accidentale di un *paper* scientifico da parte di un *dandy* in età avanzata che avrebbe altrimenti tutte le possibilità, comprese quelle economiche, di divertirsi oziando oppure oziare divertendosi.

Tanti auguri di un 2017 di scoperte, sorprese, progressi. Se attraverso Safari, ancora meglio.

E grazie per essere passati di qui anche l’ultimo giorno dell’anno.
