---
title: "App per dimagrire"
date: 2017-07-05
comments: true
tags: [Halide]
---
Apple [ha intrapreso azioni](https://macintelligence.org/posts/2017-06-25-il-razzismo-supremo/) per arrivare a un Apple Store di qualità dal punto di vista editoriale e popolato il più possibile da *app* sane.

Quello che non può fare è chiedere a tutti gli sviluppatori di seguire l’[esempio di Halide](https://blog.halide.cam/one-weird-trick-to-lose-size-c0a4013de331) e usare ingegno e capacità per arrivare a contenere la dimensione delle *app*.

Compressione delle immagini, rinuncia al codice superfluo e alle librerie ridondanti, cura del processo di *build* via Xcode: questo e altro tagliano drasticamente – se si è bravi – l’ingombro delle *app*.

Significa meno dati da fare correre in rete, magari con connessione cellulare; vuol dire aggiornamenti più veloci e snelli; permette di vivere meglio anche con un iPhone da sedici gigabyte di spazio disco.

Tutte cose che contribuiscono a un ecosistema migliore e dovremmo perseguire anche noi nel nostro piccolo, scrivendo agli sviluppatori e preferendo quando possibile le *app* più leggere.

Sono passati i tempi in cui si poteva avere [il gioco degli scacchi in un chilobyte](https://news.ycombinator.com/item?id=9151552); nel contempo, il fatto che uno sviluppatore possa trascurare una sana economia di spazio e risorse indica la necessità di lavorare per aumentare la consapevolezza e invertire la tendenza.
