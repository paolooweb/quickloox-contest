---
title: "Di passaggio"
date: 2019-04-21
comments: true
tags: [Pasqua]
---
Di passagggio in tanti modi. Torno dalla Cina, vado verso il lago, attraverso esperienze e condizioni inaspettate.

Tutto magari faticoso e però positivo. Non che mi debba vantare o lamentarmi; ho una vita interessante e fortunata, gran parte della quale è merito di altri.

Altri che includono i frequentatori di questa paginetta. Per questo si meritano i miei più sinceri auguri. Di passaggio, dovrebbero essersi chiuse numerose parentesi che complicavano l’aggiornamento del blog. Mi auguro, in più, che sia vero.

Buona Pasqua!
