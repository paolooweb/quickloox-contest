---
title: "Per molti ma non per tutti"
date: 2016-11-04
comments: true
tags: [Schiller, Rospach, Gruber, Bjarnason, MacBook, Pro, Independent, gamut, nit]
---
Phil Schiller in un’[intervista all’Independent](http://www.independent.co.uk/life-style/gadgets-and-tech/features/apple-macbook-pro-new-philip-schiller-interview-phil-iphone-ios-criticism-a7393156.html):

>Sappiamo di avere preso buone decisioni su che cosa inserire nel nuovo MacBook Pro e il risultato è il miglior portatile mai costruito, ma potrebbe non essere subito la cosa giusta per tutti. Va bene così, qualcuno si sentiva così nei riguardi del primo iMac ed è finita piuttosto bene.

John Gruber [in risposta](http://daringfireball.net/linked/2016/11/01/bjarnason-macbook-pros) all’[articolo di Baldur Bjarnason](https://www.baldurbjarnason.com/notes/the-less-pro-apple/) che afferma *Apple è veramente, veramente disinteressata ai suoi utenti professionali di Mac*:

>Non sono d’accordo. Chi ha lo schermo come priorità trova più luminosità (500 nit) e un gamut wide color. I nuovi Ssd su MacBook Pro offrono prestazioni dominanti in lettura e scrittura, [anni davanti alla concorrenza](http://www.computerworld.com/article/3136714/data-storage/apples-new-macbook-pro-may-be-the-worlds-fastest-stock-laptop.html). Il modello da 15 pollici offre un i7 quad-core con frequenze fino a 3,8 gigahertz. Per molte situazioni impegnative, queste sono macchine professionali. Non per tutte le professioni.

>Un titolo più accurato, meno allusivo dell’articolo di Biarnason sarebbe “I contro di dipendere da una azienda con una linea di prodotti relativamente ristretta quando i propri bisogni personali sono di una ristretta minoranza”.

Conclusione [presa da Chuq Von Rospach](https://chuqui.com/2016/10/how-apple-could-have-avoided-much-of-the-controversy/):

>Molto si riduce a questo concetto: chiediamo ad Apple di innovare, ma insistiamo sul fatto che non cambi niente.

>So che quando compro una nuova auto mi tocca invariabilmente acquistare degli accessori perché quelli vecchi non vanno con il nuovo modello. Se sto spendendo venticinquemila dollari per un’auto, spenderne altri cento in catene da neve non è un grosso problema. Ma occhio ai nerd che si lamentano di una cosa da diciannove dollari per personalizzare il proprio computer da duemilacinquecento. La mia risposta in sintesi: tempo di acquisire un minimo di prospettiva, gente.

>Mentre scrivevo questo pezzo ho pensato e parlato molto di nicchie e, più ci penso, più vedo questo scontento come una battaglia per stare fuori dalla nicchia: le persone vogliono essere parte del flusso principale dell’attività di Apple, e vedo la rabbia maggiore nei gruppi di persone che arrivano a comprendere di non esserlo più.

>Lamentarsi non annulla le forze di mercato dietro a queste tendenze. Apple reagisce ai cambiamenti, non li impone.

Tre letture indispensabili per capire.
