---
title: "Un curricolo per l’estate"
date: 2021-08-08T23:25:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [scuola, Lidia, Office, Runescape, iPad] 
---
Nelle prossime due settimane credo che darò spazio a un’urgenza personale. Settembre è infatti vicino. Settembre 2022.

Da qui a un anno la figlia più grande inizierà il triennio che completa il primo ciclo di scuola primaria. E, se nessuno fa qualcosa, comincerà ad affrontare l’informatica a scuola; le insegneranno… a usare Office.

Devo fare qualcosa anche a costo di buttare via del tempo e voglio abbozzare un curricolo di studi tecnologici alternativo, che si basi sugli standard e sugli skill prima che sulle applicazioni, sia trasversalmente applicabile a tutte le materie e che insegni ai ragazzi qualcosa di interessante, utile e accessibile per la loro età.

L’obiettivo è passare agli insegnanti una proposta che, se fossimo in un mondo ideale, avrebbero un anno di tempo per valutare ed eventualmente recepire, anche solo in minima parte.

Il mondo ideale non è questo è la fine più probabile di uno sforzo come questo sarà il dimenticatoio. Devo essere però capace di cogliere la ricompensa insita nel viaggio (*the journey is the reward*) ed essere consapevole che a mia figlia daranno in pasto Office ma io potrei comunque uscirne con tante idee più chiare e tante risorse più visibili.

Si intende che qualsiasi *feedback*, in qualunque forma, sarà oltremodo gradito. Se invece il progetto finisse nel nulla, magari tra due giorni ci si troverà a parlare di [Runescape](https://www.runescape.com/) ora che è vivo su iPad, chi lo sa. Ho sempre preferito la serendipità alla programmazione ossessiva.

*Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile.*