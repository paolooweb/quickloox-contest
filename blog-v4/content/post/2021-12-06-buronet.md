---
title: "BuroNet"
date: 2021-12-06T00:01:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [eBay, Urania] 
---
Una delle promesse fondamentali della rete è la possibilità di rendere le operazioni quotidiane più lineari, semplici, leggere; evitare o minimizzare la burocrazia, portare razionalità e precisione in tante procedure inutilmente arzigogolate.

Qualcosa è andato storto però. Ultimamente ho ripreso in mano diversi account vecchi su altrettanti servizi trascurati e più spesso che no bisogna affrontare piccole odissee prima di ritornare operativi.

L’episodio più irritante riguarda eBay, dove mi sono registrato molti anni fa per provare come funzionasse. Giusto per provare, misi in vendita un Urania che avevo doppio; se lo aggiudicò qualcuno a due euro. Ci rimisi perché ingenuamente non avevo contemplato nell’entusiasmo del collaudo le spese di spedizione, ma ero soddisfatto; l’esperimento aveva funzionato.

Sono tornato su eBay e il mio account non rispondeva più. Il perché l’ho scoperto dopo, quando ho effettuato un accesso attraverso il sign-on Apple e ho provato a conciliare le credenziali usate con il vecchio account, o qualcosa del genere; la procedura non è stata affatto chiara e faccio fatica a ricostruire perfettamente il percorso seguito.

Di fatto, durante questi anni eBay *mi ha modificato il nome utente* e si spiega perché non riuscivo a entrare. Il nome utente attuale ora finisce con un numero, una cosa che detesto e che ho sempre evitato accuratamente, senza eccezioni, in tutta la mia vita (motivo per cui posso affermare che mi hanno modificato il nome utente e spernacchiare chi pensa a una deriva senile di disattenzione).

Comunque sono entrato e ho ripetuto la prova: stavolta ho messo in vendita un cofanetto di Dvd che mi hanno regalato per Natale svariati anni fa, ancora una volta doppione di analogo cofanetto già in mio possesso.

Dopo poche ore, eBay mi ha informato di avere bloccato l’account per attività sospetta (la mia). Ora vogliono un documento di identità che mostri nome e indirizzo e, in più, la prova di proprietà di ciò che ho messo in vendita.

Potrei forse produrre, posto di averlo conservato, un biglietto di auguri, visto che era un regalo, e null’altro.

Dietro questa piccola vicenda di nessun conto (era una prova) c’è una cascata di fallimenti a più livelli. Conservazione dei dati, supporto al cliente, interfaccia utente, design del linguaggio, architettura dell’informazione e non so quant’altro; logica e buonsenso vogliono che io possa utilizzare il mio account nel modo previsto dal sito anche se sono passati anni e, se questo non è possibile per qualsiasi ragione, essere informato in maniera chiara ed evidente, nonché disporre di una strada ben progettata e scorrevole per risolvere il problema.

Niente di tutto questo. Intanto, uno dei siti più importanti della rete è diventato una sorta di ministero delle aste dei privati, con le sue regole senza senso, i suoi malfunzionamenti, la sua burocrazia cieca e stolida, le perdite di tempo; tutto uguale agli uffici complicazioni affari semplici della pubblica amministrazione che, anzi, a volte qualche progresso riesce a mostrarlo.

Ora ho mandato la carta di identità e vedo che succede. Nel frattempo, mi chiedo che cosa sto a fare dietro a quel sito. Per mandare documenti, armeggiare con il login, ubbidire a richieste davvero improbabili, il prossimo Dvd lo porto al mercatino dell’antiquariato del martedì, dove non ho bisogno di identificarmi né verrò trattato come un mariuolo.

_Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile._