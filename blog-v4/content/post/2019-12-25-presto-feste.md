---
title: "Presto, feste"
date: 2019-12-25
comments: true
tags: [Natale, Polytopia]
---
Domani la vita ricomincia, ma oggi davvero è festa.

Da poco è passata la mezzanotte che ho già ricevuto un dono incredibile e inaspettato e mancano poche ore a un raduno di famiglia dove, a un certo della vita non è scontato, ci sono tutti tranne chi sta vivendo una esperienza unica molto lontano.

Per quello che è l’oggi posso solo ringraziare qualcuno, qualcosa, nessuno, non importa: importa ringraziare.

E così spero di te. Che possa essere un Natale di gioia e leggerezza.

(Una battaglietta a [Polytopia](http://midjiwan.com/polytopia.html), comunque, mettila su).

Buona festa!