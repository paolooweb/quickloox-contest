---
title: "Direi che dura"
date: 2015-08-21
comments: true
tags: [watch, iPhone]
---
Oggi primo test vagamente significativo per la batteria di watch, con tre ore di macchina e itinerario affidato alle Mappe.<!--more-->

Quando watch e iPhone si parlano, quest’ultimo resta in tasca e l’orologio pubblica sullo schermo le informazioni di direzione, nel contempo avvisando con tocchi sul polso e segnali acustici quando è ora di girare a destra oppure a sinistra.

Partenza mattutina con orologio pieno, messo in carica la notte con il 67 percento di batteria ancora disponibile.

L’apparecchio ha sopportato con successo anche un paio di assalti della figlia, che ha visto lo schermo luminoso e ha voluto toccare tutto. È arrivata a chiamare Siri (pressione della corona digitale) e a cancellare una manciata di notifiche, oltre a entrare senza conseguenze nella personalizzazione del quadrante orologio (per il momento ho adottato [Astronomia](http://www.apple.com/it/watch/timekeeping/) con la visualizzazione delle posizioni relative dei pianeti).

Un po’ scontato dire [come volevasi dimostrare](https://macintelligence.org/posts/2015-03-12-lassillo-dellora/), però è agosto e ci sta.