---
title: "Nelle pieghe della computer science"
date: 2024-02-11T02:40:18+01:00
draft: false
toc: false
comments: true
categories: [Internet]
tags: [Quanta, origami, Quanta Magazine, Turing complete]
---
A scuola lamentano che non ci sono computer per tutti, per insegnare Office in terza elementare. Proprio a scuola che dovrebbe essere un bastione del sapere, si dovrebbe avere presente che lo scorso anno [è stata dimostrata la completezza Turing dell’origami](https://www.quantamagazine.org/how-to-build-an-origami-computer-20240130/).

Come del resto vale anche per [Life di John Conway](https://macintelligence.org/posts/2024-01-30-un-nome-due-artisti/). Però un computer bisogna averlo.

Invece, qualsiasi problema computabile può essere computato piegando un foglio di carta. È ovvio che a un certo punto tutto diventi teoria, ma nell’articolo linkato e poi nel [paper](https://arxiv.org/pdf/2309.07932v1.pdf) vengono mostrate la logica di fondo e le istruzioni per produrre concretamente e in modo semplice i cancelli logici alla base del funzionamento dei transistor e della logica booleana.

Ci sarà a disposizione qualche risma di carta per spiegare come funzionano i computer? Più difficile ancora, ci sarà qualche docente disposto a leggersi un Pdf di lunghezza modesta per imparare come funziona?