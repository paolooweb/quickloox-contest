---
title: "Obsoleto chi?"
date: 2015-07-16
comments: true
tags: [Apple, iPhone, iOS9, iOS, Slicing, Bitcode]
---
Prima o poi capita di trovare qualcuno convinto che Apple pratichi [obsolescenza programmata](https://macintelligence.org/posts/2013-09-10-dieci-trucchetti-che-non-ho-capito/) e ricorra ad accorgimenti ai limiti del lecito per spingere al cambiamento sempre più frequente, possibilmente compulsivo, dell’hardware.<!--more-->

Prendiamo per esempio iOS 9. Apple lavora a un complesso di funzioni noto come [App Thinning](https://developer.apple.com/library/prerelease/watchos/documentation/IDEs/Conceptual/AppDistributionGuide/AppThinning/AppThinning.html), snellimento delle *app*.

Una di queste funzioni è Slicing, l’affettato. In soldoni, quando scarico la *app* da App Store mi arrivano solo le parti della *app* necessarie al funzionamento sul mio sistema. Così l’ingombro della *app* è minore e tutto funziona comunque.

Un’altra è Bitcode. In breve, invece che mandare ad App Store il file eseguibile della *app* che voglio vendere, mando una sua rappresentazione in *bitcode*, un formato intermedio. La compilazione la esegue al volo App Store quando qualcuno vuole la *app* e viene eseguita secondo le specifiche più recenti e aggiornate; in questo modo la *app* non ha bisogno di essere periodicamente aggiornata con il cambiare delle architetture e delle specifiche, ma solo per inserire nuove funzioni o miglioramenti.

Poi ci sono le On-Demand Resources. Supponiamo di avere scritto un gioco con millemila livelli e un sacco di grafica. Possiamo mettere la grafica relativa a ciascun livello nelle On-Demand Resources. Così chi scarica la *app* si troverà su disco solo i primi livelli (poniamo). Se non giocherà mai più a quel gioco, non avrà su disco i millemila livelli e la grafica relativa. Se invece si appassiona e va avanti, il programma scaricherà la grafica man mano che si avvicina il momento in cui servirà. Anche questo secondo i criteri dello Slicing: su un iPhone gli sfondi di gioco creati per iPad non verranno caricati. Questo consente di avere *app* in partenza ancora più piccole.

Ecco. Leggendo, mi convinco che sono funzioni mirate a migliorare la mia esperienza di uso di iPhone 5 da sedici gigabyte, dove avere *app* più piccole, con lo spazio ridotto all’osso, è vitale. iOS 9 aumenta l’aspettativa di vita del mio iPhone 5, quasi tre generazioni indietro all’attualità.

Però Apple pratica l’obsolescenza programmata. Perché sia vero basta raccontarselo, o servono anche riscontri fattuali?