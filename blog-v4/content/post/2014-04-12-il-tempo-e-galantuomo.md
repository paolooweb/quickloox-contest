---
title: "Il tempo è galantuomo"
date: 2014-04-13
comments: true
tags: [Samsung, iPad, Wsj, VentureBeat, HuffingtonPost, Computerworld, Galaxy]
---
Per via di una causa in corso tra Apple e Samsung su questioni di brevetti, [sappiamo](http://appleinsider.com/articles/14/04/11/exclusive-apple-vs-samsung-docs-reveal-galaxy-tab-was-a-flop-and-samsung-knew-it) che nel 2011 Samsung ha venduto negli Stati Uniti un milione di tavolette Galaxy Tab. Un milione in un anno. iPad, 17,4 milioni.<!--more-->

Al tempo, una dichiarazione fornita da un dirigente Samsung lasciava pensare a due milioni in meno di un mese e mezzo. In verità un errore di trascrizione audio trasformò [sul Wall Street Journal](http://blogs.wsj.com/digits/2011/01/31/samsung-galaxy-tab-sales-actually-quite-small/) vendite *small* (piccole) in *smooth* (tirate a lucido).

*VentureBeat* titolò [Android ruba quote di mercato tablet a iPad di Apple](http://venturebeat.com/2011/01/31/android-tablet-marke-share/).

*Huffington Post* scrisse [Le vendite di tavolette Android riducono il divario con iPad](http://www.huffingtonpost.com/2011/01/31/android-tablets-vs-ipad_n_816219.html).

*Computerworld*: [Le vendite di tavolette Android decollano, un chiaro segno che domineranno su iPad](http://blogs.computerworld.com/17736/android_tablets_sales_skyrocket_a_clear_sign_that_theyll_dominate_the_ipad).

L’errore può sempre capitare. Lo scandalo è che *un* errore può sempre capitare, ma nessuno si è preoccupato di verificare. La verità è che a molti faceva gran piacere poter scrivere quei titoli gaglioffi. Falsi, come il tempo ha mostrato.
