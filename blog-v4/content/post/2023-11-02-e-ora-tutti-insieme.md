---
title: "E ora tutti insieme"
date: 2023-11-02T23:40:48+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [SubEthaEdit, briand06, Maggi, Sabino Maggi]
---
I grandi fanno grandi cose con strumenti piccoli, meglio ancora se dimenticati o rispolverati.

Per averne dimostrazione è sufficiente leggere il [resoconto di *briand06* sull’uso di SubEthaEdit](https://melabit.wordpress.com/2023/10/30/subethaedit-scrivere-insieme-in-tempo-reale/), editor di un tempo lontano oggi nel dimenticatoio, non particolarmente sviluppato a livello di nuove funzioni ma con una particolarità che lo ha contraddistinto da sempre: scrivere collaborativamente nello stesso momento sullo stesso documento.

Uno dice *ma oramai ci si riesce persino con Pages* ed è vero; con Google Documenti, addirittura, funziona davvero. La wiki di emacs [descrive come fare con l’editor migliore di tutti](https://www.emacswiki.org/emacs/CollaborativeEditing).

Ma vuoi mettere, farlo con un Mac, con una applicazione scritta nello spirito di Mac? E ottenere i risultati. Questo è il talento.