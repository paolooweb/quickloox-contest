---
title: "Causa bagordi"
date: 2015-12-26
comments: true
tags: [Go, Catan]
---
E un numero esagerato di regali di interesse che richiedono attenzione, mi prendo la libertà di rinnovare a tutti gli auguri e poco più. Devo (ri)studiare tra le altre cose [Coloni di Catan](https://itunes.apple.com/it/app/catan-hd/id390422167?l=en&mt=8) e [Go](https://it.wikipedia.org/wiki/Go_(gioco)).