---
title: "Chiedimi perché amo la tecnologia"
date: 2019-04-09
comments: true
tags: [Pixelmator, Photo, BBEdit, Helvetica, Monotype]
---
Certo, ci sono momenti dove la tecnologia è noiosa, non funziona, delude.

Poi ci sono giornate come questa.

Esce [Pixelmator Photo](https://www.pixelmator.com/photo/) che offre editing fotografico spettacolare su iPad, confortato dall’apprendimento meccanizzato. Il programma sfrutta Core ML e le capacità di rete neurale presenti nei nuovi iPad per rendere ancora più efficace ogni operazione di modifica. Il prezzo, di fronte alle prestazioni, fa ridere.

[Ridisegnano Helvetica dopo trentacinque anni](https://www.creativeboom.com/resources/monotype-launches-the-first-redesign-in-35-years-of-the-worlds-most-ubiquitous-font-helvetica/). Il font da cui è partita l’impaginazione digitale era anche diventato il più noioso e abusato al mondo, clonato in modo triste per non pagare royalty da una nota multinazionale esperta in copiacce di roba funzionante. Improvvisamente ridiventa interessante. Non avevano alcun motivo pressante per farlo, ma lo hanno fatto ugualmente. Il clone brutto diventa un brutto pensiero più lontano.

[Mi aggiornano BBEdit](https://www.barebones.com/support/bbedit/current_notes.html). Ma sì, succede miliardi di volte, non c’è niente di nuovo, tutto collaudato. Comunque mi ritrovo l’editor di testo più potente al mondo, o nei paraggi, che pesa la sciocchezza di 13,4 megabyte e si aggiorna nel giro di una manciata di secondi.

La tecnologia, almeno certi giorni, è bellissima.