---
title: "Ancora carta"
date: 2018-01-03
comments: true
tags: [Nive, Lidia]
---
Ho avuto la grande fortuna di entrare per seconda volta in una sala parto come migliore attore non protagonista.<!--more-->

Tre anni dopo, l’ospedale è stato ricostruito accanto a quello vecchio: materiali moderni, apparecchiature di avanguardia, architettura razionale, i primi robot a spasso tra le corsie.

La sala parto ariosa, attrezzata, efficiente. Nei giorni di sole la vista deve essere splendida.

Il personale straordinario per umanità e preparazione. Mai, dopo la prima volta, si è pensato a un luogo diverso per la seconda.

Poi arriva lo Stato, quello dei burocrati, e ti ritrovi con i modulicchi da scrivere a penna su carta quasi igienica, per replicare infinite volte i nomi e cognomi e residenze e codicifiscali, e le firme, e le liberatorie. Una parentesi fine Ottocento dentro una struttura del 2015.

Spero che mia figlia potrà un giorno entrare in sala parto potendo vivere il suo tempo invece che quello di mummie morte viventi chiuse dentro grattacieli di faldoni. Abbiamo le reti, le scansioni di Dna, la cifratura, gli algoritmi a conoscenza zero: è ora di usarle.

Ho fatto sapere di mia figlia dall’altra parte del mondo in un nanosecondo, con foto; per farlo sapere all’anagrafe devo compilare il modulino, guai a sbagliare la virgola nel tripudio dello stampatello nero bluastro. E poi uno si chiede perché siamo indietro.
