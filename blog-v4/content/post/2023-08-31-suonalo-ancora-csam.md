---
title: "Suonalo ancora, Csam"
date: 2023-08-31T11:43:56+01:00
draft: false
toc: false
comments: true
categories: [Internet, Software]
tags: [Apple, Csam, Heat, Ars Technica, Wired, Communication Safety]
---
Alla fine Apple ha definitivamente abbandonato i piani di scandire gli apparecchi dei singoli a caccia di immagini potenzialmente pedopornografiche, a favore di una strategia detta [Communication Safety](https://support.apple.com/en-us/HT212850) che munisce gradualmente tutti i suoi sistemi di comunicazione di metodi per riconoscere nudità nei contenuti e limitarne preventivamente la diffusione.

Ora però una organizzazione per la lotta contro la pedopornografia ha chiesto ad Apple di sviluppare un sistema per andare a caccia di foto compromettenti su iCloud e la società ha (insolitamente) [risposto in pubblico](https://arstechnica.com/?p=1964978) con il dettaglio dei motivi per cui preferisce la propria strategia. In sostanza, sicurezza e privacy; aprire iCloud a controlli automatici di massa del materiale crea un precedente pericoloso e consente a gente non invitata di provare a mettere le mani nel sistema, con conseguenze potenziali preoccupanti.

Quando Apple ha annunciato il sistema per il riconoscimento di massa delle foto contenute nel database Csam, ho spiegato perché lo facesse; c’era da rispondere alle pressioni delle autorità che vorrebbero avere mano libera sui contenuti in cloud, lasciando aperta la porta alla possibilità di cifrare end-to-end iCloud in modo totale. Cosa questa che Apple vorrebbe fare senza troppe opzioni e configurazioni in mezzo, ma non può, precisamente per non mettersi totalmente contro i governi di mezzo mondo e non solo quelli autoritari. [Come dovrebbe fare secondo certi attivisti](https://macintelligence.org/posts/2021-08-28-privacy-e-realtà-un-attacco-omologato-e-superficiale/), di cui l’idealismo resta un pregio e la distanza dal mondo reale un bel difetto.

Queste tematiche sono trasversali e universali, quindi rimangono attuali; la libertà del singolo contro la volontà di controllare contro il problema dei reati contro la necessità di limitare la libertà di chi li commette, per poterli perseguire efficacemente.

Non c’è una soluzione che vada bene per tutto e per tutti e Communication Safety ha inevitabilmente i propri pregi e difetti; va tenuto presente quando ci radicalizziamo sulla nostra idea, semplice, perfetta e originale per risolvere problemi come questo in mondo di miliardi di persone interconnesse, invece di capire che le soluzioni reali a grandi problemi sono sempre imperfette. Un po’ come noi. 

*Gli aggiornamenti in agosto del blog potrebbero essere irregolari o infrequenti.*