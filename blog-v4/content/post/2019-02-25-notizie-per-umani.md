---
title: "Notizie per umani"
date: 2019-02-25
comments: true
tags: [Apple, News, Digiday]
---
Stando a un articolo di *Digiday UK*, Apple News [non funziona bene](https://digiday.com/media/hard-to-back-out-publishers-remain-frustrated-by-apple-news-monetization/) per gli editori che speravano in ricavi interessanti.

Conforta sapere che Apple News ha risultati deludenti per una serie di ottime ragioni. Prima di tutto agli inserzionisti è vietato l’uso di dati o indirizzi IP acquisiti da terze parti; in altre parole, la privacy del lettore è tutelata. Secondo, è vietata anche la pubblicità programmatica, controllata da automatismi e algoritmi. A proporre un’inserzione non è un software, ma un umano che desidera comunicare verso altri umani.

Non tutto il male viene per nuocere. Alcuni inserzionisti riferiscono di avere ottenuto risultati accettabili veicolando abbonamenti attraverso Apple News. Altri puntano a promuovere podcast o altri mezzi per monetizzare indirettamente quello che non riescono a monetizzare direttamente.

Mi piace essere ottimista e pensare che, come ha fatto l’anno scorso nonostante i cattivi risultati, l’*audience* di Apple News continui a crescere a ottimo ritmo e arrivi nel tempo a diventare anche redditizia per gli inserzionisti, restando piacevole e leggibile per i lettori.

Notizie per umani, senza occhi indiscreti a carpire dati non concessi in forma non autorizzata, accompagnate da pubblicità di umani per umani. In questo momento storico non esiste alcuna altra azienda di tecnologia disposta a promettere questo (come, del resto, ad [abbandonare uno standard per la privacy rivelatosi disfunzionale](https://spreadprivacy.com/do-not-track/) come Do Not Track a favore di tecnologia sviluppata *ad hoc*, oppure a [offrire l’uso della propria tecnologia di mappe](https://macintelligence.org/posts/2019-01-16-nessuno-lo-sapra/) all’unico browser rispettoso della confidenzialità verso chi naviga).