---
title: "Con discrezione"
date: 2024-02-21T02:43:51+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [Mail]
---
Sono noto per essere l’ultimo a sapere le cose più evidenti del sistema, sempre alla ricerca di quelle più esoteriche. Così sono rimasto sorpreso nel comporre una email in inglese, pochi minuti fa, e vedere Mail che mi proponeva il completamento delle frasi in perfetto stile *machine learning applicato alla user experience, che chi deve vendere spaccia per intelligenza artificiale*.

Sorpreso di una buona sorpresa. L’autocompletamento è comparso in grigio, visibilmente diverso dal testo che stavo scrivendo e nel contempo assolutamente non intrusivo. Guarda caso, avevo un mezzo dubbio su una frase idiomatica con cui concludere un periodo, e il programma lo ha chiarito. A me è bastato un colpo di tabulatore.

Fosse sempre così. Invece che rompere le scatole, anticipare l’impossibile, insistere, infastidire, presenziare, il sistema mi ha risolto un piccolo problema in modo lineare, facile, comprensibile, efficace e discreto. Mentre scrivevo di getto, non ho visto alcun suggerimento comparire (potrei avere scritto in inglese perfetto fino a quel punto, niente è impossibile, ma è molto improbabile).

La notte sarà lunga e chiudo qui il collegamento dalla scrivania, con una nota di buonumore e morale alto. Tanti anni e sono ancora sulla piattaforma hardware e software che lavora per me, invece del contrario. Una conferma estemporanea e minuscola fa molto bene nel ricordarmelo e allietare la nottata.