---
title: "C’è avventura e avventura"
date: 2018-09-25
comments: true
tags: [Myst, FastCompany]
---
Ho già versato a suo tempo la quota di nostalgia richiesta per poter [parlare di Myst](https://macintelligence.org/posts/2015-01-11-ventanni-dopo/), che stando a *FastCompany** [ha compiuto venticinque anni](https://www.fastcompany.com/90240345/myst-at-25-how-it-changed-gaming-created-addicts-and-made-enemies) ahimé ieri.

Forse per necessità di trovare a tutti i costi qualche cosa di nuovo su un argomento già coperto, l’articolo si avventura su una strada bizzarra cercando di montare una polemica secondo me risibile su come il gioco di Cyan abbia creato problemi all’allora fiorente mercato delle avventure testuali.

Nel mio piccolo, ricordo niente del genere. Semmai, quel mondo statico ed enigmatico faceva venire voglia di avventura. Erano altri tempi, bastava avere dell’immaginazione al momento di leggere una breve descrizione.

In ogni caso l’articolo si riscatta prima della fine e vale la lettura. Anche *Myst* vale la pena di una piccola riscoperta, certo nei modi moderni che ho linkato sopra (credo che le macchine in grado di fare girare il suo Cd-Rom si contino sulle dita di due mani, che non metterei sul fuoco per scommettere che il Cd-Rom funzionerebbe).

Devo dire che se avessi tempo e voglia di calarmi in una vertigine, un quarto di secolo dopo *Myst*, la sceglierei testuale come un [Mud](https://www.mudconnect.com/index.html). I moderni mondi virtuali sono talmente estesi e popolati che ci si fanno dentro un sacco di cose interessantissime e divertenti. Ma quella sensazione di avventura data dal silenzio e dalla luccicanza del paesaggio, no, quella è rimasta dentro la creazione di Atrus.