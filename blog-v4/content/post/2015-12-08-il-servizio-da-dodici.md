---
title: "Il servizio da dodici"
date: 2015-12-08
comments: true
tags: [NetHack, iNetHack2, Discworld, Pratchett, WoW, Linux, Voice, iPad, Ssh, Prompt, roguelike, Terminale]
---
Scenario: fuori è freddo e buio. Gli altri dormono, ciondolano a tavola dopo un pranzo troppo lungo, oppure guardano intontiti uno di quei film da vacanza, replicati ogni anno da vent’anni su tutti i canali. A guardare bene si potrebbe anche intravedere un fiocco di neve transitare davanti alla finestra.<!--more-->

Quest’anno accenderò [NetHack](http://www.nethack.org/index.html), che è stato [aggiornato dopo dodici anni di attesa](http://www.nethack.org/v360/release.html) giungendo alla [versione 3.6.0](http://www.nethack.org/common/index.html).

L’aggiornamento è in gran parte sotto il cofano, per eliminare le parti più polverose del codice e preparare il terreno alle migliorie future. I *fan* di Terry Pratchett potranno trovare nel gioco numerose citazioni dei suoi lavori, specialmente da *Discworld*.

NetHack è uno di quei bizzarri *roguelike* dove tutto è disegnato con caratteri alfanumerici e viene lasciato all’immaginazione. Non tutti apprezzano il genere e lo posso capire (anch’io, avessi tempo, riattiverei l’abbonamento a [World of Warcraft](http://eu.battle.net/wow/en/)). Però l’anno scorso di questi tempi si chiedevano [su Linux Voice](http://www.linuxvoice.com/nethack/) se non fosse il miglior gioco di sempre. Risposta variabile, ma domanda legittima.

Se poi per l’Epifania si è ancora contagiati, e il gioco è ben lungi dall’essere stato vinto, eh, capita. Sarà il momento dei propositi del nuovo anno, in un senso o nell’altro.

(Per iPad c’è [iNwtHack2](https://itunes.apple.com/us/app/inethack2/id962114968?mt=8), per ora fermo alla precedente versione 3.4.3. Oppure, se il Mac è raggiungibile, una qualunque connessione Ssh permetterà di giocare la versione aggiornata via Terminale. Io uso [Prompt 2](https://panic.com/prompt/)).