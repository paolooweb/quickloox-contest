---
title: "Ottusità standard"
date: 2022-05-05T00:29:19+01:00
draft: false
toc: false
comments: true
categories: [Hardware, Software]
tags: [iPhone, Gruber, John Gruber, Daring Fireball, Nfc, Apple Pay, CurrentC, Margrethe Vestager, iPhone]
---
Gli eventi della cronaca fanno scrivere a molti di una ritrovata unità di intenti e soprattutto di capacità concreta di influire sugli eventi da parte dell’Europa.

Purtroppo c’è da ricordare che l’Europa è capace anche di straordinaria ottusità e che gli esempi di quest’ultima per ora soverchiano le note positive. È in questo contesto che John Gruber scrive su *Daring Fireball* un capolavoro a partire dal titolo, [La nota esperta di design interattivo e sicurezza Margrethe Vestager riprogetta il supporto Nfc di iPhone](https://daringfireball.net/2022/05/vestager_nfc).

Il tema: le sciocchezze contenute nel documento europeo [Commenti da parte della vicepresidentessa esecutiva Margrethe Vestager alle obiezioni inviate ad Apple riguardo alle sue pratiche nei confronti di Apple Pay](https://ec.europa.eu/commission/presscorner/detail/en/speech_22_2773).

Ce n’è per tutti i gusti, sul *leit-motiv* di Apple che lavorerebbe in modo ostile alla concorrenza e la Commissione europea vogliosa di aprire la piattaforma alle terze parti, in nome del mercato.

Di mercato la Commissione ne capisce notoriamente poco, ma di tecnologia ancora peggio, così Gruber ha da una parte un gran lavoro nell’individuare tutte le stupidaggini presenti nel documento, e dall’altra buon gioco a produrre dosi equine di sarcasmo.

>Apple ha affrontato un mercato animato e perfettamente equilibrato, nel quale i pagamenti Nfc erano usati da praticamente nessuno e lo ha trasformato in un mercato dove Apple Pay viene accettata in quasi tutti i negozi con soddisfazione di milioni di utilizzatori di iPhone, equipaggiati con qualsivoglia carta di credito o debito desiderino impiegare. Torniamo indietro al mercato equilibrato, dunque?

Gruber va anche al sodo, quando racconta di come Nfc sia uno standard, ma il lavoro di Apple attorno a esso sia stato talmente buono che Apple Pay funzionava anche sui terminali di pagamento che teoricamente non lo riconoscevano. Al punto che la catena di farmacie americana Rite Aid [disabilitò i suoi terminali Nfc](https://macintelligence.org/posts/2014-10-26-tre-segni/) pur di fermare Apple Pay; perché Rite Aid sosteneva CurrentC, un consorzio alternativo che voleva competere con Apple Pay ma pare tuttora in fase di assestamento. Dice Gruber:

>CurrentC sembra il genere di iniziativa che la Commissione europea è più propensa a proporre: ostile all’utente e amico delle aziende di medio calibro.

La Commissione vede Nfc più o meno come una app, mentre è una funzione del sistema operativo: l’idea dell’Europa, evidenzia Gruber, è che Apple dovrebbe essere obbligata a permettere ad aziende indipendenti di aggiungere funzioni al proprio sistema operativo.

C’è molto più di questo: considerazioni di design, esperienza utente, merito di Apple (che oggi detiene tipo il novanta percento dei pagamenti contactless nel mondo dove è disponibile, nonostante la sua – sempre qui si arriva – quota di mercato sia assai minore di quella di Android, che supporta Nfc dal 2011).

Tra l’altro, Apple ha di recente annunciato la funzione Tap to Pay: toccare il resto dell’apparecchio per pagare con carte contactless, *o con altri digital wallet*. Questo per l’anticoncorrenzialità.

C’è moltissimo da dire e Gruber lo dice tutto, senza mezzi termini, con abbondanza di link e particolari. Da leggere accuratamente e riflettere su dove possiamo andare, se alla guida del continente stanno ottusità come quella di Margrethe Vestager.