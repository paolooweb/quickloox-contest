---
title: "Pensa in grande, agisci in piccolo"
date: 2020-02-12
comments: true
tags: [iMac, iPad, iPhone, Technology, Learning, Londra, Alitalia, Finnair, Trenord, Padcaster, iPad]
---
Qualche foto scattata di fretta durante [la mia giornata](https://macintelligence.org/posts/2020-02-12-una-gita-fuori-aeroporta/) a [Learning Technology 2020](https://www.learningtechnologies.co.uk).

 ![iMac a Junction 18](/images/iMac-a-Junction-18.jpg  "iMac a Junction 18") 

 ![iMac a People Matter Most](/images/iMac-a-People-Matter-Most.jpg  "iMac a People Matter Most") 

Dal punto di vista Apple, una tendenza già latente negli anni scorsi, adesso assolutamente esplosa, è iMac come macchina da fiera: per mostrare demo, filmati promozionali, interfacce e sistemi per la produzione di contenuto.

 ![iMac a LinkedIn](/images/iMac-a-LinkedIn.jpg  "iMac a LinkedIn") 

 ![iMac a Netex](/images/iMac-a-Netex.jpg  "iMac a Netex") 

Dal punto di vista *learning*, un grazioso paradosso: schermi sempre più grandi per inseguire un cliente con la promessa di lezioni online che funzionano anche sul più piccolo degli schermi. Legioni di iPhone e iPad mini, per mostrare su superfici minime la bontà del lavoro compiuto su quelle mastodontiche.

 ![iMac a Thirsty](/images/iMac-a-Thirsty.jpg  "iMac a Thirsty") 

 ![iMac a Totara](/images/iMac-a-Totara.jpg  "iMac a Totara") 

Qualche Mac portatile, più che altro per amministrazione, anagrafiche, sbrigare il lavoro nonostante il dovere di presenza negli stand. L’idea di frequentare corsi online su un computer convenzionale, almeno in una fiera di settore, è *out*.

 ![iMac a Area9](/images/iMac-a-Area9.jpg  "iMac a Area9") 

 ![iMac a LearningPool](/images/iMac-a-LearningPool.jpg  "iMac a LearningPool") 

 ![iMac a Fuse](/images/iMac-a-Fuse.jpg  "iMac a Fuse") 

Menzione d’onore a Padcaster, studio di produzione video mobile basato su iPad, alla faccia del gadget che non è un computer vero e serve solo per consumare dati.

 ![Padcaster](/images/padcaster.jpg  "Padcaster") 

Grazie ad Alitalia che ha reso impossibile il check-in online per il volo di ritorno, dopo avere incamerato ogni e qualsiasi possibile dato di registrazione per quello di andata, fiscale anche sui *middle name* come se fossero mai importati ad alcuno; e anche per avere richiesto al momento del ritorno tutti i dati, nessuno escluso, come se niente fosse mai accaduto.

Se penso alla [app di Finnair](https://apps.apple.com/it/app/finnair/id933867978) vengono i brividi, non perché l’ho usata a Helsinki.

La schermata qui sotto è cortesia di Trenord.

 ![Gli orari di Trenord alla stazione di Milano Porta Vittoria](/images/trenord-screen.jpg  "Gli orari di Trenord alla stazione di Milano Porta Vittoria") 