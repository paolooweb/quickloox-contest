---
title: "Non è una Apple per vecchi"
date: 2015-03-29
comments: true
tags: [Cook, Hertzfeld, Folklore, Mac, Macintosh, FastCompany, valori]
---
[The Apple Spirit](http://www.folklore.org/StoryView.py?project=Macintosh&story=The_Apple_Spirit.txt&sortOrder=Sort+by+Date) di Andy Hertzfeld, 29 novembre 1988, sul meraviglioso sito *Folklore* dedicato all’aneddotica sulla nascita di Macintosh raccontata da chi ci ha veramente lavorato.<!--more-->

>L’industria del personal computer ha continuato a crescere e cambiare dalla presentazione del Macintosh originale. Apple è diventata una impresa da quattro miliardi di dollari e spesso temo che si sia perso il contatto con i loro valori originali. Eppure ricordo di avere avuto preoccupazioni simili giusto prima di iniziare a lavorare su Mac. Sono sicuro che in questo momento in Apple esistono piccoli gruppi che sono ispirati da Macintosh nella stessa maniera in cui lo furono da Apple ][. La grande sfida che attende i dirigenti di Apple è permettere a questi gruppi di seguire il loro cuore e la loro immaginazione, senza compromessi verso le inevitabili scelte politiche delle grandi organizzazioni. Spero che nel 1991 potrò comprare un nuovo computer Apple che non è un Macintosh né un Apple ][ piuttosto un sistema interamente nuovo che ancora una volta condivida lo spirito possente dei suoi illustri antenati.

Ai giorni nostri Tim Cook ha appena concesso a *Fast Company* un’intervista intitolata [Tim Cook sul futuro di Apple: tutto può cambiare tranne i valori](http://www.fastcompany.com/3042435/steves-legacy-tim-looks-ahead).

>È fondamentale che Apple faccia tutto quello che è nelle sue facoltà pur di rimanere informale. Uno dei modi per riuscirci è stare insieme. Uno dei modi per garantire la collaborazione è assicurarsi che le persone possano imbattersi una nell’altra, non solo le riunioni a calendario, ma tutte quelle evenienze serendipitiche che accadono ogni giorno in cafeteria e camminando per un corridoio.

>Cambiamo ogni giorno. Lo facevamo quando c’era [Steve] e lo facciamo ora che lui non c’è. Ma il nucleo, i valori centrali rimangono quelli del 1998 che sono quelli del 2005 e del 2010. Non credo che i valori debbano cambiare. Tutto il resto può.

Si sente ancora l’eco dei piccoli gruppi, dell’ispirazione, e c’è l’assicurazione che tutto può cambiare ma i valori restano quelli. Nel 1991 Apple non aveva un nuovo computer che non era né un Apple ][ né un Mac; nel 2015, tra iPad, iPhone, Apple TV e prossimamente watch, per non parlare di iPod, si può dire che qualcosina sia successo.

Stanno succedendo cose che Andy Hertzfeld si auspicava nel 1988 e Tim Cook risponde con chiarezza a quelle paure espresse ventisette anni fa… in un’impresa che fa cinquanta volte quei quattro miliardi.

Ogni tanto qualcuno scrive che Apple non è più quella di una volta. Ovviamente ci sono mille metriche per verificarlo e tante di queste metriche possono confermare il fatto. Più che discutere l’affermazione, mi viene da pensare che si tratti di gente vecchia. Non anagraficamente, vecchia nel pensiero. Quando parla Tim Cook non pensa minimamente ad Andy Hertzfeld, ed è è questa la buona notizia: quelle paure, quelle speranze sono uguali a oggi. E la risposta, per quanto possibile in una organizzazione che – quella sì – è decisamente cambiata, è quella giusta; ma spontaneamente, non perché Hertzfeld ha sollevato un problema. I più giovani talenti in Apple probabilmente nemmeno sanno chi era o che cosa ha fatto.

Apple pensa ancora in modo giovane. Ancora una volta, l’anagrafe non c’entra. Cook è ultracinquantenne.
