---
title: "Dall’altra parte"
date: 2021-07-26T00:49:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [Vaccino, Covid] 
---
Nella mia bolla sociale sono esplosi i commenti sull’opportunità di vaccinarsi o meno, suppongo a valle delle polemiche sulla necessità di certificazione e relativo *green pass* a partire dai primi di agosto.

Scopro le mie carte subito, così chi vorrà prendersela con me lo farà per questo e non per quanto dirò dopo: sono giustappunto reduce dalla seconda dose. Se in generale condivido l’idea del *green pass*, comunque l’Italia lo trasformerà all’italiana in un papocchio indegno e questo andrebbe evitato, perché ci costerà molto e sarà sommamente inutile.

Ma non era questo.

Ho letto molte, molte cose in questi ultimi giorni e noto che nella maggioranza dei casi si tratta di un fronte di opinione che vuole convincere delle proprie ragioni, mentre l’altro fronte controbatte sulle ragioni.

Non ho mai assillato persone perché passassero a Mac; semmai era il contrario, un sacco di gente che usava Windows mi assillava per farmi passare a Windows. E controbattevo i loro tentativi di convincimento.

Sono un tiratardi. Non ho mai provato a convincere chicchessia sull’opportunità di alzarsi tardi, mentre ho sentito predicozzi a tonnellate di chi voleva che io mi alzassi presto. Anche qui, non ho mai avuto ragioni particolari per alzarmi tardi e, se ho risposto, ho controbattuto gli argomenti per alzarsi presto.

Potrei andare avanti ma posso già concludere. Quando c’è gente che vuole convincermi a tutti i costi delle sue ragioni, per certo io sto dall’altra parte. A volte mi è costato, a volte ci ho guadagnato, tuttavia mi sono sempre trovato meglio così. Uso Mac, mi alzo tardi quando posso e mi sono vaccinato contro il Covid.

Non voglio convincere nessuno.

*Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile.*