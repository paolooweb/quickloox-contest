---
title: "Il microscopio da tredici pollici"
date: 2021-05-29T00:54:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [Lux, iPad Pro, Halide] 
---
Nella [recensione delle fotocamere del nuovo iPad Pro M1](https://lux.camera/the-m1-ipad-pro-cameras/) da parte di Lux (pura omonimia…) si accenna a un *superpotere* di cui Apple non fa cenno.

>In pratica, iPad Pro ha un microscopio. Si possono scattare foto macro incredibili senza bisogno di accessori. iPhone 12 Pro (o qualsiasi altro iPhone) ha un obiettivo differente e mette a fuoco solo fino a circa otto centimetri dal soggetto. iPad Pro mette a fuoco facilmente su oggetti molto più vicini.

Gli esempi di foto che appaiono nella pagina sono davvero impressionanti e fanno venire l’acquolina in bocca per un ennesimo motivo in più, a partire naturalmente dal processore M1.

Dietro a Lux stanno i creatori della fortissima app per fotografia [Halide](http://halide.cam/download), che è stata aggiornata apposta per sfruttare caratteristiche fotografiche specifiche di iPad e merita assolutamente un’occhiata per chi sia interessato al tema.

Una fotocamera di un certo livello, debitamente accessoriata, può certo battere iPad nella precisione e nella qualità di una foto macro. Ma d’altro canto, non sono molte le fotocamere con un visore da tredici pollici.

*Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile.*               