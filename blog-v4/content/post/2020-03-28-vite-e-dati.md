---
title: "Vite e dati"
date: 2020-03-28
comments: true
tags: [NYT, coronavirus, Debian]
---
Man mano che procediamo verso il picco, si rende sempre più evidente l’inadeguatezza della raccolta dati. Qualcosa che nessuno dei nostri ineffabili governanti ha considerato, un tema alieno, remoto. Come può gente ferma al Ventesimo secolo avere consapevolezza della potenza dei dati, specialmente quando potrebbero salvare vite e lavori in più?

Invece ogni regione conta in modo diverso, probabilmente ogni provincia, forse ogni comune. È impossibile fare confronti sensati e disegnare una rappresentazione omogenea della situazione. Se l’azione sanitaria muta strategia in corso d’opera è chiaro che cambia i risultati, ma una buona rilevazione dovrebbe prevdedere i giusti correttivi. Invece appare evidente che di competenza in questo campo scarseggiamo, mentre negli uffici sanitizzati albergano piuttosto specialisti capaci di limare all’infinito il modulo di autocertificazione.

In un paese sano (a parte il virus) avremmo un giornalismo che svolge il proprio compito deontologico di sorvegliante dei poteri e distributore di informazione corretta. Che porta alla libertà. Che puòsalvare vite, in una situazione come questa.

Spesso si fanno confronti tra il nostro sistema sanitario e quello degli Stati Uniti, dove il virus ha avuto una performance iniziale ben superiore persino alla nostra. ma lì, a compensazione delle deficienze del sistema, abbiamo un *New York Times* che [rende pubblico il proprio dataset dei casi di coronavirus](https://www.nytimes.com/article/coronavirus-county-data-us.html), a beneficio di tutti.

Dal primo caso registrato negli USA, un team di giornalisti e tecnici li ha classificati tutti. Sono credo ottantacinquemila.

Non aggiungo altro e torno a decifrare gli [Unattended Upgrades di Debian](https://wiki.debian.org/UnattendedUpgrades), ché devo allestire la prossima casa di questo blog. Prima del picco, magari.
