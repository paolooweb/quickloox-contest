---
title: "Qualcosa di meno basic"
date: 2014-10-29
comments: true
tags: [Basic, Ruby, Python, Sql, Office, Xml, JavaScript, Css, Html5, Scratch]
---
Ieri ho pubblicato le [motivazioni di una docente](https://macintelligence.org/posts/2014-10-28-veramente-troppo-basic/) che vuole insegnare Visual Basic come linguaggio di programmazione per una scuola superiore. Oggi le accompagno con brevi riflessioni che ho inviato a un genitore giustamente preoccupato della faccenda. Che riguarda Visual Basic, in quanto tale, del tutto parzialmente; da temere sono i modi di pensare, il vecchiume, l’indifferenza di fronte non al nuovo, bensì al tempo presente.<!--more-->

* è un linguaggio semplice, intuitivo che, grazie all’interfaccia grafica, permette agli studenti un immediato riscontro dei risultati del loro lavoro rispetto ad altri linguaggi (come C e Pascal) che richiedono basi teoriche solide

Visto che il Ministero incoraggia l’uso di software *open source* e la legge chiede che le amministrazioni pubbliche usino formati aperti, sarebbe consigliabile l'uso di linguaggi aperti e disponibili nativamente su qualsiasi piattaforma diffusa. Scelte come [Ruby](http://ruby-it.org) o [Python](http://www.python.it) sono assai più facili e intuitive di Visual Basic, sono *open source*, sono native su qualunque piattaforma.

Un esempio di quanto sia facile e intuitivo [Ruby](http://tryruby.org/).

Un esempio di quanto sia facile e intuitivo [Python](http://www.learnpython.org/it/).

* è propedeutico alla programmazione orientata agli oggetti supportando comunque la programmazione strutturata;

Qualunque linguaggio moderno soddisfa questo requisito.

* è il linguaggio nativo/applicativo della piattaforma Office e permette l’integrazione con il linguaggio SQL (anche esso previsto dai programmi ministeriali)

Qualsiasi linguaggio degno di questo nome si interfaccia con Sql. Quanto all’automazione di Office, esistono interfacce di programmazione che permettono a qualunque linguaggio decente di approfittarne.

Un esempio di [automazione di Office con Python](http://nbviewer.ipython.org/github/sanand0/ipython-notebooks/blob/master/Office.ipynb).

Un esempio di [automazione di Office con Ruby](http://rubyonwindows.blogspot.it/2007/03/automating-excel-with-ruby.html) e anche [un altro esempio](http://www.devarticles.com/c/a/Ruby-on-Rails/Creating-Reports-with-Ruby-and-Microsoft-Office/).

Oggi un linguaggio di programmazione non serve primariamente ad automatizzare Office, ma a interfacciarsi con il web, con le basi dati, con gli oggetti programmabili, nonché per lo scripting a livello di sistema e per creare app per sistemi mobili e per il browser. XML, HTML5, JavaScript, CSS sono molto più importanti di Office per il futuro professionale degli studenti. Lo stesso JavaScript [guadagna sempre maggiore favore](http://www.apogeonline.com/webzine/2014/09/12/gradita-sorpresa) per il fatto di essere integrabile nelle pagine web e avere una evoluzione rapidissima.

Parlando di professione, tra l’altro, [Visual Basic è pochissimo usato dalla comunità dei programmatori](http://www.tiobe.com/index.php/content/paperinfo/tpci/index.html).

* è il linguaggio che meglio appassiona gli alunni in base alla mia esperienza;

La docente nomina unicamente C, Pascal e Visual Basic. Se sono gli unici linguaggi che ha proposto, è un po’ ovvio dove cada la scelta degli studenti: la strada più pigra e comoda. Ammesso che il linguaggio da insegnare sia quello che *appassiona* (seriamente?) gli studenti e non quello che torna loro più utile, di cui magari neanche conoscono l’esistenza.

* è un linguaggio comunque di livello professionale se confrontato con le recenti proposte editoriali come Scratch;

[Scratch](http://scratch.mit.edu) è pensato per studenti dieci anni più giovani o giù di lì. Chiaro che non vada bene per una secondaria superiore. Passare da un linguaggio semisperimentale per bambini a Visual Basic, saltando a pié pari tutte le alternative in mezzo, è uno stratagemma o ignoranza?

* è attualmente utilizzato anche in altri istituti scolastici statali della zona

Questo è di interesse zero. Interesserebbe sapere con che preparazione escono, grazie a quel linguaggio. Peraltro trovo agghiacciante l’idea che tutte le scuole insegnino lo stesso linguaggio di programmazione. Ecco un [elenco dei linguaggi di programmazione finora creati](http://en.wikipedia.org/wiki/List_of_programming_languages).

* Ricordo di aver già proposto la piattaforma agli studenti e di non aver ricevuto alcun feedback negativo alla recente assemblea dei genitori.

Che ne sanno i sedicenni del linguaggio di programmazione giusto per loro? Immagino la competenza dei genitori, che avranno mollato gli ormeggi dopo la parola *gratis*.

Ho risposto al genitore che perderà, perché combatte contro due forze irresistibili: una docente che sa insegnare solo quello e chissà da quanti anni impara nient’altro e una scuola piena di vecchi PC con Windows XP.

D’altro canto, ho aggiunto che sono disponibile per qualsiasi iniziativa, dal momento che certe battaglie vanno combattute [anche quando è certa la sconfitta](http://en.wikipedia.org/wiki/The_Ballad_of_the_White_Horse).

<iframe width="560" height="315" src="//www.youtube.com/embed/sMCXrlxkhx0" frameborder="0" allowfullscreen></iframe>