---
title: "Memorabilia"
date: 2021-09-10T03:13:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [AutoCad, Teams] 
---

Ho scritto per una società il memo che segue. Ma le considerazioni contenute sono abbastanza universali da essere condivise più in largo. Magari un giorno verranno ricordate.

<h1 style="text-align:center">Il fine giustifica i mezzi di comunicazione</h1>

<p style="text-align:right">
<em>La comunicazione è terribile.<br>
— Jeff Bezos</em>
</p>

<p style="font-size:larger">
<em>Premessa: in una società qualunque, è il collaboratore che va incontro alle necessità dell’azienda; non è l’azienda che va incontro alle necessità del collaboratore. Noi vogliamo essere una società qualunque? O vogliamo essere migliori?</em>
</p>

<h2>Il vero senso della collaborazione è guadagnarci</h2>

<p>
Per la società, la risposta migliore è quella che porta il maggiore guadagno, i minori costi, una ragionevole soddisfazione dei collaboratori (sarei più soddisfatto dal lavorare oppure dal vivere di rendita e fare crociere attorno al mondo? Credo la seconda. La mia soddisfazione sul lavoro voglio che sia la più alta possibile, ma troverò sempre un limite di ragionevolezza).
</p>

<p>
Per un collaboratore, la risposta migliore è avere il massimo guadagno con il minimo sforzo… no, no. Quei tempi sono finiti. La risposta migliore è collaborare. Lavorare insieme alla società; se la società va bene ed è una buona società, il collaboratore avrà più soddisfazioni e guadagnerà di più. Quindi il collaboratore vuole metterci impegno, per tendere a una soddisfazione comune, che gioverà a entrambe le parti.
</p>

<p>
Si dovrebbe capire che la risposta più conveniente per tutti alla domanda sopra (vogliamo essere una società qualunque oppure essere migliori?) è trovare il punto giusto in cui si incontrano le rispettive necessità, se siamo gente come tanta. Se siamo bravi, società e collaboratore, troveremo il punto perfetto per fare incontrare i rispettivi desideri. Molto meglio delle necessità.
</p>

<p>
È difficile perché tendiamo tutti, società e collaboratori, a confondere necessità e desideri. La prima responsabile di questa confusione è l’abitudine.
</p>

<h2>L’ostacolo peggiore è l’abitudine</h2>

<p>
<strong>L’abitudine è desiderare quello che si ha.</strong>
</p>

<p>
L’abitudine, quando si presenta la necessità a chiedere il cambiamento, cerca di cacciarla via per mantenere le cose come sono. Perché l’abitudine ci fa pigri, paurosi, diffidenti. Per difenderla, trasformiamo in necessità tutto quello che abbiamo già.
</p>

<p>
Purtroppo chiudersi al cambiamento invece di approfittarne è un vicolo cieco. Le abitudini sono come l’alcool: una birra bendispone e scioglie le tensioni. Troppa birra fa perdere il controllo della situazione e quando ce ne accorgiamo è tardi.
</p>

<p>
Se invece desiderassimo obiettivi che ancora non abbiamo raggiunto? Significa crescita, carriera, self-improvement, maturità, un sacco di cose. Significa trovarsi alla porta il desiderio a chiedere il cambiamento, non più la necessità. Significa accogliere il desiderio, fare un patto insieme, partire per fare cose nuove, più importanti di prima, più belle di prima, più difficili di prima. Ciao ciao, necessità; cambiamo e inseguiamo una vita più interessante.
</p>

<h2>Tradotto in pratica</h2>

<p>
Che cosa significa questo giro di parole nella mia vita lavorativa – forse non solo – quotidiana?
</p>

<p>
Tutto questo riguarda per prima la società. Ha desideri forti: vuole crescere, vuole fare di più, fare meglio; per riuscirci, può solo cambiare. Restare quello che era il primo giorno la farebbe spegnere in poco tempo.
</p>

<p>
<strong>Tra i desideri della società c’è comunicare di più e con più efficacia, internamente e fuori.</strong>
</p>

<h2>Che cosa desidera la società</h2>

<p>
Internamente, la società lavora alla costruzione di un team coeso, motivato, propositivo, in crescita, unito da qualcosa che vada oltre l’avere lo stesso datore di lavoro, capace di condividere competenze e doti anche con chi non fa parte del mio gruppo di lavoro, capace di lavorare duramente e anche di divertirsi, a volte persino insieme.
</p>

<p>
Esternamente, si impegna per farsi conoscere, ampliare il raggio d’azione, trovare nuovi clienti e nuove commesse, farsi riconoscere autorevolezza, trasmettere un’idea diversa di società, che lavora duramente e sa anche divertirsi. (Sì, siamo sempre gli stessi, quando parliamo tra noi e quando parliamo con il resto del mondo).
</p>

<p>
Sulla base di quanto già detto, per chi legge questo documento ha senso cercare il miglior punto di incontro tra i propri desideri e quelli della società in tema di comunicazione. Attivamente e propositivamente; è giusto che la società avanzi nuove idee, metà delle volte. L’altra metà tocca a noi.
</p>

<p>
<strong>Una riflessione autentica su questo potrebbe diventare vantaggiosa per tutti</strong>. Ed è il messaggio più importante di questo memo.
</p>

<p>
Ce n’è un secondo, che riguarda i mezzi di comunicazione.
</p>

<h2>I dati restano, le app passano</h2>

<p>
Durante gli ultimi confronti è emersa la tendenza a ragionare per mezzi, o per strumenti: per app, per piattaforme, per servizi, presentandoli come i migliori, oppure come necessità, o come abitudini.
</p>

<p>
Abbiamo già visto quanto necessità e abitudini siano un ostacolo al cambiamento e quindi alla realizzazione dei desideri, di tutti.
</p>

<p>
È molto importante comprendere che la crescita va in direzione dei dati, non delle app per produrli. Non usiamo AutoCad perché ci chiedono di usare AutoCad; lo usiamo perché dobbiamo produrre file nel formato preferito da AutoCad.
</p>

<p>
Tra dieci anni useremo ancora AutoCad? Se il formato richiesto continua a essere quello, nessun dubbio. E se cambiasse? In editoria usavano tutti XPress; poi è cambiato il formato di riferimento e oggi usano InDesign. Gli anziani che usano Excel, da giovani usavano 1-2-3. E ha cambiato il 99% delle persone, compresi i difensori strenui di questo o quel programma. Perché il fine era cambiato e il cambiamento si è portato via chi voleva restare fermo.
</p>

<p>
In nome dei dati, non dovremmo avere un problema a cambiare la app che usiamo per produrli. Altrimenti abbiamo un problema, di abitudine.
</p>

<h2>Conta il messaggio, non lo strumento</h2>

<p>
Lo stesso vale per gli obiettivi di comunicazione. È stato detto un gran bene di Teams perché <em>ha la chiamata, la condivisione dei file e la chat</em>. Ora, l’elenco delle piattaforme che hanno le stesse identiche possibilità è ridicolmente lungo. Alcune di queste piattaforme sono gratuite, o sarebbero installabili privatamente su un server della società. Funzionano meglio di Teams? Alcune no, altre sì, altre boh (ne uso tante, ma non proprio tutte).
</p>

<p>
Se il desiderio della società fosse avere una piattaforma unificata di comunicazione interna, dove si troverebbe il punto di incontro migliore con il mio desiderio? Probabilmente sulla richiesta di avere video-condivisione-chat. Sui dati. Il fatto di avere queste cose sul programma X oppure su Y diventa secondario. Anche perché l’anno prossimo potrebbe essere più conveniente il programma Z…
</p>

<p>
Lo stesso discorso si applica alla produzione di documenti. Personalmente rimango perplesso ogni volta che sento parlare di licenze Office a pagamento. Perché – con una singola eccezione: le macro di Excel – Office è inutile.
</p>

<p>
Per inutile intendo dire che è facile produrre senza Office il tipo di documenti che ci servono (questo, per esempio); ed è altrettanto facile produrli in formato Office ove serva. Rimane ugualmente facile leggere file in formato Office in arrivo da terzi.
</p>

<p>
Il software per farlo, ancora una volta, è vario e può essere gratuito, in forma di app o di piattaforma, su cloud pubblico o privato.
</p>

<p>
Provocazione: <strong>accetteresti di rinunciare a Office e metterti in tasca la metà della quota di licenza che ti permette di usarlo</strong> (l’altra metà la risparmia la società e tutti sono più contenti)? Più terra terra: qual è il fine che giustifica Office? Necessità o desiderio? Crescita o abitudine?
</p>

<h2>Tre stili di lavoro (e di vita)</h2>

<p style="font-size:larger">
Ragionare per desideri può fare crescere la società e fare crescere ciascuno di noi, grazie al cambiamento.
</p>

<p style="font-size:larger">
Ragionare per necessità ci fa mettere pezze, stare in ritardo sulle cose, restringere la visione.
</p>

<p style="font-size:larger">
Ragionare per abitudini ci fa uscire dal mercato, o al più scivolare verso il basso. Magari non proprio oggi, ma il conto arriverà.
</p>

<p style="font-size:larger">
Scegliamo.
</p>



*Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile.*