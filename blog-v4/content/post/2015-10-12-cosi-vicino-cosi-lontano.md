---
title: "Così vicino, così lontano"
date: 2015-10-12
comments: true
tags: [emulatori, Mac, Plus, Dark, Castle, Duncan, Prince]
---
Avevo [accennato](https://macintelligence.org/posts/2013-12-27-testardo-come-un-emulatore.md) all’esistenza di un [emulatore di Mac Plus con System 6](http://jamesfriend.com.au/pce-js/).<!--more-->

Di recente la dotazione di programmi si è arricchita con [Dark Castle](http://darkcastle.wikia.com/wiki/Dark_Castle). Imperdibile per gli sfortunati cui non è stato donato di poterlo giocare nel 1986.

Non più discriminazioni, ora: chiunque può cimentarsi nei panni del principe Duncan. Tranne me, perché ai tempi riuscii a finire il gioco e non intendo riprovarci per evitare di rovinare il record. Attenzione perché l’emulatore configura il gioco su un tastierino numerico, assente sulla gran parte delle tastiere attuali. Prima di iniziare, passare dalle opzioni.

Che nostalgia. Sembra ieri. Al tempo stesso, quanto è lontana quell’esperienza di gioco e di Mac, quando quella finestrella 512x342 era un mondo che si apriva e oggi, pure su uno schermo Full HD che è già vecchio di suo, è grande come la tessera dell’ipermercato.