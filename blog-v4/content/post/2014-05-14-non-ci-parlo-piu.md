---
title: "Non ci parlo più"
date: 2014-05-14
comments: true
tags: [Skype, Microsoft, Nsa, Prism, Ars, Snowden, iMessage]
---
Chi avesse bisogno di aggiornamenti sul passato recente di Skype potrebbe consultare questo [post del 2012](https://macintelligence.org/posts/2012-07-24-manco-a-dirlo-a-voce/) e magari l’articolo di *Ars Technica* intitolato [Pensi che i tuoi messaggi Skype siano cifrati dalla partenza all’arrivo? Ripensaci](http://arstechnica.com/security/2013/05/think-your-skype-messages-get-end-to-end-encryption-think-again/). Microsoft può controllare i dati in transito su Skype e non esita a farlo quando lo ritiene opportuno.<!--more-->

Sul presente immediato, un altro articolo di *Ars Technica* nota come uno dei documenti resi pubblici da Edward Snoden classifichi Skype come *vitale* per la sorveglianza clandestina delle comunicazioni su Internet effettuata dalla National Security Agency americana, di cui nessuno avrebbe saputo senza le rivelazioni di Snowden stesso. I dati raccolti comprendono:

>elenchi di contatti, informazioni sulle carte di credito, registrazioni delle chiamate, dati sugli account degli utilizzatori e altro.

Lo fa anche Facebook, per dire. Con la differenza che su Skype la gente è convinta di telefonare e fa passare informazioni che probabilmente su Facebook non arrivano.

Giusto per confronto, iMessage cifra tutto, dall’inizio alla fine. E lo faceva anche Skype, prima che lo acquisisse Microsoft.

Il mio [bando personale di Skype](https://macintelligence.org/posts/2013-10-02-il-signore-si-che-se-ne-intende/) continua con un certo successo ed eccezioni rarissime. Se c’è gente che intende parlare con me su una piattaforma dove c’è la coda per intercettare dati, faccio del mio meglio per proporre alternative. Con Skype non ci voglio parlare più.