---
title: "Questa non me la perdo"
date: 2020-04-24
comments: true
tags: [Bloomberg, Arm, Mac, Intel]
---
Secondo Bloomberg, [l’anno prossimo arrivano i primi Mac con processore Arm](https://www.bloomberg.com/news/articles/2020-04-23/apple-aims-to-sell-macs-with-its-own-chips-starting-in-2021).

La cosa in sé [non è una novità](https://macintelligence.org/posts/2018-12-27-divergenze-parallele/). Se ne parla da anni.

La notizia vera, fosse vero, sarebbe la data precisa, più il parlare *dei processori* e non *dell’annuncio della transizione*.

John Gruber [copre al meglio l’articolo di Bloomberg](https://daringfireball.net/linked/2020/04/23/bloomberg-arm-macs) e nota la vera bomba: se l’anno prossimo ci saranno Mac con processori Arm, l’annuncio avverrà durante [Wwdc](https://developer.apple.com/wwdc20/), a giugno prossimo.

Visto anche il formato virtuale dovuto all’emergenza sanitaria, questa edizione di Wwdc potrebbe essere davvero epocale. Non me la perdo proprio.

Anche sapendo bene, come nota Gruber, che Bloomberg ha fatto non pochi buchi nell’acqua in tema Apple negli ultimi anni, e di entità ragguardevole.