---
title: "C’è sempre una prima volta"
date: 2015-01-17
comments: true
tags: [iPad, Mac, Goodreader, AirPort, iTunes, sincronizzazione, Wi-Fi]
---
Ho appena portato un file da Mac su Goodreader per iPad via iTunes.<!--more-->

iPad è sul comodino, sei metri in linea d’aria dalla base AirPort. Ho potuto farlo perché ho attiva su iTunes l’opzione di sincronizzazione via Wi-Fi.

Ho appena cercato di portare lo stesso file da Mac sul Goodreader di un altro iPad, sempre via iTunes, cinque secondi dopo.

L’altro iPad è qui appoggiato sulla scrivania, un metro in linea d’aria dalla base AirPort. Non ho potuto farlo nonostante sia attiva su iTunes l’opzione di sincronizzazione via Wi-Fi.

Prima volta che vedo una cosa del genere. Lo stupore dovrebbe diventare reato; non c’è da stupirsi di niente.