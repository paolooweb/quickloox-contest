---
title: "Dare, non dire"
date: 2022-03-14T00:41:17+01:00
draft: false
toc: false
comments: true
categories: [Hardware]
tags: [The Verge, Mac Studio, Apple, iMac, Mac Pro]
---
*The Verge* gode di una solida reputazione per non presentarsi come antiApple a prescindere e dimostrarsi ampiamente schierato non appena si entri nel merito.

È pertanto abbastanza sorprendente che scrivano un pezzo complessivamente positivo sugli [ultimi annunci di Apple](https://macintelligence.org/posts/2022-03-09-panta-mac/), che accenna alla [nuova strategia di dare – non dire – agli utenti quello che vogliono](https://www.theverge.com/2022/3/9/22968839/apple-mac-studio-display-m1-ultra-strategy-users).

Ed è il pezzo più squilibrato nella critica a sfavore che si trova in giro. Complessivamente sembra che i critici sperassero nell’avvento di un Mac a lungo desiderato e rimasto ipotetico, modulare e versatile più di un iMac e meno costoso di un Mac Pro.

La sensazione a pelle è che fosse una macchina che aspettavano in molti anche fuori dai siti di tecnologia, più presi dalla necessità di lavorare ad alte prestazioni che a chiacchierare della porta che manca o dell’innovazione che invece una volta.

L’idea generale è che il prezzo non sia così fuori mercato e anzi, puntando in alto, si possa tranquillamente comparare con quello che si otterrebbe el cercare un PC di fascia alta.

Con la differenza che Mac Studio consumerà meno se non va più veloce e andrà più veloce se non consumerà meno.

Questo aspetto dell’offerta, Apple Silicon, sta davanti da qualche tempo a tutti gli altri pregi di Mac. C’è un vantaggio sempre; qualcosa che non si era mai visto prima. Se poi si dà alle persone quello di cui hanno bisogno, ce n’è abbastanza per spargere nell’aria un po’ di ottimismo che in questo momento fa solo un gran bene.