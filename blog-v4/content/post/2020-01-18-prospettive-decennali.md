---
title: "Prospettive decennali"
date: 2020-01-18
comments: true
tags: [Snell, Six, Colors, Apple]
---
Ogni tanto pare che trimestre per trimestre la fine del mondo sia vicina, o il nirvana, secondo quel più o meno uno percento.

Mettere le cose in prospettiva rilassa e rasserena. Per esempio, Jason Snell su *Six Color* ha pubblicato [alcuni grafici sull’andamento decennale di Apple](https://sixcolors.com/post/2020/01/fun-with-charts-a-decade-of-apple-growth/).

Dieci anni di fatturati, vendite di Mac, iPad e iPhone. Utile per vedere quando i professionisti sono scappati da Mac, o quando iPhone ha iniziato a declinare.

Già, iPhone. È l’oggetto del primo grafico di Snell, solo che i fatturati sono rappresentati con una scala diversa da quelli di Mac e iPad, per contenerlo in dimensioni accettabili.

A fine pagina Snell ripubblica lo stesso grafico, però con scala identica a quella degli altri.

È istruttivo e induce un vago senso di vertigine.