---
title: "Corsa contro il tempo"
date: 2016-08-23
comments: true
tags: [Pangea, Cro-Mag, Rally, Lidia]
---
L’iPad di Lidia contiene numerosi giochi ancora troppo complicati per la sua età ed è interessante vedere quanto durano prima di essere impietosamente terminati dalla piccola collaudatrice. Alcuni non passano la schermata con i marchi dei produttori, altri arrivano a malapena a quella di inizio gioco.

[Cro-Mag Rally](https://appsto.re/it/x7XYq.i) ha superato a pieni voti lo scrutinio guadagnandosi secondi e secondi di considerazione e, addirittura, più di una partita, giocata dal papà con la bimba a commentare quello che accadeva sullo schermo.

Alla fine era divertito anche il papà. Non se lo ricordava più, a parte l’essere gioco di corse di kart con ambientazione cavernicola. Grafica, opzioni, giocabilità, velocità, interfaccia, tutto fresco e piacevole come appena colto quando invece si tratta di un gioco che ha un’età e la stessa produttrice Pangea, [in cerca di acquirente](https://macintelligence.org/posts/2016-04-11-gli-extra-che-compri/), difficilmente aggiornerà in modo significativo. Eppure quegli 1,99 euro sono più che ben spesi e ad avere cinquecentomila volte la stessa cifra verrebbe voglia di fare un pensierino a Pangea stessa.

Pangea vende su App Store anche un [bundle](https://appsto.re/it/QctU2.i) da dieci giochi per 14,99 euro che è un affarone in termini di rapporto qualità/prezzo. Se non ne possedessi già otto, Cro-Mag Rally incluso, sarebbe un acquisto a colpo sicuro.
