---
title: "Questione di metodo / 1"
date: 2017-05-08
comments: true
tags: [Jobs, iPhone, Cook]
---
Suppongo che la mancanza di Steve Jobs si avverta, in Apple. Come non potrebbe? È stato nel bene e nel male un personaggio unico che ha definito un’epoca. Mi sembra più interessante provare a capire se il *team* abbia saputo serbarne gli insegnamenti e da questo punto di vista la recente attualità è rivelatrice.<!--more-->

Tim Cook, a commento degli ultimi risultati di vendita di iPhone (crescita nominale a causa di una flessione importante in Cina), ha [indicato tra le cause](http://www.cnbc.com/2017/05/03/apple-ceo-tim-cook-says-rumors-hurt-iphone-sales-in-china.html) anche i *rumor* riguardanti il prossimo modello. I siti acchiappaclic hanno anticipato o millantato veramente di tutto rispetto a iPhone 8, o iPhone del decimo anniversario, o iPhone X eccetera. Molto prima delle specifiche tecniche comincerei dal nome; può darsi che il prossimo iPhone sia piu ambizioso dei modelli precedenti. Se però venisse seguita la nomenclatura abituale, dovremmo aspettarci un iPhone 7s…

Da questo punto di vista, Apple ha mantenuto l’attitudine alla segretezza cui Jobs teneva tantissimo e, come si vede, con ragione. L’[effetto Osborne](https://en.m.wikipedia.org/wiki/Osborne_effect) può essere letale per una azienda basata su pochi prodotti di eccellenza disponibili in pochi modelli e con una sola uscita annuale. Se il cinque percento degli acquirenti potenziali si lascia intortare dalle invenzioni dei siti qualunque, l’effetto delle mancate vendite inizia ad amplificarsi e a favorire una spirale negativa.

Certo, è più difficile osservare il segreto su una produzione di cinquanta milioni di unità che su cinque. A parte questo, Apple si sta comportando come avrebbe fatto Steve.

E questo è un singolo capitolo del tema. Altri nei prossimi giorni.
