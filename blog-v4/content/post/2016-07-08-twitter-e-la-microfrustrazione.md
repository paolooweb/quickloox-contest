---
title: "Twitter e la microfrustrazione"
date: 2016-07-08
comments: true
tags: [Twitter, iPad]
---
Si legge che Twitter [ha perso smalto](http://time.com/4216413/twitter-stock-user-growth-users-twtr/) ultimamente e la sua crescita è un po’ troppo lenta per i professionisti dell’analisi.

Posso dire due cose: la prima, che non sorprenderà nessuno, è la farraginosità dei suoi meccanismi (una foto ti costa x caratteri, puoi attaccare un punto finale a uno *hashtag* ma non a un *username* eccetera).

La seconda è che sono in spiaggia a sbrigare lavoro con iPad e, parola, sta andando meravigliosamente. Poi, in un impeto di vanità, faccio per cambiare la foto del mio profilo e *non posso*.

Quanto meno, non ne sono stato capace. Questo con la pagina *mobile* del sito Twitter.

*Usa la pagina desktop*, mi dice. Sono d’accordo, ma allora a che cosa serve quella *mobile*, che dovrebbe facilitare l’interazione? E se fossi su iPhone, con uno schermo ancora più sacrificato?

*Usa la app*, mi dice. Sorry, uso [Tweetbot](http://tapbots.com/tweetbot/) che è meglio. Non ho la *app* di Twitter, non intendo scaricarla, non so se permetta di cambiare la foto del profilo, mi interessa poco saperlo.

Anche perché, appunto, non sto cercando di pilotare un braccio meccanico su Marte. Sto cercando di cambiare la foto del mio profilo. L’interfaccia dovrebbe consentirlo, in modo semplice e chiaro. È la classica microfrustrazione che mi incoraggia a chiudere l’account e dimnticarmi di Twitter. Non mi sorprenderei se altri avessero lo stesso pensiero.

*[A volte sono i dettagli dell’uso di un software che fanno la differenza. È qui che [il nuovo iperlibro di Akko](https://macintelligence.org/posts/2016-06-15-un-kickstarter-per-cambiare-il-libro/) casca a fagiolo. Finanziarlo permette anche di pilotare i contenuti. Serve il [coraggio di sostenere un’idea](https://www.kickstarter.com/projects/295108589/cuore-di-mela-i-segreti-di-apple-iphone-e-mac).]*