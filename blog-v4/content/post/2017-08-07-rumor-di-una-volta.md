---
title: "Rumor di una volta"
date: 2017-08-07
comments: true
tags: [Asymco, Daring, Fireball, Gruber, Dediu, iPhone]
---
Una volta, nipotini, esisteva il giornalismo di inchiesta, di anticipazione, dei retroscena. Persone brave nello scrivere, nell’investigare, nel raccogliere dati e nell’elaborarli incontrava persone, analizzava documenti, metteva a frutto intuito e competenze e scriveva quello che gli altri non potevano sapere, o non sapevano ancora, o non avevano mai considerato. C’era anche una funzione sociale in questo, nipotini, perché una stampa capace di ragionare e investigare è un antidoto agli abusi di potere, ma ne riparleremo quando sarete più grandi.

Quell’epoca è finita quando un cinico con il fiuto per i soldi e per gli escrementi ha capito che i fatti era più comodo *inventarli* e ci si poteva anche guadagnare sopra. Poteva anche succedere che una anticipazione inventata con scaltrezza bastante corrispondesse per caso alla realtà. Allora si strombazzava per conoscenza quella che era coincidenza, per fare pensare ai creduloni di saperne una più di loro: un trucco ampiamente usato dai veggenti ciarlatani, dalle rivendite di giochi a premio e dai politici per persuadere la gente a spendere tempo e soldi ancora una vita da loro.

Cinismo, merda e soldi hanno costituito una ricetta formidabile per alimentare in anni di inciviltà un numero esagerato di siti di *rumor*, che favoleggiavano di conoscere in anticipo le caratteristiche del software e dell‘hardware di Apple.

È un‘epoca al tramonto, nipotini, perché una dieta di sterco alla fine stufa anche i palati più disposti ad accontentarsi. E difatti quei siti sempre più scrivono di altro, alla ricerca di nuovi filoni puzzolenti e malsani.

A voi, quando sarete adulti, il compito di riportare in auge quel giornalismo di una volta, che informava davvero e portava alla comunità qualcosa più di quello che costava, invece che vivere parassitando il tempo dei gonzi.

Se volete degli esempi, guardate questo articolo di Horace Dediu su [quanto costerà il prossimo iPhone](http://www.asymco.com/2017/07/31/how-much-will-the-new-iphone-cost/). Horace lavora sui dati, costruisce grafici che stordiscono per chiarezza ed evidenza e dã a ciascuno la possibilità di comprendere il meccanismo dei prezzi di iPhone, chiarissimo per chi voglia applicarsi un quarto d’ora su quella pagina.

Se non vi basta, leggete come John Gruber arriva su *Daring Fireball* a definire [le caratteristiche dello schermo del prossimo iPhone](https://daringfireball.net/2017/08/d22_display_conjecture) sulla base di codice diffuso incautamente da Apple per HomePod.

Fidatevi, nipotini, solo di chi mostra di ragionare su dati verificabili e porta conclusioni non autoevidenti, frutto di analisi e competenza. Siete giovani e la vostra dieta deve abbondare in proteine nobili, più che scarti della digestione.

Il vostro affezionato zio.
