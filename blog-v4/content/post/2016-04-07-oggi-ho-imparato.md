---
title: "Oggi ho imparato"
date: 2016-04-07
comments: true
tags: [Ctrl-F8, Mac]
---
Control-F8.

Mi evidenzia da tastiera i menu grafici di Mac, in alto a destra nella barra. Non sapevo colpevolmente che ci fosse, non mi immaginavo che ci fosse, sbagliavo e chissà quanti secondi di produttività ho perso ogni giorno a staccare le mani dalla tastiera.

È una giornata speciale.