---
title: "Le vere ragioni"
date: 2013-09-15
comments: true
tags: [Mac, Windows]
---
Sono grato a **Fabio** che mi segnala un interessante articolo di Farhad Manjoo su *Internazionale*: [Windows è morto](http://www.internazionale.it/opinioni/farhad-manjoo/2013/09/06/windows-e-morto-2).<!--more-->

Singolare perché arriva a una conclusione corretta pur procedendo spesso in base a presupposti sbagliati. Ecco qualche puntino sulle *i*.<!--more-->

>Il software aveva un volto, che restava impresso nella memoria degli utenti. Una volta imparato a usare un pc basato su Windows, si poteva usare qualunque altro pc che girasse su quel sistema operativo.

Sembra dall’analisi di Manjoo che sia stato Windows, il software, a vincere la battaglia tra i computer. Non è vero; è stato l’hardware. Windows, quando tutto si è deciso, non c’era o faceva ridere. L’evento decisivo è stato l’avvento dei *cloni*, i computer che facevano funzionare il Dos – mica Windows – esattamente come sul Pc Ibm. Questo scatenò la guerra dei prezzi e l’ubiquità del Pc. Senza i cloni, il Pc sarebbe rimasto una piattaforma proprietaria, esattamente come Apple ][ e Mac. Quando arrivò Windows 95, conobbe una enorme diffusione: su Pc che *erano già stati acquistati* senza che Windows fosse un fattore e su Pc nuovi venduti *perché erano Pc*, non perché avevano Windows! E perché avevano un prezzo stracciato.

>Come la Apple ha dimostrato, nel mondo dei dispositivi mobili l’hardware può produrre tanti profitti quanto il software.

Nell’ultimo trimestre 2012, Apple ha totalizzato da sola [quasi metà di tutti i profitti del mercato Pc](http://www.asymco.com/2013/04/16/escaping-pcs/). Vendendone il cinque percento. Apple ha dimostrato qualcosa anche nel mercato Pc.

>Forse, quando i criteri di design per realizzare un buon telefono o un buon tablet saranno noti, molte aziende li imiteranno e l’hardware diventerà nuovamente a buon mercato. È questo che non piace alla Apple.

Realmente? Abbiamo appena visto che nei Pc, dove questo accade, Apple se la cava piuttosto bene.

>A un certo punto i componenti per dispositivi mobili diventeranno tanto perfetti ed economici che uno smartphone da cinquanta dollari potrebbe funzionare bene quanto uno da cento o da duecento.

Come accade con le auto, mercato vecchio di oltre un secolo: la macchina da diecimila euro funziona bene quanto quella da ventimila. Veramente? Questa affermazione veleggia verso l’ingenuità.

>(il timore che l’hardware si riduca a merce a basso costo è il motivo per cui il valore delle azioni della Apple sta scendendo)

La discussione sul tema può essere lunga, ma certo la risposta non è così sciocca. Altrimenti dovremmo pensare che quando le azioni salgono, è perché non si ha timore che l’hardware si riduca a merce a basso costo. Le azioni, peraltro, in questo momento sono più o meno stabili. Le paure si sono livellate? Via, in gioco c’è altro.

>Il sistema operativo per dispositivi mobili di Google (basato su Linux, il sistema open source che nei sogni dei suoi ammiratori avrebbe annientato Windows) è gratuito. Qualunque produttore di telefoni cellulari può usare e modificare Android a suo piacimento. Questo spiega la sua quota di mercato, pari quasi all’80 per cento, stando ai dati della International Data Corporation.

Non ci capiamo. Android è diventato ubiquo perché era *l’unico sistema operativo disponibile*, non perché era gratuito! Microsoft, sorpresa dall’improvvisa obsolescenza di Windows Mobile, ha avuto *niente da offrire* per oltre un anno. Apple non concede in licenza iOS. C’era solo Android. Che, se fosse stato venduto in licenza, sarebbe stato adottato ugualmente. La prova? Praticamente tutti i costruttori pagano moneta sonante a Microsoft per ogni apparecchio Android che vendono, allo scopo di evitare cause giudiziarie per questioni di brevetti. Android *non è gratuito*!

Detto ciò, è un buon articolo, che merita la lettura. Solo qualche precisazione.