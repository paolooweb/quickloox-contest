---
title: "Viva la differenza"
date: 2020-10-03
comments: true
tags: [Biden, Arial, Helvetica, Gruber, Nyt, Times]
---
Impossibile superare [la sintesi e la precisione](https://daringfireball.net/linked/2020/10/02/when-the-clients-specs-arial) di questo commento di John Gruber ispirato da un [articolo del *New York Times*](https://www.nytimes.com/2020/09/28/us/politics/presidential-debate-joe-biden.html), per cui lo traduco e va bene così.

>Quando qualcuno dice di preferire Arial, intende dire che gli piace Helvetica e non conosce [la differenza](https://www.swiss-miss.com/2009/09/arial-versus-helvetica.html). Qualsiasi designer degno del proprio titolo, se gli chiedono Arial, usa Helvetica.

*Ipse dixit*.