---
title: "Semplice e inedito"
date: 2019-12-04
comments: true
tags: [Apple, Must, Opzione, Option, Alt]
---
Ho una bassa opinione degli elenchi di trucchi, salvo eccezioni: un elenco con un tema elementare, capace di insegnarmi qualcosa che ancora non conosco, è degno di nota.

Come questo articolo di Apple Must sull’[uso del pulsante Opzione](https://www.applemust.com/mac-users-how-the-option-button-can-save-your-day/). Due dritte che ho trovato inedite in un elenco nutrito e nemmeno tanto marginali. Bravi.
