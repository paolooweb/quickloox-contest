---
title: "Secondo stadio"
date: 2020-03-25
comments: true
tags: [Arbasino, Skype, Winer]
---
È scomparso da poco [Alberto Arbasino](https://www.ilsole24ore.com/art/alberto-arbasino-ultimo-gran-lombardo-ADlLOIF). Della sua sterminata produzione voglio ricordare un suo aforisma:

>La carriera dello scrittore italiano ha tre tempi: brillante promessa, solito stronzo, venerato maestro.

Prendiamo ora Skype. La citazione, stavolta, [arriva da Dave Winer](http://scripting.com/2020/03/23.html#a015019):

>Quelli che fanno Skype dovrebbero guardare la televisione via cavo. Che cosa c’è che non funziona? Perché così tanti esperti intervistati in remoto appaiono e si sentono così da schifo? Andiamo, si può fare meglio!

Alla sua comparsa, Skype aveva fatto rumore con un approccio innovativo che decentralizzava la rete e lo rendeva formidabile, in prestazioni e possibilità.

Poi i soliti noti [hanno allungato le mani](http://ww.macintelligence.org/blog/2014/05/14/non-ci-parlo-piu/) e Skype ha perso il suo gioello, l’architettura. È stato scientemente reso [intercettabile](https://macintelligence.org/posts/2012-07-24-manco-a-dirlo-a-voce/) e la qualità, beh, si legga Winer che non può essere tacciato di fanatismi in questo senso.

Il primo stadio *à la Arbasino* è passato. Ho l’impressione che si resterà indefinitamente al secondo.