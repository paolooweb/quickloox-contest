---
title: "Gli assenti hanno torto"
date: 2020-01-05
comments: true
tags: [Mit]
---
Altra perla dall’[articolo di Mit Technology Review dedicato al digitale che penalizza gli studenti](https://macintelligence.org/posts/2019-12-29-una-scuola-a-due-piani/):

>Secondo altri studi, gli studenti di college negli Stati Uniti che hanno usato laptop o apparecchi digitali nelle loro classi hanno fatto peggio al momento degli esami.

Ohibò, pare grave. Sembrerebbe che sia meglio fare a meno del computer se si vuole avere successo all’università. Proviamo a continuare la lettura:

>Gli studenti che hanno sostenuto Algebra I online hanno fatto molto peggio di quelli che hanno frequentato il corso in persona.

Senza avere condotto studi autorevoli, azzardo che chi ha studiato solo sulle fotocopie, senza mai mettere piede in aula, abbia fatto peggio di chi ha frequentato.

Oppure si vuole dimostrare che un corso online non porta gli stessi risultati dell’insegnamento diretto. E grazie, c’era bisogno dello studio? Peraltro, in uno scenario del genere l’apparecchio digitale è unicamente uno strumento di visione. Se il corso fosse stato erogato per televisione, sarebbe uguale.

Già detto e da ripetere: oggi il vento è mutato e il presupposto dichiarato è che la tecnologia sia dannosa, ovunque. Se finora ci sia stato eccessivo entusiasmo verso la tecnologia, o le aziende abbiano portato molta acqua al loro mulino a volte per aumentare i fatturati senza troppo rispetto del contesto, male, sono squilibri che è giusto sanare (parliamo di tante scuole che usano il digitale, cioè si sono fatte regalare Office o quasi, e si considerano a posto…). Eccedere sull’altro versante è ugualmente sbagliato.

Giusto per una riflessione più circostanziata sul tema, i [risultati dei test scolastici americani dal 1972 al 2019](https://blog.prepscholar.com/average-sat-scores-over-time) danno l’idea di risultati altalenanti ma nel complesso stabili.

Se è vero che la grande maggioranza delle scuole ha adottato strumenti tecnologici per studiare, e questi creano danno, perché non si vede? Possiamo chiederci magari con più profitto perché non siano decollati. Ma torniamo a quello che si diceva l’altra volta: strumenti digitali di cattiva qualità danno cattivi risultati, così come il loro cattivo impiego, così come il loro impiego da parte di insegnanti ostili, impreparati o ignoranti, così come la loro somministrazione passiva a ragazzi cui invece andrebbe insegnato, esattamente come si insegna a leggere un testo, o scrivere una ricerca. 