---
title: "Pochissimo tempo"
date: 2013-12-21
comments: true
tags: [iPhone, Evans]
---
Devo correre e mi scuso. Al volo, segnalo questo bell’[articolo di Benedict Evans](http://ben-evans.com/benedictevans/2013/12/8/who-buys-the-iphone-5c) su chi compra iPhone 5C.

Se ne parla come di un insuccesso, invece si rivolge a fasce di persone ben precise.

Una vista più colorata di quello che sembra, insomma.