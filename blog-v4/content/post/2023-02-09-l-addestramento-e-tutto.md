---
title: "L’addestramento è tutto"
date: 2023-02-09T00:58:33+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [Bing, ChatGPT, Large Language Model, TechCrunch]
---
La grande novità del nostro tempo: la diffusione di massa dei sistemi di generazione di contenuto basati su Large Language Model (LLM). Questi modelli, immense masse di dati, vengono dati in pasto ad agenti software che vengono addestrati a riconoscere schemi presenti nei dati stessi. Su richiesta, il sistema elabora contenuti generati a partire dagli schemi riconosciuti grazie all’addestramento.

Rispetto a quello che si faceva con i sistemi esperti trent’anni fa è cambiato tantissimo in termini di quantità dei dati e velocità di elaborazione, ma una cosa resta fondamentale, alla base di tutto: l’addestramento.

Un’altra novità è che, a volte, il sistema reagisce in modi inaspettati, non previsti. Come questo, che inizia così…

[I download della app Bing crescono di dieci volte dopo le notizie di Microsoft sull’intelligenza artificiale](https://techcrunch.com/2023/02/08/bings-previously-unpopular-app-sees-a-10x-jump-in-downloads-after-microsofts-ai-news/) (TechCrunch).

…con il reclutamento degli agenti software da addestrare.

E poi…

[L’intelligenza artificiale mangia sé stessa: l’intelligenza artificiale di Bing cita disinformazione sul Covid acquisita da ChatGPT](https://techcrunch.com/2023/02/08/ai-is-eating-itself-bings-ai-quotes-covid-disinfo-sourced-from-chatgpt/).

…gli agenti software, naturalmente stupidi, vengono resi artificialmente intelligenti grazie all’addestramento. Che resta alla base di tutto.