---
title: "A lunghissimo termine"
date: 2023-10-02T15:01:50+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [Mastodon, Twitter, X, TechCrunch]
---
Mi dicevano che avrebbe sostituito Twitter, anzi, X, e scopro che *TechCrunch* suona le trombe per la crescita di Mastodon, giunta nel 2022 a ben [un milione e ottocentomila utenti attivi](https://techcrunch.com/2023/10/02/amid-twitter-chaos-mastodon-grew-donations-488-in-2022-reached-1-8m-monthly-active-users/).

Certo, uno è commerciale, l’altro è autogestito. Ma uno, per quanto stia perdendo utenti, [si trova abbondantemente sopra i trecento milioni di utenti](https://www.insiderintelligence.com/press-releases/twitter-will-lose-more-than-32-million-users-worldwide-by-2024-amid-turmoil/). L’altro è cresciuto in donazioni e donatori, in modo anche salutare, per carità. Ma come pubblico globale, vale un centosettantesimo dell’uno.

Non vedo un futuro luminoso per X, in questo momento. Ma a parlare di Mastodon come alternativa a livello di numeri, a questo ritmo, aspetterei quei venti-venticinque anni.