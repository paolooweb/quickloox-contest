---
title: "Scuole di pensiero"
date: 2020-10-26
comments: true
tags: [Mathematica, Wolfram, Estonia, Cambridge]
---
È veramente difficile pensare alla nostra scuola in questi giorni senza sentire voglia di fare polemica. A causa del coronavirus sono rimaste chiuse per un quadrimestre, durante il quale abbiamo sentito alla nausea le geremiadi su come fosse brutta la teledidattica e quanto fosse indispensabile riaprire le scuole.

Uno che avesse detto una parola su quello che doveva cambiare, prima di riaprire. L’inadeguatezza degli spazi e delle strutture; il virus della burocrazia, che non uccide nessuno ma mortifica tutti; la necessità di ripensare radicalmente programmi e dinamiche della scuola, che continua a funzionare come nell’Ottocento, solo che siamo nel Duemila.

Contava riaprire e basta. Anzi, contava riaprire *esattamente come prima*. Ci voleva poco a capire che, come la scuola di prima ieri ha dovuto chiudere, la scuola di prima oggi è candidata a rifarlo. Volevano riaprire le scuole in presenza per esorcizzare il demone della teledidattica, che non capiscono e non sanno praticare. Hanno ottenuto quello che volevano e ora riavranno la teledidattica come conseguenza della loro inettitudine e di quella del governo, il che dice molto sulle rispettive intelligenze.

Andiamo a respirare piuttosto un po’ di aria fresca, grazie alla [visita in Estonia del team di Cambridge Mathematics](https://www.cambridgeassessment.org.uk/insights/mathematics-education-estonia-style/) per vedere come fanno da quelle parti a insegnare matematica.

>Nelle tre classi che abbiamo visitato, ci siamo concentrati sull’uso della tecnologia digitale. In due di esse consisteva nell’uso di iPad; ogni ragazzo ne aveva uno, sapeva usarlo bene e l’apparecchio era realmente incorporato nella lezione, impiegato soltanto quando c’era una buona ragione per farlo. Gli insegnanti ci hanno parlato con entusiasmo delle possibilità offerte dall’informatica e in tutte le lezioni che abbiamo seguito questa è stata alternata con altre attività complementari, per esempio l’origami. Nella terza classe che abbiamo visitato, abbiamo osservato come funziona la Computer Based Maths. È un progetto centrato sulla statistica, progettato congiuntamente dagli specialisti estoni in insegnamento della matematica e [Wolfram](https://www.wolfram.com). Per quanto sia una parte molto piccola del curriculum estone, volevamo vedere come il docente riusciva a tenere unita una classe con capacità miste e se il software [Mathematica](https://www.wolfram.com/mathematica/) supportava un’esperienza di soluzione problemi sia realistica sia efficace, due questioni per noi interessanti visto che a Cambridge abbiamo esplorato un progetto simile, solo concentrato sulla geometria.

Niente di che. Scambio culturale, apertura a strumenti nuovi, sperimentazione ragionevole, progetti concreti. Basta poco per sentirsi un altro pianeta.

>Mentre noi ci preoccupiamo che ogni classe abbia la propria lavagna interattiva, in Estonia hanno il videoproiettore, ma preferiscono inviare i contenuti su apparecchi portatili individuali.

E infine:

>[I docenti estoni] sono ben preparati […] e liberi di prendere le proprie decisioni in base alla materia e alla loro conoscenza degli studenti. […] C’è una forte enfasi sulla creatività del curriculum e sull’educazione culturale, vista come assolutamente essenziale.

Non so se mostrare che cosa succede non lontanissimo da noi sia fare polemica. Di certo, mi sembra di intravedere un modello più resistente a problematiche come quelle attuali.