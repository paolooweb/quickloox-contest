---
title: "Contano i tasti, più che le dimensioni"
date: 2014-07-17
comments: true
tags: [Gemmell, Air, MacBookAir, BBEdit, Alfred, Scrivener, Mail, Spazi, Photoshop, Dock, Moom, StickyNotifications, SelfControl]
---
All’inizio sono rimasto scettico di fronte all’affermazione di Matt Gemmell per il quale [il computer perfetto è un MacBook Air 11”](http://mattgemmell.com/small-screen-productivity/).<!--more-->

Poi ho letto il pezzo e ho capito. Gemmell ha un Air pesantemente personalizzato:

* La finestra di [BBEdit](http://barebones.com/products/bbedit/) è su misura.
* La finestra di [Scrivener](https://www.literatureandlatte.com/scrivener.php) è su misura.
* La finestra di [Photoshop](http://www.adobe.com/products/photoshop.html) è su misura.
* Lavora in modalità a tutto schermo.
* Fa pieno uso degli Spazi (le scrivanie virtuali).
* Usa Mail a finestra massimizzata ma non a tutto schermo (c’è una ragione specifica).
* Usa pochissimo il Dock, sempre nascosto.
* Ha impostato il pieno accesso della tastiera a tutti gli elementi dell’interfaccia.
* Usa [Alfred](http://www.alfredapp.com/).
* Usa [Moom](http://manytricks.com/moom/).
* Usa [Sticky Notifications](http://instinctivecode.com/sticky-notifications/).
* Usa [Self Control](http://selfcontrolapp.com/).

La sua teoria è che usando il mouse si percepiscono i confini fisici dello schermo, mentre usando la tastiera il più possibile – persino per spostare le finestre! – si percepiscono solo confini logici, molto più ampi di quelli dello schermo materiale.

La spiegazione psicologica potrebbe avere una base, anche se ne farei più una questione di controllo: spostare il mouse costringe a colpire continuamente bersagli, dare comandi di tastiera fa accadere le cose come per magia.

La vera lezione tuttavia è che dovremmo sforzarci per fare del nostro Mac il migliore ambiente di lavoro possibile per le sue possibilità e per le nostre necessità. Impostare, configurare, memorizzare costa tempo e fatica. Però non è una spesa, ma un investimento. 