---
title: "Trasmissione a catena"
date: 2016-09-01
comments: true
tags: [Transmission, BitTorrent]
---
Sono grande fan di [Transmission](https://transmissionbt.com/) in quanto *client* BitTorrent piccolo, comodo, funzionale, veloce, intelligente.<!--more-->

Purtroppo è già la seconda volta che [viene infettato da *malware*](https://transmissionbt.com/keydnap_qa/). La [prima](https://macintelligence.org/posts/2016-03-10-anello-debole/) non è neanche tanto lontana nel tempo.

La buona notizia è che gli unici Mac a rischio sono solo quelli che hanno scaricato la versione 2.92 tra il 28 e il 29 agosto. Il *link* qui sopra rimanda anche alla pagina con le istruzioni del caso. Per il resto, non ci sono problemi. Ma si ricordi che la versione infetta è pericolosa proprio perché l’intrusione nel sito ha permesso di fare sì che Mac autorizzi l’esecuzione del programma, pensando sia una versione genuina.

Prima riflessione: evidentemente le falle nel sito di Transmission erano più di quelle che sembravano (sono state usate per sostituire la copia sana con quella infetta e aspettare che quest’ultima venisse distribuita). Per fortuna quelli di Transmission hanno capito l’antifona e hanno spostato tutto il codice su GitHub, che a oggi è esemplare per sicurezza. Un terzo incidente di questo tipo non potrà verificarsi. Non di questo genere, almeno.

Seconda riflessione: questi attacchi a catena contro un ottimo *client* BitTorrent fanno capire che gli utenti BitTorrent sono particolarmente appetiti dai fabbricanti di *malware*. Sono persone che a volte scaricano software in modo indiscriminato, qualunque film o album musicale purché sprotetto eccetera. Ovvero, sono particolarmente suscettibili ad attacchi e infezioni.

C’è un modo più sano di usare BitTorrent, che pirateria a parte ha mille applicazioni utili. Per esempio, LibreOffice si scarica via BitTorrent in pochi secondi dove una normale Adsl asfittica italiana vuole anche qualche minuto. Consiglio di usare il più possibile BitTorrent solo quando conveniente e necessario e solo per attività legali. A impiegarlo poco e bene, si corrono molti meno rischi e si vive più tranquilli.