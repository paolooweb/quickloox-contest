---
title: "Più free che form"
date: 2023-02-14T00:11:40+01:00
draft: false
toc: false
comments: true
categories: [Internet]
tags: [A2, Marin, Roberto Marin, Filippo Strozzi, Strozzi, Freeform]
---
Che bello ritrovarsi con [Filippo e Roberto](https://a2podcast.fireside.fm) a registrare un altro podcast! Avevamo una scaletta articolata e minuziosa, che abbiamo fatto saltare dal minuto uno. Un’ora e mezza passata a ricordarci le cose che avevamo capito, scoprirne di nuove in diretta, approfondirne altre sempre in diretta, tutto il pretesto di sviscerare [Freeform](https://www.apple.com/it/newsroom/2022/12/apple-launches-freeform-a-powerful-new-app-designed-for-creative-collaboration/).

Eccome se lo abbiamo sviscerato, al punto che ho potuto sollevarmi [un paio di pesi dal petto](https://macintelligence.org/posts/2022-12-16-appena-nato-libero/) riguardo le sue funzioni pensando di essere aggiornato (misteriosamente corroborato in questo dalle Impostazioni di macOS) mentre in effetti non lo ero.

Il tempo per l’editing e per riportare l’ordine nel caos sonoro e ne verrà fuori una nuova puntata. Da cui spero traspariranno suggerimenti e dritte utili per usare con profitto Freeform. Se poi traspirasse anche una frazione del calore, dell’allegria e del gusto di stare insieme a chiacchierare di Apple e affini, ne andrei ancora più orgoglioso.

E poi abbiamo pensato a un tema di una futura altra puntata… per parafrasare la barzelletta, a momenti per farci smettere di parlare dovevamo abbatterci a vicenda.

Insomma, devo andare a dormire e non ne ho voglia. Quanto meno devo fare un paio di prove in Freeform, e poi aggiornare il macOS malandrino che ha sostenuto di essere aggiornato per tutta la puntata, fino a propormi l’update non appena ho finito di consegnare la mia traccia audio.

Alla prossima. È stato bello.