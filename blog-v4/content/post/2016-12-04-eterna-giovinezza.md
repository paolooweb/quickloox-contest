---
title: "Eterna giovinezza"
date: 2016-12-04
comments: true
tags: [TechCrunch, tv, Roku, Jobs, iPhone, Android, Microsoft]
---
Si sentiva la mancanza di un articolo come [Fine della dinastia Apple?](https://techcrunch.com/2016/11/29/the-end-of-the-apple-dynasty/) e *TechCrunch* ci è venuto incontro, prima che finisse l’anno.<!--more-->

Una raccolta parziale delle idee assolutamente originali, inedite e nuove contenute nell’articolo.

* Apple sta giocando male le carte che gli ha lasciato Steve Jobs.
* I telefoni Android hanno specifiche superiori a quelle di iPhone (come accade, se non è Android è qualcun altro, dal 2007).

>La gente si attacca ai propri vecchi apparecchi Apple nel terrore di “che cosa faranno la prossima volta?” invece che scrivere assegni in bianco per “che cosa faranno la prossima volta?”

* iPhone (che ottiene [più del cento percento dei profitti](https://macintelligence.org/posts/2016-11-15-fatto-cento-facciamo-centoquattro/) sul mercato) non ha abbastanza quote di mercato.
* Apple TV non ha risoluzione 4k e [Roku](https://www.roku.com/index) sì (disordini a Cupertino e nelle principali capitali europee; proclamato lo stato di emergenza planetario).

>Predico che Microsoft si muoverà ambiziosamente nello spazio dei telefoni cellulari.

Pezzi così vanno auspicati con buona frequenza, perché sono il migliore viatico alla prosperità e alla rilevanza di Apple. Qualunque elisir di giovinezza impallidisce al confronto.