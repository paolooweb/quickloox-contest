---
title: "Puzza di bruciato"
date: 2022-08-26T01:29:19+01:00
draft: false
toc: false
comments: true
categories: [Hardware]
tags: [Guardian, M2, MacBook Air M2, MacBook Air]
---
Il *Guardian* ha preso in esame MacBook Air M2 e ne fa [una recensione](https://www.theguardian.com/technology/2022/aug/15/apple-macbook-air-m2-review-chip-battery) interessante perché densa e compatta, veloce da leggere e completa.

La sintesi non fa giri di parole:

>Il miglior laptop consumer premium disponibile.

La cosa ancora più interessante è che il *Guardian* non è una testata specializzata, quindi niente marchette e niente secondi fini; al tempo stesso è scritto da gente capace, differentemente da altre testate di cui per carità di patria si tace.

È del tutto normale che trovi dei *contro*; non si può piacere a tutti e anche MacBook Air M2 ha i suoi difetti. Secondo la recensione sono questi:

>Costoso, solo due porte USB-C, nessuna porta USB-A, nessuno slot per schede SD, può pilotare un solo monitor esterno, assenza di fotocamera Centre Stage, niente Face ID.

Ma non doveva [scaldare](https://macintelligence.org/posts/2022-07-23-ondate-di-calore/), questo portatile? La sensazione che la puzza di bruciato sia da ascrivere a questioni diverse dal processore si fa sempre più intensa.