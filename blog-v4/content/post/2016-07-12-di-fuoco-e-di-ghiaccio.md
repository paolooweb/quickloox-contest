---
title: "Di fuoco e di ghiaccio"
date: 2016-07-12
comments: true
tags: [7DRL, roguelike, FireTail, Shadow, Desert, Force, Nature, Trapped, Heart]
---
Non c’è estate senza *roguelike*. L’anno scorso però mi sono dimenticato di citare [FireTail](http://7drl.org/2015/03/16/firetail-success/), che presenta più di un livello di originalità, dalla pianta esagonale al tema manicheo – fuoco contro ghiaccio – che funziona benissimo nel gioco e nella grafica.

Si scarica tranquillamente per Mac e disgraziatamente lo stesso non si può dire per la maggioranza dei prodotti dell’annuale concorso Seven-Day Rogue-like o [7DRL](http://7drl.org/), nel quale i programmatori in gara devono appunto scodellare un (buon) gioco in tema nel giro di sette giorni. Non è banale.

L’edizione di quest’anno [ha prodotto bene](http://roguetemple.com/7drl/2016/), anche se appunto trattandosi di oggetti spesso messi assieme di fretta, spetta sovente a chi scarica riuscire a farli girare.

Uno dei primi in classifica, giocabile anche nel browser, è [Force of Nature](https://evgeniipetrov.itch.io/force-of-nature), la sfida di un albero contro troll boscaioli e incendiari. Ci sono elementi interessanti come il terreno che cambia stato secondo gli eventi che accadono e le meccaniche di attacco.

[The Only Shadow That the Desert Knows](http://humbit.com/shadow/) si distingue per l’uso dei viaggi nel tempo ed è scaricabile in versione Mac.

[The Trapped Heart](https://darrengrey.itch.io/the-trapped-heart) è anch’esso scaricabile in versione Mac e offre una mappa esagonale abbinata a combattimenti più interessanti del solito: i nemici cadono subito, ma dispongono di scudi direzionali che bisogna aggirare con sagacia.

La [classifica](http://roguetemple.com/7drl/2016/) contiene numerose altre gemme alla cui scoperta invito ogni curioso. Un *roguelike* non è per tutti e lo dico in senso positivo. È anche possibile compensare a piacere lo sforzo degli sviluppatori e qualsiasi cifra, anche irrisoria, viene percepita e contribuisce a un mondo più giusto anche se non necessariamente migliore.

*[L’ultima considerazione si applica appieno anche all’[iniziativa di Akko](https://www.kickstarter.com/projects/295108589/cuore-di-mela-i-segreti-di-apple-iphone-e-mac) per creare un libro sull’ultimo sistema operativo Mac che vada oltre Mac, oltre l’ultimo e oltre il libro.]*
