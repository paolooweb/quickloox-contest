---
title: "Senza margini"
date: 2015-06-14
comments: true
tags: [Menlo, DejaVu, font, Vera, Drang]
---
Mettersi di traverso a Dr. Drang presenta sempre un altissimo rischio di rivelarsi al mondo come cretino patentato. D’altronde nel mio caso si tratterebbe magari della duecentoventesima volta, quindi tanto vale.<!--more-->

Drang ha riveduto un suo vecchio *post* che [ripercorre le origini di Menlo](http://www.leancrew.com/all-this/2009/10/the-compleat-menlovera-sans-comparison/), il font monospaziato di Apple, con la consueta completezza e capacità. Menlo è la rielaborazione di [DejaVu](http://dejavu-fonts.org/wiki/Main_Page) Sans Mono, a sua volta un ampliamento di [Vera Sans Mono](http://www.fontsquirrel.com/fonts/bitstream-vera-sans-mono). Nel *post* sono incluse due comparazioni grafiche e un elenco delle variazioni.

Si può solo concordare con le due cose che di Menlo non gli piacciono: i trattini sono di lunghezza poco distinguibile e anch’io soffro nel fare faticare più del dovuto a distinguere il segno meno dal trattino *en* dal trattino *em* (quelli che dovrebbero essere larghi uno come la enne e uno come la emme). Onestamente lo zero barrato anziché puntinato non mi crea disturbo, però Drang stesso adduce ragioni estetiche e non c’è niente da dire. Invece, la chiusura del vecchio pezzo, che poi è quella del nuovo:

>Ho l’impressione che gli utenti Mac si stiano fissando sulla nozione che Menlo sia una versione di Vera Sans “sistemata” da Apple. È un’idea che puzza di tifoseria. Uso Vera Sans Mono da quando è stato presentato. Dopo i miei dati, è la prima cosa che ho portato su Mac quando ho compiuto il passaggio da Linux. Ho sempre usato Vera Sans (o DejaVu) sul Terminale; lo stesso su Mail, BBEdit e, in seguito, TextMate. In breve, Vera Sans Mono è un grande font e lo è sempre stato. Ecco perché Apple lo ha scelto come base per Menlo. Se dobbiamo parlare di miglioramenti apportati da Apple, sono al massimo marginali.

Non ho sentito nessuno, nel mio minuscolo, dire che Apple abbia sistemato Vera Sans o DejaVu con Menlo. E non conta. Sarebbe comunque una scempiaggine, ma proprio perché quando lavori su un font – soprattutto su un ottimo font alla partenza – puoi certamente fare danni o risultare inutile. Per portare veri miglioramenti devi essere davvero bravo e, comunque, ecco che cosa volevo dire, *potranno essere solo marginali*. Perché la tipografia è fatta di micropunti, settantaduesimi di pollice, variazioni impercettibili, ritocchi invisibili a qualsiasi persona non seriamente interessata e concentrata.

Un font moderno, con Unicode e le sue varianti interne, oggi può contenere migliaia di glifi e varianti, la più parte generata automaticamente perché un essere umano difficilmente riuscirebbe a creare più di due o tre font moderni completamente a mano in tutta una vita. E dai tempi della prima LaserWriter sono nati migliaia, forse decine di migliaia di font. Fare sensazione, inventare qualcosa di veramente nuovo, rivoluzionare il campo è impossibile a molti anni.

Non ho serie competenze per dire se Apple abbia complessivamente migliorato o peggiorato le cose con Menlo rispetto a DejaVu Sans Mono. Sicuramente, quale che sia il risultato, è marginale. Non perché conti poco. In tipografia, spesso *marginale* significa *cura maniacale del particolare*.