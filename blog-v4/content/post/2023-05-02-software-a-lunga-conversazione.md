---
title: "Software a lunga conversazione"
date: 2023-05-02T02:38:49+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [Carmack, John Carmack, Doom, Quake, Steam, Quake III Arena, Gog]
---
Che cosa penserà del [problema della conservazione del software](https://twitter.com/ID_AA_Carmack/status/1651597032778264578) John Carmack, creatore di *Doom* e *Quake* e guru dello sviluppo di videogiochi?

<blockquote class="twitter-tweet"><p lang="en" dir="ltr">Last night I downloaded Quake III Arena on Steam. I always looked back at it as my favorite Id game, but it had been many years since I had actually played it. There are a few polish things that I would nitpick today, but it was like dropping a quarter in a classic arcade game.…</p>&mdash; John Carmack (@ID_AA_Carmack) <a href="https://twitter.com/ID_AA_Carmack/status/1651597032778264578?ref_src=twsrc%5Etfw">April 27, 2023</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

>Ieri sera ho scaricato Quake III Arena da Steam. L’ho sempre considerato il mio gioco Id preferito, ma era da molti anni che non facevo una partita. Oggi sistemerei qualche piccolo dettaglio, ma è stato come infilare una moneta in un arcade game dei bei tempi…

Sembra che il codice di embed salti un paio di paragrafi alla fine del tweet. Li traduco.

>Ho giocato i primi due livelli contro i bot con il sorriso in faccia. Sì, sono ancora orgoglioso del mio gioco.

>Lodevole che l’ecosistema del PC permetta facilmente di giocare a un titolo del 1999 in modo che “everything just works”.

Ecco, ci siamo fatti un milione di domande su che cosa sarebbe successo quando i CD-ROM avrebbero smesso di funzionare, i vecchi sistemi operativi sarebbero stati incompatibili con il nuovo hardware, i nuovi giochi avrebbero mandato quelli vecchi nel dimenticatoio con conseguente perdita di materiale storico e culturale.

In effetti abbiamo soltanto condotto grandi quantità di conversazione sostanzialmente inutile. Dove non c’è Steam c’è [Gog](https://www.gog.com), dove non c’è Gog c’è L’[Internet Archive](https://archive.org) e via dicendo.

Ora qualcuno chiederà *e quando Gog smetterà di funzionare? Se domani il cloud si interrompe per qualche ragione?*

Interessantissimo, però sono – aggiornate – le stesse domande di un quarto di secolo fa e intanto sono passati venticinque anni. È ancora una conversazione con un senso?