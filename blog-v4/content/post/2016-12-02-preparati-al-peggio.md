---
title: "Preparati al peggio"
date: 2016-12-02
comments: true
tags: [Milunovich, Jobs, Ive, design, netbook, Ford, jack, Blu-ray]
---
L’[intervista di Business Insider all’analista Steve Milunovich](http://www.businessinsider.com/interview-steve-milunovich-ubs-future-apple-2016-11?r=US&IR=T) sul futuro di Apple, sulla strategia a lungo termine, sulle capacità di Tim Cook, sulla possibilità che Jonathan Ive lasci il posto di responsabile del design, è lunghissima, molto ben fatta, e vuota. Aria fritta.

C’è una ragione fondamentale: basta guardare la faccia di Milunovich all’inizio dell’articolo.

Scherzo. Tuttavia una ragione fondamentale c’è. Per parlare di Apple (villaggi vacanze, carico fiscale, muretti a secco, patologie dell’alluce, viaggio interstellare, transustanziazione) bisogna essere preparati.

Parlare di Apple ha una difficoltà in più, perché significa parlare del lavoro di altre persone. Oltre a essere preparati, bisogna esserlo almeno quanto loro.

Quanto sono preparati? Difficile saperlo. Non sappiamo in base a quali dati si formano un’opinione e prendono delle decisioni, né con quanta efficacia vengano usate le informazioni in loro possesso. Ugualmente ignoriamo i vincoli e le problematiche interne che potrebbero alterare il corso di una decisione presa.

Una cosa è certa, però: le critiche ad Apple arrivano per il novantanove percento a posteriori, a lavoro fatto. Quando si è diffusa la voce che iPhone 7 avrebbe rinunciato alla porta audio, stracciamento di vesti. Fino a che qualcuno ha fatto notare che le [vendite di auricolari *wireless*](https://www.npd.com/wps/portal/npd/us/news/press-releases/2016/bluetooth-capable-headphone-sales-surpass-non-bluetooth-sales/) avevano la maggioranza assoluta.

Sfido chiunque a esibire un articolo sul tema *secondo me il prossimo iPhone 7 dovrebbe fare a meno del jack audio*. Non c’è. Tutti quelli che discettavano di iPhone 7 non sapevano che cosa acquista la maggioranza del pubblico. Scommetto un centesimo di euro contro un fondo di bottiglia che in Apple lo sapevano e da un bel pezzo.

Quando si parla di quello che dovrebbe fare Apple è difficilissimo che si vada oltre il mitico cliente di Henry Ford e la sua ipotetica richiesta: *un cavallo più veloce*. Steve Jobs era contrario ai *focus group* perché il pubblico pensa in termini di quello che già c’è, certo più grosso, certo più veloce. Ci fosse stato uno che avesse illustrato l’idea di iPhone prima del 2007. Il pubblico vuole semplicemente continuare a fare quello che fa ed è tendenzialmente incapace di produrre una vera novità. Nessun *focus group* ci avrebbe dato Macintosh.

A sentire la gente, Apple doveva [produrre un netbook](https://macintelligence.org/posts/2009-10-24-in-attesa-del-notbook/). È stata in ritardo su Usb 3.0, poi – imperdonabile – non ha messo lettori Blu-ray nei Mac. E indietro, o avanti se si preferisce, con il lettore di floppy, la porta modem, la tastiera su iPhone, ora il jack audio ed è un disastro che non si possa collegare un disco rigido a iPad per spostare i file avanti e indietro, e come faccio con la chiavetta. Intanto gli sviluppatori sono stati abbandonati per strada e, signora mia, il Mac non è più quello di una volta.

Riapro gli occhi oggi, con Usb-C, un miliardo di apparecchi iOS, il cloud sempre più importante, il *wireless* sempre più decisivo e tanto spazio e tempo sprecato ad abbaiare alla luna da parte di chi aveva i piedi così ben piantati a terra da non vedere che i cieli mutavano. Oppure, semplicemente, era preparato al peggio per affrontare l’argomento.