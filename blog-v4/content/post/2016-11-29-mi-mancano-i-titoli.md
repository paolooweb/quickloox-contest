---
title: "Mi mancano i titoli"
date: 2016-11-29
comments: true
tags: [Wiggins, Fcpx, Macbook, Pro]
---
Titolo di un recente articolo di Adam Engst: [Comprendere la marginalizzazione di Mac in Apple](http://tidbits.com/article/16914).

Citazione da un recente articolo di Peter Wiggins, [Una settimana di montaggio video con i nuovi MacBook Pro e Final Cut Pro X](http://www.fcp.co/final-cut-pro/articles/1891-a-week-editing-with-the-new-macbook-pros-and-final-cut-pro-x), pubblicato su un sito che si definisce *la risorsa numero uno per Final Cut Pro* e magari non sarà vero, però insomma, qualcuno che ci svolge del lavoro sopra ci girerà pure intorno.

>Con un pizzico di aritmetica elementare si vedrà che il nuovo MacBook Pro 15” è veloce [con Final Cut Pro X] quasi il doppio del modello del 2012.

Dovrei riuscire a tenere insieme l’idea che Apple sta marginalizzando Mac e per effetto di questa scelta i nuovi Mac portatili vanno (quasi) il doppio di quelli precedenti. Non so come fare.

(Se qualcuno pensa che sia solo questione di cambiare processore e viene da sé, sta fresco).