---
title: "Come un ombrello su una macchina da cucire"
date: 2021-02-09T02:39:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [Franco Battiato, Come un ombrello su una macchina da cucire, Pdf, Adobe, Word, zip, Pages] 
---
A un certo punto arriva comunque quello che *deve modificare il file Pdf*. Un po’ come al ristorante quando vede il pesce spada grigliato e chiede di averlo come sashimi.

In qualche modo, con [LibreOffice](https://www.libreoffice.org), si riesce anche a fare qualcosa di buono se la codifica del Pdf è misericordiosa a sufficienza e ci risparmia i problemi di font. Sono rimasto sorpreso dalla bontà della funzione, che ovviamente fa quello che può; non era scontato, comunque. Rispetto a [Inkscape](https://inkscape.org), che può fare le stesse cose, è un bel po’ avanti.

Quello della modifica, però, vuole di più. Per modificare il Pdf vuole convertirlo in Word. Vuole avere il sashimi dello spada grigliato dentro la cotoletta impanata.

Qui ho scoperto che, tra i millanta servizi gratuiti su web che promettono di farlo, [ci sono anche quelli di Adobe](https://www.adobe.com/acrobat/online/pdf-to-word.html). Convertire un Pdf in Word è assurdo in partenza ma, se proprio bisogna, almeno facciamolo con quelli che lo hanno inventato. Il bello è che ero rimasto ai tempi della bisnonna e pensavo fosse obbligatorio passare da Acrobat a pagamento.

Ma quello della modifica ha come obiettivo ultimo la conquista della galassia. Vuole usare Word *per distribuire file .zip*. Affascinante e, appunto, fuori posto come un ombrello su una macchina da cucire.

Un file Word, come un file Pages, è un realtà un file .zip con una estensione ingannevole e basta rinominarlo per vederne le interiora.

Uno .zip dentro uno .zip? Si può fare. Gli autori di Word, che si sono messi a programmare per curare la dipendenza da Settimana Enigmistica, si sono anche divertiti a cambiare in .bin l’estensione di tutto quello che si trovasse nella pancia della balena. Così, se non hai indizi sulla consistenza del file che cerchi, puoi passare del tempo in allegria a provare questo o quell’altro file.

Perché Minecraft, perché Roblox, perché Pornhub, quando puoi usare Word come piattaforma per dare vita a qualsiasi perversione?

*L’errore nel titolo [arriva dall’autore del brano](https://www.battiato.it/lombrello-e-la-macchina-da-cucire/).*

*Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile.*