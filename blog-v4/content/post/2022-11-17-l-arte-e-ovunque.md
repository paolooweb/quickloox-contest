---
title: "L’arte è ovunque"
date: 2022-11-17T00:08:00+01:00
draft: false
toc: false
comments: true
categories: [Internet, Software]
tags: [CrossFire, BIOS]
---
![Autoritratto con Bios](/images/autoritratto-bios.jpg "Autoritratto con BIOS.")

*Autoritratto con BIOS. Milano Centrale, alba, bit su led.*

Più tardi, ho sottoposto a collaudo il mio primo vero cruciverba, realizzato con [CrossFire](http://beekeeperlabs.com/crossfire/index.html) di Beekeeper Labs. Solo un nove per nove, ma c’erano motivi di fruibilità pratica che rendevano necessario un lavoro semplice.

La mia esperienza di cruciverbista è ancora irrisoria; intanto ho imparato che la soglia minima per avere un dizionario minimo su uno schema nove per nove è molto più elevata di quello che pensavo. Mille parole non bastano, oppure devono essere molto, molto ben selezionate.

Ho provato anche qualche situazione impossibile, lasciando girare il software a provare soluzioni. Mac mini M1 non ha fatto una piega; anche se non ne ho la prova, la versione Intel si sarebbe messa a fumare o circa quello.