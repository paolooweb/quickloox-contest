---
title: "Nelle pieghe dell’isteria"
date: 2014-09-27
comments: true
tags: [bendgate, antennagate, iPhone]
---
Gli argomenti per discutere del *bendgate* li porto uno di questi giorni. Nel frattempo rimando alla (lunga) lettura di [quanto scrissi nel 2010 sull’Antennagate](https://macintelligence.org/posts/2010-07-19-la-presa-di-sale/) (c’è sempre una parola chiave che serve a riempirsi la bocca e fingere che sia una cosa importante).<!--more-->

Prima ho preso in mano iPhone 5 e l’ho stretto per bene. In pochi istanti ho perso una tacca. Pardon, un pallino, su iOS 7/8. Ho allentato la presa. In pochi istanti, il pallino è tornato.

Solo che non è più di moda.