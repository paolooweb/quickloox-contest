---
title: "Come è umana lei"
date: 2019-09-11
comments: true
tags: [iOS, iPad, TV+, Arcade, iPhone]
---
A breve saranno vent’anni da quando Steve Jobs tenne un *keynote* in apertura di Macworld New York per annunciare, essenzialmente, Mac OS X e iBook.

Macworld come esposizione non esiste più. Mac OS X si è riprodotto come macOS, iPhoneOS, tvOS, watchOS e iPad OS. I portatili sono diventati una cosa scontata e si vendono a milioni, tanto che non vengono più presentati in un *keynote* ma semplicemente immessi sul mercato. Le innovazioni principali di iBook erano la sagoma a conchiglia e gli inserti di colore.

Nel frattempo sono arrivati telefoni, tavolette, orologi, centri media per schermi televisivi. In questi venti anni Apple ha trasformato il modo di usare il computer, mettendo un computer in luoghi indicibili come, appunto, un telefono o un orologio.

Sono arrivati l’assistente vocale, l’identificazione biometrica con impronta digitale e ora con impronta 3D del volto, funzioni come Handoff che ti fanno cominciare una email alla scrivania e finire sull’autobus, lavorando indifferentemente da un Mac Pro con schermo a 6K oppure da un iPhone, l’apprendimento automatico dei sistemi.

Per quelli che il loro orologio mentale lo hanno fermato lì, il *keynote* di ieri [è stato *zero innovazione*](https://www.facebook.com/groups/applerebels/permalink/1408689485946701/). Si aspettano, suppongo, un sistema operativo nuovo ogni sei mesi (nonostante [APFS](https://www.howtogeek.com/331042/whats-the-difference-between-apfs-macos-extended-hfs-and-exfat/) sia recente e sia tanto), un frigorifero Apple, l’interfaccia neuronale, il protocollo dati per la trasmissione del pensiero.

È che si sono fermati e non hanno continuato a osservare. Un anno dopo quel *keynote* Apple ha iniziato a scardinare il mercato tradizionale dell’ascolto della musica. Più tardi sono arrivati i film.

Ha iniziato a pensare meno allo hardware e più a quello che le persone facevano con lo hardware.

Intanto Steve Jobs tiene un *keynote* parlando di [intersezione tra tecnologia e arti liberali](https://macintelligence.org/posts/2016-11-09-umani-e-algoritmi/). Altri, invece di pensare a come rendere le macchine più intelligenti grazie a noi, pensano a [come rendere noi più intelligenti grazie alle macchine](https://macintelligence.org/posts/2017-08-22-intelligenza-umanistica/). In altre parole, il prodotto è solo metà della faccenda.

Si arriva a oggi e infatti Tim Cook parla di servizi. Non solo musica, non solo film, ma giochi, editoria, pagamenti. Mac, iPad, iPhone, watch e tv sono quattro linee di business che alimentano l’attività. I servizi sono la quinta. Una volta ne esisteva solo una: Mac. Poi Mac e iPod. Poi Mac e iPhone. Poi Mac, iPhone e iPad. Certo iPhone non fa più sfracelli, iPad non prende più di sorpresa il mondo. Però sono filoni importanti. Mentre watch e i servizi sono gli iPad di ieri e gli iPhone dell’altroieri, la forza in crescita.

A pensarci, watch non è hardware; è un accessorio. Neanche i più radicali tra i sostenitori della riparabilità degli apparecchi vorrebbero la possibilità di ripararsi da soli un watch. Neppure i più fanatici del ricambio dei componenti si azzardano a chiedere di poter cambiare il processore o la scheda video o il disco interno, su watch. watch riguarda quello che fa la gente.

Nei *keynote* passati, Apple ha mostrato i disabili che approfittano delle sue funzioni di accessibilità per fare cose altrimenti impossibili. Nel [keynote](https://www.apple.com/apple-events/september-2019/) di questo settembre, ha mostrato persone che hanno rinviato l’appuntamento con la morte, o consentito quello con una nuova vita, grazie ad watch. Ha presentato non solo giochi in esclusiva per Arcade ma anche nuovi studi medici per fare progredire la ricerca sulle malattie cardiache, quelle dell’udito, sulla salute delle donne.

Tutte persone che poi, quando sono tranquille, guardano un film, ascoltano musica, scattano foto, creano contenuti.

Riesco a mostrare il punto? Apple non mette più in primo piano le doti dello hardware, o lo fa sempre meno, o solo dove è essenziale (il nuovo Mac Pro, il nuovo iPhone). Mette in primo piano le persone.

Per vendere tecnologia, si è umanizzata. Al contrario di chi si è tecnologizzato, vent’anni fa, ed è rimasto ancora lì.

Apple è diventata la più umana delle aziende tecnologiche, senza concorrenza alcuna. Poi, se interessa quanto contrasto abbia lo schermo di iPhone Pro, lo si può sapere. Solo che è sempre più irrilevante e con esso la reazione pavloviana *ho pagato meno e faccio le stesse cose*. Più brutte, ma è gente che non si rende conto della stupidità della premessa, figuriamoci se vede la conclusione.

Per quanto riguarda la questione dei profitti mostruosi, della Apple trasformata in macchina per soldi, la risposta è semplice: se servi un miliardo di persone soddisfatte, fai un sacco di soldi. Se questo blog venisse letto da un miliardo di persone, ci metterei una pubblicità grande un francobollo e diventerei un milionario.

Ma divago. Pensare a questo: *By innovation only*, solo per mezzo dell’innovazione. Innovazione che consiste nel pensare non più agli apparecchi, bensì alle persone. Citofonarlo agli androidiani che snocciolano a memoria i giga di Ram dentro il loro clone.