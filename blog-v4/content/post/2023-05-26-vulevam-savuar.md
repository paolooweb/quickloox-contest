---
title: "Vulevam savuar"
date: 2023-05-26T09:37:31+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [Wi-Fi]
---
Meraviglioso Wi-Fi di istituzione edita e autorevole che mi parla in inglese per tutto il tempo, insiste nel considerarmi americano rispetto al numero di cellulare e infine invia un Sms di autenticazione che contiene la password *caponago85*.

(Per i diversamente lombardi: *caponago* è un lemma che corrisponde o potrebbe corrispondere a qualche paese a due passi da casa mia, un po’ come inventarsi un cognome esotico e venire fuori con *Brambilla* o *Rossi*).

Se vincessi le elezioni, emanerei una legge che obbliga le aziende a fornire a tutto il personale per un mese le infrastrutture offerte al cliente a partire dalla prima installazione. Devi farti un account sul servizio come se fossi un cliente, collegarti con Internet tramite quel Wi-Fi e senza altri metodi, compilare l’anagrafica… se la User Experience non ci arriva per capacità, che almeno sia imposta fino a quando verrà capita per sofferenza diretta.