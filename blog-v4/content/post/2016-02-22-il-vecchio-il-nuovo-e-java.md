---
title: "Il vecchio, il nuovo e Java"
date: 2016-02-22
comments: true
tags: [iPad, Runescape, Html5, JavaScript, Flash, plugin, Java, C++]
---
Ogni tanto commento che, nel terzo millennio, scrivere giochi di massa funzionanti su una sola piattaforma è indice di cattivo *design*. Un gradino sopra arriva la considerazione che anche scrivere scrivere giochi multipiattaforma con sistemi vecchi è cattivo *design* se hai cominciato oggi e necessità di rinnovamento se lo hai fatto molto tempo fa.

[Runescape](http://www.runescape.com/) ricade nel secondo segmento. I suoi autori a [Jagex](http://www.jagex.com) sono partiti subito bene con il linguaggio Java, scelta che ha permesso loro di offrire il gioco su Mac, Windows e pure nel *browser*. Però sono partiti quindici anni fa (appena [festeggiati](http://runescape.wikia.com/wiki/15_Year_Anniversary_Celebrations)) ed era un’altra epoca.

Oggi i giochi scritti in Java si avviano a fare la fine, mai rimpianta, dei giochi scritti in Flash. Se non funzionano su un computer da tasca, si perde un pubblico sterminato. Se non funzionano su una tavoletta, si infastidisce chi vuole giocarli anche lontano della scrivania.

[Mi ero detto deluso](https://macintelligence.org/posts/2015-10-08-la-lunga-attesa-di-runescape/) nel vedere Runescape mancare, dopo due anni dall’annuncio, della [versione Html5](https://macintelligence.org/posts/2013-07-23-i-tempi-del-software/) che doveva consentire in teoria l’esecuzione su iPad. Credo siano rimasti delusi anche in Jagex, tanto che questo weekend si è svolto [il primo collaudo semiprivato di un nuovo client per giocare](http://services.runescape.com/m=news/first-nxt-closed-beta-weekend--friday-at-1200-utc?jptg=ia&amp;jptv=community_news), molto più veloce e prestante.

Scritto in linguaggio C++, una scelta estremamente impegnativa ma anche pagante in risultato finale. [Funziona su Windows, Mac e Linux](http://services.runescape.com/m=news/dev-blog--nxt---platforms-for-runescape?jptg=ia&amp;jptv=community_news).

E iPad? Siamo sempre ad attendere Html5, presumibilmente quando – dice Jagex – arriverà [WebGl 2.0](https://www.khronos.org/registry/webgl/specs/latest/2.0/). Vogliono consentire anche il gioco nel *browser*, prima consentito dal *plugin* Java, domani da Html5 e JavaScript. Purtroppo sempre un po’ domani.

La morale, a scelta, è che per molti versi Java si avvia a diventare il nuovo Flash. Oppure che di questi tempi, se progetti un gioco di massa, nessun apparecchio può essere lasciato indietro. Oppure che, se ci fosse un modo semplice ufficiale e diretto per avere Runescape su iPad, sarei un giocatore e non solo un iscritto. Per quanti andrebbe moltiplicato?
