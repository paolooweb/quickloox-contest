---
title: "Quanto costa un certificato"
date: 2022-01-20T02:23:15+01:00
draft: false
toc: false
comments: true
categories:
tags:
---
Poco dopo essere entrati nel XXI secolo, [The Open Group](https://www.opengroup.org) [intentò causa](https://arstechnica.com/civis//viewtopic.php?f=14&t=674603) ad Apple per abuso di marchio registrato: Mac OS X Server parlava di Unix, ma non passava la relativa certificazione.

> Sì, a quel punto del lavoro avevamo accesso all’intero codice sorgente di Apple.

Le opzioni erano due, [racconta Terry Lambert](https://www.quora.com/What-goes-into-making-an-OS-to-be-Unix-compliant-certified), che si occupò di risolvere il problema: rendere Mac OS X conforme a Unix e così disinnescare la causa, nella quale The Open Goup chiedeva duecento milioni di danni; oppure comprare The Open Group, probabilmente per un miliardo di dollari.

> In quel momento conoscevo praticamente ciascuna riga di codice dei tredici milioni che componevano il kernel di Mac OS X.

Venne scelta la prima opzione e al team di Lambert venne promesso sotto forma di azioni il dieci percento dei duecento milioni a rischio per via della causa. Dieci milioni a Lambert, cinque milioni a Ed Moy e a Karen Crippes. Per completare il team vennero temporaneamente presi a contratto altri due programmatori.

> Se mi avessero chiesto di fare la stessa cosa su Linux, ci sarebbero voluti probabilmente cinque anni, con il lavoro di due dozzine di persone.

Nel giro di pochi mesi il gruppo di lavoro sistemò tutto quello che andava sistemato per passare la certificazione Unix. A parte intoppi burocratici che impedirono di inserire i cambiamenti in Mac OS X Tiger, la prima versione del software di sistema raggiungibile, fu un gran successo e a rileggere il racconto di Lambert vengono i brividi dall’emozione.

> Ricevemmo un sacco di gratitudine dalla comunità Open Source, in particolare per i nostri interventi per fare passare i test a [bash](https://devhints.io/bash).

> Immagino che nel corso di quell’anno abbiamo contribuito a centinaia di progetti open source per circa due milioni di righe di codice.

_Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile._
