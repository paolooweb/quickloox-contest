---
title: "Pensare agli anziani"
date: 2018-01-25
comments: true
tags: [Spectre, Meltdown, Capitan, High, Sierra, macOS]
---
Apple ha pubblicato vari aggiornamenti di sicurezza che tra l’altro proteggono per quanto possibile contro [Meltdown e Spectre](https://macintelligence.org/posts/2018-01-05-li-disegnano-cosi/). E lo ha fatto [anche per macOS 10.11 El Capitan](https://support.apple.com/en-us/HT208465).<!--more-->

È raro che un aggiornamento si spinga oltre la versione precedente di macOS (che in questo caso sarebbe Sierra, macOS 10.12), anche se non inedito. Stavolta è accaduto e significa che una macchina vecchia di otto anni, come la mia, è ragionevolmente protetta anche contro una minaccia nuovissima. Certo non come se ci fosse High Sierra, ma non c’è da lamentarsi.