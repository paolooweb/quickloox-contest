---
title: "Retrospettive"
date: 2020-12-08
comments: true
tags: [M1, Gassée, A7, A14, iPad, iPhone, 5S, Moorhead, Forbes, Geekbench, Intel, Schiller]
---
Il 10 settembre 2013, [ricordava Jean-Louis Gassée](https://mondaynote.com/64-bits-it-s-nothing-you-don-t-need-it-and-we-ll-have-it-in-6-months-1d394641e97a), Apple annunciò il processore A7 per iPhone 5S, il primo a sessantaquattro bit per iOS.

Patrick Moorhead [twittò](https://twitter.com/PatrickMoorhead/status/377491310191464449) *i 64 bit aggiungono al massimo più memoria e più registri. Punto.*

<blockquote class="twitter-tweet"><p lang="en" dir="ltr">We&#39;ll see just how good Apple&#39;s marketing team is trying to leverage 64-bit. 64-bit add more memory and maybe registers. Period. <a href="https://twitter.com/hashtag/A7?src=hash&amp;ref_src=twsrc%5Etfw">#A7</a> <a href="https://twitter.com/hashtag/Apple?src=hash&amp;ref_src=twsrc%5Etfw">#Apple</a></p>&mdash; Patrick Moorhead #WebexONE #SAPTechEd (@PatrickMoorhead) <a href="https://twitter.com/PatrickMoorhead/status/377491310191464449?ref_src=twsrc%5Etfw">September 10, 2013</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

Oggi M1 esce mentre la serie A è arrivata al numero quattordici. Patrick Moorhead ha criticato aspramente i nuovi Mac con M1 per varie ragioni, tra le quali la [mancanza di connessione 5G](https://macintelligence.org/blog/2020/11/26/recensioni-a-vapore/) (più lo rileggo, più è sensazionale).

Gassée riportava benchmark di velocità molto interessanti per A7, ottenuti con Geekbench. Sì, quello che oggi quelli del PC liquidano perché la vita reale è un’altra cosa bla bla bla. Parla di Phil Schiller che commenta le prestazioni del processore e le definisce *di classe desktop*. Si chiedeva Gassée:

>“Classe desktop” implica che Apple potrebbe usare versioni future del suo processore a 64 bit per sostituire i chip Intel nei suoi Mac?

E ricordo quelli che si accaloravano a spiegare che i sessantaquattro bit in sostanza non portavano alcun vantaggio. Senza quell’A7 di sette anni fa, oggi non c’è M1. Hai detto niente.