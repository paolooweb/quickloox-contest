---
title: "L’inflazione non esiste"
date: 2023-09-14T11:27:37+01:00
draft: false
toc: false
comments: true
categories: [Hardware]
tags: [iPhone, PerfectRec, Pixel Envy, Nick Heer, Heer, Nowinski, Wally Nowinski]
---
*I prezzi di iPhone sono sempre più alti*, dice il barista. Può essere vero, c’è anche l’inflazione, la tassazione che è inclusa nei prezzi italiani ed esclusa nei prezzi americani… sarà vero?

A *PerfectRec* hanno aggiustato i prezzi secondo il tasso di inflazione e scrivono che [i prezzi dei nuovi iPhone sono i più bassi di sempre](https://www.perfectrec.com/posts/iPhone15-price). Il prezzo di un iPhone 15 base è il più basso dal 2007; iPhone 15 Plus è il Plus più basso di sempre; iPhone Pro è il Pro più basso di sempre.

Sulla pagina ci sono tutti i dati, di cui si può naturalmente dubitare, a patto di verificarli. Si parla di prezzi americani e non altro, quindi non è detto che le conclusioni siano applicabili all’Italia o a qualunque altro posto.

*Pixel Envy* non è del tutto d’accordo e [muove una serie di critiche](https://pxlnv.com/blog/iphone-pricing-inflation/): si parla di iPhone senza contratto, le categorie sono formate in un modo che non lo convincono eccetera. Però non porta dati alternativi.

Come facciamo allora? È certo che ai dati si può fare dire qualsiasi cosa, basta torturarli abbastanza a lungo. È anche vero che rappresentare in forma semplice e concisa l’andamento di un prodotto che ha avuto letteralmente centinaia di prezzi diversi e decine di modelli diversi nel tempo richiede forzatamente qualche semplificazione.

Va capito se le semplificazioni siano accettabili e portare dati migliori, o almeno più ricchi se non sono semplificati, purché interpretabili.

Di sicuro l’inflazione, quando supera le quote fisiologiche, minaccia ogni ragionamento che non ne tenga conto. Al minimo, andrei piano con tutti i ragionamenti del tipo *due anni fa costava cinquanta euro in meno* a meno di avere presente quanto valore hanno perso gli euro di oggi rispetto a quelli di ieri.

