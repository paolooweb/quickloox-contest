---
title: "La fabbrica dei sogni"
date: 2020-07-06
comments: true
tags: [Eco, Pendolo, Foucault]
---
Nel capitolo 40 de *Il pendolo di Foucault*, Umberto Eco smonta il meccanismo dell’industria degli Aps, gli _autori a proprie spese_, quelli disposti a pagare per essere stampati e pubblicati in cambio di promesse.

La narrazione è ironica e i personaggi del libro sono inventati; le dinamiche invece le ho vissute in seconda persona.

Posso parlarne perché è passato molto tempo. L’amico protagonista è oggi un consulente ben pagato, con una bellissima famiglia, reduce da posizioni di prestigio in aziende importanti, libero di soddisfare interessi artistici con una preparazione ragionevole. Una persona realizzata appieno.

Molti anni prima cercava la sua strada e, a partire da una istruzione sommaria, voleva essere riconosciuto letterariamente. Scriveva poesie e si impegnava molto. Solo che non basta. Già non basta con la prosa, con la poesia è sfidare l’abisso. Neanch’io sono un letterato, però le superiori, persino le mie, facevano già sufficiente differenza. Era materiale sotto il dilettantismo.

Diedi qualche timido suggerimento migliorativo senza mai dire quello che realmente pensavo. Come fai a tarpare le ali a un amico? E poi ero un ragazzino, potevo benissimo avere torto marcio.

Per un amico si fa tutto e io ero appena venuto in possesso di una novità pazzesca: PageMaker 1.0. Lui voleva un tipografo che gli impaginasse e stampasse le poesie, un desiderio piuttosto costoso per il nostro studentato. Però potevo almeno impaginarle e risparmiargli la prima parte della spesa.

PageMaker 1.0 su un Macintosh Plus era una dimostrazione dell’esistenza di Dio; arrivai in fondo, nonostante il programma fosse 1.0 probabilmente perché avevano invertito uno 0.1 sull’etichetta e uno schermo 512 x 342 dovesse visualizzare talvolta pagine A4 intere. Non parliamo della velocità. Però ci riuscii.

Tempo dopo, l’amico mi informò entusiasta che aveva ottenuto un premio letterario. Assolutamente dovevo accompagnarlo a Roma, visto che avevo il merito dell’impaginazione. Tra l’incredulo e il curioso, il viaggio ci stava tutto. E poi, _friends will be friends_.

La premiazione avveniva nella sala congressi di un hotel abbastanza defilato. A memoria ci saranno stati un centinaio di posti. Molti di essi erano occupati e quasi tutti da vincitori.

Per due ore, se ricordo, fu tutto uno sfilare, ritirare la targa, la stretta di mano, la foto ricordo, gli applausi dei premiandi che applaudivano i premiati e pregustavano il loro momento. C’erano infinite categorie e altrettanti premi: il giovane, l’anziano, la donna, l’uomo, l’opera prima, la rivelazione, l’insegnante, il pensionato, la saggistica, la storicistica, la menzione d’onore, il personaggio, il paesaggio, il viaggio… sto svariando, non ricordo a memoria le categorie esatte. Certamente ve ne erano abbastanza per assegnare decine di premi.

C’era posto anche per la poesia e il mio amico ritirò orgoglioso la targa, strinse la mano, si mise in posa. Fu un viaggio che significò molto per lui ben oltre il premio letterario e se scriverò la mia autobiografia non mancherò di raccontarlo. La prova dell’esistenza di Dio si compì con un miracolo ancora più grande di una impaginazione portata a buon fine su PageMaker 1.0.

Lessi Eco anni dopo. Era come se nella sala dell’albergo ci fosse stato seduto lui, a prendere appunti. La situazione, i personaggi, tutto coincideva millimetricamemte. Anche la fregatura.

Ė un altro secolo. Mi capita di svolgere piccole consulenze per persone interessate ad autopubblicarsi. Le aiuto ad avere un libro all’altezza e attraverso le procedure di pubblicazione. Una persona motivata e paziente arriva ovunque da sola ma non tutti hanno il tempo o l’interesse per farlo; inoltre possono generarsi inghippi o problematiche e particolari, così preferiscono un supporto.

Nessuna di queste persone vince premi letterari. Mi riconoscono un compenso per l’aiuto ricevuto. Su Amazon, su Books di Apple, tuttavia pagano _zero_ per avere il loro libro in catalogo. Se vendono una copia, anche una sola, incassano il dovuto. Se vogliono l’edizione cartacea (Amazon) possono averla a prezzo di costo. Le _royalty_ sono vastamente superiori a quelle riconosciute a un autore italiano da un editore italiano. Vengono pagate molto prima e pure per cifre modeste.

Evito una trattazione approfondita. Neanche questo sistema è perfetto, ci sono abusi da una parte e dall’altra, qualcosa funziona sempre diversamente da come dovrebbe e via dicendo. Il cambiamento dai tempi degli autori a proprie spese raccontati nel _Pendolo_, in termini di civiltà e rispetto, rimane sconvolgente.

Come [scritto ieri](https://macintelligence.org/blog/2020/07/05/oggi-politica/), non esiste un diritto a essere letti. Ci sarà sempre chi vorrà a tutti i costi andare su Amazon e resterà ignorato. Dignità e gruzzoletto resteranno comunque anche loro, invece di essere succhiati dalla fabbrica dei sogni che vidi in azione di persona e spero di non vedere mai più.