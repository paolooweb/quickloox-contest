---
title: "Un castello di parole"
date: 2016-01-26
comments: true
tags: [Wolfenstein, 3D, awk, GitHub, Homebrew, Mac, iPad]
---
Per amare Wolfenstein 3D bisogna essere entrati nel castello dei nazisti di notte, da soli e in silenzio, con i sensi all’erta per eliminare la maledetta SS prima che abbia il tempo di farlo lei con noi.<!--more-->

Devo averlo già raccontato: incontrai una delle versioni di Wolfenstein giocando su un Apple II di redazione, al termine dell’orario di lavoro, assieme a un collega. L’interfaccia voleva una ventina di tasti per muoversi, puntare l’arma e sparare; ci dividevamo i compiti, dieci tasti ciascuno. Era estate, c’erano i Mondiali di calcio, le strade di Milano erano semideserte. Durante le partite dell’Italia un boato arrivava dalle case e da come era composto si distinguevano il gol degli azzurri, quello avversario, la rete annullata, il fuorigioco, il rigore.

Molti anni dopo, per quanto poco spazio possa esserci su disco, una copietta di [Wolfenstein 3D](http://www.myabandonware.com/game/wolfenstein-3d-3sg) su Mac la tengo sempre. E su [iPad](https://itunes.apple.com/it/app/wolfenstein-3d-classic-platinum/id309470478?l=en&mt=8).

Si capisce perché dovevo verificare che funzionasse il virtuosismo di un programmatore che [ha ricreato il gioco nel Terminale](https://github.com/loox/awk-raycaster), in seicento righe di [awk](http://www.grymoire.com/Unix/Awk.html), con le pareti e gli altri elementi del gioco rappresentati da caratteri di testo colorati per mezzo di un elementare (per un addetto) algoritmo della grafica 3D.

[GitHub](https://github.com/) è oramai un’abitudine acquisita, [Homebrew](http://brew.sh) ha richiesto una spolveratina ed è stato di grande aiuto nel velocizzare l’installazione.

Il virtuosismo funziona.

Adesso, dopo l’esplorazione del castello nazista, resta da avventurarsi nelle combinazioni magiche di awk. Affascinante.