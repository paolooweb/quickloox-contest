---
title: "Per me, numero due"
date: 2023-02-03T00:24:56+01:00
draft: false
toc: false
comments: true
categories: [Internet]
tags: [Apple, Six Colors, Tim Cook, Cook, Windows, Android, Meta]
---
Il trimestre natalizio di Apple ha visto, anno su anno, calare il fatturato come non accadeva da sei anni a questa parte.

Ovverossia, è stato il secondo miglior trimestre di sempre invece di essere il primo.

Nella sua [conferenza con gli analisti](https://sixcolors.com/post/2023/02/this-is-tim-apple-q1-2023-analyst-call-transcript/), l’amministratore delegato Tim Cook ha spiegato che tra inflazione, pandemia (che qui si ignora ma in Cina ha causato ritardi importanti alla produzione), guerra e via dicendo, non è che sia stato il migliore dei contesti possibili. Senza questioni valutarie, i fatturati dei singoli segmenti sarebbero in crescita.

Non è questo, comunque. È che Apple ha *due miliardi di apparecchi attivi*. Per confronto, 
Meta (Facebook, Instagram, WhatsApp) ha [due virgola novantasei miliardi di utenti attivi mensilmente](https://www.cnbc.com/2023/02/01/facebook-parent-meta-earnings-q4-2022.html). Ma non vale, è software, e la gente vende sé stessa invece di comprare. Android ha due miliardi e settecento milioni di utenti attivi e [ben più di tre miliardi di apparecchi](https://9to5google.com/2022/05/11/google-io-2022-numbers/). E Windows? [Uno virgola sei miliardi](https://earthweb.com/windows-users/) di apparecchi.

Come base di apparecchi attivi, Apple è numero due. Come classifica dei migliori trimestri finanziari, quello appena concluso è il numero due. 	In questo ambito, è un numero che non dispiace.