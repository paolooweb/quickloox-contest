---
title: "Intuitivo sarà lei"
date: 2021-02-04T01:51:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [iPhone, Mac, HomePod Mini, John Gruber, Daring Fireball, Mavericks, Notre-Dame, Macintosh Plus, Apple II, Bruno Munari] 
---
È scioccante contare le persone che osservano un’interfaccia al lavoro con la competenza in design del pompiere, l’esperienza del surfista e la consapevolezza del bibliotecario, e decretano se e quanto sia *intuitiva*.

(Mestieri bellissimi, importanti, massimo rispetto, però il design c’entra fino a un certo punto e specialmente quello delle interfacce).

Più o meno il livello del giudizio sta attorno a *io (non) ho capito subito e quindi*.

Uno dei grandi designer italiani, [Bruno Munari](https://www.munart.org), ha lasciato testimonianze straordinarie del suo lavoro con i bambini.

Guarda caso, i bambini sono tra i più grandi collaudatori al mondo di interfacce. Perché le valutano senza pregiudizi. Qui si vede che il problema del pompiere o del surfista non sta nella competenza zero, in effetti, quanto nei pregiudizi. Un designer, quanto meno, li ha ma sulla questione ci lavora davvero. Mentre noi non ci lanciamo nella spiegazione di come avremmo saputo spegnere il rogo di Notre-Dame senza neanche conoscere [i retroscena](https://www.nytimes.com/2019/07/17/world/europe/notre-dame-cathedral-fire.html) oppure [cavalcare un’onda alta come un condominio a Mavericks](https://www.youtube.com/watch?v=Lb-q_eJzXEY), a meno di non voler fare la figura dei nullasenzienti su Facebook.

Gli altri grandi collaudatori di interfacce? Gli anziani. Perché sono fragili e faticano a cavarsela guardando al contesto.

In questi giorni ho appreso dall’esperienza con gli anziani come sia complicato progettare un’interfaccia *realmente* intuitiva.

Anche l’ignoranza (nel senso buono, la non-conoscenza) può fare molto. Molti anni fa, quando si andava a trovare gli amici con la borsa di Macintosh Plus a tracolla, vidi un amico e coetaneo, Paolo, alle prese con un programma di introduzione ad Apple II. (Devo averlo già raccontato, ma tanto devo ancora portare i vecchi post nella nuova struttura).

Paolo non aveva mai approcciato un computer. Si sedette, Valerio inserì il floppy in Apple II, digitò [PR#6](https://en.wikipedia.org/wiki/Apple_DOS) e premette Invio. Paolo osservò ogni cosa.

Il programma partì e spiegò la prima cosa da capire: Apple II si governava attraverso la tastiera e, per fare eseguire un comando, occorreva premere il tasto Invio. Quello che Paolo aveva visto premere un minuto prima, allo stesso scopo.

Sullo schermo comparve una rappresentazione fedele della tastiera. Il tasto Invio lampeggiava. Sotto il disegno, la scritta *premi Invio per continuare*.

Ai miei occhi, dopo mesi di Sinclair Spectrum, Sinclair Ql, Olivetti M10, Z80 di Cambridge Computing, quella schermava gridava *premi il tasto Invio*, era la cosa più ovvia ed evidente del mondo.

Paolo guardava lo schermo divertito e sconcertato. *E adesso?* Ai suoi occhi, con esperienza di computing pari a zero, quella schermata gridava *sono un disegno che lampeggia*. Non aveva alcun collegamento mentale precostruito tra tastiera virtuale e tastiera fisica. Non aveva neanche l’idea di dover necessariamente fare qualcosa. Per quello che ne sapeva, quella era una animazione che probabilmente sarebbe andata avanti da sola, o forse no.

Era un’interfaccia intuitiva?

Veniamo all’oggi. Anziano (lucido, intelligente, istruito, consapevole) alle prese con iPhone. La prima volta nella vita alle prese con un cellulare diverso da quelli degli anni novanta.

Gli si spiega, lo si assiste. A un certo punto l’interfaccia mostra un messaggio. *Che cosa faccio?*, chiede l’anziano. *Leggi con calma il messaggio e comprendilo*.

In fondo al messaggio, un tasto OK azzurro fa contrasto corretto con il messaggio. Comunica di non fare parte del messaggio; ai nostri occhi esperiti, grida *sono da toccare per confermare l’eliminazione del messaggio dallo schermo*.

Agli occhi dell’anziano, è un’altra scritta. Chiede *e adesso?*. *Devi toccare l’area colorata*.

Niente, ai *suoi* occhi, mostra che quello sia un pulsante e che vada toccato. Venticinque anni dopo, la verità è che l’interfaccia più evoluta a nostra disposizione non è ancora in grado di parlare a una persona priva di una esperienza pregressa.

L’errore? Presupporre la conoscenza del meccanismo di feedback dell’interfaccia. L’interfaccia ti comunica, tu confermi di avere ricevuto. Naturale? Per niente. La verità è che su un iPhone *si ragiona ancora come se fosse necessario dare conferma al computer di avere letto un messaggio*. Come se stessimo usando il Terminale. l’OK di oggi come l’Invio degli anni ottanta.

È un’interfaccia intuitiva?

Una interfaccia *veramente* intuitiva non sarebbe così criticamente modale. Mostrerebbe il messaggio, senza alcuna richiesta implicita di feedback. Lo toglierebbe da sola se l’interazione con l’umano dimostrasse che il messaggio è stato recepito. In caso contrario, dopo un tempo di attesa accuratamente calibrato, cambierebbe messaggio per spiegare meglio che cosa fare, o per chiedere *vuoi che lo faccia io al posto tuo e ti insegni a rifarlo?*. Per dire.

John Gruber su *Daring Fireball* è lecitamente entusiasta dell’interfaccia usata da Apple per [passare la riproduzione di un brano da iPhone a HomePod Mini e viceversa](https://daringfireball.net/linked/2021/02/03/homepod-mini-handoff). Avvicini un apparecchio all’altro. Fatto. C’è feedback visivo, c’è feedback tattile, è una cosa fatta benissimo. Apple al suo meglio.

Forse sufficiente persino per un anziano. *Avvicina il telefono alla palla* è comprensibile da chiunque e soprattutto viene imparato istantaneamente. Non servono competenze particolari per riprodurre lo stesso gesto.

È abbastanza? È intuitivo? Attenzione a rispondere così, tanto per fare conversazione. Potresti trovarti a valutare il tuo punto di vista nella pratica, davanti a un collaudatore spietato.

<iframe width="560" height="315" src="https://www.youtube.com/embed/Lb-q_eJzXEY" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

*Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile.*