---
title: "Gli esami non tradiscono mai"
date: 2024-03-12T00:18:22+01:00
draft: false
toc: false
comments: true
categories: [Internet, Education]
tags: [Austin, SAT, algebra, San Francisco]
---
Mentre le scuole media di San Francisco lavorano per [reintrodurre l’algebra in terza media](https://macintelligence.org/posts/2024-02-18-algebra-e-disuguaglianze/), tolta nella convinzione che rendesse la vita più difficile agli studenti svantaggiati, l’università texana di Austin [torna a richiedere obbligatoriamente ai nuovi iscritti gli esami indipendenti di valutazione SAT](https://news.utexas.edu/2024/03/11/ut-austin-reinstates-standardized-test-scores-in-admissions/) (un equivalente dei nostri Invalsi, detto con grande approssimazione ma velocemente).

L’ateneo di Austin (per i non addentro, la capitale del Texas, come fosse Berna o Lisbona) è la spia di [un macrotrend che coinvolge oltre duemila college](https://news.slashdot.org/story/24/03/11/1910201/u-of-texas-at-austin-will-return-to-standardized-test-requirement) di ogni rango e dimensione. Il coronavirus aveva cambiato tante dinamiche di ammissione nelle università, che avevano allentato i controlli all’ingresso; ma poi c’era anche la parte ideologica. I test di valutazione premiavano i privilegiati, quelli che avevano più possibilità di prepararsi, magari prendere pure un insegnante a domicilio per farsi aiutare, che non avevano magari bisogno di lavorare parallelamente alla frequenza dei corsi; insomma, i test portavano discriminazione.

Nessuno li ha tolti, ma nessuno li ha più richiesti; una domanda di ammissione poteva pervenire con o senza risultati di un test di valutazione.

Dopo qualche anno, ci sono i dati. Si è scoperto che gli studenti ammessi senza un test alle spalle hanno (mediamente, ovvio) più difficoltà a progredire nel curriculum.

Si è capito che i risultati di un test aiutano le università ad attivare i programmi di supporto per gli studenti che possono avere bisogno di aiuti supplementari per arrivare a risultati. Se uno studente arriva senza test e ha bisogno di una mano, non c’è modo di saperlo e nemmeno di dargliela, la mano.

Ciliegina sulla torta: i soggetti più discriminati dall’assenza di test di valutazione sono… gli studenti brillanti provenienti da famiglie bisognose. Che, con i test, hanno la possibilità di emergere e fare attivare da un’università un programma di borsa di studio o altro atto a permettere di continuare e completare gli studi. Se nessuno porta i test, non si possono individuare i talenti e aiutarli come necessario.

Il principio di garantire a tutti l’accesso agli studi superando le diseguaglianze sociali è un cardine di una società moderna. L’idea che nascondere la bravura (o meno) degli studenti alleggerisca le diseguaglianze di partenza, ecco, a livello di prima ingenuità ci può anche stare, basta rendersi conto almeno con i dati di fatto che è una stupidaggine.