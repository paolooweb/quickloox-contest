---
title: "Spazio per le autonomie"
date: 2013-09-14
comments: true
tags: [iPhone]
---
*AnandTech* [ha frugato](http://www.anandtech.com/show/7324/apple-increases-iphone-5c-and-5s-battery-sizes-relative-to-5) nella documentazione su iPhone depositata da Apple presso la [Federal Communications Commission](http://www.fcc.gov) americana, l’ente che deve autorizzare preventivamente tutti gli apparecchi elettronici capaci di comunicare. Alcuni dati che ha trovato a proposito di batteria sono uguali tra iPhone 5 e nuovi iPhone; questi sono quelli differenti.<!--more-->

Watt per ora: 5,45 su iPhone 5; 5,73 su iPhone 5C; 5,96 su iPhone 5S.  
Milliampère per ora: 1.440 su iPhone 5; circa 1.507 su iPhone 5C; circa 1.570 su iPhone 5S.

Dalle pagine di [confronto tra modelli](http://www.apple.com/it/iphone/compare/) e di [specifiche tecniche](http://www.apple.com/it/iphone/specs.html) del sito Apple si ricavano ricavano le seguenti differenze ufficiali di batteria tra iPhone 5 e nuovi iPhone.

L’autonomia delle conversazioni in 3G, di otto ore su iPhone 5, passa a dieci ore su ambedue iPhone 5C e iPhone 5S.  
L’autonomia di *standby*  passa da 225 a 250 ore.  
La navigazione in Internet con collegamento Lte arrivava a otto ore e adesso arriva a dieci.

Giusto perché al bar Sport sta passando l’idea che iPhone 5C sia uguale a iPhone 5, solo di plastica. Non esattamente.
