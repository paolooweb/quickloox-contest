---
title: "E ora qualcosa di completamente diverso"
date: 2020-11-01
comments: true
tags: [Cook, virus, Covid-19, Snell, Six, Colors, Apple]
---
È luogo comune che Apple sia centrata su iPhone e possibilmente trascuri gli altri settori di attività. Poi avviene che i [risultati trimestrali relativi all’estate](https://www.apple.com/newsroom/2020/10/apple-reports-fourth-quarter-results/) manchino, per la prima volta, del lancio di un nuovo iPhone. E, nonostante questo, siano il record di tutti i tempi per il trimestre estivo, con analogo record per il fatturato Mac e pure per quello dei servizi. Senza contare un bell’andamento di iPad.

La notizia c’è, possiamo chiuderla qui.

Invece no. Durante l’audioconferenza con gli analisti, uno degli eventi più noiosi e incolori che si possano concepire, pieno di domande contorte per cercare di estorcere una informazione inedita e risposte sterilizzate piene di frasi fatte e gergo aziendalese, Tim Cook ha fatto qualcosa di diverso. Meglio, lo ha detto. Traduco dalla [trascrizione di Jason Snell su Six Colors](https://sixcolors.com/post/2020/10/tim-cook-on-apples-2020/):

§§§

Quando sposti lo sguardo sull’intero anno fiscale, vedi un attestato del lavoro del team e della solidità dell’attività al tempo di Covid-19…

[traduco *resilient* in qualsiasi modo diverso da *resiliente*, che è indecente da sentire in italiano, anche a costo dell’imprecisione.]

Quando abbiamo iniziato a confrontarci con Covid-19, ho dichiarato che per un’azienda fondata sull’innovazione ci sono cose peggiori che dover cambiare periodicamente il modo di fare praticamente tutto. Quest’anno abbiamo lavorato in un modo che ci ha spinto a reimmaginare ogni fase del nostro processo di innovazione, fino a come condividiamo i nostri annunci con il mondo e come mettiamo i nuovi prodotti nelle mani dei nostri clienti. Al lavoro da stanze da letto o tavoli della cucina, in ambienti da ufficio distanziato, laboratori riarrangiati e fabbriche ridisegnate, il team ha riprogettato ogni parte del piano intanto che veniva attuato. I risultati parlano da soli…

Innovazione non è solo quello che produci, ma anche come approcci i problemi. Questi team, e ogni team in Apple, non hanno affrontato una singola domanda per cui non siano riusciti a rispondere con pazienza e decisione. Le loro azioni non hanno semplicemente corrisposto al momento, ma ci renderanno un’azienda migliore mentre andiamo avanti…

Penso che se dovessi descrivere i nostri risultati di questo trimestre in una singola parola, questa sarebbe *solidi*. A parte i dati finanziari, non credo che alcuno di noi ricorderà quest’anno con tenerezza o nostalgia. Quelli di noi che si svegliano la mattina con la speranza di tornare alla normalità possono dirsi fortunati. Non tutti possono concedersi questo lusso. C’è chi ha perso una persona amata, chi vive l’incertezza e la paura di essere senza lavoro, chi si preoccupa di persone care che non è in grado di vedere. Un senso di opportunità mancate, piani rimandati, tempo perso. Per quanto separati, è stato ovvio quest’anno che in azienda i team e i colleghi abbiano contato gli uni sugli altri più che in tempi normali. Ritengo che questo istinto, questa determinazione abbia costituito una parte essenziale della nostra navigazione di quest’anno.

Il lavoro non può sostituire tutte le cose che ci mancano attualmente, ma uno scopo condiviso può fare molto. La convinzione che insieme possiamo fare più che da soli, che le persone di buona volontà – guidate da creatività, passione e quel prurito che viene da una grande idea – possano comunque realizzare nel nostro piccolo qualcosa per aiutare le persone a insegnare, imparare, creare o anche solo rilassarsi in un momento così. Anche se quello che facciamo ci richiede di lavorare sulla frontiera della tecnologia, in termini di materiali, prodotti e idee che pochi anni fa neanche esistevano, quest’anno ci ha forzato a guardare quello che ci rende umani: malattia, volontà, speranza.

Nessuno si augura un altro anno come questo, ma non potrei essere più orgoglioso del team, del lavoro che abbiamo svolto e del piccolo ruolo che abbiamo ricoperto nell’aiutare le nostre comunità a ritrovare speranza ed energie in questo momento.

§§§

Nel mio piccolo condivido questo con chiunque passi a leggere. È un momento nel quale siamo più vicini di quello che pensiamo e, in un modo o nell’altro, riusciremo a fare meglio insieme che da soli. Speranza ed energie per tutti.