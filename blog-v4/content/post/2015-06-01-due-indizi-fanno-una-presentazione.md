---
title: "Due indizi fanno una presentazione"
date: 2015-06-01
comments: true
tags: [PowerPoint, Keynote, Google, iCloud, LibreOffice, Docs]
---
Ci sono cose che fanno stringere il cuore. Una di queste è trovarsi ad aiutare uno studente di seconda media cui l’insegnante ha chiesto di produrre diapositive in PowerPoint.<!--more--> 

A disposizione ci sono come minimo Keynote e l’omologo di Google Docs, ma niente: bisogna esportare in PowerPoint, perché la presentazione va proiettata attraverso un PC Windows. Come se non potesse utilizzare LibreOffice, per esempio, o appunto Google Docs. Notizia: se lo studente avesse un account iCloud, potrebbe persino usare sul PC Keynote in versione *browser*.

Io le avrei fatte preparare in Html, le diapositive. Che occasione sprecata.

Spero vivamente di non trovarmi mai a dover discutere di persona la questione con il docente ignorante: non sono sicuro di sapermi controllare in un caso così.

Impossibile che sia pura coincidenza l’essermi imbattuto contemporaneamente in un articolo del *Washington Post* intitolato [PowerPoint dovrebbe essere messo al bando](http://www.washingtonpost.com/posteverything/wp/2015/05/26/powerpoint-should-be-banned-this-powerpoint-presentation-explains-why/).

Il settore pubblico è tra l’altro tenuto a prendere in considerazione il software libero e i formati aperti [per legge](http://www.apogeonline.com/webzine/2014/01/15/se-non-basta-la-legge-ecco-pronte-le-linee-guida): l’incapacità personale di adeguarsi ha smesso da tempo di essere giustificabile o tanto meno comprensibile. Chi ha di fianco un coatto di Office, che non ha compiuto una scelta ma solo perpetuato una cattiva abitudine del secolo scorso, inizi a parlargli.