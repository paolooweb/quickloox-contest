---
title: "E adesso la Creative Suite"
date: 2014-10-24
comments: true
tags: [Office, CS, Adobe, LibreOffice, Zoho, Google, iWork, iDraw, Pixelmator, Photoshop, Illustrator, iPhone, iPad, Mac, InDesign]
---
Pacifico che serva software per scrivere, fare di conto, presentare. È che Office non serve davvero più a nessuno, non per via del software in sé, ma perché le funzioni offerte sono davvero a livello universale. Senza neanche parlare di iWork, il 99 percento delle persone può svolgere il 99 percento del lavoro anche con le Google Apps. Poi c’è [LibreOffice](https://it.libreoffice.org), poi le soluzioni stile [Zoho](http://www.zoho.com). Spero davvero che Office venga scelto per questioni di simpatia, di abitudine, di interfaccia, di quello che si vuole. Perché se viene scelto in quanto Office, in quanto è quel software lì con quel nome lì che va scelto perché è quello lì, non ci siamo più. Siamo usciti dalla logica e dalla normalità, esattamente perché oggi è normalissimo non usare Office.<!--more-->

Non vedo l’ora che arrivi lo stesso momento anche per la Creative Suite di Adobe. Nessuna antipatia personale; stesso problema di Office. Sono corredi software che una volta risolvevano problemi e da qualche anno giustificano la propria esistenza.

Sulla Creative Suite c’è il problema delle alternative, della gente che allarga le braccia e insiste su InDesign perché c’è solo lui. Photoshop è un nome entrato nel vocabolario comune e allora bisogna che sia lui perché lo si sente nominare e non c’è la paura dell’ignoto. Ora di essere più coraggiosi, specie per quanti abbracciano la *continuity* di Apple e alternano l’uso di Mac a quello di iPad, o addirittura di iPhone (neanche tanto addirittura se è un iPhone 6 Plus, quasi un iPad mini).

Allora cominciamo a giocare qualche carta. [iDraw](http://www.indeeo.com/idraw/), pronto da usare su Mac e su iPad, qui [21,99 euro](https://itunes.apple.com/it/app/idraw/id404705039?l=en&mt=12), là [7,99 euro](https://itunes.apple.com/it/app/idraw/id363317633?l=en&mt=8). Serve ancora, davvero, Illustrator?

Fresco di uscita su iPad e straordinario: [Pixelmator](http://www.pixelmator.com/ipad/), anche per Mac, [26,99 euro](https://itunes.apple.com/it/app/pixelmator/id407963104?l=en&mt=12) sullo schermo più grande e [4,99 euro](https://itunes.apple.com/it/app/pixelmator/id924695435?l=en&mt=8) su quello più piccolo. È così indispensabile, Photoshop?

Iniziamo a renderci indipendenti dalla Creative Suite, come lo abbiamo fatto da Office. Si vive meglio, si lavora meglio.