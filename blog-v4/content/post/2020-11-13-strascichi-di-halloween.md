---
title: "Strascichi di Halloween"
date: 2020-11-13
comments: true
tags: [OpenOffice, LibreOffice, LibreItalia]
---
Leggo che gira un aggiornamento di OpenOffice, 4.1.8. Contenente la bellezza di [ventuno bug fix](https://bz.apache.org/ooo/buglist.cgi?list_id=233429&query_format=advanced&resolution=FIXED&resolution=FIXED_WITHOUT_CODE&target_milestone=4.1.8). Dopo soli quattordici mesi di sviluppo dalla versione precedente, che aveva sistemato la bellezza di [due bug](https://bz.apache.org/ooo/buglist.cgi?list_id=233429&query_format=advanced&resolution=FIXED&resolution=FIXED_WITHOUT_CODE&target_milestone=4.1.7).

La versione 4.0 data al [2013](https://blogs.apache.org/OOo/entry/a_short_celebration_and_then).

Per confronto, BBEdit 13.5.2 ha sistemato [dodici bug](https://www.barebones.com/support/bbedit/current_notes.html). Ma il precedente 13.5, che dista tre settimane da oggi, ne toglieva sessantacinque intanto che applicava rifiniture, aggiunte e persino la [compatibilità con Apple Silicon](https://macintelligence.org/blog/2020/10/24/chi-sbaglia-fa-pagare/) che ancora non era la famiglia Mx [appena presentata](https://macintelligence.org/blog/2020/11/12/cosi-andra-il-mondo/).

BBEdit è curato da una manciata di persone. Ci vivono, ma sempre una manciata sono. Per manutenere una suite di produttività individuale servono, se è un vero progetto *open source*, centinaia di coinvolgimenti.

Questo fa capire la portata del lavoro su OpenOffice. Parliamo di un morto che cammina.

OpenOffice è *open source* di sola facciata. Da anni lo tengono in “vita”, modalità mostro di Frankenstein, aziende che vogliono solo ostacolare l’affermazione di [LibreOffice](https://www.libreoffice.org), autenticamente libero e alternativo a chi vuole prosperare sui formati chiusi, sul lock-in, sullo strangolamento della concorrenza e sull’eliminazione della libertà di scelta per il proprio desktop.

Questa è anche una storia molto concreta di come funziona l’amore di Microsoft per il software libero. [Apache Software Foundation](http://apache.org), che mette il proprio marchio sulla pagina di OpenOffice, relativamente a questo progetto è un pupazzo nelle mani di una multinazionale. Se ci fosse un grano di sincerità nelle parole di Microsoft sull’open source, OpenOffice sarebbe effettivamente sostenuto; oppure verrebbe sostenuto LibreOffice. Invece, zero.

Cestina OpenOffice. Scarica LibreOffice. Iscriviti a [LibreItalia](https://www.libreitalia.org). La notte degli zombi è divertente, ma è terminata.