---
title: "La festa dell’Ascensione"
date: 2011-12-18
comments: true
tags: [Angband, Morgoth, roguelike, Sauron, Ladder, Silmarillion]
---
Nessun intento blasfemo e neanche errore marchiano di calendario: &#232; il gergo usato per indicare la vittoria finale in una partita ad <a href="http://rephial.org/">Angband</a>. E ho vinto.

Ci ho messo alcuni anni. Se il proprio personaggio muore durante la discesa verso il centesimo livello del sotterraneo, bisogna ricominciare tutto da zero. Giocando pochi minuti al giorno, una partita richiede come minimo molte settimane. Ci sono personaggi che sopravvivono pi&#249; facilmente a inizio gioco ma hanno difficolt&#224; nel finale e viceversa; ci sono persone che comprendono perfettamente lo spirito del gioco e arrivano a segno al primo tentativo e altri che non ce la fanno in una vita. Nella discesa i nemici si fanno man mano pi&#249; temibili e minacciosi e l&#8217;unico modo per accumulare esperienza in merito &#232; sopravvivere al loro incontro: non riesce sempre. Pi&#249; si gioca, inoltre, pi&#249; &#232; facile commettere un&#8217;imprudenza o una svista, magari fatale.

Angband, dal quale discendono molte varianti all&#8217;interno del cosiddetto genere <i>roguelike</i>, &#232; di ispirazione tolkieniana. Cos&#236;, per arrivare in fondo, ho sconfitto numerosi malvagi popolanti la saga del <a href="http://www.ibs.it/code/9788845269707/tolkien-john-r-r/signore-degli-anelli-la.html">Signore degli Anelli</a> e il <a href="http://www.ibs.it/code/9788845256547/tolkien-john-r-r/il-silmarillion.html">Silmarillion</a>, fino a Sauron &#8211; la cui sconfitta apre la strada all&#8217;altrimenti proibito centesimo livello &#8211; e al devastante Morgoth. Nel corso della discesa ho rinvenuto l&#8217;Unico Anello, che d&#224; un grande potere e tuttavia &#232; permanentemente maledetto, ma sono riuscito a prevalere senza indossarlo.

Prossimamente affigger&#242; il mio <a href="http://angband.oook.cz/ladder-show.php?id=12145">personaggio vittorioso</a> sulla <a href="http://angband.oook.cz/ladder.php">Ladder</a>, la grande scala ideale dei forum di Angband.

&#200; solo un gioco, naturalmente. Per di pi&#249; giocato in grafica Ascii: Sauron &#232; una <i>p</i> violetta, Morgoth &#232; una <i>P</i> indaco, il giocatore una chiocciola. Eppure c&#8217;&#232; pi&#249; profondit&#224; e pi&#249; variet&#224; che in ogni altro prodotto ludico esistente: anche in questa partita, delle tante che ho giocato, sono entrato in possesso di armi e oggetti magici mai visti prima.

&#200; solo un gioco. Oso comunque condividere la mia soddisfazione.