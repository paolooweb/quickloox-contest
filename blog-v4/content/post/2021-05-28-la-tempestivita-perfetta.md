---
title: "La tempestività perfetta"
date: 2021-05-28T00:38:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [macOS, macOS 11.4, Mike Bombich, Carbon Copy Cloner, AppleInsider, Big Sur, Apple Silicon, M1] 
---
Mike Bombich, il produttore di Carbon Copy Cloner, ha raccontato sul proprio blog di [alcuni problemi legati al boot con dischi esterni su Big Sur](https://bombich.com/blog/2021/05/19/beyond-bootable-backups-adapting-recovery-strategies-evolving-platform). Lettura interessante e autorevole; Carbon Copy Cloner è in giro con successo da oltre vent’anni.

>Apple ha chiarito che continuerà a supportare il boot da unità esterne sui Mac con Apple Silicon, ma la realtà è che saranno più limitati in ciò che possono fare.

Questa è una delle conclusioni del post, il quale consiglia essenzialmente di attuare strategie di backup meno basate su dischi esterni in grado di avviare Mac e spiega come Carbon Copy Cloner si adatterà alla nuova situazione.

Era il 19 maggio; *AppleInsider* lo riprende con tempestività il giorno 24 per accorgersi del post e parafrasarlo a modo suo, con il titolo [Le mosse di Apple puntano a un futuro privo di backup bootabili, afferma uno sviluppatore](https://appleinsider.com/articles/21/05/24/apples-moves-point-to-a-future-with-no-bootable-backups-says-developer). Qualcosa di leggermente diverso dal contenuto effettivo del post.

I casi del destino: proprio il 24 maggio, Apple [pubblica macOS 11.4](https://9to5mac.com/2021/05/24/apple-releases-macos-11-4/).

Il 27 maggio, *The Eclectic Light Company* titola [I Mac M1 con Big Sur 11.4 supportano pienamente i dischi esterni](https://eclecticlight.co/2021/05/27/m1-macs-running-big-sur-11-4-support-external-disks-fully/). Questa la conclusione del pezzo:

>In precedenza, le mie riserve maggiori sul raccomandare i Mac M1 si concentravano sulle loro importanti limitazioni con i dischi esterni bootabili. Grazie alla estensione del kernel AppleVPBootPolicy e a altri cambiamenti in macOS 11.4, ora posso raccomandarli senza questa riserva. I Mac M1 sono OK.

Il futuro del booting esterno su Mac sarà diverso da come lo racconta *AppleInsider*. In compenso, si sono goduti tre giorni eccezionali di acchiappaclic.

*Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile.*               