---
title: "Script per scommessa"
date: 2013-10-25
comments: true
tags: [OSX, Mavericks, AppleScript, Pages]
---
AppleScript. Una delle funzioni più preziose e meno considerate, l’equivalente di quel 95 percento del cervello umano che secondo lo stereotipo va sprecato. Qui è il cervello elettronico.<!--more-->

AppleScript (vale anche per Automator) vede novità in Mavericks. Come [riassume The Unofficial Apple Weblog](http://www.tuaw.com/2013/10/23/applescript-and-automator-gain-new-features-in-os-x-mavericks/), gli script ora si sincronizzano su più Mac collegati via iCloud, possono comunicare con il Centro Notifiche, possono essere immagazzinati in *Library* apposite se sono di uso comune, possono essere firmati digitalmente e attivati via voce.

Niente male. C’è dell’altro però: il dizionario AppleScript di Pages è stato, per così dire, energicamente potato.

Voglio scommettere che sia legato allo sviluppo delle nuove versioni di iWork e che AppleScript sia stata considerata funzione secondaria. Dopotutto, anche se non mi piace ammetterlo, credo che a usare regolarmente AppleScript collegato a Pages sia meno gente di quella che serve per mettere insieme un poker.

Se vinco la scommessa, in un qualche prossimo aggiornamento tornerà supporto AppleScript come si deve anche per Pages. Se perdo, si è perso qualcosa.