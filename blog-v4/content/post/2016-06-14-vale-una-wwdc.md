---
title: "Vale una Wwdc"
date: 2016-06-14
comments: true
tags: [Venerandi, X11basic]
---
Di Wwdc globale c'è tempo per parlare. Oggi vorrei mettere l'accento su un Wwdc personale che ha messo in piedi Fabrizio Venerandi, capace di [catalizzare un porting di X11basic su Mac](https://www.facebook.com/fabrizio.venerandi/posts/10210132915785365).<!--more-->

È scritto semplice, in italiano, stupendo. Dovremmo tutti prendere esempio e non solo per meditare. A volte, per avere uno strumento in più a nostra disposizione, basta poco più che volerlo.