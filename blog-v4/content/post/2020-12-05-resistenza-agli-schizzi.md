---
title: "Resistenza agli schizzi"
date: 2020-12-05
comments: true
tags: [iPhone, Altroconsumo, Antitrust]
---
Note sulla [multa di dieci milioni di euro](https://www.ilsole24ore.com/art/antitrust-multa-10-milioni-ad-apple-pubblicita-ingannevole-iphone-ADU2XQ5) inflitta ad Apple dall’Autorità garante della concorrenza e del mercato.

La multa punisce due pratiche scorrette. La prima è stata pubblicizzare vari modelli di iPhone come resistenti all’acqua secondo le specifiche di uno degli standard in vigore, senza precisare che i valori delle specifiche (per esempio, immersione a una profondità massima di due metri per massimo trenta minuti) riguardano prove condotte in acqua statica e pulita e quindi non rispecchiano le situazioni reali vissute dai consumatori.

Apple dichiara la conformità allo standard, che altro dovrebbe fare? Come è possibile testare *qualunque* condizione di immersione in acqua? Gli standard esistono apposta per fornire un dato condiviso di confronto. È un po’ come dire che il [test dell’alce](https://it.wikipedia.org/wiki/Test_dell%27alce), siccome si svolge su una pista apposita in condizioni predefinite, è ingannevole perché ci sono infinite strade e condizioni diverse da quella dove avviene il test. Boh.

La seconda pratica scorretta sarebbe stata dichiarare che i danni da liquidi non sono coperti dalla garanzia, senza specificare meglio che, per esempio, la garanzia di conformità potrebbe tutelare l’acquirente dal danno da liquido (nel caso di iPhone fallato in partenza), mentre la garanzia legale no.

Non mi risultano *pubblicità* di prodotti con descrizione così minuziosa dei distinguo sulle garanzie. Se parliamo delle pagine dei prodotti sul sito dell’azienda è un’altra cosa, però Apple da tempo scrive tutto. Questo si legge sulla [pagina di iPhone 11](https://www.apple.com/it/shop/buy-iphone/iphone-11):

>iPhone 11 resiste agli schizzi, alle gocce e alla polvere; è stato testato in laboratorio in condizioni controllate, con un rating di grado IP68 (profondità massima di 2 metri fino a 30 minuti) secondo lo standard IEC 60529. La resistenza agli schizzi, alle gocce e alla polvere non è una caratteristica permanente e potrebbe diminuire con la normale usura. Non tentare di ricaricare il tuo iPhone quando è bagnato; consulta il manuale per pulirlo e asciugarlo. I danni da liquidi non sono coperti dalla garanzia limitata di un anno di Apple. I danni da liquidi potrebbero essere coperti dalla tua garanzia legale di conformità.

C’è qualcosa di ingannevole, di fuorviante? Per curiosità, la [pagina americana](https://www.apple.com/shop/buy-iphone/iphone-11) è esattamente uguale a parte i riferimenti alla doppia garanzia, per forza dato che è un’invenzione europea. Quindi, niente forzature o contenuti *ad hoc* per fare passare messaggi ambigui. Ecco che cosa scrivono in Usa:

>iPhone 11 is splash, water and dust resistant and was tested under controlled laboratory conditions with a rating of IP68 under IEC standard 60529 (maximum depth of 2 meters up to 30 minutes). Splash, water and dust resistance are not permanent conditions and resistance might decrease as a result of normal wear. Do not attempt to charge a wet iPhone; refer to the user guide for cleaning and drying instructions. Liquid damage not covered under warranty.

Uno dirà: parlare di resistenza all’acqua e poi specificare che i danni da liquidi non rientrano nella garanzia è fuorviante.

Prima di tutto, l’esistenza di un messaggio sui danni da liquidi, in un mondo logico, dovrebbe smentire la tesi della pubblicità che inganna gli utenti sull’impermeabilità di iPhone. Come fai a pensare che sia impermeabile mentre leggi che non ti riconosco la garanzia sui danni da liquido? Come inganno, è un po’ stupido.

Secondo, nessuno parla di *impermeabilità*. Mai. Sulle pagine di Apple, compresa quella [di supporto](https://support.apple.com/it-it/HT207699) che invito a leggere per giudicare quanto sia ingannevole, si parla di *resistenza* all’acqua, agli schizzi, alla polvere eccetera.

La resistenza, come concetto, non è assoluta. Nella pratica, lo si legge, diminuisce con il tempo e con l’usura. Nel caso dell’acqua, porta a un problema: mi garantiscono che iPhone sopravvive fino a 30 minuti in acqua fino a due metri di profondità. E allora sai che cosa faccio io? Un mese prima della fine della garanzia metto in ammollo iPhone per il tempo che serve a farlo guastare. Poi me lo faccio sostituire in garanzia e ripeto il ciclo all’infinito.

Ecco perché la garanzia limitata non copre i danni da liquidi. Buonsenso, o dovrebbe esserlo.

Chiaramente ci sarà qualche persona onesta che ha recuperato iPhone dall’acqua entro i ventinove minuti, è andata in buona fede al Genius Bar, è stata respinta con perdite, si è arrabbiata e ha totalmente ragione. Apple dovrebbe trovare un modo di distinguere i furbi dagli onesti. Giusto che venga multata se non riesce a tutelare adeguatamente il cliente.

Questo sembra un problema reale. La pubblicità ingannevole pare invece un buon pretesto per fare rumore sui giornali, dove la parola *Apple* ha sempre molto richiamo.

Mica per niente, credo che i primi a sollevare polvere sul tema siano stati quelli di Altroconsumo. Quelli di [iPad miglior cornice digitale di tutti i tempi](https://macintelligence.org/blog/2010/02/08/quelli-degli-antiforfora/). [Hanno contestato uno spot del 2016](https://www.altroconsumo.it/hi-tech/smartphone/news/iphone-7-water-resistant), con la motivazione che il consumatore ne esca convinto di acquistare un prodotto, attenzione, *impermeabile o comunque resistente agli schizzi*. Come se fosse la stessa cosa e come se Apple ingannasse su tutti e due. Apple, dicono, inganna il consumatore inducendolo a pensare che iPhone *resista a contatto con l’acqua*. Non è un inganno! È assolutamente vero. Considerarlo un inganno è un pretesto per intentare un reclamo, fare rumore e forse cercare compiacenza in altre sedi.

Per inciso, lo spot parla [dell’audio di iPhone 7](https://www.youtube.com/watch?v=rWhqjaNj1qs) e, certo, mostra schizzi d’acqua su un iPhone posato su un tavolino con dell’acqua. Nessuna immersione, nessuna sommersione.

D’altro canto, Altroconsumo offre [consulenza legale ai soci con problemi di garanzia rispetto ad Apple](https://www.altroconsumo.it/hi-tech/smartphone/news/problemi-apple). Soci *paganti*.

iPhone resiste agli schizzi d’acqua, conforme a standard internazionali riconosciuti e usati da migliaia di aziende e organizzazioni. Agli schizzi di fango, come quelli di Altroconsumo, un po’ meno.

<iframe width="560" height="315" src="https://www.youtube.com/embed/rWhqjaNj1qs" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>