---
title: "Primi premi"
date: 2015-06-22
comments: true
tags: [DoesNotCommute, CrossyRoad, Vainglory, Shadowmatic, Workflow, Fantastical2, AffinityDesigner, Photoshop, Pixelmator, Adobe, Metamorphabet, Robihood, Pacemaker, ElementaryMinute, jump-O, trading]
---
Per una volta mi sono lasciato andare alla curiosità tardigrada verso le *app* [premiate da Apple per il *design* durante il Wwdc 2015](https://developer.apple.com/design/awards/).<!--more-->

Le cose interessanti: [Does Not Commute](https://itunes.apple.com/it/app/does-not-commute/id971756507?l=en&mt=8) è veramente da premio. [Crossy Road](https://itunes.apple.com/it/app/crossy-road-endless-arcade/id924373886?l=en&mt=8) ha metà dell’originalità di Does Not Commute, ma altissimo rischio assuefazione. [Vainglory](https://itunes.apple.com/it/app/vainglory/id671464704?l=en&mt=8) è un giocone molto bello che però mi pare premiato soprattutto per la capacità di spremere al limite il processore. Di originalità non c’è molto. [Shadowmatic](https://itunes.apple.com/it/app/shadowmatic/id775888026?l=en&mt=8) è una cosa curiosa, molto ben realizzata, che mi ha lasciato un po’ freddo nella prova nonostante l’idea sia carina e il fatto tecnico sia interessante. Forse merita una seconda *chance*. Questi erano i giochi.

Di Workflow [si è già parlato](https://macintelligence.org/posts/2014-12-16-tutte-insieme-ora/). [Fantastical 2](https://itunes.apple.com/it/app/fantastical-2-calendar-reminders/id975937182?l=en&mt=12) è consigliatissimo a chi fa uso intenso e non passivo dei calendari, una di quelle app con cui si sostituisce una volta e per sempre l’offerta standard di Apple. [Affinity Designer](https://itunes.apple.com/it/app/affinity-designer/id824171161?l=en&mt=12) è un notevole concorrente di [Pixelmator](https://itunes.apple.com/it/app/pixelmator/id407963104?l=en&mt=12) e… Photoshop. Per chi come me soffre la dipendenza da Adobe (io solo per InDesign però), può essere l’occasione di archiviare finalmente una causa prima.

[Metamorphabet](https://itunes.apple.com/it/app/metamorphabet/id858010121?l=en&mt=8) è educativo e assai grazioso nella realizzazione. Mi chiedo però se abbia veramente valore aggiunto.

[Robinhood](https://itunes.apple.com/us/app/robinhood-free-stock-trading/id938003185?mt=8) sembra molto bello, forse – finalmente – una vera reinvenzione come si deve del *trading*. Disgraziatamente non è disponibile in Italia (il che dice molto dell’Italia).

[Pacemaker](https://itunes.apple.com/it/app/pacemaker-your-personal-mix./id593873080?l=en&mt=8), sarà che non ho il gusto del *mashup*, mi dice niente. Suppongo sia premiato perché riguarda la musica e la musica è un fattore importante per l’ecosistema Apple.

Le due *app* degli studenti, [Elementary Minute](https://itunes.apple.com/it/app/elementary-minute/id889417668?l=en&mt=8) e [jump-O](https://itunes.apple.com/it/app/jump-o/id796270242?l=en&mt=8) sono simpatiche e ovviamente non classificabili. Servono a capire che sbarcare su App Store è questione di volontà, non di capacità. Certo, poi fare successo in mezzo a un milione e passa di concorrenti è tutt’altra questione.

C’è netta preponderanza di *app* iOS tra quelle premiate e va pensato che sette anni fa il genere era inesistente. Che evoluzione.