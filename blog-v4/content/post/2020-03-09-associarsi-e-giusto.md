---
title: "Associarsi è giusto"
date: 2020-03-09
comments: true
tags: [LibreItalia, Copernicani, budget.g0v.it, LibreOffice]
---
Anche quest’anno faccio il socio di [LibreItalia](https://www.libreitalia.org) e [Copernicani](https://copernicani.it).

I primi sono splendidi volontari per il software libero, che si battono come leoni – siamo al paradosso – benché inferiori per numero e potenza di fuoco a una multinazionale vergognosa, perché vengano rispettate le disposizioni statali per l’utilizzo del software libero, nelle scuole prima di tutto e poi in tutta l’amministrazione pubblica.

Mia figlia entra nelle primarie a settembre e mi sono reso conto del calvario informatico che ci aspetta.

Poi c’è la promozione del software libero, il contributo fattivo al [progetto LibreOffice](https://www.libreoffice.org) e tanta altra roba. LibreItalia è veramente una scelta di civiltà.

I Copernicani hanno fatto cose interessanti come [budget.g0v.it](https://budget.g0v.it/partition/overview), lo strumento per analizzare visivamente la legge di bilancio italiana 2020, parallelamente ai whitepaper e ai dossier che un po’ da associazioni come queste ci si aspetta comunque.

Mentre un contributo software di questa portata non lo si trova da alcuna altra parte.

So che sono in lavorazione altre cose e ho avuto modo di toccare con mano la natura apartitica dell’associazione (e soffrirei fortemente se non fosse così) e il valore complessivo delle persone che la animano. La procedura di elezione delle cariche interne ha un meccanismo unico, ispirato alla Venezia dei Dogi, che rende futile creare correnti e cordate nonché improbabile l’elezione di figli-di e raccomandati. I Copernicani funzionano e chiacchierano, come è inevitabile per chiunque si muova nella politica, ma intanto lavorano davvero.

Mi permetto di caldeggiare ciascuna di queste scelte e, soprattutto, entrambe.