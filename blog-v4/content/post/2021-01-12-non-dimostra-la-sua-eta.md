---
title: "Non dimostra la sua età"
date: 2021-01-12
comments: true
tags: [ Big, Sur, BBEdit, Mailsmith, VirtualBox, Adobe, Creative, Cloud, InDesign, XScreenSaver]
---
Sono su [Big Sur](https://www.apple.com/macos/big-sur/). È stato un bel salto, proveniendo da [Mojave](https://en.wikipedia.org/wiki/MacOS_Mojave).

Per il momento non ho nulla da dire di particolare, anche perché credo di essere proprio l’ultimo arrivato. Mi abituo alla nuova interfaccia grafica e sto anche sperimentando il desktop con il passaggio automatico al Night Mode per vedere l’effetto che fa.

La nuova interfaccia è graficamente molto differente da quella cui ero abituato e voglio attendere un momento a dare giudizi. Devo dire che l’attività di oggi è stata tranquilla e mi sono trovato bene. L’unica osservazione che voglio fare subito è che l’icona dell’orologio analogico mostrata nella barra dei menu ha qualcosa di strano. Sembra non sia stata ridimensionata come il resto degli elementi nella barra dei menu e il cerchio del quadrante sborda (venendo tagliato) verso l’alto e verso destra. Magari è un mio problema di underscan sul monitor, ma non riesco a capire perché sia l’unico elemento che è rimasto graficamente uguale a prima.

Peggio ancora, ero abituato a cliccare sull’orologio analogico e vedere ora e data in digitale; in certe situazioni faceva comodo. Ora l’orologio apre l’intera sezione che combina widget e notifiche e certamente posso ricavare la stessa informazione, però in modo meno pratico.

Son minuzie comunque. La velocità è ottima, potrei anzi avere leggermente guadagnato.

A livello di incompatibilità, sapevo che il vecchio InDesign non avrebbe funzionato e nemmeno [Mailsmith](http://mailsmith.org), che mi sono rassegnato a non usare più da mesi e di cui non ho ancora recuperato le caselle da archiviare. Per entrambi ho riservato un posto in una vecchia macchina virtuale [VirtualBox](https://www.virtualbox.org) con Yosemite; quando arriverà il loro momento, sarò pronto.

Una cosa mi ha sorpreso; per tutta la giornata Big Sur è andato avanti con il contagocce a notificarmi, uno per volta, che i miei preziosi salvaschermo tratti da [XScreenSaver](https://www.jwz.org/xscreensaver/) andavano aggiornati. L’ho fatto, c’è una nuova versione datata sette dicembre e credo vada bene. Stiamo a vedere.

Mi pare di essere brillantemente sopravvissuto alla scomparsa di iTunes. Per il resto sono ancora sul mio Mac, a parte i cambiamenti grafici. Proverò a confrontarmi con zsh e vedere se mi sposta qualcosa di serio nel mio uso del Terminale, oppure no.

Una cosa che sicuramente è rimasta fedele a se stessa è VirtualBox. Non lo usavo da anni e non è invacchiato per niente. È esattamente lo stesso carrozzone inaffidabile, confuso, disfunzionale e abborracciato di prima. Per ora è sufficiente, ma immagino che nel mio futuro ci sarà un qualche acquisto di software per macchine virtuali vagamente più serio.
