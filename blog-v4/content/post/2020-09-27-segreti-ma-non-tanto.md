---
title: "Segreti ma non tanto"
date: 2020-09-27
comments: true
tags: [Windows, Aqua, Microsoft, Apple, MacOSX, Verge, XP]
---
Titolo da *The Verge*, che dovrebbe già dire tutto: [Microsoft aveva un tema segreto di Windows XP che lo faceva somigliare a un Mac](https://www.theverge.com/2020/9/25/21456525/microsoft-windows-xp-theme-mac-aqua).

Lo sappiamo perché su Internet è apparso di recente un grosso ammontare di codice sorgente di Windows XP, sottratto alla stessa software house che tiene saldamente per la giugulare il flusso dei dati confidenziali nella tua azienda.

Scelte incaute e fiducie malposte a parte, il tema simil-Aqua (l’interfaccia grafica di Mac OS X a quel tempo) non è mai stato pubblicato ufficialmente: che in Microsoft volessero fare somigliare Windows a Mac, doveva rimanere un segreto.

Penso a tutte le persone autocostrette a mantenere lo stesso segreto e tuttavia condannate a rivelarlo appena mettono piede in un Media World, nel tentativo vano di spendere Windows per comprare Apple, e osservo per loro un minuto di compassione. Coraggio, ci sono strade per stare meglio. Serve solo pensare almeno per qualche istante con la propria testa.