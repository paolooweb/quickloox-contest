---
title: "Le scorciatoie costano fatica"
date: 2013-11-13
comments: true
tags: [iOS]
---
Federico Viticci diventerà qualcuno. Non inganni che per ora faccia il *blogger* su MacStories; ha talento e voglia di faticare, ricetta perfetta per avere risultati.<!--more-->

Una delle sue ultime fatiche è un elenco di (altre) [scorciatoie di tastiera per iOS 7](http://www.macstories.net/tutorials/a-list-of-new-ios-7-keyboard-shortcuts/). Tutto corretto; sono i comandi che si possono dare con successo su una tastiera Bluetooth Apple collegata a un iPad o iPhone, diversi secondo le *app*.

Non esiste alcuna documentazione ufficiale e, come ci si può immaginare, mettere assieme una lista così costa tempo e sudore.

Lo stesso articolo rimanda a una preziosa [pagina in materia](http://www.logitech.com/en-us/articles/8694) nientemeno che sul sito Logitech.

Senza Federico avremmo meno gusto su iOS. Speriamo scriva a lungo e continui a farlo anche quando avrà fatto fortuna.