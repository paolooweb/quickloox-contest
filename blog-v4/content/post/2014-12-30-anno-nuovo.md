---
title: "Anno nuovo, app nuove"
date: 2015-01-01
comments: true
tags: [IcewindDale, Prompt2, Workflow, Natale]
---
Un munifico Babbo Natale aveva già finanziato l’acquisto di [Workflow: Powerful Automation Made Simple](https://itunes.apple.com/it/app/workflow-powerful-automation/id915249334?l=en&mt=8) di cui [si parlava](https://macintelligence.org/posts/2014-12-16-tutte-insieme-ora/) e ora ha fatto arrivare anche [Prompt 2](https://itunes.apple.com/it/app/prompt-2/id917437289?l=en&mt=8) e [Icewind Dale: Enhanced Edition](https://itunes.apple.com/it/app/icewind-dale-enhanced-edition/id909472985?l=en&mt=8).

Rimboccarsi le maniche. C’è un anno da costruire.