---
title: "Su la testa"
date: 2020-03-26
comments: true
tags: [WebKit, Safari, Tor, Brave, macOS, iPadOS, iOS]
---
Da Safari 13.1 su macOS e iOS/iPadOS 13.4, il *browser* di serie [applica il blocco totale dei cookie di terze parti](https://webkit.org/blog/10218/full-third-party-cookie-blocking-and-more/), annuncia il blog di WebKit. In altre parole, l’unico sito autorizzato a raccogliere informazioni sulla nostra navigazione è quello in cui ci troviamo.

>Per quanto ne sappiamo, solo [Tor](https://2019.www.torproject.org/projects/torbrowser/design/#identifier-linkability) è preimpostato nello stesso modo da prima che lo facesse Safari e [Brave prevede alcune eccezioni](https://brave.com/ok-google/) nel suo meccanismo di bloccaggio.

Detto che Tor è una nicchia della nicchia, Safari è a oggi il *browser* più avanti nel proteggere la *privacy* dei navigatori.

Domani, tuttavia, Ciccio scoprirà che un servizio inutile a caso funzionerà male a causa del blocco dei cookie di terze parti. Invece che ringraziare, se la prenderà con Safari.

Nel 2019, Apple ha informato gli sviluppatori che macOS Catalina sarà l’ultima versione a supportare pienamente le estensioni di sistema che installano estensioni del *kernel*.

>Nel superare queste estensioni, gli sviluppatori aiutano la modernizzazione di Mac, migliorano la sua sicurezza e affidabilità e consentono metodi più amichevoli di distribuzione del software. Non è stata ancora stabilita una data finale per la transizione.

>È un buon momento per contattare uno sviluppatore per sapere se ha in programma o disponibile una versione aggiornata del proprio software. Lo sviluppatore può anche spiegare come eliminare o disabilitare l’estensione e le relative conseguenze.

Domani Giuseppa, a scelta, userà un programma inutile scritto da sviluppatori che ignorano scientemente o meno l’avvertimento. Non funzionerà. E lei se la prenderà con Apple.

Il fatto che un sito o un programma siano utili o inutili dipende solo relativamente dal loro scopo. Se non seguono l’evoluzione dei sistemi in cui si muovono, diventano inutili a prescindere. O pietre al collo, volendo.

Dobbiamo essere capaci di guardare al nostro ecosistema a testa alta, con gli occhi verso l’orizzonte, per sapere che strada si accinge a prendere e provvedere per tempo. Stare a testa bassa, concentrati solo sull’oggi, porta a cattive sorprese appena certe tendenze diventano improvvisamente l’attualità e questi giorni lo dimostrano fin troppo ampiamente.

*Nei prossimi giorni il blog potrebbe interrompersi per ventiquattro-settantadue ore per via di un cambio di provider.*