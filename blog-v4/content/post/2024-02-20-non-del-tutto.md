---
title: "Non del tutto"
date: 2024-02-20T01:51:59+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [Stroke, Galactic Compass, Guardian, The Guardian, The Verge]
---
Su PlayStation Store si trova una serie di giochi, chiamiamoli così, intitolati *Carezza l’animale*. Ci sono il criceto, il cane, il gatto, il castoro e così via.

Funzionano tutti allo stesso modo. Sullo schermo appare una foto dell’animale e di fianco il numero di carezze. A ogni pressione del tasto X sul controller, il numero di carezze incrementa. A venticinque carezze si ottiene un trofeo di bronzo, a duemila uno di platino. Finito.

Ogni app costa quattro dollari. Sony trattiene il trenta percento delle vendite. Da settembre 2022 sono state scaricate centoventimila volte, così che il programmatore ha incassato – prima delle tasse – [duecentoquarantamila dollari](https://www.theguardian.com/games/2024/feb/15/stroke-of-genius-how-one-developer-created-a-hit-game-in-just-30-minutes). Dopo la realizzazione della prima app, che ha richiesto una giornata di lavoro, ogni nuova app della serie richiede una mezz’ora di lavoro.

Il programmatore non lo era; ha una moglie e un bambino piccolo. Le bollette arrivavano una dopo l’altra e si è inventato un lavoro. Ha imparato a programmare guardando video didattici su YouTube.

A questo punto bisogna precisare che i *trofei* nel mondo della PlayStation sono come gli *achievement* dei giochi su App Store. Si è sviluppata una sottocultura nella comunità che premia l’accumulo cieco di trofei e il programmatore che non lo era l’ha cavalcata. Nessuno è andato in rovina per comprare le sue app e il suo bambino ha di che crescere.

Certo non vincerà mai alcun premio di qualità del software.

Su App Store, invece, è appena uscita una app gratuita che fa una cosa sola: [punta una grande freccia verde in direzione del centro della nostra galassia](https://apps.apple.com/it/app/galactic-compass/id6451314440?l=en-GB), verso il buco nero supermassiccio noto come Sagittarius A*.

Galactic Compass, come si può capire, [prescinde da qualsiasi conseguenza pratica](https://arstechnica.com/gadgets/2024/02/new-app-always-points-to-the-supermassive-black-hole-at-the-center-of-our-galaxy/#p3). Anche profittando dell’indicazione e procedendo in linea retta, sono sempre ventisettemila anni luce di strada. Eppure la app trasmette qualcosa. Una solennità, un *memento di chi siamo*, un richiamo all’immensità del cosmo, un pretesto per attaccare bottone, un’ispirazione a guardare in alto verso le stelle. Qualcosa, comunque.

Tra milioni di app possiamo trovarne di fortemente inutili. Non sempre, però. Non del tutto, anche se inutili lo sono.