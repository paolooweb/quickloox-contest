---
title: "Tradizionale e unico"
date: 2019-06-19
comments: true
tags: [iPadOS, Viticci, MacStories, Files, Mac]
---
Per capire Apple, va letto [John Gruber](http://daringfireball.net); per capire iPad, va letto Federico Viticci. Le sue [prime riflessioni su iPadOS](https://www.macstories.net/stories/initial-thoughts-on-ipados-a-new-path-forward/) sono ineccepibili e diritte al punto.

Il quale è molteplice. L’articolo è molto lungo e contiene infiniti punti di interesse. I miei due preferiti sono (primo) l’evidenza che Apple aggiunge funzioni in ordine di priorità decrescente e l’avere abilitato il mouse o l’accesso a dischi USB non è riscattarsi da errori del passato, bensì avere già spuntato negli anni le altre voci più importanti della lista; secondo, Apple sta lavorando su iPad per creare una classe di macchine nuova nel concetto, ispirata a Mac ma completamente diversa nell’interpretarne le funzioni dentro l’interfaccia.

Viticci riassume concludendo con l’idea che l’iPad migliore sia una macchina tradizionale, che mutua idee e funzioni dalla sua cultura ultraquarantennale, e nel contempo nuova, nel tradurre operazioni stranote in procedure inedite eppure efficaci.

In queste settimane iPad è la mia macchina di lavoro principale e non posso correre rischi con versioni preliminari del software. Tuttavia, già solo leggere le novità di iPadOS in forma un po’ più dettagliata mi mette dì buonumore.
