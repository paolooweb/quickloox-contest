---
title: "Sempre anniversario"
date: 2019-01-25
comments: true
tags: [Macintosh, Advanced, Substitute, Juran]
---
Parlando di [anniversario di Macintosh](http://www.macintelligence.org/blog/2019/01/24/dal-floppy-a-dd/), il modo più bello di festeggiarlo è trasformarlo carrollianamente in un non-anniversario, da celebrare tutti i giorni.

È quanto potrebbe accadere se decollasse [Advanced Mac Substitute](https://www.v68k.org/advanced-mac-substitute/) di Josh Juran, un emulatore che si dimentica dell’hardware per emulare direttamente il sistema operativo e, di conseguenza, non richiede Rom preinstallate o vecchi dischi di sistema.

Lo stato del progetto è ancora largamente acerbo e, per dirne solo una, non si compila ancora sotto macOS Mojave. Però non sembrano esserci difficoltà insuperabili, solo problemi di tempo e di risorse da parte di Juran.

Il codice è open source e qualche sviluppatore potrebbe raccogliere la sfida al livello superiore: agevolare il completamento di Advanced Mac Substitute.