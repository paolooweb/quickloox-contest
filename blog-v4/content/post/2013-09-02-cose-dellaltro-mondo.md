---
title: "Cose dell’altro mondo"
date: 2013-09-02
comments: true
tags: [iOS, Hojoki, Asteroid, Locusta, Colombini, Mac, MacBookAir, Acer, Aspire57]
---
[Hojoki](https://itunes.apple.com/it/app/hojoki-one-client-for-dropbox/id525010205?l=en&mt=8), *app* gratuita per iPhone iPod touch e iPad che promette di unificare l’interfaccia per Dropbox, Google Drive, Twitter e una ventina di altri servizi di nome.<!--more-->

Non l’ho ancora provata e mi fa anche un po’ paura. Tuttavia, se è un approccio che funziona, merita un premio.

[The Asteroid [hypernovel]](https://itunes.apple.com/it/app/the-asteroid-hypernovel/id687945912?l=en&mt=8): come racconta il comunicato stampa, un romanzo che contiene nove sfide da superare, venti diversi percorsi narrativi e nove finali alternativi a 3,59 euro.

<iframe width="560" height="315" src="//www.youtube.com/embed/78IjRZie4Rs" frameborder="0" allowfullscreen></iframe>

Il concetto di iperlibro non è nuovo: facile pensare a [Locusta Temporis](https://itunes.apple.com/it/app/locusta-temporis/id488701532?l=en&mt=8) di Enrico Colombini. per l’interattività, [Pinocchio](https://itunes.apple.com/it/app/pinocchio-for-ipad-iphone/id422133774?l=en&mt=8) e [The Voyage of Ulysses](https://itunes.apple.com/it/app/the-voyage-of-ulysses/id538571921?l=en&mt=8) di Elàstico hanno già mostrato buone idee. Qui però mi interessa l’intenzione di sfruttare al massimo il supporto, fino a sconfinare nel videogioco, il che non scandalizza, semmai ricorda che oggi i modi di narrare sono diventati molti. E la storia, che ci si ritrova a vivere in prima persona, non è male.

Aspire S7 di Acer: un *ultrabook* [recensito](http://arstechnica.com/gadgets/2013/09/haswell-to-the-rescue-acers-refreshed-aspire-s7-ultrabook-reviewed/2/) da Ars Technica in una pagina dove l’ultima riga è questa:

>Più costoso di altri ultrabook comparabili, compreso MacBook Air.

Nel mondo precedente Mac era il computer che faceva scuotere il capo ai superficiali perché *sì, è bello, ma costa di più*.

Nel bene e nel male viaggiamo verso un mondo nuovo e in un modo o nell’altro, anche solo pochi anni fa sarebbe stato difficile pensare a prodotti come questi.