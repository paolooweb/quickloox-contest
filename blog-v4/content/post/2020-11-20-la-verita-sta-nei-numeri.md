---
title: "La verità sta nei numeri"
date: 2020-11-20
comments: true
tags: [Apple, Silicon, Fortran, Arm]
---
A ogni aggiornamento software importante, vertono chiacchiere attorno al problema della compatibilità. *Aspetto perché non so quando saranno disponibili le applicazioni*, *metti che qualcosa non funzioni*, *devo essere sicuro che funzioni tutto* eccetera.

Con [la transizione di Mac a Apple Silicon e ai processori Arm](https://www.apple.com/it/apple-events/november-2020/), le discussioni sono elevate al cubo.

Fondate o meno che siano, l’impressione che resta sulla pelle è che gli sviluppatori si vedano investiti di un compito improbo (faccio solo presente che l’informatica personale è fatta di transizioni, aggiornamenti e salti di piattaforma da cinquant’anni). Alternativamente, che si ritrovino all’improvviso travolti da un ciclone a sorpresa che spazza le loro certezze (della serie *me lo avevano [detto a giugno](https://p-events-delivery.akamaized.net/2605bdtgclbnfypwzfkzdsupvcyzhhbx/m3u8/hls_vod_mvp.m3u8) che i primi computer Apple Silicon sarebbero arrivati entro la fine dell’anno e questa intemerata di novembre mi coglie di sorpresa*).

Così, senza che debba significare qualcosa in particolare o abbia un senso così profondo: [c’è già un compilatore Fortran nativo per Apple Silicon](https://www.nag.com/news/first-fortran-compiler-apple-silicon-macs).

[Fortran](https://ourcodingclub.github.io/tutorials/fortran-intro/). Tuttora usatissimo nella comunità scientifica per le sue capacità di elaborazione numerica, ma data agli stessi anni in cui nasce [Lisp](https://common-lisp.net) e, analogamente a quest’ultimo, gode di grande reputazione per chi sa apprezzarne le capacità.

Ciò detto, le tendenze della programmazione moderna e i grandi numeri vanno in tutt’altre direzioni, da Python in poi. Su Tiobe, uno degli indicatori più usati per guardare da che parte va il mondo degli sviluppatori software, Fortran sta guarda caso vicino proprio a Lisp, [oltre il trentesimo posto](https://www.tiobe.com/tiobe-index/), in quota zero virgola. Lo dico da fanatico di Lisp; donerei un rene se potesse aiutarlo a passare l’uno percento di adozioni e immagino ci siano persone disposte a fare lo stesso per Fortran. La realtà però è la realtà: siamo una comunità microscopica, nel grande schema della rete.

Se arriva un compilatore nativo per un linguaggio da zero virgola, e arriva appena arriva lo hardware, sarà così difficile, così complicato per chi vende software a pagamento farsi trovare puntuali a servire il cliente? BBEdit è stato [pronto per Apple Silicon](https://www.barebones.com/support/bbedit/notes-13.5.2.html) dal primo momento e certo non ha a disposizione le risorse di una Adobe, per fare un nome qualunque.

La verità, per chi si lamenta della mancanza della sua app nativa, è nei numeri: c’è già arrivato perfino Fortran. Se avesse tanto così di coerenza, dovrebbe lamentarsi con lo sviluppatore competente.