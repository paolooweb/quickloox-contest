---
title: "Voglio tornare bambino"
date: 2014-02-12
comments: true
tags: [LaTeX, TeX, StackExchange]
---
**Domenico** mi segnala – grazie! – una pagina di Stack Exchange, sezione TeX. Stack Exchange è il luogo dove porre domande di programmazione a tutti i livelli e avere sempre una buona risposta. TeX è il capostipite dei linguaggi di composizione di documenti da cui discende LaTeX.<!--more-->

La pagina propone un compito: [mostrare la propria migliore illustrazione scientifica!](http://tex.stackexchange.com/questions/158668/nice-scientific-pictures-show-off) Naturalmente scritta in codice TeX.

Invito a scorrere la pagina. Poi propongo io un compito: scegliere tra tornare bambini e avere nei libri di testo immagini come quelle oppure tornare bambini e avere lo stesso libro su cui abbiamo studiato.

Ovviamente il libro con dentro le animazioni può essere solo digitale.

Mi porterò la pagina nei miei pellegrinaggi evangelici dentro le scuole e le istituzioni. La tirerò fuori ogni volta che sentirò una obiezione aprioristica e banale contro i libri di testo digitali. Mica le *app*, che sanno ancora di attività antisociale. I libri di testo.