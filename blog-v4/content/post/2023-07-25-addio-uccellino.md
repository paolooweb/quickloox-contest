---
title: "Addio uccellino"
date: 2023-07-25T00:07:40+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [Twitter, X]
---
Diamo addio al simbolo di Twitter con [il ricordo di come è nato](https://twitter.com/martingrasser/status/1683266038602010624).

<blockquote class="twitter-tweet"><p lang="en" dir="ltr">Today we say goodbye to this great blue bird<br><br>This logo was designed in 2012 by a team of three. <a href="https://twitter.com/toddwaterbury?ref_src=twsrc%5Etfw">@toddwaterbury</a>, <a href="https://twitter.com/angyche?ref_src=twsrc%5Etfw">@angyche</a> and myself,<br><br>The logo was designed to be simple, balanced, and legible at very small sizes, almost like a lowercase &quot;e&quot;, a 🧵 <a href="https://t.co/pogZnorRko">pic.twitter.com/pogZnorRko</a></p>&mdash; martin grasser (@martingrasser) <a href="https://twitter.com/martingrasser/status/1683266038602010624?ref_src=twsrc%5Etfw">July 24, 2023</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

Un pensiero affettuoso a chi va scrivendo a spruzzo che con la presunta intelligenza artificiale *si crea un logo in minuti*.

Si crea anche in secondi, basta lasciare un pennarello in mano a mia figlia. Il grande assente è il design, come se fosse inutile. Purtroppo, per una fascia crescente di individui in modo crescente pericolosi, lo è.