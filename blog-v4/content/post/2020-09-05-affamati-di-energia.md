---
title: "Affamati di energia"
date: 2020-09-05
comments: true
tags: [Zoom, Teams, Meet]
---
Evidenza empirica: Teams e Meet consumano circa il doppio della batteria che consuma Zoom.

Non ci si può credere: il servizio è sommariamente identico, eppure c’è una evidente differenza di programmazione sotto al cofano.

Se si sta alla scrivania importa magari poco; in mobilità, non ci sono dubbi sulla scelta ideale e viene naturale anche mettere in secondo piano le perplessità sulla privacy garantita da Zoom.

Fosse per me io farei comunque tutto in [Jitsi](https://meet.jit.si). È che tutto non si può avere.