---
title: "Il pendolo e Skim"
date: 2014-03-27
comments: true
tags: [Skim, Anteprima, feedback, imagemagick, Terminale]
---
Era molto tempo che l’oscillazione periodica tra Skim e Anteprima come *app* preferita per la visione dei Pdf stava dalla parte di Anteprima, per semplicità di utilizzo ed eleganza dell’interfaccia.<!--more-->

Ora sono tornato a [Skim](http://skim-app.sourceforge.net) per un motivo preciso: nonostante prometta di ricordarsene, Anteprima dimentica l’ultima pagina letta e riapre un Pdf sempre alla pagina iniziale. Leggo molti Pdf poco alla volta e questa funzione è banale, eppure fondamentale.

Il paradosso è che continuo a usare tantissimo Anteprima… come editor grafico. Per ritagliare o ridimensionare immagini da infilare in un qualche sistema di pubblicazione, è veloce e pratico.

Anche se per questo sto facendo un pensierino al Terminale e [imagemagick](http://www.imagemagick.org). Sarebbe una oscillazione del pendolo in direzione imprevista.

**Aggiornamento:** ispirato da [briand06](http://melabit.wordpress.com) ho provato a cancellare le preferenze di Anteprima. Ora il programma si comporta come dovrebbe ed è stato riaccolto a servizio, in attesa di verificare se dura.