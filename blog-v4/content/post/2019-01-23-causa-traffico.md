---
title: "Causa traffico"
date: 2019-01-23
comments: true
tags: [SemRush, Apple, Must, iPhone, Pixel, Google, Samsung, S9]
---
Anche i sassi sanno che c’è un problema di vendite di iPhone inferiori alle previsioni, ma non tutti hanno avuto l’idea di confrontare il traffico su Internet dei lanci di prodotto negli anni per avere un’idea dell’entità del problema.

Apple Must ha pubblicato i grafici delle [rilevazioni di SemRush](http://www.applemust.com/apples-iphone-miss-revealed-by-search-traffic-analysis/) che mostrano come, effettivamente, il traffico suscitato da iPhone X al momento del suo lancio sia stato molto superiore a quello generato dagli iPhone di quest’anno.

Lo stesso accade grosso modo per Google con la sua gamma Pixel, che ha prodotto meno traffico interessato quest’anno rispetto al varo della linea, mentre Samsung va in qualche modo in controtendenza, con più interesse quest’anno per S9 rispetto al modello dello scorso anno.

Sono dati che vanno presi con ampio beneficio di inventario, però hanno il grande merito di essere dati invece che congetture. Era molto meglio quando le aziende comunicavano il numero di unità vendute. Poi rimase a farlo solo Apple. Poi ha smesso anche lei. Causa traffico.