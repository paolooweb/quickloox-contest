---
title: "A fare incunabolo"
date: 2021-03-02T02:42:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [Incunabolo] 
---
Mi è capitato di tornare di recente sulla questione della conservazione dei media analogici e dei media digitali e [questo esempio](https://twitter.com/incunabula/status/1366323734055972866) mi pare perfetto per spiegare come devono funzionare le cose.

<blockquote class="twitter-tweet"><p lang="en" dir="ltr">A remarkable coloured Hypnerotomachia Poliphili, printed in 1499 by Aldus Manutius in Venice. The only illustrated book produced by Aldus, 600 copies were printed, of which about 230 survive today. This copy was coloured in the mid 16th cent. ~50 years after it was printed. 1/4 <a href="https://t.co/vXzui85MQh">pic.twitter.com/vXzui85MQh</a></p>&mdash; Incunabula (@incunabula) <a href="https://twitter.com/incunabula/status/1366323734055972866?ref_src=twsrc%5Etfw">March 1, 2021</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

Questo incunabolo, di una bellezza sconvolgente, è sopravvissuto per più di cinquecento anni. Nessun disco rigido, ottico, scheda perforata può pensare di competere. Forse un domani un cristallo olografico, ma per ora si tratta di tecnologia da ricerca e sviluppo, completamente indisponibile alle persone normali.

Tuttavia la sua esistenza è una bomba a tempo. Seicento copie stampate, trecentosettanta *non esistono più*. Ne rimangono duecentotrenta. Magari dureranno mille anni. Poi finiranno. L’incunabolo si sarà estinto.

Che speranze ho di vedere un capolavoro del genere? Non sono uno specialista del settore; senza Twitter, neanche ne avrei conosciuta l’esistenza.

Ne esiste una copia presso la Biblioteca nazionale spagnola. Se non c’è niente di più vicino, dovrei affrontare un viaggio lungo e relativamente costoso. Non sono neanche uno studioso; potrei al massimo vederlo dentro una teca. Niente odore della carta, niente tatto. Impossibile guardarlo da vicino quanto voglio, nella teca riflessi e illuminazione spesso arbitraria (non è la prima volta che faccio un’esperienza del genere). Contestualmente, il costo quotidiano della sua conservazione è molto, molto alto.

Il digitale offre molto meno ai sensi e all’intelletto, di una copia su pergamena. Ma posso [vedere una copia dell’incunabolo](http://bdh-rd.bne.es/viewer.vm?id=0000024346&page=1), ogni pagina, a livello di dettaglio. Le immagini riempiono la cache del mio browser; a parte le questioni di copyright, potrei recuperarle e farne backup a un costo miserrimo in confronto a quello della conservazione in biblioteca. Se altri fanno lo stesso, le copie potrebbero essere migliaia. Milioni.

Mi si dice che però i formati cambiano, i servizi chiudono, *shit happens*. Certo, però esiste anche il calcolo delle probabilità. Più le copie sono numerose, più è probabile che qualcuna sopravviva. E, anche da una sola, farne altre copie, esattamente uguali. Ciò che con l’opera originale è impossibile.

L’arte è indubitabilmente una faccenda analogica. La divulgazione e la conservazione sono una faccenda digitale. Liberi di mandarmi a fare incunabolo quanti non condividano. E di comperarmi un biglietto per la Spagna, naturalmente. Ah, vero, neanche si vola in questo periodo…

*Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile.*