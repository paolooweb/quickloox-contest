---
title: "La trilogia dell'aspirante"
date: 2014-06-25
comments: true
tags: [Swift, Playground, Yosemite, OSX, Xcode, Xcode6, iBooks]
---
L’aspirante non è il programmatore, ma diciamo l’automatizzatore. Quello che come la gazzella del proverbio africano, si sveglia e sa che deve affidare al computer tutto quello che il computer sa fare con più precisione e velocità, altrimenti sono soldi sprecati e pure tempo.<!--more-->

Dopo l’[esempio delle letture](https://macintelligence.org/posts/2014-06-20-il-buon-pastore/) e l’[aggiornamento automatico di Xquartz e Homebrew](https://macintelligence.org/posts/2014-06-21-e-fammi-sapere/) spendo una parola a favore di… Xcode.

Sì, l’ambiente complicato che serve a programmare in ambito Apple. Molto complicato.

Ma a brevissimo cominceranno le *beta* pubbliche di OS X 10.10 Yosemite. E diventerà accessibile Xcode 6, che contiene i Playground, ovvero finestre a metà strada tra il documento di testo e quello di programmazione, dove si possono dare istruzioni e vedere subito il risultato, evitando il passaggio dalla compilazione e tutto il resto, quello difficile che appartiene ai programmatori veri.

La [guida al linguaggio di programmazione Swift](https://developer.apple.com/swift/) scaricabile gratis anche dentro iBooks, per dire, si può aprire nei Playground. Gli esempi di programmazione citati nel manuale si possono modificare, integrare, rivoltare e il risultato appare sempre all’istante.

Swift, di suo, è una versione semplificata di Objective-C, il linguaggio che si è usato finora.

Linguaggio semplificato, ambiente interattivo, prezzo gratis… da qui a qualche settimana si apre una ennesima, clamorosa possibilità di imparare.