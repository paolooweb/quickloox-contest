---
title: "Le app dimenticate"
date: 2014-03-06
comments: true
tags: [widget, Dashboard, iBooksPreview]
---
Sono quasi caduto dalla sedia a leggere la notizia di un aggiornamento a iBooksPreview: un [widget Dashboard](http://the.dashboard.onemac.net/2014/03/05/ibookspreview/), per giunta dedicato a qualcosa di attuale come iBooks.<!--more-->

Fa quasi più impressione del fatto che Snow Leopard, Mac OS X 10.6, sia [ancora in vendita](http://store.apple.com/it/product/MC573T/A/mac-os-x-106-snow-leopard?afid=p231%7Ccamref%3A10l72&amp;cid=AOS-US-AFF-PHG) (sì, Apple pratica l’obsolescenza programmata, obbliga tutti a cambiare parco macchine ogni anno, impone dittatorialmente le sue scelte software e bla bla, intanto vende ancora il sistema operativo di quattro generazioni fa).

Zitti zitti, i *widget* non sono mai spariti e Dashboard, nel disinteresse quasi generale, sta sempre lì. Lo stesso sito Apple li elenca ancora e non sono mezzo milione come le *app* per iPad, ma quasi quattromila per Mac ed è una cifra rispettabile.

Per come sono fatti, c’è davvero poca differenza tra una *app* elementare e un *widget*. Anche la piattaforma però conta e lo stesso software interessa di più impacchettato sotto uno schermo touch che da attivare con un tasto su Mac.