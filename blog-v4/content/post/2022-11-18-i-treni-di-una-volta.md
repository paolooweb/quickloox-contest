---
title: "I treni di una volta"
date: 2022-11-18T16:00:00+01:00
draft: false
toc: false
comments: true
categories: [Internet, Software]
tags: [Trenitalia]
---
![Tabellone ferroviario in crash](/images/tabellone.jpg "Una volta i treni arrivavano in orario. Ora, neanche più gli orari arrivano.")

Non avrei altro da dire che la battuta. Aggiungo ugualmente: vuoi vedere che una percentuale, anche piccola, dei ritardi e delle inefficienze ferroviarie italiane dipenda *davvero* anche da software obsoleto e di qualità pessima?

Se fosse, sarebbe un generatore di PIL negativo degno di nota. I tabelloni ferroviari hanno la complessità di una app da prima liceo e li vediamo. Infrastrutture più complesse, invisibili, invece?