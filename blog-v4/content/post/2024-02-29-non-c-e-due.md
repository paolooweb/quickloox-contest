---
title: "Non c’è due"
date: 2024-02-29T00:17:44+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [Terminale, Unix, Micro]
---
Tutte le volte che da iPad voglio modificare un file sul Mac via SSH nasce il dilemma: [pico](https://ss64.com/mac/pico.html) o [emacs](https://www.gnu.org/software/emacs/)?

Pico dovrebbe chiamarsi *poco*: lo strettissimo essenziale, con esperienza utente pessima. Per aggiungere un cancelletto dentro un file di configurazione e veloce e indolore; manca tutto il resto però e su file più complessi, con dentro Unicode magari, possono esserci sorprese.

Emacs è troppo: molte volte salpa una portaerei per affrontare una zanzara. È vero che c’è un sacco da imparare e che molte volte si tratta di un buon pretesto per approfondire la conoscenza del programma. È vero che a volte proprio è una risorsa esagerata per il bisogno.

Per questo do una possibilità anche a [micro](https://github.com/zyedidia/micro).

Micro, come dice il nome, prende le mosse da pico e nano ma poi salta in avanti, per esempio con il supporto del mouse, il copiaincolla, la compatibilità moderna e serena con Unicode, una architettura a plugin ed estensioni che si possono creare in linguaggio [Lua](https://www.lua.org).

Ci sono macro, cursori multipli, schermo splittabile… insomma, è lontanissimo da emacs ma ci si può fare davvero tanto e la difficoltà di utilizzo è minima. I principali comandi da tastiera, se non venisse usato *ctrl* al posto di Comando, potrebbero essere quelli di una app standard per Mac. E sono tutti modificabili toccando un file Json.

Molti programmi fatti per girare nel Terminale a volta pasticciano su Mac. Micro no. Ho provato a scaricare l’eseguibile binario e farlo lanciare via Finder da [Warp](https://www.warp.dev); poi l’ho installato dentro il sistema con [Homebrew](https://brew.sh). Successo indolore in entrambi i casi.

Non che abbia veramente risolto il dilemma di partenza; semmai l’ho trasformato in trilemma. In compenso dispongo di un grado di libertà superiore e posso motivare meglio la mia scelta. Poi un editor in più si prova sempre.