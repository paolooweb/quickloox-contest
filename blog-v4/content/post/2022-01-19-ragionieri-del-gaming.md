---
title: "Ragionieri del gaming"
date: 2022-01-19T00:01:17+01:00
draft: false
toc: false
comments: true
categories:
tags:
---
Microsoft ha acquisito una software house capace di produrre giochi fuori dal coro e con quel qualcosa in più, per trasformarla in una macchina da soldi che guarda solo a profitto, produce a questo scopo giochi ordinari e uguali a tutti gli altri, spesso o in prima battuta solo per i propri sistemi.

Parlo naturalmente di Bungie, un tempo autrice di capolavori come la serie [Myth](http://myth.bungie.org) o [Marathon](http://marathon.bungie.org) e oggi, dopo l’acquisizione del 2000, appiattita sullo [sparaspara multiplayer online](https://www.bungie.net) che fanno tutti, tutti allo stesso modo.

La copertina dell’evento per i trent’anni dell’azienda potrebbe essere incollata con poche varianti su qualsiasi altro sito di gioco concorrente e nessuno se ne accorgerebbe.

Il problema non è il monopolio o l’esclusiva per Xbox; è ingoiare qualsiasi cosa abbia un vago sentore di originalità e spianarla a uso e consumo dei ragionieri al reparto contabilità, che faccia soldi non importa come, più soldi non importa perché, ancora più soldi qualunque sia il prezzo.

_Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile._
