---
title: "Destra, sinistra: basta"
date: 2020-04-26
comments: true
tags: [Andreessen, Horowitz, Netscape, Horowitz, Gaber]
---
Interrompiamo le trasmissioni per parlare di un articolo di Marc Andreessen, creatore del *browser* [Netscape](https://www.versionmuseum.com/history-of/netscape-browser), uno dei primi a essersi arricchito con Internet e oggi investitore in compagnie e tecnologie promettenti per mezzo di [Andreessen Horowitz](https://en.m.wikipedia.org/wiki/Andreessen_Horowitz).

Andreessen ha scritto una pagina di storia già una volta con il suo articolo [Perché il software sta mangiando il mondo](https://www.wsj.com/articles/SB10001424053111903480904576512250915629460) sul *Wall Street Journal*. Spiega perché qualunque azienda è destinata a (e dovrebbe) diventare una azienda di software. Dovrebbe anche essere una lettura obbligata nella scuola dell’obbligo.

Lo ha rifatto, con [È tempo di costruire](https://a16z.com/2020/04/18/its-time-to-build/). È una lettura potentissima, obbligata per qualsiasi adulto, che farà storia anche se data a pochi giorni fa, su quello che manca al mondo per riprendersi dalla pandemia ma anche prosperare dopo: la voglia di *costruire*.

Andreessen ne ha anche per il mondo politico: una lezione che molti italiani dovrebbero capire e capire subito. Qui sotto, qualche paragrafo.

§§§

*La destra parte da una posizione più naturale, sebbene compromessa. Generalmemte è per la produzione, troppo spesso si fa corrompere da forze che ostacolano la concorrenza di mercato e la costruzione di cose. La destra deve lottare duramente contro il capitalismo a favore degli amici, i regolamenti che intrappolano, gli oligopoli ossificati, i paradisi fiscali e i buyback amici degli investitori che sostituiscono l’innovazione amica del consumatore (e, in un tempo più lungo, anche dell’investitore).*

*È ora che la destra sostenga a gran voce, senza compromessi, senza sensi di colpa investimenti aggressivi in nuovi prodotti, nuove industrie, nuove fabbriche, nuova scienza, nuovi grandi salti in avanti.*

*La sinistra parte in molte di queste aree con un pregiudizio favorevole verso il settore pubblico. Al che dico, si dia prova della sua superiorità! Dimostriamo che il settore pubblico può costruire ospedali migliori, scuole migliori, trasporti migliori, città migliori, case migliori. Finiamola di proteggere il vecchio, il consolidato, l’irrilevante; mettiamo il settore pubblico a lavorare pienamente sul futuro. Milton Friedman una volta disse che il più grande errore del settore pubblico è giudicare politiche e programmi dalle loro intenzioni invece che dai loro risultati. Invece che prenderlo come un insulto, lo si prenda per una sfida: costruire nuove cose e mostrarne i risultati!*

§§§

Non riesco a trovare parole migliori per commentare l’Italia capace di fare una questione di bandiera perfino di una pandemia, prigioniera di retorica e ideologia, popolata di gente sempre più ignorante man mano che si sale nelle gerarchie dei partiti e dello stato (minuscolo), che vota per i propri interessi – e ci mancherebbe – ma si mobilita contro quelli altrui (che è demenziale).

Chi pratica la politica come una religione, vota lo stesso partito per tutta la vita, vede zero ragioni e qualsiasi torto nell’avversario, confonde *destra* e *sinistra* (in qualunque ordine) con *sopra* e *sotto*, è pregato di leggersi Andreessen, come tutti, ma leggere tre volte i paragrafi qui sopra e farsi un profondo esame di coscienza.

[Destra, sinistra: basta](https://youtu.be/4kuySYnIvt4).

<iframe width="560" height="315" src="https://www.youtube.com/embed/4kuySYnIvt4" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
