---
title: "Mal comune, mezza verità"
date: 2014-06-16
comments: true
tags: [bitrot, Hfs+, Swift, Ntfs, ext4, Windows, Linux]
---
La partenza, [su Aymeric on Software](http://blog.barthe.ph/2014/06/10/hfs-plus-bit-rot/):

>Hfs+ è un filesystem terribilmente vecchio con serie lacune. Spero sinceramente che Apple esca con un aggiornamento per il convegno sviluppatori del 2015. Dopotutto hanno lavorato su Swift per i quattro anni passati e lo abbiamo saputo solo ora.<!--more-->

Tra i difetti del sistema che maneggia i file sui dischi di Mac c’è anche quello noto nel gergo come *Bit Rot*, putrefazione dei bit. È fisicamente e statisticamente inevitabile che, nel tempo, qualche bit sul disco si inverta, da zero a uno o viceversa. Perché sia faccenda grave, è esperienza personale dell’autore:

>Ho una raccolta di 15.264 foto iniziata attorno al 2006. Hfs+ ha perso un totale di 28 file in sei anni.

Effettivamente grave. A fine articolo viene aggiunto un poscritto che integra le osservazioni pervenute attraverso Internet a risposta dell’articolo, tra cui questa:

<blockquote class="twitter-tweet" lang="en"><p>As much as I dislike HFS+, bitrot is actually a problem shared by most popular filesystems. Including NTFS and ext4.</p>&mdash; Aymeric Barthe (@bartheph) <a href="https://twitter.com/bartheph/statuses/477360712730550272">June 13, 2014</a></blockquote>
<script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>

Si tratta di un problema condiviso da tutti i sistemi operativi del disco più diffusi, anche su Windows e su Linux.

Si capisce allora che il problema è di tecnologia che non si è ancora evoluta a sufficienza per la diffusione di massa e questo comprende anche Hfs+. Magari l’*incipit* dell’articolo poteva essere più ecumenico.