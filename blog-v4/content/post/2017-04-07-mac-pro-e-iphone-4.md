---
title: "Mac Pro. E iPhone 4"
date: 2017-04-07
comments: true
tags: [Gruber, Daring, Fireball, iPhone, Mac, Pro, MacBook, Schiller]
---
Un numero oramai pazzesco di anni fa [ho scritto un post](https://macintelligence.org/posts/2010-07-19-la-presa-di-sale/) su un evento storico, se non altro, per la distanza cronologica: l’evento stampa organizzato da Apple per fare il punto su iPhone 4, che aveva il problema del *death grip*. Ossia, perdeva segnale se la mano copriva le antenne di bordo, come tutti i telefoni cellulari su questo pianeta.<!--more-->

Pochi giorni fa si è tenuto [un altro evento stampa di Apple](http://daringfireball.net/2017/04/the_mac_pro_lives), che sembrerebbe diametralmente opposto: invece che decine di giornalisti erano una manciata, invece che parlare del prodotto più piccolo hanno parlato di quello più grosso, anziché parlare di un prodotto esistente si sono concentrati su qualcosa che ancora non esiste: l’a prossima evoluzione di Mac Pro (e iMac). Volendo trovare un altro opposto, un evento riguardava un oggetto venduto in milioni di esemplari e l’altro un oggetto venduto in migliaia di esemplari.

Il raccordo tra i due episodi, che al momento restano più o meno i soli di questo genere nella storia di Cupertino, è che *hanno spiegato come stanno le cose*. Nessun problema è stato risolto, nessun *bug* è stato corretto, nessun cambio di rotta è stato ordinato dal timoniere o chi per lui.

Ma come? E l’ira dei professionisti abbandonati? E lo scandalo di non avere un Mac Pro aggiornato? Eccetera eccetera?

Immagino che a raccontare dettagliatamente e con competenza il fatto tecnico provvederà [Sabino](https://melabit.wordpress.com). Qui mi limito a spiegare perché Apple ha convocato pochi commentatori selezionati per spiegare che cosa è successo e che cosa succederà.

Con questi pochi, sia pure non esplicitamente, Apple ha ammesso di avere commesso un errore nel prevedere l’evoluzione delle macchine come Mac Pro. Invece in direzione di tante schede grafiche capaci di lavorare in concerto, l’industria è andata verso una sola scheda grafica superpotente, per esempio; l’architettura di Mac Pro non è adatta a una evoluzione di questo tipo. Ancora, la scelta dei connettori di espansione non è stata delle più felici e si è ritrovata superata dagli eventi. Per questo Apple sta riprogettando integralmente Mac Pro in modo che ridiventi una macchina competitiva e lo rimanga nel tempo attraverso una facile aggiornabilità. Arriveranno anche nuovi iMac e nelle linee di prodotto resterà anche Mac mini. Craig Federighi, responsabile dei sistemi operativi, ha inoltre dichiarato ufficialmente che la questione delle capacità di automazione di Mac, messa in dubbio da [recenti sviluppi societari](http://www.macintelligence.org/blog/2016/11/20/aut-out/), rimane *super importante*.

Mentre gli iMac arrivano quest’anno, tuttavia, Mac Pro arriverà più tardi. Probabilmente l’anno prossimo, ma certamente non questo.

Il tema resta quello di quell’evento su iPhone 4. Non è che ci fosse un problema reale; *la gggente* parlava come se ci fosse stato un problema. Così Steve Jobs ha fatto sentire la sua voce.

Se si legge tra le righe delle dichiarazioni di Phil Schiller e degli altri che hanno incontrato i giornalisti, si capisce che i Mac Pro venduti, anche quando sono usciti, sono stati veramente pochi. Il bilancio dell’azienda o la salute del mondo Mac potrebbero tranquillamente prescindere da quei numeri.

Se però il 2017 fosse trascorso senza avere alcuna notizia su Mac Pro, *la gggente* avrebbe scritto che il prodotto è stato certamente abbandonato, che Apple lascia i professionisti al loro destino eccetera eccetera. Se c’è stato l’ultimo evento stampa, è per evitare che questo accadesse.

Riassumendo: *Apple è interessata al mondo professionale indipendentemente da quanto fa vendere*. Altrimenti non si prenderebbe la briga di riprogettare Mac Pro e soprattutto di incontrare i giornalisti: di computer nuovi ne progettano tutti gli anni, di eventi così ce ne sono stati pochissimi.

Certamente, la situazione di chi vorrebbe un aggiornamento è purtroppo la stessa di chi deve pazientare (a meno di non accontentarsi di uno *speed bump* al Mac Pro esistente previsto per i prossimi giorni). Ma questo non è abbandonare: è rimediare a un errore. Sarebbe stato molto peggio se Apple avesse continuato a proporre iterazioni annuali di Mac Pro basate su un concetto sbagliato. Quello sì, è abbandono: ti vendo una macchina che non ha più senso e me ne disinteresso.

Disgraziatamente riprogettare una macchina come Mac Pro non è banale. Non per Apple almeno. Gli altri, chi più chi meno, fanno cassoni ed è più facile.

Così tocca aspettare. Ma Apple, esponendosi, ha messo a rischio una cifra importante di fatturato dei prossimi mesi (non so quanti compreranno un iMac prima che esca quello nuovo…). E ha gli occhi puntati addosso: il prossimo Mac Pro non può sbagliare. Vuol dire che vale la pena di aspettarsi qualcosa di valido.