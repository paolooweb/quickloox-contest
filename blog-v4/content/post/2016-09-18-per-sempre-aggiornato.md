---
title: "Per sempre aggiornato"
date: 2016-09-18
comments: true
tags: [Apple, Newton, II, ProDos]
---
E così ProDos per Apple II [ha ricevuto un aggiornamento software](http://www.callapple.org/uncategorized/announcing-prodos-2-4-for-all-apple-ii-computers/) dopo ventitré anni. Per computer usciti di produzione abbondantemente prima della fine del secolo scorso.

Mi viene in mente l’[aggiornamento per Newton](http://40hz.org/Pages/Patch%20711000) che permette alle unità ancora attive di segnare la data esatta anche dopo il 2010 e offre possibilità di connessione con un Mac anche se le Newton Connection Utilities originali hanno cessato di funzionare da molto tempo.

Penso a quelli che che si preoccupavano, fine anni novanta, dei rischi connessi con lo scegliere Mac *perché se Apple fallisce poi non c’è più supporto*.

Apple non è fallita, pare, ma certamente Newton o Apple II non sono argomenti particolarmente attuali a Infinite Loop. Eppure, vent’anni dopo, qualcuno si siederà davanti a un Apple II per provare una nuova edizione del sistema operativo.