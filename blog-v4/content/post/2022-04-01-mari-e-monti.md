---
title: "Mari e monti"
date: 2022-04-01T01:44:55+01:00
draft: false
toc: false
comments: true
categories: [Internet]
tags: [April Fool’s]
---
1) La Russia, per evocare il ricordo dei fasti imperiali, torna al calendario giuliano.

2) Dyson lancia una mascherina che purifica l’aria abbinata a cuffie antirumore che purificano il suono.

3) L’Ucraina stampa francobolli a tema bellico.

4) I Genesis annunciano per il 2023 una tournée in formazione rinnovata, con il figlio di Phil Collins alla batteria e la figlia alla voce.

5) Trump annuncia di avere fatto buca al primo colpo giocando a golf.

6) Per portare una immagine da un *device* Apple a un altro, la si pizzica con tre dita che si rilasciano sopra l’apparecchio di destinazione.

7) Un Comune italiano adotta a livello sperimentale la settimana lavorativa di due giorni per i dipendenti del municipio.

8) Hacker smonta un Mac mini e ne ricava un portatile fatto in casa.

9) Il prossimo macOS si chiamerà Altamont e il lancio verrà celebrato con la collaborazione dei Rolling Stones.

10) Debutta il vaccino antiCovid per cani e gatti (con una soglia di peso minimo).

Visto che ci si sveglia la mattina ed è impossibile credere a quello che si legge, tanto vale mescolare la ciccia e il pesce. Vediamo chi sopravvive.