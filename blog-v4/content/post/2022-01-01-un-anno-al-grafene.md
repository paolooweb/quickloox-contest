---
title: "Un anno al grafene"
date: 2022-01-01T02:06:24+01:00
draft: false
toc: false
comments: true
categories:
tags:
---
Un segnale di serenità e ottimismo per affrontare questo nuovo anno.

Grazie a una cognata vanesia che si è presentata a un pranzo di famiglia pur sapendo di non essere del tutto sana, ho avuto il privilegio di entrare nell’elenco *contatti stretti di un positivo*.

Così la sede milanese della [Agenzia per la Tutela della Sanità](https://www.ats-milano.it) (credo) mi ha invitato a consultare una pagina apposita del loro sito.

Per accedere, mi hanno chiesto il codice fiscale. Significa che hanno una certa fiducia in me e che non lo hanno (misteriosamente, perché sui fascicoli sanitari, ma non stiamo a spaccare il capello).

Ho fornito il codice fiscale, ma non va bene. Il sito dice che è scorretto. Ovvero, sono convinti di avere il mio codice fiscale. Ma allora perché me lo chiedono?

Esco dalla fiction: ho un doppio nome. La cognata vanesia deve avermi inserito nell’elenco con un nome solo. Il loro supersoftware di gestione deriva automaticamente il codice fiscale dai dati a sua disposizione e, mancandogli un nome, ne genera uno sbagliato.

Allora ho composto il numero telefonico.

Ero solo il trentottesimo in coda, con un tempo di attesa garantito non superiore a *un’ora e quattordici minuti*.

I complottisti non si possono più sentire, ma questi fanno del loro meglio per impedirti anche di avere una conversazione. Sarà un effetto collaterale del grafene?

*Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile.*
