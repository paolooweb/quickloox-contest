---
title: "Trascrittori che cambiano il mondo"
date: 2020-03-06
comments: true
tags: [Mori, Morrick, Jobs, Tesler]
---
La lettura del weekend è composta da due letture, fornite da [Riccardo](http://morrick.me) che con pazienza ha trascritto due splendide interviste, [una a Steve Jobs](http://morrick.me/archives/8816) e [una a Larry Tesler](http://morrick.me/archives/8801), uscite dai documentari della serie [The Machine That Changed The World](https://waxy.org/2008/06/the_machine_that_changed_the_world/).

Non sempre c’è tempo e modo per guardare i filmati originali da cui provengono le interviste e con l’inglese parlato c’è chi fatica più che con quello scritto. E poi, con il testo, ce la si cava anche con un iPhone in poltrona senza perdere niente

Riccardo ha svolto un lavoro straordinario e avrà la mia attenzione.