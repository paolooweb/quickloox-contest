---
title: "Perdite e profitti"
date: 2017-01-30
comments: true
tags: [LG]
---
Continuo a capacitarmi con fatica e a porre la stessa domanda: compreresti un computer da tasca da una azienda che ci perde soldi, in maniera sistematica, senza una strategia precisa di recupero? Che qualità, che progettazione, che supporto ci si aspetta?

Continuo perché le notizie si susseguono e scopro adesso che LG lo scorso trimestre ha perso [duecento milioni di dollari](https://www.engadget.com/2017/01/25/lg-wont-share-its-disappointing-mobile-sales-figures/) nel complesso, “grazie” a perdite nel settore smartphone (fortuna che sono *smart*) che pesano quasi il doppio.

Di più: è dall’estate 2015 che LG perde denaro in questo, chiamiamolo, *business*.

Certo, ho sentito millemila persone blaterare che non avrebbero scelto un Mac per paura che l’azienda svanisse. Tuttavia Apple ha iniziato a crescere prima con nuovi Mac, poi aprendo un’attività nel mercato della musica, poi una nel mercato della telefonia.

LG che innovazioni sta preparando?