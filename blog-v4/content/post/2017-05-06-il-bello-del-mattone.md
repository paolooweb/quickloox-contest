---
title: "Il bello del mattone"
date: 2017-05-06
comments: true
tags: [Workflow, Automator, iOS, macOS, iPad]
---
Mi sono chiesto se si potesse in qualche modo automatizzare il ritaglio di un gruppo di foto su iOS.

Ho acceso [Workflow](https://appsto.re/it/2IzJ2.iWorkflow:) e in tre minuti, con tre mattoni software, ho creato il mio primo flusso di lavoro in iOS.

Niente di cui vantarsi: è veramente facile, applicandosi su una operazione semplice come questa.

Molto su cui riflettere: più che mai bisogna chiedersi se facciamo a mano qualcosa che il computer potrebbe fare al posto nostro. Dalla capacità di essere noi a trovare la soluzione, invece che vederla arrivare da qualcun altro, possono dipendere molti futuri

Qualcosa da sognare: Workflow è stato acquistato da Apple e ha una meccanica molto, molto simile a quella di Automator su macOS. Metti che in una futura iterazione di sistemi operativi veda la luce un sistema di automazione facilmente programmabile trasversale, dove un flusso di lavoro possa progredire passando da Mac a iPad e viceversa.
