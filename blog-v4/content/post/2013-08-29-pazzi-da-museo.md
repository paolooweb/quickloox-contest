---
title: "Pazzi da museo"
date: 2013-08-29
comments: true
tags: [iPad, Smithsonian, Planetary, GitHub, iOS, Android, design]
---
Da adesso, se mi chiederanno la differenza tra iOS e Android, risponderò *il codice di una app per iPad [è stato acquisito dal museo Smithsonian del design](http://www.cooperhewitt.org/object-of-the-day/2013/08/26/planetary-collecting-and-preserving-code-living-object)*.<!--more-->

[Planetary](https://itunes.apple.com/it/app/planetary/id432462305?l=en&mt=8) è una *app* abbandonata, ancora scaricabile da iTunes, che suona musica con una visualizzazione a tema spaziale, che il museo ha trovato interessante dal punto di vista del *design* dell’interazione e della rappresentazione interattiva di dati. È gratis e chiunque può verificare il concetto.

Ancora più interessante è un interrogativo che si pone allo Smithsonian: la conservazione dell’opera. L’istituto possiede già oggetti realizzati in stampa 3D a partire da un algoritmo e vari esemplari di tecnologia hardware: ma come preservare il software in modo rispondente ai criteri museali e contemporaneamente conservarlo funzionante attraverso gli anni?

Il museo ha deciso di rendere libero il codice, disponibile a chiunque sugli archivi di GitHub [Planetary](https://github.com/cooperhewitt/Planetary) e [PlanetaryExtras](https://github.com/cooperhewitt/PlanetaryExtras). In questo modo Planetary avrà la possibilità di sopravvivere all’obsolescenza degli apparecchi per cui è stato concepito e magari trovare nuovi sistemi su cui funzionare.

La differenza tra iOS e Android è che il primo può ispirare *design* software degno di essere conservato. È un’affermazione da pazzi, per un mondo che ha dimenticato l’idea di valore e pensa solo al prezzo. Mi piace per questo.