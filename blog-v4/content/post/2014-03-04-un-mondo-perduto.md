---
title: "Un mondo perduto"
date: 2014-03-04
comments: true
tags: [McCracken, Newton]
---
Ringraziamenti a **Phil** per avermi trasmesso questo [tweet](https://mobile.twitter.com/harrymccracken/status/439870839387721728/photo/1?screen_name=harrymccracken) di Harry McCracken.<!--more-->

<blockquote class="twitter-tweet" lang="en"><p>1995 issue of PC World with possibly the only issue of Newton World as an insert. <a href="http://t.co/9vDPESBBmX">pic.twitter.com/9vDPESBBmX</a></p>&mdash; Harry McCracken (@harrymccracken) <a href="https://twitter.com/harrymccracken/statuses/439870839387721728">March 1, 2014</a></blockquote>
<script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>

Niente nostalgia perché il mio Newton è vivo e vegeto. Lo ammetto, meno sollecitato di un tempo.

Semmai ricordo di un’epoca che, per ogni fenomeno tecnologico, vedeva nascere un Fenomeno-world in edicola.

Stiamo andando verso tempi certamente diversi, non necessariamente peggiori. Certo è più difficile che una rivista in embrione diventi oggetto di una piccola preziosa scoperta paleoinformatica.