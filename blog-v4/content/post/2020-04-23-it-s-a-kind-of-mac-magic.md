---
title: "It's a Kind of Mac Magic"
date: 2020-04-23
comments: true
tags: [Mac, mini, iPad, Immuni, Apple, Google, coronavirus, VPN, iTunes, ssh, Terminale, iMessage]
---
Ti distrai un attimo e succedono cose che sbalordiscono, come [l’indiscrezione che Immuni](https://www.ilsole24ore.com/art/l-app-immuni-cambia-seguira-modello-decentralizzato-apple-e-google-ADcBF4L) userà la piattaforma [Privacy-Preserving](https://www.apple.com/covid19/contacttracing) di Apple e Google. Eppure ci sono magie più piccole capaci di incantarmi maggiormente.

In serata il mio Mac era connesso via VPN a una scuola americana.

Mia nipote, da casa sua, controllava lo schermo del mio Mac via iMessage e faceva i compiti sulla piattaforma della scuola americana.

Mia figlia si addormentava mentre il sistema audio di casa trasmetteva una playlist di sottofondo presente, ovvio, su Mac.

Contestualmente, Mac era connesso via ssh alla macchina virtuale in cloud che pubblica questo blogghino.

Io ero collegato via ssh da iPad a Mac e lavoravo sulla macchina virtuale.

Su un normalissimo Mac mini, su un normalissimo macOS, senza moduli di software per server o altri trucchi, con banda ordinaria e come unico accessorio extra una AirPort Express più vecchia delle due figlie messe insieme.

Hardware normale, software scontato e sotto gli occhi la meraviglia di questa rete tanto facile da usare male quanto potente per farci vivere, lavorare, studiare, giocare, rilassare meglio.

Al centro della [magia](https://www.youtube.com/watch?v=0p_1QSUsbsM), Mac.

<iframe width="560" height="315" src="https://www.youtube.com/embed/0p_1QSUsbsM" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
