---
title: "Il prezzo della nostalgia"
date: 2019-01-02
comments: true
tags: [Cydia, iPhone, Saurik, Freeman, Raspberry]
---
Bisogna ricordare che si entra nel nuovo anno con [l’addio agli acquisti su Cydia](https://www.engadget.com/2018/12/15/cydia-jailbreak-app-store-shuts-down/), l’app store del jailbreaking su iOS.

Non è l’addio a Cydia, che persiste; tuttavia non è più possibile acquistarvi software. Che pare il preludio a una successiva decisione più radicale, dato che le ragioni addotte sono principalmente costi e sicurezza.

Jay *Saurik* Freeman, il genio dietro Cydia, mantiene una piattaforma che è in perdita e lo è ancora di più se permette di acquistare. Peggio ancora, un bug ha causato perdite ulteriori, visto che consentiva di comprare a spese dell’account di terzi in situazioni dove la vittima visitava *repository* non fidati.

Sarebbe facile atteggiarsi a grillo parlante e alzare il ditino: chi ha difeso il diritto a installare sul proprio iPhone quello che pareva, nel nome della libertà, lo sospende per difendere persone da rischi che potrebbero correre. Non molto diverso, alla fine, da quello che ha sempre promosso Apple.

La cosa da dire invece è un’altra. Mandare avanti un app store, anche artigianale, è una faccenda maledettamente costosa e problematica. Perché qualsiasi occasione di abusare del sistema verrà sfruttata da qualcuno e parare tutto in modo che nessuno si faccia male è pressoché impossibile. Quello che diamo per scontato, in termini di sicurezza e qualità, è il frutto di fatica continua e difficile.

Ho anch’io usato il *jailbreaking* sul mio iPhone prima edizione e non ho mai cambiato idea. Bella iniziativa, per scopi specifici, per pochi. La libertà, il diritto, la ribellione contro le multinazionali, tutte scemenze. L’unica libertà che si possiede nel mondo della tecnologia è quella di chi possiede la conoscenza; chi non la possiede, a un certo punto del suo cammino deve fidarsi di qualcuno. Saurik ha finito per scoprire che non poteva fidarsi neanche di gente che popolava di prodotti il suo store. Lui di conoscenza ne ha un bel po’.

Il *jailbreaking* ha fatto il suo tempo, da tempo. Andiamo avanti e, se vogliamo sperimentare e prenderci i nostri rischi, piuttosto usiamo un [Raspberry Pi](https://www.amazon.it/Raspberry-Pi-3-modello-B/dp/B07BDR5PDW), molto più divertente e istruttivo.