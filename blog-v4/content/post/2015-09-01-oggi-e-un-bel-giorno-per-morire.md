---
title: "Oggi è un bel giorno per morire"
date: 2015-09-01
comments: true
tags: [Flash, Chrome, Amazon]
---
Mi riferisco alla decisione di Chrome, che da oggi inizia meritoriamente a [bloccare in automatico i contenuti web *non rilevanti per la pagina* erogati in Flash](http://arstechnica.com/information-technology/2015/08/google-chrome-will-block-auto-playing-flash-ads-from-september-1/).<!--more-->

Significa parti di sito realizzate in Flash per il puro gusto di farlo, video che partono da soli di solito a scopo pubblicitario, animazioni inutili e gratuite che hanno il solo obiettivo di consumare batteria o adescare compratori gonzi.

Tutto sarà semplicemente fermato sul nascere e sarà possibile fare clic per sbloccare il contenuto “desiderato”. Ma intanto molta gente inizierà a rendersi conto del perché la sua navigazione rallenta e anche di quanto serva veramente quella roba.

Sempre da oggi, Amazon [rifiuta inserzioni pubblicitarie in Flash](http://advertising.amazon.com/ad-specs/en/policy/technical-guidelines).

La morte di una tecnologia anacronistica libera lo spazio per cose nuove e migliori. Magari ricorderemo questo settembre come il mese della liberazione definitiva da Flash.