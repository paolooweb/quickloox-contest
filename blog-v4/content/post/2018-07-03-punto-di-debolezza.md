---
title: "Punti di debolezza"
date: 2018-06-03
comments: true
tags: [Inc]
---
Amazon è un colosso globale, che sta arrivando a competere con Apple nella capitalizzazione.

Il suo amministratore delegato [ha vietato l’uso di PowerPoint](https://www.inc.com/carmine-gallo/jeff-bezos-bans-powerpoint-in-meetings-his-replacement-is-brilliant.html) nelle riunioni interne. Le persone hanno mezz’ora per studiare sei fogli e poi fanno *storytelling*,

Sono parzialmente in disaccordo verso una misura così radicale contro le *slide*, che in varie circostanze sono effettivamente utili.

Se però limita lo strapotere omologatore di PowerPoint, ben venga. Apparentemente, Amazon funziona solo meglio, senza PowerPoint, ed è evidente che il punto-di-forza che dà il nome al programma sia una mera invenzione.
