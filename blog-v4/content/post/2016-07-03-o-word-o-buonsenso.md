---
title: "O Word o buonsenso"
date: 2016-07-03
comments: true
tags: [LaTeX, Word]
---
Consiglio in tutti gli offici del pianeta Italia la lettura di [Da LaTeX a Word (e ritorno) su MelaBit](https://melabit.wordpress.com/2016/07/02/da-latex-a-word-e-ritorno/).<!--more-->

Chiunque abbia un briciolo di buonsenso e una copia di Word dovrebbe gettare quest’ultima nel Cestino, e vuotare il Cestino.

Altrimenti significa che il presupposto di base è falso e il buonsenso è assente, anche in quantità minima.

Chi mi conosce sa quanto sia raro sentirmi raccomandare la lettura di un articolo in italiano. Ecco, appunto, quando accade è perché proprio la lettura è necessaria.

*[Di sicuro il [libro di Akko su macOS e dintorni](https://macintelligence.org/posts/2016-06-15-un-kickstarter-per-cambiare-il-libro/) non sarà scritto su Word: sarebbe un segnale di pessima qualità del lavoro. [Facciamolo conoscere e aderiamo al progetto](https://www.kickstarter.com/projects/295108589/cuore-di-mela-i-segreti-di-apple-iphone-e-mac) su Kickstarter.]*