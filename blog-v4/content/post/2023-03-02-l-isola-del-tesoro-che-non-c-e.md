---
title: "L’isola del tesoro che non c’è"
date: 2023-03-02T18:15:24+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [ebook, Dropbox]
---
Un segreto di Pulcinella di chi si interessa ai libri, per lavoro o per diletto o per entrambi, è l’esistenza di una cartella condivisa su Dropbox contenente un gran numero di libri piratati. Immagino anche che esistano più cartelle, di cui ignoro l’esistenza.

Dico *piratati* e non *rubati* o *illegali* con intenzione; sono rubati e sono anche per lo più illegali, ma il punto è che sono piratati.

Significa che in forma di ebook sono sballati, mancanti di qualche pezzo, con i Css fuori posto. Se sono Pdf, a volte sono scansioni approssimative, immagini distorte, aspetto generale incoerente.

Insomma, non sono stati presi a qualcuno, ma lavorati di fretta, in modo incompetente e male, per rimetterli in circolo.

Anni fa avevo per così dire adottato uno di questi ebook e, per gusto e per prendere confidenza con strumenti e sintassi, impiegavo qualche momento libero per rileggere e correggere refusi e imprecisioni. Avevo anche cancellato qualche doppione dalla cartella; spesso di un libro piratato esistono più versioni concorrenti, ognuna sbagliata a modo suo. A volte, l’autore della copia piratata e cestinata rimetteva il file su Dropbox, scrivendo qualcosa di rabbioso nel nome file (il modo più veloce per comunicare qualcosa ai frequentatori della cartella).

Da tempo ho lasciato perdere, poco tempo e poco interesse per svuotare il mare con il cucchiaio.

Non ho mai aperto alcuna di queste edizioni piratate.

Questo è tutto quanto ho da dire sulla questione della pirateria dei libri e sulle politiche degli editori.