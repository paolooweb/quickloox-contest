---
title: "Esistono le reti"
date: 2017-10-29
comments: true
tags: [iPad]
---
Sì, qualcuno sostiene che iPad non sia un vero computer perché non si può usarlo per programmare. O che non si possa programmare su iO ad perché non è un computer.

Esiste un articolo dal titolo [iPad Pro come computer principale per programmare](https://jann.is/ipad-pro-for-programming/), scritto da un *backend engineer*, ossia uno che lavora tutto il giorno con le mani dentro il cofano del sistema informatico e di programmazione deve occuparsene prima e dopo i pasti.

La sintesi del pezzo:

>iPad Pro con Smart Keyboard collegabile a un server con [ZSH](http://www.zsh.org/), [tmux](https://github.com/tmux/tmux/wiki) e [neovim](https://neovim.io/) è una fantastica macchina portatile per lo sviluppo.

*Sviluppo* sta per sviluppo di software. Uno sviluppatore crea software rilevante per dimensioni e complessità.

Poi, certo, c’è di mezzo un server. Il fatto è che siamo nel 2017 e esistono le reti. Nessun computer è un’isola, da vent’anni almeno, ed eseguire codice su una macchina remota è attività che si esegue in ambito professionale dai tempi dei *mainframe* degli anni cinquanta.

Consiglio un’occhiata alle *app* per iOS menzionate nell’articolo. Meritano.
