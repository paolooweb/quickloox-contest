---
title: "Cancellando qualcosa resterà"
date: 2019-10-15
comments: true
tags: [Safari]
---
Sembra inevitabile continuare a parlare di privacy dopo le notizie non piacevoli dei pasticci di Apple con la Cina tra [bandierine di Taiwan](https://macintelligence.org/posts/2019-10-08-rubabandiera/) e [dati sulla navigazione sicura](https://macintelligence.org/posts/2019-10-14-sicuri-a-casa-propria/), però stavolta è solo questione tecnica, con lieto fine.

È successo che dopo l’aggiornamento a Safari 13, vari siti non si aprivano più. Tra cui YouTube, Twitter, Unsplash. Mentre tutto il resto funzionava normalmente.

Alla fine ho capito che si trattava di cancellare i dati memorizzati da Safari relativamente ai siti in questione, nelle Preferenze, alla voce Privacy, con un clic nel pulsante per gestire appunto i dati sui siti.

Cancellando i dati, ho riavuto i siti a disposizione, chiaramente dovendo reinserire una password dove è richiesto un accesso autenticato, *no problema*.

Può darsi che serva a qualcun altro. All’inizio pensavo che si trattasse di problemi dati dalle impostazioni piuttosto restrittive che Safari inizia ad applicare alla pubblicità e al tracciamento della navigazione, che invece possono restare come sono.