---
title: "Signori della corte"
date: 2019-09-10
comments: true
tags: [Avvocati, Strozzi, macOS, iOS, Pandoc, Markdown, LaTeX, Office]
---
La prova provata di tante sciocchezze che si ripetono incessanti riguardo all’indispensabilità di Office è l’esistenza nel sito [Avvocati e Mac](https://www.avvocati-e-mac.it/blog/2019/9/2/alcune-migliorie-al-mio-metodo-di-scrivere-atti-telematici-avanzati-in-markdown) di ampia, avanzata e chiarissima documentazione di uso di [LaTeX](https://www.latex-project.org/), [Markdown](https://daringfireball.net/projects/markdown/) e [Pandoc](https://pandoc.org/), adoperati per produrre con Mac e iOS atti di valore giudiziario dei quali, per forza di cose, non è facile improvvisare tipografia e impaginazione.

Direi anzi che questa è la testimonianza definitiva.
