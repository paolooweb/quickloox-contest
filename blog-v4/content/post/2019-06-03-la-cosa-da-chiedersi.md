---
title: "La cosa da chiedersi"
date: 2019-06-03
comments: true
tags: [LeanCrew, Scripting, Drang, Winer, WWDC]
---
Mancano pochissime ore a [WWDC](http://www.macintelligence.org/blog/2019/06/01/cari-amici-vicini-e-lontani/) e mi aspetto [da tempo](http://www.macintelligence.org/blog/2019/05/06/l-automazione-non-e-mai-troppa/) cose interessanti sul fronte dell’automazione personale.

Campo dove, più leggi più ti si aprono davanti orizzonti sconfinati. Il bello è che una ricetta non esiste; forse proprio la parte più difficile è scegliere un percorso, tra i tanti possibili, il che implica l’abbandono di tante alternative che ogni giorno, una per volta, improvvisamente si rifanno vive e promettenti più di prima.

Ci ho pensato mettendo a confronto Dr. Drang e Dave Winer. Il primo racconta di come [usa LaTex per realizzare relazioni da presentare in azienda](https://leancrew.com/all-this/2019/06/a-little-more-latex/), documenti il cui primo requisito tecnico è la possibilità di lavorarci da iPad o da Mac, indifferentemente, dipende dal mezzo che ha davanti.

Il secondo racconta in sintesi [come funziona il suo blog](http://scripting.com/2019/06/02/150411.html). In tempi di Wordpress-mania è come se sbarcassero gli alieni.

Buona [Wwdc](https://developer.apple.com/wwdc19/) a tutti. Il motto di quest’anno è *Write code. Blow Minds.* In fondo alle pagine di Winer, a parafrasare Kennedy, una volta la frase del giorno è stata *Non chiederti che cosa Internet può fare per te*.

In fondo, è la stessa cosa.