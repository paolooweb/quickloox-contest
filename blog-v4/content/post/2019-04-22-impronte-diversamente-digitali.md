---
title: "Impronte diversamente digitali"
date: 2019-04-22
comments: true
tags: [Facebook, biometria]
---
Mentre sui media girano tonnellate di materiali che spiegano quanto Facebook sia cattivo e mal rispettoso della *privacy*, si parla poco del nuovo sistema di identificazione e controllo biometrico che dovrebbe unificare la base dati europea in materia e creare un bacino di cittadini identificati biometricamente di dimensioni inferiori solamente al sistema cinese e quello indiano.

Gli interessati possono vedere una [presentazione del meccanismo](https://www.securityresearch-cou.eu/sites/default/files/02.Rinkens.Secure%20safe%20societies_EU%20interoperability_4-3_v1.0.pdf), che è stato approvato dal Parlamento Europeo ed è destinato a entrare in vigore in tempi non lunghi.

Personalmente la biometria non mi disturba. Nei mesi scorsi sono transitato dai confini statunitensi e cinesi e in ambedue i casi ho lasciato impronte digitali e foto del volto, come già fatto in una questura milanese per ottenere un passaporto, ovvero un permesso di chiedere il permesso di entrare in qualche altro Paese.

Potrei anzi sostenere di essere in qualche modo pioniere, dal momento che ho prestato servizio militare e lo stato deteneva le mie impronte già da molti anni.

Tuttavia la sete di dati personali di Facebook mi disturba assai meno, per quanto tendente all’illegalità e all’indifferenza verso il consenso consapevole dell’utente. Da Facebook, per quanto farraginosamente, si può uscire, stare fuori, guardare senza toccare. Da questi nuovi stati multinazionali, ancora più multinazionali delle multinazionali, non si può. E se capita un brutto incontro con l’agenzia delle entrate basato sul nulla (mi è capitato) o un errore giudiziario (per fortuna non mi è capitato), allentare la morsa dell’autorità è del tutto impossibile.

Le impronte digitali sono una cosa, l’identificazione digitale sistematica senza alcun controllo da parte del cittadino è tutta un’altra questione, che mi lascia inquieto.
