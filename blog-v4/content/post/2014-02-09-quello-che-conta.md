---
title: "Quello che conta"
date: 2014-02-10
comments: true
tags: [Pages]
---
Mi ero fatto l’appunto e anche [Mario](http://multifinder.wordpress.com) mi ha fatto notare la stessa cosa: Pages, che nella nuova e controversa versione unificatrice del formato applicava un ingestibile conteggio caratteri *privo degli spazi*, ora finalmente applica il conteggio senza spazi e anche quello con, oltre a quantificare parole, paragrafi e pagine.<!--more-->

Ho pensato ai [due feedback](https://macintelligence.org/posts/2013-11-26-un-metro-severo/) che a suo tempo ho lasciato al supporto Apple per chiedere che tornasse il conto dei caratteri come va fatto (il senso di contare i caratteri è di capire quanto sarà lungo il testo e solo un programmatore anestetizzato può ignorare che gli spazi contribuiscono alla lunghezza, dunque vanno contati).

Ho pensato a quanta gente si lamenta di cose che non gradisce, ma non lascia *feedback*. Vanno lasciati, anche più volte. Pages, come tanti altri prodotti, ha una [pagina apposta](http://www.apple.com/feedback/pages.html).

Ho pensato a chi si dichiara deluso perché lascia un *feedback* che non viene accolto nell’aggiornamento successivo o non viene accolto del tutto. Lo sviluppo software è una faccenda complicata e molto dilatata nel tempo. Probabilmente la sistemazione del conteggio dei caratteri in Pages era in programma da prima che uscisse la nuova versione! Oppure no, ma non lo possiamo sapere.

Ho pensato a chi la prende sul personale, perché crede di vedere ignorato il proprio *feedback*. Non lo si può sapere. Certamente è una goccia nel mare. Può darsi che a chiedere il conteggio degli spazi siamo stati in centomila, forse un milione, forse più. Ovvio che preso singolarmente, un *feedback* non conta. Eppure lasciarlo continua a essere la cosa giusta da fare.
