---
title: "Una lezione di evoluzione tecnologica"
date: 2020-01-23
comments: true
tags: [AirPods, Google, Translate, Winer]
---
Devo questa [perla](https://twitter.com/LouisAnslow/status/1219643485902508036) a [Dave Winer](http://scripting.com/2020/01/22.html): due ragazze dotate di AirPods si scambiano uno degli auricolari. Una scrive una frase in Google Translate e attiva la sintesi vocale *text-to-speech*. L’altra ascolta e poi risponde allo stesso modo. Conversazione senza farsi udire da alcuno.

<blockquote class="twitter-tweet"><p lang="en" dir="ltr">Kids are swapping AirPods in class then using text to speech to ‘talk’ without talking 🤩🤩🤩 <a href="https://t.co/moLxK1rzbv">pic.twitter.com/moLxK1rzbv</a></p>&mdash; Louis Anslow ✪ (@LouisAnslow) <a href="https://twitter.com/LouisAnslow/status/1219643485902508036?ref_src=twsrc%5Etfw">January 21, 2020</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

Il divertimento vero arriva con la lettura dell’intero thread.

>Sono più impressionato da mia figlia e le sue amiche che, in terza elementare, capiscono come Google Slides [possa essere trasformato in un piccolo Slack](https://twitter.com/dcurtisj/status/1219745895010299904) multicanale di classe.

<blockquote class="twitter-tweet"><p lang="en" dir="ltr">I’m more impressed by my daughter and her friends figuring out in 3rd grade that Google Slides could be turned into a kind of multiple-channels in-class Slack.</p>&mdash; Dan Curtis Johnson (@dcurtisj) <a href="https://twitter.com/dcurtisj/status/1219745895010299904?ref_src=twsrc%5Etfw">January 21, 2020</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

D’altronde c’era quell’articolo di *The Atlantic* su [Google Docs usato a scuola dagli adolescenti come chat di gruppo](https://www.theatlantic.com/technology/archive/2019/03/hottest-chat-app-teens-google-docs/584857/).

Facile fare ironia o commentare che ai miei tempi eccetera eccetera. Nel resto del thread emerge tutto un divario generazionale. Mentre la storia dei nativi digitali è una scemenza, è vero che gli adolescenti di oggi – come quelli di sempre – si inventano tecniche imprevedibili e magari anche ingegnose per ottenere quello che è il loro scopo primo: parlarsi, conoscersi, conoscere sé attraverso gli altri, sfuggire ai genitori e disporre di uno spazio intimo in cui cimentarsi come aspirante adulto.

La scuola che fa? Proibisce i cellulari, lamenta le distrazioni, continua a proporre le lezioni frontali di una volta a ragazzi abituati a parlarsi *many-to-many* di continuo, persino troppo.

Si coinvolgano invece, questi ragazzi, e anche i loro cellulari. Se guardo le funzioni di collaborazione di Google Docs vedo un ambiente di grande potenzialità per trasmettere sapere e alimentare feedback. È gratis, funziona su qualunque aggeggio anche da due soldi e gli unici a doverlo imparare, come se ci fosse qualcosa da imparare, sono certi insegnanti.