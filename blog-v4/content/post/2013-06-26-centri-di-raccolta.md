---
title: "Centri di raccolta"
date: 2013-06-26
comments: true
tags: [Programmazione, XCode]
---
Si avvicina l’estate e mi assalgono le solite voglie di riempire almeno parzialmente le mie immense lacune programmatorie.<!--more-->

Siccome la sincronicità esiste, Dave Mark ha appena pubblicato una [pagina eccezionale di risorse](http://www.davemark.com/?p=1829) per la programmazione in XCode.

Mi dico sempre che è la volta buona e poi combino l’uno percento di quello che speravo. Se tuttavia fosse la volta buona almeno per qualcun altro, ne sarei unicamente felice.