---
title: "Valori postali"
date: 2021-04-27T18:18:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [iOS, iOS 14.5, Federico Viticci, MacStories, Hey, Basecamp, Pixel Envy, Joanna Stern] 
---
Questi giorni rimarranno nella storia, in un modo o nell’altro, perché esce iOS 14.5, assieme a iPadOS stesso numero. Perché da oggi su iPhone e iPad un business che vuole fare soldi registrando il tuo percorso di navigazione su Internet, dovrà chiederti il permesso di farlo.

Storica perché finora questo è accaduto essenzialmente di nascosto. Doppiamente storica perché chi vuole potrà dire *sì, seguimi pure*. La scelta, il controllo della privacy personale viene lasciato alla persona. Non ci saranno più scuse, in qualsiasi direzione.

La cosa importante da capire è quella [espressa molto bene da Pixel Envy](https://www.wsj.com/articles/ios-14-5-a-guide-to-apples-new-app-tracking-controls-11619457425):

>mi sovviene l’idea della privacy non come qualcosa che si *ha* e che invece bisogna *comprare*; un componente tra i tanti in una lista.

Apple ha pubblicato un video di due minuti a illustrare [il concetto di App Tracking Transparency](https://www.youtube.com/watch?v=Ihw_Al4RNno). Due minuti, no, un minuto e cinquantasei. Chi abbia dubbi, lo guardi.

Craig Federighi, responsabile software di Apple, [spiega in un video](https://www.youtube.com/watch?v=G05nEgsXgoI) basilare di otto minuti che è l’inizio di una partita tra gatti e topi: le aziende provano e proveranno ad aggirare le restrizioni al tracciamento indesiderato.

Senza contare che, fuori dall’infrastruttura di Apple, ci si trova di nuovo in mare aperto, dove vale tutto. Insomma è un inizio, non una conclusione, di un processo che sarà come quello della sicurezza, dove il problema non si risolve mai in modo definitivo e piuttosto si mira a raggiungere livelli di elevati di protezione grazie a una attenzione costante.

In uno svolgersi di eventi apparentemente slegato da questi, [Federico Viticci](https://twitter.com/viticci) [ha deciso di abbandonare](https://twitter.com/viticci/status/1386778154031894530) il servizio di posta [Hey di Basecamp](https://hey.com), che usava – lui e tutto lo staff che gravita attorno a [MacStories](https://www.macstories.net) – per rivolgersi ad altri.

<blockquote class="twitter-tweet"><p lang="en" dir="ltr">I&#39;m a user, and paying customer, of Basecamp products.<br><br>Their new policy of silencing workplace discussion of political and societal issues forces me to consider taking my business elsewhere. Disappointing, shortsighted, and out of touch. Just wrong.<a href="https://t.co/dPCCIAVRXd">https://t.co/dPCCIAVRXd</a> <a href="https://t.co/pHsSp1OKmY">pic.twitter.com/pHsSp1OKmY</a></p>&mdash; Federico Viticci (@viticci) <a href="https://twitter.com/viticci/status/1386778154031894530?ref_src=twsrc%5Etfw">April 26, 2021</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

Il motivo? [Basecamp](https://basecamp.com) ha deciso (oltre a varie altre cose) che sull’account aziendale [non si parla di politica o di problematiche sociali](https://world.hey.com/jason/changes-at-basecamp-7f32afc5).

A Viticci interessa (e ci mancherebbe) avere per la propria attività un servizio di eccellenza. Al tempo stesso, desidera che chi glielo fornisce si trovi sulla sua linea rispetto a certi valori.

Si noti come il torto o la ragione, qui, siano nozioni irrilevanti. Il punto focale è questo: unire la valutazione professionale a quella su valori altri.

Qui il cerchio si chiude. Arriverà il momento in cui si sceglierà un servizio (anche) sulla base del rispetto della privacy.

<iframe width="560" height="315" src="https://www.youtube.com/embed/Ihw_Al4RNno" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

<iframe width="560" height="315" src="https://www.youtube.com/embed/G05nEgsXgoI" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

*Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile.*