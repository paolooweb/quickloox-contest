---
title: "Per partito presa"
date: 2020-01-25
comments: true
tags: [iPhone, Apple, EU]
---
Apple ha preso posizione contro la presa di posizione della Commissione Europea che vorrebbe [uno standard unico di alimentazione per tutti i cellulari](https://www.zdnet.com/article/apple-heres-why-ditching-iphone-lightning-port-for-a-standard-charger-is-a-bad-idea/) venduti sul continente. Si tratta, dice, di rendere obsoleto un miliardo di apparecchi e accelerare una generazione immane di rifiuti tecnologici, con lo scopo di… ridurre la produzione di rifiuti tecnologici. e adduce uno studio secondo il quale la norma costerebbe più dei risparmi che porta.

Ma sono ragioni sbagliate. Protestare contro uno standard dei caricabatteria in Europa dovrebbe averne altre.

Per esempio, è assai probabile che una eventuale legislazione entrerebbe in vigore quando iPhone si caricherà per contatto, finto *wireless*, senza connettori.

E bisognerebbe capire come è scritta la legge: pensa che bello se il connettore europeo standard fosse *obbligatorio* e iPhone diventasse fuorilegge per non avere alcun connettore. Il primo indizio di un fatto: si tratta di una norma che penalizza l’innovazione. Se l’avessimo avuta nel 1990, saremmo ancora a quei caricabatteria.

Su tutto, però, vogliamo ricordare che l’Europa non ha uno standard per le prese elettriche ed è così da sessant’anni? Standardizzare gli altri, a spese del contribuente, è sempre più facile.