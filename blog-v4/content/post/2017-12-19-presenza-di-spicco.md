---
title: "Presenza di spicco"
date: 2017-12-19
comments: true
tags: [Flavio, Elio, MacBook, Forum, Assago]
---
Grazie a **Flavio** per una immagine speciale dal [Mediolanum Forum](http://www.mediolanumforum.it/en/) di Assago in occasione del [concerto di non addio](http://elioelestorietese.it/concerti/) di Elio e le Storie Tese.<!--more-->

 ![MacBook Pro al concerto di Elio e le Storie Tese al Forum di Assago](/images/elio.jpeg  "MacBook Pro al concerto di Elio e le Storie Tese al Forum di Assago") 

Flavio commenta come il MacBook Pro qui spicchi in modo particolare ed effettivamente ha l’aria di essere veramente nel ruolo della supervisione.

È la prima foto di *product placement* con una valenza artistica che riesca a pubblicare e non sarà l’ultima. Esattamente come i concerti di Elio.