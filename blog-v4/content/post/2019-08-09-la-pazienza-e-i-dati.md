---
title: "La pazienza e i dati"
date: 2019-08-09
comments: true
tags: [Backblaze, Toshiba]
---
È sempre tempo guadagnato leggere le [statistiche sugli hard disk installati presso BackBlaze](https://www.backblaze.com/blog/hard-drive-stats-q2-2019/) e anche l’ultimo appuntamento è stato all’altezza delle aspettative.

Il campione in esame contiene all’incirca centoottomila dischi rigidi, di cui vengono tenuti in conto i malfunzionamenti. Si possono leggere cose come questa:

>Questi dischi [Toshiba MG07ACA14TA da quattordici terabyte) hanno avuto una partenza ballerina, con sei guasti nei primi tre mesi di utilizzo. Da allora si è verificato solo un altro guasto e il secondo trimestre 2019 è stato immacolato. Il risultato è che il tasso di guasti annuale di questi dischi è sceso a uno 0,78 percento molto rispettabile.

Dalla tabella si vede che i dischi Toshiba da quattordici terabyte installati sono milleduecentoventi, con una età media di 8,85 mesi e sette guasti totali durante 328.960 giorni/disco di funzionamento, paragonabili a un ipotetico singolo disco rigido in funzione da novecento anni e più.

Si vede facilmente che, se il conto si fosse fermato ai primi tre mesi, le prestazioni dei dischi avrebbero registrato un valore disastroso.

Poi però sono arrivati altri sei mesi di funzionamento praticamente perfetto, cosa che nessuno avrebbe predetto se si fosse trovato quei dischi fallati sulla propria scrivania. *Ho preso un Toshiba da quattordici tera, neanche tre mesi e ha smesso di funzionare. Fanno schifo, non comprateli*.

Invece ci vogliono dati. Ci vuole pazienza. Molto spesso le due cose vanno insieme.