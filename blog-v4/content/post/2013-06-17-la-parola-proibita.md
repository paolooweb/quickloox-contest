---
title: "La parola proibita"
date: 2013-06-17
comments: true
tags: [Scrivere]
---
Steve Sande ha iMac, MacBook Pro, iPad, iPhone, iPad mini; usa <a href="http://www.ulyssesapp.com">Ulysses III</a>, <a href="http://www.daedalusapp.com/">Daedalus Touch</a>, <a href="http://agiletortoise.com/">Drafts</a>, <a href="http://bywordapp.com/">Byword</a>.<!--more-->

Megan Lavey-Heaton ha MacBook Pro, MacBook Air, iMac, iPad mini, iPhone; usa Ulysses III, Byword, [Evernote](https://evernote.com/) e una [agenda Moleskine](http://www.moleskine.com/) con penna stilografica <a href="http://www.lamyusa.com/fountain_main_safari.php">Lamy Safari</a>.

Mike Schramm usa <a href="https://www.google.com/intl/it/chrome/browser/">Chrome</a>.

Kelly Hodgkins ha MacBook Pro, iPhone 5, iPad mini; usa <a href="http://www.markdownpro.com">Markdown Pro</a>, <a href="http://smilesoftware.com/TextExpander/index.html">TextExpander</a>, <a href="http://www.irradiatedsoftware.com/iclip">iClip</a>, Evernote, <a href="http://www.devontechnologies.com/">DevonThink</a>.

Victor Agreda, Junior, ha MacBook Air, iPad di terza generazione, iPhone 5; usa Byword, <a href="http://markedapp.com/">Marked</a>, Evernote, <a href="http://notestab.com/">NotesTab Pro</a>. Ha anche una agenda Moleskine, che non richiede batteria e si può usare quando l’aereo atterra o decolla.

Erica Sadun usa un *browser*.

Chris Rawson usa TextEdit.

Richard Gaywood ha Mac e iPad; usa <a href="http://macromates.com/">TextMate</a>, <a href="http://getwritingkit.com/">Writing Kit</a>, (raramente) <a href="http://notepad-plus-plus.org/">Notepad++ su Windows</a> e <a href="http://www.ithoughts.co.uk/iThoughtsHD/">iThoughts HD for iPad</a>.

Ilene Hoffman usa TextEdit e <a href="http://barebones.com/products/bbedit/">BBEdit</a>.

Shawn *Doc Rock* Boyd usa <a href="http://brettterpstra.com/projects/nvalt/">NVAlt</a>, TextMate, Byword, Writing Kit, Drafts, Evernote, <a href="http://dayoneapp.com/">Day One</a>, TextExpander, Marked.

Dave Caolo usa <a href="www.ithoughts.co.uk/iThoughtsHD/&lrm;">MindNode</a>, che esporta come file <a href="http://home.opml.org">Opml</a> per usarli in <a href="http://www.literatureandlatte.com/scrivener.php">Scrivener</a>, poi Marked, Byword.

Queste persone guadagnano denaro <a href="http://www.tuaw.com/2013/06/13/how-we-write-for-tuaw-a-look-at-blogger-workflows-and-tools/">pubblicando contenuti</a> in un sito esistente da molti anni, azionato da un sistema di organizzazione e pubblicazione chiamato <a href="http://www.blogsmith.com">Blogsmith</a>. Lavorano. Collaborano. Hanno un editore.

Quasi tutti usano <a href="http://daringfireball.net/projects/markdown/">Markdown</a> per produrre facilmente testo con grassetti, corsivi, citazioni e link.

Word è assente. Il punto non è naturalmente che nessuno costringa a usarlo, quanto che per arrivare a un lavoro ricco, complesso, finito e redditizio sia completamente inutile.

A rincarare la dose, l’<a href="http://www.imore.com/how-we-write-imore-workflows-mac-ipad-iphone-and-back">esempio di iMore</a> aggiunge all’elenco <a href="http://www.iawriter.com">iA Writer</a>, Siri (!), <a href="http://hogbaysoftware.com/products/writeroom">WriteRoom</a>, <a href="http://barebones.com/products/textwrangler/">TextWrangler</a>, <a href="http://www.secondgearsoftware.com/elements/">Elements</a>.

(Ho approfittato dell’occasione per rispolverare un [*post* di Script](https://macintelligence.org/posts/2012-01-13-de-bello-scrivere/) piuttosto corposo, in argomento.)