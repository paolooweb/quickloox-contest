---
title: "La dieta del fanatico"
date: 2018-04-16
comments: true
tags: [applecarfans.com]
---
Ogni tanto pare che chi sceglie Apple sia un tifoso fazioso, un convertito militante, un cretino asservito o uno zelota benestante.

Di solito arriva da qualcuno che legge siti di spazzatura; riguardante Apple, ma sempre spazzatura.

Per esempio, *Macity* riesce a dare di seconda mano la notizia della [morte di Michael Spindler](https://www.macitynet.it/e-morto-michael-spindler-fu-ceo-degli-anni-orribili-di-apple/), amministratore delegato di Apple tra il 1993 e il 1996.

Attenzione: Spindler è scomparso *un anno fa*. E la notizia è *di seconda mano*.

Su tutt’altro versante, si apprezzi Archive.org per il suo immenso archivio di siti web del passato, magari con una donazione. Poi si passi al 2016 e al 2017, guardando quello che è rimasto delle pagine di [Applecarfans.com](https://web.archive.org/web/20160214092741/http://applecarfans.com/).

Il sito non esiste più. Non erano fanatici di Apple né della Apple Car. Erano fanatici di spazzatura. Una dieta che su Internet, non solo in ambito Apple, seguono troppi.