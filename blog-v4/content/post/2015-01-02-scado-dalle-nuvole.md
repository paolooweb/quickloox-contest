---
title: "Scado dalle nuvole"
date: 2015-01-04
comments: true
tags: [Apple, iCloud, Numbers, iWork, Drive, Google]
---
Involontariamente mi sono trovato nella situazione di ricorrere a un certo foglio Numbers, su un Mac diverso dal mio. L’ho richiamato da [iCloud](https://www.icloud.com) e la *app* si è comportata molto bene, veloce, precisa, stabile.<!--more-->

La sera, dopo qualche ora, ho riaperto lo stesso foglio da Mac e non l’ho visto aggiornato. Ho riapportato a mano un paio delle modifiche fatte da iCloud e poi l’ho lasciato lì per fare altro. C’è voluta mezz’ora prima che arrivasse l’avviso che esistevano due versioni contrastanti del foglio, una su iCloud e una su Mac, e mi venisse chiesto quale tenere. Perdendo le modifiche dell’altra.

Premesso che su iCloud le *app* iWork sono ampiamente contrassegnate come *beta* e quindi non è lecito pretendere il funzionamento da orologio svizzero, mi auguro ugualmente per il 2015 che sia l’anno di iCloud; che sia veloce e precisa al punto di bagnare il naso a [Google Drive](https://drive.google.com/drive/). Che è indietro perché gli mancano le *app* su Mac, ma a livello di sincronizzazione dentro il *browser* è molto bravo.

Ieri mattina ho riaperto il famoso foglio con iPad e nel pomeriggio l’ho ripreso con Mac; le nuove modifiche erano a posto dal primo istante. Così dev’essere e non per pretendere; semplicemente per amore del buon *design*. Se le *app* costruite per aggiornarsi via cloud non lo fanno istantaneamente, diciamo che ci arrangiamo senza e magari sincronizziamo solo i file, come fa [Dropbox](https://www.dropbox.com), che è indietro perché non ha proprio app, però a sincronizzare è perfetto. Non è obbligatorio offrire *app*; se le si offrono, una volta tolto quel triangolino giallo con la scritta *beta*, devono funzionare. Altrimenti si prendono quelle risorse lì e le si mettono su qualche altro progetto.

Apple è nata per fare poche cose bene, non tante scadenti. Per quelle già abbiamo fornitori a volontà.