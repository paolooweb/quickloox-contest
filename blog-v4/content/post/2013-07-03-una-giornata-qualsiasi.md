---
title: "Una giornata qualsiasi"
date: 2013-07-03
comments: true
tags: [Mac, iOS, Windows]
---
Una volta si stava su <a href="http://www.netmarketshare.com">NetMarketShare</a> con un certo timore. Le sue statistiche di traffico web per browser, sistema operativo, *mobile*, *desktop* venivano febbrilmente compulsate per essere sicuri che Mac fosse in buona salute, Safari conservasse il proprio posto al sole, Internet Explorer risparmiasse almeno qualche piccola parte del mondo web in cui respirare un po’ di libertà.<!--more-->

Sono arrivate le statistiche di giugno. Tutto tranquillo, tutto normale, quasi noioso. Nel *desktop* Mac veleggia sopra il sette percento, ancora *élite* eppure salutarmente qualunque soglia di sopravvivenza. Windows 8 ha passato Windows Vista (capirai…) e si pone trionfalmente come *terza* versione di Windows più usata. Per confronto, su Mac la versione più usata è Mountain Lion con il 3,14 percento del traffico globale. Poi viene Snow Leopard (1,76), poi Lion (1,73), poi altre versioni di OS X (0,55). Mountain Lion fa il 44 percento dell’installato Mac, Windows 8 qualcosa più del cinque. Non parliamo dei *browser*, dove Explorer 10 guadagna un po’ di terreno perché adesso procede all’aggiornamento automatico e o lo prendi o lo prendi.

Apparecchi *mobile*: iOS batte Android 58 a 20,5. Il bello è che, lo si è detto alla nausea, iPhone e ipad sono molti meno. Eppure su Internet ci sono. Android costerà anche meno, ma la mia opinione è che se uno ha da fare cose serie si compra un computer da tasca e allora è un iPhone. Sennò ha usare un telefono, giusto raccontando agli amici che è uno *smartphone* per darsi un tono ed essere alla moda, e se ne compra uno da novantanove euro. Tanto, per non farci niente, è inutile spendere di più.

Ultima annotazione: NetMarketShare continua a modificare il sistema di misurazione in modo da tenere artificialmente il più alto possibile il valore di Windows. Adesso si sono inventati che se il *browser* ha pagine dentro i *tab*, che vengono caricate ma non si vedono a meno di cliccare sul *tab*, quelle pagine non contano. Guarda caso, Internet Explorer punta poco sui *tab*, Chrome moltissimo. Ciononostante Windows si trova al punto più basso da novembre.

Ars Technica ha fatto il lavoro che NetMarketShare faceva e ha smesso di fare, sempre per cercare di avvantaggiare Windows: ha conteggiato <a href="http://arstechnica.com/information-technology/2013/07/windows-8-vaults-past-vista-ie10-continues-to-surge/">il traffico globale</a>, senza separare *desktop* e *mobile*. Internet Explorer perde la maggioranza assoluta. Più di metà del mondo, stando a questi risultati, fa a meno del *browser* Microsoft.

Speriamo che l’altra metà rinsavisca presto, unica considerazione forte che è possibile formulare in una giornata che è gioiosamente qualsiasi, priva di ansie, vuota di timori.