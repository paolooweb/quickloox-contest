---
title: "What a Wonderful World"
date: 2021-05-31T00:35:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [TikTok, Lucy Edwards, Braille] 
---
C’è una ragazza inglese che pubblica video su TikTok e ha più di un milione e mezzo di *follower*.

Una descrizione come questa potrebbe corrispondere ad abbastanza ragazze da raddoppiare la popolazione femminile di Torino.

Questa ragazza però si chiama [Lucy Edwards](https://www.tiktok.com/@lucyedwardsblind). Ha diciassette anni e ha appena terminato di perdere completamente la vista.

Pubblica video su TikTok, dicevo. Per farlo si aiuta con una app che le suggerisce quando è effettivamente inquadrata dalla telecamera. Prende appunti audio con una app di grande accessibilità, ascolta il testo di articoli e commenti, utilizza un lettore di etichette per capire che cosa ci sia dentro un barattolo. Tutto da sola.

Neanche a dirlo, [usa un iPhone](https://apps.apple.com/us/story/id1561037483). Ed è tutt’altro che una storia strappalacrime. Lucy ha prodotto una serie di video per insegnare l’alfabeto Braille e usa sempre TikTok per sfatare uno per uno i pregiudizi sulla cecità, spesso in risposta a domande dirette di chi la segue.

È un mondo difficile, con tanti problemi. Contemporaneamente [è un mondo meraviglioso](https://www.youtube.com/watch?v=CWzrABouyeE) perché esiste Lucy, esiste iPhone, esistono app che permettono a una cieca non solo di vivere una vita il più possibile normale, ma anche comunicare, distinguersi, lasciare un segno nell’universo.

<iframe width="560" height="315" src="https://www.youtube.com/embed/CWzrABouyeE" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

*Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile.*               