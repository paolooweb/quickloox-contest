---
title: "L’immaginazione elevata al potere"
date: 2021-05-26T00:51:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [50 Years of Text Games] 
---
L’inglese che si fa a scuola non è mai abbastanza e l’anno prossimo credo che saremo una in più ad avere l’età giusta per perlustrare assieme al papà [50 Years of Text Games](https://if50.substack.com).

Perché la grafica, l’immersione, la realtà aumentata e quella virtuale; ma prima che qualsiasi mondo visibile possa competere con quello che abbiamo il potere di evocare con la nostra immaginazione, deve passare ancora un bel po’ di tempo.

(E poi ci sono un paio di titoli che ho bisogno di un pretesto per riuscire a riprendere, perché mi sono rimasti lì senza riuscire ad arrivare in fondo).

*Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile.*               