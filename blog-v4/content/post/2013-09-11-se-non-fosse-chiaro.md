---
title: "Se non fosse chiaro"
date: 2013-09-11
comments: true
tags: [iOS, iPhone]
---
Devo ancora vedere il [keynote](http://www.apple.com/apple-events/september-2013/).<!--more-->

Sto guardando la [pagina home](http://www.apple.com) di Apple e vado avanti e indietro tra le quattro schermate. Confidenti, tranquille, orgogliose.

Da ricordare per quando passa il prossimo profeta di sventura. Tim Cook e compagnia possono magari sbagliare. Ma hanno le idee ben chiare e sono capaci di scegliere.

Domani immagino che leggerò le critiche per la mancanza dell’orologio, del televisore, dello spremiagrumi, per le azioni che vanno già oppure su. La mamma degli imbecilli, oltre a essere sempre incinta, è pure isterica.