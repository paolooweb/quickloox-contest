---
title: "Caratteri a sorpresa"
date: 2023-07-19T01:19:19+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [AirDrop, BBEdit, Runestone]
---
Sarà ricordato come il giorno in cui non avevo altre possibilità e me la sono cavata trascinando testo selezionato da [BBEdit](https://www.barebones.com/products/bbedit/index.html) in AirDrop per mandarlo su iPad.

Obiettivo raggiunto, con effetti collaterali. Il testo è arrivato come tale su iPad (dentro [Runestone](https://runestone.app)). Però:

- era moltiplicato per tre o per quattro (c’erano più snippet di testo e non ho capito bene);
- conteneva codici di controllo numerici che forse erano RTF, forse Unicode;
- ho copiato uno degli snippet, solo una parte di testo, e l’ho incollata in WordPress (sigh); ogni carattere si è rivelato affiancato da un carattere spurio Unicode.

Pulito tutto il necessario, ho avuto il testo come e dove mi serviva. Tuttavia mi rendo conto ora di avere usato un flusso molto più complesso di come lo avrei pensato io. In futuro guarderò ad AirDrop con meno sufficienza.