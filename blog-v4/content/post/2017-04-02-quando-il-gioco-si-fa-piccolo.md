---
title: "Quando il gioco si fa piccolo"
date: 2017-04-02
comments: true
tags: [watch, Saqqara, O’Flaherty-Chan, Doom, Playground, Swift]
---
Ho sempre trovato appropriata la scelta di lasciare watch senza un *browser* web e giustificato la sostanziale mancanza di giochi degni di questo nome sul computer da polso.<!--more-->

Nel contempo penso che esista uno spazio di frontiera da riempire. Cose che sono fattibili su watch e però solo, diciamo, un esploratore abbastanza determinato può arrivare a trovare. Il tutto fuori dalle dimostrazioni fini a se stesse come [fare girare Doom](http://www.theverge.com/tldr/2015/10/2/9443491/doom-apple-watch-new-apple-tv-port) o, appunto, [un *browser*](https://macintelligence.org/posts/2015-05-13-compagnie-limitate/).

Il Cristoforo Colombo di questi volonterosi potrebbe essere Gabriel O’Flaherty-Chan, che ha dapprima varato su watch un [gioco di ruolo 3D](http://gabrieloc.com/2016/12/20/SAQQARA.html) simili a quelli su Ms-Dos negli anni ottanta e, di recente, un [emulatore di Game Boy](http://gabrieloc.com/2017/03/21/GIOVANNI.html).

I giochi richiedono pazienza; la piattaforma è evidentemente sollecitata ai limiti delle sue possibilità attuali. Leggere i resoconti delle sue imprese è invece assai istruttivo, per capire il lavoro di un programmatore che invece di vedere un limite come un muro, ci lavora come a un’asticella da valicare.

Così O’Flaherty-Chan racconta la scoperta tardiva dell’inesistenza di [OpenGl](https://www.opengl.org) per watch e della ricerca di una soluzione tramite i Playground di [Swift](http://www.apple.com/it/swift/). Oppure dà conto di come un algoritmo per generare labirinti sia molto più rispettoso delle risorse disponibili di un altro, a parità di soddisfazione per il gioco.

Progressi grandi per espandere le scelte possibili su schermi piccoli. In un certo senso torna la genialità di quando si scriveva un [programma di scacchi capace di stare in un chilobyte di memoria](http://www.zx81stuff.org.uk/zx81/tape/Chess(Psion)).