---
title: "Obsolescenza programmata"
date: 2014-05-02
comments: true
tags: [iMac, SnowLeopard, OSX, Mavericks, obsolescenza]
---
Il primo passo è stato installare Snow Leopard. Questo ha permesso di aggiornare Snow Leopard al punto di avere a disposizione Mac App Store. Dal quale ho scaricato gratis una copia di Mavericks.<!--more-->

Su un iMac di metà 2007. Che monta l’ultima generazione del sistema operativo.

Poi leggi gli articoli sulla [trappola dell’obsolescenza programmata](http://www.nytimes.com/2013/11/03/magazine/why-apple-wants-to-bust-your-iphone.html?_r=1&).