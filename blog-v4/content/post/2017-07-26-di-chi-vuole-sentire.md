---
title: "Di chi vuole sentire"
date: 2017-07-26
comments: true
tags: [Nucleus, iPhone]
---
Ecco perché preferisco parlare del presente di iPhone più che [rievocare i suoi dieci (straordinari) anni di storia](https://macintelligence.org/posts/2017-07-04-passato-e-presenti/): a settembre inizierà la commercializzazione di un [impianto cocleare](http://www.prnewswire.com/news-releases/cochlear-introduces-the-worlds-first-made-for-iphone-cochlear-implant-sound-processor-300494185.html) – l’equivalente di un apparecchio acustico, ma impiantato internamente – che si appoggia a iPhone per ricevere suono e per configurarsi nel modo più personalizzato.<!--more-->

Neanche Steve Jobs avrebbe pensato a qualcosa del genere, al momento di cominciare il lavoro su iPhone iniziati da poco gli anni duemila.

Ed è il primo apparecchio nel suo genere. Probabile che ne arriveranno altri, concorrenziali, e altri ancora, similari ma in ambiti diversi: viene facile – adesso – pensare ad ausili per la vista o l’equilibrio, sistemi che migliorano la vita delle persone che ne hanno più bisogno basati su hardware e software pratici e potenti come quelli che oggi stanno in una tasca

Nessun dubbio che iPhone, come complemento a un impianto cocleare, potrebbe essere più piccolo, o più sviluppato nella componente microfono, o comunque più specializzato. Quello che lo rende veramente fuori dal comune è che sia così eccezionalmente generico dal consentire persino questo.

La sua storia inizia solo ora a essere raccontata e la frontiera dei suoi impieghi possibili è proprio lontana.
