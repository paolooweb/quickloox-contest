---
title: "L’opzione umana"
date: 2020-01-26
comments: true
tags: [iOS13, Wi-Fi, Cellular]
---
Sorpresa piacevole in iOS 13: Il Wi-Fi è momentaneamente scollegato da Internet e il sistema chiede se usare temporaneamente la connessione dati cellulare.

 ![iOS 13 propone di usare la connessione cellulare quando il Wi-Fi è scollegato](/images/wi-fi-cellular-ios-13.png  "iOS 13 propone di usare la connessione cellulare quando il Wi-Fi è scollegato") 

iOS ha da sempre privilegiato il Wi-Fi alla connessione cellulare, tanto a che a volte diventava necessario disattivare il Wi-Fi per riuscire a lavorare: un caso classico, il Wi-Fi pubblico invadente che appare e scompare; o ancora, il Wi-Fi gratuito del locale o dell’edificio, dotato di banda risibile quando non sovraccarico.

Appare naturale che un apparecchio intelligente supplisca a questi problemi facendo uso saggio della connessione cellulare e la chiave di questo sta nella parola *temporaneamente*, che rassicura quanti abbiano un piano dati da centellinare. Importante, il tutto non accade automaticamente ma appare una richiesta precisa e intellegibile, che mantiene il proprietario del sistema in sella a quest’ultimo.

Tutto semplice, umano e funzionante. *It just works*, come dovrebbe sempre essere.