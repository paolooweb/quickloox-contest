---
title: "Una strategia per il Paese"
date: 2018-06-28
comments: true
tags: [Cook, Italia]
---
Da tempo si respira, nella politica e nelle discussioni, un clima fastidioso. La dialettica si è estinta, riconoscere un centimetro di ragione all’avversario – ormai diventato nemico – sembra una concessione inaccettabile, avanziamo più che mai nella vita – cito Philip Roth – convinti che gli altri abbiano sempre torto e avendo sostituito il dialogo con l’urlo.

C’è bisogno di qualcosa in più per affrontare quello che abbiamo davanti.

Personalmente mi sono iscritto ai [Copernicani](https://copernicani.it), ma oggi volevo dire un’altra cosa che in verità ha detto Tim Cook, l’amministratore delegato di Apple, ai microfoni di *Fortune*, [anticipato da MacRumors](https://forums.macrumors.com/threads/apple-ceo-tim-cook-talks-immigration-human-rights-privacy-apple-news-and-more-in-new-interview.2124830/):

>Cook ha detto che Apple effettua spesso investimenti di durata da sette a dieci anni. Se fosse "re per un giorno," i rapporti finanziari trimestrali li eliminerebbe "tirando lo sciacquone" perché sono "vestigia di un tempo diverso".

Ecco, da sette a dieci anni. La soluzione è riprendere a pensare e ragionare come se ci fosse un futuro. In controtendenza con gli strumenti e le piattaforme che ci stimolano a pensare sull’adesso, con visione a dieci secondi. Ricominciamo a pensare in avanti.