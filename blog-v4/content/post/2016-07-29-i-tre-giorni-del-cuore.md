---
title: "I tre giorni del cuore"
date: 2016-07-29
comments: true
tags: [cuoredimela, Misterakko, Akko, Accomazzi]
---
Ho già spiegato il [significato dell’operazione Cuore di Mela](https://macintelligence.org/posts/2016-06-15-un-kickstarter-per-cambiare-il-libro/) per il sistema editoriale informatico italiano. Aria nuova, possibilità inedite, un avviso chiaro che si può fare di più. E andrebbe fatto.

In vari commenti ho enumerato i vantaggi insiti per i lettori: un testo che si aggiorna e cresce, un filo diretto con gli autori, la possibilità di chiedere approfondimenti o informazioni particolari, un dialogo continuo che va oltre la copertina e la vendita, dove invece si fermano tutte le produzioni attuali.

La argomentazioni razionali contano poco, però, nel complesso delle cose. Provo a spiegare in altro modo perché [Cuore di Mela](http://cuoredimela.accomazzi.it) merita una possibilità:

* Il lucro, oltre il recupero del tempo impiegato e delle spese sostenute, c’entra pochissimo. Si fanno più soldi in meno tempo praticamente con qualunque altra alternativa professionale (non parliamo delle altre). *Cuore di Mela* sarà un *labour of love*, un prodotto professionale preparato con amore più che con il bilancino.

* È facile vedere che i libri sono una cosa del passato, fatti come erano fatti nel passato. Pensare a un’epoca senza libri mette il magone. Certo vanno fatti nel modo giusto e questo è un inizio. Se la pianta germoglierà, potrà diventare albero.

* Un lavoro fatto bene aiuta una marea di persone a risolvere una marea di problemi in una frazione del tempo altrimenti necessario a chiedere in giro. È un’occasione di aiutare tante persone che non possono o non vogliono passare tempo su Internet a chiedere informazioni. Voglio solo accennare al problema dell’accessibilità dei siti e mi fermo qui.

* Se *Cuore di Mela* riesce, ci proveranno altri. I libri utili e meritevoli potrebbero aumentare. La qualità di tutto quello che esce, in libreria e in rete, potrebbe salire. Mostriamo agli editori che si può cambiare e si può fare di più.

* Crediamo che questa sia una cosa giusta da fare.

Mancano tre giorni e qualche euro. Se ritieni non faccia per te, nessun problema, anzi. Se invece non hai ancora preso una posizione, [fai un giro su Kickstarter](https://www.kickstarter.com/projects/295108589/cuore-di-mela-i-segreti-di-apple-iphone-e-mac) e pensaci su un momento. Certo è l’epoca delle *app* da 0,99 e tutto sembra costoso. Ma la cosa *giusta* a volte richiede decisioni importanti.

Grazie per l’attenzione.
