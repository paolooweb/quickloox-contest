---
title: "Uno schermo per tutti, tutti per una app"
date: 2021-08-13T00:57:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [Apple TV, Hdmi, Ethernet, Apple Arcade, Badland+, iPad Pro, Mac, iPhone] 
---
Devo dare conto dell’arrivo in casa di una [Apple TV 4K da 64 gigabyte](https://www.apple.com/it/apple-tv-4k/).

Un acquisto del tutto esagerato dato che non abbiamo un televisore 4K né contiamo di averlo a breve, non abbiamo un impianto audio collegato al televisore e non siamo patiti di serie televisive. Però la Apple TV precedente è durata molti, molti anni e non so come si troverà a operare questa nel futuro.

Esteriormente i cambiamenti riguardano l’altezza (rispetto allo scatolino nero che avevamo, il nuovo scatolino ha la stessa superficie ma è alto quasi il doppio) e le porte di collegamento; la Apple TV precedente ne aveva una pletora, qui o è Hdmi o è Ethernet. Avevo irrazionalmente il timore che una Apple TV 4K facesse questioni per vedersi collegata a un televisore obsoleto e mi è passato. Nessun problema.

Avere un iPhone nella stessa rete Wi-Fi permette di configurare molto rapidamente l’apparecchio. iPhone fornisce il livello di sicurezza e confidenza per fare dare ad Apple TV molte cose per scontate e arrivare rapidamente a funzionare livello base. La configurabilità dell’apparecchio a parte le impostazioni fondamentali è veramente ampia e ci sono molte cose su cui chissà quando troverò il tempo di passare.

Poi c’è il telecomando. Quello nuovo è significativamente più grande, più spesso, con più pulsanti, anche se non passiamo la mezza dozzina di punti su cui mettere il dito, più il tasto laterale per chiamare in causa Siri. In più ci sono un pulsante di accensione e stop, di cui onestamente non si sentiva la mancanza, e i pulsanti per il volume, che bypassano il telecomando del televisore e per questo sono molto, molto comodi.

Il nuovo telecomando è anche a batteria ricaricabile via connettore Ligthning. Per giudicare questa scelta rispetto alle tradizionali batterie a perdere ci vorrà tempo; per ora registro l’arrivo di un ulteriore cavo Usb-Lightning che torna molto utile per i nostri iPhone.

Il telecomando ha il pulsante grande che è anche sensibile al tocco, ed è di fatto tutta una nuova interfaccia. Bisogna assolutamente abituarcisi, anche se ci vuole poco, e lo trovo piuttosto sensibile, forse un filo troppo rispetto alla mia esperienza. Però la sensazione dell’eccessiva sensibilità è già quasi svanita dopo due serate, forse è solo questione di prenderci il dito.

Il modo che abbiamo per sfruttare una parte minima delle notevoli possibilità della nuova Apple TV è usare l’abbonamento ad Apple Arcade per giocare davanti al televisore, usando il telecomando come controller.

Dopo averlo testato per il periodo di prova, ho convintamente accettato di versare 4,99 euro mensili per Apple Arcade. Forniscono alle figlie un ambiente con app di qualità, senza pubblicità, senza sorprese sgradevoli, disponibili su tutti gli apparecchi di casa compresa appunto Apple TV, il cui numero continua ad aumentare, di ogni genere. Certamente non è cosa per un vero *gamer*, ma per il nostro *parterre* di seienni-treenni è una buona scelta.

Buona, non ottima, perché vari giochi sono di complessità elevata oppure adatti a fasce di età superiori. Tutta roba che si dirime molto in fretta e con tranquillità.

A volte anche un gioco non-adatto può diventare un’occasione per stare insieme. La primogenita ha deciso di caricare Badland+ (fuori da Apple Arcade [costa 2,99 euro](https://apps.apple.com/it/app/badland/id535176909)), lo ha trovato troppo difficile (*et pour cause*) e ha chiesto a papà di provare a giocare lui, visto che voleva vedere che cosa succedeva. Anche secondogenita ha esclamato che voleva vedere e così abbiamo passato una mezz’ora di relax sul divano a risolvere livelli e commentare quello che si vedeva e che succedeva. Un’esperienza che vale decisamente la pena di alternare al gioco, diciamo, impegnato su iPad Pro. (Su Mac e su iPhone di fatto non ho mai scaricato nulla da Apple Arcade).

Mi ritrovo totalmente nella [chiosa di John Gruber ad Apple TV](https://daringfireball.net/linked/2021/08/10/apple-tv-point) e a come viene giudicata rispetto alla concorrenza:

>Direi che Apple TV è la quintessenza di un prodotto Apple: il suo punto primario è offrire una esperienza di utilizzo superiore a quanti la desiderano e per averla sono disposti a pagare più di un prodotto concorrente.

*Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile.*               