---
title: "Una boccata d’Air"
date: 2018-02-20
comments: true
tags: [Air, Sasha]
---
Gentilissimo **Sasha** a inviare quanto segue, testo e foto. Lo ringrazio e sostengo con convinzione il suo pensiero. Avrei voluto avere la stessa abilità anni fa, quando si è rovesciato il caffè sulla tastiera del PowerBook.<!--more-->

§§§

Ti allego un paio di foto.

Si tratta di un MacBook Air 13” Early 2014 smontato: uno scatto ritrae il “sotto-tastiera” e l’altro la scheda logica (il lato superiore della stessa, a contatto con il “sotto-tastiera”). I cerchi gialli evidenziano il risultato di un modesto e sfortunato versamento di una nota bevanda zuccherata e gassata sul portatile. Modesto scrivo, ma quanto basta per impedire al calcolatore di completare il caricamento di macOS.

 ![Il sottotastiera di MacBook Air aggredito dalla bevanda gassata](/images/sottotastiera.jpg  "Il sottotastiera di MacBook Air aggredito dalla bevanda gassata") 

Il Mac mi viene affidato da un amico sconfortato: il centro assistenza chiede 600 euro per ricambio, manodopera e la speranza che non ci siano altri guasti. Il sottoscritto, deciso a far risparmiare qualche euro allo sfortunato, si prodiga e reperisce una scheda logica oltreoceano: ricambio, valuta, iva e dogana fanno, se tutto va bene, 350 euro (“E speriamo che funzioni…”).

 ![La scheda logica di MacBook Air corrosa dalla bevanda gassata](/images/scheda-logica.jpg  "La scheda logica di MacBook Air corrosa dalla bevanda gassata") 

L’amico non è convinto e pensa di acquistare un nuovo Mac. Io ho bisogno di spazio sul banco di lavoro e questo pomeriggio decido di riassemblare il portatile, ma prima voglio togliermi uno sfizio: pulirlo "da cima a fondo”, motherboard compresa…

Ora attendo con trepidazione il momento in cui comunicherò al mio amico che il suo Mac è tornato a funzionare! :-D Per lui una boccata d’Air. :-)

Fortuna? Forse, però di “plasticoni” da supermercato ne ho smontati parecchi e posso dire che, sia ad occhio nudo che al tatto, la scheda logica del MacBook Air è diverse spanne sopra la concorrenza. Eh no, non sono tutti uguali :-)

Per la cronaca, il prodotto da me solitamente utilizzato per la pulizia dei dispositivi elettronici è [questo](https://www.radtech.com/products/omnicleanz).