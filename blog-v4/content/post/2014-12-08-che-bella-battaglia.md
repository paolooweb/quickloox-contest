---
title: "Che bella battaglia"
date: 2014-12-09
comments: true
tags: [Wesnoth]
---
Periodicamente devo raccomandare [Battle for Wesnoth](http://www.wesnoth.org) e il momento giunge propizio perché si avvicinano le feste.<!--more-->

Assolutamente da scaricare e giocare da soli nelle lunghe notti invernali quando tutti gli altri dormono; giocare in compagnia di illustri sconosciuti incontrati in rete se la famiglia è lontana; giocare in famiglia in *hotseat* (a turno davanti al computer) per divertimento semplice ma non banale, competitivo ma non violento, per togliersi lo sfizio di una sera oppure dedicare decine di ore a completare le campagne più difficili (o magari usare l’editor integrato e giocare con ragazzi dell’età giusta a creare insieme uno scenario).

Il tutto a costo zero (a parte la possibilità di lasciare una piccola donazione natalizia a un progetto eccezionale per longevità e risultati).

È appena uscita la nuova versione 1.12 che rifinisce interfaccia e gioco a livelli notevoli e lo rende ancora più godibile, in maniera significativa.

Unica lacuna del gioco è la situazione di App Store: per iOS è disponibile solo il *porting* di uno sviluppatore che non aggiorna il programma da oltre un anno. [Battle for Wesnoth HD](https://itunes.apple.com/it/app/battle-for-wesnoth-hd/id575852062?l=en&mt=8) (iPad) e [Battle for Wesnoth](https://itunes.apple.com/it/app/battle-for-wesnoth/id575239775?l=en&mt=8) (iPhone e iPod touch) sono fermi alla versione 1.10.5 del gioco. Che comunque merita, ma rispetto alla 1.12 odierna si vede una certa distanza. C’è un costo di 3,59 euro a versione che tutto sommato potrebbe anche valere l’esperimento, ma è doveroso segnalare che rispetto all’esperienza su Mac, non è la stessa cosa. Lo sviluppatore [sostiene](http://forums.wesnoth.org/viewtopic.php?f=6&t=37879&start=360#p578698) che tra tre settimane circa potrebbe inviare un aggiornamento su App Store. Al momento ci si può solo fidare. Se qualcuno sapesse fare di meglio, il codice sorgente è [liberamente disponibile](https://github.com/dailin/wesnoth_ios).

Tra Natale e Epifania accetto sfide.
