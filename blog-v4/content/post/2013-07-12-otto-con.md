---
title: "Otto con"
date: 2013-07-12
comments: true
tags: [Mac, iPad, iPhone]
---
Otto persone radunate per una dimostrazione tecnologica di Creative Cloud di Adobe.<!--more-->

Quattro iPhone, tre iPad, un Mac, un Vaio, un cellulare con tastiera che forse era un BlackBerry o forse un Nokia, due blocchi note di carta.

Non parlatemi mai più di quote di mercato. La realtà è composta da isole, ognuna diversa dal tutto, in cui i discorsi globali hanno poco o nessun senso.