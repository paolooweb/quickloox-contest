---
title: "Viva l’iniziativa"
date: 2014-07-20
comments: true
tags: [Swift, Automator]
---
Riporto una sintesi della mail di **Luca**, da grassetto a grassetto.<!--more-->

**Buonasera** Lucio, facendo seguito al tuo post [La trilogia dell’aspirante](https://macintelligence.org/posts/2014-06-25-la-trilogia-dellaspirante/)…

Mi sono sempre chiesto come dal nulla si possa arrivare a un’applicazione funzionante con interfaccia grafica e tutte le funzionalità a cui ormai siamo abituati, mi ha sempre affascinato la programmazione, proprio come affascinano tutte le cose che non si conoscono. […]

Mia mamma a volte mi chiede se la aiuto ad inviare via mail foto a parenti e amici. Lei non sa ridimensionare le immagini e non si ricorda mai come allegarle; io ho poca voglia di farlo al posto suo e allora, per fare presto, mando in giro mail con allegati di megabyte ciascuno intasando la rete di casa per mezz’ora.

Ieri ho aperto Automator, deciso per la prima volta a fare un’applicazione perché mia mamma possa spedire le sue foto senza troppe difficoltà.

Dovevo fare qualcosa per duplicare le foto in modo da non modificare le originali, ridimensionarle a un valore standard deciso da me, allegarle a un nuovo messaggio di Mail e poi cestinare le copie create, tutto in automatico trascinando le foto sopra l’icona del programma. Dopo meno di un’ora, l’applicazione *Ridimensiona e Invia* era pronta. Mia mamma era entusiasta.

Oggi ho pensato di modificare il programma in modo che si possa decidere durante l’esecuzione dello stesso il valore a cui ridimensionare le foto, e in modo che tramite uno *script* da terminale incorporato, l’applicazione elimini le copie temporanee create con il comando *rm* senza riversarle ogni volta nel cestino. Come si farà? Google. Fatto.

Io sono rimasto soddisfatto, almeno questa volta sono riuscito a fare una cosa, seppure piccola, che abbia le sembianze di un programma e che faccia qualcosa di utile. Forse forse riprendo in mano i libri di Objective-C e ci riprovo. La soddisfazione deve essere grande e le possibilità infinite, poi oggi c’è anche **Swift**.

Finisce qui la mail. Non è mai finita la possibilità di fare un passo avanti con le proprie capacità. Anche piccolo, è importante. Ci vuole solo un pizzico di iniziativa. Anche per questo, stavolta invece di linkare lascio le soluzioni e la ricerca al lettore.