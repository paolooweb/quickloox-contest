---
title: "Dilettanti che sbancano"
date: 2017-11-01
comments: true
tags: [iMac, Trading, Online, Borsa, Milano]
---
Settimana scorsa sono passato dalla Borsa di Milano per [Trading Online Expo 2017](http://www.borsaitaliana.it/borsaitaliana/eventi/tol2017.htm).

Immagine dallo *stand* di IWbank, che ha ritenuto di potersi affidare a iMac per presentare interfacce di lavoro presumibilmente destinate anche a professionisti.

 ![iMac per IWBank a Trading Online Expo 2017](/images/iwbank.jpg  "iMac per IWBank a Trading Online Expo 2017") 

*Post scriptum:* Mentre è ancora Halloween, MacGamer scrive che [i migliori giochi horror sono compatibili Mac](https://www.macgamerhq.com/guides/best-horror-games-for-mac/).