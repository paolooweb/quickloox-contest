---
title: "Giochi da bambinoni"
date: 2023-11-13T10:53:50+01:00
draft: false
toc: false
comments: true
categories: [Internet]
tags: [Roll20]
---
Non so perché, ma ci sono luminarie da tutte le parti eppure il Natale, in qualche modo, fatica ad arrivare.

Provo ad agevolarlo con una [raccolta di spunti-regalo preparata da Roll20](https://blog.roll20.net/posts/10-games-to-play-with-kids/), cose da giocare in famiglia con bambini da quattro anni in avanti e ragazzi delle medie.

Niente di quanto elencato si trova al supermercato o nel negozio di giochi convenzionale. Non c’è da mimare, indovinare il disegno, disegnare l’indovino, fare esplodere il maiale, mangiare le palline, fare la cacca, capire chi ha detto la bugia e tutti gli altri giochi di società, onestissimi, rispettabilissimi, francamente occasioni perse. Certo, spesso c’è un problema di partecipanti e di contesto e va capito che oltre una richiesta di sforzo elementare non si riesca ad andare.

Nell’ambito familiare c’è qualche possibilità in più di osare giochi capaci di stimolare l’ìmmaginazione o coinvolgere grandi e piccini con la mente che si allarga anziché restringersi.

Giochi in scatola per vivere avventure fantastiche, giochi di ruolo pensati per i piccolissimi, avventure propedeutiche al gioco di ruolo per partire con calma, meccanismi per avviare e coltivare storytelling; niente è più utile a una mente che si sviluppa di una bella storia, a volte da ascoltare, a volte da inventare.

Quest’estate ho azzardato una prova di gioco di ruolo con le figlie, molto libera perché con la più piccola non è ancora possibile fare cose strutturate. È stato sorprendente scoprire come, nel momento in cui le ragazze hanno compreso che potevano ingrandire e sviluppare il gioco, hanno preso le direzioni più impensabili. A un certo punto la narrazione si è trasferita dal tavolo alla casa e, visto come andavano le cose, nascondere qua e là alcune miniature è bastato a creare una esperienza *live* nella quale scendevamo in cantina a identificare il drago responsabile di una ruberia di caramelle e cose così, con tanto di discesa delle scale illuminate dalla torcia.

Divago, però resta il consiglio di considerare, anche magari nel caso di un regalo a un compagno di classe o a una amichetta, qualche scatola diversa da quelle trascinate dalla corrente.