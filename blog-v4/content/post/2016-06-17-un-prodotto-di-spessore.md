---
title: "Un prodotto di spessore"
date: 2016-06-17
comments: true
tags: [Kickstarter, Akko, ScreenSavrz, RadTech, MacBook, Pro]
---
Quando leggo della presunta obsolescenza programmata di Apple e della deriva consumistica che spinge il cliente a cambiare telefono tutti gli anni mi sento fuori dal mondo; la mia esperienza è di acquisti assai diluiti nel corso del tempo e lo scrivo alla vigilia del momento in cui probabilmente acquisterò un nuovo Mac, per stare al passo con macOS Sierra che apparentemente non funzionerà sul mio MacBook Pro 17”

MacBook Pro che è di febbraio 2009 e continuerà a vivere anche dopo un nuovo acquisto, con sette anni abbondanti di servizio straordinario sulle spalle. Credo che si sia ripagato più di cinquanta volte e di fronte a un dato del genere c’è poco da obiettare sul prezzo di acquisto.

Sto divagando. Invece, la mia esperienza di scarsissimo compratore si estende anche agli accessori. Acquisto pochissimo e sempre cose essenziali.

Se davvero arriva un Mac nuovo, farò probabilmente una eccezione a favore di un [copritastiera e proteggischermo ScreenSavrz di RadTech](https://www.radtech.com/products/screensavrz-macbook-pro-keyboard-cover). L’ho scoperto per caso e il livello di cura del prodotto mi ha sorpreso.

Solo un panno da mettere tra tastiera e schermo? Certo. Però è venduto su misura per ogni portatile Apple e attenzione, è su misura *lo spessore del panno* perché ogni modello di portatile ha la sua distanza tra tastiera e superficie Lcd. Leggere che non è raccomandato l’uso di un panno con un Mac diverso da quello per cui il panno è stato prodotto potrebbe sembrare una sparata oppure, appunto, attenzione ai dettagli.

Mi si dirà che un normale panno di microfibra basta e avanza e forse è vero. Non nego tuttavia che, sul MacBook Pro venterano di cui sopra, per il quale ho sempre usato un panno generico, qualche flebile segno del contatto tra tasti e schermo si possa notare. E mi chiedo se il livello di sofisticazione raggiunto, almeno a parole, nella produzione degli ScreenSavrz potrebbe avere evitato la cosa, oppure no.

Insomma, farò la prova, per i 14,95 dollari previsti e verificherò se le promesse di avere un prodotto su misura più di quanto sia normalmente concepito ha senso oppure è una esagerazione.

*[Anche l’idea di un [prodotto editoriale su misura più che mai prima per ilmondo Apple](https://macintelligence.org/posts/2016-06-15-un-kickstarter-per-cambiare-il-libro/) potrebbe magari essere interessante. A patto che [sia fatto conoscere ad abbastanza persone perché possa finanziarsi](https://www.kickstarter.com/projects/295108589/cuore-di-mela-i-segreti-di-apple-iphone-e-mac).]*