---
title: "Detenuti e tiranni"
date: 2021-03-12T00:26:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [Microsoft, GitHub, Hafnium, Exchange, Ars Technica, Internet Explorer, Flash] 
---
C’è un seguito alla vicenda dell’[attacco informatico che ha fatto strage di server](https://macintelligence.org/posts/In-cambio-di-che.html) negli Stati Uniti e nel mondo (sessantamila solo quelli ufficiali e chissà quanti ancora) grazie a falle storiche presenti in Microsoft Exchange e chiuse in emergenza solo pochi giorni fa.

Come succede di regola nella cibersicurezza, ora che il problema è stato arginato, un ricercatore ha pubblicato su GitHub una *proof of concept* dell’attacco: un esempio funzionante ma innocuo (per esempio, che lavora su un server configurato apposta per lasciarsi colpire e mostrare gli effetti dell’attacco stesso).

[GitHub ha rimosso il codice](https://arstechnica.com/gadgets/2021/03/critics-fume-after-github-removes-exploit-code-for-exchange-vulnerabilities/).

La cosa ha fatto scalpore perché è inusuale; le *proof of concept* sono strumenti a disposizione dei ricercatori. I pirati che volevano usare l’*exploit*, come viene chiamato il software capace di portare a segno un attacco informatico, lo hanno già fatto e certamente hanno bisogno di conoscenze complessive ben superiori a quella del codice in sé.

Il problema è che l’attacco è stato devastante per i server Microsoft Exchange, non altri; e che GitHub è proprietà di Microsoft. È da escludere che la seconda abbia ordinato alla prima di censurare, mentre invece lo zelo spontaneo per compiacere il padrone è considerabile.

Uno dirà che GitHub, in quanto casa di innumerevoli sviluppatori e progetti di software libero, non possa piegarsi troppo ai voleri di Microsoft; se delude la comunità, è l’argomento standard, la comunità se ne va.

Il problema è che la comunità inizia a essere una comunità Microsoft, di gente che programma con l’ambiente Microsoft, pubblica il software in un *repository* di proprietà Microsoft, accende istanze di cloud Microsoft eccetera.

Infatti sono sorte voci in difesa del provvedimento; una delle motivazioni è che ci sono ancora cinquantamila server non aggiornati, vulnerabili all’attacco.

Il problema dunque non è più avere server non aggiornati, ma fare sì che possano restarlo senza conseguenze.

Questa cultura è tossica e difatti ancora a livello planetario non siamo riusciti a estirpare completamente Internet Explorer 6, come del resto [vale per Flash](https://macintelligence.org/posts/Browser-fai-da-te.html).

I portatori di questa cultura detengono il software (lo fanno loro); detengono lo spazio di pubblicazione del software indipendente; detengono le menti di chi è stato assimilato e asseconda questa strategia.

La filosofia open source è abituata alla figura del *benevolent dictator*, il plenipotenziario che ha l’ultima parola sullo sviluppo del software. Questa dittatura è tuttavia molto più subdola e mira a sterilizzare l’open source, che deve diventare un parco giochi innocuo per gli interessi di Microsoft, pronto a sterzare dove serve quando serve grazie al controllo di tutti gli strumenti e di una comunità assuefatta a pensare a Microsoft tutte le volte che si parla di software libero.

Una comunità di detenuti allevata da un tiranno amorevole, morbido, simpatico, che ama tanto Linux, vende strumenti tanto comodi. E fa sparire il codice scomodo all’occorrenza.

*Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile.*