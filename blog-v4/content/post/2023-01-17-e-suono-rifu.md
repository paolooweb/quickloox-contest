---
title: "E suono rifu"
date: 2023-01-17T00:37:40+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [macOS]
---
Ho tante pagine di Safari aperte. Anche qualcuna in più. apro l’ennesima pagina e la musica si interrompe.

Riavviare il Mac? Improponibile. Meglio [chiedere a Stackexchange](https://apple.stackexchange.com/questions/16842/restarting-sound-service).

La prima risposta è:

sudo kill -9 \`ps ax|grep 'coreaudio[a-z]' | awk '{print $1}'\`

(No formattazione per evitare problemi di Markdown).

Funziona. Voglio che tutti possano sentirlo.