---
title: "Senza rete"
date: 2021-09-27T00:26:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [Sierra, High Sierra, Hp] 
---
Venghino siore e siori al più grande spettacolo del mondo! Il qui presente installa una stampante da configurare via Wi-Fi sul Mac di una casa momentaneamente senza rete a causa di un guasto che verrà risolto, dicono a giorni.

La rete viene fornita da uno hotspot che, per come funziona la prima configurazione della stampante, deve arrivare da un apparecchio diverso da quello che dovrà controllare la stampante.

Per evitare fastidì, il qui presente ha pensato bene di usare il cavo USB per  collegare la stampante al Mac; la prima configurazione deve essere online, ma poi è possibile ricorrere a USB.

Ma attenzione!, il software di gestione della stampante su Mac vuole almeno macOS High Sierra e il computer è fermo a Sierra! Così il qui presente scarica da App Store l’immagine dell’aggiornamento di sistema, con connessione cellulare essendo la casa priva di Internet come già detto.

Il più grande spettacolo del mondo! Solo un po’ lento.

Venghino, venghino…

*Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile.*