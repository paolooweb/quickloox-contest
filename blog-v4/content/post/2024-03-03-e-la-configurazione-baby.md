---
title: "È la configurazione, baby"
date: 2024-03-03T00:55:18+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [Cook, John D. Cook, Far Manager, Martin, Game of Thrones, Wordstar]
---
Per fortuna che John D. Cook ha lanciato l’esca senza metterla sull’amo, quando ha titolato [Qual è il miglior editor di codice?](https://www.johndcook.com/blog/2024/03/01/whats-the-best-code-editor/) Perché ha iniziato elencando emacs, vi, TextEdit, nano, Sublime, Notepad, Wordpad, Visual Studio, Eclipse ma poi è partito per la tangente andando a pescare uno dei migliori programmatori in attività e il suo uso di un vecchio file manager semigrafico risalente ai tempi del DOS.

Non è il primo personaggio a usare in modo attuale e competitivo un software antidiluviano. Prima di lui, solo due esempi, abbiamo avuto George Martin, autore de *Il trono di spade* e Arthur C. Clarke, uno dei più grandi scrittori di fantascienza, [rimasti fedeli a Wordstar](https://macintelligence.org/posts/2019-12-07-scrivere-come-se-fosse-il-1979/). Un programma che è giunto al massimo dello splendore alla fine degli anni settanta.

Il che porta alla vera lezione. Non è l’editor in sé, ma come lo personalizzi, lo fai tuo, gli permetti di portarti al massimo delle tue possibilità. Il valore che mi dà BBEdit in configurazione base non è paragonabile a dopo che ho inserito tutti i comandi di tastiera che volevo per ogni tag Html e aggiustato numerosi altri parametri.

Corollario: un programma dotato di millemila opzioni di personalizzazione, talmente mal congegnate al punto che nessuno lo personalizza, è un pessimo programma. Chi ha orecchie intenda.