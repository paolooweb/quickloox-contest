---
title: "Locusta vorace"
date: 2017-07-03
comments: true
tags: [Locusta, Temporis, Quinta, Colombini, Erix]
---
L’inarrivabile [Enrico Colombini](http://www.erix.it/) mi ha scritto a proposito di [Locusta Temporis](http://www.quintadicopertina.com/index.php?option=com_content&view=article&catid=38:locusta-temporis&id=61:locusta-temporis) dopo avere letto [il mio recente post](https://macintelligence.org/posts/2017-06-27-se-questo-e-un-gioco/) sui giochi *Choose Your Own Adventure*.<!--more-->

**ddregs** mi aveva fatto notare la mancanza del gioco su App Store ed Enrico mi ha purtroppo confermato che il codice necessiterebbe di aggiornamento per continuare a funzionare su iOS 11, aggiornamento che non verrà effettuato.

 ![Problemi di iOS con Locusta Temporis](/images/locusta.png  "Problemi di iOS con Locusta Temporis") 

*Le versioni eBook e Pdf dovrebbero rimanere*, scrive Enrico.

Sono in dubbio su che cosa consigliare. Spero che Enrico e l’editore Quinta di Copertina ritornino sulla decisione. Non so se e quanto eBook e Pdf consentano di fruire la storia allo stesso livello della app (suppongo di sì per l’eBook e di no per il Pdf, ma posso sbagliare).

Se la *app* davvero resterà al palo, ricomprerò la *Locusta* come eBook; è un lavoro geniale e enorme, che non sono ancora riuscito a terminare. Devo averne una copia funzionante, costi quello (pochissimo) che costi.

Una *Locusta Temporis* che, *nomen omen*, divora lentamente anche se stessa. Uffa.