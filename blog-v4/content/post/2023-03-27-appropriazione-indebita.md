---
title: "Appropriazione indebita"
date: 2023-03-27T02:10:58+01:00
draft: false
toc: false
comments: true
categories: [Software, Internet]
tags: [Gates, Bill Gates, ChatGPT]
---
Secondo Bill Gates è [cominciata l’era dell’AI](https://www.webpronews.com/bill-gates-the-age-of-ai-has-begun/) e si capisce, ha in mano prodotti che se non altro nel breve sono competitivi e il ferro va battuto finché è caldo.

Il marketing e il tirare la volata sono comunque un genere di azione diversa dall’appropriazione indebita.

In un insieme imponente di nozioni e di attualità, meritevole di lettura come minimo per la completezza, c’è una caduta di stile clamorosa. Per lui, i sistemi di sintesi generativa basati su grandi modelli linguistici (sarebbero più che altro testuali) sottoposti ad allenamento sono *intelligenze artificiali*.

Certo, dirlo fa vendere e il concetto si commercializza molto meglio dei *big data* di cinque anni fa. Il software, fatto salvo il progresso quantitativo e di velocità è sempre quello, esattamente lo stesso. Ed è intelligente come una saponetta.

Ma c’è di peggio. Gates qualifica l’intelligenza artificiale come *soluzione di problemi*, escludendo in un colpo arte, musica, danza e altre attività oziose e materialmente inutili, che segnano la grandezza dell’umanità. E inventa una distinzione tra questa intelligenza artificiale e l’*intelligenza artificiale generica*, che si occuperebbe della soluzione di problemi generici. A capire di che cosa si tratti, ovviamente.

Occupare impunemente una posizione, neanche meritata, attraverso la falsità è una brutta cosa e inganna le persone ignare. Non c’è la minima traccia di intelligenza e trent’anni fa giravano sistemi dedicati a software analogo all’odierno, solo che era più piccolo e lento per problemi facilmente immaginabili. Non parliamo di questa distinzione tra specifica e generica, buona per un imbonitore più che per un miliardario sedicente filantropo. Restituire il maltolto, prego.