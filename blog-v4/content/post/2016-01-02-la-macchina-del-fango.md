---
title: "La macchina del fango"
date: 2016-01-02
comments: true
tags: [Prompt]
---
E quando, piuttosto in là nella notte, si è deciso ufficialmente di dormire qualcuno è rimasto sveglio, usando [Prompt](https://panic.com/prompt/) per accedere a un [Mud](https://en.m.wikipedia.org/wiki/MUD) precedentemente concordato.<!--more-->

Era in buona compagnia. È stato lungo e coinvolgente.

Prompt semplifica al massimo grado la memorizzazione dei server e farsi un elenco di Mud pronti da accedere con un tocco di dito è veramente questione di un attimo.