---
title: "Vince a cuori"
date: 2022-08-18T04:00:29+01:00
draft: false
toc: false
comments: true
categories: [Hardware]
tags: [Stohen Hackett, Hackett, MacBook Air M2, MacBook Air, 512 Pixels]
---
Una piccola parte di [quello che Stephen Hackett ha da dire di MacBook Air M2 su 512 Pixels](https://512pixels.net/2022/08/m2-macbook-air-review/):

>Non è più fatto a forma di cuneo, ma ogni altra cosa che ha reso grande MacBook Air in passato è ancora qui: prestazioni allineate ai bisogni di quasi tutti, autonomia impressionante e un design pensato per chi voglia portarsi la macchina ovunque. In sé, MacBook Air M2 è uno dei migliori Mac mai fatti da Apple.

Il resto del post chiarisce che intorno c’è dell’altro e può darsi che la scelta più logica per molti sia diversa. Qualcuno potrebbe voler spendere meno e stare sul modello M1. Altri vorranno il design più solido e *da lavoro* di MacBook Pro.

Ma qui Hackett decolla e spiega che è tutto vero, solo che la macchina ha qualcosa di speciale. Per quanto la testa possa ragionare, probabilmente vincerà il cuore. MacBook Air è il computer ideale per tante persone a seguito di ragioni che vanno oltre la componentistica o i benchmark. Il dettaglio del come è il sugo che dà sapore eccellente all’articolo.

Riscaldamento, limitazioni alla velocità? *Neanche te ne accorgerai*, scrive. Apparentemente [l’ossessione dei cerotti termici](https://macintelligence.org/posts/2022-07-23-ondate-di-calore/) ce l’ha solo chi ha interesse a venderli.