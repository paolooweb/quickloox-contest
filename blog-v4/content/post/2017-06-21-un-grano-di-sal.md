---
title: "Un grano di Sal"
date: 2017-06-21
comments: true
tags: [Soghoian, AppleScript, Numbers, JavaScript, Keynote, automazione, MacStories, watch, Workflow]
---
Niente come l’automazione si conquista attraverso infinite piccole battaglie, da vincere una alla volta per riutilizzare l’esperienza acquisita nell’occasione successiva.

Per questo considero utile da leggere questo [pezzo di Sal Soghoian apparso su MacStories](https://www.macstories.net/stories/app-extensions-are-not-a-replacement-for-user-automation/): un po’ di teoria sulla differenza tra estensioni e scripting, una spruzzata di conoscenza di macOS e iOS, e uno script che lavora su cifre prese da Numbers per rappresentare un grafico in Keynote. Fatto con AppleScript oppure a scelta, non piacesse, JavaScript.

Là in fondo si intravede il traguardo di un sistema unificato di automazione che passa attraverso portatili, aggeggi da tasca, tavolette, persino orologi (Workflow [sta sul tuo polso](https://workflow.is/on/your/wrist)) e altro ancora.

L’alternativa: farsi le cose a mano in un mondo robotizzato, algoritmizzato, automatizzato. Non praticabile.