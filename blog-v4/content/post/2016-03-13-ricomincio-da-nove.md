---
title: "Ricomincio da nove"
date: 2016-03-13
comments: true
tags: [backup, iCloud, iPad, Mac]
---
A un certo punto mi ero stufato di eseguire il backup di iPad su Mac. Occupa spazio, sembrava un’operazione di costo superiore al beneficio ed ero curioso di provare il backup su iCloud.<!--more-->

Che è superiore, solo che a volte costringe a interventi manuali indesiderati, tipo per escludere cose perfettamente inutili e assai ingombranti quali la posta elettronica o le foto.

Così sono tornato al backup su Mac.

Da un po’ iPad aveva iniziato a rallentare e ogni tanto riavviarsi da solo. Sintomi tipici del degrado della memoria di massa interna.

Ho provato allora ad azzerarlo e ripristinare il backup da Mac. Tutto è andato bene, ma iPad ma mantenuto gli stessi problemi. Evidentemente, qualunque cosa causasse i riavvii, è finita anche nel backup.

C’era la possibilità che il backup fosse danneggiato. Per cui ho rifatto il backup e rieseguito l’operazione.

Risultato, il nuovo backup è venuto talmente danneggiato da risultare incompatibile e non ripristinabile (il disco interno del Mac è OK).

Mi sono stufato, ho eliminato tutti i backup da Mac e ho iniziato pazientemente a riconfigurare da zero programmi e sistema su iPad con iOS 9.2.1.

Il backup è tutto tranne che una scienza esatta.

Per la cronaca, ho riacceso il backup su iCloud. Comunque vada, mai mi dirà di essere danneggiato. Metti poi che l’Fbi sia interessata ai miei dati, facilito l’impresa.