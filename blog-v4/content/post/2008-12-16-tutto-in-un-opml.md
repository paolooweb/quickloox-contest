---
title: "Tutto in un Opml"
date: 2008-12-16
comments: true
tags: [Scrivere, Ping!]
---
Per stavolta, invece che dire *ho installato Mac OS X 10.5.6 e la mia radiosveglia &#232; tornata all&#8217;ora legale, cattiva Apple!*, controlla il contenuto dell&#8217;aggiornamento alla ricerca di indizi che possano realmente collegarsi al tuo problema.<!--more-->

I grandi di Mdj hanno pubblicato <a href="http://www.macjournals.com/extras/opml/MacOSX10.5.6Upd.opml" target="_blank">il contenuto dell&#8217;aggiornamento in formato Opml</a>. Per scoprire Opml in generale (la pagina di Mdj si vede anche dentro Safari) c&#8217;&#232; almeno <a href="http://editor.opml.org/" target="_blank">un editor disponibile per Mac OS X</a>.