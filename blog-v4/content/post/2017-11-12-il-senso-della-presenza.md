---
title: "Il senso della presenza"
date: 2017-11-12
comments: true
tags: [Codemotion, Redbull]
---
Sono passato dall’edizione milanese di [Codemotion](https://milan2017.codemotionworld.com/) con l’idea, tra l‘altro, di raccogliere qualche foto di uso di Mac. L'evento è piuttosto importante; il format è internazionale e la rete degli speaker si misura in migliaia di contatti. A Milano sono stati venduti tutti i biglietti disponibili – si paga, per entrare a Codemotion – e l’agenda prevedeva cento *talk*, mentre sul terreno erano schierate quaranta *community* di programmatori e sviluppatori, più di trenta aziende con uno stand.

Uno degli elementi tipici di Codemotion sono le offerte di lavoro da parte delle aziende, per molte decine di posizioni disponibili, tutte qualificate e a volte specialistiche. I temi trattati nelle presentazioni lo sono a tutti i livelli, ma sempre tecnici. Gli interventi di livello più alto sfiorano l’esoterico per chi non sia specificamente esperto di *quel* campo che può essere *machine learning*, *blockchain*, *serverless*, microservizi, cloud e via sviluppando.

Era presente il tradizionale spazio per gli sviluppatori indipendenti di giochi, nel quale il piatto forte era la realtà virtuale. Nelle vicinanze, per iniziativa di Redbull, era presente un distributore automatico della bevanda che forniva una lattina a quanti riuscivano a penetrare in un sistema informatico collegato.

Spero di avere dato l’idea della portata dell’evento. Un tutto esaurito da migliaia e migliaia di partecipanti paganti, esperti o in via di formazione, dove è normalissimo ascoltare una prolusione in inglese sulle *closure* di Java 9 o sugli aspetti legali dell’open source a livello aziendale.

Dopo pochi minuti mi sono cadute le braccia, perché i Mac erano presenti *ovunque*. Non dico la maggioranza assoluta, ma in grande numero, in qualsiasi ambito. Sarebbe stato magari più interessante cercare aree con *meno* Mac: gli sviluppatori di giochi tendono a mostrarne meno, lo stand di qualche azienda aveva un PC invece di un Mac.

Si tenga presente che il pubblico di Codemotion è diverso da quello dei ragazzini che affollano la fiera a caccia di gadget e neanche è composto da vecchi manager con tanto potere e altrettanti completi da pinguino: l’età media è tipica del giovane adulto, tra i venti e i quaranta, con portatili recenti, che per lo più produce invece di amministrare od organizzare e la macchina la stessa più che pavoneggiarvisi.

Sembra che i Mac siano, complessivamente, macchine molto adatte per sviluppatori all’inizio e all’apice del loro curriculum.

Oppure si sono dati convegno tutto insieme quelli che non hanno capito i propri bisogni hardware.

L’alternativa è che tante polemiche sull’utilizzo dei Mac in ambito non dilettantistico siano vicine all’aria fritta.
