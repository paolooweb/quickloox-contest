---
title: "Grandi ustionati"
date: 2022-08-02T03:23:59+01:00
draft: false
toc: false
comments: true
categories: [Hardware]
tags: [dhh, MacBook Air, MacBook Air M2, M2, Apple Silicon, M1, Speedometer]
---
C’è sempre un pazzo che ignora i segnali di pericolo, in questo caso un pazzo che ha addirittura [misurato le prestazioni di MacBook Air M2 con Speedometer](https://twitter.com/dhh/status/1553848086497214464).

<blockquote class="twitter-tweet"><p lang="en" dir="ltr">Apple&#39;s chip team continues to embarrass everyone else in the business. Just clocked a clean 400 on the Speedometer 2.0 test for the M2 Air. That&#39;s 33% faster than the M1 (and A15) can do. 2.5x faster than a 4.2Ghz i7 Intel iMac. Bananas. Try your own CPU: <a href="https://t.co/o25wL35Zu4">https://t.co/o25wL35Zu4</a> <a href="https://t.co/WbBmys2bk1">pic.twitter.com/WbBmys2bk1</a></p>&mdash; DHH (@dhh) <a href="https://twitter.com/dhh/status/1553848086497214464?ref_src=twsrc%5Etfw">July 31, 2022</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

Lo stolto non sa che dovrebbe [farcire il computer di cerotti termici](https://macintelligence.org/posts/2022-07-23-ondate-di-calore/), prima di osare l’utilizzo.

Avrà perso le ultime falangi di dieci dita? O dato fuoco alla casa? Non c’è notizia di vittime, forse perché Apple non vuole che si diffonda il panico? O è chiuso in un ambiente refrattario nell’ambito di un esperimento segreto?

Sono in ansia. Non me lo vorrei ritrovare al reparto grandi ustionati, per un MacBook Air poi. Almeno fosse un Mac Studio.