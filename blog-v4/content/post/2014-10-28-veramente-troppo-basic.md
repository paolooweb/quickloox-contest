---
title: "Veramente troppo basic"
date: 2014-10-28
comments: true
tags: [Basic, C, Pascal, SQL]
---
Mi giunge voce di una scuola dove si vuole insegnare come linguaggio di programmazione… Visual Basic. Con le motivazioni elencate qui sotto, scritte dal docente.<!--more-->

Domani pubblico le mie obiezioni. Intanto invito alla lettura, così ci si fa un’idea di come stiamo messi.

* è un linguaggio semplice, intuitivo che, grazie all’interfaccia grafica, permette agli studenti un immediato riscontro dei risultati del loro lavoro rispetto ad altri linguaggi (come C e Pascal) che richiedono basi teoriche solide

* è propedeutico alla programmazione orientata agli oggetti supportando comunque la programmazione strutturata;

* è il linguaggio nativo/applicativo della piattaforma Office e permette l’integrazione con il linguaggio SQL (anche esso previsto dai programmi ministeriali)

* è il linguaggio che meglio appassiona gli alunni in base alla mia esperienza;

* è un linguaggio comunque di livello professionale se confrontato con le recenti proposte editoriali come Scratch;

* è attualmente utilizzato anche in altri istituti scolastici statali della zona

* Ricordo di aver già proposto la piattaforma agli studenti e di non aver ricevuto alcun feedback negativo alla recente assemblea dei genitori.

