---
title: "Programmatori giapponesi"
date: 2015-12-05
comments: true
tags: [iPad, Ibm, Swift, Apple, open, source, playground, Xcode, CodeAnywhere, Sandbox, GitHub]
---
Un po’ abusato, il paragone con i soldati giapponesi nascosti in una giungla che sono andati avanti a combattere la Seconda guerra mondiale per altri trenta o quarant’anni, fino a che li hanno trovati.<!--more-->

Eppure nelle giungle *social* si trova ancora qualche giapponese pronto a spiegarci che iPad non è un vero computer perché non ci si compila sopra, o perché su iPad non si può scrivere una app per iPad, o altro a scelta tra le fantasie più gettonate.

Per prepararli a quando verranno a prenderli: a mantenimento delle promesse fatte a giugno, Apple [ha reso open source il codice di Swift](https://swift.org) e una serie di componenti a contorno. Si trova tutto [su GitHub](https://github.com/apple/).

In concreto, Swift open source significa che il linguaggio può essere arricchito e migliorato a piacere e anche usato su sistemi e in modi non contemplati (ma previsti) da Apple fino a oggi.

La notizia è vecchia di due o tre giorni e già è apparso l’esperimento di IBM con [Swift Sandbox](http://swiftlang.ng.bluemix.net/?cm_mmc=developerWorks-_-dWdevcenter-_-swift-_-lp#/repl). Il principio dei *playground* di Xcode – campi gioco virtuali in cui scrivere e collaudare codice Swift senza preoccuparsi di compilazioni e formalità burocratiche – è stato portato su web, dove basta un *browser* per scrivere codice a sinistra e vedere l’esito a destra. E il motore gira sopra… Linux.

È tutto una gran beta: adesso funziona e adesso no, adesso c’è ma è inattivato, adesso funziona ma va piano eccetera. Tuttavia è apparso nel giro di tre giorni, e da una Ibm.

Fai conto che domani diventi uno strumento affidabile e collaudato, come un qualunque [CodeAnywhere](https://codeanywhere.com). O che a qualcun altro venga l’idea di farlo senza scomodare Ibm.

Fai conto che per iPad ci sia la sua brava *app*. E ancora una volta, ecco servita la programmazione anche al giapponese.