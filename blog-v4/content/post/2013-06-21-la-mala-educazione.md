---
title: "La mala educazione"
date: 2013-06-21
comments: true
tags: [iPad, didattica]
---
Quando mi trovavo alle superiori arrivarono le calcolatrici tascabili. Tra le varie reazioni ci furono quelle preoccupate: i ragazzi avrebbero disimparato a fare di conto, che cosa sarebbe successo se una catastrofe ci avesse riportato tutti all’età della pietra, ai miei tempi si lavorava di carta e matita e non come adesso che tutto è dovuto. A me sono piaciute subito, anche se ho sempre cercato di fare i conti a mente e ancora oggi, in pizzeria, quando c’è da dividere per diciannove chi mi conosce mi riserva un’occhiata per vedere se ci sto provando (tipicamente l’importo del conto è un numero primo, con centesimi in numero primo, e ognuno ha un distinguo da fare).<!--more-->

È passato qualche decennio, faccio colazione in luogo ameno e di fianco a me giovanissime studentesse fanno i compiti con una calcolatrice in bella vista. Altro che tascabile, è più grossa di un iPad mini. In tre ne hanno due e sono di produzioni e colori diversi, oramai oggetti talmente comuni che una bambina bada alla forma, al colore, al *feeling* dando per scontate le funzioni.

 ![Calcolatrice](/images/calcolatrice.jpg  "Calcolatrice") 

Apparentemente il mondo va avanti e un numero straordinario di ragazzi cresciuti a minaccia di decerebramento per essere nati con la calcolatrice in mano fa il programmatore, l’ingegnere, il ragioniere, il cassiere, magari pure il matematico o il tabaccaio o l’idraulico. Nessuna catastrofe ci ha riportati alla preistoria e c’è una calcolatrice oramai anche dentro certi orologi da polso.

Dispiace piuttosto che quelle bambine non avessero un iPad mini, al posto di una calcolatrice che sa fare solo i conti e con la quale è impossibile creare, solo calcolare.

Ma dare tavolette agli studenti, dicono alcuni, fa impigrire, perdere il contatto con il mondo reale, poi disimparano le cose essenziali, Internet è una grande risorsa ma presenta tanti rischi, non si possono dare a nessuno fino a che non si possono dare a tutti e poi che accadrebbe se una catastrofe ci riportasse all’età della pietra.

Nel frattempo lo *School Board of Education* di Los Angeles [stipula un contratto](http://www.macworld.com/article/2042428/apple-will-roll-out-ipads-to-los-angeles-public-school-students-this-fall.html) con Apple del valore di trenta milioni di dollari per fornire una tavoletta a ogni studente, da qui al 2014. 640 mila ragazzi su oltre mille istituti, il secondo distretto scolastico statunitense dopo quello di New York City.

E mi chiedo, con questo andazzo, chi saranno i maleducati, tra i nostri studenti e i loro. Non nel senso del galateo.