---
title: "Non c’è miglior sordo"
date: 2018-08-10
comments: true
tags: [iPhone, Siri, Facebook, Alphonso]
---
Apple si è trovata davanti a una a una commissione parlamentare statunitense a spiegare che [iPhone non registra né ascolta di nascosto le conversazioni che avvengono in sua presenza](https://variety.com/2018/digital/news/apple-iphone-listen-conspiracy-theory-1202899402/).

Atto dovuto, per carità, ma va ricordato che di tutti i possibili attentati alla *privacy*, quello microfonico è il meno probabile di tutti. Specialmente su iOS, dove una app deve chiedere forzatamente al proprietario il permesso di accedere al microfono, altrimenti non può nemmeno se vuole. Permesso che è sempre revocabile.

L’unico audio che gli apparecchi sono autorizzati a captare – anche questo è disattivabile – è *ehi Siri*, che attiva un sistema di conversazione chiaramente consapevole e volontaria.

Aggiungiamo che il filtro dell’approvazione di App Store diminuisce di molto le *chance* di violazione, reale o tentata, delle regole.

Da questo punto di vista, Apple è non solo un’isola felice, ma anche l’unica. Le aziende paragonabili [non si fanno troppi problemi](https://macintelligence.org/posts/2018-07-05-paranoie-e-facili-costumi/) anzi, ci badano solo per tenere pulita la facciata.

Per quanto sapere di [sistemi tipo Alphonso](https://macintelligence.org/posts/2017-06-05-dillo-ad-alphonso/) abbia il potenziale di irritare, le app che ne fanno uso devono comunque chiedere il permesso e il meccanismo si basa su *hash* di brevi campioni audio; nessuna registrazione, nessun riconoscimento, nessuna comprensione. Irritante, ma ragionevole. E questo è il peggio che offra l’ecosistema Apple. Quando si parla di prezzi e differenze, si confrontano un sistema che non ascolta le tue conversazioni e un altro che dipende, vediamo, a seconda, può succedere, c’è da fare attenzione, non sai mai.

Nel mondo delle app, non c’è miglior sordo di iPhone.