---
title: "Mille a zero"
date: 2022-02-05T01:23:06+01:00
draft: false
toc: false
comments: true
categories:
tags:
---
Una settimana micidiale nella quale, tra l’altro, bisognava intervenire su un sito molto complesso, fatto in WordPress naturalmente che così quando devi fare qualcosa c’è un plugin apposta, per inserire tipo centocinquanta banner tutti uguali dentro altrettante pagine.

*A mano*.

Perché WordPress ha un miliardo di plugin per fare qualsiasi cosa, solo che sul WordPress del sito in questione non c’è alcun plugin per replicare uno stesso contenuto su centocinquanta pagina con un clic. O non lo hanno trovato, o non lo vogliono comprare, o hanno incaricato della manutenzione una software house che non lo sa o non lo vuole fare, la realtà è che l’unico modo era fare a mano.

Non era possibile infatti – per ovvie ragioni di sicurezza, no? – andare sul server oppure accedere alle API di WordPress.

Intanto io avevo in mente l’espressione regolare che avrebbe risolto il problema. Magari da sistemare, o con eccezioni, però buona per metterci una istruzione e finire lì. Impossibile, comunque.

Teoria a mille e pratica a zero, potrebbe essere il prossimo *payoff* di WordPress.

Ah, i banner andranno levati tra qualche tempo.

A mano.
