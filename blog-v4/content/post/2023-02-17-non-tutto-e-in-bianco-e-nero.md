---
title: "Non tutto è in bianco e nero"
date: 2023-02-17T00:33:44+01:00
draft: false
toc: false
comments: true
categories: [Internet]
tags: [Reddit, Stockfish, ChatGPT]
---
Con questo post annuncio una moratoria unilaterale su ChatGPT e compagnia: d’ora in avanti e salvo richieste, solo post che abbiano valore divulgativo e scientifico e solo se assolutamente di qualità.

Anche perché oggi ho scoperto che [lo hanno messo a giocare a scacchi contro Stockfish](https://www.reddit.com/r/AnarchyChess/comments/10ydnbb/i_placed_stockfish_white_against_chatgpt_black/). L’animazione della partita, meno di un minuto, è veramente imperdibile.

Si tenga presente come esistano nel campo intelligenze artificiali che, pur non essendolo, dominano gli scacchi in modo assoluto. AlphaZero di Google [ha imparato a giocare in quattro ore](https://www.youtube.com/watch?v=0g9SlVdv1PY).

ChatGPT è un simpatico *idiot savant* con un grande talento nel rimescolare le informazioni che possiede ed è tutto.

<iframe width="560" height="315" src="https://www.youtube.com/embed/0g9SlVdv1PY" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>