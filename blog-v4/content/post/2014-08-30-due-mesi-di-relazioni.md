---
title: "Due mesi di relazioni"
date: 2014-08-29
comments: true
tags: [Gurman, 9to5Mac, Apple]
---
Mark Gurman di *9to5Mac* afferma di avere lavorato per due mesi al suo articolo [Seeing Through the Illusion: Understanding Apple’s Mastery of the Media](http://9to5mac.com/2014/08/29/seeing-through-the-illusion-understanding-apples-mastery-of-the-media/), che svela i retroscena della macchina di relazioni pubbliche Apple.<!--more-->

Un reparto non particolarmente numeroso che cura un meccanismo di comunicazione copiato e invidiato molto più di quanto accada a iPhone o alle interfacce grafiche, meccanismo dove nulla è lasciato al caso e si organizzano quindici giorni di prove assolutamente blindate prima di un evento pubblico.

L’articolo è clamorosamente lungo e dettagliato, tanto che è stato diviso in nove sezioni. Ne raccomando la lettura integrale anche se sono arrivato solo a metà della seconda parte.

>Ogni singolo elemento della presentazione viene determinato specificamente in anticipo, dalle sfumature dell’illuminazione al posizionamento degli schermi a chi siede e dove siede. I dipendenti vengono distribuiti strategicamente all’interno del pubblico, i giornalisti hanno ognuno una propria posizione e i dirigenti di Apple non hanno bisogno di preoccuparsi di cambiamenti all’ultimo minuto. È tutto sotto controllo.

Si può pensare che venga a mancare la poesia. Al contrario, leggendo del reparto relazioni pubbliche mi viene da pensare alla cura che viene messa nei prodotti, della quale la cura messa nelle presentazioni è un sintomo evidente. Pensando al prossimo evento del 9 settembre, mi sento più intrigato, non meno, cogliendo la profondità del lavoro e non solo la facciata offerta al grande pubblico.