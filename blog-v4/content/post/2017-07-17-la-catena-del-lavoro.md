---
title: "La catena del lavoro"
date: 2017-07-17
comments: true
tags: [Academy, Apple, Akko, app]
---
L’amico Akko mi fa sapere di aspettare da due settimane una risposta alla email che riporto di seguito:

>Buongiorno, leggo che avete “sfornato” da poco la prima classe di graduati della Apple Academy. Noi saremmo interessati a ingaggiarne uno, per ora per un lavoro che impegnerà circa sei mesi e potenzialmente per una assunzione a tempo indeterminato alla fine. Come faccio a far pervenire ai ragazzi una proposta?

La email è stata spedita il 5 luglio all’indirizzo <a href="mailto:developeracademy@unina.it">developeracademy@unina.it</a>, che appunto corrisponde alla [Apple Academy](https://www.developeracademy.unina.it/it/).

Una piattaforma affamata di talenti, strumenti eccellenti per impratichirsi, un luogo dove imparare e formarsi, ragazzi entusiasti che guardano a un possibile nuovo mestiere e comunque a una preparazione non comune.

Una catena promettente, solo che manca un anello, piccolo piccolo.

**Aggiornamento**: le cose si muovono e può darsi che avremo un lieto fine. Davvero un anellino.