---
title: "Già al lavoro"
date: 2018-01-09
comments: true
tags: [ Leopolda, Mac, Claudio]
---
Immagini gentilmente e deliziosamente fornite da **Claudio** che così chiosa:

>Pitti Uomo - Stazione Leopolda - Firenze, allestimento sfilata Woolmark

 ![Mac al lavoro alla Leopolda per Pitti Uomo](/images/mac-leopolda.jpeg  "Mac al lavoro alla Leopolda per Pitti Uomo") 

>Tanto per inquadrare l’ambiente…

 ![Allestimento della sfilata Pitti Uomo alla Leopolda](/images/rotaie-leopolda.jpeg  "Allestimento della sfilata Pitti Uomo alla Leopolda") 

Tre Mac su tre portatili non è male.
