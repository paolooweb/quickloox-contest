---
title: "Aggiungi un milione di posti a tavola"
date: 2020-03-14
comments: true
tags: [Apple, Wwdc]
---
Ho avuto modo di assistere a qualche *keynote* Apple dal vivo ma non ho mai partecipato a una edizione di Wwdc.

Neanche quest’anno la vedrò dal vivo, ma invece di attendere la fine della manifestazione per vedere i materiali, [potrò seguire in diretta o quasi tutto quello che l’agenda mi renderà possibile](https://www.apple.com/newsroom/2020/03/apples-wwdc-2020-kicks-off-in-june-with-an-all-new-online-format/). Assieme ovviamente a qualche altro milione di interessati.

Se vale per me vale per moltissimi altri e significa che il 2020/2021 potrebbe portare a una fioritura eccezionale dello sviluppo software indipendente per Mac e iOS.

Considerazione a latere: passato l’allarme attuale, credo che dall’anno prossimo torneremo a vedere una Wwdc in luogo fisico e così per molti altri grandi eventi di risonanza planetaria.

Mi chiedo però per quanti altri si scoprirà che mantenerli online o farli diventare online sia la cosa più giusta.