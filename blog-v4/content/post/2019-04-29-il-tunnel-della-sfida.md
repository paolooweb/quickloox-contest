---
title: "Il tunnel della sfida"
date: 2019-04-29
comments: true
tags: [Evans, Hackintosh, Raspberry, FreeBSD]
---
[Scrive Benedict Evans](https://macintelligence.org/posts/2019-04-28-dove-si-va-per-i-servizi/) a proposito di un tema assai scollegato dall’argomento di oggi:

>Come ci siamo spostati verso strati di astrazione superiori (dalla riga di comando a finestre e mouse a multitouch, dal software locale a quello in cloud) qualche cosa sparisce nei livelli inferiori. […] Non usiamo più utility di deframmentazione né ci preoccupiamo di configurare l’hardware e, su un iPhone o un Chromebook, non dobbiamo preoccuparci dei virus. […] Così come Apple ha provato a risolvere deframmentazione o plug and play, oggi cerca di risolvere nuovi problemi nella nostra esperienza del computing. 

È una buona approssimazione del perché leggo dell’ennesimo [Hackintosh](https://motherboard.vice.com/en_us/article/8xznw4/how-to-make-a-hackintosh-laptop) e avverto sintomi di orticaria.

L’argomento è diventato più affascinante di qualche anno fa. Ci sono nuove utility che facilitano certe fasi del lavoro e c’è tutto il vecchio armamentario delle cose che si sono sempre dette: una sfida con se stessi, la libertà di fare come si vuole, il gusto di mettere le mani nell’hardware, una volta i computer erano espandibili e via così.

Appunto, è roba che è sparita. La mia esperienza attuale è tenere allineati gli ambienti di lavoro di Mac e iPad Pro, così che siano intercambiabili sotto le mie mani. Se voglio provare il brivido della sfida e dell’hardware artigianale, porto a casa un [Rapsberry Pi](https://www.raspberrypi.org/) equipaggiato di tutto l’immaginabile per cento euro e ci metto pure sopra [FreeBSD](https://www.freebsd.org/).

Le altre scuse, mai veramente valide, hanno stufato. Un Mac costa più di un Hackintosh, ovviamente, se la manodopera è gratis. Inoltre, se mi porto a casa un Raspberry Pi, lavoro per sfruttarlo al centouno percento di quello che può fare. Quello che monta l’Hackintosh avvisa chiaramente che ci si porta a casa una cosa che è *meno* di un Mac e lo è proprio dove fa più male:

>Una cosa su cui Apple è davanti a chiunque altro è il trackpad. Il trackpad Synaptic su questa macchina funzionerà bene con un po’ di sintonia fine, ma i gesti saranno molto più limitati di quelli consentiti dal trackpad Apple.

Non è più la sfida con se stessi, ma volersi fare male. Uscite dal tunnel dei computer usati per lavorarli invece di quelli usati per lavorare.
