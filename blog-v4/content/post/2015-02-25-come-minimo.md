---
title: "Come minimo"
date: 2015-02-27
comments: true
tags: [UltimateStatusBar, MinimalStatusBar, Safari, Chrome]
---
Sono normalmente poco disposto verso i meccanismi di personalizzazione dei programmi di Apple, perché risolvono magari un problema e poi ne creano un altro, perché spesso non si aggiornano più mentre i programmi continuno ad aggiornarsi, perché – soprattutto – non si integrano bene con il *design* del programma ed è la cosa peggiore. Di caos visivo e funzionale sullo schermo ce n’è già abbastanza.<!--more-->

Ciò detto, mi sono lasciato tentare da [Minimal Status Bar](http://visnup.github.io/Minimal-Status-Bar/) per Safari. Questa estensione riduce la barra di stato del *browser* all’angolo inferiore sinistro, in modo simile a quella di Chrome, e mostra per intero i famigerati link compressi che infestano la rete (così uno sa dove sta *veramente* facendo clic).

Ingombro minimo e ancora più minimo di quello standard, funziona, fa esattamente e solo quello che annuncia di fare e – soprattutto – si integra perfettamente nel *design* di Safari. Vita migliorata di una parte su un milione, però effettivamente e incontestabilmente.

Per chi volesse invece una barra di stato con un approccio totalmente diverso, che fa tutto e il suo contrario, ovvero ancora qualcosa in più di cui occuparsi con la promessa di ripagare con gli interessi, forse [Ultimate Status Bar](http://ultimatestatusbar.com) può essere qualcosa da provare. Io preferisco il minimalismo e non l’ho provata.