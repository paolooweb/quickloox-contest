---
title: "L’unico pericolo"
date: 2022-06-23T01:12:24+01:00
draft: false
toc: false
comments: true
categories: [Hardware]
tags: [iPad, Nive, GarageBand]
---
Passata un’ora insieme alla secondogenita, cui piace cantare a squarciagola in finto inglese, a registrare tracce in [GarageBand](https://www.apple.com/mac/garageband/) con i relativi effetti collaterali: scoprire il suono di sé raddoppiato, provare a distorcere la voce per sembrare un animaletto o un mostro, fare rumore con questo e quello strumento. A quattro anni di sicuro c’è chi è già al secondo maestro di pianoforte; non abbiamo alcuna di queste ambizioni e ci siamo semplicemente divertiti un sacco.

Questa sarebbe pericolosa *tecnologia digitale*, potenzialmente [nociva per lo sviluppo e la crescita](https://retezerosei.savethechildren.it/approfondimenti/diritto-protezione/luso-delle-tecnologie-digitali-nei-bambini-da-0-a-6-anni/).

Dicevo, passata un’ora; dopo sessanta minuti la secondogenita ha perso interesse nella cosa e si è messa a giocare ad altro.

Quindi devo dire che eravamo su iPad, pericoloso *tablet* cui stare attenti perché in quanto tale [genera problemi di dipendenza](https://www.repubblica.it/salute/2022/04/15/news/bambini_troppo_tempo_sul_telefonino_cosa_fare-343575191/). (Non che nella nostra famiglia la cosa sia una novità o una eccezione, [anzi](https://macintelligence.org/posts/2017-07-11-disintossicazione-infantile/)).

Mezz’ora dopo abbiamo dichiarato la fine serata e lei è andata a letto del tutto normalmente. Ora dorme come un sasso.

A proposito della pericolosa luce blu dello schermo che [disturba il sonno dei bambini](http://webfio.it/sonno-e-luce-blu-qual-e-il-collegamento/).

Il tutto su un iPad di prima generazione, acquisito dal sottoscritto a maggio 2010, più anni da solo delle mie figlie insieme.

Perché, non dimentichiamolo, c’è la pericolosa [obsolescenza programmata](https://www.greenme.it/scienza-e-tecnologia/mobile/obsolescenza-programmata-apple-manipola-gli-iphone-di-proposito-parte-la-nuova-class-action-per-difendersi/).

L’unico pericolo è dare troppo retta alle stupidate che popolano la rete. E non si parla solo delle *fake news*.