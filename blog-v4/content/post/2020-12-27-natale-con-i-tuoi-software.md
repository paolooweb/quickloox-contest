---
title: "Natale con i tuoi software"
date: 2020-12-27
comments: true
tags: [Natale, Mosaic, Amazon]
---
Considerazioni su un Natale vissuto più tecnologicamente del solito.

I programmi, ma specialmente i browser, dovrebbero avere per legge un *elderly mode* con pulsanti grandi come campi di patate e *tooltip* conseguenti. Massimo numero di comandi permessi, quello offerto dalla prima edizione di [Mosaic](http://www.ncsa.illinois.edu/enabling/mosaic/versions) (già troppi).

I programmi di videoconferenza dovrebbero partire obbligatoriamente in *modalità galateo*: chiunque parli, silenzia automaticamente tutti gli altri per massimo un minuto. Passato un minuto, viene attivato a caso un altro partecipante. La modalità si disattiva da sola dopo che per un giro tutti hanno rispettato il limite di tempo da soli e si riattiva alla terza violazione della regola.

Si può discutere di Amazon e dei piccoli negozi, ma il 23 dicembre alle 18 ho avuto bisogno di un microfono e il 24 mattina alle 10:30 mi è arrivato a casa. I regali per le maestre della scuola li abbiamo ordinati il 9 dicembre sul sito di una prestigiosa casa produttrice di carta e strumenti per la grafica: non ci è stata data alcuna data di consegna, neanche stimata; ogni giorno il pacco risultava *in transito*, non giunto al magazzino di destinazione. Nessuna previsione di consegna, nessuno stato preciso del pacco (non arrivato ok, ma almeno è partito? boh). Il magazzino aveva il servizio clienti fino alle 17 e, quando ho chiamato alle 16:40, nessuno ha risposto, né c’era una segreteria, né una chat, niente. Il pacco è arrivato sabato 19 mattina, senza alcun preavviso nemmeno venerdì sera.

La [legge di Sturgeon](http://gandalf.it/arianna/sturgeon.htm) (il novanta percento di tutto è spazzatura) si applica pienamente anche ad App Store. Non che ci fossero dubbi, ma una verifica empirica fa toccare le cose con mano.