---
title: "Uno lo sente"
date: 2016-03-18
comments: true
tags: [Audacity]
---
Da molto tempo trascuravo [Audacity](http://www.audacityteam.org). Per svolgere un lavoretto audio facile facile ho scaricato la nuova versione 2.1.2 uscita a gennaio e la devo raccomandare, perché rispetto alle precedenti, lato Mac, c’è un salto di qualità eccezionale.<!--more-->

Oltretutto ci sono soluzioni di interfaccia qua e là che risultano comodissime e questo, nell’*open source*, è una rarità. Il progetto si è evoluto molto e bene, si sente da subito. Bravi.
