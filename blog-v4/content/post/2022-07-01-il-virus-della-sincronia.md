---
title: "Il virus della sincronia"
date: 2022-07-01T16:10:25+01:00
draft: false
toc: false
comments: true
categories: [Internet]
tags: [comunicazione, exchange, email, nativi digitali]
---
La vecchiaia non è anagrafica; è l’incapacità di comprendere quello che accade dopo il trentacinquesimo anno di età, questo sì, anagrafico.

Per questo sono perseguitato da gente che manda messaggi in tutti i modi possibili per avvisarmi che dobbiamo *sentirci* riguardo a una qualche questione.

Sono tutti vecchi, non anagrafici; gente che non ha mai capito il lavoro asincrono e vive nell’illusione che *sentirsi* sia più veloce per portare a termine il lavoro.

Non è più veloce.

Chi mi ha mandato un messaggio, stamattina, mi ha colto nel mezzo di una riunione. Non potevo parlarci perché stavo parlando con altri (anche loro convinti che eccetera). Ho scritto che ero nel corso di una riunione e che se mi avesse scritto del problema le avrei risposto intanto che partecipavo all’altra riunione.

(La prossima volta che sei dentro una riunione, calcola il tempo in cui hai dovuto effettivamente contribuire rispetto al tempo totale. Sottrai tempo attivo da tempo totale e hai la risultanza del tempo perso. Moltiplica il tempo perso per i partecipanti. Moltiplica il tempo perso per la paga oraria. Ottieni i soldi persi dall’azienda durante la riunione).

Ma niente, dovevamo *sentirci*. Un’ora dopo sono riuscito a richiamare. L’incipit è stato *bene che ci sentiamo, così andiamo più veloci*. Il lavoro era fermo da un’ora.

La verità è che il tempo necessario a dirimere in modo efficace un problema attraverso la comunicazione è *invariante*. Ci sono solo due ragioni, fuori dalle emergenze reali, per cui una persona trova accettabile usare la comunicazione sincrona:

- è incapace di articolare in modo efficace un pensiero scritto;
- intende risparmiare il proprio tempo e quindi, per l’invarianza di cui sopra, costringe l’interlocutore a impiegarne di più;
- intende fare pesare la propria posizione gerarchica a latere della comunicazione;
- intende manipolare l’interlocutore grazie alle sue possibili capacità di ingegneria sociale.

(Lo so che avevo detto due ragioni).

Il terzo punto, tra l’altro, è quello che per cui le aziende retrive pretendono che il dipendente in (sedicente) *smart working* stia incollato davanti al computer quando lavora da casa propria. Serve solo a fare pesare la gerarchia e come succedaneo di una pressione psicologica applicata fisicamente in ufficio da superiori inadeguati.

No, il fatto che tu sia cresciuto professionalmente in un ambiente dove la gente si telefona a tutto spiano non ti ha insegnato a comunicare; solo a telefonare. Se tu fossi cresciuto prima, ora pretenderesti di alzarti e andare dal tuo ufficio a quello del collega tutte le volte che serve dirsi qualcosa. Se tu fossi cresciuto dopo, telefoneresti perché saresti un *nativo digitale*, ossia una persona abituata ad avere in mano mezzi di comunicazione… che nessuno ti ha insegnato a usare e quindi ti ritrovi con le stesse competenze comunicative del vecchio (non anagrafico) ingessato dentro il lavoro sincrono.

La sincronia è un virus peggiore di quello che [girava ultimamente sui server Exchange](https://www.bleepingcomputer.com/news/security/microsoft-exchange-servers-hacked-to-deploy-blackcat-ransomware/).

(Lì girava del ransomware, ma prendo licenza per comporre la frase a effetto).

Poi un giorno parliamo anche di quelli convinti che si lavori sempre comunque e dovunque tramite la posta elettronica, anche loro prigionieri di quello che gli hanno tirato addosso quando hanno iniziato a lavorare. Purtroppo la malattia non è ancora riconosciuta come tale e quindi fargli notare che hanno un disturbo della personalità, invece che un percorso di crescita, provoca una reazione stizzita.