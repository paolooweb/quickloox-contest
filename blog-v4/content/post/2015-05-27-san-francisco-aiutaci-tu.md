---
title: "San Francisco aiutaci tu"
date: 2015-05-27
comments: true
tags: [SanFrancisco, watch, iOS9, OSX]
---
I futuri detrattori a prescindere di San Francisco, il nuovo font di watch e secondo chiunque anche quello di iOS 9 e OS X 10.11, sono gentilmente pregati – prima – di visitare la pagina dedicata di [Type Detail](http://typedetail.com/san-francisco.html) e comprenderla in ogni sua parte.