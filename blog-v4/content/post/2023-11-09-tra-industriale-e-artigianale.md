---
title: "Tra industriale e artigianale"
date: 2023-11-09T22:52:32+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [Ango, Steph Ango, Runestone, Warp, Strozzi, Filippo Strozzi, Tailscale]
---
Parto dalla fine di questo sanissimo [post di Steph Ango](https://stephango.com/quality-software), La cui segnalazione devo a [Filippo](https://www.avvocati-e-mac.it), che ringrazio:

>Se vuoi vivere in un mondo popolato da più di una manciata di produttori di software, spendi qualcosa di più in software indipendente di qualità. È una cosa che merita una piccola parte dei tuoi sudati guadagni.

Le multinazionali hanno creato un mondo dove chiunque ha l’impressione che il software debba costare poco o nulla… e hanno messo a punto strategie per rovesciare il costo reale addosso al consumatore senza che questo se ne accorga. Google offre buon software gratis ma in cambio vampirizza la privacy; Microsoft butta nel cloud tutto quello che può dando l’impressione che arrivi come add-on o a prezzo di favore, quando invece costa (indirettamente) la morte di tutta la concorrenza che ardisca stare nelle vicinanze. Apple offre software gratuito, purché si comprino i propri apparecchi eccetera.

Ango spiega perché avere software a disposizione software industriale offra notevoli vantaggi e perché sia necessario disporre di alternative indipendenti, artigianali chiamiamole. La marmellata al supermercato ha diversi vantaggi ma certo quella creata a mano dal fattore con i frutti del campo ha un altro sapore, anche se costa un po’ di più.

Dedicare qualche soldino del nostro pur risicato budget informatico al software indipendente di qualità è una mossa che aiuta tutti e fa vivere meglio tutti, anche se con qualche euro di meno in tasca. Vale per per il software open source, che è libero di quella libertà analoga a quella di espressione e di movimento, non libero della libertà analoga all’*arraffa e fregatene*.

Accettare di pagare un giusto costo per programmi indipendenti di qualità aiuta certo un tot di programmatori a guadagnare uno stipendio, ma porta benefici alla nostra qualità della vita computazionale e sostiene una idea di diversità che è positiva anche nel software e non solo in natura.

Su App Store è ancora più facile che su Mac, vista la scala di prezzo decisamente più bassa.

Scrivo questo pezzo su [Runestone](https://runestone.app), per cui ho pagato la versione piena, e più tardi lo pubblicherò via Mac grazie a [Warp](https://www.warp.dev), di cui uso le funzioni gratuite e non manco di promuovere quando posso, un po’ come [Tailscale](https://tailscale.com). Non è obbligatorio né inevitabile; è una libera scelta che ognuno di noi compie secondo le proprie possibilità e che, nel proteggere l’artigianalità, protegge la libertà.