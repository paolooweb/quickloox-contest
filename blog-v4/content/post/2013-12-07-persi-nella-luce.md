---
title: "Persi nella luce"
date: 2013-12-07
comments: true
tags: [iPhone, Android]
---
iOS contiene una funzione di torcia elettrica (non schermo alla massima luminosità al solo scopo di fare luce, come le vecchie *app*, ma accensione del flash di bordo) che ha reso inutili le varie *app* prima nate per lo stesso scopo.<!--more-->

I possessori di iPhone sono comunque penalizzati perché, con tutte le alternative a disposizione, manca qualcosa come [Brightest Flashlight](https://play.google.com/store/apps/details?id=goldenshorestechnologies.brightestflashlight.free) di Android.

Scaricata tra cinquanta e cento milioni di volte, [registra di nascosto](http://www.ftc.gov/opa/2013/12/goldenshores.shtm) l’identificativo del telefono e la posizione geografica, per inviare i dati a gente vogliosa di monetizzarli.

Chi compra Android vede evidentemente cose che altri non riescono a vedere. Chissà se l’[ordine ufficiale](http://www.ftc.gov/os/caselist/1323087/131205goldenshoresorder.pdf) della Federal Trade Commission americana li illuminerà.