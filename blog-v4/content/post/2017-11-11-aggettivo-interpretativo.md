---
title: "Aggettivo interpretativo"
date: 2017-11-11
comments: true
tags: [iPhone, X]
---
Messo a confronto da professionisti con una videocamera professionale, iPhone X se la cava piuttosto bene.<!--more-->

Per una volta la [videorecensione](https://www.youtube.com/watch?v=hyJbIwWma3Y) è divertente e realizzata con gusto oltre che perizia tecnica. Si guarda volentieri, sono sei minuti da spendere.

Il verdetto, sintesi estrema, è che iPhone X soffre le condizioni di scarsa illuminazione. Se però c’è luce adeguata, offre risultati di qualità professionale. Oddio, l’ho detto.

<iframe width="560" height="315" src="https://www.youtube.com/embed/hyJbIwWma3Y" frameborder="0" allowfullscreen></iframe>