---
title: "Grandi e piccine"
date: 2021-08-07T01:50:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [Apple, Csam, iPhone, iCloud, Minority Report] 
---
Ho iniziato a leggere tutta la documentazione tecnica legata all’annuncio della [protezione allargata dei bambini](https://www.apple.com/child-safety/) annunciata da Apple. Una faticaccia, ma voglio capire molto bene ogni piega dell’annuncio.

Al momento capisco che sono stati fatti tutti gli sforzi possibili per tutelare la privacy. Molti dei commenti in merito all’annuncio sono inesatti quando non inventati di sana pianta: umani di Apple intervengono (e possono intervenire) solo e unicamente in casi di gravità clamorosa, che eccedono una soglia di tolleranza sotto la quale tutto resta invisibile come è giusto che sia. Il lavoro di identificazione dei contenuti pericolosi per i bambini viene svolto in locale da software di apprendimento meccanizzato. Il confronto con il materiale fotografico presente su iCloud – la funzione agisce solo se è attivo il servizio iCloud Photos – avviene attraverso hash che oscurano totalmente la sostanza del materiale. Eccetera.

Le nuove funzioni di protezione inizialmente avranno seguito solo negli Stati Uniti e verrà valutata con attenzione ogni espansione internazionale, per non dare a sistemi repressivi l’occasione di abusarne. Eccetera.

A parole funziona tutto e Apple, che lo scrive, si rende conto della grande ambizione dietro un programma con questi obiettivi, quindi del potenziale di rischio che presenta se male attuato.

Grande ambizione di Apple. Mia piccola, piccolissima obiezione: *ad Apple dovrebbe importare zero di che contenuti tiene la gente sui propri apparecchi*.

Tutto il resto arriva dopo. I reati, fuori dalla fantascienza di [Minority Report](https://en.wikipedia.org/wiki/Minority_Report_(film)), si perseguono dopo che sono stati commessi e nelle nostre case non avvengono perquisizioni periodiche per verificare che riviste leggiamo o che cosa mostra il calendario appeso al muro.

Apple dovrebbe essere totalmente indifferente a quello che tengo e scambio attraverso il mio iPhone. Dovrebbe inoltre difendere con le unghie e con i denti la mia possibilità di essere l’unico a sapere che cosa tengo e che cosa scambio. Tutto il resto percorre la proverbiale strada lastricata di buone intenzioni, che porta sai bene dove.

*Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile.*