---
title: "I cento giorni"
date: 2015-06-04
comments: true
tags: [WirelessKeyboardII]
---
Ottavo cambio di pile, se conto bene, sulla [Wireless Keyboard II](https://www.apple.com/it/keyboard/), con 103 giorni di durata alle spalle (e un paio di sessioni-valanga di diverse ore con iPad).<!--more-->

Mi sembra di vedere conferma di un maggior consumo della tastiera lasciata andare a riposo senza intervenire manualmente: la media di durata è di 247 giorni, solo che quattro degli ultimi cinque valori sono sotto media e quattro dei primi cinque valori sono sopra la media (all’inizio spegnevo manualmente la tastiera a fine utilizzo).

Sembra evidenziarsi un *pattern* di cento giorni e poco più di autonomia, con gli ultimi tre valori rispettivamente di 110, 118 e 103 giorni.