---
title: "Maestra della voce"
date: 2023-12-06T02:20:26+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [macOS, Personal Voice]
---
Il valore della lettura nel coltivarsi: ho finalmente capito la funzione [Personal Voice](https://www.apple.com/accessibility/the-lost-voice/), novità di macOS alla… voce accessibilità.

La preziosità della tecnologia per chi soffre una perdita o una alterazione del parlato mi era apparsa vaga, mentre invece c’è tutta una categoria di disabilità dove passiamo dal condurre una vita vicina a quella normale al salvavita.

Soprattutto avevo pensato cinicamente all’abuso che sarebbe possibile fare e avanzato dubbi praticamente su una possibilità supplementare di creare *deepfake* e attentare alla sicurezza di qualche sistema biometrico.

Ho visto invece che Apple ha fatto il possibile per tutelare la privacy e la sicurezza alla sua maniera: la voce personale viene salvata in modo sicuro solo sull’apparecchio locale, può essere usata solo dal proprietario dell’account e, anche se vale poco nel mondo di oggi, i termini di uso ammettono solo un impiego personale e non commerciale.

Riflettendoci, meglio aiutare chi ha un vero beneficio, a costo di qualche rischio prevedibile, che rintanarsi dietro la paura. Di fatto, non lo facesse Apple, prima o poi qualcuno lo farebbe e da tempo girano notizie di *deepfake* audio. Meglio allora avere una cosa fatta da Apple, che è curata e tutela il più possibile l’utente legittimo.

(Che poi si tratta di una applicazione da manuale di *machine learning*. Apple viene data da alcuni come indietro sulla sedicente intelligenza artificiale perché non ha realizzato il proprio *chatbot*. Eppure sembra muoversi con disinvoltura nell’uso dei componenti base della sedicente intelligenza artificiale stessa).

<iframe width="560" height="315" src="https://www.youtube.com/embed/UbjOXKUrGpE?si=19G1BGkq7z35Mecw" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>