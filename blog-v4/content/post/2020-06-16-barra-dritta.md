---
title: "Barra dritta"
date: 2020-06-16
comments: true
tags: [BitBar, Mac]
---
Sarà che si avvicina Wwdc; di fatto mi ritrovo a guardare con gusto a cose che da tempo trascuravo o rifuggivo.

Per esempio, sono diventato via via restio a installare aggeggi che affollino la barra dei menu su Mac. Solo che [BitBar](https://getbitbar.com) è troppo semplice e ingegnoso per non provarlo.

Il principio è semplice: BitBar può mostrare a partire dalla barra dei menu l'effetto di qualsiasi script in funzione su Mac.

Bella idea. Moltiplicata per la rete, eccellente idea: chiunque può creare facilmente uno script apposta per BitBar sotto forma di plugin. Ne esistono già decine, opera di altrettanti sviluppatori che hanno contribuito.

Gratis, _open source_.

Invece che più affollata, la barra dei menu diventa più furba e così ci piace.