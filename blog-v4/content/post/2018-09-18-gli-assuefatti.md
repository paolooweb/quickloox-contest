---
title: "Gli assuefatti"
date: 2018-09-18
comments: true
tags: [Windows, OSX, macOS, iOS, watchOS, tvOS]
---
Mi è capitato recentemente di sentir dire che chi usa Apple sia una specie di monomaniaco perché mal sopporta l’uso di sistemi operativi diversi.

Non entro del merito delle cose alternative che si possono fare in termini di sistema operativo, da Linux in giù. Continuo a usare la mia macchinetta virtuale per fare esperimenti e pasticci con [FreeBSD](https://www.freebsd.org), ma non conta: di questi tempi basta perfino il gratuito [VirtualBox](https://www.virtualbox.org) per accendere decine di macchine diverse, con i sistemi operativi più disparati.

Ho anche usato Windows in qualche momento della vita, al lavoro presso Virgilio per esempio. Neanche questo conta, il lavoro è un’altra cosa.

Invece, in ambito Apple, pensavo ai sistemi operativi che ho usato: ProDos, la prima serie per Mac (culminata con System 7, MacOS 8, MacOS 9), Newton OS, Mac OS X/OS X/macOS, iOS, watchOS, tvOS. Forse dimentico qualcosa e comunque la classificazione non è rigorosa; per esempio non conto qualsiasi cosa facesse funzionare Apple IIgs.

Certo molte cose hanno punti di similitudine evidenti e anche abbondanti. Non sono la stessa cosa, però. Se mi siedo alla scrivania, vedo una barra dei menu, finestre, un puntatore sullo schermo, menu a discesa. Se prendo in mano iPad, ho una griglia di icone. macOS si basa dal 1984 sull’idea che tutti i comandi a disposizione debbano essere esposti nel modo più evidente possibile. iOS si basa sull’idea che le opzioni a disposizione vengano scoperte attraverso i gesti. Sono due punti di partenza antitetici e, per quanto l’icona delle Impostazioni sia uguale, è inevitabile constatare una diversità di fondo.

Mentre uno che usa Windows lo usa sul telefono, sul computer, sulla tavoletta. È sempre quello, con le inevitabili eccezioni che vengono dagli schermi e dai meccanismi di interazione; ma è sempre quello.

Eppure gli assuefatti sono quelli di Apple.