---
title: "Corpi elettorali"
date: 2019-05-02
comments: true
tags: [Butterick, Typography]
---
Molto meglio di tante parole e tanta propaganda sparata con il ventilatore: se vuoi valutare un politico, analizza la sua *tipografia*.

Pochi possono farlo meglio di Matthew Butterick, l’autore di [Practical Typography](https://practicaltypography.com), che [ha messo in fila i candidati democratici alle presidenziali americane del 2020](https://practicaltypography.com/typography-2020.html). E lui stesso spiega perché:

>La tipografia è una decisione concreta che i candidati devono prendere oggi e spenderci denaro vero, con conseguenze effettive. Se non posso fidarmi che tu sappia scegliere una manciata di caratteri e colori in modo ragionevole, perché dovrei fidarmi a metterti in mano la valigetta con i codici per il lancio delle armi nucleari?

Butterick sostiene che i candidati usano la tipografia nell’intento di mettere in luce i propri punti di forza e tendono invece a evidenziare le loro limitazioni.

Sarebbe bello avere una persona competente che si cimenta allo stesso modo con gli italiani. Nel mentre, se a qualcuno frullasse in testa la malsana idea di donare denaro a qualcuno dei protagonisti della scena politica attuale nel nostro Paese, suggerisco sommessamente di destinare la stessa cifra a *Practical Typography*, soldi molto meglio spesi.