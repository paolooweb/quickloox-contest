---
title: "A passo di Mmorpg"
date: 2017-12-01
comments: true
tags: [Runescape]
---
Il sogno di poter lanciare [Runescape](https://www.runescape.com/) su iPad è per me di lunga data e finora dai risultati [molto meno appaganti](https://www.runescape.com/info/mobile) delle aspettative.<!--more-->

Eppure qualcosa si muove. Non me sono accorto finora, ma ci sono piani chiari per l’uscita entro il 2018 di *app* iOS dedicate, apparentemente capaci di trattare tutto il gioco e non solo parte di esso, come più o meno è accaduto per altri giochi di grande successo (Runescape è gratis ma duecentocinquantotto milioni di account sono ugualmente tanti).

Si muove solo lentamente, come è costume in questo settore. Far giocare tutti e tutti insieme, in modo gradevole, è difficile.