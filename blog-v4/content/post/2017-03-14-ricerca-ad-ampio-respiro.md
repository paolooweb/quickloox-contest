---
title: "Ricerca ad ampio respiro"
date: 2017-03-14
comments: true
tags: [ResearchKit]
---
L’ospedale americano Mount Sinai [ha pubblicato i risultati](http://www.nature.com/nbt/journal/vaop/ncurrent/full/nbt.3826.html) di uno studio sull’asma condotto tramite iPhone e [ResearchKit](/http://www.apple.com/it/researchkit/).<!--more-->

Che [sono affidabili](https://www.appleworld.today/blog/2017/3/13/study-apples-researchkit-platform-generates-reliable-data-in-asthma-study). Lo studio ha trovato vari fattori che limitano l’efficacia di programmi come questo, ma la piattaforma software è appena nata, le app sviluppate allo scopo sono il primo tentativo e l’efficienza del tutto può solo migliorare.

Uno dei problemi presenti è che la popolazione con iPhone può non essere rappresentativa di quella totale, dal momento che la grande maggioranza dei telefoni in giro è Android e il profilo dei compratori di iPhone è diverso dal profilo medio.

Nonostante (anche) questo, c’è progresso e lo studio compiuto su iPhone andrà a beneficio di tanti malati. Un’altra ragione per scegliere iPhone e farlo pure respirando a pieni polmoni. Aria più pulita.