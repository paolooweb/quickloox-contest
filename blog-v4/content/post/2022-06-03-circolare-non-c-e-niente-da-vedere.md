---
title: "Circolare, non c’è niente da vedere"
date: 2022-06-03T00:54:16+01:00
draft: false
toc: false
comments: true
categories: [Hardware, Web]
tags: [iPad, Il Fatto Quotidiano, Mazzini-Modugno, Bari, OrizzonteScuola]
---
L’editoria italiana è discesa verso livelli di qualità poco edificanti e la capacità della popolazione di discernere non è che si senta benissimo. Vediamo una dimostrazione pratica.

Un quotidiano italiano trova uno *scoop* interessante: [A scuola per forza con l’iPad, la dirigente della scuola spiega: “non banalizzare, va incontro a didattica utile per gli alunni”](https://www.ilfattoquotidiano.it/2022/06/02/a-scuola-per-forza-con-lipad-la-dirigente-della-scuola-spiega-non-banalizzare-va-incontro-a-didattica-utile-per-gli-alunni/6613041/). Dentro l’articolo si riferisce testualmente:

>A stranire molti genitori pronti a mandare i loro figli in questa scuola, tuttavia, è stata una circolare consegnata loro nei giorni scorsi in cui si dice che “Chi non è munito di iPad di ultima generazione (o non intende farlo) è invitato a “proseguire il suo percorso di studi presso altra istituzione scolastica”.

Un quotidiano di una volta avrebbe postato un PDF o una schermata della circolare, visto che ci sono virgolettati. Qui invece ci dobbiamo fidare.

Da giornalista di una volta, verifico la fonte possibile: il sito dell’[Istituto comprensivo Mazzini-Modugno](http://www.icmazzinimodugno.edu.it/) di Bari.

Trovo [tre incontri programmati a fine maggio](http://www.icmazzinimodugno.edu.it/images/sito_documenti/locandine/Locandina_eventi.pdf) per i genitori della terza primaria e la prima secondaria dell’anno 2022-2023, su un documento che reca il marchio della scuola, quello dell’[Associazione nazionale dirigenti pubblici e alte professionalità della scuola - Struttura regionale della Puglia](http://www.anppuglia.it) e quello di [Compulab](https://www.compulab.biz), Authorized Service Provider certificato da Apple che certamente sarà il fornitore delle macchine.

Qui comincia a comporsi il quadro. Ai genitori sarà stato detto durante gli incontri che iPad è un requisito didattico per la scuola.

Non vedo circolari con il testo citato dal quotidiano, però. Un altro documento inviato ai genitori li invita a [fornire un Consenso sui servizi digitali entro il 6 giugno](http://www.icmazzinimodugno.edu.it/attachments/article/1401/Didattica_digitale_innovativa_e_sicurezza%E2%80%9D_per_a.s._2022-2023.pdf) e sul sito non sembrano trovarsi ulteriori indizi.

Sono stati veramente invitati, i genitori, a cambiare scuola se non gli va bene l’uso di iPad? Sembra strano, perché sono genitori di terza primaria e prima secondaria; salvo eccezioni provenienti da altri istituti, i ragazzi interessati frequentano la scuola almeno da due anni, se non cinque. Non si sono mai accorti, non sono mai stati informati, nessuno ha un fratello o sorella maggiore che sia già passato dall’esperienza?

(Tutto questo non frena Orizzontescuola.it dal [prendere le parole del quotidiano come oro colato](https://www.orizzontescuola.it/chi-non-ha-lipad-non-puo-seguire-le-lezioni-e-quindi-e-invitato-a-cambiare-scuola-la-circolare-che-ha-fatto-infuriare-i-genitori-la-preside-scelta-utile-per-la-didattica/), intanto).

Sulla home page del sito compare intanto una [videointervista alla preside](https://www.youtube.com/watch?v=rEUhTnMg6-Y), nella quale si afferma che il requisito di iPad è presente sul modulo di iscrizione alla scuola (compilato sei mesi fa, per come funziona l’iscrizione alla scuola dell’obbligo) e che alcuni genitori *hanno storto il naso* di fronte all’indicazione precisa dello hardware da usare.

Anche il quotidiano se lo chiede: *perché in una scuola deve regnare solo la Apple?*

Domanda eccellente, che porrei volentieri ribaltata su Office, al novantanove percento della scuola italiana. Strano che il quotidiano si accorga di queste situazioni solo se riguardano Apple, vero?

La scuola avrebbe comunque una risposta, erogata tramite il video:

>In questi sette anni abbiamo verificato come il lato educational dell’iPad è in effetti quello che risponde meglio ai bisogni educative degli alunni e alle esigenze didattiche dei docenti.

La scuola Mazzini-Modugno ha un *curriculum studiorum* digitale dal 2015/2016. Sono sette anni che ci lavorano e può essere che abbiano capito qualcosa. Più che porsi domande generiche sul regnare di Apple, bisognerebbe mettere alla prova le parole della preside. Speriamo che qualcuno lo faccia, sarebbe giornalismo vero. Mentre il quotidiano riesce anche a sbagliare il prezzo minimo di acquisto di iPad, ovviamente per eccesso, pure trascurando le possibilità di comprare macchine a prezzo *education*.

Ho scritto ad Apple chiedendo se abbiano investito sulla Mazzini-Modugno per portare avanti qualche sperimentazione o comunque le abbiano riservato particolari attenzioni, o se invece si tratti semplicemente di una scuola capace di andare oltre il conformismo e la massificazione. Vediamo se rispondono.

In ogni caso, della circolare non trovo traccia. Le iscrizioni accettate da una scuola sono vincolanti per la scuola, che non può certo dire il giorno dopo di andarsene altrove se non piacciono le regole. L’impressione è che sia stato aggiunto un po’ di falso a un po’ di vero per rendere la notizia più pepata e rastrellare qualche clic in più. Se non è vero, attendo volentieri una copia della circolare.

A ognuno le proprie conclusioni. La mia è che, se esiste una scuola su diecimila dove si usa iPad, al contrario delle altre novemilaeccetera, si tratta di una eccezione da sostenere e approvare, come qualsiasi voce si opponga al pensiero monolitico e all’omologazione collettiva in modo preparato, consapevole e magari pure positivo per tanti bambini che crescono.

<iframe width="560" height="315" src="https://www.youtube.com/embed/rEUhTnMg6-Y" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>