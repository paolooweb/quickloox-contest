---
title: "Si tagghi chi può"
date: 2021-05-16T14:17:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [Pages, Finder, Recenti, tag, cartelle smart] 
---
Mi sono ritrovato a farlo, non l’ho deciso. È iniziato anni fa, quando macOS ha introdotto la finestra Recenti. Quando aprivo una nuova finestra, vedevo quella.

Apri oggi, apri domani, mi sono reso conto che nove volte su dieci, o forse anche più di così, la vista standard della finestra mi mostrava i file che mi servivano.

Allargare appena la finestra mi avvicinava al cento percento. Poi Spotlight trova i file facilmente. Ho cominciato a capire che una organizzazione in cartelle mi faceva solo perdere tempo.

Le eccezioni ci sono sempre, chiaro. Però, una volta applicata la cura ai salvataggi, penso di averle eliminate.

Quando salvo, oltre a dare un nome esplicativo (cosa che ho sempre fatto), inserisco anche i tag.

A questo punto, se mi serve una cartella per avere una visione dei file di un progetto, la cartella la faccio smart: tra nomi file, date, tag, tipi di file, poche regole e la cartella si riempie da sola. Non solo, si aggiorna da sola.

Non basta? Molti programmi, per esempio BBEdit, hanno una vista a progetto: una sola finestra dove nella barra laterale si raccolgono tutti i file necessari e, di volta in volta, si visualizza quello che serve.

Aggiungo che su iPhone e iPad, dove un filesystem è rudimentale a dire poco, l’organizzazione gerarchica è persino impossibile. Le cose funzionano grazie alla ricerca e alle app.

Anni dopo, non saprei neanche considerare l’alternativa. E il tempo perso a salvare con l’aggiunta dei tag?

Eh. Anni fa, quando ho iniziato a rendermi conto, si faceva il Salva paranoico. Per non perdere dati, per il backup, perché non sai mai.

Oggi, 2021, quando salvi? Praticamente tutti i programmi lo fanno in automatico. Quelli che non lo fanno – ancora BBEdit – tengono nota internamente del lavoro fatto, anche se non è salvato. Può saltare la corrente e, quando riaccendo il computer, i file sono tutti lì, non importa se siano stati salvati. Non c’è neanche bisogno che abbiano un nome. L’attività di salvataggio, oggi è marginale. L’attività di backup è automatica.

Ecco perché, mi ci è voluto tempo ma ci sono arrivato, quando mi dicono delle migliorie al Finder tendo a stringermi nelle spalle. Vale sempre la pena, ma nella maggior parte dei casi per me il Finder è questione di visualizzare una finestra e aprire qualche file.

Il tempo aggiunto per scrivere qualche tag è ampiamente compensato da quello risparmiato a creare alberi di cartelle sempre più frondosi. Gli alberi li preferisco quando passeggio al parco; per lavorare, mi serve il file che mi serve, subito.

*Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile.*