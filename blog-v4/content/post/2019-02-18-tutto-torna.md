---
title: "Tutto torna"
date: 2019-02-18
comments: true
tags: [Gemini, Psion, Newton, eMate]
---
La musica è diventata digitale e per forza di cose nasce una nicchia di adepti del vinile, se non delle audiocassette.

I libri si digitalizzano e va da sé che si aprano spazi diversi, nuovi, per leggere sulla carta.

La fotografia, vogliamo parlarne? Evidente che ci sia spazio per nuove Polaroid, divertimento di pochi per pochi, non per snobismo né per distinguersi, semplicemente perché si è creato lo spazio per qualunque uso.

Niente di strano che qualcuno provi a farsi finanziare, riuscendoci e arrivando alla produzione effettiva, un [successore dei Personal Digital Assistant Psion](https://www.indiegogo.com/projects/gemini-pda-android-linux-keyboard-mobile-device--2#/).

Ho amato alla follia [quegli Psion](https://en.m.wikipedia.org/wiki/Psion_Series_5), una oasi di superiorità tecnologia e design a tratti geniale in anni che andavano verso la peggiore omologazione.

Arrivati al 2019, tuttavia, vedrei di investire in un [eMate 300](https://www.macworld.com/article/2020270/the-forgotten-emate-300-15-years-later.html). Meglio retrocomputing consapevole che revival senza sbocchi.

Con tutto il rispetto per i finanziatori, eh. C’è spazio per tutti, oggi, che ce lo siamo conquistato difendendolo con i denti quando di spazio per lo Psion originale non ce ne fu più.
