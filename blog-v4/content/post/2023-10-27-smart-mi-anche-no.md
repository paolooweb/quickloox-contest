---
title: "Smart Mi anche no"
date: 2023-10-27T16:26:59+01:00
draft: false
toc: false
comments: true
categories: [Hardware]
tags: [Mi Smart Clock, Xiaomi, Google Home]
---
Una rosa avrebbe lo stesso dolce profumo anche se si chiamasse in modo diverso, scrisse il Bardo.

Guai invece a cercare di configurare una Mi Smart Clock [in dotazione a un parente](https://macintelligence.org/posts/2022-06-04-una-sveglia-indiscreta/) a partire da Google Home, se sveglia e iPhone parlano lingue diverse.

Compare un errore numerico -83902 insormontabile che non lascia proseguire l’installazione né offre soluzioni.

Se la sveglia è in italiano, Google Home e l’iPhone che lo ospita deve essere in italiano e così via. Probabilmente ha a che vedere con il fatto che la sveglia, in teoria, si governa con Google Assistant e immagino non funzionerà proprio con tutte le lingue.

Poi il clock va bene, è carino, si fa benvolere. È la parte smart a lasciare perplessi.