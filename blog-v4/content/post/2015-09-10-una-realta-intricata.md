---
title: "Una realtà intricata"
date: 2015-09-10
comments: true
tags: [BlueStacks, Android, Mac, Galaxy, S5, iOS, Google, Play, iPhone, Wi-Fi]
---
Dopo [averlo segnalato](https://macintelligence.org/posts/2015-07-03-tanti-programmi-tanto-onere/), ho provato [BlueStacks](http://www.BlueStacks.com), il programma per avere il mondo Android su Mac.<!--more-->

Complimenti ai realizzatori, perché l’impresa non deve essere stata facile. Se ho capito bene, lo strato software Android installato da BlueStacks è quello di un Galaxy S5 Samsung. C’è ancora molto lavoro da fare perché i *crash* ci sono e le prime due volte che ho avviato il programma, questi si è bloccato a metà strada. La velocità a volte scricchiola, l’audio pure, però la soluzione c’è; deve crescere.

Alla fine ci si arriva. Mi ha colpito invece quanto sia intricato il mondo Android. Forse come quello iOS; solo che iOS fa tutto il possibile per nasconderlo e lasciare l’utilizzatore a lanciare le sue *app*, che al resto pensa il sistema.

Qui invece, tra Google Play Services, Google Play Games e aggiornamenti vari, l’esperienza di provare un gioco in versione Android viene posposta di minuti e sembra di usare un computer. Non sono pratico di computer da tasca Android e spero vivamente che sia tutto dovuto alle necessità interne di BlueStacks, per simpatia verso chi ha preferito un Galaxy a un iPhone. Diciamo che, come cavallo di Troia, BlueStacks non ispira l’abbandono di iOS.

L’altra cosa che colpisce è l’atteggiamento decisamente aggressivo dei servizi Google verso la privacy. L’ansia che hanno di sapere la carta di credito, la *password* del Wi-Fi è palpabile. Sono dati che ovviamente transitano anche su iOS, ma è il modo in cui viene presentata la situazione. Android ti alita sul collo.

È un mondo difficile e intricato. Più su quel lato.