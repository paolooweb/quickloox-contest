---
title: "Desertica a distanza"
date: 2020-08-13
comments: true
tags: [Burning, Man, Covid-19, Bicocca]
---
Decine di migliaia di persone si trasferiscono a fine agosto per nove giorni nel deserto del Nevada a Black Rock e formano una città temporanea che ospita performance artistiche, spettacoli, feste in modo autogestito. Niente sponsor, marchi, organizzazioni: ognuno porta sé e il proprio vissuto, oltre ad acqua, viveri e riparo, perché con il deserto non si scherza.

La manifestazione si chiama [Burning Man](https://burningman.org) e quest’anno, per cause che si intuiscono, avverrà in forma digitale, come è accaduto per molti altri eventi a cominciare da [Wwdc](https://developer.apple.com/wwdc20/).

Digitalizzare Burning Man [è un’impresa pazzesca](https://techcrunch.com/2020/08/12/digitizing-burning-man/), come racconta _TechCrunch_. L’evento è l’ultimo avamposto delle controculture da West Coast, stile New Age o hippy, e la gente si sposta in un ambiente estremo proprio alla ricerca della massima libertà espressiva fuori dai vincoli consueti.

Burning Man fa anche soldi. Il biglietto standard quest’anno sarebbe costato quattrocentosettantacinque dollari; l’anno scorso hanno partecipato settantottomila persone.

Creare un’esperienza di rete ugualmente coinvolgente e, se non ugualmente profittevole, almeno capace di sostenere l’organizzazione, non passa dal ricreare Black Rock in realtà virtuale. La cosa più ovvia e più banale, fare finta di non essere in rete, non può funzionare.

Invece è stato deciso di creare una serie di app che complessivamente riportano l’esperienza complessiva del _burner_ durante i nove giorni di happening. C’è realtà virtuale ma anche chat con navigazione di mappe bidimensionali, teatro immersivo, feste a tema lunare e altro ancora, raccontato da TechCrunch con dovizia di particolari.

Gli obiettivi non sono cercare di rifare in rete quello che si sarebbe fatto nel deserto ma offrire esperienze capaci di ricreare emozioni e sensazioni, nonché ampliare la platea dei partecipanti; biglietto a parte, vivere nove giorni nel deserto ha un costo e richiede capacità fuori dalla media delle persone. I _burner_ nell’animo, che non possono permettersi il viaggio in Nevada per ragioni economiche e/o psicofisiche ma condividono lo spirito di Burning Man al punto di spendere qualche decina di dollari per partecipare a una edizione online, potrebbero essere molti, in tutto il mondo.

L’edizione 2020 di WWDC è stata un enorme successo di partecipazione, per avere consentito a chiunque di avere accesso alle risorse e alla conoscenze di Apple, che prima richiedevano il viaggio a San José, millecinquecento dollari di biglietto, vincere la lotteria degli accrediti.

L’edizione 2020 di Burning Man potrebbe esserlo oppure no; in ogni caso, l’evento si trasformerà online in qualcosa di molto diverso dalla tendopoli sotto il sole di Black Rock.

Qualsiasi cosa facciamo, online è diversa da offline.

Ci pensavo a proposito dello studio condotto dall’università di Milano Bicocca su [come i genitori degli studenti hanno recepito la didattica a distanza](https://www.unimib.it/comunicati/didattica-distanza-65-cento-delle-mamme-lavoratrici-non-ritiene-conciliabile-lavoro) nei mesi di _lockdown_.

Inconciliabile con il lavoro, hanno detto i due terzi. Un terzo considererebbe di abbandonare il lavoro se dovesse ripresentarsi.

A parte che mescolare il parere di genitori di bimbi alle primarie con quelli di chi aveva in casa maturandi, boh.

Ma pare proprio che parlassero della didattica a distanza. Quella vera, curata per raggiungere l’obiettivo, elaborata da docenti competenti, capace di valorizzare il canale digitale.

Non di lezioni tradizionali filmate, il contrario – o il peggio – della didattica a distanza.

Potrebbero portare il loro sapere ai responsabili di Burning Man e semplificargli in compito, no?