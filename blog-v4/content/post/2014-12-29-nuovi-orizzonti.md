---
title: "Nuovi orizzonti"
date: 2014-12-31
comments: true
tags: [ChallengePost, Cristoforetti, Tøndering, Calendar, Gerst, Iss, Glif, iPad, iPhone, iOS8, Capodanno, festa]
---
Anni fa aspettavo il passaggio al nuovo millennio con sguardo quasi mistico. A distanza dall’evento, è stato un momento come gli altri.<!--more-->

Ciò non toglie che la capacità di conteggiare il tempo e di festeggiare, ossia interrompere straordinariamente l’abitudine quotidiana, distingua l’uomo da qualsiasi animale e sia uno dei contrassegni della civiltà. Guardare l’orologio e il calendario che completano un ciclo merita un attimo di riflessione e un pensiero al nuovo ciclo in arrivo.

Che bello se fosse per tutti un anno di nuovi orizzonti, da contemplare se non raggiungere o addirittura realizzare (rendere reali).

Il mio cenone avrà tre portate principali. La prima, [The Calendar FAQ](http://www.tondering.dk/claus/cal/astronomy.php) di Claus Tøndering. È la risorsa più chiara e completa esistente sui calendari, le ricorrenze, la misurazione del tempo e come si è modificato nella storia. Oltre a contenere una quantità smisurata di nozioni interessanti, trabocca di curiosità. Ci sono persone morte prima di essere nate, mesi durati dieci giorni e tra mille modifiche e metodi di conteggio qualcosa che nessuno è mai riuscito a modificare: la scansione settimanale. Conoscere davvero il tempo che scorre: bell’orizzonte. Se non altro, perché tutti festeggiarono il 2000 e ben pochi il terzo millennio, iniziato a Capodanno 2001.

La seconda portata è il filmato a passo uno dell’astronauta Alexander Gerst, che ha legato 12.500 fotografie scattate dalla Stazione Spaziale Internazionale in un [filmato di sei minuti e mezzo](https://www.youtube.com/watch?v=lNwWOul4i9Y). Se c’è un nuovo orizzonte che ha bisogno di essere riscoperto è quello spaziale, l’unico che può portarci avanti nel senso del progresso. Checché ne pensino i nati ieri che hanno scoperto lo spazio grazie alla bravissima [Samantha Cristoforetti](http://avamposto42.esa.int/futura/samanthacristoforetti/), abbiamo da vincere sfide poste già cinquant’anni fa. Avere un apparecchio con iOS 8 e la funzione di passo uno (*time-lapse*) dentro la fotocamera, e non avere un treppiede, vuol dire che abbiamo lasciato una cosa a metà. Da [Glif](http://www.studioneat.com/products/glif) in poi ce ne sono millemila, anche [per iPad](https://www.ishotmounts.com). Questo è un proposito del nuovo anno molto più valido che quello finto di dimagrire.

Per finire, qualcosa di più leggero e curioso, [ChallengePost](http://challengepost.com): una comunità di programmatori che sfidano e si sfidano a colpi di Html, costruendo le applicazioni più utili, divertenti, improbabili, spettacolari, ingegnose, esoteriche, perché-non-ci-ho-pensato-prima, da far girare su un computer, un orologio, una lavatrice, qualsiasi cosa con dentro un processore. Nel futuro che ci aspetta, *tutto* conterrà un processore.

Non facciamolo aspettare oltre. Auguri.

<iframe width="560" height="315" src="//www.youtube.com/embed/lNwWOul4i9Y" frameborder="0" allowfullscreen></iframe>