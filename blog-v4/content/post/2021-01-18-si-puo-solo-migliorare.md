---
title: "Si può solo migliorare"
date: 2021-01-18T01:43:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [Mac, M1, Twitter] 
---
Visto su Twitter, solo che non riesco più a trovarlo. In compenso, ricordarlo è semplicissimo: *i Mac M1 sono i migliori fatti da Apple finora. E sono i peggiori Mac M1 che vedremo*.

Se appena appena le aspettative saranno corrisposte, le prossime generazioni di Mac potrebbero veramente restare nella storia per avere cambiato con decisione le regole del gioco.