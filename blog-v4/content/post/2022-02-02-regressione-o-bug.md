---
title: "Regressione o bug"
date: 2022-02-02T00:48:42+01:00
draft: false
toc: false
comments: true
categories:
tags:
---
Ho trascinato senza pensarci l’icona proxy (quella prima del nome file nella barra del titolo della finestra di un documento) di un testo scritto con Pages dentro nella finestra di Messaggi, per mandare il file in questione.

L’icona del documento è entrata nella finestra di Messaggi con un nome generico, perdendo il suo.

Ho riprovato usando l’icona del file dentro il Finder che, trascinata su Messaggi, ha funzionato normalmente.

Qualcuno riscontra lo stesso comportamento su sistemi anteriori a Monterey aggiornato? Vorrei capire se si tratta di una regressione stupida o di un bug esistente da tempo.

I bachi peggiori, su un Mac, sono quelli che spezzano la coerenza dell’esperienza di utilizzo, non importa quanto impattino concretamente sulle operazioni (il file avrebbe anche potuto partire con un nome file generico).
