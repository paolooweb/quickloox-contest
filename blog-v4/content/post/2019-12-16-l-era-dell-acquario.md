---
title: "L'era dell'acquario"
date: 2019-12-16
comments: true
tags: [Schiller, iPad, Jobs, Mossberg, Cook, NYT, Times]
---
Il *New York Times* ha dedicato uno dei suoi notevoli speciali web al [decennio in cui la tecnologia ha perso l’orientamento](https://www.nytimes.com/interactive/2019/12/15/technology/decade-in-tech.html) e, oltre a non mantenere le sue promesse, ha preso in vari casi direzioni inquietanti, sulla privacy dei navigatori così come sulle tecnologie di sorveglianza indiscriminata.

C’è molto da leggere e in particolare pezzi come le rievocazioni di Phil Schiller e Walt Mossberg sulla nascita di iPad, o Regis McKenna che ricorda come cambiò il confronto con Apple  quando Tim Cook divenne amministratore delegato al posto di Steve Jobs. Schiller racconta di come in Apple si chiesero che computer avrebbe potuto costare meno di cinquecento dollari, essere di qualità e offrire un’esperienza gradevole, per rispondersi che avrebbero dovuto *tagliare aggressivamente*.

La nascita e il successo di iPad si devono alla volontà e capacità di eliminare il non necessario, invece che decidere di mettere più funzioni possibile a scapito della qualità.

Ma divago. Voglio dire un’altra cosa: il *Times* ha sprecato una grande occasione per spiegare il *motivo* che ha trasformato la tecnologia, da panacea per il progresso, in una strega da cacciare.

Semplice: per anni, per decenni, chi se ne occupava, chi ci lavorava, chi la comprava, era una piccola frazione del totale. La tecnologia era una boccia di vetro nell’oceano.

Quando lo spazio sembrava poco e si sentiva la necessità di crescere, era facile: bastava una boccia un po’ più grande. Appena fuori da quella piccola c’erano nuovi compratori, nuovi spazi.

Non funziona più perché la boccia si è allargata fino a comprendere la totalità delle acque disponibili. Tutti hanno un computer in tasca, i programmi disponibili sono talmente tanti che il loro numero non conta, da domani o dopo avremo talmente tanti indirizzi IP che il loro possesso varrà poco o nulla, dopo che per mezzo secolo sono stati una risorsa da tesaurizzare e usare con giudizio. Aree vergini non ne esistono più e comunque si guadagna meglio a fornire servizi migliori a chi già è cliente che sfruttare giacimenti di consumatori cinesi, indiani, africani che al più daranno qualche punto percentuale extra nei bilanci e non altro, grane escluse.

Come si cresce? Nessuno di chi c’è stato dentro fino a oggi era abituato alla nuova risposta: *a spese di un altro come te*. Concorrenza. Strategia. Efficienza. Quello che nell’alimentare sanno da secoli, nell’automobile da decenni.

Nella tecnologia nessuno lo sapeva, erano tutti abituati a pescare giusto un po’ più lontano. Nessuno era stato abituato a parità di condizioni competitive, rispetto delle regole comuni, interoperabilità. Neanche al rispetto delle leggi, dato che il potere per metà trascurava il settore, per metà non lo capiva. Sicurezza? Via, per qualche hacker. Non si vedeva la possibilità di colpire il programma nucleare di uno stato o intraprendere un programma di sorveglianza capace di brutalizzare la libertà di un miliardo di persone.

Cambierà, nel tempo, con fatica e con perdite. Il punto è che non si è perso l’orientamento. Non c’era mai stato. Arriveremo all’era dell’Acquario agognata dagli hippy, con pazienza, quando saremo usciti dall’era dell’acquario dove un’azienda di tecnologia poteva più o meno permettersi tutto e ignorare chi aveva davanti, vicino, sopra o sotto.