---
title: "Fatica e soddisfazione"
date: 2013-10-17
comments: true
tags: [OSX]
---
Una volta l’anno deve essere permesso incensarsi e siamo a metà ottobre.<!--more-->

Sono arrivato in fondo, a parte qualche *marginalia*, al lavoro per la pubblicazione dei libri su OS X Mavericks che firmo insieme a Luca Accomazzi alias Misterakko (il lavoro vero naturalmente è il suo, io aggiungo i refusi e invento qua e là). Ora è certo che i due ebook appariranno in contemporanea all’annuncio del sistema operativo. E a tempo debito uno dei due, quello destinato ai *power user*, avrà anche la versione cartacea.

Anche la sola aggiunta dei refusi è stata una fatica consistente. Dà soddisfazione, però.

Del primo libro – la solita, ma mai scontata, *Guida all’uso* – si trova traccia in preordine ovunque: [Google Play](https://play.google.com/store/books/details?id=ybZIAQAAQBAJ&rdid=book-ybZIAQAAQBAJ&rdot=1&source=gbs_atb), [Kobo](http://www.inmondadori.it/OS-X-109-Mavericks-Luca-Accomazzi-Lucio-Bragagnolo/eai978885031658/), [Amazon](http://www.amazon.it/dp/B00FRR0P6A/ref=cm_sw_r_udp_awd_UjYvsb0Y9X1R5), [iBookstore](https://itunes.apple.com/it/book/os-x-10.9-mavericks/id723829180?l=en&mt=11).

La scheda più esaustiva è quella [su Apogeo](http://www.apogeonline.com/libri/9788850316588/scheda), che è l’editore.

Akko ha scritto una [pagina riassuntiva complementare](http://www.accomazzi.net/TlibriI120.html).

Ora uno può pensare che io alle sei di mattina mi metta a pubblicare link di preordine per farmi pubblicità e ne ha tutto il diritto. Semplicemente mi sento sollevato e soddisfatto e questo è un modo per dirselo.

Padroni di non credermi e va bene così. Intanto, oggi si fa colazione a mezzogiorno e poi si festeggia.