---
title: "L’orizzonte dell’evento"
date: 2023-03-29T16:32:09+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [Utility ColorSync, ColorSync Utility, Preview, Anteprima]
---
In più occasioni avevo già menzionato il [memory Leak](https://macintelligence.org/posts/2022-11-12-uffa.-però/) di Utility ColorSync, che da qualche versione di macOS impedisce di lavorare artigianalmente sui parametri di codifica dei documenti PDF.

In pratica in MacOS sussiste l’alternativa perfetta alla balena Acrobat. Meglio, sussisterebbe, dato che al momento attuale il programma soffre di un problema serio di allocazione della memoria e nel giro di pochi minuti rende il sistema inutilizzabile, con la memoria applicativa riempita fino all’orlo quando va bene e il disco pieno di RAM inutilmente patinata quando va male. Un buco nero che inghiotte qualsiasi buona intenzione.

Poco fa usavo Anteprima e ho avuto l’intuizione: chiamare dal programma uno dei filtri che avevo preparato a mano in Utility ColorSync. Un equivalente di avvicinarsi all’orizzonte degli eventi, la linea che separa il nostro universo dal buco nero, per approfittare della sua energia senza essere risucchiato.

Ahimé, non funziona. Chiamare il filtro da Anteprima scatena il *memory leak* e restano solo misure drastiche, se si vuole continuare a lavorare.

Tutto ciò vale fino a Ventura 13.2, sempre sperando che un *engineer* Apple con la vocazione eroica trovi il momento per chiudere il buco nero e restituirci in un futuro possibile un universo gravitazionalmente sereno. Per quello che riguarda i PDF, mica altro.