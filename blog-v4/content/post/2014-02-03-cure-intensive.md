---
title: "Cure intensive"
date: 2014-02-03
comments: true
tags: [Windows]
---
Domenica da conoscenti.

Il driver sul CD allegato alla stampante non funziona e la stampante non stampa. Il masterizzatore scarta due Dvd su tre (o è libero da difetti un terzo dei Dvd, a scelta).<!--more-->

Il portatile ha il disco diviso in due partizioni, piene zeppe, e il sistema è ingolfato. All’avvio parte automaticamente una scansione del registro che trova tipo tremila errori piccoli e grandi e si offre di ripararne gratis quindici, per gli altri bisogna acquistare il programma. Una *utility* gratuita individua un numero simile di errori e, chiesta la riparazione, non succede assolutamente nulla.

Nel frattempo si decide di unificare le due partizioni. Il programma deputato si rifiuta di eseguire a causa di problemi gravi sul disco. Con gli strumenti di sistema passa una mezz’ora a sistemarli, dopo di che il programma di prima esegue la fusione delle partizioni e dichiara successo. Mentendo.

Il sito di scacchi *non funziona*. Viene proposta l’installazione di Chrome come *browser* preimpostato in alternativa a quello canonico. Il sito funziona.

Indovinare sistema operativo e *browser*.