---
title: "L’antenato illustre"
date: 2021-02-05T01:24:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [Newton, MessagePad, Mac, M1, Apple, Arm, PowerBook Duo, iPad] 
---
I Mac M1 hanno già fatto il botto e ne faranno presto altri. È utile ricordare che la tecnologia sottostante è la stessa del 1993, quando uscì Newton MessagePad.

Su Twitter è apparso un bel racconto di un entusiasta che [affrontò un viaggio della speranza](https://twitter.com/jimmyg/status/1334202649688891395) pur di avere una delle primissime unità in vendita. C’era affollamento, ma sicuramente non le code che poi abbiamo visto con iPhone.

L’esperienza di Newton è stata incredibile e una delle mie migliori decisioni rimane la vendita del PowerBook Duo per usare come portatile un MessagePad 2100 appena uscito di produzione. Durò molti anni, l’ho in casa, si accende e la batteria ancora regge.

Su Apple si è scritto tutto? No, mancherebbe una storia seria e documentata dei reimpieghi della tecnologia. Il riconoscimento della scrittura a mano che sta arrivando sugli iPad è pronipote del riconoscimento usato su Newton. I processori sono Arm oggi come erano Arm ieri.

Probabilmente l’entusiasmo di quegli anni fu diverso. Oggi entusiasmarsi richiede più ragionamento, più saldezza mentale, nel momento in cui tutti parlano di tutto e la realtà viene masticata infinite volte al secondo da miliardi di persone annoiate e quasi sempre fuori luogo.

Però esiste, è solo più difficile che traspaia. Provare un M1 in un Apple Store può dare brividi analoghi a quelli di spacchettare un MessagePad 2100 e scoprire un sistema operativo diverso da tutti gli altri.

La tecnologia dentro quell’oggetto troppo avanti per i suoi tempi anima oggi una comunità quattro ordini di grandezza superiore. Ventisette anni fa. Complimenti, antenato MessagePad.

<blockquote class="twitter-tweet"><p lang="en" dir="ltr">More than two billion people regularly use smartphones with ARM processors. This revolution began nearly 30 years ago with the first ARM-powered handheld computer. Here’s my story of what it was like to be there on Day 1 when the first Apple Newton PDA went on sale. [thread] <a href="https://t.co/18NLt0eNi7">pic.twitter.com/18NLt0eNi7</a></p>&mdash; Jimmy Grewal (@jimmyg) <a href="https://twitter.com/jimmyg/status/1334202649688891395?ref_src=twsrc%5Etfw">December 2, 2020</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

*Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile.*