---
title: "Il buco per conto terzi"
date: 2014-03-20
comments: true
tags: [EA, Apple, iTunes, phishing]
---
Per capire dove sta il valore, si possono studiare i movimenti dei criminali. Che per esempio [hanno violato un server di Electronic Games](http://blog.econocom.com/it/blog/cloud-big-data-crowdfunding-per-leducation/) (EA).<!--more-->

Dei giochi di EA, tuttavia, gli interessa zero. Avere violato il server ha consentito loro di mettere in piedi un sito di *phishing* fatto per acquisire ID Apple assieme a tutti i dati anagrafici e di carta di credito.

Se una vittima ci casca, apre completamente il proprio account iTunes e poi viene spedito su una pagina vera del vero sito Apple, così da fargli pensare che tutto sia stato regolare.

Possiamo tranquillamente aspettarci, da qui a qualche mese, un articolo-inchiesta-scandalo su quanto sia scarsa la sicurezza di iTunes, dato che qualcuno si lamenterà di avere subito un furto di identità.