---
title: "Cimiteri a confronto"
date: 2022-03-28T01:31:49+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [Apple, Microsoft, Cyberdog, Microsoft Network, MSN]
---
La comparazione è sicuramente sleale e parziale, perché lato Apple non fatico a individuare elementi da aggiungere alla tabella.

Però è interessante verificare sull’altra sponda, dove la classificazione è più puntuale, come sia andata finora. Ricordiamo, sarebbe l’azienda professionale, che nessuno è mai stato licenziato per avere scelto, che non sbaglia un colpo.

[Progetti uccisi da Apple](https://www.killedbyapple.nl) contro [progetti uccisi da Microsoft](https://killedbymicrosoft.info).

Ripeto: la pagina Apple è *evidentemente* incompleta. Nessun tentativo di dare credibilità al confronto così com’è. Comunque, sulle due pagine si può lavorare. Per esempio, aggiungo di mio [Cyberdog](https://apple.fandom.com/wiki/Cyberdog), che oggi non è neanche immediato da rintracciare perché corrisponde a un marchio di abbigliamento, e [Microsoft Network](https://en.wikipedia.org/wiki/MSN).
