---
title: "Otto colpi da maestro"
date: 2015-04-25
comments: true
tags: [web, Apple, Flickr]
---
Lucas Hinch, 37 anni, è stato arrestato dalla polizia di Colorado Springs per avere violato la legge esplodendo otto proiettili con la propria [pistola Hi-Point da nove millimetri](http://www.thesmokinggun.com/buster/colorado/colorado-computer-killer-786520), appena acquistata sul sito di annunci [Craigslist](http://milan.craigslist.it).<!--more-->

La vittima è un portatile [Xps 410](http://en.wikipedia.org/wiki/Dell_XPS) prodotto da Dell nel 2012.

Il [rapporto della polizia](http://www.springsgov.com/units/police/policeblotter.asp) (identificativo 21312) dichiara che *Hinch era stufo di litigare da mesi con il computer*.

[Intervistato dal *Los Angeles Times*](http://www.latimes.com/nation/nationnow/la-na-nn-colorado-shooting-computer-20150421-story.html), il colpevole del computericidio ha rilasciato commenti che non lasciano dubbi:

>È stato glorioso. Gli angeli hanno cantato. […] È stato estremamente frustrante. Avevo raggiunto la massa critica. […] È stato premeditato, oh, assolutamente. Sono stato attento che non ci fosse niente dietro e non ci fosse rischio di proiettili vaganti.

Hinch probabilmente se la caverà con una multa. Mentre, come spiega [*The Gazette*](http://gazette.com/man-shoots-computer-in-colorado-springs-alley-gets-revenge-he-wanted-and-a-citation/article/1550042), *il computer non riuscirà a farcela*.

Possiamo ricordarlo per il suo valore: molto più di qualche grammo di piombo. Almeno otto volte tanto.