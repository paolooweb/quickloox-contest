---
title: "Il generale Contatta"
date: 2017-09-05
comments: true
tags: [Mediolanum]
---
Quando si dice interfaccia amichevole.

 ![Messaggio di errore nella app di Banca Mediolanum](/images/general-contatta.jpg  "Messaggio di errore nella app di Banca Mediolanum") 

Nel complesso Mediolanum funziona bene e migliora nel corso del tempo. Ogni dieci passi avanti ne fanno uno indietro. Capita a tutti, eh. Però manca qualcosina in fase di test.