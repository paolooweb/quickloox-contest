---
title: "Infatti infetti"
date: 2014-10-15
comments: true
tags: [iWorm, Mac]
---
Gira un *malware* per Mac chiamato iWorm che avrebbe [contagiato diciassettemila computer](http://news.drweb.com/show/?i=5976&lng=en&c=14).<!--more-->

Si sprecano i commenti sul falso senso di sicurezza di chi possiede Mac, Windows e OS X sono uguali, più antivirus per tutti e così via.

Intanto *The Safe Mac* [individua la fonte dell’infezione](http://www.thesafemac.com/iworm-method-of-infection-found/): software commerciale distribuito illegalmente su PirateBay e farcito con la sorpresina.

Diciassettemila mentecatti, più che un problema del sistema operativo (peraltro già aggiornato contro la minaccia), che concedono a software rubato la propria password di amministratori. E qualcuno la tratta come una notizia.