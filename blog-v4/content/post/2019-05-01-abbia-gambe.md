---
title: "Abbia gambe"
date: 2019-05-01
comments: true
tags: [Apple, MacStories, iPhone, iPad, Mac]
---
Apple ha avuto una trimestrale in linea con le (sue) previsioni, in leggero calo sullo stesso periodo dello scorso anno. La vendita degli iPhone è diminuita, quella degli iPad è cresciuta e parliamo dell’hardware che ha registrato le variazioni maggiori (Mac è in calo leggero, da fluttuazione). I servizi sono cresciuti di molto invece e non hanno mai contato così tanto.

Che cosa vuol dire in una manciata di righe?

* Apple non è più quella di una volta, quando vendeva solo Mac e sarebbe stata a rischio se non ne avesse venduti. Oggi vende meno iPhone ma vende più iPad e compensa. Quello che era un business su una gamba sola, ora ne ha quattro o cinque.
* Apple non è più neanche quella della volta dopo, quando sembrava che vendesse solo iPhone e sarebbe stata a rischio se non ne avesse venduti. Certamente quel segmento dell’attività è il più consistente e redditizio, ma c’è anche molto altro.

Ancora qualche giro e saremo a parlare del successo di Apple Watch, che all’arrivo pareva un giochetto per impallinati.

Sia Apple Watch che Mac raggiungono ancora un grande pubblico di *first-timer* che si avvicinano ad Apple, appunto, per la prima volta.

Apple Pay è sulla via di portare a casa dieci miliardi di dollari l’anno. Pazzesco.

La base installata attiva di Apple è a 1,4 miliardo di macchine. Teniamo presente che la metrica di Apple nel valutare l’andamento dell’attività è molto probabilmente non un dato come il fatturato o i profitti globali, ma quanto frutta ogni apparecchio in attività. Il numero degli apparecchi in attività è record assoluto.

Attendo con ansia il primo che arriverà a dire *ecco, pensano solo ai servizi e trascurano iPhone*.

La migliore copertura dei risultati è [su MacStories](https://www.macstories.net/news/apple-q2-2019-results-58-billion-revenue/), dove hanno meritoriamente raccolto anche una selezione di tweet che è questi più significativa e interessante dell’articolo. Proprio bravi.
