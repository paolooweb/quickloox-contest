---
title: "Le devastazioni del 2022"
date: 2020-12-26
comments: true
tags: [M1, ad, Arm, Mac]
---
Tutti adesso parlano di 2021. Io penso che sarà un anno – informaticamente parlando – da ignorare o quasi.

Lato hardware, arriveranno altri Mac con processori Arm dopo M1. Ma lo sappiamo già.

Lato software, arriverà il [controllo dei tracciamenti pubblicitari da parte dell’utilizzatore](https://developer.apple.com/app-store/user-privacy-and-data-use/). Anche questo lo sappiamo già. *MacRumors* segnala che nelle beta del prossimo aggiornamento di iOS [inizia ad apparire la finestra di dialogo più rivoluzionaria di questo secolo](https://www.macrumors.com/2020/12/23/ios-14-4-beta-app-tracking-prompt/).

 ![Vuoi concedere a questa app la possibilità di tracciarti in giro per Internet allo scopo di mandarti pubblicità su misura?](/images/tracking.jpg  "Vuoi concedere a questa app la possibilità di tracciarti in giro per Internet allo scopo di mandarti pubblicità su misura?") 

Il 2021 passerà abbastanza noiosamente nel dispiegamento di queste novità. Non succederà niente di speciale, informaticamente parlando.

Il 2022 invece porterà le conseguenze di quello che sarà successo l’anno prossimo.

Sarà una cosa devastante, informaticamente parlando. Non per Apple, eh.