---
title: "Il Mac della batteria"
date: 2021-08-30T23:30:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [Charlie Watts, Mario, Rolling Stones, Start Me Up, Mac, J. Geils Band, Guardian, Stewart Copeland, Max Weinberg, Microsoft, Torino] 
---
Mi sarebbe piaciuto commemorare degnamente Charlie Watts con un suo collegamento al mondo Apple, solo che non avevo appigli per farlo mentre era vivo e neanche adesso ne ho trovati. I Rolling Stones [si sono semmai fatti pagare una bella cifra da Microsoft](https://www.aaaa.org/timeline-event/start-3-million-anthem-launched-microsofts-windows-95/) per i diritti di utilizzo del brano [Start Me Up](https://www.youtube.com/watch?v=SGyOaCXr8Lw).

Tuttavia [Mario](https://multifinder.wordpress.com) ha scritto una cosa illuminante sul [mio canale Slack](https://app.slack.com/client/T03TMRQRC/C03TMRQU0) (gli inviti sono sempre aperti, basta una email) e gli ho chiesto il permesso di riprodurla:

>Il suo modo di suonare era già “Apple” vent’anni prima del primo Macintosh. Toglieva tutto quello che non era essenziale e quello che rimaneva “just worked”. A differenza di tanti altri, suonava muovendo solo polsi e braccia (poco), evitando tutti i movimenti inutili. Il primo esempio che mi viene in mente è “Start me up”.

Mario ha anche aggiunto questo:

>Da batterista dilettante, lo ammiravo molto. Avrebbe potuto fare il virtuoso, ma aveva capito che agli Stones i virtuosismi non servivano. E anche questa era una forma di virtuosismo.

Quando Mario dice *dilettante* intende *so più di quello che voglio farti credere*, ma capisco che qualcuno potrebbe nutrire dubbi sulla sua autorevolezza. Per questo invito alla lettura del [ricordo di Watts](https://www.theguardian.com/music/2021/aug/26/stewart-copeland-max-weinberg-on-charlie-watts-rolling-stones) offerto dal *Guardian* a cura di due batteristi molto noti, Stewart Copeland e Max Weinberg. Corroboreranno.

Ho cominciato a seguire la musica degli Stones dal concerto di Torino del 1982, mentre prima seguivo più che altro il loro clamore mediatico. Prima degli Stones si esibiva credo la [J. Geils Band](https://www.scaruffi.com/vol3/jgeils.html); ero un ragazzino ingenuo e rimasi impressionato dalla batteria del gruppo, che occupava un bilocale.

Finita la performance introduttiva, venne smontata la batteria e posizionata quella degli Stones. Tre tamburi, due piatti, pareva il set di uno che ha iniziato ieri a studiare.

E invece era il Mac della batteria a usarla, in modo magistrale. Dopo tanti anni Mario mi ha aiutato a capirlo e Charlie Watts resterà per sempre un riferimento.

<iframe width="560" height="315" src="https://www.youtube.com/embed/SGyOaCXr8Lw" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

*Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile.*