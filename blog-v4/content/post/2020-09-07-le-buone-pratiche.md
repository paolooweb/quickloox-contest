---
title: "Le buone pratiche"
date: 2020-09-07
comments: true
tags: [Ruby, on, Rails, Microsoft, Mac, Visual, Studio, Emacs, vim, TextMate, Atom, RubyMine, Sublime, Text]
---
Secondo [il sondaggio 2020 della comunità mondiale degli sviluppatori su Ruby On Rails](https://rails-hosting.com/2020/), un terzo degli oltre duemila che hanno risposto usa come editor Visual Studio di Microsoft, che ha la maggioranza relativa. Per fare un governo servirebbero però alleanze, visto come ci sia spazio nelle risposte per Vim, Sublime Text, RubyMine, Atom, Emacs, TextMate e altri.

In compenso, quasi tre su quattro usano macOS. Settantatré percento, per essere esatti. A usare Ruby on Rails su Windows è il *tre percento*.

Si può pensare quello che si vuole, degli sviluppatori. Comunque sia, è
gente che si guadagna da vivere attraverso il proprio lavoro e conosce
bene hardware e software. Se usano un certo software o un certo
hardware, lo fanno in genere a ragion veduta e perché è la cosa migliore
per le loro necessità.