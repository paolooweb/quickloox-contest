---
title: "Nemici per sempre"
date: 2014-02-18
comments: true
tags: [Telecom, D’Itri, Sparkle, Cogent, Sip, ]
---
Una volta non si chiamava Telecom, ma Sip: Società Idroelettrica Piemontese, incaricata dal re se ricordo bene. Poi si occupò dei telefoni, ignoro su incarico di chi, e da allora ha lavorato senza sosta per l’arretratezza e l’inefficienza delle telecomunicazioni italiane, curando alla rovescia ogni aspetto, dalla tariffazione alla tecnologia di chiamata.<!--more-->

Ogni tanto qualcuno mi dice che sono passati anni, le cose sono cambiate, non bisogna trascinarsi il passato come una pietra. Sono d’accordo al cento percento e d’altronde, se ho un difetto, è che con tutto il presente disponibile faccio fatica ad attaccarmi al passato.

Il [*post* di Marco D’Itri](http://blog.bofh.it/id_443) risale al passato, otto giorni fa per l’esattezza. Ricorda che negli anni novanta accadeva di frequente come per passare da una rete italiana a un’altra rete italiana via Internet toccasse passare dagli Stati Uniti.

Sta accadendo di nuovo. Parte del traffico su Cogent Communications (uno dei grandi *provider* Internet globali) diretto ai clienti di Telecom Italia Sparkle passa da New York invece che passare da Parigi o Francoforte.

>La spiegazione fornita da Cogent ai propri clienti è che questo dipende da Telecom Italia, che ha imposto che il traffico per alcune proprie reti non gli sia più inviato in Europa perché queste interconnessioni sono sature.

Cogent, racconta D’Itri, riferisce come stia collaborando con Telecom per risolvere la situazione solo che, sfortunatamente, *Telecom non ha ancora deciso come procedere*.

>La mia impressione quindi è che Telecom Italia stia di nuovo ritardando l'upgrade di alcune delle proprie interconnessioni, con l'effetto di spingere il traffico su percorsi non ottimali.

Oltretutto si mandano inutilmente a passare per gli Stati Uniti dati diretti verso l’Italia all’indomani dello scandalo intercettazioni.

Questa è Telecom oggi. Personalmente ho fatto di tutto per doverci avere a che fare il meno possibile, premesso che siamo in Italia e la rete è disgraziatamente loro. Anche se mi offrono qualche euro in meno, anche se adesso sorridono e mi assicurano che è tutto cambiato. Niente è cambiato. La Sip del XXI secolo, in peggio. Non sono fornitori di un servizio, ma nemici.