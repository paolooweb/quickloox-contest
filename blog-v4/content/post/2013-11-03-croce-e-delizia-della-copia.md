---
title: "Croce e delizia della copia"
date: 2013-11-03
comments: true
tags: [OSX, Mavericks, Macity, WD]
---
Su Macity hanno avuto il coraggio di [scrivere](http://www.macitynet.it/os-x-10-9-mavericks-emergono-gravi-problemi-i-dischi-fissi-esterni/2/)

>Pe il momento è consigliabile evitare di collegare qualsiasi disco fisso esterno ai Mac che funzionano con OS X 10.9 Mavericks.

(Sì, *però*).<!--more-->

È una stupidaggine siderale. Significa quasi sempre rinunciare al proprio *backup* Time Machine. Se qualcuno ama buttare il proprio tempo a leggere cretinate, almeno ignori questa.

È vero invece che Mavericks [ha problemi con dischi Western Digital](http://community.wd.com/t5/News-Announcements/External-Drives-for-Mac-Experiencing-Data-Loss-with-Maverick-OS/td-p/613777). Se nel sistema è in funzione software Western Digital adibito alla gestione dei dischi, meglio spegnerlo – ora – e disinstallarlo, poi spegnere Mac e riaccendere. Potrebbe non bastare; al momento, non è sicuro usare dischi esterni Western Digital con Mavericks ed è totalmente sconsigliato usare le utility di gestione che arrivano insieme al disco (è *sempre* sconsigliato).

In agosto avevo giustappunto vissuto [problemi con il disco Western Digital addetto a Time Machine](https://macintelligence.org/posts/2013-08-05-se-manca-time-machine/), in uso con la *Developer Preview* di Mavericks, e avevo sospettato anche un problema al sistema operativo, in versione provvisoria. Adesso sappiamo che i programmatori Western Digital sono stati un po’ distratti.

Ogni altra cosa si legga su quell’articolo di *Macity* è frutto di una digestione difficile o del *jet lag* dell’autore.

Per quanto riguarda le altre segnalazioni nella pagina, in particolare quelle su Seagate e LaCie, segnalo che il mio nuovo disco Time Machine (LaCie, quindi Seagate) non perde un colpo e ha nessun problema. I dischi rigidi hanno una vita limitata e accade che un errore porti alla perdita dei dati. Succede a chiunque, ovunque, in questo stesso istante. Trasformare segnalazioni singole in un caso generale richiede fantasia e impreparazione.

Altra cosa: se sei professionista, sai che i tuoi dati sono al sicuro. Hai un piano nel caso fallisca il disco interno, nel caso fallisca il disco Time Machine, nel caso fallisca il disco che fa *backup* a Time Machine e in qualsiasi altro caso. Altrimenti non sei professionista, o i tuoi dati hanno zero valore.

Si devono valutare il valore dei dati e predisporre le copie adeguate, su disco, su Internet, su nastro, dove sembra opportuno. A salvare i dati digitali sono le copie, da sempre.

Sul web la legge è diversa. Davanti a periodi come

>Alcuni professionisti e utenti che conservano dati importanti di lavoro dichiarano che i più diffusi software di recupero dati risultano inutili e che saranno costretti a richiedere il recupero dati professionale che può arrivare a costare oltre 1.00 dollari o euro per intervento.

*Uno punto zero*. *Dollari o euro*. Si capisce come vivere copiando, senza capire e senza sapere, sia una disgrazia.