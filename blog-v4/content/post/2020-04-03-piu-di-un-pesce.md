---
title: "Più di un pesce"
date: 2020-04-03
comments: true
tags: [Swift, Mac, OS9, Mpw, Classic, Tsai, Throughton-Smith]
---
Poca enfasi quest’anno sui pesci d’aprile ed è facile capire perché. Nondimeno c’è chi ha voluto se non altro liberare la mente e si è cimentato nel progetto di [fare funzionare una applicazione Swift su Mac OS 9 Classic](https://belkadan.com/blog/2020/04/Swift-on-Mac-OS-9/).

Il resoconto è puntuale è incredibile. Salta fuori che [AIX](https://www.ibm.com/it-infrastructure/power/os/aix) condivideva più dell’immaginabile con Mac OS 9 e che esiste un [emulatore](https://github.com/ksherlock/mpw) di [Macintosh Programmer’s Workshop](https://macintoshgarden.org/apps/macintosh-programmers-workshop), oramai un grimorio da leggenda per chi impegnava un metro cubo di libreria con quei sei fascicoli di manualistica.

Non si riesce neanche a leggere con distacco perché viene voglia di menare le dita sulla tastiera. È che devo sistemare i problemi di DNS che impediscono la visione del blog su, ehm, Safari.

È anche che mica si tratta di un pesce, ma di una lezione di pesca d’altura.
