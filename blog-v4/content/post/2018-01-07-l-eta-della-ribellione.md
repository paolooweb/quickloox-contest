---
title: "L’età della ribellione"
date: 2018-01-07
comments: true
tags: [Safari, Chrome, Firefox]
---
Proprio un oggi di quindici anni fa nasceva [Safari](https://www.apple.com/it/safari/). L’età in cui si scoprono il mondo e anche la voglia di cambiarlo, nella quale le posizioni costituite e il *si è sempre fatto così* appaiono insopportabilmente datati.

Safari è un adolescente che i suoi sogni li ha realizzati. Ha demolito lo stato delle cose precedente e dato vita a una epoca di apertura e progresso continuo. Qualsiasi modifica viene apportata a Safari non deve compromettere la velocità di base del motore; che differenza da prima, quando le modifiche servivano a emarginare i browser diversi da quello orwelliano.

All’epoca nella quale capitava di essere esclusi da un sito perché non conformarsi al conformismo ne è succeduta un’altra, dove il primo interesse di un sito ben fatto è assicurare la migliore esperienza su qualsiasi schermo, con qualunque browser.

È Safari ad averne il merito, per avere un [motore open source](https://webkit.org/) e per avere attraverso iOS numeri sufficienti per guadagnarsi autorevolezza, senza alcun monopolio né abuso dello stesso. Safari non è mai stato il browser più diffuso ma continua a essere quello più influente, oggi in compagnia di Chrome che peraltro nasce da una costola di Safari. Firefox ė open source fino al midollo, ma non possiede la forza trainante di un miliardo di computer da tasca, i più utilizzati e versatili.

Grazie a Safari il web, almeno quanto a strumenti di navigazione, è un luogo molto migliore di quindici anni fa. Al contrario di tante ribellioni adolescenziali, questa ha *veramente* portato un mondo differente.
