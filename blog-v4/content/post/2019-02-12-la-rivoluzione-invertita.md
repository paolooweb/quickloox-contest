---
title: "La rivoluzione invertita"
date: 2019-02-12
comments: true
tags: [burocrazie, patente, passaporto]
---
L’Italia è una repubblica fondata sulla fotocopia.

Questa sarebbe la battuta. Il ragionamento appena più articolato nasce dal termine (incrocio le dita) delle mie recenti [peregrinazioni burocratiche](http://www.macintelligence.org/blog/2019/02/07/digitalia/); dopo un’ultima visita in Motorizzazione, dove ho fornito due moduli anagrafici uguali, sempre ovviamente compilato a mano, e un set completo di riproduzioni di codice fiscale-patente-carta di identità, che avevo già passato alla commissione medica, pare che nel giro di un mese mi spediranno la patente.

Il che coincide nella durata con i tempi attesi di consegna del mio nuovo passaporto.

La cosa che non torna è che l’uno e l’altro documento sono oramai elettronici. Perfino la solerte impegnata in Motorizzazione, dotata di una forma con svolazzo calligrafico che da solo vale la visita, per non parlare della soddisfazione con la quale cala  il timbro di stato sul documento acquisito, dicevo, perfino lei ha dovuto interfacciarmi con un qualche sistema online e perfino scandire il codice a barre delle ricevute dei versamenti in conto corrente.

Vent’anni fa la procedura allo sportello, più o meno, era uguale e il tempo richiesto pure. inesplicabilmente continuano a chiederti fotocopie che il loro ufficio evidentemente non è in grado di produrre, e neanche mettono nell’androne una fotocopiatrice a pagamento. Però sprecavi una mattina allora, la sprechi anche oggi.

Tuttavia il passaporto veniva stampato in modo tradizionale, compilato a mano, la foto graffettata dal questurino più abile nella manipolazione fine. Aspettare un mese è sempre stato ingiustificato, però si capiva che c’era una catena di lavoro.

Adesso non c’è più, la catena. Da stampatelli e inchiostro neri o neri bluastri, fotocopie, pecette e faldoni nasce un documento prodotto da una macchina. Eppure ci vuole un mese come prima.

Un altro miracolo dei funzionari al (vero) potere italiano, quello di mettere i bastoni tra le ruote a chiunque soprattutto se immeritevole: dopo avere burocratizzato l’informatica invece di informatizzare la burocrazia, sono riusciti a usare le tecnologie digitali per andare più lent≤≤ n\i di quando non erano disponibili.

Chi teme o auspica la rivoluzione in Italia, stia tranquillo: non accadrà. Anche il più agguerrito dei rivoltosi troverà tra sé e la stanza dei bottoni una invalicabile doppia o tripla firma, gli mancherà un contrassegno telematico, prenderà il numerino sbagliato per occupare i centri nevralgici della nazione.
