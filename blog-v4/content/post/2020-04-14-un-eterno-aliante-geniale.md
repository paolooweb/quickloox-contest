---
title: "Un eterno aliante geniale"
date: 2020-04-14
comments: true
tags: [Life, Conway, Doomsday, Macintosh, GitHub, Medium, Guardian, ArsTechnica, Json]
---
Ogni giorno consulto i miei canali Rss preferiti per scegliere il tema di questa pagina. Oggi non l’ho fatto, perché il primo link del *feed* su [NetNewsWire](https://ranchero.com/netnewswire/) – programma che per ora [mi ha convinto](https://macintelligence.org/posts/2020-03-11-confessioni-di-un-lettore-malandrino/) – bastava e avanzava per fermare tutto. [Se ne è andato John Conway](https://twitter.com/samwangphd/status/1249132655737790464).

<blockquote class="twitter-tweet"><p lang="en" dir="ltr">I am sorry to confirm the passing of my colleague John Conway. An incomparable mathematician, a pleasant neighbor, and an excellent coffee acquaintance. <br><br>His passing was sudden (fever started only Wednesday morning). Part of coronavirus&#39;s hard toll in New Jersey.</p>&mdash; Sam Wang (@SamWangPhD) <a href="https://twitter.com/SamWangPhD/status/1249132655737790464?ref_src=twsrc%5Etfw">April 12, 2020</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

Lo ricorda in modo rapido ed eccellente [Ars Technica](https://arstechnica.com/science/2020/04/john-conway-inventor-of-the-game-of-life-has-died-of-covid-19/) e il *Guardian* lo ha [ritratto in dettaglio](https://www.theguardian.com/science/2015/jul/23/john-horton-conway-the-most-charismatic-mathematician-in-the-world) pochi anni fa.

I matematici che hanno segnato il nostro *mondo* si contano a decine, ma quelli che fanno lo stesso sulla nostra *vita* sono ben pochi.

John Conway ha accompagnato la mia vita adulta e il mio lavoro. Il suo gioco [Life](https://www.conwaylife.com/wiki/Main_Page) era una scelta ovvia e sorprendente da mostrare su un computer all’alba dell’epoca dell’informatica personale. Farlo su un Macintosh era addirittura naturale, perché il suo schermo era bitmap: ogni singolo pixel era controllabile via software. Mentre oggi si tratta di una ridicola ovvietà, nel 1984 era una eresia pionieristica. Tramite l’intuizione di Conway si poteva cogliere quanto fosse avanti Macintosh, letteralmente, con una occhiata.

Ogni inizio anno memorizzo il Giorno del Giudizio: sapere in che giorno della settimana cade l’ultimo dì di febbraio permette di recuperare mentalmente lo stesso dato per qualsiasi giorno dell’anno, con poca fatica. Ci sono [più sistemi per ottenere questo risultato](https://www.scientificamerican.com/article/calendar-algorithm/), ma quello di Conway è il più brillante.

Il matematico britannico ha prodotto molto più di questo, naturalmente, e lascio il piacere della scoperta. Ma tendo a dimenticarmelo, se non lo rileggo; invece Life e il Doomsday Algorithm mi accompagneranno per tutta la vita. Anzi, prometto a Conway di imparare anche la parte più difficile, quella per trovare il giorno della settimana in anni diversi dall’attuale.

Voglio immaginarlo in volo su un aliante del suo Life, a procedere lentamente, inesorabilmente lungo una retta obliqua in uno spazio infinito. Qualcosa che deve essere abbastanza simile al luogo dove riposano le menti eterne dei grandi matematici.

Per praticare Life, c’è l’imbarazzo della scelta. Segnalo su Mac [Game of Life](https://apps.apple.com/it/app/game-of-life/id1377718068?l=en&mt=12) e il sempreverde [Golly](http://golly.sourceforge.net) (da vedere l’intestazione della pagina, prodotta proprio con Life). Su iOS si trova per esempio [Game of Life Cellular Automata](https://apps.apple.com/us/app/game-of-life-cellular-automata/id1026735601#). Esiste un Life persino per TV, [Glider](https://apps.apple.com/us/app/glider-conways-game-of-life/id1166014726).

Life è anche un pretesto intelligente per cimentarsi nella programmazione. GitHub ne offre [almeno un esempio](https://github.com/codyd51/Life) e su Medium si trova una [spiegazione passo per passo](https://medium.com/flawless-app-stories/creating-game-of-life-on-ios-73bd51b63430) di come implementare il gioco nel linguaggio Swift di Apple.

Chi non ha mai provato Life dovrebbe farlo una volta, un minuto: è deliziosamente semplice e misterioso, come tutte le grandi intuizioni. Sono sufficienti un browser e una pagina come [questa](https://bitstorm.org/gameoflife/), o [quest’altra](https://css-tricks.com/game-life/), piena di versioni diverse. A me piace molto [questa](https://copy.sh/life/).

Ho provato a infilare un Life qui sotto, ma restituisce un *errore Json* e va debuggato per farlo funzionare. Crea problemi di ogni tipo alla pagina e alla fine l’ho tolto. La sua casa è [qui](http://pmav.eu/stuff/javascript-game-of-life-v3.1.1/).

Buon viaggio, John Horton Conway.