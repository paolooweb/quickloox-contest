---
title: "Lo sventurato rispose"
date: 2014-09-26
comments: true
tags: [Continuity, iPhone, Mac, iOS8, Yosemite]
---
È suonato il telefono. *Su Mac*. Incredibile come anni di abitudini possano ottundere i riflessi di fronte alle novità che veramente cambiano le cose.<!--more-->

Ho guardato la notifica sullo schermo, con il suono della chiamata negli auricolari. E ho fatto per allungare la mano verso iPhone. Fermandomi, perché certo iPhone suonava, ma suonava Mac. Non era FaceTime. Non tornava.

Pur con minima esperienza di paternità, ho imparato una cosa: frequenti corsi, leggi libri, parli con personale specializzato, senti altri genitori con esperienza, ti documenti ovunque e poi, alla prima sollecitazione, dimentichi tutto.

Qui è successa la stessa cosa. Mac aggiornato a Yosemite, iPhone a iOS 8. [Continuity](https://macintelligence.org/posts/2014-06-03-continuity/). Ho ascoltato tutte le spiegazioni di Apple, ho letto fino alla nausea la documentazione, ho aspettato il momento e quando è arrivato, quando Mac ha preso in carico la chiamata su iPhone mediante il Wi-Fi, sono rimasto a metà strada di tutto, inebetito per tre secondi.

Poi era una promozione inutile sulla telefonia e facevo meglio a non rispondere. Però questa è l’informatica che cambia davvero la vita, anche se *vita* qui è un concetto ristretto a una suoneria e una scrivania.

Mezz’ora dopo è suonato il *cordless* e ho avuto un moto quasi di stizza. Una cosa talmente primitiva. La telefonata dovrebbe apparire su Mac. Quando cambia la vita, tutto sembra così ovvio.