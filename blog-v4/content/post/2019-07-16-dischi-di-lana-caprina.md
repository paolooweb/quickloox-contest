---
title: "Dischi di lana caprina"
date: 2019-07-16
comments: true
tags: [MacBook, Pro, Ssd]
---
Ma che sorpresa! I nuovi MacBook Air, più economici, [montano dischi più lenti](https://consomac.fr/news-10886-des-ssd-moins-rapides-dans-les-macbook-air-de-2019.html). Le unità Ssd da 256 gigabyte sono marginalmente più veloci nella scrittura dei dati rispetto a quelle del 2018 e, in compenso, perdono un terzo della velocità di lettura.

Non sono sicurissimo di avere letto valutazioni positive specifiche della velocità di scrittura dei dischi, quando sono usciti i MacBook Air 2018.

Ricordo invece almeno qualche confronto con l’inevitabile modello di Asus o Acer o Lenovo o la moda del momento, stessa capacità e ovviamente costo inferiore.

Confronto dove l’unica metrica considerata era la capacità. Per la serie *tutti i computer sono uguali e dentro sono fatti allo stesso modo*.

Presupposto che, alla luce della notizia, mi sembra di lana ancora più caprina del consueto.
