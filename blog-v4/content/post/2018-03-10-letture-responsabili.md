---
title: "Letture responsabili"
date: 2018-03-10
comments: true
tags: [Supplier, Apple]
---
Qualsiasi grande azienda di oggi spende un sacco di parole per il rispetto dei diritti dei lavoratori e del pianeta a suon di abuso della parola sostenibile.<!--more-->

Non so quante siano all’altezza degli sforzi di Apple come descritti nell’edizione 2018 del rapporto sulla [Supplier Responsibility](https://images.apple.com/supplier-responsibility/pdf/Apple_SR_2018_Progress_Report.pdf).

A tutto si può e bisogna fare la tara. Però leggi che nel 2017 tre milioni di lavoratori hanno beneficiato di iniziative di formazione rispetto ai loro diritti. Sono tanti, spesso in Paesi dove il progresso sociale e produttivo è, eufemismo, leggermente indietro.

Entro il 2020 un milione di lavoratrici avranno fruito di corsi sui diritti e sulla salute al lavoro. Scrivo mentre è appena terminata la festa della donna e penso che qui, in segno di gioiosa adesione alla causa, c’è stato lo sciopero dei treni. Ed è evidente al confronto, che si è persa la cognizione.

Apple ha mappato al 100 percento la catena di approvvigionamento delle materie prime, per comprare il più possibile dove non vi sia sfruttamento del lavoro, specie minorile, e le condizioni siano accettabili. Ci sono sanzioni per chi non rispetta le regole e programmi di rimborso ai lavoratori dove le cose non sono fatte a regola.

Sembra persino troppo bello per essere vero. Eppure questa è la dodicesima edizione del rapporto.

Sarebbe il caso di leggerla e andare anche oltre la superficie delle frasi che fanno marketing, per entrare nella sostanza di chi vive in Paesi poveri o arretrati, dove montare tastiere è davvero il primo passo verso una vita migliore.