---
title: "Tutto scorre"
date: 2017-12-25
comments: true
tags: [Natale, giochi]
---
Sarebbe il milleseicentosessanquattresimo *post* qui pubblicato, in accordo con **mimmo** che li ha fatti contare a [R](https://www.r-project.org/).

Auguro ai meritevoli di avere letto per tempo la guida di *Ars Technica* ai [migliori giochi da tavolo del 2017](https://arstechnica.com/gaming/2017/12/the-best-board-games-of-2017/) e che, terminato anche l’ammazzacaffè, ci sia da passare il resto della giornata in modo divertente. Altrimenti sotto con Amazon, si fa più che in tempo per i prossimi giorni.

Federico Viticci ha pubblicato la sua selezione delle [settantacinque migliori app iOS](https://www.macstories.net/stories/my-must-have-ios-apps-2017-edition/), che va letta con una mano sull’ID Apple per regalarsene una.

Sento lo spirito del tempo soffiare dentro i *live* di [Kraftwerk 3-D](https://www.youtube.com/watch?v=ZMrB45RKbnI), anche se si tratta di ultrasessantenni.

Potrei proseguire a lungo. Tutto continua a scorrere incessantemente.

Ma oggi è Natale. Buona festa a tutte le persone buone.

<iframe width="560" height="315" src="https://www.youtube.com/embed/ZMrB45RKbnI" frameborder="0" gesture="media" allow="encrypted-media" allowfullscreen></iframe>
