---
title: "La felicità nella semplicità"
date: 2017-09-27
comments: true
tags: [Markdown, Marked2, Terpstra]
---
Mi trovo in un picco di lavoro anche più intenso dei soliti e, ovunque hardware o software possano semplificare o risolvere una situazione con un risparmio di tempo, sono salvifici.<!--more-->

Il discorso dovrebbe essere ben più articolato di così e per esempio tirare in ballo la nuova versione di [Marked2](http://brettterpstra.com/2017/08/01/long-form-writing-with-marked-2-plus-2-dot-5-11-teaser/), ma sarò sintetico (anche perché non ho tempo): ho maneggiato una quantità notevole di testo sensibile scritto in [Markdown](https://daringfireball.net/projects/markdown/). Ed è stato tutto molto, ma molto più semplice.

Per quanto mi riguarda, più o meno scrivere in Markdown è come scrivere in Html perché negli anni ho straconfigurato BBEdit in modo che metta a portata di mano i tag utili.

Molti però potrebbero trovare Markdown più facile. Li esorto a impegnarsi; maneggiare testi Markdown semplifica e non porta la felicità. Comunque, aiuta.