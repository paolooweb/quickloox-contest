---
title: "Il fascino della meccanica"
date: 2018-03-12
comments: true
tags: [Matias, Laptop, Pro]
---
Ha esattamente tutto quello che vorrei… eccetto il layout italiano, accidenti.

[Laptop Pro](http://matias.ca/laptoppro/mac/) di Matias è la prima vera buona tastiera meccanica che vedo, pensata per portatili e soprattutto collegabile con Bluetooth. Ovvero, ideale anche per iPad.

Centosesssantanove dollari, viste le specifiche, sono giusti. È impensabile scrivere senza un layout italiano, per me. Qualcuno altro, magari più impostato verso la programmazione, avrà meno remore e gliela raccomando di cuore.