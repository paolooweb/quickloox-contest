---
title: "Air o non Air, questo è il problema"
date: 2016-09-06
comments: true
tags: [MacBook, Air, watch, iPhone]
---
È notorio che domani, 7 settembre, Apple [presenta](http://www.apple.com/apple-events/september-2016/) iPhone 7. Molto probabile che presenti anche watch 2. Del tutto improbabile che sia spazio ai nuovi Mac, a meno che il *keynote* superi le due ore oppure ci sia qualcosa di veramente eclatante.<!--more-->

Dico che di Mac (hardware) non si parlerà ed è quasi un peccato. Sappiamo che iPhone 7 sarà un prodotto curatissimo e watch 2 sarà la prima evoluzione importante. Pensare a che cos’era il primo iPhone e che cosa il secondo. Quello che non sappiamo è come si articolerà la gamma portatile non Pro ed è la questione più interessante dell’autunno 2016 parlando di Apple.

Sono infatti ancora in vendita i [MacBook Air](http://www.apple.com/it/macbook-air/), che non ricevono aggiornamenti da molto tempo eppure sono vendutissimi come soluzione a basso costo. E convivono con [MacBook](http://www.apple.com/it/macbook/), visibilmente un *concept* nuovo e una nuova linea; certo una scelta meno universale di MacBook Air, che soddisfa un sacco di tasche e di padroni delle suddette (sto [usando provvisoriamente un MacBook Air](https://macintelligence.org/posts/2016-05-29-in-groppa-al-muletto/) e non lo cambierei per un MacBook Pro come dico io, però si lavora tranquilli).

I MacBook Air sono computer eccezionali, come dimostra la loro longevità nonostante la mancanza di aggiornamento. D’altro canto, in questo momento l’offerta è confusa: capire che cosa sia meglio per me tra un Air piccolo, uno grande e un MacBook – senza contare le versioni personalizzate – è poco immediato. Il mancato aggiornamento vuol dire che gli Air stanno per sparire dagli scaffali o che tornano in grande spolvero? E se tornano aggiornati alla grande, non fanno concorrenza a MacBook? Il quale, a questo punto, sembrerebbe quasi un errore da dimenticare dopo averlo sperimentato.

Ci sono troppi portatili nella zona Air-MacBook, per una Apple. Lasciare il solo MacBook sarebbe una grande semplificazione radicale. Forse lo sarebbe troppo. E così via.

Se normalmente il toto-hardware è la cosa meno interessante del mondo, questa è una eccezione, perché la parabola di MacBook e MacBook Air dirà molto sullo spirito Apple di oggi. Se orientato alla semplificazione e alla linearità come siamo stati a lungo abituati, o se si andrà verso la diversificazione iperpersonalizzata propria di watch.