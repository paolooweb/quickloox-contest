---
title: "L’evoluzione del linguaggio"
date: 2019-06-11
comments: true
tags: [Swift, Facebook, Dice]
---
È colpa mia, che ancora leggo post e commenti su Facebook, dove mi tocca stare per lavoro e dove mi può capitare di sentire definire [Swift](https://swift.org) *linguaggio inutile ma trendy*, come una moda del momento.

Per fortuna riesco anche a trovare articoli come quello di Dice, che in tre parole spiega come nel 2019 Swift [abbia raggiunto alcuni traguardi importanti](https://insights.dice.com/2019/06/05/swift-5-wwdc-2019-apple-objective-c/) che lo rendono alternativa sempre più efficace a [Objective-C](https://developer.apple.com/library/archive/documentation/Cocoa/Conceptual/ProgrammingWithObjectiveC/Introduction/Introduction.html).

Ci sono voluti cinque anni per arrivare ad avere una velocità di compilazione superiore o equivalente a quella di Objective-C, per esempio. Se devi fare un linguaggio trendy, inventi un trucchetto, non spali concime per un lustro.

Si tratta di un lavoro di lungo termine, che deve ancora portare i suoi veri frutti. Objective-C [è stato scritto oltre trent’anni fa](https://en.wikipedia.org/wiki/Objective-C) e solo oggi, scrive Dice, è tempo di considerare seriamente il passaggio a Swift per uno sviluppatore.

(Il titolo è il pretesto per un tributo a Tom Wolfe, autore de [Il regno della parola](https://www.giunti.it/libri/narrativa/il-regno-della-parola/) e dedicato al linguaggio e come non trovi spazio nella teoria dell’evoluzione. Il linguaggio parlato, non di programmazione).