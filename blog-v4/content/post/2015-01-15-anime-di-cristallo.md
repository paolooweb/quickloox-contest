---
title: "Anime di cristallo"
date: 2015-01-15
comments: true
tags: [Avernum2, CrystalSouls, Spiderweb]
---
È appena uscito [Avernum 2: Crystal Souls](http://www.avernum.com/avernum2/index.html) e l’unico motivo per cui lo potrei eventualmente sconsigliare è che questa è la versione Mac; quella iPad arriva nel giro di qualche settimana.<!--more-->

Giocabilità eccellente, ricchezza di contenuti fuori dalla media, inventiva superiore fanno dei prodotti Spiderweb una bella dimostrazione che si può fare software eccellente e guadagnarci il pane anche senza sfornare l’inevitabile giochino da fermata del bus.

*Crystal Souls* non è banale e richiederà decine di ore di gioco per arrivare in fondo (per giunta con tre modi diversi per farlo). Chiaramente ci vuole amore per i giochi di ruolo con ambientazione fantasy vecchio stile, giocati a turni. D’altro canto, le specifiche di funzionamento sono tali che se oggi un Mac riesce ad accendersi, nel 99 percento dei casi è capace di fare funzionare il gioco.

Chi non si fida può scaricare la *demo*.