---
title: "Cani vecchi, trucchi vecchi"
date: 2014-10-06
comments: true
tags: [iMovieHD, Unix, Terminale, alias]
---
Recita il proverbio anglosassone *you can’t teach an old dog new tricks*, un cane vecchio non impara nuovi trucchi. Affermazione che contesto nel caso degli umani, detto di passaggio; è solo questione di volontà, là dove invece il cane ha dato quello che poteva. Eventualmente si approfondirà.

Invece, le *app*. Incappo accidentalmente in una finestra Applicazioni dove si vede il vecchio iMovie HD, che torna ancor utile per qualche lavoretto in pellicola, per fare il verso ai *lavori in pelle* di *Blade Runner*. L’icona su Yosemite mostra il segnale di divieto e il doppio clic non funziona più.

Clic destro sull’icona ad aprire la cartella *Contents* e da lì apertura della cartella *MacOS*.

Dentro c’è un eseguibile Unix di nome *iMovie HD*. Su quello il doppio clic funziona.

Per evitare di aprire ogni volta la cartella, ho usato il comando `alias` del Terminale:

`alias imoviehd='open /Applications/iMovie\ HD.app/Contents/MacOS/iMovie\ HD'`

Ora mi basta digitare *imoviehd* nel Terminale per lanciare iMovie HD, anche se l’icona dice no.

Tutta roba che si sa e probabilmente sono l’ultimo cane a riferirne.