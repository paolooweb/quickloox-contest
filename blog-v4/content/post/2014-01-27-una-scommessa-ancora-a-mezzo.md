---
title: "Una scommessa ancora a mezzo"
date: 2014-01-27
comments: true
tags: [Numbers, Pages, iWork, AppleScript]
---
Noto che molti si sono adirati per la [scomparsa di varie funzioni](http://www.theverge.com/2013/11/6/5073458/apple-responds-to-iwork-missing-feature-complaints-promises-updates) dalle nuove edizioni di iWork, mentre pochi si compiacciono degli aggiornamenti come [quello più recente](http://www.macrumors.com/2014/01/23/iwork-mac-ios-updates/), con varie funzioni che tornano e anche qualche novità interessante.<!--more-->

[Scrivevo](https://macintelligence.org/posts/2013-10-25-script-per-scommessa/) a fine ottobre:

>Se vinco la scommessa, in un qualche prossimo aggiornamento tornerà supporto AppleScript come si deve anche per Pages. Se perdo, si è perso qualcosa.

Non ho ancora vinto perché il supporto AppleScript su Pages è tuttora mancante. Però è tornato in grande stile, come e più di prima, per [Numbers](http://macosxautomation.com/applescript/iwork/numbers/). Non abbiamo ancora perso.