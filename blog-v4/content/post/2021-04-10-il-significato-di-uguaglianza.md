---
title: "Il significato di uguaglianza"
date: 2021-04-10T02:12:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [iPad, Edinburgh Learns for Life, Dad, Edimburgo] 
---
Sono ventisettemila macchine nuove più dodicimila *rinfrescate*, secondo la municipalità di Edimburgo, che [verranno consegnate da qui a fine 2022](https://www.edinburgh.gov.uk/news/article/13149/digital-boost-for-pupils-with-39-000-ipads-in-1-1-roll-out) a studenti tra i dieci e i diciotto anni e docenti, nell’ambito del programma *Edinburgh Learns for Life*.

Siccome le macchine senza una struttura serie di rete, nel 2021, sono uno spreco, assieme alla distribuzione degli iPad avverrà un potenziamento della connettività nelle scuole, in primo luogo wireless.

Analogamente, per i docenti verranno organizzati corsi di aggiornamento professionale, dato che un iPad è una macchina splendida per insegnare, per chi la padroneggia.

Mentre in Italia [i somari riempiono le piazze](https://macintelligence.org/posts/Analogici-con-il-digitale-degli-altri.html) contro le lezioni in digitale in quanto *aumentano le disuguaglianze*, in Scozia le disuguaglianze le eliminano.

Quando si deve stare tutti fermi perché qualcuno fa più fatica degli altri a muoversi, non è uguaglianza, ma truffa sulla pelle dei ragazzi e delle famiglie.

Uguaglianza è quando tutti vengono messi in grado di progredire insieme.

*Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile.*