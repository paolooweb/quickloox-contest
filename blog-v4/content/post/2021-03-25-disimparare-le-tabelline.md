---
title: "Disimparare le tabelline"
date: 2021-03-25T00:34:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [Excel, setteBit] 
---
[Software sbagliato non per quello che fa, ma per quello che consente di fare](https://macintelligence.org/posts/Cattive-attitudini.html). Ti dicono che Excel è il foglio di calcolo più completo, che è tanto comodo, che lo usano tutti.

È necessaria, una categoria *fogli di calcolo*? Ha qualche valore, essere il più completo dei fogli di calcolo?

Il problema è sempre quello: per uno che usa sacrosantamente Excel, dodici commettono nequizie inenarrabili. Ultima della serie la confusione delle vaccinazioni lombarde, pianificate – si fa per dire – mediante [la compilazione di fogli Excel non integrati nella piattaforma](https://twitter.com/setteBIT/status/1374813299280330761).

<blockquote class="twitter-tweet"><p lang="it" dir="ltr">L&#39;Italia è 1 Repubblica democratica (forse) fondata su… Excel: &quot;le agende con gli appuntamenti vengono tutt’oggi formate attraverso la compilazione di file excel non integrati nella piattaforma&quot; <a href="https://t.co/f7s4r3bQbh">https://t.co/f7s4r3bQbh</a> <a href="https://t.co/5d0D1ubqUz">pic.twitter.com/5d0D1ubqUz</a></p>&mdash; setteBIT (@setteBIT) <a href="https://twitter.com/setteBIT/status/1374813299280330761?ref_src=twsrc%5Etfw">March 24, 2021</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

C’è sicuramente del demoniaco nelle menti di sedicenti amministratori che hanno bisogno di un database, di un applicativo scritto in Python, di lunghi file di testo governati da script bash, di un’interfaccia verso la piattaforma messa a punto da Poste italiane (che sa tanto di brace dopo la padella), di un client verso un [data lake](https://aws.amazon.com/big-data/datalakes-and-analytics/what-is-a-data-lake/), forse di tutte queste cose o forse di nessuna, chissà. Non lo so, che cosa serva per fare funzionare l’agenda delle vaccinazioni.

C’è del demoniaco, ripeto, in questi cervelli che, di fronte a un’esigenza come questa, scelgono Excel.

Ma c’è anche Excel. Il frutto proibito dell’Albero della Scarsa Conoscenza. Perfetto per disegnare tabelline, colorare caselline, bruciare neuroni per presentare orgogliosi una macro capace di replicare perfettamente una funzione delle ottocentomila già incluse ma che nessuno conosce, ordinare un’anagrafica per nome di battesimo e sentirsi orgogliosi di avere trovato il comando di ordinamento, trastullarsi con i numeri nella falsa convinzione di avere battuto il proprio analfabetismo numerico con lo strumento prodigioso che, se selezioni una colonna di numeri, ti dà la somma da solo e ti libera dalla fastidiosa incombenza di accendere la calcolatrice da tavolo originale Ventesimo secolo.

Disimparare le tabelline, imparare i programmi, imparare a programmare; ecco la missione per un nuovo Umanesimo informatico. Ci vogliono missionari con pazienza infinita e ispirazione celeste, capaci di dire paciosi *per fare [la cosa da fare] Excel è la soluzione sbagliata* e attraversare indifferenti le ondate di riprovazione che ne seguono.

La cosa incoraggiante è che chiunque di noi può diventare disevangelizzatore di Excel. Anche senza sapere nulla di nulla. Basta fare presente, nella situazione, che usarlo è sbagliato. Nel novantasei virgola undici percento dei casi è comunque vero.

*Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile.*