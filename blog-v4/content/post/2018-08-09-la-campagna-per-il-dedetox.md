---
title: "La campagna per il dedetox"
date: 2018-08-09
comments: true
tags: [detox, Mac, iOS12, iOS]
---
Sono finalmente a fare lavorativamente niente per qualche giorno e mi capita di leggere un servizio sul *digital detox*. L’opportunità di approfittare delle vacanze come occasione per spegnere tutto, uscire dalla rete e disintossicarsi. Suppongo da una intossicazione.

Questa è gente che sta davvero male e il digitale non c’entra.

In questi pochi giorni scatto qualche foto di più alle figlie, per inviarle – le foto – ai nonni. Che sono felici. Nelle congiunzioni astrali favorevoli ci sta anche un FaceTime, sempre i nonni, sempre le figlie, sempre persone felici.

Via *browser* ho noleggiato un’auto pagandola meno, prenotato i biglietti per un parco a tema, scaricato orari di mezzi pubblici locali, visionato calendari di feste paesane, invitato amici, scoperto luoghi ideali per passare un pomeriggio di vacanza.

La messaggistica in generale è risultata indispensabile per tenere i contatti con le persone che contano maggiormente in questo periodo: parenti, amici, conoscenze che festeggiano un compleanno, vecchi compagni di scuola, contatti che magari una volta l’anno ma vanno mantenuti e così via.

La sera c’è qualche occasione in più di leggere, con impegno e senza, articoli e libri, blog e giornali. Su iPad più spesso che su Mac.

I social li uso pochissimo. Continuo a usarli pochissimo. Insomma, una normalità banale che fa quasi venire l’emicrania per eccesso di semplicità.

So che per qualcuno la disintossicazione riguarda proprio i social, ma mi trovo in questo caso nella parte di chi beve una birra al mese e sente dire quanto fa bene rinunciare all’alcool. Fa benissimo, come anche una birra con le persone giuste.

Ci sono anche quelli che consigliano di spegnere gli apparecchi, staccare Internet, dimenticare la posta. Quella di lavoro, certo. Ma il resto? Mi vedo isolato dal mondo, dagli affetti, dalle amicizie, dalle cose interessanti, dalle occasioni, dalle informazioni utili per la mia vacanza, dal piacere di vedere tua figlia correre nel prato e scattarle una foto per i nonni. Un deficiente (letterale, mancante di qualcosa) alienato (sempre letterale, lascio l’esercizio al lettore).

Gli apparecchi digitali e la rete, come del resto il basilico e il tosaerba, vanno usati bene con rispetto per la loro funzione e senso della misura. Spegnerli per guarire da una intossicazione presunta è un segno di uso scorretto e semmai la cura sarebbe un corso, più che un digiuno.

La funzione più imbecille di iOS 12 sarà il computo del tempo passato davanti allo schermo, che se sei intelligente non è un problema e dunque non ti interessa, mentre se sei un cretino (letterale, eh?) non basterà a fermarti. Mai visto qualcuno che smette di fumare perché vede messaggi e immagini *splatter* sui pacchetti di sigarette. Visti diversi che hanno smesso di fumare per mille altre motivazioni, molto più concrete e non sempre materiali o medicali.

La verità è che la campagna da fare è quella per il dedetox. Invece che astinenze più modaiole delle mode, uso brillante, creativo, positivo, amplificazione dell’intelligenza.

Ma anche fare del mare non guasta. Montagna, collina, città…
