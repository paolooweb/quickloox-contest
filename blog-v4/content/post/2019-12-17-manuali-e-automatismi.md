---
title: "Manuali e automatismi"
date: 2019-12-17
comments: true
tags: [emacs]
---
Propositi per il nuovo anno? Al momento ho solo uno di quelli vecchi: approfondire la lettura del manuale di [emacs](https://www.gnu.org/software/emacs/), che peraltro è *borderline* con il masochismo.

Eppure, nei giorni in cui c’è chi si preoccupa della [mancanza di coesione tra macOS e iOS](https://wormsandviruses.com/2019/12/catalyst-and-cohesion/#fn:1) nel momento in cui si permette agli sviluppatori di portare facilmente il software del secondo sopra il primo, trovo che la soluzione sia davvero padroneggiare di più e meglio quello che sta alla base del sistema.

C’è una pagina del 2006, [Personalizzare il sistema testuale in Cocoa](http://www.hcs.harvard.edu/~jrus/Site/cocoa-text.html), che è assai più corta e indolore del manuale di emacs, ma spiega una marea di cose in modo veloce e comprensibile, compresa la pletora di combinazioni di tasti non dichiarate ufficialmente ma presenti in macOS e nelle sue applicazioni.

Vengono, guarda caso, da emacs.

Torno al manuale. Per capire tanti automatismi che ancora non conosco.