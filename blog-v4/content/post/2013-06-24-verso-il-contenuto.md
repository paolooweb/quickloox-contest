---
title: "Verso il contenuto"
date: 2013-06-24
comments: true
tags: [iOS]
---
Facile sentire commenti di qualunque tipo nei confronti del *restyling* di iOS 7.

Andrebbero però modulati su una scala temporale adeguata, come [ha fatto Craig Hockenberry](http://furbo.org/2013/06/11/been-there-done-that/).<!--more-->

Se certi toni di colore paiono esagerati, va ricordato che Apple ha calcato la mano già con un’altra interfaccia grafica: Aqua, quella di OS X, che in dieci anni si è raffinata a diventare sempre più discreta e meno evidente.

La cosa che colpì molti ai tempi di Aqua fu l’idea di avere finestre senza contorno, esattamente come è successo con Lion quando le barre di scorrimento sono diventate opzionali.

Apple cammina verso un’interfaccia che dia il massimo spazio al contenuto e sia la meno evidente possibile.

E nel caso di iOS 7, proprio come con Aqua, passeranno anni prima che il lavoro di Jonathan Ive raggiunga la maturità.

Non è un caso che, osserva sempre Hockenberry, le *app* più attente a esaltare il contenuto come [Twitterrific](http://twitterrific.com/ios) o [Vesper](http://vesperapp.co/)

>si sentano da subito a casa.

Chi si è stracciato le vesti per quanto ha visto durante la presentazione di San Francisco in apertura del Wwdc, tenga presente che Ive ci ha lavorato per otto mesi. Pochi, pochissimi. Chi si è accorto che è mancato qualsiasi esempio relativo a iPad? L’interfaccia di iOS 7 verrà portata a compimento in fretta, ma non è lavoro che si improvvisa. Come non andrebbero improvvisati i voti in pagella.