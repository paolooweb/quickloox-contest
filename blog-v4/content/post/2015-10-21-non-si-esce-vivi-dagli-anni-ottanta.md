---
title: "Non si esce vivi dagli anni ottanta"
date: 2015-10-21
comments: true
tags: [Mac, iPad, MacBook, Air, Chromebook, FreeBsd]
---
Piccolo sfoghino dell’una di notte. Tra Facebook, Twitter, posta privata, *chat* e incontri casuali ho incontrato un nostalgico di HyperCard e ho letto che *Apple ha smesso da tempo di produrre portatili*, che una volta c’era più scelta e adesso si rimane obbligati tra Windows e Mac – come se non esistessero, per dire, un Chromebook OS o un Linux o un FreeBsd – e poi naturalmente Apple merda (tutto in maiuscolo), potevi spendere meno e avere una Gpu migliore e così via.<!--more-->

Sono io che frequento troppa gente che ha la mia età ma è rimasta intrappolata laggiù, o va proprio in questo modo? Sembra non ci si voglia accorgere che esistono un miliardo di apparecchi Apple in uso per il mondo (un miliardo) e che si vendono più Mac in un mese che in un anno dei fantastici ottanta.

Ho il mio (vecchio) MacBook Pro 17” qui davanti e, come *second screen*, un iPad oramai vecchio anche lui, che non perde un colpo ed è diventato il mio portatile di completa fiducia. Francamente, dal punto di vista hardware e software non ho mai lavorato così bene. Non ho mai avuto così tante opportunità di approcciare la tecnologia: si ha un’idea dei linguaggi di programmazione pronti da usare dentro un Mac appena tolto dalla scatola? Certo si può sempre migliorare: iWork su iCloud è appena uscito dalla fase beta ma Numbers si rifiuta di condividere un foglio di calcolo. Eppure, per chi segue Apple, trovo sia veramente difficile non considerare questo periodo il migliore per offerta, possibilità e anche prezzi, perché a mille euro ti porti via un MacBook Air per quanto piccolo. Negli anni ottanta, con due milioni al massimo potevi entrare in negozio a salutare il commesso tuo amico.

Uscite dal *bunker*. La guerra è finita.