---
title: "Meno spazi dentro"
date: 2023-06-10T01:30:05+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [Mimmo, Macintelligence, Quickloox]
---
Mentre annego [nelle sottigliezze delle stanze cinesi e soprattutto delle loro esegesi](https://macintelligence.org/posts/2023-06-09-la-complessità-cinese/), [Mimmo](https://muloblog.netlify.app) ha spostato una falange del mignolo e ora, dopo avere sistemato [la resa dei link negli abstract](https://macintelligence.org/posts/2023-06-08-meno-spazi-fuori/), ha ottenuto lo stesso risultato all’interno dei post.

In altre parole, non dovrebbero vedersi più spazi non necessari e grammaticalmente indesiderabili all’interno dei link.

Un altro piccolo passo, importante.

Se solo avessi il ringraziamento come superpotere assoluto.