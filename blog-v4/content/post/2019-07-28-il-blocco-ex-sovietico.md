---
title: "Il blocco ex sovietico"
date: 2019-07-28
comments: true
tags: [Microsoft, GitHub, Crimea, Winer, Scripting, Kashkin]
---
La vecchia Microsoft doveva propagare Windows a tutto l’universo conosciuto calpestando qualunque cosa si trovasse sul proprio cammino. Quella nuova, chiamamola nuova, ha bisogno di fagocitare l’universo dentro la propria infrastruttura cloud e lascia, diciamo, liberi di usare qualsiasi cosa, purché dietro ci sia la sua mano amorevole. Diciamo.

Il nuovo non meglio del vecchio, neppure marginalmente, ed ecco un primo indizio. Tra gli appetiti della bulimica Microsoft figurava GitHub, la comunità indipendente degli sviluppatori, che ora indipendente non è più. E se ti capita di stare in un Paese sottoposto a sanzioni dagli Stati Uniti, [GitHub ti taglia servizi essenziali](https://www.zdnet.com/article/github-starts-blocking-developers-in-countries-facing-us-trade-sanctions/).

Dave Winer, l’inventore del blog, [commenta meglio di me](http://scripting.com/2019/07/27.html#a183634):

>Ecco perché non può essere che Microsoft si occupi del serbatoio mondiale del codice aperto. Non è questione commerciale qui, ma di libertà di espressione. Inoltre potrebbero esserci sviluppatori di altre nazioni che lavorano in opposizione ai propri governi così che, tagliandoli fuori, gli Stati Uniti e Microsoft potrebbero di fatto opporsi a movimenti democratici.

Il caso citato da *ZDnet* non riguarda Iran o Corea del Nord, ma Crimea, giusto per dire. Non ci riguarda? Certo, non penso che nella vita vedrò mai l’Italia sanzionata dall’amministrazione statunitense. Ci riguarda per il fatto più generale che un governo accusato di nefandezze è diverso dalla persona che quel governo magari lo subisce. Mentre un governo illiberale e antidemocratico va combattuto, chi se lo trova addosso deve semmai essere aiutato.

Molti anni fa, secolo scorso, ho imposto le mie sanzioni a Microsoft e ho abbandonato tutto il suo software. Oggi è sempre più difficile mantenere un distacco totale: chi ti condivide un file su OneDrive, chi pretende di collaborare su Office365, ieri la figlia ha aperto con curiosità Minecraft sul vecchio iPad prima che mi venisse in mente di cancellarlo… più di tutto questo, chissà quanti siti che visito si appoggiano ad Azure senza che io lo sappia. Insomma, sfuggire completamente al leviatano è impossibile.

Però mi mantengo più pulito che posso.
