---
title: "Malelinguette"
date: 2018-03-07
comments: true
tags: [Verge, iPhone]
---
*The Verge* può essere difficilmente tacciato di partigianeria e allora, se titola [Stanno spuntando malelinguette su buoni telefoni Android](https://www.theverge.com/2018/3/4/17077458/iphone-design-clones-mwc-2018), vuol dire che qualcosa di concreto c’è. A partire dal sottotitolo: *La copia del design di iPhone effettuata con velocità e cinismo mai visti prima*.

Un paio di paragrafi, che dopo tutto stupiscono: la narrativa comune poco tempo fa era *fanno quello che fa iPhone e costano meno*. Ora, invece:

>Dieci anni che visito il Mobile World Congress e mai ho visto copiare  iPhone così spudoratamente e cinicamente. MWC 2018 passerà alla storia come la base di lancio per una massa di imitatori di iPhone, ciascuno assemblato con più fretta e approssimazione del precedente.

>Nessuno sforzo viene fatto per emulare il complesso sistema Face ID che risiede nella linguetta dell’apparecchio Apple; aziende come Noa e Ulefone hanno una tale fretta di fare uscire il loro sosia di iPhone che neanche hanno personalizzato il software in modo che si adatti alla nuova sagoma dello schermo. Più di un telefono con linguetta visto a MWC mostrava l‘orologio finire sotto l’angolo curvo dello schermo.

L’articolo procede chiamando in causa nomi di rango maggiore, come Asus e LG, a livello sempre basso. La cosa veramente incredibile è che la linguetta in cima allo schermo, per loro, è *estetica*: non funzionano come iPhone X, ma ne imitano l’aspetto esteriore.

Anche qui la narrativa comune fino a qualche tempo fa era *come è brutto iPhone X con la linguetta*, e fior di *concept* a mostrare come Apple avrebbe dovuto seguire l’esempio dei costruttori Android.

Copiare le funzioni, lo fanno tutti e lo fa pure Apple. Alla fine, scrive *The Verge*, è anche un sistema che permette al massimo numero di utenti di fruire dei progressi tecnologici e, nel bene e nel male, democratizza l’accesso.

Ma copiare l’aspetto, è povertà. Non nel senso della mancanza di risorse.
