---
title: "Un elenco di cursa"
date: 2023-04-10T01:34:07+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [Mac, cursori, puntatori]
---
Uno non ci pensa proprio, sono cose che passano largamente inosservate. Eppure, su Mac si mostrano un sacco di puntatori diversi.

La cosa interessante è che la [pagina ufficiale Apple](https://support.apple.com/en-gb/guide/mac-help/mh35695/mac) potrebbe avere bisogno di pagine indipendenti come [macOS Cursors](https://mac-cursors.netlify.app) per offrire un elenco veramente completo. O viceversa, dipende da come uno la vede.