---
title: "Obsolescenza sprogrammata"
date: 2014-06-04
comments: true
tags: [Yosemite, OSX]
---
Giusto perché [se ne parlava a maggio](https://macintelligence.org/posts/2014-04-30-obsolescenza-programmata/) dell’idea che gli apparecchi Apple siano fatti scientemente per invecchiare quando meno te l’aspetti e farti correre a comprare il nuovo.<!--more-->

I requisiti di sistema di Yosemite, OS X 10.10 annunciato l’altroieri, sono gli stessi di Mavericks, annunciato l’anno scorso, uguali a quelli di Mountain Lion, annunciato due anni fa.

Il mio MacBook Pro di inizio 2009 è più che abbondantemente compatibile e cinque anni iniziano a essere un’età rispettabile, per un portatile.