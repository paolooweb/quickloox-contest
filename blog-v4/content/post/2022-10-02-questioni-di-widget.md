---
title: "Questioni di widget"
date: 2022-10-02T02:26:48+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [iPadOS, iPad, widget, Mac OS X, Dashboard, Note, Messaggi, Catalina, Stage Manager, iPadOS 16]
---
Il mio [nuovo iPad](https://macintelligence.org/posts/2022-09-25-uelcom-tu-itali/) è partito con la parte superiore della prima schermata Home occupata da un numero consistente di widget e da qui ho cominciato a pensare come utilizzarli. Prima ero fermo al *se*.

Ho poche risposte e molto vaghe. Ci sono elementi di interfaccia concepiti con uno scopo preciso e privi di flessibilità in quanto tali. Esempio stupido, il Cestino o le finestre. Ci sono invece elementi di interfaccia che nascono perché si intravede un potenziale e da lì si evolvono per diventare qualcosa di importante, oppure svanire nel dimenticatoio.

Chiunque avrebbe detto fino all’anno scorso che [Dashboard](https://macintelligence.org/posts/2014-05-22-riscoprire-i-cazzilli/) era finito nel dimenticatoio [con l’arrivo di Catalina](https://www.theverge.com/2019/6/4/18652971/apple-macos-catalina-dashboard-widgets-removed-feature), dopo avere tentato di affermarsi su Mac OS X. Invece Apple non butta via mai niente, a dispetto delle apparenze, ed ecco di nuovo i widget alla ribalta, stavolta su iOS e iPadOS. Come usarli, però?

Parto dal fatto che su iPad ho numerose app in ordine più che sparso, tra interazioni mie, quelle filiali, i nuovi arrivi, le prove eccetera. Sono guarito molti anni fa dalla sindrome del pomeriggio passato a riordinare le icone come se fossero il servizio da tè nella cristalleria del soggiorno o la collezione dei fumetti da tenere in ordine cronologico e adotto una strategia che definisco evolutiva: per un po’ di tempo, prima di aprire una app, la individuo nella schermata in cui si trova e la sposto di una schermata verso sinistra, più vicina a Home.

In poco tempo le app che uso *veramente* si accumulano nella schermata Home o al massimo in quella precedente. Se mi fa comodo ne raggruppo alcune in cartelle e per il resto sono, sarei, a posto. Per lanciare una app poco usata, basta scrivere tre lettere in Spotlight ed eccola pronta.

Hanno un senso, allora, i widget, per un uso come il mio? Probabilmente sì. I widget più grandi (due per due, se li misuriamo in posti occupati nella schermata Home) si possono impilare e ho tenuto un widget grande che essenzialmente mi mostra cose che vedrei aprendo una app, senza aprire la app. Il tempo, i file recenti, le foto recenti, i promemoria.

Ho un altro widget separato, due per uno, che mostra calendario e appuntamenti. Lo uso rettangolare (molto widget possono avere dimensioni diverse) perché vedo calendario *e* appuntamenti. Lo tengo separato dal due per due perché così è sempre in vista.

Infine, per avere un’area di occupazione rettangolare nella Home, più equilibrata e gradevole all’occhio, ho due widget uno per uno: le Note, con la nota più recente in vista (quasi sempre è la prima che mi serve) e un Comando rapido per produrre automaticamente la data in questi post.

Mi piacerebbe tantissimo avere un widget Messaggi, solo che sembra assente.

C’è una cosa che mi lascia perplesso su tutto questo ed è il fatto che i widget appaiono solo nella schermata Home. Quindi sono molto adatti a ospitare informazione che tipicamente mi serve se sveglio iPad e voglio sapere subito qualcosa.

In iPadOS però c’è anche il Dock. Nella mia visione, su iPad il Dock serve ad avere disponibile una certa app in qualsiasi schermata io mi trovi, perché in qualsiasi momento potrebbe servire.

Fare una scelta tra informazione nei widget e app nel Dock, senza sovrapposizioni e sempre in vista della massima usabilità, è una cosa da male di testa.

Un esempio per tutti, le Note. Mi è effettivamente molto utile vedere subito la nota più recente. Ma molto spesso mi serve *la seconda* (o qualche altra) nota e per questo voglio le Note sempre nel Dock.

Senza dimenticare che, in caso di emergenza, ho la Quick Note: trascino dall’angolo dello schermo ed ecco una nota nuova di zecca per scrivere, subito.

Così ho tre usi diversi di Note, che desidero, e per averli finisco per mostrare le Note sia in un widget sia nel Dock, più la funzione Quick. Istintivamente sento che vorrei una sola app in una sola posizione, ma la ragione e l’uso quotidiano mi mostrano che risparmio tempo se faccio diversamente.

In attesa di iPadOS 16 con Stage Manager, nato per tenere assieme fino a quattro app durante il lavoro in multitasking su iPad, dove finirà per avere le Note nei widget, nel Dock e in Stage Manager.

Il dibattito sul multitasking in iPad è annoso e condotto da persone più competenti di me. Mi limito a sentire che preferirei avere un monotasking deciso, per quando serve, e un multitasking deciso, pure. Mentre invece si va verso l’aggiunta al sistema operativo di funzioni assolutamente utili in sé, ma che si sovrappongono e creano percorsi e flussi di lavoro confusi, mai univoci, sempre poco chiari quando si riprendono le cose in mano il giorno dopo.

Da Apple mi aspetto di più *un* sistema di multitasking e di informazione immediata, che tutti possono contestare ma di cui è innegabile il perfetto design, di una collezione di pezze sempre pensate per metà del tragitto.