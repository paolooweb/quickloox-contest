---
title: "Una foto della realtà"
date: 2017-12-07
comments: true
tags: [Flickr, iPhone, Canon, Nikon]
---
Fa notizia proprio perché non dovrebbe più farla: i dati di Flickr dicono che nel 2017 iPhone è stato [la fotocamera assolutamente più usata](https://blog.flickr.net/2017/12/07/top-devices-of-2017/).<!--more-->

iPhone occupa nove posti della *top ten* e il marchio Apple sta dietro il 54 percento delle foto; seguono Canon con il 23 percento e Nikon con il 18 percento.

*Ci sono più foto scattate con iPhone che con Canon e Nikon, messe insieme.*

Se accendo il televisore un momento, parte uno spot Huawei o Asus o Samsung che mostra ragazzi trendissimi intenti a scattare e condividere, condividere e scattare, condividere e condividere, scattare e scattare e scattare.

Sono immagini pubblicitarie, come quelle del Mulino Bianco o delle case dove casalinghe euforiche combattono i nemici del pulito. La realtà è diversa. Quelli che scattano e condividono hanno – spesso – un iPhone.