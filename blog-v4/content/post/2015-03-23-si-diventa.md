---
title: "Si diventa"
date: 2015-03-24
comments: true
tags: [Jobs, Isaacson]
---
Ho il sospetto che il vero racconto della storia dell’attuale penultimo amministratore delegato di Apple sia [Becoming Steve Jobs](https://itunes.apple.com/it/book/becoming-steve-jobs/id936502684?l=en&mt=11) molto più del [tomo di Walter Isaacson](https://itunes.apple.com/it/book/steve-jobs-italian-edition/id469342556?l=en&mt=11).<!--more-->

Confesso di averne letto poco del secondo e ancora niente del primo. Ho scaricato l’estratto gratuito di *Becoming Steve Jobs* incuriosito dal fatto che i *manager* di Apple gli hanno prestato collaborazione e supporto, cosa del tutto inusuale.

<blockquote class="twitter-tweet" lang="en"><p>Best portrayal is about to be released - Becoming Steve Jobs (book). Well done and first to get it right.</p>&mdash; Eddy Cue (@cue) <a href="https://twitter.com/cue/status/577495428696043520">March 16, 2015</a></blockquote> <script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>

Di solito, quando scompare una persona fondamentale in una grande azienda, si scatena la lotta di successione non tanto per prendersi la poltrona o i soldi, ma i meriti. Il vertice di Apple sta tutelando la memoria di Jobs e ha interesse del tutto relativo a farlo: il cinico pensa per istinto alla reazione più comune, *ora che non c’è più lui a farmi ombra vi faccio vedere quanto sono veramente bravo*.

Invece è diverso. Per questo un’occhiata a *Becoming Steve Jobs* va data.