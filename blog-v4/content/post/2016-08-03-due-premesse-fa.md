---
title: "Due premesse fa"
date: 2016-08-03
comments: true
tags: [iPad, Smart, Keyboard]
---
Premessa uno: diffido delle tastiere esterne fatte su misura per iPad, se non altro perché il passo tra i tasti è diverso da quello della tastiera standard e il *feeling* cambia passando dalla tastiera Mac alla tastiera iPad e questo, volendo scrivere con naturalezza, è inconcepibile. La mia tastiera per iPad finora è stata la stessa identica Apple Wireless Keyboard che uso per Mac, oggi sostituita a listino da [Magic Keyboard](http://www.apple.com/it/shop/product/MLA22T/A/magic-keyboard-italiano). L’unico suo difetto è non essere retroilluminata. Ma quando sono in forma riesco a scrivere abbastanza efficacemente anche al buio, perché se scrivi con naturalezza non hai bisogno di guardare i tasti. Per dire quanto sia importante avere tastiere con lo stesso passo.<!--more-->

Premessa due: la [Smart Keyboard per iPad Pro 9,7”](http://www.apple.com/it/shop/product/MM2L2ZM/A/smart-keyboard-per-ipad-pro-97-inglese-usa), fino a ieri aveva solo la disposizione inglese dei tasti. Inaccettabile, per scrivere sempre con naturalezza.

La premessa due è venuta a mancare. La Smart Keyboard ora ha anche la disposizione italiana dei tasti, basta chiederla. (Vale anche per la versione da iPad Pro 12,9”).

Sulla premessa uno, mi manca la verifica pratica. *Sembra* che sulla versione 9,7” – sulla 12,9” il problema non si pone – il passo sia allineato con quello di MacBook. Se così fosse, anche questa premessa cadrebbe e la coppia iPad Pro più Smart Keyboard diventerebbe interessante anche per me, quando l’attuale iPad cesserà il suo onoratissimo servizio. Appena passato il periodo estivo si impone una visita in Apple Store.