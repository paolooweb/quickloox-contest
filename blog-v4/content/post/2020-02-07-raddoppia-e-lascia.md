---
title: "Raddoppia e lascia"
date: 2020-02-07
comments: true
tags: [Swift, Objective-C, Tiobe, ZdNet, Apple, iOS, Android, Google, Play, App, Store]
---
Su [Tiobe](https://www.tiobe.com/tiobe-index/), uno dei più seguiti indici di popolarità dei linguaggi di programmazione, si è appena assistito a uno [storico passaggio di consegne](https://www.zdnet.com/article/programming-language-popularity-apples-objective-c-tumbles-down-the-rankings/): [Swift](https://swift.org/) è salito dal ventesimo al decimo posto in classifica mentre [Objective-C](https://developer.apple.com/library/archive/documentation/Cocoa/Conceptual/ProgrammingWithObjectiveC/Introduction/Introduction.html) scendeva, per coincidenza altamente simbolica, dal decimo al ventesimo.

Dopo che cinque anni Apple ha annunciato Swift come successore di Objective-C, la popolarità del secondo è immediatamente crollata (era terzo), ma il primo ha semplicemente cominciato una lenta ascesa, da molto in basso.

Sembra che la spiegazione sia una iniziale diffidenza dei programmatori verso Swift, a favore di altri linguaggi emergenti che magari promettevano una maggiore facilità per lo sviluppo di app multipiattaforma, da propinare simultaneamente su App Store e Google Play.

Il tempo è stato galantuomo e Swift, peraltro, è maturato vertiginosamemte dal debutto del 2014; non è più la scommessa né il salto nel buio, men che meno uno strumento poco vantaggioso nella pratica.

Il linguaggio per lo sviluppo sull’ecosistema Apple nel futuro prevedibile è ora autorevole e si avvia a rappresentare una *conditio sine qua non* soprattutto per i veri professionisti. È recente la notizia che App Store ha pagato complessivamente ai programmatori iOS il doppio di quanto abbia fatto Google Play ai programmatori Android, nonostante il mercato iOS sia numericamente una frazione di quello Android.

Se vuoi tentare la fortuna con la programmazione, va tentata su iOS e di conseguenza va usato,  molto bene, Swift. Objective-C è ancora molto usato ma, ora più che mai, rappresenta un percorso che ha raggiunto il compimento, per lasciare strada a chi da un mese all’altro ha raddoppiato la popolarità.

A chi ancora non ci avesse provato: sotto con [Swift Playgrounds](https://www.apple.com/swift/playgrounds/).
