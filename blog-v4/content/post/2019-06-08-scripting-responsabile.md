---
title: "Scripting responsabile"
date: 2019-06-08
comments: true
tags: [Python, Xcode, Drang, Tsai]
---
[Sono entusiasta](https://macintelligence.org/posts/2019-06-06-sogni-e-realta/) degli annunci dell’ultima WWDC, ma questo non significa che siano tutti buoni. Una situazione che fa alzare il sopracciglio riguarda la [futura esclusione](https://developer.apple.com/documentation/xcode_release_notes/xcode_11_beta_release_notes) di Python, Ruby e Perl dalla dotazione Unix standard di macOS.

Dr. Drang ha [twittato](https://twitter.com/drdrang/status/1136059002146738183?s=20) la parte rilevante delle lunghissime note di Xcode beta linkate sopra, dando credito a Michael Tsai per [essersene accorto](https://mjtsai.com/blog/2019/06/04/scripting-languages-to-be-removed/) prima di tutti.

La tesi di Dr. Drang è pessimista: vero che i linguaggi di scripting preinstallati da Apple sono sempre stati poco e male aggiornati, sono facilissimi da installare nella versione più evoluta e chiunque voglia farne uso consapevole lo fa responsabilmente, come parte attiva sapendo di non avere  tutto pronto. Però, sostiene Drang, la scelta di escluderli taglierà fuori molti che potrebbero scoprire la materia casualmente e interessarmene senza averlo saputo prima.

La posizione di Tsai è più sfumata e soprattutto i commenti al suo post inducono alla riflessione: probabilmente ci sono ragioni legali dietro la questione e non è per niente immediato stabilire se dal punto di vista dello sviluppo di software sia un bene o un male.

Constato che iPad è privo di linguaggi di scripting e che, al momento di volermene munire, non ho avuto particolari problemi, quindi non mi spaventa una loro possibile assenza su Mac. Installi e via. Mi rende più perplesso l’incertezza sulla direzione che prenderà la piattaforma. Apple tiene molto a che tutti scrivano app e lo facciano con Swift, ma questo non mi sembra il modo migliore per favorirlo.
