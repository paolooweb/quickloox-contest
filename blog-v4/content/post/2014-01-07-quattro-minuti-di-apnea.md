---
title: "Quattro minuti di apnea"
date: 2014-01-07
comments: true
tags: [Windows]
---
È stata una vera epifania, quella in piadineria.<!--more-->

Il registratore di cassa funzionava con Windows XP.

Si è bloccato tutto per ragioni sconosciute e la cassiera ha riavviato il sistema.

Ho conteggiato. *Quattro minuti* prima che la cassa tornasse operativa.

Nel frattempo si era formata una coda consistente e certamente qualcuno, invece che pazientare, se ne è andato.

Ci si può sbizzarrire a ipotizzare quanto occorra prima che, a causa dei fermi macchina, l’apparecchio finisca per costare il doppio o il triplo di quello che l’azienda ha speso.

E tutti i sistemi informatici possono bloccarsi o avere bisogno di un riavvio. Il punto è che quattro minuti di riavvio, per una cassiera di fronte a una fila di clienti scocciati, sono un’apnea da primato.