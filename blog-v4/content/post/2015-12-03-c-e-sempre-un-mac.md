---
title: "C’è sempre un Mac"
date: 2015-12-03
comments: true
tags: [Londra, Mac, Chrome, Remote, Desktop, iPad, iPhone]
---
Il Mac è rimasto a casa e il progetto era aggiornare il blog attraverso [Chrome Remote Desktop](https://chrome.google.com/webstore/detail/chrome-remote-desktop/gbchcmhmhahfdphkhkmpfmihenigjmpp?hl=it). Che ha funzionato benissimo, fino a quando per qualche ragione Mac ha perso la connessione e aggiornare è diventato impossibile.<!--more-->

Il resto delle giornate è andato meravigliosamente dal punto di vista delle cose da fare; iPad e iPhone hanno sbrigato graziosamente tutto il lavoro. In particolare, con iPad ho pubblicato un centinaio di *tweet* ciascuno corredato di una foto, quasi sempre soggetta a un minimo editing come un ritaglio o un aggiustamento automatico dei livelli di colore. Ho iniziato alle otto e a fine giornata, rientrato in albergo, iPad aveva ancora carica.

La sera, chiaro, dopo la cena di chiusura lavori c’era la festa. Con il Mac di ordinanza.

 ![Dj Set](/images/dj-set.jpg  "Dj Set") 