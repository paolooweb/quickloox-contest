---
title: "Le antenne in testa"
date: 2015-05-21
comments: true
tags: [Wi-Fi, Quinta, Quintarelli]
---
Anche se siamo un po’ fuori dall’ambito Apple e dintorni, c’è bisogno. Sta aumentando il rumore assurdo di quanti sostengono la necessità di togliere i Wi-Fi dalle scuole per via della loro presunta, supposta, possibile, ipotetica, percepita pericolosità per i giovani cervelli.<!--more-->

Si invoca un sedicente *principio di precauzione*, che è semplicemente un finale di ignoranza secca e ostentata. Il principio di precauzione vero è diverso dall’avere paura di qualsiasi cosa purché sufficientemente sconosciuta; consiste piuttosto nell’esaminare scientificamente l’oggetto di una preoccupazione e poi comportarsi in base al rischio effettivo. Si sappia che tutto presenta rischi, anche prendere il treno, accendere il fornello in casa, fare il bagno al mare; nei semi di mela è presente cianuro, semplicemente in misura così bassa che chiunque mangi una mela e muoia, muoia per un qualunque altro motivo.

Un sunto più che efficace di come stanno le cose, senza scomodare enciclopedie e dispense accademiche, [si trova sul blog di Stefano Quintarelli](http://blog.quintarelli.it/2015/03/bisogna-spegnere-il-wifi-nelle-scuole-.html), attualmente tra i pochissimi deputati che capiscono qualcosa di informatica – lui molto più di qualcosa e aiuta tantissimo la media – e costituisce lettura semplice, chiara e risolutiva. Anche i commenti sono informativi e preziosi.

Da stampare se bisogna andare al consiglio dei genitori o degli insegnanti e salta fuori uno di questi venditori di olio di serpente, ossessionato dalle antenne e con in tasca un cellulare grande come un libro tascabile. Da stampare perché, se lo si legge da iPad, quelli intorno non ci credono, sembra stregoneria. E magari si può anche stampare in trenta copie, anche se non verrà letto e, ove letto, non sarà capito.

Abbiamo già la [penultima Internet europea per velocità](http://www.apogeonline.com/webzine/2014/03/21/quando-la-banda-passo). Mettiamoci pure ad azzoppare le scuole e saremo in perfetta forma per il futuro. Che già è un miracolo averne, di Wi-Fi nelle scuole.