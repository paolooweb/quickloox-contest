---
title: "Una redazione e quattro Mac"
date: 2017-10-17
comments: true
tags: [Ebdo, Matteo]
---
L’immagine qui sotto mi arriva (grazie!) da **Matteo** che commenta:

>Proviene dalla redazione di un nuovo settimanale francese, [Ebdo](http://fabrique.ebdo-lejournal.com), dove, come puoi notare, stanno utilizzando i soliti Mac non professionali…

 ![Riunione di redazione a Ebdo](/images/ebdo.jpg  "Riunione di redazione a Ebdo") 
