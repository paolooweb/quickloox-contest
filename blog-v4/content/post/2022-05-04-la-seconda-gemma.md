---
title: "La seconda gemma"
date: 2022-05-04T23:55:58+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [Keynote Live, Apple Watch]
---
Apple ha fatto cose splendide, cose discutibili, cose orride. È che ne faceva poche, all’inizio praticamente solo una. Ora ne fa decine, centinaia e diventa più difficile accorgersi del buono o del meno buono, quando non sia in evidenza.

Di una delle cose più belle degli ultimi tempi, design magico che *just works*, ho già parlato: [la torcia elettrica di watch](https://macintelligence.org/posts/2022-01-15-due-fastidi-e-una-luce/).

Oggi ne ho vissuta un’altra.

Mi tocca la riunione in presenza. Mi tocca preparare alcune slide, cosa che non amo particolarmente, condizione però necessaria nel contesto della riunione.

Arrivo *in loco* e, complice il virus, si scopre che non c’è a disposizione una sala riunione. Un bel memento di quanto sia da riconsiderare il valore del lavoro in presenza, nei casi in cui se ne può benissimo fare a meno.

Manca una lavagna, manca un proiettore, manca uno schermo. Però tutti, come potrebbe essere diverso, hanno almeno un device, o un portatile. Io ho le mie slide, su iPad.

*Keynote Live*.

Un tasto e mando una email con un link ai partecipanti alla riunione. Non tocca subito a me, ma posso impostare Keynote Live subito, così che sia pronto nel momento in cui servirà. Interfaccia cristallina, semplice, chiara, *it just works*.

Arriva il mio momento. Ricordo agli astanti che trovano il link in email. Tocco il tasto Play e faccio andare la mia presentazione. Funziona e basta, semplice, immediata, zero problemi, nessun attrito, nessun intoppo, nessun inceppo, neanche una controindicazione. Questa è vita.

Liscio come l’olio. Banalissimo nel meccanismo, nessuno viene preso di sorpresa o resta diffidente di fronte a una funzione sconosciuta: un link sanno azionarlo tutti, alla fine passa dell’Html interattivo che ha smesso di essere una novità vent’anni fa. Tutto estremamente naturale, leggero, appagante per chi presenta e chi assiste, con il suo apparecchio, che è automaticamente quello giusto. Nessuno è inadeguato, nessuno è esagerato.

Punto fondamentale. Normalmente, sono piuttosto stanco della logica delle slide. Specialmente da quando ho scoperto il metodo Amazon, memo di sei pagine massimo che vengono consegnati a inizio riunione a ciascun partecipante e vengono letti in perfetto silenzio nei primi dieci minuti, ecco, darei tutto per rinunciare alle slide e avere un memo da leggere.

In seconda battuta, fare slide per fare slide, le farei in qualche altro modo, più vicino al testo di quello che sa fare Keynote (il miglior programma su piazza, comunque).

L’esperienza di Keynote Live, però, per quanto conosco a oggi, è assolutamente unica. Per me, *killer application*; un grande incentivo a manovrare comunque all’interno di Keynote. Una gemma del *design* Apple.