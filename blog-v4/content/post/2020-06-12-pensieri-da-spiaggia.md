---
title: "Pensieri da spiaggia"
date: 2020-06-12
comments: true
tags: [Php]
---
È un punto di partenza, oppure un punto di arrivo. È un corso di laurea breve dell'università della strada sulla tecnologia digitale e le sue ricadute sulla società.

![Il menu in spiaggia con un codice QR](/images/inquadrami.jpg "Il menu in spiaggia con un codice QR.")

Ho visto nella vita numerosi stabilimenti balneari. Una costante è sempre stata il bancone: chiacchiere, disimpegno e magari flirting negli orari morti, ressa a volte indecorosa in quelli di punta.

Poi arriva una circostanza che ti obbliga a distanziar… a sanificar… a fare le cose bene. Le persone devono sentirsi a loro agio e libere, ma ci sono vincoli da rispettare.

Ecco che la tecnologia diventa una soluzione al problema di fare le cose bene. Ha senso sgomitare in mezzo a corpi sudaticci per avere un caffè o un tramezzino? Ovviamente no. O meglio, solo se non hai mai considerato che potrebbe essere svantaggioso. Ci sono quelli che si stufano, che neanche ci provano, che si arrabbiano, che l'anno dopo non tornano.

Questo del cartello è appunto un punto di partenza oppure un punto di arrivo. Per qualcuno è difficilissimo. Come faccio a servire la gente sotto l'ombrellone senza farli alzare? Mi faccio telefonare? Mi faccio mandare un messaggio? Come gli faccio sapere che cosa prevede il menu?

Per qualcun altro è facilissimo. Ho visto i codici QR su una pubblicità, su un giornale, in un negozio. Perché non potrei usarne uno io? Come si fa? Pochi minuti di Google e ci si fa una cultura.

Ma poi? Serve probabilmente della _programmazione_. probabilmente c'è dietro un database collegato al magazzino, probabilmente serve qualche riga di Php che tenga insieme il meccanismo. Chi programma? Mio cugggino? Magari c'è una piccola software house di qualcuno che conosco in paese. Magari parlo con il nipote che ha messo in piedi un sito con gli amici. Magari chiedo in un forum perché amo fare tutto da solo e passo qualche notte a dormire poco, ma acquisisco una piccola competenza per il mio business familiare.

Le persone stanno più comode sotto l'ombrellone e si sentono anche più sicure. A te cambia poco o nulla anche negli incassi, perché la ressa davanti al bancone era semplicemente una coda e non puoi servire in nessun caso più persone di quelle che sei in grado di servire. Magari riesci a fare digerire ai bagnanti un euro in più sul listino, con cui paghi uno stagionale in più per il servizio.

La faccio semplice? È semplice. Più sicurezza, più servizio, più benessere, più soddisfazione, più controllo sull'attività, più lavoro per tutti, più tutto. È la tecnologia che trasforma la società, in meglio, riassunta in poche righe sotto l'ombrellone.

La vera domanda, e la vera sfida per l'Italia, è perché sia così difficile. Perché ci voglia la pandemia, per organizzarsi in modo da portare un vantaggio a tutti. Perché Php sia un esoterismo per iniziati invece di un patrimonio di qualsiasi ragazzo che abbia passato il biennio di secondaria superiore. Impegnarsi per farlo capire. Serve a tutti, tutti ci guadagnano.
