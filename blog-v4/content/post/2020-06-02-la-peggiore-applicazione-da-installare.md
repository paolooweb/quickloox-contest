---
title: "La peggiore applicazione da installare"
date: 2020-06-02
comments: true
tags: [Immuni, Apple, Google]
---
Quando pareva che le premesse fossero quelle del classico pasticcio all’italiana, o all’europea, non ho esitato a [distanziarmi](https://www.macintelligence.org/blog/2020/04/20/ciao-pepp-pt/) dall’idea di [Immuni](https://www.immuni.italia.it).

Lo hanno fatto in tanti, compreso qualcuno che è riuscito a fare la voce abbastanza grossa. Immuni ha cambiato modello di funzionamento e ora si appoggia all’architettura studiata apposta da Apple e Google per dare il massimo di privacy e il minimo di rischio per i dati personali.

È _open source_ e possiamo contare su una ragionevole sorveglianza da parte di quelli che hanno la capacità, il tempo e la motivazione di esaminare il suo codice costitutivo.

È pur sempre una app su cui lo stato può dire la sua e questo non depone a suo favore.

Doveva uscire tre mesi fa. Adesso è uscita, ma funziona solo in quattro regioni-test (Abruzzo, Liguria, Marche, Puglia).

È possibile che per mancanza di massa critica serva a pochi o a nessuno.

Non dovrebbe, ma potrebbe forse rosicchiare un po’ di autonomia.

Varie cose non sono ancora perfettamente chiare.

Infine, è una app che può causare ansia o porre problemi prematuri. In fin dei conti, chi avrà l’esito migliore dall’uso di Immuni sarà proprio chi non riceverà mai un messaggio preoccupante.

Ciò detto, sono soddisfatto che il mio giudizio iniziale fosse fondato, dal momento che i presupposti della app sono stati cambiati radicalmente. Il mio post ora è (lasciato volutamente) anacronistico. Non mi sono, non ci siamo sbagliati; abbiamo reclamato a gran voce che si percorresse la strada giusta, come detto sopra, e miracolosamente almeno qualcuno di noi è stato ascoltato.

È la peggiore app da installare; eppure ha fatto fare qualcosa di giusto allo stato, un miracolo. Potrebbe perfino proteggerci.

Io la installo; e mi fermo qui perché posso spiegare quali siano i programmi migliori per un iPhone, ma non dare consigli sulla salute personale.