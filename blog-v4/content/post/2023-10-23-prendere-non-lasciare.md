---
title: "Prendere, non lasciare"
date: 2023-10-23T13:02:12+01:00
draft: false
toc: false
comments: true
categories: [Internet]
tags: [Evans, Benedict Evans, Twitter, X]
---
Sono solidale con Ben Evans per tutte le ragioni che adduce al suo [addio a X-una-volta-Twitter](https://www.ben-evans.com/benedictevans/2023/10/23/leaving-twitter). Neanche bisogna essere sagaci osservatori: il mezzo ha perso affidabilità tecnica, ha perso pubblico, le funzioni di autenticazione a pagamento sono malfatte, lo *hate speech* è più libero di prima, il proprietario ha un approccio urticante, si è concesso dichiarazioni poco assennate eccetera.

Non lascio X per ora, tuttavia. Ne faccio una questione di utilizzo: mi è stato prezioso per seguire la pandemia e ora mi è prezioso per seguire la guerra. Niente, per ora, può darmi una copertura migliore.

Vorrei dire che Evans lascia Twitter perchè starci e usarlo non gli rende più come prima e a) lo capisco benissimo nonché b) approvo incondizionatamente. Però, ecco, la presenza dei neonazisti o l’inconsistenza delle spunte blu sono pretesti.

Durante la pandemia ho cercato di capire il mondo novax. Ho seguito molti individui enigmatici, problematici, odiosi, inconsistenti, bugiardi, inconsapevoli. Ho avuto contraddittori, discussioni, alterchi. Alla fine ho ricavato un ritratto collettivo del gruppo e delle loro non-ragioni. Quando è stato abbastanza, li ho bloccati tutti.

Lasci X perché non ti conviene, non per il filorusso o il neonazista. Il filorusso lo blocchi e intanto cerchi gente onesta, documentata, capace. Nonostante gli ultimi anni, X ne è pieno.

Quello che non trovi adeguato in un social è sempre una scelta che hai fatto, o non hai fatto ancora.