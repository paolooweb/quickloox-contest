---
title: "Da starne lontani"
date: 2023-07-26T15:40:04+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [Word, kerning, crenatura, tipografia, Gruber, John Gruber, Daring Fireball, Microsoft]
---
John Gruber ha pubblicato un sintetico aggiornamento all’indagine sul nuovo font preimpostato di Office e ai candidati alternativi, [materia trattata qui qualche giorno fa](https://macintelligence.org/posts/2023-07-22-aptos-a-sollevare-dubbi/). Citazione ancora più sintetica:

>I problemi di kerning con i PDF originali che ho prodotto si devono, alla fine, al fatto che stavo usando la versione web di Microsoft Word.

Un incubo condensato in due righe. Un programma che produce versioni diverse dello stesso documento a seconda della piattaforma, curato (per modo di dire) da un’azienda per la quale l’aspetto finale di un documento creato da una sua applicazione è sacrificabile.

Forse così è più chiaro il perché non sopporto l’idea di avere Word a scuola per spiegare la tipografia, la cui traduzione va oltre una lista di comandi o parole di gergo. Se spiegassi pasticceria, il cupcake dovrebbe venir fuori gustoso e non bruciato dal forno.

Un programma per scrivere incapace di avvicinare correttamente caratteri? Da starne lontani.