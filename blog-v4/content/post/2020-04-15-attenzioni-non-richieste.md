---
title: "Attenzioni non richieste"
date: 2020-04-15
comments: true
tags: [Siri, Octopress]
---
Il mio rapporto con Siri è molto buono. Nessun rapporto è comunque esente da frizioni e la mia scatta quando Siri mi propone/ricorda/suggerisce di lanciare il Comando rapido che genera automaticamente l’intestazione Octopress di un nuovo post.

Il fatto è che lo lancio volentieri, il Comando rapido, quando sono pronto a scrivere un post. Se invece non sono pronto, è chiaro che non lo lancio. Non si tratta di una azione ripetitiva acefala come puntare la sveglia o lanciare la app del supermercato quando entrò nel relativo parcheggio. Siri non sa *perché* lancio quel Comando rapido, eppure vede che lo faccio spesso a una cert’ora e allora me lo ricorda.

Se avessi bisogno di un promemoria su quanto siamo lontani dall’intelligenza artificiale, nel concetto così come nell’implementazione, questo sarebbe certamente il migliore e mi chiedo se, con l’anticipazione ossessiva dei nostri gesti, gli assistenti vocali non stiano andando un po’ oltre la loro giusta competenza.

Da una intelligenza artificiale mi aspetterei di vedermi proposto il ridimensionamento preciso di certe immagini che prelevo regolarmente da un certo sito, oppure di ritrovarmi la mattina in primo piano la prima applicazione che uso nella giornata, che no, non è l’ultima prima di andare a dormire e quindi porto sempre davanti manualmente. Oppure, boh, valutazioni specifiche su casi limite di messaggi che finiscono nello spam e magari non dovrebbero. Ehi, Siri: grazie per le attenzioni, comunque. È che a volte sono poco opportune.
