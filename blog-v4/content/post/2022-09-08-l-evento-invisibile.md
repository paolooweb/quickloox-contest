---
title: "L’evento invisibile"
date: 2022-09-08T02:49:45+01:00
draft: false
toc: false
comments: true
categories: [Hardware, Software]
tags: [iPhone, iPhone 14, Apple Watch, Apple Watch Ultra, Severance, AirPods, Dynamic Island, TV+, notch]
---
Non trovo più il *tweet* dove l’ho letto e mi dispiace, perché merita l’attribuzione, comunque *watch Series 8 è per chi non vuole morire e watch Ultra è per gli altri*.

Presentano iPhone 14, i superesperti fanno spallucce, disquisiscono degli spigoli di watch e naturalmente commentano l’ennesimo rendering buttato lì da qualcuno che sa niente di Apple e invece sa modellare tridimensionale. Il rendering di watch Ultra, invece, non l’ho proprio visto, chissà come mai.

Un iPhone 14 può chiedere aiuto via satellite e durante [l’evento](https://events-delivery.apple.com/2807skttevpekgjkgcyolyxgkexyahqp/m3u8/vod_index-bHTtMFcgdqmJGoHoDBPadNWwGwrNevrj.m3u8) si dilungano a spiegare come funziona e che cosa consente. Sono curioso di vedere come faranno a copiarla su Android, questa funzione, che in pratica è il trapianto hardware di un sottoinsieme di telefonia satellitare dentro iPhone.

Non che manchino gli altri progressi; anche qui, *mea culpa*, non ho il tweet sottomano, ma *ogni anno Apple dice: visto che schifezza di fotocamera abbiamo mostrato l’anno scorso? Guarda quest’anno*.

Però Apple non innova, dice, il design non è più quello di una volta. Da anni non vedevo i commenti che vedo a proposito della Dynamic Island, *design is how it works*. [Prendi una caratteristica hardware che potrebbe sembrare un difetto e la trasformi in un vantaggio](https://twitter.com/GregoryMcFadden/status/1567618805236436993), con una eleganza software che tradisce l’attenzione persino esagerata al dettaglio.

<blockquote class="twitter-tweet"><p lang="en" dir="ltr">Apple took what should have been a weakness for this phone, and made it into one of its greatest strengths. Ingenious. <a href="https://t.co/s1aifF1yXY">pic.twitter.com/s1aifF1yXY</a></p>&mdash; GregsGadgets (@GregoryMcFadden) <a href="https://twitter.com/GregoryMcFadden/status/1567618805236436993?ref_src=twsrc%5Etfw">September 7, 2022</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

Parlando di design, tornare a watch Ultra per un momento e contemplarlo. Le linee, ma anche l’action button e poi i cinturini. Sono contentissimo del mio watch Series 7 che, per filosofia consolidata, non si tocca e non si cambia. Ma i cinturini dell’Ultra arrivano da una civiltà più evoluta.

Sarà che si concentrano sul design di iPhone e di AirPods e di watch e trascurano macOS? Può darsi. Oggi però parliamo di questi prodotti e assistiamo a una evoluzione di prodotto che, nel caso di iPhone, pure arrivata alla tredicesima iterazione o giù di lì, impressiona.

Da notare che l’evento è stato virtuale, però c’è stata una partecipazione fisica ad Apple Park. È una conferma che certi cambiamenti indotti dalla pandemia sono destinati a incidere in modo permanente. Vuoi gente che prenda in mano i nuovi modelli, ma la presentazione globale, preparata cinematograficamente, non si batte, soprattuto se realizzata in maniera impeccabile e sempre con un tocco diverso rispetto alle precedenti.

Presentazione in cui c’era una citazione dalla nuova stagione di [Severance](https://www.imdb.com/title/tt11280740/). Non guardo *Severance*, ma ho visto quei pochi fotogrammi a fine evento che non riuscivo a collocare. Trovata la spiegazione, mi è venuta la curiosità di vedere *Severance*. È possibile che l’evento di quest’anno porti un numero imprecisato di nuovi spettatori su TV+, se qualcun altro avrà provato le stesse sensazioni e compie identico percorso. Fai l’evento per vendere iPhone e intanto guadagni spettatori per le tue serie in modo scaltro e accattivante, si possono permettere anche questo.

Che altro? È stata dispiegata sul palco virtuale una quantità e una qualità di tecnologia davvero poco usuali. Eppure torniamo a casa, virtualmente, pensando a gente in difficoltà in montagna che chiama aiuto con il satellite, fa incidenti d’auto ma se la cava perché l’orologio chiama i soccorsi, riesce a filmare correndo e a produrre video stabile, capisce meglio quando ovula grazie alla rilevazione della temperatura corporea.

Vediamo sempre più persone e sempre meno tecnologia, nonostante la tecnologia sia tanta ed evoluta. È una delle premesse della vittoria della tecnologia: rendersi invisibile. iPhone lo compri perché fotografa alla grande, ti tira fuori dai guai e come tocchi il *notch* rimani conquistato. Fino a ieri, il *notch* era lo stigma. Se non è genio, è lavoro. Pazzesco.

*Thank you for joining us, have a good day*.