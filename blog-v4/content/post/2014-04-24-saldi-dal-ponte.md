---
title: "Saldi dal ponte"
date: 2014-04-24
comments: true
tags: [Playdek, Waterdeep, Stackup, bundle, MacHeist, Scanner, Recordium, TextExpander]
---
Interrompiamo momentaneamente le trasmissioni per segnalare che tra un ponte e l’altro attraversiamo forse il momento migliore per rinnovare il parco software.<!--more-->

In campo iOS sconti e promozioni sono all’ordine del giorno; sopra a tutto voglio però sottolineare che fino al 28 aprile si può approfittare dei [saldi di Playdek](http://mad.ly/c97bb4?fe=1&pact=21985471761). In particolare spero che qualcuno si lasci conquistare da Lords of Waterdeep; e poi i ragazzi di Playdek svendono altre cose interessanti (oltre a regalare Summoner Wars).

Ho anche scoperto che perfino su iOS organizzano i *bundle* e quasi non ci si crede, visto che i prezzi di App Store sono già bassi. [Stackup iOS App Bundle](https://deals.tuaw.com/ios_bundle) rende vero tutto ciò. La convenienza non pareggia quella delle offerte in ambito Mac, ma ci mancherebbe; Boxer e TextExpander touch – più forse Scanner Pro e Recordium Pro – sono le prede che mettono più acquolina in bocca.

[Name Your Own Price](https://stacksocial.com/sales/the-name-your-own-price-mac-bundle-5-0) per Mac ha una formula furbetta: se offri più della media degli altri porti a casa più programmi. C’è da dire che comunque si porta a casa diverso software interessante a un prezzo clamorosamente inferiore al listino. Incuriosisce Projects in iOS E-Learning Course; per una persona seriamente interessata è un’occasionaccia.

[MacHeist](http://macheist.com), per concludere con un sempreverde, non ha offerte in corso, ma ha saputo inventarsi una cornice grafica e di narrazione cui non riesco a resistere. Anche senza comprare niente, mi diverto con i *puzzle* e i misteri da risolvere.

Riprendiamo le trasmissioni scusandoci per l’interruzione.