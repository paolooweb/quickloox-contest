---
title: "In attesa del notbook"
date: 2009-10-24
comments: true
tags: [Apple, Mac, Ping]
---
Nel periodo luglio-settembre 2009 Apple ha stabilito il record di sempre nella <a href="http://images.apple.com/pr/pdf/q409data_sum.pdf">vendita trimestrale di Mac</a>: tre milioni e oltre di macchine, mai accaduto prima. Di questi, 2,3 milioni sono portatili.

La vendita dei portatili, rispetto allo stesso periodo del 2008, ha registrato +35 percento in unit&#224; e +27 percento in fatturato. Si stima che il prezzo medio di vendita sia stato di 1.265 dollari. Il tutto ha sensibilmente contribuito al guadagno totale di 1,67 miliardi di dollari, mai cos&#236; alto finora.

Nessuno di questi portatili &#232; un *netbook*.

Il 14 ottobre 2008 Doug Amoth di CrunchGear titolava <a href="http://www.crunchgear.com/2008/10/14/five-reasons-why-an-apple-netbook-is-a-no-brainer/">Cinque ragioni per cui un netbook Apple &#232; una scelta ovvia</a>.

Il 20 gennaio 2009 Brian Caulfield di Forbes titolava <a href="http://www.forbes.com/2009/01/19/apple-earns-preview-tech-enter-cx_bc_0120apple.html">Il vero problema di Apple sono i netbook</a>.

Il 21 gennaio 2009 Brian X. Chen titolava su Wired Gadget <a href="http://www.wired.com/gadgetlab/2009/01/apple-still-thi/">Apple &#232; ancora indifferente all&#8217;opportunit&#224; dei netbook</a>.

Il 18 marzo 2009 Shane O&#8217;Neill di Pc World titolava <a href="http://www.pcworld.com/businesscenter/article/161496/recession_breathes_life_into_windows_pcs_as_apple_gasps_for_air.html">La recessione insuffla vita nei Pc Windows mentre Apple boccheggia</a>. Questo &#232; formidabile e si merita anche la citazione di un paragrafo:

>Credo che smetter&#242; di chiedermi quando Apple riconoscer&#224; i tempi bui in cui viviamo, perch&#233; penso la risposta sia mai. Pu&#242; darsi che Apple debba essere solo un&#8217;azienda da vacche grasse. Quando si fa grigia, dovrebbe fare i bagagli come un giostraio o sparire come una squadra di baseball in inverno e non tornare fino a quando non sono nuovamente tutti ricchi e felici.

(Ripeto: Apple non ha mai venduto cos&#236; tanti Mac da quando esiste e secondo l&#8217;autore avrebbe dovuto nascondersi ad aspettare che passasse la bufera).

Il 24 marzo 2009 Scott Moritz di TheStreet.com sapeva come sarebbe andata a finire prima ancora che iniziasse: <a href="http://www.thestreet.com/story/10476593/2/apples-netbook-foray-will-flop.html">L&#8217;incursione di Apple nei netbook sar&#224; un flop</a>.

Infine, il 19 agosto 2009, Charles Moore di The Apple Blog titola <a href="http://theappleblog.com/2009/08/19/lack-of-netbook-price-hurting-apple-in-this-years-back-to-school-market/">La mancanza di un netbook e i prezzi alti stanno danneggiando Apple sul mercato back-to-school</a> (l&#8217;acquisto di computer in previsione del ritorno a scuola).

Nel settore education Apple ha fatto <a href="http://seekingalpha.com/article/167404-apple-f4q09-qtr-end-9-26-09-earnings-call-transcript?page=-1">il trimestre migliore di sempre</a>, con una crescita di fatturato del 12 percento. In particolare ha venduto 50 mila MacBook allo Stato americano del Maine, che li distribuir&#224; a docenti e studenti.

La verit&#224;: se Apple fosse stata cos&#236; stupida da produrre un *netbook* avrebbe perso una montagna di soldi e messo nelle mani di migliaia di persone l&#8217;aggeggio pi&#249; stupido che sia mai stato inventato per elaborare informazioni.

Continuo a sperare che Apple rimarr&#224; saggia e, se decider&#224;, produrr&#224; il *notbook*: non un appecoronamento al gregge dei tutti uguali, ma qualcosa che sconvolga ancora una volta le regole del gioco, ci dia ancora una volta opportunit&#224; migliori e ancora una volta ci faccia dire *incredibile che nessuno ci abbia pensato prima*.