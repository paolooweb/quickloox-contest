---
title: "Itera, itera, qualcosa resterà"
date: 2013-07-02
comments: true
tags: [Design]
---
Gli sviluppatori della raccomandata <a href="https://itunes.apple.com/it/app/vesper/id655895325?l=en&mt=8">Vesper</a> hanno copiato una pagina del libro degli schemi, come dicono gli americani, da Bare Bones. E hanno fatto bene.<!--more-->

Si vede da come <a href="http://vesperapp.co/blog/vesper-1-001/">l’elenco delle modifiche alla versione 1.001</a> somigli così tanto in minuzia, precisione e spirito alle <a href="http://barebones.com/support/bbedit/arch_bbedit1054.html">note di aggiornamento di BBEdit 10.5.4</a>.

Il prodotto che vale più del suo prezzo non è quello che esce perfetto. È quello che tende alla perfezione una iterazione dopo l’altra.