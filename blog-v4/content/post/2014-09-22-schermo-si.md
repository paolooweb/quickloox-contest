---
title: "Schermo sì, schermo no"
date: 2014-09-22
comments: true
tags: [Samsung, Apple, iPhone, iPhone6, S5, TheOnion]
---
Abbastanza disquisizioni negli ultimi giorni per apprezzare la sana ironia di *The Onion* nel suo parodistico [confronto tra iPhone 6 e S5 Samsung](http://www.theonion.com/articles/iphone-6-plus-vs-samsung-galaxy-s5,36969/).<!--more-->

Un bel colpetto anche ai fanatici della lista della spesa, quelli che spaccano il pixel in quattro e spacciano voti al grammo di batteria. Purtroppo è gente prima di *sense of humor*.