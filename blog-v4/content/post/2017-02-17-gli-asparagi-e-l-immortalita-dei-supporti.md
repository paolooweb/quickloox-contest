---
title: "Gli asparagi e l’immortalità dei supporti"
date: 2017-02-17
comments: true
tags: [dischi, Ssd, Backblaze, iPhone, Esselunga, Seagate, WD, LaCie]
---
La mamma di un mio amico ha vinto un iPhone 7 facendo la spesa All’Esselunga. A me è capitato di sentire che due persone hanno vinto un iPhone 7 mentre mi trovavo al bar dell’Esselunga. Io non ho vinto un iPhone 7 All’Esselunga; in casa ne abbiamo uno (non mio) e lo abbiamo acquistato All’Apple Store. Un mio amico ha acquistato un iPhone 7, ma online. Un altro mio amico non ha comprato iPhone 7 ma ha un contratto con Vodafone che gliene lascia usare uno per due anni.

Nessuno di noi ricava dalla propria esperienza personale una completa rappresentazione della realtà. Se pensassi che gli iPhone 7 si comprano, tutti, sarei smentito dalla mamma del mio amico. La quale, se immagina che tutti gli iPhone in giro siano stati vinti facendo la spesa, è in errore.

E adesso, chiedo scusa per la lunga premessa, veniamo ai dischi rigidi e in generale al tema della conservazione dei dati.

Ti trovi bene con Seagate? Bravo. Ti si è rotto un LaCie? Mi dispiace. Consiglieresti a tutti un Western Digital? Buon pro ti faccia. La tua esperienza personale (la mia, quella di chiunque) *conta zero virgola zero zero zero zero zero zero zero zero uno* e così il tuo consiglio. Molto meno di così a dirla tutta, ma basta il concetto. Nessuno di noi ricava dalla propria esperienza con i dischi rigidi (Ssd, audiocassette, floppy disk) una completa rappresentazione della realtà.

Per esempio: Backblaze (servizi di backup online) ha una base installata di quasi ottantamila dischi e raccoglie costantemente i dati sul loro funzionamento. Le [statistiche del 2016](https://www.backblaze.com/blog/hard-drive-benchmark-stats-2016/) riguardano un miliardo di ore di funzionamento e registrano quasi duemila guasti, sugli ottantamila supporti, nei dintorni del due percento.

I dati sono consultabili per tipologia di disco, dimensione, fabbricante.

Hai una vasta esperienza di dischi, con i tuoi ottanta supporti? Ammesso che tu abbia una documentazione completa e articolata, vale un millesimo di quella di Backblaze. Quando avrai fatto le tue analisi altre novecentonovantanove volte, inizieranno ad avere un qualche valore. Prima ti dirò che la mamma di un mio amico, iPhone 7 lo ha vinto all’Esselunga.

Tra lei e le dinamiche di acquisto degli iPhone esiste la stessa relazione che regola i rapporti tra asparagi e immortalità dell’anima descritta da Achille Campanile, o tra Apple Pencil e Surface Pen: [non c'è niente in comune](https://macintelligence.org/posts/2015-09-15-gli-asparagi-e-apple-pencil/).

Così come tra la tua (mia, di chiunque) rispettabilissima esperienza personale con i dischi e la rappresentazione completa della realtà.

 A seguire altre sezioni dell’argomento, sempre dominate dallo stesso tema: l’esperienza personale informatica sta alla realtà complessiva come gli asparagi all’immortalità dell’anima.
