---
title: "Digitalizzazione con scappellamento"
date: 2021-11-04T00:04:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [Agenzia delle Entrate] 
---
L’Agenzia delle Entrate ritiene che la mia commercialista abbia commesso degli errori nella dichiarazione dei redditi del 2013 (duemilatredici) e io debba versare alcuni euro al fisco per sanare la ferita. La qual faccenda lascia, anzi, deve lasciare giustamente indifferenti, se non per il fatto di fornire un pretesto letterario da cui parte la vicenda.

L’Agenzia delle Entrate mi fa consegnare *brevi manu* dal postino la busta che contiene l’avviso. Firmo con il dito sul terminale in dotazione al postino stesso. È il digitale, bellezza.

Il giorno dopo, il postino citofona nuovamente. Deve darmi la notifica della consegna del giorno precedente. E lo fa, una striscetta di carta che attesta senza pietà la mia piena sottomissione e testimonierà nei secoli che quella busta, l’ho ricevuta.

A questo punto la apro, la busta, che era rimasta dimenticata per ben ventiquattr’ore. Contiene dieci fogli di carta stampati fronte e retro e graffettati assieme per dettagliare la mia colpa. Dieci fogli.

Mi accingo a scandire tutto e spedirlo alla commercialista, quando vedo un URL nelle note al testo. Incredibilmente, il documento esiste anche in forma digitale e basterebbe scaricarlo dall’URL che ho in mano! Troppo bello per essere vero.

Scarico e invio alla commercialista un PDF bellissimo. Le gioie del digitale nella pubblica amministrazione.

Il giorno dopo, la commercialista mi scrive. Leggo, vado a verificare. È vero.

Il documento in digitale contiene *nove* pagine. Manca la decima, correttamente elencata nella copertina come allegato.

Ho fretta; scandisco approssimativamente la pagina mancante e la invio alla commercialista.

Poi cerco la versione digitale dell’allegato, che sarà certamente meglio della mia scansione frettolosa. Non c’è. Quella pagina esiste solo sulla carta, viene pinzata assieme al documento più voluminoso e rimane assente nella versione digitale.

Sulla copia cartacea un codice QR mi permette, volendolo, di *verificare la conformità della copia digitale rispetto alla copia cartacea*. Sulla carta c’è un foglio in più.

E niente, così. Che pianeta pensano di salvare ventisei primi ministri, da soli, quando domani l’Agenzia delle Entrate potrebbe scrivere a tutti loro?

Siamo in piena fase tognazziana. Tognazzi era stato additato come [capo delle Brigate Rosse dalla feroce satira de Il Male](http://www.ugotognazzi.com/brigate_rosse.htm), o così pensavamo.

In realtà era una copertura per mascherare [tutt’altro incarico](https://www.youtube.com/watch?v=IoEK2Z3-JH8).

<iframe width="560" height="315" src="https://www.youtube.com/embed/IoEK2Z3-JH8" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

*Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile.*