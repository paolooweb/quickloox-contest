---
title: "Il browser più veloce"
date: 2009-07-15
comments: true
tags: [Ping, Unix]
---
Adesso &#232; anche molto pi&#249; comodo.<!--more-->

Un programmatore geniale ha creato <a href="http://habilis.net/lynxlet/">Lynxlet</a>: una normale applicazione che fa partire lynx dentro il Terminale.

lynx &#232; un retaggio del passato e non si trova a proprio agio con il web 2.0, con l&#8217;interativit&#224; spinta, con i filmati e con i giochi Flash.

Tuttavia, se si cercano informazioni, eclissa qualsiasi Safari.