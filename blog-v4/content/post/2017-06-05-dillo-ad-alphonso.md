---
title: "Dillo ad Alphonso"
date: 2017-06-05
comments: true
tags: [Untangled, privacy, Alphonso, Acr]
---
Ho pensato di scaricare [Untangled HD](https://appsto.re/it/JbOBz.i) e poi ho letto fino in fondo la descrizione della app:

>Questa app è integrata dal software Automated Content Recognition (“ACR”) di Alphonso, un servizio indipendente. Con il tuo permesso, accordato al momento di scaricare la app, il software ACR acquisisce brevi campioni di audio captati dal microfono del tuo apparecchio. L’accesso al microfono avviene solo previo consenso e i campioni di audio restano nell’apparecchio, però viene generato un loro hash che costituisce una “firma sonora”. Le firme sonore vengono confrontate con il contenuto commerciale proveniente dal televisore o da altre fonti video come set-top box, riproduttori di media, console di videogiochi. Se trova una corrispondenza, Alphonso può usare questa informazione per inviare al tuo apparecchio pubblicità più calibrata. Il software ACR esamina solo contenuto commerciale noto e non capisce conversazioni umane o altri suoni.<!--more-->

Sintesi: mentre giochi, il software ascolta la pubblicità trasmessa in casa e, se la riconosce, ti invia pubblicità più specifica, sulla base di quello che ha captato.

In sincerità, non è cosa che mi infastidisca. Una *app* iOS accede al microfono solo dietro permesso. È sufficiente negarlo.

Poi però penso a quanti non leggono le descrizioni dei programmi, danno permessi senza pensare, stanno poco attenti al consumo di *app* da parte dei figli e, soprattutto, a che cosa potrebbe succedere nel mondo Android. Dove mi aspetto tranquillamente che ci siano app che usano Alphonso senza dirlo, si prendono permessi senza chiedere, l’utilizzatore medio sta attento la metà che su iOS e allora un po' di domande me le pongo.

Anche perché gira un sacco di gente convinta che sicurezza e privacy dipendano dall’antivirus. Vallo a raccontare a Alphonso.
