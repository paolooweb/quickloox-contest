---
title: "La soddisfazione di Alba"
date: 2022-08-04T01:00:53+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [Alba, Alba A Wildlife Adventure, Lidia, iPad Pro]
---
Lidia ha completato, praticamente da sola, [Alba: A Wildlife Adventure](https://apps.apple.com/it/app/alba-a-wildlife-adventure/id1528014682).

*Alba* è una app perfetta per magnificare Apple Arcade. Un’avventura grafica semplice e non stupida, con una innocua (ma articolata) trama di salvataggio della riserva naturale messa in pericolo dagli speculatori e tanti, tanti animali da avvistare e fotografare all’interno del gioco.

A Lidia gli animali piacciono molto e gli avvistamenti l’hanno guidata, uno dopo l’altro, ai percorsi necessari per risolvere l’avventura. Alba è una bambina in vacanza presso i nonni su un’isola e, per arrivare al lieto fine, dovrà esplorare l’isola nei suoi vari ambienti, artificiali e naturali. Dovrà parlare con persone ma anche soccorrere animali e, appunto, riempire il suo taccuino da *birdwatcher*.

La realizzazione è onestissima, la grafica tranquilla e gradevole, niente è stressante, quando sembra che il gioco stia per svoltare nella noia ecco aprirsi un nuovo percorso, appare l’animale cui si dava la caccia fotografica, si trova la strada per entrare nel castello o uscire dalla palude.

Lidia ha fatto anche gli straordinari perché a casa nostra, per editto genitoriale, i videogiochi sono obbligatoriamente in inglese; e così abbiamo passato un tempo apprezzabile a collegare i nomi degli animali non ovvi avvistati nel gioco al loro corrispondente italiano.

I dialoghi erano anch’essi in inglese, ma hanno causato pochi problemi. Il più delle volte la situazione è autoesplicativa, il gioco guida verso la missione da compiere e in una manciata di occasioni abbiamo tradotto al volo qualche fumetto che non rientrava nelle nostre conoscenze.

Il gioco è terminato, ma a Lidia è piaciuto sufficientemente da ripartire con una nuova partita, per quanto la rigiocabilità sia bassa. Il vero motivo, anche se non lo ha detto chiaramente, è che ha fotografato tutti gli animali a disposizione *tranne uno*.

Divertimento istruttivo, leggero ma serio quanto serve a una bambina, zero pubblicità, zero acquisti in-app, zero tracciamenti. Da settembre la mia missione è scandagliare Apple Arcade e trovare almeno una perla al mese. Di giochi carini ce ne sono molti, al livello di *Alba* pochi però.