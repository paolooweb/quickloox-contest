---
title: "L’unica cosa è sorridere"
date: 2017-06-14
comments: true
tags: [Chess.com, Gangnam, YouTube, Windows, Backblaze, Windows, iPad]
---
Sembra incredibile, ma trovi gente a frotte che giustifica la vendita di una versione a 32 bit di Windows 10, [a differenza dell’amministratore delegato di Backblaze](https://macintelligence.org/posts/2017-06-10-nostalgia-dei-sedici/).

<iframe src="https://www.facebook.com/plugins/post.php?href=https%3A%2F%2Fwww.facebook.com%2FEdoardoVolpiKellermann%2Fposts%2F10212952477716868&width=500" width="500" height="235" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true"></iframe>

Le cose funzionano, nessuno le tocca, diventano intoccabili perché nessuno sa più metterci mano. E vabbeh. Ma poi vai a comprare un sistema operativo moderno *per usarlo a 32 bit*. No; ti tieni il sistema operativo vecchio e ci fai funzionare le cose vecchie, se proprio.

Ma non c’è problema, funziona tutto, sul mio computer faccio quello che voglio. Poi succede che sui vecchi iPad a 32 bit [si blocchi Chess.com](https://twitter.com/chesscom/status/874349405444460544).

<blockquote class="twitter-tweet" data-lang="en"><p lang="en" dir="ltr">32-bit iOS devices are experiencing issues due to limitations interpreting game IDs over 2,147,483,647. Fix should be out in 48 hours :)</p>&mdash; Chess.com (@chesscom) <a href="https://twitter.com/chesscom/status/874349405444460544">June 12, 2017</a></blockquote> <script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>

Windows a 32 bit non farà molto meglio di così.

C’è [un precedente del 2014](http://www.apogeonline.com/webzine/2014/12/09/lumanita-e-a-32-bit): il rapper coreano Psy totalizzò oltre due miliardi di visualizzazioni del suo Gangnam Style, costringendo YouTube a passare a 64 bit per tenere il conteggio. Allo stesso modo, le partite di scacchi su Chess.com hanno superato la quantità di due miliardi e spiccioli, 2^31.

(**Aggiornamento:** 2^31 è il numero positivo più alto ottenibile usando una variabile *signed integer* nei linguaggi di programmazione più usati. 2^32 vale quattro miliardi e oltre, ma una variabile *signed integer* usa la metà mancante dei valori per i numeri negativi. YouTube era stato aggiornato a 64 bit mesi prima che *Gangnam Style* sfondasse quota 2^31 e così il servizio non ebbe mai alcun problema effettivo, ma Google [annunciò ugualmente l’accaduto](https://www.cnet.com/news/gangnam-style-busts-youtubes-view-counter-not-so-fast/) per guadagnare visibilità sui media. Il che non cambia la questione: il passaggio a 64 bit fu semplicemente previsto invece che improvviso. Chess.com ha dedicato un [lungo _post_](https://www.chess.com/news/view/the-unique-reason-the-broken-now-fixed-chess-com-app-made-headlines-6918) alla faccenda).

Per chi uscisse oggi da una caverna profonda e isolata, Internet sta faticosamente passando a IPv6, indirizzamento a sessantaquattro bit. Perché trentadue bit non bastano più per tutti gli URL che occorre generare.

La vita va verso i sessantaquattro bit. Comprare qualcosa di *nuovo* e volerlo a 32 bit è come acquistare un’auto nuova senza le cinture di sicurezza. Le auto di una volta funzionavano, vanno ancora benissimo se tenute con cura, che problema c’è? C’è, al punto che la legge interviene per evitare che un fabbricante demente metta in vendita auto senza cinture. Il 2017 non è il 1997: in questo secolo [il software si aggiorna](http://www.apogeonline.com/webzine/2017/05/18/domani-e-un-altro-aggiorno-e-si-vede-gia), anche se costa un pizzico di impegno.

Oggi si blocca il programma di scacchi e tutto sommato si sopravvive. Domani magari è volta di uno home banking, o di una rete sanitaria, e il grado di consapevolezza cresce improvvisamente.

Quelli costretti a vivere nel passato, è un conto; quelli che *cercano* attivamente il passato, oggi, vanno guardati sorridendo.

Perché creano un problema a sé e agli altri. E comunque trentadue denti, al contrario, sono perfettamente sufficienti.