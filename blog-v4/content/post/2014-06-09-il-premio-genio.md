---
title: "Il premio Genio"
date: 2014-06-10
comments: true
tags: [Healthbook, Health, HealthKit, Apple, Gurman, Tuaw, 9to5Mac]
---
Siamo verso metà anno, una assegnazione ci sta tutta.

<blockquote class="twitter-tweet" lang="en"><p>I am convinced Apple completely redesigned Healthbook and dropped the &quot;book&quot; because of the leak.</p>&mdash; Mark Gurman (@markgurman) <a href="https://twitter.com/markgurman/statuses/473565076377858048">June 2, 2014</a></blockquote>
<script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>

>Sono convinto che Apple abbia completamente riprogettato Healthbook e cambiato nome a causa dell’indiscrezione.<!--more-->

Mark Gurman, premio Genio 2014, pubblicò a marzo (mica un anno fa) un [articolo-scoop](http://9to5mac.com/2014/03/17/this-is-healthbook-apples-first-major-step-into-health-fitness-tracking/) ricco di particolari e schermate su Healthbook, *il primo passo importante di Apple nel tracciamento della salute e del benessere*.

Disgraziatamente, tre mesi dopo (mica trenta), la *app* si chiama [Health](http://www.apple.com/ios/ios8/health/) e l’insieme delle interfacce di programmazione si chiama HealthKit. E le schermate sono alquanto diverse.

The Unofficial Apple Weblog offre [tre spiegazioni alternative](http://www.tuaw.com/2014/06/05/rumor-roundup-summer-child/) all’accaduto e specifica che sono *non egotistiche*.

>La tua fonte semplicemente si sbagliava. Succede.

>La tua fonte ha potuto vedere solo una versione preliminare che poi è cambiata per un sacco di ragioni completamente scorrelate dall’indiscrezione, tipo migliorare l’interfaccia utente o semplificare il marchio… cose che Apple tende a fare prima di presentare cose in pubblico.

(il *tende a fare* presenta tracce di *understatement*).

>La tua fonte era sospettata di passare informazioni all’esterno e le è stato assegnato un finto progetto per metterla alle prova. Quando le informazioni relative al progetto sono trapelate all’esterno, Apple ha avuto la conferma dei sospetti e ha accompagnato la fonte senza cerimonie alla porta.

Chi si ostini ancora a leggere masochisticamente siti italiani di *rumor* Mac, potrà trovare ampie e puntuali riprese delle informazioni su Healthbook, prive di ogni valore e vuote di ogni utilità.