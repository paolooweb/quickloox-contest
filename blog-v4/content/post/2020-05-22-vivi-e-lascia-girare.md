---
title: "Vivi e lascia girare"
date: 2020-05-22
comments: true
tags: [TidBits, iOS, Mac]
---
Un grande come Adam Engst può prendere un tema abusato come [l'abitudine sbagliata di forzare la chiusura delle app su iOS](https://tidbits.com/2020/05/21/why-you-shouldnt-make-a-habit-of-force-quitting-ios-apps-or-restarting-ios-devices/) e farne un pezzo istruttivo eccellente da leggere.

C'è la spiegazione a tutto, logica, esaustiva, compresa la nozione che terminare le app compulsivamente aumenta il consumo della batteria. Viene citato un esempio di autonomia addirittura quadruplicata in un iPad e mi pare un po' esageratamente aneddotico; non lo prenderei come il dato tipico. C'è però effettivamente da guadagnare in autonomia a lasciare vivere le app, o almeno a terminarle con giudizio.

Sono certo che qualcuno interpreterà il tutto come l'ennesimo attentato alla propria _libertà di fare quello che vuole_ sul suo iPhone, visto che lo ha pagato. Non resteremo mai senza confusi sulla differenza tra uso legittimo e uso appropriato. E mai nessuno che si lanci in autostrada a centotrenta orari in prima, perché l'auto è sua e può farci quello che vuole.