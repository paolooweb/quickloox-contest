---
title: "La città degli iPad"
date: 2021-07-13T01:09:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [Crema, Gorizia, Csuccess, California State University, iPad Air, Apple Pencil] 
---
Otto campus parte dell’Università statale della California [forniranno ai nuovi iscritti di quest’anno](https://www2.calstate.edu/impact-of-the-csu/student-success/csuccess/Pages/default.aspx) che lo richiedono un kit composto da iPad Air, tastiera e Apple Pencil, nonché il supporto tecnico che sarà necessario.

In totale saranno serviti trentacinquemila studenti, che potranno tenere il kit durante la loro intera frequenza universitaria.

California State University non è roba da ricchi; metà degli studenti frequenta grazie a borse di studio e iniziative filantropiche. La maggioranza dei laureati è il primo o la prima a raggiungere il traguardo nella propria famiglia di origine.

Non so se sia il modo più intelligente per risolvere il *digital divide*; certo è più intelligente che lamentarsi del digital divide e intanto comprare banchi a rotelle e Lim.

Pensando all’Italia, è come se questo autunno, nessuno escluso, tutti gli abitanti di Crema, o di Gorizia, ricevessero un iPad Air.

*Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile.*