---
title: "Il novanta fa paura"
date: 2018-07-08
comments: true
tags: [Mimmo, R, emacs]
---
Ringrazio **Mimmo** per avermi segnalato [Usare R con Emacs e ESS - un ambiente multifunzione](http://lucidmanager.org/using-r-with-emacs/).

Con un titolo così può passare per materiale esoterico, ma comincia così:

>Pochi anni fa mi sono liberato del foglio di calcolo per scrivere invece codice in R. Nel procedere ho appreso una lezione di valore: più la curva di apprendimento è ripida, maggiore è il ritorno. Il mio tempo investito nell’apprendimento di R ha pagato alla grande e oggi uso questo linguaggio per tutte le mie attività numeriche e di analisi quantitativa.

E ancora:

>Dall’anno scorso uso emacs e ancora una volta la regola della curva ripida che porta grandi vantaggi si è dimostrata vera. […] Quasi il novanta percento di tutta la mia attività computazionale ora avviene dentro emacs.

A questo punto, a leggere quel *novanta percento*, ti viene la paura di stare veramente perdendo qualcosa di grosso a non approfondire.