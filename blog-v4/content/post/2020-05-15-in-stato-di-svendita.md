---
title: "In stato di svendita"
date: 2020-05-15
comments: true
tags: [Amazon, Microsoft, Conte, Pisano]
---
Per chi seguisse le sorti del cloud in Italia, Amazon ha da poco [aperto la regione di Europa (Milano) dei propri Amazon Web Services](https://aws.amazon.com/blogs/aws/now-open-aws-europe-milan-region/). I lavori sono cominciati nel 2018. In fatto di infrastruttura, indotto e attenzione per l’Italia è essenzialmente una notizia positiva. Dal link si raggiunge la notizia presente sul blog di Amazon.

Microsoft ha appena annunciato [un investimento parte del programma Ambizione Italia](https://news.microsoft.com/europe/2020/05/08/microsoft-announces-1-5-billion-investment-plan-to-accelerate-digital-transformation-in-italy-including-its-first-cloud-datacenter-region/), iniziato nel 2018, che *promette* una regione italiana del proprio cloud entro cinque anni.

Dal link si raggiunge il comunicato stampa, che contiene dichiarazioni entusiastiche *del presidente del consiglio e del ministro dell’innovazione*.

Siccome rappresentano in questa situazione il Paese, da cittadino vorrei sapere se abbiamo comprato qualcosa, o venduto qualcosa, o svenduto qualcosa, o promesso in cambio di promesse, o promesso in cambio di investimenti, o qualcosa d’altro, e l’esatta natura di tutti questi qualcosa.