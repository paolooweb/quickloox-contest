---
title: "Chiedo per un collega"
date: 2021-09-02T02:10:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [M1, MacBook Air, iMac, Monterey, macOS, Thunderbolt] 
---
Passerò parte del fine settimana a cercare accrocchi che rendano possibile usare un iMac 2018 come schermo di un MacBook Air M1.

Sono già passato da una [pagina di supporto Apple che spiega come fare](https://support.apple.com/en-us/HT204592) con determinati modelli… solo che i Mac del problema suesposto ricadono in fasce d’età non contemplate.

Ironia della sorte, iMac che è troppo giovane per profittare della pagina di supporto è anche troppo vecchio per approfittare di [Monterey](https://www.apple.com/macos/monterey-preview/) che altrimenti risolverebbe via AirPlay.

So già che passare da un banale cavo Thunderbolt non è abbastanza. Devo trovare un adattatore/convertitore/transmogrification che colleghi i due Mac e passi il video di MacBook Air sullo schermo di iMac.

Cercare è ovviamente compito mio; se qualcuno avesse già trovato e fosse soddisfatto della sua scelta, e me lo facesse pure sapere, sarei felice per lui e anche per me.

*Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile.*