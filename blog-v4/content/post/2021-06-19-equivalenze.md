---
title: "Equivalenze"
date: 2021-06-19T11:32:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [BBEdit, Excel] 
---
Mi sono spostato a lavorare al mare, come tutti gli anni. Diversamente da essi, ho portato come me Mac mini e l’ho collegato a un televisore superfluo presente nella casetta. Ho ricostruito così il flusso di lavoro consueto, equamente ripartito tra Mac e iPad.

Sto anche provando qualche piccola variante di flusso, più di scrivania che autostradale. E posso affermare quanto segue:

*Un uso intermedio di BBEdit rende superfluo un uso base di Excel*.

Dopo di che, BBEdit consente di fare un’altra montagna di cose.

(Vale per qualsiasi editor di testo evoluto e per qualunque foglio di calcolo, in realtà).

*Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile.*