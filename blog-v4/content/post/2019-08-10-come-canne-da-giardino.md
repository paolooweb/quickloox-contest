---
title: "Come canne da giardino"
date: 2019-08-10
comments: true
tags: [Unix, McIlroy, pipe, Thompson, Bell, Ritchie]
---
Per la serie *Letture tanto epiche quanto futili adatte al mese di agosto* consiglio la [storia di come è nato il comando Unix `pipe`](https://thenewstack.io/pipe-how-the-system-call-that-ties-unix-together-came-about/).

Pensando al mondo informatico di oggi appaiono come situazioni inconcepibili. Doug McIlroy, di lì a poco responsabile dei laboratori Bell nei quali è nato Unix, che scrive *dovrebbe esserci un sistema di congiungere tra loro i programmi come si fa con le canne da giardino*. Nel 1964.

Nessuno lo ascolta fino al 1973, quando Ken Thompson annuncia *lo faccio io* e, nel racconto di McIlroy, usa metodi prossimi alla magia:

>Non ha fatto esattamente quello che avevo proposto ma ha inventato un sistema leggermente migliore […] Siccome la maggior parte dei comandi più comuni, come `grep` e `cat`, usava ancora la lettura di un file come sistema di input, in una notte li modificò tutti, non so come…

Fu la nascita della moderna filosofia Unix:

>Scrivi programmi che fanno una sola cosa e la fanno bene. Scrivi programmi che lavorano insieme. Scrivi programmi che trattano flussi di testo, perché costituiscono una interfaccia universale.

Lavorare con l’aspettativa che l’output di un programma diventi input per un altro programma. Roba di mezzo secolo fa e contemporaneamente di una attualità e una urgenza che trascurare è un errore grande e forse imperdonabile.
