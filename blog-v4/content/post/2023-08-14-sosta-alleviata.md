---
title: "Sosta alleviata"
date: 2023-08-14T11:48:06+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [CarPlay]
---
Anche se i [dog day](https://earthsky.org/earth/dog-days-of-summer/) propriamente detti sono finiti, sono ancora giorni di (anche auspicabile) lentezza nel dialogo collettivo, ognuno nella sua cameretta a disegnare [alberi di spaghetti](https://www.swissinfo.ch/ita/cultura/pesce-d-aprile-_il-raccolto-degli-spaghetti-svizzeri-resta-fra-i-migliori-scherzi-di-sempre/43069078) con il *paintbot*, perché è originalissimo, perché sviluppa l’ego e perché si può fare.

Per questo mi piace riportare un dettaglio piccolo e assai gradevole scoperto per caso durante una spedizione a caccia di ocra (niente domande, niente bugie): CarPlay imposta sulle Mappe la posizione dell’auto parcheggiata, tutto da solo.

Da [novizio di CarPlay](https://macintelligence.org/posts/2023-07-04-guida-alluso/) mi è piaciuta molto questa cosa e ancora di più la sua totale automatizzazione. La vera intelligenza artificiale non è semplicemente generare un dato, ma elaborarlo in forma compatibile con le nostre abitudini umane e metterlo a disposizione in forma funzionale e comoda.

La posizione dell’auto su Mappe non è inventata né si sposta dopo un po’ con l’aumentare delle interazioni. Così per dire.

Ora inizio a giocare per vincere su [iBrogueCE](https://macintelligence.org/posts/2023-08-06-giochi-senza-tastiere/). Sarà durissima. *No pain, no gain*.

*Gli aggiornamenti in agosto del blog potrebbero essere irregolari o infrequenti.*