---
title: "Qualunque faccia"
date: 2018-01-19
comments: true
tags: [Lidia, Nive]
---
Quaranta mesi fa mi hanno spedito la tessera sanitaria della figlia nata pochi giorni prima.<!--more-->

Nonostante l’aspetto ineccepibile, [era una tessera provvisoria](http://www.apogeonline.com/webzine/2014/09/16/dataset-vs-mindset). Ho dovuto portarla all’Asl per ricevere in cambio un foglio di carta e, forte di quello, attendere la tessera definitiva che è arrivata una manciata di giorni dopo.

In pratica, ho ricevuto due tessere (uguali) per tenerne una e ho passato una mattina all’Asl per ricevere la tessera a casa. Il tutto ben prima che la figlia compisse il primo mese.

Il buonsenso mi fa pensare che nella procedura ci siano alcuni sprechi, di tempo, materiali, passaggi burocratici.

Ieri mi è arrivata la tessera sanitaria della figlia nata da pochi giorni. La figlia è un’altra, sono passati quasi tre anni e mezzo, ma la procedura è rimasta esattamente quella.

Mi dicono che a marzo ci siano le elezioni. Se sento un candidato dire *spediremo la tessera sanitaria dei nuovi nati a casa loro in un solo passaggio senza importunare i genitori che hanno già varie cose a cui pensare*, lo voto qualunque sia la lista, qualunque sia la faccia.