---
title: "Lo Stato in prima persona"
date: 2023-04-20T01:47:33+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [IO]
---
Grandi piani per l’innovazione digitale, che ruotano spesso attorno alla app IO, indicata come tramite per pagare, prenotare, gestire, certificare, dire-fare-baciare.

Ecco la pubblica amministrazione al lavoro per dare alla app tutte le funzioni che cambieranno per sempre il rapporto tra Stato e cittadino.

![IO in funzione su iPhone](/images/io.jpeg "IO nel pieno delle sue funzioni.")