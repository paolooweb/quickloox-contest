---
title: "La classifica degli assist"
date: 2018-03-20
comments: true
tags: [Apple, Hawking]
---
Il passaggio che mette il compagno di squadra in condizione di segnare.

Un dettaglio non raccontatissimo della [vita di Stephen Hawking](https://macintelligence.org/posts/2018-03-16-le-incredibili-coincidenze/) è che il primo computer a fare girare un programma per mantenerlo in condizioni di comunicare con il mondo esterno è stato [un Apple II](http://www.wired.co.uk/article/giving-hawking-a-voice).<!--more-->

Raramente Apple inventa qualcosa e altrettanto raramente entra in un settore per prima.

Ma come i suoi prodotti aprono la strada all’invenzione e alla creatività, nessuna. La classifica degli assist dell’informatica è senza storia.