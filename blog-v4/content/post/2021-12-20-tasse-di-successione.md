---
title: "Tasse di successione"
date: 2021-12-20T01:48:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [Foundation, Joel Podolny, Steve Jobs, Apple, Isaac Asimov, Hari Seldon, Jonathan Ive] 
---
Si sono sprecati i commenti competenti e aspiranti tali sugli effetti della scomparsa di Steve Jobs per Apple. Il dato indubbio è che Apple sia enormemente più grande, diversificata, ricca, intraprendente. Ci sono anche ombre in mezzo alle luci e d’altronde non è che una società dal prodotto interno lordo simile a quello di una nazione medio-piccola possa essere esente da problemi. Tuttavia, Apple dopo Jobs è cresciuta e non poco.

Potrebbe essere merito di Steve Jobs. Non solo per gli effetti del lavoro che non fece in tempo a rendere pubblico, ma per via di Apple University.

Jobs fondò questa attività perché l’identità, gli insegnamenti, la filosofia della sua Apple vivessero anche dopo la sua scomparsa. l’anno scorso è apparso un bell’[articolo sull’organizzazione interna di Apple](https://hbr.org/2020/11/how-apple-is-organized-for-innovation) che ha spiegato molte cose del funzionamento interno della società.

Era cofirmato da Joel Podolny, decano di Apple University, chiamato a occuparsene proprio da Jobs. E Podolny, dopo dodici anni, [ha lasciato la carica](https://www.bloomberg.com/news/articles/2021-12-13/apple-management-training-chief-joel-podolny-leaves-for-startup) per perseguire altri obiettivi.

Su Tv+ di Apple sto guardando [Foundation](https://tv.apple.com/it/show/foundation/umc.cmc.5983fipzqbicvrve6jdfep4x3), serie Tv ispirata al primo ciclo della Fondazione di Isaac Asimov, in cui lo scienziato Hari Seldon dà origine a una Fondazione (niente spoiler…) incaricata di custodire il sapere umano per riportare l’ordine e la civiltà nel tempo più rapido possibile una volta che sarà crollato l’Impero regnante.

Il parallelo è vago, certamente, ma esiste: Apple University intende fare proseguire la società sui giusti binari a prescindere da chi la stia guidando e dalle circostanze esterne.

Al posto di Podolny ora siedono due ex sottoposti, una scelta di continuità come è stato fatto per il design al momento della dipartita di Jonathan Ive. Quindi niente sorprese e niente tagli con il passato.

Il momento cruciale nella successione a Jobs, in realtà, potrebbe essere questo. Tim Cook come amministratore delegato è stato una buona scelta; bisogna vedere ora se Apple University continuerà a funzionare secondo gli insegnamenti e gli scopi che aveva previsto Jobs.

Se la mancanza del talento e delle intuizioni del fondatore sarà o meno un costo per Apple, forse lo scopriremo tra cinque o dieci anni.

*Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile.*