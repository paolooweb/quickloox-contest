---
title: "Caduto in trascrizione"
date: 2019-11-03
comments: true
tags: [Google, Pixel, iPhone]
---
Se ascoltiamo la [voce di Bloomberg](https://www.bloomberg.com/news/articles/2019-10-30/apple-just-killed-the-google-pixel-s-killer-feature-review), notoriamente diversa da quella di un adoratore di Apple a prescindere, impariamo che i nuovi iPhone sono davanti a tutti nella qualità del video, da tempo, e che la serie Pixel di Google risultava migliore di iPhone nelle foto.

Solo che i Pixel 4 di quest’anno hanno perso il vantaggio sulle foto e hanno pure la batteria peggiore. Sempre Bloomberg riferisce che la batteria è la prima priorità dell’acquirente medio, non il prezzo, per cui anche il fatto di costare qualcosa meno vale fino a un certo punto nel valutare l’acquisto di un Pixel.

L’unica cosa che si potrebbe veramente invidiare ai nuovi computer da tasca di Google è la loro funzione di trascrizione automatica dell’audio, gratuita e attiva anche in assenza di rete.

Non comprerei un Pixel 4 solo per quello (altrimenti la funzione sarebbe tutt’altro che gratuita) e devo dare ragione a Bloomberg quando scrive che si tratta di una novità molto interessante, solo che non sposterà in alcun modo le scelte di chi compra.

In pratica anche Google è finita dietro Apple nella competizione per il mercato delle tasche e delle borsette. Qualsiasi discussione sulla scelta tra iOS e Android finisce per ruotare attorno a un unico componente, il prezzo, perché sul resto non c’è più niente da dire o quasi e quel poco che ci sarebbe scompare un pezzo per volta, iterazione dopo iterazione di prodotto.

Anche la classica diatriba *non sono computer, sono telefoni*, perde di significato. Un oggetto che trascrive automaticamente l’audio di una conversazione, da solo e senza aiuti, potrà mai essere un telefono? Insomma, il mondo smartphone è ancora un po’ più scontato e presto ci sarà ben poco da dire di interessante sulle evoluzioni di hardware e software, come già accade per desktop e portatili.
