---
title: "A titolo personale"
date: 2015-03-01
comments: true
tags: [Swift, Playground]
---
Con grande soddisfazione e sonno ridotto al minimo ho messo la parola fine al mio libriccino su Swift, che a questo punto corre seriamente il rischio di andare in stampa tra qualche giorno.<!--more-->

Non sono riuscito a sfruttare pienamente [i nuovi Playground](https://developer.apple.com/swift/blog/) come li hanno aggiornati solo pochi giorni fa. Non è da tutti; per chi ha voglia, c’è da divertirsi. Gran bella invenzione per avvicinare la programmazione a un pubblico il più vasto possibile.
