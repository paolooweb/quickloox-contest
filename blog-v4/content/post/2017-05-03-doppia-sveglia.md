---
title: "Doppia sveglia"
date: 2017-05-03
comments: true
tags: [Mac, Surface, MacBook, iPad, iPhone, Apple, Schnitzler]
---
[Doppio sogno](https://www.adelphi.it/libro/9788845913792) è un gran libro, questo è un piccolo blog. Ma almeno dare una sveglia doppia, si può.

Mi sono svegliato l’altroieri e Microsoft ha aggiornato la sua linea Surface. Linea che finora ha dato risultati non clamorosi e ultimamente in decisa flessione, con calo di oltre il venti percento.

Mi è stato rimproverato di [riportare questo dato](https://macintelligence.org/posts/2017-05-02-pre-verita/) senza tenere in conto l’aggiornamento dell’altroieri. Il quale ammonta a un [portatile con prezzo base di 999 dollari](https://www.cnet.com/news/who-exactly-is-microsofts-new-999-surface-laptop-for/).

Per quanto interessante, difficilmente farà vendere più Surface Book o Surface Studio o Surface Pro.

Ieri mi sono svegliato e Apple aveva diramato i [risultati trimestrali](https://images.apple.com/it/pr/pdf/q2fy17datasum.pdf). Vendite di Mac in crescita del quattro percento, mentre il resto del mercato è in stallo. Sembra strano che in questo scenario la gente abbandoni Mac con numeri mai visti prima come millanta, *pardon*, [sostiene](https://blogs.windows.com/devices/2016/12/12/wonderful-time-year-surface/#bERjFSRUbl1MHS24.97) Microsoft.

Bello sognare, eh. A patto che, quando arriva la realtà, ci trovi svegli.
