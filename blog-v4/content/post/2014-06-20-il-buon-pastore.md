---
title: "Il buon pastore"
date: 2014-06-21
comments: true
tags: [script, shell, Luoma, Markdown, Hazel, Alfred, Html]
---
Il caso: un sacerdote che deve passare regolarmente l’elenco delle letture al direttore del coro e al segretario della chiesa.

La soluzione.<!--more-->

Scorciatoia su [Alfred](http://www.alfredapp.com) per localizzare i passaggi giusti nelle Scritture.

[Script di shell](https://gist.github.com/tjluoma/9ee835f9548de69289ff#file-sundays-revised-sh) per mettere insieme tutte le domeniche da qui alla fine del 2015.

Script che viene preparato in [Markdown](http://daringfireball.net/projects/markdown/) così che [Hazel](http://www.noodlesoft.com/hazel.php) generi automaticamente la [pagina Html sempre aggiornata](http://worship.luo.ma) che costituisce il documento.

Il reverendo Luoma racconta tutta la storia in un [articolo](http://www.tuaw.com/2014/06/09/alfred-dropbox-hazel-and-markdown-all-in-a-days-work/) dove si trova anche una versione più leggibile dello script delle domeniche.

La morale.

Bisogna avere fame, sete, ansia di automazione. Quando facciamo una cosa che poteva fare il computer da solo, è una piccola sconfitta. Siamo umani; manteniamoci al livello superiore che ci compete.