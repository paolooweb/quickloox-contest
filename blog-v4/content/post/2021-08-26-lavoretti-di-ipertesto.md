---
title: "Lavoretti di ipertesto"
date: 2021-08-26T01:14:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [scuola, curricolo, Zork, Avventura nel castello] 
---
La scuola dell’obbligo dovrebbe introdurre alle regole e agli standard di creazione di ipertesto.

La cosa difficile, dopo avere teorizzato, è sempre scendere nel pratico. Non ho il polso di una classe né la sensibilità di chi insegna da anni, quindi è possibile che ecceda in qualche direzione e presenti lavori troppo complicati, troppo semplici, inadatti per insegnare veramente eccetera.

Consapevole che niente è meglio di essere corretti e aiutati a imparare, ci provo lo stesso, in modo disordinato e sequenziale. Penso che sarebbe più corretto inquadrare tutto in una matrice, con le materie canoniche su un asse e la difficoltà del lavoro (quindi anche la fascia di età) sull’altro asse, però ci vuole tempo che mi prenderò più avanti.

Ciò che va fatto è esporre lavori possibili da eseguire in classe, di difficoltà adeguata alla materia tirata in ballo e all’età degli studenti, per tutte le materie.

Per esempio, cose facili, una prima media potrebbe studiare per italiano il funzionamento dell’ipertesto attraverso l’analisi e la mappatura di una avventura testuale come [Avventura nel castello](https://www.erix.it/avventure.html) (giocabile nel browser, anche di un telefono); la stessa cosa, se si usasse [Zork](http://textadventures.co.uk/games/view/5zyoqrsugeopel3ffhz_vq/zork), potrebbe avvenire per inglese (anche qui, forse con un po’ troppa pubblicità, è comunque possibile giocare online). In terza media l’attività potrebbe diventare progettare l’ipertesto di una avventura originale.

Vorrei tirare fuori nei prossimi giorni circa due dozzine di esempi. Chi vorrà fornire feedback di qualsiasi tipo sarà ovviamente benemerito.

*Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile.*