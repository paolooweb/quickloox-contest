---
title: "Professione regia"
date: 2017-02-09
comments: true
tags: [Mac, Live, Trezzo, Adda]
---
**Lorescuba** invia immagine del banco di regia di un [noto locale](http://www.liveclub.it) a ridosso della Milano-Venezia, in zona Trezzo sull’Adda.

Non lo sanno quei tecnici, però Apple li ha abbandonati. Attendiamo notizie dai fabbricanti del *mixer*.

 ![Banco di regia con Mac](/images/live.jpg  "Banco di regia con Mac") 