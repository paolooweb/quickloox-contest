---
title: "Padroni del silicio"
date: 2017-06-19
comments: true
tags: [Moorhead, Amd, Intel, Gpu, Cpu, A10X, iPad, Intrinsity, Imagination, PowerPC, Mac]
---
Come si descrive Patrick Moorhead:

>Prima di dare vita alla mia azienda di ricerche e analisi di mercato, sono stato abbastanza fortunato da passare un decennio a lavorare nei semiconduttori presso Advanced Micro Devices e quasi un altro decennio per fabbricanti di computer e server. Ho incontrato gente molto particolare come Tim Cook in Compaq, Mark Hurd in NCR e Jerry Sanders in AMD.

Moorhead si racconta così nel principio di un articolo fondamentale pubblicato da *Forbes* con il titolo [Il piano di Apple per il dominio del silicio](https://www.forbes.com/sites/patrickmoorhead/2017/06/14/apples-plan-to-dominate-silicon/#285130b02461). L’articolo è fondamentale perché riassume, nelle parole di uno che se ne intende, i passi della strategia Apple che, una acquisizione societaria dopo l’altra, mira ad avere il completo controllo della catena di produzione dei propri prodotti, dai processori alle schede grafiche giù fino ai modem telefonici e ai chip radio per Bluetooth e Wi-Fi.

Ci si ritrova a leggere le tappe di un cammino che dura da dieci anni e, visto da un esperto, è evidentemente programmato e pianificato, con una striscia di successi impressionante che lascia aperte le porte anche alle previsioni più ambiziose. Il fatto più evidente sono i processori A10X di iPad Pro che competono apertamente con le prestazioni dei portatili e sono superiori, o nel peggiore dei casi competitivi, rispetto ai concorrenti diretti delle altre tavolette.

Moorhead prefigura tra un triennio che Apple possa sfoderare oggetti completamente autoprodotti e autoprogettati, in aperta concorrenza con una Intel o con una Nvidia parlando di prestazioni di calcolo e grafica.

Basta un passo falso per andare incontro a un fallimento spettacolare e questa è la parte più suggestiva: c’è chi gioca con regole diverse da quelle cui si conformano quelli che vanno per la strada più comoda. E da dieci anni vince contro ogni previsione (comprese quelle di Moorhead, come ammette nell’articolo).

Trovo suggestivo un altro punto: poco più di dieci anni fa Apple si trovò in difficoltà perché aveva scelto processori PowerPC, all’inizio superiori a quelli di Intel ma poi sempre più problematici nella fascia alta. Era sempre più difficile realizzare Mac competitivi e dal consorzio PowerPC arrivavano solo promesse. Apple organizzò una storica transizione a Intel.

Oggi siamo quasi nella situazione opposta: Apple deve contenersi nel vantare la velocità dei suoi processori su iPhone e iPad, per non imbarazzare Intel e non mettere Mac in cattiva luce. Se decidesse di organizzare domani un’altra transizione, sarebbe doppiamente storica.

Viene la tentazione di pensare che, dopo il passaggio a Intel, qualcuno al timone delle società si sia dato l’obiettivo di affrancarsi dalla dipendenza dai processori di chiunque altro. E che passati tanti anni, quell’obiettivo sia vicino, sia pure in assenza di chi lo ha stabilito.

Poi uno dice che tutti i computer sono uguali. Anche fosse, ci sono storie che meritano molto più di altre.
