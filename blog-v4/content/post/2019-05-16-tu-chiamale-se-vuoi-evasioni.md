---
title: "Tu chiamale se vuoi evasioni"
date: 2019-05-16
comments: true
tags: [Apple, Asymco, Dediu]
---
Nella eccellente [analisi di Horace Dediu sullo stato di Apple e di iPhone](http://www.asymco.com/2019/05/16/the-pivot/) colgo questa perla:

>Dal lancio di iPhone, le vendite di Apple hanno raggiunto 1,918 trilioni di dollari, circa mezzo trilione dei quali si è accumulato sotto forma di reddito.

Di questo mezzo trilione di guadagni, 360 miliardi sono andati agli azionisti e 131 miliardi sono stati pagati in tasse.

Equivale a una tassazione sul reddito del ventisei percento circa.

Si può ovviamente discutere sulla percentuale e se sia etica, appropriata o altro.

Però, quando si parla delle [evasioni o elusioni fiscali di Apple](https://macintelligence.org/posts/2016-08-31-un-posso-falso/), si dovrebbe quanto meno alzare un sopracciglio. O l’analisi di Dediu è numericamente sbagliata, cose che fatico a credere una volta letto l’articolo, o in effetti Apple le tasse le paga. I tredici miliardi chiesti all’Irlanda dalla Commissione Europea sono il dieci percento della cifra di cui sopra, giusto per contestualizzare.
