---
title: "Comodamente insensibile"
date: 2019-05-26
comments: true
tags: [Tevac, kOoLiNuS, Rota, Pink, Floyd, Comfortably, Numb]
---
[Apprendo da kOoLiNuS](https://koolinus.net/blog/2019/05/15/addio-tevac/) della fine di Tevac; per i nuovi del posto, è stato un sito Mac molto frequentato e animato quando andavano di moda i siti Mac.

C’era molta competenza e c’era anche molta passione. Disgraziatamente abbondava l’idea che chiunque potesse scrivere di qualsiasi cosa e ci si attaccava eccessivamente ai *rumors*, le scemate sparate a caso dai siti americani che non sapevano niente delle uscite di Apple e inventavano o distorcevano per guadagnare traffico.

Per lo meno, è come me lo ricordo io. Da molti anni, forse dal nuovo secolo, per me Tevac non è esistito. Provando a recuperare tracce in rete si coglie un appello del fondatore, Roberto Rota, del 2010, lui stufo, il sito stanco, o forse viceversa.

Ho la sensazione, al buio, che sia stato un sito importante per molti, molto tempo fa, e che da un po’ meno tempo contasse poco o nulla, anche per chi lo manteneva in vita.

Con Rota ho avuto pochi scambi e non sempre piacevoli ma ovviamente ho nulla contro la persona; in generale mi dava fastidio, come è sempre successo, l’idea che scrivere di Apple implicasse la mancanza di *fact-checking*. Gli auguro le [migliori fortune](https://robrota.com) personali e professionali.

Quanto a Tevac, il suo spegnimento mi fa venire in mente il titolo del famoso brano dei Pink Floyd, [Comfortably Numb](https://www.youtube.com/watch?v=_FrOQC-zEog). E niente altro.

<iframe width="560" height="315" src="https://www.youtube.com/embed/_FrOQC-zEog" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>