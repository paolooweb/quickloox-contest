---
title: "Prima del New York Times"
date: 2018-01-04
comments: true
tags: [Alphonso, NYT]
---
Mi gioco subito il momento annuale di autoincensamento, per avere [scritto a giugno](https://macintelligence.org/posts/2017-06-05-dillo-ad-alphonso/) quello che *The Old Gray Lady* [ha scoperto a dicembre](https://www.nytimes.com/2017/12/28/business/media/alphonso-app-tracking.html): certe app contengono software che, sia pure senza analizzare il parlato, riescono a identificare via audio parte del contesto in cui vengono usate, a scopo di profilazione pubblicitaria.

Disturba più che altro la presenza di app per bambini, i quali d’altronde non avevano bisogno di attendere le app per essere appetiti dagli inserzionisti.

Ciò detto, sempre leggere la descrizione di una app sullo Store. E ricordare che una app si installa senza avere accesso a microfono o fotocamera; siamo noi a darglielo, su richiesta esplicita.
