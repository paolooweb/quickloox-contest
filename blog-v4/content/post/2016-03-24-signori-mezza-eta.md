---
title: "Signori di mezza età"
date: 2016-03-24
comments: true
tags: [Jobs, Quartz, iPod, iPhone, iPad, iMac, watch, tv]
---
Accennavo al pubblico [ansioso di sentirsi dire quello che vuole sentirsi dire](https://macintelligence.org/posts/2016-03-22-non-peggior-sordo/). Questa tremenda tendenza trova un altro apice, nell’idea che Apple abbia perso la spinta innovativa.<!--more-->

A dimostrazione, un articolo di Quartz ([Apple adesso è noiosa](http://qz.com/644889/apple-is-boring-now/)) che prende le mosse dalla presentazione di iPad Pro 9,7” e iPhone SE per sostenere che Apple *è passata da una netta focalizzazione di prodotto a una azienda che apparentemente produce qualsiasi apparecchio in qualsiasi dimensione e colore*.

Ricordiamoci, parliamo dell’azienda che si è rilanciata quasi vent’anni fa con l’uscita dei primi computer colorati.

Si parla dell’enorme varietà di modelli e combinazioni di watch, definito *il primo prodotto interamente nuovo in cinque anni*, coem se fosse un problema. Da iMac a iPod sono passati quattro anni, da iPod a iPhone sei anni, da iPhone a iPad tre anni, da iPad a watch cinque anni. Siamo così fuori media? Questo, naturalmente, ignorando Mac Pro, per dire, o TV. E se un prodotto interamente nuovo, proprio perché si rivolge a un pubblico diverso in tempi nuovi, potesse avere bisogno di una linea di prodotti più assortita?

>Come i Marvel Studios di Disney, Apple è apparentemente bloccata in un ciclo senza fine di seguiti e di prodotti derivati.

Parliamo dell’azienda che vende iMac dal 1997, seguito dopo seguito, come se iterare il prodotto anno dopo anno fosse una novità.

>Perfino il prossimo quartier generale di Apple è un seguito.

Questo è il segnale che l’argomento è stato stirato veramente all’impossibile pur di cavarne fuori qualcosa, un attimo prima che la corda si spezzi. E infatti:

>qualcuno suggerisce che siccome l’azienda compie quarant’anni, stia entrando nella mezza età.

E non si può credere alla quantità di gente che poi vomita le stesse scemenze su Facebook, alla maniera degli zombi.

Giusto per chiarire sulla mezza età e sulle sue possibilità: quando è uscito iPhone, Steve Jobs doveva compiere cinquantadue anni.
