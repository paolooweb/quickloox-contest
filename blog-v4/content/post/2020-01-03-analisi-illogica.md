---
title: "Analisi illogica"
date: 2020-01-03
comments: true
tags: [Asymco, Apple, Dediu]
---
Assieme agli anni Dieci possiamo lasciarci alle spalle anche gli analisti.

Su *Asymco* Horace Dediu ha chiamato le carte in modo direi definitivo e il *bluff* regge solo per chi ci vuole credere ancora, a tutti i costi (letteralmente: si fanno pagare), chissà perché.

In [Analista, analizzati](http://www.asymco.com/2020/01/02/analyst-analyze-yourself/) Dediu espone la verifica dei pronostici degli analisti sul titolo Apple a dodici mesi, formulate dodici mesi fa, confrontabili oggi con la realtà. Basta una riga:

>Le probabilità di un target di valore corretto sono inferiori di quelle attribuibili al caso.

Gli analisti lavorano *peggio di chi tiri a indovinare*.

La conclusione di Dediu è che sia certo legittimo inserire considerazioni personali o collettive sull’intorno del titolo, a modificare i dati che possono uscire da una analisi razionale e scientifica degli andamenti.

A patto che i risultati siano misurati, che si impari dagli errori, che si abbia il coraggio di esporre una teoria coerente in base al quale si raggiungono margini di errore che straniscono per ampiezza.

Analisti, restate negli anni Dieci e lasciateci in pace. Un invito rivolto anche alle redazioni che ne diffondono i deliri.