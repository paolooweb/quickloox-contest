---
title: "Dal letame nascono regex"
date: 2021-07-07T01:27:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [De André, intelligenza artificiale, Buzz Andersen, Stephen Kleene, Noam Chomsky, Marvin Minsky, regex, Ken Thompson, Bell Labs] 
---
Non potevo astenermi da un articolo che racconta la [storia della creazione delle espressioni regolari](https://whyisthisinteresting.substack.com/p/the-regular-expression-edition) a partire dai tentativi di realizzare la prima intelligenza artificiale.

Intanto, come effetto collaterale, ho trovato un’[indagine approfondita](http://regex.info/blog/2006-09-15/247) sulla genesi di una battuta piuttosto nota:

>Alcuni, di fronte a un problema, pensano “ci sono, userò espressioni regolari”. Ora sono di fronte a due problemi.

Inoltre scopro che le espressioni regolari sono più vecchie di me e non ci avrei creduto:

>In un paper del 1951 scritto per RAND Corporation, Stephen Kleene ragionava sugli schemi che le reti neurali avrebbero potuto riconoscere dentro linguaggi-giocattolo molto semplici detti “linguaggi regolari”. Per esempio: dato un linguaggio la cui grammatica ammette unicamente le lettere A e B, esiste o no una rete neurale che può determinare se una sequenza arbitraria di caratteri è valida per la grammatica A/B? Kleene sviluppò una notazione algebrica per incapsulare queste “grammatiche regolari” (come a\*b* nel caso del nostro linguaggio A/B) ed ecco nata l’espressione regolare.

Passando per Noam Chomsky, Marvin Minsky, Ken Thompson e altri si arriva lettura dopo lettura ai giorni nostri, dove di intelligenza artificiale continua a vedersi al massimo l’intenzione, mentre le espressioni regolari prosperano e sono uno strumento impagabile.

*Dai diamanti non nasce niente, dal letame nascono i fior.*

*Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile.*