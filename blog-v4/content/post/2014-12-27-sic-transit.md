---
title: "Sic transit"
date: 2014-12-29
comments: true
tags: [Samsung, Apple, Store]
---
Il più ambizioso dei [Samsung Experience Store](http://www.samsung.com/uk/retail/what_are_samsung_experience_stores.html?CID=AFL-hq-mul-0813-11000170) inglesi, aperto in aprile, [chiude](http://www.theverge.com/2014/12/24/7446097/samsung-experience-store-westfield-closure) senza neanche attendere la fine delle feste.<!--more-->

Faceva tutto quello che fa un Apple Store e costava anche un po’ meno. Evidentemente non era uguale all’originale.

<blockquote class="twitter-tweet" lang="en"><p>And it&#39;s all over for the flagship <a href="https://twitter.com/hashtag/Samsung?src=hash">#Samsung</a> Experienced store in <a href="https://twitter.com/westfieldstrat">@westfieldstrat</a> <a href="https://twitter.com/hashtag/signofthetimes?src=hash">#signofthetimes</a> <a href="http://t.co/P03eNivsab">pic.twitter.com/P03eNivsab</a></p>&mdash; Najeeb Khan (@najeebster) <a href="https://twitter.com/najeebster/status/547481558471671808">December 23, 2014</a></blockquote> <script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>