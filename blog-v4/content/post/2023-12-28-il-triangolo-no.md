---
title: "Il triangolo no"
date: 2023-12-28T03:03:13+01:00
draft: false
toc: false
comments: true
categories: [Hardware, Software]
tags: [Secure List, TrueType, iOS]
---
A un certo punto la tecnica prende il sopravvento e diventa impossibile seguire (per me) nei dettagli. L’inizio è invece straordinariamente comprensibile, con uno schermino fatto apposta per aiutare le persone normali e narrazione che racconta la situazione invece di scendere nel concreto, per noi umani incomprensibile.

Su _Secure List_ è comparsa la disamina di [Operation Triangulation](https://securelist.com/operation-triangulation-the-last-hardware-mystery/111669/), lo sforzo congiunto di alcuni veterani della sicurezza informatica per documentare, parole loro, *l’attacco hardware più sofisticato che si sia mai visto.*

È inquietante scoprire che l’attacco è progettato contro iOS, ma anche affascinante da un punto di vista forensico, come quando si valutano scientificamente le azioni dei grandi criminali, in cui si veda del genio a prescindere momentaneamente dallo scopo.

Su un iPhone aggiornato e capace di montare il sistema operativo più aggiornato, il problema non sussiste. Altrimenti l’attacco è capace di installare spyware all’insaputa dell’umano.

Fin qui non sarebbe né il primo né l’ultimo; ma questo attacco sfrutta una catena di vulnerabilità inedita per lunghezza e complessità, a partire da un problema (che era) presente nei font TrueType (!) fino ad almeno tre bug nel kernel del sistema, passando per uno in Safari. Il tutto serve a mettere in azione undicimila righe di JavaScript offuscato (illeggibile all’occhio umano), di nuovo, senza che si levi alcun segnale di pericolo.

Saremmo già a qualcosa di speciale, sia pure in un campo dove gli attacchi sono normalità quotidiana per chi si occupa di sicurezza. Gli studiosi autori dell’articolo osservano tuttavia che l’attacco si basa su istruzioni del firmware che *il sistema non usa* e che *non sono documentate*.  Potrebbero essere rimaste negli apparecchi per via di questioni diagnostiche o come rami secchi dello sviluppo; il punto è che non si capisce come possano essere state trovate e ancora meno messe a frutto in modo tanto (criminalmente) geniale.

Apple non ne esce male, intendiamoci, né dobbiamo preoccuparci più di quanto sia normalmente giusto. È una bella testimonianza della sofisticazione che hanno raggiunto le architetture e, di converso, chi ne cerca i punti deboli. Da un certo punto di vista, il fatto che ci sia bisogno di ingegno e risorse fuori dall’ordinario per violare il sistema significa che il sistema stesso ha protezioni ordinarie forti e questo può tranquillizzare.

Si conferma invece che la sicurezza è uno stato di coscienza, oltre che un insieme di protezioni. Abbiamo la responsabilità di evitare cattive compagnie, siti di dubbia reputazione, promesse di facili guadagni o facili regali, sottoboschi, bassifondi, situazioni poco raccomandabili, comportamenti sospetti. Proprio come facciamo nella vita reale con le persone reali nei luoghi reali. La prima regola per non trovarsi nei guai è *to play safe*, stare in sicurezza, e vale anche per il telefono. Questo non basta a eliminare i rischi, ma sapere di avere fatto tutto il possibile vale molto più di una scrollata di spalle finché tutto va bene.