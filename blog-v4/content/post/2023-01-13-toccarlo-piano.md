---
title: "Toccarlo piano"
date: 2023-01-13T00:48:06+01:00
draft: false
toc: false
comments: true
categories: [Hardware]
tags: [Jobs, Steve Jobs, Philip Elmer-DeWitt, Elmer-DeWitt, iPhone, Mac, iPad, Gurman, Mark Gurman, Bloomberg]
---
Il mondo parallelo, quello surreale, vede Apple offrire qualcosa che non offriva e altri offrono da anni, per dire *è in ritardo* e fare battute da commedia all’italiana prima che diventasse *cult*.

In realtà, Apple va per priorità e, se aspetta anni, vuol dire che quella opzione era a bassa priorità e, se viene infine offerta, significa che tutte le cose fatte prima erano più importanti.

Questo per introdurre il tema dei supposti Mac con schermo touch che sarebbero in lavorazione nonostante Jobs abbia detto dodici (dodici!) anni fa una cosa come questa:

>Le superfici da toccare non vogliono essere verticali. Dopo un periodo di tempo sufficientemente lungo, il tuo braccio vuole cascare.

Fosse la prima volta. Quando presentò iPhone, disse che uno stilo non serviva a nulla e oggi [ha una Apple Pencil per iPad perfino](https://macintelligence.org/posts/2022-12-30-le-magie-della-punta/) il sottoscr… mia moglie.

Sempre nel presentare iPhone, Jobs spiegò perché era sbagliata una tastiera hardware per un apparecchio *mobile*. Oggi, sempre per iPad, Apple è felicissima di venderle. Certo, come accessori. Cambia poco.

Ma non è ritardo. È ordine di priorità. È cominciare con l’essenziale (sempre Jobs, all’incirca, non garantisco la letteralità: *bisogna saper dire no a tutto quello che non è essenziale*).

Comunque, ammesso che sia vero, [è sufficiente Philip Elmer-DeWitt a spiegare](https://www.ped30.com/2023/01/12/apple-touchscreen-mac-steve-jobs/) (ovvero, non è più quella discussione complessa e articolata che fu tanti anni fa):

>I Mac fanno di nuovo più soldi degli iPad. Non c’è più una necessità – come nel 2010 quando parlava Jobs – di proteggere il mercato delle tavolette di Apple.

Tra i commenti alla notizia originale (che sta dietro il paywall di Bloomberg), questo è particolarmente illuminante:

>L’articolista afferma che gli schermi touch su Mac non significano la fusione di iPadOS e macOS e quindi resta solo una ragione per averli: facilitare l’uso su Mac di app per iPhone e iPad nel momento in cui fa comodo attivarle.

A toccarla piano, chi è partito da subito con schermi touch sui portatili – e tutte le varianti mutanti del caso – aveva, se va bene, priorità diverse, non necessariamente tra queste il servizio al consumatore; oppure, ha improvvisato e seguito il gregge.