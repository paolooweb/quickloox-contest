---
title: "Cose che nessun altro"
date: 2018-09-17
comments: true
tags: [iPhone, XS, Max, watch, Gruber, Daring, Fireball]
---
Una ragione per stare dietro a *Daring Fireball* è che John Gruber, quando si impegna, scriverà cose impossibili da leggere altrove, e diritte al punto. Come nelle sue [riflessioni e osservazioni](https://daringfireball.net/2018/09/iphone_xs_xr_series_4_apple_watch_event) all’indomani della [presentazione degli iPhone 2018 e di watch Series 4](https://www.apple.com/lae/apple-events/september-2018/).

Un paio di passaggi (che saranno tre, al solito). Su Apple Park:

>Sospetto che Apple voglia essere conservativa nell’uso [dello Steve Jobs Theater] per gli eventi. […] Dubito che mai lo useranno per più di un paio di eventi pubblici l’anno. È straordinario pensare a quanto hanno speso per costruire una struttura che intendono usare solo una manciata di volte ogni anno (contando anche gli eventi interni).

watch:

>Sostengo che i modelli significativi di iPhone siano quello originale, iPhone 4, iPhone 6 e iPhone X. È interessante come l’evoluzione di watch sia stata parallela a quella di iPhone. Una prima generazione come niente prima di essa, buona da cambiare un intero mercato ma difettosa in certi modi ovvi. Seconda e terza generazione che si limitano a risolvere i difetti ovvi. E poi una quarta generazione che porta le cose a un livello totalmente nuovo, in particolare rispetto allo schermo e all’involucro fisico.

iPhone XR:

>iPhone XR [non è] quello che mi aspettavo. Pensavo che sarebbe stato qualcosa meno di un iPhone classe X. Magari con il chip A11 dell’anno scorso. Oppure una fotocamera peggiore. O un Face ID di prima generazione. Niente di questo. Immaginavo che lo avrebbero chiamato iPhone 9 in base al presupposto sbagliato che che sarebbe stato un iPhone di gamma media. Non lo è. È assolutamente degno di essere chiamato un iPhone X.