---
title: "L’arte inconoscibile"
date: 2014-12-15
comments: true
tags: [Flavio, Arnad, Windows, Alpi, museo, Bard]
---
Scrive **Flavio** e mi fa un regalone:

>Ricca selezione di performance Windows raccolta al Museo delle Alpi, Arnad, Aosta.

(Credo si riferisca alla sede del [Forte di Bard](http://www.fortedibard.it/musei-del-forte/museo-delle-alpi)).<!--more-->

Sarà situazionismo? Dadaismo, arte astratta? Ermetismo?

Soprattutto, chi è il responsabile del Museo delle Alpi? E il responsabile tecnico? A chi viene reso conto dei soldi che si spendono?

Anche questa è arte, comunque. Sono io a non capire.

 ![32](/images/trentadue.jpg  "32")

 ![75](/images/settantacinque.jpg  "75")

 ![Senza titolo](/images/senza-titolo.jpg  "Senza titolo")