---
title: "Frecce e setacci"
date: 2020-11-03
comments: true
tags: [Facebook, Apple, Gruber, Daring, Fireball, Windows, Microsoft, Google, Android]
---
L’ennesima prova che il design e l’interfaccia utente non solo suggeriscono che cosa sia meglio comprare, ma dicono anche tante cose su chi li applica male.

[Ne parla John Gruber](https://daringfireball.net/2020/11/disclosing_a_few_thoughts_on_facebooks_ui_design) a proposito della nuova versione della app di Facebook per iOS, con la novità del supporto del *dark mode*.

>Mi sono accorto immediatamente – senza esserne sorpreso – che Facebook ha sbagliato a impostare le frecce che rivelano contenuto gerarchico nascosto. Facebook lo fa nel modo ottuso, come Android, per cui la freccia indica l’azione e non lo stato. iOS funziona nel modo giusto e le sue frecce indicano lo stato, come hanno fatto a partire dal 1984 su Mac.

>Nello stile di Facebook e Android, una freccia che punta in basso va toccata per mostrare contenuto aggiuntivo e una con la punta in alto, invece, va toccata per nasconderlo. Nello stile di iOS e Mac, una freccia che punta a destra (o un triangolo, dipende dal sistema operativo) indica che il contenuto è nascosto e una freccia che punta in basso significa che il contenuto è visibile. Secondo Android, una freccia in basso vuol dire *se la tocchi si apre*; su Mac e iOS, vuol dire *è aperta, se la tocchi si chiude*. Si potrebbe osservare che lo stile di Android non è sbagliato a priori, solo diverso, ma a) sosterrò fino alla fine che le frecce di esposizione del contenuto dovrebbero mostrare lo stato e implicare l’azione (iOS), non mostrare l’azione e implicare lo stato (Android); b) è inequivocabilmente sbagliato per una app iOS, perché tradisce il linguaggio interno della piattaforma.

Il punto b) è soprattutto grave. Se è previsto che le app mostrino frecce per esporre contenuti nascosti, le frecce stesse devono seguire le regole del sistema operativo. Apple dovrebbe rifiutare l’approvazione della app fino a che le frecce non sono a posto.

Se avessi la possibilità di farlo, cestinerei la app di Facebook all’istante. E ricorderei questa dimostrazione della superiorità di Mac e iOS a partire dal design, che è tutto; è *come funziona*. Gruber ha anche parole non del tutto lusinghiere per Windows e la sua interfaccia utente iniziale in tema, ma lascio scoprirle ai più curiosi.