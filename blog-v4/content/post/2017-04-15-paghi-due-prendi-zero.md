---
title: "Paghi due prendi zero"
date: 2017-04-15
comments: true
tags: [Omnigraffle, Omni, iPad]
---
Per un periodo della vita ho avuto l’esigenza di produrre diagrammi di gran qualità in poco tempo e, magari, di doverli modificare all’ultimissimo istante, anche in ascensore appena prima di entrare in ufficio.

Ho fatto le cose per bene con l’acquisto di [Omnigraffle](https://www.omnigroup.com/omnigraffle) in versione Mac e iPad, a un prezzo complessivo da vero software. Sono rimasto veramente soddisfatto. Poche volte soldi spesi altrettanto bene.

Passato qualche anno, oggi Omnigraffle mi tornerebbe utile, solo che la versione iPad 1.9.3 in mio possesso salta per aria appena dopo il lancio. Avendo accettato di spedire a The Omni Group il resoconto dell’errore, ho ricevuto una email di spiegazione: Omnigraffle si appoggia a impalcature Apple tanto cambiate negli anni al punto che, sotto iOS 9, non funziona più. Né basterebbe un aggiornamento, poiché App Store non accetta più *app* basate su quelle impalcature lì.

La soluzione? Scaricare [Omnigraffle 2.0](https://www.omnigroup.com/blog/introducing-omnigraffle-2-for-ipad). Versione la quale, oltre che riportare la compatibilità, dispone di un ragguardevole set di funzioni Pro acquistabili *in-app.*

La questione è che Omnigraffle 2 va acquistato e costa persino più della versione 1 comprata a suo tempo. Le funzioni Pro costano come l’applicazione base. In pratica, se volessi la nuova *app* con il massimo delle funzioni a disposizione, mi costerebbe più del doppio dell’originale.

Come acquirente della versione 1, ho un privilegio: aggiornare *in-app* alle nuove funzioni Pro è gratis. Pagherei insomma Omnigraffle 2 solo qualcosa più di quanto mi è costato Omnigraffle 1.

Però qualcosa non torna: per avere Omnigraffle 2 base, finisco per pagare due volte il prezzo (una volta per la versione 1, una volta per la versione 2). Uno nuovo utilizzatore che arrivasse domattina lo pagherebbe una volta sola. Questo non mi tutela come utente di lunga data. A saperlo, mi conveniva rimandare l’acquisto a ora invece che effettuarlo anni fa.

Se voglio acquistare Omnigraffle 2 con le funzioni Pro, pago circa due volte, alla pari con chi arriva domattina. E ancora una volta non ho vantaggi come utente di lungo corso.

Se sto come sono, mi ritrovo senza il programma pur avendolo pagato una volta. Chi arrivasse domattina senza avere il programma, comunque non avrebbe pagato per farne a meno.

Non mi pare una politica di aggiornamento rispettosa di chi ha investito fiducia nella versione 1.
