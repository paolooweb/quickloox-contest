---
title: "Anno nuovo, Mac nuovo"
date: 2022-01-07T00:20:22+01:00
draft: false
toc: false
comments: true
categories:
tags:
---
Dovevo accennare a questa novità. Apparentemente c’è poco da dire: ho preso un [Mac mini M1](https://www.apple.com/it/shop/buy-mac/mac-mini/chip-apple-m1-con-cpu-8-core-e-gpu-8-core-512gb) con sedici gigabyte di memoria e un terabyte di disco, per sostituire un Mac mini Intel i3 con la stessa memoria ma la metà dello spazio. Un i3 funzionava e funziona a meraviglia, ma è bastato un minimo incremento delle necessità di grafica e video per metterlo in crisi. Mi ha fatto sballare l’inizio di una presentazione in diretta e, quando ho premuto il tasto per avviare Keynote, lui ha iniziato a fare cose sue, rotella, ventole sbuffanti e scarsa risposta sul multitasking.

La presentazione è partita tipo un minuto dopo che avevo premuto il tasto. Presentazione che avevo provato ed era partita a dovere. Bisognava cambiare, anche se avrei preferito attendere e prendere un eventuale futuro mini con M1 Pro o Max. Però sono sempre in tempo a rivendere, magari annunciano a fine 2022, non è neanche detto che mi serva un M1 Max. Certamente non potevo attendere.

Il vecchio mini servirà alla primogenita l’anno prossimo, immagino. Sul momento, gli ho appoggiato sopra quello nuovo, dopo di che ho staccato tutti i cavi sotto e li ho attaccati sopra. Tutto esattamente come prima, perché M1 ha una porta Usb-C in meno, che però non usavo. Ora ho semplicemente tutte le porte occupate (salvo Ethernet) mentre prima ne avanzava una.

Usare il mini nuovo è stato ritornare a usare un Mac propriamente detto. Risposta istantanea, prestazioni sopra ogni sospetto, tranquillità. Ovviamente sono riuscito a tirargli il collo come prova (l’ho fatto con qualsiasi Mac abbia posseduto), ma il livello dell’insostenibilità è ben lontano dall’uso normale quotidiano. Con l’i3, dovevo chiudere tutto il chiudibile se si annunciava una videoconferenza o una sessione su [Miro](https://miro.com/). Inaccettabile.

La vera differenza è che il mini Intel sbuffava, soffiava, scaldava. Questo, zero. A volte lo si avverte impercettibilmente più caldo della temperatura naturale della superficie, esattamente la stessa sensazione che talvolta provo con iPad Pro. È questo il vero progresso ed è sbalorditivo. Prima stavo molto attento a non appoggiare niente sopra il mini i3, per non ostacolare la dispersione del calore. Il mini stesso si appoggiava su un piattino da caffè capovolto, un piccolo trucco per agevolare il flusso dell’aria anche sotto l’unità.

Ora, se chiedessi a una persona ignara e il monitor fosse in standby, immagino che toccherebbe il Mac e mi direbbe che è spento. Io so che non è così e psicologicamente percepisco ogni minima variazione della temperatura del telaio. Sospetto che, senza la psicologia, ci cascherei anch’io. Avere sulla scrivania un computer che è acceso – lavorando non poco – e sembra spento fa pensare. Penso non mi sia successo in trent’anni, fatta eccezione per Newton.

A chi si lamenta della carenza di innovazione di Apple, posso solo dire che effettivamente il mio nuovo mini è fisicamente identico e sovrapponibile al mio vecchio mini. Ma uno è caldo, l’altro è freddo e, conseguentemente, del tutto silenzioso. E fa tutta la differenza del mondo.

Non c’è altro da dire; un Mac mini non è fatto per strappare grida di meraviglia o sensazioni estatiche. No, una cosa ancora; posso autenticarmi con watch.

Invece che digitare la password, faccio un doppio clic sul pulsante laterale di watch. Tutte le volte che succede, guadagno un giorno di benessere. In più mi sembra di avere di fianco Craig Federighi che commenta *How cool is that?*.

Questa sì, è meraviglia. Qualità della vita a mille.

*Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile.*
