---
title: "Non è questo il giorno"
date: 2022-10-19T16:12:20+01:00
draft: false
toc: false
comments: true
categories: [Hardware, Software]
tags: [iPad, iPad Pro, iPadOS, iPadOS 16, Stage Manager, Apple Pencil, M2, M1]
---
Anni fa mi sono trovato senza Mac per alcune settimane e mi sono arrangiato a lavorare con iPad. Non iPad Pro, iPad. Di terza generazione, anno 2012.

In alcuni momenti è stata dura, in altri tranquilla. Ce l’ho fatta.

Quest’estate mi sono trovato senza iPad Pro e mi sono chiesto se prendere subito una macchina nuova oppure attendere con pazienza l’uscita annuale del nuovo modello.

Certamente ho lavorato su Mac. Si lavora splendidamente su Mac. Essendo un Mac mini, tuttavia, mi trovavo in situazioni dove… ero senza iPad Pro. Clienti, viaggi, la notte a letto eccetera. Non potevo lavorare.

Così, in base al mio principio che si prende quello che serve quando serve, [ho cambiato il mio iPad Pro 2018 con un 2021](https://macintelligence.org/posts/2022-09-25-uelcom-tu-itali/).

Ora che è uscito il 2022, lo trovo una macchina davvero stupenda… ma non mi serve.

Non sono un grafico, quindi i progressi dello schermo mi appaiono relativi (lo schermo 2021 è percepibilmente meglio dello schermo 2018).

Non disegno né prendo appunti a mano, per cui non sono interessato alle novità di Apple Pencil (proprio bella la funzione di hover che mostra dove si poserà la punta).

Il Wi-Fi di casa ragiona ovviamente sulla compatibilità con la macchina più vecchia regolarmente usata, ergo le migliorie del nuovo iPad Pro in materia resterebbero inutilizzate.

Certo, se avessi avuto in mano il 2018 avrei ragionato allo stesso modo, ma con un occhio in più alle differenze di prestazioni e di schermo. Adesso ho un iPad M1; confrontato con un iPad M2, per come ci lavoro io, va benissimo. Dello schermo ho già detto.

Potrei naturalmente permutare il vecchio, usato per meno di un mese e in condizioni perfette, con un nuovo; il nuovo però, a parità di configurazione, parte da duecento euro in più. Probabilmente potrei trovarmi a sborsare quattrocento-cinquecento euro per il processore superiore, che lo è marginalmente; lo schermo superiore, pure; le nuove funzioni di Apple Pencil, che non uso; il nuovo Wi-Fi, che non uso.

Se guardo quattrocento euro, sto guardando gran parte di un iPad semplice per una figlia, magari. Non l’upgrade della macchina che uso con totale soddisfazione da tre settimane e poco più.

Ecco perché sono fortemente orientato a seguire comunque il mio principio. Quello che mi serve ce l’ho e prenderò dell’altro quando mi servirà, non perché esce qualcosa.

Si aggiunga che le notizie sullo stato dell’arte di Stage Manager non sembrano proprio confortanti rispetto a [quanto si commentava questa estate](https://macintelligence.org/posts/2022-06-26-lo-stage-posticipato/).

Al limite, uno Stage Manager superperformante ma fortemente desideroso di un M2 potrebbe essere una leva che spinge verso il nuovo iPad Pro.

Anche qui, però, il mio uso di iPad Pro è in mobilità. Se mi siedo, ho davanti il monitor di Mac con otto milioni di pixel. È più che abbastanza. Il supporto dei monitor esterni non è un requisito.

Il mio me stesso di luglio considererebbe iPad Pro M2 con curiosità e effettivamente qualche interesse.

Il mio me stesso di settembre attenderà quasi sicuramente di avere di nuovo bisogno di cambiare iPad Pro. [Non è questo il giorno](https://macintelligence.org/posts/2022-08-11-datemi-una-data/).