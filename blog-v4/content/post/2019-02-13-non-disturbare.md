---
title: "Non disturbare"
date: 2019-02-13
comments: true
tags: [Gizmodo, Amazon, Apple, Facebook, Microsoft, Google, Hill]
---
Kashmir Hill, reporter di *Gizmodo*, ha vissuto per un breve periodo bloccando tutti i servizi di una grande multinazionale a scelta tra Amazon, Apple, Facebook, Google e Microsoft. Niente browser, software, sistema operativo, servizi di messaggistica, ricerche, niente di niente. Poi ha fatto l’esperienza definitiva: [bloccarli tutti insieme](https://collegiali.slack.com/archives/CEP3NSS68/p1550072024003200).

*È un inferno*, riporta, a maggior ragione in quanto ha scelto la strada più radicale possibile: bloccare non solo prodotti e servizi delle multinazionali, ma anche *tutto il traffico in transito dai loro server*. Per esempio, niente DuckDuckGo, perché si appoggia al cloud di Amazon. Lo stesso vale per Dropbox.

Si è fatta configurare appositamente la rete di casa e il blocco del traffico le ha permesso di sperimentare la reale influenza delle *Big Five* su Internet, anche sotto forma di numeri.

Il suo racconto è da leggere e una considerazione emerge sopra tutte: la porzione di Internet estranea alle Big Five é misera. I siti che si appoggiano a un cloud sono innumerevoli e riuscire a soddisfare tutte le proprie esigenze senza compromessi è praticamente impossibile. E poi, questa frase:

>Ai critici delle grandi aziende di informatica viene spesso risposto *Se non ti piace l’azienda, fai a meno dei suoi prodotti*. Ho condotto questo esperimento per scoprire se sia possibile e ho scoperto che non lo è; con l’eccezione di Apple.

Hill ha contato quante volte in una settimana la sua rete di casa è stata contattata dai servizi delle Big Five. I risultati sono pazzeschi: più di novantacinquemila volte da Amazon, più di quarantamila volte da Google.

Più di duemila volte da Facebook, più di mille volte da Microsoft.

Trentasei volte da Apple.

Quando ci si chiede come sia diversa dalle altre, c’è adesso un’altra risposta oltre alle molte già note: Apple si impegna a non disturbare se non quando strettamente necessario. Il grafico dei tentativi di contatto dovrebbe essere appeso in tutti i Media World italiani.
