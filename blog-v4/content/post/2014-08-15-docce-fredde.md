---
title: "Docce fredde"
date: 2014-08-16
comments: true
tags: [Apple, Microsoft, Facebook, Zuckerberg, Cook, Nadella, Schiller]
---
Apple è come altre aziende, quando si tratta di farsi versare in testa una secchiata di acqua gelida. Lo ha fatto [Phil Schiller](https://twitter.com/pschiller/status/499751668414566401) e lo ha fatto [Tim Cook](https://www.youtube.com/watch?v=uk-JADHkHlI), così come lo hanno fatto [Larry Page e Sergey Brin di Google](https://www.youtube.com/watch?v=D9QYepYfGxo), [Mark Zuckerberg di Facebook](http://www.theverge.com/2014/8/14/6001275/mark-zuckerberg-takes-ice-bucket-challenge), [Dick Costolo di Twitter](http://www.theverge.com/tech/2014/8/14/6001811/twitter-ceo-dick-costolo-ice-bucket-challenge) e [Satya Nadella di Microsoft](https://www.youtube.com/watch?v=3YDxB6hXWYc).<!--more-->

<blockquote class="twitter-tweet" lang="en"><p>Phil takes challenge! Challenges Chris, Kim, and Tim.&#10;<a href="https://twitter.com/hashtag/IceBucketChallenge?src=hash">#IceBucketChallenge</a> <a href="https://twitter.com/PeteFrates3">@PeteFrates3</a> <a href="https://twitter.com/tim_cook">@tim_cook</a> <a href="https://twitter.com/chrisodonnell">@chrisodonnell</a> <a href="https://twitter.com/hashtag/ALS?src=hash">#ALS</a> <a href="http://t.co/DfJIuY3hnb">pic.twitter.com/DfJIuY3hnb</a></p>&mdash; Philip Schiller (@pschiller) <a href="https://twitter.com/pschiller/statuses/499751668414566401">August 14, 2014</a></blockquote> <script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>

La secchiata fa parte di una [campagna](http://www.alsa.org/news/archive/als-ice-bucket-challenge.html) per il finanziamento della ricerca sulla sclerosi laterale amiotrofica e dà il diritto di designare pubblicamente altre tre persone, che possono scegliere entro ventiquattro ore tra la secchiata e una donazione alla causa (molti personaggi in vista fanno entrambe le cose).

La campagna pare stia andando bene, con quattro milioni di dollari raccolti in due settimane, quasi quattro volte la somma dello stesso periodo del 2013 e tante partecipazioni da personalità che arrivano anche dalla [politica](https://www.facebook.com/photo.php?v=10154504678515389) e dallo [spettacolo](http://www.bostonmagazine.com/arts-entertainment/blog/2014/08/12/justin-timberlake-boston-marathon-survivors-als-ice-bucket-challenge/).

Provare a immaginarsi sotto la doccia fredda un italiano in prima fila tra imprenditoria, politica o spettacolo.

<iframe width="560" height="315" src="//www.youtube.com/embed/uk-JADHkHlI" frameborder="0" allowfullscreen></iframe>