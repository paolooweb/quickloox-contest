---
title: "Corri corri gamberetto"
date: 2016-10-07
comments: true
tags: [Pixel, Google, iPhone, SE]
---
Google ha annunciato con grande fanfara la nuova linea di computer da tasca [Pixel](https://madeby.google.com/phone/).<!--more-->

*AppleWorld.Today* [fa notare](http://www.appleworld.today/blog/2016/10/5/iphone-7-performance-leaves-google-pixel-far-behind) che, test di velocità alla mano, un iPhone 6s Plus supera a mani basse un Pixel.

Ops. Pixel è uscito ieri, iPhone 6s Plus tredici mesi fa. iPhone 7 ha [prestazioni largamente superiori a quelle di iPhone 6s Plus](https://macintelligence.org/posts/2016-09-20-con-permesso/).

I test puri di velocità significano poco, eh. Specialmente se tra i contendenti le differenze sono minime. Quando uno vale una volta e mezza l’altro, meritano un minimo di considerazione.

Persino un iPhone SE, quello meno costoso di tutti, supera nei test di velocità pura un Google Pixel.

All’opposto, Google sta rimontando e punta al pareggio con Apple in una caratteristica decisiva: il prezzo.

Aspettiamo il mitico confronto tra caratteristiche dove verrà fatto spezzatino di componenti, dando valore zero all’integrazione e al design, per dire tutto e il suo contrario. Tralasciando, per coincidenza, i test di velocità effettiva.