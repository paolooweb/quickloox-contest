---
title: "Il fattore umano"
date: 2020-11-29
comments: true
tags: [M1, mini, Mac, ]
---
Sì le specifiche, sì i benchmark, sì i *big data*. Rimane che nel computing sano di cui auspichiamo la diffusione, il focus sono le persone. Se avessi un euro per tutti quelli che si vantano della Ram che hanno installato sarei a bloggare a bordo piscina dai Caraibi; se lo avessi per quelli che dicono *mi ha cambiato la vita* (basterebbe anche un po’ di lavoro o di intrattenimento), al massimo sarei un po’ più abbonato a Netflix.

Segue un messaggio spontaneo ed estemporaneo di [Matteo](https://nrs.substack.com) (sostenere, diffondere, apprezzare please). Matteo è uno specialista di podcast, si occupa di [letteratura indipendente](https://rivista.inutile.eu), insomma è un alieno rispetto a noi, che passiamo le giornate a mixare musica a duecentocinquantasei tracce con mille plugin, a montare video 6k o forse 8k oppure 16k, renderizzare l’Antartide in scala 1:1, ritoccare foto a millemila [yottapixel](https://en.wikipedia.org/wiki/Yottabyte). Lui usa il computer per fare cose, per lavorare, per divertirsi, fa parte della sua vita quotidiana anche se non scriverà mai *professionista* in qualche post per lamentarsi della mancanza di innovazione adesso che non c’è più lui. Ecco che cosa mi ha scritto:

>Ho fatto questa cosa qui. Ho preso su Amazon il Mac mini M1, modello base (256, 8 giga). Ho detto: vedo un po’ come funziona, se ci girano i vari plugin audio che uso per lavorare, com’è ‘sta Rosetta, quelle robe lì. Poi lo rendo, ché Amazon rende molto facili i resi, facilissimi, e se mi convince mi prendo la versione che penso sia più adatta (1 tera, 16 giga). Il Mini mi ha spettinato, da quant’è potente. Al punto tale da farmi fermare e dire: ma se han tirato fuori ‘sta bestia ed è solo per la fascia medio bassa, che tirano fuori tra tre o quattro mesi? Così ho reso il Mini e aspetto ancora un po’, ma che macchina. Quasi mi è venuta voglia di tenerla, pur con un SSD così limitato!

Tutto qui. È mai successo nella storia, di sentire un commento così da una persona normale, a proposito di un modello base? Perché a me non è mai successo e mi fa capire che M1 è una bella cosa perché le sue doti sono, come dire, parte del pacchetto; finiscono per mettere in primo piano chi lo usa. Quando mi vanti i tuoi sestantonove gigabyte di Ram, in primo piano vedo solo banchi di memoria.