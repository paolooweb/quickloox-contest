---
title: "Un attimo di follia"
date: 2023-09-27T19:49:47+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [macOS, iPadOS, iOS, watchOS, tvOS]
---
Mai aggiornare tutti gli apparecchi tutti insieme. Se andasse male qualcosa, potrebbe non essere disponibile una scappatoia o una piattaforma dalla quale ripartire.

Invece l’ho fatto. In una pausa pranzo ho lanciato gli aggiornamenti di tutto. Mac, iPad, iPhone, watch, tvOS.

Un mito da sfatare è che si consumi molto tempo. Era anche vero una volta; nella pausa propriamente detta avevo terminato gli aggiornamenti di Mac, iPad e iPhone. watchOS è arrivato dopo perché aspettava l’aggiornamento di iPhone; tvOS è arrivato dopo perché pensavo che scaricasse il software intanto che guardavo [Invasion](https://www.apple.com/it/tv-pr/originals/invasion/episodes-images/) e invece no. Per le 15 comunque era tutto finito, cinque apparecchi aggiornati nel giro di due ore.

Il momento di scrivere recensioni è ancora lontano. Mi limito a poche osservazioni del primissimo momento.

Su Mac, il salvaschermo/sfondo scrivania dinamico di Sonoma è un bel vedere. Per il momento ho sospeso la mia tipica esecuzione casuale. Il controllo nella barra dei menu della telecamera in funzione è interessante, anche se mi chiedo perché le [Reazioni](https://www.reddit.com/r/MacOSBeta/comments/166n8ec/all_the_sonoma_reactions_hand_gestures/) siano attive per default. Significa che uno può ritrovarsi con i cuoricini o il temporale durante una chiamata con i capi o con il cliente, perché ha alzato i due pollici senza pensarci o altro. Sono dettagli comunque, i progressi nella videoconferenza mi piacciono perché aspetto di poter proporre efficacemente un ambiente di chiacchiera e collaborazione basato su FaceTime e FreeForm e che ci sia lavoro in quella direzione comunque conferma l’attenzione, per cui si vedrà altro lavoro.

iPad. Stage Manager mi pare migliorato e più stabile. Aneddoticamente: c’è un Comando rapido semplice che lancio alcune volte al giorno. Di sicuro si esegue più velocemente del cinquanta percento o più. Non significa che sia un guadagno di tutti i Comandi rapidi né del sistema in generale. Fa piacere comunque.

iPhone mi sembra quello di prima, non ho notato niente di particolare. D’altronde lo uso poco.

watch è cambiato molto e mi piace molto quello che è cambiato. Le viste sono quasi sempre migliori, meglio disegnate, oppure più funzionali. Le notifiche sono decisamente migliori, la velocità pare leggermente superiore. Ho da lamentarmi solo di una cosa: le Mappe su watch mostravano l’itinerario svolta per svolta e il tempo stimato di arrivo. Ora ci sono tre viste diverse ma il tempo di arrivo stimato si vede solo nella terza, incidentalmente la più povera di informazioni. A parte questo, sono molto, molto soddisfatto. Aggiornamento raccomandato.

tvOS si è raffinato. Ora dispone di una sorta di Centro di controllo come iPhone e iPad e qua e là si vedono tocchi di miglioramento, anche minimo, ma presente.

Se invece della pausa pranzo avessi scelto ora di cena, avrei riavuto tutti i sistemi in funzione e a posto prima di andare a dormire. Aggiornare costa meno tempo, è tranquillo, Mac e iPad si sono riavviati tipo tre o quattro volte, ma non hanno dato alcun problema.

Mi raccomando sempre, però: mai aggiornare tutti i sistemi contemporaneamente.