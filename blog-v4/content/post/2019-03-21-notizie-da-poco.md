---
title: "Notizie da poco"
date: 2019-03-21
comments: true
tags: [Times, WSJ, Apple, News, Journal, NYT]
---
L’editoria e specialmente quella quotidiana deve ancora trovare un modo per esistere nella sua forma attualmente conosciuta nonostante Internet. È il motivo per il quale Apple pensa di offrire Apple News come canale di distribuzione che aiuti gli editori a monetizzare; è ugualmente la ragione per cui propone agli editori di dividere gli incassi a metà, 50/50.

Il *New York Times* ha fatto sapere che [non gli passa neanche per l’anticamera del cervello](https://www.nytimes.com/reuters/2019/03/21/business/21reuters-media-new-york-times.html), mentre il *Wall Street Journal* si sarebbe accordato. Al *Times* quasi non ne hanno bisogno; il loro obiettivo di fatturato digitale annuo è di ottocento milioni di dollari entro il 2020 e attualmente stanno a settecento milioni; possono farcela, così come potrebbero raggiungere l’obiettivo di dieci milioni di iscritti online a pagamento entro il 2025. Al *Journal* probabilmente pensano che peggio di così non possa andare e anche passare ad Apple metà dei ricavi potrebbe diventare interessante, dato che l’altra metà oggi come oggi manco la vedono, divorata da Google e Facebook.

Parliamo di corazzate editoriali; al *Times* contano millecinquecentocinquanta giornalisti, per dire. E penso che il *busillis* sia questo.

Apple News deve chiaramente partire con il botto e, come da tradizione, essere comunque un servizio almeno autosufficiente a prescindere da quante centinaia di miliardi di *cash* si trovi a totalizzare il capo contabile Luca Maestri. Questo spiega i grandi nomi e lo *share* pretenzioso dei ricavi.

A lungo andare, tuttavia, Apple News interessa i piccoli. Il *Times* ha tutto l’interesse a fare da solo e i soldi per poterci provare. Nel mondo esiste qualche decina di quotidiani nelle stesse condizioni e, sotto questa soglia, i soldi per poterci provare non ci sono, oppure non sussiste la capacità di prendersi rischi importanti in nome di obiettivi ambiziosi.

A tutti costoro viene naturale la constatazione di avere niente da perdere e qualcosa da guadagnare. Anzi, più si è piccoli e più diventa interessante: metti che un domani bastasse essere iscritti da *blogger* ad Apple News per ricavarne automaticamente, che so, tre euro al mese. Per iscriverti hai probabilmente dovuto buttare via un’ora o due, dopo di che paghi una bolletta, magari lo *hosting*, non ti cambia niente, non infliggi pubblicità ai lettori e forse ti regali una pizza. *Perché no?*, potrebbe pensare l’universo.

Scrivo nell’imminenza della ventilata presentazione del nuovo Apple News ed è probabile che questo scenario neanche esista, nella presentazione suddetta. Eppure, se il servizio sopravvive cinque anni, troverà lì la vera ragione di esistere e supongo anche i ricavi veri. Potrebbe anche esser l’occasione di riprovare a democratizzare il web dal punto di vista dell’informazione, obiettivo utopico che a oggi è stato chiaramente e clamorosamente mancato. Ancora una volta, come nel caso di Apple Music, sarebbe Cupertino a dare la linea verso una rete più umana e meno algoritmica. L’informazione "vinta" da chi la fa per passione e con convinzione, con serietà ragionevolmente garantita da Apple, guadagnandoci poco invece di niente e aggirando i colossi multimiliardari con i loro prodotti in scatola.

E i nostri quotidiani? Quelli che la notizia la intravedi tra i gattini, le sfilate di moda, il circo Barnum, il gusto dell’orrido, la fake news involontaria, la velina di partito, la foto disgustosa/morbosa/*softcore*? Una adesione ad Apple News potrebbe essere un bel pretesto per ripulirsi. Per questo non ci scommetto.
