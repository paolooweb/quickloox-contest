---
title: "La pena del contrappasso"
date: 2013-06-22
comments: true
tags: [iPad, Apple, Microsoft]
---
Il dettaglio più incredibile della storia degli [iPad che andranno agli studenti delle scuole di Los Angeles](http://macintelligence.org/blog/2013-06-22-la-pena-del-contrappasso) è che, stando al [Los Angeles Times](http://www.latimes.com/news/local/la-me-0619-lausd-20130619,0,3194906.story),

>Un rappresentante di Microsoft ha sollecitato il consiglio [direttivo delle scuole di Los Angeles] a provare più di un prodotto e non appoggiarsi a una [sola] piattaforma. Questo potrebbe escludere il distretto da future innovazioni e riduzioni di prezzo.<!--more-->

Se questi sono gli argomenti per mettere da parte iPad, sono debolucci. *In futuro potrebbe arrivare innovazione o un prezzo minore…* mi pare che si possa applicare a qualsiasi prodotto su qualunque mercato. Più una ammissione evidente che per il presente si ha niente da offrire.

Parlando di cose serie, invece, questo è un periodo luminoso per la biodiversità informatica. Posso avere un Mac a casa, un Pc in ufficio, un server Linux, accendere qualsiasi macchina virtuale su una infrastruttura cloud senza neanche sapere che sistema operativo c’è sopra. Posso avere un telefono Android e un tablet iOS, e fare parlare tutto con quanto descritto sopra.

Potrebbe essere ancora più luminoso di così. Solo che ci siamo sobbarcati vent’anni di secolo buio in cui una qualsiasi alternativa a Windows andava sudata e guadagnata a fatica, sul lavoro spesso osteggiata se non proibita.

Dovrebbe esserci una Norimberga per quelli che da Microsoft si azzardano a inneggiare alla diversità. Prima passino vent’anni a usare qualsiasi cosa non sia Windows, per giusto contrappasso. Poi se ne riparla.