---
title: "Delusioni a raffica"
date: 2014-01-28
comments: true
tags: [OSX, iOS]
---
È il mese dei miti infranti.

Tante chiacchiere da bar investite nel dire che iOS e OS X diventano una cosa sola, che si fondono insieme, che nascerà un sistema operativo unico, tanti *post* sui blog della speranza (che i fessi clicchino).

Poi due pesi massimi come Phil Schiller (marketing) e Craig Federighi (sistemi operativi) rovinano la festa e spiegano per esteso e in chiaro che [non se ne parla proprio](http://www.macworld.com/article/2090829/apple-executives-on-the-mac-at-30-the-mac-keeps-going-forever.html). Federighi:

>Così iOS e OS X dovrebbero essere la stessa cosa, indipendentemente dal loro scopo? Facciamoli convergere, per il gusto della convergenza? È assolutamente un non-obiettivo.

Schiller:

>Non sprechiamo il tempo a pensare che l’interfaccia dovrebbe essere una sola e come fare a fondere questi sistemi operativi. Sarebbe buttare via energie.

Chissà, magari per una settimana o due la realtà riesce a prevalere.