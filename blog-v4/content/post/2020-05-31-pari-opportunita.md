---
title: "Pari opportunità"
date: 2020-05-31
comments: true
tags: [Drang, Mac, iPad]
---
Le mie conoscenze di scripting o programmazione sono ben poca cosa se confrontate con il sapere di Dr. Drang.

C’è un _però_.

Negli ultimi quattro mani, scrive lui a proposito di [sistemi per automatizzare il salvataggio in iCloud Drive di documenti](https://leancrew.com/all-this/2020/05/drafts-and-icloud-drive/) scritti su iPad con [Drafts](https://getdrafts.com), è passato da _Mac-only_ (solo Mac) a _Mac-mainly_ (principalmente Mac) fino a diventare _iPad-mainly_ (principalmente iPad).

Ho cominciato esattamente la stessa transizione ma nel 2010. Sei anni di anticipo.

Se lui mi ha raggiunto nell'uso di iPad, posso farlo anch’io in quello di Python. C’è speranza.

(Per inciso, l’articolo – non è una novità – è piuttosto interessante).