---
title: "Viaggi allucinanti"
date: 2023-09-05T02:42:29+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [TechCrunch, Ai, Intelligenza artificiale, Llm]
---
Un articolo di *TechCrunch* si chiede se [i modelli di intelligenza artificiale siano destinati ad avere allucinazioni per sempre](https://techcrunch.com/2023/09/04/are-language-models-doomed-to-always-hallucinate/). Un bel clickbait, perché la domanda nel titolo è pleonastica e l’articolo sarà, eventualmente, di aiuto ai soli che ignorano la natura dei sistemi generativi basati su grandi modelli testuali (Llm) e pensano a qualche immaginifica versione due punto zero che miracolosamente cambi la natura stessa del meccanismo. Conosciuta molto bene, peraltro, dall’autore:

>I modelli di intelligenza artificiale generativa non possiedono una vera intelligenza; sono sistemi statistici che predicono parole, immagini, parlato, musica o altri dati.

Lo stesso concetto di *allucinazione* è truffaldino quanto quello di intelligenza artificiale applicato agli Llm. Allucinazione è prendere per vera una cosa che non lo è o viceversa, il che implica la conoscenza delle nozioni di *vero* e *falso*. Il software non la ha e non può avere allucinazioni. Semplicemente, scrivere *due più due fa quattro* e *due più due fa cinque* è per la macchina totalmente equivalente.

A meno che… l’articolo spiega correttamente che si possono ridurre le *allucinazioni* tramite il cosiddetto *apprendimento per rinforzo*. Spiegato un po’ alla brace, si assolda una truppa di poveracci in un Paese povero, basta che sappiano l’inglese, e per pochi soldi li si mettono a integrare il modello così che faccia meno errori. Così, nei Paesi ricchi, gli sprovveduti possono stupirsi, emozionarsi, chiedersi *ma come fa* e avviare dibattiti su come funziona il nostro cervello, metti mai che si sia capito come nasce il pensiero. No, nella nostra testa non ci sono omini a sistemare un modello testuale. No, non scegliamo parole a caso, pur pesato. I prodigi delle *intelligenze artificiali* si devono alla fatica degli sfortunati naturali, nati nel posto sbagliato, che da piccoli cucivano palloni e quantomeno, rispetto al passato, hanno ricevuto una istruzione.

Chi fa sviluppo Web sa benissimo che l’Html originale non aveva alcuna velleità di strumento di impaginazione. Però è stato subito stirato, forzato, contorto perché in qualche modo si riusciva a fare una qualche impaginazione rudimentale. In trent’anni si è riconosciuta l’esigenza e lo sviluppo, tra nuovi tag e CSS, ha cambiato focus, dalla semantica al layout. Oggi HTML impagina molto meglio che nel 1993, ma in trent’anni è stato stravolto. I tag iniziali oggi sono una minima percentuale del totale.

Tra dieci, venti, trent’anni le tecniche di machine learning e di trattamento dei dati avranno stravolto nella stessa maniera i risultati di oggi al punto da renderli irriconoscibili e, forse, potremo di nuovo chiedere a un computer di fare un’addizione ed essere confidenti con il risultato, come con le calcolatrici degli anni settanta. Nel frattempo, se è un Llm, prima o poi ha le allucinazioni, pardon, scrive fregnacce. L’unico modo per usarli bene è evitare di prendere per oro colato le loro cosiddette ricerche e, come chiude l’articolo, *guardare i loro risultati con occhio scettico*. Si vedono già esempi di prodotti di Llm, pubblicati senza verifiche e controlli, e sono un ottimo sistema per riconoscere i cretini naturali ai quali i nuovi strumenti hanno tolto ogni meccanismo di contenimento, finalmente liberi di correre più veloci del loro cervello.