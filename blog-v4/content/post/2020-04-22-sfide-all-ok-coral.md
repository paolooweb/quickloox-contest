---
title: "Sfide all'OK coral"
date: 2020-04-22
comments: true
tags: [Gruber, iPad, Magic, Keyboard, iPhone, SE, Viticci, MacStories, Daring, Fireball]
---
L’idea di mettere enfasi su una tastiera per iPad da parte di Apple [continua a piacermi poco](https://macintelligence.org/posts/2020-03-29-habemus-cursor/) ma non ho potere di fermarla; mi consolo con la recensione della [Magic Keyboard per iPad Pro](https://www.apple.com/it/shop/product/MXQT2T/A/magic-keyboard-per-ipad-pro-11-seconda-generazione-italiano) da parte di Federico Viticci, che titola di [una nuova specie di laptop](https://www.macstories.net/stories/magic-keyboard-for-ipad-pro-a-new-breed-of-laptop/).

*Ticci* scrive quello che speravo di leggere: la natura di computer modulare propria di iPad non cambia e la tastiera incoraggia un uso fortemente ambivalente dell’apparecchio, ora portatile ora tavoletta, senza pretendere di farlo diventare un nuovo modello di MacBook Pro. Ne abbiamo già, fanno cose straordinarie, hanno altre specificità di uso. Sono un’altra specie.

Apple fa la cosa sbagliata, però almeno la fa bene. E cambia in corsa il ruolo di iPad senza snaturarlo, come vorrebbbero quelli che devono appiccicargli un’etichetta tipo [*2-in-1*](https://www.digitaltrends.com/computing/ipad-pro-perfects-2-in-1-that-microsoft-invented/).

Su un altro versante, appare una nuova edizione di [iPhone SE](https://www.apple.com/it/iphone-se/?afid=p238%7CslaUWG45K-dt_mtid_20925s1839965_pcrid_431699772430_pgrid_102033400724_&cid=wwa-it-kwgo-iphone--slid---productid--), modello che prediligo perché, per avere lo schermo grande, spendo su un iPad. Confesso che iPhone X è più grande, ma ancora gestibile con una sola mano e che mi trovo meglio di quanto avrei pensato, dunque non faccio piani per cambiare.

In compenso leggo che iPhone SE è [lo smartphone per la recessione](https://www.indiscreto.info/2020/04/iphone-se-lo-smartphone-apple-per-la-recessione.html), come se Tim Cook avesse alzato il telefono alla notizia del coronavirus e avesse chiesto un iPhone SE da fare uscire entro due mesi.

John Gruber spiega invece molto bene come [iPhone SE si inserisca in un calendario di uscite pensato nel dettaglio](https://daringfireball.net/2020/04/the_quadrennial_iphone_se_schedule) e legato a fattori molto meno contingenti di quelli che vengono in mente al commentatore improvvisato.

Gruber spiega anche come [sia difficile fare previsioni sul mercato che potrebbe avere un iPhone SE](https://daringfireball.net/linked/2020/04/21/market-for-small-phones), perché in quattro anni nessun grande nome ha prodotto un modello importante con schermo da quattro pollici e sette. Solo Apple.

Per essere il gigante che è diventata, Apple è ancora coraggiosa abbastanza da sfidare a ripetizione il coro e il consenso comune, anche quando è il mio.
