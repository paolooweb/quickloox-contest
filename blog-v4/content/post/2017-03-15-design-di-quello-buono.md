---
title: "Design di quello buono"
date: 2017-03-15
comments: true
tags: [O’Reilly]
---
Mi è arrivata una mail dall’editore librario americano di tecnologia [O’Reilly](https://www.oreilly.com). Sono utente registrato sulla loro piattaforma, ma ricevo comunicazioni da loro praticamente MAI.<!--more-->

Questa è un’eccezione. Uno dei libri in formato elettronico che ho preso da loro ha ricevuto un aggiornamento: tutti gli *errata* sono stati sistemati.

La mail contiene un link per scaricare immediatamente l’aggiornamento.

Ho prelevato a suo tempo la versione ePub. Ciononostante ho facoltà di scaricare la versione aggiornata in cinque formati diversi, ePub compreso.

Nell’interfaccia è prevista una opzione di ricerca di contenuti all’interno del libro.

Già che ci sono, nella pagina vedo se esistono aggiornamenti anche per gli altri libri O’Reilly che possiedo.

Ogni frase di questo *post* è un capitolo di un altro libro ancora non scritto: il buon *design* che facilita la vita del cliente e gli rende un servizio invece di porgli problemi. Design di quello buono, che apre la mente e agevola il benessere.