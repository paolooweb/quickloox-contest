---
title: "Installare e instillare"
date: 2020-01-27
comments: true
tags: [iPhoneItalia, Kaspersky, Andy]
---
Allarme! Come segnala *iPhoneItalia*, [un malware ha infettato il dieci percento degli utenti macOS](https://mac.iphoneitalia.com/109195/malware-shlayer-trojan-mac).

Sembra preoccupante. **Andy**, che la sa lunga e mi segnala la notizia, parla di **concentrato di imbecillità**.

>Kaspersky afferma che nel 2019 lo Shlayer Trojan ha infettato un utente macOS su 10. C'è da preoccuparsi?

Non saprei. Leggo il resto dell’articolo alla ricerca di una risposta, che non c’è. Allora vado da Kaspersky, che titola [Il trojan Shlayer attacca un utente macOS su dieci](https://securelist.com/shlayer-for-macos/95724/). Attacca, non *ha infettato*, anche se è lana caprina. Vado alla ricerca di cifre e trovo altro:

>Nel 2019, una su dieci delle nostre soluzioni di sicurezza per Mac ha incontrato questo malware almeno una volta.

Hai capito? Non sono gli utenti! Sono le installazioni di antivirus Kaspersky.

Saranno un campione valido? Vediamo. Nella mia famiglia allargata ci sono quattro Mac e nessuno di loro monta Kaspersky. Nel mio gruppo di gioco di ruolo ci sono altri quattro Mac e anche lì non se ne parla. Sento un po’ di amici in giro, tutti con un Mac, e nessuno ha Kaspersky montato.

Saranno Mac infetti? A casa mia no. I miei amici stretti, ne dubito; sanno fare il loro lavoro, che è di carattere informatico, e non prendono malware neanche su Windows, figuriamoci Mac. Neanche questo è un campione affidabile, eh? Però insomma, quell’uno su dieci puzza di botto di Capodanno.

Leggo l’articolo di di Kaspersky, che se non altro è pieno di dettagli tecnici e di codice. L’infezione si propaga attraverso finti installatori di Flash e link malevoli su Wikipedia. Roba già vista, che lavora sui disattenti perché autorizzino l’impossibile sui loro Mac e che resta lontana da chiunque possieda il minimo sindacale di consapevolezza.

L’articolo chiude così:

>Dopo avere studiato la famiglia Shlayer, possiamo concludere che la piattaforma macOS sia una buona fonte di fatturato per i cibercriminali.

A me pare che, date le premesse, cerchino di trasformarla in una buona fonte di fatturato per Kaspersky, attraverso una comunicazione che è ambigua a dire poco e, invece di installare malware, mira a instillare paure poco fondate. Adesso speriamo che iPhoneItalia si ricordi di scrivere se dobbiamo preoccuparci oppure no. Come facciamo, senza?