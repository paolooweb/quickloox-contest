---
title: "The FUD Crisis"
date: 2021-12-14T01:34:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [Intel, Apple Silicon, International Electron Devices Meeting, Iedm, Ieee, Amd] 
---
Intel ha partecipato all’edizione 2021 dell’[International Electron Devices Meeting di IEEE](https://www.ieee-iedm.org/) e ha presentato un impressionante elenco di nuovi processori… ehm, no. Di [promesse per il 2025](https://wccftech.com/intel-unveils-its-plans-to-accelerate-moores-law-at-iedm-2021-10x-density-improvement-up-to-50-logic-scaling-post-silicon-transistors-era/).

Ogni tanto si discute di quanto sia consistente il vantaggio di Apple Silicon sulle architetture attuali di Intel e adesso lo sappiamo: tra i quattro e i cinque anni. Posto naturalmente che Apple stia ferma a guardare e non faccia progressi di suo, magari maggiori o migliori.

La tripla arma di FUD (Fear, Uncertainty, Doubt) è stata molto usata nel passato da Intel (e da altri). Se appena emergeva un concorrente con un vantaggio concreto nel presente, veniva annunciato qualche progresso consistente per gli anni a venire.

Mica era necessario che fosse vero: paura, incertezza, dubbio. Era sufficiente alzare una cortina fumogena di propaganda per persuadere amministratori delegati, direttori IT e tecnici a stare sul certo invece che prendersi rischi: perché lasciare le scelte consolidate e abituali per qualcosa di inedito, imprevisto, imprevedibile? Quando nel giro di pochi anni la supremazia sarebbe tornata nelle mani dei soliti?

Peccato che adesso la stessa arma sappia di tappo. Intel non ha più la vecchia compagna di merende per fare insieme il bello e il cattivo tempo sul mercato. A parte Apple Silicon, anche Amd produce già oggi chip problematici per l’azienda che ha coniato la legge di Moore tanti anni fa e la ritira fuori trent’anni dopo con lo scopo di di intestarsi i prossimi anni di progresso nei microprocessori.

La verità è che il progresso, oggi, lo stanno scrivendo altri. E promettere nel vuoto non spaventa più come prima chi sia disposto a provare alternative.

_Per ora si può lasciare un commento dalla [pagina apposita di Muut per Qui
ckLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile._