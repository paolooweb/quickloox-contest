---
title: "Promossi e rimandati"
date: 2021-09-05T01:10:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [scuola, ipertesto, curricolo] 
---
L’idea di creare un [curricolo](https://macintelligence.org/posts/Un-curricolo-per-lestate.html) di studi digitali per elementari e medie continua anche se discontinua ed è il momento di ipotizzare attività per introdurre bambini all’ipertesto, trasversalmente nell’ambito di ciascuna materia convenzionale.

Mi ha lasciato [un bel commento](https://muut.com/quickloox#!/apple:ipertesti-fisici) **energio**:

> Non ricordo il nome, ma per iniziare la tesi (oramai una quindicina di anni fa) mi ero basato su un vocabolario in cui le parole presenti nello stesso erano evidenziate rispetto alle altre, in modo che si potessero creare “reti” tra le varie descrizioni e spiegazioni: da questo avevo costruito una mappa concettuale su tre livelli a partire dall'oggetto della mia ricerca. La cosa che mi aveva colpito era che l'umanità organizza naturalmente da sempre le informazioni e un “collegamento” tra fonti diverse era già presente prima dei “link”…

È uno spunto buono da cui si possono trarre molte variazioni sul tema.

In ordine sparso, in italiano si potrebbe ragionare su ciò che è ipertestuale nei media anche se nessuno ci pensa. Per esempio sommari e indici dei libri cartacei, o di riviste, sono ipertesti. Oppure la segnaletica per muoversi all’interno della scuola.

I più grandi potrebbero comporre delle semplici wiki contenenti lavoro scolastico.

I più piccoli potrebbero rappresentare la loro giornata disegnando i luoghi in cui si trovano per poi congiungerli in accordo agli itinerari reali: casa, scuola, giardinetti, palestra, casa di un amico. Trovando nodi comuni nelle rispettive rappresentazioni si potrebbe passare dai disegni individuali a quelli di classe.

Per l’inglese, oltre ovviamente a replicare tutto in lingua, ci si può soffermare sul concetto di _link_.

Un gioco che riporta all’ipertestualità è quello delle frasi composte a caso pescando in gruppi ordinari di bigliettini (nome, verbo, complemento eccetera).

Più ne scrivo più ne vengono in mente e ci si può fermare qui. L’idea è promuovere gli studenti insegnandogli anche la disciplina del rimando.

*Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile.*