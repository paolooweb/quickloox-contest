---
title: "Natale con il coding"
date: 2023-12-01T22:09:59+01:00
draft: false
toc: false
comments: true
categories: [Software, Web]
tags: [Advent of Code]
---
Quasi duecentomila quelli che hanno affrontato la prima sfida di [Advent of Code 2023](https://adventofcode.com/2023/), nona edizione.

Di tutti i calendari dell’Avvento a tema programmatorio, questo ha passato la prova del tempo ed è diventato tradizione. Ne parlano su Reddit in tantissimi, ci sono siti su siti che commentano le sfide e le soluzioni, ci sono scuole che si coalizzano e classi che si dividono per vedere chi arriva primo…

Poi ci sono anche millemila imitazioni, più facili, più improvvisate, meno eleganti, più accessibili, tutto. L’importante è cogliere l’opportunità prima dell’occasione: non fa più moda, ma saper pensare con logica computazionale continua a essere una capacità importante e apprezzata. Facciamoci eventualmente un regalo per le vacanze.