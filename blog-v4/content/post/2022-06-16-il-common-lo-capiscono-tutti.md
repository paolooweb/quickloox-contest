---
title: "Il Common lo capiscono tutti"
date: 2022-06-16T00:23:12+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [Common, Common Lisp, Kandria, Kickstarter, D&D, Dungeons & Dragons]
---
Un giocatore di [Dungeons & Dragons](https://dnd.wizards.com), anche estemporaneo, sa bene che il *Common* è la lingua franca del gioco, parlata da tutte le razze rilevanti e in tutti i contesti primari.

Su Kickstarter è appena partita [la campagna di lancio di Kandria](https://www.kickstarter.com/projects/shinmera/kandria/description), un *action Rpg* che fa buon uso del Common. O meglio, di [Common Lisp](https://lisp-lang.org):

>In virtù dell’essere scritto in Common Lisp, interi blocchi del gioco possono venire completamente cambiati e aggiornati mentre il gioco stesso è in esecuzione. Questo facilita molto l’iterazione sul design e il collaudo di nuove meccaniche di gioco. La stessa possibilità  permetterà ai modder di applicare cambiamenti anche drastici ai meccanismi di funzionamento del gioco, come aggiungere nuove dinamiche di movimento, NPC [personaggi artificiali], missioni e così via.

Chiaramente devono piacere genere e ambientazione (qui siamo nel campo dei dopobomba fantascientifici), prima di tutto. È possibile provare una [demo del gioco su Steam](https://store.steampowered.com/app/1261430/Kandria/?utm_source=kandria.com).

A me il genere piace, l’ambientazione piaciucchia, la grafica piace poco ma il linguaggio di programmazione piace moltissimo e ho scelto di appoggiare il progetto, che in tre giorni ha raccolto oltre due terzi della somma richiesta.

I giochi si chiudono il 14 luglio e, se il tetto finanziario sarà superato, avremo aiutato con successo Kandria ad arrivare sul mercato.