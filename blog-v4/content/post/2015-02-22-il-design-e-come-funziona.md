---
title: "Il design è come funziona"
date: 2015-02-23
comments: true
tags: [Ive, ITProPortal, NewYorker, StarWars, Abrams, design]
---
Il *New Yorker* dedica un [articolo di lunghezza sterminata e bellissimo a Jonathan Ive](http://www.newyorker.com/magazine/2015/02/23/shape-things-come), il responsabile del *design* in Apple.<!--more-->

Di sfuggita si accenna ai consigli che Ive ha fornito a J.J. Abrams per rinnovare l’aspetto delle spade laser nel nuovo film della saga *Guerre Stellari*: dare loro un tocco più imperfetto, più impreciso, renderle leggermente più primitive nell’impressione, perché sembrino più intimidatrici. E poi, oltre alle spade laser, nell’articolo ci sono duecento altre cose.

Tutte ignorate [su ITProPortal](http://www.itproportal.com/2015/02/17/jony-ive-iphone-designer-invented-new-star-wars-lightsaber/), per l’occasione né IT né Pro, da un deficiente che commenta

>La cosa più strana di tutta la storia è che un’idea del genere sia arrivata da un designer Apple; una azienda nota per l’aspetto filante, pulito e futuristico dei prodotti, con bordi curvi e colori semplici.

Deficiente perché gli manca appunto qualcosa: la nozione che essere bravi nel *design* non vuol dire produrre oggetti filanti. Vuol dire essere bravi nel *design*. E quindi saper anche essere primitivi quando quella è la risposta giusta per il contesto.