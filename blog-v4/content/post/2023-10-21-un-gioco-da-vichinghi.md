---
title: "Un gioco da vichinghi"
date: 2023-10-21T18:51:33+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [Oddmar, Lidia, Nive]
---
Ribadisco che Apple Arcade è uno dei modi migliori di spendere soldi in direzione Cupertino quando si abbiano in casa due videogiocatrici a tempo perso con età a cifra singola.

Giochi di ogni genere, di buona o ottima qualità, zero pubblicità, zero acquisti in-app. È un giardino dorato dentro il giardino dorato, sicuro; qualunque preoccupazione possa derivare dall’uso di App Store per bambine, tuttavia, si azzera.

Avevo ancora qualche perplessità sui limiti di età, per via di certi giochi progettati per dodicenni o oltre, e applicavo una leggera sorveglianza preventiva. Ho smesso perché il problema si risolve da solo; una sa leggere e sorvola sui giochi con rating superiore a 9+; l’altra, se carica un gioco da grandi, lo trova immediatamente troppo complesso perché i comandi non sono evidenti. Alla fine, capita che la sera mi venga chiesto *posso scaricare un gioco da Arcade?* e la risposta è *sì, attenta che sia Arcade*. Sono stati commessi un paio di errori, poi la procedura si è consolidata e di fatto ho solo da partecipare se è richiesta la presenza del papà. È una pace dei sensi in cambio di una cifra mensile veramente esigua che, se non potessi permettermi, mi farebbe sacrificare volentieri tre colazioni a caffè e brioche ogni mese, una ogni dieci giorni. Un costo così è vicino a non essere un costo.

Ma divago. Primogenita si è ultimamente impadronita di [Oddmar](https://apps.apple.com/us/app/oddmar/id1247397901), gioco di piattaforma a scorrimento basato su una trama di vichinghi antichi e mitologie norrene.

Nel mondo esterno credo che i primi livelli del gioco siano praticabili gratis e quindi chiunque può farsi una prima idea. Nel mondo dorato di Arcade, ovviamente, l’intero gioco è a disposizione.

All’inizio la grafica, simpatica e improntata a una goffaggine generale dei personaggi, lascia pensare a un gioco facile, che effettivamente lo è. I comandi da tastiera sono semplici e come per tutti i *platform* si è pronti per andare a fondo una volta acquisiti quei quattro automatismi con le dita.

Nell’andare a fondo, tanto di cappello. La trama è articolata, i personaggi nel gioco si moltiplicano, si incontrano i boss, ci sono i mondi onirici (senza spoilerare) e più di una metrica per valutare i propri risultati.

Il quoziente di difficoltà non è altissimo, tuttavia diverso da quello che appare inizialmente quando la meccanica di gioco pare ingenua o banale. L’inventiva disseminata livello per livello è molta e di idee c’è ricchezza; i puzzle da superare sono vari e ingegnosi, tanto da richiedere a volte una occhiata preventiva allo schema o il sacrificio di un paio di vite per capire che cosa succede.

La tematica vite è frustrante quanto basta e non punitiva. Il gioco è costellato di *checkpoint*; tra l’uno e l’altro, la prima morte è ininfluente e si continua a giocare dove ci si trova; la seconda riporta al *checkpoint* precedente più vicino. Durante il percorso si può trovare uno scudo magico che evita una morte, oppure una mela per guadagnare una vita ove se ne fosse persa una. Le vite non sono comunque mai più di due.

*Oddmar* è profondo e contemporaneamente risolvibile. Lidia è arrivata a vedere la sequenza di fine gioco, avendo sviluppato skill di controllo del personaggio nettamente superiori ai miei. Fatico a stimare il tempo di completamento perché lei non è competitiva e gioca inventando storie sui personaggi che appaiono, oppure mollando tutto per affrontare un livello inferiore già fatto in quanto più divertente e via dicendo, quindi ha certamente giocato più a lungo di una persona determinata. Penso che possa servire una quindicina di ore, dato da prendere con le pinze da vichingo, quelle che si usano nelle forge del Valhalla.

La rigiocabilità dipende dal tasso di perfezionismo. Primogenita è appunto arrivata in fondo, senza però completerà tutte le sfide e tutti i traguardi parziali presenti in ogni livello. Oltre che profondo, Oddmar è ampio e per ripulire al cento percento un livello, specie andando avanti, il lavoro richiesto è significativo. Non tutto è visibile, non tutto è immediato, certi livelli sono facili solo che per catturare una moneta rara c’è da impazzire eccetera. Molte ricompense sono dichiaratamente fuori dal percorso per arrivare in fondo e volerle richiede la scelta consapevole. È presente una dinamica di crescita del personaggio attraverso l’acquisto di nuove armi e arrivare a possederle tutte non è scontato.

La violenza è mediata più che a sufficienza per non traumatizzare alcuno e la grafica sdrammatizza tutto, quindi non ci sono remore a consigliarlo a bambini dai nove anni; sotto, le atmosfere sono tranquille (magari le prime volte si gioca insieme) e però non è detto che ci sia coordinazione oculo-manuale sufficiente. Saper leggere la trama che evolve da livello a livello aggiunge sapore e senso al gioco, anche se Lidia ha iniziato guardandola poco per pigrizia (in casa mia si gioca tassativamente in inglese). Più avanti si è incuriosita ed è tornata indietro per recuperare le informazioni perdute.

La prima volta che ho visto *Oddmar* l’ho sottovalutato molto. Sembra un giochino, mentre possiede valore e qualità.