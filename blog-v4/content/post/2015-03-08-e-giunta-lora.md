---
title: "È giunta l’ora"
date: 2015-03-09
comments: true
tags: [Apple, Vertu, watch, Gruber, DaringFireball, Ive, Cook, Jobs]
---
La guida definitiva alla presentazione di watch, come sempre, è il [preludio](http://daringfireball.net/2015/03/apple_watch_prelude) scritto da John Gruber su Daring Fireball.<!--more-->

C’è anche una [autocitazione](http://daringfireball.net/2014/09/apple_watch) preziosa per filtrare i commenti successivi all’evento:

>Quando saranno annunciati i prezzi degli Apple Watch di acciaio e (specialmente) d’oro, mi aspetto la maggiore ondata escrementizia della stampa di settore nella storia delle ondate escrementizie Apple-contro-le-abitudini-consolidate-del-settore. L’abito mentale utilitarista che chiede “Perché uno dovrebbe sprecare denaro su un orologio d’oro?” non è in grado di capire che cosa sta facendo qui Apple. Diranno che Jony Ive e Tim Cook sono impazziti. Consumeranno le tastiere per digitare “Non sarebbe mai successo con Steve Jobs”. Prediranno un fallimento netto e umiliante. In breve, scambieranno Apple per [Vertu](http://www.vertu.com/it/it/home).

Andranno anche guardati i polsi. Più sono i Rolex, più scriveranno che tutti gli orologi sono uguali.