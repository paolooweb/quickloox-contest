---
title: "Tempi vivi"
date: 2021-02-12T00:00:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [Apple Store] 
---
Un colpo secco che a un orecchio distratto avrebbe potuto anche sembrare un’arma ad aria compressa. Uno degli iPhone familiari è appena caduto sulla piastrella del bagno perfettamente di piatto, dalla parte sbagliata. I testimoni oculari mi dicono che lo schermo ha dato un ultimo lampo di luce azzurrina e poi si è spento.

Il cristallo è perfettamente integro, solo che la caduta è stata eccessivamente precisa e si è rotto qualcosa dentro. L’apparecchio non reagisce ad alcuna terapia.

Gli Apple Store funzionano solo previo appuntamento, di questi giorni pandemici. Riesco a trovare uno spazio nella tarda serata e parto, sapendo come andrà; è un iPhone talmente vecchio che la riparazione richiederà una cifra ingiustificabile, così si aggiungerà il necessario per prendere qualcosa di nuovo, che ha più senso.

Arrivo al centro commerciale – [Carosello](https://www.apple.com/it/retail/carosello/), per la cronaca – e raggiungo Apple Store. Non c’ero ancora stato, da quando è arrivato il virus. quasi tutto lo spazio viene usato dai Genius per i loro interventi, in gran distanziamento. I clienti hanno lo spazio per entrare e trovarsi davanti tre sportelli: ritiro prenotazioni, acquisto, Genius Bar. Tre clienti per volta, uno per sportello, gli altri fanno la fila fuori.

Mi metto nella mia fila, per il Genius Bar, e arriva uno dei ragazzi dello staff. Ho un appuntamento? Come mai sono lì? di che iPhone si tratta? Mi costerebbe trecentocinquantanove euro, per averne uno equivalente ricondizionato con tre mesi di garanzia.

Non ne vale la pena, lo sapevamo già ma ora abbiamo la conferma. Ok, allora ne prendiamo uno nuovo. So già di che tipo? Sì, XR, centoventotto giga, ora che ci penso non so il colore, chiedo in famiglia. Non c’è problema, intanto mi fanno cambiare la coda; da Genius ad acquisto.

Il ragazzo torna e mi dice di avere controllato, hanno a disposizione tutti i colori disponibili, per cui non ci saranno problemi. Nel frattempo vengo a sapere di volere un modello bianco.

Allo sportello acquisto, una ragazza molto gentile ha già pronto tutto. Mi chiede se desidero AppleCare, accessori vari, no, niente, grazie, abbiamo già tutto e questo iPhone è quello di casa nostra che è durato meno di tutti: cinque anni abbondanti. AppleCare non ha senso. Piuttosto, ero venuto per questo iPhone 7, che ci faccio?

La ragazza lo controlla, dice che se non si accende non c’è risparmio sull’acquisto, va in riciclo e basta. Però strano, il cristallo le sembra a posto…

Spiego che l’impatto è stato l’apoteosi dell’ortogonalità e che la custodia ha impedito l’urto dello schermo contro il pavimento, ma la botta di piatto perfetto ha creato un problema all’interno. Lei lo soppesa e mi verrà incontro, dice, con uno sconto del dieci percento sull’acquisto.

Sono in libertà e faccio abbondantemente in tempo a recuperare qualcosa da sgranocchiare in macchina mentre torno a casa. Ho fatto due code diverse, ho portato un caso che non si aspettavano, partito con un iPhone da riparare sono tornato con un altro iPhone nuovo e, orologio alla mano, tutto è durato meno di dieci minuti.

Apple Store Carosello dista circa quarantacinque minuti di auto da casa, che sommati ai minuti del ritorno fanno novanta minuti. Sono stato in ballo un’ora e mezza per risolvere un problema in dieci minuti. Non sembra uno spreco?

In verità, mi è capitato di recente di prendere appuntamento con la banca. Una pena, disorganizzazione, lentezza, improvvisazione, peggio di quando si andava alla cieca e ci si metteva in coda spontanea.

Apple Store è riuscito a trasformare una situazione malinconica come quella delle restrizioni da coronavirus in una esperienza rapida ed efficace, in cui comunque hanno trovato modo di ascoltarmi, o almeno di farlo sembrare. Proprio il fatto che sia stato un tempo così breve, preciso e risolutivo mi dice di istinto che valeva la pena di guidare un’ora e mezza, per dieci minuti così densi. Così vivi, nonostante le mascherine, gli spazi semivuoti, i camminamenti deserti.

Lo rifarei anche subito. Però aspetterò con calma, a naso, il 2026 o il 2027.

*Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile.*