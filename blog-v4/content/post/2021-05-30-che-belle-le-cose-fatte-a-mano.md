---
title: "Che belle le cose fatte a mano"
date: 2021-05-30T00:10:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [Dr. Drang, Markdown, BBEdit, AppleScript] 
---
In Markdown ci sono due modi per inserire link nel testo: *inline* (l’Url del link segue immediatamente il testo linkato) e *reference* (Al testo linkato fa seguito un codice di riferimento e in fondo al documento si trovano i riferimenti e gli Url collegati).

Ciascuno ha la sua preferenza, per questioni di leggibilità del sorgente oppure di editing veloce di un link in fase di revisione. C’è anche però chi crea link in Markdown a mano e tende a preferire i link *inline*, per la ragione che sono più immediati da creare e richiedono meno testo e spostamenti del cursore.

Qui arriva Dr. Drang, per il quale la preferenza è davvero tale e non una ricerca della minore fatica; perché [si è fatto un AppleScript per BBEdit](https://leancrew.com/all-this/2021/05/flowing-markdown-reference-links/) che riduce l’impegno della creazione di link *reference* a un comando da tastiera.

L’AppleScript di Drang guarda la finestra di Safari in primo piano, ne prende l’Url, aggiunge la sintassi Markdown necessaria a creare un link *reference* e mette l’Url al posto giusto in fondo al documento, tenendo anche conto del suo numero progressivo. Si preoccupa infine – sono i dettagli a fare l’eleganza di uno script – di rimettere il cursore nella posizione migliore per ricominciare a scrivere senza altre manovre.

Tutto questo, per lui, significa Control-L.

Che belle le cose fatte a mano, quando sono script che fanno lavorare il computer invece di fare perdere tempo agli umani.

*Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile.*               