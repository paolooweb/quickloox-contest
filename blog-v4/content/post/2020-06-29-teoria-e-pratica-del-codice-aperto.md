---
title: "Teoria e pratica del codice aperto"
date: 2020-06-29
comments: true
tags: [Fontcase, iOS, Iconfacrory]
---
La [teoria](https://macintelligence.org/blog/2020/06/14/la-ragione-e-i-dati/). Installare font nuovi su iOS è macchinoso e problematico. Se dietro una installazione c’è uno sviluppatore subdolo, [possono essere guai](https://blog.iconfactory.com/2020/06/introducing-fontcase/).

The Iconfactory ha visto una soluzione e ha contattato lo sviluppatore. Hanno lavorato insieme e, dall’intuizione originale dello sviluppatore più il contributo di The Iconfactory, è nata [Fontcase](https://apps.apple.com/it/app/fontcase-manage-your-type/id1205074470?l=it).

La pratica. Ho installato Fontcase e funziona subito, bene, semplice.

Dubbi? No. Perché il codice è [open source](https://github.com/manolosavi/xFonts/pull/1) e chiunque può verificare che cosa combina. Chiunque no; occorre un minimo di capacità tecnica. Nondimeno, il codice è lì da vedere.

Ecco perché sono socio [LibreItalia](https://www.libreitalia.org) e perché tifo appassionatamente per l’open source. Lavora onestamente alla luce del sole e, nell’informatica odierna, è ossigeno puro a ottomila metri.