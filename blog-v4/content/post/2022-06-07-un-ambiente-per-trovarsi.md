---
title: "Un ambiente per trovarsi"
date: 2022-06-07T12:21:41+01:00
draft: false
toc: false
comments: true
categories: [Hardware, Software]
tags: [Apple, WWDC, Misterakko, M1, M2, FreeForm, Stage Manager, Apple Silicon, Runestone, BBEdit, Continuity, Handoff, Universal Control, FaceTime, SharePlay, Shared iCloud Library, metaverso, Facebook]
---
Non ho ancora visto il *keynote* di [Wwdc](https://macintelligence.org/posts/2022-06-06-non-cè-tre-senza-cloud/) in forma completa dall’inizio alla fine in condizioni di tranquillità e però credo di avere capito un senso possibile dell’evento.

Che non è la parte strettamente tecnica dell’evento, per la quale rimando volentieri a **Misterakko**, autore di un’ottima [spremuta di annunci](https://apple.quora.com/Una-volta-all-anno-Apple-chiama-raduno-i-suoi-sviluppatori-per-raccontare-i-frutti-del-lavoro-di-12-mesi-dai-suoi-migli?ch=17&oid=71179261&share=c9eb2c41&srid=hwssL&target_type=post).

Durante Wwdc Apple comunica agli sviluppatori (devo farti venire voglia di scrivere app) e al mondo (cose egregie che abbiamo messo a punto per farti venire voglia di comprare un device).

Tutti questi concetti si appoggiano a piattaforme hardware che vogliono essere desiderabili e, per quanto la comunicazione di Wwdc sia orientata al software tranne casi eccezioni, un messaggio chiaro e preciso è stato dato: la transizione verso Apple Silicon procede (c’è ancora un semestre per completare la gamma dei prodotti, praticamente un quarto del tempo promesso) e procede bene, con M2 che è già qui e porta rispetto a M1 la crescita che tutti speravano di vedere. C’è chi si attrezza adesso per [acchiappare M1 nel 2025](https://macintelligence.org/posts/2022-05-26-è-cambiato-il-metronomo/) o per [fine 2023](https://macintelligence.org/posts/2022-05-03-corse-di-recupero/). Intanto ha iniziato a correre una nuova lepre, più veloce e che consuma meno energia. Il posto hardware dove stare, oggi, è Apple Silicon.

Tutto il resto è software e riparto dalla mia considerazione di ieri: un post scritto partendo da iPad, rifinito un attimo su iPhone, terminato su Mac. Un unico oggetto di lavoro realizzato su tre macchine diverse che, dal mio punto di vista, sono state una sola, declinata in tre modi. La macchina-me al lavoro, con lo strumento più conveniente.

Le mie due copie di Runestone e quella di BBEdit hanno *collaborato* attorno a un testo con l’aiuto di iCloud. La collaborazione era fino a pochi anni fa un segmento sul quale Apple non faceva gran che di buono. Ora, come si vede, esistono modi per collaborare a livello di applicazioni.

Tutto gli aggiornamenti a Continuity presentati, tra Handoff e Universal Control, rafforzano la *collaborazione* a livello di hardware. Uno schermo, una tastiera, questo o quel device, diventano parti di un tutt’uno, ancora una volta la macchina-me che mette al lavoro questa o quella appendice hardware in un insieme organico.

E poi le estensioni di FaceTime, la vista dall’alto della scrivania ricostruita via software, SharePlay, Shared iCloud Library: *collaborazione* tra persone, che sempre più iniziamo a vedere e percepire, seppure attraverso lo schermo, come posizionate differentemente nello spazio, a superare in prospettiva lo iato tra presenza fisica e remota, con FreeForm che porta finalmente un tabellone condiviso dove scrivere, disegnare, mostrare, sottoporre tutti insieme.

Mentre si dipana tutta questa strategia di collaborazione a livello software, hardware e umano, watch o – da domani – persino iPhone si prendono cura del nostro corpo. Riescono a capire come camminiamo e corriamo, derivano da pochi rilievi corporei una serie di indicazioni per il nostro benessere che facciamo fatica a immaginare per quanto è complessa e ingegnosa l’elaborazione effettuata dietro le quinte.

Riassumendo: alcuni device si prendono cura del nostro corpo; altri formano un insieme armonico di hardware e software e ci immergono in spazi comuni di lavoro e intrattenimento, dove troviamo altre persone.

A pensarci bene, vengono in mente tutte le chiacchiere sul metaverso, l’universo artificiale in cui ci vuole infilare Facebook. La quale ha pensato di arrivarci buttando la gente nel contenitore intanto che si capisce che cosa metterci dentro.

Invece che partire dal contenitore, Apple costruisce un passo per volta l’ambiente in cui trovarsi, dove il singolo software contribuisce a un tutto, il singolo hardware pure, le persone non sono più solo nomi e icone ma iniziano a portare segni più concreti e tangibili della loro umanità.

Questo è Wwdc 2022. Un altro passo verso l’inizio del metaverso, intrapreso dalla parte giusta.