---
title: "Il potere dei nomi"
date: 2024-01-10T00:53:31+01:00
draft: false
toc: false
comments: true
categories: [Hardware]
tags: [Vision Pro, 9to5mac, Apple Vision Pro, visionOS, spatial computing]
---
Si registra una certa agitazione in taluni che scoprono come Apple [abbia comunicato agli sviluppatori di evitare, per le proprie app su Vision Pro, le dizioni](https://9to5mac.com/2024/01/08/apple-visionos-apps-ar-vr/) *AR*, *VR* *MR* (Mixed Reality) eccetera. Ugualmente, Vision Pro non va chiamato *headset* (in italiano, casco o anche visore).

Apple lo ha sempre fatto, a partire da sé stessa (e non è affatto scontato). Si possono percorrere le sezioni del sito ufficiale alla ricerca di termini generici che indichino una categoria (*smartphone*, *tablet*, *smartwatch*). Non c’è nulla.

Apple conosce la potenza dei nomi. L’atto di assegnare un nome aveva proprietà esoteriche o magiche o rivelatrici, nel mondo antico. Il ridurre un oggetto a una categoria è un trucco dei media per distribuire ignoranza, sminuire il valore dei singoli prodotti, fare sentire il pubblico falsamente in possesso di un gergo per iniziati. Se si può usare una parola per descrivere un mondo, ecco torme di gente assetata di riscatto personale che vuole poterla pronunciare, *netbook*, *rimovibili*, *convertibili*, *desktop publishing* *Web3*, *metaverso*, *AI* (oggi è il suo momento, per la chiamata a sé delle folle) e così via.

C’è da scrivere *Apple Vision Pro*; la forma giusta per il nome del sistema operativo è *visionOS*. L’esperienza che offre la miscela di hardware e software si chiama *spatial computing*, non realtà ibrida o altre formule già confezionate. In Italia, la pronuncia di *spatial*, piuttosto vicina a quella di *special*, non aiuterà. Bisognerà essere forti e guardare dall’altra parte quando il collega a tavola pontifica come se lo avesse progettato lui Vision Pro. Avere lo sguardo proprio come lo renderebbe Vision Pro, proiettato verso un orizzonte lontano.