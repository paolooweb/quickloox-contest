---
title: "Ipotesi di complotto"
date: 2020-08-25
comments: true
tags: [Apple, Fortnite]
---
Succede abbastanza spesso che qualcuno o qualcosa là fuori usi uno dei miei indirizzi iCloud per provare a iscriversi a qualche servizio. Mi arriva la richiesta di resettare la password, o di iscrizione a qualche servizio, la ignoro e ovviamente finisce tutto lì.

Se però arriva una richiesta di accredito di un account Fornite per un minore, sempre effettuata da un mio indirizzo iCloud, in questo [momento di conflitto tra Epic Games e Apple](https://macintelligence.org/blog/categories/fortnite/) mette a rischio la possibilità di giocare a Fortnite su iOS, viene da pensare qualsiasi cosa.

 ![Richiesta di autorizzazione di minore a giocare Fortnite](/images/fortnite.png  "Richiesta di autorizzazione di minore a giocare Fortnite") 

Al momento sono uno a uno e palla al centro. Apple, [ha detto il giudice](https://www.courtlistener.com/recap/gov.uscourts.cand.364265/gov.uscourts.cand.364265.48.0.pdf), non è obbligata a riammettere Fortnite su App Store ma non può proibire a Epic di usare gli strumenti di sviluppo di Apple per lavorare al motore 3D Unreal.
