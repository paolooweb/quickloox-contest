---
title: "Polizia informatica"
date: 2013-09-25
comments: true
tags: [iPhone, iOS]
---
Da che parte vogliamo affrontare il fatto che la polizia di New York [incoraggia i possessori di iPhone](http://www.tuaw.com/2013/09/23/nypd-handing-out-flyers-asking-people-to-upgrade-to-ios-7/) ad aggiornare a iOS 7 attraverso la distribuzione di volantini per strada?<!--more-->

Ce n’è per ogni angolazione. Sociologica, di sicurezza, di costume, di confronto con l’Italia. Preferisco questa: se si muove la polizia, l’oggetto è importante e la misura di sicurezza è significativa. In altre parole, iOS 7 contiene il potenziale per cambiare la vita di chi lo adotta, per quanto su un ambito ristretto come quello della sicurezza.

È la sconfitta del piccolo relativismo da piccole menti *fanno-le-stesse-cose-conta-solo-il-prezzo*. Un iPhone è diverso, abbastanza per sollecitare un ente che certo non ha interessi economici nella faccenda.