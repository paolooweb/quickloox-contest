---
title: "Il valore della messa"
date: 2022-06-25T12:25:04+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [Ethereum, Bitcoin, criptovalute, Jobs, Steve Jobs]
---
Non sono un investitore e non sono un investitore nelle cosiddette criptovalute. La matematica dietro a [Bitcoin](https://bitcoin.org/en/) e compagni, però, mi ha sempre intrigato, più che per l’(ingegnoso) algoritmo in sé, per quello che sarebbe potuto succedere dopo, una volta che ci si avvicina ai limiti di estrazione, la difficoltà di estrazione tende all’infinito e si rimane con una moneta la cui disponibilità è fissa.

L’evoluzione delle criptovalute mi sta insegnando il valore della messa in discussione delle mie convinzioni. Quanto accade in questo periodo, con l’attività di estrazione che può diventare antieconomica per chi vi si dedica (platealmente, al momento, [su Ethereum](https://cryptoslate.com/ethereum-mining-no-longer-profitable-for-many-miners-as-energy-prices-and-eth-dip-cause-perfect-storm/)) è tutto sommato abbastanza prevedibile. A un certo punto, *per forza* qualcuno finisce fuori mercato.

Ma poi, quando tutti gli *Ethereum coin* saranno stati estratti? (Chi dice *minati* è pregato gentilmente di reiscriversi alla prima media, grazie) La quantità di valuta è costante, se il bacino degli utilizzatori cresce si genera una inflazione alla rovescia per cui vengono create frazioni di moneta sempre più piccole anziché coniare nuova moneta, il quantitativo di energia per sostenere la criptovaluta potrebbe diventare più o meno accettabile… le variabili in gioco sono tante e molte anche sconosciute, perché non si sono mai verificate prima, anche per chi conciona per spiegarci il metaverso e altre storie che è meglio valutare con pazienza e spirito critico.

L’importante è restare pronti a mettere e mettersi in discussione, perché poi aiuta nella vita reale. Steve Jobs poteva licenziare persone per andare dietro a una sua idea, per cambiarla mezzo secondo dopo che qualcuno gli forniva un argomento sufficiente. Nel mio piccolissimo, sono stato un paladino dei portatili per più di un decennio e oggi passo il cento percento del mio tempo tra un megaschermo alla scrivania e una tavoletta tuttofare. Non era sbagliata l’idea, ma sono cambiati i tempi, le circostanze, le scelte disponibili.

Ecco, cambiano i tempi anche per le criptovalute, figuriamoci quando si tratta di scegliere un Mac o un programma da usare per una certa attività. Mai smettere di mettere in discussione.