---
title: "Scoperte che toccano"
date: 2017-01-19
comments: true
tags: [iPhone, Reddit]
---
Un (ennesimo) motivo per cui l’esperienza di Mac è differente da quella di iOS ed è futile applicare uno stesso ragionamento a tutt’e due le piattaforme è la tipologia delle due interfacce.

Mac nasce per essere esplicito al massimo. In principio, tutto quello che è possibile fare con un programma dovrebbe essere visibile studiando la barra dei menu. La cosa va ovviamente presa con un grano di sale, ma è lecito esemplificare così: davanti a un nuovo programma Mac, la cosa migliore da fare è *studiarlo*.

iOS è un po’ diverso perché lo schermo può essere quello di un iPhone SE, per citare un modello in commercio. È ragionevolmente impossibile che lo schermo possa consentire lo studio di tutte le funzioni di una app. E poi il puntatore del mouse lavora pixel per pixel, mentre il tocco del dito identifica un’area vasta. Gli oggetti effettivamente toccabili su uno schermo iOS possono solo essere relativamente pochi, anche se le funzioni sono innumerevoli.

Ecco perché, davanti a un nuovo programma iOS, la cosa migliore da fare è *scoprirlo*. Scorrere, toccare, premere porta alla luce comportamenti nuovi e impossibili da dichiarare preventivamente nell’interfaccia.

È probabile che, pur con un miliardo di iPhone che funzionano nel mondo, *nessuno conosca veramente tutte le opzioni a disposizione*.

Incredibile, eh? Non dopo avere dedicato una serata a due pagine di Reddit: [All iPhone tricks in one posting](https://www.reddit.com/r/apple/comments/5l1wk3/all_iphone_tricks_in_one_posting_xpost_from/) e [What’s your favorite little known feature in iOS?](https://www.reddit.com/r/apple/comments/5m3mx6/whats_your_favorite_little_known_feature_in_ios/).

Scommetto una birra che nessuno potrà onestamente sostenere di avere letto solo materiale conosciuto. Scommessa retorica, perché la vittoria è certa.

Ne butto lì una, tanto per dire: nella schermata di selezione delle app è possibile, si sapeva, spingere in su la schermata di una app per eliminarla dall’elenco. Non tutti sanno che si possono usare più dita per eliminare contemporaneamente più app.

E un’altra, che però richiede hardware e software aggiornato: Console su Mac mostra l’attività di background di qualsiasi apparecchio iOS collegato via cavo, anche un watch.