---
title: "Ingerenze straniere"
date: 2018-01-24
comments: true
tags: [Wi-Fi, FastWeb, router]
---
FastWeb mi cambia il modem per metterne uno predisposto alla – [finta](http://www.apogeonline.com/webzine/2016/11/09/google-sfibrata) – fibra ottica.<!--more-->

Il tecnico installa (sfila i cavi dal vecchio modem e li infila nel nuovo modem) e mi spiega di attendere un paio d’ora prima che dalla centrale FastWeb venga attivata la nuova configurazione.

Nel frattempo accendo il personal hotspot su iPhone per continuare a lavorare da Mac, in mancanza di linea. Mac, tuttavia, non si connette.

Perplesso, lancio la diagnostica. Di solito serve assolutamente a nulla, ma l’evento è talmente strano. Durante l’estate il personal hotspot non perde un colpo.

Arriva questo messaggio, che non avrei mai sospettato potesse esistere.

 ![Un router straniero può interferire nel Wi-Fi](/images/country-codes.png  "Un router straniero può interferire nel Wi-Fi") 

Lo aggiungo alla base delle conoscenze: i modem hanno un *country code*.

Sospetto che questo sia un altro capitolo della saga *costa meno però fa tutto quello che serve*.

Costa meno, ma disturba la connessione degli altri. Come fatto, lo vedo poco pubblicizzato.