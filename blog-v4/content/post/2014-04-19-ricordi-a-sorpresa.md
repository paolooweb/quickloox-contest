---
title: "Ricordi a sorpresa"
date: 2014-04-20
comments: true
tags: [Melton, Jobs, Safari, OSX, iOS]
---
Auguri a tutti con il [ricordo di Steve Jobs condensato da Don Melton](http://donmelton.com/2014/04/10/memories-of-steve/), il *Safari Guy*, quello di Safari, che ha portato a esistere il *browser* di OS X e iOS.<!--more-->

Il merito di questo articolo è che contiene ricordi reali e concreti, piccoli se sono piccoli, marginali se sono marginali, importanti se lo sono. Tutto vissuto in prima persona da Melton, geloso dei propri ricordi veri tanto da ignorare biografie, film e altri pseudodocumenti, che potrebbero alterare o condizionare le memorie personali.

>Poi mi sono adagiato contro lo schienale, da solo, e ho compreso quanto ero stato fortunato ad avere conosciuto quest’uomo, anche solo per qualche momento.

Un bel ritratto, umano e genuino come è difficile trovare. Buon Passaggio a ciascuno.