---
title: "A brutta posta"
date: 2016-09-26
comments: true
tags: [email, Slack]
---
L’ultimo [accadimento](https://macintelligence.org/posts/2016-09-25-li-stiamo-perdendo/) in materia è, appunto, l’ultimo. La posta elettronica sta diventando sempre più inadeguata al ritmo e alle esigenze attuali. Non per niente faccio sempre più fatica a smaltirla in modo decente e tempestivo.

Sto pensando seriamente di attivare un canale [Slack](https://slack.com) per chi mi volesse contattare e volesse farlo via email.

Ho solo un dubbio: mi spiacerebbe se l’iniziativa cannibalizzasse i commenti sul blog. Sono una grande ricchezza e nobilitano i *post* oltre ogni mia aspettativa. Se ricevessi un messaggio Slack invece di un commento sul blog comunicheremmo alla grande ma si perderebbe qualcosa nel valore complessivo.

Però è una mia opinione che potrebbe essere sbagliata in molti modi. Ci sono altre opinioni? Sono interessatissimo e ringrazio da ora.