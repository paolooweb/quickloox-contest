---
title: "Lucidamente, ludicamente"
date: 2016-01-19
comments: true
tags: [RPG, D&D, 5e, HeroLab, Fantasy, Grounds, PCGen]
---
Mi scuso anticipatamente per la richiesta di servizio che interessa pochi o nessuno.<!--more-->

Cerco persone che abbiano provato estesamente [Fantasy Grounds](https://www.fantasygrounds.com) con innestato il [modulo per Dungeons & Dragons quinta edizione](https://www.fantasygrounds.com/buyFG/DungeonsAndDragons.html) (D&D 5e).

Mentre il nostro gruppo è più che mai immerso in una lunga campagna basata sulla quarta edizione, stiamo esaminando le opzioni future per l’amministrazione dei personaggi. Il panorama si è recentemente schiarito con il prossimo supporto ufficiale – per ora solo in versioni di sviluppo di D&D 5e [da parte di PCGen](http://pcgen.org/2016/01/13/i-hear-you-like-5e-so-do-we/).

Anche HeroLab [dovrebbe vedere presto](http://www.wolflair.com/blog/category/hero-lab/) la compatibilità 5e.

Si tratta tuttavia di soluzioni che, per quanto ufficiali, implicano anche una certa spesa. Questo non è un problema a prescindere, ma si vuole capire bene che rapporto c’è tra qualità e prezzo nelle varie opzioni disponibili.

Ringrazio anticipatamente!