---
title: "Rumore bianco"
date: 2018-12-30
comments: true
tags: [AirPods, Bluetooth, Vagrant, VirtualBox]
---
Immagina di impartire il comando `vagrant up` dentro una macchina virtuale creata da [VirtualBox](https://www.virtualbox.org/) e scoprire che l’audio degli AirPods si degrada in modo inaccettabile.

Succede davvero: [gli AirPods vanno in modalità audio bassa qualità a sedici chilohertz quando si fa partire una macchina virtuale](https://www.jeffgeerling.com/blog/2018/airpods-get-stuck-low-quality-16-khz-audio-mode-when-starting-vm).

Leggendo il *post* si scopre che la cosa avviene saltuariamente, magari una volta su dieci.

Leggendo i commenti, si apprende che capita con vari altri esemplari di cuffie e auricolari Bluetooth.

Inoltre esiste una soluzione, involuta ma efficace, che prevede l’uso di Audio Midi Setup di Apple.

E si può configurare VirtualBox in modo da eliminare il problema. La versione più recente o quasi di VirtualBox, in quanto le altre funzionano bene da questo punto di vista. Non stupirebbe se – da utente VirtualBox posso testimoniare della frequenza di quanto sto per dire – un prossimo aggiornamento chiudesse definitivamente la questione.

Mi chiedo se sarebbe stato più corretto titolare *Una volta su dieci la versione più recente di VirtualBox abbassa la qualità dell’audio via Bluetooth, però si risolve facile*.

È che gli auricolari fanno più rumore se sono Apple.