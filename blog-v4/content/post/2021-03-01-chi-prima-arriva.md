---
title: "Chi prima arriva"
date: 2021-03-01T04:35:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [Happy Scribe, Yoda] 
---
Anni fa ho creato un account a pagamento su [Happy Scribe](https://www.happyscribe.com), sistema di trascrizione automatica di audiovideo, creazione di sottotitoli e altro. L’ho usato una volta e poi l’ho dimenticato. Avevo bisogno di un transcript, il prezzo era un affarone, non ci ho pensato due volte; poi però non si è più ripresentata l’opportunità.

Anno 2021 ed ecco che in due settimane si ricreano altrettante necessità di trascrizione. Rientro in Happy Scribe metto al lavoro il sistema. Intanto curioso e scopro che il ventaglio dei servizi offerti è clamorosamente aumentato, così come il prezzo, raddoppiato rispetto a quello che ricordavo.

Va beh, pazienza; è comunque una cosa sostenibile e la soluzione del problema fa premio sul costo, che comunque è nei termini della dozzina di euro.

Ragiono così quando appare un avviso; sono proprietario di un account *legacy* e quindi mi si applica ancora il vecchio prezzo.

Il vantaggio di avere provato un servizio innovativo anni fa paga in termini assolutamente piccoli, pochi euro di differenza, ma mi suggerisce una lezione di valore molto maggiore. Può convenire spendere in qualcosa di non ancora mainstream, perché quando poi lo diventa, il nostro anticipo potrebbe darci dei vantaggi.

Oltretutto il sistema di editing online dei testi prodotti da Happy Scribe è rimasto semplice e comodo come quella volta là. Seppure funzionando assai meglio.

Il saggio Yoda direbbe *Fare o non fare; non c’è provare*. Tra risparmiare pochi euro e rinunciare a provare un nuovo servizio, ho fatto bene a scegliere la prima alternativa. Dovrei anche farlo più spesso ed essere più curioso, sia pure nel rispetto di tutti i budget.

*Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile.*