---
title: "Grandangolo su Grand Tour"
date: 2017-11-15
comments: true
tags: [Amazon, Prime, Grand, Tour]
---
Questa foto perviene da **Janko** (grosso grazie!) e si riferisce al *backstage* del set di [The Grand Tour](https://www.amazon.com/The-Grand-Tour-Season-1/dp/B01J94A5GQ) trasmesso da Amazon Prime; *il seguito di [Top Gear](http://www.bbc.co.uk/programmes/b006mj59) della BBC per intendersi*.

Il mixer è interessante.

 ![Backstage dal set di The Grand Tour](/images/mac-grandtour.jpg  "Backstage dal set di The Grand Tour") 