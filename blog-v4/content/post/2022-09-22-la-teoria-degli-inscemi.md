---
title: "La teoria degli inscemi"
date: 2022-09-22T00:13:43+01:00
draft: false
toc: false
comments: true
categories: [Hardware]
tags: [iPhone, obsolescenza programmata, diritto alla riparazione, Treehugger, Kyle Wiens, Wiens, iFixit, right to repair]
---
La notizia è che [iPhone 14 ha rinnovato radicalmente l’arredamento dei propri interni](https://www.treehugger.com/apple-s-new-iphone-is-easier-to-repair-6740681), con conseguenze interessanti. Ecco che cosa dichiara a *Treehugger* Kyle Wiens, fondatore di iFixit:

>Apple ha completamente riprogettato gli interni di iPhone 14 per renderlo più semplice da riparare. Da fuori non si vede, ma è un cambiamento grande. È il cambiamento di design più significativo in iPhone da molto tempo.

In particolare, ora è possibile cambiare lo schermo senza intervenire sul resto del *device* e levare lo schermo permette un accesso facile a tutta la componentistica interna.

Chi da piccolo si è dilettato a scuola con l’insiemistica ora si può divertire. Prendiamo gli insiemi *iPhone 14 è praticamente uguale a iPhone 13*, *Apple rende le sue macchine sempre più difficili da riparare*, *Apple pratica l’obsolescenza programmata*, *Apple vuole che si cambi iPhone tutti gli anni*, *iPhone 14 è più facile da riparare* e proviamo a intersecarli in cerca di coerenza.

Non so, sostenere nello stesso tempo che Apple persegue l’obsolescenza programmata e facilita la riparazione degli apparecchi. Oppure dire che iPhone 14 è uguale a iPhone 13 e dire contemporaneamente che vuole far cambiare telefono tutti gli anni.

È troppo tardi perché abbia lucidità sufficiente a calcolare tutte le intersezioni possibili tra gli insiemi. Ma vedo a colpo d’occhio che un gran numero di esse fa apparire meno brillanti della media le opinioni espresse. Tutte molto rispettabili, se non cozzassero con i fatti.