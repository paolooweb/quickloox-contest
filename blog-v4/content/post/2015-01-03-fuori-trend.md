---
title: "Fuori trend"
date: 2015-01-05
comments: true
tags: [NetMarketShare]
---
I dati di traffico di [NetMarketShare](http://www.netmarketshare.com) sono sempre stati da prendere con beneficio di inventario, perché si basano su un campione di siti che serve a scopi commerciali e non è stato rifinito a fini statistici.<!--more-->

Tuttavia, se il singolo dato era intrinsecamente discutibile, il *trend* – la tendenza – aveva un qualche valore. Anche con i dati sbagliati, l’insieme delle loro variazioni probabilmente seguiva l’andamento generale del mercato. Chrome al venti percento dei *browser*, per dire, era un dato discutibile in sé; Chrome in crescita o in flessione mese dopo mese era una informazione ragionevolmente attendibile.

Poi è successo qualcosa: NetMarketShare ha smesso di elaborare i dati grezzi che aveva e [ha cominciato a *pesarli*](https://macintelligence.org/posts/2013-07-03-una-giornata-qualsiasi/). Le visite registrate dai siti della rete di rilevazioni non erano più tutte uguali, ma per esempio una visita su un sito cinese valeva nei calcoli più della visita su un sito canadese.

Il sistema può funzionare se la pesatura segue criteri centrati e compensa effettivamente i difetti del campione. Adesso, dopo avere constatato nel tempo un paio di modifiche cosmetiche tese a dare una immagine migliore di Windows, sappiamo che la pesatura è proprio sbagliata.

L’ultima serie di dati mensili di NetMarketShare [ha mostrato andamenti completamente irragionevoli](http://www.zdnet.com/article/more-weird-science-web-analytics-firms-kick-off-new-year-with-suspicious-statistics/). Anche senza una laurea in statistica ci si rende conto con un’occhiata che qualcosa non va.

I dati sono stati rapidamente rielaborati e ora mostrano una flessione di 8.1 minore di prima. Visti gli incrementi precedenti, probabilmente erano anche sbagliati i dati di crescita. Peggio ancora, Internet Explorer mostrava una diminuzione di trenta punti percentuali, che farebbe tanto piacere, ma è del tutto irrealistica.

In altri termini, sapevamo già che il campione non era ottimale. Ora sappiamo che neanche rispecchia l’andamento generale. E sappiamo pure che la pesatura non compensa il primo difetto, né il secondo.

Non resta più nulla. NetMarketShare è fuori bersaglio anche sulla tendenza del traffico. Nella pesatura c’erano problemi nascosti di portata clamorosa. Non ci sono strumenti per valutare l’efficacia delle correzioni attuali. Sempre più i dati si avvicinano a una proiezione delle opinioni dello staff di NetMarketShare che alla realtà.

Personalmente non aprirò più quel sito e ne ignorerò le conclusioni. Invito chiunque a fare lo stesso o a mostrarmi con informazioni concrete che questa posizione è sbagliata.