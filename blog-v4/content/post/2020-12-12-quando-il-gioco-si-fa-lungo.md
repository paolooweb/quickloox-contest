---
title: "Quando il gioco si fa lungo"
date: 2020-12-12
comments: true
tags: [M1, giochi, Apple, Silicon]
---
Da quando avevo segnalato al volo [l’esistenza di un foglio Google condiviso per raccogliere dati sulla prestazioni dei giochi in ambito M1](https://macintelligence.org/blog/2020/11/26/recensioni-a-vapore/) la situazione si è evoluta.

Ora abbiamo il sito [Apple Silicon Games](https://applesilicongames.com) in tutto il suo splendore, con oltre quattrocento giochi messi alla prova, motore di ricerca interno e note aggiuntive varie su prestazioni, Mac, financo controller.

La striscia di informazioni e di segnalazioni inizia ad allungarsi; è già significativo di per sé che succeda qualcosa del genere. In altre circostanze, nemmeno si sarebbe pensato alla mera installazione di giochi su Mac.

Il quale non diventerà mai una piattaforma gaming come la intendono di là: Mac nasce da sempre per raggiungere obiettivi e mercati di carattere generale e quella è una funzione specifica. Che poi, dal punto di vista di Apple, è già assolta in mille modi. I *gamer* sono affezionati ai giochi AAA, alle tastiere rinominate, ai setup immersivi: ho giocato a [World of Warcraft](https://eu.shop.battle.net/en-gb/product/world-of-warcraft-shadowlands) per anni e so bene che cosa voglia dire. Ed è un livello di coinvolgimento ancora inferiore a quello di titoli più recenti, [Fortnite](https://www.epicgames.com/fortnite/en-US/home) per non fare nomi.

Nel contempo, per ogni *gamer* con mouse a diciotto pulsanti, un milione di giocatori casuali fa cose ludiche su iOS e iPadOS, per esempio: non parlo per forza dei *casual game* più triti. Ho passato nottate su [Hearthstone](https://playhearthstone.com/en-us) che sì, è un gioco di carte collezionabili. Certamente non è un gioco AAA; altrettanto certamente è roba proprio diversa da [Flappy Bird](https://flappybird.io), con tutto il rispetto.

Il nostro piccolo gruppo di appassionati di [Battle for Polytopia](https://apps.apple.com/it/app/the-battle-of-polytopia/id1006393168) [cerca sempre nuovi adepti](https://macintelligence.org/blog/2020/04/27/cose-da-lockdown/) e il gioco, dopo il recente aggiornamento, è diventato ancora più godibile. Non succede più con la stessa frequenza di un tempo, ma talvolta mi perdo ancora in un [Mud](https://macintelligence.org/blog/2020/07/26/un-posto-per-l-estate/); con la primogenita è stato bello [scoprire insieme Monument Valley](https://macintelligence.org/blog/2020/04/13/la-valle-della-tranquillita/) prima e seconda edizione. Gioco a [Dungeons and Dragons](https://dnd.wizards.com) coadiuvato da [Roll20](https://roll20.net) e il computer è solo un aiutante, ma accidenti se è giocare.

Faccio esempi personali solo perché so di che cosa parlo e non certo per dare un esempio o pretendere di essere la norma; a giocare nei Mud saremo in centomila se va bene in tutta Italia. Voglio solo dire che *giocare* ha una quantità notevole di significati e ad Apple quello associato a [Call of Duty](https://www.callofduty.com/home), per fare un nome, importa ben poco, non certo al punto di farci un Mac intorno.

Fine della divagazione. Il succo è che sui Mac M1 i giochi girano, anche con Rosetta 2. Alcuni solo per onor di firma, altri fanno onestissimi sessanta fotogrammi al secondo in configurazione preimpostata. Sul primo esemplare di M1, il modello base, uscito dall’altroieri, con codice Intel tradotto – non emulato – da Rosetta! Perfino su un portatile, ripeto, un portatile *senza ventole*, magari a otto gigabyte di memoria. Parliamone.

Chi non vuole capire che si è avviato qualcosa di grosso, si troverà in imbarazzo sul lungo (periodo). Forse anche se è un *gamer* appassionato.