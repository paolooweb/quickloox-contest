---
title: "Non c'è più trippa"
date: 2020-06-01
comments: true
tags: [Campanile, MSN, Apple, News]
---
Diversi scrittori hanno iniziato la carriera come cronisti di _nera_ oppure nella _cucina_ redazionale, a preparare non le pause pranzo ma le notizie ordinarie, quelle che chiunque potrebbe scrivere.

Il mio aneddoto preferito, per quanto non certificato, è quello su [Achille Campanile](http://www.campanile.it), tra i maggiori umoristi del XX secolo. Una vedova visita ogni giorno la tomba del marito fino a quando, proprio lì, la coglie un malore fatale.

Campanile titola la notizia _Tanto va la gatta al lardo_.

Nasce uno scandalo che arriva fino alla scrivania del direttore del quotidiano, il quale pensa _o è un pazzo, o è un genio_ e – preferita la seconda ipotesi – lo mette a lavorare alla terza pagina, per tradizione quella dai contenuti più nobili e culturalmente elevati.

Questo tipo di esperienza oggi sarebbe impossibile. Un novello Campanile dovrebbe scrivere un titolo ottimizzato per i motori di ricerca, cioè omologato alla massa.

Le notizie hanno perso qualunque valore e non stupisce un'altra notizia, relativa al prossimo passo: [MSN sostituisce i curatori delle news con l'intelligenza artificiale](https://www.seattletimes.com/business/local-business/microsoft-is-cutting-dozens-of-msn-news-production-workers-and-replacing-them-with-artificial-intelligence/).

Il sito è avvantaggiato dal fatto che da anni non produce news proprie e paga siti partner per ripubblicare le loro. Nondimeno, le persone interessate dalla decisione sono una cinquantina ed è assolutamente una prima volta per un ambito editoriale di queste dimensioni.

Preferisco la controtendenza di Apple, che [ha scelto per Apple News un curatore umano e di rango](https://macintelligence.org/blog/2017/05/28/umanamente-impossibile/), anche [a scapito del ritorno economico](https://macintelligence.org/blog/2019/02/25/notizie-per-umani/).

Spero di potermi ancora permettere il lusso di leggere un titolo sorprendente, un paragrafo fulminante, un paradosso prepotente, qualcosa che solletichi la mente.

La mia intelligenza è sempre stata quello che è, comunque mai bassa come una artificiale.