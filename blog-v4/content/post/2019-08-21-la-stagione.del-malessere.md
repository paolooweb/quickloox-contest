---
title: "La stagione del malessere"
date: 2019-08-21
comments: true
tags: [Swift, SwiftUI, Catalyst, NeXT, Jobs, WWDC, Objective-C]
---
Sì, in questo periodo cerco di generare malessere e inquietudine capaci di portare verso [Swift](https://swift.org/) una persona interessata almeno vagamente alla programmazione e abitante dell’ecosistema Apple.

Oggi per farlo mi servirò di *inessential* di Brent Simmons e del suo articolo [Termina l’era NeXT; inizia l’era Swift](https://inessential.com/2019/06/07/the_next_era_ends_the_swift_era_begins).

È apparso poco dopo [WWDC](https://macintelligence.org/posts/2019-06-06-sogni-e-realta/) ma potrebbe essere stato scritto ieri e spiega bene come l’annuncio di SwiftUI segni l’inizio della fine di un’epoca lunghissima in cui il software Apple era legato a Objective-C.

Da adesso in poi sarà sempre più legato a Swift, con vantaggi vari tra i quali anche la possibilità di documentarsi mentre la materia è ancora fresca e in fase di consolidamento. Molto più facile che aprirsi a nozioni di cui milioni di altri sono già padroni da quando ancora si usavano le lire.

Da [Swift Playgrounds](https://www.apple.com/swift/playgrounds/) in su, se avanzano giorni di vacanza, sappiamo che cosa fare.
