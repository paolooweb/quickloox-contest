---
title: "Evoluzionismo e iPad"
date: 2016-08-21
comments: true
tags: [LiquidText, Goodreader]
---
Giù le mani da [Goodreader](https://www.goodreader.com), che mi serve in modo eccellente da anni e soddisfa pressoché ogni mio volere in tema di Pdf su iPad, e [anche di più](https://macintelligence.org/posts/2014-05-09-il-vero-office/).

Però ammetto che a leggere la [recensione di LiquidText pubblicata da MacStories](https://www.macstories.net/ios/liquidtext-3-0-a-uniquely-digital-pdf-experience/) mi viene l’acquolina.

LiquidText parte dall’idea di un tuttofare per leggere i Pdf, e va bene. Poi aggiunge l’idea di annotare i Pdf, e va benissimo. Dopo di che arriva all’idea di sfruttare al massimo Apple Pencil per andare oltre la semplice idea di annotazione, per applicare una idea geniale: permettere l’uso della Pencil anche per compiere *gesti*, che legano insieme annotazioni e poi mille altre cose.

Lascio il gusto della sorpresa a chi vorrà leggere la recensione e rifletto. L’evoluzione dell’annotazione Pdf su iPad passa anche da Goodreader e probabilmente non sarebbe accaduta senza Apple Pencil.

Come dire che il sistema-iPad, oltre che un ottimo strumento di lavoro studio e intrattenimento, è anche una piattaforma di innovazione, dove è lecito e possibile investire su nuove idee. E la piattaforma continua a evolvere, portando concetti di applicazione sempre nuovi e progressivamente più avanzati.

Non sono un annotatore accanito, ma LiquidText mi fa venire quasi voglia di applicarmi. Esiste un programma che consenta analoga libertà su hardware con la stessa forma di iPad, ma altri marchi? Dubito. Di penne, stili, punte scriventi per gli schermi sensibili al tocco ne esistono tante, da un pezzo; è mai uscito qualcosa di paragonabile a quello che è stato messo a disposizione per Apple Pencil?

Se la risposta è davvero *no* come credo, c’è da farsi delle domande sul ruolo di certe idee di *design* e anche più pedestremente certi accessori. Che va oltre l’ovvia fruizione e apre a menti brillanti possibilità nuove, come altri prodotti non fanno.