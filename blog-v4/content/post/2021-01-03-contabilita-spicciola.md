---
title: "Contabilità spicciola"
date: 2021-01-03
comments: true
tags: [Calca, Pages, BBEdit, Swift, Playground, Xcode]
---
Manco di una vera gestione della mia fatturazione: produco le fatture in Pages e nient’altro. Il motivo è presto detto: sono un libero professionista che tipicamente si dedica a una manciata di clienti. Per ragioni di praticità si tende a condensare la fatturazione e alla fine mi trovo a maneggiare poche decine di documenti l’anno.

I nomi file riportano date e importi; tenere un progressivo degli incassi e del calcolo delle tasse presunte, con una manciata di nuove voci mensili, è banale. Le intestazioni dei clienti sono sempre quelle e mi basta duplicare una vecchia fattura e cambiare importo e data, quando me ne serve una nuova. Se servono elenchi di qualche tipo, prendo i nomi file e con due espressioni regolari ho qualunque cosa mi serva in BBEdit, nel giro di istanti.

All’inizio facevo tutte queste cose in un foglio di calcolo. Nel mio caso, è uno di quei classici compiti per il quale un foglio di calcolo è completamente inutile. Meglio, mi fa risparmiare zero tempo di fronte a una complessità aggiuntiva.

Quest’anno farò un esperimento e proverò a tenere nota delle fatture con [Calca](http://calca.io), un editor di testo che riconosce Markdown e permette di assegnare variabili, definire funzioni, eseguire calcoli. In pratica, potrei scrivere cose tipo

`Fattura 1 = 100`<br>
`Fattura 2 = 90`<br>
`Fattura 3 = 112`

e poi

`Fatturato = Fattura 1 + Fattura 2 + Fattura 3`

e magari

`IVA totale = Fatturato * 0,22`

Le stesse cose che avverrebbero in un foglio di calcolo, certo; ma con la maneggevolezza del testo scorrevole e la velocità di formattazione di Markdown. Per assurdo, potrei scrivere l’anno fiscale in forma di racconto, con tutte le cifre al loro posto, che si aggiornano con il procedere dei mesi. Se io fossi un’azienda, avrei in un colpo solo la contabilità spicciola e l’ossatura del rapporto annuale, in un unico documento.

Sarebbe uguale farlo dentro un *playground* Swift in Xcode, solo che lanciare Xcode è un’operazione mastodontica e invece Calca è una applicazioncina minima che oltretutto c’è per Mac, iPhone e iPad, così che i conti sono sincronizzati su tutti gli apparecchi e stanno in un file di testo di poche centinaia di byte.

Sono sicuro che si possa fare uguale uguale con un qualche *notebook* Python e poi però inizio a chiedermi cose: se per esempio si possa fare direttamente in BBEdit con qualche estensione (non credo, anche se dovrei studiarmi bene i Worskheet) o in Emacs (più probabile). O se c’è qualche webapp che si comporti allo stesso modo.

Ma la vera domanda, ora che ho fatto mente locale, è: c’è ancora qualcuno che, con queste possibilità a disposizione, usa un foglio di calcolo? E perché?