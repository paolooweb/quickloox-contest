---
title: "A costo accessibile"
date: 2018-02-27
comments: true
tags: [Apple, Park]
---
Una serie di [tweet](https://twitter.com/xarph/status/967652885604450305) corredati da fotografie sulle caratteristiche di accessibilità di Apple Park.<!--more-->

<blockquote class="twitter-tweet" data-lang="en"><p lang="en" dir="ltr">The Apple Park Visitor&#39;s Center is a demonstration of an accessible building when cost is no object. 1/?</p>&mdash; @xarph (@xarph) <a href="https://twitter.com/xarph/status/967652885604450305?ref_src=twsrc%5Etfw">February 25, 2018</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script> 

Pulsanti strategici ad altezza di carrozzina, esposizione di prodotti ad altezza compatibile sempre con la carrozzina, spazi delimitati con illuminazione in aiuto degli ipovedenti, percorsi identici tra i tavoli per chi cammina e chi non può eccetera.

Come giustamente fa notare l’autore, così si fa accessibilità quando il costo non è un problema.

Quando si parla di prezzi, se comprando un iPhone X pago (anche) strutture accessibili a un disabile che voglia fare lo stesso, allora la discussione sul costo dovrebbe prendere una piega diversa da quella usuale.