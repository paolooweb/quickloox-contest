---
title: "Ti leggo per le feste"
date: 2023-08-12T01:43:14+01:00
draft: false
toc: false
comments: true
categories: [Web, Software]
tags: [Ars Technica]
---
La probabilità che un lettore di questo post si astenga dal lavoro almeno fino a martedì compreso è elevata e per questo suggerisco una lettura per le vacanze: [Come Ars Technica provvede allo hosting del proprio sito](https://arstechnica.com/information-technology/2023/08/ars-on-aws-04/), scritto da loro stessi.

Il link porta al quarto e ultimo articolo della serie, all’inizio del quale sono diligentemente elencati gli altri tre. Il livello è discretamente tecnico, nel senso che avere un’idea di che cosa sia per esempio un *load balancer* aiuta.

*Ars* ha scelto uno hosting cloud basato su Amazon Web Services, ma è solo l’inizio della storia; c’è molto più di questo. A me ha fatto bene per rinfrescare certe nozioni e anche accorgermi di come si sia aggiornato il panorama. Chi più chi meno siamo alle prese con hosting piuttosto semplici o anche con nessuno hosting e scoprire come lavora chi deve amministrare volumi di traffico importanti con un occhio di riguardo per l’efficienza e due per il risparmio diventa un bel corso di aggiornamento.

Speravo, per puro bastiancontrarismo, che non usassero WordPress. Il contrario aveva anch’esso probabilità molto elevata di verificarsi e con la probabilità non si combatte, si spera solo in un lancio favorevole dei dadi.

A tutti auguro il cento percento di probabilità di passare qualche giorno di festa come si deve, ché serve ed è meritato.

*Gli aggiornamenti in agosto del blog potrebbero essere irregolari o infrequenti.*