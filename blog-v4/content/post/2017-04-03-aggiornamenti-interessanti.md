---
title: "Aggiornamenti interessanti"
date: 2017-04-03
comments: true
tags: [watch, iPhone, Palm, Pilot, 10.3, Apfs, 10.3.1]
---
Mi ero chiesto se l’aggiornamento di iPhone a iOS 10.3, con conseguente passaggio al filesystem Apfs, fosse stato [possibile solo da iTunes](https://macintelligence.org/posts/2017-03-30-adesso-si-adesso-no/) per inconveniente tecnico oppure volontà.<!--more-->

Ora tendo a scegliere la seconda opzione, perché 10.3.1 è arrivato regolarmente su iPhone e ho aggiornato da lì come faccio di solito.

Da notare che contestualmente ho aggiornato anche a watch e, tra le opzioni di risposta a un messaggio, c’è anche la scrittura a mano. Un carattere alla volta, da scrivere con il dito sul quadrante.

Dai primi collaudi funziona benissimo e risulta assai comodo. Non si vedeva la scrittura un carattere per volta Dai tempi di [Palm Pilot](https://en.wikipedia.org/wiki/PalmPilot), ma allora l’alfabeto era in codice e occorreva tracciare segni *ad hoc* in luogo delle lettere.

Rimango sempre un po’ sconcertato dagli aggiornamenti minori che contengono novità effettive di interfaccia e funzioni, anche se oramai la tendenza è consolidata da qualche anno. In casi come questo però ne vedo la convenienza, specie se la funzione è davvero utile, interessante e bene applicata.