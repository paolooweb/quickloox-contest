---
title: "Uno vale molti"
date: 2021-04-26T00:37:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [M1, ExtremeTech, Apple, x86, Intel, Amd, Arm] 
---
Se si segue Apple, è  perché è interessante e raramente scontata. A volte ti costringe a cambiare un pensiero abitudinario. A volte prende una nozione canonica e la ribalta.

Pensiamo per esempio al dibattito *Bello iMac, peccato che alla fine abbia lo stesso processore di un MacBook Air*. È una considerazione che viene dal modo classico di pensare il rapporto tra processore e potenza, quindi valore, del computer.

Per fortuna ci sono testate come *ExtremeTech*, impossibili da etichettare come sbilanciate a favore di Apple, che spiegano le cose a chi preferisce tenere in esercizio le sinapsi invece che calcificare il pensiero.

Spiega *ExtremeTech* che, con il *system-on-chip* M1 di Apple, il modo tradizionale di pensare al ruolo dei processori ha smesso di funzionare. Ci vuole un modo nuovo. Il suo pezzo ha come titolo [Il posizionamento di M1 ridicolizza l’intero modello di business di x86](https://www.extremetech.com/computing/322120-apples-m1-positioning-mocks-every-x86-cpu-amd-and-intel-have-ever-launched), l’architettura Intel protagonista degli ultimi trent’anni e più. Non è un sito acchiappaclic. Se titola così forte, ci sono ragioni.

>Secondo Apple, M1 è la Cpu giusta per un computer da 699 dollari, un computer da 999 dollari e un computer da 1.699 dollari. È il chip giusto per avere la massima autonomia con la batteria e la Cpu giusta per avere prestazioni ottimali. Vuoi le prestazioni straordinarie di un iMac M1 ma non puoi permetterti (o non ti interessa) uno schermo costoso? Compri nu Mac mini da 699 dollari, che ha la stessa Cpu. Il posizionamento di M1 è quello di una Cpu economica al punto di poter essere venduta a 699 dollari, potente a sufficienza per costarne 1.699, ed efficiente il giusto per una tavoletta e un paio di portatili di prezzo intermedio.

Qui è dove la gente abituata a pensare per riflesso incondizionato si perde. Se costa poco, deve avere un chip da poco… se costa tanto, vuol dire che c’è un chip superiore. Pensano che avere lo stesso processore su più modelli diversi sia un’offerta al ribasso. È l’opposto: è una condizione di superiorità tecnologica netta. Prosegue *ExtremeTech*:

>Nessuna Cpu x86 è venduta o posizionata in questo modo, per tre ragioni. La prima: gli acquirenti di PC si aspettano generalmente di avere famiglie di prodotto con sistemi di fascia superiore che offrono Cpu più veloci. La seconda: Intel e Amd beneficiano di una narrazione vecchia decenni, che mette la Cpu al centro dell’esperienza di utilizzo, e progettano e vendono i loro sistemi di conseguenza, anche se la narrazione è in qualche modo meno vera di quanto fosse in epoche precedenti. Terza: nessuna Cpu x86 sembra in grado di uguagliare contemporaneamente i consumi e le prestazioni di M1.

M1 è talmente superiore nel complesso (questo è il punto importante: non solo il consumo o le prestazioni, ma *entrambi, insieme*) che, all’interno della stessa generazione di prodotti, la scelta del chip è irrilevante.

Questo sconvolge sistemi e abitudini di pensiero vecchie una generazione (umana). E arriveranno parenti di M1 a sancire la stessa superiorità anche sulle line di prodotto più alte. Oggi i Mac di base (e iPad Pro) montano un processore che, semplicemente, può essere forse sconfitto in una partita, ma vince per forza ogni serie di playoff. E hai voglia a dire che sì, c’è il PC che fa le stesse cose e costa meno. Non costa meno se le fa, e se costa meno non le fa. A partire dal processore.

*Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile.*