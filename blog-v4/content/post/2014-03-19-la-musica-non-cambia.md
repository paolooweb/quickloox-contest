---
title: "La musica non cambia"
date: 2014-03-19
comments: true
tags: [Windows, Mac]
---
La storia di oggi la racconta **Flavio** ed è singolare come valga oggi ma valeva anche nel secolo scorso; c’è una novità tutti i giorni, solo che certi fondamentali restano immutati.<!--more-->

>Treno 25069 della Linea S11, proveniente da Chiasso e diretto a Milano Centrale (*Attenzione! Allontanarsi dalla linea giaaalla*). Di fianco a me un tizio altero, dalle fattezze e atteggiamento elvetici. In grembo, un raffinato e sottilissimo laptop HP, nuovo fiammante, ultimo grido. Mi cade l'occhio: su Outlook sta terminando di redigere una lunga, elegantissima e tecnicissima email in inglese, il cui incipit è "Dear Nigel". Per rispetto, non leggo oltre e mi volto verso il finestrino. Bello, però, rifletto, poter lavorare in mobilità. Il treno riparte, lui preme "send" con il nuovissimo trackpad a due tasti e all'improvviso, l'imprevedibile: schermata blu. Ricevo una telefonata, mi distraggo giusto mentre sullo schermo del nuovissimo HP compare il logo del boot di Windows. Termino la telefonata diversi minuti dopo, e guardo nuovamente il mio compagno di viaggio. Il laptop mostra ancora la schermata di logon di Windows. Lo spegne e lo riaccende, impassibile (il treno proviene da Chiasso…). Questa volta Windows riparte correttamente, ma la mia fermata si avvicina. Lancio un'ultima occhiata, se non altro per solidarietà "tecnologica": ha aperto Outlook e sta scrivendo una mail, in inglese: "Dear Nigel…"

>Scendo dal treno. Ringrazierò sempre chi a suo tempo mi fece conoscere il mio primo Mac.