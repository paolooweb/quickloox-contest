---
title: "Ideali e illusioni - mobile"
date: 2023-07-06T12:45:05+01:00
draft: false
toc: false
comments: true
categories: [Hardware]
tags: [Fairphone, The Verge]
---
Suscita giusto interesse [l’arrivo negli Stati Uniti di Fairphone 4](https://www.theverge.com/2023/7/5/23783714/murena-fairphone-4-us-release-date-price-sustainability-repair), nell’intento di ottenere un riconoscimento apprezzabile di vendite.

[Fairphone](https://shop.fairphone.com/it) è partito nel 2021 come classico progetto *equo e solidale*, *sostenibile* e tutto il resto. Si smonta facilmente con l’aiuto di un solo cacciavite.

Offre nella versione 4 anche qualche funzione interessante, come la doppia SIM, e ha una garanzia di cinque anni.

C’è anche una partnership con [Murena](https://murena.com/shop/smartphones/brand-new/murena-fairphone-4-eu/), che vende versioni deGooglizzate, con il sistema operativo /e/OS, praticamente una rilavorazione di Android che taglia fuori gli agganci con cui Google succhia dati personali ai proprietari, ma lascia la libertà di usare le app Android.

Quindi: smartphone *libero*, non legato alle multinazionali, etico.

Va notato che il prezzo è leggero ma non leggerissimo: il modello base sta sui seicento euro, quello superiore arriva fino ai settecento. In Italia si può comprare direttamente un Fairphone 4 dall’azienda, ma con Android 12; comprarlo da Murena, deGooglizzato, costa quaranta-cinquanta euro in più.

Non voglio entrare nella diatriba dei prezzi, che in realtà non esistono, in informatica: si compra quello che si desidera. Chi spende poco, vuole spendere poco. Chi vuole molto, lo compra. Esistono le rate, i contratti con i gestori, i tassi zero, il ricondizionato… chi attribuisce una decisione di acquisto a fattori esterni, mente, magari inconsapevolmente, ma mente. Va comunque notato che l’apparecchio equosolidale vuole la sua parte.

Quello che da un po’ fastidio è la parte in cui *The Verge* parla di componentistica *ethically sourced*, procurata in modo etico. Il sito Fairphone trabocca di certificazioni, si dichiara *neutrale rispetto ai rifiuti*, [mostra impegno su tutti i fronti](https://www.fairphone.com/en/impact/fair-materials/). Però riassume:

>Stiamo lavorando per integrare cobalto e litio equi nelle nostre batterie.

Sul sito c’è una lunga dissertazione sul ricorso alle forniture artigianali, alla costituzione di una alleanza equa sul cobalto eccetera eccetera.

Sono temi pesanti e difficili, aree del mondo dove nessuno di chi discetta di sostenibilità vorrebbe vivere per dieci minuti, dove la vera etica è, sarebbe, vorrebbe essere fermare lo sfruttamento feroce dell’ambiente e delle persone.

Occorre avere un’idea di come vanno le cose sul posto, instaurare trattative magari complicate, avere l’autorità necessaria per battere i pugni sul tavolo se serve e affamare un signore della guerra togliendogli un contratto di fornitura a costo di fare più fatica altrove.

Per fare questo occorrono organizzazione e mezzi, persone, investimenti, capacità di sopportare perdite in funzione di un obiettivo più a lungo termine e così via.

Sul cobalto o sul litio, raggiungerà risultati più equi Fairphone o qualche altra azienda che fabbrica quattro milioni di apparecchi alla settimana e ha una scala di intervento e di priorità decisamente più ampie?

Bello Fairphone, ma non bisogna farsi illusioni né credere a tutto quello che viene scritto.