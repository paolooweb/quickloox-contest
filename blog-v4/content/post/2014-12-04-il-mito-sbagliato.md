---
title: "Il mito sbagliato"
date: 2014-12-05
comments: true
tags: [Folklore, AppleI, Wozniak, Woz]
---
Il mondo ha titolato sulla caduta del mito del garage dove sarebbe nata Apple, grazie a una [intervista a Steve Wozniak su Bloomberg](http://www.businessweek.com/articles/2014-12-04/apple-steve-wozniak-on-the-early-years-with-steve-jobs).

>Il garage è un po’ un mito. Non ci abbiamo fatto progettazione, breadboarding [cablaggio di schede per circuiti stampati], pianificazione di prodotto. Non ci abbiamo fatto produzione. Il garage non aveva questo grande scopo, tranne che potevamo sentirlo come casa. Non avevamo soldi. Quando non hai soldi devi lavorare fuori da casa.

Il senso è che Apple I lo avrebbe costruito lui da solo, nel suo cubicolo di impiegato HP.

Verosimile. Certo che dirlo magari prima del 5 ottobre 2011 avrebbe procurato miglior figura. Non sembra un segreto clamoroso, quasi quarant’anni dopo gli eventi. A trentacinque sarebbe stato uguale.

Più *Woz* avanza in età più sembra cercare popolarità, che godersela. Per sapere del passato remoto di Apple preferisco appoggiarmi sempre più a fonti più rispettose e meno sensazionalistiche, come [Folklore](http://www.folklore.org) di Andy Hertzfeld.

Se c’è un mito che sta cedendo ai miei occhi, è progressivamente quello di Steve Wozniak.