---
title: "Se ci fosse un critico"
date: 2016-03-06
comments: true
tags: [Note, Contatti]
---
Da notare che riesco a sentire ogni tipo di lamentela sulle interfacce e sui comportamenti dei sistemi Apple, ma nessuno che abbia alzato una mano per fare presente come sia completamente impossibile modificare il font preimpostato nelle Note di OS X.<!--more-->

Che da un punto di vista di usabilità è una enormità e meriterebbe ben più che una tirata d’orecchi o uno sbuffare nervoso. A paragone di questo, Contatti – la *app* peggiore di OS X, di gran lunga – sembra un paradiso della éuser experience*.