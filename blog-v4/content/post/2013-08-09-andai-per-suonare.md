---
title: "Andai per suonare"
date: 2013-08-09
comments: true
tags: [Mac, iTunes]
---
Ho sempre manifestato una certa contrarietà all’idea di tenere un lettore di musica indipendente su Mac nel momento in cui la cartella Applicazioni contiene iTunes.<!--more-->

È agosto; ho provato [Vox](http://coppertino.com/vox/).

Apparentemente mi sono perso qualche anno di sviluppo software nel campo della riproduzione musicale.

Finisce anche che pago quegli ottantanove centesimi per abilitare la radio Internet, l’unico punto dove la concorrenza di iTunes può dire qualcosa.