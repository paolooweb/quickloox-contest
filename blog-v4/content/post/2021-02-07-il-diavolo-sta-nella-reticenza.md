---
title: "Il diavolo sta nella reticenza"
date: 2021-02-07T02:08:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [Microsoft, Raspberry Pi, Visual Studio Code] 
---
Un recente aggiornamento a Raspberry Pi OS aggiunge alla lista dei repository (i depositi) di codice un server Microsoft, ufficialmente a supporto degli utenti di Visual Studio.

Il diavolo non sta qui.

Viene aggiunta anche una chiave GPG di Microsoft usata per firmare digitalmente i pacchetti software. Quello che Microsoft aggiunge al repository verrà automaticamente considerato affidabile dal sistema operativo.

Il diavolo non sta neanche qui.

Il nuovo repository viene aggiunto alla lista anche se viene effettuata una installazione minimale del sistema, senza interfaccia grafica, al minimo indispensabile.

Neppure qui si vede il diavolo.

L’aggiunta è stata fatta *senza dirlo*. E per due settimane non è nemmeno comparsa sulla pagina GitHub del software, che è *open source*. *Open source* significa *codice aperto*, pienamente visibile e modificabile. Il cambiamento è apparso poche ore dopo che su Reddit si era accesa la discussione tra gli sviluppatori.

Il diavolo sta qui.

Ci sono diverse alternative per chi voglia usare un Raspberry Pi senza ritrovarsi un repository Microsoft installato *surrettiziamente* su software libero e indipendente.

Per ora. Il diavolo lavora incessante.

*Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile.*