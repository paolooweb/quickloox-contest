---
title: "Vota altro"
date: 2022-08-23T00:46:09+01:00
draft: false
toc: false
comments: true
categories: [Internet]
tags: [elezioni]
---
Una guida rapida alla decisione elettorale in occasione delle prossime Politiche.

Una premessa: in tutte le votazioni esiste una percentuale di voti cosiddetti di protesta, espressi sotto forma di schede bianche o nulle.

Chi avesse mai vissuto l’esperienza dello scrutinio in qualche seggio consistente, saprà che uno degli sport preferiti consiste nel riuscire ad assegnare comunque una bianca o una nulla al proprio partito, mediante cavilli, pretesti, interpretazioni tendenziose e forzature. Se voto di protesta ha da essere, sconsiglio con forza di esprimerlo via scheda bianca o nulla.

Ciò detto, veniamo a qualche suggerimento per chi vuole scegliere un partito.

Diffidare dei leader che parlano di soldi. Dovrebbero parlare di obiettivi, soluzioni, piani concreti. Altrimenti pensano al breve periodo, oppure a una nicchia di elettori, oppure alla pura ricerca di consenso.

Diffidare dei leader che criticano gli altri partiti. Se hanno proprie idee, programmi, progetti in cui credono, dovrebbero approfittare di ogni spazio possibile per valorizzarli, invece che parlare di altro. Un leader capace di riconoscere il buono nell’avversario ha il potenziale per essere una buona guida del Paese.

Per corollario, se c’è modo di porre una domanda a un candidato, suggerisco *che cosa apprezza nel programma degli altri partiti?* Un buon candidato a rappresentare la totalità degli italiani sa che non esistono il Bene e il Male alle elezioni e che un pezzettino di ragione sta ovunque, magari male distribuito, ma ovunque. I partiti che hanno sempre ragione su tutto e vedono gli altri sempre e comunque in torto sono una forma di demenza della democrazia.

Diffidare dei leader che promettono a categorie o nicchie specifiche, a maggior ragione se ci si sente rappresentati per appartenenza a quella categoria o a quella nicchia. Chi propone sgravi fiscali per i rabdomanti vuole solo ingraziarsi i rabdomanti; quegli sgravi dovrà pagarli qualcun altro. Al giro successivo sarà il turno del bonus per i palafrenieri, che pagheranno – anche – i rabdomanti. Ragionare per categorie è un pessimo criterio di scelta per governare un Paese.

Diffidare dei leader che si propongono di difendere l’Italia da un altro partito. Dovunque nell’arco costituzionale e fuori da esso, bastano cinque minuti per recuperare tonnellate di buoni motivi per volersi difendere da *qualsiasi* partito. Il punto non è avere difetti, che tutti hanno, ma che cosa potrebbe fare un partito, pure tenendo conto dei suoi difetti.

Infine, il suggerimento aureo. Sempre e comunque, soprattutto se si è sempre votato quello per tutta la vita, se la madre la nonna e la bisnonna hanno sempre votato lì, se non si vede alcuna altra alternativa (e quindi si guarda con occhio distorto) ma, ripeto, sempre e comunque, *votare qualcos’altro* rispetto alle scorse Politiche.

L’alternanza in democrazia è un valore. Non esistono partiti Buoni e partiti Cattivi e nessun partito ha ragione o torto al cento percento. Qualsiasi scelta si faccia, che non sia automatica, ripetitiva, preconcetta, scontata; a votare così possono essere al massimo dei computer.

Buon voto.