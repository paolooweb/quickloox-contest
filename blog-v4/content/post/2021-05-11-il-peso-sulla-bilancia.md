---
title: "Il peso sulla bilancia"
date: 2021-05-11T18:04:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [Dr. Drang, M1, MacBook Air, Intel] 
---
Dr. Drang osserva acutamente che la grande autonomia di un MacBook Air M1 viene anche dal fatto che [la nuova logica sta dentro il vecchio case](https://leancrew.com/all-this/), creato per alloggiare la più ingombrante e riscaldante attrezzatura Intel.

Di conseguenza, argomenta, i prossimi MacBook Air potrebbero essere più sottili e leggeri, potendolo fare, e avranno meno autonomia, perché ci sarà meno spazio per la batteria.

La sua conclusione è acuta:

>Senza dubbio ci sono persone disposte a sacrificare cinque o sei ore di autonomia in cambio di una macchina significativamente più leggera. E potrà arrivare un tempo in cui sarò uno di loro. Non ora, però. Grazie al fortuito design ad interim di questi del primo MacBook Air M1, potrei essermi imbattuto nel computer ideale per me.

Senza Jonathan Ive, Apple vorrà insistere sullo spessore dei MacBook Air e sacrificare la batteria? Oppure punterà su un’autonomia oggi irraggiungibile per qualsiasi PC equivalente, sacrificando una ulteriore leggerezza?

Sarà un bel test per capire quale sia veramente la preferenza primaria per chi vuole MacBook Air.

*Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile.*               