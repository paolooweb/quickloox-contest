---
title: "La gemma nascosta"
date: 2023-05-17T01:39:41+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [GarageBand, Inverse, Logic]
---
È anche vero che ci troviamo in una fase di attesa che precede i fuochi di artificio di Wwdc. In ogni caso c’è un risvolto positivo: la lettura di articoli meno urgenti e leggeri, che però fanno bene il punto su tanti argomenti in agenda.

A partire da questo [Il GarageBand gratis di Apple è la gemma più sottovalutata del tuo MacBook](https://www.inverse.com/tech/apple-garageband-mac-best-free-music-recording-app).

Difficile contraddire anche una riga dell’articolo, avendo sperimentato sia pure a livello più basso tutto quanto è stato scritto. Il software non è certamente il più potente, né il più aggiornato, ma a costo zero permette di risolvere qualsiasi esigenza musicale di base e, in più, una persona competente può scavare sotto la superficie e trovare funzioni straordinariamente utili.

>Un cambiamento degno di nota, per chi come me non ha usato GarageBand da qualche tempo, è l’inclusione di una delle migliori funzioni di Logic: il batterista virtuale. Chiunque registri musica rock sa quanto sia difficile tenere traccia della batteria. Se, come me, volete solo fare un demo di tracce da registrare più professionalmente in un secondo tempo, sa quanto sia complicato trovare un batterista e registrare la sua batteria in modo che non faccia schifo.

Ovviamente GarageBand non è inteso per un uso alto professionale.

>Ma per chi non sia un professionista, GarageBand ha un sacco da offrire e, se anche non raggiunge il livello di sofisticazione richiesto da chi volesse registrare la propria musica, è impossibile non ammirare questo pezzo di software (tecnicamente) gratis che si trova su qualsiasi Mac, iPhone e iPad.

Preso dall’entusiasmo, ho aperto il mio iPad di prima generazione (quest’anno diventa un *teenager*) e ho lanciato GarageBand. Ci sono dentro i giochi e i pasticci combinati registrando ludicamente con le figlie. La facilità con cui poter pasticciare è un elemento decisivo nella mia valutazione e sono certo che, almeno sui miei sistemi, nel futuro prevedibile GarageBand sarà presente in buona evidenza in schermate home e cartelle di applicazioni utili.