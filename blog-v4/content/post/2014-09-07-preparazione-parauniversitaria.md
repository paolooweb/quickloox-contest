---
title: "Preparazione parauniversitaria"
date: 2014-09-07
comments: true
tags: [Might, Perl, awk, sed, bash, regex, grep, emacs, vim]
---
Il modo migliore per sentirsi inadeguati è frequentare il blog di Matt Might è trovare articoli come [Scolpire testo con regex, grep, sed, awk, emacs e vim](http://matt.might.net/articles/sculpting-text/).<!--more-->

Il rapporto segnale-rumore è altissimo, la leggibilità è rara a trovarsi, il contenuto è di alto livello e difficile da seguire; contemporaneamente è una occasione come poche di acquisire almeno una conoscenza superficiale, ma valida, degli argomenti in gioco.

Se il tono e il genere piacciono, raccomando vivamente anche [Programmare la shell con bash: per esempio, per controesempio](http://matt.might.net/articles/bash-by-example/) e [Una guida a Perl: per esperimento](http://matt.might.net/articles/perl-by-example/).

Sussiste il rischio di perdersi nel resto delle pagine. Ne vale la pena. È uno dei pochi siti dove è persino sopportabile la pubblicità. E c’è una montagna di cose da imparare.