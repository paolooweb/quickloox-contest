---
title: "Cronache dal dopobomba"
date: 2016-10-02
comments: true
tags: [Fallout, Shelter]
---
Ho richiamato Danielle dall’esterno. Accumulava esperienza preziosissima, ma rischiava di morire in mezzo a radiazioni e mutazioni.  Devo approfondire la questione dell’equipaggiamento degli esploratori.

Henry e Christina hanno approfittato del buon funzionamento delle cose e così il rifugio ha una bocca da sfamare in più. È una buonissima notizia perché per incrementare la popolazione non possiamo contare solo su chi arriva da fuori.

È piuttosto urgente individuare il buon equilibrio tra produzione di energia, acqua e cibo. Siamo sulla soglia di rischio in tutti e tre i casi ed è prioritario capire chi sia meglio assegnare a ciascuna attività. Sto pensando di mandare Istvan in esplorazione all’esterno perché la sua produttività è veramente molto bassa rispetto ai suoi parametri vitali che sono invece notevoli. Forse la sua vocazione è sfidare pericoli più che assicurare la quotidianità.

Una volta sistemata la produzione di energia è tempo di mettere a punto l’infermeria e il laboratorio scientifico, per minimizzare le conseguenze degli incidenti e accelerare lo sviluppo tecnologico. Sembra proprio che la tipologia del rifugio sia un fattore assai più determinante di quanto si possa pensare al primo impatto.

Il livello di soddisfazione generale è positivamente alto. Però camminiamo su un filo sottile e non si possono trascurare particolari, o la situazione potrebbe precipitare in pochissimo.

(Questo se dovessi sintetizzare una prova veloce di [Fallout Shelter](https://appsto.re/it/1V8e7.i), gioco *free-to-play* per iOS di amministrazione di un rifugio antiatomico dopo la catastrofe nucleare. Grafica accattivante e curata, giocabile a livello superficiale ma anche con possibilità enormi di approfondimento, originale e personalizzabile. Merita di essere giocato spesso a piccole dosi.)
