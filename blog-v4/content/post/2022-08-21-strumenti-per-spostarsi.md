---
title: "Strumenti per spostarsi"
date: 2022-08-21T19:09:50+01:00
draft: false
toc: false
comments: true
categories: [Hardware]
tags: [Mac, iPad, iPhone]
---
Aveva ragione Steve Jobs. Mac è un pick-up, iPad un’utilitaria, iPhone un Segway, watch un *pogo stick*.

Scritto alla fine di un lungo viaggio, ritrovando Mac ma anche la sua posizione nel suo ambiente canonico. Dopo tanti giorni in camper (iPad), siamo tornati a casa (Mac). L’estate prosegue; ci aspetta ancora qualche breve spostamento, che probabilmente compiremo in tenda (iPhone).

watch? Certamente porteremo anche una giacca impermeabile, non si sa mai.