---
title: "Un disamore progressivo"
date: 2022-12-18T03:29:16+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [Dropbox]
---
Ieri raccontavo dell’[arrivederci a iMovie](https://macintelligence.org/posts/2022-12-17-e-grazie-per-tutte-le-dissolvenze/) per la sua macchinosità e inadeguatezza nel tempo presente ove uno non cerchi i titoli di testa modello trailer o il filo diretto tra fotocamera e montaggio.

Oggi sto estraendo il cartellino giallo per Dropbox, che ha cambiato la sua posizione nel sistema dalla cartella Inizio a una delle misconosciute cartelle dei servizi cloud. E non è una cosa buona.

Intanto fa saltare alias e preferiti nella barra laterale del Finder. Vabbè, si rifanno. Però, uffa. E poi, più grave, ho l’impressione che dentro la cartella in questione non funzioni, o non funzioni bene, la ricerca classica del Finder.

Per quanto ne so, potrebbe anche essere un cambiamento richiesto da Apple; per quanto dà fastidio, è tale da farmi rivalutare l’utilità del servizio (e qui capirei il perché, se è dovuto ad Apple, quest’ultima lo ha deciso).

Anche qui, Dropbox è in trasformazione progressiva da salvavita brillante a male necessario e talvolta pure pasticcione. Progressivo è anche il disamore; ammetto che per iCloud pago tre euro al mese e per Dropbox no. Al tempo stesso, proprio non è questione di denaro, ma di esperienza d’uso.

Comincio a pensare che Steve Jobs avesse ragione, quando diceva che Dropbox *era una feature, non prodotto*.