---
title: "La linea morbida"
date: 2015-03-21
comments: true
tags: [Consolas, BBEdit, Pages, Palatino, Futura, Keynote, Typoggraphica, Menlo, Terminale]
---
Quando si passa molto tempo davanti allo schermo, una delle cose che fanno la differenza è la scelta del carattere. Menlo per il Terminale ma Consolas su BBEdit, i monospaziati non sono tutti uguali e la scrittura fluisce più veloce o più a fatica. No, non è immaginazione. Palatino per Pages, mentre se appena c’è tempo si sostituisce Helvetica con Futura in Keynote.<!--more-->

Il giusto font alleggerisce la giornata, ammorbidisce la vita e vorrei dire che concilia persino il sonno. È con questo che raccomando la rassegna delle [migliori famiglie di caratteri del 2014](http://typographica.org/features/our-favorite-typefaces-of-2014/) compilata anche quest’anno da *Typographica*.

Basta scorrere la pagina home per sentirsi più rilassati.