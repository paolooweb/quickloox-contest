---
title: "La scarica dei centouno"
date: 2023-05-05T14:14:24+01:00
draft: false
toc: false
comments: true
categories: [Internet]
tags: [Internet, Sinofsky, Steven Sinofsky, Wired, Mac]
---
Durante il trimestre invernale Apple ha venduto meno che l’anno scorso ma ha incassato di più, per via di una crescita nel numero di iPhone venduti che ha compensato cali sulle altre linee, dovuti a una vasta quantità di contingenze tra politiche COVID cinesi, guerra, inflazione e quant’altro.

A tanti non sembra vero di poter parlare di diminuzioni nelle vendite per tirare fuori la propria personalissima opinione su come dovrebbe fare Apple per salvarsi e sfuggire alla catastrofe, avendo registrato solamente novanta e passa miliardi di fatturato per ottenere un misero guadagno netto superiore ai venti miliardi (tipo, con immensa approssimazione, cinque milioni di volte quanto riesce a fare un impiegato di concetto).

Con grande tempismo, una persona totalmente competente e oramai da un pezzo sopra le parti, come Steven Sinofsky, ha opportunamente [ricordato i tempi del 1997](https://twitter.com/stevesi/status/1654328764501987328) in cui tutti erano molto generosi nello spiegare ad Apple dove sbagliava e che cosa serviva fare. In particolare, *Wired* pubblicò una lista di centouno consigli, di cui lui ricorda i più gustosi.

<blockquote class="twitter-tweet"><p lang="en" dir="ltr">Apple earnings today. Some happy (after hours trading). Some might say less than perfect. So much short-term punditry over the years has been so very wrong about Apple in the long term. <br><br>My favorite predictions came more than a quarter century ago as Apple was on the brink... 1/ <a href="https://t.co/DCBLChuRpf">pic.twitter.com/DCBLChuRpf</a></p>&mdash; Steven Sinofsky (@stevesi) <a href="https://twitter.com/stevesi/status/1654328764501987328?ref_src=twsrc%5Etfw">May 5, 2023</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

Ci si potrebbe fermare al numero uno della lista: *Ammettetelo: siete fuori dai giochi nello hardware*.

Sono state fatte al tempo anche altre liste. [Questa](https://twitter.com/stevesi/status/1654328771363872769) è spettacolare:

<blockquote class="twitter-tweet"><p lang="en" dir="ltr">6/ From Ann Winblad ❤️<br>1. Merge with Nintendo<br>2. Buy Gateway<br>3. License Win95/NT and keep the guts/make just a GUI <a href="https://t.co/4WIVCnsSRY">pic.twitter.com/4WIVCnsSRY</a></p>&mdash; Steven Sinofsky (@stevesi) <a href="https://twitter.com/stevesi/status/1654328771363872769?ref_src=twsrc%5Etfw">May 5, 2023</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

*Fondersi con Nintendo, comprare Gateway, diventare licenziatari di Windows 95/NT e farci sopra una interfaccia utente*.

E avanti così.

Bisognerebbe imparare che i risultati trimestrali, per Apple, sono semplicemente fotografie di un istante, di cui si farebbe a meno se non fossero dovute in base alla legge americana. Per fortuna, l’azienda programma a lungo termine e speriamo che continui a farlo, allungando il termine se possibile. Sul lungo termine, infatti, le sciocchezze e soprattutto i loro produttori finiscono la carica.