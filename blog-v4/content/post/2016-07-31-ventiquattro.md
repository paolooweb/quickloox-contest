---
title: "Ventiquattro"
date: 2016-07-31
comments: true
tags: [Akko, MisterAkko, Accomazzi, Kickstarter]
---
(Venti ore scarse.)
Mi ero imposto di non guardare Kickstarter questo fine settimana, tra la scaramanzia e la disposizione d’animo a svolgere comunque il lavoro di comunicazione fino al termine; ugualmente, dalla condivisione di tanti ho saputo della soglia raggiunta. [Cuore di Mela](https://www.kickstarter.com/projects/295108589/cuore-di-mela-i-segreti-di-apple-iphone-e-mac?ref=discovery) c’è.

A caldo: la comunità è (relativamente) ristretta. Chi si è impegnato lo ha fatto per somme consistenti. Questa è una responsabilità notevole che ora si porta (lietamente) sulle spalle. Dobbiamo davvero fare bene e costruire per ampliare la comunità così come per rendere il progetto veramente longevo ed efficace. Un sostegno come questo va onorato, che è il modo migliore per ringraziare.

Abbiamo dimostrato che c’è ancora spazio per idee buone e per un compenso equo, in un ambito editoriale difficile.

Vorrei riuscire a vivere l’ultima giornata di Kickstarter come se fosse la prima, sognando di vedere una cifra disordinatamente alta da usare per poter dare tanto oltre che per fare bene. E poi, da domani, iniziare a fare sul serio.
