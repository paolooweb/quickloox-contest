---
title: "Balle spaziali"
date: 2015-01-06
comments: true
tags: [Apple, Which, SurfaceRT, Microsoft]
---
Invito a guardare l’infografica sullo spazio effettivamente libero dentro vari computer da tasca, [creata da *Which?* un anno fa](http://blogs.which.co.uk/technology/phones-3/phone-storage-compared-samsung-s4-still-in-last-place/) e di cui [si era riferito](https://macintelligence.org/posts/2014-04-18-il-bicchiere-mezzo/).<!--more-->

Ora: [Apple è stata portata in tribunale](https://cdn2.vox-cdn.com/uploads/chorus_asset/file/2893306/1-main.0.pdf) per via dello spazio occupato dal software preinstallato sui propri apparecchi.

Come possano coesistere in un universo reale le due informazioni mi rimane un mistero. D’altronde l’anno è appena cominciato.

A margine: il primo Surface RT di Microsoft da 32 gigabyte [ne lasciava liberi la metà](http://betanews.com/2012/11/06/how-much-free-space-does-microsoft-surface-rt-actually-have/) e nessuno ha sporto denuncia. Forse perché nessuno se ne è accorto.