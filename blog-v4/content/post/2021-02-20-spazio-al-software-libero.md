---
title: "Spazio al software libero"
date: 2021-02-20T01:08:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [Perseverance, Nasa, G3, PowerPC, Linux, VxWorks, AirPort Extreme] 
---
Sugli aspetti tecnologici di Perseverance e della sua missione su Marte si può ancora dire qualcosa. Per prima, la battuta più risaputa: [Marte è diventato il secondo pianeta ad avere più computer Linux che Windows](https://twitter.com/mikko/status/1362763793042972673).

<blockquote class="twitter-tweet"><p lang="en" dir="ltr">Breaking: Mars becomes the second planet that has more computers running Linux than Windows. <a href="https://t.co/bsx0HukK9P">pic.twitter.com/bsx0HukK9P</a></p>&mdash; @mikko (@mikko) <a href="https://twitter.com/mikko/status/1362763793042972673?ref_src=twsrc%5Etfw">February 19, 2021</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

Meno noto forse il fatto che il laboratorio scientifico Curiosity sia [equipaggiato con un processore PowerPC G3](https://www.extremetech.com/extreme/134041-inside-nasas-curiosity-its-an-apple-airport-extreme-with-wheels). E che il [sistema operativo](https://www.windriver.com/products/vxworks/) sia lo stesso usato per le basi wireless Airport Extreme.

Tecnologia che abbiamo scartato vent’anni fa perché troppo lenta, ora conduce esperimenti mai tentati prima in un ambiente alieno.

Altre tecnologie, considerate irrinunciabili da tantissimi, sono di nessun interesse quando si tratta di scrivere una pagina nuova della storia dell’esplorazione spaziale.

Il software che fa volare il piccolo elicottero in dotazione a Perseverance è [libero e disponibile a chiunque](https://github.com/nasa/fprime) su Github.

Libero è meglio.

*Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile.*