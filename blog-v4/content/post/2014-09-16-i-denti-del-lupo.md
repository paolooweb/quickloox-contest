---
title: "I denti del lupo"
date: 2014-09-16
comments: true
tags: [Mojang, Minecraft, Xbox, Bungie, Mac]
---
Mi si dirà che porto ancora sulle spalle lo stress della storia passata, che è roba di un altro secolo e che Microsoft ha affermato di [continuare a produrre Minecraft per iOS e Mac](http://news.xbox.com/2014/09/games-minecraft-to-join-microsoft) anche dopo l’acquisto di Mojang, la *software house* che lo produce, per la bella cifra di due miliardi e mezzo di dollari.<!--more-->

Le mie paturnie infatti riguardano roba vecchia e stravecchia: quella azienda geniale che era Bungie, ridotta a esca per vendere Xbox dopo che Microsoft [se l’è comprata nel 2000](http://www.ign.com/articles/2000/06/20/microsoft-acquires-bungie) per un prezzo tra i venti e quaranta milioni di dollari.

Mi si racconta che è nell’interesse di tutti lasciare che Minecraft rimanga un prodotto e una comunità globale e sarà certamente vero. Intanto, dalla dichiarazione del responsabile di Xbox e dal [comunicato di Microsoft](http://www.microsoft.com/en-us/news/press/2014/sept14/09-15news.aspx), manca qualsiasi riferimento a Mac, nonostante si parli di mantenimento delle piattaforme attuali. Minecraft su Mac [esiste](https://minecraft.net/download).

E i fondatori di Mojang [se ne vanno](https://mojang.com/2014/09/yes-were-being-bought-by-microsoft/) da subito, a cominciare dal [creatore di Minecraft](http://notch.net/2014/09/im-leaving-mojang/).

C’è una nuova Microsoft, mi dicono, rispettosa della pluralità delle scelte, che giustamente vuole essere presente su tutte le piattaforme invece che imporre la propria schiacciando tutto il resto.

Sarà vero? Del pelo del lupo mi interessa niente, men che meno i vizi. Quelli da cui guardarsi sono i denti.