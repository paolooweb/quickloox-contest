---
title: "Si vede la killer"
date: 2017-06-17
comments: true
tags: [iPad, Stefano, ProMotion]
---
Ricevo, ringrazio, riepilogo quanto mi ha mandato **Stefano**.

**Arrivo da un “vecchio” iPad Pro 9,7” 128 gigabyte.**
Dopo circa un anno di lavoro, abbandonato definitivamente ogni laptop aziendale, cercavo una soluzione più confortevole per i miei occhi lavorando da scrivania, ma con la stessa portabilità del 9,7”: ecco il motivo per cui non mi sono buttato subito sull’iPad Pro da 12,9”.

E poi arriva il 10,5” portandosi in dote [ProMotion](https://www.apple.com/it/newsroom/2017/06/ipad-pro-10-5-and-12-9-inch-models-introduces-worlds-most-advanced-display-breakthrough-performance/).

Dopo un giorno completo di utilizzo posso dire senza ombra di dubbio:

* Apple Pencil: con 120 hertz la latenza è praticamente scomparsa, sembra di scrivere con un pennarello sul vetro.
* Lo schermo è semplicemente fantastico: difficile descrivere a parole l’esperienza d’uso. Saltare da una app all’altra è immediato e fluido. La tonalità dei colori è brillante, sembra che lo schermo faccia scomparire i bordi, soprattutto nella modalità *portrait*, leggendo un libro o una pagina web come sottolinea giustamente [la recensione](https://macintelligence.org/posts/2017-06-13-recensioni-a-catena/) di Viticci.
* Ne ho guadagnato in comfort di scrittura grazie a una tastiera più larga.
* Portabilità garantita come nel 9,7”.

Non ero molto convinto all’inizio dell’acquisto arrivando già da un iPad Pro.

Ma questo taglio di schermo è davvero l’uovo di Colombo per le mie esigenze e mi ripeto: **ProMotion è la *killer application* di questo device**.

La *killer application* di iPad Pro 10,5” non è una app. È il guardarla.