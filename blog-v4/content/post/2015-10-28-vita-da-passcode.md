---
title: "Vita da passcode"
date: 2015-10-28
comments: true
tags: [Microsoft, Exchange, iOS, watch, passcode, iPad, iPhone, Mac]
---
Dal secolo scorso i miei computer sono *Microsoft-free*, a parte una [installazione di Silverlight dentro una macchina virtuale](https://macintelligence.org/posts/2013-10-11-realta-virtuale/).<!--more-->

Ora purtroppo ragioni lavorative mi hanno costretto all’apertura di un account di posta Exchange, che ovviamente funziona su Mac, iPhone e iPad.

Non senza problemi: Exchange si rifiuta di funzionare su iOS se non è attivo il *passcode* che protegge l’accesso alla macchina. *Passcode* che per me è sempre stato un fastidio e ho sempre evitato.

Non finisce qui: avere un *passcode* attivato su iOS lo attiva, comprensibilmente, anche su watch.

Avere il *passcode* su watch è veramente una fettina di vita grama, che oltretutto nega anche alcuni dei migliori benefici dell’oggetto.

La parte migliore: Exchange *inibisce la disattivazione del passcode*. Che quindi deve restare attivo, sempre e comunque.

Ecco che cosa accade a installare software Microsoft: la qualità della vita scende. E pensare che qualcuno lo fa pure per scelta.