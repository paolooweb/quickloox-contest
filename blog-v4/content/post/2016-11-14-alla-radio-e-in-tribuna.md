---
title: "Alla radio e in tribuna"
date: 2016-11-14
comments: true
tags: [rugby, Mac, Italia, kiwi, Rtl, Faccioli]
---
Ripresa dello schermo televisivo inviatami da **lorescuba**, con vista del settore tecnico del rugby italiano. Non il migliore al mondo, ma membro del club.

 ![I computer usati dal rugby neozelandese](/images/coach-kiwi.jpg  "I computer usati dal rugby neozelandese") 

Sempre **lorescuba** segnala un’altra immagine, stavolta dei commentatori di [Italia-Nuova Zelanda](http://www.gazzetta.it/Rugby/12-11-2016/rugby-italia-all-blacks-10-68-dieci-mete-maestri-170824977633.shtml), [pubblicata dal quotidiano Rovigo Oggi](http://www.rovigooggi.it/articolo/2015-02-17/non-tutti-sono-all-altezza/).

Già che si parla di immagini riprese dallo schermo televisivo, questa arriva dal notiziario di Rtl, la più ascoltata o una delle più ascoltate radio italiane.

 ![Ivana Faccioli della redazione di Rtl](/images/ivana-faccioli.jpg  "Ivana Faccioli della redazione di Rtl") 

Li do tutti per professionisti e forse le espressioni potrebbero essere migliori. Non mi sembrano abbandonati da Apple, comunque.