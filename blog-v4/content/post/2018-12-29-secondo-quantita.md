---
title: "Secondo quantità"
date: 2018-12-29
comments: true
tags: [Sensor, Tower, Android, iOS]
---
Una delle soddisfazioni del successo di iOS è che continua a smentire le teorie meramente quantitative riguardanti le dinamiche di una piattaforma, teorie che andavano per la maggiore negli anni di Windows (una per tutte: il mito per cui c’erano più virus su Windows perché Windows era più diffuso).

Ulteriore conferma a fine anno: secondo [i dati di Sensor Tower](https://sensortower.com/blog/million-dollar-publishers-2018), gli autori di app che hanno incassato almeno un milione di dollari su App Store nel 2018 sono il doppio di quelli che hanno realizzato la stessa impresa su Google Play.

Si noti che Android rappresenta i tre quarti del mercato, ma gli autori milionari sono un terzo del totale. Il trend è di lenta rimonta di Android su iOS. Molto lenta; se nel 2017 il rapporto era di due a uno, quest’anno è stato di 1,9 a uno.

Ancora più interessante è spacchettare il dato nelle categorie di app. Il mito è che contino solo i giochi, giusto? Su App Store è vero è trentatré percento, proprio perché le app milionarie sono giochi in un solo caso su tre.

Su Google Play è abbastanza vero; le app milionarie sono giochi in due casi su tre. Il resto conta pochissimo.

Ecco perché anche nel 2019 si potranno fare tutti i raffronti quantitativi del mondo e però, per uno sviluppatore in cerca di riconoscimenti tangibili, iOS continuerà a essere un obiettivo qualitativamente assai superiore.