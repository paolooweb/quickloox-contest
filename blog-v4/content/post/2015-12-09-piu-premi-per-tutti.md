---
title: "Più premi per tutti"
date: 2015-12-09
comments: true
tags: [iPad, iTunes]
---
Su App Store si possono vedere le classifiche secondo Apple delle [migliori app](https://itunes.apple.com/WebObjects/MZStore.woa/wa/viewRoom?fcId=1066247819&genreIdString=36&mediaTypeString=Mobile+Software+Applications) e dei [migliori giochi](https://itunes.apple.com/WebObjects/MZStore.woa/wa/viewRoom?fcId=1066247822&genreIdString=36&mediaTypeString=Mobile+Software+Applications) e constato con perplessità di non avere su iPad neanche un esemplare delle une e degli altri.

Vivo felice fuori dalla nuvola consumistica, sono un utilizzatore non tipico, ho criteri differenti oppure mi sto perdendo qualcosa? Per Natale mi farò più di un pensierino e, specie tra i giochi, si vedono cose promettenti anche di tipo *free-to-play* con investimento iniziale zero.

Consiglio una passata in rassegna. A meno di non essere io, quello strano.