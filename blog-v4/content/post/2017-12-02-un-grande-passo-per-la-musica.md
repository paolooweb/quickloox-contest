---
title: "Un grande passo per la musica"
date: 2017-12-02
comments: true
tags: [Young, Neil, Archives]
---
Neil Young è tangente ai miei interessi musicali, che spaziano su generi differenti dal suo. Eppure so che passerò il resto del 2017 attaccato ai suoi [Archives](https://www.neilyoungarchives.com/).<!--more-->

Un [articolo su Pitchfork](https://pitchfork.com/news/neil-youngs-massive-online-archive-is-open/) riassume bene la vicenda. In occasione del lancio di un nuovo album, Young ha messo online in *streaming* tutta la propria produzione musicale (iniziata che dovevo ancora nascere e qualche anno da allora è passato).

Fin qui, esistono *streaming* di tutta la musica del mondo. Ma l’interfaccia degli Archives è strepitosa, bellissima, usabile, quasi sinestesica. Uno dei pochi siti che mi fa aprire la finestra a tutto schermo per pura goduria. Guardi l’interfaccia e senti il sapore di *Rust Never Sleeps*, il ritmo della consultazione marcia assieme a *Field of Opportunity*, esplori e da qualche parte nella testa si avverte l’eco dei suoi amici Crosby, Stills e Nash.

Strumento perfetto per riflettere sul *digital divide*, gli Archives erogano musica a qualità totale, se c’è abbastanza banda; se non c’è si può passare a una versione campionata a 320 chilobit per secondo, paragonabile a quello che si scarica da iTunes.

Young se la prende con Apple in una introduzione per avere appunto popolarizzato la diffusione di musica compressa con perdita di qualità, anche se riconosce le virtù del connettore Lightning dell’hardware attuale di iOS. Disgraziatamente non c’era modo nel 2001 di offrire un servizio di scaricamento musicale che fosse a qualità totale. Oggi si può scoprire in Italia di poterlo teoricamente avere… oppure no. Dipende dal luogo e dipende *molto* dal luogo.

Ma è un altro discorso. Qui voglio dire che Young ha un’età veneranda, eppure ha capito il web come legioni di ragazzini e sedicenti *web designer* di oggi non hanno idea neppure lontana. Fosse anche musica che non piace, la visita si impone. In questi giorni negli auricolari mi gira solo la musica degli Archives e il merito è tutto di questo sito e di Neil, che ha compiuto per gli ipertesti un passo paragonabile a quello compiuto per l’astronautica (e l’umanità) dal suo [omonimo Armstrong](https://www.nasa.gov/centers/glenn/about/bios/neilabio.html).