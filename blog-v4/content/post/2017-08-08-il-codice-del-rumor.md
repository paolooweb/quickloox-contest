---
title: "Il codice del rumor"
date: 2017-08-08
comments: true
tags: [9to5Mac, Troughton-Smith, tvOS, tv]
---
Un ultimo aggiornamento, nipotini, su [quanto scritto ieri](https://macintelligence.org/posts/2017-08-07-rumor-di-una-volta/). Ci sono anche nuovi giornalisti di inchiesta che hanno la volontã e il genio di sporcarsi le mani con il codice per arrivare a scoprire verità interessanti in anticipo sul resto del mondo.<!--more-->

Uno di questi è Steve Througton-Smith, di cui *9to5Mac* riporta lo *hack* che gli ha permesso di [fare funzionare il prossimo tvOS alla risoluzione di 4k](https://9to5mac.com/2017/08/08/apple-tv-tvos-4k/).

La cosa ha rilevanza oltre la prodezza tecnica per una numerosa serie di ragioni. Per esempio ora sappiamo che le *app* già esistenti potranno funzionare su una tv di nuova generazione con nessuna o poche modifiche, nonostante un eventuale aumento della risoluzione video. E quindi non c’è da preoccuparsi della tenuta del proprio investimento in *app*.

È anche così, nipotini, che oggi si fa vero giornalismo, opposto al letame che percola da tanti siti a basso prezzo e zero resa.

Sempre affezionatamente vostro.