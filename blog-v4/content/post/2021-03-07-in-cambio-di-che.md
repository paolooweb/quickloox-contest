---
title: "In cambio di che"
date: 2021-03-07T00:05:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [Microsoft, Exchange, Hafnium, Krebs, John Gruber] 
---
Si parla su KrebsOnSecurity di almeno trentamila organizzazioni, per centinaia di milioni di utenze individuali, [colpite dall’attacco di un gruppo di pirati informatici cinesi](https://krebsonsecurity.com/2021/03/at-least-30000-u-s-organizations-newly-hacked-via-holes-in-microsofts-email-software/) ai server di Exchange.

Dove l’attacco ha avuto successo, i pirati hanno installato una shell web che consente accesso indiscriminato ai server. Tra le organizzazioni vittime, leggo, si trovano ricercatori medici, studi legali, istituzioni scolastiche, fornitori dell’esercito, *think tank* e organizzazioni non governative.

KrebsOnSecurity parla degli Stati Uniti, ma l’attacco è avvenuto a livello globale e non ci sono dati relativi all’impatto che potrebbe avere avuto in tutto il mondo. Probabilmente bisogna aggiungere un ordine di grandezza alle cifre americane e magari neanche basta.

Il tutto grazie a quattro falle nella sicurezza che Microsoft ha chiuso con un aggiornamento di emergenza.

Exchange è un sistema di amministrazione della posta con una architettura bizantina e una complicazione intrinseca che, a livello di sofferenza, fa preferire piuttosto un’unghia incarnita.

Se lo scopo della sofferenza non è almeno la sicurezza, qual è? Dice bene John Gruber:

>Microsoft Windows e Exchange sono sempre stati insicuri e probabilmente sempre lo saranno. È sorprendente quante minacce informatiche ampiamente pubblicizzate si possano ignorare evitando Windows ed Exchange.

La logica sfugge. C’è chi si affida a un software bacato, difficile da usare e da manutenere, insicuro, e per avere questo *paga*.

Che cosa riceve in cambio da Microsoft chi paga Exchange, a parte un server di posta inutilmente involuto e penalizzante? Perché, se non c’è un extra, un benefit, un vantaggio non evidente, tocca porsi domande.

*Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile.*