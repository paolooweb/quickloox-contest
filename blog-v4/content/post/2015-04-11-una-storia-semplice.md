---
title: "Una storia semplice"
date: 2015-04-13
comments: true
tags: [Apple, Arduino-Uno]
---
Un Apple ][ nel 1977 cambiava il mondo, ma oggi è una macchinetta talmente semplice che può essere [riprodotta dentro Arduino Uno](http://dpeckett.com/turning-the-arduino-uno-into-an-apple).<!--more-->

Riprodurre un Apple ][ dentro un Arduino Uno è una faccenda che comporta capirne di elettronica e di programmazione, oltre ad avere una minima manualità per saldare, connettere, ponticellare.

Può essere complicato riprendere una storia semplice di quarant’anni fa.
