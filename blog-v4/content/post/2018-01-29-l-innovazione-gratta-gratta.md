---
title: "L’innovazione gratta gratta"
date: 2018-01-29
comments: true
tags: [Jobs, Google, innovazione, Grab, Yagge, Quora]
---
Steve Yagge abbandona Google dopo tredici anni di lavoro ingegneristico di vertice per giocarsela nel Sudest asiatico dove una Uber locale che conquistasse il mercato potrebbe diventare un colosso globale. Comunque, [il suo punto principale](https://macintelligence.org/posts/2018-01-28-l-influenza-asiatica/) è che Google ha smesso di innovare e copia semplicemente.<!--more-->

Come mi ha fatto notare [Misterakko](http://accomazzi.net), a Yagge [ha rispostp su Quora](https://www.quora.com/Is-Steve-Yegge-right-about-Googles-ability-to-innovate/answer/Terry-Lambert?share=803e8c5f&srid=u0aE) un altro ex ingegnere software Google, Terry Lambert. Yagge ha ragione, sostiene, ma non per le ragioni che elenca.

L’innovazione nelle aziende è un incidente: o c’è qualcuno che vuole togliersi uno sfizio e può permettersi di farlo, oppure è il mondo attorno all’azienda a costringerla a innovare.

Steve Jobs, racconta Lambert, era stufo di perdere chiamate con i cellulari dell’epoca e per questo decise che c’era la possibilità di creare un prodotto migliore degli esistenti. Per fortuna si seppe solo in seguito di una torre cellulare At&T nelle vicinanze della sede Apple che era difettosa e i cui componenti, surriscaldandosi, interrompevano le chiamate. Forse dobbiamo il primo spunto per iPhone a equipaggiamento telefonico difettoso.

È comunque, Jobs poteva permettersi di fare ruotare l’azienda attorno al suo obiettivo. È facile immaginare mille altri contesti aziendali nei quali dalla situazione sarebbe nato esattamente nulla.

Promemoria, questo, utile per chi rimpiange i tempi della Apple che innovava e-invece-adesso-no.

E le app? Inizialmente Jobs non le voleva. Si arrivò ad App Store, una delle più grandi innovazioni degli ultimi dieci anni, sotto la pressione incredibile di una comunità enorme e agguerrita di sviluppatori con un solo obiettivo: far girare un loro programma su iPhone. Jobs prese atto e Apple elaborò un sistema per consentire alle app di terzi di funzionare sull’apparecchio.

Tutto questo non toglie meriti a Jobs né ad Apple. Fa capire tuttavia come sia imbarazzante sentire parlare di innovazione alla stregua di una funzione volontaria aziendale, praticata intenzionalmente oppure spenta come la luce nel tinello, basta un interruttore. Quanto siano imbarazzanti quelli che quantificano e confrontano. Steve, scrive Lambert in forma idiomatica, aveva un prurito che voleva grattarsi. L’innovazione nasce così più che per comunicato stampa del consiglio di amministrazione.
