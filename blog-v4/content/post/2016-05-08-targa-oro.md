---
title: "Targa d’oro"
date: 2016-05-08
comments: true
tags: [Bancomat, Serravalle]
---
Sempre parlando di [Bancomat e affini](https://macintelligence.org/posts/2016-05-07-il-bancomat-oggi/), al casello la mia tesserina elettronica non viene letta. Il giorno dopo verificherò che funziona da McDonald’s, al bar, in un negozio di scarpe per bambini, in gelateria e alla stazione di servizio, mentre non funziona in altri due caselli della stessa barriera, ma sono cose che succedono e il pedaggio poi è di soli 1,90 euro. La macchina genera l’attestato di transito con mancato pagamento e procedo tranquillo.

Il numero del mancato pagamento somiglia a 7100385956. Dieci miliardi di combinazioni potenziali; o nessuno paga mai il pedaggio e mi sembra strano, o il numero contiene codici che classificano la transazione, il che è di buon auspicio. Anche se anno e giorno sono indicati a parte: *6/127*. Utilissimo per quando dovrò inserire la data dell’evento, in notazione giorno/mese/anno. Se chiedo quale sia il centoventisettesimo giorno dell’anno chiunque mi risponde all’istante, giusto?

Entro nel sito di [Milano Serravalle Milano Tangenziali](http://www.serravalle.it/it/viaggia-informato/pedaggio/mancato-pagamento.html) e accedo alla pagina per il pagamento *online*.

Altro che precisione: oltre al codice da dieci miliardi di cifre occorre inserire anche la targa dell’auto, nome e cognome, email e telefono. Vabbè, la sicurezza, la ridondanza, c’è di peggio, raccolgono tutti i dati per poter contattarmi se qualcosa non andasse a buon fine.

Sicurezza e ridondanza, ma mi accorgo che sull’attestato la targa dell’auto è stampata con due lettere scambiate di posto. Li seguo nell’errore per favorire la conclusione della transazione o inserisco il dato corretto con il rischio di scombinare la procedura? Inserisco la targa giusta e incrocio le dita.

Passo al modulo di pagamento *online*, interamente in inglese. Non che sia un problema, ma provengo da una interfaccia in italiano. Per molta gente non è una situazione facile, ancora oggi. Devo inserire nuovamente l’email, che è la stessa di prima.

Alla fine il pagamento è completato e spero di essere tornato cittadino modello.

Ho visto sul web situazioni ben più complicate e disastrose. La somma di errori, inesattezze, inefficienza, inusabilità e incertezze di questa situazione, tuttavia, mi pare comunque da premio. Bravi, avanti così. Se magari scappano cinque minuti a un tecnico per esaminare il funzionamento della barriera con i Bancomat, mi è comodo ché tra due settimane devo ripassare.