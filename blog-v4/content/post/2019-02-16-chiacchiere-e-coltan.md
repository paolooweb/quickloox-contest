---
title: "Chiacchiere e coltan"
date: 2019-02-16
comments: true
tags: [Apple, Coltan, MacRumors, Enough]
---
Ci sono le chiacchiere sull’approvvigionamento di metalli rari in Africa (per esempio la columbite-tantalite, *coltan*) da parte delle multinazionali della tecnologia e poi ci sono le chiacchiere di Apple.

Queste ultime consistono nella nuova edizione del [rapporto sull’approvvigionamento di minerali conflict-free](https://www.sec.gov/Archives/edgar/data/320193/000119312519041571/d694085dex101.htm), il cui reperimento non implica il finanziamento diretto o indiretto di gruppi armati che hanno un interesse nell’attività di produzione e taglieggiano chi ci lavora.

Come si può leggere, cinque fonderie o raffinerie presenti nella catena di approvvigionamento di Apple non lo sono più, [e non è la prima volta che accade](https://macintelligence.org/posts/2015-02-14-come-fai-sbagli/), avendo rifiutato di portare a compimento un audit indipendente sul loro operato. Per altri 253 fornitori che lo hanno fatto, Apple conclude che non collaborano, né finanziano è, né dipendono da gruppi armati.

Nel 2017, la [classifica di Enough Project](https://enoughproject.org/demandthesupply?utm_source=PR&utm_campaign=Rankings2017) vedeva Apple come l’azienda più virtuosa in assoluto in materia, seguita a notevole distanza da Google.

Tutte le chiacchiere su temi che riguardano la qualità della vita e del lavoro nelle nazioni in via di sviluppo sono da rispettare, però c’è anche bisogno di fatti e numeri. Non vedo gran che da parte delle altre aziende implicate, e non sono poche né piccole.
