---
title: "La realtà costa"
date: 2023-08-11T02:25:40+01:00
draft: false
toc: false
comments: true
categories: [Internet, Hardware]
tags: [Oculus, Vision Pro, Venerandi, Fabrizio Venerandi]
---
Fabrizio Venerandi visita una cattedrale e pensa a quanto [sarebbe bello e istruttivo poter accedere ad affreschi e sculture anche dalla realtà virtuale](https://m.facebook.com/story.php?story_fbid=pfbid0t6HjSMhoefhUrZDQGaSm7S21qB3GuFGv9CzYykZSSWfAywzVybr8t5ciGiKq9z4Tl&id=1412334258), *con l’Oculus*.

>Mi sono collegato subito con [l'Oculus](https://www.oculus.com/experiences/quest/) per vedere se era possibile camminarci dentro in realtà virtuale, ma non ci sono riuscito. Ci sono ancora, mi pare, due limiti: la visione a 360 gradi c'è, ma in due dimensioni: con l'Oculus non si riesce ad attivare la realtà virtuale. 

>E la seconda è la qualità della definizione: la navigazione all'interno della stanza ha una definizione piuttosto bassa. Solo cliccando sugli affreschi si ha una visione 2D con l'affresco con una possibilità di zoom impressionante.

2D, niente realtà virtuale, considerato che l’apparecchio dovrebbe servire a quello c’è da farsi qualche domanda, su come è stato creato il modello da visitare o sulle effettive possibilità dell’Oculus.

>Ma il sistema così sembra un po' un compromesso: o passeggi per la stanza, o guardi nel dettaglio alcuni degli affreschi "uscendo" dalla stanza. Comunque non in realtà virtuale.

Bello, eh, ma non è esattamente quello che abbiamo in mente nei nostri sogni di realtà virtuale.

Per inciso, questo è Venerandi [il giorno dopo la presentazione di Vision Pro](https://m.facebook.com/story.php?story_fbid=pfbid025TotCGYn5JdDJ3a7zCzpzSGuMCj1RonQtuh27BMNkZz6vXWzpHLj4ktbLtFnCqLxl&id=1412334258):

>Molte delle cose che si vedono nei video dimostrativi sono già nell'Oculus, anzi, per molti aspetti l'approccio di Apple è fin troppo timido e tradizionale […] certe cose dell'Oculus sono più "rivoluzionarie" di quelle del visore Apple. Le App multiutenti, per dire, fanno già ora molto più dei meet visti nel promo del prodotto.

Certo, Vision Pro va ancora provato su strada. La sensazione resta quella che, da quanto abbiamo visto e sentito, si possa sperare in esperienze più soddisfacenti di quelle a metà, solo 2D ma senza realtà virtuale, con realtà virtuale ma senza 2D eccetera.

Dice, ma Oculus mica costa tremilacinquecento dollari prezzo base. Di sicuro; nel contempo, Apple sembra l’unica ad avere effettivamente fatto i conti di quanto costi veramente ricreare un ambiente fisico dentro un visore e farlo con soddisfazione per chi lo usa. Gli altri vendono il prodotto a chi preferisce, si accontenta, è legato, non lo so, a realtà più economiche di quella che ci circonda.

*Gli aggiornamenti in agosto del blog potrebbero essere irregolari o infrequenti.*