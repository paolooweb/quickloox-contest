---
title: "Il coraggio e il valore"
date: 2016-02-17
comments: true
tags: [iPhone, Fbi, iOS, cifratura, privacy, terrorismo, Gruber]
---
Perché compri un Mac? Tutti i computer sono uguali. Perché compri un iPhone? Android fa tutto quello che ti serve e costa meno. A che ti serve un iPad? Meglio un ibrido, un convertibile, un aggeggio con tutti i difetti di un computer e tutti i problemi di un *tablet*, che però puoi divertirti a montare e smontare alla bisogna sentendoti *trendy*.<!--more-->

Ogni tanto c’è dell’altro. Per esempio la [lettera aperta di Tim Cook](http://www.apple.com/customer-letter/) per denunciare il tentativo dell’Fbi di arrivare per legge alla possibilità di sbloccare arbitrariamente qualsiasi apparecchio cifrato.

Apple ha collaborato in ogni modo per agevolare le indagini degli agenti federali sugli atti terroristici di San Bernardino in California. Solo che ora l’Fbi chiede ad Apple una versione speciale di iOS da poter installare a proprio gusto su apparecchi soggetti a indagine. Una versione di iOS priva delle salvaguardie dei dati dell’apparecchio, che installata a freddo su una macchina indagata consenta con facilità di arrivare a leggerne i contenuti.

Anche un bambino capisce che, una volta che l’Fbi disponesse di questo software, entro breve – una falla qui, una corruzione là – arriverebbe in mano più o meno a chiunque. La sicurezza attuale degli iPhone, di tutti gli iPhone, diventerebbe istantaneamente zero.

Apple è doppiamente coraggiosa su questo. Intanto perché si oppone all’Fbi e alle autorità, in nome della nostra *privacy*. Secondo, perché si tratta di una mossa doppiamente rischiosa. Come [nota John Gruber su Daring Fireball](http://daringfireball.net/linked/2016/02/17/apple-fbi-thompson), è la cosa giusta moralmente, ma politicamente apre a grandi rischi, per un’azienda. L’Fbi ha scelto questo caso con malizia: c’è di mezzo il terrorismo islamico e molta parte dell’opinione pubblica potrebbe schierarsi contro Apple per il solo fatto che l’iPhone oggetto del contendere apparteneva a uno dei terroristi. Per Apple sarebbe stato molto più semplice tenere una linea pubblica assai più morbida, se non addirittura accontentare l’Fbi di nascosto invece di opporsi apertamente.

Il silenzio di Microsoft, il [tiepidume di Google](https://recode.net/2016/02/17/google-ceo-sundar-pichai-says-government-request-for-apple-back-door-could-set-troubling-precedent/), la latitanza di Samsung sono eloquenti. La miglior ragione di sempre per scegliere un iPhone: il computer di maggior valore, dall’azienda di maggior valore.

*Post Scriptum:* qualcuno potrebbe sottovalutare la vicenda, visto che sono cose americane, lontane migliaia di chilometri e alla fine neanche ci riguardano perché da noi ci sono altre leggi e bla bla bla. Invece è il tema dell’anno anche se siamo a febbraio e forse è pure il tema del triennio. Leggere, per credere, che cosa [sta succedendo nel nostro Parlamento](http://www.apogeonline.com/webzine/2016/02/17/legiferare-senza-cognizione-di-causa).