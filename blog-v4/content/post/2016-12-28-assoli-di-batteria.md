---
title: "Assoli di batteria"
date: 2016-12-28
comments: true
tags: [MacBook, Pro, Consumer, Report, Schiller, iMore]
---
Consumer Report, l’Altroconsumo degli americani, non raccomanda i nuovi MacBook Pro. Perché hanno sedici gigabyte di Ram? O perché costano molto? No, perché non riescono a ottenere valori ragionevoli nei test di autonomia. Apple, per voce di Phil Schiller, sta collaborando.<!--more-->

<blockquote class="twitter-tweet" data-lang="en"><p lang="en" dir="ltr">Working with CR to understand their battery tests. Results do not match our extensive lab tests or field data. <a href="https://t.co/IWtfsmBwpO">https://t.co/IWtfsmBwpO</a></p>&mdash; Philip Schiller (@pschiller) <a href="https://twitter.com/pschiller/status/812461342728695808">December 24, 2016</a></blockquote> <script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>

Consumer Report sostiene di avere ottenuto poco più di tre ore di autonomia, vergognosamente poche per una macchina reclamizzata per un massimo di dieci ore. Ma scrive anche di averne ottenute diciannove.

Sta nel novero delle cose scoprire che un portatile non ha l’autonomia reclamizzata, una volta usato nel mondo reale. Scoprire che ne ha il doppio è veramente stimolante.

Il maligno cinico potrebbe argomentare che i test di Consumer Report sono ridicoli (come d’altronde quelli di Altroconsumo). La verità deve essere che Apple sta cercando di farsi insegnare da loro come ottenere diciannove ore di batteria per i MacBook Pro della prossima edizione.

*Assoli* ha assonanza con un insulto inglese che si adatta molto bene a descrivere i sedicenti esperti di Consumer Report.
