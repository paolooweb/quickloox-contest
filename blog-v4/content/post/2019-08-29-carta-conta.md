---
title: "Carta conta"
date: 2019-08-29
comments: true
tags: [Card]
---
Mi mancavano un po’ gli interventi di Horace Dediu su *Asymco* e sono rimasto soddisfatto. [Apple Car(d)](http://www.asymco.com/2019/08/27/apple-card/) riesce a catturare la realtà di Card con una analisi non pedestre, al contrario della quasi totalità di quanto si possa leggere in giro.

Perché la capacità principe di Dediu è contestualizzare.

>Il vecchio cliché vuole che ci vengano promesse auto volanti ma si finisca per avere x, dove x sta per qualcosa di più banale o elementare. […] Lo si dice usualmente per svalutare gli sviluppi della tecnologia neggli ultimi decenni. Invece di costruire grandi cose, si costruiscono minuzie. L’ironia è che x spesso diventa qualcosa di ubiquo e popolarissimo. Magari genera molto profitto e potrebbe persino cambiare i comportamenti. In verità, le alternative alle auto volanti sono sovente idee migliori.

Tutti conosciamo le auto e sappiamo quello che possono fare. Viene spontaneo immaginare il futuro dell’auto come qualcosa che si comporti alla stregua di ciò che conosciamo, ma faccia qualcosa in più. Nel caso dell’auto, muoversi su tre dimensioni invece che due.

È invece probabile che al posto dell’auto volante (o senza pilota, per stare nell’attualità) avremo qualcosa di inaspettato, apparentemente meno clamoroso, più utile, migliore.

È ora, [Card](https://www.apple.com/apple-card/). Certamente Apple preparava già la carta di credito mentre sviluppava Pay. La carta di credito, *dopo*, sembra qualcosa di scontato o banale, che certo non cambierà il mondo e certo non è all’altezza della fama innovatrice di Apple.

La domanda, tuttavia, è un’altra: che cosa stia già preparando Apple come tappa successiva del percorso iniziato con Pay e, più in generale, sullo sviluppo di un ecosistema che facilita la vita in tutti i campi, anche la salute, anche l’economia individuale.

Che cosa si fa dopo avere fatto una carte di credito? Qual è il corrispondente dell’auto volante, che non vedremo, per avere invece una idea migliore? Darsi una risposta interessante è tutt’altro che facile.