---
title: "Gente di rispetto"
date: 2021-02-02T01:18:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [Paolo Attivissimo, Clubhouse] 
---
Si parla molto di un nuovo social esclusivamente audio, per ora solo a invito, per ora solo su iPhone (se provi seriamente qualcosa di nuovo, lo fai su una piattaforma seria).

Potrebbe avere un grande successo. I podcast si sono rivelati in questi anni un veicolo eccellente per comunicare le proprie idee e in fin dei conti la proposta è un ambiente di podcast con maggiore interattività.

Potrebbe essere un grande flop. I social nascono per nutrire l’ego delle persone e poche cose gonfiano l’ego più di parlare. Al tempo stesso però, le persone si stufano in fretta di fare gli spettatori. Vogliono gonfiarsi l’ego anche loro. Difatti Facebook ha prosperato grazie alla sua abilità di confezionare una finta interazione degli spettatori con il parlatore. Vedo tre righe, metto un like, siamo tutti e due contenti, mi metto a scrivere le mie tre righe aspettandomi un like, che arriva. C’è un equilibrio. Che si farà più fatica a raggiungere con l’audio; non sto a sentirti dieci minuti per metterti un like.

Più su di tutto questo, la guardo dal versante comunicativo. E penso alle volte in cui me la sono presa con Paolo Attivissimo per la sua grande capacità di approfondire e sintetizzare su tanti temi, contrapposta alla superficialità con cui ha coperto gli sviluppi specialmente di iOS e in generale di Apple.

Ma appunto, Paolo – quando Apple non c’entra – sa essere preciso e ficcante. Come [in questo caso](https://twitter.com/disinformatico/status/1356259094190059522):

<blockquote class="twitter-tweet"><p lang="it" dir="ltr">Mandare un messaggio vocale farcito di errori e di &quot;uh.. eh...uhm...&quot; invece di scrivere vuol dire che chi manda considera più importante il proprio tempo di quello di chi dovrà ascoltarselo.<br><br>Leggere è molto più veloce che ascoltare. <a href="https://t.co/vMzpGAmqDo">https://t.co/vMzpGAmqDo</a></p>&mdash; Paolo Attivissimo (@disinformatico) <a href="https://twitter.com/disinformatico/status/1356259094190059522?ref_src=twsrc%5Etfw">February 1, 2021</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

*Leggere è molto più veloce di ascoltare*. Contemporaneamente, scrivere è più lento che parlare.

Siccome scrivere è più faticoso per l’autore e più comodo per il pubblico, significa che più probabilmente ci sarà rispetto dell’autore per il pubblico. Siccome parlare è meno faticoso di scrivere, ci sarà più probabilmente meno rispetto dell’oratore per il pubblico.

Questo, tra l’altro, sistema definitivamente la questione dei *vocali* su WhatsApp o altro. Se mi rispetti, prendi un momento per scrivermi. Se non ce l’hai il momento, usi la *dettatura* e mi mandi un messaggio invece di un vocale. Tempo impiegato, identico.

Pensa che io potrei non avere il momento per ascoltarlo, il vocale.

*Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile.*