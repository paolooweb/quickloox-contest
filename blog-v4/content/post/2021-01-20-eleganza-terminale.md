---
title: "Eleganza terminale"
date: 2021-01-20T00:35:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [Big Sur, WordPress, Pages, Html, ePub, Safari, Mac mini, iPad Pro, Mojave, MacMomo] 
---
Giusto ieri parlavo del mio [tour tra Pages e WordPress per arrivare ad avere del testo Html editabile in BBEdit](https://macintelligence.org/posts/Il-test-del-documento-formattato.html). Oggi è arrivato **MacMomo** a spiegarmi una cosa semplice, elegante, potente.

§§§

Penso che una soluzione semplice sia usare il comando da Terminale `textutil`, che permette di elaborare i vari formati di testo.

L’idea è questa:

- selezioni e copi il testo dal file di Pages (ma potrebbe anche essere Word o TextEdit);
- esegui questo comando da Terminale: `pbpaste | textutil -stdin -convert html -stdout | pbcopy`;
- incolli l’HTML ottenuto dove ti pare.

Il comando in pratica incolla il testo copiato e lo passa direttamente a textutil che lo converte in HTML e lo ri-copia negli appunti.

In questo modo penso sia più rapido ed eviti il problema delle foto, che vengono bypassate.

Chiaramente poi non è detto che l’HTML così ottenuto sia perfetto per i propri scopi, ma nel caso penso basti BBEdit per editarlo come si preferisce… ;)

P.S.: il comando può anche essere salvato come alias all’interno del file *.bash_profile*, così da averlo rapidamente con un scorciatoia a piacere.

Tipo: `alias 2html='pbpaste | textutil -stdin -convert html -stdout | pbcopy'`

§§§

Se serviva spiegare a chiare lettere come l’interfaccia grafica sia un grande aiuto, ma il Terminale è un’arma formidabile, ecco fatto. Grazie!

