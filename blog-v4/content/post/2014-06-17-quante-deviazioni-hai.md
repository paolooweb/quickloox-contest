---
title: "Quante deviazioni hai"
date: 2014-06-17
comments: true
tags: [Excel, deviazione, Vasco, Rossi, Blasco]
---
La [canzone di Vasco Rossi](http://www.testitradotti.it/canzoni/vasco-rossi/deviazioni) metteva in guardia nella sua semplicità dal credersi troppo a posto, troppo regolari (o forse il contrario, questione di interpretazioni).<!--more-->

Sicuri di stare usando il foglio di calcolo per i compiti giusti e nel modo giusto? Specialmente certi fogli di calcolo? Perché, per esempio, Excel [ha discreti problemi](http://www.npl.co.uk/mathematics-scientific-computing/numerical-computation/research/testing-the-numerical-correctness-of-scientific-software) nel calcolare deviazioni standard in modo affidabile nel tempo.

Niente di rilevante se sono le spese di casa, o anche il *budget* aziendale. Ma quando si va sullo scientifico, ci sono soldi spesi molto meglio e anche *open source* gratuito ottimo per la bisogna.
