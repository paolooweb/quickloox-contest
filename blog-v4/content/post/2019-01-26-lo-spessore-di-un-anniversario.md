---
title: "Lo spessore dell’anniversario"
date: 2019-01-26
comments: true
tags: [iPad, Pro]
---
Oltre all’[anniversario di Macintosh](http://www.macintelligence.org/blog/2019/01/24/dal-floppy-a-dd/) appena trascorso, si avvicina l’anniversario di iPad di domani e a questo proposito è capitato del tutto casualmente a casa mia che il nuovo iPad Pro 12”9 si trovasse accanto a iPad di prima generazione.

Lo spessore dell’intero iPad Pro è minore dello spessore dello schermo del primo iPad.

Sotto lo schermo del quale c’è una rientranza, che nasconde all’occhio ulteriore spessore, quello per la logica: è il *retro bombato* che Paolo Attivissimo [additò come rischio](https://attivissimo.blogspot.com/2010/01/aiped-aipad-aipod.html), quello che la macchina dondolasse se appoggiata su un tavolo, per avere visto la presentazione di Steve Job in *streaming*.

Ma divago. Il punto è che iPad Pro si infila senza problemi nella rientranza sotto lo schermo.

In pratica, è la dimostrazione empirica (senza guardare le cifre ufficiali) che l’iPad Pro più recente ha neanche la metà dello spessore di un iPad 1.0.

E va veloce molto più che il doppio.

L’anniversario di iPad lo passerò usandolo un bel po’, iPad. Quello nuovo o quello vecchio? Sono in servizio attivo tutti e due e il secondo, quello vecchio, ha otto anni e mezzo. Mica male.