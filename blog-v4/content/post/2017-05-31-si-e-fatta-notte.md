---
title: "Si è fatta notte"
date: 2017-05-31
comments: true
tags: [CoC, Clash, Clans]
---
Il nuovo e massiccio aggiornamento di [Clash of Clans](https://clashofclans.com) ha portato grandi novità: la prima è certamente un secondo villaggio da gestire, che si raggiunge con una barca e dove l’ambiente è notturno in contrasto con la luce solare del villaggio primario. Cambiano anche le regole di gestione e combattimento, ma è materia di approfondimento eccessiva per queste righe.<!--more-->

Dopo avere accumulato una certa esperienza, posso dire: di tanti giochi del tipo, che chiedono tempo oppure denaro in cambio del tempo, questo è abbastanza intelligente e vario da meritare la prova.

Ma il vero plus è entrare in un clan e darsi man forte a vicenda durante le fasi di accumulo risorse e truppe, oltre che andare in guerra contro altri clan. Come già detto altre volte, un gioco discreto diventa più bello quando c’è compagnia e magari quando è anche buona.

Per questo rinnovo la chiamata alle armi per eventuali reclute. Non c’è alcun problema a essere novizi (anzi, equilibrano l’assetto del clan) e ci si diverte senza strafare e senza ansie. L’investimento di tempo, stando un pochino attenti a operare con giudizio, è minimo se si punta a crescere e infimo se invece no (voler crescere in fretta è uno degli errori più tipici). Salvo casi del tutto eccezionali facciamo guerre solo nei prefestivi e festivi (le guerre richiedono un pizzico di attenzione in più dato che sono concentrate nel tempo e c’è un minimo di strategia di squadra).

Tutti i dettagli sono in questo [vecchio post](https://macintelligence.org/posts/2015-09-30-clan-in-espansione/). Mi raccomando di farsi riconoscere e non usare le frasi standard del gioco, altrimenti la richiesta di ingresso nel clan verrà presumibilmente rifiutata. Sono a disposizione per ulteriori approfondimenti ove servissero.