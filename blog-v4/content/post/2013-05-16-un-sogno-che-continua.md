---
title: "Un sogno che continua"
date: 2013-05-16
comments: true
tags: [tastiere]
---
Scrivevo <a href="http://www.icreatemagazine.it/script/toccare-il-sogno/1675">un anno fa</a> di sognare una tastiera da scrivania <a href="http://www.daskeyboard.com/model-s-ultimate/">Model S Ultimate Keyboard</a> di Das Keyboard.<!--more-->

Quella con i tasti ciechi. Sì: nel giro trenta giorni migliori clamorosamente la velocità di scrittura. Sei lento perché guardi i tasti, non perché ti manca il corso di dattilografia.

Non avrei neanche bisogno della retroilluminazione.

La sogno ancora perché nel frattempo è apparsa <a href="http://techcrunch.com/2013/05/14/a-chat-with-daniel-guermeur-founder-of-das-keyboard">una intervista</a> molto carina con il fondatore Daniel Guermeur, e naturalmente perché costa 139 dollari. Poi perché, dopo averli spesi, dovrei passare una sera a riconfigurarla per l’Italia lavorando con <a href="http://pqrs.org/macosx/keyremap4macbook/">KeyRemap4MacBook</a>, <a href="http://scripts.sil.org/cms/scripts/page.php?site_id=nrsi&amp;id=ukelele">Ukelele</a> (<a href="http://doublecommand.sourceforge.net">DoubleCommand</a> mi risulta obsoleto al momento).

Per sognatori più concreti e meno attaccati di me alle lettere accentate c’è anche il <a href="http://www.daskeyboard.com/model-s-professional-for-mac/">modello per Mac</a>, con le serigrafie sui tasti (su quella cieca, ovviamente, il problema della disposizione dei tasti Mac non si pone).