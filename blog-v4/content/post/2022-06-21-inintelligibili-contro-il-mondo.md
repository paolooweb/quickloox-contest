---
title: "Inintelligibili contro il mondo"
date: 2022-06-21T01:04:46+01:00
draft: false
toc: false
comments: true
categories: [Internet]
tags: [John D. Cook, R]
---
Un modesto ma sentito ringraziamento a John D. Cook per il suo post sul [lavoro inintelligibile](https://www.johndcook.com/blog/2022/06/18/illegible-work/): quello difficile da classificare, che mette in crisi i burocrati e gli stampatori di biglietti da visita, impossibile da spiegare ai genitori e qualche volta anche agli amici.

>Svolgere lavoro inintelligibile è molto divertente, ma difficile da vendere. Prova a digitare in Google “Qualcuno che può aiutarti con un problema matematico o informativo insolitamente difficile” e non riceverai grande aiuto. I motori di ricerca sanno rispondere solo a domande per loro intelligibili. Il lavoro inintelligibile arriva molto più facilmente dal passaparola che da un motore di ricerca.

Qui non si raggiungono le altezze di Cook, che spiega nelle note di dedicarsi talvolta a riscrivere in [C++](https://en.wikipedia.org/wiki/C%2B%2B) codice che in [R](https://www.johndcook.com/blog/2022/06/18/illegible-work/) era cento volte più lento. Però si sta lavorando nella comunicazione aziendale e nella trasformazione digitale e ci sono momenti in cui il genere di lavoro è esattamente quello: inintelligibile.

Difficile da descrivere, quantificare, replicare, persino giustificare a volte, eppure reale e impegnativo (nonché ampiamente divertente, come scrive Cook).

Il problema è che domani qualcuno potrebbe pensare di fondare una associazione dei lavoratori dell’inintelligibile e nessuno sarà in grado di descriverne lo scopo.