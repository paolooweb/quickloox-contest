---
title: "L’insofferenza"
date: 2020-04-05
comments: true
tags: [illiteracy, Burioni]
---
Le sacche di analfabetismo informatico che abbiamo in Italia normalmente ricalcano Totò e Fantozzi, la farsa nella tragedia e viceversa.

Ora però è diverso. Si deve pensare a che cosa sarebbe questo periodo senza Internet. Senza le videoconferenze. Senza la rete che continua a consentire la comunicazione anche quando tutti stanno a casa. Senza i computer che lavorano senza sosta a supporto degli scienziati che cercano una cura.

Gli analfabeti sono quelli che ti hanno un po’ riso dietro o avversato in modo che non dovesse capitare anche a loro di usare la tecnologia; quelli dell’odore della carta; quelli della *scuola democratica* che smette di essere tale se avviene a distanza. Ora, che cavolo sarebbe la *scuola democratica*? Quella dove i genitori votano le pagelle? O eleggono i docenti?

Un insegnante non in grado di erogare una lezione efficace e coinvolgente online è una persona pagata per saperlo fare, che non lo sa fare. Un insegnante che ritirava i telefoni all’ingresso in classe per manifesta incapacità di sfruttarli, ora deve esserne capace, o ritirarsi. Se un ragazzo ha *solo* un cellulare, ha in mano un computer multimediale, su cui è possibile scrivere, leggere, filmare, programmare, accedere a cloud e banche dati. Sono una risorsa, non una mancanza.

Gli analfabeti non sono i nonni che magari si trovano in difficoltà e hanno bisogno di una mano a orientarsi; sono i creativi dei moduli di autocertificazione un tanto al chilo, gli incapaci di fare stare in piedi un sito seppure pagati per farlo, i burocrati che domani passata la buriana e dimenticato Burioni pretenderanno che si torni alle carte bollate e ai passaggi allo sportello per certificare l’ovvio e giustificarsi la poltrona.

In questo momento, dove tutta la tecnologia che dà più fastidio alle anime belle è quella più vitale, spesso anche in ospedale oltre che nello *home office*, l’analfabeta informatico non è più una figura un po’ ridicola e un po’ triste.

Fa proprio direttamente incazzare.