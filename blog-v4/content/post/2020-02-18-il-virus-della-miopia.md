---
title: "Il virus della miopia"
date: 2020-02-18
comments: true
tags: [Covid-19, Cina, Apple, iPhone]
---
I problemi sanitari in Cina affliggono anche le fabbriche di componenti e di apparecchi elettronici e Apple ha spiegato come [l’attuale emergenza le renderà impossibile il rispetto delle indicazioni di fatturato](https://www.apple.com/newsroom/2020/02/investor-update-on-quarterly-guidance/?1581973282) fornite durante la recente presentazione dei risultati trimestrali.

L’effetto – doppio, sulla produzione per via delle fabbriche chiuse e sulle vendite per via dei negozi chiusi – è notevole: si parla di una riduzione *globale* delle consegne di computer [tra il ventinove e il trentasei percento](https://www.digitimes.com/news/a20200217VL200.html?mod=2).

Scommettiamo che alla prossima scadenza trimestrale ci sarà un buontempone che salta fuori a dire come Apple sia spacciata perché ha venduto meno iPhone e meno Mac?

Che, invece, sarebbe da concentrarsi sulle persone infette che non riescono a riprendersi dal contagio. Il conto delle vittime, quello sì, sarebbe bello che crollasse.