---
title: "Il prezzo della fama"
date: 2017-11-06
comments: true
tags: [iPhone, X, iFixIt]
---
Era da molto tempo che trascuravo le dissezioni anatomiche di iFixIt per mancanza di interesse. Lo smontaggio completo di un iPhone 7 era simile a quello di un iPhone 6, al punto da togliere attrattiva all’operazione.<!--more-->

Con iPhone X [le cose sono cambiate un po’](https://www.ifixit.com/Teardown/iPhone+X+Teardown/98975) e c’è una frase che mi rimbalza in testa:

>Questa scheda logica miniaturizzata è incredibilmente efficiente dal punto di vista dello spazio. La densità di connettori e componenti è senza precedenti. Oncia per oncia, persino un watch ha più spreco di superficie di scheda logica.

Si scopre che l’apparecchio contiene due batterie, un inedito per iPhone, e che se la scheda logica non fosse divisa in due parti sovrapposte occuperebbe il 135 percento della superficie effettivamente offerta da iPhone X.

Non è male. iFixIt assegna a iPhone X un punteggio di riparabilità di sei punti su dieci, perché la scheda logica è talmente compatta che alcune operazioni rischiano di essere impossibili e perché, se si rompe il dorso dell’apparecchio, occorre sostituire una quantità notevole di componenti non riparabili.

In altre parole, si ripara con difficoltà perché è un pezzo di tecnologia estremamente evoluto. E qualcosa, nella valutazione complessiva, dovrebbe contare nelle mille chiacchiere che invadono la rete in proposito.