---
title: "Ti scrivo una presentazione"
date: 2016-08-07
comments: true
tags: [BBEdit, Markdown, MultiMarkdown, Terpstra, Marp, TextMate, LibreOffice, XeLaTex, TeXShop]
---
Che interessante questa mail di **Federico**:

**Ringraziando** [Brett Terpstra](http://brettterpstra.com) che me lo ha fatto scoprire, segnalo [Marp](https://yhatt.github.io/marp/) nell’ottica dei due bei post di qualche settimana fa sul blog di Sabino *[che presumo siano [Da Keynote a PowerPoint](https://melabit.wordpress.com/2016/07/22/da-keynote-a-powerpoint/) e [Vita oltre PowerPoint](https://melabit.wordpress.com/2016/07/21/vita-oltre-powerpoint/)]*.<!--more-->

Al momento dice che si tratta di una *prerelease*. Non è entusiasmante per il risultato (ancora) ma a me lo sembra invece per l’idea. Appena riesco me lo studio un po’.

§§§

Devo dire che [Markdown](https://daringfireball.net/projects/markdown/) e [MultiMarkdown](http://fletcherpenney.net/multimarkdown/) sono davvero un’invenzione grandiosa. Una di quelle che ti chiedi come si faceva *a vivere senza*.

Un esempio.

Mi hanno mandato un file Rtf da 450 megabyte (quattrocentocinquanta megabyte). Ci è voluto un servizio mail apposito con tutti i fastidi del caso: si apre una finestra piena di scritte *download* che se sbagli partono *puttanate dantesche*.

Nello Rtf le tabelle sono, passami il termine scientifico, *affanculizzate* e quindi il senso del formato si ridurrebbe alla portabilità (?). Dice: lo apri con qualsiasi programma (?) su qualsiasi piattaforma. Va beh, ma le tabelle? Ok, le rifaccio; oppure le *screenshotto* da un Pdf e le inserisco manualmente.

Me lo ero già fatto da solo, ma ho chiesto se potevano inviarmi una versione formato testo del documento Rtf. Solo per sapere se si poteva fare dal programma fonte. 

Si poteva e me lo hanno mandato, insieme al Pdf (solo immagine, non riconoscimento ottico dei caratteri, ma ho preferito non indagare), in una mail, come semplici allegati. Il file ha queste caratteristiche principali:

* 1,8 megabyte.
* 20.178 capoversi secondo [TextMate](https://macromates.com) (comprese le righe vuote di salto).
+ Tabelle inutilizzabili, affanculizzatura assimilabile, per inutilizzo, a quella dello Rtf.

Apro il file di testo in TextMate (bundle MultiMarkdown) e con ⌘-F, grazie a *find&replace*, ho assegnato un po’ di *#* ai *Capi* ed agli *Art*. Noioso, ma stimo di averci dedicato 15/20 minuti; erano quasi 500 pagine.

Non sono pochi (i minuti), ma ora con TextMate posso navigare il documento con un indice anziché scrollare su e giù (smadonnando perché non trovo le cose!).

Alternativa, operare in [LibreOffice](http://www.libreoffice.org) sul file Rtf: fare dei bookmark, uno per sezionamento (capi, articoli eccetera). Ma ci vorrà una mezza giornata (ipotizzo). Il documento, in Libre, ha già dei bookmark derivanti dallo Rtf ma, perlomeno in questo caso, inutili e fastidiosi.

In più, TextMate si becca da solo gli elenchi (trattini e numeri puntati) quindi non c’è da fare manco quella fatica (esclusi i numeri e le lettere con parentesi ma me ne faccio una ragione).

Partendo dal file testo così ottimizzato (o meglio marked-izzato), usando anche [Marked 2](http://marked2app.com) e [Critic Markup](http://criticmarkup.com), da ora in poi sono solo gioie. 

Volendo essere pignoli, nel file testo marked-izzato andrebbero cancellati i numeri degli articoli: è un capitolato organizzato per Capi, articoli e sottoarticoli già numerati di cui non utilizzerò tutte le voci. Se ne può fare a meno e rimandare al documento di lavoro finale, così da avere anche un controllo sulle parti lavorate: sono quelle a cui avrò tolto il numero via via che le rivedo. 

Una parte degli articoli (quelli che mi servono) la voglio portare in un documento [XeLaTeX](https://it.sharelatex.com/learn/XeLaTeX) (per questo ho tolto i numeri nel file testo di lavoro, LaTeX rinumera e ordina da solo le sezioni): da TextMate è semplicissimo. 

Poi in [TeXShop](http://pages.uoregon.edu/koch/texshop/) la ri-pulisco velocemente dal codice inutile (inizio e fine altrimenti sono errori) e la *impatacco* nel mio documento-master con un bel `\include{}" (includeonly)`.

Ok, le label sono un po’ un disastro. Ma – caxxo – è tutto un altro mondo!

Ora, se mi servisse una demo veloce-veloce, forse con Marp potrei anche fare qualcosa di interessante.

N.B. per lux: lo so, si potrebbe usare BBEdit. Lo conosco poco e **sono ormai abituato a TextMate.**

Lavoro vero, obiettivi reali, scadenze concrete, risultato ottimale, apprendimento, approfondimento, strade nuove che si aprono. La mail meritava la pubblicazione non fosse altro che per l’elenco degli strumenti citati. E poi l’idea di poter realizzare una presentazione essenziale scrivendola in Markdown è davvero intrigante.

E poi Federico ha pure citato [BBEdit](http://www.barebones.com/products/bbedit/) a mo’ di *captatio benevolentiæ*. Devo trattarlo bene.