---
title: "Pro e contro"
date: 2016-06-22
comments: true
tags: [Dediu, pro, app]
---
Ritorno sul frusto tema *Apple si dimentica dei professionisti* per via di un interessante articolo di Horace Dediu sullo [stato dell’ecosistema](http://www.asymco.com/2016/06/16/state-of-the-ecosystem/) Apple.

Una serie di grafici riassume l’andamento dei ricavi dei servizi come vendita di musica, *app*, libri eccetera.

Si nota che le *Pro Apps* rappresentano una fonte di ricavo minima rispetto al totale. La cosa più interessante è che in termini assoluti questi ricavi sono piuttosto costanti negli anni. C’è stato un certo incremento attorno al 2010-2011 (presumibilmente quando Apple, secondo molti, si preparava ad abbandonare Mac per pensare solo a iPhone e iPad) e recentemente una flessione… che porta i valori ai livelli del 2006-2007, quando iPhone e iPad non c’erano e, sempre secondo il meme, i professionisti non venivano trascurati.

Scoprire, cifre alla mano, che le *app* professionali sono una fonte di ricavo costante nel tempo fa pensare che forse tutta questa fuga di professionisti non sia avvenuta. E anche che sia relativamente costante nel tempo il valore della fornitura.

*[L’amico Akko ha lanciato il progetto di un [uberlibro](https://macintelligence.org/posts/2016-06-15-un-kickstarter-per-cambiare-il-libro/) sul mondo Apple, diverso e nelle intenzioni migliore dall’offerta editoriale che si è vista sino a qui. Il progetto ha bisogno di [essere divulgato e finanziato](https://www.kickstarter.com/projects/295108589/cuore-di-mela-i-segreti-di-apple-iphone-e-mac) per riuscire.]*