---
title: "I lanci del disco"
date: 2013-11-22
comments: true
tags: [Google, Backblaze, HD]
---
Quanto vive un disco rigido?<!--more-->

È uscito uno [studio di Backblaze](http://blog.backblaze.com/2013/11/12/how-long-do-disk-drives-last/), società di *backup* *online*, basato su circa **venticinquemila** dischi di qualità *consumer*, gli stessi che troviamo in negozio. Tre quarti di questi sono sopravvissuti oltre i quattro anni di vita. La stima è che metà dei dischi rigidi passi i sei anni e metà no. Non sanno ancora quale possa essere il limite massimo.

Esiste da sei anni la [ricerca di Google](http://static.googleusercontent.com/media/research.google.com/en//archive/disk_failures.pdf), che non tira conclusioni ma spiega un sacco di cose sull’importanza delle variazioni di temperatura, sulla scarsa efficacia della prediagnostica Smart e tanto altro, su una popolazione di oltre **centomila** dischi.

Sempre dal 2007 arriva il [trattato dell’università Carnegie Mellon](http://static.usenix.org/events/fast07/tech/schroeder/schroeder.pdf), sempre su una popolazione di **centomila** dischi, che esprime un tempo medio di guasto tra un milione e e un milione e mezzo di ore.

Riassumo. Bisogna considerare la vita media di un disco rigido come si considera quella di un gatto, un pesce rosso, un criceto, un frigorifero, una *bougainvillea*.

La vita effettiva del mio gatto, pesce rosso eccetera conta **zero** nel momento in cui si parla del gatto di un altro. Che potrebbe vivere molto di più o molto meno e il fatto che sia della stessa razza conta poco o niente, salvo episodi precisi e documentati.

Anche se di gatti ne ho venticinque. Le misure le ha prese gente che ne ha visti venticinquemila.