---
title: "Se lo dicono"
date: 2014-07-24
comments: true
tags: [Mac, iPhone, R&D, Apple]
---
L’azienda che sta lasciando morire Mac per abbandono e pensa solo a iPhone [non aveva mai venduto così tanti Mac](http://www.apple.com/it/pr/library/2014/07/22Apple-Reports-Third-Quarter-Results.html) in un trimestre primaverile.<!--more-->

L’azienda che da sempre spende in ricerca e sviluppo una frazione di quello che fanno le sue concorrenti – peraltro con multipli dei risultati – non spendeva tanto in ricerca e sviluppo dalla vigilia del lancio di iPhone.

<blockquote class="twitter-tweet" lang="en"><p><a href="https://twitter.com/search?q=%24AAPL&amp;src=ctag">$AAPL</a> R&amp;D was over 4% of revenue. It hasn&#39;t been that high since 2006, before the first iPhone launched.</p>&mdash; Walter Piecyk (@WaltBTIG) <a href="https://twitter.com/WaltBTIG/statuses/491696783429746688">July 22, 2014</a></blockquote>
<script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>

Apple è costantemente il campo nel quale, se lo dicono, non c’è qualcosa di vero.