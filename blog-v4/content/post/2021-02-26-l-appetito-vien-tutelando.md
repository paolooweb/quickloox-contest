---
title: "L’appetito vien tutelando"
date: 2021-02-26T01:56:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [John Gruber, Daring Fireball, Apple, privacy, Safari, Vpn, Mail, Ascii] 
---
E se Apple, lanciata nel posizionamento come azienda che mette al primo posto la *privacy* degli utilizzatori, andasse oltre il blocco dei tracciamenti disonesti su Safari, per fornire [un’edizione di Mail capace di bloccare i pixel di tracciamento](https://daringfireball.net/2021/02/apple_mail_and_hidden_tracking_images) e anche [una Vpn per compiere in pace le operazioni che vogliamo restino confidenziali](https://daringfireball.net/linked/2021/02/25/el-toro-ip-targeting)?

Sono due proposte di John Gruber, che mi trovano del tutto consenziente. Personalmente mi sforzo di inviare email solo Ascii, una cosa sempre più difficile per design. Mai come riceverne, comunque.

Se ci teniamo alla privacy, andiamo fino in fondo. Questo non ci impedirà di concedere dati personali a destra e a manca, se pensiamo valga la pena farlo. In compenso, nessuno potrà farlo di nascosto e senza il nostro permesso esplicito e informato. Dovrebbe funzionare così.

*Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile.*