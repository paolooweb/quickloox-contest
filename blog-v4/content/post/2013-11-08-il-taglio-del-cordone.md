---
title: "Il taglio del cordone"
date: 2013-11-08
comments: true
tags: [iTunes, app, iPad]
---
Quando c’è il *backup* via iCloud per le *app* che contengono dati ed è possibile scaricare all’infinito da App Store qualsiasi *app* che non contenga dati, a che serve conservarle tutte in iTunes?<!--more-->

Ho dichiarato maggiorenne e pronto per incontrare il mondo a proprio rischio il mio iPad e ho cancellato da iTunes tutte le *app* con la sola eccezione di una vecchia edizione di [Battle for Wesnoth](http://wesnoth.org) che immagino non si potrà mai più scaricare (adesso c’è quella nuova per [iPhone](https://itunes.apple.com/it/app/battle-for-wesnoth/id575239775?l=en&mt=8) e [iPad](https://itunes.apple.com/it/app/battle-for-wesnoth-hd/id575852062?l=en&mt=8), ma non so quanto migliore). Liberando quaranta gigabyte come effetto collaterale.