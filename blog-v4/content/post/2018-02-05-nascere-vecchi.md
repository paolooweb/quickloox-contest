---
title: "Nascere vecchi"
date: 2018-02-05
comments: true
tags: [GoSquared, iOS, Oreo, Android, Marshmallow, Lollipop, Nougat, Dashboards, Google]
---
GoSquared riferisce che [il traffico di rete riguardante iOS 11](https://www.gosquared.com/global/ios/11/#month) sia attorno all’ottanta percento del traffico di rete totale di iOS.<!--more-->

Non è la misurazione ufficiale di Apple, ma fornisce comunque una buona idea dell’adozione di iOS 11: maggioranza assoluta e schiacciante.

Se chiedo i dati su Mac, piattaforma più conservativa e più longeva (dove sono in vita numerosi computer che non possono installare la versione più recente) si vede che [High Sierra sta attorno al trentatré percento](https://www.gosquared.com/global/mac/high-sierra/#beta) e ha la maggioranza relativa.

La pagina [Dashboards di Android](https://developer.android.com/about/dashboards/index.html) mostra d’altro canto che Oreo, la versione più recente di Android, non arriva all’uno percento delle adozioni. Nougat, la penultima generazione, è inferiore come numeri sia rispetto a Marshmallow (quello prima) sia verso Lollipop (quello prima ancora).

Riassumendo: il proprietario medio di iOS usa la versione più recente. Quello di Android ne ha una che, pensando alla sicurezza, alle funzioni nuove, alla sistemazione dei bachi, al supporto, è vecchia in modo preoccupante.

Tra usare iOS 9 e usare iOS 11 ci sono differenze importanti e un salto di qualità considerevole. Su Android, l’equivalente del salto di qualità lo usano in pochissimi.

In più, è chiaro che vengono venduti terminali nuovi con sistemi vecchi. Che poi spesso neanche vengono aggiornati nel tempo.

Invecchiare è un’avventura che ha anche lati affascinanti e appaganti, proprio perché si parte da giovani. Ma se uno nasce già vecchio, gli manca la vita.