---
title: "Con permesso"
date: 2016-09-20
comments: true
tags: [iPhone, MacBook, Pro, Air, Samsung, Mac]
---
Riassumendo: iPhone 7 ha [lo schermo migliore](http://www.displaymate.com/iPhone7_ShootOut_1.htm#) che è possibile portarsi a casa in qualsiasi categoria.<!--more-->

In termini di velocità teorica, [se la gioca](http://daringfireball.net/linked/2016/09/14/geekbench-android-a10) con MacBook Air, con MacBook Pro non troppo nuovi e perfino in  circostanze specifiche con un Mac Pro (!).

<blockquote class="twitter-tweet" data-lang="en"><p lang="en" dir="ltr"><a href="https://twitter.com/gruber">@gruber</a> Grain of salt and all, but Geekbench has the iPhone 7 beating the $6500 12-core Mac Pro in single-thread. <a href="https://t.co/DFQoULZ15G">pic.twitter.com/DFQoULZ15G</a></p>&mdash; Matt Mariska (@MattMariska) <a href="https://twitter.com/MattMariska/status/776495257311584261">September 15, 2016</a></blockquote> <script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>

Nella vita reale, è approssimativamente [due volte più veloce](https://www.youtube.com/watch?v=k_PK_6F_Bhk) del suo concorrente appena fatto uscire da Samsung.

<iframe width="560" height="315" src="https://www.youtube.com/embed/k_PK_6F_Bhk" frameborder="0" allowfullscreen></iframe>

Ha [resa fotografica](http://www.astramael.com/5) impensabile fino allo scorso anno per un computer da tasca.

Poi lo hanno fatto impermeabile, con una nuova finitura non solo cosmetica, con una luminosità dello schermo nettamente migliore della precedente eccetera eccetera. Non sto a dilungarmi. La meschinità di commenti tipo [A iPhone 7 manca un design di avanguardia](http://www.nytimes.com/2016/09/08/technology/whats-really-missing-from-the-new-iphone-dazzle.html) rivela il bisogno di attirare clic più che scrivere informati.

Stavolta Phil Schiller si è trattenuto. Ma un altro [Apple can’t innovate anymore my ass!](https://macintelligence.org/posts/2013-06-16-la-serie-a-la-serie-b/) ci stava tutto.