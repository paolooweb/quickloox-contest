---
title: "Intelligenza e interfaccia"
date: 2020-02-20
comments: true
tags: [Tesler, Hofstadter, Espinosa, Macintosh, Pascal]
---
Addio a [Larry Tesler](http://www.nomodes.com/Larry_Tesler_Personal/Home.html).

Di personaggi straordinari ce ne sono molti, nella storia dell’informatica personale. Tesler è speciale perché era uno scienziato puro prestato ai computer, come si vede dalla sua home dove due sezioni su cinque riguardano il [problema dei compleanni](http://www.nomodes.com/Larry_Tesler_Personal/Birthday_math.html) e i [numeri primi](http://www.nomodes.com/Larry_Tesler_Personal/Primes.html).

Seppi per la prima volta della sua esistenza e importanza grazie a *Le Scienze*: devo recuperare la fonte esatta, ma ritengo che Douglas Hofstadter avesse lanciato una delle sue sfide dalle pagine dei *Temi metamagici* e lui l’abbia affrontata per forza bruta, tramite un programma Pascal fatto girare su Macintosh. Va tenuto presente che erano anni in cui si doveva spedire un floppy disk per posta ordinaria, per fare circolare software di cui parlare su una rivista.

Tesler si è dedicato anche all’interfaccia umana e questo sancisce la sua grandezza tra chi ha saputo posizionarsi al consueto [incrocio tra tecnologia e arti liberali](https://macintelligence.org/posts/2017-08-22-intelligenza-umanistica/).

Uno dei documenti migliori per ricordarlo è proprio questa antica trascrizione di una conversazione avuta con Chris Espinosa sulle [origini dell’interfaccia umana Apple](http://web.archive.org/web/20040511051426/http://computerhistory.org/events/lectures/appleint_10281997/appleint_xscript.shtml).