---
title: "Chissà chi lo sa"
date: 2016-02-27
comments: true
tags: [Twitter, Mac]
---
Il nostalgico ricorda ogni tanto di quando i Macintosh venivano venduti senza un manuale, o quando lo avevano ma al massimo serviva a un bimbo come rialzo per la seduta. (Perlomeno, lo diceva la pubblicità). Il contrasto con i tempi di oggi, dice il nostalgico, è lampante: certe funzioni non sono evidenti né intuitive.<!--more-->

Non entro nel merito delle funzioni singole, ma sostengo che i tempi sono cambiati in modo tale che l’intuitività è più importante che mai, ma non c’è *design* che tenga per accogliere tutte e proprio tutte le funzioni che interessano a qualcuno.

Un esempio: questo [elenco non ufficiale di comandi e operazioni su Twitter](https://eev.ee/blog/2016/02/20/twitters-missing-manual/). Che alla fine è un servizio per mandare messaggi lunghi 140 caratteri. Provare a leggerlo e contare quante funzioni si conoscevano, quante vengono spiegate e quante compaiono nell’interfaccia.