---
title: "Antologia del buonumore / 2"
date: 2013-11-02
comments: true
tags: [MobileMe]
---
Sempre da **Walter** un’altra perla. Il suo commento:

> Visto ieri sugli scaffali di Media World nel centro Piazzale Lodi a Milano… 9,99 euro…

 ![MobileMe in vendita](/images/mobile-me-sale.jpg  "MobileMe") 

Sono anche tentato di comprarlo. Magari tra vent’anni ha un valore!