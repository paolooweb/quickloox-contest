---
title: "Per un voto utile"
date: 2013-02-21
comments: true
tags: [Script]
---
Il mio codice fiscale ha la struttura *ABC DEF 01Z99 X333Y*.<!--more-->

Il mio codice Iban è *IT 88 A 12345 12345 012345678999*.

Recentemente ho rinnovato l’iscrizione all’Ordine dei Giornalisti, mediante la digitazione di un codice *Rav* della forma *12345678901234567*.

Un codice Inps ha la forma *12345678901234567* (esiste persino un <a href="http://vinboisoft.altervista.org/calcolo_contributi_inps.php">programma per Mac</a> che esegue il calcolo dei contributi).

Il mio indirizzo di posta, unico al mondo, è *lux@mac.com*.

Il mio numero di cellulare, unico al mondo, è *+39 333 1234567*.

Vi invito caldamente a votare chiunque, purché prometta – e mantenga – di dimezzare qualsiasi codice richiesto da qualunque attività. O almeno accorciarlo a lunghezza equivalente a quella di un numero di cellulare.