---
title: "La carica di USB-C"
date: 2019-04-12
comments: true
tags: [USB-C, Foinnex, iPad, Pro, iPhone]
---
Cose semplici, ma semplificano la vita un bel po’.

[Accrocchio per iPad Pro](https://macintelligence.org/posts/2018-12-05-doppio-is-peggio-che-quadruplo/) con ingresso USB-C e uscite VGA, HDMI, USB-C e (due) USB 3. Risultato, carichi due apparecchi (e potrebbero essere tre) con una sola presa a disposizione e intanto, fosse necessario, potresti anche andare sul videoproiettore.

 ![Accrocchio Foinnex carica iPhone da iPad Pro](/images/voinnex.jpg  "Accrocchio Foinnex carica iPhone da iPad Pro") 600’

