---
title: "Il tempo si è fermato"
date: 2018-03-03
comments: true
tags: [Rai, Flash, iPad, Safari]
---
L’ultimo mio contatto con il sito Rai data quasi esattamente a quattro anni fa.

Mi scandalizzavo perché, finalmente uscito dal guano di Silverlight, il sito [si era infilato](https://macintelligence.org/posts/2014-02-28-quello-bravo/) nella brodaglia di Flash.

Niente è cambiato. Se eccettuiamo l’universo attorno alla Rai.