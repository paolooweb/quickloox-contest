---
title: "La Prima Regola"
date: 2023-04-27T00:40:56+01:00
draft: false
toc: false
comments: true
categories: [Software, Web, Internet]
tags: [Open to Meraviglia, WhatsApp, Excel, WordPress, Attivissimo, Paolo Attivissimo, Armando Testa]
---
Paolo Attivissimo ha fatto una delle cose che gli riescono bene e ha pubblicato un post dove [elenca, documenta e attribuisce il merito della scoperta di tutte le enormità](https://attivissimo.blogspot.com/2023/04/gli-ideatori-della-campagna-social-open.html) presenti nella, chiamiamola così, campagna di comunicazione *Open to Meraviglia*.

Tra dimenticanze, errori, trascuratezze, ignoranza e incompetenza c’è spazio anche per notare che alcune delle foto usate per il sito Open to Meraviglia sono transitate da WhatsApp. Una pratica da evitarissimo in campo professionale, in quanto WhatsApp può comprimere anche robustamente le immagini e dunque degradare la loro qualità.

Qui volevo scrivere un pippone sul perché non bisogna usare WhatsApp (perché poi, anche se sei in un giro percepito di nove milioni di euro, ci fai passare le foto da pubblicare sul sito e ti sputtani una volta di più). Invece mi limito a enunciare la Prima Regola della Comunicazione:

>Chi trasmette lavora nell’interesse di chi riceve.

Gran parte della nostra comunicazione personale e professionale funziona nel modo esattamente opposto e non entrerò nel merito. Se il principio è che chi trasmette si fa contento e chi riceve si arrangi, ci sono dei problemi, ma non strettamente di WhatsApp o della comunicazione. C’è una intera visione del lavoro, della vita, delle persone attorno a noi, che necessita di una messa a punto.

E no, chi riceve non può fare il furbo e giocare al ribasso chiedendo formati inadatti, app tossiche e sistemi punitivi. Perché c’è la Seconda Regola:

>Più l’apparato di comunicazione prende le distanze dai minimi comuni denominatori, più è alta la probabilità che la comunicazione sia qualitativamente superiore.

I minimi comuni denominatori sono quelli che tanto va bene così, chissenefrega, così faccio prima. Tutta roba che cozza contro la Prima Regola.

In Armando Testa hanno fatto strame delle due regole ed è esattamente così che poi non vengono registrati gli account social, non si registrano i domini, si fa tradurre alla presunta intelligenza artificiale senza guardare (e poi la città di *Camerino* diventa *Garderobe* in tedesco). Dovevano essere a posto loro, non il pubblico. Hanno usato strumenti al minimo comune denominatore ed ecco il risultato.

Sembrano cose lontane da noi, vero? Parlo io che nel nostro gruppo Telegram dedicato a D&D facciamo uno .zip delle immagini degli avatar prima di spedirle, perché Telegram comprime di brutto. (Sì, usiamo Telegram. Come dei supereroi abbiamo scoperto come funziona, sul telefono poteva stare una app in più, lo usiamo anche se non *ce l’hanno tutti*, abbiamo compiuto imprese epiche pur di parlarci con qualche rispetto reciproco).

Invece l’Italia è tutta un ribollire di rimasticato di Prima e Seconda regola. Gli uffici sono pieni, le scuole sono piene così come lo sono le associazioni, le classi hanno dietro genitori e professori così, c’è una maggioranza silenziosa enorme votata alla mediocrità dello strumento e allo spregio per il destinatario. Alla fine Armando Testa, che dovrebbe essere una punta di diamante, è un pezzo qualunque del Grande Bollito in cottura perpetua nel calderone nazionale, solo un po’ più grande.

Come uscirne? Non esorto, non consiglio. Troppa gente prende come un insulto l’idea di comunicare pensando a chi riceve. Gli sembra una truffa.

Da leggere: [Un marziano a Roma](https://dlfroma.it/images/stories/PDF/evergreen/DLFR_Evergreen_Un_Marziano_a_Roma_E_Flaiano.pdf), di Ennio Flaiano.

Da ascoltare: [Elio](https://www.youtube.com/watch?v=kgpxvjw0-Nk).

<iframe width="560" height="315" src="https://www.youtube.com/embed/kgpxvjw0-Nk" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>