---
title: "Consumo (ir)responsabile"
date: 2019-05-10
comments: true
tags: [Vice, AirPods]
---
Articoli come [Gli AirPods sono una tragedia](https://www.vice.com/en_us/article/neaz3d/airpods-are-a-tragedy) impressionano e non per il contenuto, ma per il contesto dentro al quale evidentemente nascono.

L’idea di base, una riga per riassumerne cento, è che gli [AirPods](https://www.apple.com/it/airpods/) siano una iattura per la civiltà perché fatti di plastica e difficili da riciclare.

Fin qui è una posizione che si può condividere, o meno. Il contesto attorno è terrificante. Varie frasi iniziano con _molti pensano che_ o varianti sul tema, come se la cosa avesse rilevanza fattuale. L’idea che gli AirPods siano simboli di ricchezza ostentata (!) arriva mostrando una foto di Kanye West che impugna il suo portatile in modo noncurante (come peraltro è capitato spessissimo di fare anche a me, che il portatile me lo devo sudare) e una tizia che vende orecchini porta-AirPods.

Naturalmente non manca la tirata moralistica sui ricchi occidentali che progettano AirPods e i poveri orientali che li assemblano dopo che i poverissimi africani hanno estratto i metalli oggetto di conflitti. Il tutto senza il minimo accenno a [che cosa si stia facendo o non facendo](https://macintelligence.org/posts/2019-02-16-chiacchiere-e-coltan/) in merito, dati, informazioni serie su eventuali tendenze, niente.

Tanta demagogia, pillole raccolte in giro per Internet e buttate lì senza un senso logico, argomentazioni un tanto al chilo: la tecnologia Bluetooth [prende il nome dal re che voleva unificare la Danimarca](https://www.ancient-origins.net/history-famous-people/bluetooth-why-modern-tech-named-after-powerful-king-denmark-and-norway-007398) e adesso invece connota apparecchi con batterie che dopo due anni muoiono. Vergogna!

La critica agli AirPods è libera, le contraddizioni del nostro tempo sono importanti (come peraltro in tutti i tempi), nessun problema; questo modo di ragionare, mettere insieme fattoidi in una articolessa come se bastasse a legittimare una tesi, l’assenza totale di *fact-checking* e di autoverifica del proprio prodotto… tutto questo lo vedo sempre più spesso sui media e lo sento nelle conversazioni, e fa più paura.

Preferisco relazionarmi con un programmatore alienato che prende ogni frase alla lettera invece di avere a che fare con i fustigatori di costumi (o meglio consumi) del XXI secolo. Gente che, senza quei consumi, non esisterebbe. Al termine della lettura dell’articolo, ho capito che il tempo necessario mi è costato molto più di un paio di AirPods.