---
title: "El Terminalo"
date: 2022-09-23T00:03:41+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [a-Shell, Prompt, La Terminal, Miguel de Icaza, Xibbon]
---
Molto interessante l’uscita di [La Terminal](https://apps.apple.com/us/app/la-terminal/id1629902861?platform=ipad), app per fare ssh ed emulazione terminale su iOS e iPad OS.

A una prima occhiata è molto ma molto più amichevole di [a-Shell](https://holzschu.github.io/a-Shell_iOS/), più pulita di [Prompt](https://panic.com/prompt/) – anche se meno evoluta – e molto amichevole da approcciare.

Non rinuncio a Prompt in questo momento, però qualche prova la merita di sicuro. Oltretutto è gratis ma già ora mangia in testa a diverse proposte a pagamento, per semplicità e design.

Va anche notato che ci sono sviluppatori anche di un certo nome interessati a scrivere app estremamente tecniche e orientate allo sviluppatore, per iPad e iPhone, teoricamente piattaforme riservate a un pubblico più consumista e meno istruito sulla tecnologia.

Gli stereotipi si sgretolano e il vero potenziale di queste macchine emerge progressivamente.