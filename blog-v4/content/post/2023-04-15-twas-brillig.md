---
title: "’Twas brillig"
date: 2023-04-15T17:32:56+01:00
draft: false
toc: false
comments: true
categories: [Internet, Software, Web]
tags: [Jabberwocky, The Gostak, Lewis Carroll, Carroll]
---
I traduttori l’hanno reso in italiano in diversi modi; è l’inizio di [Jabberwocky](https://www.poetryfoundation.org/poems/42916/jabberwocky), la poesia di Lewis Carroll dedicata a un animale fantastico, il Jabberwock, scritta in un misto di inglese e una lingua inesistente, comprendente termini fantastici assenti da tutti i vocabolari.

Che accadrebbe se lo stesso principio fosse applicato a una avventura testuale?

È successo ed esiste. Chiunque può imbracciare il browser e giocare a [The Gostak](https://iplayif.com/?story=https%3A%2F%2Fifarchive.org%2Fif-archive%2Fgames%2Fzcode%2Fgostak.z5), accolti da questa descrizione iniziale:

>Finally, here you are. At the delcot of tondam, where doshes deave. But the doshery lutt is crenned with glauds.  
Glauds! How rorm it would be to pell back to the bewl and distunk them, distunk the whole delcot, let the drokes discren them.  
But you are the gostak. The gostak distims the doshes. And no glaud will vorl them from you. 
 
L’effetto è affascinante. Normalmente risolvere una avventura testuale richiede la costruzione di una mappa spaziale. Stavolta c’è una difficoltà aggiuntiva: costruire una mappa semantica.

C’è dentro qualcosa più della traduzione. Indubbiamente va creato un dizionario gostakese-italiano, però il gioco crea un’atmosfera peculiare e particolare. La comprensione della situazione va oltre la semplice resa delle parole sconosciute, provare per credere. A partire dal fatto che il linguaggio inventato riesce a creare sensazioni e retrogusti di per sé, prima che intervenga la traduzione. Spesso non si tratta di tradurre un termine ma di guadagnare una comprensione generica del testo. Se *no glaud will vorl them from you* è chiaro che *vorl* ha un significato simile a *sottrarre*. Ma che cos’è un *glaud*? Prima di scoprire se si tratti di umani, alieni, animali, piante, rocce, stati d’animo o giorni critici in un calendario, ci siamo già formati una qualche immagine nella mente. Una immagine probabilmente negativa, in contrapposizione al nostro obiettivo. Posto che non si tratti di una condizione temporanea e che i *glaud* diventino da un certo punto in avanti i nostri migliori alleati…

Non faccio spoiler. Certo, affrontare *The Gostak* richiede dedizione ancora superiore a quella di una normale avventura di testo. Per certo, è un’esperienza unica. Un po’ come provare a dare un senso a *Jabberwocky*.

>’Twas brillig, and the slithy toves  
      Did gyre and gimble in the wabe:  
All mimsy were the borogoves,  
      And the mome raths outgrabe.