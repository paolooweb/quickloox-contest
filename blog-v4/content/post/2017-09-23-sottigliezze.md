---
title: "Sottigliezze"
date: 2017-09-23
comments: true
tags: [Gruber, Apple, watch]
---
Da qualche mese è possibile trovare frammenti significativi della spiegazione generale di come funziona Apple. Questo è John Gruber nella sua [recensione di watch serie 3](https://daringfireball.net/2017/09/apple_watch_series_3).

>Apple ha trasformato watch in un accidenti di cellulare, senza ispessirlo o appesantirlo, che come prima funziona per tutto il giorno.

>Vale la pena di rifletterci. Apple è un’azienda tesa a rendere i propri prodotti sempre più sottili. A costernazione di molti, quando Apple crea chip più efficienti, tende a mantenere l’autonomia della batteria in un apparecchio più sottile invece di mantenere l’ingombro e aumentare l’autonomia grazie a batterie più grandi. Nei primi anni di vita di una nuova linea di prodotto, però, non lo fanno. iPhone ha conservato il proprio spessore fino a iPhone 4. Nelle prime generazioni era più importante aggiungere funzioni mancanti, come rete 3G, una fotocamera migliore e un processore più potente, che renderlo più sottile. watch potrebbe mantenere le stesse dimensioni di adesso ancora per qualche anno.

Semplice: nell’infanzia della linea di prodotto si aggiungono le funzioni. Nella maturità si toglie l’ingombro. Eppure, quando mai si è potuto leggerlo prima di adesso?

Ok, Gruber stesso puntualizza che il rivestimento sul retro degli watch serie 3 è due decimi di millimetro più spesso. Non due millimetri, zero virgola due. Lo spessore di due fogli di carta. Una sottigliezza che lascia intatto il ragionamento.