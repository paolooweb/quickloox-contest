---
title: "Spostare una virgola"
date: 2021-10-16T03:45:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [LoveFrom, Jonathan Ive, Marc Newson, WordPress] 
---
Che godimento, se il sito di [LoveFrom,](https://www.lovefrom.com) restasse com’è per sempre!

Al posto dei *chi siamo* (abbinati ai *chi se ne frega*), degli *scopri i nostri servizi* (nel senso dei sanitari?), dei WordPress con il plugin *bello ma che costi poco*. Sembra una guasconata la pagina messa in piedi da Jonathan Ive e compagnia e invece quella virgola sposta una montagna di cose.

Pensa un po’ che non hanno scritto il testo per il motore di ricerca e non hanno cercato *l’engagement* attraverso la *reach* della community *targettizzata* con le *call to action* per generare le *lead* e spedire le *DEM*.

Certo, se lo possono permettere. Ma senza quella virgola sarebbero stati solo patetici. Con la virgola ti aprono un mondo e ancora neanche hai letto il messaggio (chiedo scusa, il *body*).

La virgola se la possono permettere tutti. È pensarla in un contesto, che è di pochi.

Grazie per un breve momento di luminosa lontananza dalla mediocrità a basso impatto intellettuale merito del cugggino che ha studiato il piaccapì al corso estivo.

*Stay hungry, stay foolish* resterà nella storia. Ma un posticino per *love & fury* nel cuore ce l’ho.

*Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile.*