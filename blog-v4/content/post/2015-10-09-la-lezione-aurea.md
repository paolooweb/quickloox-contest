---
title: "La lezione aurea"
date: 2015-10-09
comments: true
tags: [iPad, iPhone, 6s, Pro]
---
Perché il prossimo [iPad Pro](http://www.apple.com/it/ipad-pro/) ha uno schermo con diagonale di 12,9 pollici e non tredici o 12,8?<!--more-->

Bisognerebbe occupare una carica di alto livello in Apple per saperlo con certezza. Tuttavia è affascinante notare che iPhone 6s, il nuovo modello, ha diagonale di 4,7 pollici.

Il rapporto tra le diagonali di iPad originale (9,7 pollici) e iPhone originale (3,5 pollici) è pari a 2,7.

Indovinato: il rapporto tra le diagonali di iPad Pro e iPhone 6s [è ugualmente pari a 2,7](http://www.apple.com/it/ipad-pro/).

Si sta arrotondando perché i due valori non sono esattamente gli stessi: uno è 2,74, l’altro 2,77. Il secondo decimale però sembra non avvertibile in qualsiasi valutazione venga fatta a livello di design e non percepibile da un utente nell’utilizzo quotidiano.

Il fatto che questa proporzione si mantenga immutata dopo cinque anni, comunque, indica una sua importanza. E va notato che, in altri mondi, questo tipo di eleganza è totalmente assente.