---
title: "Aptos a sollevare dubbi"
date: 2023-07-22T03:09:45+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [Aptos, Microsoft, Gruber, John Gruber, Daring Fireball, San Francisco, Apple, Calibri, Bierstadt]
---
Nei giorni passati si è accennato a [questioni di tipografia e uso di Word](https://macintelligence.org/posts/2023-07-13-il-tipografo-sbagliato/); motivo per riprendere un argomento che altrimenti avrei cestinato senza troppi pensieri, l’arrivo di un nuovo font preimpostato per i documenti Office dopo quindici anni di regno di Calibri.

John Gruber, che ha il pallino tipografico più sviluppato del mio, ha approfondito [l’analisi](https://daringfireball.net/2023/07/aptos) a partire da una domanda: come mai cambiare font, se Calibri è valido e ancora più visto che Calibri *è* valido? Un buon font regge con dinsivoltura qualsiasi passare degli anni e non è un caso se Pages, dal 1991, ha usato come font preimpostato Helvetica cambiando negli anni solo per Helvetica Neue, *parente superiore espanso*. E prima di Helvetica Geneva, omaggio di Susan Kare a Helvetica e nato per la barra dei menu di Macintosh.

Detto comunque che Microsoft volveva cambiare, avrebbe proposto cinque font diversi al pubblico, tra cui sarebbe stato stato scelto Bierstadt, ridenominato Aptos.

*Le aziende che hanno gusto*, chiosa Gruber, *non fanno sondaggi sul design* e per di più sembra che i gusti del pubblico siano stati rilevati leggendo i commenti su Twitter.

Siccome i cinque font sono stati poco mostrati in pubblico e per lo più in forma di esempi di grandi dimensioni, poco dirimenti per un font preimpostato per documenti Office, Gruber ha usato la versione web di Word – definita *brutale* e *frustrante* per creare campioni di testo in corpo undici in ciascuno dei cinque font da provare. Nel farlo si è lamentato del pessimo *kerning* ottenuto, da capire se responsabilità dei font o di Word edizione web (il kerning, o *crenatura*, è l’avvicinamento tra caratteri adiacenti in modo da ottenere il migliore effetto di lettura).

Si adombra anche il sospetto che sia stata tutta una manfrina e Microsoft abbia scelto da subito Aptos, dato che un paio degli altri candidati sono platealmente improponibili come default di Office. Comunque, dice Gruber,

>volendo scegliere la moda del momento invece che lo stile senza tempo, Aptos è la scelta giusta. Non è una copia del San Francisco di Apple, ma è il più sanfranciscoso dei font in lizza.

Nel corso del post si trovano alcuni commenti gustosi che lascio scoprire, a parte *non ho mai impostato una volta un font a Arial*, orgoglio che nel mio piccolo condivido.

Per quanto molti siano rimasti nel tunnel di Arial, probabilmente per sempre, da ora arriveranno anche documenti impostati con la non-scelta di Aptos. La buona notizia è che almeno gli occhi non sanguineranno come con Arial.

Così per sapere: tra sondaggi discutibili, carenza di gusto nel design, versioni web con problemi di kerning (sperando che non si debba ai font), qualcuno si affiderebbe a Word (e a Microsoft) per perseguire tipografia di qualità?