---
title: "Tutto s’aggiusta: commedia circolare in tre atti"
date: 2020-11-23
comments: true
tags: [Apple, iPad, MacBook, Pro]
---
Primo atto: cattiva Apple, perché il nuovo Mac/iPad/iPhone⁄watch/tv è una macchina chiusa e non riparabile o difficilmente riparabile. Segue trombonata sul *right to repair*.

Secondo atto: cattiva Apple, perché ho portato il mio aggeggio all’Apple Store e vogliono darmene uno diverso per cinquecento euro invece di ripararlo. Ampiamente criticabile ma, per logica, come potrebbero ripararlo a costi ragionevoli se è una macchina chiusa?

Terzo atto: cattiva Apple, perché a me non la si fa: mi sono documentato su Internet, ho trovato i pezzi con poca spesa e mi sono fatto la riparazione da solo (variante: il cinese con spesa modica). Tiè!

La morale: il *right to repair* non esiste. Né in informatica, né altrove. Esiste la *ability to repair*. semmai.