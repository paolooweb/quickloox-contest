---
title: "Lei sa chi sono io"
date: 2015-06-19
comments: true
tags: [Eff, Adobe, Apple, Credo, Dropbox, Nsa, Sonic, Wikimedia, Snowden, Cook, privacy, Nyt, Wordpress, Yahoo, Google, Mossberg, Microsoft]
---
Electronic Frontier Foundation, cane da guardia come mai ce ne sono stati a sorveglianza della *privacy* delle persone su Internet, ha [dato i voti per il 2015](https://www.eff.org/files/2015/06/17/who_has_your_back_2015_protecting_your_data_from_government_requests.pdf) alle aziende di tecnologia digitale per come tutelano la confidenzialità dei dati e difendono il diritto individuale alla riservatezza.

Di ventiquattro aziende, hanno preso il massimo dei voti – cinque su cinque – Adobe, Apple, Credo Mobile, Dropbox, Sonic.net, Wikimedia, Wordpress e Yahoo. Google è a tre sue cinque, Microsoft a tre su cinque.

Di recente, Edward Snowden – l’uomo che ha fatto scoppiare lo scandalo dello spionaggio nazionale e internazionale condotto su Internet dalla National Security Agency degli Stati Uniti – ha pubblicato un [articolo sul New York Times](http://www.nytimes.com/2015/06/05/opinion/edward-snowden-the-world-says-no-to-surveillance.html) di cui riporto un passaggio:

>Misure tecniche di base come la cifratura – una volta considerate esoteriche e superflue – oggi sono abilitate di serie nei prodotti di aziende pionieristiche come Apple, per assicurare che perfino in caso di furto di un telefono la vita privata possa restare privata.

Walt Mossberg ha titolato su *Re/Code* che [l’ultimissimo prodotto di Apple è la privacy](http://recode.net/2015/06/12/apples-latest-product-is-privacy/).

Se qualcuno dubita delle [parole di Tim Cook](https://www.apple.com/it/privacy/) in merito, magari è ora che si ricreda un pochino (o le smonti con una dimostrazione chiara e incontestabile, se è capace di farlo e ha informazioni che altri non hanno).

Apple sa chi sono io, ma non è interessata a vendermi come prodotto. Altri, beh, costano meno.