---
title: "Come l’Aqua"
date: 2013-10-01
comments: true
tags: [iOS]
---
La recensione definitiva su iOS 7, quella da leggere per sapere tutto e in omaggio pure per capire come girano le cose, [l’ha scritta Nick Heer di Pixel Envy](http://pxlnv.com/blog/ios7-review/).<!--more-->

È accurata, completa, interminabile, dal punto di vista di uno che il sistema lo usa, più che programmarlo o stressarlo oltre le sue possibilità.

La fine:

>iOS è Aqua. È una ripartenza da zero, con nuove tecnologie che nel 2007 non erano di uso pratico. Cambierà nel corso dei prossimi anni. Ma le sue fondamenta sono state gettate e sono molto solide.

Che completa perfettamente l’inizio:

>Circa un anno fa avevo chiuso la mia recensione di iOS 6 notando che Apple tende a preferire aggiornamenti sottili: “Se confrontate due versioni principali e sequenziali di iOS, non vedrete grandi rivolgimenti nel sistema operativo”. Si può quindi immaginare quanto mi senta stupido a guardare una versione di iOS dall’aspetto vastamente differente da quelle che lo hanno preceduto.

Veramente da leggere.