---
title: "Un parere terzo"
date: 2016-01-22
comments: true
tags: [Giannotta, GarageBand, Logic, Memo, loop, iOS]
---
Se lo scrivo io mi danno del fanatico. Invece [lo scrive Roberto Giannotta](https://www.facebook.com/robegian/posts/10207247685343511), uno che ne sa e parecchio.<!--more-->

C’è una manciata di commenti al *post*, perlopiù aggiunte e considerazioni suppletive di Roberto, che consiglio di leggere. Bastano trenta secondi.

>Il percorso Memo Musicali >>> Garage Band >>> Logic è un’autostrada per far arrivare a meta le buone idee, senza perderne nessuna.

<div id="fb-root"></div><script>(function(d, s, id) {  var js, fjs = d.getElementsByTagName(s)[0];  if (d.getElementById(id)) return;  js = d.createElement(s); js.id = id;  js.src = "//connect.facebook.net/it_IT/sdk.js#xfbml=1&version=v2.3";  fjs.parentNode.insertBefore(js, fjs);}(document, 'script', 'facebook-jssdk'));</script><div class="fb-post" data-href="https://www.facebook.com/robegian/posts/10207247685343511" data-width="500"><div class="fb-xfbml-parse-ignore"><blockquote cite="https://www.facebook.com/robegian/posts/10207247685343511"><p>Ma l&#039;avete visto, il nuovo Garage Band per iOS? o_O</p>Posted by <a href="#" role="button">Roberto Giannotta</a> on&nbsp;<a href="https://www.facebook.com/robegian/posts/10207247685343511">Mercoledì 20 gennaio 2016</a></blockquote></div></div>
