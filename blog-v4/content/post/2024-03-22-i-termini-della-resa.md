---
title: "I termini della resa"
date: 2024-03-22T01:49:43+01:00
draft: false
toc: false
comments: true
categories: [Web, Internet, Education]
tags: [Apple Style Guide]
---
Devo arrendermi. Quando Apple ha iniziato a usare il logo della mela invece di scrivere *Apple*, come in *watch*, la cosa mi è piaciuta tantissimo e l’ho adottata.

Ora però è uscito l’aggiornamento della [Apple Style Guide](https://support.apple.com/en-gb/guide/applestyleguide/welcome/web) in cui questa forma viene chiaramente indicata come scorretta e, diceva quel comico, non capisco ma mi adeguo.

*Apple Style Guide* era stata aggiornata per l’ultima volta nel 2022. È un’opera monumentale, la cui lettura è paragonabile a quella di un dizionario tascabile di inglese. E in effetti, se si sorvolano le parti che riguardano strettamente il marketing di prodotto, è un eccellente corso accelerato (da anni a mesi) di scrittura in inglese.

Teniamo presente che *Apple Style Guide* stessa spiega quali siano le altre risorse editoriali in uso nella società: [Merriam-Webster’s Collegiate Dictionary](https://shop.merriam-webster.com/products/merriam-websters-collegiate-dictionary-eleventh-edition) e [The Chicago Manual of Style](https://www.chicagomanualofstyle.org/home.html).

Mi arrendo. Ma ne approfitto per imparare e, quando c’è da imparare, la resa sui termini è più che onorevole.