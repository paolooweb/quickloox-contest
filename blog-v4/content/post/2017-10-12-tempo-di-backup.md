---
title: "Tempo di backup"
date: 2017-10-12
comments: true
tags: [Time, Machine, backup, Disk, Utility, Disco]
---
Tra letto e sentito, tre persone in tre giorni che si sono lamentate di Time Machine perché, nel momento del bisogno, il disco ha dato problemi.

Ogni due/tre mesi controllo puntualmente il disco di backup con Utility Disco. L’ho fatto ieri e ci ha messo circa una decina di ore. Significa che, sì, la struttura di Time Machine è complessa; significa anche che è *necessario* verificare di tanto in tanto il disco. Se è il backup, dobbiamo essere ragionevolmente certi che funzioni alla bisogna; trascurarlo visto che ora non serve è trascurare il disco di backup e ho detto tutto.

Il nuovo filesystem Apfs da questo punto di vista contiene funzioni moderne che nel prossimo futuro renderanno molto più tranquilli i sonni di chi confida nel proprio backup. Intanto, però, i dischi Time Machine continuano a essere in Hfs+ indipendentemente da quello che accade sul disco di sistema. Una volta ogni due/tre mesi raccomando un giro di verifica con Utility Disco.

A dirla tutta, conservo anche tre dischi rigidi che attacco una volta l’anno al Mac e su cui riverso disordinatamente più o meno tutti i documenti presenti nel disco di sistema. In questo modo sono ragionevolmente certo che, se fallisse Time Machine, avrei almeno una copia recuperabile – magari vecchia, ma presente – di tutti o quasi tutti i file. Oggi ho iniziato a copiare disordinatamente cose anche su un quarto disco rigido.

Non dovrebbero servire; a meno che il backup online dei documenti importanti effettuato su [Degoo](https://degoo.com) fallisca e servano cose che non si trovano in Dropbox o iCloud Drive.

Sembra un eccesso di backup? *Unicuque suum*. Per quanto mi riguarda, se domani mi avanza un quinto disco rigido, metto i documenti anche lì.