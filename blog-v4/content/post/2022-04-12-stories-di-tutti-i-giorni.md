---
title: "Stories di tutti i giorni"
date: 2022-04-12T09:28:21+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [Comandi rapidi, Shortcuts, MacStories, Automation April]
---
C’è tempo fino al 20 aprile per proporre Comandi rapidi alla giuria del [concorso Automation April](https://www.macstories.net/stories/enter-your-shortcuts-in-the-automation-april-shortcuts-contest/) indetto da *MacStories*.

Rispetto al testo visibile in pagina, [è stata aggiunta una nuova categoria](https://www.macstories.net/news/best-everyday-shortcut-a-new-automation-april-shortcuts-contest-category/): *Best Everyday Shortcut*.

>Uno dei nostri scopi principali con Automation April è incoraggiare la partecipazione di utilizzatori a tutti i livelli di Comandi rapidi. Ecco perché i giudici non si limiteranno a cercare i comandi più complessi con il maggior numero di azioni. Considereranno fattori come originalità, prestazioni, design, user experience e utilità, applicabili anche a comandi semplici.

Per rendere chiaro il messaggio di una gara non riservata agli esperti, abbiamo aggiunto una nuova categoria: Best Everyday Shortcut. La categoria sarà valutata come le altre, ma con un’enfasi sull’uso brillante di azioni per creare un Comando rapido capace di eseguire particolarmente bene un singolo compito, che possa essere di beneficio a un’ampia varietà di utilizzatori.

Una delle cose che fanno spiccare Comandi rapidi è proprio che, rispetto ad altre soluzioni come AppleScript dove la complessità decolla rapidamente appena si supera un numero minimo di righe, è possibile escogitare sequenze di azioni anche lunghe con un aumento ridotto della complessità stessa. Analogamente, è possibile elaborare soluzioni utili eppure semplici.

Sarebbe bello cimentarsi e partecipare, comunque, senza ritegno e senza complessi. Vinceranno i più bravi, ma anche chi saprà mettersi in gioco superando la linea del timore di provare una cosa nuova, a qualsiasi punto del percorso si trovi. Anche con una cosa talmente semplice e arguta da tornare utile ogni giorno.