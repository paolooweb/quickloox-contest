---
title: "Subdolo più della marea"
date: 2020-10-20
comments: true
tags: [Venezia, Windows, Lorescuba]
---
A Venezia sono riusciti a fare funzionare persino il [Mose](https://www.mosevenezia.eu) eppure, come testimonia **Lorescuba** che ringrazio, alla stazione di Venezia fanno fatica a calcolare l’Irql.

 ![Maxischermo Windows in panne alla stazione di Venezia](/images/venezia.jpg  "Maxischermo Windows in panne alla stazione di Venezia") 

A dimostrazione che l’acqua alta è un bel problema, ma qualcosa si può fare, mentre altri fastidi più pervasivi e subdoli saranno assai più difficili da eradicare.