---
title: "Caccia all’errore"
date: 2023-01-08T00:44:42+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [ChatGPT, Noam Chomsky, GPT-3, Chomsky, Marcus, Gary Marcus]
---
Voglio innanzitutto chiarire che non ce l’ho con [ChatGPT](https://macintelligence.org/search/?query=chatgpt). Anzi, ammiro lo straordinario livello di addestramento del modello e, [come ho già scritto](https://macintelligence.org/posts/2023-01-04-minimi-comuni-denominatori/), ha sacche di straordinaria utilità che gioveranno immensamente a persone preparate con un livello culturale o tecnico superiore a quello del motore di chat.

Per chi sta sotto quel livello si prospetta la probabilità di scenari non sempre gradevoli, ma tant’è. PostScript ha tolto il lavoro a tanti linotipisti e questo è stato un male per loro e per le loro famiglie, sicuramente. Al tempo stesso, il ritorno alla linotype non è pensabile.

Mi disturba la pretesa che si tratti di intelligenza artificiale e che padroneggi il linguaggio. La prima affermazione è falsa e la seconda è illusoria.

La spiegazione sta in un bell’[articolo](https://garymarcus.substack.com/p/noam-chomsky-and-gpt-3) di Gary Marcus, scritto quando ancora si parlava di [GPT-3](https://macintelligence.org/posts/2020-07-27-intelligenza-superficiale/) e la parte di chat era ancora chiusa in laboratorio.

Marcus tira in ballo anche Noam Chomsky, nel bene e nel male *il* linguista del Ventesimo secolo, e si pone una bella domanda: *i grandi modelli di linguaggio [su cui si addestra ChatGPT] sono buoni modelli del linguaggio* umano*?*

ChatGPT (e i modelli di apprendimento meccanizzato in generale) svolgono un lavoro incredibile, enorme, di predizione di che parola verrà dopo la parola di prima, in estrema banalizzazione. Se, dice l’articolo, scriviamo *il cielo è…* e passiamo la frase alla chat, sicuramente questa aggiungerà *azzurro* e certamente sarà in grado di aggiungere molto altro. Ma solo perché nel suo database ha trovato abbondanza di esempi per arrivare a questa conclusione, mica perché abbia un’idea di cielo o di azzurro. Chi abbia un pappagallo in casa lo addestri; quando l’animale avrà imparato, anche lui saprà completare la frase. Come la chat, completare la frase senza avere la minima idea del suo significato.

Questo sistema, per come è concepito e per come funziona, non può evitare di commettere errori, sempre di più e sempre più marchiani:

>Un buon sistema di andare oltre il mero completamento della frase sarebbe saper distinguere la realtà dalla fantasia. Di fatto, [GPT-3] non può. Come tutti gli altri, tende a perdere coerenza con l’allungarsi del testo e a produrre inesattezze.

La spiegazione è la chiave di tutto il pezzo:

>Non c’è una intenzione di creare disinformazione; nemmeno la capacità di evitarlo, perché GPT-3 è fondamentalmente un modello di come le parole si relazionano tra loro, non un modello di come il linguaggio si relaziona al mondo percepito. Gli umani cercano di correlare il loro linguaggio a ciò che sanno del mondo; i grandi modelli di linguaggio no. Significa che i secondi hanno ben poco da aggiungere ai primi.

Ecco perché ChatGPT, se non commette errori, può essere di grande aiuto a un programmatore o a un tecnico; che venga applicato al linguaggio umano, è incidentale.

>Questi sistemi sanno imitare con la medesima capacità i linguaggi umani e quelli che umani non sono (per esempio i linguaggi di programmazione dei computer).

Così ChatGPT e famiglia non imparano l’addizione in modo astratto, come viene insegnata (programmata) ai micrprocessori, ma per esempi. Più esempi trovano, meglio conoscono l’addizione. Vuol dire che la loro capacità di addizionare si scontra con i limiti del loro modello o della loro rete neurale: secondo l’articolo, uno studio ha mostrato che perfino una rete neurale con centinaia di miliardi di parametri ha difficoltà a lavorare *sempre* esattamente facendo aritmetica su numeri di tre cifre. Per un umano, boh, quinta elementare?

Oppure non c’è problema nel conoscere le capitali del mondo a menadito, mentre tracciare lo svolgersi di eventi nel tempo è molto più problematico.

>Se diciamo di avere cento monete in tasca e poi di averne tolte tre, il sistema potrebbe comprenderlo, oppure no. Nonostante recenti studi affermino che l’immensità del modello di addestramento può aiutare in alcune situazioni. il sistema continuerà a mancare di ragionamento e di buonsenso.

Noam Chomsky ha riscritto la storia della linguistica perché, mentre gli altri studiosi descrivevano i linguaggi, si chiedeva perché i linguaggi funzionassero nel modo in cui funzionano. Lui è molto critico verso GPT-3 perché si limita a mettere insieme parole, invece di porsi domande come farebbe una vera intelligenza artificiale.

>Non si può andare a una conferenza di fisici e dire di avere una grande teoria, che spiega ogni cosa e bastano due parole per enunciare: vale tutto. Questo è GPT-3. Funziona così o anche meglio di così su quarantacinque terabyte di dati presi da linguaggi impossibili. Si sa da sempre che una teoria deve sapere rispondere a due tipi di domande: perché questo? Perché non quello?

Ecco. Non abbiamo ancora capito la vera natura del linguaggio umano. Quando accendiamo ChatGPT, abbiamo davanti un sistema che non ne sa nulla; si limita a mettere insieme una serie di parole affidandosi alla statistica e, il più delle volte, ci prende, anche molto bene. Il più delle volte. Ma, a vederla dal lato opposto, è come se si sforzasse di riuscire prima o poi a commettere qualche errore. A un certo punto ci riesce. Sempre.

Domani diciamo una cosa su come dovrebbe essere, che cosa dovrebbe stare dentro a una intelligenza artificiale.