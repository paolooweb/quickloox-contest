---
title: "Il rumore di un clic"
date: 2016-06-06
comments: true
tags: [Chinko, Windows, Microsoft, Register]
---
Non ci avevo pensato mentre scrivevo degli [aggiornamenti a Windows 10 estorti con malizia da Microsoft](https://macintelligence.org/posts/2016-06-04-volente-o-nolente/): in certe situazioni la cosa potrebbe avere conseguenze molto più gravi di lavoro perso. Cito da [The Register](http://www.theregister.co.uk/2016/06/03/windows_10_upgrade_satellite_link/):<!--more-->

>Il [progetto Chinko](http://www.chinkoproject.com/) si occupa di circa 17.600 chilometri quadrati di savana e foresta tropicale nella Repubblica Centrafricana orientale, vicino al confine con il Sudan del Sud. Il denaro è poco e altrettanta la banda Internet. Così lo staff è rimasto più che dispaciuto quando uno dei portatili donati all’organizzazione ha cominciato ad aggiornarsi automaticamente a Windows 10, scaricando gigabyte di dati tramite un collegamento radio satellitare.

>“Se un aggiornamento forzato bloccasse uno dei nostri PC mentre coordiniamo i nostri ranger in una sparatoria contro bracconieri armati militarizzati, il loro sangue potrebbe letteralmente ricadere sulle mani di Microsoft”, ha dichiarato un componente del team.

Il clic di un grilletto può costare una vita, ma anche il clic di un mouse. O la sua mancanza, se l’aggiornamento criminale parte per conto proprio.

Questa è una situazione limite, sicuro. Eppure mi meraviglio che niente e nessuno faccia qualcosa di ufficiale, di significativo. È stata creata una cultura dove un aggiornamento forzato con mezzi al limite della truffa viene ingoiato al pari della pillola amara, con disgusto e con accettazione. Dove nessuno si stupisce più di alcunché. Dovrebbe.