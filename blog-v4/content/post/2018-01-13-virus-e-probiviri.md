---
title: "Virus e probiviri"
date: 2018-01-13
comments: true
tags: [XProtect, Mac, Stefano, virus]
---
Ho avuto una interessante conversazione con [Stefano Paganini](http://www.stefanopaganini.com) a proposito dell’obbligo di installazione di antivirus sui sistemi informatici che amministrano dati personali (ovvero tutti) in ambiti vari come il giornalismo, la ricerca scientifica, l’azienda eccetera. Il tema era *Su Mac, dove un antivirus è sostanzialmente inutile, che si fa?*<!--more-->

Il punto ovviamente non è funzionale. Esistono gli antivirus per Mac, sommamente inutili ma esistono. Il punto è soddisfare l’improbabile ma tutt’altro che impossibile visita del finanziere in verifica dell’applicazione della legge.

Legge che credo consista nel decreto 196 del 2003. L’[Allegato A](http://www.parlamento.it/parlam/leggi/deleghe/03196dl3.htm#ALLEGATO%20B) recita:

>16. I dati personali sono protetti contro il rischio di intrusione e dell'azione di programmi di cui all'art. 615-quinquies del codice penale, mediante l'attivazione di idonei strumenti elettronici da aggiornare con cadenza almeno semestrale.

>17. Gli aggiornamenti periodici dei programmi per elaboratore volti a prevenire la vulnerabilita' di strumenti elettronici e a correggerne difetti sono effettuati almeno annualmente. In caso di trattamento di dati sensibili o giudiziari l'aggiornamento e' almeno semestrale.

Che si fa?

Intanto si fa studiare bene da un avvocato la faccenda. Non ho tempo né laurea in giurisprudenza per esprimere pareri definitivi, ma ho il sospetto che esistano campi di attività nei quali questi obblighi non sussistono, diversamente dalla vulgata per cui se hai un’azienda deve esserci l’antivirus, se sei un professionista devi avere l’antivirus eccetera. Posso sbagliare, magari sto leggendo il decreto sbagliato, ma azzardo che non sia detto.

Secondo. Il problema non è tanto seguire la legge, che come al solito è scritta da un incapace di intendere. È convincere il finanziere ad andarsene a mani vuote.

Come si vede, non si parla di virus, ma generici programmi malevoli. Che – risparmio l’esegesi dell’articolo 615-quinquies – possono causare distruzioni o furti o danni ai dati personali.

macOS non ha un antivirus propriamente detto. Soprattutto non è *visibile direttamente*. Ha una funzione chiamata [File Quarantine/Known Malware Detection](https://support.apple.com/en-us/HT201940) che analizza i file in arrivo sul computer e li blocca se li riconosce come *malware**. Inoltre, con GateKeeper, da la possibilità da Preferenze di Sistema di bloccare l’esecuzione di programmi non verificati.

La parte più accessibile di File Quarantine è XProtect, reperibile in Sistema/Libreria/CoreServices.

 ![XProtect](/images/xprotect.png  "XProtect") 

Dentro il *bundle* di XProtect si trova un file .plist che contiene l’elenco dei *malware** riconosciuti.

Come si vede, il mio XProtect è aggiornato a novembre scorso. Apple lo aggiorna ogni volta che creda opportuno.

Domanda: è sufficiente avere installato macOS – contenente XProtect, il cui funzionamento è automatico – per dire di avere *attivato* lo strumento?

Domanda: XProtect è considerabile *strumento informatico idoneo*?

Domanda: XProtect si aggiorna arbitrariamente. Probabilmente si aggiorna più che semestralmente. Ma, se per un semestre non lo facesse, soddisferebbe comunque il criterio di necessità di aggiornamento?

Il comando da Terminale

`defaults read /System/Library/CoreServices/XProtect.bundle/Contents/Resources/XProtect.meta.plist Version`

Permette di conoscere il numero di versione di XProtect.

È anche possibile azionare l’aggiornamento manualmente, tramite il comando da Terminale `softwareupdate`. Questo [post](https://macops.ca/os-x-admins-your-clients-are-not-getting-background-security-updates/) è lungo ma molto interessante in materia. In sintesi, il comando è

`sudo softwareupdate --background-critical`

e permette, in teoria, di conservare un log di aggiornamenti effettuati manualmente in assenza eventuale di quelli automatici, da mostrare al finanziere.

La domanda vera è sempre la stessa: basterà a convincerlo che siamo persone perbene?

Osservazioni, esperienze, competenze a commento sono molto gradite.