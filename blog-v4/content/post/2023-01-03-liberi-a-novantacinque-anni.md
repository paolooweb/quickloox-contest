---
title: "Liberi a novantacinque anni"
date: 2023-01-03T02:12:38+01:00
draft: false
toc: false
comments: true
categories: [Internet]
tags: [Copyright, Metropolis, Sherlock Holmes, Project Gutenberg]
---
Come ogni anno, Slashdot passa in rassegna [film, libri e contenuti che entrano a far parte del pubblico dominio](https://news.slashdot.org/story/23/01/01/2356232/metropolis-sherlock-holmes-finally-enter-the-public-domain-95-years-later) perché finalmente sono scaduti tutti i termini possibili e immaginabili del copyright americano, dove si trovano scappatoie delle più varie per riuscire a prolungare il controllo sul diritto di copia (in Europa la situazione è migliore ma il lavoro da fare è tanto).

L’elenco quest’anno è ricco anche di qualità, perché comprende per esempio storie degli investigatori Sherlock Holmes e Poirot, oltre al capolavoro [Metropolis](https://it.wikipedia.org/wiki/Metropolis_(film_1927)) di Fritz Lang, da ora distribuibile, copiabile, modificabile, riutilizzabile a volontà.

L’occasione è buona per notificare inoltre che, non so quando né come, è stato finalmente ripristinato l’accesso libero dall’Italia al [Project Gutenberg](https://www.gutenberg.org), per molti mesi [possibile solo tramite sotterfugi come l'uso di una Vpn](https://macintelligence.org/posts/2020-07-05-oggi-politica/) per motivi che, fossero anche ufficiali, rimarrebbero comunque enigmatici.

Project Gutenberg ha un piccolo ma prezioso emulo italiano, [Liber Liber](https://www.liberliber.it/), da praticare e sostenere.

Uno degli elementi fissi di questa paginetta è l’invito ad associarsi a [LibreItalia](https://www.libreitalia.org), per agevolare e promuovere la diffusione del software libero a ogni livello.

Ci sono tanti modi per parlare e agire a proposito di libertà nel mondo digitale, dai film che compiono novantacinque anni alle app libere dal primo momento. È un tesoro caro da custodire e valorizzare durante questo 2023.