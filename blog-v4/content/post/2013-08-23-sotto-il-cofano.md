---
title: "Sotto il cofano"
date: 2013-08-23
comments: true
tags: [iPad, Scuola]
---
**Stefano** si prodiga molto in questo periodo e lo ringrazio. Da lui arriva questo commento, che sottoscrivo.<!--more-->

>In Olanda stanno combinando [questo genere di cose](http://www.educationforanewera.com/):

>Per dire che non basta semplicemente regalare quattro iPad a tre insegnanti: sotto il cofano c'è ben altro.

Visto che [si parlava di scuola](https://macintelligence.org/posts/2013-08-21-prepararsi-per-la-scuola/) l’altroieri e alla fine neanche stavolta sembrano cose da miliardari o da rimbecilliti. Consiglio almeno la visione integrale del filmato introduttivo – una manciata di minuti – prima di farsi un’opinione.