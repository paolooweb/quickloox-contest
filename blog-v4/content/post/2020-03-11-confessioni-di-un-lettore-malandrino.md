---
title: "Confessioni di un lettore malandrino"
date: 2020-03-11
comments: true
tags: [Simmons, NetNewsWire, Feedly]
---
Mi rimangio allegramente [quello che ho scritto tre settimane fa](https://macintelligence.org/posts/2020-01-19-problemi-che-scompaiono/) e ho deciso di dare una chance a [NetNewsWire](https://ranchero.com/netnewswire/) nella sua nuova edizione open source.

Ho continuato a leggerne e a leggerne bene; c’è il supporto di [Feedly](https://feedly.com/) che se non erro era inizialmente assente e mi consente di configurare il programma velocemente e senza pensieri; lo sviluppo del programma è stato intenso e ricco e questo in generale è un buon segno.

Aggiungo che sempre più spesso tendo a dare preferenza al software open source se appena è possibile e questo è un caso da manuale: la scommessa è trovare qualche funzione in più di quelle abituali, che migliori la mia esperienza; comunque, se anche fosse di pari livello, l’idea di viverla attraverso un programma open source ai miei occhi dà un piccolo bonus.

Il test verrà effettuato seriamente, abbandonando temporaneamente i programmi che attualmente uso su Mac e iOS e scaricando NetNewsWire sui miei tre apparecchi di lavoro.

Vediamo se Brett Simmons è riuscito ad attivare un campo di distorsione della realtà o se, effettivamente, c’è del merito nel suo lettore.