---
title: "In proporzione"
date: 2014-04-27
comments: true
tags: [iPad, Android]
---
«Interessante quel tablet! Quanto l’hai pagato?»

«[5.599 dollari](http://www.pcworld.com/article/2146941/what-kind-of-tablet-does-5000-get-you.html).»<!--more-->

(faccia dubbiosa, fronte aggrottata) «Così tanto?»

«È esattamente il modello che mi serve.»

«Hmmm. Per quello che devo fare io, mi basta e avanza un iPad, che costa meno della metà.»

«Mica detto che un iPad sia brutto, ma chi compra un tablet come questo ha i suoi buoni motivi e se compra questo invece che un iPad, vuol dire che ci vede un valore specifico e superiore.»

Sostituire, in proporzione, iPad e Android.