---
title: "Astenersi perdisoldi"
date: 2015-07-14
comments: true
tags: [9to5Mac, iPhone, Apple, Samsung, Nokia]
---
*9to5Mac* ha [ben riassunto la situazione](http://9to5mac.com/2015/07/13/apple-smartphone-profit-share/) del settore dei computer da tasca e di chi ci guadagna.<!--more-->

Quando iPhone è nato nel 2007, Nokia intascava circa due terzi dei profitti di tutto il mercato.

Nel 2012 i profitti erano divisi circa a metà tra Apple e Samsung.

Adesso risulta che Apple, con il venti percento del mercato, incamera il novantadue percento dei profitti.

Poi c’è Samsung con il quindici percento.

Esatto, totale centosette. Perché gli altri vendono perdendoci soldi.

Un pensiero semplice: se Apple guadagna molti soldi dalla vendita di iPhone, avrà interesse a fare iPhone sempre più interessanti, che si possano vendere bene. Se un’altra azienda perde soldi a vendere un prodotto, ho idea che quel prodotto sarà realizzato con una cura non proprio meticolosa.

Forse è troppo semplice. Comunque, chi vende sapendo di non guadagnare è a un passo dal giocare sporco. O ha piani ben precisi che si stanno avverando, o farebbe meglio ad astenersi.