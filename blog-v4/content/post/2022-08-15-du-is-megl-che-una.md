---
title: "Du is megl’ che uan"
date: 2022-08-15T01:29:46+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [iOS 16, Ventura, A2, Filippo, Roberto, Marin, Strozzi, Roberto Marin, Filippo Strozzi]
---
Deve essere una bella festa ed è quindi giusto che ci sia un bel podcast. Dopo [la prima prova](https://macintelligence.org/posts/2022-07-05-perseverare-podcast-diabolicum/), per la quale vale sempre la fortuna del principiante, arriva il secondo tentativo da parte del sottoscritto di guastare la puntata di [A2](https://a2podcast.fireside.fm/) prodotta da [Filippo](https://a2podcast.fireside.fm/hosts/filippo) e [Roberto](https://a2podcast.fireside.fm/hosts/roberto) per discettare delle novità presenti in [macOS Ventura](https://www.apple.com/macos/macos-ventura-preview/) e nelle altre prossime uscite di sistemi operativi di Apple.

La fortuna del principiante, al tentativo numero due, non vale più ed è successo di tutto tra blackout, connessioni sul filo della disconnessione, improvvisazioni e arte dell’intrattenimento applicata.

Ho la fortuna di avere performato assieme a due amici con un senso magico del ritmo radiofonico e dell’editing; così è venuta fuori una seconda cosa che a me piace ancora più della prima.

È una bella festa e auguro lo stesso a chiunque passi di qui.