---
title: "Sorveglianza a poco prezzo"
date: 2015-05-23
comments: true
tags: [TheIntercept, Google, Samsung, Play, spyware, iPhone, Android, man-in-the-middle]
---
Nel mondo anglosassone si parla comunemente dell’alleanza dei *Cinque occhi* per riferirsi ad azioni comuni dei servizi segreti di Stati Uniti, Canada, Regno Unito, Nuova Zelanda e Australia.<!--more-->

Se è vero quanto [racconta The Intercept]/(https://firstlook.org/theintercept/2015/05/21/nsa-five-eyes-google-samsung-app-stores-spyware/),

>i Cinque occhi pianificavano il sabotaggio dei link agli app store di Google e Samsung per infettare smartphone con spyware.

Google e Samsung. App store. Manca qualcuno, l’Apple Store con la maiuscola. Come mai? Forse perché non interessa agli spioni?

Tutt’altro: lo stesso articolo svela che i Cinque occhi hanno progettato *spyware* per iPhone e *smartphone* Android,

>software che consente di infettare i terminali e prenderne posta, testi, cronologie del web, registri delle chiamate, video, foto e altri archivi memorizzati su essi.

Tuttavia, prosegue il pezzo,

>i metodi usati dalle agenzie per arrivare a installare lo spyware sui telefoni sono rimasti poco chiari.

Non è affatto facile installare di nascosto software sul telefono di uno che lo tiene in tasca. Se però si pratica un attacco *man-in-the-middle* e si fa finta di essere il server dello store Samsung o di Google Play, il malcapitato crede di scaricare il programma X e intanto finisce per prendersi, omaggio, anche lo *spyware* Y, senza accorgersene.

I meccanismi di firma digitale dell’ecosistema iOS rendono più difficile l’operazione. Una app alterata non viene eseguita dal sistema. Per fare scaricare uno *spyware* da App Store, lo *spyware* deve trovarsi su App Store. Solo che Apple deve essere connivente, o non si può fare. E l’attacco a metà strada non è praticabile.

Per farla breve, ritrovarsi uno *spyware* su iPhone è molto più difficile. Su Android invece non ci si fanno tutti questi problemi. E che cosa vuoi che sia avere il telefono a rischio sorveglianza, quando costa così poco?