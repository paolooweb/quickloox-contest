---
title: "Mettersi in regolare"
date: 2019-08-26
comments: true
tags: [BBEdit, regex]
---
Almeno una volta a settimana ringrazio BBEdit per la sua copertura delle *regex*, espressioni regolari, i codici per cercare e sostituire testo in modo evoluto.

Su GitHub si trova un [compendio](https://gist.github.com/ccstone/5385334) delle regex come le intende BBEdit molto pratico e comodo rispetto all’aiuto del programma, ineccepibile ma di interfaccia antica (se Apple volesse adeguare l’aiuto in linea del software al ventunesimo secolo, non mi offenderei).

Non esiste uno standard per le *regex* ma BBEdit è allineato alle realizzazioni più comuni e quindi la paginetta potrà aiutare anche chi preferisce altri editor.
