---
title: "Un campeggio coinvolgente"
date: 2023-07-05T02:08:59+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [The Last Campfire, Nive]
---
Dietro sollecitazione della secondogenita, incuriosita dall’icona della app, qui si sta giocando [The Last Campfire](https://apps.apple.com/it/app/the-last-campfire/id973039644?l=en-GB): una avventura grafica di atmosfera con una storia quieta e malinconica e qualche enigma logico lungo la strada.

Non ci perderei l’estate ma neanche se ne sente il bisogno. In un paio d’ore abbiamo raggiunto quattro *achievement* su ventidue e da questo deduco che arrivare in fondo non richiederà moltissimo.

I ritmi del gioco sono lenti e rilassati. È un piacere tradurre i pochi dialoghi per secondogenita, che ha cinque anni e si emoziona per i colori e le figure animate, ma diventa concentratissima e spiegata quando si tratta di affrontare le prove logiche. Mi ha già ricordato due volte che abbiamo in programma la prosecuzione dell’avventura e questo vuol dire molto, nella sua condizione di avente diritto a guardare tutti i giochi 4+ di Arcade e, di fatto, spiare i 9+ cui accede la sorella. In qualche modo, *The Last Campfire* è coinvolgente oltre l’apparenza

I puzzle sono relativamente semplici ma per niente banali. Bella, come sopra, l’ambientazione. La ripetibilità sarà nulla, essendo un’avventura, ma la giocabilità è semplice e ben fatta. Una di quelle cose per cui i cinque euro mensili di Arcade si considerano ripagati con soddisfazione.