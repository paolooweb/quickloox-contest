---
title: "Due fastidi e una luce"
date: 2022-01-15T02:07:11+01:00
draft: false
toc: false
comments: true
categories:
tags:
---
L’innovazione, i prezzi, il lock-in…? Macché, se Apple deve irritare e forse pure preoccupare, è perché uno sceglie dalle Preferenze di Sistema di mostrare l’icona del cambio rapido di utente nel Centro controlli e l’icona non appare.

Allora decido di mostrare l’icona nella barra dei menu e funziona subito. L’irritazione però è doppia perché mi rendo conto di come nelle Preferenze di Sistema esistano _due_ modi diversi per farlo.

Grave doppio fastidio, perché queste non sono le funzioni marginali di Utility ColorSync o i confini dell’universo di GarageBand; sono le Preferenze di Sistema. Il cuore di tutto. Quello che c’è deve funzionare e, soprattutto, farlo in maniera univoca.

Una interfaccia degna del suo nome mostra un percorso per arrivare a destinazione. Uno, quello ottimale. Poi consente di ottenere lo stesso risultato con un comando da tastiera e, naturalmente, offre un accesso alla funzione anche dal Terminale, che è un mondo parallelo. Una funzione del l’interfaccia attivabile da due percorsi è una cattiva funzione di interfaccia. È un problema di design. _Apple_ e _problema di design_ devono essere ossimori.

Vuoi mettere con la funzione di torcia elettrica di watch? Mi immagino il progettista che la prova per la prima volta, in una stanza semibuia. Magia bianca, elegante, funzionale, perfetta. Prova entusiasmo per sé e al pensiero di chi la azionerà.

Apple mette la magia in quello che fa, quando lo fa bene. Altrimenti è uguale al resto.

_Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile._
