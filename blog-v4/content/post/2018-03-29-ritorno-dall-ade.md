---
title: "Ritorno dall’Ade"
date: 2018-03-29
comments: true
tags: [iPhone, batteria]
---
Curioso comportamento di un vecchio iPhone 4S oramai trascurato anche dalla figliolanza e rimasto per tutta la scorsa estate senza alimentazione.<!--more-->

La batteria, ripreso in mano l’aggeggio a settembre, sembrava senza speranza; si ricaricava ma poi bastava svegliare l'apparecchio per perdere due o tre punti percentuali di carica. L’autonomia si era ridotta a pochi minuti di uso e poche ore di *standby*.

L’ho dato per perso ma ho continuato a lasciarlo in carica nei momenti in cui c’era un cavo libero, per fare esperimenti e confronti.

Mesi dopo, la batteria sembra non dico nuova, ma in buone condizioni. Lo *standby* dura vari giorni e la percentuale di carica durante il funzionamento decresce come ci si aspetterebbe da una batteria carica, seppure di un vecchio iPhone 4S.

La morale: non è detto che una batteria morta lo sia davvero. Potrebbe inaspettatamente tornare dall’Ade.
