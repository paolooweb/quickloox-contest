---
title: "La civiltà della cucina"
date: 2014-09-16
comments: true
tags: [Word, Microsoft, Octopress]
---
Leggo un articolo intitolato [Iniziare a bloggare con Microsoft Word](http://www.cio.com/article/2682610/microsoft-office/how-to-start-blogging-with-microsoft-word.html).<!--more-->

Provo a immaginare un analogo *Iniziare a cucinare con i Quattro salti in padella*.

Degnissimi, per una situazione eccezionale. Come abitudine quotidiana è costosa e insoddisfacente, proprio come Word. L’estetica è omologata, il sapore è standard, esattamente come in Word. Il blog come espressione personale dovrebbe portare originalità. Consapevole che sto usando il tema standard di Octopress. Intanto però già Octopress non è *mainstream*.

Dico tutto ciò da incapace totale in cucina, per giunta nell’era del *food porn*. Eppure a buttare della pasta in acqua bollente, o due uova in padella, bisogna imparare. Perfino il microonde dà qualcosa da apprendere, per quanti pulsanti specialistici possa sfoggiare. Chi cucina tutti i giorni al microonde premendo un tasto?

Eppure c’è gente che si mette a bloggare con Word. Sembra poco e invece è un dramma. Perché lo stesso approccio poi percola in mille altre situazioni che impattano sulla società.