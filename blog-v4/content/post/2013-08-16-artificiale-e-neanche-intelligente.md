---
title: "Artificiale e neanche intelligente"
date: 2013-08-16
comments: true
tags: [Jobs, Apple]
---
[Brogue](https://sites.google.com/site/broguegame/) o [Sil](http://www.amirrorclear.net/flowers/game/sil/) sono cose molto più serie di certe pagine di quotidiano *online*, specie in tempo di vacanza e *relax*; la segnalazione di **Bruno** impone peraltro attenzione.<!--more-->

Si è accorto Bruno che al *Corriere della Sera* *online* sono andati tutti in vacanza e a riempire le pagine hanno lasciato una anonima intelligenza artificiale. L’artificialità abbonda, mentre il livello di intelligenza è minimo.<!--more-->

L’[articolo artificiale](http://www.corriere.it/tecnologia/13_agosto_13/apple-spacciata-larry-ellison_7045ff70-043c-11e3-b7de-a2b03b792de4.shtml) fa riferimento a una [intervista video](http://www.cbsnews.com/video/watch/?id=50152857n) concessa alla Cbs da Larry Ellison, *boss* di Oracle (su Mac serve Flash oppure Chrome oppure fare finta di essere un iPad).

>Apple senza Steve Jobs? «È spacciata»

Ellison non dice affatto ciò che gli si attribuisce nel virgolettato.

>Larry Ellison, Ceo di Oracle, è scettico sul futuro dell’azienda di Cupertino: «Steve come Edison, senza di lui durerà poco»

Ellison ha accostato Jobs a Edison e basta. Il resto del virgolettato è artificiale.

>«Abbiamo visto la Apple con Steve Jobs» e mima il pollice alto in segno di vittoria. E «abbiamo visto la Apple senza»: pollice verso.

Qualcuno spieghi all’intelligenza artificiale che per noi umani il segno di vittoria è un altro. E l’amministratore delegato di Oracle alza e abbassa *l’indice*. Il che lascia invariata la sostanza, ma rivela come l’intelligenza artificiale abbia scritto senza guardare (la fonte della notizia), o non distingua un indice da un pollice.

>La domanda è: quanto reggerà la Apple sul mercato prima di crollare? La risposta di Larry Ellison è altrettanto secca: «Non a lungo».

Domanda che non è stata posta e risposta che Ellison non ha dato. *En plein*.

Oltre a inventare, l’intelligenza artificiale si preoccupa di infarcire il pezzo di opinioni in libertà.

>Apple in bilico

Credo che qualsiasi altra azienda vorrebbe trovarsi *in bilico* con cento e passa miliardi di dollari in cassa.

>Larry Ellison non è solo il Ceo di Oracle e non ha solo dato il via alla sua azienda proprio mentre Jobs creava la «mela morsicata» destinata a diventare paradigma

A dire il vero, quando Ellison fondava Oracle il 16 giugno 1977, Apple era operativa da quattordici mesi e mezzo (primo aprile 1976).

>Sul mercato, Apple ha già vacillato su scala mondiale, eccezion fatta per le (inaspettate) alte vendite dell’iPhone 5 che hanno permesso di evitare il disastro nell’ultimo trimestre e di mantenere, attualmente, Apple al settimo posto nel più grande mercato di smartphone del mondo, ovvero il mercato cinese.

A parte la creatività sull’italiano, ad aprile l’intelligenza artificiale era ovviamente spenta. Se no avrebbe saputo che Apple aveva anticipato per i tre mesi successivi dati di vendita che sono stati pienamente rispettati. Forse le vendite (di iPhone, non di iPhone 5) non erano tanto *inaspettate*. Che significhi *vacillare* in questo contesto sfugge.

Il *disastro* forse è riferito a una decrescita annunciata dei profitti, i quali hanno comunque [sfiorato disastrosamente i sette miliardi di dollari](http://www.apple.com/pr/library/2013/07/23Apple-Reports-Third-Quarter-Results.html).

>Il prezzo delle azioni e, negli ultimi mesi, aumentato da 450 a 475 dollari, ma resta incredibilmente più basso rispetto ad un anno fa, quando una azione costava 700 dollari.

Nel frattempo si sono succeduti i migliori trimestri finanziari della storia di Apple, compreso l’ultimo trimestre che ha visto alcuni record nonostante il preannunciato calo dei profitti. L’intelligenza artificiale vorrà spiegarmi come il prezzo delle azioni sia collegato alla poca durata di Apple.

>Senza contare che la quota di mercato di iPad si è dimezzata.

Un anno prima, con un modello nuovo, Apple ha venduto 17 milioni di iPad. Un anno dopo, con un modello vecchio, ne ha venduti 14,6 milioni, con un magazzino più asciutto che di fatto riduce la flessione effettiva a meno di un milione di unità. Non sembra esattamente una catastrofe.

>Forse è il fatto che Android ha raggiunto l’80 percento del mercato globale del settore

Forse è il fatto che Apple intasca una larga maggioranza di tutti i profitti di quel mercato. Il resto lo prende Samsung e tutti gli altri guadagnano *zero* o perdono denaro.

>E anche Windows Phone di Microsoft sta acquistando una sua identità, distanziandosi dal concept dell’iPhone, e riducendo le distanze da Apple.

Si diceva appunto che le vendite di iPad erano in flessione, dalle quali l’azienda ha ricavato molto approssimativamente tra uno e due miliardi di dollari in profitti. Microsoft ha *ridotto le distanze* mettendo in conto una perdita di quasi novecento milioni di dollari per mancate vendite di Surface RT, rivale diretto di iPad. Il prezzo dei Surface RT rimanenti è stato tagliato del trenta percento, il che ridurrà forse le distanze, certo più i profitti. L’identità che sta acquistando Windows Phone tra le tavolette è quella del prodotto di nicchia, svenduto con i saldi dopo mesi di stagnazione.

Per gli *smartphone*, facciamo riferimento all’intelligenza artificiale stessa: Android ha l’80 percento del mercato, le vendite di iPhone sono state *inaspettatamente* alte… in altre parole, Windows Phone tra gli *smartphone* conta al momento niente o quasi. Mentre Apple guadagnava quattro o cinque miliardi di dollari grazie a iPhone, Nokia perdeva 151 milioni grazie ai suoi Windows Phone.

>In molti si stanno domandando cosa stia facendo Apple

Sta a girarsi i pollici, non è ovvio?

>Mentre si palesano anche dubbi sull’iPhone 5S, sul più economico iPhone 5C (fatto in plastica), su un futuro iPad e su altri prodotti che dovrebbero servire ad arginare le mancate vendite.

L’intelligenza artificiale avrebbe dovuto ricevere qualche *input* quando è stata accesa. Sono diminuiti i profitti di Apple, ma le vendite sono in linea con le previsioni diramate dall’azienda (e in tempi come questi non vanno nemmeno così male, confrontate per esempio con il mercato globale dei computer che è in contrazione). Le mancate vendite significano invenduto e oggi Apple non ha alcun invenduto degno di nota.

>Riusciranno i dipendenti della Apple a proseguire la visione di Jobs e mantenere la «fede»

Se neanche l’intelligenza artificiale riesce a tralasciare l’obbligatorio riferimento ad Apple come organizzazione parareligiosa i cui prodotti sono comprati dagli adepti, non c’è speranza, altro che fede.

Chissà se gli umani del *Corriere*, quando torneranno alle scrivanie, faranno meglio che inventarsi frasi mai pronunciate, annunciare il disastro sulla base del nulla, ignorare completamente il contesto e lo storico di quello che si scrive.