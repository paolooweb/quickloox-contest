---
title: "Fantasie perverse"
date: 2016-05-28
comments: true
tags: [Pyto, Drang, Zorn, Pythonista, Editorial, Drafts, Dropbox, iCloud]
---
Mi è venuto questo pensiero. Amo [Editorial](http://omz-software.com/editorial/) eppure ho praticamente smesso di usarlo per i suoi problemi ricorrenti di sincronizzazione con Dropbox e, in modo complementare, per la mancanza di sincronizzazione di iCloud. Al suo posto utilizzo [Drafts](https://getdrafts.com), anche se non ho il pattern di utilizzo adeguato per abbonarmi e quindi lo tengo in modalità gratuita.

L’autore, Ole Zorn, [ha già fatto sapere](https://twitter.com/olemoritz/status/1225671020784189441) di non avere al momento intenzioni di aggiornare l’applicazione. È in un periodo di scarsa attenzione allo sviluppo, che si può permettere dal momento che [Pythonista](http://omz-software.com/pythonista/) continua a vendersi su App Store e [gli paga tuttora le bollette](https://twitter.com/olemoritz/status/1225689059575484418).

Vedo però che il campo inizia a popolarsi. Per esempio, nel creare [uno script che gli elimina certe noie con l’amministrazione delle password su iOS](https://leancrew.com/all-this/2020/10/passwords-widgets-and-pyto/), Dr. Drang ha iniziato a usare [Pyto](https://pyto.app). La app è gratis in prova per tre giorni, poi costa 3,49 euro senza librerie esterne oppure 10,99 euro al massimo delle sue capacità.

Pythonista costa da subito 10,99 euro e non vede un aggiornamento da otto mesi.

Vuoi vedere, ho pensato, che Pyto pregiudica la redditività di Pythonista e che, per pagarsi le bollette, Zorn si rimette alla programmazione e magari dedica anche qualche attenzione a Editorial?

Pagherei un buon aggiornamento di Editorial senza battere ciglio e sono convinto che Zorn si rifarebbe rapidamente del tempo passato sulle migliorie.