---
title: "Metterci il cuore"
date: 2017-05-19
comments: true
tags: [Cardiogram, Eugenio, watch]
---
Ricevo, ringrazio e redigo da **Eugenio**.

**Ti inoltro questa mail**, riguardante uno studio a cui sto partecipando semplicemente condividendo i dati rilevati dalla app [Cardiogram](https://cardiogr.am) e watch.<!--more-->

watch non sarà un dispositivo medico, ma **la sua utilità è evidente**.

La mail riepiloga essenzialmente la notizia circolata in questi giorni: grazie all’utilizzo di algoritmi di apprendimento meccanizzato in congiunzione con la rilevazione del battito di watch, Cardiogram è riuscita a ottenere un tasso di riconoscimento della fibrillazione atriale [attorno al 97 percento](https://blog.cardiogr.am/applying-artificial-intelligence-in-medicine-our-early-results-78bfe7605d32).

Che è una cosa enorme.

È già successo in casi isolati che una condizione cardiaca critica venisse individuata grazie ad watch, ma distinguere con alto tasso di precisione la fibrillazione atriale è un potenziale salvavita di massa.

Aggiungo che leggere la notizia è un conto; sapere di persone vicine e note che partecipano attivamente a uno studio porta un livello di concretezza decisivo a tutta la faccenda. Guardo il mio watch e, siccome a suo tempo qualche lavoretto sul cuore l’ho fatto eseguire, mi chiedo se una installazione di Cardiogram mi porterebbe più ansia o più sollievo.

Insomma, questa è una cosa vera e improvvisamente qualunque considerazione sul prezzo di watch, se sussiste un qualunque interesse sensato per la propria condizione cardiaca, diventa futile.