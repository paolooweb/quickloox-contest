---
title: "Quasi vacanza"
date: 2015-07-31
comments: true
tags: [Octopress]
---
Un blog non va in vacanza, perché non è lavoro. Tuttavia è stata una stagione discretamente impegnativa e ho voglia di alleggerire la presenza in rete per il prossimo mese.<!--more-->

Conto di pubblicare ogni giorno, su temi mediamente più leggeri, senza preoccuparmi troppo della copertura integrale dell’argomento.

Si parlerà di giochi, magari di scripting o Swift, e può darsi mi concederò qualche riflessione pseudofilosofica, di quelle sulla sdraio rubata allo stabilimento in spiaggia a mezzanotte.

Ah, e mi piacerebbe portare il blog su [Octopress 3.0](https://github.com/octopress/octopress), possibilmente chiudendo qualche bug e imparando qualcosa.

Riuscirò sì e non nella metà di quanto detto e conto sull’indulgenza generale di chi ascolta, che ringrazio profondamente è sempre per l’attenzione e la presenza.

Buon agosto a tutti!