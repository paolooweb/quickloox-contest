---
title: "Il libero SUSEguirsi delle stagioni"
date: 2023-11-03T23:40:48+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [Rhel, CentoOS, Linux, Oracle, Red Hat, OpenELA]
---
In piena estate si parlava di Red Hat e del suo [giocare ai limiti del regolamento con le licenze open source](https://macintelligence.org/posts/2023-07-16-superior-stabat-red-hat/) allo scopo di proteggere le proprie fonti di reddito.

Uno dei temi sul tavolo era la nascita di una nuova *distro* curata dagli insoddisfatti, che fosse compatibile con Red Hat Enterprise Linux ma accessibile secondo lo spirito delle regole, oltre che stando alla forma.

Siamo in pieno autunno ed ecco [OpenELA](https://www.zdnet.com/article/ciq-oracle-and-suse-unite-behind-openela-to-take-on-red-hat-enterprise-linux/), a cura di Ciq, Oracle e Suse.

A un certo punto entreremo nell’inverno e qualcuno rischia di trovare sotto l’albero solo campioni di combustibile fossile e neanche della varietà pasticcera.