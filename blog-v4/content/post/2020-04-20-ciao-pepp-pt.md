---
title: "Ciao PEPP-PT"
date: 2020-04-20
comments: true
tags: [PEPP-PT, DP-3T, Immuni, Bending, Spoons, Frediani, Flora, coronavirus, Covid-19]
---
Dalle mie parti si dice *ciao pepp* quando qualcosa è andato male o andrà male e nulla si può più fare a riguardo.

[Pepp-Pt](https://www.pepp-pt.org/), chiedo scusa per le banalizzazioni atroci che stanno per seguire, è un gruppo di ricercatori che lavora a livello europeo per una soluzione *centralizzata* di tracciamento contatti destinata ad app utili a consentire l’avvio della fase due, una volta che il contagio sia in fase discendente, e favorire la circolazione delle persone senza fare lo stesso per quella del virus.

Centralizzata. È possibile che i dati vengano raccolti su un server unico.

[Dp-3t](https://github.com/DP-3T/documents), invece che un droide, è un sottogruppo di Pepp-Pt al lavoro su una soluzione di tipo *decentralizzato*.

Decentralizzato. Nessuno ha accesso ai dati di tutti.

Dp-3t *era* un sottogruppo. Il loro lavoro è cessato e [se ne sono andati](https://nadim.computer/posts/2020-04-17-pepppt.html), pare anche sbattendo la porta. L’infrastruttura di *contact tracing* europea sarà centralizzata o centralizzabile.

Il governo italiano ha deciso di usare per il tracciamento contatti una app di nome [Immuni](https://www.agendadigitale.eu/cultura-digitale/immuni-come-funziona-lapp-italiana-contro-il-coronavirus/), che debutterà prossimamente. Immuni seguirà il modello Pepp-Pt.

I dati che verranno generati da Immuni saranno elaborabili in forma centralizzata dallo stato e, volendo, a livello europeo. Nulla vieta che all’utilizzo di Bluetooth per il tracciamento si aggiunga in seguito l’uso del Gps. Tradotto: oltre che sapere chi hai incrociato per strada, il governo e per estensione l’Europa sanno anche dove questo è accaduto, con precisione di pochi metri.

La [soluzione annunciata da Apple e Google](https://macintelligence.org/posts/2020-04-11-una-nuova-carriera-per-alice-e-bob/) è fortemente decentralizzata e anonimizzatrice. Quella che si preparano a usare lo stato italiano e l’Europa è centralizzata, con fortissimi rischi di sicurezza e privacy, e nessuna garanzia di anonimato né di utilizzo etico e rispettoso dei dati, se non la fiducia nello stato suddetto. Allegria.

Quanto sopra è una sintesi barbara e violenta di una situazione, al solito, confusa e cangiante. Ci vorrebbero dei *forse*, *però*, *probabilmente*; il punto da cogliere tuttavia resta ed è che l’Europa ha tolto di mezzo l’idea di un sistema di raccolta dati che tuteli il cittadino al massimo grado, a favore di  una tutela ipotetica, non garantita, elastica, mutevole e dipendente dalle voglie del politico del momento.

Approfondimento qualificato e sintetico: [Matteo Flora](https://www.facebook.com/Lastknight/posts/10158445457007053).

Approfondimento qualificato e dettagliato: [Carola Frediani](https://guerredirete.substack.com/p/guerre-di-rete-contact-tracing-scontri).

Conclusione: Immuni ha le carte in regola per essere il tradizionale pasticcio all’italiana di incompetenza, interessi inconfessabili, indifferenza per i diritti del cittadino e opportunismo della politica, non importa sulla pelle di chi. Applicherò nei suoi confronti un perfetto distanziamento sociale. E *ciao pepp*.
