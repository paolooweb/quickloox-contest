---
title: "Parliamo di marketing"
date: 2015-01-10
comments: true
tags: [Panic, BBEdit, BareBones, Transmit, Coda]
---
Quando leggo l’[elenco degli interventi](http://www.barebones.com/support/bbedit/current_notes.html) su un aggiornamento di BBEdit, in quel momento potrei concludere qualsiasi acquisto di impulso di un software di Bare Bones.<!--more-->

Quando leggo il [rapporto annuale di Panic](http://www.panic.com/blog/the-2014-panic-report/), al termine ho voglia di comprare Transmit o Coda. Anche se mi servono poco o nulla.

Sono cose che testimoniano la bontà di un lavoro mille volte più di qualsiasi pubblicità o strombazzata su Facebook.