---
title: "Programmazioni scolastiche"
date: 2021-07-29T00:58:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [Apple, Australia, Queensland, Swift] 
---
È già iniziato il balletto sulla ripresa della scuola a settembre, con preoccupazioni fondamentali come aprire necessariamente in presenza e vaccinare il corpo docente.

Mi dico: da marzo 2020 a settembre 2021, passato un anno e mezzo, la scuola si sarà certamente organizzata per lavorare in presenza e in sicurezza, sempre e comunque. Abbiamo un ministro che si occupa apposta della scuola, certamente ha predisposto al meglio ogni cosa.

Anche rispetto alle vaccinazioni, noto che da tempo sono aperte le prenotazioni per qualsiasi fascia di (maggiore) età. Sicuramente i professori si sono prenotati per tempo e si sono vaccinati o si accingono a farlo. In Lombardia, perfino a prenotare una prima dose ieri si trova posto per fine agosto e già mezza vaccinazione inizia a fare la differenza. Abbiamo un ministro anche qui e male che vada si organizzeranno eventi speciali di vaccino apposta per i docenti; dopotutto, a marzo si prometteva che tutti gli italiani sarebbero stati vaccinati entro fine estate e a giugno, comunque della promessa era rimasta almeno la prima dose. Fine estate è il 23 settembre; avere il cento percento dei docenti con la prima dose entro quella data, per la scuola italiana dove è in pieno svolgimento il valzer delle cattedre, sembra un progresso clamoroso.

Insomma, tutto va per il meglio.

Dall’Australia, invece, arrivano aggiornamenti critici riguardanti lo Stato del Queensland, dove irresponsabilmente – più che pensare ai temi strategici come i nostri – [cresce nei college l’adozione del linguaggio Swift di Apple](https://macdailynews.com/2021/07/28/australian-educators-embrace-apples-swift-programming-language/) per insegnare coding, introdurre allo sviluppo delle app, preparare neanche tanto al futuro (che poi sarebbe presente) quanto al pensiero computazionale e alla logica.

A questi australiani il sangue è proprio andato alla testa e per forza, stando loro tutto il giorno a testa in giù: non vorranno mica preparare seriamente gli studenti al mondo del lavoro e dare loro un’istruzione adeguata alla realtà corrente. Con tutte le quisquilie di cui la scuola può occuparsi in sicurezza (di perdere tempo e farne perdere), occuparsi di questioni concrete e con ricadute effettive è davvero un affronto.

*Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile.*