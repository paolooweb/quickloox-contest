---
title: "Completo o coerente?"
date: 2020-11-30
comments: true
tags: [Hofstadter, Gödel, Hilbert, Russell, Whitehead, Lisp]
---
Si vede che è un segno dei tempi o chissà: dopo un [articolo veramente bello](https://macintelligence.org/blog/2020/10/22/un-libro-in-un-articolo/) dedicato a Gödel e al suo teorema dell’incompletezza, eccone [uno veramente fantastico](https://stopa.io/post/269).

Sono di parte: si parla di Gödel e, per descrivere il teorema che lo ha posto tra gli immortali della matematica, si usa Lisp!

La cosa sorprendente è che dura il giusto, si capisce senza bisogno di sapere di matematica, è chiarissimo e oltretutto uno può anche guardare il codice Lisp prodotto, pure lui luccicante nella sua linearità.

Anche stavolta fuori tema; però questa è l’essenza di come sono fatte le cose. Chi vuol essere coerente, non sarà completo; chi vuol essere completo, non sarà coerente. Vale anche come consiglio per ben disporsi alla lettura di certe recensioni-paura.