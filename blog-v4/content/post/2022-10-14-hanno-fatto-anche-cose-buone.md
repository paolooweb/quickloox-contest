---
title: "Hanno fatto anche cose buone"
date: 2022-10-14T01:47:31+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [Windows, Blocco Note]
---
Ecco, è il giorno in cui ammetto che invidio una cosa presente su Windows.

Un programmatore è riuscito a [fare girare Doom dentro il Blocco Note](https://arstechnica.com/gaming/2022/10/how-to-get-doom-running-in-windows-notepad-exe/).

Non è la conversione in sé. Con un po’ di buona volontà, [Doom gira tranquillamente nel Terminale](https://gamerant.com/doom-text-based-terminal/).

Ma vuoi mettere averne una copia in TextEdit?