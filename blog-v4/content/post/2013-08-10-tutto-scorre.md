---
title: "Tutto scorre"
date: 2013-08-10
comments: true
tags: [Giochi, iOS]
---
[Badland](https://itunes.apple.com/it/app/badland/id535176909?l=en&mt=8), iPhone e iPad a 3,59 euro: un gioco *sidescrolling*, dove la scena scorre gradualmente da sinistra a destra, come sa chi abbia provato [Jetpack Joyride](https://itunes.apple.com/it/app/jetpack-joyride/id457446957?l=en&mt=8) per esempio.<!--more-->

L’atmosfera fantascientifica di *Jetpack Joyride* lascia qui il posto, però, a una giungla del dopobomba, luce spettrale e sguardi di creature solo apparentemente familiari.

Un piccolo clone svolazzante viene espulso insieme a spazzatura industriale da una tubatura in un primo piano buio e meccanicamente minaccioso. Dovremo governare i suoi svolazzi per fargli scampare mille pericoli, nel corso dei quali incontreremo centinaia, migliaia di altri cloni. Più ne salviamo, più siamo bravi.

A questo punto, o è una roba noiosa oppure hanno lavorato bene. Hanno lavorato bene. A volte la scena che scorre serve a mostrare il paesaggio, a volte bisogna andare almeno altrettanto veloci o perdere ignominiosamente.

Ci sono a disposizione vite infinite, ma giocare senza rifletterne porta a perderne molte e quando sono troppe vengono sacrificati cloni che avevamo salvato in precedenza.

Ogni livello è composto da più fasi e, se si perde, si può ripartire all’inizio della prima fase non completata. La rigiocabilità solletica l’orgoglio del giocatore: la prima volta è inevitabile perdere un sacco di cloni o non capire la meccanica di certe trappole e viene voglia di riprovare per salvare più cloni.

Fantastico il *multiplayer* sull’iPad locale: si può giocare fino in quattro, su livelli disegnati apposta, e chi arriva più lontano vince. Se per caso è davanti il cugino antipatico, niente di più normale che dare una spallata al suo clone e mandarlo contro una sega circolare, una mina, una superficie appiccicosa, un *malus* invece che un *bonus* e via dicendo. In compagnia c’è divertimento da spendere.

Bisogna però apprezzare l’atmosfera da dopobomba, crudele e cinica. Diventa esperienza frequente affrontare un passaggio difficile con uno sciame di cloni fino a che alla fine ne passa indenne solo uno, gli altri schiacciati, trafitti, sepolti o banalmente troppo lenti per il ritmo del gioco e rimasti indietro.

In compenso la fluidità è perfetta, la colonna sonora è da amplificazione – vietato giocare senza sonoro, si perde coinvolgimento – e l’inventiva degli autori sembra senza fine. È impossibile trovarsi a giocare troppo a lungo e allo stesso tempo scoccia non riuscire a passare una trappola che ancora non si è compresa. A volte è questione di precisione, a volte di pura riflessione. Fantastiche le fasi in cui il clone vola su un *bonus* temporale e l’intero gioco rallenta con grande effetto cinematico.

Non sono passatempi seri, valgono qualche minuto di ombrellone, la fine di una pizzata tra amici, l’attesa del risciò (il gioco si sincronizza tramite iCloud e si può continuare su iPod touch o iPhone quello che si era iniziato su iPad o viceversa). Se l’ambientazione attira (mi raccomando il sonoro), c’è anche un po’ di impegno da metterci. Altrimenti non si farà gran strada. Tutto scorre piuttosto velocemente, in *Badland*.