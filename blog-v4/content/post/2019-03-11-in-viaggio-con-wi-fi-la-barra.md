---
title: "In viaggio con Wi-Fi - la barra"
date: 2019-03-11
comments: true
tags: [Wi-Fi, Monterrey, Houston, United]
---
Volo da Monterrey (Messico) a Houston (Texas), organizzato da United. Il Wi-Fi di bordo è dotazione di serie. È persino funzionante prima del decollo e mando alcuni messaggi con grande tranquillità.

Ci alziamo in volo e la connessione scompare, giustamente perché l’idea è che il servizio funzioni in quota. Raggiunta la quota, provo a caricare una pagina web. La barra di progressione si lancia con entusiasmo verso metà percorso e poi si ferma, per non dare più segni di vita sino all’atterraggio.

Inviare messaggi dalla posta di decollo (con il Wi-Fi di bordo, intendo) è un inedito gustoso. Certo, i cento minuti successivi potevano essere migliori.
