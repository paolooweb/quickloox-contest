---
title: "A chi server"
date: 2014-11-23
comments: true
tags: [Server, OSX, ArsTechnica]
---
L’anno scorso, lavorando sulle versioni preliminari di Mavericks, ho installato anche Server. Mal me ne incolse: anche arrivati alla versione definitiva, il computer era orribilmente rallentato.<!--more-->

Dopo svariati tentativi di sistemare le cose ho eradicato Server dal sistema (pochi minuti) e dato la caccia a svariati processi randagi, difettosi, fuori luogo che intasavano di lavoro inutile il processore (settimane).

Adesso sto leggendo la [recensione di Ars Technica](http://arstechnica.com/apple/2014/11/a-power-users-guide-to-os-x-server-yosemite-edition/) che parla decisamente bene della versione Yosemite.

Molto probabilmente il mio è stato uno di quei casi sfortunati in cui qualcosa va storto subito e non si riaggiusta. Al tempo stesso, l’idea di riprovare Server per provarlo (non per applicazioni reali) mi lascia dubbioso.