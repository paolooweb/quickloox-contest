---
title: "Fuori quota (di mercato)"
date: 2015-11-02
comments: true
tags: [WoW, Blizzard, Metal, HotS, SCII, OpenGL]
---
Per quelli che ancora pensano sia una cosa sensata parlare di quote di mercato o di nicchie, sempre di mercato: nel 2015 un gioco che non giri ragionevolmente ovunque è scritto male, oppure scritto in economia e, dunque, probabilmente scritto male.<!--more-->

Tanto per dire: World of Warcraft, che è tuttora il gioco di ruolo di massa su Internet più frequentato e pagato, è di Blizzard. E così [si esprime Blizzard](http://us.battle.net/wow/en/forum/topic/18000263457?page=4#74) a proposito del supporto di Metal:

>Nonostante le voci che girano… World of Warcraft sta girando [internamente] su Metal. Probabilmente lo abiliteremo prima di fine anno. Nel semestre successivo faremo lo stesso con StarCraft II e Heroes of the Storm. Terremo in funzione OpenGL per le persone con [OS X] 10.9 o 10.10 oppure un computer antecedente al 2012.

Una volta Mac era la cenerentola del mercato PC, in numeri, e oggi in fondo lo resta. Ma nel 2015, un gioco fatto come si deve è pensato per funzionare anche su Mac. Perché non puoi più permetterti di rinunciare a fette di mercato, anche fossero il cinque percento.