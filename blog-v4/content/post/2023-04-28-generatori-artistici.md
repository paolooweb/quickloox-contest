---
title: "Generatori artistici"
date: 2023-04-28T00:22:15+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [intelligenza artificiale, AI, IA, chatbot, ChatGPT, Bard]
---
Posso pubblicare solo chi è.

Oppure solo che cosa ha detto.

Scelgo la seconda.

>Il mio lato umanistico è sempre stato reverente verso il genio umano deluso e/o scioccato quando l’intelligenza artificiale ha fatto cose che non mi aspettavo o volevo facesse, come battere Lee Sedol a Go, e naturalmente la recente ondata di modelli linguistici e generatori artistici alla DALL-E. Questo tipo di sistemi mi ripugna per come li trovo tanto terrificanti quanto profondamente deprimenti.

>Convengo su quanto sia urgente, anzi imperativo, che l’umanità contenga questi mostri usciti dalla lampada e impossibili da rimettere via, forieri di pensieri agitati e confusi rispetto a sviluppi tecnologici che odio e temo profondamente.

Per buttarla sul pop, la locuzione *generatori artistici* è geniale e riporta alla mente Battiato quando cantava

>Mandiamoli in pensione / i direttori artistici / gli addetti alla cultura.

Non avevo capito subito che il verso evidenziava contraddizioni in termini: l’arte non si può dirigere, la cultura nasce da una identità e non come pratica per addetti di un ufficio.

Allo stesso modo, *generatori artistici*. Se qualcosa è generato, sarà anche meraviglioso, però certamente non è arte.