---
title: "Il bello della riproduzione"
date: 2023-03-06T00:09:18+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [Healy, Kieran Healy, The Plain Person’s Guide to Plain Text Social Science]
---
È tornata alla ribalta in modo importante per la mia quotidianità di ieri la *Plain Person’s Guide to Plain Text Social Science* di Kieran Healy, [cui avevo accennato](https://macintelligence.org/posts/2019-12-07-scrivere-come-se-fosse-il-1979/). Per questo motivo:

>Si può svolgere lavoro produttivo, aggiornabile e riproducibile con ogni tipo di configurazione software. Questa è la ragione principale per la quale non vado in giro a incoraggiare chicchessia a convertirsi all’uso delle applicazioni che uso io. […] Così questa esposizione non è orientata a convincere alcuno che esista Una Vera Via all’organizzazione delle cose.

All’interno dei limiti ovvi e ragionevoli, quello che si fa con un programma, se è valido, può essere fatto con un altro programma. Se ti dicono che è necessario Excel, o InDesign, o Windows, o qualunque altra cosa, bisogna arricciare il naso.

Usare software dà più gusto se è possibile la riproduzione.