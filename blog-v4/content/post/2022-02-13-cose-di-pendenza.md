---
title: "Cose di pendenza"
date: 2022-02-13T01:44:11+01:00
draft: false
toc: false
comments: true
categories:
tags:
---
Sono molto stanco e molto soddisfatto della settimana appena trascorsa, a tutti i livelli. Sono una persona fortunata e devo ricordarmelo.

Stanotte va pure in onda il Super Bowl, che da privilegiato [guarderò con il commento originale](https://macintelligence.org/posts/2022-02-08-e-tutto-un-prepararsi/); arriverò a lunedì ancora più stanco e soddisfatto (specialmente se sarà una bella partita).

Mi si perdonerà, spero, se per oggi abdico ai miei doveri di cronaca a favore di un vacuo intrattenimento mediale. Ma davvero, in questo momento la capacità di concentrazione e di analisi ha livelli pari all’utilità di [questa barra di progressione](https://twitter.com/neilsardesai/status/1492287582952169475).

<blockquote class="twitter-tweet"><p lang="en" dir="ltr">Physically accurate slider <a href="https://t.co/xBRUdUjsMd">pic.twitter.com/xBRUdUjsMd</a></p>&mdash; Neil Sardesai (@neilsardesai) <a href="https://twitter.com/neilsardesai/status/1492287582952169475?ref_src=twsrc%5Etfw">February 12, 2022</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

