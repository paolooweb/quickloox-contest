---
title: "Il costo della semplicità"
date: 2019-08-18
comments: true
tags: [Apple, Jobs, Mac, iMac]
---
Dopo avere letto una certa dose di esagerazioni sulle conseguenze dell’uscita di iMac, del quale è appena [ricorso l’anniversario](https://www.macworld.com/article/1135017/imacanniversary.html), ho sentito il bisogno di cercare qualcosa di più fondativo. Sempre nell’ambito delle letture pazzamente inutili quanto adeguate per l’agosto in vacanza.

L’ho trovato in questo articolo di Steve Jobs, [Quando abbiamo inventato il personal computer…](http://www.tenmilecreek.net/images/pdf/Steve_Jobs_When_We_Invented_the_Personal_Computer.pdf). Jobs ha cessato da tempo di essere di moda e oggi il trattamento più tipico che riceve è l’insistenza sui difetti e sulle mancanze. Tuttavia ha anche fatto e pensato qualcosina di significativo. Quando leggo una cosa del 1981 perfettamente valida trentotto anni dopo, fatico a restare indifferente.

>Crediamo che la leadership e il successo continuativo di Apple risulteranno dall’innovazione, non dalla duplicazione. Innovazione di prodotto e di marketing così come di distribuzione.

Dedicato a quanti volevano che Apple facesse un *netbook* o sminuiscono il lavoro di Tim Cook perché non è nato genio ma si è formato professionalmente attraverso la perfezione nell’operatività aziendale.

Poi c’è questo:

>Abbiamo imparato come uno dei fattori della nostra crescita sia che ci vogliono venti ore per diventare veramente fluenti nell’uso del proprio Apple [II]. Ci piacerebbe portare questo tempo a meno di un’ora. Il modo è impiegare una porzione maggiore della potenza di calcolo nell’imterfaccia utente. I futuri sistemi di Apple dedicheranno una quota maggiore di intelligenza a tradurre o adattare l’infornazione in modi già familiari alle persone, invece che forzare loro ad adattarsi al computer.

Se c’è qualcosa che spiega la direzione da tenere nella tecnologia al servizio degli umani, è questa. Con un corollario interessante:

>Rendere un compoter più semplice da usare richiede un computer più sofisticato. Più il computer è sofisticato, più è costoso.

Se per capire quanto vale un computer contasse solo il prezzo, o quanto è grande il disco, o quanto è frenetico il processore, l’informarica sarebbe più simile in effetti all’ortofrutta, detto con grande rispetto per il comparto.

iMac non era il più veloce, né il più economico. Eppure è di lui che ci ricordiamo.
