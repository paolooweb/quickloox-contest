---
title: "Manager e leader"
date: 2020-08-10
comments: true
tags: [Jobs, Cook, MacDailyNews, Wsj, Mickle, Powell, Gogue, Auburn]
---
Steve Jobs sapeva che si possono fabbricare milioni di iPhone tutti uguali, ma uno Steve Jobs è irripetibile. Così si adoperò per la successione ad Apple così che alla sua amministrazione seguisse qualcosa di segno diverso.

L’errore è credere che per forza di cose serva sempre uno Steve Jobs e un articolo di Tripp Mickle sul *Wall Street Journal*, [ripreso da MacDailyNews](https://macdailynews.com/2020/08/09/how-apples-tim-cook-builds-upon-steve-jobs-successes/), spiega perché.

>Jay Gogue, già presidente della Auburn University, ha dichiarato di avere discusso con Tim Cook della convinzione di Colin Powell [generale dell’esercito statunitense] per cui il management ha il compito di spostare un esercito da un punto a un altro, mentre il leader sposta un esercito dove nessuno avrebbe mai pensato che fosse possibile. “Ci sono momenti per essere un bravo manager e momenti per essere un bravo leader”, dice Gogue. “Lui lo sa”.

Apple è l’azienda più capitalizzata al mondo perché Cook ha consolidato i traguardi raggiunti su strade aperte dalla visione di Jobs. E a pensarci bene dopo tanti anni, per raggiungerli, quei traguardi, serviva anche organizzazione capillare, non solo intuizione e genio.