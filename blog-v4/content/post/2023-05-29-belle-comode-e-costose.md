---
title: "Belle, comode e costose"
date: 2023-05-29T02:14:23+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [iA Presenter, Keynote, Markdownn]
---
In riferimento a quanto [appena raccontato](https://macintelligence.org/posts/2023-05-28-centro-di-levit%C3%A0-permanente/), va detto che si dovrebbe essere coraggiosi e provare software ardito, giovane, con approcci nuovi.

Bisognerebbe dare fiducia a [iA Presenter](https://ia.net/presenter), per esempio, che promette presentazioni fatte a partire da testo [Markdown](https://www.markdownguide.org), con tutta una serie di facilitazioni e funzioni interessanti intorno, come sfondi di colore variabile generati automaticamente o la restituzione di uno *handout* di testo per distribuirla al pubblico.

Tutte cose belle. Ora però, anche senza considerare i settantanove dollari di acquisto one-time (altrimenti c’è un odioso abbonamento), venerdì ho dovuto *improvvisare* e inoltre mi sono educato a usare meno testo che posso. La strutturazione ordinata in Markdown, nella mia situazione, avrebbe davvero giovato poco.

Per improvvisare, Keynote su iPad non si batte. iA Presenter merita sicuramente una chance perché è interessante… ma gira solo su Mac.

Insomma, ci sono app là fuori che giustificano tranquillamente anche una pazzia, perché sono veramente innovative e fuori da schemi classici. Nel momento del bisogno, tuttavia, c’è sempre un motivo a scoraggiarne l’impiego, a favore della vecchia minestra consolidata che è Keynote. Accidenti se è efficace a mille, però.