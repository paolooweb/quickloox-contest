---
title: "Ripartire dal sottosuolo"
date: 2022-05-05T15:08:38+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [BBEdit, shellcheck, Dostoevskij, Fëdor Dostoevskij, Memorie dal sottosuolo, Scripting OS X]
---
Sottosuolo perché [le pagine di Dostoevskij](https://neripozza.it/libri/memorie-dal-sottosuolo) sono attuali e potrebbero informare meglio un sacco di gente che ha problemi dentro sé e sceglie una via sbagliata per affrontarli o dribblarli, a seconda dell’intenzione di partenza.

Sottosuolo perché _Scripting OS X_ parla [dell’uso di shellcheck per eseguire il controllo sintattico degli script di shell, attuato dentro BBEdit](https://scriptingosx.com/2022/04/use-shellcheck-with-bbedit/).

Il che è parallelo al comportamento delle persone suddette, solo che porta beneficio. Invece di avere problemi dentro il Mac, si va a risolverli (io in genere vado a crearli e shellcheck mi serve più che il filo interdentale). Invece che tenerle dentro, pardon, sotto il cofano, le soluzioni arrivano al livello dell’interfaccia utente, salgono in superficie.

Conciliare il sotto-il-cofano con il sopra-il-motore. Un percorso sempre raccomandato, che porta sempre risultati, anche quando si sbaglia. Sbagliare è benvenuto per imparare e shellcheck segnala pure le correzioni da apportare.

