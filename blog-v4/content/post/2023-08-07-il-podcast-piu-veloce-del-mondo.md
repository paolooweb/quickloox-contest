---
title: "Il podcast più veloce del mondo"
date: 2023-08-07T13:34:27+01:00
draft: false
toc: false
comments: true
categories: [Internet]
tags: [A2, Marin, Strozzi, Roberto Marin, Filippo Strozzi, Wwdc]
---
Beh, probabilmente non è vero, anche se dalla [registrazione](https://macintelligence.org/posts/2023-08-04-le-ore-dei-dilettanti/) al [prodotto finito](https://a2podcast.fireside.fm/65) è passato pochissimo. 

Però è stato il più interessante e il più divertente, per me. È facile perché mi sono accorto che c’è un algoritmo infallibile quando faccio l’ospite di A2: ogni volta è più divertente di quella prima.

Ed è sempre interessante, visto che Filippo e Roberto sono fiumi in piena. Gli dai una WWDC e possono andare avanti a parlare per ore.

Ora si tratta di organizzare un’altra WWDC. Più facile che organizzare il mio setup come si deve senza fare pasticci.

Buon ascolto!

*Gli aggiornamenti in agosto del blog potrebbero essere irregolari o infrequenti.*