---
title: "Metterci le facce"
date: 2021-03-27T01:25:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [Panic, Audion, Playdate] 
---
Due faccende molto diverse, il retrocomputing e la conservazione del software del passato, cioè di pezzi della nostra storia.

Panic le ha messe insieme in modo mirabile rispetto ad Audion un suo riproduttore musicale del tempo che fu. Audion era, si diceva allora, *skinnable*, ovvero la sua presentazione grafica poteva cambiare arbitrariamente da parte di chi producesse nuove “pelli” per il programma seguendo regole prestabilite.

Oggi Panic le chiama *face*, facce, ma la sostanza non è cambiata. Solo che Audion non è più in commercio da tempo e la sua compatibilità con il presente è nulla. Che fanno loro? Producono una nuova versione minimale del programma, con licenza *open source*, in grado di girare su Mac moderni, capace di fare poco più che riprodurre brani in sequenza, e adattano tutte le facce create a oggi per Audion, in modo che le si possano visualizzare sulla nuova versione suddetta.

Il totale delle facce disponibili era ottocentosessantasette a gennaio e ne mancavano altre quindici da completare, secondo il blog di Panic.

La quale Panic è una software – e [non solo](https://play.date) – house che lavora a pieno ritmo e certamente non ha bisogno di inventarsi modi di passare il tempo. Eppure ha trovato il modo di realizzare questa piccola grande impresa che riporta in vita una biblioteca caratteristica di creatività e ingegno grafico, tutto gratis e a disposizione.

Da qui potrebbero dipartirsi molti percorsi di pensiero: la grandezza del ruolo degli sviluppatori indipendenti nel successo di Mac, il valore di dedicare tempo a progetti solo superficialmente improduttivi, come si diceva prima la conservazione storica del software eccetera.

Per ora mi contento di leggere il post sul blog Panic, che è veramente gustoso, e di imparare la loro lezione: si può fare business cui fare tanto di cappello e intanto mantenere viva la passione, con sopra un pizzico di follia che fa solo bene.

*Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile.*