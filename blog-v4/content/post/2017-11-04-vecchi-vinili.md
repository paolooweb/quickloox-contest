---
title: "Vecchi vinili"
date: 2017-11-04
comments: true
tags: [iPhone, iPad, Mac]
---
A un certo punto iPhone ha cominciato a [fatturare più di Mac](http://www.businessinsider.com/iphone-profit-2012-8?IR=T) e qualcuno ha cominciato a spiegare che si pensava solo agli iPhone, trascurando i Mac.

Poi iPad ha cominciato a [fatturare più di Mac](https://www.statista.com/statistics/382260/segments-share-revenue-of-apple/) e allora il mantra è cambiato: si pensava solo a iOS, così trascurando Mac (il vento, da allora, [è cambiato](https://www.theverge.com/2015/4/27/8505411/apple-revenue-mac-vs-ipad)).

Oggi i servizi di Apple [fatturano più dei Mac](https://9to5mac.com/2016/10/26/aapl-services-revenue-importance/) e mi immagino che qualcuno commenterà sull’abbandono dei Mac a favore dei servizi.

A me sembra di sentire vecchi vinili che gracchiano su piatti starati. Essendosi venduti [5,38 milioni di Mac](https://www.apple.com/newsroom/pdfs/fy17-q4/Q4FY17DataSummary.pdf) in questo trimestre, [quasi record](https://www.computerworld.com/article/2998817/apple-mac/mac-sales-reach-record-high-as-unrelenting-ipad-slide-continues.html): dieci anni fa, quando era tutto professionale e impeccabile e si pensava solo ai Mac, [erano il quaranta percento di adesso](https://www.apple.com/newsroom/2007/10/22Apple-Reports-Fourth-Quarter-Results/).