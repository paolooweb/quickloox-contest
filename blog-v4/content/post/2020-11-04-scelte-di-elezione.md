---
title: "Scelte di elezione"
date: 2020-11-04
comments: true
tags: [Mac, Trump, Biden, Cnn, Surface, Florida, Texas, Ohio]
---
Scrivo mentre Florida, Texas e Ohio sono ancora lontani dall’essere assegnati e potrebbe vincere chiunque.

Dopo la [farsa dei Surface messi in mostra per compiacere lo sponsor](https://macintelligence.org/blog/2014/11/06/meglio-un-ipad/), tuttavia, quest’anno ho visto campeggiare su una scrivania degli studi Cnn un MacBook genuino, davvero usato dalla giornalista inquadrata.

Tanto basta a rendermi fiducioso sulla tenuta della democrazia americana.