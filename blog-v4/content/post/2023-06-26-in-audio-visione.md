---
title: "In audio-visione"
date: 2023-06-26T16:06:45+01:00
draft: false
toc: false
comments: true
categories: [Software, Internet]
tags: [A2, Filippo Strozzi, Marin, Strozzi, Roberto Marin]
---
Stasera più o meno dalle 21, si registra un’epica [puntata di A2 podcast dedicata a Vision Pro](https://www.youtube.com/watch?v=LyyfCF7EZ3M).

Epica soprattutto per me che non sono ancora riuscito a governare una scaletta decisamente immersiva.

Ci sarà da divertirsi grazie ai fantastici [Filippo e Roberto](https://www.avvocati-e-mac.it/a2-podcast) e io, *more solito*, punterò soprattutto al buffet.