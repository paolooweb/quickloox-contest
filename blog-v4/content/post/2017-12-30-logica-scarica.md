---
title: "Logica scarica"
date: 2017-12-30
comments: true
tags: [iPhone, batterie]
---
Una cosa è stata condotta molto male ed è la comunicazione in merito; tant’è vero che Apple ha scritto una [lettera aperta](https://www.apple.com/iphone-battery-and-performance/#ntf) per chiarire i dubbi sulla faccenda di iOS che rallenta gli iPhone con batterie esauste in situazioni di picco di consumi, per evitare spegnimenti indesiderati.<!--more-->

Si noti che tutta la vicenda viene descritta perfettamente nelle ultime diciotto parole.

Eppure si è alzato un polverone con pochi precedenti, caratterizzato da un costante disprezzo per il ragionamento logico.

Qualcuno ha ovviamente tirato fuori l’obsolescenza programmata. Io faccio in modo che il tuo telefono continui a funzionare affidabilmente anche se è consunto e questo, secondo te, dovrebbe costituire una spinta a cambiare modello. Invece, se il tuo telefono se ne va sul più bello perché il software non fa alcunché per impedirlo, no, questo non spinge a cambiare modello, tutt’altro.

Peggio ancora, la speculazione sulla batteria. Il telefono va più piano perché la batteria non consente più di farlo funzionare al massimo delle prestazioni, ma va. Continua a funzionare, invece di spegnersi, perdere dati, bloccarsi sul più bello. Questo – prolungare la vita utile di una batteria allo stremo – dovrebbe essere un incentivo subdolo a cambiare la batteria. Invece, lasciarla lì com’è, no.

Prolungare il funzionamento del telefono, sia pure senza i picchi di prestazioni, sarebbe introdurre obsolescenza programmata, ossia abbreviare la vita utile dell’apparecchio. Come possano stare insieme le due affermazioni proprio non arrivo a capirlo e spero che nel 2018 la qualità dell’aria migliori, perché qualcuno sta aspirando sostanze che alterano la percezione. E azzerano la logica.

Gli altri computer da polso, quando la batteria inizia a traballare, che fanno? Per ora si sa solo che Htc e Motorola [non fanno niente](https://www.theverge.com/circuitbreaker/2017/12/28/16825288/htc-motorola-dont-slow-processor-speeds-old-batteries-apple).

Cioè, se il telefono rischia di spegnersi all’improvviso, si spegne all’improvviso. Questo sarebbe, per molti, il metodo ideale da seguire.

Riguardo alle speculazioni sulla spinta a cambiare le batterie, Apple [abbatterà di due terzi il costo di sostituzione della batteria](https://9to5mac.com/2017/12/28/apple-temporarily-drops-price-of-iphone-battery-replacement-by-50-promises-ios-update-to-address-battery-health-in-new-apology/) per i modelli interessati dalla polemica, fino a fine 2018. Decine di milioni di telefoni la cui batteria non sarebbe stata normalmente sostituita, vedranno la sostituzione, prolungando la loro vita utile.

Immagino, con la nuova logica a zero consumo di meningi, che fare vivere più a lungo un telefono vecchio con una batteria nuova a prezzo minore sia obsolescenza programmata.