---
title: "Quasi lavoro"
date: 2016-10-22
comments: true
tags: [convention, Mac]
---
La foto è stata scattata durante la *convention* aziendale di una multinazionale, con centinaia di intervenuti e una sala regia impegnativa, come forse si può intuire dall’immagine (scattata in condizioni di luce impossibili, più di così non sono riuscito a fare).

A parte il Mac in primo piano, di un partecipante alla *convention* in condizioni di divertimento rilassato, garantisco che ce n’è un altro tra quelli inquadrati, in funzione a pieno carico presso la zona regia.

Questo per i professionisti dimenticati da Apple, nello spettacolo e nelle aziende.

 ![Alla convention aziendale](/images/convention.jpg  "Alla convention aziendale") 