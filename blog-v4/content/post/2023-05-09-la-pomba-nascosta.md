---
title: "La Pomba nascosta"
date: 2023-05-09T00:42:55+01:00
draft: false
toc: false
comments: true
categories: [Internet]
tags: [Enciclopedia Pomba, Book of Kells]
---
Mentre si discuteva – in termini interessanti – della [conservazione di Quake III Arena](https://macintelligence.org/posts/2023-05-02-software-a-lunga-conversazione/), rinvenivo nella casa del vecchio zio i due volumi di una edizione del 1929 dell’[Enciclopedia Pomba](https://www.maremagnum.com/en/libri-antichi/enciclopedia-pomba-per-le-famiglie-vol-i-e-ii/162439733/).

Per mia ignoranza, ero all’oscuro dell’esistenza dell’enciclopedia Pomba. I volumi risentono dei quasi cent’anni di vita e si sono conservati ottimamente nel complesso, mentre le rilegature e i bordi di qualche pagina avrebbero certamente bisogno di un intervento. Nell’improbabile ipotesi che l’enciclopedia Pomba ritorni in una libreria e rimanga lì per altri cent’anni, presumo che i due volumi si sfarinerebbero nelle mani dello scopritore, una volta giunta veramente a fine vita la carta.

Quanto durerà l’Enciclopedia Pomba? Capisco che doveva essere relativamente diffusa; se vale tipo venti euro presso un sito di libri usati, vuol dire che non è rara. Questo ne ha permesso la sopravvivenza di un numero di esemplari sufficiente a renderla ancora reperibile.

L’enciclopedia non è più in produzione, però, e chiaramente il numero delle copie esistenti continua a ridursi. A un certo punto, magari nel duemilacentoventinove, è molto probabile che non ne esista più una copia ancora consultabile.

Supponiamo che ne rimanga una, in cantina o in un magazzino di chissà chi, chissà dove. Mentre oggi posso sfogliare in libertà le pagine dell’enciclopedia Pomba, qualche mio bisnipote potrebbe trovarlo impossibile, a meno di non essere il fortunato in possesso dell’ultimo e unico esemplare.

In questa situazione, potremmo definire sopravvissuta la Pomba? Un documento esiste ancora anche se nessuno può vederlo? Il [Book of Kells](https://www.visittrinity.ie/book-of-kells/) è un *unicum*, però chiunque di noi [può ammirarlo se non altro in forma mediata](https://www.tcd.ie/library/news/book-of-kells-now-free-to-view-online/) senza doversi necessariamente recare in Irlanda.

Non sembra che l’enciclopedia Pomba riceverà lo stesso trattamento. A meno che in questo istante qualche meraviglioso folle stia fotografando e ridigitando i due volumi.

Il destino della Pomba dipende da quante copie ne sono state prodotte inizialmente e poi da quante ne vengono riprodotte nel tempo. Il primo valore è probabilmente alto, ma il secondo tende a zero e crea una situazione estremamente critica. Il destino del *Book of Kells* è partito malissimo, esistendo in una sola copia; tuttavia, se la biblioteca che lo ospita venisse vaporizzata da un asteroide, in giro per il mondo ci sarebbero copie digitali in numero tale da assicurare la sopravvivenza dei suoi contenuti.

Se devo scommettere, il Book of Kells vivrà più a lungo dell’enciclopedia Pomba. Per una ragione: il numero di sue copie, riproduzioni, digitalizzazioni cresce. La Pomba non se la fila più nessuno.

La chiave per la sopravvivenza di un contenuto nel tempo è la sua riproducibilità e su questo il digitale non ha rivali. La durata del supporto ha un ruolo, secondario però. Una buona politica di conservazione digitale ne tiene conto e ci sta attenta. Il formato ha un ruolo anche lui, ancora più secondario. Possiamo stare tranquilli che il *Book of Kells* ha dietro di sé curatori molto attenti alla leggibilità della digitalizzazione, non importa che formato sia di moda oggi.

Ah, io non sapevo dell’esistenza della Pomba. Quindi posso legittimamente chiedermi quante altre opere siano in corso di estinzione perché la loro carta si corrode lentamente e nessuno si preoccupa di crearne una edizione digitale.