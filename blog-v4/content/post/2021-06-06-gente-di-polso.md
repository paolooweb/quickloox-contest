---
title: "Gente di polso"
date: 2021-06-06T00:48:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [watch, Hodinkee, GQ, Benjamin Clymer] 
---
Un modo possibile di affrontare il cambiamento, [dalle pagine di GQ](https://www.gq.com/story/benjamin-clymer-june-column).

> Io, uno dei più in vista tra i sostenitori degli orologi meccanici, indosso un Apple Watch. Non me ne vanto né ci scrivo post per i social media, ma metto al polso il mio Apple Watch tre o quattro volte a settimana, il che ne fa uno dei pezzi più fidati di tutta la mia raccolta.

— Benjamin Clymer, fondatore di [Hodinkee](https://www.hodinkee.com)

Il dato può apparire singolare; eppure, nella posizione di Clymer, indossare lo stesso orologio tre volte a settimana è _tanta roba_.

La giusta dose di conservatorismo, la giusta apertura verso il nuovo, l’assenza di pregiudizi. Per Clymer, Watch è _un grande contributo all’orologeria_, che _stimola tutti gli operatori del settore a migliorarsi_.

Averne, di gente capace di restare fedele alla tradizione e al tempo stesso proiettarla nel presente.

*Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile.*