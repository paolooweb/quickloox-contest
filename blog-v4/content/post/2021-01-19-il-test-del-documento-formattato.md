---
title: "Il test del documento formattato"
date: 2021-01-19T01:57:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [Big Sur, WordPress, Pages, Html, ePub, Safari, Mac mini, iPad Pro, Mojave] 
---
Procedo con l’esperienza in Big Sur, che per il momento trovo neutra. Qualcosa forse era meglio prima, qualcosa forse è meglio adesso, almeno rispetto alle parti evidenti del sistema.

Sotto il cofano invece… uno dei lavori che svolgo prevede la pubblicazione di articoli su WordPress. Capita, anche nel 2021, che arrivi un file Pages (quantomeno Pages e non Word, già qualcosa) con tutta una serie di formattazioni interne e anche le foto.

Non esiste al mondo che io faccia editing dentro WordPress, quando ho a disposizione BBEdit. Quindi la prima cosa da fare è disporre del testo in formato testo, preferibilmente Html.

Il punto è che molte delle formattazioni presenti nel sorgente sono utili e, con sforzo minimo, sono già una parte di lavoro fatto. Se esporto da Pages posso tutt’al più ottenere un file ePub, che mi porta di parecchio fuori strada.

Quello che posso fare è selezionare tutto il testo su Pages e incollarlo sull’editor Visuale di WordPress (lo ammetto: WordPress serve di sicuro almeno a *una* cosa). Poi commuto la visione in quella di Testo e, direbbe il milanese imbruttito, *taaac!*, ho pronto il mio Html da riversare in BBEdit dove posso editarlo come si deve, con tutti gli strumenti che servono per fare prima, meglio e in fretta.

Selezionare tutto il testo in Pages, però, significa copiare e incollare in WordPress anche le immagini. Queste mi dimentico sempre di eliminarle prima di passare da Visuale a Testo; nel passaggio a Testo diventano codifica binaria lunga poco più delle dimensioni native dell’immagine. Se ci sono tre immagini nel testo, significa aggiungere tipo mezzo milione di caratteri all’articolo.

Arrivo finalmente al punto. Sul mio Mac mini, Safari ha sempre boccheggiato nel passare questo test. Ci mette del tempo ed è ragionevole, ma poi capita che si pianti oppure addirittura che si chiuda. Molte volte sono passato da questo workflow lavorando per scelta su iPad Pro, dove l’operazione ha più probabilità di riuscita.

Fino a ieri. Oggi Safari su Mac mini ha completato l’operazione nel giro di un paio di minuti e poi mi ha restituito il controllo con noncuranza. Nessun problema. Il file era sui duecentoquarantamila caratteri, certo non uno dei più grossi, ma perfettamente in grado di creare problemi nella mia esperienza.

Tutto ha funzionato. L’unica cosa differente rispetto a prima delle vacanze, nella mia configurazione, è che ora lavoro su Big Sur. Prima ero fermo a Mojave. In qualche modo, l’impalcatura sottostante Safari – quindi a un sacco di componenti del sistema – è più solida.