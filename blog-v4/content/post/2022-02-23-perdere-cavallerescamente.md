---
title: "Perdere cavallerescamente"
date: 2022-02-23T00:33:23+01:00
draft: false
toc: false
comments: true
categories: [Web]
tags: [Medioevo]
---
La preoccupazione per la conservazione digitale dei documenti. Domani potrebbero mancare i lettori di supporti, oppure sparire il software capace di leggere certi formati, se arriverà una catastrofe qualsiasi, se si spegnesse Internet, se una dittatura prendesse il comando della rete eccetera.

Uno studio pubblicato su *Science* stima che, dei manoscritti medievali dedicati alla cavalleria o alla tradizione eroica prodotti nei principali Paesi europei, [sia sopravvissuto il nove percento](https://arstechnica.com/science/2022/02/study-finds-90-percent-of-medieval-chivalric-and-heroic-manuscripts-have-been-lost/). Il resto dell’epica cavalleresca è stato perso nei secoli.

E sì che erano su carta, anzi, su pergamena. La pergamena è incredibilmente più resistente e durevole della carta.

E venivano anche copiati dagli amanuensi nelle abbazie; difficile che un tomo esistesse in unica copia.

Questo ci fornisce un eccellente *benchmark* sulle nostre aspettative di conservazione. Se usare il digitale ci facesse perdere in settecento anni il novanta percento dei contenuti cui teniamo, faremmo meglio (per un punto!) della civiltà medievale.

Su un numero di documenti incommensurabilmente superiore, a disposizione di un numero di persone astronomicamente maggiore. Se poi la mettiamo sui bit generati, là nel passato abbiamo testo illustrato; qui basta parlare di video e qualunque partita si chiude subito.

Il sottoscritto ha una passione per il Medioevo (che fu tutt’altro, in termini di civiltà e progresso, rispetto allo stereotipo pensato dagli ignoranti che aprono bocca per fare paragoni fuori luogo). Ma settecento o ottocento anni fa il digitale non c’era. Oggi c’è e rappresenta un progresso importante e decisivo per il trattamento dei documenti.

Compresa la loro conservazione.
