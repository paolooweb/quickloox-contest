---
title: "Dove non si vede"
date: 2016-08-11
comments: true
tags: [Flash, Chrome, Google]
---
La maggior parte dei difensori di Flash di Adobe parlano di video che sarebbe altrimenti impossibile da visionare o interfacce sviluppate in Flash che per questo motivo meriterebbero di sopravvivere invece che essere sostituite da Html5.

Evidentemente non sanno come viene usato Flash in rete. Grazie al cielo sempre meno e grazie all’iniziativa di Apple, Google (e perfino Microsoft, figurarsi). Google ha appena annunciato [un altro colpo quasi finale](https://chrome.googleblog.com/2016/08/flash-and-chrome.html) a Flash su Chrome e spiega come il problema sia meno visibile di quanto si creda:

>Oggi più del 90 percento di Flash sul web si carica dietro le quinte, per supportare attività come l’analisi del traffico. Questo impiego di Flash rallenta la navigazione e, a partire da settembre, Chrome 53 inizierà a bloccarlo. Html5 è molto più leggero e veloce, e ci pubblica pagine sta adottandolo per velocizzare il caricamento delle pagine e risparmiare energia della batteria. Su molti siti si vedrà un miglioramento in reattività ed efficienza.

Non è il colpo finale mi ci siamo sempre più vicini. Per una piaga che lavora anche e soprattutto dove non si vede.