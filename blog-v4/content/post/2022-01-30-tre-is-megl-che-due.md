---
title: "Tre is megl che due"
date: 2022-01-30T01:42:30+01:00
draft: false
toc: false
comments: true
categories:
tags:
---
Ecco, Apple si è accorta dei miglioramenti apportati alla *user experience* di questo blogghino e si è messa in testa di eliminare [Python 2](https://www.python.org/downloads/release/python-272/) a partire dal prossimo aggiornamento di Monterey.

[Non sarà una passeggiata.](https://macmule.com/2022/01/29/macos-monterey-12-3-will-remove-python-2-7-usr-bin-python/) Come avevo scritto a suo tempo, la cosa non rappresenta alcun problema in sé per le persone con un po’ di tecnica in tasca, mentre invece potrebbe creare inconvenienti non da poco a script e applicazioni varie che da Python 2 dipendono.

Lo avevo scritto perché Apple [aveva avvisato che sarebbe successo](https://developer.apple.com/documentation/macos-release-notes/macos-catalina-10_15-release-notes) e, oltretutto, che sarebbe successo anche con Ruby e [Perl](https://www.perl.org).

Per ora sparisce solo Python 2, che verrà sostituito da [Python 3](https://www.python.org/download/releases/3.0/) solo sui Mac dove effettivamente si crea un’esigenza e il sistema propone il download. Peraltro lo stesso articolo linkato all’inizio butta lì come battuta che Perl e Ruby potrebbero sparire nell’aggiornamento successivo…

Le dipendenze da Python 2 sono nulle in macOS, che funzionerà indisturbato, e frequenti in applicazioni e script indipendenti. È probabile che non in molti Mac, se non tutti, l’effetto dell’aggiornamento si farà sentire.

È una cosa positiva, in fondo, perché susciterà consapevolezza. Un domani, [Homebrew](https://brew.sh) – basato su [Ruby](https://www.ruby-lang.org/en/) – potrebbe bloccarsi per mancanza del linguaggio. Solo un esempio.

È probabile che Homebrew reagirebbe con una certa velocità fornendo un meccanismo alternativo di installazione, ma non tutte la applicazioni si comporterebbero con la medesima reattività. E a molti toccherà occuparsi di Terminale, compilazioni, script di shell e così via anche se non vogliono, a meno che abbiano la pazienza e la possibilità di attendere un qualche aggiornamento risolutore.

Che non è detto arrivi; Python 3 non ha come priorità la retrocompatibilità con Python 2 e installarlo potrebbe servire solo a occupare spazio su disco.

Ci aspettano tempi interessanti. Non dispiace mai.
