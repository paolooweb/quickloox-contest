---
title: "Pace e posta"
date: 2015-08-22
comments: true
tags: [Mailsmith, BBEdit, Unix, iCloudiPad, iPhone, Mutt, Alpine, sup, notmuch, Mail, Mac]
---
Ho cercato per molto tempo di spostare verso il Terminale certe attività, ma devo riconoscere che il punto di incontro migliore si ha in una fascia dove esiste ancora interfaccia grafica, di solito minimale, e il programma è sufficientemente versatile e potente per non fare rimpiangere Unix.<!--more-->

È il caso di [BBEdit](http://www.barebones.com/products/bbedit/), pressoché perfetto come editor di testo, comodissimo da usare dentro l’interfaccia grafica senza pagarle pegno.

Sto raggiungendo la stessa pace dei sensi informatica con [Mailsmith](http://www.mailsmith.org), pure da anni congelato nel suo sviluppo; eppure fa tutto quello che serve, maneggia basi di posta immense senza batter ciglio e ha tutta la comodità dell’interfaccia grafica senza averne il peso.

C’è una sola cosa che non fa: ritirare la posta di iCloud, cui… ho rinunciato. Per lo meno con Mailsmith; la posta iCloud arriva regolarmente su iPad e iPhone dove, essendo poca, viene agevolmente smaltita. Quando proprio non se ne può fare a meno, lancio Mail su Mac, dove è configurato unicamente per scaricare la posta iCloud e quindi è leggero e veloce.

È impossibile conoscere il futuro di Mailsmith e quindi sto tenendo d’occhio le soluzioni di posta via Terminale, come [Mutt](http://www.mutt.org) o [Alpine](http://www.washington.edu/alpine/) ma anche il sorprendente [sup](http://supmua.org), studiato per grandi volumi di posta, e l’ancora più sorprendente [notmuch](http://notmuchmail.org): il solo sistema di ricerca dentro la base dati della posta, da collegare ai sistemi di ricezione e spedizione che si pensano più opportuni.

Finirà magari un giorno che troverò la vera pace postale nel Terminale, comunque.
