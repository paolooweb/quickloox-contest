---
title: "A scuola di risparmio"
date: 2017-11-25
comments: true
tags: [iPad, Lynn, Boca, Raton, Distinguished, School]
---
A Boca Raton, Florida, Usa, la Lynn University [ha ottenuto il titolo di Apple Distinguished School 2017-2019](https://www.lynn.edu/news/2017/lynn-named-apple-distinguished-school). Il perché viene spiegato sul sito:

>Dal suo lancio nel 2013, il programma di apprendimento basato su iPad ha fatto risparmiare agli studenti fino al 90 percento dei costi per i libri di testo e contribuito ad alzare risultati e livello di coinvolgimento. L’ottantotto percento degli studenti di primo anno interpellati ha dichiarato che la tecnologia ha contribuito alla loro comprensione dei materiali di corso e l’ottantanove percento ha affermato che iPad li ha fatti collaborare meglio con i compagni.

Una cosa interessante del [programma](https://itunes.apple.com/us/book/a-whole-new-style-of-teaching-and-learning/id1296952013?ls=1&mt=11) è che, grazie all’uso di iPad, aree dell’ateneo prima destinate a laboratorio informatico sono state riconvertite ad altro, più variamente utile. Più tecnologia. o magari tecnologia migliore, significa liberare spazio dove prima stavano computer.

In Italia, ti dicono che non ci sono i soldi. Oppure che il programma non ha dato i risultati sperati. O che ci sono problemi con Office.

Qualcuno dovrebbe studiare di più e non sono i ragazzi. 