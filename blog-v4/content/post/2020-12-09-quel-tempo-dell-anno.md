---
title: "Quel tempo dell’anno"
date: 2020-12-09
comments: true
tags: [Advent, Code]
---
Il tempo dello scripting, naturalmente. I calendari dell’Avvento a tema informatico sono più vivi che mai e ne ho scovati diversi.

Quello che mi piace di più resta [Advent of Code](https://adventofcode.com):

>un calendario dell’Avvento di piccoli enigmi da programmazione per ambiti e capacità di livello variegato, risolvibili in qualsiasi linguaggio di programmazione a piacere […] Per partecipare non serve una preparazione informatica ma un pizzico di conoscenza della programmazione e qualche capacità di problem solving; chi li possiede arriverà lontano. […] Qualsiasi problema ha una soluzione che si completa al massimo in una quindicina di secondi su hardware di dieci anni fa, quindi non serve un computer particolare.

Un altro grande classico, per chi volesse alternative, è il [Perl Advent Calendar](http://www.perladvent.org/2020/). Abbiamo anche [JavaScript](https://scrimba.com/learn/adventcalendar), persino con premio in denaro.

Niente Lisp e niente Swift; d’altronde è l’Avvento 2020, non si può avere tutto. Qui manca anche il tempo, tanto che sono persino fermo con Swift Playgrounds. Chi risolva un problema da un calendario dell’avvento può pensarmi con affetto; lo invidio dal mio stato momentaneo di privazione del sonno.