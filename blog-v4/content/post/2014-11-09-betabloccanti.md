---
title: "Betabloccanti"
date: 2014-11-09
comments: true
tags: [Misterakko, Yosemite, OSX, VirtualBox, CharacterBuilder]
---
Nell’aggiungere i punti e virgola alla [guida a OS X Yosemite](http://www.apogeonline.com/libri/9788850317264/scheda) in collaborazione con l’amico [@misterakko](https://twitter.com/misterakko), la cosa più difficile è stata sopportare la lentezza e l‘instabilità delle installazioni di OS X 10.10 Developer Preview virtualizzate in [VirtualBox](https://www.virtualbox.org).<!--more-->

Dopo una breve illusione, l’idea di utilizzare le macchine virtuali per condurre esperimenti e verifiche è rapidamente naufragata.

Ora che [Yosemite](http://www.apple.com/it/osx/) è definitivo e alla vigilia dell’aggiornamento 10.10.1 (non vedo l’ora di vedere 10.10.10 per avere OS Ten Ten Ten Ten), sono comunque sollevato nel constatare che il meccanismo è nettamente migliorato e, sia pure lento, usabile per operazioni mediamente semplici. A questo punto mi serve solo per usare [Character Builder](http://dnd.wizards.com/digital_tools) senza installare software Microsoft sul mio sistema di lavoro.

Comunque sia potrà tornare utile ad altri. Inoltre l’esperienza mostra quanto sia diversa una copia preliminare di OS X da quella finale: tra codice ancora da rifinire e codice di controllo, che rallenta anche notevolmente le prestazioni, una versione beta di OS X richiede ancora una quantità importante di lavoro. Figuriamoci lo sviluppo integrale del sistema dalla prima all'ultima versione. Il numero di *build* 14A389 significa che OS X è stato compilato integralmente quasi quattrocento volte prima di diventare definitivo. Alcuni *bug* che passano ugualmente i controlli; possiamo solo immaginare quanti siano quelli che vengono bloccati e neutralizzati durante ogni fase dello sviluppo.