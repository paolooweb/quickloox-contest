---
title: "Uno, due, tre"
date: 2017-01-14
comments: true
tags: [iPhone, Asymco, Dediu]
---
Dicevo [giusto ieri](https://macintelligence.org/posts/2017-01-13-dieci-anni-e-non-sentirli/) che il decennale di iPhone, sì, è una gran cosa, simpatizzo e ho pure [rievocato su Apogeonline](http://www.apogeonline.com/webzine/2017/01/12/presentazione-dieci-piu) la più straordinaria presentazione di prodotto di sempre.

Dopo di che, non mi soffermerei più di tanto.

Per esempio, è recentissimo il pezzo di Horace Dediu di Asymco su [iPhone prodotto più venduto di tutti i tempi](http://www.asymco.com/2017/01/11/the-first-trillion-dollars-is-always-the-hardest/).

Nel solo 2017, iPhone avrà contribuito a generare fatturato complessivo – tutto ma proprio tutto compreso, hardware, software, servizi, app, accessori – per *mille miliardi di dollari*. Un trilione di dollari.

In una qualche data del 2018, l’ecosistema complessivo di iOS toccherà i due miliardi di unità vendute, una notevolissima percentuale delle quali in funzione.

Più del decennale mi fanno più impressione queste analisi. E la mia curiosità va irresistibilmente a, un trilione, due miliardi, quale *tre* ci capiterà di vedere.