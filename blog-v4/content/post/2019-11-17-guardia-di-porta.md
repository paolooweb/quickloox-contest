---
title: "Guardia di porta"
date: 2019-11-17
comments: true
tags: [Prompt, Connection, Keeper, Drang, iPad, Ghostbusters, ssh, Mac]
---
Quasi tutte le sere, a letto con iPad in mano, apro una connessione ssh con Mac dello studio e sbrigo qualche ultima attività Prima del sonno.

Lo faccio con [Prompt di Panic](https://panic.com/prompt/) e ho sempre sofferto la difficoltà del programma di tenere attiva la connessione in background, mentre lavoro su un’altra app.

A volte la app su cui lavoro è quella giusta e mi arriva un avviso: torna per un attimo su Prompt in modo da tenere attiva la connessione. A volte non faccio in tempo, a volte mi sfugge, a volte la app è quella sbagliata e non arrivano avvisi. La connessione si perde e quando torno in Prompt sperimento vari gradi di frustrazione, secondo ciò facevo e la sua dipendenza dalla connessione continua.

Finalmente qualcosa è cambiato. Nelle Impostazioni di Prompt, accessibili dal pulsante con l’icona da ingranaggio, è comparso un *Connection Keeper* che promette di tenere uno storico delle connessioni, utile per consultazione o per copiare qualcosa di già fatto, e *mantenere attiva la connessione anche in background*.

Lo collaudo stasera. Ma intanto Dr. Drang [scrive che funziona](https://leancrew.com/all-this/2019/11/prompt-forever/) e, se lo scrive lui, fiducia (quasi) cieca. Drang aggiunge anche una frasetta interessante:

>Un passo in più verso il rendere iPad un computer a tutto tondo.

A dire che magari non è ancora un computer completo… ma è nondimeno un computer.

Riguardo al titolo, in ambito informatico una connessione avviene sempre attraverso una porta logica, quindi era inevitabile la reminiscenza di [Ghostbusters](https://www.youtube.com/watch?v=_SuO6lS3yvU).

<iframe width="560" height="315" src="https://www.youtube.com/embed/_SuO6lS3yvU" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
