---
title: "Ubiquo contro versatile"
date: 2016-07-23
comments: true
tags: [vi, emacs]
---
*Opensource.com* ha lanciato un [referendum su vi ed emacs](https://opensource.com/life/16/7/vim-or-emacs-which-text-editor-do-you-prefer) anacronistico solo fino a un certo punto.

Certo, con le interfacce e i sistemi attuali, nel 99 percento dei casi è più conveniente optare per un programma bene integrato e con una efficace interfaccia grafica. Su Mac potrebbe significare [BBEdit](http://www.barebones.com/products/bbedit/), [Sublime Text](https://www.sublimetext.com/), [Atom](https://atom.io/).

Però confrontare [vi](http://www.vim.org/) ed [emacs](https://www.gnu.org/software/emacs/) è pesare diversi approcci alla produttività abituale, quella talmente familiare che è diventata automatica, e anche considerare l’esistenza di computer diversi da quelli convenzionali. Un senso quindi si trova. Per dire, su iPad [avere vi](https://appsto.re/it/iuyxD.i) è banale; [avere emacs](https://appsto.re/it/DbMH4.i) comporta vincoli da sopportare come la presenza obbligatoria di una tastiera fisica.

Numericamente, sta vincendo di netto vi, per una ragione di ubiquità: anche i sistemi più sdruciti hanno vi a bordo o consentono di averlo facilmente. Però emacs esce come vincitore morale, a leggere i commenti, perché tipicamente chi usa emacs fa molto più che utilizzare un editor di testo: magari programma direttamente in emacs, magari gioca, magari produce formati particolarmente esoterici di testo. Tutte cose dove vi arriva meno o non arriva proprio.

Se domani realizzassi la fantastica folla di lavorare per il novanta percento del tempo nel Terminale, certamente sceglierei emacs proprio per la sua immensa versatilità. Pesa anche la scelta di campo generale di Apple, che ha incluso in macOS molti comandi essenziali di emacs. Per dire, Control-t per scambiare di posto due caratteri funziona praticamente in ogni situazione su Mac e lo stesso BBEdit contiene una preferenza per abilitare le combinazioni di tasti di emacs.

Se poi qualcuno capace riuscisse a fare girare decentemente emacs su iOS anche senza obbligo di tastiera esterna, la fantastica follia acquisterebbe in fantastico e perderebbe in folle.

*[È tradizionalmente difficile spiegare a un non addetto che la fatica di familiarizzare con emacs, o con un editor di testo progredito, potrebbe portare grandi vantaggi di produttività e velocità. È una scommessa che Cuore di Mela affronterebbe volentieri, in forma semplice da capire e completa a sufficienza per arrivare al confine con la maestria. Chi avesse questa lungimiranza è libero di contribuire al’iniziativa.]*
