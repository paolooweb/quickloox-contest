---
title: "Servizi di sicurezza"
date: 2021-03-14T02:02:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [Apple, Vpn, Mail] 
---
A metà weekend, per un complesso di ragioni, mi trovo completamente solidale con chi chiede ad Apple, in nome dell’impegno sulla privacy, una edizione di Mail che liberi dai [pixel-spia](https://www.bbc.com/news/technology-56071437) e pure un [servizio di VPN](https://www.fastcompany.com/90369909/heres-the-next-big-step-apple-should-take-to-protect-our-privacy) per la navigazione più riservata.

Capisco perfettamente la pubblicità su Internet e quella fatta bene è perfino utile. Però deve vigere un accordo esplicito tra consumatore e inserzionista: ti concedo uno spazio ben preciso e tu lo rispetti.

*Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile.*