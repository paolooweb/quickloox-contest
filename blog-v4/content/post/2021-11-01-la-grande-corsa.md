---
title: "La grande corsa"
date: 2021-11-01T02:31:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [Super Mario] 
---
Chi dice che i videogiochi non siano uno sport, non sa che cosa dice.

In clima di vacanza, mi sono dedicato a più futilità del solito e mi sono imbattuto in questo [video che sovrappone cinquemilacentosessantadue tentativi di arrivare in fondo a Super Mario Bros](https://www.youtube.com/watch?v=X_eXSzyZudM), per centonovantatré ore di gioco.

Teniamo presente che quelli bravi, arrivati in fondo, hanno impiegato poco più di cinque minuti; il video dura appena di più, con un po’ di (interessanti) statistiche riassuntive alla fine.

Per essere una clip di cinque minuti dedicata a Super Mario, l’ho trovata incredibilmente evocativa. Biologia, fantascienza, metafore, analogie, a parte il puro piacere visivo di osservare il flusso dei tentativi di approdare alla schermata successiva.

La storia raccontata da questi cinquemila aspiranti racconta molto più del gioco o dei loro risultati. Esattamente come lo sport.

<iframe width="560" height="315" src="https://www.youtube.com/embed/X_eXSzyZudM" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

*Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile.*