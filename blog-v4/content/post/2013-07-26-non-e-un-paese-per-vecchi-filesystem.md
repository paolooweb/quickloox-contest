---
title: "Non è un paese per vecchi filesystem"
date: 2013-07-26
comments: true
tags: [Finder, OSX]
---
Matthew Panzarino ha scritto una serie di cose interessanti a proposito di [OS X, iCloud e l’ascesa del filesystem populista](http://thenextweb.com/apple/2013/06/28/os-x-mavericks-icloud-and-the-rise-of-the-populist-file-system/).<!--more-->

La sintesi estrema del suo ragionamento è che, tra il nuovo Finder con i *tag* che debutta in OS X Mavericks, e iCloud che cresce, si finirà un po’ per tutti come sta già andando su iOS: file, cartelle e relativa gestione saranno una cosa del passato o, al massimo, per qualche specialista, professionista, appassionato eccetera. Ha citato anche un [video di Steve Jobs del 2005](https://www.youtube.com/watch?v=xES5-qDv-4Q):

>Alla fine, l’amministrazione del filesystem finirà per essere una app per pro, e i consumatori non avranno bisogno di usarla.

<iframe width="420" height="315" src="//www.youtube.com/embed/xES5-qDv-4Q" frameborder="0" allowfullscreen></iframe>

Posso solo essere d’accordo: il mio uso del Finder è da anni limitato all’avere cartelle specifiche per argomenti specifici. Qualcosa che, con le cartelle smart da molto tempo e con il Finder a tag di adesso, è possibile realizzare in larga misura automaticamente: *mettere in ordine* i file nelle cartelle del Finder è da molto tempo un innocente e rilassante passatempo per chi lo gradisce e una necessità per nessuno.

Mi si parla dei *power user*, gli *utenti evoluti*, quelli che vogliono avere a disposizione strumenti sofisticati di gestione dei file e delle cartelle.

Mi perdonino: un utente evoluto non ne ha assolutamente bisogno. File e cartelle, l’intera metafora della scrivania, sono nati per *facilitare* l’uso del computer. Un *utente evoluto* non ha problemi ad andare sul difficile. Terminale, comando `ls` (per cominciare) e si fanno montagne di cose che l’interfaccia grafica non conosce né conoscerà mai.

Siamo condizionati da tanti anni di esperienza. Il prossimo passo verso avere computer ancora più semplici per tutti non è mettere a punto un Finder complesso. È renderlo inutile. Dopo di che, se uno è *pro*, fa miracoli con il Terminale (e al limite [Quicksilver](http://qsapp.com), [Keyboard Maestro](http://www.keyboardmaestro.com/main/) e compagnia).