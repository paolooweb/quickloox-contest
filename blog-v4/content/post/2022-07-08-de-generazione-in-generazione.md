---
title: "De generazione in generazione"
date: 2022-07-08T00:44:58+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [Slack]
---
Ricevo un messaggio su [Slack](https://slack.com): *Ciao, mi puoi girare il tuo numero di telefono che ci sentiamo per la questione?*

Sono davanti a Slack e, [come scrivevo](https://macintelligence.org/posts/2022-06-30-schermi-mentali/), ho un pulsante di chiamata a disposizione immediata. Lo uso, ma non c’è risposta.

Arriva più tardi: *Le chiamate di Slack non mi funzionano molto bene. Ci sentiamo per telefono tra una mezzoretta?*

Rispondo: *Va bene! Chiamami tu per favore. Grazie!*

Più tardi ancora arriva questo messaggio su Slack: *Scusa ma ho il telefono su cui aspetto una chiamata, possiamo fare su Slack?*

Ci parliamo a voce attraverso Slack per sedici minuti. Funzionamento perfetto.

Nel tempo del minuetto avremmo potuto risolvere in metà del tempo, in chat, e (volendo) passare l’altra metà a salutarci a voce con gusto e soddisfazione.

Avremmo anche potuto sentirci a voce immediatamente su Slack e ci sarebbero voluti comunque sedici minuti, ma due ore prima.

Però bisogna opporre resistenza, per nessuna ragione diversa dalla forza dell’abitudine. Invece di dire che Slack fa schifo o che uno adora la chiamata cellulare, *la chiamata Slack non funziona bene*.

Questi sono i figli della generazione *non ho spazio sul telefono*. E i nipoti della generazione *non c’è campo*. I cui bisnonni appartenevano alla generazione *l’email non è arrivata*. Parenti alla lontanissima degli *in questo momento non è in ufficio*.