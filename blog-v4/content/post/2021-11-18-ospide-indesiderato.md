---
title: "OSPIDe indesiderato"
date: 2021-11-18T00:10:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [Spid, PosteID, PagoPA] 
---
Di punto in bianco, l’altra sera la app di PosteID si è rifiutata di autenticarmi con Spid sul servizio PagoPA cui la scuola ha demandato la raccolta delle quote per la gita scolastica della primogenita. Ovvero niente di che, a parte il fatto che ritrovarsi senza la propria identità digitale può davvero diventare fastidioso, per non dire peggio.

La app reagiva a qualunque tentativo di accesso con il messaggio *si è verificato un errore*. Dal sito l’autenticazione per nome e password risultava scorretta; dopo tre login non riusciti, Poste blocca l’account per mezz’ora e si intuisce come cercare di risolvere un problema di password sotto queste condizioni non sia da augurare ad alcuno.

Dopo una cinquantina di brutti quarti d’ora, il problema si è risolto in modo abbastanza misterioso; all’ennesimo tentativo di login, la app – invece che lamentare un errore generico – ha dichiarato che la password era sbagliata e mi ha dato modo di cambiarla. Da lì è stata tutta discesa.

In mezzo, però ci sono state alcune telefonate poco ispirate con il servizio di assistenza di Poste. Il primo operatore mi ha consigliato di provare a effettuare il login *con un altro motore di ricerca*, intendendo di cambiare browser; la seconda, gentilissima, ha detto che avrebbe dovuto cancellare *i certificati* e che questo forse avrebbe risolto il problema. Naturalmente, non è accaduto. I tempi di attesa per parlare con qualcuno sono davvero stancanti e il servizio chiude alle otto di sera. Ricordo che si parla dell’identità digitale; è un po’ come se chiudessero le caserme dei carabinieri. Almeno un canale di emergenza dovrebbe restare accessibile.

Man mano si fa strada la verità: ai tempi, ho sottovalutato Spid e ho scelto la strada più veloce per togliermi il fastidio. Ora che effettivamente ha una utilità diffusa, capisco che avrei dovuto scegliere un operatore più qualificato, non importa a che livello di complessità per registrarsi o di costo. Su PosteID, pare che il problema di restare senza identità digitale importi assai relativamente a chi gestisce il servizio. La cosa non depone bene per il giorno in cui dovessi fare qualcosa di più importante che pagare pochi euro di quota per una gita.

_Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile._