---
title: "Il dono delle lingue"
date: 2015-02-03
comments: true
tags: [Terminale, lingua, languagesetup, MacOSXHints, SuperUser, AppleScript]
---
Stranota è o dovrebbe essere la possibilità su OS X di cambiare lingua di sistema tra un’applicazione e l’altra e così, per dire, usare il Finder in inglese, Pages in italiano, TextEdit in francese, Calendario in tedesco e così via.<!--more-->

Cambiare lingua richiede tuttavia il passaggio dalle Preferenze di Sistema, sezione Lingua e Regione. Si può fare in modo molto più veloce, perché qualsiasi operazione abilitata nell’interfaccia grafica corrisponde a un qualche comando di Terminale.

Non è stato difficile trovare le risposte. Un vecchio [Mac OS X Hint tuttora valido](http://hints.macworld.com/article.php?story=20061229203902170) descrive il comando

`defaults write NSGlobalDomain AppleLanguages "(it, en, ja, fr, de, es, nl)"

che imposta la sequenza delle lingue in Preferenze di Sistema a – nell’ordine – italiano, inglese, giapponese, francese, tedesco, spagnolo, olandese.

Si possono aggiungere lingue, toglierle tutte tranne una, tenerne solo due o tre eccetera.

Questo cambia la lingua di tutto il sistema e presume che lanceremo l’applicazione da fare funzionare con una lingua diversa da quella di sistema, per poi riapplicare il comando stesso e tornare all’impostazione di lingua precedente (l’applicazione, fino a che resta aperta, continuerà a usare la lingua con cui è partita).

Ma si potrebbe anche impostare una applicazione in modo che sia lei ad aprirsi sempre e comunque, per dire, in tedesco. Bisogna modificare il suo file di preferenze, come in

`defaults write com.apple.mail AppleLanguages '("de")'

Di programma in programma cambiano solo il nome del file di preferenze e ovviamente l’elenco delle lingue.

Attenzione a non lasciare il sistema senza una lingua preimpostata, cosa che potrebbe creare problemi. In caso di emergenza, dare

`defaults write -g AppleLanguages -array it en

L’esempio mostra due lingue per mostrare che vanno separate dallo spazio. Le lingue possono essere due, tre, mille, una.

Su SuperUser ho letto [come cambiare solo occasionalmente la lingua usata da una applicazione](http://superuser.com/questions/643074/change-the-osx-system-language-in-a-script): con un comando che lancia l’applicazione medesima e indica la lingua desiderata. Così:

`/Applications/Calendar.app/Contents/MacOS/Calendar -AppleLanguages '(it)'

Di volta in volta bisogna cambiare la lingua – indicarne più di una è sostanzialmente inutile – più il nome dell’icona dell’applicazione e il nome dell’eseguibile che si nasconde dentro il *bundle* dell’applicazione stessa (qui *Calendar-app* e *Calendar*).

Esiste persino un comando `languagesetup`, che tuttavia ho trovato piuttosto criptico e meno comprensibile degli esempi qui sopra.

Passare dal Terminale in questo caso non è fine a se stesso; questi comandi possono diventare uno *script* da chiamare al volo, pezzi da inserire in un AppleScript, servizi cui assegnare una scorciatoia di tastiera e così via, alla fine creando una scorciatoia verso il cambio della lingua di sistema (o dell’applicazione) ben più comoda e veloce che rivolgersi ogni volta alle Preferenze di Sistema.