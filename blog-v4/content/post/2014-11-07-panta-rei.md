---
title: "Panta rei"
date: 2014-11-07
comments: true
tags: [Battiato, Apple//c, Mac, Idc, Macintosh, keynote, Macworld, iWorld]
---
*Non ci si può bagnare due volte nello stesso fiume / né prevedere i cambiamenti di costume.*<br />
*(Franco Battiato)*

Molti si lamentarono anni fa quando Apple iniziò a disertare lo Smau italiano e soprattutto la fiera Macworld. Adesso [Macworld 2015 è stato messo assieme alla consorella iWorld *on hiatus*](http://www.tuaw.com/2014/10/14/r-i-p-macworld-iworld/), in pausa, eufemismo gentile per dire che non si terrà più, salvo eventi catartici che all’orizzonte proprio non si vedono.<!--more-->

Pareva che Mac fosse negletto e a Cupertino si occupassero sono di iPhone e compagnia. I nuovi dati Idc – sempre da prendere con un grosso grano di sale – dicono che [Mac è il terzo computer negli Stati Uniti](http://recode.net/2014/11/06/mac-hits-market-share-milestone-in-u-s-what-post-pc-era/), con una quota di mercato del 13 percento. Probabilmente non l’ha avuta nemmeno il primo Macintosh del 1984.

A proposito di 1984, più che di Macintosh, niente mostra quanto siano cambiati i tempi quanto la [presentazione italiana di Apple //c](https://www.youtube.com/watch?v=S4o4dxqt2NA&feature=youtu.be), maggio di quell’anno. Da mostrare a chi non gradisce gli attuali *keynote*. Guarisce subito.

Non è tanto che i tempi siano cambiati. Dobbiamo piuttosto ricordarci che continuano a cambiare. Se questo settore ha un lato bello, è esattamente l’impossibilità di adagiarsi nell’abitudine.

<iframe width="420" height="315" src="//www.youtube.com/embed/S4o4dxqt2NA" frameborder="0" allowfullscreen></iframe>