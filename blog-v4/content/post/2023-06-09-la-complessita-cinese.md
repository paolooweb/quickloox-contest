---
title: "La complessità cinese"
date: 2023-06-09T01:31:35+01:00
draft: false
toc: false
comments: true
categories: [Internet]
tags: [Mailmasterc, briand06, Douglas Hofstadter, John Searle, Searle, Hofstadter, intelligenza artificiale, AI, L’io della mente, The Mind’s I, Daniel Dennett, Dennett]
---
Primo aggiornamento al tema della diversità di opinione tra John Searle e Douglas Hofstadter sull’argomento della [stanza cinese](https://plato.stanford.edu/entries/chinese-room/), [sollevato da **Mailmasterc**](https://macintelligence.org/posts/2023-06-05-ricerche-e-sviluppi/).

Grande mossa di [briando6](https://melabit.wordpress.com) che ha individuato il riferimento esatto: l’articolo originale di Searle e le riflessioni conseguenti di Hofstadter si trovano ne [L’io della mente](https://www.adelphi.it/libro/9788845906329).

Ora però ci devo studiare sopra. L’analisi di Searle è molto articolata e va seguita con attenzione. La risposta di Hofstadter pure, è lunga e in certi passaggi molto sottile nel ragionamento.

Mi sentirei di produrre una prima anticipazione estremamente rozza: Searle attribuisce la sede della conoscenza del sistema al signore che passa i bigliettini, mentre per Hofstadter la conoscenza viene amministrata da tutto il sistema. In altre parole, i fogli con le norme di trattamento degli ideogrammi fanno parte della conoscenza e sono anzi il fattore cruciale, a partire dal fatto che nella pratica un umano non avrebbe tempo materiale di utilizzare un sistema del genere, posto che lo si riesca a scrivere. Punto su cui Searle glissa.

Potrei sbagliare clamorosamente e quindi aprirò una parentesi di lettura attenta. Tutte le descrizioni semplici della stanza cinese… non sono buone descrizioni. Oppure non sono semplici.