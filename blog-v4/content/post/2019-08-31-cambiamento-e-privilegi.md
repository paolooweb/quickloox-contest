---
title: "Cambiamento e privilegi"
date: 2019-08-31
comments: true
tags: [Villani, FileMaker]
---
Ho avuto il privilegio di leggere **Giulio** a proposito della recente decisione di FileMaker Inc. di [rispolverare il nome Claris e annunciare una strategia tutta nuova](https://macintelligence.org/posts/2019-08-08-un-nome-nessuna-garanzia-ma-una-intuizione/).

§§§

È la prima volta a mia memoria che Apple acquisisce qualcosa e la passa a FileMaker (normalmente sempre trattata come un oggetto separato, sia come risorse che come fatturato).

FileMaker sta valutando la possibilità di eseguire codice Javascript direttamente (e supporta direttamente JSON da due versioni).

FileMaker ha cambiato modello di business anni fa, adesso sta cambiando anche offerta di prodotto: non solo Claris Connect ma anche una offerta di cloud molto più specifica, ottimizzata e ingegnerizzata, seppur basata comunque su istanze Amazon Web Services. E Claris Connect opererà solo su cloud, e FileMaker sta spingendo molto a livello di spostare su cloud tutto quanto.

Non mi stupirei quindi se in quattro-cinque anni FileMaker Pro come prodotto fosse molto ridimensionato (o comunque non evoluto) ma che Claris si trasformasse in una sorta di braccio armato di Apple per i servizi generici. In fondo, il cloud è totalmente apiattaforma (quindi anche per Windows e Android, nonché la fine delle guerre di religione) e FileMaker come ambiente di sviluppo professionale deve evolversi in qualcosa di diverso, o morire.

§§§

Giulio non è l’ultimo arrivato nel mondo FileMaker e anzi è uno dei primissimi per bravura, intraprendenza, competenze. Sempre parlando di privilegi ho potuto [vivere una giornata a sentirlo parlare di FileMaker](https://macintelligence.org/posts/2017-01-16-guru-per-un-giorno/) e rimane una di quelle migliori di sempre nel mio conteggio professionale.

FileMaker deve evolversi o morire, dice. Vale per qualsiasi software e gli sviluppatori FileMaker, che sono complessivamente più intelligenti della media, capiranno il messaggio. A testimonianza della bontà della loro scelta hanno avuto una quantità di anni di soli progressi incrementali enorme, per una piattaforma software, e anche questo è stato un privilegio. Ma neanche FileMaker può durare uguale a se stesso per sempre in un mercato dove le condizioni a contorno continuano a mutare. Anche potersi preparare per tempo a un cambiamento importante è un privilegio.