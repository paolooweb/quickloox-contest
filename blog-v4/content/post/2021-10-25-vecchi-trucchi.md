---
title: "Vecchi trucchi"
date: 2021-10-25T02:32:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [Jean-Louis Gassée, Monday Note, Steven Sinofsky, M1, M1 Pro, M1 Max, Intel, Alper Lake, Tsmc, x86, Microsoft, Windows] 
---
Jean-Louis Gassée ha perfettamente inquadrato la situazione all’indomani della presentazione dei nuovi processori M1 nel suo commentario su [Monday Note](https://mondaynote.com/apple-m1-pro-max-surprises-7b097788160b).

>La fazione Intel del nostro villaggio ha archiviato M1 Pro e Max come notevoli, ma non proprio una minaccia: “Certo, Apple è in vantaggio sui tempi per via del loro accesso alla tecnologia di fabbricazione di circuiti da cinque nanometri di Tmsc, ma una volta che Intel ci arriva, i chip x86 supereranno Apple Silicon, specialmente grazie al loro accesso alla vasta libreria di software per Windows”.

È tutto già successo. Il paragrafo successivo è gustosissimo, con i riferimenti alla reazione verso la presentazione del primo processore a 64 bit di Apple, anno 2013 (iPhone 5). Reazione identica. Il titolo del pezzo di Gassée era [64 bit. Niente, non ti servono. E li avremo entro sei mesi](https://mondaynote.com/64-bits-it-s-nothing-you-don-t-need-it-and-we-ll-have-it-in-6-months-1d394641e97a).

Gassée segnala un [riassunto definitivo](https://medium.learningbyshipping.com/apples-long-journey-to-the-m1-pro-chip-250309905358) scritto da Steven Sinofsky nell’ambito di un articolo molto più lungo:

>Si è tentati di guardare a M1 Pro/Max in termini di prestazioni, mentre prestazioni per watt PIÙ grafica integrata PIÙ memoria integrata PIÙ processori dedicati integrati significa innovazione in una direzione interamente differente. È solo l’inizio.

C’è molto di più su *Monday Note* ed è veramente tutto da leggere. Solo una nota per chi non conosca la figura di Sinofsky:

>Prima di lasciare Microsoft dopo oltre due decenni, è stato President per l’area Windows. In questa carica, Sinofsky ha seguito da vicinissimo le vicissitudini dello sviluppo x86 di Intel e le sfide create dal supporto dei sistemi operativi. In altre parole, un punto di vista vantaggiosamente esclusivo dal quale valutare Apple Silicon.

Come dire: domani il *cugggino* parlerà entusiasta del chip Intel che immancabilmente arriverà tra sei mesi e batterà Apple Silicon. Basta tenere presente che sono trucchi di lunga data, un po’ logori. Un Sinofsky la sa ben più lunga.

*Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile.*