---
title: "Non si parla di politica"
date: 2022-09-01T01:47:34+01:00
draft: false
toc: false
comments: true
categories: [Internet]
tags: [Steve Jobs, Jobs, scuola, metropolitana, Roma, Palermo, Milano, Torino, Valencia]
---
Una città da ottocentomila abitanti, se fosse in Italia, sarebbe [la quinta più grande](https://www.statista.com/statistics/589331/largest-cities-in-italy-by-population/), dopo Torino e prima di Palermo.

Una città con dieci linee metropolitane competerebbe a testa alta anche con Milano e Roma, per non parlare di Torino o di [qualsiasi altra città italiana](https://mapa-metro.com/en/Italy/).

Ho scoperto che, nelle domeniche estive, le suddette dieci linee metropolitane sono ad accesso gratuito.

Durante la pigra fruizione delle linee in questione mi sono imbattuto in un notevole [intervento di Steve Jobs sul sistema scolastico americano](https://www.youtube.com/watch?v=Tuw8hxrFBH8). Non dimentichiamo che Jobs non è stato un grande imprenditore con tempo a sufficienza per parlare di cose fuori dalla sua portata: si è messo a fabbricare computer più che lavagne, ma Apple ha avuto sempre una attenzione altissima per il mondo scolastico e tuttora vi detiene una autorevolezza e una diffusione importanti. Jobs dedicava tempo alla scuola.

Durante la visione di parte del discorso di Jobs sono arrivato al museo delle scienze della città al centro di questo *post*. Il museo ha una capienza dichiarata di diecimiladuecentotré visitatori.

Tutto ciò, considerato che in Italia inizio delle scuole e elezioni politiche avvengono questo mese, potrebbe indurre a considerazioni politiche sui programmi dei partiti e sulle capacità dimostrate dai loro parlamentari, amministratori locali, tecnici e *spin doctor*.

Mi guardo bene dal farle.

<iframe width="560" height="315" src="https://www.youtube.com/embed/Tuw8hxrFBH8" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>