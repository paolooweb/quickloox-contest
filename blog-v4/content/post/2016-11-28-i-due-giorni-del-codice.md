---
title: "I due giorni del codice"
date: 2016-11-28
comments: true
tags: [Codemotion, Unity, Steam]
---
Mi sono trovato nella singolare situazione di essere giurato a [Codemotion](http://milan2016.codemotionworld.com) per scegliere il miglior gioco indipendente tra quelli in mostra.

Più dei giochi, tutti con scintille creative meritevoli sempre almeno sotto un aspetto particolare (la grafica, il concetto, la giocabilità, la distribuzione eccetera, sempre almeno uno), è stata un’esperienza conoscere le comunità di chi li sviluppa e cerca un posto al sole. Un sacco di ragazzi e ragazze giovani, qualcuno già con lo sponsor e qualcuno in cerca, qualcuno con un lavoro convenzionale preso con l’unico scopo di consentirsi lo sviluppo del gioco la sera e nei weekend.

Due parole che ho sentito ricorrere molto spesso: [Unity](https://unity3d.com) e [Steam](http://store.steampowered.com/?l=italian). Il motore fisico e quello di distribuzione. E poi tanto sviluppo per iOS e Android, oltre a quello per computer e *console*.

Nei prossimi giorni segnalerò qualcosa da ricordare, almeno come progetto; molti dei giochi erano ancora in fase di sviluppo.

Ho visto anche diversi Mac. Uno di questi era usato in diretta per proseguire lo sviluppo di un gioco in concorso. Posso assicurare che lo sviluppo di videogiochi è una attività assolutamente professionale.

 ![Un Mac per sviluppare giochi](/images/gamedev.jpg  "Un Mac per sviluppare giochi") 

Di più. A Codemotion c’era un tabellone pieno di offerte di lavoro vero da parte di vere aziende. E poi un’area startup, con aziende appena nate in cerca di talenti da assumere. In primo piano, Mac.

 ![Startup in cerca di professionisti](/images/startup.jpg  "Startup in cerca di professionisti") 