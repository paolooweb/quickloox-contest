---
title: "Vacanze in West Coast"
date: 2021-01-24T00:42:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [Liguria, All About Apple Museum, Savona, West Coast, Savona] 
---
Che la Liguria abbia grande potenziale turistico certo lo si sapeva anche prima di oggi.

Senza offesa per nessuno, tuttavia, è relativamente da poco tempo che si fa una promozione pubblicitaria capace di lasciare un segno comunicativo. Varrebbe anche per altre regioni.

Quest’estate sarebbe stupendo avere tempo e possibilità di passare dei giorni di vacanza in libertà e parlo di quella dello spirito, prima ancora delle mascherine o delle condizioni economiche.

L’idea di definire la Liguria [la nostra West Coast](https://italianwestcoast.it) è fresca e ben realizzata. Mi avessero detto di guardare un quarto d’ora di promozione pubblicitaria nel browser, mi sarebbe scappato da ridere. L’ho fatto invece e mi sono arreso solo dopo Alassio (ci sono diversi segmenti ciascuno dedicato a una delle città principali).

Ma sono passato da Savona e ho trovato gli amici, sempre, dell’[All About Apple Museum](https://www.allaboutapple.com). Che per forza di radici storiche e passione sono i più West Coaster di tutti e a rappresentare le eccellenze della ex Riviera di Ponente (in questo caso, eccellenze anche mondiali) potevano, dovevano essere in prima fila.

Ci sono e hanno bellissime storie da raccontare. Pensare qui e ora all’estate e all’idea, al contesto, alle persone, mi consola e mi dà forza.

*Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile.*