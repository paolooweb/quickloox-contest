---
title: "Vision e più Vision"
date: 2023-06-23T18:53:35+01:00
draft: false
toc: false
comments: true
categories: [Internet, Hardware, Software]
tags: [Vision Pro, kOoLiNuS, Fried, Jason Fried]
---
Devo a [kOoLiNuS](https://koolinus.net) l’individuazione di questo straordinario post di Jason Fried, [Due visioni per il futuro](https://world.hey.com/jason/two-visions-of-the-future-88e4d9bf). Lo rubo tutto, per una volta spero che nessuno si arrabbierà. Ne vale la pena.

§§§

Che momento interessante.

Guardiamo a due diverse visioni del futuro. Possono coesistere, ma sono punti di vista radicalmente diversi su che cosa sia moderno, che cosa sia attuale e in che direzione andiamo.

Una visione elimina l’interfaccia utente dal campo visivo. L’altra è interfaccia utente ovunque si guardi.

Una visione toglie di mezzo il computer. L’altra te ne monta uno sulla faccia.

Una visione è prendi e vai. L’altra è prendi e stai.

Una visione sfuma nello sfondo. L’altra visione è frontale e centrale.

Una visione riguarda l’informazione. L’altra visione riguarda l’immersione.

Una visione è naturale e capisce come siamo fatti. L’altra visione richiede nuovi metodi di interazione che dobbiamo imparare e padroneggiare.

Una visione si avverte come un supporto. L’altra si avverte come un’ostruzione.

Una visione si adatta a qualunque cosa abbiamo già. Una visione richiede di acquistare qualcosa che si adatti.

Una visione è semplicemente testo. Una visione è tutto escluso il testo.

Una visione dà l’idea del prima. Una visione dà l’idea del dopo. Ma non sono sicuro quali siano.

Entrambe sono straordinarie.

§§§

Con questo potremmo anche chiudere definitivamente il dibattito su Vision Pro.