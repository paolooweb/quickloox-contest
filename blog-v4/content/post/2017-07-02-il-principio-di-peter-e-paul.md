---
title: "Il principio di Peter e Paul"
date: 2017-07-02
comments: true
tags: [Attivissimo, iPhone, iOS, Apfs, Sinofsky]
---
Questo è Paolo Attivissimo che spara il titolone: [Apple ha usato gli utenti come cavie di nascosto](http://attivissimo.blogspot.it/2017/06/apple-ha-usato-gli-utenti-come-cavie-di.html).

*(Momento di riflessione sulle leggerissime differenze tra le cavie di laboratorio e un miliardo di utenti iOS che si avvantaggiano di un iPhone o un iPad).*

Questo è Paolo Attivissimo giorni dopo, in un [commentino](http://attivissimo.blogspot.it/2017/06/apple-ha-usato-gli-utenti-come-cavie-di.html#c697617800920850043) che nessuno vedrà mai, sepolto in un becerume melmoso di frustrati e dove pur emergono poche eccezioni isolate e coraggiose:

>Ringrazio tutti per i dettagli tecnici che ridimensionano leggermente la natura dell’aggiornamento sperimentale, ma resto dell'idea che in un caso del genere sarebbe stato più corretto avvisare gli utenti-cavia.

*(Quindi era un po’ meno di un aggiornamento? O era un esperimento ma non troppo? Negli altri casi, invece? È percepibile la fretta).*

Sarebbe facile pensarlo vittima del noto [principio di Peter](http://www.gandalf.it/stupid/cap06.htm), per il quale ciascuno arriva a raggiungere il proprio livello di incompetenza.

Invece è un corollario, che chiamo il principio di Peter e Paul: *il numero delle attività cresce fino all’impossibilità di svolgerle tutte bene*.

Paolo è il migliore dei cacciatori di bufale ed è un eccellente *debunker*, tanto che ha dovuto perfino sopportare [l’invito alla Camera dei Deputati](http://attivissimo.blogspot.it/2017/06/debunker-alla-commissione-internet.html), una mazzata da stroncare la fibra più forte. Chiaro che poi, quando arriva il momento di occuparsi *pure* di tecnologia e metterci attenzione e competenza, le energie siano scarse.

Scettici? Gli indizi sono evidenti. Per esempio le frasi che scappano in automatico: *aggiornamenti di sicurezza*. Lo scrive talmente tante volte che, per una occasione di aggiornamento di sistema, gli è scattato il riflesso.

Oppure: il *pubblico di fan Apple*. Lo ha scritto millemila volte, gli viene automatico, probabilmente ha una macro apposta. Qui parla di tecnici, programmatori, imprenditori, professionisti che hanno sborsato [millecinquecentonovantanove dollari di iscrizione](http://www.macworld.co.uk/how-to/apple/how-get-wwdc-2017-tickets-scholarships-3510253/) e affrontato le spese di viaggio e soggiorno per una settimana a San José. Un po’ diverso da un *pubblico di fan*, forse più vicino a *persone che si guadagnano da vivere con l’informatica*. Manca il tempo di riflettere sulla differenza e le dita, causa affaticamento, seguono da sole sulla tastiera percorsi familiari.

Il principio di Peter e Paul chiaramente va affrontato, per circoscrivere le attività a quello che si riesce a fare bene. Altrimenti succede che ti fanno notare le approssimazioni e rispondi *È superficiale nel senso che è uno degli articoli di accompagnamento alla trasmissione radiofonica che conduco alla Radio Svizzera Italiana e quindi non può essere un approfondimento tecnico* che è come dire *Lì mi pagano per dire inesattezze mentre qui le scrivo finanziato dai lettori, quindi qui posso scrivere la qualunque*.

[Avevo già riportato](https://macintelligence.org/posts/2017-06-12-recensioni-divertite/) il parere di Steven Sinofsky sulla transizione di iOS al nuovo filesystem.

>La quantità di straordinaria ingegnerizzazione che è stata messa sia nella creazione che nel dispiegamento di Apfs [il nuovo filesystem di Apple] fa scoppiare la testa. E che questo avvenga su telefoni, computer e orologi è niente meno che spettacoloso; eccetto forse per la transizione da Fat a Fat32, non riesco a ricordare alcunché che nemmeno gli si avvicini.

A chi ti affideresti per una disamina sulle modalità di *testing* e *deployment* di una infrastruttura di sistema critica da distribuire in modo indolore e in tempi brevi a una miliardata di macchine: un veterano dei sistemi operativi che è stato ai vertici di una delle più grandi software house del mondo (rivale di Apple) o un giornalista distratto dai troppi impegni?

C’è anche questa cosa del riassumere affrettatamente dal sito scritto in inglese, a tradire la carenza di tempo e di attenzione. Mai lasciare che l’abitudine a fare distolga dalla qualità del fare. [The Register](https://www.theregister.co.uk/2017/06/13/long_apple_iphone_updates/):

>APFS was finally introduced in iOS update 10.3 at the end of March, three months after the 10.2 updates and five months after Apple first started experimenting on people's phones. Users reported surprise at the fact that it freed up around 10 per cent of the phone storage space as well as increased its overall capacity by around five per cent. Of course, this being Gruber and a theater full of Apple fanbois, Federighi's explanation for why millions of people had to suffer enormous and slow updates as well as phone problems was met with… lots of whooping and a round of applause. Amazing.

Paolo:

>Il nuovo filesystem è stato introdotto con la 10.3 a fine marzo e ha liberato un po’ di spazio di memoria. Ma la cosa curiosa è che l’ammissione di aver usato i dispositivi dei clienti e i loro dati per un test segreto è stata accolta dal pubblico di fan Apple con un applauso.

*(Il contesto è che Gruber chiede conto della spettacolare transizione a Apfs e l’applauso più fragoroso arriva prima, quando Federighi elogia il team che ha lavorato al filesystem. Poi spiega la meccanica e c’è un nuovo applauso, di persone esperte che hanno capito la difficoltà del compito e colto il valore del successo riportato. Va da sé che il cacciatore di bufale si è ben guardato dal verificare le affermazioni di The Register sui problemi degli aggiornamenti a iOS e la loro entità. Troppe cose da fare, la qualità scende).*

La soluzione è semplice, Paolo: ci sono cose che sai fare benissimo e per cui sei insostituibile. Concentrati su quelle, fai un’ottima figura e sei stimato da tutti.