---
title: "Perculare e percolare"
date: 2021-06-03T12:44:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [Wwdc, Jason Snell, Six Colors, Comandi rapidi, Shortcuts, Mac, iPad] 
---
Dell’opportunità di portare i Comandi rapidi su Mac [hanno già detto in diversi](https://macintelligence.org/posts/Caos-calmo.html) e poco cambia che Jason Snell lo abbia ribadito nella sua raccolta di [cose che vorrebbe vedere presentate a questa Wwdc](https://www.macworld.com/article/347866/wwdc-2021-a-mac-and-macos-wish-list.html).

Da ribadire c’è pure la gran differenza tra quanti perculano sulla storia dell’unificazione del sistema operativo e le tecnologie software che percolano da un sistema all’altro e portano a un ecosistema omogeneo senza sacrificare l’adattamento ottimale di ciascun sistema operativo a ciascuna macchina. La lista si allunga, Catalyst migliora e il progetto complessivo ha un buon aspetto. Avanti con la percolazione.

*Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile.*               