---
title: "Un po' come Miss Muretto"
date: 2013-09-01
comments: true
tags: [Mac]
---
Un concorso di bellezza a rovescio: qual è il software più brutto che usi?

Niente usabilità, interfaccia, opzioni di personalizzazione, robe complicate. Proprio bruttezza: una cosa che non si può vedere, anche se è funzionale, anche se è utile.<!--more-->

Per esempio: [FileZilla](https://filezilla-project.org) è un programma straordinario per versatilità e mi ha tolto varie castagne dal fuoco. Però lo trovo brutto, di quelli che proprio non si possono guardare.

Qual è il software più brutto che usi?