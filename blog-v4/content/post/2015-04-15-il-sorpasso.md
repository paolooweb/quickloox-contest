---
title: "Il sorpasso"
date: 2015-04-16
comments: true
tags: [Windows, Apple, Gartner, iPhone, iPad, iOS, OSX]
---
[Segnala](http://www.asymco.com/2015/04/14/personal-computer/) Horace Dediu di Asymco che, in base a una interpretazione conservativa delle stime di vendita di Gartner, nel 2015 si venderanno 285,6 milioni di PC Windows e un combinato di 302 milioni tra Mac, iPhone e iPad.<!--more-->

**Si venderanno più computer iOS e OS X che computer Windows.**

Se va davvero così, propongo che il 14 aprile – giorno del *post* di Dediu – diventi la Festa del Respiro a Pieni Polmoni.