---
title: "Futuro contro favole"
date: 2014-01-22
comments: true
tags: [Adams, Mac, OSX]
---
Due generi che vivono benissimo insieme, a patto di saperli distinguere.<!--more-->

Le chiacchiere su iPhone 6, orologi, televisori e quant’altro appartengono al secondo.

Per il primo può fare scuola Douglas Adams, come testimonia l’ultimo [*post* che ha affisso sul proprio forum](http://www.douglasadams.com/cgi-bin/mboard/info/dnathread.cgi?2922,1) prima di salutare tutti e partire alla volta della galassia più irraggiungibile:

>Volevo aspettare fino all’estate, ma non ce l’ho fatta e l’ho installato settimana scorsa. Ci vuole un pochino ad abituarsi, le vecchie abitudini sono difficili da cambiare e molto del software disponibile è in versione provvisoria.

>Ma…

>Penso che sia brillante. Me ne sono completamente innamorato. E la promessa di quello che arriverà una volta che la gente inizi a programmare in Cocoa è fantastica…

Era il 26 aprile 2001 e Douglas Adams parlava di Mac OS X. Come scrittore di fantascienza aveva più idee in un secondo che tutti i siti di *rumors* messi insieme in un semestre e sapeva capire il futuro invece che intortare la gente con le favole. Lui le favole le metteva nei libri ed erano anche straordinari.
