---
title: "Le penultime parole famose"
date: 2015-02-26
comments: true
tags: [Palm, Colligan, Apple, Hayek, Swatch, watch, Zetsche, Daimler, Mercedes]
---
Ci fu l’amministratore delegato di Palm, Ed Colligan, a [commentare](http://www.palminfocenter.com/news/9110/colligan-laughs-off-iphone-competition/) l’eventuale ingresso di Apple nel mondo dei cellulari.<!--more-->

>Abbiamo faticato per anni per capire come costruire un buon telefono. Gente che fa computer non riesce a capire questo settore. Non ci entreranno così all’istante.

Ed è arrivato Nick Hayek, amministratore delegato di Swatch, a [commentare](http://www.bloomberg.com/news/articles/2013-03-06/swatch-chief-hayek-skeptical-that-watch-could-replace-an-iphone) l’imminente ingresso di Apple nel mondo degli orologi da polso.

>Non credo che sarà la prossima rivoluzione. Sostituire un iPhone con un terminale interattivo da polso è difficile. Non si può avere uno schermo immenso.

Per ora è l’ultimo in lista; siamo tutti in attesa di vedere che cosa combinerà [watch](http://www.apple.com/it/watch/?cid=wwa-it-kwg-watch-com).

Ma c’è sempre qualcuno disposto a competere per il primato: Dieter Zetsche, amministratore delegato di Daimler, ha [commentato](http://www.motoring.com.au/news/2015/mercedes-benz-dont-build-an-icar-49356) il possibile ingresso di Apple nel mercato automobilistico.

>Se girasse la voce che Daimler o Mercedes pianificano di mettersi a costruire cellulari, in Apple non perderebbero il sonno. Lo stesso vale per me e lo dico pieno di rispetto per Apple.

Avanti il prossimo.