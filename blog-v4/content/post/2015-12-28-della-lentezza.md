---
title: "Della lentezza"
date: 2015-12-28
comments: true
tags: [Numerera, Drafts, Graphic, Autodesk]
---
Ci si può immaginare che la cronaca di questi giorni sia tipica dello *slow news cycle*, cioè non succede niente. Personalmente, per la prima volta da anni, sto pochissimo tempo al computer e oltretutto passerò diversi giorni in zone assai poco servite dalla connessione.

Tuttavia cercherò di organizzarmi.<!--more-->

[Confermo](http://macintelligence.org/blog/2015-12-17-avvistamento-renne/) per ora l’arrivo natalizio di [Drafts 4](https://itunes.apple.com/it/app/drafts-4-quickly-capture-notes/id905337691?l=en&mt=8) per iOS e e Graphic per [iOS](https://itunes.apple.com/it/app/graphic-illustration-design/id363317633?l=en&mt=8) e [Mac](https://itunes.apple.com/it/app/graphic/id404705039?l=en&mt=12).

Drafts sembra un innocente editor di testo e invece nasconde una serie di raffinatezze per avere sia il massimo della semplicità per scrivere sia il massimo della sofisticazione per postprodurre e fare viaggiare i file. Causa il pochissimo tempo ho solo scalfito la superficie e non mi sono azzardato, ancora, a usarlo come piattaforme di lavoro. Appare molto interessante. Graphic, ex iDraw acquisito da Autodesk, è un vettoriale leggero e spero di poterlo mettere alla prova quanto prima. Spero di poter raccontare qualcosa più di questo nei giorni prossimi.

Quanto a qualcosa di completamente diverso, mi hanno anche regalato [Numenera](http://www.numenera.com). Il medio termine mi vede sicuramente preso da Dungeons & Dragons, però bisogna avere l’occhio sulle novità e Numenera sembra la cosa più fresca in materia uscita negli ultimi anni. Se c’è interesse, a domanda rispondo.