---
title: "Giochi al tempo della carta"
date: 2018-01-20
comments: true
tags: [PowerBook, Tempest, Atari, retrocomputing]
---
Se veramente il PowerBook 1400 che **Paolo** mi ha segnalato su eBay verrà venduto [a 13.099 euro](https://www.ebay.it/itm/PowerBook-Apple-Macintosh-vintage/122925775391?hash=item1c9ef27a1f%3Ag%3A4WwAAOSwFMdaZHxe), vorrà dire che forse questa cosa del retrocomputing sta sfuggendo di mano.<!--more-->

Disciplina altrimenti reverendissima quando presa in giuste dosi, se vaccinati dal meme *si stava meglio quando si stava peggio*.

Per esempio questa [rievocazione della creazione di Tempest](https://arcadeblogger.com/2018/01/19/atari-tempest-dave-theurers-masterpiece/), videogioco straordinariamente innovativo per il suo tempo.

Un tempo dove un programmatore di punta in Atari, azienda di punta nel settore, scriveva il codice del gioco *a mano* e lo passava a *dattilografi* che lo inserivano in un minicomputer per il successivo debugging.

Curiosità su Tempest? Lo si può giocare in macOS 9 sotto forma di un clone di nome [Arashi](https://www.macintoshrepository.org/5615-arashi) oppure sotto [Mame](http://sdlmame.lngn.net), dopo avere reperito le Rom del gioco su Google.