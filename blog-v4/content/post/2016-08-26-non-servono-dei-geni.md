---
title: "Non servono dei geni"
date: 2016-08-26
comments: true
tags: [Excel, Numbers, LibreOffice, Sept2, Septin, March1]
---
L’amico **Sabino** ha pubblicato uno spettacolare *post* che dimostra, esempi alla mano, come un [Numbers](http://www.apple.com/it/mac/numbers/) o un [LibreOffice](http://www.libreoffice.org) siano fogli di calcolo completamente sufficienti per un utilizzo normale ma, appena si preme l’acceleratore, siano ancora [inferiori a Excel](http://www.libreoffice.org).<!--more-->

Ho scritto *dimostra*; il contenuto del *post* è indiscutibile. Difatti il problema vero di Excel non è come svolga il suo mestiere, bensì come decenni di condizionamento abbiano portato la gente a concepire Excel e gli altri componenti di Office. Lo stesso Sabino lo spiega chiaramente per esempio in [Vita oltre PowerPoint](https://melabit.wordpress.com/2016/07/21/vita-oltre-powerpoint/) e in [Da Keynote a PowerPoint](https://melabit.wordpress.com/2016/07/22/da-keynote-a-powerpoint/). (Essere iscritti al blog di Sabino è una grande opportunità per qualsiasi persona attenta al mondo della tecnologia, ma è un altro discorso più articolato che magari si farà una prossima volta).

Il problema di Excel, per esempio, è che – qualcuno si occupa qui di genetica? – una ricerca su cinque di quelle che contengono un file Excel in allegato [storpia i nomi dei geni](http://genomebiology.biomedcentral.com/articles/10.1186/s13059-016-1044-7) grazie, si fa per dire, alla conversione automatica e arbitraria effettuata da Excel. Forse non sono stato chiaro: *uno studio di genetica ogni cinque contiene una nomenclatura sballata dei geni*, a causa di Excel. È una cosa assurda con una diffusione abnorme, che non aiuta certo il lavoro dei ricercatori.

Succede che i geni hanno nomi che Excel interpreta come numeri con un esponente: *2310009E13* diventa in Excel due virgola trentuno per dieci elevato alla tredicesima (*2.31E+13*). Oppure sigle quali *SEPT2* e *MARCH1* vengono trasformate prevedibilmente in date di calendario, con esiti nefasti per la comprensione.

Sembrano stupidi questi scienziati, vero? Sappiamo tutti che basta mettere i valori tra virgolette perché vengano interpretati come testo puro e lasciati in pace.

Invece non sono stupidi. Sono *scienziati*. Hanno una conoscenza poderosa del loro campo e, per forza di cose, più superficiale in altri. Ed è qui che Excel, come il resto di Office, mostra la sua perniciosità.

Hanno in mano un software dove è facile scrivere colonne di dati e le scrivono, o le incollano. Anche se il software migliore per scrivere colonne di dati non è certo un foglio di calcolo. Sono abituati a *fare tutto con Excel* e lo fanno, senza accorgersi che è sbagliato, profondamente: Excel è un tuttofare, non un software per genetisti. Pure lui ignora cose che in certi laboratori sono scontate, per esempio il nome di geni che tutti i suoi programmatori portano nel Dna.

Credono questi scienziati, o gli fanno credere, che Excel sia l’unico software veramente valido per svolgere il loro lavoro. Pensano, o gli fanno pensare, che il suo uso non nasconda insidie, che non serva un manuale o una seconda occhiata. Perché dovrebbe? Excel *lo usano tutti*, come può causare problemi?

Ecco una buona ragione per mettere da parte Excel: l’immagine che lo circonda è diventata eccessiva e alterata. Viene usato in modo acritico e miope, dando per scontate cose che non lo sono. Facciamo, per lo meno, che venga usato con coscienza. Quella che non è una panacea, non è l’unico e non è una forchetta né un cacciavite: in misura magari minima, ma va capito, come ogni altro software. Non servono dei geni per farlo capire agli scienziati e neanche al compagno di scrivania illuso che al mondo esista solo Office.