---
title: "Remoto, passato"
date: 2023-08-09T01:19:41+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [Zoom]
---
Pensavo di avere visto un bel po’ di cose nella vita, ma oggi è arrivato il giorno di leggere che [Zoom chiede ai suoi dipendenti di tornare in ufficio](https://www.businessinsider.com/zoom-tells-employees-return-to-office-remote-work-video-conferencing-2023-8) e così mi rendo conto di dover aggiornare il catalogo.

Di fatto la richiesta copre due giorni a settimana e riguarda i dipendenti a meno di cinquanta miglia da un ufficio Zoom. Non è l’apocalisse del *remote working*, tuttavia che a richiederlo sia la maggiore società di soluzioni di videoconferenza fa porre più di una domanda.

Apple, per esempio, ha già annunciato la videoconferenza stile previsioni del tempo, dove il relatore può apparire sovraimposto ai dati e così superare la malinconia della famigerata condivisione dello schermo così come la condizione monotona del mezzobusto. Zoom non ci è ancora arrivata e post-COVID aveva un‘autostrada spianata per condurre una rivoluzione nella condizione lavorativa; chissà su quale oscura strategia ha buttato via risorse e talenti.

Tranquilli, comunque, che esplora sistemi per mettere l’intelligenza artificiale a disposizione degli utenti, non si capisce bene per che cosa farci: far parlare un deepfake al posto nostro? Per molte riunioni potrebbe essere un eccellente moltiplicatore di produttività.

Finisce che sprechiamo una grandissima occasione di innovare il lavoro ed è una tragedia, perché indica l’incapacità di prendere rischi, di intravedere strade nuove, di ripensare la logica dell’ufficio, che diventa servizio invece di caserma, ma niente.

Se non altro, ho la fortuna di sperimentarlo di persona, ci sono comunque realtà che hanno ridisegnato il proprio flusso produttivo all’insegna del vero smart working. Per una di esse ho coniato lo slogan *il lavoro non è dove sei, ma dove stai*. Ci possono essere incidenti di percorso, ma il modello novecentesco del raduno mattutino in ufficio si sbriciolerà ugualmente, per tante ragioni che mi confortano. Come videoconferenza si può anche provare FaceTime con sotto un bel Freeform condiviso, eh.

*Gli aggiornamenti in agosto del blog potrebbero essere irregolari o infrequenti.*