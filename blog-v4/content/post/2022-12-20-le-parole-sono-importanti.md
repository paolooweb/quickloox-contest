---
title: "Le parole sono importanti"
date: 2022-12-20T20:04:30+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [OpenAI, ChatGPT, Point-E]
---
Gli obiettivi di ricerca di OpenAI continuano a dispiegarsi: da un input testuale, ora il sistema [genera a richiesta anche nuvole di punti](https://techcrunch.com/2022/12/20/openai-releases-point-e-an-ai-that-generates-3d-models/) da cui ricavare, al momento con alterne fortune, modelli tridimensionali.

Nel frattempo capita di proseguire negli esperimenti con ChatGPT e i limiti dello strumento diventano sempre più chiari e frustranti. Non perché siano limiti in quanto tali: se dipendessero da un modello insufficiente, si potrebbe sempre adeguare il modello. Perché i limiti gravi sono quelli algoritmici, assai meno chiari da sistemare e suscettibili di permanere anche quando ChatGPT fosse addestrato con tutti i dati dell’universo (una versione tecnologica della [mappa 1:1 sulla cui perfezione ironizzava Lewis Carroll](https://people.duke.edu/~ng46/topics/lewis-carroll.htm)).

Spinto da un amico convinto che la chat potesse produrre articoli *nello stile di*, ho provato con l’amico, il sottoscritto, un amico comune di rango giornalistico superiore, fino ad arrivare a Federico Rampini e Umberto Eco. Per tutti la mia richiesta era quella di un articolo o di un post da blog.

Non solo la prosa risultante era all’altezza di un temino di prima media, ma i post erano tutti sosostanzialmente uguali. Di indicazioni di stile ce n’erano zero; nessuna locuzione abituale, niente vocaboli particolari, modi di dire, abitudini sintattiche, niente. Il periodare era sempre quello, stesso ritmo, stessa costruzione, stessa prima media.

Nel frattempo il bravo Fabrizio Venerandi scriveva su Facebook dell’impegno che ci vuole nell’imparare a fare produrre a ChatGPT quello che desidera l’umano. (Non trovo il link ora, ma è assolutamente vero).

Ecco, io pensavo che nel machine learning fosse la macchina a imparare. Così, per significato delle parole.