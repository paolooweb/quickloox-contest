---
title: "Il leone si è risvegliato"
date: 2014-03-24
comments: true
tags: [Fallon, Joel, Loopy, Android]
---
Il conduttore americano Jimmy Fallon, Billy Joel e la *app* [Loopy HD](https://itunes.apple.com/it/app/loopy-hd/id467923185?l=en&mt=8), 6,99 euro.<!--more-->

Gente preparata, che quindi può improvvisare, o fare finta, al punto che [nessuno nota la differenza](https://www.youtube.com/watch?v=Bmq8792Kr6w).

Quando mai si vedrà una *app* così nascere su Android, uno si chiede.

<iframe width="560" height="315" src="https://www.youtube.com/embed/Bmq8792Kr6w" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>