---
title: "Universo grande, intelligenza piccola"
date: 2023-09-11T00:00:59+01:00
draft: false
toc: false
comments: true
categories: [Internet, Software]
tags: [50 Years of Text, Lebling, Dave Lebling, Zork, ai, intelligenza artificiale, Ars Technica]
---
Continuo a godere a piccole gocce dell’acquisto di [50 Years of Text Games](https://macintelligence.org/posts/2023-06-20-un-testo-per-lestate/) e continuo a raccomandarne l’acquisto, salvo avversione allo stimolo dell’immaginazione tramite il testo puro.

In una [recensione dell’opera comparsa su Ars Technica](https://arstechnica.com/gaming/2023/06/50-years-of-text-games-parses-the-rich-history-of-a-foundational-genre/), mi sono imbattuto in questa citazione del cocreatore di [Zork](https://macintelligence.org/posts/2016-10-28-morte-e-rinascita-dellimmaginazione/), [Dave Lebling](https://www.mobygames.com/person/201/dave-lebling/):

>Chiaramente, nessun piccolo programma per computer può abbracciare l’intero universo. Quello che può fare, comunque, è simulare abbastanza universo in modo che ci appaia più intelligente di quanto sia davvero.

Parafrasando, per quanto mi riguarda, tira più una microavventura capace di aprire nuove strade al pensiero e all’immaginazione che mille chatbot bacchettoni e prosaici.