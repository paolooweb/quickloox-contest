---
title: "Le giuste proporzioni"
date: 2023-02-24T04:43:56+01:00
draft: false
toc: false
comments: true
categories: [Hardware]
tags: [Apple, Apple Watch]
---
Maturo lentamente l’impressione che ci siano una attesa e una insistenza su un visore Apple per realtà virtuale e aumentata che, quando verrà effettivamente presentato, interesserà quattro gatti (con rispetto parlando e sempre relativamente a una società con oltre un miliardo di utenti attivi).

Di converso, si parla pochissimo di un nuovo sensore a zero invasività (pronto per watch, diciamo) per controllare i livelli di glucosio nel sangue. Che, quandunque uscisse, sarebbe accolto con fervore da una platea di milioni di persone per le quali cambierebbe veramente la vita.