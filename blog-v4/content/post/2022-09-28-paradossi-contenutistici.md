---
title: "Paradossi contenutistici"
date: 2022-09-28T01:43:01+01:00
draft: false
toc: false
comments: true
categories: [Software, Web]
tags: [Dall-E, Dall-E 2, OpenAI, Gpt-3. DiffusionBee]
---
È caduta un’altra barriera: ora [chiunque può accedere a DALL-E](https://openai.com/dall-e-2/) e commissionare a un sistema esperto (sorry, non è una intelligenza artificiale e neanche gli somiglia) disegni e fotografie realizzate a partire da una descrizione testuale (come *vorrei una torta di frutta con sopra un pinguino disegnato da Picasso che ruota dentro un cerchio di fuoco viola*).

Si tratta di materiali alieni, che vediamo apparire da un mondo diverso, immaginario, frutto delle elucubrazioni degli algoritmi di Dall-E (arrivato alla seconda versione) e completamente *fiction* anche dove mostri il massimo realismo.

(La via più veloce per provare la nuova tecnologia su Mac è [DiffusionBee](https://github.com/divamgupta/diffusionbee-stable-diffusion-ui), gratis su GitHub e pronto da scaricare come immagine disco).

Il fatto che si possa fare lo stesso con il testo, sebbene in modo leggermente più complicato, attraverso [Gpt-3](https://openai.com/api/), significa che sarebbe possibile creare una rivista illustrata di attualità non solo riempita di materiale inventato (praticamente equiparata a un qualunque organo di partito), ma anche *non inventato da umani* (per quanto da essi indirizzato e specificato).

Come sempre, le implicazioni per tutti e per ciascuno sono enormi, difficili da valutare compiutamente e, in modo ostinato, particolarmente equivalenti tra positivo e negativo. Se ci sono dubbi su come le tecnologie moderne possano essere usate bene o male pur restando sempre quelle, la cronaca di questo triennio dovrebbe averli fugati in abbondanza.

Dobbiamo sempre guardare alla necessità – la possibilità non basta proprio più – di adottare la tecnologia con l’intento preciso di crescere, nella conoscenza e nella consapevolezza, per fare cose che nessun’altra fascia di umanità in nessun altro snodo della storia ha avuto a disposizione.

Solo crescendo in conoscenza e consapevolezza possiamo infatti disporci adeguatamente a contrastare chi non vuole crescere e utilizza la tecnologia solo e unicamente per bieco profitto personale o per il gusto di distruggere vite di altri, a tutti i livelli.

Questo testo è scritto alla vecchia maniera. In futuro dovremo essere capaci di conservare il raziocinio necessario a evitare di leggere un testo analogo, ma scritto da Gpt-3, e dire *ha ragione* o *ha torto*, per non generare paradossi certo meno intricati di quelli temporali, ugualmente rovinosi.