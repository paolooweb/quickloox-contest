---
title: "Uno Zoom dietro le quinte"
date: 2020-03-31
comments: true
tags: [Zoom, Gruber, iOS, Mac, Daring, Fireball]
---
Delle mille soluzioni di videoconferenza possibili emerge come preferenza assai diffusa Zoom, che non è esattamente il mio preferito (invece, [Valarea](https://www.valarea.com) per lavorare e [Jitsi](https://jitsi.org) per chiacchierare), magari dopo un’occhiata all’elenco di server di [iorestocasa.work](https://iorestoacasa.work), che può tornare utile).

Non linko Zoom perché è venuto fuori che la versione iOS trasmetteva dati a Facebook, anche se non si era iscritti a Facebook. Un aggiornamento passato poco dopo l’uscita della notizia ha eliminato il problema, solo che resta la licenza d’uso del programma, che praticamente consente a Zoom di fare un po’ quello che vuole in termini di rispetto della privacy.

Su Mac, Zoom installava senza dire niente un server che restava installato e in funzione anche quando l’applicazione veniva cancellata. Lo scorso luglio Apple ha applicato un aggiornamento di sistema silente a macOS con lo scopo preciso di eliminare il server nascosto di Zoom.

John Gruber [riassume tutta la vicenda](https://daringfireball.net/2020/03/regarding_zoom).

La tecnologia di Zoom, intendiamoci, è di prima classe. Da un punto di vista di resa e tenuta della videoconferenza, anche con grandi numeri, niente da dire. Quello che accade dietro le quinte, però, è poco simpatico.