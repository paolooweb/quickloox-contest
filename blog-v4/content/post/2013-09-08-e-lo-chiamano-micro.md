---
title: "E lo chiamano micro"
date: 2013-09-08
comments: true
tags: [Android]
---
Pare che il Galaxy Note III di Samsung avrà un connettore Usb 3.0. Il che porta diversi vantaggi, per esempio l’intensità di corrente che si può assorbire dal computer al momento di caricare la batteria.<!--more-->

Adesso però [si osservi il cavo](http://www.androidbeat.com/2013/09/say-hello-hideous-new-usb-3-0-cable-new-smartphone-tablet-will-come/) che andrà connesso al suddetto Galaxy: lo chiamano *microUsb*.

Si facciano ragionamenti su miniaturizzazione, semplicità di utilizzo, possibilità di guasti eccetera.

Si guardi un cavo Lightning di Apple e si pensi alle polemiche che lo hanno accompagnato, perché *non standard*.

Se lo standard è questo, preferisco Lightning, grazie.