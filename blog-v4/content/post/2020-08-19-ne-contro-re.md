---
title: "Ne contro Re"
date: 2020-08-19
comments: true
tags: [NetNewsWire, Reeder, iOS, RSS, Feedly, iPadOS, Drang]
---
Alla lunga la [adozione di NetNewsWire](https://macintelligence.org/blog/2020/03/11/confessioni-di-un-lettore-malandrino/) come lettore di feed RSS su iOS mi convince. Il software è semplice, veloce, usabile, soddisfacente.

Oggi ho provato a confrontarlo velocemente con [Reeder](https://reederapp.com), che trovo una buonissima app come prima impressione. In superficie, tuttavia, non trovo stimoli a cambiare cavallo.

Su tutti e due ho caricato il mio account [Feedly](https://feedly.com/), da tempo il deposito dei miei feed. Ho visualizzato una delle categorie e ho osservato il risultato per come è il default dei due programmi, senza entrare nel merito delle possibilità di configurazione. Questo è iOS (NetNewsWire è il primo):

<p float="left">
  <img src="/images/netnewswire.png" alt="NetNewsWire iOS" width="48%" />
  <img src="/images/reeder-4.png" alt="Reeder iOS" width="48%" />
</p>

Questo è iPadOS:

<p float="left">
  <img src="/images/netnewswire-ipad.png" alt="NetNewsWire iPadOS" width="48%" />
  <img src="/images/reeder-4-ipad.png" alt="Reeder iPadOS" width="48%" />
</p>


[NetNewsWire](https://ranchero.com/netnewswire/) visualizza più contenuti ed è più funzionale a uno skimming, uno scorrimento veloce dell’elenco alla ricerca di cose interessanti. Reeder è decisamente più elegante ma un po’ meno efficace: mostra troppe cose in prima battuta, quando mi interessa solo cogliere al volo l’argomento per decidere se approfondire.

La cosa è particolarmente evidente su iPhone. Su iPad, invece, NetNewsWire sfrutta più intelligentemente lo spazio a disposizione, togliendo di torno l’elenco delle categorie dopo averne scelta una.

Questa naturalmente non è una recensione; Reeder mi ha dato l’impressione di una ottima versatilità e penso che, ben configurato, possa costituire una eccellente app per i feed RSS. La prima impressione, tuttavia, premia NetNewsWire.

Mi limito a iOS anche perché entrambi i programmi sono disponibili per Mac, solo che non guardo un feed su Mac da mesi. In questo momento per me è una attività esclusivamente da _second screen_.

Mi piace chiudere con una scelta di programma per feed RSS capace di raggiungere quello che nessuna app può dare: la completa aderenza alle proprie specifiche personali. Dr. Drang si è scritto la lettura di feed come la voleva e ha reso pubblico il [codice sorgente](https://leancrew.com/all-this/2015/11/simpler-syndication/).

Più complesso del solito script, ma ancora più che accessibile per chi volesse guardare il proprio feed dal piano nobile.