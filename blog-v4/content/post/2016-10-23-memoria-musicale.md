---
title: "Memoria musicale"
date: 2016-10-23
comments: true
tags: [Footloose, Mac, Lorescuba]
---
Scrive **Lorescuba**:

>Teatro nazionale, musical Footloose. Musica e voci live, foto del direttore dell’orchestra… casualmente si vede un Mac…

Improbabile. Apple, mi dicono, si è dimenticata dei professionisti.

 ![Mac a Footloose](/images/footloose.jpg  "Mac a Footloose") 