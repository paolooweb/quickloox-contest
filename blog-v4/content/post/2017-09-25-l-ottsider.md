---
title: "L’ottsider"
date: 2017-09-25
comments: true
tags: [iPhone, S8, Samsung, Galaxy, Tom’s]
---
Si sa che Apple ha annunciato la nuova linea di iPhone e i commenti non mancano.<!--more-->

È emerso ultimamente che i test di potenza del processore A11 Bionic [pongono iPhone all’altezza di un MacBook Pro 13”](http://appleinsider.com/articles/17/09/18/with-iphone-8-apples-silicon-gap-widens-as-the-new-a11-bionic-obliterates-top-chips-from-qualcomm-samsung-huawei) e molto sopra apparecchi Android di punta come Galaxy Note 8, OnePlus 5 e Galaxy S8.

I test di velocità però possiedono sempre una componente astratta e separata dal mondo reale. Per dirne una, è vero che A11 Bionic pareggia i conti con l’i5 di MacBook Pro. Al tempo stesso un MacBook Pro è carrozzato per sostenere quella potenza su lunghi periodi; un iPhone manca di dissipazione termica paragonabile a quella di un portatile e quindi può sì spingersi a quei livelli di prestazione, mantenendoli tuttavia per periodi brevi.

Diverso è mettere alla prova i telefoni con operazioni che potrebbero benissimo accadere nel mondo reale.

*Tom’s Hardware* [ha dato in pasto ad alcuni apparecchi un video 4K](https://www.tomsguide.com/us/iphone-8-benchmarks-fastest-phone,review-4676.html) da due minuti, per manipolare il video con varie operazioni e infine esportarlo.

Galaxy S8+ ci ha messo più di quattro minuti. Galaxy Note 8, più di tre. iPhone, quarantadue secondi. Intanto che S8+ finiva, poteva fare il giro sei volte.

Eh sì: i nuovi iPhone ridicolizzano le capacità hardware della concorrenza Android.

I nuovi iPhone 8, naturalmente.

Se iPhone X non esistesse, gli annunci di quest’anno sarebbero comunque notevoli per il divario di prestazioni che scavano tra essi e Android. E adesso chi lo spiega a quelli che *non c’è innovazione*?