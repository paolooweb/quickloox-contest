---
title: "Come cerchi"
date: 2018-09-23
comments: true
tags: [Viticci, iOS12, watch, MacStories]
---
Diffido delle funzioni di sistema legate alle abitudini personali e così  ero scettico nei confronti di watch e del suo sistema di cerchi da completare facendo movimento, facendo esercizio e trovando modo di stare in piedi durante il giorno.

Mi sono ricreduto nel tempo. Il sistema è rilassato e rilassante; invece che dare ordini, tende a suggerire nel tempo buone abitudini. In nessun caso mi sono trovato a consultare febbrilmente watch per vedere a che punto erano i cerchi e invece il riepilogo del lunedì mattina è diventato una piacevole abitudine. È una cosa buona, insomma.

È rinfrescante leggere lo stesso tipo di considerazioni nella enciclopedica [presentazione di iOS 12 di Federico Viticci su MacStories](https://www.macstories.net/stories/ios-12-the-macstories-review/5/#content).

Il link porta direttamente alla pagina che tratta delle app e funzioni preposte a monitorare l’uso del sistema e porvi limiti. Raccomando in particolare là lettura di questi passaggi, anche se la recensione è tuta di livello molto alto; si vede chiaramente come il software non sia volto a cambiare le abitudini o imporre dei comportamenti, bensì a fornire informazioni. Se salta fuori che passi un sacco di tempo a scorrere la *timeline* di Facebook, sei tu a decidere che è troppo, o troppo poco, e decidere se orendere provvedimenti.

Il software non ti fa cambiare abitudini ma ti dà la base informatica per, eventualmente, farlo. È una differenza sottile ma importante, che Apple ha colto molto bene, sui dati di uso di iPhone/iPad come sui cerchi colorati di watch.
