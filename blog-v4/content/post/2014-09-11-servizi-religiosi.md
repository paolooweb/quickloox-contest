---
title: "Servizi religiosi"
date: 2014-09-12
comments: true
tags: [Eugenio, Lumia, iPhone4, iPhone6, iCloud]
---
Mi scrive **Eugenio** ed è una perfetta risposta a certi [episodi](https://macintelligence.org/posts/2014-09-11-cose-che-succedono/). Quanto sotto, da grassetto a grassetto, è il suo testo.<!--more-->

**A** giugno scorso ho effettuato l’abbonamento al piano di archiviazione iCloud da dieci gigabyte pagandolo sedici euro.

Stamattina ho avuto la gradita sorpresa di ricevere una mail da Apple, con la quale mi avvisano che, a seguito dell’annuncio dei nuovi piani di archiviazione, il mio spazio non solo viene esteso da dieci a venti gigabyte, ma mi viene pure rimborsata la differenza per i mesi restanti.

Apple sarà pure una religione come dice qualcuno (e oltretutto limiterebbe la mia libertà con i suoi *device* chiusi) ma ha un servizio clienti che le altre aziende si sognano.

Ti racconto un’altra storiella.

Un mio amico qualche mese fa ha rotto iPhone 4. Per temporeggiare aspettando iPhone 6, si è comprato un Lumia 520.

Dopo sei mesi, ieri ha provato a riaccendere iPhone 4, ha scoperto che funziona e il suo commento è stato *è come fare un salto nel futuro*.

Tre anni di differenza e non c’è **storia**.

Di sicuro il mio senso religioso va in direzioni differenti da quelle del listino Apple. Eppure, che a Cupertino abbiano un po’ di culto del cliente, mi aggrada.