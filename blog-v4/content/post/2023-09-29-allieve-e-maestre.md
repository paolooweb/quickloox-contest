---
title: "Allieve e maestre"
date: 2023-09-29T23:12:16+01:00
draft: false
toc: false
comments: true
categories: [Hardware]
tags: [Tesla, unibody, Reuters, iPhone 15 Pro, Titanium]
---
[Tesla reinventa la fabbricazione delle auto grazie a sottaciuti progressi tecnologici](https://www.reuters.com/technology/gigacasting-20-tesla-reinvents-carmaking-with-quiet-breakthrough-2023-09-14/), titola *Reuters*.

Elon Musk ha l’obiettivo di dimezzare il costo di produzione di un’auto e la sua azienda ha adottato in forma pionieristica immense presse capaci di stringere con una forza tra le seimila e le novemila tonnellate,

>per produrre la struttura anteriore e quella posteriore della Model Y con una procedura di “gigacasting” che abbatte i costi e ha lasciato le concorrenti ad agitarsi per recuperare lo svantaggio.

Il punto di arrivo sarebbe fare della struttura interna dell’auto un pezzo unico, invece dei quattrocento attuali.

Applicata ad autoveicoli, non mi sorprende scoprirla procedura innovativa che lascia indietro la concorrenza. È inevitabile però correre con la memoria a quando Apple ha iniziato a costruire i MacBook Pro con la tecnica [unibody](https://macintelligence.org/posts/2019-06-14-la-reinvenzione-delle-ruote/). Quindici anni fa.

Reuters fa riferimento a un’altra tecnologia di rottura che Tesla ha in via di introduzione nei processi di costruzione: l’utilizzo di stampi di sabbia industriale abbinato alla stampa 3D, che abbatte i tempi di validazione di uno stampo da mesi a settimane.

Chissà a quale materiale si fa riferimento nella grafica di lancio di [iPhone 15 Pro](https://www.apple.com/it/iphone-15-pro/), con tutte quelle particelle soffiate via dalla parola *Titanium*.

Forse un giorno l’allieva supererà la maestra, chissà. Per ora è chiarissimo chi insegni a chi.