---
title: "Comunicazione da riparare"
date: 2019-08-27
comments: true
tags: [iPhone, iPad, batterie]
---
Era solo mezza notizia, quella della [crescente pignoleria di Apple verso l’uso di batterie non originali nei propri apparecchi](https://macintelligence.org/posts/2019-08-23-la-carica-dei-malpensanti/).

L’altra mezza notizia è che [viene allargato il numero degli operatori indipendenti autorizzati a compiere riparazioni su apparecchi Apple](https://www.apple.com/newsroom/2019/08/apple-offers-customers-even-more-options-for-safe-reliable-repairs/). I riparatori avranno accesso a tutti gli strumenti, le parti di ricambio e le documentazioni di Cupertino. In più dovranno essere certificati e superare una serie di esami.

Apple diventa pignola sulle batterie. Il sospetto di molti è che voglia blindare un business dei ricambi.

Apple diventa pignola sulla qualità dei riparatori e contemporaneamente ne fa aumentare il numero. In pratica, se c’è un business dei ricambi, lo demanda, tutto o in parte, a terzi. Il che smentisce l’idea della blindatura del business.

Il sospetto mio è che, semplicemente, vogliano rendere più agevole la riparazione – per dire – di un iPhone e, grazie all’alleggerimento del carico di lavoro sugli Store, aumentare la qualità del servizio degli Store stessi e quindi la qualità del servizio al cliente.

Contemporaneamente, con la pignoleria sulle batterie originali, sembra anche esserci l’intenzione di limitare almeno certe categorie di guasti e lasciare *al cinese* solo e soltanto quelli che proprio vogliono scientemente correre il rischio di una riparazione senza garanzie.

Faccio fatica a vedere penalizzazioni dei clienti o profitti furbi a spese dei più sfortunati. Lo scenario più probabile nel medio termine è che mi sia più facile trovare un riparatore sotto casa, che questo riparatore sia preparato e certificato, usi pezzi garantiti e sappia fare un lavoro a regola d’arte.

È una buona situazione. Ma qualcosa, nella catena dell’informazione, fatica a trasmettere il messaggio. Anche di giornalisti preparati e certificati ci sarebbe abbastanza bisogno, nonché capaci di valutare le informazioni e formulare una sintesi onesta.