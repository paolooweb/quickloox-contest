---
title: "Il fare della tranquillità"
date: 2019-10-28
comments: true
tags: [Keynote, iPhone, iPad, iOS]
---
Per la prima volta mi sono trovato a lavorare su una presentazione più curata dell’usuale su iPad Pro e l’esperienza è stata gratificante. Il novantacinque percento delle operazioni è più immediato, non manca niente di veramente importante rispetto all’opzione desktop.

Meglio ancora, l’ho tenuta in un ambiente impervio alla rete cellulare e privo di Wi-Fi. Nonostante questo (o forse proprio per questo, chissà), l’uso del vecchio iPhone come telecomando è andato liscio senza la minima esitazione; e non era mai stato esattamente così. Qualche volta era sparito il segnale a mezza strada, qualche volta era andato in crash Keynote su iPhone, qualche volta Bluetooth aveva esitato.

Stavolta niente del genere, ma soprattutto neanche il timore che succedesse; i due Keynote si sono parlati subito e non hanno più smesso. Né ho smesso io, arrivando in fondo con una tranquillità superiore alla (mia) media.

Non so dove ringraziare, tra hardware e software, ma quello che conta è l’insieme. Che mi ha lasciato soddisfatto.
