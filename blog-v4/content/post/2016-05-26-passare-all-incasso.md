---
title: "Passare all’incasso"
date: 2016-05-26
comments: true
tags: [Corriere, watch]
---
Si nota subito il titolo: *La clessidra del tempo*, come se ve fossero altre. (Varianti sul tema: *La bilancia del peso*, *La lampada della luce*, *La ruota che rotola*).

*Il settore, messo alle strette dalla crisi, si riscatta con ricerca e tecnologia.*

Veramente? L’inserto del *Corriere* è di settantadue pagine, delle quali *due* dedicate ai computer da polso. Uno di questi, nel 2015, ha venduto per sei miliardi di dollari. Rolex, per dire, [ne ha totalizzati quattro e mezzo](https://www.yahoo.com/tech/apple-watch-flop-outsold-rolex-1-5-billion-134852140.html).

Ho l’impressione che la crisi c’entri relativamente e che soprattutto certe tendenze di fondo siano rimaste largamente incomprese. Qualcuno, in quelle altre settanta pagine, passerà dall’incassare denaro a incassare mazzate e non esattamente per mano della crisi.

 ![La clessidra del tempo](/images/corriere-orologi.jpg  "La clessidra del tempo") 