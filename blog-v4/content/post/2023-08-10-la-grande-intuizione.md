---
title: "La grande intuizione"
date: 2023-08-10T01:40:15+01:00
draft: false
toc: false
comments: true
categories: [Internet]
tags: [digitale]
---
La storia umana del lavoro è una storia di masse, dai cacciatori-raccoglitori alla Rivoluzione industriale britannica al fordismo. Erano di massa persino le vacanze.

La tecnologia digitale ha portato, non sempre sinceramente e con criterio, la grande promessa della personalizzazione. L’uso acconcio della tecnologia digitale permette in potenza a ciascuno di avere un ambiente di lavoro e un flusso di produzione unico e su misura.

Ne segue che, dentro la tecnologia digitale, i comportamenti e le piattaforme che indulgono nella massificazione sono retaggi del passato e quelli che incoraggiano la personalizzazione vanno promossi, sostenuti, incoraggiati, nutriti.

Quello che vogliamo e non sappiamo, spesso, di volere è il poter raggiungere la personalizzazione in modo semplice, espandibile e supportato.

*Gli aggiornamenti in agosto del blog potrebbero essere irregolari o infrequenti.*