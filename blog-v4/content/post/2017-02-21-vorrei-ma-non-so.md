---
title: "Vorrei ma non so"
date: 2017-02-21
comments: true
tags: [Jobs, Ford, Zte, Hawkeye, Kickstarter]
---
Mi trovo bene con Apple quando mi propongono opportunità che non sapevo di volere. Non lo ha inventato Steve Jobs: avrò citato mille volte Henry Ford (che poi è frase apocrifa) con il suo *Se avessi chiesto ai miei clienti che cosa volevano, avrebbero risposto: un cavallo più veloce*.

Servisse una dimostrazione per negazione dell’opposto, Zte ha avuto una grande idea: chiedere ai clienti che cosa avrebbero voluto in un computer da tasca. Ok, uno *smartphone*.

La seconda grande idea è stata lanciare un Kickstarter di finanziamento del progetto per dare ai clienti più assidui la possibilità di essere i primi ad avere l’oggetto.

Kickstarter che è stato [cancellato](https://community.zteusa.com/community/csx/blog/2017/02/17/phasing-out-the-kickstarter-campaign) dopo avere raccolto trentaseimila dollari a fronte dei cinquecentomila richiesti.

Sintetizzo: gli affezionati clienti hanno detto che cosa volevano e, al momento di estrarre la carta di credito per averlo davvero, si sono dileguati.

Zte afferma che le critiche di chi non ha investito nel Kickstarter riguardano soprattutto le specifiche generali del terminale, giudicate troppo basse; il cliente desiderava qualcosa di più prestante e di conseguenza costoso.

Il cliente Zte? Qualcosa di più costoso? Faccio finta di crederci e aspetto di vederne uno.

Nel frattempo rifletto su quanti sfruttano ogni occasione di fare presente che non possiedono un iPhone perché manca di quello che dicono loro. Sanno quello che vogliono, mica sono gente confusa nella massa.
