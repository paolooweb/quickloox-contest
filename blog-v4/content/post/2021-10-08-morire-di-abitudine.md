---
title: "Morire di abitudine"
date: 2021-10-08T01:16:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [Kandji, Apple, Forrester, Mac, Jamf] 
---
Dopo lo [studio commissionato da Apple a Forrester sull’adozione dei Mac M1 in azienda](https://macintelligence.org/posts/Tre-anni-di-risparmi.html) eccone [un altro](https://info.kandji.io/hubfs/Ebooks/Employee%20Devices%20and%20the%20Hybrid%20Workforce.pdf), sponsorizzato da [Kandji](https://www.kandji.io), che è sostanzialmente concorrente di [Jamf](https://www.jamf.com) e vende soluzioni per facilitare la gestione nelle aziende di grandi quantità di Mac.

Lo studio mostra a suon di numeri che le persone al lavoro preferiscono scegliere che computer usare, che molto spesso hanno più piacere nell’usare un Mac, il quale ha un costo totale di proprietà inferiore a quello dei PC e in definitiva è più conveniente, oltre a essere più produttivo e a favorire il buonumore della forza lavoro.

Come potrebbe essere diversamente? L’oste scrive che il suo vino è buonissimo.

Intanto però che ci si accapiglia sul dito, osserviamo la luna. C’è un mercato, nel quale operano almeno due soggetti: concorrenza. Quindi è un mercato vero, dove è possibile crescere, avere successo e anche essere sfidati da un prodotto alternativo, che difficilmente verrebbe sviluppato in assenza di possibilità di emergere.

Mac in azienda non è una faccenda di pochi lunatici (o di quell’azienducola che porta a casa duecento miliardi l’anno), ma una realtà concreta, numerosa e in crescita.

Giusto per ricordarlo alle persone con cui chiacchieravo oggi, <em>Windows è lo standard</em>.

Semmai, l’abitudine. E fare sempre la stessa cosa, per la ragione che si è sempre fatta quella cosa, in un mondo come quello tecnologico, oltre a essere controproducente lo trovo persino di una noia mortale.

*Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile.*