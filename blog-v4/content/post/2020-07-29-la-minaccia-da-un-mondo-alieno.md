---
title: "La minaccia da un mondo alieno"
date: 2020-07-29
comments: true
tags: [Paolo, Cns, Reader, Adobe, DikeIC, Mac, Windows, Chrome, Spid, Cciaa]
---
Ogni tanto ci si chiede perché l’Italia faccia tutta questa fatica a diventare un Paese dove i servizi per il cittadino sono al servizio del cittadino.

La risposta è che per milioni di persone questa prospettiva è rovinosa come un’invasione di extraterrestri ostili e vogliosi di ridurli in schiavitù. Per loro l’idea di servizi che funzionano significa lavorare, acquisire nuove conoscenze, assumersi responsabilità, pensare al cliente invece che al proprio interesse.

Un esempio da manuale è l’esperienza che segue. Ringrazio **Paolo** per averla condivisa. Alcuni dettagli del post sono stati modificati per non renderlo identificabile.

§§§

**Rinnovo CNS per malfunzionamento**

Sulla tessera CNS andavano rinnovati due certificati in scadenza, dopo tre anni. Mi muovo un mese prima a seguire la procedura (semplicissima, via software apposito). Provo ovunque, con qualunque browser, con tutti i sistemi operativi: nisba, non si rinnovano. Chiamo l'assistenza. Mi chiedono di allegare file di log. Dopo 20 giorni (!) mi dicono che ho la tessera malfunzionante: va sostituita con una nuova.

Come da indicazioni datemi prendo appuntamento via web con l'ufficio di 09510 per il primo giorno disponibile (cioè dopo circa una settimana dopo la scadenza effettiva della carta…)

**Appuntamento Ufficio CCIAA 09510**

h14.15<br />
Modulo in entrata: dichiaro di non avere temperatura corporea oltre i 37,5°C. Firma (!).

Sono l'unico nell'ufficio: _prenda il biglietto dal totem e attenda lì la chiamata._

Prendo biglietto, mi siedo nell'ufficio vuoto, passano 30 secondi, mi chiamano (!).

Rendo il biglietto e noto che sotto c'è scritto: _alla fine potrà chiedere allo sportello il rilascio dello Spid, gratuitamente_. Io ho già lo SPID: vuoi vedere che posso fare tutto senza carta? No.

Carta vecchia restituita per malfunzionamento.

I dati sono ovviamente identici, ma lo stesso:

- richiesta carta di identità  (ok, sono io).
- richiesta tesserino codice fiscale! Ma se sono io e tutti i dati della vecchia tessera li avete già…
- signora batte alacremente (e lentamente) su tastiera ed emette un foglio A4 stampato.
- richiesta di controllare il foglio A4 stampato al momento con tutti i miei dati anagrafici (ma non li avevano già?): controllo bene, ok, ha scritto giusto.
- la stampante emette altri due fogli: devo firmare con una biro in cinque spazi diversi (solite cose, privacy eccetera. Ma i dati, ancora una volta, li avevano già…)
- signora batte alacremente su tastiera. Nuovi due fogli stampati.
- richiesta di controllare i miei dati… ma è UGUALE alla precedente, solo che adesso tutti i dati sono sottolineati (!).
- richiesta di… apporre le cinque firme identiche alle prime (!).
- graffetta tutto e mette via.
- stampa nuovo foglio (siamo al quinto foglio cartaceo) con scritto _sostituzione gratuita tessera_, e me lo danno.
- nuovo tesserino.
- imbustano tutto in custodia con PIN e PUK da grattare e mi restituiscono il tutto.
- ho chiesto all'inizio anche di aggiungere la firma remota (da una scrivania all'altra: _posso fargliela, Concetta_ (nome di fantasia…)_?_ _Sì, fagliela, tanto cosa ti costa?)_.
- mi rendono il tutto, acquisiscono e imbustano in cartellina trasparente tutta la copia della documentazione e la vecchia tessera.
- _Le arriverà una email con scritta tutta la procedura per l'attivazione della firma remota_. Vabbé. Ok.

h14:51<br />
(tempo totale di presenza fisica: 36 minuti, più spostamento agli uffici per fare una cosa di cui avevano già tutto: sarebbe bastato annullarmi da remoto la vecchia tessera e spedirmene una nuova identica e funzionante).

Attivazione:

- apro l'email, ok, c'è una email che dice di seguire un link. Inserisco tessera nel lettore, apro il link, pulsante inizio procedura, mi dice che si verifica un errore e non si può attivare la firma remota. Grrr.
- provo con la fima in locale: apro [DikeIC](https://www.card.infocamere.it/infocard/pub/download-software_5543), mi dice che i certificati vanno rinnovati (!) (era il motivo per cui sono andato a sostituirla…).
- guardo la sezione certificati: ci sono sia quelli vecchi scaduti, sia quelli nuovi, ma c'è anche un avviso che dice certificato ancora in scadenza… relativo a uno dei due certificati visibili nella sezione scaduti…
- infatti la tessera non va, nel senso che non firma.
- provo su Mac, vuoi vedere che se non è Windows… provo su Windows, cambio browser un paio di volte. Niente.
- riguardo i manuali di installazione, riscarico il software DikeIC… poi un'illuminazione: vuoi vedere che se elimino i vecchi certificati (possibilità data da DikeIC) funziona? Non c'è scritto da nessuna parte, se poi non va… provo.
- 
OK! Adesso la firma in locale funziona!

- riprovo su Mac l'attivazione della firma remota: dà errore.
- provo su Windows (stesso browser Chrome): sì! Mi permette di attivare la firma remota dopo avere inserito il codice tessera (e vabbé) e una ulteriore passphrase che mi devo inventare (l'ennesima password), più OTP sul cellulare.
- un messaggio dice di scaricare un Pdf CIFRATO, usare la passphrase per aprirlo e leggere le istruzioni che verranno date.
- ormai mi sembra di essere in una caccia al tesoro. Apro su Mac con anteprima e appare il lucchetto con richiesta della passphrase. Inserisco: errata. ?!
- vuoi vedere che… apro con Adobe Reader, lucchetto, passphrase… ok, appare il pdf! Cosa c'è scritto?
- dice di stampare (!) tutto e conservare con cura perché i tre codici riportati sono indispensabili ogni volta che si firmerà in remoto (cioè da una app che si deve scaricare sul cellulare o su altri dispositivi).
- stampo.
- provo tessera su macOS: ok, adesso tutto ok anche qui. Posso finalmente firmare, esattamente come prima.

- mi chiedo: ma chi l'ha inventata, 'sta procedura? Ha mai fatto una prova dall'inizio alla fine per vedere se c'è qualche (!) ridondanza nei passaggi?

- mi chiedo… ma lo SPID, non avrebbe già risolto tutto il problema dell'identificazione? Già, ma questi sono CCIAA, ente già di per sé inutile che ogni anno ti scuce più di 100 euro.

- mi chiedo ancora: ma senza la fantasia degli informatici, una normale segretaria non troppo esperta come avrebbe fatto?
