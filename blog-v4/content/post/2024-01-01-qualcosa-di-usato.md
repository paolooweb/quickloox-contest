---
title: "Qualcosa di usato"
date: 2024-01-01T01:55:56+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [Ifdb, Infocom, Lectrote, Electron]
---
Uno degli stereotipi sul Capodanno è che porti bene indossare qualcosa di usato. Lo interpreto in chiave software e, risolte le pratiche alimentari e augurali, consiglio a tutti per questa giornata una pesca miracolosa dentro [Ifdb](https://ifdb.org), il database delle avventure testuali contenente un autentico tesoro in fatto di fantasia e immaginazione. Dalle avventure Infocom a quelle di marchi meno nobili, per finire con appassionati e liberi creatori di avventure, scegliamo qualcosa di interessante e perdiamoci positivamente in un mondo parallelo, un bel massaggio per la mente.

Servirà un lettore per decodificare il formato originale delle avventure. Consiglio [Lectrote](https://github.com/erkyrath/lectrote/releases), nonostante sia una app Electron; è moderna, aggiornata, completa e – sembra, dopo un po’ che smanetto – stabile.

Suona scontato, per un 2024 all’insegna dell’avventura. Eppure.