---
title: "Scelte decisive"
date: 2020-10-21
comments: true
tags: [Mann, iPhone, Backblaze]
---
La nostra umanità intrinseca concede chiaramente un bel po’ di chance all’amico smanettone che ci dice la sua sui progressi fotografici di [iPhone 12](https://www.apple.com/it/iphone-12/). Dopo averci amabilmente chiacchierato, tuttavia, andrei a leggermi [la prova di Austin Mann](http://austinmann.com/trek/iphone-12-pro-camera-review-glacier), perché oltre a essere umani è anche necessario essere razionali.

(Bonus: a [Glacier National Park](https://www.nps.gov/glac/index.htm) ho avuto il privilegio di fare trekking per una settimana e, oltre che umani e razionali, ogni tanto è bello pure essere emotivi, e rivedere quei posti fotografati in quel modo è stato un tuffo al cuore).

La nostra ascendenza scimmiesca ci porta invece a dare retta all’amico smanettone che ci parla dei suoi dischi rigidi. Qui è solo questione di essere seri: mandiamo pure a monte un’amicizia se necessario, perché le cattive compagnie fanno male. L’unico modo di parlare di dischi rigidi senza regredire a uno stadio evoluzionistico precedente è fare riferimento al [report periodico di Backblaze](https://www.backblaze.com/blog/backblaze-hard-drive-stats-q3-2020/). Si possono leggere frasi come questa:

>Il Seagate da 14 terabyte (ST14000NM001G) [ha zero guasti] con 21.120 giorni/disco con 2.400 unità, ma è in funzione da meno di un mese. Il prossimo trimestre ci darà un quadro più chiaro.

Capito? Con duemilaquattrocento dischi in funzione, non hanno ancora dati sufficienti per dare un giudizio attendibile. Quando sento qualcuno che dà giudizi sul suo disco, potrei rispondere che l’altro giorno camminavo ai margini del bosco e ho incrociato una biscia, ma questo non mi autorizza a trarre conclusioni sulla fauna che l’italiano medio incontra camminando ai margini del bosco.

Per inciso, il parco totale di Backblaze supera i centocinquantamila dischi, per centosettantacinque milioni di giorni/disco.

Peggio ancora, nessun modello ha un tasso annuale di guasti superiore al 2,54 percento. Vuole dire che, su cento dischi, in un anno potrebbero guastarsene due o tre (trascuro la parte statistica delle deviazioni standard, se non lo dico [Sabino](https://melabit.wordpress.com) mi buca le gomme). Un modello che abbia il doppio dell’affidabilità, cioè un tasso annuale di guasti dell’1,27 percento, nelle stesse condizioni vedrebbe uno o due guasti invece che due o tre.

Sto facendola molto ortogonale e chiedo perdono, ma è per chiarezza: i due modelli hanno uno affidabilità doppia dell’altro, però magari capita che, su cento dischi in un anno, del modello affidabile se ne guastano due perché è andata male e del modello meno affidabile se ne guastano due perché è andata bene.

L’amico smanettone è quello che vede i duecento dischi in taverna, vede due dischi rotti di là e due di qua, e conclude *guarda, uno vale l’altro*.

Come? L’amico smanettone ha visto dieci dischi in tutta la sua vita, e tira conclusioni? Ah, beh.