---
title: "Che palloni"
date: 2013-01-17
comments: true
tags: [Windows, Mario, Multifinder]
---
Quando la biglietteria automatica del parcheggio del calcetto, pilotata da Windows, informa che il computer *potrebbe essere a rischio*. Di influenza, se piove.<!--more-->

Del resto, come <a href="http://multifinder.wordpress.com/2013/01/16/riflesso-condizionato/">riferisce Mario</a>, oramai i bambini sanno che ci vuole l’antivirus prima ancora di uscire di casa da soli. E qualcuno dovrà pure avere una responsabilità per questo squallore.
