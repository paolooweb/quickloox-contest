---
title: "Pensare in piccolo"
date: 2019-01-14
comments: true
tags: [watch, browser, WebKit]
---
Non ho pensato abbastanza in piccolo, quando [ho scartato l’idea di un *browser* su watch](https://macintelligence.org/posts/2015-05-13-compagnie-limitate/).

Non me ne ero accorto, infatti, ma watchOS 5 ha portato [WebKit](https://webkit.org), il motore Html che sta sotto Safari, [anche sugli watch](https://www.macrumors.com/how-to/browse-the-internet-watchos-5/) dalla serie 3 compresa in su. E nel giro di neanche tre mesi siamo già ai [consigli per ottenere il meglio con le immagini](https://ericportis.com/posts/2018/respimg-apple-watch/).

Certo, non c’è il *browser* vero e proprio, si potrebbe cavillare. Tuttavia è chiaro che Apple considera ragionevole presentare contenuto web su watch. In effetti, uno dei link sopra porta a un articolo di *MacRumors* che spiega come accedere a una pagina web arbitraria via computer da polso. A leggerla, effettivamente suona tutto ragionevole e avevo proprio sbagliato io.