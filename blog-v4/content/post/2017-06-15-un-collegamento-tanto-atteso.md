---
title: "Un collegamento tanto atteso"
date: 2017-06-15
comments: true
tags: [iWork, Jida, Numbers, Pages, Keynote, iOS, Mac]
---
Dovevo assolutamente notificare all’amico [Jida](https://www.facebook.com/andrea.jida.guida) che Pages [per iOS](https://9to5mac.com/2017/06/13/pages-keynote-numbers-ios-update/) e [per Mac](https://9to5mac.com/2017/06/13/apple-updates-iwork-apps-for-mac-with-new-shape-library-auto-correction-features-more/) ha riacquistato i riquadri di testo collegabili.

Sì, anche Pages per iOS. L’aggiornamento contiene cose interessanti anche sul piano della collaborazione e costituisce una ulteriore sorpresa aggiuntiva che nessuno aveva messo in conto, vista la messe di annunci a Wwdc.