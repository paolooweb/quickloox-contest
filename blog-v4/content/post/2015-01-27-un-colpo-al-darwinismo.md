---
title: "Un colpo al darwinismo"
date: 2015-01-28
comments: true
tags: [MPW, mpw, System1.1g, OSX, Terminale]
---
Sappiamo che Apple si cura poco di portarsi dietro il passato, la compatibilità, il software che dovrebbe girare all’infinito per chi decide che è venuto il tempo di smettere di aggiornare. L’ambiente Mac è decisamente evoluzionista e il codice che smette di essere adatto difficilmente sopravvive.<!--more-->

Mai mi sarei aspettato di trovare in rete la [compilazione per OS X](http://blog.steventroughtonsmith.com/post/109040361205/mpw-carbon-and-building-classic-mac-os-apps-in-os) di una app che avrebbe potuto funzionare su System 1.1g, anno 1984.

Ai tempi usai Macintosh Programmer’s Workshop. Sembrava di andare sulla Luna. Oggi, con il Terminale, pare un gioco da bambini. Allora era veramente magia.
