---
title: "Autentiche sorprese"
date: 2023-02-19T03:12:17+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [iOS, 2FA, autenticazione, Google Authenticator, Green, Matthew Green]
---
Ammetto che facevo parte del [gruppo degli inconsapevoli](https://twitter.com/matthew_d_green/status/1627360064527908865). In iOS è nascosto un autenticatore per i siti che applicano la 2FA, autenticazione a due fattori (metti la password e anche il codice che ti è arrivato a parte).

<blockquote class="twitter-tweet"><p lang="en" dir="ltr">Prior to this week, how many people were aware that Apple has a built-in 2FA authenticator code (TOTP) app that can be used in place of third-party apps like Google Authenticator.</p>&mdash; Matthew Green (@matthew_d_green) <a href="https://twitter.com/matthew_d_green/status/1627360064527908865?ref_src=twsrc%5Etfw">February 19, 2023</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

E sì che Cnet [lo aveva mostrato](https://www.cnet.com/tech/mobile/you-should-be-using-your-iphones-built-in-two-factor-authentication-heres-why/) già mesi fa. La tentazione di buttare alle ortiche l’autenticatore attuale è forte, anche se prima bisogna verificare che tutti i siti da coprire funzionino.

Da capire anche se le autenticazioni vengano via insieme al backup cifrato su disco. Sarebbe una gran comodità.