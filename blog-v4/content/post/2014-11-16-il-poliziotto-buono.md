---
title: "Il poliziotto buono"
date: 2014-11-16
comments: true
tags: [Android, iPadAir2, AndroidPolice, Lollipop]
---
Proprio bello e interessante, di profondità altrimenti difficile da reperire, il confronto tra iPad Air 2 e Nexus 9 pubblicato da, nientepopodimenoche, [Android Police](http://www.androidpolice.com/2014/11/11/nexus-9-vs-ipad-air-2-a-mostly-subjective-comparison/).<!--more-->

L’autore ha parlato delle sue impressioni soggettive e eseguito le sue valutazione sulla base di competenze e interessi personali, senza pretendere di scrivere una recensione classica e senza impantanarsi in campi che non sono i suoi. Come risultato abbiamo un pezzo sincero e profondo, molto lungo e mai annacquato, che abbina alle opinioni anche capacità di analisi e un tempo di prova concreta più che adeguato. Sulle opinioni è difficile andare d’accordo, specie se uno mette tra i *contro* di iPad Air 2 una motivazione come questa:

>È un iPad e come risultato qualcuno penserà che siete sicofanti di Apple; il genere di persona che bazzica coffee shop per otto ore al giorno.

Eppure si apprezzano l’onestà intellettuale e il candore adulto. Il pezzo si astiene dal cercare di spacciare opinioni come fatti e allora le opinioni si possono solamente apprezzare, comunque.

Al termine dell’analisi condotta su progettazione, qualità costruttiva, schermo, audio, autonomia, digitazione, visione filmati, multitasking, navigazione, documenti, fogli di calcolo, presentazioni, posta, mappe, *app*, contenuti a disposizione, esperienza di acquisto, condivisione tra *app*, usabilità, giochi, stabilità e esperienza utente, arrivano le conclusioni:

>Non mi pare che i 600 dollari per iPad Air 2 (non potevo resistere ai 64 gigabyte) siano una cifra fantastica per quello che si ottiene, però non butterei mai via 480 dollari per un Nexus 9, specie se tutto quello che si vuole è che faccia funzionare Lollipop [l’ultima versione di Android] […] Intanto iPad Air 2, francamente, è lo standard di riferimento per le tavolette. […] ha probabilmente la migliore architettura di processore sul mercato e uno schermo notevole. Ha le sue debolezze, che i punti di forza compensano a mani basse. […] Non è una cosa per tutti, ma dico che iPad rimane la tavoletta da battere per la maggior parte delle persone che si pongono il problema, me incluso.

Non si pensi che il confronto sia un peana per iPad Air 2, tutt’altro. Siamo su *Android Police*. Peraltro, dovessi consigliare un testo da leggere per farsi un’opinione informata, sarebbe questo.