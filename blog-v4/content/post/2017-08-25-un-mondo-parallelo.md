---
title: "Un mondo parallelo"
date: 2017-08-25
comments: true
tags: [Windows, Baden-Württemberg, Microsft, ReFS, Sasha]
---
A seguito delle discussioni sugli [aggiornamenti non richiesti a Windows 10](https://macintelligence.org/posts/2017-02-13-o-paghi-o-ti-aggiorno/), l’autorità di tutela dei consumatori del Baden-Württemberg in Germania ha ingaggiato una battaglia legale con Microsoft. Dopo diciotto mesi di cause e due verdetti sfavorevoli, Microsoft [ha rinunciato a proseguire la lite in tribunale](https://www.verbraucherzentrale-bawue.de/windows-update--microsoft-gibt-unterlassungserklaerung-ab-) e ha annunciato (solo là) che *non eseguirà più aggiornamenti di sistema operativo senza il consenso dell’utente*.

Prego di rileggere la parte in corsivo e rileggerla ancora. Implica che il consenso dell’utente è o è stato irrilevante nelle valutazioni di Microsoft e che l’azienda si riserva o si è riservata di fare quello che le pare con i computer degli utenti del suo sistema operativo, il quale secondo vari sostenitori consente a ciascuno di fare ciò che desidera in libertà, al contrario di quello che accade su altre piattaforme.

D’altronde si parla dell’azienda che crea un nuovo sistema operativo evoluto, ReFS, e poi (come mi faceva notare **Sasha**, che ringrazio) [elimina la funzione di creazione di dischi ReFS](https://support.microsoft.com/en-us/help/4034825/features-that-are-removed-or-deprecated-in-windows-10-fall-creators-up) dalle edizioni più diffuse di Windows, per lasciarla sulle edizioni che costano di più.

<blockquote class="twitter-tweet" data-lang="en"><p lang="en" dir="ltr">ReFS creation ability will be available in the following editions only: Windows 10 Enterprise and Windows 10 Pro for Workstations <a href="https://t.co/9oKhTpZ7YQ">pic.twitter.com/9oKhTpZ7YQ</a></p>&mdash; Tero Alhonen (@teroalhonen) <a href="https://twitter.com/teroalhonen/status/899525761333432320">August 21, 2017</a></blockquote> <script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>

A me sembrano disservizi e notevoli. Però forse vivo in un mondo parallelo.