---
title: "L’editor scodinzolante"
date: 2022-07-02T01:07:58+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [BBEdit, Dr. Drang, Drang, Tail Mode, AppleScript, Terminale, zsh]
---
Il Terminale possiede ovviamente un modo per seguire le aggiunte in fondo a un file che viene aggiornato automaticamente con buona frequenza, per esempio un file di log. Ma è il Terminale, ci mancherebbe. Roba per stomaci forti e dita intrepide.

Quale editor di testo permette di fare la stessa cosa?

BBEdit 14.5, per esempio, con il suo [Tail Mode](https://www.barebones.com/support/bbedit/notes-14.5.html). Il link porta in realtà all’elenco di tutte le novità nella versione 14.5 uscita da poco, Tail Mode compreso.

Dr. Drang connette i mondi culturali degli editor di testo e del Terminale con [un gioiello di post](https://leancrew.com/all-this/2022/06/bbedit-tail-mode-and-bbtail/) che illustra in breve Tail Mode e poi permette di azionarlo su BBEdit da Terminale invece che applicazione, con un misto di shell zsh e di AppleScript.

Sentirsi parte di una comunità come questa è entusiasmante, anche quando l’unica cosa che si può veramente condividere con certe teste è il desiderio di imparare.