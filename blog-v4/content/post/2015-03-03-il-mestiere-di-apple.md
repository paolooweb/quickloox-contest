---
title: "Il nuovo Think Different"
date: 2015-03-03
comments: true
tags: [watch, Apple, iPad, netbook, ThinkDifferent, OurSignature, Ford, Jobs]
---
C’è poco da scherzare: Apple c’è dal 1976, ma gran parte del pubblico non ha capito in che direzioni si muove e soprattutto perché. Bravo Horace Dediu di Asymco con la propria [guida ad Apple per analisti](http://www.asymco.com/2015/03/02/the-analysts-guide-to-apple/). (Che poi è una maschera, perché gli analisti non capiscono, ma vabbè).<!--more-->	

• *Apple lavorerà in direzioni dove ritiene di poter dare un contributo significativo*. Qualcuno ricorderà il baccano di tanti anni fa perché, invece del *netbook* di moda, Apple tirò fuori iPad. Il motivo era che non c’era niente da contribuire. Difatti oggi il *netbook* è storia.

• *Apple lavorerà su poche cose*. Il catalogo dei prodotti Apple è aumentato sensibilmente negli ultimi tempi e sta per arrivare anche watch eppure, per usare una fortunata espressione di Tim Cook, tutti i prodotti Apple stanno su un tavolo da cucina.

• *Apple lavorerà su cose che sono inevitabili anche se nessuno le ha chieste*. Difficile da digerire, ma resta vero ciò che disse Henry Ford: *se avessi chiesto ai miei clienti che cosa volevano, mi avrebbero risposto “un cavallo più veloce”*. L’innovazione non si fa sulle scrivanie, ma nei laboratori.

Dediu cita un [video](https://www.youtube.com/watch?v=loL77XU2Rzg&spfreload=10) che ha avuto meno fortuna di [Think Different](https://www.youtube.com/watch?v=nmwXdGm89Tk) ma potrebbe benissimo esserne un successore. Provo a tradurlo.

*È questo. È questo ciò che conta*

*L’esperienza di un prodotto*<br />
*Come farà sentire qualcuno*<br />
*Migliorerà la vita?*<br />
*Merita di esistere?*

*Passiamo un sacco di tempo su poche grandi cose*<br />
*Fino a quanto ogni idea che tocchiamo*<br />
*Migliora ogni vita che tocca*

*Potrai farci poca attenzione*<br />
*Ma ne sentirai sempre la presenza*<br />
*Questa è la nostra firma*<br />
*E significa tutto.*

<iframe width="600" height="338" src="https://www.youtube.com/embed/loL77XU2Rzg" frameborder="0" allowfullscreen></iframe>