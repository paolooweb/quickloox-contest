---
title: "Leggere il passato"
date: 2016-05-30
comments: true
tags: [ZesarUX, Spectrum, emulatore, retrocomputing]
---
Ogni tanto sento preoccupazioni per come le informazioni digitali che creiamo verranno preservate per i posteri, e come saranno leggibili se mancheranno le macchine originali che le hanno create eccetera.

Poi vedo [ZEsarUX](https://sourceforge.net/projects/zesarux/). Non l’ho ancora provato estesamente, ma una scorsa alle specifiche lo presenta come l’emulatore Spectrum più completo e versatile che sia uscito finora. Dire *Spectrum* è pure riduttivo, dato che il numero delle macchine effettivamente emulate va ben oltre.

ZEsarUX funziona ovunque, persino su [Raspberry Pi](https://www.raspberrypi.org), un computer da cinque dollari i cui piani di costruzione sono pubblici. Ha a disposizione una platea potenziale approssimativa di un miliardo di computer. Ovvero, l’emulatore Spectrum potrebbe essere teoricamente essere duecento volte più diffuso dello Spectrum originale, che vendette [cinque milioni di unità](https://en.wikipedia.org/wiki/ZX_Spectrum). Ovviamente l’emulatore esegue qualsiasi programma creato per Spectrum. Arrivare a un enorme [archivio di programmi](http://www.worldofspectrum.org) per Spectrum costa a chiunque una veloce ricerca su Google e niente più.

Come proprietario e assiduo utilizzatore di ZX Spectrum, sono interessato al lato retroinformatico della faccenda e mi piace moltissimo trovare esemplari di Spectrum ancora in funzione. Ma sono rarità; in una casa tipica di oggi, posato uno Spectrum funzionante davanti ai proprietari, mancano le tecnologie di base per farlo funzionare. Mentre in tutte le case tipiche di oggi è presente la tecnologia per fare funzionare l’emulatore.

Tradotto: il software per Spectrum è stato brillantemente conservato e i programmi scritti trent’anni or sono sono a piena disposizione di chiunque li voglia eseguire. Comprese persone adulte e vaccinate che quando c’era lo Spectrum nei negozi, neanche erano nate.

Le preoccupazioni mi sembrano allora eccessive, o frutto di timore disinformato.