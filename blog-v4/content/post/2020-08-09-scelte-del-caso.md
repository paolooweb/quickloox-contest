---
title: "Scelte del caso"
date: 2020-08-09
comments: true
tags: [InternetTemple, YouHole, Ars, Technica, Collins, Alden]
---
Sempre nello spirito dello *slow news cycle* mi piace ricordare i tempi in cui Internet era un posto dove si scoprivano le cose e si potevano scoprire cose che non ci si sarebbe mai immaginati.

Molto tempo fa esisteva un sito chiamato URoulette il cui scopo era spedire il visitatore a un altro sito, scelto a caso. Ci avevo costruito sopra una rubrica e il piacere di scriverla era niente rispetto al gusto dell’ignoto.

Oggi Ars Technica dedica spazio a due pazzi di Portland, Clayton Collins e Toby Alden, che in nome della *performance* artistica [costruiscono siti molto vicini a quello spirito](https://arstechnica.com/gaming/2020/08/diy-site-builders-battle-algorithmic-feeds-with-sheep-filled-wackiness/).

Per esempio [Internet Temple](https://arstechnica.com/gaming/2020/08/diy-site-builders-battle-algorithmic-feeds-with-sheep-filled-wackiness/), una chat vecchio stile dedicata alla scoperta di musica alternativa; oppure [YouHole](http://youhole.tv), che trasmette video presenti su YouTube scelti randomicamente tra quelli che hanno meno di cinquecento visualizzazioni.

Il rischio di trovare contenuti scomodi o non condivisi è alto, ma anche la *chance* di imbattersi in qualcosa di inaspettato. Se c’è qualcosa che l’epoca dei social ci ha levato è il fascino della scoperta inattesa, serendipitica. Andrebbe ritrovato.