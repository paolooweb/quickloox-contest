---
title: "Comandi da esportazione"
date: 2021-11-27T01:15:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [Mac, Federico Viticci, MacStories, Shortcuts, Comandi rapidi, Safari] 
---
Se i Comandi rapidi su Mac, arrivati in un metaforico altroieri, consentono a Federico Viticci di [esportare senza scripting aggiuntivo i link dei siti da leggere memorizzati in Safari](https://www.macstories.net), vuol dire che c’è del buono.

Forse è la volta buona che lo scripting su Mac, oltre a esserci, riceve attenzioni serie dagli sviluppatori interni ad Apple. Vedere una tecnologia come questa fiorire e affermarsi può solo fare bene persino agli scettici, quelli che brontolano ma poi, dei comandi utili, approfittano per primi.

_Per ora si può lasciare un commento dalla [pagina apposita di Muut per Qui
ckLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile._