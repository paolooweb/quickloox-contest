---
title: "So long, John"
date: 2015-04-18
comments: true
tags: [Siracusa, OSX, Schiller]
---
[Non appariranno più](http://hypercritical.co/2015/04/15/os-x-reviewed) nuove, epiche recensioni di OS X firmate John Siracusa.

Phil Schiller, mente agile, ha già inquadrato la situazione.

<blockquote class="twitter-tweet" lang="en"><p>John&#39;s OS X reviews = Legendary&#10;<a href="http://t.co/9gBbjMJFvW">http://t.co/9gBbjMJFvW</a>&#10;<a href="https://twitter.com/siracusa">@siracusa</a></p>&mdash; Philip Schiller (@pschiller) <a href="https://twitter.com/pschiller/status/588812454823645185">April 16, 2015</a></blockquote> <script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>

È una grossa perdita. Uno dei pochi momenti dove davvero si imparava qualcosa di inedito bene e in fretta sul web. Siracusa è l’unico che qualche volta ho citato senza verifiche, perché si poteva stare matematicamente certi che sarebbe andata come scriveva lui.

A noi maldestri allievi, ora tocca studiare. Grazie di tutto, John.