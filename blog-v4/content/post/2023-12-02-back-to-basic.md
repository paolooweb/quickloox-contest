---
title: "Back to Basic"
date: 2023-12-02T22:18:27+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [BBC Basic, Acorn, BBC Micro Model A, BBC Micro, Model A]
---
Attenzione, rischio produttività, serio. Il mondo si è accorto che, quasi quarant’anni dopo, qualcuno si è rimesso a sviluppare BBC Basic, una reliquia assoluta che equipaggiava i computer [BBC Micro Model A](https://www.computinghistory.org.uk/det/3041/acorn-bbc-micro-model-a/) di Acorn e, dal 2019 a oggi, il linguaggio [BBC Basic](https://www.bbcbasic.co.uk/bbcsdl/) è tornato in vita scintillante, moderno e incredibile.

BBC Basic oggi funziona su Mac, Windows e Linux ma anche su Raspberry Pi, Android, iOS e, non bastasse, nel browser. Tutto open source, tutto aperto e libero.

Le versioni applicative sanno maneggiare fino a duecentocinquantasei megabyte di Ram, per dire come siano cambiate le cose dal 1982 rispetto al 2023. Di sicuro questa versione non è un giocattolo né un’anatra zoppa e neppure una sfida da hacker che domani sarà già dimenticata.

Agli italiani, ricordiamo che BBC Basic era nato su computer BBC Micro e si chiamava tutto BBC perché l’ente radiotelevisivo inglese era il motore di tutta l’operazione. E in televisione, quella nazionale, passavano lezioni di programmazione al computer.

Quando capiremo?