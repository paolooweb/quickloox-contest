---
title: "Te la do io l’obsolescenza"
date: 2014-10-04
comments: true
tags: [Visicalc, 1-2-3, Lotus, Ibm, AppleII, Mac]
---
Dedicato a chi trova eccessivo il ciclo annuale di rinnovo del software, oppure scarsa una compatibilità di cinque anni tra hardware e software, o ancora straparla di [obsolescenza programmata](https://macintelligence.org/posts/2014-06-04-obsolescenza-sprogrammata/): [Ibm ha abbandonato il supporto di 1-2-3](http://www-01.ibm.com/common/ssi/cgi-bin/ssialias?subtype=ca&infotype=an&appname=iSource&supplier=897&letternum=ENUS913-091) dopo *trent’anni*.<!--more-->

Metà del mondo non era ancora nata, quando nasceva 1-2-3. Altri tempi. Anche se su Mac contava poco o niente e su Apple II la marcia in più la diede [Visicalc](http://www.bricklin.com/visicalc.htm).