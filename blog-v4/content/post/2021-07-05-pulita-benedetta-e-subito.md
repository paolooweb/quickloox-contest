---
title: "Pulita, benedetta e subito"
date: 2021-07-05T00:24:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [Erica Sadun, Dr. Drang] 
---
Non mi ero reso conto prima di quanto sia preciso, conciso e chiaro il [blog di Erica Sadun](https://ericasadun.com) e del beneficio che può portare a una persona interessata allo scripting.

Erica va dritta al punto con estrema linearità e sintesi. Inoltre ha una facilità soprannaturale nel variare il taglio degli interventi. Scorrere la pagina home per una dozzina di post ha quasi il valore di un corso.

Un esempio: [controllare la condivisione schermo dalla riga di comando](https://ericasadun.com/2021/05/13/controlling-screen-sharing-from-the-command-line/). O anche [creare un servizio di conteggio parole](https://ericasadun.com/2021/04/14/crafting-a-custom-word-count-service/), sul quale Dr. Drang [ha ricamato da par suo](https://leancrew.com/all-this/), aggiungendo un sacco di migliorie e rifiniture.

Rimane il fatto che la soluzione saduniana, per quanto sbrigativa e artigianale, era funzionale. Drang l’ha levigata e lucidata, ma era già in essere.

Nella mia raccolta di feed RSS ora ne compare uno in più. Una come Erica ci vuole.

*Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile.*