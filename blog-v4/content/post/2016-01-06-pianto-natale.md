---
title: "Pianto Natale"
date: 2016-01-06
comments: true
tags: [ iPad, Jurassic, World, in-app, Mohamed, Faissal, Shugaa, Apple]
---
Apprendo dal britannico Metro che Faisall Shugaa, sette anni, ha speso quattromila sterline con l’iPad del papà Mohamed giocando, meglio, [comprando in-app dentro il gioco Jurassic World](http://metro.co.uk/2015/12/30/boy-7-racks-up-4000-on-dads-ipad-playing-itunes-game-5592453/).<!--more-->

La seconda notizia che si legge è che Faisall conosceva il codice di sblocco numerico impostato dal papà e, non pago (mai aggettivo calzò tanto a pennello), anche il relativo ID Apple.

Due livelli di sicurezza che per un bambino dovrebbero essere insuperabili per tutta una serie di motivi che arrivano anche molto prima dei soldi, e invece.

Le cose cominciano ad apparire strane quando si legge che l’acquisto ha richiesto cinque giorni, dal 13 al 18 dicembre, durante il quale la banca di Mohamed ha registrato 65 transazioni con Apple.

Per semplicità supponiamo parità di cambio tra sterlina ed euro. Il singolo acquisto in-app più costoso è di cinquanta euro. Qualsiasi cosa abbia fatto Faisall, quattromila diviso cinquanta, lo ha fatto almeno ottanta volte, quindi circa quindici volte al giorno. Un acquisto ogni quaranta minuti ipotizzando un utilizzo di otto ore.

Mohamed, che ha dato al vento un codice di sblocco e una password privata e ha lasciato un bambino a smanettare per sei giorni su iPad da mattina a sera senza alcun controllo, è furioso:

>Ho trentadue anni, come può Apple credere che mi metterei a spendere migliaia di sterline comprando dinosauri dentro un gioco? Perché non mi hanno scritto una email per capire se sapevo di questi pagamenti? Quanto doveva durare?

Interessante anche sapere perché la banca di Mohamed non abbia battuto ciglio, sicuro. E i programmatori del gioco. E gli amici di Faissal. Le mamme, i vicini, il consiglio di quartiere, il sindaco, la società civile, tutti quelli interessati a sapere come se la cava un bimbo di sette anni, escluso il padre.

Che è stato rimborsato. Secondo *Metro*, Apple sarebbe stata convinta dalla motivazione che il denaro serviva per comprare i regali di Natale ai due figli. Ci credo poco.

Buon Natale, retroattivo, anche a Mohamed e famiglia. A Napoli comunque avrebbero commentato [chiagne e fotte](https://it.wikipedia.org/wiki/Chiagni_e_fotti).
