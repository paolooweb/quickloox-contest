---
title: "Consigli per la manutenzione"
date: 2022-03-11T00:57:48+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [BBEdit, macOS, Terminale]
---
Casomai succedesse che [BBEdit](https://www.barebones.com/products/bbedit/) si fermasse in eterno su una finestra di dialogo che dice *Waiting for macOS*, si sappia che c’è un malfunzionamento del sottosistema di gestione dei font.

Per rimediare, occorre chiudere BBEdit, aspettare almeno un minuto, lanciare il Terminale e digitare i tre comandi che seguono:

`atsutil server -shutdown`

`sudo atsutil databases -remove`

`atsutil server -ping`

Poi si riavvia e infine si rilancia BBEdit, che auspicabilmente tornerà a fare il proprio dovere.

I seguenti consigli sono forniti dal supporto di Bare Bones, forse il migliore che abbia mai sperimentato più di una volta, sicuramente sul podio fra tutti.

Casomai.