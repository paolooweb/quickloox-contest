---
title: "Natale informatico"
date: 2013-12-26
comments: true
tags: [Arduino, Furby, Kindle, iPad, iPhone, Galaxy, Samsung]
---
Ho visto passare sotto l’albero – non per me – disco rigido portatile autoalimentato, chiavetta Usb, scheda SD, batteria supplementare, Kindle, fotocamera digitale, album in regalo via iTunes, *smartphone*, Arduino Uno e libro su Arduino.<!--more-->

Presenziavano oltre a parenti e affini tre iPad, quattro o cinque iPhone, un Samsung Galaxy e perfino un Furby con relativa *app*.

Sarebbe stato impensabile anche solo cinque anni fa. Siamo su un mondo al galoppo.