---
title: "Fatta la tara"
date: 2014-06-24
comments: true
tags: [SurfacePro, MacBookAir, MacBook, Surface, Microsoft]
---
Microsoft si è premurata di presentare [Surface Pro 3](http://www.microsoft.com/surface/it-it) come *il tablet che costituisce il portatile*. Portatile [graziosamente identificato](http://9to5mac.com/2014/05/20/microsoft-announces-12-inch-surface-pro-3-wants-to-replace-your-ipad-macbook/) nella figura di [MacBook Air](http://www.apple.com/it/macbook-air/).<!--more-->

Gli eventi dell’ultima ora impongono una correzione: Surface Pro è il tablet che può sostituire il portatile, [meno la valutazione del portatile da 150 a 650 dollari](http://content.microsoftstore.com/en-us/offers/prudentialcenter#offer-surface-pro-3-trade-in). Senza i quali, sembra, la proposta di vendita sia meno attraente.

Seicentocinquanta dollari che fanno da tara al valore netto di Surface Pro 3. E si è detto tutto.