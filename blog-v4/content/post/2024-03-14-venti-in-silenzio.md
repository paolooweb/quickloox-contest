---
title: "Venti in silenzio"
date: 2024-03-14T01:26:32+01:00
draft: false
toc: false
comments: true
categories: [Internet]
tags: [TextPattern, Dean Allen, Allen]
---
Nel perfetto stile che aveva il suo creatore [Dean Allen](https://en.wikipedia.org/wiki/Dean_Allen), un genio che ci ha lasciati troppo presto, [TextPattern ha compiuto vent’anni](https://textpattern.com/weblog/textpattern-turns-twenty).

Lo sviluppo di TextPattern è fortunatamente proseguito e ancora più fortunatamente seguendo il modello dei suo fondatore, senza proclami, senza versioni dal nome roboante, senza campagne pubblicitarie, solo il lavoro di sviluppo e crescita del sistema.

In un mondo funzionante secondo regole logiche, TextPattern dovrebbe essere citato in qualsiasi discussione sulla scelta per il motore di un sito. Non è la mia tazza di tè perché in questo momento storico siedo saldamente nel campo dei motori statici e lui è dinamico, ma c’è stato un momento tanti anni fa in cui avevo seguito la sua avventura con lo hosting di TextDrive e pensavo seriamente di adottare TextPattern per pubblicare.

Essendo dinamico, TextPattern è da confrontare con WordPress, per forza. Ebbene, fa praticamente tutto meglio di WordPress. È più veloce, più ordinato, più pulito, più comprensibile, più semplice ma certo non meno dotato. Ci sono i temi e i plugin, molti meno come si può immaginare; però funzionano.

TextPattern è una eccellente alternativa a WordPress per fare più in fretta, capire meglio che cosa si si sta facendo, andare veloci, fare la cosa più vicina a quella che si aveva in mente. Certo, manca il caos da cui partorire stelle danzanti, manca il mercato in cui “scegliere” tra migliaia di plugin come fosse un scelta, manca la *suspence* di quanto lavoro servirà per fare funzionare il blob che gli sviluppatori birmani hanno coraggiosamente chiamato *tema*. Le regole dell’interfaccia umana non vengono reinventate secondo i gusti di chi passa.

TextPattern è decisamente meno divertente di WordPress e va sconsigliato a chi ama l’esistenza avventurosa. Invece, per qualcuno con delle cose da dire e una moderata capacità tecnica di mettere insieme un accrocchio per dirle, potrebbe liberare del tempo. Perché quello che deve fare lo fa e quello che va impostato è impostabile anche da un essere umano.