---
title: "Il pagamento degli arretrati"
date: 2021-03-09T01:36:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [LaserWriter, desktop publishing, PageMaker, Big Sur, Catalina] 
---
Si sa che la tecnologia ha un suo [ciclo di adozione](https://en.wikipedia.org/wiki/Technology_adoption_life_cycle) e vorrei dire qualcosa a proposito delle stampanti.

Nel 1985 Apple presentò [LaserWriter](https://apple.fandom.com/wiki/LaserWriter) e diede inizio al desktop publishing. La stampante cessava di essere un accessorio funzionale per stampare bozze, etichette e tabulati; diventava uno strumento di emancipazione. Consentiva di esprimersi e persino di avviare piccole imprese e carriere professionali (la zona degli innovatori nella curva del ciclo di adozione). La prima versione di [PageMaker](https://en.wikipedia.org/wiki/Adobe_PageMaker) chiudeva il cerchio, nel consentire al contenuto digitale di avere anche una forma in stampa che poteva essere diversa dal semplice flusso di dati (il segmento *early adopter*).

Da lì in avanti una stampante laser arrivò in tutti gli uffici (early majority) e successivamente anche nelle case (late majority) che la preferivano al più tradizionale modello a getto di inchiostro.

A quel punto iniziò anche lo spostamento verso la digitalizzazione totale; una stampante, in verità, serviva sempre meno. Apparivano gli aneddoti di gente un po’ indietro nella curva di adozione mentale, che stampava la posta elettronica, cose così.

Quando succedono queste cose significa che la curva del ciclo di adozione è arrivata in fondo, ai *laggard*, gli ultimi, quelli che hanno resistito veramente fino alla fine.

Dopo avere posseduto vari modelli di stampante a getto di inchiostro, da molti anni ne ho fatto a meno; le situazioni professionali in cui serve stampare, nel mio caso, si sono azzerate.

Ma la serrata delle scuole a causa della terza ondata del virus mi farà comprare una piccola stampante laser, poiché le maestre assegnano lavori da *stampare e incollare sul quaderno*. A mio giudizio si tratta di arretratezza; quello che è creatività e libera espressione, i bambini possono e devono farlo a mano libera. Quello che è modulistica travestita da compito – completa la parola, trova l’intruso, esegui le operazioni – si dovrebbe fare in digitale. Stampare è un vicolo cieco, una dimenticanza nel camminare tutti insieme verso il progresso. Ma tant’è.

Così sono il più *laggard* dei *laggard* mentre consulto il web per selezionare una stampante laser che possa togliere di impaccio la famiglia quelle volte che ci si misura con le arretratezze della scuola. Per modelli semplici, adatti alla situazione, la spesa è una preoccupazione modesta… molto più modesta della preoccupazione della compatibilità.

Con Mac? No, quei tempi tristi sono cambiati per sempre, a parte qualche ufficio dove ancora azienda e dipendenti sono ostaggi della loro chiusura mentale.

Con *Big Sur*. Che una stampante laser normalmente sul mercato sia compatibile con macOS 11.x, non è affatto scontato. Devi guardare con attenzione e approfondire sempre, dato che è un attimo scoprire modelli fermi a Catalina anche se non sembrava.

Torno al ciclo di adozione della tecnologia. Quando siamo ai *laggard*, o perfino oltre, che la curva ce la dobbiamo dissotterrare da soli da quanto si è azzerata, non vuol dire che la tecnologia sia matura; significa che nemmeno si tratta più di tecnologia. Sono oggetti scontati, in vendita in modo scontato per situazioni scontate e non sto parlando dei prezzi. Oggi una stampante si compra un po’ come un asciugacapelli; lo accendi e funziona. Non c’è più nulla da scoprire, a parte qualche funzione esoterica di differenziazione.

E allora il driver per il sistema uscito un quadrimestre fa, annunciato due quadrimestri fa, ci deve essere.

Altrimenti, arretratezza per arretratezza, si potrebbe fare che compro la stampante, ma pago quando arriva il driver per il mio sistema operativo.

*Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile.*