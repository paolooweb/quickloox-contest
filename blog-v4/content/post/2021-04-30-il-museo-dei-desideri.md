---
title: "Il museo dei desideri"
date: 2021-04-30T14:31:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [All About Apple, Wishraiser] 
---
C’è un gran parlare di riaperture e continuo a pensare alla prossima occasione di mettermi in volo, al galoppo, sui pedali, spinto dalle onde verso Savona, dove sta il museo All About Apple.

Gli amici che hanno dato vita al museo sono persone splendide e indomabili, che negli anni hanno attraversato più di una peripezia per tenerlo aperto e funzionante. Il virus, ovviamente, è una peripezia pesante anche per All About Apple, che è una Onlus e si mantiene grazie al sostegno delle persone che riconoscono il valore di un pezzo di storia e di cultura irripetibile.

*Aperto* vuol dire che è una vera sede, con un vero allestimento, con percorsi guidati. Non si chiama museo giusto per darsi un tono. Anzi, forse dovrebbe ribattezzarsi centro culturale, o esposizione permanente. Perché *funzionante* significa che le macchine sono accese, si possono toccare, sperimentare, vivere. Niente imbalsamazioni, niente cadaveri.

Siccome il museo vive nel presente, inventa, si muove, propone cose. L’ultimissima è diventare membri donatori di All About Apple *e anche* rischiare di [vincere viaggi con Wishraiser](https://www.wishraiser.com/it/memberships/all-about-apple-onlus).

Così poi succede che faccio di tutto per arrivare a Savona e poi mi tocca pure ripartire per la Versilia o Sorrento o il Sestrière o quel che è.

D’altronde, quello che la vita ci mette davanti, va affrontato. E se si riapre, facciamolo con gusto.

*Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile.*