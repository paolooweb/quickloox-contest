---
title: "Video in programma"
date: 2019-01-29
comments: true
tags: [iPad, iPhone, XR, Swift, Jobs]
---
Pare che Apple si stia divertendo con il tema del video interpretato via iOS, con risultati quali la serie *fai questo e quello con iPad Pro* (compreso un [video del backstage](https://www.youtube.com/watch?v=I3iVR3qWE3s), non sappiamo se girato a sua volta con iPad Pro)…

<iframe width="560" height="315" src="https://www.youtube.com/embed/I3iVR3qWE3s" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

…e [video sperimentali](https://www.macrumors.com/2019/01/28/apple-iphone-xr-experimental-video/) girati con accrocchi per avere a disposizione trentadue riprese dello stesso soggetto da parte di altrettanti iPhone Xr, disposti a trecentosessanta gradi.

Alla deriva creativa dei contenuti non corrisponde però una minore cura delle piattaforme: *9to5Mac* relaziona rispetto ai [miglioramenti nell’ingombro delle app](https://9to5mac.com/2019/01/28/apple-swift-5-update-ios-12-2/) che si otterranno nella prossima versione 5 del linguaggio di programmazione [Swift](https://developer.apple.com/swift/), grazie al raggiungimento della compatibilità binaria tra versioni differenti dopo anni nei quali la priorità era la crescita del linguaggio stesso.

Video sperimentali, linguaggi di programmazione evoluti… viene in mente il momento in cui Steve Jobs parlò di Apple [situata all’incrocio tra tecnologia e arti liberali](https://www.youtube.com/watch?v=wNfaVImybns). Si veda anche questo bell’[articolo del New Yorker](https://www.newyorker.com/news/news-desk/steve-jobs-technology-alone-is-not-enough).

<iframe width="560" height="315" src="https://www.youtube.com/embed/wNfaVImybns" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Il linguaggio di programmazione dell’azienda è cambiato, ma forse i suoi obiettivi non tanto.