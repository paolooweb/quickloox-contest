---
title: "Dimenticavo il cavo"
date: 2022-05-29T00:08:37+01:00
draft: false
toc: false
comments: true
categories: [Hardware]
tags: [Magic Keyboard, Ligthning]
---
Per diversi secondi non avevo capito che stesse succedendo, poi ho ritrovato l’intuizione e ho inserito il cavetto Lightning nella [Magic Keyboard](https://www.apple.com/it/shop/product/MK2A3T/A/magic-keyboard-italiano), che era evidentemente scarica oltre ogni remissione.

Ho [cominciato a usarla il 5 gennaio scorso](https://macintelligence.org/posts/2022-01-05-tocchi-e-rintocchi/) (per alimentare un blog che da allora dispone di una ricerca interna decisamente più utile della media) e dunque sono trascorsi più di quattro mesi e mezzo. La tastiera non ha perso un tocco e mi ero completamente dimenticato che avesse una batteria, che andasse caricata, che servisse un cavo.

La usavo e basta.

Perdere il conto della tecnologia è il migliore complimento che possa meritare la tecnologia stessa. Centoquaranta giorni di semplice, basico, costante uso e neanche un pensiero. Averne, di tecnologia così.