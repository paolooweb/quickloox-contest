---
title: "Il signore sì che se ne intende"
date: 2013-10-02
comments: true
tags: [Microsoft]
---
Caspar Bowden è stato *Chief Privacy Advisor*, primo consigliere sulla *privacy*, di Microsoft tra il 2002 e il 2011.<!--more-->

Da quando ha lasciato l’incarico, ha smesso di portare con sé un cellulare ed è diventato, [racconta HotHardware](http://hothardware.com/News/Former-Microsoft-Privacy-Chief-Says-He-No-Longer-Trusts-The-Company/), uno strenuo sostenitore del software libero.

Il cellulare lo porto con me, ma oramai il mio uso di Skype – [proprietà di Microsoft](https://macintelligence.org/posts/2012-07-24-manco-a-dirlo-a-voce/) – è ridotto virtualmente a zero.