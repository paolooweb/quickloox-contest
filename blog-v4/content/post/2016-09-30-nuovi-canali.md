---
title: "Nuovi canali"
date: 2016-09-30
comments: true
tags: [Slack]
---
Non c’è ragione per non provare, dopo [averne parlato](https://macintelligence.org/posts/2016-09-26-a-brutta-posta/). Oltretutto il canale era già attivo e inutilizzato.<!--more-->

Credo che serva un invito per entrare, non ho capito benissimo. Forse da [questo link](https://goedel.slack.com/signup) si può combinare qualcosa.

Il *team* Slack si chiama Loox, risponde all’indirizzo [slack://goedel.slack.com](slack://goedel.slack.com) e NON è (inteso per essere) una comunità, bensì una corsia preferenziale. Chi lo usa al posto di scrivermi una email verrà certamente letto prima e probabilmente riceverà prima anche una risposta.

A parte tutto, Slack è un sistema formidabile per semplificare la gestione delle comunicazioni. Cerco di farlo adottare in tutte le aziende dove metto piede e qualche volta ci riesco anche. Raccomando a tutti di provare.