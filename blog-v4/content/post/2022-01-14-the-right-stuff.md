---
title: "The Right Stuff"
date: 2022-01-14T00:45:10+01:00
draft: false
toc: false
comments: true
categories:
tags:
---
Quando solcavo gli oceani del computing personale a bordo del mio [Power Mac 7500/100](https://everymac.com/systems/apple/powermac/specs/powermac_7500_100.html) neanche lontanamente potevo immagine che un parente strettissimo del mio processore avrebbe lasciato un giorno l’atmosfera terrestre, per [dare vita al James Webb Telescope](https://twitter.com/pavolrusnak/status/1480299213443383297).

<blockquote class="twitter-tweet"><p lang="en" dir="ltr">Did you know that James Webb Space Telescope runs on RAD750?<br><br>It&#39;s a $200,000 radiation-hardened single-board computer based on PowerPC 750 running at 118 MHz, first released over 20 years ago in 2001.<br><br>For comparison, a $35 Raspberry Pi 4 released in 2019 is around 35x faster.</p>&mdash; Pavol Rusnak (@pavolrusnak) <a href="https://twitter.com/pavolrusnak/status/1480299213443383297?ref_src=twsrc%5Etfw">January 9, 2022</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

Un PowerPC 750 discendente del primo esemplare presentato nel 2001, funzionante a centodiciotto megahertz e opportunamente protetto contro le radiazioni cosmiche.

> Per confronto, un Raspberry Pi 4 del 2019 da trentacinque dollari è circa trentacinque volte più veloce.

Sicuro, ma prima che entri nella storia dell’astronomia, anche alla sua massima velocità, ci metterà qualche tempo.

In effetti, dopo l’affascinante [The Right Stuff](https://us.macmillan.com/books/9780312427566/therightstuff) di Tom Wolfe dedicato agli eroi e antieroi umani del programma spaziale americano, non è ancora uscito qualcosa che metta insieme le storie dei chip che hanno contribuito a portare l’umanità in orbita, sulla Luna, nel sistema solare e – grazie alle sonde Voyager – nello spazio interstellare.

Può darsi che tra molti anni qualche pannello solare sperduto tra le galassie funzioni ancora e che ci saranno processori PowerPC a elaborare istruzioni quando nemmeno i musei dell’informatica esisteranno più.

*Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile.*
