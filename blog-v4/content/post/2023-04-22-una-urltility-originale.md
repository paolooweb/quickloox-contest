---
title: "Una URLtility originale"
date: 2023-04-22T02:52:30+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [MacStories, Automation April, HyperDuck]
---
Nel procedere dell’[Automation April 2023](https://macintelligence.org/posts/2023-03-25-adesione-automatica/) su *MacStories* si segnala la loro [recensione di HyperDuck](https://www.macstories.net/reviews/automation-april-hyperduck-leverages-the-power-of-url-schemes-to-control-your-mac-from-an-iphone-or-ipad/).

HyperDuck manda URL da un apparecchio Apple a un altro. È una cosa che fa anche AirDrop, vero, ma non in certi casi particolari che possono anche diventare frequenti e dove HyperDuck, per inciso gratuito, diventa la classica mano santa.

Non dovrebbe mancare, davvero, su nessun apparecchio e anzi, mentre scrivo da un albergo lontano, mi viene in mente che forse la capacità di inviare un Url al mio Mac potrebbe risolvermi una piccola esigenza immediata. Vado a scaricare.