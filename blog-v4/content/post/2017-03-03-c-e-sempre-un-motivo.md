---
title: "C’è sempre un motivo"
date: 2017-03-03
comments: true
tags: [Beam, Word]
---
A colloquio con un amico che ha ricevuto per lavoro una massiccia quantità di file Word, con l’estensione .doc del tempo che fu.

Anteprima vede perfettamente il contenuto dei file: sono lunghe tabelle di più pagine, strutturate su tre colonne.

Pages, però, nell’aprirli mostra solo la terza colonna; le prime due spariscono. Anche LibreOffice. Anche TextEdit. Anche un Pages ’09.

Certo, si tratta di una tabella con tre colonne, ma è una tabella con tre colonne. Qualunque altra complicazione è assente. Chi adduce il problema della compatibilità per sostenere la necessità di usare Word, sostiene un software costruito per riuscire comunque a creare casi di incompatibilità. Volutamente, a spese della produttività e dell’efficienza delle vittime, pardon, degli utilizzatori.

Prova e riprova, tuttavia, l’amico scova [Bean](http://www.bean-osx.com/Bean.html) in un metaforico cassetto della scrivania. E risolve.

C’è sempre un modo in Word per tradire la collaborazione. C’è sempre un motivo per farne a meno. Si può farne a meno.