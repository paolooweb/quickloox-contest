---
title: "Questione chiusa, per forza"
date: 2017-07-23
comments: true
tags: [iPhone, Gruber, Jobs]
---
Se John Gruber dedica una pagina di *Daring Fireball* all’[inutilità di chiudere sistematicamente in modo forzato le *app* su iOS](https://daringfireball.net/2017/07/you_should_not_force_quit_apps), nel 2017, vuol dire che c’è un’emergenza sociale o qualcosa di simile.<!--more-->

I link li ha tutti lui e cita perfino una email di Steve Jobs che nel 2010 (!) aveva fatto giustizia della questione:

>Usa [il multitasking di iOS] come è stato concepito e funzionerà. Non c’è bisogno di chiudere le app.

Mi spingo a ribadire che questo è diverso dal dire che non vada *mai* fatto. A volte, eccezionalmente, una *app* può bloccarsi o malfunzionare. A volte. Eccezionalmente.

Farlo per abitudine, su base sistematica, è sbagliato. Anzi, le prestazioni e l’efficienza di iOS *ne soffrono*, cioè si ottiene un effetto peggiore di quello che si ha lasciando fare al sistema.

Se va scritto sette anni dopo, mi allarmo e rilancio l’appello. Il multitasking di iOS funziona bene.