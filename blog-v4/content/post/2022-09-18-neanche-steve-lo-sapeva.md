---
title: "Neanche Steve lo sapeva"
date: 2022-09-18T01:16:28+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [Jobs, Steve Jobs, NeXTstep]
---
Come da [questa schermata](https://twitter.com/DayTechHistory/status/1571469093588197376) si sarebbe arrivati a cambiare il mondo, una persona alla volta, in numero di miliardi.

<blockquote class="twitter-tweet"><p lang="en" dir="ltr">On this day in 1989, NeXTSTEP 1.0 was released. <a href="https://t.co/GWuVi8qf1x">pic.twitter.com/GWuVi8qf1x</a></p>&mdash; Today in Tech History (@DayTechHistory) <a href="https://twitter.com/DayTechHistory/status/1571469093588197376?ref_src=twsrc%5Etfw">September 18, 2022</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>