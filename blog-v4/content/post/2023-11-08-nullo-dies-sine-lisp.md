---
title: "Nullo dies sine Lisp"
date: 2023-11-08T22:52:32+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [Lisp, lone, GitHub, Linux]
---
Registro con piacere l’inizio di un progetto per dare a Linux un linguaggio Lisp completamente autonomo, privo di qualsiasi dipendenza software. Si chiama [lone](https://github.com/lone-lang/lone), presumibilmente da *standalone*, autonomo appunto.

Non c’è ancora molto, tuttavia sono fiducioso che lo sviluppo proseguirà fino a raggiungere un livello di utilizzabilità almeno accettabile. Da questo punto di vista, la destinazione Linux è un buon indizio di potenziale continuità.

Programma oggi, programma domani, un Lisp privo di dipendenze software finirà per funzionare anche nel Terminale di Mac e poco importa se servirà lo [Homebrew](https://brew.sh) della situazione.

Il futuro continua a svolgersi e ogni tanto dà anche una soddisfazione.