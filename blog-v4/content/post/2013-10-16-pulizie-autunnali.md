---
title: "Effetti collaterali"
date: 2013-10-16
comments: true
tags: [OSX]
---
Un fatto che riesco a collegare solo a Mavericks ma non posso scrivere in un libro.<!--more-->

Al momento di installare la versione Golden Master di Mavericks ne ho approfittato per fare un po’ di sistemazione. Avevo circa trenta gigabyte liberi su disco (su 320). Con l’obiettivo di arrivare a cinquanta gigabyte ho spostato vecchio lavoro su dischi di archivio e contestualmente, visto che avevo spazio, ho portato sul disco di sistema una immagine VirtualBox che tenevo su un disco esterno.

La somma algebrica mi ha portato a circa 45 gigabyte liberi. Non i cinquanta cui ambivo, ma bene così.

La cosa è avvenuta una decina di giorni fa.

Oggi mi accorgo che lo spazio libero è di 98 gigabyte.

Una cifra che non vedevo dai tempi dell’acquisto del MacBook Pro. Non trovo niente si evidente che sia sparito. Non avevo condotto rilevamenti con strumenti tipo GrandPerspective prima degli spostamenti e così, anche se li conducessi ora, non saprei dire che cosa non c’è più e perché sia sparito.

Nel frattempo ho lavorato a ritmo fin troppo pieno, semmai mi aspettavo di perdere un paio di gigabyte, non di trovarne cinquanta imprevisti.

La spiegazione più semplice è che Mavericks abbia ripulito a dovere un po’ di cache e di file di lavoro che Mountain Lion non era stato tanto efficiente a maneggiare. Lavoro con decine di programmi aperti, su progetti multipli, in preda a un multitasking febbrile e a Safari sempre sovraccarico, per cui non mi scandalizza una certa inefficienza del sistema a starmi dietro.

Se veramente fosse un effetto collaterale della installazione di Mavericks, mi piazzerei davanti a un Apple Store a fare il buttadentro, il giorno che esce.

Non lo fosse, agli incerti dico: nessun problema. C’è un bug fastidioso di riposizionamento delle finestre se si fa avanti e indietro tra due utenti sul computer e qualche scritta non ancora in italiano. Il resto c’è tutto.