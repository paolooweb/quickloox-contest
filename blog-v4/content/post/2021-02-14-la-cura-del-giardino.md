---
title: "La cura del giardino"
date: 2021-02-14T01:51:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [App Store, Apple Arcade, Arcade, Google Play, walled garden] 
---
A seguito di [un nuovo iPhone entrato in casa](https://macintelligence.org/posts/Tempi-vivi.html), la famiglia può giovarsi di tre mesi di prova gratuita di [Arcade](https://www.apple.com/apple-arcade/). Una figlia ha passato i sei anni, l’altra i tre, un’occhiata al servizio l’ho data.

App Store, rispetto a Google Play Store, fa la figura di un centro urbano rispetto a una favela (con tutto il rispetto per chi abita quelle reali, certamente non per scelta). Ci sono problemi, ci sono strade da evitare, tutto potrebbe (sempre) andare meglio, ma ti puoi anche rilassare, fare cose utili, divertirti.

A confronto con App Store, tuttavia, Arcade è la Città Ideale. La selezione è reale e attenta, i giochi sono interessanti anche quando non sono del mio genere, tutto viene via con leggerezza e tranquillità.

In casa si gioca, ma nessuno è giocatore accanito. Pagheremmo più che volentieri Arcade se sostituisse una spesa videoludica, ma non la abbiamo. Quest’anno abbiamo già speso in giochi più dell’intero 2019, [diciotto euro di app Toca Boca](https://macintelligence.org/posts/Shopping-selvaggio.html) più 5,49 euro per [Castle of Illusion](https://apps.apple.com/it/app/castle-of-illusion/id646489967). Solo che è l’eccezione, non la regola. Il 2018 è stato come il 2019, che è stato come il 2017.

Così non ci abboneremo a Arcade. Però ci godremo questi tre mesi come gente di città che può concedersi un soggiorno straordinario nel resort sulla barriera corallina.

Apple viene spesso associata all’idea di *walled garden*, il giardino recintato, dove il recinto è la parte negativa. Francamente, se guardo Arcade vedo piuttosto un *tended garden*, dove c’è qualcuno che oltre a recintare il terreno si prende anche cura di che cosa ci cresce.

*Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile.*