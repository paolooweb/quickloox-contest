---
title: "Altre cornici"
date: 2015-10-01
comments: true
tags: [Fabio, Altroconsumo, Android, iPad, Nexus]
---
Mi scrive **Fabio**, seccato per ricevere proposte commerciali non richieste da… [Altroconsumo](https://offertaprova.altroconsumo.it/newseq/it/tombola/index_it.html).<!--more-->

>sai a chi posso rivolgermi affinché i paladini dei diritti dei cittadini la smettano di inondarmi di spam nel quale mi chiedono di iscrivermi alla loro associazione in cambio di mirabolanti ritrovati tecnologici?

Mi è venuta la curiosità di scoprire quali fossero i *mirabolanti ritrovati tecnologici*. L’offerta per i nuovi abbonati prevede in omaggio un *tablet* Android con schermo da sette pollici e *risoluzione di 800 x 480 pixel*.

Senza approfondire troppo, siamo all’equivalente tecnologico del primo iPad, anno 2010.

Sarebbe bello chiedere ad Altroconsumo se lo hanno testato e classificato tra i migliori prodotti sul mercato, il *tablet* che offrono agli abbonati.

Ricordo che sono gli stessi di una mitica [recensione proprio del primo iPad](https://macintelligence.org/posts/2010-02-08-quelli-degli-antiforfora/), scritta dopo avere visto la presentazione su Internet, con l’iPad fisicamente più vicino a novemila chilometri di distanza. Scrivevano questo:

>Questo apparecchio potrebbe essere la miglior cornice per foto digitali di tutti i tempi.

Cinque anni dopo, ne offrono un equivalente ai nuovi abbonati, mentre i consumatori veri sono andati altrove (un [Nexus 7](https://it.wikipedia.org/wiki/Nexus_7_(2013)) di due anni fa aveva risoluzione 1.920 x 1.200). A loro modo, in Altroconsumo sono coerenti. Come cornice per foto digitali, il loro *tablet* è perfetto.

**Aggiornamento del 5 ottobre:** e se provi a disiscriverti dalla *mailing list*, loro continuano a mandartela, da un altro mittente. Complimenti.