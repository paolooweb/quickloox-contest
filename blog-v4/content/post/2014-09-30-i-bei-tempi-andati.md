---
title: "I bei tempi andati"
date: 2014-10-01
comments: true
tags: [Riccardo, MacOS9, Titanium]
---
Non ho ancora letto con attenzione il lungo [articolo di Ars Technica](http://arstechnica.com/apple/2014/09/my-coworkers-made-me-use-mac-os-9-for-their-and-your-amusement/) dove Andrew Cunningham cerca – e praticamente non riesce – a mettersi in condizioni di lavorare usando un PowerBook G4 Titanium e Mac OS 9.2.<!--more-->

Vero che molto è cambiato ed è specialmente difficile adattare un sistema operativo del 2000 alla Internet odierna. Tuttavia l’istinto mi spinge a condividere la [conclusione di **Riccardo**](https://twitter.com/morrick/status/516364206871179264):

<blockquote class="twitter-tweet" lang="en"><p>I maintain vintage Macs running System 7 to OS X 10.5. I think the author paints a worse picture than it actually is: <a href="http://t.co/VKTY5nPjg9">http://t.co/VKTY5nPjg9</a></p>&mdash; Riccardo Mori (@morrick) <a href="https://twitter.com/morrick/status/516364206871179264">September 28, 2014</a></blockquote> <script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>

Se dovessi tornare a Mac OS 9 per due giorni, penso che mi mancherebbe il Terminale più di tutto il resto. E Internet secondo me si sistema. Mi riservo di approfondire.

**Aggiornamento:** Riccardo ha svolto il tema da par suo e composto la [risposta migliore possibile](http://systemfolder.wordpress.com/2014/10/02/vintage-macs-work/).