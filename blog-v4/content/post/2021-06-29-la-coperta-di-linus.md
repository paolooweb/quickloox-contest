---
title: "La coperta di Linus"
date: 2021-06-29T01:52:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [Linus Torvalds, Linux, Mac, Apple Silicon, M1] 
---
In attesa che qualcuno realizzi davvero la [mappa della libertà](https://www.macintelligence.org/posts/La-mappa-della-libertà.html), per fortuna c’è chi si dà da fare per difendere la libertà di codice. Su qualcosa il software libero dovrà pure girare.

Per fortuna c’è quel computer chiuso, proprietario e inaccessibile che si chiama Mac, per il quale – come avranno fatto? – è iniziato il [supporto di Linux relativamente ai sistemi con chip Apple Silicon](https://www.phoronix.com/scan.php?page=news_item&px=Linux-5.13-Released). (Scrivo *Apple Silicon* perché a breve scrivere M1 non sarà più comprensivo di tutta l’offerta). Linus Torvalds e compagni hanno iniziato a coprire quello che sarà l’ultimo avamposto di libero pensiero informatico.

Ribadisco la previsione già fatta, da un’altra angolazione: da qui a dieci anni, per usare software veramente libero, molto probabilmente sarà indispensabile un Mac.

Su qualunque altro apparecchio funzionerà solo il Linux-frankenstein, quello sottoposto a lobotomia per funzionare come accessorio sterile e vicolo cieco del sistema operativo su cui [si sono appena inventati che sia stato inventato il web](https://macintelligence.org/posts/La-piattaforma-degli-animali.html).

*Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile.*