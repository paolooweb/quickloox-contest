---
title: "An Experience Company"
date: 2022-07-09T00:27:40+01:00
draft: false
toc: false
comments: true
categories: [Hardware]
tags: [Andre Garzia, iPad, Surface Pro, MacBook Pro]
---
Chi fosse in cerca di un concentrato di saggezza per il weekend, può cavarsela con una lettura da cinque minuti scarsi: [di iPad e degli sviluppatori](https://andregarzia.com/2022/06/about-ipads-and-developers.html). Gli sviluppatori sono il pretesto, perché il post – forse inconsapevolmente mette nero su bianco alcuni fondamenti della cultura Apple che valgono per tutti e che non si riesce mai a dare per scontati anche dopo anni e anni. Per dire:

>Apple è una Experience Company. Molto similmente a Disney, Apple vende sogni. Vende a partire da un’idea, da un’esperienza curata di che cosa nella loro visione dovrebbe essere il computing.

Questo si traduce nei prodotti e, segnatamente, su iPad:

>Quando Apple produce una tavoletta, non vuole che sia un portatile. Vuole che sia **la tavoletta Apple più autentica che riesce a produrre**. Che cosa significhi forse non lo sanno neanche loro, ma questa è la loro stella guida. Apple vuole che i propri device siano autenticamente se stessi. Una tavoletta è una tavoletta, un telefono è un telefono, un Mac è un Mac. Le loro esperienze sono complementari, ma non necessariamente intercambiabili.

Il che vuol dire una cosa ancora non sufficientemente ovvia a tutti:

>Cercare in una classe di apparecchi le esperienze che Apple vuole tenere in una classe di apparecchi diversa è una ricetta per la frustrazione. non è come funziona Apple.

Per chi si ostina a preconizzare, desiderare, ipotizzare apparecchi Apple che facciano tutto e che si trasformino o contengano due sistemi operativi o altra creatività assortita, fermo restando che il futuro è imprevedibile e che Steve Jobs era noto per cambiare idea anche sul più solido dei suoi argomenti, se gli dimostravano che aveva torto, il presente indica chiaramente come stiano le cose:

>La società che lavora sulla convergenza è Microsoft, non Apple.

Ne segue che chi vuole la tavoletta ma anche il portatile, il touch insieme al mouse, il convertibile, il 2-in-1, tutte quelle formulette che vengono inventate dagli uffici marketing perché i cretini si sentano competenti per il fatto di sapere a memoria delle sigle, ha una strada molto precisa davanti: si compri un Surface.

>Surface un portatile peggiore di MacBook Pro. È anche una tavoletta peggiore di iPad, ma è un portatile migliore di iPad e una tavoletta migliore di un MacBook.

Specificando doverosamente che molti modelli di Surface supportano Linux, perché c’è un limite a tutto, anche al prezzo che si è disposti a pagare per la convergenza.

Più o meno ho sempre avuto un’idea del perché scrivo queste cose su un Mac ma tra poco vado a dormire con un iPad in mano. Ora lo so come se il perché lo avessi tatuato addosso.