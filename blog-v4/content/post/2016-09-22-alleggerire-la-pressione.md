---
title: "Alleggerire la pressione"
date: 2016-09-22
comments: true
tags: [iPhone, Lorescuba]
---
La foto di oggi arriva (ringraziamenti!) da **Lorescuba**. Il suo capo deve tenere la pressione sotto controllo e così tiene sulla scrivania iPhone 5s, dock di [iHealth](https://ihealthlabs.eu/en/) e sfigmomanometro.

 ![iPhone 5s, iHealth, sfigmomanometro](/images/sfigmomanometro.jpg  "iPhone 5s, iHealth, sfigmomanometro") 

Una volta erano di moda frasi come *cambiare il mondo una persona alla volta*. Sta succedendo davvero ed è bello pensare che tante persone possano pensare alla loro salute con più precisione, a cuore più leggero e arterie pure.