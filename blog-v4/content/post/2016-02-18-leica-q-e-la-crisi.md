---
title: "Leica Q e la crisi"
date: 2016-02-18
comments: true
tags: [Leica, Q, iPhone]
---
Sono fuori tema e mi scuso: il tempo a disposizione per scrivere questo *post* se ne è andato a leggere la [pazzesca recensione della Leica Q](http://craigmod.com/sputnik/leica_q/), usata durante un viaggio di sei mesi in Estremo Oriente. In abbinamento a un iPhone, ovvio.

So che nel 2016 sembra assurdo prendersi un’ora per leggere una recensione. *Sembra*. E per il resto rimando a [considerazioni già fatte](https://macintelligence.org/posts/2015-09-18-ok-la-crisi-pero/) sulla crisi dell’editoria. Analisi così, su una rivista vecchio stile, sono state l’eccezione più che la regola, ammesso che siano mai comparse.