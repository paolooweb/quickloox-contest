---
title: "Come ci siamo ridotti"
date: 2015-05-03
comments: true
tags: [Adc, Wikipedia, iPhone, broadband]
---
Nel sito archeologico del mio box ho rinvenuto una scatola di cartone che peserà due o tre chili. Contiene settantasei buste di cartone ciascuna comprendenti da uno a tre Cd o Dvd del servizio [Apple Developer Connection](http://it.wikipedia.org/wiki/Apple_Developer_Connection), che una volta spediva a intervalli regolari software agli iscritti al programma sviluppatori Apple.<!--more-->

(Da ammirare come la pagine Wikipedia qui sopra linkata riesca a parlarne al presente).

Potrebbero essere centodieci-centoventi dischi, per una media stimata con estrema approssimazione di centocinquanta-trecento gigabyte di materiale pubblicato tra settembre 1994 e maggio 2002.

Otto anni, centinaia di supporti, decine di euro in spese di spedizione per una quantità di dati che oggi può stare in un angolo di un disco non molto più grande del mio iPhone.

Quello che ancora manca in Italia è la capacità di scaricare una quantità simile di dati in un tempo ragionevole. Tranquilli: [non la avremo mai](https://macintelligence.org/posts/2014-11-14-la-terra-della-confusione/).

Per il resto, con l’evoluzione tecnologica, di cui abbiamo goduto in questi anni, in termini di ingombri e spesa rapportati all’informazione disponibile, non ci siamo ridotti così male.
