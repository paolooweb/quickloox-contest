---
title: "Musica, foto e fischi"
date: 2017-04-19
comments: true
tags: [iPhone, Bon, Appétit, Wired, Lacy, GarageBand, Grossman, Condé-Nast, Grammy]
---
Per la prima volta la foto di copertina della rivista *Bon Appétit* [è stata scattata con un iPhone](https://techcrunch.com/2017/04/18/bon-appetit-iphone-cover/).

Certo, non è come scattare con una vera macchina fotografica. Nell’opinione di Alex Grossman, che dopotutto fa solo il direttore creativo per l’editore Condé-Nast, gli scatti presi da iPhone – specialmente quando si va in stampa invece che restare in digitale – non sono completamente confrontabili con una *Dslr [Digital Single-Lens Reflex] da 25 mila dollari*. Tuttavia, se si scatta nelle giuste condizioni, *il 99,9 percento delle persone* potrebbe non notare la differenza. I corsivi sono attribuiti a Grossman da *TechCrunch*.

L’articolo fa presente come sia la prima volta per *Bon Appétit* ma non certo per i periodici illustrati in genere. E che l’editing degli scatti è stato effettuato direttamente su iPhone con [Vsco](https://appsto.re/it/oegdJ.i) invece di *dover tirare fuori il portatile*.

Su *Wired* appare inoltre un [pezzo dedicato a Steve Lacy](https://www.wired.com/2017/04/steve-lacy-iphone-producer/), diciottenne musicista e produttore che ha già ricevuto la *nomination* a un premio Grammy ed è comparso in produzioni di altri nomi emergenti del panorama *hip-hop* americano. Dove, per amore dell’eufemismo, se emergi vuol dire che hai battuto una certa concorrenza.

Lo strumento di lavoro di Lacy è il suo iPhone.

Ha preso confidenza con il software preferito della sua casa discografica, ma funziona su portatile e lì si trova poco. Il suono è troppo asettico e va elaborato prima di risultare adatto alla pubblicazione. Poi l’ispirazione gli viene quando si sente più libero e si sente più libero usando iPhone e GarageBand. Mentre scrivo, apprendo che GarageBand – assieme alle altre *app* di iWork – [è diventato gratis](https://www.macrumors.com/2017/04/18/apple-imovie-garageband-iwork-free-for-all-users/) per vecchi e nuovi utenti, senza la limitazione del necessario nuovo acquisto hardware.

Ha deciso di facilitarsi il lavoro e ha preso apparecchiature aggiuntive. Cioè un secondo iPhone.

A diverse persone dovrebbero fischiare le orecchie.
