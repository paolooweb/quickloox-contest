---
title: "Accavallavacca"
date: 2017-01-31
comments: true
tags: [cavi, Bartezzaghi, Scanjet, Time, Machine, watch, tv, wireless, Keystation, Jambox]
---
Rubo un [titolo](https://www.ibs.it/accavallavacca-libro-stefano-bartezzaghi/e/9788845254475) a Stefano Bartezzaghi per descrivere la situazione della mia scrivania.

* Alimentazione di MacBook Pro.
* Auricolari.
* Time Machine.
* Hub Usb che collega a sua volta:
	* Alimentazione e sincronizzazione di watch.
	* Alimentazione e sincronizzazione di iPhone.
	* Alimentazione e sincronizzazione di iPad.
	* Alimentazione e sincronizzazione di un orologino che mi hanno regalato a un evento e devo ancora decifrare.

Aggiungo, staccati ma presenti sulla scrivania,

* Alimentazione e collegamento di [Scanjet 300](http://www8.hp.com/it/it/products/scanners/product-detail.html?oid=5251707).
* Alimentazione e aggiornamento di [Jambox](https://www.amazon.com/Jawbone-Bluetooth-Speaker-Discontinued-Manufacturer/dp/B004E10KI8).
* Alimentazione di [Keystation 61 es](http://www.m-audio.com/products/view/keystation-61es).
* Collegamento della suddetta Keystation.

Sono sicuro che frugando bene sotto la superficie salterebbe fuori qualcos’altro.

Sono tutti **cavi**.

A guardare bene l’elenco, si capisce che varie voci potrebbero essere eliminate già oggi. Time Machine, lo scanner, gli auricolari potrebbero collegarsi *wireless*, per esempio.

Altre voci sono *già* state eliminate. Per esempio, la connessione al televisore è regolata *wireless* da [tv](http://www.apple.com/it/tv/). Abbiamo abolito la stampante di casa ma quando c’era, era senza fili. E così via.

Il punto è che la mia scrivania oggi è affollata di cavi che si accavallano, intrecciano, annodano, logorano, impolverano.

La prossima rivoluzione, di quelle vere, mica da comunicato stampa, è l’eliminazione di tutti-tutti-tutti i cavi. Compresa l’alimentazione.

E se uscisse un Mac che rinunciasse alle sue componenti professionali per promettermi oggi, qui, subito, *it just works*, di eliminare il viluppo di cavi sulla mia scrivania, darei priorità assoluta alla tentazione.