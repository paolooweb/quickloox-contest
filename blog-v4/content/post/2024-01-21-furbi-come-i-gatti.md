---
title: "Furbi come i gatti"
date: 2024-01-21T00:02:29+01:00
draft: false
toc: false
comments: true
categories: [Software, Internet]
tags: [El Païs, intelligenza artificiale, Ai, IA, LeCunn, Yann LeCunn, Clegg, Nick Clegg, Meta]
---
Al forum di Davos in corso in questi giorni c’è stata una conversazione tra gli invitati di cinque testate giornalistiche, l’ex politico Nick Clegg – ora President per i Global Affairs di Meta – e Yann LeCunn, sempre di Meta Chief Ai Scientist. Uno che qualcosa ne sa, certamente non un pessimista né un illuso.

Nel [resoconto della testata spagnola El Païs](https://english.elpais.com/technology/2024-01-19/yann-lecun-chief-ai-scientist-at-meta-human-level-artificial-intelligence-is-going-to-take-a-long-time.html) compaiono alcune dichiarazioni di LeCunn su cui è intelligente, in modo naturale, essere informati.

>Contrariamente a quello che qualcuno potrebbe dire, non abbiamo un progetto di sistema intelligente capace di raggiungere l’intelligenza umana. […] L’intelligenza artificiale di livello umano non è dietro l’angolo. Ci verrà molto tempo. È richiederà nuovi progressi scientifici rivoluzionari di cui oggi non abbiamo idea.

Per LeCunn, un sistema di questo genere dovrebbe saper sperimentare il mondo fisico, avere ricordi, pianificare e ragionare; quattro capacità di cui i sistemi attuali mancano. Secondo le sue aspettative,

>sarei felice se entro la fine della mia carriera avessimo sistemi intelligenti come un gatto o qualcosa del genere.

Quando si legge o si sente dire di intelligenza artificiale, bisogna ricordare questo: si tratta di sistemi i cui progettisti venderebbero l’anima pur di riuscire a farli pensare come un gatto. Cosa lontanissima da quello che abbiamo oggi e irraggiungibile con le tecnologie attuali.

Tutto ciò è molto diverso dall’avere sistemi migliori di noi in aree specifiche. Per LeCunn, è assai probabile che avremo sistemi migliori di noi in qualsiasi area, non solo nel gioco del Go o nel calcolo delle probabilità. Non è intelligenza, però.