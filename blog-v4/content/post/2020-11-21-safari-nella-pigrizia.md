---
title: "Safari nella pigrizia"
date: 2020-11-21
comments: true
tags: [AppleScript, Safari]
---
Confesso di avere molto peccato.

Mi serviva aprire in altrettante finestre di Safari una lista molto nutrita di indirizzi web.

Mi sono per un attimo brillati gli occhi al pensiero di risolvere il problema con uno script… ma era molto urgente.

Ho così incollato l’elenco di Url in una nuova cartella di segnalibri e, con un clic destro, ho detto a Safari di aprirli.

Sono pentito e attendo la penitenza.