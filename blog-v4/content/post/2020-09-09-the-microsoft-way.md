---
title: "The Microsoft Way"
date: 2020-09-09
comments: true
tags: [Microsoft, Teams, Network, Analytics]
---
Per motivi ineludibili di lavoro mi tocca sostenere il peso di due account ai servizi online di Microsoft.

Significa che, senza che io abbia chiesto alcunché o espresso alcun consenso, mi arriva in posta periodicamente un rapporto di Network Analytics in cui Microsoft riassume i contatti che ho avuto, quanto sono durati eccetera.

Ovvero, sorvegliano il mio lavoro.

Dopo due o tre rapporti, ho fatto clic sul link di disiscrizione. Il risultato è questo:

 ![alt](/images/network-analytics.png  "title") 

Non c’è dubbio: la situazione è immediatamente comprensibile a chiunque.

Risultato, i rapporti continuano ad arrivare e la sorveglianza a verificarsi, ma che caso. Un modo furbetto di procedere.

Ah, mi ero dimenticato: la comunicazione, qualunque cosa voglia significare, riguarda *l’altro account*. Non quello da cui ho chiesto di disiscrivermi.

Io lo devo subire e lo subisco. Ma chi la sceglie, questa modalità di trattamento, non la trova perlomeno un tantino frustrante?