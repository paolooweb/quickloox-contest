---
title: "La fortuna aiuta gli audaci"
date: 2023-01-28T02:33:49+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [iPadOS, iPad, macOS, Mac, Stage Manager]
---
L’unica paura che bisogna avere è quella di non tentare tutto quello che vorremmo. Le cose cambiano, nascono idee nuove, fossilizzarsi sulle abitudini sembra protettivo all’inizio ma si rivela presto una posizione perdente.

Ho provato nel tempo centinaia di app, da quando ancora si chiamavano programmi, ogniqualvolta ci fosse la minima aspettativa di scoprire qualcosa di interessante.

Ho dato fiducia a estensioni, plugin, moduli esterni, script, espansioni infinite di infinito software, sempre per trovare qualcosa di inedito. Sempre per fare un passo in più.

A volte è mancato il coraggio di sacrificare un comportamento acquisito, mettere in discussione una *comfort zone*, accettare una temporanea diminuzione della produttività, accantonare una abitudine per provare una impostazione nuova, un comando diverso. Se scrivo sulla tastiera quasi con dieci dita, oggi, è merito dei dolorosi giorni passati ad aggiungere dita e, intanto, controllare lo stress e il disagio dati dal fatto di andare piano.

Non siamo qui per crogiolarci nella comodità del noto. A seconda del momento ho davanti una bicicletta per la mente, un amplificatore di intelligenza, una tavoletta prefigurata da Alan Kay, un iPod-telefono-comunicatore via Internet. E pure un computer da polso. Il progresso è sempre davanti a me, come è possibile pensare di ignorarlo e stare fermi, magari per miope ignavia?

Ecco perché, dopo uno strenuo combattimento mentale trascinatosi per settimane, ho deciso di buttarmi e ho ritrovato il fegato sufficiente per accettare il rischio.

Ho acceso Stage Manager.