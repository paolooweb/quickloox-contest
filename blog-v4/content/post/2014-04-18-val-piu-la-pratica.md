---
title: "Val più la pratica"
date: 2014-04-18
comments: true
tags: [Pasqua, Typekit, Adobe]
---
Compiti per dopo le vacanze di Pasqua, visto che comprensibilmente in questi si starà lontano dagli aggeggi più a lungo del solito: imparare a cogliere il valore della tipografia.<!--more-->

Risorsa utile, e lo dico io che tutto sono tranne che tifoso sfegatato Adobe, [Typekit Practice](http://practice.typekit.com). Per capire di tipografia fa meglio avere studiato, ma vale più la pratica. Per chi si appassiona, molto di più.