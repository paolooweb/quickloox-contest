---
title: "Famolo modulare"
date: 2016-09-03
comments: true
tags: [Ara, Google, Samsung, Galaxy, Note]
---
Google ha di fatto [disconosciuto il proprio progetto Ara](http://arstechnica.com/gadgets/2016/09/project-ara-googles-modular-smartphone-is-dead/), lasciando ad altri il compito di realizzare un’architettura modulare per computer da tasca assemblabili da parte dell’acquirente. Che prende il modulo tastiera fisica se vuole la tastiera fisica, il modulo processore economico se vuole spendere meno e quando un modulo è obsoleto o guasto, cambia il modulo invece di cambiare il computer: stacca e riattacca e tutto procede.<!--more-->

Un sacco di gente he parlato con favore e aspettativa e non ho voglia di andarli a ritrovare uno per uno, basta la memoria che è ancora buona. Li pregherei eventualmente di farsi vivi loro. Perché mi spieghino, come avrei voluto mi spiegassero l’anno scorso, come si realizza un’architettura modulare competitiva in un mondo dove la miniaturizzazione è tutto.

Perché avere un’architettura modulare ha costi: di spazio, di prestazioni, di sovrastruttura. Se qualcuno si intende di parallelismo del software, sa che suddividere lo stesso compito di elaborazione tra due processori permette di completarlo in *poco più* della metà del tempo. Una parte della capacità di elaborazione se ne va infatti in coordinamento dei due colleghi. Più aumenti il numero dei processori più aumenta la richiesta di coordinamento e la parte di vantaggio teorico che se ne va.

Tanti sono invece convinti che per avere un apparecchio modulare piccolo, sottile, potente, resistente, aggiornabile, efficiente come un iPhone basti farlo modulare e la sua resa sarà uguale, solo con la modularità. Così non è.

Gli spazi fisici all’interno di un iPhone sono calcolati al decimo di millimetro per riuscire a farci stare tutto e farlo in condizioni di sicurezza. Cosa che non è automatica: Samsung sta [richiamando in massa](http://m.yna.co.kr/mob2/en/contents_en.jsp?cid=AEN20160901010900320) i nuovissimi e stilosissimi Galaxy Note 7 perché, ehm, alcune unità esplodono mentre si carica la batteria. Che succede a un apparecchio modulare se togli o aggiungi un modulo mentre la batteria è in carica? Il sistema deve saperlo e farsene carico. I connettori devono essere sicuri. Eccetera. In un apparecchio non modulare, questi problemi non esistono e le risorse a disposizione sono usate per migliorare ogni altro aspetto dell’utilizzo.

*The Verge* ha un bell’[articolo del 2014](http://www.theverge.com/2014/4/15/5615880/building-blocks-how-project-ara-is-reinventing-the-smartphone) che descrive il progetto e le problematiche di Ara. Citando il quale, John Gruber di *Daring Fireball* [scrive](http://daringfireball.net/linked/2014/04/15/project-ara):

>[Rimango](http://daringfireball.net/linked/2013/09/15/phonebloks) altamente scettico sull’idea che un design modulare possa competere in una categoria di prodotto dove dimensioni, peso e autonomia sono così importanti. Anche se riuscissero a mettere qualcosa sul mercato, perché una persona normale dovrebbe essere interessata?

Ecco, appunto.