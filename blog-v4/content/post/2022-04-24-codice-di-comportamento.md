---
title: "Codice di comportamento"
date: 2022-04-24T11:08:15+01:00
draft: false
toc: false
comments: true
categories: [Hardware, Software]
tags: [M1 Ultra, DaVinci Resolve]
---
Leggo con interesse (non materiale) che [DaVinci Resolve 18 ha il playback trenta volte più veloce su M1 Ultra](https://ymcinema.com/2022/04/19/davinci-resolve-18-8k-optimized-and-30x-faster-on-mac-m1-ultra/). Disturbarsi a scrivere e compilare codice su misura per il computer che deve eseguirlo porta frutti buoni per tutti.

Ed è disturbarsi, è avere spirito di comunità, è sapersi comportare assieme agli altri.

I maleducati, gli sciatti, gli arruffoni, propongono _framework_ e piattaforme con la promessa che scrivi una volta sola e va bene per tutto, va bene per tutti.

Va bene per il minimo comune denominatore. Poi è mancanza di visione, di senso civico, di intelligenza nel senso letterale di comprensione. Equivale a un marciapiede con barriere architettoniche, un palasport senza il parcheggio, un bidone senza divisioni per i materiali riciclabili, una pista ciclabile che incrocia la superstrada con un semaforo.

Un buon software è come una persona bene educata: si accorge degli altri. Si comporta in modo appropriato in ciascuna circostanza invece di avere un modo solo per tutte.

I software da minimo comune denominatore sono come persone maleducate o indifferenti e vanno trattati come tali, a cominciare dal farne a meno.

E siamo alla compilazione per il processore specifico. I software che non adottano l’interfaccia del sistema operativo o non parlano con le tecnologie a disposizione (Comandi rapidi, per esempio?) sputano nel piatto, rubano il pane al vicino e buttano la cartaccia per terra. Ci vorrebbe il disprezzo sociale.