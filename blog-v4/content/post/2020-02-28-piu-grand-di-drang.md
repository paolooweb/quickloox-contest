---
title: "Più grand di Drang"
date: 2020-02-28
comments: true
tags: [Wireless, Magic, Keyboard, Shortcut, Drang, Viticci, MacStories, Bluetooth]
---
Leggevo il pezzo di Dr. Drang sulla sua [ricerca di una buona tastiera meccanica che non fosse troppo meccanica](https://leancrew.com/all-this/2020/02/better-typing-through-old-keyboards/) e approvavo completamente il suo pensiero: Apple Wireless Keyboard. Da anni è il centro del mio flusso di produzione e la uso con Mac quando sono alla scrivania per passarla a iPad quando sono in giro e dispongo di un appoggio per la tastiera. Non è la migliore mai prodotta da Apple, ma è ottima e alla fine, quando scrivo con una tastiera fisica, uso sempre la stessa, con memoria dei tasti comprensibilmente notevole; se sono rilassato, riesco a scrivere bene anche al buio nonostante la tastiera manchi di retroilluminazione. È come se fosse diventata un prolungamento dei polpastrelli.

Quando la metto nello zaino per uscire, la spengo per evitare che consumi energia cercando Mac dove non c’è. Quando smetto di usarla con iPad la spengo ugualmente, perché altrimenti, quando rientro, non riesco a collegarla a me. Qui ovviamente Drang se la cava in modo superiore, grazie a un Comando rapido per richiamare le impostazioni Bluetooth direttamente da Siri, dove io faccio vergognosamente ancora a mano. Il fatto che lo abbia preso da *MacStories* e che sia semplicissimo non giova alla mia immagine.

Poi ho scoperto, divertito, che Drang non sapeva di poter spegnere la Wireless Keyboard (pressione prolungata del pulsante di accensione)! Nessuna [schadenfreude](https://it.wikipedia.org/wiki/Schadenfreude) e tanta comprensione ed empatia; capita a chiunque di incartarsi su una cosa semplicissima eppure non completamente ovvia.

Drang ci ha messo due aggiornamenti del suo post a rendersi conto e poi ha concluso in modo fantastico:

>Così ora non ho difetti da lamentare rispetto alla Wireless Keyboard, una situazione per me inusuale.

Si può fare tanta personalizzazione anche su cose molto semplici, come richiamare a voce le impostazioni Bluetooth su iPad. Capita anche ai più grandi che possa scappare un dettaglio semplicissimo.

E ho avuto la soddisfazione di stare un passo davanti a Dr. Drang almeno su un tema per quasi una giornata.