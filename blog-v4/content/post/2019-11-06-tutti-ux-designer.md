---
title: "Tutti UX designer"
date: 2019-11-06
comments: true
tags: [Dong, Trump, Home, iPhone, UX, Sanremo, Nazionale, FastCompany]
---
Il Presidente degli Stati Uniti è noto per le sue azioni e reazioni viscerali e ha pensato anche a un miglioramento per la UX, la *user experience*, l’esperienza utente, di iPhone. Una designer già in Apple all’epoca di quello sviluppo lo ha [commentato con ironia](https://twitter.com/lindadong/status/1188441723170504704).

<blockquote class="twitter-tweet"><p lang="en" dir="ltr">☑️ Do the dishes <br>☑️ Go for a walk <br>☑️ Have the President of the United States tweet he does’t like the UX your team worked on<br>☑️ Water the plants <a href="https://t.co/mZSqVt0eqk">pic.twitter.com/mZSqVt0eqk</a></p>&mdash; Linda Dong 🐍 (@lindadong) <a href="https://twitter.com/lindadong/status/1188441723170504704?ref_src=twsrc%5Etfw">October 27, 2019</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

Prima che qualcuno sia tentato di buttarla incautamente in politica, che c’entra niente, legga *FastCompany* quando scriveva [iPhone X è un’esperienza utente da incubo](https://www.fastcompany.com/90150025/the-iphone-x-is-a-user-experience-nightmare).

Siamo sempre stati tutti commissari tecnici della Nazionale e giurati di qualità di Sanremo. Oggi che c’è la rete e possiamo diffondere incompetenza senza confini, impariamo la semplice lezione di togliere le mani dalla tastiera e la voce dai microfoni tutte le volte che cogliamo una verità evidente, solo che abbiamo esercitato poco rispetto a quanto vorremmo parlare.

Non è una questione di studi o di titoli, ci sono quattordicenni che davanti a uno schermo [creano cose geniali](https://macintelligence.org/posts/2019-10-07-umani-e-quattordicenni/). È questione di equilibrio. Quanto meno, a differenza di [Potus](https://www.potus.com) o di certi giornali, si potrebbe magari usare una dubitativa. 