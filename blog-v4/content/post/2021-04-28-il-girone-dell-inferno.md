---
title: "Il girone Dell’inferno"
date: 2021-04-28T01:51:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [Dell, Eyepatch Wolf] 
---
Ogni giorno arriva qualcuno sui social a dire *oggi abbiamo il vincitore di Internet*, per indicare una battuta fulminante, un disastro fantozziano, una cosa impossibile eppure avvenuta, insomma un’apoteosi di qualcosa.

Non so se abbia i requisiti per *vincere Internet*, certo è una lettura epica. [L’acquisto di un PC con Dell: il mio viaggio all’inferno](https://twitter.com/EyePatchWolf/status/1382355450050580486).

<blockquote class="twitter-tweet"><p lang="en" dir="ltr">Buying a PC with Dell: My Journey Into Hell</p>&mdash; Eyepatch Wolf (@EyePatchWolf) <a href="https://twitter.com/EyePatchWolf/status/1382355450050580486?ref_src=twsrc%5Etfw">April 14, 2021</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

Per quante storie dell’orrore informatico possano essersi lette, questa le supera di anni luce e oltretutto è pure ben scritta, su molte decine di tweet. Con finale a sorpresa.

Una grande storia oltre che una esperienza fuori dalla logica e dal mondo. Quello degli umani, chiaro. Poi c’è Dell.

*Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile.*