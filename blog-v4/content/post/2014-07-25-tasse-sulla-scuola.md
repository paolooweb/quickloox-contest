---
title: "Tasse sulla scuola"
date: 2014-07-25
comments: true
tags: [Siae, iPhone, Coachella, iPad, Wi-Fi]
---
Nel 2012 il distretto scolastico unificato di Coachella Valley in California ha approvato una *Mobile Learning Initiative*, che prevedeva tasse per raccogliere venti milioni di dollari allo scopo di dare un iPad in mano a ogni studente e professore, circondato (l’iPad, lo studente, il professore) di tutto quello che serve per fare veramente la differenza.<!--more-->

>Nell’anno scolastico appena concluso, dei circa ventimila iPad dispiegati solo “sei o sette” sono stati dichiarati persi o rubati. Circa dieci ragazzi hanno ricevuto provvedimenti disciplinari per avere aggirato il filtro sui contenuti proibiti.

Sei o sette, circa dieci, su ventimila. Un bel risultato.

L’[articolo](http://www.citeworld.com/article/2453290/mobile-byod/coachella-school-districts-20000-ipads-are-head-of-the-class.html) prosegue con un’altra considerazione, più difficile da leggere altrove: ventimila iPad che chiedono Wi-Fi necessitano di un gran lavoro di adeguamento dell’infrastruttura.

Il punto tuttavia non era questo, o forse sì. Per portare ventimila iPad nella scuola italiana e fare a tutti la banda necessaria, con insegnanti preparati e attenzione ai contenuti per gli studenti, sarei disposto a pagare la mia quota di tasse.

Invece, la Siae che [accresce gli incassi del centocinquanta percento](http://www.tomshw.it/cont/news/equo-compenso-ecco-il-documento-che-imbarazza-siae/57564/1.html) grazie a (esempio) quattro euro in più sullo *smartphone* mi fa provare un fastidio fisico, anche se sono quattro euro. La scuola, quei soldi, [non la vedranno mai](http://www.dday.it/redazione/13799/equo-compenso-per-copia-privata-ecco-dove-vanno-a-finire-tutti-i-soldi).