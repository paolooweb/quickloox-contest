---
title: "Android invecchia bene"
date: 2017-10-19
comments: true
tags: [Krack, Marshmallow, iOS, Android]
---
È venuta alla luce una vulnerabilità detta [Krack](https://www.krackattacks.com) che mette in pericolo di intercettazione un numero enorme di reti Wi-Fi.

Tutti i produttori si sono attrezzati e chi non aveva già provveduto a mettere in sicurezza la situazione lo sta facendo; nel giro di poche settimane il problema non sussisterà più. Ovviamente parliamo di sistemi operativi aggiornati all’ultimissima versione e all’ultimissima *patch* dell’ultimissima versione.

Come [racconta Ars Technica](https://arstechnica.com/information-technology/2017/10/how-the-krack-attack-destroys-nearly-all-wi-fi-security/), Android 6 è particolarmente vulnerabile. Si usano espressioni come *colpito duro* e *devastante*.

Se oggi un apparecchio Android usa la versione 6, [Marshmallow](https://www.android.com/versions/marshmallow-6-0/), significa che quasi certamente non verrà mai aggiornato. La versione più recente di Android è la 8, [Oreo](https://www.android.com/versions/oreo-8-0/), due passi più avanti.

La versione più recente di iOS è la 11. Due passi più indietro, per confronto, c’è iOS 9.

Le probabilità che oggi un apparecchio con iOS 9 venga aggiornato a iOS 11 sono certo molto basse.

L’unico problema è che iOS 9 è utilizzato dal [nove percento delle utenze](https://developer.apple.com/support/app-store/). Uno su undici.

Marshmallow è impiegato dal [trentadue percento delle utenze](https://developer.android.com/about/dashboards/index.html). Uno su tre.

Aggiungo che la percentuale di chi usa uno iOS sotto la versione 9 è il due percento.

La percentuale di chi usa un Android sotto la versione 6 è il *cinquanta percento*.

Evidentemente sono sistemi che invecchiano bene. Forse giusto un po’ duri d’orecchio sulla sicurezza, via.