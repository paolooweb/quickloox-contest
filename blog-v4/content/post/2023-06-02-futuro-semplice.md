---
title: "Futuro semplice"
date: 2023-06-02T01:36:45+01:00
draft: false
toc: false
comments: true
categories: [Internet]
tags: [Ai, intelligenza artificiale, LeCun, Yann LeCun, Hofstadter, François Chollet, Chollet]
---
Relativamente a [ieri](https://macintelligence.org/posts/2023-06-01-lacchiappasegugi/), aggiungo un [commento](https://twitter.com/fchollet/status/1664507791913140225) di François Chollet, che dal mio punto di osservazione è quanto di più vicino a un Hofstadter per i nostri tempi di ora.

<blockquote class="twitter-tweet"><p lang="en" dir="ltr">To be clear, at this time and for the foreseeable future, there does not exist any AI model or technique that could represent an extinction risk for humanity. Not even in nascent form, and not even if you extrapolate capabilities far into the future via scaling laws.</p>&mdash; François Chollet (@fchollet) <a href="https://twitter.com/fchollet/status/1664507791913140225?ref_src=twsrc%5Etfw">June 2, 2023</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

>Per essere chiari, in questo momento e per il futuro prevedibile, non esiste alcun modello o tecnica di intelligenza artificiale che potrebbe rappresentare un rischio di estinzione per l’umanità. Nemmeno in forma embrionale e neppure se estrapoliamo capacità attuali nel futuro remoto scalandole.

È già una grossa concessione chiamarla *intelligenza artificiale*. Usiamola come usiamo Markdown o il web, per quello che può dare, e per i nostri bisogni di fantascienza diamoci a qualche tema più interessante. Il futuro, relativamente all’intelligenza artificiale, è semplice, molto semplice; è tutto ancora da inventare.