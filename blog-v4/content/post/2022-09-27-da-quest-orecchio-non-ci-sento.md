---
title: "Da quest’orecchio non ci sento"
date: 2022-09-27T00:43:52+01:00
draft: false
toc: false
comments: true
categories: [Hardware]
tags: [Bang & Olufsen, A8]
---
Uno dei regali più belli che ho ricevuto consiste in una copia di auricolari [A8 di Bang & Olufsen](https://www.beoworld.org/prod_details.asp?pid=764). Belli, comodi, indistruttibili, di ottima resa audio.

L’unico difetto consiste nella tendenza del cavo ad aggrovigliarsi e degli auricolari a farlo con altri cavi, per via del sistema di fissaggio all’orecchio. Sistema che d’altra parte ha una sua ragione d’essere, funzionale oltre che bello, cioè design puro.

Mi duole ammetterlo, però uno dei due cavetti di collegamento all’auricolare deve essersi rotto o danneggiato e l’audio arriva da un lato solo.

Proverò a passare in un punto vendita Bang & Olufsen per avere informazioni, ma temo che non ci sarà nulla da fare.

Il punto è che sono passati *undici anni* in cui gli auricolari hanno funzionato da dio. Mi hanno accompagnato fino in Cina e ritorno, sono sopravvissuti a due figlie, si sono collegati praticamente a tutto il mio hardware dall’avvento degli anni dieci in poi.

Rimarranno uno dei regali più belli che ho mai ricevuto.