---
title: "In viaggio con Wi-Fi - la piccolezza"
date: 2019-03-10
comments: true
tags: [Delta, Detroit, Monterrey, Wi-Fi]
---
Volo di qualche ora tra Stati Uniti e Messico; la connessione wireless a bordo viene molto pubblicizzata e c’è perfino una spia luminosa dedicata al Wi-Fi, come quella per il divieto di fumo o l’obbligo di allacciare le cinture.

Di più; ai tagli di banda a pagamento si aggiunge anche una promozione straordinaria per la sola messaggistica, disponibile gratis.

Tiro fuori iPad Pro, lancio iMessage e non funziona. Lo steward, interpellato, armeggia con disinvoltura nelle impostazioni dell’apparecchio, per farmi sapere che sarebbe meglio parlare con il supporto via chat.

Sempre con iPad Pro, apro la chat (che funziona benissimo) e vengo a sapere che la promozione gratuita sulla messaggistica riguarda unicamente gli *smartphone*. Penso a iPhone chiuso nel bagaglio a mano dentro la cappelliera, al mio posto finestrino e alle due persone che dovrei fare alzare per togliermi lo sfizio. Ringrazio per il chiarimento, ricevo la proposta di un coupon per acquistare Internet a metà prezzo e finisce qui.

Sicuramente la connessione funziona, a diffferenza di quanto accaduto con Air France. I prezzi sono paragonabili. Certo, se fai una promozione sui messsaggi, fai miglior figura se la fai sui messaggi. L’idea di farla solo per i telefoni odora un po’ di voglia di profilazione aggressiva.
