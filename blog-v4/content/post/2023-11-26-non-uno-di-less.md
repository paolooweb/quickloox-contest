---
title: "Non uno di less"
date: 2023-11-26T18:33:57+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [macOS, terminale, Unix, Ruby, Terpstra, Brett Terpstra, less, Homebrew, emacs, Markdown]
---
I tredicimila e passa file con dentro i contenuti di questo blog sono in formato [Markdown](https://www.markdownguide.org). Ci sono ancora tremila e rotti file che attendono una elaborazione automatica dei nomi file e una quantità imprecisata di recuperi da Internet Archive di cui solo [Mimmo](https://muloblog.netlify.app) sa qualcosa, ma anche accontentarsi di tredicimila (per il momento, solo per il momento) è più di niente.

Nel tempo presente, almeno la metà dei post viene scritta su iPad e poi caricata su Mac via Terminale. La maggior parte, anzi, praticamente tutta l'interazione che ho con Mac da iPad avviene tramite Terminale (e, per dire, se devo fare una correzione al volo a un file già passato su Mac dove risiede il motore di pubblicazione, prendo un aereo per farmi il caffè al bar, in pratica lancio [emacs](https://macintelligence.org/posts/2023-11-02-e-ora-tutti-insieme/)).

Date queste premesse, mi sono chiesto se fosse il caso di approfittare della utility [mdless](https://brettterpstra.com/projects/mdless/) di Brett Terpstra, che mostra file Markdown dentro il Terminale nel modo più efficace. È gratuita, si installa via [Homebrew](https://macintelligence.org/posts/2023-09-02-la-vita-segreta-delle-foto/), arriva da un programmatore tra i più bravi sulla piazza.

Devo dire che certo non si trattava di una questione di vita o di morte, piuttosto di migliorare il *comfort* di certe situazioni, evitare [less](https://man7.org/linux/man-pages/man1/less.1.html) che su un file Markdown privo di colorazione della sintassi in effetti non è una meraviglia.

Sì, era il caso.