---
title: "Alla sciatto"
date: 2021-09-30T18:56:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [Teams] 
---
È tornato lavoro che mio malgrado mi costringe a usare Teams. Però è cambiato l’account.

Allora inserisco il nuovo account nella app per iPad… ma la app logga da sola con l’account vecchio prima che io faccia in tempo a inserire l’indirizzo.

Riprovo e riesco finalmente a inserire l’indirizzo del nuovo account. Inserisco la password. Tocco il pulsante di conferma. Non funziona.

Avrò digitato male? Riprovo. Stesso risultato.

Alla fine cancello la app e la reinstallo. Funziona.

Il tutto moltiplicato due, perché la app per iPhone si comporta esattamente nello steso modo.

In precedenza mi ero cimentato su Mac, dove avevo provato a resettare la password del nuovo account per provare anche quella strada. Mi è stata promessa una email di conferma del reset che, dopo ore, penso proprio non arriverà mai.

Non resterò all’asciutto di chat. Neanche di software sciatto e, immagino, malcurato *by design* ovunque non ci sia un Pc di mezzo.

*Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile.*