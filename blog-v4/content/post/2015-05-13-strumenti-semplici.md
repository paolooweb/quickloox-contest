---
title: "Strumenti semplici"
date: 2015-05-14
comments: true
tags: [tv]
---
Sono giorni in cui c’è una certa agitazione attorno alla scuola italiana, con discussioni, scioperi e controversie. Non sono esperto, ma una cosa la posso capire: se guardo questo articolo sulla [configurazione per usare tv in aula](http://learninginhand.com/blog/appletv), lo trovo geniale. Soluzioni semplici a problemi fondamentali, uno strumento di costo irrisorio rispetto ai benefici, valorizzabile con un pizzico di ingegno e buon senso.<!--more-->

Nello stesso momento, capisco che ogni riga dell’articolo susciterebbe una obiezione di tipo didattico, metodologico, sociologico: nessun consiglio di istituto farebbe passare qualcosa del genere.

E questo spiega tante cose.

(A latere: una giornata di sciopero dei docenti toglie agli studenti una giornata di apprendimento. Chi gliela restituisce?)