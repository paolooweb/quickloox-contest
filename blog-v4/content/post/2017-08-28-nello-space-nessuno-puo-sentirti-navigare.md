---
title: "Nello Space nessuno può sentirti navigare"
date: 2017-08-28
comments: true
tags: [Space, Cinema]
---
Sono tornato a [The Space Cinema](http://www.thespacecinema.it) dopo un’assenza di tre anni e mezzo e ho deciso di comprare online i biglietti.

Bisogna essere utenti registrati e io lo ero; ma la password non funzionava. Ho chiesto il recupero password, solo che il link porta a una pagina inesistente.

Sono arrivato al recupero password da un’altra pagina e ne ho impostata una nuova. I requisiti della password sono cambiati e la mia vecchia parola d’ordine non li rispettava; tuttavia sarebbe stato carino, come utente, ricevere una email e magari saperlo.

Dopo di che ho finalmente proceduto all’acquisto. Dilemma: stampo il biglietto a casa o lo prelevo alle casse automatiche quando arrivo al cinema? Viene fuori che, se stampi il biglietto, non lo puoi prelevare alle casse automatiche e viceversa.

Capisco poco il perché della limitazione ma il punto vero è che sul sito bisogna stare bene attenti, per capirlo. Le informazioni potrebbero essere presentate meglio.

Nel frattempo scarico la *app* e scopro che, novità rispetto ai tempi andati, è possibile visualizzare il biglietto che potrà essere letto dalla maschera all’ingresso.

Ottima iniziativa, che rende ampiamente superflue la stampa del Pdf e le casse automatiche. E ancora più bizzarre le limitazioni di prima: in sostanza, alla lettera delle istruzioni, non si può entrare mostrando all’ingresso il Pdf del biglietto sullo schermo, mentre lo stesso biglietto, visualizzato dalla *app*, vale.

Il tutto con un sovrapprezzo di un euro e venti a biglietto. Acquistare online dovrebbe essere un vantaggio per chi vende e di conseguenza non una punizione per chi compra. Anche perché ci sono varie opportunità di sconto e promozione, anche consistenti, che non sono accessibili nell’acquisto online.

Un altro caso in cui il comportamento più efficiente e veloce, per tutti, viene scoraggiato. Sembrerebbe con intenzione.

Per quanto una sera al cinema in famiglia costi come tre mesi di Netflix, il grande schermo e la poltrona accogliente, con contorno di popcorn e *nachos*, hanno ancora il loro fascino.

Però uno sarebbe meno tentato di investire sullo schermo di casa invece di quello del cinema, se si sentisse trattato bene. Qui l’esperienza è straniante, non sai mai quello che può succedere al link successivo.