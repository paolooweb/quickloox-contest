---
title: "Volta la carta"
date: 2019-05-20
comments: true
tags: [Segall, Apple, Card, Cook, Jobs]
---
*[Volta la carta](https://www.rockol.it/testi/1339982/fabrizio-de-andre-volta-la-carta) si vede il villano…*

Dunque, Apple non fa più innovazione. Invece di lavorare esattamente al computer che volevo io come lo volevo io al prezzo che volevo io (oggi il criterio per definire l’innovazione sembra diventato questo), perde tempo con fregnacce come una carta di credito Apple. Steve Jobs non avrebbe mai fatto una cosa del genere.

Peccato che, racconta Ken Segall, già dirigente di Apple, [Steve Jobs ci ha provato eccome](https://kensegall.com/2019/05/20/the-ghost-of-apple-card-past/), nel 2004, quindici anni fa. Semplicemente non ci è riuscito.

Come si fa allora ad aver ragione? Beh, capovolgo il ragionamento. Apple non fa innovazione perché Tim Cook e compagni non riescono a creare niente di nuovo. Perfino il progetto della carta di credito Apple lo aveva già concepito Steve Jobs!

Ed ecco che la premessa è sempre vera. Se Jobs ha fatto una cosa, viene copiato. Se non l’ha fatta, non è innovazione.

*Volta la carta e il gallo ti sveglia…*
