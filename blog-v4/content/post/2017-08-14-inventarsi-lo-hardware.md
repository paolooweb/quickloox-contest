---
title: "Inventarsi lo hardware"
date: 2017-08-14
comments: true
tags: [Windows, Surface, Consumer, Report]
---
Con ordine: *Consumer Reports* [ha ritirato la raccomandazione](https://www.consumerreports.org/laptop-computers/microsoft-surface-laptops-and-tablets-not-recommended-by-consumer-reports/) di due modelli di Surface Laptop e Surface Pro a causa della loro inaffidabilità: blocchi del sistema e altri problemi.

Fino a qui, *no news*: *Consumer Reports* è un Altroconsumo americano, molto ascoltato e incompetente, come [si è già mostrato](https://macintelligence.org/posts/2010-07-19-la-presa-di-sale/) in altra occasione. Se invece di *Consumer Reports* fosse il gatto che passeggia sulla tastiera, sarebbe grosso modo uguale.

Microsoft [smentisce](https://blogs.windows.com/devices/2017/08/10/stand-behind-surface/#TaIE8VD8bQdGWeFD.97), come prevedibile, le conclusioni di *Consumer Reports*.

La cosa inizia a farsi interessante quando Paul Thurrott, commentatore Microsoft tra i più autorevoli, scrive apertamente che [la smentita di Microsoft manca di dati concreti](https://www.thurrott.com/mobile/microsoft-surface/132764/microsoft-mounts-defense-surface-reliability) (*hard evidence*).

Si fa ancora più interessante quando salta fuori che Microsoft dà la colpa a Intel e precisamente ai processori Skylake usati nelle macchine, mentre invece i problemi sarebbero dovuti ai *driver* sviluppati da Microsoft. Deficitari perché sviluppati troppo in fretta, per riuscire ad arrivare sul mercato in tempi stretti. Citazione:

>Il fiasco di Skylake è saltato fuori internamente quando l’amministratore delegato Satya Nadella si è incontrato con Lenovo l’anno scorso e ha chiesto all’azienda cinese, oggi il più grande produttore di PC al mondo, come stesse affrontando la questione della scarsa affidabilità dei chip Skylake. Lenovo era confusa. Nessuno stava avendo problemi, è stata la risposta.

Anche se ne ha scritto *Consumer Reports*, che ha l’autorevolezza del mago Merlino, Surface Laptop e Surface Book hanno *veramente* problemi di affidabilità vastamente superiori alla media. Ancora più grave se si considera che il loro prezzo di vendita è nella fascia alta: competono con i portatili Apple, non con i catafalchi che si trovano in vendita al chilo come le patate, senza offesa per le patate.

Se quanto riporta Thurrott (che l’autorità e la reputazione le ha) è autentico, l’amministratore delegato di Microsoft era convinto che il problema fosse esterno. Invece è interno ed è stato nascosto sotto il tappeto tanto che neanche il vertice dell’azienda ne era consapevole.

Non per altro, ma Microsoft ha iniziato a produrre hardware dall’altroieri. E vende macchine allo stesso prezzo di quelle Apple, però con esperienza e capacità, come dirlo senza urtare sensibilità, inferiori. Non ci si inventa da zero produttori di hardware negli anni dieci, nemmeno se ti chiami Microsoft. O forse, soprattutto, visto che finisci per inventarti pure le colpe per i malfunzionamenti di quello che produci.