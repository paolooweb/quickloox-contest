---
title: "L’isola che non c’è più"
date: 2023-07-03T01:11:12+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [Myst, The Verge, Atari 2600, Myst Mobile]
---
Trovo impossibile trascurare il *demake* che un professore universitario sta producendo per hobby di [Myst](https://macintelligence.org/posts/2023-01-27-il-fascino-del-vuoto/): una [versione per Atari 2600](https://arstechnica.com/gaming/2023/06/30-years-later-myst-demake-for-atari-2600-reminds-us-how-far-weve-come/). Fa venire le vertigini perché *Myst* ci ha preso alla gola per quei panorami, quegli effetti grafici, quel realismo impensabile alla sua uscita, che oggi chiunque darebbe per scontato ma in quegli anni era letteralmente impensabile. L’audio, del mare, del vento, degli ingranaggi, dei messaggi vocali lasciati dai personaggi. E tutto questo viene reso su una console dalla risoluzione di centonovantadue per centosessanta pixel. Perfino un Sinclair Spectrum aveva una risoluzione maggiore.

Eppure sta avvenendo e, nel procedere lungo l’articolo di *The Verge*,ho scoperto che c’è un precedente clamoroso: lo stesso professore universitario ha già prodotto un analogo demake di *Myst* per Apple ][, che risiede in tre floppy disk da 140 kilobyte.

In questi giorni sto pensando se e quale gioco provare ad adottare per l’estate e certamente, se fosse *Myst*, non avrei dubbi: tra una cartuccia per Atari 2600 e la app [Myst Mobile](https://macintelligence.org/posts/2023-02-10-buona-la-ennesima/) che mi aspetta già pronta su iPad, non comincio neanche a pensarci.

Questi exploit di divertimento informatico, tuttavia, conservano un fascino e anche un’ironia che forse nessun’altra forma di intrattenimento può uguagliare. *Myst* è stato portato negli anni sulle piattaforme più varie. Questa le batte tutte, sicuramente. Anche se diventa difficile anche solo distinguere i contorni dell’isola.