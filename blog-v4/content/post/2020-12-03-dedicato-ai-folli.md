---
title: "Dedicato ai folli"
date: 2020-12-03
comments: true
tags: [Think, Different, M1, Z80, 6502, Intel, Amd, Rob, Out-of-Order, Re-Order, Buffer, Engheim]
---
Continuo a leggere cose su M1 per via di un effetto speciale: c’è più divulgazione su come funzionano i processori di quanta se ne sia mai vista dai tempi del 6502 di Apple ][. Parlare di che cosa c’è dentro un processore è diventato interessante, perché in questo caso gli altri processori sembrano indietro di un giro e forse più.

Parlare di *processori* è riduttivo perché, come [scrive Erik Engheim](https://erik-engheim.medium.com/why-is-apples-m1-chip-so-fast-3262b158cba2),

>M1 non è una CPU, è un intero sistema composto da numerosi chip su una ampia piattaforma di silicio. La CPU è solo uno di questi chip.

Però ci siamo capiti e possiamo accettare la semplificazione del discorso.

La disamina di Engheim va ancora più in profondità delle precedenti ma si mantiene pienamente comprensibile al lettore comune e affronta temi molto concreti: che differenze ci sono tra M1 e i soliti processori di Intel e Amd; che cosa possono o non possono fare queste ultime per stare dietro a M1.

Le differenze sono numerose e notevoli, una fra tante questa:

>Le istruzioni in codice macchina sono suddivise in micro-operazioni da quello che chiamiamo un decodificatore di istruzioni. Più decodificatori abbiamo, più possiamo suddividere istruzioni in parallelo, più riempiamo velocemente il Rob.

Il Rob è il Re-Order Buffer, spazio nel quale alloggiano le micro-operazioni già elaborate che ne attendono altre elaborate in parallelo ma con tempi più lunghi, dalle quali dipendono e ultimate le quali il processore avrà effettivamente eseguito i comandi richiesti.

>Qui è dove vediamo le differenze importanti. I nuclei di elaborazione [core] dei processori più grossi e cattivi di Intel e Amd possiedono quattro decodificatori, quindi possono decodificare quattro istruzioni in parallelo per sputare fuori micro-operazioni.

Il senso di tutto è che, se devo calcolare il volume della sfera (quattro terzi pi greco erre tre), posso calcolare contemporaneamente più operazioni che posso e fare prima. Un decodificatore lavora su quattro fratto tre; un decodificatore su erre al cubo; uno sulla prima moltiplicazione per pi greco e uno sulla seconda. *Quasi* contemporaneamente, perché a volte devo aspettare un risultato per poterne calcolare un altro e perché non tutte le operazioni richiederanno lo stesso tempo. Ma ci metto meno che a eseguire le operazioni una dietro l’altra.

>Solo che Apple ha la follia di otto decodificatori. Inoltre, il Rob è circa tre volte più grande. In pratica può contenere il triplo degli altri. Nessun altro grande produttore di chip offre altrettanti decodificatori nelle proprie Cpu.

Beh, Intel e Amd aumenteranno il numero di decodificatori. Semplice, no?

No.

Il pezzo spiega molto bene la difficoltà strategica e di esecuzione dei concorrenti di M1. Niente di irrisolvibile in linea di principio, ma certamente non domattina e non a costo zero. Qualsiasi soluzione decidano di adottare, non possono banalmente fare più in grande quello che hanno sempre fatto. Devono fare qualcosa di diverso e non poco.

Il pezzo di Engheim in originale fa

>But Apple has a crazy 8 decoders.

E a me è subito venuto in mente *Here’s to the crazy ones*, *questo filmato lo dedichiamo ai folli*, *perché solo coloro che sono abbastanza folli da pensare di poter cambiare il mondo lo cambiano davvero*.

[Think Different](http://www.storiediapple.it/apple-e-il-think-different.html) batte pensiero unico di x86 otto decodificatori a quattro (per tacer del resto).