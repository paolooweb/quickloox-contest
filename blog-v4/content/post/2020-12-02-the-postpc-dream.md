---
title: "The PostPC Dream"
date: 2020-12-02
comments: true
tags: [Asymco, Mac, iPad, Jobs, iPhone, watch, M1]
---
Prima venne Steve Jobs a [parlare di furgoni e automobili](https://www.youtube.com/watch?v=YfJ3QxJYsw8), PC e postPC, con l’arrivo di iPad.

<iframe width="560" height="315" src="https://www.youtube.com/embed/YfJ3QxJYsw8" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Poi ci fu il momento in cui iPhone doveva portarsi via tutta la torta e Apple smettere di occuparsi delle minuzie.

Poi, perfino watch. I *servizi*. Le fughe dei professionisti che abbandonavano la nave prossima ad affondare. La trasformazione, lo aveva detto Jobs, per qualcuno avrebbe potuto essere *uneasy*, non facile.

Apple potrebbe essere l’azienda che più di tutte, negli ultimi anni, ha remato contro l’idea di PC. E, per qualcuno, ha smesso di occuparsi dei Mac.

Non è meraviglioso che le vendite di Mac ora siano il doppio di quelle di dieci anni fa e questo alla vigilia della diffusione di massa di M1, che hanno tutte le carte in regola per prendersi ulteriore spazio?

Asymco va forte perché da [un post apparentemente glaciale](http://www.asymco.com/2020/11/30/post-pc/), tutto trend e mercati, estrae verità che colpiscono. Siamo più che mai all’inizio dell’era postPC… e chi l’ha inaugurata ha creato le migliori premesse per l’affermazione di Mac, personal computer per eccellenza.

Da sogno.

Il titolo è [un omaggio ai Pink Floyd](https://www.youtube.com/watch?v=OAdwirnAtFc).

<iframe width="560" height="315" src="https://www.youtube.com/embed/OAdwirnAtFc" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>