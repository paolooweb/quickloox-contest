---
title: "Editor e automazione"
date: 2017-08-09
comments: true
tags: [Snell, Six, Colors, Editorial, BBEdit]
---
Anche il sottoscritto, [come Jason Snell di *Six Colors*](https://sixcolors.com/post/2017/08/my-ios-writing-app-of-the-moment-is-editorial/) (ancorché con ben più modestia), utilizza [Editorial](http://omz-software.com/editorial/) per le proprie sessioni di scrittura su iPad (compresa questa).

Snell descrive bene e sveltamente il perché, che riassumo così: Editorial ha alcuni punti di forza fondamentali. Dopo di che ha anche alcune debolezze e Snell svolge un ottimo lavoro di confronto rapido e concreto con altre *app* più versatili e dotate.

Solo che Editorial ha una possibilità fondamentale: *l’automazione*.

Il sogno di tutti noi che scriviamo su iPad senza essere autori di *fiction* o scienziati – per loro c’è [LaTeX](https://www.latex-project.org/), praticamente la divinità massima nell’Olimpo dei sistemi di scrittura – è riuscire a fare sulla tavoletta quello che ci consente [BBEdit](http://www.barebones.com/products/bbedit/).

Editorial è lontanissimo da questo obiettivo, proprio come ogni altra *app* in circolazione. In compenso la sua architettura contiene i *workflow*: già pronti, scritti da altri o scritti da noi, consentono di ridurre il divario, impercettibilmente, un passo per volta, con fatica e in modo macchinoso. Ma concretamente.

Editorial è L’unica *app* nella propria categoria che può diventare migliore ogni giorno che passa senza dover attendere un aggiornamento e che potrebbe conservare a lungo il proprio valore anche se cessasse il supporto da parte di chi l’ha creata.
