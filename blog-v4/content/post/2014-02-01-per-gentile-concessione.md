---
title: "Per gentile concessione"
date: 2014-02-01
comments: true
tags: [TeamViewer, LogMeIn]
---
Se si legge questo *post* è per gentile concessione di TeamViewer, applicazione di controllo remoto rimasta gratis per uso personale dopo che LogMeIn è diventata esclusivamente a pagamento.<!--more-->

Non ricordavo quanto fosse semplice e immediata per supplire ai (miei) ritardi infrastrutturali nel fare in modo di pubblicare sempre e ovunque su questa pagina. Lo è.

E vorrei anche dire che sono stato in mostra senza l'audioguida; era inclusa nella *app* della mostra. La campagna di [Dungeons & Dragons](http://dnd.wizards.com) procede amministrata a distanza dalla Francia grazie a iPad, Roll20.net – chiedo scusa se non applico i link, li aggiusterò appena torno a casa – e iPlay4e. Tutto è stato prenotato, coordinato, pagato, sistemato tramite iPad e iPhone. Viva le *app*, viva iOS.