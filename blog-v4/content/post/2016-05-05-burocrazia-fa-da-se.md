---
title: "Burocrazia fa-da-sé"
date: 2016-05-05
comments: true
tags: [Wikipedia]
---
Non molto tempo fa ho stilato un *post* lunghetto per spiegare [la mia posizione molto, molto ambivalente verso Wikipedia](https://macintelligence.org/posts/2015-11-04-very-good-very-bad/).<!--more-->

Ho cercato, come sempre, di essere semplice e comprensibile, nonché completo nell’esposizione per quanto consentono il tempo e le risorse. Certo non è uscito un documento scientifico, un classico *paper*: ho citato molti fatti, ma la gran parte dell’esposizione è mia opinione.

Ora però inviterei alla (lo so, impegnativa) rilettura alla luce di uno studio, questo sì, scientifico, appena [diffuso dalla Indiana University](http://www.mdpi.com/1999-5903/8/2/14/html). Traduco parte dell’*abstract*:

>Nonostante la reputazione di ente governante *ad hoc* di cui gode Wikipedia, abbiamo scoperto come la sua evoluzione normativa sia estremamente conservatrice. Gli utenti arrivati per primi creano regole che dominano la rete e persistono nel tempo. Questo nucleo di norme governa sia le interazioni con le persone sia quelle con il contenuto, usando principî astratti come neutralità e verificabilità e presumendo la buona fede. Con il crescere della rete, i gruppi di regole si disaccoppiano topologicamente l’uno dall’altro, aumentando la coerenza sematica. Nel loro insieme, questi risultati suggeriscono che l’evoluzione della rete di regole di Wikipedia sia simile a quella dei sistemi burocratici nati prima dell’età dell’informazione.

Ho scritto mie opinioni, ma adesso c’è pure un’analisi scientifica che le rafforza. Non per dire che ho ragione; per dire che gli argomenti comunemente usati dai wikipediani nella cerchia degli autoeletti, non reggono. Chi può, chi vuole, faccia qualcosa. Io continuo ad auspicare milioni di studenti impegnati a scrivere milioni di pagine e, nel farlo, rendere irrilevanti i mandarini al governo di quella che è una burocrazia autoconservativa.