---
title: "Comprendiamo benissimo"
date: 2013-10-20
comments: true
tags: [Microsoft]
---
Leggo [su Internet Evolution](http://www.internetevolution.com/author.asp?section_id=466&doc_id=268847&_mc=MP_IWK_EDT_STUB) che Surface di Microsoft sarebbe un *gioiello incompreso*:

>Soddisfa un’esigenza non contemplata dalla generazione attuale di portatili e tavolette.<!--more-->

Poi leggo Twitter e penso al caso singolo, che succede anche nelle migliori famiglie.

<blockquote class="twitter-tweet"><p>Windows 8 -&gt; Windows 8.1: sistema autodistrutto grazie :D (meno male che uso OSX :D ) <a href="https://twitter.com/search?q=%23windows&amp;src=hash">#windows</a> <a href="https://twitter.com/search?q=%23microsoft&amp;src=hash">#microsoft</a> <a href="https://twitter.com/search?q=%23chepena&amp;src=hash">#chepena</a> <a href="http://t.co/TaCYm6NR3w">pic.twitter.com/TaCYm6NR3w</a></p>&mdash; Daniele Savi (@danosavi) <a href="https://twitter.com/danosavi/statuses/391603450993065985">October 19, 2013</a></blockquote>
<script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>

Non è un caso singolo e *Annoying stuff I figured out (or am trying to)* [commenta in merito](http://kickthatcomputer.wordpress.com/2013/10/19/windows-rt-8-1-upgrade-fails-with-boot-configuration-error/):

>Noioso che Windows RT si possa aggiornare solo da App Store. Probabilmente non è un problema per chi abbia una sola tavoletta. Leggermente fastidioso per chi ne abbia due, ché deve scaricare due volte l’aggiornamento. Se una ditta ne ha molte deve scaricarlo un sacco di volte.

Fino a che *Ars Technica* [informa](http://arstechnica.com/gadgets/2013/10/windows-rt-8-1-update-temporarily-pulled-due-to-a-situation/) del ritiro dell’aggiornamento da parte di Microsoft, per risolvere una *situazione* che impedisce la riuscita della procedura a un numero *limitato* di utilizzatori. Il commento:

>Definire ciò imbarazzante per Microsoft è in qualche modo una sottovalutazione. Mentre i PC presentano una diversità straordinaria di hardware, software e driver e questo può essere di ostacolo a un aggiornamento pulito, gli apparecchi Windows RT hanno una diversità estremamente limitata. Aggiornarli dovrebbe essere a prova di bomba. Molto deludente che non lo sia.

*Gioiello incompreso*? Direi che lo abbiamo compreso fin troppo bene.