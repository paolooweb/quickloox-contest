---
title: "Parlare resta gratis"
date: 2016-01-29
comments: true
tags: [app, Android, iPhone, iOS, Parks, Siri, Google, Now]
---
Si è [capito da tempo](https://macintelligence.org/posts/2011-11-27-esseri-inutili/) che i detentori di un iPhone acquistano online più disinvoltamente dei possessori di un Android.<!--more-->

Ultimamente, nonostante il rapporto tra base installata di Android e iPhone continui ad aumentare a favore del primo, si è anche saputo che le *app* per iOS [generano il 75 percento in più](http://9to5mac.com/2016/01/20/app-store-ios-downloads-vs-android-revenue/) del fatturato originato dalle *app* per Android.

A fare notare queste cose ci si sente rispondere che chi acquista Apple ha più soldi, con Android si risparmia eccetera.

Poi arriva questa [ricerca di Parks Associates](http://www.parksassociates.com/blog/article/pr-01272016) a mostrare che c’è un divario consistente tra l’uso di riconoscimento vocale su iOS (stile Siri) e quello su Android (stile Google Now), a favore di iOS.

Riconosco che in molte situazioni far pagare chi apre bocca per dire qualcosa sarebbe una eccellente misura di contenimento e civiltà. A parte questo, mi sembra che parlare sia gratis. Ma non c’è verso, qualsiasi metrica di consideri, iOS è sempre davanti.

Non sarà che c’è una questione di design e che a volte quando qualcosa costa meno, è perché vale meno?