---
title: "Un nuovo inizio. O due"
date: 2019-05-28
comments: true
tags: [Troughton-Smith, NeXTStep, OpenStep, OSX, macOS, Carbon, Cocoa, AppKit, UIKit, Apple, NeXT, Snell, Wwdc, macOS, iOS, Cook]
---
Il tempo vola e non mi rendo conto di quanto sia imminente [Wwdc](https://developer.apple.com/wwdc19/). Me lo ricorda bene Jason Snell, con un [pezzo stranamente utile da leggere](https://www.tomsguide.com/us/apple-wwdc-2019-predictions,news-30164.html). Di solito gli articoli di anticipazione sulla Wwdc contengono chiacchiere liquide e nulla più.

Anche se non avesse [letto Steve Troughton-Smith](http://www.macintelligence.org/blog/2019/05/25/convergenze-e-reminiscenze/), Snell la saprebbe lunga già da solo, per capire come inevitabilmente questa sarà una annata storica, in cui comincia l’unificazione di Mac e iOS come piattaforme applicative… ma non come sistemi operativi, la scommessa che hanno fatto da anni quelli che la fanno facile e perdono regolarmente, essendo la materia complicata.

Comincia quest’anno una transizione che ne durerà molti e probabilmente non ne comincia un’altra, puramente hardware, per fare le cose difficili una per volta. A meno che, naturalmente, Tim Cook non annunci *one more thing* per segnalare già da quest’anno l’inizio dell’[abbandono dei processori Intel su Mac](http://www.macintelligence.org/blog/2019/05/27/bye-bye-intel/).

Non me lo aspetto da lui, non è tipo da rovesciare i tavoli. Però non so a che punto sono gli ingegneri Apple. Metti che siano più avanti di come suppongo…