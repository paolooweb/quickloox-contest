---
title: "Sottocoperta"
date: 2022-09-26T01:00:14+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [macOS, CoreServices, QuickLook Simulator, XCode, Monterey]
---
Quando sono particolarmente annoiato, potrei aprire */System/Library/CoreServices/* e, se è passato abbastanza tempo dalla volta precedente, andare a caccia di funzioni offerte da macOS sotto forma di app. Come Control Center, Game Center, Clock, Screen Time eccetera.

Dentro Monterey mi sono divertito, era tanto che non guardavo e il raccolto è stato florido. Una sorta di collezionismo di app misconosciute.

Ma ieri, dopo che procastinavo da più di qualche giorno, ho riavviato Mac con il pieno di Aggiornamento Software. Tra 12.5.1, 12.6, Safari, strumenti da Terminale per Xcode e qualcos’altro ancora.

Dopo pranzo ho ritrovato Mac riavviato e riaggiornato. In primo piano funzionava… QuickLook Simulator, che certamente non avevo lasciato aperto al momento di riavviare. Neanche sapevo che esistesse.

Il divertimento ricavabile dall’esperienza diretta di QuickLook Simulator è limitato. Scoprire la sua esistenza in modo serendipitico, senza averla cercata, è conferma che macOS ha ancora tanto da offrire anche al curioso e all’esploratore, come tanti anni fa, nonostante l’aumentata complessità e profondità del sistema.

Il veliero è affascinante da vivere in plancia, ma anche sottocoperta si respira un’aria speciale, per chi la vuole cogliere.