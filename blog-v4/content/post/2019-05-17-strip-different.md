---
title: "Strip Different"
date: 2019-05-17
comments: true
tags: [Jobs, Apple, Park, MacRumors, Ive]
---
Più che la festa per l’apertura formale di Apple Park, più che il ricordo di Steve Jobs, più che il concerto – pare – di Lady Gaga, quello che mi porto a casa dal [resoconto di MacRumors](https://www.macrumors.com/2019/05/09/apple-park-arch-may-17-event/) è che la struttura del palco a strisce arcobaleno è stata progettata dal team di Jony Ive.

È fatta per essere smontata e montata in tempi molto brevi. Nonché per essere riconoscibile da altre, un *palco Apple*.

Un palco progettato da un’azienda impegnata per mestiere su tutt’altro. Che però non disdegna di avere le idee più che chiare su [come debba essere costruito il proprio quartier generale](https://macintelligence.org/posts/2017-02-12-sta-nei-dettagli/). E così [le scale di vetro dei propri negozi](https://patents.google.com/patent/USD478999S1/en). Oppure, appunto, lo *stage* per uno o più eventi speciali.

Si può pensare, nel bene o nel male a scelta, che sia un’azienda come tutte le altre?
