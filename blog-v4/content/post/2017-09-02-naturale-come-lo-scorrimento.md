---
title: "Naturale come lo scorrimento"
date: 2017-09-02
comments: true
tags: [Touch, Bar, Atom]
---
Come [scrivevo](http://www.macintelligence.org/blog/2017/04/04/corsi-ricorsi-e-convenzioni/), la Touch Bar dei MacBook Pro equivale allo scorrimento naturale venuto in auge con gli schermi *touch*: né giusto né sbagliato, semplicemente una convenzione più adatta ai tempi.<!--more-->

Tra qualche tempo nessuno si porrà più domande in proposito. Succederà quando si saranno moltiplicati gli articoli come questa [raccolta piccola, ma significativa, di personalizzazioni della Touch Bar](https://dzone.com/articles/bringing-touch-bar-support-to-the-atom-text-editor) per l’editor di testo [Atom](https://atom.io).

Le persone si accorgono che la Touch Bar è personalizzabile e si adatta al contesto di utilizzo. Iniziano a sfruttarne le vere possibilità. L’utilità reale dell’oggetto inizia a palesarsi e sempre più persone ne approfittano. Semplice e naturale, come lo scorrimento.