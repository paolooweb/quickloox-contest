---
title: "Uscite di sicurezza"
date: 2021-05-23T23:33:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [cybersecurity, iPhone, FaceID, Spid, 1Password] 
---
Oggi dovevo accreditare un centinaio di euro sul sito del servizio mensa per la scuola della primogenita.

Ci si può registrare con nome e password oppure entrare con Spid. Preferisco quest’ultimo sistema perché, sì, registrarsi è un piacere pari a quello di una colonscopia e, se qualcosa si rompe nel meccanismo, meglio sei mesi di sedute dal dentista.

Nel mezzo di queste due parentesi, tuttavia, il servizio funziona bene e in questo è essenziale il potersi autenticare con FaceID al posto di scrivere l’ennesimo codice.

Entrato con Spid, che apre la porta al servizio mensa ma anche a qualunque servizio della pubblica amministrazione e come tale è considerato dall’Italia un sistema di identificazione assai sicuro, accredito il denaro e, per farlo, posso usare solo una carta di credito. Essere entrato con Spid non mi serve a niente: il sito demanda la sicurezza della transazione alla società che emette la carta. Ma allora, che sicurezza tutela l’accesso con Spid?

*Le misure di sicurezza digitale della pubblica amministrazione non hanno alcun nesso con la sicurezza effettiva; sono pensate programmaticamente al solo scopo di ostacolare, rallentare, intralciare l’utente.*

La banca (semplifico) dietro la carta di credito fa del suo meglio per rendere sgradevole l’operazione. Prima vogliono tutti i dati della carta, poi mandano l’Sms con il codice di approvazione, poi vogliono quello creato dall’utente. Qualcosa nel caso abbiano rubato la carta ma non il telefono, qualcosa se fosse stato rubato il telefono ma non la carta, qualcos’altro per evitare un [attacco man-in-the-middle](https://www.cybersecurity360.it/nuove-minacce/attacco-man-in-the-middle-tutti-i-modi-possibili-e-come-difenderci/) e così via.

*La banca non pensa in termini di sicurezza globale, ma si accorge di un problema per volta e, invece di allestire una soluzione globale, accatasta una soluzione sull’altra per affrontare un problema dopo l’altro*.

Non basta che io sia entrato via Spid sul sito? Se la transazione dipende da codici numerici, a che serve inviare la data di scadenza della carta? Perché non posso metterci la faccia e autenticarmi grazie a FaceID, più sicuro di qualsiasi Pin e controPin?

Alla fine la pagina mostra un *gateway error 504* o qualcosa del genere. Per esperienza so che la transazione è riuscita, perché alla fine della transazione su quel sito con quella banca esce *sempre* quell’errore. Sospetto che un hacker ben motivato, su questa circostanza, andrebbe a nozze, alla faccia di codici e password.

*Il sito è sempre molto meno sicuro di quanto lasci intendere il suo sistema di autenticazione, che serve solo a salvare le apparenze ed eventualmente scaricare eventuali responsabilità*.

Neanche mi ricollego al sito per controllare che davvero la transazione sia avvenuta. Arriva una email di conferma, con dentro tutti i dettagli della transazione e due terzi del numero della carta di credito. La email viaggia *in chiaro*. Anche qui, per un hacker motivato, poter collegare con certezza una transazione a un utente è una bella occasione di divertimento.

*Il responsabile della sicurezza è sempre una persona solitaria, intorno alla quale l’azienda commette qualsiasi errore possa giustificare una cospicua ignoranza interna di che cosa implichi garantire la sicurezza*.

Alla fine arriva sempre qualcuno a dire *sì, ma ti conviene usare [1Password](https://1password.com)* (sostituire con il password manager prediletto). Certamente è molto comodo; poi consolida i mille punti deboli della mia sicurezza personale (perchè ogni Pin, ogni codice, ogni password, ogni autenticazione è un punto critico suscettibile di attacco) in un unico punto debole, la cui eventuale caduta significa la resa incondizionata al pirata di turno.

*Alla fine dei conti, qualunque procedura di sicurezza ruota attorno a un singolo codice*.

*Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile.*               