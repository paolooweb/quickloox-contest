---
title: "Il costo delle ossessioni"
date: 2015-10-07
comments: true
tags: [Enel, iMac, consumi, energia]
---
Lascio la parola ad **Alessandro**.

**Leggo ancora di tanto in tanto qua e là** di diatribe se lasciare Mac acceso, spento o in *standby*.<!--more-->

Vivo da solo e posso fare calcoli abbastanza empirici ma efficaci avendo un controllo totale di quanto acceso e spento, per quanto tempo, come e dove.

A luglio ho pagato 45 euro di bolletta Enel distribuzione (maggio-giugno), contratto domestico 3kw più standard possibile.

Ho un frigorifero, forno a gas, faccio una lavatrice e una lavastoviglie a settimana (sono collegate alla presa dell’acqua calda della caldaia), schermo plasma di cinque anni orsono full HD 50” (sei ore acceso al giorno mediamente salvo la domenica che sono sempre fuori, quando l’energia costa meno), esclusivamente lampadine Led, un decoder Ddt sempre acceso che uso per registrare su chiave Usb, alimentatore iPad mini Retina che uso tutte le notti per ricaricare, un piccolo congelatore a pozzetto in cantina, *boombox* Logitech sempre alimentato per ascoltare gli *streaming* in bagno, la caldaia che è elettronica (scheda e ventole tiraggio forzato vanno ad elettricità), idropulsore per denti (due volte al giorno), spazzolino elettrico caricato una volta a settimana, modem-router, forno a microonde dall’uso sporadico ed ovviamente, ultimo ma non meno importante, un iMac 21” (2011) perennemente acceso; vanno a nanna solo schermo e disco quando possibile (qualcosa di marginale potrebbe comunque essermi sfuggito).

Decisamente la questione se lasciarlo acceso (l’iMac) o spento od in *standby* per risparmiare energia elettrica è superata da eoni, se non divenuta una coglioneria bella e buona anche considerato che **parte della bolletta è composta da costi fissi.**

Costa più preoccuparsi di salvare il mondo spegnendo la lucina dello *standby* di quello che è il risparmio. Mi pare definitivo.