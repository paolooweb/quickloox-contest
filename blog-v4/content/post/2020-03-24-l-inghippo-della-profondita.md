---
title: "L’inghippo della profondità"
date: 2020-03-24
comments: true
tags: [chat, videoconferenza]
---
Non mi ero reso conto del problema e potrebbe anche esserci una soluzione.

Alcuni bimbi della scuola materna si sono visti ieri tramite [Jitsi](https://jitsi.org) e naturalmente c’erano le relative famiglie, un genitore, a volte due, fratellini sorelline e animali, viventi o simulacri.

(Sempre più convinto: Jitsi per il personale dove non ci sia modo di usare FaceTime, [Valarea](https://www.valarea.com) per il professionale. Il resto è dietro).

Tornando ai bimbi, betatester come nessuno, il problema dei moderni sistemi di videoconferenza è che funzionano meglio *se si parla uno alla volta*. Servono disciplina e diligenza, nonché tanta empatia e controllo per non interrompere (e sapersi interrompere). Chiaro che magari adulti motivati possono provarci con profitto, mentre con bambini (e relative famiglie) è impossibile. Tutti parlano con tutti.

La parte interessante è che i genitori hanno vissuto il momento con un risvolto comportamentale proprio dei ritrovi fisici: in occasione di una festa, di una merenda, un picnic tra più persone, può capitare che una persona parli e tutti ascoltino. Mi sembra però che accada in pochi momenti.

Nella maggior parte del tempo, *ciascuno parla con una persona diversa* o al massimo con poche persone. Nel macrogruppo si creano i capannelli, tendenzialmente di due-cinque persone. Nessuno ascolta tutto quello che viene detto nel raduno, ma ascolta e si rivolge a un insieme ristretto di persone. Nei capannelli c’è un turnover; si formano e si disfano, c’è chi passa dall’uno all’altro, crescono o si riducono.

È chiarissimo che un sistema attuale di videoconferenza non ha alcuna idea di questo. Ma è così che ci si parla in grandi gruppi.

Ambienti virtuali immersivi come [Second Life](https://secondlife.com) hanno affrontato il problema radicalmente, con la creazione di una spazialità che simula in tutto quella ordinaria. D’altronde è chiaro che, prima di avere famiglie medie che in modo semplice e familiare portano in un prato virtuale i propri avatar bimbi compresi, passano ancora cinquant’anni. Dunque?

Il prossimo sistema di videoconferenza che immagino permette di disporre gli avatar dei partecipanti in profondità su un piano 2,5D (tre dimensioni simulate sullo schermo), più vicini, più lontani, secondo l’interesse del momento. Quando le persone coinvolte manifestano un interesse simile, si forma un capannello. Il sistema aggiusta automaticamente l’audio di ciascun partecipante in funzione della disposizione degli avatar: più flebili quelli più lontani, più forti quelli vicini. Una funzione tipo forchetta sul bicchiere permette di chiedere l’attenzione collettiva, con un meccanismo antiabuso.

È complesso ma fattibile e ho già ragionato su vari dettagli che non sto a esporre qui per non allungare troppo. Da nessuna parte ci vuole *rocket science* comunque, basta buonsenso. Sistemi come [Discord](https://discordapp.com) consentono di aggiustare manualmente l’audio di ciascun partecipante a una chat. Si tratta di aggiungere logica che automatizzi la cosa in funzione di un criterio. Complesso, fattibile.

Ecco, se lo brevetti non pretendo per forza le royalty, ma almeno la citazione come pioniere sì e un caffè, grazie.