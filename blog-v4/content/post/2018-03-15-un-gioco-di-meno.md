---
title: "Un gioco in meno"
date: 2018-03-15
comments: true
tags: [FreeCiv]
---
Sì, sono maniaco di [FreeCiv](http://www.freeciv.org) e faccio [quel che posso](https://macintelligence.org/posts/2016-11-17-una-falla-nella-civilizzazione/) per sostenerlo.<!--more-->

Non ottengo grandi risultati: il gioco compilato è assente da Mac da diverse versioni e ultimamente è stata chiusa la versione web, che assicurava una universalità. Il motivo: se ne occupava un solo sviluppatore, che ora non ha più tempo.

Il mondo *open source* è fatto anche di evoluzione ed è fisiologico che un progetto possa avere una fine. Ma non è il caso di FreeCiv, che continua a essere aggiornato e recentemente ha anche portato il codice [su GitHub](https://github.com/freeciv/freeciv/), oggi la norma. Solo, non ci sono sviluppatori con tempo e voglia per compilare una versione Mac. E adesso, web.

Sembrerà poca cosa. Invece, quando un buon software perde colpi. ci si perde tutti, compresi i non interessati.