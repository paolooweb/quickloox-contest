---
title: "Strumenti di ribellione"
date: 2015-02-06
comments: true
tags: [Mac, Apple]
---
Mi scrive (e lo ringrazio) **Matteo**, allegando la foto di una pagina del *Corriere della Sera* che qui riproduco ridotta al dettaglio più significativo.<!--more-->

L’oggetto della sua *mail* è *Dalla foto non poteva essere altrimenti*. E difatti. Apple oggi è una multinazionale globale *mainstream* in molte sue manifestazioni. Ma un Mac è tuttora simbolo *concreto* della possibilità di non arrendersi alla corrente.

 ![Scuola ribelle, computer da ribelli](/images/scuola-ribelle.jpg  "Scuola ribelle, computer da ribelli") 