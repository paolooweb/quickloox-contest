---
title: "Il silenzio del rumore"
date: 2022-03-08T14:30:43+01:00
draft: false
toc: false
comments: true
categories: [Hardware, Web]
tags: [Apple, Ars Technica, Bloomberg, melamorsicata, Franco Battiato, Il silenzio del rumore]
---
Mancano poche ore agli annunci ufficiali di Apple e si fatica a capire l’ossessione per sapere che cosa verrà presentato, come se un anticipo di poche ore servisse a chissà che. Forse qualcuno vuole giocare in Borsa sulla base di quello che scrive un sito alimentato a banner pubblicitari, oppure ha tempo libero veramente abbondante.

*Ars Technica*, che sarebbe anche una cosa mediamente seria da leggere, titola [la lunga attesa di un nuovo Apple Thunderbolt Display può essere finita presto, oppure no](https://arstechnica.com/gadgets/2022/03/rumors-of-a-new-more-affordable-apple-monitor-just-wont-go-away/) che come informazione è perfetta: non si può sbagliare in alcun caso.

*Bloomberg* scrive [che cosa aspettarsi dall’evento](https://www.bloomberg.com/news/newsletters/2022-03-06/apple-aapl-peek-performance-march-8-event-5g-iphone-se-ipad-air-mac-updates-l0fdz98z) e inserisce questa perla:

> Mi hanno detto che è possibile venga presentato un quarto prodotto “wild card” che potrebbe essere un monitor di nuova generazione o iMac Pro o un Mac Pro nuovo e più piccolo.

Praticamente può essere qualsiasi cosa, oppure no, non lo presentano. Vale tutto.

Ammiro chi giunge al punto di [raccogliere questo materiale informativo di capitale importanza](https://twitter.com/melamorsicata/status/1501111252092870659):

<blockquote class="twitter-tweet"><p lang="it" dir="ltr">Oggi c’è <a href="https://twitter.com/hashtag/AppleEvent?src=hash&amp;ref_src=twsrc%5Etfw">#AppleEvent</a> <br><br>Ecco il menù<br><br>👓la raccolta dei rumors è qui <a href="https://t.co/DoA2w0YH7c">https://t.co/DoA2w0YH7c</a><br><br>🐥 alle 19:00 live tweet su questo account<br><br>🍎 dalle 21.00 approfondimenti nel blog</p>&mdash; Kiro (@melamorsicata) <a href="https://twitter.com/melamorsicata/status/1501111252092870659?ref_src=twsrc%5Etfw">March 8, 2022</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

Qual è il senso? Mettere le crocette, questo sì, questo no? E una volta fatto? Tutto come prima, a parlare dei fondamentali [rumors per il 2023](https://www.notebookcheck.net/All-Apple-iPhones-to-be-notch-free-by-2023-according-to-display-expert.604804.0.html).

Rumore che contiene il nulla, riempie il canale ma porta l’equivalente del silenzio.

Davvero, da chi segue questa roba vorrei sapere, di cuore, se *ti sei mai chiesto quale funzione hai*.

<iframe width="560" height="315" src="https://www.youtube.com/embed/M0RGfXpvxfA" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>