---
title: "Vivere all’aggiornata"
date: 2021-01-11
comments: true
tags: [ iPhone, Heic, iOS, Mac, Big, Sur]
---
Oggi non sono riuscito a spedire alcune foto da iPhone tramite una normale email. Figuravano presenti nel messaggio, che però è arrivato vuoto al destinatario; solo il testo, niente foto.

Poi mi sono ricordato che iOS 14 produce foto in formato Heic. Per qualche motivo, parrebbe, a Mail non vanno bene, almeno nel condividerle dal Rullino.

Nella fretta ho risolto in altro modo senza approfondire. Approfondirò. Intanto lo considero il viatico ideale per l’installazione di Big Sur che partirà pochi minuti dopo la pubblicazione di questo post.
