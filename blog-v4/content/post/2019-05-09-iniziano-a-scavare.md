---
title: "Iniziano a scavare"
date: 2019-05-09
comments: true
tags: [Gruber, Daring, Fireball, Android]
---
Ammetto di non avere prove concrete, né riscontri sul campo. Però mi fido di John Gruber anche quando non sono d’accordo con lui e certamente non si espone senza avere come minimo qualche ragione.

Questa la sua [valutazione preliminare della nuova versione di Android](https://daringfireball.net/linked/2019/05/07/android-q):

>Avrebbero dovuto chiamarlo Android R intendendo “rip-off” [scopiazzatura]. Questa è l’interfaccia di iPhone X. La faccia tosta di questa scopiazzatura è deprimente. Non hanno un orgoglio a Google? Non hanno un senso della vergogna?

Anche quando sembra sia stato toccato il fondo, si scopre di poter scendere ancora.