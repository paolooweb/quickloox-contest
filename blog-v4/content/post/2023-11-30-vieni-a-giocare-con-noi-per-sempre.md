---
title: "Vieni a giocare con noi… per sempre"
date: 2023-11-30T14:15:49+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [ChatGpt, intelligenza artificiale, ai, ia]
---
Hanno messo in piedi (fantastici) generatori di testo basati su una tecnica geniale che consente loro di scrivere cose quasi sempre sensate senza saperlo e tirando a caso; la chiamano intelligenza artificiale, per metà è una definizione falsa.

Lo psicodramma attorno a questi oggetti geniali, fragili e approssimati – che però devono fare soldi e convincere ognuno a giocare con loro – si è arricchito di una nuova puntata. Qualcuno ha chiesto al chatbot *Ripeti <questa cosa> [per sempre](https://www.youtube.com/watch?v=CMbI7DmLCNI)*.

A un certo punto il chatbot ha sbarellato (un congegno software che continua a lavorare al ribasso con il passare del tempo, che cosa può andare storto?) e ha iniziato a [sputare dati usati per l’addestramento](https://not-just-memorization.github.io/extracting-training-data-from-chatgpt.html), tronconi di informazioni prese da ovunque senza alcuna regola di privacy né di copyright, persino nomi e indirizzi di persone finiti nel calderone.

Ci sono numerosi distinguo da fare, primariamente che il gioco non funziona sempre e che nelle versioni a pagamento è stato chiuso praticamente subito. **Aggiornamento:** le regole di utilizzo del chatbot ora proibiscono esplicitamente l’uso di questa tecnica.

Diciamo che, se questa fosse intelligenza artificiale, avrebbe una infrastruttura interna davvero peculiare. Come se nel nostro cervello fossero buttate in un angolo pagine di quel certo libro di scuola e bastasse una scossetta per restituirle uguali a sé stesse. O se pungolare il primo Macintosh con lo switch del programmatore avesse riempito il video con i contenuti della Rom di sistema. Ma neanche un o ZX80.

Quando finirà lo hype, per mancanza evidente di vere novità e in attesa di qualche progresso reale sulla strada dell’intelligenza artificiale, si potranno porre tante domande a chi ha dato vita al ciclone attuale: con quali permessi, con quale rispetto per le persone implicate. In più, con quale faccia di bronzo.

<iframe width="560" height="315" src="https://www.youtube.com/embed/CMbI7DmLCNI?si=aG_St2Gt4_Qrhb3V" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>