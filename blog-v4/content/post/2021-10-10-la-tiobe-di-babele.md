---
title: "La Tiobe di Babele"
date: 2021-10-10T02:30:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [Tiobe, Lisp, Swift, Python, Swift Playgrounds, Java, C, Scheme] 
---
Di solito il fine settimana è avaro di notizie interessanti, ma fa eccezione [Python](https://www.python.org) al primo posto nella [classifica dei linguaggi più usati secondo Tiobe](https://www.tiobe.com/tiobe-index/), che è la fonte tradizionalmente più seguita in tema.

Il primo posto era da decenni, senza esagerare, una faccenda a due tra [C](https://www.programiz.com/c-programming) e [Java](https://www.java.com/en/) e questa variazione è davvero epocale.

Noto distrattamente che [Swift](https://developer.apple.com/swift/) è diciassettesimo e [Lisp](https://lisp-lang.org) trentanovesimo, niente di particolarmente inconsueto. C’è anche [Scheme](https://www.scheme.com/tspl4/) quarantreesimo, che un po’ sarebbe da contare come Lisp, anche se cambierebbe assai poco.

Impressiona che i primi tre linguaggi riscuotano ciascuno poco più del dieci percento dell’utilizzo; poi si va in cifra singola e basta pochissimo per entrare in una serie interminabile di zero virgola.

Segno che ci sono linguaggi più seguiti e di tendenza di altri, oppure più consolidati, ma esiste la possibilità di scegliere un linguaggio di programmazione da fare proprio in mezzo a un ventaglio di opzioni molto ampio, che è molto positivo.

Vuol dire anche che bisogna farsi pochi scrupoli e avere zero paura di sbagliare: scartato un linguaggio, ne esistono decine di altri. Pensare di poterli valutare tutti in anticipo è illusorio ed è meglio andare dove porta il cuore che perdersi in valutazioni del tutto teoriche.

Era molto che non invitavo alla scoperta della programmazione. D’altronde è stato un sabato rilassato e mi sarei concesso volentieri un picnic, oppure un qualche problemino da risolvere appunto con del software.

Il sabato è stato denso, per quanto rilassato, e nulla si è potuto. Però avrei preso volentieri in mano [Swift Playgrounds](https://www.apple.com/swift/playgrounds/) o Lisp per chiudere l’oramai annosa questione dei commenti qui sotto. Il momento si avvicina.

*Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile.*