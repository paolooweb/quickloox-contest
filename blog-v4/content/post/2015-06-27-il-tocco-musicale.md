---
title: "Il tocco musicale"
date: 2015-06-27
comments: true
tags: [Jobs, Iovine, Beats, Beats1, Google, Facebook, Genius, iTunes, Flipboard, Thompson, Stratechery]
---
Un tempo Steve Jobs dichiarò che Apple si trovava all’intersezione tra la tecnologia e le arti liberali e una eco di questa affermazione si può vedere nelle novità che sono Apple Music e Apple News: servizi curati da umani.<!--more-->

Ne parla questo [articolo su Stratechery](https://stratechery.com/2015/curation-and-algorithms/). Jimmy Iovine (fondatore di Beats) descrive così la stazione radio Beats 1:

>Suona musica non basata sulla ricerca, non basata sul genere, non basata sulle battute per minuto, solo bella musica che sa di buono. Una stazione con un solo padrone: la musica in quanto musica.

Se c’è qualcosa che stona nelle ricerche di Google o nel flusso di contenuti che vediamo in Facebook, è che è algoritmico. Per quanto l’algoritmo sia bravo, prima o poi arriva una stonatura.

A difesa di Google e Facebook, sempre nell’articolo di Ben Thompson, il fatto che loro possono solo lavorare su algoritmi. Google effettua le sue ricerche su un flusso di dati infinito, Facebook approssima la personalizzazione infinita: ognuno di noi riceve un flusso di contenuti assolutamente unico e questo vale per oltre un miliardo di flussi. Un traguardo tecnologicamente incredibile.

Sulle notizie e sulla musica, tuttavia, ci sono risvolti emotivi e agganci culturali che un algoritmo proprio non può cogliere. O almeno non ancora, ma non c’è da sperarci troppo. [Flipboard](https://flipboard.com) mostra che si può lavorare  algoritmicamente sulle notizie, Genius vive da anni dentro iTunes.

Se si vuole aspirare tuttavia alla massima qualità, non si prescinde dal tocco umano. Come al solito c’è un’unica azienda – a questi livelli – in grado di pensarci. Culturalmente e umanamente.