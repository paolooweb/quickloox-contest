---
title: "Una partita da divano"
date: 2022-02-14T04:04:22+01:00
Draft: false
toc: false
comments: true
categories:
tags:
---
Il cinquantaseiesimo [Super Bowl](https://www.nfl.com/super-bowl/) poteva essere una partita inevitabile e però prematura, di passaggio tra generazioni, e invece.

Contrariamente rispetto alle ultime edizioni, mancavano i grandi nomi e le grandi squadre: ad affrontarsi a bordo campo, i due allenatori più giovani della NFL, e in campo il _quarterback_ più intercettato e quello più placcato sul posto. I team tradizionalmente da vertice sono usciti di scena nei playoff poco a poco, fino a scomparire nelle finali di *conference*; il _quarterback_ del decennio, [Tom Brady](https://en.wikipedia.org/wiki/Tom_Brady), ha annunciato il ritiro per l’appunto dopo la sconfitta a una sola partita dalla finalissima.

I due _quarterback_ superstiti hanno confermato la loro fama, l’uno più volte intercettato, l’altro ripetutamente placcato sul posto; ma la partita è stata tutt’altro che noiosa o di basso livello. Pathos, alternanza di punteggio, colpi di scena non sono mancati e fino alla fine il risultato è rimasto in discussione, che è quanto ci aspettiamo ogni anno.

Ha portato a casa il trofeo il _quarterback_ più intercettato, che ha giocato molto peggio dell’altro, ma quando il cronometro lo imponeva è riuscito a fare la cosa giusta. Ha anche sfruttato bene la scarsa vena difensiva di Apple. Il *quarterback* più placcato sul posto, nell’ultima azione della partita, è stato placcato sul posto.

Poi c’è la cornice. Quest’anno mi sono concesso, per novantanove centesimi di euro, la [visione della trasmissione originale](https://macintelligence.org/posts/2022-02-08-e-tutto-un-prepararsi/) senza bisogno di trucchi. Fa una bella differenza, perché permette di prendere visione piena della questione culturale.

L’America è un mondo completamente diverso dal nostro, davvero difficile da giudicare senza sfumature. Oltre la partita, lo spettacolo dell’intervallo è uno spaccato identitaria; e soprattutto la pubblicità, va vista per capire come sia una parte integrante della serata, al pari di _linebacker_ e _wide receiver_.

L’immagine complessiva è di una nazione forte, tanto piena di risorse quanto capace di dispiegarle ad ampio raggio per combinare i peggiori disastri o raggiungere le vette più alte, apparentemente con la medesima indifferenza e con identico genio.

Non è questa la pagina giusta per trattati sociologici. Mi limito a considerare che nello spazio di una decina di giorni ho assistito, per ragioni diverse, all’evento televisivo che blocca l’Italia e a quello che ferma gli Stati Uniti. Fare un confronto sui soldi è impossibile, ma sarebbe anche scorretto. Il confronto sulle differenti forme di strapaese sarebbe molto più interessante e lungo da sviluppare. Diciamo che le pance dei due soggetti, italiano e statunitense, si disegnano molto bene.

Il confronto infine tra le creatività, le varietà, le intensità, le narrazioni è impietoso.

Si sta andando troppo in là, alla fine si tratta di una partita, per quanto importante. Ricordo però di essere stato a Los Angeles quando il [SoFi Stadium](https://www.sofistadium.com) non esisteva. E accidenti, che stadio. Mentre sono entrato per la prima volta nella vita allo stadio Meazza molto, molto tempo prima. A parte l’anello supplementare, il Meazza è ancora lì.

Questa è una lezione che dovremmo assimilare. A volte in modo discutibile, a volte lodevolmente, ma negli USA le cose vanno avanti, cambiano, fremono. La tradizione è il piedistallo per arrivare in po’ più in alto. Da noi è il divano dove sedersi.
