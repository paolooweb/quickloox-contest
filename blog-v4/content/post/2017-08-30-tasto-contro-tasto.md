---
title: "Tasto contro tasto"
date: 2017-08-30
comments: true
tags: [iPad, Smart, Magic, Keyboard]
---
Veloce ma diritto al punto l’articolo di Jeff Benjamin su 9to5Mac: [Per un iPad Pro, Magic Keyboard o Smart Keyboard?](https://9to5mac.com/2017/08/29/ipad-pro-smart-keyboard-vs-magic-keyboard-video/).

Lui conclude che [Magic Keyboard](https://www.apple.com/it/shop/product/MLA22T/A/magic-keyboard-italiano) offre un migliore rapporto prestazioni/prezzo, ma quando si tratta di muoversi con un iPad Pro la comodità e la rapidità superano ogni altro parametro e preferisce [Smart Keyboard](https://www.apple.com/it/smart-keyboard/), libera da problemi di apparentamento e ricarica e da usare anche come custodia.

Sprovvisto attualmente di iPad Pro, uso regolarmente una Magic Keyboard con iPad e apprezzo i vantaggi che lo stesso Benjamin riconosce, tra i quali la completa libertà di orientamento rispetto allo schermo. Si tratta sempre però di due oggetti separati; in viaggio, averne uno solo – un iPad Pro con una custodia-tastiera – è indubbiamente più comodo.

Inoltre, e qui cade la mia obiezione principale verso la Smart Keyboard, mi pare di avere capito che sugli iPad Pro 10,5” sia full size, cosa che prima non era. Non fosse e mi sbagliassi, rimarrei deciso su Magic Keyboard: per me avere una disposizione fisica uniforme dei tasti, da tastiera a tastiera, ha la priorità su qualsiasi altro requisito.

Il dibattito è aperto, volino i polpastrelli sulla tastiera prediletta.