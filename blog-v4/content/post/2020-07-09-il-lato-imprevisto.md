---
title: "Il lato imprevisto"
date: 2020-07-09
comments: true
tags: [Larson, Misterakko]
---
Gary Larson e le sue tavole di *The Far Side* sono state da sempre un punto di riferimento. Il suo umorismo stralunato e surreale l’ho sempre trovato irresistibile così come detesto i calendari da appoggiare sulla scrivania. Il calendario di *The Far Side* tuttavia non mancava mai e facevo la posta all’American Bookstore di Milano in attesa del suo arrivo.

Un giorno Larson, come avrebbe potuto fare uno dei suoi personaggi, ha deciso che era stufo di disegnare secondo scadenza, avere un editore in attesa del suo lavoro, sentirlo come un lavoro. Ha smesso di lavorare. Non di disegnare, ovviamente; solo, non è più stato un *cartoonist* di mestiere.

È appena successo qualcosa e ringrazio [Misterakko](https://twitter.com/misterakko) per la segnalazione: Gary Larson [ha ripreso a disegnare tavole di *The Far Side*](https://www.thefarside.com/new-stuff). Non per lavoro, non su commissione, per puro piacere di farlo.

La pagina del suo sito spiega come si sia ritrovato con una penna che non scriveva più e, da lì, abbia iniziato a esplorare il mondo dei *digital tablet*.

>Ne ho preso uno, l’ho acceso e improvvisamente è accaduto qualcosa di totalmente inaspettato: nel giro di pochi istanti mi stavo di nuovo divertendo a disegnare. Ero stordito da tutto quello che l’apparecchio offriva, da tutto il potenziale creativo che conteneva. Semplicemente non avevo idea di quanto queste cose si fossero evolute. Forse appropriatamente, per prima cosa ho disegnato un cavernicolo.

Larson non menziona il nome del suo *digital tablet* e il rapporto base/altezza delle sue immagini mi sembra diverso da quello dello schermo di un iPad. Significa poco, solo che non ho prove conclusive del fatto che abbia preso proprio un iPad.

Non importa. Per prima cosa racconterei questa storia nella scuola. Ai docenti.

Secondo, Larson mi ha suscitato in molti anni tante sapide risate attraverso il suo mondo. Vederlo ritrovare il gusto di disegnare è ritrovare quindici anni di vita. Sentire che è accaduto attraverso il digitale, ora che la narrazione convenzionale lo vuole freddo e disumano invece di strumento amplificatore delle nostre capacità, riporta fiducia.

Irrazionalmente, è come se una briciola del mio mondo avesse portato di nuovo una voglia creativa nel suo. Un microscopico restituire.