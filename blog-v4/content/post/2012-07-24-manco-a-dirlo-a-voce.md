---
title: "Manco a dirlo. A voce"
date: 2012-07-24
comments: true
tags: [Microsoft]
---
Le conversazioni audio su Skype non erano ascoltabili da un, chiamiamolo, osservatore indipendente.

Poi Skype è stato acquistato da Microsoft e pare che certi difetti del software siano stati finalmente migliorati.

Un mese dopo l’acquisto, a Microsoft è stato riconosciuto un <a href="http://appft1.uspto.gov/netacgi/nph-Parser?Sect1=PTO2&Sect2=HITOFF&u=%2Fnetahtml%2FPTO%2Fsearch-adv.html&;r=1&amp;f=G&l=50&d=PG01&p=1&S1=20110153809&OS=20110153809&RS=20110153809">brevetto</a> riguardante l’intercettazione silente di comunicazioni di telefonia digitale.

Vari hacker <a href="http://skype-open-source.blogspot.ch">hanno affermato</a> a maggio che Microsoft ha modificato l’architettura di Skype per facilitare l’intercettazione. Non sono deliri di pazzi chiusi in una cantina, tanto che – seppure in forma interrogativa – sono arrivati su <a href="http://www.forbes.com/sites/anthonykosner/2012/07/18/did-microsoft-change-the-architecture-of-skype-to-make-it-easier-to-snoop/">Forbes</a>.

Microsoft <a href="http://www.slate.com/blogs/future_tense/2012/07/20/skype_won_t_comment_on_whether_it_can_now_eavesdrop_on_conversations_.html">non ha confermato né smentito</a>.

Suggerire di evitare Skype è una battaglia persa. Sono tempi in cui la comodità prevale sul bene comune. Comunque, a parte soluzioni come Messaggi di Apple, ci sono strumenti <i>open source</i> come <a href="https://jitsi.org">Jitsi</a>, che consentono la telefonia via Internet e le video e audiochat, compresa la cifratura se necessario. Scaricare Jitsi è gratis e funziona su qualunque desktop, anche se va complementato su computer da tasca da qualche altra app VoiP.