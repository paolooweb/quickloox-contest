---
title: "Com'è fatto il mondo"
date: 2014-05-12
comments: true
tags: [Apple, Amazon, Google]
---
Abbiamo tutti un problema: la tendenza a disquisire assegnando un peso eccessivo alla nostra esperienza personale. Dovremmo essere capaci di prescinderne e acquisire uno sguardo più generale, più globale, più universale. Il *particulare* conta ovviamente, compone la vita. Tuttavia spostare l’orizzonte più in avanti per qualche pensiero allunga la vita, oltre che la vista.<!--more-->

È a questo che pensavo leggendo l’[articolo di <del>Federico Viticci</del> Graham Spencer](http://www.macstories.net/stories/mapping-the-international-availability-of-entertainment-services/) sulla disponibilità internazionale dei servizi di intrattenimento: dove arriva Apple con i film, Amazon con i libri, Google con la musica.

L’articolo è in inglese e tuttavia i grafici interattivi e animati sono evidenti a chiunque e nessuno sentirà come una perdita la mancanza dell’italiano. Bello cominciare la settimana con un respiro a pieni polmoni, guardare com’è fatto il mondo prima di cercare parcheggio alla stazione.