---
title: "Motion perpetuo"
date: 2018-11-30
comments: true
tags: [Codemotion, Milano, Mac]
---
Ho visitato [Codemotion Milano 2018](https://milan2018.codemotionworld.com) e sono rimasto anche quest’anno favorevolmente impressionato dall’atmosfera del luogo. Si parla di programmazione e design applicativo, anche in modo pesante, ma si vedono persone motivate, allegre, entusiaste e lontane dallo stereotipo del *nerd* asociale e avulso dalla realtà.

C’erano tanti Mac e ne ho fotografati alcuni in contesti particolari. L’obiettivo era fotografare Mac e farlo in fretta senza disturbare; la resa delle immagini è pessima e me ne scuso, anche se l’illuminazione certo non aiutava.

Il consueto Mac al banco regia. Aspetta, più di uno. Aspetta, non portatile.

 ![Mac al banco regia](/images/regia.jpeg  "Mac al banco regia") 

Al banco Ibm Cloud, con il robot di ordinanza per dare l’idea di futuro. Il robot stava meglio in un evento per bancari: in compenso, se Ibm si muove, si muove con un Mac. Come altre aziende importanti, Ibm fa presenza fissa a Codemotion alla ricerca di talenti. Codemotion ha tradizionalmente una forte componente di *recruitment* e le aziende che partecipano non sono quelle dello Smau di tuo papà, che ti tiravano dietro l’antistress in cambio di una email. Qui ti tirano dietro l’antistress ma sono più interessate ai curriculum.

 ![Banco Ibm Cloud](/images/ibm-cloud.jpeg  "Banco Ibm Cloud") 

Sempre Ibm ha allestito una Escape Room dove si poteva realmente evadere, grazie a caschi di realtà virtuale. C’era Mac.

 ![Escape Room con realtà virtuale](/images/virtuale.jpeg  "Escape Room con realtà virtuale") 

Chiudo con il banchetto di Salesforce, azienda conservatrice come poche ma anch’essa determinata a trovare bravi tecnici giovani dove ci sono. Questo contrasto tra l’ambiente giovanile di Codemotion e le aziende paludate disposte a togliersi la palandrana pur di incontrare i ragazzi è una costante interessante di Codemotion.

 ![Salesforce](/images/salesforce.jpg  "Salesforce") 

Niente polemiche. Presa d’atto che Codemotion dura nel tempo ed è una manifestazione imperdibile per chi sia in *target*. Altra presa d’atto, la presenza di Mac in un campione di ragazzi poco interessati alla forma e al *fashion* è ingente e inequivocabile.