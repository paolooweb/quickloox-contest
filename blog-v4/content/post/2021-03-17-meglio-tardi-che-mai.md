---
title: "Meglio tardi che mai"
date: 2021-03-17T01:03:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [DuckDuckGo, Google, privacy] 
---
In grande ritardo su come si comporterebbe una azienda rispettosa dei propri interlocutori, Google ha iniziato a svelare [che tipo di dati raccoglie attraverso le sue app](https://twitter.com/DuckDuckGo/status/1371509053613084679), a questo giro Chrome e la app Google per la ricerca.

<blockquote class="twitter-tweet"><p lang="en" dir="ltr">After months of stalling, Google finally revealed how much personal data they collect in Chrome and the Google app. No wonder they wanted to hide it.<br>⁰<br>Spying on users has nothing to do with building a great web browser or search engine. We would know (our app is both in one). <a href="https://t.co/lJBbLTjMuu">pic.twitter.com/lJBbLTjMuu</a></p>&mdash; DuckDuckGo (@DuckDuckGo) <a href="https://twitter.com/DuckDuckGo/status/1371509053613084679?ref_src=twsrc%5Etfw">March 15, 2021</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

DuckDuckGo è un ottimo motore di ricerca nel 99 percento delle situazioni e risulta inferiore a Google solo se si compiono ricerche veramente molto sofisticate. Altrimenti, la differenza è quasi nulla e cambiare è semplice: un salto nelle Impostazioni alla voce Safari.

Non è l’unico ritardo di Google. La società è arrivata solo oggi ad [abbassare al 15 percento il prelievo sulla vendita delle app che incassano meno di un milione di dollari](https://macdailynews.com/2021/03/16/google-follows-apple-cuts-store-fee-from-30-to-15-on-first-1-million-developers-earn/). Apple, accusata di ogni nefandezza in termini di maltrattamenti agli sviluppatori su App Store, lo ha fatto da mesi.

Sempre parlando di ritardi, non mi risulta che le mappe americane di Google contengano l’indicazione dei punti dove è possibile vaccinarsi, come invece [hanno preso a fare quelle di Apple](https://www.apple.com/newsroom/2021/03/apple-maps-now-displays-covid-19-vaccination-locations/) con tanto di integrazione con Siri, alla quale si può chiedere dove sia possibile effettuare l’operazione.

Google non si trova nel suo momento più felice. Ma c’è di peggio che arrivare in ritardo a dichiarare che cosa combinano le proprie app dietro le quinte, come dimostra la società cinese (appoggiata dalla dittatura) al lavoro su [un sistema di tracciamento alternativo](https://apple.slashdot.org/story/21/03/16/1252215/chinas-tech-giants-test-way-around-apples-new-privacy-rules) a quello che Apple vieta in prima battuta.

Qualcuno si rifiuterà sempre di fare la cosa giusta. Allora, è meglio perfino un ritardatario nel farla.

*Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile.*