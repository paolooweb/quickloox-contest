---
title: "Commentario di metà incontro"
date: 2021-02-08T02:33:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [Super Bowl, Tom Brady, Patrick Mahomes, Tampa Bay Buccaneers, Kansas City Chiefs, Cbs, Ivacy, Mike D’Antoni, Dan Peterson, Olimpia, Rai] 
---
Tema banale quest’anno, trovare modo di seguire il Super Bowl in diretta, grazie alla copertura Rai. Non amo i commentatori italiani di stato sul football americano (sarebbero stati meglio quelli della televisione commerciale) e così, complice la VPN di Ivacy, ho ascoltato gli americani via Cbs.

Trattavasi brutalmente del confronto tra il veterano quarterback-leggenda e  la giovane stella emergente. Anche quando ero ragazzino, forse suggestionato dalle imprese dell’Olimpia di Mike D’Antoni e Dan Peterson, in questa situazione ho sempre tifato per la prosecuzione della leggenda, che sfida la legge del tempo che passa. Leggenda che oltretutto, quest’anno, aveva i pronostici a sfavore.

Tentando benevolmente di non dare spoiler a chi vuole godersi la differita, dirò solo che a metà partita c’era un dominio netto di una squadra, psicologicamente e sul campo, con l’impressione che l’altra compagine dovesse ancora entrare in partita.

Teoricamente, bissare il trionfo dello scorso anno per Patrick Mahomes o riscrivere per l’ennesima volta il racconto delle imprese di Tom Brady sono entrambi eventi ancora possibili. Il Super Bowl ci ha abituato a grandi imprese e grandi sorprese; il punteggio dei primi due quarti potrebbe benissimo essere stato ribaltato. Di sicuro il team inseguitore non si è fatto distrarre dallo spettacolo dell’intervallo, che ho trovato scialbo e sottotono rispetto alle edizioni passate.

Ti basterebbe comunque questo post per sapere chi ha vinto, se lo volessi.