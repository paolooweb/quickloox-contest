---
title: "Pubblicità cattiva"
date: 2020-09-11
comments: true
tags: [Gruber, adv]
---
Citazione da un [pezzo](https://daringfireball.net/2020/09/online_privacy_real_world_privacy) di John Gruber:

>L’industria del tracciamento ha ragione nel ritenere che gli utenti di iOS 14 negheranno a stragrande maggioranza il permesso di farsi tracciare. Non dipende dal fatto che il messaggio informativo di Apple li spaventi senza motivo; dipende dal fatto che il messaggio informativo di Apple spiega con precisione e in linguaggio chiaro che cosa sta succedendo, ed è una cosa repellente. La richiesta del permesso di farsi tracciare è qualcosa che nessuna persona sana di mente accoglierebbe, perché nessuno sano di mente accetterebbe questo tipo di tracciamento.

Il pezzo riferisce che, per alcuni nel mondo della pubblicità online, l’iniziativa di Apple in iOS 14 *è eccessiva e provoca danni sproporzionati a un ecosistema vivo e vitale, perché butta via il bambino assieme all’acqua sporca.* Non è una cosa da poco, tanto che a seguito delle pressioni Apple ha deciso di posporre l’attivazione della funzione all’anno prossimo. Tuttavia arriverà e per l’industria del tracciamento sarà un colpo duro.

Ancora peggio per i tracciatori andrebbe se, metti mai che niente è impossibile, saltasse fuori che Google copia la funzione ad Apple e la inserisce dentro Android, un sistema operativo che attualmente è un colabrodo *by design*, strutturato esattamente per sfruttare tutte le occasioni possibili di tracciamento di qualunque dato.

Riassumendo: c’è un settore di business importante, potente, agguerrito, popolato di menti talentuose per quanto la causa sia sbagliata, che fa soldi a palate tracciando la navigazione delle persone oltre il consentito e oltre il ragionevole. Apple mette i bastoni tra le ruote di questo meccanismo con una mossa radicale che non le dà apparentemente alcun vantaggio (in realtà scommette sulla soddisfazione degli utenti per non essere tracciati, ma è un altro discorso).

Steve non c’è più, non si innova più, non escono più prodotti nuovi, si è persa la strada, una volta era tutta campagna. Ma… questa mossa non sa tanto, tantissimo, di *Think Different*?