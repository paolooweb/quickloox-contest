---
title: "Sembra facile"
date: 2021-01-29T02:09:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [Apple, Google, iOS, Facebook] 
---
Quando uscirà la versione di iOS che obbliga le app a chiedere il consenso per il tracciamento a fini pubblicitari da parte di chi le usa, Google non mostrerà la richiesta di consenso.

Perché [le sue app smetteranno di raccogliere quei dati e la richiesta non sarà necessaria](https://www.blog.google/products/ads-commerce/preparing-developers-and-advertisers-for-policy-updates/).

Google sta avvisando partner e sviluppatori che la mossa di Apple potrebbe causare diminuzioni nel fatturato delle app.

Apple sta avvisando le persone che il tracciamento della navigazione a scopo pubblicitario diventerà una questione trasparente e che le app potranno tracciare solo chi dà esplicitamente il proprio consenso.

Tim Cook [ha definito la privacy una delle questioni fondamentali di questo secolo](https://www.fastcompany.com/90599049/tim-cook-interview-privacy-legislation-extremism-big-tech).

Google non è contenta, chiaramente. Ma, rispetto alla [reazione scomposta](https://www.facebook.com/audiencenetwork/news-and-insights/preparing-audience-network-for-ios14/) di Facebook, che è arrivata a assegnarsi il ruolo di [portavoce dell’interesse delle piccole imprese](https://www.facebook.com/business/news/ios-14-apple-privacy-update-impacts-small-business-ads), giganteggia.

Sembra così facile. Chi naviga ha diritto a sapere che uso viene fatto dei dati che lasciasse lungo la via. Eppure si pone come una svolta di cui si parlerà molto a lungo.

*Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile.*