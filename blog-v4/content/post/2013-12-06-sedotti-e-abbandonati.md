---
title: "Sedotti e abbandonati"
date: 2013-12-06
comments: true
tags: [Android, iPhone]
---
Il mito: Apple taglia fuori gli iPhone vecchi dagli aggiornamenti e non rende giustizia nel tempo all’investimento fatto dagli acquirenti di un nuovo iPhone.<!--more-->

La realtà: il [grafico presso The Understatement](http://theunderstatement.com/post/11982112928/android-orphans-visualizing-a-sad-history-of-support) mostra senza bisogno di lingua inglese come vadano le cose rispetto ad Android e quanto un iPhone non più nuovo possa contare su aggiornamenti e supporto.

Su Fidlee c’è un altro [grafico ancora più chiaro](http://www.fidlee.com/android-support-vs-ios-support/) più qualche considerazione sui dati:

>Dieci Android su sedici sono stati dismessi entro un anno dall’uscita.
>Sei su sedici non hanno mai eseguito la versione più aggiornata di Android.
>Galaxy Nexus è il primatista della più lunga esecuzione del software Android aggiornato: un anno e dieci mesi. iPhone 3GS: quattro anni e tre mesi.

Sedotti dal prezzo, abbandonati mai troppo presto. Chissà quando la capiscono. 