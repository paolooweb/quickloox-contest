---
title: "Nostalgia e canaglia"
date: 2015-07-12
comments: true
tags: [Apple, Idc, Microsoft, Nokia]
---
C’è ancora inspiegabilmente un gruppo di nostalgici cui può interessare Office 2016 per Mac, un nome – Office, 2016 – che è un ossimoro dal momento che si potrebbe parafrasare come *A volte ritornano*.<!--more-->

Nel [dare diligentemente conto](http://arstechnica.com/apple/2015/07/office-2016-for-mac-is-out-now-for-office-365-usersor-september-for-everyone-else/) della disgrazia, *Ars Technica* ha lasciato aperti i commenti e ce n’è uno solo, di tale *Rosyna*, di cui riporto un breve estratto:

>È ancora a 32 bit (per via di Visual Basic). Non supporta funzioni di OS X come App Termination o App Nap.

Ci sarà gente che lo *acquista*. Office nel 2015, a 32 bit. È un’operazione per cui posso avere solo una parola, che mi sono già giocato nel titolo.

Preferisco festeggiare il ventennale della mia personale festa di liberazione da Word e ricordare che la soluzione è aperta, libera, lavorata da entusiasti. Si chiama [LibreOffice](http://www.libreoffice.org).

(*Disclaimer*: faccio sempre orgogliosamente parte del comitato tecnico-scientifico di [LibreItalia](http://www.libreitalia.it).)