---
title: "Il fine giustifica i mezzi"
date: 2018-12-02
comments: true
tags: [Samsung, iPhone]
---
Trecentotrenta degli ultimi tremiladuecentoventuno tweet di Samsung Mobile sono stati eseguiti da un iPhone.

<blockquote class="twitter-tweet" data-lang="en"><p lang="en" dir="ltr">Might as well add &quot;Twitter police&quot; to my bio at this point 🤦‍♂️ <a href="https://t.co/DRlrXl7bak">pic.twitter.com/DRlrXl7bak</a></p>&mdash; Marques Brownlee (@MKBHD) <a href="https://twitter.com/MKBHD/status/1069246242130145280?ref_src=twsrc%5Etfw">December 2, 2018</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

Forse avevano bisogno di farsi pubblicità, oppure di un apparecchio più efficiente, chissà.

Su tutto, il [commento di John Gruber](https://daringfireball.net/linked/2018/12/03/samsung-iphone-tweet):

>Quando accadono questi inconvenienti vedo talvolta gente chiedersi perché questi tweet siano inviati da un telefono e non da un computer desktop. Questi tweet sono lavoro. Credo che queste persone non si rendano conto di quanto lavoro – serio lavoro professionale – venga svolto su telefoni.

Qualcuno pensa ancora che siano telefoni. Sono computer.