---
title: "Quante beta"
date: 2018-08-25
comments: true
tags: [iOS, macOS, beta]
---
A memoria non ricordo di avere visto [dieci beta di una nuova versione di iOS](https://www.macrumors.com/2018/08/23/apple-seeds-ios-12-beta-10/) come è accaduto per la versione 12 e potremmo non avere ancora finito, visto che c’è ancora tempo.

Tempo che giorno più giorno meno è invariante: il ciclo annuale di rinnovo significa che in sostanza ogni iOS sarà stato preparato per lo stesso tempo (ignoro volutamente questioni come le risorse dedicate, nell’assenza di dati certi, e presuppongo arbitrariamente che anche qui, nel lungo periodo, vi sia una sommaria equivalenza).

Più beta nello stesso tempo vuol dire maggiore frequenza. Sarà un bene o un male? Più cura nello sviluppo o maggiore necessità di *feedback* esterno?

Personalmente preferisco vedere più beta. Una pubblicazione è comunque un segnale volontario e implica confidenza nell’impatto del software, per quanto software non ancora rifinito. Un’assenza di segnali verso l’esterno potrebbe anche indicare problemi non risolti in modo soddisfacente.

Sarà ovviamente la stabilità del definitivo a dirci dove stia la verità.
