---
title: "Grand Hotel Excel"
date: 2020-10-08
comments: true
tags: [Excel, Gruber, Covid-19, Csv, Microsoft]
---
Non è tanto che il governo britannico si sia dimenticato per strada sedicimila vittime del virus a causa di [un file Csv portato in Excel per creare un file in formato .xsl](https://www.bbc.com/news/technology-54423988), troppo antiquato per funzionare.

È un po’ di più la fissazione di usare Excel come se fosse [il programma tuttofare](https://macintelligence.org/blog/2020/09/01/un-eccezione-utopica/). Buono per gli scadenziari così come per elaborare *big data*, il perfetto alibi per l’ignorante che rifiuta di padroneggiare uno strumento adatto al suo lavoro, tanto c’è Excel.

La tragedia l’ha evidenziata John Gruber: la scelta di un formato inadeguato provoca la scomparsa di righe sufficienti a contenere i dati di sedicimila vittime e nessuno se ne accorge subito perché in questo caso Excel [non mostra alcun messaggio di errore](https://daringfireball.net/linked/2020/10/07/excel-row-limit).

Avviso per gli aspiranti geni del *machine learning* che affrontano la frontiera del futuro armati di Excel, lo stesso Excel usato per il Fantacalcio: è un programma che può mangiarsi dati senza neanche avvisare.

E un’altra vicenda nazionalgrottesca va a popolare l’[Hotel California](https://www.youtube.com/watch?v=EqPtz5qN7HM) di Microsoft, entrare è questione di un momento, poi non se ne esce più.

<iframe width="560" height="315" src="https://www.youtube.com/embed/EqPtz5qN7HM" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>