---
title: "Arte amanuense"
date: 2015-11-22
comments: true
tags: [Apple, Pencil, Jobs, Macintosh, tipografia, calligrafia]
---
Per reinventare il *computing*, Steve Jobs è partito dalle arti, non dall’ingegneria, compresi i corsi di calligrafia che frequentava al *college* anche dopo avere lasciato gli studi. Corsi cui si deve l’ispirazione di dotare Macintosh di tipografia evoluta dal primo giorno. Ed è grazie a Steve Jobs se oggi possiamo riassaporare la calligrafia.<!--more-->

Ho parafrasato la conclusione della [recensione di Apple Pencil scritta da Horace Dediu di Asymco](http://www.asymco.com/2015/11/18/my-review-of-the-apple-pencil/). Da leggere, nel senso più pieno della parola.

Credo che nessun’altra matita elettronica abbia mai portato alcun altro a scrivere a una recensione di questo livello. Men che meno, a permettergli di scriverla.