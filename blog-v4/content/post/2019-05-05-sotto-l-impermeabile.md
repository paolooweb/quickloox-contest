---
title: "Sotto l’impermeabile"
date: 2019-05-05
comments: true
tags: [iPhone, OnePlus, IP, Cnet]
---
Gli iPhone recenti sono resistenti alla polvere e all’acqua, classificati secondo uno standard denominato IP che prevede una serie importante di prove e specifiche. La materia sembra semplice e invece è complessa, come testimonia questo [articolo (pure divulgativo) di C|Net](https://www.cnet.com/how-to/how-waterproof-is-your-android-phone-or-iphone-heres-what-ip68-and-ip67-ratings-mean/).

Poi c’è OnePlus, che produce il telefono omonimo. Lì hanno avuto un’idea brillante: invece che impegnarsi a certificare i loro modelli secondo uno standard internazionale riconosciuto, [hanno girato un video dove buttano OnePlus 7 Pro in un secchio d’acqua](https://forums.oneplus.com/threads/a-note-on-smartphone-ip-ratings.1027087/).

Guarda caso, OnePlus è uno di quei tanti esempi di *come iPhone, ma costa meno*. In questo caso si capisce benissimo dove stia il risparmio e di quale tipo di equivalenza in prestazioni si parli.

Fidarsi è bene; io mi fiderei più di una certificazione ufficiale, certo ottenuta dovendo impiegare del tempo e del denaro. Non vorrei dover scoprire a mie spese che cosa OnePlus nasconde sotto quell’impermeabile lì.