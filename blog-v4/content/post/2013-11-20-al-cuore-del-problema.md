---
title: "Al cuore del problema"
date: 2013-11-20
comments: true
tags: [ePub, BBEdit, Hofstadter, Calvino]
---
Dicevo, gli autori del XXI secolo [devono avvicinarsi al lato tecnico della produzione](https://macintelligence.org/posts/2013-11-12-travestimenti-da-autore/) dei libri, soprattutto libri elettronici.<!--more-->

Mi scrive giustappunto [Edo](http://www.evk.name/), che è al lavoro su un’opera di una certa complessità e mole, contenente anche degli *easter egg*, sorprese non evidenti al primo approccio. E si è trovato di fronte a un problema:

>I link funzionano ma sono tutti “aperti”: ad esempio, se seleziono il simbolo che mi porta al titolo della prima scena del capitolo 0, vedo la relativa pagina ma anche tutte le pagine seguenti (ovvero i titoli delle altre scene) come fosse un enorme capitolo extra!

Non si approfondisce per non guastare la sorpresa. Il punto è che Edo, per risolvere, apre [BBEdit](http://barebones.com/products/bbedit/) e il file epb.obf, ingrediente essenziale di un libro elettronico in formato ePub. Trova una serie di marcatori del tipo

`<itemref idref="chapter-1” linear=“yes”>`

e la risposta al suo problema:

>La soluzione è di una semplicità estrema, sia lode ai creatori del protocollo ePub: un bel `linear=“no”` e quel marcatore ti porta in un cul-de-sac, una pagina isolata e “chiusa" dalla quale non puoi fare altro che tornare indietro. Così i miei Easter Egg sono belli funzionanti e soprattutto, nascosti come volevo.

Un libro elettronico può fare cose diverse da quelle che può fare un libro di carta. Pensare di scrivere un libro elettronico partendo da un *word processor* e al *word processor* arrivando funziona se sei Faletti, Eco, Busi, Dan Brown. Se vuoi offrire qualcosa di più, di diverso, di speciale, devi conoscere un po’ di ePub, una spruzzata di Html, guardare in faccia senza arrossire un *tag*.

Che poi, Douglas Hofstadter ha infarcito di *easter egg* (a partire dal titolo!) [Gödel, Escher, Bach: un’Eterna Ghirlanda Brillante](http://www.adelphi.it/libro/9788845907555), un libro convenzionale. Ma guarda caso, ha deciso lui i font, disegnato molte delle illustrazioni e supervisionato tutte le fasi di produzione.

Italo Calvino ha scritto un libro che è una *matrioska* di *easter egg*, [Se una notte d’inverno un viaggiatore](http://www.ibs.it/code/9788804482000/). Lui però era Italo Calvino, mica l’ultimo arrivato. E il libro è del 1979.

Solo due esempi. Se vogliamo esprimerci, ci serve più tecnica che quella bastante alla generazione precedente. È un’epoca nuova, nel bene e nel male.