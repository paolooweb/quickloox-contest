---
title: "Ne resterà almeno uno?"
date: 2020-03-18
comments: true
tags: [npm, TypeScript, JavaScript, Microsoft, Linux, Wsl, kernel, Hyper-V, VMware]
---
[npm](https://www.npmjs.com/?_ga=2.198185391.1490285651.1584405775-824474009.1584405775), package manager strategico per chi sviluppa in JavaScript, è stato [acquisito da GitHub](https://github.blog/2020-03-16-npm-is-joining-github/).

Cioè è stato acquisito da Microsoft, che due anni fa ha acquisito GitHub.

Microsoft è la stessa che produce [TypeScript](https://www.typescriptlang.org), un superinsieme di JavaScript che lo estende. (JavaScript sarebbe uno standard, nel frattempo).

Estendere tecnologie molto usate era la strategia della vecchia Microsoft per farle fuori. *Embrace, Extend, Extinguish*. Nel caso in esame, tutto il codice JavaScript funziona tranquillamente in TypeScript, mentre il contrario non è vero; una volta che un numero sufficiente di sviluppatori avrà adottato TypeScript, JavaScript sarà svuotato di senso e Microsoft avrà il pieno controllo della tecnologia. Certo, parliamo di *open source*, chiunque può creare un *fork*, una branca differente dello stesso codice, e ripartire liberamente. Ma l’indomani Microsoft mette, che so, cento sviluppatori a lavorare con l’obiettivo di rendere inutile o superato il *fork*. E puoi avere tutto l’open source che credi, diventi irrisorio lo stesso.

In questo contesto, Microsoft ora ha il controllo del *package manager* più usato dagli sviluppatori.

Nel frattempo, sempre Microsoft annuncia la seconda versione del suo [Windows Subsystem for Linux](https://devblogs.microsoft.com/commandline/wsl2-will-be-generally-available-in-windows-10-version-2004/) (in sintesi: per non correre il rischio che tu voglia installare Linux al posto di Windows per il tuo lavoro di sviluppatore, ti facciamo trovare un Linux già pronto così puoi continuare a usare Windows).

Tra le novità: il *kernel* Linux non farà parte dell’immagine di installazione, ma verrà distribuito tramite Windows Update. Ovvero, uno potrà scegliere la versione di Linux che desidera, ma il kernel (il nucleo del sistema) sarà sotto stretto controllo di Microsoft.

Ancora, la nuova versione del sottosistema Linux può essere fatta funzionare dentro una macchina virtuale, [grazie all’utilizzo dell’architettura Hyper-V di Microsoft](https://docs.microsoft.com/en-us/windows/wsl/wsl2-faq).

Casualmente questo causa problemi nell’utilizzo di macchine virtuali concorrenti, per esempio [VMware](https://www.vmware.com), i cui ingegneri dovranno spendere tempo e risorse per risolvere il problema invece di fare progredire VMware. Una volta risolto il problema, rimarranno impiccati al capriccio di Microsoft, che potrà introdurre quando vuole le incompatibilità che vuole fino a quando a nessuno verrà in mente la malsana idea di usare VMware per virtualizzare il sottosistema Linux di Windows.

Insomma, la nuova Microsoft non è come quella vecchia, che voleva distruggere l’open source. Al contrario, [lo ama](https://www.techrepublic.com/article/whats-really-behind-microsofts-love-of-open-source/). Al punto da volerlo tutto per sé, in modo possessivo.

Resterà almeno un pacchetto software open source di qualche rilevanza, che potrà chiamarsi ancora *free software*, nel senso di *freedom* e non in quello di *free beer*?