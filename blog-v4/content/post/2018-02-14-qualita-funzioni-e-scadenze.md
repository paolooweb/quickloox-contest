---
title: "Qualità e scadenze"
date: 2018-02-14
comments: true
tags: [Apple, macOS, iOS, Sinofsky, Gurman, Bloomberg]
---
Mark Gurman ha pubblicato un [pezzo su Bloomberg](https://www.bloomberg.com/news/articles/2018-02-12/how-apple-plans-to-root-out-bugs-revamp-iphone-software) in cui dettaglierebbe una nuova strategia di sviluppo software di Apple, nata per contrastare un supposto declino della qualità del software, che prevede lo *scuotere il proprio programma di sviluppo* con l’obiettivo di fare più attenzione alla soluzione dei bug e se necessario posporre le funzioni nuove che non si fa in tempo a inserire.<!--more-->

Steven Sinofsky, uno che di sviluppo software all’interno di una multinazionale [se ne intende](https://macintelligence.org/posts/2017-06-12-recensioni-divertite/), gli ha risposto indirettamente con una lunga serie di *tweet* poi [condensata su Medium](https://medium.learningbyshipping.com/apples-software-problem-and-fixing-it-via-twitter-c941a905ba20). La risposta effettiva è questa:

>Mi sento di poter dire che le mosse di Apple, anche se la vedo dall’esterno, non denunciano una crisi né rappresentano una reazione a fattori esterni. […] Si tratta di una evoluzione metodica e predicibile di un sistema estremamente collaudato e solido.

Questo per lo *scuotere* il programma di sviluppo.

Un *tweet* dietro l’altro, tuttavia, Sinofsky va molto oltre, fino ad accennare alle problematiche vere che stanno dietro allo sviluppo di software su vasta scala e che palesemente sfuggono all’uomo della strada che trova il baco in Anteprima e allora si stava meglio quando si stava peggio.

>Apple non farà che rinnovare le procedure di ingegnerizzazione. Significa riflettere su come viene analizzato il rischio, come vengono costruiti i calendari di lavoro, come si impostano le priorità. È il significato letterale di portare avanti un progetto ed è ciò che li paghiamo per fare.

Soprattutto questo:

>Quello che è andato perso nella discussione è la sfumatura tra funzioni, scadenze e qualità. È come parlare con un promotore finanziario di reddito, rischio e crescita. Non è che uno si mette in testa di volere tutte e tre insieme e si senta rispondere “certamente”.

Numerosi paragrafi sono fortemente elogiativi dello sviluppo software compiuto da Apple in questi decenni. Siccome sono di parte e non obiettivo, li lascio alla scoperta del lettore. Sono l’opinione di un personaggio che è stato ai vertici di Microsoft, non la mia.