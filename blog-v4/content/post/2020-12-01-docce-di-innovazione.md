---
title: "Docce di innovazione"
date: 2020-12-01
comments: true
tags: [Watch, Brittney]
---
Comincia così il pezzo.

>Lo confesso: faccio la doccia con Apple Watch al polso.

Anche [Tim Cook](https://www.businessinsider.com/tim-cook-apparently-showers-with-his-apple-watch-2015-2?IR=T), del resto.

>Dopo essermi asciugata e avere riordinato l’ego, ho deciso che avrei lasciato il mio Apple Watch Series 3 per Apple Watch SE. I modelli più recenti di  Apple Watch hanno il riconoscimento delle cadute.

Problemi da primo mondo? No, una persona disabile che [grazie a Apple Watch ritrova autosufficienza e confidenza](https://medium.com/@iwriteforsoreeyes/apple-watch-is-the-new-life-alert-3ffdf9bc6915).

Una caduta in doccia può fare male. Ma chi non ha piena capacità motoria può trovarsi intrappolata. E, se ha perso conoscenza, anche in difficoltà veramente gravi.

A meno di avere al polso un computer con cui poter chiedere aiuto. O che avvisa automaticamente un servizio di emergenza, se non ha risposta dalla sua padrona.

C’è chi sbeffeggia Apple Watch perché non è innovazione, farebbe tendenza fighetta, non fattura come iPhone. C’è chi lo usa e si fa cambiare la vita in meglio, ma non perché le notifiche sono più comode; perché riduce il rischio di morire.

A volte l’innovazione è aiutare concretamente una persona.