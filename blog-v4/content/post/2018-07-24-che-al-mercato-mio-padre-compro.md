---
title: "Che al mercato mio padre comprò"
date: 2018-07-24
comments: true
tags: [Huawei]
---
Colleghi la chiavetta e lanci il software di connessione. *Questo apparecchio non esiste*, è la risposta. Certo, che sciocco: l’anno scorso ti sei collegato con un Mac diverso.

Bisogna installare un driver aggiornato. Dove stava? Ne avevo parlato in un post. Una ricerca su BBEdit ed [ecco il link](https://macintelligence.org/posts/2017-07-12-chiavette-e-modernariato/). A che serve un blog, se non come archivio delle cose utili?

Allestisci una connessione di emergenza e carichi la pagina. Che non esiste più.

Provi a cercare il driver su Google e ne viene fuori uno da [chissã dove](https://withsteps.com/4231/huawei-e3131-digi-net-mobil-stick-on-macos-sierra-10-12-5.html). Ma sembrerebbe a posto. Lo scarichi.

Mac segnala che ci sono problemi con l’immagine disco. *Vuoi aprirla comunque?* Mah, proviamo. L’immagine si apre regolarmente, pare a posto.

Fai partire il software. *Non arriva da uno sviluppatore autorizzato*. Vero, la aprì con il menu contestuale per bypassare Gatekeeper.

Però adesso sei su High Sierra. *Per installare questo driver devi autorizzarlo dal pannello Sicurezza & Privacy delle Preferenze di Sistema*. D’accordo, lo apri. C’è il pulsante apposta.

*Impossibile aprire il pannello Sicurezza & Privacy*. Provi ad aprirlo manualmente. Non funziona. Sullo sfondo, l’installazione del driver attende una autorizzazione che non può arrivare.

Ordini l’uscita forzata delle Preferenze di Sistema. Sullo sfondo l’installazione è sempre ferma. Riaprì le Preferenze. Non c’è il pulsante di autorizzazione.

Esci dall’installazione. Meglio, vorresti uscire. *L’installatore è impegnato*. Provi ad aspettarlo. Minuti. Nessun segno di vita.

Ordini l’uscita forzata. *Rischi di danneggiare il Mac se interrompi questa installazione* (essendo un driver, cioè una extension). *Ma fammi il piacere*. Confermi l’uscita forzata.

Rilanci l’installazione. *Aspetto che finisca l’installazione precedente*. Quella terminata a forza. Riavvii.

Provi a riscaricare, sai mai che l’immagine avesse davvero un problema. No, arriva uguale a prima e dà identico messaggio.

Rilanci l’installazione. Va a buon fine, o così sembra.

Rilanci il software di connessione della chiavetta. Si chiude senza spiegazioni.

Provi a fare partire la chiamata direttamente dal pannello di controllo Network. Manca la gestione della chiavetta, ma la connessione funziona. Era quello che volevi, due ore fa.

Non sai se sentirti un mago, un miracolato, un deficiente o il topolino de [Alla fiera dell’Est](https://www.youtube.com/watch?v=IVwCOO0PYZA).
