---
title: "Obsolescenza sprogrammata"
date: 2017-10-07
comments: true
tags: [iPhone, Futuremark]
---
Siamo davvero nei guai. Futuremark si è chiesta *ma sarà vero che gli aggiornamenti di iOS rallentano i vecchi iPhone e che magari è fatto pure con intenzione, per spingere all’acquisto del nuovo?*.

E [ha verificato le prestazioni effettive di processore e circuiteria grafica](https://www.futuremark.com/pressreleases/is-it-true-that-iphones-get-slower-over-time) con iOS 10 e poi con iOS 11, a cominciare da iPhone 5s (il più vecchio ancora riconosciuto dal nuovo sistema) per arrivare fino a iPhone 7.

Ahimé, l’obsolescenza programmata in questo senso non esiste. Le prestazioni rimangono costanti al netto di fluttuazioni minimali che sono impercettibili nell’uso quaotidiano.

Ossia, non è vero che un nuovo iOS fa andare più lenti i vecchi iPhone.

Anni e anni e tonnellate di illazioni gratuite. E portatori insani di stupidaggini che da domani riprenderanno a berciare di avere ragione loro, comunque, perché la loro posizione è quella giusta a prescindere, checché ne dicano i grafici.

Niente è più scatenante per un invasato della prova provata di esserlo senza ragione. Siamo nei guai.