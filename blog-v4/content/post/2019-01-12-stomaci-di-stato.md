---
title: "Stomaci di stato"
date: 2019-01-12
comments: true
tags: [burocrazia, fatturazione, Spid]
---
Tra fine anno e inizio anno le congiunzioni astrali hanno fatto sì che mi occupassi simultaneamente di rinnovo patente, rinnovo passaporto e fatturazione elettronica.

I pregi dell’Italia e degli italiani si notano in molte situazioni diverse. Per vedere il peggio dell’Italia e degli italiani, basta e avanza la burocrazia.

Le procedure a spirale, i controsensi, i paradossi, il lessico, il tempo bloccato e la voglia tignosa di complicare e prevaricare senza mai pagare pegno. La ricerca dell’incomprensibilità e dell’inefficienza come strumento di conservazione del potere. Lo stesso potere che hanno il tarlo sul mobile, la muffa nell’angolo, il frutto che marcisce per primo nella cesta.

Se poi si mette in mano a questi indefinibili la tecnologia, è la fine. Tutto quello che è stato sviluppato finora in ambito tecnologico e digitale è nato all’incirca con scopi di potenziamento o semplificazione. Ma i gattopardi hanno capito l’antifona e hanno aggirato il (loro) problema.

Invece di informatizzare la burocrazia, hanno burocratizzato l’informatizzazione.

Tutto quello che poteva aiutarci è stato rivoltato e storpiato fino a farlo diventare una parodia del servizio dichiarato, che sembra inizialmente una svolta seria e, virgola dopo punto e virgola, ostinazione dopo stolidità, sfarina qualunque credibilità.

Vorrei conoscere qualche autore di questi capolavori procedurali, vedere i diagrammi di flusso sulle loro lavagne, sentire come parlano, cercare di capire come pensano soprattutto, e come si fa a diventare così. Quale percorso di studi, quali esperienze, quali attitudini portino alla totale indifferenza verso logica, buonsenso, consequenzialità.

Mi si dirà che operano in condizioni difficili, che ci sono anche quelli bravi, che tutto è molto migliorato e tutto il consueto armamentario di scuse e cavilli. Per carità, rispetto e compassione. Nonché meraviglia per uno stomaco che fa digerire veramente tutto, condizione necessaria per somministrare senza rimorsi la poltiglia amara a chi si presenta allo sportello.