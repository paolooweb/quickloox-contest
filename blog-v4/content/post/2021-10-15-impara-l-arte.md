---
title: "Impara l’arte"
date: 2021-10-15T01:28:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [ffmpeg, iMovie, YouTube, mChapters] 
---
Dopo vari esperimenti condotti con iMovie, [mChapters](https://www.tranquillitybase.jp/index_en.html) e soprattutto [ffmpeg](https://www.ffmpeg.org), che è straordinario ancorché esoterico, ho capito il necessario su come aggiungere capitoli a un video.

Peccato che YouTube non li importi e non li riconosca, a favore del *suo* metodo che a questo punto vedo necessario applicare *ex post* al posto delle soluzioni più eleganti che immaginavo fossero possibili.

Metto l’arte da parte. Però è stato un bel viaggio.

*Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile.*