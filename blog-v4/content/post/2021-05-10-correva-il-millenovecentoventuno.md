---
title: "Correva il millenovecentoventuno"
date: 2021-05-10T02:45:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [Mac, La classe capovolta] 
---
È si potevano leggere valutazioni come [questa](https://www.facebook.com/groups/laclassecapovolta/permalink/1815344605310139/?comment_id=1815459848631948&reply_comment_id=1815632405281359) sull’adeguatezza di Mac per il docente:

>Sì, e difficoltà a leggere file di formati diversi e variabili, tutto a pagamento sempre e comunque e poca compatibilità con Windows e Office che sono i sistemi più utilizzati in assoluto nelle scuole. I Mac secondo me vanno bene per i principianti con difficoltà (nel settore dell'insegnamento) e per chi ha anche una certa quantità di denaro personale da investire. Non è il caso di tutti.

*Principianti con difficoltà*.

Per fortuna, nonostante una guerra mondiale, una guerra fredda, anni di piombo, incidenti nucleari, pandemie e governanti assortiti siamo riusciti a progredire di cento anni.

*Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile.*