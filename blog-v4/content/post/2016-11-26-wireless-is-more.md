---
title: "Wireless is more"
date: 2016-11-27
comments: true
tags: [Wi-Fi, Note, Galaxy, Android]
---
Al termine del mio viaggio ad Amsterdam posso dire che ho praticato due aeroporti e due hotel più un museo, trovando cinque reti Wi-Fi vere.

Non quelle di cartapesta a velocità zero, non quelle che funzionano su un solo apparecchio, non quelle che vanno e vengono, non quelle con una procedura bizantina di iscrizione, non quelle che fanno finta di farti entrare e non entri mai, non quelle. Wi-Fi vere.

Non lo dico con soddisfazione, ma con speranza. Finalmente qualcuno inizia a capire, o forse aumenta il numero di quelli che si lamentano con ragione e vengono ascoltati. Se i bagni o i bar degli aeroporti funzionassero come certe reti Wi-Fi che ho trovato, si scatenerebbe la rivolta popolare. Internet, in un aeroporto, è un bene essenziale come l’acqua corrente e la possibilità di mangiare un panino.

Ultima nota: in partenza sull’ultimo volo, Amsterdam-Milano di Alitalia, i passeggeri sono stati invitati a consegnare al personale di volo i Galaxy Note 7.

E qui devo dare ragione a chi dice che un iPhone non può fare tutto quello che può fare un Android.