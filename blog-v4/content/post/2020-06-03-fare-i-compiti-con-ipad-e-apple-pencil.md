---
title: "Fare i compiti con l’iPad e l’Apple Pencil"
date: 2020-06-03
comments: true
tags: [software, Pencil, Covid-19, GoodNotes, iPad, Notability, Notes, Plus, Sabino]
---
_Ho il privilegio di condividere questa_ lectio magistralis _di **Sabino**. Chi preferisce può leggerla [sul suo blog Melabit](https://melabit.wordpress.com/2020/06/03/fare-i-compiti-con-ipad-e-apple-pencil/). Chi voglia iscriversi al gruppo Slack può scriverlo in fondo alla pagina, nei commenti._

§§§

 ![Fare i compiti con l’iPad e l’Apple Pencil](/images/ure.jpg  "Fare i compiti con l’iPad e l’Apple Pencil") 

Il canale Slack [Goedel](https://goedel.slack.com) di [Lucio Lux Bragagnolo](https://macintelligence.org/) è una miniera di discussioni stimolanti (se non siete iscritti fatelo, non ve ne pentirete). Qualche giorno fa Eugenio chiede:

_[…] per mio figlio ho preso un iPad 7 generazione, la differenza con il 4 si sente tutta […]. 
Ora avrei bisogno del vostro aiuto per un software di annotazioni. Siccome la scuola sarà pure diventata digitale ma i professori ancora ragionano analogico, c’è una professoressa che invia dei PDF (anche piuttosto corposi) che i ragazzi devono stampare, eseguire degli esercizi, fotografare e rinviare alla docente. Siccome volevo evitare l’acquisto di una stampante solo per questo motivo c’è qualcuno che usa Apple Pencil con Notability o Good Notes (mi sembrano i migliori leggendo le recensioni) disponibile a farmi alcune prove su un PDF? […]
Aggiungo una richiesta al volo, da Notability si riescono ad esportare singole pagine in PDF?  Con Anteprima su Mac è un gioco da ragazzi, su iPad non ho ancora trovato una soluzione._

Ci offriamo subito di dargli una mano e basta pochissimo per accorgersi che i risultati vanno molto al di là delle aspettative: un iPad moderno, non solo il Pro ma anche un normalissimo iPad modello base da 400 euro, è in grado di gestire agevolmente i grossi file PDF inviati dalla professoressa, aprendoli con [Notability](https://www.gingerlabs.com/), [GoodNotes](https://www.goodnotes.com/) ma anche con [File](https://support.apple.com/it-it/HT206481) (lapplicazione per la gestione dei file integrata da Apple nelle ultime versioni di iOS), e scrivendoci sopra con l[Apple Pencil](https://www.apple.com/it/apple-pencil/), quello che secondo me è di gran lunga il miglior accessorio per liPad.

Stefano usa un setup di alto livello, iPad Pro 12.9 e Apple Pencil di seconda generazione, e riporta che:

_Premessa: possiedo un iPad Pro 2018 da 12,9”. Ho scaricato il file e aperto il documento prima in Documents poi in Notability. 
In Documents l’apertura del file è immediata ma l’esperienza di editing con la Apple Pencil (seconda generazione) non è il massimo. Ma l’esportazione in PDF una volta eseguita la modifica del file è molto veloce.
In Notability a mio avviso ottieni il massimo dell’esperienza utente nel fare editing di in un PDF con Apple Pencil: tutto molto fluido. L’esportazione successiva in formato PDF del lavoro svolto richiede circa 20 secondi (file size 27.3 MB). […]
Non ho avuto alcun problema con Documents ma il tratto di tocco e fluidità che offre Notability è migliore. 
L’esportazione del PDF in Notability ha impiegato un lasso di tempo non degno del Pro (ben un minuto !) ma alla fine ha completato il task. Si, credo che la dimensione del file sia la causa di tutto._

Ma anche un iPad ultimo modello da 10.2 accoppiato ad una Apple Pencil di prima generazione, come quello che uso io, funziona alla grande, non arriverà di sicuro alle prestazioni di un iPad Pro, ma per quello che costa non cè proprio da lamentarsi.

_Ho provato ad aprire e ad editare il tuo documento sia con Documents che con Notability e Notes+ usando un iPad (non Pro) ultimo modello da 10.2. […]
Notability fra quelli che ho provato è il top: apre il documento in un attimo, lo scorrimento o lo zoom sono fluidi, si può scrivere con molta facilità negli spazi disponibili. Anche lesportazione è ottima, ci sono un sacco di opzioni e si può scegliere di esportare solo alcune pagine selezionandole in modo visuale. Non sono invece riuscito a esportare tutto il file dopo averlo editato, Notability mi dice che è troppo grosso, la soluzione ovvia sarebbe quella di rimetterlo su iCloud e continuare da lì.
È chiaro che sarebbe meglio rimpicciolire il pdf prima di importarlo. Se la prof non sa come si fa e non si può insegnarglielo, magari puoi farlo tu stesso con il Mac prima di passarlo alliPad, io uso PDF Toolkit+, soldi spesi benissimo.
Ho provato anche Notes+, ci sono alcuni aspetti utili come larea di zoom molto comoda, ma in realtà con una Apple Pencil non è necessario, si può benissimo scrivere sullo schermo senza paura di segni spuri. Purtroppo Notes+ ha un lag fastidioso quando si scrive e anche lesportazione delle singole pagine è meno comoda che con Notability, perché bisogna inserire a mano i numeri di pagina desiderati, che naturalmente non si ricordano mai la prima volta._


<hr style=width:50%; margin-left:25%; border : 0; border-top: 3px double #8c8c8c;>


Basterebbe solo questo per chiudere la questione. Ma largomento è stimolante, le scuole sono ancora chiuse causa emergenza COVID-19 e linsegnamento online sta cambiando, anche se lentamente, le modalità assestate di fare lezione e di studiare. In più, da un anno mi occupo per motivi professionali di file PDF, per cui decido di dedicare qualche ora ad approfondire il problema. Ed ecco un riassunto di quello che ho trovato.

**Notability.** Fra tutti i programmi di annotazione che ho provato, Notability è di gran lunga il migliore, in particolare se viene accoppiato ad una Apple Pencil. Importare un documento PDF in una nuova nota è un attimo, lo scorrimento del file è fluido e anche tutte le altre funzioni si attivano senza il minimo intoppo. Posso ingrandire leggermente la pagina e svolgere gli esercizi, proprio come se stessi usando un foglio di carta. 

 ![Apple Pencil](/images/notability.gif Scrittura su una pagina di un documento PDF con Notability e l "Scrittura su una pagina di un documento PDF con Notability e lApple Pencil") 
*Figura 1. Scrittura su una pagina di un documento PDF con Notability e l'Apple Pencil.*

Con uno schermo più grande come quello da quasi tredici pollici dell'iPad Pro avrei a disposizione un vero foglio A4, ma in fondo dover zoomare (poco) per poter svolgere comodamente gli esercizi è solo un piccolo fastidio, soprattutto se si pensa al costo, poco meno di 600 euro, della configurazione che sto usando.

Un altro grosso punto a favore di Notability riguarda le funzioni di importazione ed esportazione. Posso importare il file PDF da tutti i principali servizi di archiviazione cloud, da File (e quindi da iCloud) a Dropbox, da Google Drive a OneDrive e perfino da Box o da un server Webdav. Posso decidere di importare l'intero documento o solo alcune pagine, selezionando con il tocco le miniature di ciascuna pagina. Posso esportare il documento in formato PDF o come immagini (oltre che nel formato nativo di Notability) e, come per l'importazione, posso inviarlo a tutti i servizi di archiviazione cloud già menzionati e selezionare con facilità le pagine da esportare. 

L'unica vera limitazione di Notability riguarda l'impossibilità di inviare l'intero documento via Gmail, ma questo dipende dal fatto che il file di partenza è troppo grosso per essere gestito direttamente da Gmail (che ha un limite di 25 MB per gli allegati). Poco male, visto che si può usare il cloud e condividere via mail solo il link. Oppure si può _rimpicciolire_ preventivamente il file PDF, ma di questo parleremo un'altra volta.

 ![Esportazione da Notability: selezione del servizio di esportazione](/images/notability-esportazione-1.jpg  "Esportazione da Notability: selezione del servizio di esportazione") 
*Figura 2. Esportazione da Notability: selezione del servizio di esportazione.*

 ![Esportazione da Notability: opzioni di esportazione tramite email](/images/notability-esportazione-2.jpg  "Esportazione da Notability: opzioni di esportazione tramite email") 
*Figura 2 bis. Esportazione da Notability: opzioni di esportazione tramite email.* 

 ![Esportazione da Notability: scelta delle pagine da esportare](/images/notability-esportazione-3.jpg  "Esportazione da Notability: scelta delle pagine da esportare") 
*Figura 2 ter. Esportazione da Notability: scelta delle pagine da esportare.*

**Notes Plus.** Di Notes Plus [ho già parlato due anni fa](https://melabit.wordpress.com/2018/05/11/scrivere-a-mano-sull-ipad/) e continua ad essere la mia applicazione preferita per prendere note a mano sul mio venerando iPad 3, soprattutto perché permette di scrivere agevolmente su unarea ingrandita del foglio virtuale delliPad anche usando una penna da pochi soldi (e perfino il dito). Si può fare anche con Notability, ma in questo caso limplementazione è decisamente meno efficace di quella di Notes Plus.

Purtroppo Notes Plus risente il peso degli anni e le sue funzioni non sono state aggiornate a sufficienza per far fronte alla concorrenza di Notability. Lo si vede quando si prova ad importare il file PDF di Eugenio: Notes Plus supporta solo Dropbox e Google Drive, oltre che naturalmente File e quindi iCloud. Non è male, ma rispetto a Notability sembra un po pochino. Lo stesso vale per lesportazione: posso esportare tutto il file o solo alcune pagine, ma devo scrivere a mano i numeri di pagina che desidero, molto comodo per chi ha esperienza e deve fare selezioni complesse, molto meno per lutente medio. Anche i servizi cloud di esportazione sono un po troppo limitati rispetto a Notability.

Ma quello che è davvero seccante è il leggerissimo _lag_ che si avverte quando si prova a scrivere sulliPad con lApple Pencil. È appena percettibile, è vero, ma dà fastidio lo stesso, ed è inaccettabile per un programma così vecchio e blasonato,  e che costa pure un pelo più di Notability. Questultimo, da parte sua, non sa nemmeno cosa significhi il termine _lag_.

 ![Scrittura su un documento PDF con Notes Plus](/images/notes-plus.png  "Scrittura su un documento PDF con Notes Plus") 
*Figura 3. Scrittura su un documento PDF con Notes Plus.*

**GoodNotes.** Due parole anche su GoodNotes 4.[^1] Ammetto di non averlo provato più di tanto, ma l'impressione è che si comporti altrettanto bene di Notability. L'unica nota negativa di GoodNotes è che permette di esportare tutto il file oppure solo la pagina corrente.
Ma a parte questo dettaglio, usare una o l'altra app è più che altro una questione di gusti personali o di dettagli accessori, come la gradevolezza dell'interfaccia grafica o la possibilità, che solo Notability offre, di sincronizzare i documenti con il Mac.

**File.** Chi non vuole spendere nemmeno un centesimo o non ha bisogno di usare le funzioni più sofisticate di Notability o GoodNotes, può tranquillamente usare l'app File integrata in iOS. È una applicazione minimale, ma per scrivere sul file PDF e poi inviare il documento alla professoressa va più che bene. Secondo Stefano, Files è meno fluido di Notability, io francamente non me ne sono accorto, ma potrebbe trattarsi di sensibilità diverse. Oppure l'iPad Pro è così veloce che esalta anche queste piccolissime differenze, che invece con un iPad _liscio_ rimangono nascoste.

<hr style="width:50%; margin-left:25%; border : 0; border-top: 3px double #8c8c8c;">

E chi non ha un iPad abbastanza recente, magari uno di quelli [non compatibili con l'Apple Pencil](https://support.apple.com/it-it/HT211029)? Ci sono speranze di poterlo ancora usare per degli scopi come quello appena descritto? La risposta è sì, ma bisogna accettare qualche compromesso.

Per verificarlo, ho provato a ripetere le prove precedenti con il mio vecchio iPad 3, un tablet che ormai ha ben otto anni ma che continua a funzionare perfettamente, anche se ormai fatica a gestire le (poche) app recenti ancora compatibili (quelle più vecchie non hanno grossi problemi).

Con **Notability** non ho avuto nessun problema. Anche se il file proposto da Eugenio è piuttosto grosso, Notability riesce lo stesso ad importarlo in pochi secondi e lo scorrimento lungo il file ha una fluidità più che accettabile. La scrittura, perfino con una normalissima penna con la punta "gommosa", è piuttosto comoda, basta solo avere l'accortezza di ingrandire a sufficienza l'area di scrittura, a causa della minore precisione delle penne passive rispetto all'Apple Pencil. Nessun problema nemmeno per l'esportazione, veloce in assoluto, e ancora di più se si pensa che si tratta di un tablet di ben otto anni fa.

Anche **GoodNotes** si comporta molto bene, più o meno come Notability, e come dicevo prima usare l'una o l'altra app è più che altro una questione di gusti personali. Inutile dilungarsi oltre.

La vera sorpresa, ma in negativo, è **Notes Plus**, troppo lento a scorrere il file, e con un _lag_ in scrittura che ora diventa quasi una tortura. Praticamente inutilizzabile, un vero peccato.

Ho provato anche altre app della mia collezione ma tutte con difetti così seri da renderle di fatto inutilizzabili. La peggiore in assoluto è **Creative Notes**, che non solo pasticcia durante l'importazione, inserendo a caso più fogli del documento PDF nella stessa pagina, ma che richiede un acquisto in-app per effettuare l'esportazione. Una cosa legittima, sia chiaro, quello che non è legittimo è che non si degni di avvisarti prima di farti accedere all'App Store. Sembra proprio la classica trappola per i meno esperti, abituati a cliccare senza pensarci troppo. Di app truffaldine come queste non se ne sente proprio il bisogno.

[^1]: Usandolo poco, non l'ho mai aggiornato alla versione 5, la più recente disponibile sull'App Store.
