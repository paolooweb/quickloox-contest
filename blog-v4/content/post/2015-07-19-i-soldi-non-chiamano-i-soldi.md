---
title: "I soldi non chiamano i soldi"
date: 2015-07-21
comments: true
tags: [iPad, netbook, iPhone, Dediu, Asymco, PC, watch, HealthKit]
---
Si è [già visto](https://macintelligence.org/posts/2015-07-14-astenersi-perdisoldi/) come iPhone si aggiudichi il novantadue percento di tutti i profitti nel settore cellulare.<!--more-->

Horace Dediu di Asymco ci ricorda come Apple [abbia un notevole successo](http://www.asymco.com/2015/07/17/how-ipad-educates/) anche con i Mac: ogni mese i Mac portano a casa profitti per cinquecento milioni di dollari. I PC, tutti i PC, tutti quanti insieme, compresi i mitici *netbook* che Apple avrebbe dovuto fabbricare perché tutti ne volevano uno, ne fanno 310 milioni.

iPad, quello che sarebbe in crisi ogni mese raccoglie 333 milioni di profitti; più dei PC, tutti insieme, compresi gli ibridi, i convertibili, i doppia faccia, i multiuso, quelli a rotelle e quelli del circo.

Uno dice *bella forza, Apple fa i prezzi alti*. Apple in effetti fa un’altra cosa:

>15 milioni di iPad sono in uso nelle istituzioni didattiche. Un numero sconosciuto è in mano per scopi didattici a insegnanti e utenti finali. Il risultato sono centomila app per la didattica su App Store e le guide compilate da docenti per altri docenti. Non c’è un modo di conoscere il “profitto” di questa attività.

>Né c’è modo di conoscere il “profitto” di quanti fanno esercizio perché indossano un watch.

>Neanche si può tenere traccia del profitto di HealthKit che ha portato alla creazione di mille app che aiutano le persone a restare in salute.

Si arriva al punto.

>Questi sforzi nella didattica e nella salute non sono progettati per generare profitto. Eppure Apple accumula grandi profitti. Creare cose di valore porta generalmente il loro creatore a vedersi riconosciuto valore. Come accade non è tanto importante. Concentrarsi su questo, anzi, spesso è perdente. Importano il perché e il che cosa si fa. Comprendere questo porta a capire le fondamenta di qualsiasi grande organizzazione, non solo di Apple.

Chi guarda ai soldi di Apple vede il dito. Dietro c’è una luna.