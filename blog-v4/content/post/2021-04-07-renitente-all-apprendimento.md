---
title: "Renitente all’apprendimento"
date: 2021-04-07T03:45:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [scuola, DaD, Axios] 
---
La primogenita al suo primo anno di scuola primaria torna in aula dopo un mesetto di blocco e di lezioni a casa.

Questo post doveva contenere una serie di elogi, spiegati, alle maestre per come hanno organizzato l’attività e tenuto vivo l’interesse di bambini di sei anni senza alienarli con videolezioni scervellate. Hanno lavorato davvero bene, con semplicità ed efficacia.

Invece lo scrivo domani, perché la società che fornisce la piattaforma di registro elettronico alla scuola della primogenita è stata attaccata da pirati informatici che hanno cifrato i contenuti della piattaforma e chiesto un riscatto; il classico *ransomware*.

Per portare a segno un attacco di questo tipo, occorrono debolezze di piattaforma e ingenuità nelle persone. In ogni caso, non dovrebbe essere una minaccia capace di impattare significativamente su un’azienda; e, se lo fa, dovrebbe esistere una procedura apposita di *disaster recovery* per liberarsi del problema.

Invece, no. In questo momento la [home di Axios Italia](https://axiositalia.it) riporta quanto segue:

§§§

06/04/2021 - Ore 08:45

__________________________________

Gentili Clienti,

a seguito delle approfondite verifiche tecniche messe in atto da Sabato mattina in parallelo con le attività di ripristino dei servizi, abbiamo avuto conferma che il disservizio creatosi è inequivocabilmente conseguenza di un attacco ransomware portato alla nostra infrastruttura.

Dagli accertamenti effettuati, al momento, non ci risultano perdite e/o esfiltrazioni di dati.

Stiamo lavorando per ripristinare l'infrastruttura nel più breve tempo possibile e contiamo di iniziare a rendere disponibili alcuni servizi a partire dalla giornata di mercoledì.

Sarà nostra cura tenervi costantemente aggiornati.

05/04/2021 – Ore 18:20

__________________________________

Gentili Clienti,

nello scusarci per il protrarsi del disservizio, teniamo a informarVi che stiamo lavorando alacremente con l’obiettivo di rendere disponibili tutti i servizi web entro pochi giorni. Sarà nostra cura aggiornarVi al loro ripristino.

05/04/2021 – Ore 13:20

_______________________________

Gentili Clienti,

a seguito di un improvviso malfunzionamento tecnico occorso durante la notte, si è reso necessario un intervento di manutenzione straordinaria.

Sarà nostra cura darVi comunicazione alla ripresa del servizio.

Scusandoci per il disagio, per qualsiasi necessità rimaniamo a Vostra completa disposizione attraverso l’indirizzo mail contatti@axiositalia.com

03/04/2021 – Ore 11:00

§§§

Mentre scrivo, sono tre giorni che i servizi sono fermi. Soprattutto, il tentativo iniziale di fare passare per *malfunzionamento tecnico* un attacco *ransomware* fa cadere le braccia.

Questa azienda ha i requisiti per fornire il registro elettronico alla scuola pubblica italiana? Registro che, stante l’ordinamento della scuola pubblica suddetta, è di importanza critica (almeno burocratica)?

Perché non si riesce mai con la scuola a fare una cosa come si deve fino in fondo? Perché la scuola insegna ma sembra che mai riesca a imparare pienamente?

Vediamo intanto se oggi tornano su.

*Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile.*