---
title: "Tutto scorre"
date: 2017-05-23
comments: true
tags: [Safari, Amp, Google, scorrimento, scrolling, scroll]
---
Si [faceva riferimento](https://macintelligence.org/posts/2017-04-04-corsi-ricorsi-e-convenzioni/) al fatto che non esistono uno scorrimento naturale e uno innaturale, così come una striscia Oled di tasti funzione non è meglio né peggio di una striscia di tasti funzione. Sono convenzioni più o meno adatte in funzione del tempo, che quindi – sotto la pressione del tempo che passa – si modificano.<!--more-->

Mi è sovvenuto leggendo della polemica che sta montando a proposito del [protocollo Amp](http://www.apogeonline.com/webzine/2016/02/22/il-web-in-un-lampo) di Google e di come, in particolare per il punto di vista di questo articolo che ignora volutamente le questioni dell’apertura del web e delle politiche di Google, cambi alcune abitudini di chi naviga con Safari in iOS.

Su Hacker News c’è una [discussione puntigliosissima](https://news.ycombinator.com/item?id=14384938) dalla quale si evince che WebKit, il motore dietro Safari, sta per applicare appunto [alcuni cambiamenti](https://trac.webkit.org/changeset/211197/webkit) alla regolazione dello scorrimento delle pagine in determinate circostanze. E chi ne parla commenta così:

>Avere uno scorrimento sempre coerente, una volta che ci hai fatto l’abitudine, ti fa sentire a tuo agio.

Posso solo sottoscrivere. Se è sbagliato solo perché siamo abituati in altro modo, non è sbagliato ma diverso. Perché sia sbagliato ci vuole più di questo e può darsi che diverso sia migliore di quello che è sempre stato. Perché tutto scorre, non solo le pagine web.