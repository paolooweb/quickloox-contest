---
title: "Vieni avanti killerino"
date: 2015-03-26
comments: true
tags: [iPhone, Gilbert, Yahoo, Torch, Storm, Pre, Galaxy, BlackBerry, Samsung, Droid, Motorola, Elsa, WindowsPhone7, Evo, Hd2, Touch, Htc, Xperia, X10, SonyEricsson, Voyager, Ku990, Lg]
---
Il lavoro lo ha fatto tutto Jason O. Gilbert di Yahoo: [sedici *smartphone* che sono stati definiti *iPhone killer* tra il 2008 e il 2011](https://www.yahoo.com/tech/16-smartphones-that-were-deemed-iphone-killer-114506321304.html).<!--more-->

Nexus One, BlackBerry Torch 9800 e Storm, Palm Pre, Galaxy S II e Sgh-F480 di Samsung, Droid di Motorola, Else, Windows Phone 7 (tutta la serie), la triade Evo 4G-Hd2-Touch di Htc, Xperia X10 di Sony Ericsson, Voyager e Ku990 di Lg.

Più che ridere dei titolisti dei siti spazzatura, invito alla riflessione sulle qualità che fanno veramente di iPhone un prodotto di punta. Sono da leggere le motivazioni e le caratteristiche che rendevano *killer* i sedici nanetti di questa classifica, per capire quanto valgano poco.