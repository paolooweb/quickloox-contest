---
title: "Cinque anni dopo"
date: 2015-03-10
comments: true
tags: [iPad, CultOfMac, Disney, McDonald’s, YouTube, Flash]
---
Tutti stanno scrivendo di orologi. Preferisco stare sulle tavolette, con questo [articolo carino di *Cult of Mac*](http://www.cultofmac.com/310020/ipad-haters-5th-anniversary/) scritto per il quinto anniversario dell’annuncio di iPad.<!--more-->

Effettivamente [si scrisse](http://www.np.reddit.com/r/technology/comments/auuxp/the_ipad_no_flash_looks_like_a_big_itouch_with_an/c0jii4y) che iPad era un prodotto per anziani. L’anno scorso un sondaggio lo dava come [marchio più popolare nella fascia 6-12](http://www.cultofmac.com/298931/kids-love-ipads-oreos-disney-youtube/), prima di Disney, YouTube e McDonald’s tra gli altri.

Giocando sul fatto che *pad* indica nel parlato comune anche gli assorbenti femminili, le ironie [fluirono libere](http://jezebel.com/5458338/that-time-of-the-month-the-best-period-related-ipad-jokes).

Per non parlare della mancanza di una fotocamera e della fatale, irreparabile compatibilità con Flash.

Da ricordarsene per le ironie che probabilmente ora abbondano sugli orologi.