---
title: "Marcia indietro, prima, avanti di nuovo"
date: 2023-04-18T18:37:16+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [Freeciv]
---
Devo fare parzialmente marcia indietro sulle mie affermazioni riguardanti [FreeCiv per Mac](https://macintelligence.org/posts/2023-04-05-a-difesa-della-free-civiltà/), perché non mi ero accorto di come l’unica immagine montabile per Mac sia, per ora, quella della beta del gioco e, in particolare, del fatto che proprio non parte, meglio, parte e si chiude immediatamente.

Privo di qualunque voglia di investigare sulle cause, ho ripiegato su [Homebrew](https://brew.sh), sistema che è stato sempre disponibile. Dopo l’insuccesso della beta volevo però verificare che funzionasse davvero.

`brew install freeciv` ha dato esito del tutto positivo. L’unica cosa da sapere, rintracciabile solo nel chiacchiericcio su Web, è che per fare partire il gioco occorre dare da Terminale il comando `freeciv-gtk3.22`, esattamente in questa forma.

Ma lo facciamo un torneino?