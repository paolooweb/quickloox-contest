---
title: "Il suono nella storia"
date: 2016-11-01
comments: true
tags: [MacBook, Pro, bong]
---
I [nuovi MacBook Pro](http://www.apple.com/it/macbook-pro/) sono i primi nella storia di Apple a [partire senza il caratteristico *bong* di apertura](https://pingie.com/2016/10/28/apple-says-goodbye-to-the-startup-chime-with-the-new-macbook-pro/) (che poi è cambiato diverse volte).

C’è anche un motivo: da spenti e chiusi, basta aprirli perché si accendano. Si comportano come ha sempre fatto un MacBook Pro in stop. E un MacBook Pro in stop che si risveglia, non fa rumore. In una sala riunioni affollata, o una biblioteca, o in casa mentre tutti dormono, fa la differenza.

L’articolo linkato contiene anche il rimando a un file che raccoglie i vari suoni di apertura succedutisi negli anni. Il nostalgico potrebbe anche creare un AppleScript da eseguirsi all’avvio del computer e sarebbe quasi la stessa cosa.

Non moltissimi, pare, hanno fatto caso all’assenza dal listino attuale di portatili Apple con lettore ottico. Il mondo cambia e perfino le tecnologie più pervasive, a un certo punto, sono adatte per la storia e non molto di più.