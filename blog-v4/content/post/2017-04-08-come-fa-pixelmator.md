---
title: "Come fa Pixelmator"
date: 2017-04-08
comments: true
tags: [Pixelmator, iCloud]
---
Il discorso sulle [convenzioni che cambiano](https://macintelligence.org/posts/2017-04-04-corsi-ricorsi-e-convenzioni/) al mutare delle circostanze spiega molte decisioni di interfaccia utente di Apple, oltre contingenze come la direzione di scorrimento dei contenuti di una finestra o la sostituzione di tasti funzione fisici e immutabili con una barra Oled sensibile al tocco.

Per esempio, il filesystem tradizionale contrapposto all’architettura app-centrica di iOS, anche se sarebbe meglio dire di iCloud.

Ieri ero in viaggio e mi serviva modificare un file grafico di Pixelmator, presente nel mio Mac e salvato in iCloud, ovvero “dentro” Pixelmator.

Ho lanciato la versione iPad del programma. Il file era già lì dentro, identico alla versione che avrei trovato su Mac. Ho apportato le modifiche e inoltrato il file dove serviva. Missione compiuta.

A sera sono arrivato al Mac che mi aspettava a casa. Ho lanciato Pixelmator e il file era pronto e aggiornato, senza che io avessi mosso un dito per favorire la sincronizzazione.

È un caso particolare nel quale l’organizzazione di file centrata sulla app è vastamente più efficiente di quella tradizionale. In che cartella ho lavorato? Non lo so, non serve saperlo.

Ovviamente ci sono altre situazioni nelle quali l’organizzazione classica batte quella app-centrica e, se c’è un campo dove Mac è superiore a iPad, è proprio nel poter offrire al massimo livello entrambe le esperienze, dopo di che uno si regola come crede.

Appunto. In certi casi è meglio una specifica organizzazione, in altri no. Non c’è niente di giusto o sbagliato, né di eterno o immutabile. Convenzioni che si modificano al mutare dei tempi e delle circostanze.
