---
title: "Una app con cui parlare"
date: 2014-08-11
comments: true
tags: [UniversalAccess, Apple, UABlog]
---
Era fine gennaio quando [cedevo la parola](https://macintelligence.org/posts/2014-01-31-il-mac-che-non-si-vede/) a **Rossy KK**, che raccontava dell’intersezione tra mondo Apple e mondo dei non vedenti.<!--more-->

Mi ha scritto – indirettamente – anche a maggio e colpevolmente non sono riuscito a prendere in considerazione il suo messaggio fino a ora.

Lo riporto comunque, con più chiavi di lettura. La prima è la conferma su quali siano i migliori apparecchi possibili per chi deve fare a meno, parzialmente o completamente, della vista; la seconda è ricordare a chiunque ne traesse beneficio dell’esistenza di comunità che facilitano l’utilizzo della tecnologia a chi non vede grazie all’incontro con chi condivide la medesima condizione; Internet [dà problemi di *privacy*](https://macintelligence.org/posts/2014-08-08-patti-chiari/) e però rappresenta anche una risorsa sensazionale, a usarla bene.

L’ultima chiave di lettura è che l’impegno di persone non vedenti possono portare alla realizzazione di una *app*. Quando non riusciamo a fare una cosa con un Mac o con un iPad, dobbiamo mettere in conto prima il tempo che gli abbiamo dedicato e solo dopo il resto. Si tratta di imparare, e volere.

Il testo che segue è tutto di **Rossy KK**.

La nostra è una comunità di utenti Apple che ha la particolarità di essere formata da, e rivolta a, persone cieche e ipovedenti utenti di Mac, iPhone e iPad.

Il nostro nome è [Universal Access](http://www.universalaccess.it) e siamo la comunità più numerosa e longeva di utenti Apple italiani con disabilità visiva.

Da quando esistiamo, il nostro obiettivo è sempre stato quello di diffondere e scambiare esperienze e conoscenze relative all’uso dei dispositivi Apple da parte degli utenti e appassionati ciechi e ipovedenti come noi. I tutorial e podcast presenti sul nostro sito, così come i momenti formativi a contatto diretto con gli utenti, che abbiamo organizzato o a cui abbiamo partecipato, e la nostra mailing list, nascono tutti da questo obiettivo.

Le persone non vedenti per utilizzare i computer, smartphone e tablet hanno bisogno di un software particolare di lettura schermo che traduca i contenuti visivi del display in messaggi verbali comunicati via sintesi vocale; mentre gli ipovedenti hanno bisogno di software d’ingrandimento dei contenuti dello schermo.

La Apple è stata la prima azienda informatica mainstream ad integrare questi software specifici per disabili visivi nei propri prodotti, per la filosofia da lei definita di *accesso universale*. Secondo questa filosofia, anche gli utenti disabili devono essere in grado di usare un Mac o dispositivo iOS con semplicità fin dalla sua prima accensione, senza bisogno di aggiungere e comprare ulteriore software o hardware.

L’accesso universale introdotto dalla Apple per noi è stato una novità entusiasmante, e ci ha dato la spinta e la motivazione per far nascere Universal Access.

Oggi il nostro sito è anche un’app per iOS, chiamata [UABlog](https://itunes.apple.com/it/app/uablog/id871238042?l=en&mt=8).

L’app permette agli utenti disabili visivi di navigare con facilità il nostro sito, di scaricare i materiali da noi prodotti (tutorial e podcast) e di consultarli anche off-line.

Inoltre dall’app si ha accesso alle dirette che trasmetteremo in streaming, sempre incentrate sul tema dell’accessibilità dei prodotti Apple per gli utenti disabili visivi.

Saremmo felici se ci vorrete aiutare a far conoscere questo spazio che può tornare utili agli utenti con problemi visivi alle prime armi con i prodotti apple.

Vi ringraziamo in anticipo per la vostra attenzione e rimaniamo a disposizione per ulteriori informazioni e chiarimenti.

Firmato,
lo staff di <a href="mailto:info@universalaccess.it">Universal Access</a>: Fabio Strada, Rossy KK, Giovanni Lo Monaco e Guglielmo Boni