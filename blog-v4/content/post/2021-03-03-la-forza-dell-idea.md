---
title: "La forza dell’idea"
date: 2021-03-03T10:29:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [Dock, Aqua, Mac OS X, macOS, OS X, Tech Reflect, Big Sur] 
---
La storia della nuvoletta con cui scompaiono gli elementi asportati dal Dock si è chiusa dopo vent’anni; Big Sur fa semplicemente svanire l’elemento.

Però la nuvoletta c’è stata e una storia ce l’ha, meno complicata e avvincente di altre del mondo Apple, non meno stimolante.

[Racconta Tech Reflect](https://techreflect.net/2021/02/22/origin-on-macos-cloud-poof-animation/) che l’animazione, insolitamente rozza per lo standard di pulizia grafica che contraddistingueva l’interfaccia grafica Aqua del primo Mac OS X, era nient’altro che un bozzetto realizzato in forma provvisoria da un disegnatore.

Avrebbe dovuto essere ripresa, curata, uniformata allo stile Aqua. Invece piaceva così tanto ed era così tanto perfetta per lo scopo, anche se bozza, che venne lasciata nel suo stato originale.

Una bella lezione di design. Quell’abbozzo conteneva dentro tutti gli intangibili che contribuiscono al successo di un elemento di interfaccia. Era un’idea forte, abbastanza da superare la sua provvisorietà nativa.

Mi sono sforzato di pensare ad altri esempi, ma non ne ho trovato nessuno ugualmente intenso. La nuvoletta del Dock, graficamente parlando, è anche durata più di qualunque altro elemento sulla schermata tipo del sistema operativo.

*Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile.*