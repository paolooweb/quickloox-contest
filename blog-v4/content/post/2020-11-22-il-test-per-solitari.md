---
title: "Il test per solitari"
date: 2020-11-22
comments: true
tags: [M1, Pixelmator, Gruber, Sinofsky, Tensorflow]
---
La transizione è cosa grossa, se ne parlerà per un bel po’ e cambieranno tante cose. Lo [dice Steven Sinofsky](https://twitter.com/stevesi/status/1329512596022513664), ex responsabile di Windows, non uno scappato di casa:

<blockquote class="twitter-tweet"><p lang="en" dir="ltr">Most amazing about M1 Macs is that the fact that Macs got faster (than Intel), fanless, and crazy battery life in old form factors is a 2nd order effect of where computers and software are going.<br><br>Imagine your product’s interim first step being paradigm shifting. Just step 1. 💯</p>&mdash; Steven Sinofsky (@stevesi) <a href="https://twitter.com/stevesi/status/1329512596022513664?ref_src=twsrc%5Etfw">November 19, 2020</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

>Immagina che il primo passo del tuo prodotto sia spostare il paradigma. Ed è il primo passo.

I negazionisti su Facebook e altri media sembrano quelli del virus e, se a volte sono irritanti o generano frustrazione, ora fanno ridere, non sanno dove attaccarsi. Scorrerà tanto popcorn.

Una delle ragioni è che il chip M1 contiene una ottimizzazione del *machine learning*, l’apprendimento meccanizzato del computer. Tensorflow, una delle piattaforme per addestrare il computer e aumentare la precisione delle sue analisi, [viene clamorosamente accelerato](https://blog.tensorflow.org/2020/11/accelerating-tensorflow-performance-on-mac.html) rispetto ai Mac Intel se lo si usa su macchine M1. Perché il processore Arm dei nuovi Mac è pensato anche per fare machine learning al massimo.

Il machine learning è una delle questioni principali dei prossimi anni dell’informatica. Sembra comunque una questione lontana? Va bene: prendiamo Pixelmator 2.0, pronto per M1. Il programma usa tecniche di machine learning per ingrandire artificialmente immagini piccole con la migliore qualità possibile, nonostante debba letteralmente indovinare che tipo di pixel mettere attorno a un pixel originale.

I risultati di Pixelmator 2.0 non sono quelli dei ricercatori che svolgono esperimenti a questo scopo, ma sono comunque estremamente buoni. Pixelmator ha un prezzo totalmente ragionevole e può atterrare su qualsiasi scrivania di chi abbia l’esigenza di ingrandire una immagine che rimanga nitida. Vita di tutti i giorni, o quasi.

Su M1, la funzione di ingrandimento [va fino quindici volte più veloce](https://www.pixelmator.com/blog/2020/11/13/pixelmator-team-unveils-pixelmator-pro-2-0-with-support-for-m1-powered-macs/) grazie all’architettura ottimizzata per l’apprendimento automatico.

Ora uno potrà chiedersi come si comportano gli altri processori muniti di una architettura per ottimizzare il machine learning, per fare il confronto con M1.

Il fatto è che, se prendiamo macchine comparabili con un MacBook Air o un Mac mini… non ce ne sono. In questo momento M1 offre una funzione unica nel suo genere, sui computer meno costosi di Apple. (*Dimmi una cosa che puoi fare con il tuo costoso MacBook Air che io non possa fare con il mio PC che costa la metà*, bofonchiano gli hater sui social)

Arriveranno, ovviamente. Copiare è permesso. Ma oggi, qui e ora, si può chiedere al fanatico di Windows *che prestazioni ha l’unità specializzata di machine learning del tuo processore da casa?* e guardarlo che si nasconde nella lavatrice.

Un test con un vincitore sicuro e indiscusso, perché ha un partecipante solo.