---
title: "Il decennio dell’ahio"
date: 2023-04-04T15:35:30+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [intelligenza artificiale, Artificial Intelligence, AI, Chollet, François Chollet, Eliza, Minsky, Marvin Minsky, ChatGPT]
---
Il 23 agosto 1976 il giornalista e scrittore Tom Wolfe pubblicò sul New York Magazine [Il decennio dell’io](https://nymag.com/news/features/45938/), un report che era praticamente un libro su quello che lui chiamò il (terzo) grande risveglio (religioso) americano, dove la religione sulla cresta dell’onda era quella del sé.

Da diversi anni utilizzo il testo di Wolfe (anche [tradotto in italiano per i tipi di Castelvecchi](https://www.libraccio.it/libro/9788876158377/tom-wolfe/decennio-io.html) a una cifra modestissima per il suo valore) all’interno di corsi e conferenze. La ragione è semplice: Wolfe vedeva un decennio, ma la verità è che tra poco siamo al mezzo secolo e la religione del sé non mostra alcun segno di tramontare, anzi. È di una attualità tragica e scandalosamente sottovalutata. Forse anche incomprensibile all’interno di certe fasce d’età, che si trovano nella condizione dei pesci a nuoto in una boccia e si fanno domande sul mondo. Che ci sia un fuori dalla boccia, neanche gli passa per la testa.

Per questa gente, abituata a pensare in termini di sé e soprattutto di sé attorno a cui ruota l’universo, la prospettiva di avere per le mani una intelligenza artificiale che gli obbedisce, gli risponde e sostanzialmente si atteggia a servo, è il massimo dell’autogratificazione. Spiegargli a che livello stanno le cose e a che punto possiamo essere davvero aiutati da una intelligenza artificiale, è del tutto inutile.

Per tutti gli altri, con una possibilità di salvezza e di chiarimento delle idee ancora a disposizione, c’è questo [tweet di François Chollet](https://twitter.com/fchollet/status/1643021276729196544) che sintetizza bene la situazione come è veramente, piuttosto che come viene venduta:

<blockquote class="twitter-tweet"><p lang="en" dir="ltr">In 2033 it will seem utterly baffling how a bunch of tech folks lost their minds over text generators in 2023 -- like reading about Eliza or Minsky&#39;s 1970 quote about achieving human-level general intelligence by 1975</p>&mdash; François Chollet (@fchollet) <a href="https://twitter.com/fchollet/status/1643021276729196544?ref_src=twsrc%5Etfw">April 3, 2023</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

Chollet è tutt’altro che un ultimo arrivato e ne riparlerò. Una traduzione abborracciata del tweet è la seguente:

>Nel 2033 sembrerà oltremodo sconcertante il come un gruppo di tecnologi abbia perso la testa nel 2023 per i generatori di testo; un po’ come leggere di Eliza o ritrovare la citazione di Marvin Minsky nel 1970 riguardo al raggiungimento dell’intelligenza generica di livello umano per il 1975.

Nei prossimi dieci anni si dispiegherà il decennio dell’*ahio*: infinite persone con un ego ipertrofico e sufficiente miopia da andare a sbattere contro un muro di supposizioni e fantasie riguardanti l’intelligenza artificiale e la sua applicazione nel mondo concreto. Finirà, ma come al solito si saranno fatti danni.

L’importante è che sia chiaro a tutti che lo si sapeva ed era evidente a qualunque persona assennata, a prescindere da quanto di Wolfe abbia letto.