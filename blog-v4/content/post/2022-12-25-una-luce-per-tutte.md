---
title: "Una luce per tutte"
date: 2022-12-25T02:49:46+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [BeLight]
---
Il basket prima e il lavoro dopo mi hanno insegnato che giova più alla propria felicità perseguire quella altrui. Un obiettivo della vita è fare il possibile per diffondere la felicità attorno a sé.

Sono parole grosse in una notte speciale e non me la sento di argomentare. Amare la diversità di pensiero è un altro modo di essere felici, sebbene funzioni nello stesso modo: la propria felicità è proporzionale alla diversità di pensiero che si ha intorno.

Non per questo, ma per esserci e condividere il cammino, ringrazio e saluto tutti quanti sono qui a leggere. Che possa essere una giornata da ricordare e solo per motivi buoni, nobili o ambedue. Grazie sempre.

Infine, a riassumere altre parole grosse in una notte speciale: buon Natale, [Belight Software](https://www.belightsoft.com).