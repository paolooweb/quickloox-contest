---
title: "Imputati per stage"
date: 2023-02-05T01:58:47+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [Stage Manager, iPadOS, macOS]
---
Non solo ho avuto [il coraggio di accendere Stage Manager](https://macintelligence.org/posts/2023-01-28-la-fortuna-aiuta-gli-audaci/), ma ci ho anche lavorato.

Il concetto fila. È curioso come, applicato su Mac, in sostanza tenda a lasciare sullo schermo una sola app invece di molte e, su iPad, tenda a lasciare molte app sullo schermo invece di una.

Su Mac la realizzazione è riuscita a metà. Trovo gli automatismi troppo rigidi e drastici; lanci un documento in più e qualcosa compare sullo schermo, quindi qualcos’altro si riduce e va nella barra a sinistra. Probabilmente sono i miei flussi lavoro confusi e le mie troppe app aperte; servire uno come me, per Stage Manager, è una sfida.

Infatti fa fatica. Continua a mancarmi qualcosa a schermo, che devo riportare a mano dalla barra di sinistra alla piena dimensione. Una distrazione ed ecco che, delle quattro app con cui voglio lavorare, una finisce ridotta. La riporti a mano. E avanti.

Lo sforzo di razionalizzazione è lodevole, però mancano fluidità e accoglienza. Se i flussi di lavoro sono stabili, sempre quelli, è una manna. Se le cose cambiano, diventa fastidioso. A me le cose cambiano. Così, all’ennesima finestra che non era dove doveva essere, ho disattivato Stage Manager.

Su iPad invece mi sembra più riuscito. Avere più finestre sullo schermo, con meno rigidità del multitasking precedente, è piacevole. Anche il ridimensionamento delle finestre, che avviene secondo schemi fissi e formati predefiniti, sa di iPad ed è qualcosa di diverso dal solito. Non mi ha cambiato la vita, ma è una aggiunta piacevole che di tanto in tanto velocizza certe operazioni prima più macchinose.

Il lettore di fantascienza che dorme in me suggerisce che Stage Manager non sembri creato per iPad e neanche per Mac. È un genere di interfaccia che mi aspetterei di vedere usato per mettere a disposizione documenti o strumenti in una situazione di realtà aumentata o virtuale… e chissà che le implementazioni attuali su macOS e iPadOS siano, in realtà, messe a punto di qualcosa che deve arrivare.