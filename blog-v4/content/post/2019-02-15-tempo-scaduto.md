---
title: "Tempo scaduto"
date: 2019-02-15
comments: true
tags: [Gates]
---
I coniugi Gates hanno di recente pubblicato la loro [Annual Letter](2019-02-18 00:41).

All’interno si può leggere una scioccante novità:

>I libri di testo iniziano a diventare obsoleti.

Se è arrivato ad accorgersene Bill Gates, vuol dire che l’evidenza è ormai soverchiante e il bisogno di agire urgente. Se qualcuno dal pianeta scuola è in ascolto e non ha voglia di ascoltare me, che non ho titolo, almeno ascolti Bill. Sta facendo del suo meglio per espiare, io dico che è sincero.
