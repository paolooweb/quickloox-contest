---
title: "Monaci digitali"
date: 2017-06-01
comments: true
tags: [Poc, ATM, Trenord, McDonalds, iPhone, app, watch, pay]
---
A cavallo del millennio esistette il [Poc](http://www.poc.it/), PowerBook Owners Club: persone che avevano fatto della mobilità informatica uno stile di vita e alzavano l'asticella continuamente per compiere con un portatile attività ritenute comunemente impossibili o molto ostiche.<!--more-->

I tempi sono cambiati al punto che forse oggi occorrerebbe un Dop, un club di appassionati di *computing* desktop.

La riprova è che in un rigurgito di gioventù sono uscito di casa come accadde il primo giorno di liceo: senza portafogli.

Ed è stata una normalissima giornata di lavoro e movimento a Milano: con iPhone ho pagato il treno via app, la metropolitana pure, il pranzo anche. Non che in qualunque locale di Milano si possa pranzare e pagare via app, ma ne esistono diversi e uno è stato abbastanza.

Mentre le conquiste del Poc erano davvero tali, questa è la scoperta dell'acqua calda, quelle cose di cui ci si accorge al momento ma bastava pensarle. Scommetto che migliaia di persone lo trovano del tutto consueto e si stupiscono del mio scriverne. È che la pratica fa sempre più effetto di qualsiasi teoria; se anzi avessi equipaggiamento più moderno e vivessi in un Paese più progredito, avrei potuto cavarmela unicamente con watch e pay. Un grado ulteriore di libertà e affrancamento dal mondo fisico. Parlarne è una cosa, farlo veramente è un’altra. Specie quando non ci sono alternative realistiche per rispettare l’agenda di lavoro.

Ho apprezzato questa dimensione quasi monastica. Niente in tasca salvo il terminale connesso alla rete globale, i bisogni primari soddisfatti per via immateriale, atomi spostati attraverso i bit. Più che un club dovrebbe prendere vita un *dojo*, una comunità virtuale di chi cerca l’illuminazione per mezzo del minimo ingombro nelle tasche.

Ogni rivoluzione chiede comunque compromessi e, nel mio impeto innovatore, sono uscito anche senza le chiavi di casa. Il rientro ha richiesto la riorganizzazione dell’agenda in accordo con la famiglia. Un bel modo per riunirsi, oppure il segno che è ora di pensare a [Schlage Sense](http://www.schlage.com/en/home/keyless-deadbolt-locks/sense.html).


