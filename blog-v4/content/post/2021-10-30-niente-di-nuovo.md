---
title: "Niente di nuovo"
date: 2021-10-30T02:33:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [Apple, Asymco, Horace Dediu, Services] 
---
Quando chiedono a Horace Dediu che cosa c’è di nuovo in Apple, dice lui, mostra [questo grafico](https://twitter.com/asymco/status/1454090614899941383). Non c’è niente di nuovo.

<blockquote class="twitter-tweet"><p lang="en" dir="ltr">If anybody wants to know what new with Apple I show this graph. (I.e. Nothing’s new.) <a href="https://t.co/0na1nP8ryy">pic.twitter.com/0na1nP8ryy</a></p>&mdash; Horace Dediu (@asymco) <a href="https://twitter.com/asymco/status/1454090614899941383?ref_src=twsrc%5Etfw">October 29, 2021</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

L’immagine mostra serie storiche di dati che normalmente passano inosservati o neanche vengono sempre comunicati, tipo account iCloud o numero di apparecchi attivi. Se si aggiunge la curva degli abbonamenti a pagamento, cioè i Services che dovevano essere una scappatoia per ovviare alla fine della crescita del mercato di iPhone e oggi sono il secondo business più importante, emerge il quadro di una rotta tracciata dieci anni fa e seguita con somma regolarità.

Inutilmente si possono cercare i segni di quando i professionisti avrebbero abbandonato Mac, o del momento in cui si sarebbe spenta l’innovazione. Peraltro tutto è in crescita sostenuta, a smentire l’ipotesi di una amministrazione dell’esistente senza idee o di una aurea mediocrità.

O i media raccontano un sacco di storie gonfiate senza presupposto, a caccia di clic, o l’attività di Apple poggia su pilastri progettati con cura sovrumana, che si fa persino fatica a prendere in considerazione. O forse tutt’e due.

*Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile.*