---
title: "Le mani libere"
date: 2016-06-20
comments: true
tags: [watch, Cio, iPhone, iPad, Newton]
---
Quando si vede un articolo con un accenno di originalità, nella povertà generale dei contenuti che si vede fuori da Facebook (al cui interno è peggio), pare di respirare una boccata di ossigeno.

Il titolo è [watch è solo un segnaposto](http://www.cio.com/article/3086049/smartwatches/apple-watch-is-just-a-placeholder.html) e la tesi è interessante. Perfino l’ambientazione, che suggerisce l’utilità di un apparecchio che consenta di tenere le mani libere. Ho sempre sostenuto che la ragione d’essere di watch è lasciare iPhone in tasca e il concetto non è dissimile.

Più suggestivo – e più interessante – è il tema di fondo (capire che watch ti lascia tenere iPhone in tasca è banalotto): watch attuale occupa lo spazio e svolge le prove generali in attesa di qualche suo successore che *veramente* si affermerà come piattaforma. Perché, come Apple ha mostrato in modo mirabile con iPhone e iPad, la genialità consiste non solo nel concepire l’apparecchio, ma anche nel lanciarlo quando le tecnologie e le tecniche necessarie si trovano esattamente nel momento giusto.

È il perché esistevano tavolette ben prima di iPad, per esempio, solo che erano troppo grosse e scomode per avere successo. La tecnologia non era ancora pronta. Lo stesso concetto potrbbe valere per Newton, assai in anticipo sui tempi e per questo di vita commerciale breve.

Il giusto watch sarà quello, conclude Cio, su cui contare in barca al largo ed è una bella idea. Più modestamente devo dire che in auto, o anche solo in cucina con le mani occupate, anche quello attuale non è malaccio.

*[L’amico Akko sta lanciando l’idea di un [iperlibro](https://macintelligence.org/posts/2016-06-15-un-kickstarter-per-cambiare-il-libro/) sul mondo Apple che superi i tomi tradizionali sui sistemi operativi per risultare davvero utile a chi lo legge e che resti aggiornato assieme alla tecnologia di cui parla. Sta [cercando popolarità e attraverso questa finanziamenti](https://www.kickstarter.com/projects/295108589/cuore-di-mela-i-segreti-di-apple-iphone-e-mac). Chi volesse contribuire in qualsiasi modo, farebbe cosa più che buona.]*