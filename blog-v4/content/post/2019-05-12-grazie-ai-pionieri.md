---
title: "Grazie ai pionieri"
date: 2019-05-12
comments: true
tags: [Apple, Mac, Winer, networking]
---
Interessante e centrato il [punto di vista di Dave Winer](http://scripting.com/2019/05/11.html#):

>In 1985, Apple fece qualcosa di audace e ambizioso: incluse la capacità di collegarsi in rete in tutti i Mac che costruivano. Significa che, se le interfacce di programmazione (API) fossero state facili da adottare per gli sviluppatori, su Mac sarebbe arrivata una grande ondata di applicazioni di networking solo per Mac. Non ho dubbi sul fatto che il web sarebbe partito su Mac, magari anni prima che accadesse su NeXT. Ma le API di networking erano impenetrabili, per cui solo pochi sviluppatori […] riuscirono a usarle.

È un bel riassunto del senso di Apple nel mondo tecnologico e anche dei suoi difetti. Apple ha mostrato al mondo che tutti i computer dovevano avere una scheda di rete. Il mondo ha brontolato, criticato, stravolto il messaggio e – con anni di ritardo – lo ha adottato.

È successo in tante altre occasioni. Apple ha mostrato ovviamente a tutti che serviva una interfaccia grafica su uno schermo bitmap e poi che serviva Usb (nessuno se lo filava, prima di iMac), ma anche milioni di colori, uno schermo sensibile al tocco sul telefono, l’elenco sarebbe lungo. Il meglio di Apple è quando mostra la strada al mondo, usualmente un mondo miope (ricordi quando sembrava imprescindibile un lettore Blu-ray sui portatili? E prima ancora un floppy disk?).

Il difetto di Apple è che a volte dimentica la propria stessa natura. Se sei pioniere, ma rendi il tuo lavoro incomprensibile e inaccessibile, quel lavoro si perde.

Pionere resti sempre, però.
