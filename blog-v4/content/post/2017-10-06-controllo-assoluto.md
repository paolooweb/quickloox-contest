---
title: "Controllo assoluto"
date: 2017-10-06
comments: true
tags: [Apple, Intel, Cpu, Dediu, Asymco]
---
Ogni tanto mi fermo a considerare i motivi per i quali seguire Apple è interessante e diverso dal seguire altre aziende.

Uno di quelli più solidi è l’ossessione di Apple verso la padronanza di ogni componente riguardante la propria attività. L’azienda lavora il proprio hardware, il proprio software e ultimamente, con l’inaugurazione di Apple Park, ha tenuto a battesimo lo Steve Jobs Theatre; in pratica ha acquisito il controllo persino dello spazio per le presentazioni.

E da alcuni anni si è dedicata anche ai processori, quelli che stanno dentro i prodotti iOS, tvOS, watchOS.

Se Apple arrivasse in fondo, se riuscisse a dominare *ogni* fase del processo produttivo, si sarebbe creata una situazione profondamente interessante e anomala. Nessuna azienda è mai andata neppure vicina a realizzare qualcosa del genere, perlomeno su queste dimensioni e con questa complessità.

Nel frattempo, lo capisco solo ora e lo trovo pazzesco se ripenso al passato, Apple [vende più microprocessori di Intel](http://www.asymco.com/2017/10/02/silicon-valley/). Microprocessori che fanno mangiare la polvere alla concorrenza diretta. E che si vendono certamente per i loro meriti, ma anche perché compaiono in macchine le quali, per la loro integrazione tra hardware e software nonché per il controllo mantenuto da Apple sulla loro realizzazione, si fanno apprezzare sul mercato.

Più parti del processo produttivo sono in mano ad Apple, più i prodotti hanno chance di risultare superiori rispetto alla concorrenza. C’è un circolo virtuoso di sinergie che si autorafforza. Chiedersi fin dove il gioco possa funzionare è affascinante.