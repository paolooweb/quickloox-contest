---
title: "Il transmogrificatore"
date: 2021-08-19T01:49:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [curricolo, scuola, BBEdit, JEdit, Atom, Pandoc, Markdown, Etherpad] 
---
Al momento di [insegnare a maneggiare testo e ipertesto con strumenti digitali](https://macintelligence.org/posts/Un-curricolo-per-lestate.html), la scuola dovrebbe fare lavorare gli studenti con il testo puro e presentare loro le nozioni fondamentali sugli strumenti di marcatura. Il formato standard dei documenti scolastici dovrebbe essere HTML o al limite testo puro trattato con [Markdown](https://daringfireball.net/projects/markdown/).

Per tutto il resto c’è [Pandoc](https://pandoc.org). È un sistema di conversione tra formati semplice da installare (ci sono riuscito anch’io), aperto alla personalizzazione, in continuo progresso. È software libero, installabile ovunque, che richiede potenza di elaborazione minima.

Da Pandoc escono, solo come esempio, file PDF oppure Word a partire da testo puro o testo marcato, in modo standard (Html) o pratico (Markdown). O viceversa, o altre combinazioni ancora. L’inglese ha una parola stupenda, [transmogrification](https://dictionary.cambridge.org/dictionary/english/transmogrification), per spiegare con spirito quello che fa Pandoc.

Consiglio la lettura dell’elenco di tutte le conversioni possibili, sulla pagina nome del suo sito. Forniranno anche un indizio prezioso per tutto il resto del lavoro sul nostro curricolo digitale per elementari e medie: il testo puro, con aggiunta di marcatura, serve a produrre molto più che documenti testuali.

La maggior parte delle persone non lo sa. Risparmierebbe tempo, soldi, fatica, sforzi di comprensione. Potrebbe lavorare con profitto maggiore su computer meno costosi. Dovrebbe impararlo a scuola e senza aspettare l’università, o rischierebbe nella vita di farsi dominare dagli strumenti, invece del viceversa.

*Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile.*