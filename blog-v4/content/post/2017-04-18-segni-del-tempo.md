---
title: "Segni del tempo"
date: 2017-04-18
comments: true
tags: [Raspberry, Pi, Mac, Lego, System]
---
Per capire che incredibile crescita abbia vissuto l’informatica personale e quanto siamo stati fortunati a viverla, si pensi che una pietra miliare del software di sistema come System 7 è diventato [liberamente disponibile su Internet Archive](https://archive.org/details/mac_MacOS_7.0.1_compilation) e praticabile attraverso un [emulatore realizzato in JavaScript](https://macintelligence.org/posts/2015-10-12-cosi-vicino-cosi-lontano/).<!--more-->

E l’hardware? Beh, la sintesi migliore è quella del [Mac in Lego realizzato da Jannis Hermanns](https://jann.is/lego-macintosh-classic/). È tutt’altro che il primo, ma è speciale perché lo schermo è in inchiostro elettronico e la scheda logica è in effetti un [Raspberry Pi Zero](https://www.raspberrypi.org/products/pi-zero/), costo [ventisei euro](https://www.amazon.it/Raspberry-Zero-Wireless-Official-Case/dp/B06XD18H6K/ref=sr_1_1?ie=UTF8&qid=1492442887&sr=8-1&keywords=raspberry+pi+zero), che attraverso [Docker](https://www.docker.com) virtualizza un Mac di un quarto di secolo fa.

Un giocattolo; fatto di tecnologia incredibilmente accessibile e altrettanto sofisticata. Un bell’omaggio a macchine che hanno fatto la storia e rappresentavano il vertice dell’informatica personale di quel tempo.

Chi lo rimpiangesse, quel tempo, legga tra le righe: Raspberry Pi costa come una sera in pizzeria e permette meraviglie. Mica per niente [ha superato Commodore 64 nelle vendite totali](https://www.raspberrypi.org/magpi/raspberry-pi-sales/). Segno che la nostalgia è bellissima e ora però è nuovamente tempo di imparare.