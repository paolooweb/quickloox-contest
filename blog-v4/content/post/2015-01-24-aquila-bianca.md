---
title: "Aquila bianca"
date: 2015-01-24
comments: true
tags: [Froese, TangerineDream, WhiteEagle]
---
Una notte di pausa a [ricordo di Edgar Froese](http://www.tangerinedream-music.com/index.php).<!--more-->

Durante il concerto dei Tangerine Dream a Milano nel maggio 2012 chiesi a un tecnico di poter fotografare la sua postazione di lavoro, piuttosto singolare. Me lo consentì.

Se Edgar è in viaggio in questo momento, lo immagino sulle note di [White Eagle](https://itunes.apple.com/it/album/white-eagle/id723574837?i=723574925&l=en).

 ![Tecnico dei Tangerine Dream a Milano](/images/tangerine-dream.jpg  "Tecnico dei Tangerine Dream a a Milano") 