---
title: "Voglio macOS Golden"
date: 2019-12-05
comments: true
tags: [macOS, Lovejoy]
---
Forse lo avevo già detto, però dirlo vicino a Natale magari ti porta un regalo inaspettato: caro [Tim](https://www.apple.com/it/leadership/tim-cook/), sono dispostissimo a pagare per [macOS](https://www.apple.com/macos/catalina/) Golden, una versione del sistema operativo che contiene zero virgola novità e sulla quale per un anno si è lavorato a nient’altro che risolvere bug, soprattutto quelli abbastanza fastidiosi da interferire nel lavoro e sufficientemente secondari da venire trascurati dai programmatori che ricevono altre priorità.

Ben Lovejoy [accenna a qualcosa su 9To5Mac](https://9to5mac.com/2019/11/28/small-but-annoying-bugs/) e presso di me sfonda una porta apertissima; anche se non si tratta nel mio caso di bug in senso stretto, aggiungerei una solenne revisione delle combinazioni di tastiera, in modo da aumentare la coerenza del sistema e dovunque possibile la stessa combinazione abbia lo stesso significato. La complessità del sistema rende del tutto impossibile raggiungere la perfezione su questo, tuttavia si può fare molto e si dovrebbe.

L’elenco dei malfunzionamenti molesti è lungo. AirDrop, tanto per dire, non è mai esattamente una certezza; nel Finder i posizionamenti delle finestre negli Spazi e su schermi multipli sono soggetti a imperfezioni evidenti. Non sono tanto decisioni di design che non piacciono, quanto parametri che qui e là saltano per cui tutte le finestre si aprono nel punto giusto tranne una e cose così. Lovejoy parla anche di questo, che poi è il motivo che mi fa usare gli Spazi poco e male. Il tempo necessario a tenerli organizzati dovrebbe essere inferiore a quello necessario per lavorare su una schermata sola con dentro tutte le finestre aperte e invece non sempre.

macOS Golden potrebbe uscire ogni tre anni lasciando spazio alle nuove versioni lavorate nel solito stile. Lascerebbe la scelta aperta tra chi predilige l’innovazione e chi la stabilità. Uno potrebbe anche tenersi due versioni del sistema su due dischi diversi e lanciare quella che preferisce al momento. Finirebbero i lamenti su come si stava meglio quando si stava peggio. Allo stadio di evoluzione in cui ci troviamo, le ultimissime funzioni che proprio è impossibile perdere sono poche e spesso non primarie.

Vediamo se Tim legge. Basterebbe anche [Craig](https://www.apple.com/it/leadership/craig-federighi/).