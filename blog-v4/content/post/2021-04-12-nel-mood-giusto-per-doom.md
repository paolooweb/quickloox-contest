---
title: "Nel mood giusto per Doom"
date: 2021-04-12T01:04:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [macOS, Flappy Birds, Doom, Notifiche, iOS, Gizmodo, Big Sur] 
---
Sì, beh, niente male approfittare del codice a disposizione degli sviluppatori per [inserire un’istanza di Flappy Birds dentro le Notifiche di macOS](https://gizmodo.com/flappy-bird-has-been-revived-as-an-interactive-macos-no-1846661997).

<blockquote class="twitter-tweet"><p lang="en" dir="ltr">Did you know you can put a whole game inside of a push notification <a href="https://t.co/LlMx2AjvHH">pic.twitter.com/LlMx2AjvHH</a></p>&mdash; Neil Sardesai (@neilsardesai) <a href="https://twitter.com/neilsardesai/status/1380649026186534913?ref_src=twsrc%5Etfw">April 9, 2021</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

Neil Sardesai, ingegnere iOS, ha già messo [Pong dentro un’icona del Dock](https://twitter.com/neilsardesai/status/1362890469970214917) – riferisce *Gizmodo* – e [*Dino Runner* nella barra dei menu](https://twitter.com/neilsardesai/status/1380294442733690880).

Tuttavia sono solo exploit fini a se stessi e lui lo sa benissimo, tanto che perfino nel pezzo gliene chiedono conto: lo standard è farcela con [Doom](https://en.wikipedia.org/wiki/Doom_(franchise)), che è arrivato persino [sul display di un test di gravidanza](https://www.popularmechanics.com/science/a33957256/this-programmer-figured-out-how-to-play-doom-on-a-pregnancy-test/).

Quando vedo *Doom* nelle Notifiche di Big Sur, ci provo di sicuro.

*Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile.*