---
title: "Il canto delle sirene"
date: 2021-09-03T02:10:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [D&D, McSweeney, Wendy Molyneux, Dungeons & Dragons, Roll20] 
---
Mi prendo una licenza per commentare un momento di vita privato. Stasera ci ritroviamo a giocare a D&D in presenza. Straordinariamente, _una tantum_, almeno per ora.

Possiamo farlo perché ci ritroviamo in un salotto con una parete completamente aperta verso l’esterno e il ricambio dell’aria è assicurato; la cubatura dell’ambiente è superiore alla media e così è più difficile saturarlo di aerosol prima che si completi il ricambio; uno dei partecipanti lavora nel servizio sanitario nazionale in posizione più che qualificata per valutare il rischio lucidamente e con competenza.

Oltre a tutto ciò, siamo vaccinati con la seconda dose tipo da un mese, quindi al massimo possibile della protezione.

Chi mi conosce sa bene come il sottoscritto sia a proprio agio con le connessioni remote e le piattaforme collaborative come [Roll20](https://roll20.net/welcome) (ludica) o [Miro](https://miro.com/) (professionale). Le amo soprattutto quando è possibile _sceglierle_; se sono l’unica possibilità, ne approfitto ma sento il desiderio di poter fare diversamente se volessi.

Poter passare una serata tra amici è in gran parte merito dei vaccini. Il governo ha sempre rinunciato a contrastare attivamente la pandemia, preferendo sbrigative chiusure dove avrebbe dovuto testare, tracciare, trattare e fare altre cose ancora. Con la strategia e la tattica giusta si sarebbero potuti raggiungere grandi risultati, che però non c’era la capacità di cogliere (quelli di prima) o la volontà di farlo (quelli di adesso).

Non c’è che vaccinarsi, dunque, a parte le precauzioni di routine.

Rispetto le posizioni di tutti ma, eccezionalmente, mi sbilancio e affido la mia opinione a Wendy Molyneux di _McSweeney’s_. Non è il caso di tradurne qui un estratto, meglio lasciarlo in originale. [Le sue parole](https://www.mcsweeneys.net/articles/oh-my-fucking-god-get-the-fucking-vaccine-already-you-fucking-fucks) difficilmente convinceranno tutti, eppure per me oggi per me possiedono una rilevanza particolare.

*Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile.*