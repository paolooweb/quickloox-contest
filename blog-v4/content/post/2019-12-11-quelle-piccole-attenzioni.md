---
title: "Quelle piccole attenzioni"
date: 2019-12-11
comments: true
tags: [BBEdit]
---
Ho scritto al supporto di [Bare Bones](https://www.barebones.com) perché da un paio di versioni BBEdit non mostra correttamente a video il logo Apple che si ottiene da tastiera con Opzione-Maiuscole-8.

Nel documento il carattere è corretto e, per esempio, si vede giusto nella pagina Html prodotta nel browser a partire da un file Html o Markdown. Ma nella finestra di BBEdit appare solo un punto interrogativo rovesciato.

Mi hanno risposto subito.

Mi hanno ringraziato per avere scritto *ancora* (ovvero hanno verificato che sono un cliente storico. Per la precisione, in vent’anni avrò scritto al supporto quattro volte, non quattrocento).

Mi hanno spiegato con precisione che si tratta dell’effetto indesiderato di una modifica e che la prossima versione di aggiornamento sistemerà le cose.

Mi hanno chiesto se desidero essere informato quando è disponibile una versione preliminare dell’aggiornamento di BBEdit che sistema le cose.

Mi hanno fatto gli auguri di Natale a nome di tutti.

Sono minuzie e di supporti tecnici gentili, tempestivi e competenti se ne trovano tanti.

Eppure l’esperienza complessiva rimane un gradino sopra al 99 percento del resto. Per piccoli dettagli, magari carinerie. Eppure fanno la differenza, sembra di parlare con conoscenti anche se li ho incrociati una sola volta nella vita ed era nello scorso secolo.

*It still doesn’t suck.*