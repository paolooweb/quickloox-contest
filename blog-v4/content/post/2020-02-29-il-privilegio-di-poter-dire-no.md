---
title: "Il privilegio di poter dire no"
date: 2020-02-29
comments: true
tags: [iPad, trackpad, MacBook, Pro, Surface, Microsoft]
---
Apprezzo l’idea di un eventuale tastiera per iPad con trackpad incorporato, che farà benissimo a tante persone con disabilità e risolverà numerosi casi particolari. Soprattutto consentirà a me di farne a meno.

Se accadesse l’inverso, ossia mettessero uno schermo touch in un MacBook Pro, mi arrabbierei. Mi sarebbe possibile evitarlo solo con il boicottaggio di MacBook Pro, cosa che oggi mi lascerebbe indifferente visto che in mobilità utilizzo iPad. Tuttavia una volta usavo Mac e vorrei restare libero di cambiare ancora idea in futuro.

Condivido una serie di [considerazioni di Birchtree](https://birchtree.me/blog/the-vision-of-the-microsoft-surface-and-the-vision-of-apples-user-base/): un iPad del 2020 con tastiera e trackpad somiglierebbe molto a un Surface del 2012. Le differenze sono due: la prima è che Apple continua a preservare l’unicità di iPad come apparecchio intermedio tra computer da tasca e computer da scrivania, anche se permette di avere un assetto da portatile. L’approccio Microsoft fu diverso: doveva essere un portatile che, al limite, diventava una tavoletta.

La seconda differenza sta nella base di utenza e qui Birchtree ha visto lungo. Microsoft ha tentato con Surface di portare idee nuove nel mondo Windows ed è stata respinta dal suo pubblico, il più conservatore che esista quando si tratta di software. L’elasticità mentale e la capacità di adattarsi al nuovo sono nel DNA della base di utenza Apple.

Mica per niente vari amici che sono transitati negli anni al mondo Windows hanno detto che vedevano più innovazione di là, quando in realtà era solo più facile per loro preservare modalità di utilizzo sulle quali, per età loro o inadeguatezza tecnica degli sviluppatori, avevano totalmente perso elasticità.

Di più: Steve, quello che quando c’era lui c’era l’innovazione e adesso invece no, diceva che le idee fioccavano e il suo compito, il compito di Apple era saper dire no a tutto il non essenziale.

Benissimo che iPad si avvicini a essere un portatile per chi ne ha bisogno. Per tutti quelli con il privilegio di poter scegliere e poter dire no, che rimanga per favore un apparecchio diverso, con una sua identità, che peraltro adoro. Per avere una cosa simile a Mac, scelgo Mac. Gli apparecchi senza identità, che cercano di essere tutto per tutti e si vendono a peso della lista degli ingredienti, li fanno già altri, con tanta scelta, prezzi bassi e ricambio continuo.
