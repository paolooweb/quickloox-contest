---
title: "Con tanti auguri"
date: 2016-03-14
comments: true
tags: [Monopoli, cifratura, Apple, Fbi, Microsoft, Green]
---
Trovo sempre più bizzarro che nella [diatriba tra Apple e Fbi](https://macintelligence.org/posts/2016-02-24-provaci-ancora-zio-sam/) si possa parteggiare per l’Fbi, una volta capito che cosa è veramente in gioco.<!--more-->

La ragione, temo, ha a che vedere con il contenuto di un articolo intitolato [Acquistato di recente un computer Windows? Probabilmente Microsoft possiede la tua chiave di cifratura](https://theintercept.com/2015/12/28/recently-bought-a-windows-computer-microsoft-probably-has-your-encryption-key/).

Immagino che qualcuno lo troverà normale e capisco. E rabbrividisco. Un estratto:

>“Lo standard di qualità nella cifratura del disco è la procedura end-to-end, dove l’unico a potere sbloccare il disco è il suo proprietario” spiega Matthew Green, professore di crittografia presso la John Hopkins University. “Esistono sicuramente casi dove è utile disporre di un backup della propria chiave di sblocco o password. In questi casi è ragionevole rivolgersi a un’azienda perché lo conservi. Ma lasciare le proprie chiavi a un’azienda come Microsoft cambia le proprietà di sicurezza di un sistema di cifratura del disco […] “Il vostro computer è al sicuro tanto quanto lo è il database di chiavi detenuto da Microsoft, che può essere vulnerabile agli hacker, a governi stranieri e a persone nella posizione di poter corrompere o raggirare dipendenti Microsoft”.

Probabilmente Microsoft rende i propri utenti vulnerabili a un imprevisto. Il che, in una visione di efficacia della cifratura, equivale a tre passi indietro. Come l’imprevisto di [Monopoli](http://www.webpratico.it/27-svago/30-il-monopoli.html): con tanti auguri.