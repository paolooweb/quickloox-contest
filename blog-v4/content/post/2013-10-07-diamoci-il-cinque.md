---
title: "Diamoci il cinque"
date: 2013-10-07
comments: true
tags: [Apple]
---
Grandioso promemoria di MG Siegler sulle [regole del *blogging* tecnologico](https://medium.com/tech-blogging/43d78a82e103) e su quella principale, che lui chiama 75-20-5.<!--more-->

>Ogni giorno, il 75 percento di quello che si legge di tecnologia è all’incirca accurato, il 20 percento è spazzatura totale e il cinque percento è vero.

Il problema dell’equilibrio tra velocità e precisione è ovviamente sempre centrale. Se si legge di una cosa mentre succede, è un conto. Già cinque minuti dopo comincia a somigliare al telefono senza fili. Le cose scritte un’ora dopo sono in genere pessime. E sono la maggioranza, dove invece una analisi ragionata richiederebbe più tempo. Va a finire che:

>Stiamo addentrandoci troppo nel mondo delle mezze verità e del pattume istantaneo. Tutto quello che appare, non importa quanto impreciso, viene diffuso come vangelo. La frenesia e l’esagerazione hanno vinto, la precisione e i dettagli sono in fin di vita.

L’invito finale di Siegler è piuttosto preciso:

>Non scrivere qualcosa perché lo puoi fare. Scrivilo perché dovresti. O evita.

Nel mio piccolissimo, qui si cerca di restare costantemente dentro il cinque percento. Promessa.