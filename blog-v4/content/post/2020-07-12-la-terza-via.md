---
title: "La terza via"
date: 2020-07-12
comments: true
tags: [AltStore, Clip, iOS, jailbreak, Delta, Testut]
---
Per installare una app su iOS, la si scarica da App Store.

C’è anche un secondo sistema: quando le aziende vogliono impiantare programmi propri sugli iPhone aziendali senza dover passare da Apple Store, installano sugli apparecchi un certificato che lo consente. Fuori dalle aziende, il sistema presenta grossi rischi di sicurezza ed è totalmente sconsigliato, con l’eccezione di [Fontcase](https://macintelligence.org/blog/2020/06/29/teoria-e-pratica-del-codice-aperto/).

C’è un terzo sistema. Siccome gli sviluppatori vogliono poter collaudare le loro app sui propri apparecchi prima di mandarle ad Apple Store, le possono installare tramite [Xcode](https://developer.apple.com/xcode/), il sistema di sviluppo di Apple per Mac.

Le limitazioni sono ottime e abbondanti. Occorre essere sviluppatori paganti, le app installate non possono essere più di tre, l’installazione scade dopo una settimana.

Uno sviluppatore, [Riley Testut](http://rileytestut.com), lavora da anni a un suo emulatore di videogiochi per iOS, che allo stato attuale delle regole non può essere approvato per la pubblicazione su Apple Store. Ha poi avuto un'idea per un programma di clipboard multiple che in sé non darebbe niente da eccepire in quanto tale, se non fosse che – come un qualunque buon programma di questo tipo – vuole restare il più possibile in funzione in background.

A un certo punto Testut ha deciso che non voleva scendere a compromessi e ha studiato un modo per consentire l'installazione di app via Xcode *a tutti*. *Senza jailbreak*. [AltStore](https://altstore.io) è quasi un atto di follia e, contemporaneamente, un passaggio veramente notevole del progresso dello sviluppo software per iOS.

Testut ha trovato modi per aggirare tutte le limitazioni descritte qui sopra. A [leggerle](http://rileytestut.com/blog/2019/09/25/introducing-altstore/) si ha un’idea della fatica e del talento che ha dovuto mettere in campo. Per esempio, ha creato un server per Mac e Windows che installa AltStore e le sue app su un iPhone, grazie al fatto che, se iTunes può sincronizzare via Wi-Fi, il metodo può essere usato anche da sviluppatori non paganti (e da non sviluppatori). Anzi, il sistema permette di distribuire qualsiasi software.

Trucchi vari di programmazione fanno credere al nostro iPhone che quelle app siano state scritte da noi e quindi, legittimamente, si accorda con Xcode per installarle. Un altro trucco fa pensare a iPhone in sede di controllo che non vi siano app installate via Xcode, per cui non c’è problema a installarne una. Una in più; anche il limite delle tre app è stato superato.

Ancora, il marchingegno aggiorna ogni settimana le app installate e impedisce così che scadano.

I modi in cui un cambiamento tecnico o procedurale lato Apple può stroncare le premesse di funzionamento di AltStore sono innumerevoli. Tuttavia, il meccanismo di Testut rispetta le regole di Apple stessa, che quindi non può contestarlo. AltStore funziona da fine settembre scorso e un mese fa Testut ha presentato [Clip](http://rileytestut.com/blog/2020/06/17/introducing-clip/), il suo amministratore di clipboard, che viene proprio distribuito tramite AltStore.

Durerà? È quasi arte, per come ha saputo raggiungere un obiettivo nonostante vincoli che hanno scoraggiato centinaia di migliaia di altri sviluppatori. È un meccanismo ingegnoso e fragile; pone comunque problemi di sicurezza; le app di Testut sono open source e verificabili da chiunque, ma domani – con altre app – potrebbe andare peggio; Apple potrebbe decidere di cambiare regole per gli sviluppatori, o rendere inviolabile la sincronizzazione Wi-Fi di iTunes, o altro ancora. Potrebbe perfino decidere di consentire ufficialmente qualche meccanica di installazione parallela ad Apple Store. Insomma, la scommessa su AltStore è impegnativa.

Al tempo stesso, se avessi un soldino per tutti quelli che ho sentito rievocare i tempi felici in cui uno poteva smanettare sulle macchine aperte mentre invece oggi le macchine sono chiuse, sarei ultraricco. Si può smanettare eccome e si può anche installare software, ohibò, non autorizzato da Apple, senza jailbreak. Testut è uno studente universitario, non un milionario annoiato o un asociale seduto venti ore al giorno davanti al computer nella sua cameretta. È anche un esempio di pensiero laterale e creatività.