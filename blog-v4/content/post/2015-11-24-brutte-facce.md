---
title: "Brutte facce"
date: 2015-11-24
comments: true
tags: [FontDeck, font]
---
Purtroppo FontDeck [chiude i battenti](http://blog.fontdeck.com/post/133794978966/why-fontdeck-is-retiring).<!--more-->

La buona notizia è che la decisione sembra presa in base all’impossibilità di investire per restare al livello di servizio richiesto dal mercato e che la concorrenza mira a mantenere, non a una difficoltà intrinseca di mercato.

La notizia meno buona è che, per avere buona tipografia su web, i fornitori di font web sono essenziali ed è vitale che il modello di business consenta l’attività di più parti in gioco, per avere concorrenza e quindi miglioramento dell’offerta eccetera eccetera.

A me dispiace comunque, perché è matematico che parte dei clienti di FontDeck smetteranno di usare font web invece di adottare qualche altro fornitore. Anche se non ne sono sicurissimo, forse avrei preferito qualche sito con font non perfetti, anche *ugly faces*, invece che senza font del tutto.

La maggioranza di chi mi legge scrollerà le spalle, probabilmente ignara persino dell’esistenza di FontDeck. Io stesso non ne faccio uso. Siamo parte del problema generale, infatti. Il web è cosa importante per lasciarlo ad Arial, Verdana, Comic Sans e altri volti anonimi e anonimizzanti della tipografia.