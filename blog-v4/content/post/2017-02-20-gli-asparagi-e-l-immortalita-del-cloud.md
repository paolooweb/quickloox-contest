---
title: "Gli asparagi e l’immortalità del cloud"
date: 2017-02-20
comments: true
tags: [Amazon, Backblaze]
---
[Supporti](https://macintelligence.org/posts/2017-02-17-gli-asparagi-e-l-immortalita-dei-supporti/), [formati](http://macintelligence.org), [dati]((https://macintelligence.org/posts/2017-02-19-gli-asparagi-e-l-immortalita-dei-dati/) in generale. Ahimé, niente è per sempre. Tocca occuparsene regolarmente, con cura e dedizione. Vedo gente lavare l’auto tutti i sabati che sembra uscita da una sala operatoria. Poi si siede davanti alla tastiera e niente, lì deve vigere l’autoconservazione: creo un bit, adesso fatti vostri e guai al mondo se sparisce.

Spiegato perché si tratti di una pia illusione, giusto una curiosità finale: oggi 20 febbraio 2017, il sistema più garantista sulla conservazione dei dati è il cloud. Sì, quella nuvola virtuale di cui non abbiamo il controllo, dotata di vita propria, cui possiamo accedere per leggere e scrivere dati, e nulla più (il sarcastico dice: per questo i dati sono così affidabili…). Neanche sappiamo dove veramente stanno, in che server, sotto che software, niente di niente (se lo sappiamo, il giorno dopo può essere cambiato tutto).

Scherzi a parte, il servizio S3 di Amazon offre, al meglio delle sue possibilità, il [99,999999999 percento di durata](https://aws.amazon.com/s3/faqs/) dei dati sull’anno e il 99,99 percento di reperibilità del dato.

>per esempio, se archivi diecimila oggetti su Amazon S3, puoi aspettarti in media di perdere un oggetto ogni dieci milioni di anni.

La questione dei decimali non è uno scherzetto: Backblaze, per dire, ne offre [tre in meno](https://help.backblaze.com/hc/en-us/articles/218485257-B2-Resiliency-Durability-and-Availability). Vuol dire che, a circostanze identiche, perderebbe un oggetto ogni “soli” diecimila anni.

La cosa curiosa è che la gran parte di quelli ossessionati dall’immortalità dei dati non lo usano. Perché ha un costo a canone. L’immortalità dei dati dovrebbe essere gratis.

In realtà si può spendere meno, selezionando i dati meno importanti, che è possibile scegliere di fare custodire in maniera meno sicura: le informazioni più sacrificabili. Si può spendere ancora meno impegnandosi con Amazon ad archiviare i dati a volontà, ma andandoli a riprendere con la minore frequenza possibile: è il principio di [Glacier](https://aws.amazon.com/it/glacier/).
 
Un bel paradosso: il sistema migliore per conservare i dati a lungo è usare sistemi di cui non abbiamo il controllo e, per giunta, composti da hardware di qualità assolutamente discutibile per ogni pezzo singolo. Nel cloud i dischi sono come il filo interdentale in bagno: usi, si spezza, lo getti, ne prendi un altro senza remore.

I sistemi più fragili e di minore qualità, organizzati con sapienza, creano le soluzioni di archiviazione più robuste (ti parleranno di Blu-ray: col cavolo che arrivi ai nove decimali, e parliamo di costi).

Da rifletterci. E pensare che c’è ancora gente in giro che ti consiglia, che so, Hitachi, perché ne ha comprato uno al cugggino e si è trovato bene.