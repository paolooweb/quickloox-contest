---
title: "Sembra ieri"
date: 2016-07-16
comments: true
tags: [Spiderweb, Avadon]
---
Ho avuto accesso alla *beta* di *Avadon 3: The Warborn* e per accordo di riservatezza posso dire unicamente che esiste.<!--more-->

Allora, per mantenere viva l’attenzione, ricordo una [intervista carina](http://venturebeat.com/2015/02/17/the-original-indie-dev-how-one-man-made-22-games-in-22-years-mostly-from-his-basement/view-all/) concessa dall’autore, Jeff Vogel, a *VentureBeat* poco più di un anno fa.

Vogel ha prodotto da indipendente ventidue giochi di ruolo in ventidue anni, uno più bello dell’altro, e ha saputo rinnovare la sua formula sempre restando fedele a se stesso. L’anno scorso si parlava di [Avadon 2: The Corruption](http://www.spiderwebsoftware.com/avadon2/), teso, intrigante e soprattutto enorme. Arrivare in fondo è un bell’impegno e il finale è aperto, secondo le scelte compiute durante il gioco. *Avadon 3: The Warborn* è molto più… ma lo potrò dire solo tra un po’.