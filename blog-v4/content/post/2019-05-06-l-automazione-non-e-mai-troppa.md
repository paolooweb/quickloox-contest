---
title: "L’automazione non è mai troppa"
date: 2019-05-06
comments: true
tags: [macOS, iOS, Marzipan, Shortcuts, Automator, AppleScript, WWDC]
---
Manca meno di un mese alla [Worldwide Developer Conference](https://developer.apple.com/wwdc19/ ) e penso che gli occhi vadano puntati soprattutto sul futuro dell’automazione per Mac e iOS.

La ragione [l’ha spiegata molto bene](https://sixcolors.com/post/2019/05/are-we-headed-for-a-mac-automation-schism/) Jason Snell su *Six Colors*: sta arrivando il cosiddetto Marzipan, un sottosistema software che permetterà agli sviluppatori di portare agevolmente app iOS su macOS e in via definitiva di produrre applicazioni che in un unico *package* contengono il codice necessario per funzionare su qualsiasi apparecchio Apple.

Questo significa che arriveranno su Mac anche i Comandi rapidi e magari la possibilità di lanciarli tramite Siri. Come si interfacceranno con Automator e AppleScript è argomento intrigante perché le soluzioni possono essere più di una e non c’è dubbio che, anche nelle ipotesi più catastrofiche, qualche forma di integrazione, se non ci sarà, verrà elaborata dagli sviluppatori. L’automazione su iOS si è sviluppata abbondamentemente malgrado Apple, che ha preso atto a cose fatte anche se per fortuna sembra stare facendolo con intelligenza.

Personalmente non vedo l’ora di vedere automazione globale per macOS e iOS. Da quando ho preso iPad Pro, più o meno ci passo sopra la metà del tempo-macchina, che per l’altra metà va su Mac. Sistemi per comandare rapidamente una macchina dall’altra, in ambedue le direzioni, mi sembrano conclusione logica e necessaria.

Bellissimo sarebbe l’assenza di soluzioni chiare, unite all’arrivo dei Comandi rapidi su macOS. Scommetto che nascerebbero darwinianamente metodi assai interessanti. Continuo a reiterarlo: il nostro uso futuro delle macchine deve arrivare ad automatizzare tutto quello che è automatizzabile. Che è l’unico modo per non venire automatizzati.
