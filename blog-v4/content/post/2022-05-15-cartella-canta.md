---
title: "Cartella canta"
date: 2022-05-15T12:41:21+01:00
draft: false
toc: false
comments: true
categories: [Hardware]
tags: [iPad, Mac, Files, Finder, Recenti, iPadOS]
---
Mi sono ritrovato in uno di quei thread surreali dove uno parte affermando *non c’è una sola cosa che iPad faccia meglio di Mac*. Riferito oltretutto principalmente a un MacBook.

Bisognerebbe trattenersi dal rispondere se non fosse uno di quei casi che rendono la reazione inarrestabile. Perché anche la persona più parziale del mondo dovrebbe semplicemente guardare un iPad, vedere che può ruotare lo schermo, constatare la presenza di una fotovideocamera dorsale, prendere atto della possibilità di uso sia con la tastiera sia senza tastiera… insomma, è *fisicamente* evidente che almeno *qualcosa* iPad la faccia per forza meglio di Mac. Se non altro, guardare una foto scattata in verticale, o girare un video e inserirlo in un documento.

Invece siamo alla negazione dell’evidenza come caratteristica storica della nostra epoca. È possibile, accettato, condiviso che la realtà sia equiparata alla fantasia personale.

Poi Mac fa meglio di iPad diverse cose, anche molte. Personalmente ho deciso che preferisco Mac quando sono alla scrivania e iPad quando mi muovo. Qui sì che siamo nel dominio dei gusti e quindi è del tutto lecito sostenere il contrario o fare infiniti distinguo.

Una cosa che trovo strana è l’osservazione, molto comune, su iPadOS che sarebbe inadeguato. La stranezza è il metro di misura; per qualche misteriosa ragione, iPad dovrebbe replicare il set di strumenti che offre Mac per risolvere determinati problemi, perché sempre per un motivo a me incomprensibile la soluzione dovrebbe essere sempre uguale, dovunque.

Tipo, il file manager. Le cartelle, le sottocartelle, le raccolte manuali svolte con dedizione simile a quella della collezione di francobolli. Su sistemi che effettivamente avevano migliaia di file trent’anni fa o quaranta e incoraggiavano i metodi da collezionismo. Oggi i sistemi hanno milioni di file, sono di innumerevoli tipi diversi, è un approccio come minimo datato, a cui su Mac ho del tutto rinunciato: tag e cartelle smart e che ci pensi il computer, a fare il lavoro del computer. Trovassi una persona che le sue diecimila foto sul computer le ha classificate e suddivise. I suoi diecimila file, invece devono stare nelle cartelline o il mondo smette di ruotare.

Ecco, sì, un cosa che mi dà fastidio quando lavoro su iPad e che funziona molto meglio su Mac: la ricerca è infinitamente meno efficiente e questo, sì, andrebbe assolutamente risolto.

La consapevolezza di questo difetto, comunque, mi rende più attento a un fenomeno che su Mac ho impiegato più tempo a padroneggiare mentalmente: credo che otto volte su dieci, se non di più, su iPad io apra File e alla voce Recenti trovi il file che mi serviva.

Da tempo, il default per una nuova finestra del Finder sul mio Mac è l’apertura di Recenti. Il file che mi serve è quasi sempre lì. Siamo sicuri che un file manager sia necessario al livello in cui era necessario nel 1992? Perché la mia esperienza è che serva molto meno, a volte proprio non serva più. La mia esperienza è quella del singolo, conta zero nello schema delle cose. Però misuro la mia capacità di lavorare e di arrivare sveltamente ai risultati che desidero, perché lavoro e il mio tempo di lavoro equivale a denaro. Questo non ha niente di personale è di individualista. Il file che mi serve è nella cartella Recenti quasi sempre e questo mi fa *risparmiare* tempo, quindi denaro. In che modo le cartelle e le sottocartelle sarebbero più veloci?