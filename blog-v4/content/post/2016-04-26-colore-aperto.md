---
title: "Colore aperto"
date: 2016-04-26
comments: true
tags: [Adobe, Gimp, Scribus, iPad, colore, Unix]
---
A seguito dell’articolo di ieri sulle [qualità del colore come viene amministrato dai nuovi iPad Pro](https://macintelligence.org/posts/2016-04-25-la-visione-del-futuro-da-vicino/), **Fëarandil** ha formulato un commento molto interessante che mi permetto di citare.<!--more-->

>Guarda che è anche il motivo principale per cui GIMP e le altre soluzioni open source non possono sostituire Adobe.

Ho provato a capire meglio perché e sono arrivato a una [pagina del supporto di Gimp](https://docs.gimp.org/it/gimp-imaging-color-management.html) che forse chiarisce di più la situazione rispetto all’*open source*.

La spiegazione è che [Gimp](http://www.gimp.org) e gli altri ([Scribus](https://www.scribus.net), per dire) sono graditi ospiti di OS X, ma come tutti gli ospiti non hanno le chiavi della sala motori né la conoscenza della rete idraulica o elettrica del locale.

Sono programmi scritti in modo da funzionare ovunque – Linux, Unix, Windows, OS X eccetera – tramite una compilazione che sarà specifica per ogni piattaforma, ma deve presentare il minimo di complessità e fatica al programmatore. Sono pertanto assenti tutti gli agganci alle tecnologie interne di OS X che permettono la gestione del colore e, non presenti altrove, vengono ignorate al momento di scrivere il programma.

La pagina del supporto di Gimp spiega come si dovrebbe procedere: sostanzialmente trasformando Mac in un sistema Unix generico, tramite l’installazione dei modelli Unix di gestione del colore, che esistono e suppongo siano anche efficaci. Però Gimp da solo non ce la fa e si limita a risiedere sulle piattaforme, senza aprire la sala motori e infilare le mani tra gli ingranaggi.

Questo non pone alcun problema per chi ridimensiona una immagine per il proprio blog; in compenso, obbliga i professionisti della grafica a dipendere appunto da Adobe.

Se sbaglio, correggetemi.