---
title: "Obiettivo Guatemala"
date: 2017-11-08
comments: true
tags: [iPhone, X, Mann, Panzarino, Disneyland, Guatemala, Dx0Mark, Pixel]
---
Austin Mann ha l’abitudine di collaudare la fotocamera dei nuovi iPhone in viaggio. Non è l’unico; Matthew Panzarino [ha portato la famiglia a Disneyland](https://techcrunch.com/2017/10/31/review-the-iphone-x-goes-to-disneyland/) per provare iPhone X e riferirne su *Techcrunch*.

Tuttavia Mann, che già era passato dal [Ruanda](https://macintelligence.org/posts/2016-09-23-a-tutto-schermo/), non è uno che si accontenta e, per mettere alla prova iPhone X, [si è recato in Guatemala](http://austinmann.com/trek/iphone-x-camera-review-guatemala).

Le foto e i video del resoconto sono tutti da guardare e la dicono lunga sulle qualità della fotocamera di iPhone X.

Che neanche potrebbe essere la migliore in assoluto: perde di un punto nel punteggio globale, 97 a 98, contro Pixel 2 di Google, [secondo DxOMark](https://www.dxomark.com/apple-iphone-x-top-performer-stills/).

Per chiunque non sia Mann, è un buon accontentarsi.