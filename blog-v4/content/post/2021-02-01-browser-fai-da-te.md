---
title: "Browser fai-da-te"
date: 2021-02-01T01:46:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [Flash, Sudafrica, Sars, South African Revenue Service] 
---
Se la storia della ferrovia cinese che funziona grazie a Flash e [viene fatta funzionare ancora grazie a una copia pirata di Flash](https://macintelligence.org/posts/E-hanno-troppo-rispetto-per-lo-status-quo.html) sembra un record insuperabile, ecco, citerei Lovecraft: [col volgere di strani eoni anche la morte può morire](https://it.wikipedia.org/wiki/La_città_senza_nome_(racconto)). Figuriamoci se l’imbecillità Flash-indotta ha un limite.

Difatti, mi si ingrippano le dita a scriverlo, il [South African Revenue Service](https://www.sars.gov.za/Pages/default.aspx) (Sars, neanche beneaugurante) raccoglie le dichiarazioni dei redditi dei cittadini sudafricani grazie a Flash.

Avendo avuto solo qualche anno di preavviso, il servizio si è trovato impreparato. Però ha rapidamente messo a punto una soluzione. Rullo di tamburi…

[Un proprio browser, fatto su misura per la raccolta delle le dichiarazioni dei redditi via Flash](https://twitter.com/sarstax/status/1353699405912797184).

<blockquote class="twitter-tweet"><p lang="en" dir="ltr">We are pleased to announce that a SARS browser solution is now available following issues experienced with the discontinuation of Adobe Flash Player.<br><br>Thread:</p>&mdash; SA Revenue Service (@sarstax) <a href="https://twitter.com/sarstax/status/1353699405912797184?ref_src=twsrc%5Etfw">January 25, 2021</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

Quando qualcuno si lamenta con Apple perché ha una app che è rimasta ai trentadue bit, bisogna rispondere di chiedere agli autori del programma l’ovvia soluzione: producano un sistema operativo *ad hoc* in grado di far partire la app. Che diamine.

*Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile.*