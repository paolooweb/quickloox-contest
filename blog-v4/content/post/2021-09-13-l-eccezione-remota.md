---
title: "L’eccezione remota"
date: 2021-09-13T01:40:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [Erin Casali, Folletto, Intense Minimalism, remote working] 
---
Una regola che vige qui è parlare di cose che, più o meno approfonditamente, si sono già lette.

Poi ci sono le eccezioni. Oggi tocca a [The Three Speeds of Collaboration: Tool Selection and Culture Fit](https://intenseminimalism.com/2015/the-three-speeds-of-collaboration-tool-selection-and-culture-fit/).

Non l’ho ancora letto, ma mi tocca. Sono quattordici minuti che, a volo d’uccello, costituiscono l’approccio più brillante e profondo che abbia visto finora alla collaborazione remota.

Ovvero, è una pagina che rafforza la spina dorsale di numerosi argomenti che ho all’ordine del giorno al ritorno dalle vacanze.

C’è anche di mezzo un [libro](https://www.amazon.it/How-Communicate-Effectively-Remote-Team-ebook/dp/B00PW4GOJK/ref=sr_1_1), tra poco cominceranno le pressioni a spendere da Black Friday a Natale, si comincia a compilare la lista dei desideri, che male c’è a essere in anticipo? Quattro euro e ottantasette centesimi per fare un passo avanti si possono contemplare (grazie a [kOoLiNuS](https://koolinus.net/blog/) per la segnalazione della disponibilità su Amazon italiano).

*Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile.*