---
title: "Come un disco rotto"
date: 2021-02-13T00:36:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [Backblaze] 
---
Di gran valore il rapporto annuale di Backblaze sull’affidabilità dei dischi rigidi dentro il suo datacenter. Rapporto *annuale*, non trimestrale.

Di gran valore perché il dato annuale del 2020 viene confrontato con 2019 e 2018 e, per esempio, si vede che l’affidabilità totale dei dischi è di molto migliorata. Nei commenti si cerca di capire quali possano essere le ragioni e non è semplice.

Nei dati si nota un modello Seagate da diciotto terabyte che vanta, si fa per dire, un tasso di guasto annuale un ordine di grandezza sopra tutti gli altri. Il suo intervallo di confidenza è amplissimo, il che significa inaffidabilità del suo tasso di guasto. Il disco in questione ha totalizzato più di cinquemila giorni/disco di funzionamento. Ma il tasso di guasto è inaffidabile. Non dico altro, per non fare io il disco rotto.

In totale la base installata supera i centosessantamila dischi rigidi, per oltre cinquantuno milioni di giorni/disco. Come sarebbe bello avere questo livello di trasparenza, e questo campione, per un sacco di altre grandezze.

*Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile.*