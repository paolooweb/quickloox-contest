---
title: "Sublime ignoranza"
date: 2023-03-04T23:12:47+01:00
draft: false
toc: false
comments: true
categories: [Internet, Software]
tags: [Hofstadter, Douglas Hofstadter, Fluid Concepts and Creative Analogies, Concetti fluidi e analogie creative]
---
Come ripromesso, ho dato un po’ di spazio alla riscoperta di [Fluid Concepts and Creative Analogies](https://macintelligence.org/posts/2023-01-09-intelligenza-cercasi/). Pagina 35, al paragrafo Intelligenza generale contro conoscenza esperta:

>Una cosa che mi disturbava era comprendere che avevo fatto del mio meglio per infilare nel mio programmino quanta più sofisticazione matematica potevo mentre, riflettendoci, la sofisticazione matematica non mi interessava, al contrario dell’**intelligenza** del programma. Se a quei tempi fosse esistito il termine, avrei potuto affermare di essere stato risucchiato nella trappola dei **sistemi esperti**: l’idea che la chiave dell’intera intelligenza sia semplicemente conoscenza, conoscenza e ancora più conoscenza. Un’idea che, francamente, trovavo repellente.

Si legge da tutte le parti che il modello attuale ha certamente le sue limitazioni, ma presto arriverà il modello più grande, che risolverà molti problemi. E poi il modello più grande ancora. Con il sottotesto che, misteriosamente, dal modello sempre più grande a un certo punto emergerà l’autocoscienza, così, da sola, senza sapere bene neanche lei perché.

Più sappiamo, più sappiamo di non sapere. È acquisito dai tempi di Socrate. Eppure qualcuno è disposto a credere all’idea di un meccanismo che dovrebbe risolvere tutto con un sapere sempre più pervasivo, e a chiamarlo pure intelligenza.

(Note: la traduzione di Hofstadter è mia e non necessariamente corrisponde a quella ufficiale. *Concetti fluidi e analogie creative* è uscito in originale nel 1995 e parla di ricerche compiute grosso modo nei quindici anni precedenti.)