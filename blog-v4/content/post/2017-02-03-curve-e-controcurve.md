---
title: "Curve e controcurve"
date: 2017-02-03
comments: true
tags: [Arment, Gruber, iPad]
---
I risultati trimestrali di Apple (i [migliori di sempre](<p class="wp-caption-text">)) comprendono un consistente declino di iPad, per di più costante da molti trimestri e molti si chiedono il perché.

Marco Arment [argomenta](https://marco.org/2017/01/31/the-wrong-future) che in fondo iPad sia stato una fase. Produce da [Six Colors](https://marco.org/2017/01/31/the-wrong-future) due grafici convincenti, appunto quello di iPad che mostra la fase discendente di un’onda e quello di Mac: nuovo record di vendite, sempre a galla con qualche alto e qualche basso, ci sarà sempre qualcuno che ne vuole uno. (Tra parentesi, i MacBook Pro poco professionali, poco espandibili, poco riparabili, con poca Ram, con poca grafica, poca potenza, vanno via come il pane).

John Gruber [ritiene](http://daringfireball.net/linked/2017/01/31/cooling-ipad-sales) che gli anni del picco, 2013 e 2014, siano stati eccezionali perché il mercato era vergine e quasi nessuno aveva un iPad. Inoltre, lo spazio concettuale teorizzato da Steve Jobs – una zona franca tra portatili e computer da tasca – era molto ampia.

Questa zona si è assai ridotta: tra un iPhone 7 Plus e un iPad mini la distanza è brevissima. I Mac sono diventati sempre più leggeri e sottili, sempre più vicini come ingombro a un iPad.

Il vero punto a mio parere, che peraltro anche Gruber sottolinea, è che iPad è una macchina diabolicamente duratura. Scrivo queste note sotto il piumone, con un terza generazione che non perde un colpo, anche se non monta iOS 10. Per il grande pubblico, l’incentivo a cambiare iPad arriva  dopo molti anni, quasi come per un Mac. Quelli che lo hanno comprato nel 2014 non hanno il minimo intento di sostituzione, nella massa.

Il tutto tenendo le cose nel giusto contesto: le vendite di iPad, nel loro declinare, sono due volte e mezzo quelle di Mac. Quelle sue curve che mostra Arment sono visivamente sbilanciate, sembrano due corse alla pari e non lo sono.

Ancora, se Apple vendesse solo e unicamente iPad, sarebbe al numero 139 della classifica Fortune 500 che enumera le più grandi aziende al mondo. Molti si accontenterebbero.
