---
title: "Cari amici vicini e lontani"
date: 2019-06-01
comments: true
tags: [Sundell, Swift, Gruber, WWDC]
---
Citare John Gruber è vincere facile e cerco di starne lontano il più possibile, però [eccezioni come questa](https://daringfireball.net/linked/2019/05/30/wwdc-by-sundell) sono doverose: [Wwdc by Sundell](https://wwdcbysundell.com), sito messo in piedi da uno sviluppatore per dare modo di seguire WWDC nel modo più completo possibile, da tutto il mondo, anche a chi non ha le risorse o il tempo o il biglietto in mano per seguire i lavori che iniziano dopodomani.

Il sito è una bellezza, già pieno di cose interessanti, semplice, pulito. veloce, una bellezza.

Dello stesso autore, [Swift by Sundell](https://www.swiftbysundell.com), blog settimanale dedicato allo sviluppo con Swift. Ho sfogliato distrattamente alcuni articoli e sembra una risorsa molto utile per facilitare e velocizzare lavoro e apprendimento.

Non ho idea del perché, ma questa WWDC mi accende l’aspettativa. Più del solito.
