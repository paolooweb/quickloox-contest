---
title: "Non-recensioni: Mac mini 2018"
date: 2018-11-25
comments: true
tags: [Mac, mini, Ive, iPad, Arm, Newton, MessagePad, Schuko, Trackpad]
---
Non avrebbe senso parlare di Mac mini senza un contesto.

Venti anni fa lavoravo con un Mac desktop e un Mac portatile. Il portatile era fortemente penalizzato in dotazioni rispetto al desktop e aveva l’unico merito di essere, appunto, portatile.

Poi uscì (di produzione) [Newton MessagePad](https://512pixels.net/2015/08/apple-newton-guide/), una macchina così avanti sui tempi che dovevo assolutamente averla prima che diventasse impossibile reperirla. Vendetti il portatile e lo sostituii con un Newton, che mi permetteva di produrre lavoro di pari qualità a una frazione di peso e ingombro. Lo accendo ancora quel Newton, ma è un’altra faccenda.

I portatili però progredirono. A un certo punto smisi di avere un Mac desktop: il portatile permetteva di fare tutto quello che mi serviva e poteva seguirmi ovunque.

Arriviamo a oggi, quando da anni il mio Mac portatile resta immobile sul desktop e io esco con un iPad. Non solo: il portatile, sia pure stravecchio, se la cava alla grandissima sulla gran parte delle fatiche cui è chiamato. Significa che siamo arrivati a un punto in cui, se non sono superspecializzato in discipline ad alto impatto computazionale, ma uso trasversalmente i computer come strumento di organizzazione produzione e amministrazione, avere una macchina molto performante ha senso solo dal punto di vista della sua durata nel tempo.

E un uccellino mi sussurra la sera prima di dormire che nel giro di pochi anni vedremo Mac con processore Arm, derivato da quelli attualmente in uso sugli iPad e sugli iPhone.

Ecco che cambio impostazione, dopo molti anni, e adotto un [Mac mini](https://www.apple.com/it/mac-mini/) di basso tonnellaggio, con un banalissimo processore i3 ma sedici gigabyte di Ram (da cui mi sembra faticoso prescindere) e 512 gigabyte di disco Ssd (per pareggiare la capacità da cui provenivo e facilitare il lavoro con Time Machine).

Dopo qualche giorno, sono molto soddisfatto.

Fa impressione, dopo tanti anni di portatili, operare in modo slegato dalla macchina. La mattonella grigia (siderale) sta in un angolo, silenziosa, fredda o appena tiepida. L’unico segno di vita è il Led di accensione, che penso sia costato incubi notturni a Jonathan Ive. Lui avrebbe voluto toglierlo e io pure. Non mi preoccupo di spegnere Mac; se ne va in stop, recupera la posta e i messaggi senza dire niente con Power Nap e io me lo ritrovo aggiornato quando mi siedo, premo un tasto e sono di nuovo in sella. Niente parti in movimento, se ci sono ventole sono silenziose o inattive, consumi in stop che sono infimi… che senso ha un Led di accensione?

Non ho ancora veramente tirato il collo alla macchina, ma il Dock è sopra le venti applicazioni aperte e Safari tiene aperte una quarantina di pagine. Non ho ancora visto la *spinning beach ball*; la reattività è ineccepibile. I file si copiano, leggono, salvano a velocità Ssd, cioè ottimale.

La provvista di porte è ottima. Il monitor è collegato via Hdmi, i dischi Usb del passato recente e remoto hanno a disposizione due porte. I prossimi apparecchi Usb-C hanno a disposizione quattro porte. Usavo un piccolo hub Usb da quattro soldi ed è finito nel cassetto, almeno per ora. La connettività non è davvero un problema.

La configurazione iniziale, anche nell’età del disincanto, ispira un pizzico di magia. Un monitor che dormiva inoperoso, una vecchia Magic Trackpad, una vecchia Wireless Keyboard e… *it just works*. Le schermate che invitano ad accendere una periferica Bluetooh in modo che Mac possa connettersi sono *retró* ed è l’unica critica che possa muovere. Perfino la spina non è più l’ingombrante Schuko ma una molto più pratica spina casalinga a due poli. Ovviamente Mac è attaccato a una multipresa, che ovviamente ha una protezione dalle sovratensioni, si trova ovunque e costa pochissimo. La mancanza del terzo polo non è una mancanza; è una presa d’atto.

Il foro inferiore, che permette di accedere all’hardware, è chiuso da un coperchio di plastica (?) nera che per quanto mi riguarda resterà com’è, salvo catastrofi. Il bello di questa macchina è esattamente la sua attitudine discreta e solenne. Lo metti in opera e potresti neanche accorgerti che è lì. Si diceva che la tecnologia matura è quella invisibile e un Mac mini è il Mac più invisibile che ci sia.