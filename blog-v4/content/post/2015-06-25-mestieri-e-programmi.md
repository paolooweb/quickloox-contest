---
title: "Mestieri e programmi"
date: 2015-06-25
comments: true
tags: [R, Excel, MathML, Photoshop]
---
Era rimasta una [questione aperta](https://macintelligence.org/posts/2015-06-23-la-questione-e-aperta/) legata all’utilizzo di formati il più possibile testuali, liberi e magari pure semplici.<!--more-->

La vedo legata a doppio filo con un’altra questione: l’insegnamento a scuola delle tecniche mettendo in secondo piano quello dei programmi.

Siamo piagati dall’idea che per sapere fare qualcosa bisogna saper usare il programma che fa quella cosa. È una fabbrica di coatti: non bisogna imparare Excel ma conoscere i principî di un foglio elettronico. Non bisogna imparare a usare [R](http://www.r-project.org) ma studiare statistica, calcolo combinatorio eccetera. E poi, che sia R o altro, comunque non si resterà mai a piedi. Non bisogna sapere usare Photoshop, ma conoscere la teoria dei colori il resto che serve nella fotografia e nel fotoritocco. E così via.

Se impariamo a usare [MathML](http://www.w3.org/Math/) invece dell’editor di equazioni di Word, potremo mettere equazioni perfettamente disegnate ovunque. (MathML, già che siamo qui, è diventato [standard](http://www.digitalbookworld.com/2015/mathml-language-certified-an-international-standard/).

Poco convinti? Ecco il [racconto](http://opensource.com/life/15/6/my-linux-story-seth-kenlon) di gente che ha perso il lavoro perché, invece di conoscere la  materia, conosceva un programma specializzato in quella materia.

Il mondo sta davvero cambiando. Servono i fondamentali; gli strumenti vanno e vengono. Guai a legarsi a uno strumento, a dipendere da un software.