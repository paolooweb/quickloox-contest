---
title: "L’accento sul degrado"
date: 2024-02-28T00:01:31+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [coffeestains, LaTeX, Teams]
---
Mi tocca affrontare una conversazione su un sistema di messaggistica di cui sembra che molte società non possano fare a meno, un *must*, una *conditio sine qua non*, un *mai più senza*. Se non ce l’hai, sei fuori, non sei conforme. E quindi, siccome si lavora insieme, eccomi a scrivere.

Con un clic apro un nuovo messaggio. C’è un campo testo circondato dalla consueta panoplia di strumenti da word processor, come se la messaggistica servisse a scrivere libri o la gente partecipasse veramente a conversazioni con interventi da ventimila parole. Comunque il mio puntatore è dentro un campo testo e il cursore lampeggia per segnalare che è pronto a ricevere input.

Digito una *e* maiuscola accentata come questa: È.

Il sistema irrinunciabile di messaggistica inizia a registrare uno spezzone di video.

Sicuramente ho sbagliato qualcosa. Mi si sono incastrate le dita. Ho sbagliato un tasto, nella fretta. Oppure ho premuto due tasti insieme dove ne bastava uno. Sono cose che succedono.

Riprovo e sto attento e concentrato: Opzione-maiuscole-e. Ricordo, il *focus* della mia azione è un campo testo. Il programma si aspetta testo, non comandi.

Parte la registrazione di uno spezzone di video.

Ricordo benissimo che qualche giorno fa il programma si è aggiornato, da solo. Adesso ha aggiunto al suo nome la dizione *per il lavoro e per la scuola*. Il pensiero che un programma di messaggistica *per la scuola* abiliti a scrivere testo e non contempli la *e* maiuscola accentata mi manda ai matti.

Sicuro, è un problema della versione Mac, perché nella landa desolata la gente è abituata ad accentare con l’apostrofo anche le minuscole, figuriamoci la maiuscola. In realtà il problema è nell’atteggiamento generale della cultura della landa desolata: una fucina di ignoranti coartati a subire software che promuove, favorisce, diffonde, enfatizza l’ignoranza e la sciatteria.

Un computer è un oggetto incredibile, con cui si possono raggiungere obiettivi non importa quanto impegnativi, ognuno come sceglie di volerlo fare. Lo trasformano in un elettrodomestico per l’omologazione e per il nonpensiero, ancora peggio del [bispensiero](https://www.romanoscaramuzzino.it/2022/12/10/il-bipensiero-o-bispensiero-di-orwell-e-la-sua-attualita-ai-tempi-nostri-non-per-complottismo-ma-per-cultura-reale/) orwelliano.

La resistenza contro il degrado c’è, sotterranea, negletta, per pochi, ma c’è. Un piccolo universo dove il computer serve al suo scopo originario: controllare i nostri dati nel modo più potente e libero.

Nel mondo nascosto si può trovare persino un pacchetto per [creare con fedeltà e realismo macchie di caffè su un documento](https://ctan.org/pkg/coffeestains). Raggiungere la perfezione con l’aiuto dell’imperfezione.

Di là, la desolazione dell’appiattimento.

Non importa se siamo pochi, forse neanche ce la faranno i nostri figli a vederlo, ma a un certo punto ci sarà il *tipping point*. Vinceremo contro il degrado nel nome della ricerca della bellezza, anche se fosse rimasto uno solo sulla Terra a manutenere [TeX](https://tug.org/mactex/).