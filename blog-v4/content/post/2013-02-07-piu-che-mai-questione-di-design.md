---
title: "Più che mai questione di design"
date: 2013-02-07
comments: true
tags: [TheVerge, Inspiron, Dell, DJ, Adamo, Xps, Duo, Venue, Pro]
---
*The Verge* rivisita <a href="http://www.theverge.com/2013/2/6/3958198/10-years-of-dells-failed-consumer-devices">dieci anni di apparecchi consumer fallimentari marchiati Dell</a>. Qualche gemma dai commenti che accompagnano le foto (nota: in realtà bastano le foto).<!--more-->

**Inspiron 8600**

>Sono i portatili Windows che hanno insegnato alle persone a odiare i portatili Windows e mandato un sacco di persona a comprare MacBook.

**Dell DJ**

>Un lettore Creative rivisitato con una interfaccia fuori di testa, nessuno stile e pessimo software desktop.

**Adamo**

>Difficile dire se presentare ora un computer come questo sia cattivo tempismo o solo ignoranza.

**Inspiron Duo**

>Non solo manca di uno schermo decente, ma il software e le prestazioni balbettanti lo rendono incredibilmente frustrante da usare.

**Adamo Xps**

>Se si strizzano gli occhi si può vedere l’abbozzo delle tavolette ibride di oggi, ma se li si spalancano si vede realmente che Dell ha cercato così tanto di battere Apple sul design da dare fuori di matto.

**Venue Pro**

>Essenzialmente non sono riusciti a lanciare il telefono, con false partenze una dopo l’altra che hanno ritardato il prodotto per mesi. E quando finalmente ci è arrivata una unità da recensire, non funzionava.

Discutere sul numero di porte Usb o sui gigahertz del processore è un modo di dimenticare i fondamentali. Il *design* non è un elenco di ingredienti, è un piatto cucinato a puntino.