---
title: "Andare in bianco"
date: 2019-03-03
comments: true
tags: [Slack, logo]
---
Condivido con John Gruber l’idea che il restyling del logo di Slack [sia stato un grande errore](https://daringfireball.net/linked/2019/01/16/slack-bland-new-logo).

Non me ne sarei mai occupato se lo sfondo viola dell’icona su iOS non fosse stato sostituito da uno sfondo bianco.

Criticare un logo è uno sport da sempliciotti. Bisognerebbe sapere o almeno intuire di estetica, colore, proporzioni e un sacco di altre discipline, o stare dignitosamente zitti.

Sperimentare una mancanza di funzionalità tuttavia è tutt’altra cosa. [Secondo Slack](https://twitter.com/SlackHQ/status/1100478125530583041), la modifica rende più leggibili le notifiche nel *badge* dell’icona.

<blockquote class="twitter-tweet"><p lang="en" dir="ltr">Starting today, you may notice your Slack mobile app icon change from purple to white. This should make it a little easier to see on your device. Visit the Google Play or Apple App Store to update.</p>&mdash; Slack (@SlackHQ) <a href="https://twitter.com/SlackHQ/status/1100478125530583041?ref_src=twsrc%5Etfw">February 26, 2019</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

Sono utente intenso, soddisfatto e appassionato di Slack, lo promuovo ovunque e i suoi pregi superano largamente i difetti. La nuova icona, però letteralmente non la vedo nel Dock di iPad Pro. Per accorgermi che c’è, mi ci vuole una seconda occhiata.

Posso essere l’unico al mondo a riscontrare questa esperienza. Pazienza, sarò l’unico al mondo a dire che il nuovo sfondo bianco è fantozzianamente quella cosa pazzesca.
