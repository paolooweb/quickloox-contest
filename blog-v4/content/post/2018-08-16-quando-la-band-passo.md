---
title: "Quando la band passò"
date: 2018-08-16
comments: true
tags: [Mac, Deviazioni, Spappolate, Vasco, Rossi]
---
Alla festa della birra del paesello, finale di serata con la *tribute band* dedicata a Vasco.

Sul palco, un Mac per la musica e un iPad per le luci.

 ![Il palco delle Deviazioni spappolate alla festa della birra](/images/palco-deviazioni.jpg  "Il palco delle Deviazioni spappolate alla festa della birra") 
 ![iPad addetto al controllo luci durante il concerto delle Deviazioni spappolate alla festa della birra](/images/ipad-luci-deviazioni.jpg  "iPad addetto al controllo luci durante il concerto delle Deviazioni spappolate alla festa della birra") 

[Deviazioni spappolate](http://www.deviazionispappolate.com/it/index.php) quanto si vuole, ma sanno suonare e anche scegliersi l’equipaggiamento.
