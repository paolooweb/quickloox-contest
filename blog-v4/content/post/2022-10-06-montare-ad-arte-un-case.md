---
title: "Montare ad arte un case"
date: 2022-10-06T02:03:30+01:00
draft: false
toc: false
comments: true
categories: [Internet]
tags: [Macintosh Portable, Jean-Louis Gassée, PowerBook]
---
La Commissione europea renderà obbligatorio l’uso di caricatori Usb-C per il 2024? Diamogli magari il tempo di cambiare idea, o che qualcuno inventi una nuova generazione di connettori sufficientemente avanzata da farli apparire ancora più tecnologicamente bigotti e ignoranti di quanto già non siano. Se tanto mi dà tanto, sarà il momento in cui lo standard dell’industria consisterà nel fare a meno di connettori fisici.

Preferisco rimandare al video in cui si vede un più giovane di adesso ed emozionatissimo Jean-Louis Gassée [presentare Macintosh Portable](https://www.youtube.com/watch?v=ZzlQdWKmPuE&t=1s).

La particolarità della presentazione sta nel fatto che Gassée, in giacca e cravatta e a mani nude, *monta sul palco l’intero computer*, in una decina di minuti, senza viti, senza trucchi, senza farsi male.

<iframe width="560" height="315" src="https://www.youtube.com/embed/ZzlQdWKmPuE" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

È una dimostrazione impressionante di superiorità del design costruttivo, che eclissa a parità di prestazioni e funzioni qualsiasi altra cosa ci fosse sul mercato nel 1989. Intanto che Gassée monta, commenta, spiega i componenti tecnologici, fa battute. È *semplice*. Alla fine preme un tasto e Macintosh Portable si accende veramente. *Funziona*.

Chiunque, trentatré anni fa, sarebbe uscito da quella presentazione semplicemente conquistato.

Per fortuna non erano presenti burocrati europei. Gli sarebbe venuto naturale obbligare il mercato a costruire computer fatti come Macintosh Portable, che pesava oltre sette chilogrammi.

Due anni dopo, Apple [presentò i primi PowerBook](https://www.youtube.com/watch?v=qaBmrAiVhJ0&t=39s). Impossibili da montare su un palco scherzando con il pubblico.

A parte le prestazioni, pesavano *un terzo* di Macintosh Portable, in una frazione dell’ingombro. I presentatori di Apple al Comdex del 1991 ne nascosero uno dentro il cassetto della carta di una Personal LaserWriter.

<iframe width="560" height="315" src="https://www.youtube.com/embed/qaBmrAiVhJ0" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Per il burocrate europeo, sarebbero stati computer fuorilegge.