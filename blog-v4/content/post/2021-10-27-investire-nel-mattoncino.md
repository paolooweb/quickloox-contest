---
title: "Investire nel mattoncino"
date: 2021-10-27T00:47:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [Apple Arcade, Lego Builder’s Journey, Steam, Switch, Monument Valley 2, Apple TV] 
---
Ho già spiegato i perché della [decisione di investire quattro euro e novantanove ogni mese in Arcade](https://macintelligence.org/posts/Permesso-di-scarico.html).

Ieri ho provato con le figlie [Lego Builder’s Journey](https://www.lightbrick.com/builders-journey) ed è stato uno di quei momenti in cui hai la conferma che il ritorno è valso la spesa.

Di sicuro non è [Monument Valley 2](https://www.monumentvalleygame.com/mv2) (che tra l’altro [ha aggiunto di recente un nuovo capitolo](https://www.theguardian.com/technology/2014/nov/13/monument-valley-new-levels-reviews) al gioco), o almeno non ancora; abbiamo appena cominciato.

Tuttavia c’è atmosfera, c’è magia, a ogni livello c’è una piccola sorpresa in più oltre che una piccola difficoltà in più.

In tre sulla tv ci siamo divertiti e abbiamo scoperto insieme l’inizio della trama. Dai tre anni e tre quarti in su, siamo rimasti affascinati e ci siamo ripromessi di tornare a giocarci insieme per scoprire che cosa accade.

Davvero non ho molto altro da chiedere al contesto hardware a software.

(Ah, chi pratica Steam o Nintendo Switch lo troverà anche lì).

*Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile.*