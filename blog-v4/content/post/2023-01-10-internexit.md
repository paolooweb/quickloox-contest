---
title: "Internexit"
date: 2023-01-10T01:05:38+01:00
draft: false
toc: false
comments: true
categories: [Hardware]
tags: [The Verge, Gigabit Internet]
---
Da pochi giorni, per legge, [qualsiasi nuova abitazione inglese dovrà essere costruita con le connessioni necessarie per avere Gigabit Internet](https://www.theverge.com/2023/1/9/23546401/gigabit-internet-broadband-england-new-homes-policy).

Sinceramente non li invidio per essere usciti dall’Europa. Mi piacerebbe comunque se l’Europa entrasse nell’ordine di idee di dare ai suoi cittadini la connessione Internet necessaria. Oggi avere Gigabit Internet non è un lusso, ma una infrastruttura critica.

*Disclaimer*: qualche settimana prima, [Tim ha piazzato davanti all’ingresso della mia rampa del box la colonnina della fibra ottica](https://macintelligence.org/posts/2022-11-11-come-non-resistere-alla-tentazione-di-nuovo/). E mi offre fino a mille megabit teorici, che diventano cinquanta megabit pratici.

Prevedo un esodo di massa di inglesi vogliosi di trasferirsi nel mio quartiere.