---
title: "Un classico che emoziona"
date: 2019-05-15
comments: true
tags: [WoW, Classsic, Warcraft]
---
[World of Warcraft](https://worldofwarcraft.com/en-gb/) come è oggi è impegnativo e appassionante.

Così come è nato quindici anni fa, chiedeva pazienza e restituiva emozione.

Non ho tempo per giocare stabilmente e guardo magari a un giorno nel quale girerò per Azeroth scortato dalle figlie, ma [questo agosto WoW Classic](https://www.extremetech.com/gaming/291425-wow-classic-has-a-release-date) lo voglio provare in ogni caso.

Di cose impegnative e appassionanti sono pieno, mentre l’emozione rimane merce rara.
