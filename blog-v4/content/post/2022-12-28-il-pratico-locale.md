---
title: "Il pratico locale"
date: 2022-12-28T03:50:26+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [macOS, LocalDictionary, La lettera rubata, The Purloinedd Letter, Poe, Edgar Allan Poe]
---
Ho comprato Mac dal 1986 e sono passati trentasei anni prima di imparare che il dizionario utente, da me immaginato come sepolto chissà dove e codificato chissà come, è un banalissimo file di testo situato a `~/Library/Spelling/LocalDictionary`.

Sarà l’abitudine a confrontarsi con i percorsi micidialmente involuti delle cartelle iCloud, ma era semplice, a portata di mano, evidente e non me ne sono mai accorto prima. E sì che ho letto [La lettera rubata](https://learningenglish.voanews.com/a/the-purloined-letter-edgar-allan-poe/3307412.html) di Edgar Allan Poe: l’oggetto meglio nascosto è quello lasciato in vista.

Vale anche come antidoto alla litania *una-volta-ci-potevi-guardare-dentro*. Anche adesso. Solo questione di farlo.