---
title: "Ritratto di iteratore"
date: 2014-06-23
comments: true
tags: [Anandtech, iOS8, Nokia, Htc, Sense, iOS, Apple, iterazione]
---
*Anandtech* regala una bella spiegazione di che [cosa potrà fare in ambito fotografico iOS 8](http://www.anandtech.com/show/8131/manual-camera-controls-in-ios-8) e perché vale la pena di parlarne.<!--more-->

La sintesi: tutte le funzioni principali di una macchina fotografica degna di questo nome saranno controllabili manualmente. Bilanciamento del bianco, apertura, tempo di scatto, controllo esposizione e via dicendo.

Non solo: anche le *app* indipendenti potranno accedere ai controlli inseriti nel sistema e offrirli ove opportuno.

Uno dei punti di iPhone 2007 ritenuti deboli dalla critica era la povertà della fotocamera, che non reggeva il confronto con la concorrenza. Iterazione dopo iterazione, portando migliorie a ogni nuovo modello, oggi si può scrivere (o almeno lo fa Anandtech):

>tutti questi nuovi controlli permetteranno [di scrivere] applicazioni fotografiche simili a Pro Camera di Nokia o Sense 6 di Htc. È stato detto che Apple è uno dei pochi produttori di equipaggiamento originale a prendere sul serio la fotocamera e le novità possono solo rendere ancora più solida questa posizione.