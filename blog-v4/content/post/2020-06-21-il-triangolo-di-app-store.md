---
title: "Il triangolo di App Store"
date: 2020-06-21
comments: true
tags: [Apple, App, Store, Hey, Basecamp, Asymco, Dediu, Wwdc, Trilussa]
---
È stato scritto moltissimo sulla [diatriba tra 37signals e Apple](https://www.nytimes.com/2020/06/19/opinion/apple-app-store-hey.html), riguardante il servizio di email Hey e la sua eventuale app. Hey costa 99 dollari l’anno, Apple rifiuta la app perché non si comporterebbe secondo le regole e 37signals non ha intenzione di accettare scenari nei quali cede ad Apple parte degli incassi che registrasse la app stessa.

La _querelle_ ha portato alla luce ampia [insoddisfazione da parte degli sviluppatori](https://mjtsai.com/blog/2020/06/19/app-store-for-the-past/) per come Apple manda avanti App Store. Da una parte, regolare in modo netto e inequivocabile ogni tipo di distribuzione di software è impossibile; dall’altra è evidente che Apple non tratta tutte le software house allo stesso modo e certi grandi nomi o grandi app hanno ricevuto un trattamento di favore. Sembra anche chiaro che la struttura di App Store sia inadeguata per come il mercato del software è cambiato negli ultimi dieci anni.

Stranamente, non è stato scritto _tutto._

Certamente esiste una tensione tra Apple e gli sviluppatori. Non è un caso che questo accada a poche ore da WWDC. Neanche è la prima volta che succede. Eppure sembra che sia una faccenda a due. Invece è un triangolo. Ci sono anche gli _utilizzatori_.

Una ricerca indipendente ha stimato le dimensioni dell’ecosistema di App Store nel 2019 a [519 miliardi di dollari](https://www.apple.com/newsroom/pdfs/app-store-study-2019.pdf).

Secondo le stime di Horace Dediu di Asymco, [le vendite dirette di App Store si avvicinano a 68 miliardi di dollari](http://www.asymco.com/2020/06/20/subscribe-again/), il tredici percento del totale. Apple incassa il 30 percento di questa cifra.

Complessivamente, quindi, Apple si mette in tasca il 3,8 percento dell’ecosistema di App Store.

Chi ne parla in termini di avidità da parte di Apple, evidentemente, è un filo fuori strada.

Gli sviluppatori vogliono regole chiare e uguali per tutti e hanno ragione. Vogliono una quota Apple inferiore a 30 percento e possono anche avere ragione. Di fronte a queste cifre, tuttavia, dovrebbe essere chiaro che il nodo sta da un’altra parte.

I conti di Dediu indicano che, in estrema semplificazione, l’utilizzatore medio di iPhone spende un dollaro al giorno in hardware e 1,4 dollari al giorno in software e servizi.

Se si considera che gli utilizzatori medi sono più di un miliardo, si capisce a che cosa pensi Apple intanto che litiga con 37signals.

Nessuna delle tre parti può vivere senza le altre due. Il buonsenso ci dice che il triangolo migliore in questa situazione è equilatero: fuori di metafora, la soluzione migliore è quella che soddisfa al meglio Apple, sviluppatori e utilizzatori.

I media presentano la situazione come un triangolo isoscele, Apple contro gli sviluppatori o viceversa. Qualcuno insaporisce la vicenda con una spruzzata di Commissione europea, sempre alla ricerca di qualche pretesto per mettere insieme una accusa di antitrust e raccattare denaro non dovuto. Degli utilizzatori non scrive nessuno.

Direi, nella mia modestia, che se c’è una spesa pro capite nei termini espressi sopra, c’è un grado di soddisfazione medio piuttosto alto. Medio, certamente; però l’argomento di Trilussa – tu mangi due polli, io digiuno, per statistica siamo tutti sazi – si applica meglio ai campioni ridotti; la narrazione preferita da molti, pochi fessi intontiti dalla propaganda di Apple che spendono capitali in rappresentanza della massa e fanno media da soli, è difficile da sostenere, sopra il miliardo di teste. O i fessi sono così tanti che forse fessi non sono, oppure sono talmente ricchi da chiederci perché non si parli di loro più spesso. O perché non si comprino aziende, invece di app.

Il triangolo lo vedo scaleno. Gli utilizzatori sono soddisfatti, hanno il lato più lungo. O forse lo ha Apple, che fa soldi a palate? E perché non gli sviluppatori stessi, che si lamentano, ma stanno su App Store? È brutto, [scrive John Siracusa](https://hypercritical.co/2020/06/20/the-art-of-the-possible), che la gestione materiale di App Store si allontani dagli ideali con cui Apple lo ha aperto. È sgradevole che gli sviluppatori stiano sullo Store a denti stretti. Ma è un fatto che ci stanno e quindi una convenienza la hanno. Certo App Store non ha alternative reali; altrettanto vero che, se uno sviluppatore pensa di lasciare il cattivo App Store per fare soldi sul Play Store di Google, auguri. È il mondo del software che è privo di alternative reali, non App Store. Se vuoi fare lo sviluppatore per vivere, oggi funziona così. È per certi versi ingiusto e crudele. Dovresti vedere che cosa è diventata l’editoria, ma è un altro discorso. Invece, prova a pensare che senza il cattivo App Store, molto probabilmente, non saresti in attività.

Per dire che Apple ha certamente cose da correggere. Gli sviluppatori, giustamente, cercano un trattamento equo. Apple ha creato non solo un posto dove si vende, ma anche dove la gente _compra_. Quale che sia la forma del triangolo, che io sia [Jason Fried](https://twitter.com/jasonfried) o [Phil Schiller](https://twitter.com/https://twitter.com/pschiller), ci penserei bene, prima di intervenire sul lato del pubblico. Quello che spende e fa vivere Apple, che compra e fa vivere gli sviluppatori.