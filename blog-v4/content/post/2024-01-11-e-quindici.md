---
title: "E quindici"
date: 2024-01-11T18:15:25+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [Bare Bones, BBEdit, ChatGpt]
---
Trenta anni lo scorso maggio, quindici versioni: bello iniziare l’anno con un [BBEdit rinnovato](https://www.barebones.com/support/bbedit/notes-15.0.html). Per un fedele utilizzatore il prezzo di aggiornamento è a trentaquattro dollari e novantanove, che tra tutto diventano trentotto euro e spiccioli.

Per l’esborso arriva una versione con una cinquantina di *Additions*, quasi quaranta *Changes* e più di cinquanta *Fixes*. Non si va a peso naturalmente e specie con BBEdit; comunque il programma è più ricco, più curato e più stabile al netto di Sonoma, che introduce [bug e regressioni](https://www.barebones.com/support/new-os.html) in grado di influire eventualmente sul funzionamento standard. Personalmente non ho ancora riscontrato niente e spero di proseguire.

Ho subito comprato l’aggiornamento per via di cose che mi fanno comodo. Due *highlights* su tre, *cheat sheet* espansi e minimappa per navigare un file, troveranno subito applicazione; il terzo, il worksheet per ChatGPT, non mi riguarda ma sarà di una comodità pazzesca per chi sviluppa e può interagire con il chatbot sempre da dentro BBEdit. La funzione riguarda solo gli abbonati paganti a ChatGpt.

Poi ci sono variazioni minori che mi tornano utili ma sono troppe per essere trattate. Una voce aggiuntiva di menu ora normalizza gli spazi da un file, eliminando i *non breakable space* che arrivano spesso se si incollano testi copiati da web. Devo ancora sperimentare le implicazioni dell’accreditamento di BBEdit come applicazione autorizzata ad aprire *EPUB containers*; se è come l’ho capita, potrò risparmiare un discreto spazio su disco e anche del tempo. Anche nuove aggiunte al supporto di git e all’esecuzione di script UNIX rischiano di tornarmi utili. Eccetera.

Credo fossero due anni e mezzo circa che aspettavamo una nuova versione primaria. Ho speso più che volentieri la cifra richiesta per aggiornare. L’ho fatto anche quando è uscito BBEdit 14. Se BBEdit 15 non mi fosse sembrato utile, non lo avrei comprato e sarei rimasto serenamente con la versione 14, che continua a funzionare.

Così funziona l’acquisto del software. Mi vendi una versione, la compro, è mia e la posso usare. Non mi interessa? Non la compro, come è logico.

Se BBEdit fosse in abbonamento a quindici euro l’anno pagherei grosso modo gli stessi soldi ma dovrei sorbirmi anche una versione non gradita o non utile. Potrei interrompere l’abbonamento, solo che è un gesto superfluo rispetto al comprare la versione che voglio.

L’abbonamento inoltre è l’adesione a una promessa di sviluppo, pago per avere qualcosa che non so se vorrò. Comprare un programma quando è pronto permette di valutare il prodotto e sapere per che cosa verso il mio denaro.

Per chi ha esigenze minime di trattamento di testo, BBEdit funziona gratuitamente come editor di testo, senza le opzioni per lo sviluppo e la programmazione che sono prerogativa della versione a pagamento. BBEdit gratis è niente male.