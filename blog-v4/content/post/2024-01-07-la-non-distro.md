---
title: "La non distro"
date: 2024-01-07T19:52:24+01:00
draft: false
toc: false
comments: true
categories: [Web, Software, Hardware]
tags: [Klaus Zimmermann, Zimmermann, State of the Distro, Raspberry Pi, FreeBSD, Debian, Alpine]
---
Udite udite! Secondo Klaus Zimmermann, che periodicamente redige uno [State of the Distro](https://kzimmermann.0x.no/articles/state_of_the_distro_2023.html), la variante Linux migliore nel 2023 per il desktop è [Debian Linux](https://www.debian.org).

Debian è il meglio anche in ambito server e complessivamente.

Per curiosità, il meglio da installare su una chiavetta per portarsela dovunque è [Alpine Linux](https://www.alpinelinux.org).

La ciccia è invece che la distro migliore per [Raspberry Pi](https://my.raspberrypi.org/) è una non distro: [FreeBSD](https://www.freebsd.org).

La cultura Linux fa una certa fatica a riconoscere il valore dei sistemi Unix altri da sé, quindi di questo riconoscimento c’è da essere contenti e magari farsi venire la curiosità se sia vero, nella pratica.