---
title: "Specialisti al lavoro"
date: 2014-06-10
comments: true
tags: [Forbes, Apple, Aapl]
---
Ignoro per quanto rimarrà. Sta di fatto che mentre scrivo, *Forbes* riporta la quotazione azionaria Apple come [in ribasso di 551,87 dollari](http://www.forbes.com/companies/apple/financial/AAPL/), con una perdita dell’85,49 percento, a 93,70 dollari.<!--more-->

 ![Forbes: Apple in forte ribasso](/images/forbes.png  "Forbes: Apple in forte ribasso")

C’è una ragione, [annunciata in aprile](http://www.apple.com/it/pr/library/2014/04/23Apple-Expands-Capital-Return-Program-to-Over-130-Billion.html):

>Il Consiglio di Amministrazione ha anche annunciato uno split azionario sette-per-uno. Ogni azionista Apple registrato alla chiusura delle attività il 2 Giugno 2014 riceverà sei ulteriori azioni per ogni azione posseduta alla data di registrazione, e il trading inizierà sulla base dell’adattamento allo split il 9 giugno 2014.

Ovviamente le azioni si sono divise in sette e lo stesso è accaduto al loro valore. Gli specialisti della finanza non se ne sono accorti. Bel lavoro.