---
title: "Vedo e non vedo"
date: 2014-10-02
comments: true
tags: [iOS, iOS8, Safari, Textastic, Mail, iPhone5, iPad]
---
Procedo nell’uso di iOS 8 e per il momento non ho incontrato problemi. Mail mostra disallineati numerosi allegati presenti in un messaggio e insomma, ci si aspetterebbe una maggiore pulizia visiva in questa e altre situazioni.<!--more-->

Sempre Mail ha reso più comoda la cancellazione o archiviazione di messaggi di posta con un gesto. Qualcosa non torna, però; alcuni account mi fanno cancellare un messaggi strisciando verso sinistra, altri verso destra. L’azione di cancellazione dovrebbe essere univoca e inconfondibile, non dipendente dal tipo di *account*.

Alcune *app* hanno bisogno di un aggiornamento di rifinitura: per esempio [Textastic](https://itunes.apple.com/it/app/textastic-code-editor-for/id383577124?l=en&mt=8), che mostra una fila di pulsanti aggiuntivi sopra la tastiera standard, nasconde parzialmente con la fila extra la tastiera standard suddetta. Anche se tutti i tasti sono comunque visibili e si può scrivere, si impone un aggiornamento (di Textastic).

Si è parlato di perdita di velocità e devo dissentire, del tutto a titolo personale. Su iPad di terza generazione a volte si avverte una leggera esitazione nello scorrimento delle icone nella Springboard oppure nella partenza o chiusura delle *app* ed è tutto. L’usabilità è salva e internamente alle *app* fin qui provate le prestazioni sono come prima. Su iPhone 5 (che a questo punto è la terz’ultima generazione) l’ambiente risulta fluido e reattivo come e forse più di prima.

Una cosa bella: la nuova interfaccia di Safari che si nasconde il più possibile quando si scorre una pagina e si rivela non appena la cerchiamo. iOS (e interfaccia *mobile*) di avanguardia, che privilegia il contenuto, conserva la funzionalità, mostra eleganza e attenzione ai desideri dell’utilizzatore e non chiede di essere scoperta, cosa che ha valore e tuttavia non sempre apprezzo. Andasse sempre così.