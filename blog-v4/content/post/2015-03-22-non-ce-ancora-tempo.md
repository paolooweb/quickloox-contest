---
title: "Non c'è ancora tempo"
date: 2015-03-22
comments: true
tags: [watch, ForceTouch, trackpad, iMovie, Apple, Ntp, Taptic]
---
Nella storia di Apple ci sono state numerose innovazioni e alcune hanno sconvolto i mercati, o cambiato la storia del mondo. Ma c’è stato un solo momento *wow!*, l’uscita di Macintosh. Neanche iPhone ha stravolto così potentemente lo *status quo*.<!--more-->

Ciò detto, alcune delle innovazioni migliori sono state così sottili che quasi, non fosse per qualche professionista, passavano inosservate. Una di queste è [iMovie aggiornato](https://itunes.apple.com/it/app/imovie/id408981434?l=en&mt=12) per sfruttare il Force Touch reso possibile dai nuovi [trackpad sensibili alla pressione](http://www.apple.com/it/macbook/design/).

Quando si arriva alla fine di uno spezzone, il *trackpad* [lo fa sentire fisicamente al dito](http://alex4d.com/notes/item/bumpy-pixels-future-haptic-apple-force-touch-trackpad). E molto altro, non solo in iMovie: c’è già una [nota tecnica con l’elenco](https://support.apple.com/en-us/HT204352).

Innovazione che passa quasi inosservata, specie di fronte agli annunci roboanti sulla realtà virtuale, e cambia davvero il modo di usare il computer.

Quale potrebbe essere il prossimo momento *wow!* di Apple? Personalmente non guardo a un prodotto, ma a una tecnologia. La mia sensazione è che nessun’altra azienda possa sperare di risolvere il problema *e* farlo anche in modo da ottenere approvazione, accettazione e adozione.

Il problema è avere un [tempo di rete](https://tools.ietf.org/html/rfc3977) estremamente preciso, a livello di orologio atomico. Oggi [non lo è](http://nvlpubs.nist.gov/nistpubs/TechnicalNotes/NIST.TN.1867.pdf), anche se ce ne accorgiamo poco o nulla; domani (non remoto, domani inteso come domani) un qualche strumento chirurgico robotizzato chiederà una sincronizzazione con suoi colleghi che oggi è impossibile ottenere ed è solo uno dei numerosi esempi possibili (altro? Le auto senza pilota per esempio, che devono avere tempi di reazione infinitesimali e necessitano di precisione temporale tendente alla perfezione).

Apple può essere interessata a questo problema più che in passato perché adesso si mette a costruire orologi (computer da polso; ma ho capito dopo iPhone che certe discussioni sono inutili e non voglio insistere). E se il computer ha potuto finora permettersi di avere tempo preciso relativamente alla percezione umana, ma non in assoluto, gli orologi hanno aspirazioni superiori. Nella pagina di [presentazione di watch](https://www.apple.com/it/watch/timekeeping/) si menziona la precisione della funzione orologio: la pagina di Mac o di iPad non ne ha bisogno.

Ecco: se Apple venisse fuori con una soluzione delle sue – non una qualunque – per sistemare il problema del tempo di rete, avrebbe fatto qualcosa di paragonabile al primo Macintosh.