---
title: "Radunanza"
date: 2017-01-21
comments: true
tags: [CoC, Telegram, bot, BBEdit, NewLisp]
---
Scrivo reduce dall’epica adunanza del [clan](http://supercell.com/en/games/clashofclans/) Golden Eagle (#yv2uvg99) in un locale di Milano scelto apposta per non destare sospetti e per la bontà delle [striatelle](https://facebook.com/lestriatelle/).

Sono stato essenzialmente coartato nell’aggiornare il mio municipio immediatamente, invece che attendere altri sei giorni, quindi raggiungerò un livello eccessivo per le mie capacità in anticipo rispetto al previsto. Chi voglia compiere un’opera buona e compensare la mia presenza si faccia vivo citando questo blog. Male che vada, può dare la colpa a me.

Prima, durante e dopo l’adunanza ho radunato alcune informazioni di minima importanza, che riassumo in forma criptica per salvaguardare alcune *privacy*, sintetica perché la birra era molto buona e sono poco concentrato, olistica nel mescolare i temi.

* Il migliore *provider* telefonico per chi trascorre lunghi periodi di lavoro all’estero inframmezzati da brevi pause in Italia, è Tre.
* Il randomizzatore di righe [scritto in NewLisp](http://git.net/ml/editors.bbedit.general/2008-01/msg00088.html) è molto più elegante di quello classico di BBEdit, [scritto in Perl](http://bbedit-talk.barebones.narkive.com/jXrdz0T6/randomize-lines). (Anche se L’efficienza vuole l’installazione di [coreutils](https://www.gnu.org/software/coreutils/coreutils.html))
* I *bot* di [Telegram](https://telegram.org/) sono un mondo da scoprire.
* Melanzane e cioccolato sono un accostamento intrigante.
* [Packt](https://www.packtpub.com/) offre sempre un eBook gratis così come iTunes una *app* ed è possibile accumulare serendipiticamente biblioteche significative, più utili di quello che sembra a prima vista. (Grazie [Sabino](https://melabit.wordpress.com/)!)

<a href="https://www.clashofstats.com/clans/golden-eagle-YV2UVG99/members"><img src="https://www.clashofstats.com/signatures/YV2UVG99?lng=en&color=orange&size=medium"/></a>
