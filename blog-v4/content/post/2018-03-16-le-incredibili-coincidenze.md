---
title: "Le incredibili coincidenze"
date: 2018-03-16
comments: true
tags: [Hawking, Einstein, Matrix, Gardner, Pi]
---
In giro per i media si trova una specie di commentatori diffusa, che passa la giornata a pubblicare foto del pranzo o della sala di attesa dell’aeroporto, per non dire dell’animale di compagnia e, appena muore una personalità in vista, diventa letterato se letterato era il morto. Oppure scienziato, o attivista, musicista, bibliofilo, teatrante.<!--more-->

Così [muore Stephen Hawking](https://arstechnica.com/science/2018/03/stephen-hawking-legendary-theoretical-physicist-dies-at-76/) e il commentatore si tramuta in astrofisico di frontiera, forte – nei casi migliori – della lettura di [un libro divulgativo](https://www.amazon.it/dp/B076PNRTMB/ref=dp-kindle-redirect?_encoding=UTF8&btkr=1).

E ci illumina. Per esempio, meraviglia! sulle [incredibili coincidenze](http://www.repubblica.it/scienze/2018/03/14/news/stephen_hawking_coincidenze-191244090/) della vita e della morte di Hawking, nato il giorno della morte di Galileo, morto quando è nato Einstein, addirittura titolare della stessa cattedra di matematica di Cambridge che appartenne a Isaac Newton. Titolo che immagino vantino [dozzine di matematici](https://en.wikipedia.org/wiki/List_of_Cambridge_mathematicians).

Aggiungiamo che Hawking è morto il 14 marzo, il [Pi Day](https://fivethirtyeight.com/features/even-after-22-trillion-digits-were-still-no-closer-to-the-end-of-pi/), uno dei prodotti migliori dell’industria del pretesto per distillare sapienza sulle reti sociali, e abbiamo la tempesta perfetta, degna di rivaleggiare con qualche fattoide sulle Grandi Piramidi, tipo [l’equivalenza delle coordinate geografiche di Giza](https://ancient-code.com/eery-coincidence-the-constant-of-speed-of-light-equals-the-coordinates-of-the-great-pyramid-of-giza/) con il valore della velocità della luce.

Hawking stesso amava commentare la coincidenza del suo compleanno con la morte di Galileo, da uomo di grande spirito, ben conscio che l’anno ha trecentosessantacinque giorni da qualche secolo e qualsiasi pasticciere è nato o morto in coincidenza con un grande pasticciere del passato; basta cercare abbastanza a lungo. Ah, se qualcuno conoscesse le [incredibili similitudini tra la vita di Kennedy e quella di Lincoln](https://www.snopes.com/fact-check/linkin-kennedy/).

Stupisce semmai che nessuno degli intellettuali in quota Facebook abbia sfoderato Martin Gardner e il suo Irving Joshua Matrix, [maestro delle coincidenze numeriche](http://www.logic-books.info/sites/default/files/k09-the_magic_numbers_of_dr._matrix_0.pdf) e geometriche più esoteriche:

>Le date importanti non sono mai accidentali. L’era dell’atomo iniziò nel 1942, quando Enrico Fermi ottenne la prima reazione nucleare a catena. […] Arthur Compton telefonò a James Conant per comunicare la notizia, con le parole “Il navigatore italiano ha raggiunto il Nuovo Mondo”. Mai notato che invertendo i numeri in mezzo alla data 1942 si ottiene 1492, l’anno della scoperta dell’America? […] C’è di più. In quel pomeriggio del 2 dicembre 1942 le persone presenti quando Fermi annunciò la sostenibilità della reazione atomica erano esattamente quarantadue.

Gardner si faceva elegantemente beffe di chi studia enigmisticamente la Bibbia alla ricerca di [codici segreti](https://www.csicop.org/si/show/hidden_messages_and_the_bible_code) o estrae altrimenti dall’infinito un qualsivoglia mazzetto di coincidenze specifiche. Tipo quelle su Stephen Hawking.

Gente che sembrerebbe meno ridicola, che so, se il giorno della [morte di Martin Gardner](http://www.nytimes.com/2010/05/24/us/24gardner.html) avesse tirato in ballo l’[equazione di Hawking](http://www.iflscience.com/physics/stephen-hawking-asked-to-have-his-most-famous-equation-on-his-tombstone/). Notato come inizi per S e finisca per A, le iniziali di *Scientific American*, la rivista su cui Gardner per decenni ha pubblicato i suoi giochi matematici? Straordinario, vero?