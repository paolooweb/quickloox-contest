---
title: "Chi offre di più"
date: 2021-09-08T00:53:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [Jon Prosser, iPhone 14] 
---
Con tempismo perfetto, una settimana prima dell’evento di presentazione del prossimo iPhone (più varie ed eventuali) compare una paginaccia rigonfia di pubblicità dedicata a supposte [rese grafiche di… iPhone 14](https://www.frontpagetech.com/2021/09/08/exclusive-a-closer-first-look-at-iphone-14/). Il 2022.

Ci sono due possibilità: Jon Prosser ha avuto una idea brillante per autopromuoversi sfruttando il volano di Apple. Oppure qualcuno lo ha pagato per fare contropubblicità a un evento che chiaramente non fa felici i reparti marketing di diverse società.

Che gusto se Tim Cook annunciasse iPhone _12s_.

Ma ancora di più se lo chiamasse iPhone 15. Non sarebbe nemmeno la prima volta che salta un numero (non è mai esistito un iPhone 9, né un iPhone 2).

*Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile.*