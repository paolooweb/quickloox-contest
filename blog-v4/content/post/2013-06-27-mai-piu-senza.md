---
title: "Mai più senza"
date: 2013-06-26
comments: true
tags: [Font, Terminale, Mac]
---
*Mac OS X Hints* mi ha regalato il modo per [impedire via Terminale che Mac vada in sonno](http://hints.macworld.com/article.php?story=20130620032443758). Certo, ci sono le Preferenze di Sistema, c’è QuickSilver, ma con una finestra di Terminale aperta la cosa è molto più veloce e pratica. E io ho *sempre* una finestra di Terminale aperta:

`pmset noidle`

*Control-C* per azzerare l’effetto-insonnia.<!--more-->

E poi [quarantacinque font gratis](https://itunes.apple.com/ca/app/free-fonts-commercial-use/id647697434?mt=12) reperibili con un clic da Mac App Store e utilizzabili in qualunque progetto personale o professionale. Abbiamo in mano strumenti di creatività straordinari; almeno una volta al mese dovremmo come minimo provare un esperimento, su una paginetta, un disegnino, un programmuccio. Usciamo dal seminato, un’ora al mese.

Infine, [Streaming Foto](http://www.apple.com/it/icloud/features/photo-stream.html). È uno strumento di una potenza inaudita rispetto alla facilità di utilizzo. Banalità, mi si dirà, lo sapevamo già tutti. Sì, banalità pura. Eppure in questi giorni mi sta cambiando un discreto pezzo di vita per la possibilità di informare, essere informato, commentare e leggere commenti, senza il baccano del social network e duecento notifiche da venditori del tutto. Non sono tante le banalità che cambiano un pezzo di vita.