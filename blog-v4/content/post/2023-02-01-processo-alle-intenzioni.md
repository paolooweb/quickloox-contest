---
title: "Processo alle intenzioni"
date: 2023-02-01T02:51:45+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [Csam]
---
Si può girare intorno alla questione dei metodi usati per individuare foto improprie sul computer di chi le detiene, per anni. Apple ha fatto la sua parte con i piani di scansione automatica degli apparecchi alla ricerca di immagini riscontrate nei database Csam di pedopornografia alimentati da forze dell’ordine e organizzazioni no profit. C’è stato un grosso movimento di opinione contrario e il risultato è che oggi Apple ha portato la cifratura end-to-end su quasi tutti i propri servizi iCloud.

In un certo senso è un bene che i piani delle iniziative imperniate su Csam siano stati abbandonati; così abbiamo avuto la cifratura end-to-end, molto più interessante per il novantanove virgola novantanove percento di noi, anche se renderà la vita più facile a qualche criminale.

Un amante delle teorie cospiratorie potrebbe anche sostenere che magari Apple ha fatto *apposta* a tirare fuori Csam, per farsi attaccare e così mostrare alle autorità – mal disposte verso le cifrature troppo robuste – di non avere altra alternativa.

Un altro amante delle teorie cospiratorie potrebbe sostenere che Apple analizza ugualmente le immagini in arrivo sui Mac, nonostante tutto. È vero o no che esiste in macOS un processo [mediaanalysisd](https://mjtsai.com/blog/2023/01/25/network-connections-from-mediaanalysisd/) e che questo si attiva in presenza di immagini nuove, e che comunica con i server Apple? Non è che Csam è rientrato dalla finestra?

Tutto vero, ma Csam c’entra niente; [è il modo in cui Mac fa funzionare Live Text](https://eclecticlight.co/2023/01/18/is-apple-checking-images-we-view-in-the-finder/), la funzione che individua il testo all’interno delle foto e permette di copiarlo e incollarlo altrove.

Forse l’unica domanda che valga la pena di porsi veramente è quali siano le reali intenzioni di Apple.

Viene da dire: [se è così irretita dall’idea del capitalismo di sorveglianza](https://macintelligence.org/posts/2021-08-28-privacy-e-realtà-un-attacco-omologato-e-superficiale/), perché introdurre cifratura totale sui dati degli utenti?

Alla fine Apple non è come Google o [Microsoft](https://macintelligence.org/posts/2022-08-10-sulle-tracce-dellincoscienza/): *non ricava vantaggio economico dall’acquisizione di dati riguardanti chi usa Mac*. Rozzamente, ma concretamente, è ora di scegliere altri bersagli. Il processo `mediaanalysisd` è innocente.