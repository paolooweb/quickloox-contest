---
title: "Chiavi in mano"
date: 2016-01-16
comments: true
tags: [Apple, Gruber, iMessage, privacy, Cook, Obama, backdoor, cifratura, Zuckerberg, Nadella, Dorsey, Page, UE]
---
Il Senato dello Stato americano di New York [ha allo studio una norma](http://www.nysenate.gov/legislation/bills/2015/a8093) che se approvata potrebbe portare a multe per chi fabbrica apparecchi i cui dati non possano essere sbloccati o decrittati da parte di entità diverse dal proprietario dei dati stessi.<!--more-->

È solo l’ultima di una serie di azioni di istituzioni varie tese a minare la sicurezza e la *privacy* delle informazioni che vengono cifrate da sistemi come iMessage o comunque risiedono in forma cifrata sull’apparecchio dove sono state create.

Tim Cook, amministratore delegato di Apple, [ha chiesto con forza all’amministrazione Obama](https://theintercept.com/2016/01/12/apples-tim-cook-lashes-out-at-white-house-officials-for-being-wishy-washy-on-encryption/) di difendere l’uso di cifratura vera e seria, ossia non decrittabile. Il governo americano tentenna perché è tentatissimo dall’idea di imporre per legge *backdoor* riservate al governo (chiavi-scorciatoia per annullare la cifratura), senza capire che criminali, terroristi e governi stranieri ostili le scoverebbero in cinque minuti (forzatura dialettica; magari cinque mesi. Ma *certamente* le scoverebbero).

Apple ha un ruolo particolare in questa diatriba. Come [scrive John Gruber](http://daringfireball.net/linked/2016/01/15/cook-encryption),

>Tim Cook ha ragione e tutti gli esperti di privacy e cifratura gli danno ragione, ma sono gli altri leader delle principali aziende americane? Dov’è Larry Page? Satya Nadella? Mark Zuckerberg? Jack Dorsey? [C’è un tale silenzio che] sento frinire i grilli.

(Google, Microsoft, Facebook, Twitter).

Oramai più di un anno fa, mi si perdonerà l’autocitazione, scrivevo di [nazioni contro aziende](http://www.apogeonline.com/webzine/2014/10/22/nazioni-contro-aziende) e commentavo *manca solo il casus belli*.

Ancora qualche tempo; sarà la cifratura magari, o la tassazione. Sarà un caso che l’Unione Europea [ha messo sotto inchiesta](http://www.irishtimes.com/business/technology/apple-to-pay-318m-to-settle-italian-tax-fraud-case-1.2480300) la struttura finanziaria delle grandi multinazionali proprio mentre queste ultime danno fastidio per come propagano cifratura forte nelle mani di milioni di cittadini?