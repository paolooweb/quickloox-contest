---
title: "L’ascolto e la scoperta"
date: 2021-02-17T03:59:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [Radio.Garden] 
---
Il vero spirito di Internet, quello di una volta per chi si senta addosso la voglia di fare nostalgia, si ritrova un [Radio.Garden](http://radio.garden), sito meraviglio per *sense of wonder*, poesia, coefficiente di riutilizzo.

Un mappamondo e tanti puntini. Ogni puntino è una radio. Il clic sul puntino… non c’è bisogno di spiegare.

Possibilità infinita di sorprese, serendipità a mille, il gusto di uscire dalla *comfort zone* per sentire che cosa accadrà. Meno social, più siti di scoperta e recupero della meraviglia. È per questo che è nata la rete.

*Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile.*