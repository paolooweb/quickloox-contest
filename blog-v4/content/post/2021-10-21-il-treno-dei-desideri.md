---
title: "Il treno dei desideri"
date: 2021-10-21T00:22:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [Intel, M1, Steve Jobs, Tim Cook, PowerPC, x86, Arm, Apple Silicon] 
---
Sedici anni fa Steve Jobs annunciò [il passaggio di Apple ai processori Intel](https://www.cultofmac.com/484394/apple-intel-over-powerpc/), visto che l’architettura PowerPC non riusciva a tenere il passo con quella x86.

Un anno fa Tim Cook ha annunciato [il passaggio di Apple ai processori Apple Silicon](https://www.cultofmac.com/714845/apple-silicon-arm-based-mac-coming-in-2020/), dal momento che l’architettura x86 non tiene il passo con quella Arm.

Tre giorni fa l’amministratore delegato di Intel ha annunciato che [spera di riuscire a costruire i chip Apple Silicon per conto di Apple](https://www.barrons.com/articles/intels-ceo-wants-to-win-back-apple-new-macbook-pro-chips-show-why-its-easier-said-than-done-51634595554).

Anche a questo scopo, Intel ha stanziato venti miliardi di dollari per costruire due fabbriche di semiconduttori negli Stati Uniti.

Quando saranno finite, è probabile che Apple sia già a M3.

Intel è l’azienda che [declinò la proposta di Apple per produrre il processore di iPhone](https://www.theatlantic.com/technology/archive/2013/05/paul-otellinis-intel-can-the-company-that-built-the-future-survive-it/275825/).

I treni decisivi passano poche volte nella vita e chissà se la stazione di Intel ne vedrà ancora.

*Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile.*