---
title: "Un tocco di antico"
date: 2013-11-23
comments: true
tags: [Unicomp, Ibm, tastiere]
---
Per quanto a mio agio con le attuali tastiere Apple, da anni all’altezza della situazione (non è sempre stato così, nell’interregno dei [PowerBook Duo](http://www.everymac.com/systems/apple/powerbook_duo/index-powerbook-duo.html) per esempio), sento ancora un pizzicore di invidia di fronte al *setup* [mostrato su *Ars Technica*](http://arstechnica.com/gadgets/2013/11/why-i-use-a-20-year-old-ibm-model-m-keyboard/) da Iljitsch van Beijnum: triplo schermo a parte, una solida e impeccabile tastiera modello M di Ibm, vent’anni di vita e tuttora performante.<!--more-->

Per avere lo stesso *feeling* con un prodotto moderno a prezzo accessibile credo che l’unica soluzione sia la [Spacesaver M](http://pckeyboard.com/page/category/SpacesaverM) di Unicomp.

Altrimenti, sempre la [Model S Ultimate Keyboard di Das Keyboard](https://macintelligence.org/posts/2012-05-29-toccare-il-sogno/), magari in versione con tasti ciechi. Lo so che [lo avevo già ripetuto](https://macintelligence.org/posts/2013-05-16-un-sogno-che-continua/).