---
title: "I codici della pittura"
date: 2019-12-06
comments: true
tags: [Word, Office, PowerPoint, Excel, Css, Smith, Colossal]
---
Rispetto al dibattito sugli [strumenti da usare nella scuola primaria](http://www.macintelligence.org/blog/2019/11/29/tre-cose-da-imparare/) per introdurre i bambini al *coding* e prepararli al lavoro del futuro, ho scoperto su *Colossal* che Diana Adrianne Smith ha l’hobby di [dipingere con Css](https://www.thisiscolossal.com/2019/11/css-portraits-diana-smith/).

Si possono vedere altre creazioni sul suo [sito personale](https://diana-adrianne.com).

Diana è una *UI Engineer*, tutto tranne che una artista pura. Ma evidentemente si è trovata bene al famoso [incrocio tra tecnologia e arti liberali](https://macintelligence.org/posts/2019-09-11-come-e-umana-lei/).

Dov’è che entra in ballo la scuola? [Css](https://www.w3.org/Style/CSS/Overview.en.html) è un sistema di marcatura del testo. Praticamente Diana scrive istruzioni che, tradotte in linguaggio naturale, sono cose tipo *prendi un cerchio, deformalo in questa direzione, colora il bordo di giallo e l’interno di verde, fai il bordo spesso così e metti tutto in questa posizione*. È il sistema per assegnare stili grafici e tipografici ai siti web.

*Chissà che programma usa. Lo insegniamo ai ragazzi?*

Diana *digita ogni istruzione in un editor di testo*. Niente programmi, niente librerie, niente automazioni, niente di niente.

L’ultimo dei suoi ritratti ha richiesto [due fine settimana](https://twitter.com/cyanharlow/status/1191264717706149890) di lavoro.

<blockquote class="twitter-tweet"><p lang="en" dir="ltr">Did another CSS-only art.<br>Flemish/baroque inspired.<br>Two weekends. Made for Chrome. <a href="https://t.co/d4Z9kkvu1R">pic.twitter.com/d4Z9kkvu1R</a></p>&mdash; Diana Smith (@cyanharlow) <a href="https://twitter.com/cyanharlow/status/1191264717706149890?ref_src=twsrc%5Etfw">November 4, 2019</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

Non serve imparare alcun programma – Ok, un editor di testo – per arrivare a dipingere con Css. In compenso scommetto che Adrianne padroneggia perfettamente la tecnologia e non avrà grossi problemi a trovare datori di lavoro. Dove, immagino, userà un fior di programma specializzato per fare prima.

A scuola insegneresti questo programma o li faresti lavorare come Diana nei suoi weekend? Vedresti possibile impegnare i bambini per un’ora a settimana e portarli a fine anno a presentare una galleria di ritratti, fieri e orgogliosi di quanto hanno fatto, e oltretutto perfettamente consci di che cosa significa grafica, stile, Html? O sarebbe meglio dargli in mano un programma, ce ne sono millanta, per dire loro *clicca sull’icona del cerchietto per fare un cerchietto*?

O gli *insegni* Microsoft Paint, così stanno buoni e non consumano carta e pennarelli?

Non ho dubbi, ma ascolto molto volentieri ogni parere e ringrazio in anticipo.