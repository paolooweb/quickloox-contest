---
title: "Sbagliando si impara"
date: 2015-04-23
comments: true
tags: [watch, Bluetooth]
---
Ho trovato istruttivo l’articolo [Il mio più grosso errore con WatchKit](http://realm.io/news/watchkit-mistakes/): programmatori alle prese con watch spiegano che cosa hanno sbagliato, o che cosa hanno imparato (che in questo campo è quasi la stessa cosa).<!--more-->

Si va dal sottovalutare i problemi di connessione con Bluetooth al pensare al testo senza cornici (dal momento che i bordi dello schermo provvedono da soli), con una sottolineatura del come sia meglio collaudare senza troppa fretta di uscire a tutti i costi senza avere una cosa che funziona veramente come deve.

Perché lo schermo piccolissimo di un computer da polso non perdona, neanche il minimo errore di *design*. E perché, nonostante sia piccolissimo, consente possibilità di espressione notevoli per un’*app* che trova lo spazio e la funzione giusta.

Che cosa mettere sullo schermo di una cosa grande come un orologio non è affatto scontato. Stai a vedere che questo watch diventa un fenomeno interessante.