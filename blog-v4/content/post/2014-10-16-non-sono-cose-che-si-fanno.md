---
title: "Non sono cose che si fanno"
date: 2014-10-16
comments: true
tags: [Gand, Freesmug]
---
Arrivo a un computer solo ora! Mi trovo con chi vuole su Irc, server *freenode*, canale *#freesmug* e ringrazio come sempre [Gand](http://freesmug.org) per l’ospitalità.

Ordine del giorno: l’evento Apple di… adesso.

Mai dimenticarsi – parlando di Freesmug – di dare un contributo all’*open source*, nella forma ritenuta più opportuna.