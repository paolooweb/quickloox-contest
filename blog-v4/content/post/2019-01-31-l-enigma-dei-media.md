---
title: "L’enigma dei media"
date: 2019-01-31
comments: true
tags: [iPhone, Anandtech, Android]
---
Un amico, titolare di una azienda di software per apparecchi da tasca, mi dice che *iPhone non può eseguire più di due media contemporaneamente* e che la cosa deriva dall’incapacità del processore di fare fronte al compito. La cosa sarebbe *documentata*.

Non ho motivo particolare di dubitare della sua parola, se non per avere sempre letto giudizi molto positivi sui processori di iPhone e iPad, ultimo in ordine di tempo [quello di Anandtech](https://www.anandtech.com/show/13392/the-iphone-xs-xs-max-review-unveiling-the-silicon-secrets).

>A12 è un *System on Chip* bestiale. Se batteva già la concorrenza in prestazioni ed efficienza dei consumi, A12 rilancia […] Le CPU di Apple sono così performanti da essere marginalmente dietro i migliori processori da scrivania […] Parlamdo di processori grafici, i guadagni prestazionali corrispondono alle promesse e le superano persino, in termini di prestazioni nel tempo.

Ho provato a curiosare frettolosamente nella documentazione per sviluppatori e sui principali siti di programmazione, senza esito.

Mancia competente a chi sapesse illuminarmi.
