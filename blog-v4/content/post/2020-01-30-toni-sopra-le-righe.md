---
title: "Toni sopra le righe"
date: 2020-01-30
comments: true
tags: [Eilish, Young, Grammy, MacBook, Pro, GarageBand, Logic]
---
La cantante Billie Eilish [ha appena vinto quattro premi Grammy](https://www.engadget.com/2020/01/27/billie-eilish-grammy-2020-bedroom-studio-logic-pro-x/) e stabilito un paio di record per essere la più giovane ad averlo fatto.

Il pezzo di *Engadget* diffonde dettagli sul *setup* casalingo usato da lei e dal fratello per realizzare i differenti livelli di suoni e strumenti da inviare all’ingegnere del suono per il montaggio finale dell’album. Si tratta di hardware e software per un migliaio di dollari, compreso [Logic Pro X](https://www.apple.com/it/logic-pro/).

In altre parole, anche se *Engadget* non lo scrive, Billie Eilish ha usato un Mac, non compreso nella lista della spesa.

Neil Young si è fatto meno problemi a nominare Mac in una intervista a *The Verge*, per [definire l’audio di MacBook Pro](https://www.theverge.com/2020/1/28/21091655/neil-young-podcast-vergecast-interview-phil-baker-book-pono-hi-res-audio) di qualità *Fisher-Price*, giocattolo.

Chi dei due la spara più grossa?

Voto Young, per quanto sia fan del suo approccio alla [diffusione della sua musica sul web](https://macintelligence.org/posts/2017-12-02-un-grande-passo-per-la-musica/) e solidarizzi con diverse delle sue idee sulla qualità generale dell’audio ai giorni nostri. È molto facile che, grazie alle attrezzature che qualsiasi ragazzo non povero in canna può mettersi in cameretta con modesti sacrifici, il ragazzo in questione decida di produrre musica di qualità audio inferiore alla media che fu, e questo è un tema.

Da qui a stroncare MacBook Pro in fatto di qualità audio ce ne corre, se non altro perché [MacBook Pro possiede il miglior sottosistema audio mai visto](https://macintelligence.org/posts/2019-11-25-una-buona-scusa/), no, sentito in un portatile.