---
title: "Pensieri chiaramente oziosi"
date: 2019-08-14
comments: true
tags: [Brogue, Shortcuts, Ferragosto]
---
Ci si avvicina pericolosamente a Ferragosto e mi avvicino innocuamente con la famiglia all’estremo nordest italiano, per raggiungere un docente universitario che tanto tempo fa ma è una lunga storia. Non c’è da stupirsi che scriva dal letto di un alberghino apprezzato con iPad Pro e che per la testa girino riflessioni non proprio profonde. Del tipo.

Vorrei riuscire a vincere [Brogue](https://sites.google.com/site/broguegame/) prima di settembre. Solo che è una bestia difficile da domare, almeno per me. Gira anche [su iOS](https://apps.apple.com/it/app/brogue/id613921309?l=en).

Per divertimento vorrei scrivere un Comando rapido iOS che risolve una esigenza di lavoro: prende una foto di dimensioni arbitrarie e la porta esattamente a 600 x 513 pixel, senza deformarla. Significa che il programma ridimensiona la foto a 600 oppure 513 pixel in funzione del suo formato e poi taglia simmetricamente i pixel in eccesso dove serve, destra-sinistra oppure alto-basso. Ho chiaro in mente il workflow, però devo concretizzarlo. Intanto mi leggo il [manuale dei Comandi rapidi](https://support.apple.com/guide/shortcuts/welcome/ios), per la serie delle letture improbabili estive.

Ogni giorno che passa vedo che [Editorial](http://omz-software.com/editorial/) è sempre più obsoleto e gli aggiungo una personalizzazione in più, che me lo rende più efficace. Ole Zorn, l’autore, ha creato un meccanismo diabolico e non mi sono ancora deciso a cambiare editor di testo su iPad.

Insomma, è ozio.
