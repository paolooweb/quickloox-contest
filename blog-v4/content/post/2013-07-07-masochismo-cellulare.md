---
title: "Masochismo cellulare"
date: 2013-07-07
comments: true
tags: [iPhone]
---
Da domani si ritorna a parlare anche un po’ di Mac, perché insomma. Però non si poteva restare indifferenti alla grande novità: iPhone 5 è il telefono intelligente più odiato. Invece Galaxy S4 è quello più amato.<!--more-->

[Lo scrive](http://www.dailymail.co.uk/sciencetech/article-2355833/Apples-iPhone-5-hated-handset--majority-people-love-Samsung-Galaxy-S4-study-finds.html) l’autorevolissimo *Mail Online*.

I risultati si devono al lavoro della famosissima [We Are Social](http://wearesocial.com), azienda che avrebbe scandagliato le reti sociali alla ricerca delle conversazioni in rete relative a cinque *smartphone* in voga, per poi sommare i commenti favorevoli e quelli invece negativi.

Philip Elmer-Dewitt su Apple 2.0 [spiega](http://tech.fortune.cnn.com/2013/07/06/apple-most-hated-smartphone/) di avere cercato lo studio sul sito di We Are Social, ma di non averlo trovato. E di avere chiamato l’ufficio di New York senza avere risposta, nonché senza la possibilità di lasciare un messaggio in segreteria.

Tutta la sostanza a disposizione per tirare conclusioni è l’articolo su Mail Online. Un riquadro riassume dati tipo il numero di conversazioni registrate in occasione del lancio di iPhone 5 (1,7 milioni) e di Galaxy S4 (140 mila), oppure i commenti negativi su iPhone 5 (20 percento) e su Galaxy S4 (11 percento).

Roba grossa, insomma. Si noti di passaggio che i lanci dei telefoni sono avvenuti in momenti diversi (settembre, marzo) e che iPhone 5 aveva un nuovo connettore, argomento più che sufficiente a fare assumere un tono critico a un eventuale chiacchierista (esperto di chiacchiere) improvvisamente bisognoso di un adattatore.

Da nessuna parte si vede una nozione di *più amato* o *più odiato* se non nel titolo del Mail Online. Il quale, come testimonia la colonna laterale di *gossip* su [Uma Thurman](http://www.dailymail.co.uk/tvshowbiz/article-2357467/Uma-Thurman-43-defies-age-tiny-piece-boats-Saint-Tropez.html) o [Victoria Beckham](http://www.dailymail.co.uk/tvshowbiz/article-2357256/Victoria-Beckham-opens-working-mother-poses-flawless-shoot-Vogue-China.html), non si fa problemi a buttarla sullo scandalistico.

Nel frattempo, perlomeno a febbraio, l’odiato iPhone 5 risultava per PCMag.com [il telefono intelligente più venduto al mondo](http://www.pcmag.com/article2/0,2817,2415622,00.asp).

La gente ama farsi male.