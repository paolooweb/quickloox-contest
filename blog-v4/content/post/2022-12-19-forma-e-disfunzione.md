---
title: "Forma e disfunzione"
date: 2022-12-19T15:24:34+01:00
draft: false
toc: false
comments: true
categories: [Internet]
tags: [Poste]
---
Le feste di Natale mi hanno regalato due iconici momenti-depressione alle Poste, uno Alla Ricerca del Pacco Perduto e un altro a ritirare la Raccomandata non Consegnata perché il Postino non ha perso tempo a suonare al citofono.

Non entro nei dettagli di qualcosa noto a tutti. Mentre si svolgevano le due agonie, nella contemplazione di una distorsione spaziotemporale votata alla lentezza, riflettevo sulla componente tecnologica. Oggi l’ufficio postale ne è pieno. Terminali, firma sullo schermo, scansioni, stampanti, archivi elettronici, codici a barre eccetera.

L’ufficio postale odierno è pieno di tecnologia; eppure funziona male esattamente come quando tutto era carta, come avrebbe potuto dire qualche filosofo greco prearistotelico.

Forse la tecnologia non funziona? No. La tecnologia è uno strumento. I difetti stanno nella malaorganizzazione, nella scarsa motivazione, nelle procedure bizantine, nell’arretratezza di pensiero ove non ci sia quella della strumentazione. Tutte queste cose sono rimaste uguali a prima e sono queste a creare l’esperienza-purgatorio.

Ecco perché bisogna smettere subito di parlare di informatizzazione e cominciare a martellare seriamente, chi ne sa e chi lo sa fare, di trasformazione digitale.

Un posto disfunzionale, infuso di tecnologia senza toccare le disfunzionalità, rimane disfunzionale.