---
title: "Multisensing"
date: 2021-11-29T00:50:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [Foundation, Safari, Drafts] 
---
Un iPad e un tecnoumanista stagionato, a letto, prima che arrivi il sonno.

Lo schermo è diviso in due finestre, una per scrivere su [Drafts](https://getdrafts.com) e una per leggere da Safari. Sopra alle due finestre, in modalità picture-in-picture, si svolge una puntata di [Foundation](https://tv.apple.com/it/show/foundation/umc.cmc.5983fipzqbicvrve6jdfep4x3).

Sembrerebbe multitasking, ma è un’altra cosa. _Multisensing_: tatto, vista, udito insieme in un contesto assai differente da quello in cui tanti sono cresciuti. Dove il dovere era separato dal piacere, il lavoro dall’intrattenimento, i contenuti venivano creati o fruiti isolatamente, in microambienti dedicati, o in macroapparecchi specifici.

Giusto ieri sentivo una docente ricordare che il comitato tecnico-scientifico della sua scuola riteneva più opportuna la consultazione dei libri sulla carta e quindi, per il momento, non permettere l’uso di tablet in aula per l’utilizzo di libri digitali.

Bambini di sei anni, con un futuro che non sarà quello degli adulti di sessanta.

_Per ora si può lasciare un commento dalla [pagina apposita di Muut per Qui
ckLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile._