---
title: "Le mille bolle ordinate"
date: 2021-09-12T01:13:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [Bubble sort, Octopress, Jekyll, Coleslaw] 
---
Da questa estate il blog si generava regolarmente, ma non prima di avere sputato esattamente trentasette righe di messaggi di errore.

Ci sono già fin troppe cose da sistemare e così ho provato a leggere attentamente i messaggi: la colpa era di un *octet* che il motore di [Coleslaw](https://github.com/coleslaw-org/coleslaw) non riusciva a leggere.

Due numeri tipo 105 e 92, di cui non avevo idea. Ma l’esperienza accumulata con [Octopress](http://octopress.org) e [Jekyll](https://jekyllrb.com) mi ha insegnato che spesso questo tipo di errori nasce da un carattere non ortodosso (per chi ha scritto il motore) presente nelle zone del file originale che vengono interpretate dal motore stesso per preparare la struttura del sito.

Così avevo solo duecentoquarantotto articoli in cui cercare un carattere strano dentro le intestazioni. Non incoraggiante.

Per fortuna mi sono ricordato di [Bubble Sort](https://www.geeksforgeeks.org/bubble-sort/), un algoritmo che ordina una serie di numeri confrontando in continuazione due di essi fino a quando tutti i confronti di coppia possibili sono stati eseguiti e il file è stato ordinato.

Qui non dovevo ordinare ma compiere piuttosto una *bubble search*: confrontare coppie e vedere in quale elemento della coppia stava l’errore, per arrivare a eliminare tutti i post in ordine e restare con il colpevole in mano. L’idea sottostante è comunque la stessa, cambia solo lo svolgimento.

Ho preso centoventiquattro post e li ho levati dal motore, poi ho provato a generare il blog. Niente errore; quindi il problema stava da qualche parte nei centoventiquattro file eliminati.

Se avessi visto l’errore, avrei dovuto invertire la scelta; mettere nel motore i centoventiquattro appena levati e togliere quelli che c’erano, per riprovare a mo’ di conferma.

Invece ero sulla strada giusta. Ho diviso i centoventiquattro in due gruppi da sessantadue e ho ripetuto la prova. Il gruppo che dava errore è stato ulteriormente diviso in due metà da trentuno; i trentuno indiziati sono diventati due gruppi da sedici e quindici e così via.

In un quarto d’ora circa ho isolato un maledetto post del trenta luglio con una È nel titolo, cioè in uno dei campi soggetti ad analisi da parte del motore.

Senza il *bubble sort*, avrei dovuto ingegnarmi maggiormente. E ciò dimostra quanto sia importante leggere i libri di coding destinati, per quando ne avranno voglia, alle figlie. Mettere in ordine non riguarda solo la stanza dei giochi.

*Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile.*