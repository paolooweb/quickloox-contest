---
title: "Tocchi e rintocchi"
date: 2022-01-05T01:55:23+01:00
draft: false
toc: false
comments: true
categories:
tags:
---
Come passa in fretta il tempo quando si digita: la mia veneranda [Apple Wireless Keyboard](https://cdn.cnetcontent.com/1c/27/1c27e2b6-8c58-410c-850e-d3ef0e912494.pdf) compie dodici anni e me ne sono reso conto solo ora che non si abbina – non in modo ovvio, almeno – al mio nuovo [Mac mini](https://www.apple.com/it/shop/buy-mac/mac-mini/chip-apple-m1-con-cpu-8-core-e-gpu-8-core-512gb).

Neanche mi sogno di buttarla via; funziona egregiamente con iPad Pro e con il vecchio Mac mini, che probabilmente diventerà macchina di appoggio per la primogenita quando il prossimo autunno si affaccerà sulla terza elementare. Dire che in questi anni sia stata quasi una prostesi un’estensione delle dita delle mani, è sottovalutare il suo ruolo. Potrei averci digitato tipo cinque milioni di caratteri, con amplissimo margine di errore.

Ecco perché rivolgo un complimento implicito alla [Magic Keyboard](https://www.apple.com/it/shop/product/MK2A3T/A/magic-keyboard-italiano) con cui digito in questo istante. Perché mi sono adeguato al cambiamento molto, molto in fretta.

Le dimensioni complessiva della tastiera sono inferiori, però l’impronta dei tasti e la loro distanza reciproca mi sembra invariata o molto vicina a quella precedente; la memoria dei tasti, per cui appoggio le dita e inizio a scrivere guardando solo lo schermo e quasi mai la tastiera, è fondamentalmente invariata.

L’unica funzione veramente aliena rispetto a prima è la presenza di un tasto in alto a destra che blocca istantaneamente lo schermo. Come l’ho visto ho apprezzato molto l’idea, che trovo molto pratica specie in posti affollati e uffici. Confesso però di avere totalmente trascurato la possibilità. Sarà che sta sulla tastiera in studio, che sta a casa, e non ho bisogno di bloccare lo schermo con zero preavviso.

La corsa dei tasti è molto ridotta ora, meno che sui MacBook Pro della polemica, con la corsa ridottissima (mia moglie ne usa uno e spesso mi trovo a digitare occasionalmente qualcosa lì sopra). La trovo confortevole a sufficienza. Il ritorno dei tasti è ottimo e come conseguenza la velocità complessiva di scrittura ne guadagna. Abituato da dodici anni alla stessa tastiera, non è vada realmente più veloce; diciamo che c’è meno frizione nell’uso e mi sembra complessivamente di avere una digitazione più leggera e più libera.

Ho preso il modello più piccolo, quello senza TouchID e senza tastierino numerico. TouchID è anch’esso un bel pensiero in teoria ma, su un Mac da scrivania dentro una casa privata, serve onestamente a poco, se non altro perché da me la maggior parte degli acquisti online avviene con iPad o iPhone, assai più funzionali grazie a FaceID. Lo sblocco da login, beh, non è per fare lo snob, ma una volta che hai sbloccato Mac con watch, qualsiasi altro sistema ti sembra un passo indietro.

La tastiera, per sua natura wireless, può essere cablata per la ricarica e su M1 Apple richiede il cavo per il primo abbinamento (motivo per cui, deduco, non riuscivo ad abbinare la vecchia tastiera). Il cavo è presente di serie nella confezione, Usb-Lightning. Ad abbinamento completato lo si toglie di mezzo, volendo, e il funzionamento è completamente wireless.

L’avvicendamento al dodicesimo rintocco annuale è stato positivo, insomma.

Niente da segnalare invece in tema di Magic Trackpad; quello vecchio ha funzionato egregiamente dopo un paio di tentativi di abbinamento. L’unico problema è estetico; per dimensioni, stile e ingombro, tastiera e trackpad sono proprio due cose diverse. Me ne farò una ragione, fino a che quello che funziona (e impeccabilmente finora) continua a funzionare.

*Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile.*
