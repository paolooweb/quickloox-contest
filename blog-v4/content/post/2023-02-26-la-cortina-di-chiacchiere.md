---
title: "La cortina di chiacchiere"
date: 2023-02-26T15:41:02+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [OpenOffice, LibreOffice, ChatGPT]
---
Alla radio parla la deejay, che si descrive persona *proiettata verso il futuro*. Racconta al compagno di microfono di avere provato la nuova sensazionale novità dell’intelligenza artificiale. Dice, *ho parlato con Joe Biden*. Incredibile, dice, non ci potevo credere.

D’altronde, le fa eco il compagno di microfono, se *parli con Shakespeare e lui ti risponde a tono* vuol dire che siamo davanti a un grande cambiamento.

Radio a copertura nazionale, orario di pausa caffè, milioni di persone in ascolto, delle quali – a dispetto di quello che avviene nella nostra bolla – la gran parte al massimo aveva una vaga idea della situazione.

L’inquadratura cambia e si fissa su un piccolo dirigente di una grande società. Lei ha più di diecimila dipendenti, lui non è un pesce grosso, relativamente; comunque risponde di alcune centinaia di persone. Parla e dice *L’open source ci piacerebbe un sacco per risparmiare, solo che è un’incognita. Per esempio, abbiamo provato OpenOffice, solo che non funziona tanto bene…*

Chi sia appena addentro nella questione sa bene che il progetto di produttività da ufficio giusto per guarire dalla piaga di Office è [LibreOffice](https://www.libreoffice.org). [OpenOffice](https://www.openoffice.org) (lo linko perché ci si possa rendere conto) è da anni un morto che cammina, tenuto artificialmente in vita da alcune multinazionali per mettere i bastoni tra le ruote a LibreOffice, che in quanto libero, sai mai, domani potrebbe compiere scelte funzionali sgradite e c’è quindi bisogno di una suite controllabile, che serva al doppio scopo di tenere d’occhio l’evoluzione del software e scoraggiare chi la prova.

Fuori dalla bolla, però tutto questo non lo sa nessuno. Il mondo è grande cinquanta o cento volte la bolla e lì arrivano solo lontani echi delle notizie vere. La consapevolezza è zero; la gente fa quello che gli dicono gli intrattenitori, quelli senza impegno, ad approfondimento nullo. Oppure i consulenti che si vendono al chilo e liquidano con una frasetta fatta apposta qualsiasi argomento scomodo per il (loro) business.

È nel mondo grande che arrivano i messaggi malati, si generano danni che forse neanche saranno mai rimediabili. Si è visto con il vaccino, si vede con la guerra, a breve con l’intelligenza artificiale.

Una cortina di chiacchiere ci separa dal mondo reale. Dobbiamo lavorare in una situazione ostile e controcorrente per limitare la rovina, in attesa di poter riportare il progresso. Come se fossimo la Fondazione del ciclo galattico di Asimov. Possibilmente [la seconda](https://www.skuola.net/libri/seconda-fondazione-asimov.html).