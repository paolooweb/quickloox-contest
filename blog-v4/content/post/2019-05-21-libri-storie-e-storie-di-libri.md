---
title: "Libri, storie e storie di libri"
date: 2019-05-21
comments: true
tags: [Apogeo, Apogeonline]
---
Mi scuso per la peraltro rara autopubblicità: sono particolarmente orgoglioso di segnalare la partenza del nuovo sito [Apogeonline](https://apogeonline.com/).

Orgoglioso perché è stato un lavoro lungo e complicato, con mille variabili. La base dati preesistente era sparsa su diverse piattaforme, su diversi sistemi, con diversa organizzazione, e lo sforzo per uniformare e categorizzare è stato imponente. Non esisteva un e-commerce diretto, che ora c’è, e alle tradizionali attività di editoria libraria e *webzine* si sono aggiunti i corsi, che aprono un fronte di *business* inedito per la casa editrice Apogeo.

Voglio precisare che il mio coinvolgimento è stato minimo e il lavoro pesante e concreto lo hanno svolto altri. Ma ho dato molto volentieri il contributo che mi è stato chiesto, per quanto piccolo, e sono parte della squadra. Quindi, orgoglioso.

Particolarmente orgoglioso perché, come ho detto, uno dei risultati è la razionalizzazione di oltre quindicimila articoli pubblicati su Apogeonline nel corso di una storia che parte dal secolo scorso. Alcune serie storiche di articoli erano virtualmente impossibili da ritrovare sul vecchio sito; altre erano inaccessibili, nel senso pratico della parola, e quindi non curate e non aggiornate.

Questa grande mole di articoli ora si ritrova sotto una unica interfaccia, è omogenea nella veste, è interamente categorizzata nonché disponibile per l’editing, le correzioni, gli aggiornamenti, per nuovi collegamenti a nuove risorse, per la costruzione di nuovi percorsi di lettura. È un tesoro culturale enorme nel forziere della storia italiana dell’informatica e, assolutamente, non da tutti.

Come addetto ai contenuti di Apogeonline ora mi aspetta da domattina un lavoro consistente, fatto di contenuti nuovi da generare e vecchi da coltivare. Ne sono molto felice. Creare storie attorno alla tecnologia mi appassiona da sempre e, altezzosamente quanto con candore, trovo che Apogeonline si sia sempre distinta dal mucchio.

Chi volesse fare un giro sul sito, che era fermo da prima di Natale mentre dietro le quinte ferveva il lavoro di costruzione della nuova versione, si accorgerà di molti cambiamenti, spero in meglio. Un *feedback* sarà ovviamente gradito. Grazie!
