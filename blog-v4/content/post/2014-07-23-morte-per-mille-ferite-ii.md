---
title: "Morte per mille ferite II"
date: 2014-07-23
comments: true
tags: [Android, Gallery, Photos]
---
Avevo promesso di tornare qualche volta sul [resoconto dell’adozione di Android](http://jacksonfish.com/2014/05/22/a-user-experience-designer-switches-from-ios-to-android/) da parte di un esperto di interfacce, viste la lunghezza e la ricchezza del suo articolo. Ecco un frammento relativo alla presenza sul terminale di due *app* fotografiche al posto di una.<!--more-->

>Che app dovrei usare sul telefono per organizzare le foto? Gallery? O Photos? In onestà, ancora non lo so. Una delle due è più verde, con riferimento al colore dell’interfaccia più che all’ecologia. Ma è tutto quanto riesco a determinare. Nel momento in cui il sistema operativo offre due opzioni equivalenti, deve importunare l’utilizzatore ogni volta che serve decidere oppure lasciargli designare una preimpostazione. Android risolve questo problema con la sua ubiqua finestra di dialogo a domandare quale delle due app io voglia usare. Posso scegliere una impostazione “fai sempre così” ma finisco sempre per toccare “solo per stavolta” in quanto non conosco davvero la differenza tra le opzioni. Non ho veramente idea di quali siano i compromessi tra l’optare per l’una o l’altra app. E se non c’è vera differenza, perché offrire entrambe?

A parte l’inefficienza del *design* di una soluzione del genere, si obietterà probabilmente che questo comportamento non riguarda Android in generale, ma quella combinazione particolare di costruttore, modello e versione del sistema.

Proprio l’idea che a seconda del modello possano succedere cose diverse e imprevedibili mi sembra ancora più cervellotica.