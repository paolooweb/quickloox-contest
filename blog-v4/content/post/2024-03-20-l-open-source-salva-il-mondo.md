---
title: "L’open source salva il mondo"
date: 2024-03-20T00:08:48+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [GnuCobol, The New Stack]
---
C’è una quota importante di software sotto il cofano di infrastrutture e organizzazioni varie di tutto il mondo che è scritta in Cobol, un linguaggio finanziato più di sessant’anni fa dal Department of Defense americano come qualcosa di estremamente portabile, che funzionasse su qualsiasi computer ed eliminasse il problema di dover supportare software diversi su piattaforme diverse.

Cobol ha avuto un grande successo e si è infilato in grandi fabbriche, banche, assicurazioni. Nel tempo è stato accantonato per l’arrivo di linguaggi nuovi e più attraenti ma, in molti casi, chi lo usava ha continuato a usarlo, per evitare costi di migrazione e perché funzionava.

Risultato: quando siamo al Bancomat a prelevare, se il linguaggio sottostante non è Java è probabile che sia Cobol. Se prima si temevano i costi di migrazione, adesso il problema è che non ci sono programmatori in grado di eseguirla e i sessantenni cresciuti o approdati al Cobol sono oggi la categoria di lavoratori meno a rischio di licenziamento in qualsiasi settore. Anzi, un giovane autodidatta che imparasse oggi Cobol al livello professionale adeguato avrebbe la certezza dell’impiego e dello stipendio di prima classe vita natural durante.

Ci sono realtà commerciali che tengono in vita soluzioni Cobol, però sappiamo come va con il commerciale: se domani le cose girano per il verso sbagliato, da un momento all’altro cambia tutto. Per questo è significativo che sia stato annunciata [la maturità industriale di GnuCobol](https://thenewstack.io/20-years-in-the-making-gnucobol-is-ready-for-industry/), compilatore Cobol (prende il codice sorgente e lo trasforma in file eseguibile) open source.

Non sono annunci che si fanno alla leggera; lo sviluppo di GnuCobol dura da vent’anni e se il progetto esce allo scoperto adesso, è perché si trova davvero pronto a farlo.

Nel mondo esistono pare più di ottanta miliardi di righe di codice Cobol ansioso di essere compilato a ogni nuovo ritocco, cifra che aumenta ogni anno del quindici percento.

Il problema maggiore diventerà probabilmente a un certo punto riuscire a clonare vecchi programmatori. Intanto però un mattone della catena è stato messo in sicurezza. Chiunque, domani, potrà compilare del codice sorgente Cobol che ha in ufficio.

I nostri Bancomat possono respirare di sollievo.