---
title: "Preoccupazioni e alternative"
date: 2017-01-05
comments: true
tags: [Mac, Pro, FreeBsd, MacOS]
---
C’è chi si preoccupa di [trovare un’alternativa a MacOS](http://bitcannon.net/post/finding-an-alternative-to-mac-os-x/). Perché non è convinto dell’impegno di Apple verso Mac eccetera eccetera eccetera.<!--more-->

Non per niente lo chiama Mac OS X, come se non fosse MacOS da mesi. La mia impressione è che sia un utente per caso, anche se finisce per concludere che una vera alternativa a MacOS, che offra gli stessi vantaggi senza gli stessi difetti, non esiste.

Aggiungo che il pensiero dell’alternativa non dovrebbe basarsi su una preoccupazione, ma su una normale ridondanza, esattamente come si tiene una ruota di scorta in auto o un set di candele nella baita di montagna dove potrebbe andare via la luce. Non ho alcuna intenzione di abbandonare MacOS, ma [da anni](https://macintelligence.org/posts/2014-03-11-sottofondo-virtuale/) faccio esperimenti su una macchina virtuale FreeBsd.

C’è chi si preoccupa della mancanza di aggiornamento di Mac Pro e, servendogli una macchina come un Mac Pro, cerca in giro alternative a Mac Pro, finendo per acquistare… [un altro Mac Pro](http://subfurther.com/blog/2017/01/03/capitulation/). Masochista puro, o Mac Pro ha ancora qualcosa da dire nonostante l’obsolescenza del modello in vendita?

Oggi come oggi, se Apple deve suscitarmi preoccupazioni, lo fa perché è disposta a [eliminare la app del New York Times dal mercato cinese](http://www.recode.net/2017/1/4/14170212/apple-pulls-china-nyt-app). Ma anche in questo caso, ho la sensazione che le alternative siano poche. O nessuna.