---
title: "I gioielli della corona"
date: 2015-08-20
comments: true
tags: [watch, Mac, glance, Crown, corona]
---
Inutile spiegare come funzioni il software di watch, perché Apple ha messo in campo uno sforzo divulgativo mai visto prima, neanche con iPhone. Tra [minisito](https://www.apple.com/it/watch/), videoguide, illustrazioni in grande formato e la cornucopia di situcci e sitini che spiegano questo e quello in qualsiasi dettaglio concepibile, è impossibile che serva altro per avere l’idea.<!--more-->

Rispetto all’avere la *real thing* sul polso, invece, va detto che la resa dello schermo sembra per ora straordinaria. Tutto è molto leggibile, molto pulito, molto immediato. Attenzione però, perché le persone della vecchia scuola che cercano un watch *intuitivo* potrebbero andare deluse.

Per queste persone (me compreso), *intuitivo* è *i comandi sono davanti a me e inizio a imparare usandoli. Quando ho finito di usarli, possiedo le chiavi fondamentali del sistema*.

Con watch non è così. La vecchia scuola si lamenta se c’è un manuale perché Macintosh, anno 1984, non aveva bisogno di un manuale. Quando arriva un apparecchio del 2015 si lamenta perché il manuale c’è, solo che – è il caso di watch – è un pieghevolino che scrive in mille caratteri o nemmeno come si attivano le funzioni principali, come si sgancia il cinturino, come togliere o aggiungere maglie.

Invece che esserci un procedimento di scoperta, ce n’è uno di assimilazione. Se tieni premuto il quadrante dell’orologio scopri di poterlo personalizzare; ti scappa un dito e scopri le notifiche se è scappato in basso, gli *sguardi* (*glance*) se verso l’alto. Nel giro di pochissimo tempo queste operazioni diventano normali e, in un certo senso, sei pronto per capire l’essenza della corona digitale (il vero colpo di genio) o del tocco energico diverso dal tocco semplice.

Sono giorni di vacanza e viaggi, paradossalmente il tempo da dedicare al nuovo arrivato è minimo. Posso dire che l’apparentamento con iPhone è velocissimo e suggestivo. È stata una sorpresa ricevere una chiamata telefonica – ha squillato l’orologio e attraverso quello ho risposto, senza toccare iPhone in tasca – e mi spiace un po’ che ci sia da pagare il prezzo di tenere attivo Bluetooth, una tecnologia che detesto di gran cuore.

Qualcosa in cambio però arriva. Schiacci il pulsante laterale e appaiono subito tutti i contatti, un giro di corona digitale e diventano istantaneamente raggiungibili. Estrarre di tasca iPhone per chiedere qualcosa a Siri a volte è fastidioso; alzare appena l’avambraccio è naturale e più discreto, si fa più volentieri anche in pubblico.

Entro qualche giorno voglio esplorare per bene le *app*. I gioielli della corona, quella digitale.