---
title: "Ultraeditoria"
date: 2022-10-10T01:54:19+01:00
draft: false
toc: false
comments: true
categories: [Hardware, Web]
tags: [Apple Watch Ultra, David Smith, Highlands]
---
David Smith ha deciso di mettere alla prova watch Ultra e se lo è portato a fare [trekking nelle Highlands scozzesi per tre giorni](https://david-smith.org/blog/2022/09/28/apple-watch-ultra-scotland/).

Il prodotto della gita si traduce in una paginetta sul web.

>Sono entusiasta di avere watch Ultra al polso e voglio vivere assieme a lui molte altre avventure.

Dentro la paginetta però è contenuto anche un [video da trentaquattro minuti](https://www.youtube.com/watch?v=5IERYzho01U&t=2041s), interamente girato con un iPhone 14 Pro e montato in proprio.

Detesto le recensioni video perché normalmente sono in mano a qualche imbonitore mancato che scatena la parlantina per tenere avvinto lo spettatore intanto che un montaggio frenetico mostra particolari, sovrimpressioni, cambi di scena continui, un bombardamento di informazione che vuole evitare il cambio di canale e generare assuefazione.

Questo, invece, è il racconto di un viaggio e di un viaggio assieme ad watch Ultra. Si gusta in poltrona, meglio se con qualcosa di caldo, in un momento senza fretta. Sembra a tratti di accompagnare Smith nelle Highlands. Alla fine i nervi sono rilassati e abbiamo anche capito qualcosa di quanto watch Ultra possa essere funzionale in una situazione come quella.

È da tempo che l’editoria si è fatta personale e ognuno può creare, curare, caricare il proprio canale informativo personale. Non è comune, invece, avere *buona* editoria personale. Buoni contenuti, attenzione alla comunicazione invece che alla vendita di se stessi, un rapporto di condivisione con lo spettatore invece che di prevaricazione mediale.

Che bello se tanti *creator* seriali venissero soppiantati da persone competenti, garbate, capaci, che mostrano piacere attraverso la loro opera.

<iframe width="560" height="315" src="https://www.youtube.com/embed/5IERYzho01U" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>