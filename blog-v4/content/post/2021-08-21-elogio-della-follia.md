---
title: "Elogio della follia"
date: 2021-08-21T01:55:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [Office, Microsoft, LibreOffice, Libreitalia] 
---
Un modo possibile per uscire dalla follia di Office 365: [scegliere LibreOffice assieme a un partner che lo integri in azienda](https://www.libreoffice.org/download/libreoffice-in-business/).

Non è gratis, ma i dati restano dell’azienda, se ne occupa una app realmente conforme agli standard – i formati *legacy* di Microsoft, quelli senza la X per capirci, [sono deprecati dal 2008 per l’Organizzazione internazionale degli standard (ISO)](https://www.libreitalia.org/libreoffice-7-2-community/) – e il livello tecnologico è di prim’ordine.

Chi voglia fare prove può scaricare [LibreOffice 7.2 appena uscito](https://www.libreitalia.org/libreoffice-7-2-community/) e pagarlo nulla.

Chi non si fidi, faccia due chiacchiere con qualcuno di [LibreItalia](https://www.libreitalia.org), a partire da [Italo](https://www.hideas.com/about/italo-vignoli/) per il quale metto la mano sul fuoco.

C’è chi giudicherebbe follia anche passare a LibreOffice. Sono d’accordo; è follia sana, che cambia il mondo, migliora le cose, porta colore, freschezza, novità e progresso. L’altra è la follia del grigiume, della paura, della serializzazione delle persone. È benefica solo in virtù della possibilità di vedere quanto sia sana l’alternativa.

*Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile.*