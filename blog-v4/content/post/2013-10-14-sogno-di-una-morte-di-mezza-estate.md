---
title: "Sogno di una morte di mezza estate"
date: 2013-10-14
comments: true
tags: [Microsoft, Word]
---
Charlie Stross non è [Stephen King](http://www.stephenking.com/index.html) e neanche [Howard Phillips Lovecraft](http://www.hplovecraft.com). Però ha fatto [più che abbastanza](https://en.wikipedia.org/wiki/Charles_Stross) per essere reputato scrittore vero. Come autore di fantascienza, *fantasy* e *horror*, è più che normale che gli venga in mente [la morte di Microsoft Word](http://www.antipope.org/charlie/blog-static/2013/10/why-microsoft-word-must-die.html).<!--more-->

>Un tiranno per l’immaginazione, un dittatore di seconda classe, incoerente e privo di immaginazione, mal concepito per un qualsiasi utilizzo creativo. Peggio: un quasi monopolista, che domina il proprio settore e ha lavato il cervello degli sviluppatori software al punto che pochi riescono a concepire strumenti simili che non cerchino di imitarlo.

Stross ne ricostruisce brillantemente la storia e i perché, fino a evidenziare genialmente il paradosso del programma che esiste da sempre perché basa la propria esistenza sulla scarsa longevità dei documenti che produce:

>La sua obsolescenza pianificata è di nessun significato per la maggior parte delle aziende, per le quali la vita media di un documento di interesse professionale è inferiore a sei mesi. Altri campi chiedono tuttavia la conservazione dei documenti. Giurisprudenza, medicina, letteratura sono aree nelle quali l’aspettativa di vita di un file vuole misurarsi in decenni, se non secoli. Le pratiche di business di Microsoft sono ostili agli interessi di questi utilizzatori.

Ma non è per questo che desidera la morte di Word:

>Voglio che Word muoia perché è **inevitabile**. Non scrivo usando Word. Uso una varietà di altri strumenti, da [Scrivener](http://www.literatureandlatte.com) (un programma per maneggiare la struttura e le modifiche di grandi documenti composti, che funziona analogamente a un ambiente integrato di sviluppo per programmatori e si occupa di testo invece che di programmi) a editor classici come [Vim](http://www.vim.org). In qualche modo i grandi editori sono stati purtroppo indotti a credere che Word sia il *sine qua non* dei sistemi di produzione di documenti. Hanno distorto e rovinato il loro flusso di produzione allo scopo di usare i file .doc come substrato grezzo, nonostante sia male attrezzato per le attività editoriali. E si aspettano che *io* mi integri in un flusso di lavoro Word-centrico, per quanto inappropriato, dannoso e faticoso. È semplicemente inevitabile. Peggio, per la propria prominenza ci ha reso ciechi alla possibilità che i nostri strumenti per la creazione di documenti possano migliorare. Ci ha già tenuti fermi per quasi venticinque anni; spero che presto troveremo qualcosa di meglio per sostituirlo.

Ancora una volta, come per Microsoft in generale, il problema grave non è il danno che apporta. È il progresso che nega.

Nel pezzo di Stross figurano anche le ragioni tecniche. Mi limito a citare Richard Brodie, uno dei programmatori originali di Word, [rispetto alla propria missione](http://www.memecentral.com/mylife.htm):

>scrivere il primo elaboratore di testo che avesse l’interfaccia di un foglio di calcolo.

Subito dopo lo stesso Brodie commenta *per riparare ai danni arrecati ci vollero cinque anni*. Questo per la nascita, immaginarsi l’evoluzione.

Prima Word muore, meglio è. Incoraggiamolo.