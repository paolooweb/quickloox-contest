---
title: "App che vorrei"
date: 2016-06-07
comments: true
tags: [OSX, Siri, dadi, Speech-to-text]
---
Una *app* che faccia *vero* *speech-to-text* e a cui dare in pasto in modo facile un file audio e ricavare in modo facile un file di testo. Esistono numerose soluzioni, ma nessuna veramente facile e veramente decisiva.<!--more-->

Una *app* per lanciare molto velocemente dadi in schemi complessi. Sembra una banalità, eppure anche le interfacce migliori sul mercato – ce ne sono di bellissime – cadono prima o poi in una lentezza o in una inefficienza. Non conosco peraltro *app* con comando vocale; ritengo che un programma eccellente per lanciare dadi dovrebbe farlo tramite una interfaccia convenzionale e però trovo che la voce potrebbe risolvere varie problematiche a riguardo. No, Siri lancia solo dadi a sei facce e bisogna dirgli di *tirare* dadi, perché associa la parola *lanciare* alle *app* e ne cerca una di nome *dadi*.

Una *app* che traduca una specifica in linguaggio naturale sotto forma di espressione regolare.

Una modifica a OS X che, al momento di richiamare una *app*, porti in primo piano *solo la prima finestra* della *app* e non tutte.

Una app che maneggi un combattimento in Dungeons & Dragons a partire da messaggi in linguaggio naturale. Ma è una roba complessa, che deborda nell’intelligenza artificiale, magari ne parlo un’altra volta e trovo uno che ha un milione di dollari da metterci.