---
title: "Zitto e scrivi"
date: 2022-03-17T01:04:38+01:00
draft: false
toc: false
comments: true
categories: [Software, Web]
tags: [StackEdit, Rotary]
---
Sono reduce soddisfatto da una serata (virtuale) presso il [Rotary Club Ticino](https://www.facebook.com/RotaryClubTicino), dove sono stato ospite e ho potuto chiacchierare sul tema *La tua IT funziona?*

Ho fatto una carrellata su che cosa sia oggi l’Information Technology per le aziende: non più un centro di costo, immutabile e livellato verso il basso con tutte le altre aziende, come negli anni novanta, bensì una vera e propria *business unit*, che può contribuire al pari delle altre alla crescita e allo sviluppo, grazie alla possibilità odierna di scegliere e personalizzare praticamente tutta l’offerta hardware e software, in contrasto con l’uniformità obbligata dello scorso secolo.

Era previsto un segmento dedicato al software *no-code* o *lo-code*, i servizi accessibili via browser a prezzo quasi sempre modico spesso in grado di soppiantare applicazioni blasonate (esempio banale: [Canva](https://www.canva.com/), un tranquillo sostituto di Illustrator o InDesign in un sacco di situazioni attinenti al *graphic design*.

Più o meno sapevo che cosa proporre e non mi aspettavo grandi sorprese, quando mi sono imbattuto in [StackEdit](https://stackedit.io): un editor Markdown con i controfiocchi, open, via browser.

Dovevo tenere la mia chiacchierata e non ho approfondito, ma lo faccio senz’altro. Se ci sono magagne, non si vedono al primo sguardo e al primo sguardo c’è invece l’effetto *wow*. Mi è venuta voglia di scrivere e non è che, dopo avere visto dozzine di editor, succeda proprio sempre. Mi ci metto.