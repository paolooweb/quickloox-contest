---
title: "Imparare a insegnare"
date: 2024-02-03T23:21:34+01:00
draft: false
toc: false
comments: true
categories: [Software, Hardware, Internet]
tags: [Howell, James Howell, Emacsconf, OBS Studio, Xournal, Beamer]
---
Succede che Apple presenta [Vision Pro](https://www.apple.com/apple-vision-pro/) e, un po’ per volta, i *talk* di [Emacsconf](https://macintelligence.org/posts/2023-12-03-il-regalo-perfetto/) me li sto guardando tutti.

L’anello di congiunzione è che alla fine siamo fatto della stessa pasta dei nostri dati. In più ripensavo anche al mio amico autore di [che bello PowerPoint che scrive le presentazioni da solo](https://macintelligence.org/posts/2024-01-24-il-giorno-prima/).

E arriva questo James Howell, docente di [Penn State University](https://www.psu.edu), apparentemente con [una reputazione molto buona](https://www.ratemyprofessors.com/professor/1130718) come docente, a rimarcare tutta una serie di distanze, culturali, morali, ahimé pochissimo materiali nella sua dissertazione su [Comporre e presentare corsi universitari con Emacs e un insieme di software libero](https://emacsconf.org/2023/talks/uni/).

(Forse meglio guardarlo con Firefox o Chrome, perché con Safari ho avuto qualche problema; ma il mio Safari soffoca di finestre aperte e non so se sia solo un problema mio oppure generale).

Il filmato del talk è identico nello schema a una delle sue lezioni, che proietta sullo schermo dell’aula e contemporaneamente in videoconferenza per gli studenti fuori sede.

Howell è inquadrato a mezzo busto mentre parla e ha dietro sé lo sfondo generico della presentazione, mentre dentro una finestra scorrono i contenuti veri e propri. Testo grafica, video e animazioni, nonché disegno a mano libera quando lo ritiene opportuno.

Si è messo insieme da solo tutto il sistema, che è interamente basato su software libero, e crea da sé le lezioni. Con mille dollari ha comprato un portatile usato, una tavoletta usata con stilo, schermo esterno usato, microfono, telecamere, cavi, adattatori, tutto il necessario. La creazione della lezione avviene con [emacs](https://www.gnu.org/software/emacs/download.html); sulla tavoletta (Linux) fa andare la presentazione preordinata, scrive e disegna a mano libera, registra la voce e tutto va sul portatile (Linux), dove con [OBS Studio](https://obsproject.com) si compone la scena. Il sistema viene mostrato funzionante sulla sua cattedra. L’elenco completo dei programmi usati è ampiamente descritto nel *talk* e nelle note a contorno, chiunque può replicare lo stesso setup.

Non c’è niente di magico; si tratta di una classica proiezione su schermo verde sul quale vengono proiettati tutti gli elementi. A un certo punto Howell solleva persino un lembo del telo per svelare l’illusione.

Va notato a questo punto che insegna biochimica e biologia molecolare: non è un *nerd* con un corso di *machine learning*. Ciononostante ha lavorato su emacs in *org mode* per creare un sistema che consente, con lo stesso file, di generare tutte le slide, generare anche il supporto cartaceo da consegnare agli studenti – composto come si conviene, perché il supporto a una presentazione non è una stampa della presentazione – e predisporre tutto affinché a fine semestre la dispensa dell’intero corso, con tutte le lezioni, esca da sola o quasi.

Emacs maneggia file Pdf e LateX, oltre al testo, perché non c’è niente di ideologico nel lavoro di Howell: sceglie la soluzione più consona per presentare al meglio le sue lezioni. Così, se è meglio avere un certo file come Pdf, che sia Pdf. Per risparmiare sul portatile, ha comprato un Surface e ci ha messo sopra Linux. E così via.

Si dirà: un pazzo – comunque docente di biologia molecolare – con troppo tempo libero. Non solo uno: vengono fatti i nomi e i riferimenti di altri sette docenti, in altre università americane, che utilizzano uno schema di lavoro analogo a partire da emacs.

Un pazzo che interviene a una conferenza virtuale di nicchia, con capelli lunghi, t-shirt GNU e aria da birra bistecca e patatine prima di tornare al *ranch*; che si costruisce da solo il sistema di presentazione con risorse minime; che rende tutto pubblico e accessibile senza gelosie e senza segreti. Un docente universitario.

Ho già ricordato una vecchia canzone di Eugenio Finardi con il testo *perché l’unica cosa / che la scuola dovrebbe fare / è insegnare a imparare*. Ho sempre ritenuto questo verso degno di passare alla storia. Naturalmente, perché possa avverarsi, occorre prima *imparare a insegnare*. E oggi, anche questo da ripetere, ognuno di noi è padrone dell’intera catena della pubblicazione… se lo vuole.

Penso a quegli studenti, che si trovano davanti un docente appassionato e ingegnoso, con lezioni vive, animate, umane per quanto ci sia il passaggio dentro il digitale e sono contento per loro.