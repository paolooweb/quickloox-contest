---
title: "Si metta a referto"
date: 2013-07-18
comments: true
tags: [iPad]
---
Mi si perdoni il *post* nostalgico. In anni giovanili ho dedicato molto tempo al basket, come giocatore, poi allenatore e anche ufficiale di campo volontario, uno dei quali compiti è compilare il referto della gara.<!--more-->

Il referto è il documento che attesta la sequenza degli eventi durante la partita: la sequenza del punteggio, quanti falli sono stati commessi e da chi e in che momenti eccetera eccetera. Il tutto – ai tempi – in doppia copia e con la massima leggibilità e cura, per evitare contestazioni.

Mi sono quasi commosso [leggendo](http://www.littleleague.org/Page59339.aspx) che i referti di gara delle prossime World Series della Little League di baseball – le finalissime nazionali del campionato americano under 12 – verranno compilati su 35 iPad equipaggiati con [GameChanger](http://www.gamechanger.io).

Perché è una ulteriore attestazione della validità di iPad come strumento per questo tipo di applicazioni. Le leghe sportive sono giustamente ancora diffidenti in materia e un software deve essere supercollaudato.

E per solidarietà con gli ufficiali di campo. Se un referto del basket richiede attenzione, quello del baseball si avvicina alla magia bianca.

Potrebbe darsi che non siamo così indietro: a settembre, [stando agli annunci](http://www.fip.it/News.asp?IDNews=6008), la Federazione Italiana Pallacanestro avrà fatto mettere a punto un [referto elettronico](http://www.dataproject.com/BasketBall/News_194.aspx) dalle specifiche ambiziose.

Auguro a tutti gli ufficiali nuovi che sia veramente vero.