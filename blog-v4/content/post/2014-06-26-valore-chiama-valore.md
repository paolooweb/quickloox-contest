---
title: "Valore chiama valore"
date: 2014-06-27
comments: true
tags: [Surface, Microsoft, Skype, Sky]
---
Rispetto al [valore reale](https://macintelligence.org/posts/2014-06-23-fatta-la-tara/) di una tavoletta [Surface Pro](http://www.microsoft.com/surface/it-it/products/surface-pro-3) di Microsoft, si può aggiungere che [se ne può vincere una chiamando con Skype](https://consent.skype.com/payg/index).<!--more-->

Quando se ne troveranno anche nelle patatine e cellofanate dentro le riviste, sarà chiaro che non sostituisce tanto il portatile come afferma la sua presentazione sul sito, quanto il *gadget* promozionale. L’idea che valesse poco va corretta con l’idea che valga ancora meno.

**Aggiornamento:** nella campagna abbonamenti Sky [vengono offerti](http://soloperte.sky.it/microsoft/) i vecchi Surface Pro e Surface Pro 2 con risparmi di cento, trecento, persino cinquecento euro, con finanziamento a tasso zero e omaggi di ogni tipo. C’è qualcuno che paga il prezzo di listino?