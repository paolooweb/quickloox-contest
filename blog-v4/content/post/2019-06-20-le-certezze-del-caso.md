---
title: "Le certezze del caso"
date: 2019-06-20
comments: true
tags: [Random.org, League, Entropy, Cloudflare]
---
Curiosa e mi scuso se vado un po’ fuori argomenti, la notizia che un consorzio di aziende [produrrà distributori di numeri casuali](https://duo.com/decipher/the-league-of-entropy-forms-to-offer-acts-of-public-randomness), ovvero impredicibili, da fornire a chiunque possa averne bisogno.

I numeri casuali sono importanti in numerosi comparti tecnologici, a partire da sicurezza e privacy, e sono materia incomprensibile per la persona comune, per cui è significativo che si muovano in tanti per un servizio del quale alla fine beneficiano solo specialisti.

I più interessati possono approfondire a partire da questo [post sul blog di Cloudflare](https://blog.cloudflare.com/league-of-entropy/). Io intanto noto l’assenza di [Random.org](https://www.random.org).
