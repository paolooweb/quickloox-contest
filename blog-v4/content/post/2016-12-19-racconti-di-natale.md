---
title: "Racconti di Natale"
date: 2016-12-19
comments: true
tags: [Ikea, MacBook, Pro, Wi-Fi, FreeCiv]
---
MacBook Pro 15” (fine 2011) aveva problemi alla scheda video. Portato all’Apple Store, la risposta è stata che si poteva approfittare del cambio gratuito della scheda logica – e della scheda video – fino al 31 dicembre.<!--more-->

Quarantotto ore dopo è arrivata la mail che informava della riparazione effettuata. Solo che non ho letto in tempo ed è passata la notte.

Settantadue ore dopo il computer era di nuovo a casa, funzionante. Ovviamente nulla si ha a pretendere oltre a una garanzia di tre mesi, ma il lavoro è ripreso. Intervento di 496 euro nominali. Costo effettivo, zero.

In uno sviluppo collegato alla vicenda, il Wi-Fi Ikea ora è diventato una cosa seria. Ossia è diventato semplice. Almeno nella mia Ikea di fiducia, dove [andava diversamente](https://macintelligence.org/posts/2014-08-12-la-rete-fiacca/).

Per finire: FreeCiv – gioco di strategia libero sviluppato da volontari, con oltre vent’anni di vita – non solo ha una versione giocabile via web, ma [anche una (beta) 3D che sfrutta OpenGl](http://play.freeciv.org/blog/2016/12/freeciv-webgl-3d-version-beta-2-available/).

Trovare una simile vitalità è difficile anche in giochi commerciali strafinanziati. L’unica cosa che manca [è sempre la versione Mac](https://macintelligence.org/posts/2016-11-17-una-falla-nella-civilizzazione/). C’è ancora qualche giorno prima di Natale…