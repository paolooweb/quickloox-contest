---
title: "Sulla buona strada"
date: 2016-06-13
comments: true
tags: [Gawker, Thiel, PayPal, Hogan, Gizmodo, Jobs, Denton, Ziff-Davis]
---
Tra le regole giornaliere che mi aiutano a dormire in pace con la coscienza vi è sporcarmi le mani il meno possibile con Gizmodo, sito popolato di gente senza ritegno né rispetto. La più ripugnante che hanno fatto è stata dare Steve Jobs per spacciato con due anni di anticipo, ma ne hanno fatte di ogni colore, [loro e i loro colleghi](https://macintelligence.org/posts/2010-12-14-io-non-c-entro-niente/). Ogni clic sul loro sito ha contribuito a farli ingrassare. Ne ho fatti pochissimi e ne sono contento.<!--more-->

Ora potrebbe essere arrivata la resa dei conti. Sui siti di Gawker Media (editore di Gizmodo) era comparso tempo fa un video privato di scene piccanti, protagonisti il *wrestler* [Hulk Hogan](http://hulkhogan.com) e la moglie di uno dei suoi amici. Hogan se l’è presa e ha trovato un alleato in [Peter Thiel](http://www.forbes.com/profile/peter-thiel/), fondatore di PayPal, il cui orientamento sessuale è stato messo in piazza tempo fa sempre da Gawker senza il consenso dell’interessato.

Incidenti isolati? No, strategia dichiarata e senza scrupoli. Sempre Gawker scrisse dei [gusti sessuali di Tim Cook](http://gawker.com/5736917/meet-apples-new-boss-the-most-powerful-gay-man-in-silicon-valley) dopo che Apple se la prese perché un suo prototipo di iPhone, dimenticato in un bar, era [finito nelle mani di Gizmodo](http://www.fastcompany.com/1621516/iphone-4-leak-saga-start-finish), che aveva pagato cinquemila dollari l’autore del ritrovamento.

Così Thiel ha pagato a Hogan l’appoggio legale più costoso che si potesse permettere. Risultato, Gawker è stata condannata a pagare 140 milioni di dollari di risarcimento, di cui dieci sono a carico diretto dell’editore [Nick Denton](http://nick.kinja.com).

Per pagarsi il ricorso, Denton [ha messo in vendita Gawker Media](http://www.recode.net/2016/6/10/11903764/gawker-bankruptcy-chapter-11-sale-ziff-davis), che pare verrà rilevata dall’editore Ziff-Davis.

Comunque vada, se vorrà proseguire a vivere di immondizia editoriale, Denton non potrà più farlo come prima ed è una bella notizia.

Thiel ha definito il finanziamento alla causa intentata da Hogan *una delle sue più grandi operazioni filantropiche* e, se avesse richiesto un finanziamento pubblico, un euro ce lo avrei messo anch’io. Denton non ha ancora avuto tutto quello che si merita, però la strada è buona.