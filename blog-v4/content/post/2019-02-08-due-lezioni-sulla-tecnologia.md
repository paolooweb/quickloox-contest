---
title: "Due lezioni sulla tecnologia"
date: 2019-02-08
comments: true
tags: [Safari]
---
Una delle soluzioni (sedicenti) al problema della *privacy* in rete, secondo gente con fantasia galoppante, sarebbe consistita nell’inserire nei siti una comunicazione *Do Not Track*; in sostanza chiedere gentilmente a quanti interessati a tracciare la navigazione, d non farlo, *please*. Come [scrive *Engadget*](https://www.engadget.com/2019/02/07/apple-removes-safari-do-not-track/), somiglia a scrivere sullo zerbino all’ingresso *per favore non rubate in questa casa*.

Apple ha preso atto del (prevedibile) fallimento dell’iniziativa e rimuove da Safari il supporto di *Do Not Track*, per due ragioni: la prima è che appunto si tratta di codice inutile. La seconda è che Safari ottiene protezione della *privacy* molto migliore con la Intelligent Tracking Prevention messa a punto da Apple, funzione che impedisce a chi vuole tracciarci di starci dietro quando cambiamo sito.

Due lezioni di tecnologia in un colpo solo.

La prima: se Internet può essere abusata, qualcuno lo farà. Purtroppo è diffusa l’idea che l’abuso piccolo si possa tollerare più dell’abuso grande, che – per dire – scaricare tonnellate di video rubati non dia fastidio a nessuno, visto che c’è gente molto più cattiva. Purtroppo le disonestà si sommano, piccole e grandi, e le rapine in banca non giustificano i borseggi. Il clima delle rete in merito alla protezione di chi naviga potrà migliorare solo quando il comportamento utilitarista e menefreghista sarà l’eccezione invece della regola.

Seconda lezione: i problemi indotti dalla tecnologia si possono risolvere efficacemente solo con tecnologia migliore. Tutte le volte che si invoca come soluzione il ritorno ai bei tempi, al passato, all’indietro, trattasi di illusione.
