---
title: "Io speriamo che ci ripensi"
date: 2021-10-06T02:50:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [Fabio, 7Bit, Steve Jobs, Apple, MacJournals] 
---
Fabio ha di nuovo scelto una data significativa e malinconica per annunciare l’abbandono del suo [profilo Twitter](https://twitter.com/settebit) e io spero che ci ripensi.

7Bit era in origine anche [un sito](http://www.setteb.it). Doveva essere anche una carriera personale, che però non decollò secondo le aspettative di Fabio e ha cessato le pubblicazioni proprio con la morte di Steve Jobs, dieci anni fa.

Aspettative alte. Nel suo coprire il mondo Apple, Fabio non è mai stato accondiscendente né ha edulcorato il racconto delle cose. La sua narrazione era sempre entusiasta, a volte distaccata ma mai fredda, e comunque sincera.

Ha sempre denunciato, Fabio, un certo atteggiamento di Apple verso la stampa di settore e un ambiente dove vedeva favoritismi eccessivi, figli e figliastri, ipocrisie e ingiustizie. Come sempre ne ha parlato senza peli sulla lingua e certamente questo non ha aumentato la sua lista di amicizie, in Apple prima che fuori.

Ma lui è fatto così. Ci siamo frequentati poco e ho l’impressione che non abbia mai amato il proscenio e, per quanto desiderasse avere un giusto successo con 7Bit, non cercasse le luci della ribalta, piuttosto un’attenzione maggiore dal pubblico e da Apple.

Ignoro se ci siano ragioni più profonde per la sua decisione, oltre alla delusione personale. Spero di no e che tutto stia andando nel migliore dei modi. Le allusioni alla salute nella sua comunicazione dicono e non dicono; io conto che sia un non-issue e che tutto vada per il meglio.

In ogni caso, è una perdita. 7Bit somiglia nella sua parabola a [MacJournals](https://twitter.com/macjournals) e si unisce a un gruppo di voci che dovrebbero restare vitali e presenti nelle _timeline_ di tutti, in nome del bene comune. Per competenza, passione, umanità, profondità.

La mia forza contrattuale nei confronti di Fabio è sotto lo zero e non ho alcuna influenza sulle sue decisioni. Confido che prosegua a dispensarci analisi, approfondimenti, scoop, critiche, battute, tutto quello che abbiamo potuto leggere in questi anni e che davvero vorremmo continuare a vedere.

Perché di gente che si dice controcorrente e fuori dal coro ne abbiamo a montagne ed è il peggiore dei cori. Fabio non si è mai dato arie ed era, ed è veramente una voce alternativa, non allineata, un contraddittorio indispensabile proprio perché senza sconti.

Io speriamo che ci ripensi.

*Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile.*