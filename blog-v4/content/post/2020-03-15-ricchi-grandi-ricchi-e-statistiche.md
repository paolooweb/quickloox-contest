---
title: "Ricchi, grandi ricchi e statistiche"
date: 2020-03-15
comments: true
tags: [Gates, Jobs, Microsoft, Giving, Pledge]
---
La notizia che Bill Gates [esce dal consiglio di amministrazione di Microsoft](https://news.microsoft.com/2020/03/13/microsoft-announces-change-to-its-board-of-directors/) per dedicarsi maggiormente alle sue attività di filantropo ha suscitato comprensibilmente grande clamore.

Meno comprensibilmente, ho visto varie espressioni di ammirazione per la parabola dell’uomo Gates, che più o meno prima si disapprovava o si disprezzava per quello che aveva fatto in e con Microsoft mentre adesso, invece, fa il filantropo.

Una notizia: Gates esce dal consiglio di amministrazione. Non esce da Microsoft, dove continuerà a operare come Technology Advisor per l’amministratore delegato e altri dirigenti.

Sembra anche, a leggere commenti qua e là, che Gates si stia dedicando a spendere il proprio denaro in attività benefiche.

In un certo senso è vero; in termini assoluti, tuttavia, Gates *è più ricco di prima che facesse il filantropo*.

Ho chiesto [aiuto a Wolfram Alpha](https://www.wolframalpha.com/input/?i=bill+gates+net+worth), che prende i dati da *Forbes* e quindi non è tacciabile di inesattezze: il patrimonio personale di Bill Gates era appena superiore a quaranta miliardi di dollari nel 2002. Eccettuata la parentesi del 2008 dovuta alla crisi finanziaria globale, la cifra è salita pressoché costantemente. Oggi, diciotto anni dopo, Gates possiede centosei miliardi di dollari.

Interessa poco sapere se Gates usi l’attività filantropica per fare soldi o se accorti investimenti gli permettano di spendere e contemporaneamente mantenere, anzi, aumentare la propria ricchezza; di fatto è evidente che fare il filantropo, da un punto di vista di portafogli, gli conviene.

Gates è un membro di [The Giving Pledge](https://givingpledge.org/Home.aspx), l’impegno degli individui e delle famiglie più facoltose del mondo a dedicare *la maggior parte delle loro ricchezze* al *giving back*, al ripagare la comunità delle fortune che hanno avuto modo di raccogliere.

Dal 1994 a oggi, lui e la moglie hanno donato alla Bill & Melinda Gates Foundation un totale di [oltre trentasei miliardi di dollari](https://www.gatesfoundation.org/Who-We-Are/General-Information/Foundation-FAQ), più di 1,3 miliardi l’anno.

Meno di Warren Buffett, che dal 2006 si è impegnato a contribuire alla fondazione e solo una volta, nel 2009, ha versato quella cifra. Altrimenti, [ha fatto regolarmente di più](https://www.gatesfoundation.org/Who-We-Are/General-Information/Foundation-Factsheet).

Il quadro ritrae un grande ricco che allestisce una fondazione che gli offre vantaggi fiscali e un grande ritorno di immagine, in un contesto che lo rende ancora più ricco. Se questo meriti ammirazione, più di quanto la meritino tutti gli altri ricchi che fanno beneficenza e tutti quanti si dedicano a combattere la malaria o il cambiamento climatico, comprese aziende farmaceutiche e aziende petrolifere, non lo so.