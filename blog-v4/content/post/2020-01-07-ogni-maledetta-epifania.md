---
title: "Ogni maledetta Epifania"
date: 2020-01-07
comments: true
tags: [Terminale, blog]
---
Si parla sempre dei buoni propositi come se scattassero all’inizio dell’anno nuovo, quando è evidente a tutti che non se ne parla prima che le feste le portino via.

Il che puntualmente accade ed eccoci qua, a doverlo fare per davvero. Per cui ho cercato di darmi poche indicazioni ad alta probabilità di essere realizzate.

Per prima cosa, **voglio scrivere qui ogni giorno**. Impegnativo, ma non impossibile. Niente mi disturba più che non avere un *post* per il giorno che arriva.

Secondariamente, **voglio raggiungere un obiettivo preciso con il Terminale**. Probabilmente sarà usare decentemente [emacs](https://www.gnu.org/software/emacs/), anche se non sono sicurissimo: la lista delle possibilità è lunga e il tempo limitato.

Terzo, **voglio scrivere almeno un Comando rapido significativo**. iOS è il luogo dove l’automazione è più inaspettata e arrivano i risultati maggiori con lo sforzo minore.

Penultimo, **voglio frequentare con regolarità [Swift Playgrounds](https://www.apple.com/swift/playgrounds/)**. Un anno o due e dovrò mostrare qualcosa a una figlia o due.

Infine, **voglio diversificare le fonti di reddito**. Per un lustro ho lavorato meno della media e fatto molto il papà. Ora che anche secondogenita ha passato il secondo compleanno, voglio continuare a fare il papà; ma le giornate lavorative canoniche tornano normali. Tante occasioni di approfondire, creare, osare ridiventano possibili.

Se qualcun altro avesse una sua lista, la leggerei molto volentieri. Se potessi contribuire in qualsiasi modo a realizzare un obiettivo, con un consiglio, un’opinione, una pizza, ci sto.

Questa settimana abbiamo scherzato. Avanti con il vero 2020!