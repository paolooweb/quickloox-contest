---
title: "Un motivo ci sarà"
date: 2019-02-04
comments: true
tags: [Homebrew]
---
Intanto che guardo il [Super Bowl](https://www.nfl.com/super-bowl), provvedo a una urgenza improcastinabile: aggiornare Homebrew alla nuovissima luccicante [versione 2.0](https://brew.sh/2019/02/02/homebrew-2.0.0/).

Nelle note di aggiornamento viene data molta rilevanza al supporto-novità per Linux e Windows, tuttavia c’è più che a sufficienza sotto al cofano per stare più che abbondantemente aggiornati.

La mia dipendenza da Homebrew non è intensa ma subdola: da Common Lisp allo scaricamento dei video da YouTube, non mi serve mai urgentemente né tutti i giorni; eppure risolve miriadi di situazioni dove un mattoncino Unix sistema in un instante quello che a me costerebbe probabilmente ore di attenzione, appeso alla speranza che non si verifichi un problema di complessità troppo superiore alla mia capacità. Viva Homebrew, viva Homebrew 2.0.

A pensarci bene, non ho idea precisissima del perché sia importante aggiornare; sospetto però che mi convenga fidarmi e che ne riceverò più benefici di quanto io possa oggi immaginare, o sapere.