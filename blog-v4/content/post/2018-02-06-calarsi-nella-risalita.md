---
title: "Calarsi nella risalita"
date: 2018-02-06
comments: true
tags: [Avernum, Spiderweb]
---
È terminato il collaudo confidenziale di [Avernum 3: Ruined World](http://www.avernum.com/avernum3/index.html), cui ho potuto prendere parte solo parzialmente.<!--more-->

È stato più che sufficiente per riconoscere il marchio di qualità di Spiderweb Software e apprezzare il gioco.

Nell’ultima puntata della trilogia il *party* (è un gioco di ruolo per computer, a turni, vecchio stile, con la trama che conta più della grafica e un mondo immenso in cui muoversi) parte dal sottosuolo di Avernum per risalire alla superficie e verificare se, finalmente, il popolo dei deportati potrà rivedere la luce.

Diciamo che si può fare, ma prima occorre del lavoro su molti fronti, prima fra tutti la disinfestazione. Ed è il lavoro che tocca a chi vorrà sborsare i venti dollari per calarsi nell’esperienza della risalita.

Li vale tutti, anche nell’epoca delle *app* stracciate a prezzo conseguente.

Per ora su Mac; la versione iPad è attesa entro fine marzo.