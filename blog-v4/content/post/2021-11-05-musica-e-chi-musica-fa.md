---
title: "Musica è chi musica fa"
date: 2021-11-05T00:04:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [MacBook, iPhone] 
---
C’è chi sta a fare le pulci al connettore, a contare le porte, a fare lo scettico, a discettare di innovazione che manca.

C’è chi invece fa a tal punto da potersi permettere [il gioco](https://twitter.com/andrewhuang/status/1455221050564808713). Attività fine a se stessa, tramite la quale si raggiunge l’illuminazione.

Solo chi fa male non trova il tempo di ridersi addosso. Solo i mediocri mancano di autoironia.

<blockquote class="twitter-tweet"><p lang="en" dir="ltr">The new MacBooks really are an instrument in themselves. Bravo <a href="https://twitter.com/Apple?ref_src=twsrc%5Etfw">@Apple</a>!⁣ <a href="https://t.co/Gr2S2yYuUf">pic.twitter.com/Gr2S2yYuUf</a></p>&mdash; Andrew Huang (@andrewhuang) <a href="https://twitter.com/andrewhuang/status/1455221050564808713?ref_src=twsrc%5Etfw">November 1, 2021</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

*Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile.*