---
title: "In doppia cifra"
date: 2019-03-02
comments: true
tags: [Drang, BBEdit, regex]
---
Onore e stima perenni per Dr. Drang, rafforzati dal fatto che solo lui può arrivare a scoprire come BBEdit [sappia recuperare da una espressione regolare fino a novantanove risultati parziali](https://leancrew.com/all-this/2019/02/regex-groups-and-numerals/) e, per conseguenza, sarebbe meglio richiedere questi ultimi con un numero a due cifre, per non incappare in equivoci anche sottili, difficili da cogliere.

Onore e stima perenni a BBEdit per farlo e, in generale, avere un eccellente supporto per le *regex*. Cambiano la vita a chi scrive e, più in generale, a chi amministra sistemi.
