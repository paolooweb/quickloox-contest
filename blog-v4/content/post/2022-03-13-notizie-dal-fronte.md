---
title: "Notizie dal fronte"
date: 2022-03-13T03:58:51+01:00
draft: false
toc: false
comments: true
categories: [Internet, Web]
tags: [Ars Technica]
---
Per quanti condividessero almeno parzialmente [quanto espresso ieri](https://macintelligence.org/posts/2022-03-12-la-guerra-di-maurizio/), è apparso su Ars Technica [un bell’articolo](https://arstechnica.com/tech-policy/2022/03/russias-disinformation-machinery-breaks-down-in-wake-of-ukraine-invasion/#p3) che fa il punto sullo stato del confronto con la macchina della disinformazione.

Ribadisco: il problema non sono le opinioni, ma le conseguenze che queste hanno su un insieme vasto di altre persone. C’è gente che muore, altra che perde la connessione con la realtà, altra che si gioca relazioni, lavoro, famiglia senza capire di essere vittima di una manipolazione. Va fermata la macchina e vanno fermate anche queste persone, per il loro bene, come si ferma chiunque stia mettendo a repentaglio la propria salute o la propria sicurezza in modo inconsapevole.

Non intendo metterla sulla politica e quindi non approfondirò, né darò ulteriore seguito alla questione. Tuttavia agirò dove e come ho possibilità di farlo.