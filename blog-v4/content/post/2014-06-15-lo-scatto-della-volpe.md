---
title: "Lo scatto della volpe"
date: 2014-06-15
comments: true
tags: [Fifa, AnonymoX, Firefox, Flash, iTV, streaming]
---
La cosa non mi riguarda perché sul mio Mac si fa a meno di Flash e le partite del Mondiale sono trasmesse in *streaming* via web a quanto mi è dato di sapere solo con quella vecchia, insicura, instabile, residuale, farraginosa tecnologia.<!--more-->

E non le trasmette in Italia, amena località dove l’emittente nazionale ha i diritti per sole 25 partite su 64. E per fare un esempio non mostrerà nemmeno una delle tre partite iniziali dell’Argentina.

Chi sopportasse Flash, installi [Firefox](http://www.mozilla.org/en-US/firefox/all/) e poi, tramite il *browser*, aggiunga l’estensione *AnonimoX*, che permette di fingersi in collegamento da un’altra nazione.

Si scelga per esempio [iTV](https://www.itv.com/itvplayer/itv), o un sito americano, inglese, olandese, quello che è. Con AnonymoX si predisponga la visione del sito con la finta nazionalità giusta, americana, inglese, olandese eccetera. Arriverà lo *streaming*.

Non mi riguarda, perché qui niente Flash, grazie.