---
title: "Preferisco una certezza"
date: 2021-09-06T01:18:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [Apple, Csam] 
---
Sono contento che Apple abbia deciso di [prendere tempo addizionale](https://www.apple.com/child-safety/) per ripensare al suo sistema di individuazione di immagini abusive di minori. Insisto e ripeto: ad Apple (a chiunque) quello che tengo sul mio iPhone dovrebbe interessare zero, salvo ovviamente ordine di un giudice per motivi giustificati.

È che non abbiamo un sostitutivo: abbiamo una parentesi di incertezza di durata indefinita.

Di questo non sono per niente contento, poiché preferisco una certezza, anche sgradevole, all’incertezza.

Su qualunque altro argomento potrei essere contraddetto e ascolterei con attenzione; qui rimango irritato e nervoso perché [c’è un contesto](https://macintelligence.org/posts/Privacy-e-realtà-un-triplo-salto-mortale-carpiato-con-avvitamento.html), nel quale la politica si aspetta da Apple certi risultati quantitativi nella caccia al detentore di foto proibite e in cui le altre società analizzano senza remore gli spazi cloud delle persone, ma anche senza dirlo troppo apertamente. In altre parole, Apple deve avere un qualche sistema di riconoscimento delle foto e deve trovare un numero accettabile di immagini,

Non fa piacere a nessuno l’analisi delle foto presenti su iPhone; nemmeno farebbe piacere, però, che Apple passasse al setaccio gli account iCloud alla ricerca di questa o quella categoria di foto.

Tra queste due alternative c’è un intervallo nel quale potrebbe cadere la soluzione Apple. Solo che non sappiamo a quale estremo sarà più vicina e neanche quando arriva. Dovendo Apple fare per forza qualcosa, è pacifico che qualche meccanismo ci sarà.

Spero ce lo svelino in fretta.

*Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile.*