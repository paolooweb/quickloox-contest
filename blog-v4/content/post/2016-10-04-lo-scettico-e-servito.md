---
title: "Lo scettico è servito"
date: 2016-10-04
comments: true
tags: [watch]
---
*Ma a che cosa serve?*, il premio 2015 (watch è uscito l’anno scorso) per la domanda polemica più vuota e miope.

Si confermerà per il 2016 e diventerà sempre più difficile scalzarla dal podio, specie all’indomani di un recente annuncio di [Shake Shack](https://www.shakeshack.com) catena miliardaria della ristorazione nata a New York da un banchetto ambulante per la vendita di *hot dog*.

La società che controlla Shake Shack si è accordata con la *startup* delle prenotazioni [Resy](https://resy.com) su qualcosa di particolare, mai tentato prima, che riepilogo da un [articolo di Eater](http://www.eater.com/2016/9/28/13095780/danny-meyer-apple-watch-resy):

>Quando lo Union Square Cafe riaprirà [questo mese] a Manhattan, ogni sommelier e ogni responsabile di sala indosseranno un watch. E quando una celebrità di presenta all’ingresso, qualcuno ordina una bottiglia di vino, viene occupato un tavolo, un avventore si trova attesa di trovare un posto, il bere tarda ad arrivare su una tavolata oppure un piatto del menu non è più disponibile, ciascun responsabile riceverà una notifica mediante il piccolo computer collegato al loro polso.

(La dizione *piccolo computer* è traduzione letterale; non sono l’unico a considerare watch computer da polso invece di orologio).

Si capisce come sia una situazione ben diversa dall’avere personale che si muove per il ristorante e brandisce una tavoletta o estrae continuamente un iPhone dalla tasca.

Il bello è che il locale ospiterà anche qualche scettico. Godrà di un servizio impeccabile e, tra un boccone e l’altro, discuterà baldanzoso di tecnologia fino a esclamare tronfio e sazio, *ma a che cosa serve un watch?*