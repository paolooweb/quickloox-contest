---
title: "Codice, non parole"
date: 2021-06-12T11:07:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [OldOS, SwiftUI] 
---
Per capire quanto sia progredito iOS e quanto abbia attorno un ecosistema evolutosi in maniera impressionante negli anni, va guardato OldOS, un progetto di [iOS 4 riscritto in SwiftUI](https://twitter.com/zzanehip/status/1402625414762270723).

<blockquote class="twitter-tweet"><p lang="en" dir="ltr">Today is Launch Day 🚀<br><br>Introducing OldOS — iOS 4 beautifully rebuilt in SwiftUI.<br><br>* 🎨 Designed to be as close to pixel-perfect as possible.<br>*📱 Fully functional, perhaps even usable as a second OS.<br>* 🗺️ Fully open source for all to learn, modify, and build on. <a href="https://t.co/K0JOE2fEKM">pic.twitter.com/K0JOE2fEKM</a></p>&mdash; Zane (@zzanehip) <a href="https://twitter.com/zzanehip/status/1402625414762270723?ref_src=twsrc%5Etfw">June 9, 2021</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

Non è emulazione, non è retroingegnerizzazione; è il codice originale, ripreso, riscritto ([SwiftUI](https://developer.apple.com/xcode/swiftui/) è il sistema moderno di costruire app basate su codice Swift).

L’autore riferisce che, bug e perfezionamenti a parte, OldOS è funzionante, al punto che potrebbe forse essere usato come un secondo sistema operativo.

Un bello spunto di discussione per quando salta fuori la faccenda dell’azienda chiusa, del software proprietario eccetera: iOS4 è open source, liberamente disponibile a chiunque. E non solo lui. Fatemi vedere una vecchia versione di Windows Mobile riscrivibile a partire dal codice originale.

*Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile.*               