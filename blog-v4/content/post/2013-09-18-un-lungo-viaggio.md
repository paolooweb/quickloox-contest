---
title: "Un lungo viaggio"
date: 2013-09-18
comments: true
tags: [iPhone]
---
Interessante [articolo di Bloomberg](http://www.bloomberg.com/news/2013-09-11/the-iphone-s-secret-flights-from-china-to-your-local-apple-store.html) sulla logistica degli iPhone che vengono prodotti in Cina per poi raggiungere in aereo le località di smistamento e infine i negozi, in una operazione di logistica di precisione dove ogni spreco significa perdite sonanti di denaro.<!--more-->

Non è ancora l’articolo da inchiesta, che svela tutto; la segretezza di Apple funziona ancora per quanto su una catena di produzione lunga, complessa e internazionale (quale sito di profeti ha saputo preannunciare la presenza di un nuovo coprocessore di movimento? Per [qualche illuso](http://www.ibtimes.com/apple-iwatch-rumors-what-iphone-5s-says-about-smart-watchs-release-date-1405868) indica addirittura la data di presentazione di iWatch, eppure nessuno ne ha scritto, stranamente).

Però è godibile. Bello il particolare del volo da quindici ore che porta 450 mila iPhone al costo di 242 mila dollari dalla Cina a Memphis, Tennessee, senza scalo, su un Boeing 777. O il motivo per il quale le stampanti e altra elettronica di consumo viaggia per nave.

Giusto uno sguardo su operazioni incredibilmente complesse su numeri incredibilmente grandi, impensabili fino all’altroieri. È tecnologia anche questa, se pure meno scintillante ed esclusa dai *keynote*.