---
title: "Realtà e fantasia"
date: 2013-09-12
comments: true
tags: [iPhone]
---
Teniamo presente di stare a leggere reazioni di gente per la quale Apple ha presentato un giro di [telefoni di plastica colorata](http://www.apple.com/it/iphone-5c/) e un altro giro di [telefoni con l’impronta digitale](http://www.apple.com/it/iphone-5s/).<!--more-->

Tutt’altra cosa sarebbe se Apple avesse presentato un computer da tasca con elaborazione e architettura interna a 64 bit dotato di nuovo processore principale, nuovo coprocessore per l’elaborazione dei dati dei sensori di movimento, nuova versione ampiamente riprogettata del sistema operativo, doppio flash con euristiche di dosaggio della giusta illuminazione, stabilizzatore fotografico, sensore visivo di avanguardia, nuovo diaframma allargato.

Purtroppo dobbiamo accontentarci della realtà. Almeno quella virtuale presentata da quanti che avevano in mente l’*iPhone low-cost* e adesso lamentano che Apple non abbia ricevuto il messaggio telepatico. O dicono che Steve Jobs, la mente dietro iMac colorati e iPod colorati, non avrebbe mai permesso la produzione di iPhone colorati.