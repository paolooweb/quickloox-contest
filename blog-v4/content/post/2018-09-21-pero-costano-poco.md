---
title: "Però costano poco"
date: 2018-09-21
comments: true
tags: [Xiaomi, Reddit, Verge]
---
Il grande vantaggio di poter usare un computer da tasca con [la pubblicità dentro il menu delle impostazioni](https://www.theverge.com/2018/9/19/17877970/xiaomi-ads-settings-menu-android-phones).

Io vorrei vederli acquistare il frigorifero, con la stessa logica del *device*. Apri l’anta per prendere il latte e ci riesci solo dopo lo spot Milka, per dire.
