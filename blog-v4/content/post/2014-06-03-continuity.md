---
title: "Continuity"
date: 2014-06-03
comments: true
tags: [OSX, Yosemite, iOS8, iCloud, HealthKit, HomeKit, Swift, Objective-C, AirDrop, Continuity]
---
Si sapeva che avremmo avuto un OS X 10.10 e un iOS 8, non che avrebbero lavorato assieme fino al punto che metti già un apparecchio e riprendi a lavorare sull’altro come se niente fosse, anche con un programma di posta o di messaggistica.

Nessuno aveva pensato alla possibilità che avremmo potuto ricevere una chiamata su iPhone e prenderla da Mac.

iCloud adesso permette di arrivare a qualsiasi file e aprirlo con qualsiasi applicazione, non importa su che apparecchio Apple, e anche da Windows se si usa il *browser*.

Da dire ci sarebbe infinitamente di più, a partire da Swift: Apple ha messo a punto [un nuovo linguaggio di programmazione](https://itunes.apple.com/it/book/swift-programming-language/id881256329?l=en&mt=11) per sostituire Objective-C. Poi ci sono le consuete modifiche ai programmi, poi le interfacce, poi AirDrop funziona finalmente tra Mac e iOS, poi HealthKit, poi HomeKit e bla bla bla.

Raramente negli ultimi anni si è vista questa densità di annunci, due ore con pochissima fuffa e solo software (è il convegno dei programmatori). Nasce spontanea la curiosità per l’hardware cui è destinato in prima istanza tutto questo ben di Dio.

La notizia vera è Continuity. Il primo passo verso la concentrazione assoluta sul documento, sul contenuto, sul proprio lavoro, sull’oggetto dell’attenzione. Dove *veramente* quando lo si fa, dove, come diventa un dettaglio secondario, perché l’esperienza è quella ottimale su ciascun apparecchio.

Non sembra che Apple abbia rinunciato a innovare. Questa è innovazione. Raccomando vivamente il [keynote](http://www.apple.com/apple-events/june-2014/) per chi lo avesse mancato. E sono fortemente tentato di scaricare la *beta* di iOS 8, cosa che normalmente farei solo sotto tortura o per questioni irrinunciabili di professione. C’è dentro Continuity.
