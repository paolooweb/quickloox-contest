---
title: "Nessuno siamo perfetti"
date: 2024-01-04T02:56:09+01:00
draft: false
toc: false
comments: true
categories: [Software, Hardware]
tags: [Blue Scuti, Ars Technica, NES, Tetris, Sclavi, Tiziano Sclavi]
---
Una storia di software troppo umano e dell’intelligenza umana che spinge i confini di una disciplina dove nessuna intelligenza artificiale, vera o presunta, potrebbe arrivare. Troppo gustosa.

In [Tetris](https://tetris.com) i tetramini (i pezzi composti da quattro quadrati disposti in ogni modo possibile per formare una figura intera) aumentano la velocità di caduta a ogni nuovo livello. Dopo il livello ventinove la velocità resta costante, solo che chi gioca su Nes – [Nintendo Entertainment Sustem](https://www.nintendo.it/Console-e-accessori/Storia-di-Nintendo/Nintendo-Entertainment-System/Nintendo-Entertainment-System-627024.html) si accorge che il controller non tiene il passo del gioco e non reagisce in tempo per governare la caduta di ogni pezzo.

Quindi la comunità dà di fatto per irrisolvibile il gioco oltre il livello ventinove: un umano riesce a ragionare con i pezzi che cadono, però il controller non esegue in tempo i suoi comandi.

Questo fino a quando viene scoperta una tecnica di manipolazione del controller che consente di moltiplicare l’input: se la meccanica dell’apparecchio non ci arriva, la logica invece ci riesce. Improvvisamente diventa possibile sfidare Tetris oltre il fatidico livello ventinove. Iniziano a susseguirsi i record e diventa chiaro che un umano bene allenato può tenere botta quasi indefinitamente; viene perfino architettato un *modding* che al livello trentanove aumenta ancora la velocità, in modo che i tornei possano avere un tempo ragionevole per concludersi senza rischio che qualche ragazzino stia sulla console per giorni.

Fuori dai tornei, intanto, senza *modding*, la caccia al record prosegue e i giocatori si accorgono che il gioco inizia a comportarsi in modo strano. Viene fuori che l’algoritmo perde efficienza con il crescere dei livelli e inizia a mostrare difetti, come palette di colori dei pezzi quasi invisibili e problemi di gestione della memoria.

I programmatori sviscerano il problema. Intanto i giocatori sviluppano una manovra ancora diversa per fornire al controller più indicazioni di quelle che il suo hardware permetterebbe. Si arriva a capire che a un certo livello il gioco andrebbe in crash, superata la capacità dell’algoritmo originale di gestire la situazione.

Dopo qualche tentativo, un ragazzino tredicenne, Blue Scuti, ci riesce: arriva al livello centocinquantacinque, dove Tetris su Nes si dovrebbe bloccare. Solo che invece di completare una riga, ne completa per errore tre… e il gioco non va in crash. Blue Scuti però riesce a governare l’imprevisto e va avanti. Due livelli più tardi, quota centocinquantasette, ed ecco che il gioco si blocca come prevedevano i programmatori. [Blue Scuti ha battuto Tetris, primo nella storia a riuscirci](https://arstechnica.com/gaming/2024/01/someone-has-finally-beaten-nes-tetris/): non si  è arreso dopo avere stabilito un qualche record, ma è il gioco che non ha saputo continuare a sfidarlo.

Si dice che, tecnicamente, non sarebbe impossibile trovare un modo di fare funzionare Tetris fino al livello duececentocinquantacinque, terminato il quale il gioco tornerebbe al livello zero, in una specie di rinascita alla fine di un ciclo di vita, e che intelligenze artificiali sarebbero al lavoro per dare la caccia al record definitivo.

Mi tengo con orgoglio, per la mia specie, un ragazzino capace di maneggiare un controller come nessun progettista avrebbe pensato di fare, che dedica il suo trionfo al papà purtroppo mancato la settimana precedente; mi tengo un programma che va in confusione quando la situazione è diventata troppo grande per lui, senza inventarmi un nome per la circostanza (tipo *allucinazione*) in modo da continuare a vendere.

Potrebbe sembrare una storia di *nerd* impallinati per un videogioco e invece parliamo di gente che va oltre la tecnologia e porta il software fino ai suoi limiti. Una cosa che più umana non si può.

Tiziano Sclavi scriveva [Nessuno siamo perfetti, ognuno ci abbiamo i suoi difetti](https://fumettologica.it/2014/12/nessuno-siamo-perfetti-tiziano-sclavi/). Sono proprio i difetti che ci rendono speciali.