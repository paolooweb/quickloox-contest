---
title: "Ci facciamo sempre riconoscere"
date: 2015-03-05
comments: true
tags: [PhotoMath, OneShot, Wolfram|Alpha]
---
È da pochissimo uscita [OneShot](https://itunes.apple.com/it/app/oneshot-for-screenshots/id953724147?l=en&mt=8), *app* iPhone che permette di twittare schermate di testo. A che serve? Beh, una schermata come immagine grafica può fare passare su Twitter più testo di quello che Twitter permette. In molte situazioni è la cosa più normale del mondo, magari quando si vuole condividere il ritaglio di un quotidiano, ma anche una pagina web. E poi OneShot fa una cosa speciale: riconosce otticamente i caratteri dentro l’immagine grafica, così da renderli indicizzabili. E questo gli consente (quasi sempre) di ricostruire ingegnosamente l’indirizzo web della pagina di cui è stata prodotta la schermata.<!--more-->

Gli autori hanno raccontato su Medium il [dietro le quinte](https://medium.com/ios-os-x-development/oneshot-a-one-week-design-case-study-e0512bc02343) della realizzazione. Interessante il dietro le quinte, brillante la *app*.

Venendo ad altro, non sapevo che esistesse [PhotoMath](https://itunes.apple.com/it/app/photomath/id919087726?l=en&mt=8) per iPhone. Come spesso, è l’uovo di Colombo e ti sembra ovvia. *Dopo*.

PhotoMath inquadra una espressione matematica stampata su un libro e *la risolve*. Non bisogna comporre l’espressione sullo schermo: è sufficiente *inquadrarla*. Il programma non arriva ovunque. Non ancora. Ho la sensazione che però sia solo questione di tempo, per espanderlo o per stringere un accordo conveniente con [Wolfram|Alpha](http://www.wolframalpha.com).

Domanda agli insegnanti: quanto tempo passerà prima che gli studenti delle elementari, delle medie, facciano i compiti con PhotoMath in mano? Il problema non è di diffusione della tecnologia (basta un ragazzino o una ragazzina intraprendente e poi è solo questione di Whatsapp). È di come insegnare e di che cosa insegnare. Il tema meriterebbe approfondimenti ulteriori, ma l’attualità della scuola vede nei titoli l’assunzione sì o no di centoventimila precari. Chissà se almeno uno di loro sa che esiste Photomath.

Il punto comunque era un altro. Discutibili, interessanti, controversi, enigmatici come possono essere, questi sono passi in avanti, per oggi al capitolo *riconoscimento ottico*. *App* che prima non c’erano, o se c’erano non erano così *smart* e non avevano questo potenziale di pervasività.

Passi in avanti che avvengono tutti su iOS.