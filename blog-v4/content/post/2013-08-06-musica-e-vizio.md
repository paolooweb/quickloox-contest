---
title: "Musica e vizio"
date: 2013-08-06
comments: true
tags: [Giochi]
---
La mia efficienza in [FreeCiv](http://freeciv.wikia.com/wiki/Main_Page) cresce smisuratamente se ascolto [Incantations](http://en.wikipedia.org/wiki/Incantations_(album)).<!--more-->

Roba vecchia, buona per le notti di agosto. Pensando a *SimCity* che [torna su Mac a fine mese](http://www.polygon.com/2013/8/5/4590786/simcity-mac-release-date-aug-22) dopo tanti anni o a [FlightGear](http://www.flightgear.org) che ha compiuto sedici anni e tra poco due mesi di vita diventando il simulatore di volo numero uno sotto qualunque metro.

Tutti questi giochi e un unico agosto.