---
title: "Un mago dell’editoria"
date: 2022-03-18T01:39:17+01:00
draft: false
toc: false
comments: true
categories: [Internet]
tags: [Brandon Sanderson, Amazon, Misterakko]
---
È grazie a [Misterakko](https://www.accomazzi.net) se ho potuto leggere un lungo e molto interessante [articolo di Brandon Sanderson sullo stato dell’editoria libraria](https://www.brandonsanderson.com/some-faqs-you-might-enjoy/), almeno dal suo punto di vista.

Il suo punto di vista conta qualcosa, dato che vende libri nell’ordine delle centinaia di migliaia di copie ciascuno.

In questi giorni ha lanciato un [progetto Kickstarter](https://www.kickstarter.com/projects/dragonsteel/surprise-four-secret-novels-by-brandon-sanderson?ref=section-homepage-promo-brandon-sandersons-four-secret-novels-makes-history) per sfornare quattro libri entro fine anno o giù di lì. Ha chiesto novecentomila dollari di prefinanziamento; mancano più di dieci giorni alla scadenza ed è già a quota ventisette milioni. È il progetto Kickstarter più finanziato di tutti i tempi.

Uno così è così perché schifa gli editori tradizionali al posto di Kickstarter, oppure perché gli editori tradizionali gli vanno stretti? Più la seconda che la prima. Sanderson ha due editori con cui pubblica convenzionalmente, ma disapprova varie pratiche dell’editoria libraria di oggi e afferma di avere lanciato questo Kickstarter, bontà sua, anche per provare qualcosa ai suoi referenti nelle case editrici.

Per lui bisognerebbe:

- pubblicare insieme copia cartacea e ebook (in Italia, pochissimi. Mi vanto un pochino qui perché [Apogeo](https://www.apogeonline.com) lo fa e ho l’onore di lavorare nel team);
- evitare la protezione anticopia, il famigerato Digital Rights Management;
- avere più combinazioni diversi di prezzo e qualità per raggiungere più lettori possibile, da quelli squattrinati a quelli disposti a pagare per avere *bei* libri sullo scaffale.

È poco contento del quasi monopolio di Amazon sul mercato librario (monopolio per il quale è stata [condannata… Apple](https://www.theguardian.com/technology/2016/mar/07/apple-450-million-settlement-e-book-price-fixing-supreme-court)).

Tra le spese straordinarie in programma grazie al successo del Kickstarter, figurano carte di [Magic](https://magic.wizards.com/en). Su altro, a dire il vero, è stato piuttosto abbottonato. Ma le scelte ludiche sono una buona premessa.

C’è molto altro nell’articolo per chi avesse la pazienza.