---
title: "il mese sbagliato"
date: 2021-11-24T01:23:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [Apple, Saving Simon, iPhone 13 Pro] 
---
Sarebbe bello che fosse già la vigilia di Natale, ma devo accontentarmi del video di Apple, [Saving Simon](https://youtu.be/S5WaFx8rx54). Che in fondo è una lezione sul sapere aspettare. Oltre che sulle possibilità cinematografiche di iPhone 13 Pro.

<iframe width="560" height="315" src="https://www.youtube.com/embed/S5WaFx8rx54" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

_Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile._