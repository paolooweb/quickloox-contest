---
title: "Spazio disco, ultima frontiera"
date: 2023-04-24T01:17:32+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [macOS, Finder, The Eclectic Light Company, Apfs, Utility Disco]
---
Sono in viaggio con il solo iPad e fatico a dare un aiuto concreto a [Edoardo](http://linkedin.com/in/edoardovolpikellermann) che ha qualche problema con il conto dello spazio libero sul disco del suo Mac.

Mi sono però imbattuto in un [pezzo interessante dedicato alla tematica su The Eclectic Light Comoany](https://eclecticlight.co/2023/04/23/last-week-on-my-mac-when-the-numbers-dont-add-up/), *Settimana scorsa sul mio Mac: quando i conti non tornano*.

Viene fuori che i conteggi dei file sul filesystem Apfs mandano spesso in confusione il Finder quando deve operare una sintesi della situazione e dare all’utilizzatore un dato il più possibile coerente e preciso dello spazio effettivamente occupato o disponibile.

In realtà, degli strumenti con una interfaccia utente grafica dentro macOS, [l’unico che fornisce indicazioni affidabili è Utility Disco](https://eclecticlight.co/2023/04/17/the-finder-confuses-with-wildly-inaccurate-figures-for-available-space/). Questo chiaramente non va bene:

>La gestione dello spazio in macOS deve essere ripensata, riprogettata, implementata senza incoerenze o bug, e accuratamente documentata per utenti, staff di supporto e sviluppatori. Qualsiasi intervento meno ampio di questo ci lascerà nello stesso disastro attuale, o peggiore.

A giugno verrà annunciato macOS 14. So che non succederà, però accetterei senza esitare un macOS con le indicazioni di spazio del Finder che funzionano in cambio della rinuncia alla prossima mirabolante Nuova Funzione.