---
title: "Chiuso per imenei"
date: 2020-02-22
comments: true
tags: [nozze]
---
Per oggi sono assente giustificato. Qui sotto un particolare della località che abbiamo scelto, foto scattata a fine luglio.

A domani!

 ![Temporale sul lago a fine luglio](/images/luino.jpeg  "Temporale sul lago a fine luglio") 