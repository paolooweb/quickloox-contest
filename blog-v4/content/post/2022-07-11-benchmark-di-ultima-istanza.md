---
title: "Benchmark di ultima istanza"
date: 2022-07-11T01:13:59+01:00
draft: false
toc: false
comments: true
categories: [Software, Hardware]
tags: [Amazon, AWS, EC2, Amazon EC2, Elastic Cloud 2, Elastic Cloud, Mac, Apple Silicon, M1, Mac mini]
---
Apple Silicon consuma meno a pari potenza ed è più potente a pari consumi.

Ma come si fa a dire se è vero, con *benchmark* semiaddomesticati, spesso dissociati dall’uso reale, a volte truccati all’origine, cui si fa dire tutto e il suo contrario? Diceva uno, basta torturare i dati abbastanza a lungo e diranno tutto quello che vuoi.

Al che si va da Amazon, che di mestiere tra varie altre cose vende cloud, e si chiede a lei.

Amazon vende da tempo [istanze Elastic Cloud 2 basate su Mac](https://aws.amazon.com/ec2/instance-types/mac/?pg=ln&sec=uc)  e *ultimamente basate anche su M1*.

Collaudare i Mac via cloud di Amazon è interessante, si possono provare inizialmente gratis grazie a un sistema di crediti e, per chi non vuole prendersi impegni, dopo ventiquattro ore si può spegnere un’istanza senza altre conseguenze finanziarie.

Però sto divagando. Volevo dire, anzi, lo dice Amazon:

>Le istanze Amazon EC2 di Mac M1 superano anche del sessanta percento il rapporto prestazioni/prezzo di quelle Mac x86.

Alla partenza ci sono sempre Mac mini, Intel oppure M1. Amazon è interessata a vendere istanze, mica a dare ragione qua o là. Il suo software è collaudatissimo, rivolto al suo profitto e non certo ai benchmark; Amazon fa soldi se vende istanze che funzionano bene e costano competitivamente sul mercato.

Ecco; sessanta percento. Sappiamo esattamente quanto vale in più un Mac M1 di un Mac Intel. Traslare la cifra su un PC Intel di pari capacità magari non dà un risultato precisissimo, ma l’ordine di grandezza dovrebbe essere chiaro.