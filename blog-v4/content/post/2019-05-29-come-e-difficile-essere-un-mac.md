---
title: "Com’è difficile essere un Mac"
date: 2019-05-29
comments: true
tags: [Long, Jobs, Apple]
---
Degli oltre trecento spot girati per la serie [I’m a Mac](https://www.youtube.com/watch?v=0eEG5LVXdKo), solo sessantasei sono andati in onda. E alcuni [furono rifiutati da Jobs perché erano perfino troppo divertenti](https://ew.com/tv/2019/05/26/justin-long-steve-jobs-mac-commercials-too-funny/), con il rischio di distrarre il pubblico dall’argomento centrale, che era la superiorità di Mac.

Quanti si sono alterati perché sentivano presa in giro la loro scelta di acquisto, oggi possono tranquillizzarsi: le prese in giro vere non le hanno mai viste.

È altresì istruttivo prendere nota che per Steve Jobs, in fase di *design*, erano più importanti i *no* che i *sì*. Scelta cui è stato fedele persino al momento di approvare le pubblicità.

<iframe width="560" height="315" src="https://www.youtube.com/embed/0eEG5LVXdKo" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>