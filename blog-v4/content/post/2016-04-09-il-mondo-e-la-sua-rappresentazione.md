---
title: "Il mondo e la sua rappresentazione"
date: 2016-04-09
comments: true
tags: [Bryant, database]
---
Non è tanto avere un database che contiene tutti i tiri effettuati da Kobe Bryant nella sua carriera di cestista professionista.

È [come te lo fanno consultare](http://graphics.latimes.com/kobe-every-shot-ever/), che conta. Il database, direbbe un uomo di azienda, oramai è una *commodity*. Il 99 percento del valore arriva dall’interfaccia.