---
title: "Non usarmi, sono un’app"
date: 2017-08-10
comments: true
tags: [Enel, Energia]
---
*(colonna sonora horror)* Dovevo pagare per la prima volta una bolletta diversa dal solito e ho scelto di affidarmi alla *app*[Enel Energia](https://itunes.apple.com/it/app/enel-energia/id767368903?mt=8) *(un fulmine squarcia il silenzio della notte. In lontananza un lupo mannaro ulula alla luna)*. Mi sono lasciato tentare da un codice QR sulla bolletta e da un invito tipo *scarica, inquadra e paga*.

La *app* è per iPhone, non ottimizzata per iPad. Nel 2017.

Può usarla solo l’intestatario di un’utenza. Per dire, se si deve pagare la luce per la casa della vecchia nonna e il contatore è intestato a lei, la *app* deve essere impostata con i dati della nonna. Gestire più di una utenza non è neanche da pensare.

Nonostante la limitazione appena notata, è necessaria la registrazione. Pensare di autenticarsi anche provvisoriamente con il codice cliente e L’identificativo della bolletta non è contemplato. Certo, ci si può collegare mediante un account *social* come Twitter o Facebook: chi ci prova scoprirà che *serve comunque la registrazione*. A che serva L’account *social* mi è rimasto oscuro.

Così bisogna essere intestatari di un’utenza, dunque avere già fornito a Enel un completo quadro anagrafico di sé. Nonostante questo, la *app* chiede nome e cognome e poi il codice fiscale, che Enel ha già. Uno dice: il codice fiscale serve a fare corrispondere l’intestatario dell’utenza con chi usa la *app*. D’accordo, ma a che servono nome e cognome allora? Si noti che viene valutata la correttezza del codice fiscale, *solo su nome e cognome*. Data e luogo di nascita non vengono chiesti e dunque l’analisi del codice fiscale è vuota di significato. Esiste una funzione di calcolo del codice fiscale ma non ho avuto il coraggio di collaudarla. Si tenga presente che tutto questo ruota attorno a un codice fiscale già in mano a Enel.

Superato il fuoco di sbarramento suddetto si arriva finalmente alla fornitura di un codice via email. Gli indirizzi @mac.com vanno bene, ma *il mio* indirizzo @mac.com non può essere usato: *formato non corretto*. Poco male, si usa un altro indirizzo. Nel frattempo bisogna toccare la casella *non sono un robot* e poi, toccatala, selezionare alcune foto da una serie per dimostrare di essere, appunto, umani.

Il quesito sulle foto è una cosa tipo *seleziona le foto dove appare un ponte*, o la vetrina di un negozio. Sullo schermo di iPhone appaiono sei foto insieme e lascio immaginare quanto dettaglio si possa osservare. Le foto sono sbiadite e diventano nitide solo una volta selezionate, come se vederle sbiadite aiutasse l’umano a passare il test. Il tutto è in inglese, con foto americane: se la sfida è identificare un cartello stradale, si tratta di un cartello americano. Nella *app* di Enel Energia.

Cambiata la email, può arrivare il codice. Che serve per *ricevere un codice di identificazione sul cellulare*. Arrivati a questo punto siamo a dieci volte il fastidio di una registrazione presso Apple o Amazon o Google, che custodiscono carte di credito di miliardi di persone.

Ma non è finita. Della carta di credito neanche abbiamo parlato. È possibile associare alla *app* una carta, ma *occorre generare un Pin apposito*. L’ho generato, ma la procedura non va a buon fine, almeno quando ci provo io. Per fortuna funzionano sia l’addebito su un conto bancario sia quello su PayPal, così me la sono cavata.

È evidente che si tratta di una *app* fatta per disincentivarne l’uso. Una sequenza simile di complicazioni inutili, burocrazia superflua, procedure fini a se stesse e malfunzionamenti può essere solamente intenzionale.

Mi interrogo sul perché e non riesco a prendere sonno.

*(Una pendola rintocca sopra il brontolio di un tuono lontano. Sembra una normale notte di temporale estivo, solo che non ho una pendola).*
