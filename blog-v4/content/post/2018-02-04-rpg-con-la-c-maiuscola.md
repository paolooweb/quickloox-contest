---
title: "Rpg con la C maiuscola"
date: 2018-02-04
comments: true
tags: [Rpg, Fëarandil]
---
Dopo tanti anni, dal [blog dove si raccontano tutti i giochi di ruolo per computer](https://macintelligence.org/posts/2014-11-28-posizioni-vacanti/) è arrivato anche [il libro](https://crpgbook.files.wordpress.com/2018/02/crpg_book_1-0-1.pdf) (grazie a **Fëarandil**!).<!--more-->

È una cosa ben fatta e notevole per copertura e approfondimento, un Pdf assolutamente gratuito chiaramente molto più scorrevole del saltare da un post all’altro sul [blog](http://crpgaddict.blogspot.it). Se c’è un interesse seppur minimo per il retrocomputing, è un documento che non dovrebbe mancare in archivio. Raccomandato.