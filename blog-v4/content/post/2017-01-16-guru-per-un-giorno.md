---
title: "Guru per un giorno"
date: 2017-01-16
comments: true
tags: [FileMaker, Villani]
---
Una grande sala riunioni presso la sede di via Merano a Milano del *coworking* [Talent Garden](http://milano.talentgarden.org), gremita di persone: sviluppatori FileMaker. Età media attorno ai quarant’anni. Perché FileMaker è una piattaforma vecchia? Perché è nato nello stesso anno di Excel? No, sostengo io; perché è – appunto – una *piattaforma*. Su una piattaforma si arriva, non si nasce. Si nasce sviluppatori, oggi, buttandosi da ragazzini su un linguaggio di scripting, provando a pasticciare su un [Raspberry Pi](https://www.raspberrypi.org), giocando con Linux, sperimentando i [playground di Swift](http://www.apple.com/swift/playgrounds/) su iPad. Nessuno comincia da FileMaker. Filemakeristi si diventa.

>Lo sviluppatore non vede il dato, vede la procedura.

Una giornata – sono state due – in cui si è parlato di FileMaker da un punto di vista filosofico. Niente spiegazioni su come impostare un campo o come costruire un database da zero, per non insultare l’intelligenza dei partecipanti. Invece, riflessioni su un piano più alto: il significato di un pulsante o di un comando di menu si trova su Internet, su milioni di tutorial. In un corso di élite su FileMaker si va per fare un passo avanti, non per *imparare il programma* come avrebbe detto qualcuno nel 1990.

Dati-logica-interfaccia: lo sviluppo convenzionale procede in questa successione. Con FileMaker il percorso ottimale è esattamente l’inverso.

>Se dovete lavorare su cinquanta milioni di record, non ha molto senso usare FileMaker.

L’anima del corso è [Giulio Villani](http://www.filemakerguru.it/author/giulio-villani/), sviluppatore, libero pensatore e punta di diamante di [FileMaker Guru](http://www.filemakerguru.it). Tra i massimi esperti al mondo, con base in Italia e da qualche tempo anche in Brasile, Paese emergente dove per aprire un conto bancario occorrono cinquantacinque giorni e c’è tanta strada da fare per arrivare al livello dei Paesi più evoluti, ma una connessione Internet  (fornita da… Tim, sì, la nostra) eroga *come minimo* sessanta megabit.

Giulio parla, si addentra nella realizzazione di una anagrafica di esempio, spiega, indica, mentre un operatore filma e registra tutto.

 ![Tra i guru di Filemaker](/images/guru-i.jpg  "Tra i guru di Filemaker") 

>Il power user guarda gli alberi, ma non vede la foresta. Lo sviluppatore guarda solo la foresta e pensa a come utilizzare il legname.

FileMaker è una società posseduta al cento percento da Apple, ma completamente indipendente e separata. In che stato si trova, oggi che il business è basato per tre quarti sulla vendita di iPhone? Il software continua a evolvere, si giustifica, basta a se stesso, vive grazie al gradimento di chi lo compra e non per decisione di Tim Cook o chi per lui. Non so se questo sia preferibile ad avere tutto in casa come avviene, per esempio, con [Final Cut Pro ](http://www.apple.com/it/final-cut-pro/). Di fatto, dopo oltre trent’anni FileMaker è vivo e vitale e forse non ha tutte le risorse che potrebbe avere nel campus di [Infinite Loop](https://en.wikipedia.org/wiki/Infinite_Loop_(street)) a Cupertino; non tutto il software che ha seguito la strada opposta, comunque, è ancora attivo.

>Ragioniamo per dati, ma dobbiamo lavorare per procedure.

Non sono sviluppatore FileMaker; è uno dei treni che ho perso da subito. Credo che oggi avrei difficoltà ad andare oltre la semplice creazione di un semplice database. Vedo Giulio scriptare con leggerezza e impostare codice procedurale sempre breve e pulito. Mi colpisce è la quantità di tempo e di attenzione che viene dedicata all’interfaccia. Una volta, dice Giulio, si buttavano lì due campi e tre pulsanti e andava più o meno bene tutto. Oggi il cliente è attento anche alla qualità visiva e funzionale dell’interfaccia e per avere un mercato bisogna badarci. Poi è capace di compiere gli errori di input e di utilizzo più straordinari e, comunque, bisogna blindare il più possibile tutto il lavoro contro gli utilizzi impropri. La parte rilevante di questo discorso è che lo sviluppatore FileMaker deve essere bravo *anche* sulla parte grafica e di interfaccia e questa è una delle varie ragioni per cui una *factory* di FileMaker è sempre più difficilmente una *one man band*.

Si parla di visualizzazioni web, di utilizzo di Google Maps, di risorse che uno sviluppatore in carriera da vent’anni mai avrebbe neanche pensato di coinvolgere in un progetto di quelli che una volta erano database, a prescindere da quanto script e quante applicazioni gli stavano davanti. Oggi FileMaker riguarda la creazione di app, a prescindere da qualunque database gli stia dietro.

>Sottovalutare le persone è pericoloso, ma sopravvalutarle è mortale.

Pensavo che avrei assistito a una lezione di FileMaker, mentre si tratta di programmazione, no, anzi, di sviluppo, no, nemmeno: analisi del software. Una domanda sulla gestione del listino prezzi permette di imparare qualcosa sull’atomicità, la rappresentazione dei dati basata sull’unità più piccola possibile. Vengono disposti mattoni di costruzione che consentono di sviluppare il meno possibile e riutilizzare il più possibile. Nel tempo, il bravo sviluppatore (FileMaker e no) si ritrova con un magazzino di procedure ottimali e atomiche che facilitano sempre più il lavoro successivo, perché “basta” riprenderle e riassemblarle per la prossima applicazione.

 ![Molto più di un database](/images/guru-ii.jpg  "Molto più di un database") 

Contemporaneamente, la pratica non manca: bisogna magari partire da un elenco di codici di avviamento postale per capire quanti indirizzi in un database contengono una certa selezione di Cap, per poi creare sequenze di indirizzi interrotte da separatori per questioni di stampa delle etichette. Nasce un database all’istante, si delinea una soluzione. Magari uno script composto sul momento. A volte sembra di parlare in italiano anche se si sta scrivendo un programma, a volte si entra in tecnicalità come il conteggio dei valori di un array, diverso in FileMaker rispetto all’intero universo della programmazione. Ecco perché accedere a FileMaker è un momento, ma c’è spazio per intraprendere il percorso verso la condizione di guru.

>SAP è la vendetta tedesca per avere perso la seconda guerra mondiale.

Si prosegue, con digressioni. Allo sviluppo si alternano le considerazioni sulla politica di FileMaker, i cambiamenti nel *licensing*, persino il passato di Apple. Servono a tenere viva l’attenzione e a visualizzare frammenti di un panorama che oramai per complessità e sofisticazione è impossibile da illustrare esaustivamente in due giorni di immersione. C’è molto da dire e mostrare anche se si parla “solo” di creazione e modifica di file Pdf.

>L’interfaccia di FileMaker è confusiva perché presenta in modo semplice concetti difficili.

C’è anche spazio per un programma di supporto capace di seguire i corsisti per tutto l’anno, con continuità. Padroneggiare FileMaker va oltre due giorni di corso o l’accesso a un forum dove, sì, si possono fare domande e trovare risposte, ma apprendere è spesso porre le domande giuste e fare le giuste domande non è banale. Si tratta, si dice, di trasformare integralmente il proprio approccio allo sviluppo nel giro di dodici mesi. Mi convince.

A oggi il mio percorso di vita e di lavoro si dipana su sentieri che sono lontani da questo. L’esperienza compiuta, tuttavia, mi fa capire che aspirare alla condizione di guru FileMaker ha senso. E c’è il modo.
