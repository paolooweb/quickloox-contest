---
title: "Il tronfio trionfo dello snob"
date: 2022-11-23T13:00:07+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [iOS, iOS 16]
---
Sull'icona delle Impostazioni è apparso un badge rosso a segnalare la presenza dell’aggiornamento a [iOS 16](https://www.apple.com/it/ios/ios-16/). Non avevo riunioni né aspettavo telefonate e l’ho lanciato. L’aggiornamento si era precaricato e così serviva solo il tempo per l’installazione. Mi sono messo a fare altro e dopo una ventina di minuti ho guardato distrattamente: era tutto completato.

Praticamente ho dedicato all’aggiornamento due o tre tocchi sullo schermo, più il codice di sblocco. Quasi zero. Siccome è passato del tempo dall’uscita del software, si è installata direttamente la versione comprensiva degi ritocchi minori; ovvero, ho effettuato un aggiornamento solo invece di due o tre.

Sicuramente sono stato qualche settimana senza godere di iOS 16; in compenso, ho potuto dedicare il mio tempo a cose più importanti o più divertenti.

Continuo a pensare, a costo di passare per snob, che aggiornare iPhone quando lo vuole lui sia intrinsecamente meglio di farlo quando lo voglio io.