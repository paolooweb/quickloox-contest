---
title: "Promemoria postelettorale"
date: 2014-05-29
comments: true
tags: [Ue]
---
Mi serviva una cosa sul sito dell’Unione Europea. Nel trovarla mi è stato chiesto di partecipare a un sondaggio informativo. Ho risposto sì.<!--more-->

Questa sotto è la prima pagina del sondaggio. Si noti che, all’inizio, vengo invitato a non starci più di cinque minuti e rispondere dando retta all’istinto.

Promemoria postelettorale: il voto che si è dato o non dato ha senso se da domani si tampina ognuno l’eletto della propria circoscrizione, perché il denaro comunitario non venga più buttato in queste assurdità.

 ![Sondaggio europeo](/images/europa.png  "Sondaggio europeo")
