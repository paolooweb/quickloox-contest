---
title: "Anno tremila"
date: 2016-03-30
comments: true
tags: [Mac, Steam, giochi]
---
È luogo comune che Mac non sia il luogo migliore per giocare. Per i miei gusti non ci sono problemi – giochi di ruolo, *roguelike*, strategici, rompicapo, *multiplayer* – e non sento il bisogno di cercare altro, anche se da ogni parte mi si dice che un PC è tutt’altra cosa.<!--more-->

Non ne dubito; mi limito a osservare, dopo avere sottovalutato il fenomeno, che sulla piattaforma a pagamento Steam ci sono più di [tremila giochi per Mac](http://store.steampowered.com/search/?term=&sort_by=_ASC&os=mac&page=1#sort_by=Released_DESC&category1=998&os=mac&page=1), per la precisione 3.033 mentre scrivo. A novembre 2015 erano 2.500.

Nell’[analisi di MacGamer HQ](http://www.macgamerhq.com/news/steam-mac-games/) i numeri sono positivi: il numero di giocatori con Mac, nonostante sia solo il 3,4 percento di tutti i frequentatori di Steam, è in continuo aumento e ha superato i quattro milioni. Sette dei dieci giochi più praticati su Steam sono a disposizione per Mac. Non va così male.

Fanno tutti schifo? Sono di seconda scelta? Questo non lo so. Per Windows ce ne sono 7.874. Certamente il numero è preponderante, ma alla schiacciante unanimità di una volta ora sembra sopravvenuta una distribuzione più equa, anzi, perfino favorevole a Mac considerando i numeri grezzi: a occhio si vendono sei PC per ogni Mac nel mondo, mentre su Steam ci sono tre giochi per Mac ogni otto giochi per PC.

Non sono esperto né cliente assiduo di Steam: che cosa segna l’inferiorità di Mac? Va bene tutto, siamo qui per imparare, tranne considerazioni numeriche: prima di capire che non c’è un gioco degno di questo nome su tremila bisogna provarne tremila, e non bastano anni.