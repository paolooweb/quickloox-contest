---
title: "A valle dell’Eco"
date: 2016-02-21
comments: true
tags: [Eco, Infernet, rosa, Foucault]
---
[Il nome della rosa](https://it.wikipedia.org/wiki/Il_nome_della_rosa) mi ha entusiasmato; [Il pendolo di Foucault](https://it.wikipedia.org/wiki/Il_pendolo_di_Foucault) mi ha intrigato; [L’isola del giorno prima](https://it.wikipedia.org/wiki/L%27isola_del_giorno_prima) mi ha lasciato indifferente e ho ignorato la produzione successiva.<!--more-->

Negli anni novanta presenziai a un suo evento presso l’Università degli studi di Milano. Definì la rete *Infernet* e profetizzò che il mondo si sarebbe diviso in due, quelli che usavano il computer e quelli che usavano la televisione.

Posi una domanda: che sarebbe successo entro pochi mesi, con l’avvento di computer capaci di mostrare i programmi TV? Mi liquidò con una battuta.

Ho letto con gusto molte delle sue [Bustine di Minerva](http://espresso.repubblica.it/opinioni/la-bustina-di-minerva). Solo quella in cui [paragonava le piattaforme di *computing* alle grandi religioni](http://www.cyberteologia.it/2011/10/steve-jobs-e-i-gesuiti-secondo-umberto-eco/) mi ha lasciato freddo. Troppo facile, troppo scontato.

Una grande mente che peccava per arroganza intellettuale e mancanza di umiltà.