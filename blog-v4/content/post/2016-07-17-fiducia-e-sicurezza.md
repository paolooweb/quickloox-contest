---
title: "Fiducia e Sicurezza"
date: 2016-07-17
comments: true
tags: [Windows, Gianluca, Bologna]
---
**Gianluca** invia questo piccolo (due metri e mezzo abbondanti) capolavoro dalla stazione ferroviaria di Bologna.

Una volta le Ferrovie dello Stato avevano come motto Fiducia e Sicurezza.

Mai molto credibile, devo dire. Con queste cose si va verso Facciamo Senso.

 ![Tabellone Windows alla stazione di Bologna](/images/bologna.jpg  "Tabellone Windows alla stazione di Bologna") 

*[Per chi preferisce sistemi più a misura di persona c’è un [libro in preparazione](/blog/2016/06/15/un-kickstarter-per-cambiare-il-libro/), che sarà più di un libro. Sono gli ultimi giorni per [farlo nascere davvero](https://www.kickstarter.com/projects/295108589/cuore-di-mela-i-segreti-di-apple-iphone-e-mac) con passaparola e sostegno concreto.]*