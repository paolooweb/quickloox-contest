---
title: "Browser a metà"
date: 2018-01-02
comments: true
tags: [Edge, StatCounter, NetApplications]
---
Cerchiamo di cominciare l’anno in maniera ordinata, con le informazioni a posto e possibilmente con le informazioni *vere*.<!--more-->

Nelle statistiche globali l’uso effettivo di Edge, il *browser* successore del nefasto Internet Explorer, [è la metà di quello che è stato calcolato finora](https://www.computerworld.com/article/3242165/microsoft-windows/microsofts-edge-browser-is-in-serious-trouble.html).

Perfino [NetApplications](http://www.netapplications.com), in passato capace di varie bassezze pur di mostrare dati di traffico il più possibile favorevoli a Windows e a Internet Explorer, ha gettato la spugna. Metà del traffico attribuito a Edge si deve a *bot*, automi software, il più delle volte azionati da organizzazioni criminali nell’ambito di qualche frode o attività paralegale.

Gli umani che usano Edge sono meno di uno su sei. Chi ne conosce uno gli parli. Anche lui può cominciare l’anno in modo migliore, magari a partire da un *browser* migliore.