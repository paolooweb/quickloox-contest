---
title: "Cose che vorrei sentire più spesso"
date: 2015-01-25
comments: true
tags: [LogicProX, Raccuglia, Deltaframe, RogueAmoeba, PodCleaner]
---
Pensando più in grande, Rogue Amoeba ha effettuato un *restyling* s-t-r-e-p-i-t-o-s-o di [Audio Hijack](http://www.rogueamoeba.com/audiohijack/) con la versione 3.<!--more-->

Interfaccia completamente rivisitata, stupendamente chiara e intuitiva, bella da guardare, invoglia da sola prima di qualsiasi specifica tecnica. Una cosa che riconcilia con il software. Hanno anche pensato a un ammorbidimento del prezzo per chi arriva da versioni precedenti. Un *must* per chi ha bisogno delle sue funzioni.

Pensando più in piccolo (in senso positivo), l’amico [Alex](http://www.deltaframe.com/tv-commercials/) ha pubblicato una cosa deliziosamente *Apple-like* che si chiama [PodCleaner](http://www.podcleaner.com). Se c’è dell’audio, il programma lo ripulisce dai rumori indesiderati, elimina le pause se serve, comprime e normalizza, sincronizza più fonti diverse alle prese con lo stesso audio, insomma restituisce il sonoro come avrebbe dovuto essere dall’inizio.

Facile, semplice, preciso nel fare quello che serve, si può provare gratis per quindici giorni e poi costa 2,99 euro. Se si fanno *podcast*, interviste, pasticci creativi con l’audio, PodCleaner elimina tutta una serie di fastidi e frustrazioni per un importo ridicolo. Quando si parla di rapporto prestazioni/prezzo, è questo che significa.

Pensando molto in grande, mi dicono che l’ultimo aggiornamento di [Logic Pro X](https://www.apple.com/it/logic-pro/) di Apple sia particolarmente valido. Non ho modo né competenza per verificarlo, ma era un po’ che non vedevo commenti del genere.

Vorrei sentire più spesso di software così.