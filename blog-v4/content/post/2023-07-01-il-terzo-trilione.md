---
title: "Il terzo trilione"
date: 2023-07-01T23:00:38+01:00
draft: false
toc: false
comments: true
categories: [Internet]
tags: [Apple]
---
Tappa puramente simbolica ma, appunto, simbolica: almeno per un attimo [la valutazione in Borsa di Apple ha superato i tre trilioni di dollari](https://twitter.com/joshuajames/status/1674877404148535309).

Fu detto anni fa che questa azienda avrebbe dovuto chiudere e ridare i soldi agli azionisti. Anni dopo e a seguito di una promessa esplicita di Steve Jobs, è praticamente diventata una *blue chip*, un titolo su cui puntare a occhi chiusi.

<blockquote class="twitter-tweet"><p lang="en" dir="ltr">Hello 3T!<br>(I wonder what Dell is worth by comparison?)<br><br>Prediction: MSFT will be next, maybe in 2024? <a href="https://t.co/KJ9DLB3PI2">pic.twitter.com/KJ9DLB3PI2</a></p>&mdash; ɐnɥsoſ (@joshuajames) <a href="https://twitter.com/joshuajames/status/1674877404148535309?ref_src=twsrc%5Etfw">June 30, 2023</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

La dimensione e la conseguente complessità del business a questo punto sfuggono alla vista dei più. Sarà interessante vedere che tipo di sviluppo è possibile per una società così grande e fondamentalmente sana dal punto di vista finanziario.