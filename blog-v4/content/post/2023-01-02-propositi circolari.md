---
title: "Propositi circolari"
date: 2023-01-02T01:53:17+01:00
draft: false
toc: false
comments: true
categories: [Web]
tags: [Perfect Circle]
---
Ecco il 2023 in un sito [semplice da capire, difficile da padroneggiare](https://neal.fun/perfect-circle/).

Non riuscirò a essere perfetto ma posso provarci ogni giorno ed esultare anche per il più piccolo dei progressi.