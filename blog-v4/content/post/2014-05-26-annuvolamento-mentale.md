---
title: "Annuvolamento mentale"
date: 2014-05-26
comments: true
tags: [Wilson, cloud, Apple, Asymco, Dediu, iTunes, app, ebook]
---
Fred Wilson di Union Square Ventures, un grande fondo di investimento tecnologico, [sostiene](http://techcrunch.com/2014/05/05/vc-fred-wilson-in-20-years-apple-wont-be-a-top-3-tech-company-google-and-facebook-will/) che per il 2020 Apple sarà fuori dalle prime tre aziende del settore. Il che può benissimo essere, però sorprende il motivo:<!--more-->

>L’azienda è troppo radicata nell’hardware e le loro realizzazioni cloud sono largamente non buone. Non credo che pensino ai dati e al cloud.

Horace Dediu di Asymco ha messo alla prova questa affermazione con [qualche dato](http://www.asymco.com/2014/05/09/measuring-not-getting-the-cloud/):

* 800 milioni di utilizzatori di iTunes.
* 13 miliardi di dollari in transazioni riguardanti *app*.
* un miliardo di dollari l’anno in vendita di *ebook*.
* 9 miliardi di dollari pagati agli sviluppatori di *app*.
* 2,7 miliardi di vendite annuali di musica digitale.

La sua conclusione:

>Se ci sono dati confrontabili rispetto ad aziende che al contrario di Apple pensano ai dati e al cloud, sarò felice di confrontarli per calibrare l’entità del fallimento.

Là fuori c’è gente con il cervello annuvolato.