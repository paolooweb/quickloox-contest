---
title: "Varie da viaggio"
date: 2014-02-24
comments: true
tags: [Keynote, ViaMilano, Textastic, Pythonista, Waterdeep, iPad, Attivissimo, Google, calendari]
---
È un periodo di grandi spostamenti. Mi trovo a Malpensa a collegarmi al Wi-Fi dell’aeroporto, amministrato da ViaMilano. Mi scordo di essermi già registrato e reinserisco la mail già usata. Il messaggio di errore che appare è…<!--more-->

>Email just present.

Totò e Peppino, attuali come mai.

<iframe width="420" height="315" src="//www.youtube.com/embed/3evGvLM0FSw" frameborder="0" allowfullscreen></iframe>

Ho tenuto una presentazione in ambito aziendale grazie a iPad e Keynote, registrandola con iPhone. Prima di iOS 7 – e il nuovo Keynote – riuscivo ad avere in funzionamento parallelo su iPad anche il programma di registrazione audio. Adesso li sto provando uno dopo l’altro, ma nessuno resta in funzione e si spegne dopo pochissimo. Il sospetto è che su un iPad di terza generazione cominci a fare difetto la Ram e così il sistema operativo, per tutelare Keynote, termini spietatamente tutto quello che pretende di stare a funzionare dietro le quinte.

L’aggiornamento di [Textastic su iPad](https://itunes.apple.com/it/app/textastic-code-editor-for/id383577124?l=en&mt=8) per iOS 7 è una bellezza. Mi riprometto sempre di iniziare ad approfondire [Pythonista](https://itunes.apple.com/it/app/pythonista/id528579881?l=en&mt=8), però sono debole e cedo davanti a una gioia per gli occhi. I miei, almeno.

Due conti dietro una busta? [Calca](https://itunes.apple.com/it/app/calca/id635757879?l=en&mt=8). Ne avevo accennato tempo fa, editor Markdown che elabora calcoli numerici. Mi sono liberato di tutte le calcolatrici che potevo.

Anche di [D&D Lords of Waterdeep](https://itunes.apple.com/it/app/d-d-lords-of-waterdeep/id648019675?l=en&mt=8) avevo già parlato. Sottoposto a collaudo vero durante spostamento in aereo e rete assente: bello, bello, bello. Gioco da tavolo con ambientazione *fantasy*. Chi gradisce il genere scoprirà una perfetta realizzazione su iPad. Non sono riuscito a trovare pecche nell’amministrazione della meccanica di gioco e soprattutto iPad serve a fare quello che veramente va fatto: eliminare la complessità della gestione dei punteggi, dei mazzi di carte eccetera. Devo ancora impratichirmi, mi si batte facile e mi si trova come *lux*. Insomma, posso elargire una soddisfazione di gioco.

Paolo Attivissimo [tira in ballo il *Pensiero Unico di Apple*](http://attivissimo.blogspot.it/2014/02/agende-google-condivise-in-ios7.html) per via di un suo problema con i calendari di Google, che trova soluzione su Google: per me è finora la migliore prestazione allucinata del 2014.

Sì, il viaggio è stato lungo.