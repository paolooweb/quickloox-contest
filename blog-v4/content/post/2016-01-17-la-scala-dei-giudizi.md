---
title: "La scala dei giudizi"
date: 2016-01-17
comments: true
tags: [2015, Apple, Six, Colors, tv]
---
[Dicevo](https://macintelligence.org/posts/2016-01-08-realistiche-aspettative/) che non tutti hanno capito quanto sia stato impegnativo il 2015 per Apple in termini di uscite di prodotto. E contemporaneamente continuo a leggere commenti in giro di gente per cui chi parla di Apple lo fa solo bene perché è fanatico, non obiettivo, cieco ai difetti eccetera.

Una buone prova del nove consiste nel rispondere al [questionario di Six Colors](https://docs.google.com/forms/d/1xiU3_RyDeZZfY0tmHMB7y5C01tY5-UHncUf6bulNeUY/viewform), con un voto da 1 (minimo) a 5 (massimo) rispetto a undici categorie giudizio, da *Apple TV* a *Software*. Se si evitano i commenti e si lasciano solo i voti, bastano un paio di minuti.

Prendere nota dei voti, mentre li si danno. Ordinarli dal più alto al più basso.

Verificare quanto la propria classifica personale coincida con quella di un comitato di [ventiquattro grandi nomi del mondo Apple](https://sixcolors.com/post/2016/01/apples-final-report-card-for-2015/) tra blogger, giornalisti, programmatori.

Scommetto che le due classifiche saranno piuttosto simili. Non anticipo le medie dei voti fornite dal comitato, per non influenzare il giudizio di ciascuno. Solo andrebbero guardate e poi confrontate con l’ipotesi che chi parla di Apple lo faccia sempre e solo bene.