---
title: "Macchine intelligenti"
date: 2024-03-15T01:40:55+01:00
draft: false
toc: false
comments: true
categories: [Software, Hardware]
tags: [Symbolics, Lisp, Nvidia, Macintosh II, Ai, intelligenza artificiale]
---
Questo anniversario dovrebbe essere meno singolare. Esattamente trentanove anni fa Symbolics.com diventava il primo dominio .com della storia.

Di questi tempi è interessante sapere anche a chi appartenne. [Symbolics](https://en.wikipedia.org/wiki/Symbolics), la società, nacque il 9 aprile del 1980 con l’obiettivo di sviluppare *Lisp machine*, computer basati e ottimizzati per il linguaggio [Lisp](https://lisp-lang.org), con specifiche proibitive a chiunque non fosse almeno un laboratorio di ricerca.

Lisp era sulla cresta dell’onda in quanto ritenuto particolarmente adatto per lavorare su applicazioni di… intelligenza artificiale. C’era molto ottimismo nell’aria e l’idea che entro pochi anni lo sviluppo avrebbe potuto portare a macchine intelligenti, capaci di pensare come un umano.

Una decina di anni dopo, iniziarono ad affermarsi i *sistemi esperti*: software in grado di prendere decisioni e formulare giudizi sulla base di insiemi di regole. I sistemi esperti sapevano aiutare i medici nella diagnostica, analizzare le riprese aerofotogrammetriche alla ricerca di giacimenti petroliferi o siti di interesse archeologico eccetera.

Solo che non era più intelligenza artificiale, ma applicazione commerciale. Le logiche di mercato erano completamente diverse e Simbolics uscì lentamente dal mercato, non prima di reinventarsi sotto forma di [schede di espansione](http://c2.com/wiki/remodel/?SymbolicsMachine) per i [Macintosh II](https://lowendmac.com/1987/mac-ii/). Per il tempo, capolavori di tecnologia.

Corro molto in fretta sulla linea del tempo, ma di fatto la chiusura di Symbolics nel 1996 (sarebbe carino raccontare che cosa è successo dopo al marchio e ai prodotti e magari lo faremo) coincide con l’inizio del cosiddetto inverno dell’intelligenza artificiale, quando tutta l’attenzione va sul lato biecamente commerciale, i fondi e lo sviluppo abbandonano la ricerca pura e il progresso reale della disciplina entra in una fase di stagnazione.

Ora, chissà perché vado raccontando queste storie proprio di questi tempi. Non ci sono più Symbolics che chiudono perché l’intelligenza artificiale diventa oggetto di sfruttamento commerciale; piuttosto, Nvidia – per fare un nome – ha moltiplicato gli incassi grazie alla vendita di unità di elaborazione grafica da adibire al funzionamento dei grandi modelli linguistici.

Però può essere che entità intelligenti, macchine o altro, ascoltino e capiscano in che fase ci troviamo veramente rispetto all’intelligenza artificiale.