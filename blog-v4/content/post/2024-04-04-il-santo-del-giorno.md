---
title: "Il santo del giorno"
date: 2024-04-04T00:29:53+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [Time Machine, Terminale, rsync]
---
Qualcuno conoscerà la storia del ragazzo giovane e dinamico che, alle prese con uno schermo pieno di finestre di terminale e un problema abbastanza complicato da richiedere tutta la possibile concentrazione, a un certo punto effettua per errore un rsync tra una porzione imprecisata della cartella Inizio e il nulla.

Il ragazzo, dentro naturalmente, sono io e la storia data alla sera, guarda un po’, del primo di aprile.

A un certo punto ho cominciato ad accorgermi che alcuni programmi si avviavano e pensavano di essere su un nuovo Mac. Altri, tipo Foto e Musica o Yojimbo, si avviavano senza problemi, ma erano vuoti, niente più musica, niente più foto, niente più password conservate per backup. Gli screenshot si salvavano senza motivo nel Desktop invece che nella cartella da me un tempo designata e Safari non autocompletava più gli indirizzi che ero abituato a vedere autocompletati.

Man mano emergeva la dimensione del disastro senza che riuscissi a circoscrivere con precisione la sua area. Numerosi oggetti nella cartella Inizio erano a posto e apparentemente intonsi. Altri invece no.

Dopo avere riparato le falle prioritarie, ho deciso di procedere giorno per giorno e aspettare con calma di individuare problematiche per decidere se risolverle riconfigurando oppure ricorrendo a un backup.

E qui si possono dire le peggiori cose su Time Machine, che condivido tutte: lento, macchinoso, a volte problematico (non voleva recuperarmi la cartella del blog perché c’era dentro un file .DS_Store). Eppure, un passo per volta, programma dopo programma, cartella per cartella, mi ha salvato l’ecosistema e ora, con calma e senza fretta, recupera anche video e foto, che richiedono *molto* più tempo.

Time Machine ha tutti quei difetti lì e molti altri. In più è discreto, economico, efficace e affidabile (diverso è il discorso dell’affidabilità del *disco* di Time Machine, ovvio), a ingombro zero.

Il mio disco di backup è grande il doppio dell’unità interna e l’ho pagato un po’ più di quanto costava l’alternativa più economica. Ora, tramite Time Machine, mi sta ripagando con gli interessi.

Pensa quello che vuoi, ma – se hai scelto il backup con Time Machine – *non lesinare*. Arriva il momento in cui ti viene chiesto il conto delle tue decisioni di acquisto.

A me è arrivato venerdì scorso e sono qui che ogni giorno levo un’ode appassionata al santo da festeggiare, identico ogni nuova mattina, san Time Machine.