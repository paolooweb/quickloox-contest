---
title: "Come fare tredici a Natale"
date: 2019-12-26
comments: true
tags: [Drang, Twitter, Skywalker, Star, Wars]
---
Un appassionato si mette a twittare a raffica dell’ultimo film di *Star Wars* e, per evitare il linciaggio da parte di chi ancora deve vederlo, codifica i messaggi in Rot13.

La procedura per leggerli è elementare, vista la presenza di decodificatori Rot13 [a ogni angolo di strada](https://rot13.com).

Poi arriva Dr. Drang.

Scopre che sono già stati scritti due comandi rapidi per decifrare i messaggi in automatico. Però maltrattano certi caratteri, un po’ per colpa loro un po’ per colpa dell’architettura di Twitter.

Si mette al lavoro, tira in ballo una app, investiga e alla fine produce una versione perfezionata dell’accrocchio. Non sarà programmazione nel senso tradizionale del termine, però sicuramente è scripting alla maniera di Automator e tutto in locale sull’apparecchio.

[Tweet in Rot13 decifrati in automatico](https://leancrew.com/all-this/2019/12/rot13-a-twitter-odyssey/).

A che serva lo scrive Drang stesso, in modo definitivo:

>Vedo in giro pochi tweet in ROT13 e non riesco a immaginarmi troppo spesso alle prese con questo comando rapido. Secondo un calcolo strettamente economico, il lavoro non è valso lo sforzo. Ma è stato divertente esplorare la logica di due comandi rapidi di altri, dissezionare la struttura dell’Html che arriva in cambio di un Url di Twitter e poi capire come maneggiare i casi limite (che sono sicuro essere più di quelli che ho affrontato).

Chi avesse davanti qualche giorno di tempo libero, potrebbe pensare di approfondire. Per me è stato come il whisky torbato alla fine del pranzo di Natale: quello che ci voleva.
