---
title: "Un voto da cinque euro"
date: 2016-08-08
comments: true
tags: [Working, Copy, Bitbucket, cuoredimela, iPhone, iPad, Mac, BBEdit, CodeBucket, Git2Go, Editorial]
---
Sto organizzando la macchina per lavorare il meglio possibile su [Cuore di Mela](http://cuoredimela.accomazzi.it) e trovo indispensabile poter lavorare da iPad – e magari eccezionalmente anche da iPhone – sul *repository* [Bitbucket](https://bitbucket.org) che è stato attivato allo scopo. Tramite Bitbucket e [Git](https://git-scm.com) è possibile lavorare a più mani sullo stesso progetto senza conflitti, anche se si lavora contemporaneamente sullo stesso file (in caso raro di conflitti, è facile risolverli). Voglio poter accedere al sistema da ognuna delle mie macchine. Su iPhone e iPad voglio poter lavorare sui file con [Editorial](http://omz-software.com/editorial/).

Su Mac è facile fare da Terminale e poi lavorare sui file con [BBEdit](http://www.barebones.com/products/bbedit/). Su iOS le possibilità (serie) che conosco sono [CodeBucket](http://codebucket.dillonbuchanan.com), [Git2Go](http://git2go.com) e [Working Copy](https://workingcopyapp.com).

Le tre *app* si scaricano gratis.

CodeBucket, prima di lasciarmi clonare il *repository* in locale, vuole dieci euro sulla fiducia.

Git2Go, prima di lasciarmi clonare il *repository* in locale, vuole dieci euro sulla fiducia.

Working Copy mi lascia clonare il *repository*, mi lascia collegare Editorial e anche lavorare in locale. Per inviare i cambiamenti effettuati verso Bitbucket, vuole quindici euro.

Cinque euro in più, ma intanto ho collaudato e verificato tutto, non ho nessuna sorpresa e ho anche già cominciato a lavorare. Il che vale molto più dei cinque euro extra.

Voto pacificamente Working Copy.