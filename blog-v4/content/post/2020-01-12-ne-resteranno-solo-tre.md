---
title: "Ne resteranno solo tre"
date: 2020-01-12
comments: true
tags: [Tidbits, Macintosh, Finder, iMac]
---
Riflessione secondaria ma interessante quella di Tidbits sul fatto che [Apple da qualche anno venda *Mac* e non più *Macintosh*](https://tidbits.com/2020/01/10/the-one-remaining-use-of-the-word-macintosh/).

Il termine non è stato rinnegato: viene usato routinariamente per ricordare che nel 1984 ha debuttato Macintosh. Tuttavia non viene più associato ai prodotti odierni, salvo che per una eccezione: il disco interno dei Mac si chiama *Macintosh HD*.

Nei commenti, i lettori hanno aggiunto che sulla scatola di iMac compare ancora la scritta *Macintosh* e si può persino leggere *Think Different*. (Almeno in USA, devo ancora verificare per l’Italia). Inoltre, le Informazioni sul Finder contengono la scritta *The Macintosh Desktop Experience* (anche qui in lingua inglese, non so l’italiano).

Sicuramente *Mac* è più ficcante, incisivo, moderno di *Macintosh*, che in altre lingue si può magari ricevere più a fatica e comunque è frutto di un errore di stampa mai più rimediato (la mela di ispirazione è una californiana McIntosh). Un curioso come me, peraltro, può solo passare del tempo a chiedersi come mai non abbiano mai cambiato quelle tre ultime istanze del marchio, sopravvissute a qualsiasi cambiamento di strategia di *branding*.