---
title: "Innovazione trasversale"
date: 2014-04-01
comments: true
tags: [Jobs, Mac, iPad, Windows, Microsoft, mouse, iPod, iTunes, touch, Cupertino]
---
Dal discorso di Steve Jobs [sui camion e le automobili](http://www.cnet.com/news/steve-jobs-at-d8-post-pc-era-is-nigh/) è chiaro che iPad costituisce la piattaforma del futuro. Amiamo i nostri Mac, i quali però diventeranno un oggetto per pochi operatori specializzati. L’umanità si rivolgerà a computer molto più semplici e snelli, nati per fare l’essenziale, esattamente come iPad. Come è passata dalla civiltà agricola, dove tutti avevano un furgone, a quella urbana, dove tutti guidano un’auto o un veicolo ancora più snello.<!--more-->

Aggiungiamo che è uscito Office per iPad. Un evento che accade di rado: chi credeva che Apple avrebbe mai fatto iTunes (e iPod) per Windows? Eppure è successo e e ha posto le basi dell’affermazione di iTunes.

La grande novità di questo secolo è stata Google (Apple esisteva da un pezzo), il cui tratto principale è offrire software assolutamente per tutti, da Google Documenti a Chrome. Si vince se si opera trasversalmente e si offrono i propri migliori prodotti anche agli avversari.

Il ruolo di Apple è di grande anticipatrice delle tendenze del mercato – il suo merito è presentare prodotti che il pubblico non sapeva di volere – e soprattutto di fare le cose nel modo giusto. iPhone non è un incredibile progresso tecnologico, ma è il cellulare fatto nel modo giusto.

Sarà Apple dunque ad anticipare la prossima tendenza, che condensa tutte quelle esposte finora: fare le cose nel modo giusto e agire trasversalmente, ampliando la diffusione dei propri prodotti migliori. L’anticipazione sarà qualcosa che i concorrenti non hanno neppure iniziato a capire: lavorare trasversalmente *su se stessa*.

Le grandi rivoluzioni portate da Steve Jobs e compagni sono state il mouse, l’interfaccia a finestre e iPad. Ecco perché la prossima grande novità sarà portare mouse e interfaccia a finestre e mouse su iPad.

Non come ha cercato goffamente di fare Microsoft con Windows 8, ma appunto nel modo giusto. Lo schermo diventerà più grande per facilitare l’interazione con l’interfaccia a finestre. Il mouse cambierà radicalmente, fatto nel modo giusto; lo si monterà *sull’indice*. Correrà sullo schermo comodamente, e per fare clic basterà una leggera pressione del dito. Nessun bisogno di scrivania e piena aderenza alla filosofia *touch*.

Scettici? Bisogna saper distinguere la notizia-spazzatura dalla vera anticipazione. Già qualcuno [ha subodorato](http://www.digitimes.com/news/a20131223PD210.html) l’arrivo di un iPad più grande. Far funzionare un mouse su iPad è [questione di poco](https://www.youtube.com/watch?v=Hb_PDJ1poBg), la vera innovazione sarà la miniaturizzazione che lo monterà sull’indice.

<iframe width="560" height="315" src="https://www.youtube.com/embed/Hb_PDJ1poBg" frameborder="0" allowfullscreen></iframe>

Manca l’interfaccia a finestre. In pratica [OS X su iPad](https://www.youtube.com/watch?v=ByvqdZRZujw).

<iframe width="560" height="315" src="https://www.youtube.com/embed/ByvqdZRZujw" frameborder="0" allowfullscreen></iframe>.

Nel corso di un evento a sorpresa, Apple annuncerà oggi alle dieci del mattino ora locale, dall’*auditorium* del *campus* di Infinite Loop a Cupertino, la fusione del concetto di computer-furgone, Mac, con quello di computer-auto, iPad. L’unica incertezza che resta è sapere se lo chiameranno **iMad** o **iPac**. [Lo sapremo](http://xrl.us/bqsr8v) stasera dalle 19 ora italiana.