---
title: "Nuove sensazioni"
date: 2018-12-07
comments: true
tags: [Time, Machine, iPad, Pro, Face, ID, Mac, macOS, Mojave, Bitcoin]
---
Nuovo hardware e nuovo software dopo molto tempo e, così, nuove esperienze.

Su iPad Pro ho scaricato una app e, invece di dover digitare il lunghissimo ID Apple di ordinanza, ho schiacciato due volte il pulsante di accensione. Autenticazione facciale e via libera alla app, senza premere altro. Adesso il mio ID Apple mi sembra ancora più lungo di prima.

Come d’abitudine, ho installato macOS Mojave su un disco esterno, per i casi di emergenza. La manovra tuttavia richiede di entrare nella partizione di recupero e di abilitare Mac al boot con dischi esterni tramite una utility apposita. Altrimenti non si può fare.

Dovevo azzerare un disco usato come Time Machine e non più necessario. Come esperimento, invece di usare Utility Disco, ho buttato un terabyte di archivio nel Cestino e ho vuotato il suddetto.

Il disco (Usb 3) ha lavorato oltre ventiquattro ore per eliminare oltre tre milioni e trecentomila file. Quando il computer procede così indefessamente rimango sempre sbalordito, anche se ci sono l’esperienza e la conoscenza della complicazione interna degli archivi Time Machine.

Mi è arrivata l’email del finto pirata che sostiene di avere un video ripreso di soppiatto dal mio computer mentre guardavo un sito porno, video che verrà pubblicato se non pago l’equivalente di mille euro in Bitcoin. Tenerissimo, mi chiedo se veramente qualcuno pagherà.