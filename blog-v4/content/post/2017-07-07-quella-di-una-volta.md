---
title: "Quella di una volta"
date: 2017-07-07
comments: true
tags: [Microsoft, OneDrive]
---
È un momento di luci della ribalta per Microsoft. La notizia è la [diffida dall’utilizzo di OneDrive su dischi non Ntfs](https://arstechnica.com/information-technology/2017/07/onedrive-has-stopped-working-on-non-ntfs-drives/).

Sì, esatto, il disco di rete, quello fatto apposta per funzionare in tutto l’universo e su qualunque sistema operativo, pretende di risiedere localmente sul filesystem Microsoft.

Come se iCloud Drive imponesse a chi lo usa su PC di formattare il disco con il filesystem di Mac. Assurdo, è negare il senso di iCloud Drive. Come se Dropbox chiedesse di appoggiare le proprie cartelle esclusivamente sul filesystem usato da, boh, Linux Debian. Come se Google imponesse l’archiviazione dei documenti condivisi su un Chromebook.

Il bello è che secondo Microsoft il messaggio di errore comparso da ieri avrebbe dovuto essere presente da sempre. Se ne sono soltanto dimenticati. OneDrive, disco di rete offerto per lavorare ovunque, non accetta un disco qualunque.

La Microsoft di una volta. Quella che *embrace, extend,extinguish*. Prendo una convenzione, la stravolgo in modo che ci siano vantaggi accessibili unicamente sui miei sistemi e quando ho raggiunto l’obiettivo vincolo tutti all’uso di questi ultimi. Concorrenza, interoperabilità, multipiattaforma, fesso chi ci ha creduto, si arrangi.

La buona notizia è che bisogna subire la decisione solo usando Windows. OneDrive per Mac o iPad, a quanto capisco, non sono interessati dalla follia. Non per ora, almeno.

Per il resto, penso a uno che abbia pagato un abbonamento a Office 365 completo di OneDrive, si sia creato uno *storage* su una microscheda SD per comodità, e da domani non lo può più usare. Bella valorizzazione dell’investimento.

Stai a vedere che piano piano torna la Microsoft di una volta, che con le buone o con le cattive fai come dico io. Con la novità che i primi a pagare sono quelli che hanno pagato.