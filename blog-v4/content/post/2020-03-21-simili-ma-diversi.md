---
title: "Simili, ma diversi"
date: 2020-03-21
comments: true
tags: [Verge, iPad, Pro, trackpad]
---
Che l’uso di un iPad possa implicare la presenza del cursore sullo schermo in certe situazioni, ci può stare. Usato con una tastiera, a mo’ di portatile, lo schermo touch non è ergonomico. È il motivo per cui finora non si è visto un Mac con schermo touch.

L’importante è che il cursore su iPad non lo trasformi [in una sorta di Mac-simile](https://macintelligence.org/posts/2020-02-29-il-privilegio-di-poter-dire-no/).

Per fortuna la [recensione di The Verge](https://www.theverge.com/2020/3/18/21185188/ipad-trackpad-how-to-support-mouse-cursor) del nuovo iPad Pro e Magic Keyboard relativa porta buone notizie, ovvero la conferma che il supporto del cursore su iPad è studiato apposta per l’apparecchio, come mostrano le tre caratteristiche principali:

- il cursore appare solo se serve;
- è un circoletto, non una freccia;
- cambia forma in funzione dell’oggetto su cui si trova.

La prima caratteristica è di gran lunga la più importante. La seconda spiega bene che l’apparecchio continua a essere basato fondamentalmente sul tocco. La terza è un miglioramento interessante e benvenuto dell’interfaccia umana. Che, volendo, [si può anche spegnere](https://twitter.com/stroughtonsmith/status/1240339054870364160) se dà fastidio.

<blockquote class="twitter-tweet"><p lang="en" dir="ltr">You can turn off cursor animations/snapping in accessibility settings! 😄 I’m definitely going to keep this off /cc <a href="https://twitter.com/viticci?ref_src=twsrc%5Etfw">@viticci</a> <a href="https://t.co/bHq1Oy0cXR">pic.twitter.com/bHq1Oy0cXR</a></p>&mdash; Steve Troughton-Smith (@stroughtonsmith) <a href="https://twitter.com/stroughtonsmith/status/1240339054870364160?ref_src=twsrc%5Etfw">March 18, 2020</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

La [presentazione del trackpad per iPad Pro da parte di Craig Federighi](https://www.youtube.com/watch?v=02hnXEGn9Zg) è rinfrancante per chi auspica che iPad rimanga iPad, con la sua *leggendaria versatilità*. E in più possa azionare un cursore quando faccia comodo.

<iframe width="560" height="315" src="https://www.youtube.com/embed/02hnXEGn9Zg" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>