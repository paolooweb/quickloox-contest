---
title: "Aprire la mente e Apple TV"
date: 2021-11-17T03:21:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [Apple TV, Super Mario, Xcode] 
---
Possedere una copia di Super Mario 64, [seguire le istruzioni](https://github.com/ckosmic/sm64ex-ios/wiki/Building-for-iOS-(via-Xcode)) e compilare una versione del gioco, perfettamente legittima, che gira su tv, grazie a Xcode e a un Mac.

Non serve veramente a nulla ma insegna una montagna di cose. Soprattutto, espone a nozioni che si potrebbero volere approfondire, a sapere che esistono. Di modi per saperlo, questo è uno dei più interessanti che abbia visto ultimamente.

_Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile._