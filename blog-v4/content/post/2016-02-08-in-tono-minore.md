---
title: "In tono minore"
date: 2016-02-08
comments: true
tags: [Super, Bowl, 50, L, Game, Pass, Nfl, iPad, Carolina, Panthers, Denver, Broncos, Newton, Fox, Sky]
---
Dopo gli ultimi due anni era difficile che questo cinquantesimo Super Bowl potesse essere altrettanto emozionante e non lo è stato. Cam Newton ([Carolina Panthers](http://www.panthers.com)) ha pagato il debito di esperienza e deve essere comunque contento di essere arrivato in finale. Il gioco offensivo dei Panthers è risultato sottotono e dall’altra parte a Peyton Manning, fin troppo navigato, è bastato fare meno errori. A parte uno straordinario *touchdown* di Carolina in salto sopra le teste dei [Broncos](http://www.denverbroncos.com), la partita ha offerto poco. Bellino lo spettacolo di metà partita, con una conclusione coinvolgente.<!--more-->

Esclusa l’opzione di presenziare fisicamente a [Santa Clara](http://www.levisstadium.com), arrivare virtualmente al Super Bowl è stato scomodo. Non avevo l’abbonamento giusto su Sky né su Mediaset Premium e nemmeno tempo più voglia di attivare una Vpn per ricevere lo streaming di Cbs.

Non capisco l’impossibilità di acquistare su Sky la singola visione del Super Bowl, anche a prezzo eccessivo, come invece permetteva – sempre a prezzo eccessivo – [Nfl Game Pass](https://itunes.apple.com/it/app/nfl-game-pass/id459244446?l=en&mt=8) su iOS. Ci sono arrivato dopo avere tentato su tv, inutilmente: Cbs Sports, titolare dei diritti negli Usa, non consentiva la visione dall’Italia e Nfl aveva un *bug* a causa del quale continuavo a passare da una richiesta di acquisto del Game Pass, senza mai riuscire a soddisfarla.

Nfl Game Pass su iPad ha funzionato perfettamente e mi ha permesso di ridirigere il video sul televisore di casa, in scintillante alta definizione. L’esperienza complessiva dovrebbe tuttavia migliorare, specialmente visto che è a pagamento. Che la partita sia sotto tono può accadere e dipende da mille fattori; volerla guardare dovrebbe essere questione di un clic o due.

Aggiungo a posteriori dopo avere inserito svariate correzioni suggeritemi nei commenti al *post* (ringrazio tutti): a causa di una mia distrazione ho comprato un Game Pass NFL per l’intera stagione su tv, che però non funzionava. Il sistema continuava a proporre l’acquisto a ripetizione, senza dare accesso ai contenuti. Più tardi ho comprato un altro Game Pass su iPad, come scrivevo, e più tardi ancora ho chiesto il rimborso del primo Pass, quello non funzionante, che è avvenuto senza alcun problema in tre clic a partire dalla fattura di acquisto in posta elettronica.