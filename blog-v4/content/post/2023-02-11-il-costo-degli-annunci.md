---
title: "Il costo degli annunci"
date: 2023-02-11T02:19:14+01:00
draft: false
toc: false
comments: true
categories: [Internet]
tags: [Super Bowl, Eagles, Chiefs, Kansas City, Philadelphia, Philadelphia Eagles, Kansas City Chiefs, Rai 2, Apple, Rihanna, Dazn, Nfl, Phoenix, State Farm]
---
Le opzioni italiane per vedere il [Super Bowl](https://www.nfl.com/super-bowl/) di quest’anno sono tranquille: Dazn per i paganti e Rai 2 per i gratuiti.

Il problema è che Rai 2 trasmetterà i propri spot pubblicitari invece che mostrare quelli americani, parte dello spettacolo quasi quanto la partita. Problema doppio, dato che quest’anno a sponsorizzare il concerto dell’intervallo di Rihanna è proprio Apple.

Lo half-time show e gli spot sono una occasione di portata quasi culturale per scrutare l’America, confermare o smentire trend, ingozzarsi di *nachos* e *guacamole* (sì, lo faccio tutti gli anni, oggi minispesa per guardare il Super Bowl come se fossi seduto allo [State Farm Stadium](https://www.statefarmstadium.com) di Phoenix).

Se Nfl, la lega professionistica del football, vendesse anche quest’anno il Game Pass per la partita a novantanove centesimi [come ha fatto l’anno scorso](https://macintelligence.org/posts/2022-02-08-è-tutto-un-prepararsi/), il sacrificio rituale dell’euro necessario lo compierei. Devo ancora verificare.

Non so bene chi tifare. Né Philadelphia né Kansas City, lato football, mi hanno mai ispirato più di tanto. gli Eagles si trovano spesso nel ruolo degli eterni secondi e forse potrebbe emozionare di più una loro vittoria. Però.