---
title: "Due passi avanti e uno indietro"
date: 2017-07-29
comments: true
tags: [A5, A10, iPhone, 64, 32]
---
Due anni e mezzo da quando si raccontava delle difficoltà degli sviluppatori nello spremere al massimo il processore di iPad [a causa del vasto spettro di Cpu riconosciute dal software di sistema](https://macintelligence.org/posts/2014-11-02-vivi-che-camminano/).<!--more-->

Pareva che il problema fosse la difficoltà di supportare i processori più vecchi avendone a disposizione di più potenti e invece era quasi il contrario: l’obbligo di supportarli, che limitava la corsa al massimo della resa.

Oggi abbiamo la progressiva trasformazione di iOS in ecosistema [rigorosamente a sessantaquattro bit](https://macintelligence.org/posts/2017-06-04-velocita-sessantaquattro/) e scommetto che, appena App Store chiuderà definitivamente la strada alle *app* a trentadue bit, ci sarà chi si lamenta della perdita di supporto.

Semplicemente non si può avere tutto: il massimo delle prestazioni, della retrocompatibilità, della sicurezza, del risparmio nei requisiti di sistema, di prestazioni, di longevità, di autonomia, di risparmio nei  consumi eccetera. Ogni modello che esce insegue il miglior compromesso possibile tra tutte le componenti qui elencate e la strategia generale di sviluppo dei sistemi segue lo stesso principio, in barba alle scemate sull’obsolescenza programmate o altre scie chimiche in stile informatico.

In generale c’è progresso; a volte, per fare due passi avanti ne serve uno indietro. Nel caso in questione, il passaggio ai sessantaquattro bit obbligati rappresenta il doppio passo avanti.
