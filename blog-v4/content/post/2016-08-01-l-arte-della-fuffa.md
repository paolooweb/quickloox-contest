---
title: "L’arte della fuffa"
date: 2016-08-01
comments: true
tags: [cuoredimela, social]
---
[Cuore di Mela](http://cuoredimela.accomazzi.it) che comincia è stata una esperienza molto bella di contatto con le persone e di cura di un progetto. Adesso si è già in fase di organizzazione e diventerà una bellissima fatica da compiere nel migliore dei modi. Come già [dicevo](https://macintelligence.org/posts/2016-07-30-quarantotto-ore/), ha insegnato varie lezioni, una delle quali l’effettivo valore dei social media nei rapporti estesi con il mondo.

È infatti la soluzione su cui si è puntato per divulgare il messaggio fuori dalla cerchia dei conoscenti – anche via mail – e frequentatori, insomma al pubblico allargato. Ed è stato un flop (ce lo si aspettava) di dimensioni notevoli (un po’ meno).

Abbiamo aperto aree o pagine dedicate a *Cuore di Mela* su Facebook, Google, LinkedIn e pure un account Twitter. Dopo di che abbiamo svolto comunicazione *organica* (l’atto normale di pubblicare, testi, foto e quant’altro) e abbiamo destinato una somma minima (pochissimi punti percentuali del budget) a comunicazione sponsorizzata.

A leggere i resoconti, il lavoro compiuto non è stato cattivo. Le pagine sono state visitate centinaia di migliaia di volte, da migliaia di persone. Alla fine però le adesioni al progetto provenienti direttamente dai *social* costituiscono neanche un decimo del raccolto totale.

La pubblicità social ha reso circa il doppio di quanto è costata. Un dato bassissimo.

I distinguo che si possono fare sono infiniti. Si potevano mobilitare canali diversi (per esempio Instagram); può darsi che il messaggio non fosse attentamente calibrato; può darsi che il pubblico inseguito per età e residenza non fosse quello giusto; può essere che occorresse più lavoro, e migliore, sulle keyword da sponsorizzare nella ricerca di Google; forse un budget anche lievemente più alto avrebbe costituito una massa critica decisiva eccetera, eccetera, eccetera. Anche vero che magari abbiamo azzeccato qualcosa e avremmo potuto sbagliarlo, così ottenendo risultati persino peggiori.

D’altronde qualsiasi libro sul marketing attraverso i *social media* mette tutte le mani avanti: ci vuole tempo, pazienza, attenzione, affinamento del lavoro, attenta analisi dei risultati bla bla bla.

Assolutamente vero. Vero anche che tutto questo ha un costo da aggiungere a quello materiale.

Nessuno nutre l’illusione di sostituire una campagna televisiva o le affissioni nella metropolitana con poche decine di euro affidati a Facebook o Google. Però i risultati da aspettarsi sono infimi. Per coinvolgere veramente, diciamo, mille persone, lo sforzo necessario è molto superiore a qualsiasi aspettativa e ha costi decisivi, oppure richiede molto molto tempo e molto molto lavoro.

Un’ultima parola sulla pochezza degli strumenti che vengono messi a disposizione di chi è disposto a pagare in cambio di un servizio. Facebook è decente, solo con un’interfaccia che definire confusionaria è poco. Nel mondo Google, guai a chi volesse azzardarsi a governare una campagna pubblicitaria lontano da un desktop: la app dedicata ad Adwords permette solo di guardare, senza toccare (la prima volta sembra uno scherzo… ricordo, si tratta di *dare soldi* a Google). Con Safari su iPad, il sito salta per aria al momento di scrivere il testo di una inserzione. Magari con Chrome funziona tutto, eh. Però, accidenti, si tratta di Safari su iPad e scrivere testo normale in un normale campo testo.

LinkedIn *mi ha impedito* di sponsorizzare contenuti. Ha messo la mia carta di credito *on hold*, in attesa, e lì è rimasta. La stessa carta di cui Facebook e Google hanno approfittato negli stessi giorni senza remore.

Questo però è un errore mio. Mi sono ricordato solo dopo che LinkedIn sta per diventare proprietà di Microsoft. Dare soldi a Microsoft è uno degli errori più grandi compiuti dall’umanità negli ultimi cinquant’anni. Mi è andata bene.

A monte di tutto: che fuffa, i *social*, appena esci dalla chiacchiera con gli amici.
