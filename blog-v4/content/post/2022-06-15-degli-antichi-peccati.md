---
title: "Degli antichi peccati"
date: 2022-06-15T00:38:46+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [Apple, macOS, Ventura, Clarus, dogcow, Accomazzi, Luca Accomazzi, MisterAkko, 512 Pixels, Hackett, Stephen Hackett]
---
Quanto tempo può passare perché la redenzione da un vecchio peccato possa essere ancora considerata valida?

Perché, me lo ha segnalato [MisterAkko](https://www.accomazzi.net), [in macOS Ventura ritorna Clarus, il cane mucca](https://512pixels.net/2022/06/clarus-is-back-in-macos-ventura/) di cui [si parlò giustappunto a fine marzo](https://macintelligence.org/posts/2022-03-31-un-momento-di-chimerica-leggerezza/).

Mancava dal millennio scorso, come indicato nella [guida più completa al cane mucca](https://512pixels.net/dogcow/) che sia riuscito a trovare. A parte una foto rivelatrice in questo [reportage di dicembre 2021](https://www.wallpaper.com/design/apple-park-behind-the-scenes-design-team-interview) che racconta la vita e il lavoro dentro Apple Park.

Mi si dica tutto, ma non che Apple non sia capace di rimediare, qualche volta, agli errori del suo passato.

Più che altro, il dibattito: meglio il vettoriale di adesso o l’originale bitmap?