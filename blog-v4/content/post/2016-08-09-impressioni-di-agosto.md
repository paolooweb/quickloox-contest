---
title: "Impressioni di agosto"
date: 2016-08-09
comments: true
tags: [Android, Pokémon]
---
Un parente ha abbandonato il vecchio cellulare per un Android a basso costo, Samsung. Effettivamente il terminale fa tutto quello che gli si chiede, solo che è tutto lento, con un’interfaccia onesta e proprio scarna e bruttarella. A ogni tocco senti, vedi, capisci che costa poco e  c’è dentro niente più di quello che hai pagato. In casa abbiamo un iPhone 4S oramai venerabile e, dopo l’esperienza, è stato come abbeverarsi a un ruscello di montagna. Quanto meno, quando premi il pulsante scatti una foto. sull’Android aspetti che scatti una foto.

Per vedere l’effetto che fa, ho scaricato [Pokémon Go](http://www.pokemon.com/us/pokemon-video-games/pokemon-go/). L’introduzione al gioco è banale e il più possibile veloce in modo che si passi prima possibile alla parte interessante, la caccia ai Pokémon in realtà aumentata. Ovviamente il gioco pone il primissimo Pokémon proprio nel punto dove ci si trova, nel mio caso una sedia in un giardinetto. La cosa sorprendente è che davanti a me si trovava una sdraio vuota e il Pokémon sembrava appoggiarvisi sopra invece che galleggiare nello spazio come mi sarei aspettato. Una coincidenza o veramente l’immagine della fotocamera viene analizzata al punto da permettere di offrire al mostriciattolo un punto di appoggio? È un giochino, però è il primo che diffonde a livello planetario una modifica della realtà. È il primo assaggio di un futuro dove sarà possibile scegliere la realtà come la si desidera, o quasi. Il mio primo Pokémon virtuale mi ha dato effettivamente un brivido, solo che non so ancora di che cosa.

In occasione delle grandi manifestazioni sportive guardo con curiosità l’interfaccia visiva delle trasmissioni: occupazione dello schermo, font usati per sottopancia e sovrimpressioni varie, statistiche diramate, corredo informativo generale. Da questo punto di vista, rispetto agli Europei di calcio, le Olimpiadi di Rio sembrano un buon usato.
