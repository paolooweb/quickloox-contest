---
title: "La fascetta del vicino è sempre più lunga"
date: 2015-08-29
comments: true
tags: [Lightning, Usb, iPad, iPhone, Mac, MagSafe, ElCapitan, OSX]
---
Tra gli avvenimenti di rilievo di questa estate metterò in archivio anche la quasi rottura, quasi simultanea, dei cavetti Usb-iPhone (connettore Lightning) e Usb-iPad (30 pin).<!--more-->

Tutti i cavetti di Apple hanno una fascetta a protezione del tratto di cavo vicino al connettore, però mai lunga abbastanza: a un certo punto sembra inevitabile che la guaina si laceri lasciando intravedere la parte elettrica, appena prima che inizi la fascetta.

Ho effettuato una riparazione improvvisata con due giri di nastro isolante, che tiene coperta la parte elettrica e rafforza la solidità della guaina. Sembra che siano funzionanti (per confronto ho comunque due cavi nuovi analoghi) ma devo rimandare il giudizio a una versione più avanzata di El Capitan. Questa, sul mio Mac, a volte crea problemi sulle porte Usb e non si capisce se ci sia funzionamento difettoso per via del cavo, oppure per via dei connettori sugli apparecchi, o funzioni tutto tranne El Capitan beta.

Sono preparato anche alla sostituzione del cavo di alimentazione di Mac, vecchio MagSafe di inizio 2009, che inizia a dare segni di incertezza.

Una cosa è certa: poi passeggi sul lungomare, guardi la gente con gli auricolari che ascoltano musica, e le loro fascette sembrano lunghissime, robustissime.