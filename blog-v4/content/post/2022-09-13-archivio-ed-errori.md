---
title: "Archivio ed errori"
date: 2022-09-13T14:36:36+01:00
draft: false
toc: false
comments: true
categories: [Web]
tags: [Steve Jobs Archive, Steve Jobs, Jobs, Ive, Jonathan Ive, Jony Ive, Powell, Laureen Powell, Cook, Tim Cook]
---
L’idea di [un archivio online dedicato ai pensieri di Steve Jobs](https://stevejobsarchive.com) mi sembra doverosa, stimolante, promettente. Come per tutti i grandi, aiuta a preservarne la memoria e poi anche a sviluppare qualcosa di nuovo e persino superiore. Per una figura come Jobs, con aspetti controversi e non sempre pienamente pubblici, è poi importante che vi sia un punto di riferimento utile per valutare correttamente l’imprenditore che fu con una prospettiva storica.

Dei pensieri finora pubblicati (su un sito elegante ed essenziale, tra l’altro), che cosa sceglieresti? Io vado per certo su *Make a lot of mistakes*.

Anche se, più li rileggo, più cresce l’indecisione. Fare un sacco di errori sì, tranne trascurare lo Steve Jobs Archive e non guardare, ascoltare, leggere.