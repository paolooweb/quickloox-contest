---
title: "Il domani arriva"
date: 2013-12-22
comments: true
tags: [3D, Mac]
---
Alla festa di Natale di [Enter Cloud Suite](http://entercloudsuite.it) era presente anche una stampante 3D prodotta da [Type A Machines](http://www.typeamachines.com). Lavorava filando da una bobina di sostanza derivata dal mais e, come si vede, controllata a livello di dettaglio arbitrario da un Mac.<!--more-->

Un modello così – ora il telaio è di lamiera, non di legno – costa 1.600 dollari. Ancora fuori dalla disponibilità comune, ma sempre più vicino. Chi ritenga di essere all’altezza del compito può [costruire la propria stampante da solo](http://www.typeamachines.com/pages/firmware-source) previo scaricamento libero di tutte le specifiche e del software.

Deve ancora finire il 2013, ma credo che di questo passo ci sembrerà ieri già molto presto.

 ![Stampante 3D](/images/type-a.jpg  "Stampante 3D") 