---
title: "Sfida al senso comune"
date: 2017-04-21
comments: true
tags: [Macintosh, iPhone, Pro]
---
Apple ha senso di esistere quando cambia le regole del gioco e, nel bene o nel male, improvvisamente si gioca una partita nuova in modo inedito. Quando si limita ad accomodare il senso comune, il pensiero collettivo, a fare come fanno tutti, è uno spreco. Di aziende conformiste e conformate è pieno il mondo.<!--more-->

Per dire, bella l’idea di un Mac Pro super espandibile e speriamo di vedere presto qualcosa di interessante. Però: la rivoluzione l'ha fatta Macintosh, un computer fatto per restare chiuso, venduto. in mezzo a tonnellate di PC aperti ed espandibili. Macintosh II, la macchina di passaggio verso l’espandibilità, di certo non ha cambiato la storia.

Invece, l’idea di riuscire a produrre centinaia di milioni di apparecchi [con i soli materiali provenienti dal riciclo](https://www.apple.com/environment/) è una follia pura. Se riesce, però, significa riscrivere la storia.

Ancora più incredibile, l’obiettivo viene annunciato prima di avere un’idea precisa di come arrivarci.

_Questa_ Apple, che sfida il senso comune, ha tratti che mi piacciono.
