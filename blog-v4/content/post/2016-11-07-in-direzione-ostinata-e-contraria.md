---
title: "In direzione ostinata e contraria"
date: 2016-11-07
comments: true
tags: [iPhone]
---
La famiglia allargata ha visto di recente l’arrivo di un iPhone SE e la sostituzione di un iPhone 4 con un iPhone 7 (a dirla così sembrano tre generazioni di salto, invece sono sei).

I risultati confermano la mia convinzione, che il calibro delle macchina possa essere inversamente proporzionale alla competenza e all’esperienza. A una persona poco usa alla tecnologia, o poco interessata ad approfondirla, giova assai più una macchina superperformante che la corsa al risparmio.

Mentre, semmai, un megaesperto ipernavigato può dimostrare tutte le sue capacità cavando sangue dalle rape. Perché lui, per definizione, lo sa fare.

Invece è così facile riscontrare l’opinione opposta.