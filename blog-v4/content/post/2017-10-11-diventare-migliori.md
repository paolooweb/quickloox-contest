---
title: "Diventare migliori"
date: 2017-10-11
comments: true
tags: [BBEdit]
---
C’è un motivo in più per cui mi piace molto il *retrocomputing* ma preferisco vivere nel presente e magari, quando possibile, nel futuro: ti costringe a migliorare.

Mi spiego. Viste con gli occhi del 2017, le applicazioni che abbiamo usato e alle quali ci siamo appassionati, beh, erano *semplici*.

Anno dopo anno sono arrivate nuove versioni sempre più potenti e versatili e anche nuove applicazioni, costruite su modelli più efficaci e moderni.

La nostra crescita e il nostro rendimento – in senso astratto, non parlo di stipendi o di obiettivi di produzione – erano in un certo senso legate alla crescita del nostro parco software. E anche dell’hardware: ogni modello nuovo era un passo in avanti in capacità, in potenza, in specifiche di cui non si poteva fare a meno per restare sulla breccia.

Oggi tutto questo è finito o, meglio, si evolve diversamente. Per me avere la versione più aggiornata di [BBEdit](http://www.barebones.com/products/bbedit/) è irrinunciabile; ma quanto il prossimo BBEdit sarà *significativamente* migliore di quello prima? Certamente risolverà qualche problema e offrirà qualche opportunità in più; ma, onestamente, considererei miracolo una nuova versione capace, che so, di farmi risparmiare un’ora di lavoro a settimana.

Esistono certo altre applicazioni simili a BBEdit, più moderne, con le stesse aspirazioni e si può discutere della superiorità dell’una o dell’altra. Quanto *significativa*, però, questa superiorità? La verità è che mi munisco di un editor di testo all’avanguardia ottengo, complessivamente, le stesse possibilità, qualunque sia l’editor.

La nostra crescita e il nostro rendimento oggi dipendono non più da quella del parco software. Ma da noi. Non dubito di poter trovare cose migliori di [Editorial](http://omz-software.com) su iPad. Migliori al punto di stravolgere il mio flusso di lavoro, tuttavia, no. Se invece riuscissi a venire a capo del *workflow* che mi fa salvare un articolo su Wordpress senza passare dal browser, quello sì, mi cambierebbe le giornate.

Dipende da me. Ecco perché il passato è scintillante, ma l’energia sta tutta nel presente.
