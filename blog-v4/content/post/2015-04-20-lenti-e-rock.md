---
title: "Lenti e rock"
date: 2015-04-21
comments: true
tags: [P8, Huawei, iPhone6, Samsung]
---
Confrontare un P8 Huawei e un iPhone 6 [snocciolando numeri](http://www.itproportal.com/2015/04/16/huawei-p8-apple-iphone-6-full-specs-comparison/) è lento, lentissimo.<!--more-->

Mettere un iPhone e un Samsung in mano a bambini dai sette agli undici anni e [fare decidere loro](http://uk.businessinsider.com/kids-choose-between-iphone-and-samsung-2015-4?r=US) è piuttosto *rock*.