---
title: "Condannati a ripetere"
date: 2016-11-10
comments: true
tags: [Silver, FiveThirtyEight, Bush, Kerry, Hoover, Truman, Dewey, Roosevelt]
---
*Il 6 novembre 2004 ho scritto questo articolo per il quotidiano svizzero Corriere del Ticino.*

####I rilevamenti dell’ultima ora assai più imprecisi di quelli vecchi di un mese

##Bush come Truman. Almeno nei sondaggi

###Perché exit poll e proiezioni hanno sbagliato il nome del nuovo presidente Usa

Che George Walker Bush sarebbe stato riconfermato Presidente degli Stati Uniti d’America era una notizia acquisita praticamente per tutti i sondaggisti, in settembre. A novembre, nell’immediata vigilia delle elezioni, molti pronostici puntavano invece su John Forbes Kerry oppure su un testa a testa all’ultimo voto.

Come è noto non è stato così. Bush ha totalizzato oltre tre milioni e mezzo di voti individuali più dell’avversario (il 51 percento del totale) e il 53 percento di voti elettorali. Vittoria non larga ma netta, ben diversa da quella del 2000, in cui aveva ottenuto mezzo milione di voti individuali meno di Al Gore e l’aveva spuntata per soli 5 voti elettorali su 538.
I sondaggi più veritieri erano dunque quelli più vecchi. Cosa che avrebbe dovuto essere evidente a chiunque avesse imparato le lezioni del 1936 e del 1948.

Secondo i sondaggi, nel 1936 avrebbe dovuto vincere il Repubblicano Alfred London, con il 57 percento delle preferenze. Invece venne riconfermato Franklin Delano Roosevelt, con più del 60 percento dei voti popolari. L’autorità dei sondaggi era la rivista Literary Digest, che inviava una decina di milioni di finte schede elettorali ad altrettanti cittadini scelti a caso e redigeva le previsioni sulla base delle schede che ritornavano. La prima volta fu nel 1920 e previde esattamente la vittoria di Warren Harding. Lo schema si ripetè con successo nel 1924 (Calvin Coolidge), nel 1928 (Herbert Hoover) e nel 1932 (Roosevelt). Nel 1936 Literary Digest, al solito, aveva inviato le finte schede a nomi presi dagli elenchi di possessori di auto e di abbonati al telefono. Solo che gli Usa uscivano dalla Grande Depressione del 1929 e quelli erano beni da ceto medio-alto, preoccupato per le politiche stataliste del presidente uscente, viste invece con favore da una massa di nuovi poveri che il crollo della Borsa aveva lasciato senza auto e senza telefono.

Ma un semplice giornalista appassionato di statistica, dopo avere ricevuto 1.500 schede su tremila inviate, ottenne un risultato preciso con un margine dell’uno per cento. Si chiamava George Gallup e da lì nacque, oltre all’omonimo istituto, il sondaggio moderno. Gallup aveva interrogato un campione rappresentativo, mentre Literary Digest no.

Nel 1948 fu Gallup a commettere un errore memorabile. Il Repubblicano Thomas Dewey era talmente in vantaggio sul Democratico Harry Truman, presidente uscente, che un mese prima del voto vennero addirittura sospesi i sondaggi, per la loro evidente inutilità. La mattina dopo le elezioni tutti i quotidiani uscirono titolando a nove colonne sul successo di Dewey. Trionfò Truman, 49,6 contro 45,1 percento nel voto popolare, 303 a 189 nel voto elettorale. I media avevano smesso di ascoltare il pubblico, puntando su un’idea fissa.

Gli indicatori di voto di questo 2004, per chi li ha voluti riconoscere, erano molto chiari. Tradizionalmente la piattaforma di lancio di ogni candidato è costituita dalla convention del proprio partito. Al termine delle convention 2004 Kerry, partito in svantaggio rispetto a Bush, non aveva modificato gran che la situazione e questo costituiva un primo segnale negativo per lo sfidante. I dibattiti televisivi, se non smuovono pesantemente l’opinione pubblica (come accadde a John Kennedy contro Richard Nixon), sono perfettamente inutili. Dal 1988 a oggi solo Bill Clinton è riuscito a vincere sia in televisione che ai seggi, con un carisma e una personalità ben più brillanti rispetto a Kerry o allo stesso Bush. E che il candidato Repubblicano perda i dibattiti televisivi, in Usa, è pressappoco la normalità.

A questi indizi si è aggiunta la scompaginazione del campione dei votanti. Da mesi era noto che la strategia di entrambi i contendenti, verso una opinione pubblica polarizzata, consisteva nel rafforzare la propria base elettorale, più che catturare voti in campo avverso. L’affluenza ai seggi è infatti risultata altissima e sia Bush che Kerry hanno ricevuto una quantità di voti superiore a quella di qualunque altro candidato nella storia degli Stati Uniti. Ciascuna fazione è riuscita a portare al voto molti più sostenitori e, per come funziona il sistema americano, significa averli registrati nelle liste elettorali. Storicamente gli elettori Democratici registrati sono in numero superiore ai Repubblicani. Ma altrettanto storicamente molte persone, pur registrate, non votano. Tra i cosiddetti *likely voter*, invece, tende a esserci una leggera maggioranza Repubblicana, o una sostanziale parità. L’incognità, per i sondaggisti, consiste nel definire esattamente il profilo del likely voter (letteralmente, persona che probabilmente andrà a votare) e, appunto, che è impossibile prevedere con esattezza l’affluenza alle urne.

Su questa situazione si è innestata la propensione della grande stampa americana a parteggiare per John Kerry, culminata nell’appoggio ufficiale del prestigioso New York Times a pochi giorni dalle elezioni.
Riassumendo, gli organi di informazione tendevano a favorire il candidato Democratico. I sondaggi venivano eseguiti su una base elettorale sempre più diversa da quella rappresentata dal campione. I Democratici contavano sempre più votanti registrati, comprendenti però una notevole percentuale di persone poi assenti alle urne, mentre aumentava costantemente il numero dei likely voter mobilitati per Bush. Tutto ciò ha creato una distorsione della tendenza di voto apparente. Nelle primissime ore di spoglio delle schede altri due fattori hanno completato il quadro. Un blog molto frequentato di orientamento Democratico, [Wonkette](http://wonkette.com), ha dato indicazioni di una larga affermazione di Kerry, lasciando intendere che si trattava di dati elaborati internamente dallo staff dello sfidante. Contemporaneamente l’istituto Zogby diffondeva un sondaggio basato su dati provenienti dalla costa nordorientale, i cui risultati elettorali sono i primi ad arrivare per via dei fusi orari e della ridotta dimensione degli Stati, tutte roccaforti di Kerry. Qualche testata ha scambiato il sondaggio per un *exit poll*, oltretutto dimenticando che nelle ore seguenti sarebbero arrivati (per via dei fusi orari) solo dati favorevoli a Bush, ed [è uscita annunciando un trionfo del candidato Democratico](http://pietroraffa.com/2014/10/22/prima-pagina-manifesto-3112004-vinto-kerry/). Peccato che Wonkette avesse solo riferito pure impressioni e Zogby si fosse augurato pubblicamente nel passato la vittoria di Kerry, e che quei dati fossero quindi di bassa attendibilità.

**Esito finale: Bush 286, Kerry 252**

*Sabato’s Crystal Ball*<br />
7 settembre: Bush 284, Kerry 252<br />
1 novembre: Bush 269, Kerry269

*Gallup*<br />
12 ottobre: Bush 312, Kerry 207<br />
1 novembre: Bush 263, Kerry 275

*Election Projection 2004*<br />
12 settembre: Bush 285, Kerry 253<br />
27 ottobre: Bush 269, Kerry 269