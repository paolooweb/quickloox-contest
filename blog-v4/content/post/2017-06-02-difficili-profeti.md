---
title: "Difficili profeti"
date: 2017-06-02
comments: true
tags: [rumor, Wwdc, Swift, Playgrounds, Lego, Mindstorms, Siri]
---
Restano sempre vari motivi seri per farsi il sangue cattivo, se uno proprio ci tiene. Parlando di futilità, tuttavia, notato che mancano tre giorni a [Wwdc](https://developer.apple.com/wwdc/) e di fatto non è trapelato un annuncio vero che sia uno?

Al massimo robe che uno ci riflette cinque minuti, fa due più due e più che avere un’anticipazione produce un’ovvietà: è tanto che non esce un iPad nuovo e magari annunciano un iPad nuovo, ci sono nuovi processori quindi può darsi annuncino un Mac con dentro quelli eccetera. Così sono bravi tutti. Meglio il silenzio dignitoso

Oppure l’altoparlante con Siri. Come è fatto? Che cosa c’è dentro? Perché è speciale o non lo è? Niente di niente. Scrivere *altoparlante* e niente più non è esattamente uno *scoop*.

E così si vive bene, il tempo è bello, si risparmia tempo, si guadagna qualità. Meno spazzatura, del resto, vuol dire più pulizia.

Intanto Apple [ha annunciato](https://www.apple.com/newsroom/2017/06/swift-playgrounds-expands-coding-education-to-robots-drones-and-musical-instruments/) – lei – Swift Playgrounds con il necessario per controllare droni, strumenti musicali, mattoncini Lego Mindstorms e altri apparecchi dotati di intelligenza computazionale. Ho la sensazione che ci attenda un *keynote* di quelli interessanti.
