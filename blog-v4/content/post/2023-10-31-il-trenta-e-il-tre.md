---
title: "Il trenta e il tre"
date: 2023-10-31T15:22:27+01:00
draft: false
toc: false
comments: true
categories: [Hardware]
tags: [Mac Book Pro, iMac, M3]
---
Il formato di presentazione digitale messo a punto da Apple durante la pandemia ha dimostrato la sua flessibilità con un [evento di soli trenta minuti](https://www.youtube.com/watch?v=ctkW3V0Mh-k), che avrebbero anche potuto essere tre: *abbiamo messo M3 in MacBook Pro e iMac*.

I ventisette minuti restanti sono serviti a introdurre l’argomento con un video iniziale simpatico ma diritto al punto, rispetto a quante cose si possano fare con un Mac, e all’illustrazione del divario esistente tra M3 e i due chip precedenti, nonché per forza di cose con il resto del mondo.

Sul piano delle prestazioni c’è da aspettarsi, rispetto ai MacBook Pro passati un incremento abbastanza lineare, ma sul piano della performance per watt il quando appare impietoso per i processori altrui. Gli pseudobenchmark mostrati da Apple potrebbero essere finti; potrebbero anche gonfiare del cento per cento il divario reale, per quanto ne sappiamo.

Così fosse, a parità di prestazioni un M3 consumerebbe un terzo, metà, tre quarti dell’energia richiesta dalla concorrenza. Un predominio evidente.

Apple ha tenuto a fare notare che le prestazioni dei MacBook Pro M3 sono identiche con o senza alimentazione, cosa che non accade altrove. Misurando le performance per watt in un impiego a batteria, il confronto diventerebbe rapidamente impietoso.

Va notato a questo punto che altri costruttori avevano annunciato piani di rincorsa tecnologica, quando Apple Silicon è stato svelato al mondo. Sono passate tre iterazioni e ancora mancano riscontri.

Il vantaggio tecnologico dei processori (da ora a tre nanometri, gli unici al momento sul mercato) uniti allo schermo Xdr Liquid Retina danno sostanza allo slogano di Apple sul *miglior portatile professionale al mondo*, con il *migliore schermo al mondo per un portatile professionale*. Discutere si potrà anche, però smentire nettamente pare improbabile.

Interessante notare come Apple abbia ripetutamente parlato ai possessori di Mac Intel, che in effetti compirebbero un salto di velocità e prestazioni pazzesco passando a un Mac M3. Per non parlare dell’autonomia raddoppiata (Apple parla di ventidue ore per un M3).

Ancora più interessante è stato l’accento netto posto sugli impieghi professionali: per ogni modello di MacBook Pro, con M3, M3 Pro e M3 Max, sono stati dettagliati con precisione i tipi di utilizzo e gli esempi di figura professionale, e OK; ma anche le app, senza timore di nominare – assieme ai prodotti di casa – Da Vinci Resolve, Premiere Pro, Maya e altro software non Apple.

C’è poco altro da dire. Con i tre nanometri del processo di fabbricazione e il Dynamic Caching della GPU (per ottimizzare l’impiego dei *core* grafici e evitare il sottoutilizzo) Apple ha presentato due innovazioni di sostanza che sul mercato, fuori dalla Mela, non esistono e non si vedranno il mese prossimo. I PC a Media World costano sicuramente meno; contengono sicuramente meno.

Tenuto conto che a iMac è stato riservato giusto un angolino apposta per dire che c’è anche lui (solo sui ventiquattro pollici) e null’altro, a parte un accenno a Sonoma per dire che naturalmente sfrutta al massimo le capacità di Apple Silicon aggiornato, si capisce anche che probabilmente la mezz’ora è la dimensione minima di questo formato di presentazione.

Abbiamo visto anche Apple Park immerso in una nebbiosa e oscura atmosfera da Halloween e quindi si è mantenuto anche quel pizzico di humor che caratterizza il format.

Ho l’impressione che la notte da incubo l’abbia passata più che altro qualche dirigente di Lenovo o altri produttori di portatili di fascia più elevata. Lo scherzetto di Apple per Halloween sui MacBook Pro sarà duro da digerire.

<iframe width="560" height="315" src="https://www.youtube.com/embed/ctkW3V0Mh-k?si=ZTv7AZeQ3Lfr0APd" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>