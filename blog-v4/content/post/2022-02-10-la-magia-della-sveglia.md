---
title: "La magia della sveglia"
date: 2022-02-10T02:20:44+01:00
draft: false
toc: false
comments: true
categories:
tags:
---
È da tempo che Apple accusa una grande pressione per le sue politiche commerciali su App Store, con la commissione del trenta percento che appare eccessiva e inadeguata per i tempi, il problema del divieto per le app di usare sistemi di pagamento che non siano App Store stesso eccetera. John Gruber ha recentemente scritto [un bel compendio sulla situazione olandese](https://daringfireball.net/2022/02/going_dutch), il cui governo ha multato Apple per tre volte rispetto a certi obblighi che dovrebbe osservare sulle app per il dating.

Apple ottempera alle richieste ma in modo non proprio convincente secondo le autorità olandesi e il braccio di ferro si sta trascinando senza capire chi sarà il vincitore ma, soprattutto, che tipo di vittoria potrebbe sperare di conseguire.

Apple lavora per conservare i propri margini e la propria posizione strategica. Forse ci riuscirà, forse no; in ogni caso, se anche perdesse dieci o venti miliardi di profitto, certo non sono noccioline, ma l’azienda è talmente in salute che potrebbe permetterselo.

Sono più preoccupato del fatto che ho impostato la sveglia di watch riciclandone una già usata, cui avevo dato un nome. E ora non riesco a cambiare il nome.

Le strategie commerciali vanno e vengono, se smetti di fare soldi da una parte puoi provarci da un’altra e ad Apple non mancano le alternative, specie pensando ai recenti [ritrovamenti nei log e nel codice di riferimenti a un RealityOS](https://www.macworld.com/article/613159/realityos-ar-vr-headset.html) che preluderebbe a un ingresso nel mercato della realtà aumentata. Apple era abituata a contare solo su Macintosh; oggi i flussi di cassa arrivano da almeno cinque canali diversi e potrebbero anche aumentare di numero.

La magia, però, non dipende dai miliardi. Deve restare viva, o il danno sarà, per quanto invisibile, superiore a qualsiasi diatriba sui pagamenti.
