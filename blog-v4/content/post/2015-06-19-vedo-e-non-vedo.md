---
title: "Vedo e non vedo"
date: 2015-06-20
comments: true
tags: [Afb, iPadmini, VoiceOver, Marston, Cox, Vanda]
---
La American Foundation for the Blind ha appena omaggiato Apple del [premio Helen Keller per il 2015](http://www.afb.org/info/about-us/press-room/press-release-archive/afb-announces-2015-helen-keller-achievement-award-honorees-/1245) assieme all’attore [Charlie Cox](http://charliecox.net) (interpretazione di un supereroe cieco), al musicista non vedente [Ward Marston](http://www.wardmarston.com) e all’azienda [Vanda Pharmaceuticals](http://www.vandapharmaceuticals.com).<!--more-->

>La fondazione premia Apple per VoiceOver, un lettore di schermo con interfaccia gestuale che permette di udire una descrizione di tutto quello che accade a video e ha altre funzioni che rendono iPhone, iPad e altri apparecchi iOS accessibili alle persone con vista limitata. Apple ha ricevuto il premio nel 2009 per l’ingegnerizzazione pionieristica di prodotti accessibili e continua nei suoi sforzi straordinari per rendere i suoi prodotti accessibili a chiunque.

Intanto Apple Store [ha eliminato dal catalogo il primo iPad mini](http://9to5mac.com/2015/06/19/apple-quietly-pulls-ipad-mini-from-web-site-and-apple-store/), così lasciando in vendita solamente apparecchi iOS con schermo Retina.

Come dire che è in offerta il meglio per chi vede e il meglio per chi vede poco, o non vede.