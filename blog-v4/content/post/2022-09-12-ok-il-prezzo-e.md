---
title: "OK, il prezzo è"
date: 2022-09-12T12:24:02+01:00
draft: false
toc: false
comments: true
categories: [Hardware]
tags: [iPhone, Ben Thompson, Thompson, Stratechery, Riccardo, Mori, Riccardo Mori, Carolus]
---
Una delle reazioni all’indomani dell’[evento di presentazione di iPhone 14](https://macintelligence.org/posts/2022-09-08-levento-invisibile/) è stata quella sui prezzi della nuova gamma, più alti che l’anno scorso, in modo anche significativo.

In questo spazio ha commentato **Carolus**, in modo molto centrato e condivisibile:

>Tutto straordinario ma: “C’è vero progresso solo quando i vantaggi di una nuova tecnologia diventano per tutti”, per citare un vecchio stalinista come Henry Ford. Con i prezzi attuali, per noi europei i “computer for the rest of us”, diventeranno sempre più un ricordo, soprattutto quelli da tasca, almeno nella gamma più alta. C’è la guerra, la pandemia, l’inflazione, la svalutazione. Potrei proseguire con un elenco degno delle scuse di John Belushi a Carrie Fisher o di un food blogger molto noto che fa il politico a tempo perso, eppure dispiace. L’unicità di iOS è impareggiabile e sarà difficile rinunciare a tutte le piccole coccole di un sistema che sembra progettato davvero per chi lo usa e non per chi deve vendere qualcosa, o qualcuno.

L’amico **Riccardo** è [preciso come sempre](https://twitter.com/morrick/status/1568382746631757826):

<blockquote class="twitter-tweet"><p lang="en" dir="ltr">iPhone prices here in Spain, last year vs this year.<br><br>2021<br>iPhone 13 mini: from €809<br>iPhone 13: from €909<br>iPhone 13 Pro: from €1159<br>iPhone 13 Pro Max: from €1259<br><br>2022<br>iPhone 14: from €1009<br>iPhone 14 Plus: from €1159<br>iPhone 14 Pro: from €1319<br>iPhone 14 Pro Max: from €1469</p>&mdash; Riccardo Mori (@morrick) <a href="https://twitter.com/morrick/status/1568382746631757826?ref_src=twsrc%5Etfw">September 9, 2022</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

Il suo punto di arrivo è che un telefono non dovrebbe nemmeno costare così tanto, in generale. Ciò non toglie che nella contingenza gli aumenti in Spagna siano evidenti e notevoli.

Però andrebbe letto [L’iPhone dei servizi](https://stratechery.com/2022/the-services-iphone/) di Ben Thompson.

Lettura lunga, di taglio strategico, approfondita sia nel tempo passato sia nell’analisi del presente, centrata sullo spostamento di obiettivi di Apple dalla vendita di hardware a quella dei servizi, lungo un binario che è sempre stato lo stesso: il conto dei profitti generati da ogni singolo device installato privilegiato invece del numero di unità vendute o altre metriche molto comuni, molto facili e che alla fine interessano sul breve termine, non sul lungo. Non sto a raccontare il post, ci vorrebbe molto, e invece mi soffermo su un paragrafo chiave:

>L’annuncio più sorprendente di tutti, però, sono stati i prezzi. Sono rimasti gli stessi! Non me lo aspettavo e neanche gente che segue Apple da vicino, come John Gruber. […] Dopotutto, la strategia di Apple negli ultimi anni sembrava focalizzata sul generare maggiore fatturato dagli utenti esistenti. Più importante, questo anno ha visto un notevole aumento dell’inflazione.

Sembra fantascienza, visto dall’Europa. Eppure è così. Un altro paragrafo:

>In termini reali, significa che i prodotti di Apple sono diventati più economici. Apple ha sicuramente alzato i prezzi nel mondo, ma questo si spiega con il fatto che la società è basata sul dollaro, che è più forte di quanto sia mai stato in anni: in altre parole, i prezzi esteri sono derivati dal prezzo in dollari, che è rimasto uguale, a significare che il prezzo è più basso.

Stiamo cioè vedendo l’inflazione al lavoro. Non è che un iPhone costi di più; la moneta in cui lo paghiamo vale meno. A questo, come Europa, si aggiungono fattori evidenti e anche di lunghissimo periodo. Abbiamo costruito una dipendenza energetica da un Paese autoritario che ora siamo tiepidi nel contrastare; abbiamo ritardi nella banda larga, nella logistica, nella pubblica amministrazione, nella scuola, nella gestione dell’economia, nella classe e nei costumi politici, ovunque; abbiamo financo una costruzione politica europea che impallidisce di fronte a quella degli Stati Uniti. Nel costruire una vera Europa siamo indietrissimo, pur avendo cominciato settant’anni fa.

La cosa inquietante è che sembra stia arrivando il conto e non è vero. I prezzi *veri* sono sempre quelli. Il valore è immutato. Diminuisce il nostro potere di acquisto di europei e di italiani. Tenere presente chi ha governato negli ultimi, boh, trent’anni e metterli insieme come accumulo di ritardi e inadempienze, di inettitudini politiche, di incapacità strategiche, di mancanza di visione, di capacità di decidere.

Tra poco si vota e il prezzo degli iPhone, non in quanto tale ma come indicatore, conta molto più delle etichette, delle dichiarazioni, dei programmi scritti sulla carta.