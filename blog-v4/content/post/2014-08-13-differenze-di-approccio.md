---
title: "Differenze di approccio"
date: 2014-08-13
comments: true
tags: [Google]
---
Il mondo continua a organizzarsi davanti a me perché io intervenga [ancora](https://macintelligence.org/posts/2014-08-06-patti-chiari-privacy-lunga/) su stati-nazione e stati-corporazione.<!--more-->

[Scrivevo](https://macintelligence.org/posts/2014-08-08-patti-chiari/) che l’avanzata dei secondi è dovuta alla loro velocità ed efficienza, che prescinde dai numeri. L’obiettivo di una Google è erogare servizi pressoché istantanei a centinaia di milioni di persone e farlo in modo efficiente (*veloce* è solo parte di *efficiente*).

Ieri ho ricevuto notizia da Google che qualcuno ha cercato di introdursi in uno degli *account* che uso per scrivere sotto pseudonimo:

>Ciao Pseudonimo,

>Abbiamo bloccato un tentativo di accesso al tuo account [pseudonimo@gmail.com] il 10 agosto 2014 alle 14:15:25 dalla lo alto in cui è avvenuto.

>Se non eri tu, controlla la pagina di attività dell’account in cerca di episodi sospetti. Chiunque abbia cercato di entrare conosce la tua password e ti raccomandiamo di cambiarla.

>Se eri tu, ti raccomandiamo di accedere a Gmail con una app fatta da Google oppure abbassare i tuoi livelli di sicurezza così che l’account sia meno protetto di ora [e non sollevi falsi allarmi].

La mail si chiude con un *link* alla pagina di supporto per i casi come questo. E permette di ricostruire l’accaduto: qualcuno, probabilmente uno *spammer*, a furia di provare ha beccato la *password* giusta, solo che gli manca il secondo fattore di autenticazione. Ovvero, deve intensificare gli sforzi o almeno procurarsi il mio iPhone.

Velocità, efficienza, precisione, chiarezza.

Sto scrivendo fuori casa: sono uscito per fare assegnare una *password* alla mia tessera di assistenza sanitaria.

Da anni viaggiavo con una tessera sanitaria provvisoria – un foglio di carta timbrato – perché Azienda Sanitaria Locale e Agenzia delle Entrate, in possesso dello stesso codice fiscale, pensavano fosse assegnato a due individui diversi.

Finalmente il dissidio si è ricomposto e non sono più schizofrenico.

La tessera serve a nulla senza il Pin. La tessera con il Pin serve a nulla senza il lettore.

In compenso si può accedere ai servizi con la sola tessera, senza Pin e senza lettore, previa *password* che bisogna farsi consegnare di persona presso uno sportello autorizzato.

Quello che ho fatto. Letta su tre terminali diversi, la tessera è stata diagnosticata non funzionante.

Ora potrò telefonare ai numero verde per richiedere un’altra tessera e proseguire questa appassionante avventura.

Nei commenti che ho ricevuto vedo accenni a monopolio, liberismo, cose brutte.

Vorrei però che mi si spiegasse perché non posso collegarmi al portale sanitario con l’autenticazione e la protezione che usa Google e ricevere avvisi chiari e tempestivi come quelli che manda Google (che è solo un esempio).

Se qualcuno bypassasse le funzioni dell’Azienda Sanitaria Locale con un sistema che funziona meglio, a costo paragonabile, lo proverei subito. Sarebbe libertà di scelta e non liberismo, mentre il monopolio è quello attuale.