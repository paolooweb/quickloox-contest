---
title: "Vecchi e giovani"
date: 2014-07-01
comments: true
tags: [Sentry, Portland, iPad, MacBook, RioTech]
---
Il valore dei vecchi Mac e quello che si può, si potrebbe, fare a scuola con un po’ di cervello e altrettanta iniziativa. Cito dal [Sentry di South Portland-Cape Elizabeth](http://sentry.mainelymediallc.com/news/2014-06-06/Front_Page/Learning_on_laptops_literally.html).<!--more-->

>Quando l’anno scorso il distretto scolastico è passato dai computer agli iPad, ha comprato dallo Stato i vecchi MacBook noleggiati invece che restituirli. Quelli danneggiati sono stati affidati al team RioTech, oltre venti studenti che hanno ricevuto 400 macchine danneggiate ricavandone 200 funzionanti che sono state messe in vendita al pubblico.

Per i ragazzi è stata un’esperienza tecnica e imprenditoriale al tempo stesso. I computer rimessi a nuovi sono stati potenziali con più Ram e nuovi dischi; finora, a fronte di una spesa di seimilacinquecento dollari per l’acquisto delle macchine, il ricavo è stato di tredicimila dollari, il doppio della spesa.

Nel frattempo una fornitura di iPad pari a 860 unità sta facendo risparmiare oltre diciassettemila dollari al distretto rispetto alla soluzione concorrente. E a South Portland tutti gli studenti di liceo avranno un mano una macchina per imparare.

Altrove sembra sempre tutto più facile. Forse si dà il giusto valore ai vecchi e il giusto valore ai giovani. Che si tratti di studenti, insegnanti o computer poco conta.