---
title: "Inconsapevolmente vendonsi"
date: 2016-03-19
comments: true
tags: [Windows, Dell, privacy, McAfee, antivirus]
---
Il sito *HelpNetSecurity* [riporta](https://www.helpnetsecurity.com/2016/03/17/mcafee-web-beacons/) i risultati di una ricerca su sette portatili. Risultati che trovo strabilianti per come passino sostanzialmente sotto silenzio.<!--more-->

I sette portatili sono di Lenovo, Acer, HP e Dell, non esattamente sconosciuti cantinari cinesi. Uno dei portatili HP è Microsoft Signature Edition, ossia teoricamente esente dalla [morchia che si trova normalmente](https://macintelligence.org/posts/2015-02-21-gatti-e-volpi/) in un computer Windows all’atto dell’acquisto. Eppure sono sltate fuori cose interessanti.

Il portatile Dell contiene il certificato di autenticazione completo di chiavi private, aperto al primo malfattore di passaggio, di cui  [si è già parlato](https://macintelligence.org/posts/2015-12-14-di-ventotto-ce-n-e-una/) e soprattutto Dell aveva promesso di eliminare.

Molte funzioni di Windows 8 e Windows 10, parole dell’articolo, *raccolgono dati sull’utente e sul computer, nonché informazioni sulla configurazione della privacy*. Testuale:

>Molte delle applicazioni e dei servizi collegati a questi parametri di privacy iniziano a “chiamare casa” appena il computer entra in una rete, prima ancora dell’autenticazione. Per tutti quelli con la preoccupazione della privacy, sarebbe ideale avere una possibilità di inibire queste operazioni, specialmente quando il loro verificarsi non è evidente.

Sfortunatamente, in molti casi modificare la configurazione della *privacy* non è banale. Succede anche che, al termine di un aggiornamento del sistema, i parametri vengano riportati silenziosamente allo stato in cui si trovavano prima delle modifiche.

Poco male, si penserà. Su Windows occorre per forza l’antivirus; lo si attiva, male necessario, e tutto si risolve.

Qui arriva il bello; uno degli antivirus, esattamente McAfee, contiene *segnalatori web che che possono essere usati per tracciare la navigazione degli utenti e inviare loro pubblicità*. Tombola.

Appare chiarissimo quale sia il ruolo del compratore di questi aggeggi: consumatore da tracciare, analizzare, sezionare, classificare, in modo da poterlo conoscere anche là dove lui/lei non vorrebbe proprio, e usare le informazioni ricavate per assillare altri consumatori.

L’acquirente di un PC Windows, con tutta probabilità, vende la propria *privacy* senza saperlo, in cambio di niente. L’unica giustificazione che riesco a trovargli è che, appunto, non ne sa niente. Qualcuno glielo dica. O almeno gli consigli la lettura dello [studio originale](https://duo.com/assets/pdf/bring-your-own-dilemma.pdf) dei ricercatori.