---
title: "Le certezze del sapere"
date: 2024-03-31T00:59:51+01:00
draft: false
toc: false
comments: true
categories: [Software, Internet]
tags: [Ai intelligenza artificiale, Scientific American, Yann LeCun, LeCun, Hofstadter, Douglas Hofstadter, global workspace theory, gwt, Fluid Concepts and Creative Analogies, Concetti fluidi e analogie creative, Workspace]
---
Il numero di aprile di *Scientific American* dedica grande spazio ai temi dell’intelligenza artificiale, compreso un articolo intitolato [Costruire macchine intelligenti ci aiuta a imparare come funziona il cervello](https://www.scientificamerican.com/article/what-the-quest-to-build-a-truly-intelligent-machine-is-teaching-us/), dedicato alla ricerca per arrivare a vere intelligenze artificiali, confrontabili con quelle umane.

Si legge che viene dedicata molta attenzione alla coscienza e a una nuova teoria detta *global workspace theory*, Gwt, secondo la quale

>la coscienza sta al cervello come uno staff meeting sta a una società: un posto dove i moduli al lavoro possono condividere informazioni e chiedere aiuto.

La coscienza coincide quindi con uno spazio di lavoro condiviso. La cosa interessante secondo la teoria è che la coscienza è un *sine qua non* per avere l’intelligenza di alto livello; ci sono mille cose che possiamo fare più o meno automaticamente, dal lavarsi i denti al guidare, ma attività nuove o particolarmente complesse, prive di un modulo già esperto e collaudato, necessitano della consapevolezza da parte del sistema di che cosa sta succedendo.

In [Fluid Concepts and Creative Analogies](https://macintelligence.org/post/2023-03-04-sublime-ignoranza/), Douglas Hofstadter riepiloga le sperimentazioni da lui compiute tra 1980 e 1995 sul funzionamento della mente, propedeutiche all’intelligenza artificiale. Lavorava su *microdomini*, insiemi di dati estremamente piccoli eppure in grado di suscitare enorme complessità, ed elaborava programmi capaci, più che di ottenere risultati, di farlo con tecniche e modi il più possibile simili a quelli umani.

Un elemento centrale di tutti i programmi di Hofstadter era il Workspace, una zona di scambio e di elaborazione, dove concetti e attributi competevano tra loro per avere il maggior consenso da parte del sistema fino a quando non veniva prodotto un risultato ritenuto ottimale.

Hofstadter rigettava dal principio l’idea che l’intelligenza si raggiungesse attraverso l’accumulo di conoscenza, che è ciò che fanno i grandi modelli linguistici attuali, pieni zeppi di dati e di parametri di connessione tra i dati stessi, sempre più dati, sempre più parametri.

L’articolo di *Scientific American* cita tra gli altri Yann LeCun, ricercatore per l’intelligenza artificiale in Meta, che dichiara quanto segue:

>In questo momento sto prendendo posizione contro una serie di cose che sfortunatamente sono estremamente popolari nella comunità di Ai e machine learning. Sto dicendo alle persone: abbandonate i modelli generativi.

Infatti, spiega l’articolo,

>Le reti neurali generative si chiamano così perché generano testo e immagini a partire da ciò cui sono state esposte. Per farlo, devono essere pignole sui dettagli: devono sapere come posizionare ogni parola in una frase e ogni pixel in ogni immagine. Solo che l’intelligenza, se è qualcosa, è la negazione selettiva dei dettagli.

In altre parole, l’interesse dei ricercatori si sta spostando verso reti neurali capaci di selezionare le informazioni davvero importanti per un concetto, invece di cercare di dominarle tutte. Il nostro cervello lavora molto più nel primo modo che nel secondo.

La morale: a cercare l’intelligenza artificiale vera con i grandi modelli generativi attuali non si va da nessuna parte e la cosa si comincia a notare. I concetti chiave per lavorare a un’intelligenza artificiale di alto livello tornano a essere gli stessi di quarant’anni fa, sia pure affrontati con software e hardware ben diversi. Ma sempre quelli sono, a dispetto di chi vede la coscienza nel chatbot *du jour*.