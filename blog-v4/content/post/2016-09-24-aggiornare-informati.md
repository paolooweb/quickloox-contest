---
title: "Aggiornare informati"
date: 2016-09-24
comments: true
tags: [macOS, iOS, aggiornamenti]
---
Sì, è uscito [iOS 10](http://www.apple.com/it/ios/ios-10/), due settimane fa.

Sì, è uscito [macOS Sierra](http://www.apple.com/it/macos/sierra/), quattro giorni fa.

Sì, gli sviluppatori hanno già in mano [la beta del primo aggiornamento](https://9to5mac.com/2016/09/21/apple-releases-ios-10-1-and-watchos-3-1-beta-seeds-for-developers/) per iOS 10.

Sì, gli sviluppatori hanno già in mano [la beta del primo aggiornamento](https://9to5mac.com/2016/09/21/macos-10-12-1-developer-beta-1-xcode-8-1/) per macOS Sierra.

Sì, gli aggiornamenti erano già in lavorazione quando si preparava l’uscita dei due sistemi operativi e c’entra niente qualunque di notizia di *bug* esca in questi giorni.

Sì, la maggior parte dei *bug* che escono come notizia è nota in Apple da settimane o mesi. Che vengano risolti dal prossimo aggiornamento o da quelli futuri è questione di quanta rilevanza gli viene attribuita e dalle risorse disponibili per risolverli.

Sì, è sempre andata così. Queste dinamiche esulano totalmente dal discorso eventuale sulla bontà dello sviluppo software o delle strategie dello sviluppo stesso. Anche il miglior software dell’universo nascerebbe mentre è in lavorazione il suo primo aggiornamento.

Sì, in un modo o nell’altro mi tocca ripeterlo ogni anno.