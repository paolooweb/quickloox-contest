---
title: "Che cos’è il futuro"
date: 2020-03-05
comments: true
tags: [Facebook, Seti@Home, Folding@Home, Boinc, Mersenne, Bitcoin]
---
Si può rispondere in tanti modi a come vediamo il futuro, o fatichiamo a vederlo.

C’è però un modo troppo facile, che non mi piace, sa di vecchio. Come sempre, di vecchio mentale, non fisico.

Supponiamo per esempio che [Seti@Home](https://setiathome.berkeley.edu) vada in ibernazione per avere raccolto, al momento, tutti i dati che gli servono e quindi voglia dedicare ogni risorsa alla loro analisi.

Il futuro è guardare la [pagina di tutti i progetti Boinc](https://boinc.berkeley.edu/projects.php) e scegliere il prossimo. Se no, c’è [Folding@Home](https://foldingathome.org) per esempio.

Do per probabile che Seti@Home non annuncerà di avere scoperto intelligenza extraterrestre.

Nessuno di noi ha scoperto un messaggio ordinato mescolato al rumore radio dell’universo.

Del resto è probabile che nessuno di noi si sia ritrovato un [Bitcoin](https://bitcoin.org/en/) nel computer per avere avuto la fortuna di completare lo scavo.

Così come difficilmente troveremo noi il prossimo [primo di Mersenne](https://www.mersenne.org).

Che cos’è il futuro? Disegnare la prossima sfida con speranza immutata. Non di essere in cima alla lista; di vedere il nuovo.

Con un computer davanti, puntare verso il nuovo dovrebbe venire naturale. Forse mi sbaglio, ma fatico a pensare diversamente. Finito Seti@Home, improvvisamente posso muovermi tra decine di possibilità invece di stare seduto sopra una sola.

Il futuro è esattamente come me lo aspetto. Nuovo, pieno di aperture, chiaroscuro… come sempre è stato. Bellissimo. Il passato è un cristallo; il futuro è un organismo. La vita è in questa direzione.