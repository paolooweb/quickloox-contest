---
title: "Roba di Matt"
date: 2020-02-11
comments: true
tags: [Deatherage, MDJ]
---
Erano gli anni novanta, Apple non se la passava benissimo, pareva che Windows avrebbe fagocitato il mondo e io spesi quelli che oggi sarebbero circa duecentocinquanta euro per abbonarmi a Mdj_, la newsletter di Matt Deatherage.

Deatherage era, è tanto competente sul lato tecnico quanto sugoso da leggere come commentatore. In quei giorni Mdj_ era come accendere la luce in casa quando improvvisamente ti accorgi che si è fatto tardi ed è già buio.

Il mio abbonamento era *lifetime*, la classica scommessa sul futuro. Fu relativamente azzeccata; Mdj_ durò qualche anno e poi Deatherage smise di scrivere, per problemi di salute piuttosto importanti e altri accidenti assortiti di cui parlò diffusamente prima di chiudere.

La newsletter aveva anche un problema di identità, struttura, prezzo; erano i momenti in cui l’onda del web travolgeva tutte le cose intelligenti con la cacca gratuita.

Per molti anni ho provato a vedere di tanto in tanto se per caso Deatherage avesse ripreso a pubblicare, più per la persona che per i contenuti. Ma niente; [passava da Twitter](https://twitter.com/macjournals) a commentare le presentazioni del trimestre finanziario Apple e poi ritornava silente.

Improvvisamente [l'ho ritrovato](https://mdj.substack.com) e la trovo una cosa da pazzi, quindi bellissima. Vero che è un grande momento per lo strumento newsletter; vero anche che monetizzare la buona informazione è una missione vicina all’impossibile.

Sto valutando se iscrivermi a pagamento o stare nel segmento gratis; per chiunque non abbia vissuto l’epoca d’oro di Mdj_ raccomando vivamente *almeno* la seconda opzione. Siamo assuefatti ai siti-spazzatura e invece ci sarebbe molto ancora da dire sul mondo Apple in modo intelligente, per un pubblico capace di ascoltare e partecipare attivamente. Esiste ancora.

Forza Matt.