---
title: "Il privilegio è il messaggio"
date: 2021-12-22T00:31:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [Steve Jobs, Corrado D’Elia, Il silenzio del rumore, Franco Battiato] 
---
Ho goduto del privilegio di assistere a una rappresentazione privata di [Io, Steve Jobs](https://corradodelia.it/articoli/134-io-steve-jobs), di e con Corrado D’Elia.

È accaduto durante una festa aziendale, evento che classicamente viene interpretato come una mangiata ad alcol libero dove tutti tornano a casa più o meno uguali a prima, più o meno ubriachi, più o meno sessualmente appagati o frustrati, simmetricamente, in funzione del proprio stato civile e di quello dell’oggetto del desiderio declinato all’aziendale.

Il tempo della classicità nelle aziende, nel 2021, è proprio terminato, se non altro visto che la pandemia trasforma in una sfida già solo l’organizzazione di un evento, non parliamo della conservazione in salute dei partecipanti.

Nel 2021 una festa aziendale è un privilegio e ancora di più dove si tratti di un momento veramente identitario, comunicativo, dove sentirsi parte di un gruppo davvero connotato e coeso arriva ben prima della busta paga. Ci vuole la busta, ma se è l’unico _driver_, di strada se ne fa poca.

Anche una azienda capace di costruire un gruppo così e di guardare alla crescita del personale contestualmente a quella del fatturato, è un privilegio. Sapere che cosa si stia facendo non è difficile, ma conosco troppi posti dove la sola ipotesi del sapere _perché_ è fantascienza _hardcore_. Sono così privilegiato dal trovare posto in organizzazioni con una idea molto chiara del perché, capaci di lasciare un segno, anzi: di ammaccare l’universo, come amava fare Steve Jobs. Uno che i privilegi se li è conquistati, a prezzo a volte molto alto e forse troppo, sempre però pagando con i _perché_ molto chiari nella testa.

Ci sono molti modi di declinare il privilegio. A volte è sufficiente essere vivi e me ne sono reso conto da quello che accadeva nella carreggiata opposta della strada per il rientro. A volte è usare un Mac, a volte è avere il privilegio ricorsivo di aprire gli occhi sui propri privilegi.

_Ti sei mai chiesto quale funzione hai?_, si chiedeva Franco Battiato in un supponente, sopravvalutato e fulminante [brano](https://www.youtube.com/watch?v=M0RGfXpvxfA) giovanile.

Una domanda come questa è eccessiva. Chiedersi invece come trasformare i propri privilegi in occasioni per sé e per altri, oppure riconoscere i privilegi che abbiamo sempre dato per scontati, è una pregnante meditazione natalizia.

Il mio concetto di festa aziendale è molto cambiato e devo ringraziare parecchie persone. La mia visione di Steve Jobs è rimasta la stessa. Grazie a D’Elia, però, mi sono chiesto per la prima volta che cosa Steve avrebbe eventualmente pensato di me. E quanto il suo concetto di privilegio sarebbe stato diverso dal mio.

<iframe width="560" height="315" src="https://www.youtube.com/embed/M0RGfXpvxfA" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

_Per ora si può lasciare un commento dalla [pagina apposita di Muut per Qui
ckLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile._