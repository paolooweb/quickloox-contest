---
title: "C'è bisogno di scrivere"
date: 2014-10-09
comments: true
tags: [Colombini, Medusa, Castello, Locusta]
---
Mi scrive [Enrico Colombini](http://www.erix.it) per avvisarmi di avere aperto un (dice lui) mini-blog, [Just Half a Bit](http://www.quintadicopertina.com/enricocolombini/language/it/).<!--more-->

Ha scritto tre *post*, che ruotano attorno al [compilatore Medusa](http://www.erix.it/medusa.html) (Macchinario Esente Da Un Significativo Acronimo). Già ce n’è per un mese di studio.

Il problema è che si sta distraendo. Cercherò di tenerlo focalizzato, ma c’è bisogno dell’aiuto di tutti.

L’uomo che ha creato [Avventura nel castello](http://www.erix.it/avventure.html#cast) ieri e [Locusta Temporis](http://www.quintadicopertina.com/index.php?option=com_content&view=category&layout=blog&id=38&Itemid=74) *deve* continuare a bloggare. Va stimolato, ispirato e pungolato.

Medusa È Da Usare Senza Acronimi.