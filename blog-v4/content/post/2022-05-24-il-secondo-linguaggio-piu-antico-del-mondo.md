---
title: "Il secondo linguaggio più antico del mondo"
date: 2022-05-24T01:10:04+01:00
draft: false
toc: false
comments: true
categories: [Software, Hardware]
tags: [Lisp, Sojourner, Millennium, Nasa, Marte, CoRecursive, Ron Garret, Macintosh, Repl, Lcross]
---
È un periodo di lavoro molto intenso, la scuola sta per finire, si arriva a notte che dovrebbe essere magari metà pomeriggio. Viene voglia di fuggire, anche lasciare l’orbita terrestre se necessario.

O ascoltare un [podcast con intervista a un ex ingegnere della Nasa, che racconta di come negli anni ottanta programmavano il robot Sojourner](https://corecursive.com/lisp-in-space-with-ron-garret/), operativo su Marte, o la sonda Deep Space 1.

Soprattutto, programmavano in linguaggio Lisp.

>Facevamo eseguire linguaggio Lisp sui robot più grandi, che avevano a bordo qualche megabyte di Ram. Il codice che controllava effettivamente il robot era scritto in Lisp. L’altro modo in cui usavamo Lisp, sui piccoli robot, era usarlo per creare linguaggi di programmazione su misura per pilotare le macchine e compilarli in modo da avere codice compatto capace di girare su piccoli processori.

Interessante anche sapere su che cosa programmavano:

>Avevamo questo ambiente di sviluppo dove scrivevamo il codice su Macintosh e lo facevamo girare su un simulatore di robot.

Nell’intervista, a un certo punto Ron Garret – l’ex ingegnere – racconta l’aneddoto di quando si trovarono a fare debugging su codice in funzione a duecento milioni di chilometri di distanza. E per fortuna che avevano Lisp.

>Quando programmi in Python, hai in funzione un ciclo Repl, read-eval-print [leggi l’espressione, valutala, mostra sullo schermo la risposta]. Così puoi programmare interattivamente. Bene, Lisp è stato il pioniere del ciclo Repl. Che era il modo consueto di interagire con Lisp, il primo linguaggio a permetterlo. E per decenni fu anche l’unico.

La parte interessante è che per inviare una istruzione alla sonda e ottenere la risposta, passava un’oretta. In compenso…

>Grazie alla magia di Lisp e all’idea pazzesca di avere un ciclo Repl funzionante a bordo di una sonda, salvammo la missione.

La trascrizione del podcast rende facile ripercorrere un racconto appassionante e ricco di particolari sull’esplorazione nello spazio profondo post-Luna, con foto d’epoca che per me sono inedite e suppongo potrebbero esserlo per tanti altri.

[Lisp](https://lisp-lang.org) è il secondo linguaggio di programmazione più longevo ([Fortran](https://fortran-lang.org) è arrivato prima, anche se di poco). Ma comprendeva già quello che oggi tanti trovano estremamente comodo in [Python](http://python.it) e, in un certo senso, è ancora il più nuovo.

Non convinto? Un altro ingegnere della Nasa ha raccontato su *Hacker News* del suo [contributo a una missione robotica lunare del 2009](https://news.ycombinator.com/item?id=31253704), [Lcross](https://blogs.nasa.gov/lcrossfdblog/), che ha evidenziato la presenza di ghiaccio d’acqua vicino al polo sud del nostro satellite e di come il secondo linguaggio più antico del mondo sia stato usato per scrivere un simulatore del computer di bordo. Il suo *incipit*:

>Penso che Lisp sia tuttora in uso qua e là nella Nasa.