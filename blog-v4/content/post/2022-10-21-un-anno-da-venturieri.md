---
title: "Un anno da Venturieri"
date: 2022-10-21T00:40:15+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [Mac, macOS, Ventura, iPad, iPad Pro, iPadOS, iPadOS 16, Stage Manager, Utility ColorSync, iMovie, Cube, DaVinci Resolve, BBEdit, Scrivener]
---
Si avvicina a grandi passi la pubblicazione della prima versione ufficiale di macOS Ventura, cui seguirà senza dubbio l’uscita di iPadOS 16.

I due sistemi sono accomunati dalla presenza di Stage Manager, funzione sulla cui completezza e sul cui design leggo ogni giorno (più di) una nuova storia dell’orrore.

Steve Jobs non esitò a ritirare i [Cube](https://macintelligence.org/posts/2019-06-14-la-reinvenzione-delle-ruote/) e accollarsi (come società) le mancate vendite una volta chiaro che c’erano problemi nella percezione del computer presso il pubblico, ancora prima che ci fossero problemi effettivi.

Non è onesto parlare di problemi effettivi fino a che quello che gira per le scrivanie è software beta, eppure la percezione che stavolta il prodotto uscirà inadeguato è difficile da scacciare. *Please, Tim*: se non è a posto, non dovrebbe uscire. Se significa perdere denaro, vuol dire anche mantenere la reputazione e la figura che si fa, alla fine, è migliore. Steve ti ha detto di non pensare a che cosa avrebbe fatto lui, ma di fare la cosa giusta. La cosa giusta è fare uscire software che funziona.

Anche perché non si tratterebbe di un neo in un sistema lindo e sfavillante. macOS rimane il meglio sulla piazza (merito anche della concorrenza, che ha la missione di lavorare contro i propri clienti) e tuttavia ci sono angoli dove è un po’ che non viene passata la polvere.

Penso a Utility ColorSync, rimasto nello stesso [stato di quasi due anni fa](https://macintelligence.org/posts/2021-12-09-infine-ma-non-da-ultimo/), sostanzialmente inutilizzabile.

Mi si dirà, chi lo usa Utility ColorSync? L’ho usato eccome. Certo, mica tutti i giorni prima e dopo i pasti. Ma ha risolto alcune situazioni dove, senza, l’unica soluzione sarebbe stata l’orrendo software Adobe.

Parlando di nicchie un poco più grandi e recenti, ho appena finito di completare un lavoretto con iMovie. Un piccolo video da tre minuti, del tutto semplice, con il minimo sindacale di editing e montaggio.

Si tratta di cose che mi capitano di rado e di cui non conservo il ricordo, normalmente. Stavolta mi è venuta in mente la tortura cinese della [morte per le mille ferite](https://www.languagehumanities.org/what-does-death-by-a-thousand-cuts-mean.htm).

L’instabilità del *timing* delle dissolvenze ancora prima della loro pessima esperienza utente. Il rapporto conflittuale dei titoli con i font e con la tipografia in generale. Il limite di generazione dei video a 720p.

No, Final Cut Pro X non è la soluzione per me. La scuola delle mie figlie dista dieci chilometri e no, la mattina non prendo l’aereo per portarle. Se iMovie dà fastidio, è meglio toglierlo che lasciarlo incustodito. Quelli con le mie esigenze non comprano Final Cut Pro X, ma [DaVinci Resolve](https://www.blackmagicdesign.com/products/davinciresolve/).

Tirchieria? Ma no. La mia specializzazione è sul testo e se mi bastasse software gratuito sarei fortunato, perché TextEdit rimane una gemma. Siccome mi serve qualcosa di più, compro serenamente [BBEdit](https://www.barebones.com/products/bbedit/index.html) senza battere ciglio. Se mi servisse un modo diverso di scrivere, entro dieci secondi sarei su una copia di [Scrivener](https://www.literatureandlatte.com/store/scrivener?tab=macOS). Se mi servisse una volta a semestre, userei TextEdit e sarei felice del mio Mac per il servizio che mi offre. 

iMovie non mi offre (più) la stessa qualità e lo uso una volta a semestre. Utility ColorSync non funziona. Quest’anno si vorrebbe affrontare Ventura pensando alla California, non a che cosa va e che cosa no.