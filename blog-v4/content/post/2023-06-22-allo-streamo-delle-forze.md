---
title: "Allo streamo delle forze"
date: 2023-06-22T23:37:41+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [Streaming Foto]
---
Anche se funzionerà ancora per più di un mese, Apple ha tenuto a comunicarmi per posta che Streaming Foto cesserà di funzionare il giorno 26 luglio e, francamente, dà più fastidio la lettera che il contenuto.

La *ratio* dell’operazione è chiaramente fare pagare iCloud Foto, o come si chiama, a chi sfruttava Streaming Foto gratis. Alzo la mano e faccio un passo avanti: sì, sono anche uno di quelli che per abitudine creava un album condiviso e lo metteva in iCloud, così le immagini non contavano per il limite operativo di Streaming Foto.

Comportamento da svergognati, uno dirà; per carità, è giusto pagare i servizi che si usano. Guarda caso, ho un account iCloud per famiglia e lo pago estremamente volentieri. Mi toglie di impaccio in varie situazioni ed è molto comodo.

Il punto non è che iCloud Foto, o come si chiama, comporti il pagamento; è che *non mi serve*. Non ho alcuna ragione per volere tutte le foto in cloud; anzi, tra famiglia e lavoro ci sono foto che assolutamente vanno condivise, sincronizzate, pubblicate; e altre che restano nella privacy personale oppure professionale, sono pacificamente backuppate e non c’è ragione per cui debbano essere sincronizzate su tutti i miei apparecchi.

Streaming Foto velocizzava mirabilmente il flusso di lavoro in esterni, per uno che fa di iPad il proprio portatile: scatto con iPhone e, grazie a Streaming Photo, in pochi istanti mi ritrovo la foto su iPad. L’ho fatto più volte; *it just worked*.

Ora, per replicare lo stesso flusso, dovrò probabilmente creare qualche album condiviso, o spedirmi le foto da solo via iMessage o qualche altra cosa, meno veloce, meno facile, meno elegante, meno tutto.

D’altronde fa troppo caldo per arrabbiarsi più di tanto, non ce la faccio. Vado a cercare refrigerio in qualche filmato WWDC su Vision Pro.