---
title: "La cultura che avremo"
date: 2023-05-01T02:04:42+01:00
draft: false
toc: false
comments: true
categories: [Software, Internet]
tags: [Dwarf Fortress, Donald Knuth, Knuth, The Art of Computer Programming, Taocp, Ive, Jonathan Ive, Lovefrom]
---
Donald Knuth continua a lavorare indefessamente alla sua Art of Computer Programming, di cui quest’anno [esce il Volume 4B](https://www-cs-faculty.stanford.edu/~knuth/news.html).

Knuth si è autopensionato diversi anni fa per dedicare praticamente il suo intero tempo non libero al progetto, che potrebbe completarsi oltre il 2030. [Ha un’età](https://macintelligence.org/posts/2018-01-23-lapocalisse-dellimmortale/): *quasi un terzo di un byte; un bel palindromo binario e anche il prodotto di due primi di Fermat!* Ha festeggiato il 2023 con il [disegno di un percorso di un cavallo degli scacchi](https://www-cs-faculty.stanford.edu/~knuth/2023.jpg) sopra una scacchiera dieci per dieci, nel quale una regina posizionata al centro (che poi è il punto di arrivo del percorso) tiene sotto scacco tutti i numeri primi tra uno e novantanove.

[Dwarf Fortress](http://www.bay12games.com/dwarves/dev.html), parlando di pensioni, ha recentemente [assicurato ai suoi autori la tranquillità economica](https://macintelligence.org/posts/2023-02-04-una-pensione-meritata/) e di vita per poter stare sulla *roadmap* del gioco, che diventerebbe definitivo [nei dintorni del 2035](https://macintelligence.org/posts/2015-03-08-grandi-opere/) ed è partito nel 2002.

*Fast Company* ha appena titolato [Jony Ive ha passato gli ultimi quattro anni a perfezionare la sua famiglia di caratteri. Ecco perché non arriverà mai a una fine](https://www.fastcompany.com/90888571/jony-ive-spent-the-last-4-years-perfecting-his-typeface-heres-why-hell-never-be-done).

Quando un lavoro di sviluppo trascende il calcolo economico e i criteri di efficienza, magari per andare oltre il tempo di una generazione. significa che quel lavoro va molto oltre il gioco o l’enciclopedia della programmazione. Così come anche la famiglia di caratteri, perché la tipografia fatta bene ha un livello teorico di dettaglio frattale; si può andare avanti all’infinito. Non è più lavoro, è performance artistica; o cultura del nostro tempo che prende forma.