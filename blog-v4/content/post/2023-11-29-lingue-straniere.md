---
title: "Lingue straniere"
date: 2023-11-29T19:27:06+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [Unix, vi, emacs, John D. Cook, Cook, sed, awk, ed, Terminale]
---
Bella riflessione, quella di John D. Cook: [sarebbe interessante avere una linguistica di Unix](https://www.johndcook.com/blog/2023/11/27/unix-linguistics/), nello stesso modo in cui chi ha studiato lingue e ne conosce più di due sa dire che quella certa funzione nella lingua 1 funziona esattamente uguale nella lingua 3 mentre nella lingua 2 le cose sono diverse.

E in Unix, avere studiato più linguaggi aiuta nella stessa maniera. Come scrive lui, le [espressioni regolari](https://macintelligence.org/posts/2019-08-26-mettersi-in-regolare/) sono un linguaggio; si può pensare a [vim](https://www.vim.org) come a un linguaggio. E ovviamente [sed](https://www.gnu.org/software/sed/manual/sed.html) o [awk](https://www.gnu.org/software/gawk/manual/gawk.html) sono proprio linguaggi.

Con così tanti linguaggi, scoprirne gli incroci e le biforcazioni diventa una disciplina quasi filosofica. Cook menziona un libro su sed e & awk dove si trova il capitolo *Awk, by Sed and Grep, out of Ed*.

È materia più profonda della pur vasta aneddotica storica sul come sono nati i nomi e come si sono evoluti i comandi (per esempio, `awk` prende il nome dalle iniziali dei cognomi dei suoi tre autori).

>Per esempio, è ben noto come `grep` prenda il suo nome dal comando `g/re/p` in `ed` e sia nato dalla modifica del codice sorgente di `ed` stesso. Dal canto suo `ed` è un progenitore comune di `grep`, `sed` e `vi`.

Il fascino suscitato dal Terminale deriva anche dal suo (ingente) portato culturale e a maggior ragione per chi ha compiuto il giro più largo, con partenza dall’interfaccia utente grafica per scoprire prompt e scripting. Una babele di lingue e linguaggi, travestiti da comandi con formule arcane.