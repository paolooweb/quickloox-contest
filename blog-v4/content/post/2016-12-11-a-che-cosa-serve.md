---
title: "A che cosa serve"
date: 2016-12-11
comments: true
tags: [iPad, watch, Mac]
---
Il computer ha suscitato la rivoluzione che ha fatto perché non serviva a niente. Ovvero, possedeva infinite possibilità, che andavano concretizzate.<!--more-->

La *killer app*, l’applicazione killer così chiamata perché da sola fa vendere il computer, è il caso estremo. Più banalmente: sono innumerevoli le cose che faccio su Mac e nel 1984, quando Mac è nato, erano impossibili. A naso il novanta percento delle cose che faccio su iPad era irrealizzabile sul primo iPad, quello del 2010.

watch, quello su mi fanno le maggiori domande, mi fornisce l’ora esatta – banale – e poi una serie di servizi che pochi o tanti, buoni o cattivi, belli o brutti, nessun orologio ha mai svolto fino a pochi semestri fa.

*A che cosa serve*, *che cosa te ne fai*, *che cosa fa di particolare* sono le domande di una mente povera.

Per non parlare dell’ineguagliabile commento *fa le stesse cose*. Il computer è un apparecchio generico: tutti i computer, in potenza, possono fare tutto. Vantarsi di poter fare un certo numero di cose è ignorare il numero assai più enorme di altre cose che si potrebbero fare, se solo lo si sapesse.

Giusto per chiarire.