---
title: "Le parole per dirlo"
date: 2014-04-16
comments: true
tags: [Dediu, Asymco, creazione, invenzione, innovazione, novità, illiteracy, innumeracy, innoveracy]
---
Stiamo [chiacchierando da un po’](https://macintelligence.org/posts/2014-04-12-bisogna-provare-tutto-nella-vita/) sulle differenze tra iOS e Android e un [articolo di Horace Dediu di Asymco](http://www.asymco.com/2014/04/16/innoveracy-misunderstanding-innovation/), che sembra laterale e lontanissimo, risulta invece essenziale per formarci una capacità di giudizio atta a valutare in modo utile quello che ci troviamo tra le mani.<!--more-->

Dediu distingue tra *novità* (qualcosa di nuovo), *creazione* (qualcosa di nuovo con un valore), *invenzione* (qualcosa di nuovo che ha un valore potenziale di utilizzo) e *innovazione* (qualcosa di nuovo e utile in modo unico).

Fornisce esempi di questi quattro concetti: novità come *la scelta del colore oro per iPhone, chiamare KitKat una versione di Android, coniare una nuova parola*. Creazione come *una collezione autunno/inverno, un nuovo film, il post di un blog*. Invenzione come *le cose descritte nei brevetti o la formula segreta della Coca-cola*. Innovazione come *il modello di prezzo di iPhone, il modello di fatturato di Google, il sistema di produzione di Ford, il design dei negozi Wal-Mart, la logistica di Amazon*.

E arriva anche a definirli in base alla protezione che si può loro applicare. Le novità *di solito non si possono proteggere, ma essendo di valore molto limitato la loro copia non causa gran problema*. Le creazioni *sono protette da copyright o trademark ma non sono brevettabili perché mancano di utilizzabilità*. Le invenzioni *possono essere protette per un tempo limitato oppure indefinitamente, tenendole segrete*. Le innovazioni *si possono proteggere grazie alla concorrenza sul mercato ma non si riescono a difendere con mezzi legali*.

Tutto questo appartiene secondo Dediu alla *innoveracy*, l’incapacità di comprendere il linguaggio dell’innovazione (come l’*illiteracy* è l’analfabetismo e l’*innumeracy* è l’analfabetismo sui numeri). Abbiamo tutti difficoltà a distinguere l’innovazione da fenomeni meno importanti.

Un bell’esercizio: la *app* che ci piace, la funzione che ci fa comodo, la funzione che ci semplifica la vita, l’oggetto che portiamo con orgoglio in tasca e tutto il resto, sono novità, creazione, invenzione o innovazione?