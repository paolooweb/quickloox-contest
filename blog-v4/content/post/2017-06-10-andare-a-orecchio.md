---
title: "Andare a orecchio"
date: 2017-06-08
comments: true
tags: [tvOS, AirPods, Wwdc]
---
Altra conferma del fatto che gli annunci di Wwdc [sono solo una frazione](https://macintelligence.org/posts/2017-06-07-a-margine/) di quanto sarebbe stato annunciabile: *9to5Mac* informa che gli AirPods [si collegano automaticamente a una tv](https://9to5mac.com/2017/06/08/airpods-apple-tv-tvos-11/) nella versione beta del nuovo tvOS.

Prima il collegamento era possibile ma manualmente: come dice il sito, certo non in modo rapido e indolore come ora.

Il che la dice lunga sulla centralità degli AirPods oltre che sugli annunci di Wwdc, durante i quali Tim Cook ha praticamente glissato su tvOS.
