---
title: "Multimediale innocenza"
date: 2015-04-22
comments: true
tags: [Lidia, Scorekeeper, iPad, design, UI, UX]
---
Quando papà usa iPad o iPhone, figlioletta vuole a tutti i costi capirne di più. Così, tra i peluche da stringere, gli anelli da dentizione da mordere e gli oggetti più disparati da toccare lanciare smontare, c’è anche l’iPad di prima generazione, provvisto di provvidenziale custodia in materiale perfetto da mordere senza denti, ciucciare, insalivare, pasticciare.<!--more-->

Di tanto in tanto Lidia riesce del tutto involontariamente ad accendere iPad e accedere alle schermate dove gradualmente si accumulano app per l’infanzia assieme a quelle scaricate per i genitori. È assolutamente troppo presto perché abbia coscienza dell’interazione con lo schermo: le manine strisciano, picchiano, premono, appoggiano in maniera del tutto casuale, con risultati imprevedibili.

A parte l’occasionale gioco che parte con colonna sonora e quindi rimane per qualche istante oggetto di interesse, il prodotto dell’interazione tra l’oggetto di cinque anni e la bimba di otto mesi è complessivamente noioso, visto da un adulto e forse non solo; il divertimento sta nel sollecitare fisicamente l’oggetto assai più che nei risultati dell’interazione stessa.

Con una importante eccezione. Stasera Lidia è arrivata all’interfaccia di *input* di [Scorekeeper XL](https://itunes.apple.com/it/app/scorekeeper-xl/id463243024?l=en&mt=8) ed è rimasta conquistata dall’insieme di colori, effetti, transizioni, fluidità, animazioni, suoni, forme.

In effetti il *design* è estremamente comunicativo e funzionale, ma anche stimolante e giocoso per risultare piacevole. Un gran bel risultato in termini di progettazione e realizzazione. Che non è il più colorato, né il più rumoroso, né il più accattivante. Fa quello che deve fare, molto bene, senza equivoci, senza che alcunché possa andare male.

Non sono certo io a scoprire che i bambini hanno intuito per il buon *design* molto più che gli adulti condizionati da anni di esperienze e (pre)giudizi. È stata comunque una conferma affascinante.