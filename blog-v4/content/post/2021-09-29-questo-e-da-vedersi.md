---
title: "Questo è da vedersi"
date: 2021-09-29T01:19:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [Keynote, Pages, Numbers, iWork] 
---
Sinceramente non ho capito bene se si chiami ancora iWork o se sia una semplice triade di programmi per scrivere, fare di conto e presentare; la loro evoluzione pareva comunque giunta a un punto nel quale questa o quella miglioria appariva marginale.

L’aggiornamento 11.2 [fa eccezione](https://fossbytes.com/iwork-update-keynote-pages-and-numbers-new-features/). Le novità di poco conto sono molte, ma ce ne sono anche di rilevanti. Pages ha una anteprima del testo che si adatta allo schermo in uso in modo più accurato; Numbers ha le tabelle pivot, cosa che ha il potenziale di scatenare persone intelligenti con un motivo in più per togliersi dal tossico Excel.

Il video in diretta dentro le slide Keynote però batte tutto il resto e meriterebbe la ribalta da solo. Gli utilizzi potenziali sono molto interessanti e a questo punto non vedo l’ora di toccare gli aggiornamenti con mano per vedere se l’interfaccia è del livello che aspetto, oltre che vedere il mio faccione dentro Keynote.

*Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile.*