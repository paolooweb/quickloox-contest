---
title: "Esami di riparazione"
date: 2022-05-27T16:45:24+01:00
draft: false
toc: false
comments: true
categories: [Hardware]
tags: [iPhone, Doctorow, Cory Doctorow, Medium, Gruber, John Gruber, Daring Fireball, iFixit]
---
Nel dibattito sull’ipotetico diritto alla riparazione degli iPhone si è distinto particolarmente Cory Doctorow, un tempo protorivoluzionario difensore del proletariato tecnologico nella grande piazza della Rete con la maiuscola, oggi vecchio trombone che ha scelto il marchio della ribellione cronica, che è una malattia: se ti ribelli per affermare una causa sei un ribelle, mentre se ti ribelli per professione o per riflesso condizionato sei stato segnato in modo permanente da una malattia della crescita intellettuale.

Parlo di Doctorow perché [ha preso posizione in modo roboante e demagogico](https://doctorow.medium.com/apples-cement-overshoes-329856288d13) come suo solito e nemmeno ha fatto niente: si è limitato a riprendere, come se fosse successa a lui, l’esperienza del [programma di riparazione fai-da-te di iPhone](https://www.apple.com/newsroom/2022/04/apples-self-service-repair-now-available/) promosso da Apple, [raccontata da Sean Hollister su The Verge](https://www.theverge.com/2022/5/21/23079058/apple-self-service-iphone-repair-kit-hands-on).

Hollister si è ritrovato con quaranta chili di attrezzatura a noleggio per una settimana e un deposito cauzionale di milleduecento dollari e ha usato abbondante ironia nel commentare quello che a lui sembra una sostituzione grottesca di strumenti offerti comunemente da iFixit e altri, più leggeri, meno costosi, più facili.

In altre parole, Apple complica inutilmente procedure di riparazione che sono abbondantemente disponibili a prezzi accessibili. E questo sarebbe negare il diritto alla riparazione.

Doctorow ha recuperato vecchi arnesi come l’obsolescenza programmata per dire che Apple profitta ingiustamente dal vendere apparecchi non riparabili per danneggiare l’ambiente incoraggiando l’acquisto del nuovo a sfavore della riparazione del vecchio. È una cosa che ho letto milioni di volte e mai accompagnata da un calcolo attendibile di quanto costi e impatti la riparazione di apparecchi arbitrariamente vecchi rispetto al loro riciclo.

Ovvio che non abbia risparmiato ironie sul costo di noleggio delle apparecchiature di Apple, che comprende la spedizione di andata e ritorno ed è del tutto antieconomico. Per Apple, quella che insegue il profitto sulle riparazioni. (Ma non lo inseguiva scoraggiandole per vendere il nuovo? Incredibile come la tesi rimbalzi da una nozione all’altra come una pallina da ping-pong).

A un certo punto è entrato in scena anche John Gruber su *Daring Fireball*, che risparmia il tentativo di riassumere due articoli chilometrici in maniera maneggevole grazie a un commento definitivo, [L’insulto del ricevere gli strumenti appropriati per effettuare una attività complessa](https://daringfireball.net/2022/05/grave_insult). Due paragrafi risolvono la questione. Il primo è il seguente:

>Una possibile spiegazione della complessità e dell’ingombro del toolkit Apple per l’autoriparazione è che gli iPhone sono apparecchi intricati, che richiedono strumenti e macchine speciali per essere aperti e per operarvi, assieme a istruzioni circostanziate. E che anche muniti di tutto questo, richiedano grande attenzione nell’esecuzione. Come è tipico di queste attività, l’esperienza conta molto. È per questo che finora Apple ha affidato le riparazioni di iPhone e altri apparecchi a se stessa o a partner certificati, fidati e addestrati. Perché il Self Service Repair Program funzioni, i riparatori fai-da-te avranno bisogno degli stessi strumenti professionali e di istruzioni dettagliate. Tutto ciò è necessariamente costoso e complicato e, a parte questo, Apple non può fare nulla per impartire ai riparatori fai-da-te esperienza che non hanno. Apple non può fare alcunché per rendere queste riparazioni rapide o semplici.

Ma come? Doctorow per esempio insiste sul fatto che negli iPhone si usa colla al posto di viti, proprio – secondo lui – per complicare la riparazione. Perché non fare gli iPhone con le viti? Ancora Gruber:

>Chi realmente pensa a una batteria o a uno schermo di iPhone riparabili semplicemente con “un piccolo set di cacciaviti, leve e pinzette”, pensa in effetti a iPhone (e altri apparecchi) progettati, ingegnerizzati e assemblati in modi diversi dagli attuali. Naturalmente sembra bello, ma non è così che funzionano gli apparecchi di oggi. Apple non è una mosca bianca: non esistono apparecchi mobile di grande diffusione e facile assistenza. Se fosse possibile avere iPhone facilmente riparabili senza sacrificare aspetto, dimensioni, prestazioni, resistenza ad acqua e polvere e costo, Apple li farebbe più facilmente riparabili. Altrimenti si vuole sostenere che il cambio della batteria in Apple Store ai prezzi attuali sia un’occasione di profitto per Apple?

La verità è che, dei tanti mercati paralleli e ancillari a quello di iPhone, che prosperano in funzione delle vendite, c’è anche quello dell’idea che Apple se ne approfitti, sempre e comunque, in qualsiasi modo, in ogni circostanza. Che questo possa avvenire in certe circostanze, non c’è dubbio. Il sostenere che succeda sempre fa vendere di più però; qualsiasi argomento va allora rigirato in modo da cadere sempre lì.