---
title: "Cambio di vocale"
date: 2014-11-18
comments: true
tags: [ePub, iBook, iBooksStore, iBooksAuthor, ebook, ePub, Glazblog, TheUnarchiver, Xml, Html, Css, JavaScript, namespace, widget, Dashboard, OSX, Amazon, Kindle, briand06, paoloo]
---
Chiacchierando e commentando mi sono reso conto di non sapere che cosa stia dentro un iBook, di quelli creati con [iBooks Author](https://www.apple.com/it/ibooks-author/).<!--more-->

La prima considerazione è che un iBook è diverso da un *ebook*. Quest’ultimo si intende comunemente realizzato secondo lo standard [ePub](http://idpf.org/epub/30), anche se la realtà è decisamente più variegata e, per fare l’esempio più eclatante, sono *ebook* anche quelli per Kindle di Amazon, che impiega un [formato diverso da ePub](https://www.mobipocket.com/dev/article.asp?BaseFolder=prcgen&File=mobiformat.htm) sotto vari aspetti.

Un iBook è un’altra cosa, quindi; rimaneva il dubbio su che tipo di altra cosa e si era arrivati a ipotizzare anche la possibilità di chiamate dirette alle architetture software presenti in OS X e iOS.

La soluzione all’enigma era ovviamente aprire un iBook: ho scelto simbolicamente *Life on Earth*, un testo di biologia un cui estratto è stato pubblicato gratis su iBooks Store al momento dell’annuncio di iBooks Author (oggi è disponibile gratis l’intera opera, in sette volumi; ci si arriva con una ricerca dentro iBooks).

Aprire un iBook (o un *ebook*) su Mac non è interamente ovvio: bisogna prima cambiare l’estensione in .zip e poi aprire l’archivio compresso con una utility di decompressione, perché il Finder si lascia confondere dal formato (è uno .zip compresso secondo regole particolari). Io uso [The Unarchiver](https://itunes.apple.com/it/app/the-unarchiver/id425424353?l=en&mt=12), delle tante a disposizione.

Non sto a entrare nei dettagli tecnici specifici e mi limito a constatare che il formato usato da Apple *potrebbe* essere a prima vista ePub, mentre invece se ne distacca nella forma e nella sostanza come diventa ben presto chiaro.

Lo scopo evidente di questa dipartita dallo standard è inserire elementi di multimedialità e interattività che ePub non contiene e, se ha in programma di farlo, conterrà tra molto tempo. Il formato evolve lentamente e in modo macchinoso, come tutto quello che dipende da un comitato. Potrebbero passare anni prima che offra le possibilità di un iBook attuale.

Il modo in cui gli elementi extra vengono inseriti ha niente a che vedere con il sistema operativo e *potrebbe* fare parte di una versione futura di ePub, se tutti trovassero un accordo. Apple ha definito alcuni *namespace* in Xml tutti suoi (in pratica nuovi piccoli universi di oggetti definiti attraverso marcatori Xml) che servono, tra l’altro, a ospitare *widget* interattivi e multimediali tali e quali nella struttura a quelli presenti in Dashboard dentro OS X.

[Xml](http://www.w3.org/XML/), quindi. E poi si vedono JavaScript (a volte compilato), Html, Css. Se le specifiche diventassero di dominio pubblico, tutto potrebbe tranquillamente arrivare a funzionare su un altro sistema operativo.

[Glazblog](http://www.glazman.org/weblog/dotclear/index.php?post/2012/01/20/iBooks-Author-a-nice-tool-but) va più sul tecnico, per un primo approfondimento a partire da qui.

Grazie a **briand06** e **paoloo**.