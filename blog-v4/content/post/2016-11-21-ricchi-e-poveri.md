---
title: "Ricchi e poveri"
date: 2016-11-21
comments: true
tags: [Amazon, Apple, iBooks, Store]
---
Avevo scritto della [povertà degli strumenti per l’amministrazione delle inserzioni pubblicitarie](https://macintelligence.org/posts/2016-08-01-l-arte-della-fuffa/) sulle reti sociali. Ora sono reduce dall’essere diventato editore di libri non gratis su iBooks Store e Amazon. E anche qui c’è una povertà di mezzi che inquieta.

Pare che le due aziende si siano spartite le aree di trascuratezza. Mentre Apple offre la *app* iTunes Producer per inviare un libro in approvazione, Amazon mette un pulsantino per inviare il libro sui suoi server. C’è *feedback* zero e uno può solo supporre che il trasferimento stia avvenendo davvero; con file che possono superare i cento megabyte, avere un’idea della progressione è importante.

In compenso Amazon accompagna con domande mirate la fase della dichiarazione al fisco statunitense di risiedere in un Paese che ha stipulato un accordo con gli Stati Uniti sugli scambi commerciali. Uno risponde domanda dopo domanda e si ritrova con il modulo compilato. Apple, invece, se ne frega e ti fa scaricare il modulo originale, che è lunare nella sua enigmaticità.

Vince Apple sulla reportistica, disponibile anche come *app* di nome iTunes Connect. Invece vince Amazon nella durata del processo di approvazione. In Apple contano e misurano i capelli dell’autore uno per uno e una banalità formale può bloccare la procedura per una settimana, con messaggi criptici e una logica impeccabile in un mondo parallelo: scrivo al reparto legale spiegando le mie ragioni su una questione che blocca l’approvazione, ma non è garantito che i legali rispondano. Come faccia a sapere se devo attendere una risposta oppure no, e in quanto tempo, non è dato sapere. Amazon in ventiquattro ore risolve.

Se solo Amazon avesse un formato meno assurdo dell’attuale, che mortifica qualsiasi lavoro sull’estetica e la tipografia di un ePub, sarebbe da preferire. Se solo non offrissero la metà delle *royalty* che offre Apple, ove il prezzo di copertina sia diverso dalle fasce privilegiate.

Comunque: strumenti software tenuti insieme con lo stecchino, o di approssimazione imbarazzante. Procedure bizantine e poco chiare. Interfacce evidentemente pensate da gente che non le deve usare. I libri non sono una grande fonte di reddito per una Apple e figuriamoci per una Amazon, tuttavia sembra che l’idea sia semplicemente fare patire per mezza giornata il poveraccio di turno. A me non sembra un modo favoloso per incentivare la pubblicazione di ebook e avere un catalogo che invogli la gente a entrare nell’ecosistema (anche) per leggere.

Girandola in positivo, si fa presto a capire quanta messa a punto vi sia nelle interfacce utente per i consumatori. Perché appena quel livello di attenzione declina, il software diventa subito inusabile e involuto. Nonostante contribuisca – in linea di principio – ad arricchire la Apple o la Amazon di turno.
