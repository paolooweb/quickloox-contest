---
title: "Tocchiamo questo tasto"
date: 2019-02-17
comments: true
tags: [Gruber, iPad]
---
È da diverse settimane che utilizzo iPad Pro e sono veramente soddisfatto: uno dei computer migliori della ma vita.

Al momento mi trovo allineato nel giudizio con John Gruber. Nella sua [pagella di Apple per il 2018](https://daringfireball.net/2019/02/my_2018_apple_report_card), ha parlato dell’hardware iPad come di una cosa spettacolare, quasi una visita dal futuro, e non potrei essere più d’accordo.

Sul software ho dei distinguo nei particolari ma concordo nella sostanza: iPad è talmente progredito che merita una revisione su misura del software di sistema, dato che iOS nella sua forma attuale non ne esalta tutte le,potenzialità.

Su che cosa esattamente sarebbe utile, penso che ci troveremmo a discutere, *si parva licet*.

Però un indizio che qualcosa serva, l’ho sotto gli occhi ogni giorno: in orientamento verticale, la tastiera italiana e quella americana hanno diverse dimensioni dei tasti. Segnatamente, nella tastiera americana la striscia dei tasti numerici ha una altezza minore, compensata da una tastiera alfabetica più alta.

Mi chiedo se sia voluto, oppure un accidente. Comunque sia, non lo vorrei vedere; la tastiera dovrebbe avere un aspettò uniforme. Invece salta all’occhio a ogni cambio di lingua. Ed è un piccolo, lampante segnale che la piattaforma chiede attenzione.
