---
title: "Restano solo gli auguri"
date: 2014-12-24
comments: true
tags: [Natale]
---
Gli auguri quelli veri.<!--more-->

Natale in compagnia se si può. Se non si può, in rete. Sereni e fiduciosi: le giornate hanno iniziato ad allungarsi, solo che ancora nessuno se ne accorge.

Il più possibile relax. Se si riesce, un gioco in compagnia. Quanto meno, un gioco in compagnia remota. Oppure un libro che sollevi lo spirito.

Nessun proposito per il nuovo anno, quelli arrivano il 31. E poi a Santo Stefano verrà voglia di dormire tutto il giorno, o partire per il giro del mondo senza scalo.

Vale anche il Natale senza informatica, anche se è difficile, perché probabilmente pioveranno da ogni parte auguri belli e brutti (valgono tutti). Ogni anno cerco di rispondere a tutti e a volte finisco a metà gennaio, oppure mi dimentico qualcuno. A parte questo…

**Buon Natale. Fai clic su qualcosa che ne valga la pena.**

Grazie!