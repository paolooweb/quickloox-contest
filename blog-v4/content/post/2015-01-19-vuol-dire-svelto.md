---
title: "Vuol dire svelto"
date: 2015-01-19
comments: true
tags: [Swift, AppleScript, StackOverflow, GitHub, RedMonk, Objective-C, scripting, Xcode, Playground]
---
Non i linguaggi di programmazione più usati, ma quelli di cui si parla di più su [StackOverflow](http://stackoverflow.com) e di cui esistono più righe di codice in [GitHub](https://github.com): questa la classifica stilata periodicamente da RedMonk e appena [aggiornata a gennaio 2015](http://redmonk.com/sogrady/2015/01/14/language-rankings-1-15/).<!--more-->

Objective-C, il linguaggio usato finora per le *app* iOS e Mac, sta al decimo posto (stare tra i *top 20* è una posizione di prestigio; il numero di linguaggi esistenti è [inimmaginabile](http://en.wikipedia.org/wiki/List_of_programming_languages)).

La sorpresa del semestre è [Swift](https://www.apple.com/it/swift/), creato da Apple per sostituire il vecchio Objective-C con qualcosa di più semplice e sicuro. Swift non è presente nella *top 20*… perché è salito dalla posizione 68 alla 22, un sorpasso di quarantasei altri linguaggi nel giro di pochi mesi (è stato annunciato a giugno). La classifica di RedMonk ha normalmente un andamento glaciale, con spostamenti lenti e di poco impatto. In questo contesto Swift ha realizzato qualcosa di incredibile.

Ci sto giocando con Swift, ed è affascinante. È anche complesso; è un linguaggio che poco si presta allo *scripting* e al lavoro *quick and dirty*, poco stramaledetto e subito, anche se Apple ha realizzato un piccolo capolavoro con i Playground dentro Xcode: scrivi una istruzione, anche se non è un programma completo, anche se non hai compilato ancora niente, e già nella colonna di fianco vedi il risultato, il valore assegnato alle variabili, la correttezza della sintassi.

Non posso consigliare Swift a cuor leggero come farei con [AppleScript](http://www.macosxautomation.com/applescript/). Richiede vero impegno. Potrebbe però dare vere soddisfazioni, se l’impegno c’è.

*Swift* in inglese vuol dire *svelto*. Concordiamo, io e la classifica di RedMonk.