---
title: "Per qualcuno è sempre festa"
date: 2020-08-17
comments: true
tags: [Debian, Cpan, Mutt]
---
Impegnato in un compleanno di famiglia, mi sono distratto da quelli di Debian, [ventisettenne](https://bits.debian.org/2020/08/debian-turns-27.html) (un classico *sembra ieri*) e [Comprehensive Perl Archive Network](http://blogs.perl.org/users/neilb/2014/07/cpan-day---14th-august.html) (Cpan) che ne compie venticinque e mai avrei scommesso che potesse essere più giovane di Debian.

Solo che [passa il quarto di secolo](https://slashdot.org/submission/12189243/mutt-25-years-old) anche [Mutt](http://www.mutt.org), il programma di posta via Terminale che adotterò una volta finite altre novemilacinquecento cose più urgenti e arretrate, e che avrei pensato di mezza età o quasi.

Sembrano tutti in ottima forma e aggiornamento costante, come peraltro dovrebbe essere per degli under 30; solo Perl si trova davanti a una scelta di vita importante. Decidere [se e quanta compatibilità mantenere con il passato](https://lwn.net/Articles/828384/) scontenta per forza qualcuno ma, quando va fatto, va fatto.

Debian più vecchio di Cpan e Mutt. Accidenti. Comunque, [festa](http://www.mutt.org/image/mutt25years.png).
