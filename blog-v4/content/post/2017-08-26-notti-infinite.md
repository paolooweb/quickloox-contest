---
title: "Notti infinite"
date: 2017-08-26
comments: true
tags: [Siri, Machine, Learning, Journal]
---
Ritorno sul [Machine Learning Journal messo in piedi da Apple](https://macintelligence.org/posts/2017-07-30-finire-sui-giornali/) perché gli articoli pubblicati sono diventati più di uno e ne sono arrivati tre nuovi in fila. Segno che c’è una qualche volontà di proseguire con l’iniziativa e anche l’interesse a pubblicare.<!--more-->

I temi trattati sono molto più per addetti ai lavori che per appassionati di scienza, ma visto che è estate voglio ugualmente segnalare la [Normalizzazione inversa del testo vista come una questione di etichette](https://machinelearning.apple.com/2017/08/02/inverse-text-normal.html): una spiegazione sofisticata ma non troppo di come il *team* di Siri lavori in modo tale che, anche se noi diciamo *ventisei agosto duemiladiciassette*, l’assistente vocale visualizzi *26 agosto 2017*. È solo un esempio e la faccenda è meno banale di quanto sembri.

Tra un paio di anni tutti parleranno solo di realtà aumentata ma nel frattempo si daranno al *machine learning* e almeno una infarinatura la suggerisco vivamente.