---
title: "L’Ottusa muraglia"
date: 2019-12-12
comments: true
tags: [Cina, Windows]
---
Potrebbe essere una notizia migliore, perché [l’amministrazione statale cinese rinuncia a Windows entro il 2022](https://www.techradar.com/in/news/china-to-ditch-all-windows-pcs-by-2022-could-this-be-linuxs-time-to-shine) non perché ci sia di meglio, ma per questioni politiche e commerciali.

È il *diktat* di un regime oppressivo, non una scelta basata su criteri ragionevoli.

Nondimeno, Windows sparirà dal trenta percento dei computer di stato entro il 2020, il cinquanta percento entro il 2021, l’ultimo venti percento per fine 2022.

Il punto è che si può fare. Si parla di un numero tra venti e trenta milioni di postazioni. Dire enorme è poco.

Oltre la Grande muraglia faranno a meno di Windows. Misteriosamente non avranno problemi di compatibilità, formati di interscambio, standard di diritto e di fatto, tradizioni aziendali e altre inezie.

Eppure so che domani uscirà di casa e continuerò a trovarmi di fronte l’Ottusa muraglia di quelli che senza Windows non si può vivere, la scelta obbligata, lo standard industriale, tutte le solite cazzate.

Sono cazzate. E il governo cinese, oltre a non andare per il sottile con chi disobbedisce, è pesantemente pragmatico. Se ordinano di farlo, è fattibile. Se decidono di farlo, comunque, è perché gli conviene.