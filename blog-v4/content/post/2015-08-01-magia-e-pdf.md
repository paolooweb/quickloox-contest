---
title: "Magia e Pdf"
date: 2015-08-01
comments: true
tags: [Macscrpt, Windows, ColorSync, Quartz, Pdf, AppleScript]
---
Racconta l’amico con Windows che *anni fa mi hanno regalato Acrobat Pro e così riduco le dimensioni dei Pdf esattamente come voglio senza alcun problema*.<!--more-->

L’altro amico, con Mac, lo invidia.

Faccio notare che *lo sanno in tre, ma Utility Colorsync contiene un sistema di filtri che, una volta compreso anche empiricamente, permette di ottenere gli stessi risultati ed è di serie dentro OS X da prima che io riesca a ricordare*.

Purtroppo Utility ColorSync, a quanto mi risulta, non è accessibile da AppleScript. Tuttavia si possono ottenere risultati equivalenti lavorando direttamente su Quartz, il sistema grafico di cui è provvisto OS X. Sulla *mailing list* [MacScrpt](http://listserv.dartmouth.edu/scripts/wa.exe?A0=MACSCRPT) è comparso da pochissimo uno *script* come il seguente:

`use AppleScript version "2.3.1"`
`use scripting additions`
`use framework "Foundation"`
`use framework "Quartz" -- for PDF stuff`
``
`on extractPages:firstPage thruTo:lastPage ofPDFDocAt:posixPath`
`	--  make URL of the first PDF`
`	set inNSURL to current application's class "NSURL"'s fileURLWithPath:posixPath`
`	-- make PDF document from the URL`
`	set theDoc to current application's PDFDocument's alloc()'s initWithURL:inNSURL`
`	-- count the pages`
`	set pageCount to theDoc's pageCount()`
`	-- delete pages at end`
`	if lastPage < pageCount then`
`		repeat with i from pageCount to (lastPage + 1) by -1`
`			(theDoc's removePageAtIndex:(i - 1)) -- zero-based indexes`
`		end repeat`
`	end if`
`	-- delete pages at start`
`	if firstPage > 1 then`
`		repeat with i from (firstPage - 1) to 1 by -1`
`			(theDoc's removePageAtIndex:(i - 1)) -- zero-based indexes`
`		end repeat`
`	end if`
`	-- save in new file`
`	set newPath to inNSURL's |path|()'s stringByDeletingPathExtension()'s stringByAppendingString:("_" & firstPage & "-" & lastPage & ".pdf")`
`	theDoc's writeToFile:newPath`
`end extractPages:thruTo:ofPDFDocAt:`
``
`set thePath to POSIX path of (choose file of type {"pdf"})`
`my extractPages:2 thruTo:5 ofPDFDocAt:thePath`

La comprensione del cui scopo viene lasciata come esercizio, di inglese più chi di programmazione, al lettore.