---
title: "Automatismi e scorciatoie"
date: 2018-06-27
comments: true
tags: [Sal, Soghoian, Siri, Workflow, AppleScript, Shortcuts]
---
Sempre un po’ preoccupati per il futuro dell’automazione su Mac visto il [pensionamento del mago Sal Soghoian](http://www.macintelligence.org/blog/2016/11/20/aut-out/), se non altro possiamo leggere incantati il [resoconto di Federico Viticci](https://www.macstories.net/stories/shortcuts-a-new-vision-for-siri-and-ios-automation/) sulle scorciatoie con Siri proprie del prossimo venturo iOS 12.

L’acquisto di Workflow da parte di Apple sta portando frutti per il meglio e la nuova app Shortcuts, scrive *Ticci*, è la *automation powerhouse* che si sperava nascesse proprio da quella acquisizione. Un lavoro che *ottimisticamente non farà rimpiangere Workflow*.

Dopotutto una scorciatoia che si appoggia a Siri non è un concetto tanto diverso da una combinazione di tastiera per realizzare un compito particolare. Rimane il fatto che iPad viene posizionato come computer, anzi, come superamento del concetto di computer, e le scorciatoie per un iPad Pro sembrano un po’ poco. Se dovessi predire il futuro scriverei che gli sviluppatori saranno invitati a trovare modi creativi per saldare le scorciatoie di iOS 12 ad AppleScript su Mac attraverso Swift, ma sembra un po’ troppo macchinoso.

Per il momento sappiamo che l’automazione su iOS cresce e questa, comunque, è una buona notizia. Chi è cosciente dei vantaggi raggiungibili per tutti, inclusi gli ignari del valore che hanno sotto le mani, la sostenga più che può.