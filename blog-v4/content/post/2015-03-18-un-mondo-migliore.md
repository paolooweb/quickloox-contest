---
title: "Un mondo migliore"
date: 2015-03-18
comments: true
tags: [AppleScript, Flavio]
---
La parola a **Flavio**. Tutto quello che va da **grassetto** a **grassetto** è suo.

**Ho letto diversi tuoi articoli circa AppleScript** ma non avevo mai approfondito il tema a fondo, vuoi per pigrizia, vuoi per mancanza di tempo.

A novembre del 2014 una lunga convalescenza mi ha costretto a casa per diverse settimane, e allora ho deciso di… lanciarmi.

L’idea nasce da una recensione di uno di quei termostati digitali “intelligenti” che controllano automaticamente il riscaldamento di casa sulla base di vari parametri.

Io cercavo da tempo un sistema per controllare ed ottimizzare il riscaldamento… ma tutti i termostati compresi quelli “intelligenti” non tengono conto degli orari delle famiglie i cui membri lavorano su turni H24! Il mio termostato doveva far partire il riscaldamento QUANDO SIAMO IN CASA, a prescindere dall’orario, e non in base a uno schema basato meramente sulle ore diurne e serali.

Perché, allora, non farne uno *home made*, imparando un po’ di AppleScript?

Allora compro su Ebay (40 dollari) un *relay* Ethernet con sensore di temperatura. Semplicissima da usare e configurare, questa scheda rileva la temperatura ambientale tramite una sonda e, all’occorrenza, chiude o apre un contatto pulito tramite un *relay* incorporato. Tutto *onboard* e controllato via Ethernet.

Da qui, il passo è stato breve. Il controller, tramite un Curl sulla pagina dei parametri – in formato Xml – del server web onboard, era in grado di fornire al Mac la temperatura della casa. E di conseguenza anche lo stato del *relay* e quindi della caldaia dell’impianto di riscaldamento.

Poi mi sono inventato un sistema (molto semplice ma che non ho trovato da nessun’altra parte su Internet) per rilevare tramite gli iPhone, via Wi-Fi, la presenza in casa di un membro della famiglia. Entrambi lavoriamo su turni H24, quindi un termostato che accendesse di sera e spegnesse la mattina aveva poco senso. Invece ora il Mac era in grado di capire quanti gradi c’erano, se il riscaldamento era acceso o spento e, soprattutto, SE C’ERA QUALCUNO IN CASA!

Anche da qui, il passo è stato breve. La mia applicazione (*always running*) Applescript, è progressivamente cresciuta: se all’inizio controllava il riscaldamento e adattava la temperatura in base alla nostra presenza in casa, poi ha cominciato a controllare anche la musica (già diffusa in ogni stanza grazie a un cablaggio realizzato in sede di ristrutturazione) tramite iTunes e AppleTV. Quindi quando arriviamo a casa, oltre al riscaldamento, Applescript fa un bel *tell application* a iTunes e gli dice di suonare questa o quella *playlist*, a seconda di chi viene rilevato in casa.

E poi è stato un progredire esponenziale.

Tramite le lampadine e lo *switch* Hue di Philips ora il Mac controlla anche le luci. E tramite una semplice *webapp* (scritta in cinque minuti, scopiazzando brani di codice qua e là) che sfrutta Php, ora chiunque nella famiglia può puntare la sveglia tramite iPhone. Il Mac provvederà ad accendere il riscaldamento un’ora prima dell’ora prescelta e al momento della sveglia accenderà luci e musica con un *fade-in* delicato e gentile che porterà le luci della camera da letto e la musica selezionata (nel mio caso brani di Mozart) ad aumentare delicatamente e progressivamente durante i minuti della sveglia.

E così via.

Se non c’è nessuno in casa, visti i tempi, perché non attivare un ciclo casuale di accensione/spegnimento delle luci delle varie stanze collegate allo *switch* Hue? Basta una subroutine che stabilisce un *random number*, calcola i minuti e i secondi correnti e il gioco è fatto.

Inoltre, i servizi meteo online offrono meravigliosi output Xml da cui estrapolare dati. E allora con un CURL da Yahoo Weather, con le coordinate giuste del paesino in cui vivo, estrapolo non solo la temperatura esterna, da aggiungere a un file .csv che lo script compila ogni giorno con le temperature interne, esterne e lo stato del riscaldamento rilevato ogni ora, ma anche gli orari di alba e tramonto. Quindi, se siamo dopo il tramonto e non c’è nessuno in casa, il Mac comincia ad accendere e spegnere le luci delle varie stanze secondo uno schema completamente casuale.

Se siamo prima del tramonto, invece, si limita a spegnere, se le trova accese, le luci e la musica. Sempre che non ci sia nessuno in casa.

Insomma, nata quasi come un passatempo, la mia applicazione *HalHome* è diventata ormai un compagno di vita quotidiano per i membri della mia famiglia. Accendiamo o spegniamo luci e riscaldamento tramite la *webapp*, programmiamo la sveglia più delicata di questo mondo con la casa già calda, le luci e la musica che accolgono delicatamente il tuo ritorno dal mondo dei sogni. Quando ritorniamo a casa dopo essere stati fuori ci manda un iMessage di bentornato salutandoci e comunicandoci la temperatura attuale. Poi a seconda di chi sia il membro della famiglia arrivato a casa, attiva una *playlist* e la riproduce sugli *speaker* di casa. Se siamo dopo l’ora del tramonto e prima dell’alba, accende le luci del soggiorno. Se fa freddo, accende il riscaldamento. Se invece il riscaldamento è acceso e noi usciamo, si accorge che non ci siamo (dopo circa mezz’ora ma il tempo è personalizzabile) e spegne la caldaia. E con essa le luci e la musica, se sono accese.

Con HalHome, casa mia è diventata un intrepido esempio di domotica *homemade* grazie… ad AppleScript! Che è intuitivo, potente e… FUNZIONA! **Senza sorprese, crash o errori incomprensibili.**

Poi c’è ancora qualcuno che pensa siano importanti la frequenza di clock della Ram o la sigla del processore.

Le vendite di Mac sono in crescita continua e nettamente superiore a quella del mercato, da dieci anni a questa parte. La cosa più incredibile è la percentuale credo estremamente bassa di quanti acquisti siano dovuti ad AppleScript. Se si riuscisse a informare le persone del potenziale che hanno sotto le mani grazie a un Mac, il mondo sarebbe un posto migliore.