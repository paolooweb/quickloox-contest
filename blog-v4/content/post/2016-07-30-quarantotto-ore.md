---
title: "Quarantotto ore"
date: 2016-07-30
comments: true
tags: [cuoredimela]
---
Quarantasei, mentre scrivo. Il finale di [Cuore di Mela](http://cuoredimela.accomazzi.it/) è tutto da scrivere e per arrivare al traguardo ci vuole ancora un bello spunto finale, di passaparola e contribuzioni sulla [pagina Kickstarter](https://www.kickstarter.com/projects/295108589/cuore-di-mela-i-segreti-di-apple-iphone-e-mac). L’unica cosa garantita a oggi è la *suspence*.<!--more-->

Comunque vada a finire, da una esperienza come questa avrò imparato tutta una serie di cose, compreso il valore effettivo dei *social media* come cassa di risonanza di una notizia o di una iniziativa.

Quasi da scriverne un libro… ma è solo una battuta. Intanto sarebbe bello arrivare in porto con *Cuore di Mela*.

Farcela è possibile, a patto di mantenere la presa e il ritmo in questo fine settimana decisivo. Grazie a tutti quelli che hanno scritto e manifestato opinioni, perché rimarranno e contano qualunque sia l’esito.
