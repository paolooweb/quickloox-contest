---
title: "L’insostenibile leggerezza dei numeri a nove cifre"
date: 2022-11-16T10:24:14+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [BBEdit]
---
Ieri mi ha tenuto compagnia un documento di testo da centocinquanta megabyte abbondanti. [BBEdit](https://www.barebones.com/products/bbedit/) reagiva pressoché istantaneamente. Ho visto girare espressioni regolari da duecento caratteri (quelle ci mettevano un po’). Gli interventi manuali più critici a volte hanno visto un’esitazione di mezzo secondo per aggiornare la schermata (a tutto video, otto milioni di pixel, migliaia di caratteri visualizzati). Devo parlare di BBEdit meglio di come faccio di solito, perché non c’è un analogo per la somma pesata di affidabilità, longevità, semplicità, praticità, completezza, aggiornamento.

I problemi nascono quando esco e voglio (è un caposaldo) proseguire il lavoro su iPad, o perfino su iPhone se ci sono cinque minuti di attesa. Un succedaneo di BBEdit che faccia, se non le stesse cose, un sottoinsieme soddisfacente, ancora deve arrivare.

[emacs](https://www.emacswiki.org) fa molto più di BBEdit e qui la colpa è mia; devo ancora finire di scalare la parte necessaria della curva di apprendimento. Il Terminale è l’unica attività praticabile con profitto su iPad con una connessione remota. VNC e altri trucchi funzionano nel breve, non con centocinquanta milioni di caratteri che ballano sullo schermo. Questo a prescindere dalla questione di avere una app capace di aprirlo, quel testo; sto facendo molte prove e non è affatto scontato.

La app media su iPadOS e iOS è una ottima app da inserire in un flusso. Ma di *powerhouse*, di motori come BBEdit in grado di modulare a piena potenza aggeggi come le espressioni regolari o l’editing masiccio del testo, ancora non ne abbiamo, o sono acerbe.

iPad è ancora giovane e lo si capisce dal fatto che lavorare su un testo di centinaia di milioni di caratteri, ancora, non è banale. Su Mac lo è.