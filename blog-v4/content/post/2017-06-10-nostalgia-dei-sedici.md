---
title: "Nostalgia dei sedici"
date: 2017-06-10
comments: true
tags: [Backblaze, Windows, Panther, OSX]
---
Mentre iOS si prepara ad [accogliere unicamente le app a 64 bit](https://macintelligence.org/posts/2017-06-04-velocita-sessantaquattro/) e macOS lo seguirà a ruota, l’amministratore delegato di Backblaze, Brian Wilson, scrive una [lettera aperta a Microsoft](https://www.backblaze.com/blog/64-bit-os-vs-32-bit-os/) per ricordarle che 64 bit sono meglio di 32. Si può lavorare con più Ram, le opzioni di sicurezza sono più robuste, le architetture sono a prova di futuro.

Soprattutto, scrive Wilson, un ambiente a 64 permette di funzionare alle applicazioni ancora scritte a 32. E deplora la scelta di Microsoft di consentire ancora oggi l’installazione di versioni di Windows a 32 bit (persino Windows 10) incapaci di fare funzionare software a 64 bit.

La conseguenza di questo, per Wilson, è che Backblaze deve accollarsi lavoro supplementare per garantire ancora oggi la compatibilità a chi usa Windows a 32 bit tuttora (Backblaze è software di backup e lavora assai meglio nei 64 bit). Il tutto quando Apple ha iniziato il percorso verso i 64 bit per l’utente finale con Panther, Mac OS X 10.3, 2003.

Già così uno pensa a quanti Microsoft sia una macina da mulino per il progresso del software. Ma leggendo i commenti salta fuori qualcosa di pazzesco: Windows a 32 bit ė indispensabile per fare girare certo software da *sedici* bit. Scritto, per forza di cose, circa trent’anni fa, quando Macintosh prima edizione consentiva da subito a un programmatore di pensare a 32 bit.

Esatto. Microsoft ha creato – e fa del suo meglio per tenere in vita – un mondo dove ė concepibile che un software scritto trent’anni or sono possa rimanere senza uno straccio di aggiornamento e, naturalmente, sia assolutamente e completamente indispensabile a tutt’oggi.

Si dirà che la colpa è di chi usa quel software, o di chi lo ha scritto. Non proprio: è di chi consente qualsiasi tipo di condotta sciatta, lo giustifica, lo rende possibile, lo alimenta e lo sostiene.

La nostalgia dei sedici bit, anno 2017, è davvero malposta.
