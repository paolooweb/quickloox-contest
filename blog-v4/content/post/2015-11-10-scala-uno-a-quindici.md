---
title: "Scala uno a quindici"
date: 2015-11-10
comments: true
tags: [Lidia, Doodle, Buddy, Scorekeeper, Corriere]
---
Mia figlia è prossima a toccare i quindici mesi di sopportazione dei genitori e colgo l’occasione per accostarla a una lettura segnalata da **Matteo**: sul *Corriere della Sera*, [A due anni le app, a dieci YouTuber - la corsa digitale dei figli «mutanti»](http://www.corriere.it/italia-digitale/notizie/italia-digitale-speciale-due-anni-app-dieci-youtuber-corsa-digitale-figli-bambini-670fb93e-86c9-11e5-858b-c98d4f30b0b8.shtml).<!--more-->

Per essere un articolo pubblicato dal *Corriere* e per giunta *online* è insolitamente sobrio ed equilibrato, anche se non mancano le consuete scivolate apocalittiche e le frasi a effetto che dovrebbero richiamare i lettori come api al miele. Del resto gran parte del contenuto è riciclata da fonti estere.

Non ho però intenti polemici: intendo invece confrontare certe affermazioni dell’articolo con l’esperienza personale mia e di Lidia (e della mamma). Solo alcune informazioni ci interessano, dato che l’articolo imita i seggiolini per auto e dovrebbe coprire l’intero ciclo pediatrico, fino ai dodici anni. Qui di anni ne abbiamo uno virgola venticinque e niente più. Peraltro, di osservazione se ne è fatta un bel po’. Serva o meno, è una testimonianza di prima bimba. Le citazioni che seguono sono affermazioni contenute nell’articolo.

>Le ricerche sull’uso dei gadget digitali da parte di questo target possono lasciare sgomenti. Soprattutto se relative ai piccolissimi. Oltre un terzo dei bimbi sotto l’anno di età ha già utilizzato uno schermo touch, facendo scorrere pagine con un dito. Il 15% di loro ha usato un’app e il 12% un videogioco.

Non capisco perché rimanere *sgomenti* ma, come detto, l’intento è di confronto, non di polemica. Bisognerebbe intendersi sul termine *utilizzato*. Lidia ha un iPad di prima generazione nella cesta dei giochi. Su quello [si diverte a pasticciare](https://macintelligence.org/posts/2015-04-22-multimediale-innocenza/) con l’interfaccia di amministrazione dei giocatori in [Scorekeeper](https://itunes.apple.com/it/app/scorekeeper-xl/id463243024?mt=8) oppure a usare la funzione timbro di [Doodle Buddy](https://itunes.apple.com/us/app/doodle-buddy-paint-draw-scribble/id313232441?mt=8) per ricoprire lo schermo di animali, faccine, alberi e simboli vari. Tuttavia non ha la minima idea della funzione di Scorekeeper, né insiste per salvare i disegni fatti con Doodle Buddy. Come qualsiasi bambino che abbia visto un genitore usare uno schermo a tocco, ha voluto toccare lo stesso schermo e fatto scorrere pagine. Siccome vuole anche mangiare quello che mangiano i genitori e fare qualsiasi cosa facciano i genitori, sgomenta ben poco che abbia toccato schermi *touch*. La parola *utilizzato* però implica a mio avviso una consapevolezza degli intenti del software che non mi pare di vedere. Il termine è corretto, ma lascia intendere una complessità e una sofisticazione che Lidia ancora non applica.

>lo smartphone (o il tablet) trasformato in baby sitter digitale è un trend mondiale. Una volta che il piccolo ha messo le mani sul dispositivo farglielo mollare è complicato.

Lidia tratta l’iPad nella cesta dei giochi come un qualunque altro gioco: lo prende, lo usa per alcuni minuti e lo mette da parte per passare a un altro gioco. Lo stesso avviene con il *cordless*, il telecomando, gli iPhone. A volte vuole salire sulla sedia da lavoro di un genitore per interagire con la tastiera del Mac poggiato sulla scrivania (e si delizia dei movimenti del puntatore del mouse). Quando ha in mano un apparecchio, è complicato farlo mollare *subito*; se si soddisfa del suo uso, dopo alcuni minuti lo abbandona spontaneamente. Per i ricercatori americani c’è una questione chimica, un rilascio di dopamina che provocherebbe piacere nell’uso dell’apparecchio. Non ho l’autorità di contestarlo, né ho condotto uno studio scientifico sulla materia; qui constatiamo un attaccamento identico per iPad come per il piano giocattolo come per l’orsacchiotto di *peluche*.

>Il 73% dei genitori piazza il pupo davanti al tablet quando è alle prese con le faccende domestiche, il 65% per calmarlo, il 60% mentre fa commissioni e quasi un terzo per farlo addormentare.

Siamo nella minoranza in tutte le situazioni. iPad è uno dei giocattoli a disposizione e non ha alcuna funzione specifica.

>Su un punto tutti gli esperti sono concordi: mai abbandonare a lungo i figli davanti a un touchscreen. La sorveglianza deve essere sempre attiva e responsabile.

Tutto condiviso, fermo restando che Lidia non sta mai *a lungo* su iPad, cui dedica uguale tempo che a ogni altro gioco.

Immagino che le cose potranno cambiare, anche molto, con il passare del tempo. Quello che concludo per ora è che se si ascoltano adulti chiacchierare sul tema, inevitabile è che si rimarchi la presenza degli apparecchi digitali nella vita di tutti i giorni. Poi però sembra un’enormità che gli stessi apparecchi siano presenti nella vita dei bambini, come se vivessero in un universo parallelo. Quello che constato nella mia famiglia rispetto al tema, anche fortunatamente direi, è una tranquilla normalità. Che forse un po’ deriva anche dal considerare un iPad un oggetto normale, invece che un genietto diabolico capace di tenerti sulle spine a ogni passo.