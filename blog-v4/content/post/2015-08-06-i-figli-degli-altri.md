---
title: "I figli degli altri"
date: 2015-08-05
comments: true
tags: [Gand, Irc, Freesmug, Revoy, Pepper&Carrot, Humanoïdes, opensource, Krita, Blender, OSX]
---
**Gand** è il cortese padrone di casa che ha ospitato tante chiacchierate sul canale #freesmug di Irc. È anche l’animatore di [FreeSmug](http://www.freesmug.org), sito sul software *open source* che, in un Paese governato da persone con il cervello più grande di un mirtillo, come minimo gli garantirebbe la possibilità di poterlo fare a tempo pieno e, se superassimo la noce, di occuparsi di scuola a livello nazionale con ampi poteri. Ma è un altro tema. Volevo dire, **Gand** mi ha scritto, quello che si vede qui sotto, da **grassetto** a **grassetto**.<!--more-->

**Ti segnalo** la mia [ultima impresa](http://www.freesmug.org/peppercarrot). David Revoy è un illustratore francese che disegna una bellissima serie di fumetti:  [Pepper&Carrot](http://www.peppercarrot.com/it/static3/webcomics), tradotto in quasi 20 lingue.

È un fumetto *open source* di alta qualità che racconta le avventure di Pepper, una giovane strega, e del suo gatto Carrot, che vivono in un universo fantastico fatto di pozioni, magie e creature adatto a grandi e piccini.

Disegnato solo con software *open source*, con tutti i file sorgenti disponibili ed ogni episodio [finanziato dalle donazioni dei lettori](http://www.peppercarrot.com/it/static2/philosophy).

Ho pensato bene di dargli una mano traducendo tutti gli episodi in italiano (esclusi i primi due); non contento, ne ho creato gli ebook e i Pdf di ogni singolo episodio in sei lingue, distribuiti gratuitamente, e infine – non ancora contento – ho pubblicato su iBooks Store [tutti gli episodi in un unico volume](https://itunes.apple.com/it/book/pepper-carrot/id999482641?l=en&mt=11).

Su iBook Store i volumi saranno aggiornati fino al ventesimo episodio; i primi nove episodi ora costano 1,99 euro ed il prezzo aumenterà di un dollaro o di un euro a ogni nuovo episodio. Metà del ricavo andrà all'autore; per cui prima acquisti meno paghi, più aspetti più doni. :-)

Forse è ancora presto per la tua bimba ma… i figli **crescono in fretta**.

La mia bimba in effetti ha da mettere ancora qualche dentino prima di poter mordere la poesia e la dolcezza di Pepper&Carrot. Il fumetto è davvero dolce e poetico, di qualità indiscutibile e vera scuola francese (lo dice uno che ha trascorso una parte consistente di adolescenza in compagnia degli [Humanoïdes Associés](http://www.humano.com)). Consumato assieme a un bimbo o una bimba dell’età giusta, ha un grande potenziale.

Anche creativo, perché chi altri mette a disposizione tutti i sorgenti delle tavole? Revoy usa [Krita](https://krita.org/download/krita-desktop/), gratuito, *open source*, che su OS X deve ancora maturare un po’. Ma la situazione può solo migliorare. Anzi, sarebbe bellissimo se qualche genitore programmatore desse una mano sul codice.

Pepper&Carrot è *open source*, quindi scaricabile gratis, da web. Per questo non aggiungo schermate, basta visitare il sito per vedere tutto quello che si desidera e provare tutto quello che si crede. Ma allora perché disturbarsi a pagare un ePub su iBooks Store, o finanziare il lavoro di Revoy via [Patreon](https://www.patreon.com/davidrevoy) svenandosi per la cifra di un euro a episodio?

Perché mia figlia sì, è ancora troppo piccola. Ma ci sono anche i figli degli altri. E il lavoro di Revoy è di valore per la comunità.