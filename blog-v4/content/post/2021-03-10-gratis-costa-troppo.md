---
title: "Gratis costa troppo"
date: 2021-03-10T02:03:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [App Store, John Gruber, Play Store] 
---
Posso dire per una volta che John Gruber sia d’accordo con me, anche se non lo sa.

Nel [riprendere un suo post del 2008](https://daringfireball.net/linked/2021/03/09/df-archive-iphone-sdk) sulla novità che allora era App Store, commenta:

>non avevo previsto come le app “gratis” avrebbero azzoppato/limitato/distorto il mercato.

Non lo posso linkare ora (i commenti sono una faccenda più spinosa di quello che pensavo e il tempo è poco), ma lo avevo scritto e ribadisco: App Store è anni luce davanti a Play Store per qualità e reputazione e la strada da percorrere è quella del migliore servizio, anche se dovesse perdersi qualche dollaro per strada.

Possono esistere app gratis se sono gratis. Niente pubblicità, niente acquisti in-app, niente abbonamenti, niente. Gratis è gratis.

Fuori da quello che è gratis, qualunque app deve presentare una barriera iniziale di pagamento. 0,99 va benissimo. Da lì in poi può succedere (ragionevolmente) tutto. Quello che non può esistere è lo scaricamento gratis di cose che poi cercano di farsi pagare nei modi più disparati.

Dà una immagine falsata, offre una esperienza torbida, affatica. Si avvisi il navigatore in forma chiara e precisa che quella app prevede qualche tipo di transazione in denaro e niente lo fa più che un prezzo di ingresso.

Al tempo stesso, si avvisi in maniera altrettanto netta che una app è gratis, facendo sì che sia assolutamente vero.

Così, quando scarichiamo una app, sappiamo perfettamente da subito come andranno le cose e tutto sarà più onesto e affidabile.

Il gratis che c’è ora su App Store costa troppo.

*Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile.*