---
title: "Panta Linus"
date: 2022-08-03T01:49:21+01:00
draft: false
toc: false
comments: true
categories: [Hardware, Software]
tags: [Linus Torvalds, Torvalds, Linux, M2, Apple Silicon, M1, Arm, Air, MacBook Air, MacBook Air M1, arm64, Asahi Linux, Asahi]
---
Linus Torvalds, il creatore di Linux, [a novembre 2020](https://www.realworldtech.com/forum/?threadid=196533&curpostid=196570):

>È da molto tempo che aspetto un portatile Arm capace di eseguire Linux. Il nuovo Air [M1] sarebbe quasi perfetto, eccetto per il sistema operativo. E non ho tempo per smanettarci, o l’inclinazione verso la lotta contro le aziende che non vogliono aiutare.

Linus Torvalds, il creatore di Linux, [a luglio 2022](https://lwn.net/Articles/903033/):

>A titolo personale, la parte più interessante qui è che ho pubblicato la versione [5.19 di Asahi Linux] (e sto scrivendo questo) su un portatile arm64 [MacBook Air M2]. È qualcosa che aspettavo da un tempo mooolto lungo ed è finalmente realtà, grazie al team di Asahi. Abbiamo hardware arm64 con su Linux da molto tempo, ma niente è stato realmente utilizzabile come piattaforma di sviluppo fino a ora.

Linus non è un tipo condiscendente, né marchettaro, né diplomatico. Nel secondo link lo si legge spiegare che ha fatto sviluppo Linux su Mac tre volte: molti anni fa su PowerPC, più di dieci anni fa *quando MacBook Air era l’unico vero portatile sottile e leggero* e poi oggi. Non è neanche fanatico di Mac.

Apple cattiva, Mac chiusi e intanto le cose cambiano.