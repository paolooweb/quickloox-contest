---
title: "Quel gran pezzo dell’iMore"
date: 2015-09-05
comments: true
tags: [watch, iPhone, iPad, iMore, Mac]
---
Gran pezzo, quello [dedicato da iMore](http://imore.com/apple-watch-usage-survey-study-2015-q2) al proprio sondaggio sulle abitudini dei portatori di watch. Gran pezzo che mostra come fare buona informazione via rete e contiene di per sé un sacco di nozioni di interesse.<!--more-->

Attraverso [Survey Monkey](https://it.surveymonkey.com) hanno coinvolto ottomila persone e si può obiettare che si tratti di un campione non pesato e volontario, ergo potenzialmente più parziale di altri. Nondimeno, un campione di ottomila partecipanti ha peso sostanziale.

Il sondaggio esamina una serie interminabile di sfaccettature di uso di watch e permette di comprendere l’oggetto anche in assenza di un giro di prova in Apple Store. watch – in attesa della seconda versione del sistema operativo, che porterà *app* autonome rispetto a quelle su iPhone – punta a lasciare iPhone in tasca e semplificare ogni compito tipico. Il massimo della banalizzazione è dire che mira a farti risparmiare da mezz’ora a un’ora di tempo al giorno, che altrimenti se ne va a estrarre e riporre iPhone, cercare la app, svolgere il compito, sfuggire alle distrazioni delle notifiche che arrivano, pochi istanti che moltiplicati per più volte al giorno finiscono per contare. watch applica a iPhone il trattamento che iPhone applica a Mac.

Ci riesce molto bene. Poi c’è tutto il discorso della salute. Sorprende vedere un numero tanto alto di persone che dà retta agli inviti ad alzarsi per un minuto ogni ora, fino a che, qui sì, si prova l’esperienza. Per chi svolge lavoro sedentario è facile sottovalutare il bisogno di alzarsi e il meccanismo persuasivo di watch è studiato diabolicamente per colpire nel segno.

Invito alla consultazione dei numeri, che spiegano come tra qualche tempo ci troveremo di fronte a un fenomeno watch. C’è l’articolo di iMore, ma anche l’incrocio con le opinioni del mio campione personalissimo di persone brave, intelligenti, professionalmente brillanti e tuttavia negate per la comprensione dell’informatica. Le loro reazioni, tra le spallucce e il dileggio, sono quelle già viste con iPhone e iPad e questo significa il successo. Gran pezzo di tecnologia e di interfaccia, watch.