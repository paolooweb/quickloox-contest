---
title: "magie e burocrazie"
date: 2013-11-30
comments: true
tags: [Apple]
---
Quando [raccontavo a Valperga](https://macintelligence.org/posts/2013-11-09-il-futuro-e-nella-magia/) che la tecnologia di successo è quella che diventa magica e sparisce davanti agli occhi di chi la guarda non mi riferivo esattamente agli *exploit* di Levi Sparkx dentro gli Apple Store.<!--more-->

<iframe width="560" height="315" src="//www.youtube.com/embed/vji7Uf7M-to" frameborder="0" allowfullscreen></iframe>

Mi servirà comunque molta magia, a breve: giovedì 5 dicembre mi presenterò alle 15:30 al teatro Giacosa di Ivrea, in provincia di Torino, nell’ambito di [Ocova AlpMedNet Forum](http://www.ocova-alpmednet.netsons.org) *il punto di incontro tra il futuro della tecnologia e la qualità della vita*, a parlare di tecnologia e pubblica amministrazione.

Cercherò di spiegare come non conta tanto che cosa faccia la pubblica amministrazione con i computer e il software, quanto il mono in cui lo fa. E sarà durissima, perché le interfacce hanno lo scopo precipuo di semplificare la vita all’utilizzatore a prezzo di maggiore complicazione per il progettista. Riuscirò a spiegare alla pubblica amministrazione che deve semplificare la vita a noi e a sé? Una vera sfida. Ma sono motivato, avendo rinnovato la patente da poco.

Venerdì 6 sarà ancora più dura, perché mi presenterò ancora dinanzi a un pubblico… di studenti. Niente rischia più il patetico come un signore di mezza età che parla ad adolescenti di scuola e tecnologia. Tuttavia il rischio del ridicolo va corso, poiché la tendenza naturale dei più giovani è di trascurare quello che conta per il loro futuro. E se non capiscono subito certe tendenze fondamentali, che rimarranno in vigore per la parte migliore della loro vita, diventeranno patetici molto prima della mezza età. Se anche ce ne fosse uno disposto ad ascoltare, a capire, a salvarsi dal grigiore, sarei lì per lui.

Come sempre, se ci si incontra, si chiacchiera convivialmente molto volentieri.