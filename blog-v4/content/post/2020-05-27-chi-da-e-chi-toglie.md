---
title: "Chi dà e chi toglie"
date: 2020-05-27
comments: true
tags: [Apple, Venerandi, telnet, HyperCard, FileMaker, Texas, Instruments, TI-83, Python, Ruby, Swift, macOS]
---
Steve Jobs faceva del levare la sua ragione di vita del design industriale e Jonathan Ive, oltre che levare, voleva che i prodotti fossero sempre più essenziali e sempre più immateriali.

Anche un po’ per fortuna, perché se non avessero tolto il lettore di floppy da iMac, la porta modem dai PowerBook, le porte seriali, LocalTalk, il pulsante del programmatore per resettare la scheda logica dall’esterno, il lettore ottico, i connettori Scsi, Vga, MiniDisplay. FireWire eccetera eccetera, oggi avremmo sistemi, come dire, appena ridondanti.

Chiaro che sui tempi si possa eccepire, oppure che qualcuno sia affezionato a cose vecchie e non voglia passare a cose nuove. Si è fatta polemica sulla (perfettamente reversibile)[eliminazione di telnet dal corredo Unix di Mac](https://www.apogeonline.com/articoli/customer-experience-design-le-opportunita-perse-dalle-aziende-che-non-sono-centrate-sul-cliente-joe-heapy-oliver-king-james-samperi/), ci sono [nostalgici a oltranza di HyperCard](https://macintelligence.org/blog/2015/10/21/non-si-esce-vivi-dagli-anni-ottanta/), ogni volta che esce una nuova versione principale di [FileMaker](https://www.claris.com/filemaker/) qualcuno si lamenta di qualcosa, il prezzo, le funzioni, l’interfaccia, la compatibilità, tutto.

Apple si prepara inoltre a [rimuovere i linguaggi di scripting da macOS](https://tidbits.com/2019/06/25/apple-to-deprecate-scripting-languages-in-future-versions-of-macos/) e ne sentiremo delle belle, molto peggio di quando è uscito Catalina e le applicazioni a trentadue bit hanno smesso di funzionare, [con soli quattordici anni di preavviso](https://www.macintelligence.org/blog/2019/10/11/la-crociera-costa/). Chi li usa veramente li installerà, come fa da tempo dal momento che le versioni incluse in macOS sono sempre arretrate e a volte arretratissime. Gli altri si lamenteranno.

Il panorama è questo e Apple è cattiva. Ma non si è visto niente, in fatto di cattiveria, fino a pochi giorni fa: [Texas Instruments ha eliminato l’esecuzione di programmi assembly e C da alcuni modelli di calcolatrice programmabile](https://linustechtips.com/main/topic/1198899-texas-instruments-bans-all-asm-programs-games-on-ti-84-plus-ce-calculators/).

E non si risolve reinstallando. Il motivo: in una versione obsoleta del sistema operativo c’è un bug che consente di bypassare la modalità di funzionamento restrittiva da adottare per gli studenti che vogliono portare la calcolatrice all’esame.

Ci sono linguaggi alternativi a disposizione, ma sono mortalmente lenti o male implementati. Chi leggerà il pezzo troverà commenti letteralmente infuriati, della serie *anni di duro lavoro che se ne vanno in fumo*.

Si sistema tutto per chi vuole, eh. C’è chi si diletta tranquillamente con HyperCard ancora oggi, [grazie all’emulazione o al browser](https://osxdaily.com/2017/05/27/run-hypercard-macos-web-browser-emu/). In ogni caso, chi perderà (per modo di dire) Python o Ruby troverà Swift; chi ha perso l’assembly su TI-84 Plus CE, e contava sulle sue prestazioni, può solo masticare amaro.