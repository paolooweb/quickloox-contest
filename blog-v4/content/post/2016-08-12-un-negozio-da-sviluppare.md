---
title: "Un negozio da sviluppare"
date: 2016-08-12
comments: true
tags: [Devmate, Store]
---
**Federico** mi ha stuzzicato suggerendomi di dare un parere sull’[inchiesta condotta da Devmate](http://devmate.com/mac-dev-survey) presso 679 sviluppatori di software per Mac sul loro uso e la loro soddisfazione nei confronti di Mac App Store.

Premesso che Devmate è di parte, ossia si propone di fare concorrenza a Mac App Store per la distribuzione di software Mac, i risultati sono ampiamente condivisibili e largamente negativi: mancano molti strumenti indispensabili e vari aspetti del meccanismo, tra i quali la procedura di approvazione, sono tutt’altro che graditi. Si noti che il prelievo del 30 percento sul prezzo non è un motore fondamentale di insoddisfazione e neanche lo è il *sandboxing*, una gabbia funzionale che limita la possibilità di lavorare sotto il cofano di macOS; cose come la mancanza di versioni dimostrative e di aggiornamenti ad applicazioni già acquistate sono molto peggio nella valutazione di chi vive vendendo software o almeno vorrebbe.

Penso che Mac App Store potrà dirsi iniziativa riuscita tra una dozzina d’anni o anche di più. Siamo troppo abituati all’idea del computer come strumento modellabile a piacimento e occorre un ricambio generazionale per avere persone che pensino in modo diverso. Su iOS l’App Store chiuso e totalizzante funziona, o funziona molto di più; su Mac App Store il modello suscita notevoli diffidenze. Considerato che le persone dietro ai rispettivi schermi sono in larga parte le stesse, si capisce che qualcosa non va nella percezione di Mac come luogo atto all’uso di un Apple Store. L’idea non convince, comunque funzioni.

Ciò detto, non c’è dubbio che Mac App Store non abbia ancora ricevuto da Apole le attenzioni che dovrebbe. Nemmeno App Store di iOS le ha ricevute ancora tutte, intendiamoci. Nel novero delle cose fatte da Apple, immaginandosi tre categorie *fantastico*, *boh* e *schifezza*, gli store software cadono per forza nella seconda.

Devmate spera di convincere gli sviluppatori offrendo strumenti e possibilità che App Store su Mac non ha o pratica male. E questo fa comodo a tutti, dato che favorisce un miglioramento delle cose. Se devo trovare una lacuna nello studio, manca la consapevolezza che, rispetto a vendere in aperta Internet, farlo su Mac App Store è un lavoro a sé. Richiede strategie di prezzo è di promozione peculiari, snervanti, complicate e difficili da prevedere con successo. Tanti sviluppatori stanno fuori dallo Store perché non vogliono occuparsi di questa, che è una grana grossa.

E poi lo Store risente dell’esperienza su iOS, dove ci sono più sviluppatori che fiocchi di neve e, insieme alle eccellenze, si trovano incapaci e mezzi truffatori. Sviluppare per iPhone è mediamente molto più facile che per Mac. Apple dovrebbe tenerne conto. Imporre regole stringenti oltre certi limiti a cinesi senza storia e senza pudore è un conto; farlo con una [Bare Bones](http://barebones.com) o una [Panic](http://panic.com/), che sanno quello che fanno e hanno un rispetto sovrano per la piattaforma e il cliente, è tutto un altro.
