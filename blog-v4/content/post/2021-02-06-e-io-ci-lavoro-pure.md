---
title: "E io ci lavoro pure"
date: 2021-02-06T01:50:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [iPad Pro, iPad, Matt Birchler, BirchTree Mac, Mac mini] 
---
Come pensiero della notte, questa [considerazione di Matt Birchler](https://birchtree.me/blog/another-god-damned-ipad-post/):

>Il mio lavoro quotidiano è un misto tra designer e product owner e non sarei in grado di svolgerlo da un iPad. […] Inoltre mi occupo di un canale YouTube e, mentre posso fare sul mio iPad la maggior parte del lavoro necessario, mi ritrovo a gravitare verso il mio vecchio Mac mini. Potrei svolgere il lavoro da iPad, eppure scelgo di farlo su un Mac molto più lento.

>Tutto questo sembra condannare iPad, ma ecco che cosa succede: fuori da questi due ambiti, mi godo molto più iPad di Mac. Praticamente ogni altra cosa che faccio, che ammetto essere attività di tipo più leggero, accade su iPad. Scrivere questo post, leggere le notizie, sbrigare la posta, scrivere da freelance, modificare le foto in Lightroom, registrare e modificare audio, creare la mia newsletter, gestire le attività relative ai miei progetti YouTube, guardare video YouTube, parlare con gli amici, organizzare l’agenda e perfino rifinire il mio codice per questo sito: tutto avviene su iPad.

Ammetto che la mia suddivisione dei compiti sia più equilibrata: grosso modo sto metà del tempo su Mac e metà su iPad. L’essenza della constatazione di Birchler, tuttavia, si applica anche al mio fare. Su Mac avvengono le attività più impegnative, sicuramente, ma lo svolgere le attività più leggere è più piacevole su iPad.

E il lavoro che non posso tecnicamente svolgere su iPad, a differenza sua, sarà forse il cinque percento del totale.

*Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile.*