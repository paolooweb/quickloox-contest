---
title: "Il test della sincronizzazione"
date: 2023-04-26T01:56:38+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [Terminale, iCloud]
---
Se questo post appare con la data giusta, significa che mi sono collegato da remoto a Mac e ho trovato le cartelle iCloud sincronizzate.

Oppure che mi sono collegato da remoto ma ho trovato le cartelle iCloud non sincronizzate, così ho usato il comando `killall bird` per forzare iCloud ad aggiornare la situazione; e ha funzionato.

Se invece appare con una data diversa dall’attuale, è perché `killall bird` non ha dato esito e non ho fatto in tempo a trovare una buona alternativa.

Oppure che tutto ha funzionato ma, come a volte succede, il comando `rsync` non ha trasmesso il nuovo post al server che lo pubblica.