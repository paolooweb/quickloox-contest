---
title: "Suggerimenti per la riapertura"
date: 2021-04-14T00:09:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [iPad, St. Therese Catholic Primary School, Apple Distinguished School, Everyone Can Create, iMovie] 
---
Apple ha dedicato recentemente una pagina a una delle sue Distinguished School, la [scuola primaria cattolica di Santa Teresa](https://www.sttmascot.catholic.edu.au) a Sydney, che [utilizza massicciamente iPad](https://www.apple.com/newsroom/2021/03/australian-primary-school-drives-innovation-and-creativity-with-ipad/) per attuare i progetti di studio della serie [Everyone Can Create](https://www.apple.com/education/k12/everyone-can-create/).

Chiaro che il vino dell’oste sia per forza il migliore e che non valga la pena di cercare spunti intriganti o cronaca imparziale in un redazionale che è di autopromozione. Nondimeno, qualche frase qua e là meriterebbe un pizzico di attenzione sopra la media.

>Con una comunità di studenti di cinquanta culture diverse, dei quali cui tre quarti parlano inglese come seconda lingua, St. Therese è ricorsa a iPad per aiutarli a sbocciare contro ogni previsione.

Da noi si ama molto raccontare che l’insegnamento digitale acuisca le disuguaglianze e il disagio sociale.

>Quando ogni scuola della nazione ha dovuto improvvisamente passare all’apprendimento remoto, lo sforzo che avevamo compiuto per incorporare iPad e il curriculum Everyone Can Create in ogni aspetto dell’insegnamento ha voluto dire che ci trovavamo con una base solida, dalla quale i nostri studenti potevano continuare a imparare senza interruzione.

A proposito del digitale considerato uscita di emergenza se proprio non se ne può fare a meno e altrimenti relegato in laboratori o considerato una materia, invece che una base.

>Durante il lockdown, gli studenti hanno usato la app [Seesaw](https://web.seesaw.me) per creare raccolte di lavori digitali e condividerle con i docenti. Si sono rivelate così gradite a studenti, docenti e famiglie che ora sono diventate una pratica abituale.

Il digitale può benissimo integrarsi con l’insegnamento classico e nessuno ha mai detto che lo debba sostituire. A parte [i manifestanti che vogliono una scuola buona per il Novecento](https://macintelligence.org/posts/Analogici-con-il-digitale-degli-altri.html), nel 2021.

E ora tre capoversi impegnativi. Nell’originale il titolo del paragrafo è *Agents of Equity*. Il punto non è che iPad sia più bello degli altri, ma che il digitale possa essere usato con successo per ridurre i divari.

>Durante il lockdown abbiamo potuto osservare studenti che non avevano spiccato il volo durante i giorni di scuola tradizionale, sperimentare un livello elevato di successo. Offrire grande flessibilità sui tempi di apprendimento è stato un sistema così efficace di eliminare i divari, particolarmente per gli studenti che avevano fatto fatica a ingranare dall’inizio, che gli insegnanti continueranno a registrare lezioni usando iMovie e ad approfondire l’insegnamento mediante attività virtuali su iPad inserite nelle proprie lezioni.

>“Questa iniziativa di apprendimento misto è un buon esempio di come usiamo la tecnologia per sfidare norme datate come il modello della classe-fabbrica, dove i docenti parlano a file di ragazzi dalle nove del mattino alle tre del pomeriggio all’interno di un edificio” dice la preside.
 
>“Ogni bambino è diverso, quindi perché ogni lezione dovrebbe essere uguale per tutti?” afferma. “iPad ci permette di personalizzare dove e quanto avviene l’apprendimento, mescolando lezioni virtuali e in presenza che danno modo a ciascuno studente di decidere il modo migliore per dimostrare i propri progressi”.

Non è un racconto di fantascienza, ma quanto succede in una scuola vera, lontana dalla solita America, in un ambito che, seppure privato, è *low-fee*. St. Therese è una scuola per famiglie normali.

Certamente il lavoro di inserimento di iPad nell’insegnamento è andato avanti per quattro anni. In Australia sono lenti, non come da noi dove gira la circolare del ministro e il giorno dopo sono tutti pronti per le lezioni remote.

Ecco. In questi giorni di dibattito sulla questione delle riaperture, un modesto contributo per evidenziare che, qualunque cosa succeda alle aule, nella scuola ci sono troppe teste che continuano a restare chiuse. Il lockdown non c’entra.

*Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile.*