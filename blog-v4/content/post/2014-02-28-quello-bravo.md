---
title: "Quello bravo"
date: 2014-02-28
comments: true
tags: [Rai, Silverlight]
---
Una battuta storica nella politica italiana vuole che, al momento di decidere l’organico della dirigenza Rai, la consuetudine imponga di assumere uno della maggioranza, uno dell’opposizione e uno bravo.<!--more-->

La Rai ha storicamente trasmesso i propri contenuti via Internet facendo passare tutto dal *plugin* Silverlight di Microsoft. Ufficialmente per la tutela dei contenuti antipirateria; di fatto una balla sesquipedale, perché non appena si sono diffusi gli iPad – dove Silverlight non può girare – le *app* Rai hanno subito iniziato a diffondere lo stesso segnale video, completamente libero.

Si poteva verificare la cosa anche solo con un Mac, provando a caricare la trasmissione con lo *user agent* di iPad (abilitare il menu Sviluppo di Safari dalle Preferenze, sezione Avanzate, caricare la pagina e poi usare il menu Sviluppo > User Agent). In un caso appariva la richiesta di Silverlight, nell’altro il programma pronto da vedere.

Adesso la Rai non usa più Silverlight. Finalmente il XXI secolo, dirà qualcuno.

Usa Flash. Se possibile persino peggio per problemi di sicurezza, consumo batterie (sui portatili), stabilità e prestazioni.

La tecnica dello *user agent* ovviamente continua a funzionare. Nell’immagine qui sotto, quella mostrata due volte è la stessa pagina Rai, con *user agent* Mac e *user agent* iPad.

Continuiamo ad aspettare quello bravo.

 ![Raisport su Mac e come se fosse su iPad](/images/raisport.png  "Raisport su Mac e come se fosse su iPad") 