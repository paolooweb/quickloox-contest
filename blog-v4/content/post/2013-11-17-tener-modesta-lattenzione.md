---
title: "Tener modesta l’attenzione"
date: 2013-11-16
comments: true
tags: [Bloomberg, iPhone]
---
Premesso che il lavoro lo ha raccolto [The Loop](http://www.loopinsight.com/2013/11/14/a-person-familiar-with-apples-plans/) ed è stato svolto da [John Gruber](http://daringfireball.net), ecco le conclusioni. Il primo è [Matthew Lynn, 14 gennaio 2007](http://www.bloomberg.com/apps/news?pid=newsarchive&sid=aRelVKWbMAv0):

>iPhone è solo un’esca di lusso che parlerà a pochi maniaci di gadget. In termini di impatto sul mercato, è meno rilevante.<!--more-->

[Peter Burrows e Gregory Bensinger, 11 febbraio 2011](http://www.bloomberg.com/news/2011-02-10/apple-said-to-work-on-cheaper-more-versatile-iphone-models.html):

>Apple sta lavorando a nuove versioni di iPhone […] Una sarebbe un terzo più piccola di iPhone 4 e senza pulsante Home […] Apple ha pensato di vendere il nuovo iPhone per 200 dollari […] Sarebbe capace di funzionare sia su rete Gsm che Cdma […] Apple sta lavorando a una tecnologia chiamata Universal Sim […] Il software consentirebbe al possessore di scegliere tra Gsm e Cdma da solo.

Una che è una, in due non l’hanno presa. [Arging Chang, 17 luglio 2013](http://www.bloomberg.com/news/2013-07-17/apple-may-delay-introduction-of-iphone-5s-commercial-times-says.html):

>Apple potrebbe ritardare la presentazione di iPhone 5S oltre settembre o ottobre dopo che il progetto è stato cambiato per accogliere uno schermo Retina da 4,3 pollici.

Queste tre uscite ridicole hanno in comune l’essere pubblicate su *Bloomberg*. Si sarà capito che, in tema di previsioni su iPhone, Bloomberg vale meno della carta igienica.

Tim Culpan e Adam Satariano [hanno scritto](http://www.bloomberg.com/news/2013-11-10/apple-said-developing-curved-iphone-screens-enhanced-sensors.html) una settimana fa per Bloomberg che Apple presenterà l’anno prossimo iPhone con schermi ricurvi da 4,7 e 5,5 pollici. Visti i precedenti, chi può credergli?

Gente che parla di una notizia decisamente *controccorrente* con tre C, per esempio, e [spappagalla Matthew Lynn](http://www.macitynet.it/liphone_destinato_a_morire-_parola_di_bloomberg-com/).

Convinta che [ci sarà un iPhone low-cost](http://www.macitynet.it/anche-bloomberg-apple-far-un-iphone-low-cost/) perché c’è scritto su Bloomberg, *una sorta di ufficio stampa non formale di Apple*.

Decisa a ritrasmettere del [ritardo di iPhone 5S per via dello schermo da 4,3 pollici](http://www.macitynet.it/iphone-5s-arrivera-in-ritardo-a-fine-anno-per-introdurre-uno-schermo-da-43-pollici/), perché Bloomberg riporta una sparata di *Commercial Times*, pubblicazione cinese che *vanta un curriculum di anticipazioni in pari numero indovinate e mancate*. Come una moneta lanciata in aria vanta un pari numero di teste e croci.

Naturalmente, ansiosa di raccontare come [Apple stia lavorando a uno schermo curvo](http://www.macitynet.it/apple-lavora-ad-un-iphone-con-display-curvo/) perché lo dice Bloomberg, *una rispettata fonte che in passato ha dimostrato di avere agganci ai piani alti di Cupertino*.

Quanta modestia.