---
title: "C’era una volta l’infografica"
date: 2014-12-08
comments: true
tags: [animagraff]
---
Passerà poco tempo perché si affermi un’altra tendenza, gli *animagraff*.<!--more-->

Il problema sta nel nome che è assai poco orecchiabile. Ma se c’è abbastanza passaparola, da domani tutti i grafici web inizieranno a, ehm, ispirarsi a [come funzionano gli altoparlanti](http://animagraffs.com/loudspeaker/).
