---
title: "È venuto il tempo"
date: 2016-12-25
comments: true
tags: [Natale, Viticci, iPad, watch, Mac, Pokémon, Go]
---
Quest’anno arrivarci è stato faticoso e però si sente che il tempo è venuto.

Metterò un pizzico di attenzione in più nel blog, che ultimamente ha sofferto. Le scadenze tuttavia sono state micidiali e questa, per quanto *labour of love*, a volte deve dare la precedenza.

Se possibile darò una lucidatina a [Cuore di Mela](https://itun.es/it/UjAPfb.l) (per i disperatissimi del regalo a tempo scaduto: è in offerta natalizia a 9,99 euro fino a fine anno).

Non ho puntato l’interesse su giochi particolari ma vorrò trovarne uno. Accetto consigli.

Starò lontanissimo dalle news: gente che [annuncia l’abbandono di Pokémon Go per watch](https://9to5mac.com/2016/12/17/pokemon-go-for-apple-watch-cancelled/) pochi giorni prima che il programma [appaia su App Store](https://9to5mac.com/2016/12/22/pokemon-go-apple-watch-now-available/), va solo dimenticata. Potrebbe essere un proposito per tutto l’anno: se succede qualcosa di veramente interessante, sicuramente non lo verrò a sapere da quei siti.

Prima di pronunciare la parola *professionale* curerò di avere un *workflow* automatizzato e rifinito, che elimina ogni procedura ripetitiva a mano e ogni perdita di tempo. Altrimenti studierò in silenzio. Lettura (ri)consigliata, [l’annata con iPad Pro](https://www.macstories.net/stories/one-year-of-ipad-pro/) di Federico Viticci, che manda avanti un business redditizio su web, consiglia una quantità di hardware e software che neanche ci si immagina (seriamente) e dichiara orgogliosamente [quello che faccio oggi su iPad Pro non si può fare su Mac](https://www.macstories.net/stories/one-year-of-ipad-pro/11/#content). (Mi piacerebbe smentirlo, però servono tempo e capacità)

Penserò alla famiglia e agli amici. Cose di interesse puramente personale.

Porgerò auguri speciali a chi passa di qui, per qualunque motivo. La mia gratitudine è superiore a qualsiasi tentativo di descriverla, mai scontata o di maniera.

Buon Natale, che sia unico e speciale.
