---
title: "Scontro di civiltà"
date: 2018-08-31
comments: true
tags: [Apple, Titan, Nissan, Leaf, Dmv]
---
Fa notizia il [coinvolgimento di un’auto senza pilota collaudata da Apple in un incidente](https://www.macrumors.com/2018/08/31/apple-test-vehicle-august-24-accident/) sulle strade della California.

Apple lavora come sempre in segretezza, ma per la legge californiana deve inserire i dati relativi all’incidente in un registro pubblico, dal quale si evincono i particolari.

>Il 24 agosto alle 14:58, un veicolo Apple in modalità autonoma è stato tamponato mentre si accingeva a immettersi sulla Lawrence Expressway South da Kifer Road. Il veicolo di prova Apple procedeva a meno di un miglio orario in attesa di uno spazio libero sicuro per completare l’immissione, quando una Leaf Nissan del 2016 è entrata in contatto con il veicolo di prova a circa quindici miglia orarie. Ambedue i veicoli hanno assorbito l’impatto e non si riportano danni alle persone.

Ecco, tradotto: il veicolo Apple era in coda per entrare in superstrada ed è stato toccato da dietro. Nessun danno.

Se sembra una notizia, i nostri telegiornali dovrebbero durare cinque ore.

La civiltà dell’informazione è un grande progresso, a patto di tollerare le sue esagerazioni.