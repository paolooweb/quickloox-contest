---
title: "Guardia di porta"
date: 2021-01-30T00:43:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [sandbox, Apple, BlastDoor, iMessage, Project Zero, Google] 
---
Gli esperti di sicurezza del Project Zero che fa capo a Google [hanno scoperto che iMessage gode di un nuovo e ingegnoso meccanismo di sicurezza](https://googleprojectzero.blogspot.com/2021/01/a-look-at-imessage-in-ios-14.html), detto BlastDoor.

Ingegnoso ingegneristicamente e lo si vede avventurandosi nella lettura del post di Project Zero, che definire per tecnici è eufemistico. Fortunatamente la spiegazione per le persone normali è molto semplice: ora, quando arriva un messaggio via iMessage, il suo contenuto viene analizzato in una *sandbox* prima di venire messo a disposizione del destinatario.

Una *sandbox* (la vasca di sabbia per i bambini ai giardinetti) è un’area di memoria pressoché priva di contatti con il resto del sistema. Se per cattiva sorte fosse arrivato un attacco informatico camuffato da allegato, per esempio, il suo tentativo di prendere il controllo del Mac o di rubare si scontrerebbe contro l’impossibilità di uscire dalla sandbox.

La questione è tutt’altro che banale; in passato si sono verificati attacchi all’integrità dei Mac proprio via iMessage. Il sistema utilizzato suscita grande approvazione da parte degli esperti, perché non si limita a sistemare un bug e magari lasciarne aperto un altro; è un miglioramento infrastrutturale che toglie di mezzo una categoria intera di pericoli potenziali per i nostri dati.

Questa è la conclusione dell’analisi di Project Zero che, ricordo, è la concorrenza:

>Complessivamente, questi cambiamenti sono probabilmente molto vicini al massimo che poteva essere fatto nel rispetto dei vincoli di retrocompatibilità, e dovrebbero avere un impatto significativo sulla sicurezza di iMessage e dell’intera piattaforma. È una gran cosa vedere Apple allocare risorse per questo genere di rivisitazioni del codice allo scopo di migliorare la sicurezza per gli utenti finali.

Da iOS 14 e macOS 11 (Big Sur) in avanti, iMessage è non solo ragionevolmente cifrato, ma anche sicuro. Per chi collega la [citazione](https://it.wikipedia.org/wiki/Dana_Barrett), Mastro di chiavi è finalmente stato raggiunto da Guardia di porta.

*Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile.*