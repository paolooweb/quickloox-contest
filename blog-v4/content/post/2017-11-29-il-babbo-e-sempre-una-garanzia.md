---
title: "Il babbo è sempre una garanzia"
date: 2017-11-29
comments: true
tags: [BBEdit, Markdown]
---
Troppi viaggi, troppi impegni, troppo poco tempo, hardware troppo vecchio, ma per fortuna c’è babbo Natale che, con leggero anticipo, mi ha portato [BBEdit 12](http://www.barebones.com/products/bbedit/).<!--more-->

Nemmeno trentadue euro e briciole per aggiornare al migliore editor di testo possibile, con un [elenco delle aggiunte e modifiche](http://www.barebones.com/support/bbedit/notes-12.0.html) che parla da solo. Mi limito a sottolineare la possibilità di operare sulle colonne di testo, estrazione di testo individuato dalle ricerche e [cmark](https://github.com/commonmark/cmark) per passare da [Markdown](https://daringfireball.net/projects/markdown/) a Html.

L’installatore ha notato che uso Yojimbo e mi ha chiesto se desiderassi l’inserimento automatico nel programma del numero di serie di BBEdit. Comodità totale.

Unica nota negativa, la scomparsa di Consolas come font monospaziato di bordo. D’altro canto, è un [font Microsoft](https://www.microsoft.com/typography/fonts/family.aspx?FID=300). Toglierlo è una buona norma di igiene mentale. Sono passato a Menlo, tra l’altro scrivendo in corpo dodici anziché tredici. Risparmio pure spazio.

Qualcosa funziona. In più guardo il mio nome nei titoli di coda e faccio la ruota.

 ![Presente nei titoli di coda di BBEdit 12](/images/bbedit.png  "Presente nei titoli di coda di BBEdit 12") 