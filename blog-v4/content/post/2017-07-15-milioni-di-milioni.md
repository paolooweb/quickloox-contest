---
title: "Milioni di milioni"
date: 2017-07-15
comments: true
tags: [iPhone, Snell, Macworld]
---
Lettura raccomandata quella di [Jason Snell su MacWorld](http://www.macworld.com/article/3207552/iphone-ipad/apples-risky-balancing-act-with-the-next-iphone.html) perché porta una ventata di informazione nel delirante panorama delle illazioni sul prossimo iPhone. Segnatamente sull’ipotesi che Apple possa proporre un modello molto evoluto e molto costoso da vendere in quantità relativamente piccole dove invece i grandi numeri sarebbero riservati agli ordinari, averne, iPhone 7S e 7S Plus.

All’idea di pezzo prezzo più alto ci sono commentatori che si scatenano subito e mancano completamente di cogliere il succo della questione, ottimamente spiegato da Snell. Appke può permettersi di inserire in iPhone tecnologia inseribile in duecento milioni di unità annue, più o meno.

Viene fuori che nessuno dei concorrenti di Apple si avvicina neanche a queste cifre: la vera tecnologia da capogiro di iPhone è la sua catena di montaggio. Mai un prodotto si era venduto in tanti numeri tanto in fretta.

Ne segue che, se arriva un progresso tecnologico per ora impossibile da trasmettere a duecento milioni di unità, Apple si trova all’angolo, tra il doverla trascurare o adottarla solo su una frazione della produzione.

Ciascuna opzione ha pro e contro. Comunque vada a finire, andrà ricordato che il prezzo di iPhone non è una rapina; è dettato da mille fattori tra i quali anche la tecnologia che vi si trova dentro oltre che, nel caso di una macchina venduta in quantità limitate, la domanda.

E non è obbligatorio cedere al canto delle sirene: in casa c’è chi usa iPhone 7 con profitto e a ragione; non sono io, che ho scelto di stare con un iPhone 5 fino a che dura. Perché è quello che mi serve.
