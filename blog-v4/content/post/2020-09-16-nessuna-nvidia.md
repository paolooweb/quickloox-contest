---
title: "Nessuna Nvidia"
date: 2020-09-16
comments: true
tags: [Arm, Nvidia, Softbank, Apple, Fullo, Thelothian]
---
L’acquisto di Arm da parte di Nvidia ha fatto comprensibilmente molto rumore (una acquisizione da quaranta miliardi non è banale) e anche [inquietato un sacco di soggetti a livello industriale e politico](https://uk.reuters.com/article/us-arm-holdings-m-a-nvidia-industry-anal/nvidias-arm-deal-sparks-quick-backlash-in-chip-industry-idUKKBN2650GT).

Altrettanto comprensibilmente, molti hanno iniziato a tirare in ballo Apple nello scenario. Dopotutto impiega da sempre processori Arm per iPhone e iPad e adesso passa su Arm anche Mac, giusto?

Giusto.

E allora magari Apple [entra aggressivamente nel mercato gaming](https://www.facebook.com/Thelothian/posts/10223894739064620), oppure [decide di acquisire Nvidia](https://www.facebook.com/francesco.fullone/posts/10157511831485779), o altro. Dopotutto questa acquisizione potrebbe provocare problemi ad Apple, giusto?

Sbagliato.

Per dovere civico, elenco alcune informazioni che – sempre comprensibilmente – non sono note a tutti e per essere possedute richiedono di avere un minimo di interesse oltre la superficie delle *Apple news*.

Apple ha fondato Arm insieme ad Acorn (perfino Newton, anni novanta, aveva a bordo un processore Arm) e, quando si è ritirata dalla *joint venture*, [si è tenuta una licenza architetturale perpetua](https://news.ycombinator.com/item?id=10190521). Sostanzialmente, Apple può usare le istruzioni del set Arm come desidera, e lo fa. **Apple progetta i propri chip Arm**.

Fino a ieri Arm era di Softbank, adesso è di Nvidia. Per Apple la cosa cambia in questo modo: non comprava alcunché da Softbank e ora neanche da Nvidia.

Apple persegue maniacalmente l’obiettivo di controllare l’intera catena di produzione delle proprie macchine e si disinteressa nel modo più assoluto di fornire tecnologia o licenze ad altri costruttori. **Apple non ha alcun interesse a diventare fornitore di tecnologia Arm**. I suoi chip sono superiori e li tiene per le proprie macchine. Gli altri fanno quello che possono.

**Apple ha un modello di business basato su margini elevati**. Non avrà problemi a proporre Mac a prezzo più basso, se passare ad Arm abbassa i costi (e lo sviluppo?). Ma li abbasserà solo fino a che il suo margine rimane negli obiettivi (un Mac fa guadagnare mediamente ad Apple un 30-35 percento del prezzo di vendita). In nessun caso i Mac diventeranno concorrenti dei PC venduti a un soldo la dozzina negli ipermercati.

Chiudo: Apple non ragiona per settori. Non conosce il gaming, non conosce macchine da ufficio, non distingue *laptop* da *notebook* da *portable* da *convertible* da *workstation* eccetera. Apple non ragiona per gergo. Non produce smartphone o tablet e difatti nel suo sito è impossibile trovare tutti questi termini. Mac, iPad, iPhone, ma anche watch o tv, sono macchine trasversali, in grado di soddisfare una gamma piuttosto ampia di utilizzatori grazie alla diversificazione delle specifiche. Ma Apple, per lo meno l’attuale Apple, non produce macchine per giocare, per la ricerca di laboratorio, per lo studio. Valorizza le doti di iPad per la scuola, certo. Però iPad non è stato creato per la scuola. E avanti così.

Riassumo: Apple non viene toccata dall’acquisizione di Arm da parte di Nvidia, se non da scenari lontani nel tempo (tipo: Nvidia investe miliardi per fabbricare chip più veloci di quelli di Apple) e a oggi assolutamente astratti. Non ha alcun interesse a possedere Arm (e a gravarsi della fornitura di chip a terzi) ed è libera da qualsiasi condizionamento possa essere applicato domani all’architettura.

Si traggano le conclusioni logiche dalle premesse elencate.
