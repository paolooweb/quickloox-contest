---
title: "La valle dei tempi"
date: 2015-01-16
comments: true
tags: [MonumentValley, AppleDesign, iOS, Google, Amazon, Fire, ForgottenShores]
---
[Monument Valley](https://itunes.apple.com/it/app/monument-valley/id728293409?l=en&mt=8) ha vinto un premio Apple Design per il 2014 e soprattutto, per la cifra rovinafamiglie di 3,99 euro, offre una esperienza di gioco mai vista prima; come dicono gli autori, ogni livello deve funzionare – si perdoni il *calembour* – su tre piani che sono il gioco, l’architettura e la forma grafica.<!--more-->

Prima di tirare fuori i quattro euro – che potrebbero addirittura diventare sei se si cede alla sirena dell’espansione *in-app* Forgotten Shores – si può visitare il [sito ufficiale](http://monumentvalleygame.com) del gioco e guardarsi un tris di filmati interessanti.

Chi è che permette a questa creatività di manifestarsi? I finanziatori naturalmente, cioè il pubblico. Dal [blog di sviluppo](http://blog.monumentvalleygame.com/blog/2015/1/15/monument-valley-in-numbers) sappiamo che Monument Valley ha incassato finora quasi sei milioni di dollari. Un investimento riuscito, perché il gioco è costato 852 mila dollari nella versione base per 55 settimane di lavoro, più altri 549 mila dollari e 29 settimane di fatiche per l’espansione Forgotten Shores.

Su due milioni e mezzo di copie vendute, 1,7 milioni sono iOS; mezzo milione, Amazon Fire; Google Play trecentomila.

Se *Monument Valley* avesse dovuto contare su Android, neanche avrebbe recuperato i costi di produzione.

Se c’è progresso nel software per aggeggi da tasca, è perché c’è iOS. Altri parassitano. Sono tempi in cui per avere qualità bisogna avere il coraggio di restare in alto.

 ![Monument Valley](/images/monument-valley.jpg  "Monument Valley") 