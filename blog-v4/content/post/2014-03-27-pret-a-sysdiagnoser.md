---
title: "Prêt-a-sysdiagnoser"
date: 2014-03-28
comments: true
tags: [sysdiagnose, Mac, OSX]
---
Pensavo di conoscere tutte le scorciatoie di tastiera significative di Mac, fino a che ho letto [Mac OS X Hints](http://hints.macworld.com/article.php?story=2014032113470243).<!--more-->

Premere dal Finder i tasti *Comando-Opzione-Control-Maiuscole-punto* e attendere da qualche istante a qualche minuto. Se tutto va bene, compare una finestra equivalente alla directory */var/tmp* e, dentro, una cartella *sysdiagnose*.

All’interno, la diagnostica di Mac più incredibilmente completa e astrusa che si possa sognare. O temere.