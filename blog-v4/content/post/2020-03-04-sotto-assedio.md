---
title: "Sotto assedio"
date: 2020-03-04
comments: true
tags: [Lotr, Gandalf, Frodo, Tolkien]
---
Vacanza di un giorno dai temi consueti perché ho scoperto un blog con una serie di post dedicati alla [logistica dell’assedio di Gondor](https://acoup.blog/2019/05/10/collections-the-siege-of-gondor/) (questo è il primo post) nel [Ritorno del Re](https://it.wikipedia.org/wiki/Il_Signore_degli_Anelli_-_Il_ritorno_del_re) secondo le versioni del libro e quelle del film di Peter Jackson.

TL;DR: Tolkien ha lavorato bene e Jackson, considerati i vincoli di tempo che aveva, ha fatto ragionevolmente il possibile e va benvoluto anche se perde in verosimiglianza.

Più che altro, per purissima coincidenza stamattina ero a guardare alcune scene de [La Compagnia dell’Anello](https://it.wikipedia.org/wiki/Il_Signore_degli_Anelli_-_La_Compagnia_dell%27Anello). Ho sentito Gandalf indicare a Frodo la locanda del *Cavallino impennato* e ho pensato con malinconia ai nostri guai italiani con [una brutta nuova traduzione](https://macintelligence.org/posts/2019-11-04-parole-in-liberta/) del testo di Tolkien quando nel mondo in lingua originale possono prendersi il lusso di fare le pulci alla logistica degli eserciti nella Terra di Mezzo.

Mi sono anche reso conto, a proposito di *Ritorno del Re*, che in questo momento in libreria o online è impossibile trovare una sua versione recente e ufficiale, per quanto scarsa sia; e questa assenza durerà molto tempo, perché deve ancora uscire *Le due torri*, libro nella stessa identica condizione.

Si trovano qua e là online edizioni vecchie e invito chiunque ne sia sprovvisto di comprarne una, almeno una, prima che finiscano, e dare istruzioni agli eredi in modo che arrivino plausibilmente conservate al momento in cui scadrà il contratto con l’attuale traduttore (e chi lo maneggia a mo’ di clava come strumento di lotta politica fuori tempo massimo sarà molto più che in pensione).

Finirà secondo che qualcuno vorrà mettere a punto una versione digitalizzata della traduzione classica, darle dignità tipografica come si merita e poi diffonderla sottotraccia come *il Signore degli Anelli quello vero*.

Sarebbe un’operazione completamente illegale che naturalmente invito a non compiere, ma anche una nemesi memorabile nei confronti di chi gioca ancora a tifare per la guerriglia nella giungla.

Che c’entra Mac? Beh, non succederà; il pensiero di una edizione elettronica innovativa della trilogia tolkieniana tuttavia, fedele all’originale ma capace di aggiungere una nuova dimensione alla fruizione, fa scorrere adrenalina.