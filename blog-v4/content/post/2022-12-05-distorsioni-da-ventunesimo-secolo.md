---
title: "Distorsioni da Ventunesimo secolo"
date: 2022-12-05T00:10:41+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [Warp, Terminale]
---
Di software capace di scuotere le certezze e proporre nuovi modi di utilizzo, veramente nuovi, ne contiamo quante sono le eclissi di sole totali. Ed è una delle cose di cui più lamentarsi in campo tecnologico: lo hardware, con i processori e gli schermi che ci sono, consente praticamente qualunque cosa. Eppure continuano a vedersi schemi già consolidati che si ripetono, senza creatività, senza innovazione.

Mi è successo diverse volte con il Terminale. Ho visto passare più di un sostituto potenziale. Alla fine però si trattava di un Terminale come quello di partenza, magari un po’ colorato, con qualche funzione marginale in più, niente di cui occuparsi seriamente a meno di essere un professionista di Unix. Non ancora.

Oggi ho provato [Warp](https://www.warp.dev) e improvvisamente mi sono trovato nel Ventunesimo secolo, anche se usavo tecnologia del millenovecentosessanta.

Devo dire che ho appena scalfito la superficie del programma, che fa un sacco di cose per specialisti. Intanto però ho una finestra di tutorial accanto a quella dell’esecuzione e il *prompt*, che sta su una directory collegata a [git](https://git-scm.com)… me lo segnala. Senza che io abbia fatto niente.

La velocità di esecuzione è superiore. La home page dice che Warp è pienamente nativo, sviluppato in linguaggio [Rust](https://www.rust-lang.org) e scevro da framework orridi come [Electron](https://www.electronjs.org), o da accrocchi che trasformano in app un insieme di pagine web.

Secondo i programmatori, dietro la riga di comando sta un editor di testo completo, adatto anche per sviluppatori. E, mentre Warp usato a livello individuale è completamente gratuito, pagando si possono condividere con un team le sessioni di Unix, di editing, di programmazione.

Insomma, questo non è il solito Terminale-più-qualcosa-di-carino. Proprio un’altra cosa. Anche se è compatibile all’indietro con bash, zsh e fish *out of the box*. Ho iniziato a usarlo su piccole cose che mi servono di routine, da dilettante del Terminale, e Warp non ha battuto ciglio anche se è costruito per tutt’altro.

Consiglio il test a tutti, anche a chi non ha intenzione di considerare un’alternativa al Terminale. Il test consiste effettivamente nel prendere un paradigma vecchio come mia nonna e ritrovarsi in un ambiente da terzo millennio. È uno shock culturale interessante.