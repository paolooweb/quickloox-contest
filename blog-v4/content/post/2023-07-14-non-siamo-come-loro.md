---
title: "Non siamo come loro"
date: 2023-07-14T01:05:35+01:00
draft: false
toc: false
comments: true
categories: [Internet]
tags: [Hoftstadter, Douglas Hofstadter, GPT-4, Gödel Escher Bach, Atlantic]
---
Douglas Hofstadter [mi ha dato buca](https://macintelligence.org/posts/2023-07-12-le-sirene-dei-media/) perché non è più interessato alla materia e preferisce godersi gli anni che ha ancora a disposizione, spero numerosi. Tuttavia, capisco bene che se lo tirano in mezzo come descrive in [Gödel, Escher, Bach e AI](https://archive.is/2VtiC) su *The Atlantic*, possa desiderare di esprimersi pubblicamente fuori dalle scelte di vita.

Succede che un lettore di [Gödel, Escher, Bach](https://www.adelphi.it/libro/9788845905933) dia in pasto a GPT-4 il libro e chieda al software di impersonare Hoftstadter e scrivere in sua vece i motivi per i quali ha deciso di scrivere GEB.

Dopo di che, con grande correttezza, invia a Hofstadter (quello vero) il risultato e gli chiede se lo può pubblicare.

La risposta non è stata esattamente positiva. Queste sono le differenze che Hofstadter ha scelto di evidenziare, rispetto a quello che realmente avrebbe scritto e in effetti ha scritto, come prefazione all’edizione del ventesimo anniversario. C’è dunque una prova verificabile, stampata in migliaia di copie.

>Il pezzo non suona come se lo avessi scritto io (né ai tempi del libro, né oggi); piuttosto dà l’idea di un Hofstadter fittizio intento a scrivere frasi generiche che echeggiano quelle del libro e così posso apparire almeno un pochino come se fossero centrate.

>L’uso di genericità sfocate al posto di storie ed episodi concreti non è assolutamente il mio stile; il linguaggio che vuole volare alto di GPT-4 ha poco o niente in comune con il mio stile di linguaggio e di pensiero (che spesso descrivo come “horsies-and-doggies”). Inoltre il pezzo contiene zero umorismo (là dove l’umorismo pervade i miei scritti) e compare solo la più fuggevole allusione ai venti dialoghi dentro GEB, probabilmente la ragione principale del gradimento del libro per così tanti anni. Eccetto per la frase “personaggi immaginari”, Achille e la Tartaruga non vengono mai nominati da GPT-$ (che mi starebbe impersonando) né si trova un riferimento al provocatorio dialogo di Lewis Carroll, che è stato la fonte di ispirazione per quei “personaggi immaginari”.

>Totalmente trascurato è il fatto che i miei dialoghi abbiano strutture che imitano quelle musicali (fughe e canoni) e che la loro forma spesso rispecchi nascostamente il loro contenuto, cosa che ho scelto di fare per fare sorridere i lettori quando capiscono che cosa sta succedendo. […] Della costante giocosità verbale che dà ai dialoghi di GEB il loro carattere particolare non si parla da nessuna parte.

>Infine, ma non da ultimo, chiunque abbia letto GEB sarà rimasto colpito dall’uso pervasivo di analogie vivide che comunicano il senso di idee astratte; ma questa caratteristica centrale del libro non viene menzionata. In breve, il pezzo che GPT-4 ha composto usando la prima persona singolare ha zero autenticità, nessuna vicinanza al mio modo di esprimermi e l’artificiosità della sua creazione va contro tutti i fondamenti del sistema di valori che seguo da tutta la vita.

Mi è capitato di leggere conoscenti, amici, anche persone intelligenti e di cultura, vantarsi di *avere parlato con Dante* o Marx, Nietzsche, Giulio Cesare o chi per essi, grazie alla possibilità di buttare un libro specifico in pasto a un modello linguistico. Ora si capisce meglio perché sia sempre gente morta: un vivo avrebbe qualcosa da dire e non sarebbe piacevole. Più che altro, ora è chiaro che tipo di informazioni rispetto a un libro possano essere prodotte da un modello linguistico e quale sia la qualità, rispetto al libro originale.

Se fosse solo questo, tramite Internet possiamo assimilare stupidità infinita tutti i giorni e si tratterebbe solo di un esempio in più. Ma non è solo questo. Lascio ancora la parola a Hofstadter:

>Cadere nell’illusione che vasti sistemi computazionali privi di qualsiasi esperienza nel mondo reale fuori dal testo siano autorevoli e perfettamente affidabili sulle cose del mondo è un errore profondo e, se l’errore viene replicato sufficientemente spesso e diventa generalmente accettato, minerà alla base la vera natura della verità sulla quale la nostra società – e intendo l’intera società umana – si basa.

Mi piacerebbe tradurlo tutto l’articolo, perché c’è molto più di questo. A me ha passato questa informazione fondamentale: mentre nessuno contesta minimamente l’utilità dei modelli linguistici per una tonnellata di attività computazionali, ripetitive, propedeutiche a un lavoro successivo (lo faccio, è molto utile e lo raccomando), quando si passa alla scrittura, alla letteratura, alla libera espressione – e vale anche per Midjourney, per i sistemi che compongono video o musica – quando qualcuno magnifica le prestazioni del chatbot di turno i casi sono due: o deve vendere qualcosa o difetta di cultura e conoscenza della materia.

Purtroppo è una verità chiara e grave, che è ora di dire anche a qualche interessato. Il prodotto, chiamiamolo, artistico dei chatbot è povero e di basso livello. Chi lo porta in palmo di mano, pure.