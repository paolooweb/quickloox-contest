---
title: "Cento di questi iPad"
date: 2016-02-11
comments: true
tags: [Teresa, Virginia, iPad]
---
La mamma di **Piergiovanni** ha compiuto gli anni di recente. Non si dice l’età di una signora; come indizio, siamo appena oltre un multiplo di quarantuno.

 ![alt](/images/teresa.jpg  "title") 

Le auguro di cambiare un sacco di iPad, per (loro) anzianità di servizio.<!--more-->

Come ulteriore augurio, nel mio [Tutti pazzi per iPad](http://www.tecnichenuove.com/tutti-pazzi-per-ipad.html) del 2010 concludevo parlando di nonna Virginia. Sono passati cinque anni ma riscriverei le stesse righe uguali uguali. Sono qui sotto.

##Oh, and one more thing&#8230;

Uso l&#8217;inciso (*oh, e un&#8217;ultima cosa&#8230;*) che ha reso famoso Steve Jobs, l&#8217;amministratore delegato di Apple, quando lo ha usato per presentare prodotti o novit&#224; inaspettate, in modo fintamente distratto e noncurante al termine di un evento, per chiudere simbolicamente questa toccata e fuga dedicata a iPad.<br />
Proprio di iPad c&#8217;&#232; chi racconta come sia l&#8217;inizio di una rivoluzione; per altri si tratta di un apparecchio inutile e costoso e, per altri ancora, sarebbe anche qualcosa di interessante se solo Apple vi avesse inserito la propria tecnologia preferita.<br />
La verit&#224; finale &#232; che conta solo ci&#242; che ci cambia la vita, anche in piccolo, anche marginalmente.<br />
Invito alla visione del filmato [http://xrl.us/bhjk69](http://xrl.us/bhjk69), dedicato ai cento anni di nonna Virginia.<br />
Debole di vista e sofferente di glaucoma, nonna Virginia faceva fatica a leggere e soprattutto a scrivere. Le hanno regalato un iPad il cui schermo luminoso e la tastiera virtuale hanno cambiato la sua vita.<br />
Nel giro di pochi giorni nonna Virginia aveva letto due libri e soprattutto aveva ripreso a comporre *limerick*, composizioni tradizionali in rima che sono sempre state la sua grande passione.<br />
Se iPad cambier&#224; in meglio la vita delle persone che lo usano, sar&#224; un successo indipendentemente dalle vendite o dalle rivoluzioni.
Trovo appropriato chiudere queste pagine con un *limerick* di nonna Virginia, dedicato a s&#233; e al suo iPad. Con l&#8217;augurio per tutti noi di trovare tante cose che cambiano la vita.

*To this technical-ninny it&#8217;s clear*<br />
*In my compromised 100th year,*<br />
*That to read and to write*<br />
*Are again within sight*<br />
*Of this Apple iPad pioneer.*

*Questa tecnononna secolare*<br />
*faticava a scrivere e guardare;*<br />
*ora ha un punto fermo,*<br />
*del suo iPad lo schermo,*<br />
*che la aiuta a leggere e creare.*