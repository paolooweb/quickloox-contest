---
title: "Prese in giro"
date: 2015-03-06
comments: true
tags: [iPhone6, Flickr, Instagram]
---
Bell’[articolo di BuzzFeed](http://www.buzzfeed.com/brendanklinkenberg/apple-found-its-newest-billboards-on-the-internet) sulla campagna [Shot on iPhone 6](http://www.apple.com/worldgallery), che mostra foto da tutto il mondo scattate con l’ultimo modelli di iPhone.<!--more-->

Bello per le foto. E per il vero concetto sottostante. L’articolista si fa un’idea sbagliata: pensa che Apple voglia mandare il messaggio *difficile credere che foto così buone possano arrivare dall’apparecchio che abbiamo in tasca*.

Il vero messaggio è un altro. Le foto non sono state commissionate a professionisti, ma sono state *reperite su Internet*.

Ogni volta che si alza un lamento su Apple che abbandona i professionisti, salta fuori uno scatto mozzafiato da Flickr o Instagram.