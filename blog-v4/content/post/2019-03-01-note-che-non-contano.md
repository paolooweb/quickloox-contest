---
title: "Cose che (non) contano"
date: 2019-03-01
comments: true
tags: [watch, Schiller, Jobs, Cook, Information, Must]
---
Pare che [si stia vendendo un watch ogni secondo](http://www.applemust.com/apple-is-selling-an-apple-watch-every-second-data-shows/), in accordo alle stime – da prendere con pinze per il lavoro in fonderia – di Strategy Analytics.

watch coprirebbe la maggioranza assoluta del mercato, lasciando il secondo – Samsung – al tredici percento, poco più di un quarto rispetto al cinquantuno.

Cose che contano pochissimo o non contano. Sarebbe facile linkare gli scettici di qualche anno fa e metterli a confronto con una stima da tre milioni di unità vendute al mese. Poco interessante; è successo con iPod, con iPhone, iPad, è successo sempre in questo secolo. Quelli della prima ora brontolano e piano piano vengono lasciati alla loro incompetenza.

Pare che Phil Schiller, il responsabile marketing, possa permettersi di bocciare un’idea o un progetto dicendo o scrivendo semplicemente *NFW*, *no fucking way*.

Sembra una nozione di nessun conto messa in un [articolo sulla mappa del potere interno ad Apple](https://www.theinformation.com/articles/the-people-with-power-at-apple?) giusto per aggiungere pepe a una minestra insipida.

Tuttavia la giudico significativa perché, sulle orme di Steve Jobs, la cosa più importante per le prossime fortune di Apple e di chi ne adotta i prodotti è che si sappia dire *no* a tutte le idee e le proposte non indispensabili. Che poi avvenga in modi poco urbani, posto che sia vero, è pura nota di costume.