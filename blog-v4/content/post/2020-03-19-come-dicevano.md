---
title: "Come dicevano?"
date: 2020-03-19
comments: true
tags: [Gruber, Mac, mini, iPad, Pro, LiDar, Attivissimo, MacBook, Air]
---
John Gruber [è molto soddisfatto](https://daringfireball.net/2020/03/super_wednesday) degli aggiornamenti di [iPad Pro](https://www.apple.com/it/ipad-pro/), MacBook Air e Mac mini e, modestamente, anch’io.

Come dicevano? Questa cosa della segretezza non si porta più, oramai si sa tutto prima. Infatti, tutti erano stati avvertiti della presentazione di oggi, sapevano che dentro iPad Pro c’è uno scanner [LiDar](https://www.neonscience.org/lidar-basics) (no, non serve per guidare la macchina con iPad; velocizza e rende più precise le applicazioni di realtà aumentata), sapevano che il cursore su iPad è una cosa completamente diversa dal cursore su Mac.

Come dicevano? Apple non innova più e non fa niente di nuovo. iPad, nel suo tentativo di diventare anche un portatile a mezzo servizio, copia da Surface, guarda per esempio la tastiera.

Già, guarda la nuova Magic Keyboard, che sembra tirata fuori da un film di fantascienza. Vedremo quella nuova di Surface, quando arriverà, come sarà fatta e chi ci sarà arrivato per primo.

Come dicevano? Con quel dorso arrotondato [rischia di dondolare](https://macintelligence.org/posts/2019-01-26-lo-spessore-di-un-anniversario/), diceva Paolo Attivissimo di iPad dopo averne visto la presentazione in streaming.

Se adesso guarda il [video promozionale](https://www.youtube.com/watch?v=09_QxCcBEyU), con quella mano che porta via l’apparecchio come se fosse semplicemente appoggiato al sostegno, gli viene uno stranguglione.

<iframe width="560" height="315" src="https://www.youtube.com/embed/09_QxCcBEyU" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>