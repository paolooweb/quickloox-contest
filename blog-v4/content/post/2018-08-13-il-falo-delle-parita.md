---
title: "Il falò delle parità"
date: 2018-08-13
comments: true
tags: [ Dell, Xps15, MacBook, Pro, Geekbench, Cinebench, iPhone]
---
MacBook Pro di punta e Xps15 di Dell condividono lo stesso processore Core i9, quindi offriranno prestazioni equivalenti, giusto?

Giusto, finché sono alimentati. Se lavorano con la batteria, MacBook Pro  mantiene prestazioni costanti mentre invece Xps15 [crolla](https://appleinsider.com/articles/18/08/09/comparing-the-core-i9-and-graphical-performance-of-the-2018-15-inch-macbook-pro-with-dells-xps-15) e vince il premio *portatile che funziona quando non lo è*.

Nel frattempo, iPhone X *dell’anno scorso* [batte nei test di velocità](https://www.macrumors.com/2018/03/01/iphone-x-galaxy-s9-benchmarks/) S9 e S9+ Samsung *di quest’anno*.

Quando ti dicono che tutto è uguale a tutto, alzano fumo.
