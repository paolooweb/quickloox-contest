---
title: "Facendo si impara"
date: 2019-02-14
comments: true
tags: [Swift, Playgrounds, Shortcuts]
---
Un altro giro per curiosità attorno ai Comandi rapidi e a [Swift Playgrounds](https://macintelligence.org/posts/2019-02-03-swift-and-hungry/). La sostanza è niente: qui ho sistemato [un comando per avere anno-mese-giorno ore-minuti](https://macintelligence.org/posts/2019-02-11-detto-fatto/), una sciocchezza; lì, con il pretesto di pilotare un mostriciattolo a caccia di gemme, ho definito una funzione elementare. Intendo, da scuola elementare per quanto è semplice.

Il vero punto è che tornare, qui e lì, è *piacevole*. Un sacco di altri strumenti diretti alla programmazione o allo scripting, invece, mi provocavano frustrazione oppure noia oppure ambedue.

Del tutto indipendentemente dagli obiettivi che ciascuno potrebbe avere in tema, suggerisco di fare un giro, anzi due, qui e lì, Comandi rapidi e Swift Playgrounds. C’è potenziale perché diventino interessanti anche nostro malgrado.
