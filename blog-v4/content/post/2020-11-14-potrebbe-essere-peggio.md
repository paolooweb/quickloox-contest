---
title: "Potrebbe essere peggio"
date: 2020-11-14
comments: true
tags: [Intel, Apple, Silicon, Arm, iPhone, Mac, Lundqvist, M1, Gruber, Wilder, Feldman, Frankenstein, Junior, MacBook, Pro, Air]
---
Gli specialisti della dissonanza cognitiva hanno acceso i motori a tutta forza. La situazione è grave: bisogna a tutti i costi trovare una giustificazione al fatto che un Mac possa avere un processore migliore del loro Pc e trovare sempre a tutti i costi nei nuovi Mac dei difetti capaci di consolidare la loro convinzione fideistica di avere fatto la scelta giusta.

Solo che stavolta è dura. Tecnicamente, i *benchmark* che girano non sono ufficiali e però, se sono veri, lasciano davvero poco spazio alle chiacchiere. Un benchmark non è mai una rappresentazione fedele delle prestazioni nell’utilizzo reale. Ma uno preferisce averli dalla sua parte, no? Pare che M1 lasci nella polvere tutti i MacBook Pro attuali in funzionamento *single core* e praticamente [tutti i processori per portatile](https://twitter.com/LeahLundqvist/status/1326689614204182536) oggi sul mercato. Un MacBook Air M1 può fare meglio di un MacBook Pro 16” Intel.

<blockquote class="twitter-tweet"><p lang="en" dir="ltr">The M1 in the MacBook Air outperforms ANY other cpu out on the market right now and scores higher in multicore than the top spec mbp 16” 🤯 <a href="https://t.co/OLCifdJVHi">https://t.co/OLCifdJVHi</a><br><br>Leaderboards for reference: <a href="https://t.co/sfedgFCvzu">pic.twitter.com/sfedgFCvzu</a></p>&mdash; 👩🏼‍💻 Leah Lundqvist (@LeahLundqvist) <a href="https://twitter.com/LeahLundqvist/status/1326689614204182536?ref_src=twsrc%5Etfw">November 12, 2020</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

Il tutto, commenta in un tweet successivo Leah Lundqvist, *con raffreddamento passivo*. Un computer senza ventola batte quelli che ce l’hanno e, intanto, la batteria dura il doppio.

Un portatile PC che va più veloce, fruga fruga, si trova. Un portatile PC con la stessa autonomia può saltare fuori. Il problema insormontabile è che devono essere *due portatili diversi*, perché M1 è tecnologia che gli altri non hanno, né avranno a breve. M1 riesce ad andare più veloce e consumare meno, contemporaneamente. Non ce n’è per nessuno. [John Gruber](https://daringfireball.net/2020/11/one_more_thing_the_m1_macs):

>Se si tirano le somme, da oltre un decennio iPhone e iPad hanno avuto chip progettati da Apple che la concorrenza non riusciva e tuttora non riesce a eguagliare. Adesso vale anche per i Mac.

È grigia, molto più grigia del solito, mai stata così grigia. È solo l’inizio. Il vero dramma è la modalità di lavoro di Apple. Già con M1 si sparge il panico e M1 è *il primo* dei processori Arm che verranno prodotti.

Qualcuno si ricorderà del primo iPhone. Era la [cenerentola delle comparative](https://macintelligence.org/blog/2017/05/09/questione-di-metodo-ii/), cattivo come telefono, inferiore come software, eccessivo nel prezzo, scarso nella fotocamera. Di anno in anno, ha continuato a migliorare e gli iPhone di oggi sono mostri di rifinitura e sofisticazione, paragonati all’antenato. Se Apple ha preparato bene la transizione, quello che arriverà farà sembrare M1 il *trailer*, più che il film.

I dissonanti cognitivi sono nella situazione di Gene Wilder e Marty Feldman che [scavano di notte](https://www.youtube.com/watch?v=olKKWG3HwOg) dentro il cimitero di *Frankenstein Junior*.

*Potrebbe essere peggio*.

<iframe width="560" height="315" src="https://www.youtube.com/embed/olKKWG3HwOg" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>