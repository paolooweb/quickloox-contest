---
title: "La morte fuori dal cuore"
date: 2022-03-25T01:00:30+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [Russia, Ucraina, Financial Times]
---
Ci si abitua a dire *con la morte nel cuore* ma, nel caso degli eventi correnti, non ha alcun senso; la morte vera è là fuori e investe materialmente migliaia e migliaia di innocenti, di colpevoli, di ignari, tutti.

Si vorrebbe parlare di una eccezionale *cover story* sul sito del Financial Times, che [riassume l’andamento del conflitto e ne spiega le dinamiche](https://ig.ft.com/russias-war-in-ukraine-mapped/). L’avremmo tutti gradita maggiormente se fosse stato possibile dedicarla ad altri argomenti.

Vincendo per un momento i sentimenti, si tratta di un documento prezioso perché riesce a informare senza sensazionalismi e partigianerie, senza nascondere e senza esibire.

Sarà probabilmente parte di materiale storico che verrà studiato per interrogazioni sulla guerra che segna il declino dell’efficacia del carro armato, sul tramonto di una grande potenza, sulle strategie e tattiche che hanno segnato vittorie e sconfitte, sulle figure eroiche e su quelle vili, sui leoni da tastiera (luridi e laidi oltre ogni immaginazione).

Al momento vorremmo solo che finisse prima possibile perché tocca parlare di grande giornalismo quando il dolore sovrasta la capacità tecnica e la professionalità.

Che serva per prendere coscienza, per fare tacere almeno uno dei cervelli lavati su Facebook, per spingere all’azione chi ha davvero la possibilità di fare cambiare le cose per il meglio, oltre le mille piccole e grandi azioni di contorno che ognuno può compiere, importantissime per costruire una rete di supporto – l’accoglienza su tutte – e che però non hanno il potere di pacificare oppure di fermare la sofferenza di chi può solo subire.

Grazie al *Financial Times* e speriamo di poter parlare presto di un suo prossimo servizio ipermediale senza dover parlare di guerra.