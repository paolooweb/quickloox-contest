---
title: "I limiti dello sviluppo"
date: 2021-12-17T00:07:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [Swift Playgrounds, iPad] 
---
Tanti sforzi, tanta fatica, tanta arte retorica per dimostrare che iPad non è un computer, dal momento che non ci si può programmare una app per iPad, e che cosa fanno in Apple con scarsissimo senso dell’opportunità?

[Aggiornano Swift Playgrounds alla versione 4](https://twitter.com/c10um0/status/1471511215839260674).

<blockquote class="twitter-tweet"><p lang="en" dir="ltr">Swift Playgrounds 4 is an ideal IDE for iOS app development.<br>* Fast, fast, fast<br>* Responsive SwiftUI previews<br>* Swift Package-based app project<br>* Built-in document browser window<br>* Built-in SF Symbols browser <a href="https://t.co/60huqzXdsi">pic.twitter.com/60huqzXdsi</a></p>&mdash; Kenta Kubo (@c10um0) <a href="https://twitter.com/c10um0/status/1471511215839260674?ref_src=twsrc%5Etfw">December 16, 2021</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

Un altro appassionante dibattito che ci lascia. Un’altra possibilità in meno di definire iPad un apparecchio da consumo di informazioni. Come si permettono di offendere così quelli che ne sanno, tutto per accontentare quattro gatti di sviluppatori?

*Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile.*