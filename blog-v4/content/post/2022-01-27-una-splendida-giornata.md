---
title: "Una splendida giornata"
date: 2022-01-27T00:52:11+01:00
draft: false
toc: false
comments: true
categories:
tags:
---
In cui ho scoperto che l’ospedale in cui volevo eseguire un esame di routine per il rinnovo della patente ti fa compilare un modulo di prenotazione e poi ti avvisa *per telefono* la data effettiva. Nel mio caso, mai perché *abbiamo gran parte del 2022 già coperta*. Piccolo dettaglio: per dirmelo ci hanno messo due mesi.

In cui mi sono interfacciato con un funzionario di una banca che non risponde al telefono e guarda la posta elettronica una volta al giorno, ma non la guarda tutta: quando termina l’orario di ufficio, smette.

In cui la rappresentante di classe della secondogenita alla materna, che invia raffiche di messaggi WhatsApp per qualsiasi minuzia, ha detto di non avere trovato tempo in settantadue ore per comunicare una notizia importante che richiedeva, alla fine dei conti, una riga.

La società ha problemi di comunicazione e lo si era peraltro capito già da tempo. La domanda è: colpa degli strumenti che non riescono a costruire un ambiente ideale per comunicare o degli umani cercopitechi che riescono a fare strame di qualunque piattaforma, non importa quanto bene intenzionata?

Ripenso alla lezione del primo Macintosh, che per primo ha portato alla massa l’idea dell’interfaccia grafica. La massa era abituata a un’interfaccia a caratteri e ha fatto spallucce, inizialmente. Ma l’idea era giusta e lentamente tutti l’hanno adottata. Dopo tanti anni, l’interfaccia grafica a finestre, menu, input semplificato (mouse, trackpad, trackball) è dominante e nessuno pensa minimamente di tornare indietro. Si vedono, effettivamente, design che gridano vendetta al cospetto di [Doug Engelbart](https://dougengelbart.org), tuttavia i pilastri fondamentali reggono.

Per questo (non solo; questa è la ragione più evidente), ritengo che la risposta giusta sia la prima; strumenti ben fatti hanno il potenziale per migliorare percepibilmente la comunicazione tra le persone. L’unico problema è la mancanza dell’equivalente di Macintosh, qualcosa che mostri la strada in modo tanto netto e inequivocabile che, nonostante i bastoni tra le ruote messi dagli sciocchi, la strada diventi quella principale.

È ancora presto, a giudicare dall’esperienza quotidiana. Eppure alcuni embrioni interessanti si iniziano a vedere. Punto molto sugli sviluppi dell’idea generale di [Internet Relay Chat](https://www.irchelp.org), da [Slack](https://slack.com) in giù (quello che c’è è mediamente sotto Slack e non di poco) e su [Matrix](https://matrix.org), uno standard aperto per la comunicazione sicura e decentralizzata.

Domani arriverà qualcosa talmente conveniente e geniale che l’ospedale sarà indotto a usarlo per farmi sapere in tempo reale se c’è un posto oppure no, il bancario troverà più comodo rispondere con un messaggio in pochi secondi invece che spendere minuti dietro alle email, i gruppi di comunicazione verranno maneggiati in modo ben più efficace rispetto alla non-gestione di WhatsApp.

Sarà una splendida giornata e magari avremo pure trovato il successore di Steve Jobs per capacità di ribaltare il mondo secondo la sua idea.

_Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile._
