---
title: "Intolleranza zero"
date: 2016-08-21
comments: true
tags: [Windows, Gartner, 9to5Mac, Apple, iPhone, Android, BlackBerry]
---
È chiaro a qualunque persona di buon senso che, quando tutti sono liberi ma uno vuole togliere la libertà a tutti, i tutti devono fermare l’uno e impedirgli di nuocere. Non è limitazione dell’uno, ma tutela della libertà di tutti.

Per questo sono buone notizie i dati che arrivano da Gartner e [9to5Mac commenta](https://9to5mac.com/2016/08/18/android-ios-smartphone-market-share/) sulla diffusione dei computer da tasca durante il trimestre primaverile.

iOS e Android combinati farebbero il 99,1 percento delle vendite e questo significa duopolio, ma anche esistenza sempre di almeno una alternativa.

Si registra un declino di iOS ma poco importa. Nei numeri gioca una parte anche l’attesa del prossimo iPhone e comunque Apple non ragiona per quote di mercato, ma per utile sulle vendite; ergo, ci saranno iPhone di qualità comunque, vendano di più o vendano di meno.

Android è preponderante più che mai, tuttavia – vedi sopra – c’è un’alternativa. Nessuno è costretto.

Windows però passa da un due virgola cinque a uno zero virgola sei. Si tratta sempre di oltre un milione di pezzi, i quali però nello schema globale delle cose sono una cifra a rischio estinzione. Notizia da brindisi: mentre non basteranno due generazioni a recuperare i danni fatti da Windows sui computer tradizionali, per quelli da tasca non sussiste il problema. Quelli che vogliono togliere agli altri la libertà contano zero. Virgola sei, ma zero.

Un discorso a parte merita BlackBerry, che allo zero virgola uno è prossimo alla scomparsa. Dispiace. Solo che insistono a coltivare una visione del mondo indipendente dalla realtà. Di recente l’amministratore delegato si è per esempio [detto disturbato](http://bgr.com/2016/07/20/blackberry-ceo-apple-iphone-encryption/) dalla politica di Apple di cifratura dei dati privati sugli iPhone.

Detto per guadagnare qualche vendita, è un po’ poco. Per ostilità alla cifratura è clamoroso, dopo che per anni l’azienda ha passato guai con i governi illiberali di tutto il mondo proprio grazie alla loro posta elettronica blindata. Se il prossimo trimestre finiranno per (s)comparire nella categoria Altri, sarà loro insipienza e non monopolio assassino, come invece è accaduto a tanti in ambito desktop.

Ma divago. La cosa importante è che l’intolleranza scende verso lo zero, almeno nella tecnologia.
