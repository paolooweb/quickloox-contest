---
title: "La carica dei duecentomila"
date: 2019-04-04
comments: true
tags: [NYT, Times, Apple, News+]
---
Il *New York Times* dedica un articolo ad Apple News+ che in una settimana [ha raccolto più di duecentomila abbonati](2019-04-04 00:50) e la faccenda è curiosa, poiché il quotidiano, nonostante una corte serrata, è rimasto fuori dal servizio, un po’ come quando al ballo di corte manca la principessa.

Il giornalismo americano è (chilometri) avanti al nostro nel riconoscimento dei fatti anche quando, per così dire, non tengono in conto il parere dell’editore. Neanche è abitato da mammole e l’articolo si chiude con una battutaccia che sarebbe circolata durante un *party* di festeggiamento tra Apple e gli editori: *ma è una festa o una veglia funebre?*

(Le veglie americane sono composte ma raramente sobrie.)

[Continuo a pensare](https://macintelligence.org/posts/2019-03-21-notizie-da-poco/) che magari il modello non sia originalissimo, ma offra agli editori più possibilità rispetto a prima. E che questi primi duecentomila siano pionieri di un viaggio verso un’editoria plausibile nell’epoca digitale. Forse anche il *Times* se ne rende conto, a denti stretti; in pochissimi hanno altrettanta capacità di spesa e, per dire, sul versante europeo si ritroveranno mortificati da una direttiva copyright ispirata dalle lobby sbagliate.

Apple News+ riuscirà a dare nuova carica al settore? Aspettiamo. Intanto è un servizio curato da umani senza tracciamento degli utenti da parte degli editori e a pensarci di questi tempi viene quasi voglia di avviare la raccolta fondi per favorirlo oltre i dieci dollari al mese.
