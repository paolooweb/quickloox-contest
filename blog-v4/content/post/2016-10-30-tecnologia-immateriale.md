---
title: "Tecnologia immateriale"
date: 2016-10-30
comments: true
tags: [Jida, MacBook, Pro, iPad, Siri, watch]
---
Sono stato sollecitato dall’amico [Jida](https://www.facebook.com/andrea.jida.guida) sul tema del [sogno tecnologico](https://www.facebook.com/giulimich/posts/10155301323668765?comment_id=10155303250963765&comment_tracking=%7B%22tn%22%3A%22R%22%7D), declinato in quello che si vorrebbe vedere da Apple, corrispondente o meno alla materialità degli annunci di prodotto.

Ci ho pensato.

Le mie soddisfazioni tecnologiche attuali arrivano da watch. Succede qualcosa, ti scrivono, ti contattano…? Lo sai con uno sguardo al polso, niente più.

E poi da iPad. Tralascio l’aspetto portatile; la sera a letto, di giorno sul divano, la mattina a fianco della colazione. Se serve fare qualcosa lo fai in un attimo; se serve leggere qualcosa, è leggero e comodo e veloce.

Ancora, da Siri. Verso la quale ho ancora un *know-how* da cavernicolo. Quel poco che faccio – chiamare, messaggiare, cercare una canzone, impostare un promemoria – lo trovo liberatorio e quasi giocoso.

Infine, dalla mia tastiera. È l’unica materialità che apprezzi nella tecnologia. Le dita pestano (le ho pesanti), i caratteri si depositano a raffiche sullo schermo, un comando, due comandi, il mondo si apre, si trasforma, si modifica a piacimento con pochi movimenti quasi impercettibili, a considerare il corpo. Un gran lavoro dai polsi in giù e solo quello.

Io sono contento perché la parte innovativa dell’[ultima presentazione Apple](http://macintelligence.org/blog/2016-10-29-cattiverie-accessibili/) consiste in una trasformazione parziale della tastiera, che conferisce ancora più potere e libertà alle mie dita.

Il resto? Ripenso a quello che ho scritto. Il mio sogno è *thinner, lighter*. Sempre più leggero, sempre più sottile, fino a quando scomparirà. Saprò di avere tecnologia Apple (o chi per essa) attorno a me, ma sarà come se non ci fosse. E i dati, solo i dati, campeggeranno davanti ai miei occhi o dentro la mia testa, o dove  giusto che campeggino. E ci sarà niente tra me, i dati e un pensiero della testa, o un gesto della mano, o un comando dato a voce.

Il mio sogno è essere circondato di tecnologia avanzatissima e immateriale. Il resto è un male necessario, che prima scompare e meglio sarà.