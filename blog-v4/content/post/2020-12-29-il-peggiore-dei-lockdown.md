---
title: "Il peggiore dei lockdown"
date: 2020-12-29
comments: true
tags: [ Gutenberg.org]
---
Sta per concludersi il 2020 e l’Italia rimane un Paese dal quale [è impossibile raggiungere in modo normale Gutenberg.org](https://www.balcanicaucaso.org/eng/Areas/Italy/Project-Gutenberg-blocked-in-Italy-many-doubts-few-certainties). Questo lockdown è cominciato poco dopo quello sanitario ma è molto peggiore.

Chiunque governi o faccia opposizione, se vuole avere un qualche mio voto futuro, riapra il sito di Project Gutenberg. Avesse qualche politico bisogno di informazioni, gutenberg.org contiene libri il cui copyright è scaduto, quindi liberamente distribuibili. Grazie.
