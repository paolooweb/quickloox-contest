---
title: "I modi contano"
date: 2015-12-19
comments: true
tags: [Sim]
---
Improvvisamente, sentendo dire quanto era comodo il salvataggio dei contatti sulle Sim, ho capito una verità fondamentale.<!--more-->

Errore capitale, di chi pensa che il divario sia tra quelli che usano la tecnologia e quelli che no.

Il divario è tra chi usa ogni tecnologia nel modo migliore e chi invece cerca di perpetuare all’infinito un solo modo, sempre quello, sempre lo stesso. Ed è un divario molto più devastante e pericoloso, perché chi non usa potrà sempre mettersi a usare.

Chi perpetua all’infinito un modo solo, invece, non userà mai il modo migliore. L’inefficienza e l’incapacità elevate a sistema.

(Le Sim, sparissero domani, sarebbe un Natale fantavoloso).