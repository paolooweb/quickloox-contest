---
title: "L’origine del mondo"
date: 2023-01-20T00:33:37+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [Lisa, Chm, Computer History Museum, Markkula, Mike Markkula]
---
Gennaio contiene troppi anniversari di rilievo che riguardano Apple e devo stare attento a non cadere in tentazione; sarebbe facile riempire un mese di anniversari, come se non esistesse un presente sommamente più interessante.

Però certi momenti sono unici e necessari persino. Ieri ha compiuto quaranta anni [Apple Lisa](https://computerhistory.org/blog/the-lisa-apples-most-influential-failure/) e sarebbe una grande occasione, anche se un anniversario come altri (l’anno prossimo in questi giorni compie quarant’anni Macintosh, vedi un po’).

Non accade tutte le volte però che il Computer History Museum possa annunciare, con il beneplacito di Apple, la [pubblicazione del codice sorgente di Lisa](https://computerhistory.org/press-releases/chm-makes-apple-lisa-source-code-available-to-the-public-as-a-part-of-its-art-of-code-series/).

Ognuno di noi situa un po’ dove vuole il punto di svolta della storia di Apple, il momento dal quale l’azienda ha iniziato davvero a cambiare il mondo una persona alla volta, che fosse evidente – come oggi – o meno. Potrebbe essere la costituzione della società, l’arrivo di [Mike Markkula](https://apple.fandom.com/wiki/Mike_Markkula), il ritorno di Steve Jobs, l’uscita di Apple ][ o di Mac o appunto di Lisa o di iPhone o di iPod o della LaserWriter o la cacciata di Jobs o questo o quell’altro.

L’arrivo di Lisa è sicuramente uno dei punti possibili e ci vedo una candidatura forte. Lì è stata mostrata l’interfaccia grafica, il mouse, programmi che salvavano automaticamente il contenuto, multitasking vero, un sacco di cose. Macintosh è quello che si è preso tutti i meriti avendo svolto gran parte del lavoro, tuttavia la strada l’ha aperta Lisa.

Lisa è stata la vera origine del computing moderno. Il codice sorgente è il suo monumento, che la eterna. Domani il suo funzionamento potrebbe essere ricostruito su hardware del costo di una frazione rispetto a quello originale. Se anche Internet finisse domani, in giro per il mondo ci sarebbe già, compresa la mia, un numero di copie del codice tale da garantire la conservazione di questo patrimonio storico. Quel software lo hanno scritto umani di straordinario talento, simbolicamente a bordo del rompighiaccio dell’informatica piuttosto che sulla nave da crociera come gli umani programmatori di oggi. È probabile che ci siano dentro pezzi leggibili e da leggere con un pizzico di emozione.

Lisa è stata anche, probabilmente, il fallimento più grande di Apple e, come tale, ha insegnato a tutti i protagonisti di questa storia tante nozioni utili e funzionali al successo che anni dopo avrebbe sorpreso persino i più ottimisti.

Anniversario speciale insomma, con circostanza specialissima. Lisa compie quarant’anni e va bene; tutti possiamo tenerne un pezzo importante sui nostri device e questo è veramente straordinario.

<blockquote class="twitter-tweet"><p lang="en" dir="ltr">The Apple Lisa source code is here! Check it out. <a href="https://t.co/pKgaGP1iiV">https://t.co/pKgaGP1iiV</a><a href="https://twitter.com/hashtag/AppleLisa?src=hash&amp;ref_src=twsrc%5Etfw">#AppleLisa</a> <a href="https://twitter.com/hashtag/ArtOfCode?src=hash&amp;ref_src=twsrc%5Etfw">#ArtOfCode</a> <a href="https://t.co/DM8sN9hK8s">pic.twitter.com/DM8sN9hK8s</a></p>&mdash; Computer History Museum (@ComputerHistory) <a href="https://twitter.com/ComputerHistory/status/1616075509133357056?ref_src=twsrc%5Etfw">January 19, 2023</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>