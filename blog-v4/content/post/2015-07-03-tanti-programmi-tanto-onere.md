---
title: "Tanti programmi tanto onere"
date: 2015-07-03
comments: true
tags: [DOSbox, Bluestacks, Android, Unix, Mac, Windows, browser]
---
Una volta andava di moda contare i programmi per le piattaforme. *Sei uno sfigato perché oltre ai cento programmi che usi ce ne sono solamente centomila che non usi. Io invece non ne uso più di un milione!*<!--more-->

Adesso, se prendo un Mac, ho a disposizione i programmi Mac. Poi posso emulare Virus, pardon, Windows con almeno quattro opzioni di macchina virtuale. Ho tutti i programmi Unix del mondo o quasi ed emulatori come [DOSbox](http://www.dosbox.com) per i giochi vecchi ma sempre buoni (sempre che non preferisca usarli nel [browser](https://macintelligence.org/posts/2015-04-30-imprese/)).

Adesso è uscita l’[edizione Mac di Bluestacks](http://www.bluestacks.com) che mette a disposizione di emulatore tutti i programmi esistenti per Android.

Vorrei contare quelli che contavano i programmi. Ma sono spariti. Peccato, ora che la cosa si faceva interessante.