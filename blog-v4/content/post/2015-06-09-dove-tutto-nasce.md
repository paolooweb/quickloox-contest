---
title: "Dove tutto nasce"
date: 2015-06-09
comments: true
tags: [Swift, Wwdc, CarPlay, HealthKit, ApplePay, AppleMusic, HomeKit, ResearchKit]
---
Cedo all’autocelebrazione perché il giorno dopo l’[apertura di Wwdc](http://www.apple.com/live/2015-june-event/) è anche il giorno dopo la disponibilità del mio libriccino su Swift anche in formato ebook.<!--more-->

Finalmente si può quindi scegliere se acquistare tre copie del [libro cartaceo](http://www.apogeonline.com/libri/9788850333387/scheda) oppure cinque di [quello elettronico](http://www.apogeonline.com/libri/9788850317387/scheda). Nell’interesse del lettore prima di tutto, perché così aumentano le probabilità che a tempo debito esca l’aggiornamento.

Che a un certo punto sarà doveroso perché il nuovo linguaggio di programmazione di Apple è in evoluzione e diventerà a breve *open source*, una delle novità notevoli emerse durante Wwdc.

Venendo alle minuzie, non si era mai vista una edizione così: tre sistemi operativi uno via l’altro, Apple Pay, Apple Music e le code di CarPlay, HealthKit, HomeKit, ResearchKit eccetera. Non sono bastate due ore per parlare di solo software, che è una cosa straordinaria persino al raduno degli sviluppatori. E diversi dettagli di interesse sono rimasti fuori.

Ero in spiaggia (a lavorare) e non avevo la garanzia di poter intervenire con serietà, per cui ho evitato il consueto invito in *chat* Irc e mi scuso con chi magari se lo aspettava.

Cento miliardi di *app* scaricate è una cifretta. E più non dico prima che tutti si sia studiato per bene il *keynote* (dal link in cima al *post*). Merita.