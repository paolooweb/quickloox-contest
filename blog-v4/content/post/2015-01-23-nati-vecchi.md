---
title: "Nati vecchi"
date: 2015-01-23
comments: true
tags: [iOS, Surface2, Microsoft, iPadAir, iOS9]
---
Per quanti si lamentano che un aggiornamento di iOS ha lasciato fuori il loro iPad o iPhone: è notizia che i [Surface 2](http://www.microsoft.com/surface/it-it/products/surface-2) [non avranno possibilità di passare a Windows 10](http://www.theverge.com/2015/1/22/7871563/microsoft-surface-rt-tablet-no-windows-10-upgrade).<!--more-->

Per loro ci sarà probabilmente un aggiornamento *ad hoc* che porterà solo alcune delle funzioni.

Il punto è che Surface 2 è stato annunciato a settembre 2013, più o meno il periodo in cui è apparso il primo iPad Air.

Alzi la mano chi pensa che iOS 9 non sarà interamente compatibile con iPad Air.

Va anche detto che Surface 2 lo hanno comprato quattro gatti. Nati vecchi e non lo sapevano.