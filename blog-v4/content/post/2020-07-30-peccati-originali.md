---
title: "Peccati originali"
date: 2020-07-30
comments: true
tags: [Store, Jobs, Cook, Google, Netflix, Office, Mac, BBEdit]
---
Amazon, Apple, Facebook e Google sono finite davanti alla commissione antitrust del Congresso statunitense per rendere conto di questa e quella condotta più o meno sospetta e, nel caso di Apple, inevitabilmente si è aperto il discorso anche su App Store e le sue politiche di amministrazione.

Il tema antitrust è molto ampio e magari capita una prossima volta di sviscerarlo (indizio: non si parla di leggi ma di efficienze su scala globale). Qui sto solo su App Store.

La fetta del trenta percento è un falso problema, o meglio l’esito del problema vero. Che sono due: Steve Jobs quando, nel 2008, dichiarò che App Store appena nato non avrebbe mai fatto soldi, perché il trenta percento delle vendite avrebbe coperto i costi; e le app gratis.

Jobs, come chiunque, non aveva alcuna idea di che cosa sarebbe diventato App Store. È chiaro che decine di migliaia di app a pagamento fanno molto più che coprire i costi di gestione. Con il senno di poi sarebbe stato utopicamente bellissimo che Apple avesse modificato ogni anno la quota di spettanza sulle vendite di App Store, in modo da coprire i costi e lasciare il resto agli sviluppatori.

Il problema vero nel problema vero sono le app gratis. Tutte, anche quelle che fanno riferimento a un servizio da pagare a parte come Netflix o Office. La presenza delle app gratuite, la cui distribuzione è pagata da quelle che gratuite non sono, complica tutto. La app smette di essere una palla di codice con uno scopo univoco per diventare un terminale di un servizio, un lettore di informazioni, un aggancio ad attività di vendita esterne allo Store. Da qui arrivano tutte le deformazioni dell’offerta, gli acquisti in-app, gli abbonamenti e così via. Apple ci mette del suo con i privilegi offerti alle app di gente troppo grossa per essere trattata male, app che vengono supportate dagli indipendenti che si spaccano la schiena (metaforicamente) per vendere quanto gli serve alla sopravvivenza.

Come se ne esce? Ripeto, il trenta percento è un falso problema. Non sono nemmeno convinto che sia sbagliato, anche se sono passati anni e intorno si sono sviluppate tecnologie che hanno reso il modello dello Store sempre valido ma non più innovativo e, forse, nemmeno più ideale per l’utilizzatore.

La mia proposta è abolire le app gratuite. Qualsiasi cosa arrivi da App Store va pagata. Eliminare gli acquisti in-app e pure gli abbonamenti. Apple si tiene il trenta, il venticinque, quello deve essere. Le app che sottintendono un business esterno ad Apple Store, come quelle già citate ma anche Amazon o il _New York Times_, devono costare almeno 0,99 e, eccezione alla regola, se lo tiene tutto Apple. In cambio, dentro la app si può vendere, iscrivere, fare tutto quello che oggi è proibito.

Uno sviluppatore ha facoltà di offrire aggiornamenti gratis così come di mettere a pagamento una nuova versione della app. Esattamente come fa un sacco di software indipendente su Mac.

Semplice, e può darsi, in grado di mantenere i livelli attuali di incasso di App Store. Fa pulizia di un sacco di schifezze, elimina le ambiguità di trattamento, tutto è più chiaro. App Store si avvicina di più all’idea di punto di distribuzione di software curato e di valore; l’antitrust si ritrova con vari argomenti in meno. I peccati originali si ritroverebbero riscattati.