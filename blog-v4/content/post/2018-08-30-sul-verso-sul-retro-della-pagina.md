---
title: "Sul verso sul retro della pagina"
date: 2018-08-30
comments: true
tags: [Overleaf, LaTeX]
---
Da un po’ di tempo (sempre con poco tempo) pongo più attenzione alle forme più evolute di testo marcato come LaTeX e posso solo suggerire con entusiasmo, a chi deve ancora scoprirlo, la [versione 2 di Overleaf](https://www.overleaf.com/blog/654-overleaf-v2-launch-announcement).

Overleaf è un eccezionale ambiente web per prendere confidenza con [LaTeX](https://www.latex-project.org) senza prendere impegni sul disco locale. E riconosce [Markdown](https://commonmark.org), il che vuol dire essere produttivi da subito quando serve e prima di essere stati promossi alle elementari di LaTeX.

Ah, è collaborativo. Per lavorare insieme, farsi spiegare, o spiegare.

Naturalmente è aperta la strada per [occuparsi di LaTex in locale](https://macintelligence.org/posts/2014-04-29-piu-veloce-di-tex/) senza prendere impegni con il web. Da qualunque parte si voglia arrivare, consiglio di arrivarci.