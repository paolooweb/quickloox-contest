---
title: "L’uomo che cadde sulla rete"
date: 2022-11-08T00:09:18+01:00
draft: false
toc: false
comments: true
categories: [Internet]
tags: [Bowie, David Bowie, Paxman, Jeremy Paxman, BBC]
---
Dedico il prossimo quarto d’ora di pausa [all’intervista concessa da David Bowie a Jeremy Paxman della BBC](https://www.youtube.com/watch?v=FiK7s_0tGsg&t=966s).

Sono minuti preziosi perché a un certo punto del 1999 Bowie inizia a parlare di Internet, del suo coinvolgimento, di che cosa significa per tutti, che cosa diventerà.

La rockstar si rivela presciente, preparata, non preoccupata, molto interessata: Internet porterà conseguenze *entusiasmanti e terrificanti*. Paxman replica *è uno strumento* e Bowie risponde fermo *no, è una forma di vita aliena*.

Lo scambio in tema contiene molto di più, in mezzo alle inevitabili parentesi artistiche e di gossip, e merita.

L’uomo e l’artista che, mentre in Italia le aziende consideravano Internet un giocattolo o una moda, si metteva a fare il fornitore di banda – c’è una bella [esegesi di Bowienet sul Guardian](https://www.theguardian.com/technology/2016/jan/11/david-bowie-bowienet-isp-internet) – vorremmo augurarci di averlo a lungo con noi, se non fosse già partito da tempo per la sua *blackstar*.

Auguriamoci allora di ricordare la sua lezione specialmente in tempi impegnativi per l’informazione e la convivenza in rete. Tranquillamente, lucidamente conscio delle opportunità e dei rischi, invece di lanciare allarmi dai giornali ha colto le potenzialità del nuovo mezzo espressivo e ne ha fatto occasione di comunicare più e meglio con il proprio pubblico.

Come abbiamo da fare noi, anche ove a sentirci fossero solo i colleghi o i parenti stretti. *We can be heroes*, ma basterebbe essere tranquilli e lucidi, ragionarci sopra, e muoverci con intelligenza, intesa come comprensione.

<iframe width="560" height="315" src="https://www.youtube.com/embed/FiK7s_0tGsg" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>