---
title: "Vedo, prevedo, non rivedo"
date: 2017-06-09
comments: true
tags: [Dediu, Asymco]
---
Horace Dediu di Asymco lo aveva già fatto a maggio: notare la [correlazione elevata](http://www.asymco.com/2017/05/11/predictably-profitable-unpredictably-valuable/) tra certe spese in conto capitale di Apple e la vendita di apparecchi.

Come notava, la correlazione si mantiene da dieci anni e fornisce un’ottima metrica per verificare e prevedere nel breve termine l’andamento di Apple. I dati sono di dominio pubblico, forniti dall’azienda una volta l’anno nella propria documentazione fiscale. Sono ampiamente affidabili; Tim Cook e compagnia dovrebbero rispondere di ogni falsificazione agli azionisti e magari pure ai giudici. Data l’entità dell’attività ci si immagina che il controllo e le verifiche siano frequenti e compiute su più fronti, dai media fino al fisco.

Dediu lo ha fatto di nuovo, con l’indicazione di una [correlazione multipla](http://www.asymco.com/2017/06/07/app-story/). Account iCloud, apparecchi ativi, fatturato della sezione Servizi procedono assieme e nella stessa direzione seguita da numero degli sviluppatori registrati, numero di app scaricate da App Store e pagamenti complessivi agli sviluppatori.

Presumere l’andamento di Apple in base all’ascesa di qualche cinese d’assalto o all’uscita di un nuovo prodotto Samsung, come capita di leggere sui barzellettieri della sera o sulle repubbliche della *gallery* di modelle, per non parlare degli spazzaturai del web, fa un po’ ridere dopo avere visto questi grafici. La verità è più impegnativa ma anche molto più evidente e chiara di quanto lascio intendere i titoli a effetto.