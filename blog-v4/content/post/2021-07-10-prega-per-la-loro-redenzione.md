---
title: "Prega per la loro redenzione"
date: 2021-07-10T00:43:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [Coca-cola, Euro 2020, Panini] 
---
Oggi per fortuna finiscono gli Europei di calcio.

Per fortuna, in quanto un noto marchio di bevanda gassata sponsorizza una raccolta di figurine dei calciatori.

Sulle lattine campeggia un codice QR, che porta a una pagina dove si può leggere quanto segue:

>E utilizzando l’App [omissis] potrai anche scansionare il logo [omissis] che trovi sui prodotti [omissis] per redimere un pacchetto di 5 figurine digitali!

Capisco che oramai *scansionare* sia diventato parte del linguaggio comune (il verbo relativo alla scansione sarebbe *scandire*). Possiamo passarci sopra.

Ripetere il nome del marchio a distanza di quattro parole è brutto. Anche qui, si capiscono le ansie del *marketing manager* e lasciamo passare pure questa.

Ma *redimere* le figurine… dato per scontato che si tratta di una sedicente traduzione di *to redeem*, che colpa avranno mai commesso?

Più che pensare alla redenzione delle figurine, penserei a quella dei *copywriter*. La religione più diffusa in Italia prevede il perdono. Ma a fronte di un pentimento sincero e del fermo proposito di non farlo più.

*Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile.*