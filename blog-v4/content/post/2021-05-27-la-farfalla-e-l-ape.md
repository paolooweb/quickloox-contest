---
title: "La farfalla e l’ape"
date: 2021-05-27T00:25:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [Dolphin, M1] 
---
I creatori di Dolphin, un emulatore di giochi per console, hanno portato il loro software su Mac M1 e lo hanno [messo a confronto](https://dolphin-emu.org/blog/2021/05/24/temptation-of-the-apple-dolphin-on-macos-m1/) con quello che succede su un MacBook Pro Intel oppure un PC desktop da gaming spinto con specifiche mostruose, processore i9 9900k e scheda video Nvidia Rtx 3090.

Hanno tirato fuori una misura che ultimamente quasi nessun sito di tecnologia si azzarda a rispolverare: prestazioni per watt di potenza. Il risultato:

> L’efficienza è quasi letteralmente fuori scala. Confrontato con un PC desktop assolutamente mostruoso, MacBook Air M1 usa meno di un decimo dell’energia per fornire circa il sessantacinque percento delle prestazioni. Per il povero MacBook Pro Intel, semplicemente, non c’è confronto.

MacBook Air. I risultati sono stati ottenuti su un modello di fascia consumer. Sarà interessante vedere (magari a partire dal [keynote del 7 giugno](https://developer.apple.com/wwdc21/)?) dove arrivano i processori ottimizzati per la potenza.

*Prestazioni per watt*. Viene in mente uno dei più grandi pugili di tutti i tempi, [Cassius Clay poi rinominatosi Muhammad Ali](https://en.wikipedia.org/wiki/Muhammad_Ali).

Sul ring manifestava assoluta leggerezza, del tutto improbabile per un peso massimo. Ma i suoi colpi lasciavano ugualmente traccia, accumulandosi fino a esaurire le risorse dell’avversario.

Prima di un match si disse *Stai in aria come una farfalla, pungi come un’ape*.

*Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile.*               