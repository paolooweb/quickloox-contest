---
title: "Cattiverie accessibili"
date: 2016-10-29
comments: true
tags: [MacBook, Pro, Photoshop, Final, Cut, Keynote]
---
Apple ha [presentato i nuovi MacBook Pro](http://www.apple.com/apple-events/october-2016/) e come prevedibile è partita la litania dei professionisti che si sentono abbandonati.<!--more-->

Non so e forse non voglio sapere. L’evento ha incluso spazi dedicati alle trasformazioni di Apple TV per esempio, e si è parlato anche di iOS, Apple Pay. Appare evidente che Apple, la Apple del 2016, vende molti prodotti per fare molte cose nella nostra vita. Anche professionali.

Probabilmente posso credere che un MacBook Pro sia sprecato per fare cose *solo* professionali. Ma questo perché le nostre vite sono ben diverse da quelle di un quarto di secolo fa, non per colpa dei MacBook Pro.

Apple ha dato ovviamente molto spazio alla presentazione di Touch Bar e, ma pensa, ha fatto esibire professionisti (!) alle prese con Photoshop, Final Cut Pro, una app per deejay che consentiva di mixare a perfezione usando unicamente Touch Bar. La novità di interfaccia dovrebbe almeno godere del beneficio del dubbio, dato che verrà certo usata da Office e certo da Keynote, tanto per citare categorie di programmi che sembrebbero abbastanza professionali.

Cattivi fino in fondo? Va bene. Apple ha dedicato i primi minuti del *keynote* all’accessibilità e alla [sezione dedicata](http://www.apple.com/accessibility/) del sito Apple. Consiglio la visione. Il video introduttivo del *keynote* dura due minuti. E che cosa si vede? Una disabile grave che esegue un montaggio spettacoloso di video su Final Cut Pro con una volontà e una capacità commoventi. Per quanto mi riguarda, il *keynote* poteva terminare lì e sarebbe valso la pena.

(Domanda così: un disabile, che già deve affrontare condizioni di partenza sfavorevoli, sceglierebbe mai un sistema dalle prestazioni inadeguate, giusto per aggiungere una condizione sfavorevole in più?)

E io me lo immagino, il professionista imbruttito (una figura del tutto ipotetica, modellata sull’[omonimo milanese](http://ilmilaneseimbruttito.com)). *Ecco, Apple ci ha dimenticato e oramai pensa solo ai disabili*.