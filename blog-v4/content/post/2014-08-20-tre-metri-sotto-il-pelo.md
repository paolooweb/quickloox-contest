---
title: "Tre metri sotto il pelo"
date: 2014-08-20
comments: true
tags: [Macworld, Griffiths, iPhone, iPhone5]
---
Storia a lieto fine quasi incredibile quella di [Rob Griffiths su Macworld.com](http://www.macworld.com/article/2465171/life-after-death-resuscitating-a-drowned-iphone-5.html), dove un iPhone resuscita a nuova vita dopo avere passato almeno cinque minuti sotto il pelo dell’acqua, appoggiato su un fondale melmoso a tre metri di profondità.<!--more-->

Sono serviti un’ulteriore immersione, ma questa volta nel riso, due smontaggi e rimontaggi e varie prove su singoli componenti che rifiutavano di tornare in piena funzione. Le sue conclusioni:

>iPhone è molto meno suscettibile all’acqua di quello che pensavo. […] Aprire e smontare un iPhone 5 è veramente problematico.

E infine:

>Se il vostro iPhone si bagna – anche se si bagna veramente – non abbandonate la speranza. Non premete il pulsante di accensione. Mettetelo nel riso per tutto il tempo che potete. Smontatelo se il trucco del riso non funziona. […] Non abbiate paura di sperimentare.

Niente di che. Ma un lieto fine in questi giorni ci sta bene.