---
title: "Dimostrazioni di piazzuola"
date: 2023-07-21T03:48:14+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [EasyPark]
---
Fino a ieri possedevo un giudizio unificante per le app di parcheggio: *sòle*, in romanesco.

Certo non sono l’utente ideale. Non ci penso, non mi preparo, penso ingenuamente che tutto si risolverà, poi mi trovo in una zona dove non ho altro modo, perché non ho moneta, perché tutto è ancora chiuso, perché ho fretta, perché provo sempre una app.

Allora scarico la app, pensando che mi risolva il problema. Una voleva l’acconto via carta di credito e va bene, solo che prometteva l’attivazione del servizio entro ventiquattro ore. Un’altra voleva la stampa (!) di un contrassegno da esporre sul cruscotto. Tutte avevano demenziali fasi di registrazione e raccolta dati.

Da utente pigro curioso e frettoloso, niente risolveva il problema come volevo e finiva che ci voleva meno tempo a cercare un altro parcheggio.

Vero che sono passati diversi anni da quelle esperienze e magari quelle altre app hanno avuto modo di migliorare e di crescere. Oggi tuttavia mi sono ritrovato nella stessa identica situazione e l’unico modo era [EasyPark](https://www.easypark.com/en-it) oppure farsi mezzo chilometro come minimo per trovare prima un Bancomat e poi un bar aperto.

EasyPark è stato il Nirvana del parcheggio. Si è installato in un momento, mi ha chiesto telefono e targa dell’auto (ci mancherebbe) e nient’altro. Ovviamente ho dovuto dare l’assenso alla geolocalizzazione da parte della app, ma questa è sicurezza e va benissimo così.

L’area di parcheggio è stata geolocalizzata in un attimo. In un secondo attimo ho potuto scegliere tra PayPal e carta di credito.

L’interfaccia per scegliere la durata del parcheggio è deliziosa, con un disco rotore stile iPod. Feedback eccellente, esperienza rassicurante.

Volendo, ciliegina sulla torta, EasyPark autoinstalla un widget che visualizza sullo schermata di blocco di iPhone quanto manca alla scadenza del parcheggio. Volendo, la app notifica quando manca un quarto d’ora alla fine del periodo.

Non ne avevo vero bisogno, ma ho provato. La Live Activity del widget era perfetta, con il tempo mancante, il promemoria dell’importo pagato, le informazioni essenziali.

Chissà quando mi ricapita (non sono un parcheggiatore molto frequente). Intanto EasyPark mi ha dato la dimostrazione di come si fa una signora app per pagare il parcheggio. La prossima volta, se vedo lo sticker sul totem della piazzuola, non avrò il minimo dubbio.

§§§

([Buon compleanno Emilio!](https://macintelligence.org/posts/2022-07-21-quelli-che-mancano/)).