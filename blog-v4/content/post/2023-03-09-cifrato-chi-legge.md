---
title: "Cifrato chi legge"
date: 2023-03-09T15:10:28+01:00
draft: false
toc: false
comments: true
categories: [Software, Web]
tags: [scrutch]
---
Credo di avere raggiunto il livello di saturazione verso le notizie di nuovi editor di testo Markdown che porta ad aprire il portello del forno in cucina, infilare la testa e aprire il gas.

A salvarmi sono l’avere un forno elettrico e che grazie al cielo Internet riesce ancora a regalare eccezioni, come in questo caso [scrutch](https://scrut.ch). Un animale curioso e che potrebbe tornare utile pur funzionando via browser, anzi, proprio per quello.

scrutch, l’autore vuole la minuscola, vive in una pagina web; è leggerissimo; è cifrato; è gratuito per chi non voglia pagarlo (ci ritorno tra poco); è *privo di registrazione*.

In altre parole, è un ambiente dove posso produrre note, appunti, articoli Markdown che nessun altro può leggere e che posso recuperare a volontà semplicemente tenendo nota dell’Url creato automaticamente dal programma, diverso per ogni istanza.

Nessun dato personale in ballo, non esistono iscrizione né registrazione, né password. Se lo uso in modo che merita, posso decidere di abbonarmi (sgrunt) al servizio per cinque dollari al mese, in virtù dei quali concorro unicamente a sostenere il lavoro di sviluppo. Altrimenti posso usare scrutch gratis senza alcuna limitazione. Per i cultori della privacy, in funzione della struttura del sistema, il pagamento è totalmente svincolato dall’uso che se ne fa. È materialmente impossibile collegare una certa pagina a un certo abbonato.

I testi possono essere condivisi, scaricati localmente, inviati a un indirizzo email. Se l’intento è lasciare qualcosa di visibile a tutti, scrutch inserisce la chiave di decifrazione nell’indirizzo e così chiunque abbia l’Url legge la pagina in chiaro. Altrimenti resta cifrata, illeggibile per chiunque altro, anche per l’autore del sistema.

Autore che dichiara di avere scritto scrutch perché non c’era e a lui avrebbe fatto comodo averlo. In effetti mi sembra un cigno nero e non ricordo di avere mai visto niente del genere.

A differenza di tanti altri editor Markdown via browser, questo farò in modo di ricordarmelo.