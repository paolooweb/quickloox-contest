---
title: "Piano quadriennale"
date: 2015-04-14
comments: true
tags: [Yojimbo, Speirs, ]
---
Mi è tornata la mania di usare [Yojimbo](http://www.barebones.com/products/yojimbo/), acquistato anni fa e poco usato, per contenere in modo sicuro la mia raccolta di password e numeri seriali personali. Nel fare pulizia dentro l’archivio delle pagine da leggere più avanti mi sono imbattuto nell’articolo di Fraser Speirs [Un supercomputer in ogni zaino](http://speirs.org/blog/2011/8/28/a-supercomputer-in-every-backpack.html).

Speirs parte dall’idea che sua figlia entrerà presumibilmente nel mondo del lavoro attorno al 2029 – e si chiede:

>Esiste un qualunque scenario plausibile non apocalittico in cui la tecnologia del 2029 è meno prevalente, meno ampiamente distribuita e meno innestata nella nostra cultura della tecnologia del 2011? Non riesco a immaginarne uno.

Sì, l’articolo è del 2011. Ed è quattro anni fa che Speirs scrive:

>Siamo già al punto che il rapporto tra professionisti e computer e 1:2. Un portatile e uno smartphone sono equipaggiamento standard nella nostra società. Con l’avvento della tavoletta può essere che ci spostiamo verso i tre o più computer a persona.

Al che riporta dati delle scuole scozzesi, dove vive lui, per constatare che ogni computer nelle scuole è mediamente condiviso tra 3,2 studenti. Un rapporto capovolto rispetto a quello del mondo comune. La domanda:

>Quando è stato l’ultimo anno in cui non avevo l’uso esclusivo di almeno un computer? Risposta: 1995. Sono passati sedici anni da quando i computer erano scarsi a sufficienza da doverli condividere.

Nel 2025, se le previsioni sono corrette, dovremmo avere sette computer a testa in media. Quanti ne troveranno gli studenti a scuola? E quanti computer sono a disposizione oggi nelle scuole italiane, anno 2015 e non 2011? Stranamente è una metrica che mi pare di non avere visto ne [La buona scuola](https://labuonascuola.gov.it).