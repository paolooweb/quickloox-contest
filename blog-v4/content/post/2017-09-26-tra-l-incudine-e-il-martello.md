---
title: "Tra l’incudine e il martello"
date: 2017-09-26
comments: true
tags: [iPhone, Face, ID, Equifax, TidBits]
---
Un tizio su *Hackernoon* denuncia iPhone X e Face ID come [orwelliani e inquietanti](https://hackernoon.com/dear-apple-the-iphone-x-and-face-id-are-orwellian-and-creepy-bfca99c61fca). La tesi è che, per il fatto stesso che vengano raccolti dati biometrici relativi alla faccia delle persone, gli hacker troveranno modo di approfittarne e in un modo o nell’altro la privacy nel mondo scomparirà.<!--more-->

L’esposizione è di basso livello e piena di inesattezze, ma è lecito preoccuparsi. Dopotutto si tratta di un gioco tra guardie e ladri, come quello delle serrature e dei grimaldelli. Ci si preoccupa della prossima mossa dei ladri dato che ora hanno mosso le guardie. Ci sta, fa parte dell’andare avanti.

Tutto questo però ha un’altra faccia. Di recente gli hacker hanno aggirato misure di sicurezza mediocri presso Equifax, agenzia di valutazione del credito che raccoglie su persone e organizzazioni i dati più disparati, sempre sensibili, per determinare quanto siano solvibili e in grado di restituire un eventuale prestito.

La falla ha messo in mano agli hacker dati fondamentali di centinaia di milioni di persone e soprattutto dati collegati tra loro. Un conto è per un malvivente trovare per strada una tessera sanitaria e una carta di credito; un conto è che siano tutt’e due relativi alla stessa persona.

Ecco, il codice fiscale. Un bell’articolo su TidBits spiega come lo Stato americano amministri le persone a partire [dal loro numero di previdenza sociale](http://tidbits.com/article/17457). Nove cifre, prive di qualsiasi codifica, che non si possono cambiare, portano talvolta a errori ed equivoci e, collegate alla persona proprietaria da parte di terzi, aprono la strada a qualunque tipo di problema e di abuso.

In Italia la situazione è meno grave. Ugualmente, lo Stato lavora a partire da identificativi, come il codice fiscale o il codice INPS, che sono [una rara combinazione](https://macintelligence.org/posts/2013-02-21-per-un-voto-utile/) di ingombro inutile, protezione zero, concezione arretrata e stupida.

Devo preoccuparmi di più della mia impronta facciale oppure dei miei codici di fisco, pensione, sanità? Devo fidarmi più della custodia di una Apple oppure di quella dell’Inps, del servizio sanitario nazionale, dell’Agenzia delle Entrate?

La risposta non è semplice e apre un lungo dibattito, intendiamoci. Certamente, gridare allo scandalo per Face ID trascura l’altra faccia del problema. Tipo: l’esercito ha le mie impronte digitali. Ricordo come venivano conservate e da chi. Se qualcosa del mio esistere attuale dipendesse in modo critico dalle mie impronte digitali, sarei un pochino preoccupato, più che da Face ID.