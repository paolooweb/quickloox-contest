---
title: "Uno che ci pensava"
date: 2016-01-18
comments: true
tags: [Jobs]
---
Ho scoperto solo ora, colpevolmente, un bel [ricordo di Steve Jobs](https://medium.com/keep-learning-keep-growing/what-do-you-think-930ea72d8e99#.4yra7k7nq) pubblicato lo scorso ottobre. Con il passare degli anni si fanno sempre più rari.<!--more-->

Anticipo il meno possibile, perché va letto tutto.

>Che lo sapesse o meno, il fatto di essere interessato alla mia opinione mi incoraggiava a impegnarmi per dare il meglio. E come risultato, ho finito per ottenere un risultato migliore di quello che ero convinto di saper fare.

Meglio di sette o otto biografie autorizzate o apocrife.