---
title: "Il bello sta dentro"
date: 2020-05-06
comments: true
tags: [Aaa, All, About, Apple, Stampa, Savona]
---
Eh sì, settimane in cui si parla di cose virtuali e l’interlocutore sente sempre il bisogno di aggiungere in coda che, comunque, bello il virtuale, però la realtà è un’altra cosa. Come se qualcuno potesse seriamente metterlo in dubbio o come se uno dovesse ripeterselo, magari nei momenti in cui la fede nella realtà vacilla.

Ho una posizione leggermente diversa e la questione riguarda, per esempio, gli amici di [All About Apple](https://www.allaboutapple.com) a Savona, che sono meritatamente [apparsi con un video su La Stampa online](https://video.lastampa.it/savona/visite-dal-divano-a-savona-il-museo-apple-piu-fornito-al-mondo-ecco-tutti-i-suoi-tesori/113534/113548).

Savona è in una regione diversa dalla mia e quindi anche il più velleitario dei miei desideri di essere lì in questo momento è destinato a non realizzarsi.

È bastato poco; è bastato quel video su La Stampa. Mi sono documentato sulla [visita virtuale al museo](https://www.allaboutapple.com/2020/03/iorestoacasa-all-about-apple-360/) e l’ho eseguita.

È un posto che conosco fisicamente non dico a memoria, però abbastanza bene. Ci sono stato più volte, da solo e in famiglia, per me è casa.

La visita virtuale, senza sentire il pavimento sotto i piedi, senza poter cliccare sul mouse di NeXT, dovrebbe essere un’altra cosa. E invece.

Il bello sta dentro. Sta nelle emozioni che un’esperienza può suscitare. Che siano emozioni online, offline, virtuali, reali, nominali, frattali, lo lascio agli esegeti. Mi sono sentito bene, ho rivisto gli amici, ho ripercorso le mie visite. Tutte cose che non si vedono, nella visita virtuale. Ma ci sono, eccome se ci sono.

In questo periodo specialmente, vale anche la pena di [pagare un biglietto](https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=FX445S9QGD9DC&source=url) al termine del giro. Anche simbolico, anche minimo, anche ininfluente. Non è mai ininfluente e in questo momento è tutto ciò su cui il museo può contare.

Si noti che valgono uguale, il contante e PayPal. Nessuno che arrivi a spiegarti come la banconota in mano sia tutta un’altra cosa.