---
title: "Argomenti ripetitivi"
date: 2019-09-03
comments: true
tags: [Terpstra, Terminale, bash]
---
Uno di quelli che ti fanno amare il Terminale anche se non ci sei abituato è Brett Terpstra. Basta guardare come, altre magie dell’articolo escluse, automatizza [l’inserimento in un comando dell’argomento usato nel comando precedente](https://brettterpstra.com/2019/08/29/shell-tricks-a-random-selection/).

Più facile a guardare che a dirlo. E c’è una spiegazione di come funziona la `history` di bash relativamente ai comandi appena digitati che veramente non avevo letto da nessuna altra parte. Complimenti.