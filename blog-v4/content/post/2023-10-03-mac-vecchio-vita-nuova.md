---
title: "Mac vecchio, vita nuova"
date: 2023-10-03T17:42:21+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [OpenCore Legacy Patcher]
---
[OpenCore Legacy Patcher](https://github.com/dortania/OpenCore-Legacy-Patcher) è finalmente un progetto, di quelli per portare i nuovi macOS sui vecchi Mac, che mi attira e proverei se avessi hardware voglioso di ringiovanimento.

Faccio in fretta a spiegare perché: è uscita una versione 1.0, testimonianza di uno sviluppo continuativo e di miglioramento nel tempo; c’è una data precisa (2007) di macchine che potrebbero giovare del trattamento; tutto è spiegato con chiarezza; appare una lista molto numerosa di *credits* e non è il lavoro di un singolo talento che ha passato una notte a divertirsi e domani penserà ad altro; il progetto ha una icona degna di questo nome. Nel 2023 è un *infine ma non da ultimo*; creare un’icona non è fare un disegnino trentadue per trentadue come ai tempi eroici e bisogna avere un’idea almeno più che vaga di che cosa sia un gradiente e che cosa il disegno vettoriale.

Ho anche ricevuto testimonianze di persone che ci sono riuscite e hanno approfittato di OpenCore Legacy Patcher su Mac non più supportati. Tutto aneddotico; se c’è un settore dove si possono solo scrollare le spalle e bofonchiare *your mileage may vary*, *può andarti diversamente*, è questo. le difficoltà che possono sorgere o meno, in ogni modello, con ciascun processore, a seconda della Ram e così via, sono le più varie.

A differenza di molte altre iniziative analoghe, però, questa appare credibile e ben fondata.