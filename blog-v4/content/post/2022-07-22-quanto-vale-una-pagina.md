---
title: "Quanto vale una pagina"
date: 2022-07-22T02:50:49+01:00
draft: false
toc: false
comments: true
categories: [Web, Software]
tags: [Google Analytics, Apple]
---
Sono allo smanettamento con Google Analytics e, non tanto riguardo allo strumento specifico quanto in generale, sono sempre più convinto che manchi una metrica fondamentale: il valore della pagina (in traffico, in conversioni, in quello che si vuole) dovrebbe essere messo in relazione con la durata dell’esposizione e parametrato sull’unità di durata.

Per capirci: se guardo il traffico di giugno, ci sarà un articolo pubblicato il primo giugno, che è rimasto esposto in rete per trenta giorni, e uno pubblicato il trenta giugno, che è rimasto esposto per un giorno e forse pure meno.

Gli strumenti attuali – per quanto ne so e accetto molto volentieri insegnamenti – mostrano che il primo articolo ha fatto trenta volte il traffico dell’ultimo, senza tenere in minimo conto la diversa durata di esposizione. L’articolo del primo giugno potrebbe avere performato molto meno bene, se misurato come media per singolo giorno, per dire.

Questo ragionamento è simile anche se ortogonale a quello usato da Apple per definire la redditività dei suoi prodotti. Ad Apple interessa zero – quasi zero – di quanti Mac venda; invece è molto interessata a sapere quanto gli rende ogni Mac venduto e lavora per fare crescere questa metrica.

È anche l’argomento definitivo da lanciare come un pesce in faccia ai sostenitori dell’obsolescenza programmata (se mi interessa spremere ogni unità venduta, preferisco che abbia una vita utile naturale e non artificialmente accorciata), anche se andiamo fuori argomento. Però, insomma, datemi uno strumento che mi dica in modo semplice quanto fa un articolo giorno per giorno, senza dovermi inventare sistemi fai-da-te per arrivarci.