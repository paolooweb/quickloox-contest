---
title: "Tra nuova vita e pesce d’aprile"
date: 2024-04-01T00:29:42+01:00
draft: false
toc: false
comments: true
categories: [Internet, Software, Blog, Hardware]
tags: [Comma, nginx, AMD, x86, ARM]
---
Quale miglior pesce d’aprile? *I commenti funzionano*.

Ci sono affinamenti da eseguire e bug da chiudere. Per esempio, dopo avere pubblicato un commento, meglio non premere di nuovo il pulsante di pubblicazione, perché pubblica di nuovo lo stesso commento…

Tuttavia, il meccanismo funziona. Questo post e tutti gli altri possono essere commentati. I commenti restano.

Restano anche tutti gli altri [canali per sentirsi](https://macintelligence.org/post/2024-03-28-lavori-in-corsa/).

Siamo passati tutti dalle medie a farci dire che *Pasqua* significa *passaggio*, no? Quale migliore giornata di questa per passare a una edizione ulteriormente migliorata del blog?

[Mimmo](https://muloblog2.netlify.app/) ha compiuto un lavoro monumentale. La ricerca, per esempio. L’evidenziazione della stringa cercata è ancora da perfezionare ma, gente, si può cercare con AND e con OR logici. Su una mole di articoli imponente, è una mano santa.

C’è un menu speciale per vecchi post dell’era di Script, recuperati nelle nebbie del passato del web.

Il feed Rss dovrebbe essere (e rimanere) più leggero. E ci sono tante altre piccole migliorie in giro.

Ho scaricato da Muut lo storico dei commenti, un file Json da una dozzina di megabyte. L’obiettivo è risistemarli nel blog. Non è per niente banale e però ci proviamo. Le persone più fidate possono chiedermelo e lo fornirò sicuramente. Per tutti gli altri, ne parliamo prima. C’è un po’ di privacy lì dentro, anche se sono commenti pubblici in un blog pubblico.

Mi riservo di dedicare un post dei prossimi al sistema dei commenti, che mi ha fatto rinunciare anche al baseball di Apple TV+ a suon di cacce all’errore e test di configurazione. Abbiamo imparato tanto.

Buona Pasqua a tutti anche se è appena passata. È passata anche una vecchia epoca del blog e se ne apre una nuova, con nuovi commenti, nuova ricerca, nuovi Rss. Raramente suono la mia grancassa. ma stasera oltre che stanco sono anche orgoglioso.