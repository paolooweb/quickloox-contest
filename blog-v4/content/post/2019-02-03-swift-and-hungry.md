---
title: "Swift and Hungry"
date: 2019-02-03
comments: true
tags: [Swift, Playgrounds]
---
Dopo anni di attesa ho potuto finalmente divertirmi qualche minuto con [Swift Playgrounds](Swift Playgrounds by Apple). Come primo impatto trovo che sia una idea carina, ragionevolmente curata, con margini di miglioramento molto, molto ampi. Anche così, comunque, ha un suo perché. Cominciare da zero, risolvere un quesito al giorno nella pausa caffè…? Le dedicherei del tempo, anche minimo, anche di ritaglio, a prescindere dall’opinione personale sul *coding*, esattamente come [Duolingo](https://www.duolingo.com/) permette di fare con le lingue straniere.

Detto questo, la seconda cosa che ho fatto è stata escludere Playgrounds dall’attività in background, dal momento che il consumo di energia di iPad Pro è decollato, a spese dell’autonomia, e Playgrounds ne era responsabile per più di tre quarti.

Forse è stato un caso, forse un bug, forse un malfunzionamento dell’applicazione, forse è normale che dopo il primo avvio vengano caricate risorse che potrebbero servire nel prossimo futuro. Qualche che sia la spiegazione, mi fa piacere che Swift Playgrounds dia il massimo mentre lo uso. E il minimo quando non lo uso, diversamente da come può andare per le mappe o la messaggistica.
