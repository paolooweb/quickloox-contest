---
title: "Caratteri difficili"
date: 2024-04-06T00:19:47+01:00
draft: false
toc: false
comments: true
categories: [Software, Blog]
tags: [QuickLoox, Comma, Hugo]
---
Attivare il nuovo sistema di commenti del blog è costato una considerevole quantità di tempo/uomo, in gra parte speso da chi si è letteralmente caricato sulle spalle l’impresa e ha risolto difficoltà piccole e grandi.

Con tutto il rispetto per il lavoro serio che è stato infuso nel progetto, voglio tuttavia ricordare tre momenti insulsi che ci hanno fatto perdere una quantità di tempo inusitata rispetto all’effettiva entità del problema.

Corrispondono a tre caratteri, nel posto sbagliato al momento sbagliato, che hanno richiesto tempo per essere individuati e farci capire la natura del problema.

Prima di tutto uno *slash*, la consueta barra obliqua. Una delle cose fatte a inizio lavoro è stato aggiornare [Hugo](https://gohugo.io/), il motore del blog, all’ultimissima versione.

Hugo perfettamente aggiornato, a differenza del predecessore, non tollera lo slash dentro i tag presenti nell’intestazione di ogni post.

Dei tremila e passa post attualmente pubblicati nel blog, *tre* di questi contenevano tag con dentro uno o più slash. Hugo seppelliva un indizio criptico sulla faccenda in mezzo a decine di righe di messaggi di errore inconcludenti. A un certo punto lo abbiamo trovato, abbiamo tolto lo slash ai tag dei tre post e finalmente Hugo nuovo ha perfettamente sostituito quello vecchio. Ma quanto tempo perso.

Poi, lo spazio. L’autore di [Comma](https://github.com/Dieterbe/comma), il sistema di pubblicazione dei commenti, ha programmato il software in modo che faccia attenzione all’arrivo di un commento. Quando arriva un commento, grazie a codice JavaScript inserito nel motore del blog, questo viene trasformato in un file dentro una cartella.

Quando Comma parte, gli si deve dire che cartella sorvegliare e su che porta logica ascoltare il traffico.

Di solito, quando si indica una porta in fondo a un Url, la si attacca con due punti in fondo all’Url stesso, come in *https://sitoqualunque.it:4567*.

La sintassi per fare partire Comma è

*comma /directory/dei/commenti :porta*, cioè tipo

*comma harddisk/blog/commenti :2444*

Quello spazio è tanto corretto, nella sintassi dell’autore, quanto irrituale. Chiamatemi sbadato, ma avrei giurato che gli era scappato per sbaglio. Invece no.

Infine, più banale, una *esse*; un *http* doveva diventare *https*. Solo che capirlo nel contesto in cui si trovava ha richiesto un bel po’ di pazienza e controlli e prove e ricontrolli.

Il software si mangerà il mondo ed è vero; ma è ancora tanto fragile e richiede ancora tanta precisione sistematica. Non è davvero per tutti e chi riesce a renderlo amichevole deve essere guardato con stima.

Se poi qualcuno cerca una sigla per un nuovo protocollo informatico, suggerisco caldamente */ s*, tre caratteri davvero iconici a questo giro. Se sono previsti nella Smorfia, anche giocarli al lotto va bene.