---
title: "Il piacere della slettura"
date: 2022-08-22T19:15:53+01:00
draft: false
toc: false
comments: true
categories: [Internet]
tags: [Facebook, Twitter]
---
All’inizio di quest’anno non ho fatto propositi particolari, al contrario di [altre volte](https://macintelligence.org/posts/2021-01-06-vacanze-e-verifiche/).

Quest’estate, però, sono riuscito a *smettere di leggere Facebook*.

Smettere di leggere Facebook è un’altra cosa che uscire da Facebook, qualcosa che mi è impossibile, per lavoro, e indesiderabile nella vita: i social, nessuno escluso, hanno una valenza importante per quanto riguarda i contatti personali e il contenuto informativo.

Continuo pertanto a entrare in Facebook e a seguire affetti, conoscenze e competenze; ricevo regolarmente messaggi in Messenger e reagisco a sollecitazioni opportune.

Semplicemente, non *leggo* Facebook. Non scorro la *timeline* in assenza di un motivo; non lo uso come passatempo o come riempitivo. I social, nessuno escluso, hanno una valenza negativa, di intrusione nel tempo privato e personale oltre ogni necessità e piacere legittimo.

Non divento né migliore né peggiore; semplicemente, uso meglio lo strumento, per quanto può essere utile e contribuire a lavoro, divertimento e apprendimento.

Instagram e LinkedIn erano già largamente a posto da questo punto di vista.

L’obiettivo è terminare l’estate e avere smesso di leggere anche Twitter. Non di informarmi, di comunicare, di partecipare; di leggere.

Sembra tutto più arioso e leggero.