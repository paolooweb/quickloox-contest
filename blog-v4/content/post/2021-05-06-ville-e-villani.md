---
title: "Ville e villani"
date: 2021-05-06T00:31:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [Apple, Epic, App Store, Fai, Fondo Ambiente Italiano] 
---
Presumo che con l’allentarsi delle restrizioni agli spostamenti riprenderemo le visite di famiglia alle ville curate dal [Fondo Ambiente Italiano](https://www.fondoambiente.it) (Fai). Sono luoghi culturalmente e paesaggisticamente interessanti, dove spesso vengono organizzate attività anche per bambini e ragazzi e comunque intrattenimento oltre il puro godimento della località.

L’ingresso è a pagamento – ridotto se si ha la tessera – e ci sono ovviamente regola da rispettare. Se all’interno delle ville avvengono vendite o spettacoli a pagamento aggiuntivo, la loro presenza è stata naturalmente concordata con chi amministra la villa per conto del Fai.

Non è previsto, e non succede, che un visitatore entri e si metta a vendere del suo senza avere un permesso; men che meno capita di vedere proposti bene e servizi in concorrenza a quanto effettuato dal Fai.

Ci sono altre ville come quelle del Fai, ma certamente non a ogni angolo di strada; se il Fai chiude una villa o – come in questo periodo – limita gli ingressi e richiede prenotazione obbligatoria nei weekend, l’unico comportamento possibile è accettare la decisione e, se proprio, cercarsi qualcos’altro. Qualunque atto contrario alle regole del Fai sarebbe un ingiustificabile atto di villania.

Con tutto questo, non ho sentito né letto del Fai come un monopolista chiuso, così come invece leggo e sento di Apple mentre [in tribunale si ascoltano i suoi avvocati contrapposti a quelli di Epic](https://www.npr.org/2021/05/04/993273526/the-epic-versus-apple-trial-has-begun-heres-what-you-need-to-know).

Mentre il *walled garden* del Fondo Ambiente Italiano è considerato luogo curato, protetto, sicuro e di garanzia per il rispetto dell’ambiente, quello di App Store dovrebbe passare come un monopolio irrispettoso dei diritti degli sviluppatori e specialmente di chi vorrebbe stare su iPhone senza dover attenersi alle regole di Apple. Neanche iPhone lo avessero inventato a Cupertino.

*Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile.*