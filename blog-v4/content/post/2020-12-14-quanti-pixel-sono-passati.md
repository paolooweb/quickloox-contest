---
title: "Quanti pixel sono passati"
date: 2020-12-14
comments: true
tags: [Pixelmator, Pro, Macintosh, Plus, M1, Photoshop, bitmap]
---
Non posso farlo dalla posizione del grafico professionista e neanche esperto: sono un ignorante dotato appena del minimo di esperienza per avere risultati decenti per il web o effettuare qualche modesta correzione di colore.

Questo premesso, sono passato da [Pixelmator](https://www.pixelmator.com/mac/) a [Pixelmator Pro](https://www.pixelmator.com/pro/) per la modesta cifra di 21,99 euro grazie a una promozione su Mac App Store.

La potenza che mi sono ritrovato sotto le dita, che peraltro devo imparare a riconoscere e dominare prima di capire bene cosa ho a disposizione, è senza confronto rispetto alla spesa.

Ripenso a quando ho visto la prima copia di Photoshop 1.0 girare su Macintosh Plus, in bianco e nero, e quando ho visto Photoshop stesso girare per la prima volta a colori, a un prezzo con il quale oggi ci si porta a casa un Mac fatto e finito, o giù di lì.

Non ho titoli per dire se Pixelmator Pro sia meglio o peggio, bello o brutto, professionale o amatoriale evoluto. Certamente ho compiuto un salto di qualità enorme, almeno potenziale, per due soldi. E se domani mi capitasse in casa un Mac M1, lui sarebbe già pronto.

Immagino che il vertice della grafica bitmap appartenga ad altri programmi. È ugualmente rinfrescante constatare quanto sia a portata di mano di chiunque poter sbirciare verso la vetta.

Era tanto che non sentivo un passo avanti così pronunciato nel lanciare un nuovo programma su Mac.