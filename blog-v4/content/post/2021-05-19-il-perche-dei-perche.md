---
title: "Il perché dei perché"
date: 2021-05-19T00:44:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [1802, Matteo, Word] 
---
Quando **Matteo** ha letto il mio resoconto sull’azienda [che usa Word e non sa perché](https://macintelligence.org/posts/Non-so-perché.html), ha voluto rispondere partendo dalla sua esperienza.

Matteo è uno dei miei _amici digitali_, per quanto mi riguarda; una di quelle persone con le quali ho avuto contatti di fatto nulli nel mondo reale e però si è sviluppato un rapporto attraverso la rete, di stima e rispetto. Lui è persona squisita, competente, appassionata e poco conta per me che lo faccia da una parte della barricata sovente opposta alla mia. Discutere, anche alla morte, sulle idee tra persone che si rispettano fa crescere e imparare e non per niente lui si occupa di formazione.

Lo fa dalla sponda Microsoft ed è per questo che ha deciso di spiegare in un post [perché lui usa Word](https://www.linkedin.com/pulse/perché-usate-word-matteo-discardi-commercial-trainer/); ha parecchio da dire sulle mie affermazioni.

Siccome [mi ha scritto](https://twitter.com/1802/status/1394545720779284480) di essere interessato a una risposta, lo faccio, solo che ci metterò un po’ e non so se mi basterà un post.

<blockquote class="twitter-tweet"><p lang="it" dir="ltr"><a href="https://twitter.com/loox?ref_src=twsrc%5Etfw">@loox</a> non ho capito come si fanno i commenti al tuo blog, e in ogni caso avevo molto da dire, così ne ho fatto un articolo. Mi piacerebbe avere il tuo parere, in modo sereno. Un saluto<a href="https://t.co/oaO4O32xng">https://t.co/oaO4O32xng</a></p>&mdash; 1802 (@1802) <a href="https://twitter.com/1802/status/1394545720779284480?ref_src=twsrc%5Etfw">May 18, 2021</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

La mia intenzione era rispondere, o iniziare a farlo, già in questa pagina, solo che una rapida proiezione mi ha collocato a metà delle argomentazioni verso le cinque del mattino. Devo riorganizzarle o trovare una sintesi migliore, o pensarlo come un ebook se proprio non riesco a starci dentro.

Nel frattempo segnalo il suo post perché è interessante, bene argomentato, aggiornato sul meglio che Word ha da offrire e preciso nel rappresentare la visione di un modello di professionista per il quale, se consapevole e preparato, Word può benissimo essere una risposta efficace a tanti requisiti.

Il mio punto di vista è diverso, perché vedo l’impatto di Word in maniera più sistemica e allargata, purtroppo anche più negativa. Non è detto che sia giusto, ma ho una esperienza paragonabile alla sua e sono confidente nelle mie conclusioni, che appunto proverò a riportare in modo ordinato e leggibile in tempi umani.

Per capire chi ha ragione, o come la ragione sia distribuita, una lettura del suo pezzo è essenziale. Quindi oggi, invece che fare i compiti come al solito, li assegno. Leggere Matteo e derivare una propria idea del se e del perché Word sia una esperienza adatta al professionista di oggi, che professionista, quale oggi eccetera.

*Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile.*