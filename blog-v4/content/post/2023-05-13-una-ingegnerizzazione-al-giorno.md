---
title: "Una ingegnerizzazione al giorno"
date: 2023-05-13T01:58:46+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [2FA, autenticazione a due fattori, macOS, iOS, iPadOS, Sasser, Cabel Sasser, Panic]
---
È capitata l’occasione per la prima volta sono ricorso al [sistema di autenticazione a due fattori nascosto nei sistemi operativi di Apple](https://macintelligence.org/posts/2023-02-19-autentiche-sorprese/).

Una meraviglia. L’esperienza utente potrebbe essere anche migliorata; d’altronde il fatto stesso che sia celato dentro il sistema in modo semiinvisibile spiega come la priorità non fosse renderlo usabile come prima cosa.

Ciononostante, la procedura è ragionevolmente semplice e rapida. Ho scandito con iPhone il codice QR del login che avevo su Mac e in un attimo avevo pronto il codice di autenticazione.

Ora andrebbe capito se fare il tifo per il partito di Cabel Sasser di Panic, che [auspica la creazione di una app fatta e finita](https://macintelligence.org/posts/2023-04-03-parola-chiave-di-cabel/) per fornire a tutti autenticazione a due fattori di base semplice ed efficace, oppure sostenere lo status quo.

Con la prima posizione, c’è rischio che la app vada sotto i riflettori, venga stravolta da qualche ingegnere ansioso di mettersi in luce, magari presenti qualche bug di sicurezza, insomma diventi un fastidio.

Con la seconda, *security through obscurity*, la si usa in pochi, fa quello che deve fare, non essendo esposta non riceve troppe attenzioni e alla fine è un bell’accontentarsi.

Quasi quasi la seconda. Una buona ingegnerizzazione al giorno toglie una app superflua di torno.