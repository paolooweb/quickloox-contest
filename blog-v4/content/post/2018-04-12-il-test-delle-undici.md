---
title: "Il test delle undici"
date: 2018-04-12
comments: true
tags: [Viticci, iPad]
---
Quanti degli [undici trucchi di Federico Viticci per lavorare meglio con iPad](https://www.macstories.net/ios/11-tips-for-working-on-the-ipad/) conoscevi già?

Per esempio, l’Url unico delle email mi era completamente misconosciuto.

Assolutamente da leggere.