---
title: "Di venti in venti"
date: 2021-10-26T00:12:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [M1, MacBook Pro, iPod, Intel, Amd, Bill Gates, AnandTech, Gizmodo, fp2017, M1 Pro, M1 Max, Apple Silicon] 
---
Sono ricorsi i [vent’anni di iPod](https://www.macworld.com/article/546264/apple-ipod-20th-anniversary-success.html) e naturalmente Bill Gates aveva ragione: [non sarebbe durato](https://www.techradar.com/in/news/phone-and-communications/mobile-phones/ipod-won-t-last-says-gates-186327).

Peccato che avesse ragione come quello che guarda dalla finestra la pioggia che cade e commenta *prima o poi smetterà*, trascurando che l’unica informazione rilevante di tutta la situazione è esattamente *quando* accadrà.

iPod è oggi un evento storico, un cambio di pagina poderoso che ha trasformato la parabola di Apple in modo decisivo e veramente cambiato le vite.

È apparsa su *AnandTech* la [recensione dei nuovi M1 Pro e M1 Max](https://www.anandtech.com/show/17024/apple-m1-max-performance-review/). *AnandTech* è un sito *serio*, non un qualunque Gizmodo dove una tipa qualunque definisce le prestazioni di M1 Max *ridicolous* (in modo molto positivo, è come se fosse tipo *pazzesco*) senza averne titolo.

Per questo fa specie leggere affermazioni così:

>La gamma di test di velocità fp2017 ha più carichi di lavoro che sono più legati alla memoria ed è qui che M1 Max è assolutamente assurdo. I carichi di lavoro che mettono il maggiore stress sulla memoria hanno tutti un vantaggio di prestazione moltiplicato diversi fattori rispetto al meglio che hanno da offrire Intel e AMD. Le differenze di prestazione sono semplicemente folli.

L’autore è un super esperto e si riferisce a test specifici. La recensione è articolata e in altri settori il vantaggio di M1 Max si riduce, o anche si annulla: non è il chip perfetto che chiude l’era dell’informatica, ma l’eccezionale iterazione di una serie appena cominciata anche se [affonda le radici in processi di produzione avviati anni e anni fa](https://macintelligence.org/posts/Vecchi-trucchi.html).

Che *AnandTech* usi questi toni, tuttavia, fa pensare; pensare che, come iPod ha lasciato un segno indelebile sulla storia degli ultimi venti anni, M1 – pardon, Apple Silicon – sia un eccellente candidato a farlo per i prossimi venti.

*Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile.*