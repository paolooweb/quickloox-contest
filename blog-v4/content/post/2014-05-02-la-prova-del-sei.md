---
title: "La prova del sei"
date: 2014-05-05
comments: true
tags: [iPad, 10-Q, iPod]
---
Trimestre su trimestre, iPad [ha venduto meno](http://images.apple.com/it/pr/pdf/q2fy14datasum.pdf) questo inverno che nell’inverno scorso.<!--more-->

È la fine, vero? Apple è spacciata, Android domina il mondo, nulla sarà più come prima, Steve non avrebbe mai permesso una cosa del genere, esatto?

Un po’ di calma. Nella [relazione](http://investor.apple.com/secfiling.cfm?filingID=1193125-14-157311&CIK=320193) che Apple deve depositare quattro volte l’anno presso le autorità statunitensi, l’evoluzione dei prodotti viene seguita (anche, da sempre) sui sei mesi.

Nell’ultimo semestre, iPad ha venduto praticamente lo stesso numero di unità rispetto allo stesso semestre di un anno prima. A voler essere pignoli, anche qualche decina di migliaia in più.

Astuto trucco contabile per fare tornare i conti? No, richiamo alla naturalità delle cose. Si sono venduti 210 milioni di iPad in quattro anni e la piattaforma è in salute checché dicano le cifre. Se si vuole un esempio di prodotto in calo, si guardino piuttosto gli iPod. Quello sì, è un *business* a termine.