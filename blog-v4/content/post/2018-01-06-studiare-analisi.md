---
title: "Studiare analisi"
date: 2018-01-06
comments: true
tags: [Fortune, Apple]
---
Su *Fortune* appare una [analisi quasi enciclopedica dello stato della nazione Apple relativamente al design](http://fortune.com/2017/12/22/apple-products-design/).

Mentre l’inizio è di quelli apocalittici, sempre in bilico su un possibile disastro imminente che poi non arriva, lo svolgimento è molto più interessante, con una vera disamina di come stiano andando le cose e di come andassero prima.

La grande verità che emerge è *Apple rarely gets it right first*, raramente ci azzecca al primo colpo. Questione di iterare e di saper individuare un percorso di crescita sensato, *while adding more, and more powerful, features*, intanto che si aggiungono più funzioni e più potenti.

L’articolo rivela forse più di quanto vorrebbe: la tabella dei flop elenca solo prodotti molto vecchi e in generale il design c’entra poco. I grafici di capitalizzazione e fatturato non fanno pensare alla catastrofe prossima ventura.

Impressiona comunque quanto sia necessario scrivere per raccontare la storia e lo stato del design di un’organizzazione all’avanguardia che detta la linea da decenni. Per raggiungere un giudizio degno di essere valutato bisogna studiare e molto.

Un cavetto che si rompe, o una batteria difettosa, rivelano molto più di se stessi – e dei proprietari – che la qualità del lavoro di centomila persone e del team di design più performante al mondo.
