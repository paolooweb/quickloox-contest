---
title: "L’utile utility"
date: 2017-11-26
comments: true
tags: [Mac, Utility, Disco]
---
Ho perso il ritmo giornaliero a causa di un errore brutto su Mac. Improvvisamente la macchina si spegneva da sola appena rimasta senza cavo per pochi minuti, come se la batteria avesse smesso di scaricarsi oppure si scaricasse in brevissimo tempo.<!--more-->

Il problema vero stava nel boot che non andava a buon fine: Mac si spegneva quasi sempre durante l’avvio, non importa con che precauzioni o combinazioni di tasti.

A boot miracolosamente avvenuto, macOS dava l’avviso di batteria da riparare. Azzerare il [System Management Controller](https://support.apple.com/en-us/HT201295) era perfettamente inutile, a ulteriore teorica conferma. Ho perfino ordinato una batteria di ricambio su Amazon, trentasette euro compresi i cacciaviti appositi.

La macchina ha smesso completamente di avviarsi con successo. Ho provato il boot da disco esterno, che ha avuto successo, e ho esaminato l’Ssd interno con Utility Disco. Risultato: disco corrotto e impossibile da riparare, con annesso consiglio di riformattare e reinstallare.

Ho proceduto con Time Machine e *apparentemente* ora tutto è tornato a funzionare. Rimango all’erta, ma intanto ho annullato l’ordine su Amazon.

Morale: un disco corrotto può creare problemi che sembrano non correlati. Mac è nuovamente funzionale da ventiquattro ore circa mentre scrivo e la batteria sembra in condizioni normali per la sua età. Viva Utility Disco che [ho criticato di recente](https://macintelligence.org/posts/2014-07-30-profondo-ricco/) ma almeno per una volta mi ha tratto di impaccio, fosse anche solo per avere segnalato un problema senza poterlo risolvere.