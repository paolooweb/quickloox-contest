---
title: "Al tocco del gambero"
date: 2016-10-16
comments: true
tags: [Windows, Multi-touch, Mac]
---
Il *trackpad* del mio MacBook Pro di febbraio 2009, sette anni e mezzo dopo, funziona benissimo. A *Ars Technica* lo sanno e [scrivono](http://arstechnica.com/gadgets/2016/10/pc-oems-ditch-the-custom-touchpad-drivers-give-us-precision-touchpad/):<!--more-->

>Uno dei tradizionali punti di forza di Apple sta nei suoi touchpad. Non solo Apple detiene algoritmi accurati che consentono il riconoscimento accurato di clic e gesti, ma offre anche una ricca gamma di gesti configurabili incorporati nel sistema operativo e aggiornati con esso.

Poi continuano e io, ingenuo, ancora una volta trasecolo:

>Con Windows 10, Microsoft può [a breve, con un aggiornamento] offrire funzionalità equivalenti.

Si vuole dire che prima non poteva? Il mio MacBook Pro, ripeto, ha sette anni e mezzo. Mica è stato il primo ad avere un trackpad come si deve. L’articolo va avanti:

>Bisogna però tenere conto del fatto che la maggior parte degli utenti di touchpad non riceverà i vantaggi di questi aggiornamenti.

La maggior parte? Il software si aggiorna ma tanto lo hardware resta al palo? Sembra il mondo alla rovescia. Siamo nel 2016, pare il 1999.

Proseguo e capisco: fino a oggi, Windows non poteva influire sulla precisione e sulle funzioni dei *trackpad* e tutto era affidato ai *driver* proprietari dei vari fabbricanti (stavo per scrivere *trafficanti*, lo ammetto).

Windows 10 sta per acquisire queste proprietà ma le può applicare solo se è presente un Precision Touchpad. Manco a dirlo, alcuni PC sul mercato ne dispongono e molti altri ancora no. Questi ultimi restano con quello che possono fare i loro driver proprietari. Che magari si aggiorneranno per poter offrire funzioni simili, magari no, chissà, mah, boh.

>La mancanza di funzioni Precision Touchpad ora sta diventando uno svantaggio significativo.

Credo che lo svantaggio peggiore sia muoversi in un mondo che progredisce al passo, no, al tocco del gambero. Per citare a sproposito Guareschi *si sta meglio qui, su questa riva*.