---
title: "Pur di vendere"
date: 2023-01-22T23:32:20+01:00
draft: false
toc: false
comments: true
categories: [Web, Hardware]
tags: [Twitter, Dili Scan, Acer]
---
[Fa impressione](https://twitter.com/DiliScan/status/1617110662139809792). *I macbook 2020 invenduti vengono ora regalati in fretta e furia. Grandi liquidazioni di massa*.

<blockquote class="twitter-tweet"><p lang="it" dir="ltr">I macbook 2022 invenduti vengono ora quasi regalati in fretta e furia. Grandi liquidazioni di massa.</p>&mdash; Dili Scan (@DiliScan) <a href="https://twitter.com/DiliScan/status/1617110662139809792?ref_src=twsrc%5Etfw">January 22, 2023</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

Il link porta a una pagina che lascia presagire i peggiori phishing. Da lì si segue un altro link da famiglia Addams del web e, almeno in un caso, si arriva effettivamente a una [pagina del sito Acer](https://store.acer.com/it-it/notebook/business?series=4063%2C3943&gclid=EAIaIQobChMI8ernpZ7c_AIViY1oCR0g0gUNEAAYASAAEgLGVvD_BwE&gclsrc=aw.ds) che applica sconti interessanti sui prodotti business.

La pagina Acer è vera. Entrando dalla home si vede come, effettivamente, il sito prometta i *saldi invernali* e sconti *fino al 50 percento*.

Però, accidenti. Non capisco chi sia responsabile di tutta l’operazione. Probabilmente i twittatori sono degli affiliati di bassissima lega che guadagnano qualche centesimo per quello che Acer vende a chi proviene dalle loro paginacce.

Oppure Acer dà qualche soldo a gente dall’aspetto professionale discutibile per farsi aiutare a raggiungere veramente ogni segmento del mercato, con qualunque mezzo: una raspa per grattare il fondo del barile.

Vendere, ok. Svendere, ok. A chiunque, ok. Le aziende si reggono sui fatturati. Vedere completamente ignorato il cliente, basta che clicchi su qualcosa, fa pensare a dove sia arrivato il mercato dei PC.

Ah, di Macbook neanche l’ombra. Servono per acchiappare i fessi su Twitter e a null’altro.