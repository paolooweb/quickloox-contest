---
title: "Fame comica"
date: 2023-08-26T02:18:55+01:00
draft: false
toc: false
comments: true
categories: [Internet]
tags: [Erlich, Paul Erlich, Simon, Julian Simon, The Population Bomb]
---
Nel 1968 il biologo Paul Erlich divenne personaggio pubblico grazie al suo libro *The Population Bomb*, in cui sosteneva che [la Terra non potesse dare supporto a più dei tre miliardi e mezzo di persone presenti in quel momento](https://www.npr.org/sections/money/2013/12/31/258687278/a-bet-five-metals-and-the-future-of-the-planet). Di ospitata in ospitata Erlich preannunciò una carestia senza precedenti, in cui sarebbero morte di inedia centinaia di milioni di persone, e si trovò a discutere con Julian Simon.

Dove Erlich vedeva un meccanismo inarrestabile che avrebbe travolto la civilizzazione Simon, economista, [pensava alla capacità di adattamento umana](https://www.cato.org/economic-development-bulletin/julian-simon-was-right-half-century-population-growth-increasing). Se qualcosa manca, si troveranno alternative.

Nel 1980 i due scommisero sull’andamento del prezzo di cinque metalli nei dieci anni seguenti. Se, a partire da una base di acquisto di mille dollari, i prezzi fossero aumentati, nel 1990 Simon avrebbe pagato a Erlich la differenza. Se invece fossero diminuiti, a pagare la differenza sarebbe stato Erlich. Il punto della scommessa era che la crescente scarsità avrebbe fatto salire i prezzi secondo Erlich. Simon la vedeva nel modo opposto, i prezzi non sarebbero aumentati.

Dieci anni dopo, Erlich inviò a Simon un assegno di cinquecentosettantasei dollari e spiccioli. I prezzi di tre metalli erano calati; quelli degli altri due erano aumentati nominalmente, ma in modo inferiore al tasso di inflazione.

La carestia preannunciata da Erlich non si è vista e, anzi, ci troviamo nel miglior periodo di sempre per quanto riguarda l’accesso planetario al cibo.

Bisogna essere giustamente preoccupati per lo sfruttamento intensivo delle risorse del pianeta, non sostenibile. Al tempo stesso, però, bisognerebbe essere responsabili nell’uso dei dati. Erlich divenne una celebrità come profeta di sventure e il bello, si fa per dire, è che ancora oggi continua a prefigurare la catastrofe in arrivo. Si è sottoposto a vasectomia per dare l’esempio e sostiene la necessità di ridurre la popolazione a non più di due miliardi di persone, incurante degli effetti che questa transizione, effettuata in modo non graduale, avrebbe sul nostro modo di vivere (se rientrassimo nella lista dei due miliardi di aventi diritto).

Sembra uno di quelli innamorati dell’idea più che attento ai fatti. In un certo senso, nella sua mente superiore la situazione è talmente chiara che la catastrofe *deve* avvenire. Guai a riconoscere che la realtà non coincide completamente con l’Idea.

Fatico ad associarlo a una figura del campo tecnologico. Tuttavia mi viene in mente Bill Gates, l’uomo per cui seicentoquaranta chilobyte di RAM sarebbero stati sufficienti per tutti, iPod non sarebbe durato e prima o poi l’umanità avrebbe corso il pericolo di una pandemia. Talmente intelligente da capire come andrà il mondo anche se il mondo va in un’altra direzione.

A sbagliare è il mondo e sono convinto che Erlich sia sinceramente arrabbiato per il fatto di non vedere la gente morire di fame a milionate. Una realtà così scandalosamente divergente dalle sue idee perfette.

Chissà che non abbia ragione, ma su scala temporale assai diversa. Per il Club di Roma, i[ prezzi di moltissime risorse schizzeranno vertiginosamente in alto entro cento anni](https://macintelligence.org/posts/2023-08-23-il-futuro-dei-miopi/) dalla pubblicazione de *I limiti dello sviluppo*, datato 1972.

Avranno avuto ragione? Torto? Siamo a metà del cammino e per ora non c’è molto da segnalare. Ancora una volta, un conto è avere ragione sul principio, un conto è dimostrarlo con dati e conclusioni sbagliate. Erlich, alla fine, è un personaggio tragicomico, prigioniero della sua costruzione mentale fino a sconnettersi dal mondo reale. Miliardi di persone ringraziano gli scienziati capaci di migliore le tecniche agricole e dare da mangiare a quasi tutti, invece che morire di fame per il gusto di vedere confermata una teoria.

*Gli aggiornamenti in agosto del blog potrebbero essere irregolari o infrequenti.*