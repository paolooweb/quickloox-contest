---
title: "Volente o nolente"
date: 2016-06-04
comments: true
tags: [Windows, Thurrott, Microsoft]
---
In questi giorni penso con affetto a tutti quelli che stanno su una vecchia versione di OS X essenzialmente per partito preso, adducendo pretese di incompatibilità del software o altre questioni che sono autentiche nell’uno percento dei casi e, per il resto, sono partito preso.<!--more-->

Magari gli venisse la voglia di passare a Windows. Dove Microsoft sta imponendo praticamente a qualunque PC sul pianeta di passare a Windows 10 e, per buona misura, è arrivata a [truccare la finestra di dialogo che annuncia l'installazione](https://www.thurrott.com/windows/windows-10/67367/upgradegate-microsofts-upgrade-deceptions-undermining-windows-10) in modo tale che, se uno la chiude, *avviene l’installazione*.

È una cosa di enormità tale che perfino un fanatico semilobotomizzato come Paul Thurrott ne parla in termini che non ammettono discussione:

>Qui la violazione della fiducia è quasi indescrivibile.

In pratica, Windows 10 si installa comunque sul computer di chiunque, volente o nolente, osservi una minima disattenzione all’interfaccia di installazione.

(La possibilità di chiudere una finestra di installazione senza fare esplicitamente un clic su un pulsante *Installa* o un pulsante *Non installare* è un perfetto esempio degli abominî di interfaccia utente che Windows perpetra da decenni, ma è un altro discorso)

E non è esattamente una installazione indolore. È Windows. Il computer può restare inutilizzato per molto tempo. Possono venire a mancare driver essenziali (Samsung [sconsiglia di installare Windows 10](http://www.mobilescout.com/windows/news/n68574/Samsung-advises-customers-installing-Windows-10.html)). Poiché l’installazione può avvenire contro la volontà dell’utilizzatore, può avvenire in una circostanza critica o sfavorevole. Eccetera.

E per oggi Windows 10 è gratis, per tutti, evviva. Dall’anno prossimo, Microsoft passerà a battere cassa e se ne vedranno delle belle.

Chi preferisce stare su Mavericks e si lamenta che Apple è cattiva e pratica l’[obsolescenza programmata](https://macintelligence.org/posts/2016-04-20-stampante-stai-serena/), veramente non sa che cosa sia il mondo là fuori.