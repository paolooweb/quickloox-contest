---
title: "Quarantasette volte meno"
date: 2017-11-18
comments: true
tags: [iPhone, X, Android, Lookout]
---
Citazione da [Come iPhone si è guadagnato il suo record di sicurezza](https://www.ft.com/content/6f956fb6-98ad-11e7-8c5c-c8d8fa6961bb).<!--more-->

>Nel quarto trimestre 2016 e nel primo trimestre 2017, 47 apparecchi Android su mille tra quelli difesi da [Lookout](https://www.lookout.com) sono incappati in minacce portate da una app, a confronto con un apparecchio su mille per quanto riguarda iOS.

Quarantasette contro uno.

Qualche pagliaccio racconterà che è dovuto alla scarsa diffusione di iOS, solo un miliardo di apparecchi attivi.

Qualche altro comico dirà che costano meno e fanno tutto quello che fa un iPhone. Già; il problema, qui, è quello che su iPhone *non* succede. O meglio succede quarantasette volte meno.

>Dieci anni di iPhone e nessun malware importante? Non si era mai sentita una cosa così.

(grazie **Matteo**!)