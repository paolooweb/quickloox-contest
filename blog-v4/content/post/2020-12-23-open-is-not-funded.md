---
title: "Open != Funded"
date: 2020-12-23
comments: true
tags: [App, Store, Wesnoth, iOS]
---
Leggo dalle [pagine wiki di Battle for Wesnoth](https://wiki.wesnoth.org/Project):

>I fondi per l’infrastruttura, la grafica e il sonoro sono forniti da  Wesnoth, Inc., azienda statunitense che amministra il fatturato generato dalla versione ufficiale del gioco su App Store.

Questo App Store da osteggiare perché chiuso, proprietario e inerentemente malvagio, contribuisce alla qualità e alla crescita di un gran bel [progetto open source](https://www.wesnoth.org). Forse è il caso di essere meno rigidi nella tassonomia dei buoni e dei cattivi, delle anime belle e dei cinici.

[Battle for Wesnoth](https://apps.apple.com/it/app/battle-for-wesnoth-hd/id575852062) costa quattro euro e mezzo.

Importante:

>Wesnoth, Inc. supporta finanziariamente il progetto ma non ha alcun coinvolgimento nella sua organizzazione e nelle direzioni di sviluppo.

Il software libero è fantastico, quando lo sosteniamo.

*(Legenda: != significa “non uguale a”).*