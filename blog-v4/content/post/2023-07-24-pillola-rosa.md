---
title: "Pillola rosa"
date: 2023-07-24T10:24:03+01:00
draft: false
toc: false
comments: true
categories: [Internet]
tags: [Barbie, Microsoft, code.org]
---
C’è un film di grande successo in questi giorni che parla del mondo di una bambola. Visto che tutti i pretesti sono buoni per ispirare dell’attività di coding, niente di male nell’adottare il film, come [fa code.org](https://twitter.com/codeorg/status/1682455404041719818):

<blockquote class="twitter-tweet"><p lang="en" dir="ltr">Are your students excited about <a href="https://twitter.com/hashtag/BarbieTheMovie?src=hash&amp;ref_src=twsrc%5Etfw">#BarbieTheMovie</a>? Have them try an <a href="https://twitter.com/hashtag/HourOfCode?src=hash&amp;ref_src=twsrc%5Etfw">#HourOfCode</a> with Barbie herself! Use programming to explore a variety of careers in this <a href="https://twitter.com/gotynker?ref_src=twsrc%5Etfw">@gotynker</a> activity: <a href="https://t.co/bucFuU8sY2">https://t.co/bucFuU8sY2</a></p>&mdash; Code.org (@codeorg) <a href="https://twitter.com/codeorg/status/1682455404041719818?ref_src=twsrc%5Etfw">July 21, 2023</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

La stessa code.org quasi dieci anni fa si metteva nei panni di Elsa, la principessa di *Frozen*, per [consigliare alla bambola di lasciar perdere](https://twitter.com/codeorg/status/537671103523848192) L’idea di voler essere *anche* un’informatica:

<blockquote class="twitter-tweet"><p lang="en" dir="ltr">&quot;Elsa to Barbie: Let it go&quot; <a href="http://t.co/lsXDcyGbdB">http://t.co/lsXDcyGbdB</a> <a href="http://t.co/KpTIwEJWoV">pic.twitter.com/KpTIwEJWoV</a></p>&mdash; Code.org (@codeorg) <a href="https://twitter.com/codeorg/status/537671103523848192?ref_src=twsrc%5Etfw">November 26, 2014</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

La bambola veniva considerata un esempio negativo da respingere.

Oggi che la bambola fa sfracelli al *box office*, invece, nessun problema e benvenuta nel club. La pillola rosa adesso fa bene.

Sarà una coincidenza, ma [uno dei *platinum supporter* di code.org è Microsoft](https://code.org/about/supporters).