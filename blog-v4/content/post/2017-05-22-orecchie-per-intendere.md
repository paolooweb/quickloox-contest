---
title: "Orecchie per intendere"
date: 2017-05-22
comments: true
tags: [Fraunhofer, Mp3, Aac, Lossless]
---
Mi hanno un po’ stupito le reazioni al [mio commento](https://macintelligence.org/posts/2017-05-16-gia-sentito/) sulla fine dello sviluppo e del supporto di Mp3 da parte dell’istituto Fraunhofer.<!--more-->

Mi è stato fatto notare che il [commento di Marco Arment](http://www.audioblog.iis.fraunhofer.com/mp3-software-patents-licenses/) è meno fazioso. Arment si limita a ribadire l’ovvio: Mp3 non è morto, sono semplicemente scaduti i brevetti. Lo [dice persino Fraunhofer](http://www.audioblog.iis.fraunhofer.com/mp3-software-patents-licenses/), non mi pare questo _scoop_. Dopo di che Arment passa tutta la fine del pezzo a minimizzare una questione interessante: a parità di qualità i file Aac occupano meno spazio. Ed è faziosetto, vorrei farlo parlare con gente che ha dischi rigidi pieni di musica e qualche giga lo risparmierebbe volentieri.

C’è chi ha ricordato come Aac venisse usato praticamente solo dagli apparecchi Apple. Ma è una verità assai parziale. Rimando alla pagina di Wikipedia (urgh) [dedicata a Aac](https://en.wikipedia.org/wiki/Advanced_Audio_Coding). Oggi iOS e Android sono compatibili Aac ed è gioco-partita-incontro, tre miliardi e spiccioli di apparecchi ed è detto tutto. Ma in passato?

>YouTube, Nintendo DSi, Nintendo 3DS, DivX Plus Web Player, PlayStation 3, vari modelli di Nokia Series 40, PlayStation Vita, Wii (con l’aggiornamento Photo Channel 1.1), Sony Walkman MP3, BlackBerry.

È solo una frazione di tutto quello che suppporta o ha supportato Aac. Mancano i costruttori di autoradio, Xbox, Sony Ericsson, Kenwood, Sonos, Roku, Squeezebox, RealPlayer, Vlc e una montagna di altri, la pagina  è senza fine. Flash di Adobe supporta – supportava? – Aac. Aac _è un formato praticamente universale_.

Qualcuno ha fatto notare che sul suo computer ci sono tremila Mp3 e nessun Aac. Sul mio ci sono settecento Apple Lossless, duecento Aac e cento Mp3, più o meno. E adesso? Abbiamo dimostrato che la nostra esperienza personale vale fino alla porta di casa e poi basta. O devo pensare che il mondo ascolti musica in Apple Lossless?

Viva Mp3. Ma Aac è meglio: suona meglio a parità di spazio, occupa meno spazio a pari qualità. La trita pagina di Wikipedia, basta quella, spiega i numerosi perché della sua superiorità tecnica e qualitativa. E sono cose evidenti anche dieci anni fa, a chi aveva orecchie per intendere.