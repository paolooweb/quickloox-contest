---
title: "La banda è come l'acqua"
date: 2013-08-31
comments: true
tags: [Internet]
---
Mi è uscito spontaneo dire che Internet è diventata una fornitura primaria come acqua, gas e luce.

Mi hanno risposto che senz’acqua si muore di sete invece senza Internet si vive lo stesso e le altre prevedibili ovvietà.<!--more-->

Una casa media italiana di cento anni fa (se è un problema facciamo centocinquanta) non aveva acqua corrente, non aveva luce elettrica e non aveva gas. Molti non ce l’hanno fatta, ma altri – erano i nostri bisnonni o i loro genitori – sono sopravvissuti e difatti siamo qui a parlarne. Non si muore di sete, senza acqua corrente; tutto è “solo” più difficile e averla è un segno di progresso, benessere, civiltà eccetera.

E poi: qualcuno ricorderà certe località balneari di anni fa, specie in zone d’Italia meno sviluppate di altre. Arrivavano tanti turisti, il sole era cocente e l’acqua corrente spariva. Tornava magari di notte, oppure dai rubinetti diurni usciva un filo e la gente riempiva pazientemente bottiglie, taniche, pentole, vasche da bagno. I proprietari facoltosi o lungimiranti montavano cisterne sui tetti, qualcuno si arrangiava con l’acqua piovana e tubazioni improvvisate.

Ecco, l’acqua corrente nelle località balneari di quegli anni lontani è uguale a Internet nelle stesse località balneari oggi: dal rubinetto esce un filo quando si è fortunati, o c’è da sperare che di notte arrivi qualcosa.

Visto che non c’è differenza?