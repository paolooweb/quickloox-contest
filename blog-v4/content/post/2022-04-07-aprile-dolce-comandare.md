---
title: "Aprile dolce comandare"
date: 2022-04-07T00:41:19+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [Shortcuts, Comandi rapidi, MacStories, Automation April, Viticci, Federico Viticci, Voorhees, John Voorhees]
---
Ma quanto è bella l’idea di *MacStories* di varare [Automation April](https://www.macstories.net/stories/introducing-automation-april/)?

Me ne sono accorto solo ora e fortunatamente nulla è perduto, tutto può essere letto e, più di questo, ogni Comando rapido può essere scaricato. Il sito ne ha più di duecentocinquanta liberi e gratuiti e solo in questa prima settimana ne sono arrivati altri dieci. Prima di decidere che servono poco o nulla, meglio dare un’occhiata, perché un Comando rapido utile trasforma la vita (nel computing).

Un esempio? John Voorhees utilizza un trittico di Comandi rapidi per [tenere in sincrono gli appunti che prende durante le presentazioni con il video delle presentazioni suddette](https://www.macstories.net/stories/automation-april-a-three-part-shortcuts-workflow-for-syncing-timestamped-research-notes-with-videos/).

Federico Viticci si è impegnato a creare un Comando rapido al giorno per tutto il mese di aprile e ha già pubblicato [i frutti della prima settimana](https://www.macstories.net/stories/10-shortcuts-for-automation-april-week-1/).

Sarebbe bellissimo impegnarsi allo stesso modo, anche per creare Comandi rapidi stupidi o inutili, pur di crearli e prenderci la mano. Ma anche uno a settimana.

Ma anche fare molto meno, solamente varcare la linea della piccola pigrizia che fa perdere tempo pur di non volersi cimentare nell’imparare qualcosa. O meno del molto meno; il formato di data dei miei post, per volere di [Hugo](https://gohugo.io), è abbastanza involuto.

Per qualche giorno mi sono ingegnato a scrivere i nuovi post partendo dal post precedente, del quale modificavo la data a mano. Poi mi sono detto *perché?* e ho messo al lavoro [BBEdit](https://www.barebones.com/products/bbedit/). Scrivere un *clipping* che faccia lavorare le sue funzioni automatiche è una sciocchezza; la fatica, chiamiamola così, che ho fatto è stata arrivare a concepire `#date#T#time#+01:00`. Sempre BBEdit permette di assegnare scorciatoie di tastiera ai comandi in modo talmente facile da essere disarmante.

Così ora, per scrivere la data corrente di un nuovo post nel formato involuto gradito da Hugo, premo *control-opzione-D* ed ecco che arriva in automatico *2022-04-07T00:59:31+01:00*.

Così banale che c’è da vergognarsi, così piccolo che chiunque è in grado di dire *posso fare meglio*. E allora, perché no? Festeggiamo l’Automation April con qualche fuoco di artificio di scripting, Comandi rapidi, righe ingegnose di Terminale. Ci sentiremo in sella ai nostri *device* come non è mai capitato prima.