---
title: "Un bel Wordle dura poco ma apre un mondo"
date: 2022-01-12T00:53:16+01:00
draft: false
toc: false
comments: true
categories:
tags:
---
Dai, l’esistenza di [Wordle](https://www.powerlanguage.co.uk/wordle/) è di dominio pubblico e non serve dilungarsi.

Probabilmente va spesa una parola sul fatto che la [legge di Sturgeon](https://effectiviology.com/sturgeons-law/) regge perfettamente anche su Internet: [App Store va riempiendosi di cloni-spazzatura](https://www.macrumors.com/2022/01/11/wordle-app-store-clones/) che scopiazzano indecentemente Wordle e cercano persino di farsi pagare. La spazzatura su Internet va combattuta senza quartiere: se incappiamo in un clone-cacca, va segnalato ad Apple o a chi per lei e boicottato senza eccezioni. L’unico Wordle sicuro, autentico, genuino è quello linkato nella prima riga di questo post.

Tornando a cose per le quali vale la pena di fare tardi la sera, Dr. Drang si è messo a [fare scripting attorno a Wordle](https://leancrew.com/all-this/2022/01/wordle-letters/) per trovare una buona strategia di gioco basata sulla frequenza delle lettere nelle parole. Dal dizionario presente in Unix agli script Perl per isolare le parole di cinque caratteri, leggerlo è una delizia. Non sono cose difficili, ma risultano molto più facile dopo averle lette nei suoi post.

**Aggiornamento**: Federico Viticci ha creato [WordleBot](https://www.macstories.net/ios/wordlebot-a-shortcut-to-annotate-your-wordle-results-with-scores/), un Comando rapido che riepiloga l’andamento di ciascuna partita tentativo per tentativo.

Non sa infine quasi nessuno che **Misterakko** è al lavoro su una [versione italiana di Wordle](https://wordle.accomazzi.net). Una versione italiana e non un clonaccio: si gioca con sei lettere invece di cinque (l’italiano ha parole mediamente più lunghe) ed esistono le lettere accentate (la parola di ieri era *perché*).

L’originalità è attestata anche da qualche peccato di gioventù. Su iPhone la resa visiva va migliorata, per esempio, e l’input è meno elegante. Però è già giocabile (se inseriamo una parola non presente nel dizionario, bisogna premere il tasto *Indietro* del browser).

Un gioco piccolo, una grande idea. Ed ecco una piccola tacca nell’universo.

*Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile.*
