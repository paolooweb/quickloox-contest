---
title: "Contesto e passione"
date: 2022-03-21T03:11:43+01:00
draft: false
toc: false
comments: true
categories: [Web]
tags: [PlainTextSports]
---
Non trovo dimostrazione attuale della potenza della rete e del testo puro più cogente dell’incredibile [PlainTextSports](https://plaintextsports.com).

Per un appassionato di sport americani, è una fonte di aggiornamento pazzesca per velocità, sintesi, dettaglio e panoramica.

Ma un altro appassionato, magari di statistica, resterà ammirato dal servizio e dall’essenzialità.

Se poi la passione è per la rete e per il software ingegnoso, beh, c’è solo da entusiasmarsi di fronte al lavoro di [una singola persona](https://twitter.com/CodeIsTheEnd), che però è capace di programmare e concepire flussi di dati con pulizia mentale estrema.

Io sono appassionato di Terminale e vedere impiegato il testo puro così mi fa venire le farfalle nello stomaco.