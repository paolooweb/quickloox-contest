---
title: "Tutto e niente"
date: 2017-09-17
comments: true
tags: [Tutto, Niente]
---
*Fa tutto quello che mi serve* è il criterio di valutazione supremo a livello personale. Nessun criterio gli è superiore.

*Fa tutto quello che mi serve* è il criterio di valutazione più futile a livello generale. Qualunque altro criterio è più significativo.

È ora di farlo notare.
