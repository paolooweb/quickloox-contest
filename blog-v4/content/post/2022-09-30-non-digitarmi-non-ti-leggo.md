---
title: "Non digitarmi, non ti leggo"
date: 2022-09-30T01:01:43+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [Mary Jo Foley, Microsoft, Foley, iPhone, SwiftKey, iOS, privacy, ZDnet, Bing]
---
Il 5 ottobre Microsoft [ritirerà da App Store la propria tastiera predittiva SwiftKey per iOS](https://www.zdnet.com/article/microsoft-is-phasing-out-swiftkey-for-ios/), per non tornare più (o almeno speriamo).

Il motivo della decisione non è stato dichiarato. Mary Jo Foley [scrive su ZDnet](https://www.zdnet.com/article/microsoft-is-phasing-out-swiftkey-for-ios/):

>Ritengo che la decisione abbia a che fare soprattutto con le politiche di Apple sulla salvaguardia del proprio walled garden. Il team Phone Link di Microsoft sa bene che, se Apple non concede accesso a certe interfacce, non c’è un modo buono o cattivo che sia di fare funzionare un prodotto che richiede integrazione.

Una parentesi a parte, vista l’occasione: quelli che parlano delle testate partigiane verso Apple e si piccano di essere equilibrati, equidistanti e seduti in saldo equilibrio sulla punta del simbolico cocuzzolo del dibattito, non parlano mai dei toni e della partigianeria di Foley, a livello di master postuniversitario. Ma andiamo avanti.

Non mi risulta che su App Store siano sparite le tastiere alternative per iPhone, né che non funzionino. Ovviamente un colosso come Microsoft ha motivazioni diverse dalla totalità di tutti gli altri concorrenti in tema e tutto il diritto di eliminare un prodotto che non porti i risultati previsti. Ci si può chiedere quali potessero essere questi obiettivi.

Si deduce velocemente dalla presenza di varie tastiere alternative in App Store che il problema tecnico della concessione di questa o quella interfaccia di sistema sembra una motivazione debole, o almeno discutibile.

Negli ultimi anni che cosa è cambiato, particolarmente, su iOS? Il livello della tutela della privacy si è alzato.

Qual è stato il primo problema evidente di alcune tastiere alternative vendute su iOS? La scarsa tutela della privacy degli acquirenti. Una tastiera alternativa legge volentieri l’input su essa per tarare con sempre maggiore precisione la propria predittività ed è un’ottima cosa, a patto che poi i dati raccolti siano amministrati con onestà e correttezza, cosa che non è immediatamente stata di tutti i produttori.

Ultimamente Microsoft si è trovata al centro delle chiacchiere per un [uso diciamo disinvolto su Bing di dati provenienti da fonti non pertinenti all’uso del browser](https://macintelligence.org/posts/2022-08-10-sulle-tracce-dellincoscienza/). Ovvero, Microsoft si pone il problema della privacy fino a un certo punto (non molto avanti).

Mettiamo tutto insieme e posso giocare a fare il piccolo Mary Jo: ritengo che la decisione abbia a che fare soprattutto con l’impossibilità di avere il livello desiderato di sorveglianza e sfruttamento indebito dell’input sulla tastiera di Microsoft, che avrebbe dovuto raggiungere gli obiettivi grazie al passaggio sottocoperta di dati e analisi a Bing e alla divisione pubblicità e invece no.

Prova del nove non c’è, ma mi accontento di un indizio: lo sviluppo di SwiftKey su Android, dove le persone sono carne da profilazione, continua indisturbato. Nessun commento sulle politiche di Google da parte di Foley, peraltro.