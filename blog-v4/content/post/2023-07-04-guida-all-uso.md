---
title: "Guida, all’uso"
date: 2023-07-04T00:27:10+01:00
draft: false
toc: false
comments: true
categories: [Hardware, Software]
tags: [Bluetooth, CarPlay, iPhone]
---
Piena autorizzazione a sganasciarsi dalle risate nel sentire il racconto di questo *parvenu* che, abituato a comprare computer nuovi e automobili vecchie, per la prima volta nella vita si è messo al volante di un veicolo dotato di Bluetooth, *pairing* e [CarPlay](https://www.apple.com/ios/carplay/).

Detesto telefonare, ma farlo dal sistema audio della macchina è stato abbastanza divertente e meno stressante del tenere in mano un cellulare. La tua musica, le tue mappe (previo collegamento via USB con cavo), sentire che il cavallo d’acciaio attorno a te diventa una specie di estensione di iPhone fa davvero effetto.

(Sono benvenuto nel club di chi lo fa da quindici anni?).

Bluetooth è tanto utile e tanto irrinunciabile. Ma riesce a dare sempre il suo piccolo mal di denti e sia benemerita Apple per tutte le aggiunte e migliorie che gli mette intorno per migliorarne affidabilità e possibilità.

Prossima puntata: l’autore del blog imposta pieno di meraviglia un timer su iPhone per la cottura delle mezze penne. In attesa della [prossima versione del sistema operativo](https://www.apple.com/ios/ios-17-preview/), quando potrà impostarne anche uno per il sugo, contemporaneamente. *Viviamo davvero in un’epoca di meraviglie*, [chiosava Craig Federighi](https://macintelligence.org/posts/2023-06-06-la-solita-wwdc/).