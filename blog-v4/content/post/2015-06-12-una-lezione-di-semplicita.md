---
title: "Una lezione di semplicità"
date: 2015-06-12
comments: true
tags: [kOoLiNuS, Safari, WebKit, Safari, Apple]
---
Devo una risposta a [kOoLiNuS](http://koolinus.tevac.com/l-autore/). Mi è capitato di [scrivere](https://macintelligence.org/posts/2015-06-05-trasparenze/) che, se voglio vedere come si comporta Safari rispetto – per esempio – ai *cookie*, lo scarico.<!--more-->

Lui mi ha commentato

>ma seriamente pensi che Webkit sia identico a Safari rilasciato da Apple, ogni singola riga?

A parte che non ho mai scritto una cosa del genere, la sua risposta non è sbagliata. Ma non è semplice. E stavo scrivendo un *post* semplice, come cerco di fare spesso.

Una risposta semplice deve per forza sintetizzare. La risposta complessa è che [esistono almeno 24 sapori di WebKit](http://www.quirksmode.org/webkit.html) e non ce ne sono due con lo stesso comportamento in ogni circostanza.

Il resto della risposta complessa è ancora meno semplice e sta in questo bellissimo [post di Paul Irish](http://www.paulirish.com/2013/webkit-for-developers/). Da cui estraggo un frammento semplicissimo:

>WebKit Nightly è il *porting* per Mac di WebKit, che funziona dentro lo stesso eseguibile usato da Safari (per quanto mancante di alcune librerie). Per lo più è Apple che decide che cosa c’è dentro, per cui il suo comportamento e il suo insieme di funzioni è congruente con quello che troveremo in Safari. […] WebKit Nightly sta a Safari come Chromium sta a Chrome.

Insisto a semplificare: se voglio sapere che cosa fa Safari sotto il cofano, scarico [WebKit](https://www.webkit.org/). Se voglio sapere che cosa fa Internet Explorer sotto il cofano, non posso.

Accetto di essere smentito e va bene anche se c’è complessità.