---
title: "Topi per trappola"
date: 2019-07-22
comments: true
tags: [Gassée, \Mac, iPad, Arm, Monday, Note, Intel]
---
Jean-Louis Gassée svolge normalmente un buon lavoro con MondayNote, con un’eccezione: dopo avere scoperto tardivamente che iPadOS prossimo supporta anche il mouse, ha messo insieme [un pezzo invero confuso](https://mondaynote.com/apple-no-macintosh-forks-but-the-ipad-78894ddfadfb) dove mescola la possibile transizione di Mac ai processori Arm con quella di iPadOS verso la differenziazione da iOS  di iPhone, a favore di una maggiore versatilità produttiva.

Che una possibile transizione di Mac ad Arm debba essere graduale e forse anche parziale, bastava chiunque con un minimo di nozione del contesto. Che iPad sia avviato a proporsi come computer a tutto tondo lo si vede già ora (da inizio giugno sono fuori casa e uso unicamente iPad Pro; zero problemi), anche senza iPadOS 13.

Ma il mouse, lo ha sconvolto. Ha ritrovato evidentemente il computer della sua giovinezza e pensato che a dare valore al ricordo fosse il primo più che la seconda.

Sbagliato. Non so che supporto verrà dato al mouse in iPadOS definitivo, eppure invito a considerarlo per quello che è: una sfumatura di utilizzo in più, utile in casi di accessibilità problematica o per situazioni peculiari che necessitano di un tipo di input non convenzionale.

Pensare al mouse su iPad per via di avere passato una vita a usare mouse? Non ci penso proprio. anche perché, se sullo schermo di iPad vedo una cosa che mi interessa, *la tocco*. Come si possa fare meglio in tempo e in precisione con un mouse, sono curioso di sentirlo.

Un conto sono modalità eccezionali di input, nel senso dell’eccezione alla regola. Un altro conto è la trappola della nostalgia.
