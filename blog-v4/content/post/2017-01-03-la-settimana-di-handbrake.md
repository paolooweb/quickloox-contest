---
title: "La settimana di Handbrake"
date: 2017-01-03
comments: true
tags: [Handbrake, Francesco]
---
Dopo tredici anni è arrivata la versione 1.0 di [Handbrake](https://handbrake.fr) e tra i vari commenti [ne è arrivato uno](http://mac360.com/2016/12/handbrake-the-party-is-over/) che viene spontaneo condividere:

>Ora, se Handbrake potesse acquisire un flusso di dati da Netflix, potrebbe essere un argomento su cui scrivere in quanto, per tutti gli scopi e gli intenti, la festa del Dvd è terminata.

Un fatto per tutti è che non esistono portatili Apple con un SuperDrive interno, e non da ieri.

Contemporaneamente Handbrake conserva la sua (straordinaria per esecuzione) funzione ove ci siano ancora operazioni di transcodifica video da compiere e capita che mi scriva [Francesco](http://quasi.me):

>ho trovato una cosa che è in grado di spezzare le reni alle batterie di una macchina altrimenti capace di garantirmi servizio per una giornata lavorativa o quasi senza bisogno di essere ricaricata… Handbrake! Non l’ho mai usato molto ma in questo periodo ho deciso di convertire alcuni DVD o Blu-ray in altro formato. Quando sta rippando, la batteria scende a circa un’ora… vuoi vedere che quei [geni di Consumer Report](https://macintelligence.org/posts/2016-12-28-assoli-di-batteria/) hanno fatto qualche test del genere senza porsi minimamente il problema del cosa stessero facendo e con cosa?

Ecco, Handbrake riesce a risultare contemporaneamente arretrato e attualissimo; si merita sicuramente il titolo di punto di interesse di questa settimana.

(Per chi mira ad aumentare le prestazioni e ridurre gli ingombri, Handbrake è disponibile anche in versione per il solo Terminale. Sempre sul suo sito)