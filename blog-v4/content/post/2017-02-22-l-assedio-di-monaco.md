---
title: "L’assedio di Monaco"
date: 2017-02-22
comments: true
tags: [Monaco, Linux, Baviera, Windows, Office, LibreItalia, LibreOffice, Ballmer, Pesaro, Assisi]
---
Un esercito incattivito e avido [cinge d’assedio](http://www.zdnet.com/article/why-munich-should-stick-with-linux/) la capitale della Baviera allo scopo di invadere i sistemi informatici della pubblica amministrazione locale con Windows e Office.<!--more-->

Avevo scritto del passaggio di Monaco al software libero [su Apogeonline](http://www.apogeonline.com/webzine/2013/11/21/23-milioni-di-euro-per-liberare-gli-ostaggi-di-monaco), segnalando la grande entità degli interessi in gioco.

Ai tempi si mosse pure Steve Ballmer (probabilmente a detrimento della sua azienda), viaggiando di persona a incontrare l’amministrazione locale. Oggi Microsoft ha fatto viaggiare addirittura il proprio quartier generale tedesco, trasferitosi [proprio a Monaco](https://mspoweruser.com/microsoft-calls-new-german-headquarters-the-biggest-shift-since-the-industrial-revolution/).

Il sindaco attuale di Monaco è stato ritratto dai media [come un tifoso di Windows](http://www.zdnet.com/article/munich-sheds-light-on-the-cost-of-dropping-linux-and-returning-to-windows/), immagine che ora cerca di smentire. 

Lo [studio](https://www.ris-muenchen.de/RII/RII/DOK/SITZUNGSVORLAGE/4277724.pdf) in base al quale si deve decidere il passaggio a Windows nel giro di alcuni anni (che non è stato ancora deciso, anche se tutti si comportano come se lo fosse) è stato compilato da Accenture, [partner Microsoft dell’anno per il 2016](https://newsroom.accenture.com/news/accenture-and-avanade-named-microsoft-2016-alliance-partner-of-the-year.htm).

Tutto questo dimostra che la natura della decisione è lontanissima dall’essere tecnica, come mostra l’[analisi](https://blog.documentfoundation.org/blog/2017/02/14/statement-by-the-document-foundation-about-the-upcoming-discussion-at-the-city-of-munich-to-step-back-to-windows-and-ms-office/) pubblicata da Italo Vignoli sul sito di The Document Foundation; è politica e finanziaria. Nessuno dirà mai apertamente che cosa ha contrattato Microsoft in cambio dello spostamento del quartier generale nazionale.

I fatti sono che, primo, dietro alla piattaforma software della municipalità, ruotano appunto interessi enormi.

Secondo, a nessuno interessa il merito. Perfino il sindaco tifoso ha ammesso che tornare al software proprietario ha costi mostruosi, a carico dei contribuenti. Perfino Accenture ha scritto nel suo studio che ci sono anche argomenti contrari alla tesi del necessario ritorno al passato proprietario. Le obiezioni a Linux sono sinteticamente *non mi piace* oppure *ho problemi a usare Linux, causati dalle idiosincrasie del software Microsoft, quindi voglio Microsoft*.

Una nota comunque critica verso il sistema attuale è che forse OpenOffice costituisce una scelta discutibile rispetto al più aggiornato, compatibile, curato [LibreOffice](http://it.libreoffice.org/). Tuttavia, lo ripeto, qualunque questione tecnica è un puro pretesto rispetto alle forze in gioco.

Il resto è un copione già frusto, che Microsoft ha praticato in altre realtà di minor peso come il [Comune di Pesaro](http://www.apogeonline.com/webzine/2015/09/16/parole-che-pesaro): brandire uno studio fintamente imparziale che dimostri quanto è difficile evadere dalla prigione di Windows e Office, per consigliare il ritorno volontario in gabbia.

A un caso dì oscurantismo e ignoranza, fortunatamente, sincontrappongono anche situazioni virtuose, di recente il [Comune di Assisi](http://www.assisinews.it/politica/software-libero-libreumbria/). Tutti noi possiamo fare la nostra parte propugnando il software libero presso le pubbliche amministrazioni locali e magari sostenendo anche concretamente [LibreItalia](http://www.libreitalia.it/), della quale sono orgogliosamente e colpevolmente (per non fare abbastanza rispetto a quanto dovrei) socio.

A Monaco magari vinceranno gli assedianti. Ma è solo una delle tante battaglie. A noi evitare di ritrovarci assediati. Oppure, spezzare il cerchio malato che già abbiamo attorno.
