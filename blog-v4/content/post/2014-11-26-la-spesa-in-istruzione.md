---
title: "La spesa in istruzione"
date: 2014-11-26
comments: true
tags: [GeoGebra, BioDigitalHuman, education, Mac, iPad]
---
Un’imbeccata di **iruben** mi ha portato a scoprire [GeoGebra](http://geogebra.org), un software matematico multipiattaforma che gira ovunque (anche su [Mac](https://itunes.apple.com/it/app/geogebra-5/id845142834?l=en&mt=12) e [iPad](https://itunes.apple.com/it/app/geogebra/id687678494?l=en&mt=8)) e fa studiare non solo la matematica, ma anche la geometria, l’algebra e un sacco di altre cose con una facilità e un coinvolgimento che onestamente non ricordo di avere visto altrove.<!--more-->

Intorno a GeoGebra ruota una comunità attiva che produce esempi di programmazione, come su [Points of Inflection](http://larkolicio.us/blog/?p=255) e [Digital Mathematics](http://www.malinc.se/math/). Avessi tempo e capacità, sarei già un entusiasta.

Più autonomamente ho scoperto anche [BioDigital Human](https://itunes.apple.com/it/app/biodigital-human-anatomy-health/id771825569?l=en&mt=8), un servizio professionale di modelli 3D di anatomia umana, dal corpo in generale ai singoli organi.

BioDigital Human è gratis per l’utilizzo base e a pagamento per andare sul professionale. L’uso gratuito comprende comunque la possibilità di scaricare su iPhone o iPad fino a cinque modelli per volta. Se ne possono scaricare a volontà, basta cancellare quelli in eccesso di cinque. Per una classe che sta studiando, per dire, il sistema respiratorio, è perfetto: tutti a scaricare i polmoni. Settimana dopo, quando si parla dello scheletro, butto via i polmoni e scarico le ossa. Sempre nel caso la scuola o il docente non decida di avvalersi di uno del vari piani a pagamento.

GeoGebra invece è gratis. Costo zero. Materiale gratuitamente disponibile su Internet per chi non lo volesse creare da sé.

Non c’è bisogno di analisi pensose per capire come questi strumenti siano un salto di qualità tremendo rispetto a quello che si fa nella scuola oggi. Qualunque ragione ci sia per farne a meno, è in verità un torto, da combattere e rovesciare.