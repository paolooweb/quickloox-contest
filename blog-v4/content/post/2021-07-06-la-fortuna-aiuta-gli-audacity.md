---
title: "La fortuna aiuta gli Audacity"
date: 2021-07-06T01:14:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [Audacity, Muse] 
---
Esempio potente della necessità del software libero. Audacity, apprezzato editor audio in sviluppo da molti anni, è stato comprato da Muse Group e, da preziosa risorsa open source, è diventato uno spyware poco raccomandabile. Gli acquirenti, infatti, [hanno aggiunto al programma](https://www.audacityteam.org/about/desktop-privacy-notice/) tutta una serie di acquisizioni di dati dell’utilizzatore, oltre a una licenza d’uso che presenta diverse ambiguità.

Il tema della legittima acquisizione dei dati degli utenti per migliorare il software, qui non attacca: ci vuole molta fantasia per spiegare come il mio indirizzo IP possa aiutare lo sviluppo di un software che lavora offline.

La comunità di Audacity è prevedibilmente [in subbuglio](https://www.reddit.com/r/audacity/) e si parla di una possibile nuova versione del programma, di nuovo open source e ancora una volta libera da sfruttamento indebito di dati individuali a scopo commerciale.

Sarebbe la migliore risposta e pure una bella lezione a chi vede nel software libero solo un’occasione di fare business senza scrupoli. Con un po’ di fortuna avremo di nuovo un ottimo editor audio libero. E magari ci ricorderemo anche di fare una piccola donazione per contribuire a tenerlo sano e benvoluto.

*Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile.*