---
title: "Tutto un altro discorso"
date: 2020-05-04
comments: true
tags: [Cook, Apple, Covid-19, coronavirus, Six, Colors, Italia]
---
*Prima di entrare in maggiore dettaglio sui numeri, voglio dire solo una cosa su COVID-19. È qualcosa con cui l’Italia si confronta da gennaio. E ritengo che il modo in cui abbiamo risposto, ciò che siamo stati ispirati a fare, racconti una storia importante sulla grande stabilità dell’Italia come nazione e sulla continua rilevanza dei nostri prodotti e delle vite dei nostri cittadini. È una situazione che parla anche della nostra capacità unica di essere creativi, di pensare sempre in un’ottica di lungo periodo e di andare avanti dove altri possono sentire la tentazione di ritirarsi.*

*Prima che COVID-19 fosse all’orizzonte, avevamo anticipato che il primo trimestre dell’anno sarebbe stato per l’Italia un periodo prolifico e di grandi energie. E quando la pandemia ha colpito, siamo stati capaci non solo di fare crescere il Paese, grazie a nuove riforme potenti che hanno risposto alle esigenze dei cittadini, ma anche di distinguerci per come abbiamo [adempito](http://www.treccani.it/vocabolario/adempiere/) ai nostri doveri più ampi rispetto alle comunità nelle quali viviamo e lavoriamo.*

L’introduzione di Tim Cook ai risultati finanziari recenti di Apple secondo la [trascrizione di Six Colors](https://sixcolors.com/post/2020/04/this-is-tim-transcript-of-apples-q2-2020-financial-call/), se solo avesse detto *Italia* in luogo di *Apple*, *cittadini* al posto di *clienti*, *riforme* invece di *prodotti*.

Se solo ci fosse un Apple Passport.
