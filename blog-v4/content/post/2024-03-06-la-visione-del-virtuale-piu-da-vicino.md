---
title: "La visione del virtuale (più) da vicino"
date: 2024-03-06T18:08:22+01:00
draft: false
toc: false
comments: true
categories: [Software, Hardware]
tags: [Vision Pro]
---
Sebbene nulla vieti di farlo in linea di principio, Apple non ha insistito più di tanto sulla realtà virtuale riguardo a [Vision Pro](https://www.apple.com/apple-vision-pro/), con preferenza per il concetto più ampio di spatial computing.

Questo rende interessante la comparsa, pochissimo tempo dopo il debutto dell’apparecchio, di un [client sperimentale per usare Vision Pro su piattaforme come SteamVR a mo’ di puro visore Virtual Reality](https://github.com/alvr-org/alvr-visionos/) (nella fattispecie, per giocare).

Il client è indipendente, open source, quindi in arrivo da qualche sviluppatore non coinvolto per questioni di business. È anche molto sperimentale se si provano a leggere le istruzioni per l’installazione. ma sta accadendo.

Se Vision Pro prende velocità e attira interesse dagli sviluppatori perfino quando è ancora al livello di sfizio appena uscito per soli americani con disponibilità, è una notizia interessante e positiva. Specie in un mondo dove quelli che spendono meno si sono accorti, ieri che [Meta era down](https://www.theguardian.com/technology/2024/mar/05/facebook-instagram-outages-disruption-meta-google), di non poter usare il loro Quest.

Viva quelli che spendono di più pur di essere liberi di accendere.