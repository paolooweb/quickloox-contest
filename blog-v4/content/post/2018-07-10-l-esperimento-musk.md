---
title: "L’esperimento Musk"
date: 2018-07-10
comments: true
tags: [Musk, Schoolwork, Apple, Wolfram, Alpha, R]
---
Ci sono sicuramente questioni più urgenti, ma credo ugualmente che la nostra scuola stia avviandosi verso un piano inclinato.

Chi esce con una cultura lo fa *nonostante* l’istituzione, principalmente grazie al tessuto sociale intorno. I test mostrano chiaramente un divario di alfabetizzazione letteraria e numerica tra nord e sud… e gran parte degli insegnanti al nord arrivano da sud.

Il problema è che la scuola è in ritardo sui ragazzi di oggi e il ritardo è destinato ad ampliarsi senza proroghe.

Da dove ripartire? I temi sono innumerevoli, primo fra tutti il giusto ricollocamento in importanza delle [arti liberali](https://www.google.com/search?client=safari&rls=en&q=arti+liberali+site:macintelligence.org&ie=UTF-8&oe=UTF-8). Qui mancano le competenze per progettare una riforma, no, una ricostruzione della scuola italiana. Però una cosetta in ambito tecnologico me la permetto.

Anzi, due: una è che Apple sta mostrando con la sua [offerta di software per il corpo insegnante](https://www.apple.com/education/teaching-tools/) quanto sia sbagliato e anacronistico il concetto delle Lim, le lavagne rifatte in elettronica visto che c’erano già quelle di ardesia. Ogni studente deve avere la propria attrezzatura e giustamente il docente la possibilità di seguire e supportare tutti e ciascuno. Scusa per vendere più iPad? Non entro neanche nel merito. Dico solo che nel 1988 la mia famiglia aveva un computer in cinque persone. Trent’anni dopo, gli stessi componenti di quel nucleo assommano undici computer.

Il numero di computer pro capite è aumentato di undici volte in trent’anni. Tra trent’anni potrebbe benissimo essere raddoppiato. La direzione dell’informatica personale sembra evidente. Altre condizioni a contorno sono discutibili. Avremo un’intelligenza artificiale a disposizione e avrà successo chi saprà demandare meglio il proprio lavoro a lei? Avremo computer inseriti nei tessuti o sottopelle? Non lo so. Scommetterei a mani basse, invece, sul fatto che saper cercare la conoscenza e utilizzare gli strumenti di rete per risolvere i problemi sarà uno *skill* molto più decisivo della soluzione dei problemi in quanto tale.

Di tutto questo si è reso conto Elon Musk, l’uomo che sta vincendo la corsa spaziale dei privati, ha portato Tesla sulla bocca di tutti e ne ha mandata una in orbita attorno a Marte intanto che studia come avviare la colonizzazione (è un elenco incompleto).

Musk il quale, avendo due figli in età scolare, [ha aperto una scuola sua](https://arstechnica.com/science/2018/06/first-space-then-auto-now-elon-musk-quietly-tinkers-with-education/) dove studiano complessivamente una ventina di ragazzi.

Tutto è ampiamente sperimentale, i programmi vengono riscritti ogni anno, può succedere che i ragazzi chiedano un lanciafiamme per un progetto, metà delle materie viene decisa dai ragazzi eccetera. Paga Musk.

L’articolo è pieno di dettagli e rimando alla lettura, davvero interessante.

Ecco: in Italia siamo ancora al compito da portare su chiavetta, che deve essere per forza Office altrimenti l’insegnante non sa che pesci pigliare (e chiamarlo insegnante in questo contesto è un bell’atto di fiducia). Mica si pretende che da domani tutti si mettano a lavorare con [Swift Playgrounds](https://www.apple.com/swift/playgrounds/).

Mi chiedo però se e in quante scuole si veda, per dire, [Wolfram Alpha](http://www.wolframalpha.com), che è gratis e va anche su un Android da cinquanta euro.

Soprattutto: in un Paese di sessanta milioni di persone, ce l’abbiamo una scuola dove assieme a venti studenti si fa sperimentazione selvaggia per capire realmente che cosa serve a un ragazzo di oggi per crescere in pienezza e sviluppare tutte le sue risorse? Ne abbiamo *una*?

Perché è già tardi. I figli di Musk stanno finendo le secondarie inferiori, cioè sono dentro Ad Astra da quasi dieci anni. Se tutto fosse clamorosamente sbagliato, immagino che l’esperimento sarebbe già terminato. Dov’è il nostro esperimento?