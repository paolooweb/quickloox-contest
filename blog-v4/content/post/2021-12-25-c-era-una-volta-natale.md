---
title: "C’era una volta Natale"
date: 2021-12-25T00:13:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [Natale] 
---
Quando ci si fermava per fare gli auguri, trovare uno spazio di tranquillità, anche vedere i parenti e magari esagerare a tavola, comunque in modo che segnava una discontinuità con la routine.

Quest’anno fermarsi è difficile; ho visto tanti che arrivano a questo momento ancora più trafelati del sottoscritto, e ben più pressati dalle cose e dalle persone.

Il mio augurio è che sia possibile, per ciascuno. È la Festa a distinguerci dagli animali. È la capacità di fermarci a renderci diversi dalle macchine, se viene esercitata consapevolmente.

Che sia Natale, come lo si preferisce, con il senso che si desidera, anche senza ove questa fosse una questione importante.

Fermarsi, però.

Grazie inoltre per condividere questo viaggio, faticoso, bellissimo.

_Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile._