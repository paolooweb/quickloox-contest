---
title: "Un cliente per servirli"
date: 2016-05-24
comments: true
tags: [PieMessage, Android, iMessage, Whatsapp, Hangouts]
---
Per quanto sia ancora largamente incompleto, trovo altrettanto largamente interessante il progetto *open source* [PieMessage](https://github.com/bboyairwreck/PieMessage) che mira a portare la messaggistica Apple su Android.

Incompleto significa che bisogna ancora lavorare sull’invio di immagini e video e che, nel momento in cui si esce dalla sicurezza di iMessage, la sicurezza su Android è zero totale. Queste le lacune più importanti ancora da colmare.

Il sistema in sé è semplice e astuto, già nato mille volte ma sempre come soluzione estemporanea, di rapida estinzione: un apparecchio OS X che funge da server e si preoccupa di inviare agli apparecchi Android i testi che gli arrivano da iMessage. PieMessage ha da subito il merito di apparire destinato a una vita più lunga e proficua.

Qualcuno si chiederà perché tanto affannarsi quando ci sono a disposizione soluzioni come Whatsapp. La domanda è pertinente; il punto è che, eccettuato Google Hangouts, le piattaforme terze di messaggistica universale sono di serie B. Compreso Skype che scende in qualità da tempo e lascia intercettare le conversazioni a chiunque lo voglia.

E poi c’è un discorso di interfaccia e usabilità. Nessun dubbio che Whatsapp mandi un messaggio a chiunque. Ma è il come, in che atmosfera, con che stile. Sì, certo, un messaggio è un messaggio, tutti i computer sono uguali, il meglio è quello che cosa meno. Sul fatto che il meglio stia sempre in basso sono poco convinto.
