---
title: "Datemi tempo"
date: 2015-07-01
comments: true
tags: [Google, Amazon, Apple, iOS, OSX, Windows, Android, Linux, Unix, orologio, Utc, leap]
---
La scorsa mezzanotte è stato aggiunto all’orario ufficiale un secondo in più, per compensare il rallentamento della rotazione della Terra. Una specie di secondo bisestile, che però non viene aggiunto a scadenza fissa come l’anno bisestile, bensì quando si ritiene opportuno.<!--more-->

Ci sono due modi per affrontare questa situazione particolare (che si verifica grossolanamente ogni paio d’anni dall’inizio dei settanta). Il primo è aggiungere un secondo al conteggio:

23:59:59<br />
23:59:60 (!)<br />
00:00:00

Il secondo è sabotare – in senso buono – l’orologio di sistema in modo che per qualche ora ogni secondo duri qualche millisecondo in più del dovuto. Così si arriva alla scadenza e il secondo supplementare è già stato aggiunto, una briciola per volta.

Google e Amazon, forse anche qualche sapore di Linux, adottano questo secondo sistema. Windows [ignorerebbe il passaggio](http://www.macworld.com/article/1137809/leap_second.html) e sistemerebbe le cose quando quando capita che l’orologio locale chieda la sincronizzazione con i server dell’ora esatta.

Android [ignorerebbe il problema](http://www.zdnet.com/article/the-time-displayed-on-most-android-phones-is-wrong/) e i suoi apparecchi, salvo personalizzazioni, starebbero quietamente in ritardo di una manciata di secondi.

Apple sarebbe a posto su Mac e iOS. Però ieri sera ho fatto il curioso e guardato l’orologio di sistema su Mac. Il conteggio non ha mostrato alcun secondo aggiuntivo.

Mi sembra improbabilissimo che Apple segua il [sistema di Google](http://googleblog.blogspot.co.uk/2011/09/time-technology-and-leaping-seconds.html) e affetti il secondo per spalmarlo lungo la giornata.

Il problema è che non trovo pezze d’appoggio. Ho individuato una pagina di [documentazione Unix](https://developer.apple.com/library/ios/documentation/System/Conceptual/ManPages_iPhoneOS/man3/time2posix.3.html) che mi ha chiarito esattamente niente. Ho scoperto il [database tz](http://www.iana.org/time-zones/repository/tz-link.html), che sarebbe usato da iOS per indicare l’ora esatta contrariamente ad Android.

Ho il sospetto che in realtà l’orologio di bordo di un computer non mostri il passaggio anche se avviene correttamente dietro le quinte, perché la cattura di un secondo bisestile è [uno hobby](http://leapsecond.com/notes/leap-watch.htm). La verità è che sono al buio.

Qualcuno è così gentile da illuminarmi? Grazie in anticipo (ma puntuale).