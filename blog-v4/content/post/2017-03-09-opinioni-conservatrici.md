---
title: "Opinioni conservatrici"
date: 2017-03-09
comments: true
tags: [Mame]
---
Ogni tanto qualcuno si arrabbia per le mie vedute sulla [conservazione dei dati nel tempo](https://macintelligence.org/posts/2017-02-20-gli-asparagi-e-l-immortalita-del-cloud/), senza capire che sono un ottimista: con le dovute precauzioni e la dovuta attenzione, questa è l’epoca migliore possibile di sempre per conservare dati.

Chi si arrabbia pensa che sia esistita un’età dell’oro in cui niente si perdeva e poi è intervenuto il digitale a complicare tutto. Ha in mente un mito e non c’è niente di male. Solo che era impossibile prima, scegliere una soluzione e poi disinteressarsene per sempre. Figuriamoci ora.

Il punto è che la conservazione dei dati ha un’infinità di sfaccettature ed è una questione estremamente complessa. Prendiamo, per esempio, i videogiochi da bar.

Per fortuna abbiamo Mame: una iniziativa libera, collaborativa e autofinanziata grazie alla quale il lavoro di migliaia di appassionati e illuminati volontari ha tenuto in vita migliaia di giochi di una volta, attraverso un emulatore efficace e versatile che [ha compiuto vent’anni](http://www.mame.net/?p=439).

Lo stesso Mame è un esempio di conservazione del software: da vent’anni funziona grazie all’attenzione continua di persone che se ne occupano, mica perché lo hanno messo in una cassetta di sicurezza. Semmai perché ne esistono milioni di copie.

E i videogiochi? Problema risolto? Le sfaccettature, dicevo, sono infinite. Certamente possiamo lanciare Pac-man (l’originale, intendo) su qualsiasi computer si voglia. Al tempo stesso, emerge la problematica della [tecnologia a tubi catodici che sta morendo](http://venturebeat.com/2017/03/03/what-the-death-of-the-crt-display-technology-means-for-classic-arcade-machines/). Tutti i videogiochi di una volta visualizzavano su un tubo catodico.

La soluzione è ovvia: passare ai cristalli liquidi. Solo che i giochi erano ingegnerizzati tenendo conto delle caratteristiche di un tubo catodico; su uno schermo a cristalli liquidi la resa è diversa da quella che avevano progettato i programmatori.

È una problematica vagamente simile a chi si ritrova una raccolta di diapositive virate al giallo. Che si sono conservate, eh. Ma appunto, il tema della conservazione dei dati non si esaurisce in una battuta. O in un disco.