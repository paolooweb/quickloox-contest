---
title: "Musiche dimenticate"
date: 2016-08-27
comments: true
tags: [Savall]
---
Ho assistito a un concerto affascinante di [Jordi Savall](https://it.wikipedia.org/wiki/Jordi_Savall), musicista di fama pluridecennale per il suo lavoro di ricerca e riscoperta delle musiche mediterranee.<!--more-->

Mi ha colpito in particolare un suo riferimento a oltre duecento canzoni raccolte dalle Colonne d’Ercole alle spiagge di Israele, che condividono una identica struttura. *Mille anni fa siamo partiti tutti dalla stessa musica*, ha detto.

Poi sono accadute molte cose, tra le quali l’invenzione del microprocessore. Qui c’è un’istantanea del banco di regia, difficilmente attribuibile a dilettanti visto il livello dell’esibizione sul palco.

Altri professionisti abbandonati al loro destino per pensare solo agli iPhone.

 ![Banco di regia per Jordi Savall](/images/savall.jpg  "Banco di regia per Jordi Savall") 