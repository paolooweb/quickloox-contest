---
title: "Le porte si chiudono"
date: 2020-03-08
comments: true
tags: [Snell, Six, Colors, PowerBook, MacBook, Pro, Folding@Home, coronavirus, Covid-19]
---
Ogni riferimento alla situazione sanitaria lombarda è puramente accidentale. Mi riferisco piuttosto al grafico su *Six Colors* che mostra il variare negli anni del [numero di porte presenti su ogni portatile Apple](https://sixcolors.com/post/2020/03/fun-with-charts-the-rise-and-fall-of-macbook-pro-ports/) con schermo da quindici pollici.

Mi tornano in mente le polemiche di quando venne deciso di eliminare la porta modem. Pareva che togliessero l’ossigeno. Oggi vorrei reincontrare qualcuno degli arrabbiati e sapere se la chiedono ancora.

Ovviamente non c’è da lambiccarsi il cervello per capire quale sarà il punto di arrivo. Il grafico mostra una tendenza chiara. Non oggi e neanche l’anno prossimo, ma le porte diventeranno zero: ogni connessione sarà pilotata in modo wireless.

Se intanto a qualcuno interessasse la situazione sanitaria, per spirito di servizio segnalo la disponibilità degli [Open Data in materia da parte dell’Unione Europea](https://data.europa.eu/euodp/it/data/dataset/covid-19-coronavirus-data) e aggiungo come [Folding@Home](https://foldingathome.org), iniziativa benemerita che mi auguro stia macinando numeri su ogni computer in ascolto, ha deciso di [dare una mano anche alla ricerca sul coronavirus](https://foldingathome.org/2020/02/27/foldinghome-takes-up-the-fight-against-covid-19-2019-ncov/).