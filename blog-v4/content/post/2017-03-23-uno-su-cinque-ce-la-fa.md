---
title: "Uno su cinque ce la fa"
date: 2017-03-23
comments: true
tags: [iCloud, password, Turkish, Crime, Family]
---
Il mese scorso mi sono ritrovato più volte l’account iCloud bloccato, da riattivare usando l’apposito codice di recupero fornito da Apple oppure attraverso la doppia autenticazione.<!--more-->

Inizialmente ho attribuito il fenomeno alle iniziative di mia figlia, che ogni tanto prova ad acquistare beni dentro i suoi giochi preferiti (senza riuscirci, ovviamente; tra Restrizioni, organizzazione in famiglia e selezione dei giochi, è tutto pienamente sotto controllo). Qualche controllo incrociato mi ha rivelato che non è così e la ragione deve essere un’altra.

Ho pensato che fosse qualche tentativo di forzare il mio account iCloud e ora leggo dell’[iniziativa della Turkish Crime Family](https://www.yahoo.com/tech/hackers-threaten-wipe-millions-icloud-101801427.html): soldi o cancelliamo qualche centinaio di milioni di account iCloud.

Non ho certo la pretesa di considerare il mio account tra quelli della vicenda: di gente che sovrarappresenta la propria rilevanza statistica ce n’è fin troppa. Mi limito a rilevare la coincidenza.

La minaccia della Turkish Crime Family potrebbe essere meno grave di questa, più grave, equivalente. Sto alla mia esperienza personale: di fronte a un attacco di questo tipo, l’account regge egregiamente e certamente nessuno è riuscito a fare con il mio account più che fare scattare i meccanismi di sicurezza.

Sarà invece che uso la mia password iCloud solo ed esclusivamente per iCloud? Apple, pare, ritiene che le coordinate di accesso ottenute dalla Family siano state prese da altri servizi, meno protetti.

Al che la responsabilità diventa automaticamente personale. Se ricicli le password e ci vanno di mezzo dati sensibili, sei uno un po’ speciale. Il mondo scriverà che gli *hacker* hanno bucato iCloud; la verità sarà che qualche centinaio di milioni di utenze possono essere fatte risalire a persone poco avvedute per quello che è l’anno 2017.