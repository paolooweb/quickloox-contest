---
title: "L’ora del dilettante"
date: 2021-09-23T00:36:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [Microsoft, Apple] 
---
Neanche lo voglio tradurre. Dovrei aprire un blog a parte e ripetere [questo tweet](https://twitter.com/panzer/status/1440703634803552256) sempre uguale ogni giorno per dieci anni.

<blockquote class="twitter-tweet"><p lang="en" dir="ltr">Love that Microsoft&#39;s hardware lab looks like someone recreated Apple&#39;s hardware lab for a home movie <a href="https://t.co/rC10sYACWv">pic.twitter.com/rC10sYACWv</a></p>&mdash; Panzer (@panzer) <a href="https://twitter.com/panzer/status/1440703634803552256?ref_src=twsrc%5Etfw">September 22, 2021</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

*Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile.*