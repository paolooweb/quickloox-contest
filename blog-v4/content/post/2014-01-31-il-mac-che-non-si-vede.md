---
title: "Il Mac che… non si vede"
date: 2014-01-31
comments: true
tags: [Mac]
---
**Rossy KK** ha inviato questo pezzo alla redazione di Macworld. Le ho scritto a titolo personale spiegandole che non mi occupo più della rivista e che, se me ne fossi occupato, lo avrei pubblicato. Lei mi ha replicato di avere inviato il pezzo a varie testate e siti, al momento senza avere risposta. Allora le ho detto che, previo il suo assenso, avrei volentieri pubblicato il testo sul mio blog personale. L'assenso è arrivato ed ecco qui l'articolo.<!--more-->

Normalmente i contributi degli ospiti appaiono come citazioni. Ma la lunghezza è notevole e quindi preferisco presentare il pezzo come testo normale. Dal prossimo paragrafo, salvo un minimo *editing* redazionale, sono tutte parole di Rossy KK. Per le quali la ringrazio. È suo anche il titolo.

***

In occasione del 30esimo compleanno del computer Macintosh, la rete è piena di articoli che riflettono sui diversi aspetti del nostro tanto amato Mac.
Oggi vorrei ragionare insieme a voi sull'utilizzo attuale del computer e il futuro che ci prospettano le tecnologie proposte dall'azienda da un punto di vista un po' insolito: quello dei non vedenti!
In particolare vorrei trattare tre argomenti che spesso si affacciano nei forum e per esprimermi meglio, provo a citare alcune mie esperienze, che possono essere condivisibili o no, ma sono reali.

**Nel mondo Apple, schiavitù o libertà per l'utente?**

All'inizio del 2009 presi il mio primo MacBook, su consiglio del mio amato marito, per avvicinarmi ad una nuova tecnologia che prometteva di funzionare bene.

Casualmente, con mia grande meraviglia, scoprii che il mio nuovo e scintillante MacBook parlava.

Il sistema operativo Leopard aveva, *on board,* un accettabile, seppur giovane, *screen reader*, simile a quello che già conoscevo su Windows.

Entusiasta della scoperta, mi rivolsi ai consueti servizi di riferimento, alla ricerca di un po' di sostegno per l'apprendimento.

La mia richiesta venne respinta, anche un po' brutalmente e quando provai ad insistere, molti mi risero in faccia asserendo che il Mac non era adatto alle nostre esigenze e quindi era inutile perderci del tempo dietro.

Erano quelli i tempi in cui rullavano i tamburi di guerra  fra Mac e PC e la partita non era solo tecnologica, ma anche sentimentale!

Un po' costretta dal voler far funzionare l'oggetto ormai acquistato, un po' portata alla disperazione dall'ennesimo portatile morto e dispiaciuta per non aver ottenuto alcuna informazione in merito a questo nuovo sistema, mi impuntai e, tramite il PC dell'ufficio, feci le mie personali ricerche.

Vorrei fosse chiaro sin dall'inizio che io non ero una smanettona o appassionata del computer ma, pian piano, traducendo di qua e di là, cominciai ad usare il mio piccolo amico e devo dire che da subito rimasi affascinata e incuriosita da questo nuovo mondo.

Conobbi numerosi nuovi amici, venni accolta in comunità internazionali stimolanti e la mia vita cambiò…

Smisi di peregrinare per i centri commerciali e gli unici sconti che mi interessavano erano quelli del Black Friday!

Oggi amo il mio computer, amo leggere e comprendere le tecnologie e, con mia grande soddisfazione, vedo che molti, anche tra i sapientoni che allora mi contestavano, sono utenti Mac e anche loro adesso stanno cercando di scoprire quel mondo che noi, utentucci, abbiamo conosciuto, in autonomia, da un bel po' senza alcun aiuto specialistico.

E allora, seppure Apple per molti sia il simbolo del sistema chiuso e limitato, a me è stata offerta la strada verso la libertà. Tramite l'approccio delle tecnologie assistive integrate *on board* presenti in tutti i prodotti Apple, finalmente io, utente non vedente, posso scegliere autonomamente e posso avere la libertà di muovermi fuori dai soliti ambienti dedicati alla nostra categoria. 

La mia esperienza Mac è cresciuta mediante l'utilizzo di *podcast* e *tutorial*, materiali tipici per l'ambiente Mac e non ho mai avuto ripensamenti sulla scelta fatta.  

Non ho mai avuto necessità di approfondire le mie scarse conoscenze tecnologiche grazie all’assistenza agli utenti (vedi Genius Bar) e i periodici aggiornamenti dei prodotti generici hanno migliorato gradualmente  l’accesso universale in quanto questo elemento, pur piccolissimo, è già integrato nel sistema operativo.

**Quale prodotto deve scegliere un non vedente?**

Subito dopo la prima esperienza con il Mac, sul mercato arrivò iPhone 3GS! 

Ormai innamorata di tutto ciò che fosse Apple, presi il mio primo iPhone più per sfida, per capriccio e, come tutti, per sentirmi alla moda.
Ricordo, eravamo io e mio marito che, per la prima volta, potevamo dire alla cassiera: “Sì, signorina, un iPhone per me e uno per mia moglie!” Lo stesso oggetto, la stessa passione e nessun titolo didattico tra noi per evidenziarci le differenze metodologiche fra vedenti e non vedenti.

Sinceramente, al di là di queste ideologie per me un po' astratte, iPhone, con  la sua sintesi vocale, ha anche implicazioni molto più pratiche, come poter creare la spesa condivisa con mio marito o partecipare alla catena dei prodotti di seconda mano che ormai coinvolge intere famiglie.

Personalmente ormai possiedo iPad mini, iPhone, MacBook Air e Mac mini. Apple in qualche modo mi ha insegnato ad usare ogni suo prodotto per quello che esso fa al meglio. per mezzo della perfetta integrazione fra software e hardware su tutti i dispositivi. 

Il passaggio dall'uno all'altro è quasi inconsapevole… Appoggio lo zaino,  saluto i partecipanti della conferenza e tiro fuori l'oggetto che in quella situazione ritengo si adatti meglio ai compiti che devo svolgere. 

Torno a casa e, senza troppo riflettere, mentre mangio qualche panino riprendo il materiale prodotto per raffinarlo e pubblicarlo con il Mac mini che ormai si spegne soltanto per il Ferragosto.

**Come sopravvivere alla rivoluzione tecnologica che ci sta sommergendo di novità?**

In Italia, sono tante le casalinghe non vedenti che, come me, sopravvivono alle rivoluzioni tecnologiche grazie al sostegno di Giacinto, Giovanni, Fabio, Guglielmo e tutti gli amici della ormai storica *community* di [Universal Access](http://www.universalaccess.it), grazie ai *podcast* e molto probabilmente grazie a voi lettori che tutti i giorni condividete *tip*, *tutorial* e notizie tramite i vari siti o Twitter.

Alla fine di tutte queste avventure, appoggiandomi un po' sulle esperienze fatte e sui passaggi cruciali  che qui  ho raccontato, posso garantirvi che non ho più paura della rivoluzione tecnologica. 

Forse sbaglio, forse sono troppo ottimista ma vivendo a pieno la ben riuscita sperimentazione da parte del team di *accessibility* di Apple, avendo assistito quasi dalla prima fila, seppur virtuale, alla presentazione di [Ariadne GPS](http://www.ariadnegps.eu) e seguendo passo passo il [viaggio di un carissimo amico verso la luce](http://www.lightheplanet.net), non desidero tornare indietro in quel mondo dei non vedenti dedicato solo ai non vedenti e spero tanto di contribuire, tramite la condivisione di informazioni utili  alla costruzione di una strada, una mappa o di un mondo dove possiamo esserci tutti.

Mi rendo conto che per riuscire dobbiamo far circolare l'informazione, perciò  mi rivolgo a voi utenti, lettori e sviluppatori: fate conoscere la nostra realtà all'interno dei nostri e vostri  siti e ambienti.

*Rossy KK* (membro dello *staff* di Universal Access)
