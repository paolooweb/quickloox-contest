---
title: "La nuova disobbedienza"
date: 2016-01-20
comments: true
tags: [Siri, iPad]
---
Fa riflettere, con tenerezza, il quadretto che mi ha disegnato **Piergiovanni** intitolato *mamma contro Siri*.<!--more-->

>Ricordi Lux, quando si parlava di “human interface”…
>Ho sentito mia madre – 83 anni – litigare (furiosamente) con Siri che, per ignoti motivi, le ha interrotto una partita a dama (giocava sull’iPad di mio fratello) per chiederle non so cosa.
>Mia madre non l’ha presa bene e gli ha intimato, nei toni da genitore, di riprendere subito la partita. Siri, servizievole, desiderava sapere se avesse bisogno d'aiuto. Lei lo ha sgridato e gli diceva: fammi giocare a dama, ho detto! Siri non capiva (e secondo me lo faceva apposta) e lei lo ha insultato. Giuro. Siri ha scritto “no no” e le ha riaperto il gioco. Io sto ancora ridendo.
>La cosa buffa è che mia madre è convinta che Siri sia una persona e le parli davvero! E mia moglie che non voleva regalarle un iPhone! :-)

A casa mia, al mattino, la piccolina raggiunge il comodino del papà dove dorme iPad e preme il pulsante Home, sapendo che a) si udirà il segnale acustico di Siri, b) il papà borbotterà nel dormiveglia qualcosa tipo *buongiorno*, c) Siri risponderà a tono con divertimento dell’infante.

A dirla tutta Siri e le varie altre incarnazioni in tema sono lontanissime da quello che desidereremmo trovare in una vera interfaccia vocale. Certo, però, la capacità infantile di trovare divertente il buongiorno dell’interfaccia, o quella senile di apostrofarla per bene quando non fa il suo dovere, sono cose che a tutte le altre interfacce sono sempre mancate. Oltre che un pezzo importante del *puzzle*, anche quando scappa da ridere a chi ascolta.