---
title: "Gorgoglio e pregiudizio"
date: 2017-07-25
comments: true
tags: [Pwa, web, Safari, Android, Medium]
---
Chi usa Apple è un fanatico privo di obiettività mentre chi usa altro invece è un genio della valutazione imparziale, lo sappiamo bene.

Ne è ennesima dimostrazione l’articolo [Il rifiuto di Apple di supportare le Progressive Web Apps va a serio detrimento del futuro del web](https://medium.com/philly-dev-shop/apples-refusal-to-support-progressive-web-apps-is-a-serious-detriment-to-future-of-the-web-e81b2be29676) appena comparso su *Medium*.

(Le Progressive Web App si basano su uno strato di JavaScript che permette funzioni simili a quelle delle app classiche, come notifiche e funzionamento offline).

L’idea generale è che Apple snobbi la tecnologia perché è interessata a preservare il mercato delle *app* native, che è lucroso.

Vero: come è vero che Apple lucra sulle *app* per il 30 percento del prezzo di vendita. Il restante 70 percento va a beneficio dello *sviluppatore*. A parte le grandi software house, ci sono sviluppatori che vivono dei proventi di App Store, altri che arrotondano, altri che non ce la fanno ma hanno potuto provarci. Se il mercato delle *app* native finisse domani, Apple perderebbe qualche miliardo del fatturato e sarebbe tipo il novanta percento di quello che è ora; molti sviluppatori indipendenti sarebbero alla fame o senza lavoro. Come si possa guadagnare dalle Progressive Web App è un problema che non riguarda Apple, ma l’ecosistema.

Di più: ad Apple interessa vendere iPhone. Se il calcolo fosse di convenienza, si impegnerebbe ad avere la migliore resa possibile delle Progresive Web App su iPhone e la gente continuerebbe a comprarlo, esattamente come fa ora dato che iPhone offre (anche) il miglior parco applicativo esistente.

Smontata la tesi principale, si può leggere l’articolo nei dettagli a scoprire che l‘autore si lamenta di non ricevere *feedback* sui *bug* che segnala e, tre righe dopo, mostra una schermata dove gli viene detto che i suoi *bug* son già noti. Oppure che dovrebbe essere importante l’86 percento di quota di mercato Android ed è ridicolo pensare che un programmatore debba imparare Swift quando riguarda solo il 14 percento dei sistemi.

Se fosse così, perché le scelte di Apple dovrebbero andare a detrimento del web? La comunità iPhone, piuttosto, si emarginerebbe da sola per divenire irrilevante. In realtã i programmatori sono pressati per imparare Swift dal fatto che su App Store, con tutti i suoi difetti, è possibile guadagnare per chi lavora bene. Su Google Play è un disastro, perché pur con tutti quei numeri agli sviluppatori va in tasca la metà di quello che arriva da iOS.

Ma non importa. Ad Apple si può attribuire una colpa e poi la colpa esattamente opposta, nello stesso articolo. Si può ignorare come funziona l’azienda prima di spiegarne le decisioni. Si possono ignorare i fatti o manipolarli. Dal gorgogliare delle sciocchezze emergerà una posizione, va da sé, imparziale e meditata.
