---
title: "Sottostime e sovrastime"
date: 2021-10-28T01:45:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [M1, Intel, John Gruber, Daring Fireball, M1 Max, Affinity, Affinity Photo, Andy Somerfield, Bill Gates] 
---
Andy Somerfield di Affinity ha dispiegato [una lunga serie di tweet](https://twitter.com/andysomerfield/status/1451859111843356676) per raccontare il rapporto tra Affinity Photo e le schede grafiche, a partire da quando i progettisti della app ne hanno deciso il progetto. Una scheda grafica ideale per Affinity Photo avrebbe avuto [tre requisiti](https://twitter.com/andysomerfield/status/1451859123885289481).

<blockquote class="twitter-tweet"><p lang="en" dir="ltr">In Photo, an ideal GPU would do three different things well: 1.) High compute performance 2.) Fast on-chip bandwidth 3.) Fast transfer on and off the GPU.</p>&mdash; Andy Somerfield (@andysomerfield) <a href="https://twitter.com/andysomerfield/status/1451859123885289481?ref_src=twsrc%5Etfw">October 23, 2021</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

Ma nessuna scheda grafica poteva vantarli tutti insieme. Per questo, dopo sei anni di sviluppo, la app uscì nel 2015 senza supportare computazione sulla Gpu.

Un paio di anni dopo, Affinity scoprì che finalmente esisteva una Gpu rispondente alle loro aspettative. Solo che la app girava su Mac e la Gpu *stava su iPad*. Fecero uscire una versione di Affinity Photo per iPad, che batteva tranquillamente qualunque versione desktop, perché a differenza di queste ultime usava la Gpu.

Così ribaltarono l’architettura di Affinity Photo desktop perché sfruttasse le Gpu presenti su desktop e portatili.

Arriviamo a oggi. Questo [l’esito delle loro prove con M1 Max](https://twitter.com/andysomerfield/status/1452623920721448963):

<blockquote class="twitter-tweet"><p lang="en" dir="ltr">The <a href="https://twitter.com/hashtag/M1Max?src=hash&amp;ref_src=twsrc%5Etfw">#M1Max</a> is the fastest GPU we have ever measured in the <a href="https://twitter.com/affinitybyserif?ref_src=twsrc%5Etfw">@affinitybyserif</a> Photo benchmark. It outperforms the W6900X - a $6000, 300W desktop part - because it has immense compute performance, immense on-chip bandwidth and immediate transfer of data on and off the GPU (UMA). <a href="https://t.co/iPg3L56y2u">pic.twitter.com/iPg3L56y2u</a></p>&mdash; Andy Somerfield (@andysomerfield) <a href="https://twitter.com/andysomerfield/status/1452623920721448963?ref_src=twsrc%5Etfw">October 25, 2021</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

>M1 Max batte la W6900X – un componente desktop da seimila dollari e trecento watt – con immense prestazioni computazionali, immensa banda passante sul chip e trasferimento immediato dei dati da e per la Gpu.

La maggior parte delle persone ancora non ha capito la portata del cambiamento che abbiamo davanti agli occhi quando apriamo una pagina del sito Apple dedicata ai Mac M1 o entriamo in un Apple Store. John Gruber [attinge](https://daringfireball.net/linked/2021/10/26/m1-max-affinity) addirittura ai [detti di Bill Gates](https://www.brainyquote.com/quotes/bill_gates_404193):

>Sovrastimiamo sempre il cambiamento nei prossimi due anni e sottostimiamo quello che avverrà nei prossimi dieci. Non lasciamoci indurre all’inazione.

Quello che succede oggi, e avrà ripercussioni interessanti, è iniziato anni fa, sottostimato da tutti. Al tempo stesso, chi predice rimonte clamorose da parte di Intel nel giro di sei mesi, sovrastima.

*Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile.*