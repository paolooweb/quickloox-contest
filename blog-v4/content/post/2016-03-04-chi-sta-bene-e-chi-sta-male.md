---
title: "Chi sta bene e chi sta male"
date: 2016-03-04
comments: true
tags: [Apple, Marchionne, Reuters]
---
[Parole](http://www.reuters.com/article/us-autoshow-geneva-fiatchrysler-apple-idUSKCN0W41QI) di Sergio Marchionne, riferite da Reuters:

>Se sentono un qualsiasi bisogno di fabbricare un’auto, consiglio loro di sdraiarsi e aspettare che passi. Malanni come questi vanno e vengono, se ne esce, non si muore.

A tanti [non sta bene](https://macintelligence.org/posts/2015-09-26-condannati-a-ripetere/) vedere nuovi ingressi nel proprio settore, solo che la sottovalutazione dei nuovi arrivati si è rivelata talvolta un grave errore.