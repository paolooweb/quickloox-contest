---
title: "Il blog è morto, viva il blog"
date: 2016-12-05
comments: true
tags: [Waxy, Baio]
---
Condivido tutto quello che ha scritto Andy Baio a proposito del proprio [ridisegno di Waxy, edizione 2016](http://waxy.org/2016/11/redesigning-waxy-2016-edition/): i blog sono morti e la maggioranza delle persone si esprime più rapidamente attraverso Facebook, Twitter, Medium, YouTube, Instagram e mille altri sistemi.<!--more-->

Però c’è qualcosa che tutte queste piattaforme non possono dare: proprietà e controllo. Se deve comparire pubblicità lo decido io, se c’è da dire qualcosa di scomodo nessuno mi può censurare, se qualcosa non funziona è colpa mia e non di chissà chi, quello che ho scritto tre anni fa scompare solo se sono io a levarlo.

Andy Baio è molto più avanti di me; devo ancora sistemare la ricerca e il feed Rss non mostra i filmati YouTube.

Però, buono o cattivo, questo è uno spazio impossibile da equivocare o da rappresentare in contesto ingannevole.

Quindi va avanti, che gli altri muoiano o meno. E un grosso grazie a chi investe qualche minuto del suo tempo per fare una visita. È sempre ospite gradito.