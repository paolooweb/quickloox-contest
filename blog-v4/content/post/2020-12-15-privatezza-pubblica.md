---
title: "Privatezza pubblica"
date: 2020-12-15
comments: true
tags: [Federighi, privacy]
---
Craig Federighi, Vice President Apple per il software, [ha preso parola alla DataProtection and Privacy Conference](https://www.youtube.com/watch?v=08IC1AZTxls&feature=youtu.be&t=2940).

Da sentire e anche [da leggere](https://www.applemust.com/essential-apples-craig-federighi-on-privacy-protection/). In Europa, per il rappresentate di un’azienda americana, sono temi difficili e dice molto del ruolo chiave di Federighi a Cupertino questo suo intervento, che ordinariamente avremmo visto tenere a Tim Cook.

>L’evoluzione nella tecnologia può accadere talmente in fretta che cinque o dieci anni possono sembrare un tempo lungo. Ma, quando considero il lavoro di Apple sulla privacy, guardo molto più lontano. Cerco di immagina come il lavoro che svogliamo possa impattare sui prossimi decenni; perfino a un secolo da oggi.

>Spero che saremo ricordati non solo per gli apparecchi che abbiamo sviluppato e per quello che permettevano alle persone di fare; ma anche per avere aiutato l’umanità a godere dei benefici di questa grande tecnologia… senza chiedere alle persone il sacrificio della privacy.

>Oggi abbiamo il potere di porre fine a questo falso compromesso… di costruire per il lungo termine non solo fondamenta tecnologiche, ma fondamenta di fiducia.

Sembrano frasi fatte. Eppure rimbombano. Vanno contro lo spirito di tre quarti delle attività commerciali di oggi per come lavorano con i dati sensibili delle persone.

<iframe width="560" height="315" src="https://www.youtube.com/embed/08IC1AZTxls" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>