---
title: "Né wireless né dieci"
date: 2017-09-16
comments: true
tags: [iPhone, keynote, Apple, Park, watch, S8, Samsung, Schiller, Stanford, Touch, pay, wireless, router]
---
Due cose che proprio disapprovo dell’[evento di presentazione della nuova tv](https://www.apple.com/apple-events/september-2017/).

Concordo totalmente [con John Gruber](https://daringfireball.net/2017/09/x_man): quella X deve essere una X, non un dieci. Per tutte le ragioni che lui elenca benissimo. Se le petizioni su Internet servissero a qualcosa, ne lancerei una. Mi limiterò a dire *X* in qualsiasi occasione pubblica, Apple Store compresi, ed esorto ognuno a fare lo stesso.

E poi [Airpower](https://techcrunch.com/2017/09/12/apple-reveals-airpower-wireless-charging-pad-coming-in-2018/). Non c’è alcuna *Air*. Nella catena degli elementi coinvolti manca persino un millimetro di spazio vuoto. L’*Air* è zero.

Apple parla di *wireless* e chiunque, compresi quelli usi a criticarla gratis in preda a demenza non senile, gli va dietro.

*Wireless* vuol dire *senza cavo*. Il termine contiene una sua elasticità; il mio Mac è in rete da qualunque posizione in casa senza cavo, ma la base AirPort è collegata fisicamente a un *router*. Magari si trova dentro un muro, però c’è: in qualunque sistema *wireless* da qualche parte può esserci un cavo.

Qui però il cavo arriva dalla presa elettrica all’apparecchio. Se l’apparecchio è lontano un centimetro dal caricatore, non carica. Che *wireless* è?

È un caricamento a contatto, per adiacenza, ad appoggio, quello che si vuole. Non è *wireless*.

Poi sarà comodissimo e pratico quando arriverà, risolverà una serie di problemi, sarà persino innovativo se davvero primo a caricare più apparecchi nello stesso momento. Mi piacerebbe molto averlo sulla scrivania. Ma non caricherà l’iPhone appoggiato sul tavolo di cucina.