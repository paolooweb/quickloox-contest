---
title: "Cattivi a percentuale"
date: 2021-07-14T01:12:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [Google, Stadia, Apple] 
---
Apple è cattiva, monopolista e avida perché chiede il quindici percento dei proventi delle app in vendita su App Store che fatturano meno di un milione di dollari.

Da ottobre, Google [chiederà a chi pubblica giochi su Stadia una percentuale del quindici percento](https://arstechnica.com/gaming/2021/07/google-rolls-out-more-generous-revenue-sharing-to-attract-new-stadia-games/), fino ai tre milioni di fatturato.

In questi mesi, ne deduco, Google deve essere qualcosa di persino innominabile, visto che la percentuale richiesta agli sviluppatori su Stadia è superiore.

*Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile.*