---
title: "Ventiquattro secondi di silenzio"
date: 2020-01-29
comments: true
tags: [Bryant, Cook]
---
I miei lati buoni li devo al basket. Ho provato a proseguire a restare fedele alla copertura di mondo Apple e dintorni, solo che oggi mi fermo comunque per ventiquattro secondi a ricordare Kobe Bryant e gli altri passeggeri sull’elicottero. Può [parlare Tim Cook](https://twitter.com/tim_cook/status/1221547854054903808) in mia vece.

<blockquote class="twitter-tweet"><p lang="en" dir="ltr">Devastated and heartbroken by the passing of Kobe Bryant. I admired his athletic prowess from afar and his humanity close up. He was an original. Our thoughts and prayers are with his family, friends and fans. RIP.</p>&mdash; Tim Cook (@tim_cook) <a href="https://twitter.com/tim_cook/status/1221547854054903808?ref_src=twsrc%5Etfw">January 26, 2020</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>