---
title: "Stringhe slacciate"
date: 2023-04-21T03:40:48+01:00
draft: false
toc: false
comments: true
categories: [Internet]
tags: [Hofstadter, Douglas Hofstadter, Fluid Concepts and Creative Analogies, Concetti fluidi e analogie creative]
---
Praticamente stavo per tradurre un capitolo intero o quasi di [Concetti fluidi e analogie creative](https://macintelligence.org/posts/2023-03-04-sublime-ignoranza/) e probabilmente finirò per farlo, ma ora mi accontento del sunto seguente.

Per una sedicente intelligenza artificiale odierna, le parole non sono concetti, ma stringhe: caratteri uno dietro l’altro, in quanto tali, senza un qualunque straccio di motivo.

Mentre noi umani facciamo una distinzione immediata tra *carbonara* e *pneobanen* e magari ci viene pure appetito, per un software di oggi non c’è alcuna differenza di partenza. Se addestrassimo un modello in cui chiamiamo la carbonara *pneobanen*, il software non farebbe una piega.

Se a una cena tra amici proponessimo di cucinare una *pneobanen*, verremmo guardati con occhi smarriti. Anche dopo avere spiegato di che cosa si tratta, la situazione non migliorerebbe.

Un software preaddestrato per chiamare la carbonara *pneobanen* non si farebbe il minimo problema.

Questo introduce anche il discorso sull’importanza della percezione nello sviluppo dell’intelligenza, ma diventa un altro capitolo da tradurre. Magari più avanti.

*Gli aggiornamenti del blog potrebbero essere erratici fino a martedì 25. Grazie per la pazienza.*