---
title: "E ora qualcosa di ragionevolmente diverso"
date: 2018-01-22
comments: true
tags: [Allen, WordPress, TextPattern]
---
Nei giorni scorsi è mancato Dean Allen, un personaggio sottovalutato nella storia di Internet.<!--more-->

La migliore [commemorazione](https://om.co/2018/01/18/dean-allen-rest-in-peace/) è arrivata da Om Malik.

Allen, tra molte altre cose, è stato l’artefice primo di [TextPattern](https://textpattern.com), un sistema di pubblicazione contenuti altrettanto sottovalutato.

Ogni installazione di TextPattern tiene viva l’opera di Allen. In quest’epoca preferisco i siti statici – e devo ancora trovare il coraggio di aggiornare queste pagine a [Octopress 3](https://github.com/octopress/octopress) – però suggerisco a tutti i fautori di WordPress di considerare una alternativa più sicura e snella, con diverse piccole sorprese interessanti rispetto al canone.