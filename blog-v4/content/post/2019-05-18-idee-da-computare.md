---
title: "Idee da computare"
date: 2019-05-18
comments: true
tags: [TEDxModena]
---
Oggi sono dalle parti di [TEDxModena](http://www.tedxmodena.it/) (e saluto molto volentieri chi fosse presente).

Faccio lo spettatore. Se fossi stato uno *speaker* avrei probabilmente tenuto una presentazione sul web che vorrei. Per dire che dobbiamo semplicemente trovare quello che merita, usarlo e mantenerci degni di quello che ci viene offerto.

Uno degli esempi che avrei fatto è WolframAlpha, il motore di ricerca computazionale che [compie dieci anni proprio oggi](https://www.wolframalpha.com/input/?i=Opening+date+of+wolframalpha). Niente tracciamenti, niente spazzatura, niente forum di scimmie urlatrici, niente *fake news*, niente pubblicità invasiva.

Cioè moltissimo. Uno specchio dello spirito con cui è partita Internet e che dovremmo ritrovare dentro noi, prima di chiedere alcunché fuori da noi.
