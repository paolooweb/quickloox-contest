---
title: "Un vuoto per volta"
date: 2013-11-05
comments: true
tags: [Internet]
---
Gratitudine sempiterna a **Matteo** per questo *tweet*:

<blockquote class="twitter-tweet"><p>&quot;The New Rules of Tech Journalism - Curious Rat&quot;: questa è per te, <a href="https://twitter.com/loox">@loox</a>. <a href="http://t.co/Hxlj2xP9sP">http://t.co/Hxlj2xP9sP</a></p>&mdash; Matteo Scandolin (@matteoscandolin) <a href="https://twitter.com/matteoscandolin/statuses/396606144380436480">November 2, 2013</a></blockquote>
<script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script><!--more-->

Le [nuove regole del giornalismo tecnico](http://curiousrat.com/the-new-rules-of-tech-journalism) sono in larga parte le vecchie regole di qualsiasi giornalismo. Anzi, più che vecchie, permanenti, che esistono da sempre ed esisteranno sempre per quanto possano degradare i costumi e chi li adotta.

*Cadrà nel vuoto*, ha commentato Matteo in un *tweet* successivo. No, non è vero che possiamo migliorare il mondo con una frase. Neanche con un articolo, neanche con una Fondazione Gates.

Preferisco una frase che è girata molto quando è mancato Steve Jobs, *making a dent in the universe*. Lasciare un segno. Non importa che funzioni poco o molto; deve essere lì però, nel caso arrivi il suo momento.

Questa è una bella occasione di scoprire la [legge di Betteridge sui titoli](http://en.wikipedia.org/wiki/Betteridge's_law_of_headlines), a ogni titolo che termini con un punto di domanda si può rispondere *no*. E levare quella fonte di titoli dai propri preferiti, per un domani migliore e letture più intelligenti.
