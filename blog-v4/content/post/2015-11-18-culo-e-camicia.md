---
title: "Culo e camicia"
date: 2015-11-18
comments: true
tags: [iPad, Pro, Pages, iMovie]
---
Sono in viaggio per lavoro: l’amministratore delegato vuole girare un video in occasione di un annuncio dell’azienda.<!--more-->

Ho il testo del suo parlato completo di appunti e annotazioni, lo stesso testo in forma elegante dentro Pages, un paio di prove da guardare insieme per fornire qualche consiglio, anche una videocamera per provare una inquadratura e iMovie per mostrare se necessario dissolvenze, effetti, perfino un piccolo montaggio lampo se fosse necessario.

In altre parole, viaggio con un iPad. Ed è comodissimo, perché sta in mano alla bisogna, si ripone facilmente e offre schermo sufficiente per compiere le operazioni di cui sopra.

Se portassi con me invece un iPad Pro, avrei certamente molta più velocità e uno schermo più grande, in cambio di un ingombro maggiore e della scomodità di tenerlo in mano senza disinvoltura.

In altre parole, un iPad Pro sarebbe meno adatto alla circostanza.

Chi sta valutando o ponendosi delle domande, deve pensare a iPad Pro come al primo iPad concepito *soprattutto* per lavorare appoggiato su un piano, sedere in posizione ergonomica e magari camicia se l’ambiente è formale.

Probabilmente non è neanche la più recente evoluzione di iPad, ma il primo risultato di una nuova categoria di oggetti, che rendono più sfumate le distinzioni tradizionali tra categorie di computer. Distinzioni che restano: a mio parere MacBook Air, come raccontavo di recente a un amico, ha persino più ragioni ora di stare in catalogo di prima.

iPad, in piedi, in t-shirt e in movimento. iPad Pro, culo e camicia.