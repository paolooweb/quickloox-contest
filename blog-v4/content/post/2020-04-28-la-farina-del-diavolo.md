---
title: "La farina del diavolo"
date: 2020-04-28
comments: true
tags: [ Crusca, Montanelli, Parioli, Zen]
---
Era veramente un altro secolo quando Indro Montanelli lanciò da *il Giornale* la sottoscrizione per salvare l’Accademia della Crusca, lasciata senza fondi pubblici a morire di fame. Partecipai e dal mio punto di vista sembra che di secoli ne siano passati anche tre o quattro.

Oggi la Crusca pubblica un [documento per la ripresa della vita scolastica](https://accademiadellacrusca.it/it/contenuti/la-ripresa-scolastica-dopo-l-emergenza-sanitaria/7925) in cui tre suoi rappresentanti dichiarano che *la scuola è un’aula e non un video*. Il timore è che *il processo educativo si esaurisca nella trasmissione di contenuti attraverso il web*. Si evidenziano i limiti della didattica a distanza e si auspica un *ritorno migliorato all’attività educativa ordinaria*.

Che quindi non sarebbe un ritorno, urgendo un miglioramento e di conseguenza un cambiamento. Però la Crusca sono loro, avran ragione.

Che la didattica a distanza abbia limiti, come qualsivoglia attività umana, sembrano scoprirlo. Un po’ come l’acqua calda. Ricordano *en passant* che *l’affollamento nelle classi è stato un provvedimento ministeriale sconsiderato* e mi balena l’ipotesi che forse anche nella scuola cosiddetta ordinaria ci sia qualcosa da sistemare. Accusano l’insegnamento a distanza di penalizzare i più svantaggiati come se fosse una sua esclusiva. Come se andare in un liceo ai Parioli di Roma o allo Zen di Palermo sia uguale (con rispetto per chi allo Zen fa fatica a insegnare e studiare).

La scuola è un’aula, non un video, come forse – fanno sapere – è gradito in certi ambienti solamente commerciali. Lo strapotere delle multinazionali nelle scuole è responsabilità precisa della scuola ordinaria, dal ministero giù fino ai presidi e agli insegnanti senza spina dorsale. L’open source sarebbe obbligo ovunque costituisca una alternativa valida e, si sappia, lo fa sempre o quasi sempre.

Il peggio è che – escludiamo pure l’estate – la scuola che è un’aula e non un video avrà passato quattro mesi a fare ZERO per ragazzi e famiglie. Senza l’insegnamento a distanza, che penalizza, svantaggia e ottunde, un quadrimestre sarebbe stato il nulla totale.

È un cretino quello che vuole la scuola solo a distanza. È un cretino chi vuole la scuola solo in aula.

La scuola non è un video e neanche un’aula. *È una rete*. Basterebbe ricordarsi di esserci stati, a scuola. Una rete che quando è il caso è bene si riunisca, e quando è il caso è bene si colleghi. La prossima volta che nevica, o c’è allarme maltempo, vedremo chi accende il computer e chi si prende un giorno di vacanza. Parliamo dei disabili che non possono raggiungere un’aula, a proposito di persone svantaggiate. Di bambini autistici che riescono a comunicare solo tramite un intermediario non umano. Degli audiolibri.

Un vero insegnante di oggi dovrebbe saper insegnare in aula e in rete. Se sa fare solo una delle due cose, potrebbe anche percepire mezzo stipendio, o lavorare per un orario doppio. Nel mondo fuori, è norma essere preparati a svolgere il proprio lavoro.

La Crusca cui ho donato tanto tempo fa si è riempita di farina del diavolo. Mi pento pubblicamente di avere contribuito a farla arrivare fino a qui.

P.S.: in fondo all’articolo si trova un florilegio di commenti che mette umanamente paura. Riporto qualche gemma.

*È tempo di riaprire le aule e le menti.*

*Se la DaD verrà imposta come in una "Dittatura a Distanza" in sostituzione della scuola essa costituirà la fine dell'insegnamento e della scuola come ambiente sociale fondamentale.*

*Eliminare la scuola in favore di piattaforme digitali ed e-campus significa minare la democrazia futura del Paese formando dei robottini o degli avatar ubbidienti e buoni esecutori, non delle persone e dei cittadini pensanti.*

*Occorre ricordare che web significa "ragnatela": il ragno tesse questa bellissima ed efficace trappola, le mosche vi rimangono invischiate e il ragno le spolpa a poco a poco.*

*Non si può assolutamente fare passare il pensiero che i saperi possano essere veicolati solo con il Dio web.*
