---
title: "Senza rete"
date: 2016-11-25
comments: true
tags: [hotseat, Waterdeep, Lords]
---
Se un gioco prevede la modalità *multiplayer* in rete, è probabilmente più interessante di un gioco che non la prevede.

Ciò premesso, butto lì il caso di un viaggio aereo con un iPad o un Mac a disposizione. Quasi sicuramente non c’è rete; con altrettanta sicurezza si può dire che soluzioni alternative, come la rete locale estemporanea o una modalità *hotseat*, saranno impraticabili. Da solo, senza rete.

A che cosa giocheresti di coinvolgente, impegnativo, non banale? Niente roba *casual* di frutta che cade, o arcade (nel senso inglese).

Nell’occasione specifica ho ripescato [Lords of Waterdeep](https://itunes.apple.com/it/app/d-d-lords-of-waterdeep/id648019675?mt=8), per esempio. Era lì che aspettava. Ma potendo scegliere ipoteticamente tra qualsiasi gioco per macOS o iOS… quale prepareresti?
