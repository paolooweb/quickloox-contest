---
title: "Che spreco di menti"
date: 2022-09-17T12:57:11+01:00
draft: false
toc: false
comments: true
categories: [Internet]
tags: [TicketOne, RyanAir, Soncini, Guia Soncini]
---
Guia Soncini è una di quelle giornaliste che vanno lette quando si è d’accordo, quando non lo si è, quando lo si è ma non del tutto, sempre. E tratta da par suo la questione della [preside di Bologna inspiegabilmente portata alla ribalta dalle cronache](https://www.linkiesta.it/2022/09/preside-scuola-cellulare/) per una decisione che decinaia e decinaia di presidi, scuole e docenti praticano da tempo e senza clamore.

La parte del problema che ci riguarda è la decisione: requisire i cellulari all’ingresso a scuola per restituirli all’uscita.

Che spreco di menti.

Menti elettroniche. Una potenza di calcolo considerevole e incredibilmente distribuita passa ore dentro ceste di vimini o cassetti polverosi. Certo, sfruttarla in modo organico e organizzato è tutt’altro che banale e va pensato, sviluppato, implementato. In teoria, trattandosi di scuola, avremmo alle spalle uno Stato. Forse qualcosa si potrebbe fare.

Nel peggiore dei casi, si ha un’idea di quanti di quei cosetti sono collegabili a schermo-tastiera-trackpad e diventano computer? Si ha un’idea di quante scuole lamentino la mancanza di un numero adeguato di computer per gli studenti? La società civile mette a disposizione senza saperlo milioni di Cpu, che vengono ordinatamente messe da parte fino all’ora di uscire.

Menti organiche. L’uso acefalo e compulsivo-dissociativo che suscita tanti allarmi e tante denunce sui media dovrebbe suggerire l’urgenza di insegnare a scuola il buon uso dei computer da tasca. Come si trovano le informazioni in rete, come comunicare, come collaborare, come fotografare, come filmare, come interrogare basi dati. Ci sono dubbi sul fatto che una educazione completa, nel 2022, comprenda la confidenza con la tecnologia e specialmente quella tascabile?

Eppure, niente. Lieto per amici e conoscenti che fanno cassa con corsi e serate dedicate ai sempreverdi pericoli della rete, proprio perché a scuola c’è il vuoto. Preoccupato perché trovo scuole di danza che insegnano, a pagamento, *balletto per TikTok*. I ragazzi rintronati da TikTok vanno a scuola e i loro cellulari vengono requisiti. Nell’intervallo, provano i balletti per TikTok. Dopo l’uscita, tornano su TikTok.

La scuola potrebbe strappare qualche adolescente a TikTok. Ma preferisce strappare i cellulari.

Menti docenti. Dicono che è difficile usare i cellulari in classe per fare lezione, perché sono piccoli e scomodi. Io dico che è difficile essere creativi e mettere a punto soluzioni per superare le difficoltà. E che il sistema scolastico non sempre premia la creatività e il talento, a valle come a monte della cattedra.

Che spreco.