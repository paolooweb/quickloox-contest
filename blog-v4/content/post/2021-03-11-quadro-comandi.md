---
title: "Quadro comandi"
date: 2021-03-11T02:42:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [Matthew Cassinelli, Shortcuts Catalog] 
---
Avendo miseramente mancato il mio proposito di fare qualcosa di buono con i Comandi rapidi entro il 2020, mi tocca rinnovare la promessa.

Probabilmente partirà dall’ottima [raccolta di Comandi rapidi](https://www.matthewcassinelli.com/sirishortcuts/) pubblicata da Matthew Cassinelli, che ha fatto parte del team di sviluppo originale e ora si dedica a mostrare e creare azioni interessanti. Una parte del sito è persino in abbonamento per chi vorrà sostenere l’iniziativa e ricevere in regalo una fornitura regolare di comandi inediti.

I Comandi rapidi sono la strada maestra per moltiplicare il rendimento di un iPhone o un iPad. E c’è sempre più bisogno di distinguersi.

*Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile.*