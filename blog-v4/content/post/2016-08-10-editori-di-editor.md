---
title: "Editori di editor"
date: 2016-08-10
comments: true
tags: [Terpstra]
---
Nessuno è meglio di Brett Terpstra quando ci si mette. Ecco per esempio la sua [pagina sugli editori di testo per iOS](http://brettterpstra.com/ios-text-editors/).

Non basta ancora, quanto meno per raggiungere un parere decisivo nel giro di poco. Eppure è un bell’inizio quando si tratta di dominare la complessità di App Store.