---
title: "Mosche cocchiere 2"
date: 2013-07-05
comments: true
tags: [Apple]
---
Apple sta lavorando per ridurre la propria dipendenza da Samsung per la fabbricazione dei prodotti – situazione che avvantaggia Samsung, mettendola in grado di avviare lo scopiazzo con grande anticipo – e ha concludo un accordo con [Taiwan Semiconductor Manufacturing Company](http://www.tsmc.com/english/default.htm) (Tmsc).<!--more-->

Secondo un tale Andy Patrizio di ITworld, [ha commesso un grosso errore](http://www.itworld.com/business/363672/apple-just-made-huge-mistake-signing-tsmc).

L’argomento? Intel è brava, c’è chi dice che nel 2014 saranno avanti a Tmsc, avranno la fabbricazione a 14 nanometri.

Tutto qui e un sacco di chiacchiere a contorno. Qualunque sia il contenuto degli accordi, si tratta di mosse che valgono decine di miliardi, comportano anni e anni di sviluppi e investimenti.

Arriva Andy Patrizio e dice che è tutto sbagliato, bastandogli un paio di paragrafi. Ci si chiede perché non li abbia offerti in visione preventiva a Tim Cook, che lo avrebbe certamente compensato in termini milionari e magari anche assegnato un ruolo di altissime responsabilità. Mica si trovano sugli alberi, esperti di strategie a questo livello.

Gira anche un sacco di gente che spara nel buio con il fucile a tappi sperando di fare abbastanza rumore. E che se ci prende diventa famosa, se sbaglia non se ne accorge nessuno.