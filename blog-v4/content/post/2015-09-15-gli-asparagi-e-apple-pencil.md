---
title: "Gli asparagi e Apple Pencil"
date: 2015-09-15
comments: true
tags: [Pencil, Jobs, Surface, iPad, stilo, Palm, Pilot, PalmPilot]
---
[Leggo](http://www.wired.com/2015/09/incredible-power-apple-inside-new-stylus/) che Apple Pencil, oltre a [tradire il pensiero di Steve Jobs](https://macintelligence.org/posts/2015-09-13-mai-filmati-prima/), sia una copia di [Surface Pen](http://www.microsoft.com/surface/en-us/accessories/surface-pen).<!--more-->

Un momento. Sarebbe una copia dell’*idea* di avere uno stilo? Evidentemente no. Lo stilo gira come minimo dagli anni novanta (PalmPilot, per fare un esempio qualunque). Sia Surface Pro che iPad Pro sono creature degli anni dieci.

Forse allora è una copia della *forma*? Diciamo per cominciare che uno stilo è uno strumento poco adatto alle macrovariazioni. Si può fare qualcosa, ma le penne finiscono per somigliarsi, come i martelli o i materassi. Apple Pencil comunque pare più lunga di di Surface Pen e ha una punta palesemente diversa.

La diversità potrebbe essere solo apparente e nascondere una copia pedestre a livello di *design*, l’insieme di forma e funzione.

Notiamo che in cima a Surface Pen sta un pulsante. Apple Pencil ha il tappo del connettore di ricarica. Sì, perché Apple Pencil ha una batteria ricaricabile tramite connettore Lightning; Surface Pen invece si alimenta tramite tre batterie, di due tipi diversi (non sto inventando, è proprio così. A livello di *design* ci sarebbe qualcosa da dire in generale).

In cima a Surface Pen, dicevamo, sta un pulsante. Sul corpo dell’unità ce ne sono altri due. Apple Pencil è priva di pulsanti.

Surface Pen riconosce diversi livelli di pressione sulla punta, come prima di lei decine di onesti stili di altrettante tavolette grafiche. Apple Pencil riconosce i livelli di pressione, appunto come qualunque prodotto della sua risma, ma anche la propria inclinazione, cosa che Surface Pen non fa.

A parafrasi di Achille Campanile, che cercò similitudini tra [asparagi e immortalità dell’anima](http://uz.sns.it/~fvenez/asparagi.html) partendo dalla loro mancanza – e concluse che effettivamente non ce n’erano – bisogna dire che l’idea di Apple Pencil come copia di Surface Pen è veramente originale.

Sta per essere annunciato Surface Pro 4. Magari vedremo una nuova Pen, può darsi con caratteristiche simili a quelle di Apple Pencil. Sarebbe un rovesciamento dell’ipotesi di partenza. Ops.