---
title: "Chi la fa l’aspetti"
date: 2023-03-05T23:36:32+01:00
draft: false
toc: false
comments: true
categories: [Hardware]
tags: [Apple Store]
---
Le esperienze personali contano fino a un certo punto. Il campo di gioco è talmente vasto che parlare di vicende vissute da milioni di persone in funzione di quello che è successo a *te* è semplicemente ridicolo. Ergo, meglio non parlarne.

Ho esperienza personale di visite in Apple Store, a volte per qualcosa di buono – come un nuovo acquisto – e a volte per qualcosa di meno buono, come un guasto o un problema altrimenti irrisolvibile.

Ho meno esperienza dei sistemi alternativi. C’è chi va dal cinese di fiducia, chi dedica tempo e denaro al fai da te, chi taglia corto sulle considerazioni e va su Android *per spendere meno*, chi cerca il laboratorio specializzato in riparazioni esoteriche condotte a colpi di microlaser eccetera.

Su tutto c’è del merito ovviamente, su tutto può andare bene o anche male.

Posso solo dire di avere fatto un danno consistente, parlando di sistemi Apple (niente di nient’altro, per fortuna) e di avere risolto presso Apple Store nel giro di due ore (andata e ritorno comprese), in modo completamente indolore, a un costo nullo.

La mia esperienza con Apple Store è fatta così. Non è neanche la prima volta. A volte valgono le regole ufficiali, altre volte tocca ingoiare la pillola amara, altre volte ancora il servizio va oltre qualsiasi cosa uno potrebbe ragionevolmente aspettarsi. Non so quante volte succeda con il cinese di fiducia.