---
title: "La prima e l’ultima volta"
date: 2023-07-11T12:01:53+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [App Store, iPhone, Gruber, John Gruber, Daring Fireball, Grannell, Craig Grannell]
---
Si può discutere di innovazione per sempre ma è indubbio che nel 2008 Apple ne ha fatta, con la creazione di App Store che oggi compie quindici anni. (E di [Newton Toolkit 1.5 che ne fa ventotto](https://web.archive.org/web/19990210063408/http://product.info.apple.com:80/pr/press.releases/1995/q4/950711.pr.rel.newton.html)).

Vari commentatori hanno pensato di ricordare la prima o le prime app che hanno scaricato. (App Store, pulsante dell’account, Acquistate, scorrere in basso fino in fondo alla lista). John Gruber [ne ha nominate otto](https://daringfireball.net/linked/2023/07/10/app-store-turns-15) e dunque posso permettermene sette:

AIM  
iTunes Remote  
Carling Tap  
Solitaire free cell  
AstroTilt Premium  
Banner Free  
Scribble Lite

(Due coincidono con Gruber).

Craig Grannell ha avuto [una idea con qualche risvolto culturale](https://mastodon.social/@craiggrannell/110672881782107415): ricostruire l’elenco delle prime circa cinquecento app con cui è partito lo Store al momento dell’annuncio.

Non posso aiutare concretamente, ma qualcun altro magari potrebbe. Il post di Grannell punta a un foglio condiviso dove aggiungere il proprio contributo.

E le ultime sette?

Dispensa Emilia  
slither.io  
Chef Express  
Doodle: Easy Scheduling  
Inquisit Player  
Wix Owner - Website Builder  
Coffeecapp

In effetti, già solo il confronto tra le più antiche e le più recenti basterebbe a giustificare un saggio sociologico.