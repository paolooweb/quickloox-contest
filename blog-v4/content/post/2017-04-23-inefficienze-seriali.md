---
title: "Inefficienze seriali"
date: 2017-04-23
comments: true
tags: [Android, iPhone, Galaxy, S8, A10]
---
Parto con [questo tweet](https://twitter.com/gbhil/status/842828040900136962), che peraltro potrebbe tranquillamente chiudere il post.<!--more-->

<blockquote class="twitter-tweet" data-lang="en"><p lang="en" dir="ltr">For everyone obsessing over which CPU is better for the Galaxy S8. <a href="https://t.co/28TTXdIDhW">pic.twitter.com/28TTXdIDhW</a></p>&mdash; Jerry Hildenbrand (@gbhil) <a href="https://twitter.com/gbhil/status/842828040900136962">March 17, 2017</a></blockquote> <script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>

Jerry Hildenbrand non appartiene ai fanatici di iPhone, ma fa il redattore ad [Android Central](http://www.androidcentral.com), per dire. Per quanto appassionato, gli è difficile ignorare i fatti, gli stessi che fanno titolare a *iMore* [Come Apple ha vinto [la battaglia sul] silicio: perché Galaxy S8 non regge il confronto core contro core con iPhone 7](http://www.imore.com/heart-of-the-machine).

>In famiglia siamo in quattro ma solo uno ha la patente. Certo, le faccende di casa si sbrigano in un quarto del tempo, ma quando serve guidare un’auto il risparmio di tempo è zero. Lo stesso vale per i processori. Se metà dei compiti è seriale e metà è parallela, un processore può avere tutti i nuclei di elaborazione che vuole, ma nei compiti seriali non avrà alcun vantaggio.

Il processore di Galaxy S8, uscito adesso, registra test di velocità blandamente migliori di quelli di iPhone 7, uscito mesi fa, quando i calcoli sono paralleli. Se i calcoli sono seriali, iPhone 7 lo lascia nella polvere.

Indovina un po’? I compiti più importanti per un computer da tasca sono seriali.

Così, gli otto nuclei di elaborazione di S8 offrono praticamente prestazioni ed efficienza paragonabili a iPhone 7 su alcune attività e su altre, invece, i quattro nuclei di elaborazione di iPhone 7 offrono risultati clamorosamente superiori.

L’articolo spiega molto bene la genesi di questa superiorità e tutto quanto essa comporta.

Io penso a quanti si metteranno entusiasti a cantare le lodi di S8 perché monta otto nuclei, contro i “soli” quattro di iPhone 7.
