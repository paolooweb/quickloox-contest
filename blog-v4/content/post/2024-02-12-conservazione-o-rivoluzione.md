---
title: "Rivoluzione o conservazione"
date: 2024-02-12T01:14:31+01:00
draft: false
toc: false
comments: true
categories: [Internet]
tags: [Super Bowl, Chiefs, 49ers, Kansas City, San Francisco, San Francisco 49ers, Kansas City Chiefs, Las Vegas, Allegiant Stadium, Lombardi, Usher]
---
C’era un ulteriore elemento simbolico nella [finalissima della National Football League](https://www.nfl.com/super-bowl/event-info/) di cui [ho anticipato](https://macintelligence.org/posts/2024-02-08-promozioni-e-bocciature/) giorni fa: si affrontavano nell’[Allegiant Stadium](https://www.allegiantstadium.com) di Las Vegas i Chiefs, i capi pellerossa, contro i 49ers, quelli del Quarantanove, intenzionati a sovvertire l’ordine costituito. Pur del tutto nominalmente, una forza di conservazione e una di cambiamento, la prima delle quali detentrice del [trofeo Lombardi](https://www.fineawards.com/articles/super-bowl-trophy-the-history-of-the-vince-lombardi-football-trophy) e fortemente intenzionata, figuriamoci, a mantenerlo.

Temi interessanti: la voglia di Patrick Mahomes, guida dei Chiefs, di riconfermarsi e la presenza nella stessa squadra del fidanzato della cantante Taylor Swift, idolo assoluto negli Stati Uniti. Dall’altra parte, i 49ers schieravano come quarterback Brock Purdy, soprannominato Mister Irrilevanza: duecentosessantaduesima scelta nella lega, ottavo quarterback, un emerito nessuno che, vistasi aperta la strada dagli infortuni dei titolari, si è dimostrato un leader. Naturalmente, la sponsorizzazione da parte di Apple Music dello show di metà partita.

L’incontro è iniziato con la prevedibile fase di studio e di messa alla prova delle difese da parte degli attacchi. A un certo punto si è capito che gli attacchi convenzionali avrebbero sortito poco effetto e bisognava fare qualcosa di speciale. Da una parte passaggi prodigiosi per la capacità quasi sovrumana di raggiungere con precisione un ricevitore in messo al traffico; dall’altra l’attesa sorniona del momento adatto per tirare fuori dal nulla passaggi profondissimi e potenzialmente letali.

Solo potenzialmente, dato che per il primo vantaggio si è dovuto attendere un record, quello di un calcio piazzato da cinquantacinque yard, mai realizzato prima in un Super Bowl.

Il primo touchdown ha avuto bisogno di un *trick play*, un’azione imprevedibile e fuori dagli schemi, con la palla che ha lasciato il quarterback per poi tornare. Il quarterback suddetto ha atteso per un tempo interminabile secondo gli standard di una partita di football, mentre il pubblico e la difesa cercavano di capire dove fosse la palla e che cosa fosse accaduto, prima di sparare un passaggio inaspettato a un ricevitore improbabile e trascurato dalla difesa in completa bambola, che ha corso fino alla meta.

La terza segnatura è arrivata sotto forma di un secondo calcio piazzato, giunto alla fine dei primi due tempi e alla fine di un lungo e infruttuoso assalto a una linea di meta continuamente negata da una grande difesa. Chiusura della prima metà della partita sul dieci a tre e pronostico apertissimo; una qualunque falla delle due difese può muovere un punteggio striminzito.

La finale è ripresa dopo lo show di [Usher](https://www.usherworld.com) nell’intervallo; musica prevedibile, grande sfoggio di coreografie e corpi di ballo; con lo sforzo profuso in idee, persone e infrastruttura per un quarto d’ora di spettacolo, in Italia si fa mezza puntata di un festival della canzone oppure una cerimonia di apertura. Così, per dire.

La squadra inseguitrice ha ricominciato il match in stato di confusione mentale e ha impiegato dieci minuti, con molti rischi, per arrivare a chiarire le idee in campo e quantomeno realizzare un calcio piazzato per portarsi sul dieci a sei e tenere apertissimo il pronostico. Gli altri non molto meglio, senza buone idee per sfruttare il momento e quindi giustamente puniti quando, persi un paio di titolari per infortunio, vanno a loro volta in confusione a due minuti dai tre quarti di partita, con un *touchdown* regalato che ha capovolto la situazione.

Costretti a inseguire, gli ex leader si sono risvegliati e hanno ricominciato a giocare fino a trovare a loro volta il touchdown dopo quattro minuti nell’ultimo periodo. Però hanno sbagliato una trasformazione facilissima e quindi hanno ripreso la guida, ma con tre punti di scarto, sedici a tredici. Partita apertissima e soprattutto assenza di dominio mentale da parte di una o l’altra squadra, quindi aperta pure all’improbabile.

Invece il ritmo è diventato quello di una partita più convenzionale. Gli inseguitori trovano il pareggio, ma non la meta, a sei minuti della fine dopo un *drive* profondo, perfetto a centrocampo e sterile arrivati in fondo.

Il fronte cambia direzione e il possesso palla viene usato per tentare di segnare all’ultimissimo momento e vanificare un tentativo di rimonta. Ma la tattica dilatoria paga solo un calcio piazzato. Tre punti di vantaggio e un minuto e cinquantatré lasciato agli avversari per pareggiare o vincere. Suspense fino alla fine, un classico del Super Bowl.

A nove secondi dalla fine gli inseguitori sono a dieci yard dalla linea di meta. Possono tentare di pareggiare con grande sicurezza o rischiare il tutto per tutto andando per il touchdown. Pareggiano e si va al supplementare.

Metà dei quindici minuti vanno per ottenere un calcio piazzato. Ventidue a diciannove. Ci ricordano che i campioni in carica non si riconfermano da diciotto anni. L’altra metà serve a realizzare un *touchdown* e portare a casa la partita, con tre secondi sul cronometro.

Così termina il Super Bowl LVIII; un paio di lampi, tanta indecisione, ma altalena di emozioni in una partita mai decisa, fino all’ultimo. Il quarterback più versatile e lucido ha avuto la meglio. Certo, chi ha sbagliato quel calcio di trasformazione se lo ricorderà per la vita.

Miglior spot pubblicitario: Pfizer che festeggia i centoventi anni di vita facendo cantare scienziati e grandi personaggi storici. Nessuno ha reclamizzato l’intelligenza artificiale, nominata solo dentro il trailer di *Cattivissimo me 4.*

Un’ultima nota, tecnica: per quanto rimanga sul mio [pregiudizio etico verso Dazn](https://macintelligence.org/posts/2024-02-08-promozioni-e-bocciature/), riconosco che lo streaming è stato di qualità eccellente. Ho anche interrotto la riproduzione alcune volte nella notte e la trasmissione è ripresa senza battere ciglio nonostante la mia visione fosse irrimediabilmente diventata in leggera differita.
