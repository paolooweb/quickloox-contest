---
title: "Provocazione Final"
date: 2011-11-07
comments: true
tags: [iWork]
---
Molti professionisti del video sono rimasti a dire poco scontenti dalla pubblicazione di <a href="http://itunes.apple.com/it/app/final-cut-pro/id424389933?mt=12">Final Cut Pro X</a> da parte di Apple.<!--more-->

Il prezzo &#232; sceso da 999 euro a 239,99 euro, ma Apple ha fatto esattamente come con iMovie anni fa: ha ricominciato da zero presentando un prodotto con innovazioni interessanti e grandi prospettive di crescita, ma mancante di moltissime funzioni presenti nella versione precedente, Final Cut Pro 7, e alle quali i professionisti erano abituati.

Proprio come con iMovie, la prospettiva &#232; &#8211; per chi non pu&#242; fare a meno di quello che manca &#8211; restare con il vecchio aspettando che il nuovo arrivi a livello di qualit&#224; equivalente, buttarsi sul nuovo per tutto quello che pu&#242; fare e tenersi stretto il vecchio nel periodo di transizione, oppure naturalmente cambiare prodotto.

Non sono preparato sul video professionale e ho visto pezzi contrastanti su Internet. Certe mancanze sono evidenti e Apple ha pubblicato <a href="http://www.apple.com/it/finalcutpro/faq/">una pagina per spiegare</a> quando verranno colmate quelle pi&#249; lamentate. D&#8217;altro canto la base di codice era stravecchia e il nuovo prodotto &#232; stato finalmente realizzato a 64 bit, pronto a crescere per i prossimi dieci anni. In molti hanno criticato alcune funzioni mancanti che non lo erano in realt&#224; ma si ottengono in modo differente. Altre funzioni invece sono evidentemente inadeguate. Eccetera.

La provocazione &#232; questa: a volte la rottura &#232; necessaria per preparare il nuovo ed evidentemente la mole di lavoro necessaria e ricostruire il prodotto non ha consentito &#8211; subito &#8211; di portarlo a livelli di qualit&#224; soddisfacenti. D&#8217;altronde, per&#242;, con la pura conservazione dell&#8217;esistente non ci sarebbero mai i salti di qualit&#224;. Pi&#249; che discutere ragioni e torti, un professionista non dovrebbe prendere freddamente atto della situazione ed elaborare una strategia, di transizione, di rottura con il passato, di abbandono del prodotto in favore di qualcos&#8217;altro, di mantenimento di Final Cut Pro 7 per un anno in attesa di vedere che cosa succede, insomma una semplice strategia qualunque, senza sentirsi necessariamente abbandonati o traditi o presi in giro quasi come se Apple non avesse lo stesso interesse a vendere Final Cut che loro hanno eventualmente nell&#8217;acquistarlo?