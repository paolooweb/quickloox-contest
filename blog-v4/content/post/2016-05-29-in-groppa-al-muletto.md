---
title: "In groppa al muletto"
date: 2016-05-29
comments: true
tags: [MacBook, Air, Pro]
---
Il mio epico [MacBook Pro 17” (inizio 2009)](https://support.apple.com/kb/SP503?locale=en_US) è fermo ai box per un problema di degrado della tastiera. In attesa di riavere il computer con una nuova tastiera (reperibile su Internet per una ventina di dollari), sto usando come muletto un [MacBook Air 13” (metà 2012)](https://support.apple.com/kb/SP670?locale=en_US).<!--more-->

A parte riprovare il piacere di digitare, mi sono ritrovato ovviamente di fronte a una superficie di schermo più piccola. La differenza si vede, però la situazione è vivibile.

Invece, devo dire che – finora, primo impatto di una sera lavorativa – dal punto di vista della velocità siamo tranquillamente alla pari. Devo ancora sollecitare la scheda grafica integrata per capire dove mostri i suoi limiti.

L’Air ha due porte Usb. Su una c’è il mio disco di sistema, trapiantato momentaneamente dal MacBook Pro 17”. Sull’altra c’è un piccolo hub trovato in un cassetto cui sono collegati iPad, iPhone e watch (oppure Time Machine, quando esco e lascio il computer a casa). L’Air, suppongo per logorio del 17” carica le loro batterie nettamente più in fretta. La differenza è evidente a occhio nudo e potrei stimarla al cento percento.

Mi aspettavo che un MacBook Air con processore i5 e schermo più contenuto potesse comportarsi onorevolmente rispetto al 17”, equipaggiato con un anacronistico Core 2 Duo. Non mi aspettavo che dimostrasse perfino qualche lato di superiorità.