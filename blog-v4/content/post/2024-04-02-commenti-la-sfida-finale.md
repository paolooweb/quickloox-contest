---
title: "Commenti: la sfida finale"
date: 2024-04-02T02:58:32+01:00
draft: false
toc: false
comments: true
categories: [Internet, Software, Blog, Hardware]
tags: [Comma, nginx, AMD, x86, ARM]
---
Mancia competente a chi risolve il mistero del mancato funzionamento dei commenti.

Il sistema, detto in modo semplice ma sono pronto a espandere la spiegazione al livello di dettaglio desiderato, manda i commenti al web server del sito (nginx), il quale li inoltra verso una app che li memorizza in una cartella dedicata.

Tutto funziona in locale e tutto ha funzionato per una sera anche sullo hosting del sito. Dal giorno dopo non c’è stato verso.

Che cosa è cambiato? Non si capisce. Sembrerebbe qualcosa a livello di hosting, visto che in locale tutto funziona. Forse qualcosa a livello di configurazione di nginx? Ma la nostra parte non è cambiata, da quando funzionava a quando ha smesso di farlo.

La app che memorizza i commenti è sempre la stessa ed è sempre al suo posto. Ho riavviato lo hosting più e più volte pensando a inghippi con cache e memoria, ma nulla.

Si accettano idee, nessuna domanda è sciocca, sono disposto a provare e riprovare qualsiasi strada tracciata già decine di volte. Va bene qualsiasi [canale di comunicazione](https://macintelligence.org/post/2024-03-28-lavori-in-corsa/). Non ho tempo ora di descrivere nel dettaglio l’intera configurazione, ma nessun problema a rispondere a qualsiasi domanda e provare qualunque test.

Grazie da subito per la pazienza e la disponibilità.