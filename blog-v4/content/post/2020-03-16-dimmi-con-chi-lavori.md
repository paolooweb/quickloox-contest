---
title: "Dimmi con chi lavori"
date: 2020-03-16
comments: true
tags: [Apple, Covid-19, coronavirus, Cook]
---
In questi giorni leggo frequenti confronti tra sistemi sanitari di nazioni differenti. Penso che siano metriche di valutazione superate.

Dipende con chi lavori e per chi lavori, più che dal passaporto.

Per esempio, questo capoverso è compreso nella [risposta di Apple alla pandemia](https://www.apple.com/newsroom/2020/03/apples-covid-19-response/):

>Tutti i lavoratori pagati a ore continueranno a essere pagati come in una situazione di normalità. Abbiamo allargato le nostre politiche di permesso pagato in modo che comprendano le circostanze personali o familiari create da Covid-19, compresa la convalescenza, l’assistenza a un congiunto, la quarantena obbligatoria o le problematiche con i figli causate dalla chiusura delle scuole.

Farei il conto delle aziende che fanno questo, fanno di più o fanno di meno, più che pensare a dove si trovino. Anche perché un dipendente Apple può trovarsi circa dovunque nel mondo e lo stesso vale per decine, centinaia di aziende multinazionali.