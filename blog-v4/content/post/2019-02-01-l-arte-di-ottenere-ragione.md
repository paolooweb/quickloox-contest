---
title: "L’arte di ottenere ragione"
date: 2019-02-01
comments: true
tags: [Cook, iPhone, SeekingAlpha, Schopenhauer]
---
È il titolo di un [piccolo trattato](https://www.adelphi.it/libro/9788845908569) in cui il filosofo Arthur Schopenhauer elenca trentotto trucchi con i quali vincere una discussione, sia quando abbiamo ragione sia quando, invece, non la abbiamo.

Poniamo un caso qualunque: Tim Cook [illustra i risultati finanziari di Apple](https://seekingalpha.com/article/4236503-apple-inc-aapl-ceo-tim-cook-q1-2019-results-earnings-call-transcript?part=single) e si sofferma sulle cause della mancata crescita del fatturato, disastro a cause del quale l’azienda ha presentato [il secondo fatturato più alto della sua storia ultraquarantennale](https://macintelligence.org/posts/2019-01-30-fai-la-cosa-giusta/).

Immaginiamo una persona ignara dell’esistenza storica di [Henry Louis Mencken](https://en.m.wikiquote.org/wiki/H._L._Mencken) e soprattutto del celebre *esiste sempre una soluzione ben conosciuta a qualsiasi problema umano; pulita, semplice e sbagliata*. Questa persona, sempre ipoteticamente, decide che il problema sono i prezzi troppo alti di iPhone e Mac e che Apple dovrebbe fare fronte a queste crisi forte di un Mac economico e un iPhone altrettanto conveniente.

La persona legge le dichiarazioni di Cook in audioconferenza con gli analisti.

>A novembre avevamo evidenziato quattro fattori che avrebbero impattato sulle nostre previsioni di fatturato: tempi diversi di lancio di iPhone, fluttuazioni dei cambi, scarsità di certi prodotti e condizioni macroeconomiche nei mercati emergenti.

>Oltre il cento percento del nostro calo globale di fatturato anno su anno si deve al nostro andamento in Cina.

>Abbiamo stabilito nuovi record in grandi mercati, tra cui […] l’Italia.

>iPhone escluso, il resto del fatturato è cresciuto del diciannove percento.

>Nonostante questo trimestre impegnativo, il fatturato annuale in Cina è in leggera crescita.

>Il fatturato Mac è il più alto di sempre.

>Fluttuazioni dei cambi. In Turchia, per esempio, la lira si è deprezzata del trentatré percento durante il 2018.

>Sussidi. Per varie ragioni, è sempre meno comune che iPhone venga venduto con pagamento dentro il contratto nel corso di due/tre anni.

>Abbiamo in corso iniziative per migliorare i nostri risultati, come facilitare la permuta di iPhone e il pagamento a rate.

>Sì, il prezzo [di iPhone] è un fattore. Ritengo che parte di esso sia dovuto alle fluttuazioni dei cambi.

>Il sussidio è probabilmente la questione più importante nei mercati dei Paesi più ricchi. Se l’ultima volta hai comprato un iPhone 6 o magari un 7, è probabile che tu lo abbia pagato centonovantanove dollari o giù di lì e, in un mondo senza sussidi, questa cifra diventa improvvisamente molto superiore.

Alla luce di quanto sopra, la persona puramente ipotetica di cui parlavamo esclama *Tim Cook mi dà ragione!*

Lascio come esercizio al lettore utilizzare a proprio discernimento i [trentotto stratagemmi di Schopenhauer](https://deliberoarbitrio.wordpress.com/2010/02/13/38-stratagemmi-schopenhauer-arte-di-ottenere-ragione/) per capire come la nostra persona (assolutamente, completamente inventata) possa sostenere di avere ragione e vedere confermata da Tim Cook sia la propria tesi che la propria soluzione.

