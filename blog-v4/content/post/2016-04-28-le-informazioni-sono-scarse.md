---
title: "Le informazioni sono scarse"
date: 2016-04-28
comments: true
tags: [Lufthansa, watch]
---
Dimostrazione: un attimo dopo essermi seduto sul volo per Monaco e un attimo prima di entrare in modalità solo aereo, su watch è apparso un messaggio: la *app* di Lufthansa, onore al merito, mi ha notificato il *gate* della coincidenza che a Monaco mi aspetta.<!--more-->

Ipotesi: viviamo in un’epoca di sovraccarico informativo.

Tesi: balle. Le informazioni sono ancora scarse e soprattutto, nella maggior parte dei casi, bisogna recuperarle in modo attivo. Siamo sovraccarichi di informazioni *superflue* semmai. Invece l’informazione funzionale, e solo quella, che ti raggiunge automaticamente, è un caso ancora rarissimo e quanto ho appena raccontato avviene in un aeroporto, uno dei luoghi più evoluti per la comunicazione. L’altro giorno la solita strada per il centro commerciale era chiusa per lavori e me ne sono accorto solo al momento di vedere i lavori. Ovviamente non avevo consultato le mappe, per andare al centro commerciale. Avrei ricevuto una montagna di informazione superflua.

Se le strade funzionassero come l’aeroporto, su watch mi apparirebbe l’avviso dei lavori in corso e della strada chiusa appena ho effettuato quelle sue manovre che rendono chiarissima la mia direzione. Ma non è così e passerà molto tempo prima che accada.

Viviamo, ancora, in un’epoca di informazioni scarse.
