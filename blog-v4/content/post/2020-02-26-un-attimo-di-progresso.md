---
title: "Un attimo di progresso"
date: 2020-02-26
comments: true
tags: [Mac, Pro, IIfx]
---
La [Technology Overview di Mac Pro](https://www.apple.com/mac-pro/pdf/Mac_Pro_White_Paper_Feb_2020.pdf).

Le [specifiche tecniche di Mac IIfx](https://support.apple.com/kb/SP203?viewlocale=en_US&locale=en_US).

Per fare un esempio abbastanza sciocco, se la mia capacità di dattilografare fosse cresciuta in proporzione nel tempo quanto le prestazioni di un Mac Pro odierno rispetto a un Mac II fx, probabilmente avrei scritto questo post in poco più di un secondo.