---
title: "Chi in orario, chi fuso"
date: 2015-04-10
comments: true
tags: [watch, Gruber, iPad, Mac, batteria, 9to5Mac, Citizen, Eco-Drive]
---
Non è questione di darsi ragione; bastava il buonsenso per dire [quello che andava detto](https://macintelligence.org/posts/2015-03-12-lassillo-dellora/) sull’[autonomia di watch](http://www.apple.com/it/watch/battery.html).

La migliore recensione per capire tutto sul computer da polso di Apple – [John Gruber](http://daringfireball.net/2015/04/the_apple_watch), *as usual* – chiarisce difatti il punto:

>Dopo oltre una settimana di uso quotidiano, Apple Watch ha più che alleviato ogni mia preoccupazione sulla possibilità di arrivare a fine giornata con una carica. Ho preso nota della carica residua ogni notte prima di andare a dormire. Di solito eravamo sopra i trenta o i quaranta punti percentuali. Una volta era a 27. Un giorno ho registrato il cinque percento. Ma era un’eccezione; ho usato l’orologio per una quantità straordinaria di prove, in modo per niente simile a un utilizzo tipico. Sono rimasto sorpreso semmai che l’orologio avesse ancora della carica. Non mi è mai successo di caricare l’orologio in un momento diverso dalla notte, mentre dormivo.

Non è l’illusione dell’appassionato che mente a se stesso pur di non tradire la causa:

>Ciò detto, la carica quotidiana, confrontata con un orologio tradizionale, è terribile. La maggior parte degli orologi al quarzo funziona per anni con una batteria da dieci dollari. Gli orologi automatici meccanici si ricaricano da soli con il movimento del polso. Ho un Citizen Eco-Drive a energia solare comprato sei anni fa e per alimentarlo l’ho semplicemente esposto alla luce; è molto preciso.

Se watch fosse un orologio, non potrebbe presentarsi con l’esigenza della carica giornaliera. Invece nasce per fare molto più di un orologio, come iPhone voleva fare molto più di un cellulare. Difatti sono un computer da tasca e uno da polso.

Il premio per il titolo più cretino va intanto a *9to5Mac*: [il miglior orologio intelligente finora, ma non un acquisto essenziale](http://9to5mac.com/2015/04/08/apple-watch-review-roundup-the-best-smartwatch-yet-but-not-an-essential-purchase/). Chi hai mai detto che sia un acquisto essenziale, non si sa. E onestamente, è un acquisto essenziale un iPad, o un Mac? Si vive anche senza computer. Due miliardi di persone vivono senza Internet e devono pensare a cose *veramente* essenziali. Che oltretutto possa essere giudicato essenziale un oggetto dipendente dal possesso di un iPhone, è un segno di chiara alterazione mentale.
