---
title: "Il nodo è tratto"
date: 2020-03-17
comments: true
tags: [Linode]
---
Per non farci mancare niente, ho iniziato quello che sarà facilmente un percorso accidentato verso la migrazione di tutti i miei affari web su [Linode](http://linode.com).

Sarà un’occasione di imparare molto e anche di combinare disastri mai pensati prima. Potrebbe verificarsi una momentanea interruzione delle pubblicazioni di questo blog o qualche problema con la posta diretta a lux *chiocciola* loox *punto* net.

Farò del mio meglio per mantenere acceso tutto e quanto nelle mie possibilità, comunque, per mantenere inalterato il ritmo di produzione di un post al giorno, come da [impegno contratto a inizio anno](https://macintelligence.org/posts/2020-01-07-ogni-maledetta-epifania/).

E racconterò come è andata. Per oggi ho semplicemente acceso un account.