---
title: "Di vincolarsi"
date: 2015-02-08
comments: true
tags: [iPad, MacStories, Viticci, iPadAir2, Mac]
---
Quando si sente la frase *ognuno fa quello che vuole*, usualmente si sta parlando di cretini, oppure di personaggi insignificanti, o di comportamenti di livello mediocre. Le persone adulte (nel senso intelligente della parola) compiono scelte e quando si effettua una scelta si escludono per forza tutte le altre alternative. Nessuno può volere una cosa sola e quindi solo un cretino (non in senso offensivo, ma descrittivo) può pensare di fare quello che vuole. Si fa ciò che si sceglie.<!--more-->

C’è una forma più elevata di comportamento adulto che è la scelta artistica: quella governata da vincoli. Spiegarsi è adulto; spiegarsi in centoquaranta caratteri quando ne avremmo desiderati cinquecento, e mantenere lo stesso livello di completezza ed efficacia comunicativa, è artistico. Musicisti, pittori, scultori, *performer* non fanno *quello che vogliono*, ma operano dentro vincoli ben precisi. Vengo dal basket, dove si viene apprezzati per l’abilità di lanciare una sfera di sette etti dentro un anello di quarantacinque centimetri di diametro messo a oltre tre metri d’altezza. Se vale qualcosa, è perché c’è il vincolo, di peso e di misure; per vincere il pesce rosso al luna park devi centrare uno dei vasetti di vetro, non una piscina.

Tutto questo per dire che mi toccano sentire perfino dei parenti sostenere che *iPad non è un computer* perché non può fare questo o quell’altro.

Sostengo che lo sia. Anche dove non fosse vero, esiste una forma elevata di comportamento adulto, quella artistica, che fa prendere un iPad e trasformarlo, rispettando i suoi vincoli, in un computer.

Si capisce e si apprezza l’arte di vincolarsi e, attraverso il vincolo, compiere grandi imprese grazie ad articoli come quello di Federico Viticci su MacStories, [Recensione di iPad Air 2: perché iPad è diventato il mio computer principale](http://www.macstories.net/stories/ipad-air-2-review-why-the-ipad-became-my-main-computer/).

Solo guardare l’articolo, che è realizzato con un iPad, fa capire – o almeno dovrebbe – un sacco di cose.

Poi lo si legge e non ci si troverà un peana a iPad, che ignori le sue limitazioni o i suoi difetti; tutt’altro. Bensì una spiegazione di come e perché iPad è una macchina straordinaria per chi capisce come sfruttare alla grande le sue comodità e le sue agevolazioni. Rispettando i suoi vincoli.

>iPad, per me, è un prodotto di forze intangibili. Il come la sua natura di portatile dissolve il divario tra computer da scrivania e computer mobile. Il come una vivace comunità di sviluppatori si impegna a costruire app che ci fanno lavorare meglio, registrare ricordi, godere di momenti indimenticabili e mantenerci produttivi e intrattenuti. Per me iPad è uno schermo che mi collega con le persone e mi aiuta con il mio lavoro, ovunque mi trovi. Trasformativo e potenziante, la cui migliore incarnazione al momento è iPad Air 2. Non per tutti, ancora migliorabile, ma assolutamente necessario per me. E, credo, per altri.

Contrariamente a [@viticci](https://twitter.com/viticci) passo molto tempo davanti a un Mac e davanti a una scrivania. Ma quando sono in movimento uso un iPad e, sebbene con capacità e risultati nettamente inferiori ai suoi, provo le stesse sensazioni:

>Liberatorio. iPad è un computer che mi consente di lavorare e comunicare al mio ritmo, ovunque io mi trovi.

Soprattutto, iPad è una macchina capace di ispirare una condotta artistica, qualità all’interno di vincoli. Comportarsi da adulti è la cosa giusta; ma comportarsi artisticamente è un gradino più sopra.