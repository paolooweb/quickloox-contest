---
title: "Prendere nota"
date: 2015-05-18
comments: true
tags: [ Rudess, iPad, musica]
---
Ogni tanto riparte il meme che vuole i professionisti abbandonati da Apple. Ogni tanto trovo anche informazioni come questa, in tema musicale.<!--more-->

<div id="fb-root"></div><script>(function(d, s, id) {  var js, fjs = d.getElementsByTagName(s)[0];  if (d.getElementById(id)) return;  js = d.createElement(s); js.id = id;  js.src = "//connect.facebook.net/it_IT/sdk.js#xfbml=1&version=v2.3";  fjs.parentNode.insertBefore(js, fjs);}(document, 'script', 'facebook-jssdk'));</script><div class="fb-post" data-href="https://www.facebook.com/robegian/posts/10205681405307489:0" data-width="500"><div class="fb-xfbml-parse-ignore"><blockquote cite="https://www.facebook.com/robegian/posts/10205681405307489:0"><p>A proposito di tastiere usate dal vivo, Jordan Rudess usa anche un iPad nei concerti dei Dream Theater, non solo come controller ma anche come generatore di suoni :-&gt;</p>Posted by <a href="https://www.facebook.com/robegian">Roberto Giannotta</a> on <a href="https://www.facebook.com/robegian/posts/10205681405307489:0">Domenica 17 maggio 2015</a></blockquote></div></div>

Apple non lo ha avvisato dell’abbandono né ha provveduto al ritiro coatto del suo iPad. Il che, per un verso, si potrebbe vedere come forma di trascuratezza verso un professionista.
