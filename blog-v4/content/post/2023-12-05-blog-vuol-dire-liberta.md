---
title: "Blog vuol dire libertà"
date: 2023-12-05T21:11:26+01:00
draft: false
toc: false
comments: true
categories: [Internet, software]
tags: [QuickLoox, Macintelligence, Hugo, git]
---
Colgo l’occasione per un aggiornamento su questo blogghino, una specie di *state of the nation* per dire come vanno le cose, che cosa è previsto, che tempo fa e il perché di alcune situazioni transitorie ma presenti e, per qualcuno, fastidiose. Sarò sintetico al massimo ma i commenti oppure [Slack](https://goedel.slack.com/) (basta una email per ricevere un invito) sono un ottimo luogo per discutere dettagli.

Ne approfitterò per dare qualche informazione semiprivata a persone che mi hanno aiutato moltissimo, continuano a farlo e potrebbero farlo in futuro. Di sicuro hanno la mia gratitudine assoluta e anche dove mi avessero semplicemente segnalato un refuso o rivolto delle critiche. A loro devo molto e quello che sento è costantemente più di quello che riesco a esprimere. Ma bando alle ciance, ecco lo stato della nazione.

Al ritorno dall’estate, una combinazione di fattori tra lavoro, famiglia e salute (non mia) ha procurato uno stop di circa tre settimane alla pubblicazione di post. In passato avevo risolto situazioni simili recuperando e pubblicando gli arretrati tutti insieme, ma qualcuno si era lamentato per gli effetti che questo aveva principalmente sul feed Rss. Così questa volta sto recuperando gradualmente, poco per volta. L’effetto collaterale è avere post che compaiono con una data arretrata rispetto a quella reale. Capisco che questo possa irritare, ma c’è una legge zero che mi sono dato tempo fa e che intendo rispettare costi quel che costi: *almeno un post al giorno*. Non ho spazio ora per spiegare il perché questo sia *importante sopra ogni altra considerazione*, magari lo farò. Qui, basti dire che è così. Lo iato tra data nominale e data effettiva verrà risolto abbastanza in breve. L’insieme delle cause che avevano causato il ritardo è ancora attivo (anche se nessuna delle cause è definitiva) e quindi si procede piano, ma si procede.

Nel tempo, Quickloox diverrà la casa di tutte le mie pubblicazioni passate oltre che future. Questo è il piano a lunghissimo termine. È molto ambizioso; del resto guardare i cantieri mi annoia e sarà così anche in futuro. Lo stesso vale per le operazioni nostalgia di tanti amici che non riescono più a guardare avanti e si accartocciano dentro i social su quello che è stato. Sono solidale e provo grande affetto, ma zero condivisione.

Prima di proseguire la pubblicazione nel tempo presente E ANCHE inserire cose del passato, occorre terminare il recupero dei vecchi post delle varie incarnazioni del blog. C’è un pregresso di circa tremila articoli di quando ci si chiamava *Ping!* che aspetta una elaborazione automatica.

Gli articoli ci sono, ma bisogna inserire nei nomi la loro data di creazione. Il formato attuale dei file inizia con la data in formato 2023-11-30 e poi il titolo: `2023-11-30-beato-chi-sa-programmare-md`.

Serve *qualcosa* che permetta di mutuare in modo automatico la data dal filesystem e appiccicarla ai nomi file. Mimmo aveva pubblicato [una procedura](https://muloblog.netlify.app/posts/2022-03-30-ping/), che non sono riuscito ad applicare per via di un problema con il comando Unix `stat` che a me non funzionava come dice lui. Sicuramente ho sbagliato qualcosa io, ma non ho ancora capito cosa e fatico molto a fare debugging quando questo porta via molto tempo. (Per Mimmo: avevi lavorato su un sottoinsieme degli articoli originali presente su Gitlab, nel repository `blog`, in una cartella `Ping!`).

Per **macmomo**, che è offerto di dare un’occhiata: tutti gli articoli di quell’epoca hanno i nomi file iniziano con un codice progressivo di tre lettere, *aaa*, *aab*, *aac* e via dicendo. Sono arrivato a *fip*. Poi c’era il titolo. Faccio tre esempi:

- fii Caccia al terrore.md
- fib Consigli per la stampa.md 
- fia Medio termine.md

Se considero il terzo articolo della lista, dovrebbe diventare

- 2011-02-19-medio-termine.md

Il codice iniziale non interessa più e la data di creazione è, per l’appunto, il diciannove febbraio duemilaundici.

Al tempo stesso, all’inizio del contenuto del file deve comparire uno header contenente i dati usati da Hugo (il motore del blog) per la pubblicazione. Lo header attuale ha questa forma:

`---`  
`title: "Medio termine"`  
`date: 2011-02-11T21:11:26+01:00`  
`draft: false`  
`toc: false`  
`comments: true`  
`categories: [Internet, Hardware]`  
`tags: [Elle, Popular Science, App Store, Advertiding Age, iPhone 4, App Store]`  
`---`

Questo illustra il lavoro da fare sull’epoca *Ping!*. Vi è poi quello sull’epoca *Script*, parentesi in cui il blog venne ospitato dall’ultimo editore di *Macworld*. L’editore naufragò e con esso sparì il suo WordPress. Colpevolmente non avevo una copia locale dei post, di cui non è rimasta traccia. **Mimmo** mi ha detto in passato di avere recuperato vari post da Internet Archive, ma non ne ho più avuto notizia. (Non è un sollecito né un rimprovero, sono io quello indietro).

Risolti questi due punti, il lavoro di recupero dei post dovrebbe essere completo per quanto si può ragionevolmente pensare di fare. Il risultato sarebbe una fotografia bisognosa di moltissima curatela (aggiornamenti, link, correzioni…) e che però costituirebbe una base, da cui prima o poi farla, la curatela.

Poi c’è la questione tecnica. Da molto tempo non aggiorno Hugo e probabilmente varrebbe la pena di farlo, per esempio. E poi, pensando di avere tempo e competenze illimitate, si potrebbero apportare piccole migliore di struttura un po’ ovunque, dalla ricerca all’archivio a tutto. Questo arriverebbe comunque dopo, la mia priorità sul fatto tecnico è più bassa di quella contenutistica.

Ancora **macmomo** aveva segnalato l’anomalia del feed Rss del blog, che non cancella i post vecchi e diventa sempre più grande… questo è bug fix ed è anch’esso una cosa da fare.

Poi ci sono tante cose che magari un lettore vorrebbe, oppure no. Commentare questo articolo è una buona occasione per creare una lista di *to do* cui prima o poi si metterà mano. Sì, le mie possibilità di pianificazione del lavoro futuro sul blog sono molto basse, considerato che che è un *labour of love* e non porta né intende portare reddito. Se ci fosse un gruppo di persone disposte a supportarlo via un Patreon o cose del genere sarebbe un’altra questione, tuttavia non sarò io a spingere in questa direzione. Questo blog non nasce per mungere denaro o inviare pubblicità a chi lo legge.

Mantenere uno spazio informativo proprio in autonomia e libertà è faticoso e costoso (pochissimo in denaro, molto in tempo e attenzione). Ma *autonomia* e *libertà* sono parole chiave: possono parlare di quello che voglio nel modo che voglio senza dover temere reazioni da un editore o dover rendere conto a un inserzionista. Sono passati tanti anni da quando proposi a [Enrico Lotti](https://www.radioactiva.it/ospiti/enrico-lotti/), direttore di *Macworld*, *se scrivessi sul sito mille battute al giorno su argomenti Apple…?* Lui mi chiese se avessi abbastanza argomenti e io risposi qualcosa tipo *il problema è l’abbondanza, non la scarsità*. Era un altro secolo, ancora non si chiamava *blog*. Ma libertà e autonomia erano concetti molto presenti. Post dopo post, più di tredicimila quelli pubblicati, li ho vieppiù imparati e assimilati. Qualche volta, se sei molto bravo e hai un pizzico di fortuna, puoi condividere un pochino di quella libertà con chi legge. Quando accade, sei ripagato di tutto, con gli interessi. È il perché vale la pena di avere speso settemilaquattrocentonovanta battute per arrivare qui.