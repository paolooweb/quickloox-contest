---
title: "La settimana automatizzata"
date: 2022-04-18T00:48:07+01:00
draft: false
toc: false
comments: true
categories: [Software, Web]
tags: [Automation April, Federico Viticci, MacStories, Comandi rapidi, Shortcuts]
---
Continuo a trovare deliziosa l’iniziativa di Automation April e ricordo che Fderico Viticci ha pubblicato su *MacStories* [un secondo giro di Comandi rapidi](https://www.macstories.net/stories/10-shortcuts-for-automation-april-week-2/), che fa seguito al [primo](https://macintelligence.org/posts/2022-04-07-aprile-dolce-comandare/).

L’aspetto che mi piace rimarcare oggi è la diversità dell’approccio da quello, tipico, stile *al mio sistema operativo preferito manca questo e quest’altro, uffa* che si legge il più delle volte.

Pensare in un altro modo, *che cosa posso aggiungere a quello che già ho?* è infinitamente più potente e immagino anche appagante.

Il numero di Comandi rapidi disponibile, in costante aumento, testimonia anche come l’approccio passivo sia perdente in partenza: le cose potenzialmente utili sono infinite e neanche se Apple fosse retta dal genio della lampada potrebbe mai uscire un sistema operativo capace di contenerle tutte (perfino lui avrebbe solo tre desideri…).

Non stiamo nemmeno grattando il fondo del barile, peraltro; in questa tornata si trova per esempio un Comando rapido per scegliere una coppia di app da lanciare a finestre affiancate su iPad, tra un elenco preselezionato. Che risponde a un’esigenza reale e concreta, non è creare un comando per il gusto di poterlo fare.

Speriamo che sia un aprile molto lungo, almeno in termini di Automation April.