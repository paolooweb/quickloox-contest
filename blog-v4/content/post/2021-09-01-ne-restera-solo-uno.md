---
title: "Ne resterà solo uno"
date: 2021-09-01T01:32:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [Apple Store, Apple Arcade, Toca Lab Plants] 
---
App Store ha numerosi difetti e Apple ha fatto in certi momenti del proprio peggio in fatto di relazioni con gli sviluppatori.

Questo non toglie che App Store, al netto dei problemi, sia fondamentalmente *sano*. Provvedimenti come quello coreano, che per legge impone la [presenza di più meccanismi di pagamento alternativi sugli Store](https://arstechnica.com/gadgets/2021/08/south-korea-law-forces-google-and-apple-to-open-up-app-store-payments/), non faranno molto più che imporre un principio e rovinare una bella idea, certamente da adeguare ai tempi, eppure bella e rivoluzionaria.

Apple lo sa e non trovo un caso che stia continuando a fare crescere Arcade, al quale – se riconosco io i cinque euro mensili di canone – probabilmente contribuiscono in molti.

Alla fine del cambiamento che inizia, App Store sarà l’equivalente della TV generalista, pieno di pubblicità, app di scarsa qualità, scarsa cura per l’utente. Arcade somiglierà alla TV on demand di adesso, con qualità, sicurezza, zero pubblicità e attenzione all’esperienza di utilizzo, chiaramente a fronte di un canone. E Apple si concentrerà sul monetizzare quello.

Intanto, prima che sia troppo tardi, ho scaricato [Toca Lab: Plants](https://tocaboca.com/app/toca-lab-plants/) per le ragazze. Loro non lo sanno, ma vogliono app pulite, divertenti, capaci di dare buone suggestioni e ispirare in modo giusto per delle bambine.

*Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile.*