---
title: "Una sconfitta cocente"
date: 2016-12-09
comments: true
tags: [Mori, Vantage, Point]
---
Purtroppo [Vantage Point](http://morrick.me/archives/7755) ha cessato le pubblicazioni. Riccardo ha scritto una disamina di che cosa ha funzionato e che cosa ha funzionato meno bene, su cui sono in parziale disaccordo; nondimeno, è dettagliata e precisa.<!--more-->

E Riccardo è uno che sa scrivere. Come prova provata, [Minigrooves](http://minigroov.es).

È un passo indietro per la qualità, per il lavoro svolto con passione, per la durata media degli spazi di attenzione.

Siamo entrati in un’epoca dove è umanamente necessario allocare almeno piccole somme di denaro al sostegno di forme di espressione più elevate del tritacarne di Facebook. Almeno un euro al mese, che è nulla per chiunque possa permettersi una Adsl.