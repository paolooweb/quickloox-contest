---
title: "Il rovescio alla riparazione"
date: 2021-09-18T01:48:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [Epson, R360] 
---
La stampante [Stylus Photo R360](https://www.epson.eu/products/printers/inkjet-printers/photo/epson-stylus-photo-r360) di Epson ha servito egregiamente la mia famiglia allargata per molti anni, tanto da non essere più in commercio.

Ora però ha mostrato un messaggio di errore: il tampone che assorbe l’inchiostro in eccesso usato eventualmente in stampa è saturo. Prima di riprendere a stampare bisogna rivolgersi al distributore.

Andare per distributori con una stampante in macchina allo scopo di cambiare un tampone di assorbimento dell’inchiostro non è esattamente una priorità di oggi e allora metto al lavoro Google. Scopro due cose.

La prima è dove si trova il tampone e che è possibile ripulirlo. Prima di optare per una stampante nuova, che potrebbe essere anche una Epson, provo a raggiungere il tampone. Mi improvviso distributore, imbraccio il cacciavite e comincio a togliere viti.

Un lavoraccio perché le viti servono solo a validare gli incastri e gli incastri sono micidiali per numero e tenuta. Però arrivo al tampone. Lo tocco con il polpastrello dell’indice, ritiro il polpastrello suddetto e lo esamino. È pulito, non c’è traccia di inchiostro.

Lo sospettavo già dopo avere scoperto la seconda cosa: la R360 mostra quell’avviso di tampone saturo indipendentemente dalle condizioni del tampone in questione. Ha un suo contatore interno, che si aggiorna secondo logiche a me tuttora sconosciute, fino al giorno in cui dirama l’avviso di tampone saturo, così, perché è ora.

Esistono nella Internet grigia programmini che si offrono di resettare il contatore della stampante. Alcuni sono a pagamento, altri probabilmente sono malware.

Non ne faccio esperienza perché per arrivare al tampone è necessario liberare dagli incastri il pannello di controllo. Il pulsante di accensione, che fa parte del pannello, è collegato alla scheda logica da un cavo piatto, lungo esattamente la distanza necessaria per arrivare al connettore, più tre o quattro millimetri.

Nel momento esatto in cui il pannello di controllo è libero di spostarsi, il cavo piatto si sfila dolcemente dal connettore. Quest’ultimo è a pochissima distanza dalla parte superiore del *case* della stampante. Provare a riconnettere il pulsante di accensione è inutile: perché il cavo piatto arrivi al connettore, il pannello deve essere a posto e così facendo si incastra solidamente al *case* in alto; ovvero, non lascia alcuno spazio di manovvra. Per come sono disposti i pezzi, servono pinze sottili e mano da chirurgo e smontare *tutta* la stampante invece delle sole parti che lasciano accesso al tampone.

La stampante è platealmente progettata in modo che la sua apertura provochi il distacco del cavo del connettore di accensione e possiede un contatore interno che inibisce arbitrariamente la stampa a un momento dato.

Le mia scarse capacità di scassinatore di stampanti hanno certamente influito e, con il senno di poi, avrei dovuto rischiare un malware e resettare il contatore prima di cercare di raggiungere fisicamente il tampone. Ma è possibile concepire un cavo troppo corto per consentire l’apertura della stampante e inventarsi il trucco del tampone saturo per compromettere volutamente il funzionamento dell’apparecchio? Perché di questo si tratta. L’intenzione era pulire il tampone (una spugnetta di gommapiuma), non manomettere chissà che cosa.

La mia famiglia allargata avrà una nuova stampante, non Epson. Il diritto alla riparazione di cui ogni tanto si farnetica dovrebbe contemplare l’esistenza, che arriva prima, del dovere di progettazione onesta.

*Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile.*