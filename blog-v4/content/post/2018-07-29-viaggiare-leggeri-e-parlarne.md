---
title: "Viaggiare leggeri. E parlarne"
date: 2018-07-29
comments: true
tags: [Freelancecamp]
---
Il 10 ottobre sarò a Roma per tenere un intervento all’edizione locale di [Freelancecamp](https://www.freelancecamp.net).

Il tema è il lavoro in mobilità e toccherò le questioni hardware solo marginalmente. Gli apparecchi mobili esistono seriamente da vent’anni, chiunque ha almeno un computer in tasca ed è più difficile stare fuori che dentro la rete.

Lato software è tutt’altra faccenda. Iniziamo sì e no adesso a capire come è *veramente* meglio strutturarsi per lavorare in mobilità. Che nella mia ottica significa due cose: lavorare in modo intercambiabile su qualsiasi apparecchio e dotarsi del software meno intrusivo e più leggero possibile.

Perché mobilità non è più spostarsi, ma alleggerirsi.

Da queste premesse nascono alcune considerazioni come il ricorso a strumenti poveri, formati snelli, versatilità invece di potenza, lavoro affidato quandunque possibile al computer invece che manuale.

Sto mettendo a fuoco le idee e sono interessato a leggere le considerazioni di altri, sul lavorare in mobilità alla fine degli anni dieci.

Ringrazio ciascuno di cuore da subito e prometto di rendere a chiunque il proprio merito; l’intero Freelancecamp sarà trasmesso in streaming e sarà agevole verificare.
