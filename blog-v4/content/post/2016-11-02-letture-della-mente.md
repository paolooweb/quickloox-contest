---
title: "Letture della mente"
date: 2016-11-02
comments: true
tags: [MacBook, Pro, Touch, Bar, macOS]
---
I [nuovi MacBook Pro](http://www.apple.com/it/macbook-pro/) si venderanno oppure no? Michael Tsai ha prodotto una [pagina chilometrica di critiche](http://mjtsai.com/blog/2016/10/27/new-macbook-pros-and-the-state-of-the-mac/) dell’annuncio. La stragrande maggioranza di pensatori, esperti, soloni ha parere negativo. È dunque probabile che si venderanno bene.

La Touch Bar è veramente utile? Consiglio a tutti la lettura delle [pagine della documentazione](https://developer.apple.com/library/content/documentation/UserExperience/Conceptual/OSXHIGuidelines/AbouttheTouchBar.html#//apple_ref/doc/uid/20000957-CH104-SW1) per sviluppatori all’interno delle linee guida di progetto dell’interfaccia utente di macOS.

Tecniche di lettura della mente (dei critici e dei progettisti di Apple) per leggere il futuro. Funzionano meglio delle sparate acchiappaclic.