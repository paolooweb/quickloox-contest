---
title: "Classica tentazione"
date: 2019-04-10
comments: true
tags: [WoW, Classic, World, Warcraft]
---
Non avrò tempo. Ignoro se il mio Mac mini potrà farlo funzionare degnamente. Diffido delle operazioni nostalgia, specie nel mondo digitale.

Eppure, che voglia di vedere [World of Warcraft Classic](https://www.extremetech.com/gaming/280096-wow-classic-will-launch-summer-2019-with-unique-strategy-to-capture-players). Il vero gioco di ruolo di massa online, dove potevi attraversare un continente, ma correndo (cavalcando semmai) e ci mettevi magari una notte vera. Niente teletrasporti, niente punti di raccolta giocatori, niente generazioni automatiche di party per entrare in un *dungeon* ma dialogo con gli altri giocatori online.

Penso che neanche WoW Classic avrà più veramente quel sapore. Però, che tentazione.