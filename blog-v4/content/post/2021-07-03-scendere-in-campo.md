---
title: "Scendere in campo"
date: 2021-07-03T00:40:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [iPhone, Giardino delle Farfalle, Cessapalombo] 
---
Se a qualcuno capiterà di andare verso il [Giardino delle Farfalle](https://giardinofarfalle.it/) sopra Cessapalombo: qualunque cosa dicano le mappe, restare sulla strada asfaltata. Ci dicono che l’errore commesso da noi lo hanno compiuto in tanti per via di una particolarità del percorso e noi ce la siamo cavata bene, ma non è garantito.

Anche perché è un posto (delizioso) dove la frase *non c’è campo* ha ancora un significato concreto e, se capita di dover inviare un messaggio o telefonare prima di scendere in pianura, per farlo bisogna impegnarsi veramente.

*Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile.*