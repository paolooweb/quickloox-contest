---
title: "Un uomo solo al Comando rapido"
date: 2023-10-28T16:39:21+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [Drang, Dr. Drang, Comandi rapidi, Shortcuts]
---
Semplice, efficace, al punto: Dr. Drang imposta nei Comandi rapidi [l’apertura di una nuova nota da riempire sotto dettatura](https://leancrew.com/all-this/2023/10/dictation-automation/).

Oramai consiglio il suo blog a chi mi chiede un libro per imparare i Comandi rapidi. Non conosco nessuno con la stessa capacità di sintesi e di semplificazione della complessità.

C’è sempre anche uno scopo: in questo caso, dice Drang stesso, *il risultato è molto più affidabile che usare Siri*. Neanche fine a sé stesso.