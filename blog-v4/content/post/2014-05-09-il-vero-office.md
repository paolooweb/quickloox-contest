---
title: "Il vero office"
date: 2014-05-09
comments: true
tags: [Goodreader]
---
C’erano molte cose da dire, prevale l’attualità: è uscita la [nuova generazione di Goodreader per iOS](https://itunes.apple.com/it/app/goodreader-4/id777310222?l=en&mt=8).<!--more-->

Goodreader è una di quelle pochissime *app* nate quasi in contemporanea con iPad e ancora oggi al primo posto, o nei dintorni, per le loro competenze, dopo un milione di altre *app* e cinquanta miliardi di *download*.

Goodreader legge i Pdf, poi li annota pure; inoltre li sincronizza con tutti i sistemi *Dropbox-like* più diffusi. Si collega agevolmente con *server* in qualsiasi protocollo.

I Pdf non sono tutto; passa un file .zip? Goodreader lo scompatta. Un file di formato dubbio? C’è caso che Goodreader riesca a visualizzarlo. Serve un editor di testo veloce? C’è.

La nuova generazione di Goodreader, prima cosa buona, è diventata *app* universale, un solo acquisto per tutti gli apparecchi.

Secondariamente ma solo come elenco, ora la *app* *aggiunge, rimescola, cancella, ruota, estrae e spedisce pagine singole, spezza e combina file*. Si aggiungono un po’ di anteprime e miniature in più; è persino possibile creare un file Pdf vuoto per scriverci dentro ove servisse.

Riassumendo, l’ultimo degli iPod touch – purché equipaggiato con iOS 6 o superiore – dispone di una gamma di strumenti per trattare Pdf e dintorni che Anteprima su Mac non arriva a pareggiare.

Qualcuno sbufferà perché l’autore di Goodreader non offre la nuova generazione del programma come aggiornamento gratuito. C’è da scaricare una *app* nuova di zecca e anche migrare dentro il nuovo Goodreader impostazioni e documenti di quello vecchio. Un pulsante provvede automaticamente a tutto.

L’autore sostiene che Goodreader 4 sia in offerta a soli 2,69 euro per un tempo limitato. Non so se sia vero o quanto sia limitato il tempo; ho comprato immediatamente. E così spero di chiunque legga, se fa uso significativo di Pdf su iPad e compagni.