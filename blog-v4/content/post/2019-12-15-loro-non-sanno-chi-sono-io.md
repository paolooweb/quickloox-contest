---
title: "Loro non sanno chi sono io"
date: 2019-12-15
comments: true
tags: [Safari, Iit, privacy, Edge, Firefox, Intelligent, Tracking, Prevention]
---
Il valore degli annunci pubblicitari pubblicati su Safari [sarebbe sceso del sessanta percento](https://www.theinformation.com/articles/apples-ad-targeting-crackdown-shakes-up-ad-market) in due anni, da quando Apple ha iniziato a dotare Safari della Intelligent Tracking Prevention, protezione contro il tracciamento indebito della navigazione web a fini di profilazione.

Il resto del mercato pubblicitario è rimasto sostanzialmente quello che era e la cosa potrebbe giovare a Google o a Facebook, che di misure a protezione della privacy di chi naviga non ne mettono, o le mettono in modo che ad approfittarne siano programmaticamente in molto pochi.

Intelligent Tracking Prevention non impedisce la profilazione, che ha un senso; se proprio dobbiamo vedere pubblicità, meglio che sia centrata sui nostri gusti invece che sparata a caso. Il punto è come si vengono a conoscere i gusti.

Se facciamo la spesa da CostoMeno e abbiamo la carta fedeltà, CostoMeno fa ragionamenti su quello che compriamo e propone magari sconti specifici che ci potrebbero interessare più di altri. Ci sta. Se però CostoMeno ci mettesse alle calcagna un investigatore che spiffera che cosa compriamo quanto entriamo da ComproTutto, sarebbe poco simpatico. È quello che fa Intelligent Tracking Prevention: il sito che vende pantaloni guarda quello che facciamo sulle sue pagine, ma non può (più) seguirci quando ci trasferiamo sul sito che vende giochi da tavolo.

Cosa che invece è diventata la regola sul web, dove un sito potrebbe avere anche decine di *tracker* impiccioni che rivelano i nostri comportamenti ad altrettanti terzi incomodi.

Il rapporto linkato a inizio post spiega che le aziende hanno a disposizione altri mezzi per creare pubbblicità mirata. Per esempio, seguire il cliente sul proprio sito, che è cosa onesta e ragionevole; magari seguirlo bene, in modo che sia disposto a condividere più informazioni per essere servito meglio.

Così vincono tutti in un gioco più onesto, più equo per le piccole realtà che mancano della malizia tecnica e strategica dei colossi del web. Safari e Intelligent Tracking Prevention riportano l’anarchia del web a un livello più accettabile e simile a quello del mondo fisico; mossa di questi tempi assai apprezzabile. È giusto che chi naviga goda di un ragionevole rispetto della propria privacy e ci si arriverà in molto tempo, con pianto e stridore di denti da parte di aziende senza scrupoli.

Però la marcia è finalmente cominciata.
