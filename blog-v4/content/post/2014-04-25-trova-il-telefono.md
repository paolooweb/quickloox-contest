---
title: "Trova il telefono"
date: 2014-04-26
comments: true
tags: [iPhone]
---
Circola una vulgata secondo la quale il computer da tasca sarebbe usato per telefonare, mandare messaggini, leggere la posta, andare su Facebook e poco altro; questo potrebbe benissimo essere vero per una quota molto significativa della popolazione.<!--more-->

La vulgata però racconta anche come la pubblicità Apple spinga il concetto.

Questo è il più recente [*spot* pubblicitario di iPhone](https://www.youtube.com/watch?v=ODmfmUWqlSA). Forse sarà diretto a decine di milioni di persone ansiose di telefonare e andare su Facebook; tuttavia vorrei avere a disposizione un marziano disposto a guardare lo *spot* e, ignorando del tutto le funzioni di iPhone, desumerle dal filmato. Dico che del telefono non avrebbe nemmeno il sospetto.

<iframe width="560" height="315" src="//www.youtube.com/embed/ODmfmUWqlSA" frameborder="0" allowfullscreen></iframe>
