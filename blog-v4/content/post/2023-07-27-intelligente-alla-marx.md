---
title: "Intelligente alla Marx"
date: 2023-07-27T16:13:37+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [Marx, Groucho Marx, OpenAI, ChatGPT, Mensa, OpenAI]
---
Il [Mensa](https://www.mensa.it), l’associazione nata per raccogliere la popolazione capace di raggiungere o superare il novantottesimo percentile nella scala del quoziente di intelligenza, ha milleottocento iscritti italiani.

Semplifichiamo e diciamo, anche se non è la stessa cosa, che grosso modo il Mensa aggrega il due percento più intelligente degli italiani.

Gli italiani aventi diritto di voto e residenti in Italia nel 2022 erano suppergiù quarantasei milioni.

Milleottocento è circa il quattro per mille di quarantasei milioni; il due percento di quarantasei milioni è novecentoventimila.

Milleottocento è circa il tre per mille di novecentoventimila.

Mi scuso per il balletto di cifre e arrivo alla conclusione: su mille teorici aventi diritto a iscriversi al Mensa, lo fanno in tre. Ricordo che parliamo del novantottesimo percentile di quoziente di intelligenza, mica di caciaroni improvvisatori.

*Le persone più intelligenti d’Italia non si iscrivono all’associazione fatta per loro*. Ci sarà sicuramente qualche motivo ben fondato.

Il comico Groucho Marx era intelligentissimo e difatti, nel dare le dimissioni dal Friar’s Club di Hollywood, [dichiarò](https://www.linkiesta.it/2013/08/le-migliori-battute-di-groucho-marx-36-anni-dopo/) *non vorrei mai far parte di un club che accettasse tra i suoi soci uno come me*. Perfetta sintesi di quanto espresso faticosamente a proposito del Mensa.

È così che volevo commentare la decisione, un po’ più seria, presa da OpenAI di [chiudere lo sviluppo del loro strumento per distinguere i testi scritti artificialmente da quelli umani](https://www.businesstoday.in/technology/news/story/openai-shuts-down-tool-to-detect-ai-written-text-due-to-low-accuracy-391269-2023-07-26#).

*En passant*, vuol dire che il software – così come l’azienda – non è in grado di capire se si sta addestrando su testo da egli stesso prodotto, ovvero se sta imparando il proprio bagagli di conoscenze, già imparato.

Tornando a un tono più faceto, possiamo dire che ChatGPT accetta tranquillamente l’idea di imparare su testi scritti da sistemi come lui.

Siamo a livelli altissimi, proprio alla Groucho Marx. Purtroppo, involontari.