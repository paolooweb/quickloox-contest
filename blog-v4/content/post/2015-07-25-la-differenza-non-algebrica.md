---
title: "La differenza non algebrica"
date: 2015-07-28
comments: true
tags: [iPhone, Stefano, India, iPhone6, Gandhi]
---
La storia che mi ha inviato Stefano. Da **grassetto** a **grassetto** sono parole sue.<!--more-->

**Sto seguendo da lontano** lo sviluppo del tentativo italiano di commercializzare uno smartphone di *alta gamma* sotto i 300 euro. I promotori dell’iniziativa sostengono che al giorno d’oggi la tecnologia rende disponibile questi margini operativi ed è assurdo spendere piu soldi che poi, secondo loro, finiscono nel marketing e nella catena distributiva.

La mia esperienza con iPhone 6: torno in Italia per un mese di vacanza e come sempre accade in questi casi il telefono vola dalla tasca frantumando il vetro il giorno prima del rientro in India.

È noto che nel Paese di Gandhi non esistono ancora gli Apple Store ufficiali: esistono migliaia di centri di riparazione di fortuna.

Ho optato per un Apple Store in Italia vicino a casa sperando nel miracolo di una riparazione al volo.

Due ore di attesa nello Store, chiacchierata con il personale e caffè, 100 euro di riparazione perfetta.

Telefono come nuovo e nuovo *bumper* contro eventuali future distrazioni.

Utilizzo iPhone anche per motivi aziendali e averlo sempre con me e funzionante al 100 percento dà valore ai miei 800 e passa euro spesi.

Ho sentito che l’assistenza prevista per quello italiano da 300 euro sarà un *pick to door service*: **ecco la differenza non algebrica tra 800 e 299**.