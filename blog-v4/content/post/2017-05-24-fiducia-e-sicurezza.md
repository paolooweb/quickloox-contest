---
title: "Fiducia e sicurezza"
date: 2017-05-24
comments: true
tags: [Panic, Handbrake, Steven, Gatekeeper]
---
Apple ama i sistemi chiusi e allora inventa Gatekeeper per condizionare la mia libertà di fare quello che voglio sul mio computer e installare quello che mi pare come mi pare.<!--more-->

Certo. Poi succede che per tre giorni il sito di Handbrake distribuisca una volta sì e una volta una app insicura per via di una intrusione. Steven di Panic Software scarica Handbrake proprio in quei tre giorni, proprio dal server sbagliato, e in men che non si dica i pirati informatici si guadagnano l’accesso al codice sorgente di varie applicazioni Panic.

Steven racconta l’accaduto [sul blog dell’azienda](https://panic.com/blog/). Una lettura istruttiva.

Hai ragione a lamentarti di Gatekeeper solo se sei più bravo di Steven. In particolare, sei sicuro che i tuoi dati sensibili – carte di credito magari – sarebbero rimasti inviolati? Sei certo che saresti riuscito a determinare i danni dell’attacco? Pensi che avresti saputo eliminare il problema da solo?

Se la risposta è sempre no, ti conviene scaricare app da Mac App Store e il più possibile solo da lì. Dici che quell’altro sito è sicuro e garantito? Sì, normalmente sì. Tieni conto che ne basta uno, di attacco che va a segno. Uno solo.