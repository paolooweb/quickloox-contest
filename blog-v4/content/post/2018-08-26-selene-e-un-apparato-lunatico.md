---
title: "Selene e un apparato lunatico"
date: 2018-08-26
comments: true
tags: [Flavio, Selene, Huawei, P20, Leica, Google, iPhone]
---
Ricevo da **Flavio** e con gratitudine pubblico. Niente da aggiungere.

§§§

##Certo, costano di meno

Selene è una ragazza giovane, intelligente, acuta. Laureanda all’università, ha sempre padroneggiato la tecnologia, tanto che in casa è il riferimento famigliare per i vari problemi informatici che possono affliggere le dotazioni domestiche.

Il suo smartphone è un [Huawei P20 Paro](https://consumer.huawei.com/en/phones/p20-pro/), avuto in regalo dal proprio fidanzato grazie anche agli imponenti sconti di cui beneficia l’apparato. Prima aveva un un Huawei G620S con Micro SD su cui salvava la messe di fotografie che le piace scattare e collezionare.

Con il passaggio al P20 Pro, si è pensato, la sua passione per le fotografie non potrà che beneficiarne. Fotocamera performante prodotta da Leica, spazio su disco incorporato nell’apparato più che abbondante (128 gigabyte) e poi un sacco di funzionalità carine, anche se non proprio inventate da Huawei.

Non appena configurato, il nuovo smartphone cinese propone, dopo avere impostato il Pin di sicurezza, di effettuare la scansione del volto per poter rimuovere il blocco schermo con il riconoscimento facciale. Selene, entusiasta, ottempera.

Per sicurezza, anche se certamente non servirà, il dispositivo propone anche di fare la scansione dell’impronta digitale in modo tale di avere un metodo rapido alternativo di sblocco, e anche questa ricca funzione viene diligentemente configurata dalla brava Selene.

Nei tre mesi trascorsi dal giorno in cui è entrata in possesso del suo nuovo gioiellino, però, pur non essendo i delicati lineamenti della nostra amica certamente cambiati, il P20 Pro ha inspiegabilmente progressivamente smesso di riconoscerla. Durante i primi giorni, lo sblocco con riconoscimento facciale funzionava in media il 50% delle volte ma poi, piano piano, il telefono ha dimenticato i lineamenti della bella Selene, tanto che lei, che di pazienza ne ha in quantità, da tempo preferisce sbloccare il dispositivo tramite impronta digitale. Che però, ci racconta, viene anch’essa riconosciuta *una volta sì e una volta no*, tanto che molto spesso si ritrova a dover sbloccare l’apparato per mezzo del Pin.

Poco male, però. Abbiamo acquistato questo apparecchio per agevolare e incentivare la passione di Selene per le fotografie. Il passaggio dal G620S è stato quasi indolore e sul nuovo apparato siamo riusciti a trasferire praticamente tutti gli album bene ordinati e catalogati contenenti i video e le foto ricordo di anni e anni di momenti di vita immortalata nel pieno della giovinezza.

Ma dopo circa cinque mesi dall’acquisto, Selene ci contatta disperata: dal suo nuovissimo miracolo della tecnologia sono spariti tutti gli album fotografici. Così, da un giorno all’altro. Senza un avviso né un messaggio di errore. Il dispositivo funziona normalmente, come se niente fosse, ma gli album, centinaia e centinaia di foto e di video sono spariti.

Un app diagnostica per Android sentenzia che si è verificato un non meglio specificato errore sul disco e alla modica spesa di 5,99 euro può tentare un recupero dei file nei blocchi danneggiati.

Fortunatamente, la bella e brava Selene, che non è affatto sciocca e forse in fondo, anche perché da noi ben istruita e informata, conosce un pochino i suoi polli, cioè i suoi apparati Huawei, aveva conservato tutti i suoi album sul vecchio dispositivo, a guisa di backup.
Si applica, quindi, per recuperare i files perduti e poi, per evitare ulteriori problemi nel futuro, attiva il backup automatico di Google Foto. Si studia bene l’applicazione, sceglie le varie opzioni, la attiva e… Google Foto trasferisce SOLO alcune foto sul backup. Alcune altre, invece, spariscono dall’album e non si trovano più nemmeno sul backup.

Ora, siamo sicuri che questi malfunzionamenti sono sicuramente originati da qualche configurazione impostata male, nonostante la precisione e la diligenza di Selene, e francamente non abbiamo né la capacità né la possibilità di addentrarci dei meandri di un OS Android per capirlo.

Ciò che però capiamo, o meglio viviamo tutti i giorni, è che con il nostro oramai non più nuovissimo iPhone 7, certi episodi non sono mai accaduti. E a ben pensarci, con nessuno degli iPhone che abbiamo posseduto nel corso degli anni, è mai accaduto di perdere dei dati a causa di un errore sul disco. Che dire, poi, del backup iCloud? Si imposta, e funziona. Punto. Salva quello che deve salvare e ripristina ciò che c’è da ripristinare in caso di necessità, senza mai presentare un problema.

Il riconoscimento dell’impronta, poi, non ha MAI sbagliato un colpo.
Non abbiamo la fortuna di possedere un iPhone X, ma abbiamo vari amici e colleghi che ne sono felici utilizzatori e che sbloccano l’apparato in un batter d’occhio grazie al riconoscimento facciale. Che non sbaglia praticamente mai.

Ammettiamo di essere sicuramente di parte, lo siamo dai tempi di Macintosh Plus, ma trovare talune conferme sulle differenze sostanziali di usabilità e di funzionamento, in apparecchi che in linea puramente teorica sono equipaggiati con tecnologia equivalente, è invariabilmente, regolarmente sorprendente.
