---
title: "Uno spioncino sul futuro"
date: 2014-02-21
comments: true
tags: [iBeacon, baseball, iPhone]
---
Piccoli segnali che la rivoluzione continua: hanno iniziato a [installare iBeacon negli stadi americani del baseball](http://recode.net/2014/02/14/major-league-baseball-completes-ibeacon-installation-at-first-two-ballparks/).<!--more-->

iBeacon sta dentro iOS 7 e permette a un iApparecchio di interagire con un luogo attraverso una forma a bassissimo consumo di Bluetooth, per esempio per sapere da che parte andare per il proprio posto da spettatore, o per usufruire per un'offerta speciale.

Pochi anni e chi li chiamava telefoni non ricorderà neanche che la loro funzione originale era comporre numeri.

Per gli americani il baseball è lo sport nazionale. Come se da noi il Meazza e l'Olimpico, per capirci. Se si prende una iniziativa del genere, non è certo perché si scommette su un futuro che forse un giorno chissà.