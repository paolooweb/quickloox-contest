---
title: "Piccole innovazioni"
date: 2019-05-03
comments: true
tags: [Apple, iPad, iPhone, Watch, AirPods, SixColors]
---
Mentre leggevo su *SixColors* la [trascrizione dell’annuncio degli ultimi risultati finanziari](https://sixcolors.com/post/2019/04/this-is-tim-transcript-of-apples-q2-2019-call-with-analysts/) pensavo che:

- hanno ridefinito l’uso del personal computer;
- hanno creato la musica digitale personale;
- hanno trasformato il cellulare in calcolatore;
- hanno reso le tavolette elettroniche un oggetto realmente *for the rest of us*;
- hanno cambiato per sempre il concetto di orologio;
- hanno messo un processore negli auricolari.

Questo senza tirare in ballo traguardi minori, perlomeno quantitativamente, come portare a casa [la sperimentazione più allargata mai effettuata sulle malattie del cuore](https://macintelligence.org/posts/2019-03-16-cuore-di-polso/) oppure porre le basi del desktop publishing o ancora mettere in piedi i negozi monomarca più produttivi al mondo.

E però ragazzi, questi non innovano.
