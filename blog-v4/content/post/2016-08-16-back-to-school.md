---
title: "Back to School"
date: 2016-08-16
comments: true
tags: [ConnectED, education, scuola]
---
Apple ha diramato dati sulla propria [fornitura di apparecchiature e assistenza](http://www.apple.com/education/connectED/) alle scuole svantaggiate degli Stati Uniti.

114 istituti, ben più di trentamila macchine donate, due centoquaranta chilometri di cavi di rete, diecimila ore di formazione, attività e iniziative per le scuole, le classi e i docenti.

Va notato che non si tratta di un capriccio di Tim Cook, ma dell’adesione a un programma varato da Barack Obama di nome [ConnectED](https://www.whitehouse.gov/issues/education/k-12/connected).

Mi chiedo perché le scuole più disagiate degli Stati Uniti vengano trasformate e rinnovate con la migliore tecnologia, mentre in Italia non si sappia andare oltre la retorica della carta igienica o la microsoftizzazione forzata di bambini innocenti a cui viene inflitto Word per colpe che evidentemente non hanno ancora commesso.
