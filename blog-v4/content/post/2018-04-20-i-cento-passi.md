---
title: "I cento passi"
date: 2018-04-20
comments: true
tags: [Kare, Rand, Macintosh, NeXT, Jobs]
---
Oggi Susan Kare [riceve uno dei massimi riconoscimenti per un designer](https://www.newyorker.com/culture/cultural-comment/the-woman-who-gave-the-macintosh-a-smile).

Kare disegnò le icone del primo Macintosh, su griglie di carta, anche se Andy Hertzfeld creò un editor apposta per lei quando iniziò a lavorare per Apple.

Oggi ha sessantaquattro anni ed è scontato dire che sia diventata un’icona di per sé. Meno scontata è la porzione del suo lavoro che si è eternata in miliardi, ho detto miliardi di computer, solo una piccola parte dei quali sono stati Macintosh ma che hanno semplicemente abbracciato quel modo di rappresentare funzioni e comandi.

Quante altre persone hanno influenzato un’epoca attraverso miliardi di prodotti, di tutte le marche possibili? Poche.

Kare indirizzò un altro grande designer, Paul Rand, da Steve Jobs quando quest’ultimo cercava un logo per NeXT.

Rand presentò la sua proposta di logo sotto forma di un agile [libretto di cento pagine](https://www.logodesignlove.com/next-logo-paul-rand) che accompagnava il lettore, passo dopo passo, a scoprire il percorso che portava al logo.

Quando chiesero a Jobs come fosse stato lavorare con Rand, rispose:

>Gli chiesi se avrebbe presentato qualche alternativa e disse “No, risolverò il tuo problema e tu mi pagherai. Non sei obbligato a usare la soluzione. Se vuoi alternative, vai a parlare con qualcun altro”.

Mi sono trovato a volte nella parte del designer, di testo; affari di copywriting e pubblicità. Proporre dieci alternative al cliente è facilissimo; proporne tre impegna. Proporne una, può permetterselo Paul Rand e forse qualcun altro.

Alzi la mano chi può motivare una sua scelta di design in cento pagine.