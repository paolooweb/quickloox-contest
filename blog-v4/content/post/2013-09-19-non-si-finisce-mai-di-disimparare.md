---
title: "Non si finisce mai di disimparare"
date: 2013-09-19
comments: true
tags: [Scuola, Education]
---
Non vorrei mai più leggere articoli come quello di Simone Aliprandi sulla [ritrosia verso la tecnologia](http://aliprandi.blogspot.it/2012/02/la-ritrosia-verso-la-tecnologia-un-vero.html).

Non per Simone, che vale. Per il tema.<!--more-->

>Tizio Dei Tizi è un professore ordinario in una prestigiosa università pubblica del Nord Italia. Ha 68 anni ed è stato da poco nominato preside di facoltà. Tra circa 7 anni andrà in pensione, ma ha già trovato il modo di assicurarsi alcuni incarichi accademici anche dopo i 75 anni.
Lui non usa il computer, nemmeno per la posta elettronica o per scrivere brevi lettere; e comunque non saprebbe nemmeno inserire la carta nella stampante. Non ha mai imparato, troppo preso in questioni ben più importanti ed intellettualmente più elevate che imparare certe cose da comuni mortali.

Si parla di crisi, di soldi che non ci sono, di necessità di economie. Eppure.

>Quello che mi viene da chiedere, specie in un periodo di austerity come questo, è quanti soldi vanno sprecati per sostenere l'attività di questo autorevole (???) esponente del mondo scientifico italiano che ho qui chiamato Tizio Dei Tizi? Ore di lavoro di una segretaria che da segretaria di dipartimento diventa sostanzialmente una segretaria personale; carta e inchiostro per stampare comunicazioni che non è necessario stampare; telefonate su numeri privati che si potrebbero evitare; brutte commistioni tra collaboratori privati e "mano d'opera intellettuale" pagata invece con fondi di ricerca (e che quindi dovrebbe occuparsi di progetti di ricerca e non di attività di consulenza per uno studio professionale).

Come scrive Simone, l’uso elementare di posta elettronica, navigazione e videoscrittura è comune da oltre quindici anni.

C’è qualcuno che stia sopra Tizio Dei Tizi e voglia usare il proprio potere per mettere le cose a posto? O la catena della complicità procede ininterrotta?