---
title: "Senza fretta, senza sosta"
date: 2021-12-30T01:01:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [Mac mini] 
---
Qualche problemino di gioventù con il nuovo Mac mini. Ha voluto una tastiera nuova e aveva ragione lui; la mia precedente era *veramente* troppo vecchia.

Ora però ci sono vari errori che mi bloccano su certe cose, Il recupero dei dati da Time Machine è andato bene ma non benissimo. Per esempio Xcode non parte.

Devo anche montare una nuova versione di Homebrew giusta per Apple Silicon. Per ora dà errori strani. Questo è propedeutico ad avere il blog funzionante anche su M1.

Ora lancio un controllo del disco ed eventualmente reinstallerò. Sul resto, niente da dire: il surriscaldamento è un ricordo del passato, i tempi di reazione sono quelli che ci si aspetta, il nuovo mini supporta il monitor a risoluzione piena (prima dovevo fermarmi un gradino prima).

Senza fretta, senza sosta, vediamo di ritornare operativi.

_Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile._