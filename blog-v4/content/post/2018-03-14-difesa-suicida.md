---
title: "Difesa suicida"
date: 2018-03-14
comments: true
tags: [Intel, Gassée, Wintel, Jobs, iPhone, Qualcomm, Broadcom, Monday, Note, x86]
---
Nel 2012 si scriveva [Wintel è qui per restare](https://www.crn.com/news/components-peripherals/232400295/solution-providers-intel-smartphones-wintel-here-to-stay.htm) contestualmente all’annuncio della nuova e sicuramente vincente discesa in campo dei processori Intel per computer da tasca.<!--more-->

Tutto perché nel 2005 Steve Jobs aveva proposto a Intel di creare il processore di iPhone e aveva ricevuto un due di picche: Intel era assorbita dalla fornitura di processori per computer destinati a essere usati con Windows (appunto, Wintel). Che avrebbe dominato il mercato per sempre, superiore alle mode passeggere tipo la mania del *mobile*.

Oggi, [scrive Jean-Louis Gassée su Monday Note](https://mondaynote.com/intel-fights-for-its-future-6498f886992b) Intel dipende per l’80 percento del fatturato dai suoi processori x86, sempre quelli dell’era Wintel e sempre meno importanti.

(Pensare per un attimo a che cosa si scriverebbe se Apple dipendesse per l’80 percento da iPhone).

Intel riesce appena a fornire ad Apple alcuni chip modem per iPhone e niente più. Una colossale occasione mancata, sfruttata da Broadcom e Qualcomm.

Solo che Qualcomm è in lite con Apple e Broadcom pensa di acquisire Qualcomm. Se ci riuscisse, la lite con Apple cesserebbe e Broadcom acquisirebbe un vantaggio clamoroso verso Intel.

La quale, vista la situazione, sta valutando di spendere oltre cento miliardi per acquisire Broadcom.

Gassée la chiama *difesa suicida* perché Intel l’anno scorso ha registrato profitti per sessantatré miliardi. L’impresa e la spesa sono immense, per le risorse a disposizione.

L’unica altra speranza per Intel è che la fusione tra Broadcom e Qualcomm fallisca e lo *status quo* resti tale. Assolutamente sfavorevole, ma sempre meno peggio del disastro.

Tutto per essere rimasti ciechi nel 2005, quando Apple poteva mettere sul tavolo solo promesse e Intel aveva tutte le carte in mano per condurre i giochi.

Naturalmente, iPhone era solo un telefono.
