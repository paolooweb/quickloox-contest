---
title: "Ti sistemo io"
date: 2019-10-29
comments: true
tags: [United, Shanghai, Apple, Sfo]
---
La compagnia aerea statunitense United ha rivelato accidentalmente lo scorso anno che Apple spende in dodici mesi oltre centocinquanta milioni di dollari sui suoi voli in partenza da San Francisco, normalmente verso Shanghai.

Ora *Bloomberg* afferma che Apple [stia lavorando insieme a United per aggiornare il terminal](https://www.bloomberg.com/news/articles/2019-10-25/apple-united-in-early-discussions-to-upgrade-sfo-terminal), che appare datato e migliorabile.

Da parte di una azienda che, pur fabbricando computer, ha posto la massima cura nel progetto delle scale per i propri punti vendita, o in [quello della nuova sede](https://macintelligence.org/posts/2017-02-12-sta-nei-dettagli/), ce lo si può aspettare. Se curi il design, ti viene da curarlo sempre e soprattutto quando ogni giorno cinquanta tuoi dipendenti volano verso la Cina in business class.

Da parte di United, si dice che abbia acquistato finora oltre centomila apparecchi Apple per il proprio staff. Qualcosa devono averlo capito.

Certo, ci sono aeroporti dove si può fare la stessa cosa a prezzo minore.
