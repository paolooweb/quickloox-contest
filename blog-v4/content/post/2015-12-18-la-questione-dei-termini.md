---
title: "La questione dei termini"
date: 2015-12-18
comments: true
tags: [Snell, Mac, OSX, tvOS, watchOS]
---
Jason Snell ha pubblicato sui rimasugli di ciò che un tempo fu Macworld [un pezzettino banale banale e semplice semplice](http://www.macworld.com/article/3016426/macs/what-the-mac-needs-in-2016.html) sulle sue opinioni per l’evoluzione dei Mac nel 2016.<!--more-->

Tutte cosette ovvie, tranne la prima che mi piace e sostengo:

>Nel 2015, abbiamo visto tvOS e watchOS aggiungersi a iOS nei sistemi operativi di Apple. È logico che nel 2016 si unisca a loro macOS. (No, non mi entusiasma la minuscola, ma sospetto che ci abitueremo). Solo i Mac eseguono OS X e oggi la X sembra un reperto archeologico dei tardi anni novanta. Facciamo tornare Mac! Er, “mac”.

 km   gfsve Rsv  nS