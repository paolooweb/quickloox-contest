---
title: "Sondaggio ozioso"
date: 2014-11-21
comments: true
tags: [Origami, Incase, IcewindDale, iPad]
---
Che regalo tecnologico ci faremo fare per Natale?<!--more-->

Lato hardware chiederò al Babbo una [Origami Workstation firmata Incase](http://store.apple.com/it/product/H6353ZM/A/origami-workstation-firmata-incase) custodia per [tastiera *wireless* Apple](http://store.apple.com/it/product/MC184T/B/tastiera-apple-wireless-keyboard-italiano) che diventa supporto per iPad. Semplice e geniale, ingombro aggiuntivo tendente a zero, ideale in viaggio e anche per una scrivania estemporanea. Mai più senza.

Lato software, [Icewind Dale Enhanced Edition](https://itunes.apple.com/it/app/icewind-dale-enhanced-edition/id909472985?l=en&mt=8) per iPad.

Se sarò stato buono.