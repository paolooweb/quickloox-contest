---
title: "A che servono gli amici"
date: 2022-06-20T02:38:46+01:00
draft: false
toc: false
comments: true
categories: [Internet]
tags: [Apple, keynote, Fëarandil]
---
Ti sembra una domenica vuota e come tante altre (informaticamente parlando) e stai persino per andare a letto a un’ora vuota e come tante altre, quando per fortuna arriva [Fëarandil](https://www.twitch.tv/fearandil) con la dritta giusta: un bel video su certi [trucchi e accorgimenti usati da Apple per realizzare i suoi keynote virtuali](https://www.youtube.com/watch?v=kdFQYOigeXY&feature=youtu.be).

Alcune cose risulteranno evidenti e scontate allo specialista. Altre, beh, non ci avrei scommesso. Grazie!

<iframe width="560" height="315" src="https://www.youtube.com/embed/kdFQYOigeXY" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>