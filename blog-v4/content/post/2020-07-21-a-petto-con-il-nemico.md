---
title: "A petto con il nemico"
date: 2020-07-21
comments: true
tags: [Microsoft, Outlook, Exchange, Office, 365, iCloud]
---
Il lavoro a volte chiede compromessi e ho accettato di ricevere un account di posta su Office 365.

La premessa da cui partire è che Mail non è perfetto, iCloud nemmeno. Se parliamo di completezza, integrazione, configurabilità, Mail perde sempre e iCloud pure (qualcuno ricorderà che il primo iPhone era regolarmente il peggiore nei confronti di prodotto).

Ahimé: Steve Jobs aveva ragione. La posta di Office 365 ha un sacco di possibilità in più. Ma non è quello che fa: è il suo design. *Come funziona*.

Per prima cosa ho provato il distanziamento maggiore che potevo e ho aperto l’interfaccia Web. Office 365 dovrebbe essere l’epitome del servizio, il businessman ha tutto quello che serve a portata di mano eccetera.

Non c’è un modo chiaro per inviare una risposta senza inserire nel messaggio anche i duecentododici messaggi della conversazione. Non c’è un modo chiaro per quotare in modo semplice una frase cui rispondere. Premi il tasto Invia e il messaggio *sparisce*. Sarà partito? Per sbaglio ho premuto qualche altra cosa? Ho perso il contenuto per un singhiozzo del browser? Non si sa. Un segno di spunta, un messaggio *OK*, *messaggio inviato*, niente. Se c’è e non me ne accorgo, è ancora peggio; il design deve aiutare quelli che non ce la fanno, invece di decimarli lungo la strada. Se invii la mia email, mi devi un segnale di feedback.

Un popup mostra gli allegati già inviati e presenti nel sistema. Convenientemente, trancia i loro nomi a lunghezza ridicola. Se devi rimandare *Pianificazione commerciale per il mese di agosto versione 3*, il 3 non si vede; nel sistema sono presenti le versioni 1, 2 e 3 del file, di cui appare solo la prima parte del nome.

Questa non è una recensione, è un resoconto dei danni più evidenti. Quindi taglio corto e passo alla decisione successiva: inserire l’account in Mail.

Si riceve una lezione sul *walled garden* di Apple, quello che chiuderebbe l’utente al mondo esterno e gli farebbe perdere la libertà e l’autonomia. Apro le Preferenze di Sistema e chiedo di aprire un nuovo account Outlook: inserisco il nome utente e Microsoft mi risponde che *questo nome utente non esiste*.

Certo, quando mi hanno aperto l’account sono andato sulla pagina web relativa e l’account era vuoto. Niente app, niente servizi, niente di niente. Un po’ preoccupato (e già avevo le urgenze) ho chiamato il tecnico. La risposta:

>È normale, devi aspettare una decina di minuti perché deve fare tutte le sincronizzazioni.

I minuti sono stati venti più che dieci, ma è *normale*? Quando apro un account Google, Gmail mi arriva dopo un quarto d’ora? È normale sulla scala temporale di una mucca al pascolo.

Comunque, vedo che Outlook non è cosa. Provo allora ad aprire un account Exchange. Vuole il server. Quando ho acceso un account iCloud, il server non me lo hanno mai chiesto. Comunque *no problema*: una comodissima ricerca sul supporto Microsoft e ho trovato la risposta. Se voglio [aprire un account iCloud dentro Office 365](https://support.apple.com/en-us/HT204571), nessuno chiede un server; basta l’ID Apple.

Alla fine ho i miei account Office 365 dentro Mail, su iOS, iPadOS, macOS. Tanta fatica, nessun vantaggio, esperienza d’uso sotto media. Che amarezza, per citare *I Cesaroni*. Quello che non capisco è come faccia ad accontentarsi in questo modo gente che lavora.

Titoli di coda: ogni tanto la parte web di Office 365 mi ricorda di quanto siano belle e buone le app Microsoft per iOS. Voglio mantenere le mie macchine più che posso Microsoft-free, però ho deciso di vederle. Ho scaricato e configurato Outlook per iPad, per capire i vantaggi che mi offrirebbe rispetto a Mail.

Lo butto.