---
title: "Più in Alto"
date: 2017-06-16
comments: true
tags: [Thacker, Alto, Parc, Xerox, Ethernet, Microsoft]
---
Dobbiamo salutare [Charles P. “Chuck” Thacker](https://cacm.acm.org/news/218536-in-memoriam-charles-p-chuck-thacker-1943-2017/fulltext), grazie anche al cui lavoro si devono Xerox Alto, rete Ethernet, stampanti laser e varie altre innovazioni.

Ha trascorso un periodo anche in Microsoft a progettare lo hardware del Tablet PC, ma è chiaro che tutti possiamo sbagliare e in questo momento tutto va perdonato.

Anche lui ha ricordato a suo tempo che la famosa visita di Steve Jobs al Palo Alto Research Center di Xerox avvenne a [lavoro su Macintosh e Lisa già cominciato](https://web.stanford.edu/dept/SUL/sites/mac/parc.html): In particolare, Jef Raskin temeva che Macintosh potesse essere cassato da Apple e [desiderava che Jobs vedesse quanto avveniva in altre aziende](https://9to5mac.com/2017/06/14/chuck-thacker-lead-designer-of-the-xerox-alto-which-inspired-the-macintosh-dies/), così da convincersi a sostenere il progetto.

Non fu rubata alcuna idea e [Apple pagò](https://macintelligence.org/posts/2014-08-25-azioni-che-contano/) per quello che trovò utile portare sui propri progetti rispetto a quanto sviluppava Xerox.

Il fatto che Alto praticamente non venne venduto mentre Macintosh qualcosina, dovrebbe suggerire che esistevano anche differenze significative.

Tutto ciò conta poco di fronte all’ultimo saluto a un grande innovatore e grande tecnologo, che non ha riempito le cronache e al quale tuttavia dobbiamo tutti qualcosa qui, oggi, qualsiasi aggeggio per computare abbiamo in opera.

Mi piace immaginare che Chuck continui il proprio lavoro di sviluppo (e il proprio insegnamento ai giovani tecnici: *Cercate di essere aperti. Imparate più matematica, imparate più fisica*) da un laboratorio più in Alto.