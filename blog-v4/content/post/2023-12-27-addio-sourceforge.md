---
title: "La fonderia di una volta"
date: 2023-12-27T00:21:06+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [Sourceforge]
---
Mi capitava di girare in modo pigro per [Sourceforge](https://sourceforge.net) a caccia di progetti software open source di interesse, compatibili Mac, novità, aggiornamenti, curiosità, stranezze, esercizio della *serendipity* in opposizione alla rigidità dell’algoritmo.

Anni fa mi capitò di [consigliare anche Sourceforge come deposito](https://macintelligence.org/posts/2015-04-03-la-palestra-dellardimento/) da cui partire per avviare una attività di contribuzione a software libero.

Oggi certamente non lo consiglierei più e non so se vedrò mai più con qualche interesse una visita a Sourceforge passato il 2023. È proprio vero che i tempi cambiano; guardavo specialmente i giochi e vedevo titoli complessi, curatissimi, continuamente aggiornati, con basi di codice enormi, una comunità fremente.

Quei titoli continuano complessivamente a esistere ma su Sourceforge non ci sono più. Come sempre ho filtrato restringendo le scelte a sistema operativo macOS e categoria gioco. Sono usciti millecento e passa programmi, di cui oltre un quarto relativi agli scacchi.

Non è un problema di compatibilità con Mac; è proprio che Sourceforge ha cessato di essere rilevante. Fruga fruga salta fuori comunque qualcosina di curioso, come [pycassonne](https://sourceforge.net/projects/pycassonne/), clone del bel gioco da tavolo [Carcassonne](https://boardgamegeek.com/boardgame/822/carcassonne). Però sono poche cose, si fa poca strada, il livello di sviluppo è tale a volte da escludere la possibilità di provare a giocare.

Una volta era veramente molto diverso e ci si poteva perdere in una vertigine di opportunità. Oggi la *fonderia del sorgente* è un luogo certamente frequentato, solo in misura e qualità che impallidiscono di fronte agli antichi splendori.

Non sto a dire che cosa sia cambiato e perché. I giochi open source per Mac sono semmai cresciuti in numero, la maggior parte è multipiattaforma, il livello di sofisticazione può essere scelto in modo arbitrario, senza limiti superiori.

Di certo, con queste righe mi congedo da Sourceforge. Forse un giorno ci sarà qualche rivolgimento che oggi neanche immaginiamo e cambierà tutto. Forse. Per ora anno nuovo, repository nuovi.                                                                                                                                            