---
title: "Pubblicità pro"
date: 2017-02-05
comments: true
tags: [Super, Bowl, Hal, 2001, Lemmings, Madden, Apple]
---
Mediamente chiunque collega Apple e [Super Bowl](https://www.nfl.com/super-bowl) attraverso lo spot pubblicitario [1984](http://americanhistory.si.edu/blog/2014/01/remembering-apples-1984-super-bowl-ad.html). È meno nota invece la connessione tramite Hal, l’intelligenza artificiale di [2001: Odissea nello spazio](http://memeburn.com/2015/11/10-reasons-why-2001-a-space-odyssey-stands-as-the-best-sci-fi-movie-ever-made/).

La lettura di [questo articolo](http://kensegall.com/2017/02/the-making-of-apples-hal/) colma ampiamente la lacuna. Raccomandato per riempire le pause di gioco durante la partitissima, oppure in luogo dello *halftime show* di Lady Gaga.

Non ho favoriti ma mi piace in partenza l’idea del confronto tra una squadra presente molto spesso al Super Bowl e una molto poco, chi gioca per confermarsi e chi per affermarsi. Madden [pronostica](http://www.forbes.com/sites/erikkain/2017/02/01/madden-nfl-17-predicts-super-bowl-li-winner/#2ef8b3545ccf) la vittoria di misura dei primi, con i punti decisivi nel finale di partita, e se ha ragione ci sarà da divertirsi, più dell’[anno scorso](https://macintelligence.org/posts/2016-02-08-in-tono-minore/).
