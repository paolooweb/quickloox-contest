---
title: "Al parco per il compleanno"
date: 2022-05-17T02:34:23+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [WolframAlpha, Wolfram|Alpha, Estonia]
---
Normalmente arrivo sui compleanni il giorno dopo e oggi invece il giorno prima, perché ho avuto una esperienza fortunata (purtroppo riservata) con [Wolfram|Alpha](https://www.wolframalpha.com) a livello di ambiente scolastico.

Wolfram|Alpha compie esattamente tredici anni domani e si tratta di un compleanno significativo, perché secondo i suoi fondatori questa dovrebbe essere l’età giusta per approcciarlo, ovviamente sotto la guida degli insegnanti, da parte dei ragazzi.

È quello che succede già da un po’ nell’ambito di un [esperimento in Estonia](https://macintelligence.org/posts/2020-10-26-scuole-di-pensiero/), che sembra dare buoni risultati. Il principio è liberare i ragazzi dal vincolo di imparare per forza tutte le procedure di calcolo e invece utilizzare quel tempo per avvicinarsi a settori della matematica che i ragazzi stessi hanno le capacità intellettuali per comprendere, ma arrivano a studiare normalmente molto più tardi per via del tempo passato a calcolare per lo più inutilmente, cosa che il calcolatore riesce a fare molto meglio e molto prima.

Intanto il Wolfram|Alpha tredicenne mostra un sacco di novità e migliorie per chi non passasse da un po’ ed è diventato quasi un parco divertimenti per la matematica. I percorsi a tema sono tanti e non sempre scontati; ci sono forme di abbonamento a prezzo molto accessibile per un uso importante da parte di studenti, educatori o appassionati.

Non ne parla nessuno, forse perché è l’epoca sbagliata per trattare di strumenti sulla rete che presuppongono una qualche forma di competenza effettiva. Anche se Wolfram|Alpha potrebbe diventare di grande aiuto a farsela, una competenza matematica.