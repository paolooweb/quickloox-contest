---
title: "Non si interrompe uno streaming"
date: 2020-03-01
comments: true
tags: [RaiPlay, WittyTv, Mediaset, tv, Disney, Netflix, Amazon, Prime]
---
Anni fa si tenne un referendum per levare la proprietà di Retequattro a Mediaset e uno degli slogan dei favorevoli era *non si interrompe un’emozione*, nel senso che la pubblicità di Retequattro e delle altre televisioni commerciali spezzava l’incanto della visione.

Niente rispetto a quanto si può vedere vent’anni e passa dopo, se uno intende seguire un programma di televisione generalista in *streaming* invece che in digitale terrestre o via satellite.

Mi è capitato di farlo dopo molti anni di assenza, nei quali le figlie hanno detenuto il monopolio pressoché totale della programmazione e noi adulti abbiamo lasciato perdere la televisione (senza traumi, peraltro): nessuno guardava più il satellite e così lo abbiamo dismesso; una figlia ha giustiziato l’antenna del digitale terrestre e nessuno si è dato pensiero di prenderne una nuova, dal momento che tutta la televisione di casa passa da tv.

L’effetto pubblicità in queste condizioni è devastante.

Per un attimo si pensa a un crash del computer che riceve, o della app di competenza, o di Safari. Da quel momento si è nelle mani del destino. Può succedere che lo stream vada riavviato manualmente. Oppure parte lo spot pubblicitario, dopo una pausa di un secondo che però, per un telespettatore, come minimo è fastidiosa. Lo spot arriva veramente da un altro pianeta oltre che presumibilmente da un altro server e la differenza nella dinamica audio è disturbante. Se gli inserzionisti pensano di vendere qualcosa o convincere qualcuno con questo tipo di proposta pubblicitaria, si vede che non hanno mai pensato di provare a guardarlo dentro una trasmissione in streaming.

Dopo lo spot, che a questo indispone anche se dura sette secondi, riparte la trasmissione. O meglio, dovrebbe; capita che non riparta, o che lo faccia dall’inizio dello stream quando magari erano già passati trenta minuti di visione. Per motivi che sfuggono, succede talvolta che lo stream riparta con l’audio disattivato e tocchi riattivarlo a mano. Anche chi lavora per quei canali televisivi dovrebbe provare, sì, l’emozione di guardare una sua trasmissione via Internet.

Questo o quel canale, questa o quella app fanno ben poca differenza. Al massimo allargano la varietà dei problemi potenziali.

Un abbonato ad Amazon Prime, ad Apple TV+, A Netflix, a Disney spende una cifra mensile modesta, quando non misera, per avere contenuti di tutto rispetto e di qualità da buona a ottima, con una buona esperienza utente. Il tutto mentre paghiamo un canone annuo che finanzia realtà incapaci di mettere insieme lo stream di una trasmissione e quello di uno spot, o di mettere insieme una app capace di stare in piedi da sola per più di un’ora. Altre realtà si autofinanziano quasi solo con la pubblicità, ma non è che cambi molto.

Avrei qualche idea su che cosa interrompere in questo panorama e non sono le emozioni.