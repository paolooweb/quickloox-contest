---
title: "I giorni del giudizio"
date: 2022-11-06T23:47:35+01:00
draft: false
toc: false
comments: true
categories: [Software, Web]
tags: [Interactive Fiction Competition, IFComp]
---
Uno dei connubi più felici tra umanesimo e tecnologia digitale rimarrà per sempre il gioco di avventura testuale, che in più di un caso ha raggiunto vette degne del valore artistico e che ha portato milioni di persone a esprimersi (nel caso degli autori), viaggiare con la mente, imparare lingue nuove, divertirsi, accettare sfide.

Diecimila dollari (in aumento) sono il montepremi raccolto pubblicamente per l’edizione 2022 della [Interactive Fiction Competition](https://ifcomp.org). Otto dollari su dieci verranno distribuiti tra le oltre settanta avventure in gara e gli altri due a sostegno delle spese della Interactive Fiction Technology, Foundation, che organizza.

Possiamo fare la nostra parte come finanziatori (PayPal, è un attimo, un clic sul pulsante) oppure diventare giudici: giocare almeno cinque delle avventure e votarne almeno cinque. C’è tempo fino al 15 novembre ed è sicuramente troppo poco, perché è troppo poco a prescindere. Le avventure testuali fanno bene allo spirito e al vocabolario, allenano la mente, portano in mondi alternativi che una volta solo i libri potevano darci e i giochi moderni, pieni di grafica, non possono, perché appiattiscono l’immaginazione.

PayPal o il giudizio. O entrambi.