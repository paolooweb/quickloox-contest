---
title: "La sfera di cristallo"
date: 2013-09-07
comments: true
tags: [OSX]
---
9to5 Mac ha superato se stesso, con l’[articolo](http://9to5mac.com/2013/09/05/ibooks-textbooks-compatibility-wording-hints-at-imminent-mavericks-arrival-iphone-support/) sui cambiamenti nelle specifiche dei libri composti con iBooks Author.<!--more-->

Sì, perché il modo in cui sono scritte le specifiche

>Lascia intendere un imminente arrivo di Mavericks e il supporto di iPhone.

Grandi novità che nessuno avrebbe mai sospettato. Con l’eccezione di metà del mondo che sta attento a queste cose, sa che Mavericks è arrivato alla settima versione preliminare e che Apple ha detto a giugno che sarebbe arrivato in autunno.

Il bello è che secondo l’autore Mavericks è *quasi completo*. Il che dipende dal significato che si attribuisce alla definizione. Ovvero significa qualsiasi cosa.

Che è il trucco dei maghi.

**Aggiornamento**: Apple ha cambiato la specifica.

<blockquote class="twitter-tweet"><p>Apple changed the wording of iBooks requirements on iTunes again: <a href="http://t.co/P3nIaz5dlu">http://t.co/P3nIaz5dlu</a>&#10;&#10;This morning: <a href="https://t.co/DJFa2Xywsv">https://t.co/DJFa2Xywsv</a></p>&mdash; Federico Viticci (@viticci) <a href="https://twitter.com/viticci/statuses/375748989532246017">September 5, 2013</a></blockquote>
<script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>

Così, se il ragionamento del chiaroveggente gira, non è vero che stia arrivando Mavericks e iPhone non leggerà gli iBook (come è stato finora). Un capolavoro, una pagina piena di significato nullo.