---
title: "Servi del podcast"
date: 2023-08-03T13:58:13+01:00
draft: false
toc: false
comments: true
categories: [Internet]
tags: [A2, Strozzi, Marin, Roberto Marin, Filippo Strozzi, Vision Pro]
---
[Filippo](https://www.avvocati-e-mac.it) ha le papille gustative interrotte, [Roberto](https://marchdotnet.wordpress.com) ha il gomito che fa contatto con il piede, io [sono rimasto incastrato nell’autolavaggio](https://www.youtube.com/watch?v=FbPtvFxUb60)… ma stasera si registra per [A2](https://a2podcast.fireside.fm).

Si dovrebbe.

Parleremo del secondo pulsante di Vision Pro. Forse.

Potrebbe piovere. Non è detto.

Se l’intelligenza artificiale si disintelligenzartificializzasse, sarebbe ancora intelligenza? Sarebbe ancora artificiale?

Fu vera gloria?

Se non ora, quando?

<iframe width="560" height="315" src="https://www.youtube.com/embed/FbPtvFxUb60" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>