---
title: "Salami e salsicce"
date: 2014-08-01
comments: true
tags: [Clemenceau, Idc, Gartner, Mac, Maestri, Elmer-DeWitt, Fortune, Apple]
---
Lo statista francese Georges Clemenceau [ebbe a dire](http://www.corriere.it/Rubriche/Libri/12_Dicembre/15/gocce1512.shtml) di una certa salsiccia francese che è come la politica: *per essere gustosa, deve sapere un poco di merda.*<!--more-->

Philip Elmer-DeWitt ha titolato su *Fortune* [Stime di vendita dei PC: come viene prodotta la salsiccia](http://fortune.com/2014/07/28/pc-sales-estimates-how-the-sausage-gets-made/).

Il punto è che i due principali produttori di stime di vendita dei computer, Idc e Gartner, [davano Apple in calo in America](http://fortune.com/2014/07/10/why-lenovos-pcs-are-outselling-apples-macs/) ed è finita con i Mac [in crescita](https://macintelligence.org/posts/2014-07-24-se-lo-dicono/), per giunta in doppia cifra e non solo in America ma anche in Australia, Canada, Cina, Francia, Germania, India, Medio Oriente, Messico e Regno Unito, come da [trascrizione delle dichiarazioni](http://seekingalpha.com/article/2331715-apples-aapl-ceo-tim-cook-on-q3-2014-results-earnings-call-transcript) di Luca Maestri, italiano fresco timoniere delle casse di Apple.

Non è neanche la prima volta che succede: nel 2010 tra le stime e i risultati reali mancavano all’appello [77 milioni di telefoni](http://fortune.com/2010/11/11/missing-77-million-mobile-phones/).

Elmer-DeWitt ha ottenuto una testimonianza anonima di un ex ricercatore di Idc, che vi ha lavorato otto anni e ha spiegato come vengono generate le stime di vendita. Condenso molto, come una salsiccia assai pressata:

>Nella maggior parte dei trimestri la squadra parte dalle indicazioni dei produttori e esegue controlli incrociati su alcune nazioni. Per gli Stati Uniti, comunque, aggiustavamo i dati ad alto livello e andava bene così. […] Nel terzo trimestre 1998 stavo analizzando i punti della catena dei fornitori dove l’analisi arriva abbastanza in profondità da restituire un conteggio preciso. Ho trovato venti milioni di processori fantasma, una discrepanza del venti percento sul totale del mercato. Ne è seguita una meravigliosa procedura di offuscamento. Si escogitarono metodi per piazzare i componenti che non tornavano […] il mantra divenne preservare i tassi di crescita e chi se ne frega dei numeri veri. La categoria Altri è diventata il refugium peccatorum che viene aggiustato per fare quadrare i conti. Il mercato sa di questi problemi, ma li copre perché fa comodo. Il fatto è che i dirigenti prendono stipendi veri basati sui nostri numeri [aggiustati].

Si noti che Apple pubblica regolarmente i dati di vendita ufficiali, cosa che molti altri non fanno, Samsung in testa. Quindi i suoi dirigenti prendono stipendi basati su risultati reali.

Ma non era questo il punto. Che è invece: i numeri dati da Idc e Gartner sono falsi, o veri per metà, o quasi veri, o un po’ falsi, ma non corrispondono mai alla realtà. Sanno un po’ di quella cosa di Clemenceau e per questo danno gusto.

Non sono stime di vendita, sono salsicce. E chi ci crede, beh, è un salame.
