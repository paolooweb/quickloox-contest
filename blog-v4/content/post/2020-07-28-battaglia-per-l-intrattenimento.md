---
title: "Battaglia per l'intrattenimento"
date: 2020-07-28
comments: true
tags: [Wesnoth, Polytopia]
---
Riscopro che [Battle for Wesnoth](https://www.wesnoth.org) ha nuovamente formalizzato in positivo i rapporti con App Store. I proventi della vendita della [versione aggiornata](https://apps.apple.com/it/app/battle-for-wesnoth/id575852062?l=it) o di quella [legacy](https://apps.apple.com/it/app/battle-for-wesnoth-legacy/id575239775?l=it) vanno quindi a sostenere un bel progetto di gioco di strategia a turni coinvolgente, pulito, per tutte le età, fruibile a livelli di complessità arbitrari, personalizzabile, multiplayer volendo, insomma tutto.

Wesnoth è _open source_ e non chiede niente a chi vorrà scaricarlo su Mac. Una donazione anche minuscola farebbe del bene, sempre e non solo in questo caso, tuttavia anche un bel passaparola può essere grandemente utile. La giocabilità è altissima e le sfide, anche per i più bravi, veramente sono senza fine.

Perché ne parlo e [da sempre](https://macintelligence.org/blog/2015/08/07/sollecito-software-2/)? Perché è intrattenimento intelligente con possibilità che sia anche relazionale. Merce rara da coltivare e preservare e nemmeno mi metto a considerare il rapporto tra prestazioni e prezzo.

(Sempre in tema, chi volesse unirsi alla nostra piccola confraternita di praticanti di [Battle for Polytopia](https://apps.apple.com/it/app/the-battle-of-polytopia/id1006393168?l=it), sarà il benvenuto come sempre).