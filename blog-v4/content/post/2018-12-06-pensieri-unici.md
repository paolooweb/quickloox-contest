---
title: "Pensieri unici"
date: 2018-12-06
comments: true
tags: [Gemmell, iPad, Pro]
---
Per più di due settimane ho lavorato esclusivamente con un iPad di sei anni fa, senza finestre multitasking e con una certa lentezza. Si può assolutamente fare, solo che non è sostenibile nel lungo periodo.

Ora ho [un iPad Pro 12”9](http://www.macintelligence.org/blog/2018/11/29/non-recensioni-ipad-pro-12-9-3-gli-avanzi/) con processore A12, con le finestre multitasking, ma anche [un nuovo Mac mini](http://www.macintelligence.org/blog/2018/11/25/non-recensioni-mac-mini-2018/). Avrei potuto fare a meno di quest’ultimo? Certamente. Anche nel lungo periodo? Sì.

*Allora, perché hai preso un mini?* Per preferenza personale. Capirei benissimo uno che decide di non prenderlo. O di prendere un MacBook Pro e fare a meno di un iPad. Il bello di quest’epoca è che è possibile scegliersi la configurazione preferita di lavoro. Tutte hanno pregi e difetti, tutte funzionano.

Eppure si finisce per sbattere sempre contro quello che ti spiega come dovresti lavorare, sicuro che iPad non possa sostituire un computer per esempio. Una forma di pensiero unico sclerotizzato. Arthur Clarke scriveva che se uno scienziato dice che una cosa si potrà fare, probabilmente ha ragione; se dice che non si potrà fare, probabilmente ha torto, specialmente se avanti con l’età. Qui è la stessa cosa: pensare che con iPad non si possa fare a meno di un Mac è probabilmente sbagliato e ritenere che si possa, invece, è probabilmente giusto.

Nessuno lo ha mostrato meglio di Matt Gemmell nel suo [The Big iPad](https://mattgemmell.com/the-big-ipad/), capace di dire le cose come stanno in modo perfettamente ragionevole. Va letto per intero. Mi limito a riportare un frammento. L’antefatto è che Gemmell, autore librario, da due anni lavora solo con iPad e recentemente ha sostituito il suo iPad Pro 10”5 con il modello da 12”9. Poteva sostituire il suo portatile?

>Un iPad non è un laptop e la parola “sostituire” qui è bizzarra. Crea una aspettativa di sovrapposizione esatta di ciascuna funzione, alfiere della non volontà ad adattarsi. [Non è difficile adattarsi. Ti devi adattare, perché il sistema operativo e il modello di interazione sono diversi.] Ecco perché, se già poni la questione in questo modo, la risposta è probabilmente no. Leggo recensioni nelle quali un difetto apparente di iPad è la mancanza di un trackpad; mi sembra come lamentarsi della mancanza del timone in una automobile. È solo un modo strano di pensare.

Gemmell arriva in fondo spiegando cose che dovrebbero essere ovvie: non è obbligatorio avere un iPad ed è legittimo preferire un altro apparecchio; il prezzo è alto ma vale la pena per chiunque ritenga che sì, vale la pena. Il prezzo non è un criterio assoluto. Se usi un iPad per tutte le tue attività ti serve una tastiera fisica. Sì, sembra incredibile, ma c’è bisogno di scriverlo.

In definitiva, si possono solo rispettare le decisioni di chi adotta un iPad e quelle di chi non lo fa. Al pensiero unico di chi pretende di sancire decisioni che valgano per tutti, opporre milioni di pensieri unici, ognuno diverso, di chi sceglie quello che è meglio.