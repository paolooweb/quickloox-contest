---
title: "L’urca dell’alleanza"
date: 2024-02-06T00:15:18+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [Ai, intelligenza artificiale, Vesuvius Challenge, Ercolano, machine learning]
---
E pensare che il *machine learning* in quanto tale ha utilizzi straordinari.

Due ricercatori si sono aggiudicati un cospicuo premio in denaro per avere [vinto la Vesuvius Challenge](https://scrollprize.org/grandprize): sono riusciti a [leggere il primo](https://twitter.com/natfriedman/status/1754519304471814555) di un tesoro di manoscritti scoperti (e probabilmente ce ne sono ancora da scoprire) in una villa romana di Ercolano sepolta a suo tempo dall’eruzione del Vesuvio.

Era una sfida perché i manoscritti erano – e sono – arrotolati. La cenere rovente da cui sono stati coperti li ha cotti, prima di conservarli come i *burrito* che sembrano oggi, fragili e intoccabili: il tentativo di aprirne fisicamente uno ha portato unicamente a una striscia di frammenti di papiro illeggibili e inutilizzabili.

<blockquote class="twitter-tweet"><p lang="en" dir="ltr">Ten months ago, we launched the Vesuvius Challenge to solve the ancient problem of the Herculaneum Papyri, a library of scrolls that were flash-fried by the eruption of Mount Vesuvius in 79 AD.<br><br>Today we are overjoyed to announce that our crazy project has succeeded. After 2000… <a href="https://t.co/fihs9ADb48">pic.twitter.com/fihs9ADb48</a></p>&mdash; Nat Friedman (@natfriedman) <a href="https://twitter.com/natfriedman/status/1754519304471814555?ref_src=twsrc%5Etfw">February 5, 2024</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>


Ecco la sfida. Leggere il contenuto di un rotolo di papiro carbonizzato di duemila anni fa, senza toccarlo.

Ci sono volute le più sofisticate tecniche di scansione e di interpretazione delle immagini, per arrivarci, compresa appunto una robusta dose di *machine learning*.

Dall’alleanza tra storia, archeologia, lettere e tecnologia digitale vengono fuori scoperte e progressi che lasciano senza fiato. Non c’è che auspicare tanti ancora e tanti di più per questo tipo di operazioni che contribuiscono a scrivere la storia, quindi anche chi siamo e da dove veniamo.