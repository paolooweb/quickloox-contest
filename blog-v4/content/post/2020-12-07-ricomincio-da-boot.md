---
title: "Ricomincio da boot"
date: 2020-12-07
comments: true
tags: [M1, Nvram, Silicon, Big, Sur, Dfu]
---
Non ora; è comunque chiaro che prima o poi mi arriverà in casa un Mac Apple Silicon, con M1 o chissà.

Sarà bene ricordare che [cambiano le modalità di startup](https://eclecticlight.co/2020/11/28/startup-modes-for-m1-macs/) cui abbiamo fatto l’abitudine per vent’anni o circa.

Il più delle volte si tratta di tenere premuto il pulsante di accensione fino a che si vede il caricamento di un menu di opzioni di partenza; molto di quello che c’era è reperibile dentro il menu.

Ma c’è altro, per esempio una modalità Dfu (come quella di iPhone!) per risolvere problemi al firmware; un Recovery Mode per reinstallare macOS o ripristinare un disco Time Machine; una modalità diagnostica più chiara di prima e varie altre cose.

Le classiche cose che vanno memorizzate ora in un posto adatto; se mai serviranno, sarà nel momento in cui tra la fretta e l’ansia non verrà in mente di ricordare le nuove procedure.

Qualcosa di familiare però c’è: [Big Sur ha riportato attivo di default il suono di avvio di Mac](https://www.theverge.com/2020/6/23/21300545/apple-mac-macos-big-sur-startup-chime-sound-back-return).