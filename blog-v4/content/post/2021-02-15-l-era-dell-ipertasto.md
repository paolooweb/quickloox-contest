---
title: "L’era dell’ipertasto"
date: 2021-02-15T03:33:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [MacSparky, hyper key] 
---
Ignoravo che fossimo arrivati all’ipertasto fino a quando *MacSparky* mi ha illuminato.

L’ipertasto significa [usare Blocco Maiuscole o un altro tasto di preferenza in modo che equivalga alla pressione simultanea di Maiuscole, Control, Opzione e Comando](https://www.macsparky.com/blog/2021/2/hyper-key-via-bettertouchtool). Operazione, riferisce MacSparky, oggi ancora più facile che in passato, grazie ai programmi giusti per intervenire sulla mappatura della tastiera

Chiaramente, l’ipertasto permette di creare un numero consistente di nuove comandi da tastiera, prima impossibili.

Sono un fan acceso della navigazione e dell’uso di Mac via tastiera, ma confesso che l’idea – per quanto certamente utile e ingegnosa – mi incoraggia a passare più tempo su iPad.

*Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile.*