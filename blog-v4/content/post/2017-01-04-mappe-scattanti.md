---
title: "Mappe scattanti"
date: 2017-01-04
comments: true
tags: [Maps, Google, Mappe]
---
Questa sera ho dato una chance alle mappe di Google per una funzione simpatica che ha il sistema: cercato su Google un indirizzo, è possibile inviare al computer da tasca una email o un Sms contenenti un indirizzo che si apre nella app e permette di impostare l’itinerario.<!--more-->

Mappe di Apple ha una funzione simile, a patto di avere eseguito la ricerca in Mappe su Mac, il che accade più raramente.

Così solo salito verso la Brianza in una notte di inverno ed è andato tutto benissimo. Le mappe di Google sono piuttosto chiare, sono precise, non ci sono problemi particolari a parte uno: l’aggiornamento della mappa avviene a scatti.

Mappe di iOS è perfettamente fluido mentre, nella situazione, l’analogo di Google non lo era. Niente che disturbasse veramente la guida, ma una leggera ansia sì. Anche perché, in un paio di occasioni, la mappa si è aggiornata all’ultimissimo istante utile prima di una rotonda o un incrocio cruciale.

Con iOS mi arrabbio quando il programma diventa schizzinoso con la toponomastica e pretende che gli si dica *via Edson Arantes do Nascimiento*, altrimenti ti manda verso una via Pelè che sta in Lussemburgo invece che nel paese di fianco. Per il resto, nessun problema. Con le mappe di Google nessun problema, ma questo aggiornarsi a singhiozzo lascia dubbi: e se in un’altra situazione restassi a corto di indicazioni nel momento sbagliato?

Il che suscita la domanda successiva: ho preso la serata sbagliata per il sistema Gps, la app di Google è scritta male oppure la app di Apple ha vantaggi di sistema che sono preclusi agli sviluppatori esterni?

Se qualche sviluppatore ha voglia di illuminarmi, sono comunque tornato a casa sano e salvo, e curioso come di consueto. Ringrazio anticipatamente.