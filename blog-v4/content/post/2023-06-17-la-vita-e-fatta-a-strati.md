---
title: "La vita è fatta a strati"
date: 2023-06-17T19:09:47+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [L’io della mente, The Mind’s I, Douglas Hofstadter, Daniel Dennett, John Searle, Searle, intelligenza artificiale, ai]
---
Ci ho messo del tempo, ma eccomi qui: [L’io della mente](https://www.adelphi.it/libro/9788845906329) è un libro in cui Douglas Hofstadter e Daniel Dennett raccolgono contributi autorevoli sul tema della mente, il cervello, l’intelligenza e l’intelligenza artificiale, per aggiungere in coda a ciascuno le proprie riflessioni.

Uno di questi contributi è [Menti, cervelli e programmi](https://e-l.unifi.it/pluginfile.php/914635/mod_folder/content/0/lezione%208%20-%20Searle%20-%20Minds%2C%20Brains%20and%20Programs.pdf?forcedownload=1) di John Searle, nel quale il suddetto descrive l’esperimento della stanza cinese, in cui un umano chiuso dentro una stanza combina simboli sconosciuti in entrata a simboli sconosciuti in uscita, in base a un manuale in suo possesso, e all’esterno sembra rispondere a tono a domande in cinese, mentre l’umano il cinese non lo sa; si limita a seguire il manuale di combinazione degli ideogrammi e non sa che significato abbiano, né che cosa legga o scriva.

Searle usa questo argomento per sostenere che una macchina non è capace di pensare, a meno che sia un cervello umano o un equivalente; che un programma non può contenere significato ma è semplicemente un ambiente di manipolazione di simboli di per loro vuoti; che un programma è ciò che sta in mezzo a un ingresso e un’uscita e in base a questo tutto è un calcolatore di qualche tipo; che senza funzioni causali di una macchina capace di usarle, come un cervello umano, non può esserci intenzionalità. Il computer può fare cose geniali, ma non lo sa e neanche sa perché.

(Ovviamente siamo a un livello di riassunto scandalosamente ristretto e mi si perdonerà se qualche dettaglio viene sorvolato).

Hofstadter, [dicevo a **mailmasterc**](https://macintelligence.org/posts/2023-06-09-la-complessità-cinese/), non è d’accordo con Searle e, dopo approfondimento, dico anche perché. Egli ritiene che lo stesso Searle non sia d’accordo con il proprio ragionamento e, semplicemente, glissi con abilità sulla cosa. (Il prima e il dopo questo paragrafo è un mio resoconto e in parte una mia intepretazione, ma non una presa di posizione. Searle e Hofstadter dicono quanto riferisco; al massimo posso inserire delle inesattezze, tuttavia gli argomenti fondamentali non sono in discussione e sono i loro, non i miei).

Ecco dove Hofstadter pone le osservazioni più sostanziali.

## La stanza cinese si contraddice ##

Intanto: Searle elenca alla fine del suo ragionamento una serie di risposte tipiche che riceve dai ricercatori teorici dell’intelligenza artificiale forte e Hofstadter colloca la propria tra le *risposte dei sistemi*: nella stanza cinese la conoscenza è prerogativa dell’intero insieme che forma l’esperimento, non solo dell’umano nella stanza.

A parere di Hofstadter, la descrizione della stanza cinese è fatta apposta per identificarci nell’umano e simpatizzare con il suo punto di vista. (Searle pone addirittura sé stesso nella stanza, non una persona immaginaria).

Ancora: Searle tratta con noncuranza dettagli significativi, dove si annida un problema. Per esempio, le istruzioni che permetterebbero all’umano di combinare domande in cinese all’ingresso con domande in cinese in uscita sarebbero, quando nominate per la prima volta, *pochi fogli*. È intuitivamente evidente che dovrebbe trattarsi di un volume enorme, per di più di difficoltà estrema da memorizzare quando Searle lo propone come soluzione a una obiezione. La questione non è superficiale; se parte importante della conoscenza del sistema fosse proprio lì?

La questione delle spiegazioni di Searle prese alla lettera si pone anche perché, ricordo, per lui tutto è computer. Anche uno stomaco in peristalsi, anche una moka sul fuoco. Input, output, in mezzo un programma. Se per caso salta fuori che dentro un programma può essere contenuta intenzionalità, a qualche svitato potrebbe venire di dire che anche una moka ha intenzionalità. Hofstadter qui puntualizza: come si possono mettere sullo stesso piano la conoscenza e il significato presenti nella stanza cinese con quelli di un interruttore della luce? Per evitare che si possa riconoscere l’intenzionalità a uno stomaco che digerisce, Searle la vuole togliere anche al più complesso dei programmi.

Più pregnante è l’obiezione di avere presentato l’esperimento in forma la più convincente possibile per i nostri bias intrinseci. Hofstadter ci spinge a cambiare i parametri per renderlo meno aderente alla nostra realtà. Se siamo uomini immaginiamoci una donna, se parliamo italiano pensiamo che l’esperimento riguardi l’italiano; segni conosciuti invece che esoterici e così via.

Su questa strada, formalizza l’esperimento di Searle proponendo uno schema standard per produrre versioni diverse in numero infinito dell’esperimento stesso. Il trucco sta in manopole teoriche che regolano le varie dimensioni dell’esperimento: la velocità di trattamento dei simboli, le dimensioni fisiche della simulazione, il tipo di ambiente, il tipo di informazione scambiata, l’entità che smista input e output e avanti così.

Succede che Hofstadter gira le manopole e propone questa versione dell’esperimento. L’ambiente non è più una stanza, ma il cervello di una donna. L’umano di prima ora è un demonietto minuscolo con una prodigiosa capacità di stimolare i singoli neuroni del cervello a velocità altissima e con estrema precisione. La donna non sa il cinese; ascolta frasi cinesi in ingresso ma non saprebbe che farci. Il demonietto, però, a velocità smodata sollecita i neuroni che, nel loro insieme, generano una risposta a tono da parte della donna.

Qui Hofstadter invita a considerare non solo il punto di vista dell’interno dell’esperimento, ma anche quello dell’esterno. Noi osservatori, non consapevoli di tutto il meccanismo, vediamo una donna che ascolta frasi in cinese e risponde a tono. A istinto ci viene da dire che quella donna conosce il cinese, mentre dentro il cervello di lei il demonietto disperato strilla che nessuno gli riconosce il merito. In realtà, conclude Hofstadter, l’esperimento di Searle mostra che un sistema come quello da lui concepito può passare il test di Turing (in senso generale). È sufficiente cambiarne i parametri in modo da scavalcare le nostre convinzioni pregresse. Quando parliamo con una persona che ci risponde in modo appropriato, riconosciamo a lei l’intelligenza, non a qualcosa che si agita dentro la sua testa. Analogamente, nel vedere la stanza cinese, dovremmo riconoscerle complessivamente un’intelligenza, indipendentemente da come si comporta il tizio che maneggia fogli di carta e scrive ideogrammi di risposta.

## Un programma non basta a nessuno ##

La parte interessante a dire il vero inizia adesso, perché c’è anche un versante dove Searle e Hofstadter trovano una convergenza. Ricordo che per Searle il programma non può contenere l’intenzionalità; maneggia simboli formali che non capisce. Per andare oltre, sotto il programma deve esserci una macchina capace di applicare funzioni causali, come il cervello.

Hofstadter nota che, per avere un comportamento intelligente, un programma deve avere sotto strati di altri programmi che, prima della manipolazione ad alto livello di simboli, sovrintendono per esempio alla micropercezione e poi alla macropercezione, poi alla creazione di analogie, alla formazione dei significati eccetera. Singoli neuroni reagiscono al bombardamento della luce sulla retina, comunicato al nervo ottico; insiemi di neuroni si attivano a livello di forme o aree e finisce che sorridiamo a una persona che conosciamo e che sorride.

La stessa cosa funziona più o meno allo stesso modo nei computer. Lo strato applicativo ha sotto lo strato del sistema operativo, che ha sotto lo strato del kernel e alla fine si arriva ai transistor che dicono sì oppure no.

A maggior ragione questo assetto si vede nel computing odierno, dove abbonda l’uso di container e macchine virtuali. Un container sta in una macchina virtuale, che sta in un’altra macchina virtuale e così via. Il cloud è una gigantesca applicazione di questo principio.

C’è un precetto fondamentale: *gli strati non si parlano*. il mio programma `uptime` dentro la mia macchina virtuale FreeBSD non sa e non può sapere nulla del carico di lavoro di macOS su cui si appoggia la macchina virtuale. Se macOS fosse una macchina virtuale a sua volta, affiancata da un’altra macchina virtuale contenente un’altra istanza di macOS, non se ne accorgerebbe, né potrebbe influenzarne l’esecuzione o i parametri.

Qui le cose si fanno *molto* interessanti.

## Il segreto della vita ##

Eravamo rimasti, poveri incolti, a ignorare il cinese. Decidiamo di impararlo e di farlo bene. Studia, prova, leggi, conversa e alla fine diventiamo bravini.

Hofstadter, che conosce benissimo l’italiano e ha scritto molto sulle problematiche della traduzione, nota che il nostro programma di conoscenza del cinese non è una applicazione in più appoggiata sul nostro programma di conoscenza dell’italiano. Se siamo diventati bravini, la conoscenza del cinese funziona *in parallelo* a quella dell’italiano. (Non è parallelismo informatico; non parliamo contemporaneamente italiano e cinese).

Significa che non dobbiamo pensare in italiano e tradurre in cinese, prima di rispondere, ma la risposta arriva spontaneamente in cinese a fronte dell’input in cinese che abbiamo ricevuto.

Significa che, nell’imparare il cinese, abbiamo sollecitato e influenzato strati inferiori della nostra programmazione.

Nell’intelligenza umana, *gli strati si possono parlare*. Possono modificarsi verso il basso e verso l’alto, influenzarsi reciprocamente. La macchina virtuale del cinese non è contenuta dentro quella dell’italiano; fa riferimento indipendente agli stessi strati del cervello che usiamo nel parlare l’italiano. Si è radicata all’interno del nostro cervello; forse non alla stessa profondità dell’italiano visto che non siamo madrelingua. Ma certamente ha penetrato strati invece di galleggiare in superficie. Non abbiamo bisogno del programma *italiano* quando applichiamo il programma *cinese*.

Hofstadter riflette che questa capacità di parlarsi tra strato e strato delle macchine virtuali, del cervello e un domani del computer, di modificarsi previo feedback in entrambe le direzioni, sia una necessità cruciale per un sistema che voglia esprimere intenzionalità, intelligenza, significato.

Ecco perché Searle, nella sua analisi, sbaglia e una macchina, in linea di principio, può pensare. C’è solo molta, molta strada da fare ancora. Il segreto, uno dei segreti, dell’intelligenza è l’apertura di passaggi tra gli strati del pensiero o della computazione.

Non ci è richiesto di schierarci, piuttosto di riflettere. *L’io della mente* contiene centinaia di pagine, dentro le quali l’articolo di Searle è *uno* di tutti quelli presenti.

Però, al termine di diecimila battute, mi sia permesso dire che sentir parlare di *intelligenza artificiale* davanti a un chatbot piatto come un sottobicchiere mi fa venire una gran voglia di tirare schiaffi.

Grazie a **mailmasterc** per l’ispirazione.