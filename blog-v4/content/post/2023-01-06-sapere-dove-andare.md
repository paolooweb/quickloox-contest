---
title: "Sapere dove andare"
date: 2023-01-06T00:00:03+01:00
draft: false
toc: false
comments: true
categories: [Web, Software]
tags: [DnD, Inkarnate]
---
Comunicazione di servizio per i giocatori di ruolo: a occhio, [Inkarnate](https://inkarnate.com/maps/) è un ordine di grandezza meglio per semplicità e risultati a qualsiasi altro creatore di mappe che io abbia provato.

In sé non è una gran garanzia: genero poche mappe e, in genere, fatto qualche tentativo su sistemi troppo complessi o poco produttivi, sono finito per arrangiarmi con gli strumenti rudimentali di [Roll20](https://roll20.net/welcome) più un po’ di copia e incolla.

Inkarnate mi sembra invece il servizio dove veramente, serve una mappa e bene o male la si disegna. Non che venga fuori da sola, certo, però apprezzo molto l’intuitività, mai riscontrata altrove nella stessa misura.

Appena mi tocca, mi ci metto con impegno e vediamo.