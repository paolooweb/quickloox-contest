---
title: "Chissà chi lo sa"
date: 2023-04-23T01:49:22+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [iPhone, Windows Mobile, AI Winter]
---
Mentre leggo esegeti dell’intelligenza artificiale nati professionalmente la scorsa settimana che discettano del grande futuro davanti a ChatGPT, un amico mi manda senza alcun motivo specifico un [bell’articolo di NetworkWorld](https://www.networkworld.com/article/2350353/apple-iphone-doomed-to-failure----windows-mobile-7-plans-for-2009-leaked.html), chissà se esiste ancora, con la spiegazione del perché iPhone è predestinato a soccombere a Windows Mobile 7, nientemeno.

Era il 2009 e un articolo poteva chiudersi con la seguente chiosa:

>Apple iPhone. Godetevi la luce della ribalta, perché non durerà a lungo.

Mi è capitato di rispondere ad alcuni degli esegeti invitandoli a cercare – su un motore di ricerca possibilmente – la locuzione *AI Winter*, spiegando che il momento attuale è un film già visto, in replica. Ma non sono stato capito.

Quelli convinti di sapere come andrà il futuro si autocondannano a rivivere il passato.