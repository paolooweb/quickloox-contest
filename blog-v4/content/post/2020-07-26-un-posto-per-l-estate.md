---
title: "Un posto per l'estate"
date: 2020-07-26
comments: true
tags: [Mud, NeoNecronomicon, Venerandi, Uber, Prompt, iPad]
---
Alla fine di una notte di inverno, vedere il sole sorgere su [Darkshore](https://classic.wowhead.com/darkshore) scalda il cuore. Ma in una notte di estate, vince l’estate; la natura vera batte quella del gioco di ruolo di massa.

L’unica cosa che può battere la natura vera è l’immaginazione e questo significa che la via più attuale per godersi una notte insonne estiva è quella più tradizionale: un Mud.

Niente a che vedere con il [Modello unico di Dichiarazione Ambientale](https://it.wikipedia.org/wiki/Modello_unico_di_dichiarazione_ambientale) (anche se ci vedo lo zampino del burocrate che prima di convertirsi al Nulla di stato qualcosa lo ha giocato); si parla invece di [Multi-User Dungeon](https://en.wikipedia.org/wiki/MUD). Tutto testo, tutta immaginazione.

Se lo stato di insonnia è veramente potente, si potrebbe provare l’indicibile: _scrivere_ un Mud. È un atto di disciplina notevole: bisogna pensare a una mappa, immaginarmi le interazioni tra i giocatori, stendere a centinaia quelli che nei master di comunicazione chiamano _microtesti_, esaurienti, coinvolgenti, efficaci, allusivi, precisi, coerenti eccetera. E poi [legare il tutto](https://www.reddit.com/r/MUD/comments/6iusxa/looking_to_make_a_mud/).

Ah, naturalmente la notte insonne servirebbe a _cominciare_ il lavoro. Poi ne servirebbero numerose altre. Probabilmente, alla fine ne sarebbe valsa la pena.

Ho sempre pensato che, da insegnante, avrei fatto costruire un Mud della scuola come progetto multidisciplinare per un anno. Il Mud di un castello, un parco, una villa, un museo, un collegio universitario costituirebbe una attrazione virtuale interessante per completare la promozione di un luogo di interesse turistico, culturale, didattico.

Questa notte insonne intanto la passo a rinfrescare la lista di Mud memorizzati in [Prompt](https://panic.com/prompt/) su iPad. Della manciata di nomi che mi interessavano, è rimasto in piedi solo il [NeoNecronomicon](http://www.neonecronomicon.it), e niente in inglese. Le liste in rete abbondano, vanno provati per vedere se funzionano ancora, un incipit brillante potrebbe conquistarmi. Saprei dove passare la notte.