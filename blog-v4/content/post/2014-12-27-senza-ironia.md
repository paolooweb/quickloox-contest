---
title: "Senza ironia"
date: 2014-12-28
comments: true
tags: [GarageBand, Apple, Natale, iPod, iPad, Flipagram]
---
[Ieri ne facevo](https://macintelligence.org/posts/2014-12-27-falsi-sentimentalismi/) una questione di sentimenti che possono essere compresi e interpretati solo dopo avere vissuto certe situazioni.<!--more-->

Non scrivevo in funzione di oggi, solo che **emeralit** mi ha segnalato [Apple confessa: i nostri device non servono a niente](https://medium.com/italia/ecco-come-apple-vi-rovinera-il-natale-84fb7b8684e) ed esattamente di sentimenti si parla.

La sostanza è che compriamo prodotti Apple solo se siamo *così stupidi da pensare che sia meglio registrare una canzone su GarageBand per nostra nonna, anziché suonargliela dal vivo*.

L’analisi nasce dal video natalizio di Apple [The Song](https://www.youtube.com/watch?v=WRsPnzcZ1VY). L’idea dell’autore è che la ragazza avrebbe fatto molto meglio a suonare davanti alla nonna invece che farle trovare un iPod da azionare.

>È la pubblicità di Natale della Apple, e **l’azienda vi sta dicendo che fareste meglio a non comprare i suoi prodotti se volete essere felici**.

L’autore non ha colto che il vinile protagonista del video non è un 45 giri commerciale, ma un messaggio vocale registrato tanti anni fa dalla nonna per il suo compagno lontano, come viene spiegato nel [video di commento allo *spot*]((https://www.youtube.com/watch?v=77hi9SuKfiI&feature=youtu.be)).

Basterebbe questo a decifrare con successo il messaggio: la nipote lascia un messaggio alla nonna invece che suonarlo dal vivo, proprio come la nonna aveva inviato un messaggio invece di cantare davanti al proprio amore. La canzone è la stessa e costruisce un ponte tra generazioni. Ovviamente le emozioni della nonna non sono di quelle da lasciare trasparire di fronte a una nipote giovane che suona gagliarda la chitarra: iPod nello *spot* non è un *muro mediale*, semmai un tramite perché il messaggio possa essere fruito dalla nonna in modo confidenziale e discreto. La nipote osserva, ma a distanza, con tenerezza e rispetto per le emozioni personali e intime della nonna.

Tutto questo è stato malcompreso.

O forse no. Diciamo che l’autore del pezzo ha inserito una carica di ironia e paradosso che non ho avvertito. Gli è tutto chiarissimo, ha solo operato un sano sarcasmo verso la comunicazione di una multinazionale, cosa che non ho saputo cogliere.

Certamente ho vissuto un Natale nel quale mi è stato fatto un regalo tramite iPad e mediante [Flipagram](https://flipagram.com). Mi ha reso felice. Senza iPad e Internet non si sarebbe potuto fare. La tesi centrale dell’articolo è che senza i *device* di questo tempo l’esperienza raccontata nello *spot* sarebbe stata migliore. La mia, di esperienza, sarebbe stata impossibile e ciò depone a sfavore della tesi in questione.

Quello *spot* può essere capito pienamente avendo vissuto certe situazioni. Forse l’autore del pezzo non le ha ancora attraversate.

Prima di distribuire complesse patenti di felicità e di stupidità, in ogni caso, imparerei a scrivere *GarageBand*, che è semplice. Senza ironia.

<iframe width="560" height="315" src="//www.youtube.com/embed/WRsPnzcZ1VY" frameborder="0" allowfullscreen></iframe>

<iframe width="560" height="315" src="//www.youtube.com/embed/77hi9SuKfiI" frameborder="0" allowfullscreen></iframe>