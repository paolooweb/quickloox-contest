---
title: "Librerie e librai"
date: 2015-12-16
comments: true
tags: [SymPy, WolframAlpha, Python, Gamma]
---
Sarò velocissimo: adoro scoprire cose come [SymPy Gamma](http://www.sympygamma.com).<!--more-->

Si può vedere come un [WolframAlpha](http://www.wolframalpha.com) *de noantri*, oppure come il frutto di una [libreria Python](http://www.sympy.org/en/index.html) che è affascinante usare e, per una persona sufficientemente motivata, potrebbe diventare campo di applicazione, per migliorare la libreria e farla progredire. In pratica, dall’usare la libreria a diventare libraio, anche se parliamo di programmazione.

È una cosa semplice, accessibile a chiunque, eppure ha piani di fruizione variegati e chi lo volesse fare, potrebbe mettere le mani dentro il cofano. O farle mettere a ragazzi che hanno da imparare: ci sono insegnanti disposti a correre il rischio di sporcarsi le mani con SymPy, anche se non compare nei programmi ministeriali?