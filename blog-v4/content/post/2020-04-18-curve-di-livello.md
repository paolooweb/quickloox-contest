---
title: "Curve di livello"
date: 2020-04-18
comments: true
tags: [Drang, Python, shell, Terminale, pandas, coronavirus, Mathplotlib]
---
In questi giorni vedo diverse persone con la dovuta preparazione matematica che recuperano i dati pubblicamente dispnibili sulla pandemia e aggiornano grafici sull’andamento dei contagi.

Ma ho trovato solo Dr. Drang che [lo lascia fare al computer](https://leancrew.com/all-this/2020/03/daily-updated-covid-19-cases/).

I suoi grafici [vengono aggiornati da una combinazione di Python, comandi da Terminale e calendarizzazione di eventi sul computer](https://leancrew.com/all-this/2020/04/launch-agents-and-environments/): il reperimento dei dati e il disegno delle curve avvengono con frequenza regolare durante tutta la giornata e così non esiste il rischio di perdere un aggiornamento della base dati a causa di un crash o un blackout.

Come tutte le idee brillanti, una volta letto come fa, appare evidente come si possa fare in modo facile a sufficienza da non essere derubricato a pazzia da *nerd*.

Dedicato ai nostalgici di quando era bello smanettare sul computer e si imparava sempre qualcosa, mentre ora si è perso il gusto.

Si sarebbe perso, al massimo, perché una soluzione di questo tipo può essere affrontata da chiunque abbia voglia di buttare via un paio di serate. È ampiamente fattibile, costa nulla, c’è da imparare un sacco, il risultato è di livello qualitativo buono. Lo trovo stimolante e gustoso. L’unica controindicazione possibile sarebbe l’idea di dedicarsi all’elaborazione di dati riguardanti faccende poco allegre; ma in fondo è un modo per esorcizzarle e per accrescere la consapevolezza.

Passerà il virus e la conoscenza invece resterà.
