---
title: "Anno dopo anno"
date: 2015-07-06
comments: true
tags: [Apple, Jobs, iPhone, Microsoft]
---
Tutto può sempre cambiare, ma oggi Apple è un gigante tecnologico di dimensioni che raramente si sono viste. iPhone da solo è un business più grosso dell’intera Microsoft, quasi duecento miliardi in denaro pronta cassa (relativamente, perché intrappolato fuori dagli Stati Uniti in buona parte, ma certo non è virtuale), soprattutto l’azienda è in grado di essere la più capitalizzata del mondo senza monopolizzare alcun mercato – anzi – e anche se pochi lo notano sta procedendo a passi di entità notevole nel ricomprarsi le proprie azioni.<!--more-->

Apple società per azioni, ma proprietaria di se stessa? Sembra un obiettivo superiore persino alle sue possibilità e probabilmente non è mai stata nemmeno un’idea, ma non è impossibile. Come sarebbe pressoché per chiunque altro.

Il che dovrebbe portare a riconsiderare le ipotesi che si leggevano anni fa: Apple si basava su Steve Jobs che mandava avanti da solo un’attività multimiliardaria e, scomparso lui, sarebbe iniziato il declino.

Apple, oggi, è molto, ma molto più grande dell’epoca d’oro di Steve Jobs.

La lettura migliore per rendersi conto della situazione, senza il fastidio di procurarsi un libro, rimane sempre [questo lungo articolo di Fast Company](http://www.fastcompany.com/3042433/steve-jobs/the-real-legacy-of-steve-jobs) scritto per lanciare [Becoming Steve Jobs](http://www.amazon.com/Becoming-Steve-Jobs-Evolution-Visionary/dp/0385347405/ref%3Dtmm_hrd_swatch_0).

Se ne è già parlato, ma passano gli anni e il successo di Apple aumenta invece che languire. Dunque, le teorie sulla mancanza di creatività o di prodotti rivoluzionari in mancanza di Jobs diventano sempre più risibili.

Giusto un estratto dal’articolo:

>Jobs aveva un genio per sintetizzare ciò che Apple imparava con l’allargamento della propria linea di prodotti. Ciò di cui si sono cibati iPod, iPhone, e iPad per tendere alla perfezione, ognuno dei quali era una nuova versione di qualcosa che altri avevano tentato, fallendo, di portare con successo sul mercato. Steve è stato l’epitome di un tecnologo, nel senso che la tecnologia è più spesso che no un prodotto di pensiero ricombinante. La maggior parte delle “nuove” tecnologie sono in realtà nuove combinazioni di tecnologie indipendenti che, assemblate, creano qualcosa di nuovo in virtù delle loro sinergie. I cicli di prodotto con miglioramenti incrementali di Apple sono quello che diede a Steve i materiali grezzi per visualizzare il futuro. Le sue visioni non sono scaturite dal nulla.

E neanche aveva il nulla intorno in termini di talenti, idee, capacità. Anno dopo anno, si vede che Apple è stata ed è un’azienda, non una *one man band*. Un’azienda assai difficile da comprendere per molte menti omologate.

