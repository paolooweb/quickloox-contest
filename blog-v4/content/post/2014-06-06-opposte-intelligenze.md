---
title: "Opposte intelligenze"
date: 2014-06-06
comments: true
tags: [iOS, Android, Apple, iCloud, Evans, cloud]
---
Spettacolare [articolo di Benedict Evans](http://ben-evans.com/benedictevans/2014/6/4/digesting-wwdc-cloudy) sulla differenza di approccio tra Apple e Google all’informatica odierna, che le rende straordinariamente complementari e comprensibilmente acerrime avversarie.<!--more-->

Solo un brevissimo passaggio che però illumina. Vale la lettura integrale:

>Per Google, gli apparecchi sono vetro stupido e l’intelligenza risiede nel cloud. Per Apple il cloud è contenitore stupido e l’intelligenza risiede negli apparecchi.

Si capiscono un sacco di cose su iCloud, il confronto tra iOS e Android, il perché Apple non si mette – per fortuna e per ora – a produrre iPhone a prezzo stracciato e via discorrendo. Non si trovano spesso pezzi così.