---
title: "Imprese da consumatori"
date: 2018-07-26
comments: true
tags: [Backblaze]
---
Non c’è niente da fare: i resoconti periodici di Backblaze sul funzionamento del loro esercito di dischi rigidi sono una delle letture regolari indispensabili su Internet.

Backblaze si è anche accorta che si tratta di un eccellente sistema per farsi pubblicità e guadagnare reputazione, per cui si impegna a rendere interessante anche nella trattazione numeri che già lo sarebbero per loro conto.

Questo trimestre il tema principale è il [confronto in affidabilità tra dischi consumer e dischi enterprise](https://www.backblaze.com/blog/hard-drive-stats-for-q2-2018/).

La base dati usata per tirare le conclusioni lambisce i centomila dischi e i nove milioni di giorni/disco. Come dire, ne sanno qualcosa.

Il risultato è un *dipende*, perché non c’è una scelta per forza migliore: le due categorie di dischi hanno punti di forza diversi e ugualmente differenti punti deboli. Quindi non esiste una direzione obbligata per ottimizzare un acquisto, ma invece è necessario avere le idee chiare su che cosa desideriamo da un disco rigido.

Consiglio vivamente la lettura integrale. E anche la memorizzazione, se parliamo di dischi…