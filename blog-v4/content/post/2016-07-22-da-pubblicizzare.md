---
title: "Da pubblicizzare"
date: 2016-07-22
comments: true
tags: [Gossage, Moruzzi, advertising]
---
Dopo avere parlato delle [letture arretrate](https://macintelligence.org/posts/2016-07-14-letture-arretrate/) mi rendo conto di averne trascurata una compiuta: [What Happened to Advertising? What Would Gossage Do?](https://www.createspace.com/5357009) dell’amico [Massimo Moruzzi](https://www.dotcoma.it).<!--more-->

Il prodotto costa 3,99 dollari e si può avere anche a meno impegnandosi due minuti con Google, ma vale la cifra. Massimo conosce il mondo della pubblicità come pochi e ha scritto un *pamphlet* dedicato al mondo della pubblicità su web e, in second’ordine, al rapporto delle aziende con i *social media*.

Non avendo peli sulla lingua, ha detto esattamente come stanno le cose, esposto nella maniera più chiara e possibile i problemi e anche lasciato intravedere qua e là soluzioni. Non piacevoli, ma appunto, ci sono dei problemi di cui molti non sembrano voler avvertire la realtà e la portata.

Il testo è veramente corto e si legge di mezzo fiato, neanche uno intero. Ma è denso e contiene una quantità di riferimenti paurosamente elevata. Se appena uno si mette a seguire le note oltre che l’esposizione, rischia di acquisire l’inizio di una vera preparazione nel settore. A partire dalla figura di Howard Gossage, che trovo affascinante.

No, dai, non su Wikipedia. Su un libro vero. Per una volta.