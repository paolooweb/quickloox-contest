---
title: "Lavoro che non si vede"
date: 2018-02-07
comments: true
tags: [HomePod, Gruber, Siri]
---
Due estratti dalla molto raccomandata [recensione di HomePod scritta da John Gruber su Daring Fireball](https://daringfireball.net/2018/02/homepod). Il primo:

>La comprensione dei comandi vocali è rapida e precisa. La caratteristica più interessante è che HomePod riesce a sentire i comandi anche se si parla a voce normale mentre suona musica a volume alto. Vorrei sbilanciarmi: HomePod capisce comandi a volume normale mentre suona la musica meglio di quanto facciano le orecchie umane. […] Intuitivamente si pensa di dover parlare *sopra* la musica per farsi sentire. E, con Echo di Amazon, è così. Con HomePod, invece, no. Sussurrare non basta né HomePod legge le labbra [come Hal](https://youtu.be/QFSE4dUJYM8?t=2m12s), ma vale la pena di sottolineare che non c’è bisogno di gridare.<!--more-->

Il secondo:

>Un messaggio ricevuto da un amico: “Penso che chiunque si metta volontariamente in casa una cosa del genere sia matto. Alexa perché Amazon deciderà di farci cose sempre più terribili fino a che la fermano, Google e Apple perché prima o poi qualcuno ne abuserà per farci cose terribili”.<br />
Non sono in disaccordo, ma la mia posizione è leggermente diversa. Non direi che uno non è pazzo se non vuole un altoparlante intelligente in casa. Ma il futuro è in arrivo e ci ascolterà (oltre che guardarci). Mi fido di Apple più che di Google e mi fido di Google più che di Amazon. Anche così, tengo un’Alexa nella mia cucina da un anno. Tengo gli occhi bene aperti sui rischi della privacy, ma la comodità fa valere la pena di correrli. Preferisco HomePod a Echo perché suona molto meglio, ma un altro vantaggio è la maggiore comodità.

Chi ricorda che una Apple era famosa per la sua interfaccia umana e per l’attenzione ai dettagli? Apparentemente produce l’unico altoparlante al mondo che consente di parlare a voce normale per regolarlo. Sembra poco? È di più. È lavoro che non si vede e non viene esibito fino a che, improvvisamente, si rivela.

E la *privacy*. La mia posizione è al novantanove percento quella di Gruber. Stare attenti è doveroso, ma le chiusure pregiudiziali rischiano di far perdere qualcosa.