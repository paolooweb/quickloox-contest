---
title: "Giova ripetere"
date: 2022-12-23T04:08:22+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [PCalc]
---
Certe soddisfazioni nell’avere un blog attivo da più di un mese stanno nella certezza di avere già parlato di [PCalc nel suo venticinquesimo anninversario](https://macintelligence.org/posts/2017-12-27-alla-venticinquesima/), ora che ricorre il trentesimo.

Nel post di cinque anni si trovano i link giusti, compreso quello alla storia ufficiale del programma, puntualmente aggiornato dai suoi creatori.

In più è presente anche un sintetico commento, di cui continuo a sottoscrivere ogni sillaba anche se il 2017 è talmente vecchio che doveva ancora nascere la secondogenita.

Ripetere può giovare all’umore e alla sensazione di essere sulla pista giusta di tanto in tanto, un po’ come il famoso orologio rotto che comunque segnava l’ora giusta due volte al giorno (prima del digitale, *of course*).