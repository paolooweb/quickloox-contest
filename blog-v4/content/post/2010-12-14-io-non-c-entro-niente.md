---
title: "Io non c'entro niente"
date: 2010-12-14
comments: true
tags: [Jobs, Gizmodo, Lifehacker, Gawker, Gnosis]
---
Nei giorni scorsi sono stato poco gentile verso Lifehacker per la sua <a href="https://macintelligence.org/posts/2010-12-10-tecniche-di-disinformazione/">pretesa di definire come migliori in assoluto editor di testo che piacciono alla redazione</a> o che votano i suoi lettori, come se l&#8217;una o gli altri fossero padreterni che possono permettersi di dare patenti assolute di *migliore* o *peggiore* (senza neanche aggiungere un piccolo *editor&#8217;s choice*, la scelta della redazione, come da secoli si fa con onest&#224; nel mondo dell&#8217;informazione).<!--more-->

Lifehacker si picca di dare consigli su qualsiasi cosa, per esempio <a href="http://lifehacker.com/184773/geek-to-live--choose-and-remember-great-passwords">come scegliere (e ricordare) grandi password</a>.

Non *le migliori* password, stavolta, e si vede: tutti i siti dell&#8217;editore (Gawker Media) sono stati <a href="http://lifehacker.com/5712975/lifehacker-data-breach-qa-were-here-to-help?skyline=true&amp;s=i">violati dai pirati</a> e adesso su Internet gira allegramente un file da cui chiunque pu&#242; provare a derivare nomi e password di chiunque si fosse registrato a Lifehacker.

Qualcuno dei fortunati ha chiesto a Gawker Media di poter cancellare il proprio account, per evitare di correre nuovamente rischi simili in futuro. Al momento <a href="http://lifehacker.com/5712785/#5">&#232; impossibile</a>. Chi entra nelle grazie di Lifehacker non &#232; previsto possa uscirne.

Tra le propriet&#224; di Gawker Media &#8211; ovvero tra i parenti stretti di Lifehacker &#8211; sta Gizmodo, il sito che con grande finezza e rispetto, a dicembre 2008, <a href="http://gizmodo.com/5120687/steve-jobs-health-declining-rapidly-reason-for-macworld-cancellation">diede Steve Jobs per morto entro la primavera 2009</a>, al solo scopo di raccogliere ascolti e fare soldi.

Il gruppo di *hacker* autore della violazione, autobattezzatosi *Gnosis*, ha dichiarato <a href="http://www.mediaite.com/online/exclusive-gawker-hacker-gnosis-explains-method-and-reasoning-behind-his-actions/">ce la siamo presa con Gawker a causa della loro ostentata arroganza</a>.

Volevo solo dire che io non c&#8217;entro niente e mi sono solo un po&#8217; scandalizzato per la scorrettezza deontologica.

A meno che non fossero la punta di qualche *iceberg* di scorrettezze pi&#249; vasto.