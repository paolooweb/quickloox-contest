---
title: "La chiave della bottiglia"
date: 2016-08-17
comments: true
tags: [Windows, sicurezza, Ars, Technica, Surface]
---
Qualcuno si ricorda che all’indomani della sparatoria di San Bernardino l’Fbi chiese ad Apple di creare uno strumento per sbloccare la cifratura degli iPhone e l’azienda si impuntò, spiegando che sarebbe stata indebolita la sicurezza di tutti gli utilizzatori e scatenando infinite discussioni?

Ora arriva la prova del nove: Microsoft, [scrive *Ars Technica*](http://arstechnica.com/security/2016/08/microsoft-secure-boot-firmware-snafu-leaks-golden-key/), ha accidentalmente diffuso una chiave universale che consente di aggirare la cifratura di base di alcuni apparecchi, tra i quali le tavolette Surface.

Chiunque sappia usare la chiave può installare sui Surface il sistema operativo che desidera, ma questo è il meno: qualunque malintenzionato tecnicamente esperto può sviluppare attacchi che la sicurezza intrinseca del’apparecchio non è in grado di arrestare.

La chiave doveva restare custodita e impenetrabile; purtroppo un errore umano l’ha lasciata installata dentro alcuni sistemi messi sul mercato e  la cosa è passata sotto silenzio per pochissimo tempo prima di venire a galla. Non è questione di volere o non volere, o di intenzioni buone o cattive: se la chiave esiste, prima o poi non sarà più segreta. Figuriamoci se comincia a girare per commissariati e aule di tribunale.

La cosa interessante è che Microsoft ha già fatto uscire tre aggiornamenti di sistema a seguito della faccenda e tutti con esito parziale. Sfuggito il genio dalla bottiglia, sembra impossibile farlo rientrare del tutto.

Ancora più interessante, Microsoft ha la chiave di sblocco che Apple si è rifiutata di creare aprendo così il contenzioso con la polizia federale americana. Chissà se ne ha altre, magari riguardanti Windows desktop. Chissà se sono segrete abbastanza o ci sarà un altro errore umano. Chi vuole si fidi, eh? Nessun problema per me. Io sto con quelli che la chiave non la creano.
