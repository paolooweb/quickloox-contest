---
title: "Polemiche, grandi polemiche e soluzioni"
date: 2016-12-15
comments: true
tags: [Viticci, iPad, Pro]
---
Grandi discussioni sull’idea che iPad Pro venga presentato come computer da scrivania o, quanto meno, una possibilità di computer da scrivania. Oltre naturalmente a essere ideale da portare in giro.<!--more-->

Discussioni, dibattiti, chiacchiere e poi Federico Viticci scrive su MacStories di [un anno vissuto usando iPad Pro come computer da scrivania](https://www.macstories.net/stories/one-year-of-ipad-pro/).

Prima di esprimere qualunque commento va considerata la mole di lavoro (lo stesso articolo viene passato come ebook agli iscritti a pagamento a MacStories). Ecco: quello che si vede è stato creato ed elaborato con un iPad Pro. Così si chide l’introduzione di Federico:

>Grazie al tempo che ho impiegato nella comprensione e nella sintonia fine del mio iPad Pro mi è stato possibile impegnarmi su un numero maggiore di progetti, raddoppiare la crescita di MacStories e coordinare una squadra di lavoro più ampia.

I problemi risolti, in altre parole, non sono proprio quelli di un ragazzino alle prese con i compiti o del cuoco che carica sulla tavoletta le ricette di cucina. Mi verrebbe da dire, ahi ahi, che sono problemi notevolmente… professionali.

E lo dico da felicissimo utente Mac, che non contempla l’idea di abbandonarlo. Anzi, qualcun altro in famiglia sta per cambiare Mac.