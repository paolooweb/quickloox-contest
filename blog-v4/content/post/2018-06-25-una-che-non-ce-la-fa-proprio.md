---
title: "Una che non ce la fa proprio"
date: 2018-06-25
comments: true
tags: [MacBook, tastiera, Johnston]
---
[Apple ammette che i suoi computer non funzionano](https://theoutline.com/post/5052/apple-admits-its-computers-are-broken). Niente male come *scoop*, eh? È la traduzione di Casey Johnston della frase [Apple ha avviato un programma di sostituzione gratuita delle tastiere *a farfalla* difettose](https://www.apple.com/it/support/keyboard-service-program-for-macbook-and-macbook-pro/) su vari modelli di MacBook e MacBook Pro. Gli iMac, per dirne una, funzionano nonostante il titolone.

(L’effetto di sottolineatura dei link nell’articolo di Johnston è peraltro ben pensato. Detto a margine).

Se non funzionassero, *tutti*, ci sarebbe un programma di richiamo dei computer, non di sostituzione delle tastiere, effettuata la quale i computer insisterebbero nel non voler funzionare.

Quanti non funzionano, sul totale? Apple parla di *small number*, certamente *pro domo sua*. Johnston non lo sa – o sarebbe ansiosa di dircelo – e così, per non sbagliare, parla come se fossero il cento percento.

Un valore appena appena eccessivo. Scrivevo [Uno su dieci non ce la fa](https://macintelligence.org/posts/2018-04-14-uno-su-dieci-non-ce-la-fa/) e ho sbagliato a dare a intendere che potesse essere un insieme chiuso, mentre intendevo una percentuale. Se i computer interessati sono un milione, e quelli difettosi sono, sparo a casaccio, il tre percento di un milione, il consiglio *non comprateli* che chiude l’articolo è un’offesa al pensiero logico. Di una che proprio non ci arriva.