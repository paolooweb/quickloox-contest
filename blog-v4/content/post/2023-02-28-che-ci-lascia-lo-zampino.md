---
title: "Che ci lascia lo zampino"
date: 2023-02-28T16:48:57+01:00
draft: false
toc: false
comments: true
categories: [Software, Internet]
tags: [Hofstadter, Douglas Hofstadter, Concetti fluidi e analogie creative, Fluid Concepts and Creative Analogies, Apple, Tesler, Larry Tesler, Fluid Analogies Research Group, FARG, FARGonauti, FARGonauts, Bowen, Barbara Bowen]
---
I tempi lo esigono: provo a rispolverare la lettura di [Fluid Concepts and Creative Analogies](https://macintelligence.org/posts/2023-01-09-intelligenza-cercasi/), il libro di Douglas Hofstadter dedicato al suo lavoro pluridecennale sulle *scienze cognitive*, termine che ha adottato dopo avere visto la confusione che si era creata attorno a *intelligenza artificiale*.

Si parla di quarant’anni fa ed ecco che sembra oggi.

Due cose che non ricordavo: la prima, gli ultimi anni di ricerca da parte del Fluid Analogies Research Group (i FARGonauti) si sono svolti in Italia, presso l’[Istituto di Ricerca Scientifica e Tecnologica di Trento](https://it.wikipedia.org/wiki/ITC-irst). La seconda, questa frase presa dall’introduzione:

>Anche Larry Tesler e Barbara Bowen presso Apple Computer si sono assicurati che potessimo proseguire il lavoro anche in tempi di scarsità di fondi, attingendo per noi al budget stanziato da Apple per la ricerca esterna. Questo supporto è stato davvero senza prezzo e continuiamo a esserne grati.

Prima o poi si farà qualche *podcast* dedicato all’intelligenza artificiale dal punto di vista Apple – lo dico così, senza niente di preciso in mente, eh – e dovranno saltare fuori tante informazioni come questa. Apple ha avuto e ha lo zampino in iniziative che non sempre abbiamo presente, al momento di discutere.