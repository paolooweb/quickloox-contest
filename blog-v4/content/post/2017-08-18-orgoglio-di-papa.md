---
title: "Orgoglio di papà"
date: 2017-08-18
comments: true
tags: [Lidia, iPad, Mac]
---
Un piccolo aggiornamento sulle avventure del componente più giovane della famiglia, visto che si parlava poco tempo fa dei [rischi di dipendenza dalle tecnologie in tenera età](https://macintelligence.org/posts/2017-07-11-disintossicazione-infantile/) e l’età di Lidia avanza a grandi passi, ma rimane tuttora abbondantemente sotto i quattro anni (per l’esattezza, tre e una manciata di ore).

La figlia ora dispone di un proprio account sul mio Mac, sottoposto ai controlli genitoriali di macOS. Lo usa primariamente per [guardare i salvaschermo preferiti](http://www.macintelligence.org/blog/2017/06/22/una-storia-di-design-interfacce-e-esperienza-di-utilizzo/) in grembo a papà. Entra nel proprio account digitando una password facile (il suo nome) sulla tastiera e ha bisogno di assistenza per selezionare il salvaschermo del momento, ma ne fa partire l’anteprima sempre da sola con un clic sul *trackpad*.

La sera si addormenta dopo avere giocato un po’ a [Bee Leader su iPad](https://itunes.apple.com/it/app/bee-leader/id480886220?mt=8). Quando le si dice di mettere via il gioco (e iPad) tergiversa e ci vuole anche un paio di minuti per farsi obbedire. Dopo di che, mette via iPad. E si addormenta nel giro di un quarto d’ora.

Non ho link dimostrativi immediati al momento, ma ho letto da più parti e in più occasioni del problema della luce blu di tavolette e telefoni, che disturba il sonno, eccita i bimbi oltre la soglia eccetera.

Sarà tutto verissimo, frutto di studi e indagini da parte di luminari del campo. La mia famiglia comunque rappresenta una felice eccezione, certamente per coincidenza. Mia figlia mette via iPad e si addormenta.

Il papà è orgoglioso. Prima che per la figlia (la quale cresce abbondantemente con supervisione, ma anche senza) per quell’iPad di prima generazione che non perde un colpo.