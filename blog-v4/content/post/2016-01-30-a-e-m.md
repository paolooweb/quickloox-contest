---
title: "A & M"
date: 2016-01-30
comments: true
tags: [Apple, Microsoft]
---
Due aziende, A e M.

Durante gli ultimi tre mesi del 2015, [A](http://www.apple.com/pr/library/2016/01/26Apple-Reports-Record-First-Quarter-Results.html) ha aumentato del due percento il fatturato e i profitti del due percento rispetto allo stesso periodo del 2014. Tra le altre cose, A produce computer da tasca e anno su anno ha venduto grosso modo lo stesso numero di terminali, con una crescita del fatturato dell’uno percento.<!--more-->

Nello stesso periodo, [M](http://arstechnica.com/business/2016/01/cloud-surface-are-the-highlights-of-microsofts-23-8-billion-quarter/) ha visto diminuire il fatturato del dieci percento e i profitti del quindici percento. Tra le altre cose, M produce computer da tasca: il loro fatturato si è peggio che dimezzato e le unità vendute sono diminuite complessivamente del quaranta percento circa.

Come saranno trattate dai media e dagli analisti le due aziende, in base ai loro risultati?