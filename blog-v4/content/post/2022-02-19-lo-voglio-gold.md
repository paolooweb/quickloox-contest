---
title: "Lo voglio gold"
date: 2022-02-19T01:41:42+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [Google Slides, Laura Javier, Steve Throughton-Smith, Catalyst, Harvard, PowerPoint]
---
Si chiama Laura Javier e ha avuto il fegato di dire in pubblico [In realtà Google Slides fa ridere](https://medium.com/@laurajavier/google-slides-is-actually-hilarious-83c1ced857ee). Di mestiere fa la designer, non la virologa oppure l’influencer, quindi sembrerebbe fuori dallo spirito del tempo: una persona che dà giudizi ed è titolata a farlo perché si tratta del suo campo di professionalità. Incredibile, vero?

È la punta di un iceberg che dovrebbe venire a galla. In questi giorni, più uso software diverso da quello fondamentale e più mi infastidisco per falle di design piccole, grandi, in piena vista o nascoste, sempre e comunque irritanti.

Succede a tutti i livelli: si legga l’analisi di Steve Throughton-Smith sulle [deficienze di Catalyst](https://www.highcaffeinecontent.com/blog/20220216-Where-Mac-Catalyst-Falls-Short), la tecnologia software per portare su Mac i programmi destinati a iPadOS. È una cosa per sviluppatori, che noi utilizzatori comuni non vedremo mai. Anche in questo caso ci sono problematiche eclatanti in un pezzo di software che oramai ha l’età misurabile in anni.

Del resto nessuno ha mai ritirato o modificato l’articolo che gira credo da sette anni su quello studio della Harvard University (non esattamente scappati di casa) che invita a [smettere di usare PowerPoint per evitare di danneggiare la propria reputazione e quella del proprio datore di lavoro](https://www.forbes.com/sites/paularmstrongtech/2017/07/05/stop-using-powerpoint-harvard-university-says-its-damaging-your-brand-and-your-company/#7af563623e65).

Google, Apple, Microsoft; se non fanno software decente *loro*, vuol dire che c’è qualcosa di grandemente sbagliato e a pagare, in improduttività e frustrazione, siamo noi.

È tempo di ribellarsi, per esempio rivolgendosi al software indipendente e magari open source; per creare una presentazione, gli strumenti non convenzionali abbondano (e la prima cosa da fare sarebbe imparare a presentare, prima di buttare giù slide senza un’idea preventiva di dove andare a parare).

Alternativa: io pagherei tranquillamente per avere macOS, Pages, insomma il software di sistema in edizione *gold*, a pagamento perché ripulita il più possibile vicino al cento percento da bug, stranezze, design improbabili e difetti di progettazione.

È matematicamente inevitabile che, più complesso è un software, più bug si sviluppino. Eppure il mestiere di una grande azienda è proprio combattere contro l’entropia attraverso la realizzazione di prodotti sempre migliori. Ma un conto è il bug, un conto è il difetto di design. Lì la complessità non c’entra, anzi, un software più complesso dovrebbe rappresentare una sfida più motivante per ingegneri del software e progettisti di interfacce.

Se non ci sono i soldi per mettere a posto come si deve il proprio software, che si possa anche pagare. Anche a peso d’oro. Se l’istinto mi conforta, si aprirebbe un mercato di tutto rispetto.
