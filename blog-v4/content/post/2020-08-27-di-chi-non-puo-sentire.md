---
title: "Di chi non può sentire"
date: 2020-08-27
comments: true
tags: [Apple, Gallaudet, SmartFolio, Pencil]
---
Tutti gli studenti e i docenti della Gallaudet University [saranno dotati di un iPad Pro con Apple Pencil e tastiera SmartFolio](https://mailchi.mp/gallaudet.edu/gallaudetsmultifaceted-partnership-with-apple-inc).

Gli studenti neri con disabilità potranno partecipare a un programma speciale Apple di agevolazioni per lo studio.

Gli studenti tutti potranno altresì imparare a progettare app bilingui, in inglese e linguaggio dei segni versione americana.

Di là sentono male, ma vedono lontano. Di qua, sarei felice di scoprire iniziative che valessero anche un terzo di questa. Chi sa, parli.