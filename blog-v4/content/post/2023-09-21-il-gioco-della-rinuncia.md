---
title: "Il gioco della rinuncia"
date: 2023-09-21T17:15:57+01:00
draft: false
toc: false
comments: true
categories: [Software, Hardware]
tags: [Runestone, Health, Movies, Terminale]
---
Da una chiacchierata con la secondogenita (!) mi è uscita una riflessione puerile, che tuttavia condivido.

Puoi tenere tutti i tuoi device… ma puoi usare solo una funzione. Quale scegli?

A me è uscita così:

iPhone: Personal hotspot  
Mac: Terminale  
iPad: [Runestone](https://runestone.app)  
Apple Watch: Salute  
Apple TV: Movies  
Vision Pro: boh? (Vedremo)

Naturalmente è meno di un *divertissement*, giusto un pensiero ozioso. Tuttavia mi ha fatto focalizzare piuttosto bene che cosa effettivamente ci faccio di indispensabile o quasi, con i *device*.