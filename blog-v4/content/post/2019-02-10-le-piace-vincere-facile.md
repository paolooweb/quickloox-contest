---
title: "Le piace vincere facile"
date: 2019-02-10
comments: true
tags: [AI, Starcraft, AlphaZero, Google]
---
Sempre [fautore dell’intelligenza artificiale e sognatore di quella forte](https://macintelligence.org/posts/2017-08-22-intelligenza-umanistica/), magari invecchio, ma la piega che ha preso lo sviluppo software nel campo continua a piacermi poco.

Dopo [scacchi e Go](https://deepmind.com/blog/alphazero-shedding-new-light-grand-games-chess-shogi-and-go/), adesso gli algoritmi – AlphaZero di Google davanti a tutti – si sono affacciati sui giochi multiplayer online, stracciando i campioni umani in discipline come StarCraft II. Solo che [sembrano barare](https://m.slashdot.org/story/351600); cliccherebbero molto più velocemente di quello che sia fisicamente possibile a un umano e vedrebbero tutto il terreno di gioco anziché solo la parte libera dal *fog of war*, la foschia che limita la visuale dell’umano.

La prestazione è chiaramente fuori discussione. AlphaZero clicca velocissimo, disumano. No problem, è un computer, può consultare tutte le partite di scacchi giocate nella storia prima di muovere un pedone. È fatto per quello.

È che manca il passo in avanti. Forza bruta, velocità, ricerca infinita le abbiamo già viste. L’intelligenza, naturale e artificiale, sta più in là. Difatti, messo a cliccare come un umano con una visuale di gioco limitata allo stesso modo, AlphaZero avrebbe perso.

Non voglio fare il brontolone e dire che avrei voluto AlphaZero mettere il broncio dopo la sconfitta, o dichiararsi emozionato nel prendere posto al tavolo del campione mondiale; è chiaro che i nostri ricercatori non hanno nemmeno l’idea di come dare la coscienza a una macchina e ci vorranno decenni.

Però qualcuno dovrebbe lavorare su quello, anche se i fondi piovono sulle iniziative da ricerca bruta (anche se raffinata, rimane bruta). All’umanità serve un assistente, non un tuttofare. Qualcosa che sappia anche perdere, se è vera intelligenza.
