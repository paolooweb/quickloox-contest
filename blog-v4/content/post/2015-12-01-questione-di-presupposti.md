---
title: "Questione di presupposti"
date: 2015-12-01
comments: true
tags: [Speirs, iPad, MacBook, Pro, Apple, Pencil]
---
Devo a [Mario](https://multifinder.wordpress.com) questa [gemma micidiale](http://www.speirs.org/blog/2015/11/30/can-the-macbook-pro-replace-your-ipad) di Fraser Speirs: *MacBook Pro può sostituire il tuo iPad?*<!--more-->

Lo svolgimento del tema non lascia adito a dubbi. Un portatile Mac è penalizzato in mille modi rispetto a un iPad: dalla forma a L per cominciare e dalla mancanza di uno schermo *touch* che ne complica ulteriormente l’utilizzo in piedi.

Non parliamo degli ingombri dimensionali e del peso, della batteria inferiore, della mancanza di una periferica come Apple Pencil e di un’area di input tremendamente inferiore.

Vogliamo parlare della videocamera su un solo lato dello schermo, della tastiera hardware legata a una singola lingua al momento dell’acquisto, la confusione della moltitudine di porte di connessione (con alcune eccezioni), la mancanza di certe app dedicate che su iPad sono semplicemente irrinunciabili, la mancanza di connessione cellulare? E poi, una parola sola: prezzo.

>Se ricadi in certi flussi di lavoro molto specifici e in un ambiente di lavoro dove sono garantite una sedia e una scrivania, probabilmente è possibile svolgere il proprio lavoro con un MacBook Pro. Per il resto del mondo, c’è iPad.

Questa sarebbe la conclusione dell’articolo. Però c’è una nota finale della redazione che riporta, riferisce, comunica che, no, se lo scrivo finisce il gusto, non lo posso dire, ma che tentazione, ecco, mettiamola così: questione di presupposti.

Quella riga e mezzo va letta in originale, sulla pagina originale.
