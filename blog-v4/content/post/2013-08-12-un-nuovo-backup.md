---
title: "Un nuovo backup"
date: 2013-08-12
comments: true
tags: [Mac]
---
Preso atto della [realtà delle cose](https://macintelligence.org/posts/2013-08-05-se-manca-time-machine/), il mio nuovo *backup* Time Machine è un [Rugged Triple LaCie](http://www.lacie.com/it/products/product.htm?id=10553).<!--more-->

Capacità un terabyte, per offrire un’ottima copertura oggi e una copertura decente un domani che avessi un disco di sistema più capiente degli attuali 320 gigabyte.

Tripla interfaccia: oggi FireWire 800 principalmente per lasciare libere le porte Usb, domani probabilmente Usb 3.0, comunque Usb 2.0 per tutti i casi di emergenza. Niente Thunderbolt: aumenta inutilmente i costi attuali, per un disco di *backup* dove la velocità ha rilevanza zero. Difatti ho scartato la versione veloce da 500 gigabyte, a favore della capienza doppia. Neanche considerata l’opzione Ssd, inutile su un disco di backup e buona solo ad alzare i costi.

Portatile, autoalimentato, per fare tutto da solo in modo il più possibile invisibile. La mia attenzione al *backup* deve essere zero, visto come il computer possa occuparsene al cento percento.

Devo ancora spacchettare il tutto. Unica lamentela, a prescindere: so già che passerò i primi venti minuti a cancellare, disattivare, neutralizzare tutto il software lodevolmente incluso da LaCie. E la protezione dei dati, e dieci gigabyte di cloud, e automazione del backup e chissà che cos’altro.

Sono tutte funzioni automatiche oppure che risolvo su un Mac standard con un clic su una casella. Sarò vecchio stampo eppure un disco rigido preso per fare *backup* ha da funzionare come *backup*. E se domani uscisse una edizione che fa risparmiare un centesimo sul prezzo in cambio della rinuncia a tutto il software di bordo inutile, la perseguirei con tutte le mie forze, in nome del principio e non del risparmio.