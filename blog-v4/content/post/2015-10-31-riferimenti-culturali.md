---
title: "Riferimenti culturali"
date: 2015-10-31
comments: true
tags: [Wikipedia, Emojipedia]
---
Ho un rapporto bipolare verso Wikipedia. L’idea è straordinaria, grandiosa, una di quelle che giustificano il posto dell’umanità sul pianeta.<!--more-->

La realizzazione è vergognosa, crassa, storta, indegna, omologata, codina, bigotta.

Eppure dovremmo tutti metterci di buona volontà per trovare cinque minuti nei quali scrivere su Wikipedia.

Nel contempo, tutti dovremmo eliminare qualsiasi tempo dedicato a leggerla e il mondo sarebbe un posto migliore.

È complicato, lo so, e ho una giustificazione molto valida per ogni singola affermazione, mentre ne ho zero per la loro somma.

Figuriamoci adesso che è emersa la [Emojipedia](http://blog.emojipedia.org/ios-9-1-emoji-changelog/), come mi posso sentire.