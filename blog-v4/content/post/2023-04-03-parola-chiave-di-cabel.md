---
title: "Parola (chiave) di Cabel"
date: 2023-04-03T15:15:41+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [Impostazioni, macOS, Cabel Sasser, Sasser, Panic, 1Password]
---
Capita anche che Apple faccia cose buone e non lo sappia, oppure le tenga nascoste in posti improbabili del sistema.

Ne è prova che bisogna appoggiarsi a persone intelligenti esterne, per esempio Cabel Sasser di Panic Software, quando sostiene che [la gestione delle password di Apple merita una app](https://cabel.com/2023/03/27/apple-passwords-deserve-an-app/). Il perché è presto detto, con le sue parole:

>Sappiamo tutti che Apple possiede una buona gestione incorporata delle password in macOS e iOS. Ma pochi, molto pochi sanno che la gestione suddetta può anche: autocompletare i codici di autenticazione a due fattori, che si possono agevolmente aggiungere con la scansione di un codice QR; tenere un campo Note in cui aggiungere dati supplementari, per esempio codici di backup, per ciascuna password; e importare password esportati da un’altra app, come 1Password! E tutto si sincronizza attraverso i tuoi apparecchi, gratis?!

La proposta di Sasser è intelligente perché tiene le cose semplici e alla fine corrisponde alla missione di Apple. Non viene sottratto mercato a nessuno con una buona app sufficientemente raffinata da meritarsi un pagamento e, nel contempo, si offre una soluzione di base, efficace e semplice, per chi abbia requisiti semplici di gestione password e, oggi, si complica la vita cercando soluzioni esterne perché ignora come tutto si trovi ben nascosto nelle Impostazioni del sistema operativo.

Segno ulteriore che la proposta è intelligente: Sasser, scrive, sogna un futuro nel quale un gestore di password non serva più. Sa anche che ci vuole tempo e che, intanto, una soluzione in macOS e iOS facilmente visibile e visibilmente facile farebbe gran bene a tutti.

Ora manca solo trasmettere il messaggio giusto alla persona giusta.