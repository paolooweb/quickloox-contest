---
title: "Indipendenze di Amazon"
date: 2020-08-04
comments: true
tags: [Amazon, Bezos, PowerPoint, Outlook]
---
Jeff Bezos, l’uomo più ricco del mondo, [proibisce l’uso di PowerPoint nella sua azienda](https://macintelligence.org/blog/2020/01/22/e-solo-un-breve-invito/, una delle più grandi, organizzate ed efficienti del mondo.

Vietare PowerPoint ovunque, sempre, sarebbe stupido. Dovrebbe però essere stupido anche imporlo ovunque e sempre.

Sempre Jeff Bezos verrà ricordato per [come ha impostato la comunicazione tra i reparti di Amazon](https://homepages.dcc.ufmg.br/~mtov/pmcc/modularization.pdf).

Brevemente: ogni reparto sceglie come può essere contattato. A questo punto dovrà essere contattato solo ed esclusivamente attraverso il metodo o i metodi descritti. Niente corsie preferenziali o sistemi alternativi. Il metodo o i metodi scelti devono funzionare allo stesso modo con il mondo esterno.

_La tecnologia usata per raggiungere il risultato non conta._

Bezos pensava e pensa soprattutto in termini di comunicazione a livello software. Ma la lezione è potente e andrebbe meditata anche a livelli diversi. Puoi essere contattato solo attraverso i mezzi che hai scelto. I mezzi che hai scelto devono funzionare anche con il mondo esterno. Non esistono eccezioni. _La tecnologia usata non conta._

Amazon funziona. Non è una congrega di filosofi, lavorano nella logistica, portano risultati, migliorano continuamente. Forse qualcosa hanno capito. Sono molto [indipendenti](https://macintelligence.org/blog/2020/07/31/guerra-di-indipendenza/) e vogliono esserlo ancora di più.

