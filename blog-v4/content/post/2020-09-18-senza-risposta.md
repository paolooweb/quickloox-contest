---
title: "Senza risposta"
date: 2020-09-18
comments: true
tags: [Venerandi, netcat, Neonecronomicon, nc, telnet, Mud]
---
Dopo che anni fa avevo mostrato come si possa [usare `nc` al posto di `telnet` per entrare in un Mud](https://macintelligence.org/blog/2017/09/29/finisce-per-t/), [Fabrizio Venerandi](http://www.quintadicopertina.com/fabriziovenerandi/) mi fa notare come il suo [Neonecronomicon](http://www.neonecronomicon.it) effettivamente funzionerebbe, se il prompt non venisse popolato da punti interrogativi che si accumulano a intervallo regolare.

È così effettivamente e il problema è analogo, seppur con diversa frequenza, anche in altri Mud, per esempio [Achaea](https://www.achaea.com).

Il mio problema è un altro: ricordo come se fosse ieri di avere provato l’accesso, quando ho scritto il post tre anni fa, e di essere entrato nel gioco senza problemi. L’esperienza attuale non è quella del 2017, ma che cosa è cambiato? Il sistema operativo? Il funzionamento interno di nc? Un folletto dispettoso?

E poi c’è il problema bonus. Ho compulsato il manuale di nc e ho provato questo e quel modificatore. Niente sembra funzionare. C’è qualcuno che ne sa più di me e mi aiuta?

Un altro punto interrogativo e continuano ad aumentare…