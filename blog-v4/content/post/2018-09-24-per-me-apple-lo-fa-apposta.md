---
title: "Per me Apple lo fa apposta"
date: 2018-09-24
comments: true
tags: [iOS12, Ars, Technica, Cunningham]
---
Scrive [Andrew Cunningham per Ars Technica](https://arstechnica.com/gadgets/2018/09/ios-12-on-the-iphone-5s-iphone-6-plus-and-ipad-mini-2-its-actually-faster/):

>Collaudo iOS su sistemi vecchi da sei anni e non ho mai visto una versione che migliorasse effettivamente le prestazioni su hardware datato. Al meglio, aggiornamenti come iOS 6, iOS 9 e iOS 10 non hanno peggiorato le cose più di tanto; al peggio, aggiornamenti come iOS 7 e iOS 8 hanno fatto sentire davvero vecchi apparecchi che lo erano. Chiunque disponga di un apparecchio vecchio può aggiornarlo con sicurezza a iOS 12 senza preoccuparsi della velocità, ed è una gran cosa. E anche sui sistemi più recenti si noterà un miglioramento (il mio iPad Air 2, che con iOS 11 aveva cominciato a mostrare la sua età, va alla grande con iOS 12).

Un altro passo della diabolica strategia di Apple per costringere la gente a comprare iPhone nuovi: velocizzare quelli vecchi. E si parla di terminali acquistati nel 2013: cinque anni, in questo mondo, hanno il sapore dell’eternità.