---
title: "Famolo ricaricabile"
date: 2018-08-18
comments: true
tags: [Mori, Vlc, Schiller]
---
I pro e i contro delle app in abbonamento li ha ben spiegati l’amico [Riccardo](http://morrick.me/archives/7643). Il modello attuale di App Store spinge verso un annullamento dei prezzi, cosa che fa piacere All’utilizzatore ma penalizza lo sviluppatore e, alla lunga, danneggia l’utilizzatore.

Apple ritiene che la soluzione siano gli abbonamenti alle app e [insiste privatamente con gli sviluppatori](https://www.macstories.net/linked/apple-privately-advocates-for-developer-adoption-of-subscriptions/) perché procedano in questo senso. Fare incassare più denaro a chi sviluppa giova anche all’utilizzatore, che può contare su app migliori, solo che alla lunga lo soffoca.

Trovare un equilibrio efficace e semplice è tutt’altro che banale. Butto comunque lì l’idea: più che un abbonamento, rendiamolo un pagamento a tempo; se pago posso usare l’app per un mese, se non la voglio usare non pago e riprenderò a farlo quando la app mi servirà nuovamente. Una specie di ricaricabile per le app.

In questo modo gli sviluppatori incasserebbero cifre più giuste per il loro lavoro e gli utilizzatori potrebbero pagare il giusto senza restare impiccari ad app servite per un singolo progetto o che rimangono silenti per lunghi periodi.

Servirebbe anche ripensare al rapporto tra App Store e licenze *open source*, che oggi [esiste](https://macintelligence.org/posts/2012-07-06-liberta-e-libertuccia/) (a dispetto di certe invenzioni paraletterarie) ma è lungi dall’essere ottimale. Per ogni campo applicativo, una app costosa (in modo equo) e performante, più una soluzione open a disposizione di chi può permettersi solo il gratis o il quasi gratis; a me sembra una situazione ragionevole. Diciamolo a Phil Schiller.
