---
title: "Umili emuli"
date: 2015-10-02
comments: true
tags: [emulatori, Hackaday, Pdp/11]
---
Raccolta di emulatori? Un [articolo inaspettato di Hackaday](http://hackaday.com/2015/09/28/roundup-retro-computers-in-your-browser/), sito normalmente dedicato a tutt’altro, è una miniera.<!--more-->

Abbondano soprattutto macchine veramente dimenticate o quasi e questo fa la differenza: la collezione, qui, fa un salto in avanti verso il completamento.

Da non dimenticare i commenti, dove compaiono numerosi altri link.

Al momento ho provato solo [Pdp/11](http://pdp11.aiju.de) per ragioni sentimentali: una delle mie primissime esposizioni ai computer fu assieme a un amico che lavorava in Digital Equipment e mi mostrò una sessione di collegamento proprio con un Pdp/11 dell’azienda. Un mondo che si apriva.