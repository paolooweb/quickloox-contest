---
title: "Vedi Microsoft e poi muori"
date: 2016-07-25
comments: true
tags: [Yahoo!, Microsoft, Virgilio, Telecom, Seat, Nokia]
---
L’acquisizione di Yahoo! da parte di Verizon ricorda in qualche aspetto ciò che accadde in Italia quando Virgilio (l’azienda che lo ha sempre prodotto) fu acquisito da Seat Pagine Gialle, poi da Telecom Italia e poi [da Libero.it](http://www.repubblica.it/economia/2012/08/09/news/telecom_vende_matrix_ala_fiannziaere_egiziano_sawiris-40642965/).

La differenza è che i proprietari di Virgilio fecero un affarone e invece per Yahoo è una svendita rinunciataria.

Vale la pena di ricordare che Yahoo! firmò un patto con il diavolo qualche anno fa, rinunciando all’ulteriore sviluppo del proprio motore di ricerca per [farselo fornire da Bing di Microsoft](http://news.microsoft.com/speeches/steve-ballmer-carol-bartz-microsoft-yahoo-search-agreement/#sm.0001i5np2648wdjgxi21gbedxcj4l).

Sarà il tocco magico che ha fatto le [novelle sfortune di Nokia](https://macintelligence.org/posts/2013-09-03-mission-accomplished/)?

*[Il progetto di [Cuore di Mela](http://cuoredimela.accomazzi.it) si basa sull’idea forte che essere indipendenti da un editore consenta di rivolgere un miglior servizio ai lettori. Chi considera l’idea, visiti la [pagina Kickstarter](https://www.kickstarter.com/projects/295108589/cuore-di-mela-i-segreti-di-apple-iphone-e-mac) del progetto, faccia girare e lasci moneta!]*