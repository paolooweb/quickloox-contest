---
title: "Un giorno speciale"
date: 2018-12-25
comments: true
tags: [Natale, Carcassonne, Polytopia]
---
Credenti o no, è per forza un giorno speciale e auguro a ciascuno di viverlo in modo speciale, se possibile vicino a persone speciali.

Consiglio inoltre di destinare la seconda parte della giornata al gioco, una volta placata la tempesta parentale, quando il clamore si attenua.

In questo periodo nel mio entourage si portano bene [Carcassonne](https://carcassonneapp.com/) e [Battle of Polytopia](http://midjiwan.com/polytopia.html). Roba semplice per partire ma grande complessità se si vuole esser bravi. Se qualcosa sembra difficile, va ricordato che si può anche giocare a turni. Chiaramente il novantanove percento dello spasso è sfidare parenti e amici.

Su Carcassonne online sono *lvcivs* e su Polytopia, invece, Zf37pI6KgzGLOIL9. Non garantisco reazioni in giornata, mentre da mezzanotte in avanti vale tutto.

Perché anche questo è un modo di essere vicini a persone speciali e non mi basta mai.
