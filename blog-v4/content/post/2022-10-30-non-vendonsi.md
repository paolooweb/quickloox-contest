---
title: "Non Vendonsi"
date: 2022-10-30T00:55:22+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [Gruber, John Gruber, Daring Fireball, App Store, Arcade]
---
Apple spesso fa la cosa giusta, a volta non la fa, capita anche che faccia la cosa sbagliata. Più diventa grande, più è inevitabile che di tanto in tanto si infili una cosa sbagliata.

La pubblicità in App Store, specialmente [la pubblicità di bassa lega in App Store](https://daringfireball.net/2022/10/app_store_ads_gone_wild), è una cosa molto sbagliata che si spera venga risolta al più presto e nella maniera più draconiana possibile.

Anche una società da quattrocento miliardi l’anno di fatturato può preoccuparsi giustamente di tutelare la crescita, i margini, anche gli investimenti degli azionisti che rischiano di scricchiolare per una cifra non soddisfacente o un risultato dubbio. Discutibile, ma lecito.

Che però la pubblicità bieca in App Store faccia del bene alla reputazione e al fatturato di Apple, è da dubitare fortemente. Una prova che le petizioni su Internet sono una buffonata è l’assenza di una petizione per levare la pubblicità negativa da App Store e l’assenza di cento milioni di firme sotto.

Che poi, sono un fautore assoluto di Apple Arcade. Dove c’è l’abbonamento, non c’è la pubblicità. Forse la pubblicità-spazzatura è un messaggio subliminale per convincere la gente ad abbonarsi ad Arcade? Neanche così è una bella situazione. La presenza della pubblicità su App Store è comprensibile e si tratta di pubblicità molto più rispettosa della privacy di quella presente in altre zone della Rete. OK.

Però deve essere di qualità. Altrimenti non ci sta, in un ecosistema Apple. Se si vogliono vendere spazi, che lo siano secondo criteri di qualità. Grazie.