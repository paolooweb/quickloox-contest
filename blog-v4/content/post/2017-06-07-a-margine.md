---
title: "A margine"
date: 2017-06-07
comments: true
tags: [WebRtc, Safari, HomeKit, iOS, High, Sierra, Arduino]
---
Ieri [scrivevo](https://macintelligence.org/posts/2017-06-06-piu-computer-per-tutti/) di come gli annunci di Wwdc siano stati inaspettatamente abbondanti nonostante di vari argomenti si fosse già parlato alla vigilia dell’evento.<!--more-->

Due giorni dopo succede che le specifiche di HomeKit [vengano aperte a tutti gli sviluppatori](https://twitter.com/yas375/status/872492582831480832).

<blockquote class="twitter-tweet" data-lang="en"><p lang="en" dir="ltr">HomeKit protocol spec is now open to all devs! Can build a smart device using Arduino and control it via HomeKit without getting MFI license</p>&mdash; Victor Ilyukevich (@yas375) <a href="https://twitter.com/yas375/status/872492582831480832">June 7, 2017</a></blockquote> <script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>

È piuttosto importante, per quanto l’apertura sia limitata all’uso non commerciale. Vuol dire che chiunque può mettersi a fare esperimenti su HomeKit su un Arduino qualsiasi.

Succede pure che venga annunciata l’adozione di [WebRtc su Safari](https://webkit.org/blog/7726/announcing-webrtc-and-media-capture/). Videoconferenza via web per Mac da Sierra in su, iOS 11 in su. Chi ha Sierra e Safari Technology Preview [può provarci subito](https://webkit.org/blog/7627/safari-technology-preview-32/), scaricando la Release 32.

WebRtc è promettente come protocollo universale di comunicazione video, HomeKit diventa un componente fondamentale delle reti casalinghe. Apple è interessata a giocare forte in questi settori e si tratta di annunci legittimamente diretti, soprattutto, agli sviluppatori.

Eppure restano al margine degli eventi, soverchiati da una quantità di notizie che hanno una eco superiore.

È interessante. La quantità di cose che bollono in pentola in Apple ha superato la capacità di trasmissione dei canali dedicati. È anche notevole.