---
title: "Passo e chiudo"
date: 2015-10-24
comments: true
tags: [Elop, Hilf, cloud, Microsoft, Hp, Nokia, Azure, Helion]
---
Qualcuno ricorda la parabola di Stephen Elop? Dirigente di Microsoft, diventa amministratore delegato di Nokia. L’azienda finlandese peggiora i propri risultati fino a quando vende il ramo cellulare dell'impresa proprio a Microsoft.<!--more-->

Elop [rientra in Microsoft](http://www.macintelligence.org/blog/2013/09/23/premio-competenza/) e, seppur reduce da un risultato imprenditoriale deludente, viene [dato per candidato](http://www.digitaltrends.com/computing/is-stephen-elop-microsofts-next-ceo/) alla poltrona di amministratore delegato. Dopo di che il business cellulare per Microsoft si rivela strategico ma [ben poco profittevole](http://www.bloomberg.com/news/articles/2015-07-21/microsoft-posts-largest-ever-net-loss-after-nokia-writedown) e Elop [riceve il benservito](http://mobile.reuters.com/article/idUSKBN0OX1V720150617). Tra questi ultimi due eventi non c’è necessariamente un legame.

Meno clamore suscita la parabola di Bill Hilf. [Ex dirigente di Microsoft](http://fortune.com/2015/10/21/hp-public-cloud/), è andato a occuparsi di *cloud computing* in Hp.

Dopo alcuni mesi, Hp [annuncia la chiusura](http://h30499.www3.hp.com/t5/Grounded-in-the-Cloud/A-new-model-to-deliver-public-cloud/ba-p/6804409#.VipNCpRXerW) del proprio servizio di cloud pubblico Helion.

I clienti in essere verranno dirottati verso altri servizi cloud esistenti, come i Web Services di Amazon e, chi mai avrebbe potuto prevederlo, Azure di Microsoft.

Per ora Hilf continua a lavorare in Hp, diversamente da quanto è accaduto a Elop.

Il passaggio e la chiusura invece si somigliano.