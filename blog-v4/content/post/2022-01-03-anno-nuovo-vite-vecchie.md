---
title: "Anno nuovo, vite vecchie"
date: 2022-01-03T02:14:24+01:00
draft: false
toc: false
comments: true
categories:
tags:
---
So di avere fatto [promesse per oggi](https://macintelligence.org/posts/2022-01-02-una-premessa-sul-blogging/), però trovo opportuno rinviarle a favore della segnalazione di [Mac Source Ports](https://macsourceports.com), una iniziativa che mi piace molto e a cui mi sarei dedicato volentieri se appena fossi in possesso della giusta professionalità.

Semplice: gli autori del sito prendono vecchi (ma grandi) videogiochi e li rendono funzionanti su Mac Intel e Apple Silicon, gratis.

Per il momento il catalogo è scarno e sta in una pagina: *Return to Castle Wolfenstein*, *Quake* in tutte le sue versioni, *Doom* pure, *Disasteroids 3D*, *Heretic*, *Hexen: Beyond Heretic*, *Strife*, *Chex Quest Trilogy*, *Duke Nukem 3D*, *Blood*, *Redneck Rampage*, *Shadow Warrior Classic*, *PowerSlave*.

Poco, eh. Però, mettiamo che abbiano quel minimo di supporto da persone disponibili a supportare con pochi spiccioli un bel progetto. Mettiamo che continuino. Diventa una iniziativa fantastica tanto per il più agguerrito *retrogamer* quanto per un ragazzino che arriva oggi e non sa nulla di che cosa si giocava nella favolosa fine secolo.

Venuzza polemica: ma, a tempo debito, era così tanto difficile portare questi giochi su Mac da parte di chi li ha sviluppati? Più difficile portarli su PowerPC che oggi su Apple Silicon? La passione giustifica tutto, ma davvero può un’impresa così titanica essere proposta gratis? E se invece non è titanica, perché non metterci qualche energia? Forse su Mac mancavano certi giochi solo per la spocchia di chi li sviluppava.

È anche polemica retorica: grazie al lavoro di oggi posso godermi giochi validi di ieri, ammirare contenuti per l’epoca anche innovativi e controversi, preoccuparmi meno di che cosa succederà nel momento in cui non ci saranno più lettori capaci di usare i supporti originali. *Quake* continuerà a vivere una volta di più assieme a tutti i suoi analoghi.

Piace? [Offrire tre dollari di caffè](https://ko-fi.com/schnapple) allo sviluppatore principale del progetto dista un clic.

*Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile.*
