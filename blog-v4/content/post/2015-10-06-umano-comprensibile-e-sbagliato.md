---
title: "Umano, comprensibile e sbagliato"
date: 2015-10-06
comments: true
tags: [OSX, beta, Apple, TimeMachine]
---
È umano e comprensibile aderire al programma di [beta pubblica](https://beta.apple.com/) di OS X X per vedere in anticipo che cosa ci sia dentro il nuovo sistema operativo.<!--more-->

Ugualmente umano e comprensibile, una volta uscita la versione definitiva del software, di volerla installare al posto delle beta che continuano sì ad aggiornarsi e a restare sempre un passo avanti alla versione ufficiale; ma rimangono beta e non sono garantite rispetto a difetti e problemi.

A questo proposito le [istruzioni](https://beta.apple.com/sp/betaprogram/faq) sono semplicissime da seguire: cancellare il software beta, installare il software ufficiale, ripristinare il sistema da Time Machine.

Umano e comprensibile considerare strade alternative, perché ripristinare da Time Machine è processo lungo e noioso.

Beh, è sbagliato. Non prendere iniziative, non farti venire idee brillanti, segui le istruzioni. La lunghezza e la noia che ti aspettano in caso contrario ti faranno rimpiangere il ripristino da Time Machine e ho detto tutto.