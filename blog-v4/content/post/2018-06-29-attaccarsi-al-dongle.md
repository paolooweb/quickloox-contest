---
title: "Attaccarsi al dongle"
date: 2018-06-29
comments: true
tags: [Mac, MacBook, Pro, Surface, Sasha]
---
Devo questo post a **Sasha**, che ringrazio di cuore. Cito subito [9to5Mac](https://9to5mac.com/2018/06/26/microsoft-surface-usb-c-dongle/) perché la sostanza è semplice:

>Se pensavate che fosse fastidioso dover comprare adattatori per MacBook e MacBook Pro, rivolgete un pensiero ai proprietari di Surface Pro e Surface…

Microsoft infatti offre un’ottima scelta, per effettuare collegamenti Usb-C: un *dongle*, adattatore, che costa ottanta dollari.

Non solo: l’adattatore è grande quasi quanto l’alimentatore estero di Surface Pro.

Non bastasse… l’adattatore vuole un suo alimentatore.

La linea di portatili Mac può certo migliorare, ma parlando di adattatori non è che gli altri abbiano messo la freccia e superato in corsa, ecco.