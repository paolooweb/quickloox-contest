---
title: "La miglio gioventù"
date: 2013-09-05
comments: true
tags: [Scuola]
---
Un conto è pretendere che nella scuola di oggi sia meglio dare in mano ai ragazzi [una calcolatrice grafica che un iPad](https://macintelligence.org/posts/2013-09-04-snobismo-educativo). Un altro conto è cogliere un concetto fondamentale: *i ragazzi non ne sanno necessariamente più degli insegnanti*.<!--more-->

Ne [scrive](http://coding2learn.org/blog/2013/07/29/kids-cant-use-computers/) Marc Scott, insegnante inglese cui capita di fare da supporto tecnico involontario a colleghi e studenti.

Scommetto che la sua figura è comune in tantissimi istituti ed è questo il punto. Rispetto a tanti anni fa, quando il docente si limitava a dire *la stampante non funziona* e se non si muoveva il tecnico niente si muoveva, quello è che è cambiato è perfettamente zero. Solo che intanto è cambiata una generazione.

Nel lunghissimo articolo che meriterebbe la traduzione, Scott si misura con il mito che i ragazzi ne sappiano *necessariamente* più dell’insegnante; nell’altro mito nutrito dai genitori, i quali credono che il figlio sarà un fenomeno con il computer perché ci passa davanti tutto il giorno.

>I genitori sembrano avere qualche vago concetto che trascorrere ore ogni sera su Facebook e YouTube impartirà, per qualche sorta di osmosi cibernetica, una conoscenza di Php, Html, JavaScript e Haskell.

Abbondano storie dell’orrore da cena conviviale, purtroppo numerose e sicuramente fin troppo veritiere:

>Durante la mia lezione un’alunna incrocia le braccia. “Il mio computer non si accende”, con l’aria disperata di chi ha provato tutto. Allungo il braccio e accendo il monitor, che subito mostra la schermata di login.

Straordinari i consigli finali per i genitori, penso espressione di saggezza anche fuori dal campo informatico:

>Smettete di aggiustare le cose ai figli. Passate ore a insegnargli ad andare al gabinetto da soli, perché sapere usare un gabinetto è una capacità essenziale nella società moderna. Dovete fare lo stesso con la tecnologia. […] Quando compiono undici anni, dategli un file di testo con diecimila password del Wi-Fi e ditegli che quella vera è lì dentro. Vedete quanto impareranno in fretta a usare Python o bash.

E per le scuole:

>Dovremmo insegnare ai ragazzi a non installare malware, invece che blindare le macchine per renderlo fisicamente impossibile. Dovremmo insegnargli a stare online in sicurezza invece che filtrare la loro Internet. Google e Facebook pagano i ragazzi che trovano vulnerabilità significative nei loro sistemi. Nelle scuole lavoriamo perché i ragazzi non possano violare i nostri. È giusto?

Scott giudica negativamente il settore *mobile*, anche qui secondo me prendendo lo stesso granchio di cui all’inizio. Su iPad posso accendere [Prompt](https://itunes.apple.com/it/app/prompt/id421507115?l=en&mt=8) e collegarmi via shell in protocollo cifrato con il mondo. Costruire moduli per [Status Board](https://itunes.apple.com/it/app/status-board/id449955536?l=en&mt=8) è un esercizio fantastico, per ragazzi che hanno a che fare con il mondo del web.

Che oggi è il mondo, punto. Se qualche insegnante è in ascolto, cominci a buttare via quella coperta di Linus chiamata Office. È la cosa più vecchia che possiedi, anche se l’hai installata nuova ieri.

E smettiamo di trattare i ragazzi come passerotti che hanno bisogno del becchime per passare l’inverno. Hanno invece bisogno di essere addestrati a volare.