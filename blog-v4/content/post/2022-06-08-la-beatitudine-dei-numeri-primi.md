---
title: "La beatitudine dei numeri primi"
date: 2022-06-08T00:57:44+01:00
draft: false
toc: false
comments: true
categories: [Hardware, Software]
tags: [Cheshire, M1 Max]
---
La congettura di [Cheshire](https://twitter.com/GoblinQChesh) sui numeri primi [è stata provata come vera](https://twitter.com/landonnoll/status/1534274465919422464). Questo numero primo palindromo da ventimila e una cifra è stato trovato dopo nove giorni, otto ore, cinquantatré minuti e quarantaquattro virgola quattro secondi di elaborazione da parte di un processore M1 Max di Apple.

<blockquote class="twitter-tweet"><p lang="en" dir="ltr">The Cheshire prime conjecture, <a href="https://twitter.com/GoblinQChesh?ref_src=twsrc%5Etfw">@GoblinQChesh</a>, has been proven to be true. This 20001 decimal digit palindrome prime was discovered by <a href="https://twitter.com/landonnoll?ref_src=twsrc%5Etfw">@landonnoll</a> <br>on 2022 June 06 after 9 days, 8 hours, 53 minutes and 44.4 seconds of Apple M1 Max processor time. <a href="https://t.co/fovBscrqea">pic.twitter.com/fovBscrqea</a></p>&mdash; Landon Curt Noll (@landonnoll) <a href="https://twitter.com/landonnoll/status/1534274465919422464?ref_src=twsrc%5Etfw">June 7, 2022</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

E scommetto che nessuna ventola è stata maltrattata per raggiungere il risultato.