---
title: "Come vivono i grandi"
date: 2022-08-01T01:36:42+01:00
draft: false
toc: false
comments: true
categories: [Internet]
tags: [Gruber, John Gruber, Daring Fireball, Russell, Bill Russell, NBA, Boston Celtics, wwdc]
---
[È morto anche Bill Russell](https://www.nba.com/news/nba-reacts-to-bill-russells-death), già centro dei Boston Celtics dell’Nba.

Il più grande vincente di tutti i tempi nella storia degli sport di squadra, con un record di ventuno vinte e zero perse in partite decisive di finali.

Ha toccato gli ottantotto anni, eppure John Gruber [è riuscito a vederlo di persona](https://daringfireball.net/linked/2022/07/31/bill-russell) nel 2016 a San Francisco, in coincidenza con una WWDC.

Russell, già ultraottantenne, era lì probabilmente per vedere un incontro delle finali NBA. [Usava un iPhone](https://twitter.com/gruber/status/744726966742310913).

<blockquote class="twitter-tweet"><p lang="en" dir="ltr">Saw Bill Russell in hotel lobby last week in San Fran: <a href="https://t.co/SkC0PhsMar">pic.twitter.com/SkC0PhsMar</a></p>&mdash; John Gruber (@gruber) <a href="https://twitter.com/gruber/status/744726966742310913?ref_src=twsrc%5Etfw">June 20, 2016</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>