---
title: "Disintossicazione infantile"
date: 2017-07-11
comments: true
tags: [iPad, iPhone, Lidia]
---
Si leggono [cose tremende](https://macintelligence.org/posts/2015-11-10-scala-uno-a-quindici/) sull’assuefazione dei bimbi ai *device*, la necessità di contingentare il tempo davanti allo schermo, la luce blu che complica il sonno eccetera.

Aggiorno semplicemente una mia esperienza personale, privo di ambizioni statistiche.

Per tutto l’anno Lidia – che al momento si avvicina ai tre anni compiuti – ha chiuso le giornate giocando su iPad o guardando video YouTube su iPhone. Molto spesso, a un certo punto della serata, ha chiesto espressamente questo o quell’apparecchio, questo o quel gioco.

La ragazza è un tesoro ma ha anche un caratterino e, se si impunta su qualche cosa, si mostra determinata (eufemismo). Giusto per contestualizzare.

Per un mese ci siamo trasferiti a lavorare, il sottoscritto, al mare e il resto della famiglia a godersi il mare. Ho messo i *device* della figlia nello zaino della tecnologia che ci ha seguito nel viaggio, ma li ho lasciati lì dentro. Non sono mai stati richiesti, né cercati, neanche il primo giorno.

Ora siamo tornati da una settimana. La casa di prima, le abitudini di prima, ma Lidia non ha mai chiesto i suoi *device*. Fatto cento l’utilizzo giornaliero durante gli undici mesi precedenti giugno, il dato di quest’ultimo mese è esattamente zero. Il dato della prima decade di luglio è zero.

Se domani chiedesse di nuovo iPad o iPhone, glieli darei. Unica regola che è sempre stata in vigore: dopo il tramonto.

Tutto questo sul pericolo che gli apparecchi digitali creino traumi di crescita e distolgano irrimediabilmente i piccoli dal mondo reale, sui *campus* per la disintossicazione digitale, sul rischio della tecnologia per i bambini.

Ho l’impressione che, quando c’è, il problema sia del singolo bambino, della singola famiglia, del singolo genitore; ma questo vale sempre, dal gioco al cibo alla scuola allo sport. Trovo solo logico che valga anche per un iPad.