---
title: "La luna e il boccione"
date: 2021-06-27T12:50:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [New York Times, Tim Cook, Motorola, PowerPC] 
---
È arrivato persino il _New York Times_ a porsi il problema della creatività aziendale stimolata, o meno, dalla presenza in ufficio e dagli [incontri casuali davanti al boccione dell’acqua](https://www.nytimes.com/2021/06/23/upshot/remote-work-innovation-office.html) o alla macchinetta del caffè.

La risposta del quotidiano è, semplifico, _non ci sono dati a sostegno della presenza in ufficio_. Viene citata anche l’opinione di Tim Cook, con la quale  veniva motivata la posizione Apple di richiedere la presenza per tre giorni la settimana, a parte eccezioni.

L’ho sentita anche da altri: una specie di aura circonderebbe gli incontri occasionali e non previsti, per corridoi e stanze relax, foriera di idee e pensiero anticonvenzionale.

La mia posizione è di scetticismo, per vari motivi. Si fa molto bene brainstorming anche a distanza. Da libero professionista, per la maggior parte del tempo lavoro da solo eppure di idee me ne vengono in continuazione (la loro qualità è un’altra questione). Mi è capitato spesso di avere un’idea e scrivere un messaggio o una email, anche ai tempi delle macchine del caffè.

A questo punto normalmente scatta l’esempio. Quella genialata saltata fuori in pausa pranzo, chiacchierando di tutt’altro, come avremmo fatto senza? È noto che in Apple l’idea di transitare da Motorola 680x0 a PowerPC nacque durante una vacanza sulla neve di alcuni ingegneri.

Certamente nascono idee davanti al distributore delle bibite. Ma anche altrove. Tutta la faccenda mi ricorda la convinzione di alcune ostetriche, secondo le quali la luna piena incoraggia la nascita dei bimbi. Funziona così: nasce un bimbo, guardano fuori dalla finestra, vedono la luna piena, se la ricordano. Quando nasce un bimbo e guardano fuori ma la luna non c’è, non fissano alcun ricordo speciale.

È anche tutto da dimostrare che la presenza in ufficio sia l’unico modo per suscitare idee creative. Magari ne esistono altri migliori, che però non sono mai stati esplorati in decenni di ufficio e boccioni dell’acqua.

Idea: provare davvero a raccogliere dei dati? E mi è venuta dal divano, eh.

*Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile.*