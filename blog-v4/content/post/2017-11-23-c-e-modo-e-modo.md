---
title: "C’è modo e modo"
date: 2017-11-23
comments: true
tags: [MacDrifter, iPad, Mac]
---
Su *MacDrifter* raccontano di com’è andata, dopo l’arrivo di un nuovo iPad Pro 10”5, nel rinunciare a Mac per tutto tranne l’assoluta necessità.<!--more-->

Il titolo è [Mac ti fa sentire ancora a casa](http://www.macdrifter.com/2017/11/the-mac-still-feels-like-home.html). La disamina copre gli aspetti dove vince Mac e quelli dove vince iPad. Dove vince Mac, in sintesi, è qui:

>Mac è superiore nello spostare informazioni.

Lo schermo più grande, la maggiore varietà di servizi e automatismi e personalizzazioni sono indiscutibili. Dove vince iPad? Pencil, meeting e portabilità.

>Posso prendere note dettagliate complete di diagrammi e annotazioni, che richiederebbero molto più sforzo su un portatile. […] iPad spesso è posato sul tavolo e non sono nascosto dietro il suo schermo, così c’è meno sconnessione tra quello che sto facendo e quello che l’altro pensa io stia facendo. È un grande strumento di collaborazione in modi che un portatile non consente.

E poi:

>Non desidero altro computer quando devo stare sei ore su un aereo.

È così semplice, così una questione di buonsenso. Uno strumento fa meglio certe cose, uno strumento fa meglio certe altre.

E speriamo di avere seppellito il dibattito sulla sostituibilità di quello o di quell’altro. Si fa tutto con tutto; cambia solo il modo.