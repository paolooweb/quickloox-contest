---
title: "Partite da evitare"
date: 2016-03-25
comments: true
tags: [iPad, Pro, Apple, Pencil, Olson, Tay, Microsoft, AI, IA, Cook]
---
Mi dicono [la partita ormai è il web, l’intelligenza artificiale, i robot, le auto senza guidatore, non certo vendere i telefonini fatti dai cinesi](https://www.facebook.com/renato.gelforte/posts/10153951395847180?comment_id=10153955692892180&reply_comment_id=10153956729192180&comment_tracking=%7B%22tn%22%3A%22R%22%7D&pnref=story).<!--more-->

Già, l’intelligenza artificiale. Microsoft ne crea una che dovrebbe comportarsi su Twitter come una ragazzina, solo che nel giro di ventiquattr’ore [viene cancellata](http://www.telegraph.co.uk/technology/2016/03/24/microsofts-teen-girl-ai-turns-into-a-hitler-loving-sex-robot-wit/) dopo avere inneggiato a Hitler, sparato frasi razziste a destra e a manca e infarcito di allusioni sessuali [ogni dialogo](https://twitter.com/hashtag/TayTweets?src=hash).

<blockquote class="twitter-tweet" data-lang="en"><p lang="en" dir="ltr">&quot;Tay&quot; went from &quot;humans are super cool&quot; to full nazi in &lt;24 hrs and I&#39;m not at all concerned about the future of AI <a href="https://t.co/xuGi1u9S1A">pic.twitter.com/xuGi1u9S1A</a></p>&mdash; Gerry (@geraldmellor) <a href="https://twitter.com/geraldmellor/status/712880710328139776">March 24, 2016</a></blockquote> <script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>

(Da un punto di vista accademico c’è chi lo considera anche un certo successo, perché evidentemente l’entità è sfuggita al controllo dei suoi creatori e questo, per una aspirante intelligenza artificiale, costituisce un segnale interessante. Ma è un altro discorso).

Sui telefonini venduti da cinesi, pensando a quelli più grandi, tanto da diventare tavolette, magari dotate di una nuova stupefacente matita digitale: la quindicenne Zoe Olson, intelligenza naturale, è passata a iPad Pro e le sue illustrazioni sono cresciute di qualità tanto da procurarle un lavoro, un messaggio da Tim Cook e la possibilità di scrivere un articolo nel quale [mostrare le proprie creazioni](https://medium.com/life-tips/dear-tim-cook-abc3fbffba1b#.gm1secjkt).

Il suo [sito personale](http://www.theipadartist.org) spiega pregi e difetti di iPad Pro e Apple Pencil meglio di mille recensioni (per chi non se ne fosse accorto, dalla prossima settimana anche gli iPad 9,7” in vendita acquisiscono le stesse capacità di iPad Pro, compreso l’uso di Apple Pencil).

Sarà che invecchio, ma a certe nuove affascinanti partite con l’intelligenza artificiale preferisco la noia di quindicenni che scoprono un talento naturale grazie a tecnologia concreta. Che la facciano i cinesi mi pare secondario.