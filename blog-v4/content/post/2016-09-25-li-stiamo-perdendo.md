---
title: "Li stiamo perdendo"
date: 2016-09-25
comments: true
tags: [email]
---
Un brutto colpo.

Mi è stato chiesto di scrivere la risposta delle email sopra il testo a cui rispondere.

Non è una sfortunata condizione di fatto causata dalla preimpostazione di tutti i programmi. Mi è stato *chiesto*.

*Per non fare confusione*.

La pulizia della mail cui si risponde per eliminare le parti inutili. L’ordine di lettura da sinistra a destra e dall’alto in basso (per altri è diverso, ma per noi è questo). La logica. Il flusso del discorso. Li stiamo perdendo tutti.

Vorrei che scrivessero un copione teatrale.