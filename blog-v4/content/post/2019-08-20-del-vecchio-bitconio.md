---
title: "Del vecchio bitconio"
date: 2019-08-20
comments: true
tags: [Apple, RetroConnector, Bitcoin]
---
Talmente avanti, Apple ][ che era già pronto negli anni settanta per [estrarre Bitcoin](https://retroconnector.com/mining-bitcoin-on-an-apple-ii-a-highly-impractical-guide/).

Nota per gli sprovvisti di ironia: il progetto è bellissimo proprio perché completamente inutile ai fini pratici. Più facile vincere al Superenalotto.
