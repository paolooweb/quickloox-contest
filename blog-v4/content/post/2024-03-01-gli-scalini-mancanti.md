---
title: "Gli scalini mancanti"
date: 2024-03-01T00:23:28+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [ai, intelligenza artificiale, Chollet, François Chollet, The missing rungs on the ladder to general AI]
---
Per conseguire l’abilitazione a parlare seriamente di modelli linguistici e intelligenze artificiali presunte occorre avere letto la presentazione [Gli scalini mancanti sulla scala che porta all’intelligenza artificiale generale](https://docs.google.com/presentation/d/1DQQQJM0LwgdyPMB92LdwPqw-DrXkWeBAKbZ_bPPB0iM/edit#slide=id.p).

Spettacolare per chiarezza, sintesi, qualità del contenuto. Si impara rapidamente a che punto sono veramente gli LLM, quali sono le loro caratteristiche intrinseche, che cosa serve per arrivare alla vera intelligenza artificiale, quanto di questo è effettivamente presente negli LLM e un sacco di altre cose.

Il linguaggio è chiarissimo e le slide sono comprensibili come se fossero legate tra loro.

Ammetto che l’ultima manciata di slide tende al tecnico e richiede qualche preparazione; tutte quelle prima presuppongono solo una normale esposizione al tema e sono sentitamente raccomandate.