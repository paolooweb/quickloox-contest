---
title: "In un modo o nell’altro"
date: 2021-03-16T01:53:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [Tommaso, Covid, coronavirus] 
---
Ho lasciato stare il più possibile la faccenda della pandemia, solo che oggi era praticamente impossibile riuscire a trattenersi.

Di fronte a quello che accade e che lascia a dire poco sconcertati, l’unica risposta che riesco a dare è [tenere presente Tommaso](https://twitter.com/essexelle/status/1371445500851982337).

<blockquote class="twitter-tweet"><p lang="it" dir="ltr">I giovani non hanno perso la fiducia. Tommaso e un suo compagno di classe stanno creando un app che serve per ordinare il pranzo/merenda al bar della scuola. DAD hai i giorni contati. <a href="https://t.co/EAM8HPx4rk">pic.twitter.com/EAM8HPx4rk</a></p>&mdash; 🅴🆂🆂🅴🆇🅴🅻🅻🅴 (@essexelle) <a href="https://twitter.com/essexelle/status/1371445500851982337?ref_src=twsrc%5Etfw">March 15, 2021</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

Un sacco di gente si batte a parole perché Tommaso vada a scuola in aula e, nei fatti, agisce perché stia a casa.

L’unico lato positivo di tutta questa faccenda è che, alla fine, vincerà lui comunque vada. E il suo compagno di programmazione, e gli altri compagni, e tutti questi bambini e ragazzi che avranno tempo di leccarsi le ferite, stanno imparando come matti a dispetto delle prefiche e bagneranno il naso a tutti, virus per primo.

*Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile.*