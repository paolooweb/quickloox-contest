---
title: "Chi si distingue è bravo"
date: 2019-03-26
comments: true
tags: [Arcade, News, Card, TV+, Pay]
---
Primissime impressioni a caldo dopo un [evento](https://www.apple.com/apple-events/march-2019/) come mai ce ne sono stati nella storia di Apple, dedicato alla presentazione di *servizi*, dalle notizie all’edicola, alle produzioni televisive all’abbonamento a giochi, fino addirittura a una… carta di credito associata a Pay.

Non c’è dubbio, il fuoco di sbarramento di quelli che non c’è innovazione, una volta i Mac li potevi smontare pezzo per pezzo, sarà ingente. È superata persino la battuta per cui Apple, dopo avere perso la dizione *Computer* nella ragione sociale, dovrebbe mettere *entertainment*. Neanche questo basta più a comprendere Card nel catalogo. Una azienda che si è distinta per l’integrazione tra software e hardware e adesso si preoccupa di distribuire la sua carta di credito? E aprire direttamente la banca, no?

Il bello – o il brutto, dipende – è che in proiezione futura avrebbe persino senso e persino per chi ricorda la vecchia Apple con la bandiera teschio e tibie issata sull’edificio di Bandley Drive dove si concepiva in maniacale segreto un computer diverso dagli altri.

Uno dei capisaldi di Apple, enunciati da Steve Jobs medesimo (e lo avrebbe ripetuto anche oggi, se fosse tra noi), era il controllo il più possibile assoluto di tutte le componenti di prodotto. È la ragione per cui Apple a un certo punto si è messa a fare processori, finanziare fabbriche, acquistare macchinari industriali, inventare robot per riciclare gli iPhone, esattamente come il primo Macintosh venne messo in produzione a Fremont in una fabbrica innovativa come pochissime al tempo, realizzata apposta. È la ragione per cui Apple ha voluto avere totale voce in capitolo sulla realizzazione di Apple Park invece di aspettare che architetti e maestranze finissero il proprio lavoro. È il motivo per il quale le scale di vetro degli Apple Store sono un brevetto Apple.

Nel 2019, per una azienda tra le prime in assoluto al mondo, con l’ossessione del controllo su ogni fase della produzione, è normale pensare di volere più controllo anche sul lato finanziario e, in quanto basata sul profitto invece che sul fatturato a differenza di tante altre concorrenti, concepire modi di trasformare in profitto il controllo suddetto.

È i giochi? E le riviste online? Gli show televisivi? Non è difficile. Basta pensare a iPod, iTunes, Music, TV, iBooks… l’evoluzione è evidente, di medium in medium, fino a volerli comprendere tutti.

Un altro caposaldo di Apple è l’idea di competere solamente dove l’azienda può dire qualcosa di significativo. Se pensiamo all’editoria periodica, ai giornali che scompaiono, alle edicole che si inventano qualsiasi altra cosa per stare a galla, una Apple che [guida una sterzata della tecnologia](https://macintelligence.org/posts/2019-03-21-notizie-da-poco/) in direzione delle persone è quesi un’utopia da rivoluzione, altro che l’innovazione scomparsa. L’[intersezione tra tecnologia e arti liberali](https://macintelligence.org/posts/2016-11-09-umani-e-algoritmi/), l’[attenzione al tocco e alla creatività umana](https://macintelligence.org/posts/2017-05-28-umanamente-impossibile/) privilegiate nella musica invece che gli algoritmi e le selezioni automatiche… una uber-redazione di umani che seleziona, umanamente, il meglio delle cose da leggere. C’è qualcosa di meglio oggi per riportare l’informazione, l’intrattenimento, la riflessione a qualcosa di diverso dalla *fake news*, dal cachinno costante su Facebook, alle [foto seriali e alienate](https://www.boredpanda.com/social-media-instagram-identical-photos/) di Instagram?

L’offerta di media in formato digitale è infinita, però solo Apple ha l’ambizione di metterci il tocco umano. Inserzionisti che non potranno tracciare gli utenti. Giochi senza acquisti in-app e senza pubblicità. Carta di credito senza canone. Oggi trovare un’offerta equivalente è complicato.

Dici che non è niente di nuovo, che l’innovazione continua a mancare, che tutto si può avere altrove, che ti aspettavi dell’altro?

Certamente, cose come abbonamenti mensili o una carta di credito si sono già viste in abbondanza. Ma questo significa una sfida mica da poco. Una Apple non può permettersi una carta di credito qualsiasi, come tutte le altre. Altrimenti perderà credibilità in mezzo istante e si è già visto: John Gruber [ha già castigato](https://daringfireball.net/2019/03/show_time_event_thoughts_and_observations) la scelta di tassi di interesse al pari delle altre carte e, peggio ancora, l’idea di scriverlo in piccolo perché non si noti troppo.

Apple che non fa qualcosa di diverso, di speciale, perde e si perde. Per avere una carta di credito qualunque, la abbiamo già tutti. E diversificare, farsi notare, lasciare il segno su una carta di credito è una sfida ai limiti dell’impossibile. Per non parlare degli spettacoli televisivi.

I servizi forse sono la scommessa più grossa di Apple dai tempi di iPhone. Perché ci devi mettere la faccia e dunque, in un modo o nell’altro, scommetti l’azienda.

Se riuscirà a distinguersi e restare Apple anche su questo, davvero l’orizzonte della fine crescita diventerà lontanissimo.