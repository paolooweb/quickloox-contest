---
title: "Il vero motivo"
date: 2013-11-18
comments: true
tags: [iPad]
---
Si potrebbe pensare di optare per un nuovo [iPad Air](http://www.apple.com/it/ipad-air/) perché va pazzescamente più veloce di prima (inutile dire *il doppio*, significa niente; bisogna fare scorrere un Pdf elefantiaco oppure provare una pagina web bella pesante per JavaScript).<!--more-->

Oppure perché pesa un quarto in meno.

O ancora perché è un quinto più sottile (non si ha idea se non si prova; sono apparecchi che vanno toccati per credere).

No. La vera ragione Apple non la scrive sul sito.

Si carica più rapidamente. E lo si nota.