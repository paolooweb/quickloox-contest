---
title: "Artisti della sopravvivenza"
date: 2022-06-27T01:16:31+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [Nethack, MoMA, New York, Jean-Christophe Collet, iNethack 2, Future Shock]
---
Sto giocando troppo poco ai miei amati *roguelike* (oltre che trascurare l’aspetto tecnico del blog). In attesa di riuscire a riprendere in mano alcuni *task* essenziali, sono compiaciuto di vedere [Nethack](https://www.nethack.org) entrare a fare parte della [collezione permanente del Museum of Modern Art di New York](https://www.moma.org/collection/works/199863).

Nella quale i lavori software sono ottantacinque, non ottomila. Ci sono una selezione e un criterio della stessa e, dalla mia posizione di incompetenza, posso solo plaudire alla scelta.

Probabilmente c’entra anche il trentacinquesimo compleanno del progetto, più forse dell’altro: come [spiega lo sviluppatore Jean-Christophe Collet](https://games.slashdot.org/story/22/06/25/1927234/on-nethacks-35th-anniversary-its-displayed-at-museum-of-modern-art), che vi ha partecipato attivamente per una decina di anni,

>è uno dei primi, se non il primo progetto software a essere stato sviluppato interamente via Internet da un team distribuito sul globo.

Nethack ha bisogno di essere giocato a lungo e con impegno per apprezzare il genio che è stato profuso a piene mani nel codice. Su Mac scaricarlo è un momento, mentre su iPad o iPhone ci sono varie app. Probabilmente la più fedele è iNethack 2 di Future Shock, anche ragionevolmente aggiornata rispetto allo sviluppo più recente della base di codice.

Sopravvivere fino al ventiseiesimo livello e finire Nethack è ancora più difficile che apprezzarne il genio e dovrebbe entrare in quelle classifiche tipo delle cento cose da fare nella vita.