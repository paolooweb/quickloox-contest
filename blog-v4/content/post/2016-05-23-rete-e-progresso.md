---
title: "Rete e progresso"
date: 2016-05-23
comments: true
tags: [Iran, Usa, banda, Stefano]
---
Una mail di **Stefano** fa risuonare delle corde cui sono estremamente sensibile.

**Esigenze di lavoro mi hanno prima portato negli Stati Uniti** e subito dopo in Iran a distanza di pochi giorni.

Paesi molto distanti l’uno dall’altro, non solo in latitudine.

Tra le mille differenze riscontrate, senza toccare le facili derive politiche che nulla hanno a che fare con le persone che incontri per strada, vorrei evidenziarne una: Internet e il suo controllo. In arrivo dagli USA con libero accesso in ogni dove con reti Wi-Fi, mi ritrovo a Teheran col firewall governativo che inibisce molto, inclusi i servizi delle *big corporation* targate USA come Facebook e Apple.

Si naviga a rilento e con molti limiti: oltre che frustrante è davvero odioso. Ripeto: al di là di ogni altra considerazione in tema di diritti umani.

Siamo nel 2016: **la libera connessione è oramai entrata a gamba tesa nella lista dei diritti fondamentali dell’uomo moderno**.

Ecco: dimmi che rete hai e ti dirò quanto sei progredito. Purtroppo non siamo messi benissimo.
