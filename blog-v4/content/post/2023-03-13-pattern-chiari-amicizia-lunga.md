---
title: "Pattern chiari, amicizia lunga"
date: 2023-03-13T01:49:06+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [Concetti fluidi e analogie creative, Fluid Concepts and Creative Analogies, Hofstadter, Douglas Hofstadter, ChatGPT, intelligenza artificiale, AI, IA]
---
Sì, continuo a riscoprire [Concetti fluidi e analogie creative](https://macintelligence.org/posts/2023-03-04-sublime-ignoranza/). Nel 1977 Douglas Hofstadter era *assistant professor* presso l’Indiana University e aveva appena cominciato ufficialmente la propria attività di ricerca sull’intelligenza artificiale.

Uno dei primissimi progetti consistette nel chiedere agli studenti del corso di *computer science* di scrivere un programma il più possibile capace di riconoscere *pattern lineari*. Per esempio, guardare una sequenza di numeri e capire che regola governa la loro successione. Se scrivo `1, 1, 2, 3, 5, 8, 13…` un programma capace di riconoscere *pattern*, schemi che si ripetono, deve saper scoprire che ogni termine equivale alla somma dei due che lo precedono.

Il campo è preciso e ristretto; al tempo stesso, offre possibilità infinite di svariare e proporre sfide a qualsiasi livello di complessità.

Hofstadter dedica una sezione specifica a una sequenza inviatagli da un amico e racconta di come è arrivato a decifrarla. Nella narrazione non conta molto che arrivi alla soluzione, ma il *come* lo faccia; il senso di produrre programmi capaci di riconoscere pattern è che, nella sua concezione di intelligenza, questa dote ne faccia necessariamente parte e abbia pure una certa importanza.

Ho chiesto a una presunta intelligenza quale sia il prossimo termine della serie `1, 1, 2, 3…` e mi è stato risposto correttamente *5*. La serie è stata riconosciuta come quella (piuttosto famosa) che prende il nome dal (piuttosto noto) matematico [Leonardo Pisano](https://www.torinoscienza.it/personaggi/leonardo-pisano-detto-fibonacci), detto Fibonacci.

La sequenza in *Concetti fluidi* è `0, 1, 2, 720!…`

(`720!` è il *fattoriale* di 720, ovvero `1 x 2 x 3 x 4 x 5 x 6 x 7 x 8… x 720`).

Ho chiesto alla presunta intelligenza che un capoverso fa ha individuato la sequenza di Fibonacci e ho ricevuto una risposta completamente illogica, che riporto solo per la parte iniziale:

>Il prossimo termine della sequenza sarebbe il prodotto dei fattoriali dei primi 7 interi consecutivi: 3! x 4! x 5! x 6! x 7! x 8! x 9!

Piccola sfida: qualche umano sa fare meglio?

L’obiettivo non è tanto trovare la soluzione, ma riflettere sul percorso seguito dal pensiero per trovare un pattern convincente per la serie `0, 1, 2, 720!…`. Quindi sarebbe estremamente produttivo anche solo pensare al funzionamento della nostra mente, perfino in assenza di una risposta finale (che esiste, è una serie ineccepibile senza trucchi e non richiede calcoli particolari). Come ci muoviamo per trovare uno schema, davanti a questa serie?