---
title: "Basta asciutta"
date: 2022-06-09T18:04:56+01:00
draft: false
toc: false
comments: true
categories: [Internet]
tags: [Wwdc, Apple. Cook, Tim Cook, WeatherKit]
---
Chiaramente tutti si dedicano agli annunci di [Wwdc](https://developer.apple.com/wwdc22/).

Sarebbe carino che qualcuno si dedicasse ai rumor che si è speso a dibattere su Internet e si cono rivelati inesistenti e ammettesse di essere un ciarlatano. Carinissimo se qualcuno delle legioni che spendono tempo e attenzione su queste cose si dichiarasse intellettualmente al livello del gossip sulla Royal Family o sugli attori delle serie televisive. So che è chiedere troppo.

Nessuno si è minimamente focalizzato sugli aspetti comunicativi del format, che a me invece sembrano interessanti e dovrebbero forse esserlo per aziende che fatturano un centesimo di Apple, magari candidate a rubare un paio di dritte. In fin dei conti più darsi che Apple qualcosa di comunicazione lo abbia capito.

Ho visto in modo distratto qualche commento su una presentazione di plastica, artefatta, fredda. Sempre distrattamente e senza prove, mi figuro gente che, ai tempi del Moscone Center e degli annunci dal palco, irrideva la platea di entusiasti pronti ad applaudire e fare da *claque* a Steve Jobs. Prima il clima era troppo caldo, ora troppo freddo. Per certi autorevoli commentatori esiste solo la propria temperatura, misura del mondo.

Ho trovato la presentazione splendidamente *asciutto*. Non secca, non arida; asciutta.

Si nota la mancanza di preamboli, parentesi, descrizioni teoriche di *mission* o fantomatici valori aziendali, foto di gruppo dei commerciali in gita premio. L’amministratore delegato apre e chiude, enuncia poche informazioni di base e poi passa la palla semplicemente con un nome. Niente prego, grazie, oggi-vi-parlerò-di, i fatti e le informazioni relative al segmento, null’altro.

Niente ringraziamenti al termine della propria parte, *back to Craig* ed è abbastanza. Nelle presentazioni aziendali classiche, a questo punto comincia un patetico minuetto di passaggio del testimone, ci si incrocia sul palco per via di un passaggio troppo stretto, c’è la presentazione da fare ripartire, o la chiavetta nuova, schermata sulla finestra con un elenco di file PowerPoint.

A Wwdc, nel momento in cui si dice *back to…*, la transizione si è già conclusa e il nuovo relatore riprende senza soluzione di continuità. Il suo nome compare in sovraimpressione, mica ha bisogno di esordire con *salve, il mio nome è Bianca Rossi e da cinque anni sono Senior Vice Specialist Digital Something EMEA*. Nessuno è vestito in modo tragicamente sbagliato o conciato come un pinguino quando sono tutti scamiciati o viceversa.

Le slide dovrebbero essere oggetto di un post a parte ma risparmio la noia. Comunque vanno da sole, magicamente, mentre il relatore si concentra esattamente sul suo intervento. Non c’è la diretta chiaramente e, se si fosse verificato qualche intoppo, non si noterebbe nel montaggio finale, ma c’è da notare quanti pochi siano i cambi di scena o di inquadratura che permettono di nascondere una toppa. La sensazione è che le persone a parlare siano *preparate* e che le slide siano complementari al suo intervento, invece che *questo lo dobbiamo dire per forza, che siamo inclusivi o che rispettiamo l’ambiente*. In una presentazione efficace si vede da sé.

Niente *buzz*, niente aziendalese, niente *mettere i clienti al centro* (Tim Cook lo ha fatto un paio di volte in passato e suonava falso come una moneta da tre euro).

C’è un discorso di risorse ovviamente, di mezzi, per carità. Mica tutti possono permettersi un apparato tecnico e scenico come quello di un *keynote* Apple. Ma lavorare ai tempi giusti per ogni relatore, al passaggio del microfono, al cancellare le cose che piace sentire a te ma fregano zero al tuo spettatore, costa così tanto?

Il *keynote* andrebbe guardato e riguardato da questo punto di vista anche se Apple avesse presentato le previsioni del tempo.

(In effetti [WeatherKit](https://developer.apple.com/weatherkit/) è interessante).