---
title: "Un MacBook a quattro zampe"
date: 2023-05-16T02:30:21+01:00
draft: false
toc: false
comments: true
categories: [Hardware]
tags: [M1, Apple Silicon]
---
Ci [gira sopra nativamente Unreal Engine](https://macintelligence.org/posts/2023-05-14-più-irreale-di-così/) e va bene, ma guarda questa [applicazione](https://twitter.com/EpisodeYang/status/1658133656760897536).

<blockquote class="twitter-tweet"><p lang="en" dir="ltr">Fun fact: Do you see the white box on top? We stripped an Apple M1 Mac to its core to run visual processing onboard the robot : ) <a href="https://twitter.com/xiaolonw?ref_src=twsrc%5Etfw">@xiaolonw</a> <a href="https://twitter.com/RchalYang?ref_src=twsrc%5Etfw">@RchalYang</a> <a href="https://t.co/KrkfTSJL1X">https://t.co/KrkfTSJL1X</a></p>&mdash; Ge Yang (@EpisodeYang) <a href="https://twitter.com/EpisodeYang/status/1658133656760897536?ref_src=twsrc%5Etfw">May 15, 2023</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

Il testo tradotto suona così:

>Curiosità: vedi lo scatolino bianco in cima? Abbiamo ridotto al minimo l’M1 di un Mac per effettuare elaborazione visiva in groppa al robot.

Ecco che cosa succede quando un processore è molto potente ma richiede meno energia degli altri: inizia ad andare in giro un po’ dove gli pare.

*Un [MacBook a quattro zampe](https://twitter.com/DrJimFan/status/1658166526275645442). Incredibile potenza di elaborazione visiva in campo aperto.*

<blockquote class="twitter-tweet"><p lang="en" dir="ltr">You see the little white box on the back? That&#39;s Apple M1 chip.<br><br>This is a Macbook with 4 legs 🤣. Incredible visual processing power in the wild. <a href="https://t.co/YhyAMKeMJs">pic.twitter.com/YhyAMKeMJs</a></p>&mdash; Jim Fan (@DrJimFan) <a href="https://twitter.com/DrJimFan/status/1658166526275645442?ref_src=twsrc%5Etfw">May 15, 2023</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>