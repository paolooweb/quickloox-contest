---
title: "Trascrizione obbligatoria"
date: 2020-07-19
comments: true
tags: [Happy, Scribe]
---
Avevo già parlato di [Happy Scribe](https://www.happyscribe.co/) [tre anni fa](https://macintelligence.org/blog/2017/10/24/felice-di-trascriverla/) e lanciato un link di referral [poco dopo](https://macintelligence.org/blog/2018/01/17/regalo-minuti/), perché, certo, ne ricevo un beneficio. Il servizio però mi piace e il vantaggio che altri possono ricavere dalla sua scoperta è ben superiore.

Da allora ho usato Happy Scribe solo un’altra volta, oggi. Ho scoperto che il mio piano prezzi è obsoleto e che ora il prezzo è superiore, dodici euro l’ora. Nondimeno il servizio è stato impeccabile e allora devo proprio rifarlo: chi aprisse un account a pagamento a partire [da questo link](https://www.happyscribe.co/r/ea9e8d9c0a) mi farebbe guadagnare sessanta minuti di trascrizione da audio. Che probabilmente, vista la media, sfrutterò a inizio 2023.

Se non erro, il primo file con Happy Scribe in prova è gratis. Apprezzo l’editing in linea e la semplicità assoluta di tutto il servizio.

Mi raccomando: preferisco sapere che è stato utile a qualcuno, più che ricevere sessanta minuti che stanno lì tre anni prima di essere usati.