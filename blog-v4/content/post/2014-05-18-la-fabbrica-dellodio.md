---
title: "La fabbrica dell'odio"
date: 2014-05-19
comments: true
tags: [iPhone, Wired, Giappone]
---
[Perché i giapponesi odiano iPhone](http://www.wired.com/2009/02/why-the-iphone/), titolava *Wired* cinque anni fa.<!--more-->

Cinque anni dopo, in Giappone [un computer da tasca su tre è un iPhone](http://www.bloomberg.com/news/2014-05-13/apple-boosts-japanese-market-share-after-ntt-docomo-sells-iphone.html).

C’è da chiedersi perché fabbricare notizie che non esistono invece che sforzarsi di comprendere la realtà com’è, rispetto a quella che si vorrebbe fosse.