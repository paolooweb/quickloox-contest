---
title: "Stampe e regie"
date: 2018-03-09
comments: true
tags: [Owin18]
---
Da grande evento aziendale in prestigiosa villa ai confini di Roma, le solite foto di banco regia e giornalista estero accreditato.

 ![Banco di regia a Owin18](/images/owin18.jpg  "Banco di regia a Owin18") 

 ![Giornalista estero accreditato a Owin18](/images/owin18-press.jpg  "Giornalista estero accreditato a Owin18") 