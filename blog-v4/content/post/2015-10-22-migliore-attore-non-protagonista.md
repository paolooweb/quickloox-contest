---
title: "Migliore attore non protagonista"
date: 2015-10-22
comments: true
tags: [Blue, Mac, OSX, Windows, Unix, OpenBsd]
---
All’amico **Blue**, per il quale *oggi i personal computer sono monopolio di Mac OS e Windows. Linux lost in action*, segnalo che si sono appena festeggiati i [venti anni di OpenBsd](http://www.openbsd.org/58.html).<!--more-->

OpenBsd è praticamente non usato da alcuno fuori da chi ne cura lo sviluppo. Eppure è di importanza straordinaria, perché è una versione di Unix concentrata sulla sicurezza. Una grossa parte di quello che funziona in termini di sicurezza sui nostri computer lo si deve al lavoro fatto su OpenBsd.

Meriterebbe anzi una donazione anche piccola, anche se pensiamo a torto che il suo non-uso presso il grande pubblico lo renda poco importante. Forse è il contrario.

Inoltre è l’unico sistema operativo i cui sviluppatori [accompagnano con canzoni](http://www.openbsd.org/lyrics.html#58a) l’arrivo di una nuova versione.
