---
title: "Perdere dal titolo"
date: 2024-02-15T00:05:32+01:00
draft: false
toc: false
comments: true
categories: [Software, Internet]
tags: [Excel, Norvegia, Financial Times, Slashdot]
---
Se il titolo suona come [L’errore su Excel da novantadue milioni di dollari del Fondo sovrano di investimento norvegese](https://it.slashdot.org/story/24/02/12/211251/the-norwegian-sovereign-wealth-funds-92-million-excel-error), è già stato detto tutto.

La contabilità del fondo sovrano della Norvegia è tenuta su Excel. Excel.

Succede che un dipendente distratto scriva nel foglio *dicembre* al posto di *novembre* dentro una casella. Ecco fatto.

No, il problema non sta nel dipendente. L’errore umano esiste sempre e sempre sta in agguato. Semplicemente, un programma come quello, privo di protezioni, privo di intelligenza, privo di controllo degli input, privo di struttura, privo di strumenti di verifica è già a rischio per tenere i conti di casa.

Il programma [incoraggia le cattive abitudini, i comportamenti trascurati e il disordine mentale e tabellare](https://macintelligence.org/posts/2021-03-25-disimparare-le-tabelline/). Come può essere possibile che nella gestione di un fondo da miliardi i mesi vengano inseriti a mano? È possibile solo se c’è incuria e basta aspettare abbastanza tempo che, prima o poi, l’errore si verifica.

Per quanto Excel si pregi di mostrare all’utente l’illusione di controllare milioni di caselle, la realtà è che, raggiunta una certa quota di complessità, o di ricchezze in ballo, bisogna scappare come antilopi e adottare un software adeguato.