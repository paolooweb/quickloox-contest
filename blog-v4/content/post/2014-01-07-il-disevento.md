---
title: "Il disevento"
date: 2014-01-08
comments: true
tags: [Ces]
---
[Riccardo](http://morrick.me) ha splendidamente, per quanto crudamente, sintetizzato su Twitter.<!--more-->

<blockquote class="twitter-tweet" lang="en"><p>CESspool 2014. &#10;&#10;<a href="https://twitter.com/search?q=%23nuffsaid&amp;src=hash">#nuffsaid</a></p>&mdash; Riccardo Mori (@morrick) <a href="https://twitter.com/morrick/statuses/419875677236916224">January 5, 2014</a></blockquote>
<script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>

Mi piovono da ogni parte annunci, notizie, commenti, via web, Rss, *social media*, sul [Consumer Electronics Show](http://www.cesweb.org) di Las Vegas.

Mi pare l’edizione meno interessante nella storia dell’uomo, risultato che richiede comunque un certo impegno visti i pregressi. Niente che innovi veramente, niente che cambi veramente, nessuna demo che rimandi a un prodotto rivoluzionario da qui a tre mesi. Il nulla concentrato. Il contrario di un evento, una liturgia articolata dove si parla molto e niente accade.

Mi permetto giusto di tifare per le [*console* Steam di Valve](http://store.steampowered.com/news/12175/), che rinfrescano un po’ il mondo oramai trito dell’hardware generico per videogiochi.

Naturalmente c’è stata anche la fantastica prestazione del regista Michael Bay sul palco di Samsung. Pagato per presenziare, senza crederci, con il gobbo – Windows – guasto. La sintesi della filosofia Android. Bay si è detto [imbarazzato](http://michaelbay.com/2014/01/06/ces/) sul proprio blog e va capito. Se non altro fa spendere soldi a Samsung, che ne intasca troppi.

<iframe width="560" height="315" src="//www.youtube.com/embed/_tqRyzTvNKE" frameborder="0" allowfullscreen></iframe>