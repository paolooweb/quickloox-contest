---
title: "Non c’è niente da vedere"
date: 2021-08-10T00:49:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [Apple, Expanded Protection for Children] 
---
Non intendo perdere di vista la questione del [curricolo informatico per la scuola primaria](https://macintelligence.org/posts/Un-curricolo-per-lestate.html), solo che sui social è partito il tritacarne relativo alla prossima [protezione allargata dei bambini](https://macintelligence.org/posts/Grandi-e-piccine.html) che arriva su macOS, iOS, iPadOS, iCloud.

A scanso di equivoci, ribadisco la mia posizione primaria: *ad Apple dovrebbe importare zero di che contenuti tiene la gente sui propri apparecchi*.

Secondariamente, il tema è scivoloso, ambiguo, rischioso, delicato, per molti versi inesplorato soprattutto tecnicamente, controverso, polarizzante eccetera.

Bisogna essere preparati e comprendere molto bene la questione, altrimenti si alza il polverone, che non fa bene né a chi fosse d’accordo con il metodo né a chi lo disapprovasse.

Non sto parlando dei leoni da tastiera, dei fessi, dei disadattati da social; vedo grandi firme, persone serie e preparate, normalmente competenti, che prendono svarioni – in questo contesto – gravi.

Sembra anche che chiunque abbia una posizione radicata sulla materia, quale che sia, approfitti della questione per interpretare l’annuncio di Apple esattamente nello spirito della sua posizione, non importa che cosa ci sia scritto davvero.

Per il team di comunicazione di Apple sarà una bella prova, perché al momento le persone non hanno proprio capito o hanno frainteso o – peggio – vogliono capire solo quello che gli fa comodo. Se continua così, resteranno cicatrici sgradevoli.

Come consiglio a chi non è precipuamente interessato: stare alla larga dalle discussioni. In questo momento sono altamente tossiche. Come consiglio a chi fosse precipuamente interessato: leggere, rileggere, assicurarsi di possedere la materia. Ti trovi davanti anche persone assennate e competenti, normalmente, che non lo hanno fatto e sproloquiano.

Domani si riparte con il curricolo.

*Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile.*               