---
title: "Dilettanti al lavoro"
date: 2017-03-08
comments: true
tags: [ Jamf, Mac, iPad, iPhone]
---
C’è molto da leggere nell’[inchiesta Jamf](https://resources.jamf.com/documents/books/managing-apple-devices-in-the-enterprise.pdf#63624) relativa al 2016 sull’uso di apparecchi Apple in azienda. Cito solo due dati: il 91 percento delle aziende interpellate ha in uso Mac. Il 99 percento usa iPhone e/o iPad.

[Prova a dirlo a un viaggiatore nel tempo proveniente da vent’anni fa](http://daringfireball.net/linked/2017/03/07/jamf-apple-enterprise), è il commento di John Gruber.

L’inchiesta è seria e profonda a sufficienza per avere una buona attendibilità. Ci sono tanti numeri e anche dove difetta l’inglese vale la pena di compulsarla.

Il secolo buio è definitivamente terminato. Molto resta ancora da fare, ma l’alibi del Mac non adatto alle aziende veramente, anno 2017, fa ridere o piangere, secondo interpretazione.

Bel paradosso che l’epoca attuale, dove si usa Mac in nove aziende su dieci, sarebbe quella dei professionisti dimenticati da Apple. Si vede che negli uffici lavorano solo dilettanti.
