---
title: "La zuppa di dati"
date: 2021-12-15T01:09:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [Obsidian, MacStories, Federico Viticci, Selects 2021, Markdown, Todoist, Readwise, Kindle, Kanban] 
---
Credevo fosse passato molto più tempo da quando [ho scritto delle nuove organizzazioni di file](https://www.macintelligence.org/posts/Il-filesystem-di-Asimov.html), che nelle nuove costellazioni personali di device e cloud finiscono a volte per ignorare la tradizionale suddivisione in cartelle gerarchiche. Neanche due mesi, invece.

Nei quali MacStories ha pubblicato i suoi [Selects 2021](https://www.macstories.net/stories/macstories-selects-2021-recognizing-the-best-apps-of-the-year/): le scelte delle migliori app del 2021.

Avevo citato [Obsidian](https://obsidian.md/) come esempio dei nuovi modi di sfruttare l’archiviazione dei file senza stare a pensare alle cartelle ed ecco che Obsidian è *App of the Year*, definita da Federico Viticci *la app Markdown più veloce ed efficiente che io abbia mai usato* e molto più di questo:

> I risultati di questi compromessi sono incredibili: le note in Obsidian possono diventare una [Kanban board](https://www.atlassian.com/agile/kanban/boards) o una interrogazione di database; è possibile integrare Obsidian con [Readwise](https://readwise.io) e importare automaticamente brani dalla propria app di lettura differita o da libri Kindle; si possono aggiungere alla app funzioni di calendario o amministrazione di attività; un sistema di plugin consente di aggiungere barre laterali personalizzate, modi diversi dal solito di contare le parole e perfino plugin per installare altri plugin. Noi abbiamo creato plugin per integrare nativamente [Todoist](https://todoist.com), scrivere bozze longform in [Markdown](https://www.markdowntutorial.com) e inserire rapidamente link Markdown. Se non troviamo una funzione specifica in Obsidian, probabilmente c’è un plugin che provvede.

A parte le funzioni di personalizzazione della app in quanto tale, Obsidian è uno strumento molto potente per sfruttare i dati presenti nei propri sistemi (stavo per dire *presenti nel computer*, solo che i computer sono più di uno). E la struttura di file sottostante richiesta per il suo funzionamento è… una cartella di file Markdown.

L’intelligenza per distinguere, separare, riunire, coordinare ce la mette lui. Sotto c’è una cartella, nient’altro.

Se guardo con gli occhi della fantascienza – scrivevo del *filesystem di Asimov* – Obsidian mi sembra persino una forma limitatissima di quello che servirebbe: l’embrione di una app capace di applicare intelligenza a tutti i file presenti nei miei computer, non importa dove, non importa come, basta che ci siano.

Non è che per forza la zuppa di dati debba diventare la dieta obbligatoria di tutti; certamente, ora nel menu ci sono nuove alternative alla minestra di cartelle come la si faceva una volta.

*Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile.*