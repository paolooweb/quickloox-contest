---
title: "Amore e sintonia"
date: 2016-01-31
comments: true
tags: [Apple, MacBook, Dalrymple]
---
Finora ho sentito parlare di [MacBook](http://www.apple.com/it/macbook/) in termini negativi, critici oppure neutri e distaccati; nessuno che abbia approvato la macchina più che timidamente.<!--more-->

Per questa ragione mi sorprende il *post* di Jim Dalrymple su The Loop, [Il mio amore per MacBook](http://www.loopinsight.com/2016/01/19/my-love-of-macbook/). Anche perché lui è uno che non va per il sottile, quando c’è da criticare.

Chiama MacBook *uno dei suoi Mac preferiti di sempre* e spiega che *preserva la visione di Apple, che dà ai clienti quello che gli serve prima che sappiano di averne bisogno*.

La tastiera del computer, di cui si è parlato mediamente in termini perplessi a essere gentili, è la migliore che lui abbia mai usato. Conclude così:

>Per chi vuole un portatile potente e versatile da portare ovunque, non mi viene in mente niente meglio di MacBook. È il mio “goto computer”.

Lo hanno pagato? È impazzito? Ha iniziato la fase senile? Trovo una spiegazione nel paragrafo dove scrive che MacBook non è adatto per collegarci la sua chitarra e registrare: *l’ho fatto e ha funzionato, ma non è per questo che volevo un MacBook; ecco perché volevo un iMac*.

Forse è questione di rendersi conto degli usi ideali di una macchina e del fatto che, da anni, oltre ai computer tuttofare esistono anche quelli più adatti per certe categorie di utilizzo, da lasciare stare, semplicemente, se la propria categoria non rientra.

L’amore deriva anche da una sintonia con l’oggetto, o il soggetto, dell’amore stesso e significa apprezzare anche certi difetti, siano reali o percepiti.