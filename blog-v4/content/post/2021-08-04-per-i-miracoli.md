---
title: "Per i miracoli"
date: 2021-08-04T01:52:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [Apple, Mappe, Eurovision] 
---
L’Italia ha vinto il concorso canoro Eurovision, i campionati europei di calcio, l’oro maschile sui cento metri piani olimpici e quello del salto in alto sempre maschile, ambedue per la prima volta nella storia.

Un vento di euforia spazza la nazione.

Eppure tutto deve essere messo in prospettiva e in secondo piano, se si pensa che [l’Italia è il prossimo Paese ad avere nella beta di iOS 15 le nuove Mappe](https://www.justinobeirne.com/new-apple-maps-italy-testing).

Sembra la battuta su possibile, impossibile e miracoli, tradotta in pratica. (Gli esempi di Mappe presenti sul sito di Justin O’Beirne fanno benissimo sperare). Se va avanti così finisce che passano persino le riforme.

*Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile.*