---
title: "La vertigine dell’abisso"
date: 2017-10-21
comments: true
tags: [xz, git, Homebrew, curl, Archimista, nano]
---
Pomeriggio trascorso a cercare di installare [Archimista](http://www.archimista.it) su un Mac aggiornato, diciamo così, a Snow Leopard (10.6.8).

Gli sviluppatori, bontà loro, hanno creato unicamente i file eseguibili per Windows; tuttavia persone volonterose hanno redatto una [guida all’installazione su Mac](http://www.archiviando.org/forum/viewtopic.php?f=67&t=2289). Sì, il software è stato realizzato con componenti open source e la ragione per cui i binari esistono solo per Windows è la pigrizia, o l’incapacità, o la cattiveria, boh.

I dieci passi della guida sono tutti collaudati, solo che sono arrivato a completare il terzo; una tempesta quasi perfetta di errori e circoli viziosi ha consumato quasi tutto il tempo a disposizione.

La faccio breve: sulla macchina in questione, [curl](https://curl.haxx.se) dava un errore di certificati e di fatto non scaricava certo software. A parte questo, c’era bisogno di [git](https://git-scm.com), che non si poteva installare perché da Internet arrivava un file con compressione [xz](https://tukaani.org/xz/). xz era installato, ma [Homebrew](https://brew.sh) – attraverso cui accadeva tutto – non riusciva ad accedervi e ogni tentativo di risolvere la situazione restituiva errori di permessi apparentemente insormontabili.

Così tentavo di installare ex novo xz, solo che Homebrew dava per scontata la preesistenza di git, il quale non poteva essere installato senza xz…

Passo dopo passo ho installato git autonomamente, fuori dall’ambito di Homebrew; poi ho eradicato a mano qualsiasi traccia di xz dalla macchina, per procedere a una vera reinstallazione radicale. Il sistema ha mosso obiezioni, ma ho cambiato i permessi sempre a mano, nonché temporaneamente, per tacitarlo.

Infine, con un colpo di fortuna, ho capito che il problema dei certificati consisteva in un file di autorizzazioni che era presente, solamente vuoto. Nella documentazione di Homebrew ho trovato la soluzione al caso e finalmente ho potuto cominciare l’installazione vera e propria.

È ancora presto per vantarsi di avere fatto girare Archimista su Mac e tuttavia ho scoperto varie cose interessanti sull’uso del Terminale per installare software. Ho anche verificato che, se si dispone di codice già scaricato, è sufficiente piazzarlo nella cartella delle cache di Homebrew. Lui si accorgerà che il pacchetto è già presente, invece di cercare di scaricarlo da un sito che non è più attivo (una delle condizioni a contorno che ha reso ancora più saporito il pomeriggio).

Lavorare con il Terminale può diventare infernale, quando si finisce in situazioni circolari come quella appena vissuta, dove puoi fare *a* solo quando avrai completato *b*, il quale dipende da *c*, che non parte senza *a*.

Contemporaneamente, Homebrew è una sorta di nirvana del mondo Unix, con una capacità di descrivere errori, problemi e soluzioni chilometri avanti a qualunque altro software abbia mai usato. Fa venire la voglia di provare il brivido dell’abisso, seppure consci che si rischia di non risalire mai più.