---
title: "L’acchiappasegugi"
date: 2023-06-01T01:14:41+01:00
draft: false
toc: false
comments: true
categories: [Internet]
tags: [Ai, intelligenza artificiale, LeCun, Yann LeCun]
---
Ove possibile, uso Bard. Quando non è possibile, uso ChatGPT (ma devo ancora provare seriamente Claude). Lo strumento esiste, ha le sue capacità e i suoi limiti, semplifica certe attività, è sprecato o stupido per altre, come tutti gli strumenti.

Se per caso parte il cervello, causa temperature estive, picco di stress, bisogno di spiritualità represso, illusione ottica, ingenuità conclamata, e si comincia a straparlare di comprensione, intelligenza, intenzione, imprevedibilità e quanto altro, o peggio ancora si fanno previsioni millenaristiche o apocalittiche, teniamo presente [quanto dice Yann LeCun](https://twitter.com/ylecun/status/1663616081582252032) che qualcosa ne sa e oltretutto tifa per l’intelligenza artificiale:

<blockquote class="twitter-tweet"><p lang="en" dir="ltr">Super-human AI is nowhere near the top of the list of existential risks.<br>In large part because it doesn&#39;t exist yet.<br><br>Until we have a basic design for even dog-level AI (let alone human level), discussing how to make it safe is premature. <a href="https://t.co/ClkZxfofV9">https://t.co/ClkZxfofV9</a></p>&mdash; Yann LeCun (@ylecun) <a href="https://twitter.com/ylecun/status/1663616081582252032?ref_src=twsrc%5Etfw">May 30, 2023</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

>L’intelligenza artificiale superumana non è minimamente in cima alla lista dei rischi esistenziali. In larga parte, perché al momento non esiste. Fino a quando avremo almeno il progetto di una intelligenza non diciamo umana, ma a livello canino, discutere di come renderla sicura è prematuro.

Certamente, quando avremo una intelligenza artificiale capace di fare almeno il cane, attirerà tutti i segugi al momento in cerca di emozioni forti per vite deboli, con tanta voglia di avere a che fare con *qualcuno* invece che con *qualcosa* e nell’impossibilità emotiva di capire che, dietro, c’è un ingegnoso meccanismo probabilistico e nulla più.