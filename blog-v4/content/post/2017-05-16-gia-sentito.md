---
title: "Già sentito"
date: 2017-05-16
comments: true
tags: [Fraunhofer, Mp3, Aac, Grill]
---
L’Istituto Fraunhofer, che ha creato il formato di audio compresso Mp3, [ha annunciato ufficialmente la fine](https://www.iis.fraunhofer.de/en/ff/amm/prod/audiocodec/audiocodecs/mp3.html) del supporto al formato stesso.<!--more-->

Bernhard Grill, direttore della divisione del Fraunhofer interessata al progetto, [ha parlato in questi termini](http://www.npr.org/sections/therecord/2017/05/11/527829909/the-mp3-is-officially-dead-according-to-its-creators) del formato Aac:

>lo standard di fatto per lo scaricamento di musica e video su telefoni mobili […] più efficiente di Mp3 e con un sacco di funzioni in più.

Quando lo raccontavi dieci anni fa passavi per un fanatico di quelli che fanno sempre le cose a modo loro, mentre ci sono gli standard che usano tutti, e ho una chiavetta piena di Mp3 nel cassetto che sono tanto comodi e non è vero che Aac suona meglio perché ho appena provato sullo stereo.

Chissà se adesso qualcuno ci sente meglio.