---
title: "Attaccati a Ocsp"
date: 2020-11-16
comments: true
tags: [Apple, Arm, M1, Ocsp, Jannone, macOS, Big, Sur, iMac, Pro, MacBook, Pro, Ung, PcWorld, ExtremeTech, trustd, Paul, Anandtech]
---
Tre righe:

>No, macOS **non invia** uno hash delle tue app ogni volta che le lanci.<br>Dovresti essere consapevole che macOS **potrebbe trasmettere** delle informazioni opache riguardanti il certificato da sviluppatore di chi ha creato la app. Questa informazione viene inviata in chiaro sulla tua rete.<br>Probabilmente **non dovresti** bloccare ocsp.apple.com con Little Snitch o nel tuo file hosts.

È la conclusione del post di Jacopo Jannone [Apple tiene veramente traccia di tutte le app che lanci? Una disamina tecnica](https://blog.jacopo.io/en/post/apple-ocsp/).

È anche la conclusione delle polemiche attorno al tema. Il ricercatore Jeffrey Paul ne ha approfittato per lanciare [allarmi](https://sneak.berlin/20201112/your-computer-isnt-yours/) tipo *[la versione attuale di] macOS invia ad Apple un identificatore unico di ogni e qualsiasi programma che lanci, quando lo lanci*.

Non è vero. O è stata scritta una cosa sbagliata, o è stata scritta una bugia. Dubbi? Leggiamo Jannone:

>Dopo avere chiuso Firefox e averlo riaperto, nessuna richiesta è stata effettuata [di trasmissione di hash]. Questo è ragionevole e indica che il controllo dei certificati non viene eseguito a ogni lancio, ma solo dopo che è passato del tempo dall’ultima volta che è successo.

**Aggiornamento del 16 novembre:** Secondo Jeff Johnson, l’intervallo di tempo in cui non viene effettuato un controllo dei certificati al lancio di una app [era di cinque minuti ed è stato portato a mezza giornata](https://lapcatsoftware.com/articles/ocsp.html).

>È chiaro che il servizio `trustd` su macOS non invia uno hash delle app che lanci. Invece, invia informazioni riguardanti qualche certificazione, come chiunque si aspetterebbe dopo avere compreso che cos’è Ocsp.

Dopo avere scritto chiaramente che l’analisi di Paul *non è proprio precisa*, Jannone – invece – precisa ulteriormente:

>È possibile una situazione in cui macOS può davvero inviare ad Apple lo hash di un eseguibile e questo avviene quando Gatekeeper controlla l’esistenza di un ticket di notarizzazione sui server Apple al primo lancio dell’applicazione, nel caso il ticket non sia allegato alla app.

>Questo ha niente a che vedere con Ocsp. Avviene in circostanze specifiche e il controllo viene effettuato attraverso via connessione cifrata (Https) su un endpoint situato a api.apple-cloudkit.com. Durante questa procedura, l’utilizzatore vede comparire una finestra con una barra di progressione.

Il meccanismo che ha destato tante ansie è proprio del sistema di verifica della genuinità delle applicazioni lanciate su Mac e mira a proteggere dall’esecuzione di app potenzialmente insicure, che potrebbero creare problemi di sicurezza. Può non piacere e può non piacere la filosofia che sta dietro al meccanismo; in ogni caso, non esiste un rischio privacy, semmai una protezione contro il malware che qualcuno potrebbe – legittimamente – trovare esagerata. In questo caso, però, non stiamo parlando di una procedura clandestina che viola la privacy delle persone sotto il loro naso, ma di una misura di sicurezza nota e documentata che, semmai, protegge i meno esperti.

Chiusa la parte seria, mi permetto alcuni paragrafi di pura malignità.

Notato come appaiano test di velocità piuttosto favorevoli a M1, che per esempio [batte un MacBook Pro 16” al massimo della configurazione](https://9to5mac.com/2020/11/11/macbook-air-with-m1-chip-beats-16-inch-macbook-pro-performance-in-benchmark-test/)? I benchmark valgono quello che valgono, il mondo reale è un’altra cosa però, insomma, che un MacBook Air possa permettersi di solleticare un MacBook Pro 16” è roba interessante. [M1 batte anche iMac Pro](https://www.techradar.com/news/apple-m1-beats-the-xeon-based-macpro-so-much-it-hertz).

Il mondo PC è in subbuglio e sono apparse reazioni quasi rabbiose come quella di Gordon Ung su *PC World*: [No, il nuovo MacBook Air non va più veloce del 98 percento dei portatili PC](https://www.pcworld.com/article/3596814/no-the-new-macbook-air-is-not-faster-than-98-of-pc-laptops.html). Il problema è che Ung contesta lo slogan del marketing Apple. La quale magari esagera o provoca o mira a fare sensazione, e pazienza. Solo che, dopo avere reagito all’affermazione di marketing, nell’articolo c’è sostanza zero.

ExtremeTech riprende lo stesso titolo ma scende nel tecnico ed è costretta a modificarlo: [Il System on Chip M1 di Apple è notevole, non va più veloce del 98% dei portatili PC](https://www.extremetech.com/computing/317228-apples-new-m1-soc-looks-great-is-not-faster-than-98-percent-of-pc-laptops). Notato l’aggiustamento? L’articolo conclude così:

>Al momento possiamo presumere che x86 vinca ancora confronti contro M1 grazie a clock e conteggi di core superiori; ma questo durerà solo fino a quando Apple inizia a scalare verso l’alto il proprio progetto di SoC. Non conto su una finestra di attesa molto ampia.

(Per chi volesse proprio tutta la parte tecnica, c’è [l’analisi di AnandTech](https://www.anandtech.com/show/16226/apple-silicon-m1-a14-deep-dive/) a fornire tutti i particolari oggi umanamente consultabili rispetto a M1).

Insomma, chi ha sempre vantato la superiorità dei PC in fatto di prestazioni oggi si trova a combattere contro… MacBook Air modello base. È una situazione un po’ da disperati.

E se venisse voglia di attaccarsi a qualsiasi cosa, ma proprio qualsiasi, a costo di fare passare una procedura di sicurezza come rischio della privacy e così alzare un polverone capace di distrarre da quello che accade a livello di processori?

Pura malignità, l’ho già detto. Nel dubbio, il titolo del post funziona con entrambe le accentazioni di *attaccati*.