---
title: "Cambiamenti silenziosi"
date: 2019-11-16
comments: true
tags: [watch, Sam, People, Noise]
---
Ma quanto è banale, all’apparenza, la storia del [figlio autistico con la voce troppo alta](https://people.com/human-interest/texas-dad-apple-watch-noise-app-helped-son-autism/) e del padre che scopre di poterlo aiutare con la app di watch che avvisa quando un rumore è dannoso per la salute?

All’apparenza. Riga dopo riga emerge che il padre ha scritto su Facebook della cosa e più di cento famiglie si sono ripromesse di provare. Si scopre che ci sono negli Stati Uniti cinque milioni di soggetti che hanno lo stesso problema.

L’incredulità della mamma per un sistema che, per la prima volta, porta qualche risultato. L’ammarezza del padre al pensiero di quanto sarebbe migliorata la loro vita se questa tecnologia fosse stata disponibile anno fa, quando il figlio era ancora piccolo e con una mente più flessibile.

Sono cambiamenti enormi, di vite vere, che appaiono dal silenzio e giustificano da soli qualsiasi sforzo di ricerca e di progettazione.
