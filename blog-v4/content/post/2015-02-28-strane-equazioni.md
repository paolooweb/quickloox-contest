---
title: "Strane equazioni"
date: 2015-02-28
comments: true
tags: [iPhone, batteria, PowerBook100]
---
Ho cambiato la batteria di iPhone 5, che mostrava segni evidenti di cedimento. Gratis, perché il modello rientrava in un [programma di richiamo batterie](https://macintelligence.org/posts/2014-08-26-come-volevasi-sostituire/) (valido tre anni dall’acquisto, ultima chiamata gennaio 2016).<!--more-->

Un’ora per arrivare all’Apple Store, un’ora di attesa (pranzo, shopping, commissioni), un’ora per tornare.

Ci avrei messo meno di tre ore per cambiare la batteria da solo? Ne dubito. Il rischio di sbagliare qualcosa o di acquistare un componente non garantito, quanto incide nell’equazione?

Quando uscì il primo iPhone si levarono alte le critiche di chi considerava un problema la batteria non sostituibile dall’utilizzatore. La mia esperienza di tempo dedicato alla batteria è di tre ore in due anni. Ricordo quando i portatili avevano una autonomia ridotta e chiunque fosse avveduto acquistava una batteria di riserva. Portavo la seconda batteria ovunque potesse servire, avevo marcato diversamente le due batterie per distinguerle, la marcatura fu preziosa quando dimenticai il PowerBook 100 alla fermata dell’autobus. Lo trovò una signora che per canali romanzeschi arrivò fino a me. Mi presentai a casa sua e lei mi chiese, giustamente, come potesse essere sicura che quel portatile fosse veramente il mio. Mi ricordavo il marchio che avevo applicato alla batteria e solo io potevo ricordare, e fu la salvezza.

A parte i racconti di gioventù, alla batteria di riserva si dedicavano ben più di tre ore a biennio, soprattutto da parte di quanti cercavano la batteria miracolosa, che dura il doppio a metà del costo.

La batteria di iPhone è non sostituibile perché così si rosicchiano millimetri, milliwatt, milligrammi, per averlo sempre più sottile, che consumi sempre meno, che sia ancora più leggero.

Quando incide nell’equazione? Per capire se le mie tre ore sono vantaggiose rispetto al passato oppure no, dovrei poter quantificarle rispetto ai vantaggi della miniaturizzazione. Qualcuno dovrebbe scrivere una equazione esoterica capace di rapportare tempo contro grammi, secondi contro millimetri, volume contro consumo. Magari i progettisti di iPhone ce l’hanno, vai a sapere.