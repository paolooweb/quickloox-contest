---
title: "Ha vinto il quarterback"
date: 2023-02-13T00:59:33+01:00
draft: false
toc: false
comments: true
categories: [Internet]
tags: [Super Bowl, Super Bowl LVII, Philadelphia Eagles, Kansas City Chiefs, Rihanna, Sanremo, Glendale, Jalen Hurts, Patrick Mahomes]
---
Il presupposto della partita era *un quarterback eccezionale contro una difesa eccezionale*. In realtà la difesa eccezionale, ricevuta la palla con il calcio di inizio, l’ha depositata in meta nel giro di cinque minuti di gioco, con un *drive* praticamente perfetto, quasi intimidatorio.

Quattro minuti di gioco dopo, il quarterback eccezionale aveva già restituito il favore. Questo dice molto della partita, considerato che molti Super Bowl iniziano contratti, con le squadre che si studiano, lottano a metà campo, perdono e guadagnano possessi, esprimono nervosismo eccetera. Qui invece è iniziata una partita che, se per assurdo fosse proseguita nello stesso modo, avrebbe totalizzato una novantina di punti segnati. L’equilibrio di partenza tra le formazioni suggerisce che poteva succedere: stesso record di vittorie e sconfitte, posizioni equivalenti nella griglia dei playoff, numerose statistiche identiche o simili.

Naturalmente non poteva proseguire in questo modo e le cose si sono fatte più interessanti, con meno segnature e più confronto a viso aperto tra i due team nel tentativo di prendere in mano la partita dopo il botta e risposta iniziale.

Nel secondo periodo si sono viste inizialmente le stesse dinamiche del primo. Solo che la difesa eccezionale ha segnato dopo otto secondi e al quarterback eccezionale sono serviti otto minuti per pareggiare (grazie a un *fumble*). L’equilibrio si è rotto quando la difesa eccezionale ha messo a segno una meta e un calcio, per chiudere la prima metà della partita 21-14. Per certi Super Bowl è stato un punteggio finale e anche lusinghiero.

La scenografia aerea dello *half-time show* ancora una volta ha confermato che gli americani possono sbagliare, ma provano sempre qualcosa, a differenza di dove ci si vota all’imbalsamazione e mai si lascia la strada vecchia per la nuova. Per gli appassionati di sociologia: diversamente da Sanremo, a Glendale non sono stati lanciati messaggi pensosi o profondi; in compenso Rihanna è apparsa convincentemente incinta del secondo figlio. Dall’apice dell’intrattenimento italiano arrivano posizioni diametralmente opposte, né giuste né sbagliate; opposte. Come questo si traduca concretamente nella comunità, o meno, può essere valutato da ognuno.

Nel terzo periodo, una svolta; la difesa eccezionale ha realizzato due calci, contro una meta del grande quarterback. Decisione, quella di calciare, discutibile, poiché nell’ultimo quarto il quarterback ha quasi subito piazzato una meta che gli ha consentito di passare in testa di un punto, a dodici minuti dalla fine.

Lì si è rotto qualcosa nel meccanismo della partita. La difesa eccezionale, che aveva dominato nel possesso di palla grazie anche al sangue freddo nel giocare con successo terzi e quarti *down* complicati, ha inesplicabilmente scelto il *punt* quando sarebbe stato vitale giocarsi tutto. Hanno ridato la palla a quelli del grande quarterback, che ne hanno approfittato per portarsi con una lunga corsa fino a ridosso della linea di meta. Che è arrivata poco dopo, 35-27, otto punti di scarto, nel football sono un vantaggio tattico importante che mette pressione su chi è sotto.

Se sei degno del Super Bowl, messo alle strette fai qualcosa di speciale. La difesa eccezionale c’è riuscita, segnando meta più trasformazione da due punti: 35 pari.

La palla però torna al quarterback, che confeziona l’affondo finale. Usa i minuti rimanenti per arrivare a una yard dalla linea di meta, praticamente allo scadere.

Un calcio facile e la partita ha un vincitore.

Meritato.

La difesa eccezionale ha protetto molto bene il suo quarterback, ha giocato per il possesso di palla allo scopo di lasciare fuori dal campo più a lungo che poteva il quarterback eccezionale, è stata letale ogni volta che mancava una yard o due, ha avuto il coraggio di rischiare. Ha perso quando ha deciso di non prendersi un rischio (e si è presa una meta in faccia).

Il quarterback eccezionale e la sua squadra hanno sempre tenuto il passo senza mollare, sono rimasti calmi e freddi e, quando hanno potuto dispiegare il loro attacco, hanno sempre portato a casa bottino importante. Hanno vinto perché hanno alzato la qualità del gioco man mano che si avvicinava la fine della partite, cosa per niente scontata al termine di una lunga stagione.

Menzione speciale per lo spot di un apparecchietto da appiccicare alla spalla per la somministrazione periodica di insulina nei casi di diabete.

Tecnologia, non pervenuta. Uno spot qualsiasi di Google per i Pixel, le tavolette in panchina sono notoriamente distribuite dal Male, alla fine la visibilità maggiore l’ha avuta Apple. Apple Music però.

Uno dei Super Bowl più vivaci e avvincenti degli ultimi anni, con la giocata decisiva a undici secondi dalla fine. 38-35. Per una volta ho preferito la cronaca al bozzetto, in considerazione del punteggio alto e sempre serrato.