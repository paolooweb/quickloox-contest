---
title: "Festa e basta"
date: 2015-12-25
comments: true
tags: [widget, Dashboard, Festive, Lights]
---
Un Natale retró su Mac con il *widget* (il *widget*!) Festive Lights, ancora presente sulla vecchia [pagina Apple dedicata a Dashboard](https://www.apple.com/downloads/dashboard/justforfun/festivelights.html) e però non aggiornato: la versione più recente (funziona, collaudata riaccendendo Dashboard apposta per l’occasione, fa tenerezza ed è carinissima su un grande schermo) si trova sul [sito del programmatore](http://www.interdimensionmedia.com/widgets/?widget=lights). Chi si vuole divertire troverà anche una pletora di varianti da installare a caccia delle lucine festive preferite.<!--more-->

Un Natale da passare in compagnia su iPad con uno dei [ventidue migliori giochi da tavolo](http://www.macworld.co.uk/feature/iosapps/22-best-ipad-board-games-christmas-2015-3417561/) secondo Macworld UK (sceglierei [Carcassonne](http://carcassonneapp.com) o [Lords of Waterdeep](https://itunes.apple.com/it/app/d-d-lords-of-waterdeep/id648019675?mt=8)).

Un Natale irripetibile per quanto sarà bello, sereno, lieto. Fino al prossimo, che lo sarà ancora di più. Se mi è permesso, il mio auspicio è questo, per tutti e per ciascuno tra quanti passano di qui. Auguri, festa, grazie per esserci.