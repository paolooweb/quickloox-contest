---
title: "Letture arretrate"
date: 2016-07-14
comments: true
tags: [Montecristo, Minigrooves, Mori, Volpi, Kellerman]
---
Letture arretrate e soprattutto consigliate, perché non le ho ancora terminate ma posso già dire che ne stravalgono la pena.

La prima è [Minigrooves](https://itunes.apple.com/it/book/minigrooves/id1133617740?l=en&mt=11) di [Riccardo Mori](http://morrick.me). Sono ancora nel mezzo del primo volume di racconti, solo colpa mia però; Riccardo scrive bene e l’insieme sa di tabacco fresco, conservato con la buccia di arancia. È in inglese, un bell’inglese; come pretesto estivo per imparare la lingua, è p-e-r-f-e-t-t-o.

La seconda è [The Montecristo Project](https://itunes.apple.com/it/book/the-montecristo-project/id1092073124?l=en&mt=11) di [Edoardo Volpi Kellermann](http://www.evkmusic.it/ltr/). Fantascienza dei vecchi tempi – quelli validi – con un respiro inusitato per un autore italiano e una realizzazione in ebook con tutta una navigazione attorno al tema centrale, appendici e una enciclopedia (vera!) che rendono il tutto molto, molto immersivo. Edo è un tolkieniano di ferro e ha capito un sacco di cose dal *Signore degli Anelli* su come si scrive qualcosa di grande. Le si ritroveranno nella sorprendente Italia che ha immaginato, “qualche” anno più avanti dell’attuale, dove certe cose restano le stesse. E l’idea che sta sotto a tutto è sì fanta, ma anche ben scientifica, come appunto ai vecchi tempi. Sotto l’ombrellone, ebook obbligatorio, o si perde un sacco di contorno saporito e suggestivo.

La terza è [Writing on the iPad: Text Automation With Editorial](https://itunes.apple.com/it/book/writing-on-ipad-text-automation/id697865620?l=en&mt=11) di [Federico Viticci](https://twitter.com/viticci). Mi trovo sempre più spesso a scrivere su iPad, magari con tastiera esterna, e da tempo ho adottato [Editorial](http://omz-software.com/editorial/). Ottimo editor, che diventa eccellente se gli si costruisce attorno. Questo iBook su misura per iPad spiega come e anche perché. Per compiere il prossimo salto di qualità.

Prima o poi finirò tutto, ma non aspettatemi e battetemi sul tempo.

*[Consiglio anche [Cuore di Mela](http://cuoredimela.accomazzi.it), che al contrario delle opere succitate attende luce verde da parte dei lettori con [voglia di scommettere su un progetto](https://www.kickstarter.com/projects/295108589/cuore-di-mela-i-segreti-di-apple-iphone-e-mac).]*