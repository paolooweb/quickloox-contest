---
title: "La regola dei terzi"
date: 2021-01-31T01:52:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [Mac mini, M1, John Gruber, Daring Fireball, Apple Silicon] 
---
La versione di lusso del mio Mac mini 2018, con processore i7, da nullafacente consuma 19,9 watt; a piena potenza, arriva a 122. Il calore emesso è di 68 [British Thermal Unit](https://en.wikipedia.org/wiki/British_thermal_unit) orarie (Btu/h) a riposo, 417 Btu/h sotto stress.

Lo dice una [pagina di supporto Apple dedicata al consumo energetico di Mac mini](https://support.apple.com/en-us/HT201897).

Un Mac mini M1 del 2020 consuma, da nullafacente, 6,8 watt: *un terzo*. A piena potenza, 39 watt. Sempre *un terzo*. Il calore emesso a riposo ammonta a 23,2 Btu/h; immagina quanto potrebbe essere, sì, è *un terzo*. Al massimo arriva a 133 Btu/h. *Meno di un terzo*.

Sembrerebbe, il nuovo Mac mini, la versione risparmiosa di chi pensa più alla bolletta che alle prestazioni. Non fosse che, oltre a consumare un terzo dell’energia, va (molto arrotondato) *un terzo più veloce*.

Macchine più frugali ne abbiamo; macchine più potenti ne abbiamo; macchine che sono contemporaneamente più frugali e più potenti, beh, dobbiamo ancora renderci pienamente conto di come M1 rovesci il tavolo di gioco e cambi bruscamente le regole.

John Gruber su *Daring Fireball* mostra una [versione più leggibile della tabella dei consumi](https://daringfireball.net/2021/01/mac_mini_power_consumption), che comprende più modelli. C’è da stropicciarsi gli occhi.

Interessante è il commento di Gruber, secondo il quale la cronologia degli ultimi modelli di Mac mini, in breve, racconta la storia di come i processori Intel siano arrivati a sviluppare troppo calore per gli standard di Apple, nel tentativo di generare prestazioni all’altezza delle aspettative e di come Apple si sia resa conto di avere bisogno di una generazione extra di Mac da scrivania prima di poter contare su Apple Silicon.

Il che spiegherebbe la lunga attesa di un Mac mini tra 2014 e 2018; Intel non riusciva a tirare fuori processori adeguati e Apple contava su Apple Silicon, che però non era ancora pronto. Per cui, diciamo nel 2017, ha deciso di fare uscire comunque un nuovo Mac mini, seppure ancora Intel.

Il mio Mac mini è stato un riempitivo nella linea di prodotto. E poi uno si chiede perché affrontare una transizione di architettura così impegnativa.

*Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile.*