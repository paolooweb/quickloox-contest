---
title: "Lo spazio per aggiornare"
date: 2023-10-25T00:15:45+01:00
draft: false
toc: false
comments: true
categories: [Software, Harware]
tags: [Voyager, Nasa]
---
Nessun retrocomputing supera in ingegno e impegno quanto fa la NASA per [tenere in vita il più a lungo possibile l’attività scientifica a bordo delle sonde Voyager](https://arstechnica.com/space/2023/10/nasa-wants-the-voyagers-to-age-gracefully-so-its-time-for-a-software-patch/).

Il problema si può rappresentare come due computer con quarantasei anni di vita alle spalle e possibilità ridotte di controllo dello hardware, trovandosi essi a oltre venti miliardi di chilometri dalla Terra.

Bisogna contenere il consumo energetico al minimo perché il plutonio a bordo ha una durata finita. Gli scienziati si aspettano di poter ottenere risultati dalle sonde fino al 2030 e forse anche più in là; tuttavia arriverà un momento in cui i trasmettitori non avranno più a disposizione i duecento watt necessari per funzionare e non potranno più inviare dati a terra.

Nel frattempo, su Voyager 1 sono attivi ancora quattro strumenti scientifici (cinque su Voyager 2) e per i ricercatori ogni giorno di funzionamento è un grande regalo. Per assicurare la massima durata della missione è stato necessario nel tempo prendere decisioni anche drastiche e, per esempio, i dati acquisiti dalle sonde non hanno più, da anni, un backup.

C’è stato bisogno anche di inventarsi cose non previste da alcun manuale, come quella in corso. I condotti che portano carburante ai micropropulsori incaricati di orientare l’antenna trasmittente verso la Terra soffrono di aterosclerosi tecnologica: depositi interni di residui che ostacolano il flusso dell’idrazina.

Il team di supporto ha deciso di cambiare la modalità delle trasmissioni per minimizzare il problema e ciò richiederà un aggiornamento del software delle sonde.

Aggiornare un computer di quarantasei anni posto a ventiquattro miliardi di chilometri dal server è sicuramente una situazione interessante.