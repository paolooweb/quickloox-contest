---
title: "iPad noleggiasi"
date: 2020-08-18
comments: true
tags: [Epic, Apple, iOS, Google, Fortnite, Android, Thompson, Stratechery, Netflix]
---
La ragione per cui [Epic è entrata in guerra con Apple e Google](https://macintelligence.org/blog/2020/08/15/trenta-e-non-piu-trenta/) per smettere di pagare loro il trenta percento sui proventi di Fortnite è abbastanza semplice: i frequentatori del gioco da iOS o Android sono [il dodici percento del totale](https://www.businessofapps.com/data/fortnite-statistics/) e generano una quota marginale di fatturato. Per Epic è una fetta di mercato certo concreta, ma non critica.

A meno che Apple revochi l’account Developer a Epic, come [sembra possa succedere](https://twitter.com/EpicNewsroom/status/1295430127455596544) in assenza di novità il 28 agosto. Epic fornisce il motore per videogiochi Unreal Engine a vari sviluppatori indipendenti e una decisione simile rischia di portare conseguenze a catena.

<blockquote class="twitter-tweet"><p lang="en" dir="ltr">Apple removed Fortnite from the App Store and has informed Epic that on Friday, August 28 Apple will terminate all our developer accounts and cut Epic off from iOS and Mac development tools. We are asking the court to stop this retaliation. Details here: <a href="https://t.co/3br1EHmyd8">https://t.co/3br1EHmyd8</a></p>&mdash; Epic Games Newsroom (@EpicNewsroom) <a href="https://twitter.com/EpicNewsroom/status/1295430127455596544?ref_src=twsrc%5Etfw">August 17, 2020</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

Epic – che è ben felice, guarda tu a volte il caso, di [pagare il trenta percento a Sony, Nintendo e Microsoft](https://arstechnica.com/gaming/2020/08/as-epic-attacks-apple-and-google-it-ignores-the-same-problems-on-consoles/) per stare su Xbox, Switch e PlayStation – ha chiamato in causa il tribunale e vedremo come andrà a finire.

Il punto non è certo la messa in discussione degli store verticali; significherebbe più o meno che domani chiunque potrebbe entrare in un centro commerciale con un chiosco di gelati su ruote e mettersi a vendere gelati, senza che il centro commerciale possa buttarlo fuori; l’esito che invece abbiamo oggi. Più probabilmente questo è un test degli equilibri politico-commerciali esistenti oggi, che se fallisse porterebbe a equilibri diversi.

Apple potrebbe certamente rivedere parte della sua politica commerciale su App Store, per un motivo preciso: la regola del trenta percento come è ora va a danno dell’utente in varie situazioni e non c’entra il valore in sé, quel trenta percento che può essere discusso ma non è sbagliato a prescindere.

Probabilmente una rivisitazione delle regole diminuirà gli incassi di Apple ma sembra abbastanza inevitabile. Il 2008 è passato e i servizi online a pagamento sono diventati una norma, alla quale l’App Store attuale va stretto e non è difendibile.

Io sono per abolire le app gratis e mettere almeno a 0,99 le app che fanno da ponte a sistemi di pagamento terzi, come appunto Fortnite o Netflix. Apple sceglie come ora la sua percentuale, trenta o altro, per tutte le app ordinarie, ma nel caso di cui sopra si tiene tutti gli 0,99, per lasciare campo libero internamente alla app.

Ben Thompson su Stratechery va più semplice e propone di [liberalizzare gli acquisti da webwiew](https://stratechery.com/2020/apple-epic-and-the-app-store/), in pratica da browser interno alle app. Può essere.

Fortnite potrebbe tornare su App Store, per accordo oppure per ordine del giudice, o anche no. Nel secondo caso, noleggio volentieri a canone agevolato il mio iPad, dove l’ho scaricato e mai usato.