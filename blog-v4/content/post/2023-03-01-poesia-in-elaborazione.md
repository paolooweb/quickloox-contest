---
title: "Poesia in elaborazione"
date: 2023-03-01T02:12:23+01:00
draft: false
toc: false
comments: true
categories: [Software, Web]
tags: [Dr. Drang, Drang, pandas, baseball]
---
Ho letto sovente descrizioni del basket come *poesia in movimento* e, da cestista di lungo corso, devo dire che certe evoluzioni nell’NBA si avvicinano alla congiunzione tra artistico e atletico.

Il baseball mi dà invece sensazioni del tutto diverse. È una metafora della vita, di come siamo soli di fronte al mondo ma possiamo affrontarlo meglio se ci coordiniamo. Ci ho giocato solo due anni, ma posso dire che il baseball insegna a vincere la paura, concentrarsi, credere in sé e anche a mettersi anima e corpo al servizio degli altri. È un grande sport.

Vedere Dr. Drang [muoversi tra i dati del baseball](https://leancrew.com/all-this/2023/02/cleaning-and-graphing-baseball-data/) armato di Python e [pandas](https://pandas.pydata.org) concilia la poesia con la concentrazione. A suo modo, l’elaborazione numerica che compie è un gesto estetico oltre che produrre un risultato.