---
title: "Si sta meglio qui, su questa riva"
date: 2018-04-17
comments: true
tags: [Guareschi, Viticci, Apple]
---
[Lo scriveva Giovannino Guareschi](https://books.google.it/books?id=zFpTDwAAQBAJ&pg=PT43&lpg=PT43&dq=guareschi+%22si+sta+meglio+qui,+su+questa+riva%22&source=bl&ots=9IbKiLGazJ&sig=5X0F-rZbdcHUaFg5nApSPe2WFZo&hl=en&sa=X&ved=0ahUKEwjpwbvOxODaAhWCHJoKHd-xA0YQ6AEIOTAC#v=onepage&q=guareschi%20%22si%20sta%20meglio%20qui%2C%20su%20questa%20riva%22&f=false), ovviamente su tutt’altre altezze letterarie e, anzi, nella fattispecie poetiche.

Molti piani più in basso, mi accontento di parlare di come ogni tanto si possa legittimamente dubitare del proprio ecosistema: avrò scelto bene? Forse ho sottovalutato le alternative? Non avranno ragione quelli che dicono di spendere meno che tanto è uguale?

Soprattutto, ci sono quelli che dicono *non è più come una volta*. La qualità è diminuita, un mio amico si è lamentato, [dicono che ha i difetti di produzione](https://macintelligence.org/posts/2018-04-14-uno-su-dieci-non-ce-la-fa/), [non ascoltano i professionisti](https://macintelligence.org/posts/2018-04-11-2019-e-non-sentirli/), mancal’innovazioneperchénonc’èpiùSteve. Farò bene a scegliere Apple ancora oggi? Non è che sto perdendo un giro o i tempi sono cambiati?

Passo la parola a uno che se ne intende anche in quanto sempre superbamente aggiornato all’ultimo modello di tutto: [Federico Viticci su MacStories](https://www.macstories.net/stories/apple-ecosystem-comfort/).

>Dalla fine dello scorso anno, comunque, sono testimone di un cambiamento progressivo che mi ha fatto capire come la mia relazione con lo hardware e il software di Apple sia cambiata. Mi sono progressivamente addentrato nell’ecosistema Apple e non percepisco più come inferiori vari aspetti di servizio.

Chi segue Viticci sa che non ci va leggero con l’aggiornamento tecnologico, né con i test di prodotto. Sa anche come sia tutt’altro che un allineato su Apple, specialmente in software e accessori.

Chi non lo segue, lo faccia stavolta e mi dica se la sua è una posizione equilibrata oppure no.