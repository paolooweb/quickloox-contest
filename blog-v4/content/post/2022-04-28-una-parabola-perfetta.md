---
title: "Una parabola perfetta"
date: 2022-04-28T00:47:43+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [UI Browser, AppleScript, plaintextsports, plaintextsports.com, Cheeseman, Bill Cheeseman, Gruber, John Gruber, Daring Fireball, UI scripting, GUI Scripting, Accessibility, macOS, Monterey, Stephen Curry]
---
Non sono a parlare di basket, se non per una informazione di contorno: lavorare con aperto [plaintextsports.com](https://plaintextsports.com/#nba) per tenere sott’occhio l’andamento dei playoff in modo non distraente crea dipendenza.

Invece: [UI Browser cessa di vivere il prossimo 17 ottobre 2022](https://pfiddlesoft.com/uibrowser/), quando non sarà più scaricabile né acquistabile e non verrà più aggiornato. Commenta Bill Cheeseman, l’autore:

>Ho lavorato con amore per quasi vent’anni a UI Browser, come suo unico sviluppatore. Ora che ho compiuto settantanove anni, è tempo di portare a conclusione questo bel lavoro.

Credo che sia una delle app più sottovalutate nella storia di Macintosh, inutile se non si lavora per l’accessibilità oppure se non si considera AppleScript. Ancora una volta, la parola all’autore:

>Per chi sviluppa una app per macOS, UI Browser aiuta a progettare le funzioni di accessibilità e testare la loro conformità ai requisiti di Apple.

>Per chi sviluppa script AppleScript allo scopo di controllare una app macOS esistente, UI Browser aiuta a controllare l’interfaccia utente, anche se la app controllata non supporta AppleScript. UI Browser comprende la disposizione degli elementi di interfaccia utente di una applicazione e ne conosce i nomi AppleScript nonché i numeri di indice. Permette di navigare la gerarchia di Accessibility e genera istruzioni significative di GUI Scripting con un singolo clic.

È davvero difficile sovrastimare l’importanza di questo programma per chi abbia capito quanto sopra. Un programma senza concorrenza, che fa esattamente ciò che annuncia e lo fa molto bene, che giustifica la sua esistenza totalmente, anzi, di cui salvo imprevisti si sentirà la mancanza. La parola, stavolta, a John Gruber su [Daring Fireball](https://daringfireball.net/linked/2022/04/27/cheeseman-ui-browser):

>So che, parlando da esperto, mettersi a spendere i soldi di Apple è facile; nondimeno, UI Browser sembra uno strumento che Apple avrebbe dovuto acquistare da tempo. È come se Apple avesse solo realizzato l’infrastruttura per rendere possibile lo scripting dell’interfaccia utente e Bill Cheeseman abbia svolto il lavoro di renderla utilizzabile. Se ha senso tenere lo scripting dell’interfaccia utente in macOS, ha senso che Apple compri UI Browser. Non è solo utile; è essenziale.

UI Browser funziona a dovere sui sistemi fino a Monterey incluso e, con un po’ di fortuna, potrebbe finire la sua vita in modo da funzionare come si deve anche sulla prossima edizione di macOS. Ha un solo bug importante, noto e aggirabile, dichiarato a chiare lettere sulla pagina del programma.

Costa una fucilata, secondo gli attuali standard del software, eppure è insostituibile nella sua funzione. Costa esattamente quello che vale.

Può accadere tristemente che un programma perda il suo supporto a causa della cattiva salute, o peggio, di chi lo sviluppa. Le condizioni di Bill Cheeseman non sono pubbliche, però ha annunciato la dismissione di UI Browser il 17 aprile per il 17 ottobre, esattamente sei mesi dopo. Sembra l’annuncio di una persona sana, che programma serenamente la propria pensione senza pressioni o cause di forza maggiore, con tutta l’intenzione di essere lui a spegnere i server a ottobre.

UI Browser termina – salvo imprevisti – il proprio ciclo di vita. Ma è un lieto fine, all’epilogo di una avventura straordinaria e importante, con un un numero indefinito e ingente di persone tra sviluppatori e utenti che possono dire a Cheeseman solo un grande grazie.

Un lieto fine, una parabola perfetta come quelle fuori dal mondo di [Stephen Curry](https://www.nba.com/player/201939/stephen-curry), in campo tra poco più di due ore.