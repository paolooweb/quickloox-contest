---
title: "Tristezze e meraviglie"
date: 2014-01-16
comments: true
tags: [app, Ferrara, Lione, Tgv, Trenitalia]
---
Sono stato da poco a Ferrara, città senza una *app* ufficiale. Sono andato in macchina, ma se fosse stato il treno non avrei potuto prenotare tramite una *app* ufficiale. Sono stato in un palazzo antico privo di una *app* ufficiale a vedere una mostra che non aveva una *app* ufficiale.<!--more-->

Mi accingo ad andare a Lione, città con una *app* ufficiale, con biglietto ferroviario acquistato tramite una *app* ufficiale. Per motivi turistici mi fermerò a mezza strada e completerò il viaggio con un altro treno, facente capo a una diversa amministrazione, dotata di una *app* ufficiale. Ho già scaricato la *app* ufficiale del museo e la *app* ufficiale della mostra. Ricordarsi di portare gli auricolari, perché la *app* della mostra contiene l’audioguida già pronta.

Meraviglie a Ferrara e mi auguro anche a Lione. Senza le *app* è un po’ più triste, però.