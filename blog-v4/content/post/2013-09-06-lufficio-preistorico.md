---
title: "L'ufficio preistorico"
date: 2013-09-06
comments: true
tags: [iPhone]
---
Nel prossimo numero di [Macworld Italia](http://www.macworld.it) ci sarà una recensione di Office Mobile di Microsoft.<!--more-->

Funziona solo previo abbonamento da 99 euro l’anno, se va bene. Keynote, Pages e Numbers messi insieme costano meno di 27 euro.

È solo per iPhone. iWork funziona anche su iPad.

Quando si usa Excel, la tastiera non si adegua e, avendo numeri da inserire, occorre sempre fare due tocchi o una strisciata. Numbers cambia la tastiera in accordo a quello che succede. Dal 2010.

PowerPoint non può creare nuovi documenti e può solo modificare quelli esistenti. Keynote su iPhone è un’applicazione completa.

Un dettaglio è peggio di tutti gli altri messi insieme.

*I file vanno salvati manualmente.*

Non ci posso credere. Ho visto centinaia di programmi su iOS e credo che questo sia il secondo o il terzo caso.

Nella terra dei mammiferi è sbarcato uno degli ultimi dinosauri.