---
title: "Salute e benessere"
date: 2021-04-03T01:32:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [watch] 
---
Una persona molto anziana che vive sola, autosufficiente ma con qualche problema di deambulazione. Se cade, può essere un problema.

Se cade e non riesce più a raggiungere un terminale di comunicazione, è un grosso problema. Risolvibile con uno di quegli apparecchietti da portare al collo, con un pulsante programmato per segnalare un problema alla centrale di ascolto.

Se cade e perde conoscenza, è un problema che l’apparecchietto non risolve.

Se è già caduta una volta? La risposta del medico di base, per telefono, è *ricoverare*. Se avesse detto chiaramente *tanto vale che muoia in ospedale* suonerebbe almeno più sincera. Certamente il sottotesto suona come *ho altre cose cui pensare*.

Credo che in questo momento [watch](https://www.apple.com/it/watch/) sia l’unica soluzione buona anche per lo scenario peggiore, visto che a partire dal modello SE contiene la funzione di riconoscimento della caduta, con attivazione automatica della chiamata di emergenza se il portatore non la esclude entro trenta secondi.

Noto come la struttura adibita alla salute pensi al benessere (proprio). E come l’azienda tecnologica sinonimo di benessere, pensi alla salute.

*Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile.*