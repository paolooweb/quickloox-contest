---
title: "Fiction e realtà"
date: 2015-02-19
comments: true
tags: [fiction, Abc, ModernFamily, iPhone, iPad, Sundance, FilmicPro, OSX, Mac, Android]
---
Leggo di come [uno dei migliori film al festival Sundance sia stato girato usando un iPhone 5S](http://www.theverge.com/2015/1/28/7925023/sundance-film-festival-2015-tangerine-iphone-5s), lenti anamorfiche per l’iPhone in questione e una [*app* da 7,99 euro](https://itunes.apple.com/it/app/filmic-pro/id436577167?l=en&mt=8).<!--more-->

Poi leggo che [Modern Family di Abc ha girato un intero episodio usando solo iPhone e iPad](http://recode.net/2015/02/17/abcs-modern-family-shot-an-entire-episode-only-using-apples-iphone-and-ipad/).

([Modern Family](http://abc.go.com/shows/modern-family) è una *family comedy* della rete televisiva americana Abc e l’episodio, in onda il 25 febbraio, si svolge interamente dentro lo schermo di un Mac. Ciò non toglie che le riprese con iPhone e iPad debbano avere qualità e credibilità: OS X, per dire, è stato interamente ricreato a mano per esigenze di montaggio).

Infine sento uno dire *Dimmi che cosa si può fare con iOS che non si possa fare con Android*.

Per esempio girare *fiction* per il cinema e la televisione. Lavoro vero e impegnativo. iPhone è all’altezza, gli altri dobbiamo ancora vederli alla prova. La realtà è questa.