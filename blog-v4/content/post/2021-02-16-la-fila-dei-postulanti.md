---
title: "La fila dei postulanti"
date: 2021-02-16T03:44:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [iOS 14.5] 
---
Cresce l’attesa per iOS 14.5, la versione di sistema operativo per iPhone che farà chiedere alle app il permesso di tracciare arbitrariamente le persone nella loro navigazione in giro per Internet.

Sembra una cosa normale; ci possono intercettare le comunicazioni oppure tenere d’occhio quando andiamo per strada, ma lo possono fare solo soggetti preposti (come le forze dell’ordine) e che abbiano ricevuto una autorizzazione specifica. Almeno in teoria; lo stato ha dimostrato una scarsa capacità di proteggere le persone.

Come per la legge sulla privacy, che ha riempito il web di dichiarazioni di consenso totalmente inutili.

*Come, inutili? Sta al cittadino occuparsi attivamente della tutela della propria privacy. C’è una legge apposta.*

Col cavolo.

È apparso un articolo interessante intitolato [Che permesso diamo veramente quando accettiamo tutti i cookie](http://www.conradakunga.com/blog/what-do-you-actually-agree-to-when-you-accept-all-cookies/) che ha preso per esempio il sito di Reuters in Europa. Mica bische clandestine o vendita di creme per la crescita di parti del corpo a scelta; [Reuters](https://www.reuters.com/news/archive/europe).

Se diamo l’Ok alla profilazione prima di entrare, accordiamo a Reuters il permesso di passare i nostri dati di navigazione in giro per Internet (non solo sul sito Reuters!) a SEICENTOQUARANTASETTE agenzie che operano nel campo della pubblicità su Internet.

(Chiedo scusa per il maiuscolo ma, diamine, *seicentoquarantasette*).

Ciascuna di queste aziende ha la propria policy di tutela della privacy e trattamento dei dati.

Il cittadino coscienzioso che ha a cuore la propria riservatezza non ha che leggersi seicentoquarantasette diverse policy e decidere che cosa approvare e che cosa no.

Non è libertà, questa. È pura finzione scenica. La legge che ci tutelerebbe popolando il web di richieste di consenso è fatta semplicemente per poter dire di avere la coscienza a posto dopo ci che, nel rispetto di facciata dei contribuenti, sono caz… sono fatti loro.

È una situazione chiaramente oltre il livello della ragionevolezza.

iOS 14.5 porta niente più che una sana boccata di ossigeno in un ecosistema che, da questo punto di vista, è evidentemente tossico.

Ma porta anche potenzialmente una riduzione di fatturato per chi prospera attraverso questi sistemi. Non stiamo parlando di una normale, rispettosa, ragionevole profilazione di consumatori consenzienti, bensì di tracciamento indiscriminato, ubiquo e surrettizio.

Questi soggetti, Facebook primo fra tutti, hanno iniziato a fare campagna contro l’iniziativa di Apple. Che si è tolta dal mucchio; qualsiasi altra grande azienda tecnologica ha scelto di tacere e cercare di conservare lo status quo. Girano soldi, fa niente se a spese delle persone pedinate su Internet indipendentemente dalla loro volontà.

Ne escono iniziative come un [articolo](https://www.matthewball.vc/all/applemetaverse) [ripreso malamente da Il Post](https://www.ilpost.it/2021/02/14/controllo-apple-internet/), che si picca di fornire una informazione al servizio dei lettori e qui invece si comporta come il peggio dei siti acchiappaclic. Apple (che ha i suoi problemi e difetti, come tutti) viene dipinta come l’azienda *che controlla Internet* (più, nell’originale, *il metaverso*. Seriamente) e, a cascata, più o meno una colpevole di tutto quello che non va nel mondo tecnologico. Senza un controesempio, un confronto, una verifica.

*Follow the money*. iOS 14.5 farà un cattivo favore a chi si comporta in modo non trasparente e queste forze, come è ovvio, difendono la propria posizione, con ogni mezzo.

D’altronde, se le persone preferiranno essere tracciate a tappeto, potranno dare il permesso a tutto. Oppure potranno abbandonare iOS e andare su Android. Il punto è che lo faranno *in modo consapevole e informato*.

La differenza non è più tra monopolisti e pluralisti o aperti e chiusi; è tra chi informa l’utilizzatore e chi lo disinforma.

*Il Post*, che pure ha una condotta specchiata in tema privacy, in questo caso ha disinformato di brutto.

Dietro la richiesta di consenso di Reuters c’è una fila di seicentoquarantasette postulanti in cerca dei tuoi dati senza dirtelo.

iOS 14.5 non elimina questo problema, ma lo semplifica in maniera radicale. Chi apre una app verrà informato di quello che sta per succedere e potrà dire OK oppure no, grazie. La normalità più normale.

Io scelgo la normalità.

*Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile.*