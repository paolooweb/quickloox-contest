---
title: "Niente accade per caso"
date: 2023-05-08T01:58:10+01:00
draft: false
toc: false
comments: true
categories: [Software, Internet]
tags: [Terra2, intelligenza artificiale, Wordle]
---
Siamo [rimasti ieri](https://macintelligence.org/posts/2023-05-08-vabengono/) sull’idea di Terra2 che all’intelligenza, artificiale e no, farebbe bene disporre di routine capaci di ripescare nel bagaglio delle esperienze e generare a caso idee nuove da innestare in qualche processo creativo o comunque di pensiero.

Concordo, anzi, senza seleziona casuale la nostra intelligenza varrebbe ben poco. La domanda è se i meccanismi casuali interni dei sistemi generativi odierni sia qualcosa di simile a quella del cervello umano oppure se sia diversa.

Ho chiuso chiamando in causa [Wordle](https://macintelligence.org/posts/2023-01-15-presunto-colpevole/) e invitando a pensare come si muove il nostro cervello al momento di cercare di risolvere uno schema. In Wordle bisogna indovinare una parola di cinque lettere; a ogni tentativo, il gioco mostra che lettere sono al posto giusto e quali sono presenti nella parola da scoprire, ma messe nel posto sbagliato. Il principio è quello di [Mastermind](https://www.studiogiochi.com/enigmistica/giochi-classici/mastermind/) e altri giochi del genere.

Come funziona la mia mente?

Premessa: non siamo qui a discutere un gioco ottimizzato di Wordle. In teoria lo sottponiamo a una persona che conosce lo scopo del gioco ma non ha ancora messo a punto teorie né imparato a memoria tabelle di frequenza di accoppiamenti di lettere o altro. Semplicemente, pensa a che cosa fare.

Per prima cosa, scelgo la prima parola-tentativo e vabbeh. Ricevo il feedback: qualcosa è giusto, qualcosa va spostato, qualcosa è sbagliato. Riprovo con una nuova parola che contenga le posizioni sicure, sposti quelle da spostare e si completi con lettere diverse da quelle sbagliate di prima.

(Un giocatore esperto lascerebbe da parte le lettere giuste e magari anche quelle da spostare, cercando invece indizi con una seconda parola completamente nuova. Ma qui non andiamo di strategia, solo di pensiero).

Ottengo un nuovo feedback, sperabilmente migliore del primo, e avanti così.

La parte interessante è come penso alla seconda parola. Se non ne butto  una totalmente diversa ma costruisco sui dati sicuri, devo riempire gli spazi occupati dalle lettere sbagliate, con altre lettere.

Certamente nella mia testa non scorre un dizionario di parole da cinque lettere, con un filtro che elimina quelle non adatte. Guardo allo schema delle posizioni giuste e la mente lavora per riempire gli spazi vuoti a un livello inferiore a quello delle parole; in qualche modo dalla mente saltano fuori possibili candidati che spesso non vanno bene per una lettera, oppure parole molto ovvie che però contengono anch’esse lettere sbagliate e così via. Dopo qualche iterazione di questo schema, ho in mente una parola che soddisfa tutti i requisiti per essere inserita come nuovo tentativo.

Qui ho pensato: facciamo giocare a Wordle il sistema generativo di mia preferenza e vediamo che succede.

Ho spiegato al sistema, apparentemente con successo, che gli avrei fornito il feedback e lui avrebbe dovuto inventare nuove parole per andare avanti. Tutto ok.

La prima parola che ha voluto provare è stata *apple*. Non ottimale, ma giusto così. Non ho spiegato strategie e mettere una doppia è perfettamente legittimo.

Ho informato il sistema che la L in quarta posizione era corretta. Seguendo la mia strategia umana e ingenua di cui sopra, gli ho chiesto una parola con L nella quarta posizione.

La risposta è stata che avrebbe considerato parole con la L in quarta *o in seconda* posizione. Perché in seconda? Boh, magari è una qualche strategia. Vediamo. Le parole suggerite sono state *blaze*, *cliff*, *glaze*, *smile* e *truly*. Ho scelto a caso *smile* e gli ho detto che, ancora una volta, avevamo al posto giusto la L.

Il sistema ha insistito sul discorso *seconda o quarta posizione* e ha proposto altre cinque parole:

Flute  
Flare  
Clasp  
Babel  
Gloom

Si noti che *babel* non rientra nei requisiti. Ho considerato *flute* e ho fornito feedback: *abbiamo una F in prima posizione e una L in posizione scorretta. Quindi dobbiamo cercare una parola con F in prima posizione e L in quarta posizione*. Qui ho anche aiutato il sistema, combinando feedback da due righe diverse e per instradarlo rapidamente in una buona direzione. La sua proposta è stata la seguente:

Fable  
Flock  
Flaky  
Fleet  
Fable  

Non solo tre parole non corrispondono; ma c’è una parola ripetuta… che il sistema non si è reso conto di ripetere. Questo è molto, molto diverso dal funzionamento di un cervello umano. In ogni caso ho considerato *fable* e gli ho detto che avevamo la F giusta in prima posizione e la L giusta in quarta posizione. Le nuove parole in risposta:

Fable (you already got this one, but it's worth repeating since it fits perfectly)  
Flail  
Flute (you already got this one, but it's also worth repeating since it fits perfectly)  
Fleet (you already got this one too, but it also fits perfectly)  
Flaky

Questo output mi ha fatto vacillare. Effettivamente, da un punto di vista di ragionamento, non ho chiesto al sistema in modo esplicito una parola *nuova* e lui ha pensato di usarne una che soddisfaceva i criteri, pazienza se già usata! Poi però ha aggiunto altre parole non conformi al requisito.

Ho spiegato che ripetere una parola già usata era sbagliato e che quasi tutte le sue proposte avevano la L fuori posto. Ecco la risposta:

Fling  
Flirt  
Flair  
Flint  
Flick

Neanche una parola con la L in quarta posizione. L’ho fatto notare e ho indicato che occorreva una parola con la L al quarto posto. Niente da fare.

Dopo questo, ho fatto proporre al sistema nuove parole altre otto volte. Sono arrivate quaranta parola quindi, di cui trentanove senza la L in quarta posizione; ha fatto eccezione *fable* (già usata e che il sistema sapeva essere sbagliato usare). Ho cambiato la costruzione della frase, provato modi diversi di descrivere i requisiti, ma nulla.

Ho abbandonato il tentativo quando il sistema ha proposto *fablet*. Che ha sei lettere e non può essere usato per giocare a Wordle.

Il sistema, nel suo scegliere a caso, proseguiva verso il niente.

Il nostro cervello non funziona proprio in questo modo, quando sceglie lettere o parole a caso in presenza di vincoli.

La prestazione del sistema si spiega in due modi. Il primo, già noto, è che dà una ottima illusione di capire il testo; in realtà, non fa che cucire una parola dietro l’altra sulla base di probabilità statistica. Se gli dico *L in quarta posizione* o *y va dhnegn cbfvmvbar*, è lo stesso: lui fa dei conti dentro il suo modello. *dhnegn* ha tanto significato quanto *quarta*.

La seconda questione è che, per avere un output apparentemente umano, il sistema non sceglie *sempre* la parola con probabilità più elevata di apparire e, a volte, sceglie termini meno probabili. Questa tecnica provoca una regressione verso la media; la prestazione del sistema si degrada tanto più a lungo dura l’interazione con l’operatore. Se avessi proseguito, sarebbe andata sempre peggio.

Anche in questo caso, il nostro cervello funziona in un altro modo.

L’intelligenza parte dalla percezione e, con i frutti della percezione, produce rappresentazioni. Questo sistema non produce alcuna rappresentazione; effettua solo calcoli statistici. Qualcosa che serve anche al nostro cervello, ci mancherebbe; ma a tutt’altro livello. Chiunque di noi potrebbe scrivere un programmino che costruisce parole lettera dopo lettera e, poniamo, dopo una Q stabilisce che nel novantanove percento dei casi si estragga una U. È un bel divertimento, che come meccanismo coinvolto nella formazione dell’intelligenza è davvero minuscolo.