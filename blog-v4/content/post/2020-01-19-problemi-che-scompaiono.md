---
title: "Problemi che scompaiono"
date: 2020-01-19
comments: true
tags: [NetNewsWire, Simmons, Feedly, Unread, Vienna]
---
Rss è una delle mie necessità quotidiane; eppure i lettori di feed Rss non lo sono.

Meglio, non lo sono più. Se prima consideravo le alternative a disposizione, ora non trovo motivi validi per pensare a passare a un altro lettore.

Prima consideravo un valore la sincronizzazione tra apparecchi, gli stessi feed su Mac, iPad, iPhone (e, prima delle figlie, anche su *più* iPad). Adesso non mi interessa; passo da un apparecchio all’altro con frequenza e certo non perdo una pagina che mi interessi davvero, anche se il suo feed non è ubiquo.

C’è di più: la grandissima parte delle notizie è fortemente ridondata e, per riceverla tra i miei feed, è sufficiente che almeno uno di essi la pubblichi. La probabilità è vicinissima al cento percento.

Il colpo di grazia al disinteresse per i lettori è un altro ancora: se apro una pagina web su iOS, basta un attimo per aprirla su Mac e viceversa. Il valore aggiunto di avere la sincronizzazione completa è bassissimo, visto che mi basta avere quella pagina aperta su un solo apparecchio per riceverla ovunque.

Succedono ancora cose, nel panorama dei lettori Rss. La più rilevante di recente è il ritorno di [NetNewsWire](https://inessential.com/2018/08/31/netnewswire_comes_home).

Il programma ha una storia affascinante e il suo sviluppo attuale è promettente. Brent Simmons è uno bravo e racconta cose semplici che colpiscono, come questa:

>Può sembrare paradossale, ma è vero: proprio in quanto NetNewsWire è gratis e open source, dobbiamo avere obiettivi di qualità superiori a quelle delle app commerciali.

Date queste premesse, NetNewsWire ha tutto per piacermi. Eppure la mia motivazione a provarlo è zero e non c’entrano assuefazioni, invecchiamenti o pregiudizi; scarico e provo, o almeno lancio, un sacco di roba. Ma non lettori Rss.

Ho torto? Ascolto volentieri e continuo a seguire Brett Simmons. Il suo feed, naturalmente.