---
title: "Crederci sempre"
date: 2021-12-13T17:52:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [Apple, Asus, MacBook Pro] 
---
Test. Prendere un convinto di Windows e fargli leggere un articolo intitolato [Il nuovo rivale Asus di MacBook Pro costa cinquemila dollari](https://www.techtosee.com/asus-new-rival-macbook-pro-costs-5000/).

Chiedergli se pensi ancora che Windows costa meno.

Risponderà di sì, perché crederci sempre.

Chiedergli se con la sola batteria offrirà le stesse prestazioni e la stessa autonomia di MacBook Pro.

Risponderà di sì, perché arrendersi mai.

Apple sarà anche una religione e i suoi presunti adepti dei fanboy, ma dall’altra parte abbiamo l’esercito delle simoneventure.

_Per ora si può lasciare un commento dalla [pagina apposita di Muut per Qui
ckLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile._