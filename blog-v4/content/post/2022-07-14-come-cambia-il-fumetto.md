---
title: "Come cambia il fumetto"
date: 2022-07-14T01:23:10+01:00
draft: false
toc: false
comments: true
categories: [Web]
tags: [Copernicani, Chrome, Contra Chrome, Scott McCloud, Leah Elliott]
---
Tutto è cominciato nel 2008, quando Google ha commissionato al disegnatore Scott McCloud un [fumetto promozionale su Chrome](https://www.google.com/googlebooks/chrome/small_08.html).

Recentemente l’attivista per i diritti digitali Leah Elliott ha ripreso lo stile di quel fumetto per farne uno *contro* Chrome, ritenuto una minaccia per la privacy e la democrazia, mica per niente chiamato [Contra Chrome](https://contrachrome.com/).

L’associazione Copernicani ha deciso di approntare una traduzione in italiano del fumetto, che giusto oggi [ha annunciato ufficialmente](https://copernicani.it/un-fumetto-ci-spiega-come-chrome-sia-diventato-una-minaccia-per-la-privacy-e-per-democrazia/).

Il punto di vista di Elliott è indubbiamente radicale; peraltro, il tema privacy è sempre più delicato e problematico e immagino che molte persone, anche veterane della rete, non siano del tutto consapevoli di tutto quello che sta dietro Chrome a questo riguardo.

Il sottoscritto, socio dei Copernicani e sempre pronto a consigliare l’iscrizione a chi abbia a cuore le sorti tecnologiche e non solo del Bel Paese, ha messo lo zampino nella traduzione del fumetto e assicura che, ove non convincesse politicamente, è interessante da leggere e qualche gioco di parole è venuto piuttosto bene.