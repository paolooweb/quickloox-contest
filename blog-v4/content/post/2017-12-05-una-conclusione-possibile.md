---
title: "Una conclusione possibile"
date: 2017-12-05
comments: true
tags: [Viticci, iPhone, X]
---
Lentamente comincia a emergere il significato più profondo di iPhone, il livello di lettura superiore rispetto alle banali specifiche o alle novità commerciali.<!--more-->

Per questo è occorso del tempo prima di avere una recensione sufficientemente illuminata dell’oggetto: è [iPhone X: A New Frontier](https://www.macstories.net/stories/iphone-x-a-new-frontier/) di Federico Viticci su *MacStories*.

Lunga e articolata, copre molto bene le caratteristiche e lo spirito dell’apparecchio. Mi permetto di citare la conclusione del pezzo, alla fine della sezione [The Impossible iPhone](https://www.macstories.net/stories/iphone-x-a-new-frontier/3/#the-impossible-iphone):

>Ma Apple sa ancora dare vita a cose impossibili. E una volta ogni tanto, sono pazzi a sufficienza per modellare il futuro nella forma di un computer tascabile. Un concetto apparentemente lontano, che ridefinisce che cosa possa essere oggi un iPhone.

Si può discutere all’infinito di prezzi e processori, ma di aziende che funzionano in questo modo continua a essercene una sola.