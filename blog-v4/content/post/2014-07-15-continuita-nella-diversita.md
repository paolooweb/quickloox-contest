---
title: "Continuità nella diversità"
date: 2014-07-15
comments: true
tags: [Apple, Adobe, Photoshop, Mix, Line, Sketch, Sdk, iPad, Mac, Yosemite, iOS8, iOS, OSX, CreativeCloud]
---
Giusto ieri ho partecipato a una chiacchierata sugli ultimi progressi della piattaforma Adobe, organizzata da [Burson-Marsteller Italia](http://burson-marsteller.it) e tenuta dal sempre impeccabile [Matteo Oriani](https://www.linkedin.com/in/moriani).<!--more-->

Ho registrato audio e preso appunti per due ore soprattutto su Photoshop e il rapporto tra gli strumenti di [Creative Cloud](http://www.adobe.com/it/creativecloud.html) su Mac e i loro contraltari su iOS.

Una delle notizie del giorno era in parte la disponibilità di nuove *app* Adobe per iOS (e al momento solo iOS), come [Photoshop Mix](https://itunes.apple.com/it/app/adobe-photoshop-mix/id885271158?l=en&mt=8), [Adobe Line](https://itunes.apple.com/it/app/adobe-line-real-tools-for/id855475913?l=en&mt=8) e [Adobe Sketch](https://itunes.apple.com/it/app/adobe-sketch-inspiration-drawing/id839085644?l=en&mt=8), soprattutto il fatto che queste *app* siano realizzate con un nuovo Software Development Kit il quale diventerà pubblico in autunno e permetterà agli sviluppatori indipendenti di ampliare le possibilità del software con una libertà senza precedenti.

Più di questo: chi era presente ha visto concretizzata la possibilità di iniziare l’*editing* di una immagine su iPad, per dire, e riprenderlo all’istante su Mac nello stesso punto in cui lo abbiamo lasciato sulla tavoletta.

Ricorda niente? A giugno Apple ha dimostrato [Handoff](https://www.apple.com/it/ios/ios8/continuity/) che è praticamente la stessa cosa per il software di serie su Mac e iOS che debutterà in autunno.

Non è una questione di copia o di chi sia arrivato prima: sviluppi software di questo genere non si compiono da un mese a quell’altro. Invece è questione di evoluzione e direzioni di sviluppo. Se l’idea della continuità tra le operazioni *desktop* e quelle *mobile* trova applicazione in due ecosistemi importanti e complementari come Apple e Adobe, per molti versi paralleli, significa che è una strada quanto meno promettente.

Strada che per altri ecosistemi è ancora pura utopia.