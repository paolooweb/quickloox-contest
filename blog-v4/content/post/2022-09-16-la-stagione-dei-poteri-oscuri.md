---
title: "La stagione dei poteri oscuri"
date: 2022-09-16T12:23:13+01:00
draft: false
toc: false
comments: true
categories: [Web]
tags: [TicketOne, RyanAir]
---
Nell’acquisto di biglietti per il teatro tramite [TicketOne](https://www.ticketone.it), sapevo che le commissioni del sito sono piuttosto alte e ho a riguardo un atteggiamento neutrale. Da una parte a nessuno fa piacere vedere un sovrapprezzo, dall’altra di qualcosa devono pure vivere e comprendo che uno dei flussi di fatturato, oltre che gli accordi con i produttori e gli organizzatori e la pubblicità, possa essere la commissione sulle vendite.

D’altronde è un prezzo che sono disposto a pagare per evitare il fastidio di dover andare anticipatamente al teatro o passare tempo al telefono con una segreteria per poi dover pagare al ritiro eccetera eccetera. Soldi per libertà, le commissioni sono accettabili e il teatro non è un bene essenziale (l’anima è un tema interessante ma non qui e ora, limitiamoci alla bieca materialità).

Alcune delle stesse cose le pensa il teatro, felicissimo di affidare la raccolta dei biglietti a TicketOne e liberarsi di un onere, di competenza da possedere e così via. Il compromesso può avere qualche spigolo e comunque risultare positivo per tutti, visto che a teatro andremo.

In questo quadro però si inserisce la richiesta di due euro e cinquanta *per ricevere i biglietti digitali*. Per averli gratis (in senso lato: gratis escluse le spese di acquisto, le commissioni, il tempo impiegato) occorre riceverli in PDF, tassativamente da stampare.

In casa abbiamo una stampante che usiamo una volta al mese di media, quando proprio è impossibile farne a meno. Stavolta abbiamo stampato, per una questione di principio: TicketOne dovrebbe chiedere soldi a chi vuole stamparli, i biglietti; non il contrario.

La logica, la semplicità, perfino la tutela dell’ambiente suggeriscono di fornire biglietti digitali. Per TicketOne non ci sono reali costi aggiuntivi per fornire una opzione o l’altra.

Questo è un dark pattern, uno schema nascosto nell’ombra; il modo in cui un’azienda cerca di spillare denaro supplementare senza che le cose siano chiare dall’inizio e cercando di mettere anche qualcuno con le spalle al muro (*cavolo, e noi che non abbiamo la stampante dobbiamo pagare anche questi?*).

Quest’estate ho avuto abbastanza dark pattern per un anno grazie a [RyanAir](https://www.ryanair.com/it/it), che pubblica un prezzo e poi aggiunge supplementi per qualsiasi cosa sia oltre poter respirare normalmente in cabina.

Un solo esempio: abbiamo accettato di pagare un extra per scegliere i posti in aereo. Poi ci è stato detto che non era possibile sceglierli e siamo stati rimborsati.

Al momento del check-in, abbiamo visto che i posti erano stati preassegnati e che eravamo seduti insieme, *tranne un passeggero*. Che potevamo scegliere di sistemare noi, anche insieme al resto della famiglia perché i posi adiacenti erano liberi, a pagamento.

Dice: *ringrazia che hai pagato per scegliere un posto invece di quattro*. Rispondo: *dimmi subito che vuoi questa cifra e te le do, invece di farmi un balletto di transazioni inutile e perditempo alla fine di un processo di acquisto esasperante in cui bisogna dribblare una dozzina di richieste di spese extra per comprare un biglietto*.

I *dark pattern* ci sono dalla notte dei tempi del web, ma non ricordavo certe innovazioni in materia, né da TicketOne né da RyanAir, che già non scherzavano dall’inizio.

È invece la stagione di iniziare a lamentarsi il più rumorosamente possibile. Penso che tutti siano disposti a riconoscere prezzi onesti per servizi corrispondenti, in situazioni chiare. Alzare polvere per grattare denaro agli sbadati o ai noncuranti è una pratica di business deprecabile.