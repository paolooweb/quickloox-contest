---
title: "La teoria del multiverso"
date: 2013-09-21
comments: true
tags: [iPhone]
---
Da non esperto di astrofisica teorica o effetti quantistici, l'ho capita così: in ogni momento l'universo si biforca in infinite varianti corrispondenti alle alternative possibili. Quindi non esiste un universo solo ma universi infiniti, ciascuno separato dagli altri almeno per una minima differenza. Per un universo dove esce testa, banalizzando al massimo, ne esiste un altro dove esce croce.<!--more-->

Scrive [Macity](http://www.macitynet.it/secondo-chipworks-la7-iphone-5s-prodotto-samsung/):

>Il processore A7 dell’iPhone 5s è costruito da Samsung.

Scrive [Macity](http://www.macitynet.it/il-processore-a7-iphone-5s-prodotto-intel/):

>Il processore A7 non è prodotto da Samsung. […] Esclusa TSMC non resta che una sola azienda al mondo in grado di produrre gli A7: Intel.

Scrive [Macity](http://www.macitynet.it/apple-a7-tsmc-sta-gi-preparando-il-prossimo-processore-di-iphone-e-ipad/):

>Apple A7 è la prossima evoluzione del processore di Cupertino che sarà integrato negli iPhone e negli iPad del 2014: secondo DigiTimes, riportato da Cnet, la progettazione del chip è già nelle fasi finali e TSMC, il più grande costruttore indipendente di chip al mondo, sta completando i lavori per dare via alla fase di fotomascheratura e poi con l’avvio delle prime produzioni pilota che potrebbero cominciare già entro questa estate.

Benvenuti nel multiverso, dove un processore viene costruito da qualsiasi azienda lo possa costruire.
