---
title: "Il settimo cambio"
date: 2014-10-23
comments: true
tags: [tastiera, pile, Duracell]
---
Ho inserito il settimo giro di pile nella [Wireless Keyboard II](http://store.apple.com/it/product/MC184T/B/tastiera-apple-wireless-keyboard-italiano) di Apple, dopo la *performance* peggiore finora registrata: 110 giorni di autonomia.

La media rimane comunque al livello rispettabile di 293 giorni. Non ho spiegazioni evidenti alla durata ridotta delle ultime pile (Duracell Plus Power) e mi limito a prendere atto di una deviazione standard decisamente alta rispetto a quanto avrei sospettato.