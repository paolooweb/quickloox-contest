---
title: "Un sabato non italiano"
date: 2023-08-13T03:59:33+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [iBrogueCE]
---
Per una sera ho lasciato che [iBrogueCE occupasse lo spazio](https://macintelligence.org/posts/2023-08-06-giochi-senza-tastiere/) solitamente competenza di letture e blog.

Ho fatto un paio di spedizioni semza ambizioni, giusto per ritrovare memoria delle cose importanti. Ce ne sono diverse che proprio non ricordavo. Sono arrivato intorno al decimo livello, uno dei punti di svolta del gioco quanto a intensità e problematiche.

Prima di riuscire ad arrivare al ventesimo livello, la svolta che introduce il finale di partita, devo sicuramente riprendere la mano. Però è stato quasi emozionante ritrovare il gioco e le sue dinamiche.

Fiducia e perseveranza.

*Gli aggiornamenti in agosto del blog potrebbero essere irregolari o infrequenti.*