---
title: "Il gioco delle copie"
date: 2013-06-15
comments: true
tags: [Microsoft, Windows]
---
Diceva più o meno Karl Marx che le tragedie della storia si ripetono come farsa.

A marzo [si accennava](https://macintelligence.org/posts/2013-03-20-ce-sempre-un-perche) come Microsoft cercasse di popolare il proprio negozio *online* di applicazioni a colpi di buoni da cento dollari per i programmatori disposti a tutto pur di mettere qualche soldo in tasca (e magari parleremo anche della qualità di quello che può venir fuori da un sistema del genere).<!--more-->

Molto è cambiato. Adesso, annegata in un <a href="http://www.businessweek.com/articles/2013-06-11/apple-flatters-microsoft-with-imitation">articolo-fiume su BusinessWeek</a> di sostanziale propaganda per Windows Phone, si può leggere la frase seguente:

>I miei amici in Silicon Valley mi dicono che Microsoft ha offerto centomila dollari e anche più di questi ad aziende che realizzeranno app per Windows Phone.

Continuo a preferire un modello dove c’è un’idea, quell’idea funziona, e viene premiata, al modello dove le idee degli altri vengono fatte copiare a forza (di denaro).