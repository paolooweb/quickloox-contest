---
title: "Standard di disfatto"
date: 2020-07-14
comments: true
tags: [Microsoft, Intel, Arm, Gassée, Monday, Note, Mac, iPad, Office, Ballmer, Windows, Qualcomm, Amd]
---
Scrivevo tempo fa di come iPhone e iPad avessero messo la Microsoft di Steve Ballmer [fuori posto](https://macintelligence.org/blog/2013/06/14/q-e-d/) che offrisse o meno Office per iPad.

Jean-Louis Gassée spiega su Monday Note come [il passaggio ai processori Arm potrebbe provocare un terremoto per tutto il mercato](https://mondaynote.com/apple-silicon-the-passing-of-wintel-79a5ef66ad2b), nonostante Mac sia meno del dieci percento dei computer venduti.

Se infatti Apple crea portatili molto competitivi per prestazioni e consumi, Microsoft sarà forzata a migliorare Windows per Arm e la linea Surface. L’alternativa sarebbe arrendersi ad Apple e chiaramente non può essere fatto.

Se però Microsoft crea PC competitivi con processori Arm, che fine fanno i Dell, i Lenovo, gli Asus, gli Acer…? Devono passare ad Arm anche loro, oppure trovarsi al piano inferiore – e meno nobile – del mercato.

Qualcuno dovrà dare processori a questa gente e potrebbe essere Intel… che però dovrebbe riacquistare una licenza Arm dopo avere venduto la propria nel 2006, con notevole lungimiranza. E se lo facesse anche Amd, rivale di Intel? E che dire di altre aziende che già oggi dispongono di processori Arm, tipo Qualcomm?

Nel giro di pochi anni, davvero pochi, equilibri che tanti davano per immutabili potrebbero sconvolgersi. Le variabili sono infinite e c’è anche chi suggerisce che in fondo per Intel l’abbandono di Apple potrebbe essere semplicemente liberarsi di un cliente che chiede molto più di quello che offre, rispetto alle quantità di ordini.

Se però Gassée avesse ragione, ci sarebbero davvero da aspettarsi i fuochi artificiali, una ripresa della concorrenza tra costruttori difficile da immaginare prima.

A me piacerebbe fare un salto in una manciata di aziende che conosco, a chiacchierare con gente che ha venduto l’anima e il cervello a Wintel *standard di fatto*, per vederli pensare nel vuoto.