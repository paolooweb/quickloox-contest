---
title: "Una volta l'anno"
date: 2019-10-13
comments: true
tags: [iPad, Pro]
---
Ho riavviato iPad Pro. Non si collegava a un videoproiettore. L’ho spento e riacceso e si è collegato.

A parte gli aggiornamenti automatici di iOS, è la prima volta che mi tocca fare ripartire iPad Pro e lo uso da quasi un anno. Ho dovuto fermarmi un attimo a pensare a come spegnerlo, tanta era la desuetudine.

Sono situazioni come queste che fanno sentire contenti del prezzo del biglietto. È veramente una macchina straordinaria.
