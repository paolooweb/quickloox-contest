---
title: "Finisce per t"
date: 2017-09-29
comments: true
tags: [ftp, rsync, scp, sftp, ssh, telnet, Ascii, wget]
---
[Oltre a ftp](https://macintelligence.org/posts/2017-09-28-comincia-per-effe/), Apple ha levato da High Sierra anche telnet.<!--more-->

Sussistono polemiche anche in questo caso. Ho sentito attribuire a telnet capacità didattiche, taumaturgiche (l’unico mezzo per controllare lo stato di connessione di una porta), un sistema indispensabile per l’amministratore di rete eccetera.

Sono naturalmente fiorite le battute tipo *con quello che costa un Mac potevano pure lasciarlo*. Carine, in quanto battute.

Tornando seri, i professionisti… no. Gli appassionati… no. I power user… no. Meglio: le persone preparate hanno sostituito da anni telnet con nc (anche noto come netcat).

Per i battutisti: su Linux [se ne parla dal 2007](http://linux.byexamples.com/archives/258/when-netcat-act-as-telnet-client-it-becomes-better/). Dieci anni fa c’era chi usava nc al posto di telnet.

Le ragioni sono varie. Digitare `man nc` nel Terminale fa comparire per prima cosa questo:

>L’utility nc (o netcat) si usa per praticamente qualunque cosa coinvolga TCP o UDP. Può aprire connessioni TCP, inviare pacchetti UDP packets, ascoltare su porte TCP e UDP a piacere, scandire le porte e cavarsela sia con IPv4 che con IPv6. A differenza di telnet, nc si lascia scriptare e invia i messaggi di errore sul canale appropriato invece di mandarli sull’output standard come a volte fa telnet.

Il tono è abbastanza chiaro: il presente, e il futuro, si chiama nc e c’è solo da mettersi più comodi.

L’immenso Fabrizio Venerandi ha posto il problema di [come guardare una geniale resa di *Guerre Stellari* in caratteri ASCII](https://www.facebook.com/photo.php?fbid=10214994910852203&set=p.10214994910852203&type=3&theater), a tutti nota mediante il comando `telnet towel.blinkenlights.nl`.

`nc -v towel.blinkenlights.nl 23` fa esattamente la stessa cosa. Se aggiungo che lo stesso parametro apre la porta di ingresso dei [Mud](http://www.mudconnect.com), direi che abbiamo esaurito gli usi veramente significativi di telnet. Che peraltro, come ftp, resta installabile a volontà.

Il problema è che queste sono tutte argomentazioni razionali, fatti verificabili. Ma qui ci troviamo di fronte a persone che reagiscono oggi all’esclusione di telnet come anni fa alla mancanza di wget: se finisce per t, si lamentano.