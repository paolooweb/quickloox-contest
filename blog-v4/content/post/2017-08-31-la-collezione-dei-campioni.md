---
title: "La collezione dei campioni"
date: 2017-08-31
comments: true
tags: [Seagate, Toshiba, Backblaze]
---
A parte la [fine del servizio di Crashplan](https://macintelligence.org/posts/2017-08-24-ok-il-prezzo-e-sbagliato/), una delle ragioni per raccomandare i *backup* via rete di Backblaze è che pubblicano periodicamente un sunto molto istruttivo della vita utile che hanno i loro dischi.

Quello [relativo al trimestre di primavera](https://www.backblaze.com/blog/hard-drive-failure-stats-q2-2017/) porta una montagna di dati molto ma molto interessanti, raccolti su un campione di 83.151 dischi rigidi.

Poi c’è anche tutta una serie di osservazioni su come sono organizzati, come fanno a inseguire la massima efficienza con il minimo costo e via discorrendo. Mi ha colpito infine la seguente precisazione:

>Nel guardare ai numeri del trimestre, ricordate di considerare i modelli con almeno cinquantamila giorni disco. Questo è un dato equivalente a circa 550 dischi in funzione ininterrottamente per tre mesi. Si tratta di un buon campione. Se il campione è inferiore a questi numeri, i tassi di malfunzionamento possono alterarsi anche a seguito di un minimo cambiamento nel numero di guasti.

Ecco: sulla affidabilità dei dischi, valgono i giudizi di chi ne usa almeno 550 in un trimestre, ventiquattro ore al giorno. Sotto questo valore, i dati sono inaffidabili. Figuriamoci le opinioni.