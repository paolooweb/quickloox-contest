---
title: "Umani e quattordicenni"
date: 2019-10-07
comments: true
tags: [Campbell, Operator41, Arcade]
---
Si è già scritto delle scelte di Apple di [affidare la cura di musica e notizie a esseri umani](https://macintelligence.org/posts/2017-05-28-umanamente-impossibile/): quelli lenti che sbagliano, ma hanno la fantasia, l’improvvisazione, il colpo di genio, la capacità di prendere una direzione completamente non ovvia. L’algoritmo, il mestiere di Google, di Facebook, di Amazon, di Microsoft, di tutti, è perfetto, veloce, infinitamente personalizzabile: capisce quello che vuoi e continua a dartelo in mille salse. La prigione dell’anima.

Scopro che su [Arcade](https://www.apple.com/apple-arcade/), il servizio di giochi in abbonamento mensile, troverà posto [Operator 41](https://apps.apple.com/it/app/operator-41/id1475042637), [creato da un quattordicenne](https://www.vice.com/en_us/article/8xwvbx/how-a-14-year-old-designer-became-part-of-apples-splashy-new-gaming-service-apple-arcade). Di San Francisco? No, di Londra.

Un gioco che non arriva da studi indipendenti agguerriti e dedicati, ma da un ragazzino. Che non risponde a una selezione operata da un’intelligenza artificiale, ma da un gruppo di umani.

Sono un’entusiasta dell’intelligenza artificiale, al punto che propugno gli scenari più radicali e disdegno l’idea che il *machine learning* possa essere un punto di arrivo; semmai un punto di partenza. Nessun algoritmo seleziona o ti propone il gioco di uno studentello, che ha alle spalle al massimo i compagni di classe beta tester e nessun investimento in marketing, SEO, pubblicità.

Per questo preferisco radio, notizie, giochi, film, app selezionati da umani o almeno con il loro concorso attivo, riconosciuto e valutato verso l’alto. Una cosa che fa solo Apple.