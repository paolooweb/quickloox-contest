---
title: "Come sa di sale lo Chromebook altrui"
date: 2019-11-18
comments: true
tags: [Schiller, Chromebook, Google, Mac, Microsoft, Minecraft]
---
Phil Schiller, responsabile marketing di Apple, [ha commentato assai negativamente](https://9to5google.com/2019/11/13/apple-phil-schiller-chromebooks/) sui Chromebook a basso costo che pare stiano avendo buon successo in molte scuole americane.

Nonostante affermi che [qualsiasi studente nel XXI secolo dovrebbe poter prendere parte alla creazione della tecnologia che sta cambiando il mondo](https://www.microsoft.com/en-us/windows/upgradeyourworld/Hourofcode), Microsoft ha cambiato rotta ai propri tutorial di coding attraverso Minecraft proposti per l’iniziativa didattica *Hour of Code*: sono sempre stati eseguibili via browser, cioè da chiunque. L’ultimo giro, invece, è [compatibile con Windows, Mac e iOS](https://education.minecraft.net/hour-of-code), e niente più.

Uno dirà che negli anni è cambiato poco. C’è una differenza: nel secolo scorso, le macchine a basso costo destinate alle scuole poco esigenti facevano contenta Microsoft. In questo secolo, Google.

Si sarebbe potuto titolare anche *Chi di cheap ferisce*.