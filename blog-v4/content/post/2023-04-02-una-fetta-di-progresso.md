---
title: "Una fetta di progresso"
date: 2023-04-02T03:09:01+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [3D Slicer, Tailor-Made Organs, Dicom]
---
Mi sono ritrovato nel centro di Milano ad ascoltare un chirurgo e un ricercatore universitario che hanno capito come collaborare in modo che uno stampi in 3D copie degli organi da operare all’altro e questi ne approfitti per preparare in tempo record le operazioni, formare prima e meglio gli ultimi arrivati eccetera.

Il vero genio sta nella ricerca sui materiali. La copia dell’organo stampata in 3D non è solo conformata come l’originale, ma ha anche una consistenza equivalente. Così, se si tratta poniamo di un fegato, il chirurgo arriverà a quello vero dopo essersi esercitato su quello copiato, in modo verosimile: saprà che cosa aspettarsi e avrà letteralmente già provato ad affondare il bisturi dove poi dovrà farlo effettivamente. Un caso straordinario di tecnologia che migliora la vita di tutti, non ultima quelle del paziente, che aumenta le probabilità a suo favore.

A domanda, il ricercatore mi dice che il software impiegato in gran parte del processo di produzione degli organi-copia è [3D Slicer](https://www.slicer.org): software che non conoscevo e di cui ora, dopo avere curiosato, posso solo dire un gran bene.

A partire dal fatto che è open source, di quelli sempre aggiornati e con una documentazione eccellente, disponibile su Mac-Windows-Linux in modo paritario e degno (su Mac è supportato anche Apple Silicon). A detta degli sviluppatori, 3D Slicer è installabile su qualsiasi computer che abbia meno di cinque anni.

Installato, il software fornisce strumenti formidabili per l’elaborazione e l’analisi delle immagini 2D e 3D in formato Dicom (usatissimo in campo medico).

Stante che si augura a tutti di non averne bisogno, ove fosse auspicabile l’uso, 3D Slicer è un pezzo di software ottimo,da prendere a esempio per come vorremmo fosse l’informatica personale. Efficace, ben sviluppato, universale. I pazienti e i chirurghi meritano uguale trattamento e attenzione qualsiasi piattaforma abbiano deciso di adottare.