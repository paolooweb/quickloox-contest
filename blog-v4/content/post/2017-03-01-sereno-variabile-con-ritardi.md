---
title: "Sereno variabile con ritardi"
date: 2017-03-01
comments: true
tags: [orari, Mac, batterie]
---
Mi dispiace per i radi aggiornamenti di questi giorni e, tra oggi e domani, quasi certamente mi sarà impossibile provvedere. Conto di riprendere un ritmo più regolare entro il fine settimana e mi scuso con tutti nel frattempo.

Il mio Mac, comunque, [non è ancora esploso](https://medium.com/@dourvaris/my-2015-macbook-pro-retina-exploded-119ea5ea9d1f#.8zurgot42).