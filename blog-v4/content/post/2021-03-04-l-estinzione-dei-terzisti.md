---
title: "L’estinzione dei terzisti"
date: 2021-03-04T01:03:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [Apple, iOS 14.5, Google, cookie, Facebook, John Gruber] 
---
Lo scorso anno Apple ha annunciato la transizione di Mac ai processori Arm e a tutti è apparso evidente lo spessore dell’operazione, piena di complicazioni, rischi e azzardi, con decine di miliardi di dollari sul tavolo.

Ma è niente in confronto a quello che sta succedendo con iOS 14.5, che fa chiedere alle app il permesso di tracciare chi hanno davanti anche quando lasciano il sito in cui si trovano.

A seguito dell’iniziativa di Apple, qualcosa che mai sarebbe neanche minimamente successo senza iOS 14.5, Google ha annunciato che [metterà gradualmente fine al tracciamento della navigazione sui siti terzi](https://blog.google/products/ads-commerce/a-more-privacy-first-web).

Ci sono anche qui decine di miliardi di dollari sul tavolo, solo che se li giocano tutti: Google, Facebook, Microsoft, centinaia di agenzie trafficanti di contatti e dati di privacy.

Non tutti ci credono; John Gruber, per esempio, è scettico e pensa che Google [giochi con le parole](https://daringfireball.net/linked/2021/03/03/google-temkin-ad-tracking) per fare rientrare dalla finestra quello che esce dalla porta.

Tuttavia è già clamoroso che Google dirami un annuncio del genere.

Google ama la privacy come Microsoft l’open source. La tutela quando ha esaurito qualsiasi altro sistema per farne a meno.

*Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile.*