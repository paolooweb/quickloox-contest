---
title: "Stampante, stai serena"
date: 2016-04-20
comments: true
tags: [Kodak, Esp7, HP, ScanJet, Readiris, Photostudio, Arcsoft, HP]
---
Casa mia è, informaticamente parlando, vecchia. Alla faccia degli [analfabeti funzionali](http://www.repubblica.it/tecnologia/prodotti/2016/04/15/news/apple_l_obsolescenza_programmata_e_realta_l_iphone_dura_tre_anni-137712684/) che hanno abusato del [rapporto ambientale Apple](http://www.apple.com/it/environment/reports/) per scongelare l’ancora più vecchia bufala dell’obsolescenza programmata. Gente che sa riciclare solo i clic.<!--more-->

A casa mia un Mac è del 2009, l’altro del 2011. Gli iPad sono del 2010 e del 2012. Gli iPhone sono del 2011 e del 2013. Apple Watch e Apple TV sono arrivati di recente e fanno eccezione, però non potrebbe essere diversamente: specialmente il primo, un anno fa non esisteva.

In questo contesto stiamo mandando in pensione la stampante multifunzione [Esp 7](http://www.kodak.com/support/pdf/en/manuals/urg00976/ESP7_UG_GLB_en.pdf) di Kodak comprata, anch’essa, diversi anni fa. È stato un esperimento interessante, che ha fatto il suo tempo. Kodak è implosa dedicandosi a (poco) altro. I driver non sono più aggiornati o lo sono in modo discutibile e si possono utilizzare solo le funzioni primarie.

La goccia che ha fatto traboccare il vaso è che, nonostante le cartucce di inchiostro siano ancora reperibili online, il riconoscimento delle stesse è deficitario. Occorre comprare tre cartucce per avere una minima sicurezza che una funzioni. E, se anche solo una delle due cartucce necessarie non risponde, con scarso senso della logica la stampante *non effettua scansioni*.

Abbiamo anche fatto due conti e ci siamo accorti che produciamo di fatto poco più di un foglio di carta al mese. Una stampante *non ci serve*; molto più comodo, quella volta che, fermarsi dalla cartolaia e provvedere con pochi centesimi.

È invece arrivato uno [Scanjet 300](http://m.hp.com/lamerica_nsc_carib/en/products/scanners/product-detail.do?oid=5251707) di HP, perché di scansioni ne produciamo un buon numero, per ragioni di lavoro e fiscali.

L’unità è modesta perché corrisponde ai nostri bisogni: buona riproduzione di documenti in bianco e nero, decente risposta sul colore per prototipazione grafica. Al primo impatto è modesta anche la dotazione: il driver è meglio scaricarlo da Internet per averlo aggiornato, dopo di che installa 255 megabyte di software per fare vedere lo scanner ad Acquisizione Immagine o programmi che ne sfruttino il motore, come Pixelmator o Anteprima. HP fornisce un certo [Photostudio](http://www.arcsoft.com/photostudio/) come programma grafico (non più supportato dal produttore) e il riconoscitore ottico dei caratteri [Readiris](http://www.irislink.com/c2-1301-189/Readiris-15-for-Mac----OCR-software.aspx), che abbiamo ignorato – il primo – e accantonato in attesa di una necessità, il secondo. Anche perché Readiris nella confezione è versione 12 e la versione aggiornata porta il numero 15. Insomma.

Le prime scansioni sono buone e lo scanner ha velocità discreta, comunque superiore a quella della funzione equivalente sulla Esp 7.

L’apparecchio si trova un po’ sperso in mezzo a una dotazione di hardware altrimenti preistorica, ma si ambienterà. Prima o poi arriva per tutti il tempo della rottamazione. Esp 7, stai serena.