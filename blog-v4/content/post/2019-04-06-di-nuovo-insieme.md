---
title: "Di nuovo insieme"
date: 2019-04-06
comments: true
tags: [BBEdit, Store, Mac]
---
L’annunciato rientro di BBEdit su Mac App Store [è realtà](https://www.barebones.com/company/press/bbedit_back_to_mas_pr.html).

Si tratta di una notizia molto bella per tanti motivi. Conferma la vitalità di Bare Bones e porta credibilità allo Store, che sempre Bare Bones non si era fatta problemi nell’abbandonare quattro anni fa 

È anche un ritorno cosciente e voluto, non un compitino da esaurire formalmente per poterlo raccontare, tanto che su Mac App Store BBEdit sarà offerto in abbonamento. La formula mi sta antipatica e preferirò aggiornare il programma dal sito del produttore, ma questo è un dettaglio assai meno interessante del fatto che evidentemente c’è una strategia di vendita e di posizionamento  dietro.

Ci sono programmi storici che hanno avuto da sempre una relazione quasi simbiotica con Apple e altri che sembrano vivere nell’ecosistema nonostante le caratteristiche di quest’ultimo. In questi anni BBEdit era sembrato transitare del primo al secondo gruppo.

Scalda il cuore constatare che non era così o, almeno, non lo è più. Bare Bones continua la propria storia ultraventicinquennale confrontandosi con Apple su un piano di ricerca di vantaggi reciproci, cosa che può solo fare piacere all’utilizzatore. Finalmente (di nuovo) insieme.
