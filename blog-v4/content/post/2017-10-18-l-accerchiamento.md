---
title: "L’accerchiamento"
date: 2017-10-18
comments: true
tags: [Apple, Windows, Surface, Book, Mate, Huawei]
---
Microsoft ha presentato [Surface Book 2](https://www.microsoft.com/en-us/surface/devices/surface-book-2/overview), con prezzo a partire da 1.499 dollari (e un quindici pollici parte da 2.499 dollari).

Ho ricevuto intanto il comunicato stampa, datato 16 ottobre, che annuncia in preordine il nuovo Mate 10 Pro di Huawei a partire da 845 euro, ma soprattutto Mate 10 Porsche Design, disponibile dalla metà di novembre a 1.395 euro.

Apple è accerchiata. Se vuoi dire che Windows o Android costano meno e fanno le stesse cose, lo puoi dire gratis (fanno le stesse cose) trovando una ciofeca che costa meno.

Se invece vuoi dire che la qualità di Windows o di Android è pari a quella Apple, puoi fare notare che ci sono sistemi di prezzo paragonabile, se non superiore.

Quello che mi torna a fatica è l’intersezione degli insiemi. Vorrei sentire qualcuno smontare Surface Book 2 o un Mate 10 con la motivazione che puoi pagare molto meno e avere un apparecchio che fa le stesse cose.

Oppure qualcun altro che smonti le ciofeche con la dimostrazione evidente della maggiore qualità di questi apparecchi.

Non ne ho ancora trovati. Windows e Android sono ancora Shangri-La, la terra dei sogni dove quando serve puoi dire che costa meno e, quando cambia il vento, dire che c’è la qualità. Tutto e il suo contrario.