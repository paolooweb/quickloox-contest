---
title: "Il passato e gli spaghetti"
date: 2014-07-31
comments: true
tags: [Dropbox, Mac, SuperPaint, Wingz, ClarisWorks, ClarisResolve, MacPaint, iPad, iTunes, iPhone, Usb, LibreOffice]
---
Se non fosse per la comodità della ricarica, avrei abbandonato da tempo il collegamento fisico a Mac di iPhone e iPad. iTunes mi ricorda perfino di non essere abilitato a sincronizzare iPad – probabilmente con il [cambio di disco interno](https://macintelligence.org/posts/2014-02-03-veloce-e-silenzioso/) ho sballato il conto degli apparecchi autorizzati – e la cosa importa zero, perché da tempo i due apparecchi vivono esistenze indipendenti e separate, unite solo dai sistemi come [iCloud](https://www.icloud.com) e [Dropbox](https://www.dropbox.com), o qualche *app*.<!--more-->

La logica di chi vuole il cavo, ama godersi l’intreccio spaghettoso delle porte Usb e desidera a tutti i costi essere lui/lei a passare i dati è ineccepibile, ma a me vengono in mente lo stesso immagini di [moderna interazione](https://www.flickr.com/photos/raneko/2373696346/sizes/o/), stile anni novanta. (La foto è stupenda, peraltro).

I legami con il passato hanno più senso al momento per esempio di scaricare [LibreOffice 4.3](http://www.libreoffice.org/download/libreoffice-fresh/), tra le novità del quale sta addirittura la [compatibilità con ClarisWorks, MacPaint](https://wiki.documentfoundation.org/ReleaseNotes/4.3) e altri programmi gloriosi per Mac che potrebbero avere smesso di funzionare anche vent’anni fa o più.