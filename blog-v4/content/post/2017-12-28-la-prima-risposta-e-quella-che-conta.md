---
title: "La prima risposta è quella che conta"
date: 2017-12-28
comments: true
tags: [iPad, Pro, Apple, //e]
---
Sostengo senza problemi che, per quanto ci sia stato dato di vedere finora, l’epoca del computer e specialmente della rete è ancora assolutamente agli inizi. Se pure sono passati venti, trenta, quarant’anni da certi sviluppi tecnologici, bisogna vederli come se si trattasse delle auto, o del volo umano. Nel 1940 volavano aerei immensamente superiori a quelli del 1910. Poi però siamo andati sulla Luna, per dire. Vedremo progressi che neanche ci immaginiamo.<!--more-->

Anche perché iniziamo solo ora a riscuotere i frutti di tanto lavoro. Qualcuno si è messo a misurare [la latenza dello schermo dei computer](https://danluu.com/input-lag/) da Apple IIe a oggi, cioè il ritardo da quando si preme un tasto a quando la macchina risponde mostrando un carattere sullo schermo.

iPad Pro ultimo modello è l’unico computer negli ultimi quarant’anni che sia riuscito a offrire una latenza paragonabile a quella di Apple //e. Tutti gli altri sono peggio, a parte alcune console per videogiochi.

In quarant’anni siamo riusciti semplicemente a tornare alla latenza video del 1977.

Probabilmente il dazio di implementare mille altre cose e infinita complessità è stato pagato. Probabilmente gli apparecchi del futuro faranno meglio di quanto è successo finora.

Questo dà comunque l’idea dello stadio dell’evoluzione in cui ci troviamo. Siamo come bambini che finalmente si sono alzati in piedi, un passo in avanti clamorosamente superiore al gattonare. Ma, prima di cominciare veramente a farci qualcosa di utile, continuiamo a ritrovarci con il sedere per terra, proprio come prima di gattonare.

La latenza del video è una belle metrica per tirare giudizi, spietata e difficile da combattere. Mica per niente tutti gli apparecchi Android hanno valori peggiori di tutti gli apparecchi iOS, per esempio. Nell’articolo si trovano molte altre informazioni interessanti. Armiamoci di pazienza e speranza. Il bello deve ancora arrivare e vogliamo auspicare di vederlo il più alla svelta possibile.