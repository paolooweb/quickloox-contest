---
title: "Convergenze e reminiscenze"
date: 2019-05-25
comments: true
tags: [Troughton-Smith, NeXTStep, OpenStep, OSX, macOS, Carbon, Cocoa, AppKit, UIKit, Apple, NeXT]
---
Steve Troughton-Smith ha pensato di spiegare che cosa potrebbe accadere con l’arrivo di applicazioni iOS su Mac, lato sviluppatore, e <a href="https://www.highcaffeinecontent.com/blog/20190522-(Dont-Fear)-The-Reaper" title="Don't Fear the Reaper">lo ha fatto</a> in modo splendidamente articolato e documentato tornando a quando Apple acquisì NeXT e costruì una nuova esperienza di uso mettendo insieme progressivamente Cocoa, il nuovo che derivava dall’acquisizione, con Carbon, che faceva funzionare le app della tradizione.

Due sorprese lungo il cammino: la prima, moltissimo di quello che oggi riteniamo *Mac-like* appartiene a Cocoa, ovvero al nuovo arrivato. La seconda: la dismissione di Carbon avviene nella prossima versione di macOS, diciotto anni dopo la sua comparsa.

Oggi gli analoghi di allora sono AppKit (Mac) e UIKit (iOS) e, rileva giustamente Troughton-Smith, non ci sono un vecchio e un nuovo quanto, piuttosto, due linee di sviluppo destinate a convergere nel tempo.

Che cosa sarà di macOS non lo sappiamo ma, argomenta saggiamente l’autore, se AppKit impiegherà diciotto anni anni a uscire di scena, non c’è proprio da preoccuparsi.

Lettura affascinante, con schermate d’epoca, che getta luce sull’incredibile fatica compiuta a livello di interfaccia utente per armonizzare il sistema operativo Mac con il nuovo arrivato di NeXT in modo che il sapore restasse Mac ma al tempo stesso si potessero sfruttare certe caratteristiche evidentemente più evolute.