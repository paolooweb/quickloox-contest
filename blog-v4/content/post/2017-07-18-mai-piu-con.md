---
title: "Mai più con"
date: 2017-07-18
comments: true
tags: [Panic, Transmit, Acorn]
---
È un periodo di mio scarso interesse per il software Mac. BBEdit copre le mie esigenze professionali e non vedo programmi così dirompenti da convincermi a cambiare abitudini in altri campi.<!--more-->

Ma non bisognerebbe confonderlo con un periodo di scarsità di buon software per Mac e anzi, è un ottimo momento per decidere se sganciarsi dai soliti noti che per carità, lavorano benissimo, se uno trascura lo spreco di risorse e di funzioni inutilizzate.

Per esempio, [Transmit](https://panic.com/blog/hello-transmit-5/): straordinario per il trasferimento file, in edizione totalmente rinnovata. Non c’è prezzo di aggiornamento perché l’ultima versione principale è uscita… sette anni fa. Tuttavia per pochi giorni è in sconto e ci sarebbe da approfittarne, a trentacinque dollari. Durasse altri sette anni, sono cinque dollari l’anno. (E chi ha detto che le software house indipendenti non sfornano più buon software per Mac? Panic non ha mai smesso e, con Transmit, ha ripreso).

Per altro esempio, [Acorn](http://flyingmeat.com/blog/archives/2017/7/acorn_6.html): È uscita una nuova versione piena di novità tra le quali la possibilità di disporre testo lungo un tracciato. Costa 14,99 dollari. Se serve appena, c’è neanche da parlarne. Io sono di altra parrocchia e Pixelmator fa tutto quello di cui ho bisogno; un altro programma indipendente di ottima fattura. Quanti hanno *veramente* bisogno di Photoshop, per carità. Non ne ho bisogno e ne sono lietamente privo. Probabilmente sono in tanti, cui è sufficiente un programma economico e di qualità come Acorn.

Per esempio ulteriore, [Scribus](https://www.scribus.net) che aggiungo anche come provocazione e che comunque ha ricevuto un aggiornamento veramente significativo a maggio. Se uno ha provato Scribus l’anno scorso e poi più, dovrebbe dargli una nuova possibilità.

InDesign? Monopolio inscalfibile? Monopolio sì, inscalfibile no. Per un piccolo lavoro, con un fornitore amico, in circostanze favorevoli si può e si deve provare a introdurre della diversità. La si potrebbe trovare conveniente.

Nessuno vuole male a InDesign, ma per esperienza le dinamiche sono più sane dove il software è più aperto. Si vuole semplicemente un mondo dove uno tra dieci possa dire *mai più con* e vivere tranquillo come gli altri nove.