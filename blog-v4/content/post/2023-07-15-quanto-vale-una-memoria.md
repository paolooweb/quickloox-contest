---
title: "Quanto vale una memoria"
date: 2023-07-15T02:26:59+01:00
draft: false
toc: false
comments: true
categories: [Hardware, Software]
tags: [Apple Watch, Nive, Registratore vocale]
---
La secondogenita, cinque anni, ha deciso improvvisamente di fare la doccia da sola. In quanto prima assoluta, un genitore è rimasto fuori dal box doccia per fornire eventuale supporto o attendere pazientemente di asciugarla.

Dentro la doccia, la ragazza ha cominciato a *cantare*.

watch e registratore vocale hanno raccolto una memoria tenera e incantevole.

watch mi si è ripagato una volta in più.