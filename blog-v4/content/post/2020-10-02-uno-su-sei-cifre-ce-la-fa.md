---
title: "Uno su sei cifre ce la fa"
date: 2020-10-02
comments: true
tags: [eCommerce, Internet, Traf]
---
Gira molto il resoconto delle [sei cifre in sei giorni](https://tr.af/6) di Traf, dopo il suo exploit di vendita di icone per iOS 14 in ragione, appunto, di oltre centomila dollari in meno di una settimana.

Nella sua presenza su Internet lui ostenta una estrema trasparenza e dichiara senza problemi che nella realtà i suoi sei giorni sono cominciati sette anni fa, quando ha venduto le sue prime icone su [Cydia](https://cydia-app.com) per diciassette dollari totali.

La trasparenza è uno dei suoi consigli, assolutamente da seguire. Per il resto non è miracolato né un esempio da seguire: come lui ce ne sono più di quanto appaia, non è che copiandolo ci troveremo automaticamente con centomila dollari sul conto. Contano invece tantissimo i consigli che dispensa e devono servire come fonte di ispirazione, anche se non si disegnano icone e non si guadagna a sei cifre.

La trasparenza è un consiglio; iterare è un altro; cogliere l’attimo, pubblicare spesso e altre cose per cui rimando alla lettura dell’originale.

Mi permetto invece di erogare un metaconsiglio, che deriva dalla lettura e dall’esplorazione dei contenuti di Traf. Il suo sito personale è minimale (un suo vanto), interessante (si è divertito a farlo) e illeggibile (testo bianco su fondo nero e altre sconcezze, un disastro).

Però i centomila se li è fatti ugualmente.

La morale: mai prendersi troppo sul serio e invece, altro consiglio che riporto, non avere paura di esprimerci e pubblicare quando e come si desidera. La lezione, quella definitiva, che mi porto a casa da tutto questo è la seguente:

>Internet è un flusso ininterrotto di contenuti; l’idea di stufare o esagerare è solo una scusa che ci inventiamo per smettere di pubblicare.