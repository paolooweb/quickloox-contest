---
title: "Meditazione programmata"
date: 2023-01-23T00:26:38+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [Lisa, Pascal]
---
Un angolo di rilassamento, la mente che respira, la calma, l’allontanamento dalle urgenze. Lettura casuale di qualche file dal [codice sorgente di Lisa](https://macintelligence.org/posts/2023-01-20-lorigine-del-mondo/).

Ci sono delle lezioni da imparare. Un ordine e una pulizia sovrani, la sobrietà degli spazi bianchi, la struttura complessa eppure dispiegata con semplicità e quasi eleganza, senza fretta, ogni cosa al suo posto.

Il linguaggio usato nella sezione che ho letto dovrebbe essere Pascal, che non mi piace e ho pure fortemente avversato quando mi è capitato di doverlo usare. Continua a non piacermi; tuttavia, usato bene, i listati si leggono quasi come un libro. Basta fare attenzione al contenuto e le cose diventano man mano chiare.

I commenti sono equilibrati, abbondanti senza strabordare, sintetici e non criptici. Contengono quello che è utile sapere, niente di più e niente di meno.

Può anche darsi che il sorgente di Lisa sia stato ripulito nel tempo. Ovviamente non l’ho letto tutto; forse ci sono zone caotiche, ineleganti, incomprensibili.

Di certo, dove mi è capitato di mettere il naso si respirano organizzazione, pianificazione, struttura. Chi ha scritto quei listati era persona scrupolosa, attenta, puntuale, o almeno lo era il suo supervisore.

Era l’avanguardia, quarantaquasi anni fa. Ho ammirato quelle persone e lo spirito che hanno trasmesso al loro lavoro.