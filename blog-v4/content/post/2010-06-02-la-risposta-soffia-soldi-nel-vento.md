---
title: "La risposta soffia (soldi) nel vento"
date: 2010-06-02
comments: true
tags: [Ping, Unix, Internet, Lynxlet]
---
Nel comprare la mia Internet Key sapevo perfettamente che le prestazioni di banda sarebbero state infime. Nella zona in cui mi trovo in semivacanza sull&#8217;Adriatico c&#8217;&#232; una copertura praticamente nulla da parte di tutti gli operatori e il collegamento avviene, quando va molto bene, in Edge, altrimenti nel preistorico Gprs. Ma questo era scontato.<!--more-->

Molto meno scontato invece che il server Smtp del gestore sia perennemente sovraccarico e che, almeno per le prove compiute finora, non sia possibile usarne altri. Cos&#236; la connessione &#232; lenta (ed era previsto) ma la posta elettronica parte quando vuole e questo non &#232; leale, perch&#233; niente porta profitto indebito quanto una tariffa a consumo in cui ci si deve collegare per pi&#249; tempo e pi&#249; volte al solo scopo di riuscire a spedire le email.

Fortunatamente non ho urgenze da gestire. Di certo, quando mi capitasse di cambiare Sim, avr&#242; assai meno attenzione per le offerte del gestore citato nel titolo. Non ho problemi con il web lento, anzi, viva <a href="http://habilis.net/lynxlet/">Lynxlet</a>. Il server di posta per&#242; deve essere efficiente e il prezzo chiaro; se ricevere un servizio costa di pi&#249;, fatemelo sapere subito, oppure adeguate il servizio. Fare finta che costi poco per acchiappare il gonzo funziona solo una volta.