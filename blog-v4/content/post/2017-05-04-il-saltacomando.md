---
title: "Il saltacomando"
date: 2017-05-04
comments: true
tags: [Hotspot, Personal, iOS]
---
Sentita gratitudine a chi avesse già spulciato nella documentazione e fosse in grado di spiegarmi tecnicamente con quale criterio la funzione di hotspot personale di iOS appare nella prima schermata delle Impostazioni, oppure nella prima schermata come funzione con il pulsante rimandato alla seconda schermata, o ancora nascosta dentro la funzione Cellulare.