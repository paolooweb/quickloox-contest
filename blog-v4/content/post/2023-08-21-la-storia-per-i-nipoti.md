---
title: "La storia per i nipoti"
date: 2023-08-21T01:48:05+01:00
draft: false
toc: false
comments: true
categories: [Hardware]
tags: [iPhone, iPad]
---
…E faceva caldo al punto che, nella zona normalmemte destinata alla ricarica dei *device*, gli apparecchi suddetti [non volevano caricarsi](https://support.apple.com/en-us/HT213821#:~:text=With%20iOS%2016%20and%20iPadOS,%5D%20returns%20to%20normal%20temperature.%22), con tanto di avviso, in attesa di una riduzione della temperatura.

Ovvio e naturale che succeda, però su tutti gli apparecchi insieme dovevo ancora vederlo.

Lo abbiamo usato come pretesto per introdurre un purificatore d’aria [Purifier Cool AutoReact](https://www.dyson.it/trattamento-aria/purificatori-ventilatori/purifier-cool-autoreact-tp7a) di Dyson. Intercetta le partcelle sospese, fornisce una analisi della qualità dell’aria e abbassa di qualcosina la temperatura.

Ora abbiamo degli oggetti di design, con tecnologia superiore e funzioni bene implementate, come prima; tuttavia in caso di caldo siamo sicuri che almeno uno di essi non darà problemi.