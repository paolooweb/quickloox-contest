---
title: "Guardare e non scattare"
date: 2021-09-28T01:33:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [iPhone, Austin Mann, iPhone 13 Pro] 
---
Ogni volta che leggo come, sì, iPhone funzioni bene, ma per scattare foto veramente belle serva una vera fotocamera, vado a vedere una recensione di Austin Mann.

Quest’anno si è fatto [un giro in Tanzania](https://austinmann.com/trek/iphone-13-pro-camera-review-tanzania) e francamente, per quanto abbia usato iPhone 13 Pro come fotocamera, sembra che qualche immagine bella l’abbia ottenuta.

Enunciato il truismo, ovvero basta dare un’occhiata alle ottiche di un iPhone per capire come una fotocamera abbia possibilità di partenza diverse, sarebbe ora di guardare al concreto, ovvero che scatti vengono davvero prodotti oltre la pura teoria.

Soprattutto, quanto influisce la fotografia computazionale nel risultato finale. Perché è vero che l’ottica di qualità ha sempre battuto l’elaborazione del computer; è anche vero che forse le cose iniziano a cambiare.

E ogni tanto quelli della fotocamera mi sembrano più snob che specialisti. Non tutti, eh, ci mancherebbe.

*Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile.*