---
title: "Lezione capovolta"
date: 2024-02-07T18:30:59+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [SimpleText, Tom Dowdy]
---
Definire editor di testo [BBEdit](https://www.barebones.com/products/bbedit/bbedit15.html) è in effetti riduttivo; definire editor di testo SimpleText sarebbe stato esagerato.

Da non confondersi con [quello dalla t minuscola](https://simpletext.app), SimpleText era… testo e basta. Una finestra e ci si poteva scrivere dentro, salvare un file.

Su un Macintosh di trent’anni fa poteva comunque avere un senso, perché a volte era l’unico in grado di aprire file danneggiati o di applicazione sconosciuta e poi aveva un incredibile, per i tempi, limite di trentadue kilobyte. Tantissimo.

Così, in uno o in un altro momento, tutti abbiamo finito per usarlo e ricordiamo la sua icona per i documenti: la prima pagina di un giornale con il titolo a caratteri cubitali *EXTRA!* (disegnata in bianco e nero su trentadue pixel per lato).

Nessuno si è ricordato della *testata* dell’icona documenti di SimpleText: come si chiamava il finto giornale?

C’è voluto un po’ di tempo ma [qualcuno ci è riuscito](https://xoxo.zone/@robotspacer/111868881447239341): è il cognome dell’addetto alla manutenzione del programma in Apple, [Tom Dowdy](https://www.engineersneedart.com/blog/dowdy/dowdy.html), scritto capovolto, per giunta in un periodo nel quale gli *easter egg* non ricevevano la stessa benevolenza di un tempo.

Leggenda vuole che a Dowdy sia stata assegnata il venerdì la soluzione di un bug di TeachText (il programma predecessore) e che lui il lunedì mattina abbia consegnato SimpleText.

Una bella lezione di ingegno, inventiva, leggerezza, capacità di lasciare un segno anche in un trentadue per trentadue.