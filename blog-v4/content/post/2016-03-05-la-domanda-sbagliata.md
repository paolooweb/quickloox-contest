---
title: "La domanda sbagliata"
date: 2016-03-05
comments: true
tags: [Apple, Dediu]
---
Come scriverà fino allo sfinimento, o fino a quando le cose resteranno quelle che sono, seguire Apple è interessante perché è un’azienda diversa da molte altre. Se si considerano le dimensioni, oltretutto, veramente diversa: semplice rompere gli schemi e sfuggire all’ovvio se sei piccolo e outsider, più complesso se sei un’azienda da duecentotrentacinque miliardi di fatturato, qualcosa che neanche si riesce a visualizzare con chiarezza nella mente.<!--more-->

Per esempio, [scrive Horace Dediu su Asymco](http://www.asymco.com/2016/02/16/priorities-in-a-time-of-plenty/) Apple viene amministrata – nonostante queste dimensioni da capogiro – con un singolo conto profitti e perdite. Ed è ancora più interessante pensare all’allocazione delle risorse ai vari progetti, in questo contesto, con queste cifre.

Dalla lettura emergono cose veramente interessanti, prima fra tutte che la guida del *management* possa essere non tanto la ricerca del maggiore profitto, ma del migliore rapporto tra numero e soddisfazione dei clienti. E che il profitto diventi una banale conseguenza di questo approccio, invece che la linea guida.

Altro filone che salta fuori è l’idea che Apple valuti e conduca il proprio *business* in base a metriche misurabili con una certa difficoltà, per esempio – appunto – la soddisfazione del cliente. Dove invece molte altre aziende cadono nella trappola di basarsi su certi parametri non perché siano quelli giusti, bensì perché sono facilmente misurabili.

Troppe aziende si impegnano a rispondere alla domanda sbagliata, ipotizza Dediu, solo per il fatto che è facile dare la risposta.

La lettura è d’obbligo, per chi sia mai stato coinvolto in qualche scambio di opinioni alla fine del quale sia uscito che *pensano ai soldi come tutti gli altri*. Mentre invece c’è modo e modo, e molta diversità.