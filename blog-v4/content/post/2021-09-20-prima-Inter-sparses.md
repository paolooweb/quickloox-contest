---
title: "Prima inter sparses"
date: 2021-09-20T02:37:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [Apple, Amd, Arm, Google, M1, Intel, Micr0soft] 
---
Google annuncia l’arrivo in autunno di [computer da tasca Pixel equipaggiati con processore fatto in casa](https://asia.nikkei.com/Business/Technology/Google-unveils-first-in-house-chip-to-power-new-Pixel-phones). Nel 2023 arriverebbero anche [i primi Chromebook](https://asia.nikkei.com/Business/Tech/Semiconductors/Google-developing-own-CPUs-for-Chromebook-laptops).

Amd, intanto, non sembra intenzionata a produrre chip Arm ma ritiene opportuno fare sapere che, alla bisogna, [è pronta a farlo](https://www.tomshardware.com/news/amd-we-stand-ready-to-make-arm-chips).

Intel [intende produrre chip Arm per conto terzi](https://www.businesswire.com/news/home/20210323005981/en/Intel-CEO-Pat-Gelsinger-Announces-), nel quadro di una strategia di rilancio per uscire dalla attuale crisi strategica.

La notizia di Microsoft che progetta chip Arm [data a Natale scorso](https://www.bloomberg.com/news/articles/2020-12-18/microsoft-is-designing-its-own-chips-for-servers-surface-pcs?sref=SvwPxqpB).

Apparentemente una intera industria è al lavoro per colmare il ritardo che ha scoperto di avere nei confronti di una aziendina spesso accusata di non fare più innovazione da quando se ne è andato il titolare.

*Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile.*