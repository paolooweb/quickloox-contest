---
title: "Vecchie tecnologie"
date: 2015-05-26
comments: true
tags: [Pavia, Inoltre, Html]
---
[Sono stato a Pavia](https://macintelligence.org/posts/2015-05-22-ma-tutto-questo-da-lunedi/) e sono stato bene, per una bella serata a parlare di mondo del lavoro in un incontro indetto da una [associazione universitaria](https://thenoisefrompavia.wordpress.com/tag/associazione-inoltre/) animata da entusiasti.

Il tempo a disposizione in queste situazioni è minimo rispetto alle cose da dire e quando parlo tendo a dilungarmi. Non so se sono stato incisivo oppure se ho finito per molare la pazienza degli astanti.

Ho comunque fatto presente certe necessità urgenti che un giovane universitario dovrebbe tenere in considerazione in vista del confronto con il mondo del lavoro. L’assurdità del curriculum europeo che uniforma tutti e quindi soffoca chi meriterebbe di emergere, l’importanza di una formazione trasversale tra materia scientifica e umanistica, la conoscenza di Html, l‘uso dei social network non superficiale, per essere padroni del mezzo e uscire dalla massa indistinta.

Non sai mai, evangelicamente, dove cadrà la sememza, se sul cemento, in un tombino oppure sul terreno fertile. Certo mi ha colpito come, anno 2015, ambiente universitario, per molti queste siano nuove tecnologie e non si stava parlando di OpenStack, ma di Html.

Sono tecnologie vecchie, già lì da vent’anni, e chi non le abbia almeno sfiorate farebbe bene a porsi qualche domanda, specie e soprattutto chi è più giovane di Html.