---
title: "Tre segni"
date: 2014-10-26
comments: true
tags: [FitBit, Apple, AppleSim, ApplePay, AT&T, CurrentC, Wallet, Nfc, Gandhi]
---
Un sito partner sbaglia qualcosa e [pubblica con largo e inopportuno anticipo](http://www.theverge.com/2014/10/25/7068971/brookstone-leaks-fitbit-surge) la descrizione di un nuovo bracciale della salute che FitBit farà uscire nel 2015. Due delle novità sono l’inclusione di un orologio e un sensore a infrarossi per leggere il ritmo cardiaco.<!--more-->

Negli Stati Uniti e nel Regno Unito, iPad è disponibile con Apple Sim, che permette di cambiare operatore di Internet cellulare e tenere sempre la stessa scheda. Si scopre che uno degli operatori aderenti all’iniziativa, AT&T, [blinda la Sim](http://support.apple.com/kb/HT6499) se viene scelta una delle sue offerte, e non consente più altri cambi di operatore.

Un consorzio di aziende di vendita al dettaglio cerca di mettere a punto un proprio sistema di pagamento elettronico, detto CurrentC, e da quando è arrivato [ApplePay](https://www.apple.com/apple-pay/) [fa spegnere nei negozi il lettore Nfc](http://www.theverge.com/2014/10/25/7069863/retailers-are-disabling-nfc-readers-to-shut-out-apple-pay) (su cui si basano anche [Google Wallet](https://www.google.com/wallet/) e altri).

Anche se [l’attribuzione a Gandhi è fantasiosa](http://www.csmonitor.com/USA/Politics/2011/0603/Political-misquotes-The-10-most-famous-things-never-actually-said/First-they-ignore-you.-Then-they-laugh-at-you.-Then-they-attack-you.-Then-you-win.-Mohandas-Gandhi), la citazione sta bene ugualmente:

>Prima ti ignorano. Poi ridono di te. Poi ti attaccano. Poi vinci.

Tre segni che Apple si trova nella fase tre.