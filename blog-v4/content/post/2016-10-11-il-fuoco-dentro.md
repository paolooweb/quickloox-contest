---
title: "Il fuoco dentro"
date: 2016-10-11
comments: true
tags: [Samsung]
---
Si torna su Samsung e sui suoi Galaxy Note7, che avevamo [lasciato a terra](https://macintelligence.org/posts/2016-09-15-sorvoli-problematici/), con una notizia istruttiva.<!--more-->

Visto che i Note7 avevano una tendenza eccessiva a prendere fuoco, sono stati richiamati e sostituiti con altre unità Note7, ben distinte nell’interfaccia utente, con marchi visibili sull’apparecchio.

[Prendono fuoco anche loro](https://news.samsung.com/global/samsung-will-ask-all-global-partners-to-stop-sales-and-exchanges-of-galaxy-note7-while-further-investigation-takes-place). Samsung ha chiesto di interromperne la vendita fino a che ci sono approfondimenti di inchiesta. Eufemismo per dire che prima di vedere strombazzati in giro altri Note7 in tivù o sui giornali, passerà del tempo.

Nel frattempo vorrei dire ai felici possessori di un Note7, felici per averlo pagato così poco, che sono solidale con loro e capisco le loro ambasce. Se poi volessero evitarmi come la peste in metropolitana o al cinema, e sedersi lontano da me con i loro aggeggi in tasca, sarei loro estremamente grato.

Li invidio. Hanno il fuoco dentro. Qualcuno anche fuori.