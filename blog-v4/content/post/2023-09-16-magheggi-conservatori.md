---
title: "Magheggi conservatori"
date: 2023-09-16T02:19:45+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [Wizardry, Wizardry Proving Grounds of the Mad Overlord, Digital Eclipse, Apple II]
---
Certo, sarebbe bello se qualcuno si prendesse cura di [Wizardy: Proving Grounds of the Mad Overlord](https://en.wikipedia.org/wiki/Wizardry), il primo videogioco di ruolo single user con il concetto di *party*.

Viene fuori che Digital Eclipse non ha detto niente a nessuno e ha lavorato sul gioco originale per lasciare intatta la trama e i personaggi e però inserire piacevolezze da mondo moderno a livello grafico e di interfaccia, con abbondanza di 3D e immaginario visivo. Per poi mettere in vendita il risultato, dal [proprio sito](https://www.digitaleclipse.com/games/wizardry) e poi tramite [Steam](https://store.steampowered.com/app/2518960/Wizardry_Proving_Grounds_of_the_Mad_Overlord/), dunque anche per Mac, e [Gog](https://www.gog.com/en/game/wizardry_proving_grounds_of_the_mad_overlord).

Così un capolavoro dei primordi, che ha definito per la prima volta la modalità di gioco con più personaggi coordinati contro i mostri del sotterraneo, può farsi apprezzare anche da chi l’Apple II lo ha visto solo in cartolina. Sì, dato che in quegli anni qualcosa di innovativo, se usciva, lo faceva su Apple II.

E la filologia? E l’interfaccia originale, il clima di gioco del tempo, l’immaginazione che sublima la grafica minimale? Un tasto e *Wizardry* si mostra a schermo esattamente come faceva su Apple II.

Questo si dice mettere insieme conservazione e progresso sul software per giocare.

Aggiungo come nota personale che con questo annuncio le mie chance di battere il Mad Overlord si alzano decisivamente, da nulle a minime; ai tempi ho giocato a Wizardy pochissimo e ne ho prese il tradizionale sacco più sporta.