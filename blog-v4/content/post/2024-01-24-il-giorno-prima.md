---
title: "Il giorno prima"
date: 2024-01-24T00:29:56+01:00
draft: false
toc: false
comments: true
categories: [Hardware, Internet]
tags: [Macintosh, Riccardo Luna, Luna, Repubblica]
---
Non mi do pena di commentare il [quarantesimo anniversario di Macintosh](https://mac40th.com). Ci pensa già mezzo mondo e non saprei che aggiungere di nuovo o migliore. Oltretutto è materiale che interessa se si ha una certa età.

Mi ha fatto un po’ pena invece *Repubblica*, il quotidiano, che ha pubblicato un articolo commemorativo a firma di Riccardo Luna, *ieri*.

Ieri non era il quarantesimo. Se qualcuno venisse a farmi gli auguri il giorno prima del compleanno mi farebbe piacere sulle prime, poi penserei che si tratta di qualcuno ignaro della data vera. Oppure, per dire, sa di dover passare il giorno dopo in missione spaziale e allora fa gli auguri nell’unico momento possibile.

Non credo che Luna avesse impedimenti e penso che *Repubblica* sia al lavoro mentre scrivo per uscire anche oggi. Perché non festeggiare il quarantesimo nel giorno del quarantesimo? Volevano battere sul tempo il tempo? Avevano un buco da riempire all’ultimo? Usano un calendario diverso? Non so. Non lo linko, l’articolo.

Uno dice ma insomma, anche Jason Snell presenta [il podcast con i grandi nomi](https://www.relay.fm/upgrade/496) il ventidue gennaio.

Certo, ma è un podcast. Uno lo ascolta quando vuole, magari tra dieci giorni. Non sarà stato facile mettere insieme Dan Moren, John Siracusa, John Gruber, Stephen Hackett e Shelly Brisbin e magari era l’unica data possibile. È diverso dall’andare in edicola ogni giorno e parlare degli avvenimenti del giorno; non del giorno dopo.

Mentre riflettevo ho pensato a come deve essere stato *il giorno prima* della presentazione di Macintosh. A cosa passava per la testa di Steve Jobs mentre pregustava la soddisfazione di una vita, pronto a [presentare la sua creazione più grande come nessuno aveva mai fatto prima](https://www.youtube.com/watch?v=2B-XwPjn9YY).

(Certo, i tempi sono cambiati: oggi su Facebook una persona a me cara scriveva tristemente, per me almeno, *che bello PowerPoint che scrive le presentazioni da solo*. Ma è un altro discorso).

Tutti i maghi del software e dello hardware che avevano lavorato per anni a Macintosh come forsennati, privi di sonno e di cibo, capaci di superare una necessità con una invenzione pazzesca, di farcela all’ultimissimo secondo con una pensata geniale, di avere sacrificato tutto a un obiettivo *insanely great*: chissà come si saranno sentiti, il ventitré gennaio millenovecentoottantaquattro, quando tutto stava per diventare realtà, se avesse funzionato come previsto.

Il giorno prima tutti sapevano che sarebbe arrivato Macintosh, grazie a uno [spot che ha fatto la storia](https://www.melablog.it/ridley-scott-prima-reazione-spot-apple-1984/), ma nessuno sapeva come sarebbe stato il computer. (Lo spot ha compiuto quarant’anni due giorni fa, il ventidue).

Il giorno prima erano tutti ad aspettare, da un lato o dall’altro della barricata, con emozione, trepidazione, batticuore, forse anche sollievo per avercela fatta.

L’articolo di Luna, del giorno prima, non ha nulla di ciò.

Il giorno prima bisogna lasciarlo a quelli che creano, che sperano, che inventano, che cambiano il mondo.

I giornalisti stanno al loro posto quando scrivono al momento giusto.

<iframe width="560" height="315" src="https://www.youtube.com/embed/2B-XwPjn9YY?si=0A8lLue19arFRvgV" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>