---
title: "Tempo di rinunce"
date: 2014-06-08
comments: true
tags: [Rai, Silverlight, Html5, Netflix, Yoesemite, OSX, plugin, Flash]
---
A parte i probabili interessi e favori reciproci che portano a decisioni del genere, lo scandalo della necessità del *plugin* Silverlight per vedere i programmi Rai da personal computer è vivo [da abbastanza tempo](https://macintelligence.org/posts/2009-02-19-il-silenzio-e-doro/) per entrare nell’elenco delle millanta storia di arretratezza bieca dei servizi pubblici italiani.<!--more-->

Dirigenti Rai: [Netflix](https://www.netflix.com/global), il primo distributore di programmi video al mondo (ancora assente dall’Italia salvo trucchetti per sottosviluppo nostro), sta passando all’erogazione via Html5, che rinuncia a Flash, Silverlight eccetera.

L’edizione di Safari di OS X 10.10 Yosemite sarà in grado di trasmettere ai numerosi abbonati americani la programmazione di Netflix senza alcun bisogno di estensioni, *plugin*, aggiunte, magheggi.

Su un iPad i programmi Rai si vedono regolarmente senza *plugin* e questo, dirigenti Rai, mostra che il re è nudo. Le questioni tecniche non si pongono (lo sapete fare), quelle di diritti non si pongono (altrimenti iPad non potrebbe). Ce ne sono altre che non conosciamo? Sarebbe un bell’atto di trasparenza fare sapere. Trasparenza dovuta.