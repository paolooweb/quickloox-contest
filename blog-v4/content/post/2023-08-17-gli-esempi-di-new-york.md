---
title: "Gli esempi di New York"
date: 2023-08-16T00:57:30+01:00
draft: false
toc: false
comments: true
categories: [Software, Internet]
tags: [New York, TikTok, New York Times, NYT, AI, intelligenza artificiale]
---
La municipalità di New York [ha messo al bando TikTok sui suoi apparecchi](https://www.theverge.com/2023/8/16/23834579/nyc-tiktok-ban-new-york-china-surveillance-spy).

Il quotidiano più noto di New York [ha proibito alle sedicenti intelligenze artificiali di usare il suo contenuto a scopo di addestramento](https://www.theverge.com/2023/8/14/23831109/the-new-york-times-ai-web-scraping-rules-terms-of-service).

La questione è una e centrale, il valore e il rispetto dei dati altrui. Quello che fanno i social è noto ma non abbastanza; quello che fanno i modelli linguistici è uno scandalo che aspetta di esplodere, non tanto per quello che avviene, ma per come. Apple, che [ha imposto su App Store di chiedere esplicitamente l’autorizzazione al tracciamento](https://macintelligence.org/posts/2020-10-07-advert-different/), da questo punto di vista è avanti dieci anni.

*Posso usare i tuoi contenuti per addestrare il mio modello a scopo commerciale?* Quando lo scopo è commerciale, la domanda va posta, per forza.

Teniamo d’occhio le notizie da New York.

*Gli aggiornamenti in agosto del blog potrebbero essere irregolari o infrequenti.*