---
title: "Vent’anni di libera civilizzazione"
date: 2015-11-14
comments: true
tags: [FreeCiv, Mac, iPhone, iPad]
---
Continuo a ribadire che mi pesa la [mancanza di una vera edizione Mac per FreeCiv](https://macintelligence.org/posts/2014-11-28-posizioni-vacanti/).<!--more-->

Cionostante va festeggiato il ventennale del programma, che ricorre proprio oggi. Il sito ufficiale contiene un’[intervista ai tre fondatori](http://play.freeciv.org/blog/2015/11/freeciv-founded-20-years-ago-today/) e va sottolineato come siano ben pochi i giochi ad avere [porte logiche ufficialmente dedicate](https://www.iana.org/assignments/service-names-port-numbers/service-names-port-numbers.xhtml?search=5556).

Altre cosa bella di FreeCiv è che è [giocabile via *browser*](https://play.freeciv.org), su *qualunque* computer, compreso un iPhone o un iPad.

Propongo a tutti di sostenere FreeCiv con una donazione anche piccola, o altrimenti giocando una partita per segnalare una persona interessata in più.

C’è un motivo in più per fare tutto questo proprio oggi: il motto del gioco è *perché la civilizzazione dovrebbe essere libera*. Soffriamo di un eccesso di gente che vuole imporre ad altri la propria inciviltà e possiede una cultura estranea al gioco. FreeCiv è un bel pretesto per rimarcare la distanza.