---
title: "Mantenere le distanze"
date: 2021-09-24T00:00:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [Google Remote Desktop, Screens, Vnc, port forwarding] 
---
Oggi mi serve imprevistamente l’accesso al mio Mac che si trova a casa, mentre io no.

Non ricordavo più che Google Remote Desktop ha un bug fastidiosissimo che disloca il puntatore: il clic sullo schermo remoto avviene in un punto diverso da quello toccato in locale.

Al che ho rotto gli indugi e non avendo il tempo materiale di allestire una connessione VNC come si deve, con il port forwarding e un IP pseudostatico, ho comprato [Screens](https://edovia.com/en/screens-mac/) per [iOS e iPadOS](https://edovia.com/en/screens-ios/).

Il primo esperimento ha dato esito positivo anche se l’installazione della parte Mac dà un errore: dice che c’è una configurazione di *carrier-grade NAT* e si ferma. Tradotto, il mio provider adotta un certo meccanismo di distribuzione degli indirizzi IP dinamici che Screens affronta non sempre efficacemente.

In realtà, nonostante l’errore, la componente Mac è attiva e sembra che tutto funzioni.

Incrocio le dita per più tardi, quando mi serve davvero.

*Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile.*