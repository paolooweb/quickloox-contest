---
title: "Notizie che restano"
date: 2015-07-17
comments: true
tags: [TechCrunch, ITProPortal, GsmArena, PhoneArena, PocketGamer, AppStore]
---
L’8 giugno TechCrunch scrive che App Store ha passato il [milione e mezzo di app](http://techcrunch.com/2015/06/08/itunes-app-store-passes-1-5m-apps-100b-downloads-30b-paid-to-developers/). Lo scrive anche [PhoneArena](http://www.phonearena.com/news/Apples-App-Store-now-offers-1.5-million-apps_id70229).

Il 15 luglio, l’altroieri, GsmArena scrive che App Store ha passato il [milione e mezzo di app](http://www.gsmarena.com/apples_app_store_now_has_over_15_million_available_apps_in_total-blog-13057.php). Lo scrive anche [ITProPortal](http://www.itproportal.com/2015/07/15/apple-app-store-milestone/).

Il mezzo 37 giorni durante i quali, suppongo, la Terra ha cessato di ruotare attorno al proprio asse.

L’alternativa è che ci siano redazioni composte da nonmorti che vengono evocati nelle notti di luna piena, una volta al mese (lunare). È l’ipotesi più realistica, visto che a oggi, [riporta PocketGamer](http://www.pocketgamer.biz/metrics/app-store/), le *app* attive sono 1.768.841. Che ne siano state approvate ducecentocinquantamila in due giorni, pare esagerato.