---
title: "Aggiornamento lento"
date: 2021-11-09T00:58:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [iPhone, iPad, iOS, iOS 15.1, Monterey] 
---
Per principio, non mi occupo degli aggiornamenti di sistema, che possono essere automatici e quindi trovo poco sensato inserire nella mia agenda di cose da fare manualmente.

Un effetto collaterale è che, come è appena accaduto, sia arrivato solo ora su iPad Pro e iPhone la notifica dell’aggiornamento di sistema pronto da installare.

Aggiornamento che, si badi bene, avrebbe voluto essere iOS 14.8.1; ma un pulsante permetteva di andare direttamente a [iOS 15](https://www.apple.com/it/ios/ios-15/) e così ho fatto. Aspettando che lo proponessero le macchine, iOS 15 mi è arrivato già completo di aggiornamento a 15.1, il che mi ha fatto risparmiare una procedura.

Prime impressioni, positive: l’operazione è stata indolore e, fatta in pausa pranzo, praticamente neanche me ne sono accorto. Tutto quello che ho provato funziona. La app Impostazioni su iPad è andata in crash la prima volta che ho chiesto di cambiare la dimensione delle icone nelle schermate, poi ha funzionato bene e non ha più dato problemi.

I widget sullo schermo sono molto interessanti e sto cominciando a giocarci per trovare una configurazione funzionale e piacevole; ho sempre tenuto una configurazione abbastanza statica di icone su iPad, che mi fornisce il vantaggio della memoria dei polpastrelli; le app potrei azionarle a occhi chiusi, perché so a memoria dove si trovano. Mi rendo conto però che i widget possono impattare seriamente e in senso buono sull’amministrazione delle schermate di iPad e penso che mi convenga entrare nella logica.

Home mi ha permesso di entrare nella tv di casa senza altre configurazioni ed è stata un’altra piccola però piacevole sorpresa.

All’apertura, Mail mi ha proposto la funzione *nascondi la mia email* e ho accettato. Vediamo se c’è un’effettiva limitazione della posta fastidiosa.

Nient’altro di particolare per ora. Mi rimetto in attesa, questa volta di Mac, che prima o poi proporrà Monterey.

*Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile.*