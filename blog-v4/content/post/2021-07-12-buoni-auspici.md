---
title: "Buoni auspici"
date: 2021-07-12T01:53:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [Wimbledon, RaiPlay, iPad, Europei] 
---
Da anni, con due figlie piccole, non sono padrone dei miei schermi. Ma le figlie crescono e oggi ho fatto una vera indigestione, con due set della finale del tennis e quasi tutta la finale del calcio (di cui sono anche riuscito a [confondere la data](https://macintelligence.org/posts/Prega-per-la-loro-redenzione.html), per via del caldo, suppongo).

Dopo il primo set, non avrei messo due centesimi su un esito della prima finale diverso da quello effettivo e purtroppo avevo ragione.

Sul calcio ero del tutto privo di competenze. Ma a un certo punto dei supplementari ho dovuto spostarmi su iPad. Ho lanciato RaiPlay, per giunta via Safari, neanche la app, e dopo due false partenze _ha funzionato_.

Un’aurora boreale tricolore sarebbe sarebbe stata meno convincente, come buon  auspicio.

*Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile.*