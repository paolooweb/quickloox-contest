---
title: "Smetto quando voglio"
date: 2019-01-27
comments: true
tags: [iPad, Pro, mouse, Motherboard, mouse]
---
Già scritto [ieri](http://www.macintelligence.org/blog/2019/01/26/lo-spessore-di-un-anniversario/) che oggi è l’anniversario di iPad… ma sarà un computer oppure no?

Solo dei nostalgici possono mettersi a fare polemica, specialmente dopo lo spot [What’s a Computer](https://www.youtube.com/watch?v=pI-iJcC9JUc).

<iframe width="560" height="315" src="https://www.youtube.com/embed/pI-iJcC9JUc" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

A parte ovviamente Microsoft e [i suoi spot](https://www.youtube.com/watch?v=VceO0OPw3Mk) tesi a ridicolizzare iPhone e iPad. Con iPhone si è visto come è andata a finire.

<iframe width="560" height="315" src="https://www.youtube.com/embed/VceO0OPw3Mk" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Il punto vero dell’innovazione di iPad è un altro, svelato da questo titolo di *Motherboard*: [Ho smesso di usare il mouse per una settimana ed è stato sorprendente](https://motherboard.vice.com/en_us/article/d3m8ga/i-stopped-using-a-computer-mouse-for-a-week?utm_campaign=sharebutton).

È da anni che uso Mac senza mouse; faccio tre quarti delle cose con la tastiera, il resto con un trackpad. Probabilmente sono uno che usa pure troppo il trackpad. Non è una novità.

O meglio: lo è per gente abituata a pensare al mouse come una cosa necessaria. Nel 1989 lo era. Trent’anni dopo, non lo è più e iPad lo mostra meglio di qualsiasi altra cosa.