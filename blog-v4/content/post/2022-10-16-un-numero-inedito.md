---
title: "Un numero inedito"
date: 2022-10-16T03:56:57+01:00
draft: false
toc: false
comments: true
categories: [Hardware]
tags: [PowerBook Quattro, Dana Sibera]
---
Ignoravo totalmente l’esistenza di [PowerBook Quattro](https://twitter.com/NanoRaptor/status/1581276876667949058), una macchina con quattro processori G3 a bordo, ciascuno capace – messo nelle giuste condizioni – di lanciare un sistema operativo indipendentemente dagli altri tre.

<blockquote class="twitter-tweet"><p lang="en" dir="ltr">The PowerBook G3 Quattro was fitted with four 300Mhz PowerPC G3 processors, enabling it to boot four operating systems at once. A favourite of admins who wanted to show off, working examples rarely appear for sale – most are still in active use. <a href="https://t.co/7j4PwXywkM">pic.twitter.com/7j4PwXywkM</a></p>&mdash; Dana Sibera (@NanoRaptor) <a href="https://twitter.com/NanoRaptor/status/1581276876667949058?ref_src=twsrc%5Etfw">October 15, 2022</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

Chissà se sia vero che la maggior parte degli esemplari funzionanti è ancora in uso attivo. Chissà se sia vero che sia esistito un PowerBook Quattro. Se non ci fosse stato avrebbero dovuto inventarlo. 😉🤓