---
title: "La fonte dell’ispirazione"
date: 2013-05-27
comments: true
tags: [Markdown, Script]
---
Scopro che esiste <a href="https://github.com/jgfc/Textdown/#textdown">Textdown</a>,  editor di testo <a href="http://daringfireball.net/projects/markdown/">Markdown</a> per <a href="http://daringfireball.net/projects/markdown/">Chrome</a>.

Installo e collaudo. Niente di che, ma strumento carino e veloce, ben pensato. Mostra una pagina iniziale con istruzioni e riconoscimenti, dove è possibile leggere questo:

>Textdown è stato ispirato da due app solo Mac, <a href="http://mouapp.com">Mou</a> e <a href="http://bywordapp.com">Byword</a>. L’icona di Textdown è stata ugualmente ispirata da quella di <a href="http://www.textasticapp.com/">Textastic</a>.

Al che si capisce dove stanno l’innovazione e la creatività, dove l’emulazione e il farsi trainare.

Tra l’altro si può avere un editor Markdown anche dentro <a href="http://www.apple.com/it/safari/">Safari</a> (o qualsiasi altro <i>browser</i> moderno): <a href="https://draftin.com">Draft</a>.