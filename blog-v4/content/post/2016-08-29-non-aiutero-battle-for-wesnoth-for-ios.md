---
title: "Non aiuterò Battle for Wesnoth per iOS"
date: 2016-08-29
comments: true
tags: [Wesnoth, Wikipedia]
---
Sui forum di uno dei più bei giochi di strategia a turni open source (anche) per apparecchi Apple è apparso [l’annuncio esplicito](https://forums.wesnoth.org/viewtopic.php?p=600463#p600463):

>un programmatore che aiuti il porting di Wesnoth su iOS.

Non sarò io per ragioni spiegate molto chiaramente [sul sito](https://www.wesnoth.org):

>Ricordare che non si tratta di un compito da intraprendere con attitudine dilettantesca. Cerchiamo qualcuno capace ed esperto nello sviluppo di app che sia disposto a dedicarsi a questo progetto sul lungo termine, invece che qualcuno pronto a considerarla un’occasione per farsi le ossa.

Va notato che, chiunque assumesse l’incarico, incasserebbe anche parte dei proventi della vendita del gioco su App Store (mentre il resto finanzierebbe il progetto generale di Battle for Wesnoth). Al momento esistono su App Store due versioni del gioco, [una a pagamento](https://itunes.apple.com/it/app/battle-for-wesnoth-hd/id575852062?l=en&mt=8) e basata su codice arretrato (anche [per iPhone](https://itunes.apple.com/it/app/battle-for-wesnoth/id575239775?l=en&mt=8)), l’altra [gratis](https://itunes.apple.com/it/app/nordicbattle-battle-wesnoth/id1071230012?l=en&mt=8), apparsa da pochi mesi, orrenda per come è sfigurata dalla pubblicità. Un vero abuso ai danni di un grande progetto.

Appunto, non sarò io a curare Wesnoth per iOS. Però, se qualcuno legge ed è interessato, potrebbe essere lui (o lei, o loro).

Già che ci sono: certi problemi occorsi con gente che in passato si è assunta il compito ma non si è comportata benissimo sono stati raccontati su Wikipedia [in modo completamente mistificatorio](https://en.wikipedia.org/w/index.php?title=Frogatto_%26_Friends&diff=prev&oldid=629081226). A riprova della credibilità generale dell’iniziativa, [che è bassa](https://macintelligence.org/posts/2015-11-04-very-good-very-bad/).