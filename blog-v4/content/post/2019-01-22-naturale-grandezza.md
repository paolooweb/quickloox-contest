---
title: "Naturale grandezza"
date: 2019-01-22
comments: true
tags: [Ive, Park, Apple, Fukasawa]
---
Dal riassunto della conversazione tra Jonathan Ive e il *designer* giapponese Naoto Fukasawa, [pubblicato da 9to5Mac](https://9to5mac.com/2018/12/29/jony-ive-naoto-fukasawa-design-interview/), si evince che durante la progettazione di Apple Park vennero realizzati prototipi *in scala 1:1*. Ovviamente non dell’intero edificio, ma di sue sezioni:

>Siamo riusciti a realizzare prototipi a grandezza naturale. Dato che l’idea era semplicemente di ripetere la stessa sezione, si poteva avere un’idea di che cosa si stava progettando.

Purtroppo (per me) l’intervista intera è [su carta e in giapponese](https://www.axismag.jp/posts/2018/12/111419.html). Però mi basta questo dettaglio. Quale altra azienda arriva a pensare in grande in questo modo?