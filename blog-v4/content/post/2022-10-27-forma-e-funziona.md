---
title: "Forma e funziona"
date: 2022-10-27T01:36:14+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [Freeform]
---
Freeform è uno degli [argomenti più di tendenza](https://mjtsai.com/blog/2022/10/26/freeform-beta/) e scusa se [ne accennavo a giugno](https://macintelligence.org/posts/2022-06-07-un-ambiente-per-trovarsi/). La riprova che dentro Apple c’è ancora chi sa fare bene ma, più ancora di questo, chi sa lasciare un segno. Se Freeform si afferma, diventa un segno distintivo di Mac e iPad, come MacWrite nell’antichità oppure oggi Numbers per iOS e iPadOS.

Speriamo solo che tutta questa pubblicità gli permetta di essere conosciuto e adottato più quello che è stato Numbers su iPhone e iPad.