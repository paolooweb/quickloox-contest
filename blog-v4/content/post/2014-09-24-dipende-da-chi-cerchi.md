---
title: "Dipende da chi cerchi"
date: 2014-09-25
comments: true
tags: [smartwatch, Pebble, Moto360, Watch]
---
Non sono esperto di orologi. Come per i cellulari, ne portavo uno da due soldi giusto per sapere l’ora. Ho smesso di portarlo quando mi sono reso conto che passavo le giornate di lavoro davanti a un computer, sul quale vedevo l’ora in ogni momento.<!--more-->

Non sono neanche esperto di computer da polso. Ancora non esistono. O meglio è uscito qualcosa, ma appare evidente che – Apple o meno – lo <em>smartwatch</em> che sia tanto <em>smart</em> quanto <em>watch</em> lo dobbiamo ancora vedere.

Di conseguenza sono rimasto sbigottito nel leggere l’[articolo di Jon Bell](https://medium.com/@ienjoy/september-8-2014-447fce0fc277), redatto prima degli annunci Apple. In questo passaggio:

>Per anni, apparecchi come Pebble sono stati progettati con una sagoma quadrata o rettangolare. Di recente abbiamo visto circolare smartwatch circolari, ma l’apparecchio con le recensioni migliori, Moto 360, non riusciva a riempire l’intera superficie circolare. La gente lo chiama Moto 270. Ho letto una dichiarazione di uno che lavora nel team del 360, secondo il quale riempire l’intera circonferenza è un compito ingegneristicamente impegnativo.

Non so che successo avranno o non avranno gli Apple Watch. Di certo potrei scommettere qualsiasi cifra sul fatto che *mai* uscirebbero con una parte di schermo vuota. E se fossero stati circolari, gli ingegneri a Cupertino avrebbero provato magari a spostare l’asse terrestre, pur di riempirli. Se Apple merita una fama, è esattamente quella di lavorare per rendere facili le sfide difficili e reclutare le persone migliori a questo scopo. Certo, poi magari costa un po’ di più. Ma lo schermo è pieno.
