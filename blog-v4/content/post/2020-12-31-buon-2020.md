---
title: "Buon 2020"
date: 2020-12-31
comments: true
tags: [Mud, Jobs]
---
Da questa scrivania (polso, tasca, divano con tavoletta) è stato un anno emozionante, intendo a parte l’ovvio. Mi sono sposato il giorno prima che iniziasse il lockdown regionale, le figlie crescono impetuosamente, sul lavoro sono successe tante cose, più e meno buone, ma con saldo nettamente positivo. La parte sportiva di me ha dovuto scendere a compromessi notevoli e onestamente non vedo l’ora che sia possibile tornare a fare attività di squadra in modo ragionevolmente sicuro. Anche questo è stato cambiamento.

Esito a fare auguri perché il pensiero comune è che questo sia stato un anno pessimo e ci sono caduto un paio di volte anch’io. Eppure, se mi guardo indietro con occhio imparziale, devo definirmi fortunato, fortemente fortunato. Speriamo tutti ovviamente in un 2021 migliore, ma in fondo è quello che cerchiamo tutti gli anni.

Questo blog spazia *dal mondo Apple all’universo digitale, in visualizzazione rapida dell’ovvio* e vedo davanti a me una cosa molto ovvia: Mac, iPad, iPhone e giù fino ad tv, sono stati sempre risorse fondamentali. Quest’anno, fondamentali al quadrato. La videoconferenza o il telelavoro c’entrano fino a un certo punto; quest’anno non c’è stato giorno senza del video con qualcuno, ma in realtà ne ho sempre fatto, semplicemente è stato più frequente.

Invece, devo dire che quelli che ho sempre considerato strumenti, quest’anno sono stati infrastruttura, vera e propria dorsale di casa e di lavoro.

Ripenso a quella presentazione di Steve Jobs che classificava Mac [*hub* digitale personale](https://yourstory.com/2016/09/digital-hub-strategy-apple-innovation). In realtà non è Mac e neanche il cloud, ma piuttosto una costellazione di interfacce che ci rapportano a un ecosistema. La visione generale, tuttavia, era corretta.

Anche quell’altra presentazione, [l’incrocio tra tecnologia e arti liberali](https://macintelligence.org/blog/2016/11/09/umani-e-algoritmi/). Dalle diverse attività del ramo familiare all’inizio della scuola, non è mai stato così vero come quest’anno.

Di sicuro l’emergenza sanitaria mi ha fatto guardare con occhi diversi a tante cose e riconsiderarle, a partire dalla loro priorità. Alla fine di questo 2020 posso dire una cosa: l’ecosistema Apple è stato per me prezioso più che in ogni altra fase della vita e della carriera. Non parlo di hardware o software; oggi ho una dotazione generale quasi nuova mentre tre anni fa la avevo vecchissima e andava da dio, non è questione di denaro o di aggiornamenti. Parlo di semplicità e comodità d’uso, facilità nel perseguire gli obiettivi, accesso a tecnologie abilitanti, possibilità di pensare a quello che voglio ottenere invece che alla macchina con cui lo vorrei fare.

Tutto questo è stato determinante, per me. Sottolineo il dato personale: ci sarà chi si è trovato benissimo con altri sistemi. Io trovo che certi divari, contrariamente a quanto si tende a leggere sui social, siano aumentati. Parere del tutto soggettivo.

Va beh, se vado avanti ancora tiro mezzanotte e comprometto il brindisi. Non voglio fare auguri per il futuro ma per il passato: spero che ognuno possa fare un bilancio positivo del proprio 2020, nonostante tutto. Per il futuro, per domani mattina, nessun augurio, piuttosto un appuntamento: ritroviamoci, continuiamo a camminare insieme, a costruire insieme, a programmare insieme, a condividere idee e buone notizie.

Io metterò le figlie a dormire e poi mi infilerò in un Mud qualunque fino a che riesco a stare sveglio (accetto appuntamenti!). Domattina uscirò un po’ come la proverbiale marmotta, appena disturbato dalla luce del (tardo) mattino, e proverò a guardarmi intorno.

Ci sarà molto da fare. Del resto le giornate si allungano.

A presto, all’anno prossimo.