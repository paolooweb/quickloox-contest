---
title: "Regola in carte"
date: 2015-08-11
comments: true
tags: [Clash, Hearthstone, Forbes, Twitch, Trump]
---
Dopo [Clash of Clans](https://macintelligence.org/posts/2015-08-10-sotto-la-doccia/), una parola su [Hearthstone](http://eu.battle.net/hearthstone/it/?-), che se bisogna scegliere un gioco *freemium* giocabile gratis con opzione di pagamento, continuo nettamente a preferire (per quanto stia compiendo lenti passi avanti con il mio villaggetto e abbia anche respinto con successo un ottuso invasore).<!--more-->

La miscela di semplicità iniziale e complessità di approfondimento di Hearthstone è al momento senza rivali e meccanismi come l’Arena e la rissa in taverna avvicinano anche chi non ha tempo e voglia per calibrare allo spasimo i propri mazzi di carte.

Anche qui serve pazienza per diventare competitivi senza spendere. Si può assolutamente fare e lo dimostra [Trump SC](http://www.twitch.tv/trumpsc) sul proprio canale Twitch, dove lo si può guardare giocare a livelli di eccellenza con carte gratis disponibili veramente per chiunque.

Di converso, su Forbes si racconta di come sia possibile [spendere 639 dollari in pacchetti di carte di Hearthstone](http://www.forbes.com/sites/insertcoin/2014/12/22/why-ive-spent-639-on-hearthstone-and-dont-regret-it/) e, senza idee o genio, ritrovarsi la strada per il successo tutt’altro che spianata.

Per quanti moraleggiano su questi meccanismi diabolici di generazione di profitto, ricordo che esistevano le raccolte di figurine. Ne ho viste di scene, in edicola, e su iOS basta un pulsantino nelle Impostazioni per azzerare ogni rischio.