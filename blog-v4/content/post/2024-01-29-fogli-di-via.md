---
title: "Fogli di via"
date: 2024-01-29T15:43:03+01:00
draft: false
toc: false
comments: true
categories: [Internet, Software]
tags: [Spreadsheet, Ars Technica]
---
Abbiamo il fitness per il corpo, la meditazione per la mente, infinite modalità ibride del concetto; possiamo essere onnivori, vegetariani, paleo eccetera eccetera per curare l’alimentazione; salviamo, o così pare, il pianeta con raccolte differenziate, spese responsabili, scelte intelligenti; dai pavimenti ai sanitari, disponiamo di prodotti per curare, nutrire, mantenere, proteggere superfici e materiali. Non parliamo delle creme e delle lozioni per la pelle, il viso, i capelli, il contorno occhi. A poca distanza da casa c’è un autolavaggio che, se abbiamo un veicolo, non lo pulisce e basta: lo fa risplendere.

Ci prendiamo cura, stiamo attenti ai dettagli, manuteniamo e teniamo in ordine *tutto*.

Poi siamo di fronte a una tabellina quattro per tre e per crearla lanciamo un programma dalla potenza spropositata, da mostrare in un PDF dove si vedono duecento caselle anche se la tabella è da dodici, con la nomenclatura di righe e colonne in bella e inutile vista e magari i decimali con il punto perché siamo in Italia ma anche chi se ne frega e le intestazioni delle colonne convenientemente allineate a sinistra mentre i numeri sono a destra o così. Cura zero.

Perché ci prendiamo cura di ogni aspetto della vita e poi usiamo i fogli di calcolo in maniera sciatta e trascurata? Per risparmiare tempo? Perché non sappiamo come fare? Forse, ma la verità è che il programma incoraggia qualsiasi cattiva abitudine venga in mente.

I nodi prima o poi verranno al pettine, come si vede dalla pubblicazione di articoli come questo: [Continuiamo a commettere gli stessi errori con i fogli di calcolo, nonostante le conseguenze negative](https://arstechnica.com/science/2024/01/we-keep-making-the-same-mistakes-with-spreadsheets-despite-bad-consequences/). Si citano studi per i quali nel novanta percento dei fogli con più di centocinquanta righe esiste almeno un errore importante. Si spiega che l’uso dei fogli di calcolo è ubiquo perché tutti se ne trovano uno nel computer e che tutti gli utilizzi più scorretti (per esempio prentedere che siano database) rimangono impuniti, come è chiaro che sia quando un programma nasce disordinato per design, con il codice mescolato ai dati, l’invisibilità di quello che accade sotto il cofano, la difficoltà di lettura delle formule, la piaga delle macro usate a sproposito e che diventano veicolo di malware eccetera eccetera eccetera eccetera eccetera.

Esiste tutta una casistica che giustifica l’uso di un foglio di calcolo in maniera corretta. Per il resto, ne abusiamo, con spreco di tempo, energia elettrica (il processore lavora meglio con un milione di caselle in memoria o con il disegno in vettoriale della tabellina dentro il Pages della situazione?), rifiuto dell’eleganza e del senso estetico, attitudine a fare sempre la cosa più scorretta e abborracciata perché tanto sul foglio di calcolo vale tutto.

Sembra incredibile, ma il più delle volte non ci serve un foglio di calcolo. Sembra doppiamente incredibile, ma si lavora meglio, con migliore economia di dati, con più eleganza e maggiore precisione in quasi tutte le alternative.

Personalmente ho diradato l’uso di Numbers quasi a zero. A volte mi mandano un foglio di calcolo e chiaramente devo trattarlo come tale. Se sono io a creare il documento, succede praticamente mai.

Un giorno racconterò di quella volta (quattro o cinque in realtà) che ho dato alle stampe presso un editore una pubblicazione consistente in un database da migliaia di righe… che in realtà era un documento di testo tabulato, su cui grazie a poche semplici espressioni regolari trattavo senza problemi le operazioni su colonne e BBEdit mi forniva tutte le ricerche che servivano.

Mica devo fare scuola, c’è chi ha bisogno serio dei fogli di calcolo. Ma noialtri, che non ce ne facciamo niente o quasi, congediamocene appena possibile.