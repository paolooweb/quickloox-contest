---
title: "Non visto in TV"
date: 2014-12-18
comments: true
tags: [Munster, Apple, TV]
---
Fatico a crederlo: Gene Munster ha avuto il fegato di [apparire pubblicamente](http://9to5mac.com/2014/12/17/apple-television/) ad annunciare che Apple farà una televisione.<!--more-->

Il problema è che l’annuncio arriva ogni anno, dal 2011. Quest’anno con una novità: la scadenza non è il prossimo anno, ma il 2016.

Vorrei capire se è una manovra per riuscire a rifare la stessa predizione l’anno prossimo e dimostrare coerenza, oppure un modo per non farsi rinfacciare l’errore a fine 2015, oppure un sintomo della demenza senile.

Vorrei anche capire dove ho sbagliato. Ero convinto che i fatti contassero. Almeno qualcosa.

Invece è tutto ridicolo e fuori dal mondo come [quasi due anni fa](https://macintelligence.org/posts/2013-01-23-forza-panino/). E nessuno fa una piega.