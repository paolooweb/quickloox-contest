---
title: "Tandard e sformati"
date: 2020-12-11
comments: true
tags: [WordPress, Excel, Anteprima, LibreOffice]
---
Si avvicina la fine dell’anno e giustamente si smaltiscono le ultime assurdità.

Mi ero appena ripreso dal [plugin di WordPress che rifiuta i nomi file con il trattino](https://macintelligence.org/blog/2020/12/06/il-dado-e-trattino/).

Oggi ho un file Excel che si visualizza in modo anomalo su Numbers e pure su LibreOffice.

Il file contiene, rullo di tamburi, tre colonne di testo per una ottantina di righe. Excel riesce comunque a operare una qualche magia per complicarne la fruizione in un altro programma.

Il lieto fine, se così lo possiamo considerare: si visualizza perfettamente dentro Anteprima.

Ecco perché è così criticamente importante promuovere il software libero, contrastare Office, adottare e spingere ogni possibile alternativa. Se li lasci fare, neanche una tabella scampa alla normalizzazione.