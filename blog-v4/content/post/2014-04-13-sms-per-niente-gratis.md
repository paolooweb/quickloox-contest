---
title: "Sms per niente gratis"
date: 2014-04-14
comments: true
tags: [TextBelt]
---
Qualcuno è riuscito a usare [TextBelt](https://github.com/typpo/textbelt) e mandare gratis Sms dal Terminale? Non inseguo il sogno dei messaggini gratis e mi piace provare cose nuove e diverse.<!--more-->

Che non mi funzionano. In second’ordine, vorrei capire dove sbaglio.

Sempre parlando di messaggistica, attenzione a [TypeStatus in beta per OS X](http://hbang.ws/tweaks/typestatus/), perché ha l’aria di una gran comodità. Una delle poche cose degne di ingombrare la barra dei menu.