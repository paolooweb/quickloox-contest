---
title: "Finte contraddizioni"
date: 2013-11-15
comments: true
tags: [iOS, Freesmug]
---
**Gand** ne ha fatta un’altra delle sue. Come mi ha scritto, 

>una pagina con l’elenco delle app open source per iPhone e iPad disponibili su iTunes Store.<!--more-->

La [pagina](http://www.freesmug.org/fossios:_start) è bene organizzata, con classificazione per categoria, con icona, link, prezzo, descrizione e voto. Da scorrere, se non altro per curiosità. E ci sono dentro cosette pure interessanti.

Guai a chi, avendo informazioni, non contribuisce e non partecipa. Come minimo è moralmente impegnato a una donazione a Freesmug (colonna di sinistra, più o meno in fondo alla prima schermata).

Una pagina così serve in tutte le occasioni in cui qualche presuntuoso presume (il significato della parola è quello) che [non ci sia software libero su App Store](https://macintelligence.org/posts/2012-07-06-liberta-e-libertuccia/) in quanto piattaforma *chiusa*. Mai quanto una mente precaricata di preconcetti.