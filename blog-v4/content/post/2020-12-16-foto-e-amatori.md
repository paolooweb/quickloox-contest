---
title: "Foto e amatori"
date: 2020-12-16
comments: true
tags: [ProRaw, Heic, iPhone, Pixel, Envy, Halide, Obscura, Hdr]
---
Su *Pixel Envy* è apparsa una disamina impressionante dei [risultati della fotografia con ProRaw a bordo di iPhone](https://pxlnv.com/blog/proraw/).

È chiaro che la fotografia non è più quella dei nostri padri. Ma non è nemmeno più la nostra. Bisogna fare un salto in avanti, chi abbia un interesse concreto oltre il punta-e-scatta, e percorrere nuovi territori.

Non c’è modo di riassumere in breve le conclusioni tratte nell’articolo, che prende in esame più situazioni diverse e mette a confronto ProRaw con il nuovo formato compresso Heic e con la produzione di eccellenti app fotografiche come [Halide](https://halide.cam) e [Obscura](https://apps.apple.com/it/app/obscura-camera/id1290342794) (che non esito a dichiarare ottimi regali per un Natale diverso ad appassionati di fotografia in un mondo dove scambiarsi pacchetti sarà più complicato del solito). Le foto mostrate sono molte, le differenze analizzate nel dettaglio, l’approccio molto equilibrato e privo di partigianerie o pregiudizi di partenza. Uno di quei momenti dove sei contento che esista Internet e due soldini all’autore li dai volentieri per il servizio reso.

Da leggere, da guardare. Dubbi da porsi: continuo a leggere di tanto in tanto che sì, la fotografia sui computer da tasca evolve, migliora eccetera, ma niente può competere con un obiettivo come si deve montato su un corpo macchina come si deve.

Sono assolutamente d’accordo. Dove sta, però, la linea dietro la quale una persona competente, appassionata e però non dedita all’hobby in modo intenso, produce con iPhone foto che soddisfano i suoi criteri qualitativi e quelli del suo pubblico, e va bene così? È una linea che si sta spostando? Sapremmo distinguere tra un prodotto di ProRaw o Halide e quello di una macchina fotografica *prosumer*?

Non sono fotografo e non ho una posizione da sostenere. A guardare questi scatti e leggere questo articolo, tuttavia, mi viene da pensare. L’informatica personale ha portato strumenti di potenza enorme a un numero immenso di persone e credo che questo oramai riguardi anche la fotografia, anche sul lato qualitativo, che fino a qualche tempo fa lasciava sicuramente a desiderare.