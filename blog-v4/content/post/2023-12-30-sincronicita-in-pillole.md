---
title: "Sincronicità in pillole"
date: 2023-12-30T18:54:54+01:00
draft: false
toc: false
comments: true
categories: [Hardware]
tags: [AirTag, Sinchronicity II, Police]
---
Nel [riepilogare la parte tecnologica di questo Natale](https://macintelligence.org/posts/2023-12-26-tecnologia-zero/) mi sono effettivamente dimenticato di citare due [AirTag](https://www.apple.com/it/airtag/) in transito nella famiglia allargata.

Me lo hanno ricordato la notizia di un [furto di bagaglio sventato grazie a AirTag](https://macdailynews.com/2023/12/29/family-uses-apple-airtag-to-track-down-stolen-luggage-and-suspect/) e quella di [sconti Amazon sulla vendita delle pillole Apple antismarrimento](https://macdailynews.com/2023/12/29/apple-airtags-are-now-on-sale-for-up-to-20-off/) (non so se anche in Italia, in che misura, fino a quando, su che confezioni, non lo so, compro poca tecnologia, solo quando serve, e gli sconti li perdo praticamente tutti).

Possiedo un solo AirTag, regalato. Nell’intento di giovare alla famiglia l’ho messo a disposizione, con il risultato che, dato il cloud di borse borsine borsette zaini zainetti eccetera generato da una moglie e due figlie, il singolo AirTag se ne sta in un cassetto. Per esercitare una qualche strategia degna di nota dovrei averne, no so, diciassette. Non sono la persona più titolata per parlare di AirTag. Di certo c’è un cassetto che non corro il rischio di smarrire.

Nonostante tutto, li raccomando visto che è ancora tempo di regali, di viaggi, di visite, di scatole in sovrappiù e disordine in casa. Sono in realtà un’avanguardia della tecnologia di domani, quando avrà finito di nascondersi e farà parte dell’intero quotidiano lasciando poca o nessuna traccia. Tra una decina di anni l’equivalente di AirTag conterrà un chip autonomo e mezzo computer.

Intanto è piacevole vivere queste piccole istanze di [sincronicità](https://www.youtube.com/watch?v=XLEyIE_TMU8), quando ti passano per le mani le confezioni dell’oggetto e, appena cambiato il contesto, appare la notizia sul *feed*.

<iframe width="560" height="315" src="https://www.youtube.com/embed/XLEyIE_TMU8?si=qOAH0_56C7o0mXuG" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>