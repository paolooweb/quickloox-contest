---
title: "Dieci per duemila"
date: 2015-04-28
comments: true
tags: [5x1000, AllAboutApple, LibreItalia]
---
Ho due raccomandazioni ovvie per il cinque per mille: [LibreItalia](http://www.libreitalia.it) (94152640549) e [All About Apple](http://www.allaboutapple.com/) (90038610094).<!--more-->

Lascio libertà di voto (ci si potrebbe anche dividere in due gruppi).