---
title: "Più irreale di così"
date: 2023-05-14T00:51:57+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [Unreal, Epic, Apple Silicon, M1]
---
È sicuramente un’ottima notizia che [Unreal Engine 5.2 funzioni nativamente su Apple Silicon](https://www.engadget.com/apple-silicon-macs-now-natively-support-unreal-engine-5-124257710.html). Il motore è potentissimo e su M1 e M2 potrà mostrare meraviglie, a un costo energetico che nessun concorrente può eguagliare.

La notizia *veramente* irreale è il corollario di questa: significa che, prima, Unreal Engine doveva funzionare in emulazione attraverso Rosetta. E, nonostante lo strato di emulazione, il processore riuscisse ugualmente a fornire un livello decente di prestazioni.

Dirlo è scontato, ma va detto: più software è [nativo Apple Silicon](https://macintelligence.org/posts/2023-04-08-un-esempio-a-tre-dimensioni/), meglio è per tutti. Se poi è open source, meglio al quadrato. Speriamo di leggere tante notizie come questa, vedi per esempio [Reason](https://www.reasonstudios.com/press/344-reason-studios-introduce-native-support-from-apple-silicon-and-offline-mode-with-the-new-126-update) per l’audio.