---
title: "Roba da bambini"
date: 2014-09-20
comments: true
tags: [Panzarino, iOS, iOS8, iPhone, iPhone6, iPhone6Plus, Disneyland, Disney, Jobs]
---
Alle [recensioni da leggere](https://macintelligence.org/posts/2014-09-18-tempo-di-uscite/) in materia di iPhone 6 e iOS 8 devo aggiungere [Matthew Panzarino su TechCrunch](http://techcrunch.com/2014/09/17/life-is-tough/), con la sua idea geniale di collaudare iPhone 6 e 6 Plus sul campo. A Disneyland.<!--more-->

Un posto dove scattare miliardi di foto, i colori sono saturi e decisi, fa caldo, le celle telefoniche sono costantemente sovraccariche e alla fine di una giornata la batteria viene messa a dura prova, il tutto con il conforto di bambini e passeggini in mezzo a una marea di persone.

>Là fuori c’è una tonnellata di recensioni frutto di lavoro meticoloso con *app* di *benchmark*, righelli e termometri rettali per ricavare una valutazione degli iPhone basata sulle specifiche. Questa non è una di quelle. Ho fatto un viaggio e li ho spremuti come limoni.

Consiglio la lettura integrale. Mi è piaciuta soprattutto la parte sulla fotografia:

>Fotocamere come quella di Nokia 1020 possono avere una maggiore risoluzione, ma la decisione di Apple di realizzare il proprio coprocessore di segnali per immagini e sintonizzare finemente sensori e hardware ha pagato, e molto. Si presta molta attenzione agli sforzi compiuti in ambito processore, ma i *chip* di contorno come il coprocessore suddetto e M8 [sensore di movimento] sono ugualmente importanti e una buona ragione per la concorrenza di iniziare a progettarsi in autonomia i propri processori. I risultati sono palpabili.

C’è un video impressionante che mostra benissimo l’effetto notevole dello stabilizzatore di immagini e un parallelo tra Walt Disney e Steve Jobs, quando il primo – messo di fronte all’alternativa tra una soluzione economica e una costosa – rispose che non ci si doveva occupare di quanto costasse, ma di quanto fosse buono il risultato.

E di come fossero importanti i dettagli, per il successo di Disneyland. *Se perdiamo i dettagli, perdiamo tutto*. Questa la conclusione:

>Gli apparecchi personali come smartphone e indossabili sono fatti di niente se non dettagli. I prossimi anni promettono abbondanza per chi mantiene il controllo sulle minuzie e non molla la presa. Non è l’unico modo per farcela, ma è il modo che ha Apple, e i nuovi iPhone ne sono la rappresentazione.

Indicativo è che Panzarino ammetta di non amare la tecnologia fine a se stessa. Sotto il sole, al caldo, nella ressa, quando stai cercando una direzione o vuoi cogliere un attimo, vuoi semplicemente che i tuoi aggeggi funzionino, afferma. E che lo facciano al primo colpo. Si può dire quello che si vuole e effettuare ogni tipo possibile di misurazione. Su questa metrica, oggi, ha ragione Apple.