---
title: "Completare la raccolta"
date: 2015-01-26
comments: true
tags: [Eppela, AAA]
---
Ho formalizzato la mia iscrizione a [LibreItalia](http://www.libreitalia.it/) (sebbene la pagina soci non sia ancora aggiornata) e così spero di tutti.

Ritorno a chiedere denaro.

La causa è ugualmente importante e assai più urgente: All About Apple Museum ha lanciato una [raccolta pubblica di fondi](http://eppela.com/ita/projects/1519/all-about-apple-museum-30) per aprire la nuova sede del museo, a Savona presso la Nuova Darsena.<!--more-->

All About Apple Museum è il più grande e più munito museo Apple del mondo, nato e sostenuto dalla passione di persone straordinarie che ho avuto più volte il piacere di incrociare. Hanno raggiunto risultati che in quantità e livello li qualificano come cultura a tutto tondo, di quella vera, che affonda nella tradizione e nella storia delle persone. E si parla di cultura, non cultura Apple.

Servono cinquemila euro da raccogliere entro il prossimo mese perché il denaro dei donatori venga effettivamente erogato, altrimenti tutti saremo rimborsati. E si sarà persa una occasione.

L’associazione è una Onlus e metto la mia mano sul fuoco riguardo il fatto che qualsiasi centesimo raccolto andrà effettivamente a sostenere il museo.

Eppela, la piattaforma di raccolta fondi, è consolidata e affidabile.

Si accettano solo scuse per donare poco. Nessuna per non donare. E anche il sacrificio di una colazione al bar andrà per una buona causa.

[Grazie in anticipo](http://eppela.com/ita/projects/1519/all-about-apple-museum-30).