---
title: "Il quanto di sfida"
date: 2024-02-22T00:49:28+01:00
draft: false
toc: false
comments: true
categories: [Software, Security]
tags: [iMessage, cifratura, Post-Quantum Cryptography, PQC, PQXDH]
---
Chi glielo fa fare ad Apple di essere all’avanguardia, quando da leader può vendere a carrettate e fregarsene del resto? Eppure scopro con entusiasmo che, a partire dal decimale punto quattro delle versioni attuali dei sistemi operativi (iPadOS 17.4, macOS 14 eccetera), iMessage supporterà un protocollo di cifratura post-quantum, capace di resistere agli attacchi anche se effettuati da computer quantistici.

Lo si scopre sul blog di Apple dedicato alla sicurezza; il loro protocollo, denominato con poca fantasia PQ3, [è apparentemente l’unico](https://security.apple.com/blog/imessage-pq3/) a essere implementato su un sistema di messaggistica ad ampia diffusione e a fornire un livello di sicurezza che chiamano Post-Quantum, appunto, tre.

Dietro c’è solo [Signal](https://signal.org), posizionato a livello due; secondo gli autori del post, Signal ha il merito di essere arrivato prima di tutti ad adottare un protocollo di cifratura resistente al computing quantistico. Però rimane uno strato di sicurezza dietro al prossimo iMessage.

WhatsApp resta a livello uno, cifratura classica e nessuna sicurezza certificata rispetto alla sfida quantistica. C’è anche un livello zero, dei sistemi non cifrati end-to-end per default, dove si situano per esempio Telegram e Skype.

Nessuno di noi ha qualcosa da nascondere ma a tutti piace farsi gli affari propri sapendo di non essere spiato. Questa mossa di Apple va nella direzione non giusta, ma stragiusta, e mi piace tanto.

Si può essere leader e innovare, nel senso autentico della parola, invece che lavorare per conservare il sistema così com’è e rischiare il meno possibile di perdere posizioni.