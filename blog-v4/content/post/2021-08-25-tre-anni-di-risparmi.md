---
title: "Tre anni di risparmi"
date: 2021-08-25T23:49:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [Forrester, Apple, M1] 
---
Apple ha commissionato a Forrester uno [studio sull’impatto dell’adozione di Mac M1 in azienda](https://tools.totaleconomicimpact.com/go/apple/TEI/docs/TEI-of-Mac-in-Enterprise.pdf).

Immagino che fosse difficile ne uscissero risultati sfavorevoli per Apple. Tuttavia il metodo scientifico ci dice di analizzare, valutare e tenere per buono quello che regge all’analisi, in attesa di eventuali numeri più precisi o studi più accurati.

Lo studio è snello e semplice da leggere, va letto.

Un dato solo: in tre anni un Mac M1 fa risparmiare 843 dollari di minori costi.

Nel dato sono compresi i risparmi dati dal minor ricorso al supporto o dalla maggiore sicurezza, nonché gli incrementi di produttività di chi lavora con Mac.

Come dire che un’azienda sorda a prendere in considerazione (e valutare attentamente, ci mancherebbe) l’opzione Mac si autocondanna a spendere di più e produrre di meno.

C’è ben poco di altro da dire, il report va letto e passato alle persone illuminate che occasionalmente siedono sulle poltrone aziendali dove si decidono gli acquisti.

*Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile.*