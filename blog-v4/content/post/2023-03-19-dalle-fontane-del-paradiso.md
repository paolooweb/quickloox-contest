---
title: "Dalle fontane del paradiso"
date: 2023-03-19T00:59:53+01:00
draft: false
toc: false
comments: true
categories: [Internet]
tags: [Clarke, Arthur Clarke, Hyams, Peter Hyams, 2010 Odissea due, 2010 Odyssey Two, WordStar, Kaypro]
---
Ho letto quasi tutti i suoi romanzi, ma la cosa sua che ricordo con più precisione è il resoconto delle conversazioni con Peter Hyams, regista di *2010: Odissea due*, per rifinire la sceneggiatura del film. Conversazioni a distanza da Sri Lanka all’Inghilterra, con l’aiuto di un accoppiatore acustico attaccato al suo computer Kaypro. Clarke [scriveva con WordStar](https://macintelligence.org/posts/2015-05-19-false-equivalenze/).

Un pioniere, sempre.

Hal, [puoi aprire il portello](https://twitter.com/LKosmonautika/status/1637383236006412289).

<blockquote class="twitter-tweet"><p lang="it" dir="ltr">Il 19 marzo 2008 moriva Arthur C. <a href="https://twitter.com/hashtag/Clarke?src=hash&amp;ref_src=twsrc%5Etfw">#Clarke</a>.<br>Con i suoi romanzi ed i suoi racconti ci ha fatto volare nel <a href="https://twitter.com/hashtag/cosmo?src=hash&amp;ref_src=twsrc%5Etfw">#cosmo</a>. <a href="https://t.co/GeY9hHzBVT">pic.twitter.com/GeY9hHzBVT</a></p>&mdash; Le Storie di Kosmonautika (@LKosmonautika) <a href="https://twitter.com/LKosmonautika/status/1637383236006412289?ref_src=twsrc%5Etfw">March 19, 2023</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>