---
title: "Il salvachiappe"
date: 2023-02-02T17:46:13+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [GPT-3, ChatGPT]
---
Sono al lavoro per fornire un quadro produttivo e utile dei sistemi di generazione automatica di testo e quant’altro e, per quanto riguarda la parte testo, una cosa emerge confermatissima: ChatGPT può essere straordinariamente utile per avviare un lavoro, preparare un guscio da riempire, sbrigare compiti meccanici e noiosi.

Appena si conta sul sistema per la creazione di testo da usare, siamo al [minimo comune denominatore](https://macintelligence.org/posts/2023-01-04-minimi-comuni-denominatori/). Se quello che esce con una riga di input è pubblicabile, è utilizzabile, la persona addetta a quel compito è vicina a essere superflua.

Il primo palliativo è imparare a dominare il prompt e saper porre le domande giuste, soprattutto in approfondimento o in dettaglio.

Il secondo è una vocazione da editor. Individuare prima gli errori compiuti da ChatGPT e rivoltare poi come un calzino la sua prosa sussiegosa, prolissa, monotona e innocua permette di arrivare a un lavoro di qualità in meno tempo.

Chi non gradisse queste novità, buona fortuna. La vocazione dell’editor, comunque, ci voleva anche prima.

Non farei più di tanto affidamento sul salvachiappe, o volendo l’antidoto messo a punto dai titolari del veleno: la sedicente intelligenza che distingue la prosa automatica da quella umana.

Non è tanto la [ridotta e inaffidabile precisione](https://techcrunch.com/2023/01/31/openai-releases-tool-to-detect-ai-generated-text-including-from-chatgpt/) attuale dello strumento, che migliorerà. [Produttori indipendenti](https://writer.com/ai-content-detector/) sostengono di riuscire già da tempo a discernere la bistecca dal bollito.

I problemi sono altri. Se hai bisogno di uno strumento per separare i testi artificiali da quelli naturali, c’è un debito a monte. L’azienda che ne avesse bisogno dichiarerebbe la propria natura di ambiente tossico.

Di sicuro, negli uffici dove l’abitudine è il minimo sforzo e l’ignoranza una dote coltivata, i sistemi come ChatGPT avranno grande successo anche come produttori di lavoro finito.

È già arrivata la versione Premium di Teams che vanta funzioni di intelligenza artificiale. Presto avremo *yes-man* tutti uguali che parlano tutti uguale, in un’azienda che prima o poi rifarà i conti e scoprirà di poter sostituire un team da dieci con un consulente capace di porre quattro domande sopra la media, no, la mediocrità, a un quinto del costo di prima.

Il concetto di *posto sicuro* cambierà in *posto di chi sa tenerselo*. Fossi io, penserei ad assicurarmi che chiunque distingua i miei testi da quelli della macchina, subito, con certezza. Per tutto il resto, c’è la macchina.