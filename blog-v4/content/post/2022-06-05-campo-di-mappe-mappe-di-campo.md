---
title: "Campo di mappe, mappe di campo"
date: 2022-06-05T15:15:52+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [Apple, Mappe, Malpensa, xkcd]
---
Nel chiacchierare del [miglioramento di Mappe constatato (quindi sancito) da xkcd](https://macintelligence.org/posts/2022-05-25-mappe-da-perdizione/), penso che ognuno di noi ha la sua esperienza negativa e la sua esperienza positiva. 

La mia negativa l’ho accennata [l’anno scorso](https://macintelligence.org/posts/2021-07-03-scendere-in-campo/), quando per seguire l’itinerario online ho mancato un cartello fisico decisivo e mi sono ritrovato a seguire Mappe su un sentiero di montagna che anche con un fuoristrada sarebbe stato a senso unico, salvo soccorso alpino.

L’esperienza positiva è del 2019, quando ancora Mappe non era buona come ora, e riguarda un percorso *a piedi*.

L’aereo è arrivato con grande ritardo e ho fatto appena in tempo a prendere l’ultimo treno dall’aeroporto. Sono arrivato allo snodo dove prendere il treno locale per arrivare a casa, solo che i locali erano terminati.

Il radiotaxi non rispondeva. Non avevo parenti a disposizione. Estate profonda, notte profondissima, non c’era un’anima. 

A quel punto, l’unica possibilità era tornare a piedi. La strada in auto la conosco a menadito, una decina di chilometri. A piedi, un paio d’ore.

Provo ad aprire Mappe che mi propone, a piedi, un itinerario lungo la metà.

Provo a seguirlo. Arrivo ai margini del paese ed entro nei campi, o meglio nelle stradine agricole che li costeggiano. Mappe le conosce e le segnala con precisione. All’inizio sono perplesso, non vorrei ritrovarmi in mezzo alla campagna con iPhone che mi manda chissaddove; le indicazioni però sembrano precise e decido di fidarmi.

Camminare per un’ora in mezzo ai campi alle tre del mattino non è particolarmente raccomandabile, ma tutto va bene e arrivo davanti a casa con iPhone al lumicino, però impeccabile; senza la guida di Mappe non avrei mai neanche immaginato quel percorso, che ha davvero risolto una situazione fastidiosa.