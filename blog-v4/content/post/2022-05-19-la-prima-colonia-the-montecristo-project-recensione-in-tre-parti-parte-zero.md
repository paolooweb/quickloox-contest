---
title: "La Prima Colonia - The Montecristo Project: recensione in tre parti - parte zero"
date: 2022-05-19T00:49:37+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [La Prima Colonia, The Montecristo Project, Edoardo Volpi Kellermann, Evk, Tolkien]
---
Premessa. La sostanza di una recensione, nel mondo informatico, è scrivere di qualcosa per spiegare come è fatto, che cosa fa e come lo fa. C’è qualcosa di diverso rispetto alla recensione classica di un libro. Ma per quanto riesco recensirò [La Prima Colonia](https://www.delosstore.it/ebook/54251/la-prima-colonia-the-montecristo-project--1/) come si farebbe da informatici, anche perché l’autore di informatica ne mastica, al punto di portarne tanta e non inutile ovunque, nella trama come nella struttura.

Un altro punto della premessa è che il lavoro di [Edoardo](https://tolkieniana.net/wp/rivendell/musica/edoardo-volpi-kellermann/) al *Montecristo Project* dura da dodici anni e mi è capitato alcune volte di incrociare varie fasi della sua gestazione. Cercherò di ignorare sistematicamente questa conoscenza pregressa in quanto l’opera va giudicata da quello che porta, mentre come è nata dovrebbe essere secondario.

Il terzo e ultimo punto della premessa è che Edoardo è un tolkieniano di ferro, anzi, di acciaio. Per l’universo tolkieniano ha scritto, curato, musicato, creato, organizzato, fatto tantissimo. Potrebbe venire la tentazione di formulare paralleli tra la genesi, o la struttura, dell’opera, dato che evidentemente da Tolkien *deve* avere ricavato ispirazioni, suggerimenti, tecniche, esattamente come a me viene spesso da fare sul lavoro metafore attinenti al basket, dopo averlo praticato per mezzo secolo. Quando una esperienza ti permea, può solo manifestarsi, magari più o meno alla luce del sole; impossibile che non si rifletta sul modo di pensare e di agire.

L’esperienza permea anche me, più modestamente, e resterò impermeabile alla tentazione di fare paragoni o paralleli, per giustizia verso tutte le parti coinvolte in queste pagine. Che ci siano echi tolkieniani, o che manchino, e di che tipo siano, spetta al lettore desumerlo per trarne eventualmente le proprie conclusioni.

Ciò detto, a breve parlerò di come è fatto.