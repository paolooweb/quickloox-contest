---
title: "La mappa della libertà"
date: 2021-04-04T00:56:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [Dortmund, Il ritorno del Re, Isaac Asimov, Trilogia Galattica, Fondazione] 
---
Nel dichiarare la digitalizzazione come obiettivo primario da qui al 2025, la città di Dortmund [ha approvato due risoluzioni fondamentali](https://www.lastbulletin.com/exclusive-open-source-software-becomes-a-standard-in-dortmund-germany/):

- uso di software open source ovunque possibile;
- il software sviluppato dall’amministrazione o da essa commissionato viene messo a disposizione del pubblico.

Fatto importante è che, da qui in futuro, la pubblica amministrazione dovrà giustificare il mancato uso del software libero in luogo di una applicazione proprietaria. Di solito è il contrario.

Dortmund entra nell’elenco delle città che – ne avevo già scritto – sono assimilabili ai monasteri dopo il crollo dell’Impero romano di Occidente: isole di conservazione della cultura e della civiltà in un mare di declino.

In una visione fantasy, mi sovviene la sequenza delle pire in cima ai picchi più alti, accese in sequenza per trasmettere una richiesta di soccorso, [come mostrato ne Il ritorno del Re](https://www.youtube.com/watch?v=LUO5qhpD2pA):

Vale anche la lettura della [Trilogia Galattica di Asimov](https://asimov.fandom.com/wiki/Foundation), relativamente alla Fondazione incaricata di tenere acceso il lume della civiltà nella Galassia squassata dai conflitti. (C’è anche la Seconda fondazione, di cui però nessuno conosce l’ubicazione).

Forse è ora che qualche volonteroso realizzi una mappa dinamica sul web, che mostri e connetta le isole del mondo ancora libero svettanti sopra la melma dell’omologazione.

Se sembra esagerato o sciocco, si salvi questo post e ne riparliamo nel 2031.

<iframe width="560" height="315" src="https://www.youtube.com/embed/LUO5qhpD2pA" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

*Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile.*