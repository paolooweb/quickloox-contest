---
title: "Quando l'embedding si fa duro"
date: 2013-07-19
comments: true
tags: [iPad]
---
iPad ha favorito evolutivamente una nuova specie, dal tratto distintivo della testa durissima con la quale si affrontano ostacoli tipici dell’uso della tavoletta, fino a superarli con un misto di intuizione, pervicacia e fortuna.<!--more-->

Avevo due blog da aggiornare, entrambi basati su un Wordpress fortemente personalizzato (in due modi diversi). Ce l’ho fatta ricorrendo a una pluralità di strumenti.

I due blog erano stati predisposti prima di abbandonare la scrivania, impostando su Mac le parti personalizzate e più fastidiose da trattare su iPad, quelle che le *app* specializzate elaborano poco o male e Safari è poco indicato per lavorare sul piccolo schermo. E anche le immagini, capitolo già risolto ma farraginoso. Niente che non avrei potuto fare autonomamente su iPad, ma partenza lanciata invece che a freddo.

Il lavoro principale è stato svolto in [Blogsy](http://blogsyapp.com). E sarebbe bastato se non avessi avuto da fare *embedding* (intarsio) di un *tweet* e di una presentazione [SlideShare](http://www.slideshare.net).

Oltre a Safari e alla [app Twitter](https://twitter.com/download/iphone), ho dovuto chiamare in causa anche [Kiss My Agent](http://mobisle.org/apps/kissmyagent/).

Su Slideshare si è arreso anche lui, per un problema di *rendering* del codice Html. Allora ho barato spudoratamente: mediante [LogMeIn](https://www.logmein.com) ho preso il controllo del mio Mac, a casa in attesa vigile, e l’ho messo al lavoro, facendo produrre a lui il codice che mi serviva.

Testa dura, mani su iPad, missione compiuta.