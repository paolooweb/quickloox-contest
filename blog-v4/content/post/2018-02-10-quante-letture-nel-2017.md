---
title: "Quante letture nel 2017"
date: 2018-02-10
comments: true
tags: [Backblaze]
---
Per opinioni su quali siano i dischi migliori, più durevoli, affidabili, ci sono i fatti gentilmente forniti da Backblaze, che ha pubblicato meritoriamente le statistiche sull’andamento dei propri centri dati [per l’ultimo trimestre ma anche per il 2017](https://www.backblaze.com/blog/hard-drive-stats-for-2017/).<!--more-->

Ancora più meritoriamente, Backblaze mette a disposizione i [dati grezzi](https://www.backblaze.com/b2/hard-drive-test-data.html), liberamente a disposizione di chi voglia tirare conclusioni autonome.

Su che basi valuta, Backblaze? Hanno cominciato a tenere traccia del comportamento dei dischi nel 2013 e da allora hanno raccolto “solamente” ottantotto milioni di dati per un ingombro di ventitré giga.

Come dici? Hai comprato un Western Digital e si è rotto? Quindi non li raccomandi. Ah, OK, sei uno che ne sa.