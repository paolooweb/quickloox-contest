---
title: "À la cartina"
date: 2019-10-01
comments: true
tags: [Maps, Mappe]
---
Justin O’Beirne è il guru delle mappe digitali e ha appena pubblicato un [aggiornamento clamoroso](https://www.justinobeirne.com/new-apple-maps-northeast) sulle nuove mappe che Apple ha in sviluppo inizialmente, come era facile intuire, per gli Stati Uniti e ancora non tutti.

Tuttavia la pagina è ricchissima di comparazioni e animazioni e non c’è fatica a percorrerla, piuttosto curiosità e senso di meraviglia per capire quante e quali cose stanno veramente dentro una mappa che si vuole omnicomprensiva, e quanto lavoro serva per averle.

Qualche esempio banale: i marciapiedi, l’interno dei musei, le viste di insieme per capire con un’occhiata quali siano le strade principali. Poi, in una città come New York, sorgono continuamente nuovi edifici e altri ne spariscono. Mantenere aggiornata la pianta, la simulazione 3D, la viabilità intorno, le informazioni commerciali e di servizio…

Non lo seguivo regolarmente via Rss ed è stato un grosso errore. Non mi perdo più un aggiornamento, li voglio serviti caldi sul desktop appena arrivano.