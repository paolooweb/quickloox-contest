---
title: "Basta la parola"
date: 2020-05-21
comments: true
tags: [Immuni, iOS, coronavirus, Covid-19, Bending, Spoons]
---
Normalmente lascio che i miei computer si aggiornino da soli. Stavolta ero troppo curioso di vedere iOS 13.5 con le migliorie alle chiamate FaceTime tra gruppi e naturalmente la prima edizione ufficiale dell’infrastruttura di servizio per le app di tracciamento contatti antiCovid-19.

Sono andato a cercare notizie di Immuni. Su GitHub sono visibili [zero righe di codice](https://github.com/immuni-app/documentation). Solo chiacchiere.

Sul sito di Agenda Digitale, l’[articolo su Immuni](https://www.agendadigitale.eu/cultura-digitale/immuni-come-funziona-lapp-italiana-contro-il-coronavirus/), quello dove riescono a scrivere *Bending Spoons* in tre modi diversi, è stato aggiornato tra l’altro con una dozzina di schermate. Roba forte.

Forse si sono convinti che la gente legga *Immuni* e pensi, più che al nome della app, al risultato dell’esame. Sentirsi sicuri e protetti dopotutto ha notoriamente un effetto positivo sulla salute. La app, beh, c’è tempo, no?