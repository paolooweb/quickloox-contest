---
title: "Un addio di lusso"
date: 2017-07-14
comments: true
tags: [iPhone, Vertu, Nokia]
---
Mentre le *news* sono piene di [speculazioni surreali attorno al prossimo iPhone](https://daringfireball.net/2017/07/iphone_silly_season) e mi chiedo come si possa parlare ma soprattutto leggere seriamente su un prodotto che per due mesi almeno non esiste ancora, succedono cose come la [chiusura di Vertu](https://www.phonearena.com/news/Super-luxury-phone-maker-Vertu-shuts-down-unable-to-pay-its-bills_id96004).

Fondata nel 1998 da Nokia per vendere telefonini di lusso a persone ricche disposte a spendere tipo diecimila dollari per modelli in pelle umana o tempestati di pietre preziose, dopo il tracollo del gigante finlandese è andata in mano a faccendieri il penultimo dei quali l’ha venduta all’ultimo glissando su debiti per 165 milioni di dollari che sono saltati fuori solamente dopo.

Da tempo Vertu non pagava bollette, stipendi, fornitori e quant’altro è possibile pagare per un’azienda. Chissà se la data di inizio della rovina è in relazione con quella dell’acquisizione del ramo cellulari di Nokia da parte di Microsoft. Sarebbe un ennesimo successo per quelli che [celebravano il funerale di iPhone](http://www.businessinsider.com/microsoft-iphone-funeral-2010-9?IR=T) con la processione-burla in mezzo agli uffici.

