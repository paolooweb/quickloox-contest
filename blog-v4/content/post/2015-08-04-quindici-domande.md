---
title: "Quindici domande"
date: 2015-08-04
comments: true
tags: [Moruzzi, Gossage, Advertising, Apple, cookie, Google, Microsoft, Safari]
---
[Massimo Moruzzi](http://dotcoma.org) mi ha segnalato il suo e-libro [15 Questions About Online Advertising](http://www.smashwords.com/books/view/565143). È breve, semplice, chiaro, preciso. L’ho divorato (gratis, in qualsiasi formato lo si voglia) e mi sento meglio.<!--more-->

Massimo affronta l’argomento dei banner, la pubblicità più noiosa e inefficace del mondo, eppure ancora prodotta dopo anni e anni in quantità incommensurabili.

E spiega in modo comprensibile e sintetico perché non funzioni e anche perché nonostante questo continui ad affliggere miliardi di pagine web.

Temi per specialisti? No, abbiamo davanti banner tutto il giorno. L’Abc della faccenda deve essere cultura generale e questo bignami mancava. In cui si scoprirà come, ecco la ragione per parlarne qui, le strategie Apple siano un’eccezione tra le grandi multinazionali globali. Sia Google che Microsoft si fermano più o meno davanti a niente pur di inseguire il cliente, qualunque sia il sito (un concetto importantissimo, chiave, che spiega la diminuzione costante della qualità dei contenuti web). Apple produce Safari in modo che blocchi certa invadenza pubblicitaria di serie, senza bisogno di configurazioni; e guadagna dalla vendita degli apparecchi, non dal pedinamento online di chi li compra.

A proposito di comprare: Massimo ha scritto questa sintesi distillandola da un testo più ampio, [What Happened to Advertising? What Would Gossage Do?](http://www.dotcoma.it/what-happened-to-advertising/what-would-gossage-do), più dettagliato e un pizzico più specialistico (sempre piacevolissimo da leggere). Non è per chiunque, ma per chiunque abbia un interesse specifico nella materia. Il libro può essere pagato, una cifra ridicola per quello che vale.

Se alla fine di una catena del valore gratuita ogni tanto viene voglia di riconoscere del valore a contenuti di qualità, questo è un buon momento per farlo. Se questo libro avrà il riconoscimento che merita, ne usciranno altri e sarà solo un gran bene.