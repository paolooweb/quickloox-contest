---
title: "De crescita"
date: 2014-05-11
comments: true
tags: [iPad, Evans, Roambi, Android, Chitika, iPhone]
---
Benedict Evans ha radunato in una eccellente ed esaustiva [serie di grafici](http://ben-evans.com/benedictevans/2014/4/25/ipad-growth) tutta la fotografia dell’attuale tendenza di vendite di iPad, che mostra un deciso rallentamento.<!--more-->

Dove sta il valore è tuttavia nelle spiegazioni; dove si trovano le gemme è nel suggerire idee diverse dagli automatismi mentali.

Si vede che iPad rallenta, ma in termini di traffico web la crescita di Android è risibile, per non parlare del resto. Non c’è un concorrente che avanza, il ritmo è lo stesso per tutti. Così Chitika mostra la [situazione in Nordamerica](http://tech.fortune.cnn.com/2014/01/27/apple-chitika-tablet-traffic/): la competizione è assente.

 ![iPad e i dodici nani](/images/chitika.png  "iPad e i dodici nani")

Un’idea forte, invece: forse la concorrenza potrebbe non essere tra computer e tavolette-più-computer-da-tasca, ma computer da tasca contro tutti. iPad rallenta, ma iPhone cresce e prende spazio. In termini generali, uno schermo da cinque pollici e uno da sette sono relativamente simili in termini di esperienza di utilizzo. Il prossimo *trend* potrebbe essere di telefoni più grandi del normale, con schermi più grandi per minimizzare il problema delle dimensioni della superficie utilizzabile.

Un’idea ancora più forte: il mondo del software non ha ancora attraversato l’evoluzione necessaria per rendere iPad imbattibile, dal momento che i modi di lavorare sono ancora quelli dei personal computer. Questo passo è clamorosamente vero:

>È difficile passare una giornata a creare su iPad una presentazione sull’andamento delle vendite. Ma in verità quella presentazione dovrebbe essere un pannello di controllo Software as a Service che bastano dieci minuti per annotare. Ci vorrà tempo perché le pratiche della produttività da ufficio permettano di sfruttare appieno le tavolette.

Se non è chiaro, scaricare (gratuitamente) [Roambi](https://itunes.apple.com/it/app/roambi-analytics/id315020789?l=en&mt=8) e dare un’occhiata agli esempi. Quello è il modo futuro, che dovrebbe essere presente.

La mia opinione? Il rallentamento mi spinge a pensare che l’impatto a lungo termine di iPad sarà ancora più intenso di quello che si immagina. Una specie di quiete che precede la tempesta. Nel giro di due o tre generazioni di prodotto, gli acquirenti di personal computer saranno la metà di quelli odierni.