---
title: "Semplice e complesso"
date: 2017-02-11
comments: true
tags: [AirPods]
---
Uno dei principî cardine del *design*, e la ragione fondamentale per avere una Apple, è levare la complessità dall’esperienza di utilizzo.

I pionieri, abituati a computer semplici, amano la loro complessità perché sono cresciuti mettendoci dentro le mani per sopravvivere. Se avevi un Mac negli anni ottanta ed eri bravo, [ResEdit](http://www.mac.org/utilities/resedit/) (che permetteva l’editing delle risorse interne di programmi e sistema) era quasi una medaglia al valore e c’era chi ci passava nottate a esplorare, personalizzare, divertirsi.

Negli anni i computer sono diventati sempre più complessi, credo di tre ordini di grandezza: il sistema operativo di Mac negli anni ottanta era composto da migliaia di file. Oggi sono milioni. Lo sport di esplorare le viscere del sistema oggi è molto meno diffuso e qualcuno la sente come una mancanza intollerabile. La realtà è che, maturando, i computer hanno sempre più levato la complessità dall’esperienza d’uso. Come da molto tempo hanno fatto gli elettrodomestici, le automobili, le centrali elettriche (accendiamo la luce premendo un tasto e nessuno passa i weekend a scorrere il proprio impianto elettrico spinto dal desiderio di apparendere).

Ecco perché ha sempre meno senso pubblicare notizie come l’[aggiornamento del firmware di AirPods](https://9to5mac.com/2017/02/01/apple-airpods-silently-receive-bug-fix-firmware-update-to-version-3-5-1/) e perché è completamente a senso zero sottolineare che l’aggiornamento è avvenuto *silently*, silenziosamente.

Penso che l’architettura degli AirPods sia concettualmente interessantissima. Ma qualcuno passa veramente del tempo a sapere che *firmware* contengono i suoi auricolari? Sono auricolari. Funzionano in maniera estremamente complessa, perché noi li si possa usare nel modo più semplice. Massimo rispetto per iFixit che li [disseziona anatomicamente](https://it.ifixit.com/Teardown/AirPods+Teardown/75578): ci vogliono una competenza e una manualità spaventosa. Avvitarsi sul numero di versione del *firmware*, invece, lo trovo totalmente futile. Se vuoi apprendere, comprati un [Raspberry Pi](https://www.raspberrypi.org).

Mille volte meglio analizzare gli AirPods [come ha fatto Flavio](https://macintelligence.org/posts/2017-01-17-ti-cambiano-la-vita/).