---
title: "Giocare per dimenticare"
date: 2021-04-11T02:34:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [Super Mario Bros, Ars Technica] 
---
Ho avuto nella vita contatti sporadici e superficiali con discipline orientali come zen o yoga. Non ne ho mai fatto niente di serio. Eppure ho fato esperienza di varie situazioni di vita tipicamente occidentali che ben si prestavano a favorire l’insorgere di stadi della mente prossimi a quello della meditazione.

Da cestista, uno dei miei punti di forza NON erano i tiri liberi. La mia tecnica era approssimativa e non sono mai riuscito a sviluppare un automatismo durevole. La mia percentuale in carriera è giudicabile come appena sufficiente.

Eppure, quando i miei tiri liberi potevano decidere una partita, ho segnato quasi sempre. Invece che avvertire la pressione del momento, avevo imparato a liberare la mente e a isolarmi dal contesto. Direbbe l’autore di qualche libretto zen, a diventare tutt’uno con il pallone che entrava nel canestro.

Un’altra attività propedeutica erano i videogiochi da bar, specialmente quelli a tema spaziale, del tipo *astronave che deve arrivare più avanti possibile evitando il fuoco nemico*. Giocando concentrato, ero uno qualunque. Certe rare volte, riuscivo – come dire? – a rendere le mani indipendenti dalla parte razionale del pensiero. Giocavano loro. Con risultati radicalmente migliori.

Ma non sono mai arrivato a risultati come [finire Super Mario Bros. in quattro minuti, cinquantaquattro secondi, novecentoquarantotto millesimi](https://arstechnica.com/gaming/2021/04/new-super-mario-bros-record-breaks-speedrunnings-four-minute-mile/). Si legga l’articolo: è giocare una partita non perfetta, ma impiegando nove fotogrammi in più rispetto al limite fisico.

L’eccezionalità dell’impresa non sta nella coordinazione oculo-manuale, ma nella condizione mentale: è semplicemente impossibile raggiungere un risultato del genere attraverso la concentrazione.

Avrei dato qualsiasi cosa per poter leggere l’attività cerebrale di chi ha compiuto l’impresa, durante quei quasi cinque minuti di oblio supremo.

*Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile.*