---
title: "Apple non facit saltus"
date: 2015-05-30
comments: true
tags: [AllAboutApple, Z88, Newton, MessagePad2100, QL, Sinclair, retrocomputing, Macintosh512Ke, WallStreet, PowerBookG3, Workflow, watch]
---
La prima premessa è che amo molto il *retrocomputing* e, se ho rimpianti sul tempo che passa, consistono nell’avere dedicato al vecchio [Sinclair QL](http://www.dilwyn.me.uk) meno tempo oggi di quello che meriterebbe, per non parlare del [Newton MessagePad 2100](http://www.everymac.com/systems/apple/messagepad/stats/newton_mp_2100.html). Non so neanche dove potrebbe essere finito il fantastico [Z88 di Cambridge Computing](https://cambridgez88.jira.com/wiki/display/welcome/Cambridge+Z88).<!--more-->

La seconda premessa è che, al netto della prima, il *retrocomputing* mi ha anche un po’ annoiato. Iniziative come [All About Apple Museum](http://www.allaboutapple.com) sono da premiare e valgono da sole un festival della cultura; l’idea di trasformare un vecchio [iBook](http://it.wikipedia.org/wiki/IBook) in macchina [emacs](http://www.gnu.org/software/emacs/) per scrivere [elisp](http://www.emacswiki.org/emacs/EmacsLisp) è affascinante; di converso, quando manca uno scopo vero e proprio, l’antico per l’antico completamente fine a se stesso mi è diventato stucchevole.

Riesco a provare entusiasmo se vedo ingegno e se vedo l’idea del *retrocomputing* come un punto di partenza invece che il ritorno nostalgico agli anni di quando si era giovani e le cose non erano fantastiche perché lo erano, ma perché si era giovani.

Ingegno e slancio verso il futuro: ne avverto traccia in questo simpatico *hack* che [mette in comunicazione watch e un Macintosh 512Ke](http://www.appleworld.today/blog/2015/5/28/sharing-files-between-a-mac-512ke-and-an-apple-watch), non il primo a essere prodotto ma molto vicino a quel momento.

Mi piace perché colma trent’anni di informatica in un passo, anzi due. Mi piace infatti perché c’è ingegnosamente di mezzo anche un [PowerBook G3 WallStreet](http://lowendmac.com/1998/wallstreet-powerbook-g3-series/). Mi piace perché tutta la parte interessante dell’interazione [avviene su watch](https://www.youtube.com/watch?v=ApxR_TwmLCs), grazie a [Workflow](https://macintelligence.org/posts/2014-12-16-tutte-insieme-ora/). Programmare su un Macintosh 512Ke era un’esperienza veramente di frontiera. Ma, per quanto in un altro modo, la [frontiera di watch](https://developer.apple.com/watchkit/) è molto, molto più avanti.

Il simbolismo però è notevole: il più antico dei Mac (OK, quasi) e il più moderno dei *device* uniti per un istante, come se in mezzo non fossero avvenute centinaia di mutazioni ed evoluzioni.

<iframe width="560" height="315" src="https://www.youtube.com/embed/ApxR_TwmLCs" frameborder="0" allowfullscreen></iframe>