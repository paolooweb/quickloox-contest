---
title: "Cento di questi trent’anni"
date: 2020-04-17
comments: true
tags: [Tidbits, Engst]
---
Una pubblicazione Internet-only che [compie trent’anni](https://tidbits.com/2020/04/13/tidbits-marks-its-30th-anniversary-in-a-time-of-pandemic/) è un fatto inusitato e lo scrive lo stesso Adam Engst, che sa di essere protagonista con Tidbits del panorama editoriale Apple da sei lustri.

Neanche a dirlo, Tidbits naviga nelle stesse acque di tante altre iniziative in questi giorni; meno abbonamenti, meno donazioni, meno tutto.

Il destino umano delle persone di questi giorni mi sgomenta sempre, mentre quello delle testate dipende, a volte mi coinvolge e a volte mi lascia indifferente. Non solo Engst è uno che merita di farcela e di superare il momento difficile nel modo migliore possibile, ma Tidbits è un pilastro di informazione e soprattutto informazione corretta che spero riesca a continuare per altri trenta, sessanta, novanta anni.

Sono quei momenti in cui si vorrebbe essere milionari. Per condividere.