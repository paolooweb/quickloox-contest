---
title: "Per la precisione"
date: 2017-07-10
comments: true
tags: [Rolex, Ubs, watch]
---
watch [supera in vendite qualsiasi produttore svizzero di orologi](https://watchaware.com/post/20549/ubs-apple-watch-taking) con l’eccezione di Rolex e l’intero mercato svizzero vale circa 28 milioni di unità, mentre quello degli apparecchi indossabili ha passato i quaranta.

Vedo altri casi Nokia all’orizzonte, o sulla riva del fiume se si vuole.  Apple ha mostrato di saper fare un orologio; invece i fabbricanti di orologi, quando devono fare computer, perdono precisione.
