---
title: "Non sappia la destra"
date: 2020-07-22
comments: true
tags: [Microsoft, Smith, Slack, Apple, antitrust]
---
Brad Smith, presidente di Microsoft, [si è lamentato](https://www.bloomberg.com/news/articles/2020-07-21/microsoft-president-raised-apple-issues-to-house-antitrust-group) presso la commissione antitrust americana delle presunte pratiche scorrette dell’Apple Store.

Slack ha presentato alla Commissione antitrust europea un [esposto sulle presunte pratiche scorrette di Microsoft](https://techcrunch.com/2020/07/22/slack-has-filed-an-antitrust-complaint-against-microsoft-teams-in-the-eu/), precisamente l’avere legato Teams – sistema concorrente di Slack – a doppio filo con Office 365.

Tanti anni fa Microsoft aveva ricevuto sanzioni e multe varie per avere legato a doppio filo Internet Explorer con Windows.

Strano che la storia si ripeta: Microsoft è diventata buona, ama Linux, produce versioni di tutto per tutti…

In compenso Brad Smith è veramente persona di spirito evangelico.