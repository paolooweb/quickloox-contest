---
title: "La pagina della sfinge"
date: 2013-07-13
comments: true
tags: [Sicurezza]
---
La signora ha dimenticato sul treno la sua tavoletta.

Il primo pensiero è portarla in stazione all’ufficio oggetti smarriti. Ma la terranno in attesa che passi la signora oppure se la metteranno in tasca e tanti saluti?

Forse sarebbe meglio accenderla e vedere se c’è dentro un numero di telefono o una email che possano dare qualche indizio.<!--more-->

(Una tavoletta Android si accende in un tempo ridicolmente lungo, eppure viene spenta. Poi ci si chiede che differenze vi siano con iOS; iOS lo si *usa*).

La tavoletta è protetta da *Pin*. La signora ha fatto bene, così non posso frugare nei suoi dati alla ricerca di un numero di telefono personale. Al tempo stesso, però, non la posso aiutare.

D’altronde, se avessi trovato un numero di telefono, che sarebbe accaduto? Prima che la signora possa tornare sui suoi passi, il treno avrà già percorso diverse fermate, molti chilometri. Anche potendo avvisare la signora, non avrei probabilmente il tempo di aspettarla o di andare da lei. Potrei avvisarla che lascio la sua tavoletta all’ufficio oggetti smarriti; solo che, se all’ufficio se la intascano, la signora avrà unicamente il mio riferimento e nulla le vieterà di pensare che me la sia intascata io (scommetto che all’ufficio oggetti smarriti sono bravissimi a fare la faccia di circostanza).

Penso che magari la signora avrebbe potuto facilitare una situazione come questa adottando uno sfondo scrivania con il nome e il numero di telefono in piccolo. Allo stesso tempo, se anche ci fosse stato, non avrebbe modificato le considerazioni precedenti.

Può anche darsi che all’ufficio oggetti smarriti siano onestissimi e che la soluzione più veloce sia anche la migliore per tutti.

Mi auguro che la signora abbia qualcosa di simile a Trova il mio iPad. Anche se deve stare attenta: se in preda al panico cancella a distanza tutti i dati dalla tavoletta, come farà più tardi a dimostrare che è davvero sua? Se invece si limita a fare comparire un messaggio sullo schermo, o a fare suonare un allarme, un eventuale malintenzionato sarà motivatissimo ad azzerarla prima possibile.

Ecco perché affrontare in modo adeguato la sicurezza informatica è più difficile di quanto pensino gli sprovveduti. A ogni passo, un enigma.