---
title: "Sessanta minuti eccezionali"
date: 2015-12-21
comments: true
tags: [Cbs, Cook, Apple, Jobs, Ive]
---
La rete americana Cbs ha dedicato ad Apple un [eccezionale servizio della sua serie 60 Minutes](http://www.cbsnews.com/news/60-minutes-apple-tim-cook-charlie-rose/).<!--more-->

Eccezionale perché, come ha dichiarato Tim Cook nel corso del servizio stesso, la Apple odierna è ancora in larga misura quella di Steve Jobs. C’è una differenza, però: con Jobs non sarebbe mai stato possibile realizzare una cosa analoga, ad ampio spettro su Apple invece che focalizzata su un singolo elemento e personaggio.

C’è un bel po’ da leggere e da guardare, a partire dalle trascrizioni delle interviste a Cook, Jonathan Ive e altri. Di sicuro niente altro oggi sul pianeta può sintetizzare in modo altrettanto completo e ragionevolmente imparziale l’immagine, il clima e l’opera di Apple alla fine dell’anno 2015.