---
title: "Vabengono"
date: 2023-05-07T01:58:10+01:00
draft: false
toc: false
comments: true
categories: [Software, Internet]
tags: [Terra2, intelligenza artificiale]
---
Ho lasciato su Twitter [un’idea semplice](https://twitter.com/loox/status/1654068823996112896):

<blockquote class="twitter-tweet"><p lang="en" dir="ltr">We can’t design AI systems at the moment. Just textual models chaining words without the tiniest idea of why.</p>&mdash; Lucio Bragagnolo (@loox) <a href="https://twitter.com/loox/status/1654068823996112896?ref_src=twsrc%5Etfw">May 4, 2023</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

**Terra2** ha risposto [una serie di cose interessanti](https://twitter.com/Terra2itter/status/1655103278424653824):

<blockquote class="twitter-tweet"><p lang="it" dir="ltr">Ci stavo riflettendo stanotte (non chiedermi perché) e credo che quello che hai scritto sia corretto. Quello che mi chiedo però è: “Non è che per caso stiamo sopravvalutando la NOSTRA intelligenza?”.</p>&mdash; Terra2 🦛💨 (@Terra2itter) <a href="https://twitter.com/Terra2itter/status/1655103278424653824?ref_src=twsrc%5Etfw">May 7, 2023</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

Non metto tutti i tweet per stare leggero (si leggono facilmente a partire da qui sopra). La [conclusione](https://twitter.com/Terra2itter/status/1655109531741896705) è questa:

<blockquote class="twitter-tweet"><p lang="it" dir="ltr">In sostanza (concludo) ChatGPT, Midjourney e compagnia non sono AI, ma (già allo stato attuale) potrebbero essere pezzi indispensabili per una Intelligenza Artificiale simile a quella biologica.</p>&mdash; Terra2 🦛💨 (@Terra2itter) <a href="https://twitter.com/Terra2itter/status/1655109531741896705?ref_src=twsrc%5Etfw">May 7, 2023</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

Una risposta esaustiva mi chiede più tempo di quello che ho ora. La sostanza però è questa.

Dentro i modelli attuali ci sono certamente concetti già visti e consolidati nell’intelligenza artificiale, per esempio la *temperatura*: più il sistema è insoddisfatto delle soluzioni che ha trovato, più ribolle (pensiamo a un gas in cui il moto browniano aumenta in proporzione al calore cui viene sottoposto) e cerca alternative. Questo è un concetto che risuona con quello che accade nella nostra mente quando cerchiamo soluzioni.

Altrimenti, per dirla con Tananai, *non siamo come loro*. È consenso comune che l’intelligenza umana parta dalla *percezione* per poi arrivare a una *rappresentazione*, di cui si elaborano *mappature*, sulle quali si possono costruire *analogie*.

I sistemi attuali bypassano la parte percettiva e va bene. Ma già non svolgono alcun lavoro di rappresentazione: glielo passiamo noi pronto già cotto. Nel modello è scritto dall’inizio che *cotto* è un attributo del prosciutto, un participio passato al maschile, forse una varietà di piastrelle. Se facciamo notare al sistema che c’è anche un deejay di Virgin Radio con questo cognome, e il modello non lo prevede, il sistema non riesce a gestire la situazione.

Di più: la rappresentazione nel nostro cervello non avviene a livello di parole, ma un po’ più in basso. Mi piace ripetere l’esempio di mia figlia che a un certo punto, per dire *vanno bene*, usava *vabengono*. La rappresentazione di pezzi di parola e pezzi di struttura si è combinata e ricombinata fino a renderle accettabile un neologismo che non può avere acquisito da alcuna altra parte; è certamente una creazione del suo cervello (o quello di un compagno di classe magari, ma non altro).

Potremmo parlare di similitudine tra il modello testuale e il cervello umano se il primo partisse vuoto e, riempiendosi, riuscisse a inventare un equivalente di *vabengono*. Invece il modello testuale è già pieno ed è statico. Riesce a rimescolare l’impossibile, a patto di non chiedere neanche una virgola inedita. Questo chiaramente non è il funzionamento del nostro cervello, o saremmo ancora a costruire variazioni sulla clava e la selce scheggiata.

Poi, certo che una delle caratteristiche dell’intelligenza è compiere una selezione casuale pesata dentro la base di esperienza. È che la nostra intelligenza è anche capace di rovesciare il tavolo e prendere una strada rivoluzionaria. La loro, no.

Tornerò in argomento. Intanto butto lì una domanda: secondo noi, che meccanismi applica la mente umana per giocare a Wordle?