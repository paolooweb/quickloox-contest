---
title: "Il massimo del minimo"
date: 2015-06-11
comments: true
tags: [Wind]
---
Questo *post* arriva in ritardo perché ostaggio di una tariffa ricaricabile di Wind che elargisce dodici gigabyte in cambio di diciannove euro rinnovabili ogni ventotto giorni.<!--more-->

Se i dodici gigabyte finiscono in anticipo, la banda viene tagliata a trentadue chilobit per secondo fino a che si rinnova, dice Wind.

Tutto vero, tranne che la banda passa a *massimo* trentadue chilobit. Dove l’idea di *massimo* è quella tipica delle compagnie telefoniche. La banda reale è a malapena un chilobit con il vento a favore.

Così succede che non si riesca a rinnovare la tariffa Internet Wind attraverso il sito Wind, perché non c’è modo di caricare il sito.

Niente di male, ci sono il 155 e tutte le altre opzioni. Ma almeno consentire la navigazione del sito Wind sembrava brutto?

Sembra che la logica del massimo inteso come minimo, e nemmeno indispensabile, alberghi anche negli uffici Wind tra sedia e scrivania.