---
title: "Guida al mondo Android in duecentosessantacinque parole"
date: 2018-07-28
comments: true
tags: [Huawei, P8, P10, P20, Android, Slashdot]
---
Il lavoro di base [lo ha svolto Slashdot](https://news.slashdot.org/story/18/07/25/2057249/vlc-blacklists-newer-huawei-devices-to-combat-negative-app-reviews).

Gli sviluppatori di [Vlc](http://videolan.org), uno dei migliori lettori indipendenti e *open source di audiovideo*, hanno deciso di impedire l’installazione della loro applicazione su alcuni degli ultimi smartphone Huawei, segnatamente P8, P10 e P20.

Il motivo è che il loro software di bordo è, diciamo, appena fiscale nel concedere i permessi di funzionamento in background, allo scopo di economizzare batteria.

Per questo motivo molti che ascoltano audio in background con Vlc sui suddetti telefoni si ritrovano la app terminata senza preavviso; si arrabbiano con la app (mentre la colpa sarebbe del sistema operativo) e lasciano una recensione negativa di Vlc su Google Play Store, così danneggiando la reputazione del programma e scoraggiando altri utilizzatori dallo scaricarlo.

È sempre possibile cambiare manualmente le impostazioni degli apparecchi incriminati, solo che la stragrande maggioranza degli utilizzatori non lo sa, o lo sa ma non è capace, o lo sa e non vuole metterci tempo.

Uno dei commenti alla notizia:

>Se il problema è questo, meglio non avere telefoni Samsung, perché S8 fa esattamente la stessa cosa. Ho installato l’applicazione Sms di Google per sostituire quella di Samsung. Ci ho messo secoli a capire perché vari Sms non mi arrivavano. È saltato fuori che Samsung terminava la app.

Questa è una sfaccettatura interessante del mondo Android, frequentato spesso – si badi – da gente che dice *Sul mio computer voglio essere libero di fare quello che mi pare e perciò snobbo Apple*.