---
title: "Bene e meglio"
date: 2016-07-26
comments: true
tags: [Specout, Mac, MacBook]
---
La classifica dei [dieci migliori portatili secondo Specout](http://laptops.specout.com), in questo momento, vede due Mac ai primi due posti, tre Mac nei primi cinque, quattro Mac nei primi sei e cinque Mac nei primi dieci.

Specout lavora per arrivare a un giudizio complessivo che tenga nota di tutte le variabili in gioco, dal prezzo alle specifiche tecniche al supporto.

Se qualcuno oggi mi chiedesse se fa bene a comprare un Mac, o quale sia il portatile migliore, avrei mediamente pochi dubbi.

A quelli che *Apple non pensa più a Mac* chiedo che razza di interesse debbano avere gli altri, visti i risultati che ottiene Apple senza impegnarsi.
