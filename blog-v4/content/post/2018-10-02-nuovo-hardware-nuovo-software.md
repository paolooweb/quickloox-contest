---
title: "Nuovo hardware, nuovo software"
date: 2018-10-02
comments: true
tags: [iPhone, Xs, Halide, Books, Apple]
---
Ripetuto alla nausea, la tecnologia che si realizza compiutamente solo quando diventa invisibile.

C’è gente che va in crisi di astinenza se la privi dei gigahertz, dei gigabyte, dei nomi di tecnologie e funzioni scelti apposta per farli sembrare importanti e per fare sembrare importante, soprattutto, chi li pronuncia.

Poi c’è il tempo presente. C’è iPhone Xs. Ho già sentito dire *Non mi interessa, è solo una nuova fotocamera*. Già, ma che nuova fotocamera? Quelli della app Halide [ne hanno analizzato le particolarità](https://blog.halide.cam/iphone-xs-why-its-a-whole-new-camera-ddf9780d714c), quelle del sensore, quelle delle opzioni di fotografia computazionale.

>iPhone Xs è una fotocamera completamente nuova. Non è solo un sensore diverso, ma un approccio interamente nuovo alla fotografia, una prima assoluta per iOS. Siccome si appoggia così fortemente sulle esposizioni multiple e sulla fotografia computazionale, le immagini ottenute possono essere assai diverse da quelle scattare in condizioni paragonabili con iPhone precedenti.

Affascinante, che lo studio e la dissezione di un nuovo computer da tasca di concentrino su un componente specifico. Di fatto è uscita una nuova fotocamera, più che un nuovo iPhone. Apple, che da oltre una decine di anni non è più Apple Computer, è un produttore di fotocamere di significato globale.

Questo è il nuovo hardware di cui occuparsi, nei tempi in cui il processore fa ancora la differenza, ma la farà sempre meno e le aree di differenziazione saranno altre, tra cui appunto questa.

E il software? Quelli che *una volta ti davano il manuale*? Apple ha pubblicato le guide [Everyone Can Create](https://www.apple.com/education/everyone-can-create/). Come fotografare, come filmare, come disegnare, come suonare. E c’è la guida *per gli insegnanti*. Tutto gratis, tutto da scaricare come Pdf oppure [come ebook](https://itunes.apple.com/us/book-series/everyone-can-create/id1364129830?mt=11) (per ora in inglese, ma sono in arrivo anche le altre lingue).

Il software. All’Apple Store di piazza Liberty a Milano (e [altri sette](https://9to5mac.com/2018/10/01/today-at-apple-the-big-draw-october-sessions/) in giro per il mondo) organizzano [sessioni di disegno creativo su iPad Pro e Apple Pencil](https://www.apple.com/it/today/collection/the-big-draw/piazzaliberty/?&cid=WWA-DM-TAA_BIGDRAW) in abbinata con il festival The Big Draw.

Il software, si diceva? Si pubblicano [le migliori foto scattate con iPhone Xs](https://www.apple.com/newsroom/2018/10/shot-on-iphone-xs-users-share-their-best/?1538420182).

Un tempo sapevi usare il computer perché riuscivi a stampare, conoscevi le opzioni dei programmi in voga, mettevi le mani dentro il cofano e sistemavi un problema. Adesso la tua capacità si misura da quello che sai creare. E da quanto spicca rispetto alla massa. Sei pronto?