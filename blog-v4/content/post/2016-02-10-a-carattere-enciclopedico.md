---
title: "A carattere enciclopedico"
date: 2016-02-10
comments: true
tags: [Keppol, tipografia, font, San, Francisco]
---
Di recente è saltato fuori un mio vecchio quaderno delle elementari. Non ho fatto le aste, sono troppo giovane. Le singole lettere, quelle sì, le ho scritte e riscritte, su pagine e pagine (ecco, non spontaneamente). In un certo modo la scuola elementare mi ha tenuto un corso di calligrafia. Non era il bello scrivere, quanto lo scrivere e basta. Gli inizi.<!--more-->

Vorrei tornare non tanto alle elementari, magari alle medie, e avere un insegnante che abbia letto il [primo](http://martiancraft.com/blog/2015/10/why-san-francisco/) e il [secondo](http://martiancraft.com/blog/2015/10/san-francisco-part-2/) articolo di Nick Keppol sul font San Francisco usato da Apple nelle sue interfacce attuali.

Uno pensa che si parli del font San Francisco mentre, a tutti gli effetti, sta seguendo un corso di tipografia medio-avanzato. Una piccola e fantastica enciclopedia del carattere nell’età digitale.

Non riuscirò a raccomandarlo abbastanza. Da leggere, memorizzare, conservare, tramandare, riscoprire.

P.S.: parlando di font, Hoefler & Co. ha presentato [Operator](http://www.typography.com/blog/introducing-operator/). A 179 dollari va un poco oltre le mie intenzioni di spesa, però che bellezza.