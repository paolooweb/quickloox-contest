---
title: "Senza ritorno"
date: 2018-08-22
comments: true
tags: [Back, Torna, Mojave, macOS]
---
Un rapido sondaggio: hai mai usato *Torna al mio Mac*?

Non l’ho mai usato; la necessità di configurare il router esterno costituiva una fatica superiore ai vantaggi e all’uso effettivo.

Ora la funzione [sparisce da macOS Mojave](https://support.apple.com/en-us/HT208922) e in effetti, tra iCloud Drive e [Apple Remote Desktop](https://www.apple.com/lae/remotedesktop/), per trascurare le soluzioni indipendenti, sembra abbia fatto il suo tempo, uno nel quale era possibile trovarsi a voler accedere al proprio Mac da un altro Mac.

Ora, o almeno è la mia esperienza, novantanove volte su cento si tratta di accedere a dati su Mac da un apparecchio *diverso da un Mac* e quindi si terrà di codice che prende polvere e spazio su disco.

Ma è davvero così? O mi sbaglio?

Intanto *Ars Technica* cerca lo scandaletto con il titolo [macOS Mojave non supporterà Torna al mio Mac; Apple suggerisce alternative più costose](https://arstechnica.com/gadgets/2018/08/apple-to-end-support-for-back-to-my-mac-feature-when-macos-mojave-drops/), piuttosto forzato. Le alternative (plurale) più costose sono infatti *una* (Apple Remote Desktop) e che io sappia resta possibile la connessione Vnc, attuabile in qualunque fascia di prezzo. Poi ci sono le soluzioni sporche maledette e subito come [Chrome Remote Desktop](https://chrome.google.com/webstore/detail/chrome-remote-desktop/gbchcmhmhahfdphkhkmpfmihenigjmpp?hl=en) e tante altre. Personalmente non sentirò la mancanza di Torna al mio Mac e appunto mi piacerebbe sentire pareri diversi.
