---
title: "Conforme e più conforme"
date: 2024-02-17T01:33:22+01:00
draft: false
toc: false
comments: true
categories: [Software, Hardware]
tags: [Apple Silicon, M1, OpenGL, OpenGL ES, Asahi, Asahi Linux]
---
Sembra ieri, era agosto ma sembra ieri lo stesso, che il team di Asahi Linux annunciava con soddisfazione i [driver OpenGL ES 3.1 per Apple Silicon](https://macintelligence.org/posts/2023-08-27-un-bel-conformismo/).

Sembra ieri, ma sono tre giorni, che [esistono i driver Apple Silicon per OpenGL 4.6 e OpenGL ES 3.2](https://rosenzweig.io/blog/conformant-gl46-on-the-m1.html). Ed è una notizia molto più importante, perché superano la compatibilità offerta da Apple, che si ferma alla 4.1 senza la conformità.

Conformità significa che i driver hanno passato centomila test ufficiali (non è un numero a caso). Vuol dire che, per esempio, si può fare modellazione con Blender in ambiente Linux.

[Asahi Linux](https://asahilinux.org) offre driver conformi allo standard e superiori in qualità e affidabilità a quanto offerto da Apple. Notevole, considerato che Apple ha il controllo dello hardware e invece da Asahi lavorano senza supporto e senza documentazione ufficiale.

Come già scritto, preferisco usare macOS. A dirla proprio tutta, se dovessi rinunciare a macOS sceglierei o cercherei di scegliere [FreeBSD](https://www.freebsd.org). Tuttavia tifo convintamente per avere una buona distribuzione Linux che possa essere scelta senza patemi su un Mac da chi preferisce fare a meno del sistema operativo ufficiale.