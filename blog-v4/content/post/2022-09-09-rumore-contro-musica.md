---
title: "Rumore contro musica"
date: 2022-09-09T03:04:31+01:00
draft: false
toc: false
comments: true
categories: [Hardware, Software]
tags: [iPhone, iPhone 14, Apple Watch, Apple Watch Ultra, Dynamic Island, notch, Six Colors, Jason Snell]
---
Credo di sapere che la musica sia una combinazione organizzata di melodia, armonia e ritmo, quando il rumore è invece disordine se non loro assenza.

Lo scrive bene [Jason Snell su Six Colors](https://sixcolors.com/post/2022/09/underestimating-apples-bold-moves/) a proposito delle [novità presentate da Apple assieme a iPhone 14](https://macintelligence.org/posts/2022-09-08-levento-invisibile/).

>Dynamic Island è un chiaro promemoria dei limiti dei rumor che emergono dalla catena di produzione hardware di Apple. Tutti coloro che hanno riportato forma e dimensione dei fori nello schermo di iPhone 14 avevano assolutamente ragione; eppure non hanno visto una foresta, ma solo alberi. I fori erano solo l’inizio della storia.

Le novità hardware sono relativamente semplici da recuperare. Vengono prodotti apparecchi a decine di milioni, con componenti da mezzo mondo, in fabbriche lontane un oceano. Impossibile che tutto il flusso infomativo sia blindato.

Le novità software sono un’altra cosa. Sono prodotte a Cupertino, in un ambiente protetto e consapevole. Infatti non trapelano e gli esimi commentatori non ne sanno alcunché. Si guidano a vicenda come gli zoppi che evangelicamente finiscono nel fosso, perché gli manca metà della storia.

Quanto a quelli che *predicono*:

>Sarebbe stato molto difficile predire che Apple avrebbe realizzato un elemento di interfaccia con gli angoli arrotondati che danza attorno ai fori e li trasforma da imperfezioni nello schermo a zona informativa.

In altre parole: gente che sa solo predire le cose facili. Siamo un po’ capaci tutti, eh.

>Sottostimiamo Apple a nostro rischio.

Esattamente. O meglio, leggiamo di rumore, quando ci aspetta della musica. Chi è ancora così ingenuo da perdere tempo con gente priva di metà delle informazioni, quando va bene?