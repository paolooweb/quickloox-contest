---
title: "Primi della classe"
date: 2022-11-20T01:45:00+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [Arc, The Verge, Mac]
---
Dice The Verge che [Arc è il sostituto di Chrome che stavamo aspettando](https://www.theverge.com/23462235/arc-web-browser-review).

>Arc è la scommessa più grande [tra i browser alternativi]: un tentativo non solo di migliorare il browser, ma di reinventarlo.

Non è cosa da poco. Che piattaforma avranno scelto quelli di Arc per giocarsi le proprie carte?

>Al momento, Arc è disponibile solamente per Mac, ma stanno lavorando per avere nel prossimo anno le versioni per Windows e mobile.

Per quanto si possa criticare Mac, come piattaforma è ancora quella del 1984: negli anni le applicazioni di successo, oppure innovative, o altrimenti entrambe che sono nate su Mac formano una lista numericamente consistente e qualitativamente succosa.

I primi della classe (in questo caso, della classe dei browser) nascono su Mac e questo significa molte cose in termini di solidità della piattaforma, strumenti per lo sviluppo, soddisfazione dei programmatori.