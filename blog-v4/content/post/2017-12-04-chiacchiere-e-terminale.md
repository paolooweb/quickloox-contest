---
title: "Chiacchiere e Terminale"
date: 2017-12-04
comments: true
tags: [Terminale, Twitter, Lisp, shell]
---
Non so esattamente quando e ho una idea ragionevolmente chiara del perché: userò nel novanta percento del mio tempo a computer il Terminale e di tanto in tanto scriverò piccole cose in Lisp.<!--more-->

In questo quadro sarà inevitabile ricorrere a [strumenti come t](https://github.com/sferik/t): un client Twitter utilizzabile da Terminale.

Oggi sembra assurdo, mi rendo conto. Domani, più facilmente dopodomani, si andrà per voce se non per onde cerebrali, o gesti nell’aria. Oppure tramite la shell. Voglio essere preparato.