---
title: "Marte e il dito"
date: 2021-02-19T03:05:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [Perseverance, Nasa, M1, Mac, Lightroom, Photoshop, MacBook Pro, Ryzen, Amd] 
---
Sul [gruppo Slack](https://app.slack.com/client/T03TMRQRC) associato a questo blog è già stato fatto notare che tutti i portatili presenti nella [sala di controllo dell’*ammartaggio* (ma si può?) di Perseverance](https://www.youtube.com/watch?v=GIooAx_GkJs) sono Mac e quindi ci si passa sopra.

Siccome dovrei anche fare il giornalista ogni tanto, aggiungo un dettaglio non ovvio ai più: [la versione di macOS usata](https://twitter.com/filipeesposito/status/1362510156920201218).

<blockquote class="twitter-tweet"><p lang="en" dir="ltr">NASA computers are still running Catalina 😛 <a href="https://t.co/iOZvzZUhZ5">pic.twitter.com/iOZvzZUhZ5</a></p>&mdash; Filipe Espósito (@filipeesposito) <a href="https://twitter.com/filipeesposito/status/1362510156920201218?ref_src=twsrc%5Etfw">February 18, 2021</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

È giustamente passato più inosservato il lavoro di un fotografo professionista che, per conto di Cnet, ha messo a confronto un MacBook Pro M1 (quindi un modello base) con un PC superconfigurato, entrambi al lavoro su Photoshop e Lightroom.

In emulazione con Rosetta 2 M1 è arrivato dietro, ma appena è stato provato il software nativo di Adobe (ancora in beta), [M1 ha battuto un Ryzen 9 Amd](https://www.cnet.com/news/a-pro-photographer-tests-apples-m1-macbook/) dotato di una scheda grafica di eccezione.

Guardare la Luna ormai è da antichi; è tempo di Pianeta rosso. Intanto, chi lavora con il dito sa che acquisto programmare:

>Per chi faccia il fotografo e consideri un aggiornamento hardware, direi che MacBook M1 è una scommessa sicura. Male che vada si può continuare a usare il software esistente via Rosetta 2 e, nel momento in cui arrivano le versioni ufficiali per M1, i miglioramenti nelle prestazioni – nonché nell’autonomia – saranno estremamente benvenute.

Se uno ci pensa, avere sia le prestazioni che l’autonomia è un buonissimo viatico per la missione di Perseverance. Sulla Terra, può farlo solo M1. Su Marte, buon lavoro, robot.

<iframe width="560" height="315" src="https://www.youtube.com/embed/GIooAx_GkJs" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

*Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile.*