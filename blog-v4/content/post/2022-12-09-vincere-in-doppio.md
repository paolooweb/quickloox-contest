---
title: "Vincere in doppio"
date: 2022-12-09T02:10:10+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [iOS 16]
---
Non sono in grado per ora di dire alcunché di utile su [iOS 16](https://www.apple.com/it/ios/ios-16/); lo sto usando da pochissimo e non ho ancora approfondito i punti che mi interessano.

Peraltro, oggi ho aperto per caso i Contatti e mi sono trovato l’avviso della presenza di duecento duplicati.

Con un tocco, ho chiesto che fossero consolidate le rispettive schede; un solo contatto dai due duplicati, senza perdere informazioni. Un altro tocco ed era tutto finito.

Questo è il sistema che si prende cura di te, invece dell’opposto. Questo è un buon modo di vincere; grazie al servizio.