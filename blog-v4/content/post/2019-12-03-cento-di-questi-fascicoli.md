---
title: "Cento di questi fascicoli"
date: 2019-12-03
comments: true
tags: [Knuth, Taocp]
---
È uscito da un paio di settimane il [fascicolo 5 del volume 4 della Art of Computer Programming](http://www.informit.com/store/art-of-computer-programming-volume-4-fascicle-5-mathematical-9780134671796) di Donald Knuth, ultraottantenne [autore di musica per organo nel tempo libero](https://macintelligence.org/posts/2017-09-22-dall-algoritmo-al-ritmo/) e indefessamente al lavoro sul proprio *opus magnum*, uno che non andrà in pensione neanche a quota duecento.

La [pagina delle notizie sul suo sito personale](https://www-cs-faculty.stanford.edu/~knuth/news.html) rimarrà nello stato attuale per poco, avvicinandosi l’anno nuovo. Knuth scrive di avere 10000 anni (in base tre), propone un [enigma grafico](https://www-cs-faculty.stanford.edu/~knuth/puzzle.jpg), accenna alla [pubblicazione in ebook di uno spin-off del materiale](http://www.informit.com/store/stanford-graphbase-a-platform-for-combinatorial-computing-9780135238257) non più disponibile in cartaceo,  e accenna ai contenuti del fascicolo appena uscito:

>Vengono forniti oltre 650 esercizi, attentamente organizzati per l’autoapprendimento, con risposte dettagliate e, in effetti, a volte anche con risposte alle risposte.

Tra volumi, fascicoli, revisioni, edizioni e integrazioni varie fatico a seguire efficacemente lo sviluppo dell’opera. Nei momenti di stanchezza e frustrazione – una o due volte l’anno accade – mi sorprendo a chiedermi in che momento Knuth lascerà ai posteri la fine del lavoro.

Poi mi riprendo e so di nuovo che l’omaggio migliore da tributare a quest’uomo è considerarlo immortale, almeno fino a che arriverà la sua firma dopo l’ultimo paragrafo dell’ultimo capitolo dell’ultimo volume.

Dopo di che diverrà immortale veramente, assieme a Leonardo, Einstein, Michelangelo e qualche altro. Cento di questi fascicoli, Donald.