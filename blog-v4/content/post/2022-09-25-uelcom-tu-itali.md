---
title: "Uelcom tu itali"
date: 2022-09-25T01:52:37+01:00
draft: false
toc: false
comments: true
categories: [Internet]
tags: [iPad Pro, Agenzia delle Entrate]
---
A seguito di circostanze il cui racconto sicuramente irriterebbe il lettore alla ricerca di fatti o commenti di qualunque interesse, sono proprietario di un nuovo [iPad Pro](https://support.apple.com/kb/SP844?locale=it_IT) (2021, quinta generazione, M1).

Il mio modello precedente era un 2018. Il feeling è immutato (attenzione perché le vecchie custodie non sono compatibili) anche se la presenza di M1 sotto il cofano si percepisce e lo schermo ha certamente guadagnato in luminosità e resa dei colori. La fotocamera è progredita a vista d’occhio. Insomma, anche senza evidenze specifiche, saltare un paio di generazioni rende chiaro il miglioramento complessivo della macchina.

Non riesco ad abituarmi al fatto che basti avvicinare iPhone alla nuova macchina per avviare la configurazione. Continua a sembrare magia. In poco tempo sono ridiventato operativo per le operazioni più immediate e sono molto soddisfatto.

Dopo le prime configurazioni essenziali ho sbrigato alcune commissioni, tra le quali la richiesta di una nuova tessera sanitaria per una figlia, niente più e niente meno che burocrazia.

Ho compilato la richiesta sul sito dell’Agenzia delle Entrate, che è entrato in un ciclo infinito al momento di confermarla. Inviare un form a un server via Web in fondo si fa solo da una trentina di anni, forse non siamo maturi per questa tecnologia così improvvisa. Ho un Lidar dentro una tavoletta spessa mezzo centimetro, ma inviare tre campi alfanumerici costituisce ancora una sfida per la Nazione.

*Uelcome tu itali*. Chi vota, lo faccia per qualcuno in grado di lavorare almeno ai diritti più elementari di comunicazione con una pubblica amministrazione nel Ventunesimo secolo.