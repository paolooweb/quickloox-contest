---
title: "Con app si vola"
date: 2019-11-23
comments: true
tags: [Engst, Tidbits, Delta, Messages]
---
Molto interessante, ancorché prolungato, il racconto di come Adam Engst di Tidbits è [sopravvissuto a un viaggio aereo perseguitato dal maltempo](https://tidbits.com/2019/11/15/using-the-delta-airlines-app-and-apple-business-chat-in-a-snowpocalypse/) grazie all’uso della app della compagnia aerea prescelta e della funzione di Business Chat in Messaggi.

Quest’anno lavorativo dovrebbe essere più tranquillo, ma in quello precedente ho viaggiato in aereo con discreta frequenza e mi sento di ribadire il messaggio di Engst: sempre scaricare la app della compagnia aerea con la quale si vola. Anche se tutto fila liscio, può darsi che aiuti a risparmiare tempo o ad aumentare il comfort complessivo del viaggio.

La parte notevole dell’articolo non è tanto avere una app contenente funzioni utili, che è banale; alla fine Engst ha potuto fruire della possibilità di cambiare volo abbastanza rapidamente, e non molto più di questo. È notevole invece il racconto di una situazione nella quale *la app risolve prima di tutte le altre alternative*.

Il punto è importante eppure trascuratissimo: se un problema è risolvibile con una telefonata di venti minuti, la app dovrebbe risolverlo in diciannove minuti al massimo. O mettercene venti, però con qualità del servizio impeccabile e superiore. Altrimenti tanto valeva non scriverla.

Ci sono per fortuna app che rispettano questo requisito, purtroppo sono meno del totale e non hanno neanche una maggioranza schiacciante.
