---
title: "Core ingrati"
date: 2020-10-13
comments: true
tags: [iMac, Mac]
---
E niente, [apre oggi un iMac 2004](https://twitter.com/timsneath/status/1315067661600972800) per commentare l’unboxing come se fosse quello di una macchina appena uscita.

<blockquote class="twitter-tweet"><p lang="en" dir="ltr">It&#39;s time to upgrade my Mac. The SE/30 on the left has served me very well over the years, but the small monochrome screen is starting to limit my productivity. And the new ones come with a DVD player, a new PowerPC G4 and a built-in Ethernet and modem! <a href="https://t.co/VbjtXLScwJ">pic.twitter.com/VbjtXLScwJ</a></p>&mdash; Tim Sneath (@timsneath) <a href="https://twitter.com/timsneath/status/1315067661600972800?ref_src=twsrc%5Etfw">October 10, 2020</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

È una storia terribile e lo so perché quell’iMac l’ho avuto in casa. Una macchina straordinariamente moderna e però, a leggere adesso le sue specifiche, quasi scappa da ridere.

In realtà non è nemmeno così tagliata fuori come potrebbe sembrare, ma si divaga. La parte terribile della storia è: questa tecnologia corre con una fretta spaventosa e ancora non si vede la fine.

C’è anche, della storia, una parte mirabolante: quello che continua ad arrivare, sempre più capace, sempre più veloce, apre opportunità sempre nuove, supera vincoli che il giorno dopo si riconoscono come troppo stretti per essere tollerati.

Dovrebbe essere un tempo di frontiera, di pionierismo. Le capacità a nostra disposizione, non sfruttate o neanche capite, sono uno spreco che fa impallidire anche la peggiore raccolta non differenziata. Le intelligenze migliori del nostro tempo lavorano per darci un potere inimmaginabile; solo che ci chiedono l’ombra di un impegno e per la maggioranza sembra un oltraggio. Meglio lamentarsi o abbaiare alla luna dei rischi, come se fossero arrivati oggi. Il *divide* oggi è *digital* solo per contingenza: già quando sono state forgiate le spade in bronzo, quelli con le selci scheggiate sono rimasti indietro.

L’ingratitudine o, peggio, il disprezzo ignorante per i processori che abitano le nostre scrivanie, borse e borsette, tasche è il peggiore atteggiamento che si possa avere verso i doni enormi che riceviamo (certo, pagati di tasca nostra, ma quanto ne varrebbe la pena, se solo fossimo consapevoli).

E John Gruber maligno, a [spargere sale sulle ferite](https://daringfireball.net/linked/2020/10/12/sneath-imac-g4):

>Oggi, credo, i telefoni si trovano dove si trovavano i PC nel 2004. (Nel contesto di questa discussione considero gli iPad grossi telefoni).

Ingrati, indegni. Bisogna andare a dormire la sera e chiedersi *che cosa so fare oggi più di ieri?* Perché *loro*, per quanto ancora stupidi come lombrichi nonostante le chiacchiere sull’intelligenza artificiale, ogni giorno mettono su un gigabyte in più, un milione di transistor extra.