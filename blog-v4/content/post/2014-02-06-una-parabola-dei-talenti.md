---
title: "Una parabola dei talenti"
date: 2014-02-06
comments: true
tags: [Sony, Vaio, OSX, Jobs, Morita]
---
Si sapeva da tempo ma appaiono in questi giorni [nuovi dettagli](http://nobi.com/en/Steve%20Jobs%20and%20Japan/entry-1212.html) sulla vicenda di Steve Jobs che, nel 2001, incontrò i vertici di Sony per convincerli a concludere un accordo di licenza riguardante Mac OS X sui portatili Vaio.<!--more-->

Non se ne fece niente, principalmente perché il *management* della multinazionale giapponese ritenne di avere speso abbastanza per ottimizzare i Vaio verso Windows e di non voler spendere ancora per farlo con il sistema operativo di Apple.

Una dozzina di anni dopo [si mormora](http://asia.nikkei.com/Business/Deals/Sony-looking-to-sell-PC-business-to-investment-fund) che Sony stia cercando di liberarsi della propria attività di produzione di personal computer.

Mi sovviene quel passo della [parabola dei talenti](http://www.laparola.net/wiki.php?riferimento=Mt25,14-30):

>Ho avuto paura e sono andato a nascondere il tuo talento sotto terra.

La differenza è che Sony ha scelto di nascondere il proprio talento, al posto di quello del padrone. E adesso arriva la punizione, non divina ma del mercato.