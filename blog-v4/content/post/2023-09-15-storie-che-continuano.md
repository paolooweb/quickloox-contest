---
title: "Storie che continuano"
date: 2023-09-15T11:35:00+01:00
draft: false
toc: false
comments: true
categories: [Internet, hardware]
tags: [Magnetic Media Network, MMN, Computer Stories, Crespi D’Adda, Programma 101, Olivetti, M’useum]
---
È il pensiero del passato come inizio di un continuo che porta fino al presente, che venerdì 6 ottobre alle 18 mi porterà alla Ex portineria della fabbrica in piazzale del Cotonificio 1 a Crespi D’Adda, per l’inaugurazione di [Computer Stories](https://www.computerstories.it).

Computer Stories è un altro bel lavoro degli amici di Magnetic Media Network, che da tanti anni hanno in sede il [M’useum](https://www.mmn.it/museum/) dedicato ai più rappresentativi, rari, particolari, interessanti prodotti informatici a partire da quando tutto è nato.

Apple è molto presente nella collezione (del resto non potrebbe essere altrimenti, perché ha fatto gran parte della storia dell’informatica), ma il resto del mondo è molto ben rappresentato, a partire dal leggendario Programma 101 di Olivetti su cui è stato raccolto e preparato materiale che sarebbe sufficiente a giustificare una esposizione dedicata.

Non è il caso di Computer Stories, che presso il vecchio cotonificio espone invece il meglio del meglio della collezione, con percorsi guidati, incontri, iniziative varie e l’accoglienza speciale in cui sono bravissimi.

L’esposizione va avanti per i tre fine settimana seguenti, 7-8, 14-15 e 21-22 ottobre, dalle 10 alle 18. Scuole, aziende e gruppi possono chiedere e prenotare visite infrasettimanali. Poi si proseguirà a Brescia presso la sala della ex cavallerizza, via Cairoli 9, dalle 14 alle 19, nei weekend di 28-29 ottobre, 4-5 e 11-12 novembre. Anche qui, gruppi società e scuole sono benvenute per visite infrasettimanali.

Tutte le coordinate e le informazioni per decidere sono sul sito di Computer Stories. Di solito rimando direttamente al sito, solo che stavolta l’occasione è ghiotta. Conosco Magnetic Media Network, conosco il M’useum, so bene a che livello di cura, attenzione, empatia organizzano le cose e si rapportano con le persone. Per chi crede nel continuo, è una visita da mettere in programma assolutamente.

Chi volesse, può presenziare all’inaugurazione. Basta un commento qui sotto, un messaggio Slack, Telegram, Signal, messo comunale, va bene tutto e inoltro il nome agli organizzatori.