---
title: "Un lunedì da lezioni"
date: 2021-10-17T00:32:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [MacBook Pro, PowerBook, PowerBook 100, Steven Sinofsky, Comdex, Las Vegas, M1, macOS] 
---
Comincia [così](https://twitter.com/stevesi/status/1449443506783543297) un lungo *thread* Twitter di Steven Sinofsky:

<blockquote class="twitter-tweet"><p lang="en" dir="ltr">Apple Event Monday!! By (no) coincidence next week is also the 30th Anniversary of Mac PowerBook launch at Comdex Las Vegas (last one in October and my first one!) 🚀<br><br>PowerBook *redefined* portables. It also solidified the Apple design group. What a story of innovation💡 1/ <a href="https://t.co/JX1Kgh7Itv">pic.twitter.com/JX1Kgh7Itv</a></p>&mdash; Steven Sinofsky – stevesi.eth (@stevesi) <a href="https://twitter.com/stevesi/status/1449443506783543297?ref_src=twsrc%5Etfw">October 16, 2021</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

>Un evento Apple di lunedì! (Non) per coincidenza la prossima settimana ricorre anche il trentesimo anniversario del lancio dei PowerBook al Comdex di Las Vegas […] PowerBook *ridefinì* i portatili. […] Che storia di innovazione.

Il *thread* [termina](https://twitter.com/stevesi/status/1449443668205600768) in questo modo:

<blockquote class="twitter-tweet"><p lang="en" dir="ltr">25/ The innovation in M1++ and MacOS is so deep and such a stepwise change in computing. Monday will be crazy. Watch carefully.<br><br>PS: Code name for PowerBook project was “TIM” b/c “Time To Market” was all over whiteboards and (apparently) someone asked “What is this TIM?” // END <a href="https://t.co/sheho8GGdY">pic.twitter.com/sheho8GGdY</a></p>&mdash; Steven Sinofsky – stevesi.eth (@stevesi) <a href="https://twitter.com/stevesi/status/1449443668205600768?ref_src=twsrc%5Etfw">October 16, 2021</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

>L’innovazione in M1++ e macOS è profonda e un cambio di passo nel computing. Lunedì sarà una cosa da pazzi. Da guardare con attenzione.

In mezzo, un paio di dozzine di *tweet* affascinanti, una lezione di storia conosciuta e rivelazioni sull’evoluzione dei portatili negli ultimi trent’anni, perfetta per ingannare il tempo fino alla partenza del *keynote* di domani.

In questo periodo i portatili non sono la mia tazza di tè. Ma per un mucchio di persone lo sono e in modo importante. D’altronde, per me lo sono stati per molti anni; poi c’è stato un cambiamento sia nell’offerta sia nel mio modo di guardare al computing. Così ho cambiato idea; non è detto che non la cambi ancora, anzi. Ho avuto un PowerBook 100, il primo della serie, e sono arrivato al 17” con processore Core 2 Duo, senza mai pentirmi neanche per un minuto.

I nuovi MacBook con il successore di M1 avranno diverse cose da insegnare al mercato.

*Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile.*