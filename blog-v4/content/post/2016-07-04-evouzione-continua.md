---
title: "Evoluzione continua"
date: 2016-07-04
comments: true
tags: [apprendimento, evoluzione, Lidia]
---
Lidia oggi ha detto *ancora bastoncini*, nella sua prima frase costruita su un avverbio anziché su un verbo.

Il suo successo evolutivo – non individuale, si parla di cuccioli di uomo in senso generale – sta nel fatto che impara a una velocità sbalorditiva. Due anni fa non era ancora venuta al mondo e sta già maneggiando il linguaggio. Per di più i suoi progressi aumenteranno su base molto più che lineare. E nel contempo la sua struttura corporea produce a pieno ritmo mattoni da costruzione e crescita.

Nel giro di pochi anni un cucciolo d’uomo diventa persona adulta, con capacità inimmaginabili al momento della sua nascita.

A un certo punto però la crescita si interrompe e si passa a una sorta di mantenimento, che successivamente cede il passo a un declino via via più pronunciato. La storia individuale di ciascuno di noi dovrebbe chiarirci che cosa ci porta al successo (su base evolutiva): imparare.

Se il nostro corpo, al contrario di quello di un bambino, smette di crescere e potenziarsi, quanto meno dovremmo allenare il cervello per imparare, imparare, imparare.

Che cosa imparerò quest’estate? Ho qualche idea.

*[Sarebbe interessante quest’estate supportare il [libro di Akko su macOS e dintorni](https://macintelligence.org/posts/2016-06-15-un-kickstarter-per-cambiare-il-libro/). C’è un sacco da imparare lì. [È importante che decolli](https://www.kickstarter.com/projects/295108589/cuore-di-mela-i-segreti-di-apple-iphone-e-mac) su Kickstarter.]*