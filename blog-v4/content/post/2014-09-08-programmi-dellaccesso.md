---
title: "Programmi dell’accesso"
date: 2014-09-08
comments: true
tags: [Accessibilità, Koumanova, Asphi, Handimatica, VoiceOver, Apple]
---
Quanto sotto è tutta farina del sacco di **Rossy Koumanova**, che ha sempre cose più importanti delle mie da dire.<!--more-->

In seguito al recente dibattito su varie testate e blog internazionali, il nostro gruppo di [Universal Access](http://www.universalaccess.it) ha deciso di promuovere, durante la più grande fiera dedicata agli ausili tecnologici per le persone con disabilità, un’iniziativa concreta per approfondire il concetto dell'usabilità e accessibilità di molte *app* per *device* mobili.

La nostra opinione è [questa](http://www.universalaccess.it/cosa-dovrebbe-fare-apple-per-laccessibilita-delle-app/)

Il nostro scopo è quello di illustrare a livello pratico, tramite *workshop* e incontri individuali gratuiti, la facilità con cui tante *app* diventano accessibili per le persone che usano VoiceOver e, contemporaneamente, dare una reale possibilità a tanti sviluppatori italiani di promuovere e mettere in vetrina le loro *app*.

Vi chiediamo quindi di promuovere l’iniziativa e di diffondere questa opportunità.

Potete leggere il [comunicato stampa di Asphi ONLUS](http://www.asphi.it/news/a-a-a-apps-cercasi-call-for-apps-per-handimatica-2014/), che organizza Handimatica, e [l’articolo che abbiamo scritto noi](http://www.universalaccess.it/apps-cercasi-call-for-apps-per-handimatica-2014/).

Vi ringraziamo in anticipo per l’aiuto che ci darete nel far circolare la notizia, questa potrebbe infatti essere una buona opportunità non solo per le persone non vedenti, che potranno così conoscere *app* utili e interessanti, ma anche per gli sviluppatori alla ricerca di nuovi utenti o idee innovative con cui presentarsi sul mercato.
