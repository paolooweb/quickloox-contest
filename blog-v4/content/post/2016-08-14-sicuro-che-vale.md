---
title: "Sicuro che vale"
date: 2016-08-14
comments: true
tags: [Apple, bug, sicurezza, 9to5Mac]
---
Ironia facile su Apple che vara un programma di *bug bounty*, con compensi fino a duecentomila dollari per la rivelazione di un problema di sicurezza, quando ci sono aziende specializzate nel violarla disposte ad arrivare [fino a mezzo milione](https://9to5mac.com/2016/08/10/iphone-hack-bounty-apple-exodus-intelligence/).

Invece è una notizia confortante. Primo, per pirati informatici la falla che vale di più è quella più difficile da ottenere e/o che offre i ritorni migliori una volta sfruttata. Si guardi la classifica in apertura dell’articolo di *9to5Mac*: più un pirata è disposto a pagare, più c’è sicurezza da violare. Bonus: si può scongelare uno di quelli per cui Windows è il sistema più bucato in quanto è quello più diffuso. Mostrargli che la preda più ambita non è quella più diffusa. Congedarlo con disonore.

Secondo, ovvio che Apple – come nessuna altra azienda paragonabile – non si presterà mai a un’asta di fatto a chi offre di più per il *bug*. Ogni minuto qualcuno alzerebbe la posta e il risultato sarebbe trovarsi sotto ricatto costante. Ha più senso offrire ricompense oneste a ricercatori onesti; quelli disonesti continueranno a esserlo indipendentemente dalle cifre offerte in prima istanza, essendo il loro *business* la rivendita delle informazioni a enti che pagano somme esorbitanti, come Stati nazionali, servizi segreti, criminalità organizzata.

Grazie a [Misterakko](http://www.accomazzi.net/divulgazione-informatica/ultimo-libro-luca-accomazzi-0007125.html) per la segnalazione!
