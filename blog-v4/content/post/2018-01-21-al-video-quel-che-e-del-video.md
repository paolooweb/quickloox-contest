---
title: "Al video quel che è del video"
date: 2018-01-21
comments: true
tags: [Pink, Floyd, Legend, MailMaster, Mac, Roma, Auditorium]
---
Grazie a **MailMaster C.** per quanto segue!

§§§

Vedo che la querelle nell'utilizzo di Mac/Win in ambienti vari è sempre viva nel tuo blog, quindi mi permetto di segnalare un caso di parità! :)

Il 25 novembre scorso sono andato all’[Auditorium di via della Conciliazione a Roma](http://www.auditoriumconciliazione.it) ad ascoltare i [Pink Floyd Legend](http://www.pinkfloydlegend.it).

L'esecuzione di per sé non ha brillato, secondo me a causa dell'acustica non perfetta, anche se l'auditorium di solito è fatto proprio per questo e la capacità tecnica di esecuzione è indiscutibile, ma a me e ai miei amici è parso che il mixer audio non fosse ben regolato, eppure eravamo a 4-5 metri dietro il mixer (tradizionalmente uno dei migliori posti per ascoltare
bene i concerti).

In allegato la foto del mixer audio.

 ![Il mixer audio dei Pink Floyd Legend a Roma](/images/mixer-audio.jpg  "Il mixer audio dei Pink Floyd Legend a Roma") 

Gli effetti speciali visivi erano invece regolati da un anonimo portatile: a noi i giochi di luce sono piaciuti! :))

 ![Il mixer video dei Pink Floyd Legend a Roma](/images/mixer-video.jpg  "Il mixer audio dei Pink Floyd Legend a Roma") 

Giusto per farsi due risate, sia chiaro: le guerre di religione non mi sono mai interessate! :)))

§§§

Non tutte le ciambelle escono col buco, il che sta benissimo nel contesto di uno spettacolo con lo schermo circolare. Avevo fatto un pensiero al [concerto](http://www.pinkfloydlegend.it/touring.htm) previsto a Milano per il 26 febbraio, ma se l’audio non è ottimale… ci ripenso.