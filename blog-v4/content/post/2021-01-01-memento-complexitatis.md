---
title: "Memento complexitatis"
date: 2021-01-01
comments: true
tags: [Cook, Roche]
---
Su che portante vogliamo basare quest’anno? Suggerisco: la complessità.

Un bell’esempio di complessità è costituito dai [Biochemical Pathways di Roche](https://www.roche.com/sustainability/philanthropy/science_education/pathways.htm). Poster che se non ho capito male hanno una superficie di circa tre metri quadrati e rappresentano una *panoramica delle reazioni chimiche all’interno delle cellule in varie specie e organi*. Ne sono venuto a conoscenza grazie a un [post di John D. Cook](https://www.johndcook.com/blog/2020/01/16/memento-complexitatis/).

I poster vengono aggiornati meticolosamente da una cinquantina di anni e sono stati distribuiti in centinaia di migliaia di copie, senza contare la consultazione digitale.

Importante: rappresentano un migliaio delle reazioni che è possibile rappresentare. Il numero totale di quelle che andrebbero rappresentate, lavoro ancora da svolgere, è circa *trenta volte* più grande.

Specialmente in tempi come questi dove chiunque ti fornisce senza preavviso lezioni magistrali sul funzionamento o meno dei vaccini (dipende se pro o contro), capire la complessità che sta dietro certe ricadute (in questo caso sanitarie) sulla vita di tutti i giorni è importante.

Un altro esempio di complessità su cui martello da tempo sono i [report di Backblaze](https://macintelligence.org/blog/categories/backblaze/) sulla vita utile dei dischi rigidi. Quando ragioni su centomila dischi, le tue conclusioni sono più interessanti di quello che ti racconta il cuGGGino che ha comprato l’hard disk nuovo o butta via quello vecchio.

Padroneggiare la complessità in modo superiore alla media è un buon sistema per emergere, crescere, lavorare, semplificarsi la vita, risparmiare tempo eccetera. Come viatico per un anno che inizia, è impegnativo, ma promettente.

*Ricordati della complessità*.