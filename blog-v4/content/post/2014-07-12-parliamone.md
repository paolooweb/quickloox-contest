---
title: "Parliamone"
date: 2014-07-13
comments: true
tags: [Xcode, Xcode6, Jobs, Swift, Apple]
---
Si moltiplicano i segnali che mostrano Apple di fronte alla scomparsa di Steve Jobs non come chi incontra una battuta d’arresto bensì come la fine di un capitolo e l’inizio del successivo.<!--more-->

L’ennesimo e più recente è l’apertura di un [blog](https://developer.apple.com/swift/blog/) dedicato al linguaggio di programmazione Swift. La cui versione *beta* adesso è scaricabile a chiunque e non solo agli sviluppatori registrati (la strada più veloce è seguire il *link* fornito nell’articolo e inserire *login* e *password*).

Due cose che prima non sarebbero mai accadute e in cui è impossibile cogliere un lato negativo.