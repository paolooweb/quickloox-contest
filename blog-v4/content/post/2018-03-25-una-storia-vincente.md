---
title: "Una storia vincente"
date: 2018-03-25
comments: true
tags: [Vogel, Avernum, Spiderweb]
---
Tutte le volte che leggo un’[intervista a Jeff Vogel](https://macintelligence.org/posts/2016-07-16-sembra-ieri/), come [quella appena uscita su *Geekwire*](https://www.geekwire.com/2018/im-still-humble-toymaker-chat-indie-rpg-developer-jeff-vogel-seattles-spiderweb-software/), ne esco di umore migliore.<!--more-->

Come [Spiderweb Software](http://spiderwebsoftware.com) produce giochi bellissimi da giocare più che da guardare dal 1995 e non si è ancora fermato. L’ultima opera è il remaster di [Avernum III](http://spiderwebsoftware.com/avernum/avernum3/index.html), per Mac e prossimamente anche per iPad.

Si definisce *ancora un umile giocattolaio* e immediatamente capisci che dietro allo stile tagliato con l’accetta, da America profonda, c’è una persona tutt’altro che banale.

Dichiara *ci sarà sempre qualcuno che vorrà giochi come questi* e capisci che il problema non è sicuramente la possibile falsità dell’enunciato; è che mancano giochi come questi.

La grafica conta, ma è al servizio della storia e non viceversa. Un grande gioco parte da una grande storia. Lui ne ha create decine e tutto sommato la storia più bella è proprio la sua, in affari con la moglie che gli compra il primo Mac quando lui decide che l’università è troppo noiosa. E una laurea *honoris causa* qualcuno gliela potrebbe pure assegnare, a questo punto.
