---
title: "La differenza tra otto e ottanta"
date: 2019-12-28
comments: true
tags: [ Otterbox, iPad, Pro, Symmetry, 360]
---
Ho comprato iPad Pro lo scorso anno con l’urgenza di partire per la Polonia e non c’erano i tempi giusti per ricevere una custodia degna della macchina (un anno dopo, resta una delle migliori di sempre nella classifica personale). Per questo ho arraffato una custodiaccia asiatica da neanche dieci euro, con una sola proprietà che mi interessava: arrivava in tempo.

Dopo un anno con la custodiaccia, babbo Natale mi ha omaggiato di una bella [Symmetry Series 360 Folio Case di Otterbox](https://www.otterbox.com/en-us/apple-ipad-pro-12.9---3rd-gen-/symmetry-series-360-folio-case/77-60995.html) e, se non è bello parlare di denaro riguardo ai regali, si nota che costa tipo dieci volte tanto la custodia che ho usato fino a qui.

Il tema di quanto sia giusto spendere e se alla fine tutti gli oggetti siano uguali, per cui tanto vale spendere meno, rimane sempre di grande attualità e ora ho potuto fare l’esperienza di passare da qualcosa di molto economico a qualcosa di molto curato.

La custodiaccia, nel suo, funziona: ha protetto egregiamente la macchina per più di un anno, accende iPad all’apertura, lo spegne alla chiusura, permette di usare i pulsanti di accensione e di regolazione del volume.

Ciò detto, il *feeling* tattile è mediocre: la copertina (quella che sta sopra lo schermo) è troppo rigida rispetto alla cerniera, troppo morbida, con il risultato che spesso tende a non coprire esattamente iPad perché il suo asse di rotazione non più esattamente parallelo al bordo di iPad.

I pulsanti della macchina, come è consuetudine in queste custodie, sono coperti da maschere che ne replicano il rilievo e permettono di azionarli. Funzionano, ma sono duri e mal separati.

Nove volte su dieci, nell’estrarre iPad dalla custodia, si è avviata Siri, per via della pressione involontaria del tasto di accensione.

Una volta mi è capitato di mettere iPad e custodia separatamente nello zaino, in gran fretta. La custodia era aperta, con la copertina girata sul retro. Più tardi ho estratto iPad dallo zaino senza pensarci troppo; i pulsanti non rispondevano.

Durante il trasporto la tavoletta si era infilata nella custodia, ma *girata di centoottanta gradi* rispetto all’orientamento corretto. I pulsanti non funzionavano perché premevo quelli finti e sotto mancavano quelli veri.

Colpa tutta mia, smemorato, che neanche ricordavo di avere messo nello zaino i due pezzi separati. Tuttavia un buon design dovrebbe rendere impossibile inserire la tavoletta in una posizione non prevista.

Alla fine della sua missione, ringrazio lieto la custodiaccia asiatica che è costata meno di un pranzo al *fast food* e ha svolto benissimo la sua funzione primaria. Sui dettagli c’è stato da accontentarsi, come è giusto che sia visto il dettaglio dello scontrino.

La nuova Otterbox deve dimostrare di reggere la prova del tempo e delle sollecitazioni, ma il primo impatto è godimento: il *feeling* è gustoso, sa di robusto e leggero allo stesso tempo; le geometrie della custodia sono perfette e la copertina rimane sempre obbediente al giusto asse di rotazione; il retro, che è trasparente e lascia vedere quello di iPad, è un tocco di eleganza; una linguetta assicura la perfetta chiusura della custodia senza ingombrare durante l’uso.

La differenza tra spendere otto euro e spenderne ottanta è evidentissima e se qualcuno pensasse che Otterbox tiri a fregare, e che le custodie alla fine sono tutte uguali, sbaglierebbe di brutto. Risparmiare è una bella cosa, accontentarsi non sempre.
