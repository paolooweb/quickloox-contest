---
title: "Buon non compleanno Gimp"
date: 2020-11-27
comments: true
tags: [Gimp]
---
Oggi non è il compleanno di Gimp, ma lo è stato [pochi giorni fa](https://www.gimp.org/news/2020/11/21/25-years-of-gimp/).

È il programma di grafica bitmap che andrebbe usato e insegnato nelle scuole, che dovrebbe stare su qualsiasi computer regalato in occasione del passaggio alle scuole medie.

Ha ispirato numerosi concorrenti di Photoshop e ha ricordato a tutti che esisteva una alternativa all’omologazione.

Oggi è molto migliore di ieri, anche su Mac.

Merita sostegno materiale comunque ci sia possibile e passaparola positivo. Lo dice un adepto di Pixelmator, nel senso che abbiamo bisogno che esista Gimp anche quando non ne abbiamo bisogno.

**Bonus:** chi sa riconoscere tutti i nove riferimenti a sistemi operativi e iniziative open source nell’immagine che apre la pagina? A me, prima di iniziare le ricerche, ne mancano due…