---
title: "Varianze e invarianze"
date: 2015-03-17
comments: true
tags: [MagicTrackpad]
---
Quando ho cominciato a tenere nota della durata delle batterie, mi immaginavo una regolarità piuttosto pronunciata. Invece domina la varianza.<!--more-->

Questo giro ha visto il cambio delle batterie di [Magic Trackpad](https://www.apple.com/it/magictrackpad/), dopo 231 giorni di durata. È il decimo cambio e anche la seconda migliore prestazione. La media è di 154 giorni, praticamente cinque mesi.

E 231 è praticamente un cinquanta percento di spostamento rispetto alla media. Mica poco.