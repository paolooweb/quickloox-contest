---
title: "E un caro saluto al manager"
date: 2023-03-31T17:24:16+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [Stage Manager]
---
Ce l’ho messa tutta, ho ampliato i miei orizzonti o almeno ci ho provato, mi sono sforzato di trovare vie di utilizzo nuove, rivedere i miei processi, sforzarmi di capire. Dopo [un bimestre e oltre](https://macintelligence.org/posts/2023-01-28-la-fortuna-aiuta-gli-audaci/), tuttavia, mi sono stufato e ho spento Stage Manager su iPad.

Accetto critiche e suggerimenti e sono pronto a tornare sui miei passi. Continuo a scommettere che, se viene annunciato davvero un visore per realtà aumentata barra virtuale, dentro ci sarà del software che richiama concetti analoghi. Immagino pure che sarà qualcosa di funzionale e perfetto per il nuovo ambiente. Su iPad, ammetto di non essere riuscito a farne utilizzo concreto.

Intendo dire che non ho mai risparmiato tempo o gesti in modo significativo. In nessuna situazione mi ha salvato o ha rappresentato una piacevole sorpresa. Naturalmente, non interessa il lavoro all’interno delle app e quindi non c’erano da aspettarsi novità in quel senso.

È andata peggio, però. Bella la visualizzazione a tutto schermo, con la cornicetta vuota intorno. Dopo qualche volta mi sono chiesto perché non usarlo tutto, lo schermo, oppure era il caso di farlo. Così ho finito per aggiungere un gesto in più alla routine. Funzionante quando la app accettava di lasciarsi portare a tutto schermo. Non sempre accadeva, per motivi a me poco chiari.

Alcune app (dico anche Mail, mica cose piovute da Marte) erano infastidite dalle finestre e mettevano sotto la tastiera virtuale la riga dell’input. Ho provato a strutture le finestre sovrapponibili per lavorare su due app alla volta, scoprendo che la vista affiancata è molto più efficace, compatibilità e relativi problemi delle finestre sovrapponibili a parte.

Tengo qualche app nel Dock. Sulla home ho le app e i widget che mi servono e non stanno nel Dock. Stage Manager non mi ha mai veramente fatto andare più veloce.

Al che, un caro saluto e ci si risente per una prossima versione.