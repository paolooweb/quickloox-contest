---
title: "Realtà alterata"
date: 2021-08-24T11:24:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [Apple, Csam] 
---
Si parte da Apple che minaccia la libertà e la riservatezza delle persone per la sua [intenzione di inserire strumenti di verifica della presenza di foto valutate come pedopornografiche](https://macintelligence.org/posts/Grandi-e-piccine.html), tanto che [vengono pubblicate lettere aperte](https://cdt.org/wp-content/uploads/2021/08/CDT-Coalition-ltr-to-Apple-19-August-2021.pdf) firmate da liste chilometriche di associazioni per chiedere ad Apple di abbandonare i propri piani.

Si arriva a dichiarazioni filtrate dal processo Apple-Samsung, dove un dirigente di Apple parla di iCloud come [la piattaforma più grande](https://appleinsider.com/articles/21/08/20/apple-exec-said-icloud-was-the-greatest-platform-for-csam-distribution) per distribuire materiale sessualmente abusivo per i minori.

La realtà non corrisponde mai perfettamente alla sua rappresentazione ed è necessario sempre un passo di analisi successivo. Senza approfondire quello che si è visto, letto, ascoltato, notato nei commenti, si può solo perpetuare una immagine alterata della realtà.

Oltretutto è in atto un’offensiva contro i linguaggi di comunicazione, per agganciarli *a priori* a una rappresentazione distorta della realtà, corrispondente a questo o a quello scopo ideologico, politico, moralistico eccetera.

Ieri leggevo del tentativo di [sostituire *allattamento al seno* con *allattamento al petto*](https://twitter.com/curablefury/status/1429452820030316545), perché evidentemente parlare di *seno* non è abbastanza *inclusivo*, qualsiasi cosa debba significare *inclusivo*.

<blockquote class="twitter-tweet"><p lang="und" dir="ltr">NO. <a href="https://t.co/B9AY3tY3Pr">pic.twitter.com/B9AY3tY3Pr</a></p>&mdash; Hakudoshi (@curablefury) <a href="https://twitter.com/curablefury/status/1429452820030316545?ref_src=twsrc%5Etfw">August 22, 2021</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

Viva la realtà aumentata, viva la realtà virtuale, ma la realtà alterata no. Va combattuta costantemente e l’unico modo vero per farlo è ampliare la propria conoscenza.

*Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile.*