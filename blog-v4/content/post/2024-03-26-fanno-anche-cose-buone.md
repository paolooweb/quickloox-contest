---
title: "Fanno anche cose buone"
date: 2024-03-26T02:03:08+01:00
draft: false
toc: false
comments: true
categories: [Internet, Typography]
tags: [Microsoft Design]
---
Mi rimproverano di parlare solo male di Microsoft. ma è semplicemente questione di esperienza, che cosa si vive, che cosa si recepisce. Per esempio, Microsoft Design ha pubblicato un bell’articolo semplice e chiaro su [come la tipografia influenza le scelte e le opinioni delle persone](https://microsoft.design/articles/the-hidden-power-of-typography).

Si vede come la tipografia abbia effetti positivi sulla creatività al pari del cioccolato, per esempio.

Ci sono esempi grafici molto chiari e piacevoli e il tono dell’articolo è autorevole ma leggero il giusto.

Certo, si vantano di avere cominciato il loro viaggio tipografico nel 1992 e sai, non è che quella decina di anni di ritardo sia stata proprio colmata. L’idea di potenziare le funzioni di impaginazione automatica di Office non è esattamente la mia idea di dare potere creativo a chi usa una applicazione.

Però non si può avere tutto. Godiamoci almeno la tipografia.