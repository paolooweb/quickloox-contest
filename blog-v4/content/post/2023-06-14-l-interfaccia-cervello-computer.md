---
title: "L’interfaccia cervello-computer"
date: 2023-06-14T23:27:45+01:00
draft: false
toc: false
comments: true
categories: [Hardware, Software]
tags: [Crispin, Sterling Crispin, Vision Pro]
---
Questa [piccola perla](https://twitter.com/sterlingcrispin/status/1665792422914453506) arriva da un ricercatore che, dice, ha trascorso il dieci percento della sua vita nel Technology Development Group di Apple a creare prototipi di neurotecnologia.

<blockquote class="twitter-tweet"><p lang="en" dir="ltr">I spent 10% of my life contributing to the development of the <a href="https://twitter.com/hashtag/VisionPro?src=hash&amp;ref_src=twsrc%5Etfw">#VisionPro</a> while I worked at Apple as a Neurotechnology Prototyping Researcher in the Technology Development Group. It’s the longest I’ve ever worked on a single effort. I’m proud and relieved that it’s finally… <a href="https://t.co/vCdlmiZ5Vm">pic.twitter.com/vCdlmiZ5Vm</a></p>&mdash; Sterling Crispin 🕊️ (@sterlingcrispin) <a href="https://twitter.com/sterlingcrispin/status/1665792422914453506?ref_src=twsrc%5Etfw">June 5, 2023</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

Nessun *leak*, afferma, ma parafrasi di brevetti visibili a chiunque, solo scritti in modo da proteggere i segreti veri.

>In generale, molto del mio lavoro ha riguardato il riconoscimento dello stato mentale degli utenti in funzione dei dati provenienti dal loro corpo e dal loro cervello mentre si trovano in una esperienza immersiva.

I dati in questione vanno ben oltre la pura rilevazione e cercano di stabilire se c’è curiosità o se la mente vaga nel vuoto, se siamo spaventati o concentrati, invece che rivivere una esperienza passata o altro ancora.

Le informazioni acquisite in ambito sperimentale comprendono ovvietà come il tracciamento dello sguardo ma poi anche attività elettrica del cervello, andamento del battito cardiaco, comportamento dei muscoli, densità del sangue nel cervello, pressione, conducibilità della pelle eccetera.

La ricerca porta a realizzare le condizioni per consentire certe predizioni specifiche e questa è notevole.

>Uno dei risultati più interessanti riguarda la predizione che l’utente sta per fare clic prima che lo faccia davvero. Questo è stato un lavoro immenso e qualcosa di cui vado orgoglioso. La nostra pupilla reagisce prima del clic, in parte perché ci aspettiamo che dopo il clic accada qualcosa. Così possiamo creare biofeedback con il cervello di un utente tramite la rilevazione del comportamento dei suoi occhi e ridisporre l’interfaccia utente in tempo reale così da favorire questa risposta anticipatrice della pupilla.

Mica male.

>È una interfaccia tra computer e cervello, grezza ma molto brillante. Che sceglierei ogni giorno in alternativa a un impianto cerebrale.

Qualcuno ricorderà i bei tempi in cui Apple veniva accusata di [non fare più innovazione](https://macintelligence.org/posts/2020-02-08-innovazione-è/). Quando sento qualcuno raccontare che il suo visore preferito fa tutto e costa meno, chiederò se predice in anticipo il clic grazie a molti anni/uomo di ricerca.