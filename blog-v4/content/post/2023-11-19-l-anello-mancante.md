---
title: "L’anello mancante"
date: 2023-11-19T15:52:41+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [Infocom, avventure, Zork, Suspended A Cryogenic Nightmare, grue]
---
Non so se parliamo di giochi, retrocomputing, conservazione del software, storia dell’informatica, open source. Probabilmente tutto. Ok, [è reperibile il codice sorgente degli interpreti delle avventure testuali Infocom](https://github.com/erkyrath/infocom-zcode-terps).

Per un giocatore appassionato o nostalgico, significa poter ripensare a un setup dove dare qualche attenzione a titoli che, nel nostro piccolo settore, hanno per l’appunto fatto e riscritto un capitolo massiccio di storia.

Per i non iniziati, [Infocom](https://en.wikipedia.org/wiki/Infocom) non è un nome qualunque. Loro hanno affrontato il genere e lo hanno reinventato con storie originali, a volte molto complicate, ambientate praticamente ovunque possibile in termini storico-sociologici, dai pirati alla burocrazia.

L’avventura testuale è noiosa per le mie figlie e suppongo anche per le loro amiche. Per un adulto che abbia sperimentato il potere dell’immaginazione lontano dai supporti visivi e dalla grafica, erano un amplificatore di fantasia. Ognuno di noi porta in mente il suo [incubo criogenico](https://wiki.scummvm.org/index.php?title=Suspended:_A_Cryogenic_Nightmare) personale, che nessun disegno potrà sostituire.

Probabilmente parliamo anche di programmazione. Oggi da un punto di vista della complessità del software sono costrutti relativamente banali, mentre per lo studio di come funzioni un meccanismo di quel tipo il codice sorgente può rivelare e suggerire molto.

Viene voglia di rispolverare [Zork](https://macintelligence.org/posts/2021-08-26-lavoretti-di-ipertesto/) e rianimare il *grue*. Nota a margine: è un altro anello di una catena che porta le avventure Infocom a una immortalità sostanziale. Se il sorgente è libero, ricompilarlo è possibile e adattarlo a nuovo hardware un’idea plausibile. Le avventure? Il [loro sorgente](https://github.com/historicalsource?after=Y3Vyc29yOnYyOpK5MjAxOS0wNC0xNVQyMDowNDowMy0wNzowMM4KzaKS&language=&q=&tab=repositories&utf8=✓) è libero da tempo. L’interprete era l’anello mancante.