---
title: "La piccola bellezza"
date: 2024-03-19T23:39:01+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [macOS, Tipografia, Pixel Envy, Nick Heer]
---
Riporta *Pixel Envy* che non si sa quando, non si sa da quando, ma [la palette Tipografia di macOS ha ricevuto un trattamento di *user experience*](https://pxlnv.com/linklog/typography-palette-sonoma/) che le fa guadagnare molti punti:

>Per molto tempo la palette è stata una asciutta lista di caselle di selezione e triangoli di apertura. Bisognava sapere intanto dell’esistenza della palette e poi conoscere il significato di ciascuna opzione. In una versione recente di macOS, la palette è stata tuttavia aggiornata con icone che visualizzano con chiarezza il significato delle opzioni. Molto più simpatico.

La palette in questione è da sempre un tesoro nascosto di macOS, per chi è in grado di apprezzare la tipografia fine offerta dal sistema e pure di approfittarne. Che riceva attenzione è una cosa buona e giusta. Se ci sarà un computer capace di ridarci la tipografia persa quando siamo passati al digitale, quello sarà per forza di cose un Mac.

Speriamo che la palette possa rendersi più visibile di quanto sia ora e qualcuno, qualcosa illustri al mondo fuori che cosa è possibile fare. Che cosa è *bello* fare. I documenti redatti con un computer potrebbero fare spalancare la bocca dall’eleganza e dalla raffinatezza. È che nessuno sa dove sia la tipografia. Né più come funzioni.

Sembra poco? È talmente tanto che non riesco neanche a trovare un termine di paragone.