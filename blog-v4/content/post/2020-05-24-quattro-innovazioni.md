---
title: "Quattro innovazioni"
date: 2020-05-24
comments: true
tags: [iPhone, watch, Stanford, Apple, Mac, iPad]
---
Cambiare la **modalità** di utilizzo di una categoria di prodotti. C’erano i computer, poi è arrivato Macintosh. Qualunque computer in vendita oggi discende da lì. Qualunque sistema operativo ha una interfaccia grafica possibile che si ispira a quei principî.

Cambiare la **destinazione** di utilizzo di una categoria di prodotti. C’era chi definiva iPhone un *telefono* perché, in una schermata con dodici app, ce n’era una che effettivamente faceva telefonare. Le cose che facciamo con un iPhone, o con Android se è per quello, sono tuttora insospettabili per tanti dirigenti Nokia che ancora soffrono di incubi notturni.

Cambiare il **concetto** alla base di un prodotto. Sono esistiti computer a tavoletta ben prima di iPad, ma iPad ha saputo rivoltare come il classico calzino il concetto fondante.

**Catalizzare l’innovazione**. Mettere sul piatto qualcosa che non sai dove andrà a parare, pronto a essere sorpreso da quello che ci farà il mondo. Magari per arrivarci devi essere innovativo tu stesso, magari no; quello che conta è che il qualcosa permetta di esplorare, sperimentare, aprire strade nuove.

Non so se lo studio in corso di allestimento a Stanford porterà veramente all’[impiego di watch come strumento di diagnosi preventiva di Covid-19](https://redcap.stanford.edu/surveys/?s=YJ3FRCWX8X). Penso che neanche a Stanford lo sappiano.

Se però lo studio avesse successo, watch sarebbe stato catalizzatore di una innovazione formidabile che veramente, totalmente nessuno avrebbe saputo non dico prevedere, ma neanche immaginare.

Vorrei vederli, quelli che *Apple non innova più*. Già per il fatto stesso che una ricerca autorevole sia avviata, una autocritica costituirebbe una posizione onesta.