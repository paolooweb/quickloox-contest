---
title: "Comincia per effe"
date: 2017-09-28
comments: true
tags: [ftp, rsync, scp, sftp, ssh, Cyberduck, Rfc, 2577]
---
Sta montando la polemica perché High Sierra ha eliminato ftp dal nucleo Unix di macOS.<!--more-->

Considerato che [la cosa è nota da giugno](https://forums.developer.apple.com/thread/79056) e la polemica monta a settembre, la rete globale sembrerebbe avere retto al colpo.

Circolano diverse opinioni sul perché. La mia parte dal fatto che Apple impiega come minimo molti mesi a confezionare una nuova versione di macOS. Il successore di High Sierra, almeno a livello embrionale, è già in lavorazione.

Il sistema deve percorrere una lunga e tortuosa strada di test di validazione. Il componente che ha passato un certo test rimarrà quello, anche se nei mesi successivi esce una nuova versione. Se Apple ha iniziato a lavorare a High Sierra con perl alla versione 6.3, invento, perl esce in High Sierra con la versione 6.3. Non importa che intanto sia uscita la versione 6.4: cambiare significherebbe ripercorrere la strada dei test.

Né Apple mette mano alla parte Unix se non è strettamente necessario: un componente viene preso e, se funziona, tenuto com’è. Gli sforzi di programmazione vengono riservati alle parti che Apple costruisce da sé.

Tutta questa premessa serve a dire che, mia opinione, hanno preso ftp com’era al momento di cominciare il lavoro su High Sierra. Era a 32 bit. High Sierra inizia a discriminare il software a 32 bit per arrivare alla sua completa esclusione da macOS da qui, diciamo, a tre anni. Per le ragioni sopra dette non c’era interesse a ricompilarlo per i 64 bit e così ftp è stato tolto. Tornerà eventualmente più avanti, quando sarà comunemente funzionante a 64 bit.

O magari, che sarebbe auspicabile, mai più. Perché si tratta di un protocollo vecchio e tremendamente insicuro, che nel mondo di oggi onestamente mette i brividi. Le alternative Unix presenti nel sistema sono più di una, tutte più sicure ed efficienti, e naturalmente è possibile installare da sé ftp se proprio. Senza contare le applicazioni come [Cyberduck](https://cyberduck.io/) o [Filezilla](https://filezilla-project.org). Se uno vuole insistere a usare ftp lo può fare in un milione di modi. È anche possibile copiare il binario da Sierra a High Sierra, aggiustare i permessi di accesso e funziona.

Ma non dovrebbe.

Il documento [Request for Comments 2577](https://tools.ietf.org/html/rfc2577) (i documenti Rfc sono gli atti di nascita di Internet) si intitola *Considerazioni sulla sicurezza di ftp* ed è stato pubblicato nel 1999. *1999*. Era il secolo scorso. Si apre così:

>La specifica FTP permette a un client di ordinare a un server di trasferire file verso una terza macchina. Questo meccanismo, noto come proxy FTP, dà vita a un problema di sicurezza ben noto. La specifica permette inoltre un numero illimitato di tentativi di inserimento di una password utente. Ciò apre la strada all’acquisizione di password altrui tramite forza bruta.

Quindi, password libere di essere indovinate, possibilità di coinvolgere una terza macchina da parte di altre due. Aggiungo che il traffico non è cifrato, per cui basta mettersi in ascolto sulla porta giusta per captare tutti i dati ftp che si ha voglia di acquisire. Nel 2017 ha senso, una situazione del genere? Proprio no.

Quanto detto finora è la parte razionale del discorso, cui nessuno bada. La ragione autentica per la quale la sparizione di ftp è un bene e speriamo che torni mai più è la seguente.

Le ragioni dei polemisti sono esattamente sovrapponibili a quelle di chi si lamentava per la scomparsa del floppy disk.

Non c’è più alcuna seria ragione concreta per volere ftp di serie dentro macOS. Se non che comincia per effe e allora, se sparisce, ci si deve lamentare.