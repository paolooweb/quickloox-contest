---
title: "iPad dietro le quinte"
date: 2018-12-09
comments: true
tags: [iPad, backend, Web]
---
Dopo tutte le [polemiche](https://macintelligence.org/posts/2018-12-06-pensieri-unici/) sulla presunta incapacità di iPad Pro di sostituire un computer, fa specie leggere uno sviluppatore di backend affermare soddisfatto di [usare un iPad per il proprio lavoro](https://medium.com/@shano/why-im-using-an-ipad-as-my-at-home-machine-2fe3e8949bb2).

Uno sviluppatore di backend (la parte del sistema con cui l’utilizzatore non interagisce), parole sue, *è spesso il più vicino alla fonte dei problemi tecnici. Problemi di rete, di memoria, di hardware, di sistema operativo eccetera*. Difficile non definirlo un professionista, complicato affermare che si limiti a consumare dati senza produrne.

Viene fuori che avere una sola app sullo schermo lo aiuta a concentrarsi sul problema e, anzi, evita la visione a doppia pagina. Salta fuori che, nel suo caso, l’impossibilità di smanettare all’interno di iPad gli libera energie che può dedicare ai problemi reali da risolvere; mentre con una connessione ssh può inserirsi in qualsiasi sistema e affrontare le questioni più complesse senza essere menomato nell’operatività.

Emerge che una macchina Linux infinitamente configurabile e personalizzabile rischia di diventare una fonte di perdite di tempo.

Come si scriveva, ognuno è un caso a sé nella propria storia di utilizzatore. Casi come questo, peraltro, sono molto concreti e sottintendono una qualità del lavoro all’altezza di una qualifica ufficiale e degna di uno stipendio. Diverso dal parlare come della Nazionale e di Masterchef.