---
title: "Editori traditori"
date: 2015-10-03
comments: true
tags: [Mario, Laterza, Limbook, Pearson, Apple, iPad, Lausd]
---
Un giorno leggo **Mario** e il suo [incredibile resoconto](https://multifinder.wordpress.com) di come Laterza tratti i ragazzi con disturbi dell’apprendimento. Cose che non si vedono (quasi) più neanche negli uffici postali.<!--more-->

Un altro giorno ricevo un messaggio:

>Apriamo un caso sulla Pearson che fa i “Limbook” con DVD compatibile solo fino a OS X 10.9 e poi consiglia all’utente di installare una macchina virtuale Windows per usare un semplice lettore multimediale?

Il supporto tecnico di Pearson scrive mail come questa:

>I prodotti stampati in un certo periodo dello scorso anno, come quello in questione, sarebbero perfettamente compatibili con Mac OS 10.10 (e probabilmente anche con 10.11, sebbene questo sistema sia stato rilasciato solo oggi e non possa quindi essere stato oggetto di test).

Le scrive il 30 settembre, dopo che OS X 10.11 è perfettamente disponibile sotto forma di beta pubblica da quasi quattro mesi, a costo zero, presumendo che Pearson abbia voluto risparmiare i 99 euro di iscrizione al programma sviluppatori.

Ricordo che cosa [leggevo in aprile](https://www.edsurge.com/news/2015-04-15-lausd-ditches-ipad-pearson-software-asks-for-big-refund) a proposito di Pearson e del fallimento di una fornitura di 650 mila iPad alle scuole di Los Angeles:

>Siamo prossimi alla fine dell’anno scolastico e la stragrande maggioranza degli studenti è tuttora impossibilitata ad accedere agli strumenti didattici Pearson su iPad.

Mi viene il sospetto che nell’editoria scolastica ci sia qualcosa che non funzioni o che non funzioni più. E che il problema sia pesante.

Servono consulenti? Ne conosco numerosi, competenti e capaci. Però bisogna svegliarsi. Tradire la propria funzione è un conto, farlo a spese delle famiglie e dei ragazzi è disgustoso.