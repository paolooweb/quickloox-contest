---
title: "Un nome, un destino"
date: 2016-02-04
comments: true
tags: [Spicer, Cass, Apple]
---
So che non sarò creduto. Devo ugualmente onorare la verità delle cose. Mi hanno proposto per posta di intervistare Andre Spicer, docente di comportamento organizzativo presso una *business school* londinese, che ha analizzato come segue la situazione di Apple. (Testuale, corpo del messaggio. Ho corretto i refusi. C’erano anche quelli).<!--more-->

>Apple può anche aver registrato i maggiori profitti trimestrali della storia, ma attenzione, potrebbe andare tutto a rotoli nel giro di pochi anni, come è già accaduto a Nokia nel passato. La strategia Apple basata sul fornire una gamma limitata di prodotti a prezzi elevati potrebbe ritorcersi contro l’azienda. Gli smartphone sono sempre più considerati merci indifferenziate, la gente inizierà presto a chiedersi perché pagare tali somme per uno smartphone e Apple potrebbe trovarsi intrappolata.
 
>Le vendite dell’Apple Watch non sono ancora decollate, Apple sta quindi cercando di compensare la carenza di vendite del suo prodotto faro spostandosi in altri mercati come la sanità, i servizi finanziari e le auto. Il grande ostacolo è che questi sono settori molto diversi, complessi ed estremamente regolati, con attori ben consolidati. Non è chiaro come le competenze di Apple nel produrre smartphone trendy si tradurranno in competenze nel settore bancario.
 
>Mi sembra pertanto facile predire uno scenario catastrofico. Ciò che a mio avviso è molto probabile è che Apple passerà dall’eccezionalità alla normalità, e quando ciò accadrà, molte delle abitudini delle tradizionali aziende di mezza età si manifesteranno: taglio dei costi, processi di cambiamento inutili e ripetitivi, ecc. È probabile che gli azionisti metteranno più pressione sulla società per i dividendi e si impegneranno in un mega programma di sostegno finanziario. Alcuni investitori adocchieranno il mucchio di soldi di Apple, sperando di vederselo restituito piuttosto che reinvestito per rilanciare l’azienda. Se ciò accadrà, probabilmente sarà il primo chiodo nella bara di Apple.

Apple non funziona come funzionano le sue concorrenti e questo le ha causato infiniti problemi di credibilità presso gli analisti, i media e la Borsa. Ok. Tuttavia mi aspetto questo livello di faciloneria dalla cassiera dell’ipermercato (con grande rispetto), non da un docente di *business school*. Non c’è una riga che si salvi.

Ho risposto che seguo Apple da trent’anni e il professor Spicer è un emerito imbecille. Non mi hanno ancora ricontattato.

Dimenticavo: Spicer insegna alla [Cass](http://www.cass.city.ac.uk).