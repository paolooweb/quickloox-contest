---
title: "Donne di polso"
date: 2017-10-25
comments: true
tags: [Caldwell, watch, iMore, Android, Gear]
---
È davvero una bella e inaspettata lettura l’[articolo](https://www.imore.com/apple-gets-it-right-why-android-wear-sucks-for-women) di Serenity Caldwell su *iMore* dedicato ai computer da polso, alias *smartwatch*, con quadrante piccolo.

C’è anche una tabella superinteressante in fondo.

Il problema è che l’articolo, pure ben scritto, è anche davvero lungo. Così provo a riassumerlo in grande sintesi.

Tante donne, ma anche diversi uomini, hanno un polso piccolo. watch da trentotto millimetri è più piacevole da usare della versione a quarantadue millimetri.

Solo che praticamente non esistono alternative a watch sulle dimensioni ridotte. A parità di funzioni tutti i concorrenti sono più grandi e a parità di dimensioni si trovano meno funzioni (non sono veri computer da polso, ma sottoinsiemi).

La situazione era così tre anni fa, quando è nato watch, *ed è rimasta uguale*. In tre anni i concorrenti di watch non hanno trovato il modo di offrire le stesse funzioni nelle stesse dimensioni.

Forse che Apple ha un qualche vantaggio tecnologico sul resto del mondo?

E poi vengono a dirti che tutti i computer sono uguali e ci faccio le stesse cose e costa meno. Certo, ma hai al polso l’equivalente tecnologico – scrive Caldwell, non io – di uno schiavettone. E allora il prezzo si spiega meglio.