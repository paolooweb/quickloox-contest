---
title: "Dettagli intelligenti"
date: 2014-05-16
comments: true
tags: [iMovie, iPad, iPhone, Bentley]
---
Questo [video](https://www.youtube.com/watch?v=lyYhM0XIIwU) di Bentley è stato girato con iPhone e montato con iMovie su iPad Air.<!--more-->

Il bello è che è stato montato con gli iPad installati a bordo di una Bentley. E non è uno *spot* Apple.

La raffinatezza del risultato finale sta tutta nei dettagli, della ripresa e del montaggio. Chi mai vorrebbe una piattaforma di lavoro diversa, una volta visto che si può fare con questa?

<iframe width="560" height="315" src="//www.youtube.com/embed/lyYhM0XIIwU" frameborder="0" allowfullscreen></iframe>