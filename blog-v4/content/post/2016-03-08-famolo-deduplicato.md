---
title: "Famolo deduplicato"
date: 2016-03-08
comments: true
tags: [BorgBackup, BorgWeb, Asciinema]
---
Trovo che Time Machine sia una delle cose migliori mai prodotta da Apple in ambito software, ma so di non essere in numerosa compagnia. Negli anni ho interagito con persone che lo sorvegliano per vedere se fa bene il backup, che vogliono armeggiare dentro il disco di copia per sistemarlo a modo loro, che siccome è stato reso semplice allora non soddisfa il desiderio di complicazione di una vita evidentemente troppo tranquilla.<!--more-->

Per tutti costoro saluto con soddisfazione l’esistenza di [BorgBackup](https://github.com/borgbackup/borg), software *open source* per backup deduplicati con compressione e cifratura autenticata. In pratica quello che fa Time Machine più la compressione e se vogliamo la cifratura, che altrimenti richiederebbe FileVault.

BorgBackup si installa e si usa da Terminale. Fosse demotivante, c’è chi lavora a una [interfaccia via *browser*](https://borgbackup.github.io/borgweb/), più amichevole.

Così il lavoro svolto da Time Machine e tenuto accuratamente nascosto da Apple si rivelerà con molta chiarezza.

A margine, [Asciinema](https://asciinema.org) merita assolutamente un’occhiata. Se abbiamo mai dovuto condividere una sessione di lavoro sul Terminale, questo è un modo assolutamente spettacolare. E si installa facilissimo, lasciando fare tutto al salvavita [Homebrew](http://brew.sh).