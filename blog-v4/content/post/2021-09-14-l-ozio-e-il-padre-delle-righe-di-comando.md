---
title: "L’ozio è il padre delle righe di comando"
date: 2021-09-14T01:51:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [Bashcrawl] 
---
Siccome sono più interessato del solito all’[evento Apple di fine estate](https://www.apple.com/it/apple-events/?&mnid=slwQ6XVLE-dc_mtid_20925anz39963_pcrid_544778271732_pgrid_127066316396_&mtid=20925anz39963&aosid=p238&cid=wwa-it-kwgo-features--slid---productid--Brand-AppleLive-Pre-%7Bhashtag%7D) e devo ingannare l’attesa, mi sono messo a giocare a [Bashcrawl](https://gitlab.com/slackermedia/bashcrawl), avventura testuale per il Terminale che insegna i fondamentali della shell.

C’è sicuramente di meglio per ingannare un’attesa, ma volevo qualcosa che fosse sufficientemente lontano dai temi dell’evento.

Bashcrawl è semplice semplice e va bene anche per un neofita del Terminale, purché riesca almeno a portare a termine l’installazione e seguire le istruzioni di [questo articolo](https://opensource.com/article/19/10/learn-bash-command-line-games).

*Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile.*