---
title: "Fatevi un viaggio"
date: 2017-09-24
comments: true
tags: [iPhone, Dediu]
---
Dedicato a tutte le menti finissime che di fronte all’annuncio di iPhone X hanno iniziato a copiare e incollare su Facebook *piuttosto fatevi un viaggio*.<!--more-->

Come nota Horace [Dediu su Asymco](http://www.asymco.com/2017/09/20/good-better-best/),

>Prima del lancio di iPhone 8 e X ho fatto una predizione di quanto sarebbe costato iPhone. Ho concluso che il prezzo di iPhone non sarebbe cambiato. Perché non è mai cambiato. Apple ha raccolto 767.758.000.000 dollari da 1.203.732.000 iPhone venduti fino a fine giugno, o 637,8147 dollari per unità.

Dediu mostra, cifre e grafici alla mano, che iPhone SE da 32 gigabyte occupa una fascia bassa di vendita che non è mai stata così bassa nella storia di iPhone. iPhone X alza il prezzo della fascia alta, come è sempre accaduto.

Alla fine, a parte un lieve aumento costante per via dell’inflazione, il prezzo medio di iPhone rimane straordinariamente costante nel tempo; l’ultima uscita alza il prezzo medio, le uscite più vecchie ancora in commercio compensano l’aumento con una riduzione e intanto si vende una montagna di telefoni nella fascia di mezzo.

Conclude che la gran parte delle vendite avverrà, come è sempre accaduto, nella fascia di prezzo intermedia, quella che lui chiama *un dollaro al giorno*, legata a contratti telefonici attraverso i quali si paga anche il terminale.

>In sostanza, se vedete una scelta tra Buono, Meglio e Migliore, non sottovalutate mai la popolarità di Meglio.

E un sacco di gente potrebbe andare sì a farsi un viaggio, specialmente se vive in un luogo comune.