---
title: "La terra della confusione"
date: 2014-11-14
comments: true
tags: [Genesis, fibra, Chromebook, iPad, broadband, superfast, iBooksAuthor, VisualBasic, education]
---
*Too many men / Too many people / Making too many problems / And not much love to go round / Can’t you see / This is a land of confusion.*<br />
*(Genesis, [Land of Confusion](https://itunes.apple.com/it/album/land-of-confusion/id911731404?i=911731820&l=en))*

Può esserci qualcosa di più preoccupante di una docente che [propone l’insegnamento di Visual Basic nella scuola superiore](https://macintelligence.org/posts/2014-10-29-qualcosa-di-meno-basic/) con la motivazione che fa quello che fa qualsiasi altro linguaggio di programmazione appena decente?

È il passato che si propone al futuro spacciandosi per il presente. La promessa dell’arretratezza come costante. Nella scuola, poi; se si parte male già dalla semina, le speranze sul raccolto sono poche.

Su *Future Cities* la prendono diversamente e titolano [iPad e Chromebook stanno vincendo sul mercato dell’istruzione](http://www.ubmfuturecities.com/author.asp?section_id=407&doc_id=526893). Ma questo è il mondo, dove le cose succedono, mica dove si cambia tutto per fare sì che niente cambi.

Il titolo dell’articolo nasce all’indomani dell’evento [TechEd Europe](http://europe.msteched.com/) di Microsoft, dove ovviamente è stato trasmesso un messaggio aggressivo anche in termini di prodotti per l’*education*. Ma vincono i Chromebook e iPad. Tutto dire.

[iBooks Author](https://www.apple.com/it/ibooks-author/) sembra caduto nel dimenticatoio nonostante aggiornamenti importanti, a noi che combattiamo con libri di testo per le scuole alla ricerca di qualsiasi invenzione digitale che serva a vendere comunque la carta a prezzi da strangolamento delle famiglie. Ecco che cosa si può fare per esempio per [insegnare l’illuminazione agli aspiranti creativi](https://t.co/gOuQ6HTjES). Prezzo: gratis (è pesantuccio, 339 megabyte).

Il pezzo di *Future Cities* fa perno sul Regno Unito. Dove hanno [un piano](http://www.superfast-openreach.co.uk) per collegare il 95 percento delle abitazioni e degli uffici a [banda superveloce in fibra](http://www.superfast-openreach.co.uk/the-big-build/) entro il 2017. *Superveloce* non è solo un termine di marketing ma indica banda passante da ottanta fino a trecento megabit per secondo. Collegano centomila nuove utenze a settimana.

Scommetto che in quelle scuole non gira tutto questo amore per Visual Basic. Là pianificano e realizzano. Qui stiamo nella terra della confusione.