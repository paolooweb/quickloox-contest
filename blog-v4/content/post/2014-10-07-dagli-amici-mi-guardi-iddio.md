---
title: "Dagli amici mi guardi Iddio"
date: 2014-10-07
comments: true
tags: [Gates, Pay, ApplePay, iPod, iPad, Office]
---
Nel 2005 Bill Gates [si pronunciò su iPod](http://www.naturalnews.com/004821.html).<!--more-->

>Non penso che il successo di iPod possa continuare nel lungo periodo, per quanto Apple sia brava.

Il successo di iPod sta effettivamente terminando dopo oltre tredici anni, perché sta finendo l’epoca dei riproduttori di musica, non perché altri abbiano preso il posto di iPod.

Sempre da Gates abbiamo saputo lo scorso marzo che [iPad era penalizzato](https://macintelligence.org/posts/2014-03-28-fai-un-salto/) rispetto ad altri apparecchi similari in quanto, tra le altre cose, mancava Office. Pochi mesi dopo è uscito Office per iPad.

Non ne sbaglia una. [Si è appena espresso](http://www.theinquirer.net/inquirer/news/2373985/bill-gates-applauds-apple-pay-hints-at-windows-nfc-payments-service) su [Apple Pay](http://www.apple.com/apple-pay/):

>Apple Pay è un grande esempio di come un cellulare che identifica il proprio utente in modo forte permetta di attuare una transazione molto economica. […] Il fatto di non avere più bisogno di una carta fisica […] e sia chiaro chi sta all’altro estremo della transazione, è un autentico contributo.

Al che vengono i sudori freddi. Bill Gates funziona meglio quando ce l’hai contro.