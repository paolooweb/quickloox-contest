---
title: "Doppio lavoro"
date: 2014-04-15
comments: true
tags: [Android, iPhone, Google, Lockheimer, Apple, Samsung]
---
Android non è nato come copia di iPhone. Lo ha [testimoniato in tribunale](http://recode.net/2014/04/11/top-android-executive-says-google-didnt-copy-apples-iphone/) Hiroshi Lockheimer, *vice president engineering* di Google, durante la causa Apple contro Samsung.<!--more-->

Lockheimer ha tenuto a sottolineare che lo sviluppo di Android tra 2006 e 2007 è costato molte ore e anche di lavoro molto duro.

Comprensibile, visto che la prima specifica di Android del 2006 [non prevedeva il supporto degli schermi sensibili al tocco](http://recode.net/2014/04/13/heres-what-android-looked-like-before-the-iphone/).

Sviluppo faticoso, lavoro duro. Per forza: dopo avere sgobbato per produrre la prima specifica, hanno saputo di iPhone e hanno dovuto realizzare la seconda a passo di carica.

Lavorare due volte per avere un risultato solo è un buon sistema per faticare.