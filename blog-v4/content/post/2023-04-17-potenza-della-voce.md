---
title: "Potenza della voce"
date: 2023-04-17T19:05:18+01:00
draft: false
toc: false
comments: true
categories: [Internet, Web]
tags: [A2, Strozzi, Marin, Roberto Marin, Filippo Strozzi, podcast]
---
Qualunque altra notizia sia all’ordine del giorno, oggi esce [la nuova puntata di A2](http://www.a2podcast.it/57), il podcast di Filippo e Roberto che stavolta mi ha <s>visto</s> sentito, spero, gradito ospite a parlare di intelligenza artificiale (vera e surrettizia), da ChatGpt in tutte le direzioni.

È un settore letteralmente in ebollizione ma resto convinto che abbiamo detto cose interessanti. Vivo di testo scritto e la voce mi fa sempre un effetto particolare, non capisco mai se vergognoso o esaltante o entrambe le cose. Di sicuro, è un divertimento bellissimo e se fossi, io, riuscito a passare un centesimo di quanto mi sono divertito, avrei raggiunto lo scopo.

Filippo e Roberto sono quelli bravi, che ci lavorano seriamente sopra, e come al solito hanno svolto un lavoro tecnico e organizzativo di prim’ordine. Faccio sempre un po’ fatica ad ascoltare cose: l’ascolto è un medium diverso dalla lettura. Loro, comunque, li ascolto sempre e sono gli unici che riescano a farmi riascoltare.

*Quante fisime, alla fine è un podcast!*

Vero. Ma per me è un podcast A2 è sempre un inizio e sono sicuro che, messi giù gli auricolari, mi butto ad approfondire qualcosa. Chiedo scusa, ora ho da scaricare che non vedo l’ora di sentire.