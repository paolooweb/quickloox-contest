---
title: "Scherzi da rete"
date: 2017-01-18
comments: true
tags: [Lux, Lisp, Java, Emacs]
---
Nutro una passione, per quanto superficiale, per il linguaggio Lisp e per l’editor Emacs.

Scopro che mi creano una variante di [Lisp](https://www.common-lisp.net), con *plugin* per l’editing in [Emacs](https://www.gnu.org/software/emacs/), di nome [Lux](https://github.com/LuxLang).

Non so se inchinarmi grato al gentile omaggio o chiedere i diritti di immagine.

Anche se partire dall’ambiente Java non è un gran che.