---
title: "Scelte di libertà"
date: 2022-03-20T01:47:11+01:00
draft: false
toc: false
comments: true
categories: [Internet, Software]
tags: [LibreItalia, Prompt, Panic, Editorial, Textastic, Asahi Linux]
---
Ho appena rinnovato la mia iscrizione a [LibreItalia](https://www.libreitalia.org) e potresti farlo anche tu. Costa dieci euro (se lo fai entro fine marzo, meglio).

Ogni tanto vengo chiamato in causa perché sostengo il software libero ma uso Mac, macOS, quei *device* *chiusi* come iPad o iPhone eccetera.

Visto che i ragionamenti normali non funzionano tanto, provo a sostenere una opinione paradossale e vediamo se va meglio.

Il software proprietario non mi dà alcun fastidio.

Guai a chi mi tocca [BBEdit](https://www.barebones.com/products/bbedit/), che è software proprietario che più di così non si può.

Su iPad mi è prezioso [Prompt](https://panic.com/prompt/), che è software proprietario. Per scrivere uso [Editorial](https://omz-software.com/editorial/) e da poco ho riportato in squadra [Textastic](https://www.textasticapp.com), ugualmente proprietari.

È una mia *scelta*. Avrei potuto fare diversamente.

Non è che il mondo debba funzionare come desidero io. A volte sbaglio, a volte sono impreparato, spesso ignorante. I campi in cui non ho né la statura né la sfacciataggine di intervenire sono innumerevoli.

Però di una cosa sono sicuro: la libertà di scelta fa bene a tutti.

Senza software libero, non c’è libertà di scelta. Anche se si può scegliere tra tutti i programmi del mondo. Ed è per questo che il software libero va sostenuto, promosso, agevolato, supportato. Perché potrebbe anche sceglierlo nessuno. Ma l’importante è che sia una scelta.

Tutti i mali del mondo tecnologico si palesano quando sparisce la libertà di scegliere.

È appena arrivata [la prima versione *alpha* di Asahi Linux](https://asahilinux.org/2022/03/asahi-linux-alpha-release/) per Apple Silicon. Di installarlo o provarlo, non mi passa neppure per l’anticamera del cervello. Ma non vedo l’ora che diventi pronto per tutti coloro che, invece, vorrebbero un sistema operativo in più a disposizione per il loro Mac.

Non è difficile. Costa un’inezia. Scegli LibreItalia.