---
title: "Il recente Keynote"
date: 2016-04-29
comments: true
tags: [Keynote, Zagabria, iPhone, iPad, Bluetooth]
---
Zagabria è città placida e moderatamente tranquilla. Il traffico delle ore di punta è pesante; per il resto l’atmosfera è rilassata e la zona pedonale del centro è costellata da grandi *pub* all’aperto che già alle cinque del venerdì raccolgono grandi folle. In una piazza si esibivano sempre all’aperto le bambine di una scuola di danza moderna.<!--more-->

Mi hanno portato in locali eleganti oppure alla mano e si è sempre mangiato bene, talvolta benissimo. Tutti sono molto gentili e il cittadino medio parla inglese più che sufficientemente. Unico fastidio è l’esistenza di una valuta locale, il *kuna*, che limita le interazioni a dove accettano le carte di credito oppure impone il cambio. I prezzi sono quasi sempre molto bassi, soprattutto considerato lo standard di qualità.

L’unico Wi-Fi truffa si trova in aeroporto, con connessione gratuita limitata a quindici minuti (benissimo limitarla, però sono davvero pochi). In albergo e nei locali pubblici ai trova sempre un Wi-Fi civile.

In questo contesto ho rimesso alla frusta Keynote, che prendeva polvere da molte settimane. Non avevo ancora visto l’ultimo aggiornamento e sono rimasto piacevolmente sorpreso, dal funzionamento in verticale, dalla stabilità assoluta, dalla risposta sul venerando iPad di terza generazione.

Ho usato iPhone come telecomando via Bluetooth e ho penato inizialmente per apparentare i due apparecchi; un doppio riavvio ha sbloccato la situazione e da lì è stato tutto piacevolissimo. La funzione di puntatore e di disegno estemporaneo sopra le slide è deliziosa. Attenzione a fare qualche prova con i pulsanti di impostazione che appaiono sul’iPhone-telecomando: a volte rispondono con lieve ritardo ed è necessario mantenere il sangue freddo.

Tutto è andato comunque bene, con soddisfazione generale. Keynote si è calato bene nel contesto di Zagabria e ho potuto usarlo in modo placido e moderatamente tranquillo.
