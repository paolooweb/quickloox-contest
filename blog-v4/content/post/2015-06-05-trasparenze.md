---
title: "Trasparenze"
date: 2015-06-05
comments: true
tags: [Safari, Chrome, Firefox, Explorer, Bruxelles]
---
Supponiamo che io sia rimasto colpito dalla [paranoia](http://www.garanteprivacy.it/cookie) che l’Angariante della Privacy proietta sui *cookie* e voglia approfondire il discorso, magari analizzando il codice del mio *browser* (si presuppone un minimo livello di competenza) per capire come si comporta.<!--more-->

Nel caso di Safari è semplice: lo [scarico](http://www.webkit.org). È semplice anche [Chrome](https://www.chromium.org/Home). [Firefox](https://developer.mozilla.org/en/docs/Viewing_and_searching_Mozilla_source_code_online) è marginalmente più macchinoso. Però serve solo un pizzico di buona volontà.

Se volessi fare lo stesso per Internet Explorer, dovrei viaggiare fino a Bruxelles ed essere un politico di rango: così potrei entrare nella struttura apposita [appena aperta](https://blogs.microsoft.com/eupolicy/2015/06/03/microsoft-transparency-center-opens-in-brussels/) dove chi ha il potere decide per le persone comuni e può vedere anche il codice di Explorer, proibito alla plebe.

Il bello è che la struttura Microsoft, costruita per ingraziarsi la politica europea, si chiama *Transparency Center*.