---
title: "Rimane Troppo Farraginoso"
date: 2015-10-15
comments: true
tags: [Riccardo, Rtf, Odt, LibreOffice, TextEdit, Pages, Xcode, iPad, iPhone, WriteToGo, TextEditor, iOS, Mac]
---
**Riccardo** ha ragione da vendere quando [lamenta la mancanza di editor di testo piacevoli ma semplici](http://morrick.me/archives/7431), che non siano Office, per modificare rapidamente un documento Rtf. Cioè per fare qualcosa che su Mac è semplicissimo anche senza mai avere scaricato una app, vista l’esistenza di TextEdit.<!--more-->

Va detto che la situazione sta forse migliorando. Fortunatamente ho esigenze ridottissime di trattamento di Rtf su iPad, per cui mi basta una copia di [TextEditor](https://itunes.apple.com/it/app/texteditor-plain-rich-text/id296222961?l=en&mt=8), programma gratuito, orrendo e presente persino, non saprei perché, in versione Apple Watch. Per dire che c’è persino una soluzione gratuita, per quanto sgradevole.

Pare esistano soluzioni piacevoli, a pagamento ovviamente: [WriteToGo](https://itunes.apple.com/it/app/writetogo-pouch/id725224623?l=en&mt=8) è un esempio, su iPad a 4,99 euro e iPhone a 2,99 euro. In totale 7,98 euro e dalla descrizione del programma dovrebbe davvero essere la vera risposta al problema.

Ho visto anche una *app* di nome [Rtf Write](https://itunes.apple.com/it/app/rtf-write/id822253992?l=en&mt=8), gratis con pubblicità o a 0,99 euro senza disturbi, che dal nome promette almeno qualcosa.

C’è da chiedersi dove stia il problema. Oso supporre che si tratti della farraginosità del formato: basta guardare la [documentazione](http://www.biblioscape.com/rtf15_spec.htm) per capire come il programmatore medio, chiamato a produrre una *app* da vendere a una manciata di euro, abbia poca voglia di metterci il tempo che serve.

Inoltre Rtf è un’arma molto potente per documenti con corpi, grassetti e corsivi formattati in modo semplice. Se invece si chiede troppo, capita che un file Rtf particolarmente arzigogolato metta in crisi il programma che cerca di leggerlo, a maggior ragione se non è lo stesso programma che ha creato il file.

Potrebbe essere una delle ragioni per le quali Apple non ha inserito su Pages per iOS il riconoscimento dei file Rtf e in sé ha anche un senso, solo che porta a un circolo vizioso sgradevole: la presenza di Pages fa ovviamente sì che TextEdit non venga portato su iOS. Da ignorante penso ci vorrebbe poco. Anni fa, in un incontro per programmatori dove mi ero indebitamente imbucato, uno sviluppatore Apple creò la gran parte di TextEdit nel giro di mezz’ora da zero, sotto i nostri occhi. TextEdit era allora – penso anche oggi – sostanzialmente quello che risulta se si mettono assieme tutti i mattoni da costruzione a disposizione di Xcode al momento di creare una *app* capace di trattare il testo.

Oltretutto TextEdit legge non solo Rtf, ma anche – più importante – [Odt](https://www.libreoffice.org/get-help/documentation/), il formato testo usato da [LibreOffice](https://www.libreoffice.org). Su iOS sarebbe una gran comodità. Purtroppo la presenza di Pages inibisce TextEdit senza che Pages stesso si aggiorni con formati che hanno molto senso – Odt – o, come Rtf, sono stravecchi e problematici ma con un certo seguito di utilizzatori.