---
title: "Invasione al porto"
date: 2016-05-06
comments: true
tags: [AAA, Sabbione, Invasioni, Savona, Museum, 5x1000]
---
Sono costretto a perderla e scrivo nella speranza di evitare il rimpianto ad altri, questa notte di [Invasioni Digitali all’All About Apple Museum](http://www.allaboutapple.com/2016/05/invasioni-digitali-all-about-apple-museum/) di Savona.

<blockquote class="twitter-tweet" data-lang="en"><p lang="it" dir="ltr">Siete pronti per una visita guidata notturna? Vi aspettiamo venerdì alle 21!… <a href="https://t.co/45o9HLTO9c">https://t.co/45o9HLTO9c</a> <a href="https://t.co/t9Vf6uMC2T">pic.twitter.com/t9Vf6uMC2T</a></p>&mdash; AllAboutApple Museum (@aaamuseum) <a href="https://twitter.com/aaamuseum/status/727422016374984704">May 3, 2016</a></blockquote> <script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>

C’è la [app ufficiale](http://www.allaboutapple.com/2016/01/museo-apple-app-smartphone/), nascono iniziative da non perdere, come questa o il recente [concerto di Mauro Sabbione](http://www.allaboutapple.com/2016/03/2-aprile-concerto-mauro-sabbione-savona/): il progetto sta finalmente dispiegandosi appieno. Merita di essere sostenuto e appoggiato in tutti i modi. C’è perfino il [cinque per mille](http://www.allaboutapple.com/cinque-per-mille/).

Purtroppo solo con il cuore, ma mi sentirò savonese per una sera.
