---
title: "Con qualche ragione"
date: 2017-10-15
comments: true
tags: [BBEdit]
---
Come continua regolarmente ad accadere – *it still doesn’t suck* – l’[elenco delle aggiunte, delle modifiche e delle correzioni di BBEdit 12](http://www.barebones.com/support/bbedit/notes-12.0.html) giustifica l’acquisto, o l’aggiornamento, oltre qualsiasi altra considerazione.

(Tra l’altro: [CommonMark](http://www.barebones.com/support/bbedit/notes-12.0.html) fa parte delle aggiunte e potenzia l’uso di Markdown).