---
title: "Quelli che mancano"
date: 2022-07-21T01:22:28+01:00
draft: false
toc: false
comments: true
categories: [Internet, Software]
tags: [Emilio, macOS, macOS Ventura]
---
C’è apprezzamento e rispetto per chiunque, ammirazione per tanti, un pensiero speciale per alcuni. Le persone che al dunque mancano *veramente*, però, sono poche.

Oggi avrei volentieri augurato buon compleanno a [Emilio](https://macintelligence.org/posts/2021-11-06-troppo-presto/) e lui mi avrebbe spiegato tutto quello che c’è da sapere sulla beta di [Ventura](https://www.apple.com/macos/macos-ventura-preview/). Se le agende fossero state favorevoli ci saremmo mangiati una pizza a due passi da casa sua e chiacchierato di passato, presente e futuro.

Questo blog ha troppo rispetto della storia per trattarla come se fosse finita e non ci fossero mille altre cose da fare, guardare, leggere, giocare, programmare, imparare, insegnare, vivere. Faccio pochissime eccezioni e questa è dovuta.