---
title: "Se questo è un uomo"
date: 2023-02-18T17:46:17+01:00
draft: false
toc: false
comments: true
categories: [Internet, Software]
tags: [grammatica, Lidia, macOS, Pages]
---
Alla figlia fa comodo una tabella completa di tempi e coniugazioni dei verbi. Mi viene in mente un metodo per chiederne una che magari potrebbe funzionare. La chiedo.

In pochi secondi ottengo una tabella delle coniugazioni dei verbi. Soddisfazione.

Dopo altri pochi secondi, mi accorgo che mancano come minimo gerundio e infinito. È vero che sono un po’ *grammar nazi*, è vero che preferisco un italiano ricco di vocaboli e varietà a un italiano monocorde e di poche parole, è vero pure che credo a quella frase per cui se non abbiamo parole per descrivere un concetto non riusciamo a pensare quel concetto. Insomma, sono un po’ fissato con l’italiano. Mi sembra però che chiedere il gerundio in una tabella di tempi e coniugazioni destinata alla scuola primaria sia anche legittimo e non frutto di ossessione personale.

Chiedo di includere nella tabella gerundio e infinito. Arrivano scuse e una nuova tabella.

Stavolta la guardo subito. L’infinito c’è, però solo al presente. E l’infinito passato? Lo chiedo.

Arrivano scuse e una nuova tabella, con l’infinito passato insieme a tutto il resto. Solo che, ovviamente, non ho fatto adeguata attenzione; anche il gerundio è solo al presente.

Chiedo ancora, sperando che siano finite le parti da dimenticare e rassegnandomi a passare al setaccio la tabella alla ricerca di altri errori o omissioni?

Smetto di chiedere e vado su Google, dove una tabella in forma di immagine già pronta e completa la trovo subito. Seleziono il testo dall’immagine (grazie, macOS) e incollo in un documento Pages. Un po’ di formattazione e bene o male avrò la mia tabella.

Quello cui ho chiesto prima di fare da solo, non lo dovrei volere neanche gratis oppure dovrei riconoscergli venti euro al mese per i servigi che offre?