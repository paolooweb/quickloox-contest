---
title: "Sembra semplice"
date: 2022-03-01T02:33:52+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [Dr. Drang, AppleScript, Shortcuts, Comandi rapidi, scripting]
---
Sembra semplice, a leggere Dr. Drang: [manipolare i tab di Safari mentre sono in secondo piano, dietro BBEdit](https://leancrew.com/all-this/2022/02/switching-safari-tabs-from-bbedit/).

Per chi fa blogging, o anche semplicemente scrive e deve recuperare un URL o una informazione da un sito, questo è contemporaneamente l’uovo di Colombo e il sacro Graal: perché Drang ha *anche* l’automazione per portare automaticamente in BBEdit l’URL del sito in primo piano su Safari, ma sempre dietro BBEdit.

Un comune mortale commuterebbe su Safari, *copia*, oppure trascinamento, per tornare su BBEdit, *incolla*. Pensando di averci messo un attimo, quando sono trascorsi secondi, molti secondi, rispetto a quello che ci mette lui, cioè una combinazione da tastiera.

Quando hai scritto centinaia di articoli, o quando stai scrivendo da un paio d’ore, quei secondi si sommano, tutti, e pesano sempre più, per quanto sembrino pochi.

Sembra semplice e invece è un’altra cosa. La spiega lui stesso:

> Spesso mi dimentico di bloggare a riguardo di queste semplici automazioni. Ho bisogno di ricordarmi che **semplice** ed **efficace** non formano una antinomia.