---
title: "Fatto cento, facciamo centoquattro"
date: 2016-11-15
comments: true
tags: [Apple, Samsung, profitti]
---
Scrivevo che nel trimestre di Natale 2013 Apple [si era messa in tasca l’87,4 percento dei profitti globali](https://macintelligence.org/posts/2014-02-17-si-puo-fare-di-piu/) del mercato dei computer da tasca.

Ora pare che nel trimestre estivo 2016 questa cifra [sia diventata il 103,6 percento](http://www.investors.com/news/technology/click/apple-iphone-grabs-104-of-smartphone-industry-profit-in-q3/).

Non è che tutti gli altri perdano denaro; *quasi* tutti. Samsung, nonostante [esplosioni](https://macintelligence.org/posts/2016-09-03-famolo-modulare/) e problemi vari, è ancora profittevole. Ma questo significa unicamente che tutti gli altri *perdono ancora più denaro*.

Sarà che Apple strangola i concorrenti come è buona abitudine di Microsoft, schiacciandoli con la forza del monopolio? Improbabile: gli iPhone venduti nel trimestre sono il 13,2 percento del totale. Per ogni iPhone ci sono quasi altri sette telefoni venduti. Spazio ce n’è.

Forse Apple pratica una politica dei prezzi aggressiva che costringe i concorrenti a competere sul prezzo svenandosi? No. Tutto si può dire degli iPhone, tranne che costino poco rispetto alla media.

Mi rendo conto di infastidire, ma lo ripeto: comprare un terminale proveniente da una azienda che nel venderlo perde denaro? Boh. Non lo farei.