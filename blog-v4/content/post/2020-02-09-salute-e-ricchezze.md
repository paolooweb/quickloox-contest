---
title: "Salute e ricchezze"
date: 2020-02-09
comments: true
tags: [Apple, Health]
---
Sulle tasse internazionali o sulle condotte monopoliste sono state tutto sommato scaramucce. Sui dati sanitari se ne vedranno invece delle belle.

Inizia già a succedere; la più grande azienda americana nel settore [ha preso rumorosamente posizione](https://www.cnbc.com/2020/02/05/epic-about-60-hospitals-sign-letter-opposing-hhs-proposed-data-rules.html) contro nuove regole proposte dal Ministero Usa della salute che vorrebbero facilitare la condivisione tra enti, sistemi e apparecchi dei dati sanitari degli individui.

Ci sono certo numerosi rischi; in questo momento il mondo delle reti informatiche è una giungla dove chiunque cerca di profilare ogni navigatore e per farlo accaparra tutti i dati che può, senza alcun rispetto per la privacy e per la legge. Esempio recente, la scoperta che [le tavolette grafiche Wacom tengono traccia dei nomi delle app che apriamo](https://robertheaton.com/2020/02/05/wacom-drawing-tablets-track-name-of-every-application-you-open/).

Al tempo stesso, per l’individuo – specie quando sfortunatamente diventa paziente – il poter entrare rapidamente in possesso dei propri dati clinici e poterli passare facilmente a chi preferisce è una arma straordinaria per agevolare una guarigione.

Lo stato delle cose è, come quasi sempre prima che una Apple ci metta il naso, deludente e svantaggioso. Molti anni fa ho subìto una operazione chirurgica importante e l’ospedale mi ha consegnato la cartella clinica: un faldone di fogli di carta, tutti fotocopie distratte e difettose dei documenti originali, e un Cd-Rom di Pdf; quelli delle fotocopie di cui sopra, malamente digitalizzate e memorizzate.

Ho la fortuna, per dire, di non dover condividere questi dati con un altro ospedale; se mi fosse capitato di doverlo fare, le condizioni non sarebbero certo state ottimali.

Per questo gli attuali padroni dei dati cercano di mantenere la propria posizione e chi si affaccia oggi sul settore, come Apple, è interessato a un cambio di regole che potrebbe giocare a favore.

È da notare come negli Stati Uniti ci sia una grossa azienda privata, che costruisce la propria ricchezza su costosissimi sistemi di gestione dati installati nelle cliniche. Pesantemente inadeguati non appena si parla di interoperabilità o di condivisione dei dati.

Da noi è diverso; i dati sanitari di ciascuno sono in larga parte patrimonio statale. Quando, con i canonici venti anni di ritardo, salirà alla ribalta la stessa questione oggi dibattuta negli States, il potenziale di disastro generato dall’incrocio tra la miopia statale e l’avidità privata sarà ingente.
