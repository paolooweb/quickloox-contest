---
title: "Diversificare per prosperare"
date: 2019-06-04
comments: true
tags: [Apple, iPadOS, iOS, WatchOS, AirPods, macOS, Catalina]
---
Non potevo seguire la diretta e scrivo mentre deve ancora rendersi disponibile la [differita](https://www.apple.com/apple-events/june-2019/). Però ho letto più che potevo e un mio conoscente ha scritto *Best.WWDC.Ever.* Devo ancora capire se sia vero; l’impressione è che molti annunci siano arrivati nel modo giusto al momento giusto.

In attesa di addentrarmi nei dettagli e sapere che cosa viene detto in questi giorni, mi piace un mondo l’esistenza di iPad OS; a ogni apparecchio il suo sistema operativo, fatto su misura. Distillato di filosofia Apple.

Ci sarà Mac Pro, c’è iPad usabile per fornire input a Mac, c’è una Apple Pencil con nove millisecondi di latenza, meno della metà di prima. Prima sembrava incredibile, adesso sembrerà vera.

Sui social hanno già iniziato gli sbeffeggi, che sono un ottimo segno. Vuol dire che gli annunci hanno toccato punti dolenti. Agli altri, però. Non vedo l’ora che arrivino Catalina e iPadOS.