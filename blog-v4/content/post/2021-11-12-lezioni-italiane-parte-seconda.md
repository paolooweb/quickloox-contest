---
title: "Lezioni italiane parte seconda"
date: 2021-11-12T03:10:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [Stefano Quintarelli, Quinta] 
---
Nutro poca fiducia nel sistema politico italiano così come nei politici italiani. Per qualche anno abbiamo avuto tuttavia un bravo politico che è anche riuscito a fare funzionare il sistema, pur non riempiendo le cronache del gossip di palazzo.

Stefano Quintarelli ha lasciato (almeno per ora) ufficialmente la politica e ha riassunto la sua esperienza in [un post formidabile](https://blog.quintarelli.it/2021/11/mo-scendo/) che va assolutamente letto nella sua interezza.

Ciò che distingue Quinta (mi permetto il soprannome perché lo conosco) dal novero dei politici comuni è che lui arriva dall’informatica ed è uno dei padri dell’Internet italiana. Una figura di competenza straordinaria che sicuramente non aveva la necessità di occupare uno scranno parlamentare per soldi o per ambizione fine a se stessa.

La sua testimonianza smonta tanti luoghi comuni: il compromesso, in politica, è un buon risultato e un buon compromesso è il migliore risultato possibile (mentre fare battaglie donchisciottesche disposti a concedere nulla porta al fallimento). Il sistema è fondamentalmente sano. Quello che si legge sui media è molto diverso dalla realtà degli eventi. Parlamentari storici oggetto costante di esecrazione pubblica possono rivelarsi esperti conoscitori delle meccaniche dei lavori in aula e risultare strumentali per raggiungere risultati altrimenti irrealizzabili. 

L’elenco delle cose che Quinta è riuscito a fare approvare, aggiustare, scongiurare, proseguire è lunghissimo e resterà a lungo.

La sua esperienza è la prova vivente che servono più persone al governo del Paese con un curriculum informatico di prestigio. E che potrebbe non essere tempo sprecato, come forse verrebbe spontaneo pensare a molte di loro (me compreso, a parte il curriculum di prestigio, che non possiedo).

La grande domanda è: come faremo adesso? Un altro Quinta, nel Parlamento attuale, proprio non si vede. E serve, accidenti se serve.

*Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile.*