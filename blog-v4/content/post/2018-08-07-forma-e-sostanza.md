---
title: "Forma e sostanza"
date: 2018-08-07
comments: true
tags: [Markdown, Pandoc, Office, Word, LaTeX]
---
Sì, tu. Convinto di dover usare Office perché te lo impongono i clienti, l’ufficio, un parente, Giove e Venere in quadratura. Ansioso di pagare un abbonamento per creare documenti di scarsa qualità con un programma sovradimensionato e semiostile. Fissato con la conversione dei documenti Word che ti mandano, che per motivi misteriosi sono uguali a se stessi sulla tua scrivania solo se li apri con Word (sai che sulla mia, quando va malissimo malissimo, manca un font e per il resto sono completamente uguali, e non uso Word da vent’anni?). Che devi per forza avere le macro Visual Basic, anche se finora ti sono servite unicamente a pigliarti del *malware*.

Ecco, a te della sostanza frega niente. La lettera alla scuola per annunciare che tuo figlio va alla settimana bianca, se anche fosse in testo puro, conterrebbe esattamente le stesse frasi. Il memo per i colleghi di ufficio, anche quando lo avessi messo in Arial grassetto invece che Arial chiaro, letto a voce alta suonerebbe identico. Anche la sua versione Pdf, si leggerebbe uguale. La sostanza è sempre la stessa; usi Word irretito da un feticcio per la *forma* (oltretutto malriposto, ma vabbeh).

Ti consiglio la lettura di questo post dedicato alla [creazione di documenti formalmente ineccepibili con Markdown, Pandoc e LaTeX](https://www.avvocati-e-mac.it/blog/2018/7/29/scrivere-i-documenti-con-un-software-per-coding-ha-senso-per-un-avvocato-).

Lo ha scritto un *avvocato*. Se una professione ha una sovrana esigenza di forma nel rivestire la propria sostanza, è questa. Per un avvocato, la non conformità di un documento può diventare una grana enorme, altro che i ritorni a capo o l’interlinea.

La prossima volta che mi mandi gli appunti della riunione in formato Xml, nonostante l’assoluta mancanza di formattazione e di attributi, pensa all’avvocato.