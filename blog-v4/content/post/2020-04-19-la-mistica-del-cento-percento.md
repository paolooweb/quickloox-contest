---
title: "La mistica del cento percento"
date: 2020-04-19
comments: true
tags: [Zannoni, Austin, Wi-Fi]
---
Periodicamente ritorno sulla [questione della didattica a distanza](http://www.macintelligence.org/blog/2020/04/10/gli-assenti-hanno-torto/) perché il dibattito presenta numerosi risvolti che danno sul surreale.

Può capitare di leggere commenti come [questo](https://www.facebook.com/elena.zannoni/posts/10156833045097414), dove si trova una riflessione che ho già visto:

>Ora, [ministro dell’istruzione], lei è in grado di assicurarmi che il 100% dei bambini ha la possibilità di restare in pari con gli altri in questo momento?

Non ho nulla contro chi esprime il concetto, ma sul concetto ho molto da dire. Perché il 100% è un obiettivo finto che serve a opporsi a priori contro l’insegnamento online.

L’Italia è una delle nazioni con il tasso di analfabetismo funzionale più alto al mondo: [il 28 percento nel 2018](https://www.investireoggi.it/economia/la-classifica-dei-paesi-piu-analfabeti-funzionali-triste-primato-dellitalia/).

Contemporaneamente, nel 2015 l’Italia aveva un [tasso di alfabetizzazione del 99,7 percento](https://it.wikipedia.org/wiki/Stati_per_tasso_di_alfabetizzazione). Non del cento percento: del novantanove virgole sette. Qualcuno, per fortuna pochi, non ha avuto la possibilità di restare in pari.

Di chi l’ha avuta, *uno su quattro è un analfabeta funzionale*. Possiamo presumere che ci sia anche qualche *piccolissimo* problema su come gli studenti escono da scuola e concordare sul fatto che probabilmente anche sulle lezioni in aula c’è *qualcosina* da sistemare? Avere la possibilità significa niente: conta come la possibilità viene usata.

Poi si ritorna alla consueta foglia di fico. Non tutti hanno il Pc, non tutti hanno il tablet, non tutti hanno il cellulare, non tutti hanno il tempo, non tutti hanno lo spazio, non tutti hanno lo spaziotempo eccetera.

Sì, la società italiana è imperfetta. Le case non sono tutte identiche. La ricchezza è distribuita in modo ineguale. Ci sono persone intelligenti e persone stupide. Bambini privilegiati e altri maltrattati, purtroppo.

Quindi? A scuola invece tutte le aule sono uguali? Tutti gli insegnanti hanno la stessa capacità? I libri che vengono scelti sono sempre i migliori? Le dotazioni sono uniformi? No.

Buttiamo a mare la scuola dunque? No, lavoriamo per migliorarla. E la scuola a distanza, la buttiamo a mare? No, lavoriamo per migliorarla. Sembrerebbe un ragionamento logico. Invece c’è questa idea che la scuola a distanza sia buona solo quando raggiunge il cento percento di efficacia. Cioè mai.

Nella capitale texana di Austin sono consapevoli che tante famiglie non hanno una buona connessione Internet. Ma le scuole sono chiuse. Allora [hanno attrezzato centodieci scuolabus e li hanno mandati nei quartieri disagiati a fornire servizio Wi-Fi](https://edition.cnn.com/2020/04/14/us/austin-wifi-busses-independent-school-district-trnd/index.html). Invece di lamentare la *mancanza di universalismo*, hanno fatto qualcosa per migliorare la situazione.

Ecco, la mistica del cento percento ha stancato, come tutto quello che serve a soffocare ogni possibilità di progresso. Le persone che vale la pena di ascoltare non sono quelle che dicono *ne manca uno*. Sono quelle che agiscono per averne *uno in più*.