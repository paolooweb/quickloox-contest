---
title: "La percezione del danno"
date: 2016-02-16
comments: true
tags: [Arment, Mintsopoulos]
---
Il dibattito sulla qualità del software Apple recente si è animato in modo interessante grazie a questo [articolo](https://medium.com/@AlexandraMint/apple-s-elephant-in-the-room-5383a43dc413#.ra8kex10q).<!--more-->

La sostanza è che Marco Arment, programmatore di gran nome che aveva sollevato la questione di una diminuzione della qualità del software stesso basandosi su esperienze personali, ha più o meno ritrattato sia pure senza farlo in modo aperto. Piuttosto ha scritto che la gran parte dei problemi da lui incontrati è stata risolta da un singolo aggiornamento di sistema, nel quale notoriamente Apple ha cambiato il motore di riconoscimento delle connessioni *wireless*, tornando a una versione più collaudata dello stesso.

Ragionare sull’esperienza personale è sempre pericoloso al momento di trarre conclusioni globali. Va detto che Arment ha una reputazione ben superiore a quella di Alexandra Mintsopoulos; il suo cambio di rotta, peraltro, è incontestabile.

Lo trovo uno di quei temi dove non serve prendere posizione, quanto accumulare dati. Con un miliardo di apparecchi attivi, se la qualità globale del software provocasse, che so, l’aumento del’uno percento dei *kernel panic*, questo spiccherebbe clamorosamente nelle rilevazioni di Apple. Ed è difficile sostenere che all’azienda manchi il denaro per provvedere, o sia venuta improvvisamente meno la tensione alla qualità, visto anche che gli indici di soddisfazione non mostrano tendenze anomale.

Volendo prendere posizione, sarebbe meglio evitare quegli interventi tipo *Pages non mi fa più la maiuscola a inizio paragrafo*, che attiene a tutt’altra tematica. Il punto non è che le funzioni aumentino o si riducano, ma che Pages stia in piedi o meno. E le conclusioni di Mintsopoulos – Apple non ha un problema di software, ma di relazioni pubbliche – sono discutibili, certo, tuttavia anche bene argomentate. Vanno tenute in conto.