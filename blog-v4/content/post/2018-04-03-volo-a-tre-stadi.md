---
title: "Volo a tre stadi"
date: 2018-04-03
comments: true
tags: [Medium, Apple, Savov]
---
Ora che Apple si avvicina al trilione di dollari di capitalizzazione si moltiplicano gli articoli che spiegano come funziona il meccanismo. In altre parole, copiare la formula sta diventando facile; il problema però è la massa critica.

Questo [articolo su Medium](https://medium.com/rethink-reviews/airpods-and-the-three-stages-of-apple-criticism-fed70b84e435) illustra perfettamente le dinamiche di successo di Apple dal lato oscuro: il riflesso condizionato, articolato in tre fasi, di chi la critica o ne critica i prodotti su base pregiudiziale.

Rabbia, discriminazione, accettazione. Prima sembra assurdo; poi ci si rende conto che non lo ė e allora si degrada la figura dell’acquirente, perché una persona sana di mente non comprerebbe. Infine, nei casi meno disperati, si ammette l’errore. Sempre questi tre stadi, per mandare in orbita un pensiero conformista e condizionato.

Ad avere un centesimo per tutte le volte che ho letto e sentito esattamente queste cose, sarebbe la ricchezza. Invrce è solo la soddisfazione di avere visto giusto anche quando nessun altro intorno lo voleva riconoscere. Niente ricchezza, ma sonno sereno e giornate lunghe e luminose come queste che arrivano.
