---
title: "Uno vale unico"
date: 2016-04-17
comments: true
tags: [iPad, Mac, Tam]
---
Dall’apparizione di iPhone, gli apparecchi iOS hanno sempre rappresentato una forma di democratizzazione della tecnologia. Il primo iPhone era assolutamente uguale per tutti, dal disoccupato che investe su se stesso al sultano del Brunei. In seguito sono apparse differenziazioni nella gamma, a partire dalla quantità di spazio disco, mai comunque paragonabili alla diversificazione che si può avere nel comprare lo stesso Mac, modello base oppure *full optional*.<!--more-->

Per questo è ancora più rimarchevole l’esistenza di [un iPad Pro assolutamente unico](http://www.phillips.com/detail/APPLE-INC/UK050316/303#), creato appositamente per essere donato al museo del *design* di Londra che lo venderà in un’asta di autofinanziamento.

Il [Macintosh del ventesimo anniversario](https://en.wikipedia.org/wiki/Twentieth_Anniversary_Macintosh) venne prodotto in dodicimila esemplari, per dire. Raro è ben diverso da unico.