---
title: "Come volevasi sostituire"
date: 2014-08-26
comments: true
tags: [MacBookPro, iPhone5]
---
Non ho fatto apposta a scrivere [pochi giorni fa](https://macintelligence.org/posts/2014-08-23-manca-la-controprova/) che, in presenza di un difetto riconosciuto e dimostrabile, una multinazionale moderna avvia un programma di sostituzione, non per filantropia ma perché è più conveniente che rischiare il danno di reputazione.<!--more-->

Infatti non avevo idea che sarebbe stato annunciato un [programma di sostituzione gratuita](https://ssl.apple.com/it/support/iphone5-battery/) delle batterie di iPhone 5 per un numero di esemplari definito *very small*, molto piccolo, venduto tra settembre 2012 e gennaio 2013 (maggiori particolari per l’Italia dal 29 agosto).

Un numero *molto piccolo*, su una cinquantina di milioni di venduto, che cosa significa? Uno su mille? Quindi cinquantamila iPhone.

Il supposto difetto nella scheda grafica di MacBook Pro cui accennavo, se reale, investe un numero che potrebbe essere paragonabile, come la metà, o il doppio, comunque nello stesso ordine di idee.

Se tutti i firmatari di petizione e i forumisti incalliti vengono ignorati, c’è di sicuro una ragione. Forse solo che devono avere pazienza e verranno accontentati. Credo sia comunque un’altra.

**Aggiornamento:** la ragione è che [bisognava avere pazienza](http://www.apple.com/it/support/macbookpro-videoissues/).