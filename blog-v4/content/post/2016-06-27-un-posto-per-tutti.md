---
title: "Un posto per tutti"
date: 2016-06-27
comments: true
tags: [accessibilità, iOS]
---
Un buon momento per apprezzare la tecnologia Apple: quando escono le notizie sulle [nuove funzioni di accessibilità](http://9to5mac.com/2016/06/15/accessibility-ios-10-macos-appletv-watch/), in questo caso per iOS 10.

Si tratta sempre e comunque di funzioni che tornano utili a una minoranza ma richiedono uno sforzo di programmazione pari a quello di tutto il resto del sistema. in altre parole, una perdita totale in termini di costi e benefici per l’azienda.

Eppure, per quella minoranza, sono estremamente preziose. Quando si fanno i confronti sui prezzi, le prestazioni, i processori, le risoluzioni, la Ram di bordo, ognuno di quei parametri dove sembra sempre valere la regola che più è meglio è, apriamo sempre un angolo per parlare delle funzioni di accessibilità. Verrà fuori che l’altro aggeggio, quello che *fa tutto e costa meno*, forse costa pure meno. Ma tutto, non lo fa.

*[Le funzioni di accessibilità non sono sempre evidenti e a volte fa comodo trovarle spiegate, per esempio in un libro [il cui capitolo relativo viene aggiornato appena c’è una scoperta significativa](https://macintelligence.org/posts/2016-06-15-un-kickstarter-per-cambiare-il-libro/). Il nuovo [progetto editoriale dell’amico Akko](https://www.kickstarter.com/projects/295108589/cuore-di-mela-i-segreti-di-apple-iphone-e-mac) potrebbe portare vantaggi anche a tanti disabili. Ha bisogno di diffusione e adesioni per andare in porto.]*