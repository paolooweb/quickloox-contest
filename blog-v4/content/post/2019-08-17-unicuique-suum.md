---
title: "Unicuique suum"
date: 2019-08-17
comments: true
tags: [Swift, Dropbox, C++, Java, Objective-C, Kotlin, Rust]
---
Apple non è mai riuscita a fare adottare regolarmente e con efficacia le tecnologie esclusive del proprio sistema operativo (per esempio [AppleScript](https://macosxautomation.com/applescript/firsttutorial/index.html) o [tipografia](https://developer.apple.com/design/human-interface-guidelines/macos/visual-design/typography/) evoluta) alle applicazioni multipiattaforma diffuse. Gli esempi abbondano, a partire dai programmi Adobe come Photoshop o InDesign. Il problema sussiste anche nel mondo open source, dove applicazioni come [Inkscape] (https://inkscape.org/) oppure [LibreOffice](https://www.libreoffice.org/) offrono esattamente lo stesso pacchetto su Linux, Windows e Mac, come minimo comun denominatore. Il meglio che ci si possa aspettare in tutti questi casi è la reinvenzione della ruota (invece di usare le routine grafiche di macOS, Illustrator adopera le proprie) e la media, appunto, è la mancanza di supporto.

La motivazione addotta ha sempre avuto a che fare con i costi e i tempi di sviluppo; molto più veloce e agevole, si è sempre detto, lavorare su una base di codice unificata e uniforme e pazienza se un sistema operativo è migliore degli altri; in nome dell’economia delle risorse, la minestra deve essere uguale per tutti.

L’idea è talmente radicata che il linguaggio di programmazione più diffuso al mondo è [Java,](https://www.java.com/en/download/) che nasce dalla premessa *Write Once, Run Anywhere*: lo stesso codice funziona ovunque lo compili. filosofie simili hanno portato alla nascita di *framework* per creare app iOS e Android a partire da una singola base di codice.

Sembra tutto sgradevole ma necessario, fino a che Dropbox [spiega sul proprio blog](https://blogs.dropbox.com/tech/2019/08/the-not-so-hidden-cost-of-sharing-code-between-ios-and-android/) che la pensavano così, come tutti, ma a differenza di tutti loro hanno fatto i conti. E non conviene.

>Dropbox aveva una strategia di condivisione del codice tra iOS e Android tramite C++. L’idea retrostante era semplice: scrivere una volta sola in C++ invece di due volte, una in Java e l’altra in Objective-C.

>Dopo sei anni abbiamo completamente abbandonato questa strategia e ora usiamo i rispettivi linguaggi nativi di ciascuna piattaforma (primariamente [Swift](https://developer.apple.com/swift/) e [Kotlin](https://kotlinlang.org/)). Questa decisione è dovuta ai costi (mica tanto) nascosti associati alla condivisione di codice.

>Scrivendo codice in modo non-standard [per riuscire a utilizzarlo su ambedue le piattaforme], abbiamo sopportato un sovraccarico di cui altrimenti non ci saremmo dovuti preoccupare, che è risultato più costoso dello scrivere il codice due volte.

La prossima volta che Photoshop o chi per esso non supporta il meglio di macOS e Adobe o chi per essa lo fa passare come sgradevole ma necessario, si tenga presente che è sgradevole e basta.
