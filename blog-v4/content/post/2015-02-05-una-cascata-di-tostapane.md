---
title: "Una cascata di tostapane"
date: 2015-02-05
comments: true
tags: [AfterDark, salvaschermo, ApocalypseNow, MacOS9, GitHub]
---
Se c’è qualcosa che riconcilia con la giornata è la perfetta commistione tra Mac di una volta, nuove tecnologie, programmazione di frontiera e amore per il *nonsense*, è [After Dark](http://bryanbraun.github.io/after-dark-css/) rifatto in Cascading Style Sheet o Css, la sintassi dei fogli stile in Html5.<!--more-->

Si può vedere nel *browser* e il codice è liberamente disponibile su GitHub. È un segno che i tempi sono certo difficili, ma anche straordinari.

After Dark portò un nuovo senso nel mondo dei salvaschermo e i surreali tostapani volanti divennero per mesi un segno distintivo di chi voleva un computer veramente personalizzato.

L’interfaccia stile Mac OS 9 suscita la nostalgia ed è il tocco che mancava. Per fortuna manca la colonna sonora originale (i tostapani cavalcavano assieme alle Valchirie come gli elicotteri nel film *Apocalypse Now*), altrimenti credo che potrei sospendere i lavori e lasciare una finestra di Safari a schermo pieno.