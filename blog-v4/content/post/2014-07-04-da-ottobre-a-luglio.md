---
title: "Da ottobre a luglio"
date: 2014-07-05
comments: true
tags: [Duracell, MagicKeyboardII, consumi]
---
Era il 7 ottobre quando ho inserito nella [Magic Keyboard II](https://www.apple.com/it/keyboard/) le Simply Duracell che si sono definitivamente esaurite oggi. 270 giorni di durata, che portano la media a 329,6.<!--more-->

Sospetto che lasciare la tastiera non spenta anche a Bluetooth disattivato porti a maggiori consumi, perché ricaricabili a parte non sono mai andato sotto i trecento giorni. D’altro canto, questi mesi sono stati certamente di digitazione molto intensa.

Provo a lasciare non spenta anche questa ricarica (con Duracell Plus Power) e vedremo quando tireremo di nuovo le somme.