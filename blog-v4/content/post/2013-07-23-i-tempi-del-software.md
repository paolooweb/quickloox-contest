---
title: "I tempi del software"
date: 2013-07-23
comments: true
tags: [Giochi, iPad, iPhone]
---
Scrive **Fearandil** a segnalare l’uscita della [beta di Evernote 5 per Windows](http://www.engadget.com/2013/07/19/evernote-5-beta-windows/), quasi otto mesi dopo la versione ufficiale per Mac:

>I giochi escono prima su console e Windows, Evernote no ;)

Nessun problema su questioni di produzione o di ampiezza del mercato, se l’accesso diventa ragionevolmente possibile a tutti. Ben diverso dagli anni del Terrore Finestrato, quando comandava il partito unico degli Omologati.<!--more-->

E comunque *Runescape 3* esce per tutti, contemporaneamente. Festeggiando con una colorata [infografica](http://www.runescape.com/media/infographics/rs3_mmorpg_1_million_years_in_making):

>In ogni minuto di gioco su Runescape vengono fabbricate armi bastanti a dieci legioni Romane originali.

Nessuno batte World of Warcraft. Runescape tuttavia, nella sua gratuità, è spettacolare. È una buona ipotesi per un agosto di fuga nel *fantasy*.

A proposito di giochi che arrivano prima o dopo: Runescape 3 è anche stato riscritto in Html5 per fare a meno di Java e funzionare veramente ovunque. Ci vuole [ancora qualche tempo](http://www.pocket-lint.com/news/121085-runescape-3-mmorpg-coming-to-ipad-in-time-will-work-on-some-android-devices-from-22-july-launch) perché arrivi su iPad e iPhone, ma è questione di *quando*, non di *se*.