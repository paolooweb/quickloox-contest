---
title: "Non ci capiamo"
date: 2024-01-28T16:53:52+01:00
draft: false
toc: false
comments: true
categories: [Software, Internet]
tags: [ChatGpt, Ai, intelligenza artificiale]
---
Ci risiamo.

[Un nuovo studio](https://arxiv.org/abs/2310.17567) afferma che i grandi modelli linguistici alla base degli assistenti generativi, superata una certa dimensione, [mostrano comportamenti assimilabili all’*understanding*](https://www.quantamagazine.org/new-theory-suggests-chatbots-can-understand-text-20240122/), alla comprensione delle cose.

Secondo i ricercatori, alcuni testi generati sarebbero impossibili o molto improbabili da ottenere dato che non dovrebbero trovarsi nei dati di *training* oppure, se anche ci fossero, sarebbe difficilissimo che, a causa della mole di dati nel modello, vengano trovati e messi in connessione proprio quelli lì. Perché succeda questo, il sistema deve effettuare della *generalization*.

Chiarisco che, se si ottenesse un sistema software dotato di *understanding* a livello umano o comunque con un certo livello di intelligenza, uscirei in strada a fare i caroselli con il clacson e inviterei chiunque a fare lo stesso, per celebrare un progresso storico dell’umanità.

Solo che non organizzano in diretta planetaria una *demo* che lascia chiunque a bocca aperta e mostri il traguardo ottenuto (per dire, AlphaGo ha sconfitto il campione del mondo umano a telecamere accese con tanto di arbitro ufficiale). Così rimango perplesso.

Verificare un lavoro scientifico in modo rigoroso richiede ore e ore, dunque mi sono accontentato della prova empirica.

Premetto che non sono un abbonato pagante, in quanto i servizi gratuiti mi bastano e avanzano. In teoria, potrei cogliere più facilmente tracce del comportamento di cui sopra con la versione a pagamento, o forse solo con quella. Però la curiosità resta e quindi [ho chiacchierato con il chatbot](https://chat.openai.com/share/0d1e4599-8a1b-45c3-8dd8-2ab57e7332cf), in inglese perché è la sua lingua madre. Tradotto in italiano, è andata così:

>Scrivi la frase inglese più corta contenente tutte le lettere dell’alfabeto e conta quante lettere hai usato.

The quickest brown fox jumps over a lazy dog. (44 letters)

>Inventa una frase più corta.

Jinxed wizards pluck ivy from the big quilt. (39 letters)

La prima frase che ho usato chiedeva *la frase più corta*, superlativo assoluto. Certamente la risposta è priva di qualunque *understanding* e di lavoro originale, dato che il sistema ha semplicemente tirato fuori la più ovvia e conosciuta, certamente presente nei suoi dati di training. Se non altro, ha contato correttamente le lettere.

Ho chiesto allora un comparativo di maggioranza: una frase più corta, implicitamente, *della precedente*.

A parte il conteggio sbagliato, la frase ha effettivamente una lettera in meno. L’avrà inventata il chatbot? Improbabile, dato che [si trova comodamente su Internet](https://www.facebook.com/theSTEPtest/posts/what-is-jinxed-wizards-pluck-ivy-from-the-big-quilt-an-example-ofdailystep-engli/707754242963480/). Aveva in pancia una frase più corta; perché non me l’ha detta subito?

Perché non c’è *understanding*, ma il motore sceglie probabilisticamente le parole più plausibili per la domanda e non ha idea delle caratteristiche di ciò che sceglie. Niente metadati, per così dire. Se mi sbaglio e il sistema ha elaborato in autonomia le due frasi, o almeno una, peggio ancora; genio della manipolazione dei caratteri e lo sapevamo, ma comprensione del contesto uguale a zero.

La conversazione è continuata e la risparmio a chi non abbia voglia di seguire il link. Ho continuato a chiedere frasi ancora più corte. Ne è uscita una più corta delle prime due (conteggio errato), [presente su Internet](https://www.reddit.com/r/Handwriting/comments/cjry29/sphinx_of_black_quartz_judge_my_vow_is_a_million/). Poi un’altra, ancora più corta (conteggio sbagliato), [anch’essa nota](https://www.urbandictionary.com/define.php?term=cwm%20fjord-bank%20glyphs%20vext%20quiz). Dopo, *una più lunga* (conteggio sbagliato), usata addirittura come [titolo per un racconto](https://penteractpress.com/store/pack-my-box-with-five-dozen-liquor-jugs-tom-jenks-amp-catherine-vidler) scritto a pangrammi (frasi contenenti tutte le lettere dell’alfabeto).

Mi sono stufato e ho chiesto *la più corta in assoluto*:

Pack my box. (12 letters)

Il conteggio delle lettere è stato corretto stavolta; in compenso si è perso completamente il contesto. Questo è il solito chatbot di mesi e mesi fa con le consuete tare.

Per i curiosi, esiste almeno un pangramma in inglese che [contiene esattamente ventisei lettere](https://www.prdaily.com/16-clever-pangrams-for-word-lovers/#:~:text=For%20many%20pangram%20enthusiasts%2C%20the,it%20contains%20only%2026%20letters.):

>Mr. Jock, TV quiz Ph.D., bags few lynx.

Il chatbot non lo conosceva? Se davvero fosse capace di generalizzare, avrebbe potuto lavorarci. Lo conosceva? Avrebbe dovuto presentarlo subito. Niente di ciò.

Ma lui lavora probabilisticamente con le parole più vicine alla domanda, giusto? Quindi, se gli chiedo preciso quello che voglio, con parole rigorose, potrei ottenerlo. Così gli chiedo *Scrivi un pangramma in inglese con esattamente 26 lettere* e lui risponde istantaneamente nel modo giusto.

Bravissimo. Solo che ho chiesto esattamente e senza ambiguità la stessa cosa, in due modi differenti, e lui è andato via di testa con uno per restituire con l’altro, paro paro, i suoi dati di training. Tutto bello e utile nonché privo di qualsiasi comprensione.

Sull’*understanding* non ci capiamo proprio.