---
title: "Ritrovare lo stimolo"
date: 2015-08-31
comments: true
tags: [WoW, CoC, Angband, Morgoth, D&D, Hearthstone]
---
Si gioca per vincere. Ho vissuto di basket e la sconfitta è una esperienza utilissima, per capire i tuoi errori e provare a vincere la volta dopo. Ho infilato monete in gran numero in certi videogiochi da bar dove era chiaro che ci fosse una fine. Guardo [Angband](http://rephial.org) con tranquillità solo da quando ho sconfitto a duello Morgoth.<!--more-->

E i giochi che non hanno una fine? [Dungeons & Dragons](http://dnd.wizards.com), [World of Warcraft](http://eu.battle.net/wow/it/?-), [Clash of Clans](https://itunes.apple.com/it/app/clash-of-clans/id529479190?mt=8)? Arrivare al livello più alto è un obiettivo, ma non si vince niente. Anzi, in un certo senso è una sconfitta, perché devi trovare nel gioco un interesse nuovo che prescinda dal livello. E se hai lavorato per livellare, improvvisamente l’esperienza perde valore. 

Che cosa mi tiene in un gioco senza fine? La *curiosità*. Scoprire cose nuove, fare esperienze nuove, trovare strade nuove. Sfortunatamente, c’è Internet e su Internet tutta la conoscenza che mi piace accumulare con la curiosità, già pronta in formato più adatto a uno studioso che a un curioso. Non sono uno studioso.

Sto sperimentando per l’ennesima volta la situazione con Clash of Clans, perché esiste (come per qualsiasi altro gioco) il [wiki dedicato](http://clashofclans.wikia.com/wiki/Clash_of_Clans_Wiki). Come creare un villaggio, è scritto lì. Come difenderlo, è scritto lì. Come attaccarne un altro, è scritto lì. Come utilizzare al meglio le risorse, è scritto lì. Clash of Clans ha lati divertenti e il primo fra tutti che giocare in compagnia è sempre meglio che giocare da soli. Studiarsi il wiki per essere sempre all’altezza della situazione non è un lato divertente. Fare da soli e, talvolta, non essere all’altezza della situazione, ha lati positivi e però diverte solo in modo relativo.

Mi chiedo e chiedo a tutti i progettisti di giochi là fuori: chiaro che i giochi come Clash of Clans sono progettati per spingere all’acquisto di risorse nel gioco e garantire lo stipendio ai progettisti. Chiaro che, con Internet, qualsiasi algoritmo di gioco, per quanto complesso, è destinato a essere svelato e quindi studiabile. Come si fa a creare un gioco coinvolgente, duraturo, di massa e che stimoli i curiosi come e più che gli studiosi?

(Per esempio banale: studiare [Hearthstone](http://eu.battle.net/hearthstone/it/?-) aiuta, ma garantisce pochissimo. Per un curioso ci sono spazi enormi di godimento).

Grazie in anticipo per tutti i commenti.

(I *post* in stile estivo continueranno per qualche giorno ancora, essendo scritti in anticipo e in regime di connessione poca o nulla).