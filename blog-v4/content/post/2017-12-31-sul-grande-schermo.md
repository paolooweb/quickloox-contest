---
title: "Sul grande schermo"
date: 2017-12-31
comments: true
tags: [Vasco, Mac, Fabio]
---
Un anno memorabile per mille cose, anche personali. Anche musicali.

Affido il saluto finale a **Fabio**:

>Se vuoi ricordare il 2017, ti allego per la tua rubrica foto del concerto di Vasco a Modena: nel concetto da Guinness dei primati c'era anche quel computer di quella marca che non fa prodotti per professionisti…

 ![Mac su maxischermo a Modena Park](/images/vasco.png  "Mac su maxischermo a Modena Park") 

Diciamo che, come minimo, raramente è stato mostrato un Mac sul lavoro a così tante persone con un ingrandimento di questo genere.

Grazie sempre per esserci e tanti auguri di passare la mezzanotte con un occhio su orizzonti sterminati. Professionisti o meno, nel 2018 ci attendono un sacco di occasioni.