---
title: "Il trionfo della volontà"
date: 2014-09-23
comments: true
tags: [U2, Mac, iPhone, iPad]
---
Ho scaricato l’album gratis degli U2 su Mac, perché è gratuito e un ascolto non si nega a nessuno.<!--more-->

Non ho scaricato l’album gratis degli U2 su Mac su iPhone, perché ho sedici gigabyte di spazio e ci sta solo lo stretto necessario.

Non ho scaricato l’album gratis degli U2 su iPad, perché con iPad ci lavoro o ci gioco e in entrambi i casi è difficile che ascolti tanta musica.

Da quello che si legge in giro, sembra che gente come me sia un genio dell’informatica o un mostro di volontà. A me pare di essere solamente capace di intendere e volere e poco più. E che, nella circostanza, sia sufficiente.
