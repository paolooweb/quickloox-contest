---
title: "Il Wi-Fi più pazzo del mondo"
date: 2014-05-25
comments: true
tags: [Facebook, Wi-Fi]
---
Alla Fiera di Verona, collegato alla rete *wireless* locale. Su iPhone compare la notifica di un messaggio da Facebook. Si capisce che il messaggio sia piuttosto importante ma non altro, perché la notifica sparisce come è suo mestiere.<!--more-->

Apro la *app* Facebook: nessun messaggio. Vuoi vedere che hanno bloccato Facebook? Provo ad accedere da Safari. Il web funziona che è un piacere, ma niente Facebook.

App Store risponde velocissimo, Messaggi è impeccabile. Permettono tutto tranne Facebook.

Naturalmente guardo l’elenco delle notifiche e leggo il messaggio arrivato via Facebook, anche se sono escluso dal servizio.

A che serve una impostazione del genere? Frequento Facebook unicamente per lavoro, non ne sono tifoso particolare. Ma non vedo ragioni di proibirlo in questo contesto. Specialmente in questo modo pazzerellone, ai confini del ridicolo.