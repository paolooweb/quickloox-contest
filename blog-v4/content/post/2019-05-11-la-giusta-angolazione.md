---
title: "La giusta angolazione"
date: 2019-05-11
comments: true
tags: [iPad, Pro, Animoji, FaceID]
---
L’ho già detto, iPad Pro 12"9 è a stare larghi una delle tre migliori macchine che abbia usato nella vita. Lo uso del tutto intercambiabilmente con il Mac ed è un piacere assoluto.

Uso pochissimo gli *animoji* invece, ne avrò inviati tre da novembre, dei quali uno di prova.

iPad Pro non ha il *notch* degli iPhone serie X; l’hardware di riconoscimento facciale è nascosto nella cornice, sul lato corto  dove si trova il pulsante di accensione.

Questo implica che, se sveglio l’apparecchio mentre sta in posizione orizzontale, FaceID richiede che punti lo sguardo verso destra o verso sinistra, dove si trovano i sensori. Appare una freccia a indicare la direzione dove guardare e un avviso nel caso la fotocamera sia coperta dal palmo della mano. Analogamente, se iPad sta in verticale con la fotocamera in basso, un messaggio avvisa di guardare in giù (senza volerlo scientemente verificare, è facilissimo impugnare l’iPad Pro 2019 in modo da ignorare dove stia la fotocamera).

Sembrerebbe tutto scontato e invece ho inviato il quarto animoji. Con mia totale sorpresa, ottengo l’effetto di guardare nell’obiettivo se lo faccio *verso il centro* dello schermo.

Evidentemente il software effettua una sorta di compensazione e permette di *guardare in camera* anche se la camera si trova a lato.

Ciò non accade quando mi identifico con FaceID. È aperto il dibattito sul fatto che il software di gestione degli animoji sia più recente e sofisticato di quello di autenticazione, oppure se per qualsivoglia motivo si desideri evitare di avere uno strato software di elaborazione dei dati di FaceID al momento di autenticarsi. Il che sembrerebbe anche comprensibile.

In tutti i casi, si è trattato di un classico *Apple moment*, quelle cose che ti sorprendono perché accade esattamente la cosa giusta in modo naturale. *It just works*. Accadono ancora.


