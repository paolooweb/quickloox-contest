---
title: "Belle libertà"
date: 2015-07-07
comments: true
tags: [HT, jailbreak, malware, Ghirardini, Apogeonline]
---
I bei tempi dell’inizio di App Store. C’erano i difensori dei diritti totali globali che predicavano il *jailbreak* di iOS per avere la libertà di installare tutto quello che volevano, contro l’oppressione totalitaria di Apple.<!--more-->

È passato qualche anno e l’altro giorno hanno sottratto quattrocento gigabyte di dati a Hacking Team, società milanese che vende (vendeva, mi sa) strumenti di intercettazione e *malware* vari a governi, anche di Paesi canaglia, e altre istituzioni.

In quei quattrocento gigabyte c’è completamente di tutto e di più e la rete sta scoppiando di divulgazione di tutto il materiale (tutto, poi, boh; non li ho scaricati). Mi permetto di rimandare chi volesse saperne di più all’[articolo di Andrea Ghirardini su Apogeonline](http://www.apogeonline.com/webzine/2015/07/07/hacking-team-hacked).

Ai difensori dei diritti e della frittura totale globale, quelli che la macchina l’ho comprata e posso farci quello che voglio, dedico questo [tweet](https://twitter.com/csoghoian/status/618125764970565632).

<blockquote class="twitter-tweet" lang="en"><p lang="en" dir="ltr">Hacking Team&#39;s inability to develop iOS malware for non-jailbroken iPhones likely explains why FBI director Comey is foaming at the mouth.</p>&mdash; Christopher Soghoian (@csoghoian) <a href="https://twitter.com/csoghoian/status/618125764970565632">July 6, 2015</a></blockquote> <script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>

E chiedo: sicuri sicuri sicuri di non avere *malware* di Hacking Team installato a bordo? E come fate, a essere sicuri? È una bella libertà fare quello che si vuole, a patto di avere le competenze. O finisce che a fare quello che volevano, erano altri.

*(Mi scuso per la pubblicazione irregolare di questo periodo; tornerà regolare tra qualche giorno, una volta terminata la tempesta perfetta di lavoro, famiglia e viaggi.)*