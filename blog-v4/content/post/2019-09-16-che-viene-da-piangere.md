---
title: "Che viene da piangere"
date: 2019-09-16
comments: true
tags: [Cushing, Nicas, keynote, Loop, Dalrymple]
---
Una [lettura brutta e istruttiva](https://www.loopinsight.com/2019/09/11/the-new-york-times-shaming-of-a-crying-reporter-is-shameful/) da Jim Dalrymple di *The Loop*. Il primo protagonista è Jack Nicas del *New York Times* con il suo commento sulla [presentazione di iPhone 11](http://www.macintelligence.org/blog/2019/09/11/come-e-umana-lei/). Meglio: sul pubblico alla presentazione di iPhone 11.

>La cosa strana degli eventi Apple: molti blogger Apple si comportano da tifosi e non da giornalisti. Una persona nell’area media ha letteralmente decretato una standing ovation a Tim Cook; un’altra ha pianto durante uno spot su Watch.

La seconda protagonista è Ellen Cushing di *The Atlantic*:

>Ero io la giornalista che piangeva seduta vicino a Jack. Piangevo perché il video mostrava disabili che superano sfide e anche perché a volte mi viene da piangere che io lo voglia o no…

Il terzo protagonista, vediamo chi lo indovina:

>Non sapevo fossi tu, Ellen! Mi scuso se ti ho offeso. Come ho scritto in seguito, era una pubblicità dal messaggio emotivo e non c’è. Iente di sbagliato nel piangere!

>Né sei un blogger Apple, quindi il mio tweet è sbagliato. Aggiungo: Non c’è alcunché di sbagliato nell’essere blogger Apple.

>A difesa di Ellen, è uno spot strappalacrime e sono probabilmente io ad avere un deficit emotivo per non avere pianto.

Sembra proprio un’altra persona, vero? Invece è lo stesso voltagabbana di prima. (Dalrymple è andato molto oltre, nella sua valutazione).

Invece che guardare il *keynote* guardava la gente attorno a sé in cerca di pretesti per sputare una sentenza. Ed era quello obiettivo, imparziale e distaccato, mentre i blogger Apple sono *tifosi*.

Se penso a quanti così ne ho incontrati sentiti e letti, sempre nel giusto, sempre in cattedra, viene da piangere anche a me a prescindere dal video.

P.S.: come puntualizza Dalrymple, nello Steve Jobs Theater non esiste una zona riservata alla stampa: poltrone di Vip a parte, tutti gli altri posti sono uguali e vengono occupati in nessun ordine. Anche ignaro del contesto, il critico sopra le parti.
