---
title: "Aggiornamenti d’estate"
date: 2017-08-12
comments: true
tags: [Windows, Matteo]
---
Prosegue la serie degli avvistamenti estivi di progresso informatico al lavoro estivo: il testo che segue, ridotto ed essenziale, è di **Matteo** che ringrazio.<!--more-->

**Nei caldi giorni estivi**, ti servo un’immagine fresca fresca, anche se stantia nella ripetitività del soggetto, **l’aggiornamento**.

 ![Vetrina di libreria con Windows malfunzionante](/images/aggiornamento-vetrina.jpg  "Vetrina di libreria con Windows malfunzionante") 

(E chiariamo: se lì ci fosse stata una qualunque Linux box, starebbe ancora funzionando e continuerebbe a funzionare).