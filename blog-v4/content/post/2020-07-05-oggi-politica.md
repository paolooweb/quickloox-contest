---
title: "Oggi politica"
date: 2020-07-05
comments: true
tags: [Amazon, Apple, Drm, Books, YouTube, VHS, Kindle, Tolkien, Gödel, Escher, Bach, Grateful, Dead, Glacier, Eco, Pendolo, Foucault]
---
Nel senso delle cose della _polis_, come dovrebbe significare.

I _rischi della tecnologia_.

Qualcuno ricorderà i rischi del non averla. Non parlo dei Neanderthal, che qualche rischietto lo correvano e vivevano più preoccupati. Parlo della comunicazione, nella sua forma più ampia, compresa quella di massa. E dell’accesso alla comunicazione da parte dei singoli. Qualcuno, ignorante, la chiama _democratizzazione_, come se chi accede alla comunicazione avesse il potere di decidere. Ovviamente è una scemenza, il singolo non ha mai deciso alcunché né mai lo farà.

Qualcuno ricorderà di quando il vinile si consumava e non suonava più uguale. (Prima del vinile dovevi essere ricco). Dovevi ricomprarlo. C’erano le audiocassette, che però avevano mediamente un livello di qualità inferiore e alla fine si degradavano pure loro.

La tecnologia mi ha dato, a me come a qualunque singolo in grado di mantenersi con minima tranquillità, il potere di reperire musica con qualità altissima e poterla mantenere arbitrariamente, fino a che mi andrà di fare backup. YouTube mi permette di guardare concerti che a momenti non ero ancora nato. Non vorrebbe che io li scaricassi, ma potrei farlo. Ho visto dei Grateful Dead con tutta la qualità di un vecchio VHS sopravvissuto cinquant’anni, cioè orrenda. Senza tecnologia nessuno li avrebbe mai registrati e non potrei ascoltarli. Senza la tecnologia di oggi quei VHS non funzionerebbero oppure vorrebbero vendermene uno, a un prezzo indecente per la qualità, a una qualità indecente per il prezzo. E dovrei tenere in vita un lettore. Senza la tecnologia del 2020 non avrei mai visto un concerto dei Grateful Dead del 1970.

Proviamo con i media scritti. Ho cominciato nell’editoria. Pubblicavi solo se avevi un editore. Avevi un editore, non giriamoci intorno, se eri raccomandato, oppure se vendevi carrettate. Altrimenti eri nessuno. Esisteva un sottobosco di gente che campava di finte promesse a chi sognava la celebrità letteraria, già raccontato da Eco nel _Pendolo di Foucault_ e che devo avere già citato. Domani scrivo della mia esperienza diretta.

Sui giornali non era diverso. Per scrivere sui giornali dovevi essere giornalista, un esame di Stato, vincoli assurdi, raccomandazioni a raffica. Per diventare _qualcuno_ dovevi piegarti alla politica dei politici (non a quella della _polis_) e ai voleri dell’editore, che ci si compromettevano. Tanti degli idoli giornalistici di ciascuno di noi sono persone spregevoli che per soldi e carriera si sono costruiti addosso un personaggio ipocrita. Non tutti, eh. Ma a dire nove su dieci si sbaglia poco.

E oggi, con la tecnologia? Chiaro che non esiste un diritto di essere letto. Ma chiunque pubblica come vuole, dove vuole, quando vuole. Le carriere esistono ancora, chiaro, ma nessuno è più _nessuno_, almeno in potenza. Quanto meno può misurarsi e scoprirlo, se proprio.

Parliamo di chi legge. Uno dei problemi dell’editoria libraria di oggi è che i prezzi, rispetto a qualche inverno fa, sono ridicolmente bassi. Comprare un libro di carta non è mai costato così poco. Ci sono gli ebook, che ti fanno acquistare magari un Kindle (come sineddoche dei lettori di eBook) ma ti fanno risparmiare una libreria in casa. Come avremmo fatto fatto, da poveri, a leggere – che so – Shakespeare o Petrarca, in quell’altra epoca? Saremmo andati in biblioteca.

E se domani lo Stato diventasse autoritario e decidesse di chiudere le biblioteche? E se domani decidesse che Shakespeare non è più gradito e va levato dagli scaffali? Se decidesse che bisogna onorare certe follie del presente e togliere Petrarca perché scriveva del suo amore per una minore, o Shakespeare perché assegnava ruoli scomodi a personaggi di pelle diversa dalla sua?

Rischi della tecnologia delle biblioteche, ragazzi. C’è stata gente che ha fatto roghi dei libri in piazza. Dei responsabili, alcuni sono banditi dal mercato e altri, invece, godono di tranquilla salute editoriale. Domani potrebbe cambiare il vento, invertirsi la tendenza, tirare aria pesante, arrivare al governo gente cattiva. Nonostante certi libri siano stati banditi; guarda il caso, bandire un libro ha effetto zero sulla diffusione delle idee che contiene. La cosa migliore da fare con un libro è pubblicarlo e, fosse osceno, aberrante, scandaloso, combatterlo con libri migliori. Chi vuole bandire un libro ha paura delle _proprie_ idee, troppo deboli. Bisogna avere paura di chi bandisce, non di chi viene bandito. Ma divago.

Che poteva fare l’individuo, ieri, quando c’erano la tecnologia delle librerie e quella delle biblioteche, per proteggersi da un nuovo Terrore e avere a disposizione tutti i libri che riteneva necessari? Soldi, a palate. Nascere di casato Leopardi e riempire di libri la propria ala del palazzotto.

Oggi, con la nuova tecnologia, come posso fare? I libri possono starmi in un Kindle, in un iPad. In due iPad. In due iPad più un disco. Due iPad più un disco e un cloud. Due iPad più un disco più più un cloud più una schedina SD da nascondere nel tacco di una scarpa (ho conosciuto un commercialista che usava veramente questo metodo, per la contabilità, diciamo, non ovvia dei clienti). Quanto sopra più un’altra schedina SD che metto a casa dei parenti. E poi cifro tutto e lo metto su [Amazon Deep Glacier](https://aws.amazon.com/about-aws/whats-new/2019/03/S3-glacier-deep-archive/), pochi euro l’anno e sta lì fino a che esiste Amazon. Quanto esisterà Amazon? Potrebbe essere più a lungo di me. Non lo fosse, potrebbe cedere Glacier a una nuova startup promettente e così via. Corro veramente il rischio di perdere i miei libri? Certo. Ma è sempre più remoto.

E se domani Apple decidesse di chiudere i suoi negozi, Amazon sparisse, YouTube chiudesse, tutto si spegnesse…?

A parte che l’universo andrà incontro alla morte termica e perfino entità senzienti di pura energia a un certo punto non avranno più niente da spendere per tenere insieme la propria struttura, tutto quello che sta nel Project Gutenberg, in [LiberLiber](https://www.liberliber.it/benvenuto/), nell’[Internet Archive](https://archive.org), potrebbe essere ancora disponibile.

([Project Gutenberg](https://www.gutenberg.org/wiki/Main_Page), mentre scrivo, è accessibile solo se accendo la Vpn e faccio finta di essere canadese o vietnamita. Si scrive per chilometri sulla malvagità delle multinazionali ma, qui e ora, a impedirmi di accedere a libri liberi da copyright [è lo stato italiano](https://www.repubblica.it/tecnologia/2020/06/09/news/oscurata_la_piu_antica_libreria_digitale_wikimedia_inaccettabile_-258783839/?refresh_ce)).

Google e altri hanno diffuso i [piani](https://linearbookscanner.org) e il [software](https://github.com/google/linear-book-scanner) open source di uno scanner per libri di carta. Costa pochissimo, certo va fabbricato, ma si può fare (e non si capisce perché non venga costruito come esercizio in tutte le scuole del regno). La musica è senza protezioni anticopia da anni, grazie alla [politica – ehm – di Apple](https://macintelligence.org/blog/2014/12/06/la-solita-musica/), quella che dovrebbe censurare i libri o altro. Mi dilungherei, solo che devo concludere.

I rischi della tecnologia. Mai come in questa epoca, un singolo ha avuto tante possibilità di detenere e conservare l’intero patrimonio espressivo dell’umanità.

Ma non c’è già l’Internet Archive? Certo – c’è anche la [Library of Congress](https://www.loc.gov) – ma se lo chiudessero…?

_Come? Rifarsi in casa l’Internet Archive costa una fortuna e richiede una vita_. Non è colpa mia. Il punto è che _lo puoi fare_, volendo e avendo quei soldi. Non avresti potuto farlo prima, mai, in alcun modo. E anche la Biblioteca di Alessandria è andata a fuoco prima che si riuscisse.

È per questo che oggi si parla di politica, non di tecnologia. Politica della _polis_. Viviamo insieme agli altri, con gli altri, sfruttiamo la tecnologia per quello che può dare e miglioriamo per quello che possiamo ciò che non può dare. L’Internet Archive nasce solo da una collaborazione. Da una partecipazione.

La cosa che non funziona è fidarsi di nessuno. È come fidarsi di tutti, finisci in un’utopia castrante e troppo semplice. Invece si fatica quotidianamente per fidarsi di qualcuno e decidere quanto, fino a che punto. Ci si prende anche qualche rischio, gli unici che pagano. Si mantengono relazioni, si allacciano reti, si condivide, si prende parte. A un certo punto ti fidi anche, il minimo se serve, di un provider o di un fornitore di cloud. Dalla rete di persone, servizi, dati si sviluppa valore.

Ha più valore una pagina trascritta per il progetto Gutenberg della mia copia di [Gödel, Escher, Bach prima edizione](https://www.abebooks.com/book-search/title/godel-escher-bach/author/hofstadter/first-edition/), che va sbriciolandosi. In Italia è in corso un [attacco senza precedenti a Tolkien](https://macintelligence.org/posts/2019-11-04-parole-in-libertà/), condotto su basi ideologiche grette e fresche come mummie. Tengo in casa la mia copia di sempre del _Signore degli Anelli_. Ma sviluppo valore se faccio conoscere la differenza; se sposto la gente dalla libreria ai circuiti dell’usato. L’ho già detto, a un certo punto qualcuno [metterà in circolo copie clandestine più o meno illegali delle vecchie traduzioni](https://macintelligence.org/posts/2020-03-04-sotto-assedio/). Questo è combattere la censura. Spolverarmi la copia sullo scaffale mi fa sentire a posto. Non serve a niente, invece.

Appunto, politica.
