---
title: "Falsi sentimentalismi"
date: 2014-12-27
comments: true
tags: [Gruber]
---
Facciamo finta per un momento che questo *post* sia propedeutico a una cosa che voglio scrivere domani o dopodomani, mentre riprende un qualche flusso di notizie. Facciamo finta che ci sia dietro una questione da riprendere nei prossimi giorni.<!--more-->

Ecco. John Gruber [scriveva tre anni fa](http://daringfireball.net/2011/12/merry) questo:

>Dieci anni da oggi quando, diciamo, aspetto mio figlio che torna a casa dal college per le vacanze invernali e, quando lo fa, vuole passare il tempo a uscire con gli amici — quanto sarei disposto a pagare per tornare indietro nel tempo, per un giorno, a *oggi*, quando ha otto anni, vuole andare al cinema, giocare, costruire con il Lego assieme a me, e crede nella magia? […] Qualsiasi cifra.

La conclusione era che Gruber si sentiva quel giorno l’uomo più fortunato sulla Terra e così sperava di tutti.

Mia figlia non ha otto anni ma diciannove settimane, eppure capisco Gruber. Ciò che voglio evidenziare in un prossimo *post* è che, *prima*, non avrei potuto capire. E in certe situazioni avrei fatto meglio a non tirare conclusioni.

A dirla tutta, inizialmente non era previsto un seguito. Si è reso necessario. Facciamo finta che sia un pensiero razionale.