---
title: "L’utopia della ricerca"
date: 2020-05-18
comments: true
tags: [emacs, Google, DuckDuckGo]
---
Un tema importante sul mio desktop digitale in quest’ultimo periodo è stato la ricerca.

Dati distribuiti su apparecchi diversi, accessibili in modi diversi, con differenti possibilità di elaborazione e analisi. A guardarlo da lontano è uno scenario da incubo. Senza contare la mole di dati che arriva dalla cerchia esterna (amici, contatti, colleghi, quindi per esempio messaggi in quindicimila formati diversi, su piattaforme molto chiuse). Per non pensare allo spazio esterno, cioè Internet e addentellati.

Viene spontaneo pensare a una qualche soluzione che possa essere univoca e risolvere tutti i problemi, probabilmente perché ci illudiamo che se cerchiamo una cosa su Google lui ci mostri veramente tutto quello che esiste o, perlomeno, la fonte di sicuro più rilevante. Non è così e di molte lunghezze.

L’unica soluzione, che non lo è, è partire da più lontano e concepire un approccio più globale. Ho letto recentemente di [costruzione di un'infrastruttura personale di ricerca per la conoscenza e il codice](https://beepb00p.xyz/pkm-search.html). Molto tecnico, con una prospettiva da programmatore, pieno di riferimenti quasi inutilizzabili senza una grossa esperienza e capacità.

Però la direzione su cui, almeno, riflettere è quella. *Stay tuned*.