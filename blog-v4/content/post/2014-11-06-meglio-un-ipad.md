---
title: "Meglio un iPad"
date: 2014-11-06
comments: true
tags: [iPad, Surface, Microsoft, Cnn, fail, epicfail, Apple, megliounipad]
---
Il flop della settimana (adesso va di moda dire #fail o #epicfail) è sicuramente quello di Microsoft con le sue tavolette Surface. Cnn ha incassato una quantità imprecisata di denaro (comunque *molto* denaro) per mostrare Surface sulle scrivanie dei giornalisti che seguivano le elezioni statunitensi di metà mandato.<!--more-->

È stata anche una bella occasione per constatare l’esistenza di una legione di fanatici legati al culto di Microsoft, qualunque cosa tiri fuori. Mica solo Apple deve passare per entità parareligiosa…

<blockquote class="twitter-tweet" lang="en"><p>Watching CNN because they are using Surfaces. FOX using Macs <a href="https://twitter.com/hashtag/Microsoft?src=hash">#Microsoft</a> <a href="https://twitter.com/hashtag/surface?src=hash">#surface</a></p>&mdash; Jonathan (@Jonathan1David) <a href="https://twitter.com/Jonathan1David/status/529818338697183232">November 5, 2014</a></blockquote> <script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>

Ma divago. Il punto è che sulle scrivanie campeggiavano i Surface; intanto, dietro, i giornalisti usavano iPad. Sono anche arrivati ad appoggiare gli iPad sui Surface usati come supporto.

<blockquote class="twitter-tweet" lang="en"><p>Watching CNN because they are using Surfaces. FOX using Macs <a href="https://twitter.com/hashtag/Microsoft?src=hash">#Microsoft</a> <a href="https://twitter.com/hashtag/surface?src=hash">#surface</a></p>&mdash; Jonathan (@Jonathan1David) <a href="https://twitter.com/Jonathan1David/status/529818338697183232">November 5, 2014</a></blockquote> <script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>

<blockquote class="twitter-tweet" lang="en"><p>Even the good people at CNN who have Microsoft surface tablets in their face hide iPad ... - <a href="http://t.co/Sz6CpzLrEg">http://t.co/Sz6CpzLrEg</a> <a href="http://t.co/7Bam8uyI2G">pic.twitter.com/7Bam8uyI2G</a></p>&mdash; Melbourneer (@_Melbourneer_) <a href="https://twitter.com/_Melbourneer_/status/529881289298415616">November 5, 2014</a></blockquote> <script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>

Va ricordato comunque anche l’*exploit* di Nexus 9. Htc lo ha lanciato al prezzo base di 399 dollari. Il giorno dopo [lo hanno scontato del 50 percento](http://arstechnica.com/gadgets/2014/11/a-day-after-launch-htc-sold-the-nexus-9-for-50-off/).

\#megliounipad