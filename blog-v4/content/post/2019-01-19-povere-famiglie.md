---
title: "Povere famiglie"
date: 2019-01-19
comments: true
tags: [Office, 365]
---
Di ritorno da una trasferta di un’oretta salvo traffico pesante, ascolto la radio più ascoltata in Italia e sento la pubblicità di una (s)vendita di Office 365 *per famiglie* al prezzo scontatissimo di *59,99 euro*.

È vero che ci sono poveri e ricchi, le disuguaglianze, le difficoltà a macchia di leopardo, la crisi per alcuni mentre altri prosperano e così via. Focolari dove l’euro e ventinove provoca esitazioni e altri che spendono più in giochi che in televisione a suon di canoni e acquisti *in-app*.

Il punto non è l’importo. È lo spreco.

Girare l’Italia in camper, fermarsi nelle piazze, erogare corsi gratuiti di *TextEdit* dal titolo *Word, ciucciami il calzino*. È il sogno a occhi aperti che faccio in queste occasioni.

E poi, ove necessario, Pages e Numbers, o [LibreOffice](https://www.libreoffice.org). Seminare cultura e intelligenza, insegnare alla gente come esistano fogli di calcolo [che accettano espressioni Python](https://manns.github.io/pyspread/), oppure progettati [per il web e la collaborazione](https://ethercalc.net), così come [LaTeX](https://www.latex-project.org) offre le funzioni di cura del proprio elaborato che Word non avrà mai neanche se lo riscrive un clone di Richard Stallman.

Combattere una povertà, quella di nozioni decenti di tecnologia digitale, che fa a volte più paura di quella economica. Perché quest’ultima è questione di risorse, ma con le risorse si può risolvere. La prima è questione di ignoranza, un difetto di cui il cittadino medio va orgoglioso senza capire il danno che si provoca da solo.

Povere famiglie, quelle che buttano soldi per comprare Office 365.