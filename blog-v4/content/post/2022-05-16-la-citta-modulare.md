---
title: "La città modulare"
date: 2022-05-16T01:30:58+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [BBEdit, Markdown, BBStylish, Css]
---
Dice, che bello l’editor di testo *xyz* che ti fa scrivere in Markdown e ti mostra in diretta la resa grafica del codice. Mica come [BBEdit](https://www.barebones.com/products/bbedit/), che ha l’anteprima di tutto, ma piuttosto rudimentale e non all’altezza di tutte le cose che si fanno oggi con [Markdown](https://www.markdownguide.org).

Vero, ma possiamo arrangiarci comunque con [BBStylish](https://nostodnayr.net/projects/bbstylish/) che mostra un’anteprima di Markdown assolutamente *glamour* e, non piacesse, totalmente personalizzabile in modo semplice. Il foglio stile contiene le istruzioni per cambiare le impostazioni e neanche serve saperne troppo di [Css](https://developer.mozilla.org/en-US/docs/Web/CSS), oltre al minimo sindacale.

Scaricare è gratis, installare è banale, il risultato è ottimo.

BBEdit non sarà magari la città ideale per tutti quanti vivono di testo. La modularità, però, è assicurata e c’è sempre un’estensione al programma che risolve un problema.