---
title: "Forza quattro"
date: 2015-04-01
comments: true
tags: [Commodore64, UltimaIV]
---
Oggi i giochi stupiscono per profondità grafica e per le mille possibilità aggiunte dall’interazione e dalla rete globale. Una volta stupivano per la profondità della trama, per la vastità dell’azione, per il fascino che evocavano seppure con interfaccia limitata (della grafica neanche parliamo).<!--more-->

Fortunatamente in quest’epoca è possibile concedersi il meglio dei due mondi e godersi l’antico e il moderno secondo preferenza. Se c’è di mezzo l’antico, pochi giochi stupiscono per profondità e vastità come [Ultima IV](http://www.gog.com/game/ultima_4).

Se poi lo si può giocare su un computer di valore, progettato per dare il meglio e non per risparmiare sui componenti, l’esperienza è ancora più gradevole. Abbiamo tutti sotto gli occhi la differenza tra computer, da scrivania o da tasca, che semplificano e potenziano la vita, e altri la cui unica specifica che conta veramente è la cifra sul cartellino.

È per questa ragione che, su questo blog, la scelta del computer su cui giocare Ultima IV può essere solo una.

[Commodore 64](http://magervalp.github.io/2015/03/30/u4-remastered.html).
