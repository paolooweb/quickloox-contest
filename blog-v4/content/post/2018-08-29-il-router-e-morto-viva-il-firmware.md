---
title: "Il router è morto, viva il firmware"
date: 2018-08-29
comments: true
tags: [AirPort, Capsule, Express]
---
La voce ufficiale, da fine aprile, è che sia [cessata la produzione di AirPort e Time Capsule](https://www.imore.com/rip-airport), con gli ultimi pezzi in vendita fino al classico esaurimento scorte.

A fine agosto, tuttavia, [arriva un aggiornamento firmware per AirPort Express](https://www.macstories.net/news/apple-updates-airport-express-firmware-with-airplay-2-support/).

Nuovo software per un apparecchio abbandonato da quattro mesi.

Ogni tanto si legge dell’obsolecenza programmata e ora posso scaricare un firmware per la mia AirPort Express, che neanche ricordo quando ho acquistato da tanto è anziana.

La situazione mi ricorda vagamente quella di oltre vent’anni fa, quando venne annunciata la fine di Newton. Immediatamente vendetti il mio PowerBook Duo per acquistare un MessagePad 2100 appena uscito di produzione. Prima che finissero.

È stato uno degli acquisti più indovinati che abbia mai fatto e ancora oggi, vent’anni dopo, posso vedere gli appunti in inchiostro digitale e le cose scritte su quella macchina incredibile.

Un router Wi-Fi è qualcosa di diverso e, se domani si rompesse la Express, guarderei sul mercato con serenità. Eppure ogni tanto la tentazione di accaparrarsi un’altra Express, da tenere nel cassetto per quando terminerà l’esistenza questa, è forte.