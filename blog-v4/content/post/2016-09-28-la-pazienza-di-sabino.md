---
title: "La pazienza di Sabino"
date: 2016-09-28
comments: true
tags: [Mac, PC]
---
Lo invidio, Sabino, perché ha ancora quella pazienza di [smontare i luoghi comuni](https://melabit.wordpress.com/2016/09/28/ma-e-vero-che-i-prodotti-apple-costano-troppo/) (più sono comuni più sono da smontare) che invece sto iniziando a perdere, vista la qualità degli interlocutori controparte ma soprattutto del clima da guerra della giungla che si respira sui *social*. Se credi che la Terra sia piatta, si possa vivere d’aria e la Luna sia fatta di formaggio, accetti tutto sul tuo account Facebook tranne che una cortese critica.

Godiamoci Sabino, finché la sua pazienza dura.