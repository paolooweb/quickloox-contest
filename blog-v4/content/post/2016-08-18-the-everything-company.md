---
title: "The Everything Company"
date: 2016-08-18
comments: true
tags: [Cue, Federighi, Mappe, Fast, Company]
---
Troppe cose interessanti nell’[intervista di Eddy Cue e Craig Federighi](http://www.fastcompany.com/3062596/tim-cooks-apple/eddie-cue-and-craig-federighi-open-up-about-learning-from-apples-failures) a *Fast Company* (agosto è perfetto per leggere qualcosa in più del normale). A partire dal fatto che Tim Cook ha cambiato certe regole imposte da Steve Jobs, ma è ancora relativamente raro sentire gli alti dirigenti Apple fuori da un *keynote*.

Tengo particolarmente a due passi del testo, avendole ripetute sovente negli anni. La prima è che Apple non è una *Everything Company* e il suo successo deriva, fino a rasentare la provocazione, più dalle cose che sceglie di non fare rispetto a quelle che fa.

La seconda: se sono gli anni novanta e produci solo Macintosh, un problema sull’uno percento della produzione interessa migliaia di persone. Se è il 2016 e hai prodotto un miliardo di iPhone per tacere del resto, un problema sull’uno percento della produzione interessa milioni di persone, cioè mille volte tanto rispetto ai vecchi tempi. Se c’è un forum con cento persone arrabbiate per qualcosa, o mille, il loro numero è irrilevante rispetto alla portata della questione, che potrebbe essere importante o secondaria a prescindere da questo.

Ciò detto, è confortante anche leggere delle lezioni imparate dal primo lancio di Mappe e della tensione continua a dare il massimo, perché il pubblico si aspetta da Apple più di quello che si aspetta dalla concorrenza.

L’impressione complessiva, che è il primo motivo di raccomandazione del pezzo, è che si legge spesso di una Apple diversa da quella di prima. Mentre la realtà dice che sì, sono cambiate cose. Ma Apple è ancora una eccezione.
