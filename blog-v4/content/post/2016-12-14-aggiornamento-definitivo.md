---
title: "L’aggiornamento definitivo"
date: 2016-12-14
comments: true
tags: [Samsung, Note, 7, Galaxy]
---
Secondo [The Verge](http://www.theverge.com/2016/12/8/13892400/samsung-galaxy-note-7-permanently-disabled-no-charging-us-update), domani Samsung pubblica un aggiornamento per Galaxy Note 7 che *impedisce la ricarica dell’apparecchio*.<!--more-->

In questo modo anche i più ostinati a volersi tenere in tasca un apparecchio a rischio di incendio dovranno prendere atto e farsi sostituire la ciofeca con qualche altra ciofeca meno pericolosa.

A meno che siano utenti americani di Verizon: l’operatore ha dichiarato che [non intende applicare l’aggiornamento](http://www.verizon.com/about/news/verizon-statement-regarding-samsung-galaxy-note7) per non *eliminare le funzioni di connettività mobile nel mezzo della stagione dei viaggi natalizi* (quando un numero indefinito di americani si mette in marcia per raggiungere famiglie spesso molto lontane).

Ogni aggiornamento di sistema dovrebbe portare il sistema a operare meglio e con più profitto di prima. Questo di Samsung è una conquista difficile da eguagliare, molto affine allo spirito delle discipline filosofiche orientali: l’aggiornamento definitivo è quello che ti fa andare oltre il telefono e abbandonare le cose materiali.
