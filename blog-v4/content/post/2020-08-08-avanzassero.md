---
title: "Avanzassero"
date: 2020-08-08
comments: true
tags: [T-Mobile, Apple, iPad]
---
In California ci sono 6,2 milioni di studenti. Molti di essi, se non tutti, inizieranno l’anno scolastico a distanza e sappiamo quanto questo sia problematico per le famiglie che hanno meno disponibilità, a partire dagli apparecchi.

Lo Stato californiano si è accordato con il provider T-Mobile e con Apple per [fornire iPad e connessione cellulare a prezzi agevolati a un milione di studenti da qui a fine 2020](https://www.cde.ca.gov/nr/ne/yr20/yr20rel65.asp), con centomila macchine già disponibili per l’inizio delle lezioni.

I gloriosi Squallor avevano pubblicato una feroce [parodia delle canzoni per beneficenza](https://youtu.be/VmjYR9EMZW8) che esortava Michael Jackson a intraprendere un’altra iniziativa stile _Usa for Africa_, ma stavolta _for italì_.

L’anima degli Squallor non c’è più però lo spirito aleggia tra noi in modo molto attuale: avanzassero un po’ di iPad, ci candidiamo volentieri, nel rispetto della distanza tra le _rime buccali_.

_E un disco faccelo / anche per noi…_