---
title: "Non ce n’è per tutti"
date: 2017-02-10
comments: true
tags: [Samsung, BlackBerry, iPhone, Sony]
---
Ci eravamo soffermati sul trimestre estivo dei computer da tasca, quando Apple aveva conseguito da sola [più del cento percento dei profitti disponibili](https://macintelligence.org/posts/2016-11-15-fatto-cento-facciamo-centoquattro/). Tutti gli altri avevano lavorato in perdita.

Il trimestre di Natale è andato assai “peggio” per Apple: “solo” il [92 percento dei profitti](http://www.investors.com/news/technology/click/apple-took-92-of-smartphone-industry-profits-in-q4/) con il 18 percento delle vendite.

Altre aziende che hanno fatto soldi a Natale vendendo telefonini intelligenti: Samsung (nove percento), Sony (uno percento), BlackBerry (uno percento). Il totale è superiore a cento perché gli altri, complessivamente, hanno perso denaro con apparecchi che non recuperano quello che sono costati. Ci si era soffermati su LG ([duecento milioni di dollari](https://macintelligence.org/posts/2017-01-30-perdite-e-profitti/) in fumo).

È un mercato piuttosto consolidato, dove possono esserci salite e discese, ma per le rivoluzioni bisogna aspettare qualche innovazione vera. E non c’è posto per tutti.

A partire dalle batterie difettose. In un magazzino cinese collegato a Samsung [hanno preso fuoco](http://www.reuters.com/article/samsung-sdi-batteries-fire-idUSL4N1FT3BX). Chi troppo vuole.