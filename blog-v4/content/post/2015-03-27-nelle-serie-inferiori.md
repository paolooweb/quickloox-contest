---
title: "Nelle serie inferiori"
date: 2015-03-27
comments: true
tags: [Chemnitz, Paderborn, basket, ArsTechnica, Seidel, Apple, Windows]
---
Storia faziosa ma deliziosa, [pubblicata da Ars Technica](http://arstechnica.com/business/2015/03/german-pro-basketball-team-relegated-to-lower-division-due-to-windows-update/), quella del *match* cestistico tra Chemnitz e Paderborn.<!--more-->

Alle 18, un’ora e mezzo prima della partita, i dirigenti del Paderborn accendono il portatile che governa il tabellone elettronico. Durante il riscaldamento il computer si blocca.

Viene riavviato alle 19:20, dieci minuti prima dell’inizio ufficiale, e inizia automaticamente a scaricare aggiornamenti nonostante i dirigenti non abbiano dato alcuna istruzione in merito.

Le procedure finiscono alle 19:55, venticinque minuti dopo l’orario previsto e dieci minuti oltre il ritardo massimo previsto dalla federbasket tedesca.

Così Chemnitz perde ma fa ricorso, e vince a tavolino. Paderborn viene penalizzata di un punto in classifica, il che la condanna matematicamente alla retrocessione.

Patrick Seidel, direttore generale del Paderborn, dichiara di comprendere il comportamento di Chemnitz e sottolinea che loro hanno battuto quella squadra sul campo due volte, a parte questo episodio che niente ha a che fare con lo sport.

>Ha anche dichiarato che il portatile Windows in dotazione alla squadra sarà rimpiazzato da un portatile Apple.

Puntare in alto è anche questione del giusto equipaggiamento.