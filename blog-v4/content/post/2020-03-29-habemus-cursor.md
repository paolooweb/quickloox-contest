---
title: "Habemus cursor"
date: 2020-03-29
comments: true
tags: [Magic, Keyboard, iPad, MacStories, Daring, Fireball, Gruber, Viticci]
---
Dichiaro ufficialmente che in questa casa entra una Magic Keyboard per iPad solo se qualcuno me la regala.

D’altra parte ammetto di ricevere *feedback* positivi dagli amici (**Stefano** per primo) e di leggere praticamente solo commenti favorevoli, a partire da quello più autorevole, [di Federico Viticci](https://twitter.com/viticci/status/1240404865916055552):

<blockquote class="twitter-tweet"><p lang="en" dir="ltr">Finally tested a Magic Trackpad 2 with iPadOS 13.4. Some gestures require fine-tuning (Slide Over), and third-party apps will need updates, but:<br><br>This feels instantly natural. Small learning curve. Like the cursor has been part of UIKit for years. <br><br>Well done, Apple.</p>&mdash; Federico Viticci (@viticci) <a href="https://twitter.com/viticci/status/1240404865916055552?ref_src=twsrc%5Etfw">March 18, 2020</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

Vanno lette le nuove [Human Interface Guidelines](https://developer.apple.com/design/human-interface-guidelines/ios/user-interaction/pointers/) di Apple in merito per vedere che il lavoro è stato fatto bene anche a livello di documentazione.

Insomma, sono tutti contenti. Speriamo sia una evoluzione che lascia intatta la possibilità di lavorare (molto) bene, come ora, con iPad in mano e senza altri sistemi di input che il tocco.