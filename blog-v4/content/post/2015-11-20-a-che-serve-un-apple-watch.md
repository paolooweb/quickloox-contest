---
title: "A che cosa serve"
date: 2015-11-20
comments: true
tags: [watch, Fossil, Tag, Heuer, Svizzera]
---
La Svizzera è un Paese diverso da noi perché è sufficiente leggere un comunicato stampa scritto in modo comprensibile per sapere [come va la bilancia dei pagamenti](http://www.news.admin.ch/NSBSubscriber/message/attachments/41806.pdf).<!--more-->

Le esportazioni nel settore dell’orologeria sono definite *in caduta libera*.

I dati specifici vanno cercati in una grande tabella generica, mentre *9to5Mac* ha prodotto [un diagramma](http://9to5mac.com/2015/11/19/swiss-watch-exports-slump/) più ristretto e molto chiaro.

Ci saranno montagne di ragioni, eh. Però noto che negli ultimi mesi c’è stato l’ingresso nel mercato di un nuovo soggetto. Che tempo fa ha fatto la stessa cosa con i telefoni. E gli scettici sghignazzavano o profetizzavano sventure.

Strano che non senta più la domanda del saggio *a che serve un watch?*.

Forse perché c’è una risposta: *non lo sappiamo ma sembra che serva più di un orologio Fossil o Tag Heuer*.