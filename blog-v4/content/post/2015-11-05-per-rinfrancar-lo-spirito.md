---
title: "Per rinfrancar lo spirito"
date: 2015-11-05
comments: true
tags: [Sri, Screen, OSX, SGI, Unix, Federico]
---
Più che tra un’[Enigma](http://startpad.googlecode.com/hg/labs/js/enigma/enigma-sim.html) e l’altra stile Settimana Enigmistica, all’indomani del torrenziale [post su Wikipedia](https://macintelligence.org/posts/2015-11-04-very-good-very-bad/). Mi ci vuole tempo per digerirlo, intanto perché ne stiamo ancora parlando, secondo perché sono cose cui tengo molto e finisce che ne esco provato.<!--more-->

Quindi oggi relax e disincanto, piccolezze simpatiche di cui quasi nessuno parla. Per esempio (grazie **Federico**!) la [conversione per OS X dei font monospaziati](http://njr.sabi.net/2015/11/01/sgi-screen-fonts-converted-for-os-x/) creati per le stazioni di lavoro Silicon Graphics.

Nel secolo scorso Silicon Graphics era un’entità semidivina per tanti, che dovevano accontentarsi di semplici personal computer. Quelle erano supermacchine per gli scienziati, per scoprire cose impossibili, per disegnare frattali a velocità impensabili nelle case e negli uffici.

Oggi quanto meno ne restano i font da Terminale, per giunta in edizione bitmap. Oggi la Silicon Graphics ce la portiamo in tasca, quando non al polso. E resta un doveroso omaggio ai tempi in cui anche la leggibilità di un monospaziato era una frontiera da raggiungere, se necessario ingegnandosi come ha fatto l’autore della pagina per portare il font Screen su OS X.
