---
title: "Tremila illusioni"
date: 2017-03-12
comments: true
tags: [Lofree, Blue, MacBook, Jobs, design, Six, Colors]
---
Ha suscitato una certa attenzione il lancio della tastiera [Lofree](https://www.indiegogo.com/projects/lofree-typewriter-inspired-mechanical-keyboard-design-technology#/) per Mac: meccanica ma Bluetooth, piccola, simpatica per i suoi tasti dalla forma circolare a richiamare le macchine per scrivere di un tempo, prova secondo l'amico [Blue](http://bluebottazzibeat.blogspot.it) che [Apple non riesce più a fare design innovativo](https://www.facebook.com/groups/applerebels/permalink/774432676039055/).<!--more-->

Premesso che – come ho fatto notare su Facebook – tra iterazioni di prodotto ([Magic Keyboard](http://www.apple.com/it/shop/product/MLA22T/A/magic-keyboard-italiano)), nuovi prodotti ([Smart Keyboard](http://www.apple.com/it/shop/product/MJYR2ZM/A/smart-keyboard-per-ipad-pro-129-inglese-usa?fnode=37) per iPad Pro) e riprogettazione di tecnologie (i tasti a corsa ridotta di MacBook) e innovazione effettiva ([Touch Bar su MacBook Pro](http://www.apple.com/it/macbook-pro/)) Apple ha recentemente lavorato non meno di tre volte sull’argomento tastiera, può anche darsi che sia rimasta indietro sul *design*.

Che però ne sia prova la Lofree, è discutibile. La tastiera ha avuto un successo clamoroso: manca ancora un mese alla chiusura del finanziamento e i proponenti hanno già ricevuto fondi per il tremila percento della cifra preventivata per la realizzazione. Il tremila percento.

I primi a ricevere una Lofree saranno appunto quelli che hanno messo mano al portafogli per finanziare il progetto: a oggi, qualcosa più di tremila.

Tremila.

Se sei una Apple, nel bene e nel male devi preoccuparti che una tastiera soddisfi, che so, trenta milioni. Sono diecimila volte di più. Una tastiera originale nel proporsi, con qualche bella idea e una estetica ammiccante, tremila fan in tutto il mondo li trova.

Ma per i trenta milioni? L’estetica funziona fino a che *design* significa *l’aspetto che ha*. Steve Jobs diceva che la vera differenza la fa *how it works*, come funziona. Cito la [recensione di Six Colors](https://sixcolors.com):

>Ci sono solo due tasti a doppia larghezza su questa tastiera che è perfino più piccola del mio modello attuale, dimensionata all’80 percento dello standard: Maiuscole sinistro e Invio. Suppongo che con abbastanza tempo a disposizione potrei abituarmi a usare tabulatore e Backspace di dimensioni minime, ma non dovrei. Questi (e altri) tasti sono più grandi del normale sia perché sono importanti, sia perché aiutano chi digita a ritrovare la corretta posizione sulla tastiera.

>Anche sul posizionamento dei tasti cursore c’è qualcosa da dire. La freccia in alto non può essere posizionata direttamente sopra la freccia in basso, ma è leggermente spostata a destra, proprio di fianco al piccolo tasto Maiuscole. Non voglio parlare di quante volte ho spostato il cursore invece che impostare una maiuscola.

Sono diffidente per natura di qualsiasi nuova tastiera, perché scrivo tutto il giorno. Il posizionamento fisico dei tasti e la dimensione della tastiera, se sono diverse dal solito, modificano la mia precisione e la mia resa. Per quanto mi riguarda la Lofree è carinissima… e mai la userei, perché a) è più piccola del normale; b) i tasti importanti sono confsusi con quelli ordinari; c) la loro disposizione è in taluni casi opinabile.

Tralascio il fatto che, da come pare di capire, manchi il layout italiano, quindi le accentate.

Certo, è *design* accattivante, capace di soddisfare (preventivamente) tremila persone e raccogliere il tremila percento di diecimila dollari. Che sia *design* innovativo e di avanguardia, non mi illudo. Questione di *how it works*.