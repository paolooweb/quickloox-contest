---
title: "Auguri di realtà"
date: 2014-12-23
comments: true
tags: [Amazon, Apple, ebook, iTunes, iBooks]
---
Velocemente: il mondo degli *ebook* lo domina Amazon. Amazon aveva imposto un prezzo massimo di 9,99 dollari a qualsiasi *ebook*, checché ne pensassero l’autore e l’editore. Amazon controllava, tipo, il 90 percento delle vendite di *ebook*.<!--more-->

Apple ha aperto iBooks Store con lo stesso criterio di App Store e iTunes Store: gli editori fanno il prezzo che pare a loro e a iBooks Store resta in mano il trenta percento.

Amazon ha attirato l’attenzione dei giudici sulla cosa ed è riuscita a fare mettere sotto accusa gli editori e Apple, perché le loro azioni potevano contribuire ad alzare il prezzo degli *ebook* e quindi danneggiare il pubblico.

Gli editori hanno tutti patteggiato. Apple è andata in tribunale e ha perso.

Ora finalmente pare che questa commedia surreale possa terminare. In appello [i giudici paiono comprendere](http://fortune.com/2014/12/15/mondays-e-book-antitrust-appeal-hearing-went-well-for-apple/) che schiacciare i prezzi è da parte di un monopolista un sistema per mantenere il monopolio e, dall’altra parte, che se i prezzi degli *ebook* si sono alzati non è per una congiura contro le persone; bensì, semplicemente, quelli sono i prezzi che gli editori ritenevano di perseguire, ferma restando la libertà di non comprare da parte di chiunque.

Che bello se il 2015 fosse l’anno in cui la realtà ritorna a prevalere sull’assurdo, in questo come in molti altri campi.