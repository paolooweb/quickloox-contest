---
title: "L’offerta di un servizio"
date: 2020-09-19
comments: true
tags: [Xbox, Gruber, watch]
---
Una settimana dopo la [recensione-farsa di una console per videogiochi spenta](https://macintelligence.org/blog/2020/09/12/recensire-al-futuro/), John Gruber racconta della sua [prova di watch serie 6](https://daringfireball.net/2020/09/graphite_is_the_new_black).

Chi ha bisogno di ridurre le cose alla propria bassa statura mentale per stare bene dirà *alla fine è solo un orologio*. Gruber pone attenzione maniacale sui *cinturini* di watch, come se fossero una rivoluzione invece che, appunto, cinturini.

La sfumatura di colore, la consistenza, il comportamento da bagnato, perfino il mistero di come faccia Apple a mandargli cinturini della misura giusta senza averglielo mai chiesto; da un momento all’altro ci si aspetta che, munito di laser, ce ne disintegri uno atomo per atomo per mostrare che succede.

Se avesse recensito i cinturini senza indossarli, per descriverli visti da sopra e da sotto e di profilo, avrebbe fatto giustamente la figura del cretino. Come il giornalista di *The Verge* incaricato di fare da zerbino a un prodotto che si compra per essere acceso, e non può esserlo in recensione.

Invece – così funziona il contenuto su Internet – chi non è interessato passa via comunque; chi è interessato nello specifico, trova la trattazione più approfondita possibile. Di un oggetto vero, usato veramente, funzionale (verrebbe da dire funzionante, però per un cinturino sembra esagerato).

La differenza tra offrire un servizio e prendere per il naso.