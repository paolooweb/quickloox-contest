---
title: "Lavorare al Comune"
date: 2020-06-08
comments: true
tags: [Amburgo, Monaco, Linux, open, source]
---
Leggo che la città-stato di Amburgo in Germania [potrebbe passare all'uso di Linux e software open source](https://www.zdnet.com/article/microsoft-dropped-for-open-source-why-hamburg-is-now-following-munichs-lead/) per la sua amministrazione, che coinvolge migliaia e migliaia di postazioni di lavoro. La stessa scelta potrebbe ricapitare a Monaco di Baviera, che all'incirca cambia idea secondo chi diventa sindaco.

So che oggi è in corso uno [sciopero](http://www.flcgil.it/scuola/sciopero-8-giugno-2020-come-aderire.flc) del mondo della scuola.

Chiederei a chi sciopera che software fa usare ai suoi studenti per compiti ed elaborati vari.