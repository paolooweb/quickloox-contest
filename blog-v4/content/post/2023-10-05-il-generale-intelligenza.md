---
title: "Il generale Intelligenza"
date: 2023-10-05T14:46:01+01:00
draft: false
toc: false
comments: true
categories: [Internet]
tags: [SoftBank, AI, intelligenza artificiale, Reuters, Son, Masayoshi Son]
---
L’amministrazione delegato di SoftBank ci fa sapere che avremo [l’intelligenza artificiale generale entro dieci anni](https://www.reuters.com/technology/softbank-ceo-masayoshi-son-says-artificial-general-intelligence-will-come-within-2023-10-04/).

Per *intelligenza artificiale generale* intende *intelligenza artificiale che supera quella umana in quasi ogni settore*.

>È sbagliato dire che l’intelligenza artificiale non possa superare gli umani in quanto creata dagli umani. Oggi l’intelligenza artificiale impara da sola, si addestra da sola e effettua inferenze da sola, proprio come gli umani.

Non solo: tra venti anni avremo la *Artificial super intelligence*, che supererà di diecimila volte le capacità umane.

Bisogna certamente dare atto che la dichiarazione è avvenuta durante un evento pubblico, in cui chiaramente più un botto fa rumore più suscita attenzione, per cui si sceglie il botto più grosso possibile.

Anche SoftBank è una azienda peculiare. Ho avuto occasione di incontrare i vertici dell’azienda molti anni fa a Taiwan. Dichiaravano con orgoglio di avere un piano di business a cento anni (ci sono aziende italiane prive di un piano di business) e di avere il conto perdite e profitti aggiornato quotidianamente (ci sono aziende italiane dove l’unico a sapere come va davvero fuori dall’amministratore delegato è il commercialista, che non lo dice a nessuno). È usuale che da SoftBank arrivino previsioni forti e opinioni scandalose, nel senso di fuori dal *mainstream*.

Invece che trattare Masayoshi Son come un fesso, dunque, è meglio estrapolare qualcosa di costruttivo dalla sua dichiarazione.

Sul punto, l’intelligenza artificiale supera quella umana in aree selezionate da *sempre*. Un apparecchietto con le capacità di una calcolatrice o di un orologio sa tenere il tempo o operare sui numeri come a qualunque umano è impossibile. Un forno a microonde sa graduare la potenza erogata da un maser per un tempo dato e non conosco molti umani capaci di farlo. Nonostante ciò, l’intelligenza artificiale generale di cui è in possesso un microonde sta sotto quella di un colibatterio e nessuno ci fa caso.

L’intelligenza artificiale generale è semplicemente una invenzione linguistica in più  per ammettere, senza dirlo in chiaro, che quella attuale non è intelligenza artificiale, ma un sistema veramente ingegnoso per approssimare linguaggio umano a tema a partire da un enorme database di linguaggio pesato e classificato allo spasimo.

Non ho dubbi che ci arriveremo, ma sulle scale temporali. Per dare vita, in senso lato, a una intelligenza artificiale generale, è certamente determinante risolvere il problema della percezione. La nostra intelligenza è inestricabilmente legata alla nostra percezione e le sedicenti intelligenze artificiali attuali hanno capacità di percezione zero. No, accettare in input una immagine non è un equivalente della percezione. Su questo passaggio si sono consumate menti tra le più brillanti della nostra specie e bisogna ancora mettersi d’accordo su qualunque risultato.

Poi c’è la questione delle analogie. La capacità di vedere e inventare analogie è legata fortemente all’intelligenza. I sistemi attuali possono produrre un’analogia solo se qualche umano l’ha già creata e si capisce come sia troppo poco.

Sono sicuro che una intelligenza artificiale diecimila, ma anche dieci volte superiore a quella umana non avrebbe difficoltà a risolvere problemi che per una calcolatrice – o Mathematica – sono bazzecole o a governare con efficacia le ambiguità del linguaggio che portano all’umorismo, per esempio.

Non ci siamo ancora. Solo che, diversamente dal solito, non basta avere più tempo. Le intelligenze generative attuali sono già arrivate al loro capolinea: per come funzionano, quello che potevano dare lo danno già. Fuori da limature, perfezionamenti, migliorie, rammendi, non può arrivare molto più di quello che arriva già. Che, per inciso, ha un costo computazionale non banale. Niente di quello che hanno in mano i ricercatori somiglia a un embrione di intelligenza artificiale generale.

Occorrono scoperte, invenzioni, rovesciamenti di fronte, investimenti mostruosi e menti geniali. O SoftBank avrà da aspettare il suo generale Intelligenza davvero un bel po’, se crede che basti lasciare campo libero ai nipotini di ChatGPT.