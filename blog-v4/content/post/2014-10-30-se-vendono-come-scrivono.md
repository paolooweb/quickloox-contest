---
title: "Se vendono come scrivono"
date: 2014-10-30
comments: true
tags: [Samsung, iPod, iPhone, Walkman, Sony]
---
Samsung scrive una pagina sull’[evoluzione dei sistemi audio portatili](http://global.samsungtomorrow.com/?p=43916).<!--more-->

Non si parla di iPod (e neanche di Walkman Sony).

Si parla di *smartphone* e non si nomina iPhone.

Chissà che cosa tralascia di dire alla gente che compra i suoi prodotti.