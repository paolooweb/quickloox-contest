---
title: "Evviva il packaging"
date: 2014-01-21
comments: true
tags: [iMac]
---
Così scrive graditissimamente da parte mia **Stefano**.<!--more-->

>Dopo tre aeroporti e relativi nastri trasportatori ho finalmente aperto il mio iMac nella mia casa indiana: perfettamente integro. Ecco un buon motivo per scegliere chi pensa al tuo computer fin dalla scatola. 

Aggiungo solo la foto.

 ![iMac in India](/images/imac-stefano.jpg  "iMac in India") 