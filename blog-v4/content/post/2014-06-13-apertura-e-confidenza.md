---
title: "Apertura e confidenza"
date: 2014-06-13
comments: true
tags: [Jobs, Drance, Wwdc]
---
Terminerei la copertura del [Wwdc](https://developer.apple.com/wwdc/) di quest’anno con l’[intervento di Matt Drance su Apple Outsider](http://www.appleoutsider.com/2014/06/10/wwdc2014/), che considera il discorso di apertura come *un grande lunedì tra quelli che ricordo, per gli sviluppatori. E ne posso ricordare un bel po’.*<!--more-->

E ancora:

>Annunci come questi non arrivano spesso, perché le decisioni e gli sforzi che stanno loro dietro non vengono applicati con leggerezza. Il massiccio cambiamento tecnico e politico richiesto e successivamente generato da cose come estensibilità, tastiere indipendenti e un nuovo linguaggio di programmazione portano rischi analogamente massicci, dentro e fuori Apple. Questi rischi – per la sicurezza, per l’autonomia, per una esperienza di uso coerente e degna di fiducia – erano costantemente soppesati quando propugnavo miglioramenti a un Software Developer Kit come Technology Evangelist. E più spesso che no si decideva che il rischio era troppo elevato, o che non c’era tempo per risolvere il problema e intanto contenere a sufficienza il rischio.

Si parla della maggiore confidenza e apertura di Apple, di [Craig Federighi](https://www.apple.com/pr/bios/craig-federighi.html) che si faceva [fotografare assieme a tutti](https://twitter.com/gdonelli/statuses/474599859576897536).

<blockquote class="twitter-tweet" lang="en"><p>Line to take a photo with <a href="https://twitter.com/craig_apple">@craig_apple</a>, I like this new Apple! <a href="http://t.co/OHdEVjj6aC">pic.twitter.com/OHdEVjj6aC</a></p>&mdash; Giovanni Donelli (@gdonelli) <a href="https://twitter.com/gdonelli/statuses/474599859576897536">June 5, 2014</a></blockquote>
<script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>

Si parla di maggiore disponibilità da parte delle relazioni pubbliche Apple e maggiore flessibilità verso gli sviluppatori, nonostante la segretezza su tutto sia stata mantenuta fino all’ultimo. 

Si parla di come Apple abbia completato un cammino che parte dalla scomparsa di Steve Jobs e forse inizia di nuovo oggi, nella rotta tracciata da Jobs ma senza limitarsi a seguirne pedissequamente le orme. Apple non ha senso se non sta un passo avanti a tutti e dunque quel percorso non va calpestato all’infinito, ma prolungato in direzioni nuove.

Tanto dovevo a **supergiuovane**.