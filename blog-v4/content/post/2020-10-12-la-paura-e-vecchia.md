---
title: "La paura è vecchia"
date: 2020-10-12
comments: true
tags: [Ambient, Findability, Morville, O’Reilly, Jung]
---
Ah, la sincronicità. Un attimo dopo avere scritto delle [paure dei genitori che devono comprare tecnologia per i figli](https://macintelligence.org/blog/2020/10/11/la-paura-e-lenta/) mi ritrovo con le mani impegnate da un libro dove ci sono un sacco di risposte in tema. Il bello è che il libro non parla affatto di quello; parla di [Ambient Findability](https://www.oreilly.com/library/view/ambient-findability/0596007655/).

Non intendo spiegare di che cosa tratti il libro, solo citare passaggi che riguardano la questione di cui sopra. Il libro, si sappia, risponde veramente a un sacco di domande già nelle prime poche pagine.

Uno dei primi temi considerati è l’alfabetizzazione informativa. Esatto, non informatica; informativa.

>Il ragazzo americano medio guarda quattro ore di televisione tutti i giorni […]

>Ogni volta che sento parlare del dominio della televisione e del declino dell’alfabetizzazione, sento scarsa connessione. Posso preoccuparmi della salute di questa generazione satura di media, ma non della loro capacità di leggere e scrivere. La nostra cultura non ricompensa l’analfabetismo. Al contrario, è quasi impossibile funzionare nella società moderna senza padroneggiare la comunicazione scritta. Chi non sa compilare un modulo, è nei guai. Il tasso di alfabetizzazione negli Stati Uniti è del novantasette percento. In gran parte dell’Europa è del novantanove percento.

Bravo genio, sento dire. E allora l’analfabetismo funzionale che piaga la nostra società? Ecco, il testo non se ne occupa, dicevo. In compenso parla dell’alfabetizzazione informativa. E chiude il paragrafo precedente così:

>Tuttavia non è abbastanza.

Per spiegare il motivo.

>I nostri ragazzi si avviano a ereditare un paesaggio mediale che toglie il fiato e la calma. Libri, riviste, quotidiani, poster, telefoni, televisori, videoregistratori, videogiochi, posta elettronica, Sms, messaggistica istantanea, siti, blog, wiki e l’elenco prosegue. È emozionante avere a disposizione tutte queste fonti di informazioni e gli strumenti per comunicare…

Ma…

>La complessità dell’ambiente chiede nuove forme di alfabetizzazione.

Vorrei averlo scritto in corpo duecento.

>Sono finiti i giorni in cui potevamo cercare la “risposta giusta” nell’enciclopedia di famiglia. Oggi possiamo cercare in molti posti. C’è tantissimo da trovare, solo che prima dobbiamo sapere che cosa cercare e di chi fidarci.

Altra frase da corpo duecento:

>Nell’èra dell’informazione, l’alfabetizzazione informativa transmediale è una capacità di importanza vitale.

A questo punto arriva la definizione di *alfabetizzazione informativa* secondo la American Library Association:

>Un insieme di abilità che consente agli individui di riconoscere quando è necessaria informazione e saperla trovare, valutare e usare con efficacia.

Rispetto all’analfabetismo funzionale siamo lì, alla fine. Chiamiamo la stessa cosa o quasi la stessa cosa in modi diversi. Il problema è lo stesso.

Ora uno potrebbe chiedersi che cosa ha a che vedere l’alfabetizzazione informativa con il problema di dare un cellulare in mano ai figli. I problemi in questo senso, lo abbiamo visto sopra, sono notevoli.

In effetti è vero. Non c’è alcun nesso. *Ambient Findability è del 2005*.

Non c’era iPhone, al massimo qualche esperimento pionieristico tipo Palm, che in ogni caso stava nelle mani di un campione di popolazione ridottissimo in percentuale. Non c’era Android. I cellulari non andavano in mano ai bambini e facevano generalmente schifo.

Eppure già ci si preoccupava dell’esposizione mediale dei più giovani. L’avvento dei cellulari non ha aggiunto alcunché. Chi si preoccupa dei cellulari si preoccupa di qualcosa che non ha cambiato alcunché; ha solo, probabilmente, rubato del tempo alla televisione, lato timori, patologie, allarmi, moniti. È una paura vecchia, c’è sempre stata, ci sarà sempre, sappiamo che è sterile.

Si dirà che quanto sopra sarà stato scritto da un *nerd*, qualcuno addentro nella tecnologia, lontano dai problemi veri delle famiglie.

[Peter Morville],(https://semanticstudios.com/about/) uno dei padri dell’architettura dell’informazione, autore di *Ambient Findability*, ha due figlie. È laureato, in lettere. Ha cominciato a lavorare come bibliotecario.

Ecco dove sta il nesso. I nostri figli dovranno crescere alfabetizzati informativamente e non solo alla vecchia maniera. Ognuno scelga come fare, quando farlo e perché. Teniamo presente le parole del bibliotecario:

>I lavoratori della conoscenza sono pagati per la loro abilità di trovare, filtrare, analizzare, creare e in generale maneggiare l’informazione. Quelli privi di queste capacità si perdono sul lato sbagliato del digital divide.

Quando bisognerebbe coltivare queste capacità? Con che strumenti? Ognuno dica la sua. Io lancerei l’ennesimo allarme: gli analfabeti informativi avranno un futuro più complicato degli altri. E penso che imparare a trattare l’informazione con tutti gli strumenti a disposizione sia di importanza cruciale. Preoccuparsi dei figli è anche questo; invece che nutrire il terrore di che cosa faranno con WhatsApp, si insegni loro a usarlo con cura, controllo, consapevolezza. Perché da grandi non finiscano sul lato sbagliato a causa dell’analfabetismo informativo, ecco il problema vero, dei genitori.