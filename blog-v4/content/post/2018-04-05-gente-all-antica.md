---
title: "Gente all’antica"
date: 2018-04-05
comments: true
tags: [Recode, Jobs, privacy, Facebook]
---
Raramente si legge di informatica senza essere… informati che c’è il cambiamento continuo, costante, travolgente, niente è come era un minuto fa.<!--more-->

C’è un fondo di verità e contemporaneamente certe cose invece rimangono sempre le stesse, qualunque periodo sia trascorso.

A *Recode* per esempio si sono ricordati di un’intervista a Steve Jobs del 2010, che gli è venuta buonissima da [ripubblicare](https://www.recode.net/2018/4/2/17189192/mark-zuckerberg-facebook-steve-jobs-apple-tim-cook) visti i tempi.

Oggi sembra che una parte dell’attualità sia costituita da Facebook, dalla (mancanza di) privacy in rete, dal fatto che siamo seguiti da mille aziende e organizzazioni – Facebook è solo la più grossa e oggi quella nel mirino – mentre navighiamo. Invece la faccenda è talmente vecchia che [il contesto era Ping](http://allthingsd.com/20100902/steve-jobs-on-why-facebook-is-not-part-of-apples-new-ping-music-social-network-onerous-terms/), la rete sociale lanciata da Apple, fatta malissimo e cancellata senza che nessuno se ne sia accorto o quasi.

Steve Jobs nel 2010 diceva:

>Privacy è quando che la gente sa a che cosa sta dando il consenso. In linguaggio chiaro e ripetutamente.

Jobs dice *Qualcuno è disposto a condividere più dati di altri. Fagli dire che sono stanchi di sentirsi chiedere il consenso*.

Nel resto dello spezzone Jobs cita questo atteggiamento come *vecchio stile* rispetto a molti altri colleghi della Silicon Valley.

Sembra novità parlare di privacy, mentre otto anni fa [si diceva con chiarezza come stanno le cose](https://www.youtube.com/watch?time_continue=198&v=39iKLwlUqBo). E dove conviene stare se si tenga alla confidenzialità del proprio stare in rete. Con quelli all’antica.

<iframe width="560" height="315" src="https://www.youtube.com/embed/39iKLwlUqBo" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>