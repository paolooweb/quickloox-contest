---
title: "Shopping selvaggio"
date: 2021-01-17T01:22:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [Toshiba, Toca Boca, Mac mini, iPad] 
---
Oggi ben due acquisti: un hard disk Toshiba portatile da due terabyte da usare come Time Machine per Mac mini e un bundle di giochi [Toca Boca](https://tocaboca.com) per il sollazzo della primogenita su iPad.

Da molto tempo non acquistavo dischi rigidi, grazie a babbo Natale; questo è il primo sopra il terabyte, costato una sessantina di euro su Amazon, scelto senza particolari criteri tra una gamma assai numerosa.

Collegato, formattato Mac, in opera. Niente da segnalare, cosa che per un hard disk destinato al backup è un grande complimento. Nota di merito, l’assenza di inutilware dentro il disco: mai sopportate le utility incorporate, che servono a niente e chiedono solo di essere cancellate alla svelta. Un hard disk degno di questo nome si può amministrare con le utility di sistema, senza bisogno di altro.

Il bundle Toca Boca si compone di otto app a diciotto euro; due le avevo già ma il prezzo valeva comunque la pena. Spendere qualche soldo in buone app per una seienne è rinfrancante; sono sempre alla ricerca di buone app giocose gratuite e disposto ad accettare una presenza pubblicitaria decorosa oppure la presenza di acquisti in-app (la seienne in questione, a prescindere dalla protezione offerta da Face ID contro gli acquisti indebiti, sa di non dover spendere *soldi veri* e lo mette perfettamente in pratica) e ultimamente rimango spesso deluso.

È diventato esageratamente facile trovare su App Store app-clone fatte in serie, ripetitive in modo disturbante nel *gameplay*, che si pubblicizzano a vicenda in modo ossessivo, con *trailer* continui che prendono più tempo del gioco vero e proprio. È una deriva simile a quella dei video su YouTube dove si aprono uova con sorpresa o si colorano disegni e spero che sfiorisca prima possibile. Scegliere software gratuito comporta ovviamente un tasso molto elevato di cancellazione di app non adatte o non all’altezza delle aspettative, ma queste non sono più app fatte in economia, bensì puri vettori di pubblicità fini a se stessi, senza la minima attenzione verso il pubblico.

Stiamo parlando del miglior store di app al mondo e mi vengono i brividi pensando a che cosa possa essere in corso su Play Store, se App Store indulge in questo livello infimo di qualità, che non è così da sempre e però non è mai stato così diffuso, a giudicare dalla facilità con cui capita di imbattersi in questi prodotti.