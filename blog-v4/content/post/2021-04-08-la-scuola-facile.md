---
title: "La scuola facile"
date: 2021-04-08T00:39:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [scuola, DaD, Meet, La classe capovolta, Insegnanti 2.0] 
---
Accennavo [ieri](https://macintelligence.org/posts/Renitente-allapprendimento.html) a come le maestre di mia figlia abbiano affrontato più che validamente la faccenda delle lezioni remote.

*Disclaimer:* quell’esperienza omogenea, uguale ovunque e comunque di cui scrivono i media classici, non esiste. Ogni classe, ogni scuola, ogni indirizzo ha le proprie specificità e indicare una ricetta come se andasse bene o male per tutti è sbagliato.

Ciò detto, si può comunque trarre ispirazione generale da alcuni aspetti. Si tenga presente che vale per un primo anno della primaria, con tutti i pro e contro applicabili se si pensa ad altre annate.

Le nostre maestre hanno fatto poche cose, semplici, valide e a costo zero o quasi zero rispetto alla lezione in aula:

- lezioni da quarantacinque minuti;
- gruppi da sette bambini;
- dirette con esercitazioni pratiche e attività materiali;
- pochissima lezione frontale e tanta interazione;
- riduzione delle ore.

Da un orario in aula di otto ore giornaliere, l’orario online è stato ridotto a “sole” due ore. Molti genitori non hanno capito e si sono anche lamentati; sono quelli che poi nei sondaggi *bocciano la Dad*. In verità, quei novanta minuti complessivi sono il massimo che si possa chiedere a bambini di sei anni, quindi con una soglia di attenzione bassa; fare di più sarebbe controproducente.

Invece, le lezioni a sette bambini hanno aumentato clamorosamente l’efficacia e la quantità di lavoro svolto. Le maestre hanno demandato alle famiglie una quantità elevata di compiti, esercizi, attività che altrimenti si sarebbero svolte in classe. Senza entrare nelle ovvie implicazioni sociali della questione, che però non dipendono dall’apprendimento digitale bensì da come siamo sempre stati abituati a organizzarci, la risultante è stata che per un mese il lavoro di classe:

- è progredito alla stessa velocità di quello in aula;
- tra lezioni su Meet e compiti, ha richiesto meno tempo, con un aumento dell’efficienza dell’insegnamento;
- ha portato a un aumento del dialogo, seppure online, tra maestre e genitori.

Naturalmente i bambini non hanno socializzato. Ma siamo in zona rossa e non è colpa della telescuola se i bambini non hanno socializzato. Anche in questo caso, la nostra organizzazione sociale è di un certo tipo e per vari aspetti si è adattata piuttosto bene alla convivenza con il virus; per altri aspetti, no.

Invece di [manifestare per *riaprire le scuole*](https://www.macintelligence.org/posts/Lingue-rubate-allagricoltura.html), senza che sia stato fatto nulla per consentire di farlo in sicurezza, lo scettico dovrebbe invece capire che oggi il lavoro dell’insegnante deve riverberarsi anche attraverso il mondo digitale, specialmente oggi che siamo in condizioni di emergenza, ma poi sempre; in certe situazioni la lavagna batte lo schermo, in altre vale il viceversa; la scuola vincente miscela il meglio delle due esperienze, invece che perdersi in manicheismi.

Le persone di buona volontà, docenti e genitori, possono trovare risorse, esperienze, pareri, creatività e conforto nel gruppo Facebook [La classe capovolta](https://www.facebook.com/groups/laclassecapovolta/) o presso [Insegnanti 2.0](https://insegnantiduepuntozero.wordpress.com). I protestatari potrebbero anche usare più spesso i cappelli a cono d’asino che esibiscono nelle piazze. In fondo, si capisce bene perché li maneggino con tanta familiarità.

*Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile.*