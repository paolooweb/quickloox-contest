---
title: "Fruizione fluida"
date: 2013-08-07
comments: true
tags: [Giochi, iPad]
---
[LiquidSketch](https://itunes.apple.com/it/app/liquidsketch/id544717096?l=en&mt=8) per iPad a 1,79 euro.<!--more-->

Rompicapo il cui scopo, di livello in livello, è travasare fluido colorato da una parte a un’altra dello schermo. I vincoli: il fluido è a bassissima viscosità, peggio del mercurio, va dappertutto con un niente. Gli spostamenti avvengono mediante l’inclinazione di iPad, che è letta con puntualità fin eccessiva. Negli schemi facili ci sono pareti che contengono lo scorrimento del fluido e bene o male il successo è garantito, ma in quelli difficili è pienamente possibili che il fluido si perda fuori dai confini dello schermo. Se non ne rimane abbastanza, bisogna rigiocare il livello.

Questa è la base. Procedendo, le cose si fanno (più) interessanti. I fluidi sono colorati e mescolandosi cambiano colore; a volte bisogna ottenere un colore preciso e quindi dosare perfettamente le parti di colore. Altre volte il fluido deve passare sopra macchie di colore come potrebbero trovarsi su una tavolozza e così facendo lavarle via cambiando tonalità nel frattempo (altre volte invece *non* deve passarci sopra…).

Quando sembra che l’interesse stia per scemare, perché i livelli sono impegnativi ma la ricetta è sempre quella, ecco che compaiono schemi nei quali dobbiamo intervenire modificando l’assetto delle pareti del gioco, pena l’impossibilità di risolvere lo schema. Poi arriva la possibilità di dirigere schizzi di fluido con le dita invece che inclinare iPad e poi arrivano ancora altre cose.

Alla fine pareva un gioco e si rivela uno strumento di creatività. Si possono creare nuovi schemi e, oltre a vantarsi delle proprie performance su Facebook o Twitter, anche filmare lo schema. Certe volte il fluido colorato che rimbalza e scorre ovunque diventa una gioia per gli occhi e su YouTube si possono vedere realizzazioni di chi ha provato. Anche la colonna sonora è particolare e su iTunes si trovano diverse creazioni del suo autore.

Giocarci da soli è stimolante, in compagnia è uno spasso; nessuno vuole perdersi le evoluzioni del fluido e una mossa sbagliata provoca disastri virtuali bellissimi da contemplare.

Il tutto, [scritto da uno studente di Zurigo](http://www.liquidsketch.com/about/) che dovrebbe completare la tesi e invece perde tempo, costa meno di quanto io spenda quando faccio colazione al bar. Ed è infinitamente più intelligente della *brioche*.