---
title: "Sacrifichiamo gli ebook"
date: 2023-12-31T19:51:09+01:00
draft: false
toc: false
comments: true
categories: [Software, Internet]
tags: [Munger, ebook, Charles Munger, Buffett, Warren Buffett, Gruber, John Gruber, Gruber, Daring Fireball, Make Something Great, Steve Jobs Archives]
---
C’era una volta l’uso di gettare qualcosa di vecchio fuori dalla finestra a fine anno per ingraziarsi l’anno successivo. Se toccasse a me resuscitarla butterei volentierissimo l’ebook, inteso come oggetto generico.

Lo stato dell’ebook è comatoso. Lo sviluppo dello standard è inesistente e nessuno mostra il minimo interesse a valorizzarlo. Per gli editori è importante che gli ebook restino banali e al minimo della complessità per tre ragioni almeno: capiscono solo l’oggetto libro tradizionale; riescono a produrli con il minimo di risorse e competenze senza dover imparare; gli ebook fanno schifo e così lasciano indisturbate le vendite della carta.

L’uso della parola *minimo* abbonda, come si vede; l’ebook esiste solo come minimo comune denominatore. A parte avanguardie ed esperimenti, esistono solo pedisseque imitazioni digitali di libri tradizionali. Sappiamo benissimo come il digitale abbia senso solo quando va oltre l’analogico; se si ferma a scimmiottarlo, c’è qualcosa di sbagliato.

L’ebook ha dunque un passato fallimentare ed è pure senza speranza. La ragione è che si fanno esperienze innovative e creative di comunicazione che valorizzano il digitale, anche da parte di soggetti rilevanti. Solo che avvengono sul web.

*La piattaforma principe per raccontare storie con impaginazione innovativa è il web*. Abbiamo già visto le inchieste ultramediali del *New York Times*, per esempio. John Gruber [fa riferimento](https://daringfireball.net/linked/2023/12/29/poor-charlies-almanack) all’[ultimo numero di *Poor Charlie’s Almanack*](https://www.stripe.press/poor-charlies-almanack), mentre ricorda [Make Something Great](https://book.stevejobsarchive.com) presente negli Steve Jobs Archives.

Si tratta di media disponibili anche su carta. Tuttavia, sono organizzati sul web in modo elegante, fresco, leggero. Se esistono come eBook, quest’ultimo è una copia sbiadita del corrispondente web. L’esperienza più ricca e coinvolgente è quella sul web. Un uso sapiente e raffinato del web permette un’esperienza impossibile per un libro di carta. Ciò che ci aspettiamo dal digitale, ciò per cui il digitale è nato.

Giù dalla finestra il vecchio, l’ebook, il sacrificio per ingraziarci la divinità. Battiamoci per avere media digitali capaci di dare tutto ciò che possono. Questo è il futuro migliore che ci aspetta, se lo costruiamo e supportiamo. Buon anno!