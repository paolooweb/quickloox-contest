---
title: "L'economia dell'informazione"
date: 2014-05-10
comments: true
tags: [Time, gossip]
---
Stupendo articolo di Time su che [cosa sia accaduto al televisore di Apple](http://time.com/91918/apple-hdtv/).<!--more-->

Se ne straparla dal 2008 e si continua, solo che i soliti bene informati adesso la danno per il 2015, dopo averla data per il 2014, per il 2013, il 2012, 2011…

Il riepilogo è devastante per la reputazione di tutti quei siti che truffano i gonzi ripetendo a pappagallo le invenzioni di gente ignorante dei fatti.

Anche senza una conoscenza dell’inglese, l’elenco delle scemate ordinate per anno dice tutto. Bellissimo il grafico finale, con l’andamento per intensità dei si dice sul televisore e di quelli sull’orologio da polso.

Alla fine l’autore si chiede che cosa si possa imparare dalla lezione:

>le anticipazioni che si contraddicono sono segno di inattendibilità. Quelle che suonano improbabili anche. Le notizie dai fornitori di componenti possono confondere. Gli analisti diventano irrazionalmente esuberanti e confondono quello che potrebbe succedere con quello che succederà. I brevetti hanno niente – ripeto, niente – a che fare con le uscite dei prodotti.

Il che chiarisce come funzioni l’economia dell’informazione per i siti suddetti: semplicemente economizzano sull’informazione a favore delle favole.

Buona azione per domani mattina: cancella un sito gossip Mac dai Preferiti del tuo Safari per destinare quel tempo a contenuti di qualità migliore. Oppure cambia genere: i siti di gossip sulle celebrità sono qualitativamente equivalenti, ma almeno sono dichiaratamente tali, hanno un sacco di foto vere e non pretendono di prevedere il futuro.