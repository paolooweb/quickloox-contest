---
title: "Due anni, un secolo"
date: 2014-02-19
comments: true
tags: [Rai, Degasperi, iPad, Silverlight]
---
Due anni fa ho preso parte a un collegio docenti presso l’[Istituto di Istruzione Alcide Degasperi](http://www.istalcidedegasperi.it/website/) di Borgo Valsugana (Trento), per tenere una presentazione dal titolo *Alla tavoletta di lavoro*. Era il primo 2011, iPad nella scuola era un oggetto misterioso, tanti insegnanti guardavano tra il meravigliato e lo scettico. Tutti, a fine incontro, volevano toccare la macchina e capire di più.<!--more-->

L’attenzione alla tecnologia digitale si deve al dirigente dell’Istituto, Paolo Pendenza, e grazie alla sua energia e capacità non si è fermata lì. Decisione dopo decisione, il dovuto connubio tra scuola e tecnologie digitali si è sviluppato e giorni fa il [telegiornale regionale](http://www.rai.tv/dl/RaiTV/programmi/media/ContentItem-5353a0d0-7ba0-45c4-b742-a6bc045acc69-tgr.html?refresh_ce#p=0) ha diffuso la notizia di una ennesima iniziativa dell’istituto: formare i ragazzi in modo da consentire loro l’adozione di pagine di Wikipedia. La notizia appare attorno al minuto 12:30.

In tutte le scuole dove metto piede, mi sgolo a spiegare che nelle scuole Wikipedia non va letta, ma scritta. Ovviamente la scelta del Degasperi non è merito mio; sono solo orgoglioso di trovarmi in sintonia con una idea tradotta in pratica.

Una bella storia, a lieto fine.

Se la si guarda su iPad. Su un computer l’antidiluviana Rai impone l’installazione di Silverlight di Microsoft, per nessuna ragione (tant’è vero che su iPad non serve) diversa da denaro e ammanicamenti.

Per una scuola che in due anni fa passi da gigante, abbiamo una emittente nazionale ferma al secolo scorso. Quei ragazzi però cresceranno.