---
title: "Aspettando godevo"
date: 2022-08-08T01:41:00+01:00
draft: false
toc: false
comments: true
categories: [Hardware]
tags: [Everymac.com, PowerBook 12” G4, Niae]
---
Ho acceso, a beneficio della quattrenne, un venerabile [PowerBook 12” G4](https://everymac.com/systems/apple/powerbook_g4/specs/powerbook_g4_1.33_12.html) oltre quattro volte più anziano di lei.

Macchina superba, ancora oggi piacevole da usare, una delle eccellenze nell’infinito elenco prodotti di Apple. Per quanto portatile di dimensioni minime, tutt’altro che problematico dal punto di vista delle prestazioni.

Eppure, acceso oggi, è *lento*. Neanche tra i più lenti, se conto le macchine archeologiche che mi è capitato di accendere ultimamente. Comunque, lento.

Ai tempi, era veloce. E lui è rimasto lo stesso di allora, senza aggiornamenti non voluto ad appesantirlo, prima ancora di aprire il suo browser inadatto all’odierna Internet.

A cambiare è stato il mio cervello. Lo hardware oggi a mia disposizione, percepito veloce, rende lento quello stesso PowerBook sentito come confortevole e svelto tanti anni fa.

Quel tempo di boot era un momento di soddisfazione e quasi orgoglio, di possesso di hardware all’altezza della situazione. Oggi è noia e impazienza.

Solo uno dei motivi per i quali amo il retrocomputing senza averne alcuna passione pratica. Le macchine vecchie mi ricordano quanto ero vecchio allora, che accettavo la lentezza come fosse stata un privilegio (e solo oggi colgo come quella lentezza fosse tale). La velocità dello hardware aggiornato è un elisir di gioventù.