---
title: "Una cartolina"
date: 2016-03-28
comments: true
tags: [Aaa, Sabbione, Matia, Bazar]
---
Metto da parte la programmazione consueta perché ho ricevuto una cartolina. Di quelle di una volta, stampate. Con l’immagine di un Apple IIc.<!--more-->

È firmata dagli amici del museo [All About Apple](http://www.allaboutapple.com), il [migliore del mondo](https://macintelligence.org/posts/2015-11-29-quanti-ricordi/), da poco tornato a nuova vita in una *location* importante e suggestiva.

È stata una bella giornata di Pasqua e ora lo è stata anche di più. Chi ha la possibilità, visiti il museo, magari sabato 2 aprile alle 17:30, in occasione del [concerto di Mauro Sabbione](http://www.allaboutapple.com/2016/03/2-aprile-concerto-mauro-sabbione-savona/), già tastierista dei Matia Bazar, quello di *Vacanze romane* (composto su Apple II con scheda [Alpha Syntauri](https://en.wikipedia.org/wiki/Apple_II_sound_cards), restaurata con l’aiuto degli esperti del museo). Non c’è mai stato niente di paragonabile a All About Apple, non ci sarà. 