---
title: "Dieci trucchetti che non ho capito"
date: 2013-09-10
comments: true
tags: [iPhone]
---
Ho sempre mal tollerato il cellulare. Ne ho fatto a meno più a lungo che potevo e, nel capitolare, ho comprato il più misero ed economico che ci fosse sul mercato.

Poi è uscito iPhone e l’ho adottato con entusiasmo. Perché non era un cellulare, ma un computer da tasca, completo *anche* di funzioni cellulari. A me il computer in tasca serviva, il cellulare era un male necessario.<!--more-->

Mi rendo conto di non essere un cliente di cellulari tipico e che la mia esperienza potrebbe essere inficiata. Nonostante questo vorrei spiegare perché trovo difficile da capire l’articolo di Marco Massarotto [Le 10 tecniche usate per far durare di meno i nostri telefoni. E la “E-Waste”: l’immondizia digitale che producono](http://marcomassarotto.com/2013/09/09/le-10-tecniche-usate-per-far-durare-di-meno-i-nostri-telefoni-e-la-e-waste-limmondizia-digitale-che-producono/#more-1390). A volere fare i pignoli, perfino il titolo ha qualcosa di strano: magari il mio telefono *diventerà* immondizia, ma di certo non ne *produce*. E comunque, se fosse immondizia *digitale* invece che analogica, avrebbe un impatto ambientale molto più trascurabile.

Ma il punto non è la lettera del titolo, bensì, il contenuto dell’articolo, dedicato ai trucchi praticati dalle aziende che praticano l’*obsolescenza programmata*. Un fenomeno avente qualcosa a che fare con il fatto che *cambiamo lo smartphone ogni dodici mesi*.

Ecco: cosa che io non ho mai fatto. A dirla tutta, l’ho visto fare a veramente pochi tra quelli che conosco. Siamo sicuri che la gente cambi lo *smartphone* ogni dodici mesi? Il mio iPhone del 2007 è ancora in funzione. Il mio iPhone 4S si è guastato dopo due anni e adesso vive il terzo sotto forma di sostituto rigenerato. Ho un iPhone 5 che sta per compiere un anno e non ho intenzione alcuna di cambiare.

Veniamo ai *trucchetti* per ottenere l’obsolescenza programmata.

> l’uso di materiali di (più) facile rottura

Si intendono per caso vetro e alluminio? Il [Gorilla Glass](http://www.corninggorillaglass.com) sempre più robusto a ogni edizione? Non capisco, davvero. Oltretutto vetro e alluminio si riciclano, la plastica del bel tempo andato no.

> rendere i costi di riparo e manutenzione alti come quello di acquisto di uno nuovo

La mia pochissima esperienza: comprato un iPhone 4S attorno ai seicento euro, se ricordo bene (se no erano cinquecento). iPhone guasto dopo due anni, sostituito per 149 euro con unità equivalente. No, il prezzo della “riparazione” non è affatto alto come quello del nuovo.

> impossibilità di cambiare le batterie, così il prodotto “dura quanto” la batteria

Devo ancora cambiare un iPhone o un iPad (cinque in totale, dodici anni-apparecchio) per esaurimento della batteria e non posso dire. Ho un’esperienza con il mio Mac, cui è impossibile cambiare la batteria. L’anno scorso ho fatto una gita all’Apple Store e ho chiesto di cambiarla. Il Mac è questo e ha quattro anni e mezzo di vita, la batteria è diversa da quella originale. Boh.

> release di nuove versioni di software (volutamente) incompatibili con le versioni precedenti

Tipo iOS 7, che si installerà su iPhone 5, 4S e 4?

> accorciare o ridurre i programmi di assistenza

Niente di questo mi risulta. AppleCare continua a valere tre anni su Mac e due su iOS.

> programmando nuovi design con lo scopo di far apparire “out of fashion” quelli precedenti, usando i cicli delle collezioni di moda

Si vocifera che stasera uscirà iPhone 5S, ovvero un design uguale a quello di iPhone 5. La moda segue cicli semestrali, il design attuale di iPhone durerà due anni come minimo.

> inviando notifiche dal vecchio telefono suggerendo che sia il momento di passare a uno nuovo

Non ho mai ricevuto qualcosa del genere. E ogni settimana a dire poco ricevo un’offerta di contratto telefonico, elettrico, del gas, della pay TV, di tutto. Che l’operatore voglia farmi cambiare telefono ci credo. Ma dal *produttore* del telefono, mai successo (a me; sarò strano). Ovviamente l’operatore influisce poco o nulla sull’obsolescenza programmata dell’apparecchio, se proprio non annulla la Sim sotto il naso del proprietario.

> far smettere di funzionare il prodotto

Si fa l’esempio delle stampanti, che può essere. Sui telefoni… Seriamente delle aziende che competono all’ultimo sconto e che lottano a coltello per piazzare le loro unità poi le predispongono per l’autodistruzione? Se veramente cambiamo il telefono ogni dodici mesi, l’esperienza di acquisto passata conta. Nessuno comprerà di nuovo un telefono che smette di funzionare perché ha deciso che basta. E l’azienda si farà male da sola. Sono perplesso.

> combinare più prodotti assieme in modo che il primo che finisce obblighi alla sostituzione anche degli altri

Anche in questo caso viene portato un esempio di stampanti, che con i telefoni mi sembra poco applicabile.

> La programmazione e introduzione scadenziata di nuove tecnologie o feature.

Se capisco bene, l’alternativa sarebbe inventare tutte le tecnologie subito. Negare il progresso che arriva dalla ricerca, le richieste del pubblico, il senso delle *startup* di tecnologia o il lavoro degli enti di standardizzazione. Fatico a comprendere.

L’articolo si chiude con considerazioni varie in gran parte condivisibili sullo smaltimento, l’inquinamento, il riciclo eccetera. (Il suggerimento di tenere i prodotti sei mesi in più, peraltro, a me pare unicamente un rinvio del problema).

Il punto non sono le conclusioni, ma la premessa: siamo sicuri che gli *smartphone* si cambino ogni anno? Siamo sicuri che le ragioni qui esposte siano quelle che fanno cambiare il telefono ogni dodici mesi? Nella mia esperienza c’entrano niente.

Mi si dirà che uso iPhone e dunque rappresento una nicchia; che l’utilizzatore tipico di *smartphone* è diverso da me. È senz’altro vero; la nicchia, tuttavia, gira intorno al mezzo miliardo di unità vendute. Rappresenterà pure qualche persona reale.