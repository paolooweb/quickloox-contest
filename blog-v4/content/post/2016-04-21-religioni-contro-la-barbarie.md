---
title: "Religioni contro la barbarie"
date: 2016-04-21
comments: true
tags: [Apple, Borsa, Milano, Mezzanotte]
---
Si è detto che Apple è una religione. Mentre intorno ci sono persone razionali e obiettive. Con le loro reti Wi-Fi.<!--more-->

 ![Elenco reti Wi-Fi comprensivo di rete di odiatore di Apple](/images/reti-wi-fi.jpg  "Elenco reti Wi-Fi comprensivo di rete di odiatore di Apple") 

In giro c’è gente con bisogni e meccanismi di autogratificazione che fanno impallidire qualsiasi coda all’Apple Store.

(Non siamo al villaggio vacanze dei magnaccioni, ma a [Palazzo Mezzanotte](http://www.lseg.com/it/palazzo-en) di Milano, sede della Borsa, durante il sesto [Forum sulla consulenza finanziaria](http://www.ascosim.it/Forum20160420_6ForumNazionale_evento.asp)).

 ![Operatore video a Palazzo Mezzanotte](/images/cameraman.jpg  "Operatore video a Palazzo Mezzanotte") 

Ennesimo malinconico operatore video professionista abbandonato da Apple.