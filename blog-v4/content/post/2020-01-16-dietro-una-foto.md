---
title: "Dietro una foto"
date: 2020-01-16
comments: true
tags: [iPhone, Google, watch, Archive, Unofficial]
---
L’ultima annata di iPhone si è contraddistinta soprattutto per le novità nel campo della fotocamera, un componente che comunque continua a migliorare anno dopo anno, cui viene dedicata molto attenzione dai media e dal pubblico. Anche qualche polemica, da parte di chi considera il sottosistema fotografico come degno di interesse poco o secondario.

Dietro a tutto questo c’è però un rivolgimento epocale che riguarda il ruolo assunto della fotografia nel nostro tempo. Sono finiti i tempi della fotografia di élite, anche se i fotografi di élite esistono e si distinguono a prima vista dal principiante ambizioso; sono finiti i tempi del dilettante pioniere, che spendeva molto tempo e molto denaro su uno spettro di equipaggiamento molto ampio, che comprendeva anche il telone da salotto e il videoproiettore per le diapositive. Oggi il dilettante ordina una fotocamera che fa da sola o quasi, a una frazione della spesa, soprattutto di tempo.

È cominciato un tempo nuovo in cui la possibilità di generare quantità illimitate di foto, unita al progresso della tecnologia digitale, apre spazi nuovi e possibilità prima sconosciute.

Tanto per dire, oggi parlano tutti – giustamente – dell’[(Unofficial) Apple Archive](https://www.applearchive.org) messo in piedi da un appassionato. Una raccolta di materiale video e, appunto, fotografico sulla storia di Apple che è ammirevole. Non è come visitare un museo Apple, ma è l’esperienza digitale che gli va più vicino. Nella vecchia epoca della fotografia non sarebbe esistito, punto.

Eppure il campo di azione della fotografia si allarga, molto più di un uso come questo che è straordinario nel risultato ma del tutto convenzionale nella metodologia, quella di un archivio fotografico.

Prendiamo per esempio i ricercatori di Google, che hanno trovato un [sistema per preannunciare la pioggia nelle prossime sei ore](https://ai.googleblog.com/2020/01/using-machine-learning-to-nowcast.html?m=1) in modo più preciso delle attuali previsioni.

Detta molto semplicemente, fotografano i pattern satellitari delle precipitazioni, li elaborano e li danno in pasto a una rete neurale. La quale sa assolutamente nulla di territori, nuvole, venti, temperature; semplicemente analizza i pattern e, dopo averne visionati un gran numero, riesce a fare previsioni su come si svilupperanno, anche quelli che non ha mai visto prima. Previsioni del tempo ottenute guardando (molte) foto. Anche qui, è il futuro della fotografia.

Ripensandoci, i test di precisione durante lo sviluppo di watch sono stati effettuati [filmando le unità con fotocamere capaci di scattare mille foto al secondo](http://www.macintelligence.org/blog/2016/01/04/minacce-di-precisione/) e quindi di individuare con grande precisione lo scarto tra il riferimento e il prototipo.

Non sono più le foto di una volta e quella fotocamera dietro al nostro iPhone può avere usi molto meno tradizionali di quanto pensavamo.