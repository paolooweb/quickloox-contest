---
title: "Scripta loqui"
date: 2021-12-02T00:24:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [Jason Snell, Six Colors, John Voorhees, MacStories, AppleScript, Terminale, Comandi rapidi, Workflow] 
---
In poche righe Jason Snell mostra su *Six Colors* [il collegamento tra Terminale, AppleScript e Comandi rapidi](https://sixcolors.com/post/2021/12/run-shortcuts-from-the-mac-command-line/).

Roba che su Windows te la sogni. Non perché manchi; i pezzi, volendo, si trovano. Perché l’integrazione è zero.

Su Mac, invece, non c’è soluzione di continuità tra un Comando rapido ad altissimo livello e un comando di Terminale a bassissimo livello, passando se necessario dal terreno di mezzo rappresentato da AppleScript.

Vale a dire che Apple non si è limitata a comprare [Workflow](https://workflow.is), la app che ha inventato i Comandi rapidi, ma ha anche provveduto a radicarla nel sistema operativo.

I tre ambienti parlano. Si parlano. Si toccano. Si collegano.

Del futuro non sappiamo; del presente, a proposito di script e automazione su Mac, da molto tempo non si vedeva un panorama così prospero e promettente. Se accanto al lavoro di Snell si mette quello di John Voorhees per MacStories – per esempio [il sistema per convertire in massa i Comandi rapidi dentro Monterey](https://www.macstories.net/stories/how-to-batch-convert-shortcuts-for-use-throughout-monterey-and-with-other-automation-apps/) – si capisce, che come comunità, saremmo sciocchi a non applicarci sull’enorme vantaggio competitivo che ci viene messo in mano gratis.

Familiarizziamo con i Comandi rapidi, che tra l’altro continuano ad affinarsi, allargare il campo di utilizzo, migliorare l’interfaccia.

_Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile._