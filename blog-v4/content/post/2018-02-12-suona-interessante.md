---
title: "Suona interessante"
date: 2018-02-12
comments: true
tags: [HomePod, Reddit]
---
C’è ancora qualcosa da dire su HomePod che, dopo avere già [sottolineato alcuni aspetti](https://macintelligence.org/posts/2018-02-07-lavoro-che-non-si-vede/) del prodotto, ha l’aria di essere definitiva.<!--more-->

Su Reddit, canale *audiophile*, è stata pubblicata una [recensione](https://www.reddit.com/r/audiophile/comments/7wwtqy/apple_homepod_the_audiophile_perspective/) che sembra piuttosto accurata della qualità audio di HomePod, accompagnata da centocinque megabyte di dati prodotti dagli esperimenti e dai test. *Otto ore e mezzo di misurazioni, oltre sei ore di analisi*.

Il risultato viene anticipato nel primo paragrafo:

>Sono senza parole. HomePod suona meglio di X300A di Kef. Per i non audiofili, Kef è una azienda molto amata e rispettata che produce altoparlanti. In verità ho cancellato e rieseguito le mie prime misurazioni perché mi sembravano troppo buone e pensavo di avere sbagliato qualcosa. Apple è riuscita a ricavare prestazioni di picco da un altoparlante delle dimensioni di una pinta; un’impresa che merita applausi a scena aperta. *HomePod è al cento percento un altoparlante a livello di audiofilo*.

Sono interessanti, ancorché interminabili, le reazioni dei lettori, senza le quali si potrebbe anche pensare al delirio di un fissato che è andato per conto suo. Invece pare esserci sostanza.