---
title: "Finire sui giornali"
date: 2017-07-30
comments: true
tags: [Machine, Learning, Journal]
---
Apple non prende iniziative pubbliche a caso e dispone di risorse economiche inimmaginabili, ma sta attenta a non sprecarle. Inoltre è una azienda riservata, meno che ai tempi di Steve Jobs ma ancora nettamente più che qualunque altra: si rivolge all’esterno solo per validi motivi.

Sono le premesse che mi fanno guardare con curiosità ad [Apple Machine Learning Journal](https://machinelearning.apple.com/), pagina strutturata come una pubblicazione scientifica, il cui primo numero consta di un singolo (interessante) articolo sulla realizzazione di sguardi artificiali che siano realistici.

Il *machine learning*, l’apprendimento meccanizzato, è alla ribalta tra gli addetti ai lavori ma non certo presso il grande pubblico. Se Apple ha sentito l’esigenza di creare una pubblicazione *ad hoc* c’è un motivo. Sarà stimolante sviscerarlo.
