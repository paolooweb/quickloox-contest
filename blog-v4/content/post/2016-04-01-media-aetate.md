---
title: "Media Aetate"
date: 2016-04-01
comments: true
tags: [Apple]
---
Avevo pensato a sviluppare una storia da primo aprile sul sottoscritto che, in compagnia di un amico, aveva iniziato a fabbricare aggeggi in un garage dell’hinterland milanese, finanziandosi con la vendita del fasciatoio e della culla, oramai superflui per sopraggiunto superamento dei limiti di età della prole.<!--more-->

Ma questo primo aprile sono quarant’anni. La mezza età per un’azienda è un traguardo notevole, specie in un settore dove il tempo è davvero tiranno.

Allora per stavolta punto alla ripubblicazione di [un articolo di Computerworld del 1978](http://www.macworld.com/article/3049921/macs/apple-at-40-seeing-promise-in-the-blossoming-home-computer-market.html), su una Apple ancor giovane che puntava a vendere microcomputer per la casa.

>Con più di centomila unità vendute, il mercato del personal computer è finalmente considerato come molto più ampio di quello originale degli appassionati di elettronica, ha dichiarato un portavoce di Apple, puntualizzando che per il 1985 si prevedono già vendite per due miliardi di dollari.

Oggi Apple ha un miliardo di apparecchi in uso e vende due miliardi di dollari ogni tre giorni.

Ora che ci ripenso, quella idea del garage per costruire un aggeggio non era da buttare via.
