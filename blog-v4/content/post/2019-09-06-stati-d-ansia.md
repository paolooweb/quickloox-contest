---
title: "Stati d’ansia"
date: 2019-09-06
comments: true
tags: [Uiguri, Schneier, Francesco, iPhone, Google, Cina, Massimo, Quasi, Dotcoma]
---
Grazie a [Francesco](http://quasi.me) per avermi segnalato l’intervento di Bruce Schneier, guru della sicurezza, a proposito dei siti malevoli scoperti da Google e [capaci di infettare un iPhone](https://www.schneier.com/blog/archives/2019/09/massive_iphone_.html) con la semplice visita:

>Quest’anno il Threat Analysis Group di Google ha scoperto una piccola quantità di siti manomessi per essere usati come vettore di attacchi contro i visitatori, allo scopo di installare software di monitoraggio. Si stima che i siti ricevano migliaia di visitatori ogni settimana.

>Sono stati raccolti oltre cinque meccanismi diversi per violare iPhone da iOS 10 fino alle versioni più aggiornate di iOS 12. Questo indica l’esistenza di un gruppo che opera per violare gli iPhone di certe comunità da almeno due anni.

**Aggiornamento:** Apple [sostiene](https://www.apple.com/newsroom/2019/09/a-message-about-ios-security/) che l’attacco sui siti in questione è stato attivo per un paio di mesi e non per due anni come sostenuto dal Threat Analysis Group. Tutte le vulnerabilità sarebbero state risolte in febbraio, nel giro di dieci giorno a partire dalla loro scoperta (fine aggiornamento).

Non è stato reso noto l’elenco dei siti a rischio visita; ma è emerso che gli stessi contengono software analogo per [infettare sistemi Windows e Android](https://www.forbes.com/sites/thomasbrewster/2019/09/01/iphone-hackers-caught-by-google-also-targeted-android-and-microsoft-windows-say-sources/#5d4bde9d4adf) e che sono siti [frequentati dalla comunità degli Uiguri in Cina](https://techcrunch.com/2019/08/31/china-google-iphone-uyghur/), problematica per il regime per via delle sue istanze autonomiste e religiose.

Anche in assenza di una attribuzione precisa, quindi, sono tutti concordi nel concludere che ci sia dietro la Cina, direttamente o indirettamente, con il suo desiderio di tenere monitorata la comunità anche senza il consenso di quest’ultima.

Quando mi interrogano sulla *privacy* tendo a rispondere che Google mi preoccupa molto meno dello stato. Perché Google e compagni possono essere fermati o possono essere elusi, o se non altro ostacolati. [Massimo](https://www.dotcoma.it), per esempio, ha iniziato a usare uno smartphone [/e/](https://e.foundation).

Ma uno stato determinato a sapere tutto di te, con mezzi leciti e illeciti, come lo si ferma?