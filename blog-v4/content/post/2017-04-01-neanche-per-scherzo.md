---
title: "Neanche per scherzo"
date: 2017-04-01
comments: true
tags: [Aprile, pesce, scherzo, Google]
---
La vita è andata talmente veloce oggi che, mentre avevo voglia di scrivere una caricatura dello *scripting* via Terminale, il giorno per farlo è abbondantemente terminato.<!--more-->

Non ho neanche fatto in tempo a guardare che cosa ha combinato Google, di solito molto attiva in materia. Perlomeno posso [linkare una pagina](https://en.wikipedia.org/wiki/List_of_Google_April_Fools%27_Day_jokes) senza neanche averla guardata, eppure sicuro che contiene qualcosa di interessante e aggiornato.

Non scherzo, anche se dovrei.
