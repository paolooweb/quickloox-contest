---
title: "Sensore a sensazione"
date: 2018-09-19
comments: true
tags: [iPhone, Gruber, Daring, Fireball]
---
Pochi giorni e John Gruber ha già scritto su *Daring Fireball* del suo [test con il nuovo iPhone XS](https://daringfireball.net/2018/09/the_iphones_xs).

Il riassunto è che, arrivando da un iPhone precedente al modello X, si tratta di un aggiornamento da prendere in considerazione. Arrivando da un iPhone X, c’è un miglioramento decisivo in un’area singola: foto e video.

Nell’articolo appaiono un paio di confronti fotografici che levano ogni dubbio. La parte degna di nota è che Gruber ha voluto capire come sono stati raggiunti i nuovi livelli di qualità.

La risposta è che sulle foto lavora, oltre a processore e processore grafico, anche la rete neurale inserita in un *chip* apposta, per applicare miglioramenti in simultanea con lo scatto. È qualcosa che senza una rete neurale a bordo è impossibile fare, almeno non in tempo reale. Gruber specifica che Google ha una intelligenza artificiale superiore a quella di Apple, ma senza *chip* a bordo un apparecchio Android può solo lavorare via cloud e ottenere risultati straordinari, ma sempre a posteriori e lavorando su un file Jpeg. iPhone XS lavora sui dati rilevati dal sensore. A velocità smodata:

>Quasi confonde vedere quanto è veloce la fotocamera a fare cose mentre acquisisce una immagine o il fotogramma di un video. Un metodo consiste nel creare una immagine e poi applicarle il machine learning. L’altro è usare il machine learning per creare l’immagine. Un modo in cui Apple ottiene questi risultati con il video è mediante l’acquisizione di fotogrammi addizionali *tra un fotogramma e un altro*, sempre filmando a trenta fotogrammi per secondo, perfino in 4K. L’inteno percorso di input/output tra sensore e Neural Engine è talmente veloce che la fotocamera di iPhone XS può manipolare fotogrammi di video 4K come Neo si sbarazza dei proiettili in *Matrix*.

Altra fonte dei miglioramenti della resa fotografica è il sensore: più grande del 32 percento. È un dato enorme in un terminale e in un mercato che evolve da oltre dieci anni. Si pensi che per necessità ottica, il sensore è stato ingegnerizzato in modo che sia più lontano dall’obiettivo di quanto sia stato fatto finora. Pensando a quanto è sottile un iPHone, deve essere una specie di record di progetto e forse un miglioramento successivo richiederà mezzi di distribuzione alternativi.

Non è tutto. Nel fare il sensore più grande, Apple ha conservato la risoluzione ormai tradizionale di dodici megapixel. E ha prodotto *pixel più grandi*. Che acquisiscono più luce e producono maggiore accuratezza.

>Apple non ha parlato di questo sensore. Ma i fotografi lo faranno.
