---
title: "Ottanta voglia di ipertesto"
date: 2022-11-07T00:15:48+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [Fibery, HyperCard, Xanadu, NoteCards, Intermedia, Nelson, Ted Nelson]
---
Chi vuole indietro HyperCard non ha ragione; bisogna comunque andare avanti. Chi vuole i *concetti* di HyperCard invece ha ragione da vendere, come testimonia l’articolo (la *gemma*, nel loro gergo) di Fibery sugli [strumenti ipertestuali degli anni ottanta](https://fibery.io/blog/hypertext-tools-from-the-80s/).

La lettura è agevole, una dozzina di minuti, nonostante il tema sia bello denso. Velocemente.

Nel 2020 stiamo cercando di recuperare concetti che, se non fosse arrivato il web, ci avrebbero portato molto lontano (o almeno avrebbero potuto).

Il web prese il posto di tutto perché era infinitamente più semplice di tutto. Un sistema come Xanadu di Ted Nelson avrebbe potuto risolvere tanti dei problemi del web attuale; Nelson aveva pensato praticamente a tutto, ma la complessità estrema del risultato gli tagliò le gambe.

HyperCard fu tutt’altro che il primo sistema di organizzazione ipertestuale delle informazioni.

I problemi fondamentali oggi: tenere insieme l’organizzazione di contenuti strutturati e contenuti non strutturati; tenere insieme l’organizzazione della conoscenza e del lavoro.

Chi avesse buone idee ma soprattutto la capacità di affermarle e di farsi finanziare per farlo, oggi avrebbe grandi praterie davanti. Forse morirebbe (metaforicamente) di stenti e privazioni, oppure potrebbe arrivare all’Eldorado dell’organizzazione della conoscenza.

Suona la campanella, la lezione è finita; tutti a giocare con [Decker](https://beyondloom.com/decker/tour.html). Soprattutto i nostalgici. (Gioco fino a un certo punto).