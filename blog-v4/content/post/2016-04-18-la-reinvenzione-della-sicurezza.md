---
title: "La reinvenzione della sicurezza"
date: 2016-04-18
comments: true
tags: [sicurezza]
---
Apple ha una ragione di esistere quando leva di mezzo o almeno riduce in modo apprezzabile la burocrazia informatica: le pratiche e le necessità che si frappongono al raggiungimento degli obiettivi in modo semplice ed efficace.<!--more-->

La sfida dei prossimi dieci anni in questo senso sarà la sicurezza. Abbiamo vissuto finora nel regno delle favole, dove l’orco minaccia solo chi si avventura nel bosco di notte. La realtà si avvicina invece a un vicolo angusto, affollato e assai mal frequentato dove ogni passo è a rischio e chiunque ci affianca potrebbe avere mani troppo lunghe.

Le soluzione tipica proposta oggi è *arrangiati*. Copriti ovunque con le mani, cuci le tasche, scruta ogni anfratto, fuggi ogni ombra; qualunque deviazione dalla strada maestra è a tuo rischio.

Mentre la responsabilità individuale non è completamente eliminabile, né sarebbe desiderabile che lo fosse, la strada da compiere in termini di infrastruttura e interfaccia è agli inizi e sterminata.

La rete dovrebbe diventare più simile a un’autostrada, dove il caso di trovare qualcuno in viaggio contromano è remotissimo. Più analoga a un albergo, dove è estremamente improbabile che in camera ci attenda un mariuolo. Ispirata a uno stabilimento balneare, dove è normale assentarsi dall’ombrellone per una nuotata e ritrovare al ritorno quello che sotto l’ombrellone si era lasciato.

Sono tutte situazioni dove i requisiti di sicurezza esistono, ma sono contenuti in modo ragionevole e l’infrastruttura e l’interfaccia si assumono la maggior parte dei compiti. In autostrada non bisogna autenticarsi ogni dieci chilometri, in albergo non c’è bisogno di frugare negli armadi alla ricerca di malintenzionati, in spiaggia non ci sono finti bagnini.

All’opposto, una giornata tipo nel mondo digitale oggi si trasforma spesso in una sfilata di password, autenticazioni a due fattori, verifiche punitive, misure draconiane, diffidenza verso il cliente. Ho già parlato del [passcode di iOS](https://macintelligence.org/posts/2015-10-28-vita-da-passcode/) ed è solo un esempio.

C’è una lunga strada da percorrere, verso un mondo dove il requisito di sicurezza è ragionevole: mi autentico al mattino, la prima volta, in modo diretto o indiretto non importa. Ma arrivo a sera libero di occuparmi delle cose cui tengo. Che non sono il firewall, la password, il certificato, il livello di *privacy*, il Pin, il Puk, il dover dimostrare di continuo che siamo quelli buoni.

Possiamo passeggiare comodamente senza esibire la carta di identità a ogni incrocio e lo stesso dovrebbe poter accadere anche in digitale.

Se non lo fa Apple non lo farà nessuno e anzi andrà sempre peggio.

Speriamo che qualcuno a Cupertino sia capace e voglioso di questa reinvenzione. Scriverebbe la storia quasi più che come si è fatto con Macintosh e iPhone.