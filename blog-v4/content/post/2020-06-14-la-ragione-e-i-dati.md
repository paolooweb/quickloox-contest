---
title: "La ragione e i dati"
date: 2020-06-14
comments: true
tags: [Arm, Mac, Intel, Fontcase, open, source, Attivissimo]
---
Passerà Mac ai processori Arm? Predico che a WWDC in programma tra una settimana ci sarà un annuncio, ma nessun nuovo Mac già pronto. Al massimo qualche prototipo per gli sviluppatori, se va strabene una promessa per l'autunno. Altrimenti, 2021.

Il giorno prima che Mac passasse a Intel spiegai in una mailing list perché non sarebbe successo. Avevo assolutamente ragione; meglio, il mio ragionamento era impeccabile.

Peccato che non fossi aggiornato. Mi mancavano dati. Dati che ignoravo e facevano a pezzi il mio impeccabile ragionamento.

Ho imparato a cercare sempre e il più possibile i dati necessari al ragionamento, perché gli uni e l'altro si completano e sono reciprocamente necessari. Per quello scrivo _predìco_: il mio ragionamento non fa una grinza. Solo che potrebbero mancarmi dati.

Ieri ho letto una buona notizia su _MacStories_: [The Iconfactory collabora con uno sviluppatore indipendente e pubblica una app gratuita e open source per facilitare l'installazione dei font su iPad](https://www.macstories.net/reviews/fontcase-simplifies-custom-font-installation-on-ios-and-ipados/).

Installare su iPad font diversi da quelli forniti di serie presenta rischi di sicurezza e avere una app open source per farlo offre garanzie superiori; se qualcosa non quadra, chiunque può dare l'allarme.

Quindi è più facile installare nuovi font su iPad e farlo in sicurezza, grazie a una app open source.

Otto anni fa ho avuto un [lunghissimo scambio di opinioni sul blog di Paolo Attivissimo](https://attivissimo.blogspot.com/2012/07/chiude-il-minitel-precursore-di.html?m=1#c5115890163982989219) attorno alla chiusura di iPad e ai rischi di monopolio dell'informazione.

Paolo convinto che su App Store non possano esistere app open source. I suoi lettori preoccupatissimi da scenari apocalittici dove perdiamo ogni libertà di uso degli strumenti informatici.

Ragionamenti ineccepibili. Tanta ostinazione nel negare i dati.

Otto anni dopo, rileggi e capisci che vivono in una bolla diversa dalla realtà.

Ragionare è importantissimo. Ragionare senza usare i dati, senza essere informati, è monco.