---
title: "Fantascienza, fantasia, farneticazione"
date: 2015-09-17
comments: true
tags: [Attivissimo, Drm, tvOS, AppleTV, Vlc, VideoLan]
---
La cronaca mi spinge a riprendere questo brano (indovina [chi lo ha scritto](http://attivissimo.blogspot.ch/2015/09/apple-presenta-la-sua-nuova-gamma-di.html)).<!--more-->

>pirateria del cinema? Non sarà più un problema, se su un iCoso girerà soltanto il media player con DRM (sistemi anticopia) e i video senza DRM verranno bloccati o degradati.

La risposta [arriva da This Week in VideoLan](http://www.jbkempf.com/blog/post/2015/This-week-in-VideoLAN-18):

>Oh, e one more thing… abbiamo cominciato a preparare Vlc per tvOS. […] È molto presto per il resto, ma abbiamo già il playback del video!

[Vlc](http://www.videolan.org/vlc/), il software libero per eccellenza per guardare video in qualsiasi formato, è in arrivo su AppleTV, l’iCoso per eccellenza, finora il più chiuso e inaccessibile di tutti.

Come dire che la citazione sopra è fantascienza, fantasia o farneticazione.