---
title: "Stessa spiaggia, spesso mare"
date: 2016-07-02
comments: true
tags: [watch]
---
Faccio seguito al *post* in cui si riferiva degli [esperimenti con watch e l’acqua](https://macintelligence.org/posts/2015-07-18-un-po-water-un-po-proof/) per dire che, dopo quasi un mese di sperimentazione sulla pelle – in senso letterale – posso solo confermare che watch, con un uso normale, non ha problemi.

Come uso normale intendo nuotata quotidiana o gioco con le onde a riva, doccia con acqua dolce una volta tornati a casa, gioco con la sabbia, qualche metro di pinneggio sott’acqua tanto per ricordarsi l’effetto che fa, cose piccole senza sollecitazioni e senza insidie nascoste, però ignorando di avere l’apparecchio al polso: niente precauzioni, niente esitazioni, niente delicatezze.

watch funziona tranquillamente. Anzi, iPhone resta nella tasca dei pantaloncini – niente sabbia, niente sole, niente salsedine – e se arriva un messaggio, una notifica, qualcosa di importante e indilazionabile, si può dare una risposta rapida da watch sotto l’ombrellone, in modo anche discreto.

Insomma, confermo: watch convive benissimo con la spiaggia e il mare.

*[Se il progetto di Akko di un [libro adeguato all’epoca digitale](https://macintelligence.org/posts/2016-06-15-un-kickstarter-per-cambiare-il-libro/) troverà [i finanziamenti che cerca](https://www.kickstarter.com/projects/295108589/cuore-di-mela-i-segreti-di-apple-iphone-e-mac), si parlerà anche dell’interazione tra watch, iPhone e sì, anche Mac.]*