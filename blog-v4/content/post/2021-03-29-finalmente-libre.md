---
title: "Finalmente Libre"
date: 2021-03-29T02:12:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [Tom Taschke, OpenDocument Reader, OpenDocument, TextEdit, LibreOffice] 
---
Uno dei miei punti fermi è poter replicare i flussi di lavoro senza troppi problemi attraverso Mac, iPad e – in modo ragionevole – perfino su iPhone.

Su iPad e iPhone ho avuto finora un grosso tallone d’Achille: i file [OpenDocument](http://opendocumentformat.org), usati per esempio da [LibreOffice](https://www.libreoffice.org) (ma anche, lato testo, da TextEdit: provare per credere). Riceverne uno su iPad, senza Mac a disposizione, era un grosso ostacolo. Sì, accesso remoto a Mac, apertura con LibreOffice, salvataggio in altro formato, invio a iPad… ma è scomodo, inelegante e non sempre fattibile.

Finalmente ho scovato [OpenDocument Reader](https://opendocument.app), app [open source](https://github.com/opendocument-app/OpenDocument.ios) di un programmatore austriaco che si comporta bene come lettore di documenti e offre anche l’editing minimo per sistemare un refuso o compiere una piccola modifica.

Inizialmente diffidente, ho scaricato la versione Lite sostenuta dalla pubblicità. C’è un bannerino in alto di dimensioni più che ragionevoli e per il resto la app è pienamente fruibile.

Risolto il mio problema di visualizzazione file, soddisfatto ho curiosato su App Store per vedere quanto costasse la versione a pagamento, completamente uguale alla Lite tranne l’assenza di pubblicità.

Due euro e ventinove. Circa l’ottocentesima parte del valore totale del lavoro che sto svolgendo.

L’ho comprata subito e va a finire che gli lascio pure la recensione sullo Store.

*Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile.*