---
title: "Al centro della forbice"
date: 2012-08-22
comments: true
tags: [Css3, jQuery]
---
Un eccellente grafico interattivo <a href="http://designmodo.com/create-interactive-graph-css3-jquery/">in un’ora</a>, con Css3 e jQuery, con due modalità intercambiabili di visualizzazione. Tutto prodotto da codice, con la spiegazione passo per passo.

Da una parte siamo vicini al centro della forbice: a breve saranno cose di una complessità quasi riservata ai professionisti. Nel contempo l’eleganza e la potenza di queste soluzioni è tale che, anche non professionisti, viene voglia di approfondire.