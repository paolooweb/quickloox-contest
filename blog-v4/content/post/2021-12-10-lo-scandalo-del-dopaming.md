---
title: "Lo scandalo del dopaming"
date: 2021-12-10T15:24:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [Mario, Cangini, dopamina, quantum computing, qubit, Lubiana, Lidia, Alba A Wildlife Adventure, Red Legged Partridge, pernice rossa] 
---
Ringrazio **Mario** che [mi ha messo sulle tracce](https://twitter.com/emmerizzi/status/1468511934429466628) dell’importante discorso di un senatore italiano [impegnato nella conferenza interparlamentare di Lubiana](https://www.linkiesta.it/2021/12/digitale-scuola-imparare-apprendimento/) sulla trasformazione digitale nell’educazione.

<blockquote class="twitter-tweet"><p lang="it" dir="ltr">Il digitale non sollecita il cervello, dice il senatore Cangini. Mi vengono dei commenti irripetibili, <a href="https://twitter.com/loox?ref_src=twsrc%5Etfw">@loox</a> saprà sicuramente esprimerli meglio di me. <a href="https://t.co/rLmWGWiEqB">https://t.co/rLmWGWiEqB</a></p>&mdash; Mario Rizzi (@emmerizzi) <a href="https://twitter.com/emmerizzi/status/1468511934429466628?ref_src=twsrc%5Etfw">December 8, 2021</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

Nell’intervento è stato infatti dichiarato:

> […] ovvio è che studenti e docenti dovranno sviluppare una competenza digitale […]

e, pochi istanti dopo, quest’altro:

> teniamo il più possibile il digitale fuori delle scuole.

In pratica, l’Italia ha implicitamente annunciato di avere conseguito la [supremazia quantistica](https://www.scienzainrete.it/articolo/supremazia-quantistica-dimostrata-con-fotoni/chiara-sabelli/2020-12-10). Nel _quantum computing_ i _qubit_ contengono sia lo zero che l’uno, solo che acquistano un valore preciso appena l’operatore lo chiede. Le scuole italiane, invece, terranno insieme indefinitamente una nozione e il suo contrario.

Lascio perdere gli altri pezzi del discorso senatoriale, di raro pressapochismo e vacuità, oltre che ostinatamente contraddittori (tipo mettere insieme *la mente sta mutando* con *il digitale non sollecita il cervello*). Il livello è appena sopra il negazionismo, se qualcuno volesse un giudizio complessivo.

Mi limito a raccontare un aneddoto personale di poche ore fa. Mia figlia, sette anni, è arrivata a chiedere *papà, che uccello è il Red Legged Partridge?* Il papà, ignorantissimo di ornitologia specialmente se declinata in lingua straniera, ha chiesto a Google la traduzione italiana e ha ottenuto la risposta, in trenta secondi.

La figlia ha chiesto lumi a seguito dell’avvistamento della pernice rossa in [Alba: A Wildlife Adventure](https://apps.apple.com/it/app/alba-a-wildlife-adventure/id1528014682). Un *videogioco*, temutissimo dal senatore perché attività che favorisce il rilascio di [dopamina](https://www.doveecomemicuro.it/enciclopedia/anatomia/dopamina), *il neurotrasmettitore della sensazione di piacere*.

Sono talmente preoccupato che ora scarico [Programmare è per tutti: 5-8 anni](https://www.apple.com/it/education/docs/everyone-can-code-early-learners.pdf) di Apple. Guida per gli insegnanti. Poveracci; rischiano l’intervento della commissione antidopaming.

_Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile._