---
title: "Un problema da inquadrare"
date: 2020-09-26
comments: true
tags: [DaD, Mac, mini, Pencil, Apple, iPad, Pro]
---
Bisognerà tornare in modo approfondito sull’argomento, però vorrei sintetizzare qui e ora.

Dopo sei mesi dal *lockdown*, il pubblico preparato *inizia* a capire come comportarsi in videoconferenza e ad avere i primi indizi di come si collabora. Ci si può immaginare il pubblico non preparato.

Ci fu un tempo con un computer per nazione, poi uno per azienda, poi uno per famiglia, poi uno per persona e oggi numerosi computer per persona.

A livello di videoconferenza, siamo al punto in cui c’è uno schermo per persona. Non ci si è resi conto, ancora, che inizia l’era di più schermi per persona.

È ora di cominciare a rabbrividire quando senti di qualcuno che si chiede che computer comprare per *fare le call*. Che c’entra il computer? O lo si fa come capita e allora va bene tutto, oppure ci si organizza.

Non ti serve un computer. Ti serve una buona illuminazione. Una buona inquadratura. Una buona videocamera. Forse è il momento di avere _due_ videocamere e l’embrione di una regia. Un treppiede, forse. Quando diventerà una cosa seria, auricolari wireless il più invisibili possibile dove non si può procedere con diffusione audio *en plein air*. Strumenti collaborativi che sono certo software, ma anche hardware. La condivisione schermo sembra l’ultima frontiera, in certi uffici. Ragazzi e ragazze, è l’inizio. Ci sono le lavagne condivise, le realtà aumentate, l’animazione, il disegno a mano libera (c’è da pensare a una Apple Pencil prima che alla videocamera), l’interattività, la simulazione, gli editor collettivi, i materiali offline, quelli indipendenti dal tempo reale, i form, i Mud e qualcuno domani inventerà un’altra cosa.

Non serve tutto a tutti. A molti basta una cosa sola per fare una cosa sola. Probabilmente si tratta di un buon obiettivo montato su un computer da tasca, non un computer. Se non altro perché la prossima call potresti averla in stazione, fuori da scuola, su una panchina al parco con lo sfondo natura.

(Quando si parlerà degli sfondi, ecco, se dietro di te ci sono le piastrelle della cucina, o le tende ereditate dalla nonna arrotolate con i fiocchetti, il tinello interno giorno, l’ex ripostiglio, il salotto stile pornosoft, i quadri dal mercatino del finto antiquariato incorniciati in finto legno fintamente firmati da finti pittori, allego un consiglio empatico e solidale: usa un software con gli sfondi artificiali e metti la Terra vista dallo spazio, il prato, il Mondrian, lo skyline, la foresta dall’alto, l’atollo, le orecchie di Topolino, il *chroma key*. METTI QUALSIASI ALTRA COSA).

Il lockdown mi ha insegnato che la mia macchina più importante, per le *call* articolate, è *Mac mini*. Senza microfono, senza videocamera. Questo, in un setup tuttora amatoriale (che dovrà raffinarsi), mi permette di interagire con le persone tramite iPad Pro e con i contenuti mediante lo schermo grande.

Gente che prima di entrare in una sala riunioni passava mezz’ora a sistemare la cravatta o rifarsi il trucco (talvolta ambedue), adesso si mette in favore di camera con l’angolo che capita, la luce che capita, cadesse il cielo piuttosto che guardare una volta dentro l’obiettivo. Novità sconvolgente: dall’altra parte ci sono altre persone. Si ricorderanno della tua sciatteria. Per ora sono sciatte anche loro e quindi vale tutto, ma – altra novità sconvolgente – non durerà.