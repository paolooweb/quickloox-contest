---
title: "Cose che toccano il cuore"
date: 2018-12-11
comments: true
tags: [watch]
---
La quarta serie di watch contiene [i famosi elettrodi per la funzione di elettrocardiogramma](http://www.macintelligence.org/blog/2018/10/05/il-conto-degli-elettrodi/) ed è appena stato divulgato il fatto che si può misurare il battito manualmente tenendo un dito appoggiato alla Digital Crown.

In questo modo, riferisce [9to5Mac](https://9to5mac.com/2018/12/11/apple-watch-improve-heart-rate-readings/) che ripete l’informazione ufficiale di Apple, la lettura diventa più affidabile in quanto dai fotodiodi sul polso si passa ai sensori per l’elettrocardiogramma e la misura avviene ogni secondo invece che ogni cinque.

La cosa interessante è che in molti Paesi, al contrario di quanto è avvenuto negli Stati Uniti, la funzione elettrocardiogamma non è stata approvata dalle autorità. Tuttavia qualsiasi watch Series 4, ovunque si trovi nel mondo, può effettuare la misurazione nella forma più accurata.

Tocca il cuore pensare che probabilmente questo porterà ancora più vantaggi alle persone che possono avere bisogno di sorvegliare attentamente il proprio battito. Sull’atteggiamento dei governi che tardano a pronunciarsi (non è che disconoscano la presunta efficacia della misurazione o la riconoscano; semplicemente hanno di meglio da fare), invece, viene da toccare dell’altro.