---
title: "La lunga marcia verso il nulla"
date: 2016-06-21
comments: true
tags: [MacNN, Urss]
---
Quando, un numero oramai discreto di anni fa, cadde il blocco sovietico e si dissolse l’Unione omonima, tra mille immagini e video e commenti mi colpì uno [striscione di manifestanti](http://it.rbth.com/rubriche/Mosaico/2015/11/20/quel-che-resta-dellimpero-sovietico_542781): *Settant’anni di marcia verso il nulla*.<!--more-->

Un’altra marcia verso il nulla è quella che riguarda le testate specializzate sul mondo Apple: [a fine giugno chiuderà MacNN](http://www.macnn.com/articles/16/06/20/long.time.staff.winding.up.two.decades.of.service.at.the.end.of.june.134716/), dopo ventuno anni di più che onorabile servizio e notevole qualità.

I motivi sono molti e uno di quelli messi in chiaro è il successo di Apple:

>C’è meno bisogno di un sito di notizie specializzato su Apple quando le notizie su Apple sono spalmate ovunque, su qualsiasi sito, in ogni momento.

C’è molto di vero. L’unica risposta possibile è alzare l’asticella e porsi a un livello decisamente e percett˙ibilmente superiore a quello della media. La maggioranza si nutrirà del pastone dei siti qualunque che pubblicano notizie qualunque, ma ci sarà una massa – si spera critica – di persone che si aspettano qualità superiore, risposte dettagliate a domande precise, conoscenza invece che racconto a base di copiaincolla.

La marcia verso il nulla, anzi, verso il pastone universale, è probabilmente inevitabile. Tuttavia c’è del grande merito in chi sa rallentarla e ostacolarla al meglio delle sue possibilità.

*Se qualcuno pensa che ciò possa avere a che fare con il lancio di [un libro diverso dal solito](https://macintelligence.org/posts/2016-06-15-un-kickstarter-per-cambiare-il-libro/) sul mondo Apple, che alzi l’asticella nel mondo editoriale, ha ragione. Chi si sente di [pubblicizzare e contribuire](https://www.kickstarter.com/projects/295108589/cuore-di-mela-i-segreti-di-apple-iphone-e-mac) alla riuscita di questo progetto, avrà fatto molto più di quello che sembra.]*