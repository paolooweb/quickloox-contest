---
title: "Una storia in sviluppo"
date: 2019-01-08
comments: true
tags: [Sol, Robots, Clark, Crossword, Simile]
---
Mi ha scritto (in quanto già cliente, non per altro; è una newsletter) Cortis Clark, autore di *Crossword Forge*, programma che avevo comprato a suo tempo per creare enigmistica su Mac.

Clark ha venduto Crossword Forge e altri programmi attraverso la sua società Sol Robots, che però ha dovuto lasciare nel 2011 per un calo delle vendite, accettando lavori presso Apple e Google allo scopo di mettere insieme pranzo e cena.

Nel 2017 ha risparmiato abbastanza denaro per riprovarci e, fondata [Save the Machine](http://save-the-machine.com), ha lavorato venti mesi allo sviluppo di *Simile*, app per iOS con cui conta di risalire la china.

Clark mi ha proposto di contribuire al suo successo in vari modi, a partire dallo scaricamento (gratis) di Simile per andare a recensione della app, acquisto di funzioni in-app eccetera.

Per il momento ho solo scaricato la app, che comunque è già qualcosa. Non ho ancora lanciato Simile e non ho idea di quante promesse mantenga, oppure se valga la pena di un acquisto.

Certamente ho riflettuto sul volto umano del mondo del software. Per molti di noi significa storcere il naso davanti a una percepita estorsione di un euro o due; per alcuni è la differenza tra mantenere la famiglia orgogliosamente oppure faticosamente.

Non so se farò parte degli acquirenti di Simile; certamente ha saputo raccontare bene la sua storia e già questo è un titolo di merito.