---
title: "La voce querula"
date: 2014-02-11
comments: true
tags: [Macintosh, Jobs, Archeologia, Parc, Xerox]
---
L’amico [Stefano](http://www.stefanopaganini.com) mi ha tirato dentro il *podcast* di Archeologia Informatica dedicato ai [trent’anni di Mac](http://www.archeologiainformatica.it/2014/02/09/podcast-1x05-30-anni-di-macintosh/).<!--more-->

È stata una bella cosa, organizzata in fretta e furia nel bel mezzo della mia [transizione piuttosto improvvisa](https://macintelligence.org/posts/2014-02-03-veloce-e-silenzioso/) a nuove ventole e nuovo disco, così mi sono presentato preparatissimo, ma senza avere collaudato l’insieme come avrei dovuto: per motivi che devo ancora appurare, sotto la voce si sentono rumori che non avrebbero dovuto esserci.

Ciononostante la cosa è stata molto divertente e anche l’occasione per fare conoscere a un pubblico un po’ più ampio certe piccole verità, tipo che Steve Jobs non ha affatto rubato l’idea dell’interfaccia grafica dal Parc Xerox ma piuttosto è stato l’unico a capire quello che aveva davanti e varie altre (tante) cosette.

Per le cose che c’erano da dire sono mancate altre due o tre ore di tempo. Ma non dispero che si possa rimediare prima o poi. E mi presenterò con un *setup* più adeguato e la voce meno querula, se mi ricordo.