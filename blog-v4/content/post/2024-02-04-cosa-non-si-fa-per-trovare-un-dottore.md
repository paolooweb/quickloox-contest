---
title: "Cosa non si fa per trovare un medico"
date: 2024-02-04T02:57:04+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [Axiom 3, GVM Assistance Space Health, MMN, Magnetic Media Network]
---
La missione spaziale [Axiom 3](https://www.wired.it/article/axiom-3-missione-iss-spacex-villadei-esperimenti/) è prossima al completamento, con l’ammaraggio che avrà luogo nei prossimi giorni dopo [un ritardo](https://www.foxweather.com/earth-space/axiom-three-mission-european-crew-return-florida-weather-delay) dovuto al maltempo.

Gli astronauti hanno beneficiato durante la missione di una app di telemedicina, [GVM Assistance Space Health](https://gvmassistance.it/it-it/news/gvm-assistance-sale-a-bordo-della-missione-spaziale-ax-3-per-monitorare-la-salute-degli-astronauti), capace di tenere sotto controllo i parametri vitali e comunicare con la Terra per aggiornare il comando missione.

Ho scoperto che nell’operazione c’è lo zampino del gruppo StartApps operante in [Magnetic Media Network](https://www.mmn.it), il più grande rivenditore Apple italiano.

Un pezzo di Italia, un pezzo di amici, un pezzo di tutela della salute a distanza… spaziale che ha consentito all’astronauta italiano a bordo di non aspettare sei mesi per un consulto dall’orbita.