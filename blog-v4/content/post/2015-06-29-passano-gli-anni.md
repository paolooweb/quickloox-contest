---
title: "Passano gli anni"
date: 2015-06-29
comments: true
tags: [Avalon, British, Legends, Mud, Umbra]
---
Per un attimo credevo di avere trovato un Mud (Multiuser Dungeon) ancora più vecchio di [British Legends](http://british-legends.com/CMS/). Poi ho verificato meglio e il primato di longevità resiste: il software data addirittura al 1978.<!--more-->

[Avalon](http://www.avalon-rpg.com/play/) invece è del 1985, quindi non il più antico dei Mud. Però si tratta di un’esperienza da provare e c’è più o meno la garanzia di trovare sempre e comunque un centinaio di altri giocatori collegati. Un tocco utile per la fruizione consiste in un *client* di gioco in Html, [Umbra](http://umbra.avalon-rpg.com), compatibile con iPhone e iPad.