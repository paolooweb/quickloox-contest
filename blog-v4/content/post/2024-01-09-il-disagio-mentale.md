---
title: "Il disagio mentale"
date: 2024-01-09T17:11:21+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [WordPress, FontAwesome]
---
Mi tocca lavorare su installati Wordpress che non sono miei né frutto di mie scelte. Su uno di questi ci si accorge all’improvviso che in fondo alla pagina home non si vedono i soliti pulsanti-icone che portano verso i social.

Qui bisogna sapere che molti sviluppatori web mettono i pulsanti-icona usando il servizio di [Font Awesome](https://fontawesome.com): in sostanza si inserisce un codice relativo a un kit di icone (gratis o a pagamento) nella sezione `Head` della pagina e poi mostrano le icone desiderate con una semplice linea di normalissimo codice Html.

Le icone dovrebbero vedersi, ma così non è. Frugo. In fondo alla pagina Footer trovo un blocco di *Raw Code*, come spiega il plugin adibito a generare i contenuti. Lo apro. Il plugin mostra una finestrella minuscola, da dilatare altrimenti il codice resta nascosto (notare che si ridimensiona tirando un angolo situato *all’interno* della cornice di lavoro, invece che all’esterno. Vabbè).

Nel codice c’è la spiegazione: l’astuto programmatore ha sì impostato Font Awesome, ma a fondo pagina carica, cioè caricherebbe se potesse, icone che stanno sul suo disco di lavoro. Nel trasferire il sito dalla macchina di sviluppo a quella di produzione, ovviamente, i link al suo disco non funzionano più. L’utente finale si chiede perché siano sparite le icone ed ecco partire una chiamata di supporto da soddisfare, chiaramente a pagamento. *Business is business*.

Preferisco ricostruire la faccenda e allestire il codice semplicissimo che mette le icone al loro posto, usando Font Awesome. Lo scrivo in locale, lo copio per incollarlo dentro il blocco *Raw Code* al posto di quello inutile lasciato dal programmatore.

Seleziono il pezzo di codice da eliminare e faccio Incolla. Non solo Mac, ma da anni e anni qualsiasi computer dispone di un meccanismo di copia e incolla. Qui *viene incollata una riga vuota*.

Provo e riprovo. Finalmente arrivo a comprendere il genio degli sviluppatori del plugin. *Posso incollare se non cancello nulla*; solo dopo avere incollato, posso cancellare quello che non volevo.

Ora qualcuno salterà su a dire che si tratta di una misura per la tutela del codice contro operazioni sventate. A me sembra la negazione di qualsiasi principio di interfaccia utente nato dal 1984 a oggi.

I programmatore e i suoi trucchetti per giustificare un contratto di manutenzione. Lo sviluppatore del plugin, del quale ogni giorno scopro nuove perle, giù negli abissi in cui si trova l’esperienza utente dopo un naufragio catastrofico. Capisco che con i plugin uno si guadagna da vivere, capisco che l’altro cerca di massimizzare il ritorno in un mondo effettivamente crudele e restio a riconoscere il giusto prezzo per il lavoro. Capisco anche le persone che vogliono pubblicare in fretta e senza problemi e si aprono un WordPress in dieci minuti.

Se però lavoriamo a livello di siti professionali, si colgono tracce serie e importanti di disagio mentale. L’utente come pollo da spennare è una immagine brutta, ma può capitare. L’utente come vittima del sadismo di un pazzo incapace di comprendere l’esistenza del servizio all’utente, dell’accessibilità, della facilità di utilizzo, della documentazione, dell’interfaccia umana, non va bene. WordPress è un ambiente che fa prosperare questi pazzi sadici e come tale ha delle responsabilità, gravi.