---
title: "Sopra le parti"
date: 2022-01-23T01:29:14+01:00
draft: false
toc: false
comments: true
categories:
tags:
---
Da socio dei [Copernicani](https://www.copernicani.it), mi permetto un post in cui si fa politica. Mai ne scriverei uno, tuttavia, che sia partitico. I Copernicani sono meravigliosamente apartitici, concreti e consapevoli delle opportunità della tecnologia. In tre giorni capitano due opportunità interessanti e per una volta le segnalo.

Tutto oggi si può [votare simbolicamente per scegliere il Presidente della Repubblica](https://scegli.copernicani.it/login) con un sistema di votazione diverso da quelli usuali e usato dai Copernicani stessi per le nomine delle cariche interne all’associazione. Si parla di una figura sopra le parti, la rosa dei candidati è molto ampia, alla fine si finisce per produrre un sondaggio che poi sarà curioso confrontare con l’elezione effettiva per vedere se e quanto differisce nell’esito.

(Disclaimer: se legge qualche Grande Elettore, sappia che annuncerò ovviamente la mia candidatura ma il nome non compare nell’elenco della consultazione Copernicana, perché non ho ancora effettuato l’annuncio).

Martedì sera [si parlerà in streaming del bilancio dello Stato](https://www.youtube.com/watch?v=bi0nQYzeEfI), attraverso [uno strumento sviluppato proprio per iniziativa Copernicana](https://budget.g0v.it/partition/overview), unico a fare qualche chiarezza in un bilancio elefantiaco, frammentato, fatto per risultare opaco e per non essere conosciuto dai cittadini.

In Italia, sono cose assolutamente non banali e che alla lunga potranno effettivamente incidere in qualche misura sul funzionamento della macchina statale, si spera in meglio.

Partecipo attivamente ai Copernicani molto meno di quanto vorrei ma mi sento di suggerire la valutazione di una iscrizione a chiunque sia un po’ stufo di come vanno le cose, conosca il valore che può avere la tecnologia anche in un organismo ottocentesco come quello che manda avanti l’Italia e voglia vedere qualche risultato concreto prima di vedere bisnipoti.

_Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile._
