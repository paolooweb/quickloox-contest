---
title: "Glorbo Forever"
date: 2023-07-23T03:36:56+01:00
draft: false
toc: false
comments: true
categories: [Internet, Software]
tags: [Glorbo, World of Warcraft, Reddit, Perlini, Macity, Settimio Perlini]
---
Normalità: un sito di informazioni su [World of Warcraft](https://worldofwarcraft.blizzard.com/en-gb/) pubblica la notizia del debutto di un nuovo personaggio, [Glorbo](https://arstechnica.com/gaming/2023/07/redditors-prank-ai-powered-news-mill-with-glorbo-in-world-of-warcraft/).

La nuova normalità: il sito di informazioni è una creatura automatica, generata da una delle odierne *intelligenze artificiali*, come amano definirle i loro creatori. Il software fruga in giro per la rete e pubblica mitragliate di news in quantità e a costi completamente inarrivabili per un umano.

Il prezzo del progresso, si dirà. Ogni tecnologia sacrifica posti di lavoro, però ne crea altri e tutto il solito bla bla bla.

Non era questo il punto è non lo è, continua a non esserlo. Il punto è la natura di Glorbo, protagonista di una *fake news*. Il personaggio non esiste, World of Warcraft non lo prevede. Glorbo è una invenzione.

E allora? Siamo già afflitti da tonnellate di fake news, che sarà una in più? Che cos’ha di speciale?

La sua creazione da parte di un team di buontemponi su Reddit, allo scopo di prendere all’amo il sito creato artificialmente. Il quale guadagna pubblicando cose non importa di che qualità e con che senso, purché generino visite al sito e quindi erogazione di banner.

Al sito artificiale interessa meno di zero il *fact checking*, La verifica della notizia. Che Glorbo sia vero o falso è neanche un battito di ciglia, è una non-questione: si trova, si pubblica.

Anni fa qualcuno ricorderà che parlavo spesso dei siti di *rumor* sul mondo Apple e della loro disastrosa propensione a pubblicare qualsiasi cosa pur di fare ascolti. Affrontai diverse polemiche compresa quella con Settimio Perlini, nume di Macity, che in sostanza diceva ma che c’è di male, fanno così tutti, è così che funziona il gioco.

Il gioco continua a funzionare così e Settimio continua a restare mio amico per quanto mi riguarda, seppure immagino che lui la pensi diversamente. La posta però si è alzata di parecchio e il gioco è  divenuto più spietato.

Ora siamo a macchinari logici capaci di vomitare in rete centinaia di articoli di contenuto plausibile a un lettore distratto e sprovveduto, tutti i giorni, tutto il giorno, senza supervisione, anzi.

Questi articoli vengono incamerati dalle intelligenze artificiali di cui sopra e usati per ulteriore addestramento, qualsiasi cosa contengano. Se anche c’è qualche controllo, la ragione dei costi e della fretta prima o poi li neutralizzerà.

Su un sito potrà esserci scritto qualunque cosa, basta che generi traffico, e diverrà la nuova verità. Glorbo diventerà un personaggio credibile ed effettivo per migliaia, centinaia di migliaia di persone. I bot provvederanno ad aumentare artificiosamente il traffico ove non fosse abbastanza.

Macity non c’entra niente con i siti creati artificialmente, va da sé. La logica sottostante, invece, è tremendamente simile: che si possa pubblicare e fare pubblicare senza alcuna responsabilità. Che l’esistenza di Glorbo sia una discussione fine a sé stessa. Che la post-verità sia innocua.

Che lo facciano tutti, che non sia niente di male, che sia cosa da poco, sono tutte premesse false. I danni che questo atteggiamento, applicato ai nuovi strumenti, è in grado di infliggere alla comunità sono incalcolabili.

Ripuliamo la nostra timeline e i nostri contatti dai siti e dalle persone pericolose, anche se ci costa qualcosa. Ogni ripubblicazione di entità tossiche danneggia l’ambiente, qui inteso come la civiltà. Viva Glorbo che ce lo mostra con chiarezza.