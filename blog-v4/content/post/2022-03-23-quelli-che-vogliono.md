---
title: "Quelli che vogliono"
date: 2022-03-23T01:15:42+01:00
draft: false
toc: false
comments: true
categories: [Hardware]
tags: [Misterakko, Luca Accomazzi, Mac Studio, Quora, Luke Miani]
---
C’è così poco materiale italiano decente su Apple che non sia copiato, tradotto, ripetuto. Eccezione che conferma la regola: Misterakko che su Quora [spiega in poche righe](https://apple.quora.com/Una-bugia-può-fare-mezzo-giro-del-mondo-nel-tempo-che-alla-verità-serve-per-infilarsi-le-scarpe-Mark-Twain-Nei-gior), con tutti i link del caso, la vicenda che ha scatenato l’ultima polemica sul supposto *right to repair*.

Lo YouTuber che compra due Mac Studio per smontare il disco da uno di essi e metterlo sull’altro, per avere un Mac Studio con due unità Ssd (oppure, più probabilmente, per guadagnare una manciata di visualizzazione con un contenuto di poco valore).

Lo YouTuber non c’è riuscito, perché non ha letto le istruzioni. E se l’è presa con Apple, perché fa le macchine chiuse. Se avesse letto le istruzioni, sarebbe stata una macchina aperta, pensa.

Ho pensato alla tribù dei *l’ho comprato e ci faccio quello che voglio* e a questo suo indigeno, anzi no; lui è della tribù *l’ho comprato e ci faccio quello che so, purtroppo non basta*.

Scommetto che, se si procedesse all’esame del Dna, le due tribù scoprirebbero di essere gemelle.