---
title: "Una ammissione, una bugia"
date: 2017-07-28
comments: true
tags: [ Adobe, Flash, Shockwave, Macromedia]
---
Scrive Adobe [sul blog aziendale](https://blogs.adobe.com/conversations/2017/07/adobe-flash-update.html):

>Con il maturare negli ultimi anni di standard aperti quali HTML5, WebGL e WebAssembly, molti di essi ora offrono molte delle funzioni e caratteristiche introdotte pionieristicamente dai plugin e sono diventati una alternativa praticabile per distribuire contenuti via web.<!--more-->

Questo per dire che si sono finalmente decisi ad annunciare la fine del supporto a Flash… per il 2020. Meglio tardi che mai, sicuramente, ma la faccenda sembra diventata una maratona di [The Walking Dead](http://comicbook.com/thewalkingdead/2017/07/31/the-walking-dead-maggie-glenn-baby-legacy-/).

Potrebbe andare peggio, dirà qualcuno, ed effettivamente. Ecco un altro passaggio del *post* di Adobe:

>Dove non esisteva un formato, ne abbiamo inventato uno, come nel caso di Flash e Shockwave.

Se avessero scritto [PostScript](http://www.adobe.com/products/postscript.html) nessuno potrebbe avere da ridire. Invece questa è una bugia patentata, perché Flash è stato inventato da [Macromedia](https://en.m.wikipedia.org/wiki/Macromedia) e tutta la creatività che ci ha messo Adobe è stata acquistare Macromedia, con relativi prodotti.

Ammettere qualcosa che tutti sanno da anni – Flash è un morto che cammina – è lentezza pachidermica e ce ne facciamo una ragione; attribuirsi l’invenzione di un prodotto che ė stato piattamente comprato è avere la faccia come il lato B.
