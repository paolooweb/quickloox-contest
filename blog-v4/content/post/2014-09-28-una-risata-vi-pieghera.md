---
title: "Una risata vi piegherà"
date: 2014-09-29
comments: true
tags: [iPhone, iPhone6, iPhone6Plus, SquareTrade, Gand, bendgate]
---
Di passaggio: Apple ha ritirato l’aggiornamento a iOS 8.01 a causa di [quarantamila iPhone](http://online.wsj.com/articles/apple-defends-against-complaints-of-bending-iphones-1411668618) che avevano perso la connettività. Quarantamila iPhone su… cento milioni? Sono pochissimi in percentuale, un numero apprezzabile in assoluto, tanto che c’è stata una reazione. Ci pensino al numero quarantamila, quelli per cui un problema *lo hanno in molti* perché un *forum* contiene qualche centinaio di messaggi.<!--more-->

Comunque volevo chiudere la questione sugli iPhone 6 che si piegano.

SquareTrade, gente che vende assicurazioni contro incidenti a iPhone e altri apparecchi, ha effettuato i suoi test e afferma che [iPhone 6 è l’apparecchio a minor rischio rottura complessivo](http://www.squaretrade.com/press/iphone-6-earns-best-score-ever-iphone-6-plus-not-far-behind). Secondo in classifica è iPhone 6 Plus, terzo iPhone 5 e poi finalmente si trova un coso Samsung.

Apple dichiara *nove* casi di iPhone 6 piegato e [ha aperto ai giornalisti uno dei laboratori](http://www.theverge.com/2014/9/25/6845611/inside-apples-iphone-6-torture-building) dove si effettuano i test di resistenza a tutti i traumi possibili, piegatura compresa, su trentamila unità.

Purtroppo serve a nulla affrontare questo argomento su base razionale.

L’argomento definitivo me lo ha [segnalato Gand](https://twitter.com/cgand/status/515817302915571712) ed è una vignetta realizzata in stile xkcd.

<blockquote class="twitter-tweet" lang="en"><p>RT <a href="https://twitter.com/FreeSMUG">@FreeSMUG</a>: <a href="https://twitter.com/hashtag/bendgate?src=hash">#bendgate</a> graph <a href="https://twitter.com/hashtag/xkcd?src=hash">#xkcd</a> style&#10;<a href="http://t.co/SJ2exjzSX1">http://t.co/SJ2exjzSX1</a></p>&mdash; Carlo Gandolfi (@cgand) <a href="https://twitter.com/cgand/status/515817302915571712">September 27, 2014</a></blockquote> <script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>

E dovremmo esserci s-piegati.