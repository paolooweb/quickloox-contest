---
title: "L’anno del computer"
date: 2022-01-11T01:34:40+01:00
draft: false
toc: false
comments: true
categories:
tags:
---
Abbiamo [la prima app per iOS creata e pubblicata su App Store interamente con iPad via Swift Playgrounds](https://www.cephalopod.studio/blog/lessons-from-developing-an-app-on-the-ipad-from-start-to-finish-on-the-app-store).

Quanti spiegavano piccati in punta di forchetta che iPad non è un vero computer perché non consente di programmare app per iPad, dal 2022 possono liberamente fare spallucce e commentare seccati che non sprecano tempo con queste minuzie, come invece amavano tantissimo fare quando ritenevano di avere ragione.

Dovrebbero piuttosto essere contenti: si ritrovano un computer al prezzo pagato per un tablet.

*Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile.*
