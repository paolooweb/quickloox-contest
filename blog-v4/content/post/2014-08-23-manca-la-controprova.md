---
title: "Manca la controprova"
date: 2014-08-23
comments: true
tags: [MacBookPro, Macworld]
---
Così ci sono MacBook Pro del 2011 con scheda grafica Amd che [accusano problemi se sottoposti a compiti intensi di tipo grafico](http://www.macworld.co.uk/news/mac/widespread-2011-macbook-pro-failures-continue-3497935/).<!--more-->

Magari c’è un problema comune a queste macchine e può darsi che venga istituito un programma di riparazione come è già accaduto più volte, per [dischi rigidi](http://www.macworld.co.uk/news/mac/apple-admits-seagate-hard-drive-faults-imacs-will-replace-3405057/), [alimentatori](http://www.macworld.co.uk/news/mac/apple-settles-magsafe-lawsuit-offers-replacements-3317000/), [schede logiche](http://www.macworld.co.uk/news/mac/ibook-replacement-scheme-extended-10473/), [dischi a stato solido](http://www.macworld.co.uk/news/mac/apple-recalls-macbook-air-flash-storage-drives-some-models-3474352/).

Mi rende perplesso la mancanza di qualsivoglia spiegazione non dico tecnica, almeno circostanziata. *Macworld UK* riassume efficacemente lo stato dell’informazione:

>Si ritiene che il problema sia dovuto a surriscaldamento.

D’accordo; ma che cosa distingue le macchine con il problema da quelle identiche che non lo hanno?

L’articolo è perfettamente documentato e rimanda a una [conversazione chilometrica](https://discussions.apple.com/thread/4766577?start=0&tstart=0) sui forum Apple, *con oltre un milione di visite*. Ovvero centomila partecipanti attivi, probabilmente meno ancora. Nel 2011 si sono venduti [dodici milioni di MacBook](http://www.macworld.com/article/1164973/apple_reports_record_revenue_profit_for_fiscal_first_quarter.html), almeno metà dei quali sono MacBook Pro a essere conservativi. Il dato delle visite impressiona a prima vista, ma appare fisiologico.

C’è una [petizione](http://www.macworld.co.uk/news/mac/petition-demands-apple-fix-macbook-pro-graphics-woes-3537300/) *con oltre diecimila firme*. Su Internet “firmare” una petizione significa più o meno mettere nome e cognome e fare clic. Se lo scopo è ottenere qualcosa gratis, beh, raccogliere firme è semplice e diecimila è un numero piccolino. Non c‘è un interesse planetario, diciamo.

Ricordo che a una azienda con decine di miliardi di dollari in cassa costa molto meno attivare un programma di sostituzione gratuita che subire un danno globale di reputazione sui forum. Se non viene fatto, non è certo per risparmiare.

Il problema potrebbe esserci davvero, intendiamoci. Però. A casa mia è in funzione un MacBook Pro del 2011 che ha sempre funzionato bene, fino all’installazione dell’aggiornamento 10.9.4. Dopo quello ha iniziato a dare problemi evidenti con la scheda video: sfarfallamenti, problemi di visualizzazione sempre meno circoscritti, fino ad avere una schermata inutilizzabile.

Sospettoso che c’entrasse l'aggiornamento, ho provato a scaricarlo nuovo di zecca e riapplicarlo. Soluzione azzeccata: il computer funziona meglio di prima.

Non sto dicendo che questa sia la soluzione. Mi chiedo quanti di quei diecimila o centomila abbiano cercato di identificare il problema e provato soluzioni più complesse di riavviare. Quanti abbiano cercato informazioni in Console. O confrontato due MacBook Pro 2011 alla ricerca di un fattore comune.

Manca la controprova.

Un buon sistema per investigare un eventuale problema diffuso sarebbe raccogliere informazioni diagnostiche. Su una popolazione di milioni di computer, i dati che si possono raggruppare sono davvero tanti. Lavorarci richiede forti conoscenze tecniche, ma è fattibile.

Conosco giusto un’azienda che fa al caso. Ci lavorano centinaia di ingegneri software di livello assoluto, possiede risorse paragonabili a quelle di una nazione industrializzata, è attentissima alla propria immagine e a quella dei prodotti.

I MacBook Pro del 2011 stanno *già* inviando informazioni diagnostiche a montagne. Se non viene avviato un programma di sostituzione a partire da questo, i casi sono due.

Il primo è che il problema esista, ma ancora sfugga a una diagnosi precisa e di conseguenza a una azione riparatrice che serva davvero a qualcosa.

Il secondo è che ci siano diecimila problemi, o mille, o cento, ma non uno.