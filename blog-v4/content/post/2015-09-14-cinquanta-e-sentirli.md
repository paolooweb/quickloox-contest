---
title: "Cinquanta e sentirli"
date: 2015-09-14
comments: true
tags: [Attivissimo, Venerandi, iPad, iOS, Nokia, Sony]
---
Non entrerò in profondità nel *post* di Paolo Attivissimo su iPad Pro e gli annunci recenti di Apple, [Apple presenta la sua nuova gamma di sonniferi](http://attivissimo.blogspot.ch/2015/09/apple-presenta-la-sua-nuova-gamma-di.html). Mi demoralizza. Vi si leggono cose come…<!--more-->

>temo che l’iPad Pro sia un nuovo passo in avanti nella strategia a lungo termine di Apple per eliminare definitivamente il computer

Complottismo? No, chiarisce. Ci sono le prove di *questa progressione anti-utente*: per esempio, *la batteria sigillata*. Perché adesso la sigillano anche Nokia e Sony. Sì, c’è scritto questo. Se me lo raccontassero a voce, non ci crederei.

Sono basito e non andrò oltre. Anche perché hanno commentato il suo *post* non solo i soliti *minion* che abitano quel blog ma anche persone normali, che hanno già ristabilito la verità delle cose.

Piuttosto, uno dei temi che ricorrono nella discussione è il confronto generazionale: i ragazzi di oggi non sono come quando eravamo ragazzi noi, che ovviamente siamo stati il meglio e da lì si è solo discesa una china:

>Dal loro punto di vista, per quale motivo questi giovani cresciuti a pane e iOS dovrebbero faticare per imparare a usare un PC o un Mac, con tutte le sue bizzarrie e infettabilità, specialmente adesso che c'è un iPad da 13 pollici con tastiera che fa le stesse cose di un PC/Mac, ma senza tutte le complicazioni di un PC/Mac?

Le piattaforme come iOS elimineranno i computer, senza i quali gli smanettoni diminuiranno di numero e piano piano sparirà la libertà.

L’Attivissimo che conosco io, o pensavo di conoscere, a questo punto guarderebbe i dati: quanti programmatori c’erano rispetto alla popolazione nel 1985, 1995, 2005, 2015. L’età media dei programmatori nel tempo. Quanti computer ci sono per persona, quante *app* sono usate da ogni persona eccetera eccetera. Ma questo Attivissimo, quando si parla di Apple, lascia il posto a qualche suo *alter ego*. Con accento su *ego*.

Fabrizio Venerandi ha risposto ad Attivissimo con [una cosa infinitamente più intelligente ed equilibrata](http://www.quintadicopertina.com/fabriziovenerandi/?p=335). Solo che anche lui paventa il *rischio di formazione di “post-nativi digitali”* (torme di ragazzi che usano *app* senza saper creare *app*).

Sembra di essere al bar. Cinquantenni che quando erano ragazzi loro, eh, che genio, eh, che generazione. Adesso invece c’è rischio che questi ragazzi nuovi siano tutti rincoglioniti dalle *app* e dai sistemi chiusi. Signora mia, che tempi, ai miei di tempi era tutta un’altra cosa.

Pari pari quello che dicevano di noi quei rincoglioniti dei nostri vecchi.

Facciamo invece qualcosa di diverso da tutti i cinquantenni della storia: riconosciamo che il mondo sta andando avanti e tra cinquant’anni, quando saremo spariti, nessuno ci rimpiangerà per come eravamo smanettoni del computer. I coetanei di mia figlia, classe 2014, saranno cinquantenni a lamentarsi delle nuove generazioni. Ai loro tempi, invece…