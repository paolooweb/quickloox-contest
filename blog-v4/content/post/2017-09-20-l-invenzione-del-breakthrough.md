---
title: "L’invenzione del breakthrough"
date: 2017-09-20
comments: true
tags: [Godin, Mario, iPhone]
---
In inglese *breakthrough* è un progresso improvviso e clamoroso, una rottura degli schemi, andare oltre l’ordinario con qualcosa di imprevedibile e sorprendente.<!--more-->

Lo spiego perché **Mario** (grazie) ha evidenziato un [articolo di Seth Godin](http://sethgodin.typepad.com/seths_blog/2017/09/impossible-unlikely-or-difficult.html), storico guru del marketing e del marketing per il web, dove si afferma:

>Apple ha lanciato un telefono da un migliaio di dollari. Ingegneri e progettisti avevano tempo illimitato (dieci anni dal primo esemplare), risorse illimitate, forza di mercato illimitata. È possibile che nelle nostre intere vite non ci sia stata un’altra concentrazione di illimitato paragonabile. Eppure tutto quello che hanno saputo costruire è stata una macchina per produrre emoji animati. Un telefono leggermente migliore.

È abbastanza facile vedere che si pongono le premesse del confronto tra questo iPhone e quello di dieci anni fa ma poi si giudica questo iPhone con quello di un anno fa. Furbetto (ma non sarebbe Godin).

L’intero schermo di un iPhone 2007 [occupa circa lo spazio di due icone](https://twitter.com/drbarnard/status/908352305552117765) sullo schermo di un iPhone X. Sui dieci anni, si è fatta un po’ di strada. 

<blockquote class="twitter-tweet" data-lang="en"><p lang="en" dir="ltr">The entire home screen of the original iPhone (320x480 pixels) is about the size of 2 icons on the iPhone X home screen (1125x2436 pixels). <a href="https://t.co/QJNeEJrBMM">pic.twitter.com/QJNeEJrBMM</a></p>&mdash; David Barnard (@drbarnard) <a href="https://twitter.com/drbarnard/status/908352305552117765">September 14, 2017</a></blockquote> <script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>

C’è anche chi la mette in un altro modo e si riferisce a iPhone parlando di [dieci anni di innovazione sostenuta](https://twitter.com/joshuajames/status/908439832136785920).

<blockquote class="twitter-tweet" data-lang="en"><p lang="en" dir="ltr">This is what a decade’s worth of *sustaining innovation* looks like: <a href="https://t.co/uIONjS4ePK">https://t.co/uIONjS4ePK</a></p>&mdash; Joshua J. Arnold (@joshuajames) <a href="https://twitter.com/joshuajames/status/908439832136785920">September 14, 2017</a></blockquote> <script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>

Il punto è un altro, comunque. Il fulcro dell’articolo di Godin è questo:

>Non è che un altro breakthrough sia impossibile.

Volutamente lo lascio in originale. Per lui, oggi, iPhone 2007 è stato un *breakthrough*.

Ora vediamo [che cosa scriveva nel 2007](http://sethgodin.typepad.com/seths_blog/2007/01/inventing_a_new.html):

>Steve Jobs ha ricevuto un sacco di attenzioni dai media per la sua recente reinvenzione del cellulare. Il fatto è che iPhone non reinventa realmente il cellulare. Per lo più mescola un cellulare assieme ad alcuni altri apparecchi (il che non esclude che io ne voglia uno).

L’elenco, Godin 2007, di quello che dovrebbe fare un *cellulare reinventato*, è il seguente:

* *Lasciarmi depositare un messaggio in segreteria a un gruppo di persone.*
* *Iniziare una audioconferenza con un gruppo di persone a partire da un singolo contatto.*
* *Lasciarmi chiamare gli amici in funzione di dove si trovano in un dato momento.*
* *Iniziare chiamate con estranei secondo la loro rete di relazioni (stile Facebook) o secondo il loro stato, o vicinanza fisica. Se ci sono amici di amici in aeroporto mentre aspetto, lasciatemi vederli! Parlargli?*
* *Mettere un sito di appuntamenti dentro un telefono. Immagini e stato e posizione e bum, puoi parlare.*
* *Permettere a chi fa marketing di pagare per interagire con i consumatori disposti a permetterlo, secondo le loro esigenze, la loro posizione o la loro pura noia.*
* *Lasciarmi mettere in un elenco le persone che vogliono parlare con me e amministrare l’elenco in un modo che funzioni per tutti.*

Per Godin iPhone del 2007 non è affatto un *breakthrough*. Non è neanche una reinvenzione. Manca di un sacco di cose (e che povertà e ristrettezza di visione in questo elenchino, come se iPhone fosse solamente un cellulare. *Mettere un sito dentro un telefono*, proprio gli è sfuggito il senso).

iPhone per lui *è diventato* un *breakthrough*. Quando ne aveva bisogno per poterlo stroncare. Lo è diventato grazie all’innovazione che il suo articolo è costruito per negare.