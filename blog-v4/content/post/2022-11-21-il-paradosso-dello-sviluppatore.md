---
title: "Il paradosso dello sviluppatore"
date: 2022-11-21T00:35:00+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [FileMaker, FileMaker Guru, Villani, Giulio Villani, FMGuru]
---
Che errore sarebbe stato, andare alla [FileMaker Week di Roma](https://macintelligence.org/posts/2022-09-21-la-database-del-computing/) pensando che ci fosse in gioco solo FIleMaker. Gli sviluppatori acuti hanno colto l’occasione e sono riusciti a riunirsi per fare il punto su come sta cambiando, fortemente, il loro mestiere. Passando magari da FileMaker, perché no? Ma certe riflessioni e alcuni dati di realtà valgono per tutti.

Giulio Villani, anima di FMGuru e organizzatore della Week, è stato così gentile da condividere alcune riflessioni con me e sono lieto di ribaltarle su queste pagine. Più di una, riflessione e pagina, dato che le cose su cui soffermarsi sono numerose e pregnanti.

Qui stiamo sul paradosso dello sviluppatore, così sintetizzato da Giulio in una delle sue slide di apertura:

![Se sono più bravo e veloce di dieci anni fa, se la piattaforma mi fornisce strumenti più veloci e un'interfaccia migliore… per quale motivo i tempi di sviluppo si sono allungati?](/images/paradosso-sviluppatore.png "L’evoluzione del mestiere dello sviluppatore è tutt’altro che lineare, su FileMaker come altrove.")

Dice Giulio a contorno:

>Si parla tanto dell’esperienza utente, che è data per assodata, ma molto meno dell’esperienza dello sviluppatore nell’ambito del suo lavoro di creazione (ovvero del lato umano della meccanica di una qualunque soluzione) e di come l’aumento vertiginoso di competenze necessarie a chiudere un lavoro impatti pesantemente sull’approccio; soprattutto in Italia, dove molti sviluppatori sono one-man-band.

Prima conclusione: limitarsi a seguire la propria piattaforma non serve, o meglio non basta. E forse un oggetto come FileMaker è anche più schermato dai certi capovolgimenti di fronte che si vedono in altre arene, dove – era una battuta ma il fondo di verità era consistente – ogni settimana salta fuori un framework nuovo.

Come fa allora lo sviluppatore a cavarsela? Lo vediamo nel corso di qualche post. Giulio è stato generoso e lo ringrazio per la confidenza.