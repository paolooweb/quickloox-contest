---
title: "Un po’ dati un po’ no"
date: 2023-11-04T17:43:56+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [Finder, tag, Eclectic Light Company]
---
Chiedo scusa se ultimamente frequento spesso casa *Eclectic Light Company*, solo che attraverso loro mi sto riappassionando alle trattazioni degli interni software di Mac.

Lo fanno bene, è veloce, è approfondito, è interessante, si arriva sempre a una qualche app utile. Qui ce n’è una per [lavorare con i tag del Finder e sapere quello che accade sottocoperta](https://eclecticlight.co/2023/11/04/are-finder-tags-useful-metadata/).

C’è da imparare. Colgo l’occasione.