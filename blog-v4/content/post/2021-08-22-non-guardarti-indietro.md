---
title: "Non guardarti indietro"
date: 2021-08-22T01:29:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [Samsung, Intel] 
---
Ho letto che – per la seconda volta in tempi recenti – [Samsung è diventata la maggiore produttrice mondiale di semiconduttori](https://www.tomshardware.com/news/samsung-takes-intels-throne-as-top-semiconductor-manufacturer), davanti a Intel che storicamente ha sempre occupato questo ruolo.

Ho pensato a gente che leggo ogni tanto, nei contesti più vari, sempre nella stessa situazione: gli è cambiato qualcosa nella configurazione di lavoro, dalla ditta al software allo scanner, e chiede come continuare a fare una certa cosa che faceva prima nel modo in cui la faceva prima.

Con tutti i distinguo del caso, la domanda giusta da porsi è sempre un’altra: _che cosa posso imparare di nuovo da questa esperienza?_

Nel secolo scorso Intel faceva parte dello standard di mercato, avrebbe dominato l’universo, non era in discussione.

È cambiato persino il ruolo di Intel. Se usiamo computer siamo discepoli, adepti, coltivatori, vittime, suocere, quello che si vuole, del cambiamento. Il nostro destino è il cambiamento, il nostro interesse – a volte sfuocato – sta nel cambiamento, la nostra attitudine è il cambiamento.

L’essere umano insegue e sogna la stabilità. Se è davanti a un computer, tuttavia, ha da pensare in termini di cambiamento. Altrimenti, tanto vale che  vada a fare _jogging_ correndo all’indietro.

*Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile.*