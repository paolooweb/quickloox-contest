---
title: "Algebra e disuguaglianze"
date: 2024-02-18T00:39:31+01:00
draft: false
toc: false
comments: true
categories: [Internet, Education]
tags: [San Francisco, algebra]
---
Sintesi estrema per chi ha fretta: dieci anni dopo avere smesso, [le scuole medie di San Francisco iniziano a reinserire nelle terze l’insegnamento di base dell’algebra](https://www.axios.com/local/san-francisco/2024/02/15/sf-schools-algebra-8th-grade).

*Perché avevano smesso?*

Per agevolare studenti svantaggiati, poveri, di minoranze etniche, non madrelingua o comunque mal considerati, che facevano più fatica di altri a ottenere risultati di alto livello. La presenza di Algebra I nel curriculum rendeva le diseguaglianze tra privilegiati e svantaggiati ancora più ampie.

*Che cosa è successo?*

Uno studio datato 2023 dell’università di Stanford ha riscontrato che, dieci anni dopo avere tolto l’insegnamento dell’algebra dalla terza media, le disuguaglianze nell’accesso alla parte alta del curriculum di matematica [sono rimaste tali e quali](https://edworkingpapers.com/ai23-734). Avere tolto l’algebra non ha avvantaggiato gli svantaggiati.

*La ripresa sarà indolore?*

No. Si comincia da un certo numero di scuole (che offriranno il corso anche online per le scuole non ancora rientrate nel programma), perché da dieci anni le scuole non hanno più assunto docenti che necessariamente sapessero anche insegnare algebra. Ora, dove non ci sono per colpo di sfortuna, bisogna trovarli; le scuole hanno tre anni di tempo per reintrodurre l’insegnamento. Tre anni nei quali migliaia di ragazzi che potrebbero imparare l’algebra di base in terza media non ne avranno la possibilità. Si sono persi dieci anni e recuperare non sarà immediato.

*La morale di tutto questo?*

Tagliare il livello di l’insegnamento in nome degli alunni con problemi non aiuta gli alunni con problemi né quelli senza problemi; va a danno di tutti e impoverisce il livello di qualità degli insegnanti.

Quando viene proposto di rinunciare a una parte del programma per aiutare chi è rimasto indietro, viene proposto di penalizzare la classe. Il modo per aiutare chi è rimasto indietro è aiutarlo ad accelerare, non azzoppare tutti.

Se almeno un professore legge e comprende queste note, magari il post sarà servito a qualcosa. (Seguendo il primo link si arriva a tutte le fonti necessarie a sostanziare).