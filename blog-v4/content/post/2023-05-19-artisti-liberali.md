---
title: "Artisti liberali"
date: 2023-05-19T02:08:26+01:00
draft: false
toc: false
comments: true
categories: [Internet]
tags: [Podcast, Matteo Scandolin, Scandolin, Jobs, Steve Jobs, A2, Filippo Strozzi, Roberto Marin, Marin, Strozzi]
---
Oggi giornata con trofeo di caccia: sono tornato a casa forte di una copia autografata di [Podcast](https://www.apogeonline.com/libri/podcast-matteo-scandolin/) e di una piacevolissima chiacchierata con [Matteo Scan-do-lin](https://scandol.in), tradotto: *l’uomo che conosce i giusti ristoranti giapponesi*.

Abbiamo naturalmente parlato di podcast, perché lui è un maestro (nel vero senso della parola, dato che fa formazione sui podcast, oltre che fare podcast) e dialogare con lui à portare a casa conoscenza preziosa, soprattutto per uno che non nasce podcaster ma si lascia volentieri irretire da tipacci come gli artefici di [A2](https://macintelligence.org/posts/2023-03-08-detto-tutto/).

Durante la pandemia mi sono reso conto di quanto il video fosse un componente fondamentale dell’interazione con le persone; ma c’è voluta la comunità del podcast per portarmi a capire come la voce sia *il* componente *fondamentale*. Se ti sentono male, o peggio se ti sentono con un audio trascurato, neanche il miglior sfondo virtuale può salvarti. Se l’abito fa il monaco, cosa che non ho mai creduto, la voce fa l’anima e accidenti se è vero.

C’è anche un’altra cosa. Steve Jobs ha lasciate dette tante cose, ma quella dell’[intersezione tra tecnologia e arti liberali](https://macintelligence.org/posts/2016-11-09-umani-e-algoritmi/), pur avendo fatto meno rumore di altre, dovrebbe comparire nei libri di storia.

Le arti liberali erano tali perché chi le padroneggiava poteva mantenersi libero e una di queste era la retorica, inteso come nome di disciplina e non come discorso noioso e pomposo.

I podcaster sono i retori del nostro tempo, quelli veri a dispetto dei miliardi di interazioni dei video su TikTok. Gente che si mantiene libera, che ci mantiene liberi.

Da Matteo, Filippo, Roberto e tutti gli altri imparo ogni giorno un’altra necessità che Jobs aveva intuito: arrivare a dare alla gente quello che la gente non sa di volere. Non è colpa della gente né merito di chicchessia, siamo semplicemente fatti così come specie. Non sappiamo realmente quello che vogliamo e a volte abbiamo bisogno di qualcuno che ce lo dica, con voce pulita, passione e *sense of humor*.

Domani, anzi, oggi è già possibile fare parlare una voce artificiale come se fosse la nostra. A brevissimo qualcuno compierà la tragica crasi e registrerà podcast con testo creato da un sistema generativo e la propria voce riprodotta dal software specializzato.

Saranno cose di una noia mortale che agevoleranno il prossimo inverno dell’intelligenza artificiale.

I podcaster. Gli ultimi che rimarranno indelebilmente, irresistibilmente, fantasticamente umani. Artisti liberali.

E adesso tutti a prendersi un microfono come si deve, ché gli altri per sentire male, alzare lo stress e provare fastidio, hanno già le chiamate WhatsApp.