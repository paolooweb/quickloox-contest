---
title: "Letture in cascina"
date: 2014-12-22
comments: true
tags: [ArsTechnica, iOS, Android, iPhone6, MotoX, Windows]
---
Non sono nuovo a [raccomandare letture da Ars Technica](https://macintelligence.org/posts/2014-11-29-tre-definitivi/) e mi permetto di insistere, perché è appena uscita la [guida definitiva allo stato degli smartphone nel 2014](http://arstechnica.com/gadgets/2014/12/the-state-of-smartphones-in-2014-ars-technicas-ultimate-guide/).<!--more-->

Anche furbetta, perché evitano di compiere scelte controverse e tengono ciascuno nel proprio recinto: se ti piace iOS compra un iPhone 6, se ti piace Android prendi un Moto X, se ti piace Windows Phone… aspetta che esca una nuova ammiraglia.

Gli piace vincere facile. È giusto però che la volontà di evitare conflitti non oscuri l’analisi piuttosto dettagliata e ragionevolmente leggibile dei vari sistemi operativi e modelli in gioco.

Anche questa da mettere nella cascina virtuale delle cose da leggere durante le pause festive tra i giochi *online* e le orge alimentari con i parenti.