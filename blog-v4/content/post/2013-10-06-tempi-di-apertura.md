---
title: "Tempi di apertura"
date: 2013-10-06
comments: true
tags: [Xcode]
---
Su Mac OS X Hints ricordano come sia possibile [abilitare TextEdit alla modalità schermo pieno](http://hints.macworld.com/article.php?story=20130929062141742).<!--more-->

Il codice di TextEdit è [liberamente disponibile](https://developer.apple.com/library/mac/samplecode/TextEdit/Introduction/Intro.html), così come [Xcode](https://itunes.apple.com/it/app/xcode/id497799835?l=en&mt=12). È “solo” questione di dargli tempo, leggere, scoprire, provare e fare un passo avanti.

Cose da considerare maggiormente nell’agenda. Programmare è un mestiere vero e non si improvvisa. Al tempo stesso smanettare è molto più alla portata di quanto sembri. Non fosse altro che per la mentalità e la familiarità che si acquisiscono, può tornare assai utile.