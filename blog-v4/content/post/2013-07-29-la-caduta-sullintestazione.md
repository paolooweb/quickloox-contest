---
title: "La caduta sull'intestazione"
date: 2013-07-29
comments: true
tags: [Codici]
---
Aveva suscitato polemiche, la mia [presa di posizione](https://macintelligence.org/posts/2013-02-21-per-un-voto-utile/) sui codici troppo lunghi per essere utili, dall’Iban in giù.<!--more-->

Oggi mi sono trovato a risolvere il problema di un bonifico che non va a buon fine perché l’Iban è ultracorretto, ma *l’intestazione del conto è sbagliata*.

Ci sono due alternative possibili.

Ventisette caratteri di Iban non bastano ancora a identificare una banca, al punto che occorrono anche quelli dell’intestazione.

Oppure è stato escogitato un codice che non serve a niente, se basta un errore veniale altrove a vanificarne la funzione.

Tanto valeva dotarsi di un codice numerico a dodici cifre, bastante per mille miliardi di utilizzi.