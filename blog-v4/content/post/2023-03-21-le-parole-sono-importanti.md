---
title: "Le parole sono importanti"
date: 2023-03-21T02:03:03+01:00
draft: false
toc: false
comments: true
categories: [Hardware]
tags: [Q2U, Samson]
---
Dopo qualche giorno dalla prima [installazione del nuovo microfono](https://macintelligence.org/posts/2023-03-15-per-farti-sentire-meglio/) per videoconferenze e podcast,  non voglio più tornare indietro e mi chiedo come abbia tollerato finora l’uso degli auricolari con microfono. Forse perché la mia voce se la beccavano gli altri.

Non che gli auricolari con microfono in sé siano cattivi. Sono un ottimo mezzo per comunicare in tutte le situazioni. Certo non sono il mezzo per comunicare in modo ottimo.

Quando c’è la possibilità di stare alla scrivania, Q2U lavora bene. La voce arriva più nitida e soprattutto non si perdono pezzetti di parlato per strada. Una analogia per dare un’idea del miglioramento, anche se imperfetta: una chiamata FaceTime Audio è normalmente di pulizia e qualità complessiva superiore a una chiamata cellulare.

Un elemento per me importantissimo è che non provo alcuna ansia di dover per forza stare vicino al microfono per farmi sentire bene, una cosa che temevo. Invece sono più rilassato e tranquillo, alla distanza giusta, nella posizione giusta.

È un’altra dimensione del parlare a distanza, anche più umana perché più naturale e meno intrusiva. Sto quasi pensando anche a comprare un buon microfono da portare in giro assieme a iPad, perché davvero la voce rappresenta la portante della comunicazione; il video è un componente psicologico, che quasi sempre potrebbe anche non esserci (in alcune situazioni specifiche è essenziale per portare messaggi non verbali. Alcune).

Le parole sono importanti e, innamorato di quelle scritte, mi sono regalato un bel trattamento per quelle orali. Lo raccomando a chiunque abbia occasione di infliggere la propria voce agli altri. Farlo con pulizia e nitidezza è un atto di rispetto e professionalità.