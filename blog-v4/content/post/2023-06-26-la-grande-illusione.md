---
title: "La grande illusione"
date: 2023-06-26T12:30:13+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [50 Years of Text Games, Winograd, Terry Winograd, Shrdlu, Eliza]
---
Ho fatto bene a portarmi a casa una copia di [50 Years of Text Games](https://macintelligence.org/posts/2023-06-20-un-testo-per-lestate/) perché ha creato un collegamento inaspettato con la storia dell’intelligenza artificiale e della simulazione di interazione con l’umano. E mi porta una delusione che, da una parte, va presa con filosofia; dall’altra mi spinge ad approfondire, dato che Douglas Hofstadter dedica commenti a [Shrdlu](https://hci.stanford.edu/~winograd/shrdlu/) e voglio capire quanto ha avuto effettivamente occasione di vedere il programma o quanto ne abbia sentito parlare.

Shrdlu è stato scritto dall’allora studente di PhD Terry Winograd, scopro da *50 Years of Text*, sull’onda dell’impressione suscitata da [Eliza](https://macintelligence.org/posts/2023-04-19-così-non-impari/), la finta psicologa virtuale.

>Eliza mostrò come il sogno di conversare con un computer poteva diventare una illusione incredibilmente interessante e che il trucco richiedeva un programma relativamente semplice. […] Winograd voleva tentare di mettere un po’ di arrosto dietro il fumo.

Winograd aveva il sogno di scrivere, diremmo oggi, un chatbot capace di conversare di qualcosa che realmente capiva e che aveva una conoscenza delle strutture dell’inglese.

Riporto un dialogo con Shrdlu in originale, perché mi fa più impressione.

    pick up a big red block.  
    OK.

    grasp the pyramid.  
    I DON'T UNDERSTAND WHICH PYRAMID YOU MEAN.

    find a block which is taller than the one you are holding and put it into the box.  
    BY "IT", I ASSUME YOU MEAN THE BLOCK WHICH IS TALLER THAN THE ONE I AM HOLDING.  
    OK.

    What does the box contain?  
    THE BLUE PYRAMID AND THE BLUE BLOCK.

    What is the pyramid supported by?  
    THE BOX.

    How many blocks are not in the box?  
    FOUR OF THEM.

    Is at least one of them narrower than the one which i told you to pick up?  
    YES, THE RED CUBE.
    
Per circoscrivere l’ambito del problema, Winograd creò un ambiente chiuso, fatto di solidi colorati poggiati su un piano e scrisse Shrdlu come strumento di interazione con l’ambito suddetto.

Sembra poca cosa e invece è sufficiente per creare una sfida linguistica immensa.

Secondo *50 Years of Games*, Winograd scrisse Shrdlu in Lisp (confermato), arrivò a scrivere quasi trentamila righe di codice (da verificare) e non terminò il programma (assai probabile). La delusione è che la maggior parte dei dialoghi pubblicati e visibili su Internet sarebbero demo attentamente preparate, perché il programma faticava tantissimo a mantenere una vera conversazione libera con l’operatore e spesso non ci riusciva. Il codice sorgente di Shrdlu è disponibile ma sembra proprio difficile farlo funzionare senza grosso impegno su macchine moderne.

La parte interessante, e attuale, del confronto tra Eliza e Shrdlu è quanto sia facile creare l’illusione di una conversazione e quanto invece sia difficile dare coerenza e consistenza all’ambito di cui il programma si occupa. Affrontare questo problema, mezzo secolo di computing dopo, rappresenta ancora una sfida centrale e completamente irrisolta.