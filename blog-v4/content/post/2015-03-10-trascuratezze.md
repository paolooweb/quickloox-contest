---
title: "Trascuratezze"
date: 2015-03-11
comments: true
tags: [MacBook, MacBookPro, MacBookAir]
---
È stato annunciato un [MacBook completamente nuovo](http://www.apple.com/it/macbook/), che sfida i sensi per quanto è piccolo e leggero, con una serie di novità che in altri tempi avrebbero fatto parlare per settimane come notizia principale. Il *trackpad* ha compiuto un salto tecnologico; la tastiera pure; la batteria anche; la ventola… è sparita. Dentro un involucro che pesa meno di un chilo e supera di poco il centimetro nel punto di massimo spessore.<!--more-->

Una macchina quasi impalpabile, che ti fa toccare i bit direttamente con mano, senza la fastidiosa interposizione dell’*hardware* a ricordare che le nostre idee devono purtroppo sempre rapportarsi alle limitazioni della fisicità. Vien voglia di tenerla in mano solo a vederlo sul sito.

Alcune delle novità sono percolate nei nuovi MacBook Pro 13” Retina. Nel frattempo tutta la gamma ha ricevuto uno *speed bump*.

Una volta sarebbe stato il piatto forte. L’altroieri, un antipasto.

Mac cresce da *dieci anni* più della media del mercato. MacBook 12” è una meraviglia tecnologica come da tanto non si vedeva. E neanche sostituisce MacBook Air, come ho erroneamente pensato.

Dall’azienda che pensa solo a vendere iPhone.