---
title: "Trenta e non più trenta"
date: 2020-08-15
comments: true
tags: [Fortnite, Ferragosto, Apple, Google, App, Play, Store, Epic, Tencent, WeChat, Sony, Microsoft, Nintendo]
---
Ci sarà anche chi progetta di passare Ferragosto dentro Fortnite, nonostante la decisione di Apple e Google di rimuovere il gioco da App Store e Play Store, dopo che Epic Games ha creato il *casus belli* con l’introduzione nella app di un meccanismo per fare acquisti nel gioco senza riconoscere il trenta percento richiesto dagli Store in questione.

Come vuole oggi lo spirito del tempo, si sono già palesati sui social sessanta milioni di esperti di strategia industriale, ognuno con la sua idea sul conflitto in corso. (Io aderisco alla corrente della guerra commerciale per il mercato cinese; Epic è partecipata in modo importante da Tencent, che con WeChat ha un monopolio immenso in Cina sulle transazioni commerciali e vuole mettere bastoni tra le ruote a chiunque altro voglia fare concorrenza).

Propongo però un esame di ammissione. Ha diritto di esprimere la propria opinione chiunque abbia studiato il meccanismo di almeno un altro store di videogiochi, per esempio Microsoft, Sony o Nintendo, e sappia che percentuale viene trattenuta agli sviluppatori.

Come dispensa, è apparso un eccellente [articolo di Ars Technica](https://arstechnica.com/gaming/2020/08/as-epic-attacks-apple-and-google-it-ignores-the-same-problems-on-consoles/) che va raccomandato.

Buona festa e sempre grazie a chi trova tempo di qualità per queste pagine.