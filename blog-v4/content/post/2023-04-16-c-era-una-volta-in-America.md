---
title: "C’era una volta in America"
date: 2023-04-16T18:03:17+01:00
draft: false
toc: false
comments: true
categories: [Internet]
tags: [McCracken, Harry McCracken, Technologizer, Maximum Pc, MacLife]
---
*Maximum Pc* e *MacLife* (già nota come *MacAddict*) hanno distribuito negli Stati Uniti i numeri di aprile, in edizione cartacea oltre che online.

Sono gli *ultimi* numeri in edizione cartacea delle due riviste, che altrimenti continueranno a pubblicare in digitale.

Sono le *ultime* riviste di computer negli Stati Uniti ad avere (avere avuto, da domani) una edizione cartacea.

Harry MacCracken riassume su *Technologizer* [la fine delle riviste di computer in America](https://www.technologizer.com/2023/04/15/the-end-of-computer-magazines-in-america/) e la giusta dose di storia del settore, nonché di come siamo arrivati alla situazione attuale.

È una bella lettura. Ed è stata una grande avventura, che ha coinvolto tante persone negli ultimi quarant’anni. Non è poco.

Tempo di guardare avanti e celebrare il passato senza rimanerci aggrappati senza altro motivo che la nostalgia. I modi per comunicare il *personal computing* sono numerosi e le cose da dire sono moltissime. Basterebbe parlare di assistenti generativi spacciati per intelligenze artificiali, strapotere delle multinazionali nella scuola dove dovrebbe regnare l’open source, automazione e scripting nella vita quotidiana per sfuggire all’aurea mediocrità del *good enough*.

C’è molto da dire. Semplicemente, la carta non è più un mezzo plausibile.