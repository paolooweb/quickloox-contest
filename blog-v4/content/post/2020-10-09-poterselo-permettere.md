---
title: "Poterselo permettere"
date: 2020-10-09
comments: true
tags: [iPhone, Apple, iPad, Air, watch]
---
Il panorama cambierà drasticamente nei prossimi giorni, non c’è dubbio. Noto comunque che oggi la prima pagina del [sito Apple](https://www.apple.com) non contiene neanche un riferimento a iPhone, l’oggetto che produce i due terzi del fatturato, l’apparecchio per il quale Apple trascurerebbe Mac.

Non ci sono neanche Mac. watch ha molto spazio e poi iPad Air, appena uscito in edizione rinnovata. Fanno presenza anche servizi e contenuti.

Chi continua a usare i criteri degli anni novanta per giudicare Apple può solo prendere atto che il mondo è cambiato e bisogna cambiare anche i criteri.

Certo, non tutti possono permettersi la rinuncia a un pregiudizio.