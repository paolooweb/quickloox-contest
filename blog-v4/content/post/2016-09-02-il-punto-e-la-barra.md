---
title: "Il punto e la barra"
date: 2016-09-02
comments: true
tags: [cuoredimela, bash, shell, Terminale]
---
Preparo materiale per [Cuore di Mela](http://cuoredimela.accomazzi.it) e faccio giri di verifica su Internet per essere sicuro di scrivere cose vere e riproducibili in tema di Terminale.<!--more-->

Ovunque trovo consigli su come scrivere script di shell e tutti, naturalmente, fano un esempio banale di script per mostrare come funziona la cosa. L’ho fatto anch’io.

Tutti i siti che ho trovato finora (una manciata, ma quando sono più di due comincio a pensarci su) sostengono che il file di testo contenente lo script vada reso eseguibile con un comando <code>chmod</code> per poi poter essere, appunto, eseguito nel Terminale digitando il suo nome preceduto da <code>./</code>, punto-barra.

La realtà è che si usa <code>./</code>, <code>chmod</code> non serve assolutamente. Ho provato, riprovato, riverificato.

E questo mi fa pensare, perché è la dimostrazione del tutto involontaria di quanto sia vero che i libri servono ancora. Su Internet si trova tutto, ma la spiegazione corretta di questa piccolezza non è dappertutto; neanche è detto che uno la trovi. Poi il file dopo <code>chmod</code> si esegue ugualmente via <code>./</code>, eh. Però una istruzione inutile, moltiplicata per mille persone che leggono, sono mille istruzioni inutili.
