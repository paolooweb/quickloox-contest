---
title: "Uno vale l’altro"
date: 2014-10-22
comments: true
tags: [Wsj, iPhone, Apple, Samsung]
---
[Wall Street Journal del 28 gennaio 2013](http://online.wsj.com/news/articles/SB10001424127887323854904578264090074879024).<!--more-->

>Samsung sta riuscendo dove altre aziende di tecnologia hanno fallito: eliminare il divario di figaggine con Apple.

>L’azienda coreana ha usato una combinazione di abilità ingegneristica, potenza di produzione e sapienza di marketing per creare smartphone che possono rivaleggiare con iPhone in vendite e attrattiva.

[Wall Street Journal del 20 ottobre 2014](http://online.wsj.com/articles/apple-profit-jumps-13-on-boost-from-iphone-6-1413837044?mod=LS1).

>Trainata dalle vendite esplosive dei nuovi iPhone con schermo grande, Apple ha dichiarato profitti superiori del 13 percento e previsto vendite record per i prossimi tre mesi.

>Nel frattempo, l’approccio Samsung di offrire smartphone di ogni prezzo e dimensione in qualunque mercato si scontra contro un’ondata di produttori cinesi a basso costo.

Scorpione: annuncerete un nuovo smartphone che avrà grande successo. Qualche fastidio al duodeno. Colore della settimana, pallido.