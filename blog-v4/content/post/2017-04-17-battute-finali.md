---
title: "Battute finali"
date: 2017-04-17
comments: true
tags: [iPad, Nhl, Surface]
---
Durante i *playoff* che chiudono la stagione della National Hockey League americana, ogni panchina avrà a disposizione tre iPad per rivedere pressoché istantaneamente le azioni cruciali e decidere se richiedere agli arbitri un riesame della loro decisione.

Come sottolinea l’[articolo di Associated Press](https://apnews.com/0d760ab34e3740148ee0e1efe889b0d0/APNewsBreak:-NHL-adding-iPads-on-benches-for-playoffs), nelle fasi finali del campionato di hockey le segnature si riducono di numero e le azioni si giocano su un filo di rasoio ancora più tagliente; azioni che, nel decidere una eliminazione o una vittoria, pesano milimoni di dollari.

Si può scommettere che nessuno farà sconti al primo iPad che non dovesse funzionare non solo a dovere, ma anche tempestivamente. Quando lo hai riavviato, per dire, la partita è già avanti e se hai preso un gol che non c’era, te lo tieni.

In altri termini, ci accorgeremo subito se gli iPad per lo hockey su ghiaccio sono una barzelletta come i [Surface per il football](https://macintelligence.org/posts/2016-01-27-minuti-di-sospensione/), oppure se – come per tante situazioni – giocano in una lega superiore.
