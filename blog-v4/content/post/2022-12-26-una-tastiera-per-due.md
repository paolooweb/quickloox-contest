---
title: "Una tastiera per due"
date: 2022-12-26T17:14:08+01:00
draft: false
toc: false
comments: true
categories: [Hardware, Software]
tags: [Lenovo, ThinkPad, Microsoft, Office, Open Document Format]
---
Prima di parlare delle soddisfazioni del Natale devo raccontare del contrappasso: mi è toccato il setup di un ThinkPad Lenovo.

Il modello non ha importanza: fascia mediobassa, buon prezzo, feeling decente parlando di plastica, trackpad buttato tutto a sinistra, schermo economico e tutto il solito armamentario. Però tastiera decente e velocità pure: se me lo regalassero per giocarci con FreeBSD, lo terrei,

Il software.

L’installazione, per una persona abituata a predisporre Mac, mette addosso l’inquietudine. Il processo è composto da una faida e un tentativo di circonvenzione di incapace, oppure una rappresentazione neomoderna di *Arlecchino servitore di due padroni* dove Arlecchino è l’utilizzatore e i due padroni sono coloro che se ne contendono i dati e metadati, ovvero il produttore dello hardware e quello del software.

Ambedue i padroni chiedono a ogni passo indirizzi, numeri di telefono, accensione di account oppure autenticazione nel caso l’account esista già. E qui si acconsente alla trasmissione di dati per migliorare le prestazioni, lì la telemetria, qua l’assenso a ricevere pubblicità dentro i risultati delle ricerche di sistema, là i suggerimenti commerciali sullo sfondo scrivania e avanti così, schermo dopo schermo, richieste di permessi, concessione di autorizzazioni, annunci di aperture di canali di tracciamento, tutto nel linguaggio più felpato e involuto concepibile.

Di software, di configurazione, di opzioni, se ne parla poco o nulla. Tutto è un *dark pattern* subdolo e sofisticato per convincere l’utente distratto a passare tutti i dati del mondo e ricevere tutte le pubblicità possibili attraverso programmi, dal browser in giù, per i quali fornire un servizio è funzione secondaria, un male necessario per giustificare ogni e qualsiasi chiamata a casa. Nutro un certo fastidio verso gli slogan troppo radicali tipo *il prodotto sei tu*, le crociate contro il *capitalismo della sorveglianza* e via dicendo. Un conto sono i profitti illeciti, un conto è alimentare un business in modo onesto e la pubblicità o la profilazione, praticati con misura e rispetto, in questo possono avere un posto. Qui cadono le braccia, però. Perché cadono le maschere: l’idea che l’apparecchio possa avere una funzione per l’utente è negletta e interessa a nessuno. L’apparecchio esiste unicamente per profilare e tracciare chi lo usa. Certo, c’è su anche del software, sembra quasi per un incidente o una sbadataggine.

In realtà no e da questo punto di vista va persino peggio che rispetto al setup.

Nell’apparecchio è preimstallata una copia della solita sedicente suite di produttività individuale, che dopo trenta giorni necessiterà di *attivazione* (di soldi, pagamenti, abbonamenti non si parla, fa troppo volgare; l’attivazione è più elegante e fumosa).

Uno se ne accorge quando appare la richiesta di scegliere il formato di salvataggio preferito: quello della suite o quello di [Open Document Format](https://www.oasis-open.org/committees/tc_home.php?wg_abbrev=office)? Naturalmente la scelta preimpostata è la prima. Aggiungiamo che nessuno conosce la seconda ed è facile prevedere come finisca la farsa.

La cosa peggiore è la pretestuosità della situazione. Non c’è alcun motivo di preimpostare la scelta, salvo farla passare come una decisione dell’utente, il quale non avrebbe motivi per prenderla: in un mondo normale, basta decidere al momento di salvare un documento. Ma è pericoloso dare questa libertà e significherebbe incoraggiare l’uso dei menu e l’apprendimento del programma. Molto meglio separare questa scelta dal software che la riguarda, presentarla nel setup come obbligatoria e contare che il novantanove percento delle persone non ci badi.

Anch’io ho scelto il primo formato, perché quel portatile è destinato a produrre contenuti da portare a scuola e fare altrimenti sarebbe combattere una battaglia persa. Però ho subito scaricato [LibreOffice](https://www.libreoffice.org) e l’ho reso software preimpoststo per quei formati, allo scopo di scongiurare l’uso della suite preinstallata e della inevitabile attivazione.

Indovina? Se per caso si fa doppio clic su un documento, subito si apre una finestra di dialogo che propone di aprirlo con la suite e al diavolo la preimpostazione. La scelta preselezionata è ovviamente *sì* ed ecco che un clic incauto rimette in funzione la suite che un giorno (non lontano) vorrà essere pagata.

Il ThinkPad è stato configurato, tutto funziona, ma solo fino alla prima disattenzione, quando ripartirà la circonvenzione di incapace. Scoramento.

Per il produttore dello hardware e per quello del software non ci sono persone, neanche utenti; solo fonti di dati da aspirare e bersagli su cui sparare pubblicità. Non c’è alcun servizio, solo brama di sfruttamento dentro un meccanismo destinato ad arricchire in qualsiasi forma solo e unicamente loro due.

Denunciare la situazione non serve, mostrarla nemmeno, spiegarla neppure. Non ho neanche il coraggio di spiegare al proprietario del marchigegno che crede di avere la proprietà di una tastiera quando, in effetti, ne è lo strumento.