---
title: "Una rubrica che mancava"
date: 2015-02-24
comments: true
tags: [AppleWorld.Today, Jurgensen, Apple, Tuaw, Aol]
---
Si chiude una porta – *The Unofficial Apple Weblog* ha chiuso i battenti durante una riorganizzazione di America On Line – e si apre un portone: diversi transfughi hanno messo in piedi *AppleWorld.Today*, cui tutti si augura lunga e proficua vita.<!--more-->

Il portone è più che mai aperto a tutti perché, a dire degli autori, sono il primo sito Applecentrico a ospitare stabilmente una rubrica sull’accessibilità.

Non so se sia vero ma, anche non lo fosse, resterebbe una buona, anzi, ottima notizia. Il primo articolo è una [autopresentazione del titolare](http://www.appleworld.today/blog/2015/2/14/accessible-apple-introducing-alex-jurgensen), Alex Jurgensen:

>Apple è l’unico gigante della tecnologia a mostrare un livello di impegno così elevato nell’assicurare l’uguaglianza di accesso ai propri prodotti. Sono onorato di coprire i progressi dell’azienda in questa rubrica, perché sento che Apple ha un futuro luminoso per quanto riguarda l’accessibilità.

Non so se sia vero, ma sarebbe una ottima, anzi, eccellente notizia. Non si parla mai troppo di accessibilità.