---
title: "Eterni secondi"
date: 2018-02-28
comments: true
tags: [S9, Samsung, Matteo, iPhone]
---
Ogni tanto Apple viene accusata di questo o quel monopolio e di danneggiare subdolamente la concorrenza.<!--more-->

Prendiamo per esempio le prestazioni dei processori nei computer da tasca. Il nuovissimo S9 Samsung (grazie **Matteo**) [sta saldamente dietro iPhone 8 e iPhone X](http://appleinsider.com/articles/18/02/27/early-benchmarks-shows-samsung-galaxy-s9-well-behind-iphone-x-in-processor-performance), che pure sono usciti oramai un quadrimestre fa. (Tanto che [Bloomberg spettegola](https://www.bloomberg.com/news/articles/2018-02-26/apple-is-said-to-plan-giant-high-end-iphone-lower-priced-model) già sulle nuove uscite).

Samsung è uno dei principali fornitori di componentistica ad Apple e guadagna soldi per ogni iPhone venduto, se parliamo di concorrenza.

Ma la competizione sui processori sembra sbilanciata non tanto per il monopolio, quanto per superiorità tecnologica. Il telefono che esce per secondo (più tardi) dovrebbe quanto meno competere, invece che mettersi in coda.