---
title: "La forza irresistibile"
date: 2021-06-04T02:09:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [Apple, remote working] 
---
Da settembre, i dipendenti Apple [lavoreranno in ufficio lunedì, martedì e giovedì](https://www.bloomberg.com/news/articles/2021-06-02/apple-s-ceo-sets-september-return-to-offices-in-flexible-setup), mentre avranno l’opzione di lavorare in remoto il mercoledì e il venerdì.

Ad alcuni reparti specifici saranno chiesti quattro o cinque giorni a settimana in presenza; in compenso, tutti potranno fare domanda per avere fino a quindici giorni supplementari l’anno di lavoro in remoto, che saranno concessi o meno dai responsabili di reparto.

Nel 2022 verrà effettuata una valutazione dei risultati, che potrebbe portare a una nuova riformulazione degli orari di lavoro.

È una vera prova di forza di Apple, non con i dipendenti ma con la propria significatività; si moltiplicano negli Stati Uniti – ma arriverà l’onda anche qui – i casi di aziende ansiose di riavere le persone negli uffici, mentre le persone stesse non sono altrettanto trepidanti, con alcuni che addirittura [si licenziano](https://www.bloomberg.com/news/articles/2021-06-01/return-to-office-employees-are-quitting-instead-of-giving-up-work-from-home) se trovano la richiesta ingiustificata o eccessiva.

Ci sono mille distinguo da fare perché le situazioni, ovviamente, sono molto diverse tra loro; certo è che, se certe aziende possono porsi come oggetti inamovibili nel rivolere le persone in ufficio, queste ultime potrebbero rivelarsi una forza irresistibile. Negli Stati Uniti non è infrequente che un pendolare convertito al *remote working* risparmi cinquemila dollari l’anno.

Stai a vedere che, magari suo malgrado, Apple si ritrova a fare innovazione anche in questo campo.

*Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile.*               