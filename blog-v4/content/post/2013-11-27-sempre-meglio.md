---
title: "Migliorare, sempre"
date: 2013-11-27
comments: true
tags: [software, Mathematica, RaspberryPi, Hische]
---
Migliorare, sempre. Per esempio con l’aiuto di [The Sweet Setup](http://thesweetsetup.com), un nuovo sito dedicato a consigliare le migliori *app* per ciascuna situazione. Grazie a [Mario](http://multifinder.wordpress.com/2013/11/19/missione-impossibile/) per l’eccellente segnalazione. Dovranno dare il meglio di sé gli autori e non abbiamo più scuse noi: se vogliamo il meglio, basta leggere quale sia.<!--more-->

Migliorare, sempre. [Jessica Hische](http://jessicahische.is) ha compilato la pagina [Quotes and Accents](http://quotesandaccents.com) spiegando come si ottengono le accentate e i trattini su Mac. La pagina riguarda le tastiere americane; quelle italiane presentano qualche differenza. Un bell’esercizio sarebbe scrivere a Jessica, come lei stessa propone, per inviarle lo stato italiano delle cose. In mancanza di questo, anche leggere come si usano i trattini è già un inizio. Come si usano in inglese, naturalmente; arriva sempre qualcuno che li vuole impiegare in italiano allo stesso modo, senza sapere che da noi funziona diversamente e contemporaneamente ignorando come si adoperano in inglese, cioè il massimo.

Migliorare, sempre. Interesse per la matematica, ma pochi soldi per comprare [Mathematica](http://www.wolfram.com)? Ignorando per un momento le alternative *open source*, a breve <a http://www.panic.com/blog/the-panic-status-board/="http://www.raspberrypi.org/archives/5282">Raspberry Pi uscirà con Mathematica incorporato</a>. È un computer che sta nel palmo di una mano, pronto per tentare i progetti hardware più pazzi, pilotato da una versione di Linux apposta. Costa un sottomultiplo veramente piccolo di una copia di Mathematica.

Migliorare, sempre.