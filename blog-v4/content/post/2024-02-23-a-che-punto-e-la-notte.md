---
title: "A che punto è la notte"
date: 2024-02-23T01:44:13+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [Journal, Miro, Mural, Campfire]
---
Cerco alternative temporanee a [Miro](https://miro.com) che secondo gli amministratori di sistema di una banca notevolmente pirla costituirebbe un problema di sicurezza tale da non autorizzare il programma neanche temporaneamente. Intanto: Miro è il meglio sulla piazza. Secondo: l’alternativa più divertente è [Mural](https://mural.co). Ci sono diverse altre opzioni tra cui, di fatto, quasi un clone, che non linko perché non è una cosa onesta.

Ho scoperto di avere su iPhone una copia di *Journal*, la app che dovrebbe incoraggiare la gente a scrivere il diario personale. A parte il mio personale avere già un blog, può anche andare bene; è sicuramente una esigenza che potrebbero avere molte persone non tecniche. Soddisfatte le ambizioni di scrittura da tenere sotto il cuscino, qualche ingegnere sfaccendato in Apple potrebbe per favore frullare Note e Freeform e venire fuori con una app solo Markdown la cui specializzazione consiste nel creare con facilità disarmante link a snippet di informazione che in un attimo diventano una wiki sincronizzata su tutti i device attraverso iCloud? Grazie.

[Campfire](https://once.com/campfire) è un bel sistema, semplice e efficace, di chat e comunicazione collaborativa, realizzato da gente competente, superesperta, che può commettere errori ma sicuramente non intorta i clienti. Duecentonovantanove dollari ed è tuo, compresi tutti gli aggiornamenti minori e, quando arriva un aggiornamento maggiore, decidi se ti interessa pagarlo o se stai bene come sei. Te lo installi su un tuo server (dicono che si fa in pochi minuti), ti forniscono supporto base per arrivare a farlo funzionare (non più di quello) e hai un sistema funzionale, che non dimentica, isolato quanto lo desideri, sicuro, totalmente indipendente, che controlli pienamente. Sto pensando di fare una proposta folle a una azienda che conosco… ma se capitasse un momento finanziariamente favorevole potrei anche pensare a una pazzia a livello personale. Certo, per certi versi c’è da chiedersi perché non un server Irc, che è gratis…