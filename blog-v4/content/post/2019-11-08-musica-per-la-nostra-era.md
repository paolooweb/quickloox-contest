---
title: "Musica per la nostra era"
date: 2019-11-08
comments: true
tags: [Organelle, iPad, Raspberry, Pi, Linux]
---
Che bello [Organelle](https://www.critterandguitari.com/organelle)! Il sito spiega piuttosto bene le cartatteristiche dell’oggetto e *Engadget* ha una corposa [prova su strada](https://www.engadget.com/2019/11/08/critter-and-guitari-organelle-music-computer-review-jack-of-all-trades/).

In sintesi più adatta a questo spazio, Organelle è un talentuoso scatolotto dotato di comandi meccanici che ricordano certi sintetizzatori molto spartani del tempo che fu, in pochissimo spazio.

Dentro lo scatolotto sta un [Raspberry Pi](https://www.raspberrypi.org) equipaggiato con sistema operativo Linux, cui l’utilizzatore non ha accesso (non accesso previsto, almeno). Un po’ come dentro un iPad si trova un processore Arm con sistema operativo iPadOS, cui l’utilizzatore non ha accesso diretto.

l’interfaccia fornita con Organelle permette di aggiungere, togliere, costruire, modificare *patch*, pezze di software che descrivono variazioni di comportamento di uno strumento sintetizzato. L’idea è ingegnosa: non solo gli strumenti elettronici, ma anche la possibilità di modificare il loro comportamento in modi codificati dipendenti nella loro varietà solo dalla buona volontà di chi li vuole programmare. Esistono centinaia di patch già disponibili.

L’idea delle patch somiglia moltissimo a quella dei Comandi rapidi di un iPad. Su un iPad possono stare app, come un Organelle contiene strumenti, il cui comportamento può essere modificato sul momento da insiemi di istruzioni, così come le patch fanno agli strumenti in Organelle.

I gangli del sistema operativo sono nascosti all’utilizzatore, sia in Organelle che su un iPad.

Cionostante, chi volesse mettere le mani sul sistema operativo di iPad potrebbe effettuare un jailbreak. A quel punto potrebbe installare qualsiasi cosa. Anche un compilatore in versione Unix, per dire.

La stessa considerazione vale, a maggior ragione, per Organelle. Sicuramente c’è un qualche modo di arrivare al Linux che lo aziona, per quanto ciò non sia possibile all’acquirente non esperto.

Incredibile quante similitudini vi siano tra Organelle e iPad, vero?

I produttori di Organelle lo chiamano *music computer*. Anche se non ci si programma sopra, guarda un po’.