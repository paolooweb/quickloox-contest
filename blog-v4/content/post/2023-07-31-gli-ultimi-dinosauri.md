---
title: "Gli ultimi dinosauri"
date: 2023-07-31T01:58:39+01:00
draft: false
toc: false
comments: true
categories: [Hardware, Software]
tags: [Ars Tedhnica, COBOL, Telum]
---
Affascinante inchiesta di *Ars Technica* sullo [stato dell’arte dei mainframe](https://arstechnica.com/information-technology/2023/07/the-ibm-mainframe-how-it-runs-and-why-it-survives/), con rivelazioni inaspettate e, in mezzo alle cose scontate, anche varie informazioni inedite.

Scontato che siano pochi i mainframe nel mondo, che li usino grandi organizzazioni, che ci sia il problema di trovare programmatori COBOL. Meno scontato che un moderno mainframe possa contenere quaranta terabyte di memoria, che sia costruito su misura per il committente, che possa assicurare un uptime a cinque decimali, in pratica fermarsi per non più di una manciata di minuti in un anno.

Tutti i componenti sono *hot swappable* e si cambiano lasciando la macchina in funzione. Ogni mainframe ha centinaia di addetti e costa una barcata di denaro.

Comincia a vedersi la concorrenza di servizi cloud e di società piccole e veloci, ma il dato di fatto è che completare con successo una migrazione da mainframe è oltremodo difficile e che praticamente per chiunque la soluzione migliore è conservare l’esistente.

Saranno dinosauri, ma difficilmente si estingueranno in tempi brevi.

Ah: i processori per mainframe sono ovviamente Intel, ma anche… Samsung. Su questo non avrei scommesso un centesimo.