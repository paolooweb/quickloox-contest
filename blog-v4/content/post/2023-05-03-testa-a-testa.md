---
title: "Testa a testa"
date: 2023-05-03T02:35:51+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [Gruber, John Gruber, Statcounter, iOS, Windows, Daring Fireball]
---
Racconta John Gruber che, a dare [un’occhiata ai dati di Statcounter](https://daringfireball.net/2023/05/browser_and_os_market_share_trends_in_the_mobile_age), si vede questo:

>Negli Stati Uniti, il traffico dati di iOS e quello di Windows sono praticamente testa a testa, con il trenta percento circa di quota ciascuno.

Intanto, il profumo della concorrenza è delizioso rispetto al tanfo da monopolio.

Secondo, mando un pensiero commosso a chi tanti anni fa argomentava che *no, è un telefono, non un computer*.

Windows ha smesso di essere la maggior fonte di traffico nel 2017, a favore di Android. Da allora abbiamo un sacco di altre cose cui pensare, ma almeno delle quote di traffico dei sistemi operativi qualcosa, nel tempo, è migliorato.