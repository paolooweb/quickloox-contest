---
title: "Le pile blu"
date: 2016-06-01
comments: true
tags: [BlueStacks, VirtualBox, Android, MacBook, Pro, Air]
---
In riferimento alle mie [avventure con il muletto](https://macintelligence.org/posts/2016-05-29-in-groppa-al-muletto/), ho passato del tempo apprezzabile sul virtualizzatore Android [BlueStacks](http://www.bluestacks.com/it/index.html), sollecitando di conseguenza la scheda grafica.

Ancora una volta, un MacBook Air 13” di metà 2012 come minimo pareggia, forse supera, il mio MacBook Pro 17” di inizio 2009. Prossima avventura, [VirtualBox](https://www.virtualbox.org), che dovrebbe essere un compito ancora più pesante. Riferirò.