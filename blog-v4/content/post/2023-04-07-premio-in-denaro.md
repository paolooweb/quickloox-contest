---
title: "Premio in denaro"
date: 2023-04-07T10:24:40+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [Chollet, François Chollet, Arc, Arc Challenge, Lab42]
---
ChatGPT non parteciperà a questa [competizione](https://twitter.com/fchollet/status/1622678401323782144).

<blockquote class="twitter-tweet"><p lang="en" dir="ltr">Here&#39;s a new competition to solve the ARC challenge, with $69k CHF in prizes ($75k), running throughout 2023. Make progress towards real general intelligence -- not the statistical mimicry-based ersatz -- and make history.<a href="https://t.co/DSCwsjNpXT">https://t.co/DSCwsjNpXT</a></p>&mdash; François Chollet (@fchollet) <a href="https://twitter.com/fchollet/status/1622678401323782144?ref_src=twsrc%5Etfw">February 6, 2023</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

Si tratta di ARCathon, una gara creata per incoraggiare il fiorire di intelligenze artificiali autentiche, capaci di risolvere una batteria di test chiamata Abstraction and Reasoning Corpus e messa a punto da François Chollet, figura di punta nel settore.

ARCathon è organizzata dai [Lab42](https://lab42.global) di Davos e premierà con settantacinquemila dollari il software capace di distinguersi nella soluzione dei test. Il risultato migliore raggiunto finora è il trentuno percento.

Si potrebbe discutere di come l’intelligenza artificiale sia degna di un premio con cui al massimo acquistare un bilocale mentre si investono miliardi di dollari su sistemi generativi basati su tecnologie vecchie di quarant’anni. Mi limito a evidenziare che è facile cogliere il confine tra ricerca e mercato, tra quello che fa veramente compiere un passo avanti e quanto invece va venduto a persone in gran parte ignare.

Chi può e sa, metta a confronto ChatGPT o Bard o Claude con la parte pubblica dei test Arc (sì, c’è anche una sezione segreta; sapevi che i sistemi generativi attuali fanno un figurone nei test di esame o di selezione proposti agli umani perché, semplicemente, dentro la loro base dati ci sono già le soluzioni?) e ci sappia dire. Peccato che Lab42 non abbia pensato a un premio in crowdfunding: *vuoi dare soldi a un sistema statistico o alla ricerca sulla vera intelligenza artificiale?*.

Sarebbe un bel modo di sensibilizzare.