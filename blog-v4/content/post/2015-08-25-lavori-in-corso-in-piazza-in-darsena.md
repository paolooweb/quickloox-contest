---
title: "Lavori in corso in piazza in darsena"
date: 2015-08-25
comments: true
tags: [AllAboutApple, Savona, Museum, museo, darsena]
---
Sono stato a Savona a visitare la nuova sede dell’[All About Apple Museum](http://www.allaboutapple.com). L’inaugurazione è prevista per il 24 ottobre 2015 e si lavora quando si può – il museo è un capolavoro di passione e frutta ai soci solo lavoro extra non stipendiato – perché tutto sia pronto.

Sono stati posati i mobili ed è in via di completamento l’impianto luci. Non posso fare anticipazioni (né divulgare le foto che ho scattato) però una cosa posso dirla: l’ambiente è perfettamente in carattere con il contesto che è la nuova darsena di Savona, un lavoro spettacolare di recupero e riqualificazione di spazi ex portuali. La zona è completamente ristrutturata, pulita, moderna e il museo si trova esattamente sopra l’ufficio informazioni turistiche, al piano superiore della bella e rilassante piazza intitolata a Fabrizio De André.

Di fatto, i turisti in movimento da e per le grandi navi da crociera che attraccano proprio davanti disporranno di una attrazione in più. Per tutti gli altri, è difficile impiegare più di cinque minuti di passeggiata indolente per arrivare sul posto dal centro di Savona. I comodoni dispongono di quantità consistenti di parcheggio libero a qualsiasi distanza e un parcheggio a pagamento esattamente sotto la struttura, più vicino di così è impossibile. Un’ora di posto macchina costa 1,20 euro e, abituato a certi ritmi milanesi, quasi mi sembrava gratis.

Questa nuova sede, con la giusta comunicazione esterna, ha tutto il potenziale per segnare la definitiva consacrazione di All About Apple. Non vedo l’ora che arrivi l’inaugurazione. E così spero di te.