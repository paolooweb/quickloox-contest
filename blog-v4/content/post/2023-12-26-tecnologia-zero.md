---
title: "Tecnologia zero"
date: 2023-12-26T02:23:21+01:00
draft: false
toc: false
comments: true
categories: [Internet]
tags: [LaTeX, SSD, Markdown]
---
Il mio Natale è stato pochissimo tecnologico, anche se quello complessivo ammonta a una dose più che onesta. Mi sono passati sotto il naso un watch e una *chiavetta* da duecentocinquantasei giga. Più interessante la seconda, per il fatto di avere due connettori: da una parte Usb-C, dall’altra parte Usb classico.

A una seconda occhiata, niente di speciale: non compro chiavette da un tempo esagerato e, intanto, il tempo ha cambiato un po’ di cose. Situazioni come questa sono diventate molto comuni e niente da dire.

Più interessante l’avere chiacchierato con una parente che l’anno prossimo sarà sotto tesi. Le ho prospettato l’idea di uscire tecnicamente dal seminato e realizzare il suo lavoro con strumenti diversi dal solito Office. L’interesse almeno teorico è stato sensibile e così potrei trovarmi a sottoporre [LaTeX](https://www.latex-project.org) e compagni a una tesista nel giro di qualche mese.

La tecnologia viene ormai data per scontata da chiunque. Invece ha ancora qualcosa da dire, soprattutto quando si tratta di azzerare ogni sovrastruttura e ottenere il massimo con il grado zero di sofisticazione dei dati: il testo.