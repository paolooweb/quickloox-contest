---
title: "Un computer e non lo sa"
date: 2022-03-22T00:16:54+01:00
draft: false
toc: false
comments: true
categories: [Hardware]
tags: [Studio Display, iPhone 11, iPhone SE, iPad, A13 Bionic, tf42]
---
Giorni fa [spiegavo a un pubblico selezionato](https://macintelligence.org/posts/2022-03-17-zitto-e-scrivi/) che siamo diventati tutti IT Manager e che la figura professionale stessa è da riconfigurare (cosa vera da almeno quindici anni). Uno dei motivi è che, se nella preistoria era tanto trovare un computer appoggiato sulla scrivania dell’ufficio, oggi viviamo con due, tre cinque computer intorno a noi, parlando solo di quelli personali. *E andrà sempre peggio*, ho chiosato.

Si prenda per esempio il nuovo [Studio Display](https://www.apple.com/it/studio-display/) di Apple. Un monitor? Dipende da come lo si guarda. Si potrebbe anche vederlo come [un device iOS equipaggiato con la versione 15.4 del sistema operativo e sessantaquattro gigabyte di disco](https://www.macrumors.com/2022/03/21/studio-display-contains-64gb-storage/) (grazie a **tf42** per la segnalazione), anche se sessantadue di questi sono inutilizzati. Per ora.

Probabilmente lo resteranno, ufficialmente almeno. Qualche spazio fa comodo per aggiornamenti del firmware e magari qualche funzione accessoria in più, certo non tutto quello spazio.

Molto probabilmente si tratta di economia di scala; ad Apple costa meno inserire in Display Studio un *system-on-chip* A13 Bionic così come viene prodotto che realizzare una versione apposita per il monitor. Come continueremo a chiamarlo, per quanto contenga al suo interno – possiamo scegliere – il nucleo di un iPhone 11, un iPhone SE di seconda generazione o un iPad di nona.

Suppongo che vedere uno sviluppatore intraprendente aprirlo e farne una stazione di lavoro autonoma con qualche *hack* hardware sia solo questione di tempo.