---
title: "L’anziano specialista"
date: 2018-06-23
comments: true
tags: [backup, iMac, G5, PowerMac8,2]
---
Nella casetta dei genitori al mare c’è un iMac G5, alias [PowerMac8,2](https://everymac.com/systems/apple/imac/specs/imac_g5_2.0_20.html).

Passa buona parte dell’anno spento, con in testa un coprischermo, giusto per la polvere. La temperatura della casa scende a valori invernali e l’umidità regna sovrana.

Ogni tanto qualcuno arriva, lo accende, gli collega una chiavetta di connessione Internet e lui resta acceso giorno e notte. Il massimo di riposo è andare in stop quando si parte per una gita. D’inverno si ritrova all’improvviso con i termosifoni a potenza piena, d’estate le temperature sono da mare, attorno ai trenta di giorno e venti-venticinque di sera.

In verità fa solo quello, salvo eccezioni. Sta acceso, fa andare la chiavetta. Non ci sono dati da custodire o utenze da proteggere. Fatica zero. In compenso non si è mai guastato, non è mai stato aperto, non è mai stato espanso o modificato lato hardware. Lato software, arriva a Mac OS X 10.5.8 e niente più.

Quanti anni abbia, lo lascio scoprire a chi segua il link più sopra. Qui posso dire da quanto tempo nessuno effettua un backup.

 ![Time Machine non effettua il backup del tuo Mac da 3060 giorni](/images/time-machine-backup.png  "Time Machine non effettua il backup del tuo Mac da 3060 giorni") 

Se incontri qualcuno che parla di obsolescenza programmata e dei computer costruiti per durare oltre la garanzia e poi rompersi, salutamelo caramente.