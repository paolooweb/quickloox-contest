---
title: "In principio fu la home"
date: 2015-04-24
comments: true
tags: [web, Apple, Flickr]
---
Un bell’esercizio di archeologia informatica, andare alla ricerca della prima schermata del sito web di Apple.<!--more-->

Pare che il primato se lo aggiudichi al momento questa [schermata del 1992](http://kfury.com/the-first-apple-homepage). Erano davvero altrissimi tempi (e lo usavo, quello sfondo scrivania che imitava la carta grezza).

C’è anche una collezione strepitosa di [prime pagine del sito Apple negli anni](https://www.flickr.com/photos/kernelpanic/sets/283374) su Flickr, tutta da guardare. Le evoluzioni della *home* rispecchiano quelle tecnologiche: c’è stato un tempo senza JavaScript, senza Google, senza Html5. Abbiamo compiuto progressi impressionanti.