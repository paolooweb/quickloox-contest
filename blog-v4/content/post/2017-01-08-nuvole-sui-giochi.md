---
title: "Nuvole sui giochi"
date: 2017-01-08
comments: true
tags: [Air, MacBook, GeForce, Now, Nvidia, Witcher]
---
I Mac non sono mai stati macchine da gioco per eccellenza, ma i detrattori devono tenere presente qual è la direzione.<!--more-->

*Macworld* testimonia di un nuovo servizio Nvidia che fa lavorare il gioco nel cloud e poi [trasmette la resa video in streaming](http://www.macworld.com/article/3155191/software-games/blasphemy-i-played-witcher-3-on-a-macbook-air-thanks-to-nvidias-geforce-now.html#tk.rss_news) sul computer di preferenza. Tradotto: il lavoro pesante della scheda video avviene sul cloud mentre il computer locale pensa solo a ricevere lo streaming video e inviare i comandi.

Così acccade che un MacBook Air possa offrire al suo proprietario una partita a [The Witcher 3](http://thewitcher.com/en/witcher3), che neanche esiste in versione Mac.

Oggi non è cosa da tutti. Serve una buonissima connessione, il costo è elevato. La storia dell’informatica spiega che è solo questione di tempo in entrambi i casi.