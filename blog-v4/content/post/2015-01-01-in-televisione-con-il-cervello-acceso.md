---
title: "In televisione con il cervello acceso"
date: 2015-01-02
comments: true
tags: [AppleTV, tv, Chromecast, Google, WebTV, Microsoft, Olivetti, MsnTV]
---
I primi tentativi di unire computer e televisione datano per quanto ne so agli anni ottanta; la stessa Olivetti aveva provato a vendere una tastiera per schermo televisivo, credo ai primissimi tempi di Internet. È attualità [Google TV](http://www.google.com/tv/), lo è stata per buon tempo [Msn TV ex WebTV[(http://en.wikipedia.org/wiki/MSN_TV) di Microsoft.<!--more-->

Tutti quanti i tentativi avevano in comune un errore di fondo: non avere capito che il computer si usa con il cervello acceso e la televisione con il cervello spento. Detto senza alcuna connotazione negativa; la vacanza non è da meno del lavoro, per usare una analogia di massima banalità.

Passata una sera a esplorare il mondo per me novello di AppleTV (so di arrivare ultimo, **avariatedeventuali**), ho capito perché funziona; Apple ha dribblato il problema. Invece che cercare a tutti i costi di portare il *browser* sul televisore o pensare che la gente si sieda davanti a un Mac per guardare un film invece che goderselo dal divano, si è concentrata sull’interfaccia e l’esperienza di utilizzo.

Si può continuare a guardare la televisione con il cervello spento, senza snaturare la sua vocazione, con l’aggiunta della grande risorsa di contenuti rappresentata dai computer di casa. Il tutto con una fluidità e una immediatezza che, quando Apple raggiunge appieno, sono la spiegazione del perché al mondo ci voglia una azienda così. In pochi minuti guardavo YouTube, proiettavo uno Streaming Photo da iCloud, stavo per acquistare un fllm, a momenti compravo un *pass* per l’Nba (e poi mi toccava accendere un mutuo). E anche in questo la musica è quella consueta: di apparecchi con le stesse funzioni ce ne sono e magari costano pure meno. La stessa semplicità, la stessa linearità, però, se la sognano.

Sono rimasto definitivamente conquistato mettendo via il telecomando in dotazione per usare al suo posto iPhone. In salotto ogni telecomando in più è una cattiveria del destino, anche se è minimo come quello Apple. Il computer da tasca invece lo si vuole sempre a portata e può anche occuparsi del televisore. Ho voluto strafare e ho abbinato anche iPad alla AppleTV: *overkill* se capita di dover digitare qualche password, a parte ovviamente il collegamento via Bluetooth di una tastiera fisica propriamente detta.

In televisione con il cervello acceso è allo stato delle cose un ossimoro. AppleTV, anzi, [tv](https://www.apple.com/it/appletv/) è quanto vada più vicino finora a chiudere il divario.