---
title: "Il minimo dispensabile"
date: 2015-07-09
comments: true
tags: [Wind, Vodafone]
---
Domata con fatica percepibile [una chiavetta Wind](https://macintelligence.org/posts/2015-06-11-il-massimo-del-minimo), sono ora alle prese con una chiavetta Vodafone tipo K4201, terminale di un’organizzazione che ha qualcosa che non va. Di raffermo, di trascurato.<!--more-->

Non so da dove partire esattamente. La chat di supporto è fatta di persone bravissime e disponibili. Il sito è pieno di circoli viziosi e vicoli ciechi, con una pagina di contatori che dovrebbe mostrare i consumi in tempo reale ma non sono mai riuscito a vedere, le offerte per ricaricabili che non sono acquistabili online ma solo in negozio (perché?) e molto altro. Va notata la procedura per cui attivi un’offerta e ti arriva la possibilità di scegliere un premio, tra una serie di offerte che sono *esattamente uguali* a quelle che si vedono senza essere già clienti. *Meh*.

Il peggio del peggio è tuttavia il software. Nella barra dei menu si installa una specie di virus che fa subito desiderare di disinstallarlo, ed è il componente più amichevole.

Il programma Vodafone Mobile Broadband è uno scherzo. L’equivalente di Wind, Mobile Connect, fa pena nella grafica, nell’interfaccia, nelle funzionalità. Sembra toccare il fondo, quando Vodafone Mobile Broadband comincia a scavare almeno due piani più sotto. Qualunque cosa avvenga nell’interfaccia, *qualunque*, tocca vedere la rotella girare per dieci, venti, trenta secondi e poi forse.

L’obbrobrio ha la pretesa di essere un centro di gestione delle connessioni di rete, quando basterebbero due cose: un pulsante Connect e un contatore di connessione. Non è possibile aggirarlo perché l’orrenda cosa si registra dentro le Preferenze di Sistema come interfaccia di rete, dunque chiamare semplicemente un numero telefonico è escluso.

Peraltro, la connessione *parte da sola* se inserisco la chiavetta nella porta Usb. Volendo sarebbe anche un plus, neanche il bisogno di premere un pulsante. Naturalmente, mettendo il Mac in stop, non si spegne.

La funzione SMS è terrificante per quanto è brutta e macchinosa. Dopo che hai selezionato testo, la selezione *sparisce*. Su Mac una cosa così non si è mai vista. Sembrano i miei farfugliamenti in Xcode, che quando passo le trenta righe mi viene l’emicrania.

Dalle informazioni sul programma (accessibili dopo dieci secondi di rotella) si vede che gli autori di questo capolavoro stanno in un’azienda tedesca. Sapendo che Vodafone è una multinazionale globale immensa per dimensioni e giro di affari, io sarei curioso di sapere quanto è stata pagata. Neanche alle Poste sarebbe tollerato questo livello di qualità.

Alla fine della giornata, pensavo di avere pagato trenta euro per dieci gigabyte da usare in un mese e mi ritrovo inondato di messaggi che mi informano di stare finendo il credito e di avere raggiunto il limite giornaliero di navigazione. Non posso neanche chiedere informazioni in chat, chiusa dalle 22.

Ecco, l’idea che i clienti si muovano comunque da padelle a braci e quindi tanto valga servire software escrementizio tendente al minimo comune denominatore, vista la totale assenza di qualità nel settore, un giorno smetterà di pagare. Considerato il tempo che tocca sprecare, sarà una soddisfazione.