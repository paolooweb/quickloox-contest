---
title: "Una degna cornice"
date: 2022-11-09T01:34:17+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [Apple Frames, Apple Frames 3.0, Frames 3.0, Viticci, Federico Viticci, MacStories, Swift Playgrounds]
---
La [terza versione di Apple Frames](https://www.macstories.net/ios/apple-frames-3-0-completely-rewritten-support-for-iphone-14-pro-and-dynamic-island-new-devices-multiple-display-resolutions-and-more/) di Federico Viticci è compatibile con tutte le nuove uscite, più veloce, scritta meglio, più completa, più furba, semplice e complessa e meravigliosa.

È gratis, funziona su iPhone, iPad e Mac; parlando specialmente a chi dice che è finito il tempo in cui si smanettava su Mac, non è una app, di quelle difficili (quanto fare un po’ di percorso con [Swift Playgrounds](https://www.apple.com/swift/playgrounds/)) e dolcemente complicate. È un comando rapido, una shortcut, una scorciatoia. Lo stesso genere di complessità e difficoltà dal quale ho tirato fuori uno stupido loop che chiede operazioni a caso dalle tabelline per fare ripassare Lidia quando siamo in macchina.

Se per caso abbiamo da mostrare una schermata, Apple Frames 3.0 lo fa nell’ambiente naturale della schermata stessa, in modo naturale, senza fatica e senza problemi. Craig Federighi direbbe *how cool is that?*