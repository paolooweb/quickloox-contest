---
title: "Il bene dal male"
date: 2023-01-25T01:30:55+01:00
draft: false
toc: false
comments: true
categories: [Web]
tags: [Rijksmuseum, Amsterdam, Vermeer, Johannes Vermeer]
---
Invece di discutere tra stupidi se sia meglio in presenza oppure in remoto, sarebbe ora di passare all’idea di scegliere bene che cosa fare e poi farlo altrettanto bene. Una esperienza digitale fatta bene supera l’analogico e viceversa, in presenza fatto bene batte il digitale.

La prova sta nella spettacolare mostra che il Rijksmuseum di Amsterdam dedicherà prossimamente al pittore Johannes Vermeer. Ho avuto il privilegio di essere invitato alla visione della collezione van Gogh del museo e l’esperienza è stata indimenticabile; sul web si possono trovare immagini di tutti quei dipinti, ma averli davanti è un’altra storia, proprio diversa.

Andare ad Amsterdam non è comunque abitudine quotidiana, per un italiano medio. Ora per esempio vado verso le due di notte, tra poche ore mi metterò in viaggio per l’Emilia e non ho letteralmente modo di dedicare un paio di giorni, come minimo, alla visita del Rijksmuseum. Che oltre a richiedere tempo chiede anche soldi e scommetto che avere una congiunzione astrale favorevole di entrambi i segni, a piacere, non sia cosa automatica per nessuno.

Però il museo ha attrezzato una [visita guidata digitale al lavoro di Veermer](https://www.rijksmuseum.nl/en/johannes-vermeer), narrata dall’attore [Stephen Fry](https://www.stephenfry.com) per chi voglia ascoltare in inglese. Semplice, elegante, straordinaria e basta un browser. Una cosa fatta non bene; benissimo. Vedere i dipinti dal vivo rimane un’altra cosa, ma stare a Milano e farseli raccontare di notte sotto il piumone da una voce narrante suggestiva, su testi curati minuziosamente dai curatori del museo, non ha prezzo. E più tardi posso rivedere cose in treno, se lo desidero. O ascoltarlo assieme alle figlie, che hanno da imparare l’inglese, quando torno a casa in serata.

Non c’è da scegliere se in presenza o in remoto, ma è essenziale eliminare alla svelta le cose fatte male, per concentrarsi su quelle fatte bene, comunque siano realizzate.