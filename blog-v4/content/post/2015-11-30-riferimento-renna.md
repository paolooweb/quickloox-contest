---
title: "Riferimento renna"
date: 2015-11-30
comments: true
tags: [Natale, Pi, Zero, Raspberry]
---
Che cosa mi regalo per Natale?<!--more-->

Deve essere una cosa di costo assolutamente basso (tipicamente una app, ma un Pi Zero, [a trovarlo](https://macintelligence.org/posts/2015-11-28-quando-i-computer-avevano-la-coda/), costerebbe cinque dollari…). Con qualche potenziale di curiosità, oppure per qualche verso fuori dall’ordinario, o ancora controcorrente. Niente cose di moda, gioconi *free-to-play* dell’ultim’ora, cose troppo evidenti.

E tu, che cosa ti regali per Natale? Parlando, ovviamente, dei temi cari a queste pagine e non altro.