---
title: "Non c’è tre senza beta"
date: 2021-11-14T00:16:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [FreeCiv] 
---
È appena scoccato il ventiseiesimo compleanno di [FreeCiv](http://www.freeciv.org) e da un mesetto il gruppo degli sviluppatori riceve feedback rispetto alla terza e ultima beta di FreeCiv 3.0.

Dai commenti sembra di capire che finalmente ci sarà la compatibilità Mac.

Due belle notizie, per la comunità open source e per quella di macOS. FreeCiv interessa chiaramente agli amanti del genere; per loro, però, è una bomba.

Si avvicina Natale e poche cose fanno meglio al mondo del software che una donazione, anche minuscola, a progetti software liberi e di qualità di cui tutti possono beneficiare a costo zero. In che altro settore industriale, sociale, materiale, animale, minerale può mai accadere qualcosa di simile?

*Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile.*