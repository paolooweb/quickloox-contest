---
title: "Un caso da manuale"
date: 2020-10-01
comments: true
tags: [Apple, El, Capitan, macOS, MacBook, Pro, Vpn, Sierra, Mojave, Catalina, High, Big, Sur]
---
C’è un MacBook Pro che deve usare un certo programma di VPN per entrare in una intranet e lavorare.

Il software di VPN si installa, ma non si collega. I controesempi e le prove incrociate sembrano puntare a una incompatibilità con quella versione di macOS.

La pagina ufficiale del software dichiara la compatibilità totale (tutte le funzioni a disposizione) del programma Vpn con quella versione del sistema operativo. Dal supporto tecnico e dalla documentazione arrivano zero indicazioni utili. È evidente che qualcosa non torna nelle dichiarazioni della software house.

*Quel* macOS è El Capitan, versione 10.11. Non proprio il più recente. Dopo di lui sono arrivati Sierra, High Sierra, Mojave, Catalina e manca davvero poco a Big Sur.

Il MacBook Pro è un mid-2012, che può aggiornarsi [almeno fino a Mojave](https://support.apple.com/kb/SP777). Su [Catalina](https://support.apple.com/kb/SP803) ho un dubbio, nonostante le dichiarazioni di Apple. [Big Sur](https://www.apple.com/macos/big-sur-preview/) è certamente fuori, ma non è che ci serva per forza.

Il proprietario però non vuole aggiornare il computer, neanche [a Sierra](https://support.apple.com/kb/sp742). Altrimenti scatta l’obsolescenza programmata di Apple, dice, e comincia a non funzionare più niente.

La software house potrebbe dichiarare una falsa compatibilità ma anche soffrire di un bug di El Capitan.
El Capitan potrebbe contenere un bug ma anche essere a posto a fronte di un bug nel programma di Vpn.
L’utilizzatore dice che tutto quello che serve funziona e quindi non vede ragione di aggiornare il software di sistema (che in effetti non è dimostrato sia la causa effettiva).

Chi è il responsabile?