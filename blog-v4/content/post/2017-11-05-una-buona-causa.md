---
title: "Una buona causa"
date: 2017-11-05
comments: true
tags: [Audacity, Cyberduck, Wesnoth, Gimp]
---
L’applicazione migliore non è quella con più funzioni, né quella più semplice e neanche quella più personalizzabile, scriptabile o estensibile. È quella che raggiunge il miglior equilibrio tra tutte queste caratteristiche e, in conseguenza di ciò, potrebbe risultare utile al 99,99 percento degli utilizzatori, nonostante i suoi difetti (nessuna applicazione è esente da difetti).

[Audacity](http://www.audacityteam.org) 2.2.0, frutto di un gran lavoro da parte del *team* di sviluppo, si avvicina clamorosamente a essere quel tipo di applicazione, ove si parli di audio.

Unica nota di avvertimento, per tutti gli amici attenti all’accessibilità: la nuova versione del programma non ha ancora inserito il supporto di VoiceOver presente in quelle precedenti (ancora disponibili per lo scaricamento). Gli sviluppatori tuttavia si dichiarano impegnati ad arrivarci; servirà solo un poco di pazienza in più.

Esempi di altri programmi di questo tipo? [Cyberduck](https://cyberduck.io), [Gimp](http://gimp.org), [Battle for Wesnoth](http://wesnoth.org). Non è un caso che siano tutti *open source*. Se nella frenesia natalizia scappassero cinque euro in una di queste direzioni, farebbe solo del bene alla causa. Nemmeno alla causa del software libero; a quella del *buon* software.