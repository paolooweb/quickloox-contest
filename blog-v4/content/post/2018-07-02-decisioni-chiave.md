---
title: "Decisioni chiave"
date: 2018-07-02
comments: true
tags: [Wind, chiavetta]
---
Anzi… chiavetta.

Un indicatore su ventotto, uno su ventinove e chissà che cosa succede quando stabiliscono la fatturazione.

 ![Ventotto o ventinove? La chiavetta non sa quanto consuma](/images/traffico-dati.png  "Ventotto o ventinove? La chiavetta non sa quanto consuma") 

Banale errore di arrotondamento? Sì, ma può capitare che [non siano errori banali](https://www.ma.utexas.edu/users/arbogast/misc/disasters.html).