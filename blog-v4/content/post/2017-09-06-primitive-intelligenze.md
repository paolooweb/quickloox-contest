---
title: "Primitive intelligenze"
date: 2017-09-06
comments: true
tags: [Sea, Explorer]
---
Forse pentito, chi lo sa, per i [cattivi esempi](https://macintelligence.org/posts/2017-09-04-portali-notizie-considerazioni/) di dipendenza dal paleozoico Internet Explorer portati da **Andy**, il suddetto mi ha inviato un campione positivo.<!--more-->

 ![Il sito Sea Milano non ottimizzato per Internet Explorer](/images/sea.png  "Il sito Sea Milano non ottimizzato per Internet Explorer") 

Per quanto preistorico, il portale fornitori di Sea Milano (la società di gestione degli aeroporti) riesce a sfoggiare funzioni evolute a sufficienza da avere una possibilità di non funzionare su Internet Explorer.

Esempi buoni o cattivi, lo ringrazio sempre.

Per i dubbiosi: dopo avere presentato [l’altra volta](/blog/2017/09/04/portali-notizie-considerazioni/) i dati di StatCounter, oggi porto quelli di [NetMarketShare](http://netmarketshare.com).

<iframe width=600 height=570 frameborder=0 scrolling=no marginheight=0 marginwidth=0 src= id=na636401552035410000></iframe><script type=text/javascript>document.getElementById(na636401552035410000).src=http://netmarketshare.com/report.aspx?qprid=1+String.fromCharCode(38)+qptimeframe=M+String.fromCharCode(38)+qpsp=213+String.fromCharCode(38)+qpnp=11+String.fromCharCode(38)+qpch=350+String.fromCharCode(38)+qpdt=1+String.fromCharCode(38)+qpct=4+String.fromCharCode(38)+qpcustomb=0+String.fromCharCode(38)+qpcid=fw1089105+String.fromCharCode(38)+qpf=16+String.fromCharCode(38)+qpwidth=600+String.fromCharCode(38)+qpdisplay=1111+String.fromCharCode(38)+qpmr=10+String.fromCharCode(38)+site=+window.location.hostname</script>

Limitatamente ai desktop, Chrome domina con il 59,38 percento del traffico in agosto. Internet Explorer è al 15,58 percento, in caduta libera (a ottobre 2016 era stimato sul 23,13 percento).

Il dato è superiore a quello di StatCounter perché è limitato ai desktop. NetMarketShare prende soldi da Microsoft e [le studia tutte](/blog/2013/07/03/una-giornata-qualsiasi/) per limitare le brutte figure. Uno dei suoi accorgimenti è appunto tenere separati i dati desktop da quelli *mobile*, come se non ci fosse un computer da tasca in ogni tasca. Il dato *mobile* per Internet Explorer è 0,54 percento. Però, lato desktop, ancora si difende.

Comunque: abbiamo la prova provata che si può abbandonare Internet Explorer anche restando Neanderthal del web. A un sacco di manager poco progrediti potrebbe rasserenare la giornata.