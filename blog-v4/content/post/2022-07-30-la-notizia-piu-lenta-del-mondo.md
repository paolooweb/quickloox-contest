---
title: "La notizia più lenta del mondo"
date: 2022-07-30T02:38:42+01:00
draft: false
toc: false
comments: true
categories: [Internet]
tags: [Giovannini, Eva Giovannini, Russia, Stazione spaziale internazionale, Iss]
---
Era il 6 maggio quando la giornalista statale Eva Giovannini ha annunciato l’uscita della Russia dalla Stazione spaziale internazionale.

[Le ho fatto notare che non era esattamente nei termini in cui la metteva lei](https://macintelligence.org/posts/2022-05-06-eva-né-scienza/), che a difesa delle minacce contro la libertà di stampa mi ha conseguentemente bloccato su Twitter.

Spero di essere ancora bloccato; niente di meglio descrive infatti la statura professionale della giornalista.

Me ne sono ricordato solo perché l’agenzia spaziale russa ha annunciato, all’incirca, che se ne andranno dalla Stazione, ma non prima di averne costruita una propria, [in almeno sei anni da ora](https://www.reuters.com/business/aerospace-defense/russia-nasa-sticking-with-space-station-until-least-2028-2022-07-27/).

Nel 2028 Giovannini potrà vantarsi di avere avuto ragione e in grande anticipo su chiunque altro. Un bel riscatto, considerati i cinque anni che intanto dovranno trascorrere a fare finta di non avere scritto una notizia inesatta.