---
title: "Un nome, due artisti"
date: 2024-01-30T23:48:58+01:00
draft: false
toc: false
comments: true
categories: [Internet, Software]
tags: [Conway, John Conway, Life]
---
Lo scienziato vede il foglio di carta quadrettato e inventa un [gioco](https://conwaylife.com/wiki/Conway%27s_Game_of_Life) tanto semplice quanto profondo, sul quale [si fa ancora ricerca](https://www.discovermagazine.com/the-sciences/mathematicians-prove-the-omniperiodicity-of-conways-game-of-life) dopo tanti anni.

L’artista osserva gli assistenti generativi che, ricevuto un *prompt*, disegnano. E pensa: [voglio essere io a disegnare dopo essermi fatto dare i prompt dall’assistente generativo](https://sauropods.win/@john/111840711715154773).

Hanno età molto diverse ma si chiamano entrambi John Conway. Si vede che il nome è in qualche modo una garanzia.

Due persone differenti ma simili in un tratto fondamentale: hanno contribuito o contribuiscono al progresso dell’umanità che, sì, dipende anche dalla pittura.

(Dai un’occhiata alla [dimostrazione dell’omniperiodicità del gioco Life](https://arxiv.org/pdf/2312.02799.pdf). Anche senza essere scienziato, c’è da rifarsi gli occhi tra simmetrie e armonie).