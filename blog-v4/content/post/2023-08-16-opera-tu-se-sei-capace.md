---
title: "Opera tu se sei capace"
date: 2023-08-16T00:57:30+01:00
draft: false
toc: false
comments: true
categories: [Software, Web]
tags: [You Are the OS]
---
È infine arrivato il perfetto antidoto alla sensazione di superiorità che proviamo nel momento in cui Mac singhiozza un attimo o fa girare la rotella per un secondo in più. *Potevano anche scriverlo un po’ meglio*, cose così. Chi meglio di noi per giudicare l’efficienza di un sistema operativo?

Il nome dell’antidoto è [You Are the OS](https://drfreckles42.itch.io/youre-the-os) e l’antidoto è un gioco da browser nel quale, invece che controllare il traffico aereo, amministrare una rete ferroviaria o costruire una città, siamo… il sistema operativo.

Ci sono priorità di esecuzione tra processi da assegnare, i problemi dello spazio su disco, i log da pulire, tutte quelle cose che diamo per scontate mentre bisognerebbe consegnare un mezzo Nobel a chi le ha realizzate, funzionanti per giunta su milioni di macchine con configurazione mai perfettamente identica.

L’autore del gioco dichiara di non avere avuto alcuna intenzione didattica. Ma la parte educativa c’è tutta: non sempre siamo consapevoli della complessità nascosta dietro il Finder e provare a fare noi il mestiere della macchina, seppure per gioco e gratis, aiuta a rendere l’idea meglio di mille trattati.

Certamente non mi capiterà più di pensare *lo avrei fatto meglio*.

*Gli aggiornamenti  in agosto del blog potrebbero essere irregolari o infrequenti.*