---
title: "I duri cominciano a giocare"
date: 2013-09-13
comments: true
tags: [iOS, Giochi]
---
A 38:40 della [presentazione dei nuovi iPhone](http://www.apple.com/apple-events/september-2013/) viene data dimostrazione dell’imminente [Infinity Blade III](http://epicgames.com/community/2013/09/infinity-blade-iii-coming-september-18/). A prescindere dai gusti e dalla giocabilità, è roba forte in senso grafico e di potenza di elaborazione. Un cellulare con sopra un software del genere era un sogno, solo pochi anni fa.<!--more-->

<iframe width="420" height="315" src="//www.youtube.com/embed/6ny6oSHyoqg" frameborder="0" allowfullscreen></iframe>

Contemporaneamente, iniziative degne di nota su cui puntavo incontrano difficoltà o si bloccano. Il travagliatissimo ritorno di [SimCity](http://www.simcity.com) su Mac continua a [incontrare difficoltà](http://www.polygon.com/2013/8/29/4672890/mac-version-of-simcity-hit-with-installation-and-launch-issues). Il creatore di [Minecraft](http://minecraft.net) ha deciso almeno per ora di rinunciare allo sviluppo di *0x10c*. Troppa attesa e troppa pressione, [afferma](http://www.bbc.co.uk/news/technology-23754311).

Computer da tasca e tavolette stanno cambiando anche il mondo dei giochi. Le *console*? Fanno coppia con i computer. Hanno toccato il picco e si avviano al tramonto. I *trend* di vendita di Nintendo e Sony, ben [riassunti da Horace Dediu di Asymco](http://www.asymco.com/2013/09/09/game-over/), sono in discesa pressoché concomitante con il primo fiorire dell’era *post-Pc*. Microsoft non ha mai fornito cifre di vendita puntuali di Xbox, evidentemente per convenienza a nasconderle.

Come per i fabbricanti di computer, i produttori di *console* stanno per arrivare a un momento della verità. Saranno duri abbastanza da adattarsi e continuare a prosperare?