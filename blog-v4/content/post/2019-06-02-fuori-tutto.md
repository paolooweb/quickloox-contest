---
title: "Fuori tutto"
date: 2019-06-02
comments: true
tags: [iTunes, Back, MAC, macOS, ]
---
Meglio abituarsi a un momento di consolidamento invece che di crescita impetuosa e sorprendente, cui la tecnologia ci ha abituato fin troppo bene per molti anni.

Almeno, se fosse vero che [sparisce iTunes](https://www.bloomberg.com/news/articles/2019-05-31/apple-s-future-ios-13-macos-10-15-watchos-6-tvos-13-mac-pro), come si vocifera. Ammesso e non concesso, la vita dopo iTunes sarà probabilmente più semplice. La perdita della familiarità con l’oggetto la sentiremo però tutti.

Mica è finita: da luglio [ci dimenticheremo di Torna al mio Mac](https://support.apple.com/en-us/HT208922), che smetterà di funzionare. Personalmente me ne ero dimenticato già da un bel pezzo, ma non si può mai dire.

Da ieri [non c’è più BlackBerry Messenger](https://www.engadget.com/2019/05/31/blackberry-messenger-shuts-down/), il sistema di posta usato con più sufficienza al mondo. Dirigenti e quadri ti dicevano che era impossibile fare senza BlackBerry e vantavano la presenza della tastiera fisica.

Non so che nuovo stia arrivando; di certo se ne va un bel po’ di vecchio.
