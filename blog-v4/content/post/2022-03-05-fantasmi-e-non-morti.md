---
title: "Fantasmi e non morti"
date: 2022-03-05T02:01:57+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [Ghost, WordPress, Matt Birchler, Birchtree]
---
Questo blog non è nato per guadagnare denaro e continuerà a vivere senza questa preoccupazione.

Altre persone, legittimamente e diversamente, tengono un blog come parte della loro vita professionale e come strumento per creare reddito. Una di queste è Matt Birchler con Birchtree, che scrive [Amo Ghost](https://birchtree.me/blog/i-love-ghost/):

> Ho spostato Birchtree da WordPress a Ghost in dicembre 2019 e dopo due anni sono eccezionalmente felice della mia decisione. Non ho un grosso articolo da scrivere per sostenere la mia tesi e non penso che Ghost sia la scelta giusta per tutti, ma volevo parlarne perché mi piace sostenere il software da cui traggo valore nella mia vita.

Birchler passa a sostenere le ragioni a favore di [Ghost](https://ghost.org) e anche quelle contro. Tutto è veloce e fluido, anche se l’interfaccia di gestione su iPad e iPhone non è ottimale. C’è un problema di mappatura nelle combinazioni di tastiera; in compenso non c’è bisogno di plugin esterni per avere una buona esperienza. Eccetera.

Il discorso dei plugin esterni segna una differenza sostanziale con [WordPress](https://wordpress.com), che al contrario di Ghost vive di plugin e sembra che non possa farne a meno. In un certo senso, WordPress è fintamente gratuito perché comunque un plugin si finisce sempre per acquistarlo e, quando è gratuito, in genere si spende una cifra equivalente in farmaci contro l’emicrania.

Ghost non è fintamente gratuito. È a pagamento e neanche costa pochissimo. Non esiste un livello gratis, solo una normale prova di due settimane.

Anche questo è il motivo per cui non è per tutti. Mai penserei di alimentare il mio blogging con un software a pagamento.

Se però dovessi costruire professionalmente un sito per qualcuno, integrato con la newsletter e dotato di tutti i comfort moderni, probabilmente penserei molto prima a qualcosa come Ghost che a qualcosa di gratuito. Perché mi aspetto supporto e aggiornamento, prestazioni, cura.

Per queste ragioni, massimo rispetto per chi sceglie WordPress e lo usa in forma gratuita per bloggare o per costruire siti personali.

Massimo rispetto anche per chi costruisce professionalmente siti con WordPress. Ma mi si alza il sopracciglio. Faccio campagna per il software libero e cerco di distinguere bene la parte *libero* dalla parte *gratis*; le persone e le organizzazioni intelligenti riconoscono al software libero quello che reputano giusto, in collaborazione quando non in denaro.

WordPress non è libero, è solo gratuito. Se Ghost si ispira ai fantasmi, esseri la cui azione si percepisce molto bene pur essendo essi impalpabili, WordPress mi fa l’effetto del non morto: si muove lentamente, non si ferma davanti a nulla, con la forza del numero travolge qualsiasi cosa davanti a sé.