---
title: "Been There, Said That"
date: 2019-04-07
comments: true
tags: [Morrick, Riccardo, Entertainment, Services]
---
**Riccardo** ha scritto sull’evento Apple ultimo scorso una straordinaria [riflessione](http://morrick.me/archives/8422), per me eccessivamente critica, tuttavia assolutamente da leggere parola per parola.

L’unica cosa è il titolo: una settimana prima di lui ho scritto che chiamarla Apple Entertainment [era una cosa superata](http://www.macintelligence.org/blog/2019/03/26/chi-si-distingue-e-bravo/). Non voglio cambiare per non sembrare imbarazzato o non convinto; analogamente potrebbe sembrare polemica a chi trascurasse le date di pubblicazione e spero non succederà.

La parte positiva di tutto ciò sono le sue argomentazioni. Attualmente lo ritemgo come valore tra i primi venti blogger Apple mondiali. Va letto, seguito e sostenuto.

(Anche se lui non legge me; avrebbe potuto cambiare titolo, mannaggia).
