---
title: "Temi dimenticati"
date: 2016-03-26
comments: true
tags: [Apple, CareKit, Fbi, ResearchKit, privacy]
---
Prima che venissero presentati iPhone SE e iPad Pro, l’[evento Apple di lunedì scorso](http://www.apple.com/apple-events/march-2016/) ha dedicato spazio ingente al [lavoro di Apple sulle energie rinnovabili](http://www.apple.com/it/environment/), all’annuncio di [CareKit in affiancamento a ResearchKit](http://www.apple.com/it/researchkit/) e a qualcosa cui non siamo proprio abituati, in Italia.<!--more-->

L’amministratore delegato di una delle maggiori potenze economiche della prima economia mondiale, ripreso in occasione pubblica e diffuso in tutto il mondo, ha pronunciato la seguente frase:

>Quanto potere dovrebbe avere il governo sui nostri dati? Sulla nostra privacy?

Sono lontanissimo dall’ambientalismo d’accatto, ma consapevole dell’importanza delle fonti rinnovabili. E mi sembra un tema decisivo per i prossimi anni, e decenni.

Una soluzione *open source* aperta a chiunque, che permette di abbattere la fatica di organizzare programmi di ricerca e monitoraggio in medicina, contemporaneamente moltiplicando il volume dei risultati ottenibili, è qualcosa di significativo.

Apple sta sfidando l’Fbi e lo stesso governo americano in nome della nostra possibilità di avere un iPhone con dati inviolabili a bordo, senza il nostro consenso. D’accordo che viviamo nel paese (minuscolo) dove *privacy* significa *perdi tempo a fare clic su una casella priva di qualunque significato*; tuttavia questa battaglia è veramente decisiva per il futuro delle libertà individuali.

Tre argomenti fondamentali, o almeno molto importanti. Nessuno se li è filati. Tutti ad almanaccare sulla mancanza di innovazione, sul declino di Apple, non è più come una volta.

Un’azienda in declino difficilmente si spreca per arrivare al 100 percento di consumo da energia rinnovabile, obiettivo che ha costi vertiginosi da sostenere per essere raggiunto. Vale la pena di riguardare con attenzione quei primi ventiquattro minuti dell’evento.