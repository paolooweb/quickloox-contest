---
title: "Ragioni per non comprare Apple"
date: 2013-07-14
comments: true
tags: [Apple]
---
Perché, per esempio, un iPad costa più di un Surface RT Microsoft. La ragione è che Microsoft [preparerebbe un taglio di 150 dollari dei prezzi](http://www.theverge.com/2013/7/11/4514888/microsoft-surface-rt-tablet-price-cuts) della propria tavoletta in edizione economica. Invece che ostinarsi a cercare una cosa apprezzata da tanti, meglio optare per qualcosa che pochissimi vogliono. O forse mi sbaglio e le vendite di Surface RT sono così immense che l’azienda può ridurre di un terzo il prezzo del modello base e guadagnarci ugualmente il desiderato.<!--more-->

Un’altra ragione: se sei proprietario di un telefono Samsung, può venirti talmente voglia di un iPhone da [rubarlo e lasciare sul luogo del furto il tuo Galaxy](http://www.washingtonpost.com/local/robber-leaves-own-phone-at-scene-of-iphone-thefts-police/2013/07/11/beeea85c-ea3d-11e2-a301-ea5a8116d211_story.html). Massimo rendimento con il minimo sforzo, polizia permettendo.

Infine, la cosa più ovvia: sui Mac non c’è un filtro antiporno. Apple è stata [portata in tribunale](http://abovethelaw.com/2013/07/lawyer-apple-should-protect-me-from-my-porn-addiction/) da un tale che ha guardato talmente tanta pornografia su Internet da non accorgersi più della differenza tra lo schermo e la realtà, a grave detrimento del proprio matrimonio.

**Aggiornamento**: la licenza del tale, avvocato, è stata [disattivata per disabilità](http://www.tbpr.org/NewsAndPublications/Releases/Pdfs/Sevier%202089-5.634716349183682622.pdf) e gli impedisce di praticare la professione fino a revoca del provvedimento.

Ma sarà stato ancora capace di distinguere tra un Mac e un PC? Il nocciolo della questione è lì.
