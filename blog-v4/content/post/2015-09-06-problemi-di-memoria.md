---
title: "Problemi di memoria"
date: 2015-09-06
comments: true
tags: [Starcom, Machinima, Microsoft, Xbox, YouTube, Ftc]
---
Nel 2013 il gruppo [Starcom MediaVest](http://www.smvgroup.com/preview/) lavorava a iniziative *marketing* per conto di Microsoft allo scopo di promuovere Xbox One, scrive [ItWorld](http://www.itworld.com/article/2979700/paid-endorsements-get-xbox-one-marketer-in-trouble-with-ftc.html).

Una delle attività consisteva in recensioni video pagate. Venne scelta la rete specializzata [Machinima](http://www.machinima.com), che corrispose un totale di 45 mila dollari a due recensori per produrre video su YouTube, e a un gruppo più ampio in base agli ascolti: un dollaro per ogni mille pagine viste, fino a un massimo di 25 mila dollari.

Secondo la legge americana, in questi casi il lettore/spettatore/navigatore deve essere informato che le recensioni sono a pagamento.

Un’inchiesta della Federal Trade Commission ha stabilito che lungo la catena del denaro tutti, dagli autori fino a Microsoft, si sono dimenticati di farlo.

*No problem*: la conclusione dell’inchiesta è che si tratta di *incidenti isolati*. Microsoft e Starcom hanno smesso rapidamente di erogare soldi a Machinima. La quale nel 2014 ha effettuato un repulisti, per cui i responsabili attuali dell’azienda non erano presenti all‘epoca dei fatti. Insomma, un peccato veniale da perdonare e celermente rimuovere.

I due recensori hanno generato da soli circa un milione di pagine viste. Machinima aveva garantito a Starcom un totale minimo di 19 milioni di pagine viste per l’intera operazione. C’è da chiedersi quante di queste abbiano contribuito ad aumentare le vendite di Xbox.

Nonostante gli evidenti problemi di memoria osservabili nella tecnologia Microsoft.