---
title: "Azioni che contano"
date: 2014-08-25
comments: true
tags: [Jobs, Mac, Parc, Xerox]
---
È salita alla ribalta una [tavola rotonda di commemorazione di Steve Jobs del 2011](https://www.youtube.com/watch?v=N2C2oCsrqcM), nella quale viene spiegato una volta di più che Xerox contattò Apple per faccende biecamente commerciali e cammin facendo si stipulò un accordo per la quale Apple vendeva proprie azioni a Xerox a un prezzo di assoluto favore e questa in cambio autorizzava le perlustrazioni di Apple dentro i laboratori del Palo Alto Research Center, che molto hanno contribuito alla nascita dell’interfaccia grafica moderna.<!--more-->

Azioni in cambio di accesso alle tecnologie. Non copia di nascosto o violazione di *copyright*. A beneficio del prossimo che ordina un caffè macchiato e sputa la sua sentenza preferita.

<iframe width="650" height="488" src="https://www.youtube.com/embed/N2C2oCsrqcM" frameborder="0" allowfullscreen></iframe>