---
title: "Dare il sangue per un gioco"
date: 2016-07-07
comments: true
tags: [Garriott, British, Shroud, Avatar]
---
Non posso dubitare dell’auterevolezza di *Ars Technica* anche se la pagina relativa eBay non esiste più; devo quindi dare per vero che Richard Garriott abbia [venduto all’asta quadri contenenti il suo sangue](http://arstechnica.com/gaming/2016/07/finally-you-can-buy-richard-garriotts-blood/), allo scopo di raccogliere fondi per finanziare il suo gioco di ruolo [Shroud of the Avatar](https://www.shroudoftheavatar.com).<!--more-->

L’occasione di rivedere all’opera l’autore della serie *Ultima*, che ha fatto furore dai tempi di Apple ][ è senza prezzo. Tuttavia sosterrò lo sforzo in modo più tradizionale, facendo una visita sul sito.

*[Akko non ha versato sangue per lanciare il suo [progetto editoriale che va oltre il libro](https://macintelligence.org/posts/2016-06-15-un-kickstarter-per-cambiare-il-libro/). Però è un’idea. Gliene parlo per il [Kickstater dedicato](https://www.kickstarter.com/projects/295108589/cuore-di-mela-i-segreti-di-apple-iphone-e-mac).]*