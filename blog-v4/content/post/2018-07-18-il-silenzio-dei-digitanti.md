---
title: "Il silenzio dei digitanti"
date: 2018-07-18
comments: true
tags: [MacBook, Pro]
---
Questa cosa delle tastiere dei MacBook Pro mi sembra definitivamente scappata di mano, o di polpastrelli.

Con le nuove configurazioni è arrivata anche una nuova versione della tastiera. E tutti a [verificare se veramente è più silenziosa delle precedenti](https://www.macrumors.com/2018/07/13/2018-macbook-pro-quieter-keyboard-demoed/).

Le precedenti tastiere si guastavano più frequentamente dei modelli prima di loro, sicuro. Certamente non è la maggioranza delle tastiere, quella che si guasta. I guasti sono più frequenti, ma la grande maggioranza delle tastiere funziona. Altrimenti Apple non si dedicherebbe a ridurre il rumore, ma vedrebbe di farle funzionare.

Notare come tutti si esprimano riguardo alla rumorosità ma stranamente non sappiano dire se in qualche modo i problemi di guasti siano stati affrontati. Ma le tastiere non si guastavano? Se le usate per ascoltarne il rumore, e sono tastiere che si guastano, dovrebbero guastarsi, no?

Gli assalti alla logica e al buonsenso sconfinano nella farsa. Apple sta affrontando alcune *class action* intentate per spillarle soldi proprio a causa delle tastiere e ho letto che potrebbe essere intervenuta sulle nuove tastiere proprio per ridurre l’incidenza dei guasti, evitando di dirlo in pubblico per non complicare il quadro legale e adottando invece il pretesto della rumorosità ridotta.

Non so se sia vero. Però il pensiero di tutti i cacciatori di scandalo con l’orecchio alla tastiera per misurare il rumore, quando invece è stata modificata per non guastarsi, mi fa prudere i polpastrelli dal ridere.