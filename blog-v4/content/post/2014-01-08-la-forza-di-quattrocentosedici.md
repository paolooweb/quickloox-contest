---
title: "La forza di quattrocentosedici"
date: 2014-01-09
comments: true
tags: [Mac, MacPaint, Motorola, 68000]
---
Mancano pochi giorni al trentesimo compleanno di Macintosh, che ricorre il 24 gennaio prossimo. Cade a proposito questo [articolo di Looks Good, Works Well](http://www.looksgoodworkswell.com/elegance-of-macpaint-code/), soluzione a un problema che nessuno si era posto e cioè come mai la larghezza della tela virtuale su cui disegnare con il primo MacPaint era esattamente di 416 pixel.<!--more-->

In informatica i problemi inutili sono i migliori perché la soluzione è tipicamente elegante. In questo caso la risposta è una istruzione per governare il processore Motorola 68000, allora una novità, preziosissima per spostare rapidamente di qua e di là pixel in un programma di grafica.

Quattro bit di quei tempi facevano un *nibble*. Otto bit facevano un *byte* (e un carattere sulla tastiera). Sedici bit, una *word*. Trentadue, una *long word*.

I processori spostano i bit tra contenitori che si chiamano *registri*. Il processore di Macintosh poteva destinare a quella certa istruzione tredici registri da una *long word* ciascuna. Trentadue per tredici, quattrocentosedici.

Una immagine nell’articolo mostra la raffinata interfaccia di MacPaint, che nel 1984 sembrava il prodotto di una civiltà aliena, avanti di secoli.

Quando si parla di integrazione tra hardware e software nel caso di Mac, non sono chiacchiere. MacPaint era tagliato sartorialmente sul processore.

O viceversa.