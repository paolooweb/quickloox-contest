---
title: "La stampante che non c’è"
date: 2021-03-19T02:44:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [HP, M118dw, HP Smart] 
---
La scuola a casa ci ha consigliato di [tornare indietro di una decina di anni](https://macintelligence.org/posts/Il-pagamento-degli-arretrati.html) per riaccogliere a casa una stampante. Viste però le necessità effettive, abbiamo optato per una stampante laser moncromatica: una [HP LaserJet Pro M118dw](https://store.hp.com/ItalyStore/Merch/Product.aspx?id=4PA39A&opt=B19&sel=DEF&gclid=CjwKCAjw9MuCBhBUEiwAbDZ-7m27IkrVKPyc-b9uaEuc5OkYg8rHU-t2RLaWJATEeD8miMJO4Ed7oBoC3GYQAvD_BwE&gclsrc=aw.ds).

(Per **Fëarandil**: l’offerta su questo modello era buona e mi sa che è molto simile a quello che avevi consigliato tu e avevo cercato in prima battuta).

Non frequentavo stampanti da un bel po’ e immagino che le mie considerazioni siano naïf per chi invece ci convive da sempre. Chiedo pazienza: non ero più abituato ad avere in casa qualcosa che fa rumore per accendersi.

L’*unpacking* è stato senza brividi, l’oggetto è esteticamente neutro ma in senso positivo, ragionevolmente leggero, ragionevolmente ingombrante.

La configurazione dell’hardware è stata semplice. Il toner di serie è già dentro la stampante e, anzi, il foglio delle istruzioni avvisa di non levarlo. Prima (gradita) sorpresa per un neo-neofita: ero ampiamente consapevole dell’esistenza di stampanti che vanno via Wi-Fi. Nondimeno, avendone una sottomano, ho provato a ignorare il cavo Usb allegato nella confezione e contemplare un uso esclusivamente wireless.

Funziona. La configurazione del Wi-Fi è un po’ macchinosa e non chiarissima a livello di *for dummies*. Si supera però facilmente e nel giro di una quindicina di minuti due Mac, due iPhone e un iPad Pro avevano accesso alla stampa. Anche questo per me è una novità: mi ero sempre figurato la stampa da iPad come un mascellodonte o un sarchiapone, creature mitiche di un tempo altro.

Come quelli che vengono giù dalle montagne mi ero anche preoccupato della ricerca del driver per Big Sur. Del tutto inutile: AirPrint è comodissimo e non mi sono neanche preoccupato troppo di quali opzioni di stampa avere a disposizione: una laser monocromatica stampa in monocromatico e per i nostri bisogni è financo esagerata. Come stampa, stampa bene.

Il prezzo da pagare è lo scaricamento della app HP Smart sugli apparecchi interessati e un account sul sito HP.

Qui mi sono reso conto della cosa che è veramente, sonoramente cambiata dai tempi in cui stampavo. *La stampante non esiste*.

Meglio: non è più il centro dell’attenzione. È il pretesto per costruire un business sulla profilazione di chi ha bisogno di produrre dei fogli di carta con sopra del contenuto.

La app è un compito ben eseguito, parlando di scuola. Niente voli di fantasia o lampi creativi, ma quello che c’è da fare si fa velocemente e con chiarezza. Ho apprezzato molto il poter dare un nome alla stampante, un dettaglio, però la fa sentire più di casa.

Il sito e la gestione dell’account da parte di HP sono di seconda fascia. L’esperienza funziona, ci si orienta, ha un senso. Però il trattamento da parte di una Google o una Apple è percettibilmente superiore. La mia stampante non figura nel database del [sito di ingresso di HP](https://123.hp.com/us/en/) e la cosa fa un po’ ridere. Nel database i nomi dei modelli sono inseriti completi: *HP LaserJet Pro M118dw*. Si capisce la logica aziendale, ma c’è un sacco di inutilità evidente. Io avrò iniziato a cercare scrivendo *HP LaserJet* oppure *M118dw*?

Secondo, si sappia: comprare una stampante HP nel 2021 deve essere un massacro della privacy. La procedura di registrazione ti chiede il cellulare. Il software di registrazione, quantomeno viene detto con chiarezza, si appoggia a Google Analytics. Se vuoi impostare la stampa in modalità wireless, in un modo o nell’altro concedi un permesso almeno parziale di conoscere la tua posizione, perché aiuta nella gestione della rete Wi-Fi. Onestamente preferisco la comodità dell’assenza di cavo alla problematica della geolocalizzazione (anche perché in fase di registrazione chiedono un indirizzo e allora tanto vale).

Cosa sciocchina: non è prevista la condivisione di una stampante da parte di più account HP. Una stampante non è esattamente un oggetto individuale, in famiglia, in ufficio, a scuola. Vuoi profilare, almeno fallo bene.

Cosa che non ho ancora collaudato: la stampante ha un indirizzo email ed è possibile stampare scrivendole un messaggio di posta. Un sistema di permessi lo consente, volendo, a chiunque, oppure a una *whitelist* di persone autorizzate, oppure a tutti esclusa una *blacklist*. Esiste da molto tempo, ma è la prima volta che posso toccare con mano e devo ancora verificare che funzioni.

Sul prezzo c’è poco da dire: può variare di qualche euro secondo il momento e il canale di vendita. L’ordine di grandezza è equo e onestamente poter avere stampa laser fronte/retro con buona velocità in un ordigno che sta comodamente a lato scrivania è una testimonianza di quanto la tecnologia sia progredita. Un tempo trecento punti per pollice sembravano il trionfo della tipografia. Oggi a poco più di cento euro se ne fanno milleduecento senza neanche stare a pensarci.

Resta il fatto che, se questa fosse una recensione, lo sarebbe non di una stampante, ma di un account HP. La stampante è diventata un tramite e niente più, un gradino sotto l’elettrodomestico. Segno dei tempi.

*Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile.*