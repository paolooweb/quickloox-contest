---
title: "OK, il prezzo è interessante"
date: 2017-03-18
comments: true
tags: [AirPods, MacBook, Pro]
---
Neil Cybart di *Above Avalon* mostra che se uno si mette sul mercato alla ricerca di auricolari wireless perché vuole computer da orecchio, e wireless, più che essere un purista del suono, gli [AirPods](http://www.apple.com/it/airpods/) sono [la scelta più economica](https://www.aboveavalon.com/notes/2017/3/15/the-curious-state-of-apple-product-pricing), dopo almeno otto prodotti concorrenti nettamente più costosi.<!--more-->

Dopo di che fa vedere come [watch](http://www.apple.com/it/watch/) sia il più economico dei computer da polso, con almeno altri cinque concorrenti che hanno prezzo di ingresso evidentemente più elevato.

Non contento, analizza le mosse di prezzo di Apple, che considera una ridefinizione del concetto di lusso, con linee a prezzo molto alto affiancate ad altre che sembrano costose e invece sono, appunto, totalmente competitive.

L’analisi è suggestiva e totalmente consigliata. Vedo personalmente le cose in modo un pochino più vecchio stile e ritengo che Apple applichi una chiara distinzione di strategie tra quello che si mette in borsa, o in tasca, e quello che ci si mette addosso. Poi c’entrano anche le economie di scala e una capacità di produzione tecnologicamente avanzata che, dopo anni e miliardi di investimenti, forse ha guadagnato un vantaggio percepibile sulle catene di produzione concorrenti.

Di sicuro associare automaticamente Apple a prezzi alti, se già è stato vero assai parzialmente, lo è sempre meno e sono ansioso di sentire quelli che hanno passato una vita a spiegarci che bisognava comprare quello che costa meno, perché tutti i computer sono uguali.