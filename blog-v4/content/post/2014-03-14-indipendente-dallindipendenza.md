---
title: "Indipendente dall'indipendenza"
date: 2014-03-16
comments: true
tags: [4k, AnandTech, OSX, MacPro, risoluzione]
---
Se avessi avuto un centesimo per tutti gli articoli che ho letto in passato sullo sviluppo di un OS X indipendente dalla risoluzione degli schermi, sarei ricco. Invece l’evoluzione delle prestazioni va verso gli schermi giganteschi, la cosiddetta risoluzione 4k, con un sistema più che mai *dipendente* dalla risoluzione.<!--more-->

Nel senso che la calcola e, come [racconta per esempio AnandTech](http://www.anandtech.com/show/7847/improving-the-state-of-4k-display-support-under-os-x), si adegua: i dati vengono mostrati a risoluzione altissima con pixel piccolissimi e nel contempo l’interfaccia utente viene resa a definizione perfetta ma in ingrandimento, come se la risoluzione stessa fosse inferiore e così assicurando una perfetta leggibilità.

L’idea di un sistema dal comportamento per così dire organico, che propone una schermata sempre ergonomica lavorando a prescindere dal numero preciso di pixel a disposizione, resta molto più affascinante ma a conti fatti fantascientifica. Come minimo spostata verso un futuro ben più lontano di quello immediato. La realtà, nel frattempo, è più banale e nel contempo assolutamente efficiente (e con un Mac Pro si fanno meraviglie).

Se avessi un centesimo per ogni articolo che ho letto sui ritardi di Apple nel realizzare un’interfaccia indipendente dalla risoluzione…