---
title: "Babbo digitale"
date: 2018-12-26
comments: true
tags: [Lambda, Lisp, Natale]
---
Colgo l’occasione per ringraziare Babbo Natale, quest’anno veramente generoso su tutti i fronti. Limitatamente alla *computer science* ho avuto il gran piacere di ricevere [Let Over Lambda](https://letoverlambda.com/), libro *hard* su Common Lisp che si propone nientemeno di *spostare i confini di ciò che sappiamo sulla programmazione* e in particolare su Common Lisp, visto attraverso le sue capacità di creare macroistruzioni.

È un testo vastamente superiore alle mie capacità di comprenderlo. Contemporaneamente so per esperienza che, con questo approccio, mi resterà certamente attaccato qualcosa di utile ben oltre le mie conoscenze attuali. Il massimo che posso sperare, quindi ottimo regalo.

