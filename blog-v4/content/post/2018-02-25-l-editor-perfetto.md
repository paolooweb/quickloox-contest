---
title: "L’editor perfetto"
date: 2018-02-25
comments: true
tags: [BBEdit]
---
Dalle [note di pubblicazione](http://www.barebones.com/support/bbedit/notes-12.1.html) sappiamo che BBEdit 12.1 ora è un’applicazione a sessantaquattro bit.<!--more-->

Il cambiamento è necessario nel medio termine perché macOS si prepara, come già iOS, a disconoscere le operazioni a trentadue bit. E porta una serie di effetti collaterali assai piacevoli. Per esempio, la dimensione dei file che è possibile aprire aumenta drasticamente, dove prima dava problemi attorno al giga e mezzo; lo stesso vale per situazioni di codice molto intricato strutturalmente o formalmente.

Più vado avanti con BBEdit, più sono convinto che sia il mio editor definitivo. Non lo faranno mai, ma se solo facessero l’edizione iOS, sarebbe anche quello perfetto.