---
title: "Fuori sintonia"
date: 2021-04-29T01:17:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [The Briefing, Apple, Philip Elmer-DeWitt, The Information] 
---
Si scriveva pochi giorni fa della [newsletter informativa](https://macintelligence.org/posts/Prepararsi-al-meglio.html) che invitava i lettori a *restare sintonizzati* in attesa dei risultati finanziari di Apple, *che ha performato in modo mediocre come crescita negli ultimi anni*.

Premesso che il trimestre invernale Apple è normalmente il penultimo dell’anno per risultati, [la crescita del fatturato anno su anno è stata del 54 percento](https://sixcolors.com/post/2021/04/apples-record-second-quarter-in-charts/); il terzo migliore di sempre, il primo di sempre per il periodo gennaio-marzo.

Il fatturato dei Mac – purtroppo non ci sono più le unità vendute – è cresciuto del settanta percento, quello di iPad del settantanove percento, iPhone sopra il sessanta percento. Apple mai ha guadagnato tanto come in questo trimestre, e in quello scorso.

Numeri da paura, molto sopra la media del settore.

Non si sono mai venduti così tanti Mac, per una quota di fatturato che sfiora i nove miliardi. Anche iPad è in serie positiva con crescita forte e sostenuta da diversi trimestri.

Dica ora il candidato se, avendo due macchine da soldi come queste, a una qualunque persona di buonsenso potrebbe mai venire in mente di fonderli in una linea di prodotto unica, o di unificare il sistema operativo.

Eppure se ne parla in continuazione. Da parte di gente certamente poco sintonizzata non tanto con la realtà, ma neanche con il realismo.

*Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile.*