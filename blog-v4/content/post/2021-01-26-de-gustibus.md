---
title: "De gustibus"
date: 2021-01-26T00:31:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [Steve Jobs, Zune, The Verge, Apple, Microsoft, iPod, Bing, Cortana, Windows Phone, Reddit] 
---
*The Verge* pubblica un articolo dedicato ai [retroappassionati di Zune radunati su Reddit](https://www.theverge.com/22238668/microsoft-zune-fans-mp3-music-player-subreddit) e riporta questo passaggio:

>Microsoft ha trascorso gli anni Duemila qualche passo dietro Apple. L’azienda rivale era perennemente un po’ più avanti, elegante e raffinata rispetto all’endemica squadratura di casa Gates. Zune veniva spesso considerato l’esempio supremo di questa subalternità. Il prodotto era del tutto funzionale, certo, ma per ragioni che resta difficile descrivere – le decorazioni da videogame, il trackpad surdimensionato, l’ingombro sconcertante – era anche un milione di volte meno chic di iPod. (Lo stesso imperscrutabile problema contraddistingue Bing, Cortana, lo sfortunato Windows Phone eccetera). *[Traduzione migliorata con il contributo di **carolus**]*

*Ragioni che resta difficile descrivere*. *Problema imperscrutabile*. Come se Steve Jobs non fosse mai passato di qui. [Eccolo](https://timesofindia.indiatimes.com/gadgets-news/why-steve-jobs-thought-microsoft-was-like-mcdonalds/articleshow/70869096.cms):

>Il solo problema di Microsoft è la semplice mancanza di gusto. Assolutamente non hanno gusto. Non in piccolo, ma in grande, nel senso che non pensano idee originali e non portano molta cultura dentro i loro prodotti.

La differenza tra una mentalità Microsoft e una mentalità Apple è che nella prima tutti i gusti sono giusti. Peggio ancora, sono equivalenti. Nella seconda esistono il gusto e la sua mancanza. Per ragioni che resta difficile articolare, l’informatica continua a prosperare nella mancanza di gusto; per fortuna, abbiamo un’eccezione.

*Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile.*