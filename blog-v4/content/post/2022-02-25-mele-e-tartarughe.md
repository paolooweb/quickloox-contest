---
title: "Mele e tartarughe"
date: 2022-02-25T01:53:21+01:00
draft: false
toc: false
comments: true
categories: [Hardware]
tags: [Intel, Apple, Arrow Lake, M1, John Gruber]
---
È con genuina meraviglia che si può leggere su *AppleInsider* [Piani trapelati mostrano che Intel cercherà di essere più efficiente di M1 entro fine 2023](https://appleinsider.com/articles/22/02/24/leaked-plan-shows-intel-will-try-to-be-more-efficient-than-m1-max-by-late-2023).

Per prima cosa, è meraviglioso come i piani futuri di Intel sino così poco gelosamente custoditi e trapelino fuori dall’azienda neanche ci fosse un esercito di ventole a spingerli.

Poi, il concetto: Intel intende *battere in efficienza il processore Apple del 2021 per fine 2023* (o inizio 2024, precisa l’articolo e si sa come vanno a finire certe tempistiche). Come dire *via, abbiamo solo due anni di ritardo, che vuoi che siano?*

Questo presumendo, naturalmente, che Apple in questi due anni resti ferma. Oppure che si muova, ma più lentamente di Intel. L’annuncio mascherato da fuga di notizie potrebbe anche significare *siamo due anni in ritardo e andrà sempre peggio* oppure *chissà per quanto tempo ancora resteremo due anni indietro*. Sono prospettive affascinanti.

Tutto questo, inoltre, è sulla carta. Il [commento migliore](https://daringfireball.net/linked/2022/02/24/intel-plans) è quello di John Gruber:

> La concorrenza è una cosa buona e, ora che Apple ha alzato l’asticella per l’efficienza a livello di prestazioni per watt sui desktop, è naturale che ogni produttore di chip cerchi di rimontare. Sarebbe scioccante che Intel non avesse un piano per rimontare.

> È che Intel aveva piani per passare alla produzione di chip con tecnologia a dieci nanometri, e anche a sette nanometri, che non hanno funzionato. Pianificare è facile; eseguire è difficile.

Buon inseguimento, Intel. La mia scrivania in questo momento è più veloce.
