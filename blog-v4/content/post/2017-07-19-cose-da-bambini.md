---
title: "Cose da bambini"
date: 2017-07-19
comments: true
tags: [ iPad, Windows, Phone, Htc]
---
Leggo che Windows Phone avrebbe costituito lo [0,1 percento](https://www.recode.net/2017/7/17/15984222/microsoft-windows-phone-mobile-operating-system-android-iphone-ios) dei sistemi da tasca venduti nel primo trimestre dell’anno. Insomma è nullificato, solo che per cortesia lo si tratta ancora da pari. (No, non [supererà iPhone entro il 2015](https://www.neowin.net/news/idc-predicts-wp7-to-overtake-iphone-by-2015)).<!--more-->

Leggo poi che su *alcuni* (inconveniente, come da versione ufficiale, o esperimento?) sistemi Htc era montata una tastiera che [riempiva lo schermo di messaggi pubblicitari e chiedeva soldi per smettere](https://www.androidcentral.com/htctouchpal-keyboard-ads-likely-error-damage-already-done). Va bene costare poco, va bene accontentarsi del peggio, va bene l'utente considerato merce per l’inserzionista, ma forse si è superata la decenza.

Come si fa a veicolare prodotti che possano avere un successo di pubblico e contemporaneamente, quel pubblico rispettarlo?

Una via potrebbe essere pensare ad apparecchi che un ospedale potrebbe pensare di usare per [tenere in contatto mamme con bambini in isolamento](https://www.cedars-sinai.edu/About-Us/HH-Landing-Pages/iPads-Help-New-Moms-Connect-With-Their-Infants-in-the-Neonatal-Intensive-Care-Unit.aspx). Come linea di rispetto e utilità mi sembra accettabile.
