---
title: "Il mondo (anche) alla rovescia"
date: 2013-12-09
comments: true
tags: [Usb, Lightning]
---
La prossima rivoluzione di Usb: un nuovo connettore, di nome *Type-C*.<!--more-->

La specifica verrà completata per metà 2014 e il primo prodotto, [afferma la Bbc](http://www.bbc.co.uk/news/technology-25222101), arriverà sul mercato per il 2016.

Sarà molto stretto, molto più sottile e potrà essere inserito in qualunque verso, senza timore di doverlo ruotare di 180 gradi.

Potevano anche chiamarlo *Copia-Di-Lightning*. Dopotutto per il 2016 Apple potrebbe perfino essere avanti di una generazione ancora.

Se penso che [due mesi](https://macintelligence.org/posts/2013-09-08-e-lo-chiamano-micro/) fa si polemizzava sul connettore Micro Usb. Che a questo punto è già vecchio.