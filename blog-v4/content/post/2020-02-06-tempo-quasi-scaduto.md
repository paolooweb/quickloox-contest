---
title: "Tempo (quasi) scaduto"
date: 2020-02-06
comments: true
tags: [watch, Swatch, Tissot, Apple, Must, Tag, Heuer]
---
Così, se fosse vero, nel 2019 si sarebbero venduti [più di trenta milioni di watch contro meno di ventidue milioni di orologi svizzeri](https://www.applemust.com/apple-watch-outsold-swiss-watch-industry-in-2019-analyst/), nel senso letterale della definizione. Con il trend di watch in crescita del trentasei percento, e l’altro in calo del tredici percento.

Sicuramente il [segmento di lusso](https://www.ablogtowatch.com) non ha grossi problemi. Ma per una Swatch o una Tissot si ha l’impressione che stia per scadere, appunto, il tempo.