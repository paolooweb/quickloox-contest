---
title: "Uno su dieci non ce la fa"
date: 2018-04-14
comments: true
tags: [AppleCare, MacBook]
---
Cena fra dieci amici. È appena uscito il nuovo MacBook UltraPro.

Scenario uno: il MacBook UltraPro ha un difetto di produzione che affligge tutte le macchine.

Un amico: *Ho comprato il MacBook UltraPro e ho preso una fregatura. Il discombobulatore positronico non funziona. Non compratelo!*

L’amico ha reso un grande servizio agli altri.

Scenario due: il MacBook UltraPro ha un difetto di produzione che affligge una macchina su dieci.

Un amico: *Ho comprato il MacBook UltraPro e ho preso una fregatura. Il discombobulatore positronico non funziona. Non compratelo!*

L’amico ha reso un bel disservizio agli altri, il cui eventuale MacBook Ultra Pro ha ottime probabilità di funzionare. Tranne che per uno di essi, il quale dirà *Eh, avevi proprio ragione! Che fregatura. Lo dirò ad altri dieci amici…*

Morale: senza conoscere il quadro di insieme, è presuntuoso raccontare i propri problemi come se dovessero averli tutti.