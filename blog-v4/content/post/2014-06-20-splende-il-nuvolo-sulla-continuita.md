---
title: "Splende il nuvolo sulla continuità"
date: 2014-06-20
comments: true
tags: [iOS, OSX, Mavericks, Yosemite, iCloud]
---
L’anno scorso di questo periodo avevo Mavericks Developer Preview installato a mio rischio sul disco di lavoro. Non c’era vero rischio, solo una diffusa carenza di rifiniture e così facendo potevo lavorare anche ai [libri sul tema](http://www.lafeltrinelli.it/fcom/it/home/pages/catalogo/searchresults.html?aut=340115&canonical=false), con l’interfaccia sempre sottomano.<!--more-->

Quest’anno tutto si ripresenta identico con una eccezione: [Yosemite Developer Preview](https://developer.apple.com/osx/whats-new/) (2) è installato per ora su disco esterno.

Passo indietro? No, passo avanti in via di costruzione. Se opero da Yosemite, succede che tutto quanto subisce modifiche dentro iCloud resta per ora accessibile solo da Yosemite, e va bene. E *solo da iOS 8*.

In altre parole: se tocco un documento da Yosemite attraverso iCloud, lo posso aprire su iPhone o iPad unicamente se montano iOS 8.

Il quale si trova nello stesso stadio di lavori-in-corso di Yosemite. L’esperienza mi insegna che avere un sistema in versione provvisoria non è un grande problema, ma averne tre in contemporanea lo può diventare, e ostico.

Non ho bisogno di iOS 8 preliminare e quindi, restando sulla versione 7, dalle premesse si deduce che il mio disco di lavoro attuale su Mac resta Mavericks.

Tutto temporaneo e tutto benvenuto: lo stretto collegamento tra OS X, iOS, iCloud che si concretizzerà in autunno rappresenta una evoluzione importante.

L’anno scorso la iNuvola non costituiva un problema per lavorare sulle versioni provvisorie. Oggi che lo fa, le previsioni fanno tempo più che bello.