---
title: "Aprirsi al Lenovo"
date: 2016-02-05
comments: true
tags: [Lenovo]
---
Il tuo vicino di aereo si accomoda sul sedile di fianco e apre il suo portatile. Non ha prezzo.

 ![alt](/images/lenovo.jpg  "title") 