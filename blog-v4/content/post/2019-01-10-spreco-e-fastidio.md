---
title: "Spreco e fastidio"
date: 2019-01-10
comments: true
tags: [McDonald’s, Homebrew, Lisp]
---
Ho già parlato dell’assurdità di tanti codici numerici e alfanumerici [inutilmente obesi rispetto al compito che devono sopportare](http://www.apogeonline.com/webzine/2013/01/30/non-rimane-piu-niente-da-aggiungere), con spreco immane di banda, tempo macchina, archivi e pazienza di chi li deve subire.

Concludevo che i codici bisognerebbe lasciarli generare e maneggiare al computer, che con la complessità non ha problemi. E riportare la semplicità e la mnemonicità agli umani, che lavorano meglio e più contenti.

Ne ho avuto la riprova in una recente sosta a McDonald’s. Gli scontrini adesso portano un identificatore unico; quello del mio snack, salvo errori, è

`truw07ZuRah2bA1I+Z0YIP+DgbaJLbst1dkyUCH3rbw=`

Una entità spaventosa, incomprensibile per un umano. Quarantaquattro caratteri che possono essere maiuscoli, minuscoli, numerici o simbolici.

Per semplicità presumo che gli unici caratteri simbolici possibili siano quelli che vedo, *+* e *=*. Consapevole che questo tende a limitare il numero possibile di combinazioni.

Stimo dunque che ognuno dei quarantaquattro caratteri abbia 26+26+10+2 varianti possibili (ventisei maiuscole, altrettante minuscole, dieci cifre numeriche e due simboli): totale, sessantaquattro.

I codici possibili su uno scontrino di McDonald’s sarebbero di conseguenza sessantaquattro elevato alla quarantaquattresima potenza.

Lisp o Python non si spaventano di fronte al compito. (Python è già nel Terminale, basta scrivere `python` per entrare e `quit()` per uscire. Per elevare un numero a potenza si usa `**`. Common Lisp, *clisp*, lo installo con [Homebrew](https://brew.sh) (`brew install clisp`) e uso `expt` per l’elevazione a potenza. Come si esca da Common Lisp non lo scrivo perché è più interessante rimanervi).

Il risultato è

`29.642.774.844.752.946.028.434.172.162.224.104.410.437.116.074.403.984.394.101.141.506.025.761.187.823.616`

Capisco che McDonald’s generi un numero astronomico di scontrini, ma questo è *troppo* astronomico. È impossibile persino concepire un numero vicino ai trenta miliardi di miliardi di miliardi di miliardi di miliardi di miliardi di miliardi di miliardi. Se non ho sbagliato, già mi viene l’emicrania.

Eppure lo genera il computer. Sono fatti suoi. Ho interagito per curiosità e null’altro, niente di esso mi riguarda una volta abbandonato il locale. Uno spreco, ma non un fastidio.

Gli scontrini contengono anche un codice promozionale. Il mio è

`MXAE6IXMJ9RFZ`

Tredici caratteri, maiuscole o numeri, per trentasei combinazioni totali. Trentasei alla tredicesima potenza, bazzecole rispetto a prima. I codici possibili sono

`170.581.728.179.578.208.256`

Centosettanta miliardi di miliardi. Pochi o tanti? La catena di fast food ha smesso di contare gli hamburger serviti al clienti nel 1994, al momento di raggiungere i [cento miliardi dopo quarant’anni](http://overhowmanybillionserved.blogspot.com/2010/04/history-of-number.html).

Facciamo finta che siano raddoppiati dal 1994 a oggi. Duecento miliardi. Oltre agli hamburger McDonald’s vende infinite altre cose: facciamo duecentomila miliardi. È evidentemente una esagerazione.

Le promozioni *scadono*. E non ci sono sempre. Presumo comunque che, messe insieme tutte le filiali mondiali, ci siano mille promozioni l’anno. Diciamo che un codice resta valido per un anno, altra esagerazione, e valga ovunque sul pianeta, esagerazione al quadrato. Quanti scontrini si stampano in un anno? Approssimando sulle approssimazioni, pensando alla crescita della catena, considerando solo gli ultimi anni, potremmo dire… diecimila miliardi.

Spreco e anche fastidio. Quel codice lo devo digitare da qualche parte nel sito o nella app e fornisce *diciassette milioni di volte* la varietà che serve a coprire una versione assai fantascientifica, per gonfiaggio, del *business* di McDonald’s. Vuol dire che potrebbe essere ben più corto.

Si parla comunque di una attività planetaria, che ambisce idealmente a servire sette miliardi di persone.

Sai che il codice fiscale italiano, pensato per servire una platea cento volte minore, copre un terzo dello spazio combinatorio coperto dai codici promozionali di McDonald’s? E nella sua enormità è pure disfunzionale.

Spreco, fastidio e frustrazione.