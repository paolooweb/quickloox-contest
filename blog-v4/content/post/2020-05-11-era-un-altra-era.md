---
title: "Era un’altra èra"
date: 2020-05-11
comments: true
tags: [Pascal]
---
Undici fogli ingialliti, fotocopie di fotocopie di fotocopie, emersi senza motivo apparente da una cassa di libri. Vi si leggono frasi come queste:

>Non si deve assolutamente catalogare un file ogni volta che si scrive un nuovo programma. Il file è soltanto un’area di memoria che può contenere un certo numero di programmi. Si consiglia vivamente, quindi, una volta catalogato un file, di sfruttare completamente questa area di memoria. Ogni file può contenere diverse migliaia di elementi.

I caratteri sono monospaziati, le accentate sono rese con gli apostrofi.

Catalogare (salvare) un file da *poche migliaia di elementi* era un gesto impegnativo per le risorse a disposizione.

Oggi abbiamo a disposizione quella capacità moltiplicata milioni di volte, in casa e perfino in tasca, senza bisogno di raggiungere un laboratorio dedicato, sedersi davanti a un ingombrante terminale posto di trovarne uno libero, sprintare per utilizzare al meglio la singola ora a disposizione. Macintosh sarebbe arrivato pochi anni dopo. Chi oggi fa confronti con quell’epoca non vuole riconoscere come sia un’altra epoca, diversa, tanto quanto [Mesozoico](https://www.britannica.com/science/Mesozoic-Era) e [Cenozoico](https://www.livescience.com/40352-cenozoic-era.html).

>Le lezioni disponibili di aset riguardanti il PASCAL sono:  PASCAL.PASINTRO  
PASCAL.PAS0  
PASCAL.PAS1  
PASCAL.PAS2  
PASCAL.PAS3  
PASCAL.PAS4  
PASCAL.PAS5  
PASCAL.PAS6  
PASCAL.PAS7  
PASCAL.PAS8  
PASCAL.PASUW  
PASCAL.PASTEST  
Si consiglia di iniziare con PASCAL.PASINTRO. Per ulteriori informazioni consultare il manuale di aset contenhuto [sic] nel manuale disponibile accant0o [sic] ad ogni terminale.

Per quelli che la didattica a distanza l’hanno inventata le multinazionali avide per strappare il cuore alla scuola dell’insegnamento vero fatto con gli sguardi, con il corpo, con il feedback in tempo reale, con la lezione frontale: il docente spiegava il primo giorno di programmare un interprete Basic e presentarlo all’esame. Poi c’erano il libro di testo e le *lezioni di aset*. Non era proprio didattica *a distanza*: era il laboratorio dell’università. Il docente, comunque, era distantissimo, in quel momento e in tutti gli altri.

I primi laureati in Scienze dell’Informazione hanno cominciato così, darwinianamente. Ah, la seconda cosa che si imparava era come molestare un altro studente con una specie di [attacco DOS](https://www.us-cert.gov/ncas/tips/ST04-015) in sessantaquattresimo al suo terminale. Un uso contenuto della tecnica consentiva una forma primitiva di chat.

La terza cosa che si imparava era l’esistenza della directory dei giochi. Cose grosse, tipo *indovina il numero*.

Detto tra noi, piantarsi davanti alla telecamera e recitare la lezione non è didattica a distanza e non è neanche progresso. È lasciare gli studenti a sopravvivere da soli, quarant’anni dopo.