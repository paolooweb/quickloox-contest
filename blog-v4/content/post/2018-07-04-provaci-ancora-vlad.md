---
title: "Provaci ancora Vlad"
date: 2018-07-04
comments: true
tags: [phishing, Mail, Russia]
---
Caso vuole che sia entrato in iCloud via browser più o meno in coincidenza di tempi dichiarati. Così ci ho fatto attenzione, anche se poi il messaggio vorrebbe far pensare a un ingresso abusivo da parte di altri.

 ![Ci provano con il phishing](/images/phishing.png  "Ci provano con il phishing") 

Di norma i *phishing* di questo genere sono molto più buzzurri. Questo è ragionevolmente pulito e in un attimo di distrazione potrebbe persino sembrare vero. Ovviamente, come si vede dal link in anteprima cortesia di Mail, vero non è.