---
title: "Ora secondi a nessuno"
date: 2017-02-08
comments: true
tags: [watch, iPhone, Rolex]
---
Si parlava di iPhone 7 che, nonostante le critiche, [ha venduto molto bene](https://macintelligence.org/posts/2017-02-04-domanda-e-pretese/) (parlerò più avanti della questione dei record legati alla lunghezza del trimestre).

Ci sarebbe anche watch, sbertucciato perché non farebbe numeri importanti e non servirebbe ad alcuno, giusto per banalizzare due tra le critiche più comuni.

In realtà pare potrebbero vendersi [circa tanti watch quanti Mac](https://www.aboveavalon.com/notes/2017/1/30/apple-1q17-expectations). Secondo Tim Cook, quello natalizio è stato [il miglior trimestre di sempre per watch](http://www.imore.com/who-watches-apple-watch-watchers).

Per fatturato, watch [è secondo solo a Rolex](http://wornandwound.com/apple-second-rolex-worldwide-sales-mean/) nel mondo degli orologi.

Ha ancora senso parlare di *smartwatch*? Ai media piace inventare categorie e termini attorno a cui alzare polveroni, a partire dalla mitica quota di mercato. Nei fatti, vedo pochissime persone che portano al polso più di un apparecchio. Invece di parlare di *smart* o non *smart*, bisognerebbe fare il conto dei polsi.

Dove, a quanto sembra, watch ha pochi rivali.

Quanto alle critiche, un altro *tweet* sulla falsariga di quello di ieri.

<blockquote class="twitter-tweet" data-lang="en"><p lang="en" dir="ltr">Apple Watch had its best ever quarter. Really big ongoing disconnect between tech and normal user expectations &amp; satisfaction.</p>&mdash; Benedict Evans (@BenedictEvans) <a href="https://twitter.com/BenedictEvans/status/826552783868350464">January 31, 2017</a></blockquote> <script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>

*Apple Watch ha avuto il miglior trimestre di sempre. Sconnessione veramente ampia tra la tecnologia e le aspettative e la soddisfazione dell’utente normale.*

Non è il caso di buttarla in altri campi diversi dal digitale, ma cominciano a diventare numerosi i settori nei quali i cosiddetti (e sedicenti) specialisti appaiono completamente scollegati dal mondo e dalle vite reali. Intanto, ora come ora, watch potrebbe avere venduto anche venticinque milioni di esemplari al mondo. Non penso che sia secondo ad alcuno.