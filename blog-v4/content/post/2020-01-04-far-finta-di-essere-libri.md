---
title: "Far finta di essere libri"
date: 2020-01-04
comments: true
tags: [Venerandi, ebook, Apple, Amazon, Drm, Jobs, Apogeo, iPad, Pages, iBooks, Author, ePub]
---
[Si accennava](https://macintelligence.org/posts/2020-01-02-eterne-domande-bollenti/) al [discorso di Capodanno più interessante letto finora](http://www.quintadicopertina.com/fabriziovenerandi/?p=1452), quello di Fabrizio Venerandi sullo stato della nazione degli ebook.

Triste stato e discorso ampiamente condivisibile. Parliamo di un fallimento generale a tutti i livelli, dall’ultimo lettore al primo dei programmatori, nel provare a superare il libro di carta anziché farne una copia in digitale e nel farlo perdere i vantaggi della carta intanto che si sprecano le opportunità del digitale.

Va letto tutto, assimilato e metabolizzato per poi trasformarlo in energia: fare un passo avanti nel creare il solito ebook, fare una richiesta in più al proprio editore, insistere a scuola, in ufficio, ovunque abbia un senso. Così come sono, a qualsiasi livello, gli ebook non vanno, punto.

Venerandi sa molto più di me sugli ebook e quindi avanzo la mia idea sommessamente: ho paura che l’ebook ideale, alla fine dei conti, *sia una app* e non un ebook. Una volta che si fa digitale sul serio parliamo di programmazione e poco conta che sia JavaScript dentro una pagine HTML. Potrei sbagliarmi.

Su Apple ne so un pochino più io e mi permetto una correzione marginale a un discorso altrimenti perfetto. Scrive Venerandi:

>I grossi distributori/player come Apple o Amazon sono – paradossalmente – i primi ad avere inserito un grosso freno a mano. Hanno usato i propri formati proprietari, le proprie applicazioni di lettura e i propri DRM per rallentare qualunque sviluppo degli ebook che non fosse quello del porting digitale di libri di carta.

Questo non è esatto e, anzi, ho l’impressione che – nel disastro degli ebook oggi – i Drm contino poco o nulla. Anche ove contassero, non è questione di Apple, ma degli *editori*. Sono gli editori che vogliono i Drm.

Non convinti? Beh, è tutto già successo. Le riflessioni di Steve Jobs sullo stato della musica [le ho già rievocate](https://macintelligence.org/posts/2014-12-06-la-solita-musica/), da quanto sono vecchie e lì c’è già tutto. Musica e non libri, certamente, ma basta leggere: il Drm è una spina nel fianco di chi lo deve amministrare e ne farebbe volentieri a meno. Come è andata a finire con la musica? Apple è stato il primo distributore importante ad abbandonare il Drm, cioè a convincere le *major* ad abbandonarlo. Oggi la musica digitale è sostanzialmente libera da Drm.

La strada aperta con [iBooks Author](https://www.apple.com/it/ibooks-author/) e lasciata a metà – il programma è abbandonato a se stesso o quasi – neanche riguardava gli ebook, nonostante il nome: il suo scopo era promuovere iPad da una ennesima angolazione e, magari, stimolare gli insegnanti a produrre loro dispense in formato iPad. Pages esporta in ePub e non fa niente di speciale in mezzo, ma certo non è un freno a mano; semplicemente una ennesima occasione perduta delle mille che Venerandi cita. Gli ePub creati con Pages non hanno alcun Drm.

Serie TV e film vengono ancora distribuiti con Drm e Apple, ancora una volta, non c’entra: a volere la protezione anticopia sono i produttori, non il distributore.

Sicuramente un libro senza Drm è un prodotto più salutare e più apprezzato: lavoro con [Apogeo](https://www.apogeonline.com) da alcuni anni e le vendite vanno bene, quelle di carta e quelle degli ebook. Quelle degli ebook vanno molto meglio della media e questo potrebbe essere anche dovuto all’assenza di Drm. Ma da qui a rovesciare la situazione descritta da Venerandi ce ne corre.

Globalmente, l’ebook continua a *fingere di essere un libro di carta per non farsi scoprire*. Per capire quanto siamo indietro, è sufficiente leggere un ebook – anche un bon ebook – su un computer da tasca, tipo iPhone, e giudicare l’esperienza dal punto di vista della tipografia e del piacere della lettura. È un disastro, a partire da una constatazione di design semplice quanto inquietante: *nel dare al lettore umano un controllo di base sull’impaginazione, i reader di ebook permettono una cattiva formattazione del testo*. È l’antitesi dell’idea di libro e la dimostrazione pratica di quanto scrive Venerandi: nessuno investe abbastanza perché le cose siano fatte bene.

Siamo tutti responsabili e va tenuto in conto per l’anno che ci aspetta.