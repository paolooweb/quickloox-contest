---
title: "Ntfs invisibile"
date: 2014-01-04
comments: true
tags: [OSX, Mavericks, Ntfs]
---
Geniale *hack* di **Francesco** che spiega come [montare in scrittura, a proprio rischio](http://quasi.me/2013/12/29/macosx-mavericks-e-ntfs/), un disco Ntfs su Mac senza l’aiuto di soluzioni esterne.<!--more-->

Unica cosa che manca, la spiegazione del perché non lo faccia Apple: Microsoft non ha mai documentato il funzionamento del suo formato. Di conseguenza, un solo errore nel tentativo di indovinarlo, a disco scrivibile, può fare svanire nel nulla un terabyte di dati, per dire.