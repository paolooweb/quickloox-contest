---
title: "Sembra facile"
date: 2019-06-23
comments: true
tags: [Microsoft, Google, iPad, Pixel, TechCrunch]
---
Google non è particolarmente interessata a vendere hardware, ma ne produce di discreta qualità per dare la linea ai produttori di apparecchi Android che così hanno davanti un riferimento indicativo.

Da ora ne produce un po’ meno, perché [ha abbandonato lo sviluppo di tavolette](https://www.computerworld.com/article/3404206/googles-officially-done-making-its-own-tablets.html) e si concentrerà unicamente su portatili e computer da tasca.

Sembra che l’unico oggetto di questo tipo che continua a funzionare presso il pubblico sia iPad. E non è colpa dell’oggetto né del pubblico, bensì del progresso. Anno dopo anno, Apple ha alzato sempre più l’asticella in fatto di funzioni e specifiche hardware e, per le aziende che improvvisano sul breve periodo, ingegnerizzate una cosa come l’attuale iPad Pro dev’essere una sfida problematica.

Viene in mente anche una Microsoft che voleva a tutti i costi vendere computer da tasca propri e, invece di dedicarsi allo sviluppo, ha comprato un pezzo di Nokia per cavarsela rapidamente. È finito tutto.

Produrre una tavoletta competitiva è sembrato facile a molti. Non lo era e a maggior ragione non lo è più.
