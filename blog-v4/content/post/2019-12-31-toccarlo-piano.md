---
title: "Toccarlo piano"
date: 2019-12-31
comments: true
tags: [iPhone, iPad]
---
Sono finiti gli anni Dieci, non è finito il secondo decennio del secolo, a dispetto di qualsiasi cosa si leggerà.

Proviamo a semplificare per il cenone: sono stati gli anni del *touch*.

Gli anni Zero sono stati quelli dei social. Gli anni Venti saranno quelli della voce.

Gli anni Novanta quelli di Internet. Gli anni Ottanta quelli del mouse. Gli anni Settanta quelli della tastiera.

La storia umana degli ultimi cinquant’anni descritta attraverso le interfacce dominanti. E devono ancora arrivare i primi.

Sarà un anno bello? Sì. È possibile costruirlo insieme e pertanto, sì, lo sarà. Per tutti noi e le persone che portiamo nel cuore.