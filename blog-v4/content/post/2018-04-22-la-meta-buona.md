---
title: "La metà buona"
date: 2018-04-22
comments: true
tags: [Wi-Fi, McDonald’s]
---
Tocca scaricare in urgenza una versione di Pages su iPhone ma App Store eroga via connessione cellulare solo oggetti che pesino meno di centocinquanta megabyte. L’unica alternativa è il McDonald’s qui davanti.

Il cui Wi-Fi, incredibilmente, eroga la metà di un gigabyte in tempo ragionevole, senza errori o interruzioni o intoppi vari.

O il servizio è decisamente migliorato, o facevo meglio a giocare due colonne di Superenalotto.
