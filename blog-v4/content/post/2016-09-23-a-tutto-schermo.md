---
title: "A tutto schermo"
date: 2016-09-23
comments: true
tags: [Mann, Ruanda, iPhone]
---
Si leggono recensioni di ogni tipo su iPhone 7. Consiglio vivamente quella di Austin Mann il quale, banalmente, ha compiuto [un piccolo safari fotografico in Ruanda](http://austinmann.com/trek/iphone-7-camera-review-rwanda).

Il suo parere conta qualcosina.

Oltre a consigliare la sua recensione, consiglio di leggerla sullo schermo più grande possibile, dopo averlo calibrato. Se necessario, affittare un iMac 27 pollici 5K per una sera, o un megaschermo.

Invitare gli amici, stando attenti a quelli con un coso Android, che potrebbero macchiare di saliva il pavimento o rovinare la serata con espressioni di invidia e risentimento. (A parte il rischio esplosioni).

Viene anche da chiedersi se iPhone 7 sia ancora un computer da tasca o sia invece divenuto una fotocamera tascabile con computer dedicato. Di certo, quella funzione telefono cellulare sembra sempre più un accessorio che non fa male, per carità, ma a momenti uno neanche se ne accorge.