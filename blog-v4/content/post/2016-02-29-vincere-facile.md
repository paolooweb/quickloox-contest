---
title: "Vincere facile"
date: 2016-02-29
comments: true
tags: [Go, Gnu]
---
Per via di un regalo di Natale mi sono riavvicinato al gioco del [Go](http://senseis.xmp.net/?Go), immensamente più complesso degli scacchi e assolutamente affascinante.<!--more-->

Nonostante il tempo sempre troppo scarso a disposizione, mi piacerebbe riuscire almeno a diventare un principiante e magari, un giorno, poter sfidare un umano senza fare figuracce e fargli o farle perdere del tempo.

Nel frattempo, approfittando dell’installazione di [Gnu Go](https://www.gnu.org/software/gnugo/) avvenuta molto tempo fa nel mio Terminale, mi sono comunque preso una soddisfazione: con il comando `gnugo --boardsize 3` ho impostato una plancia di gioco 3x3 (normalmente, ehm, è 19x19) e ho sfidato il software.

Ho giocato la prima pietra al centro della plancia e ho vinto, per rinuncia.

Da qualche parte si doveva pur cominciare. Ora posso tentare una plancia 4x4.