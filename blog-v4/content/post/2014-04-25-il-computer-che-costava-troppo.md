---
title: "Il computer che costava troppo"
date: 2014-04-28
comments: true
tags: [Mac, Lisa]
---
Volevo scrivere un po’ di Mac, solo che le notizie a riguardo scarseggiano e non perché la materia sia trascurata da Apple, come vogliono certi buontemponi. Se lo fosse, la vendita di Mac non sarebbe [aumentata del cinque percento dal primo trimestre dell’anno scorso a quello di quest’anno](http://www.apple.com/it/pr/library/2014/04/23Apple-Reports-Second-Quarter-Results.html), in controtendenza con il mercato generale dei personal computer, da tempo in leggera però evidente discesa.<!--more-->

Si faccia un salto nel passato allora, con [la prima](https://www.youtube.com/watch?v=X7rLpYeahN4) e [la seconda](https://www.youtube.com/watch?v=VEhpeerSRdM) parte di questo *informercial* Apple su Lisa. Il computer che era già per certe cose dove Mac fa fatica persino oggi ad arrivare, solo che costava troppo. L’atmosfera dei video è da confini della realtà guardato con gli occhi di oggi, per le tematiche, i personaggi, il ritmo e i dialoghi. Eppure, dopo essersi fatti un’idea vera di come funzionava Lisa, verranno da pensare tante cose sull’informatica e sulla sua evoluzione *personal*.

<iframe width="420" height="315" src="//www.youtube.com/embed/X7rLpYeahN4" frameborder="0" allowfullscreen></iframe>

<iframe width="420" height="315" src="//www.youtube.com/embed/VEhpeerSRdM" frameborder="0" allowfullscreen></iframe>