---
title: "Fontascienza"
date: 2014-12-07
comments: true
tags: [Typeset, Alien, 2001]
---
Ennesima prova provata che Internet dà il meglio quando ci si occupa della nicchia più circoscritta possibile nel modo più approfondito possibile.<!--more-->

Il titolo è quello che avrei dato al blog *Typeset in the Future* se lo avessi creato io in italiano. L’autore – che all’opposto di me sa farlo davvero – disseziona i grandi film di fantascienza alla ricerca di qualsiasi elemento tipografico, che analizza.

I risultati sono straordinari. Invito alla lettura di [2001](http://typesetinthefuture.com/2001-a-space-odyssey/) e soprattutto [Alien](http://typesetinthefuture.com/alien/).

Ignoravo esistesse un *Semiotic Standard for all Commercial Trans-Stellar Utility Lifter and Heavy Element Transport Spacecraft*. Era ignoranza vera, anche se nessun esame universitario mai ne parlerà.