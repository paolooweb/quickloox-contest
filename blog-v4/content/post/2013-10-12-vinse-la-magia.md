---
title: "Vinse la magia"
date: 2013-10-12
comments: true
tags: [iPhone, Nokia]
---
Il 18 agosto 2008 giornalista finlandese Lauri Malkavaara scrisse [una lettera](http://www.hs.fi/kuukausiliite/This+is+how+a+Helsingin+Sanomat+journalist+tried+to+save+Nokia/a1381288411564?ref=hs-art-new-1) a Nokia, come privato cittadino e non in quanto giornalista, tenne a precisare. Il motivo è che non riusciva a capire come fare funzionare il proprio cellulare.<!--more-->

>Un anno e mezzo fa un amico mi mostrò un apparecchio costruito da Apple, chiamato iPod touch. Me ne innamorai all’istante. […] Ordinai il mio iPod touch, lo accesi e immediatamente sapevo come usarlo. Lo uso da sei mesi tutti i giorni e non ho neanche pensato a un manuale. La logica dell’apparecchio si dispiega passo dopo passo. Non mi meraviglio che abbia tutto questo successo.

>Il mio nuovo telefono Nokia si chiama E 51. Sfortunatamente non è stato progettato in modo che possa essere usato facilmente. Al contrario, penso che sia stato progettato come se il suo scopo più importante fosse autopubblicizzarsi presso le persone interessate alla tecnologia cellulare. Sullo schermo appare ogni tipo di funzione, tutte straordinarie, ma non capisco il loro significato e deduco che non le userò mai.

Molti dirigenti Nokia gli risposero, spiegandogli la strategia aziendale. Lui però non scriveva da giornalista ed esponeva un problema da privato. Si stancarono tutti tranne uno che lo invitò nel suo ufficio.

Gli spiegò che le persone sono differenti e quindi servono diversi tipi di telefono.

Malkavaara rispose *non esistono persone che vogliano usare un pessimo telefono*.

Alla fine della discussione il dirigente gli confidò che era d’accordo con lui e Nokia aveva un progetto segreto in corso, un nuovo sistema operativo che avrebbe portato a nuovi telefoni. Oggi sappiamo che si chiama <a href="https://meego.com">Meego</a> e non è andato da nessuna parte.

La morale della storia è che nel 2007 Nokia aveva seguito con estremo interesse l’uscita di iPhone e ne aveva consegnati numerosi ai dirigenti, perché lo analizzassero.

Il dirigente rimase talmente interessato al nuovo apparecchio che se lo portò anche a casa. Studia che ristudia, si appassionò all’oggetto anche la figlia di quattro anni. La quale, avuto iPhone in mano, imparò immediatamente a usare le funzioni principali.

La sera, la bambina si presentò sulla soglia della camera dei genitori:

>Posso prendere il telefono magico e metterlo sotto il mio cuscino stanotte?

Oggi Nokia è un’ombra pallida di ciò che fu. La verità è che non c’è tecnologia che tenga, contro una magia potente a sufficienza.