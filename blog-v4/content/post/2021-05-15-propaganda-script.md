---
title: "Propaganda script"
date: 2021-05-15T01:43:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [Dr. Drang, BBEdit, iPad, Mac] 
---
Dr. Drang è tornato a [bloggare da Mac dopo tre anni di iPad](https://leancrew.com/all-this/2021/03/blogging-from-bbedit-again/). Si era messo su iPad perché, tra altre ragioni,

>Cercavo di imparare i modi migliori di usare un iPad e forzarmi a usarlo per bloggare sembrava un buon metodo di apprendimento.

Sistema sicuramente diverso da *su Mac faccio così ma su iPad devo farlo diverso quindi non va bene*.

Avrà ottenuto qualcosa? Dopotutto perdura il mito di iPad come macchina da consumo più che da produzione di informazioni. Certo, tre anni di blogging qualcosa devono averlo insegnato, altrimenti uno come lui avrebbe smesso molto prima. Vuole dire che qualcosa da imparare c’era.

Per riprendere su Mac, Drang ha ripreso [BBEdit](https://www.barebones.com/products/bbedit/), naturalmente con la sua cifra:

>Quando bloggavo regolarmente con BBEdit, avevo realizzato diversi script per facilitarmi il compito. Nelle due ultime settimane ho riportato in vita quegli script…

Tre anni su iPad hanno comunque lasciato traccia:

>…e ho effettuato aggiunte, rubando numerose idee che ho usato in Drafts e Comandi rapidi.

Scripting che si propaga da iPad a Mac. Niente male, per un apparecchio che neanche dovrebbe potersi chiamare *computer*, per alcuni.

Ciliegina sulla torta:

>Ora mi ritrovo un package Blogging nuovo e migliore per BBEdit su Mac, che unisce script creati in [bash](https://www.gnu.org/software/bash/), [Python](http://python.it), [Perl](https://www.perl.org) e [AppleScript](https://macosxautomation.com/applescript/firsttutorial/index.html).

Che poi, costruire un package per BBEdit è una [attività a sé di sviluppo](http://www.leancrew.com/all-this/2013/08/helper-scripts-in-bbedit-packages/).

Drang non ha reso disponibili i suoi script. Però li ha elencati. Cimentarsi nel realizzarne qualcuno da soli sarebbe un’eccellente modalità di autoapprendimento.

*Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile.*               