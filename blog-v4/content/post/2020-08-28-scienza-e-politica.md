---
title: "Scienza e politica"
date: 2020-08-28
comments: true
tags: [Speroni, Difenizio]
---
C’è qualcosa peggiore, per divisività e ambiguità del tema, di scrivere su scienza e fede? Sì: scienza e politica.

Perché la politica va per idee, ma i meccanismi della politica vanno per scienza. O dovrebbero ed è già la prima area di conflitto.

*Votare*, diceva un amico su Facebook, *è l’atto politico per eccellenza*. Certo. Contare i voti è aritmetica pura; non c’è niente di politico nel conteggio dei voti e, se c’è, puzza.

Un parlamento è la manifestazione più alta della politica, del confronto, della democrazia. Il numero dei membri di un parlamento – il discorso si fa improvvisamente meno astratto – non è, non deve essere una decisione politica, che è inevitabilmente di parte. Il parlamento è, appunto, sopra le parti: pieno di parti che litigano e lo fanno secondo regole rigide, precise, che valgono per tutti, non per la maggioranza o per chi raccoglie più voti.

Quindi il numero dei membri di un parlamento va deciso in maniera scientifica: il numero ottimale per una nazione, il miglior compromesso tra la rappresentatività e la funzionalità effettiva.

Sembra impossibile? Eppure [ci hanno provato](http://publications.ut-capitole.fr/2423/1/representatives.pdf). C’è uno studio scientifico che ipotizza come calcolare il giusto numero di parlamentari in rapporto alla popolazione.

Giusto? Sbagliato? Fazioso? Sbilanciato? Imparziale? Pilotato? Frega niente. È uno studio scientifico, quindi dire *sbagliato* è rispondere con una pernacchia a un argomento. Bisogna portare dati migliori se i dati non vanno bene e argomentare correttamente se l’argomentazione è scorretta. In pratica, o si scavalca lo studio per andare oltre o è meglio stare zitti. Il metodo scientifico funziona così. Un *peer* può limitarsi a trovare difetti nello studio, ma deve mostrare che sono tali e farlo al livello di complessità dello studio, o più sopra. Più sotto, meglio leggere che scrivere.

(Il primo che si azzarda a dire che per l’Italia dovrebbe applicarsi a una camera sola, essendocene due, lo deautorizzo su iTunes).

Parlando di questo argomento si finisce inevitabilmente a parlare di sistema elettorale. Mi permetto di segnalare alcuni [articoli di Pietro Speroni di Fenizio](https://www.apogeonline.com/search/pietro+speroni/) di cui ho avuto l’onore di curare la pubblicazione. Speroni di Fenizio è un matematico specializzato nell’analisi dei sistemi elettorali, che ha inventato un sistema semplice da capire, capace di riunire caratteristiche di proporzionale e maggioritario in modo sinergico e riunire governabilità, rappresentanza, tutela delle minoranze, impossibilità di maggioranze bulgare e altro ancora. Invito alla lettura.

Molti e molti anni fa, un mio amico talentuoso utilizzò un Sinclair QL per mostrare i risultati delle elezioni locali su un grosso televisore in pizzeria *man mano che procedeva lo spoglio*: non era mai successo prima. Interesse e curiosità totali da parte di tutti in paese. Questo post discende da quel giorno lì.

I partiti si erano (metaforicamente) scannati a fare politica per conquistare voti. La partecipazione degli elettori non finiva usciti dal seggio, però; e per fare sapere a tutti chi vinceva, prima di chiunque altro, serviva un computer con un programma. Politica e scienza devono marciare insieme. Litighiamo su quanti seggi si debbano riempire in parlamento, ma su base scientifica. Chi lo fa su base politica cerca il proprio interesse e invece il parlamento, grande o piccolo, è di tutti, sopra le parti.