---
title: "Casi di cura"
date: 2014-08-22
comments: true
tags: [Cook, iPad]
---
Oramai diventato profondo conoscitore di ciascun metro quadrato del reparto ostetricia, ho qualcosa in comune con l’amministratore delegato di Apple Tim Cook, anche lui [di passaggio recente in ospedale](https://twitter.com/tim_cook/statuses/502586920476868608).<!--more-->

<blockquote class="twitter-tweet" lang="en"><p>Honored to meet doctors &amp; Veterans with <a href="https://twitter.com/RepAnnaEshoo">@RepAnnaEshoo</a> at <a href="https://twitter.com/VAPaloAlto">@VAPaloAlto</a>, now using iPads to help treat Vets &amp; families. <a href="http://t.co/lOZwDT8KZu">pic.twitter.com/lOZwDT8KZu</a></p>&mdash; Tim Cook (@tim_cook) <a href="https://twitter.com/tim_cook/statuses/502586920476868608">August 21, 2014</a></blockquote> <script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>

Invece che stare in un posto dove nascono bambini, Cook era in un luogo dove si curano veterani di guerra, anche con l’aiuto di iPad.

Posso dire che in Italia è possibile trovare assistenza ottima e personale capace oltre che umano, e sempre sollecito. Qualche iPad qua e là servirebbe, comunque, a eliminare tante scartoffie e qualche approssimazione organizzativa, a volte più ostinata della buona volontà e dell’impegno oltre il dovuto degli addetti.