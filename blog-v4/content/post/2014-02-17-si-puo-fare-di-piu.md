---
title: "Si può fare di più"
date: 2014-02-17
comments: true
tags: [iPhone, Apple, Samsung, Windows]
---
Nel trimestre di Natale 2012 Apple ha incassato il 77,8 percento dei profitti del mercato dei computer da tasca e Samsung il 26,1 percento. Esatto: hanno intascato insieme complessivamente il 103,9 percento dei profitti totali esistenti. Non hanno battuto autonomamente moneta; le altre aziende concorrenti nello stesso mercato hanno generato profitti negativi, ovvero hanno perso denaro nell’attività.<!--more-->

A Natale 2013 la concorrenza [è ancora più agguerrita](http://news.investors.com/technology-click/021114-689654-2014-mobile-phone-market-to-see-little-or-no-growth.htm): Apple ha messo in saccoccia l’87,4 percento dei profitti e Samsung il 32,2 percento. Totale 119,6 percento. Chi perdeva denaro, ne perde ancora di più.

Acquistare un prodotto che vuole supporto, aggiornamenti, attenzione da parte di un’azienda che perde denaro per venderlo… mah. Si può fare di più.