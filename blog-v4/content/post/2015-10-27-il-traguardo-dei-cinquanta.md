---
title: "Il traguardo dei cinquanta"
date: 2015-10-27
comments: true
tags: [design]
---
Mi chiedo quanti discettano di ogni nuovo Mac o delle interfacce utenti di questo o quel programma e saprebbero passare i [cinquanta termini base del design spiegati ai non designer](https://designschool.canva.com/blog/graphic-design-terms/) scrollando le spalle a ogni fermata perché già in possesso dei cinquanta corretti significati.