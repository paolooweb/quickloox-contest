---
title: "Programmarsi il Natale"
date: 2022-12-06T01:37:13+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [Advent of Code]
---
Non ho tempo. Posso solo segnalare la pagina di Slashdot dedicata ai [calendari dell’avvento 2022 a tema programmazione](https://developers.slashdot.org/story/22/12/05/0629239/2022s-geeky-advent-calendars-tempt-programmers-with-coding-challenges-and-tips).

Da [Advent of Code](https://adventofcode.com/) in avanti, comunque, me li guardo, tutti. Sperando di riuscire a programmare almeno un po’ nel tempo di Natale.