---
title: "Woodstock fugit"
date: 2023-09-20T02:19:45+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [Peanuts, Snoopy, Woodstock, Apple Watch]
---
Mi ricordo quando si parlava della precisione di watch e di quando [Apple riprendeva i prototipi a mille fotogrammi al secondo](https://macintelligence.org/posts/2016-01-04-minacce-di-precisione/) e faceva la stessa cosa con un orologio atomico, per mettere a confronto i due apparecchi. Attenzione minuziosa ai dettagli.

Con watchOS 10, arriva un quadrante [dedicato a Snoopy e Woodstock](https://sixcolors.com/link/2023/09/designing-the-snoopy-watchos-10-face/). I due personaggi danno vita durante il giorno alla bellezza di 148 diverse animazioni, amministrate da un pezzetto di software che provvede a evitare o limitare le ripetizioni. Minuziosa attenzione ai dettagli.

Siccome cane e uccellino interagiscono anche con le lancette, alcune animazioni esistono in sei versioni, opportunamente ruotate perchè l’illusione funzioni durante tutta la giornata e non solo a un orario specifico.

Sembrano una notizia da *nerd* e una da passatempo. Le vedo entrambe dentro uno stesso metodo di lavoro, in una filosofia coerente, attorno a un prodotto curato in ogni particolare, giù fino alle animazioni di Woodstock.

Se vuoi fare il computer da polso migliore del mondo, cerchi l’eccellenza anche nelle piccolezze con i *cartoons*.