---
title: "Pace e programmini"
date: 2020-09-25
comments: true
tags: [Pixelmator, Soghoian, AppleScript, scripting, Shortcuts, Workflow, Automator, Jobs]
---
Può anche essere una giornata di lavoro veramente pessima, mentre fuori si scatena il temporale e l’estate termina. Però leggi che [Pixelmator Pro 1.8 si è aggiornato con un bel supporto di AppleScript](https://www.pixelmator.com/blog/2020/09/17/pixelmator-pro-gets-applescript-support/) e, comunque, la giornata prende un’altra piega.

Pixelmator Pro è un gran bel programma. Non sempre in linea con le linee guida di interfaccia di Mac, ma con lo spirito giusto. Tanto che il supporto AppleScript è stato fornito da Sal Soghoian, il padre dello scripting in Apple.

Wired racconta tra altre cose di quando [Soghoian richiamò l’attenzione di Steve Jobs](https://www.wired.com/story/soghoian-automation/). Era il 1997, Jobs appena tornato aveva il compito di ricostruire. Si mise a tagliare rami secchi, divisioni che non rendevano il dovuto, cancellare progetti anche visionari – Newton – e però troppo costosi. Tenne un discorso molto duro a un gruppo di dipendenti. Era facile lavorare con Mac cento volte migliore di Windows. Ora non era più così, e loro non sapevano più cosa fare.

L’unico a rispondere fu Sal Soghoian, product manager Apple per l’automazione. *Sono Sal Soghoian, e hai torto. La mia tecnologia è migliore di quella di Windows*.

Jobs stava attaccando quei dipendenti proprio per capire quanta passione avessero per il proprio lavoro e se qualcuno ne avrebbe avuta abbastanza da difenderlo. Eccone uno.

Da lì AppleScript e poi Automator. AppleScript ha rischiato diverse volte di scomparire, ma – ben più di Pixelmator – partecipa con forza allo spirito Mac. Jobs ha scherzato su come [Apple fosse guidata da un grosso AppleScript](https://books.google.it/books?id=6vjvhNp1QIEC&pg=PT302&lpg=PT302&dq=the+los+angeles+times+was+produced+with+applescript&source=bl&ots=IMRLXPG62z&sig=ACfU3U0mpVdpXo-nC1_rvvxWP2FN3NGPyg&hl=en&sa=X&ved=2ahUKEwiskOPK24LsAhXEzaQKHf-DA48Q6AEwCXoECAMQAQ#v=onepage&q&f=false) e si ritiene che il quotidiano *Los Angeles Times* abbia salvato AppleScript al momento della transizione a Mac OS X: il flusso di lavoro della testata si basava sull’automazione di Mac al punto che – avesse dovuto rinunciarvi – non sarebbe riuscita a uscire con l’abituale efficienza.

Soghoian [non lavora più in Apple](https://macintelligence.org/blog/2016/11/20/aut-out/) ma continua a promuovere l’automazione, stavolta presso Omni Group.

Per considerare il futuro dello scripting e dell’automazione in Apple bisogna possedere doti serendipitiche. La società ha diverse carte da giocare: Automator, Workflow e Comandi rapidi su iOS, per esempio, più pezzi di tecnologie che non sono più – HyperTalk, per dire – e tuttavia potrebbero ritrovare uno scopo. Si può sperare in qualche colpo di teatro unificante che porti progresso in questa direzione e anche significativo.

O forse la magia algida di AppleScript continuerà ad assicurargli la sopravvivenza, non lo sappiamo. [Scrive benissimo John Gruber](https://daringfireball.net/linked/2020/09/24/pixelmator-pro-applescript):

>Chiaramente Apple non è abbastanza interessata all’automazione degli strumenti professionali per creare un linguaggio di scripting veramente nuovo, ma lo è a sufficienza per tenere funzionante AppleScript. Il persistere di AppleScript, a pensarci, è veramente inusuale.

È stata una giornata pessima sul lavoro, il tempo è brutto, sono in ritardo. Eppure stasera soffia lo spirito di Macintosh, quello di un tempo. E trovo pace prima di dormire.

P.S.: anche [Achille Campanile](http://www.campanile.it) è una buona lettura da fine giornata e il titolo è ispirato a un capitoletto di uno dei suoi libri migliori. Bonus a chi lo individua.