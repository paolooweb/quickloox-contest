---
title: "Lavorare con freddezza"
date: 2023-02-07T00:37:49+01:00
draft: false
toc: false
comments: true
categories: [Hardware, Software]
tags: [DiffusionBee, DrawThings, Mac mini, M1, Elp, Emerson Lake & Palmer]
---
Scrivo, edito, cerco cose su Internet. DiffusionBee e DrawThings lavorano in parallelo; cerco di farmi un’idea di come eventualmente uno possa, o meno, essere meglio dell’altro.

In omaggio al calendario, YouTube trasmette una vecchia *compilation* di *live* di [Karn Evil 9](https://www.youtube.com/watch?v=MFwYKI86N_c) a firma Emerson Lake & Palmer.

Mac mini è coperto da una pila di carte che devo sistemare. Non si dovrebbe fare; il mini Intel precedente avrebbe già raggiunto il punto di fusione. M1, invece, è appena tiepido; se sposto le carte, devo appoggiare convintamente la mano per avvertire un vago calore.

Un Mac Studio in questa situazione mi farebbe mangiare la polvere e anche un buon Mac con M2, non c’è dubbio. Al tempo stesso, trovo rasserenante questo rapporto plurimo di prestazioni, prezzo e consumo. Con i primi due parametri si può discutere all’infinito. Quando si aggiunge il terzo, però, la partita si chiude immediatamente.

<iframe width="560" height="315" src="https://www.youtube.com/embed/MFwYKI86N_c" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>