---
title: "Come l’aereo lascia la scia"
date: 2018-01-12
comments: true
tags: [Mario, iPhone, Barclays]
---
Mario ha scritto una [bella storia](https://multifinder.wordpress.com/2018/01/10/obsolescenza-programmata/) su come vive personalmente l’obsolescenza programmata del suo iPad.<!--more-->

C’è da aggiungere poco, perché la vita vera ha la priorità sulle chiacchiere. È ora di derubricare la faccenda al livello della Terra piatta, delle scie chimiche e altri palliativi per menti in difficoltà.

Quel poco è una [stima di Barclays](http://www.businessinsider.com/apple-battery-controversy-10-billion-lost-iphone-sales-2018-1?IR=T) su quanto potrebbe costare l’anno prossimo ad Apple in mancata vendita di iPhone il ribasso sul prezzo di cambio batteria: sedici milioni di apparecchi venduti in meno, rinuncia a dieci miliardi di dollari di fatturato.

Poco ci curiamo dell’efficacia della previsione. Potrebbe essere il doppio o anche metà, solo che la linea logica dei fatti rimane la stessa ed è sempre più chiara.

Milioni di persone compreranno una batteria nuova invece che un iPhone nuovo, perché la batteria costa 29 dollari invece che 79.

Con la batteria a settantanove dollari avrebbero comprato più probabilmente un iPhone nuovo, considerando meglio spendere ottocento dollari per il nuovo invece di ottanta per il vecchio.

Se ci fosse l’obsolescenza programmata, ad Apple converrebbe moltissimo avere batterie che malfunzionano. Così la gente, appunto, spende dieci volte di più.

Invece Apple che fa? Controlla la batteria in modo che non malfunzioni.

Questo, secondo i fautori della teoria, spinge la gente a cambiare telefono. Cioè a spendere ottocento per il nuovo invece di… zero per il vecchio, che continua a funzionare.

Per la teoria, ottocento contro ottanta fa vincere ottocento. Ottocento contro zero fa vincere ottocento.

Eppure, stranamente, ottocento contro ventinove fa vincere ventinove.

Ho una teoria sui cervelli dei fautori della teoria. Sono rimasti danneggiati dalle scie chimiche.