---
title: "Divide et non dispera"
date: 2015-09-20
comments: true
tags: [Nadella, Microsoft, Outlook, Ballmer, iPhone, LibreItalia, LibreOffice, Difesa, VirtualBox]
---
Così l’amministratore delegato di Microsoft ha tenuto una dimostrazione di Outlook [dovendo usare un iPhone sul palco](http://uk.businessinsider.com/microsoft-ceo-satya-nadella-used-iphone-2015-9).<!--more-->

Bravo per l’umiltà e l’onestà intellettuale, che riscatta neanche tanto l’ormai leggendaria [figura da bulletto](https://www.youtube.com/watch?v=eywi0h_Y5_U) del suo predecessore, ma soprattutto i [funerali di iPhone](http://www.neowin.net/news/microsoft-workers-celebrated-windows-phone-7-rtm-with-iphone-hearses) consumati con stile carnascialesco nel quartier generale Microsoft all’uscita di Windows 7. Come goliardi hanno grandi competenze e va riconosciuto.

Non sono ancora contento e auspico di più. Salvi per ora dalla tirannia di un mondo tutto di PC tutti pieni di software Microsoft, abbiamo una libertà di procurarci computer che mai prima.

Un mondo pieno di computer dei più vari, sui quali gira sempre e comunque software Microsoft, rimane però una minaccia consistente. Non dobbiamo disperare, anzi, non è mai andata così bene. Guai a distrarsi, comunque.

Sosteniamo la biodiversità del software, dei sistemi operativi, delle applicazioni. Diventiamo soci di [LibreItalia](http://www.libreitalia.it) (lo sapevi che [il Ministero della Difesa passa a LibreOffice](http://www.libreitalia.it/accordo-di-collaborazione-tra-associazione-libreitalia-onlus-e-difesa-per-ladozione-del-prodotto-libreoffice-quale-pacchetto-di-produttivita-open-source-per-loffice-automation/) come software per la produttività individuale?).

Adottiamo un sistema operativo libero dentro [VirtualBox](https://www.virtualbox.org) o equivalente e impariamo a parlare due lingue informatiche, come bisogna saperne parlare due nella vita.

Tifiamo come matti per il nostro software, il nostro hardware preferito. Che arrivi al 20 percento del mercato come altri quattro concorrenti e che ci sia una bella competizione leale e onesta per salire al 21 lasciando qualcun altro al 19.

<iframe width="420" height="315" src="https://www.youtube.com/embed/eywi0h_Y5_U?rel=0" frameborder="0" allowfullscreen></iframe>