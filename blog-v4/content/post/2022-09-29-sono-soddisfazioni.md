---
title: "Sono soddisfazioni"
date: 2022-09-29T02:41:54+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [Slack, Microsoft, WhatsApp]
---
Capitano cose buone. Oggi ho potuto dare delle specifiche di lavoro a un team riassumibili in queste righe:

>Le comunicazioni passano attraverso Slack. Il lavoro avviene attraverso qualsiasi strumento condiviso precedentemente proposto e concordato. Niente prodotti Adobe. Niente prodotti Microsoft.

Piccole, eh, ma sono soddisfazioni. Che grande mondo si potrebbe fare se fossero specifiche più praticabili ad ampio raggio.