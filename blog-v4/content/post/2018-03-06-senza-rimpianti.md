---
title: "Senza rimpianti"
date: 2018-03-06
comments: true
tags: [Leopard, MacBook]
---
Anno di grazia 2018, trovo dentro Mac, in `/Library/User Guides and Documentation`, un file *Welcome to Leopard.app*.

Centotrentasette megabyte. Il file, ovviamente, data al 2009.

Confesso di averlo spedito dritto nel Cestino senza leggerlo. Forse un giorno lo rimpiangerò. Penso di no.