---
title: "Il momento del software"
date: 2021-05-12T00:34:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [M1, MacBook Air, iPad Pro, MacRumors, Geekbench, Wwdc, MacBook Pro, Intel, BBEdit] 
---
I [dati di Geekbench 5 riportati da MacRumors](https://www.macrumors.com/2021/05/11/m1-ipad-pro-benchmarks/) sono inequivocabili: iPad Pro con M1 vale, computazionalmente, come un Mac con M1.

La media dei test multi-core restituisce un valore di 7.378 per MacBook Air e di 7.284 per iPad Pro.

Sono valori superiori a quelli di un MacBook Pro 16” Intel (6.845), che lasciano indietro iPad Pro con il processore più evoluto della serie Ax. Ne ho uno da due anni e mi sembra tuttora che vada velocissimo; secondo Geekbench, sul multi-core fa 4.656. Una tartaruga rispetto a quello che c’è sul mercato.

Tutto questo significa che chiunque abbia un iPad a fare le fusa sulla scrivania in questo momento guarda ai primi di giugno, a Wwdc, con la voglia di un bel salto in avanti. Software, ovviamente; per l’hardware siamo più che a posto.

iPadOS invece può fare molto più di ora. A voler sperare contro ogni speranza, mi piacerebbe vedere affinamenti dell’interfaccia che non vadano necessariamente verso la computerizzazione della tavoletta; benissimo l’uso di tastiera e trackpad (mancano scorciatoie di tastiera, poi essenzialmente ci siamo), però vogliamo valorizzazione dello schermo touch.

Vorrei un contesto che permetta la comparsa di (un) BBEdit per iPad, magari che consenta manipolazioni e trasformazioni avanzate del testo con il tocco.

Mi piacerebbero estensioni dei Comandi rapidi che consentissero ancora più automazione e ancora più integrazione tra iOS, iPadOS e macOS.

Non ho infine il desiderio di multiutenza che molti hanno e lo cambierei con un raffinamento di autocompletamento e autocorrezione, più un pizzico di machine learning capace di imparare come si muovono le mie dita sulla tastiera virtuale e facilitare loro il lavoro, prendendo atto degli errori e delle imprecisioni abituali e sistemando le cose come devono essere.

La transizione a Arm è tutt’altro che finita, ma sarei entusiasta di vedere l’inizio dell’adeguamento del software a un nuovo standard di prestazioni ed eleganza.

*Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile.*               