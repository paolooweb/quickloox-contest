---
title: "Lezioni da copiare"
date: 2015-05-17
comments: true
tags: [SnapSchool]
---
Scrive **Eugenio**, dal prossimo grassetto a quello dopo.

**In questi giorni** si parla tanto di riforma della scuola, di Lim, *smartphone* e *smartwatch*, poi trovo una *app* come [questa](https://itunes.apple.com/it/app/snapschool/id897944184?l=en&mt=8) (e chissà quante altre) e un pensiero mi fulmina.<!--more-->

Mentre professori e genitori (non tutti per carità) discutono e hanno timore della tecnologia, gli studenti sono da tutt’altra parte. Hanno imparato, senza che nessuno glielo insegnasse, che la forza di questi strumenti sta nella condivisione, nella collaborazione.

E noi, ancorati a vecchi schemi, pensiamo solo a vietare invece di cercare di capire come utilizzarli al meglio per far crescere i nostri figli. **Che ne pensi?**

Che ne penso. Quando avevo la loro età si organizzavano appuntamenti in biblioteca, in oratorio, a casa di uno di noi. Si perdeva gran tempo e poi di tanto in tanto si studiava pure. Quando era urgente o non c’era altro modo, ci si telefonava. In aula si riunivano i banchi e si emulava la stessa situazione; si chiamava *lavoro di gruppo*.

Oggi è semplicemente diventato *lavoro di rete*. Il gruppo è sempre quello come concetto, solo che ha perso i vincoli geografici. Ci si fa aiutare e si aiuta. Lo stupido ne approfitta per copiare, chiaro. Esattamente come allora.

*Niente è cambiato* tranne i mezzi a disposizione. La questione inquietante è che chi puntasse il dito contro la SnapSchool di questo giro o altra *app* equivalente lo punterebbe contro il lavoro di gruppo, o di rete.

La *app* c’entra niente. Toglila e si manderanno messaggini. Togli i messaggini e si manderanno posta elettronica. Togli questa e si telefoneranno. Togli il telefono e si troveranno in biblioteca, come nel secolo scorso, a fare sempre e comunque la stessa cosa.

La differenza è che il sapere fare rete e prosperare attraverso la rete, e stabilire relazioni attraverso la rete, è un’abilità essenziale per gli adulti di domani. Una *app* come SnapSchool dovrebbe essere fornita dalla scuola. O gli insegnanti dovrebbero consigliare la migliore. O dovrebbero insegnare ai loro studenti come organizzarsi e fare rete senza passare da una *app*. Altro che Lim.