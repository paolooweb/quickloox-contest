---
title: "Lezioni di stile"
date: 2022-11-30T18:47:53+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [Keynote, Markdown]
---
Sono settimane con tanti treni e, per causa o per effetto, tanto Keynote. Con iPad Pro ho composto decine di slide a velocità smodata e con ottimo effetto, niente di complicato o sopra le righe, ma messaggi chiari e composti come è giusto presentarli.

Di tanto in tanto la furia compositiva rasenta l’euforia e intanto ho finalmente capito il perché dell’efficienza: uso i template e gli stili. Il controllo dei template e degli stili dà la possibilità, di un controllo semplice e assai efficace su tutta la presentazione in ogni momento. Quel tempo speso inizialmente a impostare gli stili e sistemare i template è successivamente impagabile.

Mi piacerebbe molto abbassare l’asticella dei requisiti e adottare uno dei vari sistemi per comporre slide via Markdown: è doveroso e educativo lavorare con il testo puro ogniqualvolta possibile. Contemporaneamente, l’accensione di Keynote e l’accelerazione a velocità di crociera sono troppo veloci e troppo comode per resistere. Mi impegnerò. Non garantisco nel breve, ecco.