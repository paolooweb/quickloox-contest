---
title: "Il meglio arriva ora"
date: 2014-08-19
comments: true
tags: [Swift, Objective-C]
---
Davanti all’arrivo di qualcosa di unico e nuovo, ho la netta sensazione che crescerà e diventerà molto migliore di me.<!--more-->

Intanto Swift, il nuovo linguaggio di programmazione di Apple, già in fase beta [vanta prestazioni nettamente superiori](http://www.jessesquires.com/apples-to-apples-part-two/) a quelle del vecchio Objective-C.

Da vedere: ci sono tabelle comparative che accenderanno l’appetito di qualsiasi programmatore.