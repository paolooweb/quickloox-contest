---
title: "Chi bara e chi degrada"
date: 2013-10-04
comments: true
tags: [Android]
---
A partire da **Fabio** tanti mi hanno segnalato il tema dei telefoni Android che [barano sui test di velocità](http://www.anandtech.com/show/7384/state-of-cheating-in-android-benchmarks).<!--more-->

<blockquote class="twitter-tweet"><p>Galaxy Note 3 più veloce nei benchmark: Samsung bara <a href="https://twitter.com/search?q=%23digitalia&amp;src=hash">#digitalia</a> <a href="https://twitter.com/loox">@loox</a> ...come al solito <a href="http://t.co/AI0OlOBIjv">http://t.co/AI0OlOBIjv</a></p>&mdash; Andrea Zampierolo  (@Lee_Ago) <a href="https://twitter.com/Lee_Ago/statuses/385260605596524544">October 2, 2013</a></blockquote>
<script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>

Mi lascia tranquillo. È banale disonestà, che alla fine si assorbe. Vero, la gente viene educata a pensare di pagare poco ed essere fregata e che questo sia normale. Ma è andata così con i rottami Windows per vent’anni. Il problema va trattato alla stregua della piccola criminalità, che si può combattere e magari contenere, ma non debellare.

Mi preoccupa più un altro aspetto: finora il degrado ha sempre riguardato hardware e software. Paghi niente e ricevi in cambio merce da poco che funziona male. Desolante, ma in questo si può vedere perfino un equilibrio perverso. Che succede invece se il degrado riguarda il *design*?

<iframe width="420" height="315" src="//www.youtube.com/embed/YGB2yDEjwnk" frameborder="0" allowfullscreen></iframe>

Il problema di questo video è che, con un pubblico superiore alle tre persone, ce ne sarà almeno una che comincia a farfugliare di libertà di scelta e concetti analoghi.

Quando la vera libertà di scelta sarebbe, semmai, avere un’interfaccia condensata usabile con una sola mano a favore di uno schermo grande! Non un apparecchio grande con lo schermo che diventa piccolo. Una occasione persa e la dimostrazione che quell’interfaccia non è fatta pensando a chi la usa, ma è il progettista che pensa a se stesso. Il peggio possibile.

Il degrado programmatico del *design* è un passo indietro per tutti.