---
title: "Fai la cosa giusta"
date: 2019-01-30
comments: true
tags: [Apple, Cook, Milanesi, iPhone]
---
A costo di uscire dalla semplificazione per rasentare la banalizzazione, che sarebbe sbagliato, Apple è in croce perché è in corso una guerra diplomatico/commerciale tra Cina e Stati Uniti, la quale ha favorito una contrazione dei consumi interni cinesi. La Cina ha fatto passi da gigante negli ultimi anni ma è tutt’altro che una nazione *mediamente* ricca e basta poco per rendere inaccessibile l’acquisto di iPhone a una vasta fetta di consumatori. Situazione che ha portato Apple a presentare solamente [il secondo miglior fatturato di tutti i tempi](https://www.macstories.net/news/apple-q1-2019-results-843-billion-revenue-the-first-holiday-quarter-decline-since-the-iphones-introduction/).

Ha influito sui risultati anche il programma di sostituzione batterie a costo ridotto, che per mesi e mesi ha spinto milioni di persone a cambiare la batteria prima di arrivare a cambiare iPhone. E qui Apple ha perso molti soldi ma ha fatto la cosa giusta, come [scrive l’analista Carolina Milanesi](https://twitter.com/caro_milanesi/status/1090372175784116224):

<blockquote class="twitter-tweet"><p lang="en" dir="ltr">The battery program was the right thing to do for <a href="https://twitter.com/hashtag/AAPL?src=hash&amp;ref_src=twsrc%5Etfw">#AAPL</a>. Not because they are a charity business but because it means delaying revenue rather than upsetting customers and losing them!</p>&mdash; Carolina Milanesi (@caro_milanesi) <a href="https://twitter.com/caro_milanesi/status/1090372175784116224?ref_src=twsrc%5Etfw">January 29, 2019</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

Meglio perdere fatturato ma conservare un cliente soddisfatto che guadagnare nel breve ma perdere per strada i clienti.

Invece Apple dovrebbe fare la cosa giusta e pubblicare le cifre di vendita in unità dei suoi prodotti. Era rimasta l’unica a farlo e ha smesso; dovrebbe riprendere. Così facendo non fermerà speculazioni e articoli imbecilli che annunciano l’apocalisse per una diminuzione delle vendite; anzi, semmai la mancanza di trasparenza incoraggerà le voci peggiori della rete a lanciarsi nelle analisi più sganciate dalla realtà.
