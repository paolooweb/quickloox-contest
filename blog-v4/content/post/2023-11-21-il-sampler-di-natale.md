---
title: "Il sampler di Natale"
date: 2023-11-21T12:37:15+01:00
draft: false
toc: false
comments: true
categories: [Hardware]
tags: [Teenage Engineering, Ep-133, Po-33]
---
Il design è *come funziona*, diceva Steve, giusto? Eppure, se il funzionamento non viene trasmesso attraverso una estetica, ne esce un oggetto brutto che nessuno vuole. Quindi estetica e funzionalità devono andare a braccetto e l’una completare l’altra, per onorare il detto di cui sopra.

Ora, sarò io che ho i miei difetti, ma trovo centratissima e accattivante l’estetica di [EP-133 K.O. II](https://teenage.engineering/store/ep-133/), *sampler*, *sequencer* e *composer* di Teenage Engineering.

Non sono un professionista del settore e ignoro tutte le sfumature che bisogna applicare alla valutazione per dare un giudizio: eppure è come se l’aspetto fisico mi comunicasse qualcosa anche della funzione. Non so che cosa facciano quei pulsanti, ma mi ispirano; sembrano, danno l’idea di essere al posto giusto, nel modo giusto. Sono ammirato; lo vorrei nel mio studio di registrazione se ne avessi uno.

O vogliamo guardare al *sampler* [PO-33 K.O.!](https://teenage.engineering/store/po-33/)? Sembra un portachiavi, eppure ha l’aria di un prodotto effettivamente utile, e a ottimo livello. Nel catalogo c’è una serie intera di oggetti simili, allo stesso prezzo. Non è un singolo capriccio del progettista ma c’è un pensiero e c’è una logica di vendita.

Sono oggetti che dovrei studiare per settimane prima di imparare a usare a livello base; molto probabilmente da me diverrebbero soprammobili. Fatta questa doverosa premessa, se qualcuno pensa a dei regali di Natale costosi e inutili per eccesso di denaro in cassa, sappia che accetto prodotti Teenage Engineering con estrema gratitudine.