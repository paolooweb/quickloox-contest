---
title: "Voto il Cubo"
date: 2020-04-01
comments: true
tags: [Cube, March, Madness, 512, Pixels]
---
È la finalissima della [March Madness di 512 Pixels](https://512pixels.net/2020/03/mac-madness-2020-the-final-round/).

I risultati delle eliminatorie sono stati variamente sorprendenti, ma immagino per chiunque abbia delle opinioni minimamente formate sui computer Apple.

Il Cubo fu un disastro nelle vendite nonostante un design strepitoso. Un po’ come il primo Macintosh. Chi lo ha avuto spesso lo usa ancora e non ho sentito alcun proprietario lamentarsene.

Il penultimo MacBook Pro è un buon Mac portatile ma non passerà alla storia né sarà usato da qui a dieci anni.

Forza Cubo!
