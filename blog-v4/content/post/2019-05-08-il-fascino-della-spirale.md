---
title: "Il fascino della spirale"
date: 2019-05-08
comments: true
tags: [Apple, logo, Inkbot]
---
A pensarci dopo, era ovvio. E però per arrivarci dovevo vederlo.

Inkbot Design mostra all’inizio di [una lunga pagina dedicata all’evoluzione del logo Apple](https://inkbotdesign.com/apple-logo-design/) che la mela attuale è costruita a partire dalle circonferenze che si trovano nella spirale logaritmica della sezione aurea.

È una cosa così semplice – vedendola – e sofisticata che si rischia la sindrome di Stendhal.

Sono innamorato.