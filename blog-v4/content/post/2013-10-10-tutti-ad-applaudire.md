---
title: "Tutti ad applaudire"
date: 2013-10-10
comments: true
tags: [iTunes, Apple]
---
Abituati come siamo a considerare apparecchi interfacce sottosistemi e processori, rischiamo di perdere di vista cose possibili solo in questo mondo nuovo.<!--more-->

Scrive **Stefano**:

>L’ho sempre trascurato considerandolo una kermesse di secondo piano.
Poi quest’anno spinto dalla curiosità dei nomi in ballo ho dato una occhiata, tra i quali in particolare gli spettacoli di Lady Gaga e Katy Perry.

>Possiamo discutere all’infinito sulla qualità o meno delle interpreti ma quello che ho visto è una serie di interpretazioni vere, con musiche dal vivo e artisti che non si sono mai tirati indietro, usciti esausti dopo una bella ora di coreografie e canti.

>No, iTunes Festival è un signor festival, dura un mese intero, e l’organizzazione è pazzesca, per non dire la regia e la qualità dei filmati.

>Sembra che dietro a tutto questo ci sia una realtà che abbia da sempre organizzato eventi del genere con pop star internazionali.

>Appena mi dicono che Apple trascura il suo lato creativo musicale mi metto a ridere e cambio canale.

Musica, per di più musica pop, oltretutto senza pretese culturali o di profondità. Eppure oggi, se non è Apple a organizzare [una cosa così](http://www.itunesfestival.com), chi altri? Ci vuole solo l’applauso, anche se non mi hanno messo i [Tangerine Dream](http://www.tangerinedream.org).
