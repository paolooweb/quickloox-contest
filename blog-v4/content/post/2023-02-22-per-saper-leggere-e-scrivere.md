---
title: "Per saper leggere e scrivere"
date: 2023-02-22T01:50:48+01:00
draft: false
toc: false
comments: true
categories: [Internet]
tags: [Tom Scott, Scott]
---
Si fa sempre fatica a digerire tutta l’informazione che circola, eppure di questo periodo mi ritrovo a iscrivermi a newsletter che teoricamente sono buone.

L’ultima in ordine di tempo è quella di [Tom Scott](https://pad26.aweb.page/subscribe).

Per riuscire a scrivere cose sensate, bisogna partire da leggere cose sensate. E l’informazione è sempre tanta, ma non potrà mai essere troppa. Con tutto quello che c’è da imparare.