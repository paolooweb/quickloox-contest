---
title: "Ammissione impossibile"
date: 2020-09-13
comments: true
tags: [ddrescue]
---
Ci eravamo lasciati a gennaio 2019 con il [tentativo di recuperare i dati perduti su un disco Ntfs di amici tramite una duplicazione del disco con dd](https://macintelligence.org/blog/2019/01/24/dal-floppy-a-dd/), stroncato dalla mano innocente e inesorabile della secondogenita.

Il vero problema non era quello genitoriale, ma l’estrema lentezza della procedura. Così ho deciso di riprovare con [ddrescue](https://www.gnu.org/software/ddrescue/), programma parente di dd e, come dice il nome, indicato per soccorrere dischi in difficoltà.

ddrescue non è affatto più veloce di dd, anzi. Però salva un log dal quale si può riprendere un salvataggio interrotto da un blackout o da una figlia informaticamente sciagurata.

Bene. Settembre 2020: ce l’ho fatta. Quei dannati settantacinque giga di dati sono stati finalmente recuperati e messi su un più normale disco Hfs+.

Ci ho messo effettivamente meno di venti mesi. Durante l’estate del 2019 e quella del 2020 il lavoro di ddrescue si è fermato, con il proprietario al mare e un blackout a casa a spegnere tutto.

E poi a settembre 2019 stavo arrivando a destinazione… quando, a pochissimo tempo dalla conclusione del lavoro, è morto il disco di recupero, quello buono, su cui riportare i dati. Ho dovuto ricominciare.

Oggi, dopo circa dieci mesi dal secondo tentativo, l’operazione è riuscita. Il paziente è vivo, parlo del mio amico che finalmente rivedrà i suoi dati.

Ammetto che c’erano opzioni più rapide ed efficaci, di cui si può leggere nei commenti al primo link, frutto della competenza di [Sabino](https://melabit.wordpress.com). Ma io non sapevo che dd o ddrescue si sarebbero rivelati così lenti. Quando ho capito che erano lenti, ho fatto tentativi di variare la configurazione in modo da andare più veloce. Il primo problema è che per dominare ddrescue occorre una laurea all’università del tempo libero abbondante; il secondo è che ogni tentativo equivaleva a ripartire da capo con il lavoro.

Soprattutto, ammetto che se inizio un esperimento su Mac, sono disposto a molto pur di vedere come va a finire.