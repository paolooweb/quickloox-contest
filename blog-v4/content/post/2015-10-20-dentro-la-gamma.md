---
title: "Dentro la gamma"
date: 2015-10-20
comments: true
tags: [iWork, iCloud, beta, Pages, Keynote, Numbers]
---
Sono appena usciti i nuovi iPhone, i nuovi iMac, iOS, OS X, Watch OS, Apple TV, quello che si vuole. Eppure sono straconvinto che l’aggiornamento più significativo di tutti sia [iWork](http://www.apple.com/it/productivity-apps/whats-new/pages/).<!--more-->

L’elenco delle migliorie apportate a Pages, Keynote, Numbers è lungo e non banale, comprensivo anche di AppleScript, compatibilità con l’esterno, retrocompatibilità con versioni più vecchie (per Apple è una mossa ardita) e tipografia.

In più c’è un cambiamento ancora più significativo e importante degli altri, non solo perché mi permette di titolare con un gioco di parole particolarmente stupido: iWork su iCloud è fuori dalla beta.