---
title: "Molti milioni di opinioni"
date: 2016-03-01
comments: true
tags: [Ssd, Google]
---
Una statistica che mi sono appena inventato sancisce che il maggiore pericolo di perdita di dati deriva dall’ascolto dei consigli qualunque su marche e modelli di dischi da comprare.<!--more-->

I consigli in questo campo sono semplicemente futili. C’è in ballo il calcolo delle probabilità e di conseguenza, se si considera un insieme di miliardi di dischi, l’esperienza del singolo su uno, due, ma anche dieci o cinquanta dischi conta talmente poco che si fa prima a contarla come zero.

Sui dischi meccanici [si è già detto tutto](https://macintelligence.org/posts/2013-11-22-i-lanci-del-disco/). Su quelli a stato solido (Ssd), invece, finora c’è stato molto da scoprire.

Ora possiamo presumere con una certa fiducia che, conclusione veloce, [i dischi Ssd risentono più dell’età che dell’uso](http://www.zdnet.com/article/ssd-reliability-in-the-real-world-googles-experience/). Gli Ssd hanno meno probabilità di un disco meccanico di guastarsi durante la loro vita utile, ma un rischio più alto di perdere dati.

Per le conclusioni accurate c’è [l’apposito studio](http://0b4af6cdc2f0c5998459-c0245c5c937c5dedcca3f1764ecc9b2f.r43.cf2.rackcdn.com/23105-fast16-papers-schroeder.pdf), che sarebbe da leggere prima di fornire qualsiasi parere in materia.

In quanto si basa su *sei anni* di attività dei centri dati di Google attivi ventiquattr’ore su ventiquattro, dieci modelli diversi di disco e un numero totale di dischi che non viene fornito, ma è dichiarato sufficiente a generare *molti milioni di giorni/disco*.

Per capirci, l’esperienza di chi spara il suo consiglio e ha avuto tre Ssd per cinque anni ciascuno è di 5.477 giorni/disco, considerando due anni bisestili. Per pareggiare un’esperienza ipotetica di cinque milioni e mezzo di giorni/disco, di pareri come il suo ne servirebbero altri mille.