---
title: "Rifiuti della società"
date: 2014-09-02
comments: true
tags: [AppStore, iTunes, Apple, app]
---
Apple ha fatto una gran cosa, con la pubblicazione di una pagina che dettaglia i [dieci motivi più importanti](https://developer.apple.com/app-store/review/rejections/) per i quali una app viene rifiutata su App Store.<!--more-->

Insieme, i dieci motivi sono responsabili del 58 percento dei rifiuti, quasi tre su cinque.

Il 14 percento del totale, la voce prima in classifica sopra tutte le altre, è la mancanza di informazioni altrimenti richieste all’atto della presentazione dell’app. Una volta su sette.

Poi si scende bruscamente sotto il dieci percento. La seconda motivazione in assoluto è la presenza di *bug*, difetti di programmazione.

Insieme, le prime due voci sono responsabili di oltre un caso su cinque.

Poi non si rispetta l’accordo di licenza. A pari merito, c’è un lavoro insufficiente in termini di interfaccia grafica.

Il resto delle prime dieci motivazioni è quasi autoevidente. Chiaro che chiunque di noi, al posto di chi manca avanti iTunes, rifiuterebbe una *app* non definitiva, per esempio; o con un nome ufficiale troppo diverso da quello sotto cui viene presentata.

App Store veniva spesso descritto come un ente abbastanza spietato e oscuro nelle proprie intenzioni. A leggere queste informazioni, non sembrerebbe.