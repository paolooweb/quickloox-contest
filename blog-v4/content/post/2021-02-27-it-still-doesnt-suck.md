---
title: "It (Still) Doesn’t Suck"
date: 2021-02-27T01:50:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [Big Sur, BBEdit, Catalina] 
---
Avendo saltato Catalina, sono stato per due anni senza pensare troppo agli aggiornamenti di sistema e non mi ricordavo più di quanto possano essere numerosi, specialmente nei primi mesi. Big Sur ha appena ricevuto l’aggiornamento .2.2, [il quinto dalla sua uscita](https://support.apple.com/en-us/HT211896#macos1122), e questo la dice lunga sulla frequenza: grosso modo, è un riavvio al mese.

A seconda di come si sia lasciato Mac e delle sue impostazioni, i programmi potrebbero riaprirsi o meno e, in caso positivo, riaprire automaticamente i documenti che erano rimasti aperti, oppure no.

Ma [BBEdit](https://www.barebones.com/products/bbedit/) riapre in ogni caso i  documenti rimasti aperti, anche se non sono stati salvati.

E consente di recuperare quelli *che per errore sono stati chiusi senza essere stati prima salvati*. Per quanto ne so, nessun altro fa una cosa del genere.

Naturalmente, questo comportamento è personalizzabile e può essere inibito se risulta sgradevole.

È il parametro della pace dei sensi, della tranquillità assoluta, del Nirvana del testo. Comunque vada, BBEdit ti supporta attivamente. Ed è proprio vero che *it (still) doesn’t suck*.

*Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile.*