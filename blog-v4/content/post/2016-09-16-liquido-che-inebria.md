---
title: "Liquido che inebria"
date: 2016-09-16
comments: true
tags: [Europa, tasse, Irlanda, SeekingAlpha]
---
Nello svilupparsi della [pantomima](https://macintelligence.org/posts/2016-08-31-un-posso-falso/) miliardaria del contenzioso tra Commissione Europea e Irlanda sulle tasse di Apple, va segnalato l’approccio finalmente pragmatico di SeekingAlpha che ha fatto la cosa più concreta e precisa possibile: ha [tracciato i flussi di contante](http://seekingalpha.com/article/4005597-much-tax-apple-pay) di Apple.

Ne risulta che il contante viene tassato complessivamente attorno al diciotto-diciannove percento e le tasse sul contante ammontano al settanta percento della tassazione totale dichiarata, tra il venticinque e il ventisei percento.

(Ancora una volta il punto non è che sia tanto o che sia poco, ma che cosa accada veramente oltre le chiacchiere).

Come si concilia questo dato, incontestabile trattandosi di contante, con le affermazioni della Commissione, per cui l’Irlanda dovrebbe prelevare tredici miliardi ad Apple che avrebbe pagato lo 0,005 percento delle tasse effettivamente dovute se non ci fossero stati accordi precedenti con il governo irlandese?

*SeekingAlpha* nota come Apple abbia risparmiato circa venticinque miliardi in cinque anni in tasse sul contante, grazie ad accordi favorevoli.

L’ordine di grandezza delle pretese della Commissione Europea è la metà di questa cifra. Si noti che l’Europa, nel primo trimestre 2016, ha rappresentato [un quinto](http://images.apple.com/it/pr/pdf/q2fy16datasum.pdf) del *business* di Apple.

Tradotto: lo 0,005 percento è fumo negli occhi. L’obiettivo della Commissione è alzare l’aliquota di Apple.

Ancora tradotto: l’Europa è evidentemente l’unico posto del mondo dove Apple può conseguire rilevanti vantaggi fiscali, concessi dai governanti dell’Europa, che non piacciono ai governanti dell’Europa.

Vedere tutti quei soldi ha ubriacato qualcuno. Che non è Tim Cook.