---
title: "Pre-verità"
date: 2017-05-02
comments: true
tags: [Surface, Mac, iPad, Microsoft, Apple, MacBook, Pro, Studio, iMac, Nadella]
---
Si fa un gran rumore su *fake news* e *post-truth*, post-verità, perché le elezioni le ha vinte quello sbagliato.<!--more-->

(Questa non è una opinione politica o un *endorsement* di una parte; semplicemente, quelli che hanno perso hanno usato le stesse armi e in abbondanza. Solo che hanno perso e allora accusano l’avversario di avere barato meglio. È una cruda constatazione; avesse vinto l’altra parte, si potrebbe scrivere esattamente la stessa cosa).

Nel campo circoscritto di questo blog, a preoccuparmi maggiormente è la *pre-truth*, la verità raccontata in anticipo. Un esempio.

[Le vendite di Surface esplodono mentre è tiepida la domanda di iPad](http://appleinsider.com/articles/16/10/21/microsoft-surface-sales-boom-amid-tepid-ipad-demand), ottobre 2016.

[È il tempo più meraviglioso dell’anno — per Surface!](http://bgr.com/2016/12/12/2016-macbook-pro-vs-surface-book-switch/), dicembre 2016, dal blog di Microsoft.

>Mai come oggi la gente sta passando dai Mac a Surface.

(Bgr commenta la notizia parlando di [abbandono di MacBook Pro](http://bgr.com/2016/12/12/2016-macbook-pro-vs-surface-book-switch/)).

[Surface aiuta l’unità *device* di Microsoft mentre le vendite di telefoni crollano](http://www.cnbc.com/2017/01/26/microsofts-nadella-says-surface-sales-growing.html), gennaio 2017. L’amministratore delegato Satya Nadella:

>Sono entusiasmato dalla risposta dei clienti a Surface Studio, la nostra più recente innovazione della linea Surface e una nuova categoria di apparecchio.

E adesso un grano di verità: a giugno 2016, si vendeva un Surface Book – il portatile – [ogni nove Surface Pro](http://www.techradar.com/news/mobile-computing/tablets/microsoft-has-sold-nearly-10-times-more-surface-pro-4s-than-surface-books-1323945) (la tavoletta).

Dopo di che, gli ultimi risultati finanziari: il fatturato della linea Surface [è sceso dal 26 percento](https://www.engadget.com/2017/04/27/microsoft-surface-sales/), da 1,1 miliardi di dollari a 831 milioni.

1,1 miliardi come picco di Surface? Un momento. La linea Surface fa concorrenza come minimo a Mac (Book contro MacBook, Studio contro iMac) e a iPad (Pro contro Pro). Nel trimestre delle meraviglie di Surface, con l’abbandono di MacBook Pro a ritmi mai visti prima, il fatturato di Mac e iPad [ammontava a 12 miliardi e 777 milioni di dollari](http://images.apple.com/newsroom/pdfs/Q1FY17DataSummary.pdf). Undici volte la linea Surface.

In attesa dei nuovi risultati finanziari di Apple previsti giusto oggi, il lettore faccia i conti e azzardi, a partire dalle premesse fornite, a) quanta gente avrà mai abbandonato MacBook Pro per Surface; a) quanta gente avrà acquistato Surface Studio, che costa oltre tremila dollari e, se si vendesse molto, spingerebbe in altissimo il fatturato.

Risposte oneste a queste due domande saranno assai più vicine alla verità di tanta fuffa appena passata al pettine.