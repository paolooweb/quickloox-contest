---
title: "Diventare grandi"
date: 2024-03-23T00:01:22+01:00
draft: false
toc: false
comments: true
categories: [Software, Internet]
tags: [Excel, The Race, Williams, Vowles, James Vowles, Fry, Pat Fry]
---
Vita vissuta.

Un cliente mi parla degli sviluppi dell’azienda. *Abbiamo parlato con la persona che lavora al recupero crediti e abbiamo capito che non è più possibile andare avanti con Excel. Così abbiamo preso un tool apposta che contiamo di integrare con il gestionale generale*.

Vita digitale.

Da *The Race*, [Gli scioccanti dettagli dietro la dolorosa rivoluzione di un team di Formula 1](https://www.the-race.com/formula-1/shocking-details-behind-painful-williams-f1-revolution/):

>Non è un segreto che Williams abbia dovuto combattere per molti anni con strutture sottofinanziate e una cultura arretrata, che qualcuno definisce venti anni indietro alla concorrenza.

Tuttavia, prosegue l’articolo, si può capire quanto la situazione fosse disperata solo ora che è partita la stagione 2024 e il responsabile del team James Vowles e il *chief technical officer* Pat Fry hanno parlato apertamente delle difficoltò di questo inverno e di come abbia impattato su un inizio senza ottenere punti in classifica.

Viene fuori che progettavano secondo criteri antiquati. Per esempio, una scocca moderna è composta da poche migliaia di componenti e non più da poche centinaia. Ed è solo una parte della macchina. Il più delle volte, il numero di parti da gestire si è moltiplicato per dieci.

È diventato necessario smettere vecchie abitudini consolidate, come costruire il nuovo modello usando parti trovate in magazzino e modificandole, che però però restavano vecchie per esempio nella composizione, magari in metallo invece che in fibra di carbonio.

Non è esagerato, si scrive nell’articolo, affermare che la realizzazione di un nuovo modello partiva da un foglio Excel contenente un elenco di oltre ventimila parti. Vowles, che proviene da Mercedes, si esprime così:

>L’elenco su Excel era uno scherzo. Impossibile da navigare, impossibile da aggiornare.

Ora è in corso la migrazione da Excel a un sistema dedicato che permetta realmente il tracciamento delle componenti dall’acquisto al magazzino all’utilizzo. Prima era frequente che nessuno sapesse dove si trovava un certo pezzo e che lo si cercasse alla cieca sugli scaffali. La fine dell’articolo:

>L’aspettativa deve essere che questo team passerà i prossimi inverni ad avere le cose pronte in tempo e a lavorare sulle prestazioni invece che spararsi in un piede.

Se un’azienda si basa su Excel, deve ancora diventare grande. Le dimensioni non contano.