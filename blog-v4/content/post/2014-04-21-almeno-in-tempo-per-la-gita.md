---
title: "Almeno in tempo per la gita"
date: 2014-04-21
comments: true
tags: [Melton, Jobs, Pasqua, uova]
---
Seppure contento di avere proposto ieri il [ricordo di Don Melton su Steve Jobs](https://macintelligence.org/posts/2014-04-20-ricordi-a-sorpresa/), devo ammettere che il *post* giusto avrebbe dovuto riguardare le [vere uova di Pasqua secondo la tradizione](http://qz.com/200297/happy-easter-eggs-eastern-europe/).<!--more-->

Di nuovo auguri a tutti (che non piova, quanto meno).
