---
title: "La mia banda intona al clock"
date: 2023-06-19T15:45:44+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [Apple Watch, The Verge]
---
Omaggio a The Verge per questo bell'articolo sugli eroi dimenticati di watch: [i pulsanti di sgancio del cinturino](https://www.theverge.com/23743095/apple-watch-band-release-x206-assembly-button-of-the-month).

Dentro ciascun meccanismo di sgancio i pulsanti sono tre, anche se ne vediamo solo uno, e gli altri due interagiscono al momento di staccare o attaccare un cinturino.

Il sistema prevede anche perni e molle (!), a una scala talmente ridotta che Apple ha fatto incetta in Svizzera di macchine per la lavorazione a controllo numerico, di quelle abitualmente acquistate da Rolex e in grado di lavorare con tolleranza massima di cinque micron, dove l’ordinarietà sta sui cinquanta/settanta.

Ciascuna macchina costa un paio di milioni di dollari e Apple ha superato Rolex nella spesa per il loro acquisto.

Nonostante questo, il meccanismo richiede precisione tale che lo scarto dei pezzi lavorati è significativamente alto.

L’autore ripete due volte che è per il software open source, le soluzioni aperte, gli standard universali, ma la soluzione Apple è migliore delle altre in giro. Come peraltro può confermare chiunque abbia affrontato una minima attività di cambio del cinturino.

Alla fine si conferma che la Apple migliore salta fuori quando guardi al più minuscolo dei dettagli e salta fuori una storia degna, se non di un romanzo, almeno di un documentario. Per scegliere un cinturino intonato.