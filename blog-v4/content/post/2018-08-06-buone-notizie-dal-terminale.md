---
title: "Buone notizie dal Terminale"
date: 2018-08-06
comments: true
tags: [Newsbeuter]
---
Ho chiesto a Google se esistessero buoni lettori di *feed* Rss da usarsi nel Terminale e lui è stato generoso. Esiste ampia scelta e apparentemente di buona qualità.

Per il momento ho scelto [Newsbeuter](https://newsbeuter.org/), Obiettivo: non farmi rimpiangere Vienna e fornire utilità ragionevolmente paragonabile, con guadagno di spazio schermo e riduzione del carico di lavoro sul processore. Farò sapere.
