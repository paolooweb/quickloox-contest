---
title: "Fatti personali"
date: 2018-08-24
comments: true
tags: [Surface, Europa, Gdpr, privacy, Gamecocks, football, Microsoft, Nfl]
---
Doveva essere un post su Surface imposto a suon di soldi da Microsoft alla National Football League, con [risultati da commedia](https://macintelligence.org/posts/2016-10-19-tavoletta-rasa/), e su iPad [liberamente adottato da squadre universitarie](http://macdailynews.com/2018/08/23/south-carolina-gamecocks-inundated-with-apple-ipads/) come South Carolina, con risultati positivi.

Nel seguire il link di *MacDailyNews* ho però scoperto che [il sito è inaccessibile](https://southcarolina.rivals.com/news/ipad-inondation-gamecocks-tracking-player-film-study), molto probabilmente perché il mio *browser* si collega dall’Europa e l’Europa ha fatto passare la normativa [Gdpr](https://www.eugdpr.org/), la quale dovrebbe teoricamente tutelare la *privacy* e invece, come già la [tremenda legge italiana](https://www.siapav.it/informativa-sulla-tutela-dei-dati-personali-legge-19603/), non fa che moltiplicare la burocrazia, impennare il numero dei clic inutili su avvisi inutili e, come qui, perdere l’accesso a pagine gestite da persone che vogliono impiegare il proprio tempo amministrativo su questioni più serie.

 ![Impossibile entrare in questo sito dall’Europa](/images/rivals.png  "Impossibile entrare in questo sito dall’Europa") 

Ricordo che, se una cosa buona arriva da Internet, è la possibilità di collegamento senza confini. Se una iniziativa stabilisce nuovi confini o limita la possibilità di collegamento, per quanto bene intenzionata, c’è un problema.

Accetto il disaccordo a patto che mi si spieghino gli effetti benefici di Gdpr sulla [raccolta arbitraria di dati personali da parte degli aggeggi Android](https://digitalcontentnext.org/wp-content/uploads/2018/08/DCN-Google-Data-Collection-Paper.pdf) – *dieci volte* quello che raccoglie un iPhone – e che cosa migliori Gdpr di fronte a strumenti come Onavo, un servizio di Vpn che fa capo a Facebook e la cui app iOS [è stata fatta rimuovere](https://mobile.twitter.com/panzer/status/1032404589360758785/photo/1) da Apple per esagerazione nella raccolta di dati sensibili (in sostanza Facebook arriva a conoscere tutto quello che uno fa su Internet. Tutto).

Immagino che Gdpr mi difenda da pericoli assai maggiori che in questo istante non vedo. Certo che se in cambio qualcuno si difende dal mio *browser*, quanto io ci abbia guadagnato deve essere valutato con attenzione.
