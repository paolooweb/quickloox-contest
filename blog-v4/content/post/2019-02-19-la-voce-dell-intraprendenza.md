---
title: "La voce dell’intraprendenza"
date: 2019-02-19
comments: true
tags: [Dicenomicon, Drafts, Editorial, Playgrounds, Swift]
---
Sono a scrivere in parte con [Editorial](https://omz-software.com/editorial/), non aggiornato da tempo, solo che ha lo *scripting* Python incorporato e questo lo rende eternamente giovane o quasi; se non fa una cosa che ti serve, puoi provarci tu.

Davo un’occhiata a [Drafts](https://getdrafts.com) del quale non mi convince la formula ad abbonamento per fare le cose veramente serie. Le cose serie però sono notevoli e comprendono lo sviluppo delle azioni per automatizzare comportamenti del programma.

Ho già raccontato di [Swift Playgrounds](https://www.apple.com/swift/playgrounds/) e di come ispiri a scoprire Swift, veramente, giocando. Mi sta venendo la curiosità di scoprire quanto si possa andare avanti prima di dover per forza anteporre la logica di programmazione all’ozio giocoso e lo dice uno che si è messo in gioco imparando (i fondamenti di) Swift per scriverci un [manualetto](http://www.apogeonline.com/libri/9788850333387/scheda).

I Comandi rapidi di iOS, appena entri nell’idea, suscitano quasi automaticamente la voglia di creare nuove cose, anche minime, ma autonome.

Per via del nuovo iPad Pro ho dovuto aggiornare [Dicenomicon](http://www.dicenomicon.com) e la nuova versione incoraggia vieppiù la personalizzazione e la piccola programmazione di sequenze di dadi (sì, è un programma per lanciare dadi).

Dovunque mi giri, su Mac e specialmente su iOS, tutto quello che tocco sussurra *scriptami*. È un canto delle sirene cui, diversamente da Ulisse, viene da concedersi anche a scapito di cose più urgenti.

È la voce dell’intraprendenza. L’aggeggio che amplifica la nostra intelligenza dagli anni settanta è lì a suggerirci di cambiare passo, girare pagina, aprire una nuova fase, per dirla in stile da politicante. A differenza del politicante, consiglio vivamente di ascoltare l’aggeggio con attenzione.