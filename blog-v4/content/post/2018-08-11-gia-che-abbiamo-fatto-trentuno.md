---
title: "Già che abbiamo fatto trentuno"
date: 2018-08-11
comments: true
tags: [HyperCard]
---
Oggi HyperCard compie trentuno anni e lo voglio ricordare in modo adeguato, cioè ricordare come se ne è già parlato: un [capolavoro del software](https://macintelligence.org/posts/2017-07-16-la-carta-della-nostalgia/) che giustamente [suscita nostalgie](https://macintelligence.org/posts/2016-06-18-mezzi-e-fini/) ed è una cosa bella (quanti software possono *davvero* suscitare una nostalgia?). Purché si vada oltre la lacrimuccia e la commemorazione, per [accettare sfide di oggi](https://macintelligence.org/posts/2017-07-22-il-codice-il-codex-il-coding/) con identico spirito.

In quanto la percentuale di nostalgia per il software, in realtà, è bassa. Quella alta è la nostalgia di come eravamo trentun anni fa e ci sta tutta, ma la verità finale è che la parte migliore della vita arriva tra un attimo.

E infatti c’è chi [ha rifatto HyperCard nel Web](https://www.vipercard.net), aspetto grafico, interfaccia e tutto.