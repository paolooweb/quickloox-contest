---
title: "Live blogging in contumacia"
date: 2013-09-10
comments: true
tags: [Macworld, iOS, iPhone, iPad]
---
L’agenda mi impedisce di trovarmi stasera davanti a un computer nel momento in cui Apple annuncerà le cose che ha deciso di annunciare.<!--more-->

Nel contempo, l’amico e bravissimo Andrea Grassi animerà il *live blogging* dell’evento su [Macworld.it](http://macworld.it) a partire dalle 18:45 di oggi. Per sapere tutto, e chiacchierare di ogni cosa, sono sicuro che non mancherà niente. Ecco l’invito:

<blockquote class="twitter-tweet"><p>Ci vediamo alle 18.45 su questa pagina per il live blogging della presentazione dei nuovi iPhone (via <a href="https://twitter.com/Macworld_it">@Macworld_it</a>) <a href="http://t.co/51rvkh1Nvd">http://t.co/51rvkh1Nvd</a></p>&mdash; Macworld Italia (@Macworld_it) <a href="https://twitter.com/Macworld_it/statuses/377412682946600961">September 10, 2013</a></blockquote>
<script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>

In caso di imprevisti positivi, mi farò vivo anch’io non appena riesco. Avanzerà comunque qualche sorpresa!