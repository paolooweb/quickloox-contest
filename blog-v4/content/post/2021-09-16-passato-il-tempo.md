---
title: "Passato il tempo"
date: 2021-09-16T00:38:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [Apple Watch, MacDailyNews, John Gruber, Daring Fireball, Pc Magazine, Sascha Segan, Mark Gurman, Ming-chi Kuo] 
---
Pare incredibile dover chiarire ancora che le immagini pubblicate dei siti di *rumors* sono creazioni di fantasia realizzata da un professionista della computergrafica, oppure false piste seminate da Apple per identificare le talpe disposte a fare trapelare all’esterno informazioni di prodotto non autorizzate.

Invece no, c’è gente che guarda, si interessa, ne discute come se fossero vere, esprime giudizi. Su disegni (3D, ray tracing, ma sempre disegni restano). C’è gente che commenta disegni.

*MacDailyNews* effettua un [rapido passaggio sui rendering completamente sbagliati di watch Serie 7](https://macdailynews.com/2021/09/15/the-leakers-got-apple-watch-series-7-design-completely-wrong/) e in più cita [India Today](https://www.indiatoday.in/technology/news/story/apple-beat-tipsters-with-the-apple-watch-series-7-showing-it-is-finally-able-to-keep-secrets-1853055-2021-09-15):

>Di Apple Watch Series 7 si è parlato per mesi e si è sempre suggerito l’arrivo di una sue riprogettazione radicale. Ogni speculazione puntava a un design flat-edge [a bordo piatto], come quello degli iPhone dell’anno scorso. Si è visto un design flat-edge, ma sui nuovi iPad mini.

iPad mini che nessuno aveva messo in conto, degli specialisti in anticipazioni. Ancora *India Today*:

>Renderizzazioni del nuovo Apple Watch erano già arrivate su Internet. Analisti e commentatori da tutto il mondo avevano TUTTI confermato il design flat-edge.

Una mandria, insomma, o un gregge.

Nel quale pascolava anche [Cult of Mac](https://www.cultofmac.com/752777/apple-watch-7-last-minute-substitution/):

>È questo l’aggiornamento che Apple voleva presentare quest’anno? O è una sostituzione all’ultimo minuto su cui a Cupertino hanno dovuto ripiegare perché quello che veramente volevano presentare, semplicemente, non era ancora pronto? L’evidenza ci fa dire che sia il secondo caso.

[Risponde](https://daringfireball.net/linked/2021/09/15/old-last-minute-hardware-switcheroo) John Gruber su *Daring Fireball*:

>Lo hardware non funziona così. Questi design sono decisi con grande anticipo. In effetti, i design flat-edge potrebbero anche essere autentici, solo che riguarderebbero i modelli del prossimo anno. Proprio perché Apple prepara lo hardware con grande anticipo; mesi fa erano già avanti con la progettazione degli Apple Watch del 2022.

È semplicemente impossibile che watch Serie 7 sia stato un progetto di riserva.

Per ultimo, *PC Magazine* dedica spazio tramite Sascha Segan alle notizie, che non lo sono state, di un [iPhone con connessione satellitare](https://www.pcmag.com/opinions/that-whole-iphone-satellite-thing-was-bogus):

>Sia l’analista di primo piano Ming-chi Kuo che l’estremamente affidabile Mark Gurman sono stati convinti da un rumor per cui iPhone 13 avrebbe parlato con i satelliti, qualcosa che non è assolutamente accaduto durante la presentazione di iPhone 13.

>In effetti, iPhone 13 nemmeno utilizza la banda n53 di 5G, la banda terrestre 5G proprietà dell’operatore satellitare Globalstar, che avevo supposto essere il granello di verità dentro il rumor. Ero in errore anch’io. […]

>Forse non sapremo mai da dove sono nate le supposizioni, ma mi chiedo cupamente se possano avere a che fare con qualche sorta di malversazione su titoli azionari. Le azioni di Globalstar sono schizzate dopo l’articolo di Kuo, per crollare.

Cretini che guardano disegni, speculatori che tentano colpi gobbi inventandosi il nulla. A leggere in giro sembra che le fake news facciano male solo alla politica o alla salute. Fanno male dappertutto invece. Siccome nessuno interverrà mai per regolare la situazione, probabilmente tocca a noi staccare la spina dell’attenzione alle sedicenti anticipazioni, per ragionare sui prodotti veri, infinitamente più interessanti.

Il tempo delle ming-chi-ate sugli annunci Apple, rendiamolo passato.

*Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile.*