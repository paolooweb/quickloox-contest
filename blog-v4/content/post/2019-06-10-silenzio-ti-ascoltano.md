---
title: "Silenzio, ti ascoltano"
date: 2019-06-10
comments: true
tags: [Arment, Apple, Mac, Pro]
---
Marco Arment è stato molto critico di Apple negli ultimi anni ed è una persona di indubbia reputazione nella comunità. Il suo ultimo post si intitola [Apple è in ascolto](https://marco.org/2019/06/09/apple-is-listening):

>È difficile capire quando Apple sia in ascolto. Parlano in modo conciso, di rado e solo quando sono pronti, dicendo assolutamente nulla nel frattempo, persino quando tutti ci lamentiamo di una linea di prodotto come fosse in preda alle fiamme. Fanno grandi progressi, anche se sovente con rinunce ardite che mai annullano, per cui un silenzio prolungato dovuto al fatto che dobbiamo tenerci le cose come stanno è indistinguibile da un silenzio prolungato dovuto al fatto che la risposta non è ancora pronta. Ma da inizio 2017 a oggi c’è stato uno spostamento importante nella giusta direzione e non potrebbe essere più chiaro oggi: Apple è di nuovo in ascolto, sanno ancora il fatto loro e Mac è tornato.

Arment, quando è contento e quando no, vale migliaia di [dischi rotti](http://www.macintelligence.org/blog/2019/06/07/dischi-rotti/) che giocano al professionista so-tutto su Facebook.

Pezzo da leggere. Ulteriore dimostrazione: tra le cose che ancora sono da aggiustare, indica il Pro Display XR da seimila dollari, ma non perché sia sbagliato; perché è un *monitor di riferimento*, roba da specialisti del colore, che compete con oggetti dal prezzo multiplo di quello. Agli sviluppatori come lui, argomenta, servirebbe piuttosto una versione a prezzo ragionevole del monitor di iMac 5k.

Oppure Mac mini: per lui è *un computer così buono che per [recensirlo](https://marco.org/2018/11/06/mac-mini-2018-review) ho iniziato e terminato una carriera di YouTuber*.

Avessi visto uno dei dischi rotti scrivere la stessa cosa con la stessa chiarezza. Silenzio, Apple è in ascolto e blaterare è inutile.