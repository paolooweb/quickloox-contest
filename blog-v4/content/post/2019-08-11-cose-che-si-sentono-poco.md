---
title: "Cose che si sentono poco"
date: 2019-08-11
comments: true
tags: [AirPods, Foot, Locker, Motorola, Amd, Fortune]
---
Se gli AirPods fossero il prodotto di una società dedicata, quella società [sarebbe ben posizionata tra le prime 500 della classifica di *Fortune*](https://fortune.com/2019/08/06/apple-airpods-business/), davanti a nomi come Foot Locker, Motorola, Amd.

Poi uno dice che sono sciocchezze, che manca l’innovazione, che non nascono prodotti di rilievo, che sono solo cuffiette. Beh, starei a sentire interessato la spiegazione di come creare cuffiette sciocche, non innovative, e venderne cinquanta milioni di esemplari in un anno.

Ma è il marketing che convince gli sciocchi, replica.

Mi guardo attorno e non è che veda tutta questa comunicazione persuasiva a tema AirPods.
