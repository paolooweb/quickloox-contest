---
title: "Indispensabilmente inutile"
date: 2015-05-28
comments: true
tags: [Pages, Scribus, Dtp, opensource, Wesnoth]
---
Non capita spesso di esortare allo scaricamento di un software che non è il caso di usare. Eppure lasciare inutilizzata sul server la nostra copia di [Scribus](http://www.scribus.net/?p=239) 1.5.0 è un grosso errore. Ecco il senso dell’operazione.<!--more-->

>Dopo molti anni di lavoro e oltre un migliaio tra bug sistemati e funzioni aggiunte, lo Scribus Team è lieto di annunciare Scribus 1.5.0: una anteprima della prossima versione stabile 1.6.0, presentata primariamente a scopo di collaudo, così che più persone possano aiutarci a identificare e sistemare i bug. Non è ancora abbastanza stabile per l’utilizzo in situazioni di produzione effettiva, ma può aiutare a familiarizzare con la nuova interfaccia e le nuove funzioni, il numero delle quali è quasi raddoppiato rispetto alle versioni stabili della serie 1.4.x.

Scribus è passato attraverso progressi enormi rispetto all’ultima versione ed è arrivato il momento di conoscerlo, perché è maturo più che a sufficienza per stare sull’*open source* anche per l’impaginazione invece che ricadere su Adobe. Meglio farlo ora che c’è la versione di collaudo, per essere pronti a sfruttarlo con l’arrivo delle prime versioni stabili.

Che venga pubblicizzata una versione dichiaratamente non adatta a un ambiente produttivo non deve spaventare. La prassi seguita qui è simile a quella di altri progetti *open* come per esempio [Battle for Wesnoth](http://wesnoth.org): si alternano versioni principali di sviluppo, dove aggiornamento dopo aggiornamento si infilano nuove funzioni e si fa progredire il programma, a versioni stabili pronte per essere utilizzate, dove gli aggiornamenti riguardano stabilità e sicurezza. Di solito la versione dispari è quella di sviluppo e la versione pari è diretta all’utilizzatore finale. Ecco perché Scribus 1.5.0 non è stabile ma è il caso di impratichirsi da ora: da questa uscita, previa eliminazione di bug e problemi, deriverà la versione stabile 1.6.0.

Le funzioni aggiunte sono veramente una marea e menziono solo una banalità in mezzo a mille cose più importanti: Scribus importa file da praticamente tutti i programmi di impaginazione esistenti. Incluso [Pages](http://www.apple.com/it/mac/pages/).

Scribus 1.5.0 è (talvolta) inutile sul piano pratico. Ma averlo presente è indispensabile.