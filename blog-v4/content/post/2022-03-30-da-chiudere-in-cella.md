---
title: "Da chiudere in cella"
date: 2022-03-30T02:02:41+01:00
draft: false
toc: false
comments: true
categories: [Software, Internet]
tags: [Lapsus$, Okta]
---
Qualcuno avrà letto delle ultime imprese del gruppo di hacker Lapsus$, che farebbe capo a un adolescente inglese. Una delle loro bravate riguarda la violazione dei servizi di [Okta](https://www.okta.com), una società specializzata in servizi di autenticazione, che a sua volta ha permesso l’intrusione nella rete interna di [Sitel](https://www.sitel.com), azienda di soluzioni per *contact center*.

Lapsus$ sarebbe entrato in possesso di [un file di password conservato dentro *un foglio di calcolo*](https://techcrunch.com/2022/03/28/lapsus-passwords-okta-breach/). Si può facilmente immaginare quale foglio di calcolo sia esattamente.

Discorso già fatto: quel foglio di calcolo è diventato pericoloso suo malgrado. Non importa quanto sia potente, quanto sia comodo, quanto qui e quanto là. È diventato un profilattico della produttività da ufficio, indossando il quale si pensa di poter indulgere in qualunque comportamento, anche assurdo, anche insicuro.

Perché, anche ammettendo che possa essere vagamente normale conservare liste di password dentro un file in chiaro, che vantaggio porta farlo dentro un foglio di calcolo? Perché non un testo? Perché non un database? Perché non un documento Html?

Non so quanto i componenti di Lapsus$ rischieranno a livello giudiziario. Ma io metterei in cella quelli che creano elenchi di pass… di qualunque cosa dentro un foglio di calcolo.

Non la cella dal foglio, eh (che poi, in italiano, non parlando di un alveare, dovrebbe pure chiamarsi in altro modo).