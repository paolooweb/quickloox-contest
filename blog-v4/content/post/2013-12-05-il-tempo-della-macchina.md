---
title: "Il tempo della macchina"
date: 2013-12-05
comments: true
tags: [backup]
---
Sono seriamente preoccupato per la produttività nel Paese per via dell’attenzione che si dedica a Time Machine. Utility di configurazione, per guardare che cosa sta facendo, quanto trasferisce, quanto ci mette.<!--more-->

La mia impostazione di Time Machine: non la conosco. So che il servizio è acceso. Ho anche tolto l’indicatore dalla barra dei menu.

Una volta al mese lancio l’interfaccia di recupero per vedere se tutto è a posto. Quando verifico il disco interno, verifico anche il disco Time Machine.

Poi faccio *altro*. Lavoro, imparo, mi diverto, scrivo, leggo, gioco, chiacchiero, curioso, sperimento, navigo. Un computer non può servire a controllare quello che fa il disco rigido esterno. È uno spreco indicibile. Le macchine servono se vanno da sole, per liberare il nostro tempo.

Dovessi mai avere un problema, quello che serve sapere sta quasi certamente su [Apple OSX and Time Machine Tips](http://pondini.org/OSX/Home.html), note di Apple a parte.

La parte migliore di Time Machine è proprio la sua invisibilità.