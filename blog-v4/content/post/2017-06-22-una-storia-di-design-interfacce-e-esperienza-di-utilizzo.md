---
title: "Una storia di design, interfacce ed esperienza di utilizzo"
date: 2017-06-22
comments: true
tags: [XScreenSaver, Antmaze]
---
Ho il vezzo di tenere su Mac tutti i salvaschermo carini che trovo e da sempre ho installati quasi tutti quelli di [XScreenSaver](https://www.jwz.org/xscreensaver/), una raccolta eccellente di quelli che sono fioriti su Linux, pronti da installare su Mac.

XScreenSaver è partito come progetto molto tempo fa ed esiste da anni e anni.

Uno dei salvaschermo, AntMaze, mostra una processione di formiche variamente colorate che attraversa un semplice tracciato 3D, mostrato in lenta rotazione.

Mia figlia, due anni e dieci mesi, tutte le mattine si siede in grembo al papà che insieme a lei commenta per qualche minuto il procedere delle formiche.

Tenendo premuto il trackpad e muovendo il dito, è possibile variare l’orientamento spaziale del tracciato (immagino che funzioni anche con un mouse).

Lo ha scoperto lei.