---
title: "Vacanze e verifiche"
date: 2021-01-06
comments: true
tags: [ Terminale, emacs, Epifania, Befana, Swift, Playgrounds]
---
Sventato io che il 6 gennaio di un anno fa ho pubblicato un [elenco di buoni propositi](https://macintelligence.org/blog/2020/01/07/ogni-maledetta-epifania/). Così mi tocca la verifica e persino a scuole ancora chiuse.

**Voglio scrivere qui ogni giorno**. _Check_. La quotidianità è stata ripristinata, a volte in leggero ritardo. Nessun giorno è andato perso. L'impegno viene evidentemente rinnovato. Ci tenevo a farcela, ci sono riuscito.

**Voglio raggiungere un obiettivo preciso con il Terminale**. Ehm. Ho fatto qualche progresso con emacs. Troppo poco. Insufficienza abissale, proposito traslato a questo 2021.

**Voglio frequentare con regolarità Swift Playgrounds**. Ottima partenza, poi mi sono arenato, con dei perché, sempre arenato però. _Intelligente, ma non si applica_. Insufficiente. Sono atteso al varco della programmazione della regola della mano destra per uscire dal labirinto.

**Voglio diversificare le fonti di reddito**. Buono. Si può sempre fare meglio, non tutto è andato come volevo, non va mai esattamente come si vuole. Però nel complesso è andato molto bene. Per questo 2021 la missione è consolidare i risultati raggiunti, fare bene e migliorare la gestione tra professione e famiglia che ancora presenta sovrapposizioni migliorabili. Non che la pandemia sia stata di aiuto. I risultati positivi sul lavoro influenzano quelli meno positivi sull'autoapprendimento: il tempo a disposizione nel secondo semestre è stato una frazione di quello nel primo. Constatazione, non scusa.

In totale un ottimo, un pessimo, un insufficiente, un buono. Come media siamo appena sopra la sufficienza; il dato della media pesata è più alto, alcune sfide contavano più di altre.

Per quest'anno? Confermate, migliorare, rientrare dai debiti di autoapprendimento, più papà quando serve fare il papà e professionista quando si lavora, per quelle volte in cui la situazione si è ingarbugliata.

Ancora una volta, leggo volentieri le pagelle altrui. Si parlava pure di pizza l'anno scorso, ma la situazione sanitaria ispira poco. Contiamo anche su miglioramenti indipendenti dalla nostra volontà.
