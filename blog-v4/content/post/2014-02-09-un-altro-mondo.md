---
title: "Un altro mondo"
date: 2014-02-09
comments: true
tags: [iPad, iPhone, Apple, regex, Ubersense]
---
Se scegli Apple, stai dietro a gente che coltiva da anni con cura maniacale una [espressione regolare per riuscire a trovare automaticamente in un testo qualsiasi indirizzo web concepibile](https://gist.github.com/gruber/8891611).<!--more-->

Guardi prodotti di un’azienda all’avanguardia nei procedimenti di fabbricazione il cui amministratore delegato, in un mondo pieno di telefonini con uno schermo grande, alla domanda se sia contro un iPhone con lo schermo grande, invece di prometterlo con ardore [risponde](http://blogs.wsj.com/digits/2014/02/07/apple-still-a-growth-company-cook-says-in-journal-interview/) che non se ne parla *fino a che la tecnologia non è pronta*, il che racconta perfettamente che qualità di schermi vendano gli altri.

Leggi un articolo che [sostiene la fine](https://macintelligence.org/posts/2014-02-07-e-la-somma-che-fa-il-totale/) dell’innamoramento della gente per i tablet e due giorni dopo la notizia che il bob e lo slittino statunitense [analizzano le discese](https://twitter.com/Ubersense/status/428528053350699010) sfruttando iPad, una cosa mai vista prima.

<blockquote class="twitter-tweet" lang="en"><p>USA Bobsled &amp; Skeleton coaches record and analyze their athletes sliding over 80 mph using Ubersense on iPad. <a href="http://t.co/HT56bHnbPl">pic.twitter.com/HT56bHnbPl</a></p>&mdash; Ubersense (@Ubersense) <a href="https://twitter.com/Ubersense/statuses/428528053350699010">January 29, 2014</a></blockquote>
<script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>

Se scegli Apple, stai in un altro mondo. Se sia migliore si può discutere. Che si distingua dal grigiore, proprio no.