---
title: "Ma le gambe"
date: 2022-10-13T17:36:56+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [metaverso, Facebook, Microsoft, avatar, Accenture, Trio Lescano]
---
Prefiguro una realtà aumentata, ibrida, multiplanare, innestata, ausiliaria, orientata alle funzioni, che potrebbero prevedere il controllo delle luci così come l’editing condiviso di un documento oppure l’aggiornamento della situazione sulla plancia di un boardgame.

Al buon cuore degli sviluppatori e degli strateghi permettere di ottenere questo risultato in piena libertà di strumenti, che potrebbero essere uno schermino da tenere in tasca, un  cinquecento pollici che si avvolge per duecentosettanta gradi attorno alla mia postazione o anche un proiettore di ologrammi che mi lascia manipolare oggetti virtuali come se li avessi davanti alla faccia. Mi vanno bene anche gli occhiali o il casco, va bene tutto, davvero vorrei che la realtà supplementare, coibentata, potenziata, progressiva, multistrato fosse accessibile anche mediante una forchetta munita di microchip, se necessario.

Il proprietario di Facebook la pensa notoriamente in modo diverso e ha perfino cambiato identità all’entità che possiede Facebook stesso per scommettere *all-in* sulla trasformazione non della realtà, ma del soggetto umano destinato a percepirla, da infilare dentro un universo alternativo dove l’orientamento non è sulle funzioni, ma sulla loro rappresentazione. Non ruoto un solido 3D; divento un oggetto software che agisce su un altro oggetto software, uno che rappresenta me e l’altro che rappresenta il solido.

Lo scopo non è farmi ruotare l’oggetto, ma osservarmi nella mia forma software mentre interagisco con la forma software del solido.

Nulla da dire sul fatto che possa funzionare come modello economico; due miliardi di persone in questo momento si fanno osservare mentre buttano contenuto dentro un rullo infinito e danno vita a un discreto modello economico.

Solo, il primo Macintosh nasceva per fare lavorare meglio le persone, non per farle lavorare sotto osservazione.

Pare che per ora il modello economico consista nella vendita di occhialoni in grado di proiettare una realtà virtuale di cui abbiamo visto esempi già da trent’anni.

Occhialoni capaci di [provocare malesseri ai militari](https://twitter.com/counternotions/status/1580636813139419136) che li indossano sperimentalmente, guarda caso con software Microsoft. Gli stessi che [hanno facilitato così tanto il lavoro degli allenatori di football americano](https://macintelligence.org/posts/2016-10-19-tavoletta-rasa/) in cambio di visibilità.

<blockquote class="twitter-tweet"><p lang="en" dir="ltr">Why can&#39;t the U.S. military wait for the version 3.0?! <a href="https://t.co/S3sIfIo2KS">https://t.co/S3sIfIo2KS</a></p>&mdash; Kontra (@counternotions) <a href="https://twitter.com/counternotions/status/1580636813139419136?ref_src=twsrc%5Etfw">October 13, 2022</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

Occhialoni che consentiranno di usare… Office dentro la realtà virtuale di cui sopra. Come detto in altra occasione, è la prova vivente che Office non serve a fare lavorare meglio le persone, ma a controllarle. Microsoft sta costruendo [un sistema con cui saprà chi è seduto in ufficio e dove è seduto](https://www.officernd.com/hybrid-work-software/microsoft-workplace-experience/). Uffici non Microsoft.

La qualità del mondo in cui si userà Office sotto osservazione? [Quella Accenture](https://twitter.com/shiringhaffary/status/1579892468316676096), magari?

<blockquote class="twitter-tweet"><p lang="en" dir="ltr">Accenture&#39;s virtual office in the metaverse <a href="https://t.co/igqThmEWHZ">pic.twitter.com/igqThmEWHZ</a></p>&mdash; Shirin Ghaffary (@shiringhaffary) <a href="https://twitter.com/shiringhaffary/status/1579892468316676096?ref_src=twsrc%5Etfw">October 11, 2022</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

Nel frattempo, la novità principale riguardante lo sviluppo del progetto è che ora [gli avatar hanno le gambe](https://www.youtube.com/watch?v=njvp-E8gzqA). Sì, perché finora erano busti fluttuanti.

<iframe width="560" height="315" src="https://www.youtube.com/embed/njvp-E8gzqA" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Sono per l’avanzamento della tecnologia e per l’innovazione, consapevole che qualcosa può non funzionare o essere sbagliata. Ma non c’è altro modo che innovare, se vogliamo migliorare.

Sia pure da questa posizione, riesco a trovarmi molto perplesso.

<iframe width="560" height="315" src="https://www.youtube.com/embed/048RGeAZfJA" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>