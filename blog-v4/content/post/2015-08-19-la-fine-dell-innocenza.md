---
title: "La fine dell’innocenza"
date: 2015-08-19
comments: true
tags: [watch]
---
In circostanze impreviste sono divenuto proprietario di un watch. È infine arrivato il momento di vedere se il computer da polso cambia le cose come ha fatto il computer da tasca.<!--more-->

Suggerisco caldamente la visita di un Apple Store a chi può. Online si può sapere tutto di prezzi e materiali, però in negozio si provano ed è facile che venga un dubbio o un ripensamento una volta in contatto fisico con l’oggetto. Il commesso personale ti sta dietro, sa tutto sulle differenze e caratteristiche e anche sui prezzi: a parte i modelli Edition dei Paperoni, per i comuni mortali esiste una doppia dozzina di combinazioni tra cassa e cinturino. Tutto può essere provato e le unità da prova contengono un video introduttivo che mostra in pochissimo tempo le funzioni fondamentali e attiva anche le comunicazioni a impulso: è possibile sentire come viene sollecitato il polso quando arriva una indicazione da una *app*.

Il *feeling* dei materiali è notevole, curatissimo. Il cinturino di pelle è morbido e resistente; quello di acciaio è lavorato con precisione millimetrica e avvolge molto bene il polso. Sia uomini che donne potrebbero considerare il quadrante da trentotto oppure quello da quarantadue millimetri, per gusto estetico e praticità personali.

La scatola è rifinita come al solito e contiene anche un cofanetto apparentemente in grado di resistere a una atomica tattica, dove eventualmente riporre l’oggetto per protezione. Una bustina di materiale vellutato conserva le maglie tolte dal cinturino d’acciaio per adeguarlo esattamente alla circonferenza del polso; l’operazione è semplicissima e richiede minima manualità, tanto che l’ho compiuta persino io.

L’aggancio magnetico del caricabatteria è sconcertante, perché l’aderenza alla faccia interna dell’unità è istantanea e perfetta, ma non si sente neanche un *clic*. La forza di attrazione è insufficiente a tenere sollevato un watch d’acciaio per il cavo di alimentazione e sufficiente a iniziare il sollevamento.

Non posso rispondere a domande sul peso, perché da vent’anni non tenevo un oggetto al polso e qualunque sensazione riferisca, non fa testo. Levate dal cinturino le maglie in eccesso, il polso è vestito con buonissima precisione.

Domani, magari dopodomani, un po’ di software. Oggi la vera considerazione: in questo secolo mai avevo acquistato un prodotto Apple a prescindere da un utilizzo professionale diretto. Indiretto, potrebbe avere un senso. I prossimi mesi saranno interessanti.