---
title: "Si spezza e non si spiega"
date: 2019-04-27
comments: true
tags: [Samsung, Galaxy, Fold, iPhone, iFixit]
---
Se ricapitolo bene, abbiamo un computer da tasca prossimamente in vendita [per quasi duemila dollari](https://macintelligence.org/posts/2019-02-22-un-cambiamento-di-spessore/), che [si spacca nelle mani dei recensori privilegiati](https://macintelligence.org/posts/2019-02-22-un-cambiamento-di-spessore/) prima ancora della data di uscita (in misura sufficiente da fare slittare la data) e che di cui [non si può divulgare lo smontaggio](https://hardware.slashdot.org/story/19/04/26/017219/ifixit-pulls-galaxy-fold-teardown-at-samsungs-request).

Sull’ultima notizia bisogna dire che Samsung ha qualche ragione, dato che correttezza imporrebbe anche a iFixit di attendere l’uscita ufficiale prima di pubblicare il *teardown*.

La situazione nel complesso fa comunque pensare che si possa trattare del lancio (fallito) del secolo. O, alternativamente, di un’azienda un po’ troppo rigida per stare al passo, che fatica a tenere il controllo della tecnologia di produzione e del proprio marketing. Insomma.
