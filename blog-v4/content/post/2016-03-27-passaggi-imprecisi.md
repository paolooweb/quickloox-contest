---
title: "Passaggi imprecisi"
date: 2016-03-27
comments: true
tags: [Pasqua, AppleScript, Calendario, MacScripter]
---
A Pasqua viene naturale fare gli auguri accompagnandoli con un *easter egg*, sorprese nascoste in un programma. Calendario ha deciso di darmi una doppia mano.<!--more-->

La prima è questa gemma comparsa ieri nelle notifiche, con Pasqua e Lunedì dell’Angelo in arrivo nella stessa giornata:

 ![Due feste al prezzo di una](/images/pasqua-angelo.png  "Due feste al prezzo di una") 

La seconda è uscita dal calendario delle festività italiane.

 ![Certamente prevedibile](/images/data-pasqua.png  "Certamente prevedibile") 

Scrivere *la data esatta di questa festività non è facilmente prevedibile* è fare un torto alle decine di pagine web che descrivono l’algoritmo di calcolo della Pasqua.

Per i programmatori veri c’è [codice Perl](https://developer.apple.com/library/mac/documentation/Darwin/Reference/ManPages/man3/Date__Calc.3pm.html) a disposizione sul sito sviluppatori di Apple.

Per chi coltiva o vorrebbe coltivare AppleScript, suggerisco una splendida [pagina su MacScripter](http://macscripter.net/viewtopic.php?id=18179). Eccellente per provare e imparare.

Per quelli come me c’è il comando di Terminale `ncal -e` che fornisce la data della Pasqua dell’anno corrente, o dell’anno desiderato (da aggiungere in coda al comando).

Per i curiosi, Pasqua ricorre nella prima domenica dopo la prima luna piena dopo l’equinozio di primavera. I dettagli sono persino su [Wikipedia](https://en.wikipedia.org/wiki/Computus).

Per tutti, tanti tanti auguri sentiti e pieni, soprattutto precisi, di un bel Passaggio.