---
title: "L’ultimo prodotto"
date: 2017-05-17
comments: true
tags: [Jobs, Apple, Park, Wired, Levy]
---
Troppo facile la retorica su Steve Jobs o la nostalgia. Più impegnativo riconoscere il valore autentico del suo lascito.<!--more-->

Per questo ho faticato ad arrivare in fondo alla lunga esclusiva di *Wired*, per il quale Steven Levy ha potuto [girare in lungo e in largo Apple Park](https://www.wired.com/2017/05/apple-park-new-silicon-valley-campus).

Si capisce troppo bene e troppo spesso come si tratti dell’ultimo grande lavoro di Jobs, il cui tocco è presente nell’insieme e nei particolari.

Lettura dolorosa perché riporta alla mente la pesantezza della perdita, affascinante perché penetra nella creazione di un’architettura unica per la quale financo i cartoni della pizza sono progettati apposta e brevettati, straniante per la (ri)scoperta di un mondo dove il dettaglio è ancora fondamentale e la passione arriva prima del denaro. Apple è seduta sopra una montagna di denaro, ma è facile capire che qualunque altra azienda avrebbe speso la metà gridando ugualmente al capolavoro.

Non c’era stretto bisogno di Apple Park. Fino a quando Steve Jobs ha fatto capire che, in realtà, c’era e non ce ne eravamo resi conto.

>E così il suo nome comparirà sull’auditorium. Ma chiunque cercasse le impronte di Steve Jobs su Apple Park le troverà dovunque—nei bagliori delle curve dell’Anello, in mezzo agli alberi e in migliaia di altri dettagli che possiamo e non possiamo vedere.

L’ultimo, grande prodotto.