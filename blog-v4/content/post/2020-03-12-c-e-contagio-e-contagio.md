---
title: "C’è contagio e contagio"
date: 2020-03-12
comments: true
tags: [iPad, Cina, Apple, Huawei]
---
In Cina il governo ha ordinato alle scuole di procedere a erogare lezioni online per combattere la diffusione del virus.

Anche in Italia.

In Cina [gli iPad vanno a ruba](https://asia.nikkei.com/Spotlight/Coronavirus/China-demand-for-e-learning-bites-into-Apple-s-iPad-supply). Destinazione: gli studenti che seguono le lezioni online.

In Italia, beh, mah.

Non che i cinesi abbiano scoperto iPad a causa della pandemia. L’anno scorso Apple ha venduto in Cina oltre quaranta milioni di iPad, seguita da Huawei con qualche milione in meno e da tutti gli altri che si contendono le briciole.

Negli Stati Uniti, il West Virginia [si è rivolto ad Apple per istruire i docenti all’uso di Swift](https://apple.slashdot.org/story/20/03/10/226256/west-virginia-taps-apple-to-teach-teachers-to-teach-kids-swift) in modo che tutti i ragazzi a cavallo tra elementari e medie possano studiare Computer Science al meglio delle loro possibilità.

In Italia non saprei.

Il virus va fermato, mentre bisogna restare altrimenti aperti al contagio delle buone idee e delle buone pratiche.