---
title: "Dall’algoritmo al ritmo"
date: 2017-09-22
comments: true
tags: [Knuth, Fantasia, Apocalyptica]
---
Tra mille anni la storia della tecnologia, parlando di questi ultimi cinquant’anni, si ricorderà probabilmente solo di Donald Knuth, l’uomo a cui andrebbe dedicato il pensiero computazionale, l’autore della [Art of Computer Programming](http://www-cs-faculty.stanford.edu/~knuth/taocp.html) che mostra come sì, sia un’arte, con perfetto metodo scientifico e inesauribile senso dell’umorismo.<!--more-->

A gennaio Knuth compirà ottant’anni e li festeggerà con l’esecuzione di una sua [opera multimediale per organo a canne](http://www-cs-faculty.stanford.edu/~knuth/fant.html), composta dietro ispirazione dell’Apocalisse di San Giovanni e delle simbologie numeriche in essa contenute.

A ottant’anni vorrei essere un decimo di quello che è lui e dovrei migliorare di dieci volte per arrivarci. E lui intende dedicare i prossimi otto anni al completamento di uno dei volumi della *Art*.