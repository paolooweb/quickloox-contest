---
title: "Cinque per cinque"
date: 2023-05-24T23:56:31+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [Gruber, John Gruber, Daring Fireball, Windows, macOS, WWDC]
---
*The Verge* pubblica un articolo sulle [cinque più importanti novità presentate durante a Microsoft Build 2023](https://www.theverge.com/23734104/microsoft-build-2023-ai-bing-copilot), tutte centrate sulla fornitura della inevitabile *intelligenza artificiale* in punti diversi nel sistema, compresa la Task Bar.

John Gruber si chiede polemicamente su *Daring Fireball* [quanti annunci di intelligenza artificiale avrà Apple a WWDC](https://daringfireball.net/linked/2023/05/23/microsoft-build-top-5). Bisognerebbe ricordare che Apple fa le cose migliori quando segue la sua strada invece di scimmiottare la moda del momento. E che gli annunci su Windows sono di un’azienda *indietro* rispetto ad Apple sui sistemi operativi.

Vorrei comunque che ne avesse *uno*: integrazione forte di assistenti generativi dentro e intorno agli strumenti di sviluppo. Per programmatori e sviluppatori si tratta di un bel passo avanti, che consente di risparmiare una marea di tempo su compiti prima tediosi e complicati, snellisce la stesura di codice prototipale, trova bug eccetera. Se fossi uno sviluppatore, la vorrei tutta questa roba, subito, al sapore di Apple.

Ma dentro macOS? Riesco a immaginarmi qualche operazione marginale (conta poco) o di nicchia (interessa pochi) che probabilmente sarebbe più comodo digitare dentro un assistente generativo piuttosto che eseguirla a mano o con il supporto dei Comandi rapidi, del Terminale, di AppleScript e via discorrendo. Quello che vale in tema di programmazione vale in tema di scripting: un assistente generativo per buttare giù al volo lo schema di un AppleScript o di un Comando rapido farebbe piacere. Anche qui, operazioni di nicchia: sono strumenti meravigliosi, che otto o anche nove persone su dieci ignorano.

Li vogliamo cinque annunci che farebbero ricordare questa WWDC?

- Refactoring di iCloud. Sincronizzazione istantanea, che funziona sempre, ovunque. Livello aggiuntivo di un terabyte a cinque dollari al mese. Interfaccia di controllo remoto di tutti gli apparecchi nel proprio piano.
- Integrazione di git nei sistemi operativi e prima di tutto in macOS. Ecco, qui l’assistente generativo sulla ricerca dei contenuti potrebbe avere un senso.
- Il servizio Health+. Cinque euro al mese e, oltre allo scontato consulto video con il medico di base o lo specialista, anche la programmazione di canali da seguire per avere informazione vera sulla salute, come fa Fitness+ per lo stare in forma. Salute dura e pura, non benessere olistico o pseudoscienze. Se poi ci fosse anche School+…
- 3DOS per stampanti, realtà virtuali o aumentate, interfacce futuribili. Non solo il realityOS di cui si vociefra.
- Siri in locale, che funziona anche in assenza di rete.
- Bonus: Apple Mesh. Rete Wi-Fi che si genera spontaneamente tra persone consapevoli in possesso di apparecchi Apple anche a casa, tra gli apparecchi Apple disponibili, con possibilità di estensione a prodotti indipendenti.

Già che ne ho dati sei dopo averne promessi cinque, se Bare Bones mi fa BBEdit per iPad, altro che indimenticabile. Roba da festa scudetto.

