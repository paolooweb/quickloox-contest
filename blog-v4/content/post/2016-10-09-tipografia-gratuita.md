---
title: "Tipografia gratuita"
date: 2016-10-09
comments: true
tags: [ MacAppware, font, Halloween]
---
Mi pare una buona occasione: registrarsi alla *mailing list* di MacAppware e poter scaricare una [raccolta di quarantacinque font a tema Halloween](http://macappware.com/software/halloween-fonts/), a costo zero.<!--more-->

Non ho bisogno di ripetere – anche se lo sto facendo – quanto sia attaccato all’idea di curare la tipografia nella comunicazione, anche quella personale, anche quella ricreativa, pure quella dei figli o dei nipoti.

Una raccolta così fortemente caratterizzata lo dimostra in modo ideale: basta dare un’occhiata alla grafica che li mostra tutti insieme. Il testo a volte è in tema (certi font si chiamano *Gravestone*, *Wormfood*, *Sailors Grave*), a volte per niente (*Albert Text*, *Distro*, *Augusta*). L’aspetto parla comunque per loro e, specialmente nei meglio riusciti, suggerisce subito con forza un’atmosfera e un gusto. Prevedibili in questo caso, ma appunto qui sta il bello: praticamente è un test di verifica di quanto siano efficaci questi font.

Per questo scaricherei i font anche se Halloween fosse la vigilia di Ognissanti e invece della festa americana dell’orrore ispirasse sentimenti di pace e compassione. È una bella occasione per capire l’importanza di esprimersi *anche* attraverso la tipografia. E ricordarselo anche quando si scriverà la raccomandata all’azienda del gas. Certo, evitando il font *Carbonized Timber*.

(L’ho detto che la raccolta era di quaranta font e ne hanno aggiunti cinque per il periodo che arriva? Sì, ora l’ho detto).

(L’ho scritto che una volta dentro la *mailing list* poi arriva la possibilità di scaricare gratis seicentoventitré font a tema misto? Se il ragionamento sui font di Halloween fosse buono, basterebbe quello a convincere e tutti se ne accorgerebbero dopo. Però sembrerebbe che mi sono dimenticato…)
