---
title: "Pontes asinorum"
date: 2022-05-02T01:14:15+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [Comandi Rapidi, XCode, Apple Frames, MacStories, Federico Viticci, Viticci, Apple Watch]
---
Quando ci si cimenta con l’automazione personale e lo scripting, l’ostacolo più frequente è di caratura minima. Imparare a programmare, creare diagrammi di flusso con la mente, immaginarsi algoritmi, niente di tutto questo. Ci si blocca sulle sciocchezze.

Oggi sono riuscito a installare [TermiWatch](https://github.com/kuglee/TermiWatch) su watch, dopo [essermi bloccato](https://macintelligence.org/posts/2022-05-01-non-vedo-lora/). Il problema? In fase di ricerca e sostituzione, Xcode vuole un Invio che di norma su Mac non è necessario. Non ci ho pensato e mi ero perso in un bicchiere d’acqua. Ora, TermiWatch funziona (ho usato Apple Frames di Federico Viticci per incorniciare la schermata: strumento eccezionale, scaricabile gratis dall’[archivio di MacStories](https://www.macstories.net/shortcuts/)).

![TermiWatch in funzione su watch](/images/termiwatch.png "TermiWatch in funzione. Cornice di watch creata con Apple Frames di Federico Viticci.")

Ancora. Per limare un millimetro di noia dal ripasso delle tabelline, ho composto un banale Comando Rapido che chiede a voce il risultato di una moltiplicazione a caso tra due numeri, ambedue compresi tra uno e dieci.

![Comando Rapido per ripassare le tabelline](/images/macchina-tabelline.jpeg "Quanto fa…?")

Mi sono incastrato su un intoppo ancora più banale del problema: i miei apparecchi sono impostati sull’inglese e io desideravo che il Comando Rapido parlasse italiano, mantenendo l’impostazione del sistema operativo in inglese.

Era sufficiente guardare l’interfaccia, osservare la [Regola del Minuto](https://macintelligence.org/posts/2005-09-29-la-regola-del-minuto/), mantenere la concentrazione. Invece mi sono perso, in una goccia d’acqua invece che in un bicchiere.

In tutt’e due le situazioni, a risolvere l’impasse è stato l’aiuto. **Mastro35** sul mio [gruppo Slack](https://goedel.slack.com) mi ha illuminato sul Comando Rapido; nel caso di TermiWatch ho chiesto educatamente sulla pagina GitHub e gentilissimamente mi sono arrivate persino due risposte quando ne bastava una. La comunità non è sempre così tollerante verso le domande sciocche, ma stavolta ha funzionato.

Il *pons asinorum* arriva per tutti. È piccolo, insidioso, ti coglie mentre sei distratto. Chiedi senza patemi e qualcuno ti aiuta. Vale la pena.

(P.S.: per la prima volta, ho usato la nuova funzione di ricerca interna del blog per ritrovare un post vecchio. Mica male).