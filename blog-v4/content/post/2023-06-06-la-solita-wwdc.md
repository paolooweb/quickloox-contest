---
title: "La solita WWDC"
date: 2023-06-06T02:00:09+01:00
draft: false
toc: false
comments: true
categories: [Software, Hardware]
tags: [Federighi, Craig Federighi, iOS, macOS, Sonoma, watchOS, tvOS]
---
Intanto viene da guardare alla pulizia del format. È scomparso qualsiasi tempo morto ed è quasi impossibile prendersi una distrazione. Ogni minuto è fatto di contenuto.

I detentori del marchio scrivono *Apple silicon* con la esse minuscola e questo, oltre a costarmi una sostituzione di massa sul blog con BBEdit, è un primo fatto gradevole, in un mondo dove la maiuscola passa appena si può e il più possibile a sproposito, pur di sembrare autorevoli e professionali.

Craig Federighi ha annunciato i timer multipli in iOS e ha commentato *viviamo in un’epoca di meraviglie*. Neanche l’ironia deve andare persa, se a esprimerla è una multinazionale con l’occhio perennemente puntato al Nasdaq.

Chi voleva lo hardware ha ricevuto un MacBook Air 15”, non del tutto banale come evoluzione di prodotto. Poi Mac Studio, poi Mac Pro. La transizione a Apple silicon, con la esse minuscola, è completata a tutti gli effetti. L’azienda di hardware più importante al mondo si progetta i chip da sola, su tutta la gamma, dall’orologio al computer d’altri tempi superespandibile.

I critici si sono affrettati a fare notare che Mac Studio e Mac Pro condividono le stesse specifiche. Al momento M2 Ultra è il massimo offerto dalla tecnologia è il discorso va ribaltato: da quando aspettavamo Mac Pro, Apple ha dato il massimo che poteva su Mac Studio, al momento l’espressione più potente e funzionale della linea Mac. I critici della macchina chiusa dovrebbero inneggiare a Mac Pro grazie ai suoi slot di espansione; invece se la prendono con il processore. Questo ci insegna qualcosa su quanto valesse la critica alla chiusura del computer.

Poi il software. Nuove versioni, ovviamente, per Mac, iPhone, iPad, watch, tv. Quest’anno è sembrato che l’ordine fosse concentrarsi sulle app fondamentali. Le modifiche più macroscopiche su iOS riguardano le icone verdi: Telefono, FaceTime, Messaggi. Su watchOS è andato molto lavoro, a giudicare dalle novità annunciate. Davvero iPhone è ovviamente importante per Apple, ma la presunta trascuratezza sulle altre piattaforme appare relativa.

Non ho idea di quanto possa essere interessante la compilazione collaborativa di PDF attraverso iPad, che ha ricevuto molto tempo di presentazione. Non ho ugualmente idea di quanto le modifiche a Stage Manager possano essere rilevanti per uno come me, che ha provato ad adottarlo e [a un certo punto si è arreso](https://macintelligence.org/posts/2023-03-31-e-un-caro-saluto-al-manager/), per manifesta inutilità della funzione. I miei bisogni di multitasking erano già soddisfatti da quello che c’era prima, molto meno conflittuale di Stage Manager con le app. Forse farò un altro tentativo, forse no. Su iPad c’è poco altro da dire; per il prossimo salto qualitativo di iPadOS abbiamo da aspettare un altro giro.

Va anche detto che le modifiche minori ai sistemi operativi sono spesso per numero molto più interessanti degli annunci da presentazione e questo lo scopriremo nelle prossime settimane. Vorrei proprio riuscire a togliere la polvere dalla app Developer e guardare un po’ di cose sulle novità. Vale soprattutto per macOS e tvOS, dove mancano grandi annunci (conta anche il contesto: avere FaceTime, nonché app di videoconferenza come Zoom o WebEx su tv, è interessante). Pensiamo anche che iOS è all’interazione numero diciassette: cambiare la vita alla gente una volta l’anno, e continuare a farlo attraverso quasi due decenni, è tanta roba.

*One more thing*: domani. Più procedevo nell’ascolto del keynote e più riflettevo sul potenziale produttivo e creativo di queste persone, che hanno effettuato un lancio storico, in modo trasversale alle previsioni e ai bene informati, e intanto – come per routine – hanno presentato tre modelli hardware nuovi, aggiunte alla gamma dei processori, cinque sistemi operativi.

Non è routine per nulla e, avesse Tim Cook finito qui, saremmo comunque alle prese con un mare di cose nuove da studiare e da comprare, dove serve. È stata una [WWDC](https://developer.apple.com/wwdc23/) ottima e abbondante. Poi, appunto, c’è ancora qualcosa di rilevante. Ma domani, o sembra che il resto sparisca.