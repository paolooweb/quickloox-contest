---
title: "Il dono delle lingue"
date: 2017-08-03
comments: true
tags: [Windows, Sasha, Thai]
---
Lascio integralmente la parola a **Sasha**. La conclusione è sua, ma la condivido al cento percento.

**Mi chiedono aiuto** per cambiare lingua su un cosiddetto tablet Windows 10 (se non sono Apple, li considero accrocchi senza tastiera, piuttosto che tablet…) acquistato chissà dove in Asia. Dopo il ripristino del sistema operativo, la prima configurazione mi permette di selezionare Thai o Giapponese (vado a intuito, perché sono tutti ideogrammi). E per le altre lingue? Beh, c’è questa <a href="https://msdn.microsoft.com/it-it/library/windows/hardware/dn898477(v=vs.85).aspx" title="Pagina di supporto Microsoft">pagina di supporto Microsoft</a>, che spiega in “pochi e semplici” passaggi come fare.

**Il mondo è pieno di masochisti**.
