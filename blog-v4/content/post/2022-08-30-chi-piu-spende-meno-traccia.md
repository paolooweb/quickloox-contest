---
title: "Chi più spende, meno traccia"
date: 2022-08-30T13:43:32+01:00
draft: false
toc: false
comments: true
categories: [Internet]
tags: [StockApps, Google, Apple, Facebook, Amazon, Twitter]
---
Premesso che classifiche come queste, aride e avulse dal contesto, vanno prese con pinze da laboratorio biologico, sappiamo da *StockApps* [quanti *data point* tracciano le aziende *big tech* su ciascun utente](https://stockapps.com/blog/google-tracks-39-types-of-private-data-the-highest-among-big-tech-companies/): Google 29, Apple 12.

In mezzo Twitter, Amazon e Facebook, che traccia appena più di Apple (per questo bisogna poi considerare il contesto dei dati e non solo i dati come tali).

Poi tutti in negozio a guardare lo scontrino per fare le battute su iPhone che costa come una vacanza, trascurando il supplemento sorveglianza pagato quotidianamente che finisce per colmare qualsiasi differenza di partenza, e oltre.