---
title: "Di tutti i mestieri"
date: 2013-01-03
comments: true
tags: [BBEdit, India, Ink]
---
Trovo straordinario il <a href="http://ink.indiamos.com/2012/04/11/degristling-the-sausage/">resoconto di India, Ink.</a> su come hanno semiautomatizzato il lavoro di pulizia dei libri.<!--more-->

La cosa straordinaria è che si parla di *ebook* e la pulizia è quella *dentro* i libri: *pulire e abbellire i libri elettronici prodotti da un “tritacarne”*, cioè da una conversione automatica.

Un sacco di cose da imparare. Che i libri sono oggetti preziosi e curati, devono esserlo, anche in forma elettronica. Che la qualità non sceglie mai strade comode.

Che sia stato scelto <a href="http://www.barebones.com/products/bbedit/">BBEdit</a> è ulteriormente illuminante.