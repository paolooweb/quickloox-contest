---
title: "Dritti e doveri"
date: 2020-09-20
comments: true
tags: [UE, Apple, Nfc, pay, André]
---
A malapena diventato papà, in informatica sono già nonno perché mi ritrovo a raccontare sempre le stesse storie. Mi consolo a pensare che almeno me ne accorgo.

La storia è quella di quando eravamo più giovani e facevamo collezione di numeri di carta di credito. Per allenare la memoria facevo *shoulder surfing* in fila alla cassa: la sfida era memorizzare il numero della carta nel breve tempo in cui veniva sciorinata.

Personalmente non ne ho mai approfittato. Ho quasi varcato la linea solo una volta, in una occasione tra le più divertenti della mia vita, quando abbiamo fatto perdere del tempo a un venditore di auto di Miami con la pretesa di volere acquistare un’auto al telefono. Lui voleva un numero di carta e lo avevamo… non era ovviamente il nostro.

La morale è che il sistema delle carte di credito, lato sicurezza e lato privacy, è stato pensato da subito come un colabrodo. Si regge sulla premessa che tollerare una percentuale fisiologica di furti e frodi costa meno di metterlo in sicurezza. Agli stati nazionali questa cosa è sempre andata bene. L’uso della carta di credito è totalmente transnazionale.

Poi arriva, tra le tante, Apple che propone pay. Mica è la rivoluzione, le iniziative di questo tipo sono numerose; però io seguo Apple e di lei parlo.

Le banche e le istituzioni finanziarie restano basite [esattamente come l’industria musicale di fronte a iTunes](https://macintelligence.org/blog/2014/11/04/musica-gia-sentita/) e pensano di correre ai ripari con loro imitazioni dell’originale.

Peccato che i buoi siano già scappati dalla stalla, perché Apple non scrive una *vanity app*: costruisce una infrastruttura che, a suon di hardware e software, porta a pagamenti sicuri e privati. iPhone, con Nfc, Secure Enclave, Touch ID e poi Face ID, quando usa pay diventa un terminale di pagamento per persone libere. Il negoziante non vede la mia carta così come nessuno in fila alle mie spalle; se c’è un mariuolo in ascolto di transazioni elettroniche, può rubare un codice usa e getta utile, a lui, per essere gettato. La mia banca sa evidentemente che ho speso del denaro, ma non altro se non l’essenziale. Se mi rubano iPhone, i livelli di sicurezza presenti probabilmente terranno a zero il danno aggiuntivo al furto in sé.

Come le comari di De André in [Bocca di rosa](https://www.youtube.com/watch?v=F7hWpZUQxZ0), le cagnette della finanza sono andate a lamentarsi dal commissario. *Quella schifosa ha troppi clienti, più di un consorzio alimentare*. La differenza è che il commissario è quello europeo.

Ne segue che l’Europa [progetta di forzare Apple ad aprire l'infrastruttura di pagamenti sicuri a terze parti](https://www.bloomberg.com/news/articles/2020-09-17/apple-pay-tech-likely-to-be-open-to-rivals-in-rules-mulled-by-eu).

L’Europa costruisce forse una infrastruttura di pagamenti sicuri che torni a vantaggio dei cittadini? No. Dà vita a un regime di libera concorrenza in cui il cittadino possa scegliere il sistema che preferisce? No. Prende una infrastruttura sicura e la rende insicura per decreto.

Anziani che si fanno turlupinare da una app di *phishing* che gli entra nel sistema di cifratura. Banche alla ricerca di informazioni sui clienti, ansiose come sono di profilarle; istituzioni incapaci di allestire un sistema di pagamento funzionante, che portano tutti i loro bug nella procedura di pagamento attraverso iPhone. Apple che dà vita a una infrastruttura sicura e deve renderla colabrodo per decreto. Per poi essere sbertucciata sui media al momento in cui scoppierà il primo incidente abbastanza grosso.

Chissà se la norma passa. Nel frattempo, complimenti all’Europa. Avrebbero dei doveri ma se la cavano facendo i dritti a spese di chi paga tutti quegli stipendi a Bruxelles.

<iframe width="560" height="315" src="https://www.youtube.com/embed/F7hWpZUQxZ0" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>