---
title: "Altro che venticinque Led"
date: 2023-05-22T01:31:52+01:00
draft: false
toc: false
comments: true
categories: [Hardware]
tags: [BBC, micro:bit, Nominet]
---
Nelle scuole elementari del Regno (Unito) pioveranno presto kit-classe da trenta micro:bit ciascuno, per complessive [settecentomila unità](https://www.bbc.com/mediacentre/2023/bbc-education-digital-creativity-computing-microbit).

[micro:bit] è un computer a uso didattico creato dalla BBC – per un attimo pensiamo tutti alla RAI che mette a punto un computer didattico usabile da settecentomila bambini di scuola primaria – più piccolo di una carta di credito eppure programmabile e ideale per le prime esperienze di *coding* e *making*.

Non si può piacere tutti e neanche micro:bit può. Si è levato del dissenso attorno al fatto che la periferica di bordo per l’output di micro:bit è una matrice Led da cinque per cinque.

Venticinque pallini luminosi. Qualcuno ha detto, non è che ai bambini piace di più uno schermo 4k che oramai si trova anche sui cellulari?

Beh, un conto è guardare e un altro conto è fare. Poi non è sbagliato partire in piccolo con obiettivi alla portata di tutti, che non sono necessariamente banali: legioni di programmatori capaci di creare meraviglie su Macintosh hanno iniziato con il domane uno Zx-80. Qualcuno si è addirittura costruito da solo il computer, come Wozniak con l’Apple I. Con uno Spectrum c’è chi ha imparato quattro comandi Basic in croce e chi ha prodotto Lords of Midnight, un gioco di strategia con trentaduemila locazioni. Un mio amico si era fatto il cavo apposta per potersi collegare alla piattina apposita dello Spectrum e ditemi se non è *making*.

Sono tutte chiacchiere inutili. Lo sguardo di un bambino nel momento in cui avrà imparato ad accendere uno di quei Led. A spegnerlo. Con i *suoi* comandi, scritti da lui. La luce di quello sguardo, moltiplicato settecentomila. Non c’è 4k che tenga.