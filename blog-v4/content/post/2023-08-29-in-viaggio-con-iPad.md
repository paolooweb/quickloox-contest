---
title: "In viaggio con iPad"
date: 2023-08-29T04:54:13+01:00
draft: false
toc: false
comments: true
categories: [Hardware]
tags: [iPad, Snell, Jason Snell, Gruber, John Gruber]
---
Sotto il titolo di un articolo di Jason Snell, [La rinuncia al sogno di viaggiare solo con iPad](https://mjtsai.com/blog/2023/08/28/giving-up-the-ipad-only-travel-dream/), si legge una lunga lista di citazioni di personalità di vario rango, da John Gruber in giù, che lamentano la mancata crescita di iPad come piattaforma alternativa a Mac da usare possibilmente come unica macchina.

Qualcuno scrive che Apple Silicon *ha ucciso il sogno di iPad* dato che un Mac è praticamente leggero e maneggevole uguale; c’è chi dà la colpa ad Apple di avere chiuso iPad al punto di non permettere una serie di cose essenziali che invece su Mac si fanno o si trova un modo per fare; è saltata fuori la vecchia distinzione tra macchine per produrre informazione e macchine per consumarla e così via.

Scrivo al compimento di un lungo periodo passato fuori casa, qualche volta con Mac e iPad, qualche altra con iPad e basta: io trovo Mac e iPad spettacolarmente complementari. E ora scrivo queste note sdraiato prima di dormire, dopo avere riveduto una presentazione per una azienda che sta imparando a comunicare, dopo avere scritto il testo di una brochure e avere inviato alcune email. Si tratta di compiti base, sicuro; però li ho svolti con disinvoltura, senza problemi, dal letto. Keynote su iPad è un oggetto di design nella forma più pura del termine.

Le questioni vere secondo me sono diverse dalla chiusura o dalle funzioni del sistema operativo. Quando Steve Jobs lo presentò, collocò iPad in uno spazio di uso intermedio tra quello di Mac e quello di iPhone. Da allora è passato tanto tempo ma si sono moltiplicati gli sforzi per fare passare iPad come portatile, con la sua Magic Keyboard, il supporto di mouse e trackpad eccetera.

A ma capita rarissimamente di usare iPad come laptop. Non faccio testo; non uso neppure Apple Pencil, che è un accessorio straordinario ed esclusivo di iPad. Devo dire che, potendo intervenire a una riunione degli sviluppatori di iPadOS, direi a Craig Federighi di sviluppare su iPad funzioni fantastiche *per usarlo con Il touch*. Poi sì, la tastiera, ci mancherebbe. Ma iPad è fantastico quando si fa a meno della tastiera; con la tastiera, con il mouse, sì, utile, ma a quel punto è meglio un Mac.

Mac che mi porto in giro perché alcune cose, è vero, con iPad non si fanno. Nel mio caso sono usare InDesign o utilizzare alcune funzioni sofisticate di Google Documenti che non si comportano bene su iPad. Me la prenderei tuttavia con chi sviluppa male le app (Google) e con chi non le sviluppa (Adobe), più che con Apple.

iPad resta il mio portatile, la macchina che mi segue sempre. Fa le cose diversamente da Mac? Altroché. Ma per certi versi è il suo bello, per altri è un’altra macchina, che pensare di voler usare come un Mac è sterile. Ho la sensazione che tante critiche a iPad arrivino da persone che riescono a visualizzare un solo e unico flusso di lavoro possibile per ciascun *task*.

*Gli aggiornamenti in agosto del blog potrebbero essere irregolari o infrequenti.*