---
title: "Colpa di Edo"
date: 2014-06-07
comments: true
tags: [Yosemite, OSX, Continuity, design, MacBookPro, Mavericks, iCloud, keynote, Bing, DuckDuckGo]
---
Ho scritto di getto le righe che seguono, a proposito di [OS X 10.10 Yosemite](http://www.apple.com/it/pr/library/2014/06/02Apple-Announces-OS-X-Yosemite.html), in una *mailing list*.<!--more-->

[Edo](http://www.evkmusic.it) ha espresso la speranza che io le pubblicassi in questa sede.

Così si sa di chi è la colpa! Ecco le righe.

A me è sembrato il migliore [*keynote*] degli ultimi anni: nessun bisogno di tirare in ballo hardware per riempire, un sacco di carne al fuoco su tutti i fronti e dalla prima impressione non sono promesse al vapore. Ho installato la Developer Preview di [Yosemite](http://www.apple.com/osx/preview/) e alla primissima occhiata è a livello di quella di Mavericks, con qualche problema ma ragionevolmente stabile. Ho provato a lavorarci per una giornata - montata su disco esterno però - e tutto è filato liscio.

Non sono ancora in grado di dare giudizi. Come tutti i cambiamenti di *design*, ho trovato indigesto per un'ora il cambio di font di sistema, poi non me ne sono più accorto. Probabilmente con schermi di scarsa risoluzione non è la scelta ottimale. Probabilmente sugli schermi Retina è stupendo. Con un MacBook Pro 17", mi trovo in mezzo.

iCloud dà accesso ai file indipendente dall'applicazione e questo era quanto molti chiedevano. È arrivato. La soglia gratis è rimasta a cinque gigabyte ma per averne venti si pagano 99 centesimi di dollaro al mese. Quindi in Italia saranno tra i dieci e i dodici euro l’anno. Per uno che lavora e ne ha davvero bisogno, è la cifra di un caffè al mese.

Le cose che mi sono rimaste in mente dopo il *keynote* sono la personalizzabilità delle notifiche, comprese le estensioni indipendenti, e Continuity. Suona il telefono in borsa e rispondi dal Mac sulla scrivania, lasci a mezzo la mail su Mac per andare a prendere il treno e sul treno completi la mail da iPhone, esattamente da dove l’hai lasciata. È il primo passo dell'indipendenza finale da quello che hai davanti, a privilegiare i tuoi dati e le tue informazioni, non più se si tratti di un *laptop*, *ultrabook*, *netbook*, *tablet*, *smartphone*, *phablet*, *smartwatch*, *wearable* e la prossima sigla da infilare sulla bocca di tutti. Questo è un passo avanti, un grosso passo avanti.

Mi scoccia l’uso di Bing dove non si può scegliere. Dove si può scegliere, si può avere la privacy di ricerca, grazie a [DuckDuckGo](https://duckduckgo.com).

Già che c'erano, hanno presentato [un nuovo linguaggio di programmazione](http://www.apogeonline.com/webzine/2014/06/06/rivoluzione-swift).

La direzione è chiaramente *basta che sia Apple e poi che sia Mac, iPhone, iPad, iPod e domani chissà che altro, scegli tu*. Mi piace un mondo.