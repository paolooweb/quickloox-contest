---
title: "L’altra marea"
date: 2017-08-01
comments: true
tags: [Fagioli, Betanews, Ubuntu, MacBook, Pro]
---
Brian Fagioli (proprio) scriveva nove mesi fa su Betanews [Il disappunto per i MacBook Pro (2016) spinge alcuni lealisti Apple verso Ubuntu Linux](https://betanews.com/2016/10/30/macbook-pro-2016-disappointment-pushes-some-apple-loyalists-to-ubuntu-linux/). Sì, *lealisti*; non sono persone che hanno preferito un Apple Store a un Media World, ma truppe fedeli a un qualche tipo di potere.

L’articolo arriva a definire il numero degli abbandoni *huge*, ingente, con tanto di offerta speciale di un fornitore di sistemi vista l’abbondanza.

Per un attimo dimentichiamo l’assurdità di un supposto passaggio degli utenti da un hardware (Mac) a un software (Ubuntu) che [gira tranquillamente su Mac](https://wiki.ubuntu-it.org/Installazione/UbuntuMacIntel). Facciamo che sia vero. Vediamo che conseguenze ha avuto sulle vendite di Mac, per quantità. I dati sono anno su anno, quindi confrontano il dato dopo l’annuncio degli abbandoni con l’anno prima dell’annuncio.

* [Aprile-giugno 2017](https://www.apple.com/newsroom/pdfs/Q3FY17DataSummary.pdf): più uno percento.
* [Gennaio-marzo 2017](https://www.apple.com/newsroom/pdfs/Q2FY17DataSummary.pdf): più quattro percento.
* [Ottobre-dicembre 2016](https://www.apple.com/newsroom/pdfs/Q2FY17DataSummary.pdf): più uno percento.

Forse c’era una crescita superiore a questa, prima dell’annuncio, che l’annuncio stesso ha tarpato? Vediamo. I dati di questa tabella si riferiscono alle vendite mesi prima dell’articolo (pubblicato a fine ottobre 2016), sempre anno su anno.

* [Luglio-settembre 2016](https://www.apple.com/newsroom/pdfs/Q4FY16DataSummary.pdf): meno quattordici percento.
* [Aprile-giugno 2016](https://www.apple.com/newsroom/pdfs/q3fy16datasum.pdf): meno undici percento.
* [Gennaio-marzo 2016](https://www.apple.com/newsroom/pdfs/q2fy16datasum.pdf): meno dodici percento.
* [Ottobre-dicembre 2015](https://www.apple.com/newsroom/pdfs/q1fy16datasum.pdf): meno quattro percento.

Le conclusioni sono chiare: c’è stata una marea di abbandoni di Mac verso Ubuntu, che ha avuto il risultato di riportare le vendite di Mac in positivo dopo nove mesi di stagnazione. Certamente è qualcosa di plausibile a patto di spiegare da dove arrivi l’altra marea, quella che d’improvviso si è messa a comprare Mac tanto da colmare ogni vuoto, e come mai Fagioli si sia dimenticato di parlarne.