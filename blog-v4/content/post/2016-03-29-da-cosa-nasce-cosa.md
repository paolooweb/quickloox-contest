---
title: "Da cosa nasce cosa"
date: 2016-03-29
comments: true
tags: [Windows, Microsoft, privacy, Cina]
---
Già detto di come la *privacy* su certi computer Windows sia [carne di porco](https://macintelligence.org/posts/2016-03-19-inconsapevolmente-vendonsi/) per come è trattata dai produttori hardware, va dato atto a Microsoft di [adoperarsi per tutelarla](https://privacy.microsoft.com/it-it/privacystatement) lato software.<!--more-->

Va dato atto?

Mentre le cronache si sono nutrite per settimane del [rifiuto di Apple](https://macintelligence.org/posts/2016-02-17-il-coraggio-e-il-valore/) di creare una versione di iOS su misura per l’Fbi, Microsoft ha messo a punto [una versione di Windows 10 su misura per il governo cinese](https://translate.google.it/translate?hl=it&sl=zh-CN&tl=en&u=http%3A%2F%2Fcompanies.caixin.com%2F2016-03-24%2F100924085.html).

Visto l’atteggiamento della Cina sui diritti umani e sul rispetto delle libertà individuali, viene da chiedersi se le affermazioni di Microsoft valgano anche per questa eccezione.

Viene da chiedersi anche se sia un’eccezione. Windows è software proprietario e nessuno può vedere come è fatto dentro. È la prima volta che succede o l’ennesima? Se è la prima, sarà l’unica?

Una preoccupazione dopo l’altra, il quadro si fa tetro. Windows sta sul novanta percento dei computer desktop (e fortunatamente sugli altri è tutt’altra musica). Sappiamo che sono a rischio *privacy* e sappiamo che Microsoft è disposta a creare versioni di Windows modificate in base ai requisiti delle autorità.

Se, dopo i computer insicuri e il software su richiesta statale, il rapporto tra Microsoft e amministrazioni pubbliche uscisse dai confini etici appropriati, la catena sarebbe *veramente* preoccupante.

Ecco il [precedente](http://www.balkaninsight.com/en/article/romania-jails-ex-minister-over-microsoft-licenses-03-24-2016).

>La Corte di Cassazione della Romania ha condannato a due anni di reclusione l’ex ministro delle telecomunicazioni Gabriel Sandu per riciclaggio, abuso d’ufficio e corruzione in relazione alla concessione di licenze software Microsoft per le scuole.

Sandu, che aveva tre compari, ha incassato tre milioni di euro in mazzette – e ne aveva chiesti altri 1,3 milioni – per favorire l’adozione di software Microsoft.

Gli inquirenti hanno fatto sapere che Microsoft non è coinvolta nel caso. Ovviamente.