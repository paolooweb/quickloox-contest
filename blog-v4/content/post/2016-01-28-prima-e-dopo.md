---
title: "Prima e dopo"
date: 2016-01-28
comments: true
tags: [Chmielewski, Re/Code, Apple, risultati, iPhone]
---
Dawn Chmielewski su Re/Code, [prima](http://recode.net/2016/01/25/storm-clouds-gather-as-investors-await-apples-earnings-report/).<!--more-->

>Si prepara una tempesta mentre gli investitori aspettano i risultati finanziari di Apple. […] Gli analisti scrivono da mesi che l’azienda avrà avuto difficoltà a pareggiare, men che meno superare, le vendite record di iPhone 6 e 6 Plus del Natale 2014.

Dawn Chmielewski su Re/Code, [dopo](http://recode.net/2016/01/26/apple-posts-slowest-iphone-growth-since-smartphones-introduction/).

>Apple annuncia la crescita più modesta di iPhone dalla sua presentazione a oggi […] 74,8 milioni, un modesto miglioramento dell’uno percento sul trimestre dell’anno precedente, quando vendette 74,5 milioni di smartphone.

Non è un problema il numero di iPhone (che il prossimo trimestre sarà certamente inferiore rispetto al trimestre equivalente dell’anno scorso). È il tono. Catastrofe imminente, tempeste, momenti della verità, pessimismo diffuso degli analisti, nessuna paura di preannunciare tragedie che tanto possono essere smentite senza pudore entro ventiquattr’ore.

Se poi le vendite fossero scese dell’uno percento invece che salire, forse non ci sarebbe stato il fatturato record di tutti i tempi. Ma sarebbe stato il secondo, di misura e non sembra indice di cattiva salute, se è il fatturato dell’azienda di informatica di maggior valore al mondo, con un miliardo di utenti attivi. Un miliardo.

<blockquote class="twitter-tweet" lang="en"><p lang="en" dir="ltr">Apple lost more revenue to foreign exchange fluctuations in last quarter than *ALL* of Facebook&#39;s quarterly revenue <a href="https://t.co/TzMS2Bwwyh">https://t.co/TzMS2Bwwyh</a></p>&mdash; Tom Gara (@tomgara) <a href="https://twitter.com/tomgara/status/692111800289095682">January 26, 2016</a></blockquote> <script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>

Il fatto è che Apple sui media viene sempre trattata come *prima*, quando era il pulcino in procinto di soffocare e sarebbe fallita miseramente, o [acquistata da Sun Microsystems per un tozzo di pane](http://uk.businessinsider.com/sun-almost-bought-apple-in-1996-2016-1). Era il 1996, ragazzi. Questo è il 2016, vent’anni dopo. *Dopo*.