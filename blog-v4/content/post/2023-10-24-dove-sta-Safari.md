---
title: "Dove sta Safari"
date: 2023-10-24T00:15:45+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [macOS, Sonoma, Ventura, Criptex, Eclectic Light Company]
---
Dove sono finiti gli smanettoni che non si fermano davanti a nulla, inseguono la conoscenza, non si accontentano delle versioni ufficiali…?

Possibile che nessuno si sia accorto come in Ventura e Sonoma [la app Safari risieda in un Cryptex](https://eclecticlight.co/2023/10/24/where-safari-hides-and-bundled-apps-crash/), una immagine disco sicura caricata all’avvio, e parte unicamente se lanciata dal disco di boot?

Battute a parte, *The Eclectic Light Company* sta guadagnandosi rapidamente il trono di re degli smanettoni. Il dettaglio tecnico che si trova sul sito rispetto a macOS è eccellente, chiaro e largamente introvabile altrove. Da seguire.