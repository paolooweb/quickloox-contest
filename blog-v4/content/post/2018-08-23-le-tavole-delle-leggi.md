---
title: "Le tavole delle leggi"
date: 2018-08-23
comments: true
tags: [statistica, probabilità, regolo, Slashdot, Wolfram, Alpha]
---
Le procedure di emergenza dovrebbero essere insegnate a tutti, per poi però procedere veloci ed efficaci come un tempo era impossibile, grazie ai nuovi strumenti.

Illuminante questa [eulogia delle tavole delle probabilità](https://blogs.sas.com/content/iml/2018/08/20/demise-of-standard-statistical-tables.html) oramai scomparse dai libri di testo, visto che i calcolatori sono oramai ubiqui.

La [discussione su Slashdot](https://slashdot.org/story/344910) è interessante perché, fuori dalle nostalgie, si percepisce come insegnare il calcolo manuale e magari l’uso di strumenti come il regolo calcolatore possa avere un senso. Appunto, backup, esercizio mentale. Dopo, però, fuori il calcolatore da tasca. E avanti a imparare qualcosa di più.

Ricordo le obiezioni all’avvento delle prime calcolatrici da tasca. Che cosa sarebbe accaduto se improvvisamente fosse sparita l’energia elettrica, per via della guerra nucleare o di una catastrofe non meglio identificata?

Quarant‘anni dopo direi che le potremmo anche archiviare.
