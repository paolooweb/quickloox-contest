---
title: "il tempo è sempreverde"
date: 2018-04-23
comments: True
tags: [greenpeace, Liam, Daisy, Mac, iPhone, Macworld]
---
Sono passati dodici anni da quando Greenpeace lanciò la campagna *Green My Apple* che denunciava la presenza di agenti inquinanti in MacBook Pro.

Ne scrissi su *Macworld Italia* [in questi termini](https://macintelligence.org/posts/2006-11-20-green-christmas/). La vicenda, da un punto di vista di Rete, è invecchiata male perché molte pagine web al centro della questione non sono più raggiungibili. Inoltre, scrivendo per la carta, usai Url sintetici che, passati dieci anni, sono scaduti.

Mentre scrivo lavoro a un restauro dell’articolo che devo ancora terminare. La faccenda è già tuttavia più che comprensibile anche a chi nel 2006 non fosse ancora nato.

(La conservazione di un ipertesto suggerisce riflessioni fuori luogo ora, che però trovo interessanti e sulle quali vorrei parlare una prossima volta).

Ripesco una pagina da un altro decennio perché succede che nel frattempo Apple abbia annunciato [l’alimentazione di tutte le proprie sedi al cento percento con energia rinnovabile](https://www.apple.com/newsroom/2018/04/apple-now-globally-powered-by-100-percent-renewable-energy/). Per le dimensioni attuali di Apple, è un traguardo inusitato.

Impensabile che dodici anni fa a Cupertino non fossero al lavoro su questo traguardo. Rileggere le gesta di Greenpeace alla luce di questo sviluppo fa pensare a bambini che giocano a fare i grandi; oppure a furbastri opportunisti che infatti, oggi, [cantano le lodi di Tim Cook](https://appleinsider.com/articles/17/10/17/greenpeace-lauds-apple-renewable-energy-efforts-decries-right-to-repair-stance).

Opportunisti in quanto Apple si trova anche alla seconda generazione di [robot che riciclano iPhone](https://www.apple.com/newsroom/2018/04/apple-adds-earth-day-donations-to-trade-in-and-recycling-program/) e [Greenpeace è critica](https://www.macrumors.com/2018/04/19/greenpeace-apple-repairable-upgradeable-products/.) Entusiasmo per l’energia rinnovabile, insoddisfazione (perché, poi?) per il robot che ricicla. In fondo è un progresso; dodici anni fa mettevano in croce l’intera azienda a partire da un MacBook Pro, oggi hanno capito che un’organizzazione di centomila persone può fare anche più di una cosa per volta.

Resta il fatto che affibbiavano patenti farlocche di ecologicità in base alle promesse fatte dalle pubbliche relazioni delle aziende. Apple non ha mai promesso niente, ma da qualche parte, sul consumo energetico responsabile, sembra sia arrivata. Per Greenpeace è stata solo pubblicità facile; non sapevano niente e niente avevano capito.

Il tempo, parlando di ecologia, è sempreverde. Uguale a se stesso, non cambia mai. Dove invece c’è del marcio, passa il tempo e il tanfo aumenta. Ricordarsene per la prossima guasconata dei greenimpiccioni.
