---
title: "Quelli degli antiforfora"
date: 2010-02-08
comments: true
tags: [iPad, Ping!]
---
Altroconsumo ha gi&#224; pronta la <a href="http://www.altroconsumo.it/mobile/apple-ipad-s265883.htm" target="_blank">recensione di iPad</a>. Ne riporto alcuni brani, che traduco in italiano.<!--more-->

>Le nostre osservazioni si basano su quanto mostrato da Apple durante la presentazione del 27 gennaio.

Traduzione: Volevamo recensire iPad senza neanche averlo visto e ci &#232; toccato sprecare un&#8217;ora e mezza.

>iPad probabilmente &#8211; considerata la pessima abitudine di Apple di tradurre i prezzi da dollari a euro con un cambio 1 a 1 &#8211; coster&#242; (sic) dai 500 euro in su.

Traduzione: Siamo stati su <a href="http://store.apple.com/it" target="_blank">Apple Store italiano</a> e <a href="http://store.apple.com/us" target="_blank">americano</a>. Dalla prima pagina:

iPod shuffle - $59, &#8364;55  
iPod nano - $149, &#8364;139  
iPod classic - $249, &#8364;229  
iPod touch - $199, &#8364;189  
Apple TV - $229, &#8364;269  
MacBook - $999, &#8364;899  
MacBook Pro - $1199, &#8364;1149  
MacBook Air - $1499, &#8364;1399  
Mac mini - $599, &#8364;549  
iMac - $1199, &#8364;1099  
Mac Pro - $2499, &#8364;2299

Non ci sono due prezzi uguali. Ma se scrivessimo la verit&#224; non starebbe in piedi quello che scriviamo, quindi meglio le bugie e le supposizioni infondate.

>Questo apparecchio potrebbe essere la miglior cornice per foto digitali di tutti i tempi.

Traduzione: Ci sono 140 mila applicazioni gi&#224; compatibile a disposizione pi&#249; tutte quelle che nasceranno e sono state scaricate pi&#249; di tre miliardi di volte, ma noi non vediamo pi&#249; in l&#224; del nostro naso.

>[iPad] pu&#242; fare molte cose, ma non sembra in grado di fare qualcosa molto meglio di altri apparecchi &#8211; assai pi&#249; economici &#8211; presenti sul mercato.

Traduzione: Ignoriamo platealmente che &#232; stata scritta la stessa fesseria ai tempi di iPhone e che come criterio di giudizio non vale una cicca, considerati i risultati di iPhone.

A suon di fare le rassegne sui migliori antiforfora si sono montati un po&#8217; la testa. Chiss&#224; se recensiscono gli shampoo guardando i flaconi.