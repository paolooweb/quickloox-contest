---
title: "Vecchia maniera"
date: 2015-02-10
comments: true
tags: [2001, Kubrick, Trumbull]
---
Spero mi si scuserà se per un momento esco dalle solite tematiche e mi limito a segnalare la puntata di *Matte Shot* [dedicata a *2001: Odissea nello spazio*](http://nzpetesmatteshot.blogspot.co.uk/2015/01/kubricks-2001-one-mans-incredible.html).<!--more-->

*2001* è probabilmente l’unico film di fantascienza girato nel secolo scorso che può essere visto in modo credibile anche nell’epoca in cui la computergrafica ha reso possibile tutto e il suo contrario.

Il tempo necessario a scrivere un *post* come si deve se ne è andato a frugare tra le foto di scena e fuori scena e a leggere i commenti sullo stile di regia di Stanley Kubrick, le trovate del mago degli effetti speciali Douglas Trumbull, le curiosità, le notizie (per me) inedite e l’atmosfera generale del tutto. Quando ancora il cinema e gli effetti speciali erano una forma d’arte.