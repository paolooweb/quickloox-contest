---
title: "L’idea del cliente"
date: 2015-07-22
comments: true
tags: [Wind, Vodafone, FastWeb, Tre, Tim]
---
Riassunto delle puntate precedenti: supponi di voler usare sul Mac una chiavetta Internet ricaricabile, per un periodo di tempo limitato e navigando come faresti a casa. Wind ha a disposizione una buona offerta ma [bisogna ricordarsi](https://macintelligence.org/posts/2015-06-11-il-massimo-del-minimo) che il loro tagliare la banda a fine consumo non è a trentadue chilobit, ma a zero virgola: è impossibile fare qualsiasi cosa diversa dal telefonare al 155. Se non c’è campo, è fatta: sei isolato.<!--more-->

Tre non ha offerte, o così mi hanno detto al punto vendita e non ho troppo approfondito. Tim non esiste: sono tra i primi responsabili storici del degrado telematico italiano e da me non avranno mai più un centesimo fino a che avrò vita.

Vodafone ha una offerta più costosa di quella di Wind, [pilotata da software scritto da incapaci](https://macintelligence.org/posts/2015-07-09-il-minimo-dispensabile/). Mentre Wind prevede il rinnovo anticipato dell’offerta (paga e ti ridiamo i giga), Vodafone ha abbandonato il sistema. Vende invece una funzione SOS Giga, rinnovabile, che accredita un gigabyte ogni due giorni, al costo di cinque euro. Tradotto: se finisci i gigabyte una settimana prima, ci vogliono *solamente* quindici/venti euro per arrivare al rinnovo, in cambio di traffico risibile.

L’unico caso in cui convenga una chiavetta Vodafone prevede il possesso di una Sim voce Vodafone, che dà facoltà di raddoppiare i gigabyte in offerta (SOS Giga a parte) e chiaramente cambiare di molto le carte in tavola.

Ciò detto, fa specie l’idea del cliente che hanno in Vodafone. Hai esaurito la chiavetta? Navighi in qualche altro modo alla pagina dei contatori per verificare. Pagina visibile solo se navighi via Vodafone. Ora sono con FastWeb a bassa velocità (trentadue chilobit veri, non finti come quelli di Wind) e le cose semplici, la posta, Twitter, un sito, si fanno. In questa circostanza le pagine Fai da Te del sito Vodafone sono le più lente del mondo e non si possono vedere i contatori. Perché mai? È una punizione?

Probabilmente l’idea è che il cliente non Vodafone veda l’inefficienza di un altro operatore sulle pagine Vodafone e sia spinto a cambiare. La mia esperienza è opposta: arrivo da un altro *provider*, vedo che vengo trattato intenzionalmente con i piedi, mai più.

Mi si obietterà che i grandi operatori sono concentrati sulle offerte compulsive per gli adolescenti e dedicano poche risorse ai pochi altri che chiedono altri servizi. Sta benissimo, ma avrei una idea: visto che è business marginale, perché non farlo pagare quello che si deve e offrire un servizio eccellente? Si potrebbero guadagnare clienti di una fascia più ricca. Si perderebbero tanti che vogliono spendere poco, ma tanto stanno già su Tre e su Wind. E comunque la premessa è che sia un settore di nicchia. Se invece il *business* delle chiavette è immenso, beh, non si capisce perché tanta mediocrità. Diventare la Apple delle chiavette [potrebbe portare profitti](https://macintelligence.org/posts/2015-07-21-i-soldi-non-chiamano-i-soldi/).

Finisco con una tirata d’orecchie a FastWeb, mio operatore attuale, che non permette una chiamata cellulare al 42010 di Vodafone. Anche qui, perché? Che senso ha, oltre a quello di infastidire il cliente, che se ne ricorderà?