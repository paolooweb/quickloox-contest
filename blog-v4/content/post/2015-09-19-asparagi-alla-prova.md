---
title: "Asparagi alla prova"
date: 2015-09-19
comments: true
tags: [Pencil, Surface, iPad, stilo, Mac, Astropad, Dong, Cintiq, Wacom]
---
Si è già dimostrato come [Apple Pencil non sia una copia dello stilo su Surface Pro 3](https://macintelligence.org/posts/2015-09-15-gli-asparagi-e-apple-pencil/). Si può fare di più.<!--more-->

[Scrive](http://www.lindadong.com/blog//apple-pencil-vs-wacom-cintiq) Linda Dong, ex *designer* nel Prototyping Team Apple:

>Attualmente la [tavoletta grafica] Cintiq Wacom viene considerata il meglio per il disegno e il design professionale con stilo su superficie piatta. […] Molto semplicemente, in confronto [ad Apple Pencil], la Cintiq rosica.

Specialmente sulla latenza (il tempo di risposta da quando si appoggia lo stilo a quando “scorre l’inchiostro”) Dong è categorica nel dichiararlo vantaggio competitivo di Apple Pencil.

C’è anche un confronto di prezzo: l’insieme iPad Pro più Apple Pencil costa poco più di una Cintiq modello base e risulta assai conveniente verso tutti gli altri.

Per avere la prova provata dell’originalità di Apple Pencil, è sufficiente chiedere a un professionista della grafica o del *design* che cosa pensa dell’utilizzo a livello professionale dello stilo di Surface Pro 3.

P.S.: ho scoperto ora grazie al *post* di Dong che esiste [Astropad](http://astropad.com/ipad/), programma di trasformazione di iPad in una tavoletta grafica per Mac. Compatibile con iPad Pro.