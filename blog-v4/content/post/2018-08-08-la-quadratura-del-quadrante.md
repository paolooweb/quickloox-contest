---
title: "La quadratura del quadrante"
date: 2018-08-08
comments: true
tags: [watch, Slack, WhatsApp]
---
Da qualche tempo il mio watch è problematico. Ha smesso di aggiornare il circolo interno delle attività (l’alzarsi in piedi durante il giorno) e molto spesso, anche se non sempre, non invia più notifiche.

Ho provato a reinstallare il sistema, senza successo, e sto cercando di capire se su Internet qualcuno ha una soluzione, ma non è il punto. Invece, proprio smanettando e smadonnando attorno alle app e alle impostazioni, ho iniziato a riflettere sulle peculiarità delle app per watch.

Per alcune situazioni, tutto sommato è meglio *non* avere una app. Il caso eclatante è quello di WhatsApp, che – quando le cose mi funzionano – mostra notifiche dei messaggi che arrivano e che posso ignorare se è il caso, oppure tenerne conto e rispondere subito, con la dettatura o la scrittura manuale. È di fatto tutto quello che può servire usando WhatsApp via polso; il tutto senza una app. Non serve.

A volte c’è la app e ti passa la voglia di usarla. Slack su watch è comodissimo con le notifiche e provare ad addentrarsi nella app è più inutile che dannoso per il morale.

La app di Slack per watch [ha chiuso i battenti](https://9to5mac.com/2018/01/31/slack-communication-app-slacking-off-removes-apple-watch-app-in-latest-version/) qualche mese fa e adesso si usa il servizio come WhatsApp, attraverso le notifiche.

Anche se Slack non ha dato motivazioni ufficiali, si può pensare che corrispondano a quelle di tante altre app che [sono sparite in questi mesi](https://9to5mac.com/2018/08/02/instapaper-apple-watch-dropped/): scarso utilizzo.

Ci ho provato più volte a usare l’app di Slack su watch: era terribile. Meglio adesso che se ne fa a meno. Per forza c’era scarso utilizzo; era inutilizzabile.

E difatti dovrebbe esserci una terza categoria: le app indispensabili. Non sono un grande utilizzatore di app su watch, ma di fatto non ne ho ancora trovate.

Probabilmente parte del problema è questa: manca ancora non tanto la *killer app*, quella che ti fa comprare l’apparecchio pur di averla, ma solidi casi di uso che vadano oltre quanto viene fornito preimpostato con un watch tirato fuori dalla scatola.

Così come parte della spiegazione è che [Apple ora chiede app che siano native e autonome](https://developer.apple.com/news/?id=11162017a&1510866181), per quanti desiderassero usare watch indipendentemente da iPhone. L’impegno richiesto per questo obiettivo è ben diverso da quello di scrivere sostanzialmente una camera dell’eco per quello che succede su iPhone.

Si può supporre che la prima ondata di app per watch, quando questo era una semplice estensione del terminale da tasca, abbia esaurito la sua ragione di esistenza. La seconda ondata, sempre come supposizione, inizierà ad arrivare quando gli sviluppatori avranno le idee più chiare su che cosa mettere sul quadrante dell’orologio per farne qualcosa di utile.