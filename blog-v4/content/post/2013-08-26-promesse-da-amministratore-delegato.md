---
title: "Promesse da amministratore delegato"
date: 2013-08-26
comments: true
tags: [Microsoft, Apple, Mac]
---
Steve Ballmer, amministratore delegato di Microsoft succeduto a Bill Gates, [se ne va entro un anno](https://macintelligence.org/posts/2013-08-25-potrebbe-andare-peggio/). In genere questo è la procedura con cui le grandi multinazionali licenziano un componente importante lavando i panni sporchi in famiglia. Ma qualcuno potrebbe pensare che si tratti di una separazione consensuale o che il guerriero abbia deciso di ritirarsi dalla battaglia per dedicare più tempo alla famiglia.<!--more-->

Questo è un paragrafo da un suo [memorandum del 2008](http://allthingsd.com/20080723/microsoft-ceo-steve-ballmers-full-memo-to-the-troops-about-new-reorg/):

>Apple: nella concorrenza tra PC e Mac, battiamo Apple in vendite 30 a 1. Ma non c’è dubbio che Apple stia prosperando. Perché? Perché sono bravi a fornire una esperienza ristretta ma completa, mentre il nostro impegno a offrire scelta spesso comporta alla fine qualche compromesso. Oggi cambiamo il nostro modo di lavorare con i produttori hardware per assicurare di poter fornire una esperienza completa, assolutamente senza compromessi. Faremo lo stesso con i telefoni, offrendo scelta intanto che lavoriamo per creare grandi esperienze per l’utilizzatore finale.

Alzi la mano chi abbia visto cambiare qualcosa nei Pc Windows dal 2008 a oggi sul piano dei compromessi. Soprattutto, dati del primo trimestre del 2013 come [li racconta Forbes](http://www.forbes.com/sites/timworstall/2013/04/18/apples-mac-took-45-of-the-profits-in-the-pc-market/),

>Con il cinque percento delle vendite per unità, Apple ottiene il 45 percento dei profitti nel settore dei Pc.

Cinque anni e il trenta a uno è diventato venti a uno. E di quei diciannove che non sono Apple, a portare a casa soldini sono ben pochi.