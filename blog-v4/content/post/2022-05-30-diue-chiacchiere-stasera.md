---
title: "Due chiacchiere stasera"
date: 2022-05-30T19:10:21+01:00
draft: false
toc: false
comments: true
categories: [Internet]
tags: [A2, Strozzi, Filippo Strozzi, Marin, Roberto Marin]
---
Ci si trova in podcast [stasera su YouTube](https://youtu.be/9E02cYz_j3A) insieme a Roberto e Filippo? Alle 21. :-)

**Aggiornamento**: Non so se sia piaciuto, ma ci siamo divertiti un bel po’, o almeno io. Ce ne vorrebbero tre a settimana, *per rinfrancar lo spirito / tra un enigma e l’altro*, avrebbe detto [La Settimana Enigmistica](https://www.lasettimanaenigmistica.com).