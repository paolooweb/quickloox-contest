---
title: "Commenti a tavola"
date: 2016-10-26
comments: true
tags: [Nfl, Surface, Microsoft, Slashdot, iPad]
---
Si era appena tornati sulla faccenda delle tavolette Surface [fatte ingoiare al mondo del fooball americano a suon di dollari](https://macintelligence.org/posts/2016-10-19-tavoletta-rasa/) da Microsoft che arriva un contorno interessante.

Colin Kaepernick e Chip Kelly dei 49ers di San Francisco [hanno parlato pubblicamente dei loro problemi con Surface](http://www.mercurynews.com/2016/10/21/49ers-chip-kelly-colin-kaepernick-review-microsoft-surface-tablets-which-bill-belichick-is-done-using/) e questa sarebbe la notizia. Cioè la cosa meno interessante.

Molto meglio andare sulla [pagina di Slashdot che tratta la notizia](https://news.slashdot.org/story/16/10/23/2241246/more-nfl-players-attack-microsofts-400m-surface-deal-with-the-nfl) e leggere i commenti. Cito solo qualcosa:

>Considerato che [i nostri Surface] si guastano quasi sessanta volte più dei nostri iPad, ci credo. Certo, sono veri computer e sono più complessi, ma sessanta volte più guasti sta semplicemente uccidendo il nostro reparto IT.

>Da noi si guastano solo otto volte di più […] Attualmente forniamo un iPad di ricambio ogni quattro macchine […] e forniamo un Surface di ricambio per ogni Surface. Si fatica a manutenere così tanti apparecchi supplementari.

>È product placement, non una vera soluzione. La Nfl li conta come fatturato pubblicitario. Quindi nessuno si cura di che cosa ne pensino gli utenti finali e lo staff di supporto.

Un commento più lungo spiega che la Nfl è configurata come ente *no profit* e chi sviluppa software per essa lo fa usualmente a prezzi bassi e sovente sottocosto, perché spesso arrivare a servire la Nfl significa una porta aperta verso le altre leghe professionistiche, che invece hanno fini di lucro e quindi uno schema di investimenti e costi ben differente. Si conclude così:

>Microsoft sostanzialmente regala Surface alla Nfl per avere pubblicità e sperare di poter poi vendere sui mercati dove gira veramente denaro.

E c’è gente che li compra veramente.