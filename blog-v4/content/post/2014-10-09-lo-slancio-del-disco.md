---
title: "Lo slancio del disco"
date: 2014-10-10
comments: true
tags: [Gruber, iOS8, DaringFireball]
---
Spiegazione interessante [quella di John Gruber](http://daringfireball.net/2014/10/ios_8_storage_space) di fronte a un dato storicamente insolito di [scarsa adozione di iOS 8](https://developer.apple.com/support/appstore/).<!--more-->

Forse non è questione di *bug* o piegature varie, ma di [gente che vorrebbe aggiornare e non ha abbastanza spazio per farlo](https://storify.com/gruber/not-enough-space-to-upgrade-to-ios-8). iOS 8 vuole cinque gigabyte liberi e – esperienza diretta – gli apparecchi non sedici gigabyte di disco saranno sempre e comunque occupati oltre questo limite. Chi fa come me e non cerca attivamente di installare ma aspetta un avviso dal sistema, potrebbe non riceverlo mai.

A parte rimuovere manualmente *app* e foto fino a liberare lo spazio necessario, una soluzione elegante è installare via iTunes. Apple lo spiega in una [nota tecnica](http://support.apple.com/kb/TS4431?viewlocale=it_IT&locale=en_US), che però è scarsamente pubblicizzata. 