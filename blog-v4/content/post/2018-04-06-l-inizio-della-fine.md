---
title: "L’inizio della fine"
date: 2018-04-06
comments: true
tags: [Windows, Lovejoy, Nadella, Myerson, Azure]
---
Fa gran piacere leggere un pezzo intitolato [La fine di Windows](https://stratechery.com/2018/the-end-of-windows/), particolarmente se è documentato e preciso come quello su *Stratechery*.

Non sarà mai abbastanza presto ma sta accadendo. Soprattutto accade da dentro Microsoft, dove il primo a rendersi conto che il futuro aziendale dipende sempre più da altri fattori è proprio l’amministratore delegato. Il quale, invece di imporre scelte impopolari, ha operato con astuzia per fare sì che il gruppo di lavoro stesso si rendesse conto da solo di non essere più il punto di riferimento di una volta.

In questo modo Nadella ha avuto buon gioco nel riorganizzare Microsoft in modo che, per la prima volta, non esiste una divisione Windows.

Naturalmente nessuno ammazza la gallina che tante uova d’oro ha deposto; ma nel menu a lunga scadenza è previsto (buon) brodo. Una gran bella notizia.
