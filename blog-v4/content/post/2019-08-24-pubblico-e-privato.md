---
title: "Pubblico e privato"
date: 2019-08-24
comments: true
tags: [Safari, WebKit, privacy, Mozilla]
---
È tempo di rendere più pubblica che sia possibile la [politica di WebKit in tema di prevenzione del tracciamento della navigazione](https://webkit.org/tracking-prevention-policy/) da parte dei siti web.

Prima d mtutto c’è una bella spiegazione di che cosa significhi tracciamento della navigazione e che tipi di tracciamento vengono impiegati. Poi c’è la dichiarazione di intenti di WebKit:

>Trattiamo l’aggiramento delle misure antitracciamento con la stessa serietà applicata verso lo sfruttamento delle vulnerabilità nella sicurezza.

Trucchi per tracciare la nostra navigazione contro la nostra volontà sono trattati alle stregua di buchi nella sicurezza, problemi gravi da risolvere prima possibile. Mi piace.

WebKit – il fondamento di Safari – non concede eccezioni a questa regola. Dopo di che cerca di mitigare al massimo i problemi che essa potrebbe suscitare e, in caso di conflitti, vince il beneficio per l’utente. Dove è impossibile risolvere un conflitto in modo indolore, WebKit persegue l’uso di nuove tecnologie capaci di agevolare chi naviga e contemporaneamente di impedire il tracciamento.

Si è già detto, ma conviene ripetere: il valore della privacy diverrà sempre maggiore con il passare del tempo e farà sempre più differenza volersi permettere un browser che la tuteli invece di uno che tuteli gli interessi degli inserzionisti.

Safari sta diventando uno dei mezzi sicuri per navigare. Assieme a Firefox; la politica di WebKit per la tutela del nostro privato si ispira a [quella di Mozilla](https://wiki.mozilla.org/Security/Anti_tracking_policy), come viene pubblicamente riconosciuto nel documento.

È una di quelle situazioni dove è persino legittimo non avere capito proprio benissimo perché conviene, ma si fa la cosa giusta e si va a dormire la sera appena più tranquilli, magari inspiegabilmente, ma in modo certo. Conviene, facciamolo.
