---
title: "Il primo errata"
date: 2015-05-10
comments: true
tags: [Swift]
---
Il mio [libriccino su Swift](http://www.apogeonline.com/libri/9788850333387/scheda) è in vendita e sono emozionato come uno scolaretto. So per certo che ne sono state vendute almeno due copie, quindi il più è fatto.<!--more-->

La prima è questa:

<blockquote class="twitter-tweet" lang="en"><p lang="it" dir="ltr"><a href="https://twitter.com/loox">@loox</a> appena consegnato.. <a href="http://t.co/LUv0BUtqHz">pic.twitter.com/LUv0BUtqHz</a></p>&mdash; macmau74 (@macmau74) <a href="https://twitter.com/macmau74/status/596213455889289216">May 7, 2015</a></blockquote> <script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>

La seconda è di **Simone** che mi ha mandato il primo *errata* e ringrazio.

Al capitolo 2, elencando gli operatori principali disponibili per [Swift](http://www.apple.com/it/swift/), ho scritto dell’operatore <code>&gt;&gt;</code> e affermato che prende i bit della versione binaria di un numero e li sposta di una o più posizioni verso destra, a *raddoppiare* il valore del numero stesso.

In verità lo spostamento verso destra *dimezza* un numero binario. Prendiamo il numero otto, scritto in binario 1000. Se sposto a destra di una posizione ciascuna cifra – distruggo la cifra più a destra, introduco uno zero a sinistra – arrivo a 0100, che vale quattro.

La nozione viene spiegata correttamente all’inizio della pagina 39 in cui c’è l’errore, che riguarda solo l’esempio. Utilizzando i file *playground*  di esempio scaricabili gratis dalla pagina della scheda del libro si verifica chiaramente tutto quanto.

Per inciso, raddoppiare un numero binario implica spostarne le cifre di una posizione verso sinistra, eliminando il bit più a sinistra e inserendo uno zero in fondo a destra. Si fa in Swift con l’operatore <code>&lt;&lt;</code>, stavolta spiegato come si deve sempre a pagina 39, capitolo 2.

È solo l’inizio: questo era molto semplice e confido di vedere errori sempre più spettacolari e criptici.