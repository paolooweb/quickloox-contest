---
title: "L’azienda intelligente"
date: 2022-07-31T11:58:42+13:00
draft: false
toc: false
comments: true
categories: [Internet]
tags: [Apple, Jason Aten, Aten, Inc., Maestri, Luca Maestri]
---
[Apple ha ricordato a tutti con una frase perché è l’azienda di maggior valore al mondo](https://www.inc.com/jason-aten/with-1-sentence-apple-reminded-everyone-why-its-most-valuable-company-on-earth.html).

I prodotti non c’entrano.

Jason Aten su *Inc.* ci va giù icastico e ha ragione. La frase è tratta dall’intervento di Luca Maestri, eccellenza italiana ignorata dato che non è moda e non è cibo, durante la [comunicazione degli ultimi risultati finanziari](https://www.fool.com/earnings/call-transcripts/2022/07/28/apple-aapl-q3-2022-earnings-call-transcript/):

>Il nostro trimestre di giugno ha ancora una volta dimostrato la nostra capacità di far procedere la nostra attività in modo efficiente, nonostante le sfide poste dall’ambiente in cui ci muoviamo.

Come se avesse detto, riassume Aten, *abbiamo le stesse difficoltà di tutti, ma ci muoviamo meglio*.

Sempre nell’articolo si trova la logica conclusione: gli iPhone potrebbero essere i migliori computer da tasca dell’universo, e così tutto il resto, ma senza la capacità di reperire i componenti giusti, con la qualità adeguata, ai prezzi migliori, nei tempi necessari, Apple se ne farebbe poco.

I prodotti contano esattamente quanto la capacità di costruirli e di programmare per il futuro.

Una delle definizioni meno elusive e più sintetiche di intelligenza è *la capacità di adattarsi all’ambiente*.