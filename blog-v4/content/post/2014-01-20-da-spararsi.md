---
title: "Da spararsi"
date: 2014-01-20
comments: true
tags: [iPad, Quake, BaldursGateII]
---
Devo dire che sono molto più tipo da [Baldur’s Gate II](http://www.baldursgateii.com), che su iPad guadagna persino nuova vita nonostante il sapore retró.<!--more-->

Avanzata la giustificazione preventiva, sono rimasto intrigato dal *tutorial* per riuscire a [giocare a Quake III, versione Arena o Shareware, su iPad](http://www.grabitmagazine.com/blog/post/how-to-play-quake-iii-on-ipad---a-step-by-step-guide/).

Confesso che non l’ho verificato, perché gli sparatutto in prima persona non sono il mio pane. E che lo verificherei, perché il sotterfugio tecnico (niente di illegale) è affascinante.

Poi bisogna apprezzare il sapore del *frag*, la sparatoria generale. Non so, magari.