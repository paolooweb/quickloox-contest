---
title: "Una vita da aggiornato"
date: 2023-09-23T18:34:46+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [macOS, The Eclectic Light Company]
---
Che bella questa [storia breve degli aggiornamenti di macOS](https://eclecticlight.co/2023/09/23/macos-upgrades-since-el-capitan/) da El Capitan in avanti, ospitata su *The Eclectic Light Company*.

La lettura è concisa ma nutriente, dato che le sintesi sono ricavate dagli articoli estesi scritti dall’autore al tempo di ciascun aggiornamento. Niente viaggi nella memoria, che è fallace peggio della RAM, piuttosto riassunti di cronaca.

È anche una bella medicina contro i ricordi che ci sono rimasti in testa, forse un po’ troppo selettivamente; questo lo avevo installato in un attimo, quell’altro mi aveva dato un sacco di problemi… e – può darsi – invece no.

Se avessi conosciuto il trucco, peraltro non sempre funzionante, per visualizzare l’attività dell’installatore quando restava apparentemente inattivo o bloccato per mezze ore, avrei anche avuto meno esitazioni nel lasciarlo lavorare.