---
title: "Dal floppy a dd"
date: 2019-01-24
comments: true
tags: [Macintosh, Jobs, Backblaze, backup, dd, Tuxera, Ntfs]
---
Naturalmente oggi Macintosh compie trentacinque anni. Un momento storico che MacRumors copre con [un post essenziale completo di due video semi-inediti e interessanti](https://www.macrumors.com/2019/01/24/macintosh-turns-35/). il post da guardare è il loro.

Macintosh fece rumore anche perché utilizzava un formato di memorizzazione che lasciava nella polvere i floppy tradizionali, quelli che si tagliavano con le forbici per creare una rientranza su tutti e due i lati della custodia, in modo da poter usare nell’Apple ][ anche il lato posteriore.

Dai tre pollici e mezzo di Sony salto in un colpo solo al [rapporto 2018 di Backblaze](https://www.backblaze.com/blog/hard-drive-stats-for-2018/): statistiche sul buon e meno buon funzionamento di oltre centomila dischi, per un totale superiore ai trentasei milioni di giorni di utilizzo.

Si noti che dai dati sono esclusi i modelli per i quali Backblaze non aveva a disposizione almeno quarantacinque unità, per loro il dato soglia sotto il quale non c’è attendibilità. Ancora, quando ci sono meno di cinquecento modelli uguali, o meno di cinquantamila giorni di funzionamento, i risultati possono essere troppo approssimativi per utilizzarli in situazioni tipo una decisione di acquisto o una sostituzione.

So di essere noioso, tuttavia non vedo abbastanza passaparola su questo: hai meno di quarantacinque dischi dello stesso modello? Hai meno di cinquantamila giorni di funzionamento? Il tuo augusto parere su questo o quel modello, prima ancora che la marca, è del tutto superfluo.

Sempre parlando di dischi, amici sono venuti in lacrime con un disco rigido guasto: dentro, tutte le foto di famiglia, compresi i viaggi e le figlie. Niente backup. Disco Windows.

Ho installato i [driver Ntfs open source di Tuxera](https://www.tuxera.com/community/open-source-ntfs-3g/) via [Fuse](https://osxfuse.github.io) e il disco, effettivamente, non si monta. A intervalli regolari ticchetta, la spia di funzionamento lampeggia. A vederlo c’è da temere il peggio. Ma c’è il Terminale.

Con il comando `dd` sono riuscito ad avviare una copia lentissima, settore per settore se non più in basso ancora, che in alcune settimane di lavoro (!) ha effettivamente recuperato settantacinque gigabyte di foto.

Il lavoro non è stato portato a termine perché la secondogenita, giocando con la tastiera, ha trovato una combinazione magica – la osservavo, niente Command e niente Espelli – che ha riavviato il Mac, interrompendo la sessione.

Ora riproverò e devo studiarmi bene la sintassi di `dd` per scoprire se ci sia qualche altro modo un pochino più veloce per continuare il recupero.

La morale di tutto questo è che sono passati i decenni, le rivoluzioni, i formati e le unità di misura, ma sui dischi abbiamo ancora un bel po’ di cose da scoprire.