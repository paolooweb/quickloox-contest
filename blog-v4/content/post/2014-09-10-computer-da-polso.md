---
title: "Computer da polso"
date: 2014-09-10
comments: true
tags: [iPhone, Watch]
---
Ho fatto a meno del cellulare fino a che ho potuto; era l’oggetto più inutile del mondo. Fino a che ho capitolato, per acquistare un Nokia 3210. Il minimo assoluto, mediocre sotto ogni punto di vista, fin troppo per i miei usi.<!--more-->

Poi è uscito iPhone. Ho insistito per chiamarlo e chiamare la categoria computer da tasca, perché non sono più cellulari, a meno di voler negare l’evidenza di 1,3 milioni di *app* disponibili, di cui il telefono è una.

Di un cellulare in tasca non sapevo che fare. Un computer in tasca mi ha cambiato un bel pezzo di vita.

Veniamo agli orologi. Categoria che ho dimenticato da vent’anni, quando mi sono reso conto che avevo l’ora ovunque, a partire dallo schermo del computer.

Non so che farmene, di un orologio. Un computer da polso, tuttavia, potrebbe cambiarmi un bel pezzo di vita.

Se Apple Watch è un computer da polso, vince. Se è un orologio, perde.

Non vedo l’ora di provarlo.
