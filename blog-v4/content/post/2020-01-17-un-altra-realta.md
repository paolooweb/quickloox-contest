---
title: "Un’altra realtà"
date: 2020-01-17
comments: true
tags: [Reality, Converter]
---
È appagante seguire Apple quando gli annunci restano fedeli a un principio base: niente [chiacchiere vuote o concept esaltanti sulla carta che non diventeranno mai un prodotto](https://macintelligence.org/posts/2020-01-10-chiacchiere-e-concept/).

A volte succede anche a loro, vedi [AirPower](https://techcrunch.com/2019/03/29/apple-cancels-airpower-product-citing-inability-to-meet-its-high-standards-for-hardware/). Fortunatamente è l’eccezione.

C’è da pensarci visto il gran parlare di realtà virtuale che si fa e del diffondersi di caschi e caschetti che vorrebbero creare una realtà alternativa immersiva ma sono invece sufficientemente scomodi per dare fastidio nella realtà consueta e risultare assai poco utili salvo casi limite.

Apple per fortuna non sembra seguire il treno dei desideri *che all’incontrario va*, direbbe [Paolo Conte](https://www.youtube.com/watch?v=TGGOkp0VHac) e va dritta per una strada sensata invece che seguire la massa. Come quando bisognava fare a tutti i costi un *netbook* e tirarono fuori iPad.

La realtà virtuale, oggi, non è matura per una fruizione di massa. E infatti Apple procede sulla strada della realtà aumentata, che al contrario potrebbe trovare un sacco di applicazioni utili e immediate.

Un passo per volta, senza fanfare o demo da luna park alla fiera di settore. Per esempio, fanno uscire [Reality Converter](https://developer.apple.com/news/?id=01132020a&1578956733) per gli sviluppatori.

Chi sviluppatore non è può divertirsi e sperimentare con oggetti già pronti da appoggiare in realtà aumentata su un tavolino o una scrivania, grazie semplicemente a un iPhone decentemente recente.

Un altro modo di interpretare la tecnologia che cambia la vita. Un’altra realtà di sviluppo ed evoluzione al nostro servizio più che a quello degli acchiappaclic.

<iframe width="560" height="315" src="https://www.youtube.com/embed/TGGOkp0VHac" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>