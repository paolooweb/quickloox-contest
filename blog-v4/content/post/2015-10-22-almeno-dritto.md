---
title: "Almeno dritto"
date: 2015-10-22
comments: true
tags: [Windows, Roma, Termini]
---
Ricevo, pubblico e ne ringrazio **Vittorio**:

Di passaggio alla Stazione Termini di Roma.<!--more-->

A infastidire non dovrebbe essere che Windows si sia bloccato sulla schermata blu (se desse fastidio, basterebbe passare ad un sistema operativo la cui schermata equivalente è praticamente sconosciuta alla gran parte dei suoi utenti) quanto che il sistema non riesca nemmeno a metterla dritta.

Però, in fondo, non è neanche un vero problema: tanto quel testo lo si conosce a memoria…

 ![Windows in stazione Termini](/images/termini.jpg  "Windows in stazione Termini") 