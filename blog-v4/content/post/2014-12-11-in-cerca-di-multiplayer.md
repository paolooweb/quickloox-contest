---
title: "In cerca di multiplayer"
date: 2014-12-11
comments: true
tags: [D&D, Roll20, iPlay4e, Xpilot, plenaria, Natale, Spaceteam]
---
Comincio a sentire lo spirito del Natale e l’avvicinarsi della *plenaria*, quando il mio gruppo di gioco si riunisce una volta l’anno per passare insieme più o meno una giornata all’insegna, appunto, del gioco.<!--more-->

La parte del leone la fa sempre [Dungeons and Dragons](http://dnd.wizards.com) e, grazie a [Roll20](http://roll20.net) e [iPlay4e](http://iplay4e.appspot.com/), siamo copertissimi.

Cerchiamo anche qualcosa di più veloce per, diciamo, riscaldarci durante il pomeriggio. Dovrebbe essere qualcosa di durata media (massimo un’oretta), giocabile praticamente su qualsiasi piattaforma (presenzieranno OS X, iOS e Android), *multiplayer* capace di reggere anche otto o nove giocatori e, non necessario ma assolutamente preferito, girare anche su rete locale e non necessariamente via Internet.

Sto per iniziare a raccogliere idee in giro per Internet. Ultimamente ci siamo divertiti abbastanza con [Xpilot](http://xpilot.sourceforge.net) e [Spaceteam](https://itunes.apple.com/it/app/spaceteam/id570510529?l=en&mt=8) (le partite durano pochissimo, ma una tavolata può veramente schiantarsi tra le risate e la confusione).

Idee?