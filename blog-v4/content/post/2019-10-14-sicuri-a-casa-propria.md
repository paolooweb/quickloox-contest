---
title: "Sicuri a casa propria"
date: 2019-10-14
comments: true
tags: [Cina, Taiwan, Apple, iOS, Tencent]
---
Dicevo di non comprendere la scelta di Apple quando [nasconde la bandiera di Taiwan](https://qz.com/1725175/apple-removed-a-hong-kong-protest-map-from-its-app-store/) a chi maneggia un iPhone a Hong Kong o Macao, senza averla realmente cancellata.

Bene (meglio) se ti schieri apertamente dalla parte della libertà, anche per trattarci [non da prodotti ma da clienti con dignità e rispetto](https://www.applemust.com/20-things-apples-tim-cook-told-us-this-week/) come detto di passaggio da Monaco. Male (peggio) se colludi con i cinesi. A patto che sia chiaro, appunto perché non siamo prodotti. Capito che è difficile non calare i pantaloni, si inghiotte il rospo, ma si lascian vedere le mutande.

Un altro capitolo della faccenda è la prossima telenovela dei [dati di navigazione sicura inviati a Tencent (cinese, probabilmente compromessa con il governo) da Safari](https://blog.cryptographyengineering.com/2019/10/13/dear-apple-safe-browsing-might-not-be-that-safe/).

Ci possono essere ragioni serie per farlo. Per esempio, la *blacklist* dei siti fraudolenti di solito viene affidata a Google, solo che i servizi Google vengono bloccati in Cina e di conseguenza qualcuno là deve tenere la blacklist.

Ancora una volta, dà fastidio ma può essere capito – non giustificato, capito – che la dittatura cinese voglia disporre dei dati di navigazione sicura dei cinesi e chieda ad Apple di agire in tal senso.

Il problema è che l’avviso leggibile nell’interfaccia di iOS, sull’invio a Google e a Tencent di alcuni dati di navigazione sicura, appare anche sugli apparecchi in uso in Occidente.

Se Apple mandasse dati di navigazione sicura di cinesi al governo cinese (a Tencent; ma si sa come girano queste cose), sarebbe brutto. Se mandasse dati *nostri*, che cinesi non siamo, al governo cinese, sarebbe *molto* brutto. Per capirci, siccome viviamo in un Paese libero, sarebbe molto brutto anche se li mandasse al governo italiano.

Il problema è che Apple non ha ancora chiarito la faccenda.

Interessa relativamente poco che sia una cosa brutta o molto brutta. Interessa moltissimo invece che sia chiara. Se i miei dati possono finire in Cina, posso anche disattivare la navigazione sicura così rischiando di incappare su un sito fraudolento senza accorgermene e senza essere avvisato. Però in genere so quello che faccio, non vado a cercare software gratis quando è a pagamento, men che meno porno, posso accettare il rischio.

Voglio semplicemente [sapere in termini chiari che cosa succede](https://www.youtube.com/watch?v=39iKLwlUqBo). Come ha [detto definitivamente Steve Jobs](https://www.idownloadblog.com/2018/03/26/steve-jobs-privacy-quote-d8-conference/):

>Privacy significa che la gente sa per che cosa firma, in linguaggio chiaro, ripetutamente. Sono un ottimista; credo nell’intelligenza delle persone e che alcune persone vogliano condividere più dati di altre. Chederglielo. Chiederglielo ogni volta.

>Fai in modo che ti dicano di smettere se sono stanchi di sentirselo chiedere. Lasciali sapere precisamente che cosa farai con i loro dati.

Da Apple mi aspetto esattamente questa policy. Vogliono passare i miei dati a [Demogorgon](https://en.wikipedia.org/wiki/Demogorgon)? Può anche andare bene. Basta saperlo.

<iframe width="560" height="315" src="https://www.youtube.com/embed/39iKLwlUqBo" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>