---
title: "Il lato illuminato"
date: 2019-07-21
comments: true
tags: [Luna, AAA, All, About, Apple, Armstrong, Aldrin]
---
Vale sempre la pena di arrivare fino alla darsena di Savona quando c’è di mezzo [All About Apple](https://www.allaboutapple.com). L’idea di aprire il museo in notturna per rivivere lo sbarco sulla Luna di cinquant’anni fa è stata geniale.

Sono ripartito, classica situazione di volente o nolente, prima del fatidico *small step* di Armstrong e però ho fatto in tempo a vedere cose incredibili. Visitatori casuali del sabato sera avvinti da una lezione teorica sui razzi vettori per lo spazio; gente comune in fila per gettare un’occhiata a Giove attraverso un telescopio, con la piazza al buio per accordo con l’amministrazione; autorità affascinate dalla passeggiata lunare in realtà virtuale.

Su tutto questo sì innesta la proverbiale ospitalità dello staff, che oltre alle presentazioni e alle ricostruzioni minuto per minuto ha formito dosi ricostituenti di focaccia ligure ben oltre la mezzanotte e avrei dato molto per restare fino all’alba e godere di una colazione speciale.

Sarà per un’altra volta, perché il museo c’è, vitale e pulsante per quanto animato da volontari illuminati e poco altro. Questa nottata ha fugato qualunque dubbio sulla validità di questo progetto, ove ve ne fossero stati; eravamo circondati dalla storia di Apple e affascinati, nell’occasione, da quella dell’esplorazione spaziale. Cultura attorno a cultura, storia nella storia, patrimoni dell’umanità a contatto. Il museo vive di passione e sacrifici, non solo di tempo, e uno sponsor anch’esso illuminato farebbe gran comodo, su cifre più che abbordabili per un business aziendale decente e buonissime possibilità di contatto; la sede è a due passi letterali dall’imbarco delle grandi navi da crociera e dalla zona più turistica del porto. Parlatene all’imprenditore, al dirigente più vicino.

So bene che l’anniversario dello sbarco era *trendy*; due ore di autoradio per arrivare in piazza De André e non si è sentito altro. Eppure a Savona è stato trattato in modo unico, umano e coinvolgente, a portata di tutti eppure di alto livello scientifico.

*Ci mancano avventure come l’Apollo 11*, diceva uno dello staff mentre puntava il telescopio su Saturno. Senza quel fascino è difficile coinvolgere il pubblico sull’esplorazione dello spazio.

Ci mancano anche momenti di vera condivisione ed esperienza. [AAAllunati](https://www.allaboutapple.com/2019/07/aaallunati-il-20-luglio-la-notte-bianca-di-all-about-apple/) è stato un momento molto bello perché ha unito la scienza e il contatto umano. In questo clima storico e culturale, è operazione da pionieri.