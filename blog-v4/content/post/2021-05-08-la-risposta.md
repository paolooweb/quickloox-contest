---
title: "La Risposta"
date: 2021-05-08T00:47:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [iOS 14.5, John Gruber, Daring Fireball, Flurry Analytics] 
---
Si iniziano a contare le Risposte alla [Domanda](https://macintelligence.org/posts/A-Domanda-rispondo.html).

Per Flurry Analytics, le persone che hanno escluso a priori il tracciamento della navigazione fuori luogo da parte delle app su iOS sono il novantasei percento.

Si deduce per complemento, perché il dato che mi riguarda – quelli che hanno lasciato la possibilità di chiedere ed eventualmente ragionano app per app – [è al quattro percento](https://www.flurry.com/blog/ios-14-5-opt-in-rate-att-restricted-app-tracking-transparency-worldwide-us-daily-latest-update/).

John Gruber fa ironia sul novantasei percento, che [gli sembra poco](https://daringfireball.net/linked/2021/05/07/flurry-app-tracking).

Certo, è un dato iniziale. Pronosticavo un rapporto tipo quattro a uno più che un ventiquattro a uno e per il momento ha ragione lui, ironia o meno.

I numeri potranno anche cambiare. Di certo la Risposta è netta. Non vogliamo essere tracciati da una app anche quando ne usciamo. Non ci va di essere tracciati da un sito quando ci troviamo su altri siti.

A tutti i soggetti in gioco tocca prendere atto e adeguare… tutto. Non sempre il cliente ha ragione; qui però si esprimono, a un livello di gerarchia superiore, gli individui.

*Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile.*               