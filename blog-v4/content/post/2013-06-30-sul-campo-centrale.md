---
title: "Sul campo centrale"
date: 2013-06-30
comments: true
tags: [iPhone, iPad]
---
Mi scrive – e ringrazio – **Stefano**:

>Avevo ritenuto una buona cosa la app in uscita seppur col cappio dell'abbonamento. Quando ho saputo che non c'è la versione iPad (per motivi politici dico io) ho messo una pietra sopra a Microsoft: è governata da incompetenti.

Il riferimento è a Office 365, che diventa *app* per iOS ma non tanto, appunto con il vincolo del costoso abbonamento al servizio e senza versione iPad.<!--more-->

Microsoft è ingiustificabile e però comprensibile: tempo fa [si diceva](https://macintelligence.org/posts/2012-02-22-come-fai-sbagli) come l’esistenza di iPad l’abbia messa in un angolo dove, come uno scacchista condannato, può effettuare solo mosse perdenti. La cosa importante è tuttavia che non si può più parlare di Office e fare finta che iOS non esista. Quando Windows dominava, Microsoft commercializzava Office per Mac senza obbligo, per fare cassa (non ho studiato ma potrei scommettere che una copia di Office per Mac rende molto più a Microsoft che una copia di Office per Windows). Invece iOS, per Microsoft, è un incubo, che non si può supportare e neanche trascurare.

Nel frattempo, se BlackBerry manterrà le proprie promesse, entro fine estate [arriverà su iOS il relativo sistema di messaggistica](http://www.imore.com/blackberry-ceo-bbm-still-coming-ios-end-summer), uno dei baluardi su cui l’azienda ha costruito i suoi successi.

Di Google non sto neanche a fornire i *link*: praticamente tutti i suoi servizi più importanti esistono come *app* o *webapp* per iOS.

Si può discutere sui numeri del venduto o sulle evoluzioni dell’interfaccia, o su chi copi chi; di certo, se si parla di centralità rispetto al mercato, non ci sono dubbi su dove sia situata.