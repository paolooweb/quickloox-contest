---
title: "Fioritura indipendente"
date: 2022-06-19T02:21:54+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [Mac, macOS, iPadOS, iOS, The Eclectic Light Company]
---
Pezzo semplice semplice di *The Eclectic Light Company* sul tema della supposta convergenza dei sistemi operativi di Mac e iPad. [Incastrati tra loro o fiorenti autonomamente?](https://eclecticlight.co/2022/06/19/last-week-on-my-mac-stuck-together-or-flourishing-apart/)

La risposta è la seconda è il motivo è semplice semplice: iPadOS è avviato a diventare una mezza via tra macOS e iOS. Ci si arriverà con funzioni che possono essere diverse da un sistema a un altro (esempio semplice semplice: la sicurezza), oppure che nascono qui e arrivano là (una piccola sorpresa che vale la pena di leggere in originale), altre sviluppate insieme come Stage Manager e così via.

Invece che prendersela per una fusione di sistemi che non ci sarà, meglio studiare e sfruttare tutti i modi in cui iPad e Mac crescono pur restando fedeli a se stessi.