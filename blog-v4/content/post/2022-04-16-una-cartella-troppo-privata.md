---
title: "Una cartella troppo privata"
date: 2022-04-16T15:28:36+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [Textastic, iCloud Drive, BBEdit]
---
Di recente ho ricomprato [Textastic](https://www.textasticapp.com): sono sempre alla ricerca dell’editor di testo ideale per iPad e mi era parso che le ultime aggiunte al programma potessero rappresentare un salto di qualità interessante.

Ho scritto un post e poi ho appunto usato una delle funzioni che la vecchia versione non aveva, ovvero la scrittura di file su iCloud Drive, con l’obiettivo di passare su Mac il file stesso dalla cartella di iCloud Drive a quella del blog.

E invece no. Textastic genera una cartella omonima su iCloud Drive, che però è visibile solo dove sia presente la app. Da iPhone, dove ho installato ugualmente il programma, vedevo tranquillamente il file scritto su iPad. Da Mac, invece, iCloud Drive non mostra alcuna cartella Textastic, né nel Finder né nel Terminale.

Nessun problema, perché Textastic abbonda di funzioni di trasferimento file. La più semplice a portata di mano è quella per lavorare su Dropbox e così ho fatto.

Quindi il mio file è passato senza problemi da iPad a Mac. Però questa cartella così privata di Textastic mi ha deluso un pochino. Certamente non ho la minima intenzione di acquistare la versione Mac solo per vedere comparire una cartella di interscambio, quando su Mac ho accesso a [BBEdit](https://www.barebones.com/products/bbedit/).

Aggiungo: ho scaricato la copia di prova di Textastic per Mac e gli ho fatto salvare un file qualunque su iCloud Drive. Ha creato una cartella Textastic su iCloud Drive… che non è quella già esistente su iPad. Sono due cartelle diverse, che hanno lo stesso nome e la stessa posizione, che nemmeno potrebbero esistere.

Proverò a segnalare la cosa, che però mi sembra intenzionale.