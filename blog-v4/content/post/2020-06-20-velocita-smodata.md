---
title: "Velocità smodata"
date: 2020-06-20
comments: true
tags: [Snell, Six, Colors, iPad, Developer, Mode, Xcode, Arm, Mac]
---
Tanti si aspettano da Wwdc un XCode capace di compilare su processori Arm e/o una sua versione per iPad.

Jason Snell su Six Colors si lascia andare a una fantasia sfrenata, bellissima: un [Developer Mode per iPad](https://sixcolors.com/post/2020/06/a-wild-idea-for-wwdc-developer-mode-for-ipados/).

È che scriverne tre giorni prima non è anticipare, è andare veloci come rocce metamorfiche. Se veramente Apple presentasse una funzione simile, avrebbe cominciato a lavorarci un anno fa e magari anche prima.

Non sarebbe quindi una idea geniale, solo un rendersi conto di cose che altri avevano già concepito da tempo.

Certo, se presentassero un Developer Mode per iPad a Wwdc 2021…. Ma allora tanto valeva scriverlo a gennaio.