---
title: "Utile e professionale"
date: 2015-11-15
comments: true
tags: [OSX, Capitan, Utility, Disco, Ghostbusters, diskutil]
---
In diversi hanno mostrato di [scandalizzarsi](http://www.macitynet.it/os-x-10-11-el-capitan-la-regressione-utility-disco/) per il ridimensionamento dell’interfaccia grafica in Utility Disco in [El Capitan](http://www.apple.com/it/osx/whats-new/). Contemporaneamente procede inalterato, come credo da dieci anni a questa parte, il rivolo di commenti che vuole Apple nel corso di un [abbandono del professionale](http://www.macitynet.it/apple-abbandona-gli-utenti-pro-lalternativa-e-hp-zbook-studio/), qualsiasi cosa significhi.<!--more-->

Contrariamente all’[insegnamento di Ghostbusters](https://www.youtube.com/watch?v=38ZR9QMtCVU), incrocio i flussi e dico che, se è professionale, allora va fatto nel Terminale. Per la semplice ragione che il Terminale è infinitamente più versatile e potente.

Ne ho avuto prova pochi giorni fa affrontando un disco rigido che rifiutava l’installazione di El Capitan e che Utility Disco non poteva partizionare, in quanto l’apposito pulsante era disattivato.

Nel Terminale, `diskutil` mi ha dato modo di vedere che, per motivi ignoti, il disco aveva problemi di *ownership*, qualsiasi cosa significhi. Da `diskutil` li ho risolti e ho impartito un comando di partizionamento che ha funzionato. La situazione si è sbloccata e ho installato El Capitan.

Qui casca l’asino, pardon, il professionale: Con Utility Disco su Yosemite, i problemi di *ownership* erano ugualmente insolubili. E così su Mavericks. E su Mountain Lion. E su Lion. Indietro a piacere.

Se sei un artista della gestione dei dischi, impari [`diskutil`](https://developer.apple.com/library/mac/documentation/Darwin/Reference/ManPages/man8/diskutil.8.html). Altrimenti, se usi Utility Disco, sappi che lo spartiacque tra il professionale e il dilettantistico non è – guarda la combinazione – esattamente la funzione che eri abituato a usare tu. È il Terminale.