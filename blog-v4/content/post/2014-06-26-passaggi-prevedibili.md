---
title: "Passaggi prevedibili"
date: 2014-06-26
comments: true
tags: [FiveThirtyEight, Obama, Silver, Espn, SPI, R, Italia]
---
Si può dire. *FiveThirtyEight*, il sito di statistica fondato da Nate Silver dopo il suo trionfo personale nel prevedere con grande esattezza la rielezione di Barack Obama a Presidente degli Stati Uniti, ha pubblicato prima dell’inizio dei Mondiali un [modello predittivo](http://fivethirtyeight.com/interactives/world-cup/) dello svolgimento del torneo.<!--more-->

Il sistema si basa sul [Soccer Power Index](http://www.espnfc.com/story/1873765) sviluppato da Espn, che assegna valutazioni a squadre di club e nazionali sulla base delle prestazioni delle squadre stesse e dei singoli giocatori che le compongono. L’indice viene [aggiornato quotidianamente](http://www.espnfc.com/fifa-world-cup/story/1865575/soccer-power-index-daily-update).

Al giorno d’oggi, conoscere un pizzico di [R](http://www.r-project.org) (come ricordava [briand06](http://melabit.wordpress.com)) fa bene anche al semplice appassionato di calcio.

Da prima dell’inizio della Coppa del mondo 2014, il modello assegnava all’Italia probabilità di vincere il torneo inferiori all’uno percento. E probabilità marginalmente superiori al cinquanta per cento di passare la fase a gironi.