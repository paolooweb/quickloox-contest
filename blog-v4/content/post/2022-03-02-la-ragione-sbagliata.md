---
title: "La ragione sbagliata"
date: 2022-03-02T00:27:11+01:00
draft: false
toc: false
comments: true
categories: [Hardware]
tags: [Apple, Russia, Benedict Evans, App Store, Apple Pay, Sputnik, John Gruber, RT News, Ucraina, Mappe, Cina]
---
Lo hanno detto già tutti, che Apple ha interrotto le vendite di hardware in Russia, ritirato alcune app di news di propaganda dagli App Store del resto del mondo e ancora qualcos’altro. Apple non ha pubblicato una news ma ha scritto a diversi commentatori, [tra cui John Gruber](https://daringfireball.net/linked/2022/03/01/russian-apple-store-closed):

> Abbiamo sospeso tutte le vendite di prodotti in Russia. Settimana scorsa, abbiamo fermato le esportazioni presso i nostri canali di vendita in Russia. Apple Pay e altri servizi sono stati limitati. RT News e Sputnik News non sono più disponibili negli App Store esterni alla Russia. E abbiamo disabilitato le segnalazioni di traffico e incidenti in Mappe per l’Ucraina come misura di sicurezza e precauzione per i cittadini ucraini.

Aggiungo che non ci avrei scommesso, dopo tante decisioni di convivenza difficile ma comunque cercata in Russia e specialmente in Cina.

Benedict Evans è ancora più scettico e, oltre a rilevare – ma lo sapevamo – che Apple non si fa problemi a vendere in Cina, avanza l’ipotesi che semplicemente, tra venti di guerra, volatilità del rublo e difficoltà logistiche da sanzioni, [Apple smetta di vendere in Russia perché non è possibile farlo](https://twitter.com/benedictevans/status/1498780447823872002).

<blockquote class="twitter-tweet"><p lang="en" dir="ltr">Wondering if Apple has stopped sales in Russia because it doesn’t want to sell there or because, given FX volatility and banking sanctions, it just can’t sell there.</p>&mdash; Benedict Evans (@benedictevans) <a href="https://twitter.com/benedictevans/status/1498780447823872002?ref_src=twsrc%5Etfw">March 1, 2022</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

Tutto può essere; anche nella visione più negativa delle cose, rimane possibile fare la cosa giusta anche se per la ragione sbagliata.