---
title: "E con il suo spirito"
date: 2023-09-30T23:38:45+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [Rogue Amoeba, Audio Hijack]
---
Di recente ho acquistato Audio Hijack Pro di Rogue Amoeba, per dare al mio setup audio una parvenza di professionalità nel momento in cui capita di produrre un podcast con i [fantastici A2 Filippo e Roberto](https://macintelligence.org/posts/2023-07-10-il-gruppo-63/).

Avevo acquistato Audio Hijack tanti anni fa, per scopi di cui non ho memoria, così come del Mac su cui l’ho usato. Mi ricordo invece il programma, ingegnoso, geniale, quasi incomprensibile nell’uso. Arrivavo in fondo grazie alla genialità sua, faticando grazie all’incomprensibilità mia della sua interfaccia.

Oggi Audio Hijack Pro ha una interfaccia bellissima, esteticamente e nella funzionalità. Un piacere averlo, un piacere usarlo. In un certo senso non è più quel programma di una volta, anche se il nome è lo stesso. Di fatto però lo è e oggi, rimanendo geniale e ingegnoso, dà gusto a usarlo.

Rogue Amoeba lo ha pubblicato per la prima volta esattamente ventuno anni fa e, a festeggiamento, sul suo sito campeggia una notevole [*timeline* di tutti i suoi prodotti](https://weblog.rogueamoeba.com/2023/09/30/rogue-amoeba-icon-timeline/).

Da guardarla per capire quanto sia maturata l’azienda in questi quattro lustri e quanto sia oggi perfettamente allineata al presente, con un passo nel futuro: certe scelte di interfaccia e usabilità non si vedono tutti i giorni né in tutti i software, e continueranno a non vedersi per un bel po’.

Il tempo lo dice: Rogue Amoeba è una delle migliori firme indipendenti impegnate nella produzione di (ottimo) software per Mac.

Se al termine della consultazione venisse voglia a qualcuno di portarsi a casa Audio Hijack Pro, beh, lo capirò. Abbiamo qualcosa in comune: la debolezza per le cose fatte bene e con lo spirito del Mac così come è stato concepito.