---
title: "L'unico modo"
date: 2019-08-16
comments: true
tags: [Apple, Tiffany, Alaska, Resolve, Yukon]
---
Apple fa sempre troppo poco. Invece di salvare il mondo con parole d’ordine facili o con l’illusione che sia sufficiente rinunciare alle cannucce in plastica, annuncia insieme a Tiffany che [si rifornirà di oro in Alaska solo da estrattori che nel contempo lavorano per ripristinare o migliorare i percorsi di migrazione dei salmoni](https://www.apple.com/newsroom/2019/08/searching-for-gold-with-habitat-restoration-in-mind/).

Qualche anima bella è convinta che la via da percorrere sarebbe non estrarre oro e possibilmente nient’altro. Sedersi e aspettare la morte.

Qualcun altro dirà appunto che è troppo poco. Perché darsi un obiettivo concreto, realistico e verificabile, al posto di sparare slogan apocalittici ai quali si può rispondere con un contentino e sentirsi a posto?

A fronte delle visioni pseudoreligiose sull’ambiente da cui siamo ultimamente assediati, Apple agisce in modo preciso e definito, in modo che più di laico definirei incarnato. Presente materialmente, effettivo, definito, con confini precisi, volendo anche criticabile per chi volesse criticarlo.

È l’unico modo in cui mi sento in grado di formarmi un giudizio senza sentirmi una mosca cocchiera.
