---
title: "Figli di un Office minore"
date: 2016-01-25
comments: true
tags: [Office, Docs, Pages, LibreItalia, LibreOffice, 9to5Mac, Windows, Mac, iPad, Apple]
---
Diciamo per essere buoni che nel 2016 l’uso di Office è una assurdità inutile tre volte ogni quattro. E, in effetti, da molti anni l’unica ragione che mi viene spiegata è la compatibilità con qualcun altro, anche lui utilizzatore Office.<!--more-->

A me piacerebbe incontrare l’utente zero, quello che non deve rispettare la compatibilità con chissà chi perché è lui a imporla agli altri.

Ho sempre sostenuto che la storia della compatibilità è una balla colossale e il vero motivo per cui si continua a usare Office è la resistenza al cambiamento. Adesso c’è la prova provata.

9to5Mac pubblica infatti una serie di [tabelle sulla compatibilità](http://9to5mac.com/2016/01/21/windows-mac-ipad-microsoft-office-comparison/) tra le versioni di Office per Windows e quelle per Mac, o iPad. Premesso che sono state compilate da un dipendente di Parallels e quindi probabilmente sono fatte per apparire più pessimiste della realtà, se io pagassi Office, mi sentirei truffato. La quantità delle funzioni mancanti sui sistemi Apple è imbarazzante.

In altre parole: la strombazzata compatibilità è finta. Meglio: riguarda un campo di funzioni e caratteristiche talmente ristretto che si otterrebbe lo stesso identico risultato con un [Pages](https://www.icloud.com/#pages), un [Google Docs](https://docs.google.com/document/u/0/), un [LibreOffice](http://www.libreoffice.org) (rinnovata l’iscrizione a [Libreitalia](http://www.libreitalia.it)? Meglio prima di fine gennaio perché quello è il momento in cui si raccoglie il dato ufficiale).

Il bello è che chi paga Office per usarlo su Mac o iPad rinuncia da subito a una montagna di funzioni, ma finanzia il loro sviluppo. Come dire, paghi per un programma di cui non userai mai una montagna di possibilità perché in verità non servono e d’altronde, anche volendole, non ci sono. Come cantava Battiato quando faceva l’avanguardia, *ti sei mai chiesto quale funzione hai?*