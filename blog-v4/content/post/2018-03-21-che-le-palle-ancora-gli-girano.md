---
title: "Che le palle ancora gli girano"
date: 2018-03-21
comments: true
tags: [iPhone, Sdk, Francia]
---
Per mettere le cose in prospettiva, dieci anni fa quello di cui stiamo per parlare [non esisteva](https://medium.com/@akosma/10-years-of-iphone-a-developers-perspective-dbaf6e0c4ab5).<!--more-->

Come dire che gli sviluppatori francesi sono liberi di fare cose diverse dallo scrivere app per iPhone: attività che non ha eliminato un singolo posto di lavoro.

Eppure pare che la Francia [voglia multare Apple e Google](https://www.bloomberg.com/news/articles/2018-03-14/france-to-take-action-v-google-apple-for-commercial-practices) per come *abusano degli sviluppatori locali*.

Apple sostiene di avere finora corrisposto agli sviluppatori francesi [un miliardo di euro complessivi](http://www.lefigaro.fr/secteur/high-tech/2018/03/14/32001-20180314ARTFIG00094-bruno-le-maire-veut-attaquer-google-et-apple-en-justice-pour-pratiques-commerciales-abusives.php). Sicuramente sono diventati miliardari in pochi e, rispetto al totale, chi vive bene dei proventi di App Store è una frazione. Con questo, nessuno è obbligato a pubblicare su App Store se non ne ricava abbastanza per vivere.

Per di più il ministro competente, si fa per dire, è convinto che gli sviluppatori vendano le app ad Apple. Cosa che [non sussiste](https://twitter.com/benjaminferran/status/973818743485853697).

<blockquote class="twitter-tweet" data-lang="en"><p lang="fr" dir="ltr">Sur les contrats des App Store et Play Store, sur les commission prélevées et les données récupérées, pour protéger “nos start-up 🇫🇷 et nos développeurs”. Bon courage. <a href="https://t.co/LjoBd6s8Qq">https://t.co/LjoBd6s8Qq</a></p>&mdash; Benjamin Ferran (@benjaminferran) <a href="https://twitter.com/benjaminferran/status/973818743485853697?ref_src=twsrc%5Etfw">March 14, 2018</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

L’impressione generale è quella che nei riguardi dei francesi suscita Paolo Conte con la sua [canzone](https://www.youtube.com/watch?v=La5JBSEdIe0). Scritta nel secolo scorso, evidentemente immortale.

<iframe width="560" height="315" src="https://www.youtube.com/embed/La5JBSEdIe0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>