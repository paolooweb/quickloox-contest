---
title: "Quelli che"
date: 2014-01-10
comments: true
tags: [iPhone, Mac]
---
In queste ore si festeggia giustamente l’anniversario del primo annuncio di iPhone ma sta accadendo qualcosa forse di meno rivoluzionario, sicuramente molto più incredibile.<!--more-->

<blockquote class="twitter-tweet" lang="en"><p>&quot;In 2013 […] the worst decline in PC market history. Gartner&quot; BTW… nn ricordo che Apple sia mai stata al 13,7% del mercato USA dei computer</p>&mdash; setteB.IT (@setteBIT) <a href="https://twitter.com/setteBIT/statuses/421403607725273088">January 9, 2014</a></blockquote>
<script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>

Quelli che Apple pensa solo a iOS e trascura Mac, *oh yes*.