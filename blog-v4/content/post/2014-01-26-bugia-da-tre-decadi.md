---
title: "Bugia da tre decadi"
date: 2014-01-26
comments: true
tags: [Mac]
---
Gran festa, per i trent’anni di Mac. Mi sono goduto la pagina *home* di Apple, dove si festeggiava invece che vendere (immagino che la [panoramica sui trent'anni](http://www.apple.com/30-years/) resterà); iFixit che non esisteva nel 1984 e ha partecipato alle celebrazioni con il *teardown*, lo [smontaggio integrale](http://www.ifixit.com/Teardown/Macintosh+128K+Teardown/21422) del primo Mac 128k; i [dieci poster](http://www.cultofmac.com/263930/check-photos-apples-hq-preparation-party/) con i nomi di tutti i dipendenti Apple dal primo aprile 1976 a oggi, nello stesso spirito della carrozzeria di Mac che [le incastonava all’interno](http://www.folklore.org/StoryView.py?project=Macintosh&story=Signing_Party.txt&topic=Apple+Spirit).<!--more-->

Se c’era qualcosa da imparare, è arrivata dalla [retrospettiva sulla pubblicità di Mac](http://www.tuaw.com/2014/01/24/what-the-first-macintosh-ads-looked-like/) di *The Unofficial Apple Weblog*. Da questa immagine.

 ![Mac contro PC](/images/mac-pc.jpg  "Mac contro PC") 

L’elenco file prima e dopo Mac. La differenza tra foglio di calcolo senza e con Mac. La comparsa di un nuovo settore, la *business graphics*, che rinunciando a Mac era di fatto impossibile in quegli anni.

Nessuno sano di mente poteva negare che Mac facesse molto meglio le cose di prima, che la differenza fosse abissale. La grande maggioranza continuò con i cassoni compatibili: costavano meno.

*Alla massa interessa zero che cosa si possa fare e se si faccia meglio o peggio.*

Quando si sente qualcuno affermare che Android fa le stesse cose di iOS, che Windows *è uguale* a Mac, sta ingannando sé prima di tutto e poi gli altri. Non sa se sia davvero così e non gliene frega la metà di niente; ha una sola ragione in testa. *Costa meno*, ovviamente senza occhiali. Quando di uguale c’era niente, il discorso finiva nella stessa maniera.

Liberi tutti grazie al cielo di vivere come si crede e acquistare quello che si desidera. Le bugie però no. Specie se hanno il naso lungo trent’anni.
