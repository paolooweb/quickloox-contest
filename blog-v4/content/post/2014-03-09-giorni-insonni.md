---
title: "Giorni insonni"
date: 2014-03-09
comments: true
tags: [Mac, Hints, Sleep, stop]
---
I Mac di produzione attuale hanno il tasto di accensione piuttosto vicino all’angolo superiore destro della tastiera e può accadere di premerlo accidentalmente. In questo caso Mac va in stato di stop; niente di pericoloso, ma se capita rallenta il lavoro e irrita.<!--more-->

Nel caso, *Mac OS X Hints* ha pubblicato il [comando per inibire l’effetto-sonno](http://hints.macworld.com/article.php?story=20140305140635280). Rimane sempre la possibilità di tenere premuto il pulsante più a lungo per spegnere il computer in condizioni di emergenza. Resta la funzione, scompare il fastidio. Così si fa.