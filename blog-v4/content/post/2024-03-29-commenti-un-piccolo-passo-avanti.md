---
title: "Commenti: un piccolo passo avanti"
date: 2024-03-29T02:09:11+01:00
draft: false
toc: false
comments: true
categories: [Internet, Software, Blog]
tags: [Comma, nginx]
---
Non è esattamente una soddisfazione, ma è un piccolo passo avanti. Ora, se si tenta di commentare un post, appare un messaggio di errore 404 da parte di nginx.

[nginx](https://www.nginx.com) è il software di server web usato per pubblicare il sito una volta generato il blog.

Apparentemente abbiamo impostato in modo corretto la base del funzionamento di [comma](https://github.com/Dieterbe/comma), il motore dei commenti, ma qualcosa non va nel dialogo tra i pezzi del sistema.

Continuiamo a lavorare. Se ci sono idee brillanti [le ascoltiamo più che volentieri](https://macintelligence.org/post/2024-03-28-lavori-in-corsa/).