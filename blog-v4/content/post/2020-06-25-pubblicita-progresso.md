---
title: "Pubblicità, progresso"
date: 2020-06-25
comments: true
tags: [Apple, Forbes]
---
Altro annuncio Wwdc di cui si parla poco: su iOS 14 le app che vogliono tracciare l’utilizzatore su siti non di loro pertinenza dovranno chiedere esplicitamente il permesso.

Non ho compiuto riflessioni particolari in merito, tuttavia immagino che per istinto potrei negare il permesso.

Un [articolo su Forbes](https://www.forbes.com/sites/johnkoetsier/2020/06/24/apple-just-made-idfa-opt-in-sending-an-80-billion-industry-into-upheaval/) mostra bene come questa mossa potrebbe significare grossi rivolgimenti nel mercato pubblicitario. Un mercato dove gli interessi che ruotano attorno a iOS superano di gran lunga quelli Android.

Sembra che Apple stia mettendo a punto un meccanismo che permetterà agli inserzionisti di misurare l’efficacia della loro pubblicità senza disporre di dati identificabili a livello di singolo terminale. C’è da vedere se funzionerà e come risponderanno in generale giganti come Google e Facebook, che di pubblicità web campano.

Se funziona, Apple potrebbe avere portato una rivoluzione autentica in un campo dove finora l’abuso nei confronti dell’utilizzatore ha regnato sovrano.