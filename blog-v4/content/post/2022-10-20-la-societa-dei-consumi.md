---
title: "La società dei consumi"
date: 2022-10-20T01:12:08+01:00
draft: false
toc: false
comments: true
categories: [Hardware]
tags: [Lenovo, X1, Yoga, Chin, Monica Chin, The Verge]
---
Apple ha i suoi problemi di eccesso di diversificazione dei prodotti: troppi iPad, troppi iPhone. È lontanissima comunque dalla follia degli anni novanta, quando con i Performa e tutto il resto poteva vantare, per modo di dire, decine di configurazione diverse, aventi l’unico scopo di impedire a chiunque di discernere quella giusta per sé.

Lenovo è messa ben peggio di Apple in tema, come testimonia la [recensione di Monica Chin su The Verge](https://www.theverge.com/23411572/lenovo-thinkpad-x1-yoga-gen-7-review-price-specs) che ricorre all’iperbole delle *milleuno configurazioni*. Per il produttore non sembra essere un problema, semmai una nota di merito nello strano universo parallelo di gente che veramente pensa di comprarsi un portatile Intel nel 2022.

Certo, è gente disposta a subire nomi come *Lenovo ThinkPad X1 Yoga Gen 7*. Per il produttore non sembra essere un problema. Penso a quei coniugi che si sentono chiedere in famiglia *per che cosa sono tutti quei soldi?* e possono borbottare *Yoga* in risposta e così confondere le acque; sono tempi in cui conta più il fitness del computing. Per il resto, a me più che un nome pare un albero tagliato, ogni parola un anello che deve raccontare un pezzo di storia.

Ma divago. Scrive Chin:

>Sfortunatamente, il nuovo processore [l’anello Gen 7 dell’albero tagliato] non fornisce il tipo di prestazioni che immagino interessi a chi usa lo Yoga; porta invece a una riduzione di efficienza che ritengo danneggi la sua valutazione complessiva, specialmente se messo a confronto con i laptop Apple. Può essere un problema di Intel più che di Lenovo; nondimeno, resta un problema.

Ancora:

>Sfortunatamente devo emettere la temuta sentenza: la durata della batteria non mi impressiona. Ho ottenuto una media di sei ore e tredici minuti […], un risultato che Apple e Amd spazzano via nel segmento degli ultrà portatili, con risoluzioni superiori. […] .
Il processore nell’apparecchio, il Core i7-1165G7, è stato più che adeguato per il mio carico di lavoro. Non sono convinta che l’aumento della potenza valga la corrispondente diminuzione di efficienza.

EH sì, il processore è così potente che chiede troppa energia e penalizza la batteria. Se la batteria non fosse sollecitata, il processore non potrebbe esprimere tutta la sua potenza.

Per il produttore non sembra essere un problema, ma dovrebbe, perché Apple e anche Amd dispongono di processori tecnologicamente superiori, capaci di erogare potenza senza dissipare inutilmente energia.

Più che la superiorità tecnologica, conta quella culturale: i processori Apple e anche Amd sanno che la civiltà dei consumi può prosperare nel futuro solo dandosi una regolata sui consumi stessi. Ma mica è detto che ci sia da rinunciare alla potenza, anzi.