---
title: "Miti da non sfatare"
date: 2024-02-05T00:31:26+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [Ai, intelligenza artificiale, Aiace, Viticci, Federico Viticci, Ajax]
---
Vince la giornata di oggi questo mitico, alla lettera, [rimarco di Federico Viticci](https://mastodon.macstories.net/@viticci/111859037999527832) sul nome del grande modello linguistico messo a punto senza fanfare da Apple e chiamato in codice Ajax.

(Da non confondere con [Ferret](https://macintelligence.org/posts/2024-01-16-il-silenzio-%C3%A8-open/) che è un altro tipo di operazione).

Ajax è il nome latino di Aiace, eroe greco della guerra di Troia che al termine di varie [peripezie e interventi divini](https://mythopedia.com/topics/ajax-the-greater) perde il senno e si toglie la vita con la spada dell’eroe troiano Ettore.

Dal suo sangue sboccia un fiore i cui petali mostrano l’immagine dei caratteri greci corrispondenti al nostro *Ai!Ai!*. Come Aiace appunto, o come Artificial Intelligence.

Si dice che Apple possa essere indietro nella corsa al sacro Graal, ma a me pare avantissimo invece. I nomi contano.