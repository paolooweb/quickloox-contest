---
title: "Non basta mai"
date: 2016-12-27
comments: true
tags: [ Toshiba, Powerline, Clue, Cluedo]
---
Il Natale informatico della famiglia allargata è stato meno preponderante di altri anni rispetto ai regali più tradizionali, per quanto con qualche nota di interesse.

Sono stati regalati hard disk Toshiba esterni, portatili, autoalimentati, da un terabyte. Oramai sono oggetti piccoli, gradevoli all’aspetto nonostante la funzione tecnologica, in una gamma di prezzo del tutto compatibile con un Natale generoso ma sobrio.

In una delle abitazioni, villetta unifamiliare su tre livelli, si progetta l’installazione di apparecchi Powerline per diffondere la rete anche nella parte seminterrata, dove è più difficile portare Wi-Fi è praticamente non c’è campo per la connessione cellulare. L’installazione vera e propria sta dando qualche problema in fase iniziale: autenticare gli apparecchi presso il produttore dovrebbe essere semplice, ma non funziona. Nel bailamme della festa non c’è stato modo di essere analitici nella giusta misura. Tornerò appena possibile in argomento.

Ho scoperto l’esistenza di [Clue Solver](https://appsto.re/it/mbdgH.i). Nessuno l’ha usata al tavolo di gioco; avrebbe negato gran parte del divertimento. Si conferma però che c’è davvero una app per qualunque cosa.

La morale di queste feste: lo storage è sempre il benvenuto, la rete vuole essere ovunque, qualsiasi obiettivo è degno di una app. La tecnologia non vuole mai bastare.
