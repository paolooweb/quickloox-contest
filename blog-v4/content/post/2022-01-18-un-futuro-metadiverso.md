---
title: "Un futuro metadiverso"
date: 2022-01-18T02:35:37+01:00
draft: false
toc: false
comments: true
categories:
tags:
---
L’ultimo tentativo da parte di Apple di creare qualcosa di vagamente vicino a una comunità immersiva è stato [eWorld](https://www.macworld.com/article/223467/remembering-eworld-apples-forgotten-online-service.html), che fortunatamente si è chiuso nel 1996.

Abbiamo quindi una ragionevole sicurezza che Apple non si butterà nel *metaverso* come si accingono a fare Facebook e altri, nella vana speranza che la maggior parte delle persone gradiscano passare la più parte del loro tempo dentro una realtà virtuale (non saranno la maggior parte, oppure non ci passeranno così tanto tempo).

Invece lavoreremo, impareremo e ci divertiremo con la realtà aumentata, più che quella virtuale, e potremo raggiungere risultati concreti. Questo mentre la corrente porta gli altri a replicare la situazione che c’era alla nascita del World Wide Web: tutti dovevano avere un sito, ma nessuno sapeva veramente perché.

A breve tutti dovranno mettersi in testa un casco o degli occhiali, ma senza sapere bene per qualche motivo. Tranne chi usa Apple, che potrà partire da un motivo e magari non venderà terreni virtuali sperando di fare soldi reali, ma ci munirà di applicazioni e tecnologie pensate per risolvere qualche problema esistente, non da inventare. [Second Life](https://secondlife.com) lo abbiamo già visto, è un posto bellissimo per una piccola comunità di persone interessate e finisce lì.

Il divario, di produttività, interesse, eleganza, aumenterà.

_Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile._
