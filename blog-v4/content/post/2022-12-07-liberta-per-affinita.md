---
title: "Libertà per affinità"
date: 2022-12-07T02:20:54+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [TidBits, Creative Cloud, Affinity, Adobe, Engst, Adam Engst, InDesign, Illustrator, Acrobat Pro, Photoshop, Publisher, Designer, Photo, Pixelmator Pro]
---
Adam Engst di *TidBits* non arriva dal niente per fare il TikToker, ma ha una reputazione pluridecennale di copertura del mondo Apple e ha pubblicato letteralmente di libri, manuali, guide, materiali promozionali e quant’altro.

Avendo conoscenze tecniche, è arrivato nel tempo a usare i suoi strumenti in modo ottimizzato per risparmiare tempo e fatica, invece di limitarsi all’uso superficiale delle sole funzioni immediatamente visibili. I suoi strumenti sono stati per molti, molti anni, Creative Cloud di Adobe. Oggi [scrive](https://tidbits.com/2022/12/05/consider-switching-from-creative-cloud-to-affinity-v2/):

>Ho chiuso l’abbonamento a Creative Cloud e iniziato a risparmiare cinquantaquattro dollari al mese. Non avevo particolari lamentele da avanzare né problemi con Adobe. La decisione è stata puramente finanziaria: cinquantaquattro dollari al mese sono quasi seicentocinquanta dollari l’anno, veramente troppo rispetto al valore che mi derivava da InDesign, Illustrator, Acrobat Pro e Photoshop, senza neanche considerare le quindici o giù di lì applicazioni di Creative Cloud che non ho mai installato.

Nel tempo la sua attività di pubblicazione si è spostata quasi tutta online e come risultato passavano mesi senza che gli capitasse di aprire le applicazioni Adobe. A un certo punto la sproporzione tra utilizzo e spesa si è fatta talmente evidente da farlo decidere.

Engst ricorda tutto questo mentre, per coincidenza, Affinity lancia una promozione per la quale fino al 14 dicembre ciascuna sua applicazione (Publisher, Designer, Photo) è scontata del quaranta percento e, per centoventi euro, si può acquistare una Universal License che dà diritto a tutte e tre le applicazioni su Mac, iPadOS e perfino Windows. Tutti i prezzi per le singole applicazioni e per la licenza universale sono facilmente consultabili sulla [pagina apposita del sito Affinity](https://affinity.serif.com/en-us/affinity-pricing/).

Centoventi euro sono pochi mesi di Creative Cloud. Sono per la vita o almeno per qualche anno, invece di un effimero abbonamento mensile che ti lascia pagatore di tutto e proprietario di niente.

Affinity Publisher non risolve i *miei* problemi con InDesign e Adobe e per fare ritocco fotografico sto su un’altra parrocchia, quella di [Pixelmator Pro](https://www.pixelmator.com/pro/).

Ciò detto, chi può approfitti. Le applicazioni Affinity sono uscite in versione 2, con un sacco di miglioramenti e di novità. Permettono di lavorare su Mac e proseguire su iPad o viceversa. Sono aggiornate e manutenute.

Un buon regalo di Natale può essere la libertà dai prezzi elevati e dalle politiche utenticide di Adobe. Questa è una grande occasione. Chi può, approfitti.