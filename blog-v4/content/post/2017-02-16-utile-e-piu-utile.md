---
title: "Utile e più utile"
date: 2017-02-16
comments: true
tags: [diskutil]
---
È già la seconda volta in poche settimane che un disco tra quelli di casa dà problemi e Utility Disco riferisce l’impossibilità di ripararlo, solo che poi neanche lo reinizializza.

Ed è la seconda volta che apro [diskutil](https://developer.apple.com/legacy/library/documentation/Darwin/Reference/ManPages/man8/diskutil.8.html), il quale sistema le cose in *souplesse*.

Linko la pagina del manuale perché ne consiglio vivamente la lettura. Costa mezz’ora di concentrazione, dopo di che si è praticamente imparato a usare sempre e comunque `diskutil` al posto di Utility Disco.

Per i più frettolosi, alla fine della pagina `man` ci sono gli esempi pratici. [L’ho già scritto](https://macintelligence.org/posts/2014-07-30-profondo-ricco/) e lo reitero.