---
title: "Ondate di calore"
date: 2022-07-23T02:15:56+01:00
draft: false
toc: false
comments: true
categories: [Hardware]
tags: [MacBook Air, M2, Apple Silicon, John Gruber, Gruber, Ars Technica, MacBook Pro, MacBook Pro 13”, MacBook Air M2, MacBook Air M1, M1]
---
*Ars Technica* titola che [MacBook Air M2 scalda così tanto che, se non lo facesse, andrebbe più veloce](https://arstechnica.com/gadgets/2022/07/the-new-macbook-air-runs-so-hot-that-it-affects-performance-it-isnt-the-first-time/).

Però John Gruber scrive su *Daring Fireball* che [questi computer con Apple Silicon sono veloci, sono efficienti e non scaldano](https://macintelligence.org/posts/2022-07-17-era-vero/). Dove sta il vero?

Questi sono tutti i passaggi della [recensione di Gruber](https://daringfireball.net/2022/07/the_2022_13-inch_macbook_air) in cui si parla della questione calore:

>Il calore è dove le persone sono spaventate. Lo sono per via delle loro esperienze con i portatili x86 (di Apple e di altri) dell’ultima decina di anni, mentre Intel perdeva la sfida delle prestazioni per watt e semplicemente non possono credere che un portatile sottile, veloce, durevole, che non scalda, senza ventole (nel gergo Apple, senza un “sistema attivo di raffreddamento”) sia non solo possibile, ma addirittura con prezzi accettabili per il consumatore. Sono qui per rassicurare: MacBook Air M2 è sottile, performante, durevole, non scalda ed è senza ventole.

>L’*ultima* differenza che la maggior parte delle persone noterà tra MacBook Air e MacBook Pro è che MacBook Pro ha il sistema di raffreddamento attivo e MacBook Air non lo ha. Non rimpiangeranno mai l’assenza di una ventola in MacBook Air e non la accenderanno o sentiranno mai se invece usano MacBook Pro.

Ora Gruber può essere partigiano o sbilanciato a favore di Apple, ma mai si è lanciato in affermazioni palesemente false. È vero che dice *la maggior parte delle persone* e quindi lascia la porta aperta a un surriscaldamento di MacBook Air per *qualcuno*, ma un conto sono i casi limite e un conto è l’uso quotidiano.

*Ars Technica* sostiene di avere effettuato dei test in proposito. Andando a leggere l’articolo relativo (che è la loro [recensione di MacBook Air M2](https://arstechnica.com/gadgets/2022/07/2022-macbook-air-review-apples-clean-slate/4/)), si trova questo passaggio:

>È chiaro che questo sistema senza ventole è (prevedibilmente) non proprio efficiente nel sostenere carichi di lavoro pesanti nel lungo periodo. Il MacBook Pro 13” del 2022 rappresentato nei nostri grafici ha le stesse specifiche. La sola differenza è il sistema di dissipazione del calore. Quando serve un picco improvviso di prestazioni, Air equivale più o meno sempre a Pro. Ma abbiamo notato che in attività protratte nel tempo, come rendering di video 4K, Pro batte spesso Air.

Grazie al cavolo. C’è qualcuno in sala convinto che le prestazioni di MacBook Air e MacBook Pro siano le stesse e che quindi la configurazione di MacBook Pro non abbia motivo di esistere? Speriamo di no.

Poi, *Ars Technica* lo sa perfettamente. Due paragrafi più sotto ed ecco:

>Quando navighi sul web, rispondi alla posta o fai anche un editing fotografico veloce qui e là, non noterai differenze; e questi sono d’altronde i casi d’uso per cui è pensato MacBook Air. Air è buono come tutte le altre sue alternative per lanciare velocemente le app e risultare generalmente veloce. Ma non è progettato per carichi di lavoro pesanti e prolungati.

In pratica, banale buon senso. Solo che il buon senso non accumula clic e allora meglio sparare un titolaccio per acchiapparne un po’, in un articolo separato da quello con i dati.

Ultima cosa: sempre *Ars Technica* fa riferimento a un canale YouTube intitolato *Max Tech*, per [un video di un quarto d’ora](https://www.youtube.com/watch?v=BSkcMwzZEFo) in cui si apre un MacBook Air (Apple fa le macchine chiuse, vero? Il diritto di riparazione eccetera) e si appiccicano dei cerotti termici per dissipare meglio il calore della CPU.

Beh, il video è una marchetta per vendere i cerotti termici. Secondo, hanno girato un video identico per fare esattamente la stessa cosa con un MacBook Air M1. E qui vorrei trovarlo, quello che è riuscito a surriscaldare seriamente un M1 al punto da doverlo imbottire di cerotti termici.

Insomma. MacBook Air M2 scalda se uno ci vuole arrivare proprio per forza, o perché ha comprato la macchina sbagliata nell’illusione di risparmiare, o per vendere cerotti termici. In tutti i casi, di tutte le ondate di calore, questa la si dissipa assai velocemente; basta leggere cose più sensate.