---
title: "La strategia misteriosa"
date: 2023-09-12T15:35:43+01:00
draft: false
toc: false
comments: true
categories: [Hardware]
tags: [Apple, iPhone, Daring Fireball, Gruber, John Gruber]
---
Come nella [Lettera rubata](https://edgarallanpoe.it/la-lettera-rubata/), Apple indulge spesso nel mettere in perfetta evidenza le proprie strategie e direzioni, per chiunque abbia abbastanza pazienza da guardare lo storico e unire i puntini.

John Gruber ne ha di pazienza e sa anche tirare le conclusioni, come nel suo articolo sulla [strategia biforcata di iPhone](https://daringfireball.net/2023/09/apples_two-pronged_annual_iphone_strategy), che si riassume con grande semplicità: ogni anno dal duemiladiciassette escono un modello per la linea pro e uno, diciamo, regolare. Il modello pro equipaggia il meglio della tecnologia e dura un anno, prima che lo soppianti il modello seguente; il modello regolare contiene qualche compromesso e qualche rifinitura e rimane in vendita per due o tre anni perdendo cento dollari l’anno sul prezzo di vendita, fino a quando, dopo essere stato il modello più vecchio a catalogo, esce di scena. Commenta Gruber:

>Gli iPhone Pro sono molto più entusiasmanti, ma i non-pro sono essenziali per il listino e l’ecosistema. In termini sportivi, i modelli Pro sono l’attacco e i non-pro la difesa. I riflettori vanno sull’attacco, ma è la difesa che fa vincere i campionati.

In pratica, è un modo di accontentare quanti più segmenti possibile della domanda. Chiaramente uccide qualunque chiacchiera da bar, ma la scienza esige il suo duro prezzo.