---
title: "La piattaforma degli animali"
date: 2021-06-25T01:26:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [Orwell, Windows, Tim Berners-Lee, Mosaic, Netscape, Internet Explorer, Bill Gates, The Internet Tidal Wave, NeXT, CERN, Microsoft, Disney, FrontPage, X-window] 
---
Ha [scritto](https://www.goodreads.com/quotes/6145-who-controls-the-past-controls-the-future-who-controls-the) George Orwell in *1984*:

>Who controls the past controls the future. Who controls the present controls the past.

*Chi controlla il passato controlla il futuro. Chi controlla il presente controlla il passato.*

È [stato appena scritto](https://blogs.windows.com/windowsexperience/2021/06/24/introducing-windows-11/) sul Windows Experience Blog:

>The web was born and grew up on Windows.

*Il web è nato ed è cresciuto su Windows.*

Si noti che Orwell non si è espresso rispetto al futuro. Provare a controllarlo ha una legittimità. Controllare il presente invece è una situazione pericolosa per tutti i controllati, dal momento che il controllo del presente consente la manipolazione del passato e quest’ultima è un mezzo non consentito per cercare di prendere controllo del futuro.

È la ragione per cui devo chiedere alle persone di buona volontà di agire, ora, in modo da impedire a questi mentitori, o ignoranti del passato, di prendere controllo del presente.

Non conta che giri questo messaggio o si faccia riferimento a questo blog. È invece estremamente importante che si facciano sapere, forte e chiari, i seguenti fatti:

- il web non è nato su Windows. È nato su un computer NeXT del CERN a opera di [Tim-Berners-Lee](https://www.w3.org/People/Berners-Lee/), nel 1989;
- il web non è cresciuto su Windows.

Questo secondo punto potrebbe essere espanso a piacere. Per restare brevi, Microsoft sottovalutò Internet, figuriamoci il web. Fu solo nel 1995 che Bill Gates inviò ai dipendenti Microsoft il memo [The Internet Tidal Wave](https://www.wired.com/2010/05/0526bill-gates-internet-memo/), *l’onda di marea di Internet*, in cui scrive:

>Forse avete già letto memo miei o di altri riguardanti l’importanza di Internet. Sono passato attraverso numerosi momenti in cui ho accresciuto la mia visione della sua importanza. Ora assegno a Internet il livello di importanza più alto.

Gates si è convinto dell’importanza di Internet nel tempo. Altro che *nato su Windows*.

Uno dirà, si parla di Internet, non del web. Ok; il web prevede il browser. [Mosaic](https://en.wikipedia.org/wiki/Mosaic_(web_browser)) (che nemmeno è il primissimo browser, probabilmente il quarto), primo browser ad acquisire una certa notorietà, è nato su X-window. Microsoft ne acquistò anni dopo la licenza per realizzare Internet Explorer, i cui sviluppi – e il cui tentativo di monopolizzare lo sviluppo e la navigazione del web, che avrebbe dovuto essere patrimonio comune – portarono a una causa antitrust contro Microsoft stessa, senza la quale probabilmente ancora oggi saremmo fermi a Internet Explorer e magari a FrontPage.

Controlli chi vuole: Disney ha registrato il proprio dominio .com con un anno di anticipo su Microsoft.

Bisogna muoversi perché quella riga su quel blog venga emendata, o chi l’ha scritta si porrà per controllare il presente, nello stesso modo in cui i maiali di quell’altro romanzo di Orwell spiegavano che tutti gli animali sono uguali, ma alcuni animali sono più uguali degli altri.

Siamo tutti uguali, invece.

*Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile.*