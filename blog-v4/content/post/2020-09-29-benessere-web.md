---
title: "Benessere web"
date: 2020-09-29
comments: true
tags: [Safari]
---
 ![Tracciamenti bloccati da Safari nell’ultima settimana](/images/tracking.png  "Tracciamenti bloccati da Safari nell’ultima settimana") 

Non c’entra nulla con le questioni economiche, ma certamente questo è un segno di agiatezza sul web: il browser che pensa a te e ti leva di torno i tracciamenti subdoli a scopo di propinare cattiva pubblicità.

Nei prossimi anni, questo sarà un privilegio.