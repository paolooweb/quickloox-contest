---
title: "Gli occhi per vedere"
date: 2013-11-21
comments: true
tags: [Jobs]
---
Nel 1980 Steve Jobs regalò un Apple II a [Seva Foundation](http://www.seva.org/site/PageServer), una organizzazione benefica dedita a curare malattie degli occhi nei Paesi in via di sviluppo, fondata da Larry Brilliant, medico amico della famiglia Jobs. Il cofondatore di Apple staccò anche un assegno di cinquemila dollari e mise nel pacco una copia di un software che si annunciava rivoluzionario, [VisiCalc](http://www.bricklin.com/visicalc.htm).<!--more-->

La cifra in sé era relativamente bassa, ma diede autorevolezza a Seva e incoraggiò altre donazioni, che raggiunsero i 25 mila dollari nel giro di poche settimane e permisero all’organizzazione di decollare.

Decollò (nuovamente) grazie all’Apple II anche un elicottero di oftalmologi, in panne in mezzo al Nepal a causa di un guasto al motore. Brilliant attaccò al computer un modem primitivo e organizzò una conversazione telematica con il produttore dell’elicottero e altre persone competenti da tutto il mondo, ottenendo le informazioni che servivano a una riparazione di fortuna.

Seva ha aiutato finora tre milioni e mezzo di persone a recuperare la vista in Bangladesh, Cambogia, Nepal, Tibet, India e altre nazioni.

Come [scrive](http://bits.blogs.nytimes.com/2013/11/20/a-gift-from-steve-jobs-returns-home/?_r=1) il *New York Times*, durante una cerimonia di festeggiamento del trentacinquesimo anniversario di Seva, l’Apple II è stato restituito a Laurene Powell – vedova Jobs – e al figlio Reed.