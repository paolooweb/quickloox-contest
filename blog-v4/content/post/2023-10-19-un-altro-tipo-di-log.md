---
title: "Un altro tipo di log"
date: 2023-10-19T16:52:33+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [iPhone 15 Pro, Apple Log]
---
Le differenze tra iPhone 15 e quelli precedenti, la classica domanda della morte che ti fa lo scettico: ma alla fine non è uguale a quello prima, solo un po’ più veloce?

Da tempo la risposta è sensata solo se si sposta sui dettagli, dettagli che i media consueti si guardano bene dal coprire preferendosi concentrare sul nome del processore o sui giga di Ram.

Così ci vuole Prolost per venire a sapere che [iPhone 15 Pro e Max supportano la registrazione di video logaritmico](https://prolost.com/blog/applelog), o log video nel gergo dei videomaker. Sono i primi iPhone a farlo.

Il video logaritmico è riconosciuto dai principali software di editing e consiste in un filtraggio di tanti elementi che (non solo) iPhone aggiunge alla resa finale del video per renderlo accattivante all’utente finale, in generale inesperto se non sprovveduto. Nella sua forma più pura, il video logaritmico assegna a ogni stop di luce lo stesso quantitativo di dati.

Non mi dilungo perché dal link di partenza ci si può addentrare in una spiegazione assai più dettagliata e precisa di quanto potrei fare, con dovizia di segnalazioni, materiali e app.

Basti dire che, se sembra materia per impallinati, ha a che vedere con la compatibilità verso il sistema di gestione di colore della Academy of Motion Picture Arts and Sciences americana. Per un videomaker americano che operi a livello professionale molto alto, tutto ciò è fondamentale al punto che a metà dell’articolo si legge questo:

>Nel giro di due anni Apple è passata dal rifiuto di vendere un laptop professionale con un lettore di schede SD a produrre una fotocamera che registra su media esterni.

Se infatti a iPhone 15 Pro o Max viene attaccato un disco esterno Usb-C, un video in formato ProRes Log viene automaticamente registrato sul disco esterno anziché nella app Foto.

Secondo Prolost la possibilità di lavorare con video logaritmico attirerà professionisti sulla piattaforma. Strano, di solito sento sempre racconti di abbandoni di massa.