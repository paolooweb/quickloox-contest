---
title: "Effetto moltiplica"
date: 2022-05-10T01:18:19+01:00
draft: false
toc: false
comments: true
categories: [Internet]
tags: [Apple, Goodfellow, Ian Goodfellow, Schiffer, Zoë Schiffer, The Verge, MacRumors, Gruber, John Gruber, Daring Fireball]
---
Ho commesso un po’ di errori e usato un po’ troppa approssimazione nel [post sulla dipartita da Apple di Ian Goodfellow](https://macintelligence.org/posts/2022-05-07-partenze-intelligenti/) e forse è meglio riparare in un post diverso per non snaturare quello originale.

Goodfellow, prima di tutto, era in Apple [da aprile 2019](https://www.cnbc.com/2019/04/04/apple-hires-ai-expert-ian-goodfellow-from-google.html) e non da quattro anni.

Zoë Schiffer non ha scritto un articolo ma [un tweet](https://twitter.com/ZoeSchiffer/status/1523017143939309568), che è stato ripreso da altri media aggiungendo dichiarazioni da personale Apple in forma anonima.

<blockquote class="twitter-tweet"><p lang="en" dir="ltr">Ian Goodfellow, Apple’s director of machine learning, is leaving the company due to its return to work policy. In a note to staff, he said “I believe strongly that more flexibility would have been the best policy for my team.” He was likely the company’s most cited ML expert.</p>&mdash; Zoë Schiffer (@ZoeSchiffer) <a href="https://twitter.com/ZoeSchiffer/status/1523017143939309568?ref_src=twsrc%5Etfw">May 7, 2022</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

È secondo queste dichiarazioni che Goodfellow si sarebbe riferito esplicitamente alla questione del ritorno al lavoro in presenza, mentre Schiffer riporta unicamente la dichiarazione sul desiderio di regole più flessibili per il suo team. Sembra ragionevole pensare che Goodfellow si riferisca indirettamente al rientro in ufficio, però non è esplicito e non è verificabile.

Ecco, anche, perché moltiplicare la notizia come sta facendo *The Verge* suona un filo artificioso. Un articolo di oggi titola [Dipendenti Apple di rilievo scrivono lettere al management, si licenziano per via del rientro in ufficio](https://arstechnica.com/gadgets/2022/05/remote-work-conflict-continues-at-apple-with-at-least-one-prominent-staff-departure/).

È risaputo che un gruppo di dipendenti scontenti abbia scritto una [lettera aperta](https://appletogether.org/hotnews/thoughts-on-office-bound-work) ai vertici di Apple per lamentarsi del ritorno alla presenza e la lettera, mentre scrivo, ha totalizzato duemilacinquantasette firme.

Quello che non torna è la parte *di rilievo* nel parlare di dipendenti Apple che si licenziano in nome del lavoro smart. Non che i duemilacinquantasette siano per forza ultime ruote del carro, ma a livello di cariche importanti sappiamo solamente di Ian Goodfellow e anche di lui esiste solo un riferimento indiretto. Prima di alludere a un esodo di talenti di rango si potrebbe almeno aspettare di averne due.