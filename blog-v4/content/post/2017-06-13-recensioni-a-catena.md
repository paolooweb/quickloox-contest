---
title: "Recensioni a catena"
date: 2017-06-13
comments: true
tags: [iPad, Pro, Viticci, iMore, MacStories, MacSparky]
---
Condivido il *modus operandi* di MacSparky, che volendo parlare del nuovo iPad Pro 10,5” si è limitato a [linkare](https://www.macsparky.com/blog/2017/6/105-inch-ipad-pro-reviews) le due migliori recensioni attualmente in circolazione: [Federico Viticci su MacStories](https://www.macstories.net/stories/the-10-5-ipad-pro-future-proof/) e [Rene Ritchie su iMore](https://www.imore.com/105-ipad-pro).

Pochi o forse nessuno ha maturato un’esperienza di uso professionale a tutto campo di iPad come Viticci, che oltretutto ha compiuto una scelta esistenziale e da due anni, parole sue, vive e lavora su un iPad Pro 12,9”, quello più grande, dove comunque le funzioni di affiancamento di app e multitasking riescono meglio che non su uno schermo come quello da 10,5”, migliorato rispetto al passato ma in ogni caso più piccolo.

Tuttavia il nuovo schermo lo ha conquistato per i grandi progressi fatti nell’amministrazione intelligente della frequenza di aggiornamento video da parte di iPad. I progressi compiuti nella resa del colore e nella naturalezza delle animazioni dell’interfaccia gli hanno lasciato il dubbio e sono riusciti a smuoverlo dalla certezza granitica di voler lavorare su un iPad Pro 12,9”.

Ritchie ha compiuto più una prova a tutto campo, mettendo sotto pressione l’apparecchio anche nell’uso all’aperto, per scattare foto e girare video, e tutto quant’altro si può fare.

Insieme i due articoli formano una eccezionale super-recensione che lascia l’acquolina in bocca in chiunque abbia progetti di aggiornamento della dotazione di iPad. E c’è da dire che iOS 11 rappresenta solo un momento nel (prossimo) futuro: al momento la beta è tale e quindi non sempre affidabile, incompleta e non indicativa di quanto potrà davvero fare un iPad Pro dopo l’autunno.
