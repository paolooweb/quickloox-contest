---
title: "Una soddisfazione ogni tanto"
date: 2016-10-15
comments: true
tags: [cuoredimela]
---
[Cuore di Mela](https://www.kickstarter.com/projects/295108589/cuore-di-mela-i-segreti-di-apple-iphone-e-mac/posts/1706099) 1.0 è completo. I finanziatori stanno per ricevere o stanno già ricevendo le copie elettroniche. Stiamo facendo l’ultimissimo giro di correzioni per la versione cartacea, presto in stampa.

Grande fatica, sul mio lato, perché per lavorare in modo corretto si è dovuto partire assolutamente da zero, senza rivedere cose già pubblicate o partire da materiale preesistente.

Grande soddisfazione, perché non si *voleva* partire da cose preesistenti ma provare finalmente a cambiare almeno un poco l’angolazione per dire cose più interessanti, o più attuali, o tutt’e due, o comunque provare a farlo.

Sono molto soddisfatto di alcune cose, mediamente soddisfatto di altre, meno soddisfatto di altre ancora, come è normale in progetti di una certa dimensione come questo.

L’importante – e qui – si crea tutto il divario con quello che si è sempre fatto nel passato – è pensare a questo momento come un punto di partenza e non un punto di arrivo. Ho una cartella piena di spunti da sviluppare, aggiunte, approfondimenti, cose per cui non c’erano lo spazio o il tempo, novità.

Come è normale per progetti di una certa dimensione come questo; ma nell’editoria tradizionale, questa cartella di cose rimaste la si butta via. Invece qui vuole essere lavoro per i prossimi mesi.

Sarà una esperienza interessante. Posso solo ringraziare tutti quanti hanno espresso il loro appoggio o anche solamente la propria opinione e invitare tutti a fare quello che normalmente, con un libro consueto, non si fa: mandare *feedback* incessante. E aiutarci a conseguire sempre meglio [il vero obiettivo](https://macintelligence.org/posts/2016-06-15-un-kickstarter-per-cambiare-il-libro/).