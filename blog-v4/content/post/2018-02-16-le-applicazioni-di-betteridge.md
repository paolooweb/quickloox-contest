---
title: "Le applicazioni di Betteridge"
date: 2018-02-16
comments: true
tags: [Universal, Mac, iPhone, app, Betteridge, macOS, iOS]
--- 
Ian Betteridge è un giornalista inglese che ha sintetizzato la mania di pubblicare [titoli che finiscono con un punto interrogativo](http://betteridgeslaw.com) osservando che in genere la risposta è *no*.<!--more-->

Quando si legge di tecnologia, la legge di Betteridge vale al quadrato e così va interpretato il titolo [App universali: Mac è in pericolo?](https://www.technightowl.com/2018/02/universal-apps-is-the-mac-in-danger/).

L’articolo nasce da una voce incontrollata secondo la quale le prossime versioni di macOS e iOS potrebbero accogliere app universali, poggiate su una base comune di codice e poi differenziate a livello di interfaccia utente per funzionare su macOS, su iOS o *su ambedue*.

Primo livello di lettura: mandrie di miopi hanno pronosticato la fusione di macOS e iOS, che non è mai avvenuta. Invece i due ambienti hanno beneficiato di mutui trasferimenti di tecnologia e rimangono tanto distinti che ottimizzati: una buona app per macOS sfrutta le potenzialità del sistema senza compromessi e così una per iOS.

La voce potrebbe benissimo essere infondata o male informata: magari Apple lavora a un ambiente di emulazione per Mac dove fare funzionare le app iOS. Oppure, più semplicemente, a cambiamenti sotto il cofano, che facilitino il lavoro degli sviluppatori intenti a portare una stessa app sui due sistemi.

In generale, quando si odono ragionamenti *tanto peggio, tanto meglio* come questi, va pensato che Apple – almeno quando lavora bene – sta attenta a preservare l’esperienza utente: se c’è in cantiere una tecnologia in tema di app universali, sarà mirata a evitare compromessi. Apple non incoraggerà gli sviluppatori a creare app pessime purché funzionanti ovunque; piuttosto, lavorerà per consentire la creazione di app universali migliori delle attuali dal punto di vista della valorizzazione della piattaforma. Le grandi sconfitte di Mac nella storia riguardano tutte funzioni che lo rendono superiore alle alternative, che gli sviluppatori non usano pur di stare sul minimo comune denominatore e buttare agli utenti programmi inferiori, purché ubiqui.

Sto con Betteridge e rispondo *no*.