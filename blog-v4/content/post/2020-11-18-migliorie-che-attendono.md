---
title: "Migliorie che attendono"
date: 2020-11-18
comments: true
tags: [Apple, Big, Sur, iWork, GarageBand]
---
Una cosa, cara Apple, che si potrebbe fare meglio e attende da anni: se aggiorni il sistema operativo e aggiorni le tue app in funzione del nuovo sistema operativo, non presentarmi l’aggiornamento delle app come se io, che devo ancora installare il nuovo sistema operativo, potessi davvero aggiornare loro prima del sistema.

Dovresti farmi vedere gli aggiornamenti che posso eseguire. *It should just work*.