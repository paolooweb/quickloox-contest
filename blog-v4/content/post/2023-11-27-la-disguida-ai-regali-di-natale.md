---
title: "La disguida ai regali di Natale"
date: 2023-11-27T18:54:02+01:00
draft: false
toc: false
comments: true
categories: [Web]
tags: [Kottke]
---
Secondo me a leggere la [guida ai regali di Natale](https://kottke.org/23/11/the-2023-kottke-holiday-gift-guide) di Jason Kottke non salta fuori neppure un suggerimento utile per un italiano con famiglia, o quasi.

Però è la più divertente, o almeno la più diversa dal solito, che mia sia capitata in tanti anni. Alla fine non ne esce niente? Però ho passato del tempo in relax e con curiosità.