---
title: "Gli dai una Bar e ti prendono l’iPad"
date: 2016-11-08
comments: true
tags: [Touch, Bar, GitHub, iPad]
---
Bella la notizia della *app* [Touch Bar Demo](https://github.com/bikkelbroeders/TouchBarDemoApp) che mostra il comportamento di Touch Bar su un Mac che ne privo, mostrando sullo schermo un facsimile della barra. Questo permette di farsi un’idea di che cosa succederebbe su un nuovo MacBook Pro.

C’è una parte ancora più interessante: la Touch Bar può essere proiettata su un iPad.

Sono già apparse *app* sparse per usare un iPhone o un iPad come *trackpad* aggiuntivo, come schermo aggiuntivo o comunque come apparecchio ausiliario verso un Mac.

Non avevo mai pensato alla possibilità di questo tipo di sfruttamento da parte di una applicazione su Mac, tuttavia: per dire, invece di avere una parte comunque consistente dello schermo di Mac occupata dall’interfaccia, ridisegnare l’interfaccia su un iPad disponibile e lasciare tutto lo schermo a disposizione dei dati.

La parte significativa del discorso è *ridisegnare*, fare una cosa apposta per avere su iPad il massimo risultato ergonomico e non solo mettere semplicemente a disposizione uno spazio per mostrare da un’altra parte l’interfaccia esistente.

Se qualcuno mi segnala precedenti in merito, li accolgo volentieri perché mi pare un’idea inedita e mi pare strano che lo possa essere. Una volta focalizzata Touch Bar, sembra un pensiero quasi ovvio.