---
title: "Non so perché"
date: 2021-05-14T01:30:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [Word, Raz Degan, Paola Barale, Jägermeister] 
---
Sta per chiudersi la settimana lavorativa in cui ho chiesto *perché usate Word?*

Mi aspettavo una risposta di quelle classiche. La compatibilità, la diffusione, lo usiamo fin da piccoli, è comodo, era dentro i PC, lo standard di fatto eccetera.

Invece si sono stretti nelle spalle. Il perché non c’era.

Una volta Word si piratava. Adesso le aziende usano Office365.

Cioè pagano tutti i mesi un canone. E magari non sanno perché.

Lascio il commento [all’ex fidanzato dell’ex valletta di Mike Bongiorno](https://www.youtube.com/watch?v=14ABu_msibM).

<iframe width="560" height="315" src="https://www.youtube.com/embed/14ABu_msibM" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

*Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile.*               