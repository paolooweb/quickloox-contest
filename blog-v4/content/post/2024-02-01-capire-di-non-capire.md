---
title: "Capire di non capire"
date: 2024-02-01T14:34:43+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [Chollet, François Chollet, ai, intelligenza artificiale]
---
Marcel Duchamp [espose un orinale](https://press.philamuseum.org/marcel-duchamp-and-the-fountain-scandal/) nel 1917 presso una mostra di artisti indipendenti a New York. Fece scandalo e cambiò la storia dell’arte, perché affermò il principio che a definire l’arte è l’artista stesso.


Trovo scandaloso e per giunta privo di potenziale di cambiamento il tentativo compiuto da varie persone a tutti i livelli, dai geni assoluti della materia fino ai relativisti d’accatto privi di competenza, di cambiare l’idea di *significato* e sostituirla con una qualche altra definizione che permetta di vendere gli assistenti generativi come [dotati di comprensione](https://macintelligence.org/posts/2024-01-28-non-ci-capiamo/).

Un modo efficace di sapere a che punto siamo e come stanno le cose è seguire questo [*thread* di François Chollet](https://twitter.com/fchollet/status/1736483628971082111?), un’autorità:

<blockquote class="twitter-tweet"><p lang="en" dir="ltr">To understand X means you have the ability to act appropriately in response to situations related to X -- for instance, you understand how to make coffee in a kitchen if you can walk into a random kitchen and make coffee.</p>&mdash; François Chollet (@fchollet) <a href="https://twitter.com/fchollet/status/1736483628971082111?ref_src=twsrc%5Etfw">December 17, 2023</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

L’intelligenza (e cioè, etimologicamente, la comprensione) è il saper agire nel contesto in cui ci si trova.

*Intelligence is not skill*: uno strumento straordinario per fare certe cose può essere privo di intelligenza. Chiedere al mio microonde.

Quando non abbiamo sufficiente intelligenza per un contesto, ricorriamo alla *generalizzazione* e togliamo dettagli per creare un modello che riusciamo a maneggiare in ogni caso.

Secondo questa definizione, un sistema generativo può generalizzare… fino a dove glielo consente il database che ha dietro.

Chollet porta l’esempio del cifrario di Cesare, in cui si crea una frase segreta spostando di alcune posizioni nell’alfabeto consonanti e vocali e la chiave per decifrare il messaggio è sapere di quante posizioni occorre spostare.

Un chatbot riuscirà a gestire cifrari di Cesare con numeri di posizioni che stanno dentro il database, per fallire se il numero è diverso. L’intelligenza del sistema sta nel database e l’interfaccia, di chat o altro, non fa che mostrare in modo vario il contenuto del database stesso.

Si possono risolvere meglio i cifrari di Cesare con la cosiddetta intelligenza artificiale magari ampliando il database per inserire più numeri, oppure con l’addestramento del sistema su un solutore generale di cifrari. Il sistema diventerà più bravo a risolverli, ma questo non risolve il problema generale: l’impossibilità *by design* di andare oltre il contenuto del database. Anche se quello che appare a video sembra incredibilmente acuto e centrato.

Capiamolo e spieghiamolo, è importante, c’è gente che paga per qualcosa che crede di ricevere e non è vero: i sistemi attuali *non capiscono*.