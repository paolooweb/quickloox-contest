---
title: "È solo l’inizio"
date: 2019-11-28
comments: true
tags: [Mac, Finder]
---
Questo blog ha bisogno di mille interventi che prima o poi troverò priorità sufficiente per decidere, ma un refuso o un errore nei post hanno priorità assoluta e ho appena viaggiato nel tempo a correggere due inesattezze.

Me ne accorgo leggendo il post via web e poi vado nel Finder a ritrovare il file originale. Naturalmente mi ricordo il titolo; apro la cartella dei post e digito il titolo nel campo della ricerca.

Naturalmente non funziona.

Torno sulla pagina e guardo la data del post. Torno nella finestra del Finder e digito la data del post nel campo di ricerca.

Funziona. Però non è affatto naturale.

Il problema è che il nome file dei post è nella forma *data-titolo*.

Quando digito il nome del file nel campo di ricerca, è come se avessi impostato una ricerca tipo *trova il nome file che contiene…*.

Quando digito la data, è come se la ricerca fosse *trova il nome file che inizia con…*.

Se a livello di sistema mi dai Spotlight che cerca anche dentro i file, a livello di Finder mi aspetto almeno di trovare un pezzo di nome file anche se non si trova all’inizio. Non ho detto contenuto, ho detto nome file, accidenti.