---
title: "La ricerca del brivido"
date: 2023-03-17T12:58:09+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [ChatGPT, Bing, Paoloo]
---
**Paoloo** ha cercato informazioni sul sottoscritto usando la novella combinazione di Bing e ChatGPT-4 e a mio parere c’è da preoccuparsi. Non perché sia io l’oggetto della ricerca o per come lui impieghi il tempo libero.

Parto da lontano. Un motore di ricerca non trova quello che vogliono ottenere, ma ciò che gli diciamo di cercare. Distinzione sottile, ma cruciale. In compenso, il risultato corrisponde al cento percento: qualsiasi pagina segnalata conterrà bene o male la ricerca che abbiamo inserito.

Un sistema di generazione di testo addestrato su un modello di linguaggio di grandi dimensioni non trova quello che gli diciamo di cercare, ma – per così dire – informazioni adiacenti. Il più delle volte sono pertinenti, ma dato un compito abbastanza lungo o sufficientemente impegnativo, il sistema sbaglierà.

Così accade che la mia scheda su Bing mostri titoli di libri che non ho mai scritto (e potrebbero persino non esistere, ma è un altro racconto). Con altre persone più importanti e informazioni più delicate, potremmo trovarci a un passo dal reato. Il motore di ricerca pubblica falsità che *ha fabbricato lui* e non esistono su alcuna pagina web. Per conseguenza, si potrebbe sostenere che il costruttore del motore di ricerca sia responsabile dei danni derivanti dalla pubblicazione di informazioni false sul motore di ricerca stesso.

Sì è sdoganato il fatto che *da una ricerca su Bing* (non da una chiacchierata con la supposta intelligenza) possano comparire informazioni non vere, a caso; questo è peggio persino delle *fake news* e trovo da brivido l’indifferenza di fronte a questa situazione. Ovviamente si sistemerà tutto con la prossima versione e il suo modello di linguaggio sempre più grande, certo, come no.