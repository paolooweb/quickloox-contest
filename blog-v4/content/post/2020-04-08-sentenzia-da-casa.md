---
title: "Sentenzia da casa"
date: 2020-04-08
comments: true
tags: [Google, coronavirus]
---
Perfino l’asilo della primogenita ha improvvisato comunicazione e distanza tra le maestre e i piccoli. La scuola si è trovata circa inesistente nel momento in cui le aule sono diventate inaccessibili e tra le maestranze c’è fermento, sia positivo che negativo.

Si possono leggere su Facebook [post ad alta densità di insegnanti](https://www.facebook.com/100002183212149/posts/2912545272161551/) e si scopre quanto sia composita la categoria.

Non conosco lo stato delle cose da dentro, visto dal docente professionista, che ha da misurarsi con il ministero, il programma e mille burocrazie.

La sensazione però è che una vasta maggioranza viva l’improvviso ricorso alle lezioni a distanza come un colpo di scena inaspettato, al quale reagire, certo, ma da posizioni di scarsa preparazione ed esperienza, per non parlare delle conoscenze tecniche.

Vorrei capire se e quanti di questi docenti che scoprono la scuola online per la prima o quasi, siano al lavoro per aggiornarsi e integrare il loro eccellente bagaglio analogico con buone nozioni di insegnamento digitale.

Niente di incredibile, eh. Anche un percorso da Ground Zero in avanti, che so, l’esplorazione di [Insegna da casa di Google](https://teachfromhome.google/intl/it/). Cose semplici, di base, in italiano, specifiche, gratis. Ovviamente vengono enfatizzati gli strumenti di Google, ma la tara si fa facile.

Quando inizierà a tornare la normalità, quanti docenti saranno più preparati di prima alla didattica a distanza e all’uso del digitale?

Vorrei sbagliarmi, però l’atteggiamento prevalente sembra quello di chi ha aperto malvolentieri l’ombrello e aspetta che spiova, con commenti su quanto è tutto malfatto, sbagliato, difficile, negativo. E che giustamente si auspichi il ritorno in aula, per seppellire con un macigno l’esperienza presente e dimenticarla a velocità smodata.

Mi sbaglio?
