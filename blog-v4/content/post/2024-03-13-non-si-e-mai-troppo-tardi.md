---
title: "Non si è mai troppo tardi"
date: 2024-03-13T02:26:12+01:00
draft: false
toc: false
comments: true
categories: [Internet]
tags: [LibreItalia, Copernicani]
---
Anche quest’anno ho perfezionato le mie iscrizioni annuali a [LibreItalia](https://www.libreitalia.org) e all’associazione [Copernicani](https://copernicani.it).

Invito tutti e ciascuno a fare lo stesso perché, probabilmente lo dico tutti gli anni ma effettivamente è vero, sono iniziative che rispondono a urgenze importanti.

LibreItalia difende la causa del software libero da LibreOffice in poi. Frequento aziende e, ahimé, Microsoft spadroneggia. Ma non è il problema vero: è che sterilizza la mente di chi si ritrova a subire software malfatto senza possedere strumenti per conservare l’indipendenza mentale. Più 365 si diffondono negli uffici, più la cultura e le capacità informatiche delle persone regrediscono. Provare per credere, sono fresco reduce da [situazioni indicibili](https://macintelligence.org/posts/2024-03-07-segnali-di-vita-intelligente/) con in mezzo gente che magari ha figli, magari ha ambizioni di carriera, e letteralmente non sa fare niente con gli strumenti più potenti (parlo in generale) mai creati dall’uomo.

Una voce dissonante nel panorama omologato è necessaria. LibreItalia rende cento volte quello che costa (può costare solo dieci euro).

I Copernicani mi hanno insegnato alcune nozioni adulte che mi mancavano. Per esempio, continuo a detestare i politici, ma ho capito il perché della politica. La speranza di cambiare le cose dipende da quanti sono disposti ad ascoltare e, soprattutto, da quale livello ascoltano. Poter portare un messaggio civile, equilibrato, ragionevole a un parlamentare non incapace (ce ne sono) o a un capitano di industria aumenta di molto la probabilità che qualcosa accada davvero e incida davvero sulle cose. L’utopia del cambiare le cose dal basso è bellissima. E non cambia niente.

L’associazione ospita persone anche schieratissime, fuori, ma dentro si è apolitici e apartitici e funziona. Le idee trovano ascolto, le proposte concrete possono trovare braccia e menti collaborative per farsi concrete. Si organizzano occasioni di confronto e dialogo con persone importanti. Il punto non è che siano importanti; è che possano dare una visione delle cose più vicina al vero rispetto ai precotti dei media tradizionali, mai così inutili e così tremendamente disinformativi oltre che disinformati. (Anzi, un anacronistico abbonamento a corrieri e repubbliche scadenti e illeggibili può essere trasformato con profitto in una iscrizione ai Copernicani).

L’anno scorso ho proposto di interpellare Douglas Hofstadter sul tema della presunta intelligenza artificiale e mi sono ritrovato a organizzare un [incontro con Padre Paolo Benanti](https://macintelligence.org/posts/2023-05-27-pescatori-di-prompt/), personalità che opera ai massimi livelli, presso il governo italiano, le principali aziende, le Nazioni Unite. Parlare con una persona del genere può solo allargare il proprio punto di vista. Chiunque può tuttora constatare sull’account YouTube dei Copernicani quanto sia stato stimolante sollevare argomenti importanti presso una persona competente riconosciuta a livello planetario.

Da quell’incontro mi è capitato di [finire in televisione dentro uno speciale sull’intelligenza artificiale](https://macintelligence.org/posts/2023-07-12-le-sirene-dei-media/), a rappresentare i Copernicani. Non è per pavoneggiarsi, ero l’unico disponibile a concedere qualche ora al giornalista autore dello speciale.

Non credo che qualcuno dei Copernicani abbia guardato la trasmissione, di livello vicino al nazionalpopolare. Ma amici, conoscenti, parenti lontani, colleghi ed ex colleghi… non ho dati, ma certamente l’audience di quello speciale (replicato un paio di settimane fa e sempre disponibile sulla piattaforma del canale) è stata degna di qualche nota.

E così ho potuto passare messaggi che tuttora sono vitali; che non c’è vera intelligenza, che lo strumento è utile ma non va oltre sé stesso, che abbiamo disposizione assistenti utili e un po’ stupidi, non oracoli o quasipersone cibernetiche.

Senza i Copernicani niente sarebbe accaduto. Sarebbe mancato qualcosa nell’informazione sull’intelligenza artificiale, lo dico senza vanagloria alcuna; decine di persone avrebbero potuto fare meglio di me. Ma è anche vero che il destino fa il fuoco con la legna che ha.

Quindi, spendere cento euro (scontatissimi per gli studenti) per la speranza di finire in TV o cenare con Elsa Fornero? Macché, chi se ne frega. Spenderli per contribuire con quello che ognuno è in grado di dare, incontrare competenze e capacità straordinarie, imparare a secchiate, soprattutto avere più speranze di lasciare davvero un segnetto nell’universo.

Capisco che arrivati a questo punto qualsiasi velleità di iscriversi davvero si sarà spenta e allora mi permetto un ultimo affondo. Un po’ per anagrafe e un po’ per combinazione entro in contatto con un mare di gente che magari non è neanche pensionata, ma si pone nella condizione del vecchietto con le mani incrociate che sbircia i cantieri.

Volontariamente. Gente piena di energie, con esperienze invidiabili, competenze di valore, tutto buttato nell’apatia e nella pigrizia stanca di chi si dice *vecchio* con il birignao, a significare *sono pieno di energie ma non ho più voglia di fare niente, lasciatemi stare*.

È uno spreco indicibile, specie in un Paese vecchio. LibreItalia e Copernicani sono pieni di gente con i capelli d’argento e con gli acciacchi, che lavora con il piacere della libertà di movimento a cause importanti, anche se l’età ha passato la sesta potenza di due.

C’è bisogno. L’egoismo è una scelta insindacabile. Ce ne sono anche altre, però. Concedersi un dubbio. Pensare allargato invece che ristretto. Basta seguire un link.