---
title: "Sessanta e lode"
date: 2018-12-28
comments: true
tags: [A12, A12X, Skylake, Fortnite]
---
Non si fa in tempo a commentare le [virtù nascoste dei processori Arm di Apple](http://www.macintelligence.org/blog/2018/12/27/divergenze-parallele/) che salta fuori la notizia del [supporto dei sessanta fotogrammi al secondo per Fortnite su iOS](https://www.gsmarena.com/fortnite_gets_60fps_support_on_ios-news-34429.php).

Per contestualizzare, [Fortnite](https://www.epicgames.com/fortnite/en-US/buy-now/battle-royale) è il gioco di combattimento del momento e sessanta fotogrammi per secondo sono il Graal di chi gioca, perché permettono una visione fluida del terreno di gioco e una minore latenza, cioè vantaggio nei confronti di chi vede meno fotogrammi. Una frazione di secondo può decidere una battaglia.

Arrivare a questo risultato sui computer da tasca ha dell”incredibile e difatti non è per tutti: il supporto di Epic Games [si ferma ai tre modelli 2018 di iPhone](https://www.ubergizmo.com/2018/12/why-iphone-x-cant-run-fortnite-60-fps/). Sugli iPhone precedenti il gioco stresserebbe l’architettura fino a provocare surriscaldamenti e consumi di batteria problematici o del tutto inaccettabili. Anche su quelli appena usciti, pare sia da usare la qualità grafica media e non quella alta, pena perdite di fotogrammi o appunto temperature operative sgradevoli.

Se pare poco, si confronti la situazione con quella di Android, dove Epic ha annunciato di stare lavorando allo stesso obiettivo e la data prevista è [campa cavallo](https://www.ubergizmo.com/2018/12/60-fps-fortnite-android-not-soon/)


