---
title: "Note su Notes"
date: 2021-11-07T00:46:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [Notes, Lotus, Steven Sinofsky, Ms-Dos, Windows] 
---
In aggiunta alle cose appena scritte ieri, voglio rimarcare quanto Notes fosse un software selvaggiamente innovativo per gli anni ottanta e come sia gradualmente stato accantonato a favore di cose Microsoft che valevano meno, erano a innovazione zero e si sono affermate non per merito ma per tattiche di marketing e posizionamento, oltre che per il monopolio di Ms-Dos e poi di Windows.

Nessuno lo può spiegare meglio di Steven Sinofsky, che di Windows fu responsabile per un bel po’. La storia completa è a pagamento, ma anche questa [veloce ripassata via Twitter](https://twitter.com/stevesi/status/1456383154856476682) è adeguata almeno a porsi il problema.

<blockquote class="twitter-tweet"><p lang="en" dir="ltr">BONUS: Competing with Lotus Notes - new bonu$ post in Hardcore Software. // Today most all biz &gt; N size use Exchange as their mail server (most in the cloud). Yet before Exchange arrived Lotus Notes was a successful and innovative product. A story /1 <a href="https://t.co/xyZMzS2MaY">https://t.co/xyZMzS2MaY</a></p>&mdash; Steven Sinofsky – stevesi.eth (@stevesi) <a href="https://twitter.com/stevesi/status/1456383154856476682?ref_src=twsrc%5Etfw">November 4, 2021</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

*Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile.*