---
title: "Il gusto della vendita"
date: 2023-03-30T16:59:39+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [Autodesk, AutoCad, Maya]
---
Autodesk ha sempre snobbato Mac in modo tendente al vergognoso, al punto che quasi coglie di sorpresa l’annuncio delle [edizioni di AutoCad e Maya per Apple Silicon](https://arstechnica.com/gadgets/2023/03/after-two-years-autodesk-maya-and-autocad-become-apple-silicon-native/).

AutoCad (vale anche per la versione LT) arriva con migliorie varie tra le quali, per chi ci tiene, la lingua italiana. Secondo Autodesk, [le prestazioni su Apple Silicon rispetto alla versione Intel arrivano a raddoppiare](https://blogs.autodesk.com/autocad/autocad-2024-mac/).

Non ci sono proclami particolari [rispetto a Maya](https://makeanything.autodesk.com/maya/2024-384LG-698256.html). Anche qui comunque arriva qualche aggiunta significativa oltre alla compatibilità con Apple Silicon.

Prestazioni che arrivano a raddoppiare, su una piattaforma che letteralmente freme per poter scatenare potenza di calcolo a costi energetici mai prima così bassi, a parità di prestazioni. Diversamente da quella che sembrava divenuta cattiva tradizione, forse in Autodesk hanno riscoperto il gusto di vendere il proprio software anche a chi usa Mac. Alla fine potrebbe anche giovare all’azienda, vendere a tutti i clienti possibili, chissà.