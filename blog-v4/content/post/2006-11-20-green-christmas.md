---
title: "Green Christmas"
date: 2006-11-20
comments: true
tags: [Macworld, Greenpeace, Onufrio, Sauven, Denton, Bfr, Epeat, Tbbpa]
---
[Questo articolo è stato pubblicato su Macworld Italia di gennaio 2007]

*Greenpeace si è messa a straparlare di Apple per avere pubblicità a costo zero. E diamogliela, così magari la smettono*

Non è facile il Natale, se hai intorno un ecologista. Ti metti in casa l’albero vero e sei un criminale perché hai tagliato un albero. Allora lo compri di plastica e sei un criminale, perché resterà nei secoli e si fa con il petrolio. Dunque dici vado in montagna e ne pianto uno, e sei un criminale perché inquini con la tua auto e alteri il paesaggio naturale causando danni all’ecosistema. Stai a casa al freddo e a luci spente, ma con il tuo respiro contribuisci a coprire il pianeta di anidride carbonica. A Natale, come nel resto dell’anno, per un ecologista vai bene solo se sei morto, possibilmente in una zona dove non contaminerai il terreno con le tue puzzolenti spoglie organiche. Lui, l’ecologista, è sempre a posto. Il problema lo provoca sempre qualcun altro.

Figuriamoci se a Greenpeace può andare bene che si producano computer. Mica si chiamano GreenPc.

**Chi è senza peccato scagli la prima mela**

Greenpeace ha bisogno di pubblicità e si è inventata una campagna su Apple, sapendo che approfittare di Acer, per fare un nome, non interessa a nessuno, ma approfittare di Apple invece fa rumore. Non importa che siano cose vere o esatte, basta fare rumore. Cominciamo dal sito italiano di Greenpeace: *[Aggiornamento 2018: nel 2007 qui c’era un link, ma non funziona più e la cosa vale per la maggior parte dei link dell’articolo.]*

>Qui a Greenpeace amiamo i Mac, ma non sopportiamo che siano imbottiti di sostanze chimiche tossiche.

[Lunga tiritera sui cimiteri elettronici in Cina e in India, nei quali — dove esistono — è gravissimo che vadano computer non riciclati, ma che questi computer siano Apple oppure Dell o Altro non fa un cavolo di differenza e dunque l’argomento serve solo come esca].

>La Apple […] è agli ultimi posti nella Eco guida ai prodotti tecnologici lanciata da Greenpeace. Con 262 milligrammi per chilo, il MacBook contiene i livelli più alti, tra i 5 modelli testati — Acer, Apple, Dell, HP e Sony — di BFR (TBBPA), un composto chimico altamente tossico. Inoltre la Apple ha adottato politiche di ritiro e programmi di riciclo limitati agli Usa, mentre diverse compagnie si sono impegnate su scala mondiale.

Anche questo messaggio è altamente tossico. Contiene veleno comunicativo. Ecco una analisi sommaria:

1. La pagina originale di Greenpeace, a Greenpeace.org/apple, dice *We love Apple*. Non dice *Amiamo i nostri Mac*. C’è gran differenza tra amare la suocera e gradire la sua paella alla valenciana.
2. la parola *imbottiti*, alla luce dei risultati raccolti dalla stessa Greenpeace, è semplicemente ridicola.
3. la nozione di *sostanza chimica tossica* è fuorviante. Una pianta di prezzemolo contiene molte più sostanze chimiche tossiche di un MacBook.
4. A proposito: non è un MacBook. È un MacBook Pro. Questi non hanno neanche letto la loro indagine.
5. La Eco guida ai prodotti tecnologici è un’idea di Greenpeace, basata su criteri arbitrari di Greenpeace, senza nessuna autorità che non sia quella di Greenpeace. In italiano: racconta quello che vuole Greenpeace.
6. Nemmeno! Gran parte delle valutazioni contenute nella Eco guida non si basa su fatti ufficiali o dati concreti, ma su affermazioni contenuti nei siti delle varie società e su promesse per il futuro. In altre parole: se un reparto relazioni pubbliche la sa lunga, non importa che venda computer o bombe ecologiche; prende buoni voti nella Eco guida.
7. La Eco guida è fatta con criteri a pera. Per esempio, Nokia ha un voto più alto di Lenovo. Ovvero: se il tuo Thinkpad ti dà scandalo perché inquina, buttalo… e comprati un cellulare. È roba senza senso. Se la tua cucina brucia male il gas, comprati un aspirapolvere e salverai l’ambiente.
8. Fatti i conti, 262 milligrammi per chilo — pensando ai tre chili di un MacBook Pro — fanno una bella cifra, 786 milligrammi. Quasi un grammo. Ma non è così. Il dato si riferisce alla ventola del computer. Peserà… cinquanta grammi? Ossia un sessantesimo di tre chili. Quindi, un sessantesimo di 786 milligrammi. Una dozzina di milligrammi, più o meno. Non 262.
9. Hanno testato un modello per società e da lì tirano conclusioni sulla società. Facciamo finta che il MacBook Pro inquini, o non inquini; questo non dice niente sul MacBook, che è fatto con materiali diversi. Né sul MacPro, che è molto più grande. Né sul Mac mini, che è molto più piccolo. Né sui Cinema Display. Né sui Mighty Mouse. E questo vale per tutte le società in ballo. Guardano come metti la freccia a sinistra e ti danno il giudizio su come guidi. Ovviamente è una scemenza.
10. Nel dato della ventola, Apple è agli ultimi posti. Su altri dati invece è ai primi. Ma questo non viene detto. Si agitano i 262 milligrammi per chilo. E chissà perché proprio le ventole e proprio Apple! Evidentemente, se avesse giovato di più prendersela con Hp, avrebbero isolato un dato di Hp. Sempre e comunque scorretto, a dire poco.

**Ecologia comportamentale**

Il sito italiano prosegue:

>non entreremo in azione appendendo banner in cima al quartier generale della Apple, né bloccando i suoi rifornimenti con le nostre navi, né saltando nel Mac World più vicino.

A parte lo scollamento dal mondo reale (che cos’è per loro un Mac World? Un numero della rivista? Un negozio? Una fiera? Non si sa), Greenpeace ha affittato uno spazio al Mac Expo UK di Londra. Ed è stata cacciata dalla fiera poco dopo l’inizio. Subito si è pensato alla congiura: cattiva Apple che censura l’eroica Greenpeace e le impedisce di denunciare lo scandalo. Ma è andata veramente così? Bob Denton, Event Director, la racconta in maniera diversa. In una intervista a Macworld Usa, Denton ha puntualizzato che la cosa non aveva niente a che vedere con la distribuzione del materiale propagandistico di Greenpeace: *Avevano pagato e avevano tutto il diritto di essere presenti*. I problemi sono iniziati quando un gruppo di dodici attivisti si è raccolto fuori dalla fiera, davanti all’entrata, e ha iniziato a distribuire volantini e mele. Un conto è avere una persona che distribuisce volantini, un conto è un gruppo di dodici, che intimidisce un po’. Sempre Denton: *vogliamo che la gente possa entrare in fiera calma e rilassata*.

Così è stato detto a Greenpeace che potevano tranquillamente piazzare due persone a volantinare presso le scale di accesso alla fiera, a patto che l’ingresso (comune a tutti e non prerogativa di Greenpeace) restasse libero.

Più tardi hanno cominciato a lamentarsi cinque degli espositori, uno dei quali era Apple. Non per le opinioni di Greenpeace; perché questi ultimi entravano negli stand altrui a sostituire il materiale promozionale degli altri espositori con il loro, oltre a organizzare foto di gruppo (in pratica usando gli stand altrui per farsi pubblicità).

A un certo punto, racconta sempre Denton, hanno messo una mela nel passeggino di un bambino e hanno cominciato a scattare foto nonostante la mamma non avesse dato loro il permesso. Denton li ha richiamati e gli ha detto che, se non si fossero comportati in modo più rispettoso, sarebbero stati espulsi dalla manifestazione. Quattro nuovi reclami più tardi, due da singole persone e due da espositori, è scattata l’espulsione.

*Cercavano la provocazione*, ha commentato Denton. *Era il loro unico obiettivo*.

**Greenpièce**

Se Apple produce computer inquinanti, è più che giusto sollecitarla a migliorare in questo senso. Accusarla per farsi pubblicità è tutt’altra questione. Greenpeace non ha alcuna autorità per distribuire patenti ecologiche a chicchessia e, a dirla tutta, l’intera faccenda sa molto di *pièce*, di commedia recitata per guadagnare spazio sui media. Cari amici di Greenpeace, volevate spazio su Macworld? Ecco fatto.

(firma) Lucio Bragagnolo (<a href="mailto:lux@mac.com" title="Lucio Bragagnolo">lux@mac.com</a>, http://www.macworld.it/blogs/ping/) garantisce che tutti i byte usati per produrre questo articolo sono riciclabili.

## (riquadro) Come è andata veramente al Mac Expo di Londra

La [versione di Bob Denton, Event Director](https://www.macworld.co.uk/news/mac/Greenpeace-thrown-out-macexpo-16291/). [E la [versione di Greenpeace](https://www.Greenpeace.org/archive-international/en/news/Blogs/makingwaves/Greenpeace-kicked-out-of-london-mac-expo/blog/9410/)].

## (riquadro) Bugie, grandi bugie e Greenpeace

*Roughlydrafted.com* ha svolto un [lavoro completo e documentato](http://www.roughlydrafted.com/RD/Home/29C5599A-FCD8-4E30-9AD5-5497999ABA1B.html) sulle mistificazioni della campagna di Greenpeace su Apple e sulla tossicità dei computer in generale. C’è molto più cromo esavalente nel nostro servizio di posate da cucina che in un laptop. Le dosi dei materiali pericolosi sono ben sotto i limiti delle direttive europee. Gli articoli si possono leggere a http://snurl.com/12mdt (dove si legge anche di quella volta che una barca di Greenpeace ha distrutto cento metri quadrati di corallo protetto), http://snurl.com/12med (con link al documento del Sierra Club che riconosce *l’eccellenza delle iniziative ambientali di Apple*), http://snipurl.com/12mei e http://snipurl.com/12mej.<br />
((( fine box 2 )))

((( box 3 )))<br />
## Parola di Greenpeace (e di Apple)

La Eco guida di Greenpeace si può scaricare in formato Pdf da http://www.Greenpeace.org/raw/content/international/press/reports/greener-electronics-guide.pdf. La classifica dei costruttori secondo Greenpeace è scaricabile da http://www.Greenpeace.org/raw/content/international/press/reports/greener-electronics-ranking-c.pdf. Lo studio dei portatili condotto da Greenpeace, ancora una volta in Pdf, si scarica dall’indirizzo http://www.Greenpeace.org/raw/content/international/press/reports/toxic-chemicals-in-computers.pdf. La pagina italiana della campagna Green My Apple si trova a http://snurl.com/112ju. Il sottosito dedicato all’ambiente da Apple sta su http://snurl.com/12mf4.<br />
((( fine box 3 )))

((( box 4 )))<br />
## Volare (più) bassi

Stando al *Times Online*, John Sauven, direttore delle campagne di Greenpeace, ha portato la famiglia in vacanza in aereo in Italia e, sempre in aereo, ha viaggiato per affari verso la foresta amazzonica brasiliana. Tre tonnellate di anidride carbonica emesse nell’atmosfera per i suoi comodi. E magari aveva dietro pure un portatile! http://snipurl.com/12mhl.<br />
((( fine box 4 )))

*[Quanto segue è comparso su Macworld Italia di febbraio 2006.]*

## Una lettera finta verde, tante falsità da arrossire e originalità nero, scusate, zero

A *Macworld Italia* è arrivata questa lettera da Greenpeace:

§§§

La medaglia d’argento dell’EPA è una sorta di premio di consolazione che il governo statunitense concede alle imprese che si limitano a rispettare quanto già stabilito per legge. Stupisce quindi che la Apple esulti per un risultato in realtà mediocre. Al sotto di questa performance, infatti, Apple non sarebbe comunque potuta andare, per non uscire dai parametri di legge.

I criteri di valutazione usati da Greenpeace nella propria guida ai prodotti elettronici si basano su standard più rigorosi e garantiscono prodotti realmente ecologici.

Per esempio l’EPA non richiede l'eliminazione del PVC o dei ritardanti di fiamma bromurati, due tipi di sostanze tossiche che altri produttori hanno già deciso di ritirare gradualmente dai propri prodotti.

Alla Apple Greenpeace chiede di non contentarsi della "sufficienza" che le dà il governo Bush, lo stesso che non fa nulla per il cambiamento climatico e che promuove le armi di distruzione di massa, ma di aspirare al massimo.

Apple, devi stupirci, ti vogliamo leader non solo nel design ma anche nel rispetto dell'ambiente!

Giuseppe Onufrio,<br />
direttore delle campagne di Greenpeace

§§§

Riassunto della situazione. Macworld online ha citato (http://snurl.com/187ym) un articolo di Ars Technica (http://snurl.com/187yr) dove si riporta che l’Agenzia americana per la protezione dell’ambiente (EPA) concede simboliche medaglie di bronzo ai prodotti che soddisfano il minimo dei requisiti di legge, d’argento a quelli che vanno oltre e d’oro ove eccellano. La classifica completa (EPEAT) sta sul [sito omonimo](http://greenelectronicscouncil.org/epeat/epeat-overview/). Nessun prodotto ha una medaglia d’oro, ma Apple domina nei notebook, per esempio, e fa nel complesso la figura migliore di tutte. Questi risultati contrastano con la Greener Electronics Guide di Greenpeace, scaricabile da snurl.com/187zy. E adesso le nostre osservazioni.

L’EPA è una agenzia federale, indipendente da chi viene eletto. L’EPEAT sarebbe uguale se il Presidente degli USA fosse Hillary Clinton. Nominare George W. Bush e le armi di distruzione di massa (questione grave, ma che c’entra?) è solo propaganda. Nel consiglio di amministrazione di Apple siede Al Gore e, durante le presidenziali 2004, Apple ha finanziato John Kerry.

Stupisce leggere che Apple non potrebbe andare sotto i suoi risultati nell’EPEAT. Potrebbe avere medaglie di bronzo, o essere ultima in qualche graduatoria e invece non è così. Se è un premio di consolazione, perché ci sono aziende che Greenpeace reputa ecologiche, mentre nella EPEAT arrancano?

Questo porta ai criteri. Greenpeace chiama i propri *standard* ed è una bugia, perché non sono certificati da nessuno. Si fanno le regole da soli. Sono anche “standard” pessimi perché, per esempio, per dare il voto ad Apple Greenpeace ha testato <em>un</em> prodotto Apple. La EPEAT ne ha <em>sedici</em>. Inoltre l’EPEAT si basa sulle caratteristiche dei prodotti oggi sul mercato. Greenpeace invece dà voti arbitrari alle promesse che fanno le aziende per il futuro. Possiamo comprare bombe ecologiche, raccomandate da Greenpeace soltanto perché l’ufficio stampa di chi le fabbrica ha dichiarato che un domani saranno migliori.

Onufrio, infine, firma una lettera, come se fosse sua. È poco più di una traduzione del blog americano di Greenpeace, pagina snurl.com/18812. O è lui a scrivere il blog americano oppure ci faccia parlare con una persona vera; per ripetere le cose di altri basta un feed RSS.

Quello che c’era da dire in generale su Greenpeace (attaccano Apple unicamente per avere pubblicità e Apple è l’azienda migliore da attaccare a questo scopo) l’ho spiegato con dovizia di particolari nella mia rubrica Preferenze di gennaio. Se qualcuno volesse rileggerlo, mi scriva che gli mando il testo *[si tratta della prima parte di questo post]*.

Lucio Bragagnolo (<a href="mailto:lux@mac.com" title="Lucio Bragagnolo">lux@mac.com</a>)
