---
title: "Una rottura con il passato"
date: 2021-01-13
comments: true
tags: [ Big, Sur, Octopress]
---
Si metta agli atti che il ritardo nella pubblicazione di questi post è dovuto al post-installazione di Big Sur, il quale ha messo fuori uso il motore di pubblicazione del blog.

Ho provato in tutti i modi che conosco a riparare il danno e a reinstallare il motore, ma niente di fatto mentre scrivo.

È probabile che tornerò in linea con un motore diverso. Di per sé è una notizia e anche una bella rottura con il passato.

È anche una bella rottura in generale.
