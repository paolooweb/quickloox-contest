---
title: "Un giorno per giocare"
date: 2017-08-15
comments: true
tags: [CoC, Clash, Clans, Angband, Lisp, Gama, Jam]
---
Più festa di così, in Italia, quasi neanche Natale. Mi limito a ricordare che abbiamo un clan di Clash of Clans attivo e [sempre disposto ad accogliere nuovi membri](https://macintelligence.org/posts/2017-05-31-si-e-fatta-notte/).<!--more-->

Ricordo anche di avere [lanciato una sfida](https://macintelligence.org/posts/2017-06-29-la-sfida-dell-estate/) su Angband. Sono quasi al trentunesimo livello, che significa più o meno entrare nella fase centrale del gioco. Pericolosa perché è facile montarsi la testa e commettere imprudenze, mentre la via alla vittoria è la prudenza massima.

Infine, sono appassionato a tempo (molto) perso del linguaggio Lisp. Anche quest’anno si è svolta la [Lisp Game Jam](https://itch.io/jam/lisp-game-jam-2017-easy-mode): realizzare un gioco con Lisp in un massimo di dieci giorni. Non è un linguaggio alla moda, ma è elegante (una qualità che travalica la moda) e sorprendente quando ci si vuole appassionare. Fare funzionare questi giochi potrebbe essere la sfida iniziale.

Nessuno si convincerà a esplorare Lisp a seguito di questa segnalazione, ma a Ferragosto me la concedo.

Assieme a un abbraccio generale. Buona vacanza, in un posto bello (persino a casa, farla bella almeno per l’occasione se non lo fosse già), con persone belle intorno e, capitasse un minuto di ozio, un bel *device* o computer sottomano. Giochiamo fino a domani con animo lieto e il sole caldo.