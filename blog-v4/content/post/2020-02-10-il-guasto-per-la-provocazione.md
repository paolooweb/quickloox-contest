---
title: "Il guasto per la provocazione"
date: 2020-02-10
comments: true
tags: [Doctorow, Attivissimo, iPad, Gibson]
---
Cory Doctorow è sempre stato provocatorio e la provocazione è un atteggiamento apprezzabile e raro in quest’epoca di pensiero obbligato e moralismo coatto.

La provocazione deve però essere centrata e credo che Doctorow abbia sbagliato a [riproporre oggi](https://boingboing.net/2020/01/27/nascent-boulangism.html) il pezzo che dieci anni fa scrisse sull’arrivo di iPad. Per lui *Ipad*.

Che cosa scrisse dieci anni fa, sotto il titolo *Perché non comprerò un iPad (e penso che neanche tu dovresti)*?

Per esempio, che *se non puoi aprirlo, non è tuo*. Apple ][+ era completo degli schemi elettrici della scheda logica, per chiunque volesse metterci mano.

iPad, invece, ha un modello di cliente diverso:

>Quello stesso stupido stereotipo di madre tecnofobica, timida, deconcentrata rappresentata in un miliardo di varianti di “troppo complicato per mia mamma”.

Il modello di interazione con iPad, raccontava dieci anni fa Doctorow, è quello di un *consumatore*,

>descritto memorabilmente da William Gibson come qualcosa delle dimensioni di un cucciolo di ippopotamo, del colore di una patata bollita vecchia di una settimana, che vive da solo nell’oscurità di una casa mobile nei dintorni di Topeka. È coperto di occhi che bruciano a causa del suo sudore copioso e continuo. Non ha bocca… né genitali e può esprimere le sue pulsioni estreme di rabbia assassina e desiderio infantile solo cambiando canale su un telecomando universale.

C’è molto altro ma salto alla fine, sotto il titolo di paragrafo *I gadget vanno e vengono*.

>L’iPad che compri oggi sarà rifiuto elettronico in un anno o due (meno ancora, se non paghi per farti cambiare la batteria).

Ancora:

>Se vuoi vivere nell’universo creativo dove chiunque abbia una buona idea può realizzarla e dartela per farla funzionare sul tuo hardware, iPad non è per te.

Sbagliare qualche predizione a distanza di dieci anni ci sta. Però bisognerebbe avere il coraggio di ripercorrerle le predizioni e magari ammettere qualche sbavatura.

iPad, dieci anni dopo, è tuttora complicato per mia mamma, anche se ha semplificato molte cose. Utente appassionato, frequente e intenso di iPad, fatico a mapparmi su un cucciolo di ippopotamo e una o due cose creative su iPad mi capita di provare a prepararle. Non sento la mancanza di idee creative da fare funzionare, semmai il contrario; avessi il tempo di provare tutto quello che merita.

Dove però proprio non ci siamo è l’ossessione per la batteria. Il mio primo iPad, comprato a maggio 2010, è uno dei primi che arrivarono a Milano. Sono passati dieci anni, le mie figlie lo usano regolarmente per giocarci, la batteria funziona egregiamente, prima di dichiararlo rifiuto elettronico bisognerà passare sul mio cadavere.

Cory: sono passati dieci anni, ma il tuo articolo non è maturo. È guasto.