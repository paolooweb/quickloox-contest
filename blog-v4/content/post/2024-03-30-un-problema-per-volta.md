---
title: "Commenti: un problema per volta"
date: 2024-03-30T02:38:01+01:00
draft: false
toc: false
comments: true
categories: [Internet, Software, Blog, Hardware]
tags: [Comma, nginx, AMD, x86, ARM]
---
Le cattive notizie sono che provare a usare i commenti genera sempre un errore 404.

Le buone sono che abbiamo ridotto di molto, sempre in teoria, i punti potenziali di errore e i motivi possibili del mancato funzionamento, in partenza mostruosamente numerosi, ora sono molti ma molti meno.

Saltano fuori anche ingenuità che all’inizio non si considerano nemmeno. Controllando e ricontrollando, salta fuori che uno dei problemi potrebbe essere la mia pretesa di fare girare un eseguibile Arm su una macchina virtuale con Cpu x86_64. Ma appunto, bisogna avere una intuizione, altrimenti non è che il sistema si faccia molto parte diligente.

Non si vedono, ma abbiamo fatto progressi. Nel frattempo, per sentirci, [i canali non mancano](https://macintelligence.org/post/2024-03-28-lavori-in-corsa/).