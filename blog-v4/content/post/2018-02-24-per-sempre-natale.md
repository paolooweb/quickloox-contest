---
title: "Per sempre Natale"
date: 2018-02-24
comments: true
tags: [Patently, Apple, Samsung, Nikkei, iPhone, Oled]
---
*Patently Apple* [riassume](http://www.patentlyapple.com/patently-apple/2018/02/apples-iphone-x-is-the-instant-scapegoat-for-samsungs-failure-to-win-oled-orders-from-chinese-vendors.html) in maniera un po’ confusa ma definitiva la vicenda degli ordini di schermi Oled per iPhone X che [secondo Nikkei](https://asia.nikkei.com/Business/Trends/OLED-panel-glut-looms-as-Apple-slashes-iPhone-X-production?page=2) sarebbero in diminuzione rispetto a fine 2017, segno di crisi delle vendite.<!--more-->

Incredibile, vero? Si vendono più iPhone sotto Natale che sotto Quaresima. Una notiziona.

*Patently Apple* sottolinea come Samsung abbia prodotto più schermi Oled del fabbisogno generale perché vari produttori cinesi, dopo averne annunciato l’impiego, hanno fatto marcia indietro e sono tornati ai cristalli liquidi. In più nessuno fuori da Apple conosce esattamente la ripartizione delle forniture di componenti, mentre è noto che varie aziende cercano di accreditarsi presso Apple per venderle schermi Oled.

Insomma: un problema di Samsung viene fatto passare per un problema di Apple, la quale – a dare retta ai sensazionalismi – se riduce la produzione, [lo fa sempre](https://www.ped30.com/2018/02/20/nikkei-slashes-apple/) con il verbo *to slash*, letteralmente *menare un fendente*. L’idea che tra prima di Natale e dopo Natale ci sia un calo fisiologico, o la nozione consolidata storicamente che al trimestre migliore dell’annata segue quello peggiore dell’annata, perché le persone spendono durante le feste e poi tirano il fiato, non sembra interessare nessuno.

Certo, il Natale per chi deve farsi leggere arriva quando si possono scrivere cose che fanno il botto pure se, in verità, si tratta di bolle di sapone.
