---
title: "La palestra dell’ardimento"
date: 2015-04-03
comments: true
tags: [FreeCiv, Sourceforge, GitHub, Wesnoth, PCGen, LibreOffice]
---
Mi ha scritto **Marco** (da **grassetto** a **grassetto**):

**Sebbene** sia un programmatore in erba, mi piacerebbe tanto localizzare programmi e tradurre documentazione.<!--more-->

Ho provato in passato a contattare diversi sviluppatori ma nessuno mi ha risposto. Senza obbligarti a cercare nulla, conosci a memoria qualche portale dedicato o progetti che vorrebbero (possibilmente per Mac/iOS) essere tradotti? Da dove si **inizia?**

Così gli ho risposto per email:

i depositi più ingenti di software libero sono oggi [Sourceforge](https://sf.net) e [GitHub](https://github.com/). Per ogni software esiste una comunità con forum, documenti, *readme*, *mailing list* eccetera. Lì puoi trovare il mondo. Usualmente trovi documenti che ti spiegano come partire e se non trovi niente basta chiedere.

Se hai in mente qualche progetto preciso, sul sito del progetto trovi sempre indicazioni per cominciare. Un esempio a caso, [Battle for Wesnoth](http://wesnoth.org) è un grosso progetto che contempla programmazione, interfaccia, *design*, audio, documentazione, traduzioni in più lingue, grafica… c’è bisogno di tutto. A volte c’è già qualcuno, a volte no, a volte sì ma non basta eccetera. Entri nella *community*, chiedi e un passo dopo l’altro entri nel team per quanto ritieni opportuno. Altro esempio che conosco bene è [PCGen](http://pcgen.org).

La mia email finiva qui, solo che tutto è meno ozioso di quello che sembra. Ripeterò allo sfinimento che programmare è il nuovo inglese e non serve essere madrelingua o avere un accetto perfetto, ma *serve* capire e farsi capire anche in modo rudimentale, o almeno saper decifrare uno slogan pubblicitario o la domanda del turista che vuole sapere se girare di qui o di là.

Con queste premesse, le comunità raccolte attorno ai migliori programmi *open source* sono un’autentica palestra dell’ardimento. Entrare non costa niente, è possibile darsi da fare a qualsiasi livello a patto che ci sia impegno e serietà (altrimenti la gente se la prende e fa bene), c’è tantissimo da imparare nello stesso posto dove si trova un sacco di gente che sa le cose e alla fine si migliora anche con l’inglese.

[LibreOffice](http://www.libreoffice.org) ha fame di programmazione Mac e poi c’è un vecchio pallino che ho: [FreeCiv](http://freeciv.wikia.com/wiki/Main_Page), che da tempo non viene più distribuito in una versione Mac installabile con una normale immagine disco e lanciabile con un normale doppio clic.

Non c’è bisogno di un programmatore fenomeno: il sito elenca [varie possibilità](http://freeciv.wikia.com/wiki/Install-MacOSX) per arrivare comunque al risultato. Quindi il codice è sostanzialmente a posto. Esistono anche istruzioni per [compilare](http://freeciv.wikia.com/wiki/Build-MacOSX) dal sorgente.

(Tutti questi passi, noiosi per un non tecnico, sono superabili giocando [dentro il browser](http://play.freeciv.org/)).

Servirebbe un programmatore disposto a produrre una versione funzionante sulle versioni più recenti di OS X e impacchettarla dentro una immagine disco come si deve. È un problema relativamente semplice per un aspirante programmatore, ma non stupido se ci si arriva per la prima volta. C’è da studiare, capire, applicare.

Chi riuscisse nell’impresa avrebbe la mia gratitudine. Potrei anche sfidarlo una notte. (Vincerebbe).