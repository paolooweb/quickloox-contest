---
title: "La resa dei conti"
date: 2014-03-17
comments: true
tags: [SourceForge, Mac, Windows]
---
Anni fa, parlando di programmi per Mac o per Windows, avevo fatto un confronto tra i programmi *open source*, piuttosto facili da contare: basta andare su [SourceForge](http://sourceforge.net).<!--more-->

Ho rifatto la verifica e non si può davvero proprio più dire che ci siano molti programmi in più per Windows.

 ![Programmi open source](/images/programmi-per.png  "programmi open source")