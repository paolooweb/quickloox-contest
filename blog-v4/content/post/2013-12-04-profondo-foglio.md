---
title: "Profondo foglio"
date: 2013-12-04
comments: true
tags: [Venerandi, ebook, ePub, Pages, InDesign]
---
Definitivo Fabrizio Venerandi in [Quello che Vedi è Quello Che Affiora](http://salvoesaurimentoscorte.wordpress.com/2013/11/25/quello-che-vedi-e-quello-che-affiora/).<!--more-->

>WYSIWYG funziona benissimo per la stampa su carta perché sulla carta rimane attaccato solo l’inchiostro o il toner.

>Il testo digitale è come il mare, lascia affiorare alcune cose, ma sotto alla superficie dell’acqua possono esserci ancora un sacco di cose da usare.

Ecco perché [oggi chi scrive deve padroneggiare gli strumenti tecnici](https://macintelligence.org/posts/2013-11-20-al-cuore-del-problema/). Ecco perché chi propone la scorciatoia, tipo scrivere in Pages (o, assai peggio, InDesign) crede di risparmiare tempo. In realtà ragiona sul vecchio foglio piatto e perde una dimensione per strada.

Le pagine dei libri e dei testi virtuali, ne hanno tre.