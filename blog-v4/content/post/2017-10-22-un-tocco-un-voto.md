---
title: "Un tocco, un voto"
date: 2017-10-22
comments: true
tags: [referendum, tavolette, tablet]
---
Rifuggo totalmente da considerazioni politiche in merito al [Referendum sull’autonomia](http://www.regione.lombardia.it/wps/portal/istituzionale/HP/istituzione/referendum-autonomia) lombardo e invito i commentatori a rifuggirne in egual modo.

Voglio invece registrare il fatto tecnologico. Che il sistema sia valido o meno, sicuro o colabrodo, costoso o economico, frega niente da un punto di vista sistemico. È una questione di iterazioni e di contesto; se il colore degli apparecchi fosse sbagliato, domani qualcuno potrebbe rifare la stessa cosa con il colore giusto e la portata dell’avvenimento resterebbe tale e quale.

L’avvenimento è che per la prima volta, in ambito istituzionale e con numeri di questa portata (milioni di aventi diritto al voto), si vota con il tocco su una tavoletta.

Si dà per scontato che quanti si saranno presentati ai seggi si troveranno sufficientemente a proprio agio con una tavoletta da poter votare (e ai promotori, ovviamente, interessa che questo avvenga nei numeri più elevati). L’interazione con uno schermo *touch* è stata promossa a forma di comunicazione basilare al punto da applicarla al voto.

Dietro a questo fatto c’è una evoluzione culturale enorme della popolazione. In meglio o in peggio, è altra questione. Da oggi, per l’istituzione, toccare lo schermo vale quanto trascinare la matita. I regolamenti su elezioni di altro tipo rimarranno comunque tiranni a lungo; tuttavia ignorare questo segno dei tempi, a livello tecnologico, sarà sempre più difficile.