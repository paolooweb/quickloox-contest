---
title: "L’insolita zuppa"
date: 2021-02-18T02:39:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [iPhone, Newton, Newton Press, 512 Pixels, MessagePad, iBooks Author, soup, Hemlock] 
---
Ora non trovo link e probabilmente se li trovassi non funzionerebbero più, tuttavia ricordo distintamente diverse critiche rivolte ad Apple per la decisione di non divulgare il codice sorgente di Newton una volta chiusa la sua produzione.

La verità è che, del tutto involontariamente, Newton è stato il banco di prova per arrivare a iPhone. L’interazione tramite schermo, il riconoscimento della scrittura a mano, la miniaturizzazione e molto altro sono tutte tecnologie precise oppure di area che in prima battuta o in un tempo successivo sono tornate tutte.

Anche quando sono state poco fortunate. *512 Pixels* [ha ricordato il venticinquesimo anniversario](https://512pixels.net/2021/02/newton-press/) di [Newton Press](https://web.archive.org/web/19981202091431/http://product.info.apple.com:80/pr/press.releases/1996/q2/960215.pr.rel.newton.html), un sistema che riceveva documentazione da Macintosh e la trasformava in materiale fruibile su MessagePad, pronto da consultare, distribuire, stampare, inviare via fax (sì, era il 1996).

Non mi stupirei se frammenti di quel codice si fossero infiltrati negli anni dentro [iBooks Author](https://support.apple.com/en-us/HT211136) o nella funzione di macOS che permette di acquisire all’istante una scansione o una foto da un iDevice.

Una cosa che non è riemersa, non ancora, è la [soup](http://www.canicula.com/newton/prog/soups.htm), il sistema di immagazzinamento e consultazione dati che usava Newton. Qualcosa di nettamente diverso dalle abitudini del tempo e che ha trovato pochi riscontri altrove.

Non ho la preparazione tecnica per valutare le sue caratteristiche, ma so bene che la risposta di Newton a livello di sistema, quando chiedevi un dato, era sempre pronta e molto funzionale. So che le *soup* [si comportavano molto bene se una memoria di massa veniva rimossa dal sistema](http://linuxfinances.info/info/nonrdbmsmisc.html) e che potevano miscelarsi quando un certo insieme di dati era suddiviso tra memorie di massa diverse, niente di più.

Unix ha certamente una portata e una stabilità che le *soup* non hanno mai avuto; però il concetto di un filesystem diverso dall’ordinario starebbe bene su un iPhone, o un watch, o domani qualcos’altro, chissà. Su Newton si erano viste cose interessanti, per esempio il motore di ricerca [Hemlock](https://cs.gmu.edu/~sean/projects/newton/Hemlock/), che salvava i propri dati di interesse in due *soup* distinte.

Quando parliamo di innovazione dobbiamo ricordare anche quello che non ha funzionato, o funzionava ma non ha trovato applicazione, o ha trovato applicazione ma non interesse. Quello che trasforma le nostre vite è una frazione di quello che nasce nei laboratori di ricerca.

*Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile.*