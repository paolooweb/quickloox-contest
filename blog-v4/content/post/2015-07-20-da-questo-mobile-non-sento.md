---
title: "Da questo mobile non sento"
date: 2015-07-23
comments: true
tags: [podcast, iPhone, Android]
---
Facciamo finta per un momento che sia vero: l’82 percento di tutto l’ascolto di *podcast*, quando è *mobile*, [avviene su iPhone](http://www.appleworld.today/blog/2015/7/20/iphone-rules-the-mobile-podcast-world).<!--more-->

82 percento che, va ricordato, riguarda il [venti percento](https://macintelligence.org/posts/2015-07-14-astenersi-perdisoldi/) dei modelli venduti.

In media, se ogni iPhone ascolta quattro podcast, ci vogliono quattro Android per metterne insieme uno.

Su iPhone si compra di più perché li compra gente più ricca e come scusa regge. Si naviga di più il web perché l’esperienza d’uso del sito medio è migliore e regge anche questa.

Adesso aspetto una spiegazione sui *podcast*. Chi compra iPhone avrà magari l’orecchio assoluto.