---
title: "Restiamo connessi"
date: 2020-05-01
comments: true
tags: [Apple, Amazon, Microsoft, Google, Flavio, Auld, Lang, Syne, Facebook]
---
La paternità [predispone chimicamente all’emotività](https://www.healthygallatin.org/blog/is-it-normal-for-a-dad-to-cry-more-after-a-baby/).

Riflettevo sui buoni risultati finanziari di [Apple](https://www.apple.com/newsroom/2020/04/apple-reports-second-quarter-results/), [Amazon](https://ir.aboutamazon.com/news-release/news-release-details/2020/Amazoncom-Announces-First-Quarter/), [Alphabet](https://abc.xyz/investor/static/pdf/2020Q1_alphabet_earnings_release.pdf?cache=4690b9f) (Google), [Facebook](https://investor.fb.com/investor-news/press-release-details/2020/Facebook-Reports-First-Quarter-2020-Results/default.aspx), [Microsoft](https://www.microsoft.com/en-us/Investor/earnings/FY-2020-Q3/press-release-webcast). Buoni perché c’è dentro un mese di virus eppure sono di segno positivo. Nel prossimo trimestre la musica sarà meno allegra di sicuro, però sono colossi con tutta la possibilità di passare un momento brutto senza troppi traumi.

E mi dicevo, sono quelli che bene o male ci tengono in piedi. Chiaro che lavoro e salute sono in equilibrio instabile per tantissimi; bisogni primari a parte, però, se le nostre comunità non sono ancora collassate si deve alle chat, alle videoconferenze, alle risorse condivise, alla collaborazione remota, alla facoltà di informarsi ed essere informati alla profondità desiderata.

Poi **Flavio** mi ha mandato questa foto, dal sovrappasso ferroviario di via Farini a Milano, lato nord.

 ![Tabellone pubblicitario in via Farini a Milano. Foto di Flavio](/images/farini.jpeg  "Tabellone pubblicitario in via Farini a Milano. Foto di Flavio") 

In tempi normali vale due risate. In questi ho visto la rappresentazione del dopobomba in cui saremmo piombati senza la rete. Quello schermo gigante isolato e impossibilitato a comunicare ha parlato alla mia pancia.

A pochi metri di distanza la primogenita dormiva, cullata da [Auld Lang Syne](https://youtu.be/acxnmaVTlZA).  Curavo intanto una [raccolta di risorse anticoronavirus](https://www.apogeonline.com/articoli/in-rete-contro-il-coronavirus-redazione-apogeonline/) che mi sorprende non per la quantità ma per la tipologia; l’offerta solidale di arte, musica, libri, film è alta anche di livello e il maggiore tempo libero di tanti, purtroppo magari loro malgrado, non basta a spiegarlo. Abbiamo ancora cose da dire sull’[incrocio tra tecnologia e arti liberali](/blog/2016/11/09/umani-e-algoritmi/).

Tutto questo per arrivare a scrivere il più tardi possibile che mi sono commosso per come questa rete ci tenga insieme.

<iframe width=560 height=315 src=https://www.youtube.com/embed/acxnmaVTlZA frameborder=0 allow=accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture allowfullscreen></iframe>
