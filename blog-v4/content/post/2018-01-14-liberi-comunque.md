---
title: "Liberi comunque"
date: 2018-01-14
comments: true
tags: [Libreitalia]
---
Il 2017 di [LibreItalia](http://www.libreitalia.it) è stato segnato da una raffica di successi, iniziative e sostegno reale alla causa del software libero.<!--more-->

Purtroppo l’estate è stata peggio di quella caraibica quando passano gli uragani, per via di problemi societari che hanno fatto passare in secondo piano tutte le cose buone fatte e il lavoro appassionato di tanti soci.

Il danno in reputazione è stato notevole. La causa del software libero resta tuttavia imperativa e un punto di riferimento come LibreItalia è necessario.

Per questo rinnoverò la mia tessera di socio e invito a fare lo stesso.

La tessera standard costa dieci euro. Si possono donare somme aggiuntive e chi tocca o supera i cinquanta euro può fregiarsi della qualifica – onorifica – di socio sostenitore.

L’anno scorso l’organizzazione ha fatto cose ottime ma non ha brillato in quanto tale. Le persone che la compongono restano però di eccellenza e sono certo che sapranno evitare derive, per puntare agli obiettivi veri. Per esempio proseguire nella [conversione a LibreOffice di un ministero](http://www.marcosbox.org/2017/02/corso-libreoffice-difesa-libreitalia.html) oppure incoraggiare le municipalità italiane a seguire l’esempio recente di Barcellona, che [va verso il software libero](https://joinup.ec.europa.eu/news/public-money-public-code).

Denaro pubblico, codice pubblico: è giusto che i soldi dei contribuenti siano spesi in software trasparente e verificabile, oltre che migliorabile da chiunque.

L’iscrizione a LibreItalia per il 2018 si effettua entro il 31 marzo via PayPal a info@libreitalia.it o tramite bonifico bancario, Iban IT03I0335901600100000152784, conto corrente 100000152784 intestato a LibreItalia, Bic BCITITMX, con causale *Quota associativa di (nome e cognome) anno 2018*.

La libertà, ha detto un presidente americano, non è ereditaria. Va difesa continuamente e non è mai scontata, o si perde. Questa è una bella occasione per un gesto concreto e anche molto più incisivo, in termini di un discorso più generale sulla libertà, di quello che può sembrare. Associamoci.
