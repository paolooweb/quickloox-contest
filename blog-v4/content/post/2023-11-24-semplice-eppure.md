---
title: "Semplice, eppure"
date: 2023-11-24T17:20:33+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [git, branch, Evans, Julia Evans, Sant’Agostino]
---
Ciao mi chiamo Lucio e ho un problema. Siccome uso poco git, a volte ho lacune di comprensione anche rispetto a concetti che dovrebbero essere elementari.

In questo quadro che deve rimanere comunque di ottimismo, confesso che il concetto di *branch*, sì lo so, lo uso, tuttavia se volessi scriverci sopra un pezzo di libro, come dire, non avrei certezze granitiche da riversare di istinto sulla pagina. *Si parva licet*, mi pare di essere (un cinquecentododicesimo di) Sant’Agostino che diceva di capire perfettamente la natura del tempo quando doveva spiegarla a sé e di non capirci niente appena gli toccava di doverla spiegare ad altri.

Da oggi, comunque dopo avere letto [questo articolo di Julia Evans](https://jvns.ca/blog/2023/11/23/branches-intuition-reality/), sono molto più sicuro di prima di sapere che cosa sia effettivamente un *branch* in git.

Meglio comunque che a scrivere il libro sia qualcun altro… ma va già molto meglio.