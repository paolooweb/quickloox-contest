---
title: "La bomboletta di Raid"
date: 2022-03-26T01:00:26+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [Ping!, Mimmo, Terminale, rmdir, mv, rm, Raid]
---
Antefatto: si lavora a circa tremilacinquecento post prodotti durante la prima incarnazione del blog, *Ping!*. La loro struttura va uniformata allo standard attuale prima di poterli rimettere online.

Erano altri tempi e, per tenerli organizzati, il nome file cominciava con tre lettere: il nome del primo post cominciava con *aaa*, il nome del secondo con *aab*, quello del terzo con *aac* e così via.

Arrivati a *aaz*, il post seguente iniziava con *aba*, e poi *abb*, *abc*… fino a che mettevo la prima serie di file in una cartella dal nome *aaa-aaz*. Poi arrivava la cartella *aba-abz*. Quella dopo era *aca-acz* eccetera.

Alla fine, dopo avere completato *aza-azz*, mettevo tutte le cartelle dentro una cartella *aaa-azz*. Il post successivo avrebbe avuto un nome file che inizia con *baa* e, avanti veloce, si sarebbe arrivati a una cartella *baa-bzz* con dentro sottocartelle *baa-baz*, *bba-bbz* e via dicendo.

Riassumendo:

Ping! (cartella)<br>
|<br>
+- aaa-azz (sottocartella)<br>
|<br>
+-+- aaa-aaz (sottosottocartella)<br>
+-+-+ aaa primo file<br>
+-+-+ aab secondo file<br>
+-+-+ …<br>
+-+-+ aaz ventiseiesimo file<br>
|<br>
+-+- aba-abz (sottosottocartella)<br>
|<br>
+-+- aca-acz (sottosottocartella)<br>
|<br>
+-+- … (sottosottocartella)<br>
|<br>
+-+- aza-azz (sottosottocartella)<br>
|<br>
+- baa-baz (sottocartella)<br>
(eccetera)

Il primo passo di lavorazione consiste allora nel toglierle tutti i file da tutte le cartelle e sottocartelle e averli tutti a un unico livello. Qualcosa che certamente, con tremilacinquecento file in tre livelli gerarchici, non viene praticissima nel Finder.

Nel Terminale invece è una passeggiata… a patto di ragionare correttamente.

All’inizio non ci sono riuscito proprio bene. Come primo passo, ho provato `mv ./* .` ma non ha funzionato. Chi sa dire che tipo di errore ho ricevuto e perché? Un aiuto: il comando giusto era `mv ./*/* .` che ha risolto il primo step.

Per il secondo passo, siccome non avevo ancora capito, ho riprovato ancora una volta `mv ./* .` che, ovviamente, di nuovo non ha funzionato. Secondo me avrebbe dovuto essere  `mv */* .` ma ci sono arrivato solo mentre usavo il pensiero debole e scrivevo, con fatica e però con sicurezza, `mv [a-z][a-z][a-z]-[a-z][a-z][a-z]/* .` per beccare matematicamente le directory. Così ho preso la strada più lunga ma mi sono tolto il pensiero.

Infine ho eliminato le directory ormai vuote, con sicurezza tronfia; o meglio ci ho provato, perché il prevedibile `rmdir [a-z][a-z][a-z]-[a-z][a-z][a-z]` non ha funzionato; le directory erano prive di file e anche di file nascosti, ma dopo anni di archiviazione disordinata dentro sistemi Unix praticamente nessuna di esse era considerata vuota. Al che, come ho scritto a **Mimmo** nel raccontargli tutto, *ho estratto la bomboletta di Raid* e le ho obliterate con `rm -rf [a-z][a-z][a-z]-[a-z][a-z][a-z]` che ha superato ogni problema.

Tutto questo perché anche sulle cose piccole c’è sempre da imparare; perché il Terminale è uno strumento spettacolare; perché queste sono appunto cose piccole, piccolissime; uno sviluppatore serio probabilmente sta ridendo dall’inizio del post. Vuol dire che sono cose difficili per le persone normali, non *così* difficili però, e vale la pena di apprendere e impegnarsi per risolvere problemi che il Finder non contempla… oltre che per il gusto, chiaro.

Sono partito da centinaia di sottodirectory (un curioso potrebbe anche calcolare quante, sapendo che l’ultimo prefisso applicato ai nomi file è stato *fip*) e adesso ho i miei tremilacinquecentovirgola file tutti insieme allo stesso livello gerarchico.

Ci sono altri passaggi da compiere e ci sarà da divertirsi. Bomboletta simbolica di Raid alla mano.