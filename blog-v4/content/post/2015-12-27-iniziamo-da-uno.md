---
title: "Iniziamo da uno"
date: 2015-12-27
comments: true
tags: [OpenEmu, ColecoVision, Intellivision]
---
Premetto che forse il mio entusiasmo è mal riposto per mancanza di informazione aggiornata sul tema.

Tuttavia ho sempre avvertito la mancanza di un emulatore di PlayStation 1 per Mac, in un panorama dove l’offerta generale è ampia.<!--more-->

Finalmente, per quanto ne so, [OpenEmu](http://openemu.org) colma la lacuna con software *open source*, scritto apposta per OS X e il supporto ulteriore di un gran numero di altre macchine interessanti per il cultore dei retrovideogiochi, tipo ColecoVision e Intellivision.

Attendo volentieri correzioni e intanto scarico con gusto.