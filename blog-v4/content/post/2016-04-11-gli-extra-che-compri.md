---
title: "Gli extra che compri"
date: 2016-04-11
comments: true
tags: [Pangea, Spiderweb, Greenstone]
---
Non è più notizia la [messa in vendita di Pangea Software](http://pangeasoft.net/forum/index.php/topic,2816.0.html) da parte del fondatore Brian Greenstone, che preferisce passare il resto della vita assieme alla moglie a sviluppare la raccolta e lo scambio di minerali e fossili dopo quasi trent’anni e altrettanti giochi per Mac e iOS, mentre dovrebbero esserlo certe sue dichiarazioni.<!--more-->

>Sono estremamente grato ad Apple e ai nostri fan che ci hanno supportato negli anni, i quali non ho alcuna intenzione di abbandonare. Continuerà a supportare e aggiornare i giochi esistenti, sistemare bug e migliorare funzionalità qui e là ove il tempo me lo permette […] Non venderò Pangea al miglior offerente. Sono più interessato a trovare qualcuno che tratti l’azienda nel modo giusto e la migliori come mai prima […] Pangea gode tuttora di un discreto flusso di cassa e un sacco di patrimonio in proprietà intellettuale, per non parlare di una rete di contatti che molti sviluppatori si taglierebbero una gamba per poter avere.

Non c’è motivo di dubitare delle dichiarazioni di Greenstone, che oltretutto sottolinea come i minerali siano al momento attività in perdita. Guadagnerebbe di più (anzi: guadagnerebbe) continuando con Pangea.

Si rilegga la citazione. È l’elenco di una serie di extra che lascio elencare a ciascuno e abbiamo comprato senza saperlo, al momento di scucire qualche euro per un gioco [Pangea](http://www.pangeasoft.net/index.html). Ne ho diversi e ho sempre trovato il rapporto qualità/prezzo assolutamente appropriato; ancora oggi sono godibili senza essere all’avanguardia in alcunché se non nelle idee e nella cura della realizzazione, un po’ come per i migliori lavori di [Spiderweb](http://www.spiderwebsoftware.com).

Non è facile vedere la vendita di una casa di produzione procedere in modo altrettanto leggero, garbato, staccato dal profitto a ogni costo. E sarà un caso, una coincidenza, uno scherzo del destino, ma i giochi scritti da Pangea sono tutti per computer che notoriamente si comprano per ragioni che vanno oltre il mero prezzo.

(Da uno che si chiama Pietraverde, peraltro, i minerali prima o poi te li aspetti.)