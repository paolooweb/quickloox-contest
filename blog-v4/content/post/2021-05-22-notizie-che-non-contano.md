---
title: "Notizie che (non) contano"
date: 2021-05-22T01:41:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [iPad, MacRumors] 
---
Apprendo da *MacRumors* che iPad di seconda generazione è stato infine [dichiarato obsoleto da Apple in tutto il mondo](https://www.macrumors.com/2021/05/21/ipad-2-considered-obsolete-worldwide/).

Per Apple sono obsoleti gli apparecchi la cui produzione è terminata da sette anni, solo che ci sono nazioni con leggi che la costringono ad allungare i tempi.

Ora, tuttavia, i tempi in questione sono scaduti veramente ovunque.

iPad 2 è stato presentato a marzo 2011 e, in quanto obsoleto, Apple non fornirà più alcun tipo di assistenza hardware.

Ho un iPad di prima generazione a casa, che usano generalmente le figlie per giocare; ma mi è capitato occasionalmente di appoggiarmici brevemente per situazioni particolari. iCloud non è più accessibile, su App Store è difficile trovare ancora giochi che possano esservi installati; certamente non è più una macchina moderna.

Tuttavia continua a ben figurare nella rete di casa; si collega al Wi-Fi senza problemi, tramite Dropbox o al limite perfino la email dispongo di una limitata ma sicura capacità di interscambio con gli altri apparecchi, i software che potrebbero essere utili di sicuro non ricevono aggiornamenti da tempo ma perbacco, *funzionano*.

Undici anni dopo – mi arrivò a maggio 2010 – è un iPad che non potrei mai usare regolarmente per lavoro e però funziona benissimo per missioni occasionali. La scocca ha preso un paio di botte sopravvivendo degnamente e interamente; la batteria è ineccepibile. Lo schermo è intatto e il touch funziona a perfezione. Non ha mai ricevuto cure particolari o attenzioni specifiche. Continua a dire la sua.

Obsoleto fa notizia, funzionante no. Funzionante conta, obsoleto no. Se domani il mio iPad vecchio di undici anni non si accendesse più, dire pazienza, ho riavuto in valore non so quante volte quello che ho speso in soldi. Lo porterei dal cinese di turno, se il prezzo è buono, perché provare costa poco. Con il cuore leggero e il cervello soddisfatto di un investimento molto più che azzeccato.

*Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile.*               