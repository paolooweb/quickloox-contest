---
title: "Non sempre si può crowdvincere"
date: 2022-01-25T00:26:20+01:00
draft: false
toc: false
comments: true
categories:
tags:
---
Una agenza di consulenza open source ha sottoposto alla propria comunità un elenco di progetti possibili su cui lavorare, pronti a realizzare quello che avrebbe raccolto più donazioni.

Nell’elenco ha prevalso la pseudoclasse Css *:focus-visible* per WebKit (in pratica, per Safari).

Così l’agenzia ci ha lavorato e ha proposto la miglioria al gruppo di lavoro WebKit. Il quale ha collaborato con l’agenzia per mettere a posto qualche problema e ha infine pubblicato la modifica nella più recente versione di [Safari Technology Preview](https://developer.apple.com/safari/technology-preview/), una versione più avanti di quella ufficiale usata per anticipare nuove funzioni, fare test, lavorare al futuro prossimo del browser.

In quel preciso istante si sono aperte le gabbie del web e un torrente di critiche si è rovesciato… su Apple, colpevole di avere delegato una pseudoclasse di Css a un crowdfunding, per risparmiare, per sfruttare, per giocare sporco e chissà che altro.

Come si può leggere in questa spiegazione ordinata e pacata della situazione, [Apple non ha fatto crowdfunding per avere :focus-visible in Safari](https://meyerweb.com/eric/thoughts/2022/01/24/no-apple-did-not-crowdfund-focus-visible-in-safari/). Non ha chiesto niente e, se ha avuto un ruolo, è stato alla fine, nell’accettare una modifica che è arrivata spontaneamente da una iniziativa indipendente, non richiesta, non sollecitata, non sovvenzionata, niente.

Se il tema fosse Apple cattiva contro Apple buona, questo post sarebbe davvero patetico. Il tema è invece il recupero della rilevanza delle iniziative indipendenti nella gestione della rete e delle applicazioni. È l’importanza della [Open Prioritization](https://opencollective.com/open-prioritization), l’idea che i componenti da aggiungere ai software di uso comune dovrebbero avere priorità di sviluppo dettate da persone libere da legami con aziende dotate, diciamo, di grande influenza sul mercato.

Chi ha protestato per il supposto crowdfunding di Apple non ha capito l’importanza del momento: un browser palesemente pilotato da una multinazionale che accetta una modifica proposta da gente qualsiasi su Internet, al netto ovviamente delle competenze tecniche.

Il problema maggiore della rete oggi è l’intenzione costante e onnipresente dei grandi soggetti aziendali di controllarne crescita, direzione, sviluppo. A decidere dove va e cosa deve fare Safari è al novantanove percento Apple; se questa percentuale scendesse a favore degli sviluppatori indipendenti, soli o in gruppo, ne trarremmo un grande vantaggio tutti.

Così si conclude la sintesi dell’articolo:

> L’aggiunta di :focus-visible a WebKit è stata condotta dalla comunità, effettuata da [Igalia](https://www.igalia.com) e sottoposta a WebKit senza alcun coinvolgimento da parte di Apple se non per visionare l’aggiunta e accettare il contributo. Molti di noi sono arrabbiati con Apple per un mucchio di ottime ragioni, ma non bisogna lasciare che dare sfogo alla rabbia sporchi gli obiettivi e i traguardi raggiunti da Open Prioritization. La prossima priorità di sviluppo per un browser che tuteli, potrebbe essere la tua.

Utopia, certo, che io possa pensare a una funzione utile per me e cambiare Safari in modo che la adotti.

È esattamente questo il punto. Non deve essere una utopia e la vicenda di *:focus-visibile* dimostra che non è impossibile.

_Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile._
