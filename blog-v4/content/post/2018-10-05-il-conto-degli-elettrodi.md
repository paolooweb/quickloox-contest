---
title: "Il conto degli elettrodi"
date: 2018-10-05
comments: true
tags: [Ecg, watch, AliveCor]
---
Belle le polemiche a seguito della presentazione di [watch Series 4](https://www.apple.com/it/apple-watch-series-4/) con la funzione di elettrocardiogramma, appunto di tracciato del battito cardiaco per via elettrica.

[Non è piaciuto](https://qz.com/1389202/the-apple-watch-series-4-health-features-arent-all-that-impressive/) che Apple abbia chiaramente dichiarato fin dove arrivi il singolo elettrodo di watch: consente di individuare sintomi di fibrillazione atriale e solo quello. *Solo* è un eufemismo perché la fibrillazione atriale è pericolosetta e spesso inavvertita; rilevarla salva delle vite. Rilevarla bene salva dalle angosce inutili e watch è efficace nel 98 percento dei casi. Non è male.

Certo, un elettrocardiogramma ospedaliero prevede dodici elettrodi sparsi sul corpo invece che uno singolo sul polso e riesce a ottenere una messe di dati molto più significativa. Per [dirla con 9to5Mac](https://9to5mac.com/2018/09/14/apple-watch-series-4-ecg-accuracy/):

>Accipicchia. Un apparecchio di elettronica di consumo da quattrocento dollari non sostituisce apparecchiature ospedaliere. Ma chi lo avrebbe mai pensato?

Fare questioni sull’elettrodo è come farle sul dito, che indica la Luna, solo che si guarda il dito a spese di ciò che conta davvero.

Un indizio su che cosa conti davvero lo fornisce [AliveCor](https://techcrunch.com/2018/09/17/not-to-be-overshadowed-by-the-apple-watch-alivecor-announces-a-new-6-lead-ecg-reader/): un elettrocardiografo personale con sei elettrodi, capace di individuare oltre cento diversi problemi cardiaci. Anche questo un prodotto di elettronica di consumo, per quanto da tavolo più che da polso, probabilmente in vendita dal 2019 una volta arrivate le autorizzazioni. Sì, perché tanto AliveCor che watch effettuano le loro misurazioni solo dove ottengono l’approvazione della sanità locale.

Sta cominciando una rivoluzione silenziosa, che passa dai cuori delle persone. Niente di idealistico, però: solo salute a casa propria a prezzo accessibile.

Per chi non capisse: si ricorda quando i telefonini telefonavano? E uscì il computer da tasca, senza app, con connessione risibile, che telefonava pure peggio dei telefonini? Cambiò qualcosina. Chi fa il conto degli elettrodi, oggi, fa riecheggiare le risate di quello per cui iPhone non avrebbe venduto alcunché, con quel prezzo, senza neanche una tastiera fisica. Neanche lo linko, oramai è mitologia, più che notizia.
