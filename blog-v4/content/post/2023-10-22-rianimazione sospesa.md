---
title: "Rianimazione sospesa"
date: 2023-10-22T00:15:45+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [Utility Disco, Disk Utility]
---
Le app sotto Sonoma appena installato si comportavano in modo strano ed erano anni, letteralmente, che non verificavo il disco di avvio di Mac mini.

Messe insieme le due nozioni, ho lanciato Utility Disco (non dalla partizione di recupero, ma da quella di lavoro, quindi con le altre app aperte) e ho ricevuto un responso preciso: era tutto riparabile, ma c’erano *tante* cose da riparare.

Ho avviato la riparazione e, nel giro di pochi secondi, lo schermo si è bloccato. Niente rispondeva.

Dieci minuti dopo, uguale. Venti minuti dopo, come sopra. Trenta minuti dopo non sapevo che cosa fare.

Ho scelto la fiducia e ho proseguito a lavorare su iPad.

Più tardi, di sicuro almeno quaranta minuti essere iniziata, la riparazione era terminata con successo. Le app hanno ripreso a comportarsi a dovere e tutto era regolare.

Lo dico per avvisare; se Utility Disco congela il Mac, potrebbe essere tutto a posto e potrebbe passare molto tempo prima di avere un riscontro dall’interfaccia. Non è il momento per essere impazienti.