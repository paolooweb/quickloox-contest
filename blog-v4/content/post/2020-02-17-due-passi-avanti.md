---
title: "Due passi avanti"
date: 2020-02-17
comments: true
tags: [Cook, Apple, Mac, iPad, iPhone, watch, Notes, Note, Mdj]
---
Un dato relativamente poco pubblicizzato dagli ultimi risultati finanziari di Apple, come riporta [Mdj](https://mdj.substack.com/p/mdj-20200129):

>”La nostra base installata attiva [ha raggiunto] un nuovo record assoluto per ciascuna categoria di prodotto principale e segmento geografico”. Prenditi un momento per assimilare questa affermazione. In nessun momento della storia di Apple si sono avute più persone che usano iPhone, iPad, watch, AirPod… o Mac.

L’affermazione è straordinaria perché, neanche Mdj lo sottolinea, vale anche per aree geografiche. In nessun momento Mac ha avuto così tante persone a utilizzarlo in America. In Europa. In Asia. In Giappone. Ovunque. E vale per ogni altro prodotto principale.

Si possono fare molti pensieri su questo dato, dalle quote di mercato ai livelli di soddisfazione, e certamente rimane una affermazione generica; se mi trovo male con Catalina, mi importa poco essere parte di una comunità che non era mai stata così grande.

Nondimeno, il bicchiere lo classificherei mezzo pieno.

Ho collegato questo pensiero a una microepifania occorsami l’altro giorno al supermercato. Avevo la lista della spesa su iPhone, nelle Note, stile casalingo sprovveduto.

Nello spuntare il primo prodotto, è apparso un messaggio della app: volevo che le voci spuntate andassero automaticamente in coda alla lista?

Certo. È una sciocchezza, una cosa piccola, marginale. E comoda. L’ultima volta che mi è capitato, Note non lo faceva.

È stato un promemoria del fatto che il sistema operativo del mio iPhone continua a evolvere anche in assenza di macroaggiornamenti. Nonché una piacevole sorpresa, che mi ha fatto pensare ancora una volta a quanto dovrei, potrei ma ancora non so spremere da iPhone, da iPad, da Mac.

Un altro collegamento mentale, stavolta con varie operazioni nostalgia apparse negli ultimi tempi in tema di vecchia Apple, retrocomputing, archeologie informatiche assortite.

È una cosa che mi piace tantissimo. Ho iniziato ad accarezzare il progetto di dare un nuovo seguito a *Macintosh Story*. Sempre disponibile a compiere in via straordinaria un passo indietro nel passato a riscoprire la magia della tecnologia che ci ha appassionato e comunque accompagnato nel nostro crescere.

Purché straordinario. A parte l’occasione eccezionale, nella vita tecnologica preferisco altrimenti, sempre, comunque, fare due passi avanti.