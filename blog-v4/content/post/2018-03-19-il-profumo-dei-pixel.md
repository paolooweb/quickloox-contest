---
title: "Il profumo dei pixel"
date: 2018-03-19
comments: true
tags: [iPad, Kindle]
---
Invece di un codardo confronto con Kindle, Charles Maurer osa titolare su *Tidbits* [Meglio che la pagina stampata: leggere su iPad](http://tidbits.com/article/17850).<!--more-->

Maurer scrive con il supporto della moglie, una *prominent visual scientist* che deve avere una certa precisione in testa sul tema.

L’articolo è un viaggio attraverso le tante configurazioni che è possibile eseguire per rendere iPad l’oggetto ideale per leggere persino più del libro, persino più della carta che profuma.

La luminosità? Conta, sicuro, ma non per prima e neanche per seconda. Molte persone leggono meglio quando la colorazione dello schermo presenta un leggere viraggio, unico per ciascuno di noi. Se si usa iPad per leggere seriamente, vale la pena di provare. Sì, si può virare la colorazione dello schermo.

Poi si parla di font ovviamente, si parla di testo scuro su fondo chiaro, le ovvietà classiche. Ma l’insieme mostra una conoscenza concreta dell’argomento e rsppresenta uno dei pochissimi articoli per i quali appresto un segnaposto su Safari.

Il profumo della carta? Certo, ma quello dei pixel la mattina presto…