---
title: "Piccolo è bello"
date: 2015-03-19
comments: true
tags: [watch]
---
Per la serie *dettagli su cui non si fa abbastanza mente locale*, se c’è una differenza significativa tra watch e la pletora di orologi più o meno smart già in giro sono le *dimensioni*.<!--more-->

[Guardare per credere](http://9to5mac.com/2015/03/12/look-how-much-sleeker-apple-watch-is-compared-to-android-wear-watches/).

Quei millimetri di differenza, in una equivalenza che non tutti comprendono, arrivano da chilometri di test, tonnellate di ricerca, megawatt di tecnologia. Apple può sbagliare, ma comunque si impegna. Molti altri fanno quello che è comune e ovvio, e si fermano lì. Si vede bello grosso che cosa vuol dire.