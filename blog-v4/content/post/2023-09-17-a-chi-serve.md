---
title: "A chi serve"
date: 2023-09-17T14:08:48+01:00
draft: false
toc: false
comments: true
categories: [Hardware]
tags: [Pixel Tablet]
---
Una delle cose importanti imparate durante l’asilo della primogenita l’ho sentita durante un incontro collettivo con una psicologa, che al momento di discutere qualsiasi cosa, dal metodo educativo all’acquisto dei giocattoli al modo migliore di mettere i cuccioli a dormire, invitata a chiedersi sistematicamente *a chi serve?*

Nel senso che certi giocattoli non sono per i bambini ma per compiacere uno o più genitori, così come le scelte di sport e attività compiute dai genitori per compensare mancanze proprie eccetera.

Mi avrebbe aiutato molto quando facevo l’esperienza della vendita al dettaglio di Mac. Il papà che per avere un computer più grande si faceva scudo verso la moglie con il motivo degli studi della figlia, o l’idea di volere la qualità e un buon prezzo che in realtà significava *il prezzo più basso possibile basta che mi lusinghi abbastanza così che possa giustificare a me stesso la mia spesa*. Chiedersi a chi serve aiuta tantissimo a penetrare l’apparenza.

Vale anche per giudicare un prodotto, per esempio il Pixel Tablet [recensito su Ars Technica](https://arstechnica.com/gadgets/2023/09/the-pixel-tablet-is-actually-just-a-few-spare-parts-in-a-half-empty-body/). Il titolo prospetta una situazione interessante:

>Im realtà il Pixel Tablet è [fatto di] poche parti sparse in una scocca piena a metà - perché questo tablet da 500 dollari è così pieno di spazio vuoto?

Perché sembra un problema? Perché lo è. L’articolista scrive che *l’interno di questo tablet somiglia a una cosa costruita in garage da qualcuno con una stampante 3D*. Senza scomodare Apple, l’interno vuoto di un tablet deve preoccupare. Significa meno batterie di quanto sarebbe stato possibile, una progettazione come minimo approssimativa. *Non ho mai visto un apparecchio mobile realizzato così da una grande azienda, specialmente un apparecchio da cinquecento dollari*, va avanti l’autore.

È successo che il Pixel Tablet deve essere nato inizialmente come uno smart display Google, categoria che poi è stata eliminata per varie problematiche, che Google stessa *ha cercato di mettere in vendita in qualunque stato si trovasse*.

Tranne che per una riprogettazione successiva – il Pixel Tablet ha uno schermo da undici pollici e la componentistica fa pensare di essere stata assemblata per qualcosa di più piccolo – di questo si tratta: uno smart display che è stato abbandonato a metà strada. Qualcuno ha convocato una riunione e ha chiesto che cosa fare di tutti i componenti acquistati allo scopo. Qualcuno ha detto, facciamone un tablet e così è andata.

Non ho dubbi che ci sarà qualcuno ad apprezzare il Pixel Tablet per le sue caratteristiche, il prezzo e come risponde alle proprie necessità.

Io guardo un apparecchio rimescolato insieme per farne qualcos’altro in seconda battuta e mi chiedo, a chi serve?