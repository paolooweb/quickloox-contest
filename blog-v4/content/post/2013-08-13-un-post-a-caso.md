---
title: "Un post a caso"
date: 2013-08-13
comments: true
tags: [OSX, iOS, Unix]
---
[Stimolato da **briand06**](https://moot.it/quickloox#!/blog/blog/2013/07/31), ho cominciato ad approfondire i meccanismi di creazione di numeri casuali da parte di OS X.<!--more-->

Riassumo brevemente: ci sono situazioni in cui è desiderabile generare numeri casuali. Per lanciare dadi ma anche fare buona crittografia, accumulare basi di dati su cui compiere sperimentazioni statistiche e molte altre cose. In Mavericks, Safari suggerisce *password* particolarmente sicure di cui si prenderà cura iCloud e tutti hanno interesse a che quelle *password* siano realmente casuali, con una misura di casualità davvero forte.

I computer non sanno generare da soli numeri casuali, ma *pseudorandom*: casuali per una quantità sufficiente di tempo a soddisfare la gran parte dei bisogni e poi però condannati a ripetersi. L’efficienza dei numeri *pseudorandom* è molto maggiore se c’è un buon *seed*, seme, ossia se la sequenza pseudocasuale parte ogni volta da una posizione il più possibile imprevedibile.

OS X sfrutta due funzioni comunemente usate in tutta l’informatica: `random` e [arc4random](https://developer.apple.com/library/mac/documentation/Darwin/Reference/ManPages/man3/arc4random.3.html#//apple_ref/doc/man/3/arc4random). Su iOS esiste una funzione [SecRandomCopyBytes](https://developer.apple.com/library/ios/DOCUMENTATION/Security/Reference/RandomizationReference/Reference/reference.html) che serve a ricevere numeri casuali dal sistema e si appoggia a `random`, anche chiamata `urandom` su Linux.

Queste funzioni sono *pseudorandom* su distanze piuttosto grandi: `arc4random` crea numeri lunghi fino a 4.294.967.295, `random` fino a 2.147.483.647 e inizia a ripetersi dopo circa 34 miliardi di estrazioni, esattamente 34.359.738.352.

Il punto è il *seed*, l’imprevedibilità continua di inizio della sequenza: per assicurarla, è comune il reperimento di informazioni casuali da usare come base per gli algoritmi. Esistono organizzazioni come [Random.org](http://www.random.org) che per mestiere producono numeri casuali basati sulle variazioni del rumore atmosferico di fondo; altri usano il decadimento di elementi radioattivi, misurazioni delle variazioni di potenza di un raggio di luce, rumore visivo generato da fotocamere digitali che fotografano con sopra il copriobiettivo, osservazione delle lampade psichedeliche (le *lava lamp*) di moda negli anni sessanta e così via.

OS X e iOS lavorano, come minimo, raccogliendo fluttuazioni nel ritmo di lavoro del *kernel* (il cuore del sistema operativo) e affidandosi in sovrappiù a un algoritmo chiamato [Yarrow](http://www.schneier.com/yarrow.html), per cui il sistema è sempre in grado di fornire numeri casuali a richiesta in quantità ragionevole. Un software come TrueCrypt [aggiunge a tutto](http://www.truecrypt.org/docs/random-number-generator) questo la raccolta di dati casuali derivata dalla rilevazione dell’uso della tastiera e dei movimenti del mouse.

Esistono sistemi per mettere alla prova i meccanismi di generazione di numeri casuali, per esempio [Dieharder](http://www.phy.duke.edu/~rgb/General/dieharder.php).

Dopo tanta teoria, un pizzico di pratica: su *commandlinefu.com* compare un [comando di Terminale](http://www.commandlinefu.com/commands/view/7949/generate-random-password-works-on-mac-os-x) per generare *password* molto casuali e sette alternative possibili. commandlinefu.com contiene quasi undicimila comandi di Terminale ed è una risorsa curiosa e interessante, casualità a parte. Anche a dire che per un programmatore non è particolarmente complicato accontentarsi delle risorse di sistema per estrarre numeri a caso: può contare su strumenti più che all’altezza.

Sto viaggiando sul filo del rasoio con le mie capacità di comprensione tecnica e quindi accetto volentieri correzioni e affinamenti.