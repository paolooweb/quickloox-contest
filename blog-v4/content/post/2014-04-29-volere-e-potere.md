---
title: "Volere e potere"
date: 2014-05-01
comments: true
tags: [Viticci, Wwdc, Apple, iOS8, TechRadar]
---
Federico Viticci ha scritto un pezzo dei migliori dedicato ai suoi [desideri in vista di iOS 8](http://www.macstories.net/stories/ios-8-wishes/), che presumibilmente verrà presentato al mondo durante la [World Wide Developer Conference](https://developer.apple.com/wwdc/), il convegno mondiale degli sviluppatori Apple, di inizio giugno.<!--more-->

Me lo ha segnalato **Stefano** aggiungendo una considerazione:

>Salvare una mail importante in reminder sarebbe fantastico.

Sono d’accordo. Il gioco di quello che potrebbe essere, però mi piace meno dello spremere all’ultima goccia quello che già c’è.

Per mille ragioni e ne cito una sola: avendo a disposizione per una settimana il *team* di sviluppo Apple, su che cosa lo metterei? sarebbe più importante lavorare su ricezione *wireless*, comunicazione tra *app* oppure notifiche? O iWork? O l’integrazione tra Mail e Calendario? Eccetera.

Il problema non è quali funzioni desiderare. Le desidero tutte. Il problema è la priorità, ossia a che cosa dedicare tempo e risorse che persino a Cupertino sussistono in quantità limitata.

Ragionare sul prossimo sistema operativo è esercizio estremamente utile che può essere condotto in maniera esaustiva come ha fatto  [@viticci](https://twitter.com/viticci) e ponendosi domande precise come Stefano.

Se ci dimentichiamo delle priorità, tuttavia, si finisce come *TechRadar* con il suo video *concept* su iOS 8, che non è più possibile guardare per problemi di copyright degli autori. Un bel viaggio di fantasia a mostrare interfacce da sogno, basato sul nulla pratico, pensato a prescindere da ciò che è fattibile e ciò che è auspicabile, giustificato unicamente dall’esistenza di strumenti abbastanza raffinati da farlo sembrare vero. In altri tempi sarebbe stato tacciato di autoerotismo.