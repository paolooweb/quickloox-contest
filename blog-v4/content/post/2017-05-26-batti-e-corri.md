---
title: "Batti e corri"
date: 2017-05-26
comments: true
tags: [watch, Samsung, Stanford]
---
L’università di Stanford [ha messo alla prova](http://www.imore.com/stanford-2017-study-apple-watch-tops-heart-rate-faq) gli aggeggi da polso sulla loro precisione di rilevazione di frequenza cardiaca e consumo calorico.

Gli apparecchi sono stati messi a confronto con apparati e procedure standard usate comunemente in medicina, come l’elettrocardiogramma, per ottenere i risultati più precisi oggi a disposizione della scienza medica.

Gli oggetti da indossare sono molto precisi sul battito cardiaco, con margini di errore ben sotto il dieci percento. Il migliore di tutti è watch, che sta al due percento (se dice ottanta battiti potrebbero essere persino settantotto oppure ottantadue, ma siamo lì). Il peggiore di tutti è marcato Samsung e, se dice ottanta battiti, potrebbero essere settantaquattro come ottantasei. Non sembra grave; non mi fiderei comunque.

Non ci si può fidare di nessuno invece sul consumo calorie. Tutti gli apparecchi stimano con approssimazioni pazzesche, anche del novanta percento. Ti dice trecento calorie? Potrebbero essere trenta e scusa se fa un po’ specie.

Il dato delle calorie va preso con beneficio ampio di inventario rispetto alla cifra assoluta. In relativo è invece importante che, se proprio ci deve essere errore, almeno sia costante. Sotto questo aspetto watch è rimarchevole, sballato come gli altri, ma sempre sballato allo stesso modo. Cosa che a non tutti gli altri accade.

Finisce sempre che il computer da polso di Apple sta davanti al gruppo, persino quando si parla di prestazioni ancora molto migliorabili. Gli altri, certo, costano meno. Ma fanno ugualmente tutto quello che fa watch, eh. Se poi si passano al cardiologo dati approssimati, che vuoi che sia.
