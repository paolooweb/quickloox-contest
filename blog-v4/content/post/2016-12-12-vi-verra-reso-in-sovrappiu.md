---
title: "Vi verrà reso in sovrappiù"
date: 2016-12-12
comments: true
tags: [Savi, Teoria, Tutto, Buck]
---
Ricevo da [Daniele](http://www.danielesavi.info/it) e pubblico.

**Tra qualche giorno** lancerò una nuova iniziativa legata a [La Teoria del Tutto](http://scrittorideltutto.it), ma volevo segnalartela brevemente in anteprima.<!--more-->

Il 24 Agosto 2016, come sai, un violento sisma ha colpito le zone centrali del nostro Paese, con una replica ancora più violenta pochi mesi dopo.

Proprio in quei giorni, dall’idea di un gruppo di autori tra cui il sottoscritto, che ha avuto l’onore di partecipare, è nata una raccolta di racconti il cui filo conduttore è la speranza, e che cercherà di donare speranza perché l'intero ricavato delle vendite è devoluto alla Croce Rossa Italiana. La raccolta si chiama [Buck e il Terremoto](http://www.buckeilterremoto.com).

Perché te ne parlo? Perché ho deciso di legare strettamente la campagna su Bookabook per il mio romanzo a questa splendida iniziativa. Infatti, se riuscirai a convincere almeno tre amici a preordinare una copia de *La Teoria del Tutto*, o acquisterai tre copie da donare ad altrettanti amici, io devolverò le *royalty* che mi spetterebbero da Bookabook proprio in riconoscimento di questo impegno e ti regalerò una copia di *Buck e il Terremoto*.

Trovare tre nuovi sostenitori tra i tuoi amici, lo so, è una piccola sfida. **Ora che l’obiettivo è duplice, non ne vale la pena? :)**

Penso alle persone che potrebbero avere bisogno in questi giorni e sono prive di troppe cose. Non è il caso di porsi domande, semmai di agire se uno sente che è la cosa giusta.
