---
title: "La postpotenza delle lobby"
date: 2020-12-10
comments: true
tags: [CA, Notify, Apple, Google, Immuni, Covid-19, coronavirus]
---
Ogni tanto emerge in Europa un dibattito sullo strapotere delle multinazionali e di come esse condizionino governi ed enti pubblici grazie al loro potere di pressione.

Poi leggo che lo Stato della California lancia la sua app di tracciamento contatti per il contrasto alla pandemia, app che si aggancia al framework messo a punto congiuntamente da Apple e Google e usato, per esempio, nella nostra [Immuni](https://www.immuni.italia.it), l’App Che Stranamente Non Ha Risolto La Crisi Coronavirus Da Sola.

[L’ha lanciata il 7 dicembre](https://www.gov.ca.gov/2020/12/07/governor-newsom-announces-statewide-expansion-of-ca-notify-a-smart-phone-tool-designed-to-slow-the-spread-of-covid-19/). *Otto mesi* dopo il framework.

L’orientamento politico della California è storicamente lontanissimo da negazionismi o populismi vari.

Contemporaneamente, in California si trova la Silicon Valley. Se Apple o Google sono centri di potere o di pressione, lì la esercitano al massimo grado. Se non altro perché si trovano al centro delle polemiche sulla tassazione a livello globale. Ma a livello locale, quello che pagano lo pagano alla California e al governo americano. Cioè hanno un profilo di contribuenti vicino a quello di un semidio.

Qualcuno ricorderà Steve Jobs [presentare il progetto di Apple Park al consiglio comunale di Cupertino](https://www.youtube.com/watch?v=gtuz5OmOh_M). Con un triste senno di poi si vede un uomo smunto e indebolito, con quattro mesi di vita davanti, tuttavia capace di usare la massima cortesia e *savoir faire* per dire, in modo molto mediato e inconfondibile, *noi paghiamo una montagna di tasse alla municipalità e se non ci date il permesso di costruire il nuovo campus ce ne andiamo a farlo da qualche altra parte*. Negare che una Apple abbia un enorme potere di persuasione, come tutte le altre multinazionali, è impossibile.

Ciononostante, a casa loro, il governo statale ragiona per otto mesi (dopo avere condotto un programma pilota presso alcune università) prima di adottare un pezzo di software su cui c’è molta attenzione, politica oltre che di altro tipo.

Sono così prepotenti e aggressive, queste lobby? O dipende anche dal terreno più o meno favorevole che trovano?

<iframe width="560" height="315" src="https://www.youtube.com/embed/gtuz5OmOh_M" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>