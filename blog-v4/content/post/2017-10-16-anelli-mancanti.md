---
title: "Anelli mancanti"
date: 2017-10-16
comments: true
tags: [BBEdit, Drang, Wordpress]
---
Il mitico Dr. Drang liquidò anni fa alla sua maniera il problema di dare in pasto a Wordpress file Markdown prodotti con BBEdit: [una serie di script](https://github.com/drdrang/wp-md) che facevano da altrettanti anelli mancanti per completare la catena.

Se siano tutti, se siano rimasti validi nel tempo, se la strada da seguire sia diversa dopo questi anni sono cose che auspico di riuscire a dirimere almeno in parte nelle prossime settimane.