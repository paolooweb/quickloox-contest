---
title: "Di un altro tempo"
date: 2017-10-01
comments: true
tags: [iPhone, X, Cupertino, Viscardi, YouTube, keynote]
---
Con gli amici è persino doveroso discutere a volte, ma litigare mai: al massimo si ammette la correzione fraterna.

Per questo nutro tenerezza nei confronti di qualche amico che, in corrispondenza di un annuncio Apple, arriva il giorno dopo a dire che è rimasto deluso, e se ci fosse ancora Lui, non è più come una volta, in molti la pensano come me eccetera eccetera. Il punto è di volta in volta il prezzo di MacBook Pro che non è quello che voleva lui, oppure l’assenza di innovazione, oppure il mancato annuncio di iPhone durante la presentazione dei Mac e viceversa e via discorrendo.

Immancabilmente è un amico che ha vissuto l’età d’oro dell’informatica personale e ha partecipato con entusiasmo e ardore alla comunità di allora.

Tanto che crede di essere ancora lì, in quel tempo.

Siamo in un altro tempo. A dimostrazione: per l’annuncio di iPhone X, [Apple ha invitato a Cupertino Sofia Viscardi](https://www.youtube.com/watch?v=QYF5b_OiOMw).

Sofia Viscardi è (anagraficamente ancora per un po’) un’adolescente milanese piuttosto seguita su YouTube. Il suo canale ha più di **settecentomila** iscritti.

Il video creato a seguito della sua visita ad Apple Park è stato guardato da oltre **duecentomila** persone.

È facile perdersi in qualche giudizio di merito su Sofia, che peraltro ha diciotto o diciannove anni e il diritto di scoprire il mondo come è doveroso fare alla sua età. Sarebbe guardare il dito e perdere di vista la luna.

La luna è che Apple *ha fatto bene* a portarsi Sofia a Cupertino.

La luna è che una decisione di questo tipo *è moderata*. Nella loro comunicazione i concorrenti, quelli seri, quelli professionali, quelli che costano sempre un euro meno, quelli che servono le aziende, quelli che ti lasciano usare la chiavetta e sono aperti, si lanciano in frequentazioni che a confronto Sofia Viscardi (senza offesa Sofia, è solo per spiegare) fa la figura del premio Nobel per la fisica.

Il panorama dell’informatica – che già come parola viene da un altro secolo e sarebbe tempo di accantonare – visto con gli occhiali degli anni novanta non è solo vecchio; è scomparso. Resta solo l’illusione che le riviste di settore fossero la popolarizzazione della tecnologia digitale e che il mondo comunicasse via email.

Errore. Le riviste di settore erano rivolte, comunque, a una élite per quanto allargata. La popolarizzazione della tecnologia digitale è questa, quando il computer sta in tutte le tasche e lo usano miliardi (miliardi) di individui. Che ovviamente si parlano per email quando proprio non ne possono fare a meno. Oggi la nuova frontiera del polveroso è Facebook e, se appena appena è rimasta un po’ di giovinezza interiore, ci si trova in altri consessi, non ultimo YouTube.

Ecco, amici, correzione fraterna. Certi giudizi non sono mai stati così irrilevanti, di molte lunghezze. Il guaio è che nemmeno c’entra Apple o l’ultimo iPhone; c’è proprio uno sfasamento rispetto al mondo effettivo. È inutile voler essere quello che non si è o fingere di non avere vissuto un’altra epoca; analogamente, è assurdo voler trasferire l’anacronismo su un’entità che con le persone, le tecnologie, le comunità e gli strumenti di oggi, ci ha a che fare quotidianamente e vende pure qualche miliardo (miliardo) di apparecchi. Potrà sparire domani, ma non è anacronistica. E prenderebbe il suo posto qualcosa di altrettanto attuale, non il Bel Tempo Andato.