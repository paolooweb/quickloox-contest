---
title: "Another one"
date: 2024-04-07T02:01:06+01:00
draft: false
toc: false
comments: true
categories: [Hardware, Software]
tags: [Scribus, InDesign, MacTeX, Mac Studio, M2 Max, Queen, Another one bites the dust]
---
Un buon sabato. Potrei essere riuscito a fare [mordere la polvere](https://www.youtube.com/watch?v=rY0WxgSXdEE) a una installazione di InDesign su un magnifico Mac Studio M2 Max nuovo di zecca.

Probabilmente si darà una possibilità a [Scribus](https://www.scribus.net). Pensavo che [ci fossero difficoltà sui nuovi Mac](https://macintelligence.org/post/2023-04-06-ricomincio-da-trentadue/) e invece proprio per niente, la versione stabile gira che è una bellezza, almeno a fare cose semplici.

E comunque ho installato [MacTeX](https://tug.org/mactex/), la cui edizione 2024 è uscita giusto tre settimane fa. Sono stato distratto e faccio ammenda.

Ci vuole ancora tempo per la sicurezza. Andare a dormire con l’ottimismo, tuttavia, non è male.

<iframe width="560" height="315" src="https://www.youtube.com/embed/rY0WxgSXdEE?si=kZ105KKF6zTpnJ7U" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>