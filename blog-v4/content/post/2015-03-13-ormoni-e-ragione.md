---
title: "Ormoni e ragione"
date: 2015-03-13
comments: true
tags: [Dediu, Asymco, Apple, iPhone, iPod]
---
[Articolo estremamente stimolante di Horace Dediu di Asymco](http://www.asymco.com/2015/03/11/conversations-with-apples-brand/). Negli anni ottanta la decisione di comprare un computer atteneva alla curiosità intellettuale e il messaggio di Apple su Mac come bicicletta per la mente era sostanzialmente razionale.<!--more-->

Da iPod in avanti la conversazione di Apple con il pubblico si è spostata, per dare maggiore priorità alle emozioni che all’intelletto. Musica, video. Il marchio, guarda caso, è diventato planetario e iconico.

Dopo iPhone, il marchio Apple tende sempre più a parlare all’istinto, prima che all’emozione o alla ragione. Qualcosa che non significa irrazionalità ma parla direttamente alle viscere senza transitare prima (magari lo fa dopo) per cuore e cervello.

La conclusione è che oggi il marchio Apple, partito, con un richiamo all’intelletto, scatena piuttosto una risposta endocrina.

E la meraviglia viene dal fatto che, durante questi spostamenti, nessun altro marchio ha saputo validamente sfidare Apple, né sul suo né su altri terreni.

>Apple può entrare nel mondo del lusso, mentre i marchi di lusso non riescono a esercitare un richiamo di tipo funzionale.

La ragione, secondo Dediu, è che l’azienda è strutturata per avere pochissimi prodotti che devono tutti avere grande successo, non tantissimi a coprire ogni francobollo del mercato. E quindi può esplorare più facilmente ambiti diversi da quelli dove è nata.

No, Apple non è un’azienda come le altre.