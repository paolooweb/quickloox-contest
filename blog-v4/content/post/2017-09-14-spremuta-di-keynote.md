---
title: "Spremuta di keynote"
date: 2017-09-14
comments: true
tags: [iPhone, keynote, Matteo, watch, tv, watch]
---
Mentre scrivo devo ancora guardare il *keynote* di presentazione dei nuovi iPhone, della nuova tv, del nuovo watch e del resto (si dirà che sono noiosi e ripetitivi e scontati, ma continuo a sorprendermi di quanti cicli di prodotto vengano completati di anno in anno).<!--more-->

Per fortuna mi ha soccorso **Matteo** che ha scritto di getto una email della quale lo ringrazio, riguardante il succo della presentazione. Segue qui sotto.

**Come i leak hanno (purtroppo) rivelato**, Apple ha presentato una nuova tv, un nuovo watch (che comunque da noi arriverà al momento privo della sua funzionalità più innovativo, la capacità di connettersi alla rete cellulare), e tre nuovi iPhone.

Al di là delle potenzialità di iPhone X, vorrei portare la tua attenzione sulla quantità di modelli presenti nello Store, ben otto, fra varianti Plus e SE (per non parlare dei colori).

Sicuramente ciò é dettato da un mercato talmente ampio che Apple non ha paura di cannibalizzare se stessa, anche se ho trovato la cosa singolare, soprattutto alla luce di quanto sostenuto da Steve Jobs (ma a proposito della varietà nei computer e di una vendita terribilmente inferiore rispetto a quella degli iPhone), a cui é stato intitolato il Theatre.

A tal proposito, che tu ci creda o meno, proprio nella prima parte del keynote, quella in cui veniva “inaugurata" la nuova struttura che ospiterà Apple, l’omaggio al suo fondatore (presente Wozniak in sala), trovo la misura della differenza fra un’azienda di quelle dimensioni e la concorrenza, ma anche la sua forza, perché proprio di iPhone X sarebbe sicuramente fiero Steve Jobs, confermando quanto affermato da Ive nel promo in cui sostiene che questo sia l’iPhone come l’avrebbero pensato da oltre un decennio.

Non ti nascondo che, comunque, proprio l’orologio con cellulare mi sembra il prodotto a me più utile ora, e **mi spiace che in Italia verrà commercializzato in data da destirnarsi**.

Commento solo che Steve Jobs aveva sì disegnato una matrice di prodotti essenziale, ma stava risollevando Apple da una brutta situazione. Nel frattempo anche il mercato è cambiato e presenta altre condizioni; le crescite vertiginose sono terminate per mancanza di sufficienti esseri umani a disposizione sul pianeta. I lmodo per crescere diviene sempre più rosicchiare clientela agli altri e l’unico modo per farlo, se non è riuscito finora, è diversificare. Otto modelli sono ancora tollerabili comunque, l’importante è che non diventino sedici.