---
title: "La vita segreta del software"
date: 2021-01-23T01:55:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [Beeper, emacs, Irc, Erc, Big Sur, Mission Control, macOS, Adium, Trillian, NovaChat, Stevie Wonder, Internet Relay Chat] 
---
Dentro Big Sur (forse anche in precedenza, non so), nella sezione di Mission Control dentro le Preferenze di Sistema si trova una casella per richiedere il riordinamento automatico degli spazi di scrivania a seconda di come procede l’uso del computer. Uso diversi spazi di scrivania dedicati a clienti, hobby, giochi e sono abituatissimo a distribuire pagine web e documenti in questo o quello spazio. In breve ho identificato i miei spazi preferiti, che hanno assunto posizioni mnemoniche. Ho provato a spuntare la casella e Mac ha cominciato a mischiarle a capriccio. A un certo punto non capivo più niente e mi stavo quasi preoccupando, prima di rendermi conto. La peggiore preferenza di sistema di cui abbia memoria.

Sta arrivando [Beeper](https://www.beeperhq.com), una app di chat che promette compatibilità praticamente con tutte le piattaforme più diffuse e anche quelle meno. Una rivisitazione di idee già viste in [Adium](https://adium.im) (ancora vive!) o [Trillian](https://trillian.im), in un progetto che era già noto come [NovaChat](https://nova.chat), ma su scala mai vista prima. Sarebbe un toccasana, solo che [richiede un certo sforzo di implementazione da parte dell’utilizzatore](https://techcrunch.com/2021/01/21/pebble-founder-launches-beeper-a-universal-chat-app-that-works-with-imessage-and-others/). L’ambizione tuttavia è notevole. Mi sono registrato per saperne di più appena possibile.

A questo proposito, ho scoperto che emacs contiene un client Internet Relay Chat (Irc): [Erc](https://www.gnu.org/software/emacs/manual/html_mono/erc.html). Mi hanno già fatto notare che sarebbe più veloce, a questo punto, elencare quello che *non* sta dentro emacs.

Mentre approfondisco la faccenda dei commenti per il blog, chi vuole lasciare comunque un commento da qui può accedere liberamente alla [pagina commenti di Muut per QuickLoox](https://muut.com/quickloox#!/feed). Non è ancora (ri)collegata a questi post (è lo scopo di tutto l’esercizio). Però lo sarà.