---
title: "Professionisti del"
date: 2015-05-20
comments: true
tags: [Windows, Microsoft, U2, Apple, Solitario, CandyCrushSaga]
---
Reduce da due *post* sui professionisti e le *app*, non potevo esimermi dal notare che Microsoft, a proposito di software professionale per professionisti, [organizza un torneo di Solitario](http://blogs.windows.com/bloggingwindows/2015/05/18/celebrating-microsoft-solitaire/) per festeggiare i venticinque anni del programma.<!--more-->

Il commento è fantastico nel sottolineare come Solitario sia stato utile a insegnare ai principianti l’uso elementare del mouse. È bello trovare sempre il lato positivo delle cose, e crederci pure.

Non è finita. I maghi del software professionale per antonomasia [installeranno automaticamente Candy Crush Saga](http://news.xbox.com/2015/05/games-candy-crush-saga-is-coming-to-windows-10) su tutti i sistemi Windows 10 per un certo tempo dopo il lancio del gioco per la piattaforma.

(Ricordiamo che Apple ha provocato un putiferio per avere installato [un album di musica](https://macintelligence.org/posts/2014-09-23-il-trionfo-della-volonta/) sugli apparecchi di quelli che lo volevano).

A un certo punto [Candy Crush Saga](http://candycrushsaga.com) non verrà più installato autoamticamente su Windows 10. E questo significa che Microsoft avrà deciso di lasciare al loro destino i professionisti del…