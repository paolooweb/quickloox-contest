---
title: "La vita segreta delle foto"
date: 2023-09-02T16:34:40+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [Turan, Harley Turan, sqlite3, exiftools, OSXPhotos]
---
>Secondo la app Foto di iOS, ho scattato settantatremiladuecentoottantuno fotografie negli ultimi quattordici anni in cui ho posseduto un iPhone.

L’autore di questa citazione, Harley Turan, inizia così [Exploring Exif](https://hturan.com/writing/exploring-exif), un pezzo pazzesco di esplorazione del mondo del metadati associati a ciascuna foto che scattiamo. Se fossimo su *National Geographic* ne verrebbe fuori un documentario dal titolo come *La vita segreta delle foto* o qualcosa del genere.

Già questo corrisponde a una professione o quasi. Poi Turan inizia a parlare di come accedere alla raccolta di foto e trovare esattamente quello che vuole, settantatremila e più scatti, *senza Foto*: invece di tenere le immagini nella app di Mac, usa Sqlite3 come database, installato via [Homebrew](https://brew.sh), e [ExifTool](https://exiftool.org) come programma di navigazione, abbondantemente linkati nell’articolo originale. Un altro programma citato è [OSXPhotos](https://github.com/RhetTbull/osxphotos).

L’articolo mostra la risposta a domande tipo *quale foto contiene il maggior numero di facce?*, *in che foto il soggetto è troppo vicino all’obiettivo?*, *quale foto ho scattato più velocemente una volta acceso iPhone per scattare?*, *qual è la mia foto scattata a maggiore velocità?* (da un aereo, ovviamente), *qual è la mia foto scattata a maggiore velocità sulla terraferma?*, *mostrami un tramonto*, *mostrami le foto scattate dove iPhone mi è caduto*, *qual è la faccia più lontana cui ho scattato una foto?*, *chi aveva la testa più inclinata quando ho scattato una foto?*.

Solo con questo elenco uno va a dormire e non ci riesce. A confronto, il *prompt engineering* su ChatGpt e compagnia si dimostra davvero roba per gente di bocca buona. Prima di riuscire a fare qualcosa del genere sul proprio computer con le proprie foto e con il proprio modello linguistico da addestrare… auguri.

Se l’assenza di Foto spaventa, si possono fare cose interessanti comunque. L’articolo ne linka un altro, [Usare SQL per trovare la mia migliore foto di un pellicano secondo il giudizio di Foto](https://simonwillison.net/2020/May/21/dogsheep-photos/). Anche questa lettura è incredibile. Mi chiedo se non dovrebbero essere materie obbligatorie nei corsi di fotografia. Un professionisti capace di interrogare la propria base dati in questo modo ha per le mani un potere smisurato.