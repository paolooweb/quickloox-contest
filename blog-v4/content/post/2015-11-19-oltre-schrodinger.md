---
title: "Oltre Schrödinger"
date: 2015-11-19
comments: true
tags: [watch, Schrödinger, dock, Cio.com]
---
Non c’è bisogno di spiegare troppo, si è sentito ovunque. Il gatto nella scatola chiusa assieme all’elemento radioattivo, che gli dà il 50 percento delle probabilità di sopravvivere. Fino a che apriamo la scatola, il gatto è insieme vivo e morto. Succo ultraconcentrato, mi si perdonino le imprecisioni ed eventualmente si ricorra a [Wikipedia](https://it.wikipedia.org/wiki/Paradosso_del_gatto_di_Schrödinger).<!--more-->

C’è chi riesce a fare di più, come su Cio.com, dove [commentano](http://www.cio.com/article/3006832/mobile-accessories/apples-79-apple-watch-charging-dock-is-here.html) l’arrivo del [dock caricatore da comodino per watch](http://www.apple.com/it/shop/product/MLDW2/dock-magnetico-per-la-ricarica-di-apple-watch-bianco?fnode=80). Un estratto:

>Il dock è bello. È anche uno dei dock di carica di watch più costosi sul mercato […] cominciamo ad avere l’impressione che i prezzi degli accessori Apple siano fuori controllo.

Ora, un estratto dal paragrafo seguente:

>[…] abbiamo raccolto alcuni dei dock indipendenti che preferiamo, inclusi alcuni che sono più costosi del dock ufficiale Apple e **non** [il grassetto è loro] comprendono la carica a induzione.

I prezzi degli accessori Apple stanno andando fuori controllo e, contemporaneamente, esistono accessori analoghi che costano più e offrono meno.

Schrödinger era un dilettante.