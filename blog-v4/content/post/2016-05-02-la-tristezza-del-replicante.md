---
title: "La tristezza del replicante"
date: 2016-05-02
comments: true
tags: [Stampa, iPhone, Apple]
---
Ringrazio **Matteo** che mi segnala questa tristezza da *La Stampa*.<!--more-->

 ![La Stampa e Apple](/images/stampa-iphone.jpg  "La Stampa e Apple") 

Non c’è bisogno di un archivio, la memoria è più che sufficiente. Tolti i dati sulla trimestrale, il pretesto, rimane una minestrina di banalità trite e già sentite. Già sentite l’anno scorso, due anni fa, tre anni fa. Scritte nello stesso modo da tutti i giornali e da tutti i siti che fanno l’ascolto medio e, purtroppo, pilotano il cervello medio.

Sarebbe rinfrescante per una volta vedere un’unghia di approfondimento. Anche dire le stesse cose, ma da un’altra angolazione. Leggere una pagina in più del minimo sindacale per scrivere un articolo appena più informato del minimo indispensabile.

La cosa triste non è che sia *La Stampa*. È che potrebbe essere qualunque quotidiano, qualunque sito che va per la maggiore, in qualsiasi posto della Terra. Lo avesse scritto uno dei robot di Google, sarebbe esattamente uguale. E io provo una discreta pena per l’autore, che fino a prova contraria mi ostino a ritenere umano, anche se si comporta da replicante.