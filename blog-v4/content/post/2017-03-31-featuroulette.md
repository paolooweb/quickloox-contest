---
title: "Featuroulette"
date: 2017-03-31
comments: true
tags: [Handoff, Siri, Playgrounds, Swift, Apfs, Pages, Rtf, watch, iPhone]
---
Anni fa nelle riunioni con gli amici e presso i rivenditori Apple si giocava una specie di roulette russa, che a posteriori chiamerei *featuroulette*. Invece delle pallottole, si usavano le novità del sistema operativo imminente.<!--more-->

A bruciapelo ti chiedevano *ma che cosa c'è dentro per cui vale la pena di aggiornare?*. Avevi a disposizione, tipo, venti secondi. Convincerli non bastava, dovevi stordirli con qualcosa di eccezionale, straordinario, roboante, pantagruelico. Altrimenti ti guardavano un po' disillusi e borbottavano va beh, boh, vedremo, aspetto.

Negli anni il gioco è caduto progressivamente in disuso. Nel sentire comune gli aggiornamenti sono diventati per lo più routine, niente *che cambi la vita*, il mantra da recitare in queste situazioni.

Beh, mi ritrovo in una situazione che non era mai accaduta. Sono soddisfatto dallo hardware che neanche si rompe o diventa inusabile. Così sto rimanendo indietro con il software.

A che cosa sto rinunciando? Ho macchine relativamente vecchie e quindi da tempo faccio a meno di funzioni come Handoff, ma non contano. Invece, se penso a El Capitan contro Sierra e a iOS 9 contro iOS 10, posso elencare a memoria e senza impegno Siri su Mac, Pages che apre i file Rtf, Swift Playgrounds su iPad (proseguire a pasticciare con Swift è uno dei miei impegni), il nuovo filesystem Apfs più efficiente e protettivo dell'integrità dei dati.

Cambia la vita? Vediamo. Siri su Mac mi manca, una volta assaggiata la comodità via Watch e iPhone difficile tornare indietro. I file Rtf in Pages semplificherebbero in varie situazioni il lavoro da iPad. Con Swift Playgrounds godrei di un ambiente pratico e piacevole per usare Swift su iPad, cosa oggi più complicata. Apfs fa risparmiare spazio e stare più tranquilli.

Se fossi dall’altra parte, l’amico che ti pone la domanda a bruciapelo e aspetta di essere travolto dalla meraviglia in venti secondi, mi direi che oggi semplificare la vita sì, equivale a cambiarla.

Questo non accelererà i miei piani di aggiornamento. Tuttavia, che non arrivino più funzioni degne di nota da un aggiornamento all’altro, è una nozione più discutibile di quanto non sembri.
