---
title: "Civiltà antiche"
date: 2014-07-19
comments: true
tags: [ITworld, Windows, Android]
---
[Lenovo ha smesso di produrre tavolette Windows](http://www.itworld.com/hardware/427596/lenovo-stops-selling-small-screen-windows-tablets-us?page=0,0) con schermo sotto i dieci pollici per il mercato americano, scrive ITworld.<!--more-->

Fino a qui è tutto chiaro, dopo di che inizio rapidamente a perdere il contatto con l’articolo.

Si dice che uno dei problemi sia il prezzo e che nelle tavolette sotto i dieci pollici dominino unità Android a basso costo. Poi si dice che invece è forte la domanda nei Paesi emergenti, Cina Brasile eccetera.

Sono Paesi dove il reddito medio pro capite è molto più basso che da noi; come è possibile che si vendano bene tavolette snobbate negli Stati Uniti per via del prezzo?

Si tratta piuttosto di Paesi dove la maggior parte delle persone ha il suo primo contatto con l’informatica. Come è accaduto a mucchi da noi, chi non sa di poter scegliere si accontenta di qualsiasi cosa e probabilmente avere Windows è come avere uno status da ricconi.

Ricconi che, come spesso accade nei Paesi emergenti, non hanno una formazione alle spalle e non sanno valutare correttamente i loro acquisti. Infatti un’altra delle ragioni per cui negli States snobbano gli schermi piccoli Windows è che al momento l’interfaccia utente, su quelle dimensioni, è terribile.

Oggetti dal prezzo troppo alto e dall’interfaccia deficitaria possono avere un riscontro di vendite solo dove non c’è, se non cultura, almeno tradizione informatica. Ovvero in civiltà che dal punto di vista informatico escono a malapena oggi dalla preistoria. Ennesimo chiodo nella bara dell’idea di un solo sistema operativo per tutti i formati di hardware.

**Aggiornamento:** Lenovo ha [chiarito](http://news.lenovo.com/article_display.cfm?article_id=1803) di voler continuare negli Stati Uniti la vendita di tavolette Windows da otto pollici. L’idea è che siano i modelli attuali a uscire di vendita una volta terminati le scorte. A pensare male viene da pensare che abbiano ricevuto una telefonata, e magari un assegno.