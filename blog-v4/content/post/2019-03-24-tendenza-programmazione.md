---
title: "Tendenza programmazione"
date: 2019-03-24
comments: true
tags: [Keychron, tastiera]
---
Come percorso professionale tendo alla programmazione a partire dalla scrittura. Per questo una tastiera con *layout* italiano mi è essenziale.

Diverso se tendessi alla scrittura a partire dalla programmazione. Mi sarebbe più utile un *layout* americano e allora potrei guardare con più interesse a [Keychron](https://www.keychron.com/products/keychron-mechanical-keyboard), una tastiera meccanica wireless retroilluminata in edizione compatta o estesa che nasce per Mac e iOS ma, se proprio uno è senza speranza, può essere adattata persino a Windows.

Nata da un Kickstarter che ha avuto successo, sembra carina e ha un prezzo assolutamente interessante, ma manca del *layout* italiano. Questione di tendenze.
