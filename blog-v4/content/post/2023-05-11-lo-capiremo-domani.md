---
title: "Lo capiremo domani"
date: 2023-05-11T13:01:07+01:00
draft: false
toc: false
comments: true
categories: [Internet]
tags: [YouTube, Turing, Yoav Shoham, Shoham, Alan Turing, Carroll, Lewis Carroll, Humpty Dumpty, Stanford]
---
>Quando uso una parola – disse Humpty Dumpty in tono di scorno – significa quello che ho deciso che significhi; niente di più, niente di meno.

>Il problema è – disse Alice – se tu possa dare alle parole così tanti significati differenti.

>Il problema è – disse Humpty Dumpty – chi debba comandare. Tutto qui.

([Lewis Carroll](https://www.goodreads.com/quotes/12608-when-i-use-a-word-humpty-dumpty-said-in-rather), *Attraverso lo specchio*)

Il problema del cambiamento *pro domo propria* del significato delle parole è centrale e un sacco di gente oggi prova a ridefinire un software come intelligente o capace di comprendere coniando una definizione di intelligenza e comprensione che si adatti a ciò che in mente, sperando di imporla se appartiene una maggioranza o, comunque, magari per convincere un finanziatore a tirare fuori denaro. È un comportamento deprecabile, analogo a quello di certi politici che operano lo stesso trucco su parola come *democrazia* o *libertà*.

Nel riconoscere che certi termini hanno comunque sfumature che impediscono di assegnare significati veramente univoci, quantomeno bisognerebbe seguire persone che, se proprio sentono l’esigenza di modellare i significati delle parole, lo facciano con un minimo di onestà e rigore.

Può tornare utile l’esempio di Yoav Shoham, scienziato che in un seminario condotto dall’Università di Stanford ha parlato di come [comprendere la comprensione](https://www.youtube.com/watch?v=ZNi69FHe3y8&t=49s).

Shoham decide che il criterio per valutare la comprensione sia la capacità di *rispondere alle domande*. Ci sono obiezioni autorevoli di lunga data a questa posizione; più pedestremente, [un neozelandese ha vinto il campionato di Scarabeo in francese senza sapere il francese](https://www.npr.org/2015/07/21/425054389/new-zealander-wins-french-scrabble-title-but-doesnt-speak-french), grazie all’apprendimento di un immenso elenco di parole francesi delle quali non aveva, appunto, la comprensione. Però, appunto, usa un approccio scientifico e quindi permette eventualmente a un interlocutore di mettere in discussione il suo modello.

Lungo la presentazione vengono definiti scientificamente criteri di definizione e misurazione. Un’entità sa rispondere alle domande se genera un livello di soddisfazione *x* in chi ha formulato la domanda; è lecito rispondere *non so* ma non su tutto; viene data una definizione di *ridicolo* per affermare che un’entità capace di comprendere non deve mai rispondere coprendosi di ridicolo; e altro ancora.

Shoham è un adepto di Turing (come il sottoscritto, dieci livelli più sotto) e ritiene che non siamo in grado di definire vincoli rispetto a che capacità umane possono essere imitate da un computer. Alla domanda *Allora, i computer possono comprendere?* – misurata sulla *sua* definizione piuttosto concessiva di comprensione – la risposta è

>Sì, possono, ma non oggi.

*So, yes they can, but don’t yet.*

E questo dovrebbe essere abbastanza per chiudere una discussione sul tema.

<iframe width="560" height="315" src="https://www.youtube.com/embed/ZNi69FHe3y8" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>