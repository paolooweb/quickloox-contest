---
title: "Vent’anni dopo"
date: 2015-01-11
comments: true
tags: [Myst, Cyan, Exile, Riven, realMyst, WoW, iMac, iPad, iPhone]
---
Scopro colpevolmente solo ora che Myst non è vivo solo [su iPad](https://itunes.apple.com/it/app/realmyst/id524988885?l=en&mt=8), 6,99 euro (assieme al fratellino minore [Riven](https://itunes.apple.com/it/app/riven-for-ipad/id536344854?l=en&mt=8) che costa un euro meno), ma anche su Mac con [realMyst](https://itunes.apple.com/it/app/realmyst-masterpiece-edition/id804096519?l=en&mt=12), 17,99 euro. Mentre per iPhone esistono Myst [gratuito](https://itunes.apple.com/it/app/myst-free/id332097399?l=en&mt=8), [a pagamento](https://itunes.apple.com/it/app/myst/id311941991?l=en&mt=8) per 4,99 euro (volendo in edizione [francese](https://itunes.apple.com/it/app/myst-francais/id339584913?l=en&mt=8) e [tedesca](https://itunes.apple.com/it/app/myst-deutsch/id339582755?l=en&mt=8)).<!--more-->

Myst è stato ai suoi tempi la prova regina della superiorità dell’ecosistema Mac. Semplicemente una cosa così, su PC, vent’anni fa non poteva nascere e basta, se non altro perché mancava [HyperCard](http://c2.com/cgi/wiki?HyperCard).

È anche stagionato bene. L’esperienza sensoriale che lo rende irresistibile è stata pareggiata da molti e financo superata – basti pensare alle bellezze panoramiche mozzafiato di [World of Warcraft](http://eu.battle.net/wow/it/), per giunta di varietà infinita – e però quel mondo, insieme ai vari che lo hanno seguito, si gode appieno anche giocandolo su un iMac a tutto schermo, quando la versione originale avrebbe risoluzione troppo piccola per un iPhone.

Sto guardando ai modi per riuscire a giocare a Riven, [Exile](http://en.wikipedia.org/wiki/Myst_III:_Exile) eccetera su un Mac moderno (per esempio tramite [SheepShaver](http://sheepshaver.cebix.net) o [ScummVM](http://scummvm.org)) e riferirò su eventuali risultati concreti.

Ora vado a rispolverare quella notte di vent’anni fa che ho risolto Myst su un Power Mac 7600, per vedere se mi torna in mente proprio tutto o se qualche enigma risulta dimenticato e deliziosamente da riscoprire. Non garantisco l’ora del risveglio di domani.