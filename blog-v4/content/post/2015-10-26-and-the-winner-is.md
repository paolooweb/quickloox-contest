---
title: "And the Winner Is…"
date: 2015-10-26
comments: true
tags: [Android, Authority, iPhone, 6s]
---
Bellissimo e lunghissimo il [test condotto da Android Authority](http://www.androidauthority.com/blind-camera-shootout-winner-650299/) sulle migliori fotocamere dei migliori computer da tasca.<!--more-->

Tutto è spiegato per filo e per segno; chi si annoia può ammirare le *gallery* fotografiche e farsi un’idea anche per immagini.

Il gusto di piccante si deve al fatto che Android Authority ha fatto votare i propri lettori alla cieca, sulla base delle foto, senza dire quale apparecchio le aveva scattate. Risultati variegati in situazioni specifiche, classifica finale che vede al primo posto… iPhone 6s. Sul sito Android Authority.

Fosse noiosa la narrazione dei test, la sezione dei commenti è spettacolare. Consiglio di scendere più in fondo che si può e risalire. Quelli che dicono *come avete fatto a votare una fotocamera scarsa come quella di iPhone* sono senza prezzo.