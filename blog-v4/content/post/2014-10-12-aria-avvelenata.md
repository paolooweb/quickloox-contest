---
title: "Aria avvelenata"
date: 2014-10-12
comments: true
tags: [Mac, WSJ, iPad]
---
Mancano quattro giorni a un evento di presentazione. Arriverà di sicuro qualcosa per Mac, dico certamente Yosemite e poi anche nuovi Mac. Immagino ci saranno nuovi Mac desktop prima ancora che portatili. Si rumoreggia anche dell’annuncio dei nuovi iPad, che è circa l’ora giusta per iniziare la campagna natalizia.<!--more-->

È abbastanza per avere aspettative e curiosità per qualunque persona minimamente interessata. Un paio di amici che non si occupano del settore mi hanno chiesto informazioni per l’acquisto di un iPad. Normalmente rispondo *compra quello che ti serve quando ti serve*, l’unico consiglio sensato. Stavolta ho risposto *se puoi attendere per una settimana fallo, così vedi la nuova offerta e hai più informazioni per decidere*.

E il *Wall Street Journal* sta a parlare di Mac in produzione a dicembre, iPad fantomatici che sarebbero stati rinviati al 2015, tutto tranne quello che interessa a una persona desiderosa di informazioni su che cosa scegliere e che cosa si possa effettivamente acquistare.

Mi chiedo a chi serva questo tipo di copertura, altro che ai clic sui banner. E trovo irrespirabile quest’aria di indifferenza per i fatti concreti, sempre in coda a qualcosa che arriva sempre sei mesi dopo (e ovviamente, poi, non apparendo in quanto inventata, viene *posposta*). Che tristezza.