---
title: "Toccami, stupido"
date: 2016-12-16
comments: true
tags: [Touch, Bar, MacBook, Pro]
---
Non ancora. Un nuovo MacBook Pro non ha ancora fatto ingresso in casa. Però c’è mancato poco.

(MacBook Pro fine 2011 con problema alla scheda video? Il programma di sostituzione è ancora attivo, ma termina il 31 dicembre 2016. Ultimi giorni).

Tuttavia ho avuto occasione di giocare per qualche minuto dal vivo con la Touch Bar. Sono rimasto colpito.

L’oggetto è assolutamente integrato nel *design*. L’osservatore distratto che non facesse attenzione alla mancanza dei tasti funzioni, noterebbe niente di strano. Da spenta, la fascia nera ha indice di interesse zero e passa inosservata.

Da accesa, è uno spettacolo. È naturalissima e nulla lascia pensare a uno schermo. Il *feeling* al tatto è buono, non c’è nulla di scivoloso o di molle al contatto, come temevo. Al tempo stesso, la preoccupazione di premere un tasta virtuale sbagliato è sopravvalutata; almeno fino a che la programmazione della Touch Bar è fatta con criterio.

Non sto a dire che Pixelmator permette di calibrare le sfumature di colore, perché alla fine è quello che uno si aspetta. Invece ho lanciato il Terminale.

Il tasto Esc c’è e ha dimensioni che impediscono di mancarlo, molto più comodo del tasto fisico sulla mia scrivania. Poi ci sono un tasto che richiama la pagina man del comando digitato, in una nuova finestra apposita. Perfetto. Poi un tasto per memorizzare un comando, e una replica dei tasti su e giù per ripercorrere la cronologia dei comandi.

Sul Terminale, il programma che interessa meno persone al mondo. Nella prima versione dell’apparecchio. Mi ha già convinto.

P.S.: sulla tastiera sospendo il giudizio perché mi serve una settimana. Il tocco è giusto, il tasto risponde bene, ma la corsa è cortissima e spiazzante. Non sono in grado di dire se dopo un’ora uno abbia la nausea o si trovi a proprio agio, né quando resista la tastiera a un dattilografo di sfondamento come il sottoscritto. Test al prossimo giro.
