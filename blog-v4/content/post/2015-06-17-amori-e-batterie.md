---
title: "Amori e batterie"
date: 2015-06-17
comments: true
tags: [Tesla, Supercharging]
---
Si ricorderà di tutti problemi sollevati nel 2007 quando iPhone, primo al mondo, presentò un computer da tasca che non aveva la batteria sostituibile. Per qualcuno un affronto ai consumatori, una furbata per vendere di più, un errore progettuale e chi più ne ha.<!--more-->

Parliamo d’altro. Parliamo d’auto.

Nel vendere macchine elettriche, Tesla ha evidentemente *qualche* interesse a risparmiare ai suoi clienti problemi di batterie. Così ci ha lavorato sopra e, nelle stazioni di servizio autorizzate, la batteria di una Model S può essere cambiata in novanta secondi. *Un minuto e mezzo*. Non è la batteria cui viene da pensare a noi, che accende il motorino di avviamento e dà corrente ai fari: è la batteria che aziona il motore. E si cambia in novanta secondi, a un prezzo leggermente inferiore a quello di un pieno di benzina (in America, dove costa la metà che da noi).

Altrimenti esiste la possibilità di effettuare il Supercharging, che ricondiziona la batteria esistente e la riporta in perfetta efficienza. È gratis per i viaggi a lunga distanza, solo che richiede *settantacinque minuti*.

Test dopo test, all’ultima assemblea degli azionisti hanno annunciato che gli interessi dell’azienda si dirigeranno verso il Supercharging, [preferito dal pubblico al cambio batteria](http://arstechnica.com/cars/2015/06/tesla-might-pull-the-plug-on-battery-swap-tech-superchargers-are-fast-enough/).

Qualcuno gli spiegherà, spero, che stanno commettendo lo stesso errore di Apple nel 2007.