---
title: "Bastava così poco"
date: 2013-05-25
comments: true
tags: [Apple, Script]
---
In Apple cercano <a href="http://www.kdnuggets.com/jobs/13/05-15-apple-data-mining-scientist.html">Data Mining Scientist</a>, scienziati del *data mining*. Questa la parte significativa della descrizione:

>Apple dispone di uno spaventoso [tremendous] ammontare di dati e abbiamo appena iniziato a scalfirne la superficie in termini di riconoscimento di situazioni ricorrenti, individuazione di anomalie, modellazione predittiva e ottimizzazione.<!--more-->

Il *data mining*, letteralmente lo scavo di dati, analizza insiemi di dati per tirarne fuori conclusioni che la semplice lettura umana non riuscirebbe a cogliere, per via della dimensione e della complessità delle informazioni da correlare. Servono conoscenze riguardanti alberi decisionali, reti probabilistiche, regole associative, *clustering*, analisi di regressione, reti neurali, Sql, Python, Java, C++, statistica, ricerca operativa, apprendimento meccanico eccetera eccetera.

Mi stupisce tutto questo sforzo di ricerca e lavoro quando basterebbe prendere tre o quattro di quei geni che sanno tutto e spiegano che ieri Apple doveva fare il *netbook*, oggi l’iPhone a basso costo, domani licenziare Tim Cook; che per mettere a posto il programma tale basterebbe aggiungere il pulsantino qui e la casellina là; che *ma quanto gli costa a mettere una chiavetta in tutte le scatole*; che *mi si è guastato il Mac quindi la qualità non è più quella di una volta* e infinite altre amenità.