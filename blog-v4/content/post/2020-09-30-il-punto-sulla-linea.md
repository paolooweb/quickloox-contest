---
title: "Il punto sulle linee"
date: 2020-09-30
comments: true
tags: [Inkscape, Illustrator, Adobe]
---
Ho dovuto improvvisarmi *graphic designer* per una notte e sono ricorso, dopo tanti anni, a [Inkscape](https://inkscape.org) per l’unico motivo di averne già fatto una vaga conoscenza, aggiunto alla recente [comparsa della versione 1.0](https://macintelligence.org/blog/2020/05/09/curve-di-livello/).

È andata complessivamente bene. Ho avuto un momento di sconforto a causa di un crash dovuto al tentativo di spostare una guida; fortunatamente era a inizio lavoro e non ho perso niente. Da lì in avanti è andato tutto liscio.

Diciamo tranquillamente che se Adobe Illustrator vuole farsi pagare per farsi usare, le ragioni ci sono e abbondanti. Tutte però riguardanti l’esperienza di utilizzo, non eventuali limitazioni; Inkscape ha fatto quasi tutto quello che gli ho chiesto e, dove ho riscontrato problemi, ho anche trovato nell’aiuto online un alleato insperato. Recuperare istruzioni precise su Inkscape sembra davvero molto agevole.

I difetti del programma sono noti e direi comuni a tanti programmi open source: c’è tutto, anche fin troppo, non sempre facile da trovare, per non dire comprendere. Ho riletto sei volte le istruzioni per convertire una forma in un tracciato e avrei anche inquadrato il problema; è che proprio non ho ottenuto quello che volevo.

Sono arrivato comunque in fondo al lavoro, però, con buona qualità e anche un discreto divertimento.

Dicevo a maggio, *diamo magari una chance a Inkscape*. Ne valeva la pena. Il pensiero che comunque l’aiuto sia funzionante agevola l’esplorazione di una interfaccia sempre un po’ in crisi di sovrabbondanza. Si tratta decisamente di un programma che non ha mai visto la collaborazione di un bravo progettista di interfaccia utente. Però ragionevolmente usabile e preciso.

Ci sarebbe da discutere di un obbligo di legge di versare un euro ai creatori di un programma open source, o in alternativa di contribuire fattivamente al progetto, prima di poterlo usare. Inkscape vale certemente l’euro. È un vettoriale gratuito, impegnativo, completo, utile.