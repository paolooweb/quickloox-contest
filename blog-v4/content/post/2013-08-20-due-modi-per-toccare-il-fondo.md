---
title: "Due modi per toccare il fondo"
date: 2013-08-20
comments: true
tags: [Rpg, Roguelike]
---
Si [diceva](https://macintelligence.org/posts/2013-08-16-artificiale-e-neanche-intelligente) di [Brogue](https://sites.google.com/site/broguegame/) o [Sil](http://www.amirrorclear.net/flowers/game/sil/), due *roguelike* di nuova generazione per cacciarsi nei guai con il proprio personaggio, che si fa uccidere per sempre e un numero indecente di volte nel tentativo di arrivare in fondo a un sotterraneo sempre diverso e sempre più pericoloso. Rappresentato come sfida suprema all’immaginazione da caratteri di testo e poco più.<!--more-->

Due sapori diversi. Più intellettuale Sil, che discende da [Angband](http://rephial.org) e ne ha rivisitato tutte le meccaniche. Tutto più semplice da imparare e più infido da valutare: i rapporti di forza cambiano in continuazione e geniale è la modalità di combattimento che passa da un semplice confronto tra valori di chi attacca e chi difende a una serie di compromessi. Non è detto che le armi più potenti siano sempre le migliori e il meccanismo di capacità, esperienza, crescita del personaggio è subdolamente banale e al tempo stesso profondissimo, riferito rispettivamente a manovrarlo e ottenere davvero dei risultati. Si può morire molto in fretta – i trucchi di gioco che si appoggiano al meccanismo originale sono stati sostanzialmente eliminati – e naturalmente la morte è finale; l’unico modo per giocare di nuovo è ricominciare da zero, con l’unica possibilità di conservare l’impianto di un personaggio già creato.

Brogue discende da [NetHack](http://nethack.org) e prende del tutto diversamente il problema di rappresentare un mondo in caratteri alfanumerici, attingendo alle possibilità di colorare caratteri e sfondi [proprie del Terminale](https://macintelligence.org/posts/2013-08-17-conchiglie-e-arcobaleni/). Gli specchi d’acqua luccicano, l’erba è verde ma si arrossa di sangue o brunisce se una torcia malauguratamente caduta a terra – magari per avere raccolto incautamente la chiave di comando di un altare segreto – appicca un incendio.

In Brogue possiamo farci amici un animale che abbiamo salvato dalla crudeltà dei suoi simili, e combatterà per noi. Ci sono problemi logici e non solo tattici da risolvere; L’ambiente non è solo un vincolo, ma una possibilità non sempre evidente di interazione. Da giocare su Mac o [anche su iPad](https://itunes.apple.com/it/app/brogue/id613921309?l=en&mt=8), cosa non comune per un *roguelike*.

Due modi per fare lavorare l’immaginazione, divertendosi.