---
title: "La magia del suono"
date: 2014-05-18
comments: true
tags: [Puc, Midi, iPad, wireless, musica]
---
Le onde sonore si propagano liberamente nell’aria. Cavi e apparecchiature ci servono come artificio per dominarle e canalizzarle nelle forme che ci piace ascoltare.<!--more-->

Ora però mi sono innamorato di [Puc](http://www.mipuc.com), aggeggino a forma di dischetto da *hockey*, non molto più grande, che elimina tutti i cavi tra un iPad e uno strumento o una periferica Midi. Tutto *wireless*, tutto libero, tutto via aria. Spettacolo puro.

Fa venire voglia di mettersi a suonare. Imparare a suonare. Suonare anche senza imparare. Il prezzo? Fa niente, ci si sacrificherà per quanto occorre.

<object width="560" height="315"><param name="movie" value="//www.youtube.com/v/ZQiq0i0eGVM?version=3&amp;hl=it_IT"></param><param name="allowFullScreen" value="true"></param><param name="allowscriptaccess" value="always"></param><embed src="//www.youtube.com/v/ZQiq0i0eGVM?version=3&amp;hl=it_IT" type="application/x-shockwave-flash" width="560" height="315" allowscriptaccess="always" allowfullscreen="true"></embed></object>