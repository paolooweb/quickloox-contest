---
title: "Spacciatori di comodità"
date: 2021-02-25T00:00:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [Windows, Visual Studio, JavaScript, TypeScript, npm, GitHub] 
---
Sono alle prese con diverse situazioni di migrazione a Mac che vedono come denominatore comune la difficoltà di fare giustizia di vecchie app che parrebbero imprescindibili, accomunate da sue caratteristiche comuni: sono Windows e non prevedono una via di uscita semplice per iniziare a usare gli stessi dati con un altro programma.

Ci ho pensato leggendo questa riflessione su come, con poche mosse strategiche bene assestate, Microsoft abbia eliminato di fatto la comunità open source attorno a JavaScript per assumere il controllo assoluto delle direzioni di sviluppo. In modo morbido, amichevole, amorevole, legale, armonioso, spietato e definitivo.

Che cos’hanno in comune le due situazioni? La comodità.

Il vecchio programma faceva il suo mestiere, funzionava bene, faceva quello che gli si chiedeva, era tanto comodo. Nessuno che abbia fatto uno sforzo per porsi una domanda sul dopo, sui formati usati, sull’interoperabilità, niente. Risolto il problema contingente, tutti gli altri sono spariti dal radar.

Nel caso di [JavaScript](https://www.javascript.com), Microsoft ha messo in campo strumenti vecchi e nuovi. Ha creato [TypeScript](https://www.typescriptlang.org), un clone di JavaScript compatibile (*embrace*) capace però di fare più cose (*extend*). Poi ha  [comprato gli strumenti di distribuzione](https://thenewstack.io/github-acquires-npm-buying-microsoft-a-presence-in-the-node-javascript-community/) ([npm](https://www.npmjs.com)) oltre a quelli di reperimento ([GitHub](https://github.com)) del software.

JavaScript è uno standard neutrale e certificato, con un comitato apposito a curarsene. Non se lo fila più nessuno; TypeScript è tanto supportato, fa alcune cose meglio, cresce molto in fretta, Microsoft è una garanzia. È *comodo*.

Se vuoi fare sviluppo serio, hai convenienza e comodità a usare TypeScript. Che è open source ovviamente, solo che va esattamente dove vuole Microsoft. A differenza di JavaScript, che è standard a prescindere dalle aziende.

Ecco. Certo, la comodità è tutto. Chiedo però una piccola riflessione a chi non si pone il problema di usare una cosa buona per l’oggi senza pensare al domani. È comodo anche buttare la cartaccia per terra invece di cercare con pazienza un cestino.

Nel software, la comodità è tossica. Fa stare tanto bene, poi non ne esci più e sei controllato da qualcun altro che ti passa tutto quello che ti serve.

*Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile.*