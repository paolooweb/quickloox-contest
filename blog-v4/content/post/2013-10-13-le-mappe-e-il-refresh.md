---
title: "Le mappe e il refresh"
date: 2013-10-13
comments: true
tags: [iPhone]
---
Ho avuto l’occasione di rimettere alla prova le Mappe di iPhone, che mi avevano [prosciugato la batteria](https://macintelligence.org/posts/2013-09-28-recita-a-soggetto/) il giorno dell’installazione di iOS 7.<!--more-->

Questa volta è andata bene e tutto ha funzionato a meraviglia. Il percorso si situava nell’*hinterland* milanese, su strade dal conosciuto all’isolato, di traffico globale e locale. La nuova interfaccia minimale, sulle Mappe, è un capolavoro, molto apprezzato. La stima dei tempi ha avuto una varianza massima di tre minuti su trentotto, più che accettabile. Nessun errore di percorso, che non è una novità e non significa niente ove iPhone venisse usato in luoghi più remoti, con mappe meno definite. Però è piacevole constatarlo.

Se si ha l’accortezza di impostare l’itinerario mentre ci si trova in Wi-Fi prima di uscire, non è necessario avere la connessione attiva per seguirlo. L’importante è non sbagliare troppo strada, perché ovviamente non vengono caricati i nuovi dati cartografici.

Impostare l’itinerario su Mac, con le Mappe di Mavericks, è un piacere, visto lo schermo a disposizione. Un clic e l’itinerario è pronto da usare su iPhone. È veramente così e non potrebbe essere più facile.

A margine, una nota sul consumo della batteria di iPhone 5 con iOS 7. Dopo l’incidente dell’installazione, l’autonomia si è normalizzata e credo che sia equivalente o forse leggermente inferiore a prima. Ho svolto qualche rapido esperimento, assolutamente non esaustivo, per concludere che la criticità maggiore sia sistemare bene l’elenco delle *app* che hanno diritto di aggiornarsi da sole anche se non sono in primo piano.

Complessivamente sto apprezzando iOS 7. Ci sono diverse asperità di rifinire e ci sono punti dove l’interfaccia ha allungato la procedura. Però è una ripartenza; l’impianto generale è buono.