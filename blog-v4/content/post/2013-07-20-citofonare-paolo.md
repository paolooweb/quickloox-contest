---
title: "Citofonare Paolo"
date: 2013-07-20
comments: true
tags: [iPad, iPhone, Viticci, MacStories, Zambelli, Vlc, Attivissimo, Denis-Courmont, Nokia]
---
Non l’ho ancora visto su App Store, ma è solo questione di tempo: come da *scoop* del bravissimo Federico Viticci di *MacStories* (il più bravo resta sempre [Fabio](https://twitter.com/settebit), lui però è a livelli altissimi e dietro c’è il vuoto pneumatico), [torna Vlc per iOS](http://www.macstories.net/news/vlc-for-ios-returns-to-the-app-store/).<!--more-->

Era scomparso per una [diatriba sulla sua licenza](https://macintelligence.org/posts/2012-07-08-liberta-va-cercando/), iniziata da Rémi Denis-Courmont, uno dei programmatori del progetto, sospettabile di conflitto di interessi per via della sua militanza in Nokia e del suo possibile ampio mancato guadagno a causa della comparsa di iPhone.

Paolo Attivissimo non perse l’occasione di [definire iPad *terminale “stupido” e lucchettato*](https://macintelligence.org/posts/2012-07-06-liberta-e-libertuccia/).

Mi aspetto che Paolo ora cambi idea. Sosteneva pure l’impossibilità di avere software libero su App Store e lo avevo smentito con un elenco chilometrico.

Ora iPad non è più lucchettato, giusto? Vlc torna.

Quanto a Denis-Courmont, mi piacerebbe sentire il suo parere. Dopotutto la sua Nokia, ora, produce apparecchi la cui chiusura fa impallidire quella presunta di iOS.
