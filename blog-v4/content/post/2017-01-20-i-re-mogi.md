---
title: "I re mogi"
date: 2017-01-20
comments: true
tags: [iPhone, Android, emoji]
---
Gente furba, sempre in sella, che conosce la vita. Mica si fanno fregare da quei bastardi di Apple che ti chiedono un rene per un telefono: hanno approfittato di un tre-per-due e hanno acquistato per un tozzo di pane un bellissimo apparecchio Android che costa una frazione di iPhone e *fa tutto quello che fa iPhone*.

Mando un carattere emoji e il re del mondo [non lo vede](http://blog.emojipedia.org/androids-emoji-problem/). Perché fa parte di quel novanta percento abbondante di persone che, per qualunque motivo, non ha il sistema aggiornato.

Quasi il novanta percento di chi usa un iPhone ha, invece, il sistema aggiornato. E vede tutti gli emoji, anche quelli nuovi.

Ora io capisco che si prende un Android per risparmiare, tanto non si usa la posta, non si usa il web, non si comprano app, si fa poco più che niente. Ma pensavo che almeno uozzap e fesbuc. I gattini, le foto del cornetto della colazione… e gli emoji. Le faccine. Almeno le faccine.

Neanche quelle. Quadratini vuoti. Gente furba, sempre in sella, mogia.