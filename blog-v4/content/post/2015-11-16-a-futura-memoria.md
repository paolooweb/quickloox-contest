---
title: "A futura memoria"
date: 2015-11-16
comments: true
tags: [Apple, auto]
---
Le dieci ragioni per cui una auto elettrica prodotta da Apple [fallirà miseramente](http://www.rantcars.com/2015/02/16/10-reasons-the-apple-electric-car-will-fail-miserably/). Posto che Apple produca un’auto elettrica, naturalmente. Servisse.<!--more-->

Ovviamente, [non è la prima volta che si legge qualcosa del genere](https://macintelligence.org/posts/2015-09-26-condannati-a-ripetere/).