---
title: "Crescita e sviluppo"
date: 2016-03-20
comments: true
tags: [StackOverflow, Mac, Linux, Windows, programmazione]
---
Riferisce Apple World Today che secondo una indagine condotta da StackOverflow, comunità tra le più apprezzate in campo di programmazione, [Mac è per la prima volta più usato di Linux](http://www.appleworld.today/blog/2016/3/18/more-developers-are-now-using-mac-os-x-than-linux) per programmare.

Sono stati interpellati oltre cinquantamila sviluppatori da 173 Paesi: ovvero i risultati indicano davvero qualcosa.

Si noti che la maggioranza degli sviluppatori lavora su Windows; tuttavia si tratta del 52,2 percento, non la Bulgaria che era una volta. I computer desktop sono Windows nel 90 percento dei casi, ma solo cinque volte su nove si programma in un ambiente Windows. Anche questo ha un senso su cui riflettere.

Per non parlare dell’ennesimo bastione di professionisti che Apple starebbe abbandonando e che, misteriosamente, più viene abbandonato più porta risultati.
