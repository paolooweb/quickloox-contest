---
title: "Plastic People"
date: 2020-07-25
comments: true
tags: [Zappa, Teams, Microsoft, NBA, NFL, coronavirus, Covid-19]
---
Nel football americano professionistico girano tavolette offerte dallo sponsor che poi paga perché vengano mostrate durante la partita. Vari episodi hanno mostrato come in larga parte vengano ignorate, misconosciute o [rifiutate](https://macintelligence.org/blog/2016/10/19/tavoletta-rasa/), magari da allenatori già entrati nella leggenda. Si sta vicini alla realtà se le si descrive come fintamente usate.

Lo stesso sponsor fornisce anche tecnologia alla lega del basket, che quest’anno causa virus giocherà una serie ridotta di partite, tutte dallo stesso luogo e tutte a porte chiuse. Enormi schermi posizionati attorno al campo mostreranno le facce di appassionati paganti che oltre a vedere la partita a distanza vogliono figurare come spettatori. Ogni schermo [proietterà circa trecento facce riprese in videoconferenza](https://www.cnet.com/news/nba-is-using-microsoft-teams-to-brings-virtual-fans-into-its-real-world-games/), uniformandone lo sfondo come se appunto fosse quello dell’arena.

Le due esperienze sono accomunate dallo stesso tratto: vendere agli spettatori un’esperienza artefatta senza che in cambio arrivi un qualunque miglioramento sostanziale.

Mi viene in mente [Frank Zappa](https://genius.com/Frank-zappa-plastic-people-mystery-disc-lyrics):

_Their shoes are brown_<br />
_They match their suits_<br />
_They got no balls_<br />
_They got no roots_

_Because they’re plastic people…_