---
title: "Al settimo giro"
date: 2013-12-10
comments: true
tags: [MagicTrackpad]
---
Cambio di batterie in Magic Trackpad. Quelle precedenti – Duracell – sono durate 136 giorni, nel margine inferiore della media. A parte le batterie di serie all’acquisto, 70 giorni, e una coppia di ricaricabili, 74 giorni, solo un altro paio di Duracell è durata meno: 133 giorni.<!--more-->

La media complessiva è attualmente di 154 giorni a cambio, cinque mesi.

Sto utilizzando le funzioni di risparmio energetico dell’apparecchio, senza spegnerlo la sera. Apparentemente il consumo non ne risente, o non più di tanto.

La nuova coppia di batterie è Ikea, una prima assoluta. Vedrò come si comportano.