---
title: "Arte e app-arenza"
date: 2014-06-14
comments: true
tags: [Björk, Biophilia, Moma, Planetary, Smithsonian, Maeda]
---
Per la prima volta nella storia (dell’arte) una *app* scaricabile è stata aggiunta alla raccolta permanente del Museum of Modern Arts di New York.<!--more-->

La *app*, c’era bisogno di precisare?, è per iPad e al contrario di praticamente qualunque altra opera d’arte ha un [costo estremamente contenuto](https://itunes.apple.com/it/app/bjork-biophilia/id434122935?l=en&mt=8), vastamente accessibile a un grande pubblico.

L’[acquisizione del codice di Planetary da parte del museo Smithsonian](https://macintelligence.org/posts/2013-08-29-pazzi-da-museo/) di cui si scriveva l’anno scorso è un’operazione di altro genere, di conservazione più che di valore intrinseco.

Bisogna anche considerare la parola *scaricabile*. Come spiega l’[articolo del museo a commento dell’operazione](http://www.moma.org/explore/inside_out/2014/06/11/biophilia-the-first-app-in-momas-collection),

>Le prime *app* aggiunte alla raccolta sono state i [Reactive Books](http://www.maedastudio.com/rbooks/) del 1994 di John Maeda, distribuiti su floppy disk dentro libri fisici tradizionali.

Può sembrare una questione futile e invece ha importanza. Il dibattito attorno a cosa è arte e che cosa no è infinito, perché quello che definiamo arte diventa parte della nostra identità e storia. Quello che all’apparenza potrebbe essere considerato semplicemente un album musicale più estroso di altri è stato giudicato degno di aprire una strada mai battuta prima.
