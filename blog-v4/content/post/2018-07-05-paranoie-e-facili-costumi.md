---
title: "Paranoie e facili costumi"
date: 2018-07-05
comments: true
tags: [Android, Windows, Facebook, Google, Apple, Microsoft]
---
Uno dice che Apple è diversa dalle altre aziende e giù a sentire gli insulti, le maldicenze, sei un credente, prendi i soldi, basta guerre di religione, tutti i computer sono uguali.

Poi legge che un test su oltre diciassettemila app Android ha trovato che, tranquilli, la storia dell’apertura surrettizia del microfono per ascoltare di nascosto le conversazioni a uso pubblicitario è una leggenda urbana.

C’è il fatto trascurabile che oltre novemila di queste app, in compenso, [salvano di nascosto schermate del computer da tasca e le spediscono altrove](https://gizmodo.com/these-academics-spent-the-last-year-testing-whether-you-1826961188), sempre di nascosto, sempre a fini pubblicitari. Se va bene.

Per chi non usasse Android, si era forse già detto che [Facebook, Google e Microsoft annidano nelle loro interfacce trucchetti per acquisire dati degli utenti](https://gizmodo.com/facebook-google-and-microsoft-use-design-to-trick-you-1827168534). Banalità: dopotutto, che male potrà mai fare una Microsoft in possesso di dati non richiesti con chiarezza e forniti senza permesso? Al massimo potrà [farcire di pubblicità non richiesta il suo sistema operativo](https://www.theverge.com/2017/3/17/14956540/microsoft-windows-10-ads-taskbar-file-explorer).

A forza di considerare il compratore un prodotto da vendere agli inserzionisti, può anche capitare l’inciampo, non so, [aggeggi Samsung che spediscono le foto in memoria a contatti a caso](https://www.theverge.com/circuitbreaker/2018/7/2/17528076/samsung-phones-text-rcs-update-messages) (grazie **Andy**!).

In Apple il compratore è il compratore e basta. E tanto basta a me per preferirla e riconoscerne il ruolo di mosca bianca, in senso del tutto positivo.