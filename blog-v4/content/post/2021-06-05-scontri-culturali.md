---
title: "Scontri culturali"
date: 2021-06-05T13:00:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [Apple, remote working, Slack, John Gruber, Daring Fireball, The Verge] 
---
Non si fa in tempo a scrivere un pezzo sull’[approccio di Apple al lavoro intelligente](https://macintelligence.org/posts/La-forza-irresistibile.html) che [diviene pubblica una lettera scritta da un gruppo di dipendenti](https://www.theverge.com/2021/6/4/22491629/apple-employees-push-back-return-office-internal-letter-tim-cook) per chiedere più flessibilità rispetto alla scelta di lavorare in presenza oppure in remoto.

John Gruber [ha espresso chiaramente il punto di vista culturale di Apple](https://daringfireball.net/linked/2021/06/04/apple-remote-work): la società ha un approccio che dà molto valore al gruppo di lavoro e preferisce di molto l’ambiente dell’ufficio.

Gruber aggiunge alcune valutazioni personali che hanno diritto a fare parte del dibattito: in sintesi, Apple è diventata così grande che per forza di cose ospita anche persone poco adatte, o inadeguate, alla filosofia aziendale.

Sembra una schermaglia banale e invece va seguita, perché è rappresentativa di una tendenza che in autunno diventerà in alcune aziende una miscela esplosiva. Inoltre costituisce l’inizio di un cambiamento culturale profondo nel considerare l’attività lavorativa.

Per il momento aggiungo solo un paio di nozioni, anch’esse di diritto parte importante della discussione.

La prima: la buona organizzazione vince su tutto. Apple per prima ha dimostrato di poter sviluppare, creare e vendere a pieno ritmo anche a Apple Park deserto e tutti a lavorare da casa. È iniziata la transizione a M1 (e hai detto niente), sono usciti regolarmente nuovi modelli di tutto, i numeri hanno regolarmente superato le previsioni. *La produttività di un’azienda dipende dalla sua organizzazione, non da dove si lavora*.

(Ciononostante, Apple ha ragioni per volere la gente in ufficio tre giorni a settimana. Ma questo non riguarda la capacità dell’azienda di funzionare al meglio).

La seconda: la lettera dei dipendenti ha avuto origine su un canale Slack interno ad Apple. Ci sono strumenti per la conversazione e la collaborazione che vanno bene per aziende di decine di migliaia di dipendenti con fatturati di centinaia di miliardi.

Ha ragione Gruber nel dire che nella Apple di una volta, un canale Slack non sarebbe mai esistito. I tempi, nondimeno, cambiano.

La cultura dell’ufficio contro la cultura del lavoro intelligente (non in Apple, ovunque). È nell’interesse di tutti trovare una composizione armoniosa. Si annunciano tempi interessanti.

*Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile.*               