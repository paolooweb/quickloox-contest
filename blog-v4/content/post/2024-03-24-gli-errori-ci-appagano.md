---
title: "Gli errori ci appagano"
date: 2024-03-24T00:01:06+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [macOS, FaceTime]
---
Ho visto millanta volte il messaggio di errore per una applicazione chiusasi inaspettatamente. Ma la sua parafrasi, l’applicazione ha smesso di essere aperta, decisamente mi mancava.

Dopo tanti anni di Mac riesco ancora a generare messaggi di errore esoterici. Un giorno scoprirò anche il come.

![L’applicazione FaceTime.app non è più aperta](/images/facetime.png "Non riesco neanche a capire che differenza ci sia tra così e dire: si è chiusa.") 