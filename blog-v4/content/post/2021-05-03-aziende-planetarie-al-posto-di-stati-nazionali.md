---
title: "Aziende planetarie al posto di stati nazionali"
date: 2021-05-03T00:57:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [Gianni Catalfamo, CorpoNazione, John Perry Barlow, Apple, Google, Facebook, Amazon] 
---
Letto il post di Gianni Catalfamo sulle [CorpoNazioni](https://sonofgeektalk.wordpress.com/2021/04/30/la-nascita-della-corponazione/), come le ha definite? Queste sono le mie osservazioni a contorno, che dietro sua richiesta – generale – aggiungerò come commento sul suo blog dopo avere pubblicato qui.

Ci sarebbe spazio per dire e fare molto di più e molto meglio, solo che non c’è il tempo e chiedo pazienza, oltre che naturalmente domande e commenti.

§§§

L’azienda-nazione (d’ora in poi azienda) tende alla verticalità, a fornire principalmente un servizio, là dove lo stato-nazione (d’ora in poi stato) tende alla trasversalità, ovvero a occuparsi di tutto.

Gli stati adottano forme di interscambio di merci e servizi; le aziende tendono semplicemente a lavorare in parallelo, ciascuna sul proprio binario.

La vera differenza è nella cittadinanza, trasversale. Il cittadino aziendale, ipersemplifico, usa hardware Apple, software Microsoft, consegne Amazon, informazioni Google, socialità Facebook. Ci sono certo aree marginali di sovrapposizione e di concorrenza, che lasciano inalterato il quadro di insieme.

Il cittadino aziendale possiede non uno ma numerosi passaporti, o account, e utilizza l’uno o l’altro secondo convenienza per fruire dei servizi desiderati.

Non si resti sorpresi: si rilegga la [Dichiarazione di indipendenza del ciberspazio](https://www.eff.org/cyberspace-independence), pubblicata da John Perry Barlow nel 1996.

> [rivolto ai governi delle nazioni industrializzate] I vostri concetti legali di proprietà, espressione, identità, movimento e contesto non si applicano a noi [cittadini del ciberspazio]. Si basano tutti sulla materia e qui di materia non ce n’è.

Rileggere Perry Barlow un quarto di secolo dopo è capire che da subito lui aveva intuito l’incapacità strutturale da parte della politica e dei governi di affrontare la novità.

In questi venticinque anni il vuoto conseguente è stato colmato dalle aziende, non tanto per particolare genio o intuizione (Bill Gates aveva snobbato la nascente Internet, prima di capovolgere idea e strategia) quanto per la loro naturale inclinazione a crescere e generare profitto.

Le aziende hanno trovato nuovi settori di fornitura ai cittadini, che gli stati non potevano intrinsecamente comprendere e nei quali quindi non operavano, e hanno iniziato a fornire servizi su scala planetaria. Questa nuova dimensione ha portato le aziende più grandi ed efficienti ad acquisire la medesima scala. Il fatturato di Apple ha lo stesso ordine di grandezza di tanti prodotti interni lordi di nazioni ragionevolmente moderne e sviluppate.

È abbastanza noto che Internet favorisca la disintermediazione, come ha scoperto per esempio e amaramente l’editoria. Le aziende su scala planetaria, semplicemente, _disintermediano lo stato_.

Questa disintermediazione ha successo perché rimuove vari livelli di frizione introdotti artificiosamente dagli stati.

Si può giocare all’infinito alle differenze e similitudini, ma le due cose che più contano sono queste:

* la tassazione di uno stato è slegata dalla sua fornitura di servizi, è indifferente alla soddisfazione del cittadino e serve ad alimentare la sopravvivenza dell’apparato statale stesso, mentre nell’azienda la soddisfazione del cliente-cittadino è fondamentale e c’è una corrispondenza diretta tra contributo finanziario e servizio fornito, senza il livello intermedio della burocrazia;
* l’azienda lavora per offrire servizi capaci di agevolare e facilitare la vita del cittadino, nonché aumentare la sua capacità di sostenersi economicamente. Lo stato è indifferente alla crescita del reddito del cittadino, a parte la richiesta di una tassazione proporzionata.

Le aree di servizio dove non sono apparse aziende planetarie in sostituzione degli stati sono quelle dove, semplicemente, la presenza attuale degli stati è più radicata e monopolista. Nessuno stato consentirebbe, oggi, a un’azienda di proporre servizi alternativi in tema di istruzione, difesa, salute, per dire i principali.

Tuttavia la direzione è chiara e alcuni vecchi capisaldi, per esempio il servizio postale rispetto ad Amazon e alla posta elettronica, sono già saltati.

Se la direzione è chiara, non altrettanto è la previsione dell’assetto futuro che potrebbe prendere la situazione. È indubbio che Internet provochi la disintermediazione dello stato. Altrettanto indubbiamente, nel lunghissimo termine gli stati nazionali sono destinati a scomparire, sostituiti da forniture di servizi a livello planetario (forniture che potrebbero essere gestite da aziende come le vediamo oggi o da strutture interamente nuove). Tuttavia esistono regimi autoritari disposti a minare il funzionamento generale di Internet pur di conservare il loro potere. Anche i regimi democratici iniziano a intraprendere azioni tese a limitare e sminuire la portata dei servizi offerti dalle aziende planetarie. Che cosa succederà nel medio e lungo termine, quindi, è un’incognita sottoposta a molte variabili. Dalla persecuzione fiscale alla nazionalizzazione alla messa fuorilegge dell’azienda, le tattiche a disposizione di uno stato che voglia opporsi alle aziende planetarie sono numerose e potenti.

L’anno appena passato ha aperto comunque una finestra importante sulla concretezza della transizione, che prima della pandemia era oggetto di analisi – ho scritto diversi pezzi in proposito negli ultimi anni – ma rimaneva largamente un fenomeno teorizzato o anticipato con logica un po’ da fantascienza.

Come siano andate le cose lo sappiamo. Alle persone servivano improvvisamente strumenti per lavorare, imparare e comunicare a distanza, informazioni costanti, consegne a domicilio. Tutto questo è stato fornito dalle aziende, non dallo stato. Gli stessi vaccini sono nati per iniziativa privata e forniti su scala planetaria dalle aziende che li hanno messi a punto. Per quanto non abbiano avuto particolare successo, le app di ausilio alle strategie di tracciamento dei contagi si sono basate su un _framework_ offerto su scala planetaria da Apple e Google, implementato su apparecchi iOS e Android.

Fuori dal campo sanitario, la ripresa dei programmi spaziali e la colonizzazione futura di Luna e perfino Marte mostrano la fine del monopolio delle aziende spaziali di stato. SpaceX e Blue Origin erano impensabili ai tempi di un programma Apollo; oggi testimoniano addirittura una concorrenza in atto.

È stata una dimostrazione plastica della validità del _principio di sussidiarietà_: lo stato ha ragione di esistere nei settori dove manca una iniziativa non statale che, dove sussiste, è in generale più efficiente ed efficace e, dove non lo è, viene spinta a diventarlo, pena essere rimpiazzata dall’iniziativa di una azienda planetaria.

Nessuno stato può raggiungere a livello trasversale l’efficacia e l’efficienza, nonché la salute economica, di una azienda planetaria specialista in un settore. Si può discutere all’infinito di teoria, ma questo è un fatto concreto innegabile che provoca l’evoluzione attuale e potrà essere contrastato anche pesantemente, ma mai contraddetto.

Non credo che le mie figlie vedranno la fine degli stati nazionali, ma l’inizio del loro sfaldarsi sì; queste sono le avvisaglie. Neanche i miei eventuali nipoti lo vedranno, immagino. Spero che invece i miei pronipoti cresceranno ben preparati a questa eventualità.

§§§

*Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile.*