---
title: "Non farsi toccare"
date: 2013-09-17
comments: true
tags: [iPhone]
---
Da anni è noto come si possano impiegare gelatina, plastilina o silicone per ottenere [finte impronte digitali](http://www.puttyworld.com/thinputdeffi.html) capaci di ingannare un sensore.<!--more-->

Anni fa è successo in Malesia che, per rubare un’auto collegata alle impronte digitali del proprietario, i ladri [gli abbiano tagliato l’indice](http://news.bbc.co.uk/2/hi/asia-pacific/4396831.stm).

Sulla base di questi precedenti, c’è chi ha approfittato dell’annuncio di [Touch ID su iPhone 5S](http://www.apple.com/it/iphone-5s/features/) per spargere ilarità o [allarmismo](http://www.independent.co.uk/life-style/gadgets-and-tech/news/iphone-5s-thieves-may-mutilate-owners-in-bid-to-gain-access-to-fingerprintreading-handsets-expert-warns-8808577.html) a seconda del tasso alcolico.

Il sensore su iPhone 5S utilizza tecnologie di [riconoscimento della carica elettrica](http://www.patentlyapple.com/patently-apple/2013/08/a-new-fingerprint-sensor-patent-from-apple-surfaces-in-europe.html) del corpo umano e pertanto opera solo in presenza di tessuti viventi.

Il dito di gelatina non funziona. Il dito mozzato tecnicamente è vivo – cioè conserva la carica elettrica – per qualche istante e poi più, per cui operativamente non funzionerà.

In attesa di eventuali conferme sperimentali che non ci auguriamo, ovviamente, è inutile farsi toccare dalle battute e dalle sciocchezze.