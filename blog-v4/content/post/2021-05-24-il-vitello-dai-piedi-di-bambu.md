---
title: "Il vitello dai piedi di bambù"
date: 2021-05-24T00:42:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [Cina, Apple, Tim Cook, iPhone, New York Times, Akko, Luca Accomazzi, Fabio Massimo Biecher, Huawei, Il vitello dai piedi di balsa, Elio e le Storie Tese, Acer, Hewlett-Packard, PowerBook, M1, Nanchino, Cupertino] 
---
**Fabio Massimo Biecher** ha chiesto su LinkedIn all’amico Akko e a me una [disamina laica](https://www.linkedin.com/posts/fabiomassimobiecher_censorship-surveillance-and-profits-a-hard-activity-6800361403409604609-pxd3) del recente [articolo del New York Times](https://www.nytimes.com/2021/05/17/technology/apple-china-censorship-data.html) dedicato ai compromessi e alle contraddizioni sulla privacy che contraddistinguono il rapporto di Apple con la Cina.

Un *disclaimer* doveroso: una volta sulle mailing list, ora sui social, vengo accusato circa una volta a bimestre di essere un agente al soldo di Apple e guadagnare dalla mia attività di blogger. Si sappia, lo avevo già detto, che anni fa ho acquistato una azione di Apple. L’investimento ha avuto un successo poderoso e, per esempio, lo scorso trimestre mi ha fruttato quarantatré centesimi. Per via di una scommessa ho comprato una seconda azione. Nel 2021 potrei pertanto intascare da Apple qualcosa come due o persino tre euro. Si capisce come potrei scrivere qualsiasi cosa, pur di accumulare denaro.

Prima di tornare in argomento, faccio notare che Akko ha già composto [una risposta da tesi di laurea](https://apple.quora.com/Su-Linkedin-Fabio-Massimo-Biecher-mi-chiede-1-cosa-io-ne-pensi-della-notizia-secondo-la-quale-Apple-ha-permesso-al-go) e in ambito fattuale è rimasto assai poco da dire. La sua conclusione è largamente condivisibile:

>alla fin fine Apple non ne esce affatto bene, ma da dove sto seduto io non si vede come chiunque avrebbe potuto far meglio di Tim Cook a questo giro.

Si fa molto moralismo di maniera, ma non si sentono molte strategie alternative così migliori di quella attuale di Apple.

Quello che ho da aggiungere è in parte mutuato dal [commento di John Gruber su Daring Fireball](https://daringfireball.net/2021/05/nyt_apple_china_icloud), di cui mi piace riportare una frase su un aspetto della questione che trovo sottovalutato.

>Perfino alla luce dei molteplici e significativi compromessi accettati da Apple per rispettare la legge cinese, appare del tutto possibile che usare apparecchi Apple e iCloud sia una delle cose più private che chiunque, piramide di governo cinese a parte, possa fare in Cina.

Stiamo a parlare delle (non) alternative di Apple; e quelle delle persone? Il *New York Times* ha descritto anche la [rete di sorveglianza che copre le città cinesi](https://www.nytimes.com/2019/12/17/technology/china-surveillance.html).

>Una delle più grandi reti spionistiche al mondo prende di mira le persone ordinarie e nessuno la può fermare.

Il governo cinese *non ha bisogno* dei dati di iCloud; ha mille modi più pervasivi e invasivi di ottenere informazioni molto più compromettenti e decisive. Semplicemente, si adopera perché Apple si conformi o comunque non goda di troppa indipendenza sulla privacy degli utenti. Tim Cook non può essere messo a fare lo zerbino, neanche dai cinesi; troppi posti di lavoro, troppa economia che gira, troppa tecnologia avanzata. Xi non può neanche tollerare che Cook si prenda libertà non concesse a terzi.

Apple, è vero, scende a compromessi con il governo cinese. Ma non è compromessa con il governo cinese, come invece altri. Un iPhone può consentire una parvenza di privacy, dove l’alternativa è l’assenza. Un ritiro di Apple dal mercato cinese farebbe contenti tanti farisei della privacy e darebbe la stretta definitiva a milioni di persone già sotto strangolamento costante quotidiano, e certo non per farsi propinare pubblicità più personalizzata.

Mi accodo inoltre a quanto ha già scritto chiunque altro per fare notare che il manicheismo, tutto bianco o tutto nero, funziona male se bisogna spiegare interazioni e situazioni che implicano conseguenze per miliardi di persone e miliardi di dollari. Apple è una squadra molto vincente in questo momento e le squadre che dominano sono molto amate, e molto odiate. L’idea che, per parafrasare [Elio](https://www.youtube.com/watch?v=kcxz53LgnPY), Apple abbia in effetti piedi di pane ricoperti da un sottile strato di cobalto, è irresistibile per i rosiconi, i vorrei ma non posso, quelli che dicono *neolibberismo* con due *b*, gli infastiditi dalla necessità di usare l’intelligenza prima di provare a dire cose intelligenti, per tutto questo popolino che vive strisciando nella polvere e si vendica con la gioia di vedere cadere nella polvere qualcun altro.

Se si sale appena di un gradino, si capisce che il mondo è complicato e le reti sono intricate. Apple deve poter contare sulla produttività unica al mondo delle fabbriche cinesi, per poter continuare a crescere con i numeri attuali. Le fabbriche cinesi le ho viste di persona, venti e passa anni fa, come giornalista, in occasione del ventennale della fondazione di [Acer](https://www.acer.com/ac/it/IT/content/home).

Notato niente? Acer è *taiwanese*. Nei discorsi ufficiali, la Cina considera Taiwan parte del proprio territorio, che si riprenderà con le buone o con le cattive. Il governo taiwanese, nelle manifestazioni più gentili dei cinesi, è illegittimo.

Eppure già negli anni novanta Taiwan apriva fabbriche in Cina, con il pieno favore delle autorità. In quelle fabbriche, già allora, veniva prodotta tutta la tecnologia informatica occidentale. Ho visto pile di portatili HP pronte per la spedizione oltreoceano. Sì, pile di computer HP, prodotte nelle fabbriche Acer. E pile di PowerBook Apple accanto a quelle di portatili HP. Tutti uguali perché prodotti nelle stesse fabbriche? No, tutti diversi, perché prodotti con disciplinari differenti. La differenza non la faceva la catena di montaggio ma la precisione, le tolleranze, la qualità (e il costo) del personale al lavoro, la qualità delle materie prime, la meticolosità (o meno) del controllo finale.

Ma divago. Voglio dire che il mondo aveva le sfumature di grigio già allora e i cretini sputavano sentenze già allora. Taiwan dava vitto, alloggio, istruzione, formazione professionale e stipendio a persone i cui figli o nipoti, in uniforme dell’[Esercito Popolare di Liberazione](https://it.wikipedia.org/wiki/Esercito_Popolare_di_Liberazione), domani andranno a “liberare” in un modo o nell’altro Taiwan stessa.

L’impegno che Apple può mettere nella privacy a Nanchino è diverso da quello che può mettere a Cupertino. Contraddizione? Realtà delle cose. I nuovi, straordinari system-on-chip M1 sono fabbricati da [Tsmc](https://www.tsmc.com/english). Tsmc è taiwanese. Tsmc ha aperto fabbriche in Cina, esattamente come accadeva venticinque anni fa, a casa del lupo cattivo che aspetta solo di poterla mangiare. Il mondo è in scala di grigio. Apple non è il vitello dai piedi di balsa, semmai di bambù ed è il caso di seguire la vicenda con attenzione; non come tanti vitelli dai piedi tonnati che vogliono solo un’occasione per parlarne male, anche se a sproposito.

<iframe width="560" height="315" src="https://www.youtube.com/embed/kcxz53LgnPY" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

*Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile.*               