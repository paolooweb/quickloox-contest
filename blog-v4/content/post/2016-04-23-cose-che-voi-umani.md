---
title: "Cose che voi umani"
date: 2016-04-23
comments: true
tags: [Windows, mouse]
---
Posso essere fuori dal mondo, vivere in una bolla, ignorare la realtà. Accetto qualsiasi critica.<!--more-->

Nondimeno, ho visto comprare un mouse a Media World. Tradizionale, ancora con il cavetto, un normalissimo mouse Trust a prezzo onesto.

Ho visto collegare il mouse a un portatile vecchiotto ma neanche tantissimo, con Windows 7. Che non è Windows 10, ma nemmeno Windows Xp.

Il portatile è rimasto inattivo per almeno mezzo minuto. Poi ha iniziato una ricerca su Windows Update per installare il driver adeguato al mouse.

Sono passati *diversi minuti*. Per installare un *mouse*. Alla fine la procedura si è completata con successo.

Per quei minuti sono rimasto a bocca aperta, incredulo. Su un OS X, anche vecchio di due o tre generazioni, quel mouse avrebbe funzionato istantaneamente.

Esistono persone disposte a tollerare un’attesa di minuti per installare un mouse, nel 2016. Sono cose che vedo, ma da solo non riuscirei a immaginare.

E chissà che altro sopportano.
