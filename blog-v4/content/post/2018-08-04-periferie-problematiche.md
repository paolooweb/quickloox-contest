---
title: "Periferie malfamate"
date: 2018-08-04
comments: true
tags: [Fortnite, iOS, Android]
---
Il sindaco Giuliani affrontò il risanamento di New York in base al principio della finestra rotta: se tolleri il piccolo degrado, sarà seguito da degrado sempre più grande. Devi intervenire. sul degrado usando tolleranza zero.

Ė la cosa cui ho pensato nel leggere che Epic Games [venderà Fortnite su Android senza passare da Play Store](https://arstechnica.com/gaming/2018/08/fortnite-on-android-may-drive-its-battle-bus-past-googles-30-cut/). Su iOS il gioco va che è una bomba e incassa ogni mese decine di milioni di dollari; su Android il trenta per cento dei proventi appare a Epic una tassa sproporzionata per il servizio offerto.

Installare software Android senza Play Store richiede l'indebolimento della sicurezza. Ci sono già casi di persone truffate attraverso malware che è riuscito a installarsi su sistemi meno protetti del dovuto.

Probabile che qualcuno, specialmente ragazzi giovani, sfrutterã l’occasione per scaricare senza Play Store altro software di dubbia provenienza. È così moltiplicare il rischio.

Oggi un vetro rotto, domani il buco nel muro, l’acquedotto, l’ascensore, l’immondizia, i trasporti pubblici. Man mano che guasti e vandalismo aumentano, vederli diventa abituale e a un certo punto si danno per scontati. Il quartiere residenziale diventa poco raccomandabile. Le case perdono valore e la malavita inizia a delimitare le zone di spaccio. Eccetera. Mentre studi autorevoli dicono che mettere un vetro nuovo aumenta la probabilità che il quartiere resti vivibile.

Android, la periferia malfamata del *mobile*.
