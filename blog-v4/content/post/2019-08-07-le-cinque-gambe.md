---
title: "Le cinque gambe"
date: 2019-08-07
comments: true
tags: [Apple, Mac, iPad, iPhone, watch, AirPods]
---
Chi si ricorda che Apple basava troppo del proprio fatturato su iPhone e che quindi rischiava la dannazione una volta sparita la gallina dalle uova d’oro, ha avuto la conferma che si trattava di aria al vento. Nell’ultima relazione trimestrale, iPhone [è passato dal cinquantasei al quarantotto percento](https://twitter.com/macjournals/status/1156313669153841159?s=20) del business aziendale.

<blockquote class="twitter-tweet"><p lang="en" dir="ltr">A year ago, iPhone revenues were 56% of Apple’s Q3 revenues. This year they are 48%.</p>&mdash; MacJournals.com (@macjournals) <a href="https://twitter.com/macjournals/status/1156313669153841159?ref_src=twsrc%5Etfw">July 30, 2019</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

Niente fuoco eterno, perché il fatturato, per quanto di un banale uno percento, [è cresciuto](https://www.apple.com/newsroom/2019/07/apple-reports-third-quarter-results/).

Le altre attività strategiche di Apple hanno di conseguenza guadagnato spazio e importanza. Ricordiamo, sono quelle che Apple trascura mentre pensa solo a iPhone. Eppure, misteriosamente, giocano un ruolo più significativo di prima.

Una volta l’azienda era quella che faceva solo Macintosh; una gamba sola. Effettivamente rischiava.

Oggi [le gambe sono cinque](https://2672686a4cf38e8c2458-2712e00ea34e3076747650c92426bbb5.ssl.cf1.rackcdn.com/2019-07-30-15-40-34.jpeg): iPhone, i Servizi, Mac, iPad e il Resto (AirPods, watch, tv). Notare che la terza gamba in ordine di importanza è Mac, quella che Apple trascura da anni con effetto della fuga di professionisti eccetera eccetera. Se la si vuole vedere dalla parta opposta, si può naturalmente sostenere che Apple pensi solo a iPhone e Servizi e trascuri Mac. C’è sempre un modo di travisare i fatti.

La sostanza è che Apple è rimarchevolmente in salute e gioca bene in settori che crescono ma anche dove c’è una contrazione o una saturazione. Chi ragiona per quote di mercato o per andamenti delle vendite capisce nulla di quello che accade. Come corso accelerato, suggerisco la [lettura di Jean-Louis Gassée](https://mondaynote.com/good-news-apple-no-longer-is-the-iphone-company-4f023cdd5c53):

>[Il declino nella vendita di iPhone] è il segnale di un business in maturazione ordinata che trae vantaggio dagli effetti di rete generati dal proprio ecosistema.

Se poi si vuole pensare che Gassée sia un *fanboy* o uno *yes-man* al soldo di Apple, vabbeh, vale tutto.