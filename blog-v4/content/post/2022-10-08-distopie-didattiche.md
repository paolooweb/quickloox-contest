---
title: "Distopie didattiche"
date: 2022-10-08T00:46:36+01:00
draft: false
toc: false
comments: true
categories: [Hardware, Software, Internet]
tags: [PowerBook G4, PowerBook 12”, Lidia, CD-ROM, Jerry Pournelle, Byte]
---
Scuola 2022: il libro di inglese di Lidia ha una edizione online.

Vita 2022: se le piace, forse magari potrebbe anche darsi che Lidia frequenterà una scuola di musica. Il libro del corso non ha una edizione online.

Entrambi i libri hanno un Cd-Rom allegato.

A casa mia sono operativi due Mac, un iPad Pro, due iPhone, una tv.

Aggiungiamo macchine vecchie non più in uso, che hanno ancora qualche applicazione, ne troveranno una oppure non abbiamo ancora deciso di smaltire. Tra questi, un Mac mini Intel, un iPhone 5, un iPhone 4, un iPad prima generazione.

Siamo arrivati al 2010 e nessuna di queste macchine contiene un lettore CD.

Non c’è bisogno di comprarne uno: abbiamo in casa anche un venerabile PowerBook G4 12”.

Così oggi ho copiato sul suo disco rigido (di quelli che girano, con la superficie magnetizzata…!) i materiali del CD-ROM di inglese. Roba modernissima: una cartella di file Mp3.

Sempre con il PowerBook 12” ho rippato il CD del corso di musica, che è in effetti un CD audio. Però l’ho fatto senza guardare e non mi sono accorto che il il CD audio in questione contiene tracce senza titoli, non ha una copertina… insomma, è un contenitore di file Aiff. Mi tengo il rippaggio ma intanto ho copiato su disco rigido anche questi file.

Il PowerBook 12” vede la rete Wi-Fi (ah, l’obsolescenza programmata che penalizza i computer troppo vecchi, Apple cattiva non vuole farteli usare), per cui senza stare troppo a pensare ho attivato la condivisione file e nel giro di qualche minuto avevo le due cartelle di file audio pronte sul Mac mini M1 di riferimento in casa. Da lì basta AirDrop e i file arrivano su qualunque altra macchina. Oppure li si riproducono sul televisore, via tv, o persino sulla radio Tivoli, frutto della raccolta punti del supermercato, che sta nello spazio cucina.

Per dire che non abito in una versione aggiornata della [Chaos Manor di Jerry Pournelle](https://macintelligence.org/posts/2021-05-20-le-recensioni-che-volevo/) che affascinava certi lettori di *Byte* un fantastilione di anni fa; la dotazione informatica è forse superiore alla media, ma assolutamente qualunque.

Eppure, di fronte alla scuola di oggi, si fa la figura dell’astronave Enterprise in viaggio verso l’infinito e oltre.

Sono il primo a lamentarmi di certe scuole dotate, si fa per dire, di software antidiluviano. A parte rimboccarsi le maniche visto che Linux fa miracoli su hardware preistorico (ma anche qualche BSD non farebbe male), è guardare gli alberi senza vedere la foresta.

Le macchine non sono vecchie; sono perfettamente in linea con il modo di pensare complessivo della scuola, al netto delle solite eccellenze che fanno da eccezione alla regola.

E aggiornare cervelli, ministri, istituzioni, regolamenti, disegni di legge, posizioni di rendita è ben più difficile che fare finalmente al meno dei CD per erogare materiale non curato che potrebbe benissimo stare su un serverino qualsiasi.