---
title: "Come eravamo"
date: 2014-10-13
comments: true
tags: [interfacce]
---
Sono sempre più allergico ai segnalibri di Safari. Un tempo catalogavo tutto e finivo per usarli niente, sempre superati da cose nuove o trascurati al punto da dimenticarne l’esistenza e quindi lo scopo.<!--more-->

Un nuovo segnalibro per me è un piccolo evento; l’ho festeggiato in occasione di questa [rassegna delle interfacce grafiche di sistema operativo dal 1981 al 2009](http://www.webdesignerdepot.com/2009/03/operating-system-interface-design-between-1981-2009/). Anche solo per scorrerla velocemente e rendersi conto di quanto tutto è rimasto uguale, e niente è rimasto uguale.