---
title: "Una sveglia indiscreta"
date: 2022-06-04T13:25:16+01:00
draft: false
toc: false
comments: true
categories: [Hardware]
tags: [Mi Smart Clock, Xiaomi, Apple, Android, privacy, YouTube Music]
---
La domotica avanza e mi ritrovo arretrato rispetto alle sveglie intelligenti, che si configurano con una app e una connessione Internet.

Nel mio caso, per conto terzi, uno (Xiao)Mi Smart Clock. Bell’oggetto, gradevole al tatto, bel display, funzioni confortevoli, personalizzazione, nulla da dire. Ecco, la visualizzazione a ventiquattro ore è macchinosa e strana, posto che abbia capito davvero come impostarla. Non importa veramente a nessuno. Se la avessi sul comodino, andrebbe benissimo a dodici ore. È simpatica, amichevole, piacevole da azionare.

Xiaomi vuol dire Android, Android vuol dire Google e ciò su cui veramente c’è qualcosa da dire è la privacy. Avevo già provato questo shock culturale con l’acquisto di una stampante moderna di HP, ma qui siamo un livello sopra. Se parliamo di oggetti la cui funzione non è più svolgere il proprio servizio ma profilare e cumulare dati con cui il produttore potrà poi ulteriormente monetizzare, rientrano entrambi nello stesso insieme.

HP tuttavia sta nel proprio ambito; sorveglia le stampe, l’acquisto degli inchiostri, addirittura impone praticamente l’uso di una app tragicamente inadatta a un utilizzo di famiglia, come se le stampanti entrassero in casa al pari dello spazzolino da denti.

Qual è l’ambito di una sveglia, invece?

Vuole sapere in che stanza si trova, quali altri oggetti capaci di Bluetooth siano presenti. Vuole conoscere gli altri membri della famiglia, raccogliere dati sul funzionamento del software (e ancora) ma anche dati *sulla configurazione delle sveglie*. Una delle mire di Google è sapere chi sta dormendo, quanto, a che ora si alza e in che stanza si trova quando parla con la sveglia. La quale si interfaccia con YouTube Music e quindi è in grado di farti ascoltare musica, che userà per scoprire i gusti, proporre altro, associare quei gusti ad altri tratti che indicano un utente *x* cui proporre pubblicità *y*.

Sono tutte cose risapute, solo che vederle passare in configurazione fa impressione. L’intento è chiarissimo per chi sa, innocente per chi non sa. La configurazione è fintamente tutelatrice; chi voglia passarsi opzioni, form, alternative varie può trovarsi alla fine con un apparecchio ragionevolmente riservato. Chi non ci bada, il novantanove percento, dà alimentazione a un sorvegliante che ascolta conversazioni, registra ritmi di sonno, indaga sul resto della casa, trasmette a Google, tutto.

Ne ho già sentite di persone che scrollano le spalle, ma che sarà mai, chi se ne frega. C’è una questione: il motore di ricerca, il browser, le app di produttività individuale, le mappe sono gratis. In cambio Google vuole profilazione e il baratto può suonare inquietante, ma almeno è evidente.

[Mi Smart Clock](https://www.mi.com/it/mi-smart-clock/) costa cinquanta euro* e oltre a questo è una stazione di sorveglianza, che genera profilazione, che genera denaro. L’acquisto dell’hardware è quindi solo un acconto o una quota parte del costo vero.

Prima o poi cominceremo a identificare il costo reale degli apparecchi con la somma delle spese visibili e quelle nascoste. Per ora, quando uno dice che Android costa meno, evidentemente ignora l’invisibile.

Apple non ha una sveglia, ma configurare uno HomePod, a confronto, è trovarsi con la ciambella e il Martini da soli nella piscina di un’isola felice e soprattutto privata.

*Prezzo ufficiale sul sito. Sul mercato la volatilità della quotazione è molto alta; l’ho trovato sotto i quaranta euro e sopra i sessanta. Non è il caso di fare un acquisto di impulso.
