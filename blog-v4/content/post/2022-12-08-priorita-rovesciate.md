---
title: "Priorità rovesciate"
date: 2022-12-07T23:56:14+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [Excel, Miro]
---
I paradossi della vita. Hai davanti un team di poco più che ventenni, laureandi, laureande, con una formazione non banale, emancipati, consapevoli, impegnati, avanti di chilometri rispetto ai loro coetanei impegnati per discoteche e apericena.

Ti aspetti gente curiosa e affamata e folle un po’ in stile Steve Jobs, con l’attitudine all’esplorazione, al tuffo nell’ignoto, alla scommessa, alla ribellione più o meno anarchica che sia dentro o fuori dal sistema. Invece stanno fermi, non ci provano nemmeno.

Ma se gli dici che esiste [Miro](https://miro.com/) e glielo fai provare, se gli fai superare la barriera dell’ovvio, in dieci minuti hai attivato una macchina da brainstorming che genera idee al ritmo di una rotativa. Sono pieni di energia, solo che nessuno li ha forniti di un interruttore. Devi trovare i cavi da mettere in contatto e, se si accendono, sono fuochi di artificio.

Poi ti confronti con un team di professionisti, mediamente di mezza età, la media della media. Persone rotte a ogni esperienza professionale e informatica, che le hanno viste tutte, usano il computer dal secolo scorso, hanno visto avvicendarsi sistemi, soluzioni, metodi, servizi, applicazioni, pacchetti, la qualunque. Dovrebbero avere paura di niente, fare spallucce se appare qualcosa di nuovo o di imprevisto: come fa a essere veramente nuova, qualsiasi cosa, di fronte a gente che ha visto tutto?

Invece hanno una paura folle. Stanno abbarbicati al loro foglio elettronico coperta di Linus, al loro glaciale programma di videoconferenza che trasforma qualsiasi riunione in una veglia funebre, al riflesso condizionato, alla coazione a ripetere. Guai a sbandare, a prendere per una volta la biforcazione meno nota. Leoni nel business, e chi li frega, quanto meringhe nel software, si sbriciolano se appena stringi troppo e ti si appiccica tutto in bocca.

I pieni di vita che non osano anche se potrebbero, i pieni di esperienza che guai a farne tanta così di più, come se fosse un male, un’eresia, una perversione.

Le priorità vanno al contrario in questo assurdo mondo dove esiste un [campionato di Excel](https://www.fmworldcup.com/excel-esports/microsoft-excel-world-championship/). Esattamente come esistono un [campionato di incantatori di vermi](https://patrickcox.wordpress.com/tag/world-worm-charming-championships/) o un [campionato di sauna](https://www.thetimes.co.uk/article/the-silly-sauna-event-that-became-a-tragedy-0hfqxw5nkb6).

Ecco perché rovesciare nuovamente le priorità rovesciate. Ricorrere a software diverso, coltivare il non familiare, sparigliare. È il modo di rimanere giovani quando anche i giovani nascono vecchi.