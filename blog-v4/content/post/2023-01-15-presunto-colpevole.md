---
title: "Presunto colpevole"
date: 2023-01-15T01:16:55+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [Dr. Drang, Drang, Wordle]
---
Nella famiglia di Dr. Drang lo accusano di avere barato a [Wordle](https://macintelligence.org/posts/2022-01-12-un-bel-wordle-dura-poco-ma-apre-un-mondo/): dopo avere ricevuto i*ra*te al primo tentativo e p*a***t**ch al secondo, [trova la soluzione al terzo giro](https://leancrew.com/all-this/2023/01/not-cheating-at-wordle/), per giunta contenente una lettera ripetuta.

Lui non si scompone e, grazie a `egrep`, mostra come la sua terza parola fosse pressoché inevitabile a fronte delle ventuno alternative contenute nel file delle soluzioni accettate.

In una riga sola di espressione regolare, mica tre. Innocente e pure *master of scripting*.