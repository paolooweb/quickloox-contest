---
title: "Un anno a sproposito"
date: 2023-01-01T05:33:07+01:00
draft: false
toc: false
comments: true
categories: [Internet]
tags: [2023]
---
Per questo 2023 niente propositi e invece attenzione a facilitare qualche proposito di altri, che siano familiari o stiano in un altro continente.

Niente indica che l’anno nuovo sarà minimamente più normale degli altri anni venti vissuti finora. Firmerei comunque per un 2023 che preparasse un 2024 più normale e persino migliore. È possibile.

Aiutiamoci a realizzare i nostri non propositi (e gli spropositi) con i nostri aggeggi di fiducia, su una scrivania, in tasca, al polso, e convinciamo gli aggeggi a lavorare meglio, liberandoci tempo e pensieri, che fanno comodo.

A tutti un abbraccio e per ciascuno uno sguardo nuovo e fiducioso sui giorni che arrivano. Grazie.