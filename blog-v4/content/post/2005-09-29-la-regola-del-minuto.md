---
title: "La regola del minuto"
date: 2005-09-29
comments: true
tags: [supporto, assistenza, regola, minuto]
---
C’è un minimo sindacale anche per le richieste di assistenza.<!--more-->

Ogni tanto sento dire *però se uno non è esperto ha tutto il diritto di chiedere*.

Certamente. A patto che rispetti la Regola del Minuto.

È semplicissima. Per un minuto (un minuto, non un’ora) si ha l’obbligo morale di concentrarsi totalmente sullo schermo e guardarlo con attenzione. Se non c’è alcun messaggio che riguardi il problema, nessun pulsante da cliccare, nessun menu che sembra utile, allora si può chiedere.

Chi non rispetta la regola del minuto, lo dico con chiarezza, se ne approfitta.

E sbaglia, perché da quando l’ho adottata con quanti mi chiedono una mano, improvvisamente un quarto dei problemi si risolve da solo.

Posso dare un’ora di attenzione in cambio di un minuto. Ma questo lo esigo.
