---
title: "Sostiene Vignoli"
date: 2015-05-24
comments: true
tags: [Vignoli, opensource, Microsoft, LibreItalia]
---
Alcuni mi ritengono un tifoso o un fanatico, con Apple per partito preso oppure contro Microsoft sempre per partito preso. Non ci perdo il sonno e comunque preoccuparsene sarebbe inutile. Si può avere su me qualsiasi opinione.<!--more-->

Non su [Italo Vignoli](http://www.italovignoli.org). Italo ha un passato e un presente di libertà intellettuale e operativa tale che nessuna persona capace di intendere e volere può metterlo in dubbio senza esporsi al ridicolo.

Italo ha scritto una [lettera aperta sulla cittadinanza digitale](http://www.libreitalia.it/lettera-aperta-sulla-cittadinanza-digitale/) che va letta, perché nel terzo millennio si è cittadini degni del titolo solo se si capisce il ruolo del software libero e possibilmente si muove anche solo un dito mignolo del piede per favorirne la diffusione.

Ce n’è per tutti e io mi limito a riprodurre un paio di paragrafi che mi toccano in modo particolare. Invito comunque ognuno a leggere l’intera lettera, perché appunto condividiamo la medesima responsabilità, anche se magari in misure diverse.

>Scrivo a te, Microsoft, perché continui a fare innovazione assumendo avvocati e lobbisti, per difendere le posizioni “novecentesche” che ti hanno permesso – con la complicità dei politici – di creare un impero che condiziona non solo le scelte economiche, perché questo sarebbe il meno, ma le scelte culturali dei cittadini, per tenerli ancorati al passato invece che proiettarli verso il futuro, e proteggere solo i tuoi interessi economici.

>Scrivo a te, dirigente scolastico, perché accetti in modo acritico la sponsorizzazione della tua associazione da parte di Microsoft, facendo finta che si tratti di un supporto logistico e non di un elemento della strategia “embrace extend extinguish” (abbraccia allarga annichilisci), e quindi abdichi al tuo prezioso ruolo istituzionale per diventare uno strumento nelle mani di chi va contro gli stessi interessi che tu dovresti rappresentare (e persino difendere).

Qualunque opinione si abbia su Italo, è impossibile definirlo fanatico. Né ha mai preso una decisione per partito preso. Quando sostiene qualcosa, è perché conosce solidamente l’argomento.
