---
title: "Il digitale tornasole"
date: 2021-06-11T02:16:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [Immuni, Carlo Canepa, Linus Torvalds] 
---
Uno splendido articolo in italiano, documentato, approfondito, comprensibile:  [Chi ha ucciso l’app Immuni e perché](https://www.italian.tech/2021/06/07/news/chi_ha_ucciso_immuni_e_perche_-304210182/). Si avvicina il fine settimana e c’è il tempo per fare qualcosa oltre la solita routine. È leggere questo articolo.

In perfetta congiunzione astrale, Linus Torvalds, il creatore di Linux e una persona che non le manda a dire, si è espresso in [termini che per lui sono moderati](https://lore.kernel.org/ksummit/CAHk-=wiB6FJknDC5PMfpkg4gZrbSuC3d391VyReM4Wb0+JYXXA@mail.gmail.com/) su una mailing list degli sviluppatori Linux, a proposito di un commento antivaccino:

> Tieni cortesemente per te i tuoi commenti antivaccino folli e tecnicamente scorretti.

> Non sai di che cosa stai parlando, non sai che cosa sia l’mRNA e stai diffondendo falsità idiote. Può essere che tu lo faccia inconsapevolmente, a causa di una cattiva istruzione. Oppure, perché hai parlato con “esperti” o guardato video di ciarlatani che non sanno di che cosa parlano.

Una coincidenza interessante: due casi di ostilità verso la scienza e verso il digitale. Solo persone ignoranti possono avere agito in consapevolezza per sabotare Immuni, ignoranti al pari di chi approfitta di uno spazio di discussione specialistico per spargere malainformazione.

Il digitale va usato con cura e consapevolezza; tende comunque, sia pure con le dovute eccezioni, ad accompagnarsi al progresso. E tende a rivelare stupidi e incapaci, come una cartina di tornasole l’acido o il basico.

*Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile.*