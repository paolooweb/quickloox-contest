---
title: "Sudditanza psicologica"
date: 2018-09-22
comments: true
tags: [App, Store, Huawei, iPhone, Samsung, Sculley, Odyssey]
---
Huawei provoca le persone in fila per l’acquisto di iPhone, con la [distribuzione di batterie supplementari]()https://appleinsider.com/articles/18/09/21/huawei-trolls-apple-by-handing-out-power-banks-to-customers-waiting-in-line-for-an-iphone-xs accompagnate dal messaggio *Ti serviranno*.

Samsung [non si tira indietro](https://www.zdnet.com/article/samsung-mocks-apple-again-this-time-its-poking-fun-at-iphones-throttling-troubles/), quando c’è da sfottere Apple.

Sono anche cose divertenti che fanno parlare, però spostano poco o niente in termini concreti.

[Odyssey](https://www.amazon.it/Odyssey-John-Sculley/dp/0006383432) è del 1987, trentuno anni fa. John Sculley non passerà alla storia come il migliore amministratore delegato di Apple, ma nel libro racconta come arrivò a un passo dalla vetta di un gigante quale Pepai: inventò una strategia che cambiava le regole del gioco e colse impreparata Coca-Cola. Fu così che il numero due riuscì in clamoroso sorpasso.

Distribuire batterie non cambia niente. Samsung vende quasi sempre più terminali di Apple. Secobdo me i loro dirigenti marketing non hanno letto *Odyssey*.

E continuano a giocare contro il numero uno con le regole create da quest’ultimo. Tutto resta com’è e, nello schermo, continui a riconoscere al numero uno il suo ruolo.

Sudditanza psicologica.
