---
title: "Ti cambiano la vita"
date: 2017-01-17
comments: true
tags: [Flavio, AirPods]
---
Scrive (e ringrazio!) **Flavio**.

**Negli ultimi anni ho avuto modo di collaudare e provare diversi tipi di auricolari per iPhone.** Per il tipo di attività che pratico, salgo e scendo dall’auto decine di volte al giorno, ho necessità di togliere e mettere l’auricolare più e più volte.

Ebbene, ne ho provati di tutti i tipi. Con cavo, senza cavo, sportivi, doppi, singoli, di elevata qualità, di scarsa qualità ma dalla forma ergonomica, insomma di ogni. E mai, dico, mai ne ho trovato uno che rispondesse alle mie esigenze. Quelli Bluetooth, quando li togli, restano abbinati al telefono. Quindi se scendi dall’auto e te lo metti in tasca senza spegnerlo, una telefonata in arrivo viene passata ancora all’auricolare anche se ce l’hai in tasca. Se lo spegni, quando poi lo rimetti, lo devi riaccendere e aspettare che si riabbini.

Quelli col cavo, beh, togli e metti il cavo. Poi si impiglia, poi è scomodo, poi si rompe.

Beh, Lucio, gli [AirPods](http://www.apple.com/it/airpods/). In questi giorni di utilizzo quotidiano, ho capito che ti cambiano la vita. O quantomeno, hanno cambiato la mia.

Intanto, appena arrivati si sono abbinati da soli a iPhone senza che nemmeno me ne accorgessi. Negligentemente non ho guardato le istruzioni, ma inavvertitamente ho aperto la custodia vicino ad iPhone e… miracolo: abbinati e iPhone mi diceva già lo stato di ricarica di ciascun auricolare e della custodia ricarica. Ma, come dicevo, è l’uso di tutti i giorni che è davvero sconvolgente. Gli AirPods si accorgono da soli quando sono in posizione nell’orecchio, quindi quando li metti si attivano e quando li togli si spengono.

Se li metti e arriva una chiamata, parli con gli AirPods. Se li togli (vale anche per un solo AirPod) e arriva una chiamata, rispondi da iPhone.

Il tutto senza dover fare assolutamente niente.

Letteralmente MAGICO.

La conversazione, poi, ha dell’incredibile. Per la PRIMA VOLTA nella mia vita, quando parlo con l’auricolare il mio interlocutore non se ne accorge. La gestione del full duplex è perfetta e con essa la riduzione del rumore. Diversi insospettabili interlocutori, anche esigenti, NON SI SONO ACCORTI che hanno intrattenuto una lunga e articolata conversazione telefonica con me tramite un auricolare.

Doppio tap sulla parte superiore dell’auricolare e Siri si mette in ascolto: non sbaglia una virgola, sente solo te e non si cura di rumori estranei come gente che sta conversando vicina o il conduttore del giornale radio. E puoi dirgli di chiamare questa o quell’altra persona oppure, per buona pace dei detrattori che sostengono manchi l’apposito tasto, ordinare di passare al prossimo brano durante l’ascolto della musica.

La musica, appunto. Non sono un esperto di qualità audio, ma gli AirPods calzano perfettamente nell’orecchio e sono leggerissimi tanto che quando inizia la riproduzione,  di eccellente qualità secondo il mio modesto e inesperto parere, sembra che l’audio arrivi dal nulla direttamente al cervello.

E infine, quando non li usi, li metti nella custodia. Più piccola di uno Zippo, sta ovunque in tasca. E te li ricarica.

In conclusione, sono davvero gli auricolari che mancavano. Completi, intelligenti, ben studiati e realizzati. Sostituiscono in un colpo solo le cuffie per la musica, il viva voce dell’automobile e l’auricolare *hands free*. **Ti cambiano la vita.**