---
title: "Non tutto il bene"
date: 2019-09-14
comments: true
tags: [iPhone, Macworld]
---
Sarà davvero una buona cosa, la causa dell’articolo su *Macworld* intitolato[Apple fa durare più a lungo i propri iPhone. Ed è una buona cosa](https://www.macworld.com/article/3438636/apple-is-making-its-iphones-last-longer-thats-a-good-thing.html)?

Intendo dire che uno potrebbe scoprire, tipo, una nuova funzione di iOS 13 per [regolare il caricamento della batteria secondo le abitudini del proprietario](https://www.macworld.com/article/3408336/ios-13s-new-optimized-battery-charging-feature-explained.html), in modo da ridurre il degrado della batteria stessa nel tempo e così aumentarne la vita utile.

È che là fuori abbiamo gente con il bisogno di credere nell’obsolescenza programmata. È recente un [articolo di Wired](https://www.wired.it/mobile/smartphone/2018/10/30/obsolescenza-programmata-apple-samsung/) senza capo né coda, che la mette nel titolo per acchiappare clic da Google e poi, nel testo, gira in tondo attorno alla questione buttando banalità sul ventilatore.

Già hanno problemi a dribblare la realtà; rendergliela ancora più impegnativa è una cattiveria.
