---
title: "File e veglie"
date: 2014-09-05
comments: true
tags: [Apple, BusinessWeek]
---
Facile [accorgersi](https://twitter.com/dcbandor/status/507132958307004416) della gente che si mette in coda davanti ad Apple Store una settimana prima senza neanche sapere che cosa verrà presentato.<!--more-->

<blockquote class="twitter-tweet" lang="en"><p>Apple Store on 57th &amp; 5th. Ever seen a line start 6 days early for an Android phone? <a href="https://twitter.com/tim_cook">@tim_cook</a> <a href="https://twitter.com/MusaTariq">@MusaTariq</a> <a href="http://t.co/eKdkGFAKAG">pic.twitter.com/eKdkGFAKAG</a></p>&mdash; Dan Benton (@dcbandor) <a href="https://twitter.com/dcbandor/status/507132958307004416">September 3, 2014</a></blockquote> <script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>

Pochi invece pensano (bravi a *BusinessWeek* a [puntualizzarlo](http://www.businessweek.com/articles/2014-09-04/for-iphone-6-defects-apple-has-failure-analysis-team-ready)) alla pattuglia di ingegneri in allerta giorno e notte, che analizza gli apparecchi restituiti dai clienti in cerca di difetti non individuati in produzione, con il compito di trovare soluzioni il più alla svelta possibile.

Niente di inedito e niente di nuovo, per qualsiasi azienda di prodotti. Solo che a farlo sui volumi di produzione e con i tempi di Apple – un giorno in più o in meno di fabbricazione di apparecchi difettosi equivale a milioni di dollari – sono veramente in poche.

Anzi, probabilmente lo fa un’azienda sola.