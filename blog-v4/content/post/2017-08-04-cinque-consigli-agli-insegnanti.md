---
title: "Cinque consigli agli insegnanti"
date: 2017-08-04
comments: true
tags: [Raspberry, Pi, Zero, Gutenberg]
---
Il luogo comune vuole che gli insegnanti facciano tre mesi di vacanza l’anno e certamente non ė vero; sono peraltro convinto che in una data come quella odierna, la gran parte degli insegnanti possa dedicare un quarto d’ora alla lettura della pagina [Cinque modi di usare Raspberry Pi in classe](https://opensource.com/article/17/8/raspberry-pi-teachers).

[Raspberry Pi](https://www.raspberrypi.org/) è la fine di tutti gli alibi. Ha un costo stracciato; si installa e si programma facile; incoraggia l’apprendimento trasversale di più discipline; funziona anche su computer-relitti che nessuno ha il coraggio di buttare; dispone di una quantità enorme di contenuti sviluppati da una comunità di appassionati, oltre che di supporto; ha tutto il software necessario per essere produttivi ed è software libero; gli ingombri sono ridicoli. Qualunque sia stata la scusa finora, non regge più.

Anche perché l’articolo, *dead simple*, mette a disposizione un percorso completo per partire e suggerimenti assai interessanti, per esempio la creazione di un server di eBook che possa erogare ai ragazzi testi liberi prodotti dal [Gutenberg Project](https://www.gutenberg.org/). Cui i ragazzi, per dire, potrebbero partecipare con la gioia dei docenti di italiano e di lingua straniera.

Tutto quanto qui illustrato è facilmente replicabile attraverso siti e iniziative in italiano. Preferisco lasciare i soli *link* alle risorse in inglese in quanto, nella scuola italianq di oggi, la seconda lingua costituisce il *pons asinorum* per i docenti. E voglio che tutti, per amore dei ragazzi o per forza della dignità personale, si trovino ad averlo passato o a volerlo fare. In caso disperato, accetto comunque richieste di aiuto per risorse in italiano.
