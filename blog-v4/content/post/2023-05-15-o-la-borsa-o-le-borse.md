---
title: "O la borsa, o le Borse"
date: 2023-05-15T01:08:31+01:00
draft: false
toc: false
comments: true
categories: [Internet]
tags: [Apple, Cnbc, Germania, Regno Unito, Francia, India]
---
Si parlava di [diversificazioni](https://macintelligence.org/posts/2023-05-10-tu-chiamale-se-vuoi-diversificazioni/) e di che cosa voglia dire nel 2023 una parola simile.

Si potrebbe pensare che mettere una quantità considerevole di portafoglio su Apple sia prendersi dei rischi, anche se lo fa Warren Buffett su scala multimiliardaria. Ma questo fa pensare che Apple sia solo una società come tante altre.

Da Cnbc fanno sapere che, al valore di capitalizzazione attuale che è intorno ai duemilasettecento miliardi di dollari, [Apple vale più dell’intero mercato azionario del Regno Unito](https://www.cnbc.com/2023/05/10/apple-vs-the-world-apples-bigger-than-entire-overseas-stock-markets-.html), che è il terzo al mondo.

Immediatamente dietro in classifica stanno Francia e India, mica due *peones*. Dietro ancora, c’è la Germania. *La capitalizzazione di Apple vale due volte l’intera Borsa tedesca*.

Il titolo Apple rimane unico, certo. Ma, da incompetente, viene da pensare che il suo valore venga spinto da forze che, per funzionare su queste dimensioni, debbano essere più globali e composite di quello che pensiamo.