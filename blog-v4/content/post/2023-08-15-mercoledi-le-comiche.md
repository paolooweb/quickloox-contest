---
title: "Mercoledì le comiche"
date: 2023-08-15T03:59:02+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [Zoom]
---
Zoom ha in vigore – e [vuole eliminare](https://www.bloomberg.com/news/articles/2023-08-10/zoom-ends-no-meeting-wednesday-policy-calling-it-barrier-to-collaboration) – una regola che vieta le riunioni interne di mercoledì.

Questo dalla società che briga per [limitare il tempo di lavoro da casa](https://macintelligence.org/posts/2023-08-09-remoto-passato/) dei dipendenti. La società che si è fatta un nome con il software per la videoconferenza.

La prossima sarà obbligare a tenere la webcam spenta? O abolire la chat? Se non ci fossero, andrebbero inventati. Argomento perfetto per il barbecue.

Buona festa!

*Gli aggiornamenti in agosto del blog potrebbero essere irregolari o infrequenti.*