---
title: "Lavorare nell’aerospaziale"
date: 2020-06-08
comments: true
tags: [SpaceX, Falcon, Musk, Tesla, Linux, C++, Chromium, JavaScript, HTML, CSS]
---
Leggo di uno [sciopero](http://www.flcgil.it/scuola/sciopero-8-giugno-2020-come-aderire.flc) previsto per domani, 8 giugno, da parte del personale scolastico.

Leggo (con pazienza, essendo chilometrica) la pagina di Reddit in cui sono comparse [migliaia di domande per lo staff di SpaceX](https://www.reddit.com/r/spacex/comments/gxb7j1/we_are_the_spacex_software_team_ask_us_anything/), l'azienda di Elon Musk – patron di Tesla – che ha dimostrato di poter abbattere i costi dei missili per le missioni spaziali e ora prepara lo sbarco sulla Luna.

Termini ricorrenti nelle risposte dei tecnici di SpaceX: Linux, Chromium, JavaScript, C++, Html, CSS.

Chiedo agli insegnanti in sciopero che cosa insegneranno ai loro alunni il prossimo anno in tema di informatica e, a quelli che non insegnano informatica, che tipo di formati e applicazioni faranno usare ai loro ragazzi per le ricerche, i compiti, gli elaborati.