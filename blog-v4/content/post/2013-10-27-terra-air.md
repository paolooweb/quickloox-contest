---
title: "Terra Air"
date: 2013-10-27
comments: true
tags: [Sony, Vaio, Flip, MacBookAir]
---
C’era una volta MacBook Air di cui ridevano tutti, così piccolo, così limitato. Su *Usa Today* [dicevano](http://usatoday30.usatoday.com/tech/columnist/edwardbaig/2008-01-23-macbook-air-review_n.htm)

>Con troppe poche porte, una batteria sigillata che non si può sostituire da soli e nessun lettore di Cd/Dvd, Air non è il portatile ideale per tutti.<!--more-->

(Esiste forse un portatile ideale per tutti?)

*Apc Magazine* elencava [i dieci difetti principali di MacBook Air](http://apcmag.com/macbook_air_top_10_things_wrong_with_it.htm) con questo *incipit*:

>Come una modella anoressica, il supersottile MacBook Air di Apple è sceso a troppi compromessi per il gusto di essere magro.

Oggi sono tutti a giocare il gran premio degli *ultrabook* (per i Pc ci vuole sempre una parola d’ordine).

E molti sono in ritardo pazzesco. *Ars Technica* ha esaminato Vaio Flip 13 di Sony per scrivere che [muore di mille ferite](http://arstechnica.com/gadgets/2013/10/review-sonys-vaio-flip-13-dies-the-death-of-a-thousand-cuts/2/):

>Non è questione di un unico enorme difetto ma di un elenco di piccoli problemi. Sembra buono sulla carta. Sembra buono anche faccia a faccia, ma il telaio che si flette, lo schermo che solleva il corpo del portatile dalla scrivania, la qualità un po’ granulosa dello schermo e il rumore delle ventole infastidiscono complessivamente a sufficienza per tenere Flip 13 fuori dall’alta classifica dei convertibili.

Consiglio soprattutto i commenti sulla durata della batteria e soprattutto i test di velocità di rete, dove i nuovi MacBook Air sono circa quattro volte meglio. Quattro volte. Se Air vola, Flip sta a terra.