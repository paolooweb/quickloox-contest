---
title: "Nel modo sbagliato"
date: 2022-10-23T03:09:39+01:00
draft: false
toc: false
comments: true
categories: [Hardware]
tags: [iPad Pro, iPad, Jobs, Steve Jobs, Antennagate]
---
Ai tempi dell’[Antennagate](https://macintelligence.org/posts/2010-07-19-la-presa-di-sale/) fece il giro del mondo il commento di Steve Jobs *lo stai tenendo nel modo sbagliato*. Oramai è diventato un meme, usato in mille situazioni.

I critici obiettavano che iPhone 4 dovessere essere impugnato a discrezione di ciascuno, indipendentemente dal fatto che l‘impugnare l’apparecchio diminuisce la ricettività dell’antenna e soprattutto che questo si applica a qualsiasi cellulare.

Ora, con il nuovo iPad base, si affaccia una corrente di pensiero che vuole la telecamera di iPad [messa sul lato lungo](https://birchtree.me/blog/the-ipad-is-a-landscape-first-device-in-2022/) dell’apparecchio, con la giustificazione che sempre più lo si usa sulla scrivania, collegato a una tastiera, e sempre meno in mano.

Sulla scrivania uso un Mac, fortunato come sono a poter lavorare con due macchine. In giro uso iPad e la comodità di tenerlo in verticale fa la differenza, anche per Face ID, per scattare foto, per scandire un documento.

Sono felice e soddisfattissimo di tenere iPad nel nuovo modo sbagliato e spero di poterlo fare il più a lungo possibile.