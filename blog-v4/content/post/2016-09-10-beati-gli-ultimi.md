---
title: "Beati gli ultimi"
date: 2016-09-10
comments: true
tags: [AirPods, keynote, Sierra]
---
Molte scuse per chi [mi avesse aspettato](https://macintelligence.org/posts/2016-09-07-a-tutto-keynote/). Sono successe molte cose, prima fra tutte l’esplosione della *beta* di Sierra. Avevo un Time Machine tirato a lucido perché queste cose succedono ed ero preparato. Purtroppo si sono aggiunte complicazioni hardware non prevedibili e sono tornato in configurazione standard solo oggi. Sto guardando il [keynote](http://www.apple.com/it/apple-events/september-2016/) ora (!).<!--more-->

Poi ovviamente [ho avuto le papille gustative interrotte](https://www.youtube.com/watch?v=FbPtvFxUb60), il gomito ha fatto contatto col piede e mio padre è rimasto incastrato nell’autolavaggio.

Ho visto però abbastanza per comprendere e condividere quello che mi ha scritto **Matteo**, al momento il miglior commento possibile di cui dispongo.

**Vorrei dirti che, per quanto sia iPhone che Watch abbiano ottenuto aggiornamenti significativi,** se non necessari, la mia curiosità è andata principalmente sugli [auricolari](http://www.apple.com/it/airpods/), che credo si possano definire gli auricolari per il nuovo secolo, se il nuovo secolo non fosse già' sedicenne…

In attesa di poterli provare e valutare, vorrei solo portare la tua attenzione sulla Apple che non innova. Cantilena che Apple smentisce continuamente, e qui il mio pensiero va al *concept* (perché capisco che l’hardware abbia bisogno di essere aggiornato) del Mac Pro cilindrico, ad Watch (ma qui sono di parte, perché come già ti scrissi, ha notevolmente migliorato la mia vita) e, da ultimo, a questi auricolari che, per il semplice fatto di contennere il nuovo chip W1, fanno pensare a dove potrebbero arrivare…

Direi che, per continuare la metafora, rischiamo di avere prima o poi un computer da orecchio!

Inviato da iPad, ma **scritto con una Apple Wireless Keyboard del 2007.**

Se non fossi incappato nella piccola disavventura delle ultime settantadue ore avrei scritto prima di leggere Matteo. Quindi sono contento così.