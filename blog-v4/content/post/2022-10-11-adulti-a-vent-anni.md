---
title: "Adulti a vent’anni"
date: 2022-10-11T02:08:21+01:00
draft: false
toc: false
comments: true
categories: [Internet]
tags: [All Apple Museum, Savona]
---
Non riesco a credere che All About Apple Museum [festeggi sabato prossimo, 15 ottobre, nella sede di Savona, i venti anni di attività](https://www.allaboutapple.com/2022/10/il-15-ottobre-celebriamo-il-20-anniversario-di-all-about-apple/).

La solita frase banale: sembra ieri. Sembra ieri che una incredibile collezione amatoriale di hardware e software (principalmente) Apple trova spazio nei locali dismessi di una scuola elementare.

Sembra ieri il trasferimento in una sede avveniristica, ancora oggi, nel centro rinnovato e rilanciato di Savona, oggi meta crocieristica piena di punti di riferimento storici, gastronomici, panoramici, sportivi, in una parola turistici a tutto campo.

Il soffitto è istoriato dei nomi di chi ha donato, in grande stile o con grande cuore in mancanza di altro, per contribuire alla nascita della sede. Per me il museo potrebbe essere anche vuoto; quella serie di nomi elenca amici, conoscenze, frequentazioni, incontri, anche scontri a volte, ma vita e passione di tanti. Qualcuno non c’è più, altri ci sono ancora e speriamo per altri cinquant’anni, qualcuno lavora a migliaia di chilometri, tanti sono arrivati dopo e lasciano la loro impronta di impegno e competenza per tenere vivo il museo.

Se abbasso lo sguardo, posso percorrere il riassunto fedele, puntuale e preciso di quasi cinquant’anni di storia dell’informatica.

Non credo di potermi fermare a cena perché con la famiglia si fa troppo tardi. Però sto facendo lobby sulla famiglia per arrivare nel pomeriggio, salutare grandi amici, sentire la voglia e la passione di vivere il proprio tempo attraverso il racconto di che cosa si poggiava sulla scrivania per fare un passo avanti, scoprire una cosa nuova, giocare come mai prima, entrare in una nuova epoca, vedere computer accesi e vivi e non reliquie polverose.

Il museo a vent’anni è un adulto fatto e finito. Chiede attenzione perché porta valore. È un oggetto culturale di valore importante, che merita sostegno e – sabato prossimo – assolutamente una visita.