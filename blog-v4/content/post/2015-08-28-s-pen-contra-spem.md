---
title: "S-Pen contra spem"
date: 2015-08-28
comments: true
tags: [Samsung, S-Pen, Android, Note, Galaxy, Note5]
---
Rientro per un momento dalla pausa agostana per notare che, riporta [Android Police](http://www.androidpolice.com/2015/08/24/video-probable-note-5-design-flaw-can-cause-s-pen-to-break-pen-detection-when-inserted-backward-or-get-hopelessly-stuck/), inserire al contrario lo stilo di serie nell’alloggiamento di un Galaxy Note 5 ha buone possibilità di danneggiare irreparabilmente un meccanismo interno di riconoscimento che fa partire automaticamente applicazioni quando lo stilo stesso viene estratto e altre cose così.<!--more-->

Come minimo mi aspetto puntate su puntate del *pengate*, video a camionate su YouTube, forum chilometrici in argomento. Chi non ricorda quella volta che iPhone 6 si piegò e pareva dovesse piegarsi anche l’asse terrestre?