---
title: "La carica dei malpensanti"
date: 2019-08-23
comments: true
tags: [iFixit, Faa, MacBook, iOS]
---
Si sta creando qualche polemica in virtù del fatto che Apple inizia a essere pignola sul fatto che i suoi apparecchi utilizzino le sue batterie e non altre, sedicenti compatibili.

CNBC ha [un ottimo riassunto della situazione](https://www.cnbc.com/2019/08/18/apple-battery-fires-are-brand-risk.html). Personalmente mi limito a osservare che una batteria dentro un MacBook Pro o un iPhone non è una sorta di grossa Duracell che prendi e cambi a piacere. È un computer dedicato pieno di software, che comunica con sensori a bordo delle macchine, sulle quali girano sistemi operativi scritti tenendo conto delle peculiarità di quelle batterie.

Il tutto deve funzionare entro vincoli molto stretti di dimensioni, temperature, efficienza e sicurezza per chi usa l’apparecchio.

Nonostante tutti gli sforzi che si fanno, è inevitabile che ogni qualche milione di batterie, una volta ogni tanto, qualcosa di brutto accada: un principio di incendio, un rigonfiamento, una perdita.

Con le tecnologie usate attualmente nelle batterie, il tasso di incidenti è *miracolosamente* basso. Mai potrà essere zero.

Comprare batterie fabbricate non si sa da chi e non si sa come è un sistema eccellente per aumentare la probabilità di incidenti gravi, o anche di dover buttare via un portatile costoso per il vezzo di risparmiare su una tecnologia che fornisce energia tramite reazioni chimiche a trenta centimetri dalla mia faccia.

Preferisco risparmiare su altro e dovrebbe essere ovvio, per gente che pensi in modo logico. Certo, uno che pensa male si può prendere una cinesata senza garanzie per il gusto di combattere la multinazionale che vuole lucrare sui ricambi, una volta non c’erano tutte queste fisime sulle batterie signora mia, le batterie sono tutte uguali. L’energia per ripetere queste sciocchezze non viene mai a mancare.

Quando prendono fuoco no, le batterie non sono più tutte uguali. Samsung dovrebbe avere insegnato qualcosa.
