---
title: "Il test decisivo"
date: 2018-03-30
comments: true
tags: [AirPods, Huawei, FreeBuds, Verge]
---
Stanno arrivano i cloni di Airpods, a opera di Huawei; lo scrive *The Verge* in [Gli aspiranti AirPods di Huawei sono belli e si portano bene](https://www.theverge.com/2018/3/28/17165828/huawei-free-buds-truly-wireless-earbuds-hands-on).<!--more-->

Vlad Savod scrive come la struttura dei FreeBuds sia molto simile a quella degli AirPods, con la batteria inserita in un prolungamento che protrude oltre l’auricolare: il sistema per *equilibrare le esigenze contraddittorie di fornire sia un segnale wireless chiaro sia un output sonoro rispettabile*.

I FreeBuds sono appena più lunghi, leggermente più piatti, si inseriscono più profondamente nell’orecchio e non cadono. Huawei dichiara un’autonomia superiore a quella degli AirPods e sono disponibili anche in colore nero. Il prezzo in dollari è inferiore, 159 contro 179.

Insomma, un prodotto competitivo. La concorrenza fa bene a tutti. Che cosa potrebbe mancare?

>Purtroppo Huawei non ha permesso alcun test di ascolto durante l’evento di lancio a Parigi.

Giusto, sono auricolari. A chi può mai interessare come suonano?