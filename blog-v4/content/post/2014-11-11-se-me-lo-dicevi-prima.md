---
title: "Se me lo dicevi prima"
date: 2014-11-11
comments: true
tags: [HomeDepot]
---
Pirati informatici penetrano nei sistemi della catena americana di articoli per la casa Home Depot e si portano a casa 53 milioni di indirizzi email, più 56 milioni di carte di credito.<!--more-->

La breccia che ha consentito l’assalto, [riferisce il Wall Street Journal](http://online.wsj.com/articles/home-depot-breach-bigger-than-targets-1411073571), sta in Windows.

A seguito dell’incidente, [riprende 9to5Mac](http://9to5mac.com/2014/11/09/home-depot-windows-breach-macbooks-iphones/), un tecnico di Home Depot va a comprare *due dozzine di MacBook e iPhone* per i dirigenti.

Chissà se qualcuno glielo aveva spiegato prima. E che cosa hanno risposto.