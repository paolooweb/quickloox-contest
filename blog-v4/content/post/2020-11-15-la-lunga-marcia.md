---
title: "La lunga marcia"
date: 2020-11-15
comments: true
tags: [Apple, Arm, M1]
---
Trentacinque anni fa il primo processore Arm metteva insieme venticinquemila transistor.

M1, [sedici miliardi](https://twitter.com/kenshirriff/status/1327021630636212224). seicentoquarantamila volte di più, su una superficie grande il doppio.

Un thread affascinante. L’apice della tecnologia del 1985 sarebbe un puntino quasi invisibile sul chip all’apice della tecnologia odierna.

<blockquote class="twitter-tweet"><p lang="en" dir="ltr">how it started: how it’s going: <a href="https://t.co/R5bPcH1aOQ">pic.twitter.com/R5bPcH1aOQ</a></p>&mdash; Ken Shirriff (@kenshirriff) <a href="https://twitter.com/kenshirriff/status/1327021630636212224?ref_src=twsrc%5Etfw">November 12, 2020</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>