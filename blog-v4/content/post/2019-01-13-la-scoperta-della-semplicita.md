---
title: "La scoperta della semplicità"
date: 2019-01-13
comments: true
tags: [Smith, Mac, Windows]
---
Con buona pace dei [darwinisti fuori epoca](http://www.macintelligence.org/blog/2015/06/28/vale-tutto/), il valore di una piattaforma continua a essere quello che semplifica più di quello che consente.

A dimostrazione il [post di Zoë Smith](https://www.zoesmith.io/2019-01-04/i-love-my-mac/), una cosa talmente corta e sintetica che spiace citarla; ne dai via metà. Eppure è un diamante perfetto.

>[Mio marito Fabio] è appena passato a Mac da Windows. È stato deliziato da così tante possibilità che io do per scontate.

L’elenco lo lascio al curioso. La parte importante è il commento. Il grassetto è mio.

>Non uso Windows da dieci anni […] Forse tutte queste funzioni sono presenti. **Ma non sono state scoperte da Fabio**, persona intelligente che usa un computer per svolgere un lavoro che non è una versione più articolata di “usare un computer”.

La conclusione la voglio incidere sulla cornice del monitor, per averla sempre davanti:

>Nessun inciampo, nessun ritardo di prodotto, nessun mercato sottoperformante, nessuna barra spazio difettosa prodotta dalla enorme Apple di oggi mi ha portato a credere che l’azienda abbia perso di vista i suoi principî di design.

Qualcuno penserà che Smith sia una svitata qualsiasi, un po’ fanatica di Apple. Vada a visitare il suo blog e il *design* del suo blog soprattutto, prima ancora di guardare la sezione curriculum.