---
title: "Mi manca la t-shirt"
date: 2013-06-19
comments: true
tags: [Apple]
---
Molti anni fa mi iscrissi al programma sviluppatori Apple. Al termine di una procedura burocratica che nemmeno ricordo e previo pagamento di una somma in lire non indifferente rimasi diversi giorni ad aspettare un pacco dagli Stati Uniti.<!--more-->

Finalmente arrivò la scatola di cartone. Con dentro un libriccino, il contratto di licenza e i preziosi Cd-Rom contenenti il software per sviluppatori, che si sarebbero mantenuti aggiornati di mese in mese, un trimestre dopo l’altro, diventando Dvd. E una *t-shirt* nera *I  Code*.

Oggi in pochi secondi ho pagato ottanta euro sul sito web Apple e nel giro di minuti ho iniziato a scaricare la Developer Preview di Mavericks, OS X 10.9. Quasi cinque gigabyte che trovandomi in trasferta ho prelevato via chiavetta, in circa sei ore. Se anche fossero state dodici o diciotto niente sarebbe cambiato; lo stato delle connessioni italiane è miserevole, ma potrebbe andare peggio.

Così nel giro di mezza giornata sono pronto a fare tutti gli esperimenti del caso, a procedere con un libro in lavorazione, a scoprire cose evidenti e cercarne altre più celate.

È tutto cambiato in meglio. Molto. C’è una cosa che però mi manca: la *t-shirt*.
