---
title: "Cavo canem"
date: 2014-04-06
comments: true
tags: [Usb, MicroUsb, Apple]
---
Era settembre che si diceva di quanto fosse tremendo da vedere e concepire il [connettore MicroUsb](https://macintelligence.org/posts/2013-09-08-e-lo-chiamano-micro/). C’è stato dibattito però, non tutti erano d’accordo.<!--more-->

Passati neanche sei mesi, se MicroUsb era una buona soluzione, perché si annuncia un [nuovo connettore Usb](http://arstechnica.com/gadgets/2014/04/usb-if-posts-first-photos-of-new-reversible-type-c-connector/)?

Le cui innovazioni fondamentali sono, udite udite, l’essere piatto, l’essere piccolo, potersi infilare in qualunque verso.

Come fa Apple [da mesi e mesi](http://store.apple.com/it/product/ME291ZM/A/cavo-da-lightning-a-usb). Mentre il nuovo connettore Usb, si badi, non esiste a parte le ricostruzioni in computergrafica. Sarà presentato a metà anno, sarà presente sui prodotti più avanti, non si sa bene, forse Natale, prima, dopo, boh.

Qualcuno traccia la strada e altri seguono docili, neanche progettassero con il guinzaglio al collo.