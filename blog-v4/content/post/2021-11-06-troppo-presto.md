---
title: "Troppo presto"
date: 2021-11-06T00:18:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [Emilio, Steve Jobs, Notes, Lotus, Ibm, Hcl, Alan Parsons Project, Glass Hammer, Mac] 
---
Ho appena imparato che per capire quanto presto se ne sia andato Steve Jobs ho dovuto perdere un amico praticamente alla sua stessa età.

**Emilio** se ne è andato troppo presto. Ha vissuto la rivoluzione informatica e se fosse stato appena più cinico e senza scrupoli ne sarebbe forse stato un protagonista diretto. Invece era troppo onesto e sensibile, retto e intelligente ed è restato nelle retrovie, mentre sul palco saliva sempre qualcun altro.

Non che sia stato sfortunato; semplicemente ci sono persone fatte per lavorare con passione e persone fatte per lavorare con il pelo sullo stomaco. Lui era passione, intelligenza brillante, costanza, metodicità, precisione, affidabilità. Ha vissuto da protagonista gli anni d’oro di Lotus Notes fino alla sua acquisizione da parte di Ibm e la successiva cessione a [Hcl](https://www.hcltechsw.com/notes).

Il diventare uno specialista di Notes gli ha garantito lavoro importante e qualificato in aziende di prima fascia, tanto da creare opportunità di lavoro anche per altri attorno a sé, fino all’altroieri, quando il cuore si è fermato, forse per lo stress da lavoro, forse per qualche guaio di salute, forse per il carico di certe occasioni della vita dove fare una scelta sbagliata poi ti mette addosso un peso che porti fino alla fine.

Emilio ha sempre portato i suoi pesi con un sorriso e con apparente leggerezza, senza mai spostarli sulla schiena degli altri a suon di parole. Non è mai venuto meno ai suoi principî e sì, ai suoi valori; la parola è decisamente fuori moda ma lui ne aveva, solidi, precisi, abbracciati con consapevolezza e responsabilità.

Abbiamo lavorato insieme per qualche anno, quando i Mac erano una novità e si faceva collezione di programmi ed era effettivamente possibile avere tutti i programmi per Mac (copyright a parte, ma la questione si pose più avanti). Lui era sempre avanti di un passo nella conoscenza, nell’approfondimento, nei trucchi.

Ed è sempre stato così, fino alla fine. Raramente negli ultimi tempi, ma sempre con gusto e piacere, ci si trovava per una pizza e poi si andava a casa sua a bere una birra in più, mettere su musica country o *progressive*, parlare di Apple e vedere sempre un ultimo modello appena comprato, una beta per pochi, un programma semisconosciuto ma significativo appena uscito, di cui lui era sempre al corrente per primo. Viveva da esperto di Notes, ma la verità è che nel digitale c’era veramente poco in cui non fosse all’avanguardia.

E, contrariamente ad altri che alla sua età sono bolliti e tirano alla pensione, continuava a esserlo. A lavorare con energia e intelligenza, a migliorarsi, a crescere.

I prossimi [Glass Hammer](https://glasshammer.com) dovrò scoprirli da solo. Emilio si prendeva poche vacanze ma faceva viaggi molto lunghi; nel viaggio più lungo di tutti vorrei che lo accompagnasse l’[Alan Parsons Project](https://www.youtube.com/watch?v=9xGWSWOXYd8) con [i versi di Edgar Allan Poe](https://www.poetryfoundation.org/poems/50462/to-one-in-paradise):

*And all my days are trances,*<br>
*And all my nightly dreams*<br>
*Are where thy grey eye glances,*<br>
*And where thy footstep gleams—*<br>
*In what ethereal dances,*<br>
*By what eternal streams.*

<iframe width="560" height="315" src="https://www.youtube.com/embed/9xGWSWOXYd8" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>