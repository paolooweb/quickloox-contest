---
title: "Dare la cuccia a un grande felino"
date: 2023-09-01T16:34:24+01:00
draft: false
toc: false
comments: true
categories: [Hardware, Software]
tags: [Mac, Snow Leopard, Instructables, Autodesk]
---
Quelli che una volta i Mac sì che erano belli perché si poteva rovistare nelle estensioni e ora lamentano la chiusura e la creatività dell epoca odierna dove tutti-pensano-a-iPhone, oggi possono cimentarsi con la costruzione di un [Mac fai-da-te azionato da Mac OS X Snow Leopard](https://www.instructables.com/Upgrade-your-original-1984-Macintosh-to-run-OS-X-S/).

Su *Instructables* c’è il progetto completo, con lista di componenti e istruzioni, basato sullo chassis di un Macintosh originale del 1984 che, secondo gli obiettivi dell’autore, doveva restare integro, senza stravolgimenti, tagli e deformazioni.

Dentro, naturalmente, del Macintosh originale c’è poco o nulla, o la compatibilità software sarebbe impossibile.

Il <s>pazzo</s> genio ha speso molto più in manodopera e in recupero pezzi in giro per la rete che in puro denaro e il risultato è semplicemente ammirevole, neanche *just for the sake of it*, perché con Snow Leopard effettivamente qualcosa si combina ancora, mentre i sistemi operativi buoni per un Mac del 1984 [girano dentro un francobollo nel browser](https://macintelligence.org/posts/2022-04-02-trovare-il-quadra/).

Costruirsi un Mac tutto da soli invece di gironzolare per i cantieri di Internet. Questa sì che è una meraviglia moderna.