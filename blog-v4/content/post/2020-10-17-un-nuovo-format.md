---
title: "Un nuovo format"
date: 2020-10-17
comments: true
tags: [Apple, iPhone, watch]
---
Una nozione evidente: assistere a una presentazione di prodotto in persona ha tutt’altro sapere rispetto a guardarne una sullo schermo.

Un’altra nozione evidente: Anche dove ci fosse lo spazio in agenda, non è esattamente il momento migliore per recarsi ad [Apple Park](https://www.fosterandpartners.com/projects/apple-park/) per vedere la presentazione di un [nuovo watch](https://www.apple.com/apple-events/september-2020/) o di un [nuovo iPhone](https://www.apple.com/apple-events/october-2020/).

L’attuale modalità di presentazione dei prodotti può non essere quella ottimale. In compenso, è il meglio che possiamo avere in questo periodo.

Io sono contento, per una serie di ragioni.

La più banale è la popolarizzazione dell’evento. Non conta più avere trovato un posto allo [Steve Jobs Theater](https://www.fosterandpartners.com/projects/steve-jobs-theater/) o trovarsi in un sobborgo di Nanchino a masticare anatra con un iPhone SE in mano: l’evento è uguale per chiunque.

Meno banale: Apple ha ben chiare le soglie di attenzione diverse tra analogico e digitale. Ci sono di sicuro anche ragioni logistiche e di costi; Tuttavia non trovo solo coincidenza che si stia passando da eventi in presenza attorno alle due ore a eventi in rete di un’ora o poco più. Cambia il livello di attenzione che possiamo dare e il modo di comunicare le informazioni. Da spiegare a quelli che pensano di sostituire riunioni di due ore con teleriunioni di due ore o, peggio, chi pretende di insegnare da remoto esattamente come dalla cattedra, però dietro una telecamera.

Un po’ più sottile: la differenza tra diretta e registrato. Gli eventi Apple attuali vogliono dare l’impressione di ascoltare una persona che parla e però sono accuratamente preconfezionati. Nel vecchio mondo qualcuno avrebbe avuto da ridire, un po’ come a un concerto con il *playback*. Nel nuovo… qualcuno ha notato come il contenuto fili splendidamente, come i concetti vengano trasmessi con efficacia massima, come la perdita di tempo o il momento di noia siano tenacemente combattuti con l’intento di eliminarli?

Immaginiamo di dover insegnare il teorema di Pitagora. Meglio una diretta di un’ora con docente che si inceppa, guarda lo schermo invece dell’obiettivo, si schiarisce la voce, viene interrotto dallo studente che deve andare in bagno eccetera eccetera, o altrimenti quindici minuti di video coinvolgente, curato, ricco, chiaro, interessante e poi quarantacinque minuti di discussione?

Nell’ipotesi che vada comunque colmata l’ora canonica, chiaro. Scommetto a mani basse che nel secondo caso basterebbe mezz’ora per raggiungere lo stesso risultato e nell’ora canonica potrei insegnare due cose invece di una. Il video è un mezzo diverso dalla presenza: se vuoi fare cose efficaci, devi usare al massimo il mezzo che hai, non scimmiottare l’unico che conosci.

Ultimo e meno banale di tutti: Apple ha studiato maniacalmente i modi che aveva a disposizione per mantenere viva l’attenzione su un video di un’ora. Viste le transizioni da un evento all’altro, da una persona a un’altra, tra segmenti differenti di un prodotto? Gli effetti speciali, i movimenti di camera, l’illuminazione?

Quando sono online e vedo due persone di azienda che faticano persino a passarsi la parola senza generare noia, non ce la faccio più. Apple ha alzato l’asticella anche qui. O hai uno straccio di organizzazione, non voglio dire coordinamento, non voglio dire regia, oppure forse è meglio che mandi una email invece che farci perdere tempo.

È l’inizio di un nuovo format. Una lezione su come il digitale sia diverso dall’analogico, come vada sfruttato in modo diverso dall’analogico, come manchi di tante cose rispetto all’analogico e come ne abbia tante altre che invece all’analogico mancano. Soprattutto, una lezione sul fatto che il digitale cominciamo adesso a conoscerlo e a valorizzarlo, anche sotto necessità, e i confronti fatti fino a qui sono impropri, un canale millenario e familiare a tutti contro un canale che a ottobre 2019 era al massimo uno sfizio o una fisima o una toppa.

Forse è improprio che una lezione universitaria online cominci con pochi secondi di volo d’uccello tra i corridoi fino a entrare in aula. O che nell’aprire un cassetto di scrivania si scopra che dentro c’è un laboratorio e, dentro il laboratorio, un docente che si muove in favore di telecamera.

Forse no.
