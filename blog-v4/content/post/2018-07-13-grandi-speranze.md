---
title: "Grandi speranze"
date: 2018-07-13
comments: true
tags: [iOS12]
---
Per quanto il gioco preferito di moltissimi sia *faccio le pulci al nuovo sistema operativo apposta per lamentarmi che dopo dieci o venti anni di evoluzione non compaiono più funzioni clamorose come quando il sistema era appena nato*, constato che da tempo non ero in attesa speranzosa di una nuova versione di iOS come adesso che sta per arrivare la dodicesima iterazione.

Apple spinge chiaramente novità come la realtà aumentata o gli animoji; a leggere [un primo resoconto della beta](https://www.macstories.net/news/ios-12-the-macstories-overview/), mi entusiasmo per Siri Shortcuts, Screen Time, FaceTime fino a trentadue partecipanti simultanei, Apple Books che sostituisce iBooks, Apple News che finalmente magari diventa anche cosa italiana, unificazione dei gesti tra iPhone e iPad.

Solo alcune delle voci dell’elenco, bello lungo anche per una versione dichiaratamente orientata più a chiudere buchi che aprire fronti.

Per non parlare di macOS Mojave, ché appunto, ne parliamo un’altra volta.

L’atmosfera mi piace, c’è molto da sperare.