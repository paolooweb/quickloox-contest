---
title: "Azioni e reazioni"
date: 2018-06-30T14:33:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [Apple, Microsoft, azioni, Aapl, Macworld, Steve Jobs] 
---
L’articolo che segue è stato scritto per *Macworld Italia* a fine 2004. Lo ripropongo perché tornano periodicamente interpretazioni libere dell’investimento di Microsoft in Apple del 1997 e altre amenità.

## Passare all’azione

**Attualità, passati, futuri e demistificazioni sulla misteriosa azienda AAPL**

È passato il tempo in cui si facevano fruttare i risparmi investendoli i buoni del Tesoro. Ora a pagare è il rischio, e il rischio, recita la saggezza popolare del Bar Sport, significa investire in azioni.

La si può vedere in altri modi. Per esempio qualcuno pensa che il mercato azionario sia gioco sì di azzardo, ma l’unico in cui il banco non ha vantaggio e quindi quello dove è veramente possibile vincere nel lungo termine.

Ecco qualche domanda e qualche risposta per quando si intende passare all’azione e l’azione è rappresentata dal simbolo AAPL.

Non intendo occuparmi di banali dettagli pratici come trovare un broker o quale sia il modo più conveniente per aprire un conto corrente bancario negli Stati Uniti. Uno che fa pensieri su AAPL ma non sa come procedere è meglio che non proceda; si farà meno male.

#### La madre di tutte le sciocchezze

Un sacco di sempliciotti è ancora convinto che Microsoft abbia *comprato* AAPL. Nel 1997 Microsoft comprò azioni AAPL per 150 milioni di dollari: una frazione minuscola dell’azionariato complessivo della società, che in quel momento aveva oltre due miliardi di dollari in cassa tra denaro liquido e a breve termine.

*[Aggiornamento: non erano due miliardi come ho erroneamente scritto nell’articolo, ma 1,459 miliardi di dollari, come descritto nella [documentazione fiscale](https://www.annualreports.com/HostedData/AnnualReportArchive/a/NASDAQ_AAPL_1997.pdf) depositata da Apple alla chiusura dell’anno fiscale 1997. Nell’anno fiscale precedente, la cassa di Apple era di 1,745 miliardi. La situazione non era certo rosea e Apple chiuse il 1997 con un passivo di un miliardo di dollari su sette di fatturato. Nondimeno, [nel gennaio del 1998 Jobs annunciò il primo trimestre in positivo](https://igotoffer.com/apple/history-apple-1997-1998) dell’azienda dopo un lungo periodo, con quarantaquattro milioni di dollari nel segno più. L’annuncio dell’accordo con Microsoft fu dato in agosto 1997]*

Le azioni comprate da Microsoft erano senza diritto di voto, e comprate per cinque anni. Nel 2002 sono state vendute e oggi Microsoft non possiede neanche un centesimo di AAPL, né l’ha salvata da alcunché (AAPL aveva già in tasca tutti i soldi che le servivano), né ha mai avuto la possibilità di far pesare neanche un singolo voto su una singola decisione.

#### Chi ci crede ci guadagna

Scrivo queste note il 12 dicembre 2004. Non lo sa praticamente nessuno, ma esattamente 24 anni fa, nel 1980, AAPL divenne società per azioni. 10 mila dollari investiti in azioni AAPL il 12 dicembre 1980 equivalevano, il 29 novembre 2004, a 95.220,87 dollari, con un guadagno dell’852,21 percento sull’investimento iniziale.

Chi non ci avesse creduto dall’inizio ma lo avesse fatto dal ritorno di Steve Jobs, e avesse investito 10 mila dollari in AAPL il 28 novembre 1997, si sarebbe ritrovato sette anni dopo con 77.115,49 dollari in saccoccia.

Qualcuno ricorderà che, a fine 1997, il valore delle azioni AAPL era intorno ai dieci-undici dollari. Sulla stampa cosiddetta specializzata e secondo molti analisti il titolo più ricorrente era *Apple is doomed*, Apple è spacciata. Altri, forse meno specializzati ma più sensati, si fidarono a investire in azioni evidentemente sottovalutate, che con ottima probabilità avrebbero potuto solo aumentare di valore. I fatti dimostrano che avevano ragione i sensati e non gli specializzati.

#### C’è chi non crede se non vede

Non è semplicissimo trovare gratis uno storico completo dell’andamento delle azioni AAPL dall’inizio a oggi. Yahoo offre un ottimo servizio a <http://finance.yahoo.com/q/hp?s=AAPL>, che però parte solo dal 7 luglio 1984 *[Aggiornamento: il link attuale è [questo](https://finance.yahoo.com/quote/AAPL/history?ltr=1)]*. La risposta completa si trova in un indirizzo semisconosciuto e lungo. Ma mai come a questo riguardo… vale la pena di trascriverlo: [http://www.corporate-ir.net/ireye/ir_site.zhtml?ticker=aapl&script=345&layout=7&item_id=aapl](http://www.corporate-ir.net/ireye/ir_site.zhtml?ticker=aapl&script=345&layout=7&item_id=aapl).

#### Quando ci sono i record

Il 30 novembre 2004 la quotazione AAPL era a 68,95 dollari per azione, il quinto valore più alto nella storia della società, il massimo degli ultimi quattro anni. Il primato assoluto è di 72,10 dollari per azione, registrato il 22 marzo del 2000.

#### Quando ci sono gli split

Qualcuno si chiederà come sia possibile che il valore più alto di AAPL sia stato di settantadue dollari, visto che a memoria di uomo si sono raggiunti anche i cento. Il fatto è che le aziende americane usano effettuare il cosiddetto split. Quando il prezzo delle azioni ai aggira su quota cento, le azioni stesse vengono spezzate in due e un titolo da cento si trasforma in due titoli da cinquanta. Non cambia assolutamente nient’altro, se non per il fatto che il numero delle azioni esistenti raddoppia e che i numeri a due cifre sono più comodi da trattare di quelli a tre. Nella storia di AAPL si sono verificati esattamente due split.

#### Chi comanda in AAPL

È opinione comune che chi ha più azioni in mano comandi la società. Non è propriamente vero. Steve Jobs, comandante in capo di AAPL, possiede l’1,26 percento delle azioni AAPL e letteralmente nessuno può contraddirlo, almeno fino a che non ricevesse la sfiducia del consiglio di amministrazione. Nella tabella che compare in queste pagine ci sono maggiori particolari. Tutto è molto più complicato di così. Esistono tipi diversi di azioni, esistono le stock option (diritto  e non è sempre semplice capire in modo immediata l’intera distribuzione delle quote tra gli azionisti, ma in concreto detenere azioni AAPL è un fatto di potere solo fino a un certo punto.

#### Quando comprare e quando vendere

Vuoi comprare azioni AAPL? Bravo, bella scelta. Intanto deve passare abbastanza tempo. Se vuoi guadagnarci sopra a un mese o a sei mesi, non sei un investitore ma uno speculatore e allora devi rovinarti la vita a leggere ricerche di mercato, studi di settore e documentazione burocratica aziendale di una noia pazzesca.

Se invece sei una persona seria, cerca di individuare la fine di un ciclo fisiologico di discesa del valore delle azioni. Compra a quel punto e poi aspetta pazientemente di vedere i frutti dell’investimento.

Le persone che comprano azioni sono, quanto a emotività, peggiori di quelle che giocano al lotto. Quelli del lotto credono che un numero in ritardo debba uscire più sicuramente di un numero appena uscito, e tendono a rovinarsi. Ma, se non interpretano i sogni, il ritardo è l’unico criterio (sbagliato) di valutazione che hanno.

Quelli delle azioni, d’altro canto, fanno di qualsiasi cosa un criterio (sbagliato) di valutazione. Ogni azienda ha le sue indiosincrasie e AAPL non fa eccezione. Per esempio, dopo Macworld Expo il prezzo delle azioni tende a deprimersi. Non importa che i risultati siano buoni, i prodotti esistenti siano bene accolti e vengano presentati nuovi prodotti molto appetibili. Siccome AAPL non ha presentato il teletrasporto di Star Trek oppure il mouse antigravità, regolarmente annunciati sui siti di gossip informatico, il prezzo scende.

#### Gli analisti

Molti danno ascolto all’opinione degli analisti di mercato. Sono sedicenti specialisti, spesso aziende prestigiose nel campo, che assegnano un rating, ossia una valutazione di quello che conviene fare con questo o quel titolo.

Nel caso di AAPL, gli analisti che ne sanno veramente qualcosa sono due o tre. Gli altri non capiscono assolutamente niente di come fa i soldi AAPL e quindi sparano nel buio, applicando ciecamente criteri che ritengono validi solo perché “funzionano” su un’altra azienda, che fa i soldi in modo diverso.

C’è chi si fida degli analisti. Il mio parere molto personale si basa sulla radice del nome, che potrebbe avere attinenza con la parte del corpo utilizzata per eseguire le analisi stesse.

((( firma )))<br>
Lucio Bragagnolo (lux@mac.com) avrebbe voluto avere più soldi da investire in AAPL a fine 1997.<br>
((( fine firma )))

((( tabella )))<br>
### Azionisti e dirigenti

Numero di azioni in mano alle persone più influenti in AAPL:

Steve Jobs, capo supremo	5.060.002<br>
Fred Anderson, ex Chief Financial Officer	2.672<br>
Bill Campbell, consigliere di amministrazione	100.502<br>
Tim Cook, sovrintendente alla logistica	5.903<br>
Al Gore, già vicepresidente degli Stati Uniti, consigliere di amministrazione	10.000<br>
Millard Drexler, consigliere di amministrazione	100.000<br>
Jon Rubinstein, responsabile dell’hardware	161.087<br>
Avie Tevanian, responsabile del software	1.501.252<br>
Totale delle 16 persone che contano in Apple tra dirigenti e consiglieri	10.203.443

I cinque milioni di azioni in mano a Steve Jobs corrispondono all’1,26 percento dell’azienda. Le sedici persone che contano detengono in totale il 2,51 percento di AAPL.<br>
((( fine tabella )))

((( box 1 )))<br>
### Come diventare un analista (truffatore) di successo

Si raccoglie un indirizzario di potenziali investitori ricchi di denaro, diciamo diecimila. Si scrive a cinquemila di essi affermando che il titolo salirà e agli altri cinquemila sostenendo che scenderà.
Passata la scadenza, si scrive di nuovo ai cinquemila. A metà di questi si scrive che il titolo salirà, agli altri che scenderà. 2.500 investitori avranno a questo punto ricevuto due previsioni corrette consecutive.
Continuando, si può avere un centinaio di persone che avranno ricevuto sei o sette previsioni esatte consecutive. Gli si propone un abbonamento al nostro servizio di previsioni e si diventa ricchi.<br>
Ovviamente è solo una storiella e non si diventa analisti così. Il fatto vero è che nessuno controlla la precisione degli analisti nel tempo…<br>
((( fine box 1 )))

((( box 2 )))<br>
### Un’azione simbolica

Chi stima particolarmente AAPL per la sua tecnologia può compiere un atto simbolico: comprare una singola azione, magari per incorniciarla, o per mostrarla agli amici. Lo si può fare da http://www.oneshare.com.
((( schermata del sito, possibilmente scegliendo lo stock Apple )))<br>
*[Aggiornamento: Oneshare non esiste più, anche perché qualunque banca oramai consente l’acquisto di azioni estere. Il sottoscritto è in possesso di una azione Apple, per esempio].*
((( fine box 2 )))

(((box 3 )))<br>
### Materiale per secchioni

Chi volesse diventare veramente specialista AAPL inizi dai cosiddetti moduli 10-K della [Securities & Exchange Commission](http://www.sec.gov) e consulti il motore Edgar. Ogni azienda statunitense deve compilare periodicamente il modulo e c’è dentro veramente tutto quello che è possibile sapere senza essere nel consiglio di amministrazione. Per farsi un’idea dei pareri degli analisti si può partire da [newratings](http://www.newratings.com).<br>
((( fine box 3 )))

((( box 4 )))<br>
### C’è da pagare il ticker?

Un ticker è un programmino che aggiorna in tempo il più possibile reale sull’andamento di uno o più titoli. Su Versiontracker (http://www.versiontracker.com/macosx) basta cercare *ticker* per avere più scelta di quella che è umanamente immaginabile. Il ticker è il complemento indispensabile per chi ha perso la testa e deve controllare le azioni AAPL ogni mezzo minuto.<br>
*[Aggiornamento: Versiontraker non esiste più. Una ricerca analoga su App Store o su Google porta alla luce innumerevoli alternative. Lo stesso Google fornisce quotazioni azionarie in tempo semireale].*<br>
((( fine box 4 )))