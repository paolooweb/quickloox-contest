---
title: "Automatismi"
date: 2017-06-20
comments: true
tags: [MacOsxAutomation, Soghoian, Automator, AppleScript]
---
Sono cose gravi.

Per motivi che non riesco a spiegare, non seguivo su Twitter [Otto the Automator](https://twitter.com/macautomation).

Da troppo tempo non effettuavo una visita a [Mac OS X Automation](https://mobile.twitter.com/macautomation). È cambiato molto, in qualità e quantità.

Veramente imperdonabile è non avere capito in anni di frequentazione che  Mac OS X Automation era consigliato da Apple, pur non essendo un sito Apple, perché dietro c’era giustappunto [Sal Soghoian](https://macintelligence.org/posts/2017-05-21-voglio-non-posso-comando-d/).

Il quale, dopo essere stato [congedato da Apple](https://macintelligence.org/posts/2016-11-30-fallo-di-mano/), va seguito ancora più e meglio di prima, con regolarità. Il più producente di tutti gli automatismi.
