---
title: "Gossip irreali"
date: 2016-10-01
comments: true
tags: [iPhone]
---
iPnone 7 è uscito (figurativamente) ieri mattina eppure già possiamo deliziarci con [supposizioni preziosissime](https://9to5mac.com/2016/09/29/kgi-iphone-8-all-glass-stainless-steel/) riguardanti iPhone 8.

Viene da chiedersi quanto sia scollegata dalla realtà la gente che scrive di queste cose. Poi pensi ai gossip reali: il principe William, i cappelli della regina, come sta Kate, cosa fa la sorella di Kate, Carlo e Camilla, roba che muove un’industria miliardaria di millanterie.

Pensi allora ai consumatori che alimentano quest’industria: in generale persone un po’ frustrate o un po’ alienate, che trovano pace e distrazione da una realtà insoddisfacente nel ricamare attorno a figure irraggiungibili.

Quindi torni su quelli che leggono di iPhone 8 il giorno dopo che è uscito iPhone 7 e capisci: gente frustrata che cerca sollievo da una realtà insoddisfacente nel ricamare attorno a prodotti irraggiungibili.

E cominci a chiederti se il problema di queste persone sia la realtà o altrimenti il modo che scelgono per evaderne.

Arriva evidente e preoccupante la conclusione: il numero eccessivo di persone che fuggono dalla realtà con il principe Carlo o con iPhone 8 costituisce un problema per la gente normale, quella che legge, decide serenamente se comprare o non comprare e poi si occupa comunque di cose reali. Non nel senso dei Windsor.
