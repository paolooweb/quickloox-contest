---
title: "Agenzia degli stagisti"
date: 2022-10-01T01:43:26+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [Agenzia delle Entrate, fattura elettronica]
---
Ho dovuto alfine occuparmi della fattura elettronica. Dopo avere visionato i siti di alcuni provider del servizio e avere trovato qualche sgradevole [dark pattern](https://macintelligence.org/posts/2022-09-16-la-stagione-dei-poteri-oscuri/), ho deciso di fare da solo.

(Il dark pattern più evidente e pure fastidioso è promettere sei mesi di servizio gratis e mostrare subito un bel pulsante di acquisto, mentre il prezzo standard una volta scaduta la promozione è nascosto, in modo da spingere a comprare senza pensarci).

Onestamente, abituato a tanti anni di Stato italiano, con il sito di Agenzia delle Entrate poteva andare peggio. Le fatture sono compilabili in modo ragionevolmente semplice e il sistema parrebbe funzionare. Si poteva approfittare di una digitalizzazione della fatturazione per risolvere qualche problema e semplificare la materia, al posto di rifare in digitale le stesse perversioni della carta; tuttavia, strutturare la fattura come documento Xml può essere in linea di principio una buona idea per miglioramenti futuri.

Il problema è che il risultato è *brutto*. Povero. Trascurato. Mediocre. Rendere in PDF la fattura non si può guardare. Capisco che una fattura è intrinsecamente diversa dall’intrattenimento multimediale, ma se non altro indenta e commenta l’Xml, Stato pigro e noncurante. E fare apparire un documento standard vagamente gradevole, con le risorse che sono state spese, era un obiettivo facilmente raggiungibile.

Come i governanti si esprimono da sale sfarzose, studi sontuosi e sedute di broccato, così le fatture – se diventano affare di Stato – dovrebbero avere un aspetto perlomeno decoroso.

Invece abbiamo un file Xml creato da stagisti che hanno passato un pomeriggio in allegria e nient’altro, perché a mettere in campo non dico un designer, ma un umile grafico, non ha provveduto alcuno.

Perché *amministrazione pubblica* deve essere sinonimo di *squallore*?