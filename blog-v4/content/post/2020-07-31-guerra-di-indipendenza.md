---
title: "Guerra di indipendenza"
date: 2020-07-31
comments: true
tags: [Apple]
---
I risultati trimestrali di Apple [hanno ridicolizzato le previsioni degli analisti per quanto sono buoni](https://tidbits.com/2020/07/30/apple-q3-2020-breaks-records-while-the-world-burns-next-iphone-to-be-fashionably-late/) e trasmettono una lezione importante per il prossimo periodo che ci aspetta. Perché la ricaduta pandemica sul lavoro abbiamo appena iniziato a vederla e, se Apple ha incassato cinquantanove miliardi invece dei cinquantadue che ci si aspettava, per un sacco di attività piccole e grandi non andrà per niente bene.

Comprare un iPad non risolverà un licenziamento o un crollo di fatturato. Però, come dicevo, questi risultati portano una lezione importante che inoltro come consiglio: _indipendenza_.

Indipendenti dalle condizioni di lavoro. Apple si è costruita un’ astronave da dodicimila posti e adesso quei tutti stanno lavorando da casa. Attenzione a non fraintendere tra smart working, remote working, home working. Smart workimg è poter lavorare da ovunque, quando serve e dove serve. Lavorare sempre dall’ufficio è suicida, lavorare solo da casa è da fessi (_disclaimer_: lavoro da casa). Lavorare da ovunque, questo è smart.

Indipendenza dal software. Tranquilli che in Apple, l’azienda con la più alta capitalizzazione al mondo, nessuno è impiccato al file Word o alla tabella Excel. Usano Excel se c’è motivo di farlo. Se sai usare solo Word, se usi Excel per disegnare e niente altro, è meglio rinfrescare il curriculum.

Indipendenza dalla posta elettronica. In questi giorni vedo cose terrificanti; la posta elettronica è sinonimo di scaricabarile o di braccio di ferro tra cliente e fornitore, a chi ha la signature più tronfia, a chi accoda il maggior numero di messaggi senza mai preoccuparsi di togliere una riga inutile che sia una, a chi riesce a mettere in copia il gruppo più numeroso con il solo intento di infastidire. Parola d’ordine: affidarsi ad ambienti di chat collaborativi e non dico altro, ognuno sceglierà.

Apple è estremamente organizzata e automatizzata e ci conviene imitarla, altrimenti è un attimo diventare improduttivi. Ci sono aziende nelle quali entropia e confusione sono la norma, voluta da qualcuno come ambiente dove è più facile imporre un’autorità fine a se stessa. L’improvvisazione non pagherà più. Indipendenza dall’imprevisto.

Indipendenza dalla burocrazia. Abbattere procedure, firme, registri e fare app, autenticazioni automatiche, codici Qr. Più tempo si perde nelle scartoffie, più tempo mancherà quando diventa cruciale.

Indipendenza dalle _call_. In Microsoft hanno notato che, con tutti a lavorare da casa, la durata delle riunioni si è dimezzata. Chi si lamenta di passare otto ore al giorno in call sta in un’azienda dove non si è capito niente. La media giornaliera perfetta di videochiamata per un’organizzazione efficiente è di un quarto d’ora effettivo, tolti i saluti e lo _smalltalk_, il chiacchiericcio. Facciamo passare impegni e calendari da bacheche condivise, nelle quali l’ottanta percento delle questioni si risolve con una riga di messaggio.

Indipendenza dall’hardware. Alla bisogna, deve poter andare bene un Mac, un iPad, un iPhone perfino. Guai a restare vincolati a una macchina specifica. Se abbiamo difficoltà vuol dire che qualcosa è sbagliato; se non i programmi i formati, se non i formati il cloud. Il livello di iCloud da cinquanta gigabyte costa un euro al mese. Capisco che è il momento di stare attenti alle spese. Un euro al mese, tuttavia, non è una spesa. Piuttosto pensiamo al tempo perso in un mese per stare dietro ai capricci di Dropbox o stare nei cinque gigabyte gratis di iCloud e pensiamo se quel tempo, nel mese, valga un euro. Vale molto di più.

Indipendenza di pensiero. Se capiamo che una cosa va fatta diversamente, va fatta diversamente. Non è tempo di ossequio servile, le cose devono funzionare.

Più un’azienda, un professionista, un docente è indipendente, più ha probabilità di uscire dal Covid a testa alta e riuscire pure a pagare le tasse il prossimo anno.