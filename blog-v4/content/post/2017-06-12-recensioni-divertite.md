---
title: "Recensioni divertite"
date: 2017-06-12
comments: true
tags: [Sinofsky, Apfs, Wwdc]
---
C’è del metodo nella follia di chi ti accusa di essere un tifoso, un fanatico, un esagitato, uno con le fette di salame sugli occhi, incapace di una valutazione imparziale e tutto perché hai parlato bene di un prodotto, di una funzione, di un sistema, di un’azienda.<!--more-->

Il metodo è attribuirsi la patente di obiettività da soli in modo da poter squalificare qualunque altro parere scomodo e avere sempre ragione *by design*, in modo progettato a priori. Di peggio c’è solo il politicamente corretto.

A questi folli chiederei una valutazione spassionata del racconto di Wwdc 2017 [scritto da Steven Sinofsky su Recode](https://www.recode.net/2017/6/12/15785624/apple-wwdc-2017-developers-conference-ios-11-imac-ipad-pro-homepod-high-sierra).

Perché Sinofsky ha rivestito incarichi di altissima responsabilità in Microsoft e, come egli stesso scrive, ha partecipato più volte a Wwdc di volta in volta come partner, concorrente, sviluppatore. Se quindi titola *Quello che Apple ha mostrato a Wwdc espande il computing in nuovi modi* significa che non parla a vanvera, avendo benissimo presente anche l’altra faccia della luna. Recode è tacciabile di tutto tranne che di fanatismo verso Apple. Sinofsky ha anche un retroterra tecnico di tutto rispetto e scrive cose come queste:

>La quantità di straordinaria ingegnerizzazione che è stata messa sia nella creazione che nel dispiegamento di Apfs [il nuovo filesystem di Apple] fa scoppiare la testa. E che questo avvenga su telefoni, computer e orologi è niente meno che spettacoloso; eccetto forse per la transizione da Fat a Fat32, non riesco a ricordare alcunché che nemmeno gli si avvicini.

Interesse recondito? Difficile. I suoi passati incarichi gli hanno garantito tranquillità economica sufficiente a fare cose che gli piacciono, come testimoniano le righe a firma dell’articolo: consigliere di amministrazione qui, consulente là, investitore quando ha voglia eccetera. Non è uno che scrive su Recode per vivere, ma per divertirsi. Vediamo se c’è qualche obiettivo autoproclamato capace di criticare in modo fattuale le sue affermazioni.