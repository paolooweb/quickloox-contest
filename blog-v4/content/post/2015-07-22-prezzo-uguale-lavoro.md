---
title: "Prezzo uguale lavoro"
date: 2015-07-24
comments: true
tags: [Apple, Cook, Sacconaghi, Sanford, Bernstein, iPad, Mac, iPhone]
---
I [risultati finanziari di Apple](http://events.apple.com.edgesuite.net/15piuhasdfvlhbvlhbasvhb07/event/index.html) non sono una cerimonia particolarmente esaltante e non è neanche molto divertente ascoltarli: la qualità dell’audio è telefonica e gli analisti sono specializzati nel mangiarsi le parole, borbottare, adoperare gergo klingoniano e dare per scontate due parole su tre per ciascuna frase.

Nonostante questo c’è stato uno scambio di battute interessante tra Toni Sacconaghi di Sanford Bernstein e Tim Cook, amministratore delegato di Apple. Sacconaghi ha chiesto più o meno in che fascia di mercato cellulare vuole giocare Apple: se intenda stare [tra i telefoni sopra i trecento dollari](https://twitter.com/sixcolorsevent/status/623608819847966720) (e quindi avere una quota di mercato del 60 percento) oppure sopra i seicento dollari ([e avere una quota di mercato del 90 percento](https://twitter.com/sixcolorsevent/status/623608908112896001), a parità di venduto).

La solita tiritera noiosa sulla quota di mercato (a proposito: [Mac cresce del nove](https://twitter.com/sixcolorsevent/status/623602866821943296) e i [PC diminuiscono del dodici](https://twitter.com/sixcolorsevent/status/623602866821943296), [iPhone cresce il triplo del mercato](https://twitter.com/sixcolorsevent/status/623605361782730756)). Cook ha risposto spiegando che Apple [vede le cose in maniera diversa](https://twitter.com/sixcolorsevent/status/623609001914163205) e che il risultato [dipende dal lavoro che si svolge](https://twitter.com/sixcolorsevent/status/623609064388435968).

<blockquote class="twitter-tweet" lang="en"><p lang="en" dir="ltr">Tim: We look at it a bit differently than you do. Our job is to grow our products, regardless of the price. Means we need to convince people</p>&mdash; Six Colors liveblog (@sixcolorsevent) <a href="https://twitter.com/sixcolorsevent/status/623609001914163205">July 21, 2015</a></blockquote> <script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>

<blockquote class="twitter-tweet" lang="en"><p lang="en" dir="ltr">Tim: … to move from one price band to another. If we do a great job with product, people will be willing to spend more. Look at results.</p>&mdash; Six Colors liveblog (@sixcolorsevent) <a href="https://twitter.com/sixcolorsevent/status/623609064388435968">July 21, 2015</a></blockquote> <script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>

La traduzione è più o meno questa:

>Noi guardiamo [alla questione] in modo leggermente differente da te [Sacconaghi]. Il nostro lavoro è fare crescere i nostri prodotti, indipendentemente dal prezzo. Significa che dobbiamo convincere la gente a cambiare fascia di prezzo. Se facciamo un ottimo lavoro con il prodotto, le persone saranno disposte a spendere di più. I risultati parlano da soli.

Ecco, non è che iPhone se lo possano permettere in pochi. Se lo vogliono permettere quelli che ci vedono qualcosa di ottimo. Per quelli che tutti-i-telefoni-sono-uguali, è chiaro che il prezzo sia più alto e che esistano pure le quote di mercato.
