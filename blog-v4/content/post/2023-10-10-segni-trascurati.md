---
title: "Segni trascurati"
date: 2023-10-10T17:07:12+01:00
draft: false
toc: false
comments: true
categories: [Internet]
tags: [Burger King, apostrofo, tipografia, Gruber, John Gruber, Daring Fireball, Washington Post]
---
Passo prima [da Daring Fireball](https://daringfireball.net/linked/2023/10/09/burger-king-apostrophe) perché [l’articolo del Washington Post](https://www.washingtonpost.com/dc-md-va/2023/10/08/burger-king-apostrophe/) sta su un sito irritante nel continuo richiedere iscrizioni, abbonamenti, scelte di lettura fino a precluderla, la lettura.

La sostanza è che *Burger King* ha trasmesso uno spot televisivo che ha indignato Gruber e il *Post* per la presenza di un apostrofo tipografico sbagliato, *‘em* invece di *’em*.

Nel mio piccolissimo, condivido l’indignazione. La punteggiatura fa parte del linguaggio e quindi della piattaforma comune per scambiare informazione. Un conto è che la piattaforma evolva, un conto è sbriciolare la ricchezza cui l’ha portata l’evoluzione.

In Italia si fa strame degli apostrofi tipografici ovunque ed è solo una parte del problema, che è nazionale. Quotidiani, siti di editori, pubblicità sui media di prima fascia ignorano o trascurano l’aspetto tipografico alla prima occasione, non appena c’è di mezzo un impaginatore sufficientemente ignorante oppure un copywriter improvvisato. Il controllo, la supervisione, la rilettura non esistono più (questo blog è famoso per i suoi refusi, ma non per gli apostrofi e gli accenti. I refusi sono una parte inevitabile seppure fastidiosissima dello scrivere).

Il peggio assoluto sono quelli che scrivono come programmano. E che non programmano, ma scrivono. E occasionalmente neanche sanno scrivere, per cui usano gli apici, quelli diritti, quelli non tipografici, quelli riccioli, quelli curvi, quelli come la schiena del 6 o del 9, quelli che ognuno chiama come gli pare ma sono sempre quelli.

Sono le vittime inconsapevoli della grande rapina compiuta dall’informatica cinquant’anni fa. Ci diedero macchine con i caratteri Ascii, duecentocinquantasei se andava bene e non erano centoventotto. Era molto avere le accentate e infatti i PC non le avevano, o erano nascoste negli inferi del sistema. Lì i primitivi impararono ad accendere il fuoco e a scrivere lettera-più-apostrofo in luogo dell’accento.

Poi arrivò Macintosh. Arrivò la LaserWriter. Arrivarono i font. O meglio tornarono; l’informatica aveva rimosso in pochi anni secoli di sapienza tipografica, di grazie ma anche di grazia, di bellezza, di eleganza. Era l’inizio del ritorno.

Poi Unicode, poi i Css, poi gli schermi Retina. Non siamo ancora alla Restituzione Totale del Bottino; mancano ancora tante cose. Tuttavia è di nuovo possibile praticare buona tipografia.

Il peccato più grave è insistere a non accorgersene e allevare ancora una volta nuove generazioni agli apici dritti, agli apostrofi usati come accenti, alla confusione mentale di chi dovrebbe sapere scrivere *débâcle*, non per sapere il francese, ma per sapere produrre un circonflesso.

Se la fine del mondo si avvicinerà, la riconosceremo dai segni. Soprattutto da quelli che saranno spariti nell’indifferenza. Difendiamo i segni del linguaggio.