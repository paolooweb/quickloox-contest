---
title: "Mancanze di collaborazione"
date: 2016-05-01
comments: true
tags: [Pages, iCloud, tracking]
---
Da iPad apro con Pages un documento presente su iCloud e condiviso con un amico. Applico il tracciamento delle modifiche, così che l’amico possa verificare più tardi il complesso delle variazioni che ho inserito.

Solo che l’amico non riesce ad accedere: il suo Pages gli spiega che prima devo spegnere io il *tracking*, altrimenti può solo guardare il documento senza modificarlo.

Spengo il *tracking*, per sentirmi dire che devo approvare o rigettare in blocco tutte le modifiche. In qualsiasi caso, la mossa nega tutti i benefici del *tracking*. E allora tanto valeva non averlo neanche.

iCloud è tanto comodo ma su questi dettagli chiede ancora lavoro. Sono dettagli ma – se è cloud vero – sono anche essenziali, perché il cloud senza collaborazione è zoppo.
