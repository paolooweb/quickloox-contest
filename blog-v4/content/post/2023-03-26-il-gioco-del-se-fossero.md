---
title: "Il gioco del se fossero"
date: 2023-03-26T02:47:10+01:00
draft: false
toc: false
comments: true
categories: [Software, Web]
tags: [Abstract Reasoning Corpus, Arc, Chollet, François Chollet]
---
Se fossero veramente intelligenze artificiali, eccellerebbero nel set di problemi contenuti dentro l’[Abstract Reasoning Corpus](https://bdtechtalks.com/2020/02/19/kaggle-arc-challenge-francois-chollet/).

Il Corpus è stato allestito da François Chollet, ricercatore di spicco nel campo di machine learning e ricerche sull’intelligenza sintetica, nel 2020, con tanto di sfida ufficiale e premio di ventimila dollari per chi avesse saputo presentare un meccanismo di pensiero digitale in grado di superare le prove.

Ci hanno provato in centinaia e nessuno c’è riuscito. La parte interessante è che sono passati anni e nessuno c’è ancora riuscito, neanche i fenomeni che oggi riempiono le cronache.

I problemi del Corpus partono dalla constatazione che i sistemi prodotti fino a oggi superano agevolmente le capacità umane in campi specifici di grande complessità, ma sono inabili a trattare problematiche elementari che anche un bambino affronta senza pensarci. E non avremo intelligenza sintetica fino a quando mancheranno queste capacità.

L’articolo linkato contiene un ottimo sunto della situazione, con tutti i link per approfondire, ed è perfetto per farsi un’idea veloce ma completa della situazione.

Se fossero gente onesta, non le chiamerebbero intelligenze artificiali.