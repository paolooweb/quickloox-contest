---
title: "Sulla distanza"
date: 2020-04-04
comments: true
tags: [iPad, Editorial, Kodex, Drafts, emacs, ssh, Dropbox, BBEdit, Prompt]
---
IPad [ha appena compiuto dieci anni](https://www.macrumors.com/2020/04/03/prototype-ipad-story/) come [epitome del computer modulare](https://www.macstories.net/stories/modular-computer/) e in questi giorni lo uso sempre più spesso.

Così mi accorgo di dove lo stato dell’arte è ancora inferiore a Mac: oggi, per esempio, [Editorial](https://omz-software.com/editorial/) ha stentato alle prese con un file testo ostico, da due megabyte. C’era di mezzo anche la sincronizzazione con Dropbox, ma non ci sono scuse: su Mac un file di testo da due megabyte neanche inizia a essere un problema.

Più che una critica a Editorial che continuo ad apprezzare, è una constatazione rispetto alle condizioni del campo di gioco. Infatti Editorial è quello che se l’è cavata meglio rispetto a [Kodex](https://kodex.app/) e [Drafts](https://getdrafts.com/). Test complessivamente piccolo e di nuovo lo stesso rimarco: un file due megabyte non può imbarazzare un buon editor. Sentivo BBEdit sghignazzare su Mac mini nell’altra stanza.

Me la sono cavata barando: mi sono collegato in ssh a Mac e, da [Prompt](https://panic.com/prompt/), ho lanciato [emacs](https://www.gnu.org/software/emacs/) su iPad.

Ambiente da levigare, rispetto a un uso su una tavoletta. Come scorciatoia occasionale, tuttavia, ha funzionato alla grandissima.
