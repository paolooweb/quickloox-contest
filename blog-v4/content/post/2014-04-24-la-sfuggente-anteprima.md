---
title: "La sfuggente anteprima"
date: 2014-04-25
comments: true
tags: [iCloud, Dropbox, Pages, iWork]
---
Personalmente passerei documenti Pages tra Mac e iOS approfittando di iCloud, che è praticamente la morte sua. Però capisco l’esigenza di **Fabio**, voler avere un unico flusso di anteprima per tutti i documenti e quindi fare passare tutto tramite Dropbox, iWork compreso.<!--more-->

Fabio ha notato che la *app* Dropbox per iOS fornisce un’ottima anteprima per i documenti Office, per esempio; invece – su un iPad aggiornato – toccare un documento Pages o Numbers fa apparentemente bloccare il programma, che rimane per sempre ad annunciare il caricamento dell’anteprima senza mai mostrarla.

C’è il trucco. Toccato il documento iWork nella *app* Dropbox, ordiniamo l’inoltro a un’altra *app* con il pulsante a forma di freccia che esce dalla scatola. La app mostra per qualche secondo – a me è successo – un enigmatico messaggio di *esportazione*. Poi compare l’elenco dei programmi compatibili all’inoltro e, sotto, si vede l’anteprima desiderata.

Possiamo anche annullare l’inoltro. Da quel momento il documento verrà mostrato correttamente nell’anteprima di Dropbox, che speriamo metta a posto questo fastidio in un prossimo aggiornamento. Poteva andare peggio.
