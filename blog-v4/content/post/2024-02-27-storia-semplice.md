---
title: "Storia semplice"
date: 2024-02-27T00:49:12+01:00
draft: false
toc: false
comments: true
categories: [Internet, People]
tags: [Gottesman, Ruth Gottesman, David Gottesman, Albert Einstein College of Medicine, Buffett, Warren Buffett, Berkshire Hathaway]
---
Ci vogliono anche storie belle come [quella della dottoressa Ruth Gottesman](https://news.slashdot.org/story/24/02/26/1438202/1-billion-donation-will-provide-free-tuition-at-a-bronx-medical-school), per molti anni docente presso l’Albert Einstein College of Medicine di New York.

L’ultimo marito della dottoressa, David Gottesman, aveva effettuato un investimento nella società Berkshire Hathaway, seguendo i buoni auspici di Warren Buffett. La società è diventata nel tempo una delle ammiraglie del finanziere e ha reso ai suoi investitori un bel po’ di soldi.

Ora che ha novantatré anni ed è vedova, la dottoressa Gottesman ha deciso di lasciare un miliardo di dollari al college presso cui ha insegnato per tanto tempo, con la precisa istruzione di usarli per consentire agli studenti di frequentare gratis.

È uno dei lasciti più grandi di sempre a favore di una istituzione *education* e certamente il più grande per una scuola di medicina. Ma non è tutto; tante persone facoltose hanno lasciato denaro a scuole di New York, ma sempre ricche e in quartieri agiati. L’Albert Einstein College of Medicine sta nel Bronx, il quartiere più povero della città.

È una storia semplice, quasi commovente pensando a quanti ragazzi del Bronx potranno laurearsi in medicina e quante vite cambieranno per sempre, in meglio, direttamente e indirettamente.

È talmente semplice che sarebbe da consigliare ai miliardari. Ogni tanto li sentiamo che vorrebbero pagare più tasse, o auspicare tasse di successione sempre più salate per quelli come loro, oppure spiegare che ai loro figli verrà lasciato un patrimonio modesto, mentre tutto il resto andrà in beneficenza.

In attesa di vedere tutto questo avverarsi, è così semplice. Si prende un miliardo e lo si lascia a una università in un quartiere povero. E poi ci sono ospedali, parrocchie, centri di ricerca, munizioni per l’Ucraina, enti benefici, associazioni filantropiche, progetti per migliorare le condizioni di vita in ogni parte del mondo, malattie rare, terapie palliative, scuole di ogni ordine e grado, incubatori di startup etiche, l’imbarazzo della scelta è chiaro ma si può anche superare.

Miliardari: un miliardo. Se ha potuto farlo Ruth Gottesman che non aveva certo gli onori delle cronache, potete farlo in centinaia, migliaia. Scegliete una causa. Aiutatela. È così semplice e vi costa una frazione di patrimonio che probabilmente recupererete pure. La dottoressa ci ha messo niente.