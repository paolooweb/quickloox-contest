---
title: "Provaci ancora, zio Sam"
date: 2016-02-24
comments: true
tags: [Severgnini, Sideri, TouchID, iPhone, terrorismo, iCloud, backup, Mdm]
---
Cose che [Beppe Severgnini e Massimo Sideri](https://macintelligence.org/posts/2016-02-19-il-corriere-degli-intrusori/) non riusciranno mai a scrivere: i datori di lavoro pubblici del terrorista di San Bernardino [non usavano un sistema di Mobile Device Management](http://www.cbsnews.com/news/common-software-would-have-unlocked-san-bernardino-shooters-iphone/) (Mdm), come fanno tutte le aziende sensate di questo mondo.<!--more-->

Ci fosse stato un Mdm, l’apparecchio avrebbe potuto essere stato comodamente sbloccato dai datori di lavoro anche in assenza del terrorista, che sono i possessori legittimi dell’apparecchio, senza scomodare versioni su misura di iOS.

Non era nemmeno attivo TouchID, il che avrebbe permesso all’Fbi di sbloccare l’iPhone usando il cadavere del terrorista.

Non finisce qui: l’Fbi ha chiesto alla Contea di San Bernardino – i datori di lavoro – di [resettare l’ID Apple del terrorista](http://www.buzzfeed.com/johnpaczkowski/apple-terrorists-appleid-passcode-changed-in-government-cust#.fb8gR8GNN0). Questo ha eliminato ogni possibilità di attivare un backup su iCloud dell’apparecchio.

Uno dei punti dolenti dell’intera questione è infatti che l’ultimo backup su iCloud dell’iPhone data a sei settimane prima dell’attentato. Se iPhone è bloccato ma si trova in un Wi-Fi conosciuto ed è collegato alla corrente, effettua comunque il backup. Significa che l’FBI avrebbe potuto semplicemente collegare l’apparecchio del terrorista nel suo ufficio e, se si fosse collegato al Wi-Fi, attendere comodamente un backup su iCloud. Apple fornisce su richiesta legittima del governo americano i contenuti di un backup iCloud e lo ha già fatto nel caso di San Bernardino, per quello che era disponibile.

Si noti che l’Fbi ha [inizialmente nascosto](https://assets.documentcloud.org/documents/2716011/Apple-iPhone-Access-MOTION-to-COMPEL.pdf) al giudice di avere chiesto il reset della *password*, per essere poi sbugiardata da una [frasetta](https://twitter.com/CountyWire/status/700887823482630144?ref_src=twsrc%5Etfw) della Contea su Twitter:

<blockquote class="twitter-tweet" data-lang="en"><p lang="en" dir="ltr">The County was working cooperatively with the FBI when it reset the iCloud password at the FBI&#39;s request.</p>&mdash; CountyWire (@CountyWire) <a href="https://twitter.com/CountyWire/status/700887823482630144">February 20, 2016</a></blockquote> <script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>

Lo stallo attuale tra Apple e FBI è frutto di una robusta catena di incompetenze, se proprio si vogliono escludere la malafede o l’intenzione di creare ad arte un caso. L’Fbi poteva avere i dati che desidera senza chiedere ad Apple di indebolire la sicurezza di iPhone con una versione *ad hoc* del software di sistema. La Contea di San Bernardino non sa amministrare il parco telefoni dei propri dipendenti. Propaggini governative che non stanno dando il meglio.

Pare che la maggioranza degli americani [sostenga le ragioni dei federali](http://www.people-press.org/2016/02/22/more-support-for-justice-department-than-for-apple-in-dispute-over-unlocking-iphone/) più che quelle di Apple. [Aggiornamento: pare anche [il contrario](http://www.reuters.com/article/us-apple-encryption-poll-idUSKCN0VX159)]. Suggerirei loro di rileggere la [lettera aperta di Tim Cook](http://www.apple.com/customer-letter/), specie ora che hanno aggiunto anche le [risposte alle domande più frequenti](http://www.apple.com/customer-letter/answers/).