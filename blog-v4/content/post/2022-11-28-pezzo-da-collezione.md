---
title: "Pezzo da collezione"
date: 2022-11-28T16:44:00+01:00
draft: false
toc: false
comments: true
categories: [Internet]
tags: [Wi-Fi, Frecciarossa]
---
Poi sono riuscito addirittura a sostenere mezzo minuto di videoconferenza prima che tutto crollasse miseramente e io passassi allo hotspot come abbondantemente preventivato e predisposto. Ma prima, un pezzo che secondo me rischia di essere da collezione.

![Messaggio esoterico di errore da Wi-Fi su Frecciarossa](/images/not-found.png "Che poi, il pensiero dell’accelerazione associato al Wi-Fi del Frecciarossa è anche un ossimoro da premio letterario.")

Ho cercato di figurarmi che cosa sia Accelerator e nel caso delle Frecce riesco ad associarlo solo a richiami lovecraftiani, circoli esoterici, biblioteche arcane. Si dovrà approfondire, ma lo lascio ai più volonterosi.