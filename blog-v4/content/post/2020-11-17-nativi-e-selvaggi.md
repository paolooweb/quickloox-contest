---
title: "Nativi e selvaggi"
date: 2020-11-17
comments: true
tags: [M1, Geekbench, Apple, Siegler, Rosetta, single-core, multi-core, Intel, Arm]
---
Mi si scuserà se insisto sul tema di M1. Forse qualcuno ricorderà i tempi in cui Apple aveva perso la strada dell’innovazione. Non succedeva niente, secondo i detrattori. Qui, sembra che succedano cose, perché i detrattori si agitano a far sembrare il moto browniano una processione.

Succede che appaiono test di prestazioni sempre nuovi e adesso si legge che Rosetta 2, l’emulazione per le app Intel, [batte nei test Geekbench single-core gli altri Mac](https://twitter.com/mgsiegler/status/1328117006017544192).

<blockquote class="twitter-tweet"><p lang="en" dir="ltr">Yeah this is wild. <a href="https://t.co/asJK8uTvnq">https://t.co/asJK8uTvnq</a></p>&mdash; M.G. Siegler (@mgsiegler) <a href="https://twitter.com/mgsiegler/status/1328117006017544192?ref_src=twsrc%5Etfw">November 15, 2020</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

È *selvaggio*, per riprendere il *tweet* di Siegler, persino scriverlo: *una applicazione Intel emulata su M1 va più veloce in single-core della stessa applicazione in esecuzione nativa su un Mac Intel*.

Lo so quello che sta per arrivare. I test di velocità sono come giocare a chi fa la pipì più lontano, ci sono situazioni in cui l’uso di un solo core del processore non avviene e le prestazioni multi-core non sono altrettanto competitive eccetera eccetera. Lo so perché ho fatto un giretto su Facebook e tutti i circoli dei so-tutto traboccano di questi pareri.

Sono i pareri di quelli che ieri, quando usciva il nuovo processore Intel, si fiondavano su Geekbench a emozionarsi con i guadagni in prestazioni. E vabbeh, se non altro inizia a essere più chiaro che i benchmark non sono la vita reale. Quando Mac aveva processori inferiori, lo scrivevo sempre. Lo scrivo anche oggi.

Ma non è il punto. Il punto è che i so-tutto sapevano che l’emulazione Intel sarebbe stata inaccettabile, troppo lenta, impossibile da adottare a cuor leggero per il professionista, il *power user*, l’utente evoluto.

Ora che i benchmark raccontano una storia diversa, che l’emulazione è certo una emulazione ma è competitiva, mi dicono che i benchmark non contano niente.

Ne sono anche abbastanza convinto. Ma loro, su che dati si basano? Chiedo per un amico.
