---
title: "Il cerchio si stringe"
date: 2022-12-10T00:51:47+01:00
draft: false
toc: false
comments: true
categories: [Internet]
tags: [Poste, Pec]
---
Poste Italiane, che fornisce la mia casella di posta elettronica certificata, mi ha spedito un messaggio che, secondo il server di posta elettronica certificata di Poste Italiane, non può essere certificato perché il mittente non è certificato.

Non è un phishing.

Più ci penso e più temo di entrare in una spirale capace di disintegrarmi dentro una singolarità finale.