---
title: "LG, il prezzo è giusto"
date: 2021-04-06T00:40:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [LG, Apple, Samsung, iPhone] 
---
LG [è uscita dal business dei computer da tasca](https://www.theverge.com/2021/4/4/22346084/lg-exits-smartphone-business) insegnandoci diverse lezioni.

La prima sembra ovvia, ma palesemente va ricordata: è poco saggio investire denaro su prodotti di aziende che lavorano in perdita. LG è un colosso globale in salute, eppure la sua divisione *mobile* era in rosso da più di cinque anni in misura disastrosa, con fatturati da quattro-cinque miliardi di dollari e margine negativo di settecento-ottocento milioni ([750 milioni nel 2020](https://www.gizmochina.com/2021/01/30/lgs-profit-in-2020-hit-a-record-high-but-the-mobile-phone-arm-fails-to-rebound/), [850 nel 2019](https://www.androidcentral.com/lgs-struggling-mobile-division-lost-over-850-million-2019)). Una situazione che difficilmente può durare a lungo.

La seconda lezione è quella sul valore delle [trovate tecnologiche fini a se stesse](https://twitter.com/MKBHD/status/1378894128952573959).

<blockquote class="twitter-tweet"><p lang="en" dir="ltr">Pour one out for LG Mobile. They&#39;re the reason we have ultrawides in every new phone right now. They didn&#39;t always ace every phone, but losing them means losing a competitor that was willing to try new things, even when they didn&#39;t work. <a href="https://t.co/x6rXDhlVfN">pic.twitter.com/x6rXDhlVfN</a></p>&mdash; Marques Brownlee (@MKBHD) <a href="https://twitter.com/MKBHD/status/1378894128952573959?ref_src=twsrc%5Etfw">April 5, 2021</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

Essere *disposti a provare cose nuove, anche se non funzionano*, vuol dire fare esperimenti con il portafogli dei clienti paganti. LG sarebbe stata la prima a introdurre fotocamere *ultrawide*; ha proposto [design ricurvi e flessibili](https://edition.cnn.com/2013/10/08/tech/mobile/lg-flexible-display/index.html), [schermi rotanti](https://edition.cnn.com/videos/business/2020/09/14/lg-wing-5g-phone-orig.cnn-business/video/playlists/business-mobile-wireless/), [modelli arrotolabili](https://edition.cnn.com/2021/01/12/tech/ces-2021-day-2-highlights/index.html). Mancano solo le alabarde spaziali.

Il lettore di bocca buona si stupisce, pensa *innovazione!* e si chiede perché queste cose non le faccia Apple. In generale, se Apple non le fa, è perché non sono ancora pronte, o non piacciono alla gente che dovrebbe comprarle. Qui si preferisce una azienda che si faccia gli esperimenti nella propria stanzetta, in senso figurato, e proponga unicamente cose che funzionano e possono davvero essere utili, dopo avere scartato le idee simpatiche ma impraticabili.

Terza lezione: secondo una narrativa molto diffusa, gli smartphone costano cifre esagerate e, in particolare, gli iPhone più di tutti. La realtà è diversa: per produrre oggetti tecnologicamente evoluti come quelli attuali in termini di miniaturizzazione, autonomia, potenza, e farlo su un percorso di miglioramento costante negli anni, molto probabilmente i prezzi che ne conseguono sono *necessari*. Si può obiettare magari sull’entità dei margini di profitto; la base dei costi, tuttavia, è una dura evidenza per le aziende non preparate. Nel mercato attuale fanno soldi Apple, Samsung e poi rimangono le briciole.

L’ultima lezione.

*Il prodotto è un pretesto*. Nel 2021 si compra un ecosistema, di cui lo hardware è semplicemente l’aggancio materiale, come già scrivevo [a proposito delle stampanti](https://macintelligence.org/posts/La-stampante-che-non-cè.html). LG poteva avere gli schermi delle meraviglie, ma il suo ecosistema tende a zero. Non stupisce che i profitti seguissero la stessa traiettoria.

*Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile.*