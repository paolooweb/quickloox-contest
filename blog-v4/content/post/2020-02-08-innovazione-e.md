---
title: "Innovazione è"
date: 2020-02-08
comments: true
tags: [Jobs, NeXT, Apple]
---
Ventitré anni fa Steve Jobs cominciava il suo primo giorno di lavoro in Apple dopo essere stato acqusito assieme alla sua NeXT.

Un’azienda allo sbando ne acquisiva una di sostanziale insuccesso, per quattro soldi (quattrocento milioni sono nulla rispetto, per esempio, alla acquisizione plurimiliardaria del business cellulare Nokia da parte di Microsoft), capitanata da un manager ai tempi cacciato dall’amministratore delegato che egli stesso aveva convinto personalmente a occuparsi di Apple.

Il manager recuperava dai ranghi dell’azienda un designer trascurato e demotivato per trasformare una Apple a rischio fallimento nell’azienda di tecnologia più valutata al mondo, con una base di utenza di oltre un miliardo di persone.

Oggi va di moda accusare Apple di carenza di innovazione. Chiedo: quale altra azienda ha una storia minimamente avvicinabile a questa?

Innovazione è altro, dicono? Beh, facile concordare. Cambiare il destino di miliardi di persone è un’altra cosa. Talento, intuito, coraggio e sangue molto freddo non sono state innovazione, ma la testimonianza che lì in quel momento entrarono in Apple varie persone speciali.

E forse tutta questa parte meriterebbe un capitolo di storia a sé, lontano da come è nato iPhone o iPad.
