---
title: "Vivere a colori"
date: 2019-07-17
comments: true
tags: [Apple, logo, MacRumors, Rossignol]
---
Che cosa fai se la giornata ti sembra fiacca e la pagina del sito langue? Superi di slancio il blocco dello scrittore come ha fatto [MacRumors](https://forums.macrumors.com/threads/apples-rainbow-logo-may-return-to-some-new-products-as-early-as-this-year.2190102/):

>Il logo Apple iridato potrebbe tornare su alcuni prodotti già da quest’anno.

Forte, eh? Ma c’è dell’altro:

>Per chiarezza, questo rumor potrebbe benissimo non essere vero.

E ora occorre tenersi forte perché arriva il colpo a sorpresa:

>E perfino se fosse vero, i piani potrebbero certamente cambiare.

Quello che si apprezza dell’articolo non è solo la precisione, ma anche il puntiglio nell’evitare ogni equivoco:

>Ancora, non sappiamo se questa voce sia fondata.

Insomma, la soluzione è chiara: per vincere la noia, niente batte  un tocco di colore. Anzi, sei.
