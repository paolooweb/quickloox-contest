---
title: "La catena del disvalore"
date: 2023-03-16T14:45:20+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [Elementor, WordPress, plugin]
---
Perché sprecare tempo con i film dell’orrore quando esistono i plugin di WordPress? Oggi, nuova giornata nuove avventure, ho fatto la conoscenza con [Elementor](https://elementor.com), un autodichiarato *editor a blocchi* progettato da disagiati che di blocchi devono averne di importanti. Disagiati di cui è pieno il mondo, che meritano il massimo rispetto, ma con questa predisposizione verso la progettazione delle interfacce richiamano alla mente l’ultimo passaggio di secolo.

Lavoravo attorno al Duemila per una startup e discutevo animatamente con il direttore dello sviluppo software; il nostro compito era mettere a punto un editor di testo innovativo, open source, che andasse un gradino oltre la produzione corrente.

Non se ne fece niente. Immaginavo una tipografia avanzata e la libertà di manipolazione degli elementi sullo schermo. Volevo un canvas estremamente flessibile, con tipografia evoluta, appoggiato su un database.

Lui invece voleva un database superpotente e veloce, che gestisse il testo. Non era l’obiettivo e l’approccio era sbagliato: era l’editor, in un progetto del genere, che avrebbe dovuto dettare le regole al database. BBEdit, non FileMaker.

I disagiati dietro Elementor hanno compiuto un passo in avanti decisivo rispetto a questo quadro: pensano al database. E basta. A che serve un’interfaccia, meglio, la progettazione di un’interfaccia, quando una macro crea un pulsante uguale per ciascuno dei centoquattordici comandi che ho sviluppato? A che pro considerare la struttura di un documento, di una pagina, con tutto quello che si può risparmiare se la si ignora?

A loro credito va l’essersi inseriti con profitto in una catena del valore che funziona in senso opposto a tutte le altre. Con tutti i suoi difetti, WordPress ha un valore apprezzabile come sistema di base di creazione e pubblicazione. Gli anelli successivi della catena dissipano il valore, che tende asintoticamente a zero con l’aggiunta di plugin sempre più ambiziosi e sempre più assurdi a livello di esperienza utente.

Il meccanismo funziona perché tutti ci guadagnano. Automattic mette una copia di WordPress ovunque, perché è gratis, perché *è come scrivere in Word*. Chi sviluppa i plugin può contare sul fatto che Automattic mette WordPress ovunque e, se è bravo, vende perché è bravo; se fa schifo, vende a quelli che non lo sanno.

Rotoliamo un gradino più in basso e troviamo uno che sceglie il plugin, tanto lo paga qualcun altro. Gestisce un processo, ha una responsabilità e nessuno è mai stato licenziato per avere scelto WordPress. Come se avesse mai considerato una alternativa.

Quello che paga è contento di essersi liberato da una seccatura con poca spesa, senza dover pagare uno sviluppo web autentico. Neanche vuole sapere come si chiama il plugin. Che gli frega?

Si arriva a uno, di solito in quota *mio cugggino*, che monta il plugin. Pagato due soldi, smanetta, sacramenta ma alla fine riesce. Al più, qualcosa non funziona. Pazienza, vendiamo un abbonamento all’aggiornamento & manutenzione e prima o poi riusciremo comunque a fare funzionare quella roba. Che deve funzionare, mica velocizzare o migliorare.

In fondo alla catena c’è il servo della gleba. Gli tocca WordPress che ha scelto qualcun altro e gli tocca il plugin deciso da qualcun altro, comprato da qualcun altro, montato da qualcun altro ancora, scritto da un disagiato. A nessuna di queste figure interessa un centimetro delle necessità del servo della gleba, perché tutti hanno già ricevuto la loro ricompensa, che il sistema funzioni o meno.

Il valore si è esaurito passo dopo passo. Il servo della gleba viene stressato e sollecitato perché produca in fretta bene e a poco prezzo su un sistema che fa sanguinare gli occhi, largamente incomprensibile al primo sguardo, strabordante di scorciatoie di design estemporanee, workaround ingegnosi quanto oscuri, con supporto circa inesistente, ostile, macchinoso, confuso, pieno di comandi inutili salvo che a nascondere nella massa le poche cose che servono. E, come volevasi dimostrare, il servo della gleba produce cose di valore prossimo a zero.