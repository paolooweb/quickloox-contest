---
title: "Miliardari e piantagrane"
date: 2020-12-19
comments: true
tags: [Apple, M1, Windows, Arm, ProRaw, Halide, Facebook]
---
[…e non hanno alcun rispetto per lo status quo](http://www.thecrazyones.it/spot.html).

Hanno pensato di unire formato Raw e fotografia computazionale. Ne sta venendo fuori qualcosa di speciale, come testimonia l’articolo su *Pixel Envy* [di cui ho appena accennato](https://macintelligence.org/blog/2020/12/16/foto-e-amatori/) ma anche e soprattutto una [lectio magistralis](https://blog.halide.cam/understanding-proraw-4eed556d4c54) da parte di chi produce [Halide](https://halide.cam). La fotografia professionale è destinata a cambiare non poco e tra non molto.

Hanno pensato di farsi i processori in proprio, come non è mai accaduto in cinquant’anni di informatica individuale.

*PcWorld* (va ripetuto: *PcWorld*, vorrei avere un corsivo due volte più corsivo) titola [Che brutto confronto per Windows Arm contro i nuovi Mac M1](https://www.pcworld.com/article/3601196/tested-how-badly-windows-on-arm-compares-to-the-new-mac-m1s.html):

>Come si comporta Windows su Arm rispetto a macOS su Arm? Molto, molto male. […] È talmente indietro sugli M1 che è dura credere a qualsiasi miglioramento ulteriore capace di riavvicinarlo in modo significativo.

Di test è pieno il mondo. Toni come questi però si sono visti molto, molto raramente. Se appena appena il successore di M1 per i Mac più prestanti soddisfa le aspettative, la geografia dell’hardware da scegliere per avere i computer più veloci si sposta brutalmente e drasticamente. Va anche notato che l’annuncio degli M1 è avvenuto a giugno e, dopo sei mesi, non c’è niente di rilevante all’orizzonte né in casa Intel né in casa Amd né in casa Qualcomm, insomma, la rilevanza in ambito PC è attualmente *homeless*.

Hanno deciso di puntare sulla privacy come fattore di distinzione e di fornire i loro computer di un browser che da solo screma i tracciamenti pubblicitari ragionevoli da quelli sgradevoli. Prossimamente il sistema operativo chiederà agli utilizzatori se concedono il permesso di farsi tracciare su siti terzi dalla app X.

(Sembra fantascienza, è permettere all’utilizzatore di controllare la propria esperienza e i propri metadati sul web. Dovrebbe essere il minimo sindacale).

Facebook non l’ha presa benissimo e [ha comprato spazi pubblicitari](https://www.cnbc.com/2020/12/16/facebook-blasts-apple-in-new-ads-over-iphone-privacy-change-.html) (Facebook compra spazi pubblicitari invece di venderne!) per accusare Apple di comportamento anticoncorrenziale e di colpire duramente le piccole e medie imprese, che saranno *devastate* da questo cambiamento.

Piccole o grandi imprese, tracciare il navigatore su siti altri senza un consenso sembra comunque una brutta cosa. Probabile che il panorama della pubblicità su Internet come lo abbiamo conosciuto finora stia per modificarsi profondamente.

Tre casi esemplari di scuotimento dei mercati e delle rendite di posizione, da parte di chi fa miliardi a palate e certo, per come va la partita, avrebbe ogni interesse a non cambiare il campo né le regole.

Continuano a venire in mente le stesse parole:

*…ai folli, agli anticonformisti, ai ribelli, ai piantagrane…*

Quelli abbastanza svalvolati da pensare di cambiare il mondo e accidenti se, da qualche anno, lo stanno facendo.