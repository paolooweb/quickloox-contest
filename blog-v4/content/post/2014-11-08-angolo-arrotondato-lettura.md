---
title: "L’angolo arrotondato della lettura"
date: 2014-11-08
comments: true
tags: [iPad]
---
Da poco papà inizio a pormi domande più pragmatiche su come quando e perché esporre i piccoli alla tecnologia e già mi chiedo se il venerabile iPad di prima generazione che abbiamo in casa durerà abbastanza da diventare il primo strumento elettronico di Lidia.<!--more-->

Intanto leggo di un test che sembra affidabile e mostra come bambini di tre anni, sollecitati mediante un iPad, [mostrino progressi misurabili nelle capacità di lettura](http://www.fastcolabs.com/3037603/yes-ipad-apps-can-help-your-child-learn-to-read) rispetto ai metodi convenzionali.

Ovvio che nessuno stia sostenendo l’obbligatorietà di iPad, il suo uso esclusivo rispetto ad altri strumenti, l’utilizzo per ventiquattro ore al giorno o il beneficio assoluto dell’oggetto a prescindere che venga usato bene o male (sono obiezioni tipiche di questi dibattiti e somigliano a *straw man argument*: metti in bocca all’avversario un’assurdità e contraddici quella, invece di ciò che è stato veramente detto).

Sostengo invece che gli strumenti digitali, per il tempo adeguato e nel modo giusto, sono oggi indispensabili per crescere un bambino al massimo delle sue possibilità, proprio come quarant’anni fa i libri. Sembra ovvio, non lo è: le tavolette dagli angoli arrotondati sono ancora viste in moltissime situazioni come demoniache *a priori*, prima ancora di provare, toccare, sperimentare.
