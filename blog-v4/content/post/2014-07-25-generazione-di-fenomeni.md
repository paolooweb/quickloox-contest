---
title: "Generazione di fenomeni"
date: 2014-07-26
comments: true
tags: [Noster, deNoronha, Apple]
---
A Cnbc [arriva](http://www.cnbc.com/id/101863259#) tale Pedro de Noronha, analista di Noster Capital, e [la butta lì](http://video.cnbc.com/gallery/?video=3000294184):

>Ho bisogno di sapere dove si troverà un’azienda tra cinque o dieci anni. Guardiamo ad Apple, un’azienda che ammiriamo tutti… non so dove saranno da qui a tre anni. […] È un panorama molto competitivo. Potrebbero essere obsoleti in due-tre anni, come abbiamo visto per decine di aziende della tecnologia.<!--more-->

Proviamo a parlare di Apple come di un’azienda che, con il solo denaro che ha in cassa, potrebbe pagare centomila dollari l’anno a centomila dipendenti per sedici anni senza muovere un dito e si capisce l’enormità della stupidaggine. Tutto può accadere, ma un triennio sembra un periodo un po’ troppo esiguo, specie per uno un cerca di informazioni sui prossimi cinque-dieci anni incapace di spiccicare una singola parola di verità su quello che sarà successo il prossimo Natale.

È diventato anche normale che un cretino possa apparire e dire la sua, senza essere valutato preventivamente dall’editore. Ce la teniamo.

Mi preoccupano i fenomeni che adesso andranno in giro a ripeterlo senza un giudizio, senza un dato, senza un’analisi, senza una conoscenza.