---
title: "La prevalenza del Terminale"
date: 2024-02-24T23:15:33+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [Terminale, sips, file, screencapture]
---
Sono riuscito a farmi guastare una piccola parte del pomeriggio a causa di una serie di file grafici di incerta provenienza, ignota attribuzione e caratteristiche fastidiose.

Ma avrebbe potuto andare molto peggio: infatti sono ricorso ai comandi Unix `file`, `sips` e `screencapture` e sono riuscito a semplificare e velocizzare gran parte delle soluzioni alle problematiche.

Ci sono momenti in cui il Finder è una mano santa, ma altri in cui conviene lasciar prevalere il Terminale. Spesso anche i comandi diversi dallo Unix standard e propri di macOS contengono possibilità pazzesche di migliorarsi il lavoro.