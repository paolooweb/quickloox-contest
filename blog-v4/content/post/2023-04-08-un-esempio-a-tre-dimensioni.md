---
title: "Un esempio a tre dimensioni"
date: 2023-04-08T10:53:34+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [Autocad, Blender]
---
A chi ritiene che AutoCad abbia ragioni fondate nell’arrivare con ritardo abbondante al [supporto di Apple Silicon](https://macintelligence.org/posts/2023-03-30-il-gusto-della-vendita/) (per quanto buona notizia, alla fine), proporrei un confronto con [Blender](https://www.blender.org) sulla base di indicatori qualunque come frequenza degli aggiornamenti, ampiezza della comunità di supporto, livello di compatibilità.

Tralasciamo pure la comparazione dei prezzi di vendita, che non avrebbe senso. Anche se, almeno secondo logica, gente pagata per farlo dovrebbe essere più sollecita e precisa di chi lo fa gratuitamente.

Volendo si può aggiungere, per compensare, un confronto sulla complessità dei compiti affrontati dalle rispettive basi di codice.