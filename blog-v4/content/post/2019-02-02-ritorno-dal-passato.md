---
title: "Ritorno dal passato"
date: 2019-02-02
comments: true
tags: [burocrazia, passaporto, patente]
---
Quasi finito il *tour de force* degli ultimi giorni in cui per coincidenza ho rinnovato in contemporanea passaporto e patente.

Sono alle ultime battute e dovrei arrivarci con successo. Le osservazioni fondamentali [sono state già fatte](http://www.macintelligence.org/blog/2019/01/12/stomaci-di-stato/), ma posso aggiungere qualche piccolo dettaglio.

Per il passaporto si digitalizzano le impronte di otto dita su dieci. Mentre poggiavo un dito per volta sul piccolo lettore pensavo *perché non vanno bene le impronte digitali lasciate all’Esercito?*. Chiaro che non tutti hanno svolto il servizio di leva, ma qualcuno sì, qualche milione di persone. Quella banca dati è enorme e le impronte non invecchiano. L’incapacità di parlarsi tra due pezzi di stato genera inefficienze e sovraccarichi di gestione.

Entrando nell’edificio dove avrei svolto la visita medica per la patente, il *touchscreen* da premere per avere il numero di chiamata non funzionava. Ho chiesto all’usciere il quale, con grande tranquillità, mi ha risposto *il suo numero è il trentaquattro*. Li dava a voce.

Durante la visita medica controllano la vista (niente di male, anche se nel mio caso avrei teoricamente un problema cardiaco) e al posto delle vecchie tabelle vengono proiettate sequenze di lettere su uno schermo. Un pezzettino di modernità nel solito ambiente fatiscente, con medici fatiscenti. La documentazione medica diligentemente fotocopiata è stata bellamente ignorata; interessa a nessuno (io doppiamente deluso, orgoglioso dei miei esami perfetti). L’unico scopo dell’operazione è incassare i trentuno euro della visita, i dieci euro e venti di imposta di bollo, l’altro versamento che neanche ricordo. *Da effettuare obbligatoriamente in ufficio postale*, sai mai che a qualcuno venga l’idea folle di risparmiare un’ora e usare il proprio *home banking*.

Mi toccherà una visita in Motorizzazione perché, per motivi che ignoro, da sempre ho avuto un nome solo sulla patente, e tre nomi sugli altri documenti. Io ho sempre fornito le generalità complete. L’impiegata ha deciso che la patente va normalizzata. La spesa di tempo ed energie, naturalmente, ricade su me. Ho registrato un account Spid, l’identificativo statale universale per entrare negli spazi più intimi della mia relazione con lo stato, dall’Agenzia delle Entrate in giù. Ma collegarsi a un sito e sistemare la parte anagrafica della patente sembra ancora futuro fantascientifico.

La Motorizzazione di Milano, più che un salto nel passato, è un abisso. Peggio di loro, neanche la Asl. Avendo una fototessera graffettata sul certificato di idoneità alla guida, dovrò produrre altre fototessera? D’altronde, che sarebbe la vita senza il brivido dell’avventura?