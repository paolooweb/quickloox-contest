---
title: "Dopo Siracusa"
date: 2016-06-23
comments: true
tags: [macOS, Sierra, 10.12, Siracusa]
---
Devo ammettere che non sono ancora pronto e, specialmente per l’alternarsi di approfondimenti e leggerezza cui ero abituato, sento la mancanza di una recensione di macOS [firmata John Siracusa](https://macintelligence.org/posts/2015-04-18-so-long-john/).

Nonostante questo, per completezza e dettaglio posso solo raccomandare la lunga copertura della Developer Preview di Sierra [allestita da Ars Technica](http://arstechnica.com/apple/2016/06/the-macos-sierra-developer-preview-different-name-same-ol-mac/). Ora come ora è la lettura più completa ed equilibrata possibile sul tema. Ottantaquattro paragrafi sono molti e dimostrano come ci sia da dire utilmente su Sierra. Anche se John era un’altra cosa.

*[Tra qualche settimana potrebbe arrivare un [superlibro](https://macintelligence.org/posts/2016-06-15-un-kickstarter-per-cambiare-il-libro/), più che un libro tradizionale, che l’amico Akko sta lavorando per [fare conoscere e finanziare](https://www.kickstarter.com/projects/295108589/cuore-di-mela-i-segreti-di-apple-iphone-e-mac). Assai più completo e approfondito di questa recensione, e aggiornato periodicamente con le ultime novità.]*