---
title: "Sopravvivere ai bug"
date: 2022-03-16T02:25:22+01:00
draft: false
toc: false
comments: true
categories: [Software, Internet]
tags: [Pixelmator, Karkhiv, Ucraina, Russia]
---
Aggiorna il codice di Pixelmator sistemando alcuni bug e poi [corre verso il rifugio antiaereo](https://twitter.com/pixelmator/status/1503766530278010882), visto che bombardano le aree residenziali della città.

Non c’è molto altro da dire.

<blockquote class="twitter-tweet"><p lang="en" dir="ltr">For some users of our apps, it may seem like the war in Ukraine is a world away.<br><br>It isn&#39;t.<br><br>After pushing some bug fixes to our git repo late last night, our colleague in Kharkiv ran to a bomb shelter because the Russian army started shelling residential areas of his city.🧵1/9 <a href="https://t.co/itk142oVqx">pic.twitter.com/itk142oVqx</a></p>&mdash; Pixelmator Team (@pixelmator) <a href="https://twitter.com/pixelmator/status/1503766530278010882?ref_src=twsrc%5Etfw">March 15, 2022</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

P.S.: Pixelmator è lituana.