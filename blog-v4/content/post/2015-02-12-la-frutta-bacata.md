---
title: "La frutta bacata"
date: 2015-02-13
comments: true
tags: [Dell, Apple, Precision, MacBookPro, M3800, Bapco, Xps13, MacBookAir, PcWorld]
---
Dell è ufficialmente alla frutta. Lo prova il fatto che nella [pagina di vendita del portatile Precision M3800](http://www.dell.com/us/business/p/precision-m3800-workstation/pd) devono abbassarsi a scrivere *meglio di un MacBook Pro* e pubblicare una infografica contenente [sette motivi per cui M3800 è superiore a MacBook Pro](http://i.dell.com/sites/imagecontent/business/smb/merchandizing/en/PublishingImages/Infographic.jpg).<!--more-->

Ho guardato i sette motivi e provato a confrontare le schede tecniche dei due modelli di punta. Effettivamente due punti di superiorità di M3800 sono innegabili: schermo (8.294.400 pixel contro 5.184.000) e peso (4,15 libbre contro 4,46, 140 grammi di differenza). Sul resto ci sarebbe qualcosa da dire.

Un terzo argomento è ovviamente il prezzo: è vero che Dell chiede 2.234,30 dollari e Apple ne chiede 2.499. Solo che un quarto argomento è *più di mille configurazioni* e mi sono chiesto *possibile che tutte le mille configurazioni finiscano per avere un prezzo inferiore?* Complicato da stabilire, perché analizzare una per una mille configurazioni è impossibile. Tra l’altro le opzioni di schermo sono una sola, le opzioni di Ram una sola, le opzioni di scheda grafica una sola. Le mille configurazioni sono puro calcolo aritmetico fine a se stesso e francamente, più che un vantaggio, sembrano un focolaio di emicrania.

Interessante il punto dell’autonomia, che sarebbe del 25 percento superiore a quella di MacBook Pro. Frugando nelle scritte in piccolo si scopre che il confronto è tra il dato diramato da Apple e, nel caso di Dell, i test di [Bapco](http://www.bapco.com). I quali esistono solo per Windows e non testano macchine, ma componenti. Quel processore X, quella scheda grafica Y eccetera. Sono dati di laboratorio, non su strada. I test saranno gli stessi usati da Apple? Lecito dubitarne. Confrontando la durata di MacBook Air con quella di Xps 13 di Dell, *PcWorld* scrive [nessun test singolo può confrontare direttamente l’autonomia di un Pc e un MacBook](http://www.pcworld.com/article/2880564/dell-xps-13-vs-macbook-air-a-closer-look-at-battery-life.html). Tombola.

Sempre frugando, si scopre che MacBook Pro 15” Retina è equipaggiato con una batteria da 91 watt. M3800 ne ha una da 61; per avere quella da 91 watt, sono 42 dollari in più. Ed ecco che il divario di prezzo si riduce.

L’infografica spiega che M3800 ha due volte la capienza a disposizione di MacBook Pro. Più in piccolo si legge che effettivamente in fase di configurazione è possibile arrivare a due terabyte di disco su M3800 e a un terabyte su MacBook Pro. Qui le cose si fanno interessanti: di serie, MacBook Pro ha 512 gigabyte, M3800 solo 256 gigabyte. Portare quest’ultimo a 512 gigabyte costa 434 dollari in più. Addio prezzo inferiore! Passare a un terabyte, invece, costa cinquecento dollari extra su Mac e 735 su Dell. Il divario si azzera e l’argomento della convenienza svanisce. Vero che M3800 permette l’installazione di un secondo disco (altri 749 dollari) e MacBook Pro no, ma l’argomento del prezzo è una bugia bella e buona. O come minimo non può essere sostenuto assieme a quello della capienza.

Un ultimo argomento, quello delle certificazioni degli integratori di sistemi, è talmente risibile che non merita neanche considerazione.

Vero che lo schermo 4K è nettamente superiore e vero che il peso è lievemente inferiore. Tutto il resto, anche senza contare i *glissando* (per esempio il processore di serie è 2,5 gigahertz su Mac e 2,3 gigahertz su Dell), sa davvero di bufala scaduta.

Dell è alla frutta e il baco, per una volta, non sta nella mela.
