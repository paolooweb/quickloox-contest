---
title: "All’inizio fa un po’ male"
date: 2016-06-29
comments: true
tags: [Windows]
---
Ritorno sulla faccenda degli [aggiornamenti subdoli e/o coatti a Windows 10](https://macintelligence.org/posts/2016-06-04-volente-o-nolente/) perché [ci sono novità](http://www.zdnet.com/article/microsoft-to-make-saying-no-to-windows-10-update-easier/). Questo l’annuncio ufficiale (tradotto) di Microsoft:<!--more-->

>Abbiamo iniziato il nostro cammino con Windows 10 con l’obiettivo chiaro di portare le persone da avere bisogno di Windows a scegliere Windows ad amare Windows. […] La nuova esperienza [di aggiornamento a Windows 10] ha opzioni più chiare di aggiornare subito, scegliere un momento o declinare l’offerta. Se il pulsante X su fondo rosso viene cliccato, la finestra di dialogo verrà chiusa e ripeteremo la notifica dopo pochi giorni.

Opportuno ricordare che finora, con la chiusura della finestra di dialogo attraverso il ributtante (parlando di usabilità) pulsante X, il software si comportava come se fosse stato fatto clic su *Aggiorna subito*.

L’offerta di aggiornamento gratuito (ti chiediamo i soldi l’anno prossimo, sperando di poterti prendere per il collo più che ora) scade esattamente tra un mese. Traduzione: i fessi, quelli che cliccano senza guardare e senza capire, li abbiamo presi tutti, compresi la tizia che [ci ha fatto causa e ha ottenuto diecimila dollari di risarcimento](http://www.theverge.com/2016/6/27/12046738/microsoft-pays-10k-over-windows-10-auto-update). Adesso andiamo a prendere quelli con un minimo di cervello, che hanno capito l’antifona e sono stati alla larga, mostrando il volto umano e amichevole dopo che il danno lo abbiamo fatto tutto fino in fondo. Ed evitando che l’idea della causa venga a più gente di così.

Amerai Windows. All’inizio, certo, farà un po’ male, ma poi magari ci si abitua.

*[Certamente nel [super-libro che sta lanciando Akko](https://macintelligence.org/posts/2016-06-15-un-kickstarter-per-cambiare-il-libro/) si parlerà di come aggiornare a macOS Sierra. Fortunatamente si potranno raccontare cose più utili e piacevoli, specialmente se [il progetto sarà sufficientemente divulgato e finanziato](https://www.kickstarter.com/projects/295108589/cuore-di-mela-i-segreti-di-apple-iphone-e-mac).]*