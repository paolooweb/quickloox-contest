---
title: "Una falla nella civilizzazione"
date: 2016-11-17
comments: true
tags: [FreeCiv]
---
Mi è sfuggito il ventunesimo compleanno di [FreeCiv](http://www.freeciv.org), un bellissimo gioco libero di strategia a tema storia-del-mondo di cui si è già variamente parlato e purtroppo sono sempre a [dire la stessa cosa](https://macintelligence.org/posts/2015-04-03-la-palestra-dellardimento/): per quanto sia possibile praticarlo in modo efficace nel *browser*, sarebbe indicato avere una versione Mac scaricabile.

L’ultima immagine disco compatibile Mac riguarda invece la versione 2.1.9, mentre la migliore versione stabile è oggi la 2.5.3.

Non credo che si tratti di difficoltà tecniche insormontabili; più di assenza di sviluppatori interessati al compito e coinvolti nel progetto.

Provo ancora una volta a smuovere le acque e chissà che non accada qualcosa.
