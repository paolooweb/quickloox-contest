---
title: "Compiti a caso"
date: 2020-09-04
comments: true
tags: [Edgenuity, AI]
---
I compiti di un ragazzino vengono massacrati dalla piattaforma di apprendimento remoto su cui si esercita.

La mamma vede che il voto, spietato, compare una frazione di secondo dopo la consegna dell’elaborato e capisce: dietro non ci sono insegnanti ma un algoritmo. E invece di dire al figlio di studiare, [studia lei una soluzione](https://twitter.com/DanaJSimmons/status/1300997133311508480).

<blockquote class="twitter-tweet"><p lang="en" dir="ltr">Algorithm update. He cracked it: Two full sentences, followed by a word salad of all possibly applicable keywords. 100% on every assignment. Students on <a href="https://twitter.com/EdgenuityInc?ref_src=twsrc%5Etfw">@EdgenuityInc</a>, there&#39;s your ticket. He went from an F to an A+ without learning a thing.</p>&mdash; Dana Simmons (@DanaJSimmons) <a href="https://twitter.com/DanaJSimmons/status/1300997133311508480?ref_src=twsrc%5Etfw">September 2, 2020</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

>Due frasi compiute, seguite da un’insalata di parole composta da tutte le parole chiave applicabili. Voto pieno in tutte le esercitazioni. Studenti su Edgenuity, ecco il vostro biglietto. [Mio figlio] è passato da F ad A+ senza imparare alcunché.

(F è un’insufficienza gravissima negli Stati Uniti, A+ il massimo).

Proviamo a pensare a una, facciamo finta che lo sia, intelligenza artificiale pensata per sostituire gli studenti. Assurdo, vero? Ecco. Pensiamo a una (sedicente) intelligenza artificiale pensata per sostituire i professori. Assurdo, vero?

L’apprendimento meccanizzato, gli algoritmi, chiamiamola pure intelligenza artificiale se fa stare meglio, devono servire a dare più possibilità ai docenti, non a rimpiazzarli. Devono aiutare gli studenti a imparare meglio e di più, non a trovare scorciatoie (anche se questa è stata una lezione formidabile, ancorché involontaria, di informatica). Non c’è bisogno di passare il test di Turing per software di apprendimento remoto, ma quello del buonsenso.

La didattica digitale serve più che mai. Però quelli che ci lavorano, qualcosa devono averlo studiato pure loro, o non funziona.