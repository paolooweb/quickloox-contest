---
title: "Come lui nessuno"
date: 2024-03-05T00:27:35+01:00
draft: false
toc: false
comments: true
categories: [Software, Hardware, People]
tags: [Federico Viticci, MacStories, Viticci, Ticci, MacPad, Vision Pro, Sidecar, Continuity]
---
Un portatile Apple con lo schermo staccabile. Quando è attaccato, lo si usa come un MacBook Air *oppure* come un iPad connesso a tastiera e trackpad.

Lo schermo staccato è un iPad, utilizzabile come tale.

La tastiera senza schermo è una periferica di input che si può collegare a uno schermo per usare macOS *oppure* a Vision Pro per chi preferisce l’input manuale a quello virtuale.

Apple non vende un ipotetico *MacPad* al momento e difficilmente lo venderà. Così Federico Viticci di MacStories [se ne è fatto uno](https://www.macstories.net/stories/macpad-how-i-created-the-hybrid-mac-ipad-laptop-and-tablet-that-apple-wont-make/).

In super sintesi, Viticci ha preso un MacBook Air e gli ha levato lo schermo in via definitiva; poi ha predisposto magneti per mettere un iPad nella posizione precedentemente occupata dallo schermo. iPad è collegato magneticamente, quindi si attacca e stacca senza problemi.

Con un uso ingegnoso di Sidecar e Continuity, il *device* collegato alla tastiera (un iPad) può funzionare come sé stesso, o altrimenti diventare lo schermo di un Mac.

Il Mac *headless* può fare quello che avrebbe fatto anche con lo schermo e fungere da schermo virtuale per Vision Pro.

Non è banale e Viticci ha dovuto risolvere diversi problemi pratici, per esempio ovviare a una eventuale mancanza di Wi-Fi in ambienti come l’auto (senza Wi-Fi non c’è Sidecar).

Al tempo stesso non è per niente impossibile. Levare lo schermo a MacBook Air è l’operazione più intensiva di tutta la procedura, praticabile da chiunque in possesso della giusta attrezzatura. Poi si tratta di configurare in modo mirabile, ma normale una volta dopo averlo letto. tutti i pezzi hardware e software della soluzione. Questo uno dei commenti di Ticci:

>Scrivo questo come persona che l’anno scorso ha lavorato con un Surface Pro 0 per sei mesi e ha trascorso diverso tempo recente a esplorare i pieghevoli Android: nessun’altra società sulla Terra ha una forza di ecosistema, strumenti di sviluppo, basi di progettazione e approccio amichevole paragonabile ad Apple. La combinazione di iPadOS e macOS, al momento, gioca diversi campionati sopra a quello che Microsoft sta facendo con la linea Surface; il solo problema è che per dimostrarlo ho dovuto costruire questo apparecchio ibrido.

Se MacPad venisse costruito in serie, non ci sarebbe un apparecchio equivalente al mondo; al tempo stesso, solo un appassionato come Viticci, situato in uno *sweet spot* tra la posizione di sviluppatore e quella di utente finale, poteva escogitare una soluzione del genere e renderla effettivamente funzionale (alla fine tra software e hardware è presente una dozzina di componenti scelti apposta per consentire il funzionamento di tutto).

Il bello di seguire il mondo Apple è che, certo, a volte si fa quello che fanno tutti; a volte però capita di fare cose che nessun altro fa. Di là questa occorrenza è molto più rara e meno eclatante.