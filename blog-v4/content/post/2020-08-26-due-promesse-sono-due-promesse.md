---
title: "Due promesse sono due promesse"
date: 2020-08-26
comments: true
tags: [Cook, Associated, Press, Jobs, Apple]
---
L’altro ieri Tim Cook ha compiuto i nove anni di amministratore delegato di Apple, da quando è succeduto in via definitiva a Steve Jobs.

*Open Source Intelligence* riporta [una risposta fornita a quell’epoca da Cook](https://osi.news/2020/08/24/apple-ceo-tim-cook-is-fulfilling-another-steve-jobs-vision/) a chi chiedeva che direzione avrebbe preso l’azienda sotto la sua guida:

>[Apple deve] possedere e controllare le tecnologie primarie che stanno dietro ai suoi prodotti.

In autunno vedremo i Mac con processore progettato da Apple; una evenienza che chiunque, anni fa, avrebbe neanche minimamente considerato. Ancora Cook:

>[Apple rinuncerebbe a molti progetti] in modo da potersi realmente concentrare sui pochi per noi veramente importanti e significativi.

Per qualcuno Apple ha perso la strada dell’innovazione perché non fa uscire un *game changer* in qualsiasi settore ogni anno. Colpa della mancanza di Jobs, insostituibile.

Jobs non è mai stato sostituibile da alcuno. Cook, invece che copiare una persona così diversa da lui oltre che incopiabile, è andato per la sua strada. Ha avuto alti e bassi, ma ha mantenuto due promesse di portata formidabile. Grazie alle quali Apple è cambiata moltissimo a pensarci, eppure rimane quella di sempre nelle fondamenta di pensiero.