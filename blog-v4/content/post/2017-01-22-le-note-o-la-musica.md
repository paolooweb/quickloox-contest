---
title: "Le note o la musica"
date: 2017-01-22
comments: true
tags: [Note, Standard]
---
Dall’avvento di El Capitan, con le nuove Note, le uso molto ma molto più di prima. La sincronizzazione è ottima, la versatilità pure; non le userei mai per scrivere un articolo ma per conservare appunti e rapide annotazioni le trovo ideali.

Non mi sorprende allora vedere arrivare programmi come [Standard Notes](https://standardnotes.org), che si propongono essenzialmente di lavorare con le note meglio di Note (nel caso specifico, su Mac e anche su qualunque altra piattaforma), o anche [Bear](https://www.macstories.net/stories/why-im-considering-bear-as-a-notes-app-replacement/), per dire.

Mi chiedo se siano state le Note di Apple a far rinascere l’interesse per questo genere di programmi o se invece la tendenza sia tornata spontaneamente, e anche Note ne sia parte.

In ogni caso, chiaro, è tutta soddisfazione constatare che il parco programmi dedicato alla scrittura e dintorni migliora.