---
title: "Gli asparagi e l’immortalità dei formati"
date: 2017-02-18
comments: true
tags: [EZDraw, Quill, Psion, AppleWriter, IIe, MacDraw, MacWrite, FreeHand, Pages, Numbers, Keynote, formati, Html, Xml, Markdown]
---		
Mio papà aveva una discreta collezione di disegni vettoriali realizzata con AppleWorks, programma che dopo varie peripezie dentro e fuori di Apple non esiste più da anni.

Alcune forme di documenti AppleWorks [si recuperano agevolmente](http://www.macworld.com/article/1160843/business-software/last-appleworks.html) con una edizione ’09 di iWork (Pages, Numbers, Keynote), ma non i disegni vettoriali.

Fortunatamente esiste [EazyDraw](http://www.eazydraw.com/appleWorks.htm). Bisogna acquistare una versione precisa del software, che è ancora in grado di leggere quei documenti. Successivamente, volendo, si può aggiornare alla versione più moderna di EazyDraw, che offre tante cose in più ma perde quella compatibilità, esattamente come l’hanno persa i nuovi Pages, Numbers e Keynote.

E [MacDraw](https://www.eazydraw.com/MacDraw.htm)? [FreeHand](https://www.adobe.com/mena_en/products/freehand/)? [MacWrite](http://lowendmac.com/2014/macwrite-faq/)? [AppleWriter IIe](http://www.wikiwand.com/en/Apple_Writer)? [Quill di Psion](http://www.rwapadventures.com/ql_wiki/index.php?title=Quill)?

Rimango sempre sconcertato da come persone perfettamente responsabili, con intelligenze superiori alla media, titoli di studio qualificati, famiglie funzionali, menti elevate e socialità evoluta si mettano davanti al computer per trasformarsi in sacchi di patate a responsabilità zero.

La responsabilità della lettura nel tempo dei formati che produci è *tua*. Non è dello sviluppatore, non è della software house, non è di Apple ma neanche di Samsung, non è dello Stato e non è del coniuge. È tua.

C'è una ragione per questo. Se per caso hai un figlio, ne sei responsabile per diciotto anni. Un domani potresti cambiare indirizzo o anche Paese, parlare un’altra lingua, iniziare a viaggiare o smettere, iscriverti a Airbnb e avere un nuovo sconosciuto tutte le sere a russare sul divano. Quali che siano le condizioni a contorno, rimani responsabile di tuo figlio. Lo stesso vale per i tuoi documenti e per il formato in cui si trovano.

Se l’applicazione che lavora con quel formato si aggiorna, è tuo dovere verificare che i documenti restino leggibili. Nel caso, devi provvedere ad aggiornare i documenti secondo un cammino di compatibilità che, se agisci tempestivamente, sarà semplice ed economico. Se lasci passare vent’anni, sarà complicato e costoso.

*Ma in che formato devo salvare per essere sicuro che i documenti resteranno leggibili per sempre?*

Spiacente, questa sicurezza non c’è. Un buon approccio è salvare nel formato più semplice che hai a disposizione. Un file in formato testo (quindi anche Html, Xml, Markdown eccetera) sarà sempre leggibile nel futuro prevedibile. Per quanto riguarda le immagini, se salvi in un formato molto comune e usato sul web, tipo Png, Gif, Jpeg, Tiff, è pressoché certo che nel futuro prevedibile ci sarà sempre qualche programma che riesce a leggerla. Se salvi in un formato *aperto*, non proprietà di una azienda, le probabilità a favore aumentano. Tra venticinque anni forse nessuna delle multinazionali che ci forniscono hardware e software esisterà nella forma attuale, ma Html resterà attivo e vitale. Eccetera; è compito di ognuno trovare i formati aperti e supportati che promettono una leggibilità futura.

Attenzione ai formati *lossy*, che permettono la fruibilità ma distruggono l’informazione. Una immagine Jpeg ha perso una quantità enorme di informazione rispetto all’originale e così un file Mp3.

Attenzione perché alcuni formati aziendali in realtà sono aperti e altri formati complessi non sono che contenitori. Pdf di Adobe non è aperto ma è documentato e consultabile, i file di Pages sembrano ermetici ma in realtà sono Zip con dentro Xml e un’anteprima Pdf, se prendi il testo di un documento LibreOffice e lo incolli in Wordpress appare una struttura Html con dentro testo normale. Un eBook ePub ha una estensione che, cambiata in .zip, permette di aprire una cartella contenente essenzialmente Html, ovvero testo.

Futuro *prevedibile* perché *per sempre* non esiste, è una costruzione mentale. Tuo figlio potrebbe andare in gita con la scuola e tornare con un bernoccolo. È improbabile che succeda, ma può succedere e niente può eliminare la possibilità (se lo chiudi in casa, si farà un bernoccolo in qualche altro modo).

Quando la frittata è fatta e ti ritrovi con formati del secolo scorso, ci sono varie linee di azione. Ci sono emulatori, ci sono macchine virtuali, ci sono posti che tengono in funzione macchine del passato con programmi del passato, ci sono programmi misconosciuti che leggono formati misconosciuti. GraphicConverter ha perso un po’ dello smalto che aveva come programma di elaborazione della grafica, ma come coltellino svizzero per aprire i formati grafici più assurdi e incredibili, non lo batte nessuno.

Rimane una legge ahimé ineludibile, composta da due articoli.

* La responsabilità è tua.
* Più a lungo ignori il problema, più la soluzione sarà complessa e costosa.

L’autore del programma cui ti sei affidato ha tanta relazione con i documenti che produci quanto gli asparagi e l’immortalità dell’anima: nessuna.
