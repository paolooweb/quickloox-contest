---
title: "Tre messaggi"
date: 2014-07-16
comments: true
tags: [DwarfFortress, Xquartz, Venerandi, MacNewbie, iTerm2, dunnet]
---
Il primo messaggio è l’uscita di una versione notevolmente aggiornata di [Dwarf Fortress](http://www.bay12games.com/dwarves/), il gioco di ruolo che sembra una cosetta a interfaccia testuale e invece racchiude un mondo capace di lasciare senza fiato.<!--more-->

Non ho avuto successo nel cercare di fare funzionare il pacchetto scaricato dal sito: il programma non capisce che uso [Xquartz](http://xquartz.macosforge.org/landing/) al posto di X11 e invece dovrebbe essere una cosa automatica e banale. Boh.

Non ho avuto tempo di occuparmene finora. Tuttavia segnalo che [MacNewbie](http://dffd.wimbli.com/file.php?id=7922) funziona perfettamente come alternativa.

Il secondo messaggio è l’uscita di una versione anche qui notevolmente aggiornata di [iTerm2](http://www.iterm2.com), sostituto del Terminale di OS X. L’[elenco delle novità](http://www.iterm2.com/features.html) impressiona.

Nelle stesse ore il terzo messaggio, che è un [*tweet*](https://twitter.com/fbrzvnrnd/status/488626342783877120) dell’immenso Fabrizio Venerandi:

<blockquote class="twitter-tweet" lang="en"><p>evergreen: su mac o linux aprite il terminale e scrivete: &quot;emacs -batch -l dunnet&quot;. forse il gioco installato su più computer nel mondo</p>&mdash; fabrizio venerandi (@fbrzvnrnd) <a href="https://twitter.com/fbrzvnrnd/statuses/488626342783877120">July 14, 2014</a></blockquote>
<script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>

Nessun dubbio. Il Terminale sta cercando di dirmi qualcosa.