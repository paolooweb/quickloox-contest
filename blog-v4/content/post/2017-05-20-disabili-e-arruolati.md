---
title: "Disabili e arruolati"
date: 2017-05-20
comments: true
tags: [iPhone, accessibilità]
---
Le storie delle persone protagoniste della recente raffica di [video pubblicitari](http://www.loopinsight.com/2017/05/16/apple-videos-highlight-accessibility-achievements/) di Apple sono storie speciali, che riguardano un numero di utilizzatori piccolo anche se sempre troppo grande.

La stragrande maggioranza delle persone acquista un iPhone per ragioni diverse da quelle mostrate nei video. Eppure sui video stessi vengono investiti soldi e competenze evidentemente di massimo livello.

È una cosa alla portata – culturale – di poche aziende. Dove certamente si guarda alle vendite, ma come conseguenza dell’aver fatto la cosa giusta. Vendite come effetto prima che come obiettivo.
