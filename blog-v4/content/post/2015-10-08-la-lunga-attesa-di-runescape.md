---
title: "La lunga attesa di Runescape"
date: 2015-10-08
comments: true
tags: [Runescape, Html5, Mac, iPad, Chrome, browser]
---
Non che avrei tempo per giocarlo, eh. Tuttavia sono passati due anni da quando [notavo](https://macintelligence.org/posts/2013-07-23-i-tempi-del-software/) l’uscita di [Runescape beta in versione Html 5](http://www.runescape.com/beta/html5). Indipendente da qualunque *plugin*, giocabile in linea di principio anche su un apparecchio iOS, sempre in teoria persino sulla nuova [Apple TV](http://www.apple.com/it/tv/).<!--more-->

Ancora, niente è successo. La beta è rimasta tale, i *browser* sono rimasti tuttora quelli che si vogliono purché siano [Chrome](https://www.google.com/chrome/browser/desktop/index.html), i requisiti di sistema – invece che allentarsi per il progredire dello hardware – impongono computer nuovi o comunque recenti. La pagina ufficiale pone addirittura un requisito di sistema operativo, che dovrebbe essere inesistente se l’idea è giocare dentro il *browser*.

Il mio MacBook Pro 17” (Inizio 2009) è fuori dalla gamma delle possibilità. Il pulsante *Gioca* non risponde nemmeno. Su un iPad di terza generazione ci si scontra con la mancanza dei requisiti hardware e il fatto che il gioco, sia pure in versione Html 5, chiede lo scaricamento preventivo di una parte di risorse.

Niente è migliorato dal 2013. Per me è una consistente delusione e non ci sono abituato: normalmente sono abbastanza preciso nello stimare la disponibilità effettiva di una tecnologia.

A meno che non sia rimasto proprio indietro con lo hardware e Runescape beta, sul Mac giusto, brilli. O magari su un fiammante iPad Air 2. Qualche anima pia in possesso di macchine all’avanguardia riesce a farmi un resoconto della situazione? Grazie anticipate.