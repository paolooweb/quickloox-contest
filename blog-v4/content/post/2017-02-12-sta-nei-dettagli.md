---
title: "Sta nei dettagli"
date: 2017-02-12
comments: true
tags: [Reuters, campus]
---
Secondo questo [articolo di Reuters](http://www.reuters.com/article/us-apple-campus-idUSKBN15M0CM), Apple è più tignosa e pignola che mai nel pretendere specifiche e materiali di eccellenza per il suo nuovo campus a forma di corona circolare, quattordicimiladuecento posti nella più grande struttura al mondo chiusa da vetrate curvilinee.

C’è qualche ritardo e qualche spesa in più del previsto, anche perché varie aziende di costruzione si sono ritirate o sono state sostituite, nell’impossibilità di soddisfare i requisiti posti da Apple.

Guide da trenta pagine per l’utilizzo del legno speciale usato nell’edificio, tolleranze assai più ristrette dello standard persino sulle superfici invisibili, riunioni su riunioni con le autorità per concordare sulla segnaletica interna, un anno e mezzo per definire con precisione la maniglia per gli uffici e le sale riunioni.

Sembra assurdo? Certo, se si dimentica che la stessa azienda ha progettato e brevettato le scale interamente di vetro impiegate nei suoi negozi monomarca.

Non sembra gente che si accontenta, per la propria nuova sede. Può darsi che sui dettagli dei prodotti vi sia la stessa attenzione. Varrà lo stesso anche per la concorrenza?