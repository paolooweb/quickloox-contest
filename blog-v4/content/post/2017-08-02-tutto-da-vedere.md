---
title: "Tutto da vedere"
date: 2017-08-02
comments: true
tags: [Apple, Six, Colors]
---
Bel servizio quello di *Six Colors*, che a contorno dell’annuncio dei risultati finanziari di Apple (con notiziole come il totale di iPhone venduti nella storia salito a 1,2 miliardi) pubblica una bella [pagina di grafici e diagrammi](https://sixcolors.com/post/2017/08/apples-q3-fy17-financial-results/), precisa, completa, sintetica, senza chiacchiere e libera da distrazioni. Solo i dati, su serie storiche lunghe che rivelano bene le macrotendenze delle varie linee di prodotto, per esempio l’uscita di iPad da una lunghissima parentesi di dati di vendita negativi.

Non è ancora la console JavaScript che mi immagino ogni tanto, dove con due tocchi qui e uno lì visualizzi qualsiasi dato e qualunque tendenza nel giro di quattro secondi, ma qualcuno prima o poi la realizzerà. Nel frattempo, basta un minuto su questa pagina per comprendere il *business* di Apple oltre i semplici dati del momento.