---
title: "Furto a orologeria"
date: 2017-09-08
comments: true
tags: [watch, Yankees, Red, Sox, Boston]
---
I Red Sox di Boston sono accusati di [avere usato watch per carpire i segnali segreti](https://www.nytimes.com/2017/09/05/sports/baseball/boston-red-sox-stealing-signs-yankees.html) degli acerrimi rivali New York Yankees.<!--more-->

Se ne occupa il *New York Times* perché negli Stati Uniti il baseball è un affare serissimo e le regole in fatto di tecnologia sono stringenti e sempre controverse.

C’è di mezzo lo sport più popolare degli Usa e si fa esplicitamente il nome di un prodotto tecnologico preciso. Vuol dire che il lettore medio del *Times* sa esattamente di che cosa si tratta.

E pensare che c’era persino chi chiedeva a che cosa servisse watch. Ora lo indossano allenatori e tecnici della Major League Baseball: come e più della nostra Serie A. Lascio trarre le conclusioni.