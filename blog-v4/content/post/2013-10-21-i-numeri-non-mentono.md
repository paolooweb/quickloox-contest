---
title: "I numeri non mentono"
date: 2013-10-21
comments: true
tags: [iPhone]
---
I siti-spazzatura sono attentissimi a qualsiasi indizio che possa rivelare se iPhone 5C stia vendendo bene, male, troppo, troppo poco, in aumento, in diminuzione, la qualunque. Come se fosse in gioco chissà quale posta.<!--more-->

Soprattutto, come se il dato o la voce incontrollata proveniente, forse, da questo o quel fornitore, significasse qualcosa. Tim Cook [ne aveva parlato](http://www.macrumors.com/2013/01/23/tim-cook-warns-against-trying-to-interpret-supply-chain-order-rumors/) qualche tempo fa:

>Perfino se un dato specifico avesse valenza fattuale, sarebbe impossibile interpretarlo per il suo significato rispetto alla nostra attività. La catena di fornitura è molto complessa e i nostri componenti arrivano da più parti. Gli scarti possono variare, la qualità delle forniture pure. C’è un elenco disordinatamente lungo di motivi che possono rendere un singolo dato non molto significativo per dire che cosa stia succedendo.

L’amministratore delegato di Apple potrebbe voler confondere le acque. Sentiamo allora Benedict Evans di Enders Analysis, un ufficio di ricerche londinese, citato da John Gruber [su Daring Fireball](http://daringfireball.net/linked/2013/10/16/iphone-supply-chain):

>Lo abbiamo già constatato più volte. Nella catena delle forniture ci sono troppe parti in movimento perché si possa trarre qualsiasi conclusione.

L’errore è banale: credere che, siccome iPhone è stato un fenomeno, iPhone 5C debba per forza essere un doppio fenomeno. Invece è una mossa che fa muovere tatticamente Apple sul mercato e la prepara anche internamente a occuparsi di più modelli contemporaneamente, cosa prima mai accaduta.

La verità è che se iPhone 5S vendesse molto più di iPhone 5C, quest’ultimo avrebbe dovuto comunque essere prodotto. Ben Thompson lo mostra [numeri alla mano](http://stratechery.com/2013/where-is-the-bad-news/). Numeri – ogni volta che accade mi meraviglio come un bambino, anche se lo vedo tutti i giorni – con i quali ognuno può [giocare a piacimento](https://docs.google.com/spreadsheet/ccc?key=0Al0AqiWUKZK4dGRpUXJOd29EYVZDcEI2UnFacFhRSkE&usp=sharing#gid=0).

Difficile che un imbroglione ti lasci giocare con i suoi numeri.