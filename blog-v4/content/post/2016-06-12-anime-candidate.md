---
title: "Anime candidate"
date: 2016-06-12
comments: true
tags: [Spiderweb, Avadon, Warborn, beta]
---
Sì, sono fan di [Spiderweb Software](http://www.spiderwebsoftware.com), che produce giochi di ruolo vecchio stile, di grafica semplice, con una quantità di contenuti immensa e giocabilità altissima. [Li ho raccomandati in passato](https://macintelligence.org/posts/2015-01-15-anime-di-cristallo/) e continuerò a farlo.<!--more-->

Chi amasse il genere e il produttore può decidere di candidarsi al ruolo di [beta tester di Avadon 3: The Warborn](http://www.spiderwebsoftware.com/testing/beta_test.html), al momento solo su OS X.

Fare i beta tester *bene* è una faticaccia, ma anche farlo male, per chi non abbia mai provato, è un’esperienza formativa. Non è detto che Spiderweb accetti chiunque, ma vale la pena di provare. L’importante è avere un po’ di tempo perché, se ti candidi e poi non giochi, non fa bene all’anima.