---
title: "Digital Chaucer"
date: 2023-10-26T16:58:23+01:00
draft: false
toc: false
comments: true
categories: [Internet]
tags: [Geoffrey Chaucer, British Library, Canterbury Tales]
---
Sono cose che non vanno più di moda nella confusione dei *like* e dei *reel*, dei leoni di tastiera, delle vaccate sul *popolo di Internet* e però succedono: [la British Library ha digitalizzato l’*opera omnia* di Geoffrey Chaucer](https://www.theguardian.com/books/2023/oct/25/chaucer-digital-works-available-online-british-library-canterbury-tales-manuscripts), producendo nel processo oltre venticinquemila immagini ad altissima risoluzione di tutti i suoi lavori.

È una delle cose per cui è nata Internet: ospitare, condividere e diffondere la conoscenza.

Chaucer è noto soprattutto per i *racconti di Canterbury* mentre, un po’ come Dante in Italia per capirci, ha scritto molto altro ed è stato un pilastro della letteratura inglese del suo tempo, un po’ come Dante in Italia.

Sicuro, la [British Library](https://www.bl.uk) esce mentre scrivo da un ciberattacco e il sito è inaccessibile, o lentissimo. Ma tornerà e le parole di Chaucer si riverbereranno su tante scrivanie di tutto il mondo.

La difesa dai crimini informatici sta ai musei elettronici come quella dai crimini comuni sta ai musei analogici. Il digitale presente rischi ma offre opportunità e, messo tutto sulla bilancia, voto per il digitale.