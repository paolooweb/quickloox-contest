---
title: "Centro di levità permanente"
date: 2023-05-28T02:28:32+01:00
draft: false
toc: false
comments: true
categories: [Software, Hardware]
tags: [Keynote, iPad, MCDonald’s, Wix]
---
A volte i sistemi hanno una tendenza sgradevole ad autodegradarsi e, nel farlo, a infastidire. In questi giorni mi misuro con un provider che ha estratto un problema burocratico incomprensibile da un cappello vuoto, un notaio primadonna, una commercialista che chiede le fatture dell’anno scorso pur potendo guardarsele da sé in quanto provvista di delega di accesso, una casa editrice che mi invia un documento senza bisogno di seguito e – avendo io commesso l’errore di ringraziare – replica che nella mia risposta *manca l’allegato*, una mensa scolastica che vuole il saldo finale dei pasti concomitante con il termine delle lezioni se non fosse che la contabilità aggiorna i conteggi con un mese di ritardo e probabilmente ancora qualcos’altro. Ah sì, esiste una certificato di tipo anagrafico a proposito del quale un Comune può rispondere che non sa quanto impiegherà a produrlo né quanto costerà.

Volendo, la app di McDonald’s si offre di resettare la password di accesso e poi non invia l’email con il codice di verifica. La app di Wix, intanto, non permette di editsre un sito creato su Wix; app che servirebbe a rimediare all’impossibilità di editare il sito stesso con Safari su iPad.

Forse è solo conseguenza della deriva entropica dell’universo, oppure la gggente si agita sentendo arrivare l’estate e in qualche modo accadono cose strane che rompono i meccanismi. Boh.

Nel frattempo, nel giro di tre giorni ho aggiornato iPad, iPhone e watch alle ultimissime versioni , allertato dal sistema senza dover fare niente, tutto nuovamente funzionante nel giro di pochi minuti.

Venerdì mi sono trovato nella situazione di dover realizzare molto velocemente non una ma due minipresentazioni e anche stavolta confesso di essere ricorso senza rimorsi a Keynote su iPad, che a questo punto decreto ufficialmente *killer app*. Nel senso che, non avessi avuto Keynote su iPad in quel momento, sarei morto o circa.

Bella l’entropia, ma in attesa della morte termica è ancora possibile lavorare bene e offrire servizi e strumenti che *just work*. Si possono creare procedure che levano i pensieri anziché generarli. Dall’esperienza confusa e soverchiante di questi giorni capisco dove sta uno dei grandi intangibili di Apple: ti vendono un oggetto hardware, ma il valore sta altrove: nella *leggerezza* che hardware e software della mela possono portare nella vita delle persone.