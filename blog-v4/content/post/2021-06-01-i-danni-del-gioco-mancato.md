---
title: "I danni del gioco (mancato)"
date: 2021-06-01T01:26:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [LambdaMOO, 50 Years of Text Games, Paolo, Neil Young] 
---
Per fortuna ho dato retta a **Paolo** e considerato [50 Years of Text Games](https://if50.substack.com) qualcosa di speciale oltre che un racconto carino di retrocomputing.

La [trattazione di LambdaMOO](https://if50.substack.com/p/1990-lambdamoo), 1990, impressiona.

Tempi in cui Internet era in gestazione e però le persone cominciavano a incontrarsi e a scoprire mondi online.

Nei [Mud](https://medium.com/@williamson.f93/multi-user-dungeons-muds-what-are-they-and-how-to-play-af3ec0f29f4a) canonici occorrevano molte ore di gioco, padronanza dei meccanismi dello stesso e un buon grado di carisma e capacità di rapportarsi con gli altri, per diventare un *wizard*, un mago.

Il *wizard* disponeva di immenso potere dentro il gioco e *sul* gioco. Per esempio poteva espellere altri giocatori.

LambdaMOO rovesciò l’approccio mettendo a disposizione di tutti, subito, ogni potere possibile. Compreso quello di programmare nuove locazioni, nuovi oggetti, nuove avventure, purché coerenti con un impianto di base.

Un vero esperimento sociale, con la crescita turbinosa, le esperienze molteplici, la creatività sorprendente, la dilatazione vertiginosa dei confini.

E poi l’inevitabile scandalo con sfaccettature di sesso e violenza ancorché virtuali, la difficoltà nel conciliare punti di vista troppo differenti, il problema di governare una comunità di persone abilitate ad agire senza limitazioni, alcuni casi di dipendenza. Certi tratti della natura umana sono immodificabili e finiscono per manifestarsi ovunque si raggiunga una massa critica di persone e interazioni tra le persone.

Trent’anni dopo, LambdaMOO non è un esperimento sociale, ovviamente. Altrettanto ovviamente viene ancora praticato (i Mud non muoiono mai, come [il rock’n’roll per Neil Young](https://www.youtube.com/watch?v=cawk2cMTnGo)). Ovviamente per la terza volta, l’animazione al suo interno non è più quella degli anni d’oro.

>Quando un giocatore rimane troppo tempo senza collegarsi, il suo avatar appare dormiente: vagabondare oggi dentro la mappa dà l’idea di esplorare un regno incantato di belle addormentate, alcune da decenni.

Tuttavia, quando un mondo è programmabile, resta vivo e vitale anche quando le forze che sostengono si affievoliscono:

>Eppure la Lambda House continua a intrigare. L’esplorazione resta magica in perpetuo: a differenza di un gioco scritto da un singolo autore, qui è impossibile trovare i limiti del modello del modello o il bordo della mappa. Al prossimo oggetto domani qualcuno potrebbe aggiungere un altro verbo per interagirci e dietro ogni angolo occhieggia un nuovo dominio che attende esploratori freschi. Ascoltare una conchiglia dietro un gazebo trasporta in un pigro paradiso tropicale; azionare una scatola musicale in una radura nascosta, evoca figure spettrali che interpretano un quadro teatrale di Keats. Stanze con descrizioni dinamiche che rispondono alle stagioni e alla ciclicità del giorno macinano un’ora dopo un’altra, mentre sopra lune virtuali attraversano le loro fasi. Anche se la maggior parte delle persone se ne è andata, il codice che hanno lasciato dietro di sé mantiene Lambda House viva.

Trent’anni fa non c’era Internet, ma in LambdaMOO c’era già tutto, comunità, social, algoritmi, dinamiche di gruppo, problematiche di *governance*, gestione di risorse di rete, *wisdom of the crowd*, anche purtroppo il bullismo o la mancanza di rispetto per gli altri o lo spregio verso il bene comune. Appunto, tutto vuol dire tutto.

Se più gente avesse partecipato a LambdaMOO, oggi Internet sarebbe un posto migliore.

<iframe width="560" height="315" src="https://www.youtube.com/embed/cawk2cMTnGo" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

*Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile.*               