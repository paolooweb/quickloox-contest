---
title: "Realtà e fiction"
date: 2020-02-05
comments: true
tags: [Apple, Post, Wsj, NY, Gruber, Cue, Servant, Morning, Show]
---
Dopo tanti anni e dopo tante sciocchezze lette rispetto ai prodotti di Apple, ora tocca leggere sciocchezze riguardo alle serie TV di Apple. Segno dei tempi e immagino quanti si lamentavano per come i Mac venivano trascurati, perché Apple *pensa solo a iPhone*…

Non è sfuggita a John Gruber l’accoppiata *New York Post* più *Wall Street Journal*.

Secondo il primo, [Tim Cook scriveva annotazioni sui copioni](https://daringfireball.net/linked/2020/02/04/cue-ny-post), dirette presumibilmente a registi e produttori. La più comune sarebbe stata *non essere così crudo* (*Don’t be so mean*).

Eddy Cue, responsabile Apple per il settore multimedia, ha smentito *al cento percento* che Cook o chi per esso abbia mai espresso commenti, di qualunque tipo, su un copione: *Lasciamo lavorare i ragazzi, che sanno ciò che stanno facendo*.

Chiosa Gruber:

>Si dica quello che si vuole sui contenuti originali di Apple fino a qui prodotti, ma la crudezza non manca.

E nota come il *New York Post* non abbia mai fatto marcia indietro sulle sue affermazioni.

Il Wall Street Journal invece scriveva [quasi un anno e mezzo fa](https://daringfireball.net/linked/2020/02/04/no-sex-please-claim-chowder):

>Apple ha detto chiaramente, dichiarano produttori e agenti, di volere spettacoli di alta qualità per un pubblico ampio, ma non sesso, turpiloquio o violenza gratuita.

Commenta oggi Gruber:

>Suppongo si possa discutere sulla parola *gratuita*, ma le cose che ho visto […] non sembrano trattenersi sul sesso o sul linguaggio forte. *The Morning Show* e *Servant*, in particolare, sono chiaramente per adulti. […] Per quanto ne so, il Wall Street Journal non ha mai ammesso di avere sbagliato.

La sparate sui prodotti le ho attribuite spesso a una incompetenza di fondo. Per decidere se una serie TV contiene scene spinte o linguaggio forte non bisogna essere competenti; semplicemente, basta guardarle.

Forse anche i prodotti, basterebbe usarli prima di scriverne.