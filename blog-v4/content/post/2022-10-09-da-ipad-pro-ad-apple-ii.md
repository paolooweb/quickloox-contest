---
title: "Da iPad Pro ad Apple II"
date: 2022-10-09T01:37:52+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [iOS, iPadOS, BasiliskII]
---
Quando apro Xcode succede sempre qualcosa di simpatico, in senso sarcastico, e prima di arrivare a qualche risultato passo attraverso ondate di disperazione alternate a furia frankensteiniana.

Però adesso aprirò Xcode e, dovesse arrivare l’alba, sputerò l’anima pur di riuscire a [fare girare BasiliskII su iPad Pro](https://blog.gingerbeardman.com/2021/04/21/building-basiliskii-for-ios/).

Che significa, in pratica, fare funzionare un Macintosh Classic sulla macchina più moderna che ho in casa.

Quelli che una volta si poteva smanettare e invece adesso. Si prendano una pausa che ne riparliamo domani.