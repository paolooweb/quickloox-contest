---
title: "Tutti a scuola"
date: 2019-08-30
comments: true
tags: [Glasgow, iPad, BBC]
---
Da qui al 2021 47.100 studenti e 4.900 insegnanti di Glasgow [riceveranno gratis un iPad](https://www.bbc.com/news/uk-scotland-glasgow-west-49475108). <em>Tutti</em>.

Ciascuno sarà responsabile della propria macchina, che li aiuterà lungo il loro corso di studi.

>Sappiamo che il 90 percento dei posti di lavoro in Scozia implica attività digitali e i nostri studenti saranno bene equipaggiati al momento di entrare nel mondo professionale.

Cosa interessante è che a passare gli iPad alla città di Glasgow non è Apple, ma un gruppo informatico canadese. Il costo dell’iniziativa è sui trecento milioni di sterline.

Glasgow non è la California. È una bella città per chi c’è stato, europea, scozzese. C’è il benessere europeo e le condizioni di vita non sono dissimili dalle città italiane medio-grandi virtuose.

Lì ci sono soldi per pensare al presente e al futuro dei ragazzi a scuola e non mi risulta che si paghino più tasse che qui.

Così, sono riflessioni. Forse i soldi immessi nella scuola italiana dovrebbero entrare, appunto, nelle scuole. Forse invece vanno in altre direzioni. Mah.