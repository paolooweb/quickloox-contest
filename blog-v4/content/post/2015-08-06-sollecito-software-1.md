---
title: "Sollecito software / 1"
date: 2015-08-06
comments: true
tags: [LibreOffice, LibreItalia, Office, Microsoft]
---
Mi permetto di spezzare oggi e domani un paio di lance a favore del software libero, anche se ieri ho esortato all’[acquisto di fumetti](https://macintelligence.org/posts/2015-08-05-i-figli-degli-altri/), sempre liberi, e l’altroieri al [sostegno economico](https://macintelligence.org/posts/2015-08-04-quindici-domande/) per un libro reperibile gratis.

D’altronde che altro mese, fuori da agosto, appare favorevole per suggerire qualche spesa extra budget?

La lancia di oggi è per [LibreOffice](http://www.libreoffice.org). Intanto perché sono sempre socio di [LibreItalia](http://www.libreitalia.it); poi perché è uscita la versione 5.0; infine perché sembra ieri pensando all’uscita della 4. Ambedue versioni importanti, con dietro una marea di lavoro, che tecnicamente e per facilità di utilizzo costituiscono grandi progressi.

L’evoluzione di LibreOffice sta procedendo in modo molto più rapido di quanto avrei mai sospettato, e pure per il meglio. Il programma è un vero bastione di libertà perché è l’unico argine opponibile su scala planetaria all’omologazione conformista di Microsoft e perché il suo sviluppo dipende strettamente dalle donazioni di chi lo usa e potrebbe usarlo anche senza donare.

Non è nemmeno detto che debba essere per forza denaro; anche regalare un po' di tempo – magari per convincere un amico o un ufficio, o un collegio docenti – è un’ottima alternativa.