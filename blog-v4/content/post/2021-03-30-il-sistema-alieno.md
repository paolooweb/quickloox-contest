---
title: "Il sistema alieno"
date: 2021-03-30T00:22:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [Bell Labs, Unix, Plan 9, Linux, Utf-8] 
---
Non avevo mai, mai, mai saputo né letto alcunché di [Plan 9](https://www.bell-labs.com/institute/blog/plan-9-bell-labs-cyberspace/).

Negli anni ottanta, salta fuori, il gruppo dei creatori di Unix ha iniziato a lavorare a un altro sistema operativo, in cui i Bell Labs non hanno creduto fino in fondo.

Così Plan 9 non è mai decollato; tuttavia ha contaminato positivamente altri sistemi operativi liberi e commerciali con varie innovazioni e idee che oggi diamo per scontate e che, senza di lui, avrebbe dovuto essere messe a punto da qualcun altro in qualche altro modo.

>il concetto di rendere i servizi del sistema operativo disponibili tramite il filesystem è oggi pervasivo su Linux; il sistema minimalista di windowing di Plan 9 è stato replicato più volte; la codifica di caratteri Utf-8 usata universalmente nei browser è stata inventata per Plan 9 e lì implementata per la prima volta; il design di Plan 9 anticipa le odierne architetture a microservizi di oltre un decennio!

Oggi il codice di Plan 9 [viene reso pubblico](https://p9f.org/) e affidato a una [fondazione](https://plan9foundation.org).

Qualunque sarà il suo futuro, colpisce che il nome derivi dal filmaccio [Plan 9 from Outer Space](https://www.imdb.com/title/tt0052077/); tutta questa faccenda, nonostante fosse niente affatto segreta, è stata aliena pressoché a chiunque per quarant’anni. Pazzesco.

*Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile.*