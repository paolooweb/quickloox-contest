---
title: "Prendiamoci tempo"
date: 2016-08-15
comments: true
tags: [Cook, Ferragosto, netbook]
---
Difficile trovare il tempo per leggere la lunghissima [intervista](http://www.washingtonpost.com/sf/business/wp/2016/08/13/2016/08/13/tim-cook-the-interview-running-apple-is-sort-of-a-lonely-job/) concessa da Tim Cook al *Washington Post*. Eppure si dovrebbe. Solo un pezzo che trovo significativo:

>La tecnologia è uno di quei settori dove ogni settimana appare un nuovo oggetto scintillante che tutti devono avere. I netbook… guardo indietro e tutti scrivevano dei netbook come di questa cosa incredibile e ognuno ci chiedeva “Perché non ne state costruendo uno?” 

È incredibile come il tempo aggiusti la visione e la proporzione delle cose, se appena uno ne concede abbastanza, alle cose e a se stesso.

Spero che per tutti sia una giornata bella in cui sia possibile prendersi del tempo e mettere un pochino di distanza tra sé e le cose. Si vedranno meglio, più a fuoco, più vere.

Buon Ferragosto a ciascuno e grazie per una compagnia che è sempre preziosa e gradita.
