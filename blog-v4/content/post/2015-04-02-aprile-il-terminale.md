---
title: "Aprile il Terminale"
date: 2015-04-02
comments: true
tags: [Uber, pesce, Google, Pac-man, Slashdot, aprile, Adams, Ubuntu, servizi, OSX, Finder, Spotlight, Quicksilver, Alfred, Dock, Terminale, iTerm2, TotalTerminal, XMenu, Rumps, Python]
---
(Il servizio [Uber Boat a Bangkok](http://blog.uber.com/bkkuberboat) è simpatico; Pac-man sulle mappe di Google è molto carino; però quest’anno vince a mani basse il premio per il pesce d’aprile migliore la serie di [Slashdot](http://science.slashdot.org/), che ha trasformato le più famose saghe della fantascienza in una decina di notizie semiplausibili e riuscite. Divertente e non le ho neanche individuate tutte. Un solo esempio, la [Guida Galattica per Autostoppisti](http://science.slashdot.org/story/15/04/01/1632234/scientists-discover-meaning-of-life-through-massive-computing-project)).<!--more-->

Mi si chiede se sia possibile aprire il Terminale con una scorciatoia da tastiera, come accade per esempio [su Ubuntu Linux](https://help.ubuntu.com/community/UsingTheTerminal).

Così com’è, il Terminale non possiede questa caratteristica. Ma si può ovviare in diversi modi.

Nelle Preferenze di Sistema, voce Tastiera, nel gruppo Servizi c’è un servizio che lancia il Terminale. A tutti i servizi è possibile associare una scorciatoia e anche a questo. Esiste anche un servizio che apre una finestra di Terminale già impostata sulla *working directory*, la finestra in primo piano nel Finder.

Anche se non è una vera e propria scorciatoia diretta, con una scorciatoia di tastiera si apre Spotlight e basta digitare le prime lettere di *Terminale* per evidenziare il programma, che con una pressione su Invio parte. Un ragionamento analogo, più o meno efficiente, vale per le *utility* come [Quicksilver](http://qsapp.com) oppure [Alfred](http://www.alfredapp.com).

Anche il campo di ricerca di Launchpad può trovare e lanciare il Terminale. Anche un’icona del Terminale nel Dock è raggiungibile e lanciabile attraverso la tastiera.

Credevo esistessero *menulet* grafici da installare nella barra dei menu di Mac e in grado di lanciare specificamente il Terminale, ma non ne ho trovati. Naturalmente applicazioni generaliste come [XMenu](https://itunes.apple.com/it/app/xmenu/id419332741?l=en&mt=12) possono lanciare dalla barra dei menu qualunque cosa e quindi anche il Terminale. A questo punto valgono anche il comando Elementi Recenti dentro il menu Apple o la navigazione gerarchica da una cartella contenente il Terminale inserita nel Dock.

Esiste anche l’opzione di sostituire definitivamente il Terminale con [TotalTerminal](http://totalterminal.binaryage.com) oppure [iTerm2](http://iterm2.com); entrambe le applicazioni possono essere lanciate da una scorciatoia di tastiera.

Il gioco può complicarsi sempre più, tipo con l’installazione di [Rumps](https://github.com/jaredks/rumps) e conseguente codifica in Python di una scorciatoia di tastiera che lanci il Terminale dal menu che il programma apre sulla barra. 

Si potrebbe pensare a una gara di folli che giocano a risolvere il problema con le condizioni di partenza più difficili e proibitive. Per ora quanto sopra potrebbe bastare.