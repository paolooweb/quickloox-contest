---
title: "Neanche si avvicinano"
date: 2017-02-15
comments: true
tags: [iPad, Pencil, Procreate, Marco]
---
Grave errore oggi: ho dedicato a Facebook più di sette minuti.

Mi sono trovato a discutere di uranio impoverito, dei numeri di versione di macOS e di tutta una serie di altre sciocchezze.

Nel frattempo **Marco** aveva usato Facebook in modo migliore, con la sua testimonianza diretta di uso di iPad Pro e Apple Pencil.

<iframe src="https://www.facebook.com/plugins/post.php?href=https%3A%2F%2Fwww.facebook.com%2FmrKappa%2Fposts%2F10154295319981451&width=500" width="500" height="626" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true"></iframe>

>Ce ne sono tante, di alternative: da quelle più economiche (Wacom Creative Stylus più tablet qualsiasi) a quelle più costose (i nuovi tablet Wacom da 16”), ma semplici e intuitivi come Apple Pencil e iPad Pro non ce n’è. Not even close.

*Not even close*, nemmeno si avvicinano. Devo imparare a lasciare Facebook ai frustrati e usare quel tempo per fare cose belle.

**Aggiornamento**: **theoksantiago** segnala questo bellissimo [articolo](http://m.imore.com/kyle-lambert-made-his-illustration-career-ipad-pro) sull’esperienza di Kyle Lambert, illustratore, con iPad Pro. Da leggere e da guardare. Lui sembrerebbe un professionista.