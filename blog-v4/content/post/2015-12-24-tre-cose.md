---
title: "Tre cose"
date: 2015-12-24
comments: true
tags: [Python, Lisp, Automator, AppleScript, Workflow, Editorial, Mac, iOS, open, Drafts]
---
Di fronte al fatto che è quasi Natale mi rifiuto di commentare l’attualità e preferisco dare tre consigli per il giorno in cui tutti siamo o dovremmo essere più buoni. Consigli valevoli anche nei giorni successivi.<!--more-->

1. Acquistare una *app* in più. Vero che il denaro non si spreca; vero che di questi tempi eccetera eccetera; vero che non si possono comprare due milioni di *app* per sfizio. Vero tutto. Però non voglio vedere nessuno finire all’inferno, nel girone *costava 0,99 ed era troppo*. Mica una al giorno. Una. Zero novantanove.
2. Sostenere un progetto *open source*. Sostenere è un verbo con mille significati, dei quali programmare è solo uno. Si può contribuire con la grafica, con la traduzione o la redazione di documentazione, con la localizzazione dei comandi, con la creazione di contenuti specifici per quel programma. Si può compilare una versione Mac funzionante del programma ove nessuno lo avesse fatto prima. Si può parlarne entusiasticamente (se merita) sul blog, sui *social*, a cena con gli amici. Si può adottarlo e promuoverne l’uso – se vale la pena – negli uffici, nelle scuole, nelle famiglie. Si può fare una donazione, anche piccolissima. Si può trovare un modo che qui non mi è venuto in mente ed è la cosa più facile di tutte.
3. Automatizzare un pezzo di vita con il computer. Con [AppleScript](http://macosxautomation.com/applescript/), [Automator](http://macosxautomation.com/automator/), [Python](http://www.python.it) su Mac, [Workflow](https://workflow.is) o [Drafts](http://agiletortoise.com/drafts/) o [Editorial](http://omz-software.com/editorial/) o altri su iOS, con il Calendario, con il menu Servizi, [Common Lisp](https://common-lisp.net), qualsiasi cosa. Il requisito è che ci sia una cosa che ora si fa a mano e, a fine trattamento, ci pensi il computer da solo. Tutta salute, tanto da imparare, cervello vivo.
4. Giocare a ciò che si vuole. Purché assolutamente in compagnia, locale o remota.

Sì, erano tre.