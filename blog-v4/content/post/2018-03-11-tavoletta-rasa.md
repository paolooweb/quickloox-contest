---
title: "Tavoletta rasa"
date: 2018-03-11
comments: true
tags: [iPad, Matteo, Pixel, Google, Android]
---
Ringrazio **Matteo** per la segnalazione di questa eccellente [ricostruzione della storia di Android per computer a tavoletta](http://appleinsider.com/articles/18/03/09/google-gives-up-on-tablets-android-p-marks-an-end-to-its-ambitious-efforts-to-take-on-apples-ipad) che ha portato, oggi, all’abbandono di fatto del formato da parte di Google.<!--more-->

>Immaginate i titoli che apparirebbero se Apple smettesse di supportare iPad nella prossima edizione di iOS.

L’articolo è lunghissimo, pieno di riferimenti storici, paralleli con le vicende di Windows e Mac, il punto sulla rivoluzione vera che ha portato iPad, nel momento in cui i so-tutto-io spiegavano compunti che [mancava un Mac in formato netbook](https://macintelligence.org/posts/2009-10-24-in-attesa-del-notbook/).

Lettura raccomandata. Se non c’è tempo e occorre la sintesi estrema, sta tutta nella citazione qui sopra. Android smette di competere con iPad. Se sembra poco…