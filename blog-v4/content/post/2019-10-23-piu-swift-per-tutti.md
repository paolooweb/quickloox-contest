---
title: "Più Swift per tutti"
date: 2019-10-23
comments: true
tags: [Swift, AppleScript]
---
La storia di Apple è densa di tecnologie eccellenti che Apple stessa non ha usato come si meritavano. AppleScript per esempio.

Per fortuna sta andando diversamente con Swift. A leggere *Timac*, il numero di file binari originati da codice Swift in iOS [cresce di anno in anno con progressione geometrica](https://blog.timac.org/2019/0926-state-of-swift-ios13/).

Swift è davvero il futuro dello sviluppo Apple che sta diventando presente. Se qualcuno avesse voglia di considerare un avvicinamento alla programmazione, potrebbe partire da [Swift Playgrounds](https://www.apple.com/swift/playgrounds/). Cominciare con Swift in modo semplice e giocoso è possibile e per i concetti sofisticati di programmazione c’è tempo.

Swift funziona nella riga di comando e oramai in rete si trova molto materiale ben oltre le pagine del sito sviluppatori Apple.

Programmare è anche un bell’allenamento mentale. Oh, insomma, se c’è ancora bisogno di scuse, come non detto.