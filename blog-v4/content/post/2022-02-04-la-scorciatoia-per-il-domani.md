---
title: "La scorciatoia per il domani"
date: 2022-02-04T01:32:51+01:00
draft: false
toc: false
comments: true
categories:
tags:
---
Si diceva che [Wordle è stato acquistato dal New York Times](https://macintelligence.org/posts/2022-02-01-non-vendo/) e, mentre nelle mani del quotidiano il giochino potrebbe diventare un appuntamento irrinunciabile per gli appassionati, più o meno come il loro cruciverba, c’è chi teme che venga perduta la [risorsa gratuita e semplicemente al servizio di tutti](https://macintelligence.org/posts/2022-01-17-l-internet-di-una-volta/) che è il marchio di fabbrica di Wordle originale.

Per chi vuole, è semplicissimo conservare Wordle per (ragionevolmente) sempre su qualche proprio apparecchio; il gioco si articola in due file, che basta scaricare e organizzare in modo elementare per averlo in locale.

Farlo manualmente, tuttavia, non è elegantissimo.

Mentre invece disporre di un Comando rapido, come al solito preparato dalla *gang* di MacStories capitanata da Federico Viticci, è cosa elegante e raffinata. Una cosa semplice, se appena si ha dimestichezza con il sistema, che al primo lancio [provvede a scaricare i due file necessari](https://www.macstories.net/ios/preserve-and-play-the-original-wordle-for-decades-with-wordleforever/) e li rende disponibili per giocarci in locale, così come è Wordle adesso, quello genuino, qualsiasi cosa gli accada.

In questo modo Wordle nella sua edizione originale potrà degnamente passare al suo pezzo meritato di storia del computing su migliaia, milioni di iPhone, iPad, Mac.

E i Comandi rapidi (in inglese *Shortcuts*, scorciatoie) si confermano ancora una volta una bella chiave per interpretare il nostro domani con la tecnologia che amiamo. Abbiamo a disposizione uno strumento per padroneggiarla e convincerla a servirci in modo speciale e unico, su misura della nostra volontà. Siamo chiamati ad approfittarne, nel senso migliore del termine.
