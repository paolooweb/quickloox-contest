---
title: "Classifiche atipiche"
date: 2020-01-01
comments: true
tags: [PosteID, WhatsApp]
---
App che risultano essere le più inutilmente fastidiose nel momento in cui cambi computer da tasca, riparti da un backup e ti aspetti di essere operativo dal primo momento, senonché ti mettono in mezzo ostacoli procedurali di classico cattivo design: PosteID e WhatsApp a pari merito.

Motivazione: invece di considerare i problemi di sicurezza di un cambio di apparecchio e risolverli con un design sagace al servizio dell’utente, scaricano sull’utente stesso ogni difficoltà di gestione, con la stessa mentalità degli uffici statali degli anni settanta.

Per fortuna inizia un anno nuovo. Speriamo di passare tanto tempo su app che ci aiutano e favoriscono invece di colpevolizzarci. È l’augurio di oggi.
