---
title: "Obiettivo: superarsi"
date: 2018-11-24
comments: true
tags: [Microsoft, Apple, 9to5Mac]
---
[Microsoft supera per poco tempo Apple come azienda più capitalizzata del mercato americano](https://www.khaleejtimes.com/technology/microsoft-overtakes-apple-as-most-valuable-us-company).

Succede.

Questo succede meno di frequente: il titolo originale contiene la parola *briefly* (brevemente, per poco tempo) mentre il link *microsoft-overtakes-apple-as-most-valuable-us-company* ne è privo.

Per chi sia appena pratico di sistemi di pubblicazione per il web, questo è indizio piuttosto chiaro che la parola *briefly* sia stata aggiunta successivamente. Il sorpasso infatti è durato pochissimo e alla chiusura dei mercati era già evaporato, lasciando Apple in vantaggio di ventisei miliardi su Microsoft.

La capitalizzazione delle aziende è un gioco aritmetico (moltiplico valore delle azioni per numero delle azioni) abbastanza fine a se stesso, in quanto naturalmente esposto alle fluttuazioni del mercato, che ultimamente fluttua un bel po’. Ogni tanto torna alla ribalta quando succede qualcosa di interessante nella classifica, per esempio Apple che [per prima negli Stati Uniti supera il trilione di dollari](https://macintelligence.org/posts/2018-08-02-la-guerra-strisciante/). O appunto un sorpasso in cima alla classifica.

Colpisce l’ansia di scriverne, come se da tempo non si fosse atteso altro. Per dover rettificare (a malincuore?) poco dopo, *briefly*.