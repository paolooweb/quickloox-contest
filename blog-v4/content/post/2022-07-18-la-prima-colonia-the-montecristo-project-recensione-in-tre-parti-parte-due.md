---
title: "La Prima Colonia - The Montecristo Project: recensione in tre parti - parte due"
date: 2022-07-18T01:10:02+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [Edoardo, Edoardo Volpi Kellermann, The Montecristo Project, La Prima Colonia, Bea, Carlo, Francesca]
---
Non mi sono dimenticato de [La Prima Colonia](https://eucear.eu); anzi, sono finalmente arrivato in fondo alla lettura. Se ci ho messo così tanto dipende da me, il libro non c’entra. Ho trovato finalmente un’organizzazione di lettura compatibile con il giusto ritmo per questo libro, che si gusta meglio a mio parere con monoporzioni giornaliere.

Dopo la [parte zero](https://macintelligence.org/posts/2022-05-19-la-prima-colonia-the-montecristo-project-recensione-in-tre-parti-parte-zero/) di questa recensione, in cui esponevo i miei criteri, e la [parte uno](https://macintelligence.org/posts/2022-05-28-la-prima-colonia-the-montecristo-project-recensione-in-tre-parti-parte-uno/), come è fatta l’opera, oggi parliamo di *che cosa fa* l’opera.

Abbiamo già detto che è fantascienza dura e pura e che intreccia un respiro cosmico con uno di vita quotidiana, chiedendosi e suggerendoci come potrebbero essere tra qualche decina di anni tante componenti della nostra società che oggi hanno un ruolo più o meno preminente. Ok, ma che cosa succede?

Lo scenario è intricato. Ci sono vari filoni narrativi che si intrecciano; alcuni si risolvono, altri si chiudono con *cliffhanger* che rimandano al prossimo o ai prossimi libri (*The Montecristo Project* è una trilogia e *La Prima Colonia* è il primo libro).

Uno di questi filoni, posso parlarne perché Edoardo (l’autore) lo scrive senza patemi nei materiali promozionali, è la nascita di una coscienza artificiale (non intelligenza; coscienza). Se parlassimo di voli aerei, sarebbe quello a quota più alta.

Più sotto, a quote diverse, abbiamo spionaggio governativo, un (un?) amore che nasce, angiporti di una città cara all’autore, il futuro delle comunità open source, una nuova forma di contatto umano con altri mammiferi, famiglie difficili, politici problematici, hacker geniali, nanomacchine malandrine, un esperimento rivoluzionario, sabotaggi di alto profilo, organizzazioni criminali, un diverso ordine mondiale, scienziati vecchi e giovani a confronto, credo di dimenticare diverse componenti.

L’universo de *La Prima Colonia* è composto da stringhe; per ognuna di queste situazioni c’è una vicenda che si dipana. Come è facile intuire, alla fine del libro (o dei libri) le varie stringhe, all’inizio distanti e apparentemente divergenti, si intrecceranno solidamente. Quante, quali e come va lasciato alla lettura.

Ogni capitolo (ogni *glifo*, nel gergo dell’opera) si focalizza su una di queste storie e la porta avanti. Per questo parlavo inizialmente del ritmo giusto da dare alla lettura della storia. Mi sono reso conto che non aveva senso aggredire il tomo e mescolare nanomacchine con stupefacenti (nel libro, nel libro…). Invece, molto meglio una giusta dose al giorno di un contenuto univoco: oggi gli infiltrati nell’organizzazione, domani il ragazzo alla ricerca di un perché, dopodomani Carlo e Francesca eccetera eccetera.

Chi ha letto qualcosa della genesi del libro sa che balenò la possibilità di una versione cinematografica; la struttura del libro, con i filoni che procedono paralleli andando verso la conclusione comune, è mooolto cinematografica.

Vincerà lo spirito di gruppo oppure l’obbedienza agli ordini? Che cosa sta combinando Bea? Chi c’è veramente dietro gli sforzi di sabotaggio della Grande Impresa? È veramente amore o un semplice trovarsi coinvolti insieme in un meccanismo che mette a dura prova corpi e menti?

Questo e altro ancora sono *che cosa fa* il libro. Gli spunti narrativi sono numerosi e ovviamente qui non si svela nulla. La cosa di cui ci si rende conto a un certo punto è che, tra inseguimenti, intrecci politici, armi cibernetiche, esperimenti arditi, operazioni azzardate, interferenze militari e passeggiate in monoruota ci si perde in un turbine di vicende, piacevolmente. Ci si chiede come andrà a finire tutto. Intanto, la coscienza artificiale sullo sfondo cresce, evolve, si trasforma e sembra non avere parte nel quadro, quando invece ne è il fondamento.

Forse è qualcosa che sta succedendo anche nel mondo reale e non lo sappiamo. Non ancora.

La prossima volta completerò la recensione raccontando *come lo fa*, come *La Prima Colonia* risponde (o meno) alle mie aspettative.