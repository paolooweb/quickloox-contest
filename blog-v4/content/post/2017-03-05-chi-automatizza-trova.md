---
title: "Chi automatizza trova"
date: 2017-03-05
comments: true
tags: [BBEdit, Drang]
---
Divento noioso, ripetitivo e petulante sull’argomento dell’automazione del nostro *computing*. Dall’uso più efficiente dell’interfaccia fino allo *scripting* e ai programmi che fanno lavorare il computer da solo, se usiamo il computer nel modo più manuale e immediato, perdiamo tempo, produttività e possibilità. Stiamo attaccati alla tastiera o chi per lei mentre potremmo fare cose migliori. Questo, alla lunga, genera danni.

Un esempio di questi giorni è un modesto e spettacolare articolo di Dr. Drang dedicato a un argomento apparentemente anodino come la [funzione di ricerca e sostituzione su BBEdit](http://leancrew.com/all-this/2014/12/bbedit-finding/).

A prima vista, un cerca-e-sostituisci è routine. Dr. Drang mostra le opzioni di esecuzione da tastiera delle opzioni a disposizione; discute della ricerca veloce in diretta nel documento, che in casi determinati porta a un gran risparmio di tempo; e ricorda le combinazioni da tastiera che riempiono subito con il testo selezionato il campo di ricerca e quello di sostituzione, senza dover aprire la finestra.

Non sono degno di allacciare le scarpe a Dr. Drang e però, lavorando da anni con BBEdit, conosco tutto quello di cui ha trattato. E garantisco che il lavoro di ricerca e sostituzione in BBEdit, con la padronanza delle combinazioni da tastiera – personalizzabili! – procura vantaggi enormi rispetto all’uso più immediato dell’interfaccia. Scrive Drang:

>La ragione per cui non mi spreco con gli editor di testo semplici, perfino quelli con funzioni intelligenti quali la formattazione Markdown istantanea, è che le loro funzioni limitate ti mantengono a un livello da novizio, non importa da quanto tempo li utilizzi. Gli editor di testo seri possiedono una profondità che ricompensa chi ne approfitta.

E così vale per qualunque altro programma evoluto e ogni campo di attività su Mac. L’automazione è tutto, a partire dalle combinazioni di tastiera.