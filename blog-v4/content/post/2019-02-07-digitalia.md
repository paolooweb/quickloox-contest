---
title: "Digitalia"
date: 2019-02-07
comments: true
tags: [India, Stefano]
---
Ringrazio **Stefano** per quanto segue:

**Mi trovavo a Delhi con moglie e figlio** per seguire un amico in uno stand della fiera annuale di arte moderna. Come purtroppo capita in questi casi qualche farabutto ha pensato bene di appropriarsi della borsetta lasciata incautamente sola su una sedia da mia moglie con dentro passaporti e carte di credito.

Tieni presente che senza documenti non puoi entrare in un aeroporto indiano, non basta l’e-ticket sul telefono. Il pensiero vola subito a come poter prendere l’aereo di ritorno da li a poche ore. 

Chiamo ovviamente il numero di emergenza dell’ambasciata italiana e dopo dieci minuti uno stanco funzionario mi risponde che loro non possono fare nulla senza prima la denuncia alla polizia di Delhi. Denuncia? Polizia indiana? Stavo calcolando quanti giorni avrei impiegato per tutto questo.

Poi improvvisamente un membro della security mi informa che si può tranquillamente fare il FIR (First Investigation Report) direttamente dal sito della polizia di Delhi. Dopo pochi passaggi e una facile registrazione viene emesso il documento (verbale) in formato elettronico, ricevo sul telefono un SMS col nome dell’ufficiale che ha preso in consegna il mio caso e con quel PDF (stampato) entro tranquillamente in aeroporto.

Una volta arrivato in aeroporto ricevo la telefonata dall’ufficiale di polizia per raccontargli il fatto e tenermi aggiornato.

Sicuramente non avrò mai più indietro il mio *stuff* ma nella mia testa stavo pensando **ai verbali fatti in passato dai Carabinieri e all’India odierna.**

Pensare che ho [recentemente](https://macintelligence.org/posts/2019-02-02-ritorno-dal-passato/) sporto denuncia di smarrimento del passaporto e ho respirato di sollievo nel vedere un gentilissimo poliziotto stamparla da un computer. Il progresso va a velocità differenti.
