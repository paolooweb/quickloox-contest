---
title: "La nuvola in dieci lezioni"
date: 2018-04-13
comments: true
tags: [cloud, Amazon, Vogels]
---
A seguito della [breve storia della nuvola](https://macintelligence.org/posts/2018-04-07-breve-storia-della-nuvola/) è d’uopo rievocare il [discorso tenuto in occasione dei dieci anni di Amazon Web Services](https://macintelligence.org/posts/2018-04-07-breve-storia-della-nuvola/) da Werner Vogels, responsabile dei servizi informatici.<!--more-->

Mentre il tema e le lezioni possono sembrare un po’ rarefatte (d’altronde parliamo di nuvole…), è utile invece penetrare la superficie e distillare il senso che possono avere questi consigli per la nostra attività, anche se di server non ne abbiamo e lo spazio su disco al massimo è quello di un watch.

Ne cito tre: *Aspettarsi l’inaspettato*, *L’automazione è un fattore chiave*, *Sii consapevole dei tuoi consumi*.

>Marvin Theimer, Distinguished Engineer di Amazon, una volta ha scherzato su come l’evoluzione di Amazon S3 possa essere ben descritta come far decollare un monomotore Cessna, aggiornarlo nel tempo a un 737, poi a uno stormo di 747 e infine a una grande flotta di Airbus 380. Il tutto con rifornimenti in volo e spostando i passeggeri da aereo ad aereo senza che neanche se ne accorgano.

Ecco, lezioni provenienti da uno che lavora a questo livello di complessità vanno considerate.