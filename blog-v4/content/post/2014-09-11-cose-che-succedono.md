---
title: "Cose che succedono"
date: 2014-09-11
comments: true
tags: [Apple, Facebook, iOS, iPod, Windows, religione, adepti, bakeka]
---
Nel 2014 mi sono trovato ancora a leggere, in ritardo di vent’anni, che chi sceglie Apple è l’adepto di una religione.<!--more-->

Ho fatto presente che per parlare di religione Apple è necessario credere alla sua esistenza.

Ho ricordato che esiste il meccanismo psicologico della dissonanza cognitiva: i dati della realtà non coincidono con la nostra opinione e allora modifichiamo l’opinione in modo da non contraddire le nostre conclusioni, ma spiegare nel contempo i risultati derivanti dall’osservazione della realtà.

Poi ho posto alcune domande: se chi ha acquistato iPod è adepto di una religione, perché le vendite di iPod sono in calo? Se il numero di apparecchi iOS in circolazione è dieci volte il numero di Mac in circolazione, significa che un numero enorme di utilizzatori di iOS lavora su un PC, quindi su Windows. Dunque come possono essere adepti della religione Apple?

Date queste premesse, sono stato accusato di tacciare di cretini e stupidi i sostenitori della tesi religiosa.

E siccome ho compiuto l’oltraggio di entrare *nella mia bakeka* (come ha scritto il proprietario della pagina), ché invece di essere su Facebook pareva di compiere una violazione di domicilio, il titolare *ha rimosso il post*.

Ho creato un [file Pdf](http://macintelligence.org/images/roberto-dadda-solo-io-quando-vedo-gli-adepti-alla-religione.pdf) a futura memoria, perché temo che se lo racconto e basta possa passare per una mia fantasia. Tutto questo nel 2014. Cose che succedono, certo. Ma pensavo a un secolo passato.

 ![Apple come religione](/images/roberto-dadda-solo-io-quando-vedo-gli-adepti-alla-religione.pdf  "Apple come religione") 