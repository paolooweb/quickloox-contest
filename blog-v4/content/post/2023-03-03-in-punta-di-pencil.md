---
title: "In punta di Pencil"
date: 2023-03-03T18:49:58+01:00
draft: false
toc: false
comments: true
categories: [Hardware]
tags: [Apple Pencil, Nive]
---
Sono piuttosto attento a tenere in carica gli apparati sulla scrivania. Con [Apple Pencil](https://www.apple.com/it/apple-pencil/) è un po’ diverso perché tuttora [non fa veramente parte del mio flusso di lavoro](https://macintelligence.org/posts/2022-12-30-le-magie-della-punta/) e non è uno strumento che senta di voler utilizzare.

Così è successo che me la sia dimenticata per giorni sulla scrivania suddetta, anche perché non c’è l’idea del connettore; si carica se aderisce magneticamente al fianco destro di iPad. Anche quando non sono in viaggio con iPad, spesso sono in giro per casa; vedo la Pencil attaccata, la stacco perché non mi serve e poi non la riattacco.

In definitiva, arriva la secondogenita che vuole disegnare su iPad. Usa Note – non abbiamo neanche ancora preso una app dedicata al disegno – ed è vastamente più competente di me.

Mi rendo conto della situazione, istantaneamente attacco la Pencil a iPad e appare l’avviso di carica zero percento.

Provo a sviare l’attenzione della ragazza proponendole altre attività, solo che una volontà di cinque anni sa come mostrarsi irremovibile in certi frangenti.

Passa sì e no un minuto e le do l’aggeggio, avvisandola che la batteria è scarica, che durerà pochissimo, di non arrabbiarsi eccetera.

Torna un quarto d’ora dopo, soddisfatta e sorridente e mi rimette in mano tutto. Su Note campeggia un uccello dal piumaggio variamente colorato, secondo l’interpretazione di uccello e piumaggio che può dare un secondo anno di materna.

Rimetto la Pencil in carica. Uno percento. Missione compiuta nonostante la mia inadempienza.

Da allora la carico più spesso, con più attenzione e meno preoccupazione. Anche se non è carica al massimo, può dare il massimo per un buon tempo.