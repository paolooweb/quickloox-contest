---
title: "Caro social ti scrivo"
date: 2021-07-20T23:56:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [John D. Cook, Unicode, YayText, emacs, Perl] 
---
Ovviamente per amore della buona tipografia, dello scripting e della padronanza dei mezzi di pubblicazione. Mica per fare gli snob con il testo capovolto sperando che almeno uno nella cerchia dei conoscenti risponda *ma come hai fatto?*.

Da questo punto di vista, John D. Cook è una garanzia e per mostrare [come usare il grassetto e il corsivo su Twitter](https://www.johndcook.com/blog/2021/07/19/how-to-format-text-in-twitter/) (o dovunque si possa incollare testo) usa [emacs](https://www.gnu.org/software/emacs/), moduli di [Perl](https://www.perl.org) e altri esoterismi di gran classe.

C’è posto per tutti comunque, anche per gli interessati ai soli effetti speciali in quanto tali, da usare per stupire senza farsi altre domande. La risposta è [YayText](https://yaytext.com).

O ʇxǝ⊥ʎɐʎ, tanto per fare anch’io il bulletto.

*Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile.*