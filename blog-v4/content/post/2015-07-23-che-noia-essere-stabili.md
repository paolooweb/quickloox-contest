---
title: "Che noia essere stabili"
date: 2015-07-26
comments: true
tags: [Dediu, Apple]
---
Horace Dediu ha scritto [un pezzo così bello e preciso](http://www.asymco.com/2015/07/22/the-one-where-i-offer-stock-tips/) che spero mi perdonerà per rubargliene gran parte (se non altro, è breve).<!--more-->

>Investiresti in un monopolio o su una azienda la cui sopravvivenza dipende dal centrare sempre il prossimo grande business?

>Prima di rispondere: investiresti su un grande ereditiero o su un povero ansioso di cogliere qualsiasi opportunità?

>Prima di rispondere: investiresti in immobili di una città che vive di industria con una solida base produttiva, o in quelli di una città che vive producendo film?

Stabilità contro instabilità, commenta Dediu, in tutti i tre casi. Il noto contro l’ignoto.

Storicamente, i capitali sono sempre andati verso la stabilità.

>La ricchezza ha preso l’altra strada.

Dedicato a quanti hanno dubitato finora che Apple potesse esserci ancora da qui a tre anni. In tutti questi trentanove anni.