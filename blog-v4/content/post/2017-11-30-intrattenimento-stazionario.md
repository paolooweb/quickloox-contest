---
title: "Intrattenimento stazionario"
date: 2017-11-30
comments: true
tags: [Windows, Trenord]
---
Uno degli schermi posti senza risparmio di risorse nelle stazioni della linea ferroviaria lombarda Trenord, schermi adibiti a fornire intrattenimento e informazioni per i viaggiatori.<!--more-->

Uno dei vantaggi nell’erogare questo tipo di contenuti è la scarsa necessità di aggiornamento. Difficile che la descrizione della situazione cambi intanto che nessuno interviene.

 ![Schermo in panne dentro stazione Trenord](/images/trenord.jpg  "Schermo in panne dentro stazione Trenord") 
