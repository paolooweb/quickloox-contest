---
title: "Altro che storie"
date: 2021-11-22T02:13:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [The Interactive Fiction Competition] 
---
Si è chiusa da poco la [Interactive Fiction Competition](https://ifcomp.org),  con il risultato netto di mettere a disposizione settantuno avventure testuali create per la partecipazione alla gara.

Se nessuna di esse sarà capace di scatenare l’immaginazione di un lettore-giocatore come e più di qualsiasi sparatutto, più o meno di massa e più o meno online, sarà per mancanza di talento degli autori, non certo per la stanchezza della formula.

Un ipertesto interattivo ben congegnato batte qualsiasi altra forma di intrattenimento equivalente. E così continuerà a essere. Ciò che dorme nel nostro cervello e aspetta solo un catalizzatore è sempre superiore alla nostra capacità di espressione spontanea.

Vado a fare un giro nelle settantuno proposte.

_Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile._