---
title: "Ferragosto in anticipo"
date: 2021-08-14T01:52:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [Apple TV, Amadine, Pixelmator Pro] 
---
È finita da poco la migliore sessione dell’anno su [Roll20](https://roll20.net), il pomeriggio è andato giocando con le figlie (anche [su Apple TV](https://macintelligence.org/posts/Uno-schermo-per-tutti-tutti-per-una-app.html)), ho fornito [Amadine](https://amadine.com) alla moglie che a gran voce reclamava *un vettoriale che costi poco ma con cui lavorare in modo interessante come si fa con [Pixelmator Pro](https://www.pixelmator.com/pro/) per il bitmap* e un grillo lungo quattro dita passeggia sullo schermo del Mac che è l’unica fonte di luce in casa.

Dovrei cominciare adesso a preparare il post di oggi, ma decido unilateralmente che mi prendo una vacanza anticipata. Sii paziente.

*Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile.*               