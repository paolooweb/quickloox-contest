---
title: "Raddrizzare le storture"
date: 2014-02-27
comments: true
tags: [mouse, Xerox]
---
C’è una ragione per cui la freccia che funge da puntatore del mouse è obliqua.<!--more-->

Ho sempre pensato che fosse facilitare la visione del contenuto che eventualmente si trovasse sotto.

Invece è questione dei monitor degli anni settanta: la loro risoluzione era bassa al punto tale da sconsigliare un puntatore verticale, che venne [inizialmente usato e poi abbandonato](http://9to5mac.com/2014/02/26/ever-wondered-why-your-mouse-pointer-is-angled-not-straight/).

Anche dopo quarant’anni la paleoinformatica ha da insegnare. A me, si intende.