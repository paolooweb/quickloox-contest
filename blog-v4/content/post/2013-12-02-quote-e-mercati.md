---
title: "Quote e mercati"
date: 2013-12-02
comments: true
tags: [iPhone, iPad, Android]
---
Il quarto giovedì di novembre è per gli americani il *Thanksgiving Day*, giorno del ringraziamento. Il venerdì dopo lo chiamano *Black Friday* perché, satolli e in vacanza, si buttano nello *shopping* e inaugurano la stagione commerciale natalizia.<!--more-->

Hanno speso *online* quasi due miliardi di dollari, in aumento del 39 percento sull’anno scorso. Una volta su quattro, l’acquisto *online* è stato condotto da un computer da tasca o a tavoletta.

Le tavolette Android hanno contribuito a vendite per 42 milioni di dollari. I computer da tasca, sempre Android, per 106 milioni.

iPhone ha fatto la sua parte per 126 milioni di dollari. Da solo, è stato all’altezza di tutto Android messo insieme.

iPad ci ha messo 417 milioni di dollari. Gioco, partita, incontro.

Ho commentato il *Black Friday* anche [l’anno scorso](https://macintelligence.org/posts/2011-11-27-esseri-inutili/). Nel frattempo, leggo ovunque una volta a settimana, Android ha conquistato importantissime quote di mercato.

Intanto, quando gli apparecchi vengono usati veramente e la gente vota con il portafogli, iOS genera 543 milioni di dollari di vendite; Android 148 milioni. Il rapporto è undici contro tre.

Passato un anno è cambiato poco o nulla e mi chiedo: quale negoziante, inserzionista, programmatore, finanziatore sceglierà in prima battuta Android per un qualsiasi progetto destinato a generare ricchezza?

In omaggio, l’[infografica](http://www.cmswire.com/cms/customer-experience/retailers-thankful-for-ipads-ecommerce-and-record-sales-infographic-023329.php) riassuntiva dei dati.

 ![Black Friday 2013](/images/black-friday-2013-infographic.jpg  "Black Friday 2013") 