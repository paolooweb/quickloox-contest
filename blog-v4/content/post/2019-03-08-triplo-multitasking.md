---
title: "Triplo multitasking"
date: 2019-03-08
comments: true
tags: [iPad, multitasking]
---
Due app su iPad Pro in modalità affiancata e lo sapevo. Mi scappa un dito che striscia da destra ed ecco apparire la terza app, in modalità galleggiante.

Già avere le app affiancate è un progresso sostanziale nella produttività con iPad. La terza finestra è meno determinante ma aggiunge possibilità ulteriori.

Inutile dire che, da questo punto vista il modello da 12"9, è scelta clamorosamente superiore. Mai avuto ripensamenti e questo è un colpo direi decisivo.
