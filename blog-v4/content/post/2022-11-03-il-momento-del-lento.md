---
title: "Il momento del lento"
date: 2022-11-03T14:28:09+01:00
draft: false
toc: false
comments: true
categories: [Internet]
tags: [Frecciarossa]
---
Dopo qualche aereo e molti treni, tutti regionali, ho preso un Frecciarossa, in andata e al ritorno. Non succedeva da prima della pandemia.

All’andata, su settantaquattro minuti previsti, il treno ne ha aggiunti venticinque. Al ritorno, dieci più cinque che aveva cumulato prima di arrivare.

Sulla stessa tratta, fino al 2019, ho viaggiato molte volte, su Frecciabianca. Il Frecciabianca, seppure con qualche ritardo occasionale, è sempre stato meno lento.

Insomma, il Frecciarossa è lento.

A questo serve il Wi-Fi di bordo: mostrare qualcosa di ancora più lento così che il treno, a confronto, sembri veloce.