---
title: "Dalla AAA alla ZZZ"
date: 2021-11-08T03:33:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [M1, Forbes, Tom’s Guide, M1 Max, MacBook Pro, Windows, Parallels, MacGamerHQ, Apple Silicon Games] 
---
Dovrebbe essere chiaro, spero che titolare [Abbiamo provato giochi per PC su un MacBook Pro Mi Max ed è stato un disastro](https://www.tomsguide.com/opinion/we-tried-playing-pc-games-on-an-m1-max-macbook-pro-it-was-a-disaster) è evidentemente contromarketing, pagato dietro le quinte da qualche soggetto invidioso o rosicone, che deve cercare di mettere in cattiva luce un concorrente difficile da emulare.

*Emulare* è la parola chiave, perché *Tom’s Guide* ha scelto dichiaratamente la strada meno efficace e solo quella: giocare titoli per Windows emulandoli via [Parallels](https://www.parallels.com/it/).

Come se non fosse l’unica strada e come se già dall’anno scorso non girassero articoli sulla stessa falsariga. Questo indica come l’intento sia malevolo da subito; l’anno scorso si scrivevano le stesse cose su M1. Ora si parla di M1 Max e quantomeno sarebbe appena corretto fare un raffronto e, nell’ipotetico disastro, riferire se ci sia stato un qualche guadagno di prestazioni, e se sì di che entità.

Perché è passato quasi un anno da quando *Forbes* titolava [Buone prestazioni per i giochi piccoli e leggeri, ma esperienze dolorose per alcuni giochi](https://www.forbes.com/sites/patrickmoorhead/2020/12/26/apple-macbook-pro-13-m1-aaa-gaming-experience-good-performance-for-thin-and-lite-but-rough-experience-for-some-games/). M1 su MacBook Pro 13”; con un M1 Max, sulla carta, i risultati dovrebbero essere tre volte meglio, o meno peggio. Se anche non fosse così, è ovvio attendersi una differenza.

Bisogna anche ignorare i giochi importanti che però funzionano bene: MacGamerHQ elenca [più di cento giochi](https://www.macgamerhq.com/apple-m1/native-mac-m1-games/) e ci sono cose che proprio non funzionano, altre che lo fanno male; ma è solo una faccia della moneta. I giochi con prestazioni *good* sono decine. Per verifica, si può fare qualche ricerca su [Apple Silicon Games](https://applesilicongames.com).

Gente onesta considererebbe anche la novità della piattaforma. Giusto per confronto, in questo momento [oltre cinquanta giochi per PC non funzionano](https://www.pcmag.com/news/intel-these-50-plus-pc-games-are-incompatible-with-alder-lake-cpus-due)… su PC, se il processore appartiene all’ultimissima linea Intel.

Ma questa non è gente onesta; devono a tutti i costi trovare qualche strada per denigrare M1, avendo un pubblico che vuole leggere esattamente questo.

Solo che basta un attimo di attenzione e i loro sforzi per fare propaganda sopra i titoli AAA finiscono per suscitare sbadigli, o peggio.

*Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile.*