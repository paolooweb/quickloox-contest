---
title: "Guerre di religione"
date: 2016-05-18
comments: true
tags: [iPad, Microsoft, Open365, OneDrive, Word, Pages]
---
Ricevo in condivisione un documento e mi appresto a lavorarlo su iPad. Siamo nel 2016: ricevo documenti in ogni formato e modalità e iPad lavora con tutto.

Ma questo, attenzione, è un documento OneDrive.

Microsoft, come si legge ovunque, ha cambiato pelle, non è più quella di una volta, supporta tutto e tutti, gli standard aperti, la collaborazione l’*open source*, *cloud first mobile first* eccetera eccetera.

Apro il documento da OneDrive e parte Word Online. Che apre una anteprima del documento: oltre a guardarlo si può fare pochissimo. OK: *edit in browser*, modificalo nel *browser*.

Cerco un comando per salvare il documento in locale e aprirlo con Pages o equivalente. Non c’è. Cerco un comando di esportazione per cambiare formato al file in modo da lavorarlo con un altro programma. Non c’è. Decido di produrre un Pdf. Non si può, solo stampa. Ricordo che Microsoft ama i comandi del tipo *Invia il documento* e ne cerco uno similare, inutilmente.

Ultima spiaggia: seleziono tutto il testo e lo copio, per incollarlo altrove. Lo schermo di iPad è ingombro di interfaccia per un terzo, tuttavia un comando *Seleziona tutto* non trova spazio. Poco male: c’è una finestrella in cui *digitare il comando che si vorrebbe*. Trovo il *Seleziona tutto* e arriva anche il *Copia*. Copio e… l’incolla, fuori da Word Online, non funziona.

Sono bloccato, irrimediabilmente. Anzi, no. Posso modificare il documento con Word. Se scarico Word e mi creo un account presso Microsoft.

Nel 2016, posso disporre su iPad di un documento Word *solo usando Word*. Lo trovo anacronistico e antipatico. Sarò un fanatico Apple? Può anche essere. Però sono uso a lavorare e collaborare da anni con Google Documenti, che non ha tutte queste fisime. Sta arrivando anche su iOS [open365](https://open365.io/), che invito a scoprire come proiezione di LibreOffice su iPhone e iPad. Il mondo collabora, nel 2016. Pages su iCloud esporta tranquillamente.

La religione di Apple, il culto del Mac? Macché. I fanatici sono questi. Solo loro esistono, nulla esiste fuori da loro. Le religioni, nel XXI secolo, convivono. Le religioni assolutiste e chiuse in sé stesse, che non conoscono il dialogo e la fratellanza, sono nocive alla comunità e vanno estirpate.
