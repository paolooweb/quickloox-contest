---
title: "Pigiatasti addio"
date: 2016-07-05
comments: true
tags: [BlackBerry, Balsillie, Lazaridis, Classic, Qwerty]
---
Ci sono voluti anni e anni di evidenza perché BlackBerry si decidesse alfine a [mandare in pensione BlackBerry Classic[(http://blogs.blackberry.com/2016/07/change-is-only-natural-a-classic-model-makes-way/).

Opportuno ricordare i due ex coamministratori delegati di BlackBerry che condividono il maggior merito per l’impresa, [Jim Balsillie e Mike Lazaridis](http://www.phonearena.com/news/They-said-what-Great-quotes-from-Jim-Balsillie-and-Mike-Lazaridis_id31751). Questo è Balsillie, novembre 2007:

>Per quanto iPhone sia grazioso, offre una vera sfida ai suoi utilizzatori. Provate a digitare un indirizzo web su uno schermo touch di iPhone, è una vera sfida. Non si può vedere ciò che si digita.

Questo è Lazaridis, maggio 2008:

>Il trend mobile più interessante è quello delle tastiere fisiche full Qwerty. Mi spiace, è proprio così. Non me lo sto inventando.

Come al solito, il punto non è avere sbagliato strategia o valutazioni, che capita a tutti. È rendersi ridicoli in campo aperto negando l’evidenza. Addio, pigiatasti.

*[Diversamente da BlackBerry, iPhone costituisce parte cruciale dell’ecosistema Apple e [il libro di Akko su macOS e dintorni](https://macintelligence.org/posts/2016-06-15-un-kickstarter-per-cambiare-il-libro/) ne parlerà in dettaglio. [Per puntare a farsi sostenere e finanziare da chi ci crede](https://www.kickstarter.com/projects/295108589/cuore-di-mela-i-segreti-di-apple-iphone-e-mac).]*