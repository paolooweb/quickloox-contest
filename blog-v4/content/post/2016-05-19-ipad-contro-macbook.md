---
title: "iPad contro MacBook"
date: 2016-05-19
comments: true
tags: [iPad, MacBook]
---
Non sono del tutto io a insistere sul tema della intercambiabilità tra iPad e MacBook: è la vita. Dopo [l’esperienza di Doblerto](https://macintelligence.org/posts/2016-05-11-viva-la-differenza/), leggo questa email di **Stefano**. Lo ringrazio e la pubblico perché dice un sacco di cose che nella loro semplicità dovrebbero esssre banali e invece vanno sostenute a spada tratta come se fossero eresie.<!--more-->

**Mi trovo per lavoro negli USA e sembra che mi abbia letto nel pensiero nel tuo ultimo post riguardo la sostituzione dell'iPad Pro come nuovo computer portatile.**

Ero tremendamente indeciso tra iPad e il nuovo MacBook (per inciso bellissimo, da comprare a prescindere se ti serva o meno, ma questo è un altro discorso).

Le mie esigenze:

- viaggio molto: ho bisogno di un oggetto leggero, che non mi spacchi la schiena tra un aeroporto ed un altro;
- ho bisogno in mobilità di modificare file Excel e Word, per offerte e quant'altro;
- aprire e leggere file CAD, email, leggere libri e vedere film in aereo, modificare PDF-

È chiaro che una volta tornato in ufficio vorrei interfacciarmi col file system del server aziendale per accedere a dei file: si tratta ovviamente di file Excel o Word.

A mio avviso credo che le esigenze di cui sopra siano comuni al 90 percento degli utilizzatori di PC, sopratutto per chi viaggia.

Quindi? Alla fine ho optato per iPad Pro 9,7 pollici WiFi+Cellular.

Utilizzo Google Drive per allocare i “miei” file in un unico contenitore al quale accedo con le diverse App: trovo la Apple SIM alquanto geniale in questo.

Attendo ora di tornare in ufficio sperando con Documents di poter accedere alle cartelle condivise di Windows: dal poco che so, non dovrebbero esserci problemi.

Trovo l’interfaccia di iOS a volte geniale, multitasking incluso, a volte frustrante, ma credo che finalmente ci siamo: il salto si può fare. **Sempre che non risiedi nel restante 10 percento di utilizzatori di cui sopra.**