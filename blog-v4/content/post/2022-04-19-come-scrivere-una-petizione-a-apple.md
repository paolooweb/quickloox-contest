---
title: "Come scrivere una petizione a Apple"
date: 2022-04-19T01:09:52+01:00
draft: false
toc: false
comments: true
categories: [Internet, Software]
tags: [Final Cut Pro, Apple]
---
Contare qualcosa. Essere professionisti affermati è rilevante, se sei nei primi mille. Se sei nel primo milione, sì, sei un bravissimo professionista, però sai com’è. Corollario: tira più un paragrafo di Jason Snell che una petizione di un milione di account iCloud. Sembra antipatico, ma è semplicemente una constatazione numerica.

Lavorare per sé, ma anche per la piattaforma. Lamentarsi è a fondo perso. Spiegare in modo preciso come le cose siano migliorabili può ricevere attenzione. Protestare è vuoto. Professarsi acquirenti da trent’anni non dà diritto a punti fedeltà.

Avere fiducia e pazienza. Apple – nessuno – mostrerà di obbedire o di seguire una petizione. Significherebbe stabilire un precedente e riceverne una nuova ogni giorno da gente che ha trovato un punto sensibile da sollecitare. Se le richieste sono sensate, in un tempo ragionevole verranno recepite, o comunque considerate.

Ecco perché la [lettera aperta a Tim Cook firmata da oltre cento utilizzatori di Final Cut Pro](https://www.gopetition.com/petitions/support-open-letter-to-tim-cook-about-final-cut-pro.html), che chiedono tra l’altro funzioni di collaborazione e una migliore comunicazione di prodotto, ha ottime probabilità di essere considerata.

Si noti che nessuno tira in ballo categorie generiche tipo professionisti e che nessuno accenna neanche di striscio al prezzo. Due bonus.