---
title: "Quando i computer avevano la coda"
date: 2015-11-28
comments: true
tags: [Lisp, Spectrum, random, Basic, MagPi, Zero, Raspberry, Pi]
---
Devo fare riferimento a un mio [articolo](http://www.apogeonline.com/webzine/2015/10/28/meglio-dieci-che-ottanta) in tema *si stava meglio quando si stava peggio?* perché mi impegolo in discussioni piacevolissime e complesse con persone che ricordano con affetto e intensità i ruggenti anni settanta-ottanta del *computing*, epoca di pionieri, macchine geniali e sregolate, in cui non si parlava di interfacce perché tutto che si aveva davanti era il *prompt* del linguaggio Basic e da lì in poi era tutto un improvvisare, scoprire, inventare, imparare provando e riprovando.<!--more-->

Ricordo anch’io con affetto e intensità identici: ho fatto cose in gioventù come lasciare acceso uno [Spectrum 48k](http://www.worldofspectrum.org) tutta la notte ad accendere punti in modo casuale sullo schermo. E che delusione al mattino, quando mi attendevo chissà che risultato e invece c’erano solo fitte strisce oblique. Il mio stupido programmino di disegno casuale di punti usava la funzione di generazione di numeri casuali del Basic Spectrum, ovvero un generatore pseudocasuale. Da lì è partita una curiosità per il problema della [randomizzazione](http://www.worldofspectrum.org) nel *computing* che ancora non si è placata.

Altra grande curiosità che mi è rimasta, il linguaggio [Lisp](http://ep.yimg.com/ty/cdn/paulgraham/onlisp.pdf). Anche lui scoperto sullo Spectrum, con un interprete ingegnoso che accettava anche pezzi di codice scritti in linguaggio macchina. Sembrava incredibile che il linguaggio dell’intelligenza artificiale, usato su [macchine dedicate e costosissime](http://smbx.org), potesse arrivare su un cosetto di plastica collegato a un registratore e a un televisorino in bianco e nero. Eppure su uno Spectrum poteva succedere questo e molto altro.

Per dire, sono completamente solidale.

Quello che non mi permetto è la nostalgia, perché la giudico generazionale. Oggi non abbiamo più il tempo, la testa, la follia adolescenziale, quello che si vuole; ma viviamo tempi – su questo – totalmente migliori.

Prova ne è che [il numero 40 di MagPi è andato esaurito](http://swag.raspberrypi.org/collections/magpi/products/the-magpi-issue-40). MagPi è una rivista dedicata al computer [Raspberry Pi](https://www.raspberrypi.org). Nel numero 40 c’era *in omaggio un computer Raspberry Pi Zero*.

Scoprire, fare pazzie, sperimentare, inventare cose nuove, provare nuovi collegamenti; Raspberry Pi permette tutto questo e la sua dotazione fa impallidire quella di uno Spectrum 48k. Raspberry Pi, semplice e ridotto all’osso, è un computer completo che attende solo di essere utilizzato da chiunque abbia un briciolo di passione. E *costa cinque dollari*.

Il mio Spectrum costava trenta volte di più e non aveva davanti a sé Internet.

Ecco perché ricordo affettuoso sì, nostalgia zero. Per scoprire, inventare, imparare, il tempo giusto è questo. Non quando i computer, per parafrasare il [filmetto](http://www.comingsoon.it/film/quando-le-donne-avevano-la-coda/11040/scheda/), avevano la coda.