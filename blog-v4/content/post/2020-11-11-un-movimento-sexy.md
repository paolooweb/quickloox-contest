---
title: "Un movimento sexy"
date: 2020-11-11
comments: true
tags: [Gruber, Daring, Fireball, Windows]
---
Poter scegliere tra più alternative software è indubitabilmente una buona cosa. Quando però si aggiungere il software (mal)concepito come multipiattaforma, il contrappasso è che l’interfaccia umana va a farsi friggere. Saltano le regole dell’interfaccia utente, il comportamento degli elementi dell’interfaccia è affidato all’estro del primo programmatore che passa (indizio: molto spesso, un eccellente programmatore è uno User Experience designer di valore zero), sullo schermo è cacofonia comunicativa.

Su Windows l’attenzione a una esperienza coerente è sempre stata scarsa. Mac ha fatto la rivoluzione proprio perché improvvisamente esistevano regole che governavano il funzionamento generale di qualunque programma.

Oggi ti ritrovi sullo schermo un programma open source disegnato per Linux da un progettista estemporaneo, un software Windows virtualizzato con Parallels, una webapp, un programma basato sul framework [Qt](https://www.qt.io), un altro su [Electron](https://www.electronjs.org) (il peggio del peggio) e poi il design [Material](https://material.io/design) di Google, quello di Microsoft e persino quello di Apple per le app iPad che girano anche su Mac, [Catalyst](https://developer.apple.com/mac-catalyst/), ancora acerbo per usare un eufemismo.

Il software, così, diventa una fonte di stress.

Nasce però un interesse rinnovato per i programmi Mac degni di questo nome anche nell’usabilità. Per esempio, Sketch ne ha fatto un punto di orgoglio tanto da creare una pagina di celebrazione dei loro dieci anni in perfetto stile [Macintosh 1984](https://www.sketch.com/blog/2020/10/26/part-of-your-world-why-we-re-proud-to-build-a-truly-native-mac-app/). È un piacere per gli occhi, per la testa, per il cuore.

John Gruber ha scritto del [possibile Rinascimento delle app native per Mac](https://daringfireball.net/2020/11/sketch_mac_app_mac_apps) con la citazione proprio di Sketch ma anche di Nova, il nuovo editor di testo Panic che promette meraviglie di velocità e comodità e si qualifica orgogliosamente [come una Mac-app Mac app](https://twitter.com/panic/status/1306292904063766529). Una *app per Mac come si deve* (evito il ridicolo di una traduzione letterale).

<blockquote class="twitter-tweet"><p lang="en" dir="ltr">🌌 Nova is here! Our brand new code editor. Packed with features. Built for modern workflows. Lightning fast. Stylish. With a robust extensions architecture.<br><br>And, best of all, it&#39;s an extremely Mac-app Mac app.<br><br>We hope it makes your work wonderful.<a href="https://t.co/8BPXCadvnE">https://t.co/8BPXCadvnE</a> <a href="https://t.co/izz5FP424L">pic.twitter.com/izz5FP424L</a></p>&mdash; Panic (@panic) <a href="https://twitter.com/panic/status/1306292904063766529?ref_src=twsrc%5Etfw">September 16, 2020</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

Per non parlare naturalmente di [Bare Bones Software](https://www.barebones.com), che dal secolo scorso continua a fare di BBEdit l’editor più completo e flessibile, con una interfaccia molto spartana e tuttavia Mac fino al midollo.

Un ritorno ad app per Mac capaci di offrire una esperienza rispettosa delle regole e della realtà quotidiana dell’utilizzatore sarebbe più che utile; un’oasi di progresso nel deserto del minimo comune denominatore multipiattaforma che anonimizza le app, le mutila di opzioni che farebbero la differenza (per esempio AppleScript) e umilia programmatori talentuosi nel piegarli ai diktat discutibili del framework di tendenza per la settimana in corso.

Tifo per un movimento che riporti su Mac ondate di applicazioni attraenti e belle da guardare.