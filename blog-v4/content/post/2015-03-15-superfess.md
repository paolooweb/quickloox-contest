---
title: "Superfess"
date: 2015-03-16
comments: true
tags: [Lenovo, Microsoft, Windows, Superfish, malware]
---
Per capire la genesi di Superfish, rimando a [tre settimane fa](https://macintelligence.org/posts/2015-02-22-gatti-e-volpi/). Lo sviluppo interessante è che Microsoft ha bloggato in modo molto dettagliato e interessante sull’eliminazione di Superfish dai computer in vendita.<!--more-->

Un bel successo: nel giro di una quindicina di giorni sono riusciti ad avere [meno di mille computer al giorno infetti da Superfish](http://blogs.technet.com/b/mmpc/archive/2015/03/10/msrt-march-superfish-cleanup.aspx).

Provo a ricapitolare: Microsoft fa un sistema operativo con dentro buchi che permettono a malintenzionati di minacciare la sicurezza di chi li utilizza. Lenovo vende macchine con il sistema operativo Microsoft e un software che sfrutta questi buchi per pregiudicare ulteriormente la sicurezza del computer, allo scopo di raccogliere incassi da pubblicità.

Dopo di che Microsoft e Lenovo lavorano, l’una per eliminare la minaccia permessa dai difetti di fabbricazione del suo prodotto, l’altra per eliminare la minaccia da essa stessa volontariamente inserita nei computer dei poveracci che abboccano al *costa-meno*.

Massimo rispetto per chi è costretto a usare un PC. Quelli che possono scegliere e specialmente scelgono Lenovo dopo questi fatti di cronaca, sempre con rispetto, sono anche un po’ superfessi però.