---
title: "Quattro o cinque fa dieci"
date: 2021-10-22T00:37:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [Dr. Drang, Comandi rapidi, Shortcuts, Python, Html, LaTeX, AppleScript] 
---
Voto perfetto per la leggerezza con cui Dr. Drang affronta un problema che solo apparentemente sembra ozioso – [quanti giovedì ci sono in un mese dato?](https://leancrew.com/all-this/2021/10/how-many-thursdays/) – e, per quanto di portata ridotta, riesce a ispirare un lavoro non banale con i Comandi rapidi.

Poi fa anche invidia perché, come se niente fosse, nei suoi ultimi post inanella appunto Comandi rapidi, [LaTeX](https://leancrew.com/all-this/2021/10/semiautomated-latex-tables/), [AppleScript](https://leancrew.com/all-this/2021/10/under-the-table/), [Python](https://leancrew.com/all-this/2021/10/data-cleaning-without-boredom/) e pare quasi che lo faccia apposta.

(Il numero dei giovedì in un mese, per convenzione, corrisponde al numero di settimane in un mese e può avere una effettiva importanza in certe questioni amministrativo-burocratiche).

*Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile.*