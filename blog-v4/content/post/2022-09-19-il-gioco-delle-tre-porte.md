---
title: "Il gioco delle tre porte"
date: 2022-09-19T02:03:23+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [OBS, Open Broadcaster Software, OBS Studio, M1, Mac mini]
---
Una delle cose belle della vita nel *computing* sono le app disponibili su tutte le piattaforme. Una delle cose brutte sono le app multipiattaforma che si accontentano del minimo comune denominatore e offrono giusto giusto sé stesse invece di valorizzare la piattaforma su cui lavorano.

Ogni tre volte che apro [Obs Studio](https://obsproject.com) succede qualcosa che mi cala nella prima parte della frase e mi risparmia la seconda. Obs gira su qualsiasi computer desktop e però ogni volta la mia esperienza è un po’ più vicina a quella di Mac.

La penultima volta che mi è successo, un aggiornamento ha portato la compatibilità con M1. Più prestazioni, meno carico sulla macchina (che certo non mi aiuterà questo inverno a risparmiare sul gas, visto che al massimo la si può sentire appena tiepida, ma è un’altra questione).

Questa volta, più sottile e più gradito. Breve premessa: uso Mac mini come hub per caricare un po’ tutti i device in dotazione e tutte le porte sono occupate.

Succede che, a volte, mi sia comodo caricare iPhone con un cavo ovviamente Lightning sul suo lato, connesso però a una porta USB-C su Mac mini.

Obs Studio trasforma iPhone nella webcam di Mac mini, con i vantaggi di uno studio di regia: mi ha fatto comodo nel tempo zoomare, esporre cartelli, generare sottopancia e entrare in videoconferenza con due webcam (l’altra su iPad). Cose così.

L’aggiornamento precedente di Obs Studio riconosceva il segnale in arrivo da iPhone solo se il collegamento via cavo avveniva via Usb.

Scomodità da poco, ma pur sempre scomodità: la prima volta ci ho messo qualche istante a capire che iPhone non poteva essere collegato alla porta Usb-C e certo c’è voluto poco a cambiare la configurazione. Noioso, comunque.

Oggi mi sono reso conto solo *dopo* che iPhone era collegato appunto alla presa Usb-C e tutto ha funzionato regolarmente.

Praticamente un [Monty Hall](https://statisticsbyjim.com/fun/monty-hall-problem/) dove si vince qualsiasi porta si apra.

Un software che continua a migliorare e sfruttare meglio i vantaggi della piattaforma dove lavoro.

Ed è software libero.