---
title: "Incontri ravvicinati"
date: 2021-07-09T00:09:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [Drm, Dvd, streaming, Torrent] 
---
Come vorrei che *con lo streaming i contenuti non sono più tuoi* incontrasse *ho un hard disk pieno di film scaricati via [Torrent](https://www.bittorrent.com)*.

Quanto mi piacerebbe vedere *se domani succede qualcosa tutti i contenuti con Drm diventeranno inutilizzabili* incontrare suo padre quando diceva *se domani sparisce l’energia elettrica la gente abituata alla calcolatrice non saprà fare i conti a mente*.

Il massimo sarebbe l’incontro tra *il problema dei supporti digitali è che non durano nel tempo* e *orgoglioso della mia collezione di Dvd*.

Sì, perché a sentirli hanno tutti ragione, contemporaneamente. Spesso sono anche la stessa persona.

*Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile.*