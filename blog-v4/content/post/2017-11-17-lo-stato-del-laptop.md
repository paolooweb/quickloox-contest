---
title: "Lo stato del laptop"
date: 2017-11-17
comments: true
tags: [Mac, MacBook, Pro, Gartner, TrendForce]
---
Se il meteo funzionasse come gli analisti che pretendono di prevedere l’andamento dei mercati tecnologici, sarebbero tutti a casa o quasi.<!--more-->

A casa starebbero certamente [Gartner e Idc](https://www.technightowl.com/2017/11/apples-financials-expose-flawed-mac-sales-estimates/). Secondo i primi, le vendite di Mac nel trimestre estivo sarebbero state (anno su anno) in ribasso del 5,6 percento, 4,61 milioni di unità. I secondi puntavano al rialzo dello 0,3 percento, cioè 4,9 milioni di macchine.

Apple [ha dichiarato](https://www.apple.com/newsroom/pdfs/fy17-q4/Q4FY17DataSummary.pdf) 5,39 milioni di Mac venduti, il dieci percento in più dell’anno prima nello stesso periodo.

Dovrebbe essere una faccenda grave. C’è gente in azienda che prepara i budget, programma strategie, effettua scelte anche impegnative in base a quello che scrive Gartner. Non è tanto l’errore, ma la sua dimensione a essere macroscopica e qualcuno dovrebbe essere responsabile per i danni, nel breve e nel lungo, che si provocano. Invece nessuno paga, mai, e questo mi lascia sempre sconcertato.

Starebbero a casa quelli di TrendForce, che assegnano ai Mac [il 10,4 percento del mercato portatile globale](https://www.appleworld.today/blog/2017/11/15/apples-mac-now-has-104-of-the-global-laptop-market)? Non saprei proprio. L’andazzo è tale che non esiste alcuna pietra di paragone affidabile. A questo punto potrebbe essere il nove, l’undici. Ma anche il cinque o il venti, non ci sarebbe da stupirsi.

I Mac vanno bene perché si vendono più Mac e questa è un’evidenza. Il resto è tutto un grande sparare alla Luna con il fucile a pallini e vai a sapere dove cadono. Come stiano i laptop nel loro complesso non lo sapremo mai (più).