---
title: "La valle della tranquillità"
date: 2020-04-13
comments: true
tags: [Monument, Valley, Ida, Ro, Escher, Lidia, Nive]
---
Nel giro di poche ore, spalmate su altrettanti giorni, ho finito [Monument Valley II](https://apps.apple.com/it/app/monument-valley-2/id1187265767?l=en) assieme alla primogenita di 5,67 anni.

Avevo sentito dire bene del gioco, peraltro pluripremiato, e l’esperienza è stata molto soddisfacente, tanto per il *sense of wonder* di una bimba qunto per la curiosità dell’adulto.

Una trama esilissima è sufficiente a tenere insieme un’avventura fatta per la vista e per l’udito (l’ascolto in cuffia o su buone casse è raccomandatissimo). L’andamento del gioco è solenne ma non ampolloso, lento il giusto, mai stucchevole e sempre capace di proporre qualcosa di sottilmente inaspettato.

La parte che più mi piace dal punto di vista del design è la capacità di proporre enigmi sempre nuovi a partire da un unico semplicissimo spunto: le prospettive illusorie che abbiamo conosciuto con [Maurits Escher](https://mcescher.com/).

Il meccanismo è sempre quello, però la traduzione in pratica è continuamente diversa e il gioco mai risulta monotono. La difficoltà è media e in generale uno studio attento dello scenario, magari con qualche test, porta rapidamente alla soluzione. Mia figlia da sola non sarebbe mai arrivata in fondo e allo stesso tempo non ci siamo mai sentiti perduti davanti a uno schema troppo intricato. L’enigma è un pretesto per apprezzare la cura della grafica, gli effetti sonori, la grazia dell’impianto della storia.

Arrivati in fondo al gioco, Lidia ha insistito perché ci cimentassimo anche in [Monument Valley I](https://apps.apple.com/it/app/monument-valley/id728293409?l=en). Va notato che Nive, 2,35 anni, ci ha seguito docilmente nel giocare, capace di assistere senza voler intervenire, anche lei curiosa degli effetti grafici. Lo dico perché ha carattere impetuoso e normalmente vuole mettere mano in tutto quello che fa la sorella maggiore, solo che stavolta per qualche motivo è stata disposta a guardare senza toccare, pur di condividere l’esperienza.

Non si arriva alla fine di Monument Valley II con il senso di avere terminato un’avventura epica di cui ci si è sudati l’epilogo. Il cammino è tranquillo, rilassante più che emozionante, alimentato comunque dalla curiosità. Al termine, invece del sollievo di chi ci è riuscito, si prova più voglia di avere ancora qualche livello in più.

In questo periodo entrambi i giochi dovrebbero essere a prezzo ridotto e valgono assolutamente la pena.
