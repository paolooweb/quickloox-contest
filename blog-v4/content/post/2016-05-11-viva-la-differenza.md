---
title: "Viva la differenza"
date: 2016-05-11
comments: true
tags: [Doblerto, iPad, MacBook, Pro]
---
Sono dolentissimo per la perdita del MacBook Pro 17” sofferta da [Doblerto](https://twitter.com/Alfassassina). Che computer così non ne facciano più, purtroppo, non è un modo di dire e il mio muletto, classe febbraio 2009, lascerà certamente un vuoto incolmabile.<!--more-->

Però va ammesso che gran parte di quel vuoto, grazie ai progressi degli ultimi anni, in fondo è colmabilissimo. E invito a leggere in merito [il suo racconto](http://alfassassina.tumblr.com/post/143041710289/bello-grosso-è-meglio) di come sia passato da un MacBook Pro 17” a un iPad Pro, con soddisfazione.

C’è anche una recensione di iPad Pro in omaggio, storia di vita vera e non cronaca di una mezza giornata trascorsa a smanettare.

Non è per tutti, si dirà. Certo, e aggiungo: sarebbe anche ovvio. La grande novità di questi anni è proprio che ognuno di noi ha esigenze di *computing* differenti, che possono essere soddisfatte senza scandalo da apparecchi diversi. Leggere per credere.