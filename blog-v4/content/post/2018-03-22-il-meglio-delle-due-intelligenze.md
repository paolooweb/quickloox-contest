---
title: "Il meglio delle due intelligenze"
date: 2018-03-22
comments: true
tags: [Apple, Ibm, iPhone, Watson]
---
Molto intrigante l’annuncio dell’[integrazione tra Core ML e Watson](https://developer.apple.com/ibm/) di Apple e Ibm, perché aggiunge una dimensione nuova a quella che chiamerò intelligenza artificiale per comodità, anche se trattasi di [attività di livello inferiore](https://macintelligence.org/posts/2018-03-17-meglio-al-naturale/).<!--more-->

[Core ML](https://developer.apple.com/machine-learning/) è l’architettura di Apple che permette di abilitare velocemente le *app* a compiti di *machine learning* e [Watson](https://macintelligence.org/posts/2016-11-09-umani-e-algoritmi/) è il sistema intelligente di Ibm già famoso per avere sconfitto i campioni umani del telequiz *Jeopardy*.

Core ML risiede a bordo di ogni macchina Apple e Watson invece [vive dentro novanta server Power750](https://www.google.it/url?sa=t&rct=j&q=&esrc=s&source=web&cd=4&ved=0ahUKEwi47bLL0v3ZAhVIDuwKHa8cCdAQFghCMAM&url=https%3A%2F%2Fshare.confex.com%2Fshare%2F117%2Fwebprogram%2FHandout%2FSession9577%2FWatsonHardwareAndSoftware.pdf&usg=AOvVaw09kwcPBhVXxM-owCOutFYd) (questo link scarica un Pdf).

La parte intrigante arriva ora: grazie all’annuncio, un iPhone – per dire – può avvantaggiarsi delle capacità di Watson nel momento in cui ha la connessione e la possibilità. Watson così addestra Core ML, che poi sarà capace di comportarsi meglio nel *machine learning* anche se in locale e senza connessione di rete.

In pratica i due sistemi di apprendimento compensano reciprocamente le debolezze e approfittano dei rispettivi punti di forza.

E iPhone X ha a bordo un chip specializzato in reti neurali, alla base del funzionamento di questi sistemi.

Ancora una volta Apple (con Ibm) tira fuori un miglioramento che sembra galleggiare nel vuoto e invece verrà declinato in decine e decine di soluzioni diverse, di cui neanche i progettisti immaginavano la fattibilità al momento di progettare Core ML. Grazie a due intelligenze che si compenetrano e tirano fuori il meglio in ogni circostanza.