---
title: "Il futuro è nella magia"
date: 2013-11-09
comments: true
tags: [iPad]
---
Di ritorno da una presentazione tenuta a [Valperga](http://www.comune.valperga.to.it), nel Canavese in provincia di Torino, orgogliosamente (io) invitato dal Consorzio interaziendale canavesano per la formazione professionale o più semplicemente [C.IA.C](http://www.ciacformazione.it).<!--more-->

Tema, il *futuro posteriore*. Ovvero, crediamo di corrergli dietro quando invece ci insegue e chiede solo che lo si lasci raggiungerci. Da molto tempo non si inventa nulla di veramente nuovo, ma si matura, si miniaturizza e si rende affascinante qualcosa che già c’è. Oggi non ci basta più la tecnologia, ma vogliamo che sia nascosta e che crei l’effetto magico di cui parlava Steve Jobs al momento di annunciare iPad.

È la chiave per comprendere il presente e il futuro che aspetta i ragazzi premiati nell’occasione con l’attestato di frequenza dei corsi e borse di studio per i più meritevoli.

È un raro privilegio quello di riunirsi in un grande cinema di un piccolo paese assieme ai rappresentanti e agli abitanti di un territorio. Impari a conoscere gli uni e gli altri, a capire le dinamiche di tessuto produttivo che sui giornali non appaiono eppure sono decisive per tante famiglie e tante aziende.

Ho mostrato che cosa si possa combinare ragionando da *makers*, da persone che sanno un pochino di software e altrettanto di hardware, grazie alle schede tuttofare come [Arduino](http://arduino.cc), alle stampanti 3D e alla creatività di chi ha voglia di tentare di cambiare e magari anche cambiarsi la vita.

Ho chiesto alle amministrazioni di portare la banda larga ovunque, perché è il primo requisito per fare rinascere un distretto industriale, e di semplificare in luogo di complicare, lavorare perché sia possibile concedere permessi invece di farlo per applicare divieti. La competitività oggi si può ottenere solo perseguendo eccellenza tecnologica ed efficienza automatizzata dove è necessaria. La nostra burocrazia è tremendamente indietro e le nostre aziende fanno doppia fatica per fare la loro parte.

Felice di reincontrare gente schietta e piacevole, persone impegnate e pratiche che guardano con ottimismo ai prossimi giorni. Ce n’è bisogno, della gente e dell’ottimismo, e hanno tutto il sostegno che posso dare nel mio piccolo.

Ho lavorato alla presentazione su iPad terza generazione e su MacBook Pro 17” inizio 2009, con il nuovo Keynote e sincronizzazione iCloud. Impressione vaga e non certificata, la sincronizzazione suddetta si è velocizzata alquanto. A causa di qualche filmato tratto da YouTube nella risoluzione più alta che il mio iPad riesce a trattare, il file complessivo è di circa sessanta megabyte.

Il nuovo Keynote è molto semplificato e in qualche situazione ai limiti del lecito. In compenso mi sono trovato molto bene con gli automatismi di ridimensionamento delle immagini. La velocità operativa è superiore a prima (e ti credo, bisognerà aspettarlo al varco con l’accumularsi delle funzioni). Il canale alfa istantaneo è facilissimo da usare, quasi un gioco, ma va inteso per interventi volanti, perché il risultato è a volte approssimativo.

Gran bella, magica, giornata e un grosso grazie a tutti. Se ne avrò la possibilità tornerò molto volentieri.