---
title: "(Appena) nato libero"
date: 2022-12-16T01:52:26+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [macOS, Ventura 13.1, iOS 16.2, iPadOS 16.2, Freeform]
---
Mac mi ha proposto l’installazione di Ventura 13.1 e l’occasione ha generato una eccezione alla mia regola di lasciare che gli aggiornamenti si installino quando credono; ho infatti richiesto manualmente iOS e iPadOS 16.2.

La ragione era che, con gli aggiornamenti fatti, potevo usare [Freeform](https://www.apple.com/newsroom/2022/12/apple-launches-freeform-a-powerful-new-app-designed-for-creative-collaboration/), per il quale nutro grandi aspettative e curiosità. Sono da tempo utente convinto di [Miro](https://miro.com/) e tengo d’occhio gli sviluppi di [Penpot](https://penpot.app): nella collaborazione a mano libera si trovano opportunità di quelle che ci sono state promesse da sempre e che, diciamolo eufemisticamente, da tempo abbiamo smesso di trattenere in fiato in attesa che diventassero vere.

Il debutto è sicuramente buono. La app (app nativa, non palla di JavaScript dentro il browser) è veloce e sicura, il feeling ottimo. Non ho ancora provato la collaborazione in modo serio: sarà un test molto importante.

Nel frattempo però ci si può concentrare sui difettucci alla nascita. Certamente nessuno si aspettava *tutto* dal giorno uno. Al tempo stesso, ora ci si chiede quanto arriverà, di sviluppo, e quando. Gli elementi principali ci sono tutti, l’essenziale è pronto; sono stati cuciti assieme pezzi di software presenti in varie parti dell’ecosistema che qualcuno ha capito essere perfetti per disegnare la cornice di una applicazione di collaborazione a mano libera. Niente di male, anzi, saggio utilizzo e riutilizzo della base di codice. La pulizia di Freeform è rasserenante.

Ora ci vogliono ragioni per rinunciare a Miro o comunque contemplare seriamente un affiancamento di Freeform ad altri strumenti. Le primissime cose che mi sono segnato:

- su Mac manca il disegno a mano libera. Su iPad e iPhone era vitale e quindi c’è, su Mac no, perché non c’è su iWork (prima si parlava di componenti riutilizzati). Però ci vuole, anche va trackpad o via mouse, è proprio una mancanza;
- si possono importare file grafici, per esempio; ma sui documenti la questione è più complicata. Un documento Pages è arrivato come file Zip, impossibile da visualizzare o decomprimere, di fatto inutile. Miro su questo è molto più avanti;
- una libreria non solo di elementi grafici, ma anche di strutture grafiche, come mappe mentali, diagrammi di flusso, griglie sarebbe estremamente apprezzata. Su altri programmi generare, per dire, una mappa mentale è molto veloce e intuitivo dopo avere interiorizzato due-regole-due. Su Freeform me la dovrei disegnare a mano, una faticaccia;
- sento tantissimo la mancanza di strumenti evoluti di maneggio del testo, come la sua rotazione o la possibilità di disporlo lungo una curva. Mi piacerebbe avere strumenti per distorcerlo e creare effetti speciali; in questo sono io a esagerare, non il programma ad avere una lacuna. Fare ruotare il testo, tuttavia, nell’economia di un programma di collaborazione è una cosa vicina al basilare.

Quindi: Freeform è appena nato. Ha o potrebbe avere un grande futuro; per il momento, in quanto minore, non può lavorare come un adulto, gli mancano esperienza e strumenti. Va sicuramente tenuto in considerazione e seguito con attenzione negli aggiornamenti. Basterebbe poco per farlo diventare assai competitivo, anche in virtù del prezzo.