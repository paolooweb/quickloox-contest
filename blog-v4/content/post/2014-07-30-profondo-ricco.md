---
title: "Profondo ricco"
date: 2014-07-30
comments: true
tags: [Terminale, diskutil, UtilityDisco]
---
Ho un nuovo esempio da portare nel momento in cui si accende la discussione che pone la facilità dell’interfaccia utente contro la potenza criptica del Terminale.<!--more-->

Ultimamente ho maneggiato immagini disco e formattazioni in maniera massiccia e mi sono un po’ stancato della macchinosità di Utility Disco e della sua interfaccia.

Armato di pazienza, ho dato il giusto tempo alla lettura del comando `man diskutil`.

Contrariamente a molti altri è un `man` leggibile e completo di esempi veri. Profondità e ricchezza, in pochi minuti di lettura. Ok, pochissime decine di minuti.

A dirla tutta, non so se e quanto userò ancora Utility Disco al posto di `diskutil`. Forse quando mi sarò dimenticato dei comandi. Certo non sarà stata una scelta volontaria.