---
title: "Dura lex sed Moore"
date: 2010-11-01
comments: true
tags: [Moore, Intel, Krzanich]
---
*Cnet* ha pubblicato una <a href="http://news.cnet.com/8301-13924_3-20020078-64.html?part=rss&amp;subj=news&amp;tag=2547-1_3-0-20" target="_blank">intervista</a> moderatamente interessante a Brian Krzanich, *senior vice president and general manager for manufacturing and supply chain* di Intel.<!--more-->

A parte la promessa di arrivare a circuiti da 13 nanometri (e anche pi&#249; sottili) entro il 2013, c&#8217;&#232; una delle poche affermazioni corrette che si trovino in giro rispetto alla legge di Moore, che non riguarda la velocit&#224; dei processori &#8211; che poi sarebbe al massimo una frequenza &#8211; bens&#236; la densit&#224; dei transistor, tradotta di volta in volta in maggiori prestazioni, minor consumo oppure ingombro ridotto.

Si legge anche che nel fabbricare i processori di domani la pura prestazione conta fino a un certo punto rispetto all&#8217;integrazione sul silicio di controller, grafica e altri sottosistemi.

L&#8217;era dei gigahertz venduti un tanto al chilo &#232; davvero finita.