---
title: "Cuori di ghiaccio"
date: 2014-08-24
comments: true
tags: [iPhone, Lumia, Nokia, Samsung, Galaxy5Als, iPhone5s]
---
Contrariamente a [quanto ritenevo](https://macintelligence.org/posts/2014-08-15-docce-fredde/), vari personaggi pubblici italiani hanno ritenuto di sottoporsi alla [secchiata di acqua ghiacciata](http://www.alsa.org/news/archive/als-ice-bucket-challenge.html) contro la sclerosi laterale amiotrofica.<!--more-->

La cosa ha preso talmente piede che in Samsung hanno avuto un’idea geniale: fare partecipare [anche Galaxy S5 Als](https://www.youtube.com/watch?v=6w4Gqt-ljb4). Il quale, come d’uopo, al termine dell’esperienza ha sfidato iPhone 5s e un Lumia di Nokia.

Nel video di quindici secondi si nota come l’orologio della barra di sistema passi dalle 2:16 alle 2:50, mentre il *widget* sulla schermata principale rimanga fisso sulle 2:16.

Il particolare è importante, dato che Galaxy 5S Als è certificato per resistere all’immersione per trenta minuti alla profondità massima di un metro. L’idea dello *spot* era sfruttare la popolarità della secchiata per porsi come superiore rispetto a iPhone, che non ha la stessa certificazione (anche se può [resuscitare da un lago](https://macintelligence.org/posts/2014-08-20-tre-metri-sotto-il-pelo/)).

Ma i due orologi visibili nel filmato non corrispondono. O non è passata veramente mezz’ora, o c’è un fotomontaggio. In entrambi i casi c’è il trucco.

Così non si capisce se la sfida a iPhone riguardi la mobilitazione per una buona causa, oppure l’ennesima sparata pubblicitaria senza sostanza. Specialità nella quale Samsung appare imbattibile.