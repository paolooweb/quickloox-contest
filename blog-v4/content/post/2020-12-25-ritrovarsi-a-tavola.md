---
title: "Ritrovarsi a tavola"
date: 2020-12-25
comments: true
tags: [Natale, Jitsi]
---
È molto probabile che la famiglia allargata si ritroverà oggi almeno per qualche momento grazie a [Jitsi](https://meet.jit.si) e, considerato l’anno quasi trascorso, questo ne fa la mia app dell’anno a mani basse. Libera, liberante.

Natale è normalmente un momento ideale per giochi in rete locale, fisica o telematica, ma non quest’anno. Di giochi multiplayer ne ho suggeriti a tonnellate negli scorsi anni, Natale o non Natale; quest’anno trovo abbastanza controcorrente suggerire un gioco da tavolo… da giocare via rete. *Boardgame Arena* [ne ha numerosi](https://boardgamearena.com/gamelist). Se è gioco di ruolo, naturalmente, battere [Roll20](https://roll20.net) è impegnativo.

Per coordinarsi senza restare necessariamente vincolati al video, suggerisco [Discord](https://discord.com). Oppure [Mumble](https://www.mumble.info).

Serenità e carole per tutti.