---
title: "Marcatura al cubo"
date: 2023-08-02T12:32:31+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [OpenUsd, Universal Scene Description, HTML, Autodesk, Adobe, Pixar, Usd]
---
Insisto tanto su HTML perché è un linguaggio di *marcatura*. Vuole dire che lo uso per contrassegnare un pezzo di testo e spiegare che è un paragrafo o una tabella o un sottolineato. Significa che descrivo quel pezzo di testo e per conseguenza ne conosco, ne capisco, ne imparo la natura all’interno del testo stesso. Quando apro un paragrafo con Word, vado a capo, due volte per giunta (niente mi aiuta a distinguere un paragrafo da un capoverso, in Word) e non succede niente, a parte creare due righe vuote. Niente spiega che cosa stia succedendo, niente viene descritto, niente viene imparato.

I non convinti possono trasferirsi nel mondo delle tre dimensioni, dove è notizia che [Pixar, Adobe, Apple, Autodesk e Nvidia formano un’alleanza attorno a OpenUsd](https://www.apple.com/newsroom/2023/08/pixar-adobe-apple-autodesk-and-nvidia-form-alliance-for-openusd/).

Usd è *Universal Scene Description*, standard inventato da Pixar per descrivere le scene 3D. OpenUsd è l’iniziativa per farlo diventare davvero universale, sotto l’egida di Linux Foundation: niente cose nascoste o proprietarie, tutto alla luce de sole e totalmente a disposizione.

Lato commercial-industriale: guarda caso, Apple è impegnata a lanciare Vision Pro, dove il 3D è un elemento naturale. Notevole che dall’elenco dei partecipanti manchi Facebook, che avrebbe puntato molto sul metaverso, ma forse vuole farlo a suo modo. Se ci sarà un braccio di ferro, la cosa si farà interessante.

Ma mi interessa di più il significato fondamentale della storia. OpenUsd – non lo dico io, ma un pezzo grosso di una delle società coinvolte – *è per il 3D quello che HTML è per il 2D*.

Per poter collaborare, partire da un terreno comune, dobbiamo partire da un modo condiviso di chiamare le cose, descriverle, contrassegnarle. OpenUsd serve a questo; poi Adobe, per dire, permette di manipolare ad alto livello i documenti OpenUsd con le proprie applicazioni, e così tutte le altre. Per imparare che cosa è il 3D, da che cosa è composto, come si organizza una scena, non si passa da AutoCad; si passa da OpenUsd. Dalla descrizione. Con AutoCad si produce, quando sei già diventato un professionista. AutoCad non serve a imparare, serve a produrre.

Per imparare, si descrivono le cose. Su un computer, si usano linguaggi di marcatura. Le applicazioni servono a produrre, non a imparare, e devono stare lontane il più possibile da chi deve imparare.