---
title: "La californiana fenice"
date: 2023-05-31T15:48:28+01:00
draft: false
toc: false
comments: true
categories: [Software, Hardware, Internet]
tags: [macOS, realityOS, WWDC, iOS, Mac Pro, M3]
---
Sta per iniziare il mese di [WWDC](https://developer.apple.com/wwdc23/) e mi godo il perfetto silenzio su quello che verrà presentato durante il *keynote*.

Intendiamoci: se ne parla un sacco. Ma la chiacchiera ricorda il detto sull’araba fenice: *che ci sia ciascun lo dice, dove sia nessun lo sa*. Certo, il visore per la realtà virtuale; chissà, forse Mac Pro; o magari un MacBook Pro. Oppure M3, o un nuovo watch. E poi naturalmente i sistemi operativi nuova versione, gli sviluppatori sono lì per quello.

E niente, neanche un particolare. Circolano in rete rendering 3D di maschere da snorkeling che fanno notizia perché sono *concept* creati da un perfetto sconosciuto che andrà avanti a esserlo. *Non sapevo che cosa fare in Blender e invece dei gattini ho creato un concept di visore, perché so usare Blender*. Informazione allo stato puro, proprio.

Arriverà M3? Oppure qualcosa di completamente diverso? Nessuno lo sa. Meno di tutti, chi scrive articoli in proposito. Saltiamoli serenamente e aspettiamo Tim, che lui lo sa.

Un giorno avremo un mondo migliore e una civiltà evoluta, dove i giornalisti scrivono di quello che è noto e gli scrittori di fantascienza azzardano il futuro. Attualmente è il contrario e abbiamo ancora un po’ di strada da fare. A questo proposito, metti mai che ci presentino Apple Car.