---
title: "Input di emergenza"
date: 2013-08-28
comments: true
tags: [Mac]
---
Vorrei che *Comando-Maiuscole-T* funzionasse anche in Safari. Invece si trova solo in Chrome e Firefox.<!--more-->

<blockquote class="twitter-tweet"><p>Command + shift + t reopens the last tab you closed in Chrome (up to 10 tabs). <a href="https://twitter.com/search?q=%23keyboardshortcutoftheweek&amp;src=hash">#keyboardshortcutoftheweek</a></p>&mdash; Melia Robinson (@meliarobin) <a href="https://twitter.com/meliarobin/statuses/370617786260680704">August 22, 2013</a></blockquote>
<script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>

Riapre l’ultima scheda del *browser* che è stata chiusa e, magari, è stata chiusa distrattamente, per errore.

Mi sarebbe tornata utile almeno diciannove volte. Definirla *miracolosa* come [ha fatto Slashdot](http://tech.slashdot.org/story/13/08/26/0010239/the-greatest-keyboard-shortcut-ever) è esagerato. Comoda, però, sì.