---
title: "Gente che lavora"
date: 2017-01-15
comments: true
tags: [Milano, Princi, Mac]
---
Foto in arrivo da **Lorescuba**, scattata alle 13:30 di un giorno feriale recente a Milano, da [Princi](http://www.princi.it) in piazza XXV aprile, a quell’ora frequentatissimo dalle persone che lavorano in zona.<!--more-->

Le espressioni non sono propriamente gioiose ma ci sono anche giornate no. Piuttosto, escludendo che si tratti di pensionati, studenti perditempo, milionari in ozio o turisti, tenderei a definirli professionisti.

 ![Mac in uso in pausa pranzo da Princi a Milano](/images/princi-bis.jpg  "Mac in uso in pausa pranzo da Princi a Milano") 
