---
title: "Un bisogno terribile"
date: 2014-10-16
comments: true
tags: [Accomazzi, Apogeo, iBooksStore, Amazon, Kobo, OSX]
---
Interrompo mio malgrado la programmazione per un comunicato pubblicitario: l’editore Apogeo è contento dell’andamento iniziale della *Guida completa* a OS X Yosemite realizzata da [Luca Accomazzi](http://accomazzi.net) e dal sottoscritto.<!--more-->

Disponibile in preordine al prezzo promozionale di soli 9,99 euro contro i 19,99 che avrà una volta presentato ufficialmente Yosemite, si può recuperare su [iBooks Store](http://itunes.apple.com/it/book/isbn9788850317264?at=1l3vqXD), [Google Play](http://google.prf.hn/click/camref:110ltE/destination:https://play.google.com/store/books/details/Luca_Accomazzi_OS_X_10_10_Yosemite?id=Pq7LBAAAQBAJ), [La Feltrinelli/Kobo](http://www.lafeltrinelli.it/ebook/lucio-bragagnolo/os-x-1010-yosemite/9788850317264?utm_source=apogeo&utm_medium=cpa&utm_content=shopping&utm_campaign=link_testuale&zanpid=1957375118661841921), [Kobo e basta](http://store.kobobooks.com/it-IT/ebook/os-x-10-10-yosemite), [Amazon](http://www.amazon.it/OS-10-10-Yosemite-Guida-alluso-ebook/dp/B00OGKW55I/ref=sr_1_1?s=digital-text&ie=UTF8&qid=1413272021&sr=1-1&keywords=yosemite+accomazzi).

Il libro, tecnicamente quasi non ancora uscito, [è già numero uno](http://www.amazon.it/OS-10-10-Yosemite-Guida-alluso-ebook/dp/B00OGKW55I/ref=zg_bs_1344993031_1) in una delle tante classifiche dell’editoria digitale, effimere e però piacevoli da leggere in queste situazioni.

La verità è che stavo scrivendo un altro *post* e l’editore è arrivato con le notizie di cui sopra. Lui tiene molto alla loro diffusione e sono abbastanza in argomento con quanto andavo scrivendo.

Perché il *post* riguardava il bisogno terribile che ha l’informatica di divulgazione che vada oltre la risoluzione dei problemi e spieghi la parte sottostante delle interfacce, non in termini di approfondimento tecnico bensì di inquadramento e contestualizzazione. E questo è giustappunto il motivo che ci spinge anno dopo anno a riproporre il nostro libro in edizione riveduta e corretta per il nuovo sistema operativo.

Chiarisco: è difficile, molto difficile che un editore accetti una guida a un sistema operativo diversa dalla descrizione dei menu, dalla spiegazione di come si copia e come si cancella, dal racconto pedissequo dei vari pulsanti dell’ennesima finestra di dialogo. Con l’immancabile soluzione dei problemi: se la stampante non stampa, se il disco è danneggiato, se i permessi non permettono eccetera eccetera eccetera.

Il nostro libro contiene qua e là anche questo. Tuttavia le parti dove si è versato più sudore – a volte visibile e a volte meno – e che veramente giustificano un prezzo di acquisto sono quelle che *spiegano*. Illustrano le ragioni delle funzioni di sicurezza e *privacy*, il perché abbia senso effettuare un backup oppure che differenza vi sia tra questo o quel *filesystem*, quale sia lo scopo di una certa impostazione del firewall oppure che cosa stia dentro un *widget* di Dashboard.

C’è abbondanza di pesce sul mercato, per riutilizzare una metafora alquanto consunta, ma pochissimi che insegnino a pescare. Il risultato è un mondo di persone che cercano di continuo soluzioni ai problemi senza sapere niente di che cosa stia sotto o dietro ai problemi stessi. E anche quando abbiano risolto il problema 1, la mancata conoscenza di basi e concetti li espone inevitabilmente al problema 2, che arriverà. E da lì al problema 3 e a tutti gli altri.

Una vita di problemi e soprattutto di dipendenza da qualcuno o qualcosa con la soluzione in mano. Spesso difficile persino da proporre, perché senza le basi è difficile spiegare correttamente i sintomi di un malfunzionamento.

A prescindere dalla nostra *Guida Completa*, sarebbe auspicabile una divulgazione che insegnasse alle persone a pescare. Come funziona il sistema in generale, come sono fatte le preferenze, come scegliere una buona *password* e così via. Qualcosa che formasse le persone e le mettesse in grado di procedere in autonomia, affrontare serenamente gli ostacoli e imparare lungo il cammino, libere dalla schiavitù di dover chiedere aiuto nei modi più disparati una settimana sì e una pure.

In proposito avrei anche delle idee editoriali abbastanza ambiziose, che è difficile fare piacere agli editori in quanto richiederebbero tempo, impegno, coraggio, caparbietà. E contribuirebbero – almeno nelle intenzioni – a una cultura informatica diffusa più profonda e proficua, per tutti. Magari se ne riparla.