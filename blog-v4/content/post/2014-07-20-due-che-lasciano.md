---
title: "Due che lasciano"
date: 2014-07-21
comments: true
tags: [Cotton, Gruber, Campbell, Wagner]
---
La notizia dell’[ingresso di Sue Wagner](http://www.apple.com/it/pr/library/2014/07/17Sue-Wagner-Joins-Apple-s-Board-of-Directors.html) di BlackRock nel consiglio di amministrazione di Apple è una copertura; il vero rumore dovrebbe farlo l’addio di Bill Campbell, che aveva lavorato in Apple nel 1983 come Vicepresidente Marketing ed era il consigliere con maggiore anzianità nella storia.<!--more-->

Campbell era stato congedato dal consiglio dopo la cacciata di Steve Jobs ed era ritornato nel 1997, proprio da Jobs richiamato. Con il suo addio si chiude davvero un’epoca che ne ha abbracciate almeno tre diverse e ha segnato la storia in modo tale che ricordarlo è fin troppo retorico.

E qui va ricordato anche il pensionamento di Katie Cotton, che fino a maggio era stata responsabile delle relazioni di Apple con la stampa. John Gruber ha ricordato [su Daring Fireball un aneddoto](http://daringfireball.net/2014/05/katie_cotton) per cui salta su un aereo con trenta ore di preavviso su un evento importante e, trovandosi vicino Cotton una volta arrivato, lei dice *Sono lieto che tu ce l’abbia fatta. Come va il raffreddore?*. Lui risponde *come sai del raffreddore?* e un dirigente Apple vicino commenta *John, Katie sa tutto*.

Bill Campbell e Katie Cotton oggi sono due che lasciano. Hanno anche lasciato un gran segno e non ce ne accorgiamo, ma senza loro non sarebbe stata la stessa Apple.