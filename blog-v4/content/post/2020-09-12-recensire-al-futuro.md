---
title: "Recensire al futuro"
date: 2020-09-12
comments: true
tags: [Verge]
---
Una nuova recensione su *The Verge*. [Questo nuovo prodotto è sorprendentemente piccolo nelle dimensioni e nel prezzo](https://www.theverge.com/2020/9/10/21430298/microsoft-xbox-series-s-hands-on-photos-price).

Si accende un nuovo interesse. Addentriamoci nella nuova lettura.

>Ho passato la scorsa settimana ad armeggiare con una versione non funzionante del nuovo prodotto…

*Non funzionante*. Ecco. In che modo è utile una recensione di un prodotto non funzionante? Per esempio:

>La caratteristica che più attira l’attenzione del nuovo prodotto è il grande cerchio nero legato al raffreddamento e alle ventole. Mantiene il flusso dell’aria lateralmente se il prodotto è in piedi e in alto se il prodotto è adagiato orizzontalmente.

Sembrerebbe un foro di aerazione, ma ci deve essere dietro molta ricerca, se può fare cose come stare verticale se il prodotto è verticale e orizzontale se è orizzontale.

Prima di seguire il link, un pizzico di mistero: chi potrebbe avere realizzato questo nuovo ed entusiasmante prodotto, dotato nientemeno che di un foro di aerazione?

Un indizio: è la stessa azienda che di recente ha consegnato modelli di un altro nuovo prodotto a blogger e giornalisti. Che avevano possibilità di parlarne e mostrarlo, [a patto di non accenderlo](https://www.youtube.com/watch?v=DJyxwIGdl8Y&feature=youtu.be).

Un bel salto nel futuro del giornalismo tecnico. Mica come quei fessi di Apple, che come si vede hanno perso la strada dell’innovazione.

(*Come si vede* in senso molto lato).