---
title: "Pallone gonfiato"
date: 2016-11-11
comments: true
tags: [batteria, Sasha]
---
La foto è di una amica di **Sasha**, che gli ha spedito una immagine della sua tavoletta. Sì, quella è la batteria.

Però, rispetto a un iPad, vuoi mettere il risparmio?

 ![Batteria gonfiata](/images/batteria-pallone.jpeg  "Batteria gonfiata") 