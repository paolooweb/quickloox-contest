---
title: "Il pelo nello hashtag"
date: 2015-12-04
comments: true
tags: [Twitter]
---
Per la categoria *minuzie veramente microscopiche*, ho trovato un *bug* della *app* [Twitter](https://itunes.apple.com/it/app/twitter/id333903271?mt=8) veramente piccino e fastidioso: dopo avere digitato *#* e avere attivato il blocco maiuscole, esso si disattiva dopo la prima lettera.<!--more-->

Per scrivere uno *hashtag* di tutte maiuscole, in pratica, il disagio è assicurato.

Fatico anche da un punto di vista logico a capire come si possa introdurre un errore come questo. Il blocco maiuscole è il blocco maiuscole e non c’è ragione per cui un carattere particolare ne imponga la disattivazione.