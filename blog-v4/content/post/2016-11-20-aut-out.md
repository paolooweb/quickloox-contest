---
title: "Aut-Out"
date: 2016-11-20
comments: true
tags: [Soghoian, AppleScript, Automator, Swift]
---
Sono sicuro che c’è un nesso tra le due notizie che seguono.

La prima è che in Apple [non esiste più la posizione di Product Manager of Automation Technologies](http://dougscripts.com/itunes/2016/11/sal-sagohian/).

La seconda è che in occasione della Hour of Code, manifestazione americana che promuove l’insegnamento generalizzato della programmazione a partire dalle scuola primaria, [verranno tenute lezioni gratuite di Swift Playgrounds negli Apple Store](https://9to5mac.com/2016/11/17/apple-swift-playgrounds-hour-of-code-workshops/).

La mia posizione è assolutamente e totalmente allineata a [quella di Sal Soghoian](https://macosxautomation.com/about.html), l’ultimo a ricoprire la carica. Se fossi capace e competente come lui e fossi stato al suo posto, avrei scritto esattamente le stesse cose (o ci avrei provato).

D’altro canto, non posso ignorare quei ragazzini che fanno un giro in Apple Store e tornano a casa incuriositi per poi provare a pasticciare in un ambiente gratuito pronto su iPad e domani scoprirsi adulti autodidatti di Swift invece che di AppleScript.