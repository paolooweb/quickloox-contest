---
title: "Aspettando una (retro)illuminazione"
date: 2017-01-07
comments: true
tags: [Matias, Wireless, Aluminum, Keyboard]
---
Farei un serio pensiero sulla [Wireless Aluminum Keyboard](https://matias.store/products/copy-of-wireless-aluminum-keyboard-space-gray) di Matias, che dispone del *layout* (e di un <a href="http://idistribution.it/index.php/dmes/Prodotti/Dispositivi-bluetooth/Matias-Tastiera-Wireless/(m1)/char">distributore</a>) italiano. Solo che non è retroilluminata.<!--more-->

Da giugno sarà disponibile la [versione retroilluminata](https://matias.store/collections/slim-aluminum-keyboards-1/products/fk418btlsb), che si può preordinare. Solo che manca ancora il *layout* italiano.

Per ricevere la (retro)illuminazione, tocca attendere.