---
title: "Panta Server"
date: 2022-04-21T00:56:16+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [Mac OS X Server, macOS Server, Troughton-Smith, Steve Troughton-Smith]
---
Il modo migliore per celebrare l’uscita di scena definitiva di Mac OS X Server è probabilmente questo [tweet di Steve Troughton-Smith](https://twitter.com/stroughtonsmith/status/1517226158437371907).

<blockquote class="twitter-tweet"><p lang="en" dir="ltr">End of an era; Mac OS X Server *predates* Mac OS X, and was its own version of the operating system for over a decade (and the ‘last’ true version of NEXTSTEP). I used run it on a PowerMac G4 in a tiny corner of the house for no good reason <a href="https://t.co/0JfG6Q5r4Q">https://t.co/0JfG6Q5r4Q</a> <a href="https://t.co/uW7icg1Len">pic.twitter.com/uW7icg1Len</a></p>&mdash; Steve Troughton-Smith (@stroughtonsmith) <a href="https://twitter.com/stroughtonsmith/status/1517226158437371907?ref_src=twsrc%5Etfw">April 21, 2022</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

> Mac OS X Server **anticipa** Mac OS X ed è stato un sistema operativo autonomo per oltre un decennio (nonché l’ultima “vera” versione di NeXTSTEP). Lo facevo andare su un Power Mac G4 in un angolo di casa, per nessuna vera ragione.

È piuttosto interessante leggere la [pagina dedicata del supporto Apple](https://support.apple.com/en-us/HT208312) che mostra le sostituzioni possibili dei vari servizi offerti dal software. Alcuni sono migrati dentro macOS, per altri serve un appoggio esterno. macOS Server, giusto ricordarlo con il suo nome attuale, è stato comunque un oggetto complesso e importante nella storia recente di Apple.