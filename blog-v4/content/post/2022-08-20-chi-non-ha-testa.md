---
title: "Chi non ha testa"
date: 2022-08-20T15:01:34+01:00
draft: false
toc: false
comments: true
categories: [Internet, hardware, software]
tags: [Apple Watch, iPhone, emacs, napalm, iOS, nano, Tailscale]
---
È l’interazione che consuma energia. Cammino e ascolto musica, watch registra passi, iPhone eroga brani; io cammino e basta e alla fine la batteria consumata è irrisoria.

Non mi faccio preoccupazioni etiche sul consumo di energia necessario per produrre i miei dati; mi interrogo però su quali dati potrebbero interessare anche ad altri e quali solo a me.

Da quando uso [Tailscale](https://macintelligence.org/posts/2022-08-09-pr%C3%AAt-a-vpn/) aborro il desktop remoto. Ssh e terminale tutta la vita.

Non importa quanto lungomare io percorra; per iOS la distanza dal mio Mac via VPN resta uguale. Lo ribattezzerei ioS.

Devo fare una piccola modifica a un testo e nient’altro, ma preferisco aprire [emacs](https://macintelligence.org/search/?query=Emacs) invece di nano, [come facevo una volta](https://macintelligence.org/posts/2016-08-20-per-conciliare-il-sonno/). Guardo un po’ di tutorial, scopro una cosa nuova, faccio un esperimento. emacs è l’unico programma nel quale una cosa non si può fare unicamente perché non ho ancora scoperto come si fa.

Chi ha gambe non ha testa, cammina e prima di rientrare raccoglie amenità disordinate in un blog.

L’odore di emacs al mattino [ricorda quello del napalm](https://www.youtube.com/watch?v=MzQPTdDwtVk).

<iframe width="560" height="315" src="https://www.youtube.com/embed/MzQPTdDwtVk" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>