---
title: "Dettagli che si sentono"
date: 2023-08-08T01:45:39+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [Memo Vocali, Voice Memos, Stocks, MacLife]
---
Per il resto si tratta di una [raccolta di easter egg su macOS](https://www.macrumors.com/2023/08/07/10-hidden-easter-eggs-in-macos/) e però questa, se è vera, è davvero forte: l’icona dei memo vocali mostrerebbe il profilo sonoro corrispondente alla pronuncia della parola *Apple*.

Nella prova che ho fatto, il profilo non è proprio uguale ma segue quell’andamento. Vai a capire che inflessione ha, da chi è stato pronunciato eccetera. Credibile, comunque.

Nello stesso articolo si fa riferimento all’icona di Stocks affermando che riproduce il momento del sorpasso di Apple su Dell come capitalizzazione di mercato, avvenuto nel 2006, e mi sembra invece una cosa del tutto millantata. Il riferimento originale è un [articolo in tedesco di MacLife](https://www.maclife.de/news/ios-9-13-easter-eggs-denen-apple-liebe-detail-beweist-10078697.html?page=7#pagergalerie), dove manca qualsiasi straccio di prova. Verificarlo mi sembra aleatorio e velleitario, lo lascio a chi abbia voglia.

Rimane bellissimo poter frugare dentro macOS all’infinito e sapere che potrebbe sempre emergere un dettaglio a sorpresa.

*Gli aggiornamenti in agosto del blog potrebbero essere irregolari o infrequenti.*