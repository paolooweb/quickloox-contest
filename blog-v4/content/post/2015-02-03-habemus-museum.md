---
title: "Habemus Museum"
date: 2015-02-03
comments: true
tags: [AAA]
---
Ho appena controllato e sono lieto di constatare che All About Apple ha passato con successo la soglia minima di finanziamento per l’apertura della nuova sede del loro straordinario [museo Apple](http://www.eppela.com/ita/projects/1519/all-about-apple-museum-30), il più grande e completo del mondo.<!--more-->

Le nostre donazioni verranno effettivamente utilizzate e questo solleva l’umore; ringrazio i volonterosi che hanno letto qui l’appello a donare e hanno provveduto.

Non bisogna mollare l’osso, perché ora ci sono gli *stretch goal*, i traguardi supplementari; più si raccoglie più cose si fanno. Alla prossima soglia si raggiungerà la copertura Wi-Fi dei locali, poi la *app* per iOS e infine la *app* per Android. Se avanza un euro, All About Apple è un bel posto per investirlo. Essendo cultura e storia non tornano dividendi monetari, ma un mondo migliore.