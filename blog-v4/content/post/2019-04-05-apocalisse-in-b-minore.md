---
title: "Apocalisse in B minore"
date: 2019-04-05
comments: true
tags: [Samsung, Apple]
---
Lo scorso trimestre Apple ha avvisato il mercato che gli iPhone avrebbero reso meno del previsto e sembrava avesse ceduto una diga sulle montagne sopra Cupertino, se ve ne fossero.

Oggi, un trimestre dopo come ogni periferia che si rispetti, Samsung avvisa che i suoi profitti [caleranno di oltre la metà](https://www.engadget.com/2019/04/04/samsung-q1-2019-earnings-guidance/). Anche perché ha guadagnato meno dalla fornitura di schermi ad Apple, che ha venduto meno iPhone.

D’altronde quello che succede in serie A interessa molti e quello che accade in serie B, per quanto importante, ha rilevanza secondaria. La copertura del fatto si avvicina molto più alla fredda cronaca che alla passata descrizione da panico delle sventure che ci attendono. Copertura, aggiungo, anch’essa molto più puntuale su siti secondari.

Puoi avere un sacco di spettatori, anche più delle squadre in serie A, ma se giochi in serie B resti un gradino sotto.