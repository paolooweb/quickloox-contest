---
title: "Chiuso per stereotipi"
date: 2021-09-19T00:28:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [Apple, M1, Linux, Debian, Alyssa Rosenzweig] 
---
Ricordo come fosse ieri le letture su Apple che con il *system-on-chip* M1 chiudeva la piattaforma Mac e fondamentalmente eliminava la libertà di ciascuno di decidere che software fare girare sulla propria macchina.

Poi ho visto [che cosa ha fatto Alyssa Rosenzweig](https://twitter.com/alyssarzg/status/1432927311058194436), sviluppatrice Linux.

<blockquote class="twitter-tweet"><p lang="en" dir="ltr">Bare metal Apple M1 Debian at 4K@60.<br><br>Display driver development, day #7.<br><br>A million thanks to <a href="https://twitter.com/svenpeter42?ref_src=twsrc%5Etfw">@svenpeter42</a> for helping me tread water in the Linux kernel driver deep end and to <a href="https://twitter.com/marcan42?ref_src=twsrc%5Etfw">@marcan42</a> for reverse-engineering the Apple display controller. <a href="https://t.co/uuVtDXGTF2">pic.twitter.com/uuVtDXGTF2</a></p>&mdash; Alyssa Rosenzweig (@alyssarzg) <a href="https://twitter.com/alyssarzg/status/1432927311058194436?ref_src=twsrc%5Etfw">September 1, 2021</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

Che Linux girasse su M1 era solo questione di tempo. Che questi Mac siano più chiusi dei precedenti è solo uno stereotipo che si è ascoltato decine di volte e finisce sempre per essere smentito.

Con un pizzico di cattiveria, penso anche a quelli per cui la libertà è dovuta. Con tutti i limiti dell’argomento, più si è capaci di esercitare la libertà, più si è liberi. La sola capacità di aspettare Rosenzweig è certo libertà, ma di un altro valore.

*Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile.*