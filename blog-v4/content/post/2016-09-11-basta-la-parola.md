---
title: "Basta la parola"
date: 2016-09-11
comments: true
tags: [Schiller, keynote, coraggio, AirPods, iPhone]
---
La prima reazione dopo il [keynote](http://www.apple.com/it/apple-events/september-2016/) di presentazione di iPhone 7 è che Apple sta evolvendo, senza cambiare. Mi pare una cosa buona. Approfondirò nei prossimi giorni.<!--more-->

La cosa che mi ha colpito di più è una parola usata da Phil Schiller a proposito degli [AirPods](http://www.apple.com/it/airpods/), della scelta di reinventare gli auricolari *wireless* risolvendo i problemi e gli errori compiuti finora: *courage*. Coraggio.

Il punto è che si sarebbero venduti una carrettata di iPhone 7 anche senza gli AirPods. Si potevano fare degli auricolari come tutti gli altri, un po’ più bellini, un po’ più curati, un po’ più robusti e nessuno avrebbe detto niente. Invece gente seduta su montagne di miliardi, accusata di non innovare, rischia. Sì, perché gli AirPods potrebbero non piacere a nessuno e questo vorrebbe dire vendere qualche carrettata di iPhone 7 in meno. Oltre ad avere buttato molti soldi in ricerca e sviluppo, design, progettazione, produzione.

Schiller ha parlato del coraggio di percorrere nuove strade. Da molto tempo non si sentiva una cosa del genere in un *keynote*. Posso dirlo? Tra cinque anni Tim Cook si ritirerà. Forse il suo successore designato inizia a calarsi nel ruolo.