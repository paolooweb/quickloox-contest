---
title: "Materia ed energia"
date: 2015-07-04
comments: true
tags: [SFGate]
---
Si può capire da questo articolo del [San Francisco Gate](http://www.sfgate.com/education/article/S-F-board-sticks-with-school-assignment-system-6317632.php) che come mandare avanti le scuole nella Bay Area è oggetto di discussioni anche accese.<!--more-->

Peraltro il consiglio di amministrazione ha votato all’unanimità per introdurre la *computer science* come materia fondamentale per tutti gli indirizzi e tutti gli anni, al pari di lettura, scrittura, matematica, storia, scienze eccetera.

Dice che stanno correndo troppo. Dico che siamo noi a non correre abbastanza. Un attimo dopo averci pensato diventa ovvio, che l’informatica nel mondo di domani sarà importante quanto l’italiano, la matematica, le lingue straniere (il plurale è voluto). La scuola, se non ha le energie per questo, le deve trovare.

(Per stare in argomento, si vedono un paio di Mac utilizzati da membri del consiglio).