---
title: "L’elefante nascosto"
date: 2021-06-13T17:34:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [Apple, WWDC, DocC, Inside Macintosh] 
---
Una copertura davvero controcorrente di almeno un aspetto di WWDC? Il problema della documentazione, molto ben trattato da _The Eclectic Light Company_, per il cui autore rappresenta [L’elefante a WWDC](https://eclecticlight.co/2021/06/13/last-week-on-my-mac-the-elephant-at-wwdc/).

Non si parla della documentazione per l’utente finale, del mitico _manuale_, ma proprio di quella per gli sviluppatori. Apple sta realizzando un nuovo sistema per produrre documentazione, denominato DocC, che però sarà un contenitore e poco farà per risolvere il problema della carenza di buone descrizioni del funzionamento interno del software Apple.

Non è stato sempre così, anche senza risalire agli altrettanto mitici sei volumi di _Inside Macintosh_. Oggi lo sviluppo continuo, il moltiplicarsi dei sistemi e la complessità delle problematiche in gioco hanno reso sempre più complicato il lavoro di chi deve documentare e, si coglie, messo in evidenza il problema di una certa carenza di competenze, al contrario di quelle ingegneristiche e di sviluppo.

DocC è comunque una prima risposta al problema, di cui evidentemente Apple ha consapevolezza. Chiaro che, per conservare un primato nell’esperienza utente, non basti avere il miglior software possibile e che serva pure mettere gli sviluppatori indipendenti all’altezza di scrivere software superiore.

L’articolo cita brevemente anche esperienze indipendenti di documentazione, come il mitico (terza volta) pondini.org, oggi non più attivo, che illustrava ogni e qualunque aspetto di Time Machine.

Apple ha due problemi oggi, chiosa il post: bug e documentazione. Significativo è già che vengano esposti alla pari, anche se uno parrebbe più immediato e urgente dell’altro. Non è affatto così.

*Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile.*