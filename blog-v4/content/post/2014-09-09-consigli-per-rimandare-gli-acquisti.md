---
title: "Consigli per rimandare gli acquisti"
date: 2014-09-09
comments: true
tags: [Apple, Flint]
---
Mi pare inutile tentare di fissare un argomento il giorno in cui Apple tiene un [evento in diretta planetaria](http://www.apple.com/live/) [costruendo un secondo teatro davanti al teatro sede della presentazione](http://9to5mac.com/2014/08/28/apple-building-multi-story-structure-at-flint-center-ahead-of-iphone-event/).<!--more-->

Purtroppo sarò *on the road* e impossibilitato a seguire in tempo reale. Arriverò a un computer a cose fatte.

Mi lancio però in un pronostico, per fare il verso agli spazzaturai dei web: *quello che verrà annunciato non si potrà acquistare subito*.