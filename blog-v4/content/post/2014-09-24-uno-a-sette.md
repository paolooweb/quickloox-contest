---
title: "Uno a sette"
date: 2014-09-24
comments: true
tags: [Microsoft]
---
La frase più letta letta oggi giunge da un [articolo](http://www.cio.com/article/2687128/tablets/microsoft-redefines-experience.html) relativo alla bellezza della tavoletta Microsoft.<!--more-->

>Microsoft ha avuto il coraggio di ammettere che il mondo del computer è cambiato e che la loro precedente quota di mercato del 95 percento è diventata un 14 percento di tutti gli apparecchi nel mondo.

Domani mattina si apriranno i negozi e, ogni sette apparecchi informatici che si vendono, uno sarà Microsoft. *Uno su sette*.

C’è speranza.