---
title: "Il voodoo è fuori mercato"
date: 2015-06-16
comments: true
tags: [OSX, ElCapitan, permessi]
---
Anni passati a spiegare alle persone che i permessi di OS X vanno riparati in presenza di un problema ai permessi e che la loro riparazione a sproposito, in mancanza di altre idee faceva più male che bene.<!--more-->

Risposte tipo *Se Apple non avesse voluto che venisse usata la riparazione dei permessi, non avrebbero messo a disposizione un comando*. Logica ineccepibile, rispondevo: Apple mette a disposizione anche un comando per azzerare il disco passandoci sopra venti volte per non lasciare neanche l’ombra di un bit e stranamente non lo usi.

Quante mail simili a *Non mi si accende l’auto. Ho provato a riparare i permessi del Mac, ma niente*.

[Tutto è finito](https://developer.apple.com/library/prerelease/mac/releasenotes/General/rn-osx-10.11/index.html).

>I permessi dei file di sistema sono automaticamente protetti e vengono aggiornati durante gli aggiornamenti software. La funzione Ripara permessi non è più necessaria.

Cattiva Apple, che togli alla gente bisognosa uno strumento tanto utile quanto un ferro di cavallo e una treccia d’aglio.

Il voodoo è ufficialmente andato fuori mercato. Avanti la prossima superstizione.
