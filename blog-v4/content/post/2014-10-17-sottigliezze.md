---
title: "Sottigliezze"
date: 2014-10-17
comments: true
tags: [iPad, iPadAir, iPadAir2, ApplePay, AppleSim]
---
Uno penserà che ci si riferisca ai sei virgola uno millimetri di [iPad Air 2](http://www.apple.com/it/ipad-air-2/). Sei virgola uno millimetri. Più leggero, più veloce, stessa autonomia.<!--more-->

Sarebbe più adeguato invece osservare come uno dei punti di forza commerciali magnificati sul sito sia l’abbondanza di *app* fatte apposta per iPad. A breve, come da [evento](http://www.apple.com/apple-events/2014-oct-event/), avremo Pixelmator su iPad per cinque dollari.

Aggiungere questo spessore e questo peso e persino a me, titolare di un amatissimo iPad di terza generazione, che cambio apparecchi solo quando sono a fine vita, viene la tentazione di eccepire. Ancora più forte con il Wi-Fi 802.11ac, che può cambiare significativamente i tempi di trasferimento dei file.

Invece la vera sottigliezza sta nella [Apple Sim](http://www.apple.com/ipad-air-2/wireless/), per ora riservata agli anglosassoni. La mossa è interessante, una sola Sim fisica che può scegliere tra varie tariffe di vari operatori. Apple ha deciso che è ora di fare partire la motosega sotto le sedie degli operatori telefonici. Se funziona e prende piede, se ne vedranno veramente delle belle. Più che con [Apple Pay](http://www.apple.com/apple-pay/), che pure parte lunedì.

Assai poco sottile, invece, il nome. iPad certo, Air d’accordo, 2 è spesso.