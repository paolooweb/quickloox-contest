---
title: "Lo spirito invisibile del mercato"
date: 2023-09-28T22:35:46+01:00
draft: false
toc: false
comments: true
categories: [Hardware]
tags: [Ming-Chi Kuo, Vision Pro]
---
Adam Smith ha introdotto la [mano invisibile del mercato](https://www.britannica.com/money/topic/invisible-hand). Ming-Chi Kuo, analista rinomato soprattutto perché la maggior parte della gente non sa quale sia il cognome, ha rapporti stretti con lo spirito invisibile del mercato.

Dal proprio account Medium, infatti, mai nome di piattaforma fu più fatale, il paragnosta [racconta un sacco di cose che il mercato gli ha detto](https://medium.com/@mingchikuo/apple-vision-pro-shipment-growth-may-be-below-market-expectations-apple-vision-pro出貨成長可能低於市場預期-7a5eeb613ab0), con mia grande invidia, dato che di mercato non mi parla neanche quello del modernariato.

Secondo il mercato, le vendite 2024 di Vision Pro saranno tra le quattrocentomila e le seicentomila. Il problema è che il mercato, bontà sua, ne aspettava un milione. Certa sa cose che non sappiamo, in grande quantità, dato che ufficialmente Vision Pro ha a malapena un prezzo e potrebbe uscire il primo gennaio o il trentuno marzo senza inficiare gli annunci di Apple.

Il mercato sapeva anche, con i propri canali a metà tra questo mondo e quell’altro, che nel 2025 sarebbe uscita una versione a basso costo di Vision Pro e invece non accadrà. Sempre notevole, per un prodotto che fuori da Apple avranno toccato in mille sì e no.

Entro la metà del 2027, ha ancora vaticinato il mercato, uscirà Vision Pro 2 e, per conseguenza, Vision Pro 1 non avrà aggiornamenti hardware. Ho difficoltà a stabilire dei legami logici coerenti con l’affermazione, ma come competere con qualcosa che sa già che accadrà con il modello 2, quando il modello 1 potrebbe farsi aspettare ancora sei mesi?

Ming-Chi Kuo qui prende le redini e si arroga l’autorità di scavalcare il mercato, il quale osava pensare che Vision Pro avrebbe sostituito iPhone nel cuore dei clienti Apple, o qualcosa del genere.

La ming-chiata è chiedersi *perché gli utenti dovrebbero avere bisogno di questo prodotto*.

Domanda legittima, al quale hanno risposto i mestieranti di MacDailyNews: non fini analisti ma pennivendoli alla buona, che vivono di banner e faticano. Beh, tirano in ballo nientemeno che [un John Dvorak del 1984](https://macdailynews.com/2023/09/27/ming-chi-kuo-warns-apple-vision-pro-shipment-growth-may-be-below-market-expectations/):

>Il Macintosh usa un dispositivo sperimentale di puntamento chiamato mouse. Mancano le prove che la gente voglia usare cose come queste.

Certo, invece di fare la seduta spiritica con il mercato, loro hanno rispolverato il *San Francisco Examiner*. Ognuno fa quello che può.