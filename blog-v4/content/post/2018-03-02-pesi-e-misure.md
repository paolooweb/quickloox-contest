---
title: "Pesi e misure"
date: 2018-03-02
comments: true
tags: [Terminale]
---
Mi è tornato comodo di recente il comando

`du -a . | sort -rn | head -20`

per visualizzare i venti file più pesanti all’interno di una *directory*.

Il Terminale è sempre interessante più per come aumentare la potenza di un comando che per il comando in sé.

Compito facile: variare il numero di file mostrati.

Compito medio: visualizzare la dimensione dei file in modo leggibile da un umano.

Compito difficile: applicare il comando a un numero arbitrario di directory e sottodirectory.