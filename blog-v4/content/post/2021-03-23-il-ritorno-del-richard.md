---
title: "Il ritorno del Richard"
date: 2021-03-23T02:49:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [Richard Stallman, Fsf, Gnu] 
---
Nel 2019 Richard Stallman fu indotto a lasciare il suo ruolo di consigliere in [Free Software Foundation](https://www.fsf.org) per via di certi suoi commenti a proposito di una storiaccia di sesso con diciassettenne che avrebbe coinvolto [Marvin Minsky](https://web.media.mit.edu/~minsky/) durante una visita all’allora raccomandabile [Jeffrey Epstein](https://en.wikipedia.org/wiki/Jeffrey_Epstein).

Ora Stallman [è ritornato in carica](https://arstechnica.com/tech-policy/2021/03/richard-stallman-returns-to-fsf-18-months-after-controversial-rape-comments/). Un po’ come se avesse scontato un anno e mezzo di sospensione da Free Software Foundation per avere espresso una opinione (non ha mai lasciato la sua posizione di capo del progetto Gnu).

L’episodio controverso che Stallman è stato accusato di commentare risale, posto che sia accaduto, al 2001. La vedova di Minsky, che era in viaggio assieme a lui nella residenza di Epstein, ha smentito che sia accaduto alcunché. Minsky aveva settantatré anni.

Stallman è un gigante del nostro tempo per avere fondato il movimento del software libero. Dopo di che è un orso dall’igiene problematica, di capacità relazionali dubbie, con un sacco di visioni eccessivamente radicali e distorte verso svariate situazioni del mondo informatico.

Con tutto questo, la domanda è a che cosa siano serviti questi diciotto mesi di esclusione. Se Stallman stava lavorando bene, è stato sciocco che lasciasse; se stava lavorando male, è sciocco che ritorni.

In ogni caso, lui finirà nei libri di storia. Chi lo ha attaccato per un commento, al massimo, su TikTok.

*Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile.*