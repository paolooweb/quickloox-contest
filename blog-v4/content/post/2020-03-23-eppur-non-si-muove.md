---
title: "Eppur non si muove"
date: 2020-03-23
comments: true
tags: [Drang, iPad, Pro, Magic, Keyboard]
---
Deliziosa spiegazione di che cosa sta dietro meccanicamente e fisicamente al *design* della nuova Magic Keyboard di iPad Pro, [da parte di Dr. Drang](https://leancrew.com/all-this/2020/03/magic/).

A chi si chiede se sia stabile, se funzioni, [se non si ribalti](https://macintelligence.org/posts/2020-03-19-come-dicevano/) eccetera risponde senza dare una risposta (le tastiere non sono ancora disponibili ai non iniziati) ma mostrando con chiarezza cristallina forze, pesi ed equilibri in gioco.

È anche una bella dimostrazione di come il *design* di prodotto sia ben più articolato di quanto si possa pensare. Nella testa di tanti, una tastiera è una tastiera.