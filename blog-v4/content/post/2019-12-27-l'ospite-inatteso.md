---
title: "L’ospite inatteso"
date: 2019-12-27
comments: true
tags: [iPhone, X, Hero, Forge, Pay, ID, Face, watch]
---
La notte di Natale sono diventato proprietario a mia insaputa di un iPhone X.

Uno dirà bella forza, già vecchio, neanche è più in catalogo.

D’accordo su tutto, solo che usavo – appagato – un iPhone 5 e la distanza è comunque molta.

Nel giro di un’oretta ho completato la transizione. Il novanta percento del tempo è stato preso dal backup su disco di iPhone 5, che ho ripristinato sul nuovo terminale.

Il resto è stato veramente semplice, grazie alla possibilità di validazione del nuovo apparecchio da parte di un altro apparecchio iOS esistente. iPhone 5 era troppo vecchio per provvedere, ma iPad Pro ha risolto alla grande. Unica incertezza nella procedura, ho dovuto inserire l’ID Apple a mano perché inquadrare con la videocamera il codice visivo – una palla luminescente che ho imparato a conoscere con watch – non funzionava. Superato l’ostacolo, pochi istanti per calibrare Face ID e confermare  Pay ed ero già operativo.

Sui sistemi moderni, passare a un nuovo apparecchio è una esperienza tutto sommato banale. Grazie al backup su disco sono andate al loro posto quasi tutte le password e tutte quelle importanti; insomma, da dire c’è poco.

Lo shock culturale invece è percepibile. Lo schermo a disposizione sembra infinitamente più grande, molto più del valore effettivo; la superficie è immacolata, quasi lattiginosa, un altro mondo rispetto a iPhone 5, che sarebbe Retina; di fronte a un iPhone X, tuttavia, non c’è partita.

Il sistema è veloce, nessuna esitazione. Starò a vedere che cosa accadrà mentre lo sollecito; iPhone 5 aveva solo sedici gigabyte e questo, invece, duecentocinquantasei. L’impostazione di prima era improntata allo stretto indispensabile mentre ora posso scegliere. Ho approfittato dell’abbondanza per scattare foto e video a volontà durante il pranzo di Natale: tutto fluido e immediato, risposta istantanea, un piacere per chi ami rubare lo scatto invece di cercare dichiaratamente la posa.

Ho preso a suo tempo un iPad Pro 12”9 perché preferisco lo schermo grande lì e avere in tasca un iPhone piccolo, che risolva le contingenze e niente più. Per questo motivo sono sempre stato scettico sull’evoluzione, che per me è una deriva, verso dimensioni di schermo sempre più importanti. Devo dire che queste prime ore con iPhone X sono state più confortevoli di quello che pensavo. Certo non mi sono trovato in alcuna situazione critica o vincolante; però nella tasca dei pantaloni non dà fastidio e quel poco che ho fatto di elaborato l’ho fatto con una mano sola, come sono abituato.

La sensazione al tatto è molto buona; avere in mano iPhone X è appagante.

Non saprei che altro dire, al momento. Arriverà il primo acquisto con Apple Pay, inizierò a scaricare app utili e dilettevoli. Sono riuscito a usare, con qualche difficoltà, un sito come [Hero Forge](https://www.heroforge.com), che su iPhone 5 era fuori discussione.

Sul *notch* è stato già scritto e detto tutto; ho naturalmente il vantaggio di sapere bene che cosa attendermi, dopo anni e milioni di utilizzi in tutte le salse. Con questa premessa che potrebbe falsare la valutazione, sparisce. Non ci si pensa e basta. Tra l’altro iOS mette nelle “corna” dello schermo ai lati del *notch* solo informazioni di servizio, così che quella zona diventa l’equivalente della barra superiore di un altro iPhone. La si guarda solo per sapere che ora è, se c’è campo, se c’è Wi-Fi.

Sarei curioso di provare la carica finta wireless, per contatto; magari capiterà l’occasione. Anche la funzione di telecomando la voglio sperimentare.

Fino a qui, beh, babbo Natale è stato esagerato, ma non riesco a fingere indignazione e, indegnamente, mi godo un incredibile regalo.