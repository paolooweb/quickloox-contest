---
title: "Il vento dell’automazione personale"
date: 2019-11-27
comments: true
tags: [MacStories, Dyson, HomeKit, iOS, Home]
---
Uno dei temi del 2020, iniziamo a portarci avanti, sarà ancora una volta l’automazione personale. Lo ripeto da qualche anno solo che, a differenza del meme *L’anno di Linux per il desktop*, questo si dispiega davvero.

Si guardi a questo affascinante [articolo di Federico Viticci su MacStories](https://www.macstories.net/ios/automating-a-dyson-fan-with-homekit-and-push-notifications/), relativo al controllo di un ventilatore Dyson via iOS attraverso HomeKit e varie app indipendenti, con aggiustamento automatico in funzione della temperatura della stanza e notifiche *push* su qualsiasi apparecchio interessato, fosse anche watch.

Non è una passeggiata; Viticci, che è un portento, ha dovuto sudare per arrivarci e il ventilatore non c’entra. Eppure si fa. Le novità sono, la prima, che neanche tantissimo tempo fa non si sarebbe fatto se non con soluzioni hardware realmente artigianali alla portata di pochissime persone, per tempo e disposizione d’animo più che per capacità, ma pochissime.

La seconda, che si parla già di nuova versione di HomeKit, che risolve i problemi esistenti. Significa che la fase pionieristica è già finita; non è più tempo di fare funzionare le cose non importa come, ma farle funzionare *bene*.

Il ventilatore di casa è un caso particolare. Ma anche solo fare svegliare il Mac di casa dal sonno quando si rientra, oppure caricare automaticamente gli scontrini della spesa nel database casalingo… sono esempi di una categoria infinitamente numerosa. Sotto con i Comandi rapidi, sotto con AppleScript. Il vento dell’automazione personale soffia sempre più forte e regala vantaggi competitivi eccezionali a chi sia disposto a prendersi sulle spalle la responsabilità di imparare qualcosa in più dell’ovvio.