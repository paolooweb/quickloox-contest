---
title: "Controcorrente"
date: 2015-07-11
comments: true
tags: [Apple, Idc, Microsoft, Nokia]
---
Noto occasionalmente – se si facesse sempre sarebbe tipo la quarantacinquesima volta o simile – che secondo Idc i computer venduti nel secondo trimestre del 2015 [sono calati](http://www.cnbc.com/2015/07/10/pc-shipments-plunge-apple-surges-on-macbook.html) dell’11,8 percento rispetto allo stesso periodo del 2014.<!--more-->

All’interno di questa flessione, Apple guadagna il 16,1 percento (e Idc conta come computer solo i Mac).

No, i computer non sono tutti uguali. E neanche i telefoni: sto aspettando la comunicazione effettiva (anche se la notizia è già ufficiale) per commentare l’annuncio di Microsoft di avere buttato via sette miliardi di dollari nell’acquisto di Nokia (e mandare a casa mille persone per ogni miliardo buttato).

Apple vale la pena di essere seguita perché, nel bene e nel male, va controcorrente. Gli altri sono noiosi e basta.