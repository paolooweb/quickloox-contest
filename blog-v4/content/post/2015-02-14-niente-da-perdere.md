---
title: "Niente da perdere"
date: 2015-02-15
comments: true
tags: [FrereJones, overshoot]
---
Ragazzi e ragazze: lettura obbligata del blog di Tobias Frere-Jones, una delle massime autorità viventi in fatto di *font*. E in particolare della serie [Typeface Mechanics](http://www.frerejones.com/blog/typeface-mechanics-001/).<!--more-->

Non c’è niente da perdere perché siamo al primo articolo della serie, dedicato all’*overshoot*, lo sporgere dei caratteri appuntiti o arrotondati oltre i confini standard della riga allo scopo di dare perfezione ottica al testo.

L’articolo è della lunghezza giusta, chiarissimo, confortato da grafica perfettamente esplicativa, insomma è impossibile non capire e anche con poco tempo a disposizione, ci sta.

Una infarinatura di tipografia, nel 2015, con tutta la potenza digitale che abbiamo a disposizione, è cultura generale. Anzi: c’è qualche insegnante nei paraggi? Le spieghiamo queste cose, vero?