---
title: "Il cinico triste"
date: 2013-11-04
comments: true
tags: [Dediu, Asymco, azioni, Apple]
---
Per quando capiterà che le azioni di Apple scendano e questo venga considerato l’inizio della fine, una bella [risposta](http://www.asymco.com/2013/08/13/its-a-wonder/) di Horace Dediu di Asymco a una cronista del *Guardian*.<!--more-->

>Ciò che influisce maggiormente sul prezzo delle azioni è un cambiamento nella percezione permanente che Apple non sopravviverà. A oggi, come in tutto il passato, nessuna attività di Apple è stata considerata sufficiente per la sua sopravvivenza. Apple ha sempre avuto la quotazione in Borsa di una azienda in caduta libera perpetua. È una conseguenza del dipendere da prodotti rivoluzionari. Non importa quante rivoluzioni faccia, si presume (e si è sempre presunto) che non ce ne sarà mai un’altra. Quando era la produttrice di Apple II, la fine di Apple era imminente perché il futuro di Apple II era facilmente prevedibile come a termine. Quando produceva i Mac, la fine di Apple era imminente perché Mac avrebbe certamente visto il declino. Ripetere per iPod, iPhone e iPad. C’è da meravigliarsi che l’azienda abbia un valore purchessia.

A pensarci, il cinico incapace anche solo di sperare in un progresso rivoluzionario, che punta sempre sullo *status quo* perché è la cosa più probabile, è una persona triste.