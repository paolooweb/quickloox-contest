---
title: "La fiera delle vacuità"
date: 2021-04-23T23:56:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [iMac, M1, Apple, iPad Pro, MacBook Pro, Arm] 
---
Seguire Apple è affascinante. Purtroppo capita di seguire anche i commentatori, dilettanti e finti professionisti (professionisti nel senso sano della parola ne vedo pochi).

All’indomani dell’ultimo [evento Apple](https://macintelligence.org/posts/Gli-estremi-non-si-toccano.html), leggo e sento cose come queste. Niente link, già mi vergogno di avere letto, non voglio responsabilità.

* *Ho un iMac 27” e sono un po’ deluso perché iMac 21” ora è diventato iMac 23”5 e mi aspettavo uno schermo più grande* (per logica mi aspetterei l’arrivo di un prossimo iMac 27”, magari con schermo 29”, più che vedere il 21” diventare un 27”, ma forse sono io che sragiono).

* *Siccome hanno lo stesso processore, iMac è praticamente come un MacBook Pro* (avevano lo stesso processore anche sotto Intel…).

* *M1 su iPad Pro è inutile finché non esce iPadOS 15* (davvero, non sono proprio andato ad approfondire il perché).

* *Apple si è inventata la sostenibilità ambientale perché è di tendenza* (che può essere vero e il tema come viene trattato dalle grandi aziende mi lascia sempre poco convinto, compresa Apple; in ogni caso, il primo rapporto di sostenibilità ambientale pubblicato a Cupertino [è datato 2008](https://www.apple.com/environment/pdf/Apple_Facilities_Report_2008.pdf)).

* *Apple ha tenuto per mesi la transizione M1 nella riservatezza* (a me risultano un [annuncio di Apple Silicon a giugno 2020](https://www.apple.com/newsroom/2020/06/apple-announces-mac-transition-to-apple-silicon/), all'inizio di una presentazione web vista da milioni di spettatori, che inaugurava Wwdc, cioè una settimana di [video](https://developer.apple.com/videos/wwdc2020/) e documentazione per sviluppatori, con disponibilità immediata di un Developer Transition Kit basato su Mac mini e [battesimo ufficiale di Apple Silicon in forma di  M1](https://www.apple.com/newsroom/2020/11/apple-unleashes-m1/) il 10 novembre 2020), con disponibilità contestuale del primo MacBook Pro con chip Arm.

So bene che i social media sono palestre per l’ego e, come nella vita, è più profittevole frequentare i culturisti che scolpiscono il proprio corpo, prima di quelli che lo gonfiano.

Leggere un giudizio vuoto su un evento Apple è fonte di grande divertimento per tanti e guai a toglierglielo; ricordiamoci che approfondire il vuoto porta solo a scoprire altro vuoto.

*Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile.*