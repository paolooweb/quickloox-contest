---
title: "Impiegati di concetto"
date: 2014-07-08
comments: true
tags: [Apple, Microsoft, Nokia, Kontra, CounterNotions, KnowledgeNavigator]
---
La differenza tra Apple e una Microsoft o una Nokia la spiega bene [Kontra su CounterNotions])(http://counternotions.com/2008/08/12/concept-products/): Apple non produce *concept*, prodotti futuribili. L’ultimo della serie fu [Knowledge Navigator](http://www.digibarn.com/collections/movies/knowledge-navigator.html) e si era ancora negli anni ottanta. Guarda caso, Apple non se la passava benissimo.<!--more-->

>Sebbene Nokia e Microsoft ci abbiano dato negli anni un numero infinito di concept, non hanno prodotto alcunché come TiVo, iPod, iPhone, OS X, iTunes App Store, né creati paradigmi di esperienza utente nuovi di zecca, né trasformato mercati calcificati, né catturata l’immaginazione della gente, né altro. Non avevano la disciplina organizzativa e intellettuale per passare dal concetto al prodotto.

Lo diceva pure Steve Jobs: [real artists ship](http://www.folklore.org/StoryView.py?story=Real_Artists_Ship.txt), i veri artisti presentano la loro opera compiuta.

Internet brulica di *concept* che vogliono rappresentare il prossimo modello di iPhone, iPad, Apple TV, iWatch eccetera. Mica per niente sono realizzati da bravissimi grafici o esperti di modellazione tridimensionale; professionisti che nella situazione *non devono creare alcun vero prodotto*. Non esercitano la fantasia o la creatività; semplicemente, sono liberi dai vincoli concreti e dunque si possono permettere l’inesistente. Così sono buoni tutti. Apple, con iPhone, scommesse l’azienda su un prodotto reale, che poteva fallire, soggetto alle limitazioni della realtà. I *concept* non sono cosa creativa, bensì – con il massimo rispetto per la funzione – impiegatizia.