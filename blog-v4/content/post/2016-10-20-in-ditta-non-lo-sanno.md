---
title: "In ditta non lo sanno"
date: 2016-10-20
comments: true
tags: [Ibm, Mac, iPhone, iPad]
---
Le cifre non concordano completamente con quanto [si era scritto](https://macintelligence.org/posts/2015-10-18-trentanni-dopo/) un anno fa, ma la tendenza è indiscutibilmente quella e ora ci sono aggiornamenti.<!--more-->

Ibm dichiara di avere [il dispiegamento di Mac più ampio del mondo](https://www.jamf.com/blog/debate-over-ibm-confirms-that-macs-are-535-less-expensive-than-pcs/) (fuori ovviamente da Apple). Due apparecchi *mobile* su tre sono iOS. Il 91 percento degli utilizzatori di apparecchi Apple dentro Ibm sono soddisfatti, meno dell’uno percento ha chiesto la sostituzione di un Mac con un PC, il 73 percento vuole usare un Mac come suo prossimo computer.

Il supporto tecnico riceve una richiesta di aiuto relativa a Mac ogni **centoquattro** richieste relative a Windows, che a me pare una cosa enorme. Un terzo dei macchisti sta già usando Sierra e quasi due terzi iOS 10.

Dopo quattro anni di esperienza, Ibm può affermare che il supporto dei Mac costa un terzo di quello di Windows. Ibm Giappone ha reso i Mac la dotazione standard; chi desidera Windows deve chiederlo esplicitamente, come eccezione.

In totale i Mac installati finora sono oltre **novantamila** e supereranno i centomila entro fine anno; un anno fa c’erano trentamila Mac installati. Ogni settimana vengono installati 1.300 nuovi Mac.

In aggiunta, gli apparecchi iOS, tra iPad e iPhone, sono 217 mila.

Se capita di parlarne in ditta, magari ci sono da risparmiare dei soldi o guadagnare in produttività, o tutt’e due le cose.