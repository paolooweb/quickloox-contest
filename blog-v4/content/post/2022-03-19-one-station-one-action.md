---
title: "One station, one action"
date: 2022-03-19T00:54:23+01:00
draft: false
toc: false
comments: true
categories: [Hardware, Software]
tags: [Universal Control, Mac, iPad, iPhone, Riccardo Mori, Siri, watch]
---
Negli ultimi anni il software Apple, specie su Mac, ha avuto i suoi bei chiaroscuri. L’amico Riccardo parla senza mezzi termini di [stagnazione](http://morrick.me/archives/9523) e io non sono d’accordo ma perché penso che esageri, non perché non ci siano, appunto, questioni aperte e situazioni non proprio ottimali in qualche zona dell’ecosistema.

Eppure la grazia e la magia di Universal Control sono qualcosa che, nonostante se ne sia parlato a lungo, emoziona come la festa di compleanno a sorpresa. Ecco che sulla scrivania ci sono due, tre, cinque apparecchi, e insieme diventano una stazione di lavoro unica, una tastiera e un trackpad si occupano di tutti e tutto, gli schermi separati sono come uno solo.

Uno ci pensa solo dopo effettivamente. Ma, prima di Universal Control, si può dire in gran parte che mentre sei a usare Mac, iPad è sulla scrivania a prendere polvere e viceversa. Improvvisamente, dopo, usi tutto insieme e niente è inoperoso o anche momentaneamente inutile.

Un attimo dopo, cambiato lo scenario, i *device* prendono la loro strada abituale verso tasche, zaini, borsette, indipendenti e autonomi e completi come sono da anni. Non è la rottura dell’incantesimo ma una ricomposizione, come certi *transformer* giapponesi dei cartoni animati che, così come assumono la forma di un megarobot, un momento più tardi ritornano un corteo di veicoli con funzioni assortite.

Continua a darmi fastidio che io chieda a Siri di impostare su watch una sveglia alle 7:45 e lui riepiloghi testualmente *alle meno un quarto di mattina*. Poi però mi ritrovo a esercitare lo Universal Control e tutti i miei apparecchi diventano componenti d’orchestra, il mio trackpad un’improbabile bacchetta. Il fastidio lascia spazio alla pienezza dell’animo. Tra un anno non si potrà credere che, prima di ieri, fosse diverso da così.