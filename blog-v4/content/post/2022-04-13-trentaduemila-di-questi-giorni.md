---
title: "Trentaduemila di questi giorni"
date: 2022-04-13T03:13:08+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [BBEdit, Bare Bones]
---
Lo so che il trentesimo compleanno di [BBEdit](https://www.barebones.com/products/bbedit/) è stato ieri; volevo però godermi i commenti in giro per il web.

La cosa più notevole che ho visto è stata il ricordo della funzione principale che distingueva il programma dagli altri editor di testo.

BBEdit, nel 1992, eccelleva per la sua capacità di [leggere file più lunghi di _trentaduemila caratteri_](https://twitter.com/jsnell/status/1513893334271418369).

<blockquote class="twitter-tweet"><p lang="en" dir="ltr">Being able to edit files larger than 32k was really the biggest win in those days. <a href="https://t.co/x06GCvHQhg">https://t.co/x06GCvHQhg</a></p>&mdash; Jason Snell (@jsnell) <a href="https://twitter.com/jsnell/status/1513893334271418369?ref_src=twsrc%5Etfw">April 12, 2022</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

Oggi gli si chiede grosso modo di fare la stessa cosa su file _un milione di volte_ più lunghi. E lo fa.

Oggi, una gita per [l’account Twitter di BBEdit](https://twitter.com/bbedit) è una rara boccata di ossigeno e serenità.

_It doesn’t suck.®_