---
title: "Oltre la carta"
date: 2023-11-28T19:10:43+01:00
draft: false
toc: false
comments: true
categories: [Internet]
tags: [Popular Science, Popsci]
---
Sono talmente tanti i periodici che hanno cessato negli ultimi anni la produzione di una edizione cartacea, che la prossima testata neanche farà notizia.

Si può fare meglio (o forse peggio, dipende): si può smettere di esistere come periodico. È quanto [ha annunciato](https://www.theverge.com/2023/11/27/23978042/popular-science-digital-magazine-discontinued) *Popular Science*, veneranda rivista statunitense di divulgazione scientifica.

*Popsci*, come la chiamava chiunque, aveva già smesso di consumare carta, un paio di anni or sono. Ora chiude i battenti anche l’edizione digitale che aveva preso il suo posto.

Rimane aperto il marchio: il sito continuerà a pubblicare articoli, semplicemente non organizzati in un formato rivista. Speriamo che questa decisione, abbinata a un consistente taglio della forza lavoro redazionale, sia davvero l’ultima e l’esiguo team lasciato a occuparsi di *Popsci* per lungo tempo ancora. Centicinquantuno anni di storia sono una bella sfida all’entropia e sarebbe bello vedere continuare il conteggio.

Difficile sempre dire, in casi come questo, dove inizia la cannibalizzazione del digitale e dove ci siano state magari incompetenze nella gestione o cecità nel mettere a punto una buona strategia.

Sicuramente ci sono testate che continuano a pubblicare e alcune anche con una edizione cartacea (è il caso, che conosco bene perché me lo regalano, di *Scientific American*). Altrettanto sicuramente servono idee molto chiare, occhi molto aperti, spirito di iniziativa e creatività a mille, nonché l’umiltà di non credersi sopra un piedistallo come ogni tanto poteva capitare tempo fa a un giornalista al lavoro sulla divulgazione scientifica. Quel tempo è proprio scomparso.