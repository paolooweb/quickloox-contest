---
title: "Felice di trascriverla"
date: 2017-10-24
comments: true
tags: [HappyScribe, Happy, Scribe]
---
Ho sempre cercato un servizio di speech-to-text *decente* per trascrivere in forma di testo file audio raccolti durante interviste, presentazioni, podcast.<!--more-->

Lo cerco decente perché quello perfetto non esiste; mi basta un buon compromesso di affidabilità e possibilità di arrivare in tempo ragionevole a un transcript ragionevolmente buono.

Forse l’ho trovato con [Happy Scribe](https://www.happyscribe.co). Il servizio è a consumo, con un prezzo assolutamente accessibile di nove centesimi di euro al minuto; gli ho dato in pasto un file da oltre quaranta minuti e me la sono cavata con poco più di quattro euro, prezzo più che equo.

In cambio c’è non solo il file testo, cioè il minimo sindacale, ma anche una interfaccia web semplicissima dove è possibile eseguire editing manuale del transcript restando sincronizzati con la traccia audio.

L’editing è veramente essenziale, ma è quello che serve e permette di arrivare a esportare sul computer una trascrizione testuale che finalmente esenta dalla noiosa sbobinatura.

Faccio girare; se serve, è una possibilità a basso costo. Inoltre, proprio per questo, collaudarlo con spesa modica per valutare l’effettiva rispondenza al bisogno è semplicissimo.