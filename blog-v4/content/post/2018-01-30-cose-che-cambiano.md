---
title: "Cose che cambiano"
date: 2018-01-30
comments: true
tags: [Jobs, Google, innovazione, Grab, Yagge, Quora, Dediu, iPhone]
---
Appena parli di innovazione, se lo fai seriamente, si possono aprire mondi. C’è [chi cambia continente](https://macintelligence.org/posts/2018-01-28-l-influenza-asiatica/) abbandonando posizioni di lavoro prestigiose e pagatissime e chi tira fuori aneddoti meno conosciuti sulla storia di una grande innovazione per spiegare che sono cose difficilmente pianificabili a tavolino nonché ostiche per il *modus vivendi* di qualsiasi impresa.<!--more-->

**Stefano** mi ha regalato un commento che ho apprezzato:

>Considero innovazione quel servizio o oggetto che cambia di fatto il mio vivere quotidiano.

Che si porta dietro un corollario: è difficile fare innovazione così costante che il vivere quotidiano cambia in continuazione. Forse neanche tanto augurabile.

A chiosa di questa piccola rassegna di idee tre letture prese dagli archivi, per continuare a riflettere sulla materia: un test per (imparare a) [distinguere tra novità, creazione, invenzione e innovazione](https://macintelligence.org/posts/2014-04-16-le-parole-per-dirlo/); la [disinvoltura eccessiva](https://macintelligence.org/posts/2017-09-20-l-invenzione-del-breakthrough/) con cui si designa o meno l’innovazione e un [pesce d’aprile](https://macintelligence.org/posts/2014-04-01-innovazione-trasversale/).