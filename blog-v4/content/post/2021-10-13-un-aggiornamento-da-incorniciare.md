---
title: "Un aggiornamento da incorniciare"
date: 2021-10-13T00:34:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [MacStories, Federico Viticci, Shortcuts, Comandi rapidi, Mac, iOS, iPadOS, macOS, iPhone, iPad, Mac, Apple Frames, Apple Frames 2.0, watchOS, Visual Basic, Android, Apple Watch] 
---
Federico Viticci ha pubblicato e reso liberamente disponibile [Apple Frames 2.0](https://www.macstories.net/ios/apple-frames-2-0-faster-lighter-and-featuring-support-for-iphone-13-ipad-mini-ipad-10-2-imac-24-macbook-air-and-multiple-languages/), la testimonianza più evidente del potenziale insito nello scripting e nell’automazione.

Chi segua il suo sito *MacStories* sa che le schermate di sistema operativo e programmi vengono pubblicate nella cornice del prodotto: che io sappia, nessun altro lo fa. Tutti noi comuni mortali acquisiamo una schermata e pubblichiamo la schermata.

Lui no. Se la schermata arriva da un iPhone X, la vediamo dentro il telaio di un iPhone X, come se fosse stata fotografata la macchina mentre mostra la schermata.

Chi ha per le mani un grafico, provi a chiedergli quanto vuole per fare lo stesso lavoro. L’articolo di *MacStories* contiene ventotto schermate.

E per farlo più volte? Viticci ha creato un automatismo con i Comandi rapidi di iOS/iPadOS/macOS/watchOS. Questa versione 2.0 aggiorna le cornici disponibili, è più veloce, pesa meno e riconosce più lingue.

Non c’è che scaricarla. Su iPhone ho scoperto una cosa interessante: per poter installare Comandi rapidi provenienti da fonti non certificate, bisogna avere azionato almeno una volta un Comando rapido. Altrimenti il sistema inibisce l’accesso all’interruttore della preferenza.

Poi, la prossima volta che qualche frescone chiederà che cosa fa un Mac che non possa fare un PC, o rimarcherà che Android costa meno, ecco che cosa fargli vedere. Funziona su tutte le piattaforme di Apple con la sola eccezione di Apple TV (sì, compreso watch). Richiederebbe un lavoro di programmazione importante se non ci fossero i Comandi rapidi. Risolve un problema reale e, nel farlo, aumenta il valore dell’offerta di *MacStories*.

Chiedergli, al frescone, di fare la stessa cosa con Visual Basic, o con Android se è per quello, con i soli strumenti di sistema.

Se un esempio come questo non basta a fare venire la voglia di automatizzare, penso che niente ci riuscirebbe. Ed è un peccato, per chi si priva di un pezzo di autoformazione non banale.

*Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile.*