---
title: "Scegliere per amore"
date: 2020-05-13
comments: true
tags: [iPhone, Gruber, Android]
---
Qualche giorno fa un amico intenzionato a prendere un iPad e indifferente alle minuzie tecnologiche mi ha chiesto che differenza ci fosse, nel modello che aveva scelto, tra i sessantaquattro gigabyte di spazio e i sei gigabyte di RAm installata.

Qualcuno, fosse anche un depliant, cercava di vendergli un iPad (anche) in base alla sua Ram.

_Nemmeno Apple lo fa_. Di nessun iPad viene comunicato ufficialmente il dato della Ram. Qualcuno pensa che, se aiutasse a vendere, non verrebbe esibito?

Mezzo secolo fa, quando migliorare le specifiche dei computer era un atto straordinario di innovazione, si comparavano le macchine attraverso la lista dei componenti. Più disco o più megahertz significavano veramente qualcosa.

Sono passati quarantacinque anni e da tempo la lista dei componenti significa niente. Migliorare le specifiche di una macchina è un normale processo industriale. Più Ram o più pixel hanno un significato molto, molto relativo.

Conta l'esperienza complessiva. Conta da molto tempo. In tanti hanno continuato a usare Mac quando le prestazioni del processore faticavano a stare all'altezza del mondo Windows. Perché l'interfaccia era superiore. L'esperienza complessiva era più equilibrata di quello che suggeriva il confronto delle liste.

Un indizio per chi le liste le fa ancora, come se servissero: la gente non compra più in base a quei criteri, posto che mai lo abbia veramente fatto. Vuole spendere meno oppure innamorarsi di una macchina. Il resto non esiste, o quasi.

La prova ennesima l'ha fornita quel sito di fanatici Apple che è _Android Central_, che un mese fa ha titolato [L'iPhone più economico ha un processore più potente dell'Android più costoso](https://www.androidcentral.com/cheapest-iphone-has-more-powerful-processor-most-expensive-android-phone). Sottotitolo:

> Apple usa in iPhone SE edizione 2020 il chip A13 Bionic, che supera in prestazioni lo Snapdragon 865 sotto quasi ogni aspetto.

Apple ha – a livelli oramai imbarazzanti – la superiorità sulle prestazioni. Ha le app migliori. Ha il sistema operativo più citato. Eppure si vendono milioni e milioni di Android a persone che sono anche entusiaste e soddisfatte, pronte a difendere la loro scelta anche contro qualsiasi evidenza (un'occhiata ai commenti dell'articolo citato è sufficiente per capire).

Perché la loro esperienza, secondo il loro criterio, è soddisfacente. È l'esperienza che conta, non le liste. Chi fa le liste è rimasto al 1990. Nel migliore dei casi, è un illuso. Le liste non servono più né hanno senso.

Al mio amico ho detto di ignorare il dato della Ram e provare l'esperienza concreta. Media World ha riaperto, dalle sue parti. Ha provato, si è innamorato e ha preso iPad.

Nel 2020 funziona così. Gli anziani dentro si rassegnino, il mondo informatico è cambiato. Il sentimento conta molto più dell'analisi riduzionista.