---
title: "Superior stabat Red Hat"
date: 2023-07-16T02:47:19+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [Red Hat, IBM, Oracle, Linux, Italo Vignoli, Vignoli, Suse]
---
Favolista immortale Fedro a narrare non so quante migliaia di anni fa del [lupo che presso un torrente sbranava l’agnello](https://www.nostrofiglio.it/giochi/favole/il-lupo-e-l-agnello-la-favola-di-fedro), colpevole di sporcare la *sua* acqua pur trovandosi a valle del lupo stesso.

Se invece di dire *a valle* adoperiamo *downstrem*, eccoci proiettati nel grande fiume Linux a seguire una vicenda che a nessun *chatbot* avrebbe mai potuto venire in mente, da quanto è surreale.

Prima di tutto ringrazio [Italo](https://www.vignoli.org) per aver anticipato sul canale Telegram di LibreItalia (iscriviti!) l’accaduto e avermi messo sulla pista degli eventi che vorrebbero rendere chiuso il software aperto.

A scatenare tutto è un [post di Mike McGrath](https://www.redhat.com/en/blog/furthering-evolution-centos-stream), Vice President di Red Hat, che chiude con una frasetta:

>Clienti e partner di Red Hat potranno accedere ai sorgenti di Red Hat Enterprise Linux tramite i rispettivi portali, secondo il contratto di abbonamento.

Nel gergo del mercato Linux questa è la direzione *downstream*, verso valle, dal fornitore ai clienti per capirci, e Red Hat chiude i rubinetti: codice sorgente *downstream* solo a pagamento.

Si crea un certo fermento. McGrath integra con [un secondo post](https://www.redhat.com/en/blog/red-hats-commitment-open-source-response-gitcentosorg-changes) nel quale va al sodo: Red Hat costruisce la sua versione di Linux anche grazie alle distribuzioni *upstream*, a monte; ci aggiunge un sacco di lavoro e di valore e – correttamente – condivide tutto questo con chi lavora a monte; a valle, invece, ci sono distro che in pratica prendono Red Hat, ricompilano la suite, cambiano qualcosina e si mettono sul mercato a fare concorrenza a Red Hat. La quale non trova valore in questo e quindi ha deciso di rendere le cose difficili a chi voglia provarci.

C’è un però: parliamo di Linux. Software libero. Impedire a qualcuno l’accesso al codice sorgente è un problema, anche dove qualche cavillo attorno alla licenza [GPL](https://www.gnu.org/licenses/gpl-3.0.en.html) lo consentisse, ipotesi cui allude McGrath.

Secondo, dal 2019 Red Hat è proprietà di IBM, che pur di averla ha speso trentaquattro miliardi di dollari per cui del valore dovrebbe avercelo trovato. Quando è avvenuto l’acquisto, Red Hat era una azienda autonoma, che faceva soldi con Linux e consentiva tranquillamente i comportamenti che ora Red Hat-Ibm trova anticompetitivi e dannosi per il fatturato.

(Incidentalmente: uno dei post citati comincia con la frase *il mercato Linux per la aziende è grande e lucrativo*. Dedicato a chi capisce un marchio solo e un prodotto solo e vuole pure farlo passare come inevitabile).

Situazione spinosa a dire poco, che irrita un po’ tutti e porta anche conseguenze pratiche: Suse ha annunciato che [effettuerà il fork di una propria versione compatibile con Red Hat Enterprise Linux](https://linuxiac.com/suse-will-fork-red-hat-enterprise-linux/). Leggi: che i clienti scontenti di Red Hat potranno eventualmente adottare senza perdere niente.

Il massimo giunge quando entra in scena Oracle Linux. Con un post dal titolo micidiale, [Teniamo Linux aperto e libero: non possiamo permetterci di non farlo](https://www.oracle.com/news/announcement/blog/keep-linux-open-and-free-2023-07-10/), due massimi responsabili di Oracle Linux raccontano esattamente questa storia, ma in termini molto più crudi e diretti, senza tralasciare il capitolo motivazioni e con l’aggiunta di un dettaglio: i contratti che fa firmare Ibm prevedono la violazione se il cliente usa il proprio abbonamento per fare valere i diritti derivanti dalla licenza GPlv2, quella di Linux.

Fanno presente l’ottimo lavoro finora svolto nell’assicurare la compatibilità della loro versione con Red Hat Enterprise Linux ma avvisano che il nuovo corso potrebbe portare a problemi in questo senso e ribadiscono che binari e sorgenti della loro distribuzione sono stati sempre liberi per chiunque.

Dopo di che invitano gli sviluppatori scontenti a farsi assumere in Oracle, i clienti scontenti - ovviamente – a cambiare fornitore e poi l’apoteosi:

>Una grande idea per voi di IBM. Dite che non volete pagare tutti quegli sviluppatori Red Hat Enterprise Linux? Come risparmiare: “just pull from us”. Diventate distributori downstream di Oracle Linux. Prenderemo lietamente sulle spalle questo carico.

Si ha la sensazione che lungo il ruscello siano in tanti insoddisfatti di Red Hat, sia a monte che a valle. Vedremo chi sporcava davvero.