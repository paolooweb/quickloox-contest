---
title: "Come non resistere alla tentazione (di nuovo)"
date: 2022-11-11T00:13:00+01:00
draft: false
toc: false
comments: true
categories: [Internet]
tags: [Tim, Telecom Italia, SIP, Società Idroelettrica Piemontese]
---
Tim fu Telecom Italia che indietro per li rami fu la SIP, ereditiera dell’acronimo dalla Società Idroelettrica Piemontese per fare tutt’altro: porre le basi storiche e infrastrutturali dell’inadeguatezza della rete italiana, che tuttora è rimasta indietro e non potrà ragionevolmente recuperare per decenni. Un Paese condannato alla zoppia tecnologica da generazioni di inetti, i cui prodotti ho giurato fermamente di evitare da qui all’eternità, non importa a che prezzo.

Tutto ciò ha creato una situazione interessante nel momento in cui la mia via è stata cablata in fibra e di fatto ho una colonnina letteralmente a qualche metro da casa, in linea d’aria.

È troppo facile resistere alle tentazioni lontane; quelle meritevoli sono vicine e circostanziate. Bisogna conoscere il Male per poterlo rifiutare con soddisfazione. Per questo ho accettato le telefonate che stranamente hanno iniziato a susseguirsi sul mio cellulare, di venditori interessati a propormi Tim e la sua fibra *fino a mille mega*.

Ho risposto che volevo dieci gigabit, e che ero interessato al prezzo.

*Il prezzo è uguale*, mi hanno risposto. *Ma allora perché non mi offrite dieci gigabit?*

Mi hanno spiegato pazientemente che, prima di potermeli offrire, dovevano verificare la copertura. Volevano indirizzo e numero di telefono fisso.

Buffo, ho pensato. Cablano ieri, chiamano per coincidenza oggi e si comportano come se non sapessero dove e chi stanno chiamando. (Il nostro fisso è staccato da anni, non ha alcun uso e teniamo pigramente la linea attiva solo perché fa parte dell’abbonamento che copre tutti i bisogni familiari).

Cortesemente ma con fermezza sono arrivato al punto di lasciarci con un nulla di fatto rispetto al mio indirizzo. Dopo di che sono andato io a verificare la copertura, sul loro sito.

Nella mia via portano la fibra-truffa, Tecnologia FTTC/E con EVDSL:

>L'acronimo FTTC sta per Fiber To The Cabinet e rappresenta un tipo di collegamento internet realizzato con cavi in fibra ottica che partono dalla centrale e arrivano fino all’armadio di strada. Il tratto di collegamento che dall’armadio di strada arriva fino a casa tua (solitamente poche centinaia di metri) è realizzato con cavi in rame. L’EVDSL, Enhanced Very-high-bit-rate Digital Subscriber Line, è invece il protocollo che consente la trasmissione dei dati dalla centrale a casa tua.

*FTTC/E* significa probabilmente *fottuto con eleganza*: la fibra arriva nell’armadio di strada e da lì a casa mia, mai fosse, il segnale viaggerebbe sul solito ramaccio buono neanche per cuocerci la polenta.

Ovvero, mi offrono *fino a mille mega* intendendo *cinquanta mega in download quando va bene*.

Uno vorrebbe resistere alla tentazione, ma di tentazione dovrebbe essercene una. Questa somiglia a una punizione per i bambini cattivi, *se non ti comporti bene con il browser viene l’uomo Tim a portarti la fibra!*