---
title: "Si-può-fare!"
date: 2020-04-25
comments: true
tags: [Times, New, Arial]
---
Nutro la ferma convinzione che la tipografia digitale, con il tempo necessario, recupererà il divario con i cinquecento e passa anni di evouzione della tipografia analogica, fino a creare nuovi capolavori che rivaleggeranno in estetica e *design* con quelli di [Aldo Manuzio](https://www.maremagnum.com/vetrina/aldo-manuzio) o [Hermann Zapf](https://www.tdc.org/profiles/hermann-zapf/).

Naturalmente saranno lavori di natura anche fondamentalmente diversa, esattamente come uno schermo bitmap non ha niente a che vedere con un foglio di carta, anche se molte volte lo imita per il nostro quieto vivere.

In questo percorso trovano posto i font variabili, capaci di mutare forma e caratteristiche secondo le intenzioni del compositore.

Altrettanto naturalmente l’evoluzione porterà anche a mostruosità e incidenti di percorso. Esattamente come il [Times New Arial](https://www.inputmag.com/design/times-new-arial-mutates-familiar-fonts-into-something-wholly-new-typeface), stupefacente nel mutare quanto orrendo a vedersi in ogni percentuale di *mix*. Vedremo grandi manifestazioni di genio e talento, nel bene e [nel male](https://youtu.be/rdkecMOT1ko).

Dobbiamo essere fiduciosi che nel tempo le creazioni brutte lasceranno il posto a quelle capaci di durare e testimoniare la grandezza dell’ingegno umano. Hanno fatto pasticci anche nel Rinascimento; semplicemente, li hanno buttati.

<iframe width="560" height="315" src="https://www.youtube.com/embed/rdkecMOT1ko" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
