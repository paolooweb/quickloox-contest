---
title: "La sicumera degli stagisti"
date: 2022-06-17T12:21:21+01:00
draft: false
toc: false
comments: true
categories: [Hardware, Software]
tags: [iPad, iPadOS, Stage Manager, M1, Gruber, John Gruber, Daring Fireball]
---
Lo ricorderemo come l’anno in cui i commentatori so-tutto si sono lamentati allo spasimo che lo hardware di iPad era sprecato per il software, visto che il sistema operativo non lo sfruttava. nel primo semestre.

Nel secondo semestre, Apple ha fatto presente che Stage Manager girerà solo su iPad con M1 o al massimo, se prevarrà il furore di popolo, anche su modelli precedenti, con limitazioni e compromessi, come [spiega meglio di nessun altro John Gruber](https://daringfireball.net/linked/2022/06/15/ipad-stage-manager-m1).

>La controversia ruota attorno all’idea che Apple lo faccia per spingere i proprietari degli iPad più vecchi a comprare quelli nuovi per avere Stage Manager. Non posso provarlo, ma a fiuto non passa il mio test. Non è come si muove Apple.

L’unica altra spiegazione è che, sotto M1, un iPad fatichi abbastanza a sopportare il carico di Stage Manager. Ossia, che il sistema operativo fosse molto meno sottodimensionato rispetto allo hardware di quanto ci abbiano spiegato i so-tutto.

Da rimandare a fare gli stagisti da qualche altra parte. Di sottodimensionato vedo solo la competenza, mentre la chiacchiera si spreca.