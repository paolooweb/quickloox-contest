---
title: "Pensieri non comuni"
date: 2020-02-16
comments: true
tags: [Scandolin, No, Rocket, Science]
---
Subdolamente adescato da [Matteo](https://medium.com/@matteoscandolin), ho scritto il testo di una delle prossime uscite della newsletter [No Rocket Science](https://nrs.substack.com), di cui consiglio la lettura regolare. Esce ogni due lunedì, non pesa e non dà fastidio.

Eppure sollecita, intriga, scuote, coinvolge, suggestiona, dipende dal tema, da chi la scrive, dai link consigliati. Certamente è un’uscita dalla propria *comfort zone*, quali che siano i suoi confini.

È qualcosa di cui, nella routine attuale di contatti con media e persone spesso manovrate dai media, c’è un gran bisogno per continuare a respirare con frequenza non allineata alla portante del pensiero comune.