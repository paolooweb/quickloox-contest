---
title: "La prova privacy"
date: 2017-09-19
comments: true
tags: [iPhone, Federighi, Face, ID, Huawei]
---
Mi tocca stare su Facebook per lavoro e quando ci sono annunci Apple dover stare su Facebook equivale a stare immerso nella fossa biologica intanto che arriva l’autobotte per lo spurgo.<!--more-->

Comunque. Sappiamo da fonti certe (per esempio [Craig Federighi intervistato su TechCrunch](https://techcrunch.com/2017/09/15/interview-apples-craig-federighi-answers-some-burning-questions-about-face-id/)) che i dati acquisiti da Face ID restano sull’apparecchio che li acquisisce. Apple non li riceve, non li vede, non sa quali sono, non li usa, non li tratta, non può leggerli.

È anche noto che sei organizzazioni pubblicitarie [hanno scritto una lettera aperta](http://www.adweek.com/digital/every-major-advertising-group-is-blasting-apple-for-blocking-cookies-in-the-safari-browser/) contro le impostazioni di *privacy* su Safari per iOS 11: il *browser* pone limiti severi al *retargeting* dei navigatori. Ovvero, se hai visitato il sito X, i padroni di X sono ostacolati nella loro attività di bombardarti di annunci pubblicitari su tutti *gli altri* siti che visiti.

Adesso i geni so-tutto che *invece di spendere milleduecento euro in un telefono fatevi un viaggio* possono estrarre il loro Huawei delle meraviglie, che naturalmente fa tutto quanto fa un iPhone a una frazione del prezzo, e mostrarmi che fa anche queste cose.