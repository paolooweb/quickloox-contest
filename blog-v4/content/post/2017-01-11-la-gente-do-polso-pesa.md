---
title: "La gente di polso ha il suo peso"
date: 2017-01-11
comments: true
tags: [watch]
---
Certamente ci vuole la volontà e altrettanto certamente l’apparecchio, da solo, significa niente. Eppure ho apprezzato il [racconto di Zac Hall su 9To5Mac](https://9to5mac.com/2017/01/01/apple-watch-new-years-resolutions-losing-50-pounds/) che è riuscito a perdere più di venti chili da aprile a dicembre 2016 grazie a un proposito del nuovo anno (rotto il quattro di gennaio, ma ripreso successivamente).<!--more-->

L’ho apprezzato perché emerge il ruolo ancillare ma prezioso di watch, che grazie alle sue misurazioni e alle *app* dedicate riesce a essere di supporto autentico. Anche se, appunto, a determinare il cambio sulla bilancia sono la testa, il moto e la dieta.

Vale questo articolo a rispondere a mille domande retoriche sull’utilità di watch. Per quanto mi riguarda non ho bisogno di dieta, ma arrivare a fine giornata e vedere gli anelli della salute al giusto valore fa piacere. E apprezzo molto i solleciti ad alzarsi in piedi durante una giornata di lavoro alla scrivania: è molto facile non pensarci e restare seduti per un tempo eccessivo, quando una pausa anche di pochi minuti è assolutamente salutare fuori da qualunque mania salutista.

Ci starebbe bene uno slogan *cambiare il mondo, un polso per volta*.