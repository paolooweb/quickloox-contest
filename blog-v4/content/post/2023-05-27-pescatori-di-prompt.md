---
title: "Pescatori di prompt"
date: 2023-05-27T15:04:08+01:00
draft: false
toc: false
comments: true
categories: [Internet, Web]
tags: [Paolo Benanti, Copernicani, Benanti, AI, IA, intelligenza artificiale, ChatGPT]
---
Mercoledì 31 maggio 2023 alle 18 avrò il piacere di dialogare in diretta con [padre Paolo Benanti](https://www.paolobenanti.com), Professore Straordinario della [Pontificia Università Gregoriana](https://www.unigre.it/it/docenti/scheda/?id=1654) sui temi generali dell’intelligenza artificiale e delle considerazioni etiche che le ruotano attorno.

Non credo che parleremo molto di tecnica e probabilmente mi interessa abbastanza poco, dato che i principî generali del funzionamento degli attuali modelli testuali generativi sono tutto sommato noti. Invece avere una idea di come la tecnologia cambia il nostro modo di guardare alle cose, così come di guardare a noi stessi e agli altri, può presentare molti risvolti ancora da scoprire.

Il dialogo, organizzato dall’associazione Copernicani, avrà luogo [su Zoom per chi si sentisse partecipativo](https://us02web.zoom.us/j/88432255957?pwd=YUZNUGpPT1dYeTBSZjNObUFxQUV1QT09) e potrà essere [guardato e non toccato sul canale YouTube dei Copernicani](https://www.youtube.com/@Copernicani), per chi temesse di sporcarsi le mani. Sullo stesso canale resterà indefinitamente la registrazione dell’incontro.

Il titolo è *Vi farò pescatori di prompt* ed è in larga parte una provocazione. Ovviamente il dialogo non sarà confessionale; è che, quando mi hanno detto per la prima volta *usi il prompt sbagliato*, mi è venuto da pensare *da chi vengo addestrato, e perché?*. Qui sotto la scheda ufficiale, per i più pignoli.

§§§

**Vi farò pescatori di prompt**

*L’etica prima della tecnica. L’intelligenza umana guarda, giudica e si interroga a proposito di quelle artificiali.*

In dialogo con Padre Paolo Benanti, francescano del Terzo Ordine Regolare di San Francesco, filosofo, teologo e docente presso Pontificia Università Gregoriana. Le sue ricerche si focalizzano sulla gestione e l’etica dell'innovazione: internet e l'impatto del Digital Age, le biotecnologie per il miglioramento umano e la biosicurezza, le neuroscienze e le neurotecnologie, intelligenza artificiale e postumano.

§§§

Se cresce un’ora, ci si può vedere. :-)