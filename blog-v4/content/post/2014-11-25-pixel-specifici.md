---
title: "Pixel specifici"
date: 2014-11-25
comments: true
tags: [iPadminiRetina, Nexus7, paoloo, avariatedeventuali]
---
Ringrazio **avariatedeventuali** e **paoloo** che sono arrivati subito dove [mi ero fermato](https://macintelligence.org/posts/2014-11-24-un-altro-argomento-della-diagonale/) per mancanza di sufficiente pensiero laterale (e forse sonno, ma niente alibi): si arriva agevolmente alla superficie dello schermo di una tavoletta conoscendo il numero di pixel e la densità dei pixel stessi dentro ogni pollice quadrato. Dati reperibili ovunque a differenza delle specifiche fisiche degli schermi.<!--more-->

`pixel base * pixel altezza / pixel per pollice al quadrato * 6,4516`

Viene fuori che un iPad mini Retina ha una superficie di schermo di 190 centimetri quadrati.

Un Nexus 7 totalizza 142 centimetri quadrati.

un iPad mini Retina ha il 34 percento in più di schermo utile. Un terzo del totale! Su un oggetto di piccole dimensioni, considerato che la dimensione dei polpastrelli – e quindi dell’interfaccia che li deve accogliere – è costante, si tratta di un valore notevole, che potrebbe compensare per intero da solo la differenza di prezzo e fa una differenza sostanziale in usabilità.

Il web ribolle di confronti tra iPad mini Retina e Nexus 7. Trovarne uno dove questo piccolo particolare venga evidenziato adeguatamente.

Siamo ai limiti della truffa: se invece di tavolette si confrontassero appartamenti senza far notare la differenza di superficie a disposizione, scatterebbero denunce.

Purtroppo l’informatica continua a restare un campo dove misteriosamente non valgono le leggi della vita reale.