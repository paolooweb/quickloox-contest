---
title: "L’esercito delle mummie"
date: 2022-06-18T01:09:27+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [Microsoft, Internet Explorer, Edge, The Spokesman-Review, Esu, Extended Security Updates, Windows, Windows Server, Windows Embedded, Windows 7, Windows 8.1 Update, Windows 10 2015 LTSB, Windows 10 2016 LTSB, Windows 10 2019 LTSC, Windows Server 2008, Windows Server 2008 IA64 (Itanium), Windows Server 2008 R2, Windows Server 2008 R2 IA64 (Itanium), Windows Server 2012, Windows Server 2012 R2, Windows Server 2016, Windows Server 2019, Windows Server 2022, Windows Embedded Compact 2013, Windows Embedded Standard 7, Windows Embedded POSReady 7, Windows Thin PC, Windows Embedded 8 Standard, Windows 8.1 Industry Update]
---
Tenerezza, non si può provare altro, per chi crede agli articoli di questo periodo sulla falsariga di [Internet Explorer è morto](https://www.spokesman.com/stories/2022/jun/15/microsofts-internet-explorer-is-dead/).

Microsoft può pagare le bugie sui media naturalmente, ma non può spacciare per morto un morto vivente e basta andare sulle sue pagine di supporto per accorgersene, come la [FAQ sul ciclo di vita di Edge e Explorer](https://docs.microsoft.com/en-us/lifecycle/faq/internet-explorer-microsoft-edge#what-is-the-lifecycle-policy-for-internet-explorer-).

Prima di tutto, Edge possiede una modalità Internet Explorer che verrà supportata almeno, dice Microsoft, fino al 2029. D’accordo, si può ribattere, però si tratta comunque di una nuova app, anche se consente di restare eternamente prigionieri dell’arretratezza.

Concesso. Ora però veniamo ai prodotti con *Extended Security Updates*. Microsoft tiene in vita una [serie di prodotti antiquati](https://docs.microsoft.com/en-us/lifecycle/faq/extended-security-updates) per compiacere vari grandi clienti che pensano così di risparmiare denaro. Tali prodotti, essendo antiquati, supportano e permettono di usare Internet Explorer in qualche sua versione.

Parliamo di Windows 7, Windows 8.1 Update, Windows 10 2015 LTSB, Windows 10 2016 LTSB, Windows 10 2019 LTSC, Windows Server 2008, Windows Server 2008 IA64 (Itanium), Windows Server 2008 R2, Windows Server 2008 R2 IA64 (Itanium), Windows Server 2012, Windows Server 2012 R2, Windows Server 2016, Windows Server 2019, Windows Server 2022, Windows Embedded Compact 2013, Windows Embedded Standard 7, Windows Embedded POSReady 7, Windows Thin PC, Windows Embedded 8 Standard, Windows 8.1 Industry Update.

Un esercito di mummie in perpetua conservazione, equivalente a milioni di copie di Internet Explorer che continueranno a dis-funzionare e a creare disagi indipendentemente da ogni titolo di giornale.

Non ce ne libereremo mai. Oppure solo quando ci saremo liberati di Microsoft, che se ci fosse una bella idea in materia sarebbe il caso di iniziare a fare la colletta.