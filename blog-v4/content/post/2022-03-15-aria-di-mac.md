---
title: "Aria di Mac"
date: 2022-03-15T02:11:12+01:00
draft: false
toc: false
comments: true
categories: [Hardware]
tags: [Sabino, Mac, Nasa, Ipcc]
---
[Sabino](https://melabit.wordpress.com) sa che mi piace raccogliere testimonianze di gente che lavora con Mac nei luoghi e nei modi più singolari e mi ha passato due immagini del lavoro di un ricercatore italiano alle prese con materiale proveniente dalla Nasa sull’inquinamento da aerosol in pianura padana.

![Che cos'è un aerosol nell’atmosfera](/images/ipcc.png "Di che cosa parliamo. Immagine cortesia della Nasa.")

![Variazione degli aerosol nella pianura padana a confronto con Bruxelles](/images/aerosol.png "Va un po’ meno peggio.")

C’è chi lavora sulla qualità dell’aria che respiriamo.

Fortunatamente, può pubblicare dati che mostrano quantomeno un miglioramento della situazione (relativamente alla pianura padana).

Vedere il Dock comparire sotto le immagini rende l’atmosfera ancora più respirabile e, c’è poco da fare, ispira un pizzico di fiducia in più.