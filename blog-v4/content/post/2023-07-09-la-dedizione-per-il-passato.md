---
title: "La dedizione per il passato"
date: 2023-07-09T17:42:49+01:00
draft: false
toc: false
comments: true
categories: [Hardware, Software]
tags: [Stefano, Nanchang, Internet Explorer]
---
Una grande civiltà a cui le derive illiberali hanno tolto qualche lucidità, a metà strada tra le tradizioni millenarie e le spinte estreme delle tecnologie più futuristiche.

Lo scatto è di **Stefano** (con un pizzico di postproduzione da parte mia), che ringrazio.

La sua didascalia:

>Il Totem allo Sheraton Hotel di Nanchang.

Naturalmente al capitolo *tradizioni millenarie* si intendeva il browser.

![Il totem in crash allo hotel Sheraton di Nanchang in crash. Tra gli ideogrammi si vede la scritta Internet Explorer](/images/nanchang.jpg "Li fanno ancora i piani quinquennali? Venticinquennali?")