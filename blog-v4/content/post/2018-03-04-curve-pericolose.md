---
title: "Curve pericolose"
date: 2018-03-04
comments: true
tags: [Asymco, Apple, Dediu]
---
Il bravissimo Horace Dediu di Asymco si è appena guadagnato un gran numero di odiatori a causa delle curve che ha tracciato.

La prima [descrive il numero di apparecchi Apple attivi](http://www.asymco.com/2018/02/27/the-number/) e funzionanti. Dalla preistoria a oggi ne sono stati venduti circa due miliardi. Circa 1,3 miliardi sono attivi.

Circa due apparecchi su tre sono attivi.

Da anni *la percentuale sul totale è costante.*

La seconda curva [esprime la vita media di un apparecchio Apple](http://www.asymco.com/2018/03/01/determining-the-average-apple-device-lifespan/).

Circa sedici trimestri, attualmente.

Da anni *è in aumento*. Non da ieri, da un lustro.

Curve pericolosissime perché perfino il *Wall Street Journal* titola [Il tuo amore per il tuo vecchio smartphone è un problema per Apple e Samsung](https://www.wsj.com/articles/your-love-of-your-old-smartphone-is-a-problem-for-apple-and-samsung-1519822801) e sembra una novità. Apple lo sa da cinque anni. Prospera proprio per questo, e non nonostante. Che sia un problema per Samsung è tutt’altra questione e mettere insieme le due storie è indice di incomprensione della realtà.

Curve pericolose anche perché imbarazzanti per chi blatera di obsolescenza programmata. Vita media in aumento, percentuale di attività costante. Per dimostrare l’obsolescenza programmata alla luce di questi dati serve un genio, oppure un ignorante.

Geni non se ne vedono.