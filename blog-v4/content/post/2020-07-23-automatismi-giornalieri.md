---
title: "Automatismi giornalieri"
date: 2020-07-23
comments: true
tags: [Drang]
---
Questa estate avrò imparato che, volendo coltivare la capacità di sviluppare automatismi per le mie macchine, devo tenerla in esercizio esattamente come c’è chi esce a correre tutte le mattine o annaffia l’orchidea con regolarità infallibile.

[Dice bene](https://leancrew.com/all-this/2020/07/more-discontinuous-ranges-in-python/) Dr. Drang:

>Risparmiare tempo non è la ragione principale per costruire un automatismo. La coerenza dei risultati vale almeno quanto il risparmio di tempo. E poi c’è la questione di tenere allenata la propria capacità. Spesso creo un’automazione con lo scopo di imparare una nuova tecnica o esercitarmi con una tecnica vecchia che non uso da tempo.

Doveva visitare alcuni piani di un grosso palazzo residenziale, in base a questi criteri:

* Si parte dal secondo piano.
* Vanno esclusi gli ultimi tre piani di ogni gruppo di otto.
* Il palazzo è di 53 piani.

Bonus:

* Come in molti palazzi di questo tipo negli States, per superstizione manca il piano numero 13.

Lo ha fatto in una riga di Python. Non subito.

>Ho risparmiato del tempo? No, avrei potuto scrivere la lista a mano molto più velocemente. Il vincolo di pensare alla periodicità della struttura del palazzo mi ha dato una maggiore comprensione della struttura stessa. Ed è sempre buona cosa flettere i muscoli della programmazione.