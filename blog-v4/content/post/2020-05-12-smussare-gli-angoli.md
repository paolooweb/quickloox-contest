---
title: "Smussare gli angoli"
date: 2020-05-12
comments: true
tags: [emacs, Stallman, Lwn, BBEdit]
---
Le [polemiche su Immuni](https://macintelligence.org/posts/2020-04-20-ciao-pepp-pt/) sono niente, di fronte alle conversazioni sulla necessità – o meno – di [rivedere l’aspetto estetico di emacs per favorirne la popolarità](https://lwn.net/SubscriberLink/819452/1480c3a59d3d9093/).

Ha preso posizione persino Richard Stallman, ancora figura più che autorevole nella comunità nonostante i [recenti incidenti a base di politicamente corretto](https://macintelligence.org/posts/2019-09-29-la-caduta-di-re-riccardo/).

Onestamente, per avere i pulsanti arrotondati, scelgo tutta la vita [BBEdit](http://bbedit.com). Il senso di [emacs](https://www.emacswiki.org) sta esattamente nell’avere l’interfaccia più minimale che si possa, proprio perché così diventa scatenabile l’intera sua potenza, ovviamente sotto le mani di qualcuno che sa come controllarla.