---
title: "Materiali non di consumo"
date: 2020-09-10
comments: true
tags: [iPad, Pro]
---
Si arriva al 2020 con gente che ancora nutre il mito di iPad come [macchina buona solo per consumare bit](http://ignorethecode.net/blog/2015/08/14/ipad_consumption_device/) e mostra di non seguire Apple Gazette.

Nell'articolo [iPad Pro per i contenuti creativi, all'opposto di un apparecchio per consumo di dati](https://www.applegazette.com/ipad/ipad-pro-for-creative-content-as-opposed-to-a-consumption-device/) si respira aria della nostra epoca. Si può leggere un accenno al fatto che il nuovo iPad Pro è più veloce del novantadue percento dei portatili sul mercato. Si commenta l’interesse di Adobe per portare su iPad Pro le proprie applicazioni più redditizie (non che siano meravigliose le applicazioni di Adobe, ma sono una forza comunque).

Si fa presente alle aziende che, se ragionano ancora secondo i vecchi modelli, sbagliano e in azienda sbagliare può significare danni economici.

Un iPad Pro non serve a tutti, questo è certo. Ad alcuni però serve dannatamente tanto. Altro che per il consumo.