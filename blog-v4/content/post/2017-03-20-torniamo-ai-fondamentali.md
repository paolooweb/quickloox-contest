---
title: "Torniamo ai fondamentali"
date: 2017-03-20
comments: true
tags: [ Google, Guetzli, Jpeg, Homebrew, Png, Zopfli]
---
Si fa sempre un gran parlare di hardware eppure dovrebbe fare grande, grandissima impressione l’annuncio da part di Google di [Guetzli](https://research.googleblog.com/2017/03/announcing-guetzli-new-open-source-jpeg.html), nuovo algoritmo di compressione Jpeg che arriva a comprimere le immagini anche del 35 percento in più mantenendo la stessa qualità visiva degli algoritmi tradizionali, oppure aumentando la qualità a parità di compressione.

Fa impressione perché da lungo tempo pare terminato il dibattito sui fondamentali dell’informatica. Nascono nuovi *social*, nuove app, nuove architetture e linguaggi della programmazione, ma si è portati a pensare che su alcuni temi, la compressione per esempio, resti spazio per miglioramenti nulli o residuali al più.

Guetzli mostra in modo clamoroso che è falso. Non sono noccioline: proponi a un webmaster di ridurre di un terzo il peso delle immagini Jpeg dentro il server di un sito e bacerà il terreno dove cammini. La compressione Jpeg esiste dalla notte dei tempi digitali e non è esattamente un terreno dove sia mancata la ricerca.

I curiosi possono installare Guetzli tramite Homebrew e fare prove a volontà. Da notare il fatto che Google ha compiuto progressi anche con i file Png (Zopfli), solo meno eclatanti.

Unica controindicazione di Guetzli: lento. Per lavorare in tempo reale non va bene. Ma se c’è tempo, è una mezza rivoluzione. Il trentacinque percento, a essere precisi.
