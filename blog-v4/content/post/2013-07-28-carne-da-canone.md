---
title: "Carne da canone"
date: 2013-07-28
comments: true
tags: [iOS]
---
La *app* [Rai.Tv](https://itunes.apple.com/it/app/rai.tv/id501323740?l=en&mt=8) si è aggiornata il 24 luglio 2013 per fornire supporto a iOS oltre la versione 5.0.

La prima versione importante di iOS successiva alla quinta, ossia iOS 6, è uscita se non erro il 12 settembre 2012.

Vuol dire che posso aspettare l’anno prossimo a pagare il canone di quest’anno?