---
title: "Chi l’avrebbe mai visto"
date: 2014-06-19
comments: true
tags: [Ippa, iPhone]
---
Non sapevo davvero che esistesse un premio ufficiale e non locale per le [migliori fotografie scattate con iPhone](https://www.ippawards.com/index.html).

E c’è dal 2008, non tutti hanno sottovalutato la novità a suo tempo.