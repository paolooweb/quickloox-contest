---
title: "What a Wonderful Teen World"
date: 2021-06-02T00:11:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [Wwdc, Swift] 
---
Non solo [Lucy](https://macintelligence.org/posts/What-a-Wonderful-World.html).

Apple ha annunciato [chi ha vinto la Swift Student Challenge 2021](https://www.apple.com/newsroom/2021/06/apples-wwdc21-swift-student-challenge-winners-code-to-change-the-world/), legata all’imminente [Worldwide Developers Conference](https://developer.apple.com/wwdc21/).

Senza entrare nel merito, alla competizione prendono parte centinaia di studenti da tutto il mondo, ragazzi e ragazze che non hanno ancora la maggiore età ma si mettono in gioco, imparano un linguaggio di programmazione, inventano soluzioni a problemi reali, ci provano davvero.

Sono, o almeno somigliano a, quelle persone che nello spot [Think Different](https://www.youtube.com/watch?v=AicY5iKaJFM), pazze a sufficienza da voler cambiare il mondo, poi lo cambiano davvero.

Forse è solo invidia. Mi devo muovere in un mondo nel quale mi capita a ogni angolo qualcuno che mi spiega gli eccessi della tecnologia, come difendere i bambini dalle insidie dei cellulari, la necessità di spegnere, questo, quello, tutto.

Tra i due eccessi, preferisco una quindicenne che usa Swift a partire da due genitori immunodepressi per creare un servizio capace di aiutare tanti. Ed è solo un esempio. Ragazzi, difendetevi dai pericoli della tecnologia: programmatela. Sapete fare cose meravigliose.

<iframe width="560" height="315" src="https://www.youtube.com/embed/AicY5iKaJFM" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

*Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile.*               