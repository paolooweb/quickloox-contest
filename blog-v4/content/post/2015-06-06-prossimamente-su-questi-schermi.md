---
title: "Prossimamente su questi schermi"
date: 2015-06-06
comments: true
tags: [Apple, AppleTv, iPhone]
---
Butti un occhio al [Digital Video Report di Adobe per il primo trimestre del 2015](http://www.slideshare.net/adobe/adi-digital-videoq12015).<!--more-->

Scopri come un video che inizia a svolgersi su un apparecchio *mobile*, lo faccia quattro volte su cinque su un apparecchio Apple. Come gli apparecchi Apple siano responsabili dell’apertura di un video su quattro, globalmente. Come, quando si parla di *TV Everywhere*, tre volte su cinque ci sia di mezzo un apparecchio Apple, spesso una Apple TV.

Sono cifre incompatibili con la presenza fisica dei suddetti apparecchi sul mercato. A meno di riconoscere che il pubblico compra cose Apple e poi, per motivi ignoti, le *usa* dove invece altre cose vengano acquistate unicamente per dire di aver speso poco, o all’incirca.

È un’altra delle sfaccettature che mostra perché Apple sia diversa dalle aziende sue simili e perché valga la pena di essere seguita. L’uomo della strada è convinto che Apple fabbrichi gli iPhone. Intanto è un colosso mondiale del video e nessuno ci fa caso.

<iframe src="//www.slideshare.net/slideshow/embed_code/key/M5twdlC1QwJ3P" width="425" height="355" frameborder="0" marginwidth="0" marginheight="0" scrolling="no" style="border:1px solid #CCC; border-width:1px; margin-bottom:5px; max-width: 100%;" allowfullscreen> </iframe>