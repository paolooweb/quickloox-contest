---
title: "Recita a soggetto"
date: 2013-09-28
comments: true
tags: [iPhone, iPad, iOS]
---
Si chiedono [su Slashdot](http://ask.slashdot.org/story/13/09/22/019251/ask-slashdot-is-ios-7-slow):

>iOS 7 è più lento della versione 6? Dopo avere aggiornato, io e vari altri notiamo un funzionamento a scatti sugli scorrimenti, sul cambio delle app e così via. È comune?

Ho aggiornato iPhone e iPad quando me lo hanno chiesto loro, cioè ieri. Come succede spesso, la configurazione post-aggiornamento è diversa da quella che usavo. Per esempio ho notato subito che Bluetooth era acceso, mentre io lo tengo sempre spento.<!--more-->

Prima di verificare tutto e reimpostare le cose come voglio, la primissima impressione è che iOS 7 abbia avuto un effetto miracoloso su iPad di terza generazione. Che semmai andava a scatti prima, rispetto a ora. Tutto scorre meravigliosamente, una gioia per gli occhi. Il consumo di batteria è sensibilmente ridotto rispetto a prima.

Su iPhone 5, invece, la batteria è andata disastrosamente rispetto a prima. In circostanze dove arrivava fino a notte, era esaurita alle otto si sera.

Il che mi suggerisce – una installazione fantastica, una deludente, prima di metterci mano – che le osservazioni personali siano appunto tali e sia perfettamente inutile considerarle generalizzate. Probabilmente agli antipodi del mondo c’è un neozelandese con gli stessi apparecchi che io e risultati completamente opposti.