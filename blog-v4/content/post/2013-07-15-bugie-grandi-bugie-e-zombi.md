---
title: "Bugie, grandi bugie e zombi"
date: 2013-07-15
comments: true
tags: [iOS]
---
La BBC fu un bastione della libertà. Poi divenne sinonimo di informazione imparziale e obiettiva. Oggi è un carrozzone statale alla deriva deontologica.<!--more-->

Almeno rispetto all’informatica, che continua a essere trattata con criteri alieni al resto del mondo osservabile e abitabile. E così, all’indomani del quinto anniversario di App Store, il negozio *online* più affollato del mondo, la *Beeb* afferma che lo sarebbe sì, ma [di zombi](http://www.bbc.co.uk/news/technology-23240971), per come la mettono quelli di [Adeven](http://www.adeven.com):

>579.001 app su un totale di 888.856 app nel nostro database sono zombi […] app che non compaiono mai nelle migliaia di classifiche pubblicate da Apple e monitorate su base quotidiana.

BBC mette questa affermazione a confronto con quanto dichiarato da Tim Cook, amministratore delegato di Apple, durante il [discorso di apertura del convegno mondiale dei programmatori](http://www.apple.com/apple-events/june-2013/), per cui il 90 percento delle *app* viene scaricato almeno una volta al mese. E lo fa senza neanche accorgersi che non c’è contraddizione, come spiegano ad Adeven:

>[Le app zombi] possono venire scaricate comunque un paio di volte al giorno – magari anche cento – ma non fanno abbastanza per convincere un programmatore a scommetterci sopra o trarne motivazione per supportarle.

È proprio così? Se una *app* non appare in una qualche classifica è come se non esistesse? *PCMag* ha pubblicato l’opinione di Sascha Segan, che fa lo sviluppatore e ha titolato [Io sono uno zombi di App Store](http://www.pcmag.com/article2/0,2817,2421553,00.asp), dichiarandosi anche *felice di esserlo*:

>La mia guida di viaggio per il Mobile World Congress 2013 di Barcellona [disponibile solo [sull’App Store americano](https://itunes.apple.com/us/app/pcmags-mwc-barcelona-guide/id600595288?ls=1&mt=8)] è stata scaricata solo qualche migliaio di volte, in parte perché non mi sono premurato di promuoverla sul posto. Ma la gente è venuta a dirmi di persona quanto l’aveva trovata utile e la app ha raggiunto il nostro obiettivo come primo tentativo di realizzare una app dedicata a un evento.

Continua Segan:

>Sono anche celiaco e sul mio iPhone tengo una manciata di app che aiutano a mangiare cibi privi di glutine. I celiaci sono una piccola minoranza della popolazione e così non mi aspetto che queste app possano diventare campioni di vendite. Tuttavia sono salvavita – spesso alla lettera – per i pochi che soffrono di questa condizione.

E ne ha ancora:

>L’isoletta dove vado in vacanza ha un quotidiano locale, pubblicato online come app. Non viene scaricato molte volte, ma è un modo davvero comodo di leggere le notizie del posto.

Tre esempi, non uno, da chi lavora nel campo e chiarisce come stiano le cose:

>Il successo di App Store richiede entrambe le metà dell’operazione: l’insieme relativamente ridotto di app di cassetta e la coda lunga che soddisfa ciascuna nicchia. Ecco che cosa lo studio sulle app zombi non ha colto e gli altri negozi più piccoli faticano ad avere.

Neppure BBC lo ha capito. E Adeven? La storia degli zombi l’hanno tirata fuori [un anno fa](http://www.apptrace.com/blog/2012-08-06/inside-zombie-land). Nel frattempo continuano a svolgere il loro lavoro di *venditori di strumenti di monitoraggio dell’andamento delle app*.

Traduco: quelli che si propongono a chi produce *app* raccontano che tante *app* hanno bisogno dei loro strumenti. Ma che caso.