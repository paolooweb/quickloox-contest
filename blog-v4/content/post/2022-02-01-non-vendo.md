---
title: "Non vendo"
date: 2022-02-01T01:46:02+01:00
draft: false
toc: false
comments: true
categories:
tags:
---
[Microsoft inghiotte Activision Blizzard](https://news.microsoft.com/2022/01/18/microsoft-to-acquire-activision-blizzard-to-bring-the-joy-and-community-of-gaming-to-everyone-across-every-device/).

[Sony si appropria di Bungie](https://www.gamesindustry.biz/articles/2022-01-31-sony-buying-bungie-for-usd3-6-billion).

Ma più di tutto, [il New York Times si prende Wordle](https://www.nytimes.com/2022/01/31/business/media/new-york-times-wordle.html).

Non saprei che cosa, ma si sappia comunque che non è in vendita.
