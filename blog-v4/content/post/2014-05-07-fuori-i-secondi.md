---
title: "Fuori i secondi"
date: 2014-05-07
comments: true
tags: [Apple, Amazon, eCommerce]
---
Probabilmente da diverso tempo Apple è [il venditore online numero due al mondo](http://blogs.wsj.com/corporate-intelligence/2014/05/06/apple-jumps-in-rankings-now-second-largest-online-seller/). Solo adesso, tuttavia, si è riconosciuto il valore (monetario) della vendita di software e musica digitale da parte di chi compila la classifica.<!--more-->

Al primo posto, detto per completezza, sta l’irraggiungibile Amazon.

C’è da riflettere, sull’una e sull’altra, questa partita dal nulla e quella risorta da una crisi gravissima più o meno nello stesso periodo e oggi dominanti. Strategia e organizzazione pagano.