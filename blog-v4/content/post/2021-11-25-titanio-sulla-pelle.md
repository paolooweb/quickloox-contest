---
title: "Titanio sulla pelle"
date: 2021-11-25T00:37:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [Apple Watch, Apple Watch Series 7, Apple Pay] 
---
È arrivato [watch Series 7](https://www.apple.com/it/watch/), ordinato un mesetto dopo l’annuncio ufficiale.

I video e le celebrazioni degli *unboxing* non sono esattamente il mio genere, ma questa avrebbe potuto essere un’eccezione. La confezione è rigorosamente riciclabile eppure bellissima, per ingegnosità di ingegnerizzazione e cura dei particolari. Un incastro alla volta, piega dopo piega, la sensazione è disvelare un mistero, arrivare a un tesoro. Nessuno che conosco pagherebbe un euro in più esplicitamente per godersi l’apertura di una confenzione; analogamente, chiunque conosca si godrebbe una apertura come quella che mi sono goduto io. Sembra di essere speciali, anche se quella confezione esiste in milioni e milioni di copie tutte uguali.

Vengo dall’acquisto direttamente in Apple Store di un watch di prima generazione, numerosi anni fa (morto nel momento in cui lo schermo si è scollato dalla cassa e l’unità si è divisa in due), e non avevo idea di che cosa avrei trovato aprendo la scatola, che stavolta è arrivata a casa previo ordine online. Le istruzioni per il primo avvio sono discrete ed eleganti, semplicissime e quasi più una coccola per il nuovo proprietario, dato che in realtà non c’è questo gran bisogno, a parte il suggerimento di apparentare watch con iPhone, non scontato per una persona che potrebbe benissimo non saperlo.

L’apparentamento non è velocissimo e ha richiesto diversi minuti; a parte questo, tutto è filato perfettamente liscio.

Ho preso un modello da quarantacinque millimetri. L’aumento delle dimensioni fisiche, due millimetri, è naturalmente impercettibile; abbinato però al ridursi del margine attorno agli schermi, fa una differenza sostanziale nella resa dello schermo durante le interazioni. Mentre sul modello precedente l’agire con le dita chiedeva sempre precisione e concentrazione, ora la situazione è molto più rilassata. I pulsanti sono leggermente più grandi e la differenza, per quanto minima, si avverte decisivamente. Ho usato la scrittura a mano sullo schermo, un carattere per volta, per inserire una password da ventisei caratteri; ci sono riuscito al secondo tentativo. La difficoltà maggiore è stata fare capire al software che la prima lettera era una maiuscola e poi tutto è stato agevole. Sul vecchio watch avrei dovuto usare molta più pazienza e ci avrei comunque messo più tempo, con più stress.

Dentro la confezione si trova comunque un [cinturino Sport](https://www.apple.com/it/shop/product/MKU83ZM/A/cinturino-sport-mezzanotte-41-mm-regular), cosa che ignoravo; avevo infatti scelto un [cinturino a maglie in pelle](https://www.apple.com/it/shop/product/ML7R3ZM/A/cinturino-a-maglie-in-pelle-color-mezzanotte-41-mm-s-m), da alternare a [quello a maglia metallica](https://www.apple.com/it/shop/product/MUHK2ZM/A/bracciale-a-maglie-nero-siderale-38-mm) ereditato dal vecchio watch. La compatibilità dei nuovi orologi con i vecchi cinturini è un tocco che apprezzo.

(Possiedo cinturini costosi perché entrambe le volte ho comprato watch in anni fiscalmente – e ahimé eccezionalmente – molto leggeri, così ho potuto permettermi degli extra budget).

Il cinturino a maglia metallica si è dimostrato a suo tempo un’ottima scelta, che solo d’estate faceva un po’ sudare il polso. Resistentissimo, ha attraversato il mare, la pioggia, il sudore dell’esercizio fisico senza fiatare. Ho scelto pelle stavolta perché la maglia metallica la avevo già e per avere possibilmente qualcosa di più leggero e meno impegnativo sul polso per quando arriveranno i mesi caldi.

Al momento, posso solo dire che il nuovo cinturino in pelle è straordinario. Leggero, veste bene, la chiusura magnetica è impeccabile e la sovrapposizione della parte di cinturino in abbondanza praticamente non si vede. Gran comfort, gran comodità; vedremo se la durata sarà all’altezza, ma in queste poche ora indossarlo è stato un piacere tranquillo.

Del cinturino modello base posso solo dire che al tatto la gomma siliconica dà un feeling piacevole; non l’ho provato. Sto pensando di usarlo per lo sport, così da salvaguardare quello in pelle.

Avendo avuto un watch precedente, mi sono ritrovato un sistema aggiornato e, dove le funzioni sono rimaste quelle, già configurato allo stesso modo. Quindi ho interagito poco con le app installate. I quadranti e le complicazioni sono in numero ben più alto che una volta e ora si possono anche condividere con altri; senza guardare troppo, ho scelto un quadrante abbastanza simile a quello che usavo (digitale, con più informazioni modulari) e per il resto ho semplicemente indossato l’orologio.

Mi sono ritrovato nell’atmosfera che ricordavo, piacevole e produttiva perché watch per me era una splendida macchina per la gestione a misura delle modifiche, filtrate più che efficacemente rispetto a quanto arriva su iPhone. Quando si polemizzava sulla sua utilità o presunta tale, la risposta la avevo e molto chiara: watch consente di lasciare il telefono in tasca e arriva sempre un momento in cui questo assume un valore importante.

Lato salute, riprendo il monitoraggio cardiaco con soddisfazione e senza stare a guardarlo; sapere che c’è è una tranquillità in più (negli ultimi dieci anni ho avuto esami sempre perfetti, ma tecnicamente sono un cardiopatico). Non so se ho voglia di provare la funzione di elettrocardiogramma senza averne bisogno e penso che la lascerò dove sta, almeno per adesso.

Ben diverso è il discorso sul moto e lo stile di vita: per un sedentario sostanziale, non importa quanto sia lo sport che si riesce a inserire, watch si inserisce con discrezione e diventa un ausilio vero. Lo avevo già scritto, sembra puerile affidarsi a una macchina che ti ricorda di alzarti dalla sedia di lavoro o invita a completare i cerchi delle attività quotidiana. In questo scorcio di serata, watch mi ha ricordato per tre volte di alzarmi. Disciplina che, assorbito dal lavoro o da qualsiasi altra cosa, finisce sempre per saltare se non c’è un richiamo.

Ho diritto, come nuovo compratore, a un periodo gratuito di Fitness+ e darà un’occhiata. Non è il mio genere (deformato dallo sport di squadra, detesto fare cose da solo) e più che altro sono curioso di capire se il servizio è così buono da piacere persino a me, o se sia una normale routine come altre che non è difficile trovare su tanti canali televisivi. Comunque non rinnoverò l’abbonamento.

L’alimentatore ora ha un connettore Usb-C e mi viene per questo più facile tenerlo permanentemente attaccato a Mac mini. Sulla batteria e sulla sua durata, posso solo aspettare una messa alla prova; ho accumulato circa sei ore di funzionamento anonimo e certo non posso tirare conclusioni.

L’impressione generale è positiva; dopo qualche mese di assenza, è tornato un collaboratore fidato e discreto. Nei primi momenti l’*always on* dello schermo mi ha un po’ disorientato; ero abituato a un orologio che si accendeva per dirmi cose e vederlo acceso con la coda dell’occhio mi è costato diversi falsi allarmi. Ora è già invalsa la nuova abitudine ed è come se lo schermo fosse stato sempre acceso.

Ah, sono stato molto lieto di configurare Pay. Quando avevo il vecchio watch non c’era una pandemia, non si girava con la mascherina e si poteva pagare con il telefono usando TouchID o FaceID, senza bisogno di digitare il codice di sblocco; ora dovrei poter pagare un caffè porgendo il polso al Pos, senza bisogno di altro. Ed è una soddisfazione aggiuntiva benvenuta, in attesa del momento in cui le mascherine diventeranno un ricordo.

Per ora è tutto.

_Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile._