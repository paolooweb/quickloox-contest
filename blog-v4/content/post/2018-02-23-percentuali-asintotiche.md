---
title: "Percentuali asintotiche"
date: 2018-02-23
comments: true
tags: [Slack, BBEdit]
---
Mi ritrovo spesso a inserire file di testo dentro Slack per metterli a disposizione di collaboratori e committenti. Il cento percento di essi è stato finora scritto su BBEdit e poi trascinato sulla finestra di Slack.<!--more-->

La percentuale si è spostata dalla cifra tonda nel momento in cui ho iniziato a scrivere uno di questi file direttamente nell’editor interno di Slack.

Alla fine del secondo paragrafo, senza preavviso, l’applicazione (su Mac) ha effettuato una ricarica della finestra generale del programma, sopra la quale appare l’editor. Al termine dell’operazione i miei due paragrafi si erano vaporizzati.

Non potrò più dire che scrivo il cento percento dei testi su BBEdit, ma certamente lavorerò per tendere asintoticamente alla stessa cifra. Mai più composizione diretta dentro Slack.