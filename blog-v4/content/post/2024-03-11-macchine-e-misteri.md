---
title: "Macchine e misteri"
date: 2024-03-11T01:11:08+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [Steam, Sixty Four]
---
È una situazione nella quale mi spiace che il *gaming* avvenga solo su iPad, perché [la recensione di Sixty Four pubblicata da Ars Technica](https://arstechnica.com/gaming/2024/03/sixty-four-is-an-idle-clicker-a-factory-sim-and-a-dark-extractive-journey/) mi ingolosisce. E però il gioco funziona solo su Steam, per Windows e Mac.

>Che cosa sto costruendo e perché? Non è chiaro. *Sixty Four* inizia in uno spazio bianco e vuoto, con una singola macchina, un Extracting Channel. Se lo premo, dalla superficie inizieranno a emergere grandi cubi neri. Dopo molti clic su un cubo, questo si fraziona in sessantaquattro  cubi più piccoli, per poi brillare e raccogliersi nel mio inventario. Dopo avere raccolto abbastanza cubi (Charonite), posso costruire macchine per estrarre cubi più in fretta e romperli più facilmente. A un certo punto si riesce a tenere l’estrattore premuto tramite una pompa a pressione e fare in modo che un Entropy resonator clicchi sui cubi al posto mio.

Il nostro lavoro, da cliccacubi, diventa costruttore di macchine e poi ottimizzatore. *Allestiamo macchine che fanno cose, macchine che migliorano il lavoro di altre macchine, macchine che alimentano quelle che migliorano le altre macchine, macchine che aiutano a recuperare più risorse da macchine che più tardi dismettiamo*.

Lo schermo si copre gradualmente di macchine, reazioni e interazioni, in vista isometrica e grafica che rimanda a giochi tipo *SimCity 3000*, mentre gli elementi del gioco si moltiplicano e mantenere la comprensione dello schema generale si fa sempre più impegnativa. Questa la conclusione della recensione:

>Sono a otto ore di gioco. Da quando ho iniziato a scrivere, ho alimentato le macchine per un’ora. Ora mi servono trentadue Hell Gem da passare allo Hell Gem Injector, che aumenta la loro frequenza di ritrovamento. Recuperate centoventotto Hell Gem, posso azionare un Excavating Channel, da cui ricaverò Elmerine e Qanetite, utili per alimentare le attrezzature più piccole.

Al momento non è chiaro quando il gioco finisca e come, persino *se* finisca; se lo lasciamo in *background*, le macchine continuano le loro azioni e il flusso di trasformazione si mantiene attivo.

Se giocassi su Mac probabilmente avrei Steam e, su Steam, mi costerebbe sei dollari curiosare dentro *Sixty Four*. Lo farei.