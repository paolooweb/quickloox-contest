---
title: "Il futuro del portatile"
date: 2015-11-11
comments: true
tags: [iPad, Pro, Mac, Gruber, Daring, Fireball, x86, mouse]
---
Come di consueto, la recensione di iPad Pro da leggere è [quella di John Gruber su Daring Fireball](http://daringfireball.net/2015/11/the_ipad_pro).<!--more-->

A qualcuno suonerà strana, perché parla molto relativamente dell’apparecchio in sé e invece va diritta al punto: fino a quanto un iPad Pro possa sostituire un Mac e quali rischi o opportunità si aprano in questa prospettiva.

Gli uni e le altre emergono con precisione. Da una parte la potenza e la versatilità di un apparecchio che ha niente da invidiare ai suoi colleghi con la tastiera attaccata; dall’altra, i limiti e le imperfezioni di un sistema che, nell’avvicinarsi al comportamento di un portatile, deve imparare ancora molto nell’interazione con la tastiera.

Due i momenti che da soli valgono il prezzo del biglietto: quando si lascia intendere che iPad Pro non sia il punto di arrivo della linea, piuttosto un punto di *partenza*. E la conclusione:

>Non mi dà gioia osservarlo, ma il futuro dell’informatica portatile di massa non prevede un mouse né un processore x86.

Al posto di *non mi dà gioia* metterei personalmente *non mi crea problemi*. E firmerei il resto.