---
title: "Attorno all’interfaccia"
date: 2017-08-16
comments: true
tags: [Nintendo, Switch]
---
Macintosh resterà nella storia primariamente per il suo contributo all’evoluzione dell’interfaccia utente e va ricordato anche più di trent’anni dopo, quando ci si si trova con una miriade di apparecchi a disposizione tra i quali Nintendo Switch.<!--more-->

L’interfaccia del quale, ci raccontano, [è proprio ben fatta](https://medium.freecodecamp.org/thoughts-on-the-nintendo-switch-user-interface-b441129f063d). Ma la cosa più preziosa è leggere dei criteri in base a cui lo si può dire; e capire le sfide che hanno davanti i progettisti, nel creare apparecchi che devono essere immediati, accattivanti, semplici, completi e con un cammino di crescita davanti che deve essere il più sano possibile. Tutto quanto, insieme.

L’utilizzatore medio si rende conto sì e no di un ventesimo di queste difficoltà e ciò rende ancora più valida la lettura dell’articolo.

Personalmente ne sono uscito, tra l’altro, con un’idea ben precisa nei confronti di chi produce computer con tastiera fisica e schermo touch. Non sta facendo la cosa giusta e tante ragioni per cui questo è vero si ritrovano persino nel Nintendo Switch.