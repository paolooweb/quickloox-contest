---
title: "Il grande reset"
date: 2021-10-29T00:40:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [Apple, MacJournals, Cnbc, Tim Cook, World Economic Forum] 
---
Normalmente seguo le presentazioni dei risultati finanziari di Apple attraverso i tweet di [MacJournals](https://twitter.com/macjournals).

Per cominciare, l’attuale scarsità globale di chip è costata ad Apple [sei miliardi di dollari di mancate vendite](https://twitter.com/macjournals/status/1453830076546654212).

<blockquote class="twitter-tweet"><p lang="en" dir="ltr">TC: Supply constraints had about $6 billion in effect (meaning, apparently, that Apple would have sold $6 billion more in hardware had chips been available).</p>&mdash; MacJournals.com (@macjournals) <a href="https://twitter.com/macjournals/status/1453830076546654212?ref_src=twsrc%5Etfw">October 28, 2021</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

Va notato che il problema colpisce tutti; anzi, Apple se l’è finora cavata molto bene rispetto all’entità del problema.

Nonostante questo, per alcuni [i risultati sono deludenti](https://twitter.com/macjournals/status/1453830148458008576).

<blockquote class="twitter-tweet"><p lang="en" dir="ltr">FYI: CNBC Chyron reads “APPLE EARNINGS DISAPPOINT”</p>&mdash; MacJournals.com (@macjournals) <a href="https://twitter.com/macjournals/status/1453830148458008576?ref_src=twsrc%5Etfw">October 28, 2021</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

Ma non hai sentito, che mancano sei miliardi? Non sono andati alla concorrenza, non ci sono prodotti invenduti: anno su anno, [tutte le linee sono in crescita](https://twitter.com/macjournals/status/1453829308800245764).

<blockquote class="twitter-tweet"><p lang="en" dir="ltr">(Deleted version that said year-over-year instead of consecutively)<br><br>Year-over-year, everything rose. iPhone 47%, iPad 21%, Mac 2%, Wearables/Home/Accessories 12%, and Services 26%.</p>&mdash; MacJournals.com (@macjournals) <a href="https://twitter.com/macjournals/status/1453829308800245764?ref_src=twsrc%5Etfw">October 28, 2021</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

A parte Mac, sono anche crescite sostenute.

Il meglio deve ancora arrivare: Apple avrebbe registrato [un *miss*, bersaglio mancato, sul fatturato](https://twitter.com/macjournals/status/1453831728230019074).

<blockquote class="twitter-tweet"><p lang="en" dir="ltr">The chyron on <a href="https://twitter.com/CNBC?ref_src=twsrc%5Etfw">@CNBC</a> reads “APPLE FALLS ON RESULTS / REPORTS REVENUE MISS.” This remains untrue: Apple did not give revenue guidance, so it could not have missed its target.<br><br>Revenues came in under the estimates of analysts, who have far less information than Apple does.</p>&mdash; MacJournals.com (@macjournals) <a href="https://twitter.com/macjournals/status/1453831728230019074?ref_src=twsrc%5Etfw">October 28, 2021</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

Se non fosse che, tre mesi fa, Apple *non ha annunciato obiettivi di vendita*. Quindi non può averli conseguiti, né mancati; non ce n’erano. Si parla degli obiettivi *degli analisti*, che hanno effettuato un esercizio di stile, stimando tetti di vendita a loro capriccio sulla base di informazioni assai minori e incomplete rispetto a quelle di cui chiaramente dispone Apple, sulla propria attività.

Il grande reset che auspico è ben più piccolo di quello [pensato dal World Economic Forum](https://www.weforum.org/great-reset/). Mi accontenterei, effettivamente, di resettare le voci che parlano in dispregio alla logica, al buon senso, alla consequenzialità, all’informazione preventiva.

Mica farli tacere, eh? Resettare. Ripartire da zero. Anche buttando numeri a caso se necessario. Ma almeno che sia chiaro ed evidente a tutti.

*Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile.*