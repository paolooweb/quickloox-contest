---
title: "Circoli virtuosi"
date: 2018-02-19
comments: true
tags: [ Apple, Park, Infinite, Loop]
---
Apple non cambiava indirizzo ufficiale da un quarto di secolo e *9to5Mac* riporta che [i documenti iniziano a puntare](https://9to5mac.com/2018/02/16/apple-new-campus-corporate-address-one-apple-park-way/) a One Apple Park Way invece di One Infinite Loop.<!--more-->

Mi dispiace un po’ perdere l’Infinite Loop che sapeva di intelligenza artificiale forte, ricerche alle frontiere del linguaggio, ricorsività.

Anche se, di fatto, la *spaceship* di Apple Park è molto più *loop* del vialetto attorno agli edifici della – adesso – vecchia sede in cui rimise piede Steve Jobs per la sua seconda venuta.
