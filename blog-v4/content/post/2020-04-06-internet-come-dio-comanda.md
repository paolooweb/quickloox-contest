---
title: "Internet come Dio comanda"
date: 2020-04-06
comments: true
tags: [Topolsky, telnet, iPhone]
---
Ho toccato il tema tangenzialmente un paio di volte e sono contento che Joshua Topolsky lo abbia preso di petto nello scrivere [Grazie al cielo c’è Internet](https://www.inputmag.com/culture/thank-god-for-the-internet).

>Non è solo per i servizi o per le comodità che porta. Internet, è tutto ciò che ne utilizza le condutture, è veramente diventato la linea di contatto con il resto dell’umanità. Non in modo astratto. Nella realtà.

C’è tutto di quello che avrei scritto volentieri, naturalmente a un livello ben superiore. Sua figlia di sei anni che impara a disegnare elefanti, la collaborazione, l’umorismo, i confronti, gli scambi, anche i film o i videogiochi come è ovvio.

E poi quella sensazione che tutti abbiamo provato al momento del primo impatto:

>Avevo dodici anni quando mi sono collegato per la prima volta a quella che avrebbero chiamato Internet. Non c’erano siti, ecommerce, banner pubblicitari, tracciamento della navigazione, spyware. Non c’erano gli iPhone; le app si chiamavano *programmi* e sul mio Pc stava un monitor Ega a sedici colori. Ma la prima volta che ho fatto un telnet in una chat o parlato con sconosciuti dall’Australia, ho provato la stessa sensazione di ora. Questa forza dispiegata dalle persone che viaggia nel rame e sulla luce e ci permette di fare nuove connessioni. Connessioni di cui abbiamo più che mai bisogno.

Non mancherà mai il lato negativo o deteriore della rete, che è il nostro specchio. Al tempo stesso la rete colma distanze prima infinite e, certo, può portare alla luce il peggio di noi, ma anche il meglio, e l’inaspettato.

>Siamo qui insieme, non sappiamo per quanto. Ma non siamo soli. Non più.

Il momento più difficile mette in luce Internet come la vorremmo sempre, come è stata pensata dai visionari della tecnologia. Per quello che può dare veramente.
