---
title: "Premio competenza"
date: 2013-09-23
comments: true
tags: [Nokia]
---
Stephen Elop, che aveva lasciato Microsoft per Nokia presumibilmente previo incasso di una liquidazione, ha amministrato l’azienda finlandese per due anni provocando tipo tre miliardi di dollari di perdite.<!--more-->

Microsoft ha acquisito tutte le attività significative di Nokia, decretandone più o meno l’irrilevanza.

Dopo l‘eccellente lavoro svolto, Elop è tornato in Microsoft. Con [25 milioni di dollari di buonuscita](http://www.siasat.com/english/news/nokias-ceo-elop-get-25-mln-dollar-pay-move-microsoft).

Roba che non riesce neanche ai nostri boiardi di Stato.