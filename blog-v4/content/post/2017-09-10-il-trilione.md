---
title: "Il trilione"
date: 2017-09-10
comments: true
tags: [iPhone]
---
In dieci anni iPhone ha venduto circa 1,2 miliardi di esemplari e va bene, è una cifra incredibile.<!--more-->

La notizia veramente sconvolgente è che molto probabilmente, qualsiasi modello venga [presentato tra due giorni](https://www.apple.com/apple-events/september-2017/), sarà lui a fare toccare l’anno prossimo i [mille miliardi di fatturato](https://www.appleworld.today/blog/2017/9/8/apple-has-shipped-12-billion-iphones-in-the-past-10-years) complessivo dovuto appunto a iPhone dal 2007.

Un *trillion*, per gli americani. iPhone ha venduto più unità di qualsiasi altro prodotto nella storia, certo; però il secondo in classifica si troverà nello stesso ordine di grandezza.

Sul fatturato, non c’è partita. L’umanità non ha mai visto un successo altrettanto vasto e massiccio.

Un bel debutto per lo Steve Jobs Theater.