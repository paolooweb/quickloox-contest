---
title: "Giochi di carte"
date: 2017-01-12
comments: true
tags: [Google, Apple, Maps]
---
Devo a [Francesco](http://quasi.me) la scoperta di questo fantastico [articolo di confronto tra le mappe di Apple e quelle di Google](https://www.justinobeirne.com/cartography-comparison).<!--more-->

Attenzione, perché trattasi di materiale dettagliato e raffinato. La lunghezza è enciclopedica, il livello di cura fuori dal comune, le cose che si possono imparare fuori dalla media: non saprei dove trovare altrettanta conoscenza sull’argomento in uno spazio altrettanto definito.

Con tempo e pazienza, costituisce una fonte di cultura di valore. E garantisce di poter parlare con vera cognizione di causa delle mappe aspiranti all’universalità: l’uso da parte della maggioranza della popolazione mondiale. Il traguardo è ancora lontano ma siamo già in clima di sprint.