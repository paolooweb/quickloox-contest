---
title: "Questione di metodo / 3"
date: 2017-05-10
comments: true
tags: [Jobs, iPhone, iPad, Cook, AirPods, watch]
---
Dopo avere parlato di [cultura dell’iterazione del prodotto](https://macintelligence.org/posts/2017-05-09-questione-di-metodo-ii/) e di [segretezza](https://macintelligence.org/posts/2017-05-08-questione-di-metodo-i/), mi chiedo ancora una volta se la Apple senza Steve Jobs stia mettendo in pratica in modo intelligente gli insegnamenti del fondatore. Questa volta, riguardo alla soddisfazione del cliente.<!--more-->

Che è (quasi) sempre stata molto elevata per tutti i prodotti fondamentali e, ove non la più alta in assoluto, sempre sul podio.

Prendiamo [AirPods](https://www.apple.com/it/airpods/). Conoscendo la persona, a me basterebbe e avanzerebbe il [racconto di Flavio](https://macintelligence.org/posts/2017-01-17-ti-cambiano-la-vita/). Mi rendo conto, non a tutti.

Nel primo anno di vita, gli AirPods avrebbero raggiunto il [98 percento di soddisfazione complessiva](https://techpinions.com/apples-airpods-a-consumer-success-with-98-customer-satisfaction/49880). È una cifra altissima, che supera persino watch ([97 percento](https://techpinions.com/the-state-of-apple-watch-satisfaction/41126)). iPhone aveva il 92 percento e iPad pure.

Facile osservare che un auricolare non è certo un Mac e la sua complessità, per quanto impressionante in assoluto, è relativa rispetto ad altri apparecchi. Eppure è facilissimo essere insoddisfatti di un auricolare: se non si alloggia bene nell’orecchio, se la connessione senza fili interrompe l’ascolto della musica, se si tiene male in mano, se la batteria non dura quanto si vorrebbe. Con il prezzo che hanno, l’insoddisfazione ci mette un attimo a montare. Eppure.

Certo si tratta di un prodotto assai secondario, per collocazione e risultati. Eppure sembra che ci sia stata posta attenzione come su un’ammiraglia, attenzione ai particolari, miniaturizzazione, soluzioni realmente innovative (Apple ha aggiunto uno strato di programmazione extra allo standard Bluetooth, per assicurare una esperienza impeccabile).

Si può dire che sia almeno l’indizio di una continuata attenzione di Apple alla soddisfazione dell’utente, esattamente come succedeva quando c’era Steve? Io dico di sì.