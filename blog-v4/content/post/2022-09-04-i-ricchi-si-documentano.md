---
title: "I ricchi si documentano"
date: 2022-09-04T18:23:42+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [Runestone]
---
Dopo un trimestre abbondante di utilizzo, continuo a [ritenermi soddisfatto di Runestone](https://macintelligence.org/posts/2022-05-09-è-giovane-ma-crescerà/) e della decisione di acquistare la versione Premium per contribuire allo sviluppo.

Mi è interessante sapere che la documentazione dell’editor, già meritevole, si arricchisce di [tutorial interattivi per impiegare Runestone in situazioni di sviluppo](https://docs.runestone.app/tutorials/meet-runestone).

Il tutto [realizzato con il nuovo strumento ufficiale di Apple per scrivere documentazione](https://twitter.com/simonbs/status/1566447381494861832).

<blockquote class="twitter-tweet"><p lang="en" dir="ltr">The documentation for Runestone now contains interactive tutorials that will guide the reader through adding Runestone to a project and setting up a text view with syntax highlighting ✨<br><br>The tutorials are written using Apple’s DocC framework 📖😃<a href="https://t.co/xQu1PgyBGI">https://t.co/xQu1PgyBGI</a> <a href="https://t.co/5vWlB4tM4J">pic.twitter.com/5vWlB4tM4J</a></p>&mdash; Simon B. Støvring (@simonbs) <a href="https://twitter.com/simonbs/status/1566447381494861832?ref_src=twsrc%5Etfw">September 4, 2022</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

L’uso di [DocC](https://github.com/apple/swift-docc) è un valore aggiunto per tante ragioni. Il [materiale su DocC presente nel sito sviluppatori Apple](https://developer.apple.com/documentation/docc) lascia presagire che il suo uso durerà nel tempo. Uno sviluppatore indipendente che si agganci al lavoro di Apple invece che buttare lì l’ennesima paginetta web o schermata dentro una app, dà un segnale di entusiasmo, impegno e competenza.

Oltre che lo sviluppatore, interessa anche l’app: scrivere documentazione è una bella fatica, bella, ma fatica, e a chi fa i conti solo a breve termine sembra non portare alcun vantaggio in termini di reddito. Una app bene aggiornata è preziosa; bene documentata, oltre che aggiornata, è ancora più preziosa, specie in proiezione futura.

C’è ricchezza in Runestone, nel codice, nel suo sviluppo, nel suo autore. Spero proprio che lo sviluppo prosegua e ci porti una app ancora più distinta dal mucchio.