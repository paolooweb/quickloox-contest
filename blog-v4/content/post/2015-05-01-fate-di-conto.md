---
title: "Fate di conto"
date: 2015-05-02
comments: true
tags: [R, iPhone, iPad, Mac, Healy, Dr.Drang]
---
A proposito del [declino delle vendite di iPad](https://macintelligence.org/posts/2015-04-29-scale-temporali/) che continua a tenere banco, si segnalano gli interventi di [Dr. Drang](http://leancrew.com/all-this/2015/04/moving-averages-and-the-ipad/), che studia il fenomeno applicando il concetto della [media mobile](http://mathworld.wolfram.com/MovingAverage.html) e di [Kieran Healy](http://kieranhealy.org/blog/archives/2015/04/28/apple-sales-trends/) con la [regressione locale](http://en.wikipedia.org/wiki/Local_regression).<!--more-->

La cosa affascinante, anzi, (che appare al profano) fatata è quanto si possa raffinare l’analisi – e pure la rappresentazione grafica – di una serie di dati di vendita trimestrali. Molto spesso si tende a ragionare come se l’istogramma fosse la conclusione del ragionamento, quando invece è solo la partenza.

Si può guardare su GitHub al [codice sorgente](https://github.com/kjhealy/apple/blob/master/apple.r) usato da Healy. Se qualcuno sta leggendo con un occhio alla sua professione futura o crede nel miglioramento costante delle proprie capacità, questo è un bel pezzo di futuro. I *data scientist* saranno figure sempre più apprezzate e arriveranno anche a essere richiesti dalle aziende, non solo dalla ricerca e dalle università. E anche dalle aziende piccole. Anzi, più è piccola, più servirà il *data scientist*.

La grandezza dei tempi che viviamo – ovviamente visti con il bicchiere mezzo pieno – sta nel fatto che un pezzo di software come R è libero, gratuito e ovviamente disponibile su tutti i computer, compresi [Mac](http://cran.r-project.org/bin/macosx/), [iPhone e iPad](https://itunes.apple.com/it/app/r-programming-language/id540809637?l=en&mt=8).