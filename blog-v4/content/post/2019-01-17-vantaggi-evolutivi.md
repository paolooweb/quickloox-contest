---
title: "Vantaggi evolutivi"
date: 2019-01-17
comments: true
tags: [Apple, Google, iOS, iPhone, Android]
---
Scrivevo [nel lontano ottobre 2013](https://macintelligence.org/posts/2013-10-03-le-volpi-e-luva/) del primo computer da tasca al mondo con processore a sessantaquattro bit, iPhone 5S. Per capirci, ero fiero del mio iPad di terza generazione, il primo con schermo Retina, quello che oggi trovo lentissimo a confronto di iPad Pro. È passato davvero tanto tempo, anche ingegneristicamente.

iOS ha supportato le app a trentadue bit per qualche anno, ma [dal 2017 non sono più ammesse](https://clearbridgemobile.com/apple-dropping-32-bit-app-support-will-impact-app/).

Ed ecco che Google annuncia la [transizione di Android ai sessantaquattro bit](https://gadgets.ndtv.com/android/news/google-s-transition-to-64-bit-apps-begins-in-august-32-bit-support-to-end-in-2021-1978646). Comincia il prossimo agosto, tra soli sette mesi. E va avanti, con eccezioni e interrogativi vari, fino al 2021.

Alla prossima occorrenza della domanda *che cosa fa iOS che Android non faccia a costo minore*, c’è una risposta aggiuntiva: *avere quattro anni di vantaggio evolutivo sulla concorrenza*.