---
title: "Cinque di meno"
date: 2020-09-14
comments: true
tags: [QuickLoox]
---
Ci sono tante ragioni a spiegare la frequenza dei refusi che compaiono in queste pagine. E io odio i refusi.

A volte scrivo in modo meditato e a volte invece apro il flusso di coscienza. La velocità aumenta a dismisura, smetto di guardare la tastiera e anche lo schermo, intendo con attenzione. C’è un piacere adrenalinico nel vedere scorrere le parole e sentire che la narrazione procede verso la sua naturale conclusione. L’attenzione si sposta dal particolare al generale e l’errore non si vede più. Magari c’è una rilettura alla fine; magari c’è meno tempo del dovuto e nessuno rilegge.

Più prosaicamente, capita di scrivere dal letto con iPad Pro, alle tre di notte. Non è tanto la tastiera di vetro quanto la sonnolenza di piombo. Chiaro che con gli occhi chiusi a metà si controlli solo metà dello scritto e il resto *que sera sera*.

È un piccolo e però irritante disservizio che colpisce il lettore; non dovrebbe accadere e mi scoccia molto. Per fortuna c’è chi mi fa notare almeno qualche svarione, che provvedo sempre a correggere.

A questo proposito, mi si permetta di mostrare l’orgoglio; ho appena eliminato cinque refusi su articoli pubblicati tra gennaio e marzo 2019.

Per quanto possano essere frequenti gli errori, adesso ai sensi dell’aritmetica lo sono di meno. E non so quanti blogger si prendano il tempo o abbiano la voglia di pulire lavoro di un anno e mezzo fa.
