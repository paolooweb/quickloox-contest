---
title: "Fifty-fifty"
date: 2019-01-18
comments: true
tags: [iPad, Pro, Christoffel]
---
Non ho cominciato ieri a usare Mac come desktop e iPad come portatile, ma quasi sette anni fa.

Ciò che è cambiato è sicuramente la percentuale del lavoro che svolgo su una o l’altra macchina. Prima Mac faceva la parte del leone e iPad serviva unicamente a mantenere il flusso delle cose durante gli spostamenti.

Oggi la percentuale di lavoro è pressoché in equilibrio, metà qui e metà là.

Le capacità di Mac non sono diminuite in alcun modo, anzi: con Mojave mi trovo benissimo e anche un modesto Mac mini come l’attuale procura tranquillamente tutte le soddisfazioni che cercavo, anche inaspettate: prima di avere problemi con Safari posso aprire approssimativamente il quadruplo delle pagine aperte sul vecchio MacBook Pro, per dire. Non ho ancora raggiunto un conteggio di applicazioni aperte nel Dock tale da mandare in crisi la macchina, quando quella precedente iniziava a dare chiari segni di stanchezza dopo le venticinque.

E il *dibbbattito* su iPad capace o meno di sostituire Mac? Trovo che nel tempo appaiano sempre più commenti sensati, l’[ultimo dei quali](https://www.macstories.net/stories/thinking-different-keys-to-adopting-an-ipad-first-workflow/) è firmato su *MacStories* da Ryan Christoffel.

>Uno dei peggiori errori che si possono fare con un nuovo computer à dare per scontato che tutto funzionerà come in quello vecchio. Proprio come il passaggio da PC a Mac procura alcuni fastidi […], spostarsi su iPad comporta costi di transizioni sensibili.

Il punto, però, è non fermarsi lì; passato il primo impatto, abbracciare la filosofia di uso di iPad può rivelarsi vantaggioso. Non per tutti, certamente per qualcuno sì. Dove le modalità di utilizzo sembrano meno snelle che su Mac, le nuove *Shortcut* sono spesso preziose, basta volerle comprendere. Alcune cose continuano a essere più svelte su Mac, altre invece lo sono su iPad. Ieri ho disegnato al volo uno schemino in una riunione. Dentro le Note, senza un programma specifico e con il dito invece che con Apple Pencil. Risultato pienamente rispondente alle aspettative del momento. Se avvenisse con regolarità invece che mai – nel mio caso, chiaro – penserei molto più seriamente di prima a una Apple Pencil.

Ho anche lavorato alla valutazione delle competenze digitali di un gruppo di lavoro; sostanzialmente ho posto domande, registrato audio, preso appunti, fotografato momenti estemporanei della situazione. Il vecchio iPad proiettava *slide* che davano il ritmo e io svolgevo tutte le altre attività su iPad Pro, senza problemi e soprattutto con invasività zero, dato che lo schermo è molto vicino alla superficie della scrivania e mancano barriere, reali o percepite, tra intervistato e intervistatore.

iPad non è un sostituto di Mac, non sempre, non per tutti. Ma è un computer con tutti i crismi.