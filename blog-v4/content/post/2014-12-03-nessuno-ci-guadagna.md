---
title: "Nessuno ci guadagna"
date: 2014-12-03
comments: true
tags: [Lumia, SeattleTimes, Kelly, WindowsPhone, Samsung, Huawei]
---
Cose già dette che non devo stancarmi di ripetere, specie se vengono pronunciate da personaggi di qualche rilievo come Joe Kelly, responsabile delle relazioni di Huawei con i media internazionali.<!--more-->

Huawei è uno dei marchi emergenti nella fabbricazione di computer da tasca e tavolette, che segano le caviglie a Samsung con prodotti di prezzo inferiore e qualità equivalente (dove l’ho già sentita?). [Kelly spiega al Seattle Times](http://seattletimes.com/html/businesstechnology/2025131855_chinahuaweixml.html) una semplice verità:

>Con Windows Phone abbiamo guadagnato niente. Nessuno ha guadagnato alcunché con Windows Phone.

Un minuto di riflessione e quella gente che esce trionfante dal centro commerciale con un Lumia in tasca, convinta di averci guadagnato, pare strana.