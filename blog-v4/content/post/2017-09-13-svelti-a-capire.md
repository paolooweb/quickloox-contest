---
title: "Svelti a capire"
date: 2017-09-13
comments: true
tags: [iPhone, Decluttr, AppleWorld, Today]
---
Qualunque iPhone sia stato annunciato, prima di leggere commenti e recensioni meglio ripassare [l’infografica di Decluttr diffusa da AppleWorld Today](https://www.appleworld.today/blog/2017/9/11/infographic-from-decluttr-shows-how-the-internet-reacted-to-the-iphone-over-the-years) sull’accoglienza riservata negli anni alle varie edizioni dell’apparecchio.

>Quella tastiera virtuale sarà utile per toccare email e messaggi più o meno come un telefono a disco.

>iPhone è niente più che un’esca di lusso per pochi fanatici di gadget.

>Vogliamo una tastiera! Questa funzione rappresentativa di iPhone rimane una delle sue più grandi mancanze.

>Il controllo vocale somiglia alla risposta alla domanda che nessuno ha chiesto.

>Quest’anno a Halloween mi vesto da iPhone 4S: metto il costume dell’anno prima e deludo tutti.

Eccetera eccetera. L’infografica è deliziosa e quanto sopra è solo un assaggio.

Una cosa è chiara, per il decennio appena trascorso: a recensire iPhone sono persone tra le più lente del pianeta a comprendere la realtà delle cose.

 ![L’infografica di Decluttr sulle reazioni dei media a iPhone](/images/decluttrinfographic.png  "L’infografica di Decluttr sulle reazioni dei media a iPhone") 