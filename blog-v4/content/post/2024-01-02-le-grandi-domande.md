---
title: "Le grandi domande"
date: 2024-01-02T13:18:45+01:00
draft: false
toc: false
comments: true
categories: [Software, Hardware]
tags: [Cray-1, Raspberry Pi, Whetstone, Longbottom, Roy Longbottom]
---
Chi siamo, da dove veniamo, dove andiamo. Bello chiederselo a inizio di un anno inedito.

Da dove veniamo. Il supercomputer Cray-1 costava sette milioni di dollari, pesava cinquemila chilogrammi e consumava centoquindicimila watt.

Dove andiamo. Un Raspberry Pi base costa settanta dollari, pesa una frazione di un chilogrammo, consuma cinque watt e va *quattro volte e mezzo più veloce* di un Cray-1.

Chi siamo. Oggi dovremmo sentirci tutti un po’ Roy Longbottom, il signore responsabile delle affermazioni sopra. Stanno in una pagina che [confronta le prestazioni di Cray-1 con una pletora,di computer, telefoni, aggeggi vecchi e nuovi](http://www.roylongbottom.org.uk/Cray%201%20Supercomputer%20Performance%20Comparisons%20With%20Home%20Computers%20Phones%20and%20Tablets.htm) attraverso il test Whetstone, che nel duemilaventidue ha compiuto mezzo secolo di vita e qualcosa lo avrà pure imparato.

Roy Longbottom nel duemiladiciannove è stato reclutato come membro volontario dei collaudatori della versione alpha di Raspberry Pi. Nel duemilaventidue ha fatto lo stesso per Raspberry Pi Poco W.

Nel duemiladiciannove, Roy Longbottom aveva ottantaquattro anni.

L’attitudine ai-miei-tempi, non-ho-più-l’età, non-è-più—come-una-volta, è-cambiato-tutto, nel duemilaventiquattro è anacronistica e non si porta proprio più, né si può più sentire. Sotto, che c’è da scoprire, imparare, sperimentare come mai prima.