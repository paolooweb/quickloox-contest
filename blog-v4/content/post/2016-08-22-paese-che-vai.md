---
title: "Paese che vai"
date: 2016-08-22
comments: true
tags: [Windows, Mac, Tabakalera]
---
(scrive **Matteo**):

Mac che trovi.

**Ex Manifattura Tabacchi** in San Sebastian, Paesi Baschi spagnoli, [adibita a centro culturale](https://www.tabakalera.eu/es). Dove si può semplicemente navigare in Internet con Windows, o creare con Mac.<!--more-->

**Devo aggiungere altro?**

Direi che è sufficiente. Abbiamo anche le foto.

 ![Tabakalera in San Sebastian](/images/tabakalera-12.jpg  "Tabakalera in San Sebastian") 
 ![Ancora la Tabakalera](/images/tabakalera-plastico.jpg  "Ancora la Tabakalera") 


