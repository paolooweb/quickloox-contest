---
title: "C'è chi dice no"
date: 2014-09-15
comments: true
tags: [iPhone, Watch, Ready, Braintree, PayPal, Biver, Lvmh, Telegraph, Microsoft]
---
Bill Ready, amministratore delegato di Braintree (proprietà di PayPal), così [si è espresso su Cnbc](http://www.cnbc.com/id/101992878) a proposito di [Apple Pay](https://www.apple.com/apple-pay/).<!--more-->

>I pagamenti sono un ecosistema difficile e, sapete, molti altri, molte altre aziende Internet per consumatori hanno provato a entrare in questo spazio, trovando, sapete, un successo limitato. Si tratta di uno spazio molto difficile.

Jean-Claude Biver, direttore della divisione lusso di Lvmh, ha rilasciato una [dichiarazione al Telegraph](http://www.telegraph.co.uk/technology/apple/11088667/Apple-Watch-too-feminine-and-looks-like-it-was-designed-by-students-says-LVMH-executive.html) parlando di [Apple Watch](https://www.apple.com/it/watch/).

>Non ha sex appeal. È troppo effeminato e somiglia troppo a quello che già sta sul mercato. A essere completamente onesto, sembra progettato da uno studente del primo trimestre.

Steve Ballmer, ex amministratore delegato di Microsoft, in [risposta](http://arstechnica.com/information-technology/2007/04/ballmer-says-iphone-has-no-chance-to-gain-significant-market-share/) all’annuncio di iPhone.

>Non c’è alcuna possibilità che iPhone ottenga una qualsiasi quota di mercato significativa. Nessuna possibilità.

Ci si sente anche più giovani, a leggere cose del 2007.