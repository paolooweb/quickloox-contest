---
title: "Utenti registrandi"
date: 2017-11-22
comments: true
tags: [Microsoft, WebTap, Adobe]
---
Quasi cinquecento dei primi cinquantamila siti più visitati al mondo [dispongono di script di registrazione](https://webtransparency.cs.princeton.edu/no_boundaries/session_replay_sites.html) in grado di acquisire tutto quello che il navigatore digita, come e dove fa scorrere le pagine, quando e come fa clic e via discorrendo. In altre parole, sorveglianza totale.<!--more-->

Lo studio, eccettuati pochi casi eclatanti, si astiene dall’affermare che tutti i siti individuati sorveglino davvero il visitatore in questo modo. Ma afferma con certezza che, volendo, *possano* farlo. Il software è lì, basta farlo partire.

Il sito numero 35 della lista è wordpress.com. Il numero 45 è microsoft.com. Il 74 è adobe.com. Il 209 è spotify.com. Il 228 è skype.com. Il 1438 è androidcentral.com.

Per farsi un’idea della buona compagnia in cui si trovano, il sito numero 4353 è mackeeper.com: uno dei peggiori distributori di malware per Mac.

Windows.com, numero 1040, è uno di quei pochi casi nei quali esiste evidenza della sorveglianza. Cercando *windows* appaiono nell’elenco anche windowscentral.com (3489) e windowsazure.com (4875).

Ho cercato *apple* ma non appare niente.

Poi uno si chiede perché scegliere Mac.