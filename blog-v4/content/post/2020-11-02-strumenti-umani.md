---
title: "Strumenti umani"
date: 2020-11-02
comments: true
tags: [Excel, no-code, TechCrunch, Office, Retool, Airtable, Roblox, Minecraft]
---
C’è un articolo su TechCrunch, riguardante [l’arrivo della generazione cosiddetta No-Code](https://techcrunch.com/2020/10/26/the-no-code-generation-is-arriving/), che andrebbe letto nelle scuole. Ma non agli studenti; agli insegnanti.

Lo stesso articolo andrebbe letto nelle aziende. Non ai dirigenti; a tutto il resto del personale.

Parla di un nuovo *digital divide* che nasce e che, con il senno di poi, farà apparire come *vecchiume* il primo divide, quello tra chi sapeva usare un computer e chi no.

Le piattaforme no-code, per esempio [Retool](https://retool.com) o [Airtable](https://airtable.com), *possono cucire assieme in un paio di clic progetti che una volta avrebbero richiesto ore a un team di ingegneri*. Il loro vantaggio rispetto al passato, certamente, non è la dizione *no-code*:

>Naturalmente, gli strumenti no-code spesso richiedono codice o, almeno, quella sorta di logica deduttiva che è intrinseca al coding. […] Bisogna pensare in termini di dati e nell’ambito di input, trasformazioni e output.

Sono strumenti attualmente di successo non perché siano più semplici da usare, ma perché risuonano positivamente in una generazione che *comprende precisamente la sorta di logica richiesta da queste piattaforme per funzionare*.

>Gli studenti di oggi non vedono affatto computer e apparecchi mobile come schermi da consumo e hanno la capacità di usarli come strumenti per l’espressione di sé, la ricerca e l’analisi.

Se sembra materiale esoterico, a questo punto l’articolo fa riferimento a [Roblox](https://www.roblox.com) e [Minecraft](https://www.minecraft.net/en-us), *derisi come giochini per adolescenti ossessionati* e oggi piattaforme con centinaia di milioni di utilizzatori e centinaia di migliaia di sviluppatori.

Le nuove generazioni hanno reale dimestichezza con i nuovi strumenti e arrivano sul lavoro con competenze inedite: *sanno andare a fondo nei sistemi che usano e concatenare programmi mediante piattaforme no-code, per arrivare a rispondere a problemi in forma molto più completa e in tempo reale*.

>La differenza di produttività tra una conoscenza di base dei computer e una competenza appena più avanzata è profonda. Anche una analisi dei dati elementare, se accurata, può portare a prestazioni sostanzialmente superiori di quelle che vengono da valutazioni di pancia e fogli di calcolo scaduti.

La coincidenza della pandemia fa sì che queste nuove generazioni, per via della scuola in remoto, acquisiscano competenze ancora maggiori di informatica e reti.

Questa la conclusione dell’articolo:

>Questa generazione sta alzando l’asticella di come si usano i dati sul posto di lavoro, nel business e nell’imprenditoria. Sono più bravi che mai a connettere singoli servizi e trasformarli in esperienze efficaci per clienti, lettori e utenti. Hanno il potenziale per colmare una volta per tutte il divario di mancata produttività nell’economia globale, migliorare le nostre vite e fare risparmiare tempo a tutti.

Tra i commenti del lettori, uno spicca:

>Sebbene negli uffici la maggior parte delle persone sappia “usare il computer”, lo strumento più evoluto che sanno usare (male) è Excel. Gli ultratrentenni usano Excel per qualsiasi cosa, perfino per automatizzare processi! I ventenni arrivano con la voglia di imparare e collaudare nuovi strumenti, nonché innovare molti aspetti del business che trovano obsoleti e improduttivi. Come business leader, sono abituato ad affrontare la resistenza al cambiamento dei “vecchi” (sopra i trent’anni!) e a contare su giovani con l’energia per fare andare avanti le cose con entusiasmo.

Ho pensato a quali potrebbero essere le reazioni di tante persone che conosco, a tutte le età, tutte inserite nel nostro tessuto produttivo.

Cose tipo *io giovani così non ne vedo*, *sono cose che da noi nemmeno si immaginano*, *ci vorrà tempo*, *non è così facile* eccetera. Ovvero, non abbiamo nemmeno l’idea di quanto siamo *complessivamente* indietro.

Nella scuola, il pensiero di formare ragazzi con la giusta dose di pensiero computazionale turba ancora i sonni della maggior parte dei docenti, gente già turbata dalla sparizione dei calamai e dall’eresia delle penne a sfera. Degli altri, la maggior parte porta al collo l’incudine di Office. Quelli che avanzano sono pochi eroi solitari che fanno magie attorno a sé, meriterebbero una medaglia al valore civile e però coprono il cinque percento del fabbisogno.

Per quanto riguarda gli uffici, è stato già detto tutto. La ricerca ossessiva del minimo denominatore comune e del livello più basso di competenza sono l’esperienza comune e andare oltre Office è pressoché impossibile, per la pressione dei pari e la miopia dei dirigenti, che ne sanno meno degli altri. Siamo indietro a un livello pauroso.

Per come siamo messi demograficamente, prima che le cose cambino per virtù dei ragazzi di oggi che accedono alle leve del potere, passeranno ancora molti anni, di tendenza costante al ribasso. Bisogna salvarsi individualmente, acquisire la capacità mentale e le spalle larghe a sufficienza per superare Office, resistere alla tentazione di lanciare Excel per fare la qualunque, aprirsi a sistemi nuovi e più evoluti di fare le cose. Che poi affondano saldamente le radici nello Unix, con i suoi strumenti modulari pronti da concatenare, che i più fortunati di noi hanno già a portata di mano anche nella macchina più vetusta e malconcia.

Per i prossimi anni si prepara una grande scrematura tra chi è disposto a confrontarsi con strumenti nuovi e chi penserà di salvarsi alzando barricate dietro Office, strumenti umani manovrati dalla Grande Omologazione. Molto probabilmente la differenza finale la faranno i bilanci.