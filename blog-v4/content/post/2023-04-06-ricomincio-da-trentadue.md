---
title: "Ricomincio da trentadue"
date: 2023-04-06T10:12:09+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [Scribus]
---
Ogni tanto si fa il giro del software open source, per via degli obiettivi a lunga scadenza. Nel caso di [Scribus](https://www.scribus.net), l’obiettivo è liberarsi dalla schiavitù e dai metodi commerciali molto discutibili di Adobe.

Purtroppo l’obiettivo si allontana; la nuova versione, su un macOS aggiornato, mostra il divieto di accesso sopra l’icona Neanche parte.

Parrebbe che il problema sia l’architettura a trentadue bit.

Non so quanta difficoltà ci sia effettivamente nel convertire il codice a sessantaquattro bit. Su questo Apple fa propaganda, ovviamente, e i suoi proclami sono certamente veri solo e soltanto sulle app di cui parla esplicitamente. Una app che si è convertita a sessantaquattro bit con minimo sforzo e grande facilità, sì, si può convertire in quei termini. Le altre, bisogna vedere.

Detto questo, per una applicazione Mac di minimo rispetto è inconcepibile questa trascuratezza. Anche perché la questione dei trentadue bit è stata pianificata, affrontata e risolta nel giro di molti anni e con nettezza.

Ignoro se lo sviluppatore medio dedito a Scribus si trovi in una situazione critica che gli impedisce di realizzare una versione Mac moderna. Spero comunque che si risolva, presto. Difficile pretendere da un progetto open source la persistenza e le garanzie di una impresa commerciale; al tempo stesso, nel caso di un programma di impaginazione, fa un po’ parte del pacchetto. Se capita di cambiare il vecchio Mac e prenderne uno nuovo, ti aspetti che una app come Scribus ti segua. Anche dopo un anno magari, ma lasciarti a piedi non va bene.