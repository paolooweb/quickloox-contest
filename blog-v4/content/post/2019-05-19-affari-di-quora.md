---
title: "Affari, di Quora"
date: 2019-05-19
comments: true
tags: [Quora, Accomazzi, bigoncio, Apple]
---
Da tempo l’amico Akko [ricava più denaro con il web che con la divulgazione](https://accomazzi.net/soluzioni-internet/guadagnare-risparmiare-internet-web-0007101.html).

La divulgazione, peraltro, lo appassiona comunque ed è per questo che, morte le riviste Apple tradizionali, continua a scrivere indefesso.

Tanto che ha aperto [Apple e il resto del bigoncio](https://it.quora.com/q/ltjtvvzsluwhpmbx), uno spazio su Quora, *social* basato sulla premessa *fai una domanda e qualcuno ti risponde*.

Akko, poteva essere diversamente?, fa sul serio e risponde a domande su domande, tutte in ambito Apple, come la maggior parte dei sedicenti commentatori Apple italiani neanche riuscirebbe a immaginare in un sogno bagnato.

Chi è in cerca di informazione vera, seria, entusiasta su Apple dovrebbe investire più tempo su Quora *chez Akko* e meno su certe trappole per erogare pubblicità mediocre con il pretesto di informare.