---
title: "Siamo al completamento"
date: 2017-12-11
comments: true
tags: [BBEdit]
---
Passati alcuni giorni di [intenso utilizzo di BBEdit 12](https://macintelligence.org/posts/2017-11-29-il-babbo-e-sempre-una-garanzia/), segnalo un miglioramento assolutamente decisivo, che vale il prezzo del biglietto da solo: il completamento automatico dei delimitatori di testo.<!--more-->

 ![Completamento automatico delimitatori in BBEdit 12](/images/completion.png  "Completamento automatico delimitatori in BBEdit 12") 

Significa che, nello scrivere in Html o in [Markdown](https://daringfireball.net/projects/markdown/), o in altri formati che prevedono delimitatori, posso procedere in due modi diversi: personalizzare il programma in modo da assegnare i delimitatori a combinazioni da tastiera, oppure digitare il delimitatore di inizio e avere automaticamente a disposizione quello di fine. E anche selezionare un pezzo di testo e circondarlo con delimitatori semplicemente digitando quello che serve.

Per quanto riguarda il primo modo, negli anni il mio BBEdit è stato pesantemente personalizzato, al punto che scrivere in testo puro o Html richiede praticamente tempo identico.

Ogni tanto però tornava utile il secondo e dovevo arrangiarmi in modi indiretti. Un po’ come su iPad dove sono molto soddisfatto di [Editorial](http://omz-software.com/editorial/) e della sua personalizzabilità, ma trovo lo scrivere Html o Markdown solo buono e non ottimale.

È un’area di miglioramento che vorrei coltivare seriamente nel 2018, l’anno in cui tenterò di eliminare quanto più possibile il lavoro manuale e non creativo al computer. Affidandolo, appunto, al computer.