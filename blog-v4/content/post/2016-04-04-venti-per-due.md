---
title: "Venti per due"
date: 2016-04-04
comments: true
tags: [ Mori, Apple]
---
Raccomando vivamente la lettura del lungo post di **Riccardo** sui suoi [momenti personali di quarant’anni di Apple](http://morrick.me/archives/7605). Non fosse per lo stile sarebbe per la puntualità dei ricordi e della documentazione, o forse ancora per la peculiarità degli aneddoti e delle circostanze. È sempre un pezzo da leggere dall’inizio alla fine.

Scappasse una pausa, va riempita con [l’elenco dei Mac venduti da Apple nel 1996](http://morrick.me/archives/7584), giusto vent’anni fa, a metà strada. C’è gente che accusa Apple di avere oggi una linea di prodotti troppo articolata. Le uniche risposte dopo quest’altro pezzo potranno essere solo ignoranza o smemoratezza.
