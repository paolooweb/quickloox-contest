---
title: "La tipografia ti salva"
date: 2019-06-19
comments: true
tags: [Genius, Google]
---
Gli autori di un certo sito sono convinti che Google gli rubi testi di canzoni. Come incastrare però Big G, in una rete dove la violazione del copyright è più praticata della colazione del mattino?

Hanno avuto un’idea lunimosa e hanno iniziato a [alternare apostrofi tipografici con altri dritti](https://www.wsj.com/articles/lyrics-site-genius-com-accuses-google-of-lifting-its-content-11560677400), secondo uno schema che potesse servire come validazione dei risultati.

Effettivamente Google ha rubato quei testi ed è stata scoperta. Il tutto perché nella cultura aziendale è scarsissima l’attenzione alla tipografia e nessuno si è curato che gli apostrofi fossero corretti, prima d ripubblicare la refurtiva.

Se c’è una lezione, è che una buona tipografia apre orizzonti. E chiude all’occorrenza la strada al plagio.
