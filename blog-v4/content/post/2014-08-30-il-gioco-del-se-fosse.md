---
title: "Il gioco del se fosse"
date: 2014-08-31
comments: true
tags: [Apple, iPad, iPhone, Slate, McDonald’s, Coca-cola, Amazon, Twitter, iPod, LinkedIn, Groupon, Tesla, eBay, Google, iTunes, Mac]
---
Slate si è chiesto [che scala avrebbero](http://www.slate.com/blogs/moneybox/2014/07/22/apple_earnings_iphone_revenues_are_as_big_as_amazon.html) aziende monoprodotto destinate ciascuna a uno dei *business* di Apple, in base al fatturato.<!--more-->

Se iPhone fosse una azienda dedicata, sarebbe grande quanto Coca-cola e McDonald’s messe insieme. Oppure quanto Amazon. Starebbe appena dietro a Google e eBay riunite.

Se iPad fosse una azienda monoprodotto, bisognerebbe accorpare Yahoo!, Facebook, LinkedIn, Twitter, Groupon e Tesla per rivaleggiarla. E non basterebbe, di poco. Anche se in effetti supererebbero appena i proventi della vendita di Mac.

eBay sarebbe battuta dall’attività attorno a iTunes.

Perfino il vecchio e dimenticato iPod, che sta terminando la sua parabola commerciale, oggi surclassa Twitter in fatturato.

Tendiamo a dimenticarcelo, ma la Apple di oggi proprio non è quella di una volta e va tenuto presente quando se ne valutano politiche e strategie.