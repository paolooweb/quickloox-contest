---
title: "Le vecchie liste di una volta"
date: 2020-09-08
comments: true
tags: [Hume, macOS, Emacs]
---
Più si va avanti più diventa difficile trarre giovamento dalle raccolte di liste di scorciatoie e trucchi. Checché ne dicano quelli che si lamentano della scomparsa della *findability*, lentamente si impara tutto o quasi; le liste troppo dettagliate sono troppo lunghe per essere utili; quelle troppo corte fanno scoprire nulla di nuovo.

In questo panorama di noia strisciante e progressiva mi piace segnalare Tristan Hume e il suo [elenco di consigli potenzialmente interessanti su macOS](https://thume.ca/2020/09/04/macos-tips/).

È lungo ma non troppo, comunque facile da leggere. Qualcosa di buono l’ho trovato e a volte si tratta di cose che si sapevano, prima di dimenticarsene.

Carina anche la sezione delle app e i bonus su iOS.

Niente di che, però era tanto che mi fermavo molto prima di scorrere una lista fino in fondo e stavolta l’ho fatto.