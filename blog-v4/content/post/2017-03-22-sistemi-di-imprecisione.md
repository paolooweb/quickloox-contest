---
title: "Sistemi di imprecisione"
date: 2017-03-22
comments: true
tags: [Swatch, iOS, Android, watchOS]
---
Apple ha impiegato due anni e mezzo a sviluppare iPhone e una delle critiche più rumorose con le quali venne accolta la notizia riguardava l’inesperienza dell’azienda nella telefonia cellulare. Gli sviluppi della vicenda sono nelle tasche e nelle borsette di chiunque.<!--more-->

Swatch vuole percorrere la strada opposta e offrire un [sistema operativo per computer da polso](https://www.bloomberg.com/news/articles/2017-03-16/swatch-takes-on-google-apple-with-operating-system-for-watches), il primo esempio concreto del quale sarà un modello di Tissot previsto per la fine del 2018.

Acquisire dal nulla esperienza nella progettazione di sistemi operativi e interfacce equivale a partire da zero con lo *hardware* per la telefonia?

Bella domanda. Vedo però una differenza. Prima di iPhone, Apple aveva accumulato esperienza sostanziale nella miniaturizzazione, nella produzione di grandi quantità di unità, nelle interfacce per schermi piccoli. Attraverso iPod. E deteneva una serie di tecnologie interessanti nel campo dei computer da tenere in mano, maturata con Newton. Quella che sembrava una partenza da zero, non lo era affatto.

I pregressi di Swatch che possano essere propedeutici a un sistema operativo da orologio intelligente, non lo conosco. Può darsi che ve ne siano. Rimango dell’opinione che siano stati fatti calcoli un po’ troppo ambiziosi, oppure che gli obiettivi siano molto modesti rispetto a come se ne parla. Sarebbe interessante conoscere i piani degli svizzeri con maggiore precisione.
