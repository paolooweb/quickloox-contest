---
title: "Notizie che non lo erano (e che dovrebbero)"
date: 2021-02-10T01:22:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [iPhone 12 mini, Mount Sinai, coronavirus, Covid-19] 
---
Vari siti sono in fermento perché iPhone 12 mini avrebbe venduto poco.

Per *Ars Technica*, si è trattato del [flop 2020 per le vendite di Apple](https://arstechnica.com/gadgets/2021/02/the-iphone-12-mini-hasnt-sold-well-according-to-multiple-estimates/); gli analisti sarebbero a discutere *se ad Apple convenga fare un 13 mini l’anno prossimo*.

Per creare un minimo di contesto: i dati di riferimento sono quelli del trimestre natalizio, record di sempre complessivo e soprattutto per il fatturato di iPhone, oltre sessantacinque miliardi di dollari, il diciassette percento in più dello stesso periodo un anno prima. Siccome si parla di qualche decina di milioni di apparecchi, non mi stupisce che uno possa avere venduto meno degli altri. Indizio: quando hai più di un modello, ce ne sarà *sempre* uno che vende meno degli altri. Avrà venduto meno del previsto? Può essere. Con queste cifre, probabilmente qualcosa avrà anche venduto più del previsto. La notizia, se c’è, sembra da estrarre con più sforzo del petrolio dagli scisti bituminosi.

Si noti che nel 2019 non ci fu alcun iPhone 11 mini. Ci sarà o non ci sarà un iPhone 13 mini? Sorpresa: Apple lo sa già da un pezzo. La linea 2021 è già decisa da tempo ed è stata progettata ben prima che arrivassero sul mercato gli iPhone 12. D’altro canto, non essendoci stato un iPhone 11 mini, non si capisce come possa essere stato pensato di fare un iPhone 12 mini, se la logica è questa.

Tant’è. Pur di rastrellare qualche clic, si scrive la qualunque.

Avrei personalmente trovato più interessante lo studio della clinica americana Mount Sinai sulla [diagnosi anticipata di coronavirus tramite watch](https://preprints.jmir.org/preprint/26107). Nell’intento di preservare il più possibile dal contagio il loro personale, hanno condotto uno studio che ha coinvolto centinaia di soggetti, equipaggiati con watch e chiamati quotidianamente a dare risposte da una app creata per lo scopo.

Risultato: l’insieme dei dati biometrici provenienti da watch e dalle risposte dei partecipanti ha consentito di segnalare l’insorgenza dell’infezione [fino a sette giorni prima di quanto riesca a fare il tampone](https://www.mountsinai.org/about/newsroom/2021/mount-sinai-study-finds-wearable-devices-can-detect-covid19-symptoms-and-predict-diagnosis-pr).

Se munire la popolazione mondiale di watch e app-questionario sembra una opzione poco praticabile, per tutte le organizzazioni che devono continuare a svolgere il proprio lavoro nonostante la pandemia questa è una notizia vera. La parte specialmente interessante è che l’analisi e la previsione delle infezioni possono essere svolte in remoto, senza interagire con un potenziale contagiato. Nel caso di altri apparecchi che rilevino dati biometrici con la stessa precisione e affidabilità di watch, si può applicare la medesima tecnica.

Dirò di più: siccome lo studio è durato da aprile a settembre, è stato impossibile, anche per chi lo volesse, partecipare con un iPhone 12 mini, per la ragione che non era ancora in vendita.

Sarà un’altra ragione del flop?

*Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile.*