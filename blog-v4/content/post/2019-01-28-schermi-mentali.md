---
title: "Schermi mentali"
date: 2019-01-28
comments: true
tags: [kids, schermi, TechCrunch, iPad, YouTube]
---
Finalmente qualcuno ha avuto il coraggio di scrivere chiaramente che, oltre al tempo passato davanti a uno schermo, [conta che cosa si sta guardando](https://techcrunch.com/2019/01/17/in-defense-of-screen-time/).

Sembra ovvio buonsenso considerare la visione di un documentario di *National Geographic* più salutare di due ore di cartoni animati (da genitore apprendista, aggiungo che sono meglio buoni cartoni animati che certa sbobba di YouTube). Eppure è difficilissimo fare passare il messaggio nelle famiglie e nelle istituzioni.

L’articolo di *TechCrunch* non è una panacea ma aiuta moltissimo, anche in virtù dei link a vari studi scientifici ed esperienze pratiche. Ci sono ragazzi con *screen time* fortemente contingentato che poi all’università faticano più della media; chi legge su carta e *anche* su iPad rende di più; ci sono programmi didattici che monitorano il comportamento di chi li usa per individuare le lacune e proporre lezioni personalizzate.

Questo e altro ancora. Fare girare, portare nelle assemblee di classe, dai parenti meno informati eccetera. Chi si oppone per partito preso a un uso intelligente e bilanciato degli strumenti digitali ha davanti lo schermo peggiore di tutti: il pregiudizio.
