---
title: "Era vero"
date: 2022-07-17T01:16:58+01:00
draft: false
toc: false
comments: true
categories: [Hardware]
tags: [John Gruber, Gruber, Daring Fireball, MacBook Air, M2, M1, Apple Silicon]
---
A John Gruber mancava solo di analizzare le impronte lasciate dai piedini sul tavolo, nella sua [recensione di MacBook Air M2](https://daringfireball.net). Abbondano i dettagli, le minuzie, i rimandi, la lettura chiede tempo, nulla resta fuori o, se rimane qualcosa, ci si deve pensare per accorgersene.

A me piace da matti il doppio passaggio all’inizio, quando cita la [recensione del medesimo modello, ma con M1, dell’anno scorso](https://daringfireball.net/2020/11/the_m1_macs):

>Per riconoscere quanto sono buoni i primi Mac basati su Apple Silicon – e sono qui a dire che sono incredibilmente buoni – bisogna accettare che certi presupposti di lunga data su come i computer andrebbero progettati, che cosa rende migliore un computer migliore, di che cosa ha bisogno un buon computer, sono sbagliati. Qualcuno continuerà a negare per anni quello che Apple ha ottenuto qui. È come vanno le cose.

Torniamo a quest’anno ed ecco la continuazione ideale:

>Avevo ragione, ma forse negare era la parola sbagliata. Negare è spesso rifiutare di credere a qualcosa che si vuole non vero. Con Apple Silicon, molte persone sono esitanti nel credere a qualcosa che vogliono sia vero; che questi computer siano buoni come effettivamente sono. Che sono veloci, che non scaldano, che sono molto efficienti. Le persone sospettano che ci sia il trucco.

>Non c’è trucco. MacBook Air M1 del 2020 era (e resta) un gran portatile. Il nuovo MacBook Air M2 è chiaramente migliore da ogni punto di vista.

Continuità, crescita, miglioramento. La nuova versione è meglio di quella vecchia. La notizia reale è che possiamo crederci; consumano meno, vanno più veloci, non scaldano. Se non basta quello che è apparso due anni fa, abbiamo ciò che è apparso quest’anno.

Quello che ha annunciato Apple a Wwdc del 2020 era vero.