---
title: "L’appuntamento tanto atteso"
date: 2021-07-28T00:56:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [Apple, Asymco] 
---
Il dito è il [trimestre finanziario di Apple](https://www.apple.com/newsroom/2021/07/apple-reports-third-quarter-results/), impressionante anche per un metro ottimista. Il migliore trimestre primaverile di sempre, per distacco abissale, e vabbè, sono cose già lette. [Questa](https://twitter.com/artman1033/status/1420142103133396998), meno:

<blockquote class="twitter-tweet"><p lang="en" dir="ltr"> has surpassed <br>2020 TOTAL YEAR sales <br>in THREE QUARTERS in 2021 <a href="https://t.co/wgjV6mhuEM">pic.twitter.com/wgjV6mhuEM</a></p>&mdash; artman1033 (@artman1033) <a href="https://twitter.com/artman1033/status/1420142103133396998?ref_src=twsrc%5Etfw">July 27, 2021</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

I primi tre trimestri del 2021 hanno reso più dei quattro trimestri del 2020.

(Si possono raccontare altre cose simpatiche su questi risultati, tipo che il trimestre primaverile – il peggiore nella tradizione Apple – ha battuto il trimestre natalizio 2017, che è tradizionalmente il migliore. Andiamo avanti, che si fa tardi).

La luna è [quest’altra](https://twitter.com/asymco/status/1420142068379308035):

<blockquote class="twitter-tweet"><p lang="en" dir="ltr">All the last three quarters were more profitable than the pre-pandemic holiday quarter. <a href="https://t.co/8oyoB5TNlC">pic.twitter.com/8oyoB5TNlC</a></p>&mdash; Horace Dediu (@asymco) <a href="https://twitter.com/asymco/status/1420142068379308035?ref_src=twsrc%5Etfw">July 27, 2021</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

I profitti degli ultimi tre trimestri sono stati superiori a quello dell’ultimo trimestre non pandemico.

*Significa che Apple sfrutta cinicamente il virus per fare soldi alle spalle delle persone?*

No. Significa che c’era una debolezza negli strumenti usati per lavorare, imparare, divertirsi. La pandemia, semplicemente, l’ha rivelata.

*Forse si tratta di una situazione strettamente legata alla pandemia, che poi finirà?*

No. Il trend cesserà quando le persone saranno finalmente equipaggiate come avrebbero sempre dovuto essere, non quando finirà la pandemia. Anche perché sono ancora in vigore misure restrittive in tantissime nazioni del mondo, ma il blocco totale monolitico di durata indefinita oramai è una cosa del passato. Praticamente chiunque ha avuto modo di rifare almeno un salto in ufficio e magari continuare a lavorare da casa, ma per decisione invece che per necessità. Secondariamente, chi doveva aggiornare l’equipaggiamento per stare dietro a nuovi requisiti lo ha già fatto da un pezzo, mentre invece la crescita continua. Per non parlare del numero di vaccinati, che nelle nazioni evolute inizia a raggiungere livelli importanti. La pandemia di oggi non è quella del marzo 2020.

*Ma quindi è solo una questione di lavoro?*

No. I servizi, tra cui Apple Fitness o Apple Arcade, sono cresciuti ancora più dell’hardware, a livelli record. C’è proprio un altro modo di interpretare il computing nella vita oltre che nel lavoro e, di nuovo, il virus c’entra relativamente e sempre meno.

Tante parole dei consulenti su *data transformation*, *digital transformation*. Apple, semplicemente è puntuale all’appuntamento storico con il digitale che finalmente, quarant’anni dopo, *comincia* a mantenere le sue promesse di trasformazione positiva della società, del lavoro, della vita. E fornisce attrezzature adeguate al momento.

*Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile.*