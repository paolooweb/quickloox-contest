---
title: "La fase del multitasking"
date: 2022-07-07T01:05:32+01:00
draft: false
toc: false
comments: true
categories: [Internet]
tags: [multitasking]
---
Invece di comprare libri e leggere articoli sulla produttività e sull’organizzazione del lavoro, ricorro all’università della strada e imparo tutto quello che c’è da imparare sul multitasking e il suo confronto con il lavoro lineare. Dalle mie figlie.

Si spingono sul monopattino e intanto mangiano un gelato, tengono in mano l’inseparabile gioco del momento, ciascuna ascolta l’altra mentre entrambe parlano con un genitore a scelta, tengono d’occhio ogni dettaglio di quello che hanno intorno e chiedono un altro gelato, dell’acqua, dove andiamo, quando torniamo, di chi è il turno di iPad stasera. Tutto contemporaneamente.

Dovessi svolgere un terzo delle stesse cose, collasserei. Semplicemente, i bambini sanno fare multitasking; non dico in modo innato, ma certamente le mie figlie non sono state incoraggiate. Il gelato ogni tanto cola, il giocattolo ogni tanto cade, però complessivamente tutte le attività vengono portate a termine. La sera, quella che gioca con iPad lo fa in modo producente intanto che tiene d’occhio l’altra e, se questa ha scelto un cartone animato, fa osservazioni pertinenti sul cartone intanto che gioca.

La differenza tra il multitasking infantile e quello adulto è il *context switching*, il momento di passaggio da una attività a un’altra. Per loro è prossimo allo zero, per un adulto è questione più complessa che probabilmente chiama in causa una concentrazione di livello superiore (o una fatica maggiore nell’attivarla). Non c’è dubbio comunque che il multitasking sia potenzialmente produttivo e che la presunta superiorità del monotasking sia, appunto presunta. Naturalmente i pregi e i difetti restano: al multitasking va chiesto di sbrigare più compiti per i quali sia adatta una concentrazione relativa; al monotasking vanno richiesti i dettagli e la precisione, consci della massa di attività ancillari che vengono differite a un *dopo* non sempre ideale.

Se prima diffidavo dei programmi per scrivere con la modalità foglio bianco e schermo vuoto, dopo le vacanze – delle figlie – ne diffido maggiormente. Se scrivo, passo a Slack per inviare una risposta importante e poi mi rimetto a scrivere, sto facendo multitasking e lo sto facendo bene. Sicuramente finirò di scrivere più tardi, ma il tempo necessario a completare tutte le attività della giornata in modo ottimale è invariante, che si svolgano in parallelo o che si mettano in fila indiana.

Resta da capire se il multitasking infantile sia una fase della crescita, che poi viene superata dall’approccio adulto, o una di quelle meravigliose capacità di un cervello ancora in fase di sviluppo, elastico, modellabile, agile, scattante, che poi si tende a perdere con il passare dell’età.