---
title: "Lettera libera"
date: 2015-07-05
comments: true
tags: [FreeCode, SourceForge, AppStore, opensource, Denis-Courmont]
---
Altri la chiamerebbero lettera *aperta*, ma visto che riguarda il software libero, perché no. Premesso che:<!--more-->

* I depositi di software libero stanno passando una certa crisi di identità. Per esempio [Freecode è fermo da oltre un anno](http://freecode.com/about); SourceForge è ricorsa a [comportamenti discutibili](http://arstechnica.com/information-technology/2015/06/sourceforge-locked-in-projects-of-fleeing-users-cashed-in-on-malvertising/) che mettono in dubbio la sua affidabilità come punto di riferimento generale per lo *hosting* di software libero in rete.

* App Store sicuramente non contiene migliaia o milioni di *app* con licenza libera. Certamente [ne contiene decine](https://macintelligence.org/posts/2012-07-06-liberta-e-libertuccia/). È possibile mettere software libero su App Store, ma le circostanze attuali lo rendono evidentemente poco prioritario per tutti.

* Di cretini in giro ce ne saranno sempre. La loro capacità di incidere, comunque, [dal rancoroso Rémi Denis-Courmont a certi commentatori un tanto al chilo](https://macintelligence.org/posts/2012-07-08-liberta-va-cercando/) va scemando (come è inevitabile trattandosi di scemi).

Propongo che:

* **App Store diventi un luogo privilegiato di *hosting* di software libero**.

A questo scopo, servirebbe che:

* Apple pensasse a una categoria apposita su App Store, con termini e condizioni eccezionalmente ammorbiditi rispetto al resto dell’offerta. Dovrebbe essere più che chiaro al frequentatore dello Store che dentro la categoria del software libero può trovarsi software non completamente conforme ai requisiti stringenti posti da Apple sulle altre categorie. Al tempo stesso, la natura aperta del software libero lo rende straordinariamente controllabile dal punto di vista della sicurezza; il software libero non dovrebbe compromettere la sicurezza globale di App Store né dei suoi frequentatori.

* Gli estensori delle licenze di software libero elaborassero una variazione o perlomeno una deroga ad alcune licenze *open source* attuali. Per farla strabreve, molti programmi *open source* non stanno su App Store perché a) non è ordinariamente possibile scaricare un programma da App Store e distribuirlo a un amico e b) da App Store non è possibile recuperare il codice sorgente del programma. Si tratta di una situazione anacronistica; essendo software *open source*, il codice sorgente si può trovare facilmente sul sito degli autori del programma (per non dire che App Store potrebbe offrire un *link* apposito). Se un programma è scaricabile da chiunque gratuitamente a proprio nome, l’impossibilità di ridistribuirlo non è limitante; chiunque lo voglia potrà sempre scaricarlo quando lo desidera e basterà indicargli un *link* che gli permetterà di farlo.

Ritengo che:

* Se le parti in causa facessero un piccolo passo in avanti come descritto, ne deriverebbero grandi vantaggi per il software libero, per gli utilizzatori di software libero, per App Store e per gli utilizzatori di App Store.
