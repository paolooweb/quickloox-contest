---
title: "Lavori in corsa"
date: 2024-03-28T01:43:12+01:00
draft: false
toc: false
comments: true
categories: [Blog]
tags: [Comma]
---
In teoria, dopo il [lavoro di preparazione del frontend](https://macintelligence.org/post/2024-03-27-graditi-i-commenti/), anche il backend dei commenti potrebbe essere funzionante sulla macchina virtuale che pubblica il blog.

Chi abbia pazienza e voglia di sopportare eventuali delusioni è invitato a provare a inserire commenti e vedere come va. Grazie di cuore da subito!

*Perché non te lo fai tu?*

Perché al momento lavoro vicino al mio limite di complessità gestibile. Per stasera sono in riserva con le risorse di concentrazione e comunque domani eseguirò prove ed eventuale debug. Sono ragionevolmente sicuro di avere fatto le cose nel modo giusto, ma potrebbe non essere vero.

Per commenti, richieste, suggerimenti, critiche, interviste senza veli sono sempre reperibile sul mio [gruppo Slack](https://godel.slack.com) (fammi avere tramite un qualunque canale una email utilizzabile e mando l’invito); su iMessage a lux@mac.com (che è anche indirizzo di posta valido); su Telegram (Lucio Bragagnolo); su Signal (lux.19); su Element di Matrix (Lucio Bragagnolo); su Twitter/X (@loox); su Mastodon (@loox@mastodon.uno); su Facebook (Lucio Bragagnolo); su Instagram (goedelfan) e da qualche altra parte ancora, che non ricordo, però se serve scartabello. Su richiesta aggiungo qualsiasi altra messaggistica che non sia Teams. Possiamo anche metterci d’accordo e trovarci su Irc, per dire. Insomma, sono raggiungibile anche se i commenti qui sono temporaneamente inagibili.