---
title: "Sbagliato si impara"
date: 2016-06-03
comments: true
tags: [iPad, Note, Mac, Dropbox, Jobs]
---
Una esperienza molto concreta che posso aggiungere al mio [sproloquio](https://macintelligence.org/posts/2016-05-17-e-ora-qualcosa-di-completamente-diverso/) su come cambiano le interfacce e la modalità di utilizzo.

In questi giorni ho lavorato da iPad molto più del solito e una delle missioni più importanti consisteva nel produrre articoli a partire da una serie di materiali riguardanti un evento. lista di Link, lista di nickname Twitter, immagini, testi.

Il lavoro non aspetta e di volta in volta mi trovavo a produrre su Mac oppure su iPad. È successo anche di apportare ritocchi mentre attendevo alla classica fermata dell’autobus, su iPhone. Sempre a partire dallo stesso insieme di materiali, da cui di volta in volta prelevavo parti differenti oppure compivo elaborazioni che ampliavano l’insieme stesso.

Viene naturale predisporre il lavoro dentro una cartella condivisa su tutti gli apparecchi. Per esempio Dropbox (io *adoro* Dropbox ed è un peccato che [Steve Jobs non sia riuscito ad acquisirlo](https://gigaom.com/2015/10/01/its-time-to-revisit-apple-buying-dropbox/)) o anche iCloud Drive, o Google Drive… le opzioni, oggi, abbondano. È un percorso che viene persino istintivo da quanto è abituale.

Invece ho fatto una cosa diversa. Ho inserito tutti i materiali *dentro una nota*.

Con l’attuale Note di OS X e iOS, è un attimo. Qualunque modifica si apporti, viene sincronizzata automaticamente. Estrarre materiali dalla nota, o inserirli, è un momento. Lavorare nella nota, svolgere ricerche, apportare ritocchi è semplice e immediato.

Sfido chiunque a sostenere che si tratti di un flusso di lavoro più complesso o meno efficiente di una cartella condivisa di Dropbox. Per esperienza concretissima, posso dire che non è così e i due sistemi sono tranquillamente alla pari per risultato. In un caso metto tutto in una cartella, nell’altro in un documento (o in una *app* a seconda di come la si vuole vedere).

Certo, le cartelle si usano da trent’anni, le *app* da cinque. È del tutto legittimo che qualcuno si trovi meglio con le cartelle e le senta più consone al proprio modo di lavorare. Ma dire che l’approccio di iPad sia sbagliato, perché non ci lavora per cartelle, no. È diverso e in situazioni come questa esattamente equivalente. E se offre lo stesso risultato a parità di complessità (lo offre) può non piacere. Ma *sbagliato* è un’altra cosa. Qui abbiamo un modo in più di lavorare per lo stesso esito. Si può ignorare, o imparare.