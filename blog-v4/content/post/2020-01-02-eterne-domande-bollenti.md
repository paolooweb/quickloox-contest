---
title: "Eterne domande bollenti"
date: 2020-01-02
comments: true
tags: [Venerandi, Kellermann, Maggi, Hofstadter, Gödel, Escher, Bach, Adelphi, Friedl, Mastering, Regular, Expressions]
---
**Edoardo** ha anticipato gli anni Venti con [un post su Facebook](https://www.facebook.com/EdoardoVolpiKellermann/posts/10220608201665182) che ha suscitato alcuni commenti molto interessanti e che meriterebbero di portare la discussione lontano, anche sul versante del fare:

<iframe src="https://www.facebook.com/plugins/post.php?href=https%3A%2F%2Fwww.facebook.com%2FEdoardoVolpiKellermann%2Fposts%2F10220608201665182&width=500" width="500" height="480" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true" allow="encrypted-media"></iframe>

Staccare la discussione da dove è originata non sta bene e riporterò lì le informazioni salienti di questo post. Il fatto è che Facebook è un luogo senza memoria e questo pretesto è ottimo per mettere già in modo ordinato alcune cose che altrimenti continuerebbero a ballare nella testa.

##Un ebook di *Gödel, Escher Bach: An Eternal Golden Braid* esiste.

Fa schifo, suppongo sia clandestino ed è nient’altro che un pessimo *dump* del [libro](https://www.amazon.it/Godel-Escher-Bach-Eternal-Golden/dp/0465026567) in formato ebook, senza alcuna cura di nulla. Lo ha fatto un cinese. Però esiste, [è su GitHub](https://github.com/ccpaging/books) e quindi chiunque potrebbe provare a partire da lì se non altro per inserire un po’ di tipografia che ce ne sarebbe bisogno.

Ovviamente Edoardo ha ragione, non c’è alcun ebook relativo all’[edizione italiana](https://www.adelphi.it/libro/9788845905933). Magari si potrebbe compiere uno sforzo collettivo e farne un ebook clandestino… è anche un testo che si presta a creare in ePub molto più che l’edizione elettronica del libro di carta.

##L’autore di *Gödel, Escher, Bach: an Eternal Golden Braid*, Douglas Hofstadter, ha impaginato e composto il libro autonomamente.

Il libro è uscito nel 1980. Al tempo, [TeX](https://texfaq.org/FAQ-whatTeX), il linguaggio di composizione tipografica di [Donald Knuth](https://www-cs-faculty.stanford.edu/~knuth/), era nel mezzo dello sviluppo della seconda edizione, arrivata nel 1982. Knuth ha iniziato a lavorarvi nel 1978, mentre Hofstadter era già a buon punto nella lavorazione del libro. Anche volendo, non avrebbe fatto in tempo a usare TeX. Invece [ha impiegato TV-Edit](https://news.ycombinator.com/item?id=20007112), un editor [scritto nella sua terza versione da Pentti Kavarna](http://texteditors.org/cgi-bin/wiki.pl?TVEDIT). Linko per scrupolo: l’introduzione sulla mia edizione del 1980, una delle prime se non la prima, conferma. Non ho trovato emulatori, ma [una scheda con i comandi principali](https://stacks.stanford.edu/file/druid:sh140fb6558/sh140fb6558.pdf).

##A volte è difficile persino la traduzione, altro che l’ebook.

Ho lavorato per qualche tempo in una piccola casa editrice che deteneva i diritti della prima edizione di [Mastering Regular Expressions](http://shop.oreilly.com/product/9781565922570.do), di [Jeffrey Friedl](https://www.oreilly.com/pub/au/666).

Era un libro straordinario, non per niente giunto oggi alla [terza edizione](http://shop.oreilly.com/product/9780596528126.do), e tentammo di tradurlo. Fu un disastro.

L’editore originale, O’Reilly, non poteva mandarci alcun sorgente: il libro era stato composto tipograficamente dall’autore. Lo contattammo e lui, di malavoglia, inviò un paio di *blob* binari senza alcuna altra spiegazione. A fatica arrivammo a capire che Friedl, esperto (a dire poco) di Unix, si era costruito un processo *ad hoc* che tra `awk`, `sed`, `tron`, `troff` e mille altri comandi produceva come output un file PostScript che corrispondeva al libro a partire da un sorgente che non ci avrebbe spedito. Né avremmo mai avuto nozione di come esattamente funzionasse il suo processo.

Va aggiunto che *Mastering Regular Expressions* conteneva un intero set di caratteri e segni grafici creati dall’autore per evidenziare, circoscrivere, contrassegnare, segnalare pezzi essenziali dell’esposizione o del codice.

Come chiunque all’epoca, producevamo libri con InDesign: rifare tutta la simbologia ideata da Friedl e introdurla a mano in un impaginato (gran parte di essa andava tra una riga e un’altra, fuori dai margini, attorno a gruppi di caratteri eccetera) andava oltre ogni *budget* ipotizzabile.

Le idee per superare queste difficoltà c’erano, ma il denaro per farcela con qualità e tempi accettabili no. Se al tempo fossero andati per la maggiore gli ebook, a partire da quello che avevamo a disposizione, probabilmente avremmo subito ingerito del cianuro per risolverla in fretta.

Tutto questo mi porta a un importante [post di Fabrizio Venerandi sullo stato dell’editoria in ebook](http://www.quintadicopertina.com/fabriziovenerandi/?p=1452), di cui farò menzione prossimamente. (Questo link è stato gentilmente velocizzato da Handoff: scrivo su Mac ma la pagina era aperta su iPad. Handoff è una bellezza, anche se non c’entra niente).