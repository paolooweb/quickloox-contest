---
title: "Non-recensioni: iPad Pro 12,9” / 2"
date: 2018-11-28
comments: true
tags: [iPad, Pro]
---
Dicevo [ieri](http://www.macintelligence.org) che Face ID di fatto elimina o quasi il fastidio di avere il codice di sblocco. E da questo punto di vista, proprio è diverso guardare per un attimo lo schermo che dover poggiare il dito per l’impronta.

Face ID viene in aiuto anche per l’attenzione inversa, come l’ho chiamata: iPad Pro fa attenzione a quando gli facciamo attenzione. Una delle cose che mi hanno disorientato un attimo all’inizio è stata la mancanza dell’impostazione del tempo di funzionamento prima di andare in stop (per me i quindici minuti erano ottimali). Con Face ID, sostanzialmente la macchina va in stop quando vede che nessuno le bada. Per rientrare non ci sono tasti e basta toccare per poi  scorrere dal basso.

Grazie al backup dell’iPad precedente ho acquisito in pochi minuti la configurazione che avevo, con tanto di app e dati. Anche se non posso provarlo, sono convinto che iPad Pro abbia anche provveduto a caricare la versione aggiornata delle app. O sono stato così “fortunato” da avere sulla macchina app che si sono tutte fermate a iOS 9. Però mi sembra strano. La cosa che non sapevo è che, avessi avuto un altro apparecchio iOS moderno a disposizione, avrei potuto configurare il nuovo iPad Pro semplicemente lasciandogli copiare il contenuto dell’apparecchio in questione. Notevole, come il fatto di poter sbloccare Mac con iPad.

La tenuta della batteria, il sonoro che tutti dicono notevole, devo ancora provarli. Le foto, da incompetente, sono molto definite e così i video. La fotocamera, del resto è di quelle che sporgono leggermente dalla scocca. No, tenere appoggiato iPad sul tavolo con la fotocamera che sporge non dà alcun problema.

Sul momento sono veramente impressionato, già lo scrivevo. È un pezzo di tecnologia che non ho idea di come si possa superare in modo sostanziale. Anche dove fosse più sottile e più leggero… di quanto? Specie con lo schermo grande. Ho provato a maneggiarlo tenendolo per un angolo e non sembra soffrire di forze di torsione. Però non so quanto spazio ci sia ancora per avere resistenza sufficiente con meno spessore e meno peso.

Non posso dire nulla di Apple Pencil perché non sono interessato. Le mie capacità grafiche sono limitate, gli appunti a mano sono belli ma preferisco avere una tastiera. Della Smart Folio Keyboard nemmeno posso dire, perché non mi piace e non la prendo. Dopo molte prove, trovo i tasti troppo separati e il tocco ha qualcosa che non mi convince. Il piano è prendere una cover che possa inclinare leggermente lo schermo in modo da poterlo vedere bene mentre è appoggiato sul tavolo, intanto che scrivo con una tastiera Bluetooth per me confortevole. Quando esco con iPad Pro, mi porto dietro la tastiera wireless di Mac mini. Scrivo su due sistemi diversi sempre con la stessa tastiera. Lo trovo estremamente ergonomico.

Come dicevo, la mia indecisione nello scegliere tra 11” e 12,9” veniva anche dalla grande frequenza con la quale tenevo iPad in mano, lavorando con i pollici, a tastiera divisa in due. È una cosa che naturalmente viene meglio con l’11”.

Apple ha “risolto” il problema per me: sugli iPad Pro [non c’è la divisione in due della tastiera](https://support.apple.com/en-us/HT207521). In pratica, ho fatto bene – a posteriori – a scegliere il 12.9”. Altri troveranno naturalmente praticissimo il formato 11”, che è quello tradizionale. Li comprendo benissimo, e però voglio godermi l’esperienza dello schermo *molto* più grande. Dovrebbe essere tutta produttività in più.

iPad Pro 2018 è una macchina incredibile e non ho abbastanza aggettivi per esprimerlo. Non consiglio mai di cambiare macchina per partito preso; sempre quando c’è bisogno. Se c’è bisogno, nessuna esitazione: è un capolavoro.