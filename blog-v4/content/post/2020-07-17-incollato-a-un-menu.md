---
title: "Incollato a un menu"
date: 2020-07-17
comments: true
tags: [BBEdit]
---
La scorsa notte, al lavoro su una montagna di caratteri, ho scoperto i copiaincolla speciali di [BBEdit](https://www.barebones.com/products/bbedit/).

 ![Un menu per palati fini di copiaincolla](/images/bbedit-paste.png  "Un menu per palati fini di copiaincolla") 

Credo che mi abbiano fatto guadagnare almeno un’ora di sonno. Ho anche l’impressione che possano tornare utili in un numero di situazioni più ampio della mia. Starò loro molto vicino per carpirne i loro segreti.