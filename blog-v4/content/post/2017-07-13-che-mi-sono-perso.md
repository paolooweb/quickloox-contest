---
title: "Che mi sono perso"
date: 2017-07-13
comments: true
tags: [giochi, Mac, MacGamerHQ, Hearthstone]
---
Fare uscire una lista dei [cento migliori giochi oggi esistenti per Mac](http://www.macgamerhq.com/guides/best-mac-games/) a metà luglio è un colpo bestiale di marketing e una coltellata intercostale per chi è tenuto a produrre.

Mica tanto per queste settimane, quanto in generale. Mac non è considerato piattaforma di *gaming*, però a guardare la lista non mi viene da soffrire. Su certe cose sono arrivato, come [Dwarf Fortress](http://www.bay12games.com/dwarves/), Hearthstone, World of Warcraft. Me ne mancano decine e decine, ciascuna delle quali chiede una consistente fetta di vita per essere apprezzato.

Impossibile arrivare su tutto e non c’è bisogno di tirare in ballo l’anagrafe: anche un gatto adolescente con davanti nove vite non ha abbastanza tempo per praticare la parte interessante di giochi della lista (che oltretutto condivido quasi del tutto: mancano alcuni titoli, per il mio gusto).

La paginata di *MacGamerHQ* costa perché mi mostra non solo dove non arriverò, ma anche e soprattutto che cosa mi sono perso finora. I giochi belli su Mac sono legioni.