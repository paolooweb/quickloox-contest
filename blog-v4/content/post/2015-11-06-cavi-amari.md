---
title: "Cavi amari"
date: 2015-11-06
comments: true
tags: [Usb, Amazon, Beung, Pixel, Chromebook, Ligthning]
---
Prima della prossima critica ai cambi di connettore o ai prezi di cavo Apple, rivolgere un pensiero a Benson Leung, ingegnere di Google che si è messo a [recensire su Amazon i cavi Usb Type C in vendita](http://www.amazon.com/gp/cdp/member-reviews/A25GROL6KJV3QG/ref=pdp_new_read_full_review_link?ie=UTF8&page=1&sort_by=MostRecentReview&tag=viglink125432-20#R1189CCK1UXGT5).<!--more-->

Per la ragione che costano pochissimo. E non rispettano le specifiche. Si perdono dati, le unità non si caricano, si verificano guasti e tante altre piacevolezze.

È arrivato a pubblicare le [istruzioni per testare i cavi su Chromebook Pixel](https://plus.google.com/+BensonLeung/posts/jGP5249NppF).

Ora, tutti vogliamo pagare un cavo due soldi e poi passare un pomeriggio a capire se funziona o meno, vero? Specie gli acquirenti di un nuovo MacBook 12”, per l’appunto munito di connettore Usb Type C.