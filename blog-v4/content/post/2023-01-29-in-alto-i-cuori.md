---
title: "In Alto i cuori"
date: 2023-01-29T02:29:18+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [Alto’s Odyssey, Lidia]
---
Le ragazze crescono. Ieri sera per la prima volta ci siamo confrontati su [Alto’s Odyssey](https://www.altosodyssey.com) sul televisore governato da tv.

È stato un pezzo di serata piacevolissimo, tra risate e chiacchiere, con un gioco tranquillissimo per quanto anche intrigante nella sua semplicità, adatto a qualsiasi età salvo sufficiente coordinazione oculo-manuale, disponibile tramite Apple Arcade oltre che su iOS e iPadOS.

È stato un confronto, non una sfida. Lidia ha dichiarato da subito di avere interesse solo a migliorare il suo record personale, senza gare. Lo stesso ho fatto io, che peraltro in giochi di questo tipo non eccello e che in un ambito di sfida avrei potuto perdere con facilità.

Arriveranno giochi più complessi e interazioni più intense. Per ora siamo fortunati, con il cuore pieno di emozione per bambine che crescono e i cui progressi si constatano anche dal loro rapporto con il gioco e tutto l’indotto relativo, dalla capacità di superare l’errore a quella di saper usare la giusta misura di coinvolgimento e tempo.

Ho sempre detto loro che possiamo giocare tutto il tempo che vogliamo, se siamo capaci di smettere di farlo al momento giusto, e forse siamo sulla buona strada.