---
title: "Segnali di vita intelligente"
date: 2024-03-07T18:08:22+01:00
draft: false
toc: false
comments: true
categories: [Software, Internet]
tags: [XOXO, Cauldron VTT]
---
Immagina di lavorare con gente cui mandi la schermata di una piattaforma di collaborazione e, grazie alle preziose impostazioni aziendali, non vede le immagini e al posto degli emoji vede pallini colorati. Deprimente, vero?

Per fortuna un po’ di web libero rischiara la mente e ripristina la fiducia nella presenza di un pochino di design e intelligenza di comunicazione atta a salvarci dal disastro intellettuale.

Per esempio, mi congratulerei volentieri con il progettista del [sito di XOXO](https://2024.xoxofest.com), festival che quest’anno andrà in scena per l’ultima volta. Si fa molto in fretta, la prima sensazione è di essere presi in giro. Poi invece te lo ricordi perfettamente e allora vuol dire che il sito ha raggiunto lo scopo.

E poi. Sono molto soddisfatto di [Roll20](https://roll20.net/welcome) per le sessioni di gioco di ruolo online, tuttavia c’è sempre da tenere d’occhio il panorama delle alternative, per cogliere iniziative interessanti. Così finisco sul sito di [Cauldron VTT](https://www.cauldron-vtt.net), certamente meno dotato di Roll20 ma open source con tanto di codice a disposizione, e faccio per registrarmi per dare un’occhiata.

Solo che insieme ai campi da riempire appare una frasetta. Più o meno:

>Se sei qui solo per guardarti attorno, puoi usare questi account dimostrativi al posto di registrarti.

Intelligenza artificiale arrivata dal futuro, chiaroveggenza o user experience pensata con intelligenza e pragmatismo? Ho avuto più di quello che volevo e ho speso metà tempo e fatica di quello che mi aspettavo. È chiaro che non mi uscirà più dalla testa. Successo per chi ci ha pensato.