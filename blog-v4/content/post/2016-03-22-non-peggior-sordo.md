---
title: "Non c’è peggior sordo"
date: 2016-03-22
comments: true
tags: [Radio24, Apple, iPhone, watch, ResearchKit, CareKit, tvOS]
---
La segnalazione mi arriva da [Mario](https://multifinder.wordpress.com/2016/03/21/ci-siamo-informati-bene/) che ringrazio, anche se non so se veramente avrei voluto ascoltare la sequenza di banalità, scontatezze e luoghi comuni che è venuta fuori da [Radio24](http://www.radio24.ilsole24ore.com/programma/mix24/trasmissione-marzo-2016-100230-gSLAFOsIaB), per l’occasione ribattezzata Radio Cicaleccio, con rispetto per le cicale.

Dal minuto 36:10, sembra di sentire due che scelgono i carciofi al mercato e intanto parlano di Apple, con la medesima attenzione (rispetto per il mercato, per chi vende al mercato, per chi compra, per i carciofi).

Spezzoni di frasi così: *Apple ha un problema o forse no* (fantastica analisi); *noi lo avevamo segnalato mesi fa* (e figurati), la vendita di iPhone è in frenata (che sia il modello più venduto al mondo per un momento è irrilevante).

*Il mercato è saturo di modelli e marchi* (ma no! Invece l’anno scorso c’era il deserto), così che cosa viene in mente ad Apple? *facciamo un telefonino low-cost* (la Ryanair degli *smartphone*).

Ci vuole, perché *costano un botto gli iPhone* (e ti pareva? Mai successo prima, d’altronde). Questo significa però che l’azienda della Mela *ha un problema futuro di ricavi* (eh sì, magari fattureranno duecento miliardi invece che duecentotrenta. Vuoi mettere?).

*È la fine dell’immagine di un club esclusivo della tecnologia* (certo, un miliardo di apparecchi attivi sono un club esclusivo). *Considerate l’acquisto di un PC di alta gamma e un MacBook e vedrete che c’è una differenza di prezzo notevole* (direttamente dagli anni novanta, ecco a voi la notizia).

Ma c’è la *killer application*: anche loro puntano *sull’intelligenza artificiale e sull’auto, sui wearables* (ecco, uno scoop. D’altronde watch deve ancora uscire…).

Mi raccomando: *è la fine di un’epoca, non la fine di Apple*. Eh già, perché nelle loro menti allenate Apple rischia di scomparire. Dopotutto è solo l’azienda tecnologica di maggior valore al mondo. Pensa se fosse la seconda: a un passo dal baratro.

E questa è Radio24, con una reputazione di qualità. Immaginarsi il resto. Anche loro finiti nel gorgo del giornalismo di oggi, dove i fatti non contano più: l’importante è dire alla gente quello che la gente vuole sentire, e fa niente se sono chiacchiere vuote. Non c’è peggior sordo di chi ha la sordità parziale, convinto di sentirci, mentre certe frequenze passano via dimenticate.