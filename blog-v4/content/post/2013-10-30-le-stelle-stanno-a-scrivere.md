---
title: "Le stelle stanno a scrivere"
date: 2013-10-30
comments: true
tags: [iPad]
---
Ha perfettamente ragione [Riccardo](http://morrick.me):

<blockquote class="twitter-tweet"><p><a href="https://twitter.com/gruber">@gruber</a> You certainly don’t need me to tell you this, but: your essay on the Apple event is just stellar. Thank you.</p>&mdash; Riccardo Mori (@morrick) <a href="https://twitter.com/morrick/statuses/394265869163184128">October 27, 2013</a></blockquote>
<script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>

L’articolo di John Gruber [Pensieri e osservazioni riguardanti l’evento Apple di presentazione di iPad Air e iPad mini Retina](http://daringfireball.net/2013/10/this_weeks_ipad_event) è esattamente *stellare*. Consigliatissimo. Qualche estratto:

>La mia impressione che ci sia voluto molto per arrivare alle app e agli aggiornamenti del sistema operativo completamente gratis da un punto di vista contabile. E questo, credo, riguarda anche il perché iLife e iWork sono gratis con l’acquisto di un nuovo apparecchio o con il possesso di una versione precedente. È la ricaduta delle questioni contabili, che semplicemente non permettono di offrire gratis a chiunque queste app.

Su iPad Air:

>I nuovi iPad mi colpiscono come esempi eccellenti della leadership di Tim Cook. […] Non è solo creare hardware nuovo e di tendenza, ma di farlo in grandi numeri, con alta affidabilità e prezzi accessibili. Ho potuto parlare con Cook qualche minuto nell’area di prova e, a domanda, gli ho risposto come io sia rimasto sorpreso di vedere iPad mini passare allo schermo Retina e anche avere il processore A7, in un solo anno, senza il bisogno di intervenire sul peso o sullo spessore per inserire una batteria più grossa come è accaduto per iPad 3 e 4 appena diciotto mesi fa. Cook ha sorriso e ha detto qualcosa tipo “Da allora abbiamo imparato molto”.

Sulla concorrenza:

>iPad non ha la concorrenza nel modo in cui ne ha iPhone. Decine di milioni di persone usano telefoni Android di lusso in modo simile ada altre decine di milioni di persone che usano iPhone. Non ci sono così tante persone – oggi? – che usano Kindle Fire, Galaxy Tab, Nexus o Surface come alternative a iPad. Da qui le massicce [discrepanze tra la quota di mercato e il traffico su Internet](http://techcrunch.com/2013/10/22/apple-announces-170m-ipads-sold-with-81-tablet-usage-share-and-475k-apps/).

iWork:

>Un sacco di gente si è lamentata delle regressioni funzionali nelle versioni Mac di iWork. Disappunto giustificabile; a nessuno piace vedersi togliere funzioni software su cui fa affidamento. Ma data la recente storia di Apple – Final Cut Pro X e iMovie ’08 per fare due esempi – nessuno dovrebbe sorprendersi. Non credo che chiunque in Apple prenda alla leggera queste regressioni, ma non sono un errore. […] La priorità più alta era chiaramente la parità funzionale per iPhone, iPad , web e Mac. Nessun’altra piattaforma al mondo ce l’ha. […] Dove ci sono priorità chiare, le funzioni secondarie sono sacrificabili. Credo che l’impegno continuativo di Apple su Mac sia chiaro. […] Ma iOS è la piattaforma primaria e per iOS è meglio la situazione in cui iWork parte in parità ovunque che la situazione precedente, dove le app su iOS facevano un sottoinsieme di quelle su Mac.

E la parte più interessante, sulla Apple post-Jobs:

>Apple è un’azienda diversa oggi da quello che sarebbe se ci fosse ancora Jobs. Ma dove stanno le differenze? Faccio un nome: iOS 7. Nessuno può dire di conoscere i giusti di Jobs – nessuno poteva e questo faceva di Steve Jobs la persona che era – ma posso tirare a indovinare e dico che non avrebbe supportato questa direzione. […] Un segno tangibile che Tim Cook, quando dice che il consiglio di Jobs era non chiedersi che cosa avrebbe pensato lui ma che cosa sia meglio per Apple, lo pensa veramente. Ma credo che Steve Jobs avrebbe semplicemente amato l’hardware che Apple ha mostrato.

Che prosegue in questo modo:

>Continuo a pensare a quel vecchio video del 1990 di una fabbrica di NeXT in California, [la macchina per costruire le macchine](http://www.youtube.com/watch?v=dSj6kvv7_Sg). Guardato quello e letto il [breve pezzo scritto all’epoca da Fortune](http://money.cnn.com/magazines/fortune/fortune_archive/1990/02/26/73121/), difficile non vedere il [Mac Pro assemblato negli Stati Uniti](http://www.youtube.com/watch?v=IbWOQWw1wkM) come il culmine dello stesso sogno.

Da leggere. Anche il resto.