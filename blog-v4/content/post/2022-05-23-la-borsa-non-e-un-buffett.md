---
title: "La Borsa non è un Buffett"
date: 2022-05-23T03:26:18+01:00
draft: false
toc: false
comments: true
categories: [Internet]
tags: [Buffett, Warren Buffett, Berkshire Hathaway, Apple, Nasdaq]
---
Il titolo Apple ha discretamente perso valore negli ultimi tempi e Warren Buffett, uno dei più grandi investitori di tutti i tempi, l’oracolo di Omaha, non fa che comprare ancora più azioni.

Nell’ultimo giro, la sua Berkshire Hathaway [ha raccolto altre tre milioni e ottocentomila azioni Apple](https://finance.yahoo.com/news/buffett-ups-chevron-apple-holdings-223220603.html), che adesso rappresenta quasi il quarantatré percento del suo portafoglio azionario.

Da notare che Buffett ha passato i novant’anni e continua a mietere profitti finanziari, certamente anche consigliato bene ma insomma, la sua è una traiettoria veramente commendevole seppure non si occupi di hardware e software.

Interessante anche che Berkshire Hathaway abbia ripreso a comprare dopo avere trascorso il 2021 limitandosi a vendere.

Il suo successo nell’agire in controtendenza con l’indice azionario americano Nasdaq, comprando quando tutti vendono e viceversa, dimostra coraggio e sangue freddo, nonché – ci interessa di più – la piena salute di Apple oltre le fluttuazioni del pacchetto azionario, che accadono e un investitore saggio sa osservare con distacco.

Storicamente, dopo un Wwdc il titolo Apple perde visto che non vengono presentati tricorder o astronavi a propulsione iperspaziale, o un gadget inutile, o un Mac a novantanove dollari.

Se succedesse anche quest’anno, in questa situazione, a chi gioca in Borsa converrebbe veramente acquistare, con fiducia e coraggio e sangue freddo.