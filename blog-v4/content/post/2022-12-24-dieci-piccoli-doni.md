---
title: "Dieci piccoli doni"
date: 2022-12-23T04:08:22+01:00
draft: false
toc: false
comments: true
categories: [Internet]
tags: [Natale]
---
Caro babbo Natale,

Quest’anno non sono stato particolarmente buono, però in qualche modo sento di avere diritto a trovare qualcosa di gradito sotto l’albero. Se non ti offendi, azzardo un piccolo elenco di regali che mi piacerebbero.

1. Bug riproducibili
2. Benchmark applicabili alla vita vera
3. Esempi brillanti di ricorsività
4. Privacy come la descriveva Steve Jobs: spiega in modo chiaro e semplice che cosa succede
5. Una *vera* intelligenza artificiale, anche piccola come un granello di senape
6. Messaggi in italiano corretto
7. AppleScript 3.0, con la stretta integrazione dei Comandi Rapidi
8. Software che liberi (il software libero è importante ma non basta)
9. Lo spirito di Internet, quello di una volta
10. Interesse per il presente (di passato e futuro ne abbiamo fin troppo)

Ti ringrazio da ora per la cortese e fattiva collaborazione.