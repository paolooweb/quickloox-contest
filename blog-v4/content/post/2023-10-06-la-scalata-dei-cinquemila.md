---
title: "La scalata dei cinquemila"
date: 2023-10-06T01:34:28+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [SF Symbols, San Francisco]
---
Qualcuno racconterà che una volta i computer stimolavano la creatività e adesso invece la costringono e obbligano tutti a fare le stesse cose.

Certo i computer di una volta non avevano [SF Symbols](https://developer.apple.com/sf-symbols/), raccolta di simboli grafici in tono con il font San Francisco, quello ufficiale di Apple, diretta prima di tutto agli sviluppatori ma scaricabile gratis da chiunque.

Si scaricherà la quinta versione della raccolta, con settecento simboli nuovi di zecca e diverse altre novità come animazioni, per un totale di cinquemila simboli disponibili in nove pesi diversi e capaci di allinearsi sempre al testo.

Inoltre ci si ritrova una app ulteriore, nella quale esplorare la libreria e anche creare nuovi font composti, frutto della associazione di due o più simboli esistenti.

Certamente navigare per cinquemila caratteri non basta a esaurire uno spazio creativo. Allo stesso modo, un briciolo di ispirazione può aiutare a sbloccare la creatività.