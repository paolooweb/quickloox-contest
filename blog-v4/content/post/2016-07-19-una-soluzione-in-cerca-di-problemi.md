---
title: "Una soluzione in cerca di problemi"
date: 2016-07-19
comments: true
tags: [ResearchKit, Gsk, Glaxo-SmithKline]
---
Da decenni si ironizza, non completamente a torto, sul ruolo del computer come solutore di problemi che prima del suo arrivo non c’era bisogno di porsi.

In un certo senso, [ResearchKit](http://www.apple.com/it/researchkit/) compie definitivamente il paradosso: prima della sua esistenza, non era possibile condurre certi test medici con grandi numeri di pazienti e con una raccolta di dati capillare. O almeno, era molto più difficile e costoso e molti evitavano di porsi il problema, o lo affrontavano su scala più piccola (e meno utile).

Glaxo-SmithKline è la prima multinazionale farmaceutica a [condurre uno studio medico esteso](http://www.apple.com/it/researchkit/) (sull’artrite reumatoide) approfittando delle opportunità di ResearchKit. Tenuto conto che l’annuncio di ResearchKit è dell’anno scorso, la notizia merita attenzione. Dodici mesi, in questo settore, sono velocità.

Non è detto che uno studio porti benefici alla medicina, ma è certo che la medicina progredisca attraverso gli studi. Non è detto che ResearchKit non porti prima o poi alla messa a punto di cure migliori.

E poi uno si chiede quanto costa un iPhone. Bisogna considerare proprio tutto quello che c’è dentro. O quanti problemi risolve, di quelli che prima di lui nessuno si poneva.

*[Volessimo approfondimenti su ResearchKit, anche non previsti, come [dovrebbe essere un libro nel 2016](https://macintelligence.org/posts/2016-06-15-un-kickstarter-per-cambiare-il-libro/), basterebbe chiederli. E [dare un sostegno concreto](https://www.kickstarter.com/projects/295108589/cuore-di-mela-i-segreti-di-apple-iphone-e-mac) a chi li scrive.]*