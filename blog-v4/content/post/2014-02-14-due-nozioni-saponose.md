---
title: "Due nozioni saponose"
date: 2014-02-15
comments: true
tags: [iPad, design, Michelangelo, Ramanujan, Mari, cloud, scripting, creatività, innovazione, Alstom, zen, Apple, Windows, Picasso]
---
Il [viaggio a Savigliano](https://macintelligence.org/posts/2014-02-12-acquisti-che-durano/) è stato bello, un’esperienza da ricordare e persone sveglie e volonterose.<!--more-->

Dovevo parlare di innovazione e creatività. Ho sempre disprezzato l’infinita letteratura specializzata in tema perché le due nozioni mi paiono clamorose saponette, che scivolano via non appena sei convinto di stringerle in pugno, più stringi più scivolano e basta una goccia per intaccarle.

Così ho provato a lanciare provocazioni, riallacciate alla storia recente della tecnologia e a qualche personaggio dei passato. Riferimenti tassativamente facili e immediati perché si ha il dovere di eliminare il più possibile le barriere di ingresso anche se il pubblico è di superlaureati: Picasso, che a suo dire impiegò una vita intera per disimparare a disegnare e riuscire a farlo come un bambino; Michelangelo per il quale la statua era già dentro il marmo e basta levare la pietra in eccesso per liberarla; i monaci zen che perseguono la semplificazione e il miglioramento progressivo per iterazione. Mi sono concesso un riferimento a [Srinivasa Ramanujan](http://www-history.mcs.st-andrews.ac.uk/Biographies/Ramanujan.html), matematico indiano che univa alla conoscenza l’intuito per formule non collaudate ma delle quali intuiva l’esattezza per la loro eleganza. A un passo dal riflettere sulle dinamiche del *design*.

Parlando di creatività e *design*, ho lanciato una provocazione con Enzo Mari, grande *designer* che si è fatto da sé e, con l’età e le convinzioni radicate, certamente non le manda a dire. Nella mia provocazione stava solo un frammento televisivo nel quale, di fronte a una conduttrice esterrefatta, definisce la creatività come una parola che nel mondo di oggi suona oscena. Lascio il gusto della ricerca al lettore e qui invece punto a un suo [lunghissimo intervento recente](http://www.youtube.com/watch?v=PuDR6tzkJrE) a Pordenone.

<iframe width="420" height="315" src="//www.youtube.com/embed/PuDR6tzkJrE" frameborder="0" allowfullscreen></iframe>

E per parlare di *design* e innovazione, sono partito dal [video sulla produzione di Mac Pro](http://www.apple.com/mac-pro/video/).

Ho dedicato tempo alla nascita di iPhone e iPad come avvenimenti traumatici nei rispettivi settori di prodotto, così come alle dinamiche e alle tecnologie – stampa 3D, *cloud computing*, *scripting*, collaborazione di rete – che stanno portando creatività e innovazione, qualunque cosa siano, fuori dai laboratori di ricerca e sviluppo e verso ciascuno di noi, individualmente o come gruppo di lavoro.

Sono dinamiche che hanno portato gli apparecchi Apple a [pareggiare nelle vendite](http://ben-evans.com/benedictevans/2014/2/12/apple-passes-microsoft) di questo Natale gli apparecchi Windows. Non è un errore: *pareggiare*. Quelli dei pochi programmi, degli standard di fatto, del *così fan tutti*, possono andare a nascondersi.

Da questi spunti e altri abbiamo derivato alcuni indizi che potrebbero portare a riconoscere innovazione e creatività. Per esempio, sembra più innovazione quando il cambiamento è rapido e drastico, meno quando si compie in molto tempo e quasi passa inosservato. Sussiste certamente creatività quando un’idea di successo trova molti critici della prima ora, salvo venire adottata da tutti immediatamente dopo. Eccetera.

Ho cercato di tradurre creatività e innovazione in quello che teniamo in tasca, abbiamo intorno, si legge nelle cronache. Le due nozioni sono scivolate da tutte le parti, come prevedevo. Confido che qualcosa sia rimasto comunque in tasca a tutti.