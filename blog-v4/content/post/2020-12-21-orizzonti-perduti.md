---
title: "Orizzonti perduti"
date: 2020-12-21
comments: true
tags: [Knuth, Metafont, TeX, Dvi, Pdf, PostScript, Clarke]
---
Il mondo digitale potrebbe offrire tipografia infinitamente migliore di quella dei migliori libri cartacei e questo da quasi quarant’anni, quando un uomo insoddisfatto delle prove di stampa del suo libro mise da parte il libro e scrisse un sistema di composizione tipografica, uno di manipolazione di font, uno di rappresentazione grafica indipendente dai pixel fisici… anni e anni prima di Pdf, di PostScript, di tutto.

[Oggi potremmo avere](https://bitbashing.io/tex.html) un Web, per tacere di tutto il resto, scintillante di tipografia di eccellenza. Invece i browser sono un regno di bruttura tipografica inaudita.

Oso dire che, se quello che si legge su certi social è indecente, lo si deve anche all’aspetto sciatto e indecoroso che hanno quelle parole.

Nessuno di noi ha la statura o le risorse mentali di un Donald Knuth ed è molto più difficile cambiare con la sola forza delle buone idee un mondo dove troppe condizioni sono oramai consolidate, per esempio l’uso di programmi tipograficamente orrendi per la videoscrittura personale.

Qualcosa dovremmo però farlo, tutti a partire da ognuno. Non sembri superficiale, concentrarsi sulla tipografia; Arthur C. Clarke scrisse che certo la generazione di ricchezza non è da buttare via, ma altrimenti la civiltà dovrebbe concentrarsi sulla creazione di bellezza e sulla ricerca della conoscenza. Avere tipografia degna del nostro essere umani fa parte del primo obiettivo.

Una proposta da mulini a vento; abbiamo a disposizione più o meno tutti almeno qualche giorno o qualche momento di vacanza, da qui a poco. Facciamo tutti un piccolo passo su LaTeX. Un piccolo passo *in più*, quelli che possono insegnare più che imparare; un piccolo passo, chi finora è rimasto fermo.

Da dopo l’Epifania, proviamo ad applicare almeno una volta, almeno in un contesto qualsiasi purché pubblico, quello che abbiamo imparato nel compiere il nostro piccolo passo. Lasciamo che più persone possano apprezzare come la composizione del testo possa diventare simile alla lavorazione del ferro battuto o alla soffiatura del vetro: ricerca della perfezione artigianale.

La lezione di Knuth e di TeX va riscoperta e ridiffusa. Oggi più che mai, quando sull’intero pianeta chiunque abbia il privilegio di un’alimentazione decente ha accesso a qualche tipo di risorsa computazionale. Ci sono tutti gli strumenti per portare bellezza a due miliardi di persone come minimo.

Impegniamoci per raggiungerne, ciascuno, due persone.
