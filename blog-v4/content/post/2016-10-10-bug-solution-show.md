---
title: "Bug Solution Show"
date: 2016-10-10
comments: true
tags: [watch, Siri, bug]
---
Alcuni mi hanno consigliato di passare a watchOS 3 per risolvere il *bug* di cui [avevo parlato giorni fa](https://macintelligence.org/posts/2016-10-06-bug-show/).<!--more-->

Beh, il *bug* parrebbe essersi risolto. Il punto è che lo ha fatto da solo; non ho ancora installato watchOS 3 – il sistema ancora non me lo ha proposto e aspetto sempre che sia lui a farlo – e, anche se non ho condotto verifiche accurate, apparentemente posso di nuovo mandare messaggi via Siri e watch senza problemi.

Sono ovviamente contento (la funzione è comodissima, una delle ragioni primarie di avere watch) e un po’ perplesso, perché appunto il *bug* se ne è andato da solo e invece vorrei avere avuto l’opportunità almeno di capire che cosa possa essere accaduto.

Non so se un ingegnere del software abbia messo a posto un difetto noto, o se un oscuro file di preferenze all’interno dei miei sistemi sia stato improvvisamente riscritto in maniera corretta, se i sistemi di *machine learning* su cui poggia Siri si siano accorti di un problema e lo abbiano corretto da soli.

Mi rendo conto che il progresso ci porta sempre più verso situazioni di questo tipo. Software e hardware sono sempre più indipendenti da noi e il vecchio approccio delle mani sotto al cofano diventa sempre meno praticabile. Nel caso di Siri poi la cosa è eclatante: non risiede neanche nei nostri computer.

Sono entusiasta del progresso ma vedo che a volte c’è da pagare un prezzo. Vogliamo un assistente vocale a prezzo di sperare che si risolva da sé i suoi problemi? Personalmente tendo a rispondere di sì, eppure sento che debba essere terreno di riflessione.

(È intanto arrivata la notifica e sto aggiornando a watchOS 3).