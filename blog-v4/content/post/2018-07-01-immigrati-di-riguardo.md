---
title: "Immigrati di riguardo"
date: 2018-07-01
comments: true
tags: [Jobs, Kahn, Borland, Jandali, Fermi, Luciano]
---
Si saranno casualmente notate polemiche recenti relative all’immigrazione in Italia. Polemiche da cui mi terrò a distanza.

Tuttavia, gira da tempo un messaggio che fa notare come iPhone sia dovuto alla possibilità per un *immigrato* siriano di trovare *asilo* negli Stati Uniti e lì concepire Steve Jobs. Qui mi sento di intervenire proprio per il rispetto dovuto a persone che lasciano il proprio Paese alla ricerca di una vita migliore, insultate dall’autore di questo messaggio con la propria ignoranza.

Il [padre di Steve Jobs](https://www.macworld.co.uk/feature/apple/who-is-steve-jobs-syrian-immigrant-father-abdul-fattah-jandali-3624958/), Abdul Fattah Jandali, era un figlio di papà.

Suo padre, nonno biologico di Jobs, era un milionario che mandò a studiare il figlio in Libano.

Solo che il Paese dei cedri entrò in una fase di forte instabilità politica e Jandali, finito nei guai per il suo attivismo nazionalista, fu nuovamente inviato dal padre ricco a studiare, questa volta negli Stati Uniti.

Abitava a New York e condivideva l’appartamento con un parente: l’ambasciatore della Siria.

Jandali non era un immigrato, bensì uno studente. Non ha avuto asilo ma un permesso di studio. Oggi nessuno chiama immigrati, rifugiati o profughi gli Erasmus o i ragazzi cinesi che affollano Stanford. Poi, già detto, i denari e le conoscenze della sua famiglia non erano quelle di un cittadino siriano scelto a caso.

Se fosse stato in fuga dal suo Paese per fame o guerra o persecuzione, non vi sarebbe tornato a tentare la carriera diplomatica (non la pastorizia, la carriera diplomatica), per poi doversi accontentare di dirigere una raffineria. Se fosse stato un rifugiato in miseria e senza documenti, non sarebbe ritornato agevolmente negli Stati Uniti a insegnare al college e comprarsi un ristorante come ha poi fatto.

Steve Jobs è nato in America nonché cresciuto in America in una famiglia di americani medi e associarlo all’immigrazione è come minimo disonesto.

Tra un rifugiato siriano 2018 e il padre di Steve Jobs c’è un abisso e l’unica similitudine possibile è che sono conterranei. [Lucky Luciano](https://it.wikipedia.org/wiki/Lucky_Luciano) e [Enrico Fermi](https://en.wikipedia.org/wiki/Enrico_Fermi) sono immigrati negli Stati Uniti, però sono persone diverse dalla maggior parte degli emigrati italiani e non rappresentano la categoria.

Aggiungo in coda che, volendo raccontare una storia interessante di immigrazione e tecnologia, ne esistono di notevoli. Per esempio quella di [Philippe Kahn](https://en.wikipedia.org/wiki/Philippe_Kahn), francese che (tra l’altro) fondò Borland, negli anni novanta multinazionale del software capace di contrastare Microsoft.

Kahn, uomo eclettico e per esempio sassofonista capace di [pubblicare un album credibile](https://www.discogs.com/Philippe-Kahn-Walkin-On-The-Moon/release/4563434), [era un immigrato clandestino](http://www.fundinguniverse.com/company-histories/borland-international-inc-history/).

Le autorità se ne accorsero solo quando era diventato datore di lavoro di numerosi americani.

Certo, parlare di Philippe Kahn e Borland non permette di biascicare moralismo su Facebook e farsi condividere dalle teste vuote di tutto tranne che la propaganda.
