---
title: "Stile balneare"
date: 2017-06-18
comments: true
tags: [watch]
---
Chiamami snob se vuoi, ma oggi in spiaggia ho risposto ad alcuni messaggi di lavoro via watch, con qualche tocco del polso, giusta velocità, esatta efficacia.

Chiunque altro ha fatto giustamente quello che gli pareva e sono l’ultimo che può dire male di chi usa un apparecchio per comunicare.

Però cavarsela con un piccolo quadrante di ingombro nullo (iPhone è rimasto nella tasca dei pantaloni) è un altro livello di qualità della vita.