---
title: "Dentro il programma niente"
date: 2023-06-13T01:46:37+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [Searle, John Searle, Hofstadter, Douglas Hofstadter, intelligenza artificiale, ai, stanza cinese]
---
Ho letto tutto l’articolo di John Searle in cui viene descritto l’esperimento della [stanza cinese](https://macintelligence.org/posts/2023-06-09-la-complessità-cinese/).

Naturalmente non sono qui a scrivere il bigino o a tenere una lezione di filosofia applicata; l’argomento trova esegesi già pronte, autorevoli, di tutti i livelli. Invece annoto un paio di cose a livello quasi personale.

Per me intelligenza artificiale forte ha sempre significato allinearsi con Alan Turing e dire *non vedo ostacoli al fatto che una macchina possa pensare*, avendo ovviamente come premessa il fatto che oggi questa macchina non c’è. Secondo Searle, molti sostenitori dell’idea partono invece da un’altra premessa, quella che l’intenzionalità (il sapere che cosa si sta facendo e con che scopo) possa risiedere unicamente nel programma, del tutto separata dal computer.

Per Searle, questo è impossibile in quanto la frase *il cervello umano è come un computer* è fuorviante. Il cervello umano *produce* i programmi, mentre nel computer questo non accade. La mente non sta al cervello come un programma sta allo hardware.

Searle considera *programma* quello che sta in mezzo tra l’ingresso e l’uscita e lo considera tale in maniera molto libera. Di fatto, tutto è riconducibile al calcolatore digitale perché anche il funzionamento di un rubinetto si compone di un input e di un output con in mezzo regole.

Searle si chiede se una macchina possa pensare è la sua risposta è sì, a patto che possieda funzioni causali equivalenti a quelle del cervello umano (causali, non casuali). Non lo dice esplicitamente, però in pratica sostiene che senza disporre di uno hardware equivalente al cervello, realizzare una macchina pensante non sia possibile.

Bisogna ricordare che tutto il dibattito si snoda attorno al dubbio che una macchina pensante o meno possa passare il test di Turing o meno. Il test di Turing va preso come un concetto, non come un algoritmo in grado di dare un ipotetico voto oggettivo a un’entità che si cimenta.

Sempre Searle argomenta che il trattamento di simboli da parte di un programma è sempre privo di significato e che il significato non nasce dalla corretta manipolazione dei simboli da parte del programma. A quanto pare, vari sostenitori dell’intelligenza artificiale forte avrebbero provato a impostare programmi che incorporano conoscenza rispetto a oggetti o nozioni umane, con regole che portano a stabilire similitudini tra oggetti apparentemente separati (esempio: il programma scopre che la banana condivide con la gruccia la proprietà di *appeso*, l’una all’albero e l’altra all’attaccapanni). Dopo di che, hanno sostenuto che in base a questo tipo di output il programma riusciva a capire le differenze e le similitudini tra banane e grucce (oppure tra pianeti nel sistema solare e particelle subatomiche nell’atomo).

In generale, chi sostiene l’intelligenza artificiale forte punta sugli sviluppi futuri delle tecnologie, mentre chi sostiene quella debole considera l’esistente. Il che non è un giudizio di valore, ma modi diversi di impostare il ragionamento.

In definitiva, se pensiamo a macchine munite di comprensione e  significato, per Searle si tratta del cervello umano oppure, nell’ipotesi più benevola, di macchine che lo riproducono. Non su base programmatica, attenzione: per lui la descrizione perfetta dello stato di tutti i neuroni in un momento dato (ovvero del programma in esecuzione) non porta da nessuna parte. Invece, nella sua argomentazione il cervello dispone di funzionalità causali, come detto, che costituiscono uno strato comunque estraneo a quello del programma.

E per oggi avrei straparlato a sufficienza.