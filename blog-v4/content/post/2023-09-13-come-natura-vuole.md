---
title: "Come natura vuole"
date: 2023-09-13T00:30:55+01:00
draft: false
toc: false
comments: true
categories: [Hardware]
tags: [Apple, Apple Watch, iPhone 15, A17 Pro]
---
Ha già una rilevanza che un nuovo modello si chiami iPhone 15, con l’accento sul quindici. La sfida annuale di Apple è mantenere rilevante un apparecchio che stando agli analisti compete in un mercato dichiarato saturo più volte da anni. Più veloce ha una sua importanza, la fotocamera ne ha moltissima però non basta e bisogna avere altre carte da giocare.

Quest’anno si è andati sul green, con un [filmato surreale](https://www.youtube.com/watch?v=xqyUdNxWazA) nel quale Madre Natura in persona torchiava Tim Cook e compagni durante un meeting, rimarcando negativamente l’uso di una giacca di cuoio e mostrandosi scettica intanto che lo staff snocciolava cifre, programmi e promesse.

Ironizzare su un tema dominio del politicamente corretto per trasmettere messaggi di marketing è terreno scivolosissimo e tuttavia la manovra è riuscita, con esiti anche ilari. Semmai, dall'ambito ambientalista sono usciti gli annunci di contorno più interessanti: cento percento di cobalto riciclato dentro le batterie entro il 2025 è tanta roba, perché Apple vende tante batterie. Anche i giornalisti d’assalto a caccia di scandali di multinazionali cattive potrebbero trovarsi a dover modificare l’agenda al capitolo *sfruttamento iniquo di risorse minerarie*. Non è che Apple sia buona, bada al profitto; intanto però, sia per vocazione o per pressione mediale, a certe cose è attenta e ci spende soldi sopra.

A livello generale non c’è molto da dire, perché da tempo si tratta di aggiornamenti incrementali e non entro nel merito della fotografia, per la quale appaiono puntualmente recensioni strepitose di professionisti di prima fascia.

I dettagli che hanno lasciato il segno sono il video introduttivo a tema watch salvavita, con sopravvissuti che se la sono cavata grazie al rilevamento del battito o con una richiesta di soccorso via satellite. Si sono giocati un’idea per il video di Natale, l’atmosfera è un po’ mélo, però esiste *davvero* gente che è ancora qui per avere avuto addosso un watch e insomma, in cambio del profitto arriva anche qualcosa.

Sempre su watch, la nuova action pollice-indice è geniale, quasi Zen.

A17 Pro fa ray tracing, calcola gli effetti delle riflessioni dei raggi di luce all’interno di una immagine 3d, con conseguente maggiore realismo. Il ray tracing è computazionalmente costoso e averlo in hardware, su un coso da tasca, dice bene per il futuro di iPad e soprattutto Mac quando arriverà M3.

La scocca di titanio di iPhone 15 non è una prima assoluta di Apple, per chi ricorda il PowerBook G4 Titanium. Promette leggerezza, robustezza, polemiche di qualche tipo che attorno al titanio mai mancano e conferma una nota positiva: società da tre trilioni di capitalizzazione (prossimi venturi) che comunque cambia, sperimenta, anche azzarda e lo fa sul prodotto principale.

Una parola sul format, uguale a sé stesso ma ambientato in gran parte in esterni, da colline californiane a punti iconici di città, con la sola eccezione di un capannone industriale. Quasi tutti gli speaker hanno parlato all’aria aperta, probabilmente per tenersi buona Madre Natura in vista della scadenza carbon-neutral del 2030.

<iframe width="560" height="315" src="https://www.youtube.com/embed/xqyUdNxWazA?si=Qpb_tvz6IN_ufEP5" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>