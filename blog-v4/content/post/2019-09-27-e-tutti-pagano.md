---
title: "E tutti pagano"
date: 2019-09-27
comments: true
tags: [Apple, tasse, Fortune]
---
Uno può sempre obiettare che [le affermazioni di Apple sulla sua situazione di azienda contribuente](https://www.apple.com/uk/newsroom/2017/11/the-facts-about-apple-tax-payments/) possano essere poco neutrali e magari viziate dalla propaganda.

Tuttavia [la voce di Fortune](https://fortune.com/2017/10/31/trump-tax-reform-apple-multinational-companies/) può essere definita meno influenzabile dagli interessi aziendali e il suo racconto è assai poco diverso nella sostanza. I fatti sono chiari:

1. Apple paga meno tasse di quello che ci aspetterebbe in teoria, ma è una delle multinazionali che ha il trattamento fiscale *peggiore*.
2. Per quanto poche, le,tasse pagate sono molto più vicine al trenta percento dell’imponibile che alle cifre tendenti a zero fatte circolare di tanto in tanto.
3. Il problema vero è l’armonizzazione internazionale dei millemila sistemi di tassazione interno ai vari Paesi.

Si ha la sensazione che, più degli importi dovuti, incidano gli sforzi e l’organizzazione di cui ci si deve dotare quando hai una sede in decine di nazioni, ognuna con le sue particolarità e stranezze fiscali, e devi organizzare tutto in modo che funzioni e sia anche rispettoso delle regole stesse. Sperando che non salti fuori una Unione Europea a [chiederti quattordici miliardi](https://www.reuters.com/article/us-eu-apple-stateaid/apple-says-14-billion-eu-tax-order-defies-reality-and-common-sense-idUSKBN1W1195) perché ha deciso ieri che le regole applicate da vent’anni sono sempre state sbagliate.

Altra sensazione è che i governi nazionali preferiscano mantenere lo status quo invece di impegnarsi davvero per tassare in maniera equa e organizzata tutti. Se è così è per convenienza e significa grosso modo che stiamo pagando tasse inique.
