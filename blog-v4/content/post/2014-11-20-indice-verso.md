---
title: "Indice verso"
date: 2014-11-20
comments: true
tags: [Spotlight, sudo]
---
Capita che Spotlight si incanti e in quei momenti siamo tutti ormai dotati della conoscenza di base necessaria. Da Terminale,

`mdutil -i off`

per spegnere Spotlight,

`mdutil -i on`

per accenderlo,

`mdutil -E /`

per riavviare da zero l’indicizzazione,

`rm -R /.Spotlight-V100`

per vuotare e cancellare la cartella nascosta che contiene i dati dell’indice.

In rari casi però non basta. Bisogna aggiungere [un paio di comandi](http://www.sciencemedianetwork.org/Blog/20130321_Spotlight_fix) all’arsenale di emergenza:

`launchctl unload -w /System/Library/LaunchDaemons/com.apple.metadata.mds.plist`

per spegnere il motore del server Spotlight e

`launchctl load -w /System/Library/LaunchDaemons/com.apple.metadata.mds.plist`

per riaccenderlo.

A tutti i comandi citati sopra va anteposto `sudo`, che richiede una password di amministrazione.

E si dovrebbe riuscire a recuperare l’uso dell’ìndice.