---
title: "Distinguersi dalla app"
date: 2018-07-07
comments: true
tags: [app, Twitter, Malik]
---
Chi è abbastanza fortunato da avere visto l’inizio del *computing* personale tende a dimenticarlo, tuttavia faceva parte di una élite planetaria di pochi milioni di persone, su miliardi di popolazione.

Oggi le cose sono leggermente cambiate e ad avere accesso all’informatica, almeno quella di base, è quasi metà del pianeta.

Per chi scrive software la differenza non la fa più l’avere una applicazione, semplicemente esistere; bisogna raggiungere i propri utilizzatori nei modi migliori *per loro*. Certo, è faticoso. Fa la differenza, però.

Ne scrivo perché Om Malik ha inserito [in un suo post](https://om.co/2018/07/06/app-store-at-10/) anodino sui dieci anni di App Store una frase inaspettata è imprevedibile, che ha insaporito improvvisamente tutto.

>Il decimo anniversario di App Store è un promemoria puntuale del fatto che gli entusiasti – derisi in quanto *fanboy* – aiutano a trasformare le aziende in movimenti culturali. Twitter, nel suo atteggiamento da plantigrado verso il proprio ecosistema, non dovrebbe dimenticare che una larga parte del suo successo viene esattamente da quelli entusiasti.

Le app hanno vinto e chi si ostina a proporre unicamente un‘esperienza web, come Twitter, ha perso. Non importa che sia ancora in piedi e magari cresca pure; snobba il suo tempo e prima o poi arriverà il conto da pagare. Se vuoi distinguerti devi avere una app e magari anche una app bella.

