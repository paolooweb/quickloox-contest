---
title: "Qualcosa d’altro"
date: 2022-10-03T14:49:32+01:00
draft: false
toc: false
comments: true
categories: [Web]
tags: [Fat Bear Week, Carnival of Mathematics, Angband Live]
---
Ho un paio di amici in situazioni spiacevoli. Incrocio le dita e mi dedico a qualcosa di completamente differente, che distragga e allontani temporaneamente in attesa di buone notizie.

Se è un bisogno condiviso, sta per iniziare la [Fat Bear Week](https://explore.org/fat-bear-week); oppure, è disponibile l’edizione di ottobre del [Carnival of Mathematics](https://aperiodical.com/carnival-of-mathematics/), oltre a tutte le edizioni precedenti.

Altrimenti c’è sempre [Angband Live](https://angband.live), basta un browser e si è dentro. Un mago minimamente accorto arriva relativamente in fretta a incendiare legioni di orchi e troll, il tutto completamente immaginario e a stress zero; a volte è un bell’antidoto agli orchi veri.