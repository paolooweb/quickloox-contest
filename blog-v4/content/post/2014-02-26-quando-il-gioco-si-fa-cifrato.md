---
title: "Quando il gioco si fa cifrato"
date: 2014-02-26
comments: true
tags: [MailShield]
---
Ci sono miei amici che si sono un po’ stufati delle questioni riguardanti le intercettazioni dell’Nsa – e tutti quanti i servizi segreti di qualsiasi Paese, si parla degli americani sono perché fanno moda – così come della disinvoltura di Google sulla tutela della *privacy* personale e qui e là. Anche perché lavorano con privati e aziende a un livello che impone la sicurezza di comunicazioni riservate.<!--more-->

Al che si sono rimboccati le maniche e hanno creato un’offerta di mail cifrata e inespugnabile, residente su server… svizzeri: quelli dove l’accesso arbitrario alla posta da parte di estranei, dai criminali informatici ai magistrati che abusano del loro potere, è il meno probabile di tutti.

Hanno appena avviato la [campagna di finanziamento di Mail Shield su Indiegogo](http://www.indiegogo.com/projects/mail-shield/x/6516990) e puntano a farsi finanziare il progetto nei prossimi due mesi.

Mi piace che siano miei amici, perché li conosco e so come lavorano e so che mi posso fidare. Mi piace che non sia il mio lavoro, perché non ci guadagno niente a parlarne e anzi, ho partecipato alla campagna di finanziamento comprandomi – se tutto andrà a buon fine – una casella corazzata e inespugnabile per la posta elettronica. Mi serve per un messaggio su cento, ma quando serve, quel prezzo lì è risibile.

Insomma, ne parlo bene perché so da dove arriva e ci posso mettere la mano sul fuoco. Tutto quello che ho da guadagnarci è una casella di posta veramente privata. L’iniziativa merita.