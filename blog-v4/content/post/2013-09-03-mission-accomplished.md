---
title: "Mission Accomplished"
date: 2013-09-03
comments: true
tags: [Nokia, Microsoft]
---
A settembre 2010 Stephen Elop abbandona un posto da dirigente in Microsoft per [diventare amministratore delegato di Nokia](http://conversations.nokia.com/2010/09/10/stephen-elop-to-join-nokia-as-president-and-ceo/).<!--more-->

Nokia nel 2010 fa profitti per [due miliardi di euro](http://i.nokia.com/blob/view/-/165234/data/5/-/form20-f-10-pdf.pdf). Certo non gli [otto miliardi del 2007](http://i.nokia.com/blob/view/-/165244/data/5/-/), prima che arrivasse iPhone, ma buttali via.

Il tempo di passare Capodanno e Nokia [sceglie Windows Phone di Microsoft come sistema operativo primario](http://www.digitaltrends.com/mobile/its-official-nokia-chooses-windows-phone-7-as-its-primary-operating-system/), di fatto buttando a mare tutto il resto.

Saltiamo a questo agosto, quando l’amministratore delegato di Microsoft Steve Ballmer [annuncia la sua prossima dipartita dall’azienda](http://www.microsoft.com/en-us/news/press/2013/aug13/08-23AnnouncementPR.aspx).

Nel frattempo Nokia ha avuto il tempo di [perdere un miliardo di euro nel 2011 e due miliardi di euro nel 2012](http://i.nokia.com/blob/view/-/2246090/data/2/-/form20-f-12-pdf.pdf). Il valore dell’azienda, che nel frattempo ha fatto esperienza nella produzione di apparecchi da dotare di software Microsoft, si è comprensibilmente molto ridotto.

La produzione degli apparecchi da dotare di software Windows e licenze varie vengono [acquistate da Microsoft](http://www.microsoft.com/en-us/news/press/2013/Sep13/09-02AnnouncementPR.aspx) per cinque miliardi e mezzo di euro. Conti della serva: un terzo di quello che avrebbe speso nel 2010.

Elop torna in Microsoft e qualcuno [si chiede](http://www.theverge.com/2013/9/3/4688846/will-stephen-elop-be-the-next-microsoft-ceo-steve-ballmer-wont-say) se ora sia un candidato credibile alla poltrona di amministratore delegato nel momento in cui Ballmer si alzerà.

Mi piacerebbe molto sapere che cosa si siano detti Ballmer ed Elop l’ultima volta che si sono trovati a tavola insieme e da soli in Microsoft.

Azzardo ciò che si diranno la prossima volta. *Missione compiuta*.