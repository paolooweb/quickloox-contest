---
title: "Contrordine compagni"
date: 2020-04-21
comments: true
tags: [Prosser, iPadOS, XCode]
---
Sono il primo a dire che dei *rumor* non bisogna curarsi. Devo fare eccezione per questo [tweet di Jon Prosser](https://twitter.com/jon_prosser/status/1252187152831692800):

<blockquote class="twitter-tweet"><p lang="en" dir="ltr">I’m not gonna say that Final Cut is coming to iPad... <br><br>But XCode is present on iOS / iPad OS 14. 👀<br><br>The implications there are HUGE.<br><br>Opens the door for “Pro” applications to come to iPad.<br><br>I mentioned this last week on a live stream, but figured it was worth the tweet 🤷🏼‍♂️</p>&mdash; Jon Prosser (@jon_prosser) <a href="https://twitter.com/jon_prosser/status/1252187152831692800?ref_src=twsrc%5Etfw">April 20, 2020</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

Metti che veramente iPadOS 14 contenga XCode. Internet collassa sotto il peso di milioni di utenti che vanno a invertire la polarità su milioni di pagine web che avranno da spiegare come iPad si sia trasformato improvvisamente in un computer.
