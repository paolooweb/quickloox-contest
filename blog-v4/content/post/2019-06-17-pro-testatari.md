---
title: "Pro testatari"
date: 2019-06-17
comments: true
tags: [Logic, Mac, Pro, Panzarino, Madonna, Rihanna, Coldplay, Muse, Seal, Keys, Lovato]
---
È noto da tempo come Mac Pro sia stato progettato [con il supporto di un Pro Workflow Team](https://techcrunch.com/2018/04/05/apples-2019-imac-pro-will-be-shaped-by-workflows/) composto da professionisti di alto livello (*award-winning artists and technicians*) disposti a condividere il proprio modo di lavorare e persino progetti concreti.

Nel caso della musica, sono uscite da poco [dichiarazioni](https://www.applemust.com/music-pros-seem-to-love-apples-smashing-logic-pro-x-10-4-5-update/) di produttori che hanno lavorato per esempio con Madonna, Rihanna, Coldplay, Muse, Seal, Alicia Keys, Demi Lovato, o hanno composto musica per un film su Spiderman. Cose così:

>Talvolta succede che il subconscio di un produttore lo tenga lontano dall’esplorazione di certe idee creative, nella convinzione che hardware e software potrebbero non essere all’altezza. Logic Pro X 10.4.5 e il nuovo Mac Pro eliminano queste limitazioni.

Certo sono frasi che passano dal filtro della macchina propagandistica di Apple; altrettanto certamente si tratta di gente che non ha bisogno di svendersi e opera in un mondo dove è un attimo compromettersi la reputazione per essersi svenduti. Anche la valutazione più scettica deve concludere che, come minimo, vi sia un fondo di verità.

Poi ci sono quelli che si dicono professionisti e protestano perché per loro non si è fatta una macchina sufficientemente modulare; quando arriva la macchina modulare, non è potente abbastanza; se è potente a sufficienza, vuol dire che costa troppo; ove costasse ragionevolmente, sarebbe diretta al pubblico sbagliato; nel caso tutto fallisca, l’ultima spiaggia è puntare il dito su un sistema Windows che costerà meno a parità di lista della spesa, come è regolarmente accaduto da quando esiste Mac, come se fosse uno scandalo di oggi.

Per loro l’uva è sistematicamente acerba e il punto vero non è la macchina, che hanno comprato comunque o non compreranno, a prescindere; è che nessuno mai li chiamerà a testare il progetto di un nuovo computer. Ma mica Apple; neanche una Asus.
