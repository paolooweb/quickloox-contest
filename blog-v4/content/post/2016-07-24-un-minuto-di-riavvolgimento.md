---
title: "Un minuto di riavvolgimento"
date: 2016-07-24
comments: true
tags: [Vcr, Funai, Quartz, videoregistratori, Sony, Betamax]
---
Le notizie che segnano veramente il cammino del’umanità rimangono spesso escluse dai telegiornali. Il 30 luglio la giapponese Funai [produrrà l’ultima partita di videoregistratori](http://qz.com/738365/the-last-vcr-will-be-manufactured-this-month/) della sua storia.<!--more-->

E della storia in generale, poiché è l’ultima azienda al mondo a occuparsene.

L’articolo di *Quartz* puntualizza che Sony ha smesso di produrre lettori Betamax nel 2002 ma solo quest’anno ha cessato la produzione di nastri. A dire che passerà ancora del tempo prima che il videoregistratore diventi definitivamente un ricordo.

Succederà presto tuttavia e ci sarà da raccontare ai nipoti la storia di antiche macchine che, dopo avere mostrato un film, dovevano riavvolgere un nastro prima di poterlo fare di nuovo. A loro sembrerà leggenda.

E qualche nonno ribadirà che il Vhs mostrava i film meglio di un Blu-ray, proprio come con vinile e Cd-Rom.

*[I predecessori di [Cuore di Mela](http://cuoredimela.accomazzi.it) hanno sempre illustrato le novità del sistema operativo recente con le nozioni di base ottime per comprendere anche il funzionamento di sistemi più vecchi. Cuore di Mela farà lo stesso, basandosi anche sui feedback dei lettori. Perché è un libro che si aggiorna! Prima di tutto deve nascere e per questo si sollecitano tanto passaparola e interventi concreti sulla [pagina Kickstarter](https://www.kickstarter.com/projects/295108589/cuore-di-mela-i-segreti-di-apple-iphone-e-mac) del progetto.]*