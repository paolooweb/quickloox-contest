---
title: "Di male in peggio"
date: 2021-02-24T03:34:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [Windows, Simone Aliprandi, Luca Bonissi] 
---
Il principio della richiedibilità di un rimborso per l’acquisto forzato di una licenza Windows è assodato da anni.

Mai peraltro come nel caso di Luca Bonissi, qui [riassunto da Simone Aliprandi](https://aliprandi.blogspot.com/2021/02/risarcimento-20mila-euro-rimborso-licenza-windows.html), il giudice ha determinato altrettanto esattamente dove si trovi il merito.

Bonissi ha comprato un computer Lenovo nel 2018 e ha chiesto formalmente il rimborso per la licenza di Windows inclusa e non evitabile nel prezzo di vendita: Lenovo ha fatto di tutto per non pagare, ma lui è arrivato fino al giudice di pace, presso il quale è stato imposto all’azienda il pagamento di 42 euro come rimborso della licenza più 130 euro di spese processuali.

Lenovo si è impuntata e, oltre a rifiutarsi di pagare, ha impugnato la sentenza presentando cinquantanove pagine di obiezioni (motivi per obbligare la gente a pagare per Windows anche se non voleva farlo).

Il Tribunale di Monza ha dato ragione a Bonissi e ha anche imposto a Lenovo il pagamento di ventimila euro aggiuntivi come punizione:

>[per avere] abusato dello strumento impugnatorio costringendo la controparte […] a replicare […] ad una produzione difensiva assolutamente sproporzionata […] esemplificativa della prepotenza e prevaricazione di un colosso commerciale nei confronti di un modesto consumatore.

Ora è chiarissimo e pure quantificato il problema di Windows per gli individui e la società: un bene di pochissimo valore intrinseco, la cui imposizione genera danni enormi, cinquecento volte lo scarso valore di base.

Spendi male e riesci a farti molto peggio senza accorgertene, insomma, salvo eccezioni illuminate.

*Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile.*