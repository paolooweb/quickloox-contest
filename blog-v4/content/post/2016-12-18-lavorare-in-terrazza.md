---
title: "Lavorare in terrazza"
date: 2016-12-18
comments: true
tags: [Mac, MacBook, Terrazza, Aperol]
---
Il banco di regia alla [Terrazza Aperol](http://www.terrazzaaperol.it/it/home/) di Milano.

 ![Un Mac in Terrazza Aperol](/images/aperol.jpg  "Un Mac in Terrazza Aperol") 

Non è esattamente il locale dove si mette piede un giorno sì e uno no. Questa la vista dalla terrazza propriamente detta, su piazza Duomo.

 ![Vista di piazza Duomo a Milano dalla Terrazza Aperol](/images/piazza-duomo.jpg  "Vista di piazza Duomo a Milano dalla Terrazza Aperol") 

Sospetto che al banco di regia operino professionisti.