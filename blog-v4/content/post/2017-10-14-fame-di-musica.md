---
title: "Fame di musica"
date: 2017-10-14
comments: true
tags: [Concertato, BeCrowdy]
---
Ho già scritto in altre occasioni della necessità nuova di definire un budget, anche minimo, al sostegno delle iniziative di contenuto che meritano per qualità e senso, perché sia sempre possibile trovare spunti interessanti e diversi dall’inevitabile cacca gratis.

Ecco perché segnalo una [raccolta fondi su BeCrowdy](https://www.becrowdy.com/i-venti-dell-est-concerti-classica) mirata al finanziamento dell’attività dell’associazione [Concertato](http://concertato.com/en/).

Il punto non è più se si fruisca personalmente di quello che offre l’associazione, ma della condivisione della sua proposta e, se nel caso, appunto del sostegno, anche minimo.

Nello specifico, sono pienamente a favore di tutto quanto [concerne in modo intelligente ed educato](https://macintelligence.org/posts/2016-10-21-ricercar/) la musica classica e già mi è capitato di mettere sul piatto il [libero finanziamento a una versione libera dell’Arte della Fuga](https://macintelligence.org/posts/2017-08-13-lavori-di-gruppo/). Che tra l’altro ha avuto successo.

Se poi lo spartito [sta su Mac](https://musescore.com) o [su iPad](https://forscore.co), meglio ancora.