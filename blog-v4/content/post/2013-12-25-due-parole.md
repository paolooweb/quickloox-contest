---
title: "Due parole"
date: 2013-12-25
comments: true
tags: [iPhone, Apple]
---
Poco importa che sia già passato in TV. Probabilmente è lo  [spot](https://www.youtube.com/watch?v=nhwhnEe7CjE) più bello dell’anno.<!--more-->

Poi suggerisco non tanto una lettura, ma una visita, al [Polonsky Foundation Digitization Project](http://bav.bodleian.ox.ac.uk), che ha iniziato a pubblicare quelli che diventeranno migliaia di manoscritti antichi e libri medievali. È un pezzo di storia da cui veniamo e che per la prima volta è così tanto universalmente disponibile.

Lo stesso vale per la pagina [Digitised Manuscripts](http://www.bl.uk/manuscripts/Default.aspx) della British Library, la stessa che ha appena [pubblicato su Flickr](http://britishlibrary.typepad.co.uk/digital-scholarship/2013/12/a-million-first-steps.html) un milione di immagini prodotte negli ultimi due o tre secoli.

Qualcosa di vecchio, qualcosa di nuovo. Il nostro passato su questi binari digitali che sono il nostro futuro.

Buon Natale, di cuore, a tutti e un grosso grazie.

<iframe width="560" height="315" src="//www.youtube.com/watch?v=nhwhnEe7CjE" frameborder="0" allowfullscreen></iframe>
