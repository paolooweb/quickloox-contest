---
title: "Mangiare il panettone"
date: 2017-12-12
comments: true
tags: [Ambrosia, Maelstrom]
---
Si era parlato delle [difficoltà di Ambrosia Software](https://macintelligence.org/posts/2013-08-11-a-rischio-estinzione/) e oggi, capitando casualmente sulla [pagina home della società](https://www.ambrosiasw.com), constato che si apre una volta sì e tre no.

Le pagine interne a volte rispondono e altre volte, più spesso, no.

Pare che non sia stato annunciato con nettezza ma che i fatti portino alla conclusione più logica: si è conclusa anche la storia dei creatori di Maelstrom. Ambrosia Software non mangerà il panettone quest’inverno.

La [pagina ufficiale](https://www.libsdl.org/projects/Maelstrom/index.html) *de facto* del gioco (che si salva dall’oblio per via del passaggio, anni fa, alla licenza open source) riporta persino il file binario per giocare a BeOS, quello che in un universo parallelo è diventato il sistema operativo di Apple del XXI secolo in luogo di Mac OS X (oggi macOS). Commovente.

Ambrosia ha realizzato tante altre cose anche più utili di Maelstrom, ma preferisco ricordarla per questo: un giochino che sta in 3,8 megabyte, funziona anche sui Mac odierni (almeno fino a El Capitan) ed è capace di coinvolgere come se fosse stato approntato ieri.