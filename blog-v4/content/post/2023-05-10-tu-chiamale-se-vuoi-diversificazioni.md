---
title: "Tu chiamale se vuoi diversificazioni"
date: 2023-05-10T02:50:23+01:00
draft: false
toc: false
comments: true
categories: [Internet]
tags: [Buffett, Warren Buffett, Berkshire Hathaway]
---
Warren Buffett è un investitore novantenne privo di qualsiasi voglia di andare in pensione, che amministra tramite la propria Berkshire Hathawaway un portafoglio di 328 miliardi di dollari.

Durante l’ultima assemblea degli azionisti, [si è saputo](https://www.patentlyapple.com/2023/05/warren-buffett-told-his-shareholders-today-that-apple-is-a-better-business-than-any-other-in-berkshire-hathaways-portfolio.html) che nel 2016 Berkshire Hathaway aveva investimenti in azioni Apple per un miliardo di dollari.

Nel 2023 ha investimenti in azioni Apple per centocinquantuno miliardi di dollari. Il quarantasei percento di tutti gli investimenti di Berkshire Hathaway sono in titoli Apple. L’investimento oramai dura da più di dieci anni, non è una stella cadente.

Buffett non è un simpatico rimbambito e non ama perdere soldi. Come ha dichiarato agli azionisti,

>Apple è diversa dalle altre attività in cui investiamo, semplicemente, Apple è un’attività migliore.

Ci hanno insegnato tutti che la prima regola per minimizzare il rischio è diversificare, ma ciascuno si è sfilato in fretta al momento di vagliare il rapporto tra rischio e ricompensa. Il vantaggio di avere centocinquantuno miliardi su un singolo titolo supera evidentemente i rischi di avere quasi metà del patrimonio investita in un titolo solo.

Se tu avessi trecentoventisei euro da investire, ne metteresti a cuor leggero centocinquantuno su azioni Apple? O sarebbe meglio diversificare?

Quando le dimensioni sono di questa portata, hanno ancora senso i ragionamenti classici a prescindere?