---
title: "Modernismi"
date: 2020-09-28
comments: true
tags: [Lwn, emacs, iPad, Mac, Stallman, LaTeX, Csv, Pdf, BBEdit, Android]
---
Su *Lwn* si sono messi a discutere di [come rendere emacs più *moderno*](https://lwn.net/Articles/832311/) e accattivante per gli utenti di oggi.

Ci ho scritto persino io, anche se spostare un meccanismo in cui opera Richard Stallman è più difficile che spostare Giove dalla sua orbita.

La cosa che mi atterrisce è che ci si concentra su cose completamente irrilevanti come la curva di apprendimento, o i tutorial video.

emacs è e resterà un programma per iniziati. La sua forza è quella; dagli una interfaccia grafica, dagli un menu semplice e sarai al punto di prima. Con emacs fai cose vicine all’impossibile in qualunque altro ambiente. Naturalmente non si può vendere questo concetto a un pubblico ampio.

>Apri un file CSV, ordinalo per la seconda colonna, fanne una tabella LaTeX e genera un PDF tipograficamente elegante. Punti bonus se dal tuo editor puoi ispezionare il PDF creato. 

Per la maggioranza degli utilizzatori un obiettivo di questa portata non sarà mai neanche immaginato. Dunque emacs resterà, giustamente, di pochi per l’eternità.

Certamente quei pochi potrebbero essere un po’ più degli attuali e magari crescere con suprema lentezza.

A mio parere, la lista delle cose da fare per raggiungere questo obiettivo è essenziale:

* Portare emacs su iOS, iPad OS, Android.
* Abilitare almeno un meccanismo di sincronizzazione multipiattaforma.

A me frega niente della curva di apprendimento o della difficoltà di scoprire tutti i *mode* di funzionamento. Se sono motivato, mi ci metto. La cosa che mi frustra più di BBEdit è la sua assenza da iPad. Avere emacs su iPad e poter sincronizzare con Mac aumenterebbe vertiginosamente le mie motivazioni per usarlo. Se vale per me, vale per qualche milionata di persone che più o meno sono al mio livello e con motivazioni simili alle mie.

Fa male che il problema non sia tecnico, ma da azzeccagarbugli: emacs funziona sotto licenza GPL e, ostinato Richard Stallman a non mollare di un centimetro, ostinata Apple a non mollare neanche di un millimetro, l’uno non vuole pubblicare emacs su Apple Store; l’altra, se anche ci si provasse, lo rifiuterebbe.

Non so se questo stallo sia abbastanza moderno; certo è notevolmente stupido. Persone intelligenti avrebbero lavorato a un compromesso.