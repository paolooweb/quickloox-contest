---
title: "Passato di verdura"
date: 2021-09-25T02:11:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [Apple, USB-C, Msx, John Gruber, Rs-232, Intel, Apple, USB] 
---
Come [fa giustamente notare John Gruber](https://daringfireball.net/linked/2021/09/23/eu-usbc-mandate), se la [proposta europea di standardizzazione dei connettori per computer da tasca](https://www.nytimes.com/2021/09/23/business/european-union-apple-charging-port.html) fosse passata nel 2009, oggi saremmo a divertirci con un connettore a trenta pin o con quel microUsb che a guardarlo dentro sembra stato progettato da uno psicopatico.

Mentre non c’è il minimo dubbio che l’attuale USB-C prima o poi troverà un successore migliore. Per eliminare la spazzatura elettronica presente in forma di vecchi connettori sarebbe meglio finanziare la ricerca sulle forme migliori di carica wireless, che blindare il connettore e condannare mezzo miliardo di persone alla stasi tecnologica.

Al governo, come se fosse, dell’Europa sta gente ignorante e si sapeva. Ignoranti anche del passato. Le standardizzazioni o i tentativi di imporle nell’informatica non sono mancate, a partire dal mitico [MSX](https://www.msx.org) che nacque per razionalizzare la giungla dei microcomputer. Nacque vecchio e ebbe rilevanza zero.

USB ha effettivamente unito il mondo… grazie ad Apple, che lo adottò da sola mentre il mercato viveva ancora di porte seriali e parallele. USB generò certo una montagna di rifiuti informatici, ma portò connettori migliori anche per consumi e ingombri. Senza l’innovazione di Intel, popolarizzata da Apple attraverso una scelta elitaria, saremmo a baloccarci con Rs-232 e compagnia come quaranta (quaranta) anni fa.

Il bello del retrocomputing è che eventi favolosi e bellissimi vivono per una settimana e poi chiudono, per lasciare il posto al presente e a chi progetta il futuro. Vivere nel retrocomputing per sempre sembra una di quelle profezie da santone che sembrano promettere il paradiso e poi rivelano l’inferno.

Solo negli organismi europei si possono trovare personaggi sufficientemente ottusi da imporre il passato per legge e farlo pure passare come difesa del verde.

*Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile.*