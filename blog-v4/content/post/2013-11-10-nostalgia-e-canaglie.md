---
title: "Nostalgia e canaglie"
date: 2013-11-10
comments: true
tags: [Oubliette]
---
Su iPad o iPhone si possono giocare titoli come [D&D: Arena of War](https://itunes.apple.com/it/app/d-d-arena-of-war/id626418899?l=en&mt=8), che porta il marchio D&D e ha una concezione di gioco decisamente moderna. L’interfaccia è fantastica e l’usabilità è orribile: prima di capire esattamente come costruirsi a dovere un personaggio bisogna sperimentare non poco, oppure sono poco portato (e può essere). Giochi che senza iPad e tutto quello che è arrivato al traino di iPad non esisterebbero (basti guardare l’interfaccia del combattimento contro le immancabili canaglie *fantasy*).<!--more-->

Poi ci sono titoli come  [Oubliette](https://itunes.apple.com/it/app/oubliette/id376637764?l=en&mt=8), che come idea di gioco sono la stessa cosa. Ma Oubliette [è nato nel 1977](http://oub.liska.ca)! Senza iPad e tutto quello che ha seguito iPad, non esisterebbero più o quasi.

Questa capacità di iOS di ispirare l’innovazione e contemporaneamente riportare alla luce un passato informaticamente remotissimo mi piace da morire. Sono anche titoli *free-to-play* e chiedono soldi solo a chi non ha pazienza per fare da sé.