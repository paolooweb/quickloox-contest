---
title: "La prova dell’uno"
date: 2017-05-18
comments: true
tags: [Android, iPhone, Go]
---
Sappiamo già che [iPhone supera Android](https://macintelligence.org/posts/2016-09-14-concorrenza-asimmetrica/) in prestazioni seppure tramite specifiche hardware quantitativamente inferiori, ma questo vale poco contro un so-tutto o un geneticamente convinto.<!--more-->

Per fortuna Google ora fornisce la prova del nove, anzi, dell’uno: tra le novità prossime di Android c’è [Android Go](https://arstechnica.com/gadgets/2017/05/android-go-will-strip-android-down-for-ultra-low-budget-phones/). Una versione di Android ridotta all’osso per telefoni con un gigabyte di Ram o meno, considerati a basso costo e basse prestazioni.

Gli iPhone con più di un gigabyte di Ram sono relativamente recenti (da 6S in poi). E surclassano qualunque Android di pari classe, anche quando di Ram ne avesse il doppio.