---
title: "Caos calmo"
date: 2021-04-02T00:57:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [Shortcuts, Jason Snell, John Gruber, Automator, AppleScript, Mac, iOS, Comandi rapidi, macOS, Wwdc] 
---
John Gruber ha ragione: la storia dell’automazione per macOS e iOS è semplicemente [caotica e non pianificata](https://daringfireball.net/linked/2021/03/31/snell-mac-shortcuts).

Lo fa citando Jason Snell, che ha ragione pure lui, e spiega che [Mac ha bisogno dei Comandi rapidi](https://sixcolors.com/post/2021/03/the-mac-needs-shortcuts/). Né Automator né AppleScript arriveranno mai su iOS, mentre i Comandi rapidi potrebbero tranquillamente compiere il percorso inverso.

I Comandi rapidi per Snell sono il futuro possibile dell’automazione su Mac, per Gruber – che peraltro non prende posizione – finora si è solo generato caos. Due torti non fanno una ragione, ma due ragioni fanno una ragione al quadrato.

Questo, unito al fatto che Apple sta effettivamente ragionando non su un unico sistema operativo per tutti gli apparecchi, ma su sistemi operativi separati e però sempre più integrati, potrebbe lasciare ben sperare per qualche novità interessante in tema di automazione a [Wwdc](https://macintelligence.org/posts/Non-tutto-il-male.html).

Non c’è fretta. Ma chi ha la vista più lunga ha già cominciato a sfruttare i Comandi rapidi di iOS in modo massiccio e Snell porta esempi convincenti. Il passo successivo è logico, trova posto nelle logiche di Apple e porterebbe qualche ordine nel caos. Incrociamo le tastiere.

*Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile.*