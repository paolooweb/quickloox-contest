---
title: "La disruption del televisore"
date: 2023-06-07T11:20:32+01:00
draft: false
toc: false
comments: true
categories: [Hardware]
tags: [Vision Pro]
---
Sono seriamente impressionato. Non è necessariamente una cosa buona; watch mi aveva lasciato freddo e poi mi ha convinto sul campo. Magari qui succederà l’opposto.

Intanto Apple ha fatto la sua Cosa Giusta: trasformare un settore tramite la computerizzazione degli apparecchi usati. Chiunque sa fare telefoni migliori dei loro, ma nessuno sa fare computer da tasca come loro. E loro vendono computer, chiamati iPhone, watch, tv, iPad e ora Vision Pro. Mac  era un computer per forza, gli altri no.

Invece di proporre realtà virtuali, aumentate, miste, ibride – c’è tutta questa voglia di aggiungere realtà in tante persone che faticano già abbastanza per maneggiarne una? – Apple propone il computing spaziale. C’è quello da scrivania, quello da polso, quello desktop, quello piatto e ora anche quello che galleggia nello spazio che abbiamo intorno. Il paradigma rimane uguale a quello di sedici anni fa: se ti serve fare computing, scegli l’apparecchio che meglio to si adatta. C’è un apparecchio in più.

Invece delle rivoluzioni, delle spedizioni alla conquista di nuovi mercati, dei metaversi alternativi alla realtà, Apple dice che, volendo, puoi risolvere con una maschera sul viso, oltre che con una tavoletta o uno scatolino da attaccare al televisore. Questo sì, è dirompente e calpesta tutti gli scenari che altri raccontano e tentano di vendere da anni con scarso successo.

Quando Apple si mette di traverso a tutti gli altri, lavora al meglio. Nella presentazione o nel sito non c’è il minimo accenno a gergo di settore per iniziati e quacquaracquà che nel ripetere l’acronimo di moda fanno la ruota. *Spatial computing*, lo fa solo Apple. Vision Pro contiené un ambiente per fare quello che faresti con un iPad, solo che il puntatore è l’occhio e il clic si fa con pollice e indice che si toccano.

Tutti gli altri anelano all’immersività. Vision Pro, volendo, è immersivo e isola dall’esterno. Non volendo, un giro della Digital Crown ed ecco che si vedono le persone fuori. Si può parlare, interagire, chiacchierare. Vediamo loro e loro vedono noi. Tutti gli altri, davanti alla faccia, hanno dei coperchi opachi. Il rifiuto del mondo vero. Vision Pro offre la scelta, in ogni momento.

Qui arriva il punto dove si ricorda che la tecnologia sufficientemente avanzata è indistinguibile dalla magia, perché noi vediamo l’ambiente intorno, gli altri vedono i nostri occhi, la maschera è trasparente… invece è tutta una illusione. Vision Pro è imbottito di telecamere e microfoni e software per ricostruire tutto in modo che corrisponda alla realtà. Lo schermo non è affatto trasparente e proietta un nostro doppio digitale. Vediamo l’ambiente intorno, ma è una ripresa generata grazie a un lavoro straordinario, durato anni e costato chissà quanto in ricerca, con un chip apposta, R1, che lavora solo a quello. Vision Pro crea magia a un livello pazzesco per consentire l’interazione con l’esterno. Tutti gli altri, l’esterno lo vogliono ignorare. Noi, sembra incredibile, possiamo lavorare su un documento che fluttua davanti agli occhi usando la tastiera appoggiata sul tavolo. Quella vera.

Il dibattito su che tipo di realtà sia è stato sepolto e lasciato a tutto il vecchiume tecnologico che, fino a ieri, pareva avanguardia. Vision Pro permette certamente l’immersività totale e la fruizione di mondi paralleli. Ma è una di tante possibilità, non la ragione d’essere.

Quelli alle prese con la consueta dissonanza cognitiva hanno già fatto partire i dischi rotti: faccio le stesse cose a una frazione del prezzo, nessuno vuole lavorare con quella cosa sulla faccia per otto ore, la batteria dura troppo poco (due osservazioni un filino in contraddizione) e via dicendo.

Gente, lo hardware è una commodity. Il primo Macintosh costava duemilaquattrocentonovantacinque dollari, equivalenti a seimilaseicento-settemila dollari di oggi. iPhone costava troppo e faceva troppo poco; watch uguale. iPad, per Paolo Attivissimo, [rischiava di ribaltarsi per via dei bordi rialzati](https://attivissimo.blogspot.com/2010/01/aiped-aipad-aipod.html). Poi arrivano le iterazioni, le novità, i costi diminuiscono, si amplia la gamma; possibile non avere ancora capito che Apple non vuole vendere questo, ma quello che avrà tra cinque anni, o tre, o quello che sarà? Chi si preoccupa per gli occhiali fa bene, ma dimentica quanti cinturini si vendono per watch. Dove c’è una esigenza, verrà coperta nei tempi della roadmap che non conosciamo, ma Apple sì. Vision Pro di seconda generazione è già sul tavolo di progetto.

Che poi, quanto costano due iMac? Se proprio bisogna fare un discorso sullo hardware, due monitor 4k, un apparato audiovideo clamoroso con microfoni e telecamere a go-go, miniaturizzazione prodigiosa, un chip creato appositamente, la ricerca sui materiali e sul design. Tremilacinquecento dollari sono tanti, eh. Ma sono anche pochi.

Tanta gente è perplessa causa riflesso condizionato verso tutto quello che è nuovo e diverso e vuole che Vision Pro fallisca per non dovercisi confrontare o per risolvere la loro dissonanza cognitiva. Pazienza. In qualche momento del 2024, milioni di persone entreranno in un Apple Store e proveranno Vision Pro, assistite da personale preparato. E se rimanessero conquistate in numeri interessanti?

Le incognite di una nuova piattaforma sono innumerevoli. Però, accidenti se la partenza fa impressione.

Dimenticavo. C’è sempre qualcuno che sfodera con sapienza la domanda sulla *killer application*. Beh, vedere un film con Vision Pro sembra essere un’esperienza notevole e ancora di più lo sport, dove l’immersività ha gran valore. Ho il sospetto che anche l’esperienza 3D sia molto migliore del mettersi gli occhialini nel multisala. Se io vendessi televisori, due pensieri sul mio business inizierei a farmeli.

Così facciamo contenti anche quelli che vogliono la *disruption*.