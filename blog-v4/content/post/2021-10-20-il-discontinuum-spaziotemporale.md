---
title: "Il discontinuum spaziotemporale"
date: 2021-10-20T00:49:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [Automattic, TechCrunch, WordPress, P2, Slack, Jira] 
---
Qualcuno aveva già capito che il lavoro non richiede per forza, sempre, necessariamente la presenza simultanea in ufficio. La pandemia ha forzato anche i più riottosi ad accorgersi che la collaborazione e la produttività possono trovarsi distribuiti nello spazio, oltre i confini fisici.

Siccome la stessa cosa vale anche per i confini temporali, è ora di renderlo chiaro a quanta più gente possibile, anche se la pandemia questo non lo catalizza.

Un bell’articolo di *TechCrunch*, [Il futuro del lavoro remoto è il testo](https://techcrunch.com/2021/10/19/automattic-tc1-remote/), parla di Automattic, l’azienda che ha creato WordPress (maledetti) e che è priva di confini fisici: dà lavoro a millesettecento persone e non ha un ufficio.

Viene fuori che anche la loro comunicazione è pressoché interamente scritta e asincrona. La ridda di riunioni, meeting, videoconferenze tipica di molte situazioni italiane (non solo) semplicemente non esiste. Il giornalista chiede un appuntamento alla responsabile del marketing di WordPress.com e si stupisce che la sua agenda sia incredibilmente libera da meeting. Così risponde la per nulla sventurata:

>È la cosa che penso non funzioni del normale lavoro di ufficio. L’agenda piena usava essere una specie di vanto e essere pieni di meeting significava essere importanti. Questo, qui, non è un paradigma.

Praticamente tutto il lavoro si svolge attraverso P2, una modifica di WordPress (maledetti):

>Un post su P2 è più o meno un post su un blog, con discussioni in thread, la possibilità di seguire le risposte e un pulsante Like.

Lo strumento è relativo ovviamente, conta il concetto:

>P2 non è il solo metodo per comunicare in Automattic; l’azienda usa anche software come Slack e Jira. Nondimeno, gli strumenti in gioco condividono il tema della asincronia: i tuoi colleghi sono sparsi per il mondo, dunque non attenderti una risposta immediata.

*Eh, ma a volte serve per forza la riunione, il chiarimento, vedersi, altrimenti non si combina niente.* Sempre la responsabile marketing:

>Si può pensare che qui le cose si muovano più lente, ma in realtà non è così. Negli uffici c’è anche questa idea che si debba per forza essere in presenza. Le persone a volte aspettano a dire cose che arrivi il momento della riunione, quando avrebbero potuto inviare una email. Spesso mi ritrovo a svegliarmi e scoprire di dover recuperare su cose che stanno già accadendo in Europa, contro aspettare una settimana per avere la sala riunioni disponibile e poterne parlare.

Quanto potere va in fumo se si tolgono i vincoli di spazio e tempo. Funziona nelle aziende dove l’idea di documentare tutto e scrivere tutto, come si fa in Automattic, è improponibile causa analfabetismo funzionale e arretratezza. Più una realtà lavorativa è vitale, più è asincrona.

*Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile.*