---
title: "Giroscopio che vai"
date: 2014-05-23
comments: true
tags: [Bounden, iOS, Android, Gruber, giroscopio]
---
C’era quella campagna [Hai un potere che non immaginavi](https://www.apple.com/it/iphone-5s/powerful/).<!--more-->

I programmatori di [Game Oven](http://gameovenstudios.com/) hanno lavorato assieme al [corpo di ballo nazionale olandese](http://www.operaballet.nl/en/ballet/ballet) per creare la *app* [Bounden](http://playbounden.com).

Il computer da tasca va tenuto in mano congiuntamente da una coppia, a Bounden in funzione. La *app* mostra sullo schermo punti situati sopra una sfera virtuale che avvolge la coppia. Insieme i due devono spostare l’apparecchio nello spazio per catturare uno dopo l’altro i punti. E senza rendersene conto, iniziano a danzare.

La *app* è stata pensata per iOS e Android. Solo che quella iOS è [regolarmente su App Store](https://itunes.apple.com/it/app/bounden/id850456491?l=en&mt=8); quella per Android è stata sospesa e gli autori hanno chiesto aiuto a tutti i volontari.

Succede che Bounden si basa sulla lettura dei sensori di posizione e movimento di bordo del computer da tasca. Quando i programmatori hanno allargato il raggio dei test degli apparecchi Android, [si sono resi conto di un problema](http://gameovenstudios.com/bounden-on-android-delayed/):

>Alcuni apparecchi hanno giroscopi ‘rotti’ che non funzionano su tutti gli assi; alcuni apparecchi fingono di avere un giroscopio mescolando i dati dell’accelerometro con quelli della bussola; alcuni apparecchi non hanno un giroscopio.

Ci sono possibilità che due apparecchi Android nella stessa posizione e mossi nella stessa direzione leggano spostamenti e posizione diversi. E addio alla magia di Bounden.

Incuriosito dalla vicenda, John Gruber di *Daring Fireball* [ha messo alla prova](http://daringfireball.net/linked/2014/05/20/gyroscope-fragmentation) gli apparecchi iOS a sua disposizione. Il risultato è che non sono strumenti di precisione – e si sapeva – però leggono dati ragionevolmente coerenti e affidabili. Due iPhone nella stessa posizione, mossi nella stessa direzione, capiscono la stessa cosa.

La pubblicità spiega che con iPhone hai un potere che non immagini, perché è vero: prima di Bounden, nessuno ha pensato a danzare così.

Con Android non sei neanche sicuro di dove ti muovi.