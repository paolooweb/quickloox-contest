---
title: "Solo testo"
date: 2016-02-02
comments: true
tags: [BBEdit]
---
L’elenco delle aggiunte, modifiche e sistemazioni apportate a [BBEdit 11.5](http://www.barebones.com/support/bbedit/current_notes.html) vale da solo il prezzo del biglietto di un utilizzatore professionale.<!--more-->

Ci sono tanti altri programmi validi per fare editing di testo, naturalmente. Questione di dove si vuole arrivare e che cosa è adeguato alla bisogna.