---
title: "Pensieri semplici"
date: 2013-06-23
comments: true
tags: [iPhone, design, interfacce]
---
Primo.

Le [ultime pubblicità di iPhone](http://www.apple.com/iphone/videos/) si concentrano sullo scattare foto e sull’ascolto di musica. I gigahertz, le Ram, contano niente. Conta l’esperienza quotidiana, conta la semplicità.<!--more-->

Secondo.

[Scrive](http://justinjackson.ca/words.html) Justin Jackson:

>Il cuore del design per il web dovrebbe riguardare le parole.

La cosa più semplice possibile.

Terzo.

 ![Interfaccia](/images/interfaccia.jpg  "Interfaccia") 

Qualunque interfaccia di ampio uso pubblico dovrebbe essere così semplice che perfino l’addetto alla riparazione capisce come funzioni. Non ci siamo ancora.