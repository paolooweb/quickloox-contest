---
title: "Una giornata di Safer Internet"
date: 2014-02-22
comments: true
tags: [Wirecast, Marconi, sicurezza]
---
Finalmente tornato da Rovereto dopo una giornata massacrante e bellissima di incontri con insegnanti, genitori e ragazzi presso l’[Istituto Tecnico e Tecnologico Guglielmo Marconi](http://www.marconirovereto.it) per parlare di una Rete più sicura, il [Safer Internet Day](http://www.saferinternetday.org/web/guest).<!--more-->

Mantengo la sintesi qui perché su YouTube sono disponibili [oltre un'ora di streaming mattutino assieme ai ragazzi](http://www.youtube.com/watch?v=uvmoOdq7wAk&index=3&list=PL_MtOuQ1lY4TXARmrGtFD5P_rzjglUEjX) e [altrettanto la sera](http://www.youtube.com/watch?v=tYtyhkacy-8&index=8&list=PL_MtOuQ1lY4TXARmrGtFD5P_rzjglUEjX) di venerdì 21 febbraio 2014. Gli interessati a tutta la manifestazione trovano altri video sul [canale YouTube](http://www.youtube.com/playlist?list=PL_MtOuQ1lY4TXARmrGtFD5P_rzjglUEjX) dell’evento.

Le emozioni non riesco a trasmetterle, la fatica è stata grande ma il ritorno immenso e così il calore e l’accoglienza che ho trovato. Una cornice ideale, dove l’unica cosa necessaria era stare all’altezza di tutto il lavoro svolto dal *team* di supporto.

Vorrei sottolineare che tutto è accaduto all’interno di una *scuola*. Quei posti mitologici dove non ci sarebbe il denaro per la carta igienica. Esistono certo luoghi dove il denaro viene sprecato da incapaci o intascato da furbi. Dove invece si trovano gente capace, voglia di fare, intelligenza e amore per i ragazzi si organizzano giornate trasmesse per video a tutta l’Italia interessata, con centinaia o migliaia di visualizzazioni da altri istituti del Trentino, dell’Umbria, della Sicilia, così come da famiglie, ragazzi presenti la mattina che tornano la sera a sentire di nuovo quasi le stesse cose eccetera.

Che poi, i tempi della tecnologia che costa sono passati. Parlavamo davanti a una normalissima *webcam* Logitech messa su un normale treppiede. L’unico vero investimento è [Wirecast](http://www.telestream.net/Wirecast/), peraltro a cinquecento dollari in versione base. Sopportabile. Ne ho fatto un ritratto nel *backstage*, qui sotto.

**Aggiornamento**: Andrea Trentini, l’insegnante che ha fornito la forza motrice principale e dosi sovrumane di entusiasmo e disponibilità per tutto lo svolgersi del progetto, precisa che si tratta di Wirecast, ma in versione *Wirecast for YouTube Studio*, dal prezzo di 176,54 euro. Mi sa che costa più l’approvazione dell’acquisto, che l’acquisto.

Chissà se abbiamo aiutato qualche ragazzo, qualche genitore. Certamente ce l’abbiamo messa tutta. Io dico che è servito.

 ![Wirecast all’istituto Marconi di Rovereto](/images/marconi.jpg  "Wirecast all’istituto Marconi di Rovereto") 