---
title: "A dorso di sapienza"
date: 2023-05-12T19:24:44+01:00
draft: false
toc: false
comments: true
categories: [Web]
tags: [Il blog del Mulo, Mimmo, R]
---
La frequenza di aggiornamento del [Blog del Mulo](https://muloblog.netlify.app) è relativa; ma ogni singolo articolo che esce è un piccolo, decisivo documento di riferimento su programmi, protocolli, standard, esperienza, script, [R](https://www.r-project.org), statistica, organizzazione di informazioni.

Il tono è di chi va al sodo, evita i giri di parole e centra il problema.

Attorno al blog, una istanza Slack (alla quale immagino si debba essere invitati), una istanza locale Mastodon e qualche altra sorpresa. Tutto facile, immediato, molto (ben) pensato.

Un’occhiata gliela darei e anche più di una. Nel dubbio, c’è un signor Rss, tale che è impossibile perdersi alcunché, basta volerlo. A ogni occhiata si impara qualcosa e si sale più in alto.