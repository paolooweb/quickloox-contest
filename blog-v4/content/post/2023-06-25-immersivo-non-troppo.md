---
title: "Immersivo non troppo"
date: 2023-06-25T17:03:19+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [visionOS, HIG, Human Interface Guidelines]
---
Cerco di seguire che accade alle Human Interface Guidelines di Apple man mano che entra in gioco la possibilità di scrivere app per visionOS.

[Designing for visionOS](https://developer.apple.com/design/human-interface-guidelines/designing-for-visionos) è solo un punto di partenza, ma ci si vedono già indizi interessanti del nuovo.

Per esempio, lavorare per il comfort delle persone, posizionare il contenuto dentro il campo visivo dell’utilizzatore, supportare bene i *gesti indiretti* (eseguiti indipendentemente dallo schermo, come fare clic con l’unione di pollice e indice tenuti in grembo).

Le due cose che mi piacciono di più: aiutare le persone a condividere le proprie attività; soprattutto, non presumere che ogni momento di un’esperienza sia totalmente immersivo.

La capacità di limitare l’immersività e permettere al mondo esterno di farsi percepire mi continua a parere un punto di forza e di differenza sostanziale. Quei coperchi bianchi di fronte ai visori concorrenti mi hanno sempre fatto brutta impressione.