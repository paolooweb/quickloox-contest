---
title: "Prodotti di punta"
date: 2015-11-21
comments: true
tags: [Apple, Pencil, Microsoft, iFixit, Caldwell, iMore, HB, matita]
---
[Si era parlato](https://macintelligence.org/posts/2015-09-19-asparagi-alla-prova/) della originalità di Apple Pencil rispetto ai prodotti similari esistenti e, ora che l’oggetto è effettivamente in vendita, pare ci sia della sostanza.<!--more-->

iFixit ha [smontato completamente la Pencil](https://www.ifixit.com/Teardown/Apple+Pencil+Teardown/52955) per scoprire *la scheda logica più piccola che abbiano mai visto* un grammo di peso, piegata in due per occupare meno spazio. Più varie altre sorprese di tipo ingegneristico.

[Serenity Caldwell su iMore](http://www.imore.com/ipad-pro-experiment-pencil-arrives-and-its-going-change-my-life) dichiara di usare penne digitali da sedici anni e di riconoscere nella Pencil qualcosa di speciale. Articolo lunghetto ma assai interessante. Un frammento:

>Dice qualcosa, il fatto che Apple offra la Pencil priva di una app di impostazione, come invece fanno Wacom e Microsoft. Anche gli stili di produzione indipendente dispongono di impostazioni per regolare il trattamento della pressione. […] Sapete? Sono d’accordo con Apple. Che essenzialmente dice: è uno strumento, esattamente come la tua matita HB. Non puoi regolare la tua penna HB per dirle di usare un tratto più leggero. Devi imparare a usarla. Devi fidarti di lei.

Un oggetto tecnologico che funziona come deve senza impostazioni, per tutti o per quasi tutti, è davvero particolare.