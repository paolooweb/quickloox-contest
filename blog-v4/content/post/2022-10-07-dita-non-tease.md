---
title: "Dita, non tease"
date: 2022-10-07T02:52:12+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [iOS, iPadOS, discoverability]
---
Se c’è una differenza fondamentale tra macOS e iOS/iPadOS, a dispetto di quanti insistono sulla loro omogeneizzazione, è che una buona app su Mac mostra tutti i comandi a disposizione e una buona app su iOS ne mostra pochino nessuno, ma li lascia scoprire, in base al principio della *discoverability*.

Ne segue che è legittimo da una parte lamentarsi di funzioni che non si trovano; dall’altra, perdiamo qualcosa se scordiamo di essere esplorativi, curiosi, prendendo a prestito l’attitudine dei più piccoli a mettere a ferro e fuoco qualunque interfaccia.

Questa [raccolta di gesture per iPhone e iPad](https://mjtsai.com/blog/2022/10/07/ios-action-discoverability/) è limitata nella quantità eppure contiene almeno un paio di rivelazioni sostanziali nel facilitare il lavoro con l’interfaccia touch con app chiave come Mail e Safari. Provare per credere. E provare sempre a fare qualcosa di non previsto quando usiamo iOS, uno *swipe*, una azione con due o tre dita… se una app è ben disegnata, dà gusto.