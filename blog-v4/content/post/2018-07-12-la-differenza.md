---
title: "La differenza"
date: 2018-07-12
comments: true
tags: [Affinity, Surface, iPad]
---
Microsoft ha pubblicato un nuovo Surface economico, per cercare di rosicchiare qualche briciola all’iPad ora capace di usare Apple Pencil.

La concorrenza è un bene; il rovescio della medaglia è che compaiono pagine di inutilità leggendaria, tipo [caratteristiche e prezzi a confronto](https://www.money.it/Apple-iPad-vs-Microsoft-Surface-Go-miglior-prezzo-caratteristiche).

Come se contassero qualcosa, i prezzi e le caratteristiche.

La vera differenza è che poi per iPad esce [Affinity Designer](https://itunes.apple.com/us/app/affinity-designer/id1274090551). E altre cento meraviglie che gli altri se le scordano.