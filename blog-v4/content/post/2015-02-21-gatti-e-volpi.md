---
title: "Gatti e volpi"
date: 2015-02-22
comments: true
tags: [Lenovo, Microsoft, Superfish, U2, Apple, iTunes, Komodia, Windows, SignatureExperience]
---
Lenovo vende computer Windows con dentro un marchingegno software che abilita l’inserimento di pubblicità nelle ricerche effettuate con il *browser*. Il marchingegno si chiama *Superfish*. Detto tutto.<!--more-->

Il marchingegno intercetta la comunicazione tra il manovratore e Internet compiuta attraverso il *browser* e si inserisce nel flusso delle informazioni alterando i dati in arrivo. Nel gergo questo è un attacco *man on the middle*. Il manovratore riceve pubblicità che non aveva chiesto e non arriva dal motore di ricerca che ha consultato, bensì da un soggetto non identificato.

Per raggiungere questo bell’obiettivo, il marchingegno [crea un buco di sicurezza mica da ridere](http://arstechnica.com/security/2015/02/lenovo-pcs-ship-with-man-in-the-middle-adware-that-breaks-https-connections/). Installa un certificato di sicurezza farlocco e, quando ci si collega a un sito che usa traffico cifrato per sicurezza, si interpone nella comunicazione impersonando il componente di sicurezza del sito in oggetto.

In altre parole: mi collego alla banca in modo cifrato, svolgo le mie operazioni e il tutto viene autenticato non dalla banca, ma da Superfish, senza che io lo sappia.

Il bello è che il certificato è lo stesso su tutti i computer e [il meccanismo di cifratura è noto](http://blog.erratasec.com/2015/02/extracting-superfish-certificate.html). Può venire potenzialmente abusato da un pirata informatico mentre io credo di dialogare in cifra con la mia banca, grazie alle meraviglie di Superfish.

La cosa salta fuori e i dirigenti di Lenovo si affrettano a pubblicare uno strumento che elimina il marchingegno, con tanto di [scuse](http://www.cio.com/article/2886914/security/lenovo-admits-to-superfish-screwup-will-release-cleanup-tool.html).

(Ricordiamo qui il baccano scatenato dai più quando Apple [pubblicò uno strumento per eliminare un album musicale](https://macintelligence.org/posts/2014-09-23-il-trionfo-della-volonta/) dalle raccolte iTunes di chi non lo voleva in regalo e ciononostante lo aveva scaricato).

Microsoft, produttrice di Windows, artefice dei meccanismi scaravoltati da chi ha ideato il marchingegno, [aggiorna il software di difesa dal *malware*](http://arstechnica.com/security/2015/02/windows-defender-now-removes-superfish-malware-if-youre-lucky/) per includere l’individuazione e l’eliminazione del marchingegno. Per chi usa Internet Explorer; Su Firefox e Thunderbird l’infezione resta.

Attenzione, però: se sul computer è presente un *antimalware* di qualche altro produttore, il software Microsoft [si disattiva e gli lascia la precedenza](http://arstechnica.com/security/2015/02/windows-defender-now-removes-superfish-malware-if-youre-lucky/). Se l’*antimalware* è solo una copia di prova (come su tutti i computer appena venduti) che viene lasciata scadere, il software Microsoft si offre di entrare in funzione dopo quindici giorni di avvisi. Quindici giorni di effettivo buco di protezione durante i quali può succedere di tutto e il suo contrario.

Dimenticavo: su Microsoft Store si possono comprare computer con [Signature Experience](http://www.microsoftstore.com/store?SiteID=msusa&Locale=en_US&Action=ContentTheme&pbPage=MicrosoftSignature&ThemeID=33363200). Un servizio che depura i computer stessi da tutte le schifezze tipo Superfish, installate dai produttori che vendono computer Windows. Costa solo, ma guarda, un po’ di più. Stupenda sulla pagina la sequenza *prima-dopo*: come si riduce un computer Windows senza Signature Experience nel giro di un mese, come resta pulito pagando a Microsoft l’extra per bonificarlo. Non ho parole.

C’è gente che ancora si fida e questo è straordinario. Non esattamente in senso positivo.

Chi giochi al gatto è evidente, chi faccia la volpe è chiaro. I polli, piuttosto.