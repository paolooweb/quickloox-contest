---
title: "Il ritorno del re"
date: 2018-09-01
comments: true
tags: [Colombini, Another, Sight, Steam, castello, avventura]
---
Sapere che [Enrico Colombini](http://www.erix.it) è nuovamente di scena è come l’uscita di un album di Mina oppure una prima serata di Fiorello: un evento. Lavorava nell’ombra, come ogni creatore di mondi che si rispetti, dai tempi di [Locusta Temporis](https://macintelligence.org/posts/2017-06-27-se-questo-e-un-gioco/).

Il re dell’avventura italiana – a partire da quella, oramai leggenda, [nel castello](http://www.erix.it/retro/storia_cast.html) – ha lasciato l’impronta, inconfondibile, nel gioco [Another Sight](https://store.steampowered.com/app/888630/Another_Sight/), di cui ha realizzato i testi e che debutterà su Steam il 6 settembre.

Il primo di settembre è il giorno simbolico di fine vacanze e ritorno agli onori e oneri del lavoro. Però, se è rimasto tanto così di budget per i videogiochi, ora è allocato. Capita di rado di essere tanto garantiti sulla bontà di un titolo.
