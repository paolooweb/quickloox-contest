---
title: "A volte ritornano"
date: 2022-01-28T01:33:53+01:00
draft: false
toc: false
comments: true
categories:
tags:
---
Apple [ha polverizzato ogni record di vendite nel trimestre natalizio](https://www.apple.com/newsroom/2022/01/apple-reports-first-quarter-results/).

Io mi accontento di riavere i commenti in calce ai post, grazie all’amicizia e alla gentilezza di **Mimmo** che ha svolto tutto il lavoro e che ringrazio di cuore.

Prossime tappe in ordine di realizzazione:

- date nella home;
- funzionamento degli embed dei tweet;
- ritorno dei vecchi post;
- valutazione di un nuovo tema;
- altre migliorie assortite.

Confesso che non ho abbandonato la speranza di riuscire a bloggare usando Lisp e che un giorno potrebbe tornare [Coleslaw](https://github.com/coleslaw-org/coleslaw) come motore, ma a patto che le funzioni acquisite qui non si perdano, quindi con i commenti a posto.

Vediamo. Oggi intanto si va a nanna (più) contenti e senza messaggi temporanei a pié di pagina.
