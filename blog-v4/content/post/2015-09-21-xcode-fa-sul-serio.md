---
title: "Xcode fa sul serio"
date: 2015-09-21
comments: true
tags: [Xcode, ElCapitan, OSX, Git, BitBucket, Unix]
---
L’antefatto è che una serie di lavori, compreso il libro su [El Capitan](https://www.apple.com/it/osx/elcapitan-preview/) appena terminato e consegnato all’editore, passa da un contenitore su [BitBucket](https://bitbucket.org) sul quale si collabora attraverso [Git](https://git-scm.com) (si può lavorare in più di uno sullo stesso file nello stesso momento e il software si cura di evitare problemi).<!--more-->

Ho installato [Xcode 7](https://developer.apple.com/xcode/download/), la versione per El Capitan, e quando ho dato il primo comando Git mi è apparsa la richiesta di approvazione della licenza di utilizzo di Xcode stesso. Nel Terminale!

Sembrerà una cosa di poco conto. A memoria, è la prima volta che vedo un accordo di licenza Apple apparire in un ambiente Unix.
