---
title: "Il livello è sempre quello"
date: 2015-09-04
comments: true
tags: [Apple]
---
Sul finire dell’estate arrivano le banalità assolute. Intuizione notturna: chiedersi se i prodotti Apple costino tanto, poco, giusto, è esercizio sterile.<!--more-->

Bisogna invece partire da un’altra constatazione: *Apple cerca di venderti ogni tipo di computer sempre allo stesso prezzo*. Parliamo ovviamente di grandezze comparabili, non di numeri uguali fino all’ultimo decimale.

La domanda è allora *quanto sono disposto a dare ad Apple per un computer di qualsiasi tipo*.

Se la risposta è, poniamo, mille, Apple cerca di venderci un watch che valga mille, un iPhone che valga mille, un iPad che valga mille, un Mac che valga mille (magari anche millecinquecento).

Il ragionamento si stiracchia facilmente, eppure non lo trovo insensato anche se in lontananza si vede un temporale estivo. Apple lavora sulla nostra percezione dei prodotti e, per vita, stipendio, esperienze, ognuno di noi ha un livello di valutazione abbastanza preciso in testa. Se troviamo il prodotto a quel livello, acquistiamo.