---
title: "Come te nessuno mai"
date: 2022-02-11T01:27:30+01:00
draft: false
toc: false
comments: true
categories:
tags:
---
Vale la pena seguire – e criticare se è il caso – Apple perché è *davvero* una azienda diversa dalle altre, come testimonia la mia lettura per il weekend: [Come Apple è organizzata per l’innovazione](https://hbr.org/2020/11/how-apple-is-organized-for-innovation), di Joel Podolny e Morten Hansen.

Podolny ha retto per molti anni, [fino ai tempi recenti](https://appleinsider.com/articles/21/12/13/apple-university-dean-joel-podolny-exits-apple-for-startup), Apple University e pochi sono più in grado di lui di descrivere l’anima della struttura e del funzionamento di Apple.

> Quando Jobs tornò in Apple, trovò una struttura convenzionale per una azienda di quella dimensione e per il suo settore di attività. Era divisa in unità di business, ciascuna responsabile in proprio di profitti e perdite. C’erano direttori, tra gli altri, della divisione Macintosh, dei prodotti per la linea server e per gli apparecchi da informatica domestica. Come accade spesso dove ci sono unità di business decentralizzate, i direttori tendevano a confliggere l’uno con l’altro. Convinto che il management convenzionale penalizzasse l’innovazione Jobs, nel primo anno come amministratore delegato dopo il suo ritorno, licenziò in un giorno tutti i direttori delle unità di business, sottomise l’azienda a un unico conto di perdite e profitti e riunì i vari reparti in una unica organizzazione funzionale.

Niente di nuovo. Dobbiamo però ricordare questo:

> L’adozione di una struttura funzionale può non avere destato sorprese per una azienda come era Apple per dimensioni in quel momento. Ciò che è veramente sorprendente invece – in effetti, rimarchevole – è che Apple mantenga oggi la stessa organizzazione, sebbene la società sia quaranta volte più grande per fatturato e vastamente più complessa di quanto fosse nel 1998.

Nessun’altra società lo fa, a mia conoscenza, nei settori dove opera Apple e sarei interessatissimo a essere smentito. Si tratta di un’impresa del tutto paragonabile a mettere in piedi una struttura di centinaia di negozi in tutto il mondo [con una redditività superiore a quella di Tiffany](https://www.cnbc.com/2017/07/29/here-are-the-retailers-that-make-the-most-money-per-square-foot-on-their-real-estate.html) oppure a costruire una sede curata nei minimi dettagli per quanto è concepibile farlo su [un’astronave da dodicimila persone rivestita interamente di vetro](https://www.fosterandpartners.com/projects/apple-park/).
