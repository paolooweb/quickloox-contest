---
title: "Insegnagli a pescare"
date: 2019-11-11
comments: true
tags: [Terpstra, fish, bash, zsh, Catalina, macOS, shebang]
---
Una delle variazioni più discusse introdotte da macOS Catalina è la sostituzione di [bash](https://www.gnu.org/software/bash/) con [zsh](http://zsh.sourceforge.net/).

Ho notato (partendo prima di tutto da me) che meno si conosce la shell più si tende ad discutere del cambio e che quindi, per evitare di discutere a favore di fare scelte utili e concrete, è meglio iniziare ad approfondire il tema.

Su questa falsariga segnalo l’articolo in cui [Brett Terpstra scopre fish](https://brettterpstra.com/2019/10/11/branching-out-from-bash-fishing-expedition/), un’altra delle alternative possibili a bash.

L’arricolo è pieno di agganci utili a chi ha poca esperienza e di esplorazioni perfette per chi invece maneggia la shell come routine, con precisazioni di buon senso (la compatibilità degli script è totale, per esempio, perché non è importante; se il file contiene lo *shebang* iniziale `#!/bin/bash`, viene eseguito da bash, che è sempre lì, nessuno la cancella) e esempi validi per farsi un’idea.

In fin dei conti, se non esiste una vera ragione per abbandonare bash, ha poco senso farlo; la shell è presente nel sistema e, un domani, la si potrebbe installare se venisse a mancare. Adottare zsh per seguire comunque le linee tracciate da Apple può essere saggio, ma non immediatamente e non so se le differenze minime giustifichino lo sforzo, meglio, lo giustifichino *adesso*, che sostanzialmente non serve.

Tanto vale allora, come ha fatto Terpstra, pescare una alternativa che più di altre sentiamo adeguata senza sentirci pressati sulla scelta di zsh. Il suo pezzo offre anche un bel punto di vista sulle questioni primarie da considerare all’atto di pensare a cambiare.
