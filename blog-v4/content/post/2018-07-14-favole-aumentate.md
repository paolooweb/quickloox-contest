---
title: "Favole aumentate"
date: 2018-07-14
comments: true
tags: [AR, Android, Accomazzi.net]
---
In un intervallo della serata di gioco di ruolo:

>Abbiamo preso uno stagista veramente bravo. Sta preparando una app di fiabe in realtà aumentata. Che se solo funzionasse decentemente su Android… stiamo collaudando su un modello di punta e sì, ogni tanto vede il pavimento. Ogni tanto.

Lo sviluppo della app per iPhone invece procede a gonfie vele e il pavimento viene sempre visto nel modo giusto per applicargli la realtà aumentata.

Favola aumentata è invece che Android faccia tutto allo stesso modo e costi pure meno. Un modello di punta fa risparmiare poco o nulla e il risultato, appena si tocca tecnologia di punta, è fantastico: nel senso che bisogna crederci come alle favole.