---
title: "È cambiato il metronomo"
date: 2022-05-26T15:22:39+01:00
draft: false
toc: false
comments: true
categories: [Hardware]
tags: [Apple, Samsung, M1, Neowin, Intel, Zappa, Frank Zappa, Amd]
---
Tic tac, tic tac, prima il ritmo lo dava Intel, poi PowerPc, poi di nuovo Intel, poi Md e da quando Apple ha tirato fuori M1 la musica è cambiata di più. È cambiato il ritmo. Un po’ come [Frank Zappa](https://www.jazzwise.com/features/article/frank-zappa-s-jazz-legacy) quando suonava dal vivo e accelerava clamorosamente il brano.

Qualcomm [ha già provato](https://macintelligence.org/posts/2022-05-03-corse-di-recupero/) l’ ebbrezza del tempo, diciamo, giovanile.

Ora tocca a Samsung che, riferisce *NeoWin*, [ha assemblato un Dream Team per battere Apple nel 2025](https://www.neowin.net/news/samsung-allegedly-assembling-a-dream-team-to-take-down-apples-m1-in-2025/).

Nel 2025.

Tre anni di orchestra che continua a suonare a ritmo accelerato e lo spartito si sfoglia una pagina via l’altra. Non è infinito; a un certo punto l’orchestra cambierà canzone e qualcuno sarà rimasto veramente indietro.

Oltre che la musica è cambiato proprio il metronomo. Non c’è più il ritmo di una volta. Tic tac, tic tac.
