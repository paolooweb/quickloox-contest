---
title: "L'anno che venne"
date: 2014-01-18
comments: true
tags: [Siracusa, Apple]
---
Per le opinioni da bar c’è sempre tempo; per parte mia, trovo John Siracusa l’unico veramente autorizzato a [dare i voti al 2013 di Apple](http://hypercritical.co/2014/01/02/apples-2013-scorecard). Perché ne sa e di tutto può essere accusato, tranne che essere un tifoso o un illuso.<!--more-->

Sono ampiamente d’accordo con la sua pagella, tranne due eccezioni. Darei un voto leggermente più alto a iLife e iWork (magari B) e non ne darei affatto sulla questione televisione.

Se Apple annuncia un prodotto e questo esce buono ma in ritardo, oppure puntuale e cattivo, merita una valutazione negativa. Ma non vedo perché punirla per qualcosa che non ha annunciato, nemmeno in termini generici, neanche nominata e basta.

Una pagella non contiene voti relativi a materie inventate dal docente.