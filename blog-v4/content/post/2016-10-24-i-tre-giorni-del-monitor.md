---
title: "I tre giorni del monitor"
date: 2016-10-24
comments: true
tags: [Windows, Mac, Stefano]
---
Due giorni di immagini indicative del ruolo di Mac nelle vite e nel lavoro delle persone. Oggi, foto pubblica di schermo Windows da parte di **Stefano**:

>Plugin di attesa al Foreign Office.

Suppongo in Paese straniero. Ma l’idea dei sistemi che malfunzionano è evidentemente transnazionale.

 ![Plugin al Foreign Office](/images/foreign-office.jpg  "Plugin al Foreign office") 