---
title: "La versione di Ulisse"
date: 2016-11-22
comments: true
tags: [0AD]
---
Da pochi giorni è stata annunciata la ventunesima versione *alpha* di [0 A.D.](https://play0ad.com), denominata *Ulysses* (la U è per l’appunto la ventunesima lettera dell’alfabeto inglese).

0 A.D. ha di speciale quello che dovrebbe essere normale: è *open source*, è disponibile per tutti i sistemi operativi desktop, quindi anche per macOS.

E poi una cosa ancora più speciale: questo gioco di simulazione bellica storica è in grafica 3D e crea viste veramente, veramente [spettacolari](https://www.youtube.com/watch?time_continue=102&v=-0EJRimOuVE). C’è da chiedersi quanta dedizione e quanta bravura vengano messe al servizio di un progetto come questo, che è gratis e vive di donazioni.

Se ne va un bel gigabyte abbondante di spazio su disco. Se piace il genere, però, ne vale la pena e, per quanto *alpha* significa *non è detto che a questo stadio di sviluppo funzioni sempre e comunque*, c’è da divertirsi. Chi fosse alla ricerca oziosa di qualcosa da giocare nelle vacanze di Natale, potrebbe dargli una possibilità.

<iframe width="560" height="315" src="https://www.youtube.com/embed/-0EJRimOuVE" frameborder="0" allowfullscreen></iframe>