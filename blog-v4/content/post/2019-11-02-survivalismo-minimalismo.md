---
title: "Survivalismo, minimalismo"
date: 2019-11-02
comments: true
tags: [Drang, BBEdit, Cook]
---
John D. Cook non si considera un vero e proprio [survivalista computazionale](https://www.johndcook.com/blog/2019/10/09/computational-survivalist/), ma apprezza la prospettiva dell’approccio:

>Cercare di fare tutto il possibile con strumenti elementari di riga di comando, nell’idea che ci si possa trovare qualche volta nella situazione di non potere usare nient’altro.

Non condivido l’idea; tra connessioni remote, reti Wi-Fi, macchine virtuali e apparecchi che stanno in una tasca, trovarsi limitati alla riga di comando mi pare improbabile. Mi piace l’approccio, invece, perché sono strumenti economici, veloci, appunto elementari: mattoni fondamentali da costruzione. Per lo stesso motivo apprezzo il linguaggio [Lisp](http://www.gigamonkeys.com/book/introduction-why-lisp.html), che in linea di principio può essere sviluppato arbitrariamente a qualsiasi livello attraverso la combinazione di poche istruzioni base.

Come Cook, mi riconosco maggiormente nel *minimalismo computazionale*:

>Si può essere più produttivi se si conosce bene un insieme di strumenti ristretto invece di tante applicazioni maneggiate a malapena.

Definizioni a parte, questi punti di vista portano a *più conoscenza grazie all’uso di meno strumenti o strumenti più semplici*, che è il motivo base per buttare Office nel cestino, tanto per cominciare.

Cook riferisce per esempio di un problema come contare nella shell Unix il numero di volte in cui un carattere appare in un file di testo.

`grep carattere -o nome file | wc -l`

In diverse applicazioni si troverà ugualmente una risposta; con [BBEdit](https://www.barebones.com/products/bbedit/), a parte script vari, posso eseguire una ricerca multifile e ottenere lo stesso risultato, in forma assai meno elegante. E poi conoscere `wc` (quello della shell Unix!) apre un mondo, prima ancora di `grep`. Un’occhiata allo stesso [problema su Stack Overflow](https://stackoverflow.com/questions/1368563/how-can-i-use-the-unix-shell-to-count-the-number-of-times-a-letter-appears-in-a) è rivelatrice.

Dr. Drang aveva risolto un problema di [generazione seriale di numeri di appartamenti](https://leancrew.com/all-this/2019/09/making-lists-with-jot-and-seq/) (2A, 2B, 3A, 3B, 4A, 4C) con una combinazione complicata di `jot`e `seq`; grazie ai suggerimenti di un lettore è arrivato a [semplificare](https://leancrew.com/all-this/2019/09/brace-yourself-im-in-an-expansive-mood/) in

`printf "Apt. %s\n" {2..5}{A..D}`

L’istinto porta ad accendere un intero foglio di calcolo… per un problema dove basta una riga, essenziale come un diamante. Viva il minimalismo.

P.S.: l’informatica parla inglese anche perché tocca tradurre *survivalist* e l’italiano, uffa, non ci riesce in modo elegante. 
