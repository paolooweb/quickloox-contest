---
title: "Un solo computer"
date: 2019-02-21
comments: true
tags: [Snell, Six, Colors, podcast, iPad, Pro]
---
Durante un viaggio di una settimana, Jason Snell di *Six Colors* ha ritenuto di continuare la propria attività di *podcaster* e [lo ha fatto con iPad Pro](https://sixcolors.com/post/2019/02/a-week-of-podcasting-with-only-an-ipad-pro/).

Non è un fanatico, né voleva risparmiare, né voleva affermare un principio: semplicemente preferiva non portarsi dietro il Mac normalmente dedicato a questo compito.

Quando si discute della sostituibilità di Mac con iPad, è questo il punto, non quello che si fa qualsiasi cosa si possa fare, non se sia un computer, né altro: è la convenienza strettamente personale di una scelta rispetto a un’altra, magari anche solo per una settimana. A mio giudizio, comunque vada, si sta usando un computer.
