---
title: "Il numero della bestia"
date: 2021-06-07T09:10:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [Screen Time] 
---
Inizio a lavorare abbastanza tardi nella giornata. Oggi ho fatto eccezione e sono stato gratificato da una notifica di Screen Time: settimana scorsa sono stato davanti allo schermo di Mac, in media, l’undici percento in meno del tempo.

Solo qualcosa più di undici ore.

(Poi ci sono gli altri schermi, naturalmente).

Il numero della bestia, insomma. Quella da soma.

(Si scherza, eh? Anzi, se si reggono certi ritmi è proprio perché usare un Mac è assai più rilassante di tutto quello è che altro.)

(Correzione: Screen Time somma tutti gli apparecchi che condividono lo stesso ID Apple. Più intelligente di me!)

(A dire il vero, l’attenzione è talmente rivolta all’[inizio di Wwdc](https://www.apple.com/apple-events/) che non sto a guardare molto altro).

*Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile.*               