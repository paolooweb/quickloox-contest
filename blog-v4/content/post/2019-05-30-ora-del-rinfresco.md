---
title: "L’ora del rinfresco"
date: 2019-05-30
comments: true
tags: [iPod, Quake]
---
Sarebbe certo più appassionante vedere una Next Big Thing ogni anno, ma l’amara verità è che non è (più) possibile. Spazi per rivolgimenti tecnologici profondi ne sono rimasti pochi o nessuno e c’è ampia possibilità di miglioramenti incrementali, mentre per cambiamenti radicali bisogna aspettare qualcosa, che sia il *quantum computing* o la connessione diretta con il cervello  O che ne so.

È per questo che Asus, per dire, propone un [portatile dotato di due schermi](https://thenextweb.com/plugged/2019/05/28/asus-new-laptop-has-two-4k-screens-and-top-notch-specs/), dall’ergonomia assai dubbia. È come pensare di voler allevare un criceto con due teste, oltre la baracconata c’è poco.

Qualcuno se la prende perché a Cupertino, invece di estrarre un coniglio dal cilindro a ogni *keynote*, [rispolverano iPod touch](https://www.apple.com/it/ipod-touch/). Senza rendersi conto che era una buonissima idea, messa – temporaneamente, come si vede – nel cassetto perché nel momento del boom di iPhone nessuno voleva un aggeggio che non telefonasse. Ma oggi, dopo anni in cui chi voleva telefonare ha avuto tutto quello che poteva desiderare, un aggeggio che si collega in Wi-Fi, gioca alla grande e costa meno della metà di tutto è tornato interessante.

Se non fosse abbastanza chiaro, buttiamola in caciara che almeno ci si diverte: Nvidia ha dedicato tempo e risorse a [una versione di Quake II aggiornata](https://www.nvidia.com/en-us/geforce/news/quake-ii-rtx-june-6-release-date/) per giovarsi delle più moderne schede grafiche e offrire come scenari0 di gioco un autentico spettacolo, con resa tridimensionale a base di *ray-tracing* in tempo reale.

Quake II data al 1997 e a Nvidia nessuno ha pensato di creare un nuovo *first-person shooting*, visto che il loro lavoro non è fare concorrenza a [Fortnite](https://www.epicgames.com/fortnite/en-US/play-now/battle-royale). Così hanno ripreso un capolavoro di quando avevamo ancora le lire. Anche nei videogiochi, fare la prossima rivoluzione è praticamente impossibile, ove invece una buona idea ben sviluppata può portare grandi risultati.

(Anche idee di piccoli sviluppatori intelligenti. Se qualcuno volesse entrare in un piccolo giro di praticanti di [Battle for Polytopia](http://midjiwan.com/polytopia.html), basta un fischio).
