---
title: "Trasmissione di ottimismo"
date: 2017-03-17
comments: true
tags: [Panic, Adobe, Portland, San, Jose]
---
Se Panic Software ha sulla propria sede di Portland [un’insegna a colori che qualunque passante può modificare](https://macintelligence.org/posts/2016-08-13-che-cosa-ci-insegna/), Adobe ha in cima a un proprio palazzo di San Jose quattro grandi Led che trasmettono codici in attesa di essere decifrati.<!--more-->

L’ultimo, in ordine di tempo, è stato risolto da un insegnante del Tennessee: le quattro fonti di luce trasmettevano [una codifica dell’audio dello sbarco del primo uomo sulla Luna](https://blogs.adobe.com/conversations/2017/03/tennessee-teacher-cracks-adobes-semaphore-code.html).

Uno dirà che sono perdite di tempo o sprechi di denaro, o altro. Può essere: difficile difendere la trasmissione di un codice segreto ai passanti.

Al tempo stesso noto che l’insegnante ha vinto una camionata di software e supporto per la propria scuola, per Adobe è tutta pubblicità positiva a bassissimo costo e non c’è niente di male in tutto questo.

Certo, quando vedo un codice segreto mi esalto ed esagero. Al tempo stesso, c’è qualcosa di stuzzicante nella vicenda: sei per strada nella tua città, tutto è come sempre, ma – volendo – puoi acuire i sensi e trovi una piccola ma interessante sfida a portata di mano. È un invito a mantenere la curiosità, a restare aperti a una possibile sorpresa, alla fine si tratta di trasmissione di ottimismo e fiducia più che di enigmi *en plein air*.