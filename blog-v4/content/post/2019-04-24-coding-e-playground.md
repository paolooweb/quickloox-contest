---
title: "Coding e Playgrounds"
date: 2019-04-24
comments: true
tags: [Swift, Playgrounds]
---
Avevo già scritto in passato come Swift Playgrounds mi avesse lasciato una sensazione di voler ritornare dopo il primo impatto e confermo l’impressione positiva dopo qualche lezione, beh, no, partita, no, puzzle, insomma, diciamo esperienza.

La materia effettivamente viene semplificata al massimo eppure non è puerile. È richiesta attenzione ed è chiaramente possibile provare e riprovare, imparando dagli errori. Centrare l’obiettivo al primo colpo è immediato all’inizio e poi diventa elusivo in modo molto graduale. Effettivamente la programmazione in quanto tale diventa secondaria rispetto alla soluzione del problema. Lo scenario diventa sempre più libero e i problemi iniziano a presentare più di una soluzione.

Mi ritrovo a definire funzioni senza saper di averlo fatto, o meglio: se fossi un neofita, mi renderei conto di avere definito una funzione solo perché la narrazione associata al problema me lo spiega.

Swift Playgrounds non è così male: è leggero, non impegna, funziona o così almeno mi pare. Altre esperienze? Pareri? Percorsi alternativi?
