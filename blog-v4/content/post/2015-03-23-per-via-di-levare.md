---
title: "Per via di levare"
date: 2015-03-23
comments: true
tags: [Michelangelo, Flurry, watch, browser, app, Cook, FastCompany, Medium]
---
Un computer senza *browser*. È quello che sta per succedere, con watch, come nota brillantemente questo [articolo su Medium](https://medium.com/@paulcanetti/apple-watch-doesn-t-have-safari-and-you-didn-t-even-notice-a1970ebdb8ed).

Che cita una [statistica da Flurry](http://www.flurry.com/bid/109749/Apps-Solidify-Leadership-Six-Years-into-the-Mobile-Revolution#.VQ92KjA-NVN): l’86 percento del tempo passato su un apparecchio *mobile* riguarda le *app* e solo il 14 percento il *browser*. In diminuzione rispetto al 20 percento del 2013.

Chi potrebbe essere abbastanza pazzo da pensare a un futuro senza *browser*? La recente [intervista a Tim Cook su Fast Company](http://www.fastcompany.com/3042435/steves-legacy-tim-looks-ahead) suggerisce una risposta:

>Apple ha sempre avuto la disciplina per prendere l’ambiziosa decisione di andarsene. Ce ne siamo andati dai floppy disk quando erano ancora molto diffusi. Invece di diversificare come fanno tutti e minimizzare il rischio, abbiamo levato il lettore ottico, cui qualcuno era affezionato. Abbiamo cambiato il connettore per iOS, anche se a molti piaceva quello a trenta piedini. Alcune di queste decisioni per un po’ sono state poco popolari. Ma bisogna avere il coraggio di perdere di vista la costa e prendere il mare aperto. Continuiamo a farlo.

Michelangelo affermava che la scultura si fa [per via di levare](http://www.iisforlimpopoli.it/artusij/area-studenti/doc_view/398-11-michelangelo-buonarroti1).