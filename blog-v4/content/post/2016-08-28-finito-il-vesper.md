---
title: "Finito il Vesper"
date: 2016-08-28
comments: true
tags: [Vesper, Gruber]
---
A suo tempo [avevo raccomandato](https://macintelligence.org/posts/2013-06-25-a-scuola-di-vesper/) l’acquisto di [Vesper](https://itunes.apple.com/it/app/vesper/id655895325?l=en&mt=8), ma ora devo convenire con gli autori che le Note di Apple da El Capitan sono andate oltre i requisiti della *app*. La quale ha [cessato lo sviluppo](http://daringfireball.net/2016/08/vesper_adieu) e, per quanti fossero curiosi di vedere che cosa si erano persi, ora è gratuita.<!--more-->

L’analisi di John Gruber è sostanzialmente corretta: App Store è un luogo dove è difficilissimo fare prosperare una *app* e farne prosperare anche gli autori.

Fa pensare che Vesper, con dietro sviluppatori proprio del calibro di Gruber e superiore, dopo uno sviluppo curato nei minimi particolari, abbia gettato la spugna. E comunque si vede quanto sia difficile nel regno digitale guadagnarsi da vivere con un lavoro digitale ben fatto, libro, *app*, film, musica, qualsiasi cosa.