---
title: "Velocità sessantaquattro"
date: 2017-06-04
comments: true
tags: [iOS, 64, bit]
---
Leggo da *9to5mac* che alla vigilia di [Wwdc](https://developer.apple.com/wwdc/) è stato compiuto un altro passo decisivo verso un ambiente iOS totalmente a 64 bit: l’[esclusione dai risultati di ricerca delle *app* a 32 bit](https://9to5mac.com/2017/06/04/32-bit-apps-ios-11/).<!--more-->

La sostanza dei cambiamenti dovuti a questa misura, anticipata da tempo, è pressoché incomprensibile per l’utente comune. Sotto il cofano, in realtà, i vantaggi sono numerosi in termini di prestazioni, sicurezza, longevità eccetera.

Qualunque cosa esca dal simbolico cilindro del palco di Wwdc, soddisfi oppure meno, c’è un sottotesto di innovazione e superiorità tecnologica già stabilito da adesso. Pensare a un mondo Android uniformato sui 64 bit, in questo momento, è pura fantascienza.
