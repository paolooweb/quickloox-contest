---
title: "Esseri inutili"
date: 2011-11-27
comments: true
tags: [Script, iOS]
---
Parto dal fatto che &#8211; dati del secondo trimestre 2011 &#8211; <a href="http://www.asymco.com/2011/11/17/the-global-smartphone-market-landscape/">per ogni iPhone si sono venduti circa quattro *smartphone* Android</a>. Per ogni iPhone in attivit&#224;, potrebbero esserci in attivit&#224; sei o sette apparecchi Android, forse anche di pi&#249;.<!--more-->

Su Internet tuttavia &#8211; dati di ottobre, NetApplications &#8211; <a href="http://www.netmarketshare.com/operating-system-market-share.aspx?qprid=9&amp;qpcustomb=1">per ogni pagina visitata da un Android ce ne sono tre visitate da un apparecchio iOS</a>. Qui entrano in gioco anche iPod touch e iPad, certo, ma il rapporto numerico resta sbilanciato a favore di Android.

Secondo l&#8217;analista Gene Munster, <a href="http://tech.fortune.cnn.com/2011/11/21/piper-jaffray-android-app-revenue-is-7-of-iphones/">da App Store si scaricano tre volte le applicazioni che si scaricano per Android sul suo Marketshare.</a> App Store ha venduto applicazioni per quasi cinque miliardi di dollari nel 2011, Android quasi 342 milioni, all&#8217;incirca un rapporto di quattordici dollari su iOS per ogni dollaro su Android. Gli analisti sono terribili nelle previsioni, ma per riepilogare il passato funzionano.

Adesso leggo la notizia di un <a href="http://www-03.ibm.com/press/us/en/pressrelease/36100.wss">rapporto di Ibm</a> su come &#232; andato il Venerd&#236; Nero in America. Il *Black Friday* &#232; il giorno che segue la festa del Ringraziamento (quest&#8217;anno gioved&#236; 24 novembre) e per tradizione vede un diluvio di offerte speciali su prodotti di ogni tipo.

Gli apparecchi *mobile* pi&#249; usati per acquistare *online*, primo e secondo posto, sono stati iPhone e iPad. Android terzo, 4,1 percento contro 5,4 percento di iPhone e 4,8 percento di iPad. Ricordo, gli apparecchi Android in giro sono in superiorit&#224; numerica nettissima.

La gente che fa acquisti online via connessione *mobile* ha acquistato il 2,8 percento delle volte che &#232; entrata in un sito di *shopping*. Chi lo ha fatto con iPad invece ha acquistato il 4,6 percento delle volte. Tradotto: iPad &#232; molto pi&#249; efficiente per fare acquisti in rete.

La prossima volta che un amico mi parla del suo prossimo acquisto Android, gli far&#242; notare che costa probabilmente meno, ma poi non ci fa niente. O ci fa molto meno. Avere uno *smartphone* Android o non averlo, nella media, fa ben poca differenza.