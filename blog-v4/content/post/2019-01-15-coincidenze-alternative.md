---
title: "Coincidenze alternative"
date: 2019-01-15
comments: true
tags: [Google, iPad, Speirs, Pixelbook, GSuite]
---
Rileggevo Fraser Speirs che parla del suo [passaggio da iPad e MacBook a un Pixelbook](http://www.speirs.org/blog/2018/11/30/on-switching-from-an-ipad-pro-and-a-macbook-to-a-pixelbook), sulla base di quanto è divenuto importante per la sua attività il software Google e quanto iOS manchi di usabilità e completezza quando si voglia usarlo “come un computer”.

Prima di rileggerlo ho passato una giornata di intenso lavoro su Google Sheets sul mio iPad Pro, collegato a una tastiera Bluetooth fisica. La quantità di cose che Google Sheets fa su iPad e NON fa tramite una tastiera collegata a iPad è ingente.

Lo stesso Speirs riconosce che *le applicazioni della GSuite di Google non sono molto buone*. E scrive *sono dispostissimo a credere che non sia colpa di Apple*.

Lo credo anch’io, dal momento che vedo su iPad programmi perfettamente dotati di comandi per tastiera fisica, a decine. Il produttore di computer alternativi a iPad fa funzionare male il proprio software su iPad. Che coincidenza.

Lui passa a Pixelbook, dice, perché se dovesse scegliere tra rinunciare a GSuite e rinunciare a iPad sceglierebbe la seconda strada. Io non lo so. GSuite è difficile da sostituire con piena soddisfazione, ma ci si riesce anche se dovendo cumulare più diverse alternative. Ci sono molto alternative; parziali, certo, però molte.

Mentre sostituire un iPad significa mettersi nello zainetto un coso Android o un Surface. Altre alternative non ce ne sono.