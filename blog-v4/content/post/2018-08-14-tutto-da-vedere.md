---
title: "Tutto da vedere"
date: 2018-08-14
comments: true
tags: [Polytopia, FreeCiv, Clash, Clans, Supercell]
---
Già detto: diverse attività umane sono gradevolissime da soli, ma in compagnia moltiplicano il loro valore ed ecco perché, pur se resto tifoso di [FreeCiv](http://freeciv.org/), mi sono fatto coinvolgere, agosto galeotto, in [The Battle of Polytopia](https://itunes.apple.com/it/app/the-battle-of-polytopia/id1006393168?l=en&mt=8).

Come strategico a turni a tema confronto/scontro di civiltà, ha il merito di essere molto veloce da capire e giocare, senza però banalizzarsi e perdere la complessità che è il sale di questi giochi.

C’è ancora da fare invece in tema di accessibilità: nel gruppo di gioco abbiamo un amico fortemente ipovedente e il testo in Battle of Polytopia è ai limiti della leggibilità anche per me che mantengo per ora una vista piuttosto buona.

Ho lasciato feedback perché la questione è importante e il rimedio è relativamente semplice in termini di sviluppo. Capisco che uno studio indipendente svedese non abbia le risorse di [Supercell](http://supercell.com/en/); tuttavia l’accessibilità non ha scusanti. Quello che si può leggere in rete, o su un iPad, deve essere fruibile a chiunque. L’accessibilità di sistema in iOS fa mille cose, ma non tutte e comunque la sua presenza è una pessima scusa per trascurare il lavoro da svolgere in prima battuta.

Ciò detto, The Battle of Polytopia si fa apprezzare e chi volesse può trovarmi sotto l’identificativo Zf37pI6KgzGLOIL9.
