---
title: "Val più la pratica dell'interfaccia grafica"
date: 2013-07-29
comments: true
tags: [Terminale, Unix, Mac]
---
Si [diceva](https://macintelligence.org/posts/2013-07-26-non-e-un-paese-per-vecchi-filesystem) che gli utenti evoluti, se sono evoluti davvero, usano il Terminale più che il Finder e [commentavo](https://moot.it/quickloox#!/blog/blog/2013/07/26) con l’esempio di un compito che assolvo in modo semiautomatico ogni mese.<!--more-->

Ecco il comando che mi genera in un colpo solo undici tra cartelle e sottocartelle:

`mkdir -p numero\ 13/1\ originali\ e\ traduzioni/presi numero\ 13/2\ testi\ per\ i\ grafici/presi numero\ 13/3\ bozze\ impaginati/presi numero\ 13/4\ impaginati\ corretti/presi numero\ 13/5\ impaginati\ finali/presi`

Lo si può copiare e incollare sul Terminale di qualsiasi Mac e dare *Invio*. Non succede niente di grave, si creano cartelle e basta, che si possono buttare liberamente un secondo dopo. Per trovarle, il comando `pwd` mostrerà dove il computer sta effettivamente lavorando. Potrebbe essere la cartella Inizio (*/Users/nomeutente/*) o altra. Prima di eseguire il comando si può trasferire eventualmente l’esecuzione in una cartella specifica, con il comando `cd`. Dopo il *backslash* c’è sempre uno spazio; dopo la scritta *presi* c’è sempre uno spazio (a parte la fine del comando); non c’è mai più di uno spazio.

Da qui in avanti, è un copia e incolla. Ovvero, è una cosa mostruosamente complicata da portare a termine la prima volta. Poi è fatta. Ho lavorato in un *worksheet* di [BBEdit](http://barebones.com/products/bbedit/), che ho salvato. Da allora torno lì tutte le volte che mi serve, schiaccio *⌘-Invio* ed ecco pronta la gerarchia. In mancanza di BBEdit, basta salvarsi il comando in [TextWrangler](http://barebones.com/products/textwrangler/), per dire, e fare copia e incolla nel Terminale.

La prima volta, creare le undici cartelle a mano batte agevolmente in velocità il Terminale. La seconda volta va diversamente, perché cambio *numero 13* in *numero 14* in mezzo secondo e poi è sufficiente *⌘-Invio* per finire. La terza, la quarta, la quinta, la tredicesima…

Per chi ne sa, è ovvio come questo sia solo l’inizio. Si può sostituire tutta la sequenza con un singolo comando *cartelle*, farne uno *script* eccetera. Lo scopo dell’esercizio non era addentrarsi nei meandri di Unix (come peraltro ha fatto da par suo sempre nei commenti **briand06**), ma mostrare la via realmente più potente per raggiungere un risultato su Mac, dopo che tutti conosciamo – risolvendo novanta volte su cento ogni problema – la via più facile.