---
title: "Doppio is peggio che quadruplo"
date: 2018-12-05
comments: true
tags: [Foinnex, iPad, Vga, Hdmi]
---
L’ho già scritto, è un autunno di viaggi di lavoro e con il nuovo iPad Pro avevo bisogno, anche urgente, di connettori video Vga e Hdmi.

Continua a sfuggirmi il motivo per cui organizzazioni di ogni genere si rifiutino di collegare i videoproiettori e le Lim a un qualunque succedaneo di Apple TV: arrivi in aula, entri nel Wi-Fi, sei collegato. Invece no, ci vuole il cavo, e l’adattatore, e le canaline, e le multiprese, e se devono attaccarsi due macchine parte un balletto, staccalo di qui, attaccalo di là eccetera. Tutto questo sembra, apparentemente, più economico. Come al solito, ponendo a valore zero il tempo di chi deve domare la connessione.

Sono rimasto soddisfatto da un [accrocchio Foinnex](https://www.amazon.it/dp/B071KKNPP2/) che per trentanove euro e novantanove mi ha fatto arrivare in un giorno lo scatolino che entra in iPad Pro via Usb-C ed esce con passante per l’alimentazione, Vga, Hdmi e già che c’erano anche due prese Usb.

Lo scatolino è plasticaccia, ma i connettori sembrano di buona qualità e – almeno per Vga e Hdmi, quelli che ho provato – funzionano egregiamente.

Volevo anche uno scatolino unico per ragioni eminentemente pratiche. Durante l’ultima trasferta avevo con me iPad Pro, per scrivere, consultare i documenti di riferimento, lavorare; il vecchio iPad, per proiettare durante il lavoro; iPhone, come ovvio; e watch.

Sembrano quattro cose e lo sono. Poi però significa quattro cavi di alimentazione, con quattro alimentatori, con tre connettori video. Quando sei in aula a lavorare con le persone e proiettare devi avere almeno la ridondanza minima, il set completo di accessori per ciascuna macchina. Se ti destreggi bene riesci a sistemare quattro macchine con due cavi e due alimentatori (tre cavi in realtà perché quello di watch è a sé e ineliminabile)… ma se appena si guasta un cavo, o un alimentatore, sei nei guai.

Per il vecchio iPad e per iPhone ho avuto storicamente solo connettori Vga e sono sopravvissuto. Ma qualcosa sta cambiando e Hdmi è un passepartout ovunque ci sia un minimo di hardware moderno. Oltretutto, fa viaggare anche l’audio.

Però non avevo voglia di due connettori per un iPad; doveva essere un accessorio solo. Questa è la ragione fondamentale per cui non ho considerato gli originali Apple e non il prezzo. A comprare Apple avrei speso il quadruplo, ma avrei avuto il doppio degli accessori cui badare in viaggio; il vero fastidio è questo, i prezzi si ammortizzano.