---
title: "I conti non tornano"
date: 2013-08-03
comments: true
tags: [iPad, Android]
---
Daniel Eran Dilger scrive su *Apple Insider* un [chilometrico articolo](http://appleinsider.com/articles/13/07/27/samsung-has-not-dethroned-apple-in-mobile-profits) ove analizza l’affermazione di Strategy Analytics che Samsung abbia superato Apple nei profitti della vendita di computer da tasca e spiega come sia basata su dati falsati.<!--more-->

Poi ne scrive [un altro](http://appleinsider.com/articles/13/07/31/strategy-analytics-finds-millions-of-android-tablets-rewrites-ipad-history) per mostrare come Strategy Analytics, nel fare i conti sul mercato delle tavolette, si inventi dal nulla più di dieci milioni di apparecchi Android.

Sarà un caso che gli uffici coreani di Samsung e Strategy Analytics [si trovino nello stesso palazzo](http://tech.fortune.cnn.com/2013/08/02/apple-samsung-strategy-analytics/)?

Nel frattempo, per realizzare una *app* come iPlayer della Bbc, servono [tre volte i programmatori](http://www.bbc.co.uk/blogs/internet/posts/Video-on-Android-Devices-Update) necessari per la versione iOS.

Dove troppo e dove niente, insomma.

Per la cronaca, quando si tracciano le statistiche di traffico Internet, [aggeggi come questi](http://www.alibaba.com/trade/search?fsb=y&IndexArea=product_en&CatId=&SearchText=android+tv+stick) figurano come apparecchi Android alla stregua di un *tablet* o di uno *smartphone*.

(Da domani basta cose serie per un po’: pigrizia agostana, giochini, amenità e se riesco un pizzico di *scripting*).