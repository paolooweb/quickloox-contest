---
title: "Natale a pacco zero"
date: 2022-12-27T20:31:08+01:00
draft: false
toc: false
comments: true
categories: [Internet]
tags: [Natale]
---
Senza volerlo, questo Natale ho ricevuto (con una eccezione) solo regali digitali.

Un antefatto per capire il senso della cosa: è tradizione nella famiglia allargata che ciascuno scriva la propria lettera a babbo Natale e indichi le preferenze per i regali. Così i parenti sanno che cosa prendere e tutta la parte stressante della festa relativamente a questo è risolta.

Dal punto di vista del mittente, è sufficiente compilare un elenco lungo a sufficienza per mantenere il senso della sorpresa: sai che con ottima probabilità arriverà qualcosa presente nell’elenco, ma non sai *esattamente* che cosa e la suspence resta viva fino al momento di aprire, materialmente o simbolicamente, i pacchi.

Avendo un ventaglio di parenti vario per età e propensioni, cerco di scrivere una lettera dove si trovi qualcosa di reperibile senza troppa fatica, quindi eterogenea, con oggetti digitali certo, ma anche materiali. A volte riesce meglio e a volte peggio, ma la sostanza è che – a partire da questi vincoli – le scelte sono cadute in maggioranza sull’immateriale.

Non è detto che succeda ancora o che succederà di più; certamente è diventata una forma di regalo molto più accettata e praticata che in passato, dove avere un link accanto a una descrizione di regalo veniva visto magari come una stranezza.