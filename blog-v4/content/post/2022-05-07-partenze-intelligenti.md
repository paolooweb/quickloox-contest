---
title: "Partenze intelligenti"
date: 2022-05-07T01:44:47+01:00
draft: false
toc: false
comments: true
categories: [Internet]
tags: [Apple, Goodfellow, Ian Goodfellow, Schiffer, Zoë Schiffer, The Verge, MacRumors]
---
La notizia è di quelle da prendere con pinze molto lunghe, perché arriva da *The Verge* ed è firmata da Zoë Schiffer, che ha avuto rapporti professionali con Apple di natura piuttosto agitata.

Se però è vero che Ian Goodfellow, da quattro anni direttore del programma di machine learning di Cupertino, ha deciso di [lasciare l’azienda *anche* per via del programma di ritorno in ufficio post-pandemia](https://www.macrumors.com/2022/05/07/apple-director-of-machine-learning-resigns/), si tratta di un campanello di allarme interessante.

Goodfellow non è una seconda linea e in Google, da dove è arrivato in Apple, era uno dei dipendenti di rango più elevato. Avrebbe annunciato le proprie dimissioni con una email in cui si leggerebbe:

>Reputo con forza che la migliore policy per il mio team sarebbe stata più flessibile.

Attualmente ai dipendenti Apple vengono richiesti almeno due giorni a settimana in ufficio, che diventano tre dal 23 maggio.

Va detto che, per una azienda strutturata come Apple, permettere una presenza parziale in ufficio può sembrare sfoggio di rigidità, ma è invece una posizione molto morbida. Questo in termini relativi: in assoluto, convivere con il virus dovrebbe averci fatto capire l’importanza del vero lavoro *smart*, che non è stare a casa o stare in ufficio, ma stare dove è meglio. Obiettivo che è difficile centrare pensando di andare a sedersi alla scrivania a giorni fissi.

Se Goodfellow pensa che l’unico soggetto che dovrebbe presenziare fisso a Cupertino siano gli algoritmi, se ne può discutere e si può dissentire. Ignorarlo, però, sarebbe miope.