---
title: "Contanti… saluti"
date: 2017-08-11
comments: true
tags: [Sasha, Bancomat]
---
Questo incontro ravvicinato con l’ennesimo Bancomat preda della sindrome di Windows arriva grazie alla cortesia di **Sasha**, che aveva pronto persino il titolo: occasione perfetta per i miei ozi agostani.<!--more-->

Grazie e viva [Pay](https://www.apple.com/it/apple-pay/).

 ![Bancomat Windows malfunzionante](/images/bancomat-sasha.jpg  "Bancomat Windows malfunzionante") 