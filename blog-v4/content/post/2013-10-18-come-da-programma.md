---
title: "Come da programma"
date: 2013-10-18
comments: true
tags: [iOS, OSX]
---
Scrivo e sostengo da tempo che chi produce contenuti nel Ventunesimo secolo ha il dovere di avvicinarsi alla padronanza dei mezzi di produzione e di conseguenza, nel mondo digitale, alla programmazione piccola o grande che sia.<!--more-->

Ho visibilità principalmente sul testo, dove da Word bisogna spostarsi verso Html, verso LaTeX, verso Markdown, verso il *text processing* contrapposto al *word processing*, BBEdit in luogo di  Pages. Ho pochi dubbi che valga con le dovute distinzioni anche per il suono, il video, l’immagine. Un amico lavora da anni a un grosso libro, di quelli che si pubblicano per davvero e forse sono l’inizio invece di essere la fine. Tra i suoi strumenti di lavoro c’è FileMaker Pro, un database.

Arrivo così a Mike Swanson di [Juicy Bits](http://www.juicybitssoftware.com), che si pone il problema di [disegnare le icone](http://blog.mikeswanson.com/post/62341902567/unleashing-genetic-algorithms-on-the-ios-7-icon) con gli angoli arrotondati di iOS 7 esattamente come sono quelle create da Apple. Gli angoli arrotondati di iOS 7 [sono diversi](http://blog.mikeswanson.com/post/61651302736/photoshop-script-for-ios-7-rounded-rectangles) da quelli di iOS 6.

Per risolvere il problema di disegnare le icone ricorre… ad AppleScript. Solo che la soluzione non è perfetta. Allora ricorre a tecniche di *selezione genetica degli algoritmi* e sostanzialmente lascia un programma a girare e isolare man mano i calcoli che portano alla soluzione migliore possibile.

Alla fine arriva a [una soluzione](http://blog.mikeswanson.com/iosroundedrect), liberamente scaricabile.

Per disegnare icone serve un grafico. È che il grafico, se almeno sa che cosa sia AppleScript, nel mondo di oggi ha una marcia in più a disposizione.