---
title: "Punto di non ritorno"
date: 2022-05-18T01:38:19+01:00
draft: false
toc: false
comments: true
categories: [Internet]
tags: [Apple, Covid, smart working, Bloomberg, PowerPC, Reno, Steve Jobs, Jobs, Apple Store, Think Different]
---
E così il 23 maggio [non ci sarà il rientro generale per tre giorni a settimana negli uffici di Apple](https://www.bloomberg.com/news/articles/2022-05-17/apple-delays-plan-to-have-workers-in-office-three-days-a-week).

Delle [avvisaglie della situazione](https://macintelligence.org/posts/2022-05-07-partenze-intelligenti/) si è parlato. Se vogliamo parlarne per luoghi comuni, il genio non rientra nella lampada, niente sarà più come prima, la frittata è fatta; se preferiamo fare un discorso vagamente intelligente, il lavoro in ufficio è come il libro su carta: ci accompagna da secoli e ha numerosi vantaggi, ma non è più e non potrà mai più essere l’unico modo possibile di lavorare (leggere). Perché ha anche svantaggi e perché finora ha dominato in quanto nessuno aveva tirato fuori alternative migliori.

Il virus ha costretto pesantemente a inventarsi alternative qualsiasi, ognuna meglio del niente, e diverse nozioni interessanti che erano consapevolezza di pochi sono divenute di dominio pubblico: la tecnologia ha reso obsoleta la dipendenza del lavoro dal luogo in cui si svolge, è stupido obbligare al lavoro da casa come è stupido obbligare al lavoro in ufficio, dipendenti la cui produttività dipende dallo sguardo onnipresente del padrone sono una pessima forza lavoro in carico a una pessima azienda.

Il discorso del contatto umano, della creatività che nasce dall’incontro occasionale, sono discorsi più lunghi e complessi, ma al tempo stesso ricordano la necessità per alcuni di sentire l’odore della carta o il fruscio delle pagine. La soluzione sta nel ridisegnare la nozione stessa di lavoro e già che ci siamo quella di socializzazione; tutto il resto è mancanza di idee e di creatività, che pure dopo decenni di lavoro in ufficio dovrebbe essersi sviluppata alla grande, o altrimenti si sta mescolando molto falso a un po’ di vero.

Sono reduce dall’incontro di classe con le maestre della materna della secondogenita, che per la prima volta negli anni venti del Duemila si è tenuto in presenza, nel giardino della scuola. Una mamma – nemmeno particolarmente brillante – si è così espressa:

>Vedersi di persona è bello per via del contatto umano, ma per un’ora di incontro ti tocca gestire due ore di permessi.

Ecco, le aziende potrebbero quantomeno cogliere la questione del lavoro ottimale a livello da assemblea della scuola materna.

Il contatto umano? La creatività? Ricordo [come si arrivò](https://lowendmac.com/2014/ibm-apple-risc-and-the-roots-of-the-powerpc/) a una delle tappe dello sviluppo della piattaforma PowerPC:

>Durante una vacanza di sci Reno, Nevada, un gruppo di ingegneri Apple capitanato da Jack McHenry decise di avviare un progetto per rendere la macchina Jaguar compatibile con il software precedente.

La soluzione non è, ovviamente, pagare la settimana bianca ai gruppi di lavoro, ma capire che quando la creatività nasce in ufficio è perché non è prevista alcuna altra possibilità.

Una azienda capace di reinventarsi e consapevole di dover essere la prima a cannibalizzare i propri stessi prodotti prima che lo faccia qualcun altro, dovrebbe essere in prima fila al tavolo di lavoro per reingegnerizzare il modo stesso di lavorare non *in*, ma *per* l’azienda. La stessa azienda che [ha reingegnerizzato le scale di cristallo dei propri negozi](https://macintelligence.org/posts/2019-05-17-strip-different/), [ha reingegnerizzato il concetto di quartier generale](https://macintelligence.org/posts/2017-02-12-sta-nei-dettagli/), fondata da un uomo che [ha passato otto anni](https://www.cultofmac.com/125861/steve-jobss-quest-for-perfection-could-make-even-buying-a-sofa-into-a-decade-long-ordeal/) (di riflessione sul design) prima di convincersi a introdurre un divano in casa. L’azienda che [ha celebrato i pioli tondi nei fori quadrati](https://macintelligence.org/posts/2020-12-03-dedicato-ai-folli/), i ribelli che sfidano lo *status quo*.

Apple, il mondo è attualmente incapace di capire come lavorare senza dare per scontato l’ufficio. Hai cambiato almeno tre volte la vita di metà del mondo rispetto all’uso della tecnologia. Oggi è tempo di cambiare la struttura del lavoro e nessuna organizzazione al mondo può farlo meglio di te. Mettiti e mettici alla prova. Perché indietro non si può tornare.