---
title: "Note di configurazione"
date: 2017-03-04
comments: true
tags: [iPhone, Coordinator, Orchestra]
---
Dopo un numero di cadute e maltrattamenti effettivamente eccessivo, con un principio di piegatura della scocca, danni alla protezione esterna della fotocamera e una crepa a tutta larghezza dello schermo touch, iPhone 5 ha segnalato che era il momento di prendere provvedimenti: lo schermo touch si è staccato dal corpo durante una telefonata, mettendosi praticamente a penzolare.

L’unità ha lavorato per 1.548 giorni, quattro anni più quasi tre mesi, direi non male per un iPhone, specie così malridotto. Pagato 729 euro a dicembre 2012, è costato 47 centesimi al giorno. A Milano è la metà di un caffè.

Non aggiorno perché un iPhone 5 continua a essere esattamente quello che mi serve, cioè un terminale di connessione usabile senza remore con una sola mano. Ho scelto invece la riparazione-sostituzione, con un iPhone 5 rigenerato in cambio del modello rotto e 289 euro.

Per tenere la media di costo del vecchio iPhone, deve durare almeno una buona parte di due anni. È plausibile.

L’attivazione del nuovo apparecchio è stata indolore ma abbastanza lunga. L’ecosistema è diventato complesso: ripristini il backup del vecchio apparecchio ma poi devi riaccoppiare watch, per esempio. iCloud chiede varie volte di inserire la password e in generale bisogna prestare un po’ di attenzione.

Vedo come prossimo passo evolutivo dell’ecosistema una funzione che potrei chiamare Apple Setup oppure, che so, Coordinator, Orchestra: una app che condensa in modo semplice la configurazione e la gestione degli apparecchi Apple di proprietà, da Mac a watch e tv e, sulla base degli elementi di configurazione noti, permette con un pulsante o poco più di avvicendare un iPhone vecchio con un iPhone nuovo, aggiungere un apparecchio, levarne uno, aggiornarlo eccetera. Il tutto da *qualsiasi* macchina del parco personale.

Concettualmente, i dati di configurazione delle varie macchine stanno su iCloud e il sistema presuppone che l’apparecchio all’opera e quello su cui si opera stiano nella stessa rete Wi-Fi. Idealmente è più che fattibile; occorre una grande quantità di lavoro a monte per uniformare il più possibile i singoli processi di configurazione di ciascun tipo di apparecchio. Grande lavoro, ma è esattamente quello che una Apple potrebbe ambire a svolgere nell’ignavia generale della concorrenza.
