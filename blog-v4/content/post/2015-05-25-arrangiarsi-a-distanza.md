---
title: "Arrangiarsi a distanza"
date: 2015-05-25
comments: true
tags: [Terminale, Mac, Chrome]
---
Questo fine settimana ho collaudato con successo [Chrome Remote Desktop](https://support.google.com/chrome/answer/1649523?hl=it), ennesimo strumento per controllare un Mac da un iPad distante, nell’occasione, mezzo migliaio di chilometri.<!--more-->

Pieno successo dell’esperimento, ma questo non significa che Chrome Remote Desktop funzioni pienamente come uno vorrebbe. Più provo strumenti come questo, più constato che presso i progettisti continuano a mancare casi d’uso. Perché certe situazioni semplicemente sono state trascurate. O riceveranno supporto in futuro, importa poco.

Si supponga di operare al Terminale del Mac, appunto, da iPad. Chrome Remote Desktop mette a disposizione una tastiera del tutto standard, che però in quanto tale non ha il tabulatore (essenziale per il completamento automatico dei comandi nel Terminale). Si risolve chiamando su Mac il Visore Tastiera e digitarlo, a distanza, lì.

Altra supposizione: voglio salvare un file con Comando-S impartito dentro l’applicazione. La tastiera di Chrome Remote Desktop non lo consente… e neanche il Visore Tastiera se azionato in questo modo. L’unico modo è toccare il menu File e poi il comando Salva.

l’inghippo migliore l’ho incontrato al momento di inserire una password contenente un punto esclamativo. Chrome Remote Desktop dispone unicamente, a oggi, di una tastiera americana. Se il Mac controllato da lontano ha una tastiera italiana, auguri. La soluzione consiste nell’impostare la tastiera americana sul Mac, per poi tornare a quella italiana più tardi.

Trucchi di tastiera, poi il Visore Tastiera, poi la dipendenza dalla barra dei menu: Chrome Remote Desktop è efficientissimo su alcune cose (per esempio lo zoom con due dita, o il passaggio da schermo orizzontale a verticale). Rimane il fatto che ha attualmente limitazioni come quelle descritte e piacerebbe lasciare nel cassetto l’arte di arrangiarsi invece che doverla riscoprire ogni due comandi.