---
title: "Ok, il prezzo è obbligato"
date: 2021-08-20T10:40:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [Office, Microsoft, Pandoc, Markdown, Etherpad] 
---
Bella coincidenza che io scriva di [EtherPad](https://macintelligence.org/posts/Collaborare-come-mai-prima.html) come mezzo per collaborare online sul testo, Html come standard nelle scuole, ipertesto organizzato per essere imparato fino alla terza media e, nel contempo, [Microsoft alzi i prezzi di Office 365 per le aziende](https://www.microsoft.com/en-us/microsoft-365/blog/2021/08/19/new-pricing-for-microsoft-365/).

Uno dice, beh, Microsoft è libera di fare ciò che vuole e i clienti pure.

Probabile che sia così. Non ho il dato sottomano, ma l’ho letto l’altroieri: la quota di mercato di Office online sarebbe dell’ottantasette percento. Quasi tutto il resto è di Google, quasi tutto di utenze non paganti. Di libertà ne vedo poca. [Semplifica](https://twitter.com/disinformatico/status/1428513478789242881) Paolo Attivissimo:

<blockquote class="twitter-tweet"><p lang="it" dir="ltr">&quot;Ho cambiato il nostro accordo. Prega che non lo cambi ancora&quot;.<br><br>Se usate i formati Microsoft, sarete costretti a pagare qualunque cifra decida Microsoft per accedere ai VOSTRI documenti. Per sempre.<br><br>Con i formati aperti, invece, non c’è un daziere eterno. <a href="https://t.co/wKCG7Ijexz">https://t.co/wKCG7Ijexz</a></p>&mdash; Paolo Attivissimo (@disinformatico) <a href="https://twitter.com/disinformatico/status/1428513478789242881?ref_src=twsrc%5Etfw">August 20, 2021</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

[Si esprime così](https://twitter.com/stevesi/status/1428434154090205188) Steven Sinofsky, che di Microsoft se ne è inteso parecchio, dall’interno e molto in alto:

<blockquote class="twitter-tweet"><p lang="en" dir="ltr">Microsoft will raise Office 365 business subscription prices in 2022 // putting this in perspective relative to “boxed” software. A consumer might have paid $150 for Office once every 4 yrs. A biz paying $150/yr now pays $450/yr (and can’t stop). <a href="https://twitter.com/hashtag/saas?src=hash&amp;ref_src=twsrc%5Etfw">#saas</a> <a href="https://t.co/FEuvR0EQ9P">https://t.co/FEuvR0EQ9P</a></p>&mdash; Steven Sinofsky (@stevesi) <a href="https://twitter.com/stevesi/status/1428434154090205188?ref_src=twsrc%5Etfw">August 19, 2021</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

>[Rispetto al software in scatola di una volta, ora che lo si vende in cloud] un utente privato avrebbe potuto spendere per Office centocinquanta dollari ogni quattro anni. Un’azienda che pagasse centocinquanta dollari l’anno ora ne paga quattrocentocinquanta (e non può fermarsi).

Non capisco veramente la ragione per cui un’azienda sottostia a questa situazione senza il coraggio di voltare pagina e prendere in mano seriamente i propri dati. Certo, c’è l’ignoranza. Certo, l’ottusità. Certo, l’interesse privato di qualcuno. Ma non possono essere (quasi) *tutte* così.

*Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile.*