---
title: "Una immagine di Store"
date: 2014-05-24
comments: true
tags: [Mac, Store, Pixelmator]
---
Per quanto Mac App Store offra numerosi programmi interessanti, è difficile individuare una preziosità specifica dello Store di per sé. Ovvero, è una comodità ma non cambia la vita come ha fatto su iOS. Credo che la maggioranza dei programmi vitali usati da chiunque non arrivi necessariamente da Mac App Store.<!--more-->

Con una eccezione: Pixelmator. La *app* è cresciuta assieme a Mac App Store, registrando successi di vendita consistenti grazie a esso, capace di valorizzarsi attraverso lo Store invece che adattarsi o subirlo.

Da poco è comparso un [aggiornamento significativo](https://itunes.apple.com/it/app/pixelmator/id407963104?l=en&mt=12) del programma che continua ad avere un prezzo smodatamente conveniente per quello che offre e sempre più si propone come buona alternativa ai confini – talvolta dentro i confini – dell’utilizzo professionale.

Quando Mac App Store sarà veramente importante, sarà pieno di programmi come Pixelmator e avrà tutt’altra immagine.