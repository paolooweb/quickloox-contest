---
title: "Non vedo l’ora"
date: 2022-05-01T03:04:17+01:00
draft: false
toc: false
comments: true
categories: [Software, Hardware]
tags: [Apple Watch, TermiWatch, Terminale]
---
Qualcuno è riuscito a installarsi il quadrante per Watch in stile Terminale,[TermiWatch](https://github.com/kuglee/TermiWatch)? Il procedimento sembra a prova di bomba ma a un certo punto Xcode sputa un errore che va oltre il livello di comprensione qui disponibile.
