---
title: "Il tempo per programmare"
date: 2019-12-09
comments: true
tags: [Cook, Omotesando, Apple, coding]
---
Il fatto stesso che *9to5Mac* titoli [Tim Cook è in Giappone a incontrare sviluppatori di app di età tra i tredici e gli ottantaquattro anni](https://9to5mac.com/2019/12/09/tim-cook-is-in-japan/) dovrebbe farci (ri)pensare al ruolo della programmazione come fatto della vita e elemento di cultura generale, più che di specializzazione o strettamente professione.

[Masako san e Hikari san](https://twitter.com/tim_cook/status/1203584102441476097) sono eccezioni, sicuro. Per quanto ancora?

<blockquote class="twitter-tweet"><p lang="en" dir="ltr">What a treat to reunite with Masako san and Hikari san, some of our imaginative developers who prove that no matter your age, coding opens up new opportunities to follow your dreams! Wonderful to see you at Apple Omotesando! <a href="https://t.co/YAr4M6jSXw">pic.twitter.com/YAr4M6jSXw</a></p>&mdash; Tim Cook (@tim_cook) <a href="https://twitter.com/tim_cook/status/1203584102441476097?ref_src=twsrc%5Etfw">December 8, 2019</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

Interessante anche la visita di Cook a [una scuola primaria](https://twitter.com/tim_cook/status/1204006288155275264), dove evidentemente fanno qualcosa meglio che [insegnare a usare Office](http://www.macintelligence.org/blog/2019/11/29/tre-cose-da-imparare/).

<blockquote class="twitter-tweet"><p lang="en" dir="ltr">Coding is one of the most important skills you can learn. Loved celebrating Computer Science Education Week with students from Rikkyo Primary School at Apple Marunouchi! 🇯🇵 <a href="https://twitter.com/hashtag/TodayatApple?src=hash&amp;ref_src=twsrc%5Etfw">#TodayatApple</a> <a href="https://t.co/lBv78PRGxk">pic.twitter.com/lBv78PRGxk</a></p>&mdash; Tim Cook (@tim_cook) <a href="https://twitter.com/tim_cook/status/1204006288155275264?ref_src=twsrc%5Etfw">December 9, 2019</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>