---
title: "Eva, né scienza"
date: 2022-05-06T01:03:19+01:00
draft: false
toc: false
comments: true
categories: [Internet]
tags: [Sciandivasci, Simonetta Sciandivasci, Giovannini, Eva Giovannini, Rai, Attivissimo, Paolo Attivissimo, Twitter]
---
Breve storia di Twitter, lavori sicuri e idee sprezzanti di giornalismo.

Leggo una giornalista [elogiare un’altra giornalista](https://twitter.com/Sciandi/status/1521239559031382023).

<blockquote class="twitter-tweet"><p lang="it" dir="ltr">Quanto è brava <a href="https://twitter.com/evagiovannini?ref_src=twsrc%5Etfw">@evagiovannini</a>? Per la miseria. Ciao.</p>&mdash; Simonetta Sciandivasci (@Sciandi) <a href="https://twitter.com/Sciandi/status/1521239559031382023?ref_src=twsrc%5Etfw">May 2, 2022</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

Vado a vedere, per fiducia. Eva Giovannini si occupa di cronache inerenti astronomia e imprese spaziali. Il tema mi interessa; inizio a leggere i suoi tweet e trovo qualcosa che non va. [Lo faccio sapere](https://twitter.com/loox/status/1521241681689034753).

<blockquote class="twitter-tweet"><p lang="it" dir="ltr">Non abbastanza, se rilancia la bufala della Russia che abbandona la Stazione spaziale internazionale.</p>&mdash; Lucio Bragagnolo (@loox) <a href="https://twitter.com/loox/status/1521241681689034753?ref_src=twsrc%5Etfw">May 2, 2022</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

Per farla molto breve, il direttore dell’ente spaziale russo ha risposto a una domanda riguardante il possibile abbandono della Stazione spaziale internazionale da parte della Russia come conseguenza della crisi ucraina. Tutti i media distratti o di bassa qualità, più la testata dove lavora Eva Giovannini, hanno pubblicato di conseguenza la supposta notizia del già deciso abbandono russo della Stazione. Giovannini [ritwitta la propria testata](https://twitter.com/evagiovannini/status/1520411747689152512).

<blockquote class="twitter-tweet"><p lang="it" dir="ltr">Russia via (da quando?) dalla Stazione spaziale internazionale, contro le sanzioni. Ad annunciarlo è stato il direttore generale del programma spaziale russo Roscosmos, Dmitry Rogozin, in un&#39;intervista alla tv di Stato Rossiya 24 di cui riferisce l&#39;agenzia di stampa Tass. <a href="https://t.co/ofiXi9spAd">https://t.co/ofiXi9spAd</a></p>&mdash; eva giovannini (@evagiovannini) <a href="https://twitter.com/evagiovannini/status/1520411747689152512?ref_src=twsrc%5Etfw">April 30, 2022</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

Paolo Attivissimo, che è un punto di riferimento sull’antibufala ma soprattutto sulla divulgazione in ambito spaziale, scrive come stanno esattamente le cose in [Antibufala: no, la Russia non ha già deciso di lasciare la Stazione spaziale internazionale](https://attivissimo.blogspot.com/2022/04/antibufala-no-la-russia-non-ha-gia.html).

Nel frattempo, Giovannini trova il tempo di [trattare con sprezzo la mia osservazione](https://twitter.com/evagiovannini/status/1521367411508879360).

<blockquote class="twitter-tweet"><p lang="it" dir="ltr">Dichiarazioni del capo di Roscosmos alla tv pubblica, veda un po’ lei.</p>&mdash; eva giovannini (@evagiovannini) <a href="https://twitter.com/evagiovannini/status/1521367411508879360?ref_src=twsrc%5Etfw">May 3, 2022</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

Penso che magari non abbia letto Attivissimo. Ma come fa a occuparsi di spazio e non seguirlo? Oppure che abbia fatto correggere l’articolo. Non è accaduto. [Glielo segnalo](https://twitter.com/loox/status/1521432789014519808).

<blockquote class="twitter-tweet"><p lang="it" dir="ltr">Sbufalamento da parte di Paolo Attivissimo: <a href="https://t.co/DbjvH8fYyL">https://t.co/DbjvH8fYyL</a><br>Buon lavoro!</p>&mdash; Lucio Bragagnolo (@loox) <a href="https://twitter.com/loox/status/1521432789014519808?ref_src=twsrc%5Etfw">May 3, 2022</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

Qui mi accorgo che Giovannini mi ha bloccato, non so se prima o dopo questo ultimo tweet.

Sarebbe meglio prima, perché lascerebbe un piccolo spazio per spiegare il suo comportamento con l’ignoranza di quanto ha scritto Attivissimo. Se invece mi avesse bloccato dopo, beh, una giornalista degna di questo nome dovrebbe approfondire e cercare la verità delle cose. Soprattutto, una giornalista che ha paura del dialogo o del contraddittorio fa poco onore al suo mestiere.

Sarà davvero brava? Può essere. Mi rammarico che lavori alla Rai, pagata quindi anche con il mio canone. Certamente non deve rendere troppo conto del suo operato, visti i criteri dell’azienda statale. Forse è per questo che si comporta con la spocchia vista qui sopra: il posto fisso intoccabile.

L’aerospaziale è un tema dove l’approccio scientifico è importante. Eva Giovannini sembra averne uno diverso, come chiunque ritenga di avere ragione a prescindere fino a silenziare gli interlocutori sgraditi.