---
title: "Fatti in compagnia"
date: 2017-04-11
comments: true
tags: [Swatch, Gruber, Gassée, watch]
---
Mi piace poco scrivere cose in accordo con altri, ma devo fare una eccezione. Sostenevo questo a proposito di Swatch e del suo intento di [creare un sistema operativo autonomo per computer da polso](https://macintelligence.org/posts/2017-03-22-sistemi-di-imprecisione/):

>Rimango dell’opinione che siano stati fatti calcoli un po’ troppo ambiziosi, oppure che gli obiettivi siano molto modesti rispetto a come se ne parla.<!--more-->

Jean-Louis-Gassée scrive nella sua [Monday Note](https://mondaynote.com/swatchos-not-a-smart-decision-6b2cc883c99f):

>Cercare di sconfiggere i produttori consolidati di smartwatch non funzionerà. C’è qualcosa nella cultura del gruppo Swatch che lo predispone alla competitività nei confronti degli ingegneri di Google e Apple?

Il pezzo di Gassée merita la lettura da solo e racconta di come il padre dell’attuale conduttore del gruppo Swatch fece rinascere l’orologeria svizzera, in difficoltà contro i fabbricanti giapponesi di orologetti al quarzo. Ci riuscì con un miglior gioco delle proprie carte, dei propri punti di forza, invece che con l’adeguarsi al gioco dei concorrenti.

È arrivato pure [John Gruber](http://daringfireball.net/2017/04/double_down_on_mechanical_watches):

>Non credo che l’industria orologiaia svizzera abbia una possibilità di battere gli ingegneri Apple sul loro terreno. Dovrebbe invece concentrarsi su quello che ha sempre fatto: progettare e produrre grandi orologi meccanici e nel farlo creare una ventata di fresca aria analogica in un mondo sempre più digitale.

Di previsioni finite nel nulla e nel ridicolo se ne fanno a carrettate e Gassée ricorda appunto quella dell’attuale responsabile del consorzio Swatch rispetto alle fortune di watch. Qui mi azzardo a dire che mi sento in buona compagnia e i fatti, direbbe il mitico [Cevoli](http://www.paolocevoli.com), *mi cosano*.