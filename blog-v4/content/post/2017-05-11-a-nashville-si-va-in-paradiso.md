---
title: "A Nashville si va in Paradiso"
date: 2017-05-11
comments: true
tags: [Apple, Microsoft, Store, Stefano, Nashville]
---
Ricevo, ringrazio e redigo la mail di **Stefano**:

>Ti giro questa simpatica location in un Mall a Nashville, patria della musica country. Sopra il Male Oscuro, sotto Apple. Da notare dove stanno le anime.

Il Male Oscuro non è così tentatore come lo descrivono.

 ![Centro commerciale a Nashville](/images/nashville.jpg  "Centro commerciale a Nashville") 

 ![Microsoft Store in un centro commerciale a Nashville](/images/nashville-ms.jpg  "Microsoft Store in un centro commerciale a Nashville") 

 ![Apple Store in un centro commerciale a Nashville](/images/nashville-apple.jpg  "Apple Store in un centro commerciale a Nashville") 