---
title: "In punta di Safari"
date: 2018-01-11
comments: true
tags: [Safari, KHtml, Konqueror, Chrome, Chromium, mkdir, Ballmer, iOS, iPhone]
---
Il [post sull’anniversario di Safari](https://macintelligence.org/posts/2018-01-07-l-eta-della-ribellione/) ha suscitato vari commenti, compresi quelli di **mkdir** a proposito dei quali intendo chiarire il mio pensiero.<!--more-->

>“…non possiede la forza trainante di un miliardo di computer da tasca, i più utilizzati e versatili…” questa frase sembra di Ballmer…

Non entro nel merito dello stile-Ballmer. Il senso della frase è che gli apparecchi iOS hanno grandemente e decisivamente contribuito a portare l’attenzione su Safari. Senza i numeri portati da queste macchine, Safari – anzi: [WebKit](https://webkit.org) – sarebbe rimasto probabilmente una faccenda per pochi e non avrebbe avuto l’impatto che invece ha avuto nel mondo web. Si noti che Safari non è il *browser* più diffuso né lo è mai stato; non è mai stato monopolista e non ha soffocato alcun altro *browser* esistente. Nessun sito ha mai risposto a una persona *puoi entrare solo se usi Safari*.

>Per alcuni essere FLOSS potrebbe essere una feature migliore di un millisecondo in meno nella renderizzazione, senza contare che Firefox è velocissimo: batte spesso Chrome nei bench come Motion Mark.

Certamente. Molte persone usano Firefox, che è un ottimo *browser* e l’essere *open source* è sicuramente un valore (parlo come socio di [LibreItalia](http://www.libreitalia.it), per dire). Questo nulla toglie all’impatto e ai meriti di WebKit.

>Inoltre esiste una versione 100% FLOSS di Chrome: Chromium.

Che è un’ottima cosa. Qualsiasi gruppo di volontari sufficientemente motivato potrebbe fare esattamente la stessa cosa con Safari. Non lo vedo come un demerito di Safari.

>Che dire di Safari: ha preso codice libero (il “motore WebKit” da KHTML) per farne un app chiusa.

Indubbio. Adesso alzino la mano quanti hanno usato [Konqueror](https://konqueror.org/features/browser.php), primo *browser* a usare il motore [Khtml](https://github.com/KDE/khtml) da cui è partito il lavoro su WebKit. Ci sono poche mani. In anni di rilevazioni di traffico web posso giurare di non avere mai visto citato Khtml o Konqueror. La percentuale di uso è certamente inferiore all’uno percento.

In altre parole: Khtml è una iniziativa bellissima e nobile, che *novantanove persone su cento non hanno mai visto*.

Safari ha portato i vantaggi di Khtml a decine di milioni di persone che altrimenti li avrebbero ignorati. WebKit ha ampliato a dismisura questa cifra. Oggi, tagliata con l’accetta, *due terzi dei navigatori web hanno beneficiato di WebKit*. Siamo sopra il miliardo di persone. Trovo il compromesso accettabile di fronte al risultato.

>Se non altro ha contribuito ad affinare il codice WebKit che viene usato anche da altri browser Open Source come Chromium, disponibile pure in vari repository di distro free GNU/Linux oltre che in diverse live.

Infatti ed è molto positivo. In realtà Chromium usa [Blink](https://www.chromium.org/blink), che è un *fork* (reinterpretazione) di WebKit. Proprio come WebKit è un *fork* di Khtml. È così che funziona l’*open source*: a volte, da un progetto, nasce una variante del progetto con ambizioni più elevate.

Se non ci fosse stato WebKit, non ci sarebbe Chrome e non ci sarebbe Chromium. Ci sarebbe certo Khtml; a usarlo saremmo quattro gatti e là fuori miliardi di persone userebbero altro. Magari ancora schiave del vecchio. Se per stare fuori dalla replica dell’incubo Internet Explorer fa comodo un *browser* chiuso per quanto con motore aperto, datemelo. Subito. Perché di là usano un *browser* chiuso con motore chiuso.