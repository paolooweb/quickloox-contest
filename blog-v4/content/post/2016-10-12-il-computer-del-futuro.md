---
title: "Il computer del futuro"
date: 2016-10-12
comments: true
tags: [Viticci, iPhone]
---
Bella [recensione di iPhone 7](https://www.macstories.net/stories/iphone-7-computer-from-the-future/) da parte di Federico Viticci. Bella perché *Ticci* evita il patetico sentiero standard (c’è dentro il processore tale, Apple dice che la batteria dura così, la app si apre in tre decimi di secondo virgola otto) per raccontare, invece, vita vissuta e pratica dell’apparecchio.<!--more-->

Racconta come si vive senza jack audio, il gusto di avere immagini con una qualità del colore superiore, il funzionamento effettivo del processore W1 quando iPhone si collega *wireless* ad auricolari o cuffie di qualità.

Lettura raccomandata. Citazione:

>…iPhone 7 pianta i semi di tecnologie future che si propagheranno attraverso l’intero ecosistema Apple. Da qui a qualche anno, qualsiasi foto scatteremo sarà in wide color; le cuffie wireless di Apple avranno vantaggi che i modelli con il filo non hanno mai avuto; e le interfacce prive di feedback tattile sembreranno fuori luogo come oggi uno schermo non Retina.

>Queste non sono promesse di funzioni che prima o poi arriveranno: iPhone 7 le offre ora.

Ah: la parola *telefono* ricorre quattro volte e niente più.

>In molti modi, iPhone 7 dà l’idea di un computer portatile del futuro; solo in una maniera pratica e tangibile che è qui con noi già oggi.
