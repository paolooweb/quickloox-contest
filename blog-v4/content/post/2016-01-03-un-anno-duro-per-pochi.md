---
title: "Un anno duro per pochi"
date: 2016-01-03
comments: true
tags: [Raspberry, Pi, Zero, LinuxGizmos]
---
Se per deformazione tengo sempre l’attenzione alta sul software, a proposito di liste e buone intenzioni per il 2016 va ricordato come si stia diffondendo lo hardware a bassissimo prezzo, siano elaboratori completi oppure schede controller per collegare la qualunque, che costituiranno il percorso di apprendimento e di innovazione per un numero impressionante di persone, soprattutto giovanissime, nei prossimi anni.<!--more-->

*LinuxGizmos* ha chiuso il 2015 con un [elenco di 64 schede logiche interessanti](http://linuxgizmos.com/ringing-in-2016-with-64-open-spec-hacker-friendly-sbcs/) con prezzi tra i mitici [cinque dollari di Raspberry Pi Zero](https://macintelligence.org/posts/2015-11-28-quando-i-computer-avevano-la-coda/) in su. Dove comunque *in su* continua a significare *impossibile non poterselo permettere se si riesce a pagare la bolletta della luce*.

Vorrei vederli diffondersi nelle scuole, nelle famiglie, negli oratori, nei campeggi estivi, nelle ludoteche, dovunque si possa insegnare qualcosa che abbia attinenza con il digitale. Pochi soldi possono scatenare immaginazioni infinite nelle persone predisposte a imparare, esplorare, sperimentare. Chi può ne regali, anche se non vengono capiti. È un atto di profonda attenzione.