---
title: "Cariche sacre"
date: 2016-07-20
comments: true
tags: [Scala]
---
Sono estremamente insofferente ai *dress code*. Da decenni non metto una cravatta e già l’idea della camicia mi infastidisce. La indosso, ma la sopporto e niente più.<!--more-->

Riconosco d’altronde che certi luoghi, carichi di tradizione e cultura, chiedono rispetto da parte di chi li frequenta.

Per esempio, la Scala di Milano. Uno dei più grandi teatri d‘opera al mondo, uno dei simboli dell‘eccellenza italiana. Un posto dove giustamente a inizio spettacolo gli ospiti sono invitati a riporre i cellulari.

Durante un intervallo, una maschera della Scala, in uniforme tradizionale che data a due secoli fa, ha appoggiato il suo cellulare a terra, in un corridoio del quarto ordine sinistro. Ha collegato il cellulare a una presa per caricarlo e se ne è andato, lasciandolo lì fino a fine spettacolo.

Caricare il cellulare alla Scala è del tutto fuori luogo. Se fai parte del personale, con accesso a stanze e luoghi vietati agli spettatori, puoi peraltro farlo tranquillamente con grande discrezione.

Se lo fai in modo ostentato e volgare (in senso etimologico), manchi di rispetto al luogo in cui lavori e ai visitatori cui chiedi di mostrare rispetto per lo stesso luogo.

Esercizio: immaginare che tipo di cellulare, dalla pessima autonomia, aveva la maschera irrispettosa.