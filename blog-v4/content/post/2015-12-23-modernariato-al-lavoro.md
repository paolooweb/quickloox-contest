---
title: "Modernariato al lavoro"
date: 2015-12-23
comments: true
tags: [MacBook, vintage, obsoleto]
---
Ho ricevuto un piccolo regalo di Natale da Apple: MacBook Pro 17 pollici (Inizio 2009) è stato incluso nella lista dei prodotti [vintage oppure obsoleti](https://support.apple.com/en-us/HT201624).<!--more-->

Il mio [MacBook Pro 17 pollici (Inizio 2009)](https://support.apple.com/kb/SP503?viewlocale=it_IT&locale=en_US) si è un po’ risentito: entro la fine dell’inverno compirà sette anni e ancora funziona come la mia macchina principale per ore e ore ogni giorno, senza dare un problema.

Ho avuto buon gioco nel consolarlo spiegandogli che è solo *vintage*, non *obsoleto*. Obsoleti sono quei MacBook Pro vecchi e decrepiti del 2008, se proprio dobbiamo dirlo.