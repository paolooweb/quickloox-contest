---
title: "Il dono della scrittura facile"
date: 2022-04-10T15:29:16+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [Markdown, Google Docs, Pandoc, BBEdit, Microsoft Word, Word, Google Documenti, Google Writer]
---
Con [BBEdit](https://www.barebones.com/products/bbedit/) mi basta aggiungere al file il suffisso .md per avere un file [Markdown](https://www.markdownguide.org), quindi per avere un formato semplice, leggero, ragionevolmente completo, contemplato da [Pandoc](https://pandoc.org) nel caso servano conversioni più sofisticate di quella in Html.

Con Google Documenti, trovo nelle preferenze l’opzione di riconoscere un set base di comandi Markdown. Assai poco, grassetto, corsivo, titoli, link, però il set c’è e, per quanto base, risolve una grandissima parte delle esigenze comuni quotidiane.

Con Microsoft Word, mi attacco al tram. Naturalmente è disponibile un add-on.

Per scrivere Html nel modo più semplice del mondo con il word processor più pesante del pianeta occorre un add-on.

E c’è chi paga per avere questo privilegio.