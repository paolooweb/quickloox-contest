---
title: "Trascripta manent"
date: 2023-11-25T17:30:44+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [Audio Hijack Pro, Rogue Amoeba]
---
Neanche un trimestre e sono molto soddisfatto di [avere adottato Audio Hijack Pro di Rogue Amoeba](https://macintelligence.org/posts/2023-09-30-e-con-il-suo-spirito/).

Ancora più soddisfatto da quando è arrivato come beta [il modulo di trascrizione dell’audio](https://weblog.rogueamoeba.com/2023/11/02/turn-speech-into-text-with-audio-hijack-4-3s-new-transcribe-block/), del tutto compreso nel prezzo. Di fatto, con Audio Hijack Pro si può trasformare in testo scritto automaticamente qualsiasi audio sentiamo su Mac ed è pure facile da impostare.

È in arrivo anche da macOS la funzione, eh. Però è tutto tranne che una sovrapposizione. Per l’audio che arriva dall’esterno o origina da una app, con macOS si fa ben poco.