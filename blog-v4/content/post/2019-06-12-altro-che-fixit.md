---
title: "Altro che iFixit"
date: 2019-06-12
comments: true
tags: [watch]
---
Vai a fare sport come tutte le settimane e, con somma noncuranza, watch decide di aprirsi da solo.

 ![watch si apre](/images/watch.jpg  "watch si apre") 

Nessun colpo, nessun trauma, usura evidente invece del fissaggio dello schermo alla cassa.

Si tratta di un modello di prima generazione e non penso che in Apple Store lo ripareranno; sono curioso di vedere che mi offrono.