---
title: "Che domenica"
date: 2022-07-04T01:00:34+01:00
draft: false
toc: false
comments: true
categories: [Hardware, Software, Internet]
tags: [Manton Reece, Reece, Playdate, Wimbledon, Paolucci, iPod touch, Offagna]
---
All’ingresso del [Museo Luigi Paolucci](https://www.visitoffagna.it/museo-luigi-paolucci-offagna/) distribuiscono quest’anno un terminale che somiglia a un iPod touch, inserito in una custodia antiurto. Nelle sale si trovano codici QR che, inquadrati con l’apparato, fanno partire cartoni animati nei quali alcuni dei protagonisti del museo (animali, specialmente uccelli, parte di una collezione naturalistica) raccontano ai più piccini informazioni relative all’esposizione. Semplice, efficace, pulito, a prova di tutto, compreso nel prezzo. Bravi.

Il regista a [Wimbledon](https://www.wimbledon.com) ha inquadrato con il teleobiettivo una signora che riprendeva con iPhone. La mela sul dorso di quest’ultimo ha risposto con un riflesso quasi artistico, inconfondibile eppure discreto. Non posso provarlo, però immagino che qualcuno del team, a suo tempo, abbia speso ore di lavoro ad assicurarsi che l’effetto fosse proprio quello. E sembra un orpello, quando è invece una ragione serissima per scegliere un iPhone.

Manton Reece ha confidato di [essere collaudatore di un gioco per Playdate](https://www.manton.org/2022/07/03/helping-cheesemaker-test.html). Lui commenta quanto sia divertente trovarsi nella situazione di vedere nascere un gioco; io, quanto sia straordinaria una domenica di estate in cui si può leggere bene di [Playdate](https://play.date), cui auguro ogni fortuna e anche di più.