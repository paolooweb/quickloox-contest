---
title: "Meno di così"
date: 2016-03-02
comments: true
tags: [Super, Sharp]
---
Rompicapo di una semplicità iniziale veramente assurda, [Super Sharp](https://itunes.apple.com/it/app/super-sharp/id1033873301?l=en&mt=8). Anni fa pensavo che dietro un grande gioco ci fosse una grande complessità e questo rimane vero. Ma non avevo pensato al ramo opposto, l’inseguimento del minimalismo e della semplicità. D’altronde non c’erano le *app*.<!--more-->

L’ho scaricato gratuitamente dopo avere scoperto che, in occasione dell’anno bisestile, App Store regalava cinque *app*. Le altre quattro devo ancora guardarle; ho tuttavia maturato un secondo insegnamento. Anche se l’agenda è molto stretta, un occhio distratto alle *app* offerte in promozione gratuita vale sempre la pena. E vanno scaricate; buttarle via è sempre possibile dopo, ma la possibilità di averle gratis resterà per sempre, inteso come durata di App Store.

Per scoprire qualcosa di interessante a spesa zero in modo facile, meno di così è impossibile. D’altronde, prima, non c’erano le *app*.