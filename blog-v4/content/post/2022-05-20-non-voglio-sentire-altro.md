---
title: "Non voglio sentire altro"
date: 2022-05-20T01:56:13+01:00
draft: false
toc: false
comments: true
categories: [Software, Hardware]
tags: [Wwdc, iPhone, iPad, Mac, VoiceOver, Voice Control, Switch Control, Door Detection, Apple Watch Mirroring, People Detection, Image Descriptions, FaceTime, Live Captions, Text Checker]
---
Pur mancando due settimane a [Wwdc](https://developer.apple.com/wwdc22/), ieri cadeva il [Global Accessibility Awareness Day](https://accessibility.day) e Apple, invece di parlare di *rumor*, ha anticipato [funzioni innovative di accessibilità che uniscono la potenza di hardware, software a machine learning](https://www.apple.com/newsroom/2022/05/apple-previews-innovative-accessibility-features/), che faranno matematicamente parte delle novità software di iOS, macOS, iPadOS, watchOS e quant’altro, per cui le vedremo in azione prima di fine anno.

I non vedenti potranno individuare le porte in un posto non conosciuto e ricevere descrizioni di che cosa sta loro intorno. Maps fornirà feedback tramite VoiceOver a chi richiede direzioni per camminare verso un luogo.

Apple Watch sarà maggiormente pilotabile via iPhone, con comandi vocali, azioni attivate da suoni, tracciamento dei movimenti del capo o apparecchi compatibili Made for iPhone.

I non udenti potranno approfittare dei sottotitoli in FaceTime.

VoiceOver aggiunge venti tra lingue e dialetti alla propria copertura e introduce il controllo dei testi per trovare semplici errori di formattazione come maiuscole sbagliate doppi spazi, ostacoli importanti per una persona con disabilità intenta a controllare l’esattezza di un testo.

Ho coperto neanche un quarto del contenuto della pagina di Apple e senza entrare nei dettagli, che lascio al lettore interessato. Se a qualcuno invece i dettagli servissero davvero, basta saperlo e espanderò il post fino a coprire tutto il contenuto. Ho linkato più su la versione in inglese, come al solito più precisa e concisa; qui linko [la pagina in italiano](https://www.apple.com/it/newsroom/2022/05/apple-previews-innovative-accessibility-features/).

Sono molto fortunato perché mi trovo in forma fisica più che ragionevole; sono fortunato al quadrato per avere incontrato persone e fatto esperienze che mi hanno insegnato che dono grande sia questo. Leggere annunci come questo e pensare alle persone che avranno una vita più facile, più comoda, o potranno fare più di prima, o essere maggiormente autosufficienti, riempie il cuore e, per quanto mi riguarda, Wwdc potrebbe anche essere vuota di annunci. Queste soddisfazioni sono più vere.