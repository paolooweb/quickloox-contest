---
title: "Babbo Natale premia iOS"
date: 2020-01-06
comments: true
tags: [ iOS, Android, Sensor, Tower]
---
Secondo *Sensor Tower*, il giorno di Natale [si sono spesi in app 277 milioni di dollari](https://sensortower.com/blog/app-revenue-christmas-2019), con un incremento dell’11,3 percento sul 2018.

Il 70 percento della spesa è stata effettuata su App Store, per un valore in crescita del 16 percento anno su anno. Il resto è andato su Google Play Store, che è cresciuto del 2,7 percento.

Il numero di apparecchi iOS è una frazione di quello Android, ma genera ancora una volta molta più spesa e, nel caso, molti più regali. Le previsioni che davano nel lungo termine un pareggio o addirittura un sorpasso di Android su iOS in termini economici, sono diventate a termine ancora più lungo.

Appare evidente che babbo Natale abbia un debole per iOS. Tenuto conto di tutto il lavoro che deve svolgere in una notte, è anche un gran complimento per le Mappe di Apple.
