---
title: "Proprio pro"
date: 2016-11-18
comments: true
tags: [MacBook, Pro, Touch, Bar]
---
Ci voleva una testata irriverente e velenosa come *Huffpost* per pubblicare un articolo intitolato [Un’occhiata al nuovo MacBook Pro da parte di un professionista](http://www.huffingtonpost.co.uk/thomas-grove-carter/one-professionals-look-at_b_12894856.html). Specie se l’*incipit* è questo:

>Da quando Apple ha presentato la nuova linea di MacBook Pro, metà di quello che leggo arriva da “professionisti” che mi dicono come i nuovi Mac non siano affatto Pro, non siano Pro abbastanza e non siano il giusto tipo di Pro. Quante persone hanno veramente messo le mani su questi apparecchi? Poche. Ho usato il nuovo MacBook Pro 15” (con Touch Bar) per l’ultima settimana lavorandoci veramente ed ecco la mia opinione “professionale”.

(Parentesi: vedo apparire diverse recensioni di MacBook Pro 13” senza Touch Bar. Mi pare che l’unica ragione della loro esistenza sia la disponibilità della macchina, il che le rende disdicevolmente inutili).

L’autore dell’articolo lavora in una azienda londinese che si occupa di montaggio video per film, videoclip e spot pubblicitari. Usa Final Cut Pro X.

>Prima di tutto, [MacBook Pro] è veramente veloce. Ho lavorato tutta la settimana con materiale ProRes 5K, scorre come il burro. Qualunque cosa pensiate dicano le specifiche, software e hardware sono così bene integrati che fanno a pezzettini le specifiche controparti “superiori” Windows nel mondo reale.

Numerosi sono i momenti che fanno consigliare la lettura. Mi limito a citare i passaggi finali:

>Il mondo è cambiato. La tecnologia si muove velocemente ma a spizzichi e bocconi. YouTube eroga giornalmente a milioni di persone video 4K, mentre certe emittenti riescono appena a trasmettere spazzatura 1080p a un pubblico in diminuzione. Una macchina Pro deve coprire uno spettro talmente ampio di persone e discipline che difficilmente soddisferà tutti i professionisti in tutte le loro attività. Ma questo Mac ne soddisferà probabilmente la grande maggioranza, dalla fascia bassa a quella alta.

>Un “professionista” dovrebbe essere definito dal lavoro che consegna e dal valore che porte, non dal suo equipaggiamento. Usi il nuovo MacBook Pro, non lo usi. Al tuo pubblico non interessa. Devi semplicemente continuare a creare grandi cose al meglio che puoi.

Questa potrebbe essere davvero la parola definitiva.