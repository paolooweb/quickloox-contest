---
title: "Dell’analfabetismo informatico"
date: 2022-06-11T13:30:46+01:00
draft: false
toc: false
comments: true
categories: [Internet]
tags: [Comandi Rapidi, Excel, Calca]
---
Problema aritmetico: dati ventisette adulti e ventiquattro bambini riuniti alla cena di classe di fine anno, premesso che i ventiquattro bambini mangiano un menu univoco mentre i ventisette adulti scelgono tra cinque varianti e che una singola persona ha anticipato una caparra da ripartire tra le famiglie (alcune delle quali presenti in blocco e alcune parzialmente), posto che il locale non accetta conti separati, quanto tempo/persona occorrerà per effettuare un pagamento senza errori?

Applicato il problema a un target di censo medio/benestante e istruzione medio/alta, questo è quanto ho osservato.

La soluzione considerata accettabile è stata approssimata.

Si sono applicate non meno di dieci persone. Fosse anche per dieci minuti a testa, sono almeno cento minuti/persona. In un ambito ideale, la soluzione dovrebbe essere istantanea e generata automaticamente una volta raccolte le ordinazioni.

Su una base installata di decine di computer da tasca, il software più evoluto impiegato è stato la calcolatrice.

Avrei volentieri impostato una soluzione con l’aiuto di [Calca](http://calca.io), se non fosse stato impossibile perché avrebbe richiesto almeno qualche minuto e apparentemente un consesso come quello descritto non contempla la possibilità di attendere alcuni minuti senza che chiunque voglia dire la propria, proporre un metodo alternativo, iniziare autonomamente una raccolta arbitraria di contanti.

Penso che si possano trovare diversi software abbastanza simili a Calca ma adatti a un compito del genere; nessuno ha fatto cenno di conoscerli.

Ma parliamo un attimo dell’organizzazione, che avrebbe interesse a semplificare i processi di ordine e pagamento, visto che impegnano personale ed espongono a rischi. Il locale ha un buon sito, che elenca i menu e ne dettaglia il costo, solo che non permette di ordinare o pagare. Senza andare nel sofisticato: codici QR accanto agli elementi del menu (che sono una dozzina, mica mille). Il cameriere prende le commesse fotografando i codici man mano che arrivano gli ordini. Il codice QR dice grosso modo *uno di questo* a una struttura dietro che prepara la lista degli ordini e sa subito che cosa è stato ordinato, in che quantità e a quale prezzo complessivo. Alla fine della cena, si esamina la lista e ripartire la spesa è (più) semplice.

Ci vuole un programmatore, sicuro, almeno un programmatore web se non c’è di meglio. Ma è una cosa semplice semplice, che non dovrebbe essere proibitiva per una struttura come quella. A occhio una giornata di incassi potrebbe pagare una soluzione anche semplice e rozza, però efficace. La maggiore produttività di camerieri e cassa contribuirebbe ad ammortizzare il costo. I clienti sarebbero più contenti.

Se fosse stata una cena di nerd, qualcuno avrebbe tirato fuori in pochi minuti un Comando Rapido o qualche *snippet* di Python per chiudere rapidamente la questione.

Nessuno vuole una nazione di nerd. Tuttavia semplificare la gestione del conto di una cena di classe facilita la vita. Tutti vogliono, vorrebbero? una vita più facile.

Questo abisso tra cultura generale e software di base ci condanna (assieme ad altro) a essere una nazione di basso rango, a vivere peggio.

Intanto le persone e le aziende sono fissate con Excel. Che però gli serve a disegnare tabelline e a mettere in colonna elenchini. Pagare un conto complesso non passa per l’anticamera del cervello e fa capire quanto sia effettivamente utile, il programma universale, per il cittadino medio nella vita quotidiana.