---
title: "Per il tuo nome c’è spazio"
date: 2023-11-23T17:05:21+01:00
draft: false
toc: false
comments: true
categories: [Internet]
tags: [Clipper, Europa, Nasa]
---
La Nasa ha in programma di spedire una sonda, Clipper, a esplorare il satellite di Giove chiamato Europa, uno di quelli che sotto una superficie irrimediabilmente ghiacciata di idrocarburi potrebbe anche nascondere qualche traccia di vita extraterrestre o scoperte realmente importanti.

Sempre la Nasa offre a chiunque vorrà compilare un modulino minimale la possibilità di [fare incidere il proprio nome sulla sonda](https://europa.nasa.gov/message-in-a-bottle/sign-on/), che viaggerà provvista di un poema scritto per celebrare la missione.

Il lancio è previsto per ottobre 2024 e penso che ci sia ancora almeno qualche mese per firmare. Però sai mica che finisca lo spazio, non quello interstellare ma a bordo?

Per chi desiderasse una opinione non richiesta: si tratta della firma più concretamente utile e significativa che abbiamo mai apposto a una petizione o a qualunque altro documento online.