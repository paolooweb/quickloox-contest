---
title: "Il giardino disincantato"
date: 2021-11-15T01:48:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [Apple, macOS, Windows, Edge, Safari, Paul Thurrott, Mac, Internet Explorer] 
---
Apple viene spesso chiamata in causa per avere realizzato il _walled garden_, il giardino recintato, cattivo perché chiuso e per negare a chi lo popola la libertà di volere tutto e il suo contrario.

Intanto su Windows, il sistema dove invece volere tutto e il suo contrario parrebbe essere l’abitudine secondo i suoi sempre oggettivi e imparziali sostenitori, si legge che [Microsoft rende eccezionalmente difficile cambiare le applicazioni di default e specialmente il browser](https://www.thurrott.com/windows/windows-11/259208/windows-11-to-block-windows-11-browser-workarounds). Tocca usare Edge e, se non piace, usare Edge.

È stata scritta anche una utility per aggirare l’imposizione di uso del browser e, ad ascoltare il post, i nuovi sviluppi di Windows aggirano l’aggiramento, per renderlo inutilizzabile.

>Microsoft si limita a ignorare silenziosamente le chiavi di registro UserChoice e fa aprire Edge. […] Anche se si brutalizza l’installazione di Windows e si elimina ogni traccia di Edge, Windows aprirà una finestra vuota e mostrerà un messaggio di errore pur di non lasciare usare un altro browser.

Il cambiamento è apparso in una versione preliminare per gli sviluppatori e Microsoft avrebbe dichiarato che si tratta di un errore, naturalmente, che verrà sistemato, ovvio.

Questo dall’azienda che è passata da una dolorosa sentenza antitrust a causa dei trucchi sporchi usati per costringere le persone a stare su Internet Explorer. Azienda che è cambiata, certo. Non è più quella di prima, no. Come scrivono in un commento, prima le cose te le imponeva e basta. Adesso lo fa ugualmente, con più raffinatezza.

Su Mac, nel _walled garden_, cambiare browser preimpostato è scegliere da un menu dentro Preferenze di Sistema alla voce Generali. Fatto.

_Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile._