---
title: "Non solo Macworld"
date: 2014-12-17
comments: true
tags: [Dr.Dobb’s, Macworld]
---
Credo che gli sforzi per avere una presenza cartacea italiana di Macworld siano definitivamente tramontati, dopo la chiusura dell’edizione cartacea americana e per il fatto che gli ultimi editori a cimentarsi nel Belpaese hanno mostrato una discreta incapacità di svolgere il proprio mestiere (chiudere Macworld Italia è una cosa, fallire un’altra).<!--more-->

Il mutamento delle condizioni del mercato sta tuttavia facendo vittime ben più illustri, anche se meno conosciute. Negli Stati Uniti sta [tramontando Dr. Dobb’s](http://www.drdobbs.com/architecture-and-design/farewell-dr-dobbs/240169421), probabilmente la più importante testata dedicata alla programmazione.

*Tramontare* non è verbo scelto a caso; gli editori lo usano in America per indicare un sito che rimane visibile e funzionante, ma smette di aggiungere nuovo materiale.

*Dr. Dobb’s* ha funzionato prima su carta e poi su web per trentotto anni, su web con ascolti siderali da dieci milioni di pagine l’anno e su carta come pubblicazione quasi esoterica in Italia, dove i pochissimi a praticarla la veneravano o quasi. Probabilmente non esiste un programmatore sopra i quarant’anni che non abbia consultato almeno una volta i contenuti di *Dr. Dobb’s*.

Ma la pubblicità sul web – dicono dal sito – non rende più come una volta e tutto continua a cambiare. Tutto, non solo l’editoria: se *Dr. Dobb’s* aveva un difetto, era l’eccessiva attenzione a Windows. Vuoi o non vuoi, prima era un difetto che magari comunque portava fatturato. Ora non più.