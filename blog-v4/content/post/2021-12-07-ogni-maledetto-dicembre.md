---
title: "Ogni maledetto dicembre"
date: 2021-12-07T00:31:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [avvento, JavaScript, Raku, HtmHell, Css, Html, cibersicurezza, cybersecurity, Advent of Code, Natale] 
---
Diciannovemila dollari di premi per risolvere [sfide non impossibili di cibersicurezza](https://tryhackme.com/christmas), affrontabili – senza premi, solo per il gusto – anche nel solo ambito delle [vulnerabilità del codice](https://blog.sonarsource.com/code-security-advent-calendar-2021).

Invece che vincere premi si può decidere di pagare per ottenere [sfide (e soluzioni) su Css e JavaScript](https://www.adventofcss.com). Qualcosa di meno esoterico? [HTMHell](https://www.htmhell.dev/adventcalendar/), dall’aspetto più satanista. Ma è solo HTML da capire meglio. Un’altra chance di affrontare sfide JavaScript [si trova qui](https://www.bekk.christmas).

È sempre tempo di restituire alla comunità qualcosa di quello che abbiamo ricevuto in abbondanza, contribuendo a software libero e magari [facendolo tutti i giorni](https://24pullrequests.com) in via eccezionale.

Migliorare il proprio rapporto quotidiano con Java? Personalmente non vado oltre il caffè, ma c’è chi apprezza il linguaggio e ha [una bella occasione](https://www.javaadvent.com/calendar). A livello di scoperte clamorose, abbiamo altrimenti la possibiità di [impratichirsi con Raku](https://raku-advent.blog/category/2021/), un *linguaggio multiparadigma* (qualunque cosa voglia dire).

Quello che mi piace più di tutti, comunque, pure quest’anno è [Advent of Code](https://adventofcode.com). L’importante è provarci, da qualche parte, e mostrare a Babbo Natale che invece di essere stati buoni – dote sopravvalutata – abbiamo imparato delle cose.

(Sì, ogni anno fa effetto Natale parlare di calendari dell’Avvento e tengo alla tradizione).

_Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile._