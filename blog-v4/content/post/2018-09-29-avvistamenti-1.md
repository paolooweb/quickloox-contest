---
title: "Avvistamenti / 1"
date: 2018-09-29
comments: true
tags: [TedX, Modena, MacBook, Pro]
---
MacBook Pro in mano all’organizzazione del [TedX Modena](http://www.tedxmodena.it) di ieri.

È il quinto che viene organizzato nel giro di due anni e la formula deve rispondere a criteri precisi di efficienza e organizzazione, per potersi fregiare del marchio TedX. Non c’è un requisito sui computer ma appunto, potendoli scegliere, succede che si vedano MacBook Pro.

 ![MacBook Pro a TedX Modena](/images/tedx-modena.jpg  "MacBook Pro a TedX Modena") 