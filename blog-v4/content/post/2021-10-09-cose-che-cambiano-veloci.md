---
title: "Cose che cambiano veloci"
date: 2021-10-09T01:25:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [AnandTech, A15, Semianalysis, iPhone 13] 
---
Neanche un mese stavamo a [I progressi delle CPU Apple si arrestano e il futuro appare grigio per l’impatto che inizia a farsi sentire dell’esodo di ingegneri verso Nuvia e Rivos](https://semianalysis.com/apple-cpu-gains-grind-to-a-halt-and-the-future-looks-dim-as-the-cpu-engineer-exodus-to-nuvia-and-rivos-impact-starts-to-bleed-in/). A Milano direbbero *ciumbia*, roba forte, con spiegazione-shock:

>Riteniamo che Apple abbia dovuto ritardare lo sviluppo del nucleo della CPU di prossima generazione per via del ricambio di personale che ha dovuto affrontare.

Oggi possiamo leggere la recensione del chip A15 presente negli iPhone 13, curata come sempre in modo esemplare da *AnandTech*: il titolo sintetizza [più veloce e più efficiente](https://www.anandtech.com/show/16983/the-apple-a15-soc-performance-review-faster-more-efficient/).

*AnandTech* fa riferimento allo stesso stato di cose ma scrive in modo leggermente diverso:

>Naturalmente, qui il tono trasmette una immagine piuttosto conservativa dei miglioramenti della CPU A15 che, al momento di esaminare prestazioni ed efficienza, sono tutt’altro.

E poi:

>In confronto alla concorrenza, A15 non è il cinquanta percento più veloce come dichiara Apple, ma piuttosto più veloce del sessantadue percento.

Si parla anche di giochi:

>A15 continua a cementare il dominio di Apple nel gaming mobile.

Il che fa pensare anche alla grafica:

>I miglioramenti nelle prestazioni di picco della GPU vanno fuori scala, grazie a una combinazione di unità più grande, nuova architettura e cache di sistema maggiorata, che giova sia alle prestazioni che all’efficienza.

In un mese cambiano un sacco di cose. Velocissime.

*Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile.*