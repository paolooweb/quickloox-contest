---
title: "Basterebbe un commento"
date: 2016-12-08
comments: true
tags: [Cook, watch, Idc, Post, York, New, MacDailyNews]
---
Allarme! un rapporto Idc dà [in caduta libera le vendite di watch](https://www.idc.com/getdoc.jsp?containerId=prUS41611516).<!--more-->

Tim Cook [ribatte su Reuters](http://www.reuters.com/article/us-apple-watch-idUSKBN13V0BT) che watch ha stabilito un record di vendite durante il primo weekend di shopping prenatalizio, ma per il *New York Post* [non è chiaro se sia riuscito a restituire il colpo in modo significativo](http://nypost.com/2016/12/06/tim-cooks-defense-of-apple-watch-falls-flat/).

Che matassa intricata. Forse per scioglierla è sufficiente un commentino come quello di [MacDailyNews](http://macdailynews.com/2016/12/06/apple-watch-sales-growth-is-off-the-charts-on-track-for-the-best-quarter-ever/):

>Apple non permetterà che le vendite prenatalizie di watch siano danneggiate dai comunicati stampa Idc che non solo “stimano” le consegne (Apple non divulga numeri, per non avvantaggiare la concorrenza), ma confrontano stupidamente smartwatch da 250 dollari e più con bracciali della salute da quindici dollari.

Anche perché, aspetta un po’, Apple potrebbe persino essere [passata dal 37 percento al 46,6 percento della torta](http://appleinsider.com/articles/16/12/06/new-study-on-holiday-wearables-sales-gives-apple-watch-dominant-revenue-lead).