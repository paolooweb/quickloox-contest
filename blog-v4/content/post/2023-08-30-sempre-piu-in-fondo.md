---
title: "Sempre più in fondo"
date: 2023-08-30T02:41:47+01:00
draft: false
toc: false
comments: true
categories: [Internet, Sodtware]
tags: [Microsoft, ChatGPT, Gptbot, McCarthy, Kieran McCarthy]
---
Eric Goldman ha sintetizzato egregiamente un articolo di Kieran McCarthy sullo [stato delle cose riguardo a grandi modelli linguistici e scraping](https://pxlnv.com/linklog/scraping-not-for-thee/), al punto che il mio resoconto è praticamente una traduzione del suo pezzo. Per una volta, spero mi perdonerà.

Microsoft ha recentemente vietato [lo scraping o altre forme di estrazione di dati dai propri servizi di intelligenza artificiale](https://www.theregister.com/2023/08/15/microsoft_will_store_your_conversations/), *soi-disant*. Se vuoi curiosare e ricavare dati accumulati da quei servizi, niente da fare se non trovarsi un avvocato.

Nel mentre, OpenAI – produttori di ChatGPT, abbondantemente finanziati da Microsoft – [mettono a punto Gptbot](https://www.marktechpost.com/2023/08/10/openai-introduces-gptbot-a-web-crawler-designed-to-scrape-data-from-the-entire-internet-automatically/), uno strumento che fa scraping in Internet a beneficio di ChatGPT stesso.

>E può non essere ammesso pubblicamente, ma OpenAI ha quasi certamente già pescato a strascico nell’intera Internet libera per procurarsi i dati con cui addestrare GPT-3, ChatGPT e GPT-4.

Dati raccolti sui quali i termini di utilizzo della licenza di OpenAi, senza la minima traccia di ironia, [è proibito fare scraping](https://openai.com/policies/terms-of-use).

Il problema non è che lo facciano per soldi, ma che lo facciano senza dirlo e senza avere chiestouna autorizzazione o concesso alcuna strada per starne fuori.

Quando pensi che Microsoft abbia toccato il fondo, e dopo che ha finito di scavare, inizia a fare scraping.

*Gli aggiornamenti in agosto del blog potrebbero essere irregolari o infrequenti.*