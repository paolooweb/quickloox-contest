---
title: "Pro come una matita"
date: 2016-06-26
comments: true
tags: [iPad, Pro, Pencil, Note, Moleskine]
---
Ricevo e pubblico da **Stefano**, che ringrazio. Sono sempre molto curioso di notizie sull’interazione personale con hardware e software.<!--more-->

**Dopo più di un mese di lavoro** sul mio [iPad Pro](http://www.apple.com/it/ipad-pro/) da nove pollici, trovo eccezionale l’utilizzo della *app* Note con [Apple Pencil](http://www.apple.com/it/apple-pencil/): ne esistono mille altre ma Note fa giusto quel che serve, come sempre con Apple.

Giusto due osservazioni:

1. Quando apro una Nota, per default si presenta l’utilizzo della tastiera: passo poi alla scrittura della mia nota con Apple Pencil ma non posso più tornare all'utilizzo precedente con la tastiera (oppure non riesco a capire come si faccia). Ma perché tornare all'utilizzo con tastiera dopo aver utilizzato la penna? E qui vado direttamente alla seconda osservazione.

2. Utilizzando Note, con Apple Pencil si perde l'opportunità di indicizzare la ricerca delle tue note: devi per forza inserire del testo con la tastiera come titolo del tuo appunto (appare ovvia la cosa, ma me ne sono accorto dopo una ventina di manoscritti). 

Quindi la mia procedura operativa è la seguente: apro Note, digito il titolo dell’appunto e poi proseguo con Apple Pencil.

Trovo strano che mentre Apple blocchi l’accesso alla nota (fantastico l’uso del Touch ID) non faccia niente per un ulteriore controllo se volessi cancellare la stessa nota bloccata.

Voglio dire: blocco le note critiche perché non voglio che in caso di prestito del telefono qualcuno le guardi. Però sempre quel qualcuno potrebbe cancellarmele creandomi un danno.

Avrei forse attivato il Touch ID anche in caso di cancellazione.

Per inciso: ho provato Google Keep.

Fa molte cose in più ma l’interfaccia pulita di Note a mio avviso è insuperabile.

Tutto questo se si vuol rimanere in Note: magari altre *app* fanno meglio.

L’unica cosa che odio della Apple Pencil è il suo tappino magnetico: inizio a giocarci aprendolo e chiudendolo ed è fatta, non te ne stacchi più, è una droga.

Per il resto dico solo che è l'ennesimo miracolo di tecnologia in salsa Cupertino.

La risposta al tocco è immediata senza latenze e con minima pressione il tratto si infittisce. Il peso è nella norma e l’ergonomia perfetta: mai mi è capitato di perderla tra le mani.

Il bilanciamento del peso fa sì che appoggiandola su una superficie piana non rotoli, il che non è poco visto che Apple non ha pensato ad uno spazio ben preciso dove collocarla sull’iPad.

Questo ultimo dettaglio sembra strano: non hanno voluto sporcare il design delle cover con occhielli per la penna. A pensarci bene non saprei nemmeno io dove attaccarla all’iPad, a meno di tasche apposite di dubbia bellezza. L’aggancio magnetico a mio avviso lo hanno scartato: una protuberanza circolare su superficie piana è di facile sgancio al primo impatto.

La sto usando da due settimane buone e ho abbandonato la mia Moleskine. La mano sta prendendo sempre più confidenza a toccare il video con il polso (all'inizio la si impugna come un direttore d’orchestra): lo schermo capisce e non rimane il tratto se non attraverso l’Apple Pencil.

Sembra tutto dannatamente semplice nell’utilizzo: questa è la sua forza.

*Last but not least*: quante volte capita di sentire *dammi una penna che scrive, ho urgenza!*

Ecco, con Apple Pencil completamente scarica, in 60 secondi di carica nell’iPad hai a disposizione circa il 15 percento di batteria. È una scheggia nella ricarica! **Mai più senza.**

*[P.S.: ovviamente attendo con ansia l'ultima fatica di Akko e soci dopo il mio contributo su [Kickstarter](https://www.kickstarter.com/projects/295108589/cuore-di-mela-i-segreti-di-apple-iphone-e-mac).]*

Due commenti.

Primo, la frase *ho abbandonato la mia Moleskine* vale da sola un trattato.

Secondo, stiamo parlando evidentemente di hardware professionale.