---
title: "Dovere di testimonianza"
date: 2014-07-02
comments: true
tags: [MacTeX, LaTex, Pantieri, Gordini]
---
Dormirei assai meno sereno se trascurassi di segnalare l’edizione 2014 di [MacTeX](https://tug.org/mactex/), LaTeX per Mac. Due gigabyte e mezzo ben spesi, se in cambio abbiamo sotto le mani il miglior sistema di composizione tipografica mai concepito.<!--more-->

Servisse aiuto per cominciare, penso che il massimo sia [L’arte di scrivere con LaTeX](http://www.lorenzopantieri.net/LaTeX_files/ArteLaTeX.pdf), di Lorenzo Pantieri e Tommaso Gordini. Che poi rappresenta una eminente dimostrazione di quello che si può fare con LaTeX, naturalmente.