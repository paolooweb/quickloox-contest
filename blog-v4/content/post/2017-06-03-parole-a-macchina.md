---
title: "Parole a macchina"
date: 2017-06-03
comments: true
tags: [Editorial, workflow, Octopress, Template]
---
Faceva talmente caldo che mi sono messo a cercare un *workflow* di [Editorial](http://omz-software.com/editorial/) capace di compilare in automatico le righe di intestazione che [Octopress](http://octopress.org/) pretende all’inizio di ciascun file.

L’ho trovato: si chiama [Octopress Template](http://www.editorial-workflows.com/workflow/5252485694357504/5M7iTYKP5ak). Richiede qualche modifica per renderlo appena più furbo, ma ci siamo.

Non dovrebbe più accadere – almeno quando scrivo da iPad – che il *timestamp* del post sia più recente del dovuto o comunque sbagliato. Quando una procedura può essere automatizzata, va automatizzata, non c’è niente da fare.