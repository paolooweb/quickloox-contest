---
title: "Enterprise Different"
date: 2021-01-07
comments: true
tags: [ Jamf, Mac, Apple, Fortune]
---
Parrebbe che l’Enterprise Management per apparecchi Apple di Jamf abbia iniziato l’anno con [venti milioni di apparecchi Apple governati in azienda](https://www.jamf.com/resources/press-releases/jamf-ends-2020-powering-more-than-20-million-enterprise-apple-devices/).

Oltre quarantasettemila aziende servite e, secondo il comunicato, pure di livello. Banche università, ospedali, aziende in cima alla lista di Fortune.

Niente di male a volere usare Windows in azienda, eh. Ma che sia una scelta obbligata, possono crederci solo gli sprovveduti o gli affabulatori con un interesse in ballo.
