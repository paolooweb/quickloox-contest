---
title: "Il test dell'estate"
date: 2020-06-19
comments: true
tags: [Test, estate, tipografia]
---
Si sa che bastano i like su Facebook per profilare all’impossibile. Sembra incredibile ma sono in grado di ricavare una quantità ancora superiore di informazione sull'autore di un documento che ricevo, in base alla cura della tipografia.

Basta la tipografia, non c’è neanche bisogno di arrivare all’impaginazione.