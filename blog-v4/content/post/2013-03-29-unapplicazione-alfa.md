---
title: "Un’applicazione alfa"
date: 2013-03-29
comments: true
tags: [Quicksilver, Pasqua]
---
Nell’uovo di Pasqua da aprire dopodomani c’è una bella sorpresa: <a href="http://qsapp.com">Quicksilver</a> <a href="http://blog.qsapp.com/post/46268365849/quicksilver-comes-of-age">è uscito da dieci anni di versione beta</a> e si presenta in versione <i>uno punto zero</i>.<!--more-->

Quicksilver non cambia la vita né, in fondo, la vita su Mac. Ma velocizza, sveltisce rende più efficiente e astuta quasi ogni operazione. Volendolo, beninteso, perché richiede un minimo di dedizione per raggiungere i massimi risultati.

Specie se a Pasquetta piove, è il momento. Non so quanti si possano permettere di non avere un Mac efficiente al massimo e di mantenere flussi di lavoro farraginosi. Basta davvero qualche ora dedicata a Quicksilver, applicazione alfa tra tante altre che offrono tipicamente grandi promesse e poco più.