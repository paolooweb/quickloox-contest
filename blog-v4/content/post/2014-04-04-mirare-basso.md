---
title: "Mirare basso"
date: 2014-04-04
comments: true
tags: [AnandTech, Swift, Cyclone, Android, iPad, iPhone]
---
Affascinante quanto difficile da seguire nei dettagli tecnici la descrizione della microarchitettura dei processori dentro iPhone 5s e iPad Air, [preparata mirabilmente](http://www.anandtech.com/show/7910/apples-cyclone-microarchitecture-detailed) da AnandTech.

Per i non tecnici, un paio di note rapide. La prima è che la microarchitettura precedente aveva il nome in codice Swift, *svelto*. Questa invece si chiama Cyclone e non c’è bisogno di tradurre. Anandtech mette in chiaro che i nomi in codice corrispondono anche a un salto di qualità nelle prestazioni, che le *app* neanche hanno iniziato a sfruttare pienamente.

La seconda è la chiusa dell’articolo.

>Esaminare Cyclone rende molto chiara una cosa: la concorrenza dei produttori di processori ultramobile non ha mirato abbastanza in alto. Mi chiedo che cosa accadrà al prossimo giro.

Ricordarsene, quando si scatena la discussione sull’Android che costa meno.