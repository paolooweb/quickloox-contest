---
title: "La carica dei cento milioni"
date: 2023-06-21T13:51:59+01:00
draft: false
toc: false
comments: true
categories: [Software, Internet]
tags: [Hofstadter, Douglas Hofstadter, Temi Metamagici, Metamagical Themes, Barr, Avron Barr]
---
Gira una notizia qualunque che nemmeno sto a cercare: l’intelligenza artificiale aiuta gli archeologi a trovare qualche oggetto di grande importanza, o una tomba, o circa.

Subito si levano i commenti del partito del chatbot più in voga: *visto che l’IA non è dannosa? Visto che cosa può fare l’IA?*

Premesso che il fastidio per l’abbreviazione ossessiva supera di gran lunga quello per la difesa di ufficio contro un’accusa che nessuno ha formulato (anzi), leggendo qualcosa salta fuori che alla fine gli archeologi hanno lavorato sui dati dell’aerofotogrammetria, hanno applicato tecniche di machine learning per esaltare i dati significativi e grazie a questo lavoro è arrivata la scoperta.

Gli archeologi lavorano così da quando esiste la fotografia aerea e in questo caso hanno usato il machine learning per enfatizzare caratteristiche del terreno che avrebbero potuto suggerire la presenza di spazi sotterranei. Altre volte è capitato che si usasse una elaborazione a falsi colori (in astronomia è la norma). Non c’è alcune intelligenza artificiale.

Per punizione, il mondo si becca un frammento dal mio Hofstadter quotidiano, che dovrebbe provenire da [Temi Metamagici](https://www.ebay.it/itm/312886190300) (penso non vanga ristampato da anni).

Hofstadter si riferisce a una frase pronunciata dal ricercatore [Avron Barr](https://exhibits.stanford.edu/ai/catalog?f%5Bauthor_person_facet%5D%5B%5D=Barr%2C+Avron%2C+1949-&view=list):

>Qualsiasi cosa interessante accada nel campo della cognizione, avviene sopra la quota dei cento millisecondi; il tempo che ci serve per riconoscere la mamma.

(Cento millisecondi sono un decimo di secondo).

E la capovolge:

>Qualsiasi cosa interessante accada nel campo della cognizione, avviene sotto la quota dei cento millisecondi; il tempo che ci serve per riconoscere la mamma.

Poi rincara la dose:

>Secondo me la domanda più importante in intelligenza artificiale è questa: Che cosa ci permette di convertire il valore di centomila punti sulla retina nella parola “madre” in un decimo di secondo?

Per lui, il problema della percezione è fondamentale nella struttura della nostra intelligenza, intesa come insieme fisico e logico.

Avevo fotografato la pagina rilavante del libro. Ora, da Foto, ho copiato e incollato il testo della pagina selezionandolo dentro la foto.

Questo sarebbe impossibile senza il machine learning, che Apple usa ovunque a piene mano senza strombazzare troppo. Machine learning preziosissimo, mi ha fatto risparmiare tempo e fatica in modo elegante.

Quello che voglio, però è la libertà di chiamare *intelligenza* qualcosa di collegato a quei cento milioni di punti sulla retina senza dovermi mescolare con quelli che gli basta un po’ di fotoelaborazione.