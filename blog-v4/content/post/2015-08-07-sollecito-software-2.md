---
title: "Sollecito software / 2"
date: 2015-08-07
comments: true
tags: [Wesnoth, C++, WML, Python, Steam]
---
Dopo la lancia di ieri per [LibreOffice](https://macintelligence.org/posts/2015-08-06-sollecito-software-1/), oggi sono su un altro mio pallino storico, [Battle for Wesnoth](http://www.wesnoth.org).<!--more-->

Il progetto è oramai da più di dieci anni una bella realtà. È anche cresciuto e serve aiuto. Che può essere finanziario oppure anche – soprattutto – produttivo, creativo, logistico. Sul sito vengono forniti esempi di necessità primarie:

>Programmatori C++ di valore tra intermedio e avanzato, in special modo per OS X e Windows.

>Programmatori Python disposti a occuparsi degli strumenti di manutenzione della base di codice.

>Programmatori WML dediti ad alcune delle campagne giocabili e attualmente prive di curatori: Heir to the Throne, Dead Water, Delfador’s Memoirs, Liberty, Legend of Wesmere, The Sceptre of Fire, The Rise of Wesnoth, The South Guard, Eastern Invasion e Son of the Black Eye. In particolare, Legend of Wesmere ha urgente bisogno di testing e sistemazione del porting multiplayer nelle versioni 1.12.x e 1.13.x, o rischia di essere eliminata in una futura versione del ramo stabile.

(Lo sviluppo di Battle for Wesnoth procede su due binari paralleli, una versione stabile da proporre ai giocatori e una di lavoro su cui i programmatori applicano sistemazioni e aggiunte).

(In agosto, sì, trovo adeguato esortare a giocare a Legend of Wesmere dentro Battle for Wesnoth, e giudicare se non sarebbe un peccato la sua eliminazione).

Ci sono strategie per il futuro – come una versione gratis del gioco da distribuire su [Steam](http://store.steampowered.com) – però servono braccia e menti.

All’internettiano quadratico medio risulta incomprensibile che qualcuno voglia occuparsi gratis di un gioco praticato da chissà chi e senza neanche vantarsene su Facebook. Per questo, almeno nel mese balneare, va detto, scritto, rilanciato.