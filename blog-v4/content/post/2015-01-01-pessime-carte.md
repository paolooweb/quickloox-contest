---
title: "Pessime carte"
date: 2015-01-03
comments: true
tags: [Pgp, Gpg, GpgTools]
---
Ho cambiato operatore *mobile*. È arrivato un totale di venti fogli di carta in ordine sparso, qualcosa da leggere scritto in corpo tre, qualcosa da firmare e reinviare per fax (!), per posta elettronica dopo avere firmato e scandito o con qualche altro accrocchio.<!--more-->

Posso capire l’esigenza di avere una identificazione accurata e univoca, l’idea che un contratto possa essere complicato, che ci sia da rispettare una legge sulla *privacy* anche se fa schifo, che occorra riempire moduli per ottenere un servizio.

Ho grande amore per la carta, sono ancora pieno di libri tradizionali, amo sfogliare il quotidiano. Il lavoro però chiede efficienza e velocità.

Ciò detto, perché la carta? Mettere il modulo su web, inviare un contratto in Pdf se non in formato testo, *costa meno*, all’azienda e al consumatore. La tecnologia di identificazione esiste da anni e anni, [chiave pubblica e chiave privata](http://it.wikipedia.org/wiki/Crittografia_asimmetrica), permette di autenticare con certezza sia il contenuto di una comunicazione sia il mittente della comunicazione stessa. E anche di assicurare confidenzialità assoluta. Il costo di detta tecnologia è alto a piacere, però parte da [zero](https://gpgtools.org) su Mac e da [4,49 euro su iOS](https://itunes.apple.com/it/app/opengp/id414003727?l=en&mt=8).

Non è sufficiente? Si aggiunga un sistema supplementare di autenticazione a due fattori, con l’Sms, il *token* generato dal consumatore con macchinetta come fanno certe banche, una tessera con *chip* come già sono le tessere sanitarie…

Quello che si vuole, ma con tutte queste carte in mano si vorrebbe cambiare gioco. Posso pagare le tasse con un iPhone e si è detto tutto, parlando di uno Stato per il quale un codice fiscale ottuso e difettoso viene a volte chiesto per identificazione; perché non posso concludere un contratto usando un iPhone?

Tutto quello che serve è scambiarsi le chiavi pubbliche. Qui sotto, la mia. Può essere affissa su tutti i muri d’Italia senza ledere di un milionesimo la mia *privacy*, eppure basta cambiarle un bit per sapere con certezza che non è più la mia. Il codice che la elabora è aperto e si può accertare che si comporti pulitamente. Qual è il problema?

-----BEGIN PGP PUBLIC KEY BLOCK-----
Version: GnuPG/MacGPG2 v2.0.17 (Darwin)
Comment: GPGTools - http://gpgtools.org

mQGiBEpu2zERBADY/9KW8ZalSZ4PAb2KTr5pG5TL25oV3p0g6QKgm/tGrZkEAGQc
3AwvWdM2puscY3IgitL9NjNm5Upghu4XsiFWlvoXYsxd2gnzRToEaAJMlI7CH254
6xIzGbXFT2LKrQMizVkiSAxqufsBkMtwinvflQEuBA1Cb4hsOqOu9XLs+wCg5sgD
5WlADmiQGgKWSR/pOZreUpUD/R/UZnZ/Ttzdl6HAe5UUiqtF3VM+ecllmpvEJabk
6WnzrFZA/Y1TnRYnp+wGR0kztXYmDvb9dMbkbZ4aOE06RmME/Gl9S3zX5+yrYkRH
J8RsOXSqhaqjAjqT23qshrZXUB5datOQfI8yXKjLpowvrekSP+o7egSu3lEwvKhs
mMO8BACLFq7+iOzZR9lyC8DaZeP40PslfJJprZ9egEUV7NThfIkboi+icr8+Pgte
aq5ihOPgOyt3XHAYzbH1ioojGpklDtaWionFmJ7zFAMpu8QxtZJEOldCIf9ZckuQ
/DH7AhWxbYnefrYfcXoMOgFdtw4gwmWYddRJa8iRhl7jxFEkkbQ8THVjaW8gQnJh
Z2Fnbm9sbyAoTGEgZmFtb3NhIGFkZGl6aW9uZeKApikgPGx2Y2l2c0BnbWFpbC5j
b20+iFsEExECABsFAkpu2zEGCwkIBwMCAxUCAwMWAgECHgECF4AACgkQp1WZdwdk
5rQHUQCdHiLzw6VOl2NabO5QUw1LP1wr8GkAoK9PyyPGdqc9udzakrnijUgUN8+s
uQINBEpu210QCACIzJgVUUqxkuwHpoB7GZmWufzc3l75DKISYpZcJZlEvbn1QBiS
NpE5CnoRaUQaVYs+IUpo+GuYKTCSvDRaJ2uHWvSOhBZbhPocLFlGKFfLHLAMthG0
GpopH/tFOBPYDEw9OUY2c7PZgFmR5ZsJCNA/BKQuaFE5/8tqJalgwPEdA2+JxCLL
z5L0axfBqfvWqYVjPFL6MR4Unt4OPeN06KfkDdpOJh0YSF/BBOaGGI7T8zN7HKKL
Sq/zKLb569CgXb8MzYqjzLvnR3gZJxzfQvSPii5QMqdTShuGlixqQe0A1uNEwokN
26jL9ui6pzCBP5c/gOdbxT7K910x3Gc7kctfAAMFB/9coJtpIuHxkaCmowcWjgJG
bRzwowEOYmRz4w59/CQk9HFcWRLRLpmAq9xh0Ne/0t15VEResn2jTj9BjWRzIFf0
G8CB8oZeFwbsf3a+x3cmtqqQIittKxuxIhaQpsbGryOUgdwsCPIgG5sSCSArH8fi
MkwB/HwUMZlD+v273/VGlzraami7UqMeTJ3Wlqx26daWRLLXoVp0gmuXEi4tCY5V
H+AAF9ID+5WcCbHfuZyuKaPnPalWjvNOoLkDmTjlqe3Tqojl2orBWRHVg7DiqTCp
H4KpzOYGAfc1BQmc7zK/JRG0ZrMioqg8XoFtn8dntm77PTu8VFuKvS9g/1ploq0h
iEYEGBECAAYFAkpu210ACgkQp1WZdwdk5rSOmwCfUzaRBfaS6RuqxF4gjMUBkoRp
anEAn2/5fVz3dxsdwYED+C7JNgJV84jP
=0J63
-----END PGP PUBLIC KEY BLOCK-----
