---
title: "Servire, proteggere e riciclare"
date: 2017-08-29
comments: true
tags: [Nypd, Windows, iPhone, NyTimes]
---
La polizia di New York [deve sostituire trentaseimila computer da tasca acquistati negli ultimi due anni](http://nypost.com/2017/08/28/nypd-needs-to-replace-36k-useless-smartphones/) con altrettanti iPhone.

La ragione è che il produttore del sistema operativo che aziona i suddetti apparecchi ha annunciato la fine del supporto. Non verranno più aggiornati. Quel sistema operativo, nel mondo dei computer da tasca, ha una presenza irrisoria e in calo verso lo zero, tanto da farlo dichiarare *morto* da *Forbes*.

Gli apparecchi non sono aggiornabili con una versione più moderna del sistema operativo. Da quando è uscita la versione ora in scadenza, iOS e Android hanno visto tre versioni successive, tutte ampiamente compatibili con apparecchi non nuovissimi.

Una volta si diceva che nessuno era mai stato licenziato per avere scelto Ibm. Al posto di Ibm è entrato nelle aziende per anni un altro nome. Ora questo stesso nome, dopo avere fatto spendere alla polizia di New York 160 milioni di dollari in due anni, offre come unica opzione il riciclo.

A New York dicono che la scelta è stata fortissimamente voluta da Jessica Tisch, responsabile delle scelte informatiche della polizia. Sta tornando dalle vacanze. Sentiremo che cosa ha da dire.
