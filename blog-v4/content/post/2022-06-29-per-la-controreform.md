---
title: "Per la ControReform"
date: 2022-06-29T02:32:32+01:00
draft: false
toc: false
comments: true
categories: [Software, Hardware]
tags: [Reform, MNT Pocket Reform, Mntre, Psion 5, netbook, Playdate, eMate, eMate 300]
---
Va bene che siano open source tanto lo hardware quanto il software, va benissimo che voglia essere completamente riciclabile e riparabile, splendida la modularità e l’aggiornabilità… ma in un mondo di iPhone da sei pollici e passa, di iPad mini e iPad veri e iPad Pro, che ce ne facciamo di [un portatile da sette pollici](https://mntre.com/media/reform_md/2022-06-20-introducing-mnt-pocket-reform.html)?

Molto carino e tenerino, MNT Pocket Reform; uno lo guarda e non vede l’ora di giocarci (nel senso di smanettarci e non solo ludico). Poi pensa di avere già dato con i *netbook*, di quanto era meraviglioso [Psion 5](https://en.wikipedia.org/wiki/Psion_Series_5), di quanto è affascinante [Playdate](https://play.date) e di come si divertirebbe con un [eMate 300](https://www.macworld.com/article/219958/the-forgotten-emate-300-15-years-later.html), per concludere che ogni bella idea ha anche un limite temporale di applicabilità e questo genere di formato, con questo genere di specifiche, sembra campato per aria, o comunque limitato a usi protoesibizionistici.