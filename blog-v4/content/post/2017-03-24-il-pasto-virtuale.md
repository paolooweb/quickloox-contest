---
title: "Il pasto virtuale"
date: 2017-03-24
comments: true
tags: [VirtualBox, Parallels, Desktop, Lite]
---
*Non esiste un pasto gratis*. Se un’azienda ti fa un regalo, c’è un motivo.

Per questo le mie macchine virtuali in [VirtualBox](https://www.virtualbox.org/) sono lente e scomode. Il programma è gratis, i suoi esiti sono più che sufficienti per le mie necessità. Accetto più che volentieri le sue limitazioni.

Senonché, [leggo](http://www.mackungfu.org/HowtocreateamacOSvirtualmachineeasilyandforfree) che in Mac App Store appare [Parallels Desktop Lite](http://mjtsai.com/blog/2017/03/20/parallels-desktop-lite/). È gratis e permette di creare macchine virtuali macOS e Linux. Sembra fatto su misura per me.

A che cosa rinuncio? Beh, a creare macchine virtuali Windows. Per quello occorre un acquisto *in-app*.

La cosa non mi tocca. Per virtualizzare Windows devono pagarmi, non il contrario.

Mi tocca, invece, che occorra *hardware* del 2011 o posteriore. Ho una macchina troppo vecchia, anche se proverò comunque a verificare.

Per me il pasto della virtualizzazione continua a non essere gratis. Ma per tanti altri lo potrebbe diventare.


