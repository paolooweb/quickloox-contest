---
title: "Adesso sì, adesso no"
date: 2017-03-30
comments: true
tags: [iOS, 10.3]
---
Di solito esce un aggiornamento di sistema per iPhone e lo ignoro finché iPhone mi fa notare che è disponibile.<!--more-->

Per 10.3 la curiosità era troppa e ho lanciato Aggiornamento Software su iPhone. Tuttavia il terminale mi ha risposto che il software era allo stato dell’arte: iOS 10.2.1.

Se un iPhone ha montato 10.2, *deve* poter installare 10.3. Ho provato su Mac con iTunes, che ha subito segnalato la disponibilità dell’aggiornamento. La circostanza era così singolare che l’ho immortalata.

 ![iTunes sì, iPhone no](/images/10-3.jpg  "iTunes sì, iPhone no") 

L’aggiornamento è andato liscio come l’olio e ha pure recuperato, a occhio, mezzo gigabyte di spazio. Mi è rimasta però la curiosità: problema occasionale, *bug* nella procedura di installazione, volontà di Apple (magari per cautelarsi vista la [delicatezza dell’operazione](/blog/2017/03/28/il-tappeto-sotto-i-piedi/))? È una cosa che proprio non avevo ancora visto.