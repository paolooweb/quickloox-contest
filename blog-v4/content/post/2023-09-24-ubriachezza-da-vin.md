---
title: "Ubriachezza da Vin"
date: 2023-09-24T18:54:55+01:00
draft: false
toc: false
comments: true
categories: [Hardware]
tags: [Doctorow, Cory Doctorow, Pluralistic, Vin, Vehicle identification number]
---
Avrà ragione, e se sì in che misura, Cory Doctorow nella sua invettiva contro Apple che [nega il diritto alla riparazione a colpi di Vin locking](https://pluralistic.net/2023/09/22/vin-locking/)?

Per proseguire bisogna sapere che Vin sta per *Vehicle identification number* e si riferisce alle codifiche che i costruttori inseriscono nella componentistica dei loro apparecchi – qualunque cosa, dalle auto fino ai ventilatori – per combattere le falsificazioni e la pirateria ma anche controllare il business della riparazione e dei ricambi.

Succede che oggi il codice Vin sia accompagnato sempre più spesso da un chip, [anche sulle parti più microscopiche](https://repair.eu/news/apple-uses-trademark-law-to-strengthen-its-monopoly-on-repair/). La scheda madre, se c’è un accelerometro (per dire) nell’apparecchio, si aspetta di poterci parlare. Se l’accelerometro arriva da qualche altra fonte non autorizzata o manca di chip, ecco che la scheda madre si inquieta e l’apparecchio non funziona.

Il codice Vin applicato a tappeto significa l’obbligatorietà pratica di inserire parti originali quando si effettua una riparazione o sostituzione. Chiaro che ciò complichi la vita a numerosi operatori indipendenti privi del beneplacito dalla casa madre (ripeto, vale per un televisore come per un aspirapolvere).

Doctorow sostiene che Apple voglia monopolizzare il settore della riparazione – ci posso credere – e che si preoccupata dal fatto che la gente non compra più gli smartphone (quindi, per conseguenza, preferisce riparare quando c’è un guasto).

L’informazione arriva da una [lettera inviata da Tim Cook agli investitori](https://www.inverse.com/article/52189-tim-cook-says-apple-faces-2-key-problems-in-surprising-shareholder-letter), solo che qui Doctorow applica [una delle sue tattiche](https://macintelligence.org/posts/2023-02-21-i-libri-so-piezz-e-cory/) preferite: inizia un argomento con qualche fonte cogente e approfondita, per corroborare la tesi con altre informazioni di contorno, che però sono meno cogenti, non sempre approfondite, a volte false o mezze false.

La lettera di Tim Cook infatti descrive la situazione del mercato e le numerose e varie difficoltà che bisogna superare quotidianamente per condurre un buon business di vendita di iPhone. Tra queste c’è anche il fatto che molti utenti preferiscono cambiare la batteria che comprarsi un telefono nuovo. Ma nel complesso la gente non ha affatto smesso di comprare smartphone, se non altro perché la lettera data al 2019 e a gennaio farà cinque anni di vita. Nel mentre, qualche iPhone lo si è venduto e tanta situazione di contorno è anche cambiata, vuoi in meglio, vuoi in peggio.

C’è anche il fatto che, per esempio, su iPhone 15 il microfono sul lato inferiore sia diventato riparabile. Prima non lo era e in caso di problemi al microfono bisognava cambiare l’intera sezione inferiore.

A chi giova? Magari sul microfono c’è un codice Vin e si può sostituire solo con una parte certificata Apple. Certamente è un (piccolo) colpo alla frase *costa meno comprarne uno nuovo*; non credo proprio che cambiare il microfono costerà come cambiare un pezzo di iPhone.

Per concludere questa prima parte, ho l’impressione che le tematiche descritte da Doctorow certamente esistano, ma siano meno a senso unico di come le presenta.

C’è una seconda parte, riguardante [l’impegno di Apple nel distruggere i vecchi apparecchi](https://www.vice.com/en/article/yp73jw/apple-recycling-iphones-macbooks).

Secondo Doctorow, si tratta di una mossa spietata e cinica per opprimere il mercato della riparazione e creare o rafforzare un monopolio.

Da una parte sì, dall’altra c’è la questione del riciclo. Nel lanciare iPhone 15, [è stata messa molta enfasi sul recupero di metalli](https://macintelligence.org/posts/2023-09-13-come-natura-vuole/) e altri materiali nella fabbricazione di nuovi apparecchi. Per esempio, una frazione percepibile del cobalto presente nella batteria di iPhone 15 è riciclata e l’azienda [ha annunciato che arriverà nel giro di un paio di anni, se non erro, al cento percento nei componenti chiave](https://www.apple.com/environment/pdf/Apple_Environmental_Progress_Report_2023.pdf). Non sono esperto di riciclo e potrei sbagliarmi grossolanamente, ma immagino di poter riciclare il cobalto dentro una batteria nel modo più veloce con una macchina che fa a pezzi la batteria e poi filtra per peso o per altre caratteristiche i metalli desiderati. Smontare la batteria non mi sembra una opzione praticabile su grandi numeri.

Doctorow scrive in pratica che Apple preferisce infilare il cobalto di una vecchia batteria dentro una batteria nuova invece di lasciare che quella batteria entri nel circuito dell’usato e inizi una seconda vita. Cattiva Apple ed egoista, pensa solo al profitto e non all’ambiente.

Ho un solo problema: parliamo delle stesse batterie? Apple produce più di dieci milioni di iPhone al mese. Quanti iPhone vengono riparati in un mese? Sospetto che siano meno. Sospetto che la distruzione di vecchi apparecchi per il recupero dei materiali non sia una alternativa al riutilizzo per la riparazione, ma un percorso parallelo e anche fatto di numeri molto più importanti.

L’ultimo link qui citato porta all’Environmental Progress Report 2023 di Apple. Non si parla sicuramente delle problematiche della riparazione, ma c’è tanto ambiente. La multinazionale ha come evidente scopo il profitto e non può essere diversamente, tuttavia è anche sufficientemente intelligente per capire i vincoli cui deve attenersi per muoversi in cerca del profitto. Fatta la tara a tutto il *greenwashing* possibile, non vedo una Apple che distrugge apparecchi con il solo scopo di fregare i riparatori indipendenti.

Uno dei miei primi lavori di *copywriter* fu una pubblicità di Apple Italia per un nuovo toner di LaserWriter, che aumentava le gradazioni di nero a disposizione ed era più riciclabile.

Scrissi – e passò – *stampiamo i grigi pensando al verde*. Sono passati trent’anni e l’attenzione alle tematiche verdi, di forma o di sostanza, era viva già allora. Il beneficio del dubbio, con il dovere dell’approfondimento e della verifica, lo adotterei, evitando toni un po’ troppo radicali e ad alto tasso alcolico.