---
title: "La riparabilità nel 2018"
date: 2018-07-20
comments: true
tags: [MacBoom, Pro, iFixit]
---
iFixit assegna ai MacBook Pro ultimo modello [un punto su dieci](https://www.ifixit.com/Teardown/MacBook+Pro+13-Inch+Touch+Bar+2018+Teardown/111384) a proposito di riparabilità.

Vivo entro un’ora da un Apple Store, per non parlare dei rivenditori autorizzati, e ho la sensazione che nell’anno 2018 si tratti di qualcosa di sopravvalutato.

Scrivo queste note circondato da giochi per neonati e bambini. Molti di questi hanno un indice di reparabilità simile o forse inferiore; le viti richiedono un cacciavite a sezione triangolare, sono incassate nella plastica ed è probabile che toglierle provochi la rottura di qualche pezzo.

Ma poi vorrei sapere chi compra le scarpe in base alla riparabilità. Scommetto che chiunque le porta a riparare da uno specialista.

E lo spazzolino da denti? Le infradito? Il televisore ultrapiatto?

Sì, sono esempi parzialmente provocatori. Io però comprerei un MacBook Pro per quello che può fare, non per quello che può fare il Genius bar.