---
title: "Un futuro non compresso"
date: 2019-12-14
comments: true
tags: [Mac, Pro]
---
È apparsa ironia varia sulle opzioni di espansione di un Mac Pro che possono portare il prezzo sopra i cinquantamila euro. Trovo che [la risposta più seria](https://macdailynews.com/2019/12/10/apples-new-mac-pro-can-cost-52000-not-counting-its-optional-400-wheels-apple-hardware-purchases-on-apple-card-doubled-to-6/) sia stata scritta da *MacDailyNews*:

>È un Mac professionale, non un giocattolo. Queste macchine sono per professionisti che spendono molto, molto più del costo di un nuovo Mac Pro e di  Pro Display XDRs in fotocamere, treppiedi, illuminazione, equipaggiamento audio, tecnici eccetera. C’è stato un tempo in cui compravamo sistemi Avid Media Composer e Symphony che costavano con facilità l’equivalente di due, tre o anche quattro Mac Pro di oggi superaccessoriati e anche muniti di stand Apple Pro per gli schermi. Se devi chiedere quanto costa, Mac Pro non è per te; si presume che a te serva un iMac, un Mac mini o un MacBook Air.

Ho sentito varie volte commentare *oramai siamo al top, che altro vuoi mettere in un computer?* e invariabilmente, anno dopo anno, abbiamo visto computer sempre più potenti e sofisticati del momento in cui pareva di avere toccato il punto più alto, insuperabile.

Stavolta ho sentito una novità: *chi ha bisogno di una macchina del genere per editare audio o video, che tanto li guardano con un telefono?*

Anni fa, di fronte all’ennesimo *non c’è più niente di nuovo da mettere in un computer*, profetizzai *arriveremo alla fine della crescita dell’informatica quando sparirà la compressione dei dati* e non era del tutto una sciocchezza.

Abbiamo computer da tasca che fotografano e filmano con raffinatezza impressionante e poi… producono una immagine Jpeg. Sistemi audio mai così evoluti, alle prese con Mp3. In compenso, chi lavora davvero professionalmente si ritrova con Raw da centinaia di megabyte, foto misurate in gigapixel, video che vanno ottimizzati per gli 8k e quindi vanno ripresi in qualità superiore per poterlo fare in tranquillità. Se Mac Pro può lavorare con mille tracce audio contemporanee non è per vantarsene al bar: qualcuno, magari a Hollywood per carità, ma qualcuno ne avrà davvero bisogno. E potrebbe avere da spendere centomila dollari in hardware all’interno del budget di una produzione che conta di incassare milioni.

In altri comparti, ci sono ricercatori e sviluppatori che aspettavano una macchina da riempire di Ram ultraperformante per fare cose turche con macchine virtuali locali, o calcoli che non vogliono o non possono eseguire nel cloud.

Insomma, negli ambiti professionali di fascia molto alta abbondano (relativamente al proprio campo) persone che hanno bisogno di potenze disumane per lavorare su file immensi. Il fatto che poi questo ricada in uno streaming supercompresso di Spotify è una questione differente.

Tra vent’anni avremo AirPods o chi per loro, forniti della potenza di elaborazione di un iPhone attuale. Ciononostante, la grande maggioranza delle persone continuerà a usare dati compressi. Di converso, serviranno Mac Pro da cinquecentomila dollari.

E non mancherà il rosicone che ci ironizza sopra.