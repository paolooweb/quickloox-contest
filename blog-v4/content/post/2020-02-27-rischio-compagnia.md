---
title: "Rischio compagnia"
date: 2020-02-27
comments: true
tags: [MacAdmins, Slack]
---
Dopo un quarto di secolo di Internet e poco meno di web, per trovare buone comunità online quasi sempre si finisce per passaparola; Facebook è poco efficace e anche quando si incappa in qualcosa di potenzialmente valido, spesso prevale il clima da social, con le battute salaci, la polarizzazione, le cazzate un tanto al chilo, quelli arrivati da Marte e chi riesce a impiegare solo frasi fatte.

I gruppi Slack sono tipicamente buoni; anche solo per il fatto di usare Slack, cioè un programma diverso dall’ovvio e dal banale, equivale a una scrematura.

Or ora ho trovato un gruppo [MacAdmins](http://macadmins.slack.com), di amministratori di sistema Mac, come dice il nome.

Americano, molto numeroso, alla prima occhiata forse eccessivamente colloquiale, tuttavia conto di fermarmi almeno qualche giorno a vedere. Potrebbe essere compagnia di poco conto giornaliero che si rivela utile quando serve aiuto, oppure i temi interessanti potrebbero apparire a intermittenza. Sto a vedere.

L’adesione è libera e apparentemente basta chiedere l’iscrizione per essere iscritti d’ufficio.

Per inciso, c’è [un gruppo Slack di frequentatori di questo blog](http://goedel.slack.com). Ringrazio ogni giorno le persone straordinarie che lo animano e spesso lo popolano di contenuti assai interessanti. È aperto a tutti, basta chiedere, per esempio lasciare un commento qui sotto con una email cui essere invitati.

Un altro vantaggio rispetto alle reti sociali, dove si vede talmente tanto baccano da trovarsi soli; su Slack la compagnia è tipicamente buona e fresca. Un rischio che finora nel mio caso è sempre valso la pena correre.