---
title: "C’è sempre un perché"
date: 2013-03-20
comments: true
tags: [Microsoft, Windows, LaTeX, Programmazione]
---
Il software – piccolo o grande – che si distingue ha talvolta alle spalle una storia che fa capire subito il motivo della distinzione.<!--more-->

Un esempio grande: Donald Knuth creò <a href="http://tug.org">TeX</a> (da cui arriva LaTeX più o meno per tutti, <a href="http://tug.org/mactex/">anche su Mac</a>) perché <a href="http://tug.org/whatis.html">era profondamente insoddisfatto</a> della qualità delle bozze del secondo volume di <a href="http://www-cs-faculty.stanford.edu/~uno/taocp.html">The Art of Computer Programming</a>. Si poteva fare meglio e lo ha fatto.

Un esempio più piccolo: Loren Brichter ha avuto l’idea di <a href="https://itunes.apple.com/it/app/letterpress-word-game/id526619424?l=en&amp;mt=8">Letterpress</a> mentre <a href="http://mashable.com/2012/10/24/loren-brichter-letterpress/">praticava un *word game* con la moglie</a>. Una idea nata dal divertimento e dal divertimento comune, di cui si capisce facilmente il perché del successo. Solo che Letterpress conquista anche per la grafica e per l’esperienza di uso; il programma ispira naturalezza e simpatia.

Per arrivarci, Brichter <a href="http://gigaom.com/2012/12/03/loren-brichter-designs-on-the-future-of-ios-apps/">ha riscritto</a> lo *user interface framework* di iOS, il codice che dialoga con il processore grafico. Non aveva bisogno di farlo: Apple ne fornisce uno incorporato, eccellente, che tutti i programmatori possono usare liberamente. Ma lui voleva fare qualcosa di diverso. Lo ha fatto.

Oppure prendiamo <a href="https://itunes.apple.com/it/app/petting-zoo-by-christoph-niemann/id602773895?l=en&amp;mt=8">Petting Zoo by Cristoph Niemann</a>. È una *app* deliziosa, divertente, poetica, fantastica se per casa gira un bimbo. Come è nata? L’autore lo racconta in un <a href="http://www.newyorker.com/online/blogs/culture/2013/03/christoph-niemann-petting-zoo-app.html">articolo sul New Yorker</a>, farcito di vignette, animazioni, disegni, filmati. Anche senza leggere e avere a che fare con l’inglese, la parte visiva è una *app* a parte quasi da sola (su Mac, se privi di Flash, occorre <a href="https://www.google.com/intl/it/chrome/browser/">Chrome</a> per vedere tutto). Il motivo di creare una *app* era la voglia di sperimentare. È stato fatto.

Quando dietro c’è un motivo interessante, può darsi che esca una *app* interessante.

Chi si avventurasse su Windows 8 e Windows Phone 8, sappia che alcune <i>app</i> nascono con un motivo ben preciso: Microsoft <a href="http://allthingsd.com/20130320/microsoft-says-paying-developers-not-its-main-strategy-for-getting-more-windows-apps/">compensa il programmatore con cento dollari</a> ad <i>app</i>.