---
title: "La parte per il tutto"
date: 2014-04-09
comments: true
tags: [Lightroom, Adobe, iPad, InDesign]
---
Ero alla presentazione in anteprima per l’Italia di [Lightroom per iPad](https://itunes.apple.com/it/app/adobe-lightroom/id804177739?l=en&mt=8) e quello che ho visto mi è piaciuto. Ho una debolezza per il contesto: Adobe Italia manda per queste cose un tecnico bravo e simpatico e l’agenzia di relazioni pubbliche che coordina fa sempre le cose nel modo giusto.<!--more-->

Detto questo, normalmente sono poco attirato dai prodotti Adobe perché li ho sperimentati a lungo su computer: sono molto potenti e molto macchinosi, ottimi per professionisti che ne fanno lo strumento di lavoro principale, meno per quelli che come me possono averne occasionalmente bisogno e trovano un prezzo che vale poco la candela – anche se [Creative Cloud](https://creative.adobe.com/plans/offer/photoshop+lightroom?store_code=it&locale=it&promoid=KIPGX) ha migliorato la situazione – in cambio di un’interfaccia che richiede preparazione ed esperienza per restituire davvero l’investimento.

Su una tavoletta le cose cambiano (Lightroom è uscito solo per iPad e più avanti comparirà per iPhone e Android). Adobe si è resa conto della differenza di contesto e di aspettative e ha fatto le cose per bene.

Lightroom per iPad non sostituisce quello da scrivania ma lo complementa e consente sia di organizzare sia di effettuare correzioni veloci a foto e immagini, che vengono sincronizzate a piacere (volendolo) non appena vi sia collegamento. La cosa interessante è che è stato eliminato il problema dei trasferimenti frequenti di file tra iPad e Mac: chi scatta foto Raw a livello professionale (e anche da appassionato) riempirebbe in brevissimo tempo lo spazio a disposizione della tavoletta, senza pensare ai tempi imposti dall’Internet italiana, la peggiore in Europa fatta eccezione per Malta.

Invece quelli che viaggiano sono solo i metadati, le informazioni numeriche riguardanti le caratteristiche della foto. Su iPad si lavora sopra un’anteprima, veloce e maneggevole (infatti il programma è compatibile anche con un iPad 2 purché equipaggiato con iOS 7); arrivando a casa o in studio, ritroveremo su Mac le immagini alla risoluzione giusta, sistemate come lo volevamo.

L’approccio di Adobe lascerà insoddisfatti solo coloro che puntano a iPad come sostituto di Mac. Lightroom per iPad infatti, senza l’intesa con l’edizione da scrivania, è pressoché inutile. Per tutti gli altri – possessori di Lightroom iscritti a Creative Cloud – sarebbe sciocco rinunciare a una comodità in più, funzionale, ben progettata e gratuita.

L’idea che iPad possa lavorare su un sottoinsieme dei dati originali, guadagnando velocità ed efficacia grazie alla sincronizzazione puntuale con il desktop, è ottima e andrebbe ripresa in tanti altri ambiti, dal video al 3D alla musica. per non parlare dell’impaginazione: un InDesign per iPad, capace di prendere l’essenza di un *layout* di testo e grafica e consentire correzioni immediate anche lontani da Mac, potrei adottarlo persino io.