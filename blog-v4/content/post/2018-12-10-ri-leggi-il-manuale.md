---
title: "Ri-leggi il manuale"
date: 2018-12-10
comments: true
tags: [Macintosh]
---
Agli amici che negli anni si sono lamentati dell’aumentata complessità dei Mac e dei sistemi Apple in generale, diversi da quelli senza manuale… devo ricordare che [il manuale c’era](https://www.peterme.com/2007/08/27/thoughts-on-and-pics-of-the-original-macintosh-user-manual/).

Non che fosse come gli altri manuali in giro al tempo. Tuttavia Macintosh aveva il manuale d’uso e di conseguenza qualcuno che aveva bisogno di leggerlo.

Speciale, comunque. Un’occhiata alle foto e ai testi ricordati in questo articolo mostra che, già allora, le cose si facevano in modo diverso da quello della concorrenza.