---
title: "La forza che viene da dentro"
date: 2021-10-19T00:41:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [M1, M1 Max, M1 Pro, Apple, Tim Cook, Apple Park, TouchBar, MagSafe, MacBook Pro] 
---
Una [presentazione](https://www.apple.com/apple-events/october-2021/) durata neanche un’ora, aperta da Tim Cook in mezzo a un’area incolta di Apple Park: forse è finita la benzina? Una cosa fatta in fretta? Gli iPad Pro non sono venuti pronti in tempo?

In un altro stacco si vede benissimo la zona di Apple Park perfettamente ordinata. E nessuna inquadratura è lasciata al caso. L’ho vista come un invito a trascurare la forma e badare alla sostanza.

La sostanza è tutta nei nuovi system-on-chip, M1 Pro e M1 Max. Poi bisognava parlare anche di computer, perché un processore è utile solo quando lo si può dirigere. Così, dopo una spruzzata di musica, è stato tutto MacBook Pro.

Qualcuno è dispiaciuto, io per primo, perché è stata terminata la TouchBar. Mi sa che siamo in pochi. L’idea era tutt’altro che sciocca, ma non ha avuto seguito presso gli sviluppatori e pragmaticamente è stato riconosciuta la mancanza di trazione. In compenso tornano MagSafe (alla grandissima) e il vero successore del mitico MacBook Pro 17”, dato che il 16” Intel era percepibilmente una bellissima macchina ma anche un riempitivo nell’attesa.

Ed eccola terminata, l’attesa. Il salto dal primo M1 è importante e M1 Max arriva a triplicare i transistor, a quota cinquantacinque miliardi.

Sono processori destinati a disorientare i so-tutto e quelli che sono obiettivi e sopra le parti, quindi usano Windows. Appena ti azzardi a dire qualcosa sulle prestazioni, quelli consumano qualche decina di watt in meno; se per caso alzi un dito a proposito dei consumi, ti annichiliscono con le prestazioni.

Apple fa il pesce in barile e se la gode; si balocca con le *performance per watt*, una nozione incomprensibile a qualsiasi uomo della strada, e intanto rende chiaro che queste macchine trinciano qualsiasi PC portatile *mainstream*. Il prezzo? Non ci sono particolari novità rispetto al solito e, comunque, il punto è che a parità di prezzo non si trova oggi una macchina concorrente in grado di offrire insieme le stesse prestazioni e gli stessi consumi.

M1 è stato una sorpresa, ma Pro e Max sono due mazzate.

Dopo anni che si lamentava l’abbandono dei *pro* da parte di Apple, è difficile pensare a un portatile diverso dai MacBook Pro visti ieri per un *videomaker* o un modellatore 3D in cerca di portatilità.

Il Mac poi era trascurato, eh? Apple pensava solo a iPhone. L’attenzione ai servizi? Ormai è una macchina per fare soldi, non c’è più innovazione.

Sono curioso di vedere con che altro hardware verranno confrontati, e con che coraggio.

Ora si può procedere con le battute sugli HomePod mini colorati o sul rene da vendere per avere un MacBook Pro (conosco gente che è arrivata al dodicesimo). Intanto, lì dentro, ci sono ricerca, tecnologia ed eccellenza che nessun altro è riuscito a portare a questo livello. Mentre, con la mano sinistra, elaborano il nuovo profilo di utenza di Apple Music.

Tanto di cappello alla sostanza e della forma chissenefrega. Per Apple Park la tecnologia più avanzata che può servire è al più il tagliaerba.

*Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile.*