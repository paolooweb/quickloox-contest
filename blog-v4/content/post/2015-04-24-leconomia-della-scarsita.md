---
title: "L’economia della scarsità"
date: 2015-04-26
comments: true
tags: [ watch, Hours]
---
Segnalo che la [guida di Apple a watch](http://help.apple.com/watch/) è un documento che aiuta davvero a capire che cosa sia l’oggetto e a valutare se valga la pena di considerazione senza bisogno di andare per forza in visita all’Apple Store.<!--more-->

Noto che stanno succedendo cose interessanti sul fronte software: per esempio, il software per iPhone di tracciamento dei tempi di lavoro [Hours Time Tracking](https://itunes.apple.com/it/app/hours-time-tracking/id895933956?l=en&mt=8) [è diventato gratis](https://medium.com/the-hours-blog/hours-the-apple-watch-and-turning-an-app-into-a-businesst-b2b218899a91) per un tempo limitato proprio sulla spinta della comparsa di watch. L’idea è che il computer da polso diverrà a breve molto più comodo di un iPhone negli ambienti in cui si prende nota dei tempi di lavoro e da qui i produttori di Hours hanno deciso di rivoluzionare il loro modello di business e puntare al bersaglio grosso senza più accontentarsi di una *app che ha venduto decine di migliaia di copie a 6,99 dollari*.

Suppongo però che la notizia più ascoltata riguarderà [l’apertura di App Store per watch](http://blogs.wsj.com/digits/2015/04/23/as-first-watches-ship-apple-opens-app-store-for-new-device/), con la disponibilità iniziale di tremila *app*.

Non c’è momento migliore per commentare negativamente il futuro di watch, con una argomentazione collaudata e vincente presso qualsiasi macchinetta del caffè: ha pochi programmi.
