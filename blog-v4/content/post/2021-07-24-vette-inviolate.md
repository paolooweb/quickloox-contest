---
title: "Vette inviolate"
date: 2021-07-24T00:07:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [Mappe, Cessapalombo, Collegiacone, Colle Giacone, Casa Graziana, AirBnB, Giardino delle Farfalle, Monti Sibillini] 
---
In visita ad amici che lavorano per promuovere su AirBnB [una bella casa sulle colline umbre](https://www.airbnb.it/rooms/50186011) (a un quarto d’ora da Terni, a mezz’ora dalle Marmore eccetera), ho seguito fedelmente le indicazioni di Mappe. Quando però, negli ultimi cinquanta metri, ho visto il segnale per Colle Giacone, l’ho seguito ignorando il navigatore. Che, a dargli retta, mi avrebbe portato, bontà sua, sulla sommità del Colle Giacone, il colle che condivide il toponimo con il paese. Una cima dove arrivare in macchina, perlomeno con un’auto ordinaria, è proprio impossibile.

Una storia banalmente aneddotica che può capitare a tanti. Un’imprecisione di Mappe che non offusca migliaia di chilometri indicati con grande precisione. Il fatto è che, sopra Cessapalombo sui monti Sibillini, mi è capitata [la stessa cosa tre settimane fa](https://macintelligence.org/posts/Scendere-in-campo.html). In quella occasione è andata meno bene; non mi sono accorto della segnaletica, ho seguito Mappe oltre l’incrocio decisivo e sono finito su uno sterrato che sembrava del tutto lecito salvo trasformarsi in una trappola, dalla quale sono uscito con grande fortuna e con una nuova coppa dell’olio.

Due casi sono pochi per cominciare una serie; nondimeno, invito alla prudenza al momento di concludere itinerari appenninici. E se il segnale finale contraddice Mappe, ha ragione il segnale. Mappe, da lì in avanti, lasciamolo agli escursionisti.

*Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile.*