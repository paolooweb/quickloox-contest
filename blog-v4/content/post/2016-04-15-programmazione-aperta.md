---
title: "Programmazione aperta"
date: 2016-04-15
comments: true
tags: [VB, Swift, Rust, Freelancer, GitHub]
---
Di tutti i posti nei quali uno si aspetta di sentire parlare del nuovo linguaggio di programmazione di Apple [Swift](https://swift.org), l’ultimo era il rapporto [Fast 50 2015](http://www.apogeonline.com/webzine/2016/04/14/voxxed-days-ticino-2016) di Freelancer, che parla delle offerte di lavoro per liberi professionisti depositate sul proprio sito.<!--more-->

Dove le offerte di lavoro connesse a Swift sono aumentate del 566 percento rispetto al 2014.

Facile capire perché: alla nascita, Swift non era *open source* e poi invece lo è diventato.

Ci sono altre conseguenze, come il fatto – sorprendente – che su GitHub Swift abbia il maggior numero di favori accordati che ogni altro rivale. Il distacco tra Swift (oltre ventimila) e il secondo in classifica, [Rust](https://www.rust-lang.org) (14.434) è incredibilmente ampio.

Swift potrebbe diventare un punto di riferimento notevole nei prossimi anni e la sua attrattiva è in crescita. Pensare che non ha neanche due anni fa impressione, ha letteralmente la vita davanti per essere un linguaggio di programmazione. 

Fa impressione, ricordiamo, anche il fatto che in tante scuole italiane si pensi di preparare i ragazzi al futuro [con Visual Basic](https://macintelligence.org/posts/2014-10-29-qualcosa-di-meno-basic/).