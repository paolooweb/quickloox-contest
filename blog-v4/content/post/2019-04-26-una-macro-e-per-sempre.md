---
title: "Una macro è per sempre"
date: 2019-04-26
comments: true
tags: [Drang, Numbers, Unix, Keyboard, Maestro]
---
Dr. Drang, piuttosto che inserire a mano quaranta valori numerici in un form, [scrive una macro in Keyboard Maestro](https://leancrew.com/all-this/2019/04/security/) dopo avere ricavato i valori da una pagina web data in pasto a Numbers.

A metà strada, si vergogna un po’ di avere usato Numbers e descrive la riga di comando Unix che avrebbe raggiunto gli stessi scopi.

La spiegazione di molto di quello che tutti abbiamo da imparare arriva in fondo:

>Ovviamente non è una macro che userò più di una volta l’anno ma, adesso che ce l’ho, posso compilare i campi del sito che mi interessa in modo facile e preciso. Non è un grande risparmio di tempo, ma un grande risparmio in frustrazione.

Un problema risolto in modo automatico smette di essere un problema e scompare dall’elenco. Si risparmia tempo, ma risparmiare frustrazione è ancora più salubre. Non è una prospettiva molto, molto interessante per semplificarsi la vita?
