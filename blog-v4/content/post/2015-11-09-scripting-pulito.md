---
title: "Scripting pulito"
date: 2015-11-09
comments: true
tags: [AppleScript, Terpstra, OSX, El, Capitan, Mavericks, Unix, Terminale]
---
Ritorno su un commento aggiunto da **Mimmo** a [questo post](https://macintelligence.org/posts/2015-10-29-ecco-perche/) in quanto ho provato il da lui segnalato [script di Brett Terpstra](http://brettterpstra.com/2015/10/27/vacuuming-mail-dot-app-on-el-capitan/) per fare pulizia nel database interno di Mail e così ottenere risparmio di spazio e maggiore velocità.<!--more-->

Il mio nuovo database di Mail è grande il 96 percento di prima (per essere una sintonia fine, il guadagno è percepibile) e effettivamente il programma è più reattivo, niente miracoli quanto piuttosto, appunto, pulizia.

Attenzione a chi usa sistemi anteriori a El Capitan, perché potrebbe essere necessaria una piccola modifica nel codice. Non ho verificato perché tutti i sistemi di casa sono su El Capitan a parte PowerBook 12”, impegnato in altre faccende. La problematica viene trattata nei commenti dall’articolo.

Compatibilità a parte, va notato come l’AppleScript contenga alcuni comandi di shell e intorno un minimo di interfaccia utente e di chiarimento dello scopo e della struttura del codice. Si potrebbe – lo segnalano nei commenti – eseguire direttamente i comandi Unix e avere anche più velocità.

Corretto. Va notata anche la data di nascita dello script: 2007, otto anni fa. Terpstra lo ha emendato nel 2012 ed è recente una aggiunta ulteriore. Questa longevità, questa collaborazione nel tempo tra programmatori, la chiarezza di lettura, le avremmo viste così se si trattasse di tre o quattro comandi Unix? Direi di no. Nell’avvicinare la programmazione al linguaggio naturale, AppleScript la nobilita e la fa anche uscire dal solito recinto di *nerd* e *geek*.

Per questo resta una delle più grandi risorse e un vantaggio competitivo potenzialmente enorme rispetto non tanto a Windows, quanto a chi usa il computer solo per ricevere servizi, senza mai avere almento tentato di arrivare al gradino dal quale al computer è possibile impartire ordini. Viva AppleScript.