---
title: "Il punto sul puntatore"
date: 2020-06-10
comments: true
tags: [iPad, trackpad, iOS]
---
Ebbene sì, ho provato a scherzare con il fuoco e vedere l'effetto che fa.

 ![iPad Pro con Magic Trackpad](/images/ipad-trackpad.jpg  "iPad Pro con Magic Trackpad") 

Ho sottratto Magic Trackpad a Mac e lho collegato a iPad mentre ero in viaggio e con un sacco di cose da scrivere.

In omaggio alla famosa obsolescenza programmata di Apple, ho collegato con successo una periferica avviata a compiere dieci anni di vita a un iPad Pro 2018. Come [anticipato dal supporto](https://support.apple.com/en-us/HT211009), con un Magic Trackpad di prima generazione non funzionano lo scrolling e alcuni gesti. Penalizzante, ma lo scopo era sperimentare il lavoro con un trackpad su iPad e stralcio il problema, che riguarda il trackpad e non quelli più recenti.

In breve: ha un senso, ma tutto è ancora acerbo.

Meno in breve. Linterfaccia conferma quanto hanno scritto i recensori più autorevoli: Apple al suo meglio. Lesperienza è convincente e si inserisce perfettamente in quella complessiva di iPad, come se ne fosse un componente da sempre.

Luso complessivo è più problematico. Labito mentale da indossare con un trackpad a disposizione finisce per richiamare per forza lutilizzo di un Mac, nel quale le mani non si staccano dalla tastiera o dal sistema di puntamento, mouse, trackpad, penna che sia.

Su Mac sono come un topo nel formaggio e uso il trackpad praticamente solo per la manipolazione grafica, ridimensionare una finestra, fare un drag and drop. Per tutto il resto cè la tastiera.

Su iPad sono pronto a dimenticarmi che esiste lo schermo touch e adotto lo stesso schema… solo che lambiente è diverso. Significa che mancano alcuni comandi da tastiera. Significa che alcune combinazioni da tastiera sono diverse. Combinazioni, non comandi. Non ho alcun problema nellavere comandi differenti per cambiare un font o impostare un link. Invece andare in cima o in fondo a una pagina web, oppure spostarsi dentro un documento, quello è un po diverso. Cè anche molta varianza, prevedibile, tra le app. Alcune si fanno pilotare docilmente, altre meno. Migliorerà, ma ci vuole tempo.

La risultante è che su iPad passo _più tempo_ con il trackpad che su Mac, perché il trackpad serve come complemento della tastiera e, su iPad, la tastiera è meno versatile. La produttività massima si ha quando le mani restano sulla tastiera e su Mac questo avviene più a lungo e meglio.

Per non parlare del fatto che, abituato a usare lo schermo touch, spessissimo lo uso _con le due mani_. Un comando che sta a sinistra lo raggiungo con la sinistra, mentre uso laltra mano per laltra estremità dello schermo. Con il trackpad si una una mano sola e magari tocca attraversare lo schermo per raggiungere un certo comando. È più vantaggioso muoversi tra tastiera e trackpad che tra tastiera e schermo, tuttavia due mani battono sempre una mano.

Confermo le mie [impressioni generali](https://macintelligence.org/blog/2020/03/21/simili-ma-diversi/) e i miei auspici: per molti è interessante avere lopzione di un trackpad su iPad. Apple ha pensato molto bene la propria soluzione, che però ha bisogno di tempo per maturare. Lo farà e darà soddisfazioni a chi predilige la modalità di lavoro stile laptop.

Forse controintuitivamente, la parte dove servono più miglioramenti è nel supporto della _tastiera_. Se i comandi disponibili aumenteranno fino a raggiungere il livello di Mac, saranno maggiormente uniformati con Mac e più onorati dalle app, il tempo passato sul trackpad di iPad sarà produttivo e piacevole, perché contenuto. Oggi è eccessivo. iPad è una macchina complessivamente molto produttiva (lo dico per esperienza consolidata). Al momento, tuttavia, per lavorare al massimo con lausilio di un trackpad ci vuole un Mac.
