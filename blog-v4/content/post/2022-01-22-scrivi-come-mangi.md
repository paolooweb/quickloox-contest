---
title: "Scrivi come mangi"
date: 2022-01-22T02:15:03+01:00
draft: false
toc: false
comments: true
categories:
tags:
---
Devo confessare. Sono tornato brevissimamente nel campo del giornalismo per dare una mano a un amico che doveva trasformare in articolo il resoconto di una sua conferenza.

Uno ero io. L’altro era lui, uno dei padri della Internet italiana, con una conoscenza della tecnologia prossima all’infinito.

Per un motivo o per l’altro, siamo partiti da un file testo… e siamo passati per Open Document, Pages e Word prima della consegna della versione finale.

La quale è diretta a un sito web, che la pubblicherà ovviamente in Html. Non so che strumenti useranno per la conversione; anche ipotizzando la procedura più indolore possibile (tipo incollare il testo formattato dentro WordPress; Word e WordPress in alcuni casi sono culo e camicia), il fatto stesso che si debba essere una procedura è sbagliato.

Ecco perché l’informatica personale è ancora ai primordi. Anche due persone con una certa esperienza e una altrettanto certa disposizione d’animo sono soggette a una serie assurda di vincoli e limitazioni per elaborare un documento normalissimo. Si sarebbe certo potuto fare diversamente, solo che il tracciato con la minore resistenza era quello descritto.

Occorre assolutamente un sistema di massa capace di offrire un ambiente di scrittura semplice come quello di un semplice word processor e che internamente faccia Html, o almeno Xml pronto a diventarlo. Non c’è niente del genere e non vale dire che basta esportare o fare Salva come. Significa che la struttura nativa del file non è Html come invece sarebbe meglio fosse.

[Lyx](https://www.lyx.org) tende ad avvicinarsi al modello di quello che serve, seppure sia lontano anni luce dalla facilità di utilizzo e dalla semplicità dell’interfaccia che dovremo abituarci a pretendere.

Naturalmente, avere una struttura interna Html o Xml non significa affatto generare i minestroni assurdi che oggi escono se proviamo a esportare in Html il contenuto di un word processor, da Word in su (neanche Pages è immune, neanche le Google App).

Una startup con le idee e la visione giusta per creare il MacWrite dei quarant’anni, elegante, semplice, quasi magico e alla portata di tutti, complessità pari a quella di mangiare un piatto di spaghetti. Ma con sotto Html. Chi ci si mette?

_Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile._
