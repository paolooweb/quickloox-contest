---
title: "Portali, notizie e considerazioni"
date: 2017-09-04
comments: true
tags: [Saipem, Manutencoop, Explorer, Firefox, Andy]
---
Siamo a settembre 2017 e **Andy** invia due schermate del sito Manutencoop per i fornitori.

È *consigliabile* solo Internet Explorer. Chissà che cosa accade a violare la consegna.

 ![Portale fornitori da usarsi, è un consiglio, con Internet Explorer](/images/manutencoop-explorer.jpg  "Portale fornitori usabile solo con Internet Explorer") 

Per non far mancare niente, una opzione oscura e mai necessaria come la reimpostazione della password non funziona.

 ![Problemi di password sul sito Manutencoop](/images/manutencoop-password.jpg  "Problemi di password sul sito Manutencoop") 

Saipem, altra azienda, altro sito per i fornitori. Sito *ottimizzato* per Internet Explorer, come nel 1997.

 ![Altro portale fornitori ottimizzato per Internet Explorer](/images/saipem.png  "Altro portale fornitori ottimizzato per Internet Explorer") 

Per contesto, Statcounter afferma che la classifica di agosto dei *browser* più usati sia la seguente.

<div id=all-browser-ww-monthly-201608-201708 width=600 height=400 style=width:600px; height: 400px;></div><!-- You may change the values of width and height above to resize the chart --><p>Source: <a href=http://gs.statcounter.com/>StatCounter Global Stats - Browser Market Share</a></p><script type=text/javascript src=http://www.statcounter.com/js/fusioncharts.js></script><script type=text/javascript src=http://gs.statcounter.com/chart.php?all-browser-ww-monthly-201608-201708&chartWidth=600></script>

In forma di testo:

* Chrome 54,88 percento.
* Safari 14,88 percento.
* UC Browser 7,45 percento.
* Firefox 5,89 percento.
* Opera 4 percento.
* Internet Explorer 3,69 percento.

Una notizia buona l’abbiamo: sanno che esiste Internet.

La considerazione: quando saremo liberi da tutti i danni causati da Internet Explorer?