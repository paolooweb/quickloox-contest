---
title: "È la somma che fa il totale"
date: 2014-02-08
comments: true
tags: [Mac, LG, Sony, Dell, IBM, Lenovo, iPad]
---
Si chiamano articoli *click bait*, esca per i clic. La spari grossa, la gente un po’ ci crede un po’ ci ride, il contatore delle visite gira a volontà.<!--more-->

È la specialità di siti di basso livello. Non faccio nomi, non meritano pubblicità. Strano quindi vedere un [*click bait* su Re/Code](http://recode.net/2014/02/06/our-love-affair-with-the-tablet-is-over/), a firma di Zal Bilimoria, partner in Andreessen Horowitz.

Titolato *La nostra storia d’amore con i tablet è finita*, l’articolo si sforza di dimostrare come per le tavolette non ci sia spazio, che la loro vendita abbia raggiunto il picco, che il mercato viaggi verso la divisione tra computer da una parte e telefoni intelligenti, anche magari con lo schermo un po’ più grande, dall’altra.

Queste categorie così nette mi suonano sempre artificiose, perché se decido di passare la giornata in viaggio con iPad invece che con Mac – come oggi – bado pochissimo alla loro categoria, molto invece a peso, dimensioni, necessità del momento, app migliori da usare all’occasione. Ma facciamo finta che il quadro sia realistico.

Si è appena raccontato come [Sony voglia liberarsi della produzione di portatili](https://macintelligence.org/posts/2014-02-06-una-parabola-dei-talenti/). LG, si vocifera, [potrebbe cercare di fare lo stesso](http://www.koreatimes.co.kr/www/news/tech/2014/01/133_148787.html). Ibm, che vede più lungo, si è sfilata [quasi dieci anni fa](http://news.cnet.com/IBM-sells-PC-group-to-Lenovo/2100-1042_3-5482284.html) per passare tutto a Lenovo e quest’anno ha appena replicato la mossa [con i server](http://news.cnet.com/8301-1001_3-57617651-92/ibm-sells-its-x86-server-business-to-lenovo-for-$2.3-billion/). Dell [ha smesso di essere società per azioni](https://macintelligence.org/posts/2013-09-16-la-morte-sua/) per cercare di reinventarsi e dipendere dai PC il meno possibile. Tutti gli indicatori di mercato dei PC [sono in calo](http://www.theguardian.com/technology/2014/jan/09/pc-value-trap-windows-chrome-hp-dell-lenovo-asus-acer).

Bello che ci siano voci controcorrente ma, visto come vanno i PC, l’idea che la tavolette abbiamo già terminato di crescere sembra peregrina. Forse hanno raggiunto il picco le tavolette-truffa, Android da due soldi senza supporto, senza ottimizzazione, senza software dedicato, senza attenzione nel design. iPad è in [crescita praticamente lineare](http://www.theverge.com/2014/1/27/5350106/apple-q1-2014-earnings). Filemaker Go ha venduto [un milione di copie](http://www.broadwayworld.com/bwwgeeks/article/FileMaker-Go-for-iPad-and-iPhone-Apps-Surpass-1-Million-Downloads-20140205) ed è facile da immaginare che siano andate più su iPad che su iPhone.

[È la somma che fa il totale](http://www.youtube.com/watch?v=IGAhH3ZWink), diceva Totò. Sostenere che le tavolette siano sul limite del declino è azzardato, o falso. Che lo scriva un finanziatore di nuove aziende è singolare, certo, e interessante da investigare. La cosa stimolante tuttavia è la sua motivazione, non certo la sua opinione.

<iframe width="560" height="315" src="//www.youtube.com/embed/IGAhH3ZWink" frameborder="0" allowfullscreen></iframe>