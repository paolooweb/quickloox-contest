---
title: "Cinque per otto"
date: 2014-09-19
comments: true
tags: [iOS, iOS8, iPhone, iPhone5, iCloud, Tips, Health]
---
iPhone 5 mi ha proposto l’aggiornamento a iOS 8 e ho diligentemente accettato. Preferisco che pensi lui a scaricare tutto e avvisarmi quando è pronto anziché mettere tempo per collegarmi a mano su un server superaffollato e aspettare più tempo del dovuto e del previsto.<!--more-->

L’aggiornamento sembra andato bene, dopo lo scaricamento di 867 megabyte di dati. Come [si diceva ieri](https://macintelligence.org/posts/2014-09-18-tempo-di-uscite/), di primo acchito non ci si accorge neanche di avere effettuato un aggiornamento così cospicuo.

Non avevo assolutamente spazio per l’aggiornamento, con un disco di sedici gigabyte, così ho preventivamente cancellato tutte le foto (sono già importate automaticamente su Mac) e le app non vitali (si riscaricano da iCloud).

Al primo impatto sembra tutto come prima quanto a velocità o autonomia e potrò essere più attendibile tra qualche giorno.

Il nuovo font nelle Impostazioni è più piccolo, ma più leggibile. Le modifiche alle Impostazioni sono generalmente apprezzate e sanno di razionalizzazione, benvenuta.

Non avevo idea che venisse automaticamente installata una applicazione Tips (il nome in inglese) contenente consigli d’uso di iPhone. Il sistema sta diventando complesso come quello di un computer e, se i consigli sono validi e gli aggiornamenti della *app* puntuali, può essere una buona idea.

L’altra *app* nuova è Health (sempre in inglese). So che l’idea di mettere i propri dati medici sul computer da tasca agghiaccia molti che pensano il peggio possibile per la propria *privacy*. D’altra parte il *self quantifying*, l’automisurazione, è disciplina praticatissima e tutto il *trend* salutista diventerà di una certa importanza con l’andare del tempo. Ho conservato in iPhone per anni certi esami ecografici da mostrare ai parenti e mi sono divertito a inserire la mia *medical ID*, praticamente i riferimenti da avvertire in caso di necessità, il gruppo sanguigno, altezza e peso, roba che qualsiasi motociclista serio porta con sé da sempre in modo anche più dettagliato. Che sarà in futuro, lo vedremo. In questo discorso vanno innestate inoltre le notizie sulla politica di *privacy* di Apple, riassumibile in *non guardiamo i vostri dati e non li passiamo a nessuno*, che varrà la pena di un approfondimento nei prossimi giorni.

Complessivamente, sarà forse l’età, ma è un aggiornamento straordinariamente tranquillo e lineare, dopo il quale mi sono ritrovato in un lampo a fare quello che preferisco fare con iPhone: usarlo, invece che stare a trafficare in configurazioni e impostazioni.

Eccezioni al discorso. Come sempre, mi sono ritrovato Bluetooth acceso (lo tengo spento). Me lo immaginavo, è sempre accaduto dopo ogni aggiornamento, niente di grave.

Più bizzarro il fatto che iOS 8 riparte chiedendo la password di iCloud e alcuni altri parametri fondamentali, come ha sempre fatto. Stavolta però ha tenuto a precisare che avrei dovuto passare dall’[autenticazione a due fattori](https://macintelligence.org/posts/2014-09-13-il-coraggio-di-cambiare/) e così mi ha chiesto di inserire il codice di autenticazione, inviato sul telefono stesso.

Prima che arrivasse il codice mi è stata proposta l’accettazione del contratto di licenza. Ho ovviamente accettato e tutta la preimpostazione è andata avanti. Ero già a ricaricare le cose importanti, quando è arrivato il codice oramai inutile.

Poco male anche in questo caso particolare, nel quale l’autenticazione a due fattori avviene su un solo apparecchio invece che due. Se non altro ha risvegliato l’attenzione.