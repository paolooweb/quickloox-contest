---
title: "Lei non sa chi sono io"
date: 2022-06-13T03:55:26+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [LaMda, Google, La Prima Colonia, Montecristo, Edoardo Volpi Kellermann]
---
Si capisce che la situazione sia scappata un po’ di mano quando un ingegnere di Google si mette a scrivere a chiunque che [il generatore di chatbot di Google è diventato senziente e ha lo spessore psicologico di un bambino di sette o otto anni](https://www.msn.com/en-us/news/technology/the-google-engineer-who-thinks-the-company-s-ai-has-come-to-life/ar-AAYliU1).

In questo periodo ho spesso a che fare con bambini di quell’età e garantisco che rispetto a un chatbot hanno decisamente qualcosa in più. Ma basta anche meno; già prima dei cinque anni – se si pone attenzione all’esprimersi di un bambino – se ne vedono delle belle.

L’ingegnere, invece che ricevere il premio Nobel, la medaglia Fields e una pagina in tutte le enciclopedie degne di questo nome, è stato sospeso. Qualcosa vorrà dire. Per me vuol dire che si è perso come umano e ha perso LaMda come chatbot.

Il machine learning è una disciplina affascinante che produce risultati straordinari. In compenso, se può bastare dargli in pasto terabyte di dati per fargli generare conversazione, non significa che l’ingranaggio di machine learning *capisca* che cosa sta dicendo.

Lo sviluppo di coscienze artificiali è del tutto fantascienza ed è destinato a restarlo a lungo, salvo rivoluzioni imprevedibili. Devo giustappunto proseguire [la recensione di un fantalibro](https://macintelligence.org/posts/2022-05-28-la-prima-colonia-the-montecristo-project-recensione-in-tre-parti-parte-uno/) imperniato proprio su questo concetto.