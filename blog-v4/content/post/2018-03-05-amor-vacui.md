---
title: "Amor vacui"
date: 2018-03-05
comments: true
tags: [iPhone]
---
Non ci avevo mai pensato e invece un goccino di pensiero laterale, ogni tanto, farebbe gran bene. Tra Siri e funzioni di ricerca, che importanza ha riempire la schermata home di iPhone? Intorno a nessuna.<!--more-->

Si può invece lavorare creativamente con risultati straordinari, come testimonia questa [antologia](http://randsinrepose.com/archives/youre-probably-underestimating-what-you-can-do-with-your-iphone-home-screen/). Sono rimasto ammirato ed è difficile (colpa mia per la maggior parte, sicuro, ma non solo.)

Prossimamente mi tocca un Frecciarossa da due ore e mezzo. Mi sa che mi applico.