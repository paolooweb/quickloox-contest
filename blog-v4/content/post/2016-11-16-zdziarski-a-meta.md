---
title: "Zdziarski a metà"
date: 2016-11-16
comments: true
tags: [Zdziarski, memoria]
---
Vista l’esperienza di Jonathan Zdziarski che ha fatto [l’elenco dei programmi che si riescono a lanciare in sedici gigabyte di memoria](https://macintelligence.org/posts/2016-11-12-la-linea-zero-sedici/) su MacBook Pro, e la promessa di provare a replicare l’esperienza sui miei otto gigabyte di MacBook Pro, eccomi qui.

Ho prima di tutto perlustrato le cartelle di lancio di daemon e programmi vari, nonché gli elenchi di startup. Niente di che, un po’ di pulizia spicciola da qualche cosina vecchia, buon pretesto per spolverare.

Poi ho verificato che in Console le cose fossero a posto e non ci fossero processi impazziti, di quelli da quindici messaggi al secondo.

Obiettivo: vedere quanti programmi si aprono prima di cominciare a paginare, cioè spostare su disco contenuti della Ram.

Partiamo con iTunes, da cui dipendono Apple TV e le sincronizzazioni degli apparecchi iOS; il Terminale, perché mi prendo una licenza e, invece di Monitoraggio Attività come Zdziarski, uso il comando `top`. Monitoraggio Attività è molto più leggero sul processore, ma occupa più Ram e oggi è il giorno della Ram. Se c’è la curiosità di vedere che cosa occupi più memoria, `top -o mem`. Poi BBEdit, con cui scrivo questo *post*, e ovviamente il Finder.

(Potrei scrivere il *post* nel Terminale, ma non è un comportamento di chiunque; certamente sostituire tante applicazioni con il Terminale porterebbe dei vantaggi, ma è fuori dallo spirito dell’esperimento).

Messo così, ho liberi 4,1 gigabyte di Ram. Si noti che Dropbox occupa una volta e mezza la Ram di iTunes. Certamente, dovendo risparmiare Ram a tutti i costi, Dropbox è una questione da considerare. È parte integrante del mio flusso di lavoro, quindi lo lascio aperto. E inizio a lavorare.

Apro Vienna per avere il feed Rss periodico. Messaggi. Ecco, scendo sotto i quattro gigabyte. Ovviamente mi serve Safari e lo lancio. Safari con una sola finestra aperta vuota pesa in memoria meno di Dropbox e più di iTunes. 3,6 gigabyte liberi. Se apro un sito in Safari – questo blog, rispondendo anche a un commento – però ecco che Safari arriva quasi al peso di Dropbox. Un altro sito e Safari pareggia Dropbox. In tutto sono 3,2 gigabyte liberi ancora.

Basta tergiversare. Lancio Mailsmith per la posta elettronica. Poi Arriva un punto cruciale: lancio Slack. Zdziarski lo segnala come programma sprecone di Ram, così come Chrome. Ma per il mio lavoro è necessario. Purtroppo Slack lancia un sottoprocesso per canale, ognuno pesante quasi come Safari. A me ne apre nove… così arrivo a un solo gigabyte libero. Se spengo Slack, torno a 2,9 gigabyte. Potrei fare molto di più, tenendolo spento. Ma *devo* averlo acceso. Vabbè. Comincio il lavoro vero e proprio, con nuove finestre di Finder e Safari. Mi serve leggere un documento .docx e così lancio Pages. I giga liberi sono meno di 0,1, siamo quasi arrivati.

Sono andato avanti così per un paio d’ore e poi ho fatto una pausa. Slack aveva ridotto le pretese e così c’è stato spazio anche per Anteprima. Al ritorno ho visto che Mac aveva paginato, probabilmente per l’intervento del salvaschermo. E questo completa l’esperimento.

Questo è quello che si riesce a fare con otto gigabyte di memoria, mega più mega meno:

* Finder (quattro finestre)
* BBEdit (quattro finestre)
* Terminale (due finestre)
* iTunes
* Safari (nove finestre) 
* Dropbox
* Mailsmith
* SpamSieve
* Vienna
* Slack (nove canali)
* Pages (due finestre)
* Anteprima (cinque finestre)

Per lavorare, è il minimo indispensabile. D’altronde nel 2016 otto gigabyte portano inevitabilmente alla paginazione, a meno di essere bravi e furbi abbastanza da sostituire tanti programmi con l’equivalente da Terminale.

Allego infine una schermata di `top` della classifica dei consumatori più voraci di memoria durante l’esperimento. La schermata è stata realizzata a esperimento concluso e nel frattempo, messo in carica un iPad, si era aperto anche Foto.

Teniamo presente che con sedici gigabyte di Ram l’aspettativa non è il doppio di questa, ma il triplo. Perché, in partenza, il sistema prende quasi quattro gigabyte sui primi otto, ma nessuno sugli otto successivi.

 ![Processi nel mio MacBook Pro](/images/processi.png  "Processi nel mio MacBook Pro") 