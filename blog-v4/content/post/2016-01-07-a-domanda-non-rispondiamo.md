---
title: "A domanda non rispondiamo"
date: 2016-01-07
comments: true
tags: [Awt, iPad, Pro]
---
Passata a malapena una settimana, sappiamo già quale sarà l’argomento di lana caprina per eccellenza nel 2016, come sintetizzato da questo [articolo di Apple World Today](http://www.appleworld.today/blog/2015/12/31/can-the-ipad-pro-replace-my-laptop-nope-not-yet) (sito che peraltro lavora bene grazie a gente brava): *iPad Pro può sostituire il mio portatile?*<!--more-->

La risposta è diversa per ciascuno e pertanto qualsiasi tentativo di porre la domanda in senso assoluto è futile. Avendo ciascuno una risposta diversa, si potrebbero scrivere dozzine di conclusioni differenti e ancora non basterebbero.

Oggi come oggi sono tentato di rispondere *dipende dal giorno della settimana* ed è una frase assai meno cretina di quanto sembri, in funzione di luoghi, mansioni da svolgere, durata dell’impegno, programmi da usare, flussi di lavoro eccetera.