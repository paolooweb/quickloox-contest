---
title: "Intelligenza umanistica"
date: 2017-08-22
comments: true
tags: [Apple, Gruber, Tom, Humanistic, AI, intelligenza, artificiale]
---
Sono un fautore dell’intelligenza artificiale forte, da quando ho consumato le notti di un’estate a leggere [Gödel, Escher, Bach: An Eternal Golden Braid](https://www.amazon.com/Gödel-Escher-Bach-Eternal-Golden/dp/0465026567). Tuttavia mi indispone la piega odierna che ha preso la disciplina, tutta presa a sviluppare l’*artificiale* con nessuna attenzione per l’*intelligenza*.

I progressi del *machine learning* sono straordinari, ma le macchine non sanno perché lavorano, né se abbia un senso o sia una perdita di tempo e nemmeno quale sia l’obiettivo.

A confortarmi, questa [presentazione Ted](https://www.youtube.com/watch?v=DJMhz7JlPvA) di Tom Gruber dedicata a quella che lui ha chiamato *humanistic AI*. Non più la domanda *come rendere le macchine più acute?* e al suo posto la domanda *come rendere più acuti noi grazie alle macchine?*.

Un esempio illuminante: un’intelligenza artificiale capace di diagnosticare correttamente il cancro nel 92,5 percento dei casi, quasi brava come gli oncologi umani, che riescono a diagnosticare correttamente novantacinque volte su cento. Il punto di migliorare la prestazione della macchina non è superare l’oncologo: è *supportarlo*. L’uomo e la macchina, *insieme*, centrano la diagnosi il 99,5 percento delle volte.

Meraviglioso il discorso sulla memoria: una intelligenza artificiale che aiuti ciascun umano a tenere nota di quello che, per età, distrazione o malattia, il suo cervello fatica a reperire tra un neurone e l’altro. E che sia sicura e privata, perché ciascuno dovrebbe avere la possibilità di scegliere quali ricordi possono diventare pubblici e quali invece no.

Siamo sempre lì, all’[incrocio tra tecnologia e arti liberali](https://www.google.it/search?client=safari&rls=en&q=arti+liberali+site:macintelligence.org&ie=UTF-8&oe=UTF-8&gfe_rd=cr&ei=JkybWavjKMbc8AfTrKe4DQ), il vero autentico grande lascito di Steve Jobs.

Non importa dove lavori Tom Gruber e a che cosa; voglio più gente come lui e con idee come quelle che manifesta.

<iframe width="560" height="315" src="https://www.youtube.com/embed/DJMhz7JlPvA" frameborder="0" allowfullscreen></iframe>