---
title: "Faticosa e straordinaria libertà"
date: 2016-06-30
comments: true
tags: [youtube-dl, YouTube, Terminale]
---
Lidia è cresciuta abbastanza da imporr… gradire uno spazio quotidiano di visione di cartoni animati e a casa questo avviene talvolta su YouTube tramite tv.<!--more-->

Al mare, dove [un servizio di qualità al giusto prezzo è un’utopia](https://macintelligence.org/posts/2015-07-22-lidea-del-cliente/), scarico da YouTube i video che altrimenti con la chiavetta andrebbero a singhiozzo – ho provato e riprovato – e li guardo, pardon, li faccio guardare offline.

Per scaricare uso un gioiello software di nome [youtube-dl](http://rg3.github.io/youtube-dl/). Ingombra zero e fa miliardi di cose; tuttavia nella versione base scarica un video YouTube con il comando `youtube-dl indirizzo` ed è più che perfetto per le mie esigenze.

Siccome durante l’anno lo uso solo in casi eccezionali, appena arrivato al mare l’ho aggiornato con `youtube-dl -U`.

Mi è già successo almeno tre volte di non riuscire a scaricare un video, con gli errori più fantasiosi. Allora ho aggiornato youtube-dl e ho ottenuto una nuova versione che era sempre uscita il giorno stesso o al massimo due-tre giorni prima. Dopo l’aggiornamento, il video si è scaricato senza problemi.

Ho compreso che esiste una sorta di gioco del gatto e del topo tra YouTube e youtube-dl. A YouTube cambiano qualcosa che rompe il funzionamento di youtube-dl; i cui programmatori modificano il software perché torni a funzionare fino alla modifica successiva di YouTube e così via.

E questo avviene *continuamente* su basi che, se non sono quotidiane, si avvicinano. Retoricamente, ogni giorno YouTube cambia qualcosa, ogni giorno youtube-dl pareggia il cambiamento, come Penelope e la mitica tela.

È una fatica, complessa e pesante, per tutte le parti in gioco e a pensarci bene è una fatica che tutela una parte della nostre libertà. Quella di poter guardare filmati, di poter avere un’organizzazione sostenibile che pubblica filmati, la possibilità di aggirare i divieti in situazioni eccezionali.

Anche la libertà di rispettare le regole, sicuro. Quando sono su YouTube e parte la pubblicità, paziento fino al termine dello spot. Non è proprio la compensazione per i miei scaricamenti, ma almeno una minima forma di rispetto per chi lavora da quella parte dello schermo. Se youtube-dl chiede di segnalargli bug o altro, cerco di farlo al meglio delle mie possibilità. Svolgono un servizio per me, gratis.

Ed è straordinario che tutto questo, sintetizzato al massimo, si condensi nel funzionamento (o meno) di una breve riga di Terminale.

*[Una bella spiegazione del Terminale, magari capace di crescere nel tempo, è il tallone d’Achille di tutti i libri su macOS. Tranne il  [più-che-un-libro pensato da Akko](https://macintelligence.org/posts/2016-06-15-un-kickstarter-per-cambiare-il-libro/), dove potrebbe diventare un buon punto di partenza per chi non ne sa e un appoggio utile per chi vuole saperne di più. A patto di [dare una possibilità al progetto](https://www.kickstarter.com/projects/295108589/cuore-di-mela-i-segreti-di-apple-iphone-e-mac), che ha bisogno di tanto passaparola e qualche adesione concreta.]*