---
title: "Un nome che mancava"
date: 2020-08-06
comments: true
tags: [Fellow, Atkinson, Hertzfeld, Lisa, Hertzfeld, HyperCard, Holt, Page, Lisa, Schiller]
---
Nello scrivere dell’[ascesa di Phil Schiller al ruolo di Apple Fellow](https://macintelligence.org/blog/2020/08/05/posti-limitati/) ho fatto un elenco di alcuni dei personaggi che hanno ottenuto il riconoscimento, limitandomi ai più comuni.

Ho appena scoperto di avere dimenticato un nome primario: [Bill Atkinson divenne il terzo Apple Fellow](https://pxlnv.com/linklog/apple-fellows/), dopo Steve Wozniak e Rod Holt, insieme a Rich Page, per il suo lavoro su Lisa.

Il dettaglio interessante nel post di _Pixel Envy_, che riprende un ricchissimo [aneddoto di Andy Hertzfeld](https://www.folklore.org/StoryView.py?project=Macintosh&story=Credit_Where_Due.txt), è che la carica di Apple Fellow attesta un merito _tecnico_.

Gli eventi ci dicono che la figura di Phil Schiller va oltre l’ambito ufficiale del marketing. Un giorno, nella storia da raccontare di Apple, ci sarà anche lui.

(Comunicazione di servizio: sarà una giornata intensa e tuttavia non posso dimenticare un compleanno, quindi faccio qui i miei migliori auguri prima di svegliarmi in stato di privazione del sonno).
