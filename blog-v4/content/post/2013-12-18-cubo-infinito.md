---
title: "Cubo infinito"
date: 2013-12-18
comments: true
tags: [Apple, Minecraft, Cupertino]
---
Se mancava un motivo dei tanti che spingono Apple a costruirsi un [nuovo avveniristico quartier generale](http://www.cupertino.org/index.aspx?page=1107), eccolo: il *campus* al numero uno di Infinite Loop in quel di Cupertino, California, è divenuto talmente scontato negli anni che è stato fedelmente [ricostruito in Minecraft](https://www.youtube.com/watch?v=0JQG4rO3BUc), un cubo sopra l’altro.<!--more-->

<iframe width="560" height="315" src="//www.youtube.com/embed/0JQG4rO3BUc" frameborder="0" allowfullscreen></iframe>

Per un ragazzo, una ragazza, [Minecraft](https://minecraft.net) è un regalo possibile. Non necessariamente per scaricare la [mappa di One Infinite Loop](http://mscraft.michaelsteeber.com) e neanche aspettandosi che crei qualcosa di questo livello. È uno strumento creativo, che potrebbe restare inutile oppure scatenare la creatività.