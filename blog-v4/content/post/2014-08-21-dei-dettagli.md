---
title: "Dei dettagli"
date: 2014-08-21
comments: true
tags: [TextEdit, Yosemite]
---
La sesta versione preliminare di Yosemite non può essere quella buona; è ancora troppo presto.<!--more-->

Tuttavia è molto buona. La stabilità è eccezionale e un indizio rilevatore dello stato di lavorazione è l’indicatore della carica della batteria, assolutamente sgraziato nelle Developer Preview precedenti e ora invece con l’aria più o meno definitiva.

Compatibilità totale per quanto mi è stato dato di provare finora in fatto di software. L’amministrazione della Ram sembra essere molto efficiente e un po’ più esigente, perché un paio di volte il sistema ha chiesto esplicitamente di chiudere qualche app di troppo (eravamo sopra le trenta, con otto gigabyte a disposizione).

Yosemite potrebbe essere una edizione su cui puntare e non è un caso che ne esista una beta pubblica a disposizione di chiunque: è un caso dove una maggiore apertura rispetto al passato sta facendo bene ad Apple e al prodotto.

Adesso però ridatemi i versi di *Think Different* nell’icona di TextEdit.