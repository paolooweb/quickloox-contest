---
title: "Nessuno è neutrale"
date: 2024-03-18T02:07:13+01:00
draft: false
toc: false
comments: true
categories: [Software, Hardware, Internet, Entertainment]
tags: [Office, The Dynasty, New England Patriots, Deflategate]
---
Leggo che *sono solo strumenti* o che *se ho solo un martello, tutto mi sembra un chiodo, ma non è colpa di chi vende martelli*. Se solo fosse così semplice.

Ricordo un filmato di Diego Armando Maradona mentre palleggiava con un’arancia. Sono solo strumenti se chi li usa ha un bagaglio tecnico e di esperienza immensamente superiore alla media. Allora sì, un pallone o un agrume diventano la stessa cosa.

A volte neanche lì. Sto guardando su Apple TV [The Dynasty - New England Patriots](https://www.apple.com/it/tv-pr/originals/the-dynasty-new-england-patriots/) (più lungo di come sarebbe stato perfetto, ma interessante e avvincente soprattutto nella prima metà) e si dedica un tempo apprezzabile al [deflategate](https://www.espn.com/nfl/story/_/id/28502507/what-really-happened-deflategate-five-years-later-nfl-scandal-aged-poorly), la polemica per cui sul campo dei Patriots sarebbe invalsa l’abitudine di togliere aria dai palloni di gara per togliere un punto di riferimento agli avversari. Non so, se una società dichiara di offrire un formato standard ISO per la memorizzazione e il salvataggio dei documenti, che in teoria dovrebbe essere documentato e interoperabile, ma di fatto lo è solo per chi possa investire risorse ingenti nello sforzo di superare i problemi di documentazione, ecco, non sono solo strumenti. Sono armi per danneggiare le alternative dall’alto della propria posizione dominante, per esempio.

Non è colpa di chi vende martelli. Se però chi vende martelli è consapevole di una situazione e ne approfitta scientemente per andare oltre le proprie prerogative, c’è responsabilità. Se basterebbe un file di testo da 10k ma gli analfabeti di ritorno producono un file Word da 100k perché sanno fare solo quello, non è colpa di chi vende Word; ma c’è una responsabilità. Per esempio, quei 90k sono uno spreco di spazio, di energia, di risorse. Moltiplicato per centinaia di milioni di analfabeti di ritorno, fa un impatto ambientale.

Se quella società di martelli inserisse nel proprio software una finestra di dialogo che dice *ehi, questo documento è completamente privo di formattazione. Che ne dici di salvarlo in formato testo così risparmiamo spazio ed energia?*, eserciterebbe la propria responsabilità.

Se quella società di martelli scoraggiasse invece il salvataggio in qualunque formato diverso da quello che vuole lei attraverso l’inserimento di *dark pattern* nell’interfaccia del comando, eserciterebbe un abuso della propria posizione.

Di recente è successo che un venditore di martelli, con il proprio browser, copiasse i tab aperti in un browser concorrente senza il consenso dell’utente. Scorretto? Ma no: è stato un errore, ha dichiarato, di cui si è accorto solo dopo che svariati media avevano sollevato pubblicamente il problema intanto che le segnalazioni degli sviluppatori cadevano nel vuoto. Ora [è stato messo tutto a posto](https://arstechnica.com/gadgets/2024/02/microsoft-fixes-problem-that-let-edge-replicate-chrome-tabs-without-permission/). Solo un caso di software malfatto (chiamare *bug* una funzione di copia così puntuale e discreta richiede un certo coraggio).

Un altro venditore di martelli, qualche mese fa, faceva in modo che chi cercasse *Chrome* da Edge in Windows 11 [ricevesse in risposta una pagina contenente una finta interazione con il chatbot AI di Bing](https://www.theverge.com/2023/6/6/23736289/microsoft-bing-chrome-search-fake-ai-chatbot). Non una vera interazione con Bing; una interazione *finta*. La realtà a volte supera la fantasia più selvaggia.

Subito dopo la pubblicazione dell’articolo che la raccontava, l’*esperienza* della finta pagina di Bing è stata soppressa. Non era probabilmente disonestà volta a scoraggiare l’uso di Chrome, ma software malfatto.

Se però fai notare che da quel venditore di martelli arriva software malfatto, ti dicono che sei *fazioso*.

Faziosa è l’idea che tutto sia uguale a tutto, che siano solo strumenti, che la tecnologia sia neutrale. Come veramente minimo, a casa di certi venditori di martelli, è un’arma di guerra commerciale. Niente di male in questo; la disonestà però, anzi, il software malfatto, anzi, queste bizzarre coincidenze sempre a danno di qualcun altro potrebbero, dovrebbero fare la differenza in qualche coscienza.