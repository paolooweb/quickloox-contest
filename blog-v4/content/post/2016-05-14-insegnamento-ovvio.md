---
title: "Insegnamento ovvio"
date: 2016-05-14
comments: true
tags: [iPad]
---
Importa poco che l’[articolo originale del Wall Street Journal](http://www.wsj.com/articles/apple-steps-up-effort-to-reach-classrooms-1463082073) sia dietro il *paywall* e non tutti avranno voglia di leggerlo.

Su MacDailyNews c’è un [riassunto](http://macdailynews.com/2016/05/13/apple-ipad-helps-lift-school-districts-graduation-rate-to-82-from-65/) più che sufficiente a capire il tema, che è l’uso di iPad a scuola.

>Il professor Elliot Soloway della University of Michigan, che studia l’uso della tecnologia nella didattica, ha spiegato che tipicamente i computer nelle classi non aumentano il rendimento perché le scuole non ripromettano i programmi. “Le scuole commettono sempre l’errore di comprare prima i computer e poi chiedersi che cosa farci”.

>Nel Coachella Valley Unified School District della California meridionale, dove quasi ogni studente è in condizioni disagiate, è stato approvato un programma per 42 milioni di dollari che fornisce a ventimila studenti altrettanti iPad. Il sovrintendente Darryl Adams sostiene che gli iPad abbiano aiutato la crescita della percentuale di diplomati, dal 65 percento nel 2011 all’82 percento nel 2015.

Sembra incredibile, vero? Se i programmi di studio vengono adeguati alla presenza di nuovi e migliori strumenti, i risultati migliorano. Se invece si comprano gli strumenti nuovi per andare avanti a fare le cose nel vecchio modo, i risultati sono gli stessi.

Scioccante. Come tante, troppe ovvietà che gli insegnanti appaiono spesso troppo preparati e acculturati per cogliere. Le cose semplici li prendono di sorpresa.
