---
title: "Leggere o non leggere, questo è il problema"
date: 2020-08-07
comments: true
tags: [Excel, Verge, Giannandrea]
---
Commenti a profusione sulla notizia che gli scienziati cambiano i nomi ai geni per non farseli convertire a tradimento da Excel.

Zero commentatori a soffermarsi sul fatto che la notizia si basa su uno studio del 2016, di cui si parlò [persino qui](https://macintelligence.org/blog/2016/08/26/non-servono-dei-geni/), dunque in abbondanza. Siamo tutti assuefatti al sovraccarico informativo e si vede. Forse dovremmo diffondere un link in meno, rileggere prima di commentare, guardare una pagina concentrati oppure saltarla, se l’esito finale è identico.

Memento: il lato deteriore delle ricerche sull’intelligenza artificiale ha portato a [Gpt-3](https://github.com/openai/gpt-3), terza edizione di un sistema di generazione di linguaggio naturale [candidato a eliminare la figura di chi scrive sul web per routine](https://kitze.io/posts/gpt3-is-the-beginning-of-the-end).

Il software è predestinato a soppiantare l’uomo nelle attività non creative e scrivere news rafferme è decisamente una di queste. L’uomo deve farsi trovare all’appuntamento preparato; leggere a cervello spento significa solo che ci si nutrirà di _deepfake_ testuali. Consiglio di evitarlo.

Parlando di intelligenza artificiale, siamo fortunati che ci sia almeno una Apple a lavorare a software (pseudo)intelligente capace di potenziare l’umano invece che prenderne il posto. Invece di perdere tempo con le [riesumazioni di The Verge](https://www.theverge.com/2020/8/6/21355674/human-genes-rename-microsoft-excel-misreading-dates), che stavolta ha perso punti di brutto, dedichiamoci piuttosto alla ricca [intervista di John Giannandrea, responsabile Apple per il machine learning, ad Ars Technica](https://arstechnica.com/gadgets/2020/08/apple-explains-how-it-uses-machine-learning-across-ios-and-soon-macos/).

Da leggere con intenzione, o ignorare. Capisco che il tempo sia poco e l’attenzione razionata tra diecimila cose. Ma non esiste realmente una via di mezzo tra leggere e non leggere.