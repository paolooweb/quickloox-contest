---
title: "Cinque anni senza una data"
date: 2015-07-13
comments: true
tags: [Jobs, Stamos, Apple, iOS, iPhone, Adobe, Flash, Facebook, HT, killbit, Gruber, Safari, Explorer, Edge, Chrome]
---
Steve Jobs nell’aprile del 2010:

>Flash è stato creato nell’èra del PC […] Ma l’èra mobile è fatta di apparecchi a basso consumo, con interfacce a tocco e standard web aperti, tutte aree dove Flash non ce la fa. […] Flash non è necessario per decine di migliaia di sviluppatori impegnati a creare grandi app, giochi compresi.<!--more-->

Ieri Alex Stamos, *chief security officer* di Facebook, [su Twitter](https://twitter.com/alexstamos/status/620306643360706561):

<blockquote class="twitter-tweet" lang="en"><p lang="en" dir="ltr">It is time for Adobe to announce the end-of-life date for Flash and to ask the browsers to set killbits on the same day.</p>&mdash; Alex Stamos (@alexstamos) <a href="https://twitter.com/alexstamos/status/620306643360706561">July 12, 2015</a></blockquote> <script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>

>È ora che Adobe annunci la data di fine vita di Flash e chieda ai browser di impostare killbit per lo stesso giorno.

(Un [kill bit](https://technet.microsoft.com/en-us/security/dn535768.aspx) permette di inibire la partenza di una certa applicazione sul browser).

Tanto per cambiare, lo [scandalo Hacking Team](https://macintelligence.org/posts/2015-07-07-belle-liberta/) ha portato alla luce l’esistenza di ben [tre falle di sicurezza di Flash](http://www.pcworld.com/article/2947512/hacking-teams-arsenal-included-at-least-three-unpatched-exploits-for-flash-player.html) di cui Hacking Team conosceva l’esistenza e il resto del mondo, compresi gli utilizzatori di Flash, no.

Steve Jobs ha visto il futuro prima di chiunque altro e ha rinunciato a Flash. Ma si è dimenticato di chiedere una data. Cinque anni più tardi, qualcuno l’ha chiesta.

Manca solo che qualcuno si decida in Adobe e finalmente ci saremo liberati di un peso che va verso il decennale (iPhone senza Flash è del 2007).

A dire il vero, basterebbe [dare retta a John Gruber](http://daringfireball.net/linked/2015/07/13/facebook-flash):

>Sarebbe gentile se Adobe fosse avanti a tutti su questo, ma non è necessario. Apple ha azzoppato Flash per sempre quando ha vietato i plugin del browser su iOS. Se solo qualcuno dei grandi browser facesse la stessa cosa – diciamo Safari, Chrome e Explorer/Edge – Flash morirebbe rapidamente. Già vacilla.

Speriamo lo leggano le persone giuste.

**Aggiornamento:** Firefox [blocca Flash come preimpostazione](https://support.mozilla.org/en-US/kb/set-adobe-flash-click-play-firefox) fino a quando non saranno risolte le tre falle di cui sopra. Magari si dimenticassero di toglierla anche dopo…

**Aggiornamento**: una variante di Ubuntu [abbandona Flash](http://blog.system76.com/post/124110683268/farewell-flash).