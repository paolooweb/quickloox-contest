---
title: "In alto a sinistra"
date: 2013-11-19
comments: true
tags: [iPad, Goodreader]
---
Nel mio iPad, in alto a sinistra, c’è [Goodreader](https://itunes.apple.com/it/app/goodreader-for-ipad/id363448914?l=en&mt=8). Nell’iPad di servizio, usato soprattutto per il gioco di ruolo, c’è [MaPnakotic](https://itunes.apple.com/it/app/mapnakotic/id373314417?l=en&mt=8). Nel mio iPhone, in alto a sinistra, c’è Messaggi. Nel tuo?