---
title: "Cose da lockdown"
date: 2020-04-27
comments: true
tags: [Polytopia, Swift, Playgrounds]
---
Da qualche giorno, prima di dormire risolvo uno dei puzzle di [Swift Playgrounds](https://www.apple.com/swift/playgrounds/). Sono partito dallo zero assoluto, *Learn to Code 1*, e ne ho completati undici. Sono al punto in cui si usano funzioni come pezzi di altre funzioni, alla vigilia del completamento del capitolo sulle funzioni. La difficoltà è pressoché nulla, a questo stadio, e le uniche serate in cui non ho fatto centro al primo colpo sono quelle in cui mi sono addormentato con iPad in mano. Diciamo che qualsiasi bimbo di prima elementare ci può provare con un minimo di accompagnamento.

Continuo a giocare a [Battle for Polytopia](http://midjiwan.com/polytopia.html). Siamo un gruppetto tranquillo, a tutti i livelli di bravura, e accogliamo senza problemi aspiranti giocatori. Il gioco è semplice ma per niente banale e il requisito minimo per sostenere le partite online è di pochi minuti al giorno, scelti a piacere. Il gioco abilita il multiplayer dietro esborso di un paio di euro ma poi qualunque altro acquisto in-app è opzionale e pochissimo rilevante per essere competitivi. Se qualcuno vuole provare, può passarmi l’identificativo e lo invito. Il mio identificativo è *Zf37pI6KgzGLOIL9*.

Invece ho provato più volte a rientrare in [Clash of Clans](https://supercell.com/en/games/clashofclans/) dopo una lunga assenza forzata e non ci sono riuscito. Il gioco ha cambiato pelle ed è molto più bello, ricco e coinvolgente di prima. Ma mi chiede troppa presenza.

C’è da ripartire. Allora ho ripreso in mano [Gödel, Escher, Bach](https://www.amazon.com/G%C3%B6del-Escher-Bach-Eternal-Golden/dp/0465026567). Roba impegnativa, sì. Per questo in pausa pranzo giro qualche pagina di [Innumeracy](https://www.amazon.it/Innumeracy-Mathematical-Illiteracy-Its-Consequences/dp/0809074478). Consiglio a chiunque di trovare una buona lettura su errori e mistificazioni proprie della statistica perché in questo periodo avere percezione di che cosa si sta combinando a partire dai dati è piuttosto importante. I dati sono male assortiti, male comunicati e sospetto anche male trattati.