---
title: "Per farla breve"
date: 2014-01-06
comments: true
tags: [iOS, OSX, iCloud]
---
Nella calza della Befana metto questo [finto tutorial di 9to5 Mac](http://9to5mac.com/2014/01/04/how-to-create-and-sync-keyboard-shortcuts-between-ios-7-and-mavericks/) su come sincronizzare abbreviazioni di tastiera tra iOS 7 e Mavericks.<!--more-->

Finto in quanto non è affatto un tutorial! Se la sincronizzazione di dati e documenti via iCloud è accesa da ambedue le parti, iCloud fa tutto da solo.

Per *abbreviazioni da tastiera* si intendono quelle usabili dentro i documenti per scrivere qualcosa di più lungo, come *tadbn* per avere automaticamente *Tanti auguri di buon Natale e felice anno nuovo per te, tutti i tuoi cari e il gatto se ne hai preso uno di recente*.

Merita invece la nozione che iCloud si occupi di questi dettagli, un passo in più verso il sogno di un sistema astratto, unico e universale, che si concretizza su più apparecchi ciascuno con la giusta interfaccia, dove possiamo preoccuparci di creare e consultare lasciando il computer a preoccuparsi degli automatismi.