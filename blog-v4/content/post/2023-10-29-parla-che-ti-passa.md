---
title: "Parla che ti passa"
date: 2023-10-29T16:52:52+01:00
draft: false
toc: false
comments: true
categories: [Internet]
tags: [ChatGPT, ai, intelligenza artificiale]
---
Il fatto che [sempre più persone trascorrano ore a parlare con sedicente intelligenza artificiale](https://arstechnica.com/information-technology/2023/10/people-are-speaking-with-chatgpt-for-hours-bringing-2013s-her-closer-to-reality/) fa pensare a qualcuno che il software abbia raggiunto la capacità di esprimersi di un essere umano.

A me fa pensare che tanta gente abbia perso la capacità di esprimersi verso il mondo esterno fino a ridursi a parlare con un computer. Almeno in un gioco di ruolo online incontri degli avatar altrui.