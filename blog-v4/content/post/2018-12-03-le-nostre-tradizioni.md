---
title: "Le nostre tradizioni"
date: 2018-12-03
comments: true
tags: [Frecciarossa, Wi-Fi]
---
Da alcune settimane sto girando per aeroporti di mezza Europa e non lo sapevo, ma ho acquisito una dipendenza da Wi-Fi.

Da mesi non prendevo un Frecciarossa e ho nuovamente metabolizzato che il suo Wi-Fi non funziona. Neanche su un treno deserto che approda a Milano a mezzanotte (in ritardo).

La differenza con il passato è che prendevo spesso il Frecciarossa quando non c’era ancora LTE. Adesso ho dimenticato quel Wi-Fi inutile e in un istante la connessione è schizzata a piena velocità.

Abbiamo qualcosa che non vorremmo magari tramandare ai nostri figli. Ma finirà che lo faremo, pur controvoglia.
