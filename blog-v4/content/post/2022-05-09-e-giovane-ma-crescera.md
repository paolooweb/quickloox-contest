---
title: "È giovane, ma crescerà"
date: 2022-05-09T00:03:48+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [Runestone, Guyot, Alex Guyot, MacStories, Markdown]
---
Scrivo questo post con [Runestone](https://runestone.app), un editor di testo recente che continua a non essere [BBEdit](https://www.barebones.com/products/bbedit/) per iPad e che però rappresenta un miglioramento incrementale in varie direzioni verso la disponibilità di una app veramente ideale per un apparecchio diverso da Mac.

La categoria di editor di testo su iPad è particolarmente inflazionata e viene spontaneo filtrare con una certa severità le informazioni, dopo avere provato qualche programma e non avere trovato alcune novità che giustificasse una nuova esperienza. Runestone ha superato la prima scrematura grazie alla buona [recensione di Alex Guyot su MacStories](https://www.macstories.net/reviews/runestone-a-streamlined-text-and-code-editor-for-iphone-and-ipad/), a cui rimando per una disamina capillare di quello che fa o non fa.

Le cose che mi piacciono al primo impatto sono la colorazione automatica della sintassi, che non hanno tutti e semplifica la scrittura in [Markdown](https://www.markdownguide.org); la facilità nel configurare l’ambiente di scrittura; la libertà totale di scegliersi il posto dove salvare i file. Sembra incredibile, ma nel 2022 capita ancora di trovare qualche editor, anche blasonato, che non supporta iCloud come si deve, o che accetta solo Dropbox o che comunque costringe a compromessi. Aggiungo anche la grande semplicità dell’interfaccia.

Le cose che non mi piacciono, per ora: dover specificare il nome file prima di cominciare a scrivere e una gestione di Undo e Redo che non ho ancora decifrato e passa la sufficienza, ma è tutta da verificare da qui in avanti. Vedo ora che c’è qualche problema transitorio con il testo in fondo alla pagina, che al primo giro resta parzialmente nascosto dalla fascia superiore della tastiera software di iPad in orientamento verticale, quale con i pulsanti di Annulla, copia e incolla, indentazione e con le scelte consigliate di sostituzione delle parole. Poi si normalizza tutto, però per un minuto è scomodo. Intanto che proseguo, noto una mancanza che mi infastidisce molto: non c’è il conteggio dei caratteri. Da verificare il discorso automazione, se e come sia compatibile con i Comandi Rapidi.

Una nota a parte merita la politica di prezzo, che è innovativa e va incoraggiata. Tutto quello che sto facendo ora, in pratica tutto quello che mi serve come base, è gratis e basta. Un pagamento Premium di 9,99 euro sblocca una serie di controlli che fanno la gioia di un programmatore o di chi tiene molto all’esperienza di utilizzo, con ingredienti che su iPadOS sono inediti, come la distanza verticale tra le righe, il *kerning*, la visualizzazione dei caratteri invisibili, il controllo dello scorrimento orizzontale e tante altre cose che è agevole esplorare nelle preferenze.

A questo si aggiunge la personalizzazione. Il pagamento Premium permette di cambiare l’aspetto dell’icona, aggiunge opzioni di colore, sblocca persino un giochino che non rivelo, insomma permette di creare con Runestone un legame oltre quello utilitarista dell’utilizzo del programma.

La differenza tra gratis e a pagamento è molto chiara e i vantaggi evidenti per chi, appunto, programma o tiene molto all’esperienza di utilizzo. Al tempo stesso, chi vuole usare Runestone senza pagarlo, lo può fare senza perdere efficienza e con tutti gli strumenti essenziali a disposizione. Nessuno rimane a metà strada, nessuno rimane ancorato controvoglia a un abbonamento, dieci euro sono un prezzo onestissimo per il valore del programma e tuttavia hanno un senso nel favorire ulteriore sviluppo da parte del programmatore. Intelligente, consapevole, onesto. Anche qui, non tutte le app meritano questi aggettivi a livello di prezzo.

Proseguo l’esperimento di adozione di Runestone come editor standard su iPad e pago il Premium comunque, perché l’iniziativa merita. In versione gratis è tutt’altro che un’anatra zoppa e si giustifica pienamente. Considerato che se non piace basta buttarlo via, consiglio vivamente la prova a tutti.