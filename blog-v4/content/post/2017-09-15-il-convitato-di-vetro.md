---
title: "Il convitato di vetro"
date: 2017-09-15
comments: true
tags: [iPhone, keynote, Apple, Park, watch, S8, Samsung, Schiller, Stanford, Touch, pay]
---
[iPhone X](https://www.apple.com/iphone-x/) ha uno schermo Oled, *il primo all’altezza di iPhone* ([ha detto Phil Schiller](https://www.apple.com/apple-events/september-2017/)).

Ha un nuovo processore, progettato in casa da Apple.

Ha un nuovo processore grafico, progettato in casa da Apple.

Ha abbandonato il pulsante hardware Home, presente su un miliardo e duecentomila iPhone.

Ha abbandonato Touch ID per adottare Face ID: dal riconoscimento dell’impronta digitale, che richiede un tocco del dito, si passa al riconoscimento tridimensionale del volto, che richiede uno sguardo. Il riconoscimento avviene in tempi strettissimi e, siccome poi si usa tra l’altro con pay, *deve* funzionare bene e meglio di Touch ID.

(Tra l’altro l’icona di Face ID è un bell’[omaggio a Susan Kare](https://twitter.com/concreteniche/status/907700055817289728), leggendaria autrice delle icone del primo Macintosh).

<blockquote class="twitter-tweet" data-lang="en"><p lang="en" dir="ltr">Hello old friend <a href="https://twitter.com/SusanKare">@SusanKare</a> <a href="https://t.co/tpvbH1DyEJ">pic.twitter.com/tpvbH1DyEJ</a></p>&mdash; Patrick Smith (@concreteniche) <a href="https://twitter.com/concreteniche/status/907700055817289728">September 12, 2017</a></blockquote> <script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>

Il riconoscimento facciale è affidato a una rete neurale alimentata da un motore hardware progettato appositamente in casa da Apple. Un motore di rete neurale dentro un computer da tasca.

La batteria dura due ore in più che su iPhone 7.

È prematuro parlare di velocità per una macchina che deve ancora uscire, ma si dice (grazie **Mimmo**) che [surclassi S8 di Samsung e sia all’altezza di un MacBook](https://thenextweb.com/apple/2017/09/12/apples-new-iphone-x-already-destroying-android-devices-g/).

Alla luce di questo, mi è capitato di leggere *Apple non innova*, *il keynote peggiore di sempre*, *vendono solo fashion*.

Forse perché è stato considerato solo iPhone X e tralasciato, che so, l’orologio più venduto al mondo seppure privo di connessione cellulare autonoma, che [acquisice la connessione cellulare autonoma](https://www.apple.com/apple-watch-series-3/) e mantiene inalterate le dimensioni dei modelli precedenti. Come se fosse una banalità.

watch sarà strumentale questo autunno per uno [studio condotto dalla Stanford University](http://med.stanford.edu/appleheartstudy.html) assieme ad Apple sulla possibilità di identificare condizioni di stress cardiaco come la fibrillazione atriale, spesso fatali e spesso asintomatiche. Detto in modo crudo, watch – che ha già [salvato qualche vita in circostanze occasionali](https://www.forbes.com/sites/ianmorris/2016/03/28/apple-watch-saves-mans-life/) – potrebbe salvarne molte di più e con regolarità. Se pare mancanza di innovazione, o moda.

Tutto questo mentre da anni Apple ha lavorato al suo nuovo campus, ora sostanzialmente completo. Lavorato [in senso vero](https://macintelligence.org/posts/2017-02-24-nessuna-nuova-buone-nuove/), mentre le altre aziende lasciano un progetto in mano agli architetti.

La [visione finale](https://macintelligence.org/posts/2017-05-17-l-ultimo-prodotto/) di Steve Jobs. Lo hanno ricordato, nell’anfiteatro che porta il suo nome, e l’ho trovato un momento sincero e profondo, a prescindere da tutto quello che è arrivato dopo.

Ho lasciato indietro una montagna di briciole che aggiungono ulteriore materiale al *keynote peggiore di tutti i tempi* (tipo: è uscito iPhone 8). Vorrei puntualizzare che questo *keynote* è il primo della storia nella nuova astronave di vetro. Un “particolare” trascurato.