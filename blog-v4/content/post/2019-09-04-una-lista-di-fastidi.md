---
title: "Una lista di fastidi"
date: 2019-09-04
comments: true
tags: [Mailchimp, Slack, iOS, iPad]
---
Di norma non incrocio blog e [canale Slack](http://goedel.slack.com/) (le porte sono sempre aperte per chi volesse chiacchiere vane di grande qualità, vane a mia colpa e qualità per merito degli inquilini), però si parlava ultimamente di [Mailchimp](https://mailchimp.com) e vorrei dire due cose in proposito più pubblicamente. Magari possono servire.

Per il distratto, Mailchimp è una piattaforma per gestire newsletter, invii di posta elettronica a destinatari in numero elevato. Liste fino a duemila indirizzi sono gratis e da lì in avanti si paga.

Inviare newsletter, specialmente se fatto per professione, è una cosa complicata. Non devono essere troppe né troppo poche, è importante vedere quanto vengono aperte e lette e da chi, le barriere antispam sono un problema tecnico non indifferente, spedire un numero ingente di email in tempi brevi richiede infrastruttura, posta Html e posta solo testo vanno ugualmente considerate eccetera eccetera eccetera. Mailchimp si offre per risolvere tutte queste questioni ed è una delle punte di lancia di un mercato che offre varie alternative, quasi sempre a livello vastamente inferiore.

Uso Mailchimp per un cliente, su un piano a pagamento, da anni. Per qualcuno è la piattaforma migliore sulla piazza e anche ove non lo fosse, starebbe tranquillamente sul podio. Non ho esperienza sensata della concorrenza e non posso fare confronti, tuttavia – puramente sulla carta – prima di prendere decisioni invito chiunque a leggere bene la documentazione di [Send with SES](https://www.sendwithses.com/login), che si appoggia all’infrastruttura Amazon, è gratis fino a tremila email al mese e promette di essere più economico di Mailchimp quando c’è da pagare (c’è un calcolatore di costi).

Una piattaforma di newsletter si giudica soprattutto da due caratteristiche: la gestione delle liste di indirizzi e l’interfaccia. Sulla prima parte ho poco da dire; sulla seconda, Mailchimp ha la caratteristica di farmi imbestialire a cadenza regolare grazie a una combinazione di promesse mancate, fragilità, bug e trascuratezze.

Lavoro con Mailchimp soprattutto nell’ambito della composizione delle email da inviare alle liste. Ci sono una interfaccia web e una app per iOS. La prima critica è che lavorare seriamente con la app è problematico. Per fare cose semplici tutto bene, ma appena serve un aggiustamento fine è probabile che la app infastidisca. Ci sono infatti funzioni importanti in Mailchimp che nella app, semplicemente, mancano. Vuoi mandare un test della email a più di un indirizzo? Non si fa. Vuoi controllare se gli indirizzi web dentro la email sono corretti e funzionano? Nella app non si può.

Altre cose sono implementate a livello indegno, per esempio l’editor all’interno dei blocchi di testo. Prova a fare qualcosa di più complicato che scrivere e fai fatica. Una attività come modificare un link web all’interno di un blocco di testo preso da una email precedente – frequentissima se si mandano newsletter a cadenza regolare – è acrobatica o impossibile.

È impensabile che nel 2019 si possa avere l’esigenza di lavorare su Mailchimp nella pienezza delle funzioni da un iPad Pro? Direi proprio no. Ho lavorato su Mailchimp con il vecchio iPad di terza generazione ed era un esercizio di tenacia, sicuro. iPad Pro 12”9 di autunno 2018 è un computer fatto e finito. Eppure la app non è all’altezza di un impiego professionale.

Che si fa se devi portare a casa l’email su iPad? Lavori via browser, come faresti su un desktop. Appunto. Attenzione, però: alcune cosette non funzionano. Quando si tratta di impartire il comando finale di spedizione, c’è una sovrapposizione di elementi non prevista. Il tocco sul pulsante A attiva l’opzione B. Per essere sicuri di fare le cose nel modo giusto bisogna tornare nella app, dove l’invio funziona a perfezione.

Comporre una email in Mailchimp su iPad significa lavorare saltando più volte dalla app al browser e viceversa, secondo la cosa da fare al momento. Si fa tutto, ma l’eleganza e la fluidità che si possono avere in iOS con una buona app sotto le dita, ce la si scorda. Occhio perché Mailchimp salva da solo i contenuti ogni venti secondi. Se commuti da app a browser e viceversa, stai attento a vedere la versione salvata e aggiornata di quello che stai facendo. Se hai troppa fretta rischi di fare pasticci.

Il tutto dopo che la app ha fatto miglioramenti giganteschi. Fino all’anno scorso era uno scherzo, oppure il lavoro di un programmatore che ci lavora giornalmente il tempo della seduta in bagno. Adesso è solo incompleta.

Pazienza, si potrebbe dire. Vuoi mettere, lavorare su un desktop con uno schermone comodo?

A parte che lo schermone comodo potrebbe essere collegato a un iPad, Mailchimp in quanto tale commette un peccato mortale: alletta con una promessa ambiziosa e poi disattesa.

Un sistema modulare di composizione delle email.

È una bellissima idea: blocco titolo, blocco testo, blocco immagine, blocco immagine con didascalia, blocco gallery con due-quattro-sei-immagini, blocco immagine più testo a lato, blocco separatore, blocco titolo più sottotitolo… invece che comporre faticosamente l’Html, prendo i blocchi e li trascino nella loro giusta successione. I blocchi si possono editare, duplicare, spostare, cancellare. Per avere la mia immagine grafica, preparo *template*, modelli con dentro gli attributi di corpo, carattere, allineamento che voglio io. Tutte le volte che mi serve scrivere una nuova email, evoco il template giusto e poi modifico i blocchi come mi servono nell’occasione.

Ambizioso ed efficace. I blocchi praticamente diventano oggetti software, mattoni da costruzione riutilizzabil…

Ops.

*Si riutilizzano solo dentro quella email*. Se dentro una email hai un blocco eccezionale, che arriva da un template ma ha subito modifiche, e vuoi usarlo dentro un’altra email, non puoi.

Significa che fai un sacco di avanti e indietro tra template e email per mantenere tutto aggiornato. Sembrerebbe esiziale, invece è grave. Quando fai molte newsletter fai esperimenti continui, cambiamenti piccoli e grandi per avvicinarti alla formula magica che convince il maggior numero di destinatari a leggere. Significa che il template non va mai bene per la prossima volta. Se hai due esperimenti diversi di uguale successo, che vuoi conservare, devi avere due template della stessa email. Finisce che devi essere bravo a palleggiare una infinità di email e template, un disastro annunciato di confusione e malaorganizzazione.

Se hai una email che ti entusiasma, e viene da un template modificato, puoi duplicarla e riutilizzarla. Intanto però il template rimane non aggiornato. Da qualunque parte la giri si perde un sacco di tempo e arriva sempre il giorno in cui si fa prima a reinventare la ruota che andarla a cercare.

Ah, le email già inviate vengono conservate solo come Html. Hai la speranza di copiare un brano di codice per riutilizzarlo? Buona fortuna. La metafora dei blocchi scompare e capire dove inizia e dove finisce l’Html che serve è una impresa.

La composizione della email e l’assemblaggio dei blocchi sarebbero eccellenti, se bene implementati. Invece è tutto legnoso, lento, approssimativo, snervante. La modifica dei blocchi è eccessivamente modale e i parametri modificabili sono spezzettati qua e là. Noioso. Alcune decisioni di design sono fatte apposta per disturbare: se vuoi inserire un link e *anche* un tag `alt`, serve un clic apposito per fare comparire il campo. Va da sé che la zona da cliccare occupa spazio come lo avrebbe occupato il campo testo da evocare. Bastava mettere il campo testo subito e fare risparmiare un clic a milioni di utenti, cioè milioni di clic.

Oggetti, abbiamo detto. Parliamo di immagini. Si può fare editing delle immagini entrando in una istanza della Creative Cloud Adobe. L’interfaccia è veramente oscura, nondimeno è una bella comodità. Quando si inserisce una immagine dentro una email, come è d’uopo, puoi associare a essa una didascalia, un tag `alt`, un link.

Il giorno dopo, in un’altra email, rimetti quell’immagine, che dovrebbe avere lo stesso link, lo stesso tag, magari pure lo stesso testo. *Vanno reinseriti*.

L’editor di testo è particolarmente frustrante. Ha una modalità visuale e una codice, come WordPress o altri sistemi analoghi. La modalità codice, tuttavia, è praticamente inutile e particolarmente ostile. E se provi a compiere certe operazioni in modo visuale, per esempio cambiare il link associato a una parola, auguri.

Scavalcare temporaneamente le impostazioni da template di un blocco testo è sempre difficile e presto si impara a perdere un mucchio di tempo nei template oppure rinunciare alla creatività del momento, opzioni ugualmente negative.

Mentre si smadonna nel sistema cercando di lavorare in modo organizzato e certo, quello che meno fa sentire le carenze dell’implementazione, il sistema stesso genera *non-breakable space* a capocchia dentro il testo. Ogni tanto uno spazio normale diventa *non-breakable* e tiene sempre insieme le due parole che collega, andando a capo se necessario. Un giorno il blocco testo che ti ha sempre fatto tanto comodo cambia aspetto. Il testo fluisce diversamente. Vai a vedere e sono i *non-breakable space* emersi qui e là.

Queste sono solo macroscoperte e se uno vuole andare per il sottile ha da divertirsi. Per esempio, l’editor di testo converte automaticamente i caratteri nelle loro entità quando necessario (in Html non si scrive *è*, si scrive *\&grave;*) e lo fa sempre in forma testuale. Ove facesse comodo averli invece in forma decimale (e scrivere così &232;), bisogna andare a mano.

C’è un sistema per controllare se i link inseriti nella email funzionano. Funziona. Tranne qualche volta.

Più utenti possono collaborare a una stessa email e lasciare commenti. Che vengono persi dopo l’invio e non sono visibili una volta predisposto l’invio stesso. Se hai lavorato per bene e programmato l’invio con una settimana di anticipo, e qualcuno lascia un commento importante, non lo si vede. Analogamente, per visionare una email in invio bisogna sospendere l’invio. Esiste una possibilità di visione in sola lettura. Inspiegabilmente, cliccare sul nome della newsletter, che è la cosa più ovvia, non la attiva; bisogna disseppellirla da un menu a comparsa.

Mailchimp è un buon sistema che però soffre lacune di design micidiali. Difficile che una email ben fatta non prenda un pomeriggio, tra errori, rifacimenti, stranezze, legnosità, imperfezioni, ostacoli involontari all’utilizzo del sistema. Il giorno in cui scopri che non si adatta all’ora legale nonostante le impostazioni di fuso corrette, e l’invio non avviene all’ora che si voleva bensì all’ora solare, inizi a impostare gli invii all’ora sbagliata, per averla giusta, oppure impostare un fuso orario diverso dal nostro.

È un buon riassunto di tutta l’esperienza complessiva della piattaforma. Alla quale, se toccasse a me la decisione di acquisto, affiancherei sempre almeno una alternativa da valutare. Perché ti risolve un sacco di problemi in modo un sacco fastidioso.