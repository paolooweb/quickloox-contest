---
title: "Design di quello non buono"
date: 2017-03-16
comments: true
tags: [Bancomat]
---
Ieri si parlava di [buon *design*](https://macintelligence.org/posts/2017-03-15-design-di-quello-buono/). Ovvio che arrivi il contrappasso.<!--more-->

Le foto a fondo *post* sono state scattate presso un Bancomat di Milano. I riflessi e la necessità di fare velocemente (farsi cogliere a fotografare un Bancomat potrebbe ispirare pensieri molesti a un vigile coscienzioso o un cittadino paranoico) hanno influto negativamente sulla qualità. Le immagini tuttavia sono chiare a sufficienza.

*Lo schermo mostra le operazioni compiute dall’operatore durante la manutenzione del terminale.*

Una follia. Un Bancomat vuole essere sicuro. Questa nozione contrasta con l’idea di squadernare l’albero delle directory sottostante l’apparecchio.

Potrebbe trattarsi di un’operazione innocua, oppure no. In ogni caso, nel dubbio, rilasci una informazione in meno piuttosto che una in più.

Il problema maggiore, tuttavia, è il [design](https://macintelligence.org/posts/2017-03-15-design-di-quello-buono/). Ipotizzo che basti staccare manualmente un qualche cavo video, per evitare che lo schermo del Bancomat mostri a tutti quello che sta accadendo sulla macchina che lo amministra. Ma evidentemente nessuno ha pensato a questa eventualità. Oppure non è abbastanza facile e ovvia per cui il tecnico lo faccia senza pensarci. O ancora, nessuno lo ha avvisato dell’opportunità di farlo. Forse è scritto in un manuale, che però palesemente è poco convincente oppure non è fatto per essere letto e compreso.

Chi progetta il Bancomat dovrebbe mettere in conto che occorra manutenzione e organizzare il sistema in modo tale che la sicurezza sia garantita anche nel peggiore dei casi e non grazie, eventualmente, alla buona volontà di un tecnico. *Design* è *how it works*, come funziona.

Se posso permettermi la malignità, non stupisce vedere quale sia il sistema operativo in ballo in una situazione di cattivo *design*.

 ![Bancomat a Milano](/images/bancomat1.jpg  "Bancomat a Milano") 

 ![Bancomat a Milano](/images/bancomat2.jpg  "Bancomat a Milano") 

 ![Bancomat a Milano](/images/bancomat3.jpg  "Bancomat a Milano") 

 ![Bancomat a Milano](/images/bancomat4.jpg  "Bancomat a Milano") 
