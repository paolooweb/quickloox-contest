---
title: "Se lo dice l’autore"
date: 2022-05-21T01:26:29+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [Markdoc, Markdown, Stripe, John Gruber, Gruber, Daring Fireball]
---
Solitamente John Gruber non è tenero verso le estensioni di [Markdown](https://www.markdownguide.org) un-tanto-al-chilo, che complicano sintassi e leggibilità di Markdown senza restituire semplicità di codifica a chi scrive. Ovvero, tradiscono lo spirito di Markdown.

Se [parla bene](https://daringfireball.net/linked/2022/05/19/markdoc) di [Markdoc](https://markdoc.io), bisogna alzare le antenne.

>Sembra meraviglioso. Amo le loro estensioni, molto vicine allo spirito di Markdown. Usano le parentesi graffe; non sono sicuro di averlo mai detto pubblicamente, ma ho evitato di usare le graffe in Markdown – sebbene siano caratteri tentatori da questo punto di vista – per riservarli non ufficialmente alle estensioni. L’uso esteso di graffe di Markdoc è esattamente il tipo di cosa cui pensavo.

>Scalda il cuore vedere Markdown continuare a crescere in questo modo.

L’oste ha parole tenere per chi parte dal suo vino e ok; il punto è che Markdoc viene usato per uno scopo vero, concreto, con risultati ottimi. È infatti la codifica usata per la documentazione di [Stripe](https://stripe.com/en-gb-it), che per Gruber è *forse la migliore documentazione per sviluppatori al mondo*. Qui può permettersi molta meno partigianeria, perché gli sviluppatori non sono perditempo – non tutti almeno – e la documentazione funzionante è funzionante davvero; non c’è modo di vendere fumo.

Non tutti devono scrivere documentazione per sviluppatori e neanche documentazione; di fatto però Markdoc è un sistema di *authoring*, e chi conosce la parola capirà che un’occhiata gliela si deve dare davvero, dato che con l’authoring si producono molti tipi di opere. Anche vista la natura di *open source* di Markdoc e la naturalezza con cui è possibile navigare il sito dedicato.

Devono piacere le graffe, certo.