---
title: "Diritto di subire la riparabilità"
date: 2021-11-28T23:05:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [John Gruber, Daring Fireball, Fairphone] 
---
Sembra quasi un apologo creato apposta, il [resoconto di John Gruber delle particolarità di Fairphone 4](https://daringfireball.net/linked/2021/11/22/fairphone-4), per parlare di [riparabilità e di diritti](https://macintelligence.org/posts/La-riparabilit%C3%A0-ha-rotto.html).

[Fairphone](https://www.fairphone.com/en/) è un progetto funzionante sotto Android, nato per essere modulare, riparabile e con parti facilmente sostituibili a partire dalla batteria. Un Fairphone si apre con un normale cacciavite Philips.

In compenso, la resistenza alla polvere è IP54:

> Protetto contro l’ingresso di polvere sufficiente a pregiudicare il normale funzionamento, ma non a prova di polvere.

In altre parole, nel momento in cui un Fairphone avrà accumulato al proprio interno abbastanza polvere da creare un problema, quel problema sarà facilmente risolvibile.

Un iPhone è a prova di polvere, IP68, e sarà magari difficilmente riparabile, ma non avrà quel problema. Dal 2016, iPhone 7.

Il diritto alla riparabilità in apparecchi che sembrano destinati ad averne bisogno, non sembra proprio un diritto quanto una condanna.

_Per ora si può lasciare un commento dalla [pagina apposita di Muut per Qui
ckLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile._