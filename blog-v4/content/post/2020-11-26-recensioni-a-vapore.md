---
title: "Recensioni a vapore"
date: 2020-11-26
comments: true
tags: [Moorhead, Forbes, M1, apple, Silicon]
---
Da una parte Patrick Moorhead di Forbes, [Perché potresti voler rinunciare a MacBook Pro 13” M1](https://www.forbes.com/sites/patrickmoorhead/2020/11/21/apple-macbook-pro-13-m1-reviewwhy-you-might-want-to-pass/?sh=71d751a2786a): una recensione equilibratissima in cui tra i difetti della nuova architettura si denuncia *la mancanza di connessione 5G* (è vero, non l’ho inventato, giuro, chi non ci crede legga). Ecco come parla di giochi:

>Secondo me c’è una ragione per cui Apple e i primi recensori parlano sempre di Tomb Raider per Mac. Credo che sia perché è uno dei pochi giochi AAA che funzionano relativamente bene in emulazione, sopra i trenta fotogrammi per secondo; sta in App Store; e sfrutta le interfacce applicative proprietarie Metal di Apple invece di interfacce applicative aperte.

>Cnet dichiara che Steam funziona a malapena e senza Steam funzionante a dovere, penso che per i giochi AAA siamo spacciati [dead in the water]. Sono sicuro che Bejeweled e Flappy Birds da iOS funzionino alla grande, invece, nonostante vadano giocati dal trackpad.

>Collauderò molti giochi AAA nei prossimi giorni e ancora di più quando riuscirà ad avere un MacBook Pro da 16 gigabyte. Non pensavo che sarebbe stato equo verso Apple provare giochi AAA su una macchina da otto gigabyte.

Quest’uomo è un fenomeno; dopo avere schiantato l’oggetto della recensione, afferma di avere sospeso il giudizio. Mi permetto di aggiungere che l’[articolo di Cnet](https://www.cnet.com/news/macbook-air-m1-hands-on-big-changes-from-apple-silicon-and-big-sur/) citato dice qualcosa di leggermente diverso dalla sua versione:

>Finora, [Rosetta 2] mi ha permesso di installare Steam per giocare. […] L’interfaccia di Steam ha funzionato a scatti. Spero in una versione nativa in un futuro non troppo distante.

Il recensore di Cnet riferisce di una esperienza negativa con Baldur’s Gate 3 (mentre lo sviluppatore ha già annunciato l’aggiornamento che farà funzionare il gioco sotto Rosetta 2) e poi di avere fatto funzionare due giochi su sei dalla sua raccolta [Steam](https://store.steampowered.com), tutti provenienti da [Gog](https://www.gog.com/), bellissimo sito che tratta anche giochi AAA (se ho capito che intende Moorhead, sulle prime pensavo alle batterie) ma fa più attività di discount.

Contenuti di questo genere, nel 2020, sarebbero una recensione. Su *Forbes*, mica sull’ultimo dei siti. Speriamo che il 2020 finisca presto.

Se qualcuno su qualche altra testata volesse recensire MacBook Pro 13” M1 e parlare di giochi, esiste un simpatico [foglio di calcolo condiviso](https://docs.google.com/spreadsheets/d/1er-NivvuIheDmIKBVRu3S_BzA_lZT5z3Z-CxQZ-uPVs/htmlview?pru=AAABdgCK-Wg*LVeffzs-rBgopse9quLFaw) con dentro i risultati di massima relativi, mentre scrivo, a *duecentotrentatré* esperienze su qualche decina di titoli.

I risultati sono lì da vedere. Da notare che, a decine, provengono da Steam. Ma riportarne l’esistenza avrebbe vanificato il lavoro di diffusione di fumo, pardon, vapore negli occhi dei lettori.