---
title: "La festa della musica"
date: 2014-07-09
comments: true
tags: [YouTube, MusicVault, ConcertVault, WolfgangsVault]
---
Anche se andiamo fuori tema, tengo a segnalare come sul canale [Music Vault](https://www.youtube.com/user/musicvault), di YouTube, dove risiedeva un migliaio di concerti dal vivo di grandi *band* e singoli, ne stiano arrivando altri dodicimila.<!--more-->

L’iniziativa è frutto di due anni di restauro e rimasterizzazione dei materiali in possesso di [Concert Vault](http://www.concertvault.com), proprietà di [Wolfgang’s Vault](http://www.wolfgangsvault.com).

Davvero un bel tesoro, da mettere sulla bilancia di Internet dentro il piatto dei lati positivi.

Tornando in tema – da musicale ad Apple – Concert Vault vanta anche le *app* per [iPhone](https://itunes.apple.com/it/app/concert-vault/id294355484?l=en&mt=8) e per [iPad](https://itunes.apple.com/it/app/concert-vault-for-ipad/id604177276?l=en&mt=8).