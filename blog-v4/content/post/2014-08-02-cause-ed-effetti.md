---
title: "Cause ed effetti"
date: 2014-08-03
comments: true
tags: [Hoboken, Coachella, iPad, scuola]
---
*Correlation is not causation*, si recita nel mondo scientifico: trovare un andamento comune in due tendenze non significa automaticamente che vi sia davvero un legame. Lo statistico che non lo sa arriva entusiasta, per dire, ad affermare che consumare gelato fa venire voglia di viaggiare perché la curva del gelato e quella dei viaggi mostrano lo stesso picco quando arriva l’estate.<!--more-->

Mi avvicino lentamente al punto: si era parlato di un’esperienza positiva di [introduzione di oltre ventimila iPad in un distretto scolastico californiano](https://macintelligence.org/posts/2014-07-25-tasse-sulla-scuola/). Pochissimi danni agli apparecchi, pochissime violazioni dei protocolli, fermo restando che va adeguata la rete perché ventimila apparecchi che si collegano vogliono tanta banda.

Problema di banda verificatosi anche nel distretto scolastico di Hoboken, nel New Jersey, dove però le cose [sono andate maluccio](http://hechingerreport.org/content/new-jersey-school-district-decided-giving-laptops-students-terrible-idea_16866/).

Racconta uno degli amministratori, a proposito delle necessità di riparazione:

>Mezza dozzina di ragazzi al giorno, su base regolare, che riportavano i computer, “mi ci sono caduti sopra i libri, qualcuno ci si è seduto sopra, mi è caduto” […] Avevamo comprato unità con un guscio protettivo per minimizzare parte dei rischi […] Sono rimasto impressionato da certi danni che ho visto. Alcune macchine sono tornate completamente distrutte.

E poi i problemi software. I computer, soffocati dai filtri antiporno, antivirus, antiFacebook, antitutto, erano diventati disfunzionali. I docenti si lamentavano di tempi di avvio anche di venti minuti e software didattico che si bloccava per mancanza di memoria o rallentava intollerabilmente. Quello non didattico invece…

>Per così dire, non c’è hacker più determinato di un dodicenne con un computer in mano.

Vengo al punto. Come si sarà capito, a Hoboken hanno acquistato portatili e del genere sbagliato.

Oltre ad avere acquistato male, non sapevano amministrare la cosa e non avevano un piano. L’articolo racconta di come mezza città fosse venuta a conoscenza delle *password* e si introducesse negli istituti per approfittare del collegamento Wi-Fi.

Ora gli amministratori del distretto hanno corretto le affermazioni iniziali dei *media*, che riferivano dell’abbandono dei computer, per [spiegare](http://www.hoboken.k12.nj.us/hoboken/Recent%20News/Letter%20to%20the%20community%20regarding%20laptops.txt/_top) come invece si passerà da un modello *una macchina per ciascuno* a un modello *carrelli da classe* contenenti i computer, che tornano nel carrello a fine lezione e rimangono maggiormente sotto il controllo della scuola.

Mi verrebbe da dire che per forza, se acquisti portatili da cinquecento dollari per darli agli studenti, poi non hai un piano per saperli usare. Per forza, se non hai un piano, il programma viene massacrato. Per forza, se compri hardware da scuola che dipende da antivirus, poi tutto ti si ritorce contro.

Non posso. *Correlation is not causation*.

Che tentazione, però.