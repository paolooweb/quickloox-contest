---
title: "Il video che non ti aspetti"
date: 2019-08-19
comments: true
tags: [Aaa, AllAboutApple, Savona]
---
Ammetto che, se non avessi incontrato la passione e l’affetto per [All About Apple](https://www.allaboutapple.com/), museo e persone che lo animano, probabilmente di Savona saprei poco o niente.

Invece conosco un poco la città. Ammetto che, se fosse brutta, me la farei piacere quando posso andarci per All About Apple. È pure bella e mi ci muovo volentieri.

Tutto si tiene e difatti la città di Savona ha fatto circolare su Facebook un [video promozionale](https://www.facebook.com/1556666234640893/posts/2173641032943407?s=652614096&v=e&sfns=mo) veramente molto bello su ciò che sa offrire al turista culturale, sportivo, gastronomico, eccetera.

Nel video trova appunto posto anche una citazione del museo e sono sicuro che Alessio e compagni gongolano per un riconoscimento importante e significativo come questo, istituzionale e contemporaneamente ben fatto, capace di farsi guardare in rete da un bel numero di persone. Sono cose che, come dire, è difficile dare per scontate pensando a una amministrazione pubblica eppure qui sono riuscite al meglio.

Gongolo con loro, perché All About Apple [merita tutta la pubblicità immaginabile](https://macintelligence.org/posts/2019-07-21-il-lato-illuminato/), specie quando è pubblicità che merita la visione.
