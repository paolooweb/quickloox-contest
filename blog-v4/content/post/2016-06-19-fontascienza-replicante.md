---
title: "Fontascienza replicante"
date: 2016-06-19
comments: true
tags: [Blade, Runner, Typeset]
---
Anche stavolta sono fuori tema come quando [ho citato l’incredibile lavoro](https://macintelligence.org/posts/2014-12-07-fontascienza/) del blog *Typeset in the Future* relativamente a [2001](http://typesetinthefuture.com/2001-a-space-odyssey/) e soprattutto [Alien](http://typesetinthefuture.com/alien/).<!--more-->

Hanno concesso replica occupandosi di replicanti, vale a dire di [Blade Runner](https://typesetinthefuture.com/2016/06/19/bladerunner/). Oltre a prendere sempre di mira film che amo, il risultato è di tale pignoleria e dettaglio da affascinare e tenere avvinti alla pagina.

La decostruzione rischia a momenti di fare persino sparire l’esperienza del film, che forse non avrebbe bisogno di una lettura a livelli di puntiglio come questi. Eppure a me viene voglia di riguardarlo, anche dopo che ogni suo angolo più riposto è stato squadernato.

*[Sarebbe bello avere una [pubblicazione molto più aggiornata e presente di un libro](https://macintelligence.org/posts/2016-06-15-un-kickstarter-per-cambiare-il-libro/), per chi vuole approfondire al massimo livello, anche personalizzato, la conoscenza del mondo Apple. C’è una [iniziativa in corso](https://www.kickstarter.com/projects/295108589/cuore-di-mela-i-segreti-di-apple-iphone-e-mac) che attende l’adesione e il passaparola di chiunque ci veda qualcosa di positivo.]*