---
title: "Calcoli e ricalcoli"
date: 2023-01-31T01:55:24+01:00
draft: false
toc: false
comments: true
categories: [Software, Web]
tags: [Internet Archive, Calculator Drawer, ChatGPT]
---
Giravano gli orologi digitali a cristalli liquidi e giravano pure le calcolatrici tascabili. Qualche adulto saggio ci disse di fare attenzione: che cosa sarebbe successo se una catastrofe avesse eliminato fonti di energia e l’umanità fosse incapace di fare i conti a mano, rimbecillita dalle calcolatrici?

(Chissà dove sono adesso. Potrebbero ringalluzzirsi nel leggere il comunicato dell’aggiornamento del motore generativo di testo al centro delle cronache, che ora dispone di [capacità matematiche migliorate](https://help.openai.com/en/articles/6825453-chatgpt-release-notes)… ovvero commette meno errori, più difficili da trovare).

Nel portare un passo avanti quella logica da profeti di sventura, si potrebbe dire: come faranno quelli che erano dipendenti dalle calcolatrici tascabili, oggi che sono praticamente sparite dal commercio?

Senza scomporsi più di tanto, i presunti sventurati apriranno il [Calculator Drawer su Internet Archive](https://archive.org/details/calculatordrawer) e si divertiranno da pazzi con quattordici emulatori delle migliori calcolatrici programmabili in commercio ai tempi felici dove tutti saremmo usciti male dal non sapere più fare i conti da soli.

*Tip of the hat* bonus ai profeti di sventura della tecnologia che scompare e nessuno saprà più come riportare in vita, che si ritrovano davanti all’ennesimo caso di patrimonio digitale reso di accesso planetario e quindi virtualmente incancellabile, almeno fino a che su questo pianeta esisterà una civiltà intelligente con accesso a fonti di energia a costo sostenibile.