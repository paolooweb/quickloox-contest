---
title: "La desolazione dell’arcipelago"
date: 2022-07-29T02:05:36+01:00
draft: false
toc: false
comments: true
categories: [Internet]
tags: [amministrazione pubblica]
---
C’è questo arcipelago composto da una isola madre e tante isolette sparse in giro. Per motivi vari, c’è una ampia rete di traghetti da e per l’isola madre, ma di norma le isolette non sono collegate tra loro.

Il modo più veloce per andare dall’isoletta A all’isoletta B sarebbe navigare da A a B. Invece bisogna andare da A all’isola madre e da questa a B. I tempi e i costi ovviamente sono un multiplo.

C’è un’altra questione. La sanità sulle isolette è di difficile gestione, perché a volte lo specialista per una malattia importante sta su A e il malato sta su B. Per fare incontrare l’esigenza con il servizio, bisogna farli viaggiare entrambi oppure uno dei due deve sobbarcarsi la doppia tratta. Accade così che si arrivi a volte troppo tardi.

Anche gli scambi commerciali e pratici tra le isolette sono difficili. Se c’è di mezzo merce deperibile, per esempio, il carico perde più facilmente in valore.

Ma il peggio assoluto sono le diatribe, perché le isolette, nessuna esclusa, sono gelose delle proprie prerogative e della propria sovranità. Se A e B non si intendono, qualcuno di buona volontà va da A, ascolta, prende appunti, poi torna sull’isola madre e va a riferire a B, che replica. E si torna all’isola madre e poi verso A, in un estenuante ping-pong con due sole certezze: impossibile capirsi veramente e permanenza indefinita di qualsiasi problema appena complesso. Neanche parliamo del caso in cui il contenzioso interessi tre isolette.

Incontrarsi all’isola madre è fuori questione: ogni isoletta vuole conservare lo status quo, se possibile guadagnare posizioni, evitare che altre isolette guadagnino. In campo neutro potrebbe essere necessario fare concessioni, raggiungere compromessi, dover plasmare un accordo nuovo. Dal punto di vista della conservazione dello status quo, è un disastro. L’isola madre non sarebbe inoltre contenta dell’emancipazione eccessiva delle isolette, così persegue la sua politica dei trasporti centralistica e inoltre disincentiva ogni contatto che non passi da lei.

Ora ci si potrà chiedere se questa descrizione corrisponda all’amministrazione pubblica italiana, oppure alle aziende con una comunicazione disorganizzata e affidata prevalentemente alle telefonate.

Oppure ad ambedue.