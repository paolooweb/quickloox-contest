---
title: "Un fatt di attezione"
date: 2019-02-06
comments: true
tags: [Fatt]
---
Scrive (e pubblico) un amico (che ringrazio):

**Programma di fatturazione elettronica.** Garantito (leggi imposto) dal commercialista del cliente.

Mi trovo nello studio per fare tutt’altra cosa *ma siccome sono esperto di
Mac* magari posso dare una mano.

Citazione del commercialista:

>Nessuno con Mac ha il problema x.

Situazione e soluzione proposta dal commercialista:

>Questo Mac restituisce l’errore x quindi va portato in assistenza.

Più per curiosità che altro, smanetto un po’ e intanto che registro commenti e invenzioni varie, *devi usare solo questo browser durante l’allineamento dei pianeti*, *devi usare il cavo di rete perché il Wi-Fi potrebbe essere un rischio per gli hacker*, *Chrome è il browser su cui è stato modellato il software perché è multipiattaforma*,

mi accorgo che l’account del Mac è un utente semplice.

Mi collego con account admin e tutto funziona perfettamente senza che altro sia modificato.

Ho provato da diversi Mac. Con gli account utente (o Guest) il programma non funziona.

Non ti tedio con gli errori che incontro. Di fatto, con un account Admin tutto funziona perfettamente.

Un programma lato server che pretende che io usi un account admin. Non mi era mai successo.

Chiedo confronti con *gli altri Mac* tutti hanno account admin.

La ciliegia: la schermata di errore che ti allego. La cura con cui è realizzata credo che dica molto della software house.

**Costo: 150 euro/mese per l’utilizzo del software**.

 ![Attezione!](/images/fatt.jpeg  "Attezione!") 

Aggiungo un mio commento. Quando ho dovuto occuparmi di fatturazione elettronica, mi sono impegnato da subito per individuare una soluzione gratuita. Non per il costo (che anzi per qualcuno può persino rappresentare un vantaggio), ma per principio: non esiste che una attività obbligatoria che era possibile svolgere in forma gratuita, debba diventare a pagamento a causa di un vincolo arbitrario e stupido. Nessun problema (anzi) che tutte le fatture dell’universo passino dall’Agenzia delle Entrate. Ma lo stato che vuole le fatture elettroniche ha il dovere di facilitare e agevolare il compito a chi le deve emettere.

Invece è il contrario. E c’è chi ne approfitta. Poi scrive *attezione*. Chissà la cura con cui è scritto il codice. Farei *attezione* a usare un altro software.