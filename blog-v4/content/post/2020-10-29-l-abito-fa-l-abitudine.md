---
title: "L’abito fa l’abitudine"
date: 2020-10-29
comments: true
tags: [SF, NY, Apple, font, Francisco, York]
---
Per un lavoro vagamente creativo ho installato i [font di Apple](https://developer.apple.com/fonts/), San Francisco e New York. L’ho fatto per puro dovere, ma ora devo ammettere che non sono affatto male.

Sì, lo so che Apple li usa ovunque e non sono una novità. Nondimeno, dentro un documento scritto o un layout grafico hanno un perché autonomo, mentre fusi dentro la comunicazione di Apple sembrano semplicemente un’altra cosa fatta da Apple, pulitini, quasi anonimi.

In particolare, New York mi ha sorpreso. Sulla carta non gli avrei dato un centesimo. Invece possiede una bella energia.

Attenzione alle librerie di simboli presenti sulla pagina, perché sono davvero corpose e piene di spunti.

Non sono il mio genere di font per l’uso massiccio quotidiano. Però averci a che fare è stato molto piacevole per l’occhio e il lavoro è stato vestito tipograficamente come meritava. Mi sono già abituato a vederli.