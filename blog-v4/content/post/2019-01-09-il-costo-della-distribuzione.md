---
title: "Il costo della distribuzione"
date: 2019-01-09
comments: true
tags: [Discord, Steam, Apple, Epic, Ars, Netflix]
---
Molto rumore per Netflix che [toglie dalla sua app l’interfaccia per abbonarsi tramite App Store](https://techcrunch.com/2018/12/31/netflix-stops-paying-the-apple-tax-on-its-853m-in-annual-ios-revenue/), come ha già fatto su Google Play. Lasciando solo opzioni di abbonamento esterne ad App Store e Google Play, evita la trattenuta del quindici percento richiesta dal distributore, Apple o Google in questo caso.

(A differenza di quanto si può leggere pressoché ovunque, Netflix [ha sempre pagato il quindici percento](https://daringfireball.net/2019/01/netflix_itunes_billing) su App Store).

Il fenomeno è in crescita, con gesti eclatanti come quello di Epic Games che [ha messo in vendita Fortnite per Android fuori da Google Play](https://macintelligence.org/posts/2018-08-04-periferie-problematiche/).

È strano però che non si vedano disamine serie. Il tono dei commenti va dalla *Apple tax* a *incassano il quindici percento praticamente senza fare niente*. E nessuno che abbia affrontato la questione su un piano strettamente fattuale: quanto costa oggi fare andare avanti un app store?

Può essere che la forbice 70/30 sia stata superata dai tempi. Discord, uno store molto di nicchia (nasce come piattaforma di chat per parlarsi a distanza durante il gioco online), si è fatto pubblicità annunciando una forbice 90/10, con lo slogan *nel 2018 distribuire una app non costa più il trenta percento*.

Peraltro Steam, distributore indipendente di giochi, [applica il rapporto 70/30](https://arstechnica.com/gaming/2018/12/discord-store-to-offers-developers-90-percent-of-game-revenues/).

Banda, sicurezza, storage, amministrazione abbonati, strumenti di supporto agli sviluppatori… quale percentuale è *veramente* accettabile oggi prelevare sul costo di una app che accetta di farsi distribuire? Ed è giusto che ci sia un margine di profitto, oltre alla copertura dei costi? Di che entità?

Sarebbe bello sentire opinioni, appunto, fattuali, non tipo *dieci percento* per nessun’altra ragione che suona bene dirlo.

Eppure, non trovo alcuna lettura utile.