---
title: "Nuovi mecenati"
date: 2013-07-30
comments: true
tags: [iOS, Giochi]
---
Un gioco che mi ha colpito ultimamente è [Superbrothers: Sword & Sorcery EP](http://www.swordandsworcery.com), nato nel 2011 su iPad e oggi giocabile ovunque, anche Mac e Windows tramite Steam. Teoricamente appartiene al filone dei giochi di ruolo di azione; in verità il genere appare come pretesto per mettere in scena una esperienza grafica e sonora rarefatta, raffinata, guai a chi giocasse senza un paio di buoni auricolari oppure in un ambiente troppo rumoroso per la colonna sonora.<!--more-->

I creatori del gioco sono indipendenti e per loro raggiungere il traguardo di un milione e mezzo di copie è stato un bel successo. Una [infografica](http://www.swordandsworcery.com/news/2013/7/26/sworcery-sales-infographic.html) mostra come sono andate le vendite. Il gioco è decollato su iPad e poi su iPhone.

L’89 percento di chi lo ha acquistato su iOS lo ha pagato a prezzo pieno e il 77 percento di chi lo ha preso per Android lo ha preso in sconto. Più della metà del fatturato viene da iOS, dove è stato venduto un terzo delle copie totali.

Quelli che comprano Android perché tutto deve costare meno, sanno chi ringraziare per avere buon software.

 ![Sword & Sorcery](/images/SS_infographic.jpg  "Sword & Sorcery") 