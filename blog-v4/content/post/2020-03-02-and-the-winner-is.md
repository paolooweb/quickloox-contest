---
title: "And the winner is"
date: 2020-03-02
comments: true
tags: [Butterick, 1917, Oscar]
---
Dopo il [test sui candidati presidenziali democratici americani](https://macintelligence.org/posts/2019-05-02-corpi-elettorali/), basato unicamente sulle scelte tipografiche della propria propaganda elettorale, Matthew Butterick ha applicato la stessa regola ai film nominati per il premio Oscar: unico metro di valutazione, [l’eleganza e la riuscita della tipografia della locandina](https://practicaltypography.com/oscars-2020.html).

Non dico chi ha vinto, ma pare – non l’ho visto – sia un bel film e la coincidenza non è necessaria, ci mancherebbe, però nemmeno del tutto casuale.

In questi giorni c’è più bisogno di alfabetizzazione statistica, ma il virus passerà e torneranno anche gli skill che aiutano a vivere meglio perché in osservanza del bello e dell’esteticamente pregevole. Uno di questi è la tipografia. Gente in quarantena senza troppo da fare: studiare, studiare, studiare tipografia.