---
title: "È arrivata la quattordicesima"
date: 2020-10-31
comments: true
tags: [Mail, Calendario, Mappe, Shortcuts, Stocks, iPadOS]
---
Primo impatto con [iPadOS 14](https://macintelligence.org/blog/2020/10/30/e-tutto-va-bene/): buono.

Niente di eclatante, come è anche logico per la quattordicesima iterazione di un sistema operativo. Alcune delle novità più succose sono poi appannaggio di Pencil, che non uso.

La tipografia è gradevole per il mio gusto; Mail, Note hanno guadagnato una nuova evidenziazione del contenuto in primo piano, cosa piccola e utile; il redesign di Calendario è vincente e ci voleva. Mappe, alla prima occhiata, si è molto raffinato.

Anche Comandi Rapidi e Stocks hanno guadagnato dalla nuova versione del sistema. Rispetto a iPadOS 13 è un passo avanti. L’autonomia e la velocità sembrano quelle del passato, forse meglio, soprattutto per la fluidità di tutte le operazioni. Anche File mi sembra migliorato e lì sarebbe importante aggiungere praticità e precisione. Vediamo.

Non ho critiche particolari da muovere. Mi ritrovo tiepido verso i widget, che sono stati l’attrazione principale dell’annuncio. Sono graficamente molto curati e leggo di tanti sviluppi promettenti, però boh. La schermata zero prima delle app non mi è mai sembrata questa grande idea. Immagino che voglia interessare soprattutto chi usa iPad come macchina da scrivania. Il tempo potrebbe darmi torto, se l’attenzione sui widget in sede di sviluppo continuerà.

Noto pochi cambiamenti su iPad rispetto a quello che leggo su iPhone. Si sa che l’attenzione va ad anni alterni sulle due macchine e dunque, per vedere cose importanti sugli schermi più grandi, attendiamo iPadOS 15.

Problemi zero. Ho visto una pletora di aggiornamenti di app e, senza pretese scientifiche, qui funziona tutto al meglio. Sulla app di Facebook (mi tocca) il pulsante _<_ è diventato difficile da azionare, c’è da toccare qui e là per trovare un pixel ballerino che gioca a nascondino.

Nel complesso, aria più fresca, business ad usual, contento di questo aggiornamento svelto e tranquillo.