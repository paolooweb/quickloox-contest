---
title: "Il paese dei baloccati"
date: 2016-06-24
comments: true
tags: [Asus, Msi, Edge, Opera, browser, batteria, Microsoft]
---
Il vino dell’oste è chiaramente il più buono, tuttavia anche all’autopromozione ci dovrebbe essere un limite: se Microsoft sostiene che il suo *browser* Edge [consuma meno batteria degli altri](https://blogs.windows.com/windowsexperience/2016/06/20/more-battery-with-edge/), questo dovrebbe risultare vero. O almeno quasi vero. Invece, ecco saltare fuori Opera dopo pochissimo a [smentire e dichiarare consumi energetici più giudiziosi](http://www.opera.com/blogs/desktop/2016/06/over-the-edge/) di quelli di Edge.

La polemica è proseguita e ciò poco interessa, perché si tratta di test sempre piuttosto lontani dagli schermi di utilizzo dell’utente tipico e, per quanta differenza faccia il consumo di un *browser* rispetto a un altro, vedo la cosa poco decisiva nel mondo reale. Una cosa resta certa: la dichiarazione di Microsoft, magari non mendace, è sospetta.

Perlomeno né Opera né Microsoft hanno testato i *browser* su computer Asus o Msi e questo, oggi, è un vantaggio: altri produttori hanno finora evitato accuse di [inviare ai recensori macchine più pompate di quelle in vendita nei negozi](http://www.techpowerup.com/223440/msi-and-asus-send-vga-review-samples-with-higher-clocks-than-retail-cards).

Anche qui la polemica interessa poco: Msi sostiene di farlo per facilitare la vita al recensore che, tanto, applicherebbe l’overclock di suo, così glielo fa trovare già pronto. E le differenze sono relativamente minori. Un’altra cosa resta certa: le recensioni di computer Asus e Msi sono da prendere con le pinze rispetto alle prestazioni. E chissà per quanti altri produttori vale lo stesso.

Il paese dei Pc rimane un grande paese dei balocchi, dove però a essere oggetto dei trastulli sono gli utilizzatori finali, cui viene fatto passare un po’ di tutto in allegria, senza che necessariamente sia vero: l’aderenza ai fatti e la coerenza delle dichiarazioni, nel 2016, sono lussi inutili.

Ricordiamocene quando arriverà la prossima comparativa con i Mac, dove verrà fuori che costano di più, e vanno più lenti, e fanno paura ai bimbi, e causano il raffreddore e signora mia, sapesse quanto consuma il *browser*.