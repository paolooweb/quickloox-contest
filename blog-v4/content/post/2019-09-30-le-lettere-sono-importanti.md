---
title: "Le lettere sono importanti"
date: 2019-09-30
comments: true
tags: [pangrammi, Sallows, Hofstadter, Houston]
---
Se mai scriverò un libro che vede la tecnologia come argomento secondario (essendo improbabile che non contenga alcun riferimento tecnologico) finirà che in un modo o nell’altro ci infilerò dentro la [storia dei pangrammi](https://twitter.com/robinhouston/status/1177636866671157248).

<blockquote class="twitter-tweet"><p lang="en" dir="ltr">I think it’s time for a thread on the cultural history of this tweet. (1/?) <a href="https://t.co/qSIJ0IeRTZ">https://t.co/qSIJ0IeRTZ</a></p>&mdash; Robin Houston (@robinhouston) <a href="https://twitter.com/robinhouston/status/1177636866671157248?ref_src=twsrc%5Etfw">September 27, 2019</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

Per la cronaca, il file di testo contenente questo post consta precisamente di novecentoottantanove caratteri.