---
title: "Di bene in meglio"
date: 2016-06-28
comments: true
tags: [MacSparky, watch, watchOS3]
---
Note interessanti da MacSparky, che ha [messo alla prova la beta di watchOS 3 su watch](http://macsparky.com/blog/2016/6/watchos-3-brings-apple-watch-2).<!--more-->

>Da quasi una settimana il mio orologio funziona con watchOS 3.0. È più veloce, più reattivo, e improvvisamente ho ripreso a usare le applicazioni indipendenti. Posso attestare che i miglioramenti non sono meramente ipotetici ma […] possono cambiare drasticamente il modo in cui si usa watch.

>Il Dock di watchOS funziona bene. Ho premuto il pulsante fisico per avere il Dock più volte questa settimana che in tutto l’anno precedente, quando il pulsante richiamava i contatti. La funzione-killer qui è l’aggiornamento delle app in background. Posso considerare l’uso di app contenenti dati sensibili al trascorrere del tempo senza preoccuparmi che possano non essere aggiornati.

>Sono davvero impressionato dall’abilità di Apple di tornare al tavolo di progettazione e migliorare l’interfaccia di watch. Sono perfino più impressionato da quante prestazioni in più riescono a ottenere sullo stesso hardware dove erano così diverse una settimana fa. Semplicemente, non pensavo che fosse possibile.

Iterazione dopo iterazione, migliorare in continuazione. È accaduto con iPhone tra l’incredulità generale e probabilmente inizia ad accadere con watch. Significativo che, quasi dieci anni dopo, Apple continui a sembrare l’unica azienda in grado di ottenere risultati di questo tenore.

*[L’amico Akko sta provando a migliorare la resa pratica del concetto di [libro sul sistema operativo Mac e sul mondo Apple che gli sta intorno](https://macintelligence.org/posts/2016-06-15-un-kickstarter-per-cambiare-il-libro/). Il progetto ha bisogno di [essere divulgato e finanziato](https://www.kickstarter.com/projects/295108589/cuore-di-mela-i-segreti-di-apple-iphone-e-mac) per avere una chance.]*