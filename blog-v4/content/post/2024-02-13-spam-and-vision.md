---
title: "Spam and Vision"
date: 2024-02-13T23:38:06+01:00
draft: false
toc: false
comments: true
categories: [Internet]
tags: [Spam, Vision Pro, Meta Quest]
---
L’email vincitrice della mia giornata è quella che prometteva la possibilità di vincere Vision Pro. Spam del 2024, genuino, stranamente attento all’attualità.

Anche furbo perché, se avessero mostrato davvero Vision Pro, avrebbero anche corso qualche rischio minimo. Invece hanno messo una foto di Meta Quest 3, con doppio risultato: niente di esagerato a richiamare l’attenzione e la sicurezza che, se qualcuno ci casca, se lo è ampiamente meritato.

Alla fine, la foto di uno, il nome dell’altro… quale migliore esempio di realtà ibrida?

<iframe width="560" height="315" src="https://www.youtube.com/embed/Q3L58KyHC_M?si=peqwicPLbqD4ZFfy" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>