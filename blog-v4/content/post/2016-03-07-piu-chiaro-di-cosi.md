---
title: "Più chiaro di così"
date: 2016-03-07
comments: true
tags: [iTunes, errore, MailMaster]
---
Meraviglioso messaggio di errore ricevuto da **MailMaster C.**, che ringrazio per avermi deliziato il fine settimana.<!--more-->

Mi chiedo se c’entri qualcosa la traduzione in italiano. In tutti i casi, la confusione tra mega e giga è stellare.

 ![alt](/images/iphone-errore.png  "title") 