---
title: "Mai da sole"
date: 2018-01-31
comments: true
tags: [Edge, iPad]
---
>Non ditelo a nessuno, ma la versione per iPad è in fase di collaudo interno e funziona bene. Ci vorrà ancora un poco per completarla e per prima cosa la passeremo ai nostri utenti TestFlight per avere la loro opinione prima di renderla disponibile a tutti.

Questo [tweet](https://twitter.com/SeanOnTwt/status/956613157723693056) si riferisce alla versione per iPad di… Edge, il *browser* Microsoft che ha quasi sostituito Internet Explorer e di cui non si sente la minima necessità.<!--more-->

<blockquote class="twitter-tweet" data-lang="en"><p lang="en" dir="ltr">Shh, don&#39;t tell anyone, but the iPad version is in internal testing and looking great. It&#39;ll take a little longer to bake, so we&#39;re going to roll out it to our TestFlight users early next month and get feedback from them before making it widely available. Thanks for using Edge!</p>&mdash; Sean Lyndersay (@SeanOnTwt) <a href="https://twitter.com/SeanOnTwt/status/956613157723693056?ref_src=twsrc%5Etfw">January 25, 2018</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script> 

Speriamo che non si applichi il detto sulle disgrazie.