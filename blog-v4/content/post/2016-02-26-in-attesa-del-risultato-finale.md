---
title: "In attesa del risultato finale"
date: 2016-02-26
comments: true
tags: [emacs, vi]
---
In quello che potrebbe essere il *post* meno interessante della storia del mondo, ho scoperto un [interessante account Google+ per quanti si interessano di emacs](https://plus.google.com/u/0/113859563190964307534).<!--more-->

Seguendo i *post* sono arrivato a un articolo di interesse persino eccessivo, sul [valore posizionale dei tasti rispetto alle scorciatoie](http://xahlee.info/kbd/efficiency_of_keybinding_emacs_vs_vim.html). Scoprendo che sono fatti studi e che è possibile mettersi a calcolare l’efficienza di questa o quella scorciatoia, secondo dove sono posizionati i tasti di cui necessita.

L’articolo partiva dall’ipotesi di confrontare l’efficacia delle scorciatoie di [emacs](https://www.emacswiki.org/) con quelle di [vi](http://www.vim.org), ma non fornisce risultati. Perché questa è una guerra di religione che fa impallidire qualsiasi altra e, qualunque cosa concludesse, rischierebbe il linciaggio telematico.

Aspettiamo un coraggioso che si faccia avanti.