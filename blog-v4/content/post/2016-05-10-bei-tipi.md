---
title: "Bei tipi"
date: 2016-05-10
comments: true
tags: [Typographica, Kerning]
---
Sta diventando un appuntamento fisso e mi rendo conto di quanto poco sia informativo. Eppure è una pagina cui non posso rinunciare, [il meglio della tipografia web per il 2015](http://typographica.org/features/our-favorite-typefaces-of-2015/). Fa respirare.

(Già che siamo in argomento, chi può vada a [Kerning](http://2016.kerning.it), una delle cose tecnologiche migliori tra quelle che avvengono in Italia, con un *parterre* spettacolare).