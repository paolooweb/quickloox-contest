---
title: "Sessanta giorni fine mese"
date: 2014-05-21
comments: true
tags: []
---
Ricorreva l’altroieri l’anniversario dell’apertura del primo Apple Store, tredici anni fa. Oggi i 424 Apple Store fanno più soldi per metro quadrato delle gioiellerie Tiffany e dei grandi magazzini Harrod’s. Al tempo i commenti erano [Mi dispiace Steve, ecco perché gli Apple Store non funzioneranno](http://www.businessweek.com/stories/2001-05-20/commentary-sorry-steve-heres-why-apple-stores-wont-work) o [Apple sta grattando il fondo del barile](http://www.thestreet.com/comment/theturnaroundartist/10002957.html).<!--more-->

In fondo a questo *post* c’è il video di Jobs che mostra il primo Apple Store alle telecamere, sei giorni prima dell’inaugurazione. (Lo so, Fabio, che i video non si vedono nel *feed* Rss, ci arriverò). Ma che ci vuole, ad avere torto tredici anni dopo? Nessuno sa prevedere il futuro. Si può fare [molto meglio](http://www.cnbc.com/id/101509203):

>Hanno sessanta giorni per presentare qualcosa o sparire. Ci vorranno anni perché spariscano i 130 miliardi di cassa di Apple, ma diventerà un’azienda irrilevante […] diventeranno zombi, se non escono con un iWatch.

Era il 20 marzo. Siamo a sessantadue giorni e questa zombizzazione non si vede. Sarà una profezia a sessanta fine mese, come le fatture?

<object width="420" height="315"><param name="movie" value="//www.youtube.com/v/Ce02galgfRo?version=3&amp;hl=it_IT"></param><param name="allowFullScreen" value="true"></param><param name="allowscriptaccess" value="always"></param><embed src="//www.youtube.com/v/Ce02galgfRo?version=3&amp;hl=it_IT" type="application/x-shockwave-flash" width="420" height="315" allowscriptaccess="always" allowfullscreen="true"></embed></object>