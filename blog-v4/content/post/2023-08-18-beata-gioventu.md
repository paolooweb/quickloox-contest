---
title: "Beata gioventù"
date: 2023-08-18T19:45:14+01:00
draft: false
toc: false
comments: true
categories: [Software, Internet]
tags: [NFT, Bored Ape, OpenSea]
---
Nuove tecnologie e nuovi concetti hanno bisogno magari di messe a punto e aggiustamenti, che contrariamente al gusto corrente possono richiedere anni, ripensamenti, aggiustamenti di rotta. Il punto è quanto la realizzazione finale concreta di una nuova tecnologia coincida con le promesse iniziali. Se la distanza è eclatante, possono sorgere dubbi seri sull’opportunità di adottarla.

Nel frattempo, la nuova tecnologia può portare a pasticci, come farebbe qualche adolescente alle prese con il procedere verso l’età adulta mentre le sue facoltà cognitive sono ancora in via di raffinamento e gli tsunami ormonali ottundono il pensiero razionale.

Parliamo per esempio di NFT, una delle tecnologie di web3 che promette la commercializzazione di opere d’arte o creazioni esclusive in forma totalmente digitale, con forme di protezione dell’unicità degli oggetti in commercio e di remunerazione per chi li ha creati.

Ora succede che OpenSea, il maggiore commerciante di NFT, abbia cambiato le proprie regole e deciso che da ora [le royalty per un creatore di NFT saranno opzionali](https://opensea.io/blog/articles/creator-fees-update). Tutto è più sfumato di così; contemporanemente, la possibilità di monetizzare il proprio lavoro era una delle promesse fondamentali di NFT. Al tempo stesso, la problematica riguarda meno l’aspetto tecnologico e più l’orientamento di chi opera in questo mercato e ne stima il valore complessivo.

Sempre in tema, fa notizia una [causa intentata da acquirenti di NFT Bored Ape Yacht Club ai venditori](https://arstechnica.com/tech-policy/2023/08/buyers-of-bored-ape-nfts-sue-after-digital-apes-turn-out-to-be-bad-investment/). Bored Ape è (in brevissimo) una raccolta di figurine digitali con un’aura di esclusività dovuta al coinvolgimento di varie celebrità come Gwyneth Paltrow o il cestista Steph Curry, Paris Hilton, Madonna eccetera.

Tempo fa la casa d’aste Sotheby’s ha venduto un lotto di NFT Bored Ape Yacht Club a prezzi da ricchi e oggi il valore delle figurine suddette si è, a dirlo morbidamente, decrementato in modo apprezzabile. Gli acquirenti sono molto insoddisfatti, un intermediario nella catena digitale è in attesa di processo e la stabilità tecnica dell’operazione potrebbe risentirne, i venditori e Sotheby’s si difendono con veemenza sui media in attesa di farlo con gli avvocati.

Tutto dà l’idea di una profonda immaturità, anche in senso positivo volendo; nell’adolescenza gli errori sono parte importante della crescita e aiutano a formare persone autenticamente adulte.

C’è però dell’adolescenza anche nello spendere cifre a cinque zeri per immagini di gorilla annoiati allo scopo di sentirsi parte di un club esclusivo e poi adontarsi se la realtà si dimostra diversa dall’immaginazione. E non ho bene idea della sua utilità ai fini della formazione adulta.

*Gli aggiornamenti in agosto del blog potrebbero essere irregolari o infrequenti.*