---
title: "Aperto non troppo"
date: 2015-05-04
comments: true
tags: [opensource, ResearchKit]
---
Confesso di fare più fatica che in passato a seguire le evoluzioni di Apple in ambito *open source*.<!--more-->

Le cose stanno andando piuttosto bene in realtà, in un’ottica di apertura: basti per tutti [ResearchKit](http://researchkit.github.io), il telaio per la costruzione di *app* per la ricerca medica.

È l’organizzazione, che si sta perdendo. ResearchKit ha la sua pagina, meritatissima, però distinta da tutto il resto del lavoro; a scrivere [apple-com-opensource](http://www.apple.com/opensource/) si ottiene una cosa del tutto diversa che a scrivere [opensource-apple-com](http://opensource.apple.com); [Mac OS Forge](http://www.macosforge.org) è in stato di abbandono; dalla (buona) [pagina riepilogativa](https://developer.apple.com/opensource/) si finisce su pagine che in comune hanno niente, eccettuata la tematica. Eccetera.

Speriamo che Apple di *open source* ne faccia sempre di più. Un approccio più rigoroso alla raccolta di tutte le iniziative non guasterebbe.