---
title: "Breve storia della nuvola"
date: 2018-04-07
comments: true
tags: [iCloud, Amazon, S3, iPhone]
---
Dai cassetti virtuali è saltato fuori senza motivo speciale il [comunicato del lancio di Amazon S3](http://phx.corporate-ir.net/phoenix.zhtml?c=176060&p=irol-newsArticle&ID=830816).<!--more-->

È stato l’inizio di un cambiamento vero e imponente; oggi lo *storage* via Internet è normalità e un mondo senza cloud pare un’ipotesi assurda. Anzi, viene da scommettere che nel giro di una generazione spariranno i dischi locali, o quasi.

Eppure era il 2006. Ieri. Un anno dopo è arrivato iPhone ed ecco poste le basi per avere il nostro mondo computazionale ovunque, accessibile dal taschino. Nel 2005, a guardarlo adesso, si viveva una sorta di Paleolitico informatico.

Anche una bella lezione di praticità e opportunità. S3 nacque come risposta a un problema di costi: Nessun disco rigido dell’azienda era pieno fino all’orlo. Quindi tutti i dischi rigidi contenevano spazio inutilizzato, per cui si era pagato. Come recuperare il costo dello spazio vuoto? Perché non rivenderlo all’esterno?

A leggerli così sembrano i vaneggiamenti di uno zio tirchio. Hanno “solo” fatto nascere un’industria pressoché nuova e immensa per portata.

Chi vive il mondo digitale con noia o cinico disprezzo perde davvero qualcosa. Le storie degne di essere raccontate sono tante e ancora sorprendenti.