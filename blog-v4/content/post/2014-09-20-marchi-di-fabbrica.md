---
title: "Marchi di fabbrica"
date: 2014-09-21
comments: true
tags: [Apple, Bolt, Einstein, startup]
---
Come al solito, avessi un centesimo per ogni volta che ho sentito giudicare le finiture o i materiali di un Mac, o altro prodotto a scelta, sarei a bloggare da qualche nave di crociera in rotta sui tropici.<!--more-->

Eppure basterebbe la lettura di un articolo come [No, non puoi fabbricare come fa Apple](https://medium.com/@bolt/no-you-cant-manufacture-that-like-apple-does-93bea02a3bbf). Tempo di lettura stimato da Medium: tre minuti.

Il tema sono le *startup* che hanno in mente di produrre qualcosa che ricorda i prodotti Apple nelle tecniche di fabbricazione o nell’effetto finale. *Se lo fa Apple, possiamo farlo anche noi.* Questa è la risposta di Ben Einstein di [Bolt](https://www.bolt.io):

>Se vedete su un apparecchio Apple qualcosa che volete copiare, cercatelo sui prodotti di qualche altra azienda. Se lo trovate, probabilmente potete farlo anche voi. Altrimenti abbassate le vostre aspettative. Sarà molto meglio per la vostra startup.

Qualche esempio di cose che fa Apple e manderebbero in crisi una azienda appena nata:

* Plastica bianca.
* Utilizzo massiccio di macchine a controllo numerico.
* Fori lavorati al laser.
* Confezioni in plastica a stampo.

Ogni tanto mi si chiede perché occuparsi di Apple, che è un’azienda come le altre. Non lo è. Non sempre. Nel bene e nel male. Rappresenta comunque un *unicum* in un mare altrimenti omologato.