---
title: "Meglio al naturale"
date: 2018-03-17
comments: true
tags: [AI, intelligenza, machine, learning]
---
Mi scuso per andare un momento fuori tema e per entrare in tecnicismi forse eccessivi. Solo che, lo avevo magari già detto, sono un sostenitore dell’[intelligenza artificiale forte](https://www.ocf.berkeley.edu/~arihuang/academic/research/strongai3.html): sono convinto che avremo un giorno computer, reti, apparati con coscienza di sé e capaci di chiacchierare con vera partecipazione assieme a un umano (no, il [test di Turing](http://www.turing.org.uk/scrapbook/test.html) è molto più banale).<!--more-->

Da questa posizione, tutto questo ciarlare di intelligenza artificiale dove invece è in funzione un [machine learning](https://www.forbes.com/sites/bernardmarr/2016/12/06/what-is-the-difference-between-artificial-intelligence-and-machine-learning/#2d35409d2742), per quanto sofisticato, infastidisce.

Un apprendimento meccanico prende cinquemila foto di cavalli e, dopo averle metabolizzate, riesce a riconoscere un cavallo (o la sua assenza) nella cinquemilaunesima foto, in quanto ci sono numerosi punti di confronto che coincidono con quanto ha trovato nelle altre immagini.

Oppure riceve in pasto un modello di cavallo (come se per descrivere una frittata dicessi *un disco colorato di tonalità tendenti al giallo che odora di uova*) e poi lo confronta con le foto che vede, per distinguere un cavallo da un non-cavallo.

La chiamano intelligenza artificiale.

Ogni cinquemila foto di cavalli ce n’è una di [Pegaso](https://www.greekmythology.com/Myths/Creatures/Pegasus/pegasus.html). Oppure di un [unicorno](https://en.wiktionary.org/wiki/unicorn). Per l’apprendimento meccanico sono non-cavalli, perché i conti non tornano.

Un bambino di tre anni dice *guarda, un cavallo con le ali!*

Questa è intelligenza naturale. Questo vuole essere il traguardo. Il *machine learning* è solo una parte di un insieme ben più esteso.