---
title: "Avanti, un altro"
date: 2018-04-19
comments: true
tags: [SuperDrive, Evans, Computerworld, floppy, Sony, Blu-ray, Dvd, Betteridge]
---
Jonny Evans si chiede su *Computerworld* [che destino avrà SuperDrive](https://www.computerworld.com/article/3269017/apple-mac/is-apple-about-to-kill-the-superdrive-on-macs.html), visto che in macOS sono rimaste pochissime applicazioni a trentadue bit – destinate a sparire – e una di queste è Dvd Player.

A parte il fatto che suppongo sia sufficiente ricompilare la app per i sessantaquattro bit, la [legge di Betteridge](https://macintelligence.org/posts/2018-02-16-le-applicazioni-di-betteridge/) autorizza a pensare che SuperDrive resterà in vendita come accessorio e in qualche modo si procurerà di tenerlo funzionante.

Oppure no. Come articolo è una *madeleinette* che riporta alla mente i tempi felici in cui Apple veniva criticata per non inserire un lettore Blu-ray nei Mac e da lì, a cascata, [eliminazione prima di eliminazione](https://macintelligence.org/posts/2017-09-28-comincia-per-effe/), alla loro madre: quella del lettore di floppy.

Avvenuta, si ricorderà, dieci anni prima che Sony [ne cessasse la produzione](https://macintelligence.org/posts/2016-06-25-in-avanti-verso-il-passato/).

Ora di camminare guardando avanti invece che le punte di piedi o, come i più tenaci, i talloni.