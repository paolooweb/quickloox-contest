---
title: "Vive la difference"
date: 2021-09-11T00:32:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [Apple Watch, Facebook, Fitbit] 
---
Mi sembra che articoli come [Luxury Surveillance](https://reallifemag.com/luxury-surveillance/) passino il segno, nel loro associare watch o Fitbit a braccialetti di sorveglianza per condannati, solo più costosi e per gente che se li può permettere.

>Le persone pagano di più per tecnologie di tracciamento che vengono imposte ad altri, non volenti.

L’idea di fondo è quella del capitalismo di sorveglianza che è certamente un problema; al tempo stesso, c’è un ampio insieme di persone che paga più volentieri per qualcosa che contenga la parola *capitalismo* all’interno e una sua critica. (Non sto facendo politica, constato un fatto).

I programmi di fitness indetti dalle aziende *trendy* sono al pari del sistema di controllo sociale cinese; indossare un braccialetto per tenere sotto controllo la frequenza cardiaca o il glucosio nel sangue è vicina alle dinamiche sociali del Grande Fratello (quello di Orwell).

Così non funziona. Esistono i problemi di sorveglianza, di tracciamento, di mancanza di privacy, ma esistono differenze tra questo e quel contesto, e non da poco. Accetteresti di equiparare casa tua a un carcere perché abiti a pianterreno e così ci sono sbarre alle finestre, esattamente come in una prigione? Scommetto di no e scommetto pure che la differenza tra un posto in cui si è liberi e un posto in cui si è costretti risulti evidente.

Esattamente come un braccialetto elettronico per detenuti non si può togliere e un watch invece sì. Si può spegnere, si può configurare, si può togliere, si può cambiare con qualcos’altro oppure farne a meno. Qui il capitalismo di sorveglianza c’entra niente.

Bisognerebbe aggiungere che i sistemi Apple hanno un approccio alla privacy un po’ diverso dal resto. I miei dati di frequenza cardiaca raccolti con watch e custoditi su iPhone li ha visti solo il mio medico.

Per quanto riguarda l’aspetto *luxury*, aspetto l’annuncio dei nuovi watch di settima o ottava generazione (ho perso il conto), concedendomi il lusso di comprarne uno dopo avere rottamato un modello di prima generazione, che non aveva alcun senso riparare. Comprarmi un nuovo watch a sette anni di distanza dal primo non mi sembra esattamente una ostentazione sprezzante verso chi non se lo può permettere (e nessuno mai che chieda se per caso quella somma ragguardevole raccolta per un watch sia magari il frutto di piccoli sacrifici o di scelte precise; non è il mio caso onestamente, ma le famiglie dove avere quella cosa al polso è una questione di salute e non è facile arrivarci, ma vale la fatica di farlo, esistono; ne conosco alcune e certo non sono le uniche).

Scrivi che sto pagando per usare la stessa tecnologia usata per sorvegliare le minoranze oppresse dal sistema, autore di *Luxury Surveillance*? Mi spiace, proprio no. Sento grande solidarietà per le minoranze oppresse dal sistema, ma dare un watch a tutte quelle persone sarebbe iniziare a liberarle, non il contrario.

*Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile.*