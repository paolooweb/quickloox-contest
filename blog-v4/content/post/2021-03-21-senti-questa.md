---
title: "Senti questa"
date: 2021-03-21T02:17:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [Spaces, Clubhouse, Apple, iOS, Android] 
---
Nuova funzione di Twitter che [debutta solo su iOS](https://twitter.com/e_pagliarini/status/1373363688216920069) prima di diffondersi nel resto del mondo:

<blockquote class="twitter-tweet"><p lang="it" dir="ltr">Mi è apparso <a href="https://twitter.com/hashtag/Spaces?src=hash&amp;ref_src=twsrc%5Etfw">#Spaces</a> e penso che per <a href="https://twitter.com/hashtag/clubhouse?src=hash&amp;ref_src=twsrc%5Etfw">#clubhouse</a> sarà dura. PS. Spaces inizia da iOS anche su Twitter, il che conferma che quando si parla di audio non c’è partita. Apple batte tutti.</p>&mdash; Enrico Pagliarini (@e_pagliarini) <a href="https://twitter.com/e_pagliarini/status/1373363688216920069?ref_src=twsrc%5Etfw">March 20, 2021</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

Superiorità nelle interfacce di programmazione, nell’affidabilità, nell’autorevolezza. Inferiorità nella diffusione e interessa nulla a nessuno. iOS, quattordici anni più tardi, è sempre avanti  tecnologicamente. Inoltre la windowsvizzazione del mercato (Android al novantacinque percento e iOS le briciole) predetta da infiniti commentatori non solo non è avvenuta; iOS continua a pagare di più gli sviluppatori e a conservare una diffusione del tutto sana e soddisfacente per chi lo produce.

Così per entrare in primavera con qualcosa di rasserenante, nel nostro piccolo.

*Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile.*