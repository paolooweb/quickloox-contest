---
title: "Se me lo dicevi prima"
date: 2019-10-16
comments: true
tags: [Tencent, Safari, Apple, Dai, Zovi, Dino]
---
Abbiamo una risposta ufficiale agli interrogativi sull’[invio di dati della navigazione sicura da Safari al conglomerato cinese Tencent](https://macintelligence.org/posts/2019-10-14-sicuri-a-casa-propria/): accade solo se l’apparecchio è impostato sulla regione cinese ed è la modalità standard di funzionamento del controllo contro i siti fraudolenti.

Apple [ha scritto a iMore](https://www.imore.com/heres-apples-statement-safari-fraudulent-website-warning-and-tencent) e soprattutto su Reddit è apparso [il codice che dirime la questione](https://twitter.com/dinodaizovi/status/1183527857974403073).

<blockquote class="twitter-tweet"><p lang="en" dir="ltr">Yes, the fine print is concerning. However, one can also take a look at the code to see what is really happening (credit to saagarjha on HN for the quick disasm). Only used when locale country code is “CN”: <a href="https://t.co/OWivIaKpS3">https://t.co/OWivIaKpS3</a> <a href="https://t.co/0muoYn6syW">pic.twitter.com/0muoYn6syW</a></p>&mdash; Dino A. Dai Zovi (in SF) (@dinodaizovi) <a href="https://twitter.com/dinodaizovi/status/1183527857974403073?ref_src=twsrc%5Etfw">October 13, 2019</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

Tencent viene usato in Cina appunto, come si scriveva, perché Google non è ammesso. Il sistema vede l’indirizzo IP dell’apparecchio che invia la richiesta di confronto ma sarebbe difficile avere altrimenti una comunicazione. Il meccanismo è simile a quello di Google: Tencent invia all’apparecchio una lista di Url che corrispondono alla ricerca iniziale, però previa una codifica. Ovvero, Tencent (o Google) non sa di che sito si tratti. La lista viene inviata all’apparecchio, dove avviene il confronto vero e proprio.

In pratica, specie per la Cina, è un non problema. Per come è pervasiva la sorveglianza in mille altri modi, questo sistema rivela praticamente zero della navigazione compiuta e utilizzarlo a scopo di controllo sarebbe tempo sprecato.

Siamo ancora amici, insomma. Però, Apple, [se me lo dicevi prima](https://www.youtube.com/watch?v=OuFTF6MffGQ)…

<iframe width="560" height="315" src="https://www.youtube.com/embed/OuFTF6MffGQ" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>