---
title: "Basta il pensiero"
date: 2015-01-30
comments: true
tags: [Jobs, Flash, YouTube, Adobe, Android]
---
Si trova ancora sul sito la lettera aperta di Steve Jobs intitolata [Pensieri su Flash](https://www.apple.com/hotnews/thoughts-on-flash/) e pubblicata in aprile 2010.<!--more-->

>I nuovi standard aperti creati nell’era *mobile*, come Html5, si affermeranno sugli apparecchi tascabili (e anche sui PC). Forse Adobe dovrebbe concentrarsi maggiormente in futuro sulla creazione di ottimi strumenti per Html5 e meno sulla critica ad Apple per lasciare indietro il passato.

Quando iPhone si è lasciato indietro Flash (per ottime ragioni pratiche e non certo per ideologia) sono insorti innumerevoli sostenitori di [Flash](https://www.adobe.com/it/software/flash/about/).

Sappiano che YouTube, da tre giorni, è [preimpostato per erogare da subito a tutti video via Html5](http://youtube-eng.blogspot.it/2015/01/youtube-now-defaults-to-html5_27.html). YouTube è il numero uno indiscusso e inarrivabile per il video su Internet.

Era già chiaro come sarebbe andata a finire (tranne che in campo Android, dove hanno continuato a credere per mesi e mesi a Flash per gli *smartphone*). Bastava fare come Steve Jobs. Bastava pensarci.