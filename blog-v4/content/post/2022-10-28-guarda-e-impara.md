---
title: "Guarda e impara"
date: 2022-10-28T17:46:55+01:00
draft: false
toc: false
comments: true
categories: [Internet]
tags: [All About Apple, Alessio]
---
Alla fine geografia e storia hanno congiurato nel farmi mancare alla [festa per i venti anni di All About Apple Museum](https://macintelligence.org/posts/2022-10-11-adulti-a-ventanni/).

Ho l’appunto di trovare una buona occasione di redimermi. Nel mentre guardo [il video che Alessio e compagni hanno realizzato a celebrazione dell’evento](https://youtu.be/PNgaiIFqHT4) e penso a tutto quello che mi sono perso.

Di cui il video contiene quasi nulla. Mi sono perso amici, convivialità, clima, spirito, passione, entusiasmo, ospitalità, genio, sregolatezza, compagnia e tanto altro.

I luoghi e o modi del museo li conosco a memoria. Fosse solo quello, basterebbe una visita virtuale.

A differenza di me, altri potrebbero avere meno legami con il museo, o nessuno. In questo caso, il video contiene indizi interessanti nel perché una visita al museo è un fatto culturalmente importante e formativo. Perché fosse solo retrocomputing, All About Apple sarebbe come ormai tanti altri. Invece è unico, fatto di gente unica. E, certo, di un’esposizione che, se non unica, è top.

<iframe width="560" height="315" src="https://www.youtube.com/embed/PNgaiIFqHT4" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>