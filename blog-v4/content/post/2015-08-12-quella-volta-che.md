---
title: "Quella volta che"
date: 2015-08-13
comments: true
tags: [MacBook, WeTransfer, Windows, Dropbox, Keynote, QuickTime]
---
Piacevolissima giornata a chiacchierare in trasferta con grandi amici, mentre un [MacBook bianco del 2007](http://www.everymac.com/systems/apple/macbook/specs/macbook-core-2-duo-2.0-white-13-mid-2007-specs.html) inviava un Pdf da quattrocento megabyte via [WeTransfer](https://www.wetransfer.com) a uno stampatore prealpino con Windows e, testuale, *incapace di usare Dropbox*.<!--more-->

Intanto ho anche visto un MacBook gemello preparare e riprodurre video didattici per le scuole superiori sugli argomenti dell’informatica, con Keynote e un programmino appena oltre le funzioni di registrazione di QuickTime Player, per piccoli interventi di montaggio.

Un genio con gli strumenti giusti usati come si deve, fa miracoli anche con un computer vecchio otto anni.