---
title: "Un uomo, una parallasse"
date: 2013-07-01
comments: true
tags: [iOS]
---
Bellissimo l’articolo di Marco Tabini su Macworld.com dedicato alla [tecnologia che sta dietro l’effetto parallasse di iOS 7](http://www.macworld.com/article/2042808/inside-the-technology-behind-ios-7s-parallax-effect.html), su tutti gli schermi per l’autunno, a parafrasare i lanci cinematografici.<!--more-->

Tutta la parte tecnica è preziosa. Gli oggetti tendono ad apparire più grandi quando sono più vicini per via delle leggi della prospettiva e hanno una velocità apparente superiore; giroscopi e accelerometri su iPhone interpretano il movimento dell’apparecchio; una stima di come venga impugnato l’apparecchio e la giusta organizzazione software creano l’illusione di piani diversi che mediante l’effetto parallasse danno un effetto di profondità.

Ingegnoso e straordinario, obbligatorio per conoscere nozioni che dovrebbero appartenere alla cultura generale e per capire che cosa teniamo in mano. Noioso, pure. Tabini coglie il senso vero della novità a inizio articolo:

>L’effetto parallasse è destinato a cambiare il modo in cui si interagisce fisicamente con l’elettronica da tasca.

Già adesso l’interfaccia di iOS cambia le ombre intorno ai pulsanti secondo l’orientamento e i movimento di iPhone. Aggiungere l’effetto parallasse è un ulteriore passo verso il modo in cui i nostri sensi captano e interpretano la realtà. iOS 7 aggiungerà uno strato in più di connessione emotiva tra noi e l’oggetto.

C’è già chi parla di trucchi da prestigiatore, cosmesi per nascondere la mancanza di idee migliori. Tutto può essere: c’è gente convinta che la discriminante per un buon apparecchio da tasca sia poter passare suonerie all’amico via Bluetooth oppure montare due Sim, o avere lo schermo grande grande pur stando in (qualche modo in) tasca.

Opinione personalissima: è gente che poi ti guarda come se tu fossi un telefono, capace di connessioni unicamente utilitaristiche. Preferisco essere ingenuo e cercare una connessione emotiva con un oggetto elettronico dopo avere fatto lo stesso con chi mi sta intorno.