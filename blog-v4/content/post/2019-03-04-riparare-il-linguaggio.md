---
title: "Riparare il linguaggio"
date: 2019-03-04
comments: true
tags: [Flexgate, iFixit, MacBook, Pro]
---
La progettazione dei MacBook Pro soffre o soffriva di un [problema](https://ifixit.org/blog/12903/flexgate/) causato dall’usura prematura dei cavi che collegano il video alla scheda logica e su iFixit, che vive attorno alla riparazione degli oggetti hardware, non pare vero di poterlo chiamare *flexgate*: il nuovo scandalo da mettere sulla bocca di tutti, la nuova parola d’ordine da giocarsi ogniqualvolta possibile, per darsi l’aria di saperla lunga, di parlare il gergo degli iniziati.

Solo che la parola *flexgate*, così affascinante e sensazionalista, forse dà anche fastidio; è infatti presente nel titolo della pagina, ma non nel titolo dell’articolo. Sa di modifica effettuata a posteriori. Senza alcuna prova per affermarlo con certezza, si può nutrire il dubbio che sia dovuto a commenti [di questo tenore](https://ifixit.org/blog/12903/flexgate/#comment-162661):

>A latere: il termine “flexgate” suscita in me una forte reazione negativa.
Il Watergate fu uno scandalo enorme che portò, unica volta nella storia, alle dimissioni di un Presidente americano.
Anche nel contesto delle riparazioni tecniche problematiche, questo difetto fatica a qualificarsi come scandalo di qualsiasi tipo. È un problema frustrante ma relativamente minore, che Apple deve ancora risolvere. Giusto lamentarsene, ma non tutti i lamenti si meritano il suffisso “gate”. Il termine usato in questo modo perde di significato e alimenta scorrettamente l’idea che Apple abbia commesso qualche azione orribile.

Commento discutibile magari, però circostanziato. Lo leggi, fai due più due e forse concludi che a gonfiare troppo uno scandalo che non lo è rischi di alienarti lettori, magari lettori intelligenti ed equilibrati nel giudizio, più preziosi del becerume che fa massa.

Ho scritto *soffriva* perché sarebbe arrivato un correttivo alla situazione, in forma di [un cavo video due millimetri più lungo di prima](https://ifixit.org/blog/13979/apples-2018-macbook-pros-attempt-to-solve-flexgate-without-admitting-it-exists/), come tale più adatto ad assecondare le ripetute aperture e chiusure dello schermo.

Ma sia chiaro: è un *tentativo* di soluzione *senza ammettere che esista il problema*, specifica il titolo. Il meme che vuole le aziende informatiche renitenti ad ammettere un problema è invulnerabile a qualunque attacco della realtà, la quale mostra spietatamente che un programma straordinario di riparazione gratuita costa molto meno di un danno alla reputazione causato da un numero molto elevato di prodotti difettosi. In mancanza di programmi di riparazione, si deve supporre che il problema sia circoscritto e quando cento persone manifestano un problema, su un milione di macchine vendute, significa che il problema è circoscritto.

Lo hardware, comunque, si cambia. Il modo di trattare questi temi sui media, una volta la carta e oggi i pixel, sembra invece impossibile da riparare. Ci vorrebbe un iFixit per il giornalismo.
