---
title: "Invenzione e innovazione"
date: 2015-08-30
comments: true
tags: [Lisa, LibreOffice, Xerox, Apple, LaserWriter, WoW]
---
Per la categoria *da riscoprire* segnalo [Inventing the Lisa User Interface](http://klynch.com/documents/inventing-lisa.pdf), articolo lungo quanto serve per essere accurato e breve il giusto per essere preso con leggerezza.<!--more-->

La storia di Lisa va riscoperta perché racconta di un computer mai esistito fino al quel momento, comprato da pochissimi e sepolto (l’invenduto) in discarica dopo essere stato proposto a un prezzo astronomico persino per l’epoca.

Viene da chiedersi quanto fosse astronomico per ingorde brame di profitto e quanto invece perché c’era bisogno di un clima di creatività e innovazione per arrivare al prodotto. In altre parole: quanto fosse *necessario* che Lisa avesse un prezzo astronomico, per poter essere Lisa e non un qualunque altro computer.

L’articolo offrirà spazi di riflessione su come si inventa una interfaccia utente e confuta [per l’ennesima volta](https://macintelligence.org/posts/2014-08-25-azioni-che-contano/) l’idea che Apple abbia carpito l’interfaccia grafica a Xerox; c’era un accordo di cessione di azioni in cambio di denaro e visite ai rispettivi laboratori di ricerca.

Più di questo, appunto, la domanda: qual è il prezzo giusto per l’innovazione? Per Lisa era eccessivo; per la LaserWriter? Per World of Warcraft? Per watch? Pensiamo che l’*open source* (di cui sono paladino, sostenitore e guai a chi si ostina a usare Office al posto di [LibreOffice](http://www.libreoffice.org)), visto dal cittadino medio come gratis, porta un sacco di cose utili, e pochissima innovazione…