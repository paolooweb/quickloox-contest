---
title: "Raccolta differenziata"
date: 2016-05-16
comments: true
tags: [Icahn, Cina, Cook, Apple]
---
[Commentavo](https://macintelligence.org/posts/2016-04-30-aria-pulita/) l’uscita di Carl Icahn dall’azionariato di Apple parlando di aria più pulita all’assemblea degli azionisti.

Ora leggo che l’azienda di investimenti governata da Icahn si è vista abbassare il *rating* del debito secondo Standard & Poor’s da BB+ a BBB-.

Secondo cioè la felice sintesi di *Business Insider*, [Carl Icahn è ufficialmente pattume](http://uk.businessinsider.com/carl-icahn-is-officially-junk-2016-5).

Pare che uno dei motivi del *downgrade* sia dovuto alla scarsa liquidità del gruppo.

Questo spiega meravigliosamente il perché Icahn abbia voluto mettersi in tasca due miliardi di dollari vendendo Apple e si ha l’impressione che le difficoltà di quest’ultima sul mercato cinese (mica per niente Tim Cook [è molto attivo a riguardo](http://www.wsj.com/articles/tim-cook-visits-china-in-hopes-beijing-will-take-another-bite-of-the-apple-1463399238)) c’entrino davvero poco.

Altro che aria pulita all’assemblea degli azionisti: questa era la separazione della frazione non riciclabile.