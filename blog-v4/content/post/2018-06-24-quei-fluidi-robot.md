---
title: "Quei fluidi robot"
date: 2018-06-24
comments: true
tags: [Daleks, SheepShaver]
---
Dai backup è saltata fuori una vecchia copia di [Super Daleks](https://download.cnet.com/SuperDaleks-OS-X/3000-2119_4-10058917.html) la quale peraltro, su High Sierra, è ingiocabile. Chi fosse pratico potrebbe lavorare nel codice sorgente, magari, essendo i difetti solamente grafici.

Ho provato a recuperare una copia del giochino adatta ai tempi moderni, senza successo. Ci sono diverse versioni di Daleks, però richiedono tutte l’emulazione. L’elenco più completo è quello di [Mac GUI](https://macgui.com/downloads/?mode=search&search_keywords=daleks&cat_id=56). Il premio per la migliore presentazione del programma va a [Daleks Forever](http://www.nonanone.com/Daleks/). Chi non ci avesse mai giocato, dovrebbe partire da qui.

Al momento, per provare l’esperienza con High Sierra, occorre emularla, [dentro SheepShaver](http://www.columbia.edu/~em36/macos9osx.html) oppure in una qualunque macchina virtuale che faccia girare Mac OS 9 o un Mac OS X sufficientemente *vintage*.

È un bel modo di razionalizzare lo shock culturale che indusse il primo Mac (la percentuale di chi ha giocato a Daleks tra quanti hanno usato il primo Mac deve stare attorno al novantanove percento del totale). Daleks in quanto tale è solo un adattamento di un'idea di gioco molto semplice che esisteva da tempo. Giocarlo [su web](http://www.isaacsukin.com/sites/daleks/index.html), senza animazioni, grafica elementare, illustra come i cervelli di quei tempi vedevano un campo di gioco dove il protagonista viene incalzato da robot sempre più vicini. Poco più dell’idea astratta, giusto il minimo.

Poi arrivò Mac. La grafica era sempre elementare, solo che i robot si distinguevano da una macchia di polvere sullo schermo. E *scivolavano* fluidi e inesorabili verso il protagonista, invece che farsi cancellare e ridisegnare in una nuova posizione. Veniva da affrettare la mossa, che in un gioco a turni non ha senso. Ed è il segno distintivo di un grande gioco a turni.