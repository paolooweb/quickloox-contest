---
title: "Rock e lento"
date: 2014-08-04
comments: true
tags: [Office, Pages, Numbers, iPad, iPhone, Pdf, Microsoft, Keynote]
---
Quella distinzione fatta in televisione da Adriano Celentano.<!--more-->

Lavori su iPad e vuoi esportare un foglio di calcolo, una presentazione, un documento di testo in formato Pdf?

Se lo fai da anni grazie a [Pages](https://itunes.apple.com/it/app/pages/id361309726?l=en&mt=8), [Numbers](https://itunes.apple.com/it/app/numbers/id361304891?l=en&mt=8) e [Keynote](https://itunes.apple.com/it/app/keynote/id361285480?l=en&mt=8), sei *rock*. (E lo fai bene anche da iPhone).

Se hai aspettato [fino a settimana scorsa](http://9to5mac.com/2014/07/31/microsoft-releases-office-for-ipad-update-with-pdf-exporting-presenter-view-third-party-fonts-more/) che Microsoft aggiornasse Office, sei lento. (E su iPhone voglio un po’ vedere).