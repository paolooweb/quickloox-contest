---
title: "Chi non vuol sentire"
date: 2019-12-13
comments: true
tags: [AirPods, iPod, iPhone, Mac, iPad, Asymco, Dediu]
---
Che bello leggere Asymco: ieri il vento ha ripulito il cielo e l’aria nei dintorni di casa mia e Horace Dediu ha questo potere di fare lo stesso, rendendo evidenti tendenze che lo sarebbero se i media non si muovessero come i proverbiali ciechi che si reggono a vicenda e finiscono nel fosso (con tutto il rispetto eh).

Dediu mostra [come sia stato iPod](http://www.asymco.com/2019/12/12/ipods-pro/) a cambiare la percezione di Apple presso il grande pubblico e iniziare la grande crescita che ha portato a oggi.

Le forze di marea che hanno portato Apple dove si trova oggi, nella storia, sono state Apple ][ Mac, iPhone, iPad. E iPod, da nessuno ricordato se non da chi tiene conto delle cifre.

Cifre alla mano, nonostante la reticenza di Apple nel collegare precisamente gli incassi ai prodotti, la prossima forza trainante per Tim Cook e compagnia sarà… [AirPods](https://www.apple.com/it/airpods-pro/).

A breve, gli AirPods avranno realizzato più di quanto abbia fatto iPod.

C’è una conseguenza mediale: a breve sentiremo gente blaterare di Apple che non pensa più a Mac perché troppo impegnata sulle cuffiette.

Gente che diceva la stessa cosa di iPhone.

E che le cuffiette antirumore le ha indipendentemente da che cosa si trovi dentro le loro orecchie. Proprio non ci sentono e soprattutto non vogliono sentire.
