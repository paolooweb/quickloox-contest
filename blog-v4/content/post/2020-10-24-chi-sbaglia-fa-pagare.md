---
title: "Chi sbaglia fa pagare"
date: 2020-10-24
comments: true
tags: [BBEdit, Arm, Silicon]
---
Il [nuovo aggiornamento di BBEdit](https://www.barebones.com/support/bbedit/current_notes.html) contiene più di venti aggiunte, tra cui il recupero dei documenti che sono stati chiusi senza salvare (praticamente l’impossibilità di perdere dati); più di venticinque cambiamenti; più di sessantacinque *bug fix*.

*En passant*, è pronto per [Apple silicon](https://www.apple.com/newsroom/2020/06/apple-announces-mac-transition-to-apple-silicon/). Funzionerà nativamente a massima velocità sulle macchine con processore Arm che iniziano ad apparire nel giro credo di tre settimane.

In un universo parallelo c’è chi si lamenta di applicazioni che ancora devono passare a sessantaquattro bit, dopo anni e anni e ancora anni. Sono sviluppatori e software house che vanno stimolati a portarsi in pari, toccandoli nel portafogli se necessario. Per il software obsoleto non devono pagare i compratori, ma i cattivi sviluppatori.