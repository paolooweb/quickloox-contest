---
title: "Radio dicotomia libera"
date: 2021-03-08T00:24:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [Tivoli, Model One Digital+, Tivoli Audio Art] 
---
Tivoli è storicamente un marchio da appassionati della radio. Oggi è un bene di consumo piuttosto normale, reperibile anche attraverso la raccolta punti del supermercato.

Che è esattamente il modo in cui siamo entrati in possesso di una [Model One Digital+](https://tivoliaudio.it/products/model-one-digital-wifi-bluetooth-fm-radio).

L’oggetto è assolutamente piacevole, per l’estetica e per la resa. I bassi sono insospettabilmente vividi, l’uscita è limpida e pulita. Le funzioni ordinarie si governano tramite un bel pulsante come quelli di una volta e una ghiera rotante e cliccabile. Ingegnoso ed elegante, anche se l’albero di navigazione è un tantino involuto. il display è riposante e curato, un po’ lento, ma niente di che. Come radio, è un piacere per l’occhio, l’orecchio e persino il tatto, pulsanti fisicamente all’altezza, nessuna economia.

Questa è la parte scontata. In omaggio ai nuovi tempi, la nostra Tivoli dispone di una serie di funzioni per collegarsi alla rete Wi-Fi di casa, oppure ad apparecchi via Bluetooth, integrarsi con Spotify o un’altra mezza dozzina di servizi analoghi e così via. Così mi ci sono messo a giocare un po’.

Demoralizzante.

L’integrazione nella rete Wi-Fi casalinga avviene secondo un sistema cervellotico per cui c’è bisogno della [app Tivoli Audio Art](https://apps.apple.com/it/app/tivoli-audio-art-app/id1479059200#?platform=iphone) su iPhone, la quale app si collega a una rete Wi-Fi creata dalla radio, dopo di che provvede a collegare la rete Tivoli alla rete di casa. La procedura viene descritta in due modi diversi su due fogli diversi, uno scritto in italiano a complementare un pieghevole stampato in più lingue, la nostra esclusa, e appunto il pieghevole originale.

iPhone può riprendersi la sua impostazione normale dopo questo giro strano, nessun problema su questo. Va anche detto che in casa ci sono due reti Wi-Fi e io volevo collegare la radio *all’altra*, quella che invece l’apparecchio non ha mai riconosciuto neanche per un secondo.

Bisogna inoltre creare una *zona*, funzione pensata per organizzare la rete in presenza di più Tivoli. Farlo è semplicissimo, cambiare nome alla zona impossibile, nonostante la app consenta in teoria di farlo. Mai riuscito ad abilitare l’editing nel campo nome.

A rete attivata, una interfaccia decente permette – il mio caso – di portare la musica da iPhone alla radio. Attenzione, perché *decente* non vuol dire per forza *funzionante*. Il pulsante per abilitare la riproduzione in ordine casuale ha reagito al tocco una volta, su forse dieci tentativi. Quando sono tornato sulla stessa pagina dell’interfaccia, era nuovamente disabilitato.

Sono tornato sull’interfaccia perché, dopo forse un minuto di (ottimo) ascolto, il collegamento si è interrotto e la riproduzione pure. Ho riattivato la riproduzione e, ancora, dopo poco tutto si è fermato. Ci ho riprovato esattamente quattro volte, sempre con lo stesso risultato.

L’unica parte affidabile dell’interfaccia si è rivelata, per fortuna, quella di aggiornamento firmware. Ho provato ad aggiornarlo perché magari avrebbe potuto portare qualche vantaggio nell’affrontare il problemino appena presentato di stabilità della connessione. Nessun risultato purtroppo. Da notare che il nuovo firmware installato, come da messaggio sull’app, aveva numero di versione *p0.0.0*. Mi chiedo che versione fosse preinstallata sulla radio.

Ora, capita a tutti di incappare in un esemplare difettoso, oppure avere  magari un Wi-Fi con interferenze che pregiudicano l’esperienza, o altro. Quando una cosa non funziona, a volte, non sempre, però qualche volta sì, sbagli qualcosa tu.

Sono dispostissimo a pensare che sia così. Tuttavia le recensioni su App Store usano toni che fanno sembrare i miei quelli di Mammolo; e poi mi sono ritrovato  a navigare per alcune pagine del sito Tivoli italiano. Dico solo che – ricordo, si parla di radio, di musica – *playing* è tradotto con *giocando* e *balance* viene reso con *saldo*.

Una dicotomia pazzesca insomma, mai vista a questo livello. Se avessi speso duecentonovantanove euro, invece dei punti Esselunga, sarei un cliente deluso. L’oggetto è bello, è speciale, esce dai canoni usa-e-getta della nostra epoca, ne sono innamorato e giuro che stasera l’ho acceso per il piacere di usare fisicamente i comandi. È una radio bella persino da accendere, prima ancora di ascoltare. Raccomandata.

Il software si trova in uno stato abietto di disfunzionalità, pensato da un ufficio complicazioni affari semplici e implementato dal *cugggino* di chi ci lavora. Non credo di avere avuto esperienze altrettanto frustranti in questo campo. Visto il livello di prezzo, non sottovaluterei l’ipotesi di portarlo a trecentoquarantanove, che fa poca differenza in questa fascia, e con i proventi pagarsi un programmatore capace.

*Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile.*