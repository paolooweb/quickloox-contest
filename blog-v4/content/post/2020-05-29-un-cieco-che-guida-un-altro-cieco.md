---
title: "Un cieco che guida un altro cieco"
date: 2020-05-29
comments: true
tags: [Castor, Swift, Playgrounds, Apple, iPad]
---
…gli insegna a programmare.

Quattro minuti e mezzo di [video su Jordyn Castor](https://www.youtube.com/watch?v=ETc6c9A-ibg&feature=youtu.be), ingegnere software di Apple nata non vedente, che insegna _coding_ attraverso [Swift Playgrounds](https://www.apple.com/swift/playgrounds/) su iPad a studenti ipovedenti (comunque più vedenti di lei).

Ragazzi e ragazze felici attorno a un drone e carriolate di iPad, in un servizio televisivo indipendente e non un articolo scritto da Apple per il proprio sito.

Dopo avere visto questo, pensi alle polemiche su iPad, se sia un vero computer oppure no, e le derubrichi molto velocemente.

<iframe width="560" height="315" src="https://www.youtube.com/embed/ETc6c9A-ibg" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>