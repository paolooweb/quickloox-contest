---
title: "Fame di bottino"
date: 2023-10-08T14:57:19+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [Lode Runner]
---
L’ho scritto ieri praticamente senza rendermene conto. [Lode Runner](https://loderunnerwebgame.com/game/) di Brøderbund, capolavoro storico del genere platform, si gioca tranquillamente online e funziona.

Bisogna amare i giochi di piattaforma. *Lode Runner* ne è stata una espressione altissima su Apple II. All’inizio è alla portata di tutti. Livello dopo livello, l’asticella si alza impercettibilmente. A un certo punto, molto avanti (il gioco originale è profondo), ti ritrovi a studiare l’intero schema per determinare esattamente il percorso ottimale.

Certo non risolve una serata tra amici. Ma riempie tipo un mese di pause caffè.