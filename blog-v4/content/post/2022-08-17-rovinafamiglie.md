---
title: "Rovinafamiglie"
date: 2022-08-17T01:20:18+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [Toca World, Fantozzi, Villaggio, Paolo Villaggio, Abatantuono, Diego Abatantuono, Vukotic, Milena Vukotic, Pina, Lidia, Nive, Toca Boca, Toca Life World]
---
Mia figlia non ha mai visto e chissà se vedrà mai la clip di [Fantozzi che sospetta una tresca della Pina con il garzone del fornaio](https://www.youtube.com/watch?v=N3tNvThvqHg).

<iframe width="560" height="315" src="https://www.youtube.com/embed/N3tNvThvqHg" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Nondimeno, è il compleanno della sorella, ma è lei a poter giocare con iPad Pro. Ha lanciato [Toca Life World](https://tocaboca.com/app/world/) e ha cominciato a produrre un quantitativo ingente, diciamo decine e molte di esse, di scatole regalo. Poi le ha nascoste in tutti i contenitori e ripostigli dello scenario: armadietti, montacarichi, credenze, scatoloni, ovunque.

Il giorno dopo, iPad Pro toccherà alla sorella, quella che ha compiuto gli anni e non si è accorta dei maneggi all’interno di *Toca Life World*. Avrà una sorpresa.

Storie di rovinafamiglie, Fantozzi e Toca Boca. La seconda, mediante quei videogiochi così nocivi per lo sviluppo delle giovani menti, deviate verso l’individualismo, la dipendenza, l’obesità, la coazione a ripetere, il soffocamento della creatività.