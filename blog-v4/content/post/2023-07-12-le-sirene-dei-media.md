---
title: "Le sirene dei media"
date: 2023-07-12T12:58:38+01:00
draft: false
toc: false
comments: true
categories: [Internet]
tags: [Focus, Hofstadter, Benanti, Paolo Benanti, Douglas Hofstadter, Alla fiera dell’Est]
---
In una successione degna di emulare [Alla fiera dell’Est](https://www.youtube.com/watch?v=Ps0O3Z0UUPY): ho proposto ai [Copernicani](https://copernicani.it) un incontro con [Douglas Hofstadter](https://macintelligence.org/posts/2023-06-17-la-vita-è-fatta-a-strati/). Qualcuno nei Copernicani ha proposto di aggiungere anche padre [Paolo Benanti](https://www.paolobenanti.com). Hofstadter mi ha dato buca. Benanti no. Abbiamo [organizzato l’incontro](https://macintelligence.org/posts/2023-05-27-pescatori-di-prompt/).

Un giornalista del canale [Focus di Mediaset](https://mediasetinfinity.mediaset.it/focus) ha chiesto ai Copernicani di contribuire a un documentario in lavorazione. C’erano due persone disponibili. Una ha dato buca. Io no.

Pare quindi che comparirò, suppongo per una manciata di secondi, in un documentario di Focus Mediaset.

La manciata sarà stata spremuta, come è d’uso, da un’ora abbondante di intervista con il giornalista. In cui ho risposto a quello che mi chiedeva ma ho anche cercato spazi per dire alcune cose che secondo me gli altri ospiti del documentario non hanno trovato tempo o voglia di dire, e avrebbero fatto meglio a dire.

Ovviamente non so dire se ci sarò riuscito o meno. Il tentativo però valeva la pena di concedersi alle sirene dei media.

<iframe width="560" height="315" src="https://www.youtube.com/embed/Ps0O3Z0UUPY" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>