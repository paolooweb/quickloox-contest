---
title: "Alluminio? No, grazie"
date: 2023-09-03T16:40:34+01:00
draft: false
toc: false
comments: true
categories: [Hardware]
tags: [MacRumors, Magic Keyboard, Gurman, Mark Gurman, Bloomberg]
---
Secondo *MacRumors*, ci sarebbe in arrivo una [nuova Magic Keyboard per iPad](https://www.macrumors.com/2023/09/03/gurman-new-ipad-magic-keyboard-to-feature-aluminum/), con parti in alluminio e un *trackpad* più grande, *per dare a iPad ancora di più il sapore di un portatile*.

A parte la caratura di *rumor* della notizia, che arriva da Mark Gurman di *Bloomberg* e dunque *potrebbe* essere vera ma anche non troppo, rimango su [quanto detto giorni fa](https://macintelligence.org/posts/2023-08-29-in-viaggio-con-ipad/): sono molto più interessato a nuove fantastiche funzioni *touch* che al tentativo di portatilizzare iPad per chi vuole un portatile, ma non un Mac. Notizia: Mac è il portatile migliore che si possa comprare.

iPad è un protagonista assoluto della mia giornata tecnologica, a pari merito con Mac mini. Se proprio ho bisogno, prendo la tastiera da Mac mini e la accoppio con iPad. Funziona perfettamente e non capisco che cosa possa servire oltre questo (lo stesso vale per Magic Keyboard).

Massimo rispetto per chi gradirà una nuova Magic Keyboard; semplicemente mi pare una replica simmetrica a quanti chiedono il Mac *convertible* con lo schermo che ruota di trecentosessanta gradi, e poi si stacca magneticamente dal corpo del Mac e diventa un iPad e poi chissà che cos’altro, come farà la Huawei del momento con hardware e software da lasciar perdere o quasi.

Gente, prendete un MacBook Air e un iPad. Non vi ferma nessuno. Pensate di spendere troppo? Ne riparliamo tra cinque anni.