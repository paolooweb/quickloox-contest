---
title: "Schermi mentali"
date: 2022-06-30T16:06:48+01:00
draft: false
toc: false
comments: true
categories: [Internet]
tags: [Slack, huddle]
---
Ho alcune schermate mentali persistenti, che resteranno indefinitamente. Una di queste, nella categoria *incubi*, riguarda una persona di fronte a me, seduta davanti a un computer, che sottoposta a uno stimolo prende una calcolatrice tascabile.

Una variante della stessa schermata mentale concerne una persona davanti a me, seduta di fronte a un computer, che sottoposta a uno stimolo impugna una matita (non una penna, una matita) e un blocco per appunti.

Oggi mi si è impressa in modo vivido la schermata di una persona seduta davanti al computer, con acceso [Slack](https://slack.com), che scrive in chat *ti chiamo* e mi fa suonare il telefono.

Bisogna sapere che Slack ha un livello gratuito e un livello a pagamento. Nel livello gratuito, quando una persona ha aperta una chat con me, può fare clic su una piccola icona a forma di telefono e attivare una videochiamata con me, che per definizione può essere ridotta a una audiochiamata semplicemente escludendo il video.

Se non fosse abbastanza, Slack ha da poco aggiunto gli *huddle*, in pratica audiochiamate cui si possono invitare arbitrariamente le persone iscritte allo stesso gruppo Slack. Far partire uno *huddle* vuol dire fare clic su un’icona a forma di cuffie e invitare la persona o le persone desiderate. È una cosa comodissima e velocissima senza la minima traccia di video.

Ma quella persona toglie le mani dalla tastiera, impugna il cellulare, come minimo dà un comando vocale, si accontenta di una qualità audio inferiore a quella del digitale, si toglie dall’ambiente di comunicazione in essere, che confina con l’ambito della produzione. E costringe me a fare lo stesso.

Difendersi dalle forze dell’abitudine inizia a diventare un imperativo.