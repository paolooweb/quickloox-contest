---
title: "Come prima, più di prima"
date: 2020-10-16
comments: true
tags: [Axios]
---
In diretta dal registro elettronico della scuola di Lidia.

 ![Crash nel registro elettronico della scuola di Lidia](/images/registro.png  "Crash nel registro elettronico della scuola di Lidia") 

Dicevano che non sarebbe stato possibile tornare a com’era prima. Invece niente è impossibile per la scuola.