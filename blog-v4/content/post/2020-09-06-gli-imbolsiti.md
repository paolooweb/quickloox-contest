---
title: "Gli imbolsiti"
date: 2020-09-06
comments: true
tags: [Doctorow, Lanier, Stallman]
---
Mostri sacri dell’epoca digitale che invecchiano senza dignità.

Richard Stallman, l’uomo capace di iniziare [la rivoluzione del software libero](https://macintelligence.org/blog/2019/09/02/cinquanta-di-questi-fallimenti/), [in balia dei minus habens politicamente corretti](https://macintelligence.org/blog/2019/09/29/la-caduta-di-re-riccardo/).

[Cory Doctorow](https://boingboing.net/author/cory_doctorow_1), ispiratore di [Creative Commons](https://creativecommons.org) e pioniere di una nuova legislazione più adatta al copyright al tempo dei computer, ora pubblica un libro su [come distruggere il *surveillance capitalism*](https://onezero.medium.com/how-to-destroy-surveillance-capitalism-8135e6744d59): un concentrato di ideologia cieca, pregiudizi, luoghi comunissimi più che comuni, radicalista come chiunque abbia voglia di tutto tranne che di cambiare veramente qualcosa. Il suo bersaglio sono le aziende della Big Tech (che è di moda attaccare) con il loro sistema che finisce per privarci del libero arbitrio a forza di raccolta dati e profilazione.

Ma c’è speranza, il sistema si può distruggere. Non svelo come; faccio solo presente che non è esattamente una intuizione da Nobel, né è particolarmente originale. Facebook e Google sono il male assoluto. Parlando di abusi di tracciamento e sorveglianza, mi aspettavo due paroline sulla Cina, una dittatura al lavoro per arrivare al totale controllo informativo sulle persone, che nemmeno il Grande Fratello di Orwell. Ma non si può usare la parola magica *capitalism*, quindi non vende. Che tristezza finire così, doppia tristezza pensando che parliamo di un quarantanovenne.

Jaron Lanier, *maitre à penser* per eccellenza quando il Web decollava, oggi riesce nell’impresa di pubblicizzare il proprio libro [Dieci argomenti per cancellare all’istante i tuoi account di social media](http://www.jaronlanier.com/tenarguments.html) e intanto farsi [intervistare da GQ](https://www.gq.com/story/jaron-lanier-tech-oracle-profile) per affermare che Facebook potrebbe avere vinto, il che potrebbe significare – bum! – la fine della democrazia per questo secolo, però Facebook potrebbe anche non avere vinto, e allora invece il contrario.

Tutto questo intanto che viene pagato da Microsoft per fare il guru e progettare modalità di interazione per Teams, un altro gioiellino che, se mi pagassero un centesimo di quello che prende Lanier, distruggerei volentieri con un libro *ad hoc*, senza bisogno di inventare niente, tutta esperienza. Come se Microsoft non fosse Big Tech. Come se non [facesse soldi attraverso Facebook](https://www.nytimes.com/2006/08/23/technology/23soft.html). Come se fosse realmente preoccupata dei contenuti erogati da Facebook tanto da avere [sospeso le proprie inserzioni](https://www.axios.com/scoop-microsoft-has-been-pausing-spending-on-facebook-instagram-9fca2ee3-acd3-4b78-90c2-ed96846394e3.html). L’anno scorso invece ci ha speso centoquindici milioni di dollari, chissà quanto erano diversi i contenuti.

Microsoft [è azionista, per quanto minuscolo, di Facebook](https://www.reuters.com/article/us-facebook-microsoft/facebook-removes-microsoft-banner-ads-from-site-idUSTRE6145GE20100206). Per Lanier, tuttavia, Facebook è cattivo e Microsoft è un cliente.

È opportuno ricordare che Doctorow e Lanier si sono costruiti una carriera a spiegarci quanto è bella Internet e adesso la mantengono con il racconto di quanto sia terribile. D’accordo che l’età avanza, ma l’impressione è che la rotta non esista, sostituita dal semplice girare le vele per prendere il vento in poppa e pazienza se si va al contrario, o verso il niente. Altra impressione: *pecunia non olet*.

Cavalli bolsi.