---
title: "Quasi per gioco"
date: 2019-08-13
comments: true
tags: [HyperCard, Swift, Playground]
---
Un corollario della [nostalgia per HuperCard e compagnia](https://macintelligence.org/posts/2019-08-12-nuove-carte-da-giocare/) è il desiderio di trovare programmazione semplice da affrontare, con la quale poter fare subito qualcosa, superando la fase dei mattoni da costruzione stile Automator su Mac o Comandi rapidi su iOS.

È agosto, i buoni propositi si fanno adesso. Tempo di considerare Swift. Dopo cinque anni di sviluppo [è un linguaggio maturo](https://swift.org/blog/swift-5-released/) e pronto a tutto.

Quando mi ci sono cimentato, che era uscito da pochissimo, in tre mesi ero diventato in grado di scriverci sopra un [libriccino](https://www.apogeonline.com/libri/swift-lucio-bragagnolo/). E non sono un programmatore.

Non bastasse, due parole: [Swift Playgrounds](https://www.apple.com/swift/playgrounds/). I primi esercizi li ha risolti, con un minimo di guida certo, la primogenita a quota quattro anni, motivata dalle evoluzioni del piccolo alieno Byte. Davvero si impara giocando e all’inizio, almeno, cinque minuti la sera prima di dormire sono tutto quello che è richiesto per passare al livello seguente.

Programmare, anche poco, anche male, anche occasionale, nel 2019 ha senso. Qualunque sia il mestiere, il corso di studi, l’aspirazione, la motivazione. Swift è moderno, ragionevolmente semplice, assai a portata di assaggio. Merita l’assaggio.
