---
title: "Cose che non si possono vedere"
date: 2022-02-16T01:08:11+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [tv, John Siracusa, Disney+, Siri]
---
John Siracusa doveva avere terminato la pazienza, per produrre una [specifica non richiesta di come fare una app per lo streaming](https://hypercritical.co/2022/02/15/streaming-apps) in modo che non faccia schifo.

(Per chi non lo conoscesse, John è stato [il migliore recensore di Mac OS X della storia](https://arstechnica.com/gadgets/2015/04/after-fifteen-years-ars-says-goodbye-to-john-siracusas-os-x-reviews/)).

Ci vuole abbastanza poco e sono consigli di base: rendere ovvio come tornare a quello che si stava guardando prima, chiarire sempre di che parte dell’insieme si tratta se è un video dentro un insieme di video (come una serie TV per esempio), tenere traccia di ciò che è stato fatto e quando, comunicare visivamente con l’utente, consentire la creazione di una lista di media, avere un riproduttore di video organizzato in un certo modo eccetera.

Eppure sembra difficile. Ancora più strano pensando che parliamo di app con dietro multinazionali, che amministrano un business preciso e molto profittevole; una app di streaming non è l’intemerata di un programmatore indipendente che si è divertito nel fine settimana. I seguenti paragrafi riguardano una app di streaming diffusissima, di prima fascia:

> Durante la riproduzione, non c’è un modo di accendere o spegnere i sottotitoli con una singola azione.

> Al lancio, dopo avere selezionato il mio profilo, lo spettacolo che avevo lasciato a metà il giorno prima non era visibile sullo schermo e ho dovuto scorre per ritrovarlo.

> Non ho trovato un modo per ricevere una lista degli episodi oppure una pagina di dettagli sull’episodio che sto guardando.

Nell’articolo c’è anche un bel trucchetto, che non conoscevo, da usare su tv per far ripetere da Siri una frase che non si è capita.

Inviare feedback sullo stato delle app di visione di spettacoli non è esattamente in cima alla priorità di chi vorrebbe godersi un momento di relax in poltrona. Eppure picchiare duro sui supporti tecnici delle aziende è probabilmente l’unico modo di ottenere, a gioco lungo, qualche miglioramento degno di essere ricordato.
