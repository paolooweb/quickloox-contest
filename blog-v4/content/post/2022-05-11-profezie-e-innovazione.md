---
title: "Profezie e innovazione"
date: 2022-05-11T01:43:08+01:00
draft: false
toc: false
comments: true
categories: [Hardware]
tags: [iPod, iPod touch, Bill Gates, Gates, Zune, Kontra, Microsoft]
---
Alla fine si è realizzata la lungimirante previsione di Bill Gates: [iPod non poteva  durare nel lungo termine](https://www.marketwatch.com/story/microsofts-gates-apple-ipod-success-unlikely-to-last). Infatti sono passati appena diciassette anni, letteralmente volati e Apple ha infine dismesso anche iPod touch, ultimo superstite della famiglia, che resta ufficialmente [in vendita fino a esaurimento scorte](https://www.apple.com/newsroom/2022/05/the-music-lives-on/).

Apple avrebbe fatto meglio a dare retta a chi il mercato lo conosce bene e sa coltivare l’innovazione, più che mai a livello di elettronica di consumo rivolta a tutti.

Infatti ha cumulato ben dieci anni di ritardo nei confronti degli innovatori veri, l’avanguardia tecnologica e di design del settore, mica per niente nelle mani di un nume dell’informatica come Gates.

Bastava seguire i pionieri: già nel 2012 Microsoft aveva visto il futuro e aveva [spento definitivamente la linea Zune](https://twitter.com/counternotions/status/1524119332472512512).

<blockquote class="twitter-tweet"><p lang="en" dir="ltr">ElOhEl, it took Apple literally a decade to catch up with Microsoft. All Zune products were discontinued in June 2012.</p>&mdash; Kontra (@counternotions) <a href="https://twitter.com/counternotions/status/1524119332472512512?ref_src=twsrc%5Etfw">May 10, 2022</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

Se solo Apple desse retta a chi ne sa di più, invece di ostinarsi a fare di testa propria.