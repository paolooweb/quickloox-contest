---
title: "Il fine dell’editoria"
date: 2017-04-22
comments: true
tags: [Cannavacciuolo, Stilton, Nori, Basilicata]
---
Due visite a [Tempo dei Libri](http://www.tempodilibri.it/it/) 2017, prima edizione di sempre, a [Fiera Milano Rho](http://www.fieramilano.it/visitatori2).<!--more-->

Sentimenti ampiamente contrastanti. Stranamente per una qualunque manifestazione italiana, gli spazi erano adeguati e il Wi-Fi funzionava *veramente* Niente registrazioni farlocche, un solo tocco per accettare i termini del servizio, nessun bisogno di registrarsi nuovamente dopo uno stop dell’apparecchio, aperte tutte le porte giuste per i dati, velocità decorosa per lavorare oltre che divertirsi.

Certamente le folle della fiera dell’artigianato prenatalizio sono di ben diversa sostanza. Ma di gente ce n’era. Certo, ho visto lo stand con gli *chef* a cucinare e parlare di cucina, quello con i giochi da tavolo, quelli delle Regioni (in che senso la Basilicata, per dirne una a caso tra le presenti, ha l’esigenza di presenziare a una fiera dell’editoria a Milano?), quello della polizia, l’esame gratuito della vista. Però tutto, facciamo quasi tutto, era riconducibile in modo onesto al tema e anche la coda per gli autografi davanti ad Antonino Cannavacciuolo aveva a che vedere con un libro, così come quelle per Geronimo Stilton o per [l’autrice di *Hyperversum*](http://www.ceciliarandall.it/hyperversum-saga/).

Si leggono tante cose sulla fine dell’editoria e invece a Tempo di Libri si è rivisto il fine dell’editoria: raccontare grandi storie, portare conoscenza ovunque a basso prezzo (poche cose costano meno di un libro in rapporto al valore), trasmettere visioni del mondo. Se c’è da fare un regalo: [Strategia della crisi](http://www.paolonori.it/strategia-della-crisi/), di Paolo Nori.

Ho messo un po’ di foto nell’[articolo scritto per Apogeonline](http://www.apogeonline.com/webzine/2017/04/20/immaginare-tempo-di-libri). Ne aggiungo qualcuna ulteriore.

 ![Due Mac, un iPhone e una videocamera professionale a Tempo di Libri](/images/tempo-di-libri.jpg  "Due Mac, un iPhone e una videocamera professionale a Tempo di Libri") 

 ![Montaggio professionale su Mac in mezzo a equipaggiamento video professionale](/images/videocam.jpg  "Montaggio professionale su Mac in mezzo a equipaggiamento video professionale") 

 ![Montaggio video su Mac con iPad accanto](/images/mac-ipad.jpg  "Montaggio video su Mac con iPad accanto") 