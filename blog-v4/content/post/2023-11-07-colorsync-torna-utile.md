---
title: "ColorSync torna utile"
date: 2023-11-07T14:08:18+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [Utility ColorSync, Sonoma]
---
[Anni che aspettavo](https://macintelligence.org/posts/2021-12-09-infine-ma-non-da-ultimo/). Finalmente Sonoma ha chiuso il *memory leak* che affliggeva Utility ColorSync da troppe versioni passate del sistema. Appena si cominciava a lavorare, Mac colmava in pochi istanti la memoria applicativa e poi cominciava a mangiarsi tutto lo spazio su disco. Ottenere un qualsiasi risultato era impossibile.

Utility ColorSync è strumento impegnativo (i filtri devi crearteli tu) però offre controllo totale sui componenti di un file PDF. È possibile creare filtri che riducono le dimensioni, trasformano in bianco e nero, comprimono le immagini ma non il testo e così via.

Con la nuova versione arriva la possibilità di salvare il risultato anche in una varietà di sistemi grafici ([non PostScript](https://macintelligence.org/posts/2023-10-01-lultimo-postscript/)).

Rimane l’impossibilità di [accedere via AppleScript se non in modo surrettizio](https://macintelligence.org/posts/2015-08-01-magia-e-pdf/), però ricomincio a lamentarmi al prossimo giro. Intanto ora lo strumento funziona di nuovo. E lo strumento è superpotente.