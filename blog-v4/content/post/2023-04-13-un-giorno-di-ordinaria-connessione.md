---
title: "Un giorno di ordinaria connessione"
date: 2023-04-13T16:21:48+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [Tailscale, ifconfig]
---
Sembra innocente, questo racconto di come a Tailscale abbiano risolto un paio di bug [grazie a comandi Unix open source](https://tailscale.dev/blog/darwin-spelunking) sviluppati da Apple in supporto ai propri sistemi operativi.

I sottotesti invece offrono spaccati dell'ecosistema che restano regolarmente fuori dalle narrazioni ordinarie.

Apple sviluppa open source, per esempio, e non sotto forma di carta moschicida per sviluppatori o per spingere gli utenti verso i propri prodotti. È vero open source, a disposizione di tutte le persone di buona volontà, con funzioni di supporto o soluzione di problemi.

[Tailscale](https://tailscale.com) è uno strumento davvero speciale per assicurare la connessione tra i propri apparecchi a prescindere dalla loro posizione fisica e logica. Il suo effetto è quasi magico; in più di una situazione mi ha aperto le porte del Mac mini di lavoro da aeroporti, giardinetti, mezzi pubblici, bed and breakfast in mezzo ai monti. In modo elegante e gratis. Il servizio si mantiene attraverso clienti veri che lo utilizzano in architetture di rete di grandi dimensioni e complessità; non è neanche il software open source che si usa senza pagare e con un senso di colpa latente. Il modello di business esiste ed è evidente.

Tailscale è poi un’azienda, vera. Che non si fa problemi a usare software Apple, per risolvere problemi. Basta questo a fare giustizia di tante chiacchiere sulla chiusura, la conformità a un ipotetico standard di mercato, l’assioma che vuole l’impresa equipaggiata con sedicente software per imprese, al prezzo di conseguenze minacciose quanto misteriose e mai veramente chiarite.

Se poi venisse necessità di raggiungere un risultato con la propria rete, `ifconfig` esiste davvero, è open source, è fatto apposta da Apple, espande la conoscenza. Se pare poco.