---
title: "Veloce e silenzioso"
date: 2014-02-04
comments: true
tags: [Samsung, Mac, Ssd, Fariello, all4Mac, Apple]
---
Il mio venerando MacBook Pro 17” di inizio 2009 va avanti. Ho fatto cambiare le ventole, consumate o deformate dall’usura, che grattavano e ruggivano in modo imprevedibile quanto fastidioso per l’ambiente (sonoro). Ora la macchina è di nuovo efficiente con una spesa minima. Soprattutto, è di nuovo silenziosa. Alla peggio il rumore è quello fisiologico delle ventole, quello che dovrebbe essere.<!--more-->

Ho approfittato dell’intervento per un colpo di mano: addio al vecchio disco rigido da 320 gigabyte e benvenuto a un disco a stato solido (Ssd) da cinquecento gigabyte. Né il primo né l’ultimo a raccontare di una macchina avanti con gli anni che rinasce a nuove prestazioni grazie a questo cambiamento: sono anch’io parte del club.

Il disco Ssd è Samsung. Posso tollerarlo, è per una buona causa.

L’intervento è stato compiuto dal bravissimo Roberto Fariello, [all4Mac](http://all4mac.it). Riesce spesso ad arrivare dove il Genius Bar di Apple non arriva, in tempi minori dell’assistenza convenzionale, a costi migliori, con competenza, stile e discrezione. Più che raccomandato.