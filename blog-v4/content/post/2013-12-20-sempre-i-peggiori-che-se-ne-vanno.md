---
title: "Sempre i peggiori che se ne vanno"
date: 2013-12-20
comments: true
tags: [Acer, Apple, iPhone, Mac]
---
Apple pensa solo a iPhone e lascia morire Mac, dice il benpensante.<!--more-->

Nel frattempo si preoccupa di creare un Mac Pro, che pure a fatica sta però arrivando.

Acer, per dire, ha [annunciato](http://www.businesscloudnews.com/2013/12/19/acer-embraces-cloud-as-it-transitions-away-from-declining-pc-business/) che l’azienda intende passare a un modello di *hardware più software più servizi*.

In altre parole, a mare i PC e viva il cloud, viva i server, viva tutto quello che può dare il pane visto che vendere stracci con sopra Windows chiamandoli PC sta planando verso il valore reale. Zero.