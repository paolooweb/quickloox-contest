---
title: "Che combinazioni"
date: 2013-11-25
comments: true
tags: [baseball, Espn, Stephenson]
---
Il dilettante assoluto di calcolo combinatorio che è in me può solo stimare immensamente i coniugi Henry e Holly Stephenson, che per quasi un quarto di secolo hanno compilato i calendari ufficiali del baseball professionistico americano, fino al 2004.<!--more-->

Si tratta di un compito allucinante per variabili e interessi in gioco e loro sono riusciti a funzionare meglio dei computer per molti anni, usando la macchina solo per sgrossare la sfida e poi risolvendo i dettagli a mano, con una perizia infinita.

Espn li ha celebrati con un [video](http://espn.go.com/video/clip?id=9897968) toccante e tenero.

Quelli che li hanno sostituiti [sono in quattro](http://mat.tepper.cmu.edu/blog/?p=1832), con computer che nel 2004 avevano computer mille volte superiori in velocità di quelli del 1994.

Giusto per celebrare i miracoli dell’hardware e ricordare che valgono niente, se non c’è uno bravo messo davanti alla tastiera.