---
title: "La rete fiacca"
date: 2014-08-12
comments: true
tags: [Ikea, Wordpress]
---
Scrivevo quasi un mese fa di [rete stanca](https://macintelligence.org/posts/2014-07-18-la-rete-stanca/) e citavo l’impossibilità di collegarsi al Wi-Fi di Ikea.<!--more-->

Ora ci si collega che è un piacere. A patto di evitare una qualsiasi autenticazione tramite Wordpress, che fallisce.

Meglio evitare di insistere o peggio pensare che si possa trattare di un problema legato a Wordpress invece che al Wi-Fi. Potrebbe venire la malsana idea di provare a cambiare la password per vedere se tutto va a posto. Wordpress invia la nuova password. Che non viene accettata.

La realtà torna a congiungersi con il mondo fisico quando si spegne il Wi-Fi per accendere la connessione cellulare.

Non è più stanca, la rete Ikea. Fiacca. Il morale.