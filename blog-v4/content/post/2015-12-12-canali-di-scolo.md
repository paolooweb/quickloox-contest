---
title: "Canali di scolo"
date: 2015-12-12
comments: true
tags: [Slack, Roll20]
---
Scrivo al termine di una lunga giornata di lavoro e una lunga serata di gioco.

Molta parte della prima si è svolta dentro [Slack](https://slack.com), spazio comunitario semplice e rapido, dove è facile dialogare, condividere documenti, usare qualsiasi apparecchio e qualunque sistema, ignorare i vincoli di spazio e tempo, ottenere il meglio sia quando la comunicazione è sincrona (più vicina alla chat) sia quando è asincrona (più vicina alla posta).<!--more-->

La seconda, come di consueto, ha ruotato attorno a [Roll20](https://roll20.net). Oltre ai malridotti attorno al tavolo che trangugiavano bevande calde tra uno starnuto e un colpo di tosse, avevamo malridotti anche a casa, e tutti abbiamo condiviso mappa del gioco e lanci dei dadi, divertendoci senza escludere alcuno a causa di un raffreddore ancora più grosso.

Gli ostacoli da superare sono state le persone che pensano di lavorare a suon di telefonate e quelle che ritengono di coordinare un gruppo di lavoro mitragliando posta elettronica. Le prime ti impongono una modalità sincrona che impedisce di dedicarsi seriamente a qualunque altra cosa per tutto il tempo della chiamata. Le telefonate dovrebbero essere eventi rari e condensare numerosi punti importanti da dirimere a medio termine, si decide definitivamente e poi si prosegue, oppure essere brevissime e dedicate a un singolo punto importante. Non sono mai brevissime, non condensano mai tutto quello che dovrebbe.

La posta elettronica è meravigliosa, uno-a-uno. Appena compare una copia conoscenza e il gruppo supera le due persone cominciano a moltiplicarsi i messaggi, mentre si divide l’efficacia. Nessuno taglia le parti non più importanti dei messaggi precedenti e arrivano cinquanta righe di cui ne conta una. Le firme aziendali sono diventate la palestra del paranoico, del poliglotta che deve scrivere in cinque lingue gli avvertimenti legali per il messaggio di una riga diretto verso la scrivania oltre il muro, l’eccesso di zelo dell’ambientalista che scrive in fondo a ogni messaggio l’invito a non stampare, ignaro di quanto costano quei bit in emissioni inquinanti.

La trascuratezza di scrivere sopra le citazioni dei messaggi precedenti, al quinto messaggio sullo stesso tema, diventa un fattore di complicazione esplosivo per tenere traccia della faccenda. Se per caso si intrecciano due rami di discorso diversi, magari che coinvolgono in parte o in tutto lo stesso team, la confusione è a un passo, l’inefficienza è assicurata. Non parlo per pietà degli allegati che subiscono modifiche da un messaggio a un altro e viaggiano con il nome sempre uguale ma in versioni diverse. Il sovraccarico di lavoro necessario a comporre una semplice risposta a una semplice domanda è di un’altra epoca, rispetto a sistemi come Slack.

Non basta più lavorare; bisogna anche saper scegliere il giusto canale. Quelli degli anni novanta sono diventati inadeguati. Prendere nota, grazie.