---
title: "Una buona alimentazione aiuta"
date: 2021-01-02
comments: true
tags: [Gruber, Daring, Fireball]
---
Il motivo per cui si va così lenti, oltre al bisogno di smaltire tutti gli zuccheri in eccesso, potrebbe essere una alimentazione non all’altezza del nostro fabbisogno.

Lo mostra bene John Gruber chiacchierando di come [vari alimentatori di Apple siano più che simili nella forma ed eroghino wattaggi tuttavia diversi](https://daringfireball.net/2020/12/charger_nerdery).

I watt dichiarati sono un massimo teorico che non sempre l’apparecchio collegato riesce a sollecitare e ci sono combinazioni particolarmente sfavorevoli, che rispondono chiaramente alla domanda *perché il mio [apparecchio] si carica così lentamente?*

Questo perché l’alimentatore non è in grado di adeguare il wattaggio in maniera continua, ma è in grado di funzionare dentro alcuni intervalli di volt (tensione) per ampère (intensità), gli elementi il cui prodotto è una cifra in watt (potenza).

Gruber scende nel dettaglio con l’esempio di due alimentatori Usb-C di Apple, uno da 29 watt e l’altro da trenta. Sembrerebbero uguali nella sostanza, se non fosse che il primo ha solo due configurazioni:

- 14,5 volt per due ampère, cioè 29 watt
- 5,2 volt per 2,4 ampère, ovvero 12,48 watt

Il secondo offre solo un watt in più di differenza, ma ha il doppio delle configurazioni:

- 20 volt per 1,5 ampère, 30 watt
- 15 volt per due ampère, sempre 30 watt
- nove volt per tre ampère, 27 watt
- cinque volt per tre ampère, sono 15 watt

Se c’è di mezzo una Magic Keyboard per iPad, il terzo caso (i 27 watt) è perfetto e assicura una carica veloce; con il primo alimentatore si ricade invece nei 12,48 watt, meno della metà del possibile. Il caricamento è lento.

Distinguere un modello dall’altro a volte richiede la lettura delle note di servizio stampate in microtesto sull’apparecchio o sulla sua scatola. Insomma, può essere più facile regolarsi quando c’è di mezzo il pandoro.