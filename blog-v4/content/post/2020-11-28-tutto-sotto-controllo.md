---
title: "Tutto sotto controllo"
date: 2020-11-28
comments: true
tags: [Ocsp, GateKeeper, Eclectic, Universal, Binary, M1]
---
A seguito del polverone alzato rispetto alle [politiche di controllo delle firme digitali degli sviluppatori quando parte una app](https://macintelligence.org/blog/2020/11/16/attaccati-a-ocsp/), ho trovato una bella contestualizzazione nel tempo del tema.

L’articolo [sta su The Eclectic Light Company](https://eclecticlight.co/2020/11/25/macos-has-checked-app-signatures-online-for-over-2-years/) ed è appena appena tecnico, ma si legge molto bene.

>La cosa che mi ha reso perplesso è che i controlli Apple via Ocsp sono noti da circa due anni e sono saliti alla ribalta solo ora.

La contestualizzazione mostra che la firma digitale ha iniziato ad apparire sui Mac attorno al 2007 e, negli anni, è stata adottata sempre più intensamente. In tempi recenti Apple le ha dedicato molta più attenzione di prima e l’articolo trasmette una nozione interessante: negli ultimi anni, con un picco nel 2018, sono emerse varie vulnerabilità importanti <em>che sfruttano il formato di app Universal Binary</em>.

Universal Binary è la formula di doppia compatibilità scelta per distribuire in un solo *bundle* software sia la versione Intel del programma che quella M1. Apple sapeva benissimo, nel 2018, che il formato avrebbe acquisito importanza oggi. Guarda caso, ha intensificato la severità dei controlli.

>Quelli che considerano gli attuali controlli Apple sui certificati online come non necessari, invasivi o forme di controllo, dovrebbero avere nozione di come sono comparsi e della loro attuale importanza per la sicurezza di macOS. Dovrebbero inoltre spiegare come mai, dopo avere goduto dei benefici della situazione per un paio di anni, abbiano di colpo deciso che si trattava di una cattiva idea, e dirci con che cosa andrebbero sostituiti.

Amo la privacy digitale e ci tengo; al punto da sollecitare chi agita il tema in maniera acritica a non farle cattiva pubblicità. In particolare, mal tollero le interpretazioni integraliste del tema, dove tutto va visto come il prossimo passo verso lo stato di sorveglianza, senza ragionare, senza capire.

Se i controlli di validità delle certificazioni online da parte di Apple sono così pericolosi, dove sono i danni che abbiamo subito in questo periodo? Dove è la prova che effettivamente i timori sul buon uso dei dati sono fondati?

>Apple sembra avere applicato i suoi controlli nella forma attuale da meno di ventitré e più di sedici mesi, per quanto sia da circa sei anni che riguardano le app in quarantena [secondo GateKeeper].

Parliamone. Se ci dobbiamo preoccupare di qualcosa che va avanti da oltre un anno, come mai diventa importante ora?