---
title: "Lasciano o raddoppiano"
date: 2021-01-16T01:22:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [Tesla, Apple, Asymco, Dediu, Nokia] 
---
Horace Dediu ha aggiornato su *Asymco* la sua [guida all’ingresso nell’industria automotive](http://www.asymco.com/2021/01/13/the-entrants-guide-to-automotive-industry-updated/), perché i tempi cambiano. Si tratta di una industria nella quale, si vocifera da tempo, potrebbe un giorno entrare Apple.

Ingresso sul quale sono abbondati naturalmente i *de profundis*. Dopo i fiaschi clamorosi di Apple nel mondo della telefonia e dell’orologeria, viene naturale credere che il mercato automobilistico sia talmente consolidato e obsoleto da non offrire più sviluppi interessanti.

Invece, delle tante cose che stanno cambiando e anche radicalmente, c’è anche questa. Il mercato dell’automobile potrebbe prestare il fianco a rivolgimenti insospettabili. Ecco un estratto dal pezzo di Dediu.

>Serve una prova più evidente? Tesla ha raddoppiato da sola il valore dell’intera industria automobilistica durante un anno di calo delle vendite.

>Significa che dovremmo avere il doppio delle auto attuali, o che dovrebbero costare il doppio, o che dovrebbero fruttare due volte quello che fruttano oggi. E che dovrebbe accadere alla svelta. Non possiamo avere il doppio delle auto, dato che non avremmo dove parcheggiarle. Non possiamo raddoppiare il prezzo, a meno che si faccia lo stesso con la nostra ricchezza. Non possiamo raddoppiare i profitti, a meno di disabilitare meccanismi di concorrenza e cambiare il sistema di produzione.

>Oppure potrebbe essere che metà del mercato (tutti gli altri costruttori) sia senza valore.

Sembra un azzardo. A meno di ricordare che un tempo, in una galassia molto lontana, c’era un gigante inattaccabile di nome Nokia.

(Intanto inizio a studiare come reinserire i commenti).