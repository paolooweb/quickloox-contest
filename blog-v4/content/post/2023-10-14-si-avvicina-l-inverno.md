---
title: "Si avvicina l’inverno"
date: 2023-10-14T19:56:10+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [Forbes, intelligenza artificiale, AI]
---
Mi lascia perplesso l’affermazione di *Forbes* per la quale [l’intelligenza artificiale potrebbe essere il prossimo business di Apple](https://www.forbes.com/sites/bethkindig/2023/10/13/ai-could-be-apples-next-chapter/).

Il calcolo è del tipo *Apple ha due miliardi di apparecchi attivi e quindi un miliardo circa di utenti. Siccome OpenAI fa soldi con cento milioni di utenti attivi, Apple può contare su una base di utenza dieci o venti volte più grande e dunque può fare soldi vendendo abbonamenti a un proprio servizio di intelligenza artificiale*.

Non fa una piega, ma ignora fatti consolidati, tipo che Apple entra in un mercato solo quando sa di poter fare più e meglio di chi c’è già.

Sempre secondo *Forbes*, Apple avrebbe in pancia un sistema dalle prestazioni simili se non superiori a quelle di ChatGPT, da integrare con Siri. Tutto suggestivo e se ci fossero anche fatti concreti ci sarebbe anche una notizia. Che Apple possa avere contemplato l’inserimento dell’assistenza generativa dentro Siri, ci arriva chiunque con un ragionamento grande quanto un seme di sesamo.

Ho il sospetto che Apple non sia minimamente interessata a fare concorrenza a ChatGPT, anche in considerazione delle limitazioni della tecnologia, e preferisca continuare ad approfondire il lavoro su CoreML, Neural Engine e le altre cose che già fa, oltre alla ricerca pura riguardante il tema.

Aggiungo anche che è arrivato l’autunno perfino in Lombardia. Ancora qualche mese e la cosiddetta intelligenza artificiale sarà uno dei tanti strumenti a disposizione, senza tutto lo *hype* dei mesi scorsi e senza le promesse mirabolanti di progressi che, banale banale, la tecnologia sottostante non è in grado di raggiungere. Il prossimo inverno dell’AI non è proprio alle porte, magari ci arriveremo in primavera, ma le avvisaglie ci sono tutte.