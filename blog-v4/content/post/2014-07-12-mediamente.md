---
title: "Mediamente"
date: 2014-07-12
comments: true
tags: [Bic, Ute]
---
La ragione per cui creare una singola interfaccia che vada bene per tutti è un’arte talmente complessa da rendere molto denaro a chi la sa praticare in modo eccelso, la dimostra Bic – sì, le penne a sfera – che per festeggiare la centomiliardesima biro ha lanciato lo [Universal Typeface Experiment](http://theuniversaltypeface.com/home).<!--more-->

Il sito raccoglie campioni di scrittura a mano dell’alfabeto (è possibile inviare i propri, finora ne hanno ottenuti quasi un milione) e attraverso un algoritmo ricavano la forma di ciascuna lettera come emerge dalla media di tutti i campioni ricevuti.

Affascinante e impressionante: Una C è per tutti una C, però viene disegnata in infinite varianti ognuna differente in qualcosa, come fossero impronte digitali. Abbiamo lo stesso concetto in testa, solo che per ognuno si concretizza in modo estremamente personale. Creare una interfaccia che vada bene per il massimo numero di persone si avvicina al compito di tracciare la T ideale, quella più vicina possibile a ogni singola delle infinite T scritte nel mondo.

Di converso, ecco una lezione per gli amanti del commento sulla personalizzazione, quelli che come vedono una funzione di interfaccia aggiungerebbero un pulsante, chiedono un comando in più, si illudono che inserire opzioni di personalizzazione renda migliore il funzionamento del sistema. Come chiedere di avere il tratto centrale della H più in alto. A loro giudizio è meglio e dovrebbe bastare.

Invece che ci lo vuole più in basso, chi lo mette obliquo, chi più stretto e così via. Se l’interfaccia avesse un meccanismo capace di rendere ogni idea concepibile di H, sarebbe di una tale complessità da essere inutilizzabile nella pratica. Il lavoro del designer è cercare la H migliore possibile, non permettere un milione di H diverse.