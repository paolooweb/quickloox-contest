---
title: "La tecnologia ci fa glossi"
date: 2023-02-08T23:59:09+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [DeepL, Google Traduttore]
---
La cosa più normale del mondo.

In treno, una tizia fa gesti strani, senza parlare, in modo per me incomprensibile (più responsabilità mia che sua, conoscendomi). Alla fine armeggia su iPhone e mi mostra ansiosamente lo schermo. *La prossima fermata è Milano?*

Siccome ha usato [Google Traduttore](https://translate.google.com/?hl=it), vedo che lo scritto originale è in portoghese.

Allora lancio [DeepL](https://www.deepl.com/translator), che però è sovraccarico e non accetta input, anzi, lancio anch’io Google Traduttore e scrivo in italiano *Milano è l’ultima fermata, non ti preoccupare, quando scendono tutti siamo arrivati*.

Faccio tradurre in portoghese, mostro il risultato, lei sorride, finalmente si rilassa e si rimette a guardare le sue cose mentre io torno al lavoro.

Tutta questa tecnologia che scava i solchi, aumenta i divari, riduce i posti di lavoro, crea ansie a dipendenze e intanto due persone normali hanno fatto una cosa normalissima a pensarci, tanto che fino a pochi anni fa non c’era fantascienza distopica che non prevedesse un meccanismo di traduzione reciproca per capirsi tra parlanti di lingue diverse.

Oggi è realtà di una capillarità disarmante, in tasca a chiunque. Una traduzione volante la fai anche con il peggiore degli Android.

Il mondo non è perfetto. Ma è un posto migliore. Neanche noi siamo fantastici. Ma siamo un po’ meglio, se lasciamo che la tecnologia ci agevoli invece che distrarci e ostacolarci.