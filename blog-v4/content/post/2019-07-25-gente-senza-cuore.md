---
title: "Gente senza cuore"
date: 2019-07-25
comments: true
tags: [CNET, Hand, Orellana, watch, Ecg, Ekg]
---
Come la reporter Vanessa Hand Orellana di CNET, che ha visto su Apple Watch la notifica di irregolarità nel battito cardiaco ed è andata a farsi controllare in ospedale, dall’elettrocardiogramma di Apple Watch fatto su un dito ai dodici elettrodi della macchina tradizionale.

La cruda verità è che gli esiti dell’esame in ospedale [corrispondono a quanto ha notato Apple Watch](https://www.cnet.com/news/apple-watch-ecg-ekg-watchos-vs-hospital-medical-grade-detected-strange-heart-rhythm/).

Niente truffe *made in Cupertino* gabellate per strumenti utili alla salute, niente alternative Android che costano-meno-e-fanno-le-stesse-cose, niente disillusione antitecnologica per neoluddisto contro qualunque apparato purché digitale.

Quanti onesti venditori di fumo lasciati a secco da una articolista veramente cinica e senza cuore. Beh, diciamo con un cuore ballerino.


