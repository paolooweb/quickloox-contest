---
title: "Come non usa"
date: 2015-08-09
comments: true
tags: [Panic, StatusBoard, widget, Dashboard, Apple, iPad]
---
Avevo menzionato [Status Board](https://itunes.apple.com/it/app/status-board/id449955536?l=en&mt=8) parlando di [cose utili per la scuola](https://macintelligence.org/posts/2013-09-05-la-miglio-gioventu/) e successivamente di [app da comprare](https://macintelligence.org/posts/2013-10-19-a-chi-dare-soldi/), senza entrare nel merito.<!--more-->

Oggi c’è del merito perché [Panic Software](https://www.panic.com), sempre più brava, ha da poco presentato la versione 2. Chi come il sottoscritto ha sostenuto la prima versione riceve l’aggiornamento gratuito e quanti invece scoprono Status Board per la prima volta lo possono avere e usare gratis, però devono pagare con un acquisto in-app le funzioni che portano la versione 1 alla versione 2. Vincono tutti, nessuno resta fregato e chi ha comprato a suo tempo gode ancora di vantaggio competitivo rispetto a chi non paga. Nel contempo, chi vuole scarica e prova a volontà. Brillante.

Status Board è sommariamente un pannello informativo a componenti personalizzabili e programmabili, che adatta perfettamente a iPad l’idea bella (e oramai negletta da Apple) dei *widget* come funzionano – nell’indifferenza generale – su OS X in Dashboard.

I *widget* giusti, oggetti software leggeri con la stessa dignità sullo schermo delle applicazioni, stavano già in [Lisa](https://en.wikipedia.org/wiki/Apple_Lisa). Macintosh li confinò dentro il menu Apple per aggirare la primitività del *multitasking* di sistema. Credo che la personalizzabilità e un ambiente di fuzionamento apposito siano stati realizzati per la prima volta in [Konfabulator](http://daringfireball.net/2004/06/dashboard_vs_konfabulator), successivamente [acquistato da Yahoo!](https://en.wikipedia.org/wiki/Yahoo!_Widgets) per essere chiuso nel 2012. 

Il punto debole del sistema è rimasto quello risolto da Lisa, computer conosciuto da pochissimi fortunati: se i *widget* non sono al pari delle applicazioni diventano cittadini di seconda classe (come puntualmente è successo a Dashboard).

Se però i *widget* finiscono sullo schermo secondario e la scrivania del computer resta libera, ma iPad svolge la funzione di supporto, ecco che la produttività decolla.

Tutto ciò che produce dati in aggiornamento e che merita di ricevere un’occhiata di tanto in tanto per sapere come vanno le cose è passibile di diventare un pannello dentro Status Board: meteo, compleanni degli amici, conto alla rovescia per una scadenza, andamento della fatturazione, mail in attesa, flussi di notizie, sequenze fotografiche, messaggi ricorrenti e qualsiasi cosa sia programmabile, perché un pannello è costruito essenzialmente in Html.

I campi di utilizzo potenziale svariano all’infinito. Se è un divertimento programmarsi gli aggiornamenti della serie A o conoscere il titolo del brano in onda su [Beats 1](https://twitter.com/beats1), le informazioni di Borsa o le offerte su eBay possono portare vantaggi tangibili. Panic [racconta sul proprio blog aziendale](https://www.panic.com/blog/panic-status-board-2013-edition/) come la proiezione di Status Board su due maxischermi abbia cambiato in meglio il lavoro. La trasparenza sulle risposte del supporto tecnico, la vendita delle *app* e perfino la posizione dei bus pubblici utilizzati dai dipendenti per tornare a casa ha dato benefici misurabili e concreti.

In un modo o nell’altro, per avere Status Board al cento percento servono 9,99 euro. Per un’azienda capace di farlo fruttare vale cento volte, forse anche mille, quello che costa. E anche per realtà meno strutturate: nella versione 2 diventa possibile condividere pannelli (e l’aggiornamento dei pannelli stessi) con altre persone lontane. Se la versione 1 era da cogliere come un frutto maturo, la 2 è da accogliere in versione base come un gradito regalo. Il bello è che il programma ha un gran senso anche senza sapere che cosa farci: è probabilmente che l’uso migliore per chiunque sia qualcosa che ancora non è stato realizzato e sia un non uso fino a che sull’iPad appare l’icona a suscitare pensieri.