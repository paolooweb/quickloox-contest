---
title: "Una giornata particolare"
date: 2021-11-26T01:47:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [D&D, Dungeons & Dragons, Theodore Roosevelt, Ringraziamento, Thanksgiving] 
---
Oggi si celebra una festa americana che mi ha sempre coinvolto emotivamente più di altre. È una festa peculiare e oltretutto abbastanza artificiale, perché farebbe riferimento ai colonizzatori del Nuovo Mondo ma in realtà, per quanto ne so, è stata indetta dal presidente Roosevelt nel 1941.

Eppure mi trovo davanti a Mac mini, con abbondanza di lavoro da svolgere, una famiglia che dorme e un albero di Natale nuovo che le figlie hanno imposto di aprire e montare *subito* nonostante sia un po’ presto. Posso empatizzare con problemi da primo mondo come [sentirsi un master di Dungeons & Dragons migliore di quanto sia giocatore](https://www.enworld.org/threads/i-am-a-better-dm-than-a-player-thread-o-mancy.35287/) e pure dolermi di non riuscire a fare il master perché a distanza e a casa una famiglia con l’età media della nostra, figlia-uno alle primarie e figlia-due alla materna, te lo rende gioiosamente impossibile.

Le mie radici non stanno lì, neanche la festa in sé ha una genesi così profonda. Eppure lo sento sempre, il giorno del Ringraziamento.

E, per quanto sotto forma di semplici involtini, ho richiesto espressamente il tacchino.

_Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile._