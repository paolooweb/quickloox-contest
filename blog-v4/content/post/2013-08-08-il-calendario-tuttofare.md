---
title: "Il calendario tuttofare"
date: 2013-08-08
comments: true
tags: [iOS]
---
Ammiro **Stefano** per la sua fiducia incrollabile nel lato positivo dell’umanità.<!--more-->

Altra non potrebbe essere la ragione per cui legge *Melablog* ed è in grado di passarmi questa perla:

>Se tutto va come deve andare, e al netto di improbabili intoppi dell’ultima ora, Apple dovrebbe lanciare iOS 7 nei prossimi mesi, in particolare tra ottobre e novembre ma forse anche prima.

Il suo commento è perfetto:

>Come dire: lancio il dado e dovrebbe uscire un numero tra 1 e 6… o forse no.

Parafrasando il famoso detto, anche un calendario scassato indica il mese giusto dodici volte l’anno.