---
title: "Prepararsi per la scuola"
date: 2013-08-21
comments: true
tags: [Scuola]
---
Negli ipermercati si vedono i genitori fare la spesa per il ritorno a scuola dei figli.

Probabile che anche la scuola, come istituzione, si prepari. In Finlandia sanno già che gli esami di fine anno della scuola superiore dal 2016 verranno sostenuti dando le risposte in modo digitale attraverso l’interazione con [Digabi Live](https://github.com/digabi/digabi-live), sistema operativo liberamente e gratuitamente accessibile da chiunque.<!--more-->

Così hanno organizzato un [concorso internazionale](http://digabi.fi/doku.php?id=hackabi:kilpailu_en) per vedere chi riesce a violare la sicurezza del sistema, per essere sicuri di averlo blindato e affidabile quando servirà.

C’è tempo fino al primo settembre. Tre vincitori si aggiudicheranno [premi](http://digabi.fi/doku.php?id=hackabi:saannot_en) come un iPhone 5 da 64 gigabyte o un iPad da 128 gigabyte, o il loro equivalente in denaro. L’ufficio competente si riserverà la possibilità di assegnare eventuali altri premi se meritati.

Requisiti per partecipare: un indirizzo email e più di quindici anni.

Non commento oltre se non per dire che non mi sembrano iniziative tali da mettere in ginocchio il bilancio educativo di uno Stato.