---
title: "Doppio merito"
date: 2014-05-15
comments: true
tags: [app, iOS, Hig]
---
Merito ad Apple di pubblicare le [Human Interface Guidelines](https://developer.apple.com/library/ios/documentation/UserExperience/Conceptual/MobileHIG/) per iOS sul proprio sito per sviluppatori. Capire come strutturare nel giusto modo l’uso di una *app* è tanto più importante quanto più lo schermo è piccolo e quindi è molto più critico che su Mac.<!--more-->

Su App Store per iOS è inoltre facile imbattersi in cattivi esempi; meglio fornire materiale chiaro e autorevole specialmente a chi si improvvisa creatore di *app*.

Doppio merito per avere pubblicato la stessa documentazione su iBookstore, pronta per iBooks. Oltretutto una pubblicazione curata apposta per il formato, diversa dal pedissequo riversamento del web nel formato da libro elettronico.

Lettura (ma ci sono anche i video) essenziale per esprimere giudizi sensati su come è progettato iOS e come lo sono le *app*.
