---
title: "Il tempo delle magie"
date: 2018-01-15
comments: true
tags: [Jobs, Air]
---
Dieci anni esatti da quando Steve Jobs [estrasse il primo MacBook Air](https://www.youtube.com/watch?v=kvfrVrh76Mk) da una busta uso ufficio rimasta per tutto il *keynote* appoggiata su un tavolo senza che alcuno sospettasse. *The world’s thinnest notebook*, il portatile più sottile del mondo.<!--more-->

Aveva spiegato le cose per bene: niente compromessi sullo schermo, sulla tastiera, sul peso, sullo spessore, sulle prestazioni. Diversamente dalla concorrenza del tempo.

Il giorno dopo, quelli che capiscono tutto stroncavano MacBook Air per tutto quello su cui invece i compromessi c’erano: il processore non era abbastanza, le porte non erano abbastanza, il disco non era sufficiente, l’espandibilità inadeguata, la batteria pure e non parliamo del prezzo. Un giocattolo di lusso per pochi vanitosi danarosi.

Dieci anni più tardi, a MacBook Air ampiamente affermatosi come il Mac di maggiore successo di sempre, quelli che capiscono tutto rimpiangono i tempi in cui Jobs incantava il pubblico con le sue magie. Oggi Apple, ti raccontano, è una macchina da profitti priva di spinta innovativa reale, che propone a prezzi eccessivi portatili inadeguati. Giocattoli di lusso per pochi vanitosi danarosi.

A quei tempi, invece, era tutto diverso.

L’unica differenza è che le magie le faceva Jobs. Oggi le fanno loro, a parole invece che a prodotti.

<iframe width="560" height="315" src="https://www.youtube.com/embed/kvfrVrh76Mk" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>