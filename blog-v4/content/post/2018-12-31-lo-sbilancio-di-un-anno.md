---
title: "Lo sbilancio di un anno"
date: 2018-12-31
comments: true
tags: [Nive, Mac]
---
Si chiude un anno che, da questa parte della scrivania, ha portato problemi hardware, problemi con clienti, problemi logistici, problemi strategici, problemi pratici. Diciamo che se ne sono visti di migliori.

Eppure il bilancio faticoso e semi-insonne del 2018 è in gigantesco attivo per via dell’[arrivo della secondogenita](https://macintelligence.org/posts/2018-01-01-oggi-riparto/), che domani spegnerà la prima candelina. Secondogenita che in parte è responsabile dell’andamento non brillantissimo dell’annata; di fatto non ha ancora regolarizzato il ritmo del sonno e questo, nell’ecosistema familiare, complica di molto la quotidianità lavorativa e no.

Chi mi conosce sa che riesco a essere noioso e pedante, polemico e astioso, testardo e tignoso, però mai moralista. Al tempo stesso, sono ben consapevole di quale sia lo *zeitgeist*, la visione collettiva di certi temi, e di quanto sia di tendenza buttarla sul sé per dimenticare qualsiasi altra cosa.

Per questo mi sento di sbilanciarmi e scriverlo. Perché lo faccio a moralismo zero, perché va contro il pensiero diffuso e la prassi tipica, perché so che non lo scriverò mai più – qui, almeno – e questo neutralizza i miei difetti noti.

Non tornerei mai indietro e fare una figlia (o un figlio) è la cosa migliore che possa capitare. Fare la *seconda* figlia, poi, non è la replica di uno spettacolo già visto, ma una esperienza ulteriore.

Se capita la possibilità, conviene coglierla, anche contro tutto e contro tutti.

Il mio Capodanno sarà unico e mai vissuto prima, nonostante ne abbia visti alcune decine. È l’augurio che rivolgo a ciascuno e poi conta poco se a catalizzare la novità sarà una secondogenita o qualsiasi altro evento. Un 2019 unico e irripetibile per ciascuno di noi.

Sempre grazie a chi ha voglia di passare.