---
title: "L'arte dell'errore"
date: 2020-08-16
comments: true
tags: [Visa]
---
Mi accingo ad addebitare centocinquanta euro per portare la famiglia in visita a un parco a tema, come promessa fatta alla primogenita in occasione del suo compleanno. Ma digito a (scarsa) memoria e sbaglio una cifra della carta.

Invece di negare correttamente la transazione, il sito esplode adducendo un non meglio specificato errore. Ci riprovo, sbaglio ancora senza accorgermene, il sito riesce a crollare con un errore diverso.

Avviso il supporto clienti prima di rendermi conto e mi rispondono puntuali e cortesi su WhatsApp. Grazie mille, funziona tutto.

Cinque minuti dopo riscrivo perché sì, funziona tutto, solo che nella email di conferma mancano gli allegati dei biglietti. Mi rispondono che li recuperano.

Un’ora dopo gli chiedo a che punto sono e mi dicono di averli inviati. Avrò controllato inbox e spam tipo dieci volte da tre macchine diverse; posso garantire che non li ho ricevuti.

Allora mi scrivono che li rimandano. Quattro secondi e ho i biglietti in casella. Quel _ri_ di _rimandano_ appare sospetto.

Lieto fine, ma se fosse stata una giornata di lavoro o fossi stato distratto da qualche imprevisto? L’ecommerce di un parco a tema può permettersi questa disattenzione in un momento in cui c’è gente che ordina espressamente online proprio per evitare la coda fisica alle casse?

Nella polemica su come fare fronte al virus nel prossimo _new normal_ di settembre non vedo particolare attenzione alla parte tecnologica e a come possa introdurre livelli di protezione supplementari, se fatta bene.

Penso che l’attenzione dovrebbe esserci, e di più. L’ho già raccontata di quell’agriturismo dove c’è il misuratore automatico di temperatura e la lettura della mia fronte dava trentaquattro gradi? Sono errori deliziosi, solo che è problematico permetterseli, adesso. E andrà peggio.