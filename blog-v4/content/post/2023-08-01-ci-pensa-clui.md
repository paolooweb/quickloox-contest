---
title: "Ci pensa clui"
date: 2023-08-01T04:02:41+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [clui, Unicode]
---
Da quanto tempo non provavo l’urgenza di scaricare una Utility per Mac? Con [clui](https://www.brunerd.com/blog/2023/07/20/exploring-unicode-in-macos-with-clui/) posso assicurare che questa sensazione è pienamente ritornata.

clui somministra i classici steroidi al Visore Caratteri di Mac e permette non solo di scegliere caratteri Unicode ma di cercarli, raggrupparli, ottenere informazioni su essi come il Visore caratteri proprio non fa.

Dove sta quell’emoji che ricordo? Come si chiamano i caratteri nella nomenclatura di Unicode? Come faccio a creare sottogruppi di caratteri che mi servono al momento? clui fa tutto questo e anche molto altro.

Mi capita spesso di usare Unicode, anche sul lavoro. Visore caratteri e i selettori tipici di app e siti, semplicemente, non ce la fanno. Le funzioni di ricerca sono spartane e sovente le uniche a disposizione; Unicode si usa ma a costo di perdere tanto tempo in viste che non sono organizzate e con cui l’interazione è poco più di un segnaposto.

clui mi sta piacendo e ho l’impressione che possa darmi una discreta mano a portare il lavoro con caratteri Unicode non dico a tempi identici a quelli dell’alfabeto classico, ma accettabili sì.

Gratis, da Monterey in su, ho la sensazione che a fine estate sarà uno dei programmi intoccabili del mio setup.