---
title: "Navigazione pulita"
date: 2016-02-09
comments: true
tags: [Connect, Flash, Adobe, CC, Animate, Html5, Google]
---
Non ho ben capito se e quali differenze vi siano tra il vecchio Flash Professional e il nuovo Animate CC di Adobe. Le nuove [funzioni aggiunte](https://helpx.adobe.com/animate/release-note/releasenotes-2015-1.html) mi lasciano nel dubbio. Spero che non sia solo un cambio di nome.<!--more-->

So che ci sono ancora nostalgici di Flash in giro, ma – non si fossero rassegnati – da fine anno Google [rifiuterà inserzioni pubblicitarie](https://plus.google.com/+GoogleAds/posts/dYSJRrrgNjk) in formato diverso da Html5.

E Adobe stessa [leva Flash dai propri prodotti](http://blogs.adobe.com/adobeconnect/2016/02/add-in.html), come nel caso di Connect.

Sono anni che ho levato il mefitico *plugin* guadagnando sicurezza, batteria, stabilità e pure una pillola di spazio su disco. Chi non lo ha ancora fatto si affretti: anche i dati di traffico contribuiscono a ripulire il web dalla piaga e prima si conclude, meglio è. Facciamo vedere che vogliamo navigazione (più) pulita.