---
title: "Tutti i tablet sono uguali"
date: 2015-11-17
comments: true
tags: [Dilger, AppleInsider, A9X, Apple, iPad, Pro, Surface, iPad, Air, Geekbench, Intel, i5, multicore, core]
---
O così qualcuno liquida frettolosamente la questione. Daniel Eran Dilger ha scritto un [articolo piuttosto dettagliato](http://appleinsider.com/articles/15/11/12/apples-new-ipad-pro-is-faster-more-affordable-than-microsofts-surface-pro-4) su Apple Insider per chiarire la questione riguardo a iPad Pro e Surface Pro 4, di cui riporto alcuni brani.<!--more-->

>[Surface Pro da 899 dollari contiene un processore di fascia bassa  Core m3 Intel, che il processore A9X di Apple [su un iPad Pro da 899 dollari] batte sonoramente tanto sui test single core che su quelli multicore, nonostante iPad Pro abbia un milione e mezzo di pixel in più da amministrare. 

>Perfino iPad Air 2 da 499 dollari batte un Surface Pro 4 da 899 dollari nei test multicore, mentre ottiene risultati leggermente inferiori nelle prestazioni a core singolo.

>Per avere un Surface Pro 4 con processore più potente di iPad Pro, ci vuole la versione da 1.299 dollari con Core i5 Intel, che costa come un MacBook Pro 13” e 500 dollari più che un iPad Pro modello base.

Nell’articolo è presente anche una tabella comparativa del test di velocità [Geekbench](http://www.primatelabs.com/geekbench/). I test non sono tutto e spesso sono ingannatori. Tuttavia sono più veritieri del chiacchiericcio.