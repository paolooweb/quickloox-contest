---
title: "Con la maiuscola"
date: 2021-06-08T12:13:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [Terminale, uptime, Wwdc, Zoom, Teams, iOS, iPadOS, Comandi rapidi, Shortcuts, watchOS, Swift Playgrounds, Xcode, iCloud, Safari, Mail, privacy, Xi, Cina, Zoom, Teams] 
---
Mi piace molto questa evoluzione globale dell’ecosistema, dove i diversi apparecchi non sono isole di un arcipelago, ma tappe di un viaggio, e non c’è uno strumento unico per tutti ma versioni ideali di uno strumento per ciascuno.

È il [keynote Wwdc](https://www.apple.com/apple-events/june-2021/) più logisticamente complicato che ho vissuto, perché tra famiglia, lavoro, intoppi, imprevisti e sonno ho speso una volta e mezzo la sua durata e ancora devo vederlo tutto. Tuttavia la portante mi pare chiara.

È un ecosistema maturo, che si apre dove deve verso l’esterno, si arricchisce, si allarga, si raffina. Poco o nulla di quello che si è visto è mai-visto-prima e questo è un buon segno: difficilmente Apple inventa, bensì arriva a cambiare per il meglio qualcosa di esistente e normalmente migliorabile.

È un anno e mezzo che si invita la gente a parlare online; farlo con un FaceTime link sarà più immediato e veloce. A me è capitato di farlo con Zoom o con Teams ed è una pena; con iOS sarà molto meglio.

Durante le videoconferenze, l’audio è sempre uno dei punti dolenti, anche fisicamente quando la riunione dura molto o, come pretendono certi dirigenti ottusi, il giorno deve passare in riunione. Portare lo Spatial Audio dentro la videoconferenza migliora la vita di chi ci si trova.

E Spatial Audio è il pretesto per sottolineare una vera differenza che fa Apple. Traduzione del testo affidata al computer, l’abbiamo vista; riconoscimento del testo dentro una immagine, lo abbiamo visto; il drag and drop da uno schermo all’altro non è una novità; forse lo è da un computer a un altro, ma visivamente sembra qualcosa di già sperimentato.

Ma chi può offrire quello che si è visto ieri *a livello di sistema*? A disposizione di qualsiasi app? E chi può permetterlo con questo livello di semplicità?

La differenza che mette Apple è da sempre, per la parte fondamentale, questa. Quando Apple reinventa qualcosa di esistente e la trasforma in magia, *it just works*, dà il meglio. Per questo Wwdc comincia sotto ottimi auspici.

Un ecosistema, dove qui il cambiamento può essere più pronunciato (ma quanto sono belle le nuove mappe?), lì si insaporisce la ricetta che già di suo funziona (tutta la parte di SharePlay su iPhone, non cambia il mondo, ma introduce un sacco di cose piacevoli), altrove si inseriscono cambiamenti persino necessari (iPadOS deve evolvere ancora più di così e però il multitasking migliora), oppure si gettano ponti che ci volevano (Comandi rapidi anche su Mac, capacità di collegarsi con AppleScript; [ne scrivevano in tanti](https://macintelligence.org/posts/Perculare-e-percolare.html), non si vedeva l’ora, è arrivato).

In quest’ottica, mettere sul bilancino watchOS per capire se le aggiunte meritano questo o quel voto in pagella è da Youtuber bolso, che deve parlare del keynote per quaranta minuti altrimenti non monetizza e deve inventarsi cose per arrivare in fondo ai quaranta minuti. Conta l’insieme, la coralità. La coralità viene evidenziata anche a livello di relatori e inizia persino a sembrare troppa; l’inclusione ci mancherebbe, la diversità è un bene, però quasi quasi preferirei che i relatori sotto i Vice President venissero estratti a sorte.

Un pensiero affettuoso e adorante a quanti *iPad non è un computer perché non posso programmarci una applicazione per iPad*, che poi sono passati alla compilazione in luogo della programmazione e ora hanno solo da ammettere che, persino per il loro filtro, iPad è un computer. O si inventeranno che non è un computer perché non passa da Xcode. Chissà come digeriscono oggi poi, al pensiero di Xcode su iCloud.

Un accenno alla privacy. L’ecosistema. L’argomento è trasversale, non riguarda l’apparecchio A o il sistema operativo B. Qualche settimana e salterà fuori qualche scandalo dovuto a funzioni che non saranno implementate o non funzioneranno in Cina o in Bielorussia. Eppure la direzione dell’azienda è oltremodo chiara: su Mail potremo nascondere l’indirizzo IP, non fare sapere che abbiamo letto un messaggio. Se lo vogliamo, naturalmente. A che pro darsi da fare per implementare la privacy quando sarebbe tanto comodo lasciar perdere tutto e avere sistemi perfetti per compiacere Xi e la compagnia dei dittatori? Ringraziare invece. Ogni tracker soffocato da Safari, ogni tracking pixel neutralizzato da Mail, ogni navigazione anonima è un passo verso più libertà, da cui domani sarà più difficile regredire nel caso che il nostro governo, o l’ineffabile unione, ci ripensi o provi a fare il furbo.

Mentre armeggiavo con una finestra di Terminale durante la visione di Wwdc, mi è scappato scritto `Uptime` al posto di `uptime`. Ho scoperto una nuova funzione di un comando Unix che pensavo di padroneggiare. Abbiamo tanto da imparare intanto che i sistemi si affinano.

Ne parlo perché la scoperta dell’esistenza di `Uptime` e questo Wwdc sono state ambedue esperienze con la maiuscola.

Perché non posso, sarò in giro a lavorare con iPad Pro senza attrezzature di backup… altrimenti, per la prima volta da anni, vorrei fortemente installarmi tutte le beta.

*Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile.*               