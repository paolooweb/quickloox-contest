---
title: "Se questa è una rapina"
date: 2020-02-21
comments: true
tags: [Tesler, Post, Emacs, Teco, vi, Apple, Xerox, Parc, Digital, kill, yank, Jobs, Picasso]
---
Un non fa in tempo a [commemorare Larry Tesler](https://macintelligence.org/posts/2020-02-20-intelligenza-e-interfaccia/) che *Il Post*, [nel fare la stessa cosa](https://www.ilpost.it/2020/02/20/larry-tesler-inventore-copia-incolla-morto/), si ricorda astutamente di buttare lì qualche malignità, sapendo benissimo come si acchiappano i clic.

>Le due visite di Jobs – durante le quali gli fu anche mostrato [da Tesler] uno dei primi mouse per computer – sarebbero state definite da alcuni “la più grande rapina nella storia dell’informatica”.

Quanta finezza: *definite da alcuni* come presa di distanza, *sarebbero* al condizionale, metti mai che qualcuno dubiti; un paragrafo più avanti, il colpo alla botte dopo quello al cerchio:

>Jobs copiò sicuramente alcune idee nate al PARC, ma ebbe le giuste intuizioni per migliorarle e per renderle un successo commerciale.

Intanto *rapina* è passato, poi la più grande, eh, mica una marachella.

L’autore dell’articolo, anonimo e capisco il perché visto il suo grado di preparazione, ha centrato il pezzo (come del resto molti altri) sulla figura di Tesler come *inventore del copia e incolla*, da lui *ideato assieme al collega Timothy Mott nel 1973*.

Ecco, due editor di testo storici come [emacs](https://www.gnu.org/software/emacs/) e [vi](https://www.vim.org) nascono poco dopo, nel 1976, completi di un sistema analogo per replicare e spostare parti di testo all’interno di un documento, dove però [si usano termini diversi, come *kill* e *yank*](http://www.copters.com/teco.html).

Entrambi sono posteriori al lavoro di Tesler, ma [attingono a lavoro precedente](https://www.quora.com/Why-does-Emacs-have-kill-and-yank-rather-than-cut-copy-and-paste): per la precisione all’editor <a href="https://en.wikipedia.org/wiki/TECO_(text_editor)">Teco</a> usato sui minicomputer della defunta [Digital Equipment Corporation](https://digital.com/about/dec/).

Teco è nato nel 1962, dieci anni prima che Tesler perfezionasse il copia e incolla. Questo diminuisce la sua grandezza? Ne fa un copiatore, per quanto geniale? Possiamo parlare del suo lavoro come di una rapina nei confronti degli autori di Teco?

No, per tre volte. Larry Tesler è una delle figure che definiscono quello che siamo oggi, informaticamente parlando, ed entra a pieno diritto nella nostra storia. Che esistesse lavoro precedente dimostra solo che lo conosceva e seppe perfezionarlo per portarlo a un livello nuovo, astronomicamente più elevato rispetto a quello di provenienza.

Sì, vabbè, [i buoni artisti copiano, i grandi artisti rubano](https://quoteinvestigator.com/2013/03/06/artists-steal/), Jobs dice di Picasso, alibi perfetto per uno che voglia accusare Jobs di avere rubato, con la scusa dell’arte.

Quanto sopra sembra una lunga dissertazione pedantina in punta di penna, vero? La faccio corta: [Apple vendette azioni proprie a Xerox a prezzo di favore e in cambio ottenne l’accesso al Palo Alto Research Center](https://macintelligence.org/posts/2014-08-25-azioni-che-contano/). Si noti anche come il lavoro su Lisa e Macintosh [fosse già iniziato](https://macintelligence.org/posts/2017-06-16-piu-in-alto/).

Se questa è una rapina…
