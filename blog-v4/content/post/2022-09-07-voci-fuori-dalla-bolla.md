---
title: "Voci fuori dalla bolla"
date: 2022-09-07T01:54:46+01:00
draft: false
toc: false
comments: true
categories: [Internet]
tags: [MacBook Pro, Mac Studio, Apple Silicon, Cichy, Nordlund, Aversa, Zac Cichy, Scott Nordlund, David Aversa]
---
Voci fuori dal coro non ne esistono. O meglio, stanno in un coro diverso ma non se accorgono.

Le voci fuori dalla bolla invece sono individuabili con chiarezza; sono persone che non seguo e che non conosco neanche via rete, ma arrivo comunque a leggere nonostante non abbia alcuna intenzione di farlo, né abbia esplicitamente cercato questi contenuti. La bolla in questo caso è la mia, in tutti gli altri è quella di ciascuno.

Finalmente passata questa faticosa introduzione, vengo alla [prima voce fuori dalla bolla](https://twitter.com/NordoftheNorth/status/1567132479319904256):

<blockquote class="twitter-tweet"><p lang="en" dir="ltr">After listening to Connected for the better part of six years, I finally bought a MacBook pro.<br><br>After 15 years working on a windows laptop I feel like a lost puppy but &quot;I get it&quot;. Everything is just a little faster, smoother and the UE is fantastic.<a href="https://twitter.com/imyke?ref_src=twsrc%5Etfw">@imyke</a> <a href="https://twitter.com/ismh?ref_src=twsrc%5Etfw">@ismh</a> <a href="https://twitter.com/viticci?ref_src=twsrc%5Etfw">@viticci</a></p>&mdash; Scott Nordlund (@NordoftheNorth) <a href="https://twitter.com/NordoftheNorth/status/1567132479319904256?ref_src=twsrc%5Etfw">September 6, 2022</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

Per lamentarsi dei MacBook Pro si può sempre trovare qualcosa, chiaro; eppure pare che per chi arriva dalla faccia oscura, l’esperienza sia ancora speciale e positiva.

La [seconda voce fuori dalla bolla](https://twitter.com/zcichy/status/1566674661584564224):

<blockquote class="twitter-tweet"><p lang="en" dir="ltr">Using the 14” M1 Pro MacBook Pro feels different from using any computer I’ve ever used.<br><br>Between the 120hz refresh rate, the fact that everything I can throw at it just…works.<br><br>It’s like using a computer in God Mode.</p>&mdash; Zac Cichy (@zcichy) <a href="https://twitter.com/zcichy/status/1566674661584564224?ref_src=twsrc%5Etfw">September 5, 2022</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

*Come usare un computer in modalità divina*. E lo schermo fa la differenza, evidentemente.

Una delle risposte sposta l’obiettivo su Mac Studio, per definirlo *seriamente magico*:

<blockquote class="twitter-tweet"><p lang="en" dir="ltr">I&#39;m just rocking an M1 Max Mac Studio with 32gb of ram and I&#39;ve never felt like I have this much headroom on a computer before. It seriously feels like magic.</p>&mdash; David (@Daversa2) <a href="https://twitter.com/Daversa2/status/1566690213103271936?ref_src=twsrc%5Etfw">September 5, 2022</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

Sembra che questo Apple Silicon ne abbia azzeccata qualcuna.