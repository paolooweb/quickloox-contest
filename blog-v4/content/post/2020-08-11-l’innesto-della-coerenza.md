---
title: "L'innesto della coerenza"
date: 2020-08-11
comments: true
tags: [Macintosh, Mac, iPhone, iPad, iOS, a-Shell]
---
Lo specchietto per allodole era l’interfaccia grafica, mentre la magia esoterica con cui il primo Macintosh irretiva le folle era la coerenza.

I comandi e le procedure di base erano le stesse, su qualunque programma. Si potevano lanciare decine di applicazioni diverse, però tutte erano esempi di uso di Macintosh: il primo computer che, diversamente da un cassone pieno di programmi difformi, aveva un’identità.

La coerenza si è diluita nel tempo, inevitabile scotto della crescita del mercato. Prima un’applicazione si metteva al servizio dell’utilizzatore; oggi è una testa di ponte nella guerra per l’attenzione e i portafogli.

È anche cambiata la tecnologia a disposizione: lanciare sette macchine virtuali con altrettanti sistemi operativi è solo questione di risorse. Il valore aggiunto della coerenza, in questo scenario, si riduce.

Questa è una delle spiegazioni del caso di quelle persone che adorano Mac al punto di volerlo usare attraverso il Terminale. Il Terminale ha caratteristiche proprie ma, soprattutto, è molto coerente. Fino a che l’uso resta semplice, i comandi sono sempre quelli e i nomi sono talmente concisi e peculiari che sostituiscono la _discoverability_, altra grande dote della prima interfaccia di Mac.

Che la coerenza abbia poco valore, oggi, lo dimostrano iOS e iPadOS: ambienti dove la logica touch e le dimensioni ridotte dello schermo la emarginano senza pietà. La discoverability attraverso l’evidenza dei menu lascia il passo a quella per tentativi e interazione curiosa. Se prima si imparava a usare una app con metodo, oggi lo si fa con l’esplorazione. Ogni tanto ci viene detta una cosa in più.

Per via di queste premesse l’esperimento di [a-Shell](https://holzschu.github.io/a-Shell_iOS/) (assolutamente da leggere in inglese) è affascinante: un Terminale, ad alta coerenza, per apparecchi dediti al touch e che non la concepiscono, by design.

Open source, gratis, voluminosa, a-Shell è la cosa più vicina a un vero Terminale che si poteva realizzare da fuori Apple. Si possono scaricare comandi Unix aggiuntivi, manipolare file, editare testi, passare elaborazioni ad altre app, programmare in svariati linguaggi, _compilare_ in C (quelli che _non è un computer_…), persino usare LaTeX.

Sono volutamente svelto sulla parte tecnica, perché la vedo totalmente secondaria. La vera sensazione è la proposta di un ambiente testuale su una piattaforma touch e soprattutto una piattaforma proteiforme. Un Macintosh dei tempi gloriosi faceva qualunque cosa e restava un Macintosh. Un iPad non è mai un iPad; diventa ciò che sta girando in primo piano.

L’innesco della coerenza su sistemi anticoerenti produce una chimera con superpoteri o una esplosione devastante di inutilità? La risposta a quando avrò totalizzato un tempo di uso ragionevole.