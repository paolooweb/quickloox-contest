---
title: "Quante finestre vuoi?"
date: 2020-01-20
comments: true
tags: [Drang, multitasking, Gruber, Front, Center]
---
Come se non fosse già stressante dover accettare che ci siano utenti di [Vim](https://www.vim.org/) invece che di [emacs](https://www.gnu.org/software/emacs/), o gente che [preferisce lo scorrimento artificiale a quello naturale](https://macintelligence.org/posts/2017-04-04-corsi-ricorsi-e-convenzioni/), ecco che prende forma un’altra diatriba nella comunità Mac: quando clicco su una finestra, deve salire in primo piano solo lei o tutte le finestre di quella app?

Il primo comportamento è proprio di macOS dai tempi in cui si chiamava Mac OS X e, nel mondo Mac, fu una discontinuità importante, perché per tutto il XX secolo le cose erano andate nel secondo modo. Ora è uscita una [utility](https://apps.apple.com/it/app/front-and-center/id1493996622?l=en&mt=12) firmata nientemeno che da John Siracusa, la quale riproduce lo stesso comportamento su macOS e diverse penne autorevoli prendono posizione su un fronte o quell’altro.

John Gruber, per esempio, [è a favore del metodo Classic](https://daringfireball.net/2020/01/front_and_center), un clic e sale a galla tutta la app come si faceva una volta.

Anche senza essere autorevole mi schiero da fantaccino sul lato opposto, [in compagnia di Dr. Drang](https://leancrew.com/all-this/2020/01/multitasking-windows-and-the-mac/), le cui argomentazioni mi trovano solidale: il vecchio design deriva dalla mancanza di multitasking sul Mac originale, cui fece seguito il multitasking cooperativo che funzionava solo se l’applicazione in primo piano era disposta a cedere tempo macchina alle altre. Due situazioni dove aveva senso portare in primo piano l’intera applicazione.

Ora che c’è un vero multitasking non ha più senso, dico io spalleggiato a sua insaputa da Dr. Drang. Oltretutto è semplice evocare una intera app su macOS già ora, con un clic sull’icona del Dock per esempio, o con la commutazione rapida di Comando-Tab. Quando clicco su una finestra voglio quel documento, non quell’applicazione.

Attendo un contraddittorio e, se proprio devo avere torto, almeno butta via Vim.
