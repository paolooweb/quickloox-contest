---
title: "Olimpiadi e bancarelle"
date: 2021-07-25T00:07:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [Huffington Post, Super Mario, Yoko Shimomura, Xevious, Olimpiadi, Tokyo] 
---
I veri intenditori hanno notato, come riporta *Huffington Post*, [la citazione di varie colonne sonore di videogiochi durante la sfilata di apertura dei Giochi di Tokyo](https://www.huffpost.com/entry/japanese-video-game-music-olympics-opening-ceremony_n_60facd72e4b0d2a22d4aaa61). Una proprio [in corrispondenza dell’ingresso dell’Italia nello stadio](https://twitter.com/giopota/status/1418540660253134850).

<blockquote class="twitter-tweet"><p lang="en" dir="ltr">The ost of Kingdom Hearts (Olympus Coliseum) by queen Yoko Shimomura while Italy entered the stadium ✨ <a href="https://twitter.com/hashtag/Tokyo2020?src=hash&amp;ref_src=twsrc%5Etfw">#Tokyo2020</a></p>&mdash; Giopota (@giopota) <a href="https://twitter.com/giopota/status/1418540660253134850?ref_src=twsrc%5Etfw">July 23, 2021</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

Contemporaneamente, giravo le bancarelle sul lungomare del paesino. Purtroppo non sono riuscito – per ora – a documentarlo; mentre stavo per scattare una foto ho ricevuto un’occhiataccia e ho evitato problemi. Ho visto una montagna di confezioni evidentemente *made in China*: dentro, uno scatolotto grande come due Mac mini impilati, collegato a un piccolo controller con i pulsanti tipici di un apparecchio per console di videogiochi.

Dentro lo scatolotto, secondo le istruzioni, 620 giochi retró pronti all’uso. Sulla bancarella, un demo del primissimo Super Mario, mostrato da un comune televisore.

Non conosco ancora il prezzo e non sono riuscito a capire se dentro lo scatolotto giri una copia di [Mame](https://www.mamedev.org) o che altro.

Violazioni di copyright a parte, mi ha colpito la doppia rilevanza di quei vecchi giochi nello stesso giorno, come riferimento culturale e identitario per una nazione (Yoko Shimomura, che ha composto varie colonne sonore di videogiochi, è una leggenda vivente per i fan locali) e, al tempo stesso, come qualcosa che ha comunque la forza di ritornare persino nell’epoca dei grandi multiplayer in cloud, non importa se su una bancarella del mercato dentro un apparato dalla liceità commerciale assai sospetta.

Spero che non ci sia dentro [Xevious](https://en.wikipedia.org/wiki/Xevious). Non l’ho mai finito, non saprei resistergli e [per finirlo ci vuole troppo](https://xevious.fandom.com/wiki/Xevious_Arrangement).

*Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile.*