---
title: "Divino e terreno"
date: 2021-04-13T23:00:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [Cartoonito, RaiPlay Yoyo, BBEdit, tv] 
---
Mi è sfuggito il [ventinovesimo compleanno di BBEdit](https://twitter.com/bbedit/status/1381681523011768321).

<blockquote class="twitter-tweet"><p lang="en" dir="ltr">On this day, 29 years ago: <a href="https://t.co/sJQNHariDN">https://t.co/sJQNHariDN</a></p>&mdash; BBEdit (@bbedit) <a href="https://twitter.com/bbedit/status/1381681523011768321?ref_src=twsrc%5Etfw">April 12, 2021</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

Perché, quando avrei dovuto pensarci, litigavo con le app dei canali per bambini [RayPlay Yoyo](https://apps.apple.com/it/app/raiplay-yoyo/id1364569091) e [Cartoonito](https://apps.apple.com/it/app/cartoonito-app/id1468124028). I modi di fallire che adottano nel momento in cui tu vorresti passare la diretta del canale da iPhone al televisore attraverso tv sono creativi come riescono a esserlo in Rai quando si dilettano con il digitale.

Yoyo proprio non lo permette. Se lo permette, non ci sono arrivato.

Cartoonito è fantastica. Manda senza problemi il video dalla home sul televisore. Solo che è *verticale*. Ruotato di novanta gradi.

Per fortuna c’è una seconda possibilità: la voce di menu *Canale TV*. Se accendo il mirroring di iPhone su tv, il video va a pieno schermo sul televisore. Un terzo del quale viene occupato da una parte della schermata del centro notifiche di iPhone, presente per errore. Non ho trovato un modo per levarla.

Ci sono giorni dove devi volare alto per forza, perché come metti i piedi per terra, vien voglia di scappare.

*Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile.*