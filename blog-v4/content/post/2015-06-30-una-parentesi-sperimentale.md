---
title: "Una parentesi sperimentale"
date: 2015-06-30T02:24:08+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [Ses, emacs, Numbers, Lisp, iCloud, iOS]
---
Ci ho investito niente più che una manciata di minuti e si vede tutto, ma questo è l’inizio del principio di un embrione di un foglio elettronico dentro emacs, cosa che preconizzavo [qualche pagina fa](https://macintelligence.org/posts/2015-06-23-la-questione-e-aperta/).<!--more-->

 ![foglio di calcolo in emacs](/images/emacs.png  "foglio di calcolo in emacs") 

Ho usato la modalità Simple Emacs Spreadsheet, che non richiede installazioni supplementari: la funzione è già dentro il programma.

La cosa interessante è che le formule si esprimono in Lisp: la banale somma dei valori inseriti si ottiene con `(+ A2 A3 A4 A5)`.

È veramente un inizio: le funzioni a disposizione in partenza sono rudimentali (appunto, in un certo senso il foglio va programmato, avendo a disposizione Lisp). Formattare il foglio ha tutte le complicazioni del solo testo e la sintassi è involuta a farle un complimento.

Mettercisi sarebbe faticoso e frustrante, però ci sarebbe un mondo da scoprire e tutti i requisiti, rispetto a un Numbers, sarebbero clamorosamente ridotti. File minuscoli, velocità di editing istantanea, potenza richiesta tendente a zero, memoria occupata meno ancora.

Ci sarebbe qualche problema di sincronizzazione con iOS che oggi, con iCloud e Numbers, non sussiste. D’altro canto potrei fare su emacs più o meno tutto quello che sto facendo su Numbers. Quasi quasi.

Per pura curiosità, qui sotto appare il contenuto del file generato da Simple Emacs Spreadsheet. Interessante.

`Numeri a caso               `
`           19               `
`            3               `
`           11               `
`           14               `
`           47        Totale `
` `
` `
`(ses-cell A1 Numeri a caso Numeri a caso nil nil) `
`(ses-cell B1 nil nil nil nil) `
` `
`(ses-cell A2 19 19 nil (A6)) `
`(ses-cell B2 nil nil nil nil) `
` `
`(ses-cell A3 3 3 nil (A6)) `
`(ses-cell B3 nil nil nil nil) `
` `
`(ses-cell A4 11 11 nil (A6)) `
`(ses-cell B4 nil nil nil nil) `
` `
`(ses-cell A5 14 14 nil (A6)) `
`(ses-cell B5 nil nil nil nil) `
` `
`(ses-cell A6 47 (+ A2 A3 A4 A5) nil nil) `
`(ses-cell B6 Totale Totale nil nil) `
` `
`(ses-column-widths [13 13]) `
`(ses-column-printers [nil nil]) `
`(ses-default-printer %.7g) `
`(ses-header-row 0) `
` `
`( ;Global parameters (these are read first) `
` 2 ;SES file-format `
` 6 ;numrows `
` 2 ;numcols `
`) `
` `
`;; Local Variables: `
`;; mode: ses `
`;; End: `