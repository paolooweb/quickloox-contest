---
title: "Conflitto di interessi"
date: 2014-12-01
comments: true
tags: [Apple, AppleLink]
---
Negli anni, l’accusa più ridicola di quelle che ho ricevuto è sempre rimasta quella di essere al soldo di Apple. Negli anni novanta ho effettivamente lavorato qualche anno per il reparto marketing e comunicazione; da libero professionista, traducendo in italiano comunicati stampa e documenti vari.<!--more-->

Sono stato pagato sì, per prestazioni professionali e null’altro. Avevo il *fringe benefit* di poter accedere ad AppleLink, la rete interna di comunicazioni di Apple. Mi era indispensabile per ricevere e trasmettere i materiali. Era anche costosissima; i nodi metropolitani avevano tariffazione a consumo e dovevo stare bene attento a scaricare solo lo stretto necessario.

Passai un’estate a lavorare dal mare quasi solo per il motivo che, fuori dai grandi centri, AppleLink era accessibile solo da un *gateway* accessibile via chiamata urbana. Dieci minuti di AppleLink a Milano costavano più di una notte ad Ancona. Ero pagato da Apple per le mie traduzioni ma non avevo il rimborso di AppleLink, per dire.

Non avevo ancora iniziato a bloggare, per la banale ragione che non c’era ancora Internet.

Non ho più lavorato per Apple. Tuttavia, che divertimento c’è se i mentecatti hanno zero appigli?

Così mi sono fatto coraggio e mesi fa ho acquistato *una* azione Apple.

Nell’estratto conto è apparso il mio primo dividendo di azionista: ventiquattro centesimi di euro.

Ora si potrà dire che ho il mio bell’interesse a scrivere di Apple.