---
title: "Un keynote e tre considerazioni"
date: 2016-09-12
comments: true
tags: [ iWork, Nintendo, AirPods, iPhone, ConnectED, ResearchKit, watch]
---
Prima considerazione: Apple è (sempre) più attenta ai contenuti. Promemoria per quelli che ancora pensano che la recensione debba riguardare il processore, la Ram, il millimetro in più o in meno, come se oggi facesse veramente la differenza. Non la fa; la fa la qualità dell’utilizzo, che dipende anche dai contenuti. Quelli che si possono creare e quelli disponibili. Chi compra tecnologia ha smesso di chiedere quanto è veloce; chiede che cosa può farci.<!--more-->

Seconda considerazione: Apple progetta i propri processori. E può permettersi di vantare un proprio, nuovo processore negli *auricolari*. Gli altri acquistano gli stessi processori di chiunque altro. Se non esiste un processore per i propri auricolari, fanno auricolari senza processore. Veramente qualcuno, a questo punto, pensa che non sia un fattore di differenza? E il punto, vedi sopra, non è la velocità. A nessuno interessa quanto va veloce il processore degli auricolari.

Terza considerazione: oltre a spiegare quanti apparecchi vende, quante app fa scaricare, quanto è bello il prossimo computer da tasca, perché è da comprare il prossimo computer da polso, l’azienda – il cui mestiere è vendere, vendere, vendere – trova spazio per parlare di [piattaforme per la salute](https://macintelligence.org/posts/2016-07-19-una-soluzione-in-cerca-di-problemi/) e [iniziative per l’istruzione](https://macintelligence.org/posts/2016-08-16-back-to-school/) nelle scuole più trascurate degli Stati Uniti.

Considerazione bonus: lo stolto guarda Super Mario su iPhone e pensa a un videogioco. Il saggio guarda Super Mario su iPhone e vede Nintendo rovesciare un pilastro ultradecennale della sua strategia. È un annuncio clamoroso.

Per non farci mancare niente: se la collaborazione simultanea di iWork funziona davvero come è stata mostrata sul palco, Pages a compagnia riguadagnano non poco interesse. Il punto di riferimento è Google, che la fa funzionare benissimo, ma su app un po’ ruspanti. Se iWork, che ha applicazioni assai migliori, pareggia sulla collaborazione con Google, merita veramente un pensiero. Ricordo che iWork via browser è a disposizione anche dei PC.
