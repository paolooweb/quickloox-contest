---
title: "Ventisette anni e va a giocare con la sabbia"
date: 2019-02-26
comments: true
tags: [BBEdit]
---
Sono passati quasi due anni dal [quarto di secolo](https://twitter.com/bbedit/status/852203066120187905) e BBEdit se la cava alla grandissima.

<blockquote class="twitter-tweet" data-lang="en"><p lang="en" dir="ltr">Today is my 25th birthday! 🎂</p>&mdash; BBEdit (@bbedit) <a href="https://twitter.com/bbedit/status/852203066120187905?ref_src=twsrc%5Etfw">April 12, 2017</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

Infatti è appena uscito un aggiornamento superimportante che rende [*sandboxed*](https://www.barebones.com/support/bbedit/notes-12.6.html) il programma.

Tecnicamente, significa che BBEdit rispetta i requisiti di sicurezza sempre più stringenti imposti da Apple al software per Mac. Finora questo ha significato problemi vari, per esempio l’impossibilità di vendere su Mac App Store una versione di BBEdit equivalente a quella presente sul sito Bare Bones.

Invece, ora chi lo desidera potrà lavorare esattamente come prima. Però intanto l’applicazione potrà tornare su Mac App Store senza dover scendere a compromessi sulla funzionalità.

Sempre sperando che un giorno, per magia, compaia una trasposizione di BBEdit per iOS. Che manterrà il rigore e la certosina precisione di quella per Mac. Guardare l’elenco dei *bug fix* in questa versione 12.6, per farsi un’idea.