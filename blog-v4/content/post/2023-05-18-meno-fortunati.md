---
title: "Meno fortunati"
date: 2023-05-18T01:21:38+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [Daring Fireball, John Gruber, Gruber, CarPlay, GM, General Motors, Succession, Kendall Roy]
---
Ricordiamoci che, mentre ci coccoliamo mollemente nel benessere occidentale, ci sono persone che in General Motors hanno scelto di [abbandonare CarPlay di Apple](https://sixcolors.com/post/2023/04/general-motors-hates-your-iphone/) per offrire una loro interfaccia e, oltre a questo, decuplicare da qui a fine decennio il fatturato derivante dai servizi in abbonamento.

John Gruber [gli ha fatto i conti in tasca](https://daringfireball.net/2023/05/gm_edsel) su *Daring Fireball* e ha calcolato che da qui al 2030, per arrivare al risultato, qualsiasi acquirente di una macchina General Motors si abbonerà a servizi digitali per una somma attorno ai cinquantacinque dollari al mese. E devono essere molti, anche qualcosa più della cifra annuale attuale di sei milioni di auto, oltretutto in declino.

Apple sostiene che il novantotto percento di tutte le nuove auto in vendita monta CarPlay e che [il settantanove percento degli acquirenti è interessato ad avere la funzione](https://www.theverge.com/2022/6/6/23156741/ios-16-carplay-apple-wwdc-hvac-deeper-integration) nel momento in cui considera di comprare una macchina. Come sempre sono numeri di parte e suscettibili di essere stati gonfiati, ma certo non di essere stravolti; possiamo dare per certo che ci ia un interesse per CarPlay nelle auto, anche se su numeri meno alti.

In pratica General Motors vuole farsi un app store in proprio e immagina di incassare un quarto o un quinto di quanto fa App Store. Quello vero, quello Apple, frequentato da un miliardo di persone, contenente centinaia di miglia di app con un senso, consolidato da quindici anni.

Del resto General Motors conta di raddoppiare il fatturato da qui al 2030, portandolo a 280 miliardi di dollari (numeri abbastanza simili a quelli di Apple oggi). Di questa cifra, gli abbonamenti digitali ammonterebbero a 20-25 miliardi. Chiosa Gruber:

>L’obiettivo di fatturato dei servizi General Motors da qui al 2030 ha l’aria di un numero alla Kendall Roy: estratto dal nulla da qualcuno impegnato a riempire un foglio Excel fino a quando il risultato della funzione SOMMA() è quello che voleva.

Lascio agli appassionati di serie TV approfondire la figura di Kendall Roy e vado a dormire sufficientemente sereno. Credo che per un certo numero di manager General Motors fare la stessa cosa sarà, nei prossimi anni, più difficile.