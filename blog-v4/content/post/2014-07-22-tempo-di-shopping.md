---
title: "Tempo di shopping"
date: 2014-07-22
comments: true
tags: [Yamaha, DataRescue3, MacHeist, Yosemite, OSX]
---
Yamaha ha reso disponibili gratis alcune *app* per iOS, per esempio [Visual Performer](https://itunes.apple.com/it/app/visual-performer/id539927146?l=en&mt=8) e [Mobile Music Sequencer](https://itunes.apple.com/it/app/mobile-music-sequencer/id567375837?l=en&mt=8).<!--more-->

Inoltre, per pochissime ore ancora, [MacHeist](http://macheist.com) propone una offerta speciale di software Mac a 19,99 dollari. Che sarebbe come altre se non comprendesse Data Rescue 3, il migliore programma sulla piazza per il recupero di file perduti, che a listino costa 99,99 dollari.

Va detto che la versione 3 probabilmente verrà sostituita da una nuova versione 4 non appena uscirà [OS X 10.10 Yosemite](https://www.apple.com/it/osx/preview/). Per chi intenda restare su Mavericks, tuttavia, è un affarone.