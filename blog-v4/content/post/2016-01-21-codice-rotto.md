---
title: "Codice rotto"
date: 2016-01-21
comments: true
tags: [passcode, iOS, Mohamed]
---
Conoscevo la teoria. Arriva il momento in cui iOS ti chiede di rinnovare il codice di sblocco.<!--more-->

Codice che [sono costretto a usare](https://macintelligence.org/posts/2015-10-28-vita-da-passcode/), altrimenti mai lo prenderei in considerazione.

Il momento del rinnovo è una fonte cospicua di frustrazione, specie quando va ripetuto apparecchio per apparecchio. E mi chiedo anche quale effettivo valore abbia per la sicurezza: non è detto che un nuovo codice sia più sicuro del primo e nel 99 percento delle situazioni è una pura perdita di tempo.

Certo, se qualcuno ha *già* violato la sicurezza, il cambio del codice lo costringe a ripartire da zero. È una mossa che con un po’ di fortuna avrebbe potuto magari [aiutare Mohamed](https://macintelligence.org/posts/2016-01-06-pianto-natale/).

Per tutto il resto, è un fastidio che fa venire voglia di un’unghia incarnita, per averne meno.