---
title: "Viva la differenza"
date: 2024-01-06T15:38:47+01:00
draft: false
toc: false
comments: true
categories: [Internet, Software, Hardware]
tags: [TV+, Slow Horses, Tivoli, Tivoli Audio]
---
Ho chiuso le vacanze con due esperienza tecnologiche eccezionali, nel senso di eccezione rispetto alla media.

Una indecente. Più volte, durante le feste, i server sono stati indisponibili per le nostre richieste di radio Internet da fare passare attraverso la nostra radio [Tivoli](https://tivoliaudio.eu). Dalle musiche natalizie al jazz, i loro server mettono a disposizione una serie di stazioni radio tra le quali si trovano cose carine e gradevoli.

(Tutto è facilmente ottenibile in cento altri modi, questa è una critica al prodotto).

Il server che non funziona ci sta, l’assenza di una comunicazione, di un feedback puntale, di una interfaccia umana rifinita sono irritanti. Anche perché persistono da anni.

Lo hardware Tivoli è eccellente, bello, amichevole, valido, curato nei dettagli. La app [Tivoli Audio ART](https://tivoliaudio.it/pages/art-app-ios-android) è raffazzonata, incompleta, con messaggi di errore criptici quando ci sono, procedure bizantine, opzioni povere, ogni volta uno stress. Oggi ho detto va bene, ci ascoltiamo una playlist dalla musica che abbiamo su Mac, trasmessa alla radio via Wi-Fi.

Nel giro di pochi minuti l’ascolto di è interrotto, due volte. Il display aggiornava il titolo del brano una volte sì e due no. Se faccio la stessa cosa con tv e il televisore, posso andare avanti per una giornata e il Wi-Fi è sempre quello.

Una Tivoli vale l’acquisto. I servizi Tivoli disponibili con la app sono una fonte di imbarazzo per l’azienda. Impresentabili.

Ma la tecnologia può anche trasmettere buone vibrazioni. Ho assistito finalmente al finale di stagione della serie [Slow Horses](https://tv.apple.com/it/show/slow-horses/umc.cmc.2szz3fdt71tl1ulnbp8utgq5o).

Le recensioni in giro per il web sono positive e qualcuno ha addirittura definito la serie come una ragione sufficiente per abbonarsi a TV+; non arriverei a questo (anche se poi il servizio di Apple è una spanna superiore per qualità media dei contenuti), però una prova gratuita per vedere almeno un paio di puntate le consiglio di certo.

Il tema è spionistico, affrontato con un taglio inedito (un reparto dei servizi segreti inglesi dove vengono raccolti e messi a fare niente tutti gli agenti per qualche ragione sgraditi o disfunzionali). Occorrono occasionalmente gli ingredienti classici del film di spionaggio (inseguimenti, suspence, anche morti e violenza, uomo avvisato mezzo salvato; la serie ha rating di età quattordici+) e tuttavia il sale della trama è il rapporto tra i personaggi, i loro problemi personali e il modo con cui affrontano le situazioni impreviste in cui loro malgrado si trovano protagonisti.

Senza spoilerare niente, perché parte del divertimento è proprio vedere dispiegarsi il profilo dei personaggi, la recitazione è ottima, con almeno un paio di personaggi che si ricorderanno a lungo; gli intrighi sono… intriganti e la sceneggiatura è quasi sempre capace di tenere avvinti, o perché sta succedendo qualcosa o perché non succede niente ma i personaggi interagiscono, e sono forse i momenti migliori puntata per puntata. Un personaggio è strepitoso; l’interprete ha commentato che pensa di chiudere la carriera con questo ruolo. Le parole di chi recita vanno sempre prese con le pinze; fosse vero, sarebbe una degnissima conclusione, ai livelli di [Ted Lasso](https://tv.apple.com/it/show/ted-lasso/umc.cmc.vtoh0mn0xn7t3c643xqonfzy)-Jason Sudeikis.

È sempre tecnologia, è sempre digitale, sono sempre i nostri *device*. Che differenza, però, tra le cose fatte con impegno e il resto.