---
title: "Indipendenza dalle dipendenze"
date: 2020-08-03
comments: true
tags: [iPhone, Macmomo]
---
Il post sulla [necessità dell’indipendenza per sopravvivere al virus](https://macintelligence.org/blog/2020/07/31/guerra-di-indipendenza/) ha generato un certo dibattito, di cui ringrazio di cuore tutti perché le occasioni di imparare in modo sensato sono rare e le persone di spessore pure. Sono fortunato, molto fortunato ad averne tante che passano di qua.

Devo precisare meglio il discorso dell’indipendenza dall’hardware. Non entro nel mondo dei codici Ateco e do per scontato che vi siano milioni di mestieri soggetti a forti vincoli pratici. Però bisogna _tendere_ all’indipendenza e mai pensare da subito che qualcosa sia impossibile. Potrebbe non solo essere possibile, ma persino essere desiderabile e, semplicemente, mai stato considerato prima. Con la tecnologia le cose sono impossibili solo dopo averci provato.

Un ruolo cruciale lo svolge l’infrastruttura, dove va concentrato il meglio delle energie. Più è articolata e intelligente, più sarà possibile sfuggire ai vincoli. Non sarà _sempre_ possibile, ma più e meglio di prima, quello sì.

Macmomo poneva una domanda decisiva:

> Quanto costa creare un'infrastruttura adeguata a permettere il lavoro da casa?

In Italia, poi, è una domanda tremenda. Eppure l’Italia è proprio la risposta: abbiamo la prova evidente di quanto ci costi non averla creata, ora che ci farebbe un gran comodo.

C’è anche il rovescio della medaglia. Si potrebbe peccare di _hubris_ e pensare che l’infrastruttura possa risolvere tutto. Che tutti i casi di lavoro sia catalogabili a priori. Che si possa normalizzare ogni necessità. Chiaramente questo è un eccesso.

Stiamo lontani. Al tempo stesso, dovremmo iniziare a distanziarci anche dall’eccesso opposto, l’infrastruttura zero. Pensare che tutti i file con del testo si aprano in Word. Pensare che qualsiasi funzione aziendale si risolva con un plugin WordPress. Lanciare Excel per disegnare il piano ferie. Prototipare in PowerPoint. La creatività è bellissima, la capacità di improvvisare una soluzione a un problema è invidiabile, essere eclettici è una dote.

Poi però arrivano tempi difficili e, guarda tu, a cavarsela meglio degli altri sono le Big Tech. Sarà una coincidenza naturalmente; comunque, nelle Big Tech non usano le applicazioni come apriscatole e non vivono di plugin, né usano l’email come discarica di byte da replicare all’infinito. A cavarsela meglio degli altri è chi ha l’infrastruttura più solida e cogente. Costa molto? Immagino di sì. Anche le case costano molto, ma tanta gente che conosco appena può accende un mutuo. Una ragione ci sarà.