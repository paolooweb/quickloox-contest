---
title: "Chi sfrutta chi"
date: 2020-11-10
comments: true
tags: [Apple, Pegatron, Cina, Taiwan, wanton, Nyt]
---
Apple ha verificato che Pegatron [violava certe regole](https://www.nytimes.com/2020/11/09/business/apple-china-pegatron.html) che limitano le ore di impiego degli studenti lavoratori.

Il fornitore taiwanese, tutt’altro che un pesce piccolo, non riceverà altre commesse da Apple fino a che avrà dimostrato di essersi messo in regola. Il responsabile del programma di impiego degli studenti lavoratori in Pegatron, inoltre, è stato licenziato.

Si può malignare quanto si vuole sullo sfruttamento da parte occidentale di forza lavoro a basso prezzo, a patto di fare i dovuti distinguo. Sarebbe molto più comodo chiudere un occhio o raggiungere un accordo di massima sui tempi per continuare come prima, che individuare un fornitore alternativo. Pegatron è un fornitore grosso.

Aspetto inoltre di leggere di una qualsiasi altra multinazionale tecnologica, di quelle che a parole innovano, capace di mettere in castigo un fornitore importante per una questione etica che, in Estremo Oriente, per molti può valere meno di un wanton fritto.

Anzi, la prossima volta che leggo un articolo sugli occidentali che sfruttano i Paesi poveri, chiederò quanto è stato pagato chi lo ha scritto.