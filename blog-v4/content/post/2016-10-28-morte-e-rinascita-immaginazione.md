---
title: "Morte e rinascita dell’immaginazione"
date: 2016-10-28
comments: true
tags: [grue, Zork, IFComp]
---
Se giocavi a [Zork](https://textadventures.co.uk/games/view/5zyoqrsugeopel3ffhz_vq/zork) ti capitava prima o poi di essere mangiato da un [grue](http://zork.wikia.com/wiki/Grue) per essere entrato in un posto buio senza una fonte di luce a disposizione.<!--more-->

Nessun sopravvissuto a Zork ha mai visto un *grue*. Tutti lo hanno immaginato. Se avessimo fatto dei disegni avremmo centinaia di migliaia di *grue* diversi, ognuno dei quali azionato dalla nostra fantasia.

La computergrafica ha reso possibile visualizzare i sogni più incredibili. D’altronde, ha ucciso l’immaginazione e viene usata per il marketing più perverso. Ogni giorno su Internet passa la simulazione 3D di un nuovo computer, un nuovo *gadget*, un nuovo *concept*. Realistica quanto serve per evitare di finire in tribunale, finta il giusto per accendere non l’immaginazione, bensì la credulità. Sono un entusiasta della computergrafica, tuttavia devo riconoscere questo effetto collaterale che è dannoso per le menti non allenate.

Per questo dico: alleniamoci. Anche quest’anno si tiene IFComp ed è possibile scaricare, o giocare nel *browser*, svariate [decine di avventure testuali](https://ifcomp.org/ballot) prodotte per la manifestazione, non reperti archeologici ma cose nuove e attuali, per quanto su un registro di moda molti anni fa. Volendo si possono anche votare le migliori.

Alleniamoci: scateniamo l’immaginazione, figuriamo nella nostra mente la descrizione testuale sullo schermo, creiamo scenari che dureranno magari un decimo di secondo ma resteranno unici e, per questo, di valore inestimabile.

Poi saremo pronti ad affrontare tanta computergrafica ingannevole. Ma saremo vaccinati e capiremo quanta scarsità di immaginazione ci sia dietro tanto marketing tridimensionale.