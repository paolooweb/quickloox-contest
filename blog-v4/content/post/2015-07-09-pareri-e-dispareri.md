---
title: "Pareri e dispareri"
date: 2015-07-08
comments: true
tags: [HT, Schneier, Attivissimo, iOS, YouEat, jailbreak]
---
Siamo sempre al [casino megagalattico](https://macintelligence.org/posts/2015-07-07-belle-liberta/) suscitato dall’attacco informatico a Hacking Team, italiani che vendevano a governi buoni e cattivi buchi di sistema e strumenti per intercettare a destra e a manca.<!--more-->

Questo è ciò che [dichiara Bruce Schneier](https://www.schneier.com/blog/archives/2015/07/hacking_team_is.html), uno dei massimi esperti globali di cibersicurezza:

>Hacking Team non aveva exploit [tecniche di violazione] per un iPhone non sottoposto a jailbreak. Sembra essere la piattaforma da scegliere per stare sicuri.

Nel frattempo Paolo Attivissimo, massimo esperto di bufale informatiche ma sempre pronto a evitare l’approfondimento appena si parla di Apple, [riporta](http://attivissimo.blogspot.it/2015/07/lo-spione-spiato-hacking-team-si-fa.html) che

>Apple avrebbe dato a Hacking Team un certificato digitale come iOS enterprise developer per firmare le app e quindi farle sembrare sicure e approvate […] perché firmate col certificato. […] Sarà interessante vedere la reazione di Apple.

Apple non lo so, ma chiunque sia stato sul sito del [Developer Enterprise Program](https://developer.apple.com/programs/enterprise/enroll/) sa che un certificato digitale viene dato a *qualunque* azienda lo chieda e paghi la quota di iscrizione al programma. L’unico requisito è esistere legalmente.

Ancora, il certificato digitale non riguarda la sicurezza, ma la firma: sai che il programma arriva certamente da quello sviluppatore lì. Che cosa ci sia dentro è un’altra questione e non riguarda il certificato. Aggiungo che il certificato aziendale non riguarda la distribuzione su App Store. E una app inviata, che so, per posta elettronica (!) chiederebbe comunque di dare fiducia allo sviluppatore per essere installata.

La mia reazione è *molto rumore per nulla*.

Adesso Paolo è dietro a [una traccia](http://attivissimo.blogspot.it/2015/07/perche-hacking-team-discuteva-di.html) per cui in App Store si trova YouEat, una app capace sembra, pare, forse di accendere il microfono e prendere schermate all’insaputa dell’utente, che [si dice sarebbe stata testata](https://wikileaks.org/hackingteam/emails/emailid/29564) su iPhone 4S e iOS 7.1.2 e non riesce più a prendere schermate su iOS 8. Paolo non vede l’ora:

>Se quest’apparenza venisse confermata, sarebbe uno smacco notevole per il modello di sicurezza di Apple.

Certo. Riassumo: si vocifera senza conferme di una app su un milione e mezzo, che magari forse sarebbe capace – su un modello non più in vendita e sistema operativo usato oggi da [un apparecchio su sette](https://developer.apple.com/support/app-store/) – di fare pressoché niente rispetto allo standard di Hacking Team su Android.

Uno smaccone. Io leggerei Schneier, va’.