---
title: "Intelligenza colossale"
date: 2017-05-29
comments: true
tags: [Colossal, Cave, Adventure, GitLab]
---
Bella notizia, che [Adventure sia diventato open source liberamente accessibile](https://gitlab.com/esr/open-adventure) in forma di codice sorgente su un repository (contenitore) libero.<!--more-->

Forse c'è un pizzico di esagerazione in commenti come questo:

Ma neanche troppa. Colossal Cave, di cui Adventure è sorella minore, è una pietra miliare e questa decisione è la migliore tanto per future evoluzioni del software quanto per la conservazione dell'originale. Non inganni la disponibilità di [Adventure per Mac](http://www.lobotomo.com/products/Adventure/) a prescindere dalla notizia (nonché la presenza di [dunnet in emacs](http://www.macworld.com/article/1047210/mac-apps/oldschooladventure.html)).

Si parla molto di intelligenza artificiale (che non è tale, solo apprendimento meccanizzato e lo dico dispiaciuto, perché sostengo l'idea), eppure io vorrei vedere da qualche parte un software che crea un nuovo gioco o un nuovo modo di giocare, che abbia un successo planetario e ispiri nuovi generi.