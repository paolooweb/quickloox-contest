---
title: "Il dono della riservatezza"
date: 2023-11-22T14:07:38+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [Signal]
---
Si avvicinano le feste e naturalmente la concorrenza sulla destinazione di eventuali regalie è molto forte. Eppure voglio osare e proporre alle menti più aperte una donazione, anche modesta – il punto è manifestare un proposito, non impressionare la platea – a favore di Signal.

Sul blog dell’organizzazione è comparso un post sulla [struttura dei costi con le relative implicazioni tecniche](https://signal.org/blog/signal-is-expensive/) di Signal, tanto dettagliato e preciso che fa sanguinare l’attenzione e no, non è un elenco di voci di spesa, ma un ragionamento articolato.

Viene fuori che Signal avrà l’anno prossimo un fabbisogno vicino ai cinquanta milioni di dollari e che lo staff è minuscolo (ma ben pagato) rispetto a quello di tutti gli altri cugini.

Risulta pure che Signal è una società no profit e che il suo intento è reggersi sulle spalle di numerosi piccoli e generosi donatori. Niente *dark pattern* per incastrare la gente dentro la piattaforma, niente monetizzazione sui dati (anzi, c’è l’impegno di memorizzare il meno che si può e già questo costa oltre un milione di dollari l’anno).

In cambio? Privacy *vera*. Scambio di messaggi che *veramente* riguarda solo mittente e destinatario, niente cifrature con il buco, niente abuso di metadati, niente di niente. La messaggistica come dovrebbe essere (Apple ci va vicina, ma non ancora del tutto).

È una di quelle imprese digitali che dovrebbero essere al primo posto nei nostri pensieri. Fa parte di quella libertà che tutto è nato per offrirci e sempre più viene tolta, soprattutto agli sprovveduti. Non avevamo capito che avremmo dovuto volerla e perseguirla, tenercela cara e che va bene scambiare dati con servizi, purché in chiarezza onestà e trasparenza.

Insieme ai film, alla musica, al fitness, allo sport, alla gastronomia, alle vacanze, ai parenti e agli hobby (con assoluto rispetto per ciascuna categoria) ci tocca occuparci, anche una volta l’anno, di questioni più grandi e apparentemente meno personali, dalla libertà e dalla democrazia giù fino al nostro giochino open source prediletto.

In questa gerarchia di bisogni inedita c’è un posto anche per la riservatezza delle nostre comunicazioni. Non è questione di nascondere chissà che cosa, ma rimanere giustamente nel privato perché è un nostro diritto e non una misura di emergenza o di disonestà. Un pensierino per Signal a Natale lo potremmo fare.