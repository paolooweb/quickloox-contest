---
title: "La pazienza vince su tutto"
date: 2022-06-28T00:38:48+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [macOS, download]
---
Oggi mi sono preso i miei tempi. Anche Mac.

![Finestra di dialogo: 29 giorni per finire il download](/images/29-days.png "Magari riprovo a febbraio 2024.")

![Finestra di dialogo: cinquecentoottantaquattro ore per finire un download](/images/584-hours.png "Quei sette minuti lì mi sembrano esagerati; qualcosa non va.")