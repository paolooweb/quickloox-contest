---
title: "Colpito e afFoundation"
date: 2021-12-21T02:36:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [Foundation, Isaac Asimov] 
---
Reduce dalla visione completa di [Foundation](https://tv.apple.com/it/show/foundation/umc.cmc.5983fipzqbicvrve6jdfep4x3), provo a dare qualche parere sintetico, di massima e sommamente incompetente, a spoiler zero.

Bisogna subito sgombrare il campo dall’idea di vedere sullo schermo una trasposizione dei libri di Asimov. Il ciclo della Fondazione ispira certamente la serie, che però si prende numerose libertà in ogni direzione.

Diversi archi narrativi si snodano parallelamente e li ho trovati molto vari e discontinui. Uno degli archi è stato molto interessante, pieno di suggerimenti e collegamenti alla realtà e alle problematiche di oggi, davvero affascinante. Un altro consiste in fantascienza ai margini del B-movie, veramente niente di speciale. Un altro ancora ha lo scopo di seminare le rivelazioni e i colpi di scena episodio per episodio e sembra stato messo insieme abbastanza faticosamente. Probabilmente è quello meno riuscito, anche con qualche lungaggine. Due archi narrativi si congiungono alla fine della serie, a mio parere inutilmente.

La parte visiva è molto bene realizzata, in certi momenti anche sontuosa. Dallo spazio profondo al deserto al classico interno da astronave, passando per le palafitte al pianeta di periferia dal clima ostile, tutto si guarda con piacere. I costumi sono mediamente azzeccati. Gli effetti speciali, ottimi. La colonna sonora è adeguata.

Il cast è di qualità molto variabile, da picchi di eccellenza a personaggi davvero poco incisivi, anche dove hanno una parte rilevante nella trama.

La sceneggiatura deve essere stata un lavoro impegnativo e si vede; è tutt’altro che perfetta. Il tentativo di impostare una narrazione di lungo respiro è notevole ma non riesce, come era possibile aspettarsi del resto data la mole di materiale disponibile in partenza. All’interno di ciascun arco narrativo, tuttavia, alcuni episodi sono davvero coinvolgenti e in generale i singoli segmenti funzionano bene, solo che è davvero difficile tessere una trama di così vasto respiro.

Sui dialoghi non dico nulla perché ho seguito la serie in inglese e qua e là ho perso qualche frase per strada. Ho riscontrato grosso modo le stesse differenze di qualità corrispondenti agli archi narrativi. Un arco filosofeggia in modo abbastanza insistito e alla lunga irritante, un altro è superficiale, un terzo va spesso al sodo e colpisce nel segno.

È valsa certamente la pena di guardarla e magari ridarò un’occhiata ad alcuni episodi. Di sicuro non riaprirò i libri, che sono tutt’altra saga con tutt’altro svolgimento.

_Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile._