---
title: "La qualità non si licenzia"
date: 2023-10-18T15:45:09+01:00
draft: false
toc: false
comments: true
categories: [Software, Internet]
tags: [Stack Overflow, ai, intelligenza artificiale]
---
*Stack Overflow* ha finito per [licenziare oltre un quarto dei dipendenti](https://stackoverflow.blog/2023/10/16/stack-overflow-company-announcement-october-2023/).

Per gli ignari, si tratta di una community dove si risponde a domande sulla programmazione. Per molti lunghi e fausti anni, il punto di riferimento; sapevi che qualcuno avrebbe risposto, se appena la domanda era sensata.

Come succede in tutte le comunità di *nerd*, a volte le risposte arrivano un po’ crude o condite con del sarcasmo, oppure ridotte all’indispensabile per vedere se te la cavi lo stesso eccetera. Se sbagli a porre la domanda o poni una domanda effettivamente poco centrata, certamente arriva qualche battuta.

A nessuno comunque è stata inflitta violenza psicologica. Il più delle volte le risposte basta cercarle nella base dati, dato che le questioni fondamentali della programmazione sono sempre quelle e sempre lì. Se appena uno si è letto qualche risposta, sa benissimo che aria tira. Nel dubbio può comprarsi un buon libro, cambiare sito, darsi da fare.

Va anche detto che questo succede, toh, nel dieci percento dei casi? Per gli altri. *Stack Overflow* costituisce una community grande e preparata dedicata ai problemi della programmazione. Qualcosa di molto prezioso, che tanti professionisti pagano per approfittarne in tutte le salse previste dal contratto.

Poi arrivano i sistemi generativi. Improvvisamente fai una domanda di programmazione e rischi di avere una risposta immediata, giusta, nel tuo browser, da un software che se la fa ripetere dieci volte e non si arrabbia né fa battute. A volte la risposta è sbagliata, incompleta, fuorviante, ma chi se ne importa? È gratis, è pronta.

Stack Overflow è di fronte a una minaccia esistenziale, come tutte le iniziative dove sono le persone e il loro sapere a portare valore. E in risposta inizia a sviluppare OverflowAI, la sua versione di sistema generativo. La risposta, è il messaggio, te la dà comunque un automa, ma almeno fattela dare dal *nostro* automa.

Cosa può andare storto? Magari il fatto che, generativo per generativo, la gente usi i suoi soldi per pagare ChatGPT, con cui fa tutto, invece che Stack Overflow, che fa una cosa sola. Un nucleo di professionisti disposti a pagare per la community esisterà sempre, ma si restringe. La classe media, per dirla male, preferisce risparmiare. Il feedback è di minore qualità? Smetto di parlare con persone? Imparo meno? Chissenefrega, spendo meno.

Credo che Stack Overflow non avesse alternative. Se il denaro investito in OverflowAI lo avessero messo in cura della community e dei contenuti, servizi personalizzati, che so, una convenzione con [O’Reilly](https://www.oreilly.com) per consultare *on demand* pezzi dei suoi libri, credo che alla fine il risultato sarebbe rimasto lo stesso. La gente che vede poco valore se ne va. Licenziano.

Però sarebbero diventati l’esempio della *qualità*. Costano, è difficile, roba per pochi, ma la punta di diamante della programmazione è quella.

Invece oggi costano e usano il sistema generativo, come l’ultimo dei cantinari che si accontenta di ChatGPT. Licenziano.

Non c’è scampo per una community nell’era di ChatGPT, non se si guarda ai numeri. Bisogna giocarsela diversamente, andare sulla qualità, non temere di ridursi come numero, ma servire quei pochi in modo che nessun LLM può fare.

ChatGPT non porterà mai qualità, per definizione. Il sistema è progettato per tendere alla mediocrità. L’unico modo per non farsi licenziare è convertirsi alla qualità.