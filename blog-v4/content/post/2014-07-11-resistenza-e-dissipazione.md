---
title: "Resistenza e dissipazione"
date: 2014-07-11
comments: true
tags: [Mavericks, Yosemite, Chitika, Cook]
---
Mi dicono (scherzo, lo rileva [Chitika](http://chitika.com/insights/2014/yosemite-preview-adoption)) che la Developer Preview di Yosemite ha un tasso di adozione quasi quadruplo di quanto avvenne un anno fa con Mavericks. Se tanto dà tanto, l’edizione definitiva e pubblica di OS X 10.10 sarà ancora più adottata di 10.9, che a detta di Tim Cook nel giro di un anno si era presa già oltre la metà delle macchine.<!--more-->

Ciò mi spinge a riflettere sull’atteggiamento che alcuni ancora hanno, di restare ancorati per principio a una versione precedente del sistema e ostinarsi a mantenerla con un sacrificio di tempi, scomodità e problematiche ammirevole e sempre più inconsistente nei risultati.

Non parlo di iOS, dove l’adozione del sistema più recente è praticamente forzata, forse anche oltre il ragionevole. Non parlo neanche di macchine con limiti di aggiornamento (che comunque stanno superando il lustro di vita e meritano grande rispetto, come il mio MacBook Pro inizio 2009). Quelle situazioni dove c’è un MacBook che sarebbe aggiornabile e il tempo del proprietario va in conservazione faticosa di qualsiasi sistema purché sufficientemente arretrato.

C’era il pretesto del prezzo, superato già da Mavericks che ha inaugurato un’epoca di gratuità. C’era il pretesto delle applicazioni per cui mantenere la compatibilità, che mi sembra sempre più labile e sempre più limitato a nicchie cui il sostenitore del pretesto spesso non appartiene.

Il resto mi sembra resistenza al cambiamento fine a se stessa. Che complica marginalmente la vita dell’ecosistema globale e orribilmente la vita del proprietario.

In un circuito elettrico la resistenza ha un senso per ottenere un risultato preciso. Messa per il gusto di metterla, è dissipazione di energia.
