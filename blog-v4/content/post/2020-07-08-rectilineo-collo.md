---
title: "Rectilineo collo"
date: 2020-07-08
comments: true
tags: [Mac, iPad, BBEdit]
---
Invece di uscire *da una storia di tre anni con un tipo*, come [cantava Elio](https://www.youtube.com/watch?v=fSeInfAD0xs), vengo da un mese di utilizzo esclusivo di iPad Pro per lavoro. È successo altre volte, ma questa è stata speciale: per la prima volta ho sentito di tirare il collo alla macchina. Con i Mac sono uno specialista e, se esistesse una certificazione, me la darebbero *honoris causa*; su iPad Pro doveva ancora succedere.

È un attestato di soddisfazione, eh. Vuol dire che in un qualche senso abbiamo raggiunto una comunanza di sentire, l’apparecchio è diventato davvero *wheels for the mind* come si diceva tanto tempo fa, l’amplificatore di intelligenza. Sudavo, l’adrenalina correva tra le scadenze e le difficoltà intrinseche del lavoro; davo fondo alle migliori energie per arrivare all’obiettivo e lui pure.

A seguito dell’esperienza (a lieto fine), tornato alla scrivania di Mac, mi sento vagamente più titolato a scrivere due righe sulla differenza tra le altrettante due macchine, o meglio: di che cosa, ora che sono davanti a un Mac, sentivo la mancanza.

Prima di ogni altra cosa, lo so che mi ripeto ma [Rich Siegel](https://developer.apple.com/news/?id=r2xowjrr) (forse) non mi ascolta: mi manca [BBEdit](http://www.barebones.com/products/bbedit/). Ci sono soluzioni ottime su iPad, molto migliori di anni fa. BBEdit però è un’altra cosa.

I comandi da tastiera. Sono aumentati, non abbastanza. Ho le mie [perplessità sul trackpad per iPad](https://macintelligence.org/blog/2020/06/10/il-punto-sul-puntatore/) e e le tengo, in direzione ostinata e contraria. Sulla tastiera non c’è discussione; quando ne metto una davanti a iPad, diventa parte integrante della macchina e voglio poterci scaricare tutta la mia megalomania. Dominio completo della macchina, i polpastrelli devono poter volare tra i tasti, per ore se richiesto.

La fluidità tra le app. Cambiare dinamicamente e continuamente applicazione si fa, funziona, c’è multitasking, c’è perfino il *drag and drop*. MacOS, comunque, ha dieci anni di sviluppo in più e quando si corre a pieno ritmo la differenza c’è.

Il Terminale. Questa era facile, lo so. Non che manchino soluzioni, come [a-shell](https://holzschu.github.io/a-Shell_iOS/) per esempio. Non voglio soluzioni, però; sembra che rispondano a un problema. Il Terminale non è una soluzione, non esiste un problema. Il Terminale è un modo di pensare.

Sembra poco. L’elenco è più lungo, però da qui in poi si fa più complesso.

Un esempio è Apple Pencil. Non ho una Apple Pencil e ho vissuto benissimo senza. In questo tendere al limite, beh, per spremere al massimo un iPad Pro ci vuole una Apple Pencil. Mi è mancata. La parte sottile e perigliosa del ragionamento è che si può spremere al massimo un Mac senza una Pencil, ma questo non è un *minus* di Mac. Per un iPad, Apple Pencil è un accessorio naturale, come la tastiera di cui parlavo sopra.

Un altro esempio è iCloud Drive, che tira matta una intera popolazione di persone. Sono solidale con loro e non voglio sottovalutare le lacune del sistema; questo detto, nella mia esperienza a rotta di collo ho dovuto creare una pletora di file, con app diverse, e gestirli in modo organizzato. L’ho fatto in iCloud Drive come si deve fare su iCloud Drive, invece di trattarlo come il Finder dei poveri. Ho avuto zero problemi. iCloud Drive è robustamente inferiore al Finder e non si può neanche iniziare un confronto. Nemmeno per lamentarsi; iCloud Drive va usato in modo diverso e, per quanto possa abbondantemente migliorare, non mi ha fatto sentire la mancanza del Finder.

Potrei scrivere un elenco simmetrico: cose di cui sento la mancanza su Mac. Il punto non era fare una classifica, quanto rendermi conto – con più cognizione di causa, grazie allo scriverne – delle reali differenze tra le due macchine. Lo schermo touch, per dire, è una differenza piuttosto evidente, solo che non conta. Altre, invece, si sentono.