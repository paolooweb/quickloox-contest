---
title: "Concorso esterno"
date: 2014-10-05
comments: true
tags: [Jobs, Macklowe, Avitzur, GraphingCalculator, AppleStore]
---
Uno dei capitoli non ancora scritti della vasta epopea di Apple: le persone esterne ad Apple che hanno avuto influenza, spesso decisiva, in momenti importanti.

Un esempio che salta subito alla mente è la Calcolatrice Grafica, software iconico del vecchio Mac OS eppure [creata da Ron Avitzur](http://www.pacifict.com/Story/) senza impiego e senza contratto, lavorando in semiclandestinità nella sede di Apple fino a riuscire miracolosamente a fare inserire il software nei dischi di sistema ufficiali.

Più di recente è salita alla ribalta la storia dell’Apple Store della Fifth Avenue, una delle strutture più fotografate al mondo. Nato, [racconta NYMag](http://nymag.com/daily/intelligencer/2014/09/story-behind-the-apple-store-cube.html), grazie all’intraprendenza di un proprietario immobiliare che seppe risolvere in modo brillante un problema di sfruttamento di spazio e, cosa molto più difficile, riuscì a rapportarsi con Steve Jobs fino ad arrivare a una conclusione positiva.

Jobs fu noto per la sua capacità di cambiare idea anche repentinamente se capiva di essere in errore o se arrivava un’idea migliore della sua; la questione era farsi ascoltare.

Harry Macklowe c’è riuscito e pensandoci bene, il capitolo potrebbe ancora essere ancora più specifico: non le persone esterne ad Apple che hanno avuto influenza, ma che hanno saputo fare cambiare idea a Steve Jobs.