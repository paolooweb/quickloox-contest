---
title: "Un lifting alle righe"
date: 2022-09-24T00:13:17+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [rs, Bsd, Unix, Terminale, Dr. Drang]
---
E anche alle colonne, con l’insperato aiuto di un comando Unix di BSD presente, all’insaputa mia e forse non solo, in macOS.

[rs](https://www.manpagez.com/man//1/rs/) prende una tabella di righe e colonne e la ridispone secondo i desideri dell’operatore.

Nessuno presenta queste coese meglio di Dr. Drang e bisogna assolutamente leggere il [racconto della sua esperienza con il comando](https://leancrew.com/all-this/2022/09/reshaping-text/). Va bene anche guardare le figure, perché i passaggi tra una versione e l’altra della tabella sono evidenti.

Tutto veloce, aggraziato, riuscito. Arrivati a fondo pagina ci si sente più giovani.