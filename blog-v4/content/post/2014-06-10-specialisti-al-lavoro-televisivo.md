---
title: "Specialisti al lavoro televisivo"
date: 2014-06-11
comments: true
tags: [Rai, Raisport, basket, Sky, SkyGo]
---
In trasferta con la chiavetta attaccata al Mac, che prende connessione telefonica e crea rete Wi-Fi per gli apparecchi intorno.<!--more-->

Nel contempo Raisport trasmette sul digitale terrestre una semifinale del campionato di basket. Da inguaribile appassionato accendo il televisore.

Dopo poco il segnale si degrada fino a scomparire. Colpa dell’antenna? No, tutti gli altri canali del digitale terrestre si vedono perfettamente.

Mi viene l’idea. Accendo la *app* [Raisport](https://itunes.apple.com/it/app/raisport/id448639544?l=en&mt=8).

La *app* mostra l’elenco delle telecronache. La telecronaca non la mostra, però: il cursore gira, gira e non si vede niente.

Sarà colpa della connessione cellulare, penso. Provo [SkyGo](https://itunes.apple.com/it/app/sky-go/id454435005?l=en&mt=8) per riscontro: perfetta. Volendo potrei guardarmi la finalissima Nba in ogni particolare. Peccato che per una volta mirassi al campionato italiano.

Mi dice l’amico: *bella forza che Sky funziona, per vederla devi pagare l’abbonamento*.

Già. Perché invece, la Rai…