---
title: "Ma non si vede"
date: 2017-08-20
comments: true
tags: [Apple, Gopher, Veronica, Archie, Jughead, curriculum]
---
È stato [dissotterrato un sito segreto](https://9to5mac.com/2017/08/19/apple-engineer-secret-job-posting/) (e già eliminato) allestito da Apple per reclutare talenti particolarmente brillanti, da adibire allo sviluppo di un *componente chiave dell’ecosistema*.<!--more-->

Facile ironizzare su certi curriculum *in formato europeo* che ancora si vedono girare come se effettivamente avessero un senso (svelto: quanti curriculum hanno dato lavoro *grazie al* formato europeo e non lo avrebbero dato in sua mancanza?).

Carino interrogarsi su quanti talenti avrà davvero fruttato l’iniziativa. Suppongo che dopo la notizia il livello delle candidature sarà calato.

Rifletto su quante cose del genere girino in rete rispetto a quelle che conosciamo. In fondo conosciamo la parte di Internet che raccontano Google e colleghi, e niente più. Mica per caso le nostre ricerche geografiche sono affidate a mappe, invece che a un database di luoghi. Sappiamo del *deep web* esattamente quello che sappiamo dei nostri sottosuoli.

Quando il web non c’era, tutto stava su server ftp, accessibili via telnet. Poi il protocollo [Gopher](https://it.wikipedia.org/wiki/Gopher_(informatica)), [Veronica, Archie](http://www.netlingo.com/more/gopher.php) e poi la necessità di sapere qualcosa di Unix, o di Vax.

Sì, se oggi si volesse mettere qualcosa in rete per farlo scoprire a pochissimi, forse varrebbe la pena di rendere accessibile quel qualcosa solo via Gopher.