---
title: "Libertinaggio librario"
date: 2013-07-17
comments: true
tags: [iPad, ebook, Mac]
---
Quale potrebbe essere un insieme convincente di strumenti e ambiente per lavorare a *ebook* in formato ePub su iPad?

Su Mac conosco un po’ la strada. Il lavoro principale avviene per definizione su [BBEdit](http://barebones.com/products/bbedit/). Un gradino più sotto sta [Sigil](https://code.google.com/p/sigil/), che lavora efficacemente su progetti più o meno completi per le ultimissime limature. Non ho esperienza di [BlueGriffon Epub Edition](http://www.bluegriffon-epubedition.com/BGEE.html), che meriterebbe un’occhiata.<!--more-->

Per validare e generare gli ePub su Mac, gli [AppleScripts for ePub Creation](https://code.google.com/p/epub-applescripts/downloads/list) sono più che collaudati.

Su iPad sono più indietro. Il primo e più rozzo sistema per trattare con gli ePub è… [Goodreader for iPad](https://itunes.apple.com/it/app/goodreader-for-ipad/id363448914?l=en&mt=8), che tratta i file .zip e dispone di un editor interno. Soluzione rudimentale, buona per un’emergenza, non per lavoro sistematico.

Ci sono buonissimi editor di testo a partire da [Textastic](http://www.textasticapp.com), ma non penso che riescano a lavorare per progetto come fa BBEdit su Mac. Forse ci va vicino [Daedalus Touch](http://daedalusapp.com) con la sua metafora delle pile di carta. Ho qualche dubbio tuttavia sulla sua potenza effettiva, automazione delle operazioni, espressioni regolari, quelle cose lì.

*App* come [Easy ePub Creator Pro](https://itunes.apple.com/it/app/easy-epub-creator-pro/id496165321?l=en&mt=8), che costano 13,99 euro e poi promettono *ebook* *a pagina rigida*, francamente sembrano superflue. iBooks Author a confronto è una meraviglia, gratis.

Validazione? Certo, posso caricare un file da iPad su [Epub Validator](http://validator.idpf.org/), però non è una soluzione elegante. iBooks effettua un controllo in lettura e quindi può costituire una soluzione di base; non una soluzione.

Per la generazione dell’*ebook* esistono strumenti come [Creative Book Builder](https://itunes.apple.com/it/app/creative-book-builder/id451041428?l=en&mt=8), di cui non conosco la solidità e la consistenza. Vantano molte funzioni e la generazione dovrebbe essere più o meno garantita, però – per l’esperienza accumulata in tema – tendo a diffidare di una *app* da 3,59 euro.

Accetto suggerimenti e critiche di ogni ordine e grado, con ringraziamenti anticipati per ogni passo in più verso la libertà totale di ePub su iPad.