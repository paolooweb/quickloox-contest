---
title: "Non può durare"
date: 2018-12-04
comments: true
tags: [iPhone]
---
Bel pezzo del *New York Magazine* sul [rallentamento globale delle vendite di smartphone](http://nymag.com/intelligencer/2018/12/global-u-s-growth-in-smartphone-growth-starts-to-decline.html), pardon, computer da tasca.

Il mercato mondiale è saturo perché il tasso di crescita è praticamente zero, cioè si vendono tante unità quanto nell’anno precedente, o magari anche meno. E la frequenza di sostituzione è passata da venti a ventiquattro mesi.

Dati interessanti, peccato i soliti toni da catastrofe ambientale inevitabile per quanto riguarda Apple. Che è in una situazione diversa dai concorrenti, ma *non può sfidare la gravità indefinitamente*, riferito al vendere apparecchi dal prezzo superiore a quello medio. *Apple esaurirà la sua strategia*, si legge, come se fosse una scoperta.

Anche da pensionato, Bill Gates continua a condizionare il modo di pensare.

>Non credo che il successo di iPod possa continuare nel lungo termine.

Si sentiva lo schiumare di bile, quando [lo ha detto](https://www.techradar.com/news/phone-and-communications/mobile-phones/ipod-won-t-last-says-gates-186327).

Naturalmente iPod non è durato, nel lungo termine. Solo dieci anni, nei quali i prodotti corrispondenti di Microsoft hanno fallito.

Neanche iPhone durerà per sempre. Ma nemmeno Apple, neanche noi e nemmeno il pianeta.

Il punto è avere in mente il *dopo*. È lì che si decide. Ma il giornalista quadratico medio non ce la fa.