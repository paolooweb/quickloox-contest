---
title: "Libertà che non ci sono"
date: 2015-09-29
comments: true
tags: [Xcode, Attivissimo, Gatekeeper]
---
Qualcuno pensa sul serio che sul proprio computer uno installi *ciò che vuole*. Sono [i minion](http://attivissimo.blogspot.it/2015/09/lapp-store-subisce-la-prima-infezione.html#c8637433609810344938) sul blog di Paolo Attivissimo, innumerevoli *minus habentes* da Facebook e oltre, faciloni su forum di livello infimo, squilibrati – loro sì – in libertà.

Poi succede che su App Store (per lo più cinese) finiscano [app infette da malware](http://researchcenter.paloaltonetworks.com/2015/09/more-details-on-the-xcodeghost-malware-and-affected-ios-apps/). Per la ragione che vari sviluppatori hanno disattivato scientemente [Gatekeeper](https://support.apple.com/it-it/HT202491), scaricato volontariamente una copia di Xcode da una fonte non ufficiale, compilato app senza verificare il risultato. La copia di Xcode non genuina era stata truccata e le app generate erano bacate alla fonte.

Gente che ha installato e fatto sul proprio computer quello che voleva. *Senza esserne capace*.

Se si parla di libertà senza parlare di conoscenza e dimenticandosi della responsabilità, manca la gran parte del necessario.

Quando manca qualcosa, c’è una deficienza. Chi inneggia a questa mancanza è un deficiente.

La libertà di infliggere danni a chi non c’entra, lavorare male, ignorare la sicurezza del proprio sistema e soprattutto di quella altrui, non c’è.