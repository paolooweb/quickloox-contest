---
title: "Le belle canzoni di una volta"
date: 2024-02-09T02:03:46+01:00
draft: false
toc: false
comments: true
categories: [Hardware, Internet]
tags: [Bill Wyman, Apple II+, RetrocomputerClub, Space Invaders, PR#6]
---
Faccio una eccezione alla regola che il retrocomputing va bene purché non sia fine a se stesso e incoraggio chiunque a iscriversi su Facebook (se si frequenta Facebook) a RetrocomputerClub.

Perché hanno trovato un reperto eccezionale: [la foto di un grande della musica rock alle prese con un computer dell’epoca](https://www.facebook.com/groups/362543030506742/posts/7074622402632071/). (Sì, credo che il link funzioni solo per gli iscritti).

Non è *spoiler* rivelare che si tratta di [Bill Wyman](https://billwyman.com), bassista dei Rolling Stones fin verso il termine del ventesimo secolo, alle prese con [Space Invaders](https://freeinvaders.org) nientemeno che su un Apple II+ con doppio lettore di floppy.

Non è spoiler perché il gusto sta tutto nell’atmosfera dell’immagine. Wyman è palesemente interessato e divertito, evidentemente davanti a una macchina con cui ha familiarità anche ove non fosse stata sua, in una scena informale e non preparata.

Gli Stones erano già un nome, tipo nel 1978, certo potevano scegliere a che cosa dedicarsi nel tempo libero.

È suggestivo immaginarsi il bassista di una delle band più influenti della storia inserire un floppy nel lettore e digitare PR#6. Già allora era qualcosa che finiva per distinguerti.