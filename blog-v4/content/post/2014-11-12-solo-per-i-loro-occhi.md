---
title: "Solo per i loro occhi"
date: 2014-11-12
comments: true
tags: [BusinessInsider, GKTank, GitHub]
---
Bel lavoro di *Business Insider*, che vale un mezzo scoop: una [descrizione di *app* usate dai dipendenti Apple internamente all’azienda](http://www.businessinsider.com/apps-only-apple-employees-get-to-use-2014-11), che non compaiono su App Store e vanno dal pragmatico allo sperimentale.<!--more-->

In un caso – giochino *multiplayer* pensato per fornire un esempio ai programmatori alle prese con problemi analoghi – esiste persino il [codice sorgente su GitHub](https://github.com/sdrazin/GKTank), liberamente migliorabile.

L’elenco è curioso a sufficienza da meritare la visita. Mi permetto di segnalare la *app* che fotografa gli scontrini dei dipendenti e automatizza il calcolo delle spese da rimborsare; in tante aziende farebbe comodissimo e non pare nemmeno una cosa pazzesca da sviluppare, per un reparto di programmatori professionisti.