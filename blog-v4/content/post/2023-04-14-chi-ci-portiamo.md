---
title: "Chi ci portiamo"
date: 2023-04-14T15:59:40+01:00
draft: false
toc: false
comments: true
categories: [Internet, Web]
tags: [Hardware Upgrade, Warren Buffett, iPhone]
---
Più che la cattiva educazione o l’aggressività latente, mi preoccupa dei commenti su Internet il livello di ignoranza che lasciano trasparire. Lo strumento in sé, lo sappiamo, è uno strumento ed è notorio che, in quanto presente sulla rete, verrà abusato da qualcuno. Il punto è la base culturale di chi ne abusa.

Su *Hardware Upgrade* è stata [ripresa una dichiarazione di Warren Buffett](https://www.hwupgrade.it/news/apple/warren-buffett-se-qualcuno-ti-offrisse-10-mila-dollari-per-non-comprare-mai-piu-un-iphone-non-accetteresti_115853.html#commenti), questa:

>Se qualcuno ti offrisse 10 mila dollari per non comprare mai più un iPhone, non accetteresti.

Contestualizziamo. Buffett è l’Investitore massimo, probabilmente il più competente al mondo a parte l’elemento di casualità che sta dietro anche a rendite immense come la sua. Da anni è noto per avere investito montagne di denaro su Apple e di averlo lasciato sempre lì, piovesse o splendesse il sole.

Il suo comportamento rispetto alle azioni Apple – si noti, è l’investitore di maggiore successo al mondo e ha più di novant’anni, un profilo diverso da quello di un giovane adulto che ha azzeccato una mano fortunata – è sempre stato controcorrente: il titolo ha sempre ricevuto meno attenzioni di quelle che suggerirebbe la teoria e ed è stato spesso sminuito, anche da esperti, in coincidenza con cali o notizie sfavorevoli. Se Buffett basasse i propri investimenti su Apple in base a quello che può leggere sui media, non avrebbe collocato miliardi di dollari su quelle azioni. E invece. Tradotto: sa quello che fa, anche quando sbaglia.

La sua dichiarazione è quella di una persona con idee chiarissime sulla definizione di valore, di ritorno dell’investimento nel tempo, di come l’uso del denaro sia da analizzare anche sulla lunga distanza. Siccome è abituato a pensare in questi termini, li usa per descrivere con parole sue il perché del successo di Apple: la qualità percepita da chi usa i suoi prodotti è largamente superiore al prezzo pagato e frutta ad Apple una fedeltà da parte del proprio pubblico che nessun’altra azienda può vantare.

Tutto chiaro, tutto ragionevole e tutto bilanciato: la quantità di diecimila dollari non è un numero buttato a caso. Se fossero stati centomila, avremmo tentennato. Se fosse stato un milione, considereremmo la proposta senza troppi patemi. A dieci milioni saremmo in fila per riscuotere.

Buffett ha affermato che il vantaggio percepito da chi usa iPhone, spalmato su una vita, vale ben più di diecimila dollari. Un ragionamento lucido può solo confermare.

Escludiamo dai commenti i bastian contrari, quelli che *faccio le stesse cose con un Android da duecentocinquanta euro*, i *dovrebbero pagarmi per toccare un iPhone*, tutte reazioni già viste e spiegate. Togliamo i sarcastici di mestiere, quelli che devono comunque fare la battuta, quelli che commentano per sublimare le frustrazioni e lo stress. Rimangono persone con un senso della realtà preoccupante, con doti di comprensione che inquietano, capaci di numerare e ragionare alla pari con un’avocetta.

>Persona che possiede il 6% di Apple dichiara di credere fortemente in Apple. Capirai che notizia.

Uno che capovolge causa ed effetto. Buffett non crede in Apple perché ha azioni. Le compra perché crede in Apple. (Non ci crede affatto, valuta la bontà di un investimento, ma ci siamo capiti).

>Apple è sicuramente un investimento sicuro, ma è cresciuta enormemente negli ultimi anni. Quanto potrà mai crescere ancora? Il mercato mi pare già piuttosto saturo.

La confusione tra la crescita dell’azienda e la crescita del titolo Apple. La percezione del mercato come se lui fosse il Primo Analista. L’ignoranza dei dati effettivi, propedeutica a una affermazione che si sarebbe potuta verificare in anticipo.

>Nessuno che giudichi primitiva una società basata sul denaro e la proprietà privata?

Sembrerebbe un bastian contrario deciso a buttarla in politica a qualsiasi costo. Invece è uno sprovveduto. Benissimo contestare il denaro e la proprietà privata; le società primitive, però, non avevano l’uno né l’altra. Come minimo, l’aggettivo *primitiva* è profondamente sbagliato.

Queste persone hanno debiti seri, di cultura, istruzione, ragionamento. Ma votano, contribuiscono al prodotto interno lordo, lavorano, crescono figli. Non si tratta di opinioni, qui, ma di deficit intellettuale. Buffett ha spiegato una situazione fattuale, ha descritto un fenomeno materiale presente e innegabile, che agisce su una scala di decine di milioni di persone. Non ha detto la sua.

Come si allestisce una scuola che produca persone formate, capaci di intendere un ragionamento, di maneggiare numeri, comprendere un concetto? Come si costruisce una società dove l’intelligenza (letteralmente, la comprensione) del mondo sia un titolo di merito effettivo e un incoraggiamento per gli altri? In che modo promuovere la superiorità del pensiero sopra il riflesso della parte rettile del cervello?

Per come la vedo io (innegabilmente una opinione, a differenza del resto) questi interrogativi stanno guadagnando centralità e criticità. Non è solo chi siamo e dove andiamo, ma anche chi ci portiamo dietro.