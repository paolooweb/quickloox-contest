---
title: "Il cappello sulle ventitré"
date: 2021-09-21T00:23:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [MacStories, iPad, iPadOS 15, iOS 15, M1] 
---
Aspetto come di consueto che siano iPad e iPhone a dirmi di aggiornare a iPadOS 15 e iOS 15, usciti all’improvviso ieri.

All’improvviso significa però che devo sospendere l’agenda notturna prevista, per affrontare la [recensione epocale](https://www.macstories.net/stories/ios-and-ipados-15-the-macstories-review/) creata come è abitudine di questi anni da Federico Viticci di MacStories. Sulla Terra non esiste un documento altrettanto tempestivo, approfondito e basato sull’esperienza pratica, che permetta di formulare un giudizio veramente informato sulla nuova versione dei due sistemi operativi.

Per ora ho solo dato una sfogliata, ma ho trovato una frase promettente:

>iPadOS continua a sembrare sull’orlo del precipizio di qualcosa di più grande.

(Tradotto; Federico scrive in inglese).

Posso anticipare che la raccomandazione è adottare, per meriti acquisiti, i nuovi sistemi, consapevoli che le migliorie di quest’anno hanno un raggio di azione e di impatto, ancorché importante, meno decisivo rispetto ad altre versioni precedenti. Si sente ancora in particolare su iPad uno sbilanciamento tra hardware eccezionale, basato su M1, pronto a consentire grandi cose e software che, seppure ottimo, ancora non è riuscito a staccarsi completamente dalla sua radice di *iPhone più grosso* per abbracciare completamente la realtà di tutto quanto è possibile fare sullo schermo di iPad tra penna, tastiera, touch, vocazione di tavoletta ma funzioni da computer, però non ancora mature eccetera.

In definitiva, lettura ampiamente consigliata, alla prima esplorazione superficiale. Sono ventitré paginate; roba impegnativa ma di assoluta attendibilità e autorevolezza. Federico si conferma uno dei massimi guru di iPad e quando si cimenta in questo sforzo, davvero va fatto tanto di cappello e va colta l’occasione.

*Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile.*