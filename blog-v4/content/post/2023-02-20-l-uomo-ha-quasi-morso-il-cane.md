---
title: "L’uomo ha quasi morso il cane"
date: 2023-02-20T18:04:20+01:00
draft: false
toc: false
comments: true
categories: [Software, Internet]
tags: [Go, KataGo, TechSpot, Kellin Pelrine, Pelrine, FAR AI]
---
Vecchio adagio: il cane che morde l’uomo passa inosservato, mentre l’uomo che morde il cane fa notizia.

Siamo così a parlare di Kellin Perline, dilettante di spicco nel [Go](https://www.cosumi.net/en/), che [ha sconfitto l’ennesima sedicente intelligenza artificiale ad applicarvisi](https://www.techspot.com/news/97663-human-go-player-thoroughly-beats-ai-after-computer.html).

La vittima artificiale, [KataGO](https://github.com/lightvector/KataGo), non è [frutto della DeepMind di Google](https://macintelligence.org/posts/2021-06-23-compagni-di-gioco-e-no/) come AlphaGo e poi AlphaZero, ma viene data come equivalente in fatto di potenza di gioco.

C’è un distinguo da fare: Pelrine si è appoggiato a *un’altra* sedicente intelligenza, che ha giocato tipo un milione di partite contro KataGO e ne ha scoperto alcuni punti deboli. L’umano disponeva di sufficiente competenza nel gioco per mettere in atto le trappole che, sapeva, avrebbero favorito la sconfitta della macchina. E ha raggiunto il risultato. Sia pure con l’aiuto di armi non convenzionali, l’uomo ha morso il cane. Quasi, via.

Dopo avere cominciato con una frase fatta, eccone un’altra: *se non puoi batterli, unisciti a loro*.

Viene anche un altro pensiero, però: ad AlphaZero sarebbe mai venuto in mente di appoggiarsi a un umano per trovare i punti deboli di un altro umano? O, perlomeno, sarebbe mai ricorso a un’altra macchina software come lei?

Un’*intelligenza* incapace di vedere oltre la propria scatola, non lo è molto.