---
title: "La caduta del prezzo"
date: 2014-03-15
comments: true
tags: [Google, Drive, cloud]
---
Seguo pochissimo le guerre dei prezzi ma devo fare un’eccezione per Google Drive, che ha abbassato la tariffa dei cento gigabyte di spazio disco su Internet – ora è di moda dire cloud – a [1,99 dollari al mese](https://www.google.com/settings/storage?hl=it).<!--more-->

Al cambio attuale, sono 1,43 euro. In un anno, 17,16 euro. Per cento gigabyte. A Milano, se aggiungi il caffè e pretendi pure la birra, non ci esce una pizza.

Forse la discesa dalla soglia storica dei 4,99 dollari/mese potrebbe significare molto in termini di numero di adesioni. Se continua il *trend*, da qui a qualche anno verrà da mettere in discussione il fatto stesso di tenersi dischi in casa, a parte quello di lavoro, con pochissime eccezioni (montaggi audio e video vari, per esempio).