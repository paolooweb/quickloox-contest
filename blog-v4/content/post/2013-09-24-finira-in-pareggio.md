---
title: "Finirà in pareggio"
date: 2013-09-24
comments: true
tags: [iPhone, Android]
---
Tutti a guardare il processore, la fotocamera, la risoluzione, la diagonale. Quanto è veloce, invece, lo schermo? Sembra futilità, ma il tocco è il cuore dell’interfaccia.<!--more-->

I [test](http://www.agawi.io) di Agawi [dicono](http://appglimpse.com/blog/touchmarks-i-smart-phone-touch-screen-latencies/) che iPhone 5 è due volte e mezzo più veloce nella risposta dello schermo rispetto a un Android. Pensare ai giochi, alla scrittura, alla precisione che di pretende quando si sta facendo qualcosa di essenziale. 55 millisecondi di risposta per iPhone 5, 85 millisecondi per iPhone 4, 114 millisecondi Galaxy S4 e gli altri fanno peggio.

Lo schermo è veloce a rispondere. E invece il processore, la misura più tradizionale? Macs Future ha effettuato un test che mette a confronto iPhone 5 e iPhone 5S. Il secondo elabora [circa il doppio](http://macsfuture.com/post/61891960935/iphone-5s-head-to-head-speed-test-with-iphone-5) delle informazioni del primo, quanto ha promesso Apple durante il *keynote* di presentazione.

La cosa impressionante, veramente impressionante, è che un iPhone 5S sfiora i risultati di velocità di un Mac mini del 2010. Vale a dire che lavora quasi come il mio vecchio MacBook Pro del 2009, con il quale sto scrivendo.

Pochi anni e, se continua il trend, avremo in mano un oggetto di potenza pari a quella del computer sulla scrivania. Se non superiore.