---
title: "Advert different"
date: 2020-10-07
comments: true
tags: [Facebook, Apple, iOS, Safari, Fischer]
---
Man mano che si avvicina il momento in cui iOS offrirà agli utilizzatori il [controllo sul loro tracciamento da parte delle app a scopi pubblicitari](https://macintelligence.org/posts/2020/08/29/lo-snobismo-dello-scripter/), tema nascosto e immenso che [dovrebbe ricevere molta più attenzione](https://macintelligence.org/posts/2020/09/23/tracce-da-seguire/), aumenta il fuoco avverso.

Il Chief Revenue Officer (attenzione alla carica, dice molto) David Fischer di Facebook ha dichiarato che i modelli di business basati sulla pubblicità personalizzata [sono sotto assalto](https://www.cnbc.com/2020/10/06/facebook-revenue-chief-says-ad-supported-model-is-under-assault-.html) da parte di Apple. E fin qui, ognuno tira l’acqua al suo mulino. Ma poi:

>Esistono tanti modelli di business. Apple ne ha uno basato sulla vendita di hardware di lusso o abbonamento a servizi, principalmente a consumatori abbastanza fortunati da poter disporre liberamente di molto reddito, in alcune delle nazioni più ricche del mondo.

Certo mi sento fortunato, ma per altre cose, tipo la sanità e l’acqua corrente. Se hai una base di un miliardo di utilizzatori, sicuramente sono persone che possono concedersi un computer e quindi hanno da mangiare e da coprirsi; chiamarlo *lusso* (*luxury*) come se fosse Armani, mi sembra tanto.

>[Il modello di business di Apple] mi sta bene, ma non credo sia appropriato derivarne il dettare la linea ad altri modelli di business.

E qui suona strano. L’idea è che un modello di business non dovrebbe condizionarne altri. Esattamente quello che Fischer vuole fare nel dire che Apple dovrebbe accettare quello di Facebook.

La verità è che nel modello di business di Apple è prevista la privacy per chi ne usa le tecnologie. E Fischer qui dice la verità senza accorgersene: la privacy è un lusso, nel nuovo mondo. I poveri e i noncuranti, ricchi o meno, facciano senza, per fare guadagnare Facebook.

Facebook potrebbe avere un argomento più solido se iOS vietasse il tracciamento, *tout court*.

Invece iOS *passerà il controllo all’individuo*. Che sarà molto più libero di adesso nel rispondere sì o no. (La libertà totale non esiste; ma questa libertà aumenterà e molto).

Sai che cosa? si parla di pubblicità personalizzata. Se io ricevessi su Facebook pubblicità divertenti, coinvolgenti, interessanti, degne di attenzione, utili per la mia vita, risponderei sì. Dove sta il problema.

La questione si riduce a *la pubblicità attuale è a grandi linee uno schifo che farà rispondere no a un sacco di persone*.

Il tuo prodotto è pessimo? Allora sì, sul mio hardware *di lusso* non ce lo voglio. Riparliamone quando mi proporrai pubblicità di autentico valore. Dove *think different* vogliamo *advert different*. Senza pregiudizi né ostilità. Ben venga la buona pubblicità, così come i buoni computer da tasca.