---
title: "Tempo di uscite"
date: 2014-09-18
comments: true
tags: [Gruber, ArsTechnica, iPhone6, iPhone6Plus, DaringFireball, iOS8, iOS7, iOS, iPhone]
---
Per parlare con cognizione di causa di iOS 8, niente è meglio della accuratissima [recensione di Ars Technica](http://arstechnica.com/apple/2014/09/ios-8-thoroughly-reviewed/), mentre per iPhone 6 e iPhone 6 Plus il riferimento è assolutamente John Gruber di *Daring Fireball*, che li ha provati entrambi per una settimana e ha fissato sulla carta virtuale un [racconto dettagliato e meritevole](http://daringfireball.net/2014/09/the_iphones_6).<!--more-->

Così termina *Ars*, dopo non meno di undici pagine:

> Con questa versione, Apple cerca di portare aggiunte desiderate dagli utilizzatori evoluti e dai programmatori senza scontentare quanti arrivano a iOS specificamente per la sua coerenza e semplicità. Spiega che più o meno qualsiasi funzione primaria di iOS 8 può essere disabilitata o ignorata, e che grandi funzioni trasformative come le estensioni indipendenti sono nascoste per definizione alla vista. Un’occhiata superficiale a iOS 8 suggerisce un sistema operativo praticamente identico a iOS 7. Ma basta approfondire appena per scoprire quanto sia diverso.

Gruber invece ha quasi deciso che il suo prossimo iPhone sarà un 6 e non un 6 Plus, con tutta una serie di considerazioni interessanti sulle motivazioni degli schermi più grandi e su come iPhone 6 Plus finisca per porsi come anello di congiunzione tra la famiglia di iPhone e quella di iPad. La sua chiusura è la seguente:

>Apple continua a ripetere che gli iPhone 6 sono migliori sotto tutti gli aspetti e lo posso confermare. Più rifiniti e raffinati, si tengono meglio in mano, schermo migliore, prestazioni maggiorate, foto migliorate, video migliorato, più autonomia, Wi-Fi e Lte più veloci. Non saprei che cosa chiedere di più in tema di progressi a un anno di distanza da iPhone 5S, che rimane una macchina eccezionale. E ho citato a malapena iOS 8, che ritengo un miglioramento su iOS 7 quasi in ogni aspetto, con un forte accento sulla maggiore utilità senza indulgere in vanità non necessarie.

>Non sono ancora completamente convinto sui 4,7 pollici in sostituzione dei quattro pollici come dimensione standard di iPhone, ma datemi poche settimane e sospetto che lo diventerò. Mi piacciono così tanto le dimensioni dell’iPhone precedente e ci ho passato sopra talmente tanto tempo che per adeguarsi a una nuova misura servirà più di una settimana, specialmente dopo avere provato l’enormoso [*ginormous*] iPhone 6 Plus.

>La cosa più affascinante a proposito di iPhone 6 e iPhone 6 Plus è come sembri assolutamente ordinario il livello di miglioramento che Apple porta anno dopo anno dopo anno.

E c’è da riflettere su questo, perché sembra scontato.