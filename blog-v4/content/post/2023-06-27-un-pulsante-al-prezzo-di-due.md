---
title: "Un pulsante al prezzo di due"
date: 2023-06-27T12:31:28+01:00
draft: false
toc: false
comments: true
categories: [Hardware, Softwre, Internet]
tags: [A2, Filippo Strozzi, Roberto Marin Marin, Strozzi, Vision Pro, visionOS]
---
Il [podcast A2](https://a2podcast.fireside.fm) dedicato a Vision Pro andrà in onda il 10 luglio, dice [Filippo](https://a2podcast.fireside.fm/hosts/filippo) prima di montare la puntata e quindi del tutto sotto la propria responsabilità.

Abbiamo registrato per quasi due ore e, per quanto un podcast abbia il registro della chiacchierata più che quello dell’esame universitario, siamo stati sul pezzo; eppure abbiamo coperto sì e no la metà dei temi sul tappeto e nella scaletta. Come dire, abbiamo coperto la *digital crown* ma non il pulsante video e audio di Vision Pro, con annessi connessi e portati.

[Vision Pro](https://www.apple.com/apple-vision-pro/) è davvero un elemento di cambiamento e di cambiamento grosso. La diatriba sul doverlo indossare otto ore o dover lavorare con un visore sul volto è molto stupida: nessuno è tenuto a usare Vision Pro, quando ci sarà, né a usarlo sempre, né a tenerlo per otto ore. È una opportunità che ciascuno sceglierà se e come sfruttare.

Una opportunità importante, però. Non necessariamente di per sé, ma per i segnali che porta. Il computing spaziale non è una invenzione di Apple partorita durante una riunione con oggetto *troviamo il modo di fare altri soldi a spese degli sprovveduti*. A non tutti serve per forza, si vive anche senza; e per qualcuno invece sarà un passo in avanti.

Un oggetto appena fuori dalla fase prototipale, non ancora completato, senza una data di uscita, con un prezzo annunciato elevato; eppure sta cambiando le cose e noi ne abbiamo parlato per due ore senza esaurire il tema.

Questo nonostante l’avere lavorato bene; [Roberto](https://a2podcast.fireside.fm/hosts/roberto) e Filippo avevano le idee molto chiare. Io avevo solamente idee poche e confuse, come sempre, ma mettersi in tre è come centrifugare la nebbia mentale, ne esce un racconto [impagabile](https://macintelligence.org/posts/2023-05-19-artisti-liberali/) per me e ciò mi basta.

(Poi siamo stati un’ora *off record* a dipanare trame accennate e condividere un pezzo di vita e questo dovrebbe diventare un podcast suppletivo, ma è un altro discorso).

Come si conclude un post così? Non si conclude. Fosse per me, registrerei di nuovo stasera…