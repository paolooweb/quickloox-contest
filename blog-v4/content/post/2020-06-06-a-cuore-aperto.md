---
title: "A cuore aperto"
date: 2020-06-06
comments: true
tags: [Aaa, AllAboutApple, RNA, coronavirus]
---
Dieci anni fa sono stato davvero a cuore aperto per un po'. Non mi sono accorto di niente, ovvio. L'operazione è perfettamente riuscita e da allora funziona tutto al cento percento.

In questi ultimi tre mesi, un pezzo di cuore, stavolta inteso come amicizie, persone, talenti, è rimasto chiuso. Nessun dolore fisico ma tanta frustrazione, perché il virus non fa distinzioni e invece dovrebbe, persino se ha l'intelligenza di un gomitolo di RNA.

Finalmente [All About Apple ha riaperto](https://www.allaboutapple.com/2020/06/dal-6-giugno-riapriamo-al-pubblico/). Li abbraccio perché come amici sono un dono e invito a donare, perché lo meritano.