---
title: "Tecniche di disinformazione"
date: 2010-12-10
comments: true
tags: [Unix, Linux, Windows, Mac, editor, testo, JEdit, BBEdit, Fraise]
---
Facciamo finta di scrivere un articolo sui migliori cinque editor di testo.<!--more-->

Due candidati vengono dal mondo Unix, richiedono conoscenze approfondite, vanno lanciati dal Terminale e pochissimi li hanno usati seriamente.

Uno arriva dal mondo Linux, all&#8217;incirca un computer su cento.

Uno &#232; solo per Mac e costa 54 dollari.

L&#8217;ultimo &#232; per Windows, gratis.

Al termine dell&#8217;<a href="http://lifehacker.com/5706475/five-best-text-editors" target="_blank">articolo</a> si invita a votare per il miglior editor. Quale potr&#224; mai essere il vincitore?

Si noti che mancano all&#8217;appello <a href="http://www.bbedit.com" target="_blank">BBEdit</a>, <a href="http://jedit.org" target="_blank">JEdit</a>, <a href="http://www.fraiseapp.com/" target="_blank">Fraise</a>&#8230; pi&#249; gran parte di questo <a href="http://en.wikipedia.org/wiki/List_of_text_editors" target="_blank">piccolo elenco</a> reperibile su Wikipedia.

La verit&#224; &#232; che l&#8217;articolo &#232; stato costruito per &#8220;spingere&#8221; un editor specifico, mettendo gli altri per dare l&#8217;apparenza di una scelta equilibrata.
