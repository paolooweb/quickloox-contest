---
title: "Giochi senza tastiere"
date: 2023-08-06T01:42:45+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [iBrogueCE, iNethack2, Remote Dwarf Fortress, Angband, roguelike]
---
È un sacco di tempo che manco dal settore dei *roguelike* e, con agosto, si potrebbe impostare qualche spedizione epica con poche o nessuna speranza di successo dentro un labirinto pieno di mostri. È un bel modo di sfruttare la quiete delle notti estive ed esorcizzare la routine che attende implacabile l’accorciarsi delle giornate Dino alla ripresa delle scuole.

I requisiti sono la giocabilità su iPad dato che, a differenza del mese scorso, ho lasciato a casa Mac.

Il candidato più probabile è [iBrogueCE](https://apps.apple.com/it/app/ibroguece/id1619142059?l=en-GB), ritornato su iPad nella forma di Community Edition dopo lunga assenza. Letalità altissima, vastità minima.

L’alternativa è iNethack2 [iNethack2](https://apps.apple.com/it/app/inethack2/id962114968?l=en-GB); letalità alta, vastità notevole.

Grazie ai buoni uffici del Mac rimasto a casa potrei cimentarmi con [Remote Dwarf Fortress](https://apps.apple.com/it/app/dwarf-fortress-remote/id1003660287?l=en-GB); letalità alta, vastità clamorosa. Più che un roguelike andrebbe classificato mondo virtuale.

Mi fermo a giochi ampiamente noti anche fuori dall’universo di iPadOS e, anzi, da provare prima di tutto su Mac nelle loro versioni native. Niente [Angband](https://rephial.org) perché [una ascensione mi è riuscita](https://macintelligence.org/posts/2011-12-18-la-festa-dellascensione/) e vorrei aggiungere un trofeo nuovo invece che accumulare repliche estive. Probabilmente la scelta cadrà su Brogue, che è cruento al massimo ma potenzialmente veloce, più che altro in quanto si finisce spesso mangiati presto e male. In una estate è facile mettere a verbale molti tentativi.

Il bel paradosso è che i roguelike si basano storicamente sulla tastiera. Giocarli su iPad con una interfaccia touch fatta apposta è un piacere doppio (la tastiera è con me, solo che il tempo di gioco è tipicamente notturno e a letto).

Accetto auguri di buona perseveranza.

*Gli aggiornamenti in agosto del blog potrebbero essere irregolari o infrequenti.*