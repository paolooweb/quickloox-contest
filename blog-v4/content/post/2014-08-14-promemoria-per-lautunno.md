---
title: "Promemoria per l'autunno"
date: 2014-08-15
comments: true
tags: [Colligan, Palm, Treo, iPhone, Apple, BlackBerry, Razr, Treo, Malda, Gruber]
---
È possibile che prima di Natale Apple annunci prodotti o linee di prodotto inedite e a questo scopo riporto alcuni estratti da un [*post* di John Gruber del 2006](http://daringfireball.net/2006/11/colligan_head_stuck). Quasi un anno prima che arrivasse iPhone.<!--more-->

>La parte che preferisco è quando Colligan [allora amministratore delegato di Palm] parla di “anni di fatiche a capire come costruire un telefono decente”. Non è possibile che, *se* Apple sta entrando nel mercato della telefonia mobile, anche lei stia faticando da anni sul problema.

>Se [Colligan] crede veramente a quello che dice, probabilmente non ha idea di come Apple si avvicinerebbe al mercato. Un telefono di Apple non farebbe *più* di un Treo o un BlackBerry o un Razr, ma farebbe *meno* e quel meno lo farebbe davvero bene.

>Così come il fondatore di Slashdot Rob Malda stroncò il primo iPod – “niente wireless. Meno spazio di un Nomad. Scarso” – un cellulare Apple sarà sicuramente accolto dagli appassionati come clamorosamente sottodimensionato. *Gli mancano questo e quell’altro! È solo bello da vedere!*

>Nel frattempo, la coda davanti ad Apple Store sarà arrivata fino alla zona cibo del centro commerciale. *Se* ne fanno uno.

Buon Ferragosto!