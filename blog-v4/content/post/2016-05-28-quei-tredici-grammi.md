---
title: "Quei tredici grammi"
date: 2016-05-28
comments: true
tags: [MacBook, Asus, ZenBook]
---
Conforta vedere che cambiano i secoli, le piattaforme, gli equilibri, le tecnologie, i trend tecnologici, ma il modo di preparare una recensione cialtrona e imbecille lo si trova comunque.

Esempio pronto in tavola quello di *Engadget* che presenta lo ZenBook 3 di Asus: [più sottile, leggero e veloce di MacBook](http://www.engadget.com/2016/05/30/asus-zenbook-3/).

Che roba, eh? Un altro mito che crolla. Poi uno legge le differenze: 13,61 grammi. 1,3 millimetri. Sfido qualunque essere umano bendato a percepire la differenza. Sulla velocità, il processore è superiore e vabbeh, come se fossero computer, quelli alti un centimetro, il cui compito è lavorare di fatica.

Il loro compito è lavorare leggeri e senza problemi e qui, dopo paragrafi e paragrafi di chiacchiere, salta fuori che la durata dichiarata della batteria è di nove ore. MacBook fa decisamente meglio e un umano, quando la batteria muore prima del previsto, se ne accorge. E poi, la tastiera:

>Sfortunatamente, la tastiera di ZenBook 3 si avverte come incredibilmente piatta, al punto che non riesco a immaginare di usarla per scriverci gran che. È particolarmente strano, dal momento che Asus tiene a dire di avere tasti con più escursione di quelli di MacBook. Da quello che ricordo, la tastiera di MacBook semplicemente è migliore.

Certo, ti compri un computer ultrasottile e ultraleggero e poi non riesci a scriverci sopra. Però, quei tredici grammi danno il gusto di avere battuto la concorrenza e allora si sopporta tutto.