---
title: "Prova calore"
date: 2021-11-23T01:04:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [MacBook Pro, M1 Max, Intel, i7] 
---
Il colore è calore, il calore è colore. Un MacBook Pro M1 Max proprio non è un MacBook Pro con i7 di Intel.

Messi uno accanto all’altro, a lavorare sul medesimo compito, [si distinguono anche al buio](https://twitter.com/_MG_/status/1462651495124373506), grazie alle diverse temperature operative. 

<blockquote class="twitter-tweet"><p lang="en" dir="ltr">Intel i7 MacBook Pro next to a M1 Pro Max MacBook Pro.<br><br>Both have been under the same light workload for the last hour. <a href="https://t.co/Tlvs8adhwn">https://t.co/Tlvs8adhwn</a> <a href="https://t.co/IwBtF59VDU">pic.twitter.com/IwBtF59VDU</a></p>&mdash; _MG_ (@_MG_) <a href="https://twitter.com/_MG_/status/1462651495124373506?ref_src=twsrc%5Etfw">November 22, 2021</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

La cosa folle è che, al posto del MacBook Pro Intel, potrebbe esserci _qualunque_ portatile Pc di potenza comparabile, e farebbe la stessa figura.

Qualunque. È impressionante.

Nel frattempo, MacBook Pro con M1 Max va pure più veloce.

_Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile._