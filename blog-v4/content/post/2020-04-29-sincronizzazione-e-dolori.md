---
title: "Sincronizzazione e dolori"
date: 2020-04-29
comments: true
tags: [Dropbox, Editorial, Zorn, ssh, Mac, Octopress, emacs]
---
Diversi post negli ultimi giorni sono stati pubblicati monchi delle ultime parole o delle ultime righe. Mi sono scervellato e ho capito almeno in parte che succede.

Quando scrivo su iPad uso [Editorial](http://omz-software.com/editorial/), che sincronizza su Dropbox. Per me è estremamente comodo poi collegarmi a Mac in ssh, spostare il file del post nella cartella giusta, azionare [Octopress](http://octopress.org) e pubblicare.

Da qualche tempo, per motivi che ancora ignoro, la sincronizzazione con Dropbox si inceppa spesso e volentieri. Succede che, mentre scrivo, il file cambia nome e diventa su Dropbox una copia in conflitto con quella originale.

Al momento di collegarmi a Mac non ci faccio caso e passo a Octopress il file che ha il nome originale. In realtà non è il file completo, ma quello che contiene il testo fino a quando non è fallita la sincronizzazione.

Ora verifico manualmente che tutto sia a posto una volta pubblicato. Vorrei cambiare modalità di pubblicazione ma mi servirebbe un editor capace di sincronizzare, che so, con iCloud Drive per esempio, comunque non soltanto con Dropbox.

D’altro canto Editorial, con il suo meccanismo di scripting interno, mi ha consentito un livello di personalizzazione che non so quale altro programma mi consenta. Produco ogni giorno svariati file in Html e Markdown, che Editorial mi fa costruire in modo comodissimo. Ho la sensazione che pochissimi editor mi consentano la stessa libertà.

Mi sto abituando gradualmente a emacs, solo che su iPad è ancora poco pratico e poi, quelle rare volte che non sono a contatto con un Mac, dovrei usare un editor offline diverso. Insomma, c’è dietro tanta pigrizia oltre a qualche mancanza di tempo, ma la morale è che mi manca un *upgrade path* efficace per superare l’impasse dei problemi con Dropbox e intanto mantenere la notevole efficienza che ho raggiunto con la personalizzazione di Editorial.

Accetto suggerimenti e rimproveri.