---
title: "Grafici che parlano"
date: 2014-07-27
comments: true
tags: [BeyondDevices, iPad, Apple]
---
La cosa da guardare sull’[articolo di Beyond Devices](http://www.beyonddevic.es/2014/07/22/why-ipad-shipments-arent-growing-but-might-start-again-soon/) dedicato a iPad sono i grafici, semplici e rivelatori.<!--more-->

Ce n’è uno sulla base installata di iPad, risposta facile per chi sostiene che la piattaforma non stia funzionando per via dei cali di vendita anno su anno, con l’età dell’installato e la percentuale di aggiornamento della stessa a contorno.

E poi l’[articolo sugli ultimi risultati finanziari](http://www.beyonddevic.es/2014/07/23/thoughts-on-apples-earnings-for-q2-2014/). Chiarissimi l’andamento della ricerca e sviluppo e soprattutto l’andamento ciclico dei margini in rapporto al fatturato.

Datemi più grafici e meno frasi a effetto. Mi rilasso.
