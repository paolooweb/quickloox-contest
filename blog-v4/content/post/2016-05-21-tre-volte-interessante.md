---
title: "Tre volte interessante"
date: 2016-05-21
comments: true
tags: [Apple, iTunes, Music]
---
A proposito del problema di quanti [hanno perso musica a causa di Apple Music](https://macintelligence.org/posts/2016-05-15-la-posizione-del-problema/) o iTunes o ambedue, uno sviluppo doppiamente interessante è che Apple [ha mandato due ingegneri a casa](https://blog.vellumatlanta.com/2016/05/17/apple-sent-two-men-to-my-house-no-they-werent-assassins/) di colui che ha reso pubblico il problema.<!--more-->

Doppiamente interessante perché, dopo avere speso la parte migliore di un weekend tra diagnostica ed esperimenti, non è emerso alcun risultato degno di nota. E perché non tutte le aziende, soprattutto sedute su centinaia di miliardi di dollari di cassa, si impegnano fino a questo livello per risolvere un problema.

Il quale peraltro rimane elusivo e insoluto. Il che lo rende forse triplamente(?) interessante.