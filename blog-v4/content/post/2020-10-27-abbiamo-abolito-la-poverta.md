---
title: "Abbiamo abolito la povertà"
date: 2020-10-27
comments: true
tags: [iPhone, Avalon]
---
*Above Avalon* scrive che *secondo le nostre stime, il mese scorso Apple ha superato la boa del [miliardo di utilizzatori di iPhone](https://www.aboveavalon.com/notes/2020/10/26/a-billion-iphone-users)*.

Se leggo i commenti a seguito di una presentazione di nuovi iPhone, non mancano mai le battute sul rene da vendere, meglio pagarsi un viaggio, fai le stesse cose a metà prezzo eccetera.

Non sono ricco e ho un iPhone. Certamente sono fortunato, molto fortunato, in più di un modo: in prospettiva planetaria, so leggere e scrivere, ho l’acqua corrente, un tetto solido, accesso a cure mediche e a servizi sociali di qualità ragionevole quando non buona. Se prendo in considerazione i sette miliardi che siamo, questa è fortuna. Grande fortuna.

Però non mi si faccia passare per privilegiato, ad avere un iPhone. Se siamo un miliardo su sette miliardi, siamo fortunati di sicuro. Non possiamo però essere ricchi o privilegiati.
