---
title: "Trenta per sette centocinquanta"
date: 2014-01-12
comments: true
tags: [Hal9000, Mac, iOS]
---
Un omaggio al [calcolatore personale che ha cambiato la storia](http://apple-history.com/128k), prossimo ai trent'anni, e al [calcolatore da tasca](http://www.ifixit.com/Teardown/iPhone+1st+Generation+Teardown/599) annunciato per la prima volta sette anni or sono, dopo di che  cambiato tutto.<!--more-->

Giusto oggi ricorrerebbe il compleanno del computer pi intelligente di ogni tempo, senziente al punto da dubitare e assassinare perfino, se fosse esistito davvero e avesse recitato realmente un'ultima filastrocca prima di spegnersi.

In onore di tutti e tre, un [salvaschermo](http://www.halproject.com/hal/) - sito in Flash purtroppo  e una [app celebrativa](https://itunes.apple.com/en/app/hal-9000/id304243321?mt=8). In attesa di una missione spaziale umana verso Giove.