---
title: "Cervelli all’ammasso"
date: 2022-04-20T01:46:30+01:00
draft: false
toc: false
comments: true
categories: [Hardware]
tags: [Lenovo]
---
Il solo fatto di poter leggere una lista di oltre cento modelli Lenovo che all’altroieri, e tuttora in mancanza di aggiornamento firmware, [sono a rischio di venire infettati da malware potenzialmente ineliminabile](https://arstechnica.com/information-technology/2022/04/bugs-in-100-lenovo-models-fixed-to-prevent-unremovable-infections/), fa rivalutare il movimento no vax.

Cervelli all’ammasso, che però se non altro credono in un qualche complotto fantascientifico, invece che di risparmiare.