---
title: "La paura è lenta"
date: 2020-10-11
comments: true
tags: [watch, Family, Setup]
---
Leggo surreali scambi di pareri di genitori terrorizzati dall’idea che la scuola chieda al figlio dodicenne di usare, tra gli altri materiali didattici, il *tablet*. (Il nome in gergo serve a trasmettere istantaneamente l’ostilità verso l’oggetto alieno). Per questioni economiche? Macché. Per il timore che faccia brutti incontri in rete, o che passi la giornata con gli amici su WhatsApp, o che vada in *iperconnessione* per citare un bravissimo psicologo, e scarsissimo tecnologo, molto *à la page* negli ambienti più ricchi di analfabeti tecnologici.

L’idea dominante è che convenga mettere la testa in un buco e aspettare, aspettare, aspettare, nella speranza che a un certo punto il figlio sia magicamente in grado di prendere in mano l’oggetto senza farsi male. Certamente non per merito dell’esperienza.

L’amara verità è che il figlio non verrà educato, né accompagnato, né supportato nell’adozione della tecnologia e saranno guai, o frustrazione. Ancora più amara è un’altra verità: per tante famiglie *Internet* significa *Facebook e WhatsApp*. Come se *alimentazione* volesse dire *bomboloni e patatine fritte*, o adolescenza significasse *masturbazione* e *cattive compagnie*. Sono tutti discorsi, come dire, appena più complessi.

Ma il punto è diverso. Mi sovviene dell’[evento Apple di presentazione di watch Series 6](https://www.apple.com/apple-events/september-2020/) e della funzione di Family Setup: si usa un singolo iPhone per abilitare più watch, che possono andare per esempio a un figlio per sapere se è davvero a scuola, consentirgli chiamate ai soli contatti autorizzati, naturalmente comunicare con la famiglia.

Che sia un bene o un male potrà essere discusso all’infinito. È che i genitori sono divorati dal conflitto tra la pressione sociale di dare un cellulare ai propri figli e il timore panico che venga usato male (in molte famiglie, visto chi darà l’esempio, finirà senza dubbio così).

Ecco. Del cellulare al figlio, intanto che se ne fa una questione di sicurezza nazionale, *non c’è bisogno*. Basta l’orologio. A dire che le paure si materializzano quando la tecnologia è già scappata avanti da un pezzo.