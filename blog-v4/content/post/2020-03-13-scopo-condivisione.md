---
title: "Scopo condivisione"
date: 2020-03-13
comments: true
tags: [Valarea, Re, Mago]
---
Delle tante iniziative di [Solidarietà digitale](https://solidarietadigitale.agid.gov.it/) voglio sottolineare quella di [Valarea](https://www.valarea.com), che per tutto il periodo dell’emergenza aprirà le funzionalità premium della app gratuitamente a chiunque si registri.

In questi giorni sto facendo il giro del mondo dei sistemi di conferenza a distanza, audio, video, gesti, intenzioni, dire fare baciare. Per una volta sono d’accordo con [Vice](https://www.vice.com/en_ca/article/y3mxyw/why-conference-call-technology-never-works):

>La tecnologia della videoconferenza ha avuto bisogno di un sacco di innovazione per arrivare alla mediocrità di massa.

Il peggio del peggio è Skype. A un certo punto è caduta la linea, come quarant’anni fa con la Sip in teleselezione. Il bello è che mi hanno richiamato e Skype si è rifiutato di rispondere. Avevo tre apparecchi diversi in attesa di accettare la chiamata e niente. Per lavorare uno fa di tutto, intendiamoci, ma Skype è umiliante.

FaceTime funziona bene. FaceTime Audio è un ordine di grandezza meglio di una chiamata cellulare e oramai capita spesso che ci si parli da iPhone a iPhone, non capisco veramente perché la gente scelga di ascoltare male. Però poi ho dovuto usare Messaggi per una condivisione schermo e non ci siamo riusciti.

Mentre attendevo la (ri)chiamata su Skype è arrivato il momento di un’altra videoconferenza, su Google Meet. Questa è partita perfettamente (non era questione di linea, né di hardware, ma proprio di Skype) e in onestà ha funzionato. Lo speaker del momento è sparito a volte per un secondo o due ma è sempre ritornato e la conferenza complessiva è rimasta in piedi. Usabile.

Discord è fatto bene e offre qualità buona. L’unico problema è che ha la fama di app per i videogiocatori. Prova a proporlo a gente che usa Word anche per lavarsi i denti; impossibile.

Valarea non è un grande nome eppure ho avuto occasione di provarlo in via riservata e merita più esposizione di quella che ha. È una app in via di veloce maturazione; non è ancora perfetta e però, come soluzione completa di [conferenza e collaborazione insieme](https://www.youtube.com/watch?v=eHffMdn56iQ&t=73s), è già alcuni passi avanti a molta concorrenza blasonata. La collaborazione non è solo banale condivisione dello schermo o interscambio di file, come in altre parrocchie, ma c’è la reale possibilità di creare informazione collettivamente in tempo reale. E registrarla.

Una delle direzioni di sviluppo in cui deve crescere è il supporto nativo di Mac, dove va usato Chrome (Safari funziona, ma è assai più lento). Su iOS invece c’è una app come si deve e tutto è svelto e pratico.

Se mi capiterà di promuovere una prossima sessione di collaborazione a distanza proporrò sicuramente Valarea; di quello che ho provato questa settimana, è l’unico sistema che non mi è andato stretto.

<iframe width="560" height="315" src="https://www.youtube.com/embed/eHffMdn56iQ" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>