---
title: "Tutto o niente"
date: 2013-10-23
comments: true
tags: [OSX]
---
La cosa giusta da leggere (a parte l’[autopubblicità](https://macintelligence.org/posts/2013-10-17-fatica-e-soddisfazione/) naturalmente) è la solita monumentale [recensione di Mavericks](http://arstechnica.com/apple/2013/10/os-x-10-9/) da parte di John Siracusa di *Ars Technica*.<!--more-->

Chi non se la sente proprio, converga sui [trucchi e segreti](http://www.macstories.net/roundups/os-x-mavericks-tips-tricks-and-details/) assemblati con perizia da Federico Viticci su MacStories.

Se ci sarà altro, lo vedremo. Per ora non c’è.

Non pensavo di chiedere Mavericks a Babbo Natale; eppure arriva in regalo. Non male.
