---
title: "Legati ambiguamente"
date: 2020-02-02
comments: true
tags: [JetBrains, Mono, Fira, Unicode]
---
Rimango favorevole ai [font monospaziati per la programmazione che applicano le legature al codice](https://macintelligence.org/posts/2020-01-24-un-font-mille-app/) per renderlo più leggibile.

Registro però [l’opposizione di pesi massimi della tipografia come Matthew Butterick](https://practicaltypography.com/ligatures-in-programming-fonts-hell-no.html).

L’obiezione è doppia. La prima parte è quella dove l’aspetto del codice dovuto al nuovo font può indurre in inganno un altro che legga, se un simbolo – per via di una variazione tipografica – può sembrarne un altro.

Qui è più che altro questione di gusti, credo. Il caso riguarda un altro programmatore che guardi quel codice sul *mio* computer senza porsi il problema del font. Direi che è una situazione molto particolare e un po’ cavillosa.

La seconda parte dell’obiezione è più preoccupante. Come faccio a distinguere una legatura da un carattere Unicode di uguale aspetto?

Una legatura equivale per esempio alla fusione elegante sullo schermo di due caratteri che, dietro lo schermo, sono due codici distinti. Un carattere Unicode è un solo codice.

Di conseguenza, afferma Butterick, qualsiasi applicazione di un font con legature a un codice abbastanza lungo darà luogo a qualche ambiguità pericolosa nel momento in cui quel codice verrà messo al lavoro.

La parte in cui si dice che la legatura non rispecchia necessariamente il significato semantico dei due caratteri che le stanno dietro non mi disturba più di tanto. La parte in cui si fa notare che si può confondere una legatura con un carattere Unicode di tutt’altro significato, invece, mi rende più perplesso.

P.S.: non c’entra molto, o forse sì: già che la data di oggi (2020-02-02) è palindroma, qualcuno conosce un software capace di creare buoni ambigrammi, che non siano le due o tre schifezze che arrivano dalla ricerca standard con Google? Grazie anticipate.