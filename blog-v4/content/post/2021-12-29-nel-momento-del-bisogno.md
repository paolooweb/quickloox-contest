---
title: "Nel momento del bisogno"
date: 2021-12-29T02:07:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [Apple, Magic Trackpad, Magic Keyboard] 
---
La mia piccola [avventura con un nuovo Mac mini e una vecchia tastiera](https://macintelligence.org/posts/Cercasi-breakthrough.html) si avvia alla conclusione: nel giro di una ventina di ore dovrebbe arrivare in consegna da Apple Store una Magic Keyboard, che mi aspetto funzioni al primo colpo.

Prima però devo ringraziare i responsabili dell’ondata di solidarietà di cui sono stato immeritatamente oggetto. Non è elegante fare nomi, ma tutti sono andati oltre qualsiasi inesistente dovuto, con consigli, suggerimenti domande e infine due tastiere fisiche per fare un test e poi permettere il setup della macchina.

Per ora del mini nuovo posso solo dire che sta recuperando i dati da Time Machine e la sua temperatura operativa esterna è uguale a quella del vecchio mini spento e staccato dalla corrente. Ovviamente non è un test probante, che però segna una differenza netta rispetto alla corrente calda prima residente stabile sul lato sinistro della scrivania.

Degli amici che ho potrei dire molto di più, se questo non fosse un blog di tecnologia. Vicini e lontani, persone straordinarie che mi hanno fatto sentire dentro un film natalizio con il finale che commuove.

_Per ora si può lasciare un commento dalla [pagina apposita di Muut per Qui
ckLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile._