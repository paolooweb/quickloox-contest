---
title: "Tempi moderni"
date: 2015-04-08
comments: true
tags: [Stopera, iPhone, Cina]
---
La [storia di Matt Stopera](http://www.buzzfeed.com/mjs538/i-followed-my-stolen-iphone-across-the-world-became-a-celebr) ha dell’incredibile per chiunque nato nel secolo scorso. Gli rubano il telefono e dopo un anno capisce che l’apparecchio si trova in Cina, dove nasce un’iniziativa spontanea per trovare la persona che ne è (innocentemente) in possesso, *Fratello Arancia*.<!--more-->

L’articolo è il resoconto della sua visita, che diventa un caso nazionale, con interviste, cerimonie, striscioni inneggianti all’amicizia tra i popoli e sì, anche un’amicizia surreale ma concreta con Fratello Arancia, sullo sfondo di una Cina sospesa tra arcaismi, superstizione e ipermodernità.

La storia ha dell’incredibile, naturalmente, soprattutto per chi non abbia mai avuto un iPhone. L’unico apparecchio dotato di carisma sufficiente a dare vita a una vicenda del genere. E che valga la pena di volare fino in Cina, con effetti difficili da credere, per reincontrare.