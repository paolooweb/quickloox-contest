---
title: "Estremo Oriente"
date: 2015-05-05
comments: true
tags: [iPad, Giappone, Singapore, Lee, sudoku]
---
In Giappone i postini svolgono un servizio di verifica delle condizioni degli anziani soli, per rassicurare i parenti lontani. Apple e Ibm hanno concordato con le Poste giapponesi la [fornitura agli anziani di iPad](http://www.apple.com/it/pr/library/2015/04/30Japan-Post-Group-IBM-and-Apple-Deliver-iPads-and-Custom-Apps-to-Connect-Elderly-in-Japan-to-Services-Family-and-Community.html) per potenziare e arricchire il servizio, con l’obiettivo di raggiungere entro il 2020 tra i quattro e i cinque milioni di persone.<!--more-->

Il Primo ministro di Singapore ha un passato di programmatore e, dopo averlo ricordato in un [discorso](http://www.pmo.gov.sg/mediacentre/transcript-speech-prime-minister-lee-hsien-loong-founders-forum-smart-nation-singapore), si è sentito chiedere il codice sorgente di un certo programma che aveva scritto per risolvere i [sudoku](http://it.wikipedia.org/wiki/Sudoku).

Lo ha pubblicato l’altroieri su [Google Drive](https://drive.google.com/folderview?id=0B2G2LjIu7WbdfjhaUmVzc1lCR2hUdk5fZllCOHdtbFItbU5qYzdqZGVxdmlnRkJyYVQ4VU0&usp=sharing&usp=sharing&urp=https://drive.google.com/folderview?id%3D0B2G2LjIu7W#list).

Oriente che sarà anche estremo, ma vorrei tanto vederlo come normalità italiana. Ci sarebbe da dare anche una scorsa al discorso del *premier* di Singapore, che eclissa le volte buone e le ripartenze del nostro omologo. A sua volta – niente equivoci – il meno peggio possibile oggi nel nostro Paese, che è tutto dire.