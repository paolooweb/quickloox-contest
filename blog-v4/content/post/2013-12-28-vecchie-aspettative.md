---
title: "Vecchie aspettative"
date: 2013-12-29
comments: true
tags: [Mac]
---
Il mito vuole che i computer Apple costino più degli altri. Meglio: che *valgano* come gli altri *nonostante* costino di più.<!--more-->

Su *Futurelooks* hanno considerato un Mac Pro al massimo delle specifiche – una belva [nei dintorni dei diecimila euro](http://store.apple.com/it/buy-mac/mac-pro?product=MD878T/A&step=config) – e hanno lavorato sulla carta per creare un Pc di specifiche identiche.

[Costa di più](http://www.futurelooks.com/new-apple-mac-pro-can-build-better-cheaper-pc-diy-style/1/). Per la precisione il [venti percento in più](http://www.futurelooks.com/new-apple-mac-pro-can-build-better-cheaper-pc-diy-style/3/), non monetine.

Un’altra vecchia aspettativa superficiale che se ne va per il dimenticatoio.