---
title: "Sulle tracce dell’incoscienza"
date: 2022-08-10T01:30:12+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [giorgiospiga, GitHub, Bing, Windows, Office, DuckDuckGo]
---
Grazie a [@giorgiospiga](https://twitter.com/giorgiospiga) per avermi segnalato il prossimo cambio di abitudini di GitHub in fatto di tracciamento della navigazione: il sito, che prima non lo faceva, [distribuirà cookie](https://github.com/github/site-policy/pull/582) per *migliorare l’esperienza*, come si dice quando l’obiettivo è erogare pubblicità più personalizzata grazie alla maggiore profilazione di chi passa di lì.

I proprietari di GitHub sono gli stessi di Bing, il motore di ricerca che [traccia i Magic Link](https://scribe.rip/@ryanbadger/magic-links-can-end-up-in-bing-search-results-rendering-them-useless-37def0fae994) prodotti da certe app e servizi per eseguire un login sicuro (ehm) senza bisogno di una password.

Sempre gli stessi proprietari inseriscono [annunci pubblicitari in software a pagamento](https://answers.microsoft.com/en-us/msoffice/forum/all/remove-ads-from-paid-office-365/8235ac9d-01a6-432e-834b-68af8018884b) che loro chiamano *sistema operativo* e *suite per la produttività individuale*. Annunci pubblicitari che ovviamente valgono di più, se maggiormente personalizzati grazie al lavoro di tracciamento compiuto da motori come Bing e siti come GitHub.

Si è detto che il browser *sicuro* DuckDuckGo si proclama paladino della privacy e della riservatezza ma ricava i suoi risultati da Bing e, per accordo con i proprietari di Bing, lasciava funzionare di nascosto i sistemi di tracciamento di quest’ultimo? Una volta uscita la notizia, e solo allora, DuckDuckGo si è data da fare per [bloccare anche i tracker di Bing](https://arstechnica.com/gadgets/2022/08/microsoft-trackers-run-afoul-of-duckduckgo-get-added-to-blocklist/). Chissà se è vero, se è vero per tutti i tracker, se deve saltar fuori ancora qualcosa che per ora non sappiamo.

Ho sempre sostenuto che chi usa i software e i prodotti menzionati non ha le idee chiare su che cosa sta facendo. Chi avesse voglia di darmi torto, risponda per favore alla domanda successiva: questa gente ha coscienza di che cosa stiano facendo quelli che gli vendono i software spacciati per professionali, invece semplici accessori funzionali a un sistema seminascosto di monetizzazione del traffico personale?