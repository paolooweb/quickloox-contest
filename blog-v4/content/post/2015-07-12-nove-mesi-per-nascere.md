---
title: "Nove mesi per nascere"
date: 2015-07-10
comments: true
tags: [Dediu, Asymco, iOS, Windows]
---
Comunica Horace Dediu di Asymco che da tre trimestri [si vendono più computer iOS che computer Windows](https://macintelligence.org/posts/2015-06-11-il-massimo-del-minimo/).

Bastano nove mesi per capire che è nato un nuovo mondo e i vecchi pensieri sul mitico mercato non funzionano proprio più?