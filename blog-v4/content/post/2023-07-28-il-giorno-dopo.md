---
title: "Il giorno dopo"
date: 2023-07-28T10:24:47+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [Streaming Photo]
---
Mi è arrivata l’email finale di chiusura di Streaming Foto. Per essere esatti, le cinque email finali, su altrettanti indirizzi.

Se non fossero arrivate, non me ne sarei accorto. Il tempo era un po’ ventoso ma bello e la temperatura ottimale. Abbiamo pure mangiato la pizza, ottima.

Sì, prima o poi ne sentirò la mancanza perché mi avrebbe fatto comodo su *quella* foto, o perché sto lavorando sul campo con iPad mentre scatto immagini con iPhone. Me ne farò una ragione. Di sicuro non genenerò una confusione inutile con l’accensione di iCloud Foto.

E [tutto si conferma](https://macintelligence.org/posts/2023-06-22-allo-streamo-delle-forze/).