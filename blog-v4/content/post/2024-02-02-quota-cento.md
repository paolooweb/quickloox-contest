---
title: "Quota cento"
date: 2024-02-02T00:34:20+01:00
draft: false
toc: false
comments: true
categories: [Web]
tags: [Agenzia delle Entrate]
---
Piccola digressione burocratica. Per emettere le mie fatture elettroniche uso il portale dell’Agenzia delle Entrate. Facendolo da oltre un anno, ho un *bookmark* che mi porta direttamente dove mi serve. Così non ricordo proprio il contesto in cui si trova il portale.

Oggi sono entrato nel portale dell’Agenzia delle Entrate (area riservata) per altre faccende e mi sono detto *ne approfitto, che devo emettere una fattura*. Non ero nel mio solito *bookmark* e avevo invece davanti l’elenco dei servizi forniti dall’agenzia. Va beh, senza pensarci, cerco *fatturazione elettronica*. Entro e… non è lui. Posso consultare le fatture emesse, esportarle, ma non altro.

A questo punto subentra la curiosità. Il portale dei servizi dell’Agenzia dell’Entrata conta *novantaquattro* servizi possibili. Non vorrei mai mettermi nei panni di qualcuno che ha bisogno ma non sa bene che cosa scegliere e se li deve sorbire alla ricerca di quello giusto. Chissà che cosa pensano i guru della *user experience*, di una pagina con novantaquattro opzioni.

Nel frattempo avevo bisogno della mia fattura elettronica, ma non volevo più cliccare pigramente sul mio *bookmark*; volevo capire. Così ho cercato su Google come per partire da zero.

E ho capito. L’Agenzia delle Entrate offre sul suo portale novantaquattro servizi. Ma l’emissione delle fatture elettroniche si fa nel portale *Fatture e corrispettivi*. Un altro. Il novantacinquesimo servizio. Staccato dagli altri per chissà quale ragione.

Ci saranno altre situazioni di questo tipo? Raggiungeremo quota cento? Spero di poter restare non interessato a scoprirlo.