---
title: "Riprenderemo le trasmissioni il più tardi possibile"
date: 2019-10-22
comments: true
tags: [XScreenSaver]
---
Se nella televisione di una volta, al posto dell’[intervallo con le foto dei paesaggi](https://www.youtube.com/watch?v=_ACoU8fRyEw), avessero piazzato un bel salvaschermo, nessuno avrebbe mai avuto fretta di vedere riprendere la programmazione.

<iframe width="560" height="315" src="https://www.youtube.com/embed/_ACoU8fRyEw" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Nessuno può seriamente pensare di battere [XScreenSaver](https://www.jwz.org/xscreensaver/) quanto a varietà, ma il campo dei salvaschermo è tuttora ampio e variegato. L’appassionato è invitato a considerare questa [raccolta presente su GitHub](https://github.com/agarrharr/awesome-macos-screensavers), contenente diverse perle anche se non proprio tutto è totalmente originale come idea.

Gli orologi e in particolare quello frattale, le evoluzioni del logo Apple, gli emoji, le ricerche più gettonate nel mondo… c’è diversa roba con cui divertirsi. Non è mai spazio sprecato e, come direbbe un monaco Zen, la migliore app da usare è quella che aspettiamo di lanciare una volta finito di ammirare il salvaschermo.
