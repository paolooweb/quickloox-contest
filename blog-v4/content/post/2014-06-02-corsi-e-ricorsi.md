---
title: "Corsi e ricorsi"
date: 2014-06-02
comments: true
tags: []
---
Per terminare in bellezza la copertura dell’imminente Wwdc, ecco un bell’[intervento di Technologizer](http://www.technologizer.com/2014/06/01/wwdc/) su che cosa è successo nelle ultime edizioni della manifestazione e qual è stata la ricaduta sull’opinione pubblica nonché sul mercato delle azioni. Ci sono anche tutti i video in edizione integrale.<!--more-->

Riporto le conclusioni, che poi sono le predizioni per quest’anno:

>Apple si concentrerà sul software; qualcuno si lamenterà per la scarsa concentrazione sull’hardware e per il mancato annuncio di un iWatch; qualunque cosa annunci Apple, avrà effetti del tutto limitati sulla quotazone del titolo in Borsa.

Giusto perché dobbiamo ricordarci che ha assunto rilevanza planetaria, ma è sempre un incontro di e per programmatori.