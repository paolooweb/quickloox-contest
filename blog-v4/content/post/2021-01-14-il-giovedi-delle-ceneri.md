---
title: "Il giovedì delle ceneri"
date: 2021-01-14T00:54:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [Coleslaw, QuickLoox, Lisp, Big Sur] 
---
È destino che questo blog debba risorgere dalle sue ceneri ogni tanto e stavolta è merito, per modo di dire, dell’aggiornamento a [Big Sur](https://www.apple.com/macos/big-sur/). Sembra impossibile ricostruire il vecchio [Octopress](http://octopress.org); ogni volta qualcosa manca, quello va compilato non compila, l’installazione dà errore, ho girato a vuoto per ore e la materia non sembra di interesse estremo su Internet.

Così, la rinascita dalle ceneri avviene grazie a [Coleslaw](https://github.com/coleslaw-org/coleslaw), mio vecchio pallino. Coleslaw non ha particolari meriti se non l’essere sviluppato in linguaggio Lisp. Una occasione unica per imparare qualcosa in più e in meglio riguardo al mio linguaggio di programmazione preferito.

La base di partenza di Coleslaw è meno completa di quella di Octopress e mancano tante cose. Ora mi interessa solo pubblicare. **Tutto quello che sta intorno a questo post è provvisorio**, frutto dell’installazione standard. Ci sono cose senza senso, cose da aggiornare, cose da correggere.

Bisogna poi importare tutti i post precedenti, aggiungere i commenti, fare un sacco di cose. Lo so. Ma prima di tutto, pubblicare.

Il resto arriva un passo per volta.

Se qualcuno morisse dalla voglia di commentare o dirmi qualcosa, posso solo pregarlo di pazientare e intanto magari [chiedermi](mailto:lux@mac.com) un invito per il [gruppo Slack parallelo a questo blog](https://app.slack.com/client/T03TMRQRC/C03TMRQU0). Qualunque conoscenza di Coleslaw è benvenuta!