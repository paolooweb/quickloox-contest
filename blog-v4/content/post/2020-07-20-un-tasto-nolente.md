---
title: "Un tasto nolente"
date: 2020-07-20
comments: true
tags: [iPad, Lidia, Nive]
---
Il tempo passa e le figlie crescono. La prima ora si mette su iPad per scegliersi i giochi in cui eccelle oppure ha un interesse preciso. La seconda ha preso il posto della prima ed [esplora l’iPad di prima generazione](https://macintelligence.org/blog/2015/11/10/scala-uno-a-quindici/) (che ha il quadruplo dei suoi anni) a scoprire la qualunque.

Noto che, a parità di età, lo schema di esplorazione è diverso: Lidia si concentrava su pochi giochi e voleva andarci in fondo. Nive è più esplorativa: apre un gioco e lo scandaglia attraverso una prova dei comandi, con la metodicità propria della sua età, vale a dire come capita.

Il metodo Nive è diventato una cartina di tornasole eccellente per valutare il design dell’interfaccia di un gioco. Uno dei parametri migliori è il tempo entro cui lei arriva all’attivazione di qualche funzione che implichi il pagamento, seguito da quanto è facile o difficile uscirne.

Molti giochi sono progettati per incoraggiare la scoperta, diciamo così, delle opzioni a pagamento il più possibile a prescindere dall’effettiva volontà di farlo e una bambina ignara al riguardo è una collaudatrice formidabile.