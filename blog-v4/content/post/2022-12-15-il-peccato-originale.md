---
title: "Il peccato originale"
date: 2022-12-15T12:29:33+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [ChatGPT, Gizmodo, New Yorker]
---
Gizmodo ha chiesto a ChatGPT di scrivere un articolo. I risultati sono raccolti nell’articolo (scritto da umani) [ChatGPT ha scritto un articolo terribile per Gizmodo](https://gizmodo.com/chatgpt-gizmodo-artificial-intelligence-openai-media-1849876066). Questo l’input:

>Scrivi un articolo per Gizmodo in cui spieghi i grandi modelli di linguaggio [quelli che addestrano gli algoritmi dei sistemi come ChatGPT]. Ricordati di inserire esempi specific. Mantieni il tono leggero e disimpegnato.

Il risultato:

>Quello che pensavamo fosse un processo veloce ed efficiente si è rivelato lungo, laborioso e frustrante.

Tra i problemi, l’inserimento di informazioni sbagliate, la trascuratezza su dettagli importanti, certi paragrafi che sembravano *uno spot pubblicitario per OpenAI* più che una spiegazione accurata.

>In generale, ChatGPT ha faticato a trovare un buon equilibrio tra informazioni fattuali, struttura dell’articolo e linguaggio accessibile e spiritoso.

Se ChatGPT fosse un freelance non lo richiamerebbero, concludono in Gizmodo.

Qualche anima bella è convinta che basti dilatare ulteriormente il modello di addestramento per risolvere i problemi.

Una mossa che funzionerà molto parzialmente e, per il resto, lascerà inalterati – se non li accentuerà – i problemi algoritmici.

Se lo proponessero come tecnologia sperimentale che indaga i confini tra uomo e macchina grazie all’apprendimento meccanizzato e a all’elaborazione del linguaggio naturale, ChatGPT andrebbe celebrato con entusiasmo. È un progresso evidente, nella qualità dell’interazione e nell’accessibilità universale della tecnologia.

Invece si approfitta di ogni situazione per calare l’asso dell’intelligenza artificiale in un modo irritante che ricorda il marketing di Intel quando, per continuare a vendere, scriveva che un processore con più megahertz era più veloce di quello prima.

Il problema sta negli algoritmi, non nella base dati, e di progresso da questo punto di vista se ne vede poco. È il peccato originale di ChatGPT, senza soluzioni in vista.

Artificiale, quanto ne vogliamo; di intelligenza, non ce n’è.

Se ne accorgono persino a *Gizmodo*, che non è esattamente il [New Yorker](https://www.newyorker.com).