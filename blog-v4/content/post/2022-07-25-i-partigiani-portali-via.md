---
title: "I partigiani, portali via"
date: 2022-07-25T01:47:58+01:00
draft: false
toc: false
comments: true
categories: [Hardware]
tags: [Mac, Fëarandil, Jack Wellborn, Wellborn, M2, M1, Apple Silicon, Intel, Wintel]
---
Devo a [Fëarandil](https://www.twitch.tv/fearandil) la lettura illuminante di oggi, tratta dal blog [Worms and Viruses](https://wormsandviruses.com/2022/07/a-burger-without-heinz/):

>Ho colto una tendenza recente per cui i recensori hanno inesplicabilmente smesso di confrontare portatili Wintel con MacBook Apple. Per esempio, confrontiamo la recensione di Surface Laptop Go 2 su Ars Technica con quella di Surface Book 2 del 2017. La recensione attuale comprende solo altri portatili Wintel, mentre quella del 2017 comprendeva anche il MacBook di riferimento quell’anno.

>Hanno paura che mostrare costantemente MacBook battere Wintel li faccia passare per partigiani di Apple? Non capisco perché. I fatti sono fatti e, a prescindere, molti vogliono o devono comprare un portatile Windows comunque.

>Non riesco a fare a meno di domandarmi se, nella mente di molti recensori, i MacBook siano stati PC solo fino a quando hanno usato Intel e abbiano smesso di essere PC una volta che Apple è passata ad Apple Silicon.

Una bella scelta: o hanno paura di mostrare che Apple Silicon è superiore, o temono che mostrando i fatti siano etichettati come faziosi, o hanno un pregiudizio per cui un computer può essere tale solo se è Intel.

Il problema è che ne usciamo non benissimo, in ciascun caso, e questo dice più che abbastanza sull’attendibilità della categoria. Che quando arriverà un worm o un virus a farli sparire tutti da Internet, sarà stato comunque troppo tardi.