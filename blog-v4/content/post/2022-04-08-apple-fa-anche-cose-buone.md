---
title: "Apple fa anche cose buone"
date: 2022-04-08T02:52:52+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [App Store, iWork, Pages, Numbers, Keynote, Shortcuts, Comandi Rapidi]
---
Nel giro di poche ore: ripristinata su App Store russo [la app usata dall’opposizione](https://arstechnica.com/?p=1846517) per coordinarsi in vista delle elezioni; aggiornamento di iWork che aggiunge poche cose, una delle quali però è [un primo supporto dei Comandi rapidi](https://arstechnica.com/?p=1846651); sponsorizzato studio che [mostra l’importanza delle app indipendenti su App Store](https://www.bloomberg.com/news/articles/2022-04-07/apple-facing-outcry-says-app-developers-are-thriving-on-iphone).

Il mondo potrebbe sempre essere un posto migliore, chiaramente. La app russa avrebbe potuto non essere mai ritirata, vorremmo aggiornamenti di iWork più corposi e App Store ha la sua quota di situazioni migliorabili.

Si tratta comunque di passi sulla strada giusta e, se minimamente, c’è comunque da essere contenti.