---
title: "Guai a fermarsi"
date: 2013-08-02
comments: true
tags: [iWork]
---
Questo post è stato scritto con la *beta* di Pages su iCloud, su Safari.

Niente di speciale in sé; prima di accumulare le migliaia di parole già prodotte con un *browser* grazie a Google, iCloud ha da mangiarmi un bel po' di pagnotte.<!--more-->

Tuttavia potrebbe diventare una interessante alternativa a lanciare Pages sulla scrivania di Mac, che occupa risorse e memoria, mentre invece questa è semplicemente una pagina in più aperta con Safari.

Il tema è meno ozioso di quanto sembri. Per esempio mi è appena arrivato l'invito per la [beta di Editorially](https://editorially.com), servizio specializzato per lo scrivere che offre solo testo, ma con compatibilità [Markdown](http://daringfireball.net/projects/markdown/), lavoro collaborativo e commenti. Tre cose che questo Pages non ha.

Siamo lontani anni luce dalla comodità assoluta di un [BBEdit](http://barebones.com/products/bbedit/). Ma guai a pensare di non avere più bisogno di strumenti nuovi.

 /images/pages.png 
