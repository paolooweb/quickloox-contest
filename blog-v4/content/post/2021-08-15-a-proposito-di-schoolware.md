---
title: "A proposito di schoolware"
date: 2021-08-15T02:45:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [curricolo, scuola, BBEdit, JEdit, Atom, Chromebook, iOS, iPadOS, Java, Microsoft, Google, Apple, Unix, Linux] 
---
Nel teorizzare il mio [curricolo di skill digitali per il primo e il secondo ciclo della scuola primaria](https://macintelligence.org/posts/Un-curricolo-per-lestate.html) sono partito come un rullo compressore a [parlare di ipertesto](https://macintelligence.org/posts/Ipertestare-la-scuola.html), solo che prima occorre qualche generalizzazione riguardante il software.

Più avanti spiegherò anche perché è opportuno lavorare, specie inizialmente, in assenza di software, a patto che il software arrivi, in quanto esiste una differenza tra fondamenta e anacronismi.

Però ora illustro le caratteristiche generali che dovrebbe avere idealmente il software nella scuola:

- dovrebbe essere software libero ovunque possibile. Per chi crede ai principî, _public money, public software_: il denaro speso nella scuola è di tutti ed è brutto spenderlo su software proprietario.
- dovrebbe essere software universale, disponibile su ogni e qualsiasi piattaforma. PC, Mac, Unix, Linux, Chromebook, iOS, Android, iPadOS come minimo. L’ipotesi di lavoro è che sia possibile, nei limiti del ragionevole, applicarsi con qualunque hardware, anche l’ultimo degli smartphone portato in classe da un ragazzo meno abbiente oppure il computer più vecchio presente nel vecchio laboratorio di informatica (che dovrebbe lasciare il passo all’applicazione trasversale dell’informatica stessa nelle materie tradizionali).
- il requisito precedente è spesso utopico. Probabilmente le scelte andranno verso software basato su Java oppure webapp accessibili da browser, che creano meno problemi di compatibilità interpiattaforma. Su iOS e iPadOS vale solo la seconda ipotesi.
- Per questo motivo, invece che ragionare per _applicazioni_ sarebbe opportuno ragionare per _formati_ e lavorare su quelli raggiungibili da tutti gli apparecchi a disposizione, o dal maggior numero di essi, importa poco con che strumento software. L’obiettivo non è insegnare lo strumento, ma padroneggiare il lavoro sul formato. Più il formato è elementare, più facilmente ci si lavorerà su qualunque apparecchio.

Ragionare per formato aggira inoltre il tentativo delle società Big Tech di intrappolare le scuole in una bolla tecnologica proprietaria e permette agli studenti di scoprire la varietà degli strumenti a disposizione, insospettabile per persone non addentro e che invece rappresenta un valore immenso. Per specializzarsi su (o chiudersi in) applicazioni o piattaforme specifiche gli studenti avranno tutto il tempo che vogliono durante gli studi superiori.

Il discorso cloud è complicato. Naturalmente una Google o una Microsoft hanno un grande interesse a vendere alle scuole cloud arredato con applicazioni e da qui nascono molti mali (nonché a volte scelte dettate da interesse privato).

L’Italia avrebbe invece interesse a valorizzare nel modo migliore il denaro destinato all’istruzione di base. Purtroppo il discorso si fa politico e lo si amministra in modi che tengono conto di tutto tranne l’interesse di chi studia.

In un mondo distopico, ciascuna scuola ha le competenze (molto prima delle risorse necessarie, che sarebbero minime) per allestire il proprio cloud privato, perfino tenendolo fuori da Internet per risparmiare denaro e lavorare in sicurezza.

In un mondo utopico, il Ministero dell’Istruzione attrezzerebbe un proprio cloud a disposizione delle scuole.

In un mondo ideale, sempre il Ministero finanzierebbe la libera iniziativa delle scuole nell’acquisto dello spazio cloud necessario a ciascuna, con precedenza a fornitori europei e prezzi calmierati.

In un mondo possibile, le scuole farebbero quello che vogliono, purché il risultato sia avere a disposizione un cloud puro, da popolare con strumenti aventi le caratteristiche di cui sopra.

In un altro mondo possibile, il Ministero (eh, quante cose NON fa!) acquista a buon prezzo spazio cloud da tutti, Amazon, Google, Microsoft eccetera, e poi dà alle scuole lo spazio che occorre.

Nel mondo che abbiamo, le scuole sono ostaggio dei cloud arredati e questo crea inevitabilmente sovrapposizioni tra il software che intendiamo usare per gli studenti e quello proprietario e pagato già dentro il cloud. Qui serve una prese di coscienza delle dirigenze e chi la attuerà porterà un gran beneficio ai propri studenti. Certo bisogna impegnarsi, ma la strada più larga è quella meno vantaggiosa già dai tempi del Vangelo.

*Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile.*