---
title: "App al lavoro"
date: 2018-01-10
comments: true
tags: [app, Dediu, McDonald’s]
---
Horace Dediu riassume lo [stato dell’economia di iOS](http://www.asymco.com/2018/01/08/the-ios-economy-updated/).<!--more-->

I pagamenti agli sviluppatori hanno superato il fatturato di McDonald’s nel 2016.

La vendita di app genera grossolanamente più denaro dell’industria cinematografica. Qualcosa che forse non è immediatamente comprensibile a tutti.

Le curve dei grafici sono protese verso l’alto in maniera impressionante. Niente dura per sempre, chiaro, ma segni di rallentamento non se ne intravedono. È difficile vivere di app, ma ugualmente c’è una distribuzione globale di ricchezza evidente e consistente.

Ci sono anche altre conclusioni da trarre, sulla stabilità dell’ecosistema, sul futuro di iOS, sulla tenuta di iPhone, ma per una volta le lascio a chi vorrà.