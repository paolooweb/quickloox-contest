---
title: "Chi ci guadagna"
date: 2016-02-12
comments: true
tags: [Apple, fatturato, profitto]
---
Saranno in linea di principio più interessanti i prodotti di un’azienda molto bene organizzata oppure quelli di una che lo è meno?<!--more-->

Proviamo a capirlo da [questo tweet](https://twitter.com/dcurtis/status/694266299317243904) che riassume il fatturato per dipendente delle grandi multinazionali dell’informatica.

Nelle risposte si vede anche la tabella dei profitti per dipendente, sulla medesima falsariga.

<blockquote class="twitter-tweet" data-lang="en"><p lang="en" dir="ltr">Revenue per employee, 2015:<br><br>Yahoo: $419,830<br><br>Twitter: $462,009<br><br>MSFT: $789,145<br><br>Google: $1,160,648<br><br>Facebook: $1,412,655<br><br>Apple: $2,032,304</p>&mdash; dustin curtis (@dcurtis) <a href="https://twitter.com/dcurtis/status/694266299317243904">February 1, 2016</a></blockquote> <script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>