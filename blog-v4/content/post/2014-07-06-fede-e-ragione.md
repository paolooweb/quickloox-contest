---
title: "Fede e ragione"
date: 2014-07-06
comments: true
tags: [Mormoni, iPadmini, iPad, Luoma, Homebrew, Xquartz]
---
La religione si accorge delle tecnologie non nuove (quattro anni e passa) e, all’indomani dell’[esempio delle letture](https://macintelligence.org/posts/2014-06-20-il-buon-pastore/) e dell’[aggiornamento automatico di Xquartz e Homebrew](https://macintelligence.org/posts/2014-06-21-e-fammi-sapere/) forniti dal reverendo Luoma siamo a parlare di nuovo di ministero del culto e innovazione, stavolta lato hardware.<!--more-->

La notizia che, a seguito dell’esperimento positivo di 6.500 unità, la Chiesa (mormone) di Gesù Cristo dei santi dell’ultimo giorno intende munire 32 mila missionari di iPad mini entro l’inizio del 2015 presenta più livelli di lettura di quello che sembra.

L’organizzazione è infatti sempre stata diffidente verso la tecnologia e, tradizionalmente, i missionari hanno sempre svolto una vita sociale molto limitata, famiglia (lontana) e proselitismo sul luogo. Ora sono incoraggiati anche a convertire via Facebook.

Chi li ha conosciuti sa che i missionari mormoni vivono modestamente ma dietro loro c’è una istituzione con notevoli leve finanziarie.

Sulle scelte di fede non ho titolo di discutere; su quelle di ragione noto che una grossa organizzazione (in totale 88 mila evangelizzatori sparsi per il globo), libera di decidere senza pastoie economiche, sceglie iPad.