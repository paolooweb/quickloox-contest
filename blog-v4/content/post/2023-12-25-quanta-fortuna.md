---
title: "Quanta fortuna"
date: 2023-12-25T21:37:15+01:00
draft: false
toc: false
comments: true
categories: [Internet]
tags: [Gruber, John Gruber, Daring Fireball]
---
John Gruber ha ripubblicato [un proprio post del 2011](https://daringfireball.net/2011/12/merry).

>La notte scorsa, mentre ispezionavo il lavoro di Babbo Natale, mi è venuto un pensiero. Tra un decennio, quando, diciamo, aspetterò mio figlio che torna dal college per le vacanze invernali e, quando lo fa, vuole passare il tempo con i suoi amici… quanto sarò disposto a pagare per poter tornare indietro nel tempo per un giorno, ad adesso, quando ha otto anni e vuole andare al cinema e costruire kit Lego insieme a me, e crede nella magia?  
Quanto, per un giorno con la mia famiglia come quello che abbiamo esattamente oggi? Quanto? Qualsiasi cifra.  
La verità è che oggi sono la persona più fortunata del mondo. Spero che anche tu lo sia.

Nel 2011 non avevo gli strumenti per capire appieno il senso di questo messaggio. Oggi ho una figlia di nove anni, una di cinque, e ho la grande fortuna di poterlo capire e apprezzare di cuore.

Buon Natale anche se l’ho scritto già ieri.