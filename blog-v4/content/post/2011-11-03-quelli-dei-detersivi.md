---
title: "Quelli dei detersivi"
date: 2011-11-03
comments: true
tags: [Script, iPad]
---
Li avevo chiamati [quelli degli antiforfora](https://macintelligence.org/posts/2010-02-08-quelli-degli-antiforfora/).

Avevano scritto una magistrale <a href="http://www.altroconsumo.it/mobile/apple-ipad-s265883.htm">recensione di iPad</a> in contumacia (guardando su Internet la presentazione di Steve Jobs), che ancora oggi appare profetica.<!--more-->

>&hellip;aspettative troppo alte rischiano poi di causare delusione quando si alza il sipario, e sembra esser questo il caso dell&rsquo;iPad.

>&hellip;potrebbe essere la miglior cornice per foto digitali di tutti i tempi&hellip;

>La nostra prima impressione &egrave; che questo sia una specie di iPod Touch gigante, e non sembra scontato che il mondo ne abbia bisogno, men che meno che sia disposto a pagarlo 500 euro.

Che hanno fatto stavolta? Niente, mi hanno scritto per convincermi ad abbonarmi in promozione alla loro rivista e associazione.

Mi hanno convinto solo a disiscrivermi dal loro elenco di indirizzi e per questo ho fatto clic sul *link* apposito del loro messaggio.

*Link* che non porta da nessuna parte, per un errore nel codice.

Non solo incapaci di giudizio, ma anche di tecnica. Da adesso stanno nel filtro antispam, da qui all&rsquo;eternit&agrave;.