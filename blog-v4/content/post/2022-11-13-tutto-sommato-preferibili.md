---
title: "Tutto sommato, preferibili"
date: 2022-11-13T19:08:00+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [Ventura, macOS, Settings, Preferenze di Sistema]
---
A seguito del [post di ieri](https://macintelligence.org/posts/2022-11-12-uffa.-però/), ho fatto un giro superficiale nelle nuove Preferenze di macOS Ventura e devo dire che mi aspettavo molto peggio.

Prima le Preferenze si articolavano in una finestra rettangolare orizzontale, in cui le icone dei pannelli occupavano posizioni fisse. Un clic sul una icona la apriva, sempre all’interno della cornice di partenza.

Ora le preferenze sono a sviluppo verticale, sullo stile di quelle di iPad e iPhone: colonna di sinistra per le sezioni da aprire, colonna di destra per le opzioni di configurazione presenti.

Voto favorevole, perché i pannelli sono state aumentati e riorganizzati. Non tutto è dove lo avrei messo io; nel complesso però la situazione è un po’ più chiara e ci si è guadagnato. Non tutto è esattamente dove stava prima, ma c’è sempre la ricerca ed è questione di farci la mano.

La nuova finestra a sviluppo verticale, siccome alcuni pannelli contengono molte voci, è ridimensionabile, solo in verticale. Questo è un grande vantaggio rispetto a prima, soprattutto sugli schermi grandi. Non mi piace è che il ridimensionamento sia sempre manuale; nell’aprire un pannello di preferenze, vorrei che si dimensionasse automaticamente in modo da mostrare tutti gli elementi in esso contenuti. Qui pareggiamo.

Per via di quanto sopra, l’interno delle sezioni è stato riorganizzato per svilupparsi verticalmente. Per lo più non cambia niente; in alcune situazioni si è persa una grande occasione di fare meglio, come per scegliere sfondo scrivania e salvaschermo, che lasciavano a desiderare prima e anche adesso. In altre occasioni il riordino è stato sommamente positivo, come per il pannello Network oppure per Spotlight.

Nelle Preferenze di iPhone e iPad, quando un pannello di preferenze ha più sottolivelli, si scivola di colonna in colonna; invece qui si arriva a una finestra modale, dove viene gestita eventuale navigazione ulteriore. Il sapore è indubitabilmente Mac. A un certo punto avevo inteso che, dove vedevo un pulsante nell’interfaccia, significasse l’aprirsi di una finestra modale; invece ho constatato almeno una eccezione, dove un pulsante dentro un pannello rimanda a un altro pannello. Succedeva anche prima, ma prima la confusione era manifesta; qui a un certo punto ho sperato che fosse stata eliminata e invece non del tutto.

A un certo punto mi sono reso conto che per tutta l’estate si è discusso animatamente di Stage Manager e che non ne vedevo traccia. L’oggetto del contendere è nascosto dentro le preferenze di Scrivania e Dock e non solo è disattivato, ma l’intera voce è in grigio!

Sembra un equivoco, dal momento che un clic sulla preferenza propone di attivarlo. In ogni caso, se tutti vivono la mia stessa esperienza, lavorano senza Stage Manager senza accorgersene e, se non sono curiosi, quando se ne accorgono sembra loro di non poterlo attivare. Abbastanza indolore. Per ora l’ho lasciato spento e un giorno proverò il brivido.

Tutto considerato, preferisco leggermente queste preferenze a quelle di prima. E oggi è un buon giorno.