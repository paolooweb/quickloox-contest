---
title: "Oltre metà dell’opera"
date: 2018-02-15
comments: true
tags: [iPhone]
---
Per quanto siano dati da prendere con ben più di un grano di sale, Strategy Analytics sostiene che nel trimestre natalizio [iPhone abbia conseguito il cinquantuno percento del fatturato](https://www.strategyanalytics.com/access-services/devices/mobile-phones/smartphone/smartphones/market-data/report-detail/value-share-global-smartphone-revenue-asp-and-profit-by-vendor-by-price-tier-q4-2017) totale del mercato.<!--more-->

Si noti che questo sarà accaduto vendendo attorno al venti-venticinque percento delle unità: ovvero, la cosa più lontana da un monopolio. Si compra un iPhone per scelta consapevole.

Per Apple è l’ultimo trimestre dell’anno ma il primo dell’anno fiscale. Si comincia piuttosto bene.