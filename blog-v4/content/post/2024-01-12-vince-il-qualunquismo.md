---
title: "Vince il qualunquismo"
date: 2024-01-12T02:19:12+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [Paku Paku]
---
Non c’è niente da fare. Se ne va uno dei padri dell’informatica, ci sono dissertazioni infinite sull’intelligenza artificiale e no, esce software interessante, ma questa sera al massimo posso arrivare a sfidare qualcuno a [Paku Paku](https://abagames.github.io/crisp-game-lib-11-games/?pakupaku), una versione *unidimensionale* di Pac-Man.

Sembra stupido, eh? Certo non è un gioco di spessore (come detto, è unidimensionale…). Ma stupido no. Qualcuno [ha superato i duemila punti](https://waxy.org/2024/01/paku-paku-one-dimensional-one-button-pac-man/). Un altro ha [scritto un bot](https://news.ycombinator.com/item?id=38849868) che gioca da solo.

Qualunquismo puro finché arriva il sonno.