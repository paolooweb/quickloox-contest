---
title: "L’ora della trasformazione"
date: 2022-02-27T01:47:39+01:00
draft: false
toc: false
comments: true
categories: [Hardware]
tags: [watch, Volvo]
---
Si fanno tante chiacchiere sulla trasformazione digitale. Volvo è passata ai fatti e [ha dotato millecinquecento meccanici di… un watch](https://www.computerworld.com/article/3651369/how-volvo-put-apple-watch-at-the-center-of-its-services-vision.html), con relativa app su iPhone.

Sul terminale da polso arrivano le informazioni relative al cliente, all’auto, al lavoro da svolgere, ai particolari della riparazione. Attraverso la app i tecnici possono programmare gli appuntamenti con il cliente e mantenersi aggiornati sulla situazione.

> non servono più documenti cartacei né l’accesso a un PC per rimanere aggiornati. Cosa che porterebbe via tempo: […] l’azienda mi ha detto che servivano sei mesi per addestrare nuovi dipendenti sui quindici diversi sistemi informativi usati prima da Volvo.

Dichiara Volvo che l’ottanta percento dei tecnici che usa la app ottiene punteggi migliori di soddisfazione del cliente. Le comunicazioni al cliente successive all’intervento sono aumentate del trenta percento mentre la produzione di carta stampata è diminuita del quaranta percento.

L’articolo contiene particolari interessanti rispetto alla realizzazione del sistema, come il fatto di averlo realizzato previa osservazione di quello che i meccanici fanno veramente sul campo, invece che immaginarlo a tavolino, oppure l’aver consultato i meccanici stessi, che hanno espressamente manifestato il fastidio di ricevere troppa informazione rispetto a quella essenziale per svolgere il lavoro.

Ma il vero ordigno nascosto, che fa deflagrare il vecchiume, è questo: *non è necessario usare un personal computer per mantenersi aggiornati*. La vera trasformazione digitale è spietata e fa giustizia dei luoghi comuni, nonché delle abitudini trentennali, ove inutili.