---
title: "Breve triste storia editoriale"
date: 2021-10-23T00:56:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [Sigil, Sigil-Ebook, PageEdit, BBEdit, XHtml, Opf] 
---
Il progetto Sigil mette a disposizione un editor per lavorare sui testi che compongono un ebook e nel contempo lasciare inalterata la struttura dell’ebook stesso.

È facilissimo smontare un ebook per accedere ai file in esso contenuti ed editarli ed è ugualmente facile rimontare tutto al momento giusto. È anche dannatamente noioso. Se occorre fare test ripetuti per risolvere un bug o decifrare un comportamento strano del codice, diventa mortale. L’idea di Sigil è rendere possibile la modifica dei file e intanto lasciare integro l’ebook. Un uovo di Colombo.

Negli anni il progetto si è rinominato [Sigil-Ebook](https://sigil-ebook.com). Sigil è cresciuto e continuamente migliorato e si è sdoppiato: da una sua costola è nato PageEdit, un editor visivo di documenti Xhtml o Opf, che è l’estensione di un file importante nella struttura degli ebook.

Solo che, prima, Sigil editava – volendo – in modo visivo. Si poteva lavorare sul testo, per dire, come si farebbe in un documento Pages, senza vedere il codice.

Ora non si fa più. L’editor è di testo puro e quindi, per toccare il testo, bisogna comunque stare in mezzo al codice. E allora uno, per lavorare con il codice, fa un copia e incolla in BBEdit, mille volte più potente e comodo. Se c’è un problema tecnico, Sigil resta molto comodo, ma si poteva fare anche prima. Se c’è un problema nel testo, o si sta facendo una revisione editoriale, la convenienza che c’era è del tutto svanita.

PageEdit è il milionesimo editor di Html sulla piazza, niente di speciale. La sua funzione specifica è enigmatica, oppure sono particolarmente stanco e non colgo un’evidenza. Se nasce con l’idea di dare uno strumento a chi vuole creare file Xhtml prima di mettere insieme l’ebook che li contiene, ancora una volta, BBEdit se lo mangia a colazione.

Uno strumento con una funzione interessante, non la possiede più. Fine della storia.

*Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile.*