---
title: "Dal caso al caos"
date: 2024-02-26T01:44:56+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [ChatGPT, OpenAI, ai, intelligenza artificial]
---
Interrompiamo le trasmissioni per ritrasmettere un comunicato di OpenAI intitolato [Risposte inaspettate da ChatGPT](https://status.openai.com/incidents/ssg8fh7sfyz3).

§§§

Il 20 febbraio 2024, una ottimizzazione all’esperienza dell’utente ha introdotto un bug nell’elaborazione del linguaggio da parte del modello.

Gli LLM generano risposte mediante un campionamento casuale di parole basato in parte sulle probabilità. Il loro “linguaggio” è fatto di numeri assegnati a token.

In questo caso, il bug riguardava la fase in cui il modello sceglie questi numeri. Come quando ci si perde in una traduzione, il modello ha scelto numeri leggermente sbagliati, che hanno prodotto sequenze di parole prive di senso. Più tecnicamente, i kernel di inferenza hanno prodotto risultati scorretti durante l’uso su certe configurazioni di GPU.

Nell’identificare la causa del problema, abbiamo disposto un rimedio e confermato di avere risolto l’incidente.

§§§

A tutti quelli che *intelligenza artificiale*, *il nostro cervello potrebbe funzionare così*, *capiscono*, *hanno coscienza*, *chissà se pensano*: OpenAI, che qualcosa ne sa, è chiarissima su che cosa ci sia dentro i modelli.

>Gli LLM generano risposte mediante un campionamento casuale di parole basato in parte sulle probabilità. Il loro “linguaggio” è fatto di numeri assegnati a token.

Scelti numeri leggermente sbagliati rispetto a quelli giusti che mantengono il suo prodigioso equilibrio linguistico, ChatGPT è scivolato nel caos. No, il nostro cervello non funziona così (anche se la casualità deve avere certamente un ruolo). Noi abbiamo una coscienza, loro una mappatura di numeri su token, che è un’altra cosa.

Avvengono innumerevoli scelte a caso, pilotate da complessi meccanismi a monte, e da lì escono sequenze di parole con senso compiuto. È pazzesco che accada e ci scapperà un qualche Nobel o analogo; l’intelligenza è tutta un’altra cosa e ci arriverà tutt’altro meccanismo, che poi magari conterrà anche un LLM, ma come uno tra molti altri ingredienti.

Ah, non cederà al caos per qualche numero sbagliato.