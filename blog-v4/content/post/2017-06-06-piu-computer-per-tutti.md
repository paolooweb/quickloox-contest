---
title: "Più computer per tutti"
date: 2017-06-06
comments: true
tags: [Wwdc, AirPods, Swift, Messaggi, iCloud, privacy, Safari, iPad, iOS, Dock]
---
Una analisi fin troppo frettolosa e svelta di [Wwdc](https://developer.apple.com/wwdc/) l’ho scritta [su Apogeonline](http://www.apogeonline.com/webzine/2017/06/06/apple-e-i-fondamentali). Ho un problema su questa edizione perché richiederebbe una quindicina di post e non tutti brevi. Per la prima volta in tanti anni riguarderò ancora con attenzione il [keynote](https://www.apple.com/apple-events/june-2017/).

Nelle ore successive alla presentazione le informazioni dovrebbero decantare e lasciare a galla il vero succo dell’evento. Il fatto è che Apple si è mossa con ampiezza anche inaspettata su una quantità di fronti notevole, riportando annunci hardware accanto a quelli software e mescolando le novità di superficie, a livello di interfaccia utente, con quelle profonde, infrastrutture di sistema e di rete (Messaggi con iCloud).

La comunicazione si va anche mescolando in modi non ortodossi: alla presentazione del convegno dei programmatori Apple non si è parlato sensibilmente di Swift, che ha avuto l’onore delle stampe [quattro giorni fa](https://macintelligence.org/posts/2017-06-02-difficili-profeti/). Ovviamente il calendario delle sessioni con i programmatori sarà tutt’altra questione, ma qui si parla della vetrina.

La decantazione: [Intelligent Tracking Prevention](https://webkit.org/blog/7675/intelligent-tracking-prevention/). Navigare su un sito di velieri senza ritrovarsi pubblicità di velieri a partire dal giorno dopo, su qualsiasi canale. Se uno tiene *veramente* alla *privacy* questa è roba non grossa, ma introvabile. Provare su Android, o anche su Windows per quello che vale. Vediamo se Chrome tiene botta. Io dico che sarà difficile. E questo è un terreno importante.

Sempre decantazione: l’anno scorso iPad aveva ricevuto poche attenzioni. Quest’anno tante e i miglioramenti sono evidentissimi, a partire dalle dimensioni degli schermi fino alle funzioni del sistema operativo e all’integrazione con Apple Pencil. Dock, *drag and drop*, *app* Files che sostanzialmente è un microFinder: ma non ci si lamentava che Mac sarebbe diventato come iOS? A me pare vi sia un interscambio sano di funzioni e che sia bidirezionale. Detto questo, con la tastiera esterna e interna *full size*, lo schermo migliorato e più ampio più iOS 11, un [iPad Pro](https://www.apple.com/it/ipad/) diventa assai più goloso che prima anche in dimensione 10,5”.

Ultima decantazione: [HomePod](https://www.apple.com/homepod/). Il solone da Facebook lo scarta come un altoparlante o un concorrente di Echo di Amazon. Invece Apple ha messo un processore dentro l’altoparlante, un processore vero, non una cosetta. Ha fatto lo stesso lavoro compiuto su [AirPods](https://www.apple.com/it/airpods/), dove [ha svolto un lavoro eccellente](https://macintelligence.org/posts/2017-05-10-questione-di-metodo-iii/).

Una volta giravano le *mission* aziendali tipo *un computer su ogni scrivania*. Oggi si potrebbe dire che il programma di Apple è *un computer dentro ogni cosa*.

Con tanta tecnologia e sofisticazione sotto, che ci cambia la vita senza che ce ne accorgiamo. Uno dei sottotemi di Wwdc è stato il *deep learning*, le macchine che imparano dal nostro comportamento per svolgere servizi (più) utili. Ecco, solo questo meriterebbe un altro *post*.

Su questo Wwdc ci sarà da tornare, più volte. Lo trovo un buon segno. 