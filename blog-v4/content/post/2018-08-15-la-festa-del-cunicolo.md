---
title: "La festa del cunicolo"
date: 2018-08-15
comments: true
tags: [Angband, Cataclysm, Brogue, NetHack]
---
Tradizionalmente avvio una sessione di [Angband](http://rephial.org) quando esce una nuova versione e avrei dedicato volentieri qualche momento ferragostano alla bisogna, solo che in questi giorni accendo solo iPad, per il quale non esiste una versione giocabile.

Allora ho cercato succedanei. Il consiglio di quest’anno è certamente [Cataclysm: Dark Days Ahead](https://itunes.apple.com/it/app/cataclysm-dark-days-ahead/id1017236109?l=en&mt=8), ambientato in un allucinato e straniante dopobomba. Ho provato ad avviare il gioco senza leggere le istruzioni e in capo a pochi minuti virtuali il mio personaggio veniva sbranato dagli zombi.

*Cataclysm* è un mondo vero, che richiede tempo e impegno per sopravvivervi. Più leggero è sicuramente [Brogue](http://www.roguebasin.com/index.php?title=Brogue), una variante di [NetHack](https://www.nethack.org/) che su iPad è un capolavoro di esperienza grafica e giocabilità. L’impegno per arrivare in fondo al mitico ventiseiesimo livello, tuttavia, è consistente.

Tutto gratis, tutto leggero, scaricabile con poco. Se qualcosa contribuirà ad animare anche per pochi minuti il giorno di festa, ne sarò solo contento.

Paradossale augurare buon Ferragosto con l’immaginarsi dentro un cunicolo a combattere i mostri più disparati, ma neanche tanto. L’idea è che alla fine prevarrà il gusto del gioco sulla pigrizia. E avremo vinto anche se perdiamo.

Buona festa e grazie per essere qui!
