---
title: "Il virus del sapere"
date: 2020-08-31
comments: true
tags: [Coronavirus, Covid-19, Immuni, Pepp-Pt]
---
All’inizio si sono levate voci critiche su certe opportunità di uso della mascherina. Oggi sappiamo che protegge gli altri, protegge in qualche misura anche chi la indossa e che è fondamentale per ridurre il rischio.

Sempre all’inizio sono stato [fortemente critico verso Immuni](https://www.macintelligence.org/blog/2020/04/20/ciao-pepp-pt/) per via delle premesse su cui si avviava a basarsi. Per fortuna ha prevalso il buonsenso ed è diventata magari [la peggiore delle applicazioni da installare](https://macintelligence.org/blog/2020/06/02/la-peggiore-applicazione-da-installare/), ma da installare.

Se le voci critiche sulla mascherina si basavano su nozioni discutibili, su Immuni c’è stato un tiro a segno notevole. Si è sparsa la bufala della sua inutilità nel caso non fosse stata installata almeno dal sessanta percento degli utilizzatori. Come sempre, [si è partiti da un grano di verità per diffondere una bugia](https://www.technologyreview.com/2020/06/05/1002775/covid-apps-effective-at-less-than-60-percent-download/).

Poi la polemica sulle [poche segnalazioni generate da Immuni](https://www.lagazzettadelmezzogiorno.it/news/home/1230174/immuni-non-decolla-zero-segnalazioni-dallapp.html), come se il suo scopo fosse produrre segnalazioni. Il suo scopo è evidenziare situazioni di rischio; se il virus venisse battuto e Immuni desse zero segnalazioni, saremmo tutti a brindare. Se ne fa poche, è perché il rischio è poco e speriamo che duri così, anzi, che migliori invece di peggiorare.

Su Facebook circolano anche personaggi enigmatici secondo cui il fatto che Immuni venga popolata su base volontaria delle segnalazioni di positività vuol dire che la app non serve a niente. Il diritto alla privacy sanitaria è sacro ancora più di tutti gli altri diritti di privacy e Immuni, per fortuna, lo tutela. In certe situazioni questo può farci arrabbiare, ma è giusto così.

Ci sono naturalmente gli [ostaggi di Immuni](https://www.lagazzettadelmezzogiorno.it/news/bari/1230324/bari-prigioniera-di-immuni-costretta-alla-quarantena-dall-app-ma-non-mi-fanno-il-tampone.html). Purtroppo il quantitativo di intelligenza che possiamo sperare di ritrovare nella macchina statale è limitato; non è colpa di Immuni, però.

Abbiamo anche i critici della possibile imprecisione del calcolo dei contatti a rischio effettuato via Bluetooth, che di per sé nasce per fare tutt’altre cose. [Apple](https://developer.apple.com/documentation/exposurenotification/enexposureconfiguration) e [Google](https://developers.google.com/android/exposure-notifications/ble-attenuation-overview) sembrano fare da questo punto di vista il possibile. Può darsi che il sistema non sia perfetto, ma nessuno lo è. Tamponi e sierologici vari producono falsi negativi e falsi positivi; più la diffusione del contagio è bassa, più [il numero di esiti falsi tende ad aumentare][https://www.scientificamerican.com/article/what-covid-19-antibody-tests-can-and-cannot-tell-us/]. La sicurezza al cento percento, in ambito coronavirus, non esiste, men che meno su Immuni. C’è da farsene una ragione.

Intanto iOS e Android si apprestano a fare diventare [la parte passiva di Immuni una funzione di sistema](https://www.computerworld.com/article/3572941/apple-makes-covid-19-exposure-notifications-an-ios-feature.html); per sapere se siamo stati a contatto con un soggetto positivo, a partire da iOS 13.7 non servirà alcuna app. L’efficacia del tracciamento aumenterà ed è una cosa buona. La funzione va attivata volontariamente e la parte attiva di Immuni richiederà sempre Immuni, quindi chi vuole – con scarso senso civico ma a buon diritto personale – restarne fuori potrà continuare a farlo.

Immuni non è la soluzione; è un anello della catena. Ce la faremo solo con una catena completa e solida. Ci dà però più conoscenza a livello personale, che sta a noi trattare come meglio crediamo verso famiglia, amici, colleghi. Moltiplicato per tutti, è sapere collettivo che ci aiuta a contenere la pandemia e ancora più a sperare per il meglio.
