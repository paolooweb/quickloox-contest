---
title: "La linea di comando"
date: 2020-05-05
comments: true
tags: [Apple, Google, coronavirus, MacRumors, iOS, Android]
---
*MacRumors* [aggiorna sul lavoro congiunto di Apple e Google](https://www.macrumors.com/2020/05/04/apple-google-covid-19-app-resources/) per inserire in iOS a Android una infrastruttura di sistema utilizzabile da applicazioni fidate addette al *contact tracing*. La pubblicazione ufficiale è sempre più vicina e ora sappiamo di alcune condizioni al contorno:

* Le uniche app ammesse sono quelle create da o per conto di un governo.
* Le app devono chiedere il permesso all’utilizzatore prima di accedere all’infrastruttura.
* Le app devono chiedere il permesso all’utilizzatore prima di condividere il risultato positivo di un test con le autorità sanitarie.
* Le app dovrebbero raccogliere il minor quantitativo possibile di dati.
* Le app possono usare i dati raccolti solo per contrastare la pandemia. Ogni altro utilizzo, compresa la pubblicità mirata, è proibito.
* Le app non possono accedere ai servizi di sistema per la geolocalizzazione.
* Solo una app per nazione potrà accedere all’infrastruttura, salvo laddove un Paese opti ufficialmente per un approccio locale al problema.

Ogni tanto qualcuno si fa avanti a sostenere che le app di tracciamento contatti siano una fatica inutile. Se anche dessero risultato zero, avrebbero mostrato validamente quali soggetti, nel mondo globalizzato, ha senso stiano in cima alla linea di comando. Indizio: sono quelli nati con la riga di comando.
