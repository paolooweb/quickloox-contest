---
title: "A scuola di privacy"
date: 2019-07-23
comments: true
tags: [Hesse, Office365, cloud, Google, Apple, Microsoft, Pages, Word, Write]
---
Due parole sulla [messa al bando di Office365](https://winbuzzer.com/2019/07/09/german-privacy-watchdog-microsofts-office-365-cannot-be-used-in-public-schools-xcxwbn/) nelle scuole tedesche.

Prima di tutto, la cosa vale solo per lo Stato federale di Hesse, uno dei sedici che formano la Germania, per un bacino di circa sei milioni di abitanti.

Poi è stato messo in chiaro che il problema riguarda l’oscurità delle multinazionali rispetto al trattamento dei dati nel cloud e riguarda chiunque altri abbia funzioni cloud nella propria offerta, vale a dire – per dire – anche Google e Apple.

I problemi sono altri due.

Primo, è stato dato come notizia che il browser di Microsoft invia alla casa madre tutti gli indirizzi che vengono visitati. Non è una notizia: [è un comportamento noto da quattordici anni](https://twitter.com/ericlaw/status/1152933704198758401). Questo, tuttavia, non fuga i dubbi su Microsoft, semmai li rinfocola. Perché tanta e tale è la massa delle informazioni disponibili che per una persona normale dirimere se, come e quando corra un rischio di privacy è diventato impossibile, persino nei casi in cui tutto è documentato e messo in chiaro.

<blockquote class="twitter-tweet" data-lang="en"><p lang="en" dir="ltr">Twelve years later, people are just now discovering how SmartScreen works, as if it&#39;s not all been documented the whole time. 🤔</p>&mdash; Eric Lawrence 🎻 (@ericlaw) <a href="https://twitter.com/ericlaw/status/1152933704198758401">July 21, 2019</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script> 

Non è più sufficiente, metterlo in chiaro; se l’utente non viene avvisato *prima* di che cosa sta succedendo, in modo chiaro e inequivocabile, e non gli viene data la possibilità di uscire pulitamente e immediatamente dalla situazione, sono solo chiacchiere vuote. Giustificare una funzione di spia con la motivazione che chiunque può leggersi quelle migliaia di pagine di supporto dove è scritto tutto, è disonesto, punto. Questa è una situazione in cui l’azienda è disonesta nonostante tutte le documentazioni del mondo; che gli URL della tua navigazione sono noti altrove deve essere chiaro ed evidente, in sette parole, nel momento in cui questo sta per accadere, e l’utente deve poter fare un clic semplice e chiaro per dire *voglio che non succeda, adesso e in futuro*.

Il secondo problema è che la scuola, in un mondo ordinato secondo leggi rette, appartiene al software libero e viceversa. Usare Pages a scuola è solo marginalmente più sano che usare Google Write, a sua volta solo marginalmente più sano di Word. A scuola non serve software proprietario; se anche docenti e uffici riescono a lavorare così male (o a scambiarsi favori sottobanco) fino a trovarsi costretti a farlo, per i ragazzi proprio è inutile, dannoso, stupido e controproducente. Se mi sta leggendo qualche scuola e non sa da dove partire, l’Italia è piena di persone che possono spiegare e mostrare di persona come si fa, me compreso (in ultima posizione).

I ragazzi devono lavorare con il testo puro e i linguaggi di marcatura. Altrimenti non imparano un mestiere, ma accumulano un debito. Più che formativo, finanziario: si troveranno incatenati a un Office per il resto della loro vita. Liberiamoli. Fa bene anche alla privacy.