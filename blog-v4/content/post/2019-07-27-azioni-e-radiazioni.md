---
title: "Azioni e radiazioni"
date: 2019-07-27
comments: true
tags: [5G, Wi-Fi]
---
Sono faticamente arrivato a una paciosa indifferenza nei confronti di quanti scrivono cose indicibili sul Wi-Fi e sui presunti effetti nocivi che dovrebbe avere non si capisce come e non si capisce perché. Li lascio pascolare e ruminare in santa pace, mia e loro.

Con il 5G, invece, la follia è appena cominciata. Se esiste un metro di giudizio scientifico e utilizzabile come riferimento, è la [classificazione della International Agency for Research on Cancer](https://monographs.iarc.fr/agents-classified-by-the-iarc/), che fa capo alla World Health Organization delle Nazioni Unite.

Prima di preoccuparci delle radiazioni delle antenne, bisogna pensare ai raggi solari. E a qualche altra decina di minacce.

Un amichevole avvertimento per quanti vogliono sparare stupidaggini in merito a 5G: mi scaldo in fretta e molto. Le radiazioni, no, non c’entrano.
