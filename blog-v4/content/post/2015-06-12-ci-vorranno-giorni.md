---
title: "Ci vorranno giorni"
date: 2015-06-13
comments: true
tags: [Ford, Bloomberg, code, programmazione]
---
Tale Paul Ford, per me (colpevolmente) illustre sconosciuto, ha scritto su *Bloomberg* un trattato, veramente un trattato, intitolato [Che cos’è il codice](http://www.bloomberg.com/graphics/2015-paul-ford-what-is-code/).<!--more-->

È un vero trattato. Non so quanto ci metterò a finirlo, immagino giorni. Non so neanche tutto quello che c’è dentro: finora è una introduzione alla programmazione, una introduzione all’informatica, una descrizione del mestiere del programmatore, una analisi sociologica del mondo della programmazione, un’inchiesta sulle logiche delle software house, una panoramica dei linguaggi più graditi ai programmatori e perché.

Sono neanche a un quarto del percorso. È una cosa che non dovrei avere il tempo di leggere ma lo troverò e così spero di chi sta leggendo ora queste righe.