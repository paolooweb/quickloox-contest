---
title: "Nessuno lo saprà"
date: 2019-01-16
comments: true
tags: [Maps, Apple, DuckDuckGo, privacy]
---
A DuckDuckGo, il motore di ricerca alternativo che protegge la privacy di chi lo consulta, hanno deciso di [appoggiarsi alle Mappe di Apple per cercare luoghi e locali](https://spreadprivacy.com/duckduckgo-apple-mapkit-js/).

All’insegna della massima privacy: Apple non riceve i dati della ricerca, né li ricevono terzi qualsiasi. Appena usati i dati necessari a soddisfare la ricerca, DuckDuckGo li dimentica. Nessuno oltre a noi saprà in che ristorante volevamo andare questa sera.

Sarà un caso che Apple sia l’unica multinazionale della tecnologia con un forte ed effettivo impegno a rispettare la privacy di chi usa i suoi prodotti e trovi una sinergia con DuckDuckGo? No, non lo è. E probabilmente tra cinque anni avrà fatto una differenza notevole nel valore di quello che vende, a prescindere dal prezzo.

Grazie a [Massimo](https://www.dotcoma.it) per la segnalazione!