---
title: "Di che preoccuparsi"
date: 2015-01-18
comments: true
tags: [Yarow, BusinessInsider, Apple, Android, profitti, mercato, Walkley, Canaccord, AppleInsider, smartphone]
---
Secondo Jay Yarow di *Business Insider*, in Apple dovrebbero suonare da qualche mese i campanelli d’allarme: [è sulla strada per essere assolutamente incenerita da Android](http://www.businessinsider.com/apple-needs-to-reinvent-the-iphone-business-2013-8), che ad agosto controllava circa l’80 percento del mercato degli *smartphone*.<!--more-->

Secondo l’analista Michael Walkley di Canaccord Genuity come lo cita Apple Insider, a novembre [Apple intascava l’86 percento di tutti i profitti](http://appleinsider.com/articles/14/11/04/apple-continues-to-dominate-with-massive-86-share-of-handset-industry-profits) del mercato degli *smartphone*.

Hanno tutti da preoccuparsi di qualcosa. Ho l’impressione che ci si preoccupi troppo di cifre che da sole dicono niente e che Android, tutto mercato e niente profitti, abbia da preoccuparsi un pochino di più.