---
title: "Chiacchiere e concept"
date: 2020-01-10
comments: true
tags: [iPhone, Jobs, Gruber, Darlymple, Loop, Ces, Kontra]
---
Ieri si ricordava la [presentazione del primo iPhone da parte di Steve Jobs](https://www.youtube.com/watch?v=vN4U5FqrOdQ), nel 2007. Ricorda Jim Darlymple:

>13 anni fa ero nel pubblico. “Sapevamo” che si sarebbe chiamato iPhone, ma non avevamo idea del suo aspetto. Dire che ci ha fatto scoppiare la testa sarebbe una sottovalutazione.

Si ricorda ancora quella presentazione perché è stata storica, nel senso che entrerà nei libri di storia, perché la vita dell’umanità è cambiata e se non basta guardarsi attorno tredici anni dopo, c’è qualcosa che non va in chi guarda. Difficile mettere in discussione che sia stato un evento unico.

Ecco perché il meme che vuole Apple presentare una stregoneria l’anno, altrimenti ha perso la capacità di innovare, è una sciocchezza. Non si cambia il mondo una volta l’anno.

Controprova? Avanti veloce al Ces 2020, dopo che Jobs eclissò qualsiasi cosa fosse apparsa al Ces 2007 (qualcuno ricorda un qualunque annuncio del Ces 2007, senza barare?). John Gruber lo ha riassunto in un unico articolo: [Il Concept Electronics Show](https://daringfireball.net/2020/01/concept_electronics_show).

Lungo pezzo da leggere, nel quale si dà conto di come praticamente tutti, nel tentativo di stupire e colpire, abbiamo presentato *concetti*, idee o prototipi per i quali manca una data di arrivo sul mercato. Prodotti che potrebbero non esserlo mai.

C’è dentro un bell’aneddoto su Jobs che insieme a Jonathan Ive presenta agli ingegneri il prototipo del [primo iMac](https://everymac.com/systems/apple/imac/specs/imac_ab.html) e loro gli elencano trentotto ragioni per cui non si può fare, al che lui risponde *no, no, faremo questo, perché sono l’amministratore delegato e penso che si possa fare*. Chiosa Gruber:

>Progettare ai limiti delle possibilità è una cosa; progettare sganciati dalla realtà è un’altra.

C’è anche il richiamo a un altro pezzo, vecchio più di dieci anni, sul perché Apple non presenta *concept* (e in passato li ha presentati, quando mica per niente andava malissimo). Con una legge da ricordare:

>L’abilità di innovare commercialmente di un’azienda è inversamente proporzionale alla sua tendenza a pubblicare idee di prodotto.

Tutti quelli che pontificano sulla capacità di Apple di innovare potrebbero riguardarsi Jobs, che mostra lo scorrimento sullo schermo *touch* fatto con le dita e strappa l’applauso. Il pubblico vide usare un telefono con le dita e applaudì, perché era innovazione vera. A pensarci oggi è incredibile, vero?

Intanto Apple avrebbe perso la capacità di innovare, gli altri pubblicano idee, concetti, prototipi. Niente che sia reale, niente che possa essere ordinato o preordinato. Chi si contenta gode, in maniera più che mai virtuale.

<iframe width="560" height="315" src="https://www.youtube.com/embed/vN4U5FqrOdQ" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>