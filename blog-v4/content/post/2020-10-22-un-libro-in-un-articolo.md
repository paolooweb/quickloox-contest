---
title: "Un libro in un articolo"
date: 2020-10-22
comments: true
tags: [Gödel, Escher, Bach, Turing]
---
Fuori tema per un giorno: [Gödel, Escher, Bach: un’Eterna Ghirlanda Brillante](https://www.adelphi.it/libro/9788845907555) è stato uno dei miei libri formativi, di cui raccomando la lettura a chiunque, sempre: è materia trasversale, non importa che cosa uno abbia studiato e, in larga parte, nemmeno se lo abbia fatto. È più importante essere curiosi, porsi domande. Costa 21,85 euro, con un bimestre di Netflix puoi espandere la tua conoscenza per anni, è quasi Natale, non dico altro.

Certamente è un libro: lungo, denso. Difficile trovare una pagina su cui sorvolare o priva di un gioco di parole, un concetto complesso dietro una spiegazione semplice, un rimando impegnativo.

La lettura di [Mente contro macchina: un corollario filosofico del teorema di Gödel sull’incompletezza](https://medium.com/cantors-paradise/mind-vs-machine-the-philosophical-corollary-from-the-incompleteness-theorem-fdbbb0f05205) è gratuita e dura otto minuti. Il titolo non deve spaventare: l’articolo è perfettamente alla portata di una persona capace di leggere questo post. L’inglese dell’articolo è molto scientifico, ordinato e asciutto: per riuscire ad arrivare in fondo bastano una infarinatura e un dizionario a portata di mano.

L’articolo riesce a riassumere buona parte della tematica principale di *Gödel, Escher, Bach*. Incredibile, ma vero. Straordinario.

Arriva da [Cantor’s Paradise](https://cantorsparadise.substack.com), una newsletter veramente di livello per chi ama il genere, accessibile al neofita (altrimenti sarei morto) e però per niente banale.

Magari andrebbe segnalato a Douglas Hofstadter, l’autore di *Gödel, Escher, Bach*. Chissà se a settantacinque anni è ancora attivo.

*The Atlantic* gli ha dedicato un [bellissimo ritratto](https://www.theatlantic.com/magazine/archive/2013/11/the-man-who-would-teach-machines-to-think/309529/), molto più lungo dell’articolo di cui sopra. Lì, sessantottenne, mostra una visione lucida dell’inganno collettivo attuale a proposito dell’intelligenza artificiale, confusa da moltissimi con l’apprendimento meccanizzato, con conseguenze negative a tutti i livelli, compreso quello lavorativo.

Il suo lavoro sarà ricordato a lungo.