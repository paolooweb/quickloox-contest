---
title: "E in cambio pretendete"
date: 2015-04-20
comments: true
tags: [watch, Schiller, ITProPortal, Ahrendts, Ive, Schiller, Newson, Gaber]
---
Assai poco interessante articolo su *ITProPortal*: [Apple ammette la sconfitta a seguito del fiasco dei preordini sullo smartwatch](http://www.itproportal.com/2015/04/16/apple-admits-defeat-following-smartwatch-pre-order-fiasco/).<!--more-->

Ho cercato di capire. *Ammettere la sconfitta* sarebbe un *memo* di [Angela Ahrendts](http://www.apple.com/pr/bios/angela-ahrendts.html), responsabile degli Apple Store, ai dipendenti, nel quale gli stessi [vengono complimentati e ringraziati per il lancio di watch](http://www.igen.fr/apple-watch/2015/04/angela-ahrendts-pas-ou-peu-dapple-watch-en-boutique-le-24-avril-90785), aggiungendo che l’interesse è abbastanza alto e la disponibilità di pezzi abbastanza bassa da venderlo solamente online per le prossime settimane.

Il *fiasco* si riferisce suppongo a vendite scarse. Se non fosse che Apple non ha comunicato cifre; circolano stime tra [poco più di un milione](http://intelligence.slice.com/first-apple-watch-data-one-just-isnt-enough/) e [2,3 milioni](http://www.macrumors.com/2015/04/15/apple-watch-2-3-million-pre-orders/) di pezzi; il tutto fa riferimento a un lancio avvenuto in pochissime nazioni, con disponibilità di pezzi limitata, di un [prodotto](http://www.apple.com/it/watch/) che ha senso acquistare solo se già in possesso di un iPhone nuovo o quasi e che bisogna aspettare settimane per avere.

Non sembra neanche che Apple abbia tutta questa fretta di vendere a tutti i costi: [Jonathan Ive](https://www.apple.com/pr/bios/jonathan-ive.html) (responsabile del *design* Apple), [Marc Newson](http://www.marc-newson.com/Default.aspx) (*designer*) e [Phil Schiller](http://www.apple.com/pr/bios/philip-w-schiller.html) (responsabile marketing Apple) si sono [impegnati al Salone del Mobile di Milano](https://instagram.com/188e76/), anche se l’Italia non figura tra i Paesi della prima fascia e non ha ancora una data certa di disponibilità.

L’unica spiegazione che trovo al titolo risuona nelle [parole](http://www.math.unipd.it/~baldan/IoSeFossiDio.html) del milanese illustre Giorgio Gaber:

>Compagni giornalisti avete troppa sete<br />
e non sapete approfittare delle libertà che avete,<br />
avete ancora la libertà di pensare<br />
ma quello non lo fate<br />
e in cambio pretendete la libertà di scrivere…