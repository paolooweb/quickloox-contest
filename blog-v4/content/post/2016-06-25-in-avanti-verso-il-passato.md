---
title: "In avanti verso il passato"
date: 2016-06-25
comments: true
tags: [jack, Bbc, iPhone, Sony, floppy]
---
Il mondo è bello perché è vario: riferisce Bbc che si possono trovare trecentomila svalvolati disposti a [firmare una petizione a favore nel mantenimento del *jack* audio su iPhone 7](http://www.bbc.com/news/technology-36606220), scarsamente consapevoli dei fatti che nessuno sa veramente come sia fatto un iPhone 7 e, ove anche iPhone 7 eliminasse il *jack*, la [petizione](https://action.sumofus.org/a/iphone-headphone-jack/) poteva magari essere utile un paio di anni fa, al momento della progettazione, e non certo a pochi mesi dalla produzione di massa, quando qualsiasi modifica di questa portata è del tutto impossibile a meno di comportare ritardi epocali.

Sono pure svalvolati con poca memoria e che non leggono neppure Bbc:

>Nel 1998 [Apple] presentò iMac G3 senza lettore di floppy disk, lasciando molti nello scetticismo rispetto alla opportunità di questa decisione.

>Poco più di dieci anni dopo, Sony – pioniera del floppy disk – annunciò la fine della produzione.

Scommetto che a frugare con sufficiente pazienza nell’internet di quasi vent’anni fa, una petizione contro la fine del floppy disk la si trova.

*[Un libro sul mondo Apple ben fatto può aiutare a capire e valutare le scelte hardware e software, quando sono azzeccate e quando no. Specie se è [più di un libro](https://macintelligence.org/posts/2016-06-15-un-kickstarter-per-cambiare-il-libro/), con aggiornamenti periodici, contatto con gli autori, richieste personalizzate, edizioni su misura. Akko [ha lanciato un progetto allo scopo](https://www.kickstarter.com/projects/295108589/cuore-di-mela-i-segreti-di-apple-iphone-e-mac), che ha bisogno di essere fatto conoscere e finanziato. Chi ci sta?]*