---
title: "Aggiornato un papa, se ne fa un altro"
date: 2016-03-16
comments: true
tags: [Textastic, Editorial]
---
[Textastic](http://www.textasticapp.com/) è appena uscito con un aggiornamento spettacolare contenente una marea di modifiche utili e interessanti che non mi servono, come la compatibilità con iPad Pro.<!--more-->

Da sempre ho solo parlato bene del programma e posso solo raccomandarlo a qualcuno che ora cerca un eccellente editor di testo e codice. Costa 9,99 euro e li stravale.

In occasione della [reinstallazione](https://macintelligence.org/posts/2016-03-13-ricomincio-da-nove/) *ex novo* di iPad ho tuttavia deciso di adottare un altro pezzo da novanta, [Editorial](http://omz-software.com/editorial/), prezzo identico.

Nell’immediato perdo la capacità di autocompletare l’Html; nel breve e medio termine vorrei approfittare delle eccezionali capacità di automazione di Editorial, per il quale esiste una comunità che sviluppa *workflow* in grado di estendere le possibilità del programma.

Su iOS è importante trasformare in efficienza le modalità semplici di uso di iPad, che la mancanza di automazione porta erroneamente a considerare limitazioni. Qui Editorial prevale per l’appunto grazie ai *workflow* e al *browser* integrato, che abbrevia i passaggi per il web.

Anche la tastiera a schermo, come prima impressione, sembra più funzionale in Editorial.

Parlando di *workflow*, ne ho scaricato uno del bravissimo Federico Viticci che [recupera velocemente il prezzo di una *app* e crea automaticamente un *link*](http://www.editorial-workflows.com/workflow/5796072928575488/87CW4oubwJo). Alcune parti del *workflow* non mi servivano o le volevo leggermente diverse: modificarlo è stato facile.

Ho comprato da tempo il [libro](https://itunes.apple.com/it/book/writing-on-ipad-text-automation/id697865620?l=en&mt=11) di Federico sull’automazione di Editorial e questo sarà il pretesto giusto per metterlo a frutto.

Il sogno nel cassetto è riuscire ad automatizzare una serie di interazioni con vari sistemi di pubblicazione su Internet, segnatamente Wordpress. Lungo e difficile, ma impossibile mai.