---
title: "Del doman la biometria"
date: 2020-04-09
comments: true
tags: [Face, ID, Touch]
---
All’ennesima messa in discussione del riconoscimento facciale come buona pratica di sicurezza personale, in tempi di [sedicenti mascherine con la propria faccia stampata](https://faceidmasks.com) (sedicenti perché non ci sono ancora) e di [performance artistiche che metterebbero in discussione l’intera tecnologia](https://www.technologyreview.com/2020/02/29/905599/how-coronavirus-turned-the-dystopian-joke-of-faceid-masks-into-a-reality/), mi permetto di fare notare che la sicurezza non è mai stata una faccenda del cento percento, né mai lo sarà. Piuttosto, si passa da sistemi sicuri a sistemi ancora più sicuri, nel tempo. C’è una evoluzione. Le tecnologie si avvicendano.

La sicurezza, inoltre, invecchia. Quando è arrivato Face ID, molti hanno obiettato che si trattasse di un sistema poco pratico e che [Touch ID fosse migliore](https://www.reddit.com/r/iphone/comments/9n71mo/opinion_touchid_is_better_than_faceid_no_matter/).

Sono trascorsi anni e Face ID, che già in partenza ha una sicurezza di [un rischio su un milione di tentativi](https://support.apple.com/en-us/HT208108) e cioè venti volte meglio di Touch ID, è soltanto migliorato. Mentre le impronte digitali hanno continuato a studiarle; viene fuori che, ad attaccarli come si deve, [i sensori di impronte digitali cedono quattro volte su cinque](https://blog.talosintelligence.com/2020/04/fingerprint-research.html).

Anche quelli di un iPhone non nuovissimo, che una volta – quando quell’iPhone era modello di punta – avrebbe resistito alla grande.

L’esperienza di Face ID con la mascherina può essere controversa (i racconti divergono, c’è chi addestra nuovamente il sistema, chi rinuncia, chi [addestra il sistema con metà maschera](https://9to5mac.com/2020/04/08/iphone-how-to-use-face-id-with-mask/) sperando di farsi identificare sia con la maschera che senza), ma il bisogno della mascherina passerà mentre il riconoscimento biometrico rimane, a oggi, come il migliore per il mercato di massa.

Anche Face ID comunque invecchierà e verrà sostituito da qualcos’altro. Non è un difetto della tecnologia, è la tecnologia.

*Quant’è bella giovinezza*<br />
*che si fugge, tuttavia*<br />
*anche per la biometria*<br />
*del doman non v’è certezza.*