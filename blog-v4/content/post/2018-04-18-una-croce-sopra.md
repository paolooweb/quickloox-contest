---
title: "Una croce sopra"
date: 2018-04-18
comments: true
tags: [iPhone, Android, Counterpoint]
---
In attesa dei risultati finanziari di Apple attesi per il primo maggio, una ricerca di Counterpoint asserisce, [scrive Cnbc](https://www.cnbctv18.com/telecom/iphone-x-pocketed-35-of-total-handset-industry-profits-in-q4-2017-37151.htm), che iPhone abbia assorbito l’ottantasei percento dei profitti dell’intero mercato dei computer da tasca durante il trimestre natalizio. Fino a qui [siamo abituati](https://macintelligence.org/posts/2017-02-10-non-ce-ne-per-tutti/).

Più originale è il risultato ottenuto dal solo iPhone X: ventuno percento del fatturato totale del mercato e il il trentacinque percento dei profitti sempre totali.

Sono numeri fuori da ogni consuetudine. Cnbc li sintetizza in una statistica disarmante:

>iPhone X ha generato cinque volte i profitti di oltre seicento produttori di Android.

Ho la sensazione che Apple, se questi dati anche solo si avvicinano alla verità, abbia il fermo proposito di vendere un sacco di iPhone X e quindi farli con cura, progettarli senza economie, metterci dentro tutta l’intelligenza e il talento di cui dispone. La ricompensa potenziale è evidente.

Quei seicento produttori che guadagnano poco o nulla e che alla fine neanche il pubblico conosce, quanta motivazione hanno per fare bene sul loro prossimo modello? Avere davanti cento concorrenti invece di duecento, e continuare a guadagnare poco o nulla?

Io ci metterei una croce sopra. O una X.