---
title: "Pensare differente"
date: 2018-03-08
comments: true
tags: [Drang, HomePad]
---
Come si fa a pensare differente, ora che il 1996 è trascorso e Apple si avvia a capitalizzare un trilione di dollari?

È Dr. Drang a suggerire la strada. Si prenda HomePod e la lamentela riguardante l’impossibilità di impostare più di un timer, come invece consente la concorrenza.

La soluzione è pensare differente. E [usare i Promemoria](http://leancrew.com/all-this/2018/02/friendly-reminders/). Semplice, efficace.

E se l’affollamento dei Promemoria sembra poco elegante, la soluzione lo è maggiormente: [cancellare quelli vecchi](http://leancrew.com/all-this/2017/04/cleaning-out-old-reminders/). Con un AppleScript, che alla concorrenza manca. Pensare differente al quadrato.

Dr. Drang, quando occorre, critica Apple e i suoi prodotti spesso e volentieri. D’altra parte, quando scrive

>gli utilizzatori Apple dovrebbero essere abituati all’idea che l’azienda ha opinioni forti riguardo al giusto modo di usare i suoi prodotti e di solito conviene assecondare il sistema,

spiega in tre righe cose che certa gente fatica a capire da trent’anni.

Invece che farsi guidare da un prodotto concorrente, estrarre il massimo dal proprio prodotto, in modi sconosciuti alla concorrenza e che sollecitano l’ingegno e la creatività. Pensare differente, al cubo.
