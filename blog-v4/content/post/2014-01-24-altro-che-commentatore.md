---
title: "Altro che commentatore"
date: 2014-01-25
comments: true
tags: [MelaBit]
---
Uno dei commentatori più assidui di questo blog ha finalmente deciso di darsi da fare e ha creato [MelaBit](http://melabit.wordpress.com).<!--more-->

Ci si trova l’approfondimento che arriva dalla mentalità scientifica, una abilità tecnica non comune, uno sguardo su orizzonti del web che raramente si può trovare altrove, italiano eccellente ed esposizioni forse a volte un po’ lunghe, ma sempre precise, coerenti e diritte al punto.

Spero che sia uno stimolo ulteriore per quelli che vogliono sfruttare meglio le capacità mostruose dei Mac e affini che si ritrovano sotto le mani e hanno solo necessità di quella spintarella per vincere la paura di dare ordini diretti invece che passare attraverso la benevola mediazione di una *app*.

Auguri non se ne fanno mai; preferisco dire *ancora un altro post, per favore*!