---
title: "Quando è troppo"
date: 2019-11-05
comments: true
tags: [Xiaomi, Apple, iPhone, CC9, Pro]
---
Apple presenta la serie 11 di iPhone con enfasi sulle funzioni di fotografia computazionale e sui nuovi obiettivi.

Coro di mugugni perché oramai si è persa l’innovazione, sono gadget inutili, comunque non scatteranno mai bene come una vera macchina fotografica, [l’obiettivo sporge](https://bgr.com/2019/09/17/iphone-11-pro-camera-design-has-a-specific-purpose-review-says/) e rovina l’estetica eccetera.

Segue [satira](https://www.euronews.com/2019/09/10/twitter-users-mock-iphone-11-pro-s-triple-camera-compare-n1052086) (segno certo del successo) sulla disposizione dei tre obiettivi.

È appena uscito in Cina [CC9 Pro di Xiaomi](https://www.engadget.com/2019/11/05/xiaomi-cc9-pro-108-megapixel-smartphone/), con una modalità di scatto da centootto megapixel e quattro obiettivi, che effettivamente sono incolonnati, ma sporgono ugualmente.

Nell’annuncio di Engadget si parla solo ed esclusivamente di foto, pixel, megapixel, lenti e sì, una batteria *gigantesca*. Un po’ come capita all’iPhone 11 Pro Max.

Sembra esserci una possibilità che CC9 Pro esca dalla Cina. Vediamo che cosa scriveranno. Probabilmente che iPhone ha perso la corsa all’innovazione.

Si faceva così anche una volta, con i megahertz dei processori, venduti al chilo. Oggi sono i megapixel. Certo, con centootto megapixel puoi stampare su un foglio di quattro metri. Routine quotidiana.