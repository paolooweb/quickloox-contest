---
title: "Epifanie e confronti"
date: 2017-01-06
comments: true
tags: [Mac, Surface, Book, Pro]
---
Scrive Bambi Brannan [su Mac360](http://mac360.com/2017/01/microsofts-surface-studio-boondoggle/):

>Un Surface Book con un terabyte di Ssd, processore i7 dual-core, schermo touch da 13,5”, Windows 10 Pro, 16 gigabyte di Ram e una scheda grafica integrata costa 3.199 dollari.

>Un MacBook Pro con un terabyte di Ssd, processore i7 quad-core, schermo 15”, Touch Bar, Touch ID, macOS Sierra, 16 gigabyte di Ram e scheda grafica da due gigabyte costa 3.399 dollari.

>Duecento dollari in più vogliono dire un MacBook Pro con un processore più potente, grafica più performante, uno schermo più grande, la possibilità di eseguire Windows o Linux o altri sistemi operativi Unix, [con Touch Bar ma] senza schermo touch […] sono modelli di fascia alta, di dimensioni similari, professionali, con grande potenza a disposizione, ma solo uno venderà numeri abbastanza consistenti da poter essere dichiarati.

Certi confronti sono inaspettati e arrivano come epifanie.

Adesso uno può riflettere sul trattamento riservato mediamente dalla stampa a Surface Book e quello ricevuto da MacBook Pro.