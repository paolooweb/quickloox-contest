---
title: "Sistematizzazioni storiche"
date: 2021-08-05T00:57:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [Alphonse Eylenburg, PlayStation, Unix] 
---
Giù il cappello davanti a questa [cronologia dei sistemi operativi degli ultimi settant’anni](https://eylenburg.github.io/os_familytree.htm).

Attualmente lo schema, che per forza di cose non può essere completo né mai lo sarà, mostra la bellezza di ottocentotrenta sistemi operativi.

La maggior parte sono meteore sconosciute persino a sé stesse; molti sono scomparsi prima di arrivare a oggi; e poi si vede la storia vera dell’informatica.

Le curiosità che è possibile appagare, previo averne in merito ovviamente, sono abbondanti e le cose che si scopriranno saranno in certi casi una sorpresa, per esempio da chi abbia preso diciamo ispirazione il sistema operativo della PlayStation.

Inoltre si capisce bene quale sia stato (e sia) il ruolo di Unix in questi decenni. Nei libri di storia verrà annoverato tra le invenzioni fondamentali nella storia dell’uomo, creazione senza la quale l’informatica sarebbe stata completamente diversa e probabilmente assai meno interessante.

Vorrei dire tante altre cose, ma ora torno a guardarmi il diagramma infinito per scoprire ancora qualcosa.

*Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile.*