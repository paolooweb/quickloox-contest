---
title: "L’assillo dell'ora"
date: 2015-03-14
comments: true
tags: [watch, iPhone, Bluetooth]
---
C’è grande attenzione sulla durata della batteria di iWatch e credo che dipenda dall’attenzione che si è finito per porre sulla batteria di iPhone. Telefonate, foto, video, Internet, *app*, Bluetooth, *push* e alla fine l’energia non basta mai.<!--more-->

Credo peraltro che iPhone abbia perpetuato anche comportamenti ossessivi che tanti utilizzatori di cellulari già avevano. Giornate passate al telefono, decine di Sms quotidiani, dipendenza dalla Sim. Non c’era bisogno di iPhone per sollecitare al massimo l’uso del cellulare.

Non vedo le persone consultare ossessivamente l’orologio. E le [durate della batteria di watch](http://www.apple.com/it/watch/battery.html) dichiarate da Apple presuppongono l’ossessione.

Più che i risultati vanno lette le condizioni dei test: 90 controlli dell’ora, 90 notifiche, sei ore e mezzo di musica Bluetooth, controllo dell’ora cinque volte ogni ora e così via.

Ci sarà sicuramente chi riuscirà a esagerare anche con watch. Però uno che guarda l’ora ogni dodici minuti, tutto il giorno, non ha bisogno di una batteria più potente. Gli serve una terapia.