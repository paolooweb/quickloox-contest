---
title: "L’anello mancante"
date: 2020-10-06
comments: true
tags: [Homebrew, ffmpeg, Handbrake, Terminale, Unix]
---
Quel filmato .mov forse andrebbe convertito in Mpeg-4 per avere la sicurezza di una compatibilità totale. Solo che per motivi misteriosi non trovo [Handbrake](https://handbrake.fr) sul disco.

Decido di farne a meno.

trovo in tre secondi un comando da dare a [ffmpeg](https://ffmpeg.org) nel Terminale. Non trovo ffmpeg, ma possiedo l’anello mancante, quello che connette il mio primitivo uso dell’interfaccia grafica all’evoluta filosofia di Unix: [Homebrew](https://brew.sh).

Senza l’anello mancante rimarrei al palo. In pochi minuti HomeBrew dirime decine e decine di dipendenze, pacchetti accessori, pulizie autunnali, aggiorna perfino [Mutt](http://www.mutt.org) che non c’entra niente, manca solo che mi ordini una pizza.

Posso dare finalmente il comando trovato in rete e, *voilà*, la conversione è pronta.

Non userò mai più Handbrake (ottimo programma, peraltro). Continuerò a usare Homebrew.
