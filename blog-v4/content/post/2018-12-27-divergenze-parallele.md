---
title: "Divergenze parallele"
date: 2018-12-27
comments: true
tags: [Skylake, A12, iPad, iPhone, A12X, Lemire]
---
Anche se nessuno ha il coraggio di dirlo apertamente per esigenze di mercato, i processori Arm finiranno per soppiantare quelli Intel in più di una situazione. Certo non completamente, però in modo tale che avere un processore Intel a bordo di qualsiasi cosa sarà una informazione assolutamente non scontata.

È più questione di quando, che di se. Si vede nell’evoluzione dei chip che Apple progetta su misura per gli apparecchi iOS. L’attuale A12, che è appunto la dodicesima iterazione del progetto, sul singolo compito si presenta praticamente alla pari con gli Intel più aggiornati.

Il senso comune vuole che su più compiti, invece, la superiorità di Intel sia ancora notevole. Un Mac svolge durante l’uso una quantità di compiti enormemente superiore a quella che del chip di un iPhone, che mostra a schermo una cosa sola e dietro le quinte ne svolge, al più, poche.

È ora di allontanarsi dal senso comune, perché un *computer scientist* di nome Daniel Lemire ha pubblicato l’evidenza di una superiorità progressivamente crescente dei processori Arm di Apple contro gli Skylake Intel [quando il parallelismo a livello di memoria sale oltre certe soglie](https://lemire.me/blog/2018/11/13/memory-level-parallelism-intel-skylake-versus-apple-a12-a12x/). Il testo di Lemire è complicato e dà per scontate molte cose, ma i grafici non lasciano dubbi: quando un processore ha più richieste del sistema operativo da eseguire in parallelo, si comporta molto meglio se è un A12.

Manca ancora molto prima di piazzare un processore Arm in un Mac e avere i livelli prestazionali cui siamo abituati. La differenza è che anni fa si trattava di una cosa impensabile. Oggi ci si interroga sulla fattibilità. Domani si lavorerà sulle rifiniture necessarie per riuscirci.

Sarà un momento epocale in questa per ora breve storia dell’informatica planetaria.