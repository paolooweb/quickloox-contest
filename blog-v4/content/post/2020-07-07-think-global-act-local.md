---
title: "Think Global, Act Local"
date: 2020-07-07
comments: true
tags: [Gruber, Apole, Federighi, iPad, MacBook, Pro, iPhone, Joswiak, WhatsApp]
---
Il titolo riecheggia un vecchio slogan da West Coast ma, nel nuovo normale, sintetizza il trucco fondamentale per produrre ottimi podcast audio e video: registrare localmente in modo separato e indipendente dalla chiamata.

Lo spiega bene John Gruber per raccontare come ha prodotto una sua [chiacchierata video](https://daringfireball.net/2020/07/how_we_shot_the_talk_show_remote) con Craig Federighi e Greg Joz Joswiak di Apple.

Lui usava un MacBook Pro per la chiamata; Federighi e Joswiak, in luoghi diversi di Apple Park, i loro iPad Pro. Nessuno si è chiesto quale fosse un computer o quale no.

Il punto è che hanno registrato tutto, ciascuno, con iPhone. Due iPhone a testa in Apple, uno per Gruber.

iPhone. È costoso mettere in campo cinque iPhone. E invece cinque telecamere, come sarebbe accaduto dieci anni fa?

Alla fine sono risultati diciassette gigabyte di video, che Gruber ha affidato a uno studio di post-produzione fidato. È barare, lo ammette anche lui. Nel nuovo normale, tuttavia, il linguaggio video assume una preminenza che prima non aveva. Non dobbiamo essere tutti videomaker, ma avere dimestichezza con la materia e restare saldi sugli appoggi davanti a un montaggio o una dissolvenza, quello sì.

È anche qualcosa, sempre Gruber a dirlo, che può venire bene, molto bene, senza altro fuori da quanto raccontato. Ma ci vuole molto tempo.

Tempo che non ci è mai stato richiesto di avere nel mondo precedente e che ci toccherà considerare. Dov’è che ho già sentito queste cose?

Una generazione fa, quando la nuova normalità comprendeva il web. Ma come? Devo mettere tempo per scrivere su Internet? Ma non si è mai fatto!

Una generazione dopo, qual è l’azienda senza un sito, fosse anche un bieco WordPress? Chi non ha un account Facebook, o un blog, o uno spazietto anche trascurato su Tumblr? A chi non è toccato di mandare almeno un messaggio WhatsApp? Niente inganni, usare una app di comunicazione è fare web, per quanto surrettizio.

Tocca al video oggi. Testa nel guscio, finta di niente, speranza di passare la nottata senza essere notati, oppure teniamo conto subito del nuovo linguaggio per ricavarne vantaggi, anche solo relazionali, nel medio periodo?

Mi sono messo a leggere libri in inglese e a usare computer in inglese quando l’inglese nella mentalità comune, tutt’al più, era una scocciatura e una fonte di brutte figure o aneddoti sapidi a tema turismo. Senza cavarmela benino con l’inglese, oggi, certamente dovrei fare un altro lavoro.