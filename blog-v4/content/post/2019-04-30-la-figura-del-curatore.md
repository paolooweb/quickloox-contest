---
title: "La figura del curatore"
date: 2019-04-30
comments: true
tags: [Schnapple, Doom, Wolfenstein, Doom, Quake, Hackintosh, MDM, Xcode, AppleTV]
---
Leggi di Apple che sarebbe sotto accusa perché martella le app [che fanno uso di sistemi di Mobile Device Management](https://www.apple.com/newsroom/2019/04/the-facts-about-parental-control-apps/) per monitorare il comportamento di altri telefoni, quindi con strumenti di controllo a distanza (che è tutt’altra cosa). I più candidi lasciano intendere che la mossa [faccia sparire app scomode](https://www.engadget.com/2019/04/27/apple-clamp-down-on-screen-time-apps/) per la funzione di Screen Time di iOS. Il *New York Times* titola addirittura [Apple si accanisce sulle app che combattono la dipendenza da smartphone](https://www.nytimes.com/2019/04/27/technology/apple-screen-time-trackers.html). Che barba.

Tizio smarrisce il suo Apple Watch nell’oceano e [lo ritrova sei mesi dopo, funzionante](https://9to5mac.com/2019/04/25/apple-watch-lost-sea-found/). Che noia.

Invece: programmatore nota che i giochi *open source* di id Software pubblicati a suo tempo su iOS (*Doom* e *Wolfenstein 3D*) non funzionano più perché il loro codice è rimasto a trentadue bit, e [rimette a posto il codice](https://schnapple.com/idtech/). Già che c’è, abilita il codice anche per Apple TV e poi ripete la procedura per una manciata di altre edizioni di *Doom*, *Wolfenstein* e *Quake*.

Notizia superinteressante perché solleva una marea di questioni. Elenco disordinatamente il materiale per la riflessione:

* Il programmatore fa per lavoro tutt’altra cosa. Ci si è messo per hobby.
* Il sito lavoro non si traduce automaticamente nella (ri)pubblicazione su App Store, perché in alcuni casi è stato sistemato il motore, ma servono i file grafici originali, ossia bisogna possedere il programma e bisogna provvedere in proprio alla compilazione.
* Anche se volesse, il programmatore non può (ri)pubblicare i giochi stessi, dato che il *copryright* resta a id anche se il codice è aperto.
* Il tema della curatela e della manutenzione del software di valore (inutile borbottare, *Doom* è parte della nostra storia e cultura) è sempre più all’ordine del giorno. Servono curatori.

In questo specifico caso, chiunque di noi potrebbe diventare curatore, armato di Xcode e di pazienza. Riportare al funzionamento un software che ha bisogno di essere aggiornato ha più sapore, come sfida, che [costruire un Hackintosh largamente fine a se stesso](https://macintelligence.org/posts/2019-04-29-il-tunnel-della-sfida/).
