---
title: "Quando è troppo"
date: 2018-08-20
comments: true
tags: [Apple, iPhone]
---
A che servono le *fake news* quando abbiamo gli analisti? Questo ha deciso che il titolo Apple perderà terreno [a causa del successo eccessivo di iPhone X](https://www.cnbc.com/2018/08/20/apple-shares-downgraded-because-the-iphone-x-is-too-popular.html).

Ne avrebbero venduti troppi, al punto di penalizzare la comanda futura.

Sì, è lo stesso universo nel quale si scriveva delle vendite di iPhone C [sotto le aspettative](https://macintelligence.org/posts/2018-02-03-perseverare/).

Aspettative che probabilmente erano di non venderne nemmeno uno. In base alla logica dell’analista, questo avrebbe creato opportunità clamorose di vendita futura e quindi, grazie alle vendite assenti, la quotazione delle azioni sarebbe decollata.

Il Paese delle meraviglie di Lewis Carroll, a confronto, ha la pragmaticità di un corso per la patente europea del computer.
