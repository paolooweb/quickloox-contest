---
title: "Smart vacationing"
date: 2023-09-10T00:00:13+01:00
draft: false
toc: false
comments: true
categories: [Internet, Software]
tags: [Alto’s Adventure, Tailscale, Brogue, Prompt, Easypark, Chiara Valerio, Valerio, La tecnologia è religione, kOoLiNuS, CarPlay]
---
Termina ufficialmente oggi un periodo sperimentale che mi è piaciuto molto. Se ci arrovella sullo *smart working*, perché non dovrebbero essere smart anche le vacanze?

Così non abbiamo fissato ferie o loro equivalenti. Abbiamo trascorso in località turistiche l’intero intervallo di chiusura delle scuole e ho portato con me l’ufficio. Dopo di che ho lavorato di sera quando tutti dormivano, o in giornate di pioggia, o per soddisfare un meeting oppure una scadenza. Nel contempo ho fatto niente quando ne valeva la pena, per le gite, per stare con la famiglia, per mangiare fuori, quando non era propriamente necessario.

Il risultato: la sperimentazione è positiva. Ho seguito le linee di business dove serviva, ho trascorso tanto tempo con la famiglia, ci siamo regalati del buon tempo e non ci sono state corse per arrivare *entro il* a completare tutto il lavoro.

La medaglia ha avuto naturalmente il suo rovescio: ci sono stati giorni senza vedere la spiaggia, riunioni che hanno richiesto la levataccia, dover spostare una pizza per onorare una scadenza. Il bilancio complessivo però sembra positivo. Almeno: sono tornato molto rilassato e tranquillo, con il sonno giusto, la famiglia serena.

Rimettersi sulla routine comandata dei tempi dettati dalla scuola e dagli uffici è impegnativo. Riposizionare la sveglia per la mattina, tornare a fissare obiettivi quotidiani, tutte cose dimenticate per settanta giorni. Lo rifarei, però.

Menzioni tecnologiche d’onore: [Tailscale](https://macintelligence.org/posts/2023-04-13-un-giorno-di-ordinaria-connessione/), che salda il legame di rete tra gli apparecchi e dalla spiaggia mette a disposizione un Mac di volta in volta distante minuti, o chilometri.

[Prompt](https://macintelligence.org/posts/2022-09-23-el-terminalo/), essenziale per correre sulle connessioni di Tailscale.

[CarPlay](https://macintelligence.org/posts/2023-07-04-guida-alluso/), una scoperta. Ora, quando sono senza, mi manca.

[Alto’s Adventure - Remastered](https://apps.apple.com/us/app/altos-adventure-remastered/id1576663233), gioco comune per stare tutti insieme anche se usiamo i *device*, alla faccia dei pedagoghi da clickbait. (La secondogenita, cinque anni, mi massacra).

[Brogue](https://macintelligence.org/posts/2020-06-18-fuga-per-la-sconfitta/), la sfida dell’estate e oltre. Perché tra imprecisioni, sbadataggini e sprovvedutezza non sono andato oltre il quindicesimo livello. Però si impara.

[Easypark](https://macintelligence.org/posts/2023-07-21-dimostrazioni-di-piazzuola/), che ha risolto al novantanove percento i parcheggi nell’epoca dei totem a bordo strada.

Il totem uno percento, che mi ha spiegato l’uso dei pulsanti colorati solo con l’interfaccia in francese (pulsanti che funzionavano solo con l’interfaccia francese).

[La tecnologia è religione](https://www.einaudi.it/catalogo-libri/problemi-contemporanei/la-tecnologia-e-religione-chiara-valerio-9788806251864/), lettura saggiamente propiziata da [kOoLiNuS](https://koolinus.net), per i punti di vista sempre originali e meritevoli di discussione, anche dove non condivisi. Un bel viatico per l’anno di scuola.

Mi si è creato un problema conclusivo: lavorare senza routine mi ha portato a delegittimare la routine ancora più di prima. Capisco sempre meno perché la società sia organizzata in questo modo. Niente da dire su tutto quello che era prima dell’informatica, ma dall’informatica di massa sono passati trent’anni e la mobilità totale ne ha più di quindici. I vecchi modelli avevano ragione d’essere. Non più.