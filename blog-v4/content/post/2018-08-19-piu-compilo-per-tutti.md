---
title: "Più compilo per tutti"
date: 2018-08-19
comments: true
tags: [Sneep, BBEdit, TeX]
---
Nuova edizione degli [script di Maarten Sneep per compilare agevolmente TeX dentro BBEdit](https://msneep.home.xs4all.nl/latex/)

Cose specialistiche che interessano pochi, ma guardiamo il contesto: un utilizzatore si accorge che potrebbe automatizzare certi comportamenti nel proprio interesse.

Lo fa grazie a tecnologie di sistema presenti nel computer (AppleScript) e approfitta della rete delle reti per mettere il suo lavoro a disposizione di chiunque.

Domani qualcuno potrebbe migliorare uno script, o aggiungerne uno nuovo,  localizzarli in una nuova lingua, dare un contributo.

Questo meccanismo è così promettente che avvilisce vedere Internet considerata una appendice di Facebook. Ritorniamo a guardare al computer come a un amplificatore di intelligenza.

C’è un sacco da fare e, nel grande schema delle cose, compilare TeX dentro BBEdit è molto meno *nerd* di quello che appare.
