---
title: "L’industria dell’attesa"
date: 2019-11-22
comments: true
tags: [Huawei, iPad, M6, M7, MediaPad, rumor, Mac, Blaas]
---
Guidato da un titolo promettente su *Android Central*, [Il prossimo tablet di Huawei è un iPad Pro con lo schermo bucato per la fotocamera](https://www.androidcentral.com/huawei-upcoming-android-tablet-ipad-pro-hole-punch-display), vado alla fonte originale, *un sito di rumor*, che titola [Le rese grafiche di MediaPad M7 mostrano il primo tablet con il buco nello schermo](https://www.91mobiles.com/hub/huawei-mediapad-m7-marx-design-render-exclusive/).

Il servizio, *esclusivo*, mostra non foto ma, appunto, *render* grafici, a risoluzione infima, con zero particolari significativi. Nel mondo di oggi è roba che potrebbe avere veramente fatto chiunque.

Sembra effettivamente un iPad Pro, giusto con lo schermo forato in corrispondenza della fotocamera, tastiera compresa. Ma è il meno. Lo *scoop* sarebbero queste viste, che in sostanza sono disegnini, solo 3D. Visto che il modello prima si chiamava MediaPad M6, si allude al fatto che questo potrebbe chiamarsi MediaPad M7. Uno scoppio di intuizione.

Ma no, aspetta, c’è una indiscrezione: Evan Vattelapesca dice che si chiamerà, rullo di tamburi, MatePad Pro. Una cosa che somiglia (eufemismo) a iPad Pro si chiamerà in modo somigliante a iPad Pro. L’informazione scorre potente in quest’uomo, che ha un [feed Twitter](https://twitter.com/evleaks) *privato*. Probabilmente, se riesci a entrare senza essere stato invitato, ti uccidono.

Nient’altro. Come data di uscita si vocifera il 2020. Ma va? Non lo annunciano a Santo Stefano?

L’industria dei *rumor* è passata da Mac ovviamente, vent’anni fa. Ma leggere oggi i *rumor* su Mac, rispetto a questa roba, è come villeggiare in un resort a cinque stelle.

Il nulla pneumatico e gente a occuparsene, palesemente distaccata, molto distaccata, dalle cose concrete. Sto scrivendo su un iPad Pro che esiste veramente e auguro a tutte queste persone di avere qualcosa, qualunque cosa, di cui occuparsi in concreto nel mondo materiale, anche fosse un porcellino d’India, invece che vivere di attesa e chiacchiere dalla fogna.
