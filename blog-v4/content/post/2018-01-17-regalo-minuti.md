---
title: "Regalo minuti"
date: 2018-01-17
comments: true
tags: [HappyScribe, Happy, Scribe]
---
A proposito del servizio di trascrizione [Happy Scribe](https://www.happyscribe.co) di cui [si è già parlato](http://www.macintelligence.org/blog/2017/10/24/felice-di-trascriverla/), ho appena ricevuto la comunicazione della loro nuova versione della classica iniziativa *porta un amico*.<!--more-->

Chi volesse registrarsi attraverso uno dei link sottostanti otterrà in omaggio trenta minuti di prova (a nove centesimi al minuto, è un valore di due euro e settanta). Il sottoscritto riceverà cento minuti (un valore di nove euro).

In serena onestà, sono rimasto molto soddisfatto del servizio di Happy Scribe. E negli ultimi tre mesi l’ho usato *una* volta.

A dire che, per il mio uso, i cento minuti sono irrilevanti. Ma per qualcuno i trenta potrebbero essere un esperimento interessante e magari si scopre un servizio che vale la pena e soddisfa una esigenza reale.

Inserisco qui sotto anche il testo originale della mail che ho ricevuto per chiarire ogni dubbio. Buone trascrizioni a chi vorrà provare.

§§§

*Hey,*

*I’m André, co-founder of Happy Scribe. Hope you had a great weekend. I’m bringing some good news to kick-off the week 😃 *

*To reward you for your feedback and help, we have released a referral model: Each time that one of your friends joins Happy Scribe, you'll get 100 minutes for free and your friend will get 30 minutes. 🎁*

*You just have to give the following link to your friends. We’ll notify you each time you earn 100 minutes to your account!*

*Link : <a href="https://www.happyscribe.co/r/ea9e8d9c0a https://www.happyscribe.co/r/ea9e8d9c0a?utm_source=email-ref https://www.happyscribe.co/r/ea9e8d9c0a">https://www.happyscribe.co/r/ea9e8d9c0a https://www.happyscribe.co/r/ea9e8d9c0a?utm_source=email-ref https://www.happyscribe.co/r/ea9e8d9c0a</a>*

*<a href="https://www.facebook.com/sharer/sharer.php?u=https://www.happyscribe.co/r/ea9e8d9c0a?utm_source=email-ref-fb">https://www.facebook.com/sharer/sharer.php?u=https://www.happyscribe.co/r/ea9e8d9c0a?utm_source=email-ref-fb</a>*

*<a href="https://twitter.com/home?status=Transcribe%20your%20interviews%20within%20minutes%20at%20https://www.happyscribe.co/r/ea9e8d9c0a?utm_source=email-ref-tw%20and%20get%2030%20minutes%20for%20free%20&#129321;%20via%20%40happy_scribe">https://twitter.com/home?status=Transcribe%20your%20interviews%20within%20minutes%20at%20https://www.happyscribe.co/r/ea9e8d9c0a?utm_source=email-ref-tw%20and%20get%2030%20minutes%20for%20free%20🤩%20via%20%40happy_scribe</a>*

*<a href="https://www.linkedin.com/shareArticle?mini=true&amp;url=https://www.happyscribe.co/r/[%code%20%7C%20Default%20Value%]?utm_source=email-ref-li&amp;title=&amp;summary=&amp;source=">https://www.linkedin.com/shareArticle?mini=true&url=https://www.happyscribe.co/r/[%code%20%7C%20Default%20Value%]?utm_source=email-ref-li&title=&summary=&source=</a>*

*Chat soon, 👋  👋*

*André Bastié*

*CEO & Co-Founder at Happy Scribe*

*PS: Don't hesitate to reply to this email if you have any question :)*

*André Bastié*

*DCU Alpha, Old Finglas Road, Dublin,  D11*

§§§