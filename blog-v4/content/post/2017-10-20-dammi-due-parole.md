---
title: "Dammi due parole"
date: 2017-10-20
comments: true
tags: [Siri, Machine, Learning, Journal, watch, iPhone, Gruber]
---
Affascinante spiegazione sul *Machine Learning Journal* di tutto quello che succede dietro le quinte e dentro il processore [quando il microfono capta il segnale Ehi Siri](https://machinelearning.apple.com/2017/10/01/hey-siri.html).

Solo due parole, ma occorre una unità dedicata che stia tutto il giorno a controllare se vengono pronunciate, che non si tratti di un falso allarme, nel rispetto della durata della batteria e con la velocità giusta per emulare una normale conversazione: obiettivi impegnativi nel programmare un iPhone, che diventano sfida quando c’è di mezzo un watch con risorse computazionali, a confronto, limitate.

Il commento più pertinente è quello di [John Gruber](https://daringfireball.net/linked/2017/10/19/apple-machine-learning-journal-hey-siri):

>Machine Learning Journal è la nuova Apple “aperta” al suo meglio.