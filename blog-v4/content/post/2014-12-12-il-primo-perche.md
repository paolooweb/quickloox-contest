---
title: "Il primo perché"
date: 2014-12-13
comments: true
tags: [Word, LibreOffice, LibreItalia]
---
Se ho accettato di entrare a fare parte del [Comitato Tecnico Scientifico di LibreItalia](https://macintelligence.org/posts/2014-11-30-italia-libre/), ci sono dei perché.<!--more-->						

Uno, molto banale. Mi è appena arrivato un file Word di media complessità, riguardante una richiesta di preventivo. Poca grafica ma presente, una tabella, impaginazione tranquilla, nessuna problematica vera. 40k.

Ho *aggiunto* le mie considerazioni aumentando la quantità di testo in materia significativa, senza intervenire sulla grafica o sulla struttura della tabella, con [LibreOffice](http://www.libreoffice.org). E ho rispedito il tutto. 22k.

Sarà un caso. Però apprezzo sempre un programma rispettoso del mio spazio su disco. Che ha, per quanto risibile, un costo.

Ne approfitto per fare campagna finanziamento e ricordare che l’[associazione a LibreItalia](http://www.libreitalia.it/diventa-socio/) costa cinque euro per gli studenti, dieci euro per i soci ordinari, cinquanta euro per i soci sostenitori. O almeno queste sono le quote per il 2014. Dieci euro per sostenere il software libero sono una somma ridicola per una causa più importante di quello che sembra. Tradotto: non vuoi rischiare di ricevere file Word per tutta la vita senza avere una alternativa valida.