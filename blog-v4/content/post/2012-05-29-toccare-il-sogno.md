---
title: "Toccare il sogno"
date: 2012-05-29
comments: true
tags: [tastiere]
---
Sono estremamente soddisfatto delle mie tastiere. È vero, il *feeling* della <cite>leggendaria</cite> <a href="http://lowendmac.com/thomas/06/1013.html">Apple Extended Keyboard</a> rimane tuttora inarrivabile. Eppure la tastiera integrata del mio MacBook Pro è resistente e precisa, la <a href="http://www.apple.com/it/keyboard/">Apple Wireless Keyboard</a> è ottima (le manca solo la retroilluminazione, a parafrasare un detto usato per i quadrupedi) e la tastiera software di iPad, ancora perfettibile, è comunque già molto adeguata.<!--more-->

Ciò detto, se non ci fosse mezzo la mancanza di un layout italiano, andrei dietro a un sogno impossibile ed estremamente concreto: la <a href="http://www.daskeyboard.com/model-s-ultimate/">Model S Ultimate Keyboard</a> di Das Keyboard.

Quella *senza serigrafie sui tasti*. Apparentemente per ricoverandi in clinica psichiatrica, quando in realtà – pagato il prezzo dell’apprendistato – <a href="http://www.daskeyboard.com/about-us/">velocizza la digitazione</a>.