---
title: "Cioccolata e nonne"
date: 2020-01-09
comments: true
tags: [iPad, iPhone]
---
È già successo in passato che mi trovassi in mezzo a uno sciopero del personale ferroviario e riuscissi a portare a casa la giornata nonostante i tempi persi e le attese incerte, grazie a portatili e connessione cellulare.

Si è sempre fatta un po’ di fatica, però. Un vago alone di impresa compiuta nonostante le difficoltà. Un pizzico di orgoglio supplementare per avercela fatta.

Ieri no. iPad Pro e iPhone X, un duo da paura. La giornata è interamente trascorsa senza che potessi capire se e quando avrei potuto rispettare gli appuntamenti della giornata ma sulle scadenze, nessun problema. iPad Pro è una macchina da produttività con un rapporto tra prestazioni, superficie di lavoro e peso che non so come sia possibile battere oggi (a parte il prossimo iPad Pro).

Ho preso un po’ di freddo, ma anche una sontuosa cioccolata con panna in una caffetteria persino munita di Wi-Fi. Alla fine il bilancio della giornata si è concluso con una quindicina di euro di spese impreviste, tra caffetteria, giornali, uno spuntino imprevisto. Poteva andare peggio.

Nonostante lo stato di agitazione altrui, il lavoro (che non mi posso permettere di fermare per il capriccio di chi dovrebbe fornire un servizio) è corso spedito. Confesso che forse mi sono anche impegnato più della media, come quando si vuole dimostrare qualcosa a qualcuno che non riesce a capirlo.

Sono un privilegiato e me la sono cavata con una cioccolata con panna. Ci sono invece persone disabili, malate, impossibilitate a lavorare a distanza, con bambini o anziani da accudire. A volte sono anziani che devono prendere un treno per una necessità.

Ho visto in biglietteria una nonna che aveva faticato ad arrivarci, la sua non è stata una passeggiata di piacere a zero gradi, sentirsi dire che non c’era modo di sapere quando sarebbe passato il prossimo treno, magari un’ora, magari due.

Quella nonna stanotte ha gravato sulla coscienza di un ignoto macchinista in sciopero. Non amo la gogna pubblica, ma se conoscessi il suo nome farei fatica a trattenermi.
