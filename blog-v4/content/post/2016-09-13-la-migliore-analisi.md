---
title: "La migliore analisi"
date: 2016-09-13
comments: true
tags: [Asymco, iPhone, watch, Prescott, Numbers, Dediu, iWork]
---
John Gruber su *Daring Fireball* [lo ha scritto prima](http://daringfireball.net/2016/09/courage), ma [non sono riuscito a leggerlo](https://macintelligence.org/posts/2016-09-10-beati-gli-ultimi/) e ho scritto comunque [una cosa analoga](https://macintelligence.org/posts/2016-09-11-basta-la-parola/), della quale sono lieto di potermi pavoneggiare per dieci secondi.<!--more-->

Gruber può consolarsi perché ha scritto una montagna di cose sulle quali non sono arrivato. La sua [analisi](http://daringfireball.net/2016/09/thoughts_and_observation_iphone_7_event), come spesso, è la cosa migliore da leggere per tirare le conclusioni definitive sulla presentazione di iPhone 7 e watch 2.

Due dettagli da sottolineare. Il primo è la menzione della *demo* di iWork collaborativo effettuata sulla presentazione in corso. Una cosa che farebbe tremare i polsi a qualsiasi presentatore cui sia capitato il *bello della diretta* e invece Susan Prescott ha messo in piedi e portato a casa senza timori.

Il secondo è l’aneddoto di Gruber seduto accento a Horace Dediu di [Asymco](http://www.asymco.com), che stava *modificando un foglio [Numbers](http://www.apple.com/it/numbers/) gigantesco*, dove erano visibili *almeno mezza dozzina di diagrammi e grafici style Asymco*. La cosa, conclude Gruber, *mi ha scosso per ragioni che non riesco a spiegarmi*.

Un giro sul sito di Asymco, un’occhiata ai grafici (e al tipo e alla quantità di dati richiesti per produrli), una riflessione: l’autore di questi grafici lavora su Numbers. Piccolo compito per la giornata. Giusto perché sembra che non ci sia vita fuori da Excel.