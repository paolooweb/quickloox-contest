---
title: "Mai fidarsi"
date: 2016-05-03
comments: true
tags: [Kernel, Daily, Dot, Mac, Plus]
---
Segno dei tempi (per rendere omaggio a Prince, diciamo Sign O’ the Times). *The Kernel* ha pubblicato un articolo proprio bello su [un Mac Plus collegato, per sfida, alla Internet di oggi](http://kernelmag.dailydot.com/issue-sections/features-issue-sections/16515/1986-mac-plus/).<!--more-->

È pari pari lo stesso articolo [pubblicato a dicembre 2013 su The Daily Dot](http://www.dailydot.com/opinion/mac-plus-introduce-modern-web/), di fatto la stessa testata. Ne avevo anche [accennato](https://macintelligence.org/posts/2013-12-13-retromomenti/) a suo tempo.

Meglio rilassarsi e ricordarsi dell’[articolo originale](http://www.keacher.com/1216/how-i-introduced-a-27-year-old-computer-to-the-web/). E farsi un promemoria: mai fidarsi, in rete. Mai.