---
title: "Leggi che ti passa"
date: 2017-10-26
comments: true
tags: [Apple, iBooks]
---
A seguito di una ricerca complicata ho finito per aprire su iBooks la [raccolta degli ebook pubblicati da Apple](https://itunes.apple.com/it/author/apple-inc/id405307759?l=en&mt=11).

Non posso dire di essere sorpreso; però bisogna averla davanti per crederci. Vorrei avere davanti quelli che lamentano la *mancanza del manuale* per dire loro che ce sono a centinaia, in millemila lingue compreso l’italiano, aggiornati.

Poi i testi sulla programmazione, Swift in prima fila.

E l’*education*, iTunes U, i programmi per gli insegnanti e le scuole. E chissà quant’altro, dato che è umanamente impossibile scorrere con attenzione l’elenco intero e sicuramente manca qualcosa.

Così tanto da imparare e giornate così corte.