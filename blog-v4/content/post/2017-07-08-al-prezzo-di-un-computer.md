---
title: "Al prezzo di un computer"
date: 2017-07-08
comments: true
tags: [iPhone, Red, Android]
---
Red ha annunciato per il 2018, posto che uscirà davvero, un [supertelefono Android](http://www.red.com/hydrogen) con edizioni in alluminio e titanio, al prezzo rispettivo di 1.195 e 1.595 dollari.<!--more-->

Jonh Gruber [prevede](https://daringfireball.net/linked/2017/07/03/kuo-iphone-2017-touch-id) che in autunno arriveranno un iPhone 7S, un iPhone 7S Plus e un iPhone Pro, e si augura che quest’ultimo costi almeno 1.500 dollari perché Apple sia libera di esercitare le proprie capacità tecnologiche fuori dai vincoli di prezzo.

Questo autunno spero di non ritrovarmi a leggere commenti lungimiranti tipo *non spendo mille euro per un telefono*, per queste ragioni:

* È ampiamente possibile spendere meno, anche in campo iPhone.
* Sono computer, da tasca. La funzione telefono occuperà sì e no il cinque percento del tempo di utilizzo.

La seconda ragione è quella che conta. Anno 2017, se ci si fa caso, la potenza di calcolo di un iPhone, di un iPad, di un MacBook è grosso modo identica.

Stupisce che computer con suppergiù la stessa potenza abbiano suppergiù lo stesso prezzo? A me no.

Che sono computer lo si diceva già nel 2007. Era più difficile farsi dare ascolto, certo.
