---
title: "Lavoro di gruppo"
date: 2015-04-06
comments: true
tags: [Lucimia, Unity5]
---
Può anche succedere che un gruppo di folli si metta a sviluppare un gioco di ruolo di massa nel tempo libero e che sia estremamente promettente. [Saga of Lucimia](http://sagaoflucimia.com) sfrutta direttamente [Unity 5](http://unity3d.com) e indirettamente il fatto che Internet permette collaborazione senza limiti di tempo e di spazio. Siamo al paradosso che hanno in essere una [raccolta fondi su Indiegogo](https://www.indiegogo.com/projects/the-saga-of-lucimia-indiegogo-campaign) però giusto per rientrare un po’ nei costi e perché così ha votato la comunità che li segue; il gioco lo lavorano comunque.<!--more-->

C’è di più. Liberi dal condizionamento commerciale, hanno compiuto scelte in controtendenza. *Saga of Lucimia* non conterrà contenuto da giocare da soli e obiettivi, esplorazioni, vittorie saranno unicamente conseguibili da gruppi di avventurieri, più o meno numerosi. Le esplorazioni richiederanno giorni, settimane, mesi di tempo per essere completate. Un sistema di [Camps & Caravans](http://sagaoflucimia.com/topic/camps-caravans-a-detailed-look/) permetterà ai giocatori che hanno poco tempo di seguire comunque il gruppo e trovarsi allineati con quelli che giocano di più. Non ci saranno minimappe nel gioco perché l’orientamento dovrà venire dalla capacità di guardarsi intorno e capire la direzione giusta.

Tolkieniani senza saperlo. Unico difetto: per ora le previsioni di pubblicazione sono solo Windows. Tuttavia le previsioni di fine lavoro sono per il 2017 e Unity 5 non dà problemi di compatibilità. Siccome è gente intelligente, scommetto che le scelte definitive lo saranno.