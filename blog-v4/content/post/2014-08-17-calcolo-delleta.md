---
title: "Calcolo dell’età"
date: 2014-08-18
comments: true
tags: [Apple, Ligthning, iPhone, iPhone5, iPhone, 5s, iPhone6]
---
Di questi giorni sono particolarmente sensibile a cose che nascono e ai conteggi. Contemporaneamente, per qualche giorno mi è richiesta la sintesi e i *post* potrebbero anche andare a singhiozzo. Ci scusiamo per il disagio eventualmente arrecato.<!--more-->

Vengo allora subito al punto: è nato il [connettore Usb reversibile](http://www.tripplite.com/product/Reversible-USB-Cables/1048), che si infila sempre dal verso giusto.

Se si ricorda che il connettore reversibile Lightning di Apple è nato – tra polemiche, era Apple – con iPhone 5, è già uscito iPhone 5s e si attende a breve iPhone 6 o quel che sarà, è abbastanza facile conteggiare gli anni di ritardo con cui il resto del mondo arranca dietro alla Mela. Almeno parlando di connettori.