---
title: "Niente, Panic"
date: 2024-01-20T17:24:05+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [Panic, Prompt 3, Prompt]
---
Questo post, iniziato su Mac con [BBEdit](https://www.barebones.com/products/bbedit/), verrà probabilmente completato su iPad con [Runestone](https://runestone.app) e poi inviato nuovamente a Mac, nella cartella da cui viene generato il blog, con Prompt.

Prompt permette la connessione SSH tra iPad e Mac, o qualunque altro sistema sulla galassia, e dispone di tutta una serie di accessori, come la sincronizzazione dei segnalibri tra apparecchi, che semplificano e rendono più snelle le procedure di connessione. Mi piace e lo ritengo una delle migliori opzioni possibili per fare SSH da iPad.

È uscito [Prompt 3](https://panic.com/prompt/) e l’istinto mi farebbe mettere mano al portafogli (leggi App Store). La ragione mi fa però guardare al modello di prezzo: abbonamento a ventinove e novantanove dollari al nome, oppure acquisto *lifetime* a novantanove virgola novantanove dollari.

Di recente ho [comprato a occhi chiusi BBEdit](https://macintelligence.org/posts/2024-01-11-e-quindici/), aggiornamento della versione quattordici alla quindici.

Se fossi arrivato da novizio, avrei dovuto spendere sessanta dollari. Invece, come proprietario della versione precedente, quaranta. Sono stato riconosciuto e premiato. Volessi valutare i cambiamenti, posso stare per un mese in *free mode* nella pienezza delle funzioni del programma.

A Panic non importa se io possiede Prompt e che versione. Escluso, l’abbonamento, vuole cento dollari, due volte e mezzo quello che mi è costato BBEdit. Se risparmio trentaciqnue copie di Prompt 3, metto insieme un Vision Pro.

Che cosa ricevo, in cambio, che non abbia già?

Panic per Mac. Grazie, ma ho un Terminale fatto e finito. E uso in *free mode* [Warp](https://www.warp.dev), una app alternativa dotata di una marea di funzioni, [cui ho già accennato](https://macintelligence.org/posts/2023-11-09-tra-industriale-e-artigianale/). Capisco i vantaggi della sincronizzazione, però onestamente il Terminale su Mac lo uso in tutt’altro modo. A essere cattivi, BBEdit comprende comandi di base per connessione SSH direttamente dal programma.

Mosh e Eternal Terminal. Servono a tenere in piedi per lungo tempo anche la peggiore delle connessioni. Utilissimo a un professionista; io ho bisogno di qualche secondo di connessione attiva.

Tastiera, temi grafici e tipografia personalizzabili. Questo è un bel punto a favore.

Panic Sync. Server, chiavi di accesso, password sincronizzati tra gli apparecchi. C’è già su Prompt 2.

Integrazione di FaceID e TouchID. Sicurezza in più, sempre utile. Però a me serve molto relativamente.

Jump Hosts. Accesso a server aziendali da remoto. Ottimo per chi ne ha bisogno. Non ne ho bisogno.

Emulazione migliorata. Vedi Jump Hosts. È una bella miglioria, che non mi riguarda.

Supporto del mouse. Forte! Davvero. Solo che il mio uso del mouse su iPad è sì e no di un’ora l’anno.

Icona per il dark mode. Non serve a niente, ma è uno dei tocchi di classe di Panic. Questa è fuori dalla valutazione.

Tirando le somme: mi si giustificano trenta dollari al mese di abbonamento? No. Ho già Prompt 2. E cento dollari *lifetime*?

Cavoli. Due volte e mezzo in più di una *lifetime* di BBEdit. BBEdit lo uso tutti i giorni, più ore al giorno. Uso Prompt tipicamente la sera, dal lettone, per raggiungere Mac da iPad e trasmettere un post. Diciamo cinque minuti al giorno.

Sinceramente, trenta dollari li avrei spesi senza dire una parola, metà per il programma e metà per sostenere Panic, che continua a essere una delle migliori software house indipendenti sulla piazza per Mac e iOS. Forse anche quaranta, più o meno come per l’aggiornamento a BBEdit. Cento? Non mi passa neanche per la testa. Non è questione di poterselo permettere, ma di volerselo permettere.

Questa politica di abbonamento, con acquisto alternativo a prezzi ingiustificati, pagherà per Panic come forse fa per altri? Glielo auguro e spero che navighino nel denaro. Io resto su Prompt 2, ma il problema è che ora sono aperto ad alternative. Ce ne sono.