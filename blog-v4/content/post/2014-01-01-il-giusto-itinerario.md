---
title: "Il giusto itinerario"
date: 2014-01-02
comments: true
tags: [namebench, Dns, DnsStuff, Terminale]
---
Un nuovo anno deve partire alla grande e nel modo più efficiente.<!--more-->

Alcuni comprano un’auto da duecentocinquanta chilometri l’ora per guidarla su strade dove si va in fila indiana e c’è un semaforo ogni duecentocinquanta metri.

Poi fanno i test, tipo accelerare a tavoletta in un tratto di autostrada libero da autovelox e sì, la macchina fa i duecentocinquanta. Com’è che da casa a ufficio ci si mette così tanto, allora?

Se si nota una somiglianza con quelli che acquistano una Adsl *ventimega* e poi passano la giornata a sacramentare davanti allo *speedtest* del momento, è voluta.

Tutto è più complicato di così. Dentro l’auto ci sono centinaia di componenti cruciali; la macchina più sfavillante deluderà comunque se l’itinerario era quello sbagliato.

Ore passate sullo *speedtest* e magari neanche un secondo a chiedersi se i Dns siano i migliori possibili. Significa che trattasi di intrattenimento personale e allora consiglio piuttosto di trovarsi amici su [lichess](http://it.lichess.org), delle tante alternative possibili.

Per capire la qualità dei propri Dns può aiutare [namebench](https://code.google.com/p/namebench/). Compatibile Mac, gratis, aperto. Nel giro di una decina di minuti effettua una serie di test e infine suggerisce una configurazione di Dns basata sui criteri del più veloce e del più vicino. A me consiglia per esempio di impostare il Dns primario di Google e solo dopo quelli di FastWeb, il mio *provider*.

È inutile ripetere ossessivamente il test ogni tre giorni e anzi fa peggio, perché i risultati verranno progressivamente falsati. In presenza di ossessione, alzare il numero delle *query*, le interrogazioni inviate ai vari Dns consultati. Aumenterà la lunghezza del test e probabilmente, leggermente, anche la precisione del risultato.

I messaggi dettagliati sono più allarmistici del dovuto e non devono allarmare, se non si sia preventivamente pasticciato molto da soli.

Dns sta per *Domain Name System*, sistema dei nomi di dominio, e indica il meccanismo che consente di scrivere *macintelligence.org* al posto di 174.34.167.172 e provvede ad associare il giusto numero IP con il giusto indirizzo umano del sito.

I numeri Dns si inseriscono nelle Preferenze di Sistema di Mac, voce Network, pulsante Avanzate, scheda Dns.

Per i curiosi sulla corrispondenza tra nome sito e numero IP c’è il comando `dig` del Terminale: per esempio `dig macworld.it`.

La pagina [DnsStuff](http://www.dnsstuff.com) fornisce tutti gli strumenti base per farcela da un *browser*.

Mai visto uno che se la prende con l’acquedotto municipale perché dalla canna del giardino esce un filo d’acqua e ignora il masso accidentalmente posato sul tubo di gomma? Ecco, con un pizzico di esagerazione è uno che trascura i Dns di sistema e fa uno *speedtest* dopo l’altro.