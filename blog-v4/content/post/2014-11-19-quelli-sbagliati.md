---
title: "Quelli sbagliati"
date: 2014-11-19
comments: true
tags: [Enya, Tuaw, Sande, OSX, Yosemite, iOS8]
---
*My life goes on in endless song / Above earth's lamentations, / I hear the real, though far-off hymn / That hails a new creation.*<br />
*(Enya, [How Can I Keep from Singing?](https://itunes.apple.com/it/album/how-can-i-keep-from-singing/id305695106?i=305695128&l=en))*

Ho chiesto il permesso di tradurre integralmente in italiano l’articolo [Dove sbaglio?](http://www.tuaw.com/2014/11/17/what-am-i-doing-wrong/?ncid=rss_truncated) di Steven Sande su *The Unofficial Apple Weblog*, perché una volta l’anno ci vuole.

Sono stato ignorato e quindi devo limitarmi alla citazione:

>A giudicare dai lamenti e dalle cattive vibrazioni che ho visto ultimamente sulle reti sociali, OS X Yosemite e iOS 8 sono fallimenti che stanno causando a chiunque un sacco di problemi. Se tre Mac con Yosemite e quattro apparecchi con iOS 8 funzionano bene per me e per mia moglie, dove sbaglio?

Sì, perché può capitare di avere un problema e non è detto che tutto funzioni sempre a meraviglia. Tuttavia, quando il problema riguarda solo la macchina personale e non tutto il mondo, è possibile che ci sia un problema locale. Invece si parte lancia in resta contro la qualità che non è più quella di un tempo (e non lo sarà mai, poiché si sente da quarant’anni).