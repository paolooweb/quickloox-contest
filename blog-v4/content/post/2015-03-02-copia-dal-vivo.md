---
title: "Copia dal morto"
date: 2015-03-02
comments: true
tags: [Samsung, iPhone, Galaxy, S5, S6, Edge, TheVerge]
---
Samsung ha un problema di mercato perché negli ultimi mesi le vendite non sono andate benissimo. Così ha studiato una soluzione per risollevarsi, come scrive [The Verge](http://www.theverge.com/2015/3/1/8128619/samsung-galaxy-s6-edge-announcement-mwc-2015):

>Mentre Samsung ha sicuramente migliorato in modo significativo il *design* di Galaxy S5 e nello stesso tempo ha aggiornato la tecnologia del telefono con S6, questo ha avuto un costo. Segni distintivi dei telefoni Samsung come batterie removibili, alloggiamenti per schede SD e impermeabilità non si troveranno su S6 o S6 Edge. […] Sono anche state tagliate le funzioni a disposizione; Samsung dichiara una riduzione del 40 percento su S6 rispetto a S5.<!--more-->

Capito? Samsung passa a modelli con batteria non removibile, senza schede SD e con meno funzioni. La loro soluzione è rivivere il 2007, la nascita di iPhone.

Poi uno legge titoli come [Gli iPhone conservano più a lungo il loro valore dei telefoni Samsung](http://www.cio.com/article/2882816/smartphones/iphones-retain-more-resale-value-than-samsung-handsets.html) e magari si chiede pure il perché.

La morale: premesso che *correlation is not causation*, si riscontra che – comunque – il telefono con batteria non sostituibile conserva meglio il suo valore di quello con batteria sostituibile.

Andiamo di lusso e tiriamone fuori una seconda, di morale: la copia dal vivo ha smesso di funzionargli e in Samsung ora pensano alla copia dal morto. iPhone del 2007.