---
title: "Sicuramente da leggere"
date: 2014-03-09
comments: true
tags: [Gotofail, iOS, sicurezza, TidBits, Nsa, Portachiavi, iCloud]
---
Da una parte Apple incappa in scivoloni di sicurezza spiacevoli, come [Gotofail](https://gotofail.com).<!--more-->

Dall’altra, pubblica documenti come il [resoconto sui sistemi di sicurezza in iOS](http://images.apple.com/iphone/business/docs/iOS_Security_Feb14.pdf) che fanno respirare aria fresca.

Ecco come [ne parla uno specialista su TibBits](http://tidbits.com/article/14557):

>Per la prima volta, disponiamo di dettagli estesi sulla sicurezza di iCloud. […] Assieme agli accorgimenti più evoluti che abbia mai visto, Apple ha messo a punto un sistema che rende impossibile a organizzazioni come la Nsa ottenere la password del nostro Portachiavi iCloud.

Importante: questo non è un messaggio di Apple, bensì di un esperto indipendente che ha zero ragioni di mentire e oltretutto, se lo facesse, potrebbe essere smentito da un altro esperto al lavoro sullo stesso documento.

Di cui raccomando la lettura, anche se è molto, molto tecnico e denso: anche capirne un decimo aiuta a rendersi conto di quale complessità stia dentro un iPhone, lato software.