---
title: "Due contro uno"
date: 2014-03-01
comments: true
tags: [Atom, Skype, Anteprima]
---
Uso Skype il meno possibile. Ultimamente un paio di amici legati al mezzo mi hanno coinvolto in *chat*. Pochi giorni dopo mi serviva un frammento della conversazione. A malincuore riaccendo Skype, che sta tanto bene spento, e sorpresa! Nel *log* compaiono solo le mie frasi.

Non voglio neanche sapere se sia una questione di configurazione. Se ne esiste una che consente questo, l’ha pensata un demente (informatico).

Invece uso molto Anteprima, perché leggo tanti Pdf. Una preferenza essenziale per me è quella che riapre il Pdf dove si era fermata l’ultima consultazione. Utilissima, a patto che il Pdf venga chiuso a mano. Quando si chiude direttamente Anteprima, il programma non rispetta l’impegno. *Feedback* da lasciare, per una imperfezione talmente minuscola che infastidisce concretamente.

Dove cercare soddisfazione alternativa in bel software? Mi ha acceso qualche speranza [Atom](http://atom.io), *editor* che quelli dietro [GitHub](https://github.com) stanno mettendo in piedi con grandi aspettative.

L’idea è valida in principio: prendere tutto quello che è stato sviluppato di straordinario per maneggiare testo e codice sul *browser* e farne una applicazione desktop. Se l’assemblaggio dei pezzi riesce bene, la potenza, la semplicità e la personalizzazione potrebbero risultare degni di nota.

Il contro è la possibilità teorica di portare sul desktop i problemi del web: di compatibilità, stabilità e velocità. Lo sapremo abbastanza presto perché è possibile richiedere l’invito alla versione beta, che quindi è vicina.

Intanto, una veloce osservazione sui forum di discussione mostra come l’uso di Mac tra gli sviluppatori della soluzione sia preponderante. Soprattutto, i pezzi di codice che compongono Atom sono *open source*, utilizzabili e migliorabili da chiunque, comunque.

Nell’attesa di rispondere alla domanda più cruciale di tutte – come si comporta Atom su iPad – un bel respiro: un altro progetto di software innovativo, che verrà adottato da programmatori e sviluppatori di altissimo livello, ai quali che Windows esista o scompaia fa assolutamente differenza zero.