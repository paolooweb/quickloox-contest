---
title: "Cortesie per gli hosting"
date: 2020-04-02
comments: true
tags: [MediaTemple, Linode]
---
La primissima fase della mia [transizione web](http://www.macintelligence.org/blog/2020/03/27/sic-transit-mediatemple/) da [MediaTemple](https://mediatemple.net) a [Linode](https://www.linode.com), da un ambiente di hosting a una macchina virtuale Linux, ha avuto successo: il blog è tornato visibile.

Ha voluto dire scegliere una *distro* Linux, configurare il networking, impostare Dns, attivare un firewall, configurare un server web, cambiare i privilegi di accesso di sezioni della macchina eccetera. Tutto via riga di comando, talvolta con [BBEdit](https://www.barebones.com/products/bbedit/bbedit13.html) in connessione remota, talvolta con [nano](https://www.nano-editor.org).

Niente di clamoroso; qualunque amministratore di sistema lo fa a memoria, da mattina a sera, con una mano sola e senza guardare.

Tuttavia amministratore di sistema non sono e nel mio caso c’erano diverse cose da imparare, più la scommessa che le avrei imparate.

la documentazione di Linode mi ha aiutato tantissimo; quella di MediaTemple è stata eccellente e questa forse ancora meglio, a tratti quasi solare per chiarezza anche verso un non troppo esperto.

L’unica parte dove ho stentato un po’ è la sezione di configurazione Dns, dove si danno per scontate varie nozioni che sono certamente di base, ma vanno intuite invece che banalmente apprese.

Non finisce qui, anzi, è appena iniziata. Ho da configurare almeno un altro dominio e forse più di uno, più il server di posta, più vari dettagli non secondari come la configurazione dell’indirizzo del blog con https invece che l’anacronistico http, non cifrato.

Imparare facendo continua a essere un ottimo esercizio.