---
title: "Floppy, nuvole e scimmie"
date: 2013-07-10
comments: true
tags: [Mac]
---
Il servizio di trasmissione video in *streaming* americano Netflix ha reso di pubblico dominio da qualche mese [Chaos Monkey](https://github.com/Netflix/SimianArmy/wiki/Chaos-Home) (scimmia caotica), un programma fatto per arrecare danni casuali al servizio *cloud* di *computing* virtuale che sostiene Netflix stesso… in modo che i programmatori capiscono dove stanno le debolezze del sistema e a lungo termine il servizio rimanga sempre in piedi, qualunque cosa succeda.<!--more-->

Può solo tornare alla mente il [segnalatore di sistema MonkeyLives](http://www.folklore.org/StoryView.py?project=Macintosh&story=Monkey_Lives.txt), datato da Andy Hertzfeld a ottobre 1983. Ne riporto i passi salienti.

>Il Macintosh originale aveva solo 128k di Ram (un ottavo di megabyte), per cui l’amministrazione della memoria era di solito la parte più difficile della scrittura di sistema operativo e applicazioni. 16k erano usati dal sistema, 22k dallo schermo monocromatico 512 x 342, quindi alle applicazioni restavano 90k circa.

>Steve Capps ebbe l’intuizione di usare una funzione di “journaling”, impiegata per generare eventi da mostrare nel floppy della visita guidata, a scopo di collaudo.

>The Monkey [la Scimmia] era un piccolo accessorio di scrivania che generava eventi casuali nel programma in funzione, così che Macintosh sembrava in mano a una scimmia velocissima e in qualche modo arrabbiata, che schiacciava mouse e tastiera completamente a caso. Capps diede maggiore significato semantico allo strumento, assegnando percentuali di utilizzo a comandi di menu, attività come spostamento delle finestre e altro.

>All’inizio la Scimmia mandava in crisi il sistema facilmente, ma presto risolvemmo tutti i bug più ovvi. Un buon test per una applicazione era sopravvivere a una notte di utilizzo da parte della Scimmia, anche se raramente durava più di venti minuti, perché la Scimmia finiva regolarmente per scegliere il comando Esci.

>Bill Atkinson ebbe l’idea di definire un segnalatore di sistema MonkeyLives [la Scimmia vive] che indicava il funzionamento della Scimmia e permetteva a MacPaint e altri programmi di disabilitare il comando di uscita. Così la Scimmia poteva andare avanti anche tutta la notte a collaudare il programma.

Trent’anni fa non c’era il *cloud* e non si potevano accendere infiniti computer virtuali a migliaia di chilometri di distanza come si può fare oggi. Però le cose fondamentali si erano già capite.