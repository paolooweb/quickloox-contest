---
title: "Toccare il fondo"
date: 2021-09-09T01:24:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [Harry McCracken, Arctic Adventure] 
---
Trovo tanta verità nella storia di Harry McCracken che quarant’anni fa pubblicò l’avventura testuale [Arctic Adventure](https://www.arctic81.com) e oggi, a seguito di una serie di coincidenze fortunate, la ripubblica via web dopo, ed è qui il punto, essersi reso conto di un errore di programmazione che rendeva il gioco originale impossibile da terminare.

Quattro decenni più tardi l’errore è stato emendato e dalla pagina linkata si può apprendere l’intera vicenda, ma anche provare il brivido (appropriato, pensando al titolo) di cimentarsi con Arctic Adventure.

Tanta verità perché il software è più di un listato o di un supporto e perché con una sola semplice mossa si può fare giustizia di tante chiacchiere sulla sopravvivenza culturale dei giochi del passato e sulla perenne questione dei supporti che non dureranno per sempre. Sicuro; ma chi se ne occupa attivamente può aspirare all’immortalità funzionale delle sue opere. Trasposta sul web, Arctic Adventure potrà vivere felice per sempre o quasi.

Sarà possibile perfino arrivare in fondo.

*Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile.*