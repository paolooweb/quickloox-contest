---
title: "Il futuro della preistoria"
date: 2010-09-06
comments: true
tags: [Ping, Unix, Web]
---
Giusto perch&#233; si discute di quanto sia indispensabile Flash e di come oggi le pagine web diventino applicazioni, faccio presente che Gmail si usa perfettamente con <a href="http://habilis.net/lynxlet/">Lynxlet</a>.<!--more-->

Gmail lo fa Google: l&#8217;azienda che si &#232; fatta un *browser* apposta per avere JavaScript superveloce, che mette a disposizione di tutti gli utenti Android un *kit* per costruire applicazioni, che investe in energie alternative e in tecniche avveniristiche di profilazione e di pubblicit&#224;.

Poche aziende sono pi&#249; &#8220;avanti&#8221; di Google. Ma la sua posta elettronica funziona anche con tecnologia anni sessanta.

A te che stai progettando un sito, pensaci. Ripensaci e pensaci ancora.