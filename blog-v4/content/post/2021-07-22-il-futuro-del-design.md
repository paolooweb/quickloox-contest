---
title: "Il futuro del design"
date: 2021-07-22T00:05:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [Giacomo Tufano, Programmatori per caso, IlTofa, Accademia di Belle Arti, Napoli, Html, Css, JavaScript] 
---
Studi all’Accademia di Belle Arti di Napoli e ti formi come designer, ma devi anche passare un esame di Web Design, perché il design del XXI secolo è anche questo.

Fortunatamente hai un docente geniale e capace che sa metterti a confronto con Html, Css e JavaScript.

Perché non mostrare a tutti il lavoro fatto, nella veste grafica pensata da ciascuno? Così nasce [Programmatori per caso](https://www.programmatoripercaso.it).

Ci sono lavori molto belli. Alcune cose richiedono magari Chrome o Firefox, altre sono magari pesantine, ma sono ragazzi che studiano e fanno ricerca. I loro peccati veniali di gioventù sono niente a confronto di certi web designer del mondo professionale, che evangelicamente filtrano il moscerino e fano inghiottire il cammello al cliente.

Nel guardare il frutto di quest’anno di studio, ricordiamo: sito statico, solo Html-Css-JavaScript. Si può fare ottimo web con semplicità e ottimo risultato, di interfaccia, di fruizione, di esperienza.

Ah: andategli a dire che i computer a scuola non contano, o che bisogna aborrire una lezione via rete se si rischia di perdere tempo di apprendimento. Andte a dire a un designer neodiplomato del 2021 che sono *nuove tecnologie*, trent’anni dopo che sono state create.

È indecente che la scuola italiana abbia per forza bisogno per salvarsi dei docenti di eccellenza che mettono in ombra le mediocrità. Ma è una gran fortuna che esistano docenti di eccellenza, per il presente di ragazzi che ambiscono giustamente a un futuro degno, da progettare con orgoglio.

*Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile.*