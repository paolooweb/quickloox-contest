---
title: "Fin da piccoli"
date: 2021-10-07T01:17:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [scuola, Schoolwork, Swift Playgrounds, Everyone Can Code] 
---
Il progetto di [curriculum digitale per elementari e medie](https://macintelligence.org/posts/Un-curricolo-per-lestate.html) prosegue sia pure in sordina e mi sarà utile in prospettiva questa [pubblicazione di risorse per il coding e il design di app nella scuola elementare](https://www.apple.com/newsroom/2021/10/apple-unveils-resources-for-elementary-school-coding-and-inclusive-app-design/), da parte di Apple.

C’è molto, tra l’ovvio [Swift Playgrounds](https://www.apple.com/swift/playgrounds/) e le meno ovvie migliorie a [Schoolwork](https://apps.apple.com/it/app/schoolwork/id1355112526), passando per la guida Everyone Can Code Early Learners.

Ma tutto quello che serve è contenuto all’inizio della dichiarazione di Susan Prescott, *vice president* per Education and Enterprise Marketing:

>Coding e progetto di app costituiscono alfabetizzazione essenziale.

A un quarantenne di oggi potrà sembrare assurdo che nella scuola debbano entrare coding e app design, almeno al pari delle materie da sempre neglette come musica e attività fisica.

Un quattrenne domani ci ringrazierà, perché gli farà un gran comodo e segnerà un divario forse incolmabile in qualità della vita e aspettativa di futuro rispetto a quanti non avranno avuto la stessa possibilità.

*Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile.*