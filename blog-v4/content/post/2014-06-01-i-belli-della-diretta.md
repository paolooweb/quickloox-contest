---
title: "I belli della diretta"
date: 2014-06-01
comments: true
tags: [Wwdc, Freesmug, Gand]
---
L’agenda mi tiene fuori casa quasi tutti i martedì sera e così tendo a perdere numerosi eventi Apple.<!--more-->

[Wwdc](https://developer.apple.com/wwdc/) però inizia domani e per giunta, anche se sono scettico sulla riuscita, c’è la [trasmissione ufficiale](https://www.apple.com/apple-events/june-2014/) in diretta del discorso di apertura.

Allora ci provo e do appuntamento, come altre volte, sul solito canale `#freesmug` del server `irc.freenode.net`, dando il comando `/join #freesmug` dentro un programma atto allo scopo, su uno dei numerosi computer a disposizione. Con gratitudine infinita verso [Gand](http://freesmug.org) e più che mai con l’invito a sostenere il software libero soprattutto quando è per Mac.

Chi ci sta troverà chiacchiere, commenti, sarcasmo, battute, digressioni e relax fino a che se ne ha voglia.

Sono i momenti più belli, in cui il quarto d’ora successivo potrebbe essere una grande sorpresa o una delusione grande o piccola. Se qualcuno vuole condividerli, domani lunedì 2 giugno alle 19 ora italiana (le dieci ora del Pacifico).