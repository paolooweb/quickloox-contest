---
title: "La carta della nostalgia"
date: 2017-07-16
comments: true
tags: [HyperCard, Archive.org]
---
I sempre più benemeriti di Archive.org hanno aggiunto un nuovo emulatore dei tempi andati alla loro crescente raccolta: non hanno solo un Mac, ma addirittura [HyperCard](http://osxdaily.com/2017/05/27/run-hypercard-macos-web-browser-emu/), usabile come fosse il 1987, dentro un comune *browser*.

Sostengo che l’esperienza vada assolutamente compiuta dato che HyperCard è stato un pezzo di software straordinario e geniale.

Ma non scriverò *in anticipo sui tempi* perché, quando i tempi sono arrivati, era rimasto indietro, spettacolare per la sua epoca ma di poco aiuto andando avanti.

Da provare, appunto. Semplicissimo, intuitivo al massimo… ma inadatto a sostenere la prova di Internet. Se uno ci pensa, HyperCard – con le pagine di *stack* pronti da lanciare – è un perfetto prototipo dell’interfaccia del primo iPhone. Da lì però si vuole entrare in una esperienza diversa.

E la programmazione. Sono un sostenitore di AppleScript e sicuramente la natura stupendamente modulare di HyperCard semplificava una marea di cose. Ma siamo sicuri che fosse così più semplice AppleScript di JavaScript? O di un Python?

E gli stack. Antesignani dei siti, certo. I quali, però, hanno tutt’altra flessibilità.

Teniamocelo stretto HyperCard e facciamo visite periodiche ad Archive.org per giocarci, per impararlo a memoria e mostrare agli archiviatori che vale la pena conservarlo con un occhio di riguardo. Occupa un posto importante nella nostra storia anche se non lo abbiamo mai usato.

Ciò detto, lanciamo Swift Playgrounds su un iPad con iOS 10 e facciamoci due domande, se non sia il caso di coltivare la nostalgia ma lavorare intanto sul progresso.