---
title: "Il satellite invisibile"
date: 2022-09-10T01:58:57+01:00
draft: false
toc: false
comments: true
categories: [Hardware, Software]
tags: [iPhone, iPhone 14, Huawei, Mate 50, Mate 50 Pro]
---
Dopo che [ho scritto](https://macintelligence.org/posts/2022-09-08-levento-invisibile/) *Sono curioso di vedere come faranno a copiarla su Android, questa funzione*, giustamente **macmomo** ha approfondito:

>Rispetto all'aiuto via satellite, qualche giorno fa leggevo [questo](https://www.wired.it/article/huawei-mate-50-scheda-prezzi/).
Sbaglio o è la stessa cosa?

La risposta a questa domanda dovrebbe essere *sì* o *no* e non sono riuscito a formularla.

**Wired** è categorica quanto lacunosa nei link alle fonti. In settembre 2021 (un anno fa!) un *rumor* sul social cinese Weibo affermava che [i Mate 50 Huawei avrebbero supportato comunicazione satellitare e che Apple non sarebbe stata pronta](https://en.gizchina.it/2021/09/huawei-mate-50-sms-comunicazione-via-satellite-beidou/).

Nel 2021 Apple non era pronta… ma nemmeno Huawei, visto che **Wired** dà per ufficiale la famiglia solo ora.

Ciò detto, gli articoli dell’anno scorso su Mate 50 parlano di [uso della connettività satellitare per comunicare via Sms](https://www.gizmochina.com/2021/09/03/huawei-mate-50-satellite-connectivity/). Lo spessore dell’articolo è inferiore a quello della carta velina e quindi è perfettamente inutile prenderlo alla lettera. Se comunicazione tra persone, non è la stessa cosa del servizio Apple, che è proprio diverso. Se servizio per messaggi di emergenza, allora è la stessa cosa.

Huawei ha fornito ben poco aiuto per chiarire la situazione, almeno a me. I siti Huawei in inglese che ho consultato ignorano del tutto la funzione, o l’hanno sottoevidenziata a sufficienza per farmela trascurare. Se ho capito bene, Huawei vende in Cina tramite il sito *Vmall*; sulla pagina dei Mate 50, [per quello che Google riesce a tradurre](https://www-vmall-com.translate.goog/product/10086862843804.html?_x_tr_sl=zh-CN&_x_tr_tl=en&_x_tr_hl=en&_x_tr_pto=wapp), il modello è in prevendita e la scheda manca di date di disponibilità.

Il punto di andare a sfruculiare nei siti cinesi è che, stando ai *rumor*, il servizio verrebbe erogato tramite la rete satellitare, appunto, cinese, [Beidou](http://en.beidou.gov.cn), con servizio disponibile solo lì. 

[Pandaily](https://pandaily.com/whats-behind-the-satellite-connectivity-on-huawei-mate-50-smartphones/) e [The Verge](https://www.theverge.com/2022/9/6/23339717/huawei-mate-50-pro-satellite-text-china-beidou) danno per certa la funzione sui Mate e aggiungono particolari interessanti su come la comunicazione satellitare sui computer da tasca diverrà pervasiva; si muovono diverse aziende sul lato satellitare e anche provider telefonici. Per *The Verge*, Mate 50 consente di inviare e non di ricevere messaggi; sul supporto di Apple si legge invece che potrebbe essere richiesto di [rispondere a messaggi aggiuntivi](https://support.apple.com/en-gb/HT213426) dopo avere lanciato un Sos.

Ecco, Apple ha una pagina di supporto chiara ed esaustiva. Su Huawei ho trovato solo dei si dice.

Mi rende perplesso il fatto che Apple dia grande evidenza alla funzione, presentata a lungo durante [l’evento dedicato a iPhone 14](https://macintelligence.org/posts/2022-09-08-levento-invisibile/) e in bella evidenza sulle pagine americane del sito (dove funziona il servizio, [erogato via GlobalStar](https://spacenews.com/apple-to-be-largest-user-of-globalstars-satellite-network-for-iphone-messaging/)). Huawei invece sembra dire nulla anche in Cina.

Rimane il fatto che Huawei è diverso da Android (utilizza un [sistema operativo diverso](https://consumer.huawei.com/en/harmonyos/)) e che di tutti gli altri produttori di computer da tasca so poco o nulla rispetto alla questione.

Rimane soprattutto il fatto che Apple afferma chiaramente di cifrare i messaggi che verranno mandati e su Huawei ho trovato zero a riguardo.

Quindi magari è la stessa cosa, che però non chiamerei *copia* perché qualcosa di diverso c’è.