---
title: "Un posto migliore"
date: 2021-06-22T12:44:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [AR, Apple, IlTofa, Pixar, Pixar Theory, SharePlay, Shazam, Spatial Audio, Wwdc] 
---
Faccio eccezione alle mie regole di condivisione di contenuti e rendo omaggio a **IlTofa** che lo ha appena rilanciato. Un articolo *semiserio*, ma proprio per questo da prendere con attenzione, sulla [costruzione di un *metaverso* da parte di Apple](https://www.codevoid.net/ruminations/2021/06/20/wwdc-2021-apple-metaverse-plain-sight.html). Costruzione che sarebbe in corso davanti ai nostri occhi a partire da Wwdc 2019 e che culminerebbe in un substrato di realtà aumentata sovrapposto (affiancato? innestato? apparentato? abbinato? accoppiato?) alla nostra realtà consueta.

La costruzione si comporrebbe di quattro livelli – esperienza, il metaverso in quanto tale, applicazioni, piattaforma – e il pensiero dell’autore svolge un bel lavoro tra il creativo e l’analitico, a definire Shazam *GPS per l’audio* e incasellare le novità di quest’anno, da SharePlay a Spatial Audio generalizzato, fino a Universal Control che *sembra* un sistema per pilotare un iPad dal mouse da iPhone e invece è il prodromo di una soluzione globale di pilotaggio di un insieme iPhone/watch⁄futuri occhiali Apple eccetera.

Guardare sempre con il classico grano di sale a queste teorizzazioni, perché a un certo livello è più facile crearne di quanto possa apparire. È con noi da sempre la [Pixar Theory](http://www.pixartheory.com), secondo la quale tutti i film Pixar si svolgono nello stesso universo.

Al tempo stesso, pensare che il [Mac](https://macintelligence.org/posts/I-Mac-mancanti.html) dei prossimi anni trenta potrebbe essere un substrato di realtà virtuale, beh, un pezzettino di innovazione la contempla, no?

*Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile.*