---
title: "Conteggi magici"
date: 2017-12-09
comments: true
tags: [bash, script, Pdf, Robservatory, Terminale, shell]
---
Script e automazione, i veri tesori sepolti dentro i computer che usiamo, spesso inconsapevolmente, per lavorare noi invece che fare lavorare loro. Non esiste altro patrimonio altrettanto sottovalutato.

Qui, uno [script bash](https://robservatory.com/revisiting-a-pdf-page-counting-script/) per contare le pagine dei Pdf presenti nelle cartelle selezionate, una o tutto il sistema.

Difficile. Certo, ma c’è valore. Non è che una cosa difficile abbia valore di per sé, ma questa, senza Terminale, come si fa?

Interessante perché mostra la potenza di alcuni comandi che possiamo anche eseguire a mano quando serve e che aspettano lì, nella finestra poco amica della *shell*.

Notevole nell’individuare le aree di miglioramento: lo script che impiegava due minuti a completarsi, ora impiega dieci secondi.

Se anche dovessero passare le vacanze a decifrare il senso di queste rune magiche, beh, sarebbero passate ad avvicinarci alla magia. Con la quale tutto è possibile.