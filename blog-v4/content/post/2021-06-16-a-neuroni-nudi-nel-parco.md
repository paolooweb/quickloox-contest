---
title: "A neuroni nudi nel parco"
date: 2021-06-16T01:02:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [Xcode, Swift Playgrounds, The Verge] 
---
Il premio *non ci voleva molto* 2021 viene assegnato a *The Verge* nella persona di Monica Chin, autrice dello *scoop* [Apple dice che ora puoi fare il build di una app su iPad, ma gli sviluppatori dicono che la realtà è diversa](https://www.theverge.com/2021/6/15/22534902/ipad-pro-apple-swift-playgrounds-4-wwdc-2021).

L’articolo è un capolavoro di *vorrei ma non capisco*. All’inizio ci aspettavamo una serie di novità per iPad che non sono arrivate (ognuno aveva la sua lista, come tutti gli anni); poi scopriamo che Swift Playgrounds consente di fare il build di una app e mandarlo su App Store (a smentire il titolo, costruito come se la premessa fosse una mezza bugia).

Allora viene fuori che le app finora si sono scritte con Xcode su Mac, solo che è molto complicato. Invece Swift Playgrounds è molto semplice.

Purtroppo, emerge l’amara realtà, Swift Playgrounds manca di tutta una serie di strumenti necessari allo sviluppatore professionale.

Eh già; sono quelli che rendono complicato Xcode. Forse è sfuggito all’autrice che sviluppare app, specie app da piazzare sul mercato più competitivo al mondo, è un lavoro, anche di élite. E che *Playgrounds* significa *parchi gioco*.

(Il primo Swift Playgrounds, peraltro, era proprio una nuova funzione aggiunta a Xcode, con cui ho scritto un libriccino su Swift che allora navigava verso la versione 2.0).

*Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile.*