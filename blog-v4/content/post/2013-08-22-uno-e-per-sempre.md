---
title: "Uno e per sempre"
date: 2013-08-22
comments: true
tags: [Giochi]
---
A un certo punto della vita uno scopre la propria vocazione ludica. Che non va esagerata nella portata; diciamo però che si prende la decisione di concentrarsi su un genere o su un gioco specifico per un tempo indefinito, senza curarsi troppo dell’offerta praticamente infinita a disposizione.<!--more-->

Non sento la vocazione, in questo momento. Tempo fa ero votato ad [Angband](http://rephial.org); poi l’ho finito e le sue prospettive di rigiocabilità sono state insufficienti a convincermi.

Adesso che la parentesi agostana si avvicina (lentamente!) al termine, penso astrattamente a che tipo di giochi potrebbero farmi scattare una prossima vocazione.

Esclusi quelli di ruolo di massa su Internet come [World of Warcraft](http://eu.battle.net/wow/it/?-), che fanno gara a sé, mi viene da pensare a [FlightGear](http://flightgear.org): un simulatore di volo curato da anni in ogni dettaglio, tanto che diventare esperti significherebbe persino avere fatto mezzo passo verso la guida di veri velivoli.

Oppure [Free Orion](http://www.freeorion.org): simulazione strategica a tema spaziale di grande respiro e complessità, giocabile contro le intelligenze artificiali oppure quelle umane, via Internet. Mondi da colonizzare, l’equilibrio da trovare tra industria e ricerca, una base di conoscenza interna sterminata e soddisfazione grafica, uditiva, di interfaccia, nonché di livello di sofisticazione raggiungibile (ci si cura della progettazione della propria flotta spaziale ed è solo l’inizio).

O altrimenti, forse, [Minecraft](http://minecraft.net): possibilità creative limitate solo dalla fantasia e dalla dedizione.