---
title: "Perseverare podcast diabolicum"
date: 2022-07-05T00:59:21+01:00
draft: false
toc: false
comments: true
categories: [Web]
tags: [A dieta di mele, Filippo Strozzi, Roberto Marin, Marin, Strozzi, podcast, A2]
---
Non solo mi sono divertito come in una sera davvero speciale, ma [Filippo](https://a2podcast.fireside.fm/hosts/filippo) e [Roberto](https://a2podcast.fireside.fm/hosts/roberto) hanno perpetrato l’atto finale del crimine: dal materiale di partenza hanno editato un podcast da professionisti e lo hanno pubblicato nella pagina di riferimento di [A2](https://a2podcast.fireside.fm), per giunta con il suo titolo originale: [A dieta di mele](https://a2podcast.fireside.fm/38).

So solo dirgli un grande *grazie* per la loro capacità di creare un’occasione realmente *out of the box* e stanotte farò una cosa per la prima volta nella vita: mi riascolterò.

Non sono in grado di produrre complimento maggiore.