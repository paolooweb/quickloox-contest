---
title: "Come fai sbagli"
date: 2015-02-14
comments: true
tags: [Apple, fornitori, Cfsp]
---
Apple pubblica periodicamente un [rapporto sui progressi dei fornitori nel rispettare i diritti umani e l’ambiente](http://images.apple.com/supplier-responsibility/pdf/Apple_Progress_Report_2015.pdf).<!--more-->

Tra pagina 19 e pagina 20 si parla dei *conflict mineral*, estratti talvolta in zone dove ne guadagnano gruppi armati poco attenti ai diritti umani.

Dal 2011, si legge nel rapporto, è cominciata la pressione sulle fonderie locali per rispettare le regole del Conflict-Free Smelter Program o di iniziative equivalenti. Nel 2014 il numero dei fornitori verificati *conflict-free* è raddoppiato a 135 e altri 64 sono in fase di verifica.

>Sfortunatamente, anche dopo ampie esortazioni, quattro fonderie non hanno voluto impegnarsi a ricevere un’ispezione indipendente, così Apple le ha avvisate che saranno escluse dalla catena delle forniture.

Mi aspetto presto una qualche inchiesta sui lavoratori che perdono l’unica fonte di sostentamento perché Apple rescinde il contratto di fornitura con i loro datori di lavoro.
