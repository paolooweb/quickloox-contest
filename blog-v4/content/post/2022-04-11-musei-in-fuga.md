---
title: "Musei in fuga"
date: 2022-04-11T23:34:20+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [Ucraina, Russia, Sucho]
---
Assai meno grave rispetto ad altre notizie che purtroppo continuano ad arrivare dal [centro dell’Europa](https://it.wikipedia.org/wiki/Centro_dell%27Europa) e dintorni, nondimeno grave: la guerra mette a repentaglio una quantità importante di reperti storici e culturali, dal momento che gli invasori colpiscono indiscriminatamente.

Sono a rischio tanto i documenti analogici che quelli digitali, causa attacchi cibernetici, distruzione di centri dati e anche di normali abitazioni.

Sui dati digitali, è emerso il lavoro di [Saving Ukrainian Cultural Heritage Online](https://www.sucho.org) (Sucho), che coordina milletrecento volontari nel tentativo di preservare anche i bit oltre agli atomi; al momento si parla di oltre venti terabyte già messi in salvo, in maggioranza materiale digitale scandito presso musei, biblioteche e archivi ucraini.

L’iniziativa del Sucho è tanto più importante in quanto gli invasori dibattono apertamente della [possibilità di azzerare l’esistenza dell’Ucraina come nazione e come popolo](https://www.ilpost.it/2022/04/05/articolo-ria-novosti-ucraina-denazificazione/). Conservare testimonianze storiche, artistiche e culturali è di somma importanza in questa situazione.

Fermo restando che chiunque può contribuire all’azione di salvataggio semplicemente donando a sostegno dei costi, mi sono chiesto anche quanti documenti siano già andati persi, quante biblioteche siano state distrutte prima che fosse possibile digitalizzarne il patrimonio; è un caso nel quale la capacità di spostare bit potrebbe risultare importantissima.