---
title: "Contraddizione in privacy"
date: 2018-03-01
comments: true
tags: [iCloud, Cina, EU]
---
I dati iCloud relativi agli utenti cinesi verranno purtroppo [conservati in un centro dati cinese](http://appleinsider.com/articles/18/02/28/apple-makes-chinese-transfer-of-icloud-data-official-raising-privacy-concerns), su server affidati a una azienda cinese che, naturalmente, è gradita al governo.<!--more-->

La mossa è obbligata dalla legislazione cinese e viene giustamente criticata perché si presume esponga alla dittatura locale i dati delle persone. I dati su iCloud [sono cifrati](https://images.apple.com/business/docs/iOS_Security_Guide.pdf) e possono essere letti solo da chi possieda le chiavi di accesso all’account ma questo, chiaramente, non ferma più di tanto un totalitarismo.

Apprendo intanto [da Reuters](https://www.reuters.com/article/us-eu-data-order/europe-seeks-power-to-seize-overseas-data-in-challenge-to-tech-giants-idUSKCN1GA0LP) che

>L’Unione Europea sta preparando legislazione per obbligare le aziende a consegnare i dati personali dei clienti su richiesta, persino se i dati in questione vengono custoditi fuori dai confini europei.

La reazione tipica che sento è del tipo *Così le multinazionali che evadono le tasse la smetteranno di fare quello che vogliono*.

Certamente in Europa non ci sono dittature e auspicabilmente le richieste di dati personali saranno ben motivate da inchieste giudiziarie. Peraltro, scrivo queste righe dal Paese con [il doppio delle intercettazioni telefoniche dei primi tre Paesi europei messi insieme](https://www.ilpost.it/davidedeluca/2014/12/19/quanto-intercettazioni-si-fanno-italia/).

Fatti i distinguo, la posizione del governo cinese verso i dati su iCloud non è tanto diversa da quella dell’Unione europea.

Però in un caso è colpevole Apple di non tutelare la *privacy* dei cinesi; nell’altro è colpevole Apple di voler scantonare dal volere dell’autorità. A me pare vagamente contraddittorio.