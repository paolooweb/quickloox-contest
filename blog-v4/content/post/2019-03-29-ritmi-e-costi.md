---
title: "Ritmi e costi"
date: 2019-03-29
comments: true
tags: [watch, Ecg]
---
La funzione Ecg di riconoscimento di anomalie nel battito cardiaco e fibrillazione atriale [è arrivata anche in Italia](https://www.apple.com/it/newsroom/2019/03/ecg-app-and-irregular-rhythm-notification-on-apple-watch-available-today-across-europe-and-hong-kong/).

Mi immagino il dialogo tra due persone in ambulatorio.

*Anche lei qui per il cuore?*

*Eh sì, per fortuna c’è l’orologio di Apple che mi dà una mano.*

*Eh beh, io ho preso un Samsung perché costava un po’ meno. Anche lui misura i battiti, comunque.*

*Complimenti! Anche Samsung ha fatto lo studio con l’università californiana per vedere se funziona bene? L’ho letto il mese scorso, allo studio dell’orologio di Apple [hanno collaborato quattrocentomila persone…](https://med.stanford.edu/news/all-news/2018/11/stanford-apple-describe-heart-study-with-over-400000-participants.html)*

*…*

*Certo, risparmiare è sempre una buona cosa. Io ho fatto un piccolo sacrificio in più, ma almeno sono più tranquillo sapendo di essere sempre controllato in modo affidabile.*

*Anche se di questi aggeggi non c’è mai da fidarsi, eh!*

*Vedremo, vedremo…*