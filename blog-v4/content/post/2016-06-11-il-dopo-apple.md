---
title: "Il dopo Apple"
date: 2016-06-11
comments: true
tags: [Apple, Energy]
---
Sono passati più di dieci anni da quando Apple è diventata Apple e ha smesso di chiamarsi Apple Computer. Molte persone ritengono che in quel momento sia come finita un’età dell’innocenza e che invece di progettare i migliori computer al mondo, o almeno provarci, abbia iniziato a concentrarsi sui telefoni e finire per tradire gli ideali originali eccetera eccetera.<!--more-->

È che i tempi cambiano e lo fanno anche molto in fretta. Apple oggi produce computer e sì telefoni, ma anche orologi (in realtà sono sempre computer con diverse specializzazioni, ma sintetizzo). Corteggia il mondo della televisione, domina quello della musica, ha un sistema di pagamento per chi usa carte di credito, si dice stia lavorando a un prodotto per il mercato automobilistico.

E ha anche [costituito una Apple Energy](http://9to5mac.com/2016/06/09/apple-energy-company/), che potrebbe un domani significare la vendita di energia a utenti finali.

Pretendere che dopo Apple ci sia solo il Computer, nel mondo odierno, è un anacronismo. Nel 2050 potremmo vederla vendere intelligenze artificiali. I tempi continuano a cambiare.