---
title: "Solo i diversi sopravvivono"
date: 2020-03-03
comments: true
tags: [MacSurfer, Unread, Swift, ePub, MacStories]
---
Da tempo sostengo che in quest’epoca scrivere a livello professionale, ma non solo, implica la padronanza dei sistemi di formattazione, produzione e distribuzione del testo. A parte i grandissimi e i vendutissimi, la figura dello scrittore che pensa unicamente a scrivere, senza curarsi di come verrà trattato il proprio testo da quel punto in avanti, non è più credibile, un po’ come quella di un musicista che pensi solo a suonare nell’indifferenza da che cosa scaturisce dal mixer.

Mi ci fa pensare la chiusura di [MacSurfer](https://www.macsurfer.com), sito storico per chiunque volesse rimanere sempre aggiornato sul mondo Apple, esistente dallo scorso secolo. I produttori del sito hanno deciso di cambiare il modello di funzionamento del sito e vendere abbonamenti per la consultazione. Non ha funzionato.

Questo avviene mentre [Unread 2](https://www.goldenhillsoftware.com/unread/), uno dei lettori Rss più eleganti nel panorama di iOS [come evidenzia MacStories](https://www.macstories.net/reviews/unread-2-review-the-elegant-rss-client-leaps-into-modernity/), propone un meccanismo di abbonamento al termine di un periodo di prova gratuita. Unread è passato [attraverso una acquisizione](https://blog.supertop.co/post/163722486217/unread-is-now-a-golden-hill-software-app) quasi tre anni fa e c’è ancora. Può essere che il meccanismo di abbonamento funzioni.

La differenza tra i contenuti dei due modelli è illusoria. Alla fine si tratta sempre di informazione, semplicemente mediata in due momenti diversi per essere riproposta al pubblico. Unread 2, tuttavia, esercita un controllo immensamente maggiore sul sistema di formattazione, produzione e distribuzione del proprio contenuto, in virtù della app che avvolge il contenuto suddetto.

MacSurfer sarebbe rimasto in vita se fosse diventato una app? Onestamente, non credo. D’altro canto, se Unread decidesse di diventare un sito a pagamento, non gli darei un anno di vita.

Che cosa ha a che vedere il tema del modello di business con la tipografia, per esempio? Moltissimo. La verità è che nessuno pagherà per un contenuto in quanto tale, proposto in forma indifferenziata dagli altri. Il contenitore fa moltissimo e per questo un autore, oggi, deve sapere di Html, Css, ePub, magari JavaScript. Se sa di Swift ancora meglio, perché una app lo compenserà – potrebbe compensarlo – meglio di un ebook o di un sito. E il suo lavoro non può solo essere diverso dagli altri; deve anche apparirlo, in ogni suo elemento.

MacSurfer, per la sua presentazione del contenuto, era diventato da tempo un sito come altri, a prescindere dal contenuto effettivo. Unread è una app unica nel suo aspetto.