---
title: "Rust Never Sleeps"
date: 2018-08-17
comments: true
tags: [spam, Young, Bresaola]
---
In rapida successione: ho vinto un Samsung Galaxy S9 Edge, a patto di caricare un documento di identità; ho mancato una consegna Dhl, come documenterebbe il file eseguibile Java allegato e, soprattutto, Giulio Bresaola mi ha fatturato la sua prestazione. Quale, lo scoprirei se aprissi l’archivio .rar accluso.

So che capita a tutti sempre. È che sono in un posto bello, è una giornata di sole e vedo sfilare turisti rilassati. Mi viene da pensare a quanti, anche oggi e anche qui, sono al lavoro perché tutto funzioni e vincono un Samsung a sera, stanchi e stressati. Ci vuole un rispetto sovrano per le fatiche quotidiane e per la costruzione di un mondo dove Giulio Bresaola sia un emarginato che sparisce per mancanza di terreno di coltura.

(Con ringraziamenti a [Neil Young](https://macintelligence.org/posts/2017-12-02-un-grande-passo-per-la-musica/)).

(Se cerco *rust never sleeps* su Google la prima cosa che vedo è una voce di Wikipedia. Sempre per pensare a un mondo migliore, vorrei che fosse come minimo il video, se non il sito di Neil Young. Chi ha un parente al lavoro presso Google glielo dica).


