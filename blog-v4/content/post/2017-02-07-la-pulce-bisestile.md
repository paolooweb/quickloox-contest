---
title: "La pulce bisestile"
date: 2017-02-07
comments: true
tags: [Apple, iPhone, Elmer-DeWitt, calendario, trimestre]
---
Complotto! Apple ha annunciato fatturato e unità vendute record per iPhone, ma per via di come funziona il conteggio fiscale ha approfittato di un prenatalizio che, invece delle tredici settimane ordinarie, ne ha comprese quattordici.

Se contiamo gli iPhone venduti per settimana, c’è un calo, altro che record. Bugia, inganno, disinformazione, [fake news](http://lapcatsoftware.com/articles/14weeks.html).

Per legge le aziende americane devono conteggiare un trimestre come composto da tre mesi oppure da tredici settimane. Questa organizzazione costringe ogni qualche anno a registrare straordinariamente un trimestre “bisestile” [di quattordici settimane](http://www.slate.com/blogs/moneybox/2014/02/06/_14_week_quarters_financial_markets_get_confused.html).

In pratica, lo scorso trimestre Apple ha avuto una settimana in più per fare cassa. E i risultati, anno su anno, sono stati confrontati con un trimestre più corto. In termini di giorni utili, la settimana extra ne assicura quasi l’otto percento in più. Il fatturato ne viene favorito, ovviamente. E così Apple ha ingannato tutti, secondo la vulgata.

Che cosa succede quando avviene il contrario? Per forza di cose, capita che un trimestre da tredici settimane venga messo a confronto con un anno precedente dove lo stesso trimestre era da quattordici. Quindi il confronto parte in svantaggio, di poco più del sette percento.

L’ultima volta è successo con il trimestre natalizio 2012. Apple è stata bastonata dagli analisti e Philip Elmer-DeWitt [si è chiesto perché](http://fortune.com/2013/02/02/apple-analysts-stupid-or-lazy/) non abbiano considerato la questione del calendario.

Capito? Quando Apple approfitta della settimana in più, sparge falsità. Quando la subisce, colpa sua.

Che sia giusto o sbagliato, la realtà è che i commentatori *trascurano sempre* la questione del calendario. Si leggano i pezzi linkati sopra.

Se proprio bisogna spaccare il capello in duecentocinquantasei, si fa sommessamente notare che il trimestre luglio-settembre è composto da novantadue giorni; quello gennaio-marzo da novanta (a volte novantuno); quello aprile-giugno ne ha novantuno, così come quello settembre-dicembre. Siamo nei dintorni dell’uno percento di giorni in più in meno, ma quando un fatturato si conta in miliardi significa che anche una differenza così minima sposta cifre significative.

Mai sentito qualcuno sottolineare che il trimestre estivo è lungo un giorno in più? Mai, nessuno. Mai sentito tirare in ballo il febbraio bisestile? Mai o quasi mai (vado a memoria).

La verità è che, giusto o sbagliato, il trimestre viene usato da tutti, sempre, come *approssimazione* della realtà.

Svegliarsi una volta ogni dieci anni a puntualizzare è di conseguenza pretestuoso.

A parte il fatto che Apple ha annunciato la vendita di iPhone [trimestrale](http://www.apple.com/pr/library/2017/01/31Apple-Reports-Record-First-Quarter-Results.html) più alta di sempre e non la vendita di iPhone *per settimana* più alta di sempre. *Di sempre* vuol dire che il confronto è anche con altri trimestri da quattordici settimane.

Sempre utile fare le pulci alle statistiche, ma la pulce bisestile è un po’ tendenziosa. Dovrebbe essere sempre attiva per essere credibile, perché persino il banale trimestre di calendario in qualsiasi anno ha almeno due durate diverse, normalmente tre.
