---
title: "Di ventotto ce n’è una"
date: 2015-12-14
comments: true
tags: [Dell, certificati, autenticazione, Windows]
---
Dell ha inserito in alcuni computer uno strumento per attivarne il controllo, a scopo di supporto e soluzione di problemi. Tutto normale.<!--more-->

Per autenticare il controllo remoto senza troppe questioni, si è inventata una loro autorità di certificazione, diciamo non ufficiale. Mica tanto normale.

Siccome a volte non lavora proprio benissimo, ha lasciato (ha inserito per sbaglio? si è dimenticata? boh) nel pacchetto software le chiavi private per l’autenticazione. Tradotto in pratica: chiunque potrebbe falsificare un certificato di autenticazione che, inviato a uno di questi computer Dell, risulterebbe autorizzato e legittimo. Niente di normale qui.

La scoperta ha suscitato un leggero putiferio e Dell ha pubblicato le istruzioni per rimuovere dai computer coinvolti i due certificati al centro del problema. Rimozione che si effettua facilmente in [ventotto semplici passi](http://www.dell.com/support/article/us/en/19/SLN300321).

Qui ci si potrebbe dividere tra quanti optano per la completa uscita dalla normalità e gli altri che, abituati a Windows e a Dell, potrebbero persino trovare normale la cosa.

Di sicuro, un’azienda così è unica. Tra l’altro, tutte le volte che ho frugato nelle configurazioni, le ho sempre trovate poco competitive. È anche difficile dire che costa meno.