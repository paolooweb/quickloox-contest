---
title: "Corsa al ribasso"
date: 2015-03-31
comments: true
tags: [watch, wearable, smartwatch, Eugenio, Mantellini, Mante]
---
Ringrazio [Eugenio Biciclista](https://twitter.com/eugenioeu2) per la segnalazione.

<blockquote class="twitter-tweet" lang="en"><p>Questo piacerà a <a href="https://twitter.com/loox">@loox</a> “<a href="https://twitter.com/mante">@mante</a>: Manteblog, La scuola di mia figlia contro Apple Watch <a href="http://t.co/T1XMDfUl6O">http://t.co/T1XMDfUl6O</a>”</p>&mdash; Eugenio BICICLISTA (@eugenioeu2) <a href="https://twitter.com/eugenioeu2/status/582533379805020160">March 30, 2015</a></blockquote> <script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>

Il [*post* di Massimo Mantellini](http://www.mantellini.it/2015/03/29/la-scuola-di-mia-figlia-contro-apple-watch/) mostra come la scuola di sua figlia abbia bellamente ignorato tutti gli *smartwatch* in circolazione finora ma, ora che siamo nell’imminenza di [watch](http://www.apple.com/it/watch/?cid=wwa-it-kwg-watch-com), si sia premurata di *vietare l’uso degli orologi-smartphone*.

Niente di che: la scuola ha una ricca tradizione di divieto di apparecchiature che dieci o quindici anni dopo sono tranquillamente ammesse. E diverse università nel mondo si sono già mosse da tempo per [vietare i computer da polso](http://www.buzzfeed.com/jimwaterson/yes-but-will-the-battery-last-a-full-day) (mi dicono che non sono computer, però vorrei sapere per quale motivo, non essendolo, li vietano).

Come ebbi a dire in un paio di interventi pubblici, il prossimo giro sarà quello dei *wearable*, i computer-abbigliamento. Ci saranno sensori (e attuatori!) nelle calze, nelle tasche, nei bottoni, nel tessuto. Se non saranno i Google Glass, saranno gli occhiali di qualcun altro. Che cosa si inventerà la scuola quando i computer staranno su [tatuaggi temporanei](http://www.computerworld.com/article/2488638/emerging-technology/ready-for-your-electronic-tattoo-.html)? E quando invece saranno su innesti permanenti [sottopelle](http://www.bbc.com/news/technology-31042477)?

Quel tempo passato a vietare, lo si potrebbe trascorrere a insegnare. Magari ad aggiornarsi.