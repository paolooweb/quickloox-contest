---
title: "Giochi (più) intelligenti"
date: 2021-06-10T00:39:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [NetHack, AI, Facebook, NetHack Challenge, Go] 
---
Una vita sui *roguelike* non è stata vissuta invano, se *TechCrunch* titola [La pluridecennale avventura Ascii NetHack può suggerirci il futuro dell’intelligenza artificiale](https://techcrunch.com/2021/06/09/decades-old-ascii-adventure-nethack-may-hint-at-the-future-of-ai/).

La lettura spiega che algoritmi e apprendimento meccanizzato possono fare a pezzettini qualsiasi giocatore umano di scacchi o di [Go](https://en.wikipedia.org/wiki/Go_(game)), così come di [Dota 2](https://www.dota2.com/home) per fare un nome a caso.

Quando si arriva a NetHack, però, l’intelligenza artificiale si comporta molto peggio di un uomo. La ragione è la grande complessità interna del gioco, dovuta tra l’altro alla pazzesca varietà di interazioni tra i componenti del gioco.

Il team AI di Facebook ha allora deciso di organizzare una [NetHack Challenge](https://nethackchallenge.com) aperta fino al 15 ottobre a tutte le intelligenze artificiali, con premi per il migliore *agente* (entità software capace di progredire più di tutte le altre nel gioco), il migliore agente senza l’aiuto di una rete neurale e, ancora, il migliore agente creato da una università o da team indipendenti, che non producono giochi per mestiere, per capirci.

Il terreno di sfida sarà una normalissima edizione di NetHack montata dentro un ambiente di *machine learning*, che permetterà la partecipazione di chi brilla per intelligenza (umana) e però scarseggia in risorse computazionali.

Dice molto sulla vera natura del valore degli umani e delle sedicenti intelligenze artificiali, che il computer abbia ancora moltissimo da imparare da un gioco disegnato con il testo, [nato nel 1985](https://www.nethack.org/download/index.html) e più maturo di tanti dei programmatori che proveranno a batterlo.

*La regolarità delle pubblicazioni potrebbe risentire nelle prossime settimane dei miei spostamenti per l’Italia.*

*Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile.*               