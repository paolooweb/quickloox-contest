---
title: "Anno nuovo, deficienti vecchi"
date: 2019-01-05
comments: true
tags: [ iPhone]
---
Mi è capitato di leggere altre reazioni all’annuncio della [revisione al ribasso del fatturato Apple](http://www.macintelligence.org/blog/2019/01/04/prepararsi-al-dopo/) e ognuno ha le sue opinioni, vanno rispettate.

Un conto però sono le opinioni e un conto la deficienza, intesa alla lettera come *mancanza di*.

Da anni evito accuratamente le coperture di Apple scritte in italiano e il deficiente che mi hanno sottoposto casualmente conferma la bontà dell’impostazione nel tempo.

Il suo pezzo evidenzia nella prima riga come, *a memoria*, l’avviso di revisione al ribasso del fatturato non abbia precedenti per Apple. Deficienza di memoria, ma forse più di uso di Google, perché chiunque potrebbe accorgersi che [è successo nel 2002](https://www.apple.com/newsroom/2002/06/18Apple-Revises-Third-Quarter-Guidance/).

Sventuratamente, ho proseguito la lettura. Il deficiente si è prodotto in un triplo salto mortale carpiato scrivendo una cosa tipo *il mio caso non rappresenta certamente un campione statistico rilevante, ma rimango sempre un utente medio*. Deficienza, in questo caso, di un certo numero di nozioni – non elevatissime – in ambito matematico e logico.

Ho perseverato diabolicamente nell’errore. Il pezzo si conclude con la raccomandazione, sicuramente informatissima, di produrre un MacBook a mille euro, tre quinti del prezzo base attuale. Una mossa che certamente produrrebbe vendite miracolose di milioni di MacBook e compenserebbe i cinque-nove miliardi di fatturato limati da Apple nella [revisione al ribasso](https://www.apple.com/newsroom/2019/01/letter-from-tim-cook-to-apple-investors/). Chissà con che margini di guadagno.

All’indomani di una comunicazione in cui si parla esclusivamente di iPhone e si indica esplicitamente MacBook Air come fattore determinante nella crescita del fatturato degli altri comparti di Apple, escluso iPhone; crescita a doppia cifra.

Il deficiente risolverebbe la perdita di fatturato dovuto a mancate vendite di iPhone con un taglio al prezzo dei MacBook.

Non lo linko perché tanto chi vuole lo trova e molti lo avranno già letto. C’è infatti un aspetto nel quale il sito è tutt’altro che deficiente: pubblicità, affiliazioni, sistemi per generare denaro dal traffico web.

I deficienti, qui, sono quelli che digeriscono contenuto del genere senza non dico ribellarsi, ma almeno leggere dell’altro.
