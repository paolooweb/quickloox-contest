---
title: "Cambiano i tempi"
date: 2015-07-25
comments: true
tags: [Apple, Microsoft, Surface, iPad, watch]
---
Se si incrociano le dichiarazioni dei dirigenti Apple con quelle dei vertici di Microsoft in occasione dei rispettivi annunci finanziari trimestrali, si sa con certezza che watch ha fatturato più di Surface.<!--more-->

Prima premessa: se questi signori raccontano bugie, rappresentando società per azioni, turbano il mercato e sono passibili di reato. In America non la prendono sul ridere come in Italia. Di conseguenza è probabile che dicano il vero.

Seconda premessa: per Microsoft, Surface è un business in forma, che sullo scorso anno ha guadagnato il 117 percento in fatturato. Eppure, anche a non volerlo vedere come una toppa ai buchi aperti da iPad, si fa battere da watch.

Terza premessa: le vendite in negozio di watch sono state praticamente inesistenti per due terzi del trimestre e hanno riguardato quasi solo gli Stati Uniti e i Paesi di prima fascia. L’Italia, che è di seconda fascia, ha contribuito solo dal 26 giugno, ossia a trimestre praticamente terminato.

Dovendo puntare su una tecnologia per il futuro, watch o Surface?