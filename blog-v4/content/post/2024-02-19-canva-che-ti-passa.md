---
title: "Canva che ti passa"
date: 2024-02-19T00:58:59+01:00
draft: false
toc: false
comments: true
categories: [Web, Education]
tags: [Canva]
---
Mi sono reso conto che [Canva è usabile gratis dalle scuole elementari e medie](https://www.canva.com/it_it/educazione/); è sufficiente che la scuola si identifichi come tale e i docenti pure.

Le scuole – ne conosco varie e da qualche anno ne frequento una abbastanza grande – hanno un problema generale di approccio dei bambini alla parte informatica, che la maggior parte delle insegnanti risolve inventandosi attività su… Office perché conoscono quello e tant’è.

Canva non è la mia visione di strumento ideale per le scuole; se però è gratuito, allora è anche leggero, veloce, semplice, ricco di esempi, collaborativo, divertente, versatile. Molto meglio inventarsi attività con quello che con Word o PowerPoint.

Un sacco di piccole attività creative, dal disegnare un invito a un calendario, a un cartello a uso interno, decorare foto, creare un diploma scherzoso, dare una copertina a una ricerca, giocare insieme con la tipografia e la grafica; in una quarta elementare, faccio un esempio collegato alla mia esperienza quotidiana, nascono o possono nascere spunti come questi un giorno sì e uno pure. Canva li soddisfa senza mettere in crisi le maestre con *un altro software da imparare* (sei veramente produttivo con Canva in dieci minuti) e con risultati capaci di soddisfare qualsiasi bambino, nel raggiungerli e mel goderseli.

In altre parole, è un ottimo male minore in attesa di riuscire a introdurre tante altre opzioni più elevate e didatticamente solide, se solo non fosse di fatto troppo difficile per il contesto.

Ho provato a segnalare l’opportunità (che, ripeto, è a costo zero) alla scuola della primogenita. Vediamo se si muove qualcosa oppure se… Canva cavallo.