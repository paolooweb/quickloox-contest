---
title: "In viaggio con Wi-Fi - la grande truffa"
date: 2019-03-09
comments: true
tags: [Wi-Fi, Air, France]
---
Tratta intercontinentale Parigi-Detroit e viene naturale collaudare il Wi-Fi di bordo. Anche perché, la sera precedente, i sistemi di Air France si erano rifiutati di effettuare il check-in online, restando irraggiungibili per alcune ore prima di concedersi. Pér una grande compagnia aerea si tratta di una situazione come minimo inusuale.

In sostanza, una truffa. Nominalmente abbiamo a disposizione tre opzioni di navigazione, cinque euro, dieci euro e venti euro, rispettivamente per venti megabyte, cento megabyte, duecento megabyte di dati. Provo ad acquistare il taglio inferiore, ma non c’è verso di effettuare l’acquisto. Il meglio che riesco a ottenere è un messaggio di momentanea indisponibilità. Un’oretta dopo riprovo senza esito, due ore più tardi uguale. Me ne disinteresso e lavoro offline per come posso.

Di fianco a me una ragazza ostinata con portatile e telefono continua a provare a riprovare. In dodici ore riuscirà a ottenere l’acquisto del taglio da venti euro, che riuscirà a sfruttare per sette mega su duecento prima che il sistema si blocchi definitivamente. Almeno tre interventi del personale di bordo sembrano avere portato niente più che empatia e probabilmente un qualche tipo di rimborso.

Su una tratta di quel tipo, su sedili Premium Economy spaziosi, con prese per la ricarica e intratteniménto in volo personalizzato a buon livello, un fiasco così sembra problematico.
