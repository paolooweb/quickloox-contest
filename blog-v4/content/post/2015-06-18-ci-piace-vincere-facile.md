---
title: "Ci piace vincere facile"
date: 2015-06-18
comments: true
tags: [watch, Dalrymple, Apple]
---
Internet sta distruggendo l’editoria come la conosciamo, con un piccolo effetto collaterale positivo: uomini come Jim Dalrymple ora si autoproducono e sono più liberi di scrivere esattamente ciò che pensano, come nel caso della sua [recensione più personale di sempre](http://www.loopinsight.com/2015/06/16/apple-watch-my-most-personal-review-ever/).

Il punto non è la distruzione dell’editoria e neanche tanto la recensione di Dalrymple, di cui però vale la pena leggere qualche estratto.

>Parlo di Apple da oltre vent’anni e nessun altro prodotto ha avuto altrettanto impatto sulla mia vita.

>Ero un po’ preoccupato dopo avere letto le recensioni iniziali, che parlavano di un sistema di notifiche noioso e di una curva di apprendimento ripida.

>Non c’è una curva ripida. Sono sciocchezze. Le notifiche sono completamente configurabili. Ancora, sciocchezze.

>Quando mi chiedono come si una una interfaccia progettata da Apple, rispondo sempre di pensare al modo più facile di operare e nove volte su dieci è quello che fa l’interfaccia Apple. Una volta su dieci Apple si incarta e succede qualcosa di bizzarro, ma la risposta vale nella maggioranza dei casi.

Vengo al punto.

>Qui è dove la recensione diventa molto personale. Ecco come ho perso più di quaranta libbre [diciotto chili] usando HealthKit e Apple Watch.

>Se Apple Watch dice alzati, mi alzo. Non so ancora perché. Forse voglio solo chiudere quei cerchi [nell’interfaccia] ogni giorno e sentirmi bene per averlo fatto. Forse alzarmi ogni ora mi fa davvero bene. Non lo so, ma do retta al cosino sul mio polso e mi alzo.

Facile immaginarsi come  Dalrymple abbia perso peso e, fosse difficile, la recensione lo spiega nei dettagli.

Piuttosto, abbiamo sempre parlato dei computer come amplificatori di intelligenza e capaci di valorizzare l’impegno di chi ci vuole ottenere risultati.

Stiamo entrando a piccoli passi (e camminate quotidiane) in un’epoca successiva, dove il computer può offrire risultati anche a chi non si vuole impegnare e semplicemente segue un consiglio o accetta una indicazione.

Il computer veramente per tutti ti fa vincere facile anche se non sei un talento, non impari a programmare, sfrutti il tre percento di quello che può fare. Rimane sempre l’immenso valore aggiunto dell’impegno. Ma prima, per chi non si impegnava, il computer dava zero oppure un curriculum scritto in formato europeo, cioè zero.