---
title: "Lo spavento si sviluppa"
date: 2015-11-01
comments: true
tags: [programmazione, iOS, Android]
---
Una *software house* misura tempo e codice spesi su progetti che coinvolgono sia Android che iOS e trovano che [Android richiede il 40 percento in più di codice e il 30 percento in più di tempo](https://infinum.co/the-capsized-eight/articles/android-development-is-30-percent-more-expensive-than-ios). In altre parole, sviluppare per Android costa di più.<!--more-->

Considerato che il mercato Android è notoriamente molto meno disponibile a riconoscere un giusto prezzo per le *app*, si può banalizzare la situazione dicendo che bisogna spendere di più in partenza per ottenere di meno all’arrivo.

Questo spiega perché la gran parte dei progetti iOS-Android partano su iOS e poi arrivino su Android in un secondo tempo. Spiega anche molto sulla qualità del software Android.

Una *software house* con risorse limitate, e tenuta a considerare il mondo Android, ha di che spaventarsi.

Ovviamente il campione considerato è molto piccolo e si può discutere a lungo delle metodologie. Tuttavia si tratta di persone che dal proprio software guadagnano lo stipendio. Anche a mettere in discussione le loro conclusioni, difficile sostenere che sbaglino tutto o per motivi misteriosi siano gli unici a osservare l’effetto che documentano.