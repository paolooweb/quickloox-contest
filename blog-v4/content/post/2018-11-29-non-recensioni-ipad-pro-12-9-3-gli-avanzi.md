---
title: "Non-recensioni: iPad Pro 12,9” / 3 - gli avanzi"
date: 2018-11-29
comments: true
tags: [iPad, Pro]
---
Mi spiace di andare per spizzichi e bocconi, con una [prima](http://www.macintelligence.org/blog/2018/11/27/non-recensioni-ipad-pro-12-9/) e una [seconda](http://www.macintelligence.org/blog/2018/11/28/non-recensioni-ipad-pro-12-9-2/) parte prima di questo post. Avrei scritto volentieri un megapezzo [stile John Gruber](https://daringfireball.net/2018/11/the_2018_ipad_pros) solo che lui è più organizzato e così per metà mi sono rimaste sulla tastiera cose che volevo dire e altre le ho scoperte cammin facendo. Le pubblico in ordine sparso.

Sono passato dalla tastiera virtuale di iPad vecchio stile a quella full size di iPad Pro, che possiede tutti i tasti della tastiera convenzionale. Sul 12,9” le dimensioni dei tasti corrispondono alla memoria dei polpastrelli e l’efficacia mi sembra equivalente a quella di prima.

Il buono è che si tratta di una normalissima tastiera. Il meno buono è che tutta l’esperienza accumulata con la tastiera virtuale ridotta del vecchio iPad si azzera. Non si può avere tutto.

Ho preso un modello solo Wi-Fi perché da anni uso il Personal Hotspot di iPhone e la Sim di iPad dorme dimenticata. Un dettaglio minimo ma significativo: si può accendere il Personal Hotspot di iPhone… da iPad. Significa che iPhone rimane in tasca e non serve estrarlo per accendere a mano lo hotspot. È una piccola grande comodità di cui mi accorgo in continuazione e per la quale ringrazio un qualche progettista software di Cupertino che non mi conosce, ma ha pensato anche a me.

Tra le app scaricate automaticamente c’è anche Clips e la mia copia è arrivata difettata: App Store proponeva automaticamente un aggiornamento che non avveniva. Ho risolto cestinando e ricaricando.

Come è stato detto credo persino da Jonathan Ive, veramente questo iPad prescinde dall’orientamento. L’assenza del tasto Home fa sì che venga impugnato indifferentemente da qualunque lato e anche questo, seppure dettaglio minore tra i minori, fa una differenza. Per me vedere il tasto Home in basso oppure in alto era diverso.