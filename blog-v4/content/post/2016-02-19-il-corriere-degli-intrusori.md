---
title: "Il Corriere degli intrusori"
date: 2016-02-19
comments: true
tags: [Severgnini, Sideri, Corriere]
---
Ritorno sul [braccio di ferro tra Apple e Fbi](https://macintelligence.org/posts/2016-02-17-il-coraggio-e-il-valore/) per segnalare che dal *Corriere della Sera* si agita il partito degli intrusori, quelli cui piace l’idea di uno Stato onnipotente in grado di ficcare le mani ovunque, con una ragione o anche senza.<!--more-->

Beppe Severgnini, sempre meno giornalista e sempre più megafono di interessi, titola [Prima i clienti poi i cittadini - La scelta sbagliata di Apple](http://www.corriere.it/opinioni/16_febbraio_18/sicurezza-privacy-apple-prima-clienti-poi-cittadini-scelta-sbagliata-0e5d32a2-d5a8-11e5-bbd0-dbbf7f226638.shtml). Si chiede *Da dove viene, dunque, quest’improvvisa sensibilità [di Apple per la privacy?]*

Non sa, ha perso da anni la linea con il presente, che – da dovunque venga – questa sensibilità non è affatto *improvvisa*. Sono anni che Apple comunica il proprio impegno sulla *privacy*. Lo scorso settembre [si parlava della cifratura dei dati di iMessage](https://macintelligence.org/posts/2015-12-11-la-cifra-della-moda/). Tim Cook parlava di *privacy* ai clienti Apple [un anno e mezzo fa](http://appleinsider.com/articles/14/09/17/tim-cook-touts-new-apple-privacy-policies-in-open-letter-to-customers), con [questa lettera](http://www.apple.com/it/privacy/). La [panoramica sulla sicurezza e privacy in iCloud](https://support.apple.com/it-it/HT202303) è aggiornata a giugno 2015.

Il mandante di Severgnini non lo sa e gli fa incautamente scrivere:

>Pensate alla norma (americana) che prevede di dotare il bagaglio di una serratura accessibile alle autorità. Si chiama Tsa, da Transport Security Administration (parte del dipartimento di Homeland Security). Perché accettiamo che il bagaglio venga aperto a campione, da persone anonime, a nostra insaputa? […] Tim Cook sbaglia, invece, quando dice: «Nelle mani sbagliate, questo software avrebbe il potenziale di sbloccare qualsiasi iPhone fisicamente in possesso di qualcuno». Le mani dell’autorità giudiziaria non sono sbagliate.

Pensi Severgnini che le chiavi della serratura accessibile alle autorità [sono giustappunto finite nelle mani sbagliate](http://www.apogeonline.com/webzine/2015/09/18/o-la-borsa-o-la-chiave). Si trovano su Internet a disposizione di chiunque come piani per essere stampate in 3D. Dove finirebbe il software che fosse dato all’Fbi per eliminare la cifratura da un iPhone, ovvero da tutti gli iPhone.

Siccome Severgnini è invecchiato ma scaltro e sa di ciurlare nel manico, chiama i rinforzi in forma di Massimo Sideri, che lancia l’allarme: se Apple si oppone alla decisione dei giudici di fornire un aiuto per craccare l’iPhone 5 di uno dei terroristi di San Bernardino, [una società privata detiene più potere dello Stato](http://www.corriere.it/economia/16_febbraio_17/apple-contro-fbi-se-privacy-vale-gli-attentatori-3d97526c-d555-11e5-bbd0-dbbf7f226638.shtml).

Triplo salto mortale con avvitamento: se ti opponi a una richiesta dello Stato che consideri ingiusta, detieni più potere dello Stato. I dittatori di mezzo pianeta si stanno fregando le mani. Implica Sideri che Apple possa entrare nell’iPhone di chicchessia e lo Stato no, mentre non è così; Apple non lo può fare ed è lo Stato che sta cercando di forzarla. Scrive *società privata* per buttarla in politica, la multinazionale cattiva contro i buoni rappresentanti del popolo. La politica non c’entra, quanto piuttosto la responsabilità di creare un strumento che mette a rischio i dati su un miliardo di apparecchi. Quando l’Fbi avrà in mano lo strumento che vuole, entro poco tempo finirà nelle mani sbagliate. L’Fbi vuole craccare un iPhone e se ne infischia del miliardo; Apple pensa al miliardo, a costo di prendere decisioni dolorose su uno. In quel miliardo di apparecchi c’è anche il mio e io tengo alla mia *privacy*.

A questo dovrebbe pensare l’altro argomento demagogico di Sideri, che oltre la demagogia non sa andare:

>Il vero rischio è che Apple, magari involontariamente, passi da una crociata sulla privacy a una di marketing.

Capito, i furbetti? Lo fanno per soldi, per interesse privato.

E certo, sono una società privata. Fanno soldi (anche) proteggendo la *privacy* dei loro clienti, a differenza di altre società private che invece sono stranamente silenziose o timide, come già scrivevo. A differenza dello Stato che per la *privacy* mi fa mettere migliaia di crocette inutili su moduli inutili, e nient’altro. Google o Facebook fanno soldi vendendola, la *privacy*, ma chiaramente Sideri non ne è al corrente. È solo Apple la cattiva che pensa al profitto.

Sideri, che anche a un Severgnini invecchiato non è degno di allacciare le scarpe, impiega poche righe a farla fuori dal vaso con i toni roboanti del custode della convivenza civile:

>Se così fosse [vincesse Apple] sarebbe allora ufficializzato che le società private sono i nuovi santuari della privacy in barba al contratto sociale che tutti noi accettiamo in democrazia: lo Stato lo eleggiamo noi cittadini e ad esso affidiamo alcuni compiti fondamentali come quello della sorveglianza, consapevoli dei difetti che il sistema ha sulla falsariga di quanto sintetizzato brillantemente da Churchill («La democrazia è la peggior forma di governo, eccezion fatta per tutte quelle altre forme che si sono sperimentate finora»).

Deve avere i polpastrelli a saponetta, Sideri, per tutto quello che gli sfugge. Per proteggere la nostra *privacy* lo Stato dovrebbe offrirci una piattaforma crittografica superiore. Quella funzione serve e se la offre Apple invece dello Stato, non è Apple cattiva, è inutile lo Stato.

Per entrare nel computer di Sideri, oggi, servono un permesso e un motivo; nel mondo che desidera lui, lo Stato ci entra quando e come vuole.

Per entrare in casa di Sideri, oggi, servono un mandato e una ragione. Lui vorrebbe che le forze dell’ordine avessero la chiave per entrare a piacimento.

Dovremmo fidarci dello Stato (in Italia! dello Stato!) e confidare che le chiavi a esso affidate – per aprire un iPhone, un computer, una macchina, una abitazione, un conto bancario – non cadano in mano a malintenzionati. Lo Stato mi fa pagare le tasse prima ancora che io abbia guadagnato dei soldi da tassare; mi permetto di essere scettico.

Il contratto sociale, povero Sideri, non riguarda il controllo totale dello Stato sulla vita del cittadino. Siccome è un contratto, non un’ingiunzione, *regola* diritti e *doveri* delle parti. In una democrazia le forze dell’ordine operano entro limiti prestabiliti. Per entrare in una casa senza mandato, la polizia può solo bussare e chiedere il permesso.

Severgnini e Sideri, meglio, chi li paga desidera un mondo diverso, dove la polizia ha la chiave e dove il malvivente, con la giusta mazzetta, la duplica il giorno dopo.

Se Apple perde, cambio il mio *passcode* e lo porto tipo a sedici caratteri alfanumerici. Così, anche se si spalancano gli iPhone, la cifratura resta (S e S lo ignorano); e quella terrà impegnata la polizia nei secoli dei secoli, amen. Se Apple perde, sapete cosa fare.