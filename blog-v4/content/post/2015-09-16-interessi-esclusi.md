---
title: "Interessi esclusi"
date: 2015-09-16
comments: true
tags: [stroke, OSX, traceroute]
---
Ci sono consigli anche per Unix e in generale anche per Windows, oltre a quelli per Mac, in [Thought Deposit[(http://www.thoughtdeposit.net). Fatta la premessa, quello che si trova per Mac – se interessa agire ogni tanto da Terminale – non è scontato.<!--more-->

Per esempio, chi sapeva come [utilizzare da Terminale lo scanner di porte logiche](http://www.thoughtdeposit.net/os-x/osx-has-a-builtin-command-line-port-scanner) presente dentro Utility Network di OS X? Oppure [come sostituire Traceroute](http://www.thoughtdeposit.net/os-x/a-quick-traceroute) con qualcosa di molto più veloce, per risalire rapidamente ai passaggi di rete necessari per arrivare dal nostro computer a un indirizzo Internet lontano?

Piccole cose che portano (un po’ più) avanti e richiedono tempo vicino a zero.

*Interessi esclusi* è il motto del sito.