---
title: "Più uguali degli altri"
date: 2022-02-24T01:43:23+01:00
draft: false
toc: false
comments: true
categories: [Education]
tags: [MacBook Pro, iPad mini, Apple Pencil, Bowdoin]
---
Dopo il successo di una iniziativa del 2020 che metteva [un iPad nelle mani di tutti gli studenti](https://www.bowdoin.edu/news/2020/07/bowdoin-to-provide-ipads-to-all-students-during-pandemic.html), il *college* Bowdoin ha annunciato che da questo autunno sempre [tutti gli studenti disporranno di un MacBook Pro, un iPad mini e una Apple Pencil](https://www.bowdoin.edu/news/2022/02/bowdoin-launches-groundbreaking-digital-excellence-commitment.html), kit che potranno riscattare alla fine degli studi per la cifra di un dollaro.

Uno dice *bella forza, è facile fare la scuola digitale quando si hanno i soldi*.

Parliamone un attimo. Effettivamente, stare a Bowdoin un anno costa, come stima dell’ateneo, la bellezza di [settantaseimila dollari](https://www.bowdoin.edu/admissions/costs-and-aid/index.html), tutto compreso. Non è solo la retta, ma tutto l’insieme delle spese che uno studente si troverà ad affrontare.

Peraltro, Bowdoin si dichiara college [need-blind](https://www.bowdoin.edu/student-aid/index.html): la famiglia dello studente dichiara il proprio stato finanziario e, in base a quello, viene stabilito quanto effettivamente si chiede a lei e quanto, invece, coprirà l’istituto.

Sul sito dell’università c’è un calcolatore della cifra indicativa che potrebbe essere chiesta alla famiglia. Ho fatto finta di essere cittadino americano e vivere con due genitori che hanno quasi finito di pagare il mutuo e hanno stipulato una buona assicurazione sulla vita, ma hanno un reddito modesto, due soldi sul conto corrente e nessun investimento in titoli o fondi. Bowdoin vorrebbe da questa famiglia del tutto ipotetica seimila dollari e gli altri settantamila ce li mette lei.

*Certo, così si crea il problema degli studenti indebitati a vita*.

Problema che negli Stati Uniti esiste ed è grosso. Relativamente alla realtà ristretta di Bowdoin, dal 2008 la scuola *fa a meno di prestiti e mutui*. Quello che copre, lo fa grazie a borse di studio, donazioni e finanziamenti.

*Si sa del resto che le buone università americane sono solo per i figli dei ricchi.*

Certo, Bowdoin non è una mezza città da solo come una Stanford o una Harvard. Non è neanche una nicchietta, però: [i dati di quest’anno](https://www.bowdoin.edu/admissions/index.html) parlano di cinquecentoventi matricole. Gli studenti che frequentano complessivamente sono milleottocentocinque.

*I privilegiati, i bianchi, i raccomandati, i maschi ecco chi viene accettato.*

Sempre dai dati quest’anno, si evince che un sesto delle matricole è, per dirla con Guccini, *il primo che ha studiato*; nessuno delle loro famiglie ha avuto una istruzione universitaria.

Gli studenti neri sono il quaranta percento.

I maschi, vero, sono più delle femmine; cinquantuno a quarantanove percento.

Gli studenti che vengono da altri Stati americani, quindi un po’ lontani per godere di raccomdazioni, sono il settantuno percento. Si noti che Bowdoin non è una iniziativa del mese scorso; [è stato fondato nel 1794](https://www.bowdoin.edu/about/index.html) e di tradizione ne ha da vendere. Lo Stato in cui risiede non esisteva ancora, essendo stato proclamato ventisei anni dopo.

Gli studenti non statunitensi sono il dodici percento.

*Scommetto che però ti prendono solo se sei bravo.*

E certo, i posti sono quelli. Però l’ateneo offre un mare di possibilità a chi è bravo e però non potrebbe permettersi di frequentare.

E a tutti danno un MacBook Pro, un iPad mini, una Apple Pencil. In questo modo, parole di Bowdoin,

> il college ispira innovazione e crea uguaglianza digitale per ogni studente, indipendentemente dai mezzi che ha la sua famiglia.

Di che cosa si blaterava in Italia quando le scuole erano chiuse e nessuno aveva mosso un dito perché potessero rimanere aperte? Che il digitale era veicolo di disuguaglianza, perché c’era un divario tra ricchi e poveri. Quindi il digitale era il male.

Bowdoin dimostra che l’uguaglianza si raggiunge eliminando la disuguaglianza, tu guarda, invece di puntare il dito e muovere esclusivamente la lingua.

*E chi non vuole diventare scienziato?*

La *mission* di Bowdoin è:

> erogare una straordinaria istruzione nelle arti liberali, insieme alla conoscenza e alle capacità che serve agli studenti per essere guide nel nostro mondo sempre più digitale.

Arti liberali, mica astrofisica (con immenso rispetto per gli astrofisici).

*Non ci sono i soldi.*

Non si vogliono trovare. E se non ci sono, visto che Bowdoin è piccolo, perché non fare la stessa cosa alla Normale di Pisa, al Politecnico di Milano, al DAMS di Bologna…? Giusto per mostrare che può essere fatto e soprattutto verificare che risultati porta.

L’uguaglianza si ottiene più facilmente facendo salire chi sta in basso. Strillare perché c’è gente in alto e pretendere che scenda, come sistema per portare l’uguaglianza, non è gran che.
