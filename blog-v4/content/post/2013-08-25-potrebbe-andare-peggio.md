---
title: "Potrebbe andare peggio"
date: 2013-08-25
comments: true
tags: [Microsoft]
---
La notizia che Steve Ballmer [lascerà Microsoft entro un anno](http://arstechnica.com/business/2013/08/microsoft-ceo-steve-ballmer-to-retire-within-12-months/) è preoccupante in tutti i casi.<!--more-->

Al suo posto potrebbe infatti arrivare uno più bravo. Oppure arrivare uno meno bravo.

In ambedue i casi, siederà sopra un giocattolo che incassa miliardi di dollari automaticamente, semplicemente continuando a esistere.

Un’azienda che può permettersi qualsiasi fallimento, errore, stupidaggine, distrazione, ritardo, incompetenza. Tanto decine di milioni di persone continuano ad acquistare Office per riflesso condizionato e Windows senza sapere che esistono alternative (migliori).

In questi anni Microsoft, solo qualche esempio, si è messa a vendere *console* per videogiochi, perdendo denaro; ha bruciato miliardi e miliardi nell’attività *online*; ha buttato soldi in un *flop* clamoroso come Kin; ha sprecato quasi un miliardo di dollari in tavolette Surface invendute. Aggiungiamo quasi una decina di miliardi di dollari in aziende acquisite senza senso con il risultato di scrivere a bilancio gli importi equivalenti in forma di perdite. Certo ha fatto anche cose buone, ma sulla bilancia non c’è partita.

Risultato: negli ultimi dieci anni Microsoft ha triplicato i fatturati e raddoppiato i profitti. Perché può sbagliare tutto e tanto incassa.

Se arriva uno meno bravo, ha a disposizione soldi a palate per giocare in perdita in qualsiasi mercato, strozzando la concorrenza; se arriva uno bravo, pure.

E si parla tanto dei fedelissimi che comprerebbero Apple per ragioni parareligiose. Mi fanno tanta più paura le legioni di ignoranti, nel senso buono, che danno soldi a Microsoft come li darebbero al primo che passa per strada, se solo fossero stati abituati.