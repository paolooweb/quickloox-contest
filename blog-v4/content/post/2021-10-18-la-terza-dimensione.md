---
title: "La terza dimensione"
date: 2021-10-18T01:47:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [Apple, Blender] 
---
Il [sostegno di Apple al progetto Blender](https://www.blender.org/press/apple-joins-blender-development-fund/) è proprio una bella notizia. Vuol dire che il miglior modellatore 3D open source sulla piazza sarà cittadino di prima classe su macOS.

Vuole inoltre mostrare che persino una multinazionale multimiliardaria può sostenere in modo trasparente il software libero; certamente Apple chiederà cose alla comunità di sviluppo di Blender, ma pagherà in abbondanza per il disturbo e i miglioramenti al programma finiranno anche dentro le versioni per Linux e altri sistemi operativi.

Una condotta diversa dal fagocitare le comunità e condizionarne lo sviluppo nel proprio interesse indipendentemente da che cosa serva agli utenti finali o alle comunità stesse.

Gli sviluppatori sono la terza dimensione dell’informatica commerciale, il terzo vertice del triangolo; l’equilibrio tra i loro interessi, quelli della società e quelli degli utenti finali dovrebbe essere la norma invece che l’eccezione.

Di eccezioni come questa c’è comunque da essere molto soddisfatti.

*Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile.*