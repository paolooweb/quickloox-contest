---
title: "Farsi del bene"
date: 2019-08-15
comments: true
tags: [Ferragosto, Letoverlambda]
---
La quiete di Natale ha un sottofondo di voci, attività; quella di Ferragosto è mortale e sembra di stare nel deserto anche in mezzo a quattordici file di ombrelloni.

Gli auguri suonano sempre un po’ retorici; più di quelli, ho un pensiero per i tanti con cui ho un rapporto di amicizia attraverso queste pagine e i tantissimi con i quali non ho mai parlato, anche se lo farò volentieri alla prima occasione.

Un pensiero per ciascuno, da solo o in compagnia, per scherzo o seriamente, in cima al mondo o sul divano di casa.

Eccola qui la retorica, *vade retro*. Invece: va bene tutto quello che hai intenzione di fare. Fallo bene, fosse anche stare sdraiato a perdersi nel cielo.

Niente consigli non richiesti, solo abbracci e stima, forte e tanta, non importa se andiamo d’accordo o litighiamo. Buon Ferragosto.
