---
title: "Trentotto usi orari"
date: 2015-03-15
comments: true
tags: [Griffiths, watch, iPad, iPhone, Samsung, AppleTV, Mac]
---
Rob Griffiths, ex *Macworld*, ha meritoriamente compilato un foglio di calcolo con [tutte le varianti possibili di Watch](http://robservatory.com/the-all-in-one-apple-watch-spreadsheet/).<!--more-->

La cosa straordinaria di tutta questa varietà – trentotto modi di indossare watch – è che contrasta clamorosamente con tutto il resto del catalogo Apple. I modelli nuovi di iPhone sono una manciata, come quelli di iPad. Al netto della personalizzazione prevendita, i Mac sono pochi; di Apple TV ce n’è una sola, come c’è un solo iOS e un solo OS X (Server è una *app*).

La maggior parte degli occhi è puntata su quanto costano i modelli dorati o sulle [chiacchiere a proposito dell’autonomia](https://macintelligence.org/posts/2015-03-14-lassillo-dellora/). Pochi si rendono conto del movimento in controtendenza di Apple: una Samsung (e qualsiasi altra azienda nel settore) pubblica vagonate di *smartphone* diversi e pochissime varianti di orologi.

Chiedersi il perché. La risposta: come iPhone ha inaugurato l’epoca post-PC, watch inaugura quella dell’informatica postindustriale. C’è una sottile differenza tra saturare il mercato di prodotti per intercettare più domanda possibile e offrire ampia varietà di personalizzazione al singolo, primo passo verso il traguardo di costruire per ciascuno il *suo* prodotto, dalla completa individualizzazione.

Se watch viene eseguito bene e riesce, il tavolo delle regole del mercato verrà nuovamente ribaltato, come era già accaduto con iPhone. Ah, questa Apple che ha smesso di innovare.