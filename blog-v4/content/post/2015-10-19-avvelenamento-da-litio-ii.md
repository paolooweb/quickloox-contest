---
title: "Avvelenamento da litio / II"
date: 2015-10-19
comments: true
tags: [Magic, Keyboard, Wireless, II]
---
Subito dopo il [cambio di batteria per Magic Trackpad](https://macintelligence.org/posts/2015-10-17-avvelenamento-da-litio/), è arrivato quello per la [Wireless Keyboard II](http://www.apple.com/it/magic-accessories/).<!--more-->

Durata di 138 giorni, dieci *data point* raccolti, 235 giorni di media, dato bugiardo perché gli ultimi quattro cambi di batteria si sono susseguiti con autonomia dai cento ai centocinquanta giorni. Cioè da quando ho smesso di spegnere fisicamente la tastiera.

Vale comunque quanto già detto: continuerò a raccogliere i dati fino a che la tastiera resta in vita, pur se il nuovo modello contiene una ricaricabile interna e come tale va misurato con una scala completamente diversa.