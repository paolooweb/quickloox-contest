---
title: "La Befana vien di note"
date: 2019-01-06
comments: true
tags: [GarageBand, MacRumors]
---
Tutte le feste si porta via, tranne [GarageBand](https://www.apple.com/mac/garageband/ ), che è cresciuto fino a essere un piacere per chiunque si diletti nella musica e oggi compie quindici anni.

Lo segnala *MacRumors*, che aggiunge una suggestiva [timeline](https://www.macrumors.com/2019/01/04/garageband-turning-15/) delle evoluzioni principali del programma nel tempo.

Le mie doti di musicista sono scarsissime e GarageBand, quelle rare volte che ho tentato, mi ha sempre aiutato in modo semplice, intuitivo ed efficace, su qualsiasi apparecchio.

Non mi aspetto aggiornamenti nella calza, visto [quello recente di novembre](https://www.apple.com/newsroom/2017/11/garageband-brings-new-sound-library-and-classic-beat-sequencer/), ma un sacco di altre soddisfazioni per questo 2019 sì.