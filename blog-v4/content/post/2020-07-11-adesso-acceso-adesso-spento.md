---
title: "Adesso acceso, adesso spento"
date: 2020-07-11
comments: true
tags: [FaceTime, Skype, Meet, Zoom, Jitsi, Teams]
---
Un ripassino di UI – User Interface, interfaccia utente – a beneficio di chi ci lavora con la scioltezza di un carpentiere in oreficeria.

Parliamo di un tema semplice: le videoconferenze e i pulsanti per abilitare o escludere microfono e videocamera.

Un interruttore della luce ha due posizioni. Al posto di avere una scritta ON e una scritta OFF c’è ovviamente il feedback visivo, luce accesa o spenta. Nondimeno, un non vedente può sapere se la stanza è illuminata mediante il tatto che rileva la posizione dell’interruttore.

L’interruttore ha quindi il compito di indicare lo stato attuale della situazione.

Adesso veniamo ai software di videoconferenza. Bene o male, tutti hanno un pulsante per il microfono.

Il pulsante per il microfono si può pensare come un interruttore; ha due stati, trasmettiamo l’audio oppure siamo in muto; il microfono è acceso oppure è spento. Per essere comprensibile in tutto il mondo, il pulsante avrà un’icona, che rappresenterà un microfono stilizzato.

Quando il microfono è in funzione, logica vuole che il pulsante sia illuminato e che sia spento quando siamo in muto. Il pulsante videocamera sarà analogamente illuminato se trasmettiamo video, oppure spento se siamo in cieco.

È tutto qui.

Qualche demente ha pensato invece di introdurre il pulsante del muto. Invece di dire che il microfono è acceso e illuminare il pulsante, la girano al negativo: per dire che il microfono è acceso, tengono spento un pulsante di muto. Già gira la testa, vero? Figuriamoci in una conferenza quando di improvviso tocca a noi parlare e avevamo dimenticato di avere attivo il muto.

Si dovrebbe ricordare loro che lo scopo principale dell’applicazione è comunicare; fermare la comunicazione è una opzione. In una interfaccia di base, i comandi devono parlare dello scopo principale e solo dopo passare alle opzioni.

Se proprio il graphic designer ha pensato a una bellissima icona di muto che non si può buttare via, si potrebbe pensare a un compromesso: il microfono acceso è indicato da un pulsante illuminato con l’icona del microfono; quando il pulsante si spegne perché mettiamo in muto, l’icona potrebbe diventare quella di un microfono barrato.

Semplice. Eppure, misteriosamente, professionisti blasonati perdono di vista i fondamentali e iniziano a mettere nell’interfaccia pulsanti negativi, che devono essere spenti perché l’applicazione ottenga il suo scopo principale. Ogni volta che si usa una applicazione progettata al contrario, bisogna rampognare il supporto. Meglio ancora, smettere di usarla, anche se non sempre è possibile.

Vengono intanto in mente certe barzellettacce di anteguerra sui carabinieri.