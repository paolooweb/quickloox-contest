---
title: "Smanettare nel XXI secolo"
date: 2019-10-03
comments: true
tags: [Viticci, Url, iOS, Settings, Impostazioni]
---
Se c’è una ragione per cui *MacStories* è speciale, sta nella sua impossibilità caratteriale di copiare e incollare (infiorettamenti esclusi) le notizie uscite da un’altra parte.

Le notizie ci sono e poi però si trovano gemme come [i centoventi Url](https://www.macstories.net/ios/a-comprehensive-guide-to-all-120-settings-urls-supported-by-ios-and-ipados-13-1/) che dentro iOS consentono di richiamare immediatamente una certa parte delle Impostazioni, senza aprire le Impostazioni dall’icona canonica.

È un elenco che consente di creare semplicissimi Comandi rapidi da piazzare nella schermata preferita per accedere istantaneamente a una certa configurazione.

Esattamente: quello che tanti chiedono lamentandosi che Apple non fa più innovazione e nemmeno ha messo un modo immediato per accedere a quell’impostazione per me tanto importante.

Federico Viticci ha messo insieme l’elenco *a mano*, immaginando come potrebbe essere stata la sintassi di ciascun Url (sintassi poco coerente nell’insieme) e arrivando a stabilire l’Url giusto per tentativi.

Già, una volta si smanettava, si entrava nel sistema, si scoprivano le cose che Apple non voleva si scoprissero, c’era il gusto della scoperta, invece adesso è tutto chiuso e noioso.

Poi uno si sveglia e scopre che il mondo ha continuato a girare. Chi ama smanettare ha pane per i propri denti anche nel XXI secolo. Più che altro, oggi, sono in pochi ad avere fame. La colpa è difficilmente del sistema operativo.