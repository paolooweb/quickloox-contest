---
title: "Sviluppi emozionanti"
date: 2016-03-12
comments: true
tags: [Mac, indie, OSX]
---
Spiegare perché Mac è un computer speciale. Ci ha pensato [Brent Simmons](http://inessential.com/2016/03/11/indies_and_the_app_store) risparmiando a me la fatica di andare oltre una traduzione. La comincio e finisco con il **grassetto**.<!--more-->

**Parlando di app per Mac: dieci anni fa si poteva guadagnare bene scrivendo e vendendo app per Mac.** C’era una comunità di sviluppatori indipendenti piccola ma straordinaria. Questo non è cambiato.

Per molto tempo Mac è stato sottovalutato; prima perché Windows era così diffuso, poi perché sono arrivate le app per il web e oggi per via di iOS. Lungo tutta la mia carriera ho sentito le persone dire che Mac è una scommessa perdente, che è da stupidi scrivere app per Mac.

Nel 2002, in [Why I develop for Mac OS X](http://inessential.com/2002/09/19/why_i_develop_for_mac_os_x), ho spiegato che scrivo app Mac per ragioni emotive.

Sono ragioni tuttora valide e lo sono abbastanza per farmi continuare a scrivere app Mac. Ma la mia esperienza da allora mi dice che scrivere app mac è la migliore scommessa economica per gli sviluppatori indipendenti (specialmente quelli che vendono la app da sé e quindi possono avere versioni di prova e prezzi per l’aggiornamento).

Capisco che possiate non credermi. Lo so che non mi credete. Ma **gli sviluppatori Mac sono stati contro il pensiero dominante – e hanno scritto grandi app – per decenni. Continueremo.**

Un computer che è emozionante programmare, che va contro il pensiero dominante programmare e che può anche dare un ritorno economico programmare è un computer speciale. E chi sta a giocare al piccolo nichilista con i processori che sono tutti uguali si perde qualcosa.