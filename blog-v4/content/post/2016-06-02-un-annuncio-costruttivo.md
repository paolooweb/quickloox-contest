---
title: "Un annuncio costruttivo"
date: 2016-06-02
comments: true
tags: [Swift, Ibm, Wwdc, iPad]
---
Qualsiasi cosa verrà annunciata al [Wwdc](https://developer.apple.com/wwdc/) che comincia il 10 giugno prossimo, ho già una curiosità appagata. Sul [sito di Swift](https://swift.org) infatti si trova già la versione superprovvisoria e nientecollaudata della versione 3 del più recente linguaggio di programmazione di Apple. Nessun dubbio che al raduno dei programmatori si parlerà appunto (anche) del linguaggio di programmazione.

È importante perché la diffusione di Swift, diventato *open source* e per esempio disponibile per Linux, sta facendo [passi da gigante](https://macintelligence.org/posts/2016-04-15-programmazione-aperta/). Avevo già ricordato di come Ibm [lo abbia adottato](https://macintelligence.org/posts/2016-03-03-il-concetto-di-chiusura/) per sviluppare applicazioni dentro la sua infrastruttura cloud. Sono nate iniziative perfino curiose, come la possibilità di [provare Swift direttamente dal *browser*](http://www.runswiftlang.com). Persino su iPad, a [ennesimo dispetto](https://macintelligence.org/posts/2015-02-25-falso-in-scatto-pubblico/) di chi lo reputa inadatto alla programmazione.

Se vogliamo un ambiente informatico respirabile, bisogna contrastare in ogni modo la malvagità di chi vorrebbe essere ovunque e dappertutto soffocando qualsiasi alternativa. Vincere le menti e i cuori dei programmatori è una eccellente strategia di contrasto. Nonché un ottimo viatico perché vengano costruite sempre nuove *app* e sempre migliori.