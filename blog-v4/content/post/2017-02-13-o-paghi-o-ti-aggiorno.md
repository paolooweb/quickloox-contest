---
title: "O paghi o ti aggiorno"
date: 2017-02-13
comments: true
tags: [Windows, C|Net, Hollister]
---
Immagino che se scrivessi qualcosa come segue, passerei per un odiatore di Windows.

>Può darsi che tu stia tenendo una presentazione davanti a un pubblico consistente. Oppure che stia affrontando un esame online. O ancora, che stia lavorando a un progetto con una scadenza serrata.<br />
A Windows non interessa.<br />
Windows prenderà il controllo del tuo computer, lo aggiornerà a forza e premerà automaticamente il tasto reset, senza che tu possa fare alcunché una volta partita la procedura.<br />
Se non hai salvato il tuo lavoro, è perso. Le finestre del tuo browser se ne sono andate. E non aspettarti che duri poco; a seconda della velocità del tuo disco e della consistenza dell’aggiornamento, potrebbero essere dieci minuti o anche più di un’ora.

Non sono io a scriverlo; è [Sean Hollister su C|Net](https://www.cnet.com/news/microsoft-windows-10-forced-updates-auto-restarts-are-the-worst/). *Windows non sistemerà l’aspetto più frustrante di Windows*.

Avevo segnalato situazioni dove gli aggiornamenti forzati di Windows [possono mettere a rischio vite umane](https://macintelligence.org/posts/2016-06-06-il-rumore-di-un-clic/) e di quanto sia stato subdolo, per troppi inconsapevoli, il [passaggio automatico e non necessariamente desiderato](https://macintelligence.org/posts/2016-06-04-volente-o-nolente/) a Windows 10.

L’articolo riporta numerosi *tweet* di gente che commenta la propria esperienza non proprio positiva di aggiornamento inaspettato. Solo il primo:

<blockquote class="twitter-tweet" data-lang="en"><p lang="en" dir="ltr">lost 90mins of work. wish to destroy microsoft. it did a windows update. asked me if i wanted to restart now/later. i chose lata. it ignored</p>&mdash; Baratunde (@baratunde) <a href="https://twitter.com/baratunde/status/10541091">March 21, 2007</a></blockquote> <script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>

Si noti la data. Ha dieci anni. L’articolo di Hollister ha poco più di dieci giorni. Il tempo è passato, niente è cambiato e Microsoft – si riferisce – considera questa, uhm, sollecitudine una funzione irrinunciabile.

Si parla spesso delle esigenze dei professionisti. Chiederei quanto è professionale una sistema che parte da solo nel mezzo di una stampa 3D, di una presentazione, di un calcolo, di una analisi del disco, senza alcun rispetto per il lavoro del proprietario.

A meno che uno non abbia pagato un extra per una versione Pro, Enterprise o Education. Allora si può alzare uno scudo di trentacinque giorni, durante i quali Windows non aggiornerà. (Poi lo farà lo stesso, ma almeno le probabilità di essere colti in un momento sbagliato solo inferiori).

Paghi per riuscire a schivare l’aggiornamento del computer. Su Mac si fa gratis, eh.