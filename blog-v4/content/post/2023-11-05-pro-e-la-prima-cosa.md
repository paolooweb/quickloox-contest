---
title: "Pro è la prima cosa"
date: 2023-11-05T17:46:49+01:00
draft: false
toc: false
comments: true
categories: [Internet]
tags: [Scary Fast, iPhone 15 Pro, Blackmagic]
---
Bella mossa di Apple, che ha pubblicato un fantastico [dietro le quinte della realizzazione di Scary Fast](https://www.apple.com/newsroom/2023/10/behind-the-scenes-at-scary-fast-apples-keynote-event-shot-on-iphone/), l’evento di presentazione dei MacBook Pro M3.

Perché un sacco di gente si è adontata: *come, dicono di avere girato tutto con iPhone 15 Pro eppure il risultato è di una qualità che io mai nella vita…?*

Il punto è che sì, il girato arriva tutto da iPhone 15 Pro (e, se ci si pensa, è qualcosa di incredibile). Poi però ci sono intorno tutte le strumentazioni professionali che servono a girare un film. I film, salvo eccezioni, non si girano tenendo in mano un iPhone e mandando tutto a iMovie. Almeno, non se si vuole raggiungere un risultato di altissima qualità.

istruttivo e spettacolare. Per quanto mi riguarda, risponde a numerose domande che mi sono fatto sulla nascita delle presentazioni digitali di Apple e già solo questo basterebbe.