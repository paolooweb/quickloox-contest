---
title: "Telefonate variabili"
date: 2018-02-26
comments: true
tags: [iPhone]
---
Il giorno prima dice la sua su iPhone X: *alla fine, passata la novità, è solo un telefono*.

Il giorno dopo dice la sua sul rallentamento di iPhone in caso di batteria stanca, che tante polemiche e [class action](https://www.macrumors.com/2018/02/26/iphone-slowdown-class-action-consolidation/) ha causato.

Vorrei rispondergli che telefonare è un’attività a velocità costante, della durata approssimativa di un minuto al minuto.