---
title: "L’allineamento è molto"
date: 2015-04-30
comments: true
tags: [iPhone, Cameron, Jobs, Samsung, S6, Edge, iMore]
---
Parlavo non molto tempo fa di Apple che può permettersi di [verniciare il retro del recinto](https://macintelligence.org/posts/2015-04-12-recinti-e-dettagli), curare anche i particolari che nessuno vede.<!--more-->

Quelli che si vedono e tuttavia possono passare inosservati, tuttavia, sono ancora peggio. Su *iMore* circolano foto che dovrebbero essere più scioccanti di quanto siano: mostrano la [mancanza di allineamenti sui bordi di Samsung Galaxy S6 e S6 Edge](http://www.imore.com/difference-apple-samsung-industrial-design). Scrive l’autore dell’articolo:

>Vale la pena? Per me come cliente, sapere che Apple ha avuto la considerazione e si è presa il tempo e la fatica di allineare il proprio hardware spiega la qualità complessiva del loro lavoro. Mi rassicura sul fatto che la stessa considerazione e la stessa fatica sono probabilmente state messe nell’assicurarsi che non un millimetro o un milliampère di spazio batteria sia stato sprecato, non un nanometro di chip, non un vuoto tra schermo e guscio, o una zona morta nel sensore capacitivo. Certo, saltano fuori problemi in qualsiasi prodotto, ma come dice [il regista] James Cameron, quando punti incredibilmente in alto, perfino i tuoi fallimenti sorpassano i successi altrui.

Se l’allineamento elementare dell’hardware sembra una cosa da poco, se salta fuori l’argomento di qualche euro in meno che giustifica sempre e comunque qualsiasi pochezza, mi spiace: manca qualcosa. E non sono gli euro.