---
title: "Tre definitivi"
date: 2014-11-29
comments: true
tags: [ArsTechnica, Yosemite, Server, iWork]
---
Se non fosse abbastanza la monumentale [recensione di Yosemite](http://arstechnica.com/apple/2014/10/os-x-10-10/), ad *Ars Technica* hanno prodotto anche una [guida per utenti evoluti a OS X Server](http://arstechnica.com/apple/2014/11/a-power-users-guide-to-os-x-server-yosemite-edition/2/) e non contenti hanno completato il programma con la recensione delle nuove versioni di Keynote, Numbers e Pages.<!--more-->

Non ho cuore di raccomandarli perché ce n’è da leggere peggio che per un libro, con livello tecnico e di precisione notevolissimo. D’altro canto, senza averli letti si rinuncia veramente a qualcosa di valido. In un certo senso sono opere definitive.