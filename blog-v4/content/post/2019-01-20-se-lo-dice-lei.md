---
title: "Se lo dice lei"
date: 2019-01-20
comments: true
tags: [Windows, Phone, Mobile, iOS, Android]
---
Il 10 dicembre 2019 termina il supporto di Windows 10 Mobile. Questa la [raccomandazione ufficiale](https://support.microsoft.com/en-us/help/4485197/windows-10-mobile-end-of-support-faq) agli incauti acquirenti di apparecchi di quel tipo:

>passare a un apparecchio Android o iOS. La nostra mission […] ci spinge a supportare le nostre app Mobile su quelle piattaforme e quegli apparecchi.

Questo il commento sulla situazione dell’informatica da tasca:

>La tecnologia si è evoluta assieme alle esigenze e alle aspettative dei vostri clienti e partner, che hanno già adottato piattaforme e apparecchi Android o iOS.

Insomma, più chiaro di così. Mi chiedo che cosa aspettino a dirlo anche agli utilizzatori di computer più grandi, che avrebbero persino più bisogno.