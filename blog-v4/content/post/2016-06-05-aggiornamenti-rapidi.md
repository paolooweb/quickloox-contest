---
title: "Aggiornamenti rapidi"
date: 2016-06-05
comments: true
tags: [Go, Ogs, MacBook, Pro, Air, Usb2, Usb3, Usb]
---
La mia piattaforma preferita per [giocare a Go a turni](https://macintelligence.org/posts/2016-05-31-pietra-su-pietra/) è diventata rapidamente [Ogs](https://online-go.com/). Perdo lo stesso, ma la qualità del servizio è superiore.

Nel [confrontare la carica](https://macintelligence.org/posts/2016-05-29-in-groppa-al-muletto/) trasmessa agli apparecchi iOS dalle porte Usb di MacBook Pro 17” e quella delle porte di MacBook Air 13” non ho considerato che il primo ha porte Usb 2, mentre il secondo ha porte Usb 3. Poi il ragionamento si fa rapidamente complicato perché ci sono un sacco di variabili. Uno ha tre porte, l’altro due. Dovrei capire se internamente hanno uno o più bus. Lo hub che uso con MacBook Air è certamente Usb 2 eccetera.

La comparazione non è mai stata intesa per essere scientifica, insomma, più una curiosità nata dal caso che ha creato un confronto tra macchine e aspettative molto diverse.