---
title: "In transito dalla Social Media Week"
date: 2016-06-09
comments: true
tags: [Smw, Mac]
---
Professionisti allo sbando, abbandonati da Apple nel bel mezzo della [Social Media Week di Milano](http://socialmediaweek.org/milan/).<!--more-->

 ![Regia di un workshop](/images/regia.jpg  "Regia di un workshop") 

La regia di un workshop.

 ![Newsdesk](/images/newsdesk.jpg  "Newsdesk") 

Una redazione al lavoro per intercettare il traffico mediale relativo alla manifestazione.

 ![Accrediti stampa](/images/press.jpg  "Accrediti stampa") 

Agli accrediti stampa.

 ![Montaggi video](/images/video.jpg  "Montaggi video") 

Addetti al video.

Speriamo che Apple rimedi a questa incresciosa situazione [settimana prossima](https://developer.apple.com/wwdc/).