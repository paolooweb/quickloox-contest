---
title: "La visione della foto da vicino"
date: 2017-11-20
comments: true
tags: [Journal, Machine, Learning, Vision, Foto]
---
Il riconoscimento delle facce effettuato sulla libreria fotografica di un’utenza Apple non viene effettuato usando iCloud. Ogni apparecchio cifra le foto prima di inviarle sulla nuvola e pertanto sui server non si trova materiale per fare lavorare gli algoritmi di *deep learning*.

Gli ingegneri si sono pertanto impegnati perché il riconoscimento delle facce nelle foto avvenga localmente, all’interno di ciascun iPhone. Per riuscirci occorre superare alcuni ostacoli rilevanti come esigenze di memoria e di spazio, potenza computazionale, reattività delle risposte, necessità di assicurare la *performance* di tutte le altre attività di iPhone, che l’utente desidera siano istantanee.

Leggi [un articolo del Machine Learning Journal](https://machinelearning.apple.com/2017/11/16/face-detection.html) e impari qualcosa sul riconoscimento facciale e sulle reti neurali, ma anche sulla considerazione per la *privacy* inserita in un iPhone e, per così dire, meno presente su altre piattaforme.
