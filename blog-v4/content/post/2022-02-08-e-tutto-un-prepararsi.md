---
title: "È tutto un prepararsi"
date: 2022-02-08T02:17:54+01:00
draft: false
toc: false
comments: true
categories:
tags:
---
Quest’anno il [Super Bowl](https://www.nfl.com/super-bowl/) viene trasmesso in chiaro dalla Rai (e in cifra da Dazn), per cui non servono acrobazie particolari per guardarlo. Ciononostante, ho visto in vendita sul sito Nfl un [Game Pass](https://www.nflgamepass.com/en) valido trentuno giorni di visione a novantanove centesimi e così mi sono concesso il lusso di guardare la partita domenica notte con il commento originale.

Con la app [NFL Game Pass International](https://apps.apple.com/it/app/nfl-game-pass-international/id1242895411) si arriva su qualsiasi apparecchio, tv compresa; non dovrebbero esserci problemi di confini (e anche se ce ne fossero, accenderò la Vpn).

Le mie figlie sono state esposte a cartoni animati della vecchia scuola e in computergrafica a tre dimensioni; per loro, nate in questo secolo e anche da poco, la differenza è tra *cartoni* e *cartoni fatti bene*. La primogenita ha chiarissimo come creare un cartone animato della vecchia scuola (*lo disegni*), mentre ha chiesto interessata *come si fanno i cartoni fatti bene*. Pertanto ho scaricato [Blender](https://www.blender.org) e inizierò a guardarmi qualche tutorial nei weekend. Non credo di arrivare molto lontano; ma bisogna vedere dove arriva lei.

Grazie a un grande aiuto, la lavorazione di questo blogghino ha subìto una notevole accelerazione e presto si vedranno cambiamenti importanti sulla parte grafica e anche su quella funzionale. Per qualcuno potrebbe anche arrivare una sorpresa carina. Tecnicamente, si lavora su [GitLab](https://gitlab.com).

È bellissimo quando succedono cose. E venire sollecitati a fare cose nuove e diverse.
