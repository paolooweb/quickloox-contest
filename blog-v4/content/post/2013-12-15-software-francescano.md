---
title: "Software francescano"
date: 2013-12-15
comments: true
tags: [sqlite, ImageMagick, Pages, TextWrangler, Photoshop, Cinema4D, Pov-Ray, FileMaker, Movgrab]
---
Gran parte del mio tempo scolastico è stata tempo perso. Alcune lezioni trasversali, mai dentro una materia specifica, sono state preziose. Quella di oggi mi è arrivata da un docente di algebra o geometria al Politecnico di Milano, di cui neanche ricordo il nome.<!--more-->

>Imparate a usare gli strumenti deboli prima di passare a quelli forti.

Ora che ci avviciniamo al 2014 sarebbe bello individuare una giornata del software francescano, a somiglianza del fraticello che il mondo conosce esattamente perché ha buttato alle ortiche gli agi di famiglia e del mestiere.

Per un giorno, riporre [Pages](http://www.apple.com/it/mac/pages/) e darsi a [TextWrangler](http://www.barebones.com/products/textwrangler/).

Ritoccare le foto con [ImageMagick](http://www.imagemagick.org/script/index.php) lasciando stare [Photoshop](http://www.photoshop.com).

Dimenticarsi di [Cinema 4D](http://www.maxon.net/products/cinema-4d-prime/) e dedicarsi a [Pov-Ray](http://povray.org).

Chiudere [FileMaker Pro](http://www.filemaker.com/it/) e aprire [sqlite](http://www.sqlite.org). (È nata [FileMaker Guru](http://www.filemakerguru.it) tra l’altro, comunità italiana imperdibile). Abbiamo un motore di database gratis dentro il software Mac e nessuno lo sa.

Provare a scaricare l’ennesimo video da YouTube con [Movgrab](https://sites.google.com/site/columscode/home/movgrab) invece che [MacX YouTube Downloader](http://www.macxdvd.com/free-youtube-video-downloader-mac/).

Ci siamo capiti. Un giorno solo, come andare in sauna per purificarsi o fare un *weekend* in tenda, rinunciando alle comodità degli elettrodomestici in cambio di un panorama e di una boccata d’aria pulita.