---
title: "La letteratura cattiva"
date: 2023-07-18T02:26:57+01:00
draft: false
toc: false
comments: true
categories: [Internet]
tags: [intelligenza artificiale]
---
Pare che migliaia di scrittori abbiamo deciso di chiedere alle aziende produttrici di modelli linguistici di [smettere di usare le loro opere senza permesso o senza compenso](https://www.npr.org/2023/07/17/1187523435/thousands-of-authors-urge-ai-companies-to-stop-using-work-without-permission).

Si tratta in effetti di uno dei nodi della questione più sottovalutati.

Il problema che vedo io è che nella lista compaiono anche autori di primo piano, come Margaret Atwood e Jonathan Franzen.

Già abbiamo sistemi con problemi di qualità dell‘output. Se la OpenAI della situazione decide di aggirare il problema con la rinuncia all’uso degli autori che scrivono bene, avremo di bassa qualità anche l’input. La classica moneta cattiva che scaccia la buona.

Il secondo problema è che, credo, importi a nessuno.