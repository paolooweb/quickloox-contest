---
title: "Metriche a confronto"
date: 2016-09-27
comments: true
tags: [Acsi, soddisfazione, Satisfaction, AppleWorld.Today]
---
La classifiche di soddisfazione dei clienti, dico io, lasciano il tempo che trovano. Le metriche sono confuse, i dati cui vengono applicate sono ambigui, le conclusioni sono equivoche.

Ciò detto, la classifica dell’[American Customer Satisfaction Index](http://www.theacsi.org) vede Apple al primo posto nella casella Personal Computers.

E fin qui. Ma *AppleWorld.Today* racconta che Mac è al primo posto in quella classifica [da oltre dieci anni](http://www.appleworld.today/blog/2016/9/27/apple-continues-to-lead-in-personal-computer-satisfaction).

Conclusione equivoca da dati ambigui sottoposti a metriche confuse, eppure dotate di una minima coerenza. Più di questo, ho sentito carriolate di volte parlare della perdita di qualità dei Mac e di Apple che pensa solo a iPhone, in questi dieci anni. E vorrei vedere i dati e le metriche di quelle affermazioni, per capire dove mettere più fiducia.