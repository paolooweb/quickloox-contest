---
title: "La porta ovunque"
date: 2016-12-01
comments: true
tags: [MacBook, Pro, Usb-C]
---
Comunque vada a finire con i nuovi MacBook Pro, si leggono contributi notevoli che forse una volta mancavano, sul lato della critica, della provocazione, dell’originalità.<!--more-->

Oggi vince Adam Geitgey che titola [I nuovi macBook Pro sono stimolanti per gli hacker](https://medium.com/@ageitgey/the-new-macbook-pro-is-kind-of-great-for-hackers-64c1c577a4d2#.lapxug9sl). Nell’originale sono *kind of great*, che ho scelto di tradurre senza enfasi. Gli *hacker* sono intesi nel senso originale del termine, quelli che amano manipolare la tecnologia e scoprire nuovi usi.

Il tema dell’articolo è che con le quattro porte Usb-C del portatile si aprono utilizzi prima inusitati e non solo in ambiente Apple. Sintetizzo.

* L’alimentatore carica *qualsiasi* cosa abbia una porta Usb-C deputata.
* Di converso, qualsiasi alimentatore abbia una porta Usb-C può caricare un MacBook Pro se ha la potenza sufficiente.
* MacBook Pro può essere caricato con qualunque alimentatore Usb-C, anche non Apple.
* Anche una batteria supplementare per telefono, se ha la porta Usb-C, può caricare un MacBook Pro.
* Gli adattatori venduti da Apple *funzionano con qualsiasi apparecchio Usb-C*.
* Avendo un monitor con porte Usb-C, si possono attaccare lì tutte le periferiche Usb-C e, per usarle tutte, collegare un singolo cavo dal monitor a MacBook Pro.
* Un computer Usb-C può caricare un altro computer Usb-C.

Così conclude il pezzo:

>Dal punto di vista dell’input/output, il nuovo MacBook Pro è probabilmente l’apparecchio più aperto che Apple abbia mai costruito. Letteralmente, non possiede alcuna porta proprietaria, ma quattro ingressi universali ognuno dei quali può indifferentemente assorbire o erogare corrente, inviare e ricevere dati, trasferire video e audio. Davvero notevole.

>Certo, è fastidioso se possediamo apparecchi più vecchi per i quali servono adattatori. Ma non è necessario acquistare quelli surdimensionati e costosi di Apple. Si possono acquistare minuscoli adattatori Usb-C come quelli mostrati in figura, su Amazon, per somme irrisorie.

>In un anno o due, quando tutti avremo il cassetto dei cavi pieno di connettori Usb-C dal costo quasi nullo, guarderemo indietro e ci chiederemo perché tutti erano così irritati.

E in effetti, questo aspetto traspare veramente poco dalle recensioni lette fino a qui.