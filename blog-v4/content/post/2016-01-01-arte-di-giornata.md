---
title: "Arte di giornata"
date: 2016-01-01
comments: true
tags: [Terminale, Unix]
---
Se l’anno deve cominciare, che cominci dalle basi: [The Art of the Command Line](https://github.com/jlevy/the-art-of-command-line), con tanto di traduzione in italiano per i pigri (no, penso che sia veramente l’ultima volta che accenno alla necessità dell’inglese. Nel 2016 non serve più, o è stato fatto o è troppo tardi).<!--more-->

È solo una pagina, ma ce n’è più che abbastanza per arrivare fino a sera. Chi lo fa davvero, più che ringraziare me, ringrazierà gli estensori della pagina e chi ha avuto l’idea di basare su Unix il motore dei sistemi Apple.

Buon anno!