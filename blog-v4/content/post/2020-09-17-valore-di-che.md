---
title: "Valore di che"
date: 2020-09-17
comments: true
tags: [Amazon]
---
Amazon [erogherà corsi online](https://press.aboutamazon.com/news-releases/news-release-details/amazon-provide-computer-science-education-more-550000-k-12) a cinquecentocinquantamila studenti K-12 (fino alla terza media, per noi) in oltre cinquemila scuole, nell’ambito del programma [Amazon Future Engineer](https://www.amazonfutureengineer.com).

In Italia esiste ancora il valore legale del titolo di studio. E uno dice, che c’entra?

Amazon Future Engineer, se arrivasse in Italia, formerebbe migliaia di futuri ingegneri autodidatti, che riceverebbero un attestato di nessun valore legale. Eppure, sarei pronto a scommettere, il novanta percento di loro troverebbe un lavoro senza pretese che valorizza la loro formazione.

Sceglierei, dovessi reclutare ingegneri, gente formata da Amazon o laureati, il primo ateneo che mi viene in mente, al Politecnico di Milano? Nessun dubbio, io sceglierei un laureato.

Il laureato penso mangerebbe mediamente in testa agli autodidatti e, entrasse in azienda anche lui, diventerebbe in breve tempo il loro capo.

Il problema è che questa sorte benigna si applicherebbe a un laureato su dieci. Gli altri, per quanto ingegneri provetti, semplicemente non troverebbero occasioni per mettersi alla prova nel loro campo, saturato dagli ingegneri *made in Amazon* che hanno scelto una strada più breve e sono arrivati prima sul mercato a soddisfare la domanda.

Interessasse l’ingegneria, daresti alla prole l’opportunità di trovare lavoro in fretta con il problema di dover fare carriera senza averne i mezzi culturali e di competenza, oppure la *chance* di essere un capo di domani, con una percentuale di rischio del novanta percento?

In tutto questo, del valore legale del titolo di studio importa zero. E allora, visto che in pandemia tutti si sono accorti di quanto la scuola abbia bisogno di riforme, perché non cominciare dall’abolizione di un valore che di fatto non lo è?