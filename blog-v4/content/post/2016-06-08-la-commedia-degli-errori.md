---
title: "La commedia degli errori"
date: 2016-06-08
comments: true
tags: [errori, OSX, iOS, Apple]
---
Finora la risorsa più completa che ho trovato sui codici di errore dei sistemi operativi Apple è [questa pagina di discussione](https://discussions.apple.com/thread/4940204), con dentro un file davvero utile e vari link ad altre pagine meno complete.<!--more-->

Trovo tuttavia dissonante (da molti anni) che chi realizza i sistemi tecnologici più usabili e comprensibili al mondo non si periti di fornire una volta per tutte una fonte chiara, semplice e completa da consultare quando appare un astruso errore numerico. Molte volte una ricerca su Google chiarisce la questione; alcune, no.

Costa anche relativamente poco; i codici di errore non cambiano dalla sera alla mattina e mantenere aggiornato un piccolo database online costituirebbe un grande servizio erogato con uno sforzo minimo.

Quasi quasi, un progetto *open source*…