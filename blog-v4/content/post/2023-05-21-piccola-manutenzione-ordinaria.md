---
title: "Piccola manutenzione ordinaria"
date: 2023-05-21T01:24:22+01:00
draft: false
toc: false
comments: true
categories: [Web]
tags: [blog, Macintelligence, QuickLoox, BBEdit, regex]
---
Dentro un vecchio post ho trovato alcuni link interni che non funzionavano, tutti risalenti a una vecchia versione del blog in cui non c’era ancora la navigazione di https ma, soprattutto, la sintassi dei nomi file cambiava,

Per essere sicuro di non fare pasticci, ho lanciato una ricerca e sostituzione di massa cercando tutti gli URL interni del blog in tutti i file. Grazie a una espressione regolare sempliciotta ho cercato l’intero URL, dall’inizio alla fine. E ho chiesto di cambiarlo nella versione corretta.

In due secondi BBEdit ha individuato e corretto seicentotrentadue link, che ora funzionano.

È una goccia nel mare rispetto a quanto servirebbe fare e, contemporaneamente, una buona soddisfazione, dato che grazie a questo intervento la base di codice diventa ancora più omogenea.

Ci sono tremila post cui dare un nome file valido, altri da recuperare scavando nell’Internet Archive… un giorno verrà fatto. Per oggi, bello che la navigazione interna sia più vicina alle aspettative.