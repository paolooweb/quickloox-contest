---
title: "Ultime spiagge"
date: 2020-05-20
comments: true
tags: [Microsoft, Vsc, Linux, OpenBsd, FreeBsd]
---
La politica è una faccenda complicata e spesso fatta di strategie che vanno a vantaggio del consenso per una parte ma non per i cittadini (e in questi casi è politica cattiva).

Una di queste strategie è abbracciare una visione per guadagnare il consenso, incassare il consenso stesso e poi tipo vent’anni dopo dire candidamente *ci eravamo sbagliati*, dopo avere terminato di godere dei vantaggi ottenuti grazie all’errore.

Veniamo all’open source e a Microsoft, un cui alto dirigente [ha dichiarato](https://www.theverge.com/2020/5/18/21262103/microsoft-open-source-linux-history-wrong-statement) questo:

>Quando l’open source è esploso all’inizio del secolo, Microsoft si trovava sul lato sbagliato della storia e l’ho vissuto a livello personale.

Così vent’anni dopo siamo all’offensiva della simpatia, all’open source adesso vogliono tanto bene.

E se lo comprano, a partire da GitHub. Lo compenetrano, a partire da milioni di righe di codice per Linux. Lo sbandierano, invitando all’adozione di strumenti come Visual Studio Code.

Non so quando lo vedremo, ma lo vedremo. A un certo punto Microsoft avrà voluto tanto bene all’open source da essere in grado di dirigerlo come un burattino, in controllo dei progetti di mezzo mondo, in controllo dello sviluppo di Linux, in controllo degli strumenti di sviluppo, in controllo di tutto.

Rimarranno le ultime spiagge. Come è successo negli anni novanta, quando Internet Explorer si stava fagocitando il web. Per fortuna qualcuno mise a punto browser alternativi e standard se non buoni, almeno, decenti. Microsoft ne rimase fuori e perse.

Oggi ha imparato bene la lezione e non solo si trova ovunque nei gruppi che definiscono gli standard ma, appunto, nell’impossibilità di evitare che la gente ragioni open, si premura che lo faccia secondo le sue impostazioni. Che domani, nella sua visione, diventeranno così ubique da essere obbligatorie di fatto.

A salvarci saranno eroi del nostro tempo che ancora quasi nessuno conosce. [FreeBsd](https://www.freebsd.org) va sostenuto, per esempio. [OpenBsd](https://www.openbsd.org/67.html) va sostenuto; è appena uscita la sua versione 6.7.

Loro e altri saranno le ultime spiagge, le possibilità finali di usare tecnologie veramente aperte e veramente libere quando sarà stata perfezionata la Grande Omologazione. L’ultima difesa.

I tempi sono lunghi, ne parleranno soprattutto le prossime generazioni. Intanto bisogna mettere le mani avanti e avvisare.