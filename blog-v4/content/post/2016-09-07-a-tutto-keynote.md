---
title: "A tutto keynote"
date: 2016-09-07
comments: true
tags: [MacBook, Air, watch, iPhone, Freesmug, Irc, Snell]
---
Ho già scritto [ieri](https://macintelligence.org/posts/2016-09-06-air-o-non-air-questo-e-il-problema/) dei miei interessi rispetto al *keynote* che parte [alle 19 ora italiana](http://www.apple.com/apple-events/september-2016/).<!--more-->

Ho chiesto a Tim Cook di non fare annunci di martedì, perché l’agenda mi è sempre sfavorevole. Mi ha ascoltato ma l’agenda, gelosa, sta cercando ugualmente di boicottarmi.

Proverò lo stesso a essere almeno parzialmente presente sul canale Irc `#freesmug` del server `irc.freenode.net`, dando il comando `/join #freesmug` dentro un programma atto allo scopo, su uno dei numerosi computer a disposizione. Con gratitudine infinita verso [Gand](http://freesmug.org) e più che mai con l’invito a sostenere il software libero soprattutto quando è per Mac.

Se c’è qualcuno lo saluto più che volentieri. Magari arrivo in leggero o pesante ritardo, magari devo congedarmi in anticipo, non lo so, però ce la metto tutta e spero proprio di poter fare quattro chiacchiere piacevoli, anche se riguardano la presa jack audio oppure la sua mancanza. La migliore anteprima scritta finora per prepararsi è quella di [Six Colors](https://sixcolors.com/post/2016/09/what-to-look-for-at-wednesdays-apple-event/).

A più tardi!