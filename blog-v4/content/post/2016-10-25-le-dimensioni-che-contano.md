---
title: "Le dimensioni che contano"
date: 2016-10-25
comments: true
tags: [Finder, Mac]
---
Nel Finder si trova una cartella con centoquattordici immagini e c’è un problema di file che non devono superare i quattro milioni di pixel totali.

Non i quattro megabyte: i quattro megapixel. Non è lo stesso.

Il programma che li deve trattare ha un problema se le immagini superano appunto i quattromila pixel; ma segnala solo le prime dieci che superano il limite e poi rimanda la questione all’operatore.

Il quale, essendo su un Mac, avvia una ricerca per numero totale di pixel. Una delle decine e decine di metriche che non appaiono immediatamente nella finestra di ricerca, ma ci sono e – come in questo caso – possono contare.

Ricerca di tutte le immagini con numero di pixel uguale o maggiore di quattro milioni ed ecco scremata la cartella in pochi secondi.

Ci sono momenti in cui usare Mac, dopo tanti anni, ancora ti gratifica e questo è stato uno di loro.