---
title: "Il mistero dell’Rss perduto"
date: 2016-01-23
comments: true
tags: [Vienna, Rss, Feedly, feed]
---
Più di uno mi ha segnalato problemi nel ricevere il *feed* Rss di questo blog da una decina di giorni e sono colpevole di non avere ancora studiato a fondo il problema.<!--more-->

Ciò che posso dire è che a me i *feed* arrivano e non si sono mai interrotti. Uso [Vienna](http://www.vienna-rss.org) su Mac. Non ho ancora collaudato altri programmi su altre piattaforme (per esempio [Feedly](http://feedly.com/i/welcome) per iOS, dove mi pare più d’uno riscontri l’interruzione).

Questo mi porta a pensare che il problema non sia un *feed* guasto (funziona, lo vedo) ma un *feed* sporco, oppure un qualche problema dei programmi.

Forse anche tutte e due le cose insieme. Ho provato a validare il *feed* e [ha passato il test](http://feedvalidator.org/check?url=http://macintelligence.org/atom.xml), solo che vengo avvisato di due anomalie che potrebbero causare incompatibilità con alcuni programmi.

Una è la presenza nel *post* del 13 gennaio [di un video YouTube](https://macintelligence.org/posts/2016-01-13-sense-of-doubt/) e l’altra, il 14 gennaio, [di un tweet](https://macintelligence.org/posts/2016-01-14-un-posto-migliore/).

Guarda caso, tutti segnalano l’interruzione a partire dal 13 o dal 14 gennaio. Sono quindi portato a pensare che l’origine dell’inconveniente sia questa. Anche se, e ciò mi rende perplesso, non è certo la prima volta che incorporo un *tweet* o un video in un *post* e nessuno se ne è mai lamentato.

Come risolvere?

Posso cancellare il video e il *tweet*, però mi dispiace un po’. Quando capita l’occasione, mi piace avere *post* ricchi visivamente.

Posso ignorare il problema per sempre (il *feed* funziona), ma mi dispiace per chi non riesce più a leggere il *feed*. È una scomodità.

Posso ignorare il problema momentaneamente; a un certo punto, *post* dopo *post*, gli articoli problematici usciranno dal *feed* e prevedibilmente tutto tornerà come prima. È l’opzione che mi piace di più; d’altro canto, è possibile che un successivo inserimento di video o *tweet* provochi nuovamente l’interruzione del servizio. Non era mai successo prima e pertanto non so dire. Certo non posso escludere che accada e allora la soluzione sarebbe davvero poco efficace.

Posso evitare da qui e per sempre di caricare video o *tweet* sul blog, tuttavia mi sembra un atto sproporzionato che impoverisce il blog stesso. Non mi piace.

Posso chiedere a chi ha problemi di verificare se veramente si devono al programma specifico che usano, provandone un altro con [lo stesso feed](http://macintelligence.org/atom.xml). Forse è la soluzione più pratica. Suona però come un invito a cambiare programma e non è bello; preferirei riuscire a trovare una soluzione che preserva tutti.

Credo di avere esposto il caso con completezza. Attendo pareri e intanto ringrazio in modo speciale chi è stato così gentile e sollecito da inviarmi una segnalazione.