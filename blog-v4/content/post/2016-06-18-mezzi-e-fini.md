---
title: "Mezzi e fini"
date: 2016-06-18
comments: true
tags: [Kickstarter, Akko, ScreenSavrz, RadTech, MacBook, Pro]
---
L’amico [Blue](http://bluebottazzi.blogspot.it) ha pubblicato [ahimé su Facebook](https://www.facebook.com/groups/applerebels/permalink/632103173605340/) una lunga critica degli strumenti software disponibili su Mac, ieri e oggi, per concludere che lo ieri era molto meglio. Tra le altre cose, dice questo:

>- authoring: con HyperCard (e SuperCard) creavo ogni sorta di applicazione anche assolutamente Pro, dai giochi al multimediale alle utilities. Con AppleScript facevo comunicare le applicazioni; con FileMaker creavo gestionali. Oggi resta solo l’ultimo.

[AppleScript](http://macosxautomation.com/applescript/) continua a funzionare e permette di realizzare ogni sorta di applicazione, ma non è questo il punto. Su ogni Mac, oltre ad AppleScript, è presente Automator e neanche questo è il punto. È sparito AppleScript ma c’è [Automator](http://macosxautomation.com/automator/). Sotto il cofano ci sono linguaggi di programmazione a tutti i livelli per tutti i gusti. È possibile creare app preconfezionate con strumenti indipendenti che sono più di uno, di solito gratuiti. Non mi dilungo perché il punto non è neppure questo. [Swift](https://swift.org) è troppo difficile? Sta arrivando [Swift Playgrounds](http://www.apple.com/swift/playgrounds/) e poi dobbiamo capirci bene su che cosa significhi difficile. Il codice di HyperCard era tutt’altro che semplice. Comunque, il punto eccetera eccetera.

Ci arrivo, al punto. La testata *Politico* voleva distribuire in modo efficace e innovativo notizie sul referendum britannico per la permanenza nell’Unione Europea.

Ha usato [Wallet su iOS](http://www.niemanlab.org/2016/06/politico-europe-is-using-apple-wallet-yes-apple-wallet-to-send-readers-notifications-about-brexit/). Wallet! In teoria serve a memorizzare biglietti di aerei, ferrovie, concerti eccetera. Un porta documenti. *Politico* lo ha trasformato in un sistema di distribuzione mirato di notizie. Sta funzionando, ai lettori sembra che piaccia.

Chiunque avesse pensato in modo convenzionale avrebbe tirato su una newsletter, un forum, una feed Rss, una mailing list, tutte cose già viste. *Politico* si è guardata in giro e ha fatto una cosa assolutamente innovativa, che gli richiede molto meno impegno e ha pure successo.

Il punto è questo: pensare in modo convenzionale, in questo mondo pieno di possibilità e libertà fino a farti soffocare, non aiuta. Voglio bene a Blue, ma lui non vuole raggiungere obiettivi oggi: vuole continuare a usare le applicazioni di ieri. Ieri per andare al cinema facevi la cosa e arrivavi alla cassa un’ora prima per prenderti un buon posto; oggi c’è il biglietto numerato, entri trenta secondi prima della proiezione e magari compri il biglietto con la *app* mentre raggiungi il cinema. Lamentarsi che le casse del cinema non sono più quelle di una volta non è producente.

Oltretutto ci sono su App Store due milioni di *app*. Si faccia tutta la tara che si vuole e rimane sempre una quantità di applicazioni utili e ben fatte devastante rispetto al passato. Sta veramente andando così male? Non credo. Invece, i fini sono rimasti sempre quelli. I mezzi sono cambiati vertiginosamente. L’unica cosa sensata e conoscere il nuovo.

*[Scoprire in modo comprensibile come cominciare a programmare su Mac e magari su iOS è uno degli obiettivi della [iniziativa ultraeditoriale di Akko](https://macintelligence.org/posts/2016-06-15-un-kickstarter-per-cambiare-il-libro/). [Diffondere la notizia della sua esistenza e magari finanziarla ](https://www.kickstarter.com/projects/295108589/cuore-di-mela-i-segreti-di-apple-iphone-e-mac) potrebbe anche favorire la nascita di nuovi programmatori.]*