---
title: "L’angolo delle illusioni"
date: 2023-11-01T16:58:23+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [macOS, Sonoma, Eclectic Light Company, iCloud]
---
iCloud è un meccanismo software sempre più complicato e per fortuna abbiamo *The Eclectic Light Company* – lettura sempre più raccomandata – a spiegare come [cambiano le cose in proposito dentro Sonoma](https://eclecticlight.co/2023/10/30/how-macos-sonoma-has-changed-icloud-even-more/).

La lettura è raccomandata quanto la fonte perché fa luce su dinamiche che alla gran parte di noi risultano oscure fino a che ci sbattiamo contro in qualche momento della sincronizzazione, della copia, degli alias o altro.

Vengono anche citate un paio di app per visionare lo stato effettivo dei file in iCloud, fuori da quanto afferma ufficialmente il sistema.

Per l’autore sono programmi che aiutano a penetrare le molte illusioni create da iCloud nello svolgere il proprio lavoro e raramente una terminologia è stata più azzeccata.