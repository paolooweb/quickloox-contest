---
title: "Widget e no"
date: 2015-08-17
comments: true
tags: [widget, Konfabulator, paoloo, Macintosh, Lisa, Hertzfeld, Dashboard, JavaScript]
---
Sono grato a **paoloo** per avere commentato in modo interessante il [post su Status Board](https://macintelligence.org/posts/2015-08-09-come-non-usa/). Trovo discutibili alcune sue affermazioni e le commento a mia volta, per aggiungere informazioni alla discussione.

>Non ho mai sentito di widget nel Lisa, mi sono perso qualcosa.

Assolutamente nulla perché nessuno li chiamava così nel 1983. Ma Lisa aveva due modalità di utilizzo, l’Office System e l’ambiente di sviluppo. L’Office System comprendeva sette applicazioni, *più* cose come calcolatrice e orologio. Che non venivano chiamate *widget*, certo.

>A livello di interfaccia utente il widget è un software adibito spesso alla sola visualizzazione di dati o comunque con pochissima interazione da parte dell'utente e funzionalità ristrette.

Esatto, come calcolatrice e orologio in Lisa.

>Non ha bisogno di una menu bar, in definitiva, e ciò lo rende tecnicamente / concettualmente differente da un'applicazione.

L’abitudine ad avere una barra dei menu non significa che sia un requisito. Vogliamo dire che [WordStar](http://www.wordstar.org) o [VisiCalc](http://bricklin.com/visicalc.htm) non erano applicazioni? (Di passaggio: [WordTsar](http://wordtsar.ca/), per i curiosi, non è ancora funzionante su [El Capitan](https://www.apple.com/it/osx/elcapitan-preview/)). Per non dire dei giochi: [Hearthstone](http://eu.battle.net/hearthstone/it/?-) è privo di barra dei menu. [emacs](https://www.gnu.org/software/emacs/) ha mille funzioni tra le quali l’editing di testo e la programmazione, e non ha alcuna barra dei menu, vivendo nel Terminale. [Stellarium](http://www.stellarium.org/it/) è un planetario da scrivania senza barra dei menu.

>Quando è stata chiamata in causa per aver plagiato Konfabulator con Dashboard, Apple si è difesa sostenendo come i widget li avesse creati già a partire dal System…

E difatti. Basta leggere il [resoconto di Andy Hertzfeld](http://www.folklore.org/StoryView.py?project=Macintosh&story=Desk_Ornaments.txt), testimone di prima mano, datato 1981. *Anche se possiamo eseguire solo una applicazione importante per volta, non c’è ragione per rinunciare a piccole applicazioni in miniatura che funzionano contemporaneamente nelle proprie finestre*.

>Apple, furbescamente, sa che dal punto di vista di un utente (come può esserlo un Giudice) questi possano apparire la stessa cosa, peccato che i “widget” del System venivano programmati con lo stesso linguaggio delle applicazioni, non facevano parte di un ambiente di runtime a sé stante ma giravano nel sistema come le normali applicazioni.

Non erano normali applicazioni perché potevano essere chiamate solo dal menu Apple. E le normali applicazioni potevano girare solo una per volta; gli *Accessori di Scrivania* potevano essere in funzione più di uno nello stesso momento. A margine: nessun giudice si è mai occupato della questione Dashboard-Konfabulator. Apple detiene un [brevetto](http://patft.uspto.gov/netacgi/nph-Parser?Sect1=PTO2&Sect2=HITOFF&u=%2Fnetahtml%2FPTO%2Fsearch-adv.htm&r=1&f=G&l=50&d=PTXT&p=1&p=1&S1=8,321,801.PN.&OS=pn/8,321,801&RS=PN/8,321,801) sulla presentazione di *widget* in uno strato distinto da quello delle applicazioni, dove da nessuna parte si distinguono applicazioni da *widget* in base al linguaggio di programmazione utilizzato.

>I widget di Dashboard/Konfabulator vengono programmati in HTML/javascript/css, mica in Objective-C, e girano sopra un ambiente a parte che interpreta i linguaggi del web.

E allora? L’unica novità introdotta da Konfabulator (e plagiata da Apple) è stata l’uso del linguaggio JavaScript. Che però oggi [può essere usato per sviluppare applicazioni](http://www.apogeonline.com/webzine/2014/09/12/gradita-sorpresa)! Pretendere di distinguere applicazioni e *widget* dal linguaggio, ancora una volta, non regge.

Riassumo. In embrione, miniapplicazioni distinte dalle applicazioni canoniche le aveva anche Lisa. Macintosh le ha formalizzate come accessori da scrivania. OS X ha preso le stesse miniapplicazioni trasformandole in applicazioni come le altre. Konfabulator ha ripreso il concetto con l’idea di usare JavaScript e Html. Dashboard ha copiato l’uso di JavaScript e Html. Oggi, con JavaScript e Html, si può scrivere indifferentemente un *widget* o una applicazione.

Aggiungo per concludere: Mac OS aveva la *striscia di controllo*. Programmata per forza nello stesso linguaggio delle applicazioni, in un ambiente separato. Miniapplicazioni con interazione minima. Perché non avrebbero dignità di widgettitudine? La verità è che in informatica le classificazioni valgono sempre un tanto al chilo e vanno prese con amplissimo beneficio di inventario. Specie se consistono in una parola coniata da qualche marketing.

 ![Striscia di controllo in Mac OS](/images/control-strip.png  "Striscia di controllo in Mac OS")