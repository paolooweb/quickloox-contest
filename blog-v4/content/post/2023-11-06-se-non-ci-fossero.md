---
title: "Se non ci fossero"
date: 2023-11-06T17:58:50+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [macOS, Sonoma, Ars Technica]
---
Andrebbero inventati, quelli di *Ars Technica* al momento di recensire macOS. John Siracusa [si è ritirato da tempo](https://arstechnica.com/gadgets/2015/04/after-fifteen-years-ars-says-goodbye-to-john-siracusas-os-x-reviews/), però [la loro recensione di macOS Sonoma](https://arstechnica.com/gadgets/2023/09/macos-14-sonoma-the-ars-technica-review/) è in assoluto la migliore.

Unico difetto, che poi è un pregio: bella lunga. Se si fossero dimenticati qualcosa, peraltro, forse uno neanche si accorgerebbe, con tutto quello che c’è. Ecco, manca uno sguardo sotto al cofano, a livello di Terminale. Posto che sia cambiato qualcosa di percepibile e significativo.