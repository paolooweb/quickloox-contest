---
title: "Tre virgole"
date: 2015-07-27
comments: true
tags: [OSX, ElCapitan, Gpg, Mail, Mailsmith, Amazon, Kindle]
---
Con leggero ritardo sul dovuto, sono finalmente al timone della beta pubblica di [El Capitan](https://www.apple.com/it/osx/elcapitan-preview/).<!--more-->

Ho appena installato e aggiornato, quindi un parere almeno vagamente approfondito arriverà tra qualche giorno.

Agli ansiosi della compatibilità riferisco che l’unica applicazione dichiarata esplicitamente incompatibile dal software (e inserita in una cartella apposita) è [Kindle di Amazon](https://itunes.apple.com/it/app/kindle/id405399194?l=en&mt=12).

I controlli automatici di OS X sono comunque per forza limitati e ci sono un paio di aggiunte da fare.

Il plugin di [Gpg](https://gpgtools.org) per Mail, come succede regolarmente in tutte le beta dall’inizio dei tempi, non funziona e viene disabilitato. Mailsmith va in crash tentando di inviare un messaggio.

Mi aspetto che tutto sarà sistemato per la versione definitiva. Mi sorprende Amazon, perché per clienti paganti è un pessimo servizio trovarsi tagliati fuori. D’altro canto va ricordato che il software beta è a rischio di chi lo installa. È strano che Amazon si prenda tempo per assicurare la compatibilità, non che quest'ultima salti per aria in una versione provvisoria di OS X.

**Aggiornamento:** sono passato alla seconda versione della beta pubblica e il comportamento di [Mailsmith](http://www.mailsmith.org) è tornato regolare. Ho scoperto invece che l’installazione ha cancellato gli strumenti di Terminale di BBEdit. È bastato reinstallarli dal comando di menu apposito. Pixelmator presenta qualche stranezza in sede di ridimensionamento delle immagini. Non ho verificato se sia pronto un aggiornamento. Mail ha riattivato alcuni account che avevo disattivato. Li ho disattivati nuovamente.

**Nuovo aggiornamento:** la beta 3 ha regolarizzato Pixelmator.