---
title: "La pioniera dimenticata"
date: 2023-10-09T15:20:05+01:00
draft: false
toc: false
comments: true
categories: [Hardware]
tags: [Digital Equipment Corporation, DEC, VAX]
---
Se ricordo bene, la comprò Compaq. Non ricordo chi comprò CompaQ in seguito e probabilmente non esiste più. Digital Equipment Corporation (Dec) fu un colosso informatico fino alla nascita del computing personale e finalmente c’è un articolo di *Ars Technica* a [raccontarne l’epopea](https://arstechnica.com/gadgets/2023/10/long-gone-dec-is-still-powering-the-world-of-computing/).

Dec raggiunse il suo apice alla fine dell’era dei minicomputer. Faceva ottimo hardware e, per i tempi, ottimo software. Compì anche diverse scelte visionarie, solo che fallì completamente nell’intercettare l’utilizzo dei computer su base individuale. Un fatto di cultura interna, che impedì di comprendere il cambiamento in corso. Il fondatore di Dec, Ken Olsen, aveva dichiarato in anni precedenti che il mondo avrebbe avuto bisogno complessivamente di cinque o sei computer, per dire del tipo.

Ciò che rende diversa Dec da tutte le altre storie di produttori di computer passati dalle stalle alle stelle è che sostanzialmente i loro numerosi fallimenti sul lato dello hardware e specialmente dei processori hanno dato vita a sviluppi cui tutti continuiamo a dovere qualcosa e non solo nel solito modo retorico.

È il motivo per leggere questa piccola storia di Dec e perché ne parliamo. Termino con un estratto vicino allla fine dell’articolo:

>Salta fuori che esiste una connessione anche tra Apple e DEC. Dan Dobberpuhl è stato un progettista originale del processore Alpha prima di lasciare DEC e avviare una società di progettazione di chip chiamata P.A. Semi, acquisita da Apple nel 2008 per il talento nel talento nel lavorare con i processori Arm. Da qui sono stati sviluppati i chip usati in iPad e iPhone e infine i processori M1 e M2 usati su Mac. […] Chi sta leggendo questo su un computer o un iPad, deve ringraziare DEC.

Il discorso sui processori, da solo, vale il prezzo dell’articolo e mostra una volta di più la colossale inettitudine di Intel rispetto alla lettura del mercato mobile. Avevano in mano di fatto Arm ma non credettero e vendettero tutto, prima che arrivasse Apple con una lettura sicuramente più azzeccata.

A parte questo, comunque, sto scrivendo questo post su iPad dopo avere sempre su iPad letto l’articolo. È il caso dunque di ringraziare Dec e rendere testimonianza.