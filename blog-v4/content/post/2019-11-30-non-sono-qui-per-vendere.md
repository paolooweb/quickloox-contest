---
title: "Non sono qui per vendere"
date: 2019-11-30
comments: true
tags: [Apogeo, Black, Friday]
---
Lavoro per Apogeo, che da quest’anno ha aggiunto i corsi di formazione alla classica offerta editoriale e durante il Black Friday ha pubblicato una pagina su [come regalare un corso](https://www.apogeonline.com/regala-un-corso/).

Prima del Black Friday la pagina non c’era e, in fase di progettazione dell’evento, l’idea dei regali era stata scartata per difficoltà di implementazione.

Poi è successo che la promozione ha funzionato; al punto che sono arrivati acquirenti di corsi che ci hanno contattato per chiedere proprio come regalarli.

Le difficoltà sono state superate di slancio e sono rimasto molto colpito dalla domanda.

Siamo abituati (socialmente, oltre Apogeo) a valutare l’acquisto in area tecnologica sulla base del metro personale; vedo persone benestanti scrollare la testa con un cenno grave davanti a una app da un euro e ventinove; si legge, si sente, si parla di soldi che non ci sono, taglio dei budget, obiettivi sempre più stressanti eccetera.

Poi arrivano persone che certo approfittano di uno sconto, ma per regalare una giornata di formazione da duecento o trecento euro.

È un segnale straordinario di stima, nelle persone, nella stessa Apogeo, confidenza nelle opportunità da cogliere, fiducia nel futuro. O almeno voglia di sfidarlo a testa alta. Non lo avrei mai detto e ho molto da imparare a questo riguardo.

È un blog personale e non sono qui per vendere un corso Apogeo; ma l’idea di regalarne uno, lavorare per l’interesse personale con l’investimento su chi ci sta intorno, accidenti che bello se la comprassimo tutti.
