---
title: "Maturo per un pubblico"
date: 2023-06-28T01:11:12+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [FaceTime, SharePlay, FreeForm]
---
Un grosso punto di soddisfazione per le [registrazione del podcast effettuata ieri](https://macintelligence.org/posts/2023-06-27-un-pulsante-al-prezzo-di-due/) è che, per la prima volta a mia conoscenza, siamo riusciti a collegarci via [FaceTime](https://support.apple.com/en-us/HT204380) e a restare in FaceTime per tutta la serata.

Se per la comunicazione estemporanea FaceTime è meraviglioso, ci sono voluti anni perché acquisisse certe sfumature che contano in una videoconferenza tecnicamente complessa come è di fatto un podcast. Ieri sera ho risposto alla chiamata FaceTime con iPhone e poi l’ho trasferita a Mac, per esempio, in modo da poter usare il microfono giusto. C’è chi aveva l’esigenza di commutare in corsa la videocamera e ha potuto verificarne con successo la fattibilità in FaceTime. E così via, compresi momenti di instabilità della connessione che avrebbero potuto mettere a rischio la continuità della registrazione.

FaceTime continua a seguire una linea di sviluppo diversa da quella di uno strumento dedicato per videoconferenze, tipo [Zoom](https://www.zoom.us/it) per capirci, e giustamente. Nonostante questo, sembra arrivato il momento di poterlo proporre come soluzione *guerrilla* in certe situazioni aziendali dove l’equipaggiamento di serie è del tutto diverso ma tutti usano un iPhone, chi non lo usa potrebbe ricevere un link da usare sul proprio Pc e serve una connessione sporca maledetta e subito invece di attendere tempi biblici per l’invio di qualche invito più convenzionale.

È la solita storia di Apple che si fa strada nelle aziende grazie alle sue strategie consumer. [SharePlay](https://support.apple.com/guide/iphone/shareplay-watch-listen-play-iphb657eb791/ios) nasce per condividere un film o una canzone tra amici ma è una alternativa assolutamente praticabile alla oramai tossica condivisione dello schermo, l’alibi supremo, il *liberi tutti* di occuparsi di qualsiasi altra cosa fino a quando rimani e lo schermo condiviso.

E [Freeform](https://www.apple.com/it/newsroom/2022/12/apple-launches-freeform-a-powerful-new-app-designed-for-creative-collaboration/)? Forte di qualche esperienza con [Miro](https://miro.com/), posso dire che l’ambiente collaborativo dedicato resta complessivamente superiore. Freeform ha però dalla sua la semplicità e l’immediatezza, senza quella patina di malinconia obitoriale tipica delle  lavagne elettroniche semplici in quanto troppo indietro nello sviluppo o perché la decisione di inserirle in un programma è stata assolutamente tardiva.

Insomma, pensare di proporre FaceTime come piattaforma di collaborazione e comunicazione aziendale è poco plausibile, oggi. Invece, creare isole FaceTime per gruppi di lavoro delimitati è diventata decisamente una possibilità. C’è un pubblico potenziale e il tempo è finalmente maturo.