---
title: "La conferma della regola"
date: 2015-12-29
comments: true
tags: [Ahmad, Berenberg, azioni, titolo]
---
È sempre brutto parlare della perdita di posti di lavoro, anche se il posto di lavoro è uno solo. Bisogna però parlare dei fatti eccezionali, intesi nel senso dell’eccezione ed ecco perché merita attenzione il fatto che un analista sia stato licenziato.<!--more-->

Si chiama Adnaan Ammad e lavorava per Berenberg Bank, la quinta banca più vecchia del mondo. Tre anni fa, [spiega Philip Elmer-DeWitt su Fortune](http://fortune.com/2015/12/28/apple-doomsayer-analyst-fired/), Ahmad

>ha iniziato a predire la caduta di Apple e una quotazione delle azioni a sessanta dollari, per cambiare cinque mesi dopo da Acquistare a Vendere l’indicazione di base per gli operatori sul titolo.

La scorsa primavera, mentre il titolo Apple viaggiava a 124 dollari per azione, Ahmad ha alzato la sua previsione a 85 dollari, ultimissimo tra quaranta e più colleghi, senza cambiare le proprie previsioni pessimistiche. A ottobre ha inviato una nota ai clienti che riporto in originale, perché trovo impossibile rendere in italiano l’aria generale di nonsenso:

>For Apple, Mr. Market is questioning volumes and growth at the moment. That is why the momentum only has left the building. The issue for value investors is that when mix and ASP start being questioned, then it is natural to bring in gross margin developments. Like Nokia in the 2000s, Apple will probably cut opex, but like Nokia’s stock, Mr. Market will not be fooled. That is when the value guys will leave the building and Apple’s stock will be well and truly crushed as the ‘consumer electronics’ label will replace ecosystem/stickiness/recurring revenues.

È una brutta cosa che Ahmad abbia perso il posto. D’altra parte però non ne imbrocca una da tre anni e il suo mestiere è esattamente imbroccarle. Sarebbe interessante sapere quanto denaro hanno perso o non guadagnato i clienti che ne hanno ascoltato i consigli.

Il fatto è eccezionale anche per un altro verso: i media sono pieni di scemenze su Apple, ma questo licenziamento appare – più che l’inizio di una tendenza – un’anomalia statistica. L’eccezione, appunto, che conferma la regola.