---
title: "Aspettali al varco"
date: 2015-08-03
comments: true
tags: [Windows, Expo]
---
Contributi fotografici in arrivo da **Flavio**, con la dicitura **Tornello di ingresso, Expo 2015, varco di Roserio**.<!--more-->

Complimenti ai cervelli politici che hanno scelto i cervelli elettronici, evidentemente a propria immagine. Per saperlo: quanto è stato speso a raggiungimento di questo obiettivo?

 ![Al varco di Roserio per Expo 2015](/images/tornello.jpg  "Al varco di Roserio per Expo 2015") 

 ![Al varco di Roserio per Expo 2015](/images/tornelli.jpg  "Al varco di Roserio per Expo 2015") 