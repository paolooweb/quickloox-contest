---
title: "Un mare calmo"
date: 2023-04-25T00:59:09+01:00
draft: false
toc: false
comments: true
categories: [Web]
tags: [Jobs, Steve Jobs, Make Something Wonderful, Steve Jos Archive, Grove, Andy Grove, Avie Tevanian, Tevanian, Avadis Tevanian]
---
Ho preso il tempo di una buona lettura di [Make Something Wonderful](https://book.stevejobsarchive.com/). Non completa, non capillare, ma scorrendo attentamente tutto prima di scegliere i brani su cui concentrare il tempo.

Mi ha fatto un bell’effetto. A una dozzina di anni dalla scomparsa dell’uomo, il tratto agiografico è tenuto al minimo e la cifra di tutto è la pacatezza. È come contemplare il mare calmo.

Dentro la vista del mare si temperano e si ovattano anche le intemperanze di Jobs o certi suoi tratti caratteriali. Nessun dubbio che il lavoro dello Steve Jobs Archive abbia espunto brani che non riteneva adatti alla narrazione; al tempo stesso, il ritratto che emerge di Jobs rimane fedele, visto da qualche distanza per invitare la riflessione. Non sarebbe difficile creare l’immagine di un Jobs vulcanico e iperattivo, affamato e folle come al discorso per i graduati di Stanford. Sarebbe però anacronistico; Steve Jobs non è un uomo di quest’epoca, pur essendo passati appena dodici anni.

Gli episodi non mancano, ovviamente. Dopo uno scambio di posta con il suo mentore Andy Grove di Intel, promette di aiutare gratis un ingegnere, quando prima aveva chiesto del denaro.

Scrive ad Avie Tevanian, protagonista del rilancio di Apple lato software, invitandolo a continuare a sfidarlo con le sue proposte e considerazioni, perché gli sono utilissime.

Cerca di vietare in NeXT le riunioni di giovedì, nel tentativo di farne un giorno dedicato esclusivamente al proprio lavoro. La didascalia spiega tranquillamente che non fu un grande successo.

Ci tornerò perché mi mancano ancora tante parti della raccolta. Come consiglio spassionato, guardare dal sito è molto meglio che dall’epub. Meglio ancora se, pensa un po’, con Chrome, che a causa di un bug di Safari mostra piccoli dettagli tipografici e di impaginazione che nobilitano l’opera.

È appropriato ricordare Steve in questo modo.