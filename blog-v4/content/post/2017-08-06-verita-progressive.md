---
title: "Verità progressive"
date: 2017-08-06
comments: true
tags: [Progressive, PWA, Fortune, Google, Service, Workers]
---
Neanche un mese fa [si commentava](https://macintelligence.org/posts/2017-07-25-gorgoglio-e-pregiudizio/) un articolo molto critico sul presunto rifiuto di Apple di riconoscere la tecnologia nota come Progressive Web Apps e sul detrimento che detto rifiuto porterebbe alla comunità.

Ora un [articolo di Fortune](http://fortune.com/2017/08/04/apple-progressive-web-apps/) svela che Apple sta lavorando con Google per fare funzionare su Safari le Progressive Web App.

L’impressione è che, quando si parla di Apple, basti sedersi sulla riva del fiume e aspettare che passino i cadaveri delle scemenze che si scrivono. Piano piano, la verità arriva a galla.

Impressione che covo circa dal 1985.