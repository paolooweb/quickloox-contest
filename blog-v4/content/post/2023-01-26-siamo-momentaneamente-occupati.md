---
title: "Siamo momentaneamente occupati"
date: 2023-01-26T02:02:33+01:00
draft: false
toc: false
comments: true
categories: [Web, Software]
tags: [Rive]
---
È presto per dirlo con sicurezza e non ho le capacità professionali necessarie per usarla con mestiere e profondità; però [Rive](https://rive.app) sembra davvero interessante.

C’è un livello di utilizzo gratuito e la si può provare senza ansie. Basta il browser e Safari è ok fino a dove sono arrivato. È veloce come dicono. Il feeling è buono.

Da qui alla prossima ora potrei cambiare idea. Sicuramente, da qui alla prossima ora sarò su Rive.