---
title: "Rara avis in museo"
date: 2017-02-01
comments: true
tags: [AllAboutApple, AAA, Corriere]
---
Mi segnala **Lorescuba** (grazie!) che il *Corriere della Sera* ha [dedicato un articolo](http://www.corriere.it/cronache/17_gennaio_30/gli-attrezzi-il-primo-pc-flop-cosi-ho-raccolto-storia-apple-museo-savona-alessio-ferraro-691063ce-e72f-11e6-b669-c1011b4a3bf2.shtml) al museo ]All About Apple](https://www.allaboutapple.com) di Savona.

A Savona per il museo bisogna assolutamente andare almeno una volta nella vita e, se lo ha fatto il *Corriere*, veramente vuol dire che nessuno ha più scuse.

Non sollecito mai clic, però faccio un’eccezione. Chiedo un clic sull’articolo del Corriere. È scritto in italiano, dice cose sensate, parte dall’incontro con i protagonisti, ci sono buone foto, un buon video, niente di inventato.

Lo so: poi si viene assaliti da bambini, gattini, modelle, delitti, politicanti, creme per la ricrescita dei capelli, pubblicità ingannevole. Però, almeno, fare sapere al *Corriere* che per una volta hanno pubblicato una cosa meritevole di essere letta a differenza della gran parte.