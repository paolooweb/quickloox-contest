---
title: "Toca per credere"
date: 2016-01-15
comments: true
tags: [Toca, Robot, Lab, Train, Kitchen, Monsters, Boca]
---
Passati i sedici mesi, Lidia usa con una certa cognizione di causa una *app* particolare: [Toca Train](http://tocaboca.com/app/toca-train/).<!--more-->

Riconosce nel macchinista una effettiva somiglianza con il nonno, interagisce correttamente con i comandi, esercita volontà nel pilotare la locomotiva e caricare o meno il vagone merci, e se sì con che cosa.

Osservarla è affascinante, soprattutto perché i bambini sono *beta tester* spettacolari ed evidenziano con una naturalezza sorprendente i difetti o i problemi di un programma.

Nel caso di Toca Train bisogna dare atto ai suoi creatori di un lavoro pressoché perfetto, con una sola piccola smagliatura: l’interfaccia è impeccabile nel segnalare che il caricamento dei passeggeri è stato completato, mentre per quanto riguarda le merci lascia intendere – a un bambino – che si possa procedere indefinitamente a mettere, togliere e cambiare colli e oggetti. Per il resto il programma è un gioiellino.

Il successo di Toca Train mi ha spinto a considerare altre *app* di Toca Boca. Sono convinto, anche se non ho ancora avuto occasione di provare a riprodurlo, che Lidia abbia individuato un *bug* in [Toca Kitchen Monsters](http://tocaboca.com/app/toca-kitchen-monsters/) (è possibile lanciare cibo dietro la zona cucina, se aperta, e farlo così sparire dallo schermo).

Ma sono piccolezze: le *app* sono altrimenti tutte molto carine, ben realizzate, immediate, semplici e mai banali. Per Lidia molte avranno un senso compiuto solo tra qualche tempo e tuttavia si gode già appieno certi segmenti di alcune, per esempio [Toca Robot Lab](http://tocaboca.com/app/toca-robot-lab/).