---
title: "Come volevasi non dimostrare"
date: 2020-11-19
comments: true
tags: [Apple, Big, Sur, Ocsp, Paul, Gatekeeper]
---
A seguito del [polverone sul servizio di controllo certificati via Ocsp](https://macintelligence.org/blog/2020/11/16/attaccati-a-ocsp/), Apple ha aggiunto [alcuni capoversi](https://support.apple.com/en-us/HT202491) alla propria documentazione sulla privacy, che traduco perché vedo disponibile solo la versione inglese. Aggiungo qualche grassetto a caso.

>Gatekeeper effettua online controlli per verifica se una app contenga malware e se il certificato di firma dello sviluppatore sia stato revocato. **Non abbiamo mai combinato dati ottenuti da questi controlli con informazioni sugli utilizzatori o sui loro apparecchi. Non usiamo dati da questi controlli per arrivare a sapere che cosa i singoli individui lancino o eseguano sui propri apparecchi**.

>La notarizzazione controlla se la app contenga malware noto tramite una connessione cifrata resiliente [contento, Frix? :-)] rispetto ai malfunzionamenti del server.

>**Questi controlli di sicurezza non hanno mai incluso l’ID Apple dell’utilizzatore o l’identità dei suoi apparecchi**. Per proteggere ulteriormente la privacy, **abbiamo smesso di registrare gli indirizzi IP associati ai controlli di certificazione sugli ID Developer e ci assicureremo che qualsiasi indirizzo IP raccolto venga espunto dai log**.

>In aggiunta, nel corso del prossimo anno apporteremo numerosi cambiamenti ai nostri controlli di sicurezza: un nuovo protocollo cifrato per i controlli sulla revoca dei certificati degli ID Developer; protezioni solide contro i malfunzionamenti dei server; una nuova preferenza di rinuncia a queste protezioni di sicurezza.

Sicuramente, secondo l’impostazione di partenza del singolo, ci sono almeno due modi per leggere una comunicazione come questa: uno è *abbiamo cercato di fare i furbi ma ci avete beccati* e l’altro è *potevamo fare meglio ma siamo puliti e miglioriamo in base al feedback*.

Le opinioni sono libere. Chi però ha presentato accuse puntuali che vengono smentite, ed è partito dal primo approccio, ora dovrebbe dimostrare la falsità della smentita. O è troppo facile.