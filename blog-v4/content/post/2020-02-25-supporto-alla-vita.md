---
title: "Supporto alla vita"
date: 2020-02-25
comments: true
tags: [Hudson, Swift]
---
Il ventiquattro di gennaio è diventato una data per me scomoda; cestista per una vita, appassionato di Apple, è tutta una commemorazione.

Ho voglia di guardare avanti e di pensare alla vita. Ecco, per esempio, Paul Hudson e il suo sito [Hacking with Swift](https://www.hackingwithswift.com). Vivace, colorato, pieno di risorse.

Hudson non cambierà il mondo una persona alla volta, né diventerà il programmatore Swift più forte del mondo grazie alla sua forza mentale senza paragoni. Tuttavia fornisce contenuti interessanti, cose su cui smanettare, cento appigli per terminare la giornata con una capacità di quando l’abbiamo iniziata.

Leggere dell’esistenza di corsi come [Swift on Sundays](https://www.hackingwithswift.com/store/swift-on-sundays-1), imparare Swift nel tempo della domenica, fa venire voglia di lasciargli lì anche qualche soldo. Da assaggiare, per sentirsi di nuovo sul versante della vita.