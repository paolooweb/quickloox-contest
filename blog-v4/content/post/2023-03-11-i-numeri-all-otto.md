---
title: "I numeri all’otto"
date: 2023-03-11T11:50:32+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [Evans, Julia Evans, bit, byte, word]
---
La capacità di divulgazione che Internet consente è uno dei motivi per i quali la rete dovrebbe essere dichiarata patrimonio assoluto dell’umanità e qualsiasi regime intento a erigere firewall punitivi o a tagliare cavi sottomarini o a isolarsi dal traffico globale venire sanzionato con la massima durezza.

Perché su Internet è facile perdersi e finire in aree micidalmente vuote di valore. Oppure imbattersi in articoli come quello di Julia Evans sul [perché un byte è fatto da otto bit](https://jvns.ca/blog/2023/03/06/possible-reasons-8-bit-bytes/).

Spoiler: ci sono ragioni abbastanza logiche ma nessuna definitiva e non era per niente scontato.

L’articolo è divulgativo e rimanda alle fonti per approfondimenti più scientifici. Ma è ben lungi dall’essere superficiale o vacuo e, anzi, fa riflette su quanto ci siamo allontanati nel nostro uso dei computer dalle viscere del sistema.

In un altro momento storico, geografico, tecnico avremmo potuto tranquillamente avere byte da quattro, sei, sette, sedici bit o altro. Non cambia nulla nella vita, salvo avere imparato qualcosa su una storia che ci appartiene e ha contribuito a formarci anche se a due anni mettevamo le mani lo smartphone della mamma.