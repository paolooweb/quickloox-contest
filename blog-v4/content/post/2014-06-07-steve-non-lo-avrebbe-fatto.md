---
title: "Steve non lo avrebbe fatto"
date: 2014-06-09
comments: true
tags: [Swift, Jobs]
---
Io dico di sì ed è un bel segnale.

<blockquote class="twitter-tweet" lang="en"><p>The .swift Icon is the &quot;Crazy Ones&quot; text now. 😊&#10;(via <a href="https://twitter.com/blunckalex">@blunckalex</a>) <a href="http://t.co/JA63Fs5iyG">pic.twitter.com/JA63Fs5iyG</a></p>&mdash; Nicolas Bouilleaud (@_nb) <a href="https://twitter.com/_nb/statuses/474569926297276416">June 5, 2014</a></blockquote>
<script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>

A margine, [Swift](https://developer.apple.com/swift/) – il nuovo linguaggio di programmazione annunciato da Apple – [è in lavorazione da *quattro anni*](http://www.nondot.org/sabre/). Come mai neanche una parola dai nostri intrepidi siti di *rumors*?