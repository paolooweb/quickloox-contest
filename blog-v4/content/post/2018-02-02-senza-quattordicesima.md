---
title: "Senza quattordicesima"
date: 2018-02-02
comments: true
tags: [Apple, iPhone]
---
L’anno scorso Apple aveva pubblicato i risultati finanziari di un trimestre natalizio da quattordici settimane confrontati con quelli di uno da tredici. E [tutti a sottolineare](https://macintelligence.org/posts/2017-02-07-la-pulce-bisestile/) che aveva avuto una settimana in più per vendere e quindi il risultato record era falsato.<!--more-->

Quest’anno, inevitabile, il trimestre era da tredici settimane, a confronto con quello dell’anno scorso che era da quattordici. [Il fatturato è stato record assoluto](https://www.apple.com/newsroom/2018/02/apple-reports-first-quarter-results/), ma le vendite no.

Tutti a sottolineare che le vendite non sono state da record e nessuno che c’era una settimana di svantaggio.

Il bello è che, contando le vendite per settimana, ci sarebbe il record. Lo [ha fatto notare](https://sixcolors.com/post/2018/02/this-is-tim-transcript-of-apples-q1-2018-earnings-call/) solo Luca Maestri, il capocontabile di Apple.

Coraggio comunque, Apple neanche quest’anno dovrebbe fallire. Nonostante la mancata quattordicesima.