---
title: "Cattive attitudini"
date: 2021-02-21T03:32:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [Flash, SkilledObject, Preferenze di Sistema, malware, adware, Excel, Word, PowerPoint, WordPress] 
---
Software sbagliato non per quello che fa, ma per quello che consente di fare. [Word come contenitore di file .zip](https://www.macintelligence.org/posts/Come-un-ombrello-su-una-macchina-da-cucire.html). Excel comprato per fare tabelle brutte e disfunzionali. WordPress pretesto per installare i plugin più tossici e creare le peggiori esperienze web. PowerPoint, per dire *slaid*, sentirsi in controllo e presentare da schifo.

Ieri ero sul Mac di altri. Trovato un malware-adware talmente sfrontato che aveva perfino un’icona dentro le Preferenze di Sistema. Chi fosse interessato cerchi *SkilledObject*; non linko nulla perché arrivano un sacco di siti che spiegano come toglierlo. E con quella scusa cercano di farti installare altro adware.

Ma come ci è arrivato? Guardo in cartella Applicazioni, eccolo lì: sedicente installatore di Flash, proveniente da sito nei bassifondi di Internet.

Mica per niente Flash è morto, ma ancora [neanche vuole ammetterlo](https://www.macintelligence.org/posts/Browser-fai-da-te.html). Il contenuto tecnico di Flash interessa zero e quello zero interessa a nessuno.

Conta che nella testa delle persone sia rimasta l’idea di installare un player di Flash comunque, non importa perché; *qualcosa fa*. Non serve che arrivi da un sito sicuro, ce l’hanno tutti. È software di bassa lega, quindi l’esperienza di scaricarlo (che dovrebbe essere rivelatrice: come si mangerà in un locale che ti accoglie in modo scortese?) può essere discutibile. *Vale tutto*.

*Vale tutto* giustifica la brutta pubblicità ovunque e perfino nelle app, i social vissuti sciattamente, l’abolizione di qualsiasi criterio di valore diverso da *costa meno*, l’esaltazione dell’ignoranza come sistema di vita.

Sembra una tirata retorica. Lo è. La guerra contro i cattivi programmi non si fa perché funzionano male. Funzionano benissimo. Fanno pensare male ed è questo il problema.

*Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile.*