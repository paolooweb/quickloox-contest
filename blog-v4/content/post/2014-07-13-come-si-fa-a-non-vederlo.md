---
title: "Come si fa a non vederlo"
date: 2014-07-14
comments: true
tags: [Cook, Reuters, accessibilità, Loop, Apple]
---
[Parola](https://nfb.org/blog/vonb-blog/comments-apple-and-nfb-resolution-2014-12) del presidente della Federazione nazionale ciechi americana.<!--more-->

>A oggi Apple ha fatto per l’accessibilità più di qualsiasi altra azienda e lo abbiamo regolarmente riconosciuto con almeno due premi ed elogiandola pubblicamente ogni volta che se presenta l’opportunità.

Nel frattempo Reuters [ha inventato una polemica](http://in.reuters.com/article/2014/07/09/apple-mobilephone-accessibility-idINKBN0FE12O20140709) tra disabili e Apple prendendo una frase di Tim Cook e levando subdolamente la parte che chiarisce tutto. Questa la citazione secondo Reuters:

>[I disabili] sono frequentemente lasciati ai margini di progressi tecnologici che per altri sono fonte di arricchimento e soddisfazione.

Questa la frase effettivamente pronunciata:

>[I disabili] sono frequentemente lasciati ai margini di progressi tecnologici che per altri sono fonte di arricchimento e soddisfazione, ma gli ingegneri Apple respingono questa realtà inaccettabile e lavorano straordinariamente per rendere accessibili i nostri prodotti a persone variamente disabili.

Il [confronto](http://www.loopinsight.com/2014/07/09/shoddy-reporting-from-reuters/) è stato reso evidente su *The Loop*. Poi c’è il [video](https://www.youtube.com/watch?v=dNEafGCf-kw) del discorso di Cook.

Però qualcuno si ostina a non vedere. La disabilità non c’entra.

<iframe width="560" height="315" src="//www.youtube.com/embed/dNEafGCf-kw" frameborder="0" allowfullscreen></iframe>