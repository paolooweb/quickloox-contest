---
title: "La tavoletta di Babele"
date: 2019-08-22
comments: true
tags: [iPad]
---
[Quando le parole non sono abbastanza, gli insegnanti trovano un linguaggio comune attraverso iPad](https://www.apple.com/newsroom/2019/06/when-words-arent-enough-teachers-find-a-common-language-with-ipad/).

È una storia confezionata direttamente da Apple, quindi di parte. Eppure è tremendamente credibile. In tutta Europa arrivano ragazzi che devono imparare la lingua del Paese in cui risiedono. Favorevoli o contrari all’immigrazione, è interesse totalmente comune che lo facciano bene, in fretta e senza penalizzare chi la lingua la conosce già.

Insegnanti dalla Germania e dalla Svezia, nazioni capofila del fenomeno, mostrano come un iPad in mano ai ragazzi, con i giusti contenuti, porti a risultati percettibilmente migliori dei metodi tradizionali.

Da noi usa molto il mantra della mancanza dei soldi, ma forse è solo un problema di intendersi: dove uno dice *non ci sono soldi*, io capisco *sono spesi male e comunque vanno trovati*. La posta in gioco è troppo alta. Ancora una volta, qualsiasi idea uno abbia, non esistono dubbi sul fatto che avere in Italia persone che sanno bene l’italiano è una priorità. Vale anche per tanti italiani analfabeti funzionali.

Chissà, magari un iPad aiuterebbe tante istituzioni a comprendere il linguaggio delle necessità reali in aggiunta a quello della burocrazia improduttiva.
