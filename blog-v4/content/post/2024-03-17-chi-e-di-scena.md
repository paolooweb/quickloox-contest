---
title: "Chi è di scena"
date: 2024-03-17T19:36:07+01:00
draft: false
toc: false
comments: true
categories: [Hardware]
tags: [MacBook Air, Piccolo Teatro, Giorni felici]
---
Di passaggio al [Piccolo teatro sala Paolo Grassi](https://www.piccoloteatro.org/it/theaters/piccolo-teatro-grassi) di Milano per assistere a una [pièce di Beckett](https://www.piccoloteatro.org/it/2023-2024/giorni-felici), salutiamo il banco regia.

![Banco regia con un MacBook Air in mezzo](/images/grassi.jpeg "Giorni felici anche per il tecnico alla regia.") 