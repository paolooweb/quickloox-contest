---
title: "Boardgame 2021"
date: 2021-07-11T01:18:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [board] 
---
Oggi vado fuori tema e me ne scuso anticipatamente. Sono però rimasto colpito da una telefonata udita a bordo spiaggia, dal tavolo di fianco. Se fosse stata rivolta a me, avrebbe avuto questo tenore.

> Ti invito a fare parte del board, il consiglio di amministrazione, della mia azienda di consulenza. Il compenso per fare parte del board è l’uno percento del fatturato, che può salire fino al tre percento se, invece di fare pura presenza, presti servizio. Ho pensato di invitarti perché vogliamo mostrare un board diversificato e tu sei un maschio europeo.

La telefonata, tuttavia, non era rivolta a me.

*Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile.*