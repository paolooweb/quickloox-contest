---
title: "Notti interminabili"
date: 2017-08-21
comments: true
tags: [O’Reilly]
---
Le cose che ignoro continuano inesorabilmente a superare quelle che conosco e, in particolare, aumentano a velocità sempre maggiore.

Non sapevo che esiste una pagina di [ebook tecnici gratuiti](http://www.oreilly.com/free/reports.html) messi a disposizione dall’editore O’Reilly, uno dei migliori nel campo.

La gamma dei titoli è disperante per estensione, considerato il tempo a disposizione (la pagina è solo un punto di partenza, l’elenco reale  è ben più lungo). I titoli sono sintetici e servono a entrare in un argomento più che a sviscerarlo; disperante perché c’è sempre una buona scusa per svicolare da un tomo di centinaia di pagine, ma nessuna per farlo da una introduzione competente.

L’unico pensiero confortante è che queste notti estive sono profonde e favoriscono una concentrazione rilassata.