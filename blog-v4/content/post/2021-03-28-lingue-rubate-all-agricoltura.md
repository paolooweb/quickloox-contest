---
title: "Lingue rubate all’agricoltura"
date: 2021-03-28T03:25:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [coronavirus, Covid-19, Ars Technica] 
---
Non volevo scriverlo questo post, ma sono già le tre passate (non capisco, pochi istanti fa neanche erano le due: come vola il tempo), primo; secondo, ho passato la soglia della sopportazione della gente che scrive, manifesta, *lotta* (*soi disant*) per *riaprire le scuole*.

L’obiettivo mi trova totalmente d’accordo, ma le modalità sono da decerebrati. Se fossi lasciato a me stesso finirei all’alba e probabilmente querelato; invece sono abbastanza fortunato da poter citare un articolo di *Ars Technica* appena uscito: [«Le scuole sono sicure?» è la domanda sbagliata da porre](https://arstechnica.com/science/2021/03/are-schools-safe-is-the-wrong-question-to-be-asking/) (figuriamoci quando diventa una asserzione…).

Dovrebbe bastare il sottotitolo: *Non esiste una risposta giusta sulla sicurezza delle scuole, solo una risposta giusta per ciascuna comunità*.

John Timmer è americano e scrive degli Stati Uniti, ma è come se fosse un mio vicino di casa:

>Le risposte alla domanda sono state scrutinate, analizzate e perfino politicizzate. In tutto questo si è persa la consapevolezza che si tratti di una domanda orribilmente sbagliata… perché non ha una risposta univoca.

>Invece, qualsiasi risposta può applicarsi solo a comunità specifiche e, in molti casi, a scuole specifiche. Ed è anche soggetta a cambiamenti secondo l’evoluzione delle dinamiche della pandemia, comprese le nuove varianti del virus.

L’articolo ricorda, studi alla mano, che l’età scolare è quella soggetta al minor rischio di mortalità da coronavirus; puntualizza anche come, secondo altri studi di uguale autorevolezza, la chiusura delle scuole sia associata a una minore diffusione del virus nella comunità di riferimento della scuola.

>Questo perché le scuole fanno parte di una comunità più ampia.

Riaprire le scuole non può essere un obiettivo; ci sono scuole che possono essere riaperte e scuole che, purtroppo, invece no. Dipende dal contesto.

>Non intendo dire che le scuole non possano riaprire in modo da minimizzare il rischio per gli studenti. Ma capire quando sia il caso di farlo, e assicurare che la sicurezza venga mantenuta, deve essere effettuato a livello di comunità. E ciascuna comunità può anche avere definizioni differenti di quale livello di rischio sia accettabile per la sicurezza.

>Questo spiega perché dovremmo ascoltare di più le persone che parlano di come valutare il rischio… e molto meno chiunque creda che la domanda abbia una risposta binaria, semplice.

Ecco. Le persone che agitano la lingua per la riapertura indiscriminata delle scuole sono come quelle che vogliono la chiusura indiscriminata: gente che rende davvero un cattivo servizio alla scuola dove ha studiato, evidentemente imparando cose diverse dal fare funzionare il cervello.

*Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile.*