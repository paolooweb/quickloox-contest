---
title: "Conchiglie e arcobaleni"
date: 2013-08-17
comments: true
tags: [Unix, iPhone]
---
Dopo l’orgia di paragrafi tossici che [mi sono imposto ieri](https://macintelligence.org/posts/2013-08-16-artificiale-e-neanche-intelligente/), ho cercato nient’altro che evasione e l’ho trovata nel Terminale, grazie a questo consiglio di [Command Line Magic](https://twitter.com/climagic) per ravvivare i toni della *shell* Unix:<!--more-->

`yes "$(seq 231 -1 16)" | while read i; do printf "\x1b[48;5;${i}m\n"; sleep .02; done`

E per oggi è sufficiente, con la sola postilla di iPhone 5 che sembra essere fatto su misura per la taschina portaoggetti dei pantaloncini da esercizio. Sono scettico sui computer da tasca per i quali non basta una mano sola. E una tasca sola.