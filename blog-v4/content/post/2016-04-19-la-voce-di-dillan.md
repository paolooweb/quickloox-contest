---
title: "La voce di Dillan"
date: 2016-04-19
comments: true
tags: [Dillan, iPad, autismo, Aac]
---
Gira [un bell’articolo di Mashable](http://mashable.com/2016/04/02/apple-autism-dillan/#ubp_L2olMaqp) dedicato alla storia di Dillan, un sedicenne autistico non verbale che ha iniziato a comunicare con le persone grazie a iPad. Fino al punto di far pronunciare alla tavoletta un discorso pubblico in occasione del suo diploma scolastico.<!--more-->

Apple ha girato un video di due minuti, *Dillan’s Voice*, dedicato alla vicenda e visibile sulla pagina dell’articolo. È un video girato con cura e attenzione ai dettagli, tecnicamente perfetto; ha richiesto impegno, *budget*, tempo e persone.

Eppure, quanti iPad può muovere in termini di acquisti? Quante sono rispetto al totale le famiglie come quella di Dillan, o le persone interessate seriamente alle *app* di [Augmented and Alternative Communication](http://www.asha.org/public/speech/disorders/AAC/)? Briciole. Ognuna delle quali, tuttavia, ha un valore individuale incommensurabile, che la comunicazione delle Samsung, delle Microsoft, delle Google – magari mi sbaglio, ma attendo controprove – un po’ trascura.

Si è letto che, in una delle conversazioni di addio, Steve Jobs disse a Tim Cook di non chiedersi mai che cosa avrebbe fatto lui, Steve, e invece fare semplicemente la cosa giusta.

Il gioco che porta a vendere decine di milioni di pezzi di tecnologia sofisticata e costosa è evidentemente troppo complesso per essere semplificato in poche frasi a effetto; tuttavia, se parlare di Dillan fosse una scaltra manovra per aumentare le vendite, comunque non mi parrebbe così sbagliata. La realtà è che per aiutare un Dillan ci vuole una forza di marea di cento, mille, centomila iPad. Dopo di che, dalla presenza dell’apparecchio nascono le applicazioni concrete.