---
title: "Ok, il prezzo è sbagliato"
date: 2017-08-24
comments: true
tags: [Crashplan, Ulysses, BBEdit]
---
I fatti: Crashplan [ha annunciato la fine degli abbonamenti home](https://www.crashplan.com/en-us/consumer/nextsteps/?AID=11669504&PID=6164706&SID=j6o5b3w323013t3g04337) per il backup online. Gli abbonati possono trasmigrare su altri concorrenti con sconti oppure adottare il piano Business.<!--more-->

Ancora fatti: [Ulysses diventa una app offerta in abbonamento](https://medium.com/building-ulysses/why-were-switching-ulysses-to-subscription-47f80b07a9cd). I programmatori non vedono altro modo di preservare la qualità del prodotto e continuare a farlo crescere in modo sano.

** Aggiornamento:** Proseguono i fatti: lo sviluppatore di TextTool per iOS ha annunciato la [fine dello sviluppo della *app*](http://blackfoginteractive.com/parting-glass.html), più di mille copie vendute dopo e ottomila dollari di ricavi; numeri completamente insufficienti per giustificare il lavoro compiuto, e da compiere, sul programma.

Non un fatto, ma una indagine: le *app* offerte in abbonamento conseguono il miglior tasso di adesione e il minor costo di acquisizione di nuovi clienti [quando il canone sta fra sette e venti dollari](https://www.theverge.com/2017/8/15/16147954/liftoff-report-apple-ios-android-app-subscriptions-conversion-rate-2017). Sopra è troppo, sotto è troppo poco.

Chiamiamoli indizi e diciamo pure che, pur essendo quattro, non fanno una prova. Rimane il fatto che apparentemente la parabola delle *app* e dei servizi gratis o pseudogratis sta entrando in fase discendente.

Qui si fa un discorso che esula da mostri come Office 365 o Creative Cloud di Adobe, che nascono come macchine da soldi e si muovono in funzione di quello, con considerazione per l’utente vicina allo zero assoluto. Parliamo invece di software e servizi di gradazione assolutamente professionale, però erogati da software house o singoli programmatori, che ne ricavano direttamente lo stipendio.

A nessuno piace pagare e a meno ancora piace pagare troppo; al tempo stesso da anni si trascura il valore effettivo di una *app* e decine di pezzi di software di grande valore sono stati costretti a svendersi a prezzi da fame. Una *app* che costa 0,99 per qualcuno è troppo. Certo, a volte la *app* è ridicola. A volte no.

Ovvio che si debba cercare un equilibrio tra la bontà dell’offerta e le reali possibilità di spesa della domanda. Indubbio che, al momento, la bilancia penda sproporzionatamente da quest’ultima parte. Ne beneficiamo tutti, ma nel lungo periodo finiamo per perdere opportunità importanti e suggerisco che ognuno tenga in considerazione, per quanto ovviamente nelle proprie possibilità, l’idea di spendere cifre eque in cambio del valore che effettivamente riceve. Probabilmente può significare comprare meno software, ma se esistono programmi all’altezza è solo un bene. La gente come me usa [BBEdit](http://www.barebones.com/products/bbedit/) e non sente il bisogno di comprare [Sublime Text](https://www.sublimetext.com); chi usa Sublime Text non sente l’esigenza di comprare BBEdit. Questo perché sono programmi eccezionali e ne basta uno (salvo eccezioni, ovvio).

E il valore: attraverso BBEdit passa una parte consistente della mia generazione di reddito. Se si trattasse di mille euro l’anno, varrebbe la pena di darne dieci o quindici (annui) a Bare Bones? Evidentemente sì. Se poi gli euro fossero duemila, anche solo dibatterne suonerebbe ridicolo.

Torniamo a riconoscere il giusto prezzo per il giusto valore. Almeno nel software.