---
title: "La visione del futuro da vicino"
date: 2016-04-25
comments: true
tags: [Iconfactory, ColorSync, iPad, Pro, System, Android, Windows]
---
Già che ieri puntavo a un [articolo interessante per i suoi grafici](https://macintelligence.org/posts/2016-04-24-ricerca-e-ritrova/), oggi concedo il bis dopo avere letto [Guardare al futuro](http://blog.iconfactory.com/2016/04/looking-at-the-future/) sul blog di *The Iconfactory*.<!--more-->

>Apple si trova in una posizione senza eguali rispetto alla gestione del colore. Possiede una tecnologia chiamata ColorSync, nata nel 1993 con System 7.1 su Mac, oggi integrata a livello di sistema in qualsiasi versione di OS X. È una tecnologia estremamente matura che con iOS 9.3 è arrivata su iOS.

Il grafico fondamentale da guardare è quello del *gamut*, la gamma di colore che un iPad Pro è in grado di mostrare oltre quello che già mostravano gli iPad più vecchi. Su un iPad Pro la saturazione del colore arriva a superare del 25 percento quelle preesistenti e una reta di sensori governa la funzione TrueTone, che adegua la temperatura del colore alla luce dell’ambiente in cui si trova iPad. E gli altri, che fanno?

>Android non possiede funzioni di amministrazione del colore […] Ci sono sistemi e strumenti open source, ma integrarli a livello di sistema è un lavoro vasto. La gestione del colore, inoltre, non può essere un add-on: la maggior parte degli utenti non saranno consapevoli che esiste, o come abilitarla. Windows è un buon esempio di questo fenomeno: è raro che i programmi regolino il colore, perché i programmatori non possono dare per scontato che il loro software, una volta installato, potrà farlo.

C’è un dettaglio interessante nella trattazione: la superiorità nella gestione del colore si vede solo sugli apparecchi effettivamente superiori. L’unico modo per apprezzare la resa cromatica di iPad Pro è usarlo. In altre parole, è un argomento difficilissimo da sfruttare per il marketing: la pagina che mostra le differenze di colore tra iPad Pro e una ciofeca Android, vista sulla ciofeca Android, *non mostra alcuna differenza*. Perché l’apparecchio non è in grado di arrivarci.

Un’azienda che mette denaro, competenze e lavoro per offrire un risultato superiore, conscia che fare capire tutto questo all’esterno è straordinariamente difficile e in termini di vendite porta, probabilmente, un vantaggio inesistente o modesto. È un’azienda che ti mette in mano apparecchi più vicini degli altri al futuro.