---
title: "Il blocco dello scrittore"
date: 2021-03-05T03:16:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [Jason Snell, Six Colors, Markdown, 1Writer, Drafts, Taio, Textastic, BBEdit, MarsEdit, iPad, Editorial] 
---
Raramente mi trovo in sintonia con un articolo quanto invece lo sono con questa [veloce rassegna di Jason Snell sugli editor di testo per iPad](https://sixcolors.com/post/2021/03/searching-for-the-perfect-ios-markdown-writing-tool/).

C’è dentro tutto e nello spazio giusto: su iPad abbiamo buoni programmi, ma ancora nessuno che sia degno di diventare veramente quello di elezione, capace di farsi preferire a un [BBEdit](https://www.barebones.com/products/bbedit/) ma anche “solo” a un [MarsEdit](https://redsweater.com/marsedit/) su Mac.

Se fanno bene Markdown, errano sul lato della sincronizzazione; se hanno uno splendido sistema di macro, sono deficitari nel supporto delle scorciatoie sulla tastiera fisica; se costano il giusto hanno ancora qualche bug di troppo; se hanno una colorazione ottimale della sintassi hanno una versione Mac deludente e così via.

C’è un bel blocco di pretendenti al titolo di strumento di preferenza per scrivere su iPad, solo che nessuno ancora si stacca decisivamente da tutti gli altri.

Non c’è dubbio che undici anni fa di editor di testo per iPad neanche si parlava; la piattaforma è giovane e deve abbondantemente evolvere. Dobbiamo avere pazienza duplice, quella di provare le novità che escono e quella di sopportare le lacune del programma che abbiamo scelto sul momento.

Nel mio piccolo, sono passato per lo più a [Drafts](https://getdrafts.com) per scrivere in Markdown e però, quando devo scrivere in Html, la personalizzazione fatta su [Editorial](http://omz-software.com/editorial/) è tuttora imbattibile. Per cui alterno da un programma all’altro, secondo necessità.

Magari, in una logica di app specializzate, è giusto così e l’idea dell’applicazione monolitica a cui affidarsi per tutto (vedi BBEdit) è superata. Magari, anche no.

*Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile.*