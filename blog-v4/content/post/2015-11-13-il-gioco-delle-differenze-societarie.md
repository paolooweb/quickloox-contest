---
title: "Il gioco delle differenze societarie"
date: 2015-11-13
comments: true
tags: [Grande, Fedez, Apple, Microsoft, iPhone, Google]
---
Ispirato da [Ariana Grande e Fedez](https://itunes.apple.com/it/album/one-last-time-feat.-fedez/id997291287?l=en) con il loro gioco delle differenze sociali, ecco un gioco delle differenze societarie.<!--more-->

Nel trimestre estivo – tradizionalmente non il migliore – Apple [ha incassato 51,501 miliardi di dollari](http://images.apple.com/pr/pdf/q4fy15datasum.pdf). Di questi, 32,209 si devono a iPhone. Facciamo la differenza: il resto di Apple, senza iPhone, fa 19,292.

Nello stesso trimestre, [Microsoft](http://www.microsoft.com/investor/EarningsAndFinancials/Earnings/PressReleaseAndWebcast/FY16/Q1/default.aspx) ha totalizzato 21,660 miliardi di dollari.

Il resto di Apple gioca quasi alla pari con tutta Microsoft.

[Google](https://investor.google.com/earnings/2015/Q3_google_earnings.html) è a quota 18,675 miliardi di dollari.

Il resto di Apple gioca alla pari con tutta Google.

iPhone da solo vale “appena” quattro quinti di tutta Microsoft e tutta Google messe assieme.

Leggi che Apple dipende troppo da iPhone? Se iPhone scomparisse domani, Apple sarebbe grande come Microsoft, grande come Google.

Leggi che Apple cura iPhone e si disinteressa del resto? Il resto vale come una Microsoft, come una Google. Tu lo trascureresti?

Il gioco delle differenze societarie sposta molti degli equilibri che, inconsapevolmente, abbiamo memorizzato in questi anni.