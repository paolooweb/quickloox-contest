---
title: "Le ultime parole famose"
date: 2014-11-10
comments: true
tags: [Dilger, AppleInsider, bendgate, antennagate, iPhone6, iPhone6Plus, Qualcomm, Samsung, Motorola, iPhone, iPad, iPhone4S]
---
Quasi spassoso il [pezzo di Daniel Eran Dilger su AppleInsider](http://appleinsider.com/articles/14/09/27/after-apple-inc-dodged-the-iphone-6-plus-bendgate-bullet-detractors-wounded-by-ricochet) di un ormai lontano 26 settembre eppure sempre valido per la tematica: le invenzioni dei siti spazzatura che finiscono per raggiungere l’effetto opposto a quello previsto.<!--more-->

Esempio da manuale e pure attuale, l’ovvio *bendgate* relativo a iPhone 6 e iPhone 6 Plus. Tra le polemiche e gli scandali, i nuovi iPhone sono partiti con dieci milioni di esemplari in tre giorni e un ottimo esordio in Cina. Finirà che iPhone 6 diventerà un *bestseller* esattamente come è successo a iPhone 4S, quello con l’antenna che non funzionava secondo i più.

L’anno scorso di questi tempi si scriveva al ribasso dell’adozione di processori a 64 bit per iPhone e iPad, con Qualcomm a parlare di *fuffa da marketing* e Samsung a dichiarare pubblicamente di essere *leader* in un’*offerta di computing a 64 bit* che così Dilger commenta:

>Un anno dopo, non solo Apple è l’unico produttore di apparecchi da tasca con apparecchi a 64 bit realizzati in massa, ma batte in prestazioni i prodotti di Samsung, Lg e Motorola, tutti con processori Qualcomm, nonostante usi la metà dei nuclei di elaborazione, frequenze di clock inferiori e molta meno Ram, il che risulta in una autonomia migliore.

Stabilito che parlare male senza merito di aggeggi si ritorce contro i concorrenti di quegli aggeggi, se solo succedesse anche ai siti che inventano scandali inesistenti puramente per denaro facile.