---
title: "Un saluto ai patrioti"
date: 2015-02-02
comments: true
tags: [Sky, Fox, SuperBowl, Patriots, Seahawks, Chrome, Flash, 8tv, AppleTV, GamePass]
---
Non sono titolare di un abbonamento specifico allo sport di Sky, che a quanto mi consta era l’unico modo di vedere in diretta in Italia (su Fox Sports 2) il [quarantanovesimo Super Bowl](http://www.nfl.com/superbowl/49). Per cui ho ripiegato sul [Game Pass](https://gamepass.nfl.com/) specifico della National Football League (Nfl), che per una manciata di euro portava la partita in <i>streaming</i> su computer, tavolette e telefoni.<!--more-->

Gran partita, in bilico fino a venti secondi dalla fine, con colpi di scena, un’azione rocambolesca che sarà ricordata a lungo e alcuni passaggi da videoantologia. L’anno scorso era stato noiosuccio, quest’anno eccezionale.

Però vorrei tirare io qualcosa da cinquanta *yard* ai dirigenti e ai tecnici della Nfl. Abbastanza arretrati da imporre Flash su Mac (niente di che, ho usato Chrome, ma insomma, si sentiva puzza di vecchio) e abbastanza evoluti da avere un’ottima [app](https://itunes.apple.com/it/app/nfl-game-pass/id459244446?l=en&mt=8). Dalla quale però si sono peritati di levare – per l’occasione! – l’opzione AirPlay. E su tv c’è un canale Nfl Now interessante, ma del tutto inutile ai fini della diretta della partita più importante dell’anno nel *football* americano.

Per vedere Patriots e Seahawks valeva la pena, solo che la Nfl in questa situazione è stata più punitiva di un *fullback* da centotrenta chili che ti carica.