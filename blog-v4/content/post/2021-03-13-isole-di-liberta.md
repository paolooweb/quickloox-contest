---
title: "Isole di libertà"
date: 2021-03-13T02:24:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [Frix, Microsoft, Exchange, LibreItalia] 
---
Sviluppi della situazione delineata ieri rispetto al [mondo del software libero e di chi agisce per controllarlo](https://macintelligence.org/posts/Detenuti-e-tiranni.html) a scopo di lucro.

**Frix** mi ha chiesto se mi fossi ispirato al video [1984](https://www.youtube.com/watch?v=0q7iX0QWaTg) di Ridley Scott. In effetti no, però sono rimasto colpito dall’associazione di idee che poteva essere costituita. La situazione è quella di un Grande Fratello, anche se questo più suadente e viscido.

Un aggiornamento tecnico sulla vicenda è che alcune migliaia di server Exchange compromessi sono stati colpiti da un *ransomware*, grazie appunto alla libertà di attacco goduta dai pirati.

Un altro aggiornamento, etico anche se non mi piace la parola, è che ho rinnovato la mia iscrizione a [LibreItalia](https://www.libreitalia.org). Quest’anno potevo permettermelo e così mi sono qualificato socio sostenitore: ho infuso nelle casse di LibreItalia la bellezza di cinquanta euro.

Una iscrizione normale pesa solamente dieci euro (volendo c’è anche l’opzione cinque per mille). Sono noccioline rispetto ai miliardi che una Microsoft, ma non solo, pompa per arrivare all’omologazione e all’assimilazione.

LibreItalia è fatta di volontari che con pochi mezzi e entusiasmo inesauribile, tenacemente e onestamente, strappano alle sabbie mobili di Microsoft oggi una scuola, domani un municipio, dopodomani un ospedale. Uno per volta, sudato e sofferto.

Isole di libertà, buonsenso, civilizzazione immerse in un mare uniforme e fermo, da incubo.

Poi qualcuno dice *Ma come fai a non usare Excel?* sostituibile con uno zilione di altri nomi, Word, Windows, Access, PowerPoint, Visual Studio, Azure eccetera.

L’ampiezza e l’eco che provoca questo insieme di app sono le ragioni evidenti per non solo si può fare, ma dovrebbe anche essere programmatico, il non usare Excel o qualunque altro prodotto che contribuisca a finanziare la cancellazione psicologica della comunità open source.

<iframe width="560" height="315" src="https://www.youtube.com/embed/0q7iX0QWaTg" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

*Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile.*