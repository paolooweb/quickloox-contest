---
title: "Più veloce di TeX"
date: 2014-04-29
comments: true
tags: [Genesis, LyX, LaTeX, Office, Html, Xml, TeX, Markdown, Pandoc]
---
>Holy Mother of God / You’ve got to go faster than that to get to the top. (Genesis, *Dance on a Volcano*)

Una delle voci principali alla categoria *cose che dovrei seguire più in fretta per tenere il ritmo* è TeX/LaTeX e al momento tutto quello che posso fare è lanciare avvertimenti benevoli, sperando che qualcuno li raccolga e tenga il ritmo almeno lui/lei.<!--more-->

L’avvertimento di oggi è l’uscita di [LyX 2.1.0](http://www.lyx.org/). LyX è una via di mezzo tra l’*editing* puro in LaTeX e un vero e proprio *word processor*, ovvero leva molta della difficoltà iniziale di apprendimento senza precludere alcun progresso futuro.

LaTeX è una delle materie la cui padronanza è buon viatico per cavarsela con i contenuti informatici del XXI secolo a qualsiasi livello di requisito. Office può essere usato da centinaia di milioni di persone in tutto il mondo, ma – direbbero gli inglesi – è *carne morta*. Non porta da alcuna parte, non apporta alcun vantaggio. Se non a chi ancora lo vende (e ci vuole coraggio).

Prima ci si dà a Html, a LaTeX, a Xml, almeno a [Markdown](http://daringfireball.net/projects/markdown/), meglio è. Per quanto riguarda LyX occorre installare prima [MacTex](http://www.tug.org/mactex/) e poi metterci un pizzico di curiosità. I risultati non tarderanno e le possibilità sono infinite. come fare [questo](http://uwe-sthr.weebly.com/files/theme/CustomParShape.png), con Office? È una delle [nuove possibilità di LyX](http://wiki.lyx.org/LyX/NewInLyX21).

 ![Testo impaginato con LyX](/images/CustomParShape.png  "Testo impaginato su LyX")

Mi si dirà che però c’è sempre qualcuno che chiede file in formato Office. Se è solo per questo, c’è sempre [Pandoc](http://johnmacfarlane.net/pandoc/).

>You better start doing it right. / Let the dance begin.