---
title: "Guerra, turismo e roaming"
date: 2022-08-31T11:33:31+01:00
draft: false
toc: false
comments: true
categories: [Internet]
tags: [Tailscale, Clarke, Arthur Clarke, FastWeb]
---
Lo scrittore di fantascienza [Arthur Clarke](https://www.britannica.com/biography/Arthur-C-Clarke) affermò che il turismo sarebbe stata una delle precondizioni per porre fine alle guerre: scriveva in tempi di Guerra fredda e postulava che, nel momento in cui un milione di americani si fosse trovato in Unione Sovietica e viceversa, sarebbe stato sconveniente per ambedue le nazioni attaccare l’altra.

Dal 2008, quando Clarke ha intrapreso il suo viaggio oltrestellare personale, il mondo è molto cambiato e probabilmente la sua predizione andrebbe integrata con dei distinguo.

Nondimeno, scrivo da un Paese estero. Il mio provider mi fa usare Internet esattamente come a casa, certo con qualche giga in meno, ma più che sufficienti per qualche giorno.

Muoversi con Internet, mappe, messaggistica a piena disposizione è il paradiso del turista. Negli anni passati non è sempre stato così e ricordo bene quando c’era qualcosa, ma solo a costi proibitivi, e quando non c’era niente. Mi pare che l’incubo del roaming e dei suoi costi sia stato risolto solo nel 2017 e solo in Unione Europea; è già qualcosa.

Forse il turismo di massa porrà veramente fine alle guerre e forse no. Ci sono Paesi ancora troppo arretrati e retrivi. Sarà una lunga sofferenza, ancora per molto tempo.

Certamente, comunque, la piena disponibilità di Internet è una delle precondizioni di turismo di massa.

Per conseguenza, la piena disponibilità di Internet è una delle precondizioni per porre fine alle guerre.