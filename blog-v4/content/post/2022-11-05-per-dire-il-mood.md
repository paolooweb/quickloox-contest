---
title: "Per dire il mood"
date: 2022-11-05T01:42:14+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [Angband]
---
Dovrei attrezzare un post come si deve e ho materiale per scriverne almeno quattro. Invece finirò la settimana dentro la [prigione di ferro](https://it.wikipedia.org/wiki/Angband) e spero di [annientare un esercito di *o* e *O* prima di dormire](https://angband.live). Comprensione, grazie.