---
title: "Ohm Sweet Ohm"
date: 2020-05-07
comments: true
tags: [Froese, Schneider, Kraftwerk, Ohm, Pocket, Calculator, Robots]
---
Nel paradiso di musicisti c’era un posto libero accanto alla poltrona di [Edgar Froese](https://macintelligence.org/posts/2015-01-24-aquila-bianca/): quello riservato a Florian Schneider, che [lo ha appena occupato](https://www.npr.org/2020/05/06/851343980/co-founder-of-kraftwerk-florian-schneider-has-died).

Non saprei collegare in modo significativo l’epopea dei Kraftwerk con quella di Apple; del resto il loro percorso musicale è lontanissimo da tutto quello che è accaduto sulla West Coast anche se talmente moderno e anticipatore da fare male.

Li ho scoperti da ignorante totale, quando si sono fatti notare financo in televisione per le esecuzioni di [The Robots](https://www.youtube.com/watch?v=D_8Pma1vHmw) con lo sguardo fisso e le camicie rosse, la grafica post-costruttivista, la ritmica elettronica gelida, il tono mitteleuropeo. Ho capito che c’era di più sotto l’immagine. C’è una fetta consistente di musica commerciale contemporanea che deve praticamente tutto a loro.

Adoro i loro ritmi geometrici e l’evoluzione del linguaggio che li ha portati gradualmente dall’immaginario del dopoguerra industriale al [Computer World](https://www.youtube.com/watch?v=5ybQWD6N6Zo); il cittadino medio guardava con orrore alle calcolatrici tascabili, che avrebbero prodotto un pianeta di analfabeti aritmetici destinati a soccombere quando la catastrofe imprevista avrebbe tolto di mezzo l’energia elettrica, e intanto loro [cantavano](https://www.youtube.com/watch?v=eSBybJGZoCU) *I’m the operator / With my pocket calculator*.

(La catastrofe imprevista, a questo giro, ha azzerato tutto tranne le reti e il digitale, senza cui saremmo regrediti di cinquant’anni. Giusto per dire).

Quando il mio iPhone 5 da sedici gigabyte era saturo e dovevo prendere decisioni spietate sulle app che potevano abitarlo, ho cancellato tutta la musica per fare spazio, tranne le cose migliori dei Kraftwerk.

Florian Schneider non saliva sul palco dal 2008 e forse da anche prima. Non ci ho mai fatto *così* caso ma troverei bello se, tra i robot messi al posto dei musicisti a suonare i bis dei loro concerti, il suo fosse stato sempre presente. Certo non è l’immortalità, ma la dematerializzazione dell’artista e quella gli si addice. Ho sempre sperato che i Kraftwerk arrivassero a completare la loro digitalizzazione fino a reincarnarsi sul palco in robot capaci di sostituirli in tutto.

Gli dedico [Ohm Sweet Ohm](https://www.youtube.com/watch?v=QLwEG3cdeRw), dei tanti che meriterebbero.

<iframe width="560" height="315" src="https://www.youtube.com/embed/QLwEG3cdeRw" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>