---
title: "Il vaccino impossibile"
date: 2021-03-22T00:07:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [Pfizer] 
---
Ho accompagnato il prozio a ricevere la prima dose di Pfizer. Ho capito che non ce la possiamo fare.

La vaccinazione ha portato via un minuto, riguardando un quasi ottantanovenne che fatica a scoprirsi la spalla, in una giornata di sedicente primavera che alle nove del mattino faceva cinque gradi.

Tutto il resto invece. Per ogni vaccinato girano tre fogli A4, per quattro facciate: la nota informativa, l’anamnesi, l’anamnesi da portare il giorno della seconda dose. Chi non ci aveva pensato va a caccia di una penna, i fortunati scrivono appoggiati ai muri, per terra, assembrati attorno a un tavolino occasionale. L’infermiera alla reception improvvisata ha una manciata di biro, che spariscono ancora più veloci del vaccino.

E compili, compili, compili, e devi firmare, e devi firmare due volte, una non basta. Dopo due volte che hai scritto nome-cognome-nascita devi tirare fuori la tessera sanitaria. Lì c’è già scritto tutto. C’è memorizzato tutto. Serve scrivere il numero di tessera sanitaria, che è lì sulla tessera, che sta sulla banda magnetica della tessera e, se non ci sta, ci dovrebbe stare.

Noi fortunati eravamo previsti per le nove del mattino e abbiamo visto una quindicina di altri vaccinandi, più gli accompagnatori; quando ce ne andiamo ci sono più di cento persone che attendono il turno ammassati in un corridoio semisotterraneo da ospedale, ventilazione zero, sedie una manciata. Almeno la metà dei presenti è over 70, quando sono giovani.

A che serve avere una tessera sanitaria? Perché le persone convocate non hanno ricevuto le informazioni prima? Perché almeno quelli in grado di farlo non potevano compilare l’anamnesi da casa loro? A che serve convocare il triplo delle persone che è possibile vaccinare nel tempo dato e lasciarle ad aspettare mezze ore?

Credo di avere capito. Sono gli stessi che la scuola è scuola solo quando è in presenza. Così deve essere in presenza anche la burocrazia ospedaliera. Si perde tempo, si corrono gran rischi, ma si socializza.

Un vaccino contro l’intenzione precisa di fare le cose male non sarà mai prodotto.

*Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile.*