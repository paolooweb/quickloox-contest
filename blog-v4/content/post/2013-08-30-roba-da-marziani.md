---
title: "Roba da marziani"
date: 2013-08-30
comments: true
tags: [Giochi, Mac]
---
[Random Seed Games](http://randomseedgames.com) ha concluso con successo a fine luglio il [finanziamento su Kickstarter del gioco Lacuna Passage](http://www.kickstarter.com/projects/tylerowen/lacuna-passage). Speravano di raccogliere 40 mila dollari e ne hanno incassati più di 54 mila. Sulla pagina compaiono video in abbondanza.<!--more-->

Come accade per un progetto intelligente, Lacuna Passage funzionerà su Pc, Mac e Linux, non escludendo uno sbarco sulle *console* se la vendita procederà bene.

Il gioco ambienta una avventura sulla superficie marziana, sessantaquattro chilometri quadrati ricreati in 3D a partire dai dati veri e dalle fotografie provenienti dalle varie missioni della Nasa.

Il finanziamento aggiuntivo permetterà la compatibilità con un kit di realtà virtuale: elmetto in testa e ci si ritroverà la superficie di Marte tutto attorno.

Attenzione maniacale ai dettagli scientifici, tanto che bisognerà monitorare perfino l’analisi delle urine prodotte durante l’esplorazione.

L’autore è un maestro elementare, ideatore e capoprogetto, che dirige una dozzina di volontari con le giuste competenze. Mica terrestri qualunque.