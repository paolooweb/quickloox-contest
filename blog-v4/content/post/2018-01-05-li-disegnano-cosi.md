---
title: "Li disegnano così"
date: 2018-01-05
comments: true
tags: [Meltdown, Spectre, watch]
---
Ogni tanto Apple lavora al suo meglio e una di queste occasioni consiste nella [nota di supporto](https://support.apple.com/en-us/HT208394) riguardante le falle di sicurezza Meltdown e Spectre.

Ci sono la giusta parte di tecnica e chiarezza, nella giusta lunghezza. È chiara la parte teorica al pari delle conseguenze pratiche.

Per avere più tecnica, al prezzo della chiarezza, [Ars Technica](https://arstechnica.com/gadgets/2018/01/meltdown-and-spectre-heres-what-intel-apple-microsoft-others-are-doing-about-it/).

Per avere più chiarezza, con meno tecnica, [Misterakko su Quora](https://it.quora.com/L%E2%80%99allarme-lanciato-a-proposito-delle-falle-nella-sicurezza-di-tutti-i-processori-tipo-Intel-AMD-ecc-%C3%A8-reale-Cosa-comporter%C3%A0-Cosa-sono-Meltdown-e-Spectre/answer/Luca-Accomazzi).

Da ricordare: non è questione di sistemi operativi o, fino a un certo punto, di hardware (anche se watch è immune). Sono attacchi basati sui sistemi moderni usati per spingere al massimo le possibilità dei processori, i quali tendono a usare le stesse tecniche indipendentemente da chi li fabbrica.

Non esistono minacce concrete note e gli aggiornamenti già arrivati o in arrivo dovrebbero minimizzare i problemi. Spectre è una bruttissima bestia sul piano teorico ed eliminare il problema richiederà cambiamenti nella progettazione dei processori nonché nella programmazione delle app. Mai come in questa occasione la raccomandazione di scaricare solo software sensato e solo da fonti sicure è appropriata.

Rispetto ai processori, è proprio il caso di dirlo: non sono cattivi processori. È che li disegnano così.


