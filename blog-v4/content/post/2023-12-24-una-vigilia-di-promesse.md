---
title: "Una vigilia di promesse"
date: 2023-12-24T20:15:19+01:00
draft: false
toc: false
comments: true
categories: [Software, Internet]
tags: [QuickLoox, Strozzi, Filippo. Mimmo, Il blog del mulo, A2, Filippo Strozzi]
---
I piani hanno proceduto diversamente dal previsto. Questi auguri avrebbero dovuto trovarsi alla fine di una cosa di post completa, invece che in lavorazione. Però è Natale, grandi corse, tanti impegni, progetti ambiziosi, ottimismo della volontà.

Così è successo che sono ancora indietro. Così è anche successo che è arrivato grande aiuto da tanti amici, pratico, teorico, morale. Primi fra tutti [Mimmo](https://muloblog.netlify.app) e [Filippo](https://a2podcast.fireside.fm/hosts/filippo) e poi tanti altri. Non posso nominare tutti con precisione; di sicuro sono arrivati software, consigli, vecchi post da aggiungere alla raccolta.

Ho lo stesso arretrato di inizio dicembre. In teoria arriva un periodo di maggiore tranquillità, nel quale tornare in pari con il calendario. Questo è il prossimo proposito.

È impossibile pubblicare in retrospettiva gli auguri. Quindi per un po’ farò arrabbiare ancora **Macmomo**, perché dopo Natale appariranno post datati prima di Natale, oltre a quelli corretti. Dovrebbe consolarsi grazie a una piccola modifica nella gestione del feed Rss. Conto molto sullo spirito del Natale.

Chissà che cosa succederà domani. Auguro a ognuno che sia serena, felice, se possibile in compagnia, con musica, giochi, relax, le persone giuste.

Oggi so che può essere faticoso, ma vivere la corsa di questo blogghino insieme a chi ha la pazienza e la voglia di seguire rappresenta una delle esperienza più vive e pregnanti della mia vita.

Buon Natale.