---
title: "Portatile che trovi"
date: 2019-06-13
comments: true
tags: [iPad, Viticci, Surface, Logitech, MX, Master]
---
Dopo tante polemiche sul fatto che iPad sia o meno un computer e possa o meno sostituire il portatile, ho trovato impagabile questo [tweet](https://twitter.com/viticci/status/1137738474273103873) di Federico Viticci:

<blockquote class="twitter-tweet" data-lang="en"><p lang="en" dir="ltr">Guy sitting next to me at the airport and using a Surface was very interested in my iPad Pro setup. And yep, this is a Logitech MX Master 2S mouse with 5 programmable buttons paired with iPadOS via Bluetooth 😍 <a href="https://t.co/QxwHQBhnDS">pic.twitter.com/QxwHQBhnDS</a></p>&mdash; Federico Viticci (@viticci) <a href="https://twitter.com/viticci/status/1137738474273103873?ref_src=twsrc%5Etfw">June 9, 2019</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script> 

*Il tipo seduto accanto a me in aeroporto con un Surface era molto interessato alla mia configurazione di iPad Pro. E già, questo è un mouse MX Master 25 Logitech a cinque pulsanti programmabili collegato a iPadOS tramite Bluetooth*.

Come dire: nel 2020 cambiamo decennio e davvero, ognuno si farà la configurazione che vuole.