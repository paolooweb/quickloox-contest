---
title: "Quasi come a Fremont"
date: 2024-03-21T01:12:38+01:00
draft: false
toc: false
comments: true
categories: [Hardware, Software, DIY]
tags: [Brewintosh, Noki, Kevin Noki, Fusion 3D, Macintosh 128K, Mini vMac, Arduino, Adb, Apple Desktop Bus, Fremont]
---
Devo ai buoni uffici di [briand06](https://melabit.wordpress.com) avere trascorso tre quarti d’ora con la mascella slogata. È stato lui infatti a farmi puntare verso il video in cui [Kevin Noki costruisce il proprio Brewintosh](https://www.youtube.com/watch?v=7N9oz4Ylzm4).

Cose già viste si dirà, gli emulatori, Raspberry Pi e quant’altro, sì, carino.

Non esattamente. Parte dalla scocca vuota di un Macintosh 128k e… la riempie? Ma no; la misura tutta, poi si siede al computer e la rende dentro [Autodesk Fusion 3D](https://www.autodesk.com/products/fusion-360/overview). Dopo di che stampa una nuova scocca in 3D, interni ed esterni, per lavorare i pezzi fino a quando hanno un aspetto pressoché identico all’originale. Tutto viene riprodotto in ogni particolare, comprese le scritte adesive sul retro rifatte con stampa su vinile, i simboli in prossimità dei connettori e così via. Le uniche differenze sono la scritta *Brewintosh* al posto di *Macintosh*, un logo con tazza di caffè multicolore al posto della mela e una manopolina per lavorare sulla luminosità dello schermo.

Già qui ne avrei per rosicare per una settimana, Ma lui il Brewintosh, questo sì, lo riempie.

Recupera su eBay o comunque a prezzi da strausato un lettore di floppy disk Usb. Esegue le necessarie ingegnerizzazioni, programma il software opportuno et voila, Brewintosh può fare il boot da floppy disk, come un vero Macintosh. Il floppy viene espulso automaticamente, come su un vero Macintosh. Restasse incastrato, c’è il foro in cui inserire la graffetta aperta per forzare l’uscita, come su un vero Macintosh.

Compra a prezzo stracciato uno schermo da automobile, lo smonta e ne fa quello di Brewintosh.

Siccome non può avere il Raspberry Pi che pensava all’inizio, si procura un vecchio *thin client* al mercato delle pulci e ci monta Linux, per azionare l’emulatore [Mini vMac](https://www.gryphel.com/c/minivmac/).

Con una schedina Arduino e i giusti componenti, Noki abilita la connessione di tastiera e mouse originali Mac dell’epoca. Non contento, mette anche i connettori per periferiche Adb (Apple Desktop Bus). Dove serve un convertitore di segnale, ne assembla uno.

In corrispondenza del vano dove Macintosh 128K teneva la batteria per conservare le impostazioni permanent, lui sfrutta lo spazio per installare varie porte Usb e uno slot per scheda MicroSD. Tanto non si vede.

C’è praticamente tutto. Noki monta tutti i componenti nelle slitte e nei ripiani interni stampati in 3D, collega i cavi e ha l’equivalente di un Macintosh 128K, che fa il boot dal floppy, lancia MacPaint, i giochi, a vederlo da fuori è un Mac. L’obiettivo non è la ricerca dell’illusione, non perfetta perché i connettori disposti in basso sul retro sono diversi e più numerosi. Per il resto, il risultato è una cosa pazzesca, che Noki dice avere richiesto mesi di lavoro, da perderci il sonno.

Quest’uomo ha progettato in 3D, stampato in 3D, lavorato a mano, scritto software in C, ingegnerizzato l’interno di un computer con spazio ristretto, recuperato componenti, assemblato componenti elettronici, saldato, filmato tutto e montato un video di quarantasette minuti.

Tra retrocomputing e elettronica fai-da-te, lo dichiarerei uno statement definitivo. Chiudiamo tutto e ce ne torniamo a casa, non c’è altro da dire.

Per dare un’idea della differenza tra artigianale e industriale, ma soprattutto di quello che entra nel bagaglio culturale di una persona che intenda perseguire un progetto del genere, Steve Jobs dichiarò che dalla fabbrica iperautomatizzata di Fremont [usciva un Mac ogni ventisette secondi](https://thenextweb.com/news/steve-jobs-designed-apple-factory-the-birthplace-of-the-macintosh-considered-for-historic-status). Riuscire a ricreare un prodotto industriale raffinatissimo per l’epoca nella forma e nello spirito originale, sia pure adattato all’oggi, è un traguardo incredibile per il progresso tecnologico e per come il progresso suddetto è arrivato a disposizione delle persone comuni. Chiunque di noi sarebbe tecnicamente in grado di realizzare un progetto analogo e di permettersi la spesa; l’unica cosa che potrebbe fare la differenza in positivo o in negativo è… la conoscenza. C’è un bel po’ da riflettere.

<iframe width="560" height="315" src="https://www.youtube.com/embed/7N9oz4Ylzm4?si=M9UeVE9QfSzA5vol" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>