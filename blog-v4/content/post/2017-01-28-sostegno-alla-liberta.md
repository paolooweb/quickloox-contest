---
title: "Sostegno alla libertà"
date: 2017-01-28
comments: true
tags: [LibreItalia, LibreOffice, AllAboutApple, AAA]
---
L’anno deve cominciare bene prima che finisca gennaio.

Suggerisco con energia a questo scopo l’adesione a [LibreItalia](http://www.libreitalia.it) e ad [All About Apple](https://www.allaboutapple.com).

LibreItalia propugna la causa di [LibreOffice](http://www.libreoffice.org) in particolare e del software libero in generale. Libero nel senso della libertà, che è poter scegliere.

Ogni scrivania conquistata da LibreOffice diventa libera. O almeno più libera rispetto al conformismo, al monopolio, alla chiusura e all’opacità delle finestre oscurate. Non so se sia già di dominio pubblico, ma manca pochissimo: l’amministrazione di Assisi ha deciso di adottare il software libero.

All About Apple è un museo nato dalla passione e diventato una realtà di enorme valore culturale. E perfino turistico, adesso che sta alla Nuova Darsena del porto di Savona, in una *location* che merita la visita.

Ci ricorda da dove veniamo, fuori dalla nostalgia fine a se stessa, perché le sue macchine-reperto sono vive, accese, funzionanti, accessibili. E ci suggerisce dove stiamo andando, perché al posto di arroccarsi sul vecchio propone itinerari, iniziative, occasioni di apprendimento e intrattenimento che parlano al tempo presente.

Non mi dilungo oltre. Non dovrei essere io a convincere. Meglio compiere una piccola indagine personale, perché sono persone, luoghi, entità, azioni che parlano da sole. Mi limito ad aggiungere che, per chiunque delle persone al timone, metterei due mani sul fuoco.

Libertà è poter scegliere e conoscere la propria identità. Per i più burocraticamente orientati, c’è anche la possibilità del cinque per mille.