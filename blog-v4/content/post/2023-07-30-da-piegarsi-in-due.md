---
title: "Da piegarsi in due"
date: 2023-07-30T11:59:11+01:00
draft: false
toc: false
comments: true
categories: [Hardware]
tags: [Techradar, Birchtree, Gruber, John Gruber, Daring Fireball]
---
*Techradar* ha il coraggio di titolare [Apple ha bisogno di un iPhone pieghevole presto o gli iPhone non saranno più degni di essere comprati - i telefoni pieghevoli sono troppo fighi per ignorarli](https://www.techradar.com/phones/iphone/apple-needs-a-foldable-iphone-soon-or-iphones-wont-be-worth-buying).

*Birchtree* contiene il danno e [commenta](https://birchtree.me/blog/apples-boring-iphones-will-soon-not-be-worth-a-damn/)

>Ho scritto per Techradar in passato e scommetto che l’autore non ha scritto questo titolo, il quale potrebbe inquadrare il pezzo in una luce diversa da quella che intendeva.

Benissimo; i quotidiani italiani vivono di titoli inventati in spregio all’articolo e ci sta. Peccato che all’autore scappi una frase come questa:

>iPhone è rifinito abbastanza. È rifinito ben oltre ciò che gli altri produttori lavorano per raggiungere. Altri produttori potrebbero costruire un telefono con la stessa attenzione ai dettagli, solo che non se ne curano.

Il commento migliore [è di John Gruber](https://daringfireball.net/2023/07/jackasses_of_the_week_techradar):

>Altri cestisti potrebbero giocare come Michael Jordan, solo che non se ne curano.

Gruber completa l’analisi più seriamente. I pieghevoli di oggi hanno qualche difettuccio di base, tipo l’assenza di resistenza alla polvere, e prezzi di partenza che io tranquillamente andrei a prendermi un iPad Pro supercarrozzato anche se pieghevole non è. Rappresentano una percentuale di mercato minima e non ci sono per ora indizi di crescite significative. In passato Apple è arrivata in ritardo, quando c’era una richiesta di schermi di grandi dimensioni sui telefoni, e se l’è cavata benino.

Nulla vieta di pensare che Apple deciderà un giorno di offrire un iPhone pieghevole, se riuscirà a farlo secondo i suoi standard di *rifinitura*. Sostenere che senza un modello pieghevole iPhone perda di interesse, oggi, fa piegare la gente. Dal ridere.