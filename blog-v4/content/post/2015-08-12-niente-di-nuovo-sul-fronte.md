---
title: "Niente di nuovo sul fronte"
date: 2015-08-12
comments: true
tags: [Terminale, ElCapitan, OSX, InDesign, CS5, Java, Spotlight, Mail]
---
Man mano che procedo nell’utilizzo di El Capitan… è come usare Yosemite. Al momento la cosa che ha fatto più differenza è che il Terminale, quando c’è da digitare una password, mostra l’icona di una chiave dentro il cursore.<!--more-->

Da una parte può essere interpretata come deludente mancanza di novità, dall’altra come rassicurante continuità. A ognuno la propria; intanto constato una volta di più che l’atteggiamento *non aggiorno per non dover ricomprare decine di applicazioni* è davvero di un’altra epoca. Ho un vecchio InDesign CS5, ho riscaricato – su invito apposito del sistema – Java, risolto.

Devo ancora provare approfonditamente le novità di ricerca in linguaggio naturale dentro Spotlight e quelle in Mission Control; Mail muore orribilmente dopo essere stato lanciato e restituisce un messaggio di errore arcano. Ma queste sono cose da software beta; tutte le applicazioni che uso normalmente, e non sono Apple, funzionano.