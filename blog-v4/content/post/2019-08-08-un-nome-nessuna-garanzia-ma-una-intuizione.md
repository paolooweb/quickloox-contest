---
title: "Un nome, nessuna garanzia, ma una intuizione"
date: 2019-08-08
comments: true
tags: [FileMaker, Claris, ClarisWorks]
---
Ammetto che se le novità si limitassero al [cambio di nome da FileMaker a Claris](https://www.filemaker.com/blog/2019/claris-ceo-introduces-reenergized-brand-vision-strategy) sarei preoccupato più che solleticato nelle mie nostalgie.

Leggendo il *post* dell’amministratore delegato, però, si colgono sostanza e una grande intuizione:

>Invece di una singola offerta, costruiremo una raccolta di servizi mirati al potenziamento di chi risolve quotidianamente problemi.

È quello che vado scrivendo da anni con l’idea dell’automazione personale e che – non per niente occupa una carica come amministratore delegato di Claris – lui ha saputo esprimere con molta più precisione e sintesi.

Non credo che lo spazio dei prodotti Claris e la mia visione di automazione personale si sovrapporranno molto nel breve termine. Ma nel lungo sì. Contemporaneamente, FileMaker rimane e continua, per cui gli sviluppatori attuali hanno solo da essere soddisfatti per la crescita di possibilità che li attende.

Ecco, non mi aspetto sicuramente il *revival* di [ClarisWorks](http://groups.csail.mit.edu/mac/users/bob/clarisworks.php). Ci saranno invece numerosi sviluppi interessanti, come [hanno capito benissimo i FileMaker Guru](https://www.filemakerguru.it/devcon-19-filemaker-e-morto-viva-claris/).