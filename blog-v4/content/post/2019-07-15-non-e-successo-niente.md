---
title: "Non è successo niente"
date: 2019-07-15
comments: true
tags: [blackout, Ive, Mac, BBEdit]
---
Mi scuso per avere una infrastruttura di blog che non regge a un blackout in casa mentre il titolare si trova in viaggio. Spero di trovare tempo nella pausa di agosto per migliorare la configurazione.

Nel frattempo non mi sono perso molto: Jonathan Ive si congeda da Apple, sono usciti nuovi Mac, è arrivato il nuovo BBEdit… diciamo che è bello vivere in tempi interessanti e mi metto a recuperare.

Grazie come sempre per la pazienza e la presenza!
