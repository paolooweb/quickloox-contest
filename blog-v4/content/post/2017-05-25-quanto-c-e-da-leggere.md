---
title: "Quanto c’è da leggere"
date: 2017-05-25
comments: true
tags: [Swift]
---
Apple ha pubblicato il [corso di Swift su iBooks](https://9to5mac.com/2017/05/24/swift-app-ios-dev-free-ibooks/) e c’è un’estate sola a disposizione.

Oltretutto è gratis. Linko l’articolo di 9to5Mac perché, se qualcuno è interessato anche alle Teacher’s Guide, le guide per i docenti, è il modo più semplice per arrivare a tutto.