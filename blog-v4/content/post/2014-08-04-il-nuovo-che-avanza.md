---
title: "Il nuovo che avanza"
date: 2014-08-05
comments: true
tags: [NetApplications, Mac, OSX, Mavericks, Puma, MountainLion, Lion, SnowLeopard, Leopard, Tiger, 10.1, 10.4, 10.6, 10.5, 10.7, 10.8, 10.9, Yosemite, 10.10]
---
Il traffico web generato nell’ultimo mese tra i *desktop* (niente tavolette, niente tascabili), [dati NetApplications](http://www.netmarketshare.com/operating-system-market-share.aspx?qprid=10&qpcustomd=0), è così ripartito per quanto riguarda Mac.<!--more-->

* Mavericks (10.9): 4,12 percento.
* Snow Leopard (10.6): 0,84 percento.
* Mountain Lion (10.8): 0,70 percento.
* Lion (10.7): 0,65 percento.
* Leopard (10.5): 0,16 percento.
* Puma (10.1): 0,12 percento.
* Tiger (10.4): 0,04 percento.
* Mac OS X non identificato: 0,01 percento.

Mi sono permesso di creare un veloce grafico a torta che mostra più graficamente le proporzioni tra gli utilizzi. Mavericks è quello grosso, quello giallino è il suo immediato predecessore.

 ![OS X a luglio 2014](/images/osx-luglio-2014.png  "OS X a luglio 2014")

Mavericks da solo fa quasi due Mac su tre. Insieme a Mountain Lion sfiora i tre Mac su quattro.

A quelli che hanno un Mac non aggiornabile: condivido, il bello di un Mac è anche la sua eccezionale longevità.

A quelli che hanno il Mac aggiornabile ma non aggiornano perché le vecchie app, la sicurezza, non sono convinto, sto sempre una versione indietro, non si sa mai, la strada vecchia per la nuova, sono affezionato: rispetto, ma invito a verificare le proprie convinzioni.

Non ci sono mai stati così tanti Mac sintonizzati sul sistema operativo più recente. Ci sarà un motivo. No, non è che tutti devono cambiare Mac in continuazione. Il mio è già su Yosemite (10.10) e ha più di cinque anni.