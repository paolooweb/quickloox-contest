---
title: "La legge delle piattaforme antiche"
date: 2022-04-30T03:01:50+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [iOS, Mdj, Deatherage, Matt Deatherage, Gruber, John Gruber, Daring Fireball, Apple, Morì, Riccardo Morì, Nicola D’Agostino, D’Agostino, Substack, WWDC]
---
Apple invia una lettera a vari sviluppatori iOS: *la tua app manca di aggiornarsi da un tempo irragionevolmente lungo* [sarebbero due anni, NdL] *e ti invitiamo a mandarci un aggiornamento entro trenta giorni, o togliamo la app da App Store*.

[Nasce la polemica](https://www.theverge.com/2022/4/23/23038870/apple-app-store-widely-remove-outdated-apps-developers), su *The Verge*. Una mossa che colpisce soprattutto i piccoli sviluppatori indipendenti, che non possono permettersi di mantenere indefinitamente app di una certa età. Si crea anche un problema di conservazione del software meno nuovo, che acquisisce anche una connotazione storica per l’archeologo del software (Come conservare memoria del software memorabile?) o puramente pratica, se la filosofia di aggiornamento personale esclude modelli nuovi di *device* o cambi macchina frequenti. Che dire poi di app non più aggiornate ma scritte così bene da risultare funzionanti anche sui nuovi sistemi? O di lavori di rilevanza artistica, come installazioni mediali, o anche videogiochi non commerciali?

Intanto Apple pubblica un [chiarimento](https://developer.apple.com/news/?id=gi6npkmf&1651278071) e spiega che le app a rischio sono aggiornate da oltre tre anni e, anche, scaricate poco o niente in un periodo di almeno dodici mesi; che l’iniziativa dura dal 2016, con quasi tre milioni di app eliminate, e nessuno le ha dedicato una riga finora; che gli sviluppatori possono scrivere ad Apple e giustificare il non aggiornamento; che, se non vengono assecondati, hanno tre mesi di tempo per inviare un aggiornamento; che se non lo mandano, la app viene ritirata da App Store, ma vi tornerebbe quandunque attivasse un aggiornamento; che una app scaricata sui *device* locali resta installata anche se App Store non la distribuisce più.

Su un numero gratuito di [Mdj](https://mdj.substack.com/), Matt Deatherage titola *Le vecchie app a volte muoiono – la tecnologia non può vivere con un piede nel passato*. Spiega perché manutenere nel tempo le app è complicato ma lo stesso vale per la piattaforma sulla quale le app funzionano; in più ricorda che [WWDC](https://developer.apple.com/wwdc22/) si avvicina ed è probabile che si stia analizzando App Store alla ricerca di app che certamente non potranno sostenere una nuova versione di iOS (incidentalmente: una app che va in crash appena lanciata è suscettibile di immediata defenestrazione dallo Store). Commenta Deatherage:

>È questa politica iniqua verso le app che possono considerarsi definitive, per esempio certi giochi? In diversi hanno scritto quanto sintetizza John Gruber:  “Pixar non deve renderizzare nuovamente Toy Story un anno sì e uno no”. Vero, solo che il nastro VHS di Toy Story acquistato nel 1996 non funziona sulla Apple TV attaccata al televisore 8k. Puoi vederla sulla stessa TV con un videoregistratore, che però è l’apparecchio per il quale il nastro è stato progettato. Chi vuole la versione 4k del film deve comprarla e può farlo, in quanto Pixar ha eseguito il lavoro necessario per creare questa nuova versione diversa dalla prima.

>Se gli sviluppatori non possono fare lo stesso perché le loro app erano complete nel momento in cui sono state pubblicate per iOS 11, si può tenere un device vecchio e funzionante con iOS 11.

Scrivo questo post con [Editorial](https://omz-software.com/editorial/), notoriamente giunto alla fine del suo ciclo di aggiornamento; le ultime novità sulla app datano a due anni fa. Eppure la app è funzionale con iOS 15 ed è disponibile su App Store. Un esempio che mostra come l’eliminazione delle app ritenute obsolete sia tutt’altro che indiscriminata.

Un altro contesto personale è il mio iPad di prima generazione. Contiene soprattutto giochi, buona parte dei quali è sparito da Apple Store, alcuni titoli da molti anni. Eppure nessuno li ha cancellati e continuano a essere fruibili fino a che lo è l’iPad che li ospita. Numerose app sul mio iPad di prima generazione non possono essere usate su iPad Pro, perché non esistono più su App Store.

Trovo la posizione di Deatherage quella più vicina alla correttezza totale. Un esempio che mi viene in mente sono i libri. Un autore che voglia continuare nel tempo a pubblicare un testo tecnico, ha bisogno di aggiornarlo. I libri di *fiction* nascono completi e non hanno bisogno di aggiornamenti; però cambia l’edizione, magari vengono corretti refusi, se prima era copertina rigida ora esce la versione tascabile e via dicendo. È evidente che il contenuto del libro rimane sempre quello ma il suo contenitore deve modificarsi nel tempo, primo; secondo, la figura generica associata al mantenimento di un libro negli anni è l’editore, non la libreria. La libreria, quando lo ritiene opportuno, restituisce il libro all’editore, oppure lo manda al macero, senza preoccuparsi del problema generale della conservazione.

Nel caso delle app, è molto vero che quelle vecchie sono difficili da conservare da un punto di vista storico, che presuppone la possibilità di vederle in funzione; va detto però che oggi è molto più facile vedere e conservare software del primo Macintosh su un emulatore che su un autentico Macintosh del 1984. Il che mi porta a enunciare una legge:

>Più è antica una piattaforma hardware/software personale, più è probabile che ne esistano uno o più emulatori capaci di ovviare all’indisponibilità degli strumenti richiesti originalmente per eseguirla.
