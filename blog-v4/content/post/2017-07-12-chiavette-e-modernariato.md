---
title: "Chiavette e modernariato"
date: 2017-07-12
comments: true
tags: [Wind, Mac, E3131, Huawei]
---
Adesso la chiavetta Wind per la connessione dati su Mac ha ripreso a funzionare. Il problema è che la collegavo direttamente al mio MacBook Pro (inizio 2009).<!--more-->

Quando sono al mare la attacco a un iMac preistorico che risiede *in loco* e diffonde il Wi-Fi su tutte le stanze. Su quello, neppure il software altrettanto preistorico presente sulla chiavetta funziona; ma dalla barra dei menu è possibile lanciare la connessione telefonica e tanto basta.

I Mac ufficiali di casa invece, uno su El Capitan e l’altro su Sierra, non riconoscevano proprio la chiavetta.

Un po’ di ricerca con una connessione di fortuna ed ecco che arriva una [pagina esoterica del sito Huawei](http://consumer.huawei.com/en/mobile-broadband/dongles/support/downloads/e3372/) che in inglese fantasia offre lo scaricamento di vari pezzi software. Ho scaricato, testuale, *The driver tool of Device(for Mac10.12)* relativo in realtà a una Huawei E3372 (la mia è una E3131). Però, dopo un riavvio, funziona. Il software di bordo della chiavetta continua a essere inaccessibile; tuttavia la connessione funziona ed è tutto ciò che necessita. [**Aggiornamento del 23 luglio 2018:** il drive è sparito dal sito Huawei. Lo si trova su [questo sito](https://withsteps.com/4231/huawei-e3131-digi-net-mobil-stick-on-macos-sierra-10-12-5.html)].

Come faccio a sapere che la mia chiavetta è una Huawei ed una E3131? Semplice, l’ho aperta.

Non era più semplice sentire Wind, che mi ha venduto la chiavetta, e chiedere istruzioni al loro supporto, avendoli pagati?

Certo, sentirli è semplicissimo. Avere una risposta è ben altra questione.

Wind non provvede a distribuire aggiornamenti firmware delle proprie chiavette man mano che si verificano incompatibilità software e diventa impossibile usare un prodotto per cui si è pagato? Eh, no, proprio no. Non in questo caso, almeno.

Non c’è una pagina Wind che spieghi come reperire un aggiornamento software di un prodotto non più supportato, in modo che il cliente se la possa cavare da solo? Certo che no! Sarebbe un servizio al cliente.

L’idea è che le chiavette Wind siano oggetti di modernariato. Quando smettono di funzionare andrebbero disposte a mo’ di soprammobili in salotto, perché gli ospiti in visita possano ammirarne la linea, il design, il sapore della tecnologia di una volta.