---
title: "Internet porta libertà"
date: 2022-05-12T00:37:27+01:00
draft: false
toc: false
comments: true
categories: [Internet]
tags: [Musk, Elon Musk, Starlink]
---
Era già abbastanza chiaro, pensando al [grande firewall cinese](https://cs.stanford.edu/people/eroberts/cs181/projects/2010-11/FreeExpressionVsSocialCohesion/china_policy.html) e al blocco russo che ha fatto [decollare l’adozione di software per collegarsi via Vpn](https://www.aljazeera.com/news/2022/5/7/vpn-use-skyrockets-in-russia-during-ukraine-invasion) e aggirare le censure.

La vera cartina di tornasole è stata tuttavia l’*operazione militare speciale* russa.

L’inizio dell’invasione è stato accompagnato da un attacco cibernetico russo che [ha bloccato decine di migliaia di modem e router Viasat](https://www.cyberscoop.com/viasat-hack-russia-uk-eu-us-ukraine/), creando problemi in tutta Europa ma soprattutto mettendo in ginocchio la rete di comunicazione ucraina.

Forse è troppo affermare che, sotto questa premessa, oggi i russi sarebbero a Kyiv; certamente avrebbero avuto vita molto più facile.

Invece, due giorni dopo l’attacco, l’Ucraina chiese a Elon Musk di poter usare il servizio Internet satellitare Starlink in Ucraina. Musk fece arrivare diecimila parabole e nel giro di quarantotto ore le capacità ucraine di comunicazione si sono rivitalizzate.

È azzardato anche dire che, senza Starlink, gli ucraini non sarebbero riusciti a tenere testa ai russi. Sicuramente, però, il loro modello di difesa, basato sul miglior uso di risorse inferiori a quelle russe, avrebbe assai sofferto una maggiore difficoltà di diffusione delle informazioni di valore militare.

Un bell’articolo di *Wired* racconta anche quello che nessuno ha evidenziato finora; come gli ingegneri software di Starlink [abbiano lavorato contro il tempo](https://www.wired.com/story/starlink-ukraine-internet/) per rendere il servizio impervio ai tentativi russi di bloccare o confondere i segnali in arrivo a terra.

Questa vicenda certifica una verità incontrovertibile: Starlink sta aiutando l’Ucraina nel suo sforzo di restare libera.

In altre parole, *Internet porta libertà*.

La libertà non è gratis e non è asettica; porta con sé eccessi, errori, problemi, difficoltà. Ma è libertà ed è umanamente superiore alla sua mancanza.

Quando senti l’ennesimo allarme sui *rischi di Internet* o sui *pericoli di Internet*, pensa agli ucraini, ai quali Internet porta il rischio di riuscire a salvare la vita e l’identità.

Internet è piena di difetti, ma la vogliamo e non ci basta mai. Piuttosto fatichiamo di più per capirla e usarla bene, ma guai a cercare censure o blocchi come fanno in Cina o in Russia.

Con la speranza che domani un nuovo avanzamento tecnologico permetta di portare la rete libera anche in quei territori oppressi da governanti che non la vogliono. Più efficace di mille sanzioni.