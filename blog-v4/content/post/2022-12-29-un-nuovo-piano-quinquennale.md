---
title: "Un nuovo piano quinquennale"
date: 2022-12-29T02:32:23+01:00
draft: false
toc: false
comments: true
categories: [Hardware, Software]
tags: [iPhone X, iPhone 12, Apple Watch, Authenticator, Google Authenticator]
---
Ho estratto iPhone dalla tasca e metà dello schermo se ne era andata. La metà sinistra dello schermo restava, e resta tuttora, accesa in bianco, con qualche striscia occasionale di verde. Guasto a una parte importante della matrice dello schermo.

Ero a venti minuti da un Apple Store e ci sono andato: era la cosa più veloce da provare, specialmente passate le sette di sera.

Dovevo pensare che stiamo ricordando l’insuccesso di una famiglia nel trovare posto in città, al punto da doversi accontentare di una mangiatoia; cortesissimi, i ragazzi mi hanno detto che non avevano assistenza istantaneamente disponibile e che avrei potuto al più prendere un appuntamento.

Confrontato il costo atteso della riparazione con il fatto che sarei dovuto restare senza iPhone fino a dopo Capodanno e con il listino, ho preso all’istante un nuovo iPhone, con il solito algoritmo: il modello più vecchio ancora a listino, a parte iPhone SE. In pratica un iPhone 12, successore di un iPhone X che ha svolto onorato servizio per quasi cinque anni.

Le dimensioni dei due terminali sono pressoché uguali. Si sente una certa velocità in più, non esagerata. La prima vera differenza è il sottosistema della fotocamera: le operazioni sono nettamente più veloci, l’interfaccia è più raffinata e ricca, c’è una differenza evidente nelle foto scattate a bassa illuminazione, certe piccolezze come la registrazione video che parte tenendo premuto il pulsante di scatto sono migliorie cesellate in una cornice di lusso; fotografare con un iPhone 12 è proprio un’altra cosa rispetto a iPhone X.

Ho preso coscienza, ma poco altro, delle funzioni disponibili ai nuovi hardware: la stessa SIM ora mi fa navigare a 5G mentre prima ero a 4G; posso usare direttamente iPhone 12 come fotocamera di Mac (domani collaudo); cercherò di farmi prestare un caricatore [Qi](https://www.amazon.it/qi-wireless-charger/s?k=qi+wireless+charger) (senza dire wireless, che è una bugia) per vedere come si comporta. Eccetera.

Il passaggio da un sistema all’altra avrebbe dovuto essere magico. Voglio dire, quando ho aperto la scatola del nuovo iPhone 12 e l’ho acceso, iPhone X si è accorto e si è offerto di trasferire i suoi dati, stavolta davvero in modo wireless. Lo ha fatto davvero e nel giro di tre quarti d’ora il nuovo telefono era un clone del vecchio.

Se non è stato magico, è perché – si diceva – metà dello schermo di iPhone X è inaccessibile.

Per fortuna è rotto solo lo schermo e, sotto, tutto funziona regolarmente, anche se non lo si vede; se però un pulsante o un’icona sono dalla parte sbagliata, per fare la cosa giusta bisogna avere buona memoria e grande fortuna.

È andato tutto bene. E poi ho lanciato, ché mi serviva, Google Authenticator.

il quale è strettamente legato allo *hardware*, per motivi ovvi di sicurezza. La app era stata copiata fedelmente sul nuovo iPhone, però spiacevolmente vuota.

C’è un modo elegante di passare le funzioni di autenticazione da un telefono a un altro: la app sul telefono vecchio genera un codice QR che viene letto dalla app sul telefono nuovo.

E se metà dello schermo è guasta e metà del codice QR è invisibile? Piccolo scoop: metà codice, su Google Authenticator, non basta.

Una persona ligia al dovere avrebbe registrato i codici di backup per ciascun servizio e io sono quella persona. Il problema è che ho recuperato solo un codice su cinque e per gli altri quattro servizi si profilava una situazione poco allegra, tipo scrivere al supporto di Facebook sperando di essere ascoltati in un tempo inferiore a quello del decadimento del protone.

Per fortuna, una volta l’anno mi viene un’idea e questa è giunta in piena zona Cesarini: creato il codice QR su iPhone X, ho salvato uno screenshot. Anche se lo schermo fisico non funzionava, quello logico era a posto. Lo screenshot del codice è arrivato su Mac e da lì ho potuto importare i dati su iPhone 12. Sollievo.

Altro punto dolente: disaccoppiare un watch da un iPhone, se l’iPhone ha metà schermo fuori uso, è difficile. Almeno per me, che dopo qualche tentativo alla semicieca, stufo, ho azzerato watch, per riaccoppiarlo alla riaccensione con il nuovo iPhone 12.

Ora sembra funzionare tutto, salvo qualche avviso di tanto in tanto; c’è da reinserire una password di posta, o reimpostare un ID Apple su App Store, ultimi ritocchi.

Non posso fare considerazioni economiche sulla mia storia di acquisto recente, perché iPhone X mi era stato regalato. iPhone 12 è costato un migliaio di euro (per avere duecentocinquantasei gigabyte e una custodia). Come da algoritmo collaudato, ho scelto di evitare AppleCare, anche se ora copre pure smarrimento e furto. Vediamo come va. Se durasse un quinquennio, sarei soddisfatto.