---
title: "Impegni sovrapposti"
date: 2014-05-17
comments: true
tags: [Tizen, Hilton, Nuc, Intel, Samsung, Gear, Apple, iOS, OSX, Wwdc]
---
Presso l’[hotel Hilton di Union Square](http://www.hiltonsanfranciscohotel.com) a San Francisco si [riuniscono il 2 giugno](https://www.tizen.org) sviluppatori di tutto il mondo a scoprire [Tizen](https://www.tizen.org/), un sistema operativo *open source* dedicato a telefoni intelligenti, tavolette, *netbook*, televisori di nuova generazione e impianti di intrattenimento per gli abitacoli delle auto.<!--more-->

I non invitati sono tenuti a pagare 99 dollari per assistere; [riceveranno in regalo](http://recode.net/2014/05/16/tizen-to-offer-oprah-like-freebies-to-developers-as-lure/) un orologio intelligente [Gear 2](http://www.samsung.com/global/microsite/gear/gear2_features.html) da Samsung e un mini-computer [Nuc](http://www.intel.com/content/www/us/en/nuc/overview.html) da Intel.

Sempre il 2 giugno, presso il centro Moscone West di San Francisco, si [riuniscono sviluppatori di tutto il mondo](https://developer.apple.com/wwdc/) a scoprire i nuovi sistemi operativi di Apple.

Salvo alcuni studenti fortunati, i partecipanti hanno pagato un biglietto di 1.599 dollari.

Certamente un evento costa meno dell’altro e sicuramente con Tizen si può fare tutto quello che si può fare con iOS o OS X. Eppure ho pochi dubbi su quale sarà l’evento più scelto tra i due concomitanti.