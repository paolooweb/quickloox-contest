---
title: "Voglie ortogonali"
date: 2017-04-10
comments: true
tags: [TensorFlow, Sonnet]
---
Capisco perfettamente le esigenze e i desideri di tante persone che chiedono processori sempre più veloci, schede grafiche sempre più potenti, dischi sempre più capaci, funzioni di rete sempre più pervasive, efficienze sempre maggiori, prezzi sempre minori.<!--more-->

Da signore di mezza età, mi pongo ortogonalmente a questa visione perché vorrei *software* sempre più intelligente, sempre più evoluto, sempre più raffinato, sempre più sorprendente. Fuori da una manciata di impieghi certamente importantissimi che però riguardano pochi, è questione di software, non più di hardware.

Se là fuori c’è una Apple del XXI secolo, è chiusa in segreto tombale dentro un edificio ben dissimulato a creare un nuovo Macintosh [attorno a TensorFlow e Sonnet](https://deepmind.com/blog/open-sourcing-sonnet/) e a metterci sopra una interfaccia utente spaventosamente rivoluzionaria, che permetterà a *the rest of us* di coltivare reti neurali come oggi si è abituati a fare con i gerani sul davanzale.

Magari sarà una macchina sottile e leggera come un iPad, pronta in ogni momento a collegarsi a una rete planetaria di intelligenze algoritmiche per imparare, contribuire, farci arrivare dove oggi neanche speriamo. Oppure no. Ma il dettaglio dell’hardware interesserebbe veramente poco.