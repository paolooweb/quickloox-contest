---
title: "Avvistamenti / 2"
date: 2018-09-30
comments: true
tags: [Arese, MacBook, Pro, biblioteca]
---
Di ritorno da [TedX Modena](http://www.tedxmodena.it), sono di passaggio dalla biblioteca di Arese, nell’area metropolitana di Milano. [Seminario di illustrazione di libri per bambini](http://www.comune.arese.mi.it/Pubblicazioni/Manifestazioni/Manifestazioni_dettaglio.asp?D=01/03/2019&Data=&ID_M=43&ID=6757).

Mi scuso per la pessima foto, scattata in condizioni di illuminazione impossibili e in fretta per non disturbare. Comunque, è un MacBook Pro.

 ![MacBook Pro alla biblioteca di Arese](/images/biblioteca-arese.jpg  "MacBook Pro alla biblioteca di Arese") 