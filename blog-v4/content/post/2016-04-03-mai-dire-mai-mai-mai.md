---
title: "Mai dire mai mai mai"
date: 2016-04-03
comments: true
tags: [West]
---
Solidarietà a Kanye West, che ha presentato un suo nuovo album sul servizio di musica *online* Tidal commentando che *mai, mai, mai lo si sarebbe trovato su Apple* e dopo poche settimane [ha cambiato idea](http://www.cbc.ca/news/arts/kanye-pablo-wide-release-1.3516574). Capita a tutti di sbagliare.<!--more-->

Fine della solidarietà a Kanye West quando ha cancellato dal suo flusso Twitter la frase con il *mai mai mai* come se su Internet si potesse tornare indietro. Non si può e passi per fesso, anche se sei Kanye West.
