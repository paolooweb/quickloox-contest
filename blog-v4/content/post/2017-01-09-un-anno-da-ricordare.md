---
title: "Un anno da ricordare"
date: 2017-01-09
comments: true
tags: [Dilger, Samsung, Arm, Microsoft]
---
E adesso che si ricomincia davvero a fare sul serio, è tempo di chiudere i bilanci sul 2016 con un articolo assai lungo e assai poco scontato, [quello di Daniel Eran Dilger per AppleInsider](http://appleinsider.com/articles/16/12/31/editorial-apple-survived-2016s-onslaught-of-fake-news-and-failed-competitors).<!--more-->

Dilger ricorda, e dovrebbe farlo anche qualcun altro, che nell’anno appena concluso Apple ha rastrellato gran parte dei profitti in tutti i mercati dove compete.

Grazie al proprio investimento in Arm, l’azienda è all’avanguardia delle prestazioni nel settore *mobile*: il nuovo Google Pixel va la metà di un nuovo iPhone 7 in termini di prestazioni.

Gli investimenti in tutte le aree strategiche sono enormi. Gli avversari – vedi Samsung con le sue esplosive novità – non riescono a tenere il passo tecnologicamente.

Tutto questo nonostante un trattamento sui media poco amichevole e tendenzialmente falsato: bellissimo l’accostamento di Dilger su un titolo trionfale per un prodotto Microsoft – senza numeri a conforto – e la critica a un annuncio trionfale, sempre senza numeri, di Apple.

Ci sono sempre cose da migliorare, naturalmente; nel 2017 sarà importante vedere buone novità in campo Mac ed è solo un esempio. Bisogna però dire che il 2016 conta solo relativamente rispetto allo schema generale delle cose. E non è andato così male.
