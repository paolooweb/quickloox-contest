---
title: "Piccole speranze"
date: 2014-12-25
comments: true
tags: [Badland, Carcassonne]
---
Nella giornata mi piacerebbe giocare in *multiplayer* a [Badland](https://itunes.apple.com/it/app/badland/id535176909?l=en&mt=8), che in versione natalizia ha veramente un’atmosfera oltraggiosa, oppure a [Carcassonne](https://itunes.apple.com/it/app/carcassonne/id375295479?l=en&mt=8).<!--more-->

Informaticamente è tutto quello che chiedo, oltre naturalmente al buon funzionamento della fotocamera.

E così spero di voi.