---
title: "Il regresso in tre storie"
date: 2022-05-22T01:49:13+01:00
draft: false
toc: false
comments: true
categories: [Internet]
tags: [Internet Explorer, Alfabert, Desenzano, Brescia, Guangdong]
---
Non c’è bisogno di arrivare agli azzerati mentali del grafene nei vaccini; la prova che bisogna essere ottimisti, ma proprio non ce la possiamo fare, sta nell’osservazione della gente comune, quella che i computer sono un male necessario, che ci fidiamo più della agendina scritta a mano.

Prima storia. Sono reduce da una fiera di settore, una cosa dei vecchi tempi, padiglione fieristico pieno di stand disposti secondo la lezione degli accampamenti romani e classificati come se giocassero a battaglia navale: B7, D4, G3. In ogni stand compare in bella evidenza la propria coordinata, un A4 con su la lettera, un altro A4 con su il numero.

Parla lo speaker e invita tutti a presenziare alla dimostrazione tal dei tali presso *lo stand settantaquattro*.

La morale: la gente che ha disegnato la pianta dell’esposizione non si è mai parlata con la gente che amministra lo spazio fieristico. Soprattutto, non hanno mai condiviso un file con su la mappa degli spazi e degli espositori. Oppure la mappa è stata condivisa ma nessuno se l’è filata. Regresso.

Seconda storia. Di ritorno dalla fiera, il tabellone (che dovrebbe essere) informativo recita

>MATERIALE DESENZANO BRESCIA

Circa venticinque chilometri di autostrada.

A un certo punto, *un punto preciso*, sulla linea tratteggiata di separazione tra la seconda e la terza corsia si nota abbandonato un grosso mastello di alluminio, che sarà servito nel corso di qualche lavoro di manutenzione.

*Tra seconda e terza corsia* significa auto lanciate a centrotrenta all’ora o peggio. Incocciare in un ostacolo del genere rischia di creare problemi.

Un mezzo di servizio, mentre passiamo, sta parcheggiando in corsia di emergenza proprio in corrispondenza del mastello. I lampeggianti accesi suggeriscono che il mezzo sia in servizio e la posizione suggerisce che sia lì per recuperare il mastello stesso.

La morale: chi lavora alle autostrade sapeva perfettamente dove stava il mastello e si è curato di intrattenere i clienti con un indovinello riguardante un tratto piuttosto lungo dove tutto era assolutamente normale, fatta eccezione per un singolo punto situato in un singolo chilometro. Nessuno si è fatto male, credo, ma certo il merito dei tabelloni segnaletici è minimo. Regresso.

Terza storia. [Alfabert](https://twitter.com/Alfassassina/) [scrive](https://twitter.com/Alfassassina/status/1528058754087890944) da Guangdong:

<blockquote class="twitter-tweet"><p lang="it" dir="ltr">Non è un fotomontaggio!! <a href="https://twitter.com/loox?ref_src=twsrc%5Etfw">@loox</a> <a href="https://t.co/qhZUGhxQMF">pic.twitter.com/qhZUGhxQMF</a></p>&mdash; Alfabert (@Alfassassina) <a href="https://twitter.com/Alfassassina/status/1528058754087890944?ref_src=twsrc%5Etfw">May 21, 2022</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

La morale: non basta considerare la tecnologia. Bisogna anche considerare quella attuale. Il retrocomputing va bene come hobby e per organizzare ritrovi tra vecchi amici, se non amici vecchi. Regresso.