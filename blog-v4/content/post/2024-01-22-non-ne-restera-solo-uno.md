---
title: "Non ne resterà solo uno"
date: 2024-01-22T00:46:14+01:00
draft: false
toc: false
comments: true
categories: [Hardware]
tags: [Vision Pro, Maschwitz, Stu Maschwitz, IKEA, Iskaerna, Altroconsumo, Graham, Paul Graham]
---
Più ci penso e più sono convinto che lo avrei chiamato *Vision* e basta. Più penso a quello che penso, più mi convinco che ci sarà una ragione per essere [Vision Pro](https://www.apple.com/apple-vision-pro/) e che il futuro immersivo dell’ecosistema Apple non starà fermo per sempre in alta montagna economica, a quota tremilacinquecento.

Nell’imminenza del lancio del due febbraio iniziano a moltiplicarsi i commenti, gli slanci di fantasia, le profezie, i salti nel vuoto, le invenzioni, la fantascienza, i vorrei ma non posso. Starei sulla leggerezza, dato che Vision Pro stesso è pesantino: tra seicento e seicentocinquanta grammi, circa un iPad, quasi un pallone da basket, un centinaio di grammi più di quasi tutta la concorrenza. Che ha la batteria integrata.

Leggerezza vuol dire [rispondere con ironia ma non tanto](https://www.ikea.com/us/en/p/iskaerna-led-table-lamp-multicolor-90510403/) alla domanda *dove lo tengo?*. Oppure gustarsi il filmato promozionale del *making of*, girato in modo che [funziona perfettamente (anche in Vision Pro) sia in sedici noni che in nove sedicesimi (verticale)](https://www.threads.net/@prolost/post/C2VRzfGvMDa)

Anche perché questa è un prodotto uno-punto-zero. Ricordo Macintosh 128k con gli scambi infiniti tra floppy e l’assenza dei tasti cursore. iPhone con la penosa connessione Internet 2G, il connettore a trenta piedini, zero app. iPad che pesava troppo e secondo i geni di Altroconsumo si candidava a [migliore cornice digitale di tutti tempi](https://macintelligence.org/posts/2010-02-08-quelli-degli-antiforfora/).

Il mio watch di prima generazione si illuminava solo se giravo il polso e faceva metà delle cose di oggi con metà dell’autonomia di oggi. La prima versione di un prodotto è solo un assaggio di quello che verrà, se le cose sono fatte bene. Non trovo il link ora ma ricordo perfettamente una frase di Paul Graham: *Se nessuno trolla il tuo prodotto, vuol dire che hai aspettato troppo per uscire*.

Ho già visto un po’ di commenti e qualche aristocratico che dichiara altezzoso *a questo giro passo*. In effetti è facile che accada, genio, perché il lancio è solo americano.

Certo, anche iPhone fu lanciato solo negli States e c’era gente che, sapendo di avere un volo in programma, raccoglieva le prenotazioni. Un signore brianzolo raccolse anche la mia. Ma erano cinquecento euro e c’erano libri da scrivere, riviste da alimentare. A sette volte tanto il problema non è di chi commissiona, ma di chi dovrebbe presentarsi in Apple Store con cifre imbarazzanti e uscirne con borse altrettanto, alla mercé delle dogane se non peggio.

Anche questa volta: è solo l’inizio. Stiamo leggeri.