---
title: "Vorrei tornare al liceo"
date: 2016-05-27
comments: true
tags: [Maxima]
---
Per alzare la mano durante l’ora di matematica e dire che prof, esiste un sistema di calcolo algebrico computerizzato *open source* che si chiama [Maxima](http://maxima.sourceforge.net) e funziona su Windows, su Mac e su Linux. Potremmo fare lezione con quello, invece che scarabocchiare con il gesso e pasticciare sui quaderni a quadretti.

Oltretutto è scritto in Common Lisp, che significa nulla dal punto di vista dell’utilizzatore ma a me fa simpatia a prescindere. Che si fa, prof? Lo scarichiamo?

Chissà se nei licei del 2016 qualcuno sta alzando la mano. O facendo clic.