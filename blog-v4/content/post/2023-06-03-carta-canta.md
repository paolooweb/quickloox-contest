---
title: "Carta canta"
date: 2023-06-03T01:36:45+01:00
draft: false
toc: false
comments: true
categories: [Internet, Software]
tags: [Contra Chrome, Copernicani]
---
Ho avuto il grosso piacere di ritrovare tra le mani una copia cartacea di [Contra Chrome](https://macintelligence.org/posts/2022-07-14-come-cambia-il-fumetto/), il fumetto attivista sui pericoli posti da Chrome e Google per la privacy e la democrazia alla cui traduzione ho contribuito sotto l’egida dei [Copernicani](https://copernicani.it).

Nel rivederlo, è venuto bene. La mia posizione è molto meno militante di quella del fumetto, ma un po’ di esagerazione in questo caso non fa male almeno per acquisire la consapevolezza. Niente di male a usare Chrome o a permettere al software di acquisire dati personali che servono per corrisponderci un servizio; purché tutto sia fatto in modo onesto, alla luce del sole, con chiarezza e con la possibilità semplice per chiunque di chiamarsi fuori.

Non lo faccio da un po’, quindi mi permetto: iscriversi ai Copernicani è una cosa buona e utile, nell’Italia di oggi dove la tecnologia continua a essere negletta, ignorata e sottovalutata.