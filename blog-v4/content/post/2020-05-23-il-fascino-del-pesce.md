---
title: "Il fascino del pesce"
date: 2020-05-23
comments: true
tags: [Forstall, Microsoft, Seattle, Jobs, NeXT, MacRumors]
---
Dove Scott Forstall, artefice principe del successo iniziale di iPhone, [racconta di quando era a colloquio con NeXT per farsi assumere](https://www.macrumors.com/2020/05/22/scott-forstall-shares-story-interview-steve-jobs/) nello stesso periodo in cui aveva ricevuto un’offerta da una grossa software house situata vicino a Seattle.

Steve Jobs interferisce nella procedura, prende il posto di un reclutatore e dopo una conversazione gli garantisce il posto, a patto che faccia finta di niente e si mostri comunque interessato anche durante i colloqui successivi.

La software house spedisce a casa di Forstall un salmone, a promemoria di come potrebbe mangiare gustoso se accettasse la loro offerta.

Tutti i modi per reclutare un talento sono buoni, ma alcuni più rivelatori di altri.

Forstall sostiene di avere cucinato il salmone e avere accettato l’offerta di NeXT. Secondo me ha preso tre giorni di riflessione e si è accorto che il pesce iniziava a puzzare.