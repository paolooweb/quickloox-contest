---
title: "Visioni e previsioni"
date: 2024-01-25T00:59:18+01:00
draft: false
toc: false
comments: true
categories: [Hardware, Software, Internet, Entertainment]
tags: [Vision Pro, NBA]
---
Quando ho detto, molti mesi fa, che Vision Pro avrebbe fatto concorrenza alla televisione e agli *home theatre*, mai avrei immaginato che nel catalogo delle app su misura disponibili dal primo giorno ci sarebbe stata [quella dell’Nba](https://twitter.com/sigjudge/status/1750074473640816994), la lega professionistica americana del basket.

<blockquote class="twitter-tweet"><p lang="en" dir="ltr">The VisionOS <a href="https://twitter.com/NBA?ref_src=twsrc%5Etfw">@NBA</a> app is everything I had hoped for, for day one. Tickers, multiview… and someday, court-side view!<br> <br>(<a href="https://t.co/pP38F36nC5">https://t.co/pP38F36nC5</a>) <a href="https://t.co/jrf7S1I8iA">pic.twitter.com/jrf7S1I8iA</a></p>&mdash; Sigmund Judge (@sigjudge) <a href="https://twitter.com/sigjudge/status/1750074473640816994?ref_src=twsrc%5Etfw">January 24, 2024</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

L’Nba governa sì il campionato di basket più bello del mondo, ma a monte di questo è un titano dell’intrattenimento, perché dalla visione delle partite presso i canali migliori per il pubblico più vasto derivano introiti ridistribuiti alle squadre per poter offrire uno spettacolo il più possibile all’altezza delle aspettative.

Se l’Nba si muove in qualunque spazio dell’intrattenimento concernente la visione delle partite, ne fa una questione di business e di redditività.

Impossibile che si aspettino chissà che fatturati da Vision Pro, che nelle previsioni più rosee venderà un mezzo milione di unità da qui a fine anno. In una nazione di duecento milioni di spettatori potenziali, sono le briciole delle noccioline.

La sua azione sembra dettata da una parola: *investimento*. E sarà anche stato ben soppesato, perché non è gente che spende per divertimento o per darsi un tono.

Chi valuta Vision Pro secondo gli standard attuali fa benissimo, eh, tuttavia un’occhiata a chi cerca di individuare gli standard prossimi varrebbe la pena di darla.