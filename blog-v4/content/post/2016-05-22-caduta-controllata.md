---
title: "Caduta controllata"
date: 2016-05-22
comments: true
tags: [iPhone, 6s, Galaxy, Samsung, S7]
---
Di test di stress e resistenza sui computer da tasca ne escogitano a bizzeffe, solo che [questo](https://www.youtube.com/watch?v=MoSIXFZPCNo) è stato standardizzato: è uno speciale macchinario a lasciar cadere al suolo questo o quel modello, in modo sempre uguale e ripetibile. E il computer viene fatto cadere più e più volte fino a quando diventa impossibile effettuare una chiamata di emergenza, per via della rottura dello schermo touch.

I primi due nella classifica dei più resistenti sono un modello di Htc e iPhone 6s, tanto per cambiare. Questi tre minuti mostrano l’andamento della sfida tra iPhone 6s e Galaxy S7 Samsung.

S7 costa un po’ meno, ma mica così tanto meno. E però la resistenza agli urti non sembra proporzionale.

<iframe width="600" height="338" src="https://www.youtube.com/embed/MoSIXFZPCNo" frameborder="0" allowfullscreen></iframe>