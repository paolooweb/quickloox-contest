---
title: "Quasi senza dirlo"
date: 2015-04-11
comments: true
tags: [iOS8.3, OSX10.3.3, Foto, Aperture, iPhoto, iCloud, iPad, Keynote]
---
Attenzione a un corso di Apple che si sta rivelando sempre più nuovo: eravamo abituati a scoprire nuove funzioni fondamentali quando usciva il sistema operativo e a vedere i soli *bug fix* nelle versioni intermedie.<!--more-->

Non è più così. [iOS 8.3](https://support.apple.com/kb/DL1806?viewlocale=it_IT&locale=en_US) porta novità che non sono quelle di un iOS 9 e però sono degne di nota, come la possibilità di scaricare le app gratuite senza dover fornire una password o un set di caratteri Emoji dilatato all’inverosimile in nome della non discriminazione. OS X 10.10.3 porta con sé la nuova app [Foto](https://www.apple.com/it/osx/photos-preview/) che sostituisce iPhoto e Aperture.

Foto non piace a tutti (interfaccia stupendamente rielaborata contro mancanza di funzioni evolute che giustificavano l’acquisto di Aperture). Tuttavia ufficializza [iCloud Photo Library](https://support.apple.com/en-us/HT204264), l’inizio dell’idea che le foto stiano principalmente sul cloud prima che sul disco rigido. È un cambiamento pratico non da poco.

Tendenza interessante, perché se da una parte abitua a non dare niente per scontato – cosa a volte scocciante quando c’è una configurazione affidabile che non dà sorprese e arriva un aggiornamento importante – dall’altra può portare vantaggi inaspettati se i programmatori lavorano bene. Sono reduce da una giornata di convegnistica dove ho messo a dura prova iPad e, per quanto non possa produrre prove certe, ho la netta sensazione che iOS 8.3 abbia migliorato la reattività del sistema e, nella situazione, [Keynote](https://itunes.apple.com/it/app/keynote/id361285480?l=en&mt=8), che ha funzionato senza il minimo appesantimento e per giunta con una [app di registrazione audio](https://itunes.apple.com/it/app/voice-record-pro-7/id810588885?l=en&mt=8) dietro le quinte. Il mio iPad è un ormai veterano di terza generazione e Keynote, prima, tendeva a cambiare *slide* con quel decimo di secondo di ritardo che tradiva l’impegno al limite delle risorse. L’altroieri le transizioni erano istantanee.