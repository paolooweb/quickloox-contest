---
title: "I furbetti del computerino"
date: 2021-12-04T00:43:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [Chrome, macOS, Downloads] 
---
Sono su Mac e uso una famosa piattaforma di collaborazione, attraverso la sua app.

In un canale di chat viene depositato un documento. Nel menu associato al documento stesso scelgo il comando di download. Il documento non si scarica e ricevo un errore dalla app.

Riprovo con un altro documento per controprova. Stesso risultato.

Apro le preferenze della app e constato che manca qualsiasi possibilità di indicare dove scaricare i file.

Apro la cartella Downloads di macOS e i documenti sono lì entrambi, solo che hanno un nome file modificato rispetto all’originale. Provo a spostarli in un’altra cartella e ricevo un errore.

Chiudo tutto. Riapro la stessa chat di prima, ma con Chrome al posto della app dedicata. Il browser scarica istantaneamente i documenti nella cartella corretta.

Chi può essere così furbetto da creare una app che su macOS funziona intenzionalmente peggio rispetto alle altre piattaforme? Che scopo avrà?

_Per ora si può lasciare un commento dalla [pagina apposita di Muut per Qui
ckLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile._