---
title: "Breve invito a effettuare il backup"
date: 2022-01-08T01:31:23+01:00
draft: false
toc: false
comments: true
categories:
tags:
---
Posso vantare una certa esperienza, diretta e indiretta, nel *computing*. Per questo mi sento in diritto di esortare ciascuno a proteggere i propri dati, con il backup.

Un backup ha costi irrisori rispetto ai benefici. Alla gran parte delle persone non succede niente se il computer dà qualche errore, ma in pochi casi sfortunati le conseguenze possono essere molto gravi e si può arrivare perfino alla perdita totale e irreversibile dei dati.

Un backup può scongiurare quasi sempre anche l’eventualità peggiore.

C’è chi preferisce evitare il backup. Ritiene che l’eventualità di perdere dati sia talmente remota da essere trascurabile. Pensa che al più capiterà a qualcun altro. Crede che la spesa di un disco di backup sia la conseguenza di una cospirazione delle multinazionali per fare comprare a tutti qualcosa di inutile.

Posto che gli sia capitato in precedenza di perdere dati, è convinto che per legge di probabilità difficilmente gli ricapiterà la stessa cosa e quindi di essere già protetto, senza bisogno di backup.

Certamente il backup richiede un piccolo impegno aggiuntivo: un disco extra e un modestissimo consumo di corrente. Qualcuno obietta che effettuerà il backup solo quando la procedura gli garantirà di non perdere dati. In realtà la garanzia non esiste per nessuno e neanche il migliore backup scongiura con certezza una perdita di dati; il rischio però si riduce moltissimo e certo qualcuno perde i dati lo stesso, tuttavia parliamo di un numero assai minore di quanto sarebbe se non ci fossero i backup.

Qualcuno obietta che i backup non sono definitivi. Un disco di backup infatti ha una durata limitata nel tempo e poi occorre ricorrere a un nuovo disco di backup. È assolutamente vero; peraltro, un nuovo disco di backup al momento giusto rafforza la protezione contro la perdita di dati, che tende a indebolirsi con il tempo man mano che il disco di backup perde efficienza.

Alcune persone hanno una reazione emotiva verso il backup. Il solo pensiero che i loro dati possano essere a rischio li inquieta. Disgraziatamente, hanno bisogno di un buon backup come tutte le altre.

C’è chi denigrerà l’effetto positivo di un backup e sosterrà che conviene intervenire a posteriori sul disco danneggiato con qualche programma in grado di ristabilire l’ordine. Il problema è che questi programmi quasi sempre hanno un costo sostenuto e nessuno ha la garanzia di riuscire a risistemare un disco danneggiato a prescindere dal tipo di danno. La prevenzione del backup è molto più vantaggiosa, perché non espone i dati alla roulette russa di un danno di gravità incerta seguito dall’uso di un programma che *forse* potrebbe ripararlo.

Si è letto di persone che rifiutano il backup per paura di effetti a lungo termine che potrebbero interessare il supporto su cui risiedono i dati. Il punto è che non c’è alcuna prova di danni causati a lungo termine ai dati dai programmi di backup; invece, si è già visto molte volte come una perdita di dati senza protezioni possa avere effetti devastanti a lungo termine, a medio termine, a breve termine.

Si è visto come vi siano persone che praticano il backup senza averne capito le caratteristiche e i limiti, per cui espongono i propri dati a comportamenti sconsiderati, perché *tanto c’è il backup*. È un grosso errore; il backup è una grande sicurezza in più, ma bisogna comunque muoversi responsabilmente nel trattare i dischi e i dati.

Il backup non è un sistema perfetto per tutelare i nostri dati; comunque, è il migliore che abbiamo. In un mondo privo di backup, gli effetti della perdita di dati sarebbero estremamente più gravi di quelli attuali.

Il backup è la risposta migliore per tenere al sicuro e in salute i nostri dati.

*Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile.*
