---
title: "Grandi opere"
date: 2015-03-08
comments: true
tags: [Knuth, Taocp, DwarfFortress, Toady, Adams, Wiener, Feynman, vonNeumann, Morgernstern, Dirac, Pauling, Mandelbrot, Einstein, Woodward, Hoffmann, Smith, cibernetica, frattali, chimica, giochi, relatività, Swift, watch]
---
[Donald Knuth](http://www-cs-faculty.stanford.edu/~uno/index.html) ha settantasette anni e sta lavorando a tempo pieno per terminare la collana di volumi [The Art of Computer Programming](http://www-cs-faculty.stanford.edu/~uno/taocp.html) (Taocp). Prevede di metterci [circa vent’anni](http://www-cs-faculty.stanford.edu/~uno/retd.html). Il livello di Taocp è questo.<!--more-->

>Alla fine del 1999 questi libri sono stati collocati da American Scientist tra le migliori dodici monografie scientifiche di ogni tempo, assieme a Dirac sulla meccanica quantistica, Einstein sulla relatività, Mandelbrot sui frattali, Pauling sui legami chimici, Russell e Whitehead sulle fondamenta della matematica, von Neumann e Morgernstern sulla teoria dei giochi, Wiener sulla cibernetica, Woodward e Hoffmann sulla simmetria orbitale, Feynman sull’elettrodinamica quantistica, Smith sulla ricerca della struttura e la raccolta completa delle opere di Einstein.

*Incgamers* ha pubblicato [un’intervista a Tarn *Toady* Adams](http://www.incgamers.com/2015/03/dwarf-fortress-interview), uno dei programmatori di [Dwarf Fortress](http://www.bay12games.com/dwarves/). Questo è l’*incipit*:

>Come si possa anche solo cominciare l’introduzione a un’intervista su Dwarf Fortress è per me un completo mistero, come diverrà evidente nel giro di pochi istanti. È un simulatore *roguelike* di nani costruttori di fortezze, di cui forse avete sentito parlare anche senza averlo giocato. Se lo avete giocato ci avete probabilmente perso centinaia di ore, oppure avete smesso dopo cinque minuti. È un gioco costruito da due sole persone, da tredici anni in lavorazione, che resterà in lavorazione per i prossimi venti, senza la minima traccia di ironie.

Quando si legge che Apple [ha lavorato quattro anni](http://www.iphonehacks.com/2014/06/apple-worked-four-years-swift-goal-make-programming-approachable-fun.html) alla creazione di Swift, o che l’azienda ha lavorato con [attenzione prossima al fanatismo](http://howtospendit.ft.com/technology/77791-the-man-behind-the-apple-watch) sulla velocità con cui il fondo della confezione di watch si sfila mentre si tiene in mano la chiusura superiore, c’è chi lo considera esagerato, assurdo, fuori luogo, mitologico, esca per illusi che ci credono.

Invece *è il minimo* per creare qualcosa che resti e lasci veramente un segno, oltre che restare.

Apple è il minimo indispensabile. Il che qualifica irreparabilmente la concorrenza.