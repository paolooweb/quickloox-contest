---
title: "Obiettivo il nulla"
date: 2015-01-14
comments: true
tags: [Flickr, iPhone, iPad, S3, Samsung, Galaxy, Apple, Android]
---
Nel 2013 i marchi di fotocamera più usati su [Flickr](https://www.flickr.com) erano Canon, Nikon e Apple in ordine decrescente. Nel 2014 sono Canon, Apple e Nikon.<!--more-->

Nel 2013 le tre singole fotocamere più usate erano tre iPhone. Ugualmente nel 2014.

Nel 2013 le prime cinque fotocamere cellulari più usate su Flickr erano cinque iPhone. Nel 2014 sono quattro iPhone più il Galaxy S3 Samsung. Che ha scalzato dalla sua posizione, udite udite, *l’iPhone originale del 2007*. A causa di ciò, invece che occupare otto delle prime dieci posizioni come avveniva nel 2013, Apple ne detiene per il 2014 solo sette.

Qui il [riassunto della situazione](http://petapixel.com/assets/uploads/2015/01/rankings.jpg).

Questo diluvio di aggeggi Android, che conquistano quote di mercato a valanga, non viene usato per andare su Internet e va bene. Non viene usato neppure per fotografare (o almeno per pubblicare su Flickr). Di questo passo scopriremo che vengono comprati per fare nulla. Siamo sicuri che siano veramente concorrenza per iPhone e iPad?

 ![Flickr](/images/flickr.png  "Flickr") 
