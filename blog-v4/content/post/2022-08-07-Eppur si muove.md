---
title: "Eppur si vede"
date: 2022-08-07T02:21:18+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [Tv+, baseball]
---
Sono uno specialista nel venire a sapere le cose per ultimo. Se non altro, mantengo il gusto della sorpresa.

Smanetto con Tv+ per ragioni filiali e mi imbatto nel Friday Night Baseball. Memore dell’[annuncio di marzo](https://macintelligence.org/posts/2022-03-10-che-palle-linnovazione/), provo a cliccare. Tv+ mi chiede di poter accedere alla geolocalizzazione per capire la mia nazionalità e decidere se posso guardare o meno. Accetto, per giocare onestamente.

*Ho visto la partita.*

Funziona. Niente bisogno di VPN, trucchi, app di dubbia reputazione, circuiti alternativi. Funziona.

L’unica cosa che manca e mi spiace è la pubblicità originale, che immagino esclusa dalla trattativa sui diritti. Mi spiace perché è un’espressione culturale, nel senso positivo e nel senso negativo della locuzione, e la pubblicità è quasi più interessante da guardare di certe fasi della partita.

La partita c’è tutta però, visibile in diretta oppure dall’inizio se si arriva in ritardo a match tuttora in corso. Se capisco bene, è impossibile rivedere una partita già giocata.

Non che l’agenda fosse chissà quanto piena, ma abolisco da ora qualsiasi impegno sociale per il sabato mattina presto, almeno fino che dura la stagione.