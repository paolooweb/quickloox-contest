---
title: "Un posto migliore"
date: 2016-01-14
comments: true
tags: [iOS, iPhone, Windows, Apple, Asymco, Dediu]
---
Brevissimo, perché il momento è solenne e bisogna celebrare.

<blockquote class="twitter-tweet" lang="en"><p lang="en" dir="ltr">iOS overtook Windows last year, as expected. <a href="https://t.co/5LgnZsxWaL">pic.twitter.com/5LgnZsxWaL</a></p>&mdash; Horace Dediu (@asymco) <a href="https://twitter.com/asymco/status/687048149358231552">January 12, 2016</a></blockquote> <script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script><!--more-->

Nel 2015 si sono venduti più apparecchi iOS che apparecchi Windows. Non Apple, iOS. Mac rafforza il vantaggio ma non è determinante. Mentre quelli Windows sono tutti, dal supercomputer al telefonino.

Il mondo è un posto migliore e c’è più libertà.

Se poi consideriamo che questo grafico, messo a confronto con Android, farebbe la parte del nanetto a ridosso di un gigante, ecco che Windows viene finalmente messo al suo posto. Ci si può solo augurare che la tendenza continui.