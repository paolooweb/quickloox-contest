---
title: "Non tutti fanno gli scappati di casa"
date: 2022-12-11T01:27:37+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [Apple, iCloud, iMessage Contact Key Verification, Security Keys for Apple ID, Advanced Data Protection for iCloud]
---
Nel 2023 Apple [proteggerà in modo importante](https://www.apple.com/newsroom/2022/12/apple-advances-user-security-with-powerful-new-data-protections/) gli account e l’attività di comunicazione iCloud di persone ad alto rischio di violazione della propria privacy da parte di organizzazioni illiberali, consentirà l’uso di chiavi hardware per validare l’accesso a iCloud e espanderà radicalmente le categoria di dati su iCloud che verranno cifrate end-to-end.

Nel 2021, Apple annunciava un sistema per verificare in modo sicuro e rispettoso della privacy la presenza su iPhone di foto registrate in un database alimentato dalle organizzazioni per la lotta alla diffusione di materiale pedopornografico online e la cosa passava per un attacco alla privacy degno del peggiore capitalismo della sorveglianza. Edward Snowden [scriveva](https://macintelligence.org/posts/2021-08-28-privacy-e-realtà-un-attacco-omologato-e-superficiale/):

>Quanto tempo rimane prima che l’iPhone nella tua tasca inizi silenziosamente a compilare rapporti sul possesso di materiale politico “estremista” o sulla tua presenza in una manifestazione non autorizzata? O sulla presenza nel tuo iPhone di un video che contiene, o forse-contiene-forse-no, l’immagine sfocata di un passante che secondo un algoritmo somiglia a una “persona di interesse”?

Snowden oggi [ha preso il passaporto russo](https://www.theguardian.com/us-news/2022/dec/02/edward-snowden-gets-russian-passport-after-swearing-oath-of-allegiance). Con tutte le attenuanti concedibili a una persona perseguitata politicamente che rischia pene pesantissime nel suo Paese di origine, si capisce che le energie con cui ha lottato contro quest’ultimo non sono le stesse con le quali abbraccia, forse controvoglia o sotto pressione, il Paese dove ha trovato rifugio. Un Paese che in questi giorni deporta decine di migliaia di bambini e, se potesse, adotterebbe dal primo minuto misure di sorveglianza assai più drastiche di quelle paventate per iPhone.

Nel contempo, Apple – accusata da Snowden e altri di fare gli interessi dei governi favorevoli alla sorveglianza digitale – rafforza le misure di protezione dei dati personali, in modo che – ma guarda – preoccupa i servizi di polizia americani.

Se Apple fosse legata al volere dei governi, che interesse avrebbe a cifrare con forza i dati dei propri utenti?

Certo dobbiamo vedere che cosa succederà in Cina, dove per legge i server iCloud sono in carico a entità che li amministrano per conto della dittatura; intanto, però, nel mondo libero le persone saranno ancora più libere e tutti vorremmo vedere sorgere una democrazia liberale in Cina, ma la cosa è ancora fuori dal regno del possibile e non si realizza con delle modifiche a iCloud.

Le quali, salvassero anche solo un giornalista scomodo o qualche attivista, avrebbero comunque raggiunto un traguardo impensabile fino a pochi giorni fa. Non tutti sono scappati all’estero e hanno cambiato faccia a centoottanta gradi, per rischiare invece incolumità e pelle ogni giorno.