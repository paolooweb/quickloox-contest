---
title: "Rimando a questioni urgenti"
date: 2014-07-10
comments: true
tags: [Apogeonline]
---
È nota la mia scarsa attitudine all’autopromozione. Qui però si tratta di idee e di questioni contemporaneamente epocali eppure capaci di influenzare pesantemente il nostro quotidiano (non nel senso del giornale del mattino).

Per questo mi permetto eccezionalmente di rinviare alla lettura del mio [articolo di oggi su Apogeonline](http://www.apogeonline.com/webzine/2014/07/10/web-2-0-lassassino). Per i lettori notturni: il *link* inizierà a funzionare appena prima delle sette del mattino.

Per una volta considero le mie riflessioni con meno modestia del solito. Spero verrò perdonato.