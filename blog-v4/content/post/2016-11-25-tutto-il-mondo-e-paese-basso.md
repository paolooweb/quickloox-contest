---
title: "Tutto il mondo è Paese Basso"
date: 2016-11-26
comments: true
tags: [Mac, Olanda, Amsterdam]
---
Amsterdam, sala convegni del [Conservatorium](http://www.conservatoriumhotel.com).

Un tempo sede di banca specializzata in prestiti ai colonizzatori che partivano per mare.

Convertito pochi decenni or sono in conservatorio e, in questo secolo, ristrutturato – da architetti italiani – come albergo di lusso e centro convegni.

Si respirano business e professione a ogni piano e, specie nella sala convegni principale, non si sentono abbandonati da Apple.

 ![Due Mac per la regia del convegno](/images/conservatorium.jpg  "Due Mac per la regia del convegno") 

La sera prima siamo stati portati al [museo Van Gogh](http://vangoghmuseum.nl/it/prepara-la-tua-visita) e, per soprammercato, sotto una installazione artistica stava un Mac. Lo avranno lasciato lì dei dilettanti.

 ![Un Mac al museo Van Gogh](/images/van-gogh.jpg  "Un Mac al museo Van Gogh") 