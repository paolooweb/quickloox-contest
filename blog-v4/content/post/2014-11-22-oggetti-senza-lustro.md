---
title: "Oggetti senza lustro"
date: 2014-11-22
comments: true
tags: [iPad, Android, Lollipop, ArsTechnica]
---
Un po’ me lo aspetto, un po’ trasecolo quando leggo articoli come la [recensione di Nexus 10 di *Ars Technica*](http://arstechnica.com/gadgets/2014/11/the-nexus-10-lollipop-and-the-problem-with-big-android-tablets/) con la [nuova versione *leccalecca* di Android](http://www.android.com/versions/lollipop-5-0/).<!--more-->

Si leggono titoli di paragrafo come *widescreen sprecato*. Racconti di un’interfaccia nella quale l’uso *portrait* (aggeggio in verticale) è scoraggiato eppure le icone vengono raggruppate nella parte centrale dello schermo. Lontano dai lati, lontano dagli angoli, lontano dai pollici.

Vedo espressioni come *blown-up phone*, telefono espanso: su un apparecchio da dieci pollici di schermo l’interfaccia tende a essere quella usata sugli schermi da cinque pollici (non inganni l’aritmetica apparente: sono schermi molto, molto più piccoli). Si mostrano schermate dove lo spazio bianco buttato via e inutile giganteggia. Nel capoverso finale spicca questo commento:

>È che ci sono poche ragioni per usare una tavoletta Android con uno schermo così grande, e se Google non ha voglia di fare funzionare al meglio il suo sistema operativo e le sue applicazioni riprogettate, perché dovrebbe importare agli altri sviluppatori?

Stanno per compiersi *cinque anni* dal primo annuncio di iPad. Un lustro e ancora, sull’altra faccia della Luna, non si curano di realizzare software veramente concepito per l’hardware su cui deve girare. Le motivazioni per l’acquisto di un apparecchio Android – a parte la solita – invece che chiarirsi man mano, mi sono sempre più oscure.