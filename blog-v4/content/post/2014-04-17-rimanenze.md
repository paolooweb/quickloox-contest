---
title: "Rimanenze"
date: 2014-04-17
comments: true
tags: [Roll20, Android, iPad, Cd, iTunes, Producer]
---
Cosette non degne di un *post* a sé e che nel grande conteggio meritano comunque un valore superiore a zero, messe insieme come vengono.<!--more-->

Cd musicali vecchiotti – attorno al decennio – conservati con cura e ripresi oggi per allargare la mia libreria iTunes. Circa un disco ogni venti dà un problema di una traccia che non si legge, mancato riconoscimento all’inserzione e problemucci vari. Non c’è niente da fare; il dato digitale vuole il *backup* perché nessun supporto è degno di fiducia assoluta.

iTunes Producer versione 3 è del tutto inusabile sul mio Mac. Al primo *drag and drop* va in tilt. Sui forum Apple il problema è diffuso e sentito, ma nessuno ha ancora trovato il rimedio giusto per me. Per ora ho conservato la vecchia versione 2.9.1 che lavora a perfezione. Di bug se ne trovano sempre, ma l’impossibilità totale di lavorare non mi era ancora capitata.

Il mio gruppo di gioco di ruolo si è spostato su [Roll20](http://roll20.net) per le mappe e i combattimenti tattici e quasi tutti lo usano su iPad. Abbiamo anche un giocatore con Android. Se la mappa è piuttosto grande, a volte c’è un po’ di difficoltà nel posizionarla esattamente perché lo zoom di Roll20 è una delle funzioni da migliorare. Si percepisce subito la differenza tra il formato 4:3 dello schermo di iPad e il 16:9 di Android, che a parità di diagonale fornisce molta meno area utile sullo schermo.