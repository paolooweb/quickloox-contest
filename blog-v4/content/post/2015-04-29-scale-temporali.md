---
title: "Scale temporali"
date: 2015-04-29
comments: true
tags: [iPad, iPhone6, iPhone6Plus, MacBook]
---
Le vendite di iPad [sono in declino anno su anno](http://qz.com/392202/were-live-charting-apples-second-quarter-2015-earnings/), ma bisogna considerare che iPad ha venduto, dal primo modello a oggi, più unità di iPhone (incredibile, eh? Ma vero).<!--more-->

La crescita del venduto globale è minore di quella di iPhone, ma costante. E poi, prima che si cambi un iPad passa del tempo. Il mio terza generazione non perde un colpo e ha appena compiuto tre anni; non ho la minima intenzione di cambiarlo. Quando non lo [pasticcia la figlia](https://macintelligence.org/posts/2015-04-21-multimediale-innocenza/), mi capita di usare ancora il primo iPad uscito, del 2010, cinque anni e una batteria che sembra prodotta ieri.

Per non parlare della concorrenza interna, dal basso – [iPhone 6 e e 6 Plus](http://www.apple.com/it/iphone-6/) – e dall’alto ([MacBook](http://www.apple.com/it/macbook/)).

Ho insomma l’impressione che chi parla di iPad in declino abbia in mente una scala temporale che non è quella giusta. Se poi salta ancora fuori la storia che non è un computer, beh.

<blockquote class="twitter-tweet" lang="en"><p>Also, excited to try my iPad-only liveblogging system. It handled graph generation and real-time post updates the last time (all Markdown)..</p>&mdash; Federico Viticci (@viticci) <a href="https://twitter.com/viticci/status/592680847192514560">April 27, 2015</a></blockquote> <script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>