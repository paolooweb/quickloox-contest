---
title: "Un altro argomento della diagonale"
date: 2014-11-24
comments: true
tags: [Cantor, iPadmini, Android, diagonale]
---
È la seconda volta che parlo di argomento della diagonale e mi approprio a sproposito di [alta matematica](http://www.lidimatematici.it/blog/2010/12/30/un-infinito-piu-grande-i-reali-largomento-diagonale-di-cantor-parte-3/).<!--more-->

Questa volta perché mi sono reso conto che, se si parla di confronti tra apparecchi iOS e apparecchi Android, anche menti finissime e altrimenti brillanti possono partire dal presupposto che, a parità di diagonale dello schermo, gli schermi siano equivalenti.

**È falso.**

A oggi le tavolette iOS hanno uno schermo con rapporto tra base e altezza 4:3. Tutte o quasi tutte le tavolette Android hanno rapporto 16:9. Così messe le cose, **a parità di diagonale, una tavoletta iOS dispone di maggiore area utile.**

Il punto è dirimente perché la risoluzione in pixel dello schermo è sempre meno rilevante e, per schermi che si toccano, è invece rilevante la superficie di lavoro utile, essendo la dimensione del polpastrello invariante.

La diagonale dello schermo diventa così argomento di vendita bugiardo: un Android da 7,9 pollici non è sovrapponibile nello schermo a iPad mini da 7,9 pollici. Lo schermo Android è più piccolo (di circa il 12 percento in questo caso).

La moneta cattiva cercava di scacciare la buona già anni fa, al momento del primo *boom* dei personal computer. Apple dichiarava la diagonale dello schermo; gli altri costruttori, la diagonale della cornice contenente lo schermo. I monitor Apple erano uguali agli altri per superficie di lavoro utile, solo che a leggere le specifiche come si fa con lo scontrino del centro commerciale apparivano più piccoli. La moneta cattiva scacciò la buona; Apple si adeguò e tutti pensavano di portarsi a casa schermi più grandi. Per non parlare di quando si spacciavano i megahertz come unità di misura della velocità.

Per evitare di ricadere in una simile spirale, dovremmo abituarci a considerare gli apparecchi per superficie e non per diagonale (o smettere di leggere le specifiche come gli scontrini). Disgraziatamente non è immediato reperire il dato. Proverò nei prossimi giorni ad acquisire informazioni e se qualcuno riesce ad aiutarmi, è benvenuto.

Per inciso, anche il precedente [argomento della diagonale](https://macintelligence.org/posts/2012-11-05-argomento-diagonale/) aveva Android di mezzo e falsava un confronto.