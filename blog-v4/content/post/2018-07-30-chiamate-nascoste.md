---
title: "Chiamate nascoste"
date: 2018-07-30
comments: true
tags: [Grove, Jobs, NeXT, Be, Gassée]
---
È comprensibile che diventi progressivamente più raro trovare buona aneddotica sulla grande storia di Steve Jobs e prodotti correlati.

Ogni tanto salta comunque fuori qualcosa come questa bella [storia di telefonate](https://www.cake.co/conversations/g4CP6zJ/the-secret-call-to-andy-grove-that-may-have-helped-apple-buy-next). A quanto pare, il primo contatto tra Apple e Intel sull’adozione dei processori di quest’ultima avvenne nascostamente da Steve, esattamente come ai tempi del primo Macintosh il team di lavoro aveva nascosto un ingegnere Sony nel loro palazzo, così che Jobs non lo trovasse e cacciasse via per segretezza. Il lettore di floppy nel primo Macintosh era una novità ed era Sony; la cosa giusta era avere un aiuto.

Il bello è che, in un ironico capovolgimento della storia, potremmo essere vicini a un bel ridimensionamento della presenza di processori Intel dentro i Mac. Chissà che telefonate sono intercorse a questo riguardo.