---
title: "Partiamo dall’inizio"
date: 2023-10-17T17:48:16+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [machine learning, artificial intelligence, intelligenza artificiale, ai, Columbia, Pohlink, Claudia Pohlink, Reyes, Alfonso Reyes, Jordan, Michael Jordan]
---
Prego di leggere e diffondere questo semplice testo dell’università della Columbia che spiega in pochi e piani paragrafi la [differenza tra machine learning e intelligenza artificiale](https://ai.engineering.columbia.edu/ai-vs-machine-learning/). Ce ne sarà sempre più bisogno e soprattutto nessuna voce è stata finora ascoltata, neppure quando autorevole come quella di µichael I. Jordan che già nel 2021 avvisava, senza esito, di [smettere di chiamare intelligenza artificiale qualsiasi cosa](https://spectrum.ieee.org/stop-calling-everything-ai-machinelearning-pioneer-says).

Su Medium [lo dicevano addirittura nel 2020](https://oilgains.medium.com/why-machine-learning-is-not-artificial-intelligence-61b174a3c9a2), a opera di Alfonso R. Reyes, mentre Claudia Pohlink [ne parlava anche l’anno prima](https://www.linkedin.com/pulse/what-artificial-intelligence-without-machine-learning-claudia-pohlink/), pre-pandemico.

Scegliamo pure quello che preferiamo. E poi spieghiamo con calma e pazienza a vigili, ostetriche, cuochi, mamme, speaker radiofonici, restauratrici, pattinatori come stanno le cose, prima che sia troppo tardi.