---
title: "Fin da piccoli"
date: 2017-08-19
comments: true
tags: [Windows, crash]
---
Una volta mi è capitato di leggere una cosa tipo *è bene che i bambini usino da subito Windows perché così poi lo ritroveranno con familiarità sul lavoro quando saranno più grandi*.

Vista la foto che gentilissimamente mi inoltra **Andy**, aggiungerei: *inoltre imparano subito come funziona Windows*.

E come non funziona. Come bonus. O malus, secondo i punti di vista.

 ![Chiosco-gioco in Windows malfunzionante](/images/andy-windows.jpg  "Chiosco-gioco in Windows malfunzionante") 