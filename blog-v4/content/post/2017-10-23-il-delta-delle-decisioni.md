---
title: "Il Delta delle decisioni"
date: 2017-10-23
comments: true
tags: [Delta, iPhone, Windows, New, York]
---
Eravamo rimasti ai [trentaseimila computer da tasca](https://macintelligence.org/posts/2017-09-03-ricorsione-a-new-york/) Windows Phone diventati insostenibili per la polizia di New York e sostituiti da iPhone.

Adesso sappiamo che la compagnia aerea Delta [cestinerà ventitremila Nokia e quattordicimila Surface](http://macosken.squarespace.com/macosken/2017/10/20/ithings-prepare-to-keep-climbing) a favore di altrettanti iPhone e iPad Pro.

Mentre a New York si è trovato subito il capro espiatorio nella persona del responsabile informatico Jessica Tisch, a livello di Delta non si sa chi abbia preso nel 2014 la luminosa decisione di adottare hardware da buttare tre anni dopo a decine di migliaia.