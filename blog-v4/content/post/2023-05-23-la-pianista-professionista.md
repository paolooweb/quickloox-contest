---
title: "La pianista professionista"
date: 2023-05-23T01:57:55+01:00
draft: false
toc: false
comments: true
categories: [Hardware]
tags: [Mac, Fiamma Velo, Velo]
---
Il setup musicale della pianista per il concerto privato.

![Microtastiere su macipianoforte](/images/velo.jpg "Sintetizzatori, effetti speciali e suoni particolari.")

Il setup informatico della pianista per il concerto privato.

![Il Mac della pianista](/images/mac-velo.jpg "Anche nella situazione più acustica, non può mancare un assistente digitale.")

(Che software potrebbe essere?)