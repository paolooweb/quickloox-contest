---
title: "Chiusura e sicurezza"
date: 2022-04-26T00:09:49+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [OpenBSD, OpenBSD 7.1]
---
Apple ha operato una scelta talmente altera, chiusa e fuori dalla comunità, con la sua ultima architettura di elaborazione, che OpenBSD 7.1 annuncia come novità principale il [supporto migliorato e pronto per l’uso ordinario dei Mac con Apple Silicon](https://www.openbsd.org/71.html).

OpenBSD 7.1 è certamente un sistema operativo di nicchia, il che non va scambiato per qualcosa di meno sul piano della qualità o della opportunità di utilizzo. È un sistema il cui focus principale è da sempre la sicurezza, per cui il supporto di Apple Silicon va ben oltre avere il software che ci gira senza crash.

*The Register* nota che OpenBSD 7.1 [è compatibile con una vasta gamma di processori](https://www.theregister.com/2022/04/22/openbsd_71_released_including_apple/), tra i quali PowerPC.

> Ne abbiamo fatta girare una copia in VirtualBox e siamo onestamente rimasti sorpresi da quanto sia stato veloce e semplice. Dicendo “sì” a tutto, ha partizionato automaticamente la macchina virtuale in uno schema piuttosto complesso di nove sezioni, installato il sistema operativo, un boot loader, un X server e display manager, più il window manager. Dopo un reboot, ci è apparso uno schermo di login in grafica e, dopo, un desktop stile Motif un po’ anni ottanta.

> Installare XFCE è stato facile, il che ci ha permesso di impostare la risoluzione dello schermo e installare come come Kde, Gnome e strumenti familiari come le app di Mozilla, LibreOffice e così via.

Insomma, non esiste solo Linux e il mondo del software libero è più articolato e più intrigante di quanto si possa sospettare, mantenendo al tempo una accessibilità da parte di persone legittimamente vogliose di smanettare e imparare e altrettanto legittimamente prive di un master al Mit per farlo.

Anche su un Mac Apple Silicon, che Apple cattiva tiene chiuso e segregato dal mondo esterno. Che evidentemente include come insieme solo sviluppatori meno capaci di quelli di OpenBSD.