---
title: "Romanzetto criminale"
date: 2018-02-08
comments: true
tags: [Tisch, Nokia, Windows, iPhone, Nypd]
---
Lieto fine per la storiaccia dei [trentaseimila Lumia in dotazione alla polizia di New York](https://macintelligence.org/posts/2017-09-03-ricorsione-a-new-york/), acquistati a condizioni surreali per disfarsene dopo due anni.

È iniziata la distribuzione di iPhone 7 e iPhone 7 Plus – secondo preferenza – a tutti gli agenti, al ritmo di seicento unità al giorno.

Tra due anni potranno eventualmente essere sostituiti con iPhone 8, o con altro. Gli altri, con niente. I cimeli Nokia verranno cancellati in modo sicuro e rivenduti a Microsoft. La quale, evidentemente, sapeva che l’accordo non aveva futuro e lo ha firmata preparatissima a guadagnarci il meno possibile; l’importante era dare la massima diffusione a terminali già più aggettivi che sostantivi.

I poliziotti sono gente pragmatica; speriamo abbiano imparato la lezione. Anche un contratto perfettamente legale può essere (simbolicamente) un crimine.