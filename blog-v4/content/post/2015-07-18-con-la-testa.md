---
title: "Con la testa"
date: 2015-07-19
comments: true
tags: [iOS, Panic, Coda, SSH, OSX, Mac]
---
Su iOS credo che stiamo arrivando al punto in cui vale la pena di investire su una *app* quando, se ti metti a usarla, finisci per abbandonarne almeno due.<!--more-->

Con [Coda for iOS](https://itunes.apple.com/it/app/coda-for-ios-formerly-diet/id500906297?l=en&mt=8) (già Diet Coda) potrebbe essere che di *app* se ne buttano anche tre o quattro.

Editor di testo orientato ai programmatori, connessione SSH, organizzazione di siti o progetti, ftp e ogni altro genere di connessione, file manager, tutto con la qualità di Panic. È un aggiornamento 2.0 che come rapporto qualità/prezzo si trova molto, molto di rado.

A 9,99 euro, gratis per chi lo aveva già comprato. Viene da fare un pensierino a [Coda per Mac](https://panic.com/coda/), che pure costa dieci volte tanto.