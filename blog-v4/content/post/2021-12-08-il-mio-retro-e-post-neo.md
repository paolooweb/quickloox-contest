---
title: "Il mio retro è post-neo"
date: 2021-12-08T00:56:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [DevTerm] 
---
Non è tanto un discorso sul retrocomputing, ammirevole, sentimentalmente energico, bisognoso di passione e competenza che neanche una pianta carnivora in vaso.

Quanto su quella nostalgia dei vecchietti (mentali) per cui una volta i computer erano cose affascinanti, si poteva smanettare, potevi guardarci dentro, imparare, programmare da zero, cambiare un pezzo se non ti piaceva e tutto questo senso di avventura malriposta che sa molto del profumo della carta quando si parla di ebook.

Perché a sentirli, signora mia, adesso i computer sono chiusi, non ci puoi mettere mano e puoi solo collegarti ai social con la app.

Questo per preparare la mia affermazione: potrei compiere azioni anche sconsiderate e disdicevoli pur di mettere le mani su un [DevTerm](https://www.clockworkpi.com/devterm). E quelli che una volta il computer sì che era emozionante e ora non più, li deporterei volentieri per un anno davanti al sito, a guardare che cosa c’è dentro la macchina, tutto open source, tutto visibile, modificabile, documentato, apribile senza difficoltà.

Con obbligo di silenzio. Perché se vuoi smanettare, oggi come nel millenovecentosettantanove, è veramente difficile concepire un sistema capace di mettere più brividi di eccitazione lungo una schiena *nerd*.

Post-neo-computing, questo, che sta al retrocomputing come l’assenzio al fernet.

_Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile._