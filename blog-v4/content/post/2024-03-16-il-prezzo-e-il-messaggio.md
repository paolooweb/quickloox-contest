---
title: "Il prezzo è il messaggio"
date: 2024-03-16T02:37:36+01:00
draft: false
toc: false
comments: true
categories: [Hardware]
tags: [Best Buy, Walmart, MacBook Air, M1]
---
Best Buy, una catena statunitense di elettronica di consumo vagamente paragonabile a una Unieuro orientata all’economico, [vendeva MacBook Air M1 a settecentocinquanta dollari](https://www.macrumors.com/2024/03/15/best-buy-macbook-air-all-time-low/), un minimo storico. Chiaramente fondi di magazzino, invenduti, rimanenze.

Poi ha abbassato il prezzo a seicentocinquanta dollari. Perché nel frattempo è successa una cosa.

Apple ha stretto un accordo con Walmart. Walmart è una istituzione, una, anzi, *la* catena americana del basso costo. Una specie di Lidl, ma più grande magazzino che alimentare, completamente orientata alla convenienza. Walmart è ovunque negli USA e serve la fascia bassa dei consumatori, quelli con il reddito più basso. È una parte del tessuto sociale. Walmart non aveva mai stretto un accordo con Apple, né viceversa.

L’accordo prevede la vendita nei magazzini Walmart di MacBook Air M1. E Apple *li produce espressamente per Walmart*. No rimanenze, no invenduti: eccezione alle regole. Se c’è una cosa che Apple sa fare molto, bene, è contenere al minimo l’inventario. Non puoi rifornire Walmart con i fondi di magazzino.

Walmart non vende, né venderà almeno con questo accordo, alcun altro prodotto Apple. Solo MacBook Air M1.

C’è tutta una lezione di marketing, di *pricing* in questa decisione. Ogni tanto salta fuori il genio per cui ad Apple basterebbe fare i Mac a basso costo, così ne venderebbe di più e aumenterebbe la quota di mercato. Tutto semplice, tutto automatico, seguito dalla fame nel mondo e dalla fine delle guerre, basta una idea semplice, che ci vuole?

Invece.

L’accordo con Walmart non significa niente dal punto di vista economico. Potrebbero venderne tanti, ma non tantissimi.

Invece conta molto come raggiungimento di una fascia di persone che di fatto non conosce Apple in modo concreto, con una offerta molto conveniente e efficace: John Gruber scrive *scommetterei denaro serio che un MacBook Air M1 base supera le prestazioni di qualsiasi altro laptop da settecento dollari*.

La narrazione popolare e scontata di Apple potrebbe iniziare a cambiare. E questo varrebbe molto più di tutte le vendite.