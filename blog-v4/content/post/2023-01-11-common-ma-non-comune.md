---
title: "Common, ma non comune"
date: 2023-01-11T01:26:59+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [Kandria, Shimmera]
---
Non grazie al mio supporto specifico – una goccia in un mare di sostenitori – Kandria sarà in vendita nel giro di tredici ore.

[Come sottolineato ai tempi del lancio del finanziamento su Kickstarter](https://macintelligence.org/posts/2022-06-16-il-common-lo-capiscono-tutti/), Kandria è stato scritto principalmente in [Common Lisp](https://lisp-lang.org).

Che certamente non è un linguaggio di moda. Nel contempo, chi lo avvicinasse potrebbe scoprire che sa invecchiare piuttosto bene e che un programmatore dotato, esperto di Common Lisp, non faticherebbe moltissimo a trovare buone offerte di lavoro.

Cosa che vale oggi praticamente per qualsiasi linguaggio di programmazione un po’ diffuso. Ma c’è anche la questione della concorrenza; Java, JavaScript, PHP, Python lo conoscono in milioni. Common Lisp un po’ meno e ha diversi vantaggi interessanti, per chi abbia il bagaglio sufficiente a sfruttarli come si deve.

Per sapere qualcosa più di Kandria come gioco, linguaggi a parte, ecco [la sua pagina](https://shinmera.itch.io/kandria).