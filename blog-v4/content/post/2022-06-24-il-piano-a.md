---
title: "Il piano A"
date: 2022-06-24T02:07:45+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [Pages, Sal Soghoian, Soghoian, Mail Merge, Stampa Unione, AppleScript, Six Colors, Dan Moren, Moren]
---
Mai stato un entusiasta del *mail merge* e se non mi [avesse avvisato Six Colors](https://sixcolors.com/post/2022/06/mail-merge-returns-to-pages-after-nine-years/) non mi sarei proprio accorto del suo ritorno in Pages.

(A me scrivere lettere uguali a persone diverse sembra anacronistico in tutta una serie di scenari, non tutti, certo).

Il fatto interessante è che torna dopo nove anni di assenza. Nove anni durante i quali chi ha voluto fare *mail merge* con Pages ha potuto farlo ugualmente, grazie al [lavoro di Sal Soghoian](https://iworkautomation.com/pages/script-tags-data-merge.html), il [patrono Apple dell’automazione](https://macosxautomation.com/about.html).

Automazione, con la A maiuscola. Un buon sistema operativo porta opportunità di automatizzare e, anche se poi prende direzioni sbagliate, l’automazione può aiutare a superarle o circoscriverne gli effetti.

Un buon utente acquisisce consapevolezza delle sue possibilità di automazione come una garanzia fornita dal sistema operativo, di non rimanere mai a corto di strumenti; quando ne mancasse uno, se è possibile crearlo, il problema è relativo.

Per utenti e software house, il piano A risulta sempre vincente, quali che siano le condizioni.