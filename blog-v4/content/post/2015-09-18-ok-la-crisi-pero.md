---
title: "Ok la crisi. Però"
date: 2015-09-18
comments: true
tags: [Arment, podcast, Overcast, editoria, crisi, Amazon, Xlr]
---
La crisi dell’editoria perché la gggente va a leggere su Internet gratis e ne esce convinta di essersi informata.<!--more-->

Se si trattasse di Facebook o dei sitacci sette-segreti-per-decuplicare-la-velocità-del-tuo-Mac, non potrebbe essere più falso. Invece, c’è dell’altro.

Guardare Marco Arment e la sua [disamina dei migliori microfoni per *podcasting*](http://www.marco.org/podcasting-microphones). Benissimo organizzata, chiara da leggere, con le lunghezze giuste, non manca un link e ci sono i *campioni audio*. Non guasta che l’autore dia l’idea di conoscere l’argomento.

C’è spazio anche per citare i microfoni non ancora provati e le migliori recensioni scritte da altri. E poi i cavi, le interfacce Xlr, le cuffie, il dilemma tra condensatori e dinamici.

Se domani fossi in cerca dell’attrezzatura precisa per fare *podcast*, partirei senza dubbio da questo articolo. I *link* dei prodotti portano ad Amazon con un *referral* a nome di Arment, ovvero: se compriamo un prodotto arrivando dal link nell’articolo, lui ci guadagnerà qualche centesimo. Il lettore non ci rimette niente e non rischia nulla, se vuole passare da lì è una sua scelta. Onesto. In cima al sito c’è una inserzione pubblicitaria, di sobrietà non comune, perfettamente integrata nella grafica, a invasività zero rispetto alla lettura.

Dall’articolo è assente qualunque menzione di [Overcast](https://overcast.fm), la *app* per ascoltare i *podcast* realizzata da Arment e scaricabile gratis, ma con un acquisto *in-app* da 4,99 euro.

La crisi dell’editoria c’è e ha anche ragioni molto fondate. Però io, di articoli così, me ne ricordo veramente pochi. E non parliamo dell’etica.