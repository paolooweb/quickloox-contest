---
title: "Un undici quasi perfetto"
date: 2021-07-27T02:17:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [Apple, Mac, business, Excel, M1, Salesforce, Intel] 
---
Apple ha pubblicato un elenco di [undici buone ragioni per l’adozione di Mac da parte delle aziende](https://www.apple.com/business/mac/).

Non dovrebbe neanche essere necessario e per questo non cambierà più di tanto le cose. Spiegare a un fanatico ottuso che un Mac ti fa risparmiare ottocentoquarantatré dollari rispetto a un Pc, oppure che un’aziendina come Salesforce adotta Mac su larga scala, funziona poco come ogni argomento razionale. Se poi il fanatico ottuso decide gli acquisti della società, non c’è neanche da pensarci.

Tuttavia il lavoro di questi ultimi anni centra il bersaglio da più angolazioni ed è giusto valorizzarlo. Oggi un Mac M1 con le app giuste è una macchina di potenzialità notevoli.

Solo una cosa non mi piace: far notare che Excel su un Mac M1 va veloce il doppio che su Intel.

Da utopista sognatore mi piacerebbe ritrovarmi con una combinazione di hardware e software tale da poter promettere velocità 2x grazie a M1 e 4x grazie a qualcosa che prenda il posto di Excel e lavori meglio. Un M1 software se Excel fosse un Intel, insomma.

Dopo di che, sarebbe perfetto.

*Mentre approfondisco la faccenda dei commenti per il blog, chi vuole lasciare comunque un commento da qui può accedere liberamente alla [pagina commenti di Muut per QuickLoox](https://muut.com/quickloox#!/feed). Non è ancora (ri)collegata a questi post (è lo scopo di tutto l’esercizio). Però lo sarà.