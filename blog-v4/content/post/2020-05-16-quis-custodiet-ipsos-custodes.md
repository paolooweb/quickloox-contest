---
title: "Quis custodiet ipsos custodes?"
date: 2020-05-16
comments: true
tags: [Pad, Pro, Quill, iPad, Magic, Keyboard]
---
Un indizio incoraggiante sulla [Magic Keyboard per iPad Pro](https://www.apple.com/it/shop/product/MXQU2T/A/magic-keyboard-per-ipad-pro-129-quarta-generazione-italiano): Pad & Quill ha creato [Copertina](https://www.padandquill.com/copertina-magic-case-for-ipad-pro-11.html), una custodia per iPad Pro con Magic Keyboard.

Praticamente una custodia per la custodia di iPad.

Per quale altro prodotto potrebbe mai accadere una cosa del genere?

(Sperando che a nessuno venga in mente di vendere una protezione per Copertina a protezione di Magic Keyboard a protezione di iPad Pro, [che al mercato mio padre comprò](https://www.youtube.com/watch?v=Rm_aUj4B4dU)).

E [l’interrogativo retorico di Giovenale](https://en.wikipedia.org/wiki/Quis_custodiet_ipsos_custodes%3F) trovò risposta, duemila anni dopo.