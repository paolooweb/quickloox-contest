---
title: "Pensiero computazionale"
date: 2020-11-05
comments: true
tags: [Swift]
---
Non sono parenti. Non sono amici, non sono clienti né fornitori, non li vedo di frequente, non ci dobbiamo soldi, non abbiamo interessi comuni, non facciamo parte di una stessa associazione, non abitiamo vicino, non siamo coetanei né ex compagni di scuola o altro, neanche veniamo dalla stessa area geografica.

Sono semplicemente persone che conosco. Hanno un piccolo ristorante, che ho visto morire in un video su Facebook. Di lockdown, incertezze, chiusure, riaperture a metà, normative contrastanti, tasse assurde, regolamenti a caso e altro. Non so se ci abbiano messo del loro; a me interessa la parte che riguarda il governo e il pensiero computazionale.

Perché l’unica cosa che so è che loro erano a posto; avevano adeguato il ricambio d’aria, il distanziamento tra i tavoli, l’igienizzazione, tutto. Dopo lavori e spese importanti, erano un posto a prova di virus. Certamente andare al ristorante non rientra nelle mie priorità maggiori attuali, ma ho visto posti perfettamente in regola – soprattutto, *effettivamente* in regola, efficaci oltre la forma dei regolamenti – e posti dove finora si è fatto solo finta.

Da tempo ci si batte perché nella scuola entri il coding, non di per sé ma come portatore di pensiero computazionale; quello logico, conseguente, dove A non può valere contemporaneamente un numero x e un numero y, ma può valere prima uno e poi l’altro, comunque in forma esplicita e mai per accidente del destino.

Se avessimo un governo (e vale giù giù giù fino agli assessori comunali e alla polizia di quartiere), eseguiremmo controlli sui ristoranti, il caso in esame. Controlli severissimi, al termine dei quali il locale dovrebbe poter operare con tutti i crismi oppure chiudere senza appello dopo una multa epocale. Fare di tutta l’erba un fascio poteva funzionare a marzo, ma abbiamo avuto sette mesi per affrontare meglio e più preparati l’emergenza, vero?

In [Swift](https://swift.org) scriverei una cosa tipo

`var conforme: Bool`<br>
`if conforme = true {`<br>
`	print("Stai aperto")`<br>
`	} else {`<br>
`	print("Stai chiuso")`<br>
`	}`

Per capire se un locale è *conforme* alla normativa, andrebbe esaminato alla luce del rispetto dei vari parametri di sicurezza. In Swift si possono descrivere questi ultimi con coppie *key-value*:

`"ricambioAria": 500 // misura ipotetica di litri d'aria al minuto`<br>
`"distanzaTavoli": 2 // metri`<br>
`"capienzaMassima": 0.25 // avventori per metro quadrato`

Eccetera. Estendere il sistema con l’inserimento di procedure nelle quali si introduce un parametro rilevato e lo si confronta con le coppie key-value già viste è in sostanza una riapplicazione dell’istruzione `if`.

La parte complessa dell’operazione è la verifica effettiva dei parametri. Si potrebbe iniziare con un genericissimo *statement* da riempire con tante cose:

`func verificaParametri {} // graffe da riempire`

ma nel Paese non mancano solerti funzionari, burocrati, portaborse a cui basta dare un ditino per fargli generare pagine di documentazione, specifiche, norme operative, allegati ed errata. Potrebbero perfino semplificare con un modulo di autocertificazione declinato in tutte le duemila o tremila varianti necessarie per generare dati usa e getta fini a se stessi, come amano fare. Insomma, le procedure di verifica le fanno loro.

Finito il lavoro dei burocrati, un programmatore potrebbe trasformare il loro lavoro in codice e da qui giungere in poco tempo a una app snella e veloce da mettere in mano a finanzieri, vigili, nuclei antisofisticazione e via dicendo, armati di rotella metrica e anemometro da taschino per battere il territorio e verificare la conformità delle attività.

Il post è lungo. La sostanza no: il pensiero computazionale è lineare e sintetico. Per questo andrebbe insegnato nelle scuole. La poca stima di cui gode presso le autorità è effettivamente la ragione per cui tante scuole sono state chiuse.

Che prima o poi riapriranno. Al posto del ristorante e di chissà quante altre attività.