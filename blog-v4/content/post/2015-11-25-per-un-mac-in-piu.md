---
title: "Per un Mac in più"
date: 2015-11-25
comments: true
tags: [ED, Plus, Mac, MMN, Magnetic, Media, Network]
---
Reduce da una visita in [Magnetic Media Network](http://mmn.it), ho incontrato persone simpatiche e competenti. Ho anche visto un [Mac Plus ED](http://www.everymac.com/systems/apple/mac_classic/specs/mac_plus.html) di cui non sapevo l’esistenza e ce ne vuole. Onore al merito.<!--more-->

 ![Macintosh Plus ED](/images/plus-ed.jpg  "Macintosh Plus ED") 