---
title: "La sfida dell’estate"
date: 2017-06-29
comments: true
tags: [Angband, Morgoth]
---
Quasi in contemporanea con il solstizio, è uscito [Angband 4.1.0](http://rephial.org), con una montagna di aggiustamenti e pulizie a codice, interfaccia e dinamiche di gioco.

L’identità però resta la stessa: arrivare vivo abbastanza in fondo da uccidere Morgoth, magari trovando per strada l’Unico Anello. La morte è per sempre, un personaggio morto può solo ricominciare da zero. Morire per distrazione o arroganza è fin troppo facile e frustrante, bisogna essere prudenti e calcolatori. Nella sua versione originale, l’ambiente è reso con grafica Ascii (per chi vuole c’è la grafica).

C’è chi ci è riuscito mille volte, a sconfiggere Morgoth. Io [solo una](https://macintelligence.org/posts/2011-12-18-la-festa-dell-ascensione.html): mi ostino a giocare con un mago elfo-alto, fragilissimo fino a oltre metà percorso prima di iniziare a dispiegare una vera potenza. Tant’è vero che sono [arrivato in fondo con un irregolare](http://angband.oook.cz/ladder-show.php?id=12145), magia di base e poi, più pragmaticamente, arco, frecce e colpi bassi.

Chi accetta la sfida? Arrivare a settembre con lo scalpo di Morgoth e, come dicono in gergo gli appassionati, *ascendere*.