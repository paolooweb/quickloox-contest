---
title: "Appuntamento con il panico"
date: 2022-04-23T01:35:35+01:00
draft: false
toc: false
comments: true
categories: [Hardware, Internet]
tags: [Panic, Playdate, Ars Technica]
---
È un momento di compleanni ma anche di attualità. Panic Software [ha appena compiuto venticinque anni di vita](https://twitter.com/panic/status/1517622418906382337) che vanno festeggiati, perché sono bravi, seri e capaci di non prendersi sul serio.

<blockquote class="twitter-tweet"><p lang="en" dir="ltr">🎂 Panic is 25 years old today.<br><br>Well, if you go by the date Steve and I first registered a business called &quot;Panic LLC&quot;. A few years we re-registered as &quot;Panic Inc&quot;, and a couple years after that hired actual employees, so this is Birthday 1.0, really, but a birthday nonetheless.</p>&mdash; Panic (@panic) <a href="https://twitter.com/panic/status/1517622418906382337?ref_src=twsrc%5Etfw">April 22, 2022</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

E proprio mentre soffiano sulle candeline, iniziano dopo una fatica clamorosa a consegnare le prime [Playdate](https://play.date). (Il sito va assolutamente visitato).

Playdate è una microconsole da tasca, deliziosa, con lo schermo in bianco e nero e – tra gli altri dispositivi di input – una _manovella_. Panic ha inventato Playdate per il gusto di farlo e chi la compra riceve compresi nel prezzo due giochi a settimana per dodici settimane. Non è bellissimo spoilerare in questo caso, ma è inutile ignorare che sono stati [recensiti da Ars Technica](https://arstechnica.com/gaming/2022/04/all-24-of-playdates-included-games-reviewed/).

Chi può permettersi un Natale ambizioso può chiedere a babbo Natale una Playdate, sperando che da qui a dicembre mille difficoltà di produzione e distribuzione si siano appianate: sono centosettantanove dollari di cui neanche uno andrà sprecato.

Cento di questi venticinquennali, centomila di queste Playdate.