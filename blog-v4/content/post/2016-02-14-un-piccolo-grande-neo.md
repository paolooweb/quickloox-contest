---
title: "Un piccolo grande Neo"
date: 2016-02-14
comments: true
tags: [Neonecronomicon, telnet]
---
Mica lo sapevo. Ero convinto che il primo Mud italiano, e di svariate lunghezze il più bello e coinvolgente, avesse cessato di esistere, vista l’impossibilità di collegarsi secondo le istruzione.<!--more-->

Beh, quello che ha cessato di esistere, o meglio di essere aggiornato, sono le istruzioni. Un piccolo neo che ora deve tenere lontano il minor numero possibile di persone non informate. Si continua a entrare in *Neonecronomicon* di [Fabrizio Venerandi](http://www.quintadicopertina.com/fabriziovenerandi/) e Alessandro Uber con <a href="telnet://neonecronomicon.it:8010" title="Neonecronomicon">telnet neonecronomicon.it 8010</a>.

Stanotte si dorme un’ora meno e l’ora legale non c’entra.

Grazie a [Veggero Nylo](https://www.facebook.com/niccolo.veggero).
