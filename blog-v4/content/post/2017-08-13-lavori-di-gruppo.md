---
title: "Lavori di gruppo"
date: 2017-08-13
comments: true
tags: [macOS, Fëarandil, KickStarter, Bach, Kimiko, Ishizaka]
---
**Fëarandil** mi fa preziosamente notare un articolo molto discutibile, nel senso positivo del termine, di *Eclectic Light*: [Settimana scorsa sul mio Mac - c’è bisogno di documentare macOS](https://eclecticlight.co/2017/08/13/last-week-on-my-mac-we-need-to-document-macos/).<!--more-->

La tesi è chiara e indiscutibile: la documentazione di macOS lascia complessivamente a desiderare. Assolutamente vero. E in passato fu diverso:

>Ai tempi di Mac OS classico, quando l’editoria elettronica prosperava come risultato dei Mac, Apple pubblicava sotto la dicitura Inside Macintosh una serie esemplare di libri, alcuni dei quali stanno ancora impilati sotto la mia scrivania. E lo faceva nonostante la rapida evoluzione di Mac OS: System 7.0 in 1991, 8.0 sei anni dopo nel 1997 e 9.0 appena due anni più tardi nel 1999.

La soluzione mi lascia perplesso:

>Quello che serve più di ogni altra cosa agli utenti Mac è un progetto libero e collaborativo per documentare macOS.

In pratica: creiamo un posto dove collaborare e scrivercela da soli la documentazione, visto che Apple non lo fa, o meglio lo fa in modi confusi, imprecisi, parziali, poco aggiornati e sparsi.

Sulla carta è un bellissimo intento, però dubito che sia la cosa giusta. L’autore parla di *users* e però propone numerosi esempi, tutti centratissimi, di insufficienza della documentazione di Apple. Solo che riguardano fasce diversissime tra loro di competenza. Siamo tutti *utenti*, ma io voglio sapere tutto di AppleScript, un altro di *launchd*, un altro di Xcode, un altro ancora vuole semplicemente un aiuto contestuale dentro il Finder che funzioni e sia chiaro.

Per credere in un progetto del genere ho bisogno di sapere a che livello stiamo e come progettiamo i livelli di approfondimento. Come funziona Utility Disco? È piuttosto semplice scrivere un manuale completo che lo descriva in ogni sua funzione. Al tempo stesso, sotto il cofano di Utility Disco sta il comando Unix *diskutil*, che offre molte libertà in più ed è corrispondentemente più complesso. Scrivere un manuale di *diskutil* che sia più efficace della relativa [pagina man](https://developer.apple.com/legacy/library/documentation/Darwin/Reference/ManPages/man8/diskutil.8.html) è ben altra faccenda che scriverlo di Utility Disco; moltissime persone non hanno alcun interesse ad averlo, perché Utility Disco è sufficiente. Personalmente, uso sempre *diskutil* e mai Utility Disco, perché mi trovo meglio. La pagina man è dieci volte quello che mi serve e, se fosse per me, la riscriverei ridotta a un decimo. Qualcun altro invece la vorrebbe molto più lunga e complessa, perché capire al cento percento quello che fa Utility Disco implica capire il *filesystem*, cioè il neonato [Apple File System](https://developer.apple.com/library/content/documentation/FileManagement/Conceptual/APFS_Guide/Introduction/Introduction.html). Qui la cosa si fa tremendamente complessa e scrivere un manuale completo di Apfs, beh, dubito persino che sia materialmente possibile lavorando fuori dal *campus* di Cupertino, dove l’accesso a certe informazioni è precluso. Le capacità tecniche richieste per scrivere un manuale di Apfs sono, eufemismo, notevoli.

Chiaramente Apfs affonda le sue radici nel sistema operativo, con cui interagisce continuamente, e chi desiderava un ulteriore strato di complessità è servito. Aggiungo qui che *Inside Macintosh* l’ho avuto, utilizzato professionalmente, letto per necessità e non solo per diletto: era una lettura criptica al non iniziato e totalmente incomprensibile per il normale *utente*.

Insomma, dirlo non è come farlo. Nei miei sogni ho pensato diverse volte a un *opus magnum* su come funziona Mac a tre livelli: un livello base, dove si parla sostanzialmente dell’interfaccia grafica e null’altro, usare Mac come se fosse obbligatorio farlo con il puntatore del mouse; un livello avanzato assimilabile a una enciclopedia del Terminale, dove per ciascun comando si danno una descrizione e una spiegazione sia teorica che pratica, arrivando a coprire le modalità di funzionamento del sistema; e un livello intermedio, dove sta ciò che emerge dal Terminale o va più in profondità dell’interfaccia grafica. Qualche esempio: AppleScript e Automator, lo scripting bash, Xcode, Swift Playgrounds, l’interazione con iCloud e con iOS, la personalizzabilità di programmi e parti dell’interfaccia, i servizi di sistema.

Se cominciassi a scrivere questa *opus magnum* da solo resterebbe incompiuta, per mancanza di tempo prima ancora che per limitate conoscenze tecniche. E ogni sei mesi probabilmente tutto quanto già scritto andrebbe sottoposto a revisione, perché suscettibile di cambiamento da parte di Apple. Magari neanche annunciato.

Penso che un progetto di questo tipo finirebbe per essere perennemente incompleto e certamente lascerebbe insoddisfatti quelli che rimpiangono i bei tempi andati, quando eravamo giovani e c’era pure il manuale. La complessità del sistema è tale che il manuale è forse diventato impossibile da scrivere.

Intendiamoci: sono un *fan* dei progetti collaborativi e anche dei progetti dove alla collettività è richiesto solo il finanziamento. Per esempio, ci sono ancora tre settimane abbondanti per alimentare la [produzione di una versione libera dell’Arte della Fuga di Johann Sebastian Bach](https://www.kickstarter.com/projects/opengoldberg/libre-art-of-the-fugue) e la raccolta fondi è a metà del minimo indispensabile. Io contribuisco e invito chiunque a fare altrettanto, anche con un euro trovato in tasca.

Mentre fidarmi di una descrizione del *filesystem* di macOS scritta da qualcuno che non conosco, di buonissima volontà ma a cui potrebbe essere sfuggito qualcosa dal quale magari dipende l’integrità dei miei dati, non so.