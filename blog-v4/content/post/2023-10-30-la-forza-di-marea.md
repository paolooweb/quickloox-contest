---
title: "La forza di marea"
date: 2023-10-30T17:00:38+01:00
draft: false
toc: false
comments: true
categories: [Internet, Hardware, Software]
tags: [Siracusa, John Siracusa, Hypercritical]
---
Secondo John Siracusa, [il prossimo oceano blu di Apple sarà fornire batterie rimovibili](http://hypercritical.co/2023/10/29/apples-blue-ocean).

Breve spiegazione. L’oceano blu è una suggestiva espressione di marketing per indicare una zona di mercato dove non ci sono concorrenti e l’acqua è limpida. L’oceano rosso, in contrasto, è dove si affollano tutti e il colore dell’acqua dipende dalle conseguenze dei conflitti che nascono.

Nintendo, afferma Siracusa, è una specialista di oceano blu. Il mondo dei videogiochi e delle console sembra saturo e loro si inventano la Wii, diversa da tutto il resto.

Apple è un’altra esperta di oceano blu, dai computer curvi colorati e privi di floppy disk a [un aggeggio con le funzioni di cellulare, iPod e *Internet communicator*](https://www.youtube.com/watch?v=MnrJzXM7a6o). Domani Vision Pro potrebbe essere un altro oceano blu, chissà.

Le batterie saldate sono state un oceano blu e ora Siracusa ritiene che i progressi tecnologici siano maturi per consentire ad Apple tornare a offrire batterie sostituibili.

Può essere, naturalmente, però preferisco scommettere su Apple Silicon. A un certo punto le performance per watt arrivano al punto che la batteria dura praticamente un giorno e, francamente, chi vuole portarsi dietro la batteria di ricambio sapendo di non averne bisogno? iPhone è probabilmente l’apparecchio a maggiore rischio di autonomia di tutti, ma onestamente preferisco portarmi un cavetto in luogo di una batteria.

Più che all’oceano blu penso alle maree. Quella delle batterie saldate è una marea molto lunga e alta, che al momento di ritirarsi immagino lascerà dietro sé la fine delle preoccupazioni per l’autonomia delle batterie.

Post Scriptum: scrivo questo post da iPad che è rimasto senza carica da trenta ore. Naturalmente non sono tutte passate facendo funzionare la macchina, ma a livello di utilizzo occasionale il problema della batteria non mi si pone gran che.

<iframe width="560" height="315" src="https://www.youtube.com/embed/MnrJzXM7a6o?si=GnUa21bMoCf3RJB3" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>