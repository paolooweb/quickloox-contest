---
title: "Il capannello di allarme"
date: 2013-12-16
comments: true
tags: [education]
---
Nel lato interno della mia uniforme invernale da podista della domenica si trova una piccola tasca cucita, la cui icona esorta a sforbiciare. Dentro la tasca ho rinvenuto l’etichetta antitaccheggio, chiaramente inutile una volta completato l’acquisto.<!--more-->

 ![Antitaccheggio](/images/antitaccheggio.jpg  "Antitaccheggio") 

Anche senza una formazione da *hacker* si vede chiaramente un circuito. Fa parte di un sistema in cui, da qualche parte, c’è programmazione. C’è un sistema di gestione. Qualcuno produce le etichette, vende le uniformi, configura i sensori, maneggia i registratori di cassa. Qualcuno *programma*, ha a che fare con programmi, provvede a fare funzionare programmi in una catena di prodotti e produzione che di informatico, tecnologico, futuristico ha niente. Magliette per uscire a muoversi. Eppure scuci una tasca e trovi programmazione.

Cambio di scena. Oltreatlantico hanno svolto la manifestazione [Hour of Code](http://code.org), fatta per avvicinare i bambini alla programmazione. Dicono che oltre quindici milioni di bambini abbiano partecipato. Che siano state scritte dieci volte le righe di codice presenti dentro Windows (e molto più utili, anche fossero semplici [hello world](http://www.gnu.org/fun/jokes/helloworld.html)).

È escluso, assurdo e inutile che tutti i bambini, i ragazzi debbano imparare a programmare. Come dire che tutti i genitori dovessero sapere di elettrotecnica.

Tutti i genitori, però, devono saper cambiare una lampadina.

La scuola, oggi, fa zero per dare ai ragazzi nozioni generali di programmazione, come invece avviene per la storia, la lingua, l’aritmetica, il disegno. A scuola nessuno impara a cambiare la lampadina di domani.

Sono radunato qui, più o meno da solo, per avvisare.