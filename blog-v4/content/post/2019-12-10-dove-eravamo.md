---
title: "Dove eravamo"
date: 2019-12-10
comments: true
tags: [iPad, ZDNet]
---
*ZdNet* ha proclamato il *Device of the Decade*, l’aggeggio del decennio: [iPad](https://www.zdnet.com/article/device-of-the-decade-why-did-it-take-nine-years-for-the-ipad-to-get-its-own-operating-system/).

Annunciato il 27 gennaio 2010, ovvero all’inizio degli anni Dieci che si chiudono tra tre settimane.

Il sito celebra a modo suo, con il titolo *Perché ci ha messo nove anni ad avere il proprio sistema operativo?*.

Le domande da porre sarebbero altre. Apple è stata accusata di avere smarrito la strada dell’innovazione e l’idea del *device of the decade* rimasto tale per dieci anni sembrerebbe un appoggio alla tesi.

Non fosse che nel frattempo sono stati lanciati Apple Watch, AirPods, AirPods Pro, due Mac Pro di alterne fortune ma riprogettati da capo a piedi, gli amati odiati Servizi di ogni tipo…

*Dove erano tutti gli altri in questi dieci anni?* Se Apple non fa più innovazione, come mai nessun altro ha superato iPad durante tutto questo tempo?

C’era anche il vantaggio del sistema operativo che ci ha messo dieci anni per arrivare. Per altri, non è mai neppure partito.