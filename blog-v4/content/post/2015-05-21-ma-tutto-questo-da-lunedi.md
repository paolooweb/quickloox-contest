---
title: "Ma tutto questo da lunedì"
date: 2015-05-22
comments: true
tags: [Pavia, Inoltre, università]
---
Comunicazione di servizio: lunedì sera mi troverò a Pavia a fare quattro chiacchiere in ambiente universitario, situazione che tradizionalmente amo. Se capita un caffè o altro insieme prima o dopo, volentieri. (Non so quanto anticipo avrò e il *prima* contiene qualche elemento di incognita).<!--more-->

 ![Lunedì a Pavia](/images/inoltre.jpg  "Lunedì a Pavia") 