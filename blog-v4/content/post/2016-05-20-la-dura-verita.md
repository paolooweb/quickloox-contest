---
title: "La dura verità"
date: 2016-05-20
comments: true
tags: [Backblaze, Hgst, Seagate, dischi]
---
Backblaze continua a pubblicare meritoriamente i dati sulla durata dei dischi che acquista per la propria attività di fornitura di *backup* e io so che tuo cugggino ha comprato un disco l’altro giorno, che gli è morto in mano, e ha giurato di non comprare mai più quella marca che è inaffidabile. Ugualmente, nella scia dei [rapporti passati](https://macintelligence.org/posts/2013-11-22-i-lanci-del-disco/), sono comparsi i [dati del primo trimestre 2016](https://www.backblaze.com/blog/hard-drive-reliability-stats-q1-2016/), relativi a 61.590 dischi in attività e un miliardo di ore di funzionamento, l’equivalente di centoquattordicimila anni/disco.

Per la cronaca, i dischi migliori sono Hgst, ma anche Seagate non va malissimo. Ci sono anche i peggiori e li lascio alla curiosità di ciascuno (tra l’altro adesso Backblaze pubblica anche una newsletter che promette aggiornamenti regolari sulla questione.).

Fidarsi più dei dati del cugggino o di un fornitore di servizi che acquista i dischi a cinque-diecimila per volta? Io non avrei dubbi. Per avere un parere affidabile e veritiero sulla qualità degli hard disk, i parenti non bastano e neanche i negozianti.