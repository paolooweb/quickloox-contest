---
title: "Il colore dell’analfabetismo"
date: 2020-11-07
comments: true
tags: [Trump, Biden, Rossi, Mappa, Cognomi]
---
Al termine (di fatto, cavilli a parte) delle presidenziali americane, abbiamo capito chi ha vinto e chi fa propaganda sterile e faziosa: chiunque ricami su una mappa a due colori per ricavarne analisi che vorrebbero apparire sofisticate.

È risaputo che basta torturare i dati abbastanza a lungo per fare dire loro quello che vogliamo sentire; qui basta pochissimo. La rappresentazione blu e rossa (in ordine alfabetico) nasce per mostrare con immediatezza dove ha vinto il blu e dove ha vinto il rosso. Tutto il resto – l’idea della nazione spaccata, che i più intelligenti votino questo e i più cattivi votino quello, che le città e le campagne eccetera eccetera – è intrinsecamente malvagia e serve a fare propaganda altrettanto menzognera come le bugie sparate ad alzo zero, come sempre, durante questi mesi di campagna, da ambedue gli eserciti. Di uno bastava ascoltare il comandante in capo; l’altro esercito, fino al giorno prima, dichiarava anche dodici punti di vantaggio. Tutte tattiche già viste e anche da tanto.

Dovrebbe essere evidente che la mappa bicolore serve per il colpo d’occhio, non per il ragionamento; il poligono blu e quello rosso sono sempre dello stesso colore, che la maggioranza abbia avuto il cinquantuno percento oppure il novantanove.

La prova: se pubblico una cartina dell’America continentale colorando le nazioni in rosso o in blu secondo chi governa, gli Stati Uniti appariranno da gennaio tutti blu, dopo quattro anni tutti rosso.

Se coloro stato per stato, otterrò una mappa probabilmente sbilanciata cromaticamente rispetto al risultato effettivo, un po’ troppo rossa o troppo blu.

Ultimamente riscuote successo la mappa di un tizio che ha colorato contea per contea, fino a ottenere una mappa troppo rossa. Siccome non corrispondeva ai suoi gusti, legittimamente ha deciso di colorare per densità di popolazione. Ha ottenuto una mappa troppo blu, che però a lui legittimamente piace; così, illegittimamente, ha deciso di vestirla con un titolo propagandistico, dichiarando implicitamente che la sua visualizzazione è più giusta.

Non lo è. È distorta in modo opposto e simmetrico rispetto a quella che non gli piace. Ognuno ha diritto di spacciare le mappe che vuole, ma non di approfittare degli analfabeti numerici.

Non c’è tempo in una narrazione giornaliera di passare una settimana ad allestire un modello di visualizzazione della situazione elettorale statunitense. Dico solo questo: in una situazione teorica ideale, dovremmo utilizzare circa centotrenta milioni di punti, ognuno corrispondente a un votante (posto di voler ignorare i settanta milioni che non votano). Si parla di punti in astratto, non di pixel: la mappa che colora per stato è fatta di di cinquanta punti, grandi quanto il Montana o la Louisiana.

Chiaro che se rappresenti centotrenta milioni di dati con cinquanta punti, perdi appena un filo di informazione. Rispetto a centotrenta milioni, anche duemila punti-contee non aggiungono molto alla raffinatezza dell’analisi. Non sono mappe per tirare conclusioni sociologiche, ma per vedere chi ha vinto e dove. Fine.

In omaggio, presa da [Mappa Dei Cognomi](https://www.mappadeicognomi.it/index.php?sur=Rossi&s=Genera), la dimostrazione plastica della schiacciante maggioranza di Rossi in Italia. Nel senso del cognome. Ci credi solo perché vedi tanti cerchi che coprono tanta superficie? Abbiamo un problema.

 ![La prevalenza del cognome Rossi in Italia](/images/rossi.png  "La prevalenza del cognome Rossi in Italia") 

**Aggiornamento del 10 novembre 2020:** me la sono presa con il colpevole sbagliato, che è invece autore di [un bel lavoro complessivo](http://try-to-impeach-this.jetpack.ai), del quale le due schermate di cui parlo sono solo una parte. L’autore mostra bene i problemi delle due rappresentazioni e ne propone una terza, che ve proprio nella direzione già accennata sopra per quanto riguarda l’effettiva distribuzione dei voti.

C’è comunque un colpevole: chi ha tralasciato la parte istruttiva per darsi alla propaganda. Blu o rossa non importa proprio, ha stufato. Convincimi con una idea, invece che con uno slogan, e potrei darti ascolto a differenza di ora.