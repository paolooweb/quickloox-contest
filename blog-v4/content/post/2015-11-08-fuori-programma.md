---
title: "Fuori programma"
date: 2015-11-08
comments: true
tags: [Bolt, Meazza, OneDrive, Microsoft, iCloud]
---
Quanti articoli comparativi sono stati scritti sulle offerte di stoccaggio cloud più o meno convenienti, che magnificavano i vantaggi di quantità di OneDrive di Microsoft rispetto alla concorrenza?<!--more-->

Tutti da aggiornare, perché i vantaggi [spariscono](https://blog.onedrive.com/onedrive_changes/).

>Non offriremo più stoccaggio illimitato agli abbonati di Office 365 Home, Personal o University.

>I piani a pagamento da cento e duecento gigabyte spariscono per essere sostituiti con un piano da cinquanta gigabyte a 1,99 dollari mensili.

>Lo stoccaggio gratuito su OneDrive scende da 15 gigabyte a cinque gigabyte. Il bonus di 15 gigabyte per il rullino foto viene ugualmente eliminato.

Chi vorrà entrare nei dettagli scoprirà quanto la nuova offerta diventi straordinariamente simile a quella di iCloud e potrà chiedersi quando Microsoft fosse al corrente dell’insostenibilità della sua offerta e quanto la intendesse come esca per acchiappare con il gratis un sacco di pescioloni da spremere successivamente con il pagamento.

A me interessa più un altro aspetto della situazione: la motivazione.

>Da quando abbiamo iniziato a offrire stoccaggio illimitato, un piccolo numero di utenti ha archiviato numerosi PC e immagazzinato intere raccolte di film e registrazioni video. In alcune circostanze si sono superati i 75 terabyte per utente, o 14 mila volte lo spazio medio usato.

(Che quindi è di cinque gigabyte e spiccioli.)

Quando una multinazionale oggi offre un servizio a milioni, molti milioni di persone, si attrezza per soddisfare al meglio la media delle esigenze, non per soddisfarne qualsiasi. Perché su molti milioni di persone la variabilità delle esigenze è tale che riuscire a soddisfarle tutte diventerebbe impossibile.

Immaginiamo di trasformare lo [stadio Meazza di Milano](https://it.wikipedia.org/wiki/Stadio_Giuseppe_Meazza) in un ristorante e avere 80.018 avventori pronti a ordinare la cena. Possiamo scommettere che la scelta del menu sarebbe assai contenuta. Se ognuno degli ottantamila potesse ordinare qualunque cosa avesse per la testa, anche la più immensa delle cucine perderebbe il controllo.

Una volta era diverso. Una Microsoft, o anche una Apple, nel secolo scorso si rivolgeva a un pubblico tipo venticinque volte inferiore; la variabilità delle esigenze era di conseguenza minore ed era molto più facile che venissero accontentati anche i desideri più peculiari.

Torniamo al ristorante Meazza di prima. Chiaro che un ordine di spaghetti al ragù o di insalata caprese dovrebbe arrivare senza problemi e sarebbe giusto arrabbiarsi se questo non avvenisse, anche con ottantamila coperti. Chiedere [uova di riccio di mare](http://www.identitagolose.it/sito/it/115/2963/mare-aperto/i-ricci-si-mangiano-nei-mesi-con-la-r.html) sarebbe garantirsi il digiuno.

Torniamo a noi e ai nostri Mac, iPhone, iPad, iPod touch, Apple TV. Ci dà fastidio che Pages dia problemi con la sillabazione italiana? Giusto e sacrosanto arrabbiarsi. Ci dà fastidio che il computer non abbia cinque porte Usb? Oppure che Numbers non faccia calcolo infinitesimale? O che Siri non sappia come sta andando il campionato di tamburello?

Chiediamoci quanto sia fuori dalle righe il risultato che vogliamo ottenere. Più lo è, più siamo noi a dover essere capaci di realizzarlo. O pagando a parte una app indipendente che supplisce, o con un pizzico di programmazione a livello più o meno alto, oppure attingendo a esperienza e capacità poco comuni quanto il nostro obiettivo.

A chi pretende di risolvere problemi non standard con un’offerta standard si potrebbe chiedere di correre i cento metri in dieci secondi. *Ma io mica sono Usain Bolt!* E appunto, neanche hai comprato un apparato specialistico. Se vuoi fare qualcosa di straordinario, devi metterci del tuo anche se disponi del migliore hardware e software sulla piazza.