---
title: "Recinti e dettagli"
date: 2015-04-12
comments: true
tags: [Apple, Jobs, watch, Hertzfeld, Motion]
---
Sintetico e perfetto articolo di Wired su certi [livelli di dettaglio](http://www.wired.com/2015/04/apple-watch-design/) relativi alla progettazione e realizzazione di [watch](http://www.apple.com/it/watch/?cid=wwa-it-kwg-watch-com).<!--more-->

Perfetto il riferimento a quanti parteciparono alla creazione del primo Macintosh e videro la propria firma apposta all’interno della scocca, perché – scrive Andy Hertzfeld – [è semplicemente appropriato che un artista firmi il proprio lavoro](http://www.folklore.org/StoryView.py?project=Macintosh&story=Signing_Party.txt&topic=Apple+Spirit).

Sarà buono o cattivo, watch? Staremo a vedere. Intanto, a leggere, c’è da entusiasmarsi.

>Uno dei quadranti di watch, Motion, può essere importato per mostrare un fiore che sboccia. Ogni volta un fiore differente, un colore diverso. Non è computergrafica ma fotografia. “Credo che la ripresa più lunga dei fiori abbia richiesto 285 ore e oltre 24 mila scatti”.

>Non c’è una ragione ovvia per avere un quadrante di orologio con la foto di una medusa. Ma quelli di Apple non sono andati al Monterey Bay Aquarium con una fotocamera subacquea. Hanno costruito una vasca nel loro studio e ripreso una varietà di specie a trecento fotogrammi per secondo con videocamere Phantom di altissima qualità. Poi hanno ristretto le immagini 4.096 x 2.304 risultanti alla dimensione dello schermo di watch, che ha neanche un decimo di queste dimensioni. “Quando si guarda al quadrante con la medusa, nessuna persona ragionevole è in grado di cogliere questo livello di dettaglio. Per noi è comunque importante che questi dettagli siano perfetti”.

>"Quando [si usa il quadrante astronomico con i movimenti della Terra e della Luna] e si tocca la Terra e si vola sopra la Luna: abbiamo lavorato duramente con gli ingegneri per garantire che il percorso mostrato per arrivare dalla Terra alla Luna e la vista del satellite corrispondano a ciò che succederebbe nella posizione geografica effettiva in cui viene toccato il quadrante.

Si accenna a quanto diceva Steve Jobs: Apple può permettersi di *verniciare il retro del recinto*, fare attenzione anche a particolari che non si vedono.

A dirla tutta, ci sono molte altre aziende che possiedono liquidità e risorse per poterselo permettere. Ma non hanno la stessa visione delle cose.