---
title: "Trovare il Quadra"
date: 2022-04-02T01:16:48+01:00
draft: false
toc: false
comments: true
categories: [Web, Software]
tags: [Infinite Mac, Quadra, Macintosh Programmer’s Workshop, Ars Technica, Persistent, Chris Espinosa, Espinosa]
---
Una volta sì che ci si sentiva pionieri a usare Mac, perché mettevi le mani nel sistema e facevi i miracoli con ResEdit e ogni giorni imparavi qualcosa e ti sentivi hacker.

Ieri Apple ha compiuto quarantasei anni e il ragionamento convenzionale prosegue da almeno venti con la geremiade: oggi il sistema è chiuso, non si possono più mettere le mani nel sistema, non c’è più ResEdit, che bruttura, come faranno i gggiovani.

La semplice verità è che Mac degli anni ottanta, in termini relativi (se non capisci perché relativi, non hai avuto in casa [Macintosh Programmer’s Workshop](https://www.macintoshrepository.org/545-macintosh-programmer-s-workshop-mpw-3-x) e i suoi sei faldoni di manuale), era *semplice*.

Oggi Mac è *complicato*. A parte il fatto che avevi venti anni e adesso invece sessanta, certo sei più sveglio e scattante adesso che allora, ma la tragedia vera dell’aumentare dell’età sta nel fatto che a vent’anni potevi buttare via un sacco di tempo e a sessanta invece lo impieghi bene, avendo intanto capito un sacco di cose che a venti neanche di striscio.

Ma anche lavorando contro la propria intelligenza e mettendosi a buttare via tempo, nonostante la concentrazione e l’intelligenza vastamente superiori verso le stesse doti di una volta, mettendo le mani nel Mac di oggi, alla stessa maniera, guardandoci dentro e deducendo come stavano le cose, non caveresti un bit-ragno dal buco. In questi quarant’anni la tua saggezza è cresciuta in modo lineare, la complessità di Mac in modo esponenziale. È una missione impossibile.

La prova sta negli emulatori Mac che girano nel browser. Un Mac di quarant’anni fa (Ok, trentotto o meno) gira dentro una finestra di Safari. Se questo non ti chiarisce la questione, non c’è altro che si possa dire.

Occupandosi invece di chi fa girare Mac dentro una finestra di Safari, non solo ci riesce, ma di anno in anno diventa dannatamente più raffinato, come dimostra il lavoro fatto su [Infinite Mac](https://blog.persistent.info/2022/03/blog-post.html), che porta l’emulazione sempre più vicino a una vera usabilità.

L’autore parla di *un Quadra che parte istantaneamente dentro il browser* e non è cosa da poco. Sono state adottate soluzioni tecniche sofisticate, il tutto senza rinunciare all’obiettivo che era effettivamente realizzare una applicazione web che emuli un antico Mac. Alcuni emulatori correnti raggiungono lo stesso scopo ma sostanzialmente fanno caricare al browser una applicazione fatta e finita. Non è proprio emulazione nel browser.

Il famoso Quadra parte in un secondo, è pronto in tre secondi, dispone di archivio persistente, fornisce prestazioni paragonabili a quelle di un Quadra del mondo reale, può caricare file dall’esterno, insomma, è un’esperienza da provare, ben più ricca del giocattolo che erano i primi emulatori, degni di nota perché pezzi di bravura di un programmatore ma poco altro, assai sensibili all’effetto noia dopo i primi dieci minuti.

Un articolo di *Ars Technica* compie [una bella panoramica attorno al progetto Infinite Mac](https://arstechnica.com/gadgets/2022/04/boot-up-classic-mac-os-in-your-browser-window-with-the-infinite-mac-project/) e costituisce il modo più veloce di apprezzarlo.

Il punto vero è un altro. Per legge di natura, il software che realizza l’emulazione è più complicato del software che viene emulato. E tutto sta dentro una pagina del browser, accanto ad altre (decine di) pagine nel browser, a sua volta affiancato da numerose altre applicazioni di pari dignità rispetto al browser.

Il Mac di trenta anni fa, confrontato con quello di oggi, è una cosetta. Chi voglia trovare una quadra per conciliare tempo che passa e voglia di fare, sporcarsi le mani e intanto imparare, può scegliere miriadi di altre branche dell’informatica personale, più semplici e più accessibili di macOS.

Gli scettici possono anche scrivere a [Chris Espinosa](http://www.storiediapple.it/chris-espinosa-luomo-dei-manuali.html): nessun vivente ha lavorato in Apple più a lungo di lui. È un bel soggetto per ricordare questo quarantaseiesimo compleanno.