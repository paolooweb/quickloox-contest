---
title: "Abbiamo due problemi"
date: 2016-05-13
comments: true
tags: [Apple, Mac, Macity, Canalys, TrendForce]
---
Sarà vero che le vendite dei portatili Apple sono crollate (non diminuite, scese, ridotte, in flessione, declinanti, negative: crollate, che fa clic, come il vino fa buon sangue) del 40 percento?

Prima di tutto, una informazione di contesto: secondo Canalys, nel primo trimestre 2016 il mercato globale dei computer sarebbe crollato, ehm, [sceso anno su anno del 13 percento](http://www.canalys.com/newsroom/pc-market-2011-levels-tablets-fall-sixth-quarter), ai livelli del 2011. Apple rimarrebbe il primo produttore nonostante un crollo, pardon, una discesa del 17 percento. Sì, il primo produttore perché Canalys conta nei computer anche le tavolette. C’è stato crollo, no, declino anche per loro. In Nordamerica la flessione globale è stata a cifra singola, ma in Europa e dintorni il dato è -15 percento, dove i portatili crollano, anzi, decrescono addirittura del 18 percento. Il trimestre ha visto crolli, beh, diciamo piuttosto contrazioni ovunque.

I [dati ufficiali](http://images.apple.com/pr/pdf/q2fy16datasum.pdf) Apple dicono che i Mac sono in calo del 12 percento anno su anno. Tutti i Mac, non solo i portatili; tuttavia, se i portatili fossero sotto del 40 percento, vorrebbe dire che iMac, Mac mini e Mac Pro hanno registrato una crescita mica da ridere come contrappeso. Il che sembra improbabile: sarebbe stata evidenziata e pubblicizzata, come controtendenza controcorrente.

Forse il confronto è sequenziale, non anno su anno ma trimestre su trimestre precedente? Solo un inetto può pensare di confrontare il trimestre di Natale con quello successivo pensando che siano equivalenti e confrontabili. In tutti i casi, il dato dei Mac, tutti i Mac, è -24 percento. Consistente – il trimestre di Natale è stato record – però ben lontano da questo mitico 40 percento. Ancora una volta, i desktop avrebbero dovuto registrare un successone. Oppure i conti non possono tornare.

Di che si sta parlando allora? Di un [rapporto di TrendForce](http://press.trendforce.com/press/20160510-2472.html) secondo il quale i Mac portatili hanno chiuso il primo trimestre 2016 con una quota di mercato del 7,1 percento su un totale di 35,622 milioni di portatili venduti. Sarebbero 2,529 milioni di MacBook. Il totale dichiarato da Apple è 4,034 milioni di macchine. Cioè, servono 1,505 milioni di desktop. Tre desktop ogni cinque portatili. Ci credo pochissimo.

La quota di mercato del trimestre natalizio per i portatili Apple, dice TrendForce, era del 9,7 percento. Il comunicato stampa manca di qualsiasi altro dato per dare credito all’idea del -40. Si noti che, nella tabella pubblicata assieme al comunicato, tutti se la passano malissimo, a eccezione di Samsung che ha però peso infimo. Apple perde 2,6 punti percentuali in quota di mercato, che equivarrebbero misteriosamente al 40 percento di caduta nelle unità vendute.

Riassumo: fuffa. Se esiste un 40 percento, è una qualche variazione di quota di mercato all’interno di un intrico di andamenti di più aziende, con zero corrispondenza rispetto alla vendita reale degli apparecchi. La quale, si è visto, è rallentata ovunque per chiunque. Questo non vuol dire che i MacBook Air non meritino un rinnovamento o che sarebbe bello vedere nuovi processori eccetera eccetera: vuol dire che l’immagine del crollo, come il grattacielo raso al suolo in mezzo agli altri grattacieli che svettano immutati, è come minino finta, se non scientemente falsa.

Forse proprio per questo Macity titola [Cupertino abbiamo un altro problema: i portatili Apple crollano del 40%](http://www.macitynet.it/cupertino-abbiamo-un-problema-i-portatili-apple-crollano-del-40/).

Non gli sembra vero, di poter annunciare una Apocalisse con la solita maestria nell’italiano: quello di Apple è un *tonfo negativo* (aspettiamo di vedere tonfi positivi, o magari neutri, boh) delle *spedizioni di portatili* (mentre forse i ritiri in negozio sono in crescita?).

Se Cupertino ha un problema, a Macity, tra i copiaeincolla, l’italiano da licenza elementare, le traduzioni maccheroniche e la completa mancanza di deontologia, forse ne hanno pure un paio.
