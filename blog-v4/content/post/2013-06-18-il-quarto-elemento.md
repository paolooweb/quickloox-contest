---
title: "Il quarto elemento"
date: 2013-06-18
comments: true
tags: [Giochi]
---
Ci metto poco dato che il lavoro lo ha fatto tutto **Fearandil** in <a href="http://www.iplay4fun.net/itech4fun/2013/06/mfi-controllers-il-quarto-incomodo/">questo pezzo</a>.

Mi offro volontario per la correzione di qualche minimo refuso; per il resto il punto di vista è intrigante e non mi stupirei di vedere presto conferme di quello che ipotizza.