---
title: "Un giorno tutto questo sarà loro"
date: 2022-07-20T02:23:40+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [Swift Playgrounds, LinkedIn, Internet Relay Chat, IRC]
---
Oggi dovevo firmare un consenso e potevo farlo solo di persona, così ho totalizzato cento minuti di auto per mettere una firma (in realtà cinque, ma ci siamo capiti).

Stasera ho letto un mentecatto su LinkedIn tuonare contro i consumi energetici del software mal scritto. Non ho capito perché lo scrivesse su un social, per sua natura avido di risorse, invece che su un canale [Internet Relay Chat](https://datatracker.ietf.org/doc/html/rfc1459), ma tant’è.

Prima di tutto, a parte casi minimi ed eccezionali, ci sarebbe molto da dire sui criteri che determinano il consumo energetico o la bontà di un software.

Secondo, un software sprecone che mi avesse consentito di firmare un consenso a distanza avrebbe tenuto ferma la mia auto. Se avesse fornito lo stesso tipo di vantaggio ad altre novecentonovantanove persone, avrebbe tenuto ferme mille auto. Parliamone un attimo, di questi sprechi energetici.

Va anche detto che l’industria del software è ai primordi. Mai vista una partita di qualche sport di settanta anni fa? Quello stesso sport, oggi, è sicuramente cambiato moltissimo. Settanta anni fa i programmatori software si contavano sulle dita di due mani e scrivevano software che, rispetto ai requisiti odierni, era ridicolo.

Il software è cambiato moltissimo, ma ha avuto molto meno tempo, per dire, dei motori a scoppio. Deve ancora crescere e migliorare. Certamente migliorerà anche sotto il profilo energetico.

Per coincidenza, oggi un amico mi ha scritto di avere convinto il figlio a programmare usando [Swift Playgrounds](https://www.apple.com/swift/playgrounds/). La strada è questa: così come mandiamo giustamente i figli a fare sport, dobbiamo mandarli a programmare.

Milioni di figli hanno contribuito nel tempo a fare del rugby o del tennis uno sport assai più evoluto che agli inizi. Milioni di figli che imparano a programmare contribuiranno a un mondo migliore, almeno lato software.