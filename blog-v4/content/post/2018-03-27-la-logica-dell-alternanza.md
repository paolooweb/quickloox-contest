---
title: "La logica dell’alternanza"
date: 2018-03-27
comments: true
tags: [Apple, lavoro, Jackson, dBase, II, Bit, J.soft]
---
I *come eravamo* sanno di stantio e tuttavia, se leggo di [ragazzi e alternanza scuola-lavoro](https://www.facebook.com/story.php?story_fbid=10216669061424921&id=1412334258), devo tornare a quando sono stato ragazzo, per capire.<!--more-->

Capisco che ci possano essere iniziative male organizzate (come anche può accadere a una gita scolastica, peraltro). Capisco che esistano aziende male strutturate per accogliere ragazzi e che altre lo facciano in malafede. Tutte cose che vanno aggiustate ma non riguardano il meccanismo in sé.

Le rivendicazioni pseudosindacali dei ragazzi, invece, gli striscioni e le foto propaganda con lo slogan, la T-shirt di contestazione, il lamento sulla poca paga o la non paga, la critica contro il turboliberismo mi lasciano perplesso.

Da piccolo volevo fare l’astronauta, il giornalista o il conducente della metropolitana. Da postliceale ebbi l’opportunità – parola non a caso – di trovarmi a svolgere piccole mansioni per il Gruppo Editoriale Jackson. Libri ma soprattutto riviste, la cosa più vicina al giornalismo che potevo immaginare.

Finì che dopo qualche mese il direttore di *Bit* mi chiese se me la sentivo di andare in centro dove una *boutique* aveva messo un computer (!) a disposizione dei clienti (!), per *scrivere un pezzo*.

Poco dopo uno *spinoff* di Jackson, J.soft, mi chiese se me la sentivo di fare il redattore unico di una nascente rivista dedicata ai computer Sinclair. Certo che me la sentivo. Da lì è cominciata una parte piuttosto importante della mia piccola storia.

In Jackson mi misero a fare *data entry*. Cognome, nome, indirizzo, codice postale, telefono. Ancora adesso mi è rimasta una certa memoria di Cap e prefissi. Certamente non era giornalismo. Forse, ragazzino oggi, mi fotograferei come sfruttato davanti al terminale.

Che era un Apple II, con lettore di floppy disk da centodieci chilobyte e relativo database per l’inserimento dell’anagrafica. Ogni venti nominativi il programma disabilitava l’input e ordinava i dati sul disco, con un algoritmo che probabilmente oggi si studia nelle terze elementari; con pochi dati era un lampo mentre, a disco quasi pieno, passavano anche venti minuti. Durante le attese leggevo il manuale del computer e di conseguenza imparavo qualche nuova parola di inglese.

Il database era scritto come si scriveva il software in quel tempo: ringrazia che funzioni. Passò poco prima che un nome particolare spingesse il cursore oltre i confini della maschera di inserimento, libero di muoversi nello sconfinato oceano della matrice da ventiquattro righe per quaranta colonne. Però leggevo il manuale; sapevo che cosa fare. Finì che pilotare il cursore a prescindere dalla maschera di input velocizzava assai l’inserimento dati. Avevo imparato e molto.

Terminai l’inserimento e lo scopo della mia presenza lì. Ma volevo assolutamente restare in quell’ambiente. Improvvisai un discreto porta a porta redazione per redazione, nel caso qualcuno avesse avuto qualcosa da farmi fare. Funzionò: c’era un archivio fotografico da tenere ordinato.

Un grosso armadio di cartelline di carta appese via ganci di plastica a tondini di ferro che attraversavano ogni scaffale da sinistra a destra. Dentro ogni cartellina foto di uomini di azienda e più raramente eventi. Quasi tutti dirigenti di nuova nomina, inviati assieme al comunicato stampa. Di solito formato A4 e bianco e nero.

Un discreto problema per una redazione, perché ogni giorno arrivavano decine di buste con comunicati stampa e foto. Andavano organizzati, per trovarli in fretta quando servivano nel contesto di una notizia. Lavoro noioso, senza fine, mai urgente eppure guai a trascurarlo; in un attimo la pila delle buste iniziava a crescere, le foto abbandonate sulla scrivania in eccesso finivano fuori controllo. Quasi mai riportavano sul retro indicazioni utili e se si staccavano fisicamente dal comunicato stampa diventava problematico identificarle.

Metodo quotidiano, costanza, pazienza, precisione e allineamento con i redattori. Se non avessero trovato le foto per come le organizzavo, sarebbe stata colpa mia. Ordine alfabetico e cronologico. Intanto capivo che aziende erano sul mercato, di che cosa si occupavano. Tutti i giorni provvedevo a ordinamenti alfabetici e cronologici. Tutte le buste andavano aperte e tutti i materiali sistemati, anche se era venerdì pomeriggio. Ho imparato molto, soprattutto nel rapportarmi con persone in un ufficio e con il lavoro in senso generale.

Intanto si sviluppava lo *spinoff* cui ho accennato. Le anagrafiche che avevo inserito servivano a loro. Ora dovevano spedire materiali vari a tutti gli indirizzi inseriti. Me la sentivo di occuparmi del database – certo! – e delle stampe?

Non avevo mai stampato alcunché in vita mia. Certamente, risposi.

Il giorno dopo avevo da produrre alcune migliaia di etichette adesive su cui stampare l’indirizzo. Le etichette stavano su immensi rotoli di fogli che il rullo della stampante ad aghi faceva scorrere dentro il meccanismo. Peccato che l’interlinea della stampante fosse lievissimamente sfasata rispetto a quella che prevedevano le etichette; pochi fogli e lo scompenso si faceva totale e disastroso. La stampa doveva procedere da sola per tutta la notte e dunque l’allineamento degli indirizzi sulle etichette avrebbe dovuto essere perfetto.

Trovai per fortuna il manuale della stampante in uno scatolone. Imparai l’esistenza di caratteri Ascii speciali, che non corrispondevano a niente di visibile ma influenzavano il funzionamento della stampante. Per esempio, variavano l’interlinea delle righe in settantaduesimi di pollice.

Riuscii a sistemare tutto in tempo. E a imparare diverse cose.

Nel frattempo le cose evolvevano e pure il database di indirizzi. Ora era ospitato in un disco rigido da venti megabyte ed era sempre un Apple  II a pilotare tutto, ma il software era diventato [dBase II](https://it.m.wikipedia.org/wiki/DBase#dBase_II): un bel programma per i tempi, che consentiva di estrarre insiemi specifici di dati grazie a uno psudolinguaggio di programmazione. Me la sentivo di…? Ovviamente sì. Estraevo indirizzi da un’anagrafica e mi impratichivo di piccola programmazione.

Poi divenni effettivamente redattore e continuai ad apprendere un sacco di cose. Però avevo un ruolo preciso e quindi il racconto si fa meno interessante: in un certo senso ero tenuto a imparare, per lavorare al meglio.

È l’inizio che conta. All’inizio ho inserito indirizzi, riordinato archivi, stampato etichette. Nell’azienda di oggi sono cose che fanno al centralino quando nessuno telefona. Il punto più basso della professione.

Ma partivo da zero e quello è servito a darmi delle basi e a capire tante cose.

Se leggo di un alternante scuola-lavoro che si è ritrovato a fare fotocopie o svolgere mansioni umili, sono lieto per l’opportunità che riceve: tornassi indietro, rifarei tutto da capo. Perfino gli Agnelli hanno mandato i loro rampolli a fare gli operai in incognito; che capissero come funzionava alla base, prima di occuparsi del vertice. John Sculley, prima di diventare vicepresidente Pepsi e marito della figlia del presidente, lavorò alla catena delle patatine fritte.

Senza dimenticare che parliamo di aziende e servizi, terziario avanzato quando non oltre. Frequento da sempre la costa adriatica e so perfettamente come legioni di ragazzi e ragazze imparino a lavorare e a stare al mondo facendo *la stagione* nei bar in riva al mare o in gelateria. Lavori umili, stancanti, malpagati di sicuro. Ma non vedo mai mancanza di manodopera o sindacalismi da liceo, cioè ridicoli.

Io mi lamenterei meno e cercherei di imparare. Persino le fotocopiatrici oggi contengono tecnologia.
