---
title: "Le campane negli occhi"
date: 2021-11-16T01:03:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [MacBook Pro, Austin Mann, M1 Max, StarStax, Starry Landscape Stacker, Tubular Bells, Adobe Camera Raw, Photomerge, Cinebench, Intel, Gap Filling] 
---
Austin Mann [ha recensito a suo modo MacBook Pro 16” con M1 Max](https://austinmann.com/trek/macbook-pro-m1-max-for-photographers) e racconta questo:

>Ho caricato Mac al cento percento e ho staccato l’alimentatore.

>Per prima cosa, ho trattato cento file Tiff da centocinquanta megabyte ciascuno in [Starry Landscape Stacker](https://apps.apple.com/it/app/starry-landscape-stacker/id550326617?l=en&mt=12). Il rendering ha impiegato quattro minuti e ventiquattro secondi, al termine dei quali la batteria era sempre a cento percento (le ventole sono rimaste silenziose).

>Per seconda cosa, ho lanciato un test [Cinebench](https://www.maxon.net/en/cinebench), che è terminato nel giro di alcuni minuti e *ha lasciato la batteria ancora al cento percento*.

>Sono tornato ai cento file Tiff, li ho aperti in [StarStax](https://markus-enzweiler.de/software/starstax/) e ho lanciato un mix *Gap Filling* che ha richiesto due minuti e trentasei secondi. Anche dopo questo procedimento esigente, *la batteria è rimasta al cento percento*.

>Così ho aperto otto immagini in [Adobe Camera Raw](https://helpx.adobe.com/camera-raw/using/supported-cameras.html) e ho creato un gigantesco panorama con Photomerge. C’è voluto poco e, come si può immaginare, *la batteria è rimasta al cento percento*.

Mann è forse un *fanboy*? È pagato da Apple per millantare prestazioni esistenti solo sulla carta? Aveva un problema di aggiornamento della barra dei menu? Niente di tutto ciò:

>A questo punto avevo finito le idee, così sono tornato su Cinebench e ho avviato nuovamente il test. Dopo due minuti e mezzo, la batteria è infine passata al novantanove percento.

Certo, ma senza il confronto con una macchina Intel, significa poco. Il confronto c’è:

>Per confronto, ho eseguito gli stessi test sul mio MacBook Pro 16” Intel, la cui batteria è valida all’ottantacinque percento. Dopo avere completato il passo tre (Gap Filling in StarStax), la batteria stava al settantuno percento. Le ventole sono partite al massimo durante il primo passo e non si sono più fermate.

D’accordo, ma è chiaro che risparmiare batteria e avere zero ventole significa sacrificare le prestazioni. In effetti, riferisce Mann, M1 Max è andato veloce solo *il doppio* rispetto al processore Intel sul MacBook Pro 16” precedente.

Per coincidenza, leggendo la recensione durante l’ascolto durante il crescendo finale della [prima parte di Tubular Bells](https://www.youtube.com/watch?v=BfWJqKIxyGc). Ho sentito nelle orecchie il procedere di MacBook Pro che restava al cento percento di batteria. Mann no; lui lo ha visto, ovviamente.

<iframe width="560" height="315" src="https://www.youtube.com/embed/BfWJqKIxyGc" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

*Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile.*