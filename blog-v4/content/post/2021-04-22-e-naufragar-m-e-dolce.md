---
title: "E naufragar m’è dolce"
date: 2021-04-22T13:49:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [Philips, 276E8VJSB, Vesa, DisplayPort, Hdmi, Big Sur, Mac mini, i3] 
---
Venivo da un vecchio monitor Full HD, che peraltro simulava la risoluzione senza averla fisicamente a disposizione, con la conseguenza di una nitidezza relativa delle immagini. Ora scrivo guardando su un nuovo Philips 276E8VJSB/00 [ordinato da un rivenditore terzo via Amazon](https://www.amazon.it/gp/product/B07HNS43Z5/ref=ppx_yo_dt_b_asin_title_o01_s00?ie=UTF8&psc=1) e arrivato in tre giorni.

(Perché si sappia come funziona il mondo; ora che l’ho comprato, se torno sulla pagina, Amazon mi mostra il prezzo maggiorato di quaranta euro rispetto a quanto l’ho pagato).

Avevo iniziato la ricerca di un monitor molto largo e basso, tipo 21:9, ma ho ripiegato su un più convenzionale 16:9 da ventisette pollici. Se ci facessi la bocca, inizierei a considerare nel medio termine l’acquisto di un secondo monitor di taglia uguale, da affiancare al primo.

C’è tempo. Visto che mi sono tuffato in uno schermo più grande, ho deciso di viverlo all’estremo e così, delle numerose risoluzioni che ho a disposizione, ho impostato la massima, 3.840 x 2.160.

Shock culturale.

Venivo da un Full HD, 1.920 x 1.080. Fatti i conti, il mio nuovo spazio scrivania è circa quattro volte il precedente. Alcune finestre che erano a tutto schermo, di Safari, di Miro, di Pixelmator Pro, me lo hanno confermato empiricamente.

Come vivere in un tre locali e traslocare in un dodici locali.

È una condizione estrema, che non andrà bene per tutti. Ho una vista quasi perfetta e dunque, se c’è nitidezza, leggo tranquillamente anche caratteri microscopici.

Per dire: sulla barra dei menu avevo l’orologio analogico, con le lancette, perché si intonava meglio alle icone. Ora la barra è talmente sottile che con le lancette è impossibile discernere l’ora. Ho impostato la vista digitale.

Mail, con l’impostazione che avevo prima, mostra ventiquattro messaggi in arrivo, uno sotto l’altro. A ottanta caratteri per riga, questo punto del post riempie metà della finestra utile in BBEdit. La finestra di YouTube mostra in verticale diciotto video alternativi. Potrei avere affiancate quattro finestre di BBEdit come questa e avanzerebbe spazio. Sto scrivendo in corpo 18, per avere testo più agevolmente leggibile di quanto sia quello dell’interfaccia. Prima scrivevo in corpo 14.

Uno dice, se hai preso il monitor più grande per scrivere con caratteri più grandi, allora è tutto inutile. Non vero: i due incrementi sono tutt’altro che lineari. Ho alzato il carattere del trenta percento ma ho aumentato lo spazio scrivania del trecento percento. Il problema di ottimizzare le situazioni di lavoro per avere più finestre contemporaneamente visibili, non esiste più.

Questo in generale. Il monitor in sé? La robustezza dell’apparecchio è soddisfacente. La cornice è in materiale plastico, mentre il sostegno è in metallo. I bordi della cornice sono discreti e ragionevolmente stretti, l’insieme si inserisce bene in qualunque ambiente. Il piedistallo è un arco curvo, che occupa fisicamente pochissimo spazio sulla scrivania; il monitor si protende in avanti e, se il sostegno è appoggiato a una parete posteriore o arriva comunque al limite della scrivania, lo schermo si troverà una decina di centimetri più in avanti verso l’osservatore. Non esiste regolazione in altezza, mentre è possibile inclinare a piacere. Ovviamente il monitor è compatibile Vesa e può essere montato su una parete. Nella confezione il piedistallo è separato dal monitor; i due pezzi si mettono insieme in un secondo con una vite che, intelligentemente, si regola a mano grazie a un anello ripiegabile. Ci vogliono davvero cinque secondi.

I connettori sono tutti sul retro dell’unità. Fortunatamente, per come è fatto il piedistallo, è un attimo inclinare il monitor in avanti e arrivare ai connettori. Che sono pochi, in un monitor che serve solo in quanto tale e non funge da hub Usb o altro come altri modelli più sofisticati. Qui ci sono due porte Hdmi 2.0, una DisplayPort, un jack audio e la presa per l’alimentazione. Dalla scatola escono un cavo Hdmi, il piccolo alimentatore esterno e il cavo di prolunga per arrivare a una presa di corrente.

Sotto lo schermo, in mezzo, sporge dal telaio un logo Philips in trasparenza. Allungando una mano lì sotto, sul retro, si trova il pulsante di accensione, che astutamente può essere premuto oppure inclinato nelle quattro direzioni. Così agisce come un nanojoystick per la configurazione dello schermo. All’accensione era tutto già ottimamente impostato per i miei gusti, a parte la luminosità, che da sempre tengo al minimo. Ho ridotto la luminosità e per il resto, sul monitor, non ho toccato alcunché.

Su Mac ho impostato la risoluzione massima. Onestamente non ricordo quale, di quelle intermedie, abbia trovato preimpostata.

Ho avuto qualche problema iniziale perché, in modo non proprio ortodosso, a Mac acceso ho staccato il cavo Hdmi dal vecchio monitor e l’ho attaccato a quello nuovo. Il Mac era a schermo spento e non so dire se ci fosse stato un crash di Big Sur o se il maneggio con il cavo video lo abbia indotto; di fatto il computer non rispondeva. A un certo punto è apparso il desktop, per sparire un secondo dopo. Mac era in crash, dato che non accettava connessioni ssh; l’ho riavviato ed è comparso lo schermo. Tutto regolare.

A sera, siccome è effettivamente molto luminoso anche con la mia impostazione (la base del monitor misura sessantotto centimetri e di superficie irradiante ce n’è), ho fatto una cosa che non faccio mai; ho messo il Mac in stop. La mattina dopo, premendo un tasto, Mac si è svegliato ma il monitor no. Ho accennato a effettuare una connessione da iPad con Chrome Remote Desktop e subito lo schermo si è acceso.

Non so ancora dire se sia un problema di Mac come hardware, di Big Sur, dello schermo. Difficile che sia un problema del cavo Hdmi perché ne ho provati due diversi e ho provato anche a cambiare la porta usata. Stasera provo a lasciare Mac acceso (come faccio sempre) ed eventualmente a spegnere il monitor. Vediamo che succede. Va detto che a questo momento ho totalizzato quasi due giorni di lavoro e a parte questi due momenti, tutto ha funzionato perfettamente; quando non sono alla scrivania, dopo qualche minuto lo schermo va in standby e, quando torno, tutto si riaccende regolarmente. Per un attimo ho pensato di comprare un cavo DisplayPort, ma in effetti sto lavorando.

La nitidezza e il contrasto sono ottimi per il mio gusto e specialmente proveniendo da tecnologia inferiore su tutto. La luminosità, sempre a mio gusto, è persino esagerata e certo l’utilizzatore medio che vuole uno schermo squillante si troverà a proprio agio. Il colore è precalibrato, da quanto ho capito, e a vedere lo sfondo standard di Big Sur, in modo soddisfacente. Non sono un grafico e accetto qualsiasi critica in proposito; per quello che serve a me quanto a precisione del colore e nitidezza del testo, la spesa vale abbondantemente la resa.

Ho provato la risoluzione massima anche per sperimentare che cosa può accadere dando in pasto otto milioni di pixel a un Mac mini con processore i3 e scheda video integrata, certo non un fulmine di guerra.

Ho avuto una bella sorpresa. La risposta è ottima. Certamente non è un setup indicato per fare elaborazione pesante con Photoshop. Nell’uso composito che faccio, veramente nessun problema. Qua e là mi è capitato di vedere un attimo di scattosità, il che mi fa pensare di essere ai limiti di quanto la macchina si possa permettere. Solo un attimo, comunque, e poi si lavora al cento percento. Immagino che basterebbe diminuire anche solo di uno scatto la risoluzione per eliminare qualsiasi ansia da prestazione. Oppure un processore i5, che nell’epoca di M1 è veramente un parente povero e però sarebbe più che adeguato.

Si accettano domande ove avessi dimenticato qualche dettaglio. Per curiosità, questo post – ottomilanovecentoventotto caratteri compresa l’intestazione necessaria al motore del blog – è interamente visibile nella finestra, sempre nel corpo 18 in cui l’ho scritto, ovviamente allargando la finestra stessa. E di fianco ci starebbe un’altra finestra uguale. Scorrimento zero, zoom zero. Il guadagno di produttività rispetto a come stavo prima è clamoroso.

Molti hanno un monitor moderno da tempo e sorridono all’ultimo arrivato. Ad altri, ancora con un monitor vecchio, posso solo consigliare il cambiamento. La spesa si sente, certo, ma è onesta per le prestazioni. Questo mare di pixel finirà per diventare abituale; intanto, me lo godo.

*Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile.*