---
title: "Piuttosto che piuttosto"
date: 2018-02-18
comments: true
tags: [Twitter]
---
Se avessi un soldino per tutte le volte che ho sentito dare Mac in pericolo perché Apple pensa solo a iPhone. E mai nessuno che dia la dovuta retta a idiozie come [questa di Twitter](https://twitter.com/TwitterSupport/status/964635740444360704):

<blockquote class="twitter-tweet" data-lang="en"><p lang="en" dir="ltr">For the full Twitter experience on Mac, visit Twitter on web. 👉 <a href="https://t.co/fuPJa3nVky">https://t.co/fuPJa3nVky</a></p>&mdash; Twitter Support (@TwitterSupport) <a href="https://twitter.com/TwitterSupport/status/964635740444360704?ref_src=twsrc%5Etfw">February 16, 2018</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script> 

>Stiamo concentrando i nostri sforzi su una grande esperienza Twitter che sia coerente su tutte le piattaforme.<!--more-->

No. Stiamo penalizzando le piattaforme migliori per non irritare quelli delle piattaforme peggiori, per conseguire un risparmio peloso che pagheremo dieci volte in reputazione, perdita di utenti, perdita di conoscenze e di talenti interni e degrado della cultura aziendale.

La buona notizia è che esistono app alternative a quella ufficiale (io uso [Tweetbot](https://itunes.apple.com/us/app/tweetbot-for-twitter/id557168941?mt=12) per esempio) e dunque l’unico vero problema lo avranno quelli di Twitter, più che quelli di Mac.

Uno di quei casi, a dirla tutta, dove piuttosto che niente, è meglio niente.
