---
title: "Il sistema del vicino è sempre meno open"
date: 2021-06-17T13:53:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [OldOS, iOS, open source, Gplv3, LibreItalia] 
---
Mi [tira le orecchie](https://muut.com/quickloox#!/apple:codice-non-parole) **mmanighe** con piena giustificazione perché ho effettivamente esagerato nel [dare iOS 4 per open source](https://macintelligence.org/posts/Codice-non-parole.html).

Nella pagina dell’[open source di Apple](https://opensource.apple.com), effettivamente, per iOS 4 sono aperti solo alcuni componenti che certo non formano il sistema operativo.

La situazione non migliora né peggiora andando avanti, nel complesso. Apple, come tutte le grandi aziende, ha cercato nel tempo di distanziarsi dal software con licenza open source [Gplv3](http://www.gnu.org/licenses/gpl-3.0.html), perché *troppo* libera. Altre cose sono open source, per esempio il [kernel Darwin-XNU](https://github.com/apple/darwin-xnu) da cui prendono vita tutti i sistemi operativi sotto la Mela e però non escono dal principio guida di *commoditization of the complement* ben descritto, a tutti i livelli, in [questo articolo](https://www.gwern.net/Complement).

In pratica, ciò che è vitale per il proprio business resta proprietario e ciò che non lo è diventa libero. Questa regola si applica anche in altri settori. Si potrebbe osservare che Microsoft è ancora più avanti; oltre a rendere libero quello che non le è strategico, si preoccupa di colonizzare e invadere qualsiasi progetto non suo che possa recarle nocumento, in modo che resti libero – non può essere altrimenti – e intanto si diriga dove lei preferisce.

Nel dubbio, sostengo sempre l’opportunità di una iscrizione a [LibreItalia](https://www.libreitalia.org). E poi, ricordiamoci di quanto codice ci è comunque utile, è comunque libero (o ancora veramente libero) e si meriterebbe una volta l’anno gli euro di un caffè con cornetto, anche se nessuno ha chiesto denaro direttamente e in forma vincolante, o detraibile come spesa.

*Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile.*