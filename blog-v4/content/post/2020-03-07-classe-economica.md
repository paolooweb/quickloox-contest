---
title: "Classe economica"
date: 2020-03-07
comments: true
tags: [Oppo, watch, iPhone, Android]
---
Uno studio afferma che [i computer da tasca di vertice Android si deprezzano al doppio della velocità di iPhone](https://www.bankmycell.com/blog/cell-phone-depreciation-report-2019-2020/).

Oh bella, dirà qualcuno, come mai, dal momento che dentro sono tutti uguali e fanno le stesse cose?

Probabilmente la risposta è che sono un po’ uguali anche fuori, come succede con i computer da polso: [Oppo Watch copia watch per offrire una alternativa su Android](https://www.cnet.com/news/oppo-watch-copies-apple-watchs-design-to-offer-an-android-alternative/).

Chissà che valore può avere un design adottato senza sapere il perché, purché costi meno.