---
title: "Lo zaino del ciclista"
date: 2014-06-28
comments: true
tags: [Mario, Lovejoy, Garmin, Hotels.com, MacBookAir, iPhone, iPad]
---
Ben Lovejoy ha fatto vacanza pedalando dodici giorni nel Paesi Bassi, come [ha raccontato su 9to5Mac](http://9to5mac.com/2014/06/23/a-day-in-the-life-of-a-cycle-touring-holiday-with-apple-and-garmin-tech/).<!--more-->

Lo ha fatto su un [triciclo ultratecnologico](http://www.icetrikes.co) e con MacBook Air, iPad e iPhone nel bagaglio.

Il software: [Basecamp di Garmin](http://www.garmin.com/it-IT/shop/downloads/basecamp) [Hotels.com](https://itunes.apple.com/it/app/id284971959?mt=8), [Ride With Gps](http://ridewithgps.com), [BikeRouteToaster](http://bikeroutetoaster.com), [Garmin Connect Mobile](https://itunes.apple.com/it/app/garmin-connect-mobile/id583446403?l=en&mt=8) e poi una custodia per iPhone, una videocamera montata sul triciclo e varie ed eventuali.

Mi sorprende il fatto che sembri una cosa molto *geek*, tanta tecnologia per cose fattibili anche senza, e poi a pensarci ci si rende conto che le foto, le mappe, i posti per dormire, il gusto di farsi un diario magari anche per immagini, non sono pochi gli appassionati di cicloturismo (o di Paesi Bassi) che nella stessa situazione si sarebbero trovati con equipaggiamento equivalente o paragonabile.

I video, le foto, le sovrimpressioni del tragitto compiuto sopra [Google Earth](http://www.google.com/earth/), è tutto bello da vedere e suggestivo da fare venire voglia.

Un bell’esempio di tecnologia che cambia la vita in meglio, che mi ha ricordato la [vacanza di Mario](http://multifinder.wordpress.com/2013/08/12/non-ci-sono-piu-le-vacanze-di-una-volta/) in *tour* per la Germania senza tricicli o Gps, ma con famiglia. Di buon auspicio per tutti quelli che stanno programmando un viaggetto.