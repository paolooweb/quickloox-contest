---
title: "Onda di marea"
date: 2021-01-10
comments: true
tags: [ Signal]
---
Dall’account Twitter di [Signal](https://signal.org), [sappiamo quale app di messaggistica sia in questo momento la più scaricata da App Store](https://twitter.com/signalapp/status/1347693761044701186).

<blockquote class="twitter-tweet"><p lang="en" dir="ltr">Look at what you&#39;ve done. 🇮🇳 <a href="https://t.co/0YuqyZXtgP">pic.twitter.com/0YuqyZXtgP</a></p>&mdash; Signal (@signalapp) <a href="https://twitter.com/signalapp/status/1347693761044701186?ref_src=twsrc%5Etfw">January 8, 2021</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

Se prendi una persona a caso per strada, ne ignora il nome. Sul suo telefono, statisticamente, è assente. Non arriva da una multinazionale, non ne parla (quasi) nessuno, (quasi) nessuno la raccomanda o persino ne parla in un qualunque consesso. Eppure.

Ho aggiornato a iOS 14.3 e iPadOS 14.3 solo ieri (aspetto che il sistema mi avvisi. Nessun problema, per inciso). Sembra un aggiornamento qualunque e neanche impegnativo; ho aggiornato un apparecchio in pausa pranzo, l’altro a pausa cena, come se neanche fosse accaduto.

Eppure. Ora qualcuno può guardare quanti dati personali raccoglie Signal e confrontarlo con quanti ne raccolgono altre app. Questo porta conseguenze.

Io lo scaricherei. Comincerei a parlarne con gli amici, con i genitori delle altre classi, i colleghi, i parenti. Comincerei a raccontare una storia diversa da quella di quanto è comodo WhatsApp.

Forse le persone sono più in ascolto di prima su certi temi. Forse qualcosa sta per cambiare, anche se in piccolo. Forse neanche è piccolo.

Ci ricorderemo di iOS 14.3, non solo perché ricorda vagamente pi greco.
