---
title: "Il sole nei campi"
date: 2013-11-19
comments: true
tags: [Apple]
---
Fa impressione il lungo e meticoloso [articolo](http://gigaom.com/2013/11/18/apple-solar-farm-fuel-cell-farms-exclusive-photos-investigative-report/) dedicato da GigaOm alle fattorie solari di Apple che girano a pieno regime dopo, racconta Katie Fehrenbacher, un ancora più lungo periodo di gestazione in grande segretezza per affrontare il tema dell’indipendenza energetica come altre aziende non hanno fatto.<!--more-->

Perché nessun’altra, Microsoft, Amazon, Google, Facebook o chicchessia ha ancora uguagliato lo sforzo compiuto da Apple in estensione e investimento.

Come al solito, le foto parlano da sole anche senza bisogno di affrontare l’inglese dell’articolo.