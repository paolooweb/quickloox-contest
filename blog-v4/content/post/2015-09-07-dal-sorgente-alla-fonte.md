---
title: "Dal sorgente alla fonte"
date: 2015-09-07
comments: true
tags: [Hack, Menlo, font]
---
I font gratuiti abbondano e sono normalmente di qualità passabile. I font *open source*, esaminabili per vedere come sono fatti e magari per migliorarli, o capirci qualcosa in più, sono pochi rispetto al totale dell’offerta.<!--more-->

Per questo va considerato con attenzione [Hack](http://sourcefoundry.org/hack/), che unisce codice libero a una bella qualità di realizzazione.

Nasce per programmatori che scrivono codice sorgente o per coloro che amano i caratteri monospaziati, inquadrati in una griglia di righe e colonne. È proprio bello e ben leggibile anche in corpi decisamente piccoli. Viene quasi la tentazione di abbandonare Menlo.