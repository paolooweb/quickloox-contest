---
title: "Il migliore"
date: 2014-10-31
comments: true
tags: [Dediu, Asymco, iPadAir2, iPadAir, iPad]
---
A me fa impazzire che tante persone non appassionate e in cerca di un computer mi chiedano quale sia *il migliore*.<!--more-->

Le stesse persone, entrando in un negozio di scarpe, non chiedono le scarpe migliori. Prenotando un albergo, non cercano un albergo migliore. Entrando in un cinema, non comprano l’ingresso per il migliore dei film.

La categoria del *migliore* esiste al più nello sport, fino alla prossima gara. Se un computer fosse migliore degli altri, si venderebbe solo quello.

È che non lo so spiegare.

Internet serve a questo, a farmi imparare. Horace Dediu di Asymco [ha scritto un pezzo](http://www.asymco.com/2014/10/22/the-new-ipad-is-it-better/) per chiedersi se il nuovo iPad Air 2 sia migliore di quello precedente. È quello che si chiede un sacco di gente non appassionata, che cos’abbia in più il prodotto nuovo perché valga la pena dell’acquisto.

È sempre complicato rispondere. La spiegazione del perché sia complicato la dà Dediu. È necessario che qualsiasi miglioramento sia assorbibile, ovvero porti a cose che prima non si potevano fare, o non altrettanto bene. Altrimenti il miglioramento fine a se stesso, che non viene assorbito da alcuna attività, diventa più un danno che un vantaggio. I prodotti *good enough*, buoni abbastanza, devono stare attenti a non introdurre miglioramenti di fatto inutili.

Lettura raccomandatissima.