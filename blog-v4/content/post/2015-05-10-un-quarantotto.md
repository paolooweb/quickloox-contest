---
title: "Un quarantotto"
date: 2015-05-11
comments: true
tags: [Hearthstone, Ithilgalad]
---
Sono un praticante di [Hearthstone](http://eu.battle.net/hearthstone/it/?-) del tutto nella media: poco tempo a disposizione e soldi investiti zero. I risultati sono nella media anche loro.<!--more-->

Però prima o poi capita a chiunque una mano di carte superfortunata (e un avversario in braghe di tela) ed è successo anche a me. *Vanitas vanitatum*.

 ![Un attacco superfortunato](/images/quarantotto.jpg  "Un attacco superfortunato") 

La mia metà della plancia di gioco è quella inferiore. Sul piano di gioco c’è una (mia) pedina che mostra nell’icona un doppio *48*: sono i valori di attacco e difesa. La parte degna di nota è che i valori standard di quella pedina nel gioco sono 1 e 3. Alcune carte consentono di migliorare questi valori e a me è capitata la sequenza migliore che si potesse immaginare.

In alto, il mio avversario dispone di 27 punti vita più cinque aggiuntivi di armatura. Il che vuol dire che i quarantotto punti attacco della mia pedina mi avranno dato una vittoria eclatante pochi secondi dopo avere colto l’attimo nella schermata.

Normalmente la musica è diversa. Serve una vittoria tutto sommato facile? Citofonare *Ithilgalad* (il mio *nickname* nel gioco).