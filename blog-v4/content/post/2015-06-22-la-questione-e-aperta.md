---
title: "La questione è aperta"
date: 2015-06-23
comments: true
tags: [Slashdot, Html, Xml, ePub, LaTeX, Markdown]
---
Ogni tanto su Slashdot viene posta una domanda invece che data una notizia. Qualche tempo fa la domanda era [questa](http://news.slashdot.org/story/15/05/14/1655225/ask-slashdot-what-is-the-best-open-document-format).<!--more-->

>Sto lavorando su un progetto che richiede caricamento e archiviazione di documenti. Sebbene l’applicazione dovrà permettere il caricamento di una pluralità di formati, mi piacerebbe archiviare i documenti in un formato aperto e standard che faciliterà ricerca, compressione, resa grafica eccetera. Che formato di documento aperto è il migliore?

Da tempo sto portando i documenti che produco a formati sempre più semplici e sempre più testuali, a partire da [Html](http://www.w3.org/html/) e [Markdown](http://daringfireball.net/projects/markdown/) (che è di fatto Html in forma svelta).

I miei libri sono diventati oramai contenitori [ePub](http://idpf.org/epub/30) (praticamente Html con una spruzzata di [Xml](http://www.w3.org/XML/)).

Le aree dove ancora va diversamente da così sono quelle che richiedono più competenza di quella che possiedo. Vorrei riuscire a sostituire tanti fogli elettronici con [emacs](http://www.emacswiki.org/emacs/SpreadSheet) o con del buon [JavaScript](http://www.ecmascript.org) che porti le capacità di calcolo necessarie dentro Html, ma tempo e scarsità di conoscenze al momento mi hanno trattenuto. Lo stesso dicasi per le presentazioni.

Se avessi una produzione di tipo diverso, guarderei con grosso favore a [LaTeX](http://www.latex-project.org), ovviamente.

Più vado avanti, più vedo la formattazione come un ostacolo. La vera libertà è avere il formato più testuale possibile. E che sia aperto viene da sé. E poi c’è anche un’altra questione di cui parlerò domani.