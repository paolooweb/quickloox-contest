---
title: "Tutto programmato"
date: 2015-02-09
comments: true
tags: [Dringend, iPad, Linux, virtualizzazione, Lisping, Pythonista, Swift, Objective-C, Dropbox, iOS, Codeanywhere]
---
Anche in riferimento al [*post* di ieri](https://macintelligence.org/posts/2015-02-08-di-vincolarsi/) sull’essere o meno computer di iPad, ho sentito dire in passato che iPad non era un computer perché non si poteva usare per scrivere programmi per iPad.

Motivazione tirata per i capelli, dato che il problema – da tempo – non sono gli strumenti per programmare su iPad. Da [Pythonista](https://itunes.apple.com/it/app/pythonista/id528579881?l=en&mt=8) a [Lisping](https://itunes.apple.com/it/app/lisping/id512138518?l=en&mt=8) a tutto il resto possibile, le occasioni per programmare abbondano.

Con l’entrata in scena del *computing* remoto, della virtualizzazione e del *cloud* la questione è diventata infinitamente più sfumata. Sul mio iPad potrebbe girare una macchina virtuale contenente Linux, operante da un *server* remoto. A quel punto si potrebbe argomentare che il fulcro dell’elaborazione sta altrove e il mio iPad è semplicemente uno strumento di interfaccia. Ugualmente, potrei lanciare un ambiente di programmazione su quel server remoto e dargli ordini da iPad, vedendo il risultato su iPad. Difficile sostenere che io in quel momento non stia programmando.

Forse il punto era poter usare un iPad per scrivere *app* da far girare su iPad. Ok: è uscito [Dringend](https://itunes.apple.com/it/app/dringend-development-environment/id822329054?l=en&mt=8), ambiente di programmazione per lavorare su iPad *a progetti [Xcode](https://developer.apple.com/xcode/)*. Dringend è la strada per aprire, creare, modificare su iPad un progetto di programma Xcode, programmato con [Swift](https://developer.apple.com/swift/) oppure Objective-C, contenente una *app* per iPad (o qualsiasi altro aggeggio iOS).

Bisogna riconoscere che Dringend non è completamente autonomo, c’è il trucco: bisogna avere un Mac che fa da *server* in fase di *build*, un *account* Dropbox utilizzabile per la sincronizzazione con Mac e un *account* da sviluppatore presso Apple.

Però riguardo l’esempio appena formulato, della macchina Linux virtualizzata su iPad, e rispondo *sì, e allora?*. Posso scrivere su iPad una *app* per iPad e compilarla se c’è un Mac in rete. È un problema? Se sì, bisogna rigettare l’idea che esistano situazioni come [Codeanywhere](https://codeanywhere.com), dove si programma addirittura via *browser*. Significa più o meno rigettare il XXI secolo.

iPad è un computer. Ci posso persino programmare per iPad, al prezzo sfasciafamiglie di 9,99 euro.