---
title: "La caduta di re Riccardo"
date: 2019-09-29
comments: true
tags: [Stallman, Gnu, Levy, Gruber, Unix, metoo, Minsky, Epstein, Media, Lab, Minsky, MIT, FSF]
---
[Richard Stallman](https://stallman.org) (o Rms, la sua firma storica) [è in cerca di una stanza](https://www.stallman.org/seeking-housing.html) in cui risiedere per i prossimi mesi, dopo avere dato le dimissioni da [Massachusetts Institute of Technology](https://www.mit.edu) (MIT), [Free Software Foundation](https://www.fsf.org/news/richard-m-stallman-resigns) e [progetto GNU](http://www.gnu.org/).

(Aggiornamento del 30 settembre: Stallman rimane a capo di GNU. la frase con la quale di dimetteva, comparsa sul suo sito personale, era stata inserita a sua insaputa da un dipendente di Free Software Foundation).

La troverà, immagino, solo se il proprietario sarà ignaro delle sue richieste, [di puntigliosità leggendaria](https://groups.google.com/a/mysociety.org/forum/m/#!msg/mysociety-community/zkyZpOXjgoQ/_8xyXSxv9zYJ).

Non sarà una stanza di casa mia, per ragioni geografiche innanzitutto e perché ho constatato il suo, chiamiamolo così, [disinteresse](https://translate.google.com/translate?sl=pt&tl=en&js=n&prev=_t&hl=en&ie=UTF-8&layout=2&eotf=1&u=http%3A%2F%2Fstulzer.net%2Fblog%2F2008%2F03%2F18%2Fa-terrivel-semana-que-richard-stallman-ficou-na-minha-casa%2F&act=url) nei confronti dell’igiene personale.

Ho sempre trovato Stallman un personaggio immediatamente detestabile e ho scritto di come siano ipocrite, ai limiti del fanatismo, certe sue intransigenze verso il computing di grande diffusione.

Non solo il personaggio, è la novità, ma anche la persona: si è dimesso da tutto dopo uno [scambio problematico di email](https://www.theverge.com/2019/9/17/20870050/richard-stallman-resigns-mit-free-software-foundation-epstein) a proposito di un presunto incontro sessuale tra Marvin Minsky, pioniere dell’intelligenza artificiale, e una minorenne coartata da Jeffrey Epstein, milionario morto suicida in carcere dopo essere stato arrestato per una serie infinita di crimini sessuali ai danni di bambine e ragazzine.

Epstein è stato nel contempo un generoso finanziatore del Media Lab del MIT, in cui Stallman si muoveva come *Visiting Professor* e, come da targa scritta di suo pugno sulla porta del proprio ufficio, *Knight for Justice (Also: Hot Ladies)*.

I commenti di Stallman sulla faccenda di Minsky sono stati grossolanamente travisati, ma la miscela di attualità, abitudini, sconsideratezze e idiosincrasie dell’uomo poteva solo esplodere, comunque, in forma dirompente.

Come fondatore del progetto GNU e zelota del Free Software, Richard Stallman finirà sui libri di storia. Chi si esalta per gli studenti che vanno in piazza (dal sessantotto in poi gli studenti hanno solo bisogno di una scusa e non si fanno pregare da alcuno) dovrebbe riflettere su una figura che è riuscita a mobilitare programmatori di tre generazioni su un progetto le cui ricadute influenzano l’intero mondo che conosciamo. Gratis.

Un uomo che ha dato vita a situazioni incredibilmente rivoluzionarie nel suo lavoro, e ha plasmato la realtà globale di oggi, è caduto in disgrazia per le sue opinioni e per il suo stile di vita. C’è un fondo di ironia nella parabola del paladino del software libero, in difficoltà per questioni di libertà di parola.

La si può vedere come la trama di un film, dove prima o poi per il cattivo i nodi vengono al pettine e mai modo di dire è stato più azzeccato nella circostanza: una amica di Steven Levy accettò un invito a pranzo con Stallman ponendo come condizione che lui [arrivasse pettinato](https://www.wired.com/story/richard-stallman-and-the-fall-of-the-clueless-nerd/).

Mi interessa poco quanto tutto questo sia giusto o sbagliato. Un altro modo di dire è che non si possono insegnare nuovi trucchi a un vecchio cane e probabilmente da anni re Riccardo non incideva più sul panorama tecnologico. Forse era solo questione di tempo prima che dovesse cedere il passo; forse questo è stato un ottimo pretesto per accelerare un passaggio di consegne. Qualunque trono si sbriciola solo dopo che le crepe si sono fatte profonde a sufficienza.

Sono curioso di vedere che cosa sarà da domani. Di chi occuperà quegli spazi, con che agenda. Il suo pregio, che sarà ricordato anche come il suo limite, è che aveva un’idea precisa della direzione. Precisa fino al radicalismo cieco, ma chiara.

Umanamente, vorrei che trovasse qualcuno disposto a ospitarlo, anche se toccherà [bruciare le lenzuola dove dorme Rms](http://5by5.tv/talkshow/64-they-had-to-burn-the-sheets). Ha detto molte cose stupide e suppongo praticato comportamenti inappropriati. La sua intelligenza è chiamata a rivedere le une e gli altri; la nostra ad applicare un giudizio il più possibile equo e imparziale.
