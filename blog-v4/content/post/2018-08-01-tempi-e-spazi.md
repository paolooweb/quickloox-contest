---
title: "Tempi e spazi"
date: 2018-08-01
comments: true
tags: [Fintech, Apple, pay, Alibaba]
---
Devo riprendere un vecchio post sul [nuovo Think Different](https://macintelligence.org/posts/2015-03-03-il-mestiere-di-apple/) perché infastidisce e sorprende qualcuno la Apple che annuncia trionfante [nuovi progressi di fatturato nel settore servizi](https://macintelligence.org/posts/2018-07-31-l-annuncio-mancante/) e pensa alla TV invece che pensare a iPhone (per le stesse persone, peraltro, prima pensava a iPhone e trascurava i Mac).

È che rispetto a venti anni fa, i tempi sono diversi e gli spazi pure. Apple, vedi sopra, lavorerà in direzioni dove ritiene di poter dare un contributo; lo farà su poche cose, spesso inevitabili anche se nessuno le ha chieste.

Aggiungo una cosa di mio pugno: Apple lavorerà verso la *disruption*, la sovversione delle regole, nei campi dove questa non è ancora avvenuta. E qui c’è poco da dire: nel reparto informatica/elettronica, ha già dato. Chiaro che le nuove direzioni di sviluppo possano trovarsi altrove.

E allora, se *Quartz* titola [Apple sta diventando una formidabile azienda fintech](https://qz.com/1345500/apple-is-becoming-a-formidable-financial-technology-company-to-rival-paypal-and-square/) (*fintech* fonde *finance* e *technology*, forse potremmo dire *tecnofinanza* in italiano), è sciocco pensare a un tradimento dell’anima originale o a una deriva della dirigenza.

Trentacinque anni fa Macintosh veniva creato nella massima segretezza in una palazzina che batteva bandiera pirata. Oggi, per l’appunto, sono passati trentacinque anni e la stessa azienda è vicina a [capitalizzare il trilione di dollari](https://money.cnn.com/2018/08/01/investing/apple-closer-trillion-dollar-market-value/index.html) (se non lo fa Apple lo farà Amazon e chiunque ci riesca entrerà per una ragione in più nei libri di storia, anche se per prima e per pochissimo tempo ci riuscì anni fa una azienda petrolifera cinese). È come minimo evidente che l’azienda esplorerà nuove direzioni di crescita. E anzi bisogna ringraziare che non si vada verso le lavatrici o le racchette da tennis o le motociclette o gli elettrodomestici come usa in Estremo Oriente.