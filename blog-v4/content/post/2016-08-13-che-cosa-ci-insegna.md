---
title: "Che cosa ci insegna"
date: 2016-08-13
comments: true
tags: [Panic, Portland]
---
Giusto [ieri](https://macintelligence.org/posts/2016-08-12-un-negozio-da-sviluppare/) facevo il nome di Panic come software house dotata di talento e reputazione più che adeguati per meritare più elasticità nel momento in si affaccia su Apple Store.<!--more-->

Servisse una prova, hanno messo a punto un’insegna per il loro palazzo a Portland, di cui qualunque passante munito della giusta *app* [può modificare il colore](https://panic.com/blog/the-panic-sign/)!

Per i curiosi senza remissione c’è anche tutta la spiegazione tecnica.

Ma a me colpiscono l’attitudine, la capacità di autoironia, il gusto di fare il proprio lavoro, la libertà di interpretare le regole. Mi chiedo che cosa sarebbe necessario in Italia per autorizzare una cosa del genere.
