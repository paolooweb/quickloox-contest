---
title: "I nutrienti per le ossa"
date: 2023-09-18T16:08:00+01:00
draft: false
toc: false
comments: true
categories: [Hardware]
tags: [NextStep]
---
Trentaquattro anni fa [usciva NextStep](https://twitter.com/DayTechHistory/status/1571469093588197376), il sistema operativo per i computer NeXT, voluti da Steve Jobs quando venne estromesso da Apple e creò una nuova azienda per spingere il proprio sogno.

<blockquote class="twitter-tweet"><p lang="en" dir="ltr">On this day in 1989, NeXTSTEP 1.0 was released. <a href="https://t.co/GWuVi8qf1x">pic.twitter.com/GWuVi8qf1x</a></p>&mdash; Today in Tech History (@DayTechHistory) <a href="https://twitter.com/DayTechHistory/status/1571469093588197376?ref_src=twsrc%5Etfw">September 18, 2022</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

I computer NeXT non andarono da nessuna parte e oggi sono buoni giusto per le rassegne di retrocomputing. Il software che lo animava è invece andato avanti e gli dobbiamo gran parte del presente e financo del futuro dei nostri Mac, per non parlare di tutti gli altri apparecchi.

Senza software, il più potente degli hardware è uno scheletro inanimato. Senza hardware, il software è una fila di istruzioni senza scopo. Che però può evolversi e viaggiare nel futuro; che dà vita e capacità a quello che ci troviamo in mano o sul tavolo. È una soluzione fisiologica che trasporta nutrienti a tutti i componenti del sistema.

Viva NextStep.