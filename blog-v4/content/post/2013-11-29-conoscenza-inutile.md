---
title: "Conoscenza inutile"
date: 2013-11-29
comments: true
tags: [Terminale, Unix]
---
*Mac OS X Hints* continua a costituire una risorsa straordinaria; stavolta ha spiegato come impostare scorciatoie di tastiera personalizzate… [da Terminale](http://hints.macworld.com/article.php?story=20131123074223584).<!--more-->

Niente di più inutile. Per inserire una scorciatoia si fa molto prima nelle Preferenze di Sistema. La sintassi Unix oltretutto è a dire poco criptica se non la si installa solidamente nella memoria a lungo termine. Chi si ricorda che *@* vuol dire *Comando*, *$* significa *Maiuscole*, *~* sta per *Opzione* e con *^* si intende *Control*?

(Lo ricorda Mac OS X Hints, certo, ma il discorso era un altro.)

Eppure mi viene in mente chi volesse trasferire su Mac un insieme di personalizzazioni complesso e collaudato, costruito con l’esperienza  e nel tempo su un altro Mac. Dalle Preferenze di Sistema può solo reinserire pazientemente tutte le scorciatoie. Con il Terminale potrebbe farne un piccolo *script*, o anche semplicemente un comando lungo separando le singole istruzioni con il punto e virgola.

Conoscenza inutile, sì, fino a quando le condizioni di utilizzo restano semplici.