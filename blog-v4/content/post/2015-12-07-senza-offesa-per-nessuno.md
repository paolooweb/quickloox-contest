---
title: "Senza offesa per nessuno"
date: 2015-12-07
comments: true
tags: [open, source, Apple, Sun, Ibm, Microsoft, Linux, Khtml, Safari, WebKit, OSX, El, Capitan, MkLnux, Red, Hat, Windows, Hewlett-Packard]
---
Apple ha cambiato uno slogan sull’*open source* su una delle proprie pagine per gli sviluppatori e mi sento un po’ offeso. Il testo diceva così, inglese originale e mia traduzione in italiano:<!--more-->

>Apple, the first major computer company to make Open Source development a key part of its software strategy, continues to use and release significant quantities of open source software.

>Apple, la prima grande azienda di computer a fare dello sviluppo Open Source una parte chiave della propria strategia software, continua a usare e divulgare quantità significative di software open source.

Sono venute fuori polemiche abbastanza dense su Internet, tanto che ora il testo è cambiato come lo si può vedere sulla [pagina di competenza](https://developer.apple.com/opensource/).

Interessante è il perché delle polemiche. In giro si legge che Apple non ha sempre avuto un rapporto pulito con l’*open source* ed è vero. Per esempio, già dai tempi della nascita di Safari, Apple tratteneva in casa per mesi i miglioramenti a [WebKit](https://webkit.org) prima di passarli, come doveva, al progetto progenitore [Khtml](https://en.wikipedia.org/wiki/KHTML). Per giunta i cambiamenti arrivavano tutti insieme in numero enorme e chiaramente questo causava problemi grossi ai programmatori.

Succede anche oggi: [manca tuttora all’appello](http://opensource.apple.com) la versione libera di OS X 10.11 El Capitan, uscito da oltre due mesi **[aggiornamento dell’8 dicembre: [è arrivato](https://opensource.apple.com/release/os-x-1011/)]**. Poi ci sono questioni di licenza: l’*open source* di Apple non ha la stessa licenza di utilizzo e distribuzione di Linux, per dire, e questo nella comunità *open* è sufficiente a scatenare discussioni accese.

Il problema è che si possono trovare molte macchie, e grosse, nella condotta storica di Apple in materia di software libero. Ma **niente che abbia attinenza con la frase incriminata**.

Apple ha iniziato ufficialmente a fare qualcosa con l’*open source* nel 1996, anno di [MkLinux](http://www.mklinux.org). Certo non era una *parte chiave della strategia*; lo è diventato nel 2000, con l’arrivo di Mac OS X e il suo nucleo profondo interamente fatto di software *open source*, senza il quale Mac OS X semplicemente non sarebbe esistito e i Mac oggi sarebbero forse meglio, forse peggio, sicuramente un’altra cosa.

Quali altre *grandi aziende di computer* hanno preceduto Apple con l’*open source* come *parte chiave della strategia*? Naturalmente qui ci troviamo davanti all’ambiguità. A che punto un’azienda diventi *grande* non è definito in un manuale o in uno standard. La Microsoft del 2000, che vendeva solo e unicamente software a parte qualche mouse e qualche tastiera, era una *computer company* oppure no?

Sarebbe importante accordarsi, perché nel 1993 nasceva [Red Hat](https://www.redhat.com/it), con la scommessa di mettere insieme pranzo e cena grazie al software libero, molto prima di Apple. E Red Hat non produce computer ma solo software, come faceva Microsoft. Nel 1993 però Apple era [un’azienda da otto miliardi di dollari](https://www.zoho.com/general/blog/apples-revenue-history.html); Red Hat, nel 2015, dopo ventidue anni di operatività e continuo successo, deve ancora remare prima di arrivare a due miliardi. Certamente operano su grandezze diverse: nel 2006 [Red Hat fatturava](http://investors.redhat.com/financials-statements.cfm) 278 *milioni* di dollari, Apple diciannove *miliardi*. Non possono essere *grandi aziende* allo stesso modo.

Microsoft? Neanche per idea. Ultimamente l’azienda ha sbalordito tutti con una serie di annunci legati all’*open source*, ma tutto può essere, tranne che la prima.

Forse Ibm, che ha investito molto su Linux a partire dal 1994? C’è un problema: nel 2004 l’azienda [ha venduto il ramo di attività dei personal computer a Lenovo](http://www.cnet.com/news/ibm-sells-pc-group-to-lenovo/) e da allora ha smesso di essere una *computer company*, se vale il requisito di produrli in quantità rilevanti. Più di questo, *oggi* Linux è strategico per Ibm. Negli anni novanta l’azienda vendeva vagonate di PC con Windows, dopo il tentativo di varare un sistema operativo proprio, e l’*open source* non era affatto primario.

Quello che vale per Ibm vale anche per Hewlett-Packard: supporto a Linux da molto presto, ma niente che fosse strategico. Ciò che vale per Microsoft vale per Oracle, che non produce computer. Il gioco può proseguire puntando su aziende sempre più piccole. [Sun Microsystems](https://it.wikipedia.org/wiki/Sun_Microsystems) meriterebbe forse un esame più approfondito, se non altro per curiosità: certamente però potrebbe essere messo in discussione il suo effettivo ruolo di *major computer company* e credo si troverà qualche problema sui tempi.

Senza cercare di riscrivere la storia dell’informatica in questa pagina, si può tranquillamente concludere che lo slogan di Apple è discutibile (può essere messo in discussione), perché i criteri per arrivare a un giudizio inappellabile sono soggettivi o da definire. Al tempo stesso, l’affermazione **gode come minimo del beneficio del dubbio**, dato che contestarla su basi inoppugnabili ed evidenti è impossibile.

Perché allora la frase è stata cambiata? Perché Apple ha mentito sul proprio ruolo nei confronti dell’*open source*? No. Perché qualcuno si è offeso, ha sparato una ventilata di escrementi su Facebook e centinaia o migliaia di persone hanno iniziato a fare clic su *mi piace*. Commentando che iPhone costa troppo, che chi usa Mac è un ritardato, che Apple dice di essere la prima su tutto e non lo è mai.

Tutto questo potrebbe benissimo essere vero. Solo, ha zero attinenza con la frase contestata.

Siamo entrati in tempi tristissimi, dove una cosa non deve essere vera o falsa, ma piacere o non piacere. E c’è una brutta differenza. Sono molto offeso che la gente giudichi un’affermazione sull’*open source* a partire dal prezzo di iPhone. Posso chiedere la modifica di un commentino, anche se fosse sacrosanto e veritiero più del sorgere del Sole la mattina?