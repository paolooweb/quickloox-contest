---
title: "Quel tempo dei calendari"
date: 2015-12-06
comments: true
tags: [Perl, Avvento, Monument, Valley]
---
Sono un po’ fissato con i calendari dell’Avvento. Non ne colleziono, non ne studio la storia, non li analizzo; se però ne vedo uno, è quasi inevitabile che me lo porti dietro fino a Natale (o san Silvestro, dato che per molti è solo un pretesto e osservano la durata del mese più che quella del tempo liturgico).

Ecco, sono saltati fuori un calendario dell’Avvento per [Perl 6](https://perl6advent.wordpress.com) (versione del linguaggio di prossima uscita) e anche per la [versione 5](http://perladvent.org/2015/index.html), al momento quella ufficiale.

Di fatto sono raccolte di trucchi e consigli di programmazione, organizzate appunto con il filo conduttore del calendario. E una cosa che permetta di imparare qualcosina di programmazione, una volta al giorno, rapidamente, mi piace ancora di più. Quando uno è fissato, è fissato.

P.S.: c’entra niente, ma credo siano le ultimissime ore per acquisire gratis su App Store [Monument Valley](http://www.monumentvalleygame.com) in quanto *app* gratuita della settimana. Imperdibile. Se poi toccasse pagarlo, beh, sono ben spesi.