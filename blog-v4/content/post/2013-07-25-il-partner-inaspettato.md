---
title: "Il partner inaspettato"
date: 2013-07-25
comments: true
tags: [Apple, iOS, Giochi]
---
**Fearandil** ricordava con ragione [giusto l’altroieri](https://macintelligence.org/posts/2013-07-23-i-tempi-del-software/) che i giochi escono prima su *console* e su Windows.

Dovremo però confrontarci con un mondo che continua a cambiare. Dall’[intervista di GamesBeat a Frank Gibeau](http://venturebeat.com/2013/07/23/eas-frank-gibeau-on-interview-part-1/), alto dirigente di Electronic Arts, uno dei colossi del gioco elettronico, sui risultati trimestrali:

>Per la prima volta nella storia di Electronic Arts, Apple è stata il nostro maggiore partner di vendita.

Niente GameStop, niente Media World, niente Amazon. App Store.<!--more-->

Gibeau stesso ammette che si sia trattato di un trimestre anomalo per la mancanza di *blockbuster*, titoli di cassetta, in uscita per le *console*. Nondimeno, cinque anni fa questa sarebbe stata fantascienza.

Quanto a Windows, la questione è semplice. Benedict Evans [ha mostrato](http://ben-evans.com/benedictevans/2013/7/20/the-irrelevance-of-microsoft) come nel 2009 fossero Windows nove apparecchi su dieci di quelli capaci di collegarsi a Internet.

Adesso sono meno di un quarto.

>Microsoft può riuscire a trasformare tavolette e telefoni Windows in prodotti con una quota di mercato significativa. Ma non sarà mai più dominante.

E forse anche i videogiochi non usciranno più su Windows con tutta questa fretta.