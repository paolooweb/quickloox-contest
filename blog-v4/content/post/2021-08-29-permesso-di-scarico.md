---
title: "Permesso di scarico"
date: 2021-08-29T02:30:00+01:00
draft: false
toc: false
comments: false
categories:
tags: [tv, Lidia, Apple Arcade, tv] 
---
Rientrati a casa la sera dopo una cena da amici, Lidia ha chiesto *posso giocare un po’ sulla TV?*, che nel nostro caso significa usare tramite tv i giochi disponibili su Apple Arcade.

Abbiamo dato l’assenso e dopo pochi istanti Lidia ha chiesto *Posso scaricare un gioco da Apple Arcade?*

Il papà ha subito risposto *certo*; per mantenere una parvenza di autorità ha comunque visionato il gioco da scaricare, ma la verità è che con Apple Arcade il peggio che può accadere è suddiviso in due categorie:

- giochi che richiedono un controller (non lo abbiamo);
- giochi 12+ ancora un po’ troppo complessi oppure a rischio di contenuti più forti.

Tutte le altre perplessità le abbiamo eliminate, dietro pedaggio ad Apple di cinque euro al mese. Che sono tra i denari meglio spesi ultimamente; niente questioni di acquisti in-app, niente pubblicità, niente tracciamenti indebiti, niente catene di giochini fatti in serie che si promuovono l’un altro, niente.

Sarà pure un *walled garden* ma appunto, quando hai non più di dieci anni/figlia complessivi, se per una volta vogliono fare da sole, lo possono fare in condizioni di tranquillità parentale. Usciremo dal giardino dorato ovviamente, ma a tempo debito. La nuova tv, a distanza di tempo dall’acquisto, mi piace sempre di più.

*Per ora si può lasciare un commento dalla [pagina apposita di Muut per QuickLoox](https://muut.com/quickloox#!/feed). I commenti torneranno disponibili in calce ai post appena possibile.*