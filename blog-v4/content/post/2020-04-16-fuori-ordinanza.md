---
title: "Fuori ordinanza"
date: 2020-04-16
comments: true
tags: [Aaa, All, About, Apple, Nicoletti, Cervelli, Ribelli]
---
Anche se il museo All About Apple è chiuso causa coronavirus, è sempre possibile [visitarlo virtualmente](https://www.allaboutapple.com/virtualtour/). Chi non sia mai passato da Savona a fare un giro dovrebbe emendarsi almano via browser, per capire quanto valga l’iniziativa e specialmente in Italia, terra dove di cultura trabocchiamo ma appena si affaccia la tecnologia riusciamo a tornare indietro di decenni.

Il problema è che la visita virtuale è gratis e, senza staccare biglietti, il museo – che è una Onlus di volontari – rischia grosso. I costi incidono e i ricavi latiteranno per chissà quanto ancora.

C’è modo di [dare una mano proprio oggi](https://www.allaboutapple.com/2020/04/fuori-dallordinarioaaa-e-lasta-dei-cervelli-ribelli/): i ragazzi di All About Apple partecipano all’asta online [Cervelli Ribelli - Out of Ordinary](https://www.cambiaste.com/it/asta-0497/out-of-ordinary.asp?action=reset) con nove pezzi unici, fuori dall’ordinario proprio come è nello spirito dell’iniziativa, [lanciata dal giornalista Gianluca Nicoletti](https://asta.cervelliribelli.it).

Parte dei proventi di Cervelli Ribelli sarà proprio destinata al mantenimento e al miglioramento del museo All About Apple anche durante tempi complicati.

È raro che qui si parli di denaro e di solidarietà, esattamente perché quelle poche volte che accade deve funzionare alla grandissima. Consiglio di leggere tutto prima di cimentarsi e poi di cimentarsi, in nome di un pezzo di storia e cultura contemporanea che conta. E del fatto che possiamo stare a casa, ma lo spirito sa volare alto quando occorre.