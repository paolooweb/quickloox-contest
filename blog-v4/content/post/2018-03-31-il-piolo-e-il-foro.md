---
title: "Il piolo e il foro"
date: 2018-03-31
comments: true
tags: [Milanesi, Jobs, iPad, scuola]
---
Per una volta una cosa buona che arriva da un’analista, per la precisione Carolina Milanesi che scrive [Apple rinnova il suo amore per l’istruzione e non per il mercato dell’istruzione](https://techpinions.com/apple-renews-its-love-for-education-not-the-education-market/52562).<!--more-->

Essendo analista, perde un sacco di tempo a scrivere prima di accorgersi che aveva in mano una citazione di Steve Jobs perfetta per il suo caso. La si perdona perché fornisce la citazione:

>Quando hai figli ti viene da pensare che cosa vorresti imparassero. La maggior parte di quello che studiano a scuola è completamente inutile. Ma potrebbero imparare da giovani alcune cose di valore incredibile che si imparano quando si invecchia. E ti viene da pensare a che cosa faresti se decidessi di dedicarti all’insegnamento. Sarebbe interessantissimo! Ma oggi non lo puoi fare. Saresti un pazzo a lavorare a scuola, oggi. Non hai il permesso di fare quello che pensi meglio. Non puoi sceglierti i libri, il percorso didattico. Ti è concesso di insegnare una materia specifica. Chi vorrebbe mai farlo?

Si sente l’odore di zolfo, vero? La messa in discussione di concetti che sembrano immutabili e lo sono abbastanza, perché Jobs parlava nel 1996 e da allora nulla è cambiato. *Abbiamo sempre fatto così*.

Allora Apple parlava di *Think Different*, il piolo tondo nel buco quadrato, quelli che non accettano lo status quo eccetera. Siamo ancora qui ed è una buona cosa. Sull’istruzione, come dimostra l’[evento di Chicago](https://macintelligence.org/posts/2018-03-28-solo-per-amore/), Apple si ostina a voler cambiare per davvero le cose invece di accettare supinamente una realtà che è diventata anacronistica e non va bene, anche se ci potrebbero vendere computer per fare denaro. La scuola, dopo quello che è accaduto negli ultimi vent’anni, deve cambiare e tantissimo, o diventare progressivamente sempre più inutile. Altro che il sessantotto.

Apple vuole fare denaro nell’istruzione, ma per rivoltarla come un calzino. Guadagnerebbe molto di più se accettasse l’andazzo generale e buttasse lì un MacBook a prezzo scontato. E questo mi fa pensare bene sia dell’evento, sia delle sue intenzioni.