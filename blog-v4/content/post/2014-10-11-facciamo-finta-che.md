---
title: "Facciamo finta che"
date: 2014-10-11
comments: true
tags: [Octopress, AppleScript, Dropbox, Mac, iPad, Terminale, Unix]
---
[Octopress](http://octopress.org) – il motore di questo blog – si basa su una cartella che risiede nel Mac. La cartella contiene sottocartelle varie che ospitano i pezzi del blog.<!--more-->

Pubblicare questo articolo significa creare un file e metterlo nella giusta cartella, per poi dare un comando di Terminale che genera una edizione aggiornata del blog e un altro comando di Terminale che deposita una copia dell’edizione dentro il server di pubblicazione.

Spesso mi trovo a scrivere pezzi per il blog su iPad, lontano da Mac. Facciamo finta che il Mac rimanga irraggiungibile fisicamente per tutta la giornata. Qual è la strategia che richiede meno lavoro in assoluto per pubblicare il pezzo?

Finora la mia soluzione è una Azione Cartella che sta dentro la cartella Dropbox. Da iPad mando il file nell’Azione Cartella. Alla quale è appiccicato un AppleScript che fa tre cose:

* sposta il file nella giusta cartella di Octopress;
* impartisce il comando di Terminale che genera il blog aggiornato;
* impartisce il comando di Terminale che pubblica il blog.

In questo modo ciò che mi viene richiesto a livello pratico è mettere il pezzo in Dropbox. A livello di predisposizione, realizzare l’AppleScript è semplice.

(Questo sistema è poco sicuro; lo so.)

C’è un modo che richieda ancora meno lavoro?