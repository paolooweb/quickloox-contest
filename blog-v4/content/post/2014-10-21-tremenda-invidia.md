---
title: "Tremenda invidia"
date: 2014-10-21
comments: true
tags: [ZDNet, Apple, Mac, Windows]
---
Cinque milioni e mezzo, non si erano mai venduti [così tanti Mac in un trimestre](http://www.apple.com/it/pr/library/2014/10/20Apple-Reports-Fourth-Quarter-Results.html). L’anno scorso nello stesso periodo erano stati quattro milioni e mezzo, ovvero la crescita è uno schiaffo al mercato generale dei computer che nel complesso le stime danno in lieve flessione. Se si stralciasse la posizione dei Mac, sarebbe una mezza Caporetto del personal.<!--more-->

A *ZDNet*, antiacido per tutti. Da sempre feudo di Windows, devono dare [la notizia](http://www.zdnet.com/apples-mac-surges-on-back-to-school-emerging-markets-7000034886/) che però brucia e parecchio. Ed ecco il loro occhiello:

>La domanda più grossa è se Apple possa mantenere l’andamento positivo di Mac.

La risposta che si danno è *sì, se fanno aggiornamenti*, talmente deficiente che si capisce quanto fosse artefatta la domanda. Non possono negare la realtà e neanche nasconderla, così buttano lì il dubbio, dài che magari non ce la fanno.

C’è gente secondo la quale Apple sarebbe una religione. C’è gente per cui rappresenta l’inferno sulla terra, abituati come sono al mediocre. In che girone dell’informatica stanno gli invidiosi?