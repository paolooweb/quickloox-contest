---
title: "Realtà accelerata"
date: 2017-07-01
comments: true
tags: [ARKit, Wwdc, Verge, MadeWithArKit, 9to5Mac]
---
Neanche un mese dopo [Wwdc](https://www.apple.com/apple-events/june-2017/) si iniziano a vedere in rete numerosi esempi di realtà aumentata in ambito iOS.

*9to5Mac* ha messo insieme una [raccolta di filmati](https://9to5mac.com/2017/06/26/the-best-of-arkit-falcon-9-landing-minecraft-van-gogh-bedroom-tour-and-more-videos/) veramente da vedere.

*The Verge*, sospettabile di tutto tranne che di tifo o collusioni con Apple, titola [La realtà aumentata di Apple è più vicina alla realtà di quella di Google](https://www.theverge.com/2017/6/26/15872332/apple-arkit-ios-11-augmented-reality-developer-excitement) e scrive:

>Apple è ben lungi dal potersi vantare di avere inventato la realtà aumentata (AR), ma il suo nuovo ARkit in iOS 11 sta già dando segnali che suggeriscono come l’azienda potrebbe portare l’AR nell’uso comune meglio e più rapidamente di chiunque altro.

È inoltre nato un canale Twitter [@MadeWithARKit](https://twitter.com/madewithARKit) con tanto di newsletter settimanale e tutt’altro che vuoto.

Tutto questo considerando che l’annuncio di ARKit è del 5 giugno e che iOS 11 definitivo neanche esiste: sono sviluppatori al lavoro su beta instabili, fallibili e inaffidabili. Eppure i video danno l’impressione di un’architettura già interessante, in movimento accelerato rispetto alla concorrenza.

Vuoi vedere che è stato fatto un buon lavoro di concezione, progettazione e programmazione?

(Qui sotto, [un missile Falcon 9 atterra nella piscina della casa dello sviluppatore](https://www.youtube.com/watch?v=NodGjd3C0SQ)).

<iframe width="560" height="315" src="https://www.youtube.com/embed/NodGjd3C0SQ" frameborder="0" allowfullscreen></iframe>