---
title: "Un gioco Unix al mondo"
date: 2022-10-17T01:23:17+01:00
draft: false
toc: false
comments: true
categories: [Web]
tags: [Unix Pipe Card Game, tail, head, cat, grep, wc, sort, uniq]
---
Il gioco è veramente tale, funziona, un intraprendente potrebbe anche stampare le carte su carta bella, plastificare eccetera.

Credo che potrei permettermi di metterlo in tavola con la giusta formazione di amici.

Per la famiglia, ci sono ancora molte *pipe* che vanno spiegate bene. Al momento la situazione consiglia un rinvio.

Ciononostante, un bookmark allo [Unix Pipe Card Game](https://punkx.org/unix-pipe-game/) lo faccio di sicuro.

Anzi, mi scarico tutto. Perché, come da istruzioni, potrei governare il gioco, avendo conoscenza di `cat`, `grep`, `tail`, `head`, `wc`, `sort`, `uniq`. Però ho da migliorare quanto a approfondimento.