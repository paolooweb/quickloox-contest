---
title: "Mappe: un aggiornamento sugli aggiornamenti"
date: 2017-05-30
comments: true
tags: [Mappe, Apple, Google]
---
[Avevo segnalato](https://macintelligence.org/posts/2017-01-12-giochi-di-carte/) un eccezionale, articolato, dettagliatissimo confronto tra le mappe di Apple e quelle di Google. L’articolo [è stato aggiornato](https://www.justinobeirne.com/a-year-of-google-maps-and-apple-maps/) dopo un anno dalla compilazione dell’originale.<!--more-->

La lettura è lunghissima, solo per chi voglia davvero capirne di più e farsi un’idea seria dei due sistemi e della loro evoluzioni.

Sono software così complessi che è difficile definire un migliore in assoluto: su certe cose sembra più dinamica Google, su altre Apple, complessivamente sembra esserci uno spostamento più favorevole a Google ma c’è tanto da leggere e da mettere a verifica. Anche perché, a livello planetario, prevedere il comportamento di questa o quella mappa è impossibile e [vale l’esperienza](https://macintelligence.org/posts/2017-01-04-mappe-scattanti/).

Felici comunque di avere almeno due sistemi sempre migliori per non sbagliare mai strada.