---
title: "Imparare la magia"
date: 2023-09-06T16:34:09+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [Unix, Dr. Drang, Drang, John D. Cook, Cook, awk]
---
Una piccola [polemica](https://leancrew.com/all-this/2023/08/tools-small-and-large/) tra John D. Cook e Dr. Drang mi ha portato alla scoperta di una [paginetta magica](https://www.geeksforgeeks.org/awk-command-unixlinux-examples/) dove si insegnano le virtù di [awk](https://www.gnu.org/software/gawk/manual/gawk.html).

Magica perché come vorrei vedere sempre spiegato Unix: esempi semplici e chiari, spiegati da un umano e non mediante una processione infinita di parametri dal significato semiincomprensibile, la possibilità di seguire gli esempi veramente da subito, sicuramente senza diventare guru di `awk`, certamente arrivando subito a risolvere qualche problema. Mi sono reso conto che quest’estate avrebbe potuto essermi utile in almeno un paio di occasioni dove me la sono cavata con qualche espressione regolare, in modo definitivamente meno elegante di quanto sarebbe stato possibile con `awk`.

In contrasto, la guida ufficiale di Gnu al programma (linkata più sopra) è praticamente un libro. Ci vuole assolutamente perché la completezza è il dono dei veri maghi, però ogni tanto anche qualche briciola più maneggevole per gli apprendisti si lascia apprezzare.