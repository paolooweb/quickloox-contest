---
title: "Il programma tuttofare"
date: 2015-05-08
comments: true
tags: [OSX, Hockenberry, Terminale]
---
Raccomando vivamente la lettura faticosa e lenta dell’[articolessa](http://furbo.org/2014/09/03/the-terminal/) di Craig Hockenberry a tema Terminale di OS X.<!--more-->

Perché non è una guida *per negati* e insegna un sacco di cose, ma non ha un tono saccente e noiosamente didattico.

Perché è una raccolta di trucchi, ma non è un elenco fine a se stesso.

Perché la lunga trattazione comprende un sacco di tecniche e comandi che risolvono problemi quotidiani su Mac, per i quali ho letto e letto infinite richieste di un programma che risolva tutto.

Beh, quel programma è il Terminale. Ostico all’inizio, ma quando ti accorgi della potenza che hai sotto le mani e quanto poco ci vuole a scatenarla a fin di bene, vien voglia di estenderne l’uso più che si può. A partire da questo articolo (uno dei pochissimi di cui si possa dire), la vita su Mac potrebbe cambiare, molto, in meglio. Se c’è un dubbio, chiedere: è una occasione da cogliere.