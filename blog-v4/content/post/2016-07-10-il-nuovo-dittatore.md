---
title: "Il nuovo dittatore"
date: 2016-07-10
comments: true
tags: [Alitalia, Chrome, Safari]
---
Ricevo, e con gratitudine pubblico, da **Fabio**. Da **grassetto** a **grassetto**.

**Ho provato a prenotare un volo dal sito Alitalia**, ma al momento di selezionare il volo di ritorno l’operazione non era più possibile, perché non c’erano posti disponibili. 

Chiamato il call center per verificare la disponibilità di posti (tutti disponibili), ho successivamente chiamato l’apposito servizio per segnalare il malfunzionamento. L’operatore telefonico mi ha detto che la funzionalità è garantita solo con Chrome, mentre con Safari “potrebbero” generarsi delle anomalie. Incalzato sul fatto che, a fronte di una procedura standard, le anomalie non possono essere random e, nello specifico,  si ripetono costantemente, la risposta è stata che il sito permette le prenotazioni solo con Chrome e per tale ragione installare tale browser su tutti i computer dell’azienda.

Mi sembra ottimo.

Confido che nell’era del web 3, 4, 5, 6, 7 punto zero sia possibile accedere al e utilizzare il sito anche da chi non vuole Chrome **(dopo Explorer, la nuova dittatura?)**

Divise nuove per Alitalia, ma forse era meglio rinnovare anche i tecnici.

*[Problemi di specificità su Mac o iOS con i browser o altri accessori, per esempio la Carta regionale dei servizi? [Cuore di Mela](https://macintelligence.org/posts/2016-06-15-un-kickstarter-per-cambiare-il-libro/) prevede un capitolo apposito. [E conta sulla fiducia dei lettori per renderlo realtà](https://www.kickstarter.com/projects/295108589/cuore-di-mela-i-segreti-di-apple-iphone-e-mac).]*
