---
title: "Grandi temi, piccole vite"
date: 2023-07-17T13:09:13+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [Freeciv, Gpl, Red Hat, Red Hat Enteprise Linux, RHEL]
---
C’è conflitto, certo, tra i grandi temi e le nostre piccole vite. È plausibile che [i rivolgimenti tra le multinazionali dell’open source di cui raccontavo ieri](https://macintelligence.org/posts/2023-07-16-superior-stabat-red-hat/) abbiano realmente una qualche propagazione che arriva a toccare il nostro quotidiano?

Senza scomodare [teorie del caos](https://www.andreaminini.org/fisca/teoria-del-caos), farfalle e uragani vari, sono convinto di sì e, anzi, che nel nostro digitale sia ancora più vero che in molte altre attività umane.

Perché rotolo a valle in basso, giù, per infiniti gradini e arrivo all’annuncio di [un nuovo sito dove poter giocare online a Freeciv con il browser](https://www.fciv.net).

È una notizia anche se esiste già [un modo meno nuovo di farlo](https://www.freecivweb.org). E pure se sono riuscito a [seguire le istruzioni per farlo partire su Mac](https://macintelligence.org/posts/2023-04-18-marcia-indietro-prima-avanti-di-nuovo/).

È una notizia perché a partire dallo stesso codice, libero e gratuito per chiunque, *qualcuno ha deciso di offrire un modo in più per giocare* a Freeciv.

Red Hat, creatore di software che manda avanti aziende da centomila persone, pasticcia con la licenza del suo Linux a scopo di lucro o di strategia di mercato.

Freeciv interessa a me e a un gruppetto di appassionati per trastullarsi qualche mezz’ora libera.

Red Hat Enterprise Linux e Freeciv *hanno la stessa licenza*. Se passasse l’idea di fare trucchi per rendere il software libero meno aperto, qualcuno potrebbe pensare di togliermi Freeciv. Meglio avere più modi per giocarci.

Cambia niente, cosa vuoi che sia. Questo sì se non si ha idea di tutti i passaggi intermedi che ci sono nel mondo del software per arrivare da Red Hat Enterprise Linux a Freeciv o viceversa. La licenza GPL copre milioni di programmi.

Certi grandi temi toccano e farebbero meglio a sensibilizzare anche le piccole vite.