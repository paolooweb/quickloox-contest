---
title: "Ricerca e ritrova"
date: 2016-04-24
comments: true
tags: [Apple, R&D, Alphabet, Microsoft, Google, Cml]
---
Da dopodomani sera inizieranno le profezie lugubri sul futuro di Apple per il fatto che per la prima volta si noterà un declino anno su anno delle vendite di iPhone.<!--more-->

A nulla varrà fare notare che il confronto avviene con il debutto di iPhone 6 e 6 Plus, che hanno avuto un successo oltre ogni rosea previsione. Inutile sarà fare presente che esiste un contesto di mercato e finanza tale che [Microsoft ha visto fatturato e utili in calo](http://www.marketwatch.com/story/microsoft-shares-slump-as-earnings-miss-wall-streets-estimates-2016-04-21). Superfluo risulterà indicare la scarsa performance finanziaria di Alphabet (Google e dintorni), che [ha incassato meno utili del previsto](http://www.usatoday.com/story/tech/news/2016/04/21/alphabet-google-first-quarter-earnings/83349246/).

Essendo futile perdere tempo con i lobotomizzati, suggerisco invece di ritrovare un attimo di relax e compulsare l’articolo di Capital Market Laboratories [Questa è la crescita futura di Apple in quattro grafici sconvolgenti](http://ophirgottlieb.tumblr.com/post/143226171059/this-is-apples-future-growth-in-four-stunning).

Niente *spoiler*. Dico solo che uno dei grafici riguarda la curva degli stanziamenti in ricerca e sviluppo (per qualcuno [sempre insufficienti](http://www.fool.com/investing/general/2015/12/01/apple-inc-rd-too-little-or-just-right.aspx), chiaramente a scatola chiusa).