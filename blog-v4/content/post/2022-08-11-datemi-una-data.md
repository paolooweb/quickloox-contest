---
title: "Datemi una data"
date: 2022-08-11T02:16:52+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [Comandi rapidi, Shortcuts]
---
Ho deciso di preparare un Comando rapido che su iPad formatta la data e l’ora del momento per incollare tutto nell’intestazione dei post. Il formato previsto da [Hugo](https://gohugo.io), il motore, è *2022-08-11T02:16:52+01:00*. Ho imparato alcune cose, piccolissime, ma le ho imparate.

Il formato *anno-mese-giorno* non è contemplato tra quelli preimpostati nei Comandi rapidi e va specificato a mano. Però va scritto nella forma *yyyy-MM-dd*, con le emme maiuscole, perché se sono minuscole appare un numero di mese senza senso, almeno in apparenza. Studiando verrà fuori il motivo ma, come dice re Aragorn nel film Il Ritorno del Re, [non è questo il giorno](https://nospoiler.it/posts/il-signore-degli-anelli-torna-al-cinema-dove-e-quando).

Per infilare la *T* tra data e orario occorre delimitarla con apici singoli. Quelli doppi non vanno bene e l’assenza di delimitatori non funziona. Invece l’aggiunta di *+01:00* alla fine non ha bisogno di delimitatori. Probabilmente il software non effettua controlli a fine riga e accetta liberalmente tutto quello che trova in più.

Ho chiamato il mio miniComando rapido *Data Date*, [in omaggio ai Kraftwerk](https://www.youtube.com/watch?v=ZtWTUt2RZh0). Sui Comandi rapidi sono una pera, ma sui titoli mi difendo.

<iframe width="560" height="315" src="https://www.youtube.com/embed/ZtWTUt2RZh0" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>