---
title: "Aspettando VirtualBox"
date: 2014-06-29
comments: true
tags: [Riccardo, iPhone, VirtualBox, Yosemite, OSX, iPad]
---
Il tema di questo *post* dovrebbe essere l’installazione di Yosemite Developer Preview su [VirtualBox](http://virtualbox.org), solo che non ho ancora finito e voglio parlarne solo a successo comprovato.<!--more-->

Mi consolo pensando che è il compleanno di iPhone, sette anni che hanno cambiato la vita come pochissimi altri oggetti hanno fatto, e ho letto un [articolo di Riccardo sul suo iPad 3](http://morrick.me/archives/6953).

Anch’io ho un iPad dello stesso tipo e condivido parola per parola tutto il pezzo. Un solo estratto:

>iPad di terza generazione ha finito per essere il membro sfortunato della famiglia: di vita breve (sia la seconda che la quarta generazione sono durate più a lungo) e rapidamente criticato (è lento! Scalda! eccetera). Beh, si è rivelato un grande apparecchio di grande valore personale. E non si percepisce come vecchio, né dalla prospettiva hardware né da quella software.