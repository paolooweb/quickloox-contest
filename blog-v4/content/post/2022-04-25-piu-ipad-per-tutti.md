---
title: "Più iPad per tutti"
date: 2022-04-25T23:16:51+01:00
draft: false
toc: false
comments: true
categories: [Hardware]
tags: [iPad, Birchler, Matt Birchler, Birchtree]
---
Posso solo dichiararmi completamente d’accordo con questa [chiosa di Matt Birchler](https://birchtree.me/blog/the-ipad-is-for-everyone/) sulla utilizzabilità di iPad e sulla sua convenienza come piattaforma:

> Ripeterò fino alla nausea che iPad è speciale proprio perché si rivolge a un insieme diversificato di persone e requisiti. È manna dal cielo per chi lavora sul campo. È straordinario in campo medico e nella vendita al dettaglio o, come ama puntualizzare Apple, in cabina di pilotaggio. È eccezionale per chi scrive, chi fotografa e per chi lavora in generale sull’informazione, specialmente in movimento. È ottimo per persone con richieste di accessibilità che Mac ha difficoltà a soddisfare.

> Nell’espandere negli anni le opzioni di interazione con iPad, lo hanno reso appetibile a un insieme sempre più grande di persone. Personalmente non uso un iPad per ogni cosa, ma lo utilizzo per scrivere, leggere, chattare, pianificare e altro. Per me alcune volte è un tablet, altre volte uno schermo per la Magic Keyboard e altre volte ancora una lavagna per Apple Pencil. Quando iPad è stato lanciato nel 2010 solo uno di questi metodi di input era disponibile e gli altri lo hanno solo reso più utile, accessibile e interessante di prima.

Sono concetti che risuonano molto sulla mia scrivania, per pratica indefessa. Amo dare la caccia ai mulini a vento o imbarcarmi in imprese che restano a metà, ma devo arrivare a fine giornata con una agenda di attività completate ed è molto difficile che scelga la modalità meno efficiente per farlo.

Con questi presupposti, utilizzo iPad circa per metà del tempo giornaliero e per l’altra metà uso Mac. Da alcuni mesi ho anche totalizzato lavoro ibrido, dove svolgo una attività principale su Mac e iPad come secondo schermo per altre applicazioni (per esempio le chat o le notifiche). Ho anche iniziato a usare iPad come schermo ausiliario, per esempio mostrando lì le pagine di un libro da cui prendo ispirazione e informazioni per scrivere su Mac sul quale, anch’io ho qualcosa da ripetere fino alla nausea, c’è [BBEdit](https://www.barebones.com/products/bbedit/) ed è sempre un altro campionato.