---
title: "Lo scatto del gattopardo"
date: 2016-12-13
comments: true
tags: [Flickr, iPhone]
---
Se c’è una cosa noiosa e monotona dell’informatica odierna, è la classifica delle fotocamere più usate su Flickr.<!--more-->

Avevo già [riepilogato la situazione tra 2013 e 2015](https://macintelligence.org/posts/2015-01-14-obiettivo-il-nulla/): iPhone, iPhone, iPhone.

Poi sono successe un sacco di cose. Android ha confermato la sua avanzata inarrestabile, Apple ha dimostrato la sua carenza di innovazione, ah se Steve fosse ancora vivo, sono stati annunciati milioni di computer da tasca innovativi, rivoluzionari, di rottura, capacissimi, economicissimi, e l’avanzata dei cinesi, e il dominio di Samsung.

Come risultato di tutte queste forze, [le fotocamere più utilizzate nel 2016](http://blog.flickr.net/2016/12/06/smartphones-dominate-flickr-in-2016-apple-leads/) su Flickr sono state quelle di marca Apple, con otto iPhone nelle prime dieci posizioni della classifica.

Cambia tutto. Cambia niente.