---
title: "Foto a raffica"
date: 2017-07-24
comments: true
tags: [Foto, McElhearn, Automator]
---
Spunti per l’automazione delle operazioni su Mac possono nascere ovunque, per esempio dall’articolo di Kirk McElhearn che spiega come [applicare rapidamente una o più modifiche dello stesso tipo a un gruppo di immagini dentro Foto](http://www.kirkville.com/batch-processing-in-apple-photos/).<!--more-->

L’accorgimento (Foto dispone di un copiaeincolla delle regolazioni compiute) è semplice e mitiga una delle mancanze principali del programma.

In compenso mi suscita più domande di quante risposte fornisca: posso creare un workflow Automator basato sul comando? Posso lavorare con Workflow per ottenere lo stesso risultato su iOS?

A brevissimo il completamento dell’indagine, per la quale mentre scrivo manco di hardware e tempo adeguato. Mi piace comunque lasciarlo per qualche ora come esercizio per il lettore.
