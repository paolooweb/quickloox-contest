---
title: "Detto tutto"
date: 2023-03-08T15:01:33+01:00
draft: false
toc: false
comments: true
categories: [Software, Internet]
tags: [A2, Freeform, Filippo Strozzi, Strozzi, Roberto Marin, Marin]
---
Su [Freeform](https://apps.apple.com/it/app/freeform/id6443742539?l=it), intendo (e vario altro, come oramai di abitudine). Quello che avevo da dire è entrato più o meno trionfalmente nella [cinquantaquattresima puntata del podcast di A2](http://a2podcast.it/54), fresca di pubblicazione, pronta da ascoltare.

Nel fiume di considerazioni, provocazioni, approfondimenti, discussioni, rivelazioni, cazzate emerite, riflessioni, commenti, chiacchiere, correzioni fraterne, anteprime, trucchi e segreti, retroscena e prossimamente, buono o cattivo, quello che non riesco mai a trasmettere pienamente sono la stima e l’amicizia per i due Unici di A2, Filippo e Roberto.

Per fortuna ho un blog.