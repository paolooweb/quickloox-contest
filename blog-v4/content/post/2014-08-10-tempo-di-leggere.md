---
title: "Tempo di leggere"
date: 2014-08-10
comments: true
tags: [Oggetto:Intervista, Inutile]
---
Sono stati giorni di tanto scrivere e l’igiene mentale consiglia di leggere. La notte passerà con la raccolta delle interviste di Inutile dal 2010 al 2013, [in ebook al costo veramente simbolico di quattro euro](http://www.portreview.it/riviste/numero_rivista/283/inutile-oggetto-interviste).<!--more-->

[Oggetto: Intervista](http://rivista.inutile.eu) ha anche il suo bel *trailer* sulle pagine del sito della rivista.

Roba indipendente, che a volte va poco per il sottile, brucicchia e ha il retrogusto torbato come certo whisky scozzese. Proprio come certo whisky scozzese, bisogna berne poco per sentire tutto il gusto e questa raccolta è un bicchierino di misura perfetta.

Ammetto infine di avere un debole per Inutile, per una ragione che supera regolarmente la mia forza di volontà: è fatto da gente brava. Anche se non volessi, *dovrei* leggere.