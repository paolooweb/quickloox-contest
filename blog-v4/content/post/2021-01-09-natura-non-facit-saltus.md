---
title: "Natura non facit saltus"
date: 2021-01-09
comments: true
tags: [ BBEdit, SixColors, Snell]
---
La natura non fa salti e neanche il *computing*. Jason Snell ha scritto su *Six Colors* un [articoletto](https://sixcolors.com/post/2021/01/bbedit-a-text-utility-not-just-a-text-editor/) su come usa [BBEdit](https://www.barebones.com/products/bbedit/), come esempio, per pescare nei risultati di un sondaggio online, ordinarli e inserire tabulazioni in modo che sia facile incollarlo dentro un foglio di calcolo.

Sono cose che capita di fare infinite volte, anche abbastanza comuni.

Una persona che si avvicini al computing impatta su un’interfaccia grafica. Se ne ha voglia e possibilità, inizia a interessarsi a metodi per arrivare prima a fare cose più potenti di quelle che l’interfaccia grafica gli consente.

Nel contempo, però, deve anche ottenere risultati. Il tempo per imparare è fatalmente compresso.

BBEdit non è un editor definitivo, nel senso che non farà *tutto* quello che si può umanamente immaginare faccia un editor.

Tuttavia integra nella propria interfaccia grafica decine, forse centinaia di operazioni avanzate, oltre la visuale di un utilizzatore casuale o superficiale. Permette di avanzare e allo stesso tempo ottenere risultati.

È un legame formidabile tra dove siamo partiti e dove vogliamo arrivare un giorno. Molto pratico, pure, perché intanto arrivano anche i risultati e comunque c’è modo di imparare.

Se è in giro dagli anni novanta, è perché BBEdit ha saputo colmare infiniti divari di infinite persone, tra le loro possibilità, le loro aspirazioni e le loro esigenze.

Anche quando, per dirla con Snell, ci si limita a *spingere avanti il cursore* con lo scrivere. Esattamente come ho fatto fin qui, con BBEdit.
