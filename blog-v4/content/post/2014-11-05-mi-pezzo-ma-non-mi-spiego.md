---
title: "Mi pezzo ma non mi spiego"
date: 2014-11-05
comments: true
tags: [iPhone6Plus, Bendgate]
---
Dice che gli iPhone 6 Plus si piegano. In effetti, [nove casi su dieci milioni](http://9to5mac.com/2014/09/25/apple-responds-to-iphone-6-bendgate-controversy-says-only-9-customers-have-complained/), forse mi preoccuperei più di cadute e furti.

Salta fuori quello che ne sa una più del diavolo e [scopre](http://www.reddit.com/r/apple/comments/2kqlgh/does_anyone_think_apple_had_quietly_fixed_the_6/) che il suo iPhone 6 Plus da 128 gigabyte pesa ventuno grammi più di quello da 16 gigabyte della moglie. Tocca, misura, guarda con il microscopio dentro lo spazio tra telaio e pulsanti del volume per vedere se qualcosa è cambiato dentro. Usa uno stetoscopio per sentire se i due oggetti suonano uguale. Gli suonano diversi.

Il sito-spazzatura raccoglie la testimonianza (di chissà chi e chissà se sta inventando, uno su dieci milioni, a questo punto magari sono pure venti o trenta) e con l’aiuto di un incrocio con iFixit [trova un indizio](http://bgr.com/2014/10/31/iphone-6-plus-bendgate-design-fix/): per Apple iPhone 6 Plus pesa 172 grammi e si hanno per le mani un esemplare da 172,7 e uno da 173,5. Otto interi decimi di grammo di differenza. Ecco lo *scoop*: Apple ha modificato gli iPhone 6 Plus in modo che non si pieghino e questo spiega le differenze di peso.

Dopo che la cacca ha colpito il proverbiale ventilatore, arriva finalmente [9to5Mac a chiarire](http://bgr.com/2014/10/31/iphone-6-plus-bendgate-design-fix/):

>Una nota nella pagina delle specifiche di iPhone 6 Plus puntualizza che dimensioni e peso possono variare in funzione della configurazione e del procedimento di fabbricazione e varie fonti ci dicono che differenze nell’ordine del grammo tra due iPhone 6 Plus sono normali.

Cioè gli iPhone 6 Plus si piegano e la prova è che Apple li ha modificati. Solo che non sono stati modificati e regna una grande ignoranza sui metodi (e gli esiti) della produzione industriale su larghissima scala. È tutto talmente così sbagliato che mi viene male persino il titolo.