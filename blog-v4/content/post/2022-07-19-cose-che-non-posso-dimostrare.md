---
title: "Cose che non posso dimostrare"
date: 2022-07-19T01:58:20+01:00
draft: false
toc: false
comments: true
categories: [Hardware, Software, Internet]
tags: [Senigallia, Mappe]
---
Le Mappe di Apple sottostimano il tempo di percorrenza di uno o due minuti, sempre, comunque.

Settanta giga al mese, spartiti tra Mac, iPad e iPhone, sono giusto sufficienti per il lavoro e le piacevolezze di un periodo al mare, a meno di riorganizzare robustamente i processi e l’organizzazione rispetto a quello che si fa a casa.

Il megaschermo informativo del pronto soccorso dell’ospedale di Senigallia (cose di parenti) è azionato da Windows Server ed è in crash clamoroso.