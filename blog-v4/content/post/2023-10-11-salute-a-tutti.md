---
title: "Salute a tutti"
date: 2023-10-11T13:30:18+01:00
draft: false
toc: false
comments: true
categories: [Software, Internet]
tags: [Free Software Foundation, FSF, eMacs, vi, NetHack, Stallman, Richard Stallman]
---
Ha ancora senso una Free Software Foundation? Quarant’anni fa un visionario sognò un sistema operativo completamente indipendente, aperto e libero. Il sogno si è realizzato solo parzialmente (non è Linux quel sistema) e tuttavia ha portato vantaggi indiscutibili ed enormi a tutta la comunità. Il punto è come continuare a fare lo stesso in un altro mondo, letteralmente, dove la posta elettronica è l’ultimo dei collegamenti, ci sono gli app store, lo sviluppo del software (ancora?) libero è condotto dalle multinazionali, il codice si scrive da solo o circa, grazie a tutto il codice libero che il sistema di machine learning ha imparato senza avvisare nessuno eccetera.

Non sempre si migliora, con l’età. Certi tratti caratteriali del visionario si sono induriti, esacerbati, amplificati. La sua visione strategica si è radicalizzata, non sempre a torto. La mobilitazione può unire le persone e portarle verso un obiettivo. La mobilitazione condotta senza rinnovare metodi e linguaggio può unire quattro gatti attorno a un’idea sterile dentro una comunità diecimila volte più grande.

Free Software Foundation ha compiuto quarant’anni e ha giustamente [festeggiato](https://www.fsf.org/blogs/community/gnu40-usa-meeting-with-old-and-new-friends-and-some-first-few-steps-on-the-freedom-ladder). Tra le altre cose hanno parlato di [emacs](https://directory.fsf.org/wiki/Emacs), vi (i suoi comandi di tastiera, perlomeno) e [NetHack](https://libreplanet.org/wiki/Group:Freedom_Ladder), tutte buone cose. Vuole dire che qualcosa dello spirito fondatore c’è ancora. Qualcosa che potrebbe contribuire anche oggi. Insomma, possiamo anche tollerare vi dopo tutto.

Il visionario nel frattempo è invecchiato e oggi [affronta una terapia](https://news.itsfoss.com/richard-stallman-battling-cancer/) che tutti ci auguriamo positiva. Sarà anche una occasione per capire se l’unione dell’associazione e del visionario è ancora produttiva o come potrebbe esserlo meglio.

Sarebbe bello ritrovarci tra dieci anni con Free Software Foundation e Richard Stallman, per vederli tutti in piena salute. E il software libero con o senza loro, *of course*.