---
title: "Lo schermo definitivo"
date: 2017-11-09
comments: true
tags: [iPhone, Displaymate, Stefano, Oled]
---
Come mi segnala **Stefano**, che ringrazio, iPhone X presenta [il migliore schermo per smartphone](http://www.displaymate.com/iPhoneX_ShootOut_1a.htm#Conclusion) per le analisi degli esperti di *DisplayMate*.

Ha ricevuto in ogni categoria tranne una (dove cade qualsiasi schermo Oled) valutazioni da *Very Good* a *Excellent* e primeggia in precisione del colore (*visivamente indistinguibile dalla perfezione*), luminosità per uno schermo Oled, contrasto in luce ambiente, scala di contrasti, riflettività dello schermo e variazione della luminosità in relazione all’angolo di visuale.

A proposito della risoluzione e densità dello schermo dei computer da tasca, DisplayMate scrive:

>Non ha alcun senso aumentare ulteriormente la risoluzione e e la densità dello schermo [oltre i dati di iPhone X] a scopo marketing, senza alcun vantaggio visivo reale per gli umani!

In pratica, iPhone X ha lo schermo definitivo fino al prossimo miglioramento tecnologico sostanziale.