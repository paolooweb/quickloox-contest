---
title: "Sistemare i Css"
date: 2018-04-09
comments: true
tags: [Css, San, Francisco, font]
---
All’inizio del digitale abbiamo perso tutta la raffinatezza tipografica sviluppata negli quattrocento anni precedenti. I vantaggi superavano d’altronde le rinunce.<!--more-->

Passati quarant’anni dal primo computer personale, si comincia a colmare il divario e a portare la tipografia ricercata anche nel digitale, grazie ai Css e ai font via web.

È quello che domani distinguerà quelli bravi e anche i contenuti buoni: la tipografia personalizzata e scelta esplicitamente.

In quest’ottica, avere un sistema riconosciuto e universale per riprodurre su una pagina web la tipografia del proprio computer è un progresso. Marginale, eppure progresso. Se non altro perché la tipografia dei sistemi operativi è curatissima e c’è da imparare.

Ne scrive [Craig Hockenberry su furbo.org](https://furbo.org/2018/03/28/system-fonts-in-css/) e raccomando la lettura, che è semplice e veloce.

Faccio per primo autocritica e invito chiunque a curare la propria tipografia, o a capire come curarla.