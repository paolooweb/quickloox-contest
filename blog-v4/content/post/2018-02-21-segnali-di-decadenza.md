---
title: "Segnali di decadenza"
date: 2018-02-21
comments: true
tags: [HomePod, Sonos]
---
Con [tutto quello che si può discutere di HomePod](https://macintelligence.org/posts/2018-02-12-suona-interessante/), che il tema del momento siano [gli aloni](https://support.apple.com/en-us/HT208435) lasciati dalla base di silicone sulle superfici di legno trattato è un segnale preoccupante di declino della civiltà occidentale.<!--more-->

Non lo fa HomePod; lo fa qualsiasi base in silicone. Il maniaco della mobilia, peraltro, proteggerebbe il mobile a prescindere, contro qualunque minaccia.

Ma la sensazione è che qualsiasi pretesto serva a sviare l’attenzione dalla resa dell’oggetto.
