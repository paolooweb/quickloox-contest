---
title: "L’interfaccia prossima ventura"
date: 2023-04-01T00:40:20+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [Terminale, ChatGPT, Knowledge Navigator, John Sculley]
---
Che gran progresso sarebbe, se disponessimo di un’interfaccia testuale che capisce il linguaggio naturale e ci obbedisse in tutto e per tutto.

Un esempio banale: voglio cambiare finestra di lavoro del computer. Digito un comando tipo `change directory`, gli dico che directory voglio *et voilà*, mi trovo nella nuova finestra che volevo.

I recenti progressi nell’apprendimento meccanizzato e nell’addestramento di modelli linguistici rendono uno scenario del genere sempre più vicino e non è escluso che possiamo vedere qualcosa di concreto anche a brevissimo.

La potenza dell’intelligenza artificiale potrebbe ovviamente darmi molto più del semplice comando e andare oltre i miei desiderata. Non so, potrei voler sapere da quanto il computer è acceso; pensa se con un comando tipo `uptime` il computer capisse le mie intenzioni e mi dicesse non solo quello che chiedevo, ma magari anche il carico di lavoro attuale del processore.

Tutti abbiamo bisogno di avere sott’occhio la disposizione dei giorni del mese. Un comando tipo `calendar` (ma perché non sintetizzarlo in `cal`, per risparmiare digitazione?) che mostrasse il mese in corso, per dire, sarebbe veramente il coronamento di tanti sforzi degli specialisti in machine learning.

Certo, è possibile che certi settori dell’occupazione possano soffrire: il lavoro di segreteria, se l’agenda e il calendario fossero disponibili semplicemente digitando un comando, si troverebbero a rischio.

Di converso nascerebbero professioni nuove. Non fatico a vedere futuri *prompt engineer*, capaci di elaborare i comandi più raffinati e riuscire a far rispondere il computer esattamente a quello che vogliamo. Sempre nell’esempio del calendario, la persona comune non andrebbe probabilmente oltre `date` per sapere che giorno sia; il *prompt engineer* potrebbe andare oltre e confezionare, che so, un comando `date --utc` per chi ha bisogno di riferirsi alla specifica [Universal Coordinated Time](https://www.timeanddate.com/worldclock/timezone/utc).

E perché limitarsi al computer, all’agenda, alle finestre? Una intelligenza artificiale che si rispetti sarebbe in grado di dimostrare empatia verso gli umani, capire i loro bisogni, le loro emozioni… pensiamo per esempio al sesso, una delle pulsioni primordiali. Dal machine learning potrebbe nascere un’intelligenza artificiale capace di parlare di sesso con un umano? Un’entità senziente capace di dire

`unzip ; strip ; touch ; grep ; finger ; mount ; fsck ; more ; yes ; umount ; sleep`

Certo, serve ancora molto lavoro per padroneggiare il linguaggio naturale. Ma i processori e i dischi di oggi, per non parlare del cloud, letteralmente non pongono limiti ai risultati che possiamo perseguire. Il [Knowledge Navigator](https://www.youtube.com/watch?v=umJsITGzXd0) teorizzato da John Sculley, con un colpo di genio: l’estrema semplificazione della rappresentazione delle interazioni, grazie all’uso dell’interfaccia testuale.

Non abbiamo ancora un nome per questa nuova creatura artificiale, che sono sicuro non tutti sarebbero già pronti ad accogliere. Mi permetto di proporre *ChatGPT*, come in *Great Personal Terminal*.

Vedremo che cosa ci riserva il futuro. Il pensiero di poter contare sulla precisione e sull’infallibilità del calcolo del computer, comunque, è confortante.

<iframe width="560" height="315" src="https://www.youtube.com/embed/umJsITGzXd0" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>