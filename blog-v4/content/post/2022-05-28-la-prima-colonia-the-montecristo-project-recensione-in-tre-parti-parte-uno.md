---
title: "La Prima Colonia - The Montecristo Project: recensione in tre parti - parte uno"
date: 2022-05-28T00:00:12+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [La Prima Colonia, The Montecristo Project, Edoardo Volpi Kellermann, Evk, Tolkien]
---
Nella [parte zero](https://macintelligence.org/posts/2022-05-19-la-prima-colonia-the-montecristo-project-recensione-in-tre-parti-parte-zero/) di questa miniserie ho spiegato i criteri con cui ho scelto di parlare dell’opera letteraria (e non solo) che [Edoardo](https://tolkieniana.net/wp/rivendell/musica/edoardo-volpi-kellermann/) ha appena dato alle stampe e ai download. Oggi, a rispettare almeno una promessa, si parla di come è fatta l’opera.

Per cominciare, è una narrazione ipertestuale e vorrei sottolineare subito che intendo una cosa diversa da un ipertesto, termine questo capace di entusiasmare o esasperare allo stesso tempo, in funzione delle esperienze vissute e dell’opinione verso l’uso davvero povero che finora l’umanità ha fatto dello strumento, in attesa di superare condizionamenti mentali millenari e accorgersi delle sue potenzialità.

L’autore qui è stato accorto e, dopo essersi scornato contro i limiti delle tecnologie ipertestuali odierne, ha attuato un compromesso che a mio parere ha valorizzato la storia e pure l’ipertesto, in un raro esempio di positività. La narrazione è infatti lineare come chiunque è abituato a concepirla e questo primo libro del *Montecristo Project* può essere appunto letto come un libro. Non è un librogame che ti fa saltare da pagina 28 a pagina 50 a pagina 99 e la bussola di dove e che cosa stiamo leggendo è sempre bene orientata.

Nel 2022, tuttavia, operare un compromesso a sacrificio delle possibilità iperetestuali sarebbe stata una occasione persa: per fortuna l’ipertesto c’è ed è sano, aumenta il piacere della lettura, aggiunge un piano diverso alla lettura stessa, si inserisce perfettamente nello stile dell’opera.

Qui veniamo a una delle ragioni principali per cui parlerò il meno possibile del contenuto: scoprire il contenuto di questo libro è uno dei piaceri principali e contribuisce in modo significativo al suo valore. Ogni termine, ogni concetto svelato fuori dalle sue pagine è impoverire l’esperienza, anche se di termini e concetti da svelare il libro è strapieno.

Una delle scommesse di Edoardo è montare bella e robusta fantascienza di matrice classica su un impianto di storia che si muove a partire dal presente, con una storia di respiro potenzialmente cosmologico (quanto, lo sapremo con i prossimi due libri) innestata in territori assai vicini e familiari. Dato il contesto che abbiamo intorno, come possiamo aspettarci di trovarlo inalterato, sconvolto, inedito da qui a …anta anni nel futuro?

Le tecnologie, le strutture di potere e di società, le infrastrutture, le problematiche presenti oggi e che, decenni più avanti, sono diventate altro costituiscono un elenco lungo e suggestivo e sospetto che per l’autore infilare (mai gratuitamente, va detto) voce dopo voce nel manoscritto deve essere stato anche un divertimento oltre che l’oggetto di una analisi.

L’ipertesto casca a fagiolo in questo caso. Ogni apparecchio, organizzazione, standard, composto, acronimo, media che compare nel libro ha la propria voce dedicata, linkata ovunque serva nel testo a una pagina di Wikip… di quello che nel racconto è diventata l’enciclopedia del web dopo decenni di evoluzione.

Quindi i termini non familiari si possono chiarire subito e, nel farlo, si acquisisce un substrato di racconto che ci fa approfondire e di conseguenza entrare ancora di più nel racconto stesso. L’intelligenza triplice è stata fare di queste note un ingrediente del racconto invece che un’appendice a fine libro che nessuno avrebbe mai letto, ambientare le note stesse dentro la storia e, la cosa più importante per chi legge, renderle *indolori*. Un tocco e si va a visitare l’approfondimento, un tocco e si torna esattamente dove eravamo rimasti. Niente scomodità, niente macchinosità, tutto molto chiaro grazie a un linguaggio grafico creato apposta che permette di associare rapidamente ogni situazione della storia a una immagine mentale.

Questo intendo per narrazione ipertestuale. Ovviamente parlo dell’edizione digitale; quella cartacea non offre la stessa possibilità e deve cavarsela in modo più scontato. Non entro nelle politiche editoriali dietro il *Montecristo Project*; però dico che leggere la storia su carta merita. Leggerla peraltro senza poter avventurarsi nelle note digitalizzate durante la lettura, è perdere qualcosa.

Al prossimo giro si dirà di *che cosa fa* [La prima Colonia](https://www.delosstore.it/ebook/54251/la-prima-colonia-the-montecristo-project--1/).
