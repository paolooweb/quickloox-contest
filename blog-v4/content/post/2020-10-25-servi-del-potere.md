---
title: "Servi del potere"
date: 2020-10-25
comments: true
tags: [youtube-dl, YouTube, Riaa, Dmca, Unix, Vlc, LibreItalia, Gpl3, Apple, Microsoft]
---
Arriva a GitHub una [notifica di richiesta di cancellazione di youtube-dl](https://github.com/github/dmca/blob/master/2020/10/2020-10-23-RIAA.md) per violazione di copyright, prontamente esaudita.

youtube-dl è un programma Unix che permette di [scaricare i video da YouTube](https://macintelligence.org/blog/2016/06/30/faticosa-e-straordinaria-liberta/). Certamente ci sono usi del programma che possono essere discutibili, ma gli usi, non il programma in sé. Posso solo auspicare che venga perseguito chi approfitta illegalmente del servizio di YouTube. Poi esistono il [fair use](https://it.wikipedia.org/wiki/Fair_use), il possesso di media regolarmente acquistati, i video con licenza Creative Commons e tanti altri distinguo. Cancellare youtube-dl è un atto di censura arbitraria e stupida.

Volevo però dire un’altra cosa, attorno alla frase fatta *scopare il mare*.

Il *repository* primario di youtube-dl su GitHub [è inaccessibile](https://github.com/ytdl-org/youtube-dl) mentre scrivo. Nel contempo, su GitHub si trovano *altri repository*, perché è software libero; per esempio, l’onomatopeico [ewe-tube-dl](https://github.com/ericalbers/ewe-tube-dl). Il [sito ufficiale di youtube-dl](https://youtube-dl.org) è operante (ecco perché bisogna avere un proprio hosting privato invece di appoggiarsi a servizi di massa).

Non solo: un lettore di *Slashdot* indica [come scaricare video di YouTube direttamente dal browser](https://news.slashdot.org/comments.pl?sid=17475764&cid=60642702), con l’aiuto di [Vlc media player](https://www.videolan.org/vlc/index.html), altro software libero. (Ecco perché bisogna usare software libero appena si può e magari iscriversi a [LibreItalia](https://www.libreitalia.org), che in questo momento sta convincendo qualche scuola italiana a usare software libero come dice la legge, invece di altre soluzioni più brutte).

Non ho verificato le istruzioni, ma anche se non funzionassero per qualche cambiamento dell’ultimo minuto, il principio rimane. Tralascio anche le legioni di estensioni, script, utility e accrocchi vari per ottenere esattamente lo stesso scopo. Pensare che soffocare youtube-dl protegga chissà quale copyright è scopare il mare; è anche fattualmente impossibile soffocare youtube-dl, o qualsiasi altro programma con una licenza free.

Certo, ci sono multinazionali abbastanza infastidite dai programmi liberi e altrettanto compromesse in modo spesso servile con *major* e conglomerati mediali e pubblicitari. La stessa Apple è stata all’avanguardia nell’uso di software libero, nell’ambito di aziende che producono computer e sistemi operativi proprietari, ma [si tiene alla larga dai programmi con licenza Gpl3](https://macintelligence.org/blog/2019/06/09/dalla-ba-alla-zeta/).

Tirate le somme, è stata soppressa solo una istanza di youtube-dl: quella ufficiale, subito identificabile, su GitHub.

Indovina quale multinazionale è [proprietaria di GitHub](https://macintelligence.org/blog/2019/07/28/il-blocco-ex-sovietico/).