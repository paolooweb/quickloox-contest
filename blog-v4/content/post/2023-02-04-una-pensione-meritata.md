---
title: "Una pensione meritata"
date: 2023-02-04T04:35:19+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [Dwarf Fortress, ARS Technica, Zach Adams, Tarn Adams, Adams]
---
[Dwarf Fortress](https://macintelligence.org/posts/2015-03-08-grandi-opere/) è un progetto folle, meraviglioso e istruttivo. Chiunque abbia da dire di negativo sulla tecnologia digitale dovrebbe fermarsi di fronte a quest’opera dell’ingegno: un *simulatore di nani costruttori di fortezze* di una profondità e ricchezza di contenuti sconsiderate. Non esiste alcun altro gioco a questo livello (gli si avvicina il gioco di ruolo, ma su basi completamente diverse); per certi versi non è neanche un gioco, ma un’esperienza artistica.

Tutto questo è da oltre un decennio completamente gratis, ma la cosa che sconvolge è la *roadmap* di sviluppo, che si proietta ben oltre questo decennio: i fratelli Adams, creatori del gioco, gli avranno dedicato probabilmente l’intera loro vita professionale in un punto imprecisato e speriamo il più lontano possibile del futuro.

Avendo vissuto di donazioni per un programma freeware, i due fratelli non erano esattamente ricchi sfondati e una vicenda sanitaria appena fuori dal consueto ne ha messo uno davanti alla realtà cinica: vivere con il minimo è nobile e saggio, ma anche difficile.

Così hanno pubblicato Dwarf Fortress su Steam e Itch.io, in versione commerciale. In poche settimane hanno incassato [oltre sette milioni di dollari](https://arstechnica.com/gaming/2023/02/after-16-years-of-freeware-dwarf-fortress-creators-get-their-7m-payday/).

Va detto che il fisco se ne prenderà quasi metà; ci sono varie spese cui fare fronte, compresa una persona assunta per dare una mano nella manutenzione del codice. Ma i fratelli Adams avranno una pensione decorosa e la possibilità di curarsi in modo adeguato. Una soddisfazione piccola rispetto all’immortalità nella storia dell’informatica, del gioco e del gioco *roguelike*; anche utile e meritata, però.