---
title: "Mezze opinioni"
date: 2016-10-13
comments: true
tags: [iOS, watchOS]
---
Il mio utilizzo medio della tecnologia Apple è sempre intenso sulla quantità e assai superficiale sulla qualità. Non ne escono storie interessanti come la recensione di iPhone 7 da parte di Federico Viticci di cui [ho fatto menzione](https://macintelligence.org/posts/2016-10-12-il-computer-del-futuro/) e posso solo fornire testimonianze frammentarie di esperienza di utilizzo.<!--more-->

Le quali, per iOS 10, sono assolutamente positive. A qualche giorno di distanza dalla [prima impressione](https://macintelligence.org/posts/2016-09-29-un-messaggio-di-innovazione/) confermo e riaffermo la mia posizione su Messaggi: è una rimessa a nuovo della *app* che da sola merita l’aggiornamento. La meccanica della schermata di blocco, con le cose interessanti a sinistra e la fotocamera a destra, mi piace ed è superiore. In moltissimi punti l’interfaccia è stata ripulita, semplificata, resa più lineare. Mappe è decisamente migliorata rispetto a prima e per esempio la resa grafica della navigazione è più amichevole. Lo stesso, amichevolezza, si può dire di Contatti.

Niente di questo vale una recensione, ma pezzo per pezzo emerge un quadro di iOS 10 che mi soddisfa.

A maggior ragione, watchOS 2. Mi è un po’ spiaciuto perdere la funzione dei contatti immediati, ma il Dock richiamabile dal pulsante laterale è la cosa giusta. La velocità di apertura delle *app*, che era un punto debole, adesso è quasi perfetta. Non è vero che si aprono istantaneamente come dice Apple, però ci mettono un secondo ed è più che accettabile. L’amministrazione delle notifiche e dei comandi veloci è decisamente migliorata.

L’unica critica all’aggiornamento è la decisione di disabilitare la possibilità di acquisire schermate dell’apparecchio, per assegnare la scorciatoia alla *app* Attività. Intuisco che un mare di gente usi watch durante l’esercizio fisico e prendere schermate sia uno sport assai meno praticato; tuttavia mi vengono in mente svariate soluzioni, anche se John Gruber [è d'accordo con la scelta di Apple](http://daringfireball.net/linked/2016/09/14/watchos-screenshots) e approva che le schermate siano disabilitate.

In ogni caso, e qui siamo d’accordo Gruber e io (lui non lo sa), proprio è inammissibile che l’uso della scorciatoia durante Attività generi *anche* una schermata, quando queste ultime vengano [abilitate da iPhone](https://support.apple.com/en-us/HT204673). Spero di vedere un aggiornamento in merito presto.