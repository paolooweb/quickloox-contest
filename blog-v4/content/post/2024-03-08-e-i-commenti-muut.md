---
title: "E i commenti, Muut"
date: 2024-03-08T00:28:56+01:00
draft: false
toc: false
comments: true
categories: [Software]
tags: [Muut]
---
Su segnalazione di [Mimmo](https://muloblog2.netlify.app) e di altri prendo atto della [chiusura di Muut](https://muut.com), il sistema dei commenti per questo blogghino, in programma per il 31 marzo.

Niente dura per sempre e soprattutto nel digitale. Sono contento di avere potuto contare su Muut per tanto tempo come sistema semplice, amichevole, libero da pubblicità infestanti e collegabile al motore del blog senza dover impazzire.

E tutto questo gratis, perché se ricordo bene ho adottato Muut quando era ancora in beta. O forse c’era un livello gratuito che poi però è sparito. Comunque sia, non mi è mai stato chiesto niente.

Aspetto il 30 marzo per scaricare comunque tutto lo scaricabile dai loro server in modo da averlo disponibile e poi decidere se e come riportarlo online.

Intanto è già cominciata la selezione del successore. Sono tutti buoni a proporre Disqus, che detesto per la sua pessima interfaccia e per la pubblicità pervasiva. Qui è iniziata la selezione dei candidati, sempre grazie all’aiuto di tanti; cerchiamo qualcosa che possa ricordare Muut per leggerezza, semplicità, funzioni, interfaccia. Sarebbe bella una soluzione self-hosted, su cui avere il controllo e, certo, l’onere della manutenzione e del supporto. Cose che non serve avere con soluzioni esterne come era Muut, peraltro esposte a rischi come una chiusura improvvisa, indipendente dalla nostra volontà.

D’altronde tengo ad avere i commenti. Sono un fiore all’occhiello di questo blog e hanno moltiplicato il valore di innumerevoli post.

Il viaggio, come sempre, continua.