---
title: "La Leonessa e il dilettante"
date: 2016-10-03
comments: true
tags: [Turci, Brescia, Mac]
---
Sabato scorso a Brescia in piazza Paolo VI [cantava Paola Turci](http://www.paolaturci.it/show/brescia-bs/) e, di passaggio, ho preso una immagine del banco di regia.

Sempre dilettanti, ovviamente, perché Apple i professionisti li ha abbandonati.

 ![Banco di regia per Paola Turci a Brescia sabato primo ottobre 2016](/images/turci-brescia.jpg  "Banco di regia per Paola Turci a Brescia sabato primo ottobre 2016") 