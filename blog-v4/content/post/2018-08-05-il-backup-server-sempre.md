---
title: "Il backup server sempre"
date: 2018-08-05
comments: true
tags: [Time, Machine, backup, Mac]
---
Di questo periodo non avrei backup di sorta se non fosse possibile impostare un server Time Machine su un Mac in modo che gli altri Mac vedano in rete un disco di backup condiviso allo scopo.

Le istruzioni più puntuali e sintetiche mi sembrano [quelle di 512 Pixels](https://512pixels.net/2018/08/how-to-set-up-time-machine-server/) e di mio aggiungo solo che sono davvero semplici. Un caso nel quale il vantaggio è clamorosamente superiore allo sforzo e in cui la sostanziale dismissione di macOS Server è tutt’altro che una penalizzazione.