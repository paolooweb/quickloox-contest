---
title: "Fammi vedere il tuo dado"
date: 2011-06-09
draft: false
toc: false
comments: true
categories: [ ]
tags: [script]
---
Non potevo presenziare alla sessione più recente del nostro gruppo storico di Dungeons and Dragons. Così mi sono collegato da casa in videoconferenza con il ritrovo per la serata.

Sono stato in collegamento per quasi tre ore.

La qualità del video è stata media: qualche difficoltà sui movimenti rapidi delle persone attorno alla mappa di gioco, ma risoluzione sufficiente per distinguere le miniature di personaggi e mostri e soprattutto la quadrettatura della mappa, importante per cogliere la situazione.

La qualità dell’audio è stata, pure, media: il microfono presso il ritrovo era quello di serie di un Mac, che ha penato un po’ con le voci che arrivavano da tutte le direzioni, spesso in sovrapposizione. Tuttavia siamo riusciti a comunicare con efficacia, magari ripetendo una frase qua e là, come potrebbe succedere in pizzeria.

La prova del nove dell’efficacia della soluzione è stata il lancio dei miei dadi. Dovevo essere veloce a sufficienza per stare dietro ai miei amici riuniti fisicamente attorno alla mappa e dare la certezza di non barare.

Soluzione a bassa tecnologia ma estremamente produttiva: collegato in video con Mac, ho lanciato i dadi con Dicenomicon su iPad, mostrando iPad alla webcam intanto che i dadi virtuali rotolavano. Quindi c’erano la ragionevole sicurezza che io non barassi e la sicurezza di verificare il risultato.

La risoluzione video di iChat ha permesso agevolmente ai miei amici di visionare, sulla webcam del Mac locale, sia i dadi sia il totale del lancio, che Dicenomicon mostra brevemente in cima allo schermo.

Tutto molto a bassa tecnologia, ma capace di tenere il passo per tempistica e risultati con sette persone che lanciavano dadi fisici davanti a tutto. Mi viene voglia di definirlo come test per valutare il livello di dettaglio accettabile in una videoconferenza, nel senso che guardare il viso della zia d’America a quadrettoni come fanno certi altri programmi ci sta tutto e comunque la zia si accorge che facciamo ciao ciao con la manina, ma la parte video di videochiamata dovrebbe stare per guardo, non intuisco.

Fammi vedere il dado. Se vedo il punteggio, allora è video.

