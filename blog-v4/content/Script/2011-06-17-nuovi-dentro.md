---
title: "Nuovi dentro"
date: 2011-06-17
draft: false
toc: false
comments: true
categories: [ ]
tags: [script]
---
L’autore è Piergiovanni, che ringrazio. Autore nel senso di estensore, ma sia chiaro che è vita vissuta, non fiction.

Oggi avevamo gli scrutini di una quinta. Ormai da alcuni anni si fa lo scrutinio elettronico sulla piattaforma ScuolaNet. I piccì della mia scuola sono vecchi e stanchi. Così molti colleghi si lamentavano, dicendo che un computer ha una vita media di due anni e che dopo due anni lo devi cambiare. Eeeh?! Ho spiegato che io a casa ho un iMac DV che funziona bene da undici anni senza aver mai dato problemi e ci gira pure Office 98. Dopo un po’ un collega si accorge che la sua Usb Key non si apre più. Colpa dei virus, dice. Ma come, chiedo io col sorriso bastardo delle grandi occasioni, non c’è un antivirus? Replica il collega No, è stato disinstallato perché… rallentava il computer! Cercando di non sghignazzare ho spiegato che la piccola Sophia, l’iMac undicenne di cui sopra, ha una velocità ormai ridicola paragonata ai clock attuali in GHz ma che quando usavo il buon vecchio Norton neanche se ne accorgeva. Aah, fa il collega, ma quello è un sistema diverso. Ho detto io Diverso sì, visto che non ci sono tanti virus per Mac OS X. Alla fine sai cosa mi hanno detto? Che le scuole preferiscono Linux perchè è free ma l’assistenza costa, quindi usiamo Windows XP. Tanto era già installato sui computer!

Son tornato a casa con il mio MacBook Pro che ha due anni e che ha poca voglia di andare in pensione. E che dopo tre ore aveva ancora il 75% di carica. Ho acceso l’iMac e gli ho attaccato la mia chiavetta Usb che va su tutti i computer e sarà piena di virus, ma l’iMac non ci ha fatto caso, e ho aggiornato i dati dello scrutinio. Poi ho acceso un vecchio iPod di cinque anni fa dove tengo i brani dei Pink Floyd e ho ascoltato The Dark Side of the Moon pensando che questi utenti Windows sono proprio strani.

