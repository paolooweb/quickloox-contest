---
title: "Non è l'ambiente quello da salvare"
date: 2012-07-17
draft: false
toc: false
comments: true
categories: [ ]
tags: [script]
---
Apple documenta l’impatto ambientale della propria attività e tra l’altro certifica con ottimi risultati i propri prodotti presso Epeat, il registro globale definitivo per un’elettronica più verde.

Poi esce MacBook Pro Retina e non viene certificato da Epeat. Allora Apple esce da Epeat. Dopo di che annuncia di avere sbagliato e di tornare su Epeat.

A questo punto MacBook Pro Retina viene certificato Gold da Epeat.

Non senza che la Electronics TakeBack Coalition contesti la decisione di Epeat.

Tutti questi sono fatti acquisiti e non è possibile trarne conclusioni diverse da quelle che seguono.

Apple ha fatto la voce grossa con Epeat – le aziende pagano l’iscrizione al registro e l’assenza di Apple significa perdere soldi – e il registro si è adeguato. A dimostrazione che la sua certificazione è poco attendibile.
Apple ha fatto presente a Epeat che i suoi criteri di misurazione non sono validi ed Epeat li ha adeguati. A dimostrazione che la sua certificazione era poco attendibile, oggi forse e domani chissà.
I criteri sono validi ma Epeat si è proprio sbagliata ad applicarli, così Apple ha protestato ed Epeat ha corretto l’errore. A dimostrazione che la certificazione è attendibile, ma l’organizzazione non lo è.
A questo si aggiunge la presa di posizione della Coalition.

Apparentemente c’è un sacco di gente vogliosa di fare il cane da guardia del comportamento delle aziende, sorvegliarle, dare voti, distribuire patenti.

Apparentemente, i cani da guardia si muovono liberi di sragionare, sbagliare, confondere, diffondere false informazioni, agire a capriccio, dietro soldi, per interessi non pubblici e non evidenti.

Come ha dimostrato Apple, che aveva la capacità e la possibilità di fare braccio di ferro e altrimenti sarebbe stata travolta, più che salvare l’ambiente dobbiamo salvarci dai suoi cani da guardia. Che non salvano niente e fanno solo business parassita.

A proposito di MacBook Pro Retina, Apple ha pubblicato la sua scheda di impatto ambientale. Su cui l’opinione è libera, a patto che rispetti i numeri.

