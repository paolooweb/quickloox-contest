---
title: "Allarmi siamo allarmisti"
date: 2011-05-13
draft: false
toc: false
comments: true
categories: [ ]
tags: [script]
---
Ed Bott di ZdNet, pochi giorni fa: Prossimamente su Mac: malware sul serio.

Se è per questo ecco Tony Bradley, PcWorld, dicembre scorso: Apple non vola più sotto i radar della sicurezza.

E vogliamo dimenticare Nick Farrell, The Inquirer, di settembre 2009? Gli hacker prendono di mira Mac.

Abbiamo anche Roger L. Kay, Businessweek, marzo 2008: Apple, effetto Icaro.

A dicembre 2007 Kevin Allison, Gnt, scriveva La popolarità crescente di Apple attira gli hacker.

E quasi in contemporanea gli faceva eco Bill Snyder, Infoworld: Gli hacker prendono di mira Mac OS X.

Li aveva preceduti di un mese Ryan Singel su Wired: Il nuovo trojan per Apple indica che si è aperta la stagione di caccia su Mac.

E un altro mese prima c’era Kim Zetter, sempre su Wired: La sicurezza su iPhone rivaleggia con Windows 95 (no, non è una buona cosa).

Correva settembre 2006 quando Steve Hargreaves scriveva su Cnn Gli hacker si accingono a craccare Mac.

Mentre a maggio 2006 John McCormick su TechRepublic tuonava La X indica il bersaglio: gli hacker rivolgono l’attenzione al sistema operativo di Apple.

Più o meno in simultanea echeggiava il controcanto di Bob Johnson su Cnet: Addio alla sicurezza Apple?.

Data a marzo 2005 lo scoop di Munir Kotadia, Silcon.com: Secondo Symantec Mac OS X è un bersaglio per gli hacker.

Chiudo con Eric Hellweg, Mit Technology Review, ottobre 2004: Gli hacker mirano su Apple? Congratulazioni!.

Uso il Mac, lato sicurezza, esattamente come lo usavo nel 2001. Gli unici rischi che corro sono quelli di andare su siti che dovrei evitare, scaricare software equivoco da posti equivoci, credere ai maneggioni e ai truffatori.

Non c’è minaccia alla sicurezza di Mac che non preveda la scarsa attenzione del suo proprietario. Tenere il cervello acceso è l’unico antivirus che serva veramente. E il resto sono chiacchiere, oltre che allarmi decisamente male regolati.

