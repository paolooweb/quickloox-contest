---
title: "Il pronostico sbagliato al momento sbagliato"
date: 2012-02-29
draft: false
toc: false
comments: true
categories: [ ]
tags: [script]
---
I tempi eroici di dicembre 2008 in cui Computerworld annunciava che il mese dopo Apple avrebbe presentato un netbook da 599 dollari.

Tempi economicamente duri chiedono un sistema a prezzo più basso di quello che proporrà Apple, ma l’azienda non intende cannibalizzare le vendite di MacBook.

John Gruber di Daring Fireball commentò No way, che non c’è bisogno di tradurre.

Avanti veloce a febbraio 2012: Computerworld annuncia che Lenovo smette di vendere netbook via Internet. Pare che le sue vendite durante il trimestre natalizio siano calate del 43 percento rispetto all’anno prima.

Quanto sarebbe stato bello se Computerworld lo avesse pronosticato un mese prima.

