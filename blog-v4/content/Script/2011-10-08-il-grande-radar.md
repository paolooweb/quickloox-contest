---
title: "Il grande radar"
date: 2011-10-08
draft: false
toc: false
comments: true
categories: [ ]
tags: [script]
---
Appresa la notizia della scomparsa di Steve Jobs, la mia prima reazione è stata scrivere un post con dentro una mela.

Sono arrivate tante mele in risposta. Ho provato tenerezza, commozione, affetto per tutti quanti hanno pensato di portare la propria mela o il proprio messaggio.

Dopo poche ore mi trovavo a ripulire le mele da una quantità equivalente di spam: messaggi finti pieni di spazzatura con un fine bassamente commerciale, inviati da mezzi criminali senza scrupoli e senza umanità.

Potevo vedere contemporaneamente l’espressione più alta della condivisione umana e quella più bassa. Insieme, allo stesso livello, senza barriere. Potevo fare la differenza solo riconoscendo lo schifo e cancellandolo, un messaggio alla volta, per preservare l’umanità e l’intelligenza collettiva, un messaggio alla volta.

In mezzo al massimo e al minimo, tanti livelli intermedi. Miei cari amici hanno aperto spazi per raccogliere ricordi e testimonianze su Jobs. Li ho ammirati. Altri miei cari amici hanno colto l’occasione per fare pubblicità al loro articolo sulla morte di Steve Jobs. Li ho compatiti.

Persone di straordinaria intelligenza hanno attribuito a Jobs varie posizioni dentro un elenco di grandi figure dell’umanità. Persone di straordinaria stupidità sono uscite con frasi tipo muoiono 29 mila bambini al giorno e mi dispiace di più. Gli auguro di non sentirselo ricordare al funerale di un loro caro.

È scontato che la nostra vita sarebbe (profondamente) diversa se non ci fosse stato Jobs e non ci fosse stata Apple. Steve ha vissuto una parabola umana controversa e straordinaria, che lo ha portato a formulare considerazioni sulla vita e sulla morte da ascoltare con attenzione. Gli inviti a non sprecare il tempo vivendo la vita di qualcun altro, le citazioni di Stay hungry. Stay foolish, è tutto appropriato.

Provo ad aggiungere una mia riflessione personale a ricordo del fondatore di Apple e dell’uomo.

Siamo vivi e ci troviamo immersi in Internet, questo incredibile mezzo di comunicazione – Steve ci avrebbe messo un amazing – capace di mettere sullo stesso livello, allo stesso momento, con la stessa facilità, il peggio e il meglio di tutti e di tutto. Un messaggio prezioso con una mela, un messaggio finto pieno di niente, insieme.

Una volta esisteva un sistema imperfetto e arbitrario, ma con dei meriti, per cui a poter diffondere il proprio messaggio erano persone e organizzazioni in qualche modo selezionate. Il sistema garantiva almeno parzialmente il valore delle idee diffuse e potevamo nutrire un certo grado di fiducia preventiva in quello che leggevamo.

Questo sistema non esiste più. La responsabilità di riconoscere il valore delle idee e delle persone è interamente nostra, perché qualsiasi idea di qualsiasi persona ora può essere diffusa.

Il fondatore di Apple ci ha messo a disposizione gli strumenti più affascinanti e potenti che l’umanità abbia potuto finora immaginare, per lavorare imparare e divertirci, e confrontarci nel mondo delle informazioni, con le nostre idee, con la nostra persona.

L’uomo Steve Jobs ha spiegato verità abbastanza ovvie, su cui ovviamente riflettiamo poco. Ognuno è nudo di fronte alla morte, catene di eventi improvvisamente si formano ad acquistare un significato inaspettato e tutto il resto.

Siamo puntini su un immenso radar dell’era della comunicazione (anche se siamo panettieri, nel XXI secolo il nostro lavoro è comunicazione). Il radar segnala tutto: il massimo e il minimo, il genio e l’idiozia, la partecipazione e il cinismo, il valore e la sua assenza, un animo pieno e caldo oppure una mente arida e gelata.

Abbiamo i migliori strumenti a disposizione (tecnologia e arti liberali, disse Jobs). Abbiamo un’idea del senso della vita.

La mia modestissima interpretazione della vicenda umana di Steve Jobs è questa: siamo responsabili della nostra posizione sul grande radar. Siamo responsabili del riempire la nostra vita con comunicazione di valore e persone di valore che comunicano. E dell’evitare chi il valore se lo è dimenticato.

Siamo puntini. Ma abbiamo valore. Ricordiamocene, in ogni momento.

So long, Steve.

