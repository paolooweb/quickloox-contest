---
title: "I ragazzi del coro"
date: 2013-04-05
draft: false
toc: false
comments: true
categories: [ ]
tags: [script]
---
A volte tenere un archivio è inutile, perché esistono persone come Horace Dediu di Asymco che, passati tre anni dall’arrivo di iPad, compilano l’antologia di quelli che avevano capito tutto.

Raccolta straordinaria, bella lunga eppure concentrata tra il 29 gennaio 2010 e il 5 marzo 2010, con l’eccezione di una delle perle di Steve Ballmer, elaborata il 21 ottobre 2009:

Diciamola chiara, Internet è stata progettata per il PC. Internet non è progettata per iPhone. Ecco il perché delle loro 75 mila applicazioni: stanno tutti cercando di fare apparire Internet decente su iPhone.

Nessun’altra citazione riesce a uguagliare quel senso profondo di avere capito completamente zero degli eventi.

Anche se spicca l’idea che iPad destinato al fallimento in quanto grande iPod touch. È verissimo: iPad è un grande iPod touch. Semplicemente, gli espertoni hanno vagamente sottovalutato l’effetto del cambiamento delle dimensioni sull’interfaccia. Che condivide totalmente lo spirito eppure è assolutamente, evidentemente diversa.

Purtroppo la troppa fretta di voler cantare ancora una volta la solita canzone ha tradito quelli che hanno trascurato di dare un minimo di attenzione al nuovo spartito.

