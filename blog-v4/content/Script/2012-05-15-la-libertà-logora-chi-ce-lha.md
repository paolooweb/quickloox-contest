---
title: "La libertà logora chi ce l'ha"
date: 2012-05-15
draft: false
toc: false
comments: true
categories: [ ]
tags: [script]
---
Ricapitolo questa storia da Daring Fireball di John Gruber.

Un anno fa circa, Htc vendeva alla grande, con incremento dei profitti del 197 percento rispetto a un anno prima e del 192 percento in unità vendute.

Oggi la musica è cambiata e i profitti sono calati del 70 percento.

Che cos’è cambiato nel frattempo? Certo, nel mercato dei cellulari intelligenti fanno profitti in pochi: Apple, Samsung e poi basta, a parte proprio Htc che raccoglie ancora qualche briciole. Ma gli altri, Sony, Rim, non parliamo di Nokia, stanno poco bene da un bel po’.

L’inversione di tendenza di Htc coincide invece, ovviamente nel senso letterale della coincidenza, con l’apertura del bootloader dei modelli venduti. Aprire il bootloader significa, in pratica, avere un apparecchio equivalente a un iPhone con jailbreak, dove si può installare qualsiasi cosa e frugare nel sistema a volontà.

Di recente si è scoperto che l’ultimissimo modello di smartphone Htc avrebbe il bootloader aperto, se non fosse per At&T, l’operatore telefonico, che non lo permette.

Riassumendo: un’azienda che fa molti profitti vendendo telefoni “chiusi” e li apre, vede precipitare i profitti. Il sospetto è che gli operatori telefonici non vedano di buon occhio questi telefoni.

Se è così, cambia radicalmente il quadro del dibattito sul jailbreak, spesso ridotto a cattiva Apple che non mi permette di montare il mio tema grafico preferito.

Se diventa cattivi operatori che non vedete di buon occhio telefoni liberi, si capisce che Apple – ma anche Rim per dire – semplicemente tutelano la loro posizione sul mercato. E se Apple fa profitti elevati, le nostre probabilità di avere un iPhone 2012 ancora più bello e affascinante dei precedenti, e lo stesso nel 2013 e via dicendo, aumentano. A nostro beneficio finale. Libertà di pistolare nel filesystem di iPhone (non di Mac; di iPhone) anche a costo di vedere prodotti apparecchi al risparmio e progressi tecnologici ritardati? Io sono orientato verso il no, grazie. Voglio vedere uscire iPhone fantascientifici ed essere libero di ignorarli oppure sceglierli e avere qualcosa meglio dell’anno prima, magari anche meglio delle alternative disponibili.

Ecco. Questi sono indizi che forse il dibattito sulla libertà, assolutamente importante e condiviso, sia impostato su coordinate da rivedere.

