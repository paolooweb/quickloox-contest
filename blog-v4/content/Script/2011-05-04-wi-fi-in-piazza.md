---
title: "Wi fi in piazza"
date: 2011-05-04
draft: false
toc: false
comments: true
categories: [ ]
tags: [script]
---
A proposito delle recenti polemiche sul database di torri cellulari e basi Wi-Fi presente negli iPhone, Apple ha dichiarato che un prossimo aggiornamento di iOS eliminerà i dati in questione.

Prima che vadano persi, c’è un gruppo di nome crowdflow che ha messo a punto un software apposta e chiede gentilmente i dati in questione, allo scopo di crearne un nuovo e più grande database aperto di torri e basi, universalmente consultabile.

Il lavoro permette (anche) di produrre immagini suggestive di mappe, come questa relativa alla città di Berlino.

La paranoia non è mai eccessiva. Eppure a me tutto questo baccano su questo tipo di dati continua a parere fuori luogo.

