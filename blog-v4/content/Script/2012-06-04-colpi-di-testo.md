---
title: "Colpi di testo"
date: 2012-06-04
draft: false
toc: false
comments: true
categories: [ ]
tags: [script]
---
Sto scrivendo questo pezzo con SubEthaEdit di TheCodingMonkeys, dopo che ho appena usato il programma con successo per risolvere un problema di lavoro collaborativo in un ambiente dove non si può sempre fare affidamento su una connessione di rete.

E per il lavoro collaborativo, SubEthaEdit è una manna: tutti insieme a scrivere sullo stesso documento di testo, previa una connessione Bonjour. Basta che uno dei Mac apra una rete Wi-Fi ed è fatta.

Il programma offre anche un ragionevole supporto al programmatore; per esempio però, mi è toccato inserire il link Html a mano là dove BBEdit lo inserisce automaticamente. Lunghezza del testo? Non viene calcolata in tempo reale. E così via.
Nello svolgersi del lavoro è apparso evidente che SubEthaEdit risolveva più che efficacemente un problema importante – il lavoro collaborativo – lasciandone aperti altri, meno importanti ma complessivamente decisivi nello stesso modo in cui tanti indizi finiscono per costituire una prova.

E questo vale per tutti gli editor che ho sperimentato finora. Arrivano chi al 99, chi al 90, chi all’80 percento del compito, ma una vera corazzata con c’è. Tutti cacciatorpedinieri.

Una curiosità rivolta a chi ne sa veramente di Terminale: sarebbe possibile instaurare sessioni di lavoro collaborativo su emacs? E come? (in generale, non istruzioni ma concetti).

