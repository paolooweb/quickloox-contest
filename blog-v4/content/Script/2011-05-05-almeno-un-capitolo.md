---
title: "Almeno un capitolo"
date: 2011-05-05
draft: false
toc: false
comments: true
categories: [ ]
tags: [script]
---
Mi è apparso davanti un testo libero e gratuito intitolato Advanced Bash-Scripting Guide, la guida allo scripting avanzato di bash, ovvero a usare il Terminale come se fosse un linguaggio di programmazione.

Roba assai tosta, che richiede impegno e fatica, di livello inutile e financo eccessivo per il 99 percento di noi.

Ciò detto, suggerisco fortemente un approccio almeno superficiale. Dal sito linkato sopra o usando la versione Pdf, almeno un capitoletto, l’introduzione, un salto qua e là, una scorsa veloce.

Diventare assi della programmazione da riga di comando, no. Se Mac è nato per una cosa, è rendere il computer sempre più facile da usare.

Invece dare uno sguardo curioso, ricordarsi tre o quattro termini che potrebbero venire buoni, dedicare un quarto d’ora a un esperimento ozioso, sì. C’è la possibilità che nasca qualcosa, molto superiore a quella di grattare oziosamente un tagliando dal tabaccaio e aspettarsi qualche euro.

