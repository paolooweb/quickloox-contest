---
title: "Chi meno spende"
date: 2011-05-23
draft: false
toc: false
comments: true
categories: [ ]
tags: [script]
---
Microsoft ha chiuso i Pioneer Studios dopo tre anni di (loro) vita.

Gli Studios erano stati concepiti da J Allard, il dirigente dietro la creazione di Xbox, unica avventura di Microsoft fuori da Office e da Windows che in qualche modo abbia seriamente portato profitto all’azienda. Allard non aveva paura di rompere le uova nel paniere, sfidare lo status quo, scandalizzare i rami conservatori dell’azienda con idee controcorrente. Ha lasciato Microsoft l’anno scorso.

Dagli Studios sono usciti il concept del progetto Courier, tablet-quaderno dei miracoli mai andato oltre qualche video promozionale, parti del fallimentare lettore musicale Zune, il progetto di smartphone Kin che è durato sul mercato due mesi prima di registrare un buco nell’acqua catastrofico, parti di Windows Phone 7 di cui pare le vendite siano largamente inferiori ai due milioni di unità nel primo trimestre 2011 (cioè fallimento sostanziale, in attesa che arrivi il pompaggio artificioso dei numeri grazie all’accordo con Nokia) e anche, sì, parti di Xbox.

Georg Petschnigg, cofondatore degli Studios, ha dichiarato che lo scopo del gruppo era individuare nuove esperienze per il consumatore che potessero fruttare a Microsoft almeno cento milioni di dollari l’anno e che il tasso di successo degli Studios era del venti percento.

Nel 2009 Microsoft ha speso in ricerca e sviluppo otto volte più di Apple, una cifra esorbitante. Eppure iPhone, iPad, iTunes Store, App Store, arrivano da un’altra parte.

Nel 2006 Steve Jobs disse I nostri amici su a nord spendono più di cinque miliardi di dollari l’anno in ricerca e sviluppo, eppure in questi giorni sembra che riescano solo a copiare Google e Apple. Direi che è un buon esempio di come il denaro non sia tutto.

Ecco.

