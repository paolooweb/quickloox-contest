---
title: "Chiarificatori"
date: 2011-10-20
draft: false
toc: false
comments: true
categories: [ ]
tags: [script]
---
Michael Dell, fondatore e amministratore delegato (Ceo) di Dell, a domanda diretta ha finalmente trovato il momento per chiarificare la sua famosa battuta del 1997, che cosa farei [se fossi Ceo di Apple]? La chiuderei e restituirei i soldi agli azionisti.

Durante il Web 2.0 Summit, Dell ha spiegato che in realtà non riesce a immaginarsi di essere Ceo di una azienda diversa dalla sua. Per questo ha risposto su Apple come avrebbe risposto su qualsiasi altra azienda: senza averci mai pensato.

Ci si chiede se Dell avrebbe mai chiarificato, se in questi quattordici anni Apple non avesse passato di una dozzina di volte il suo valore di mercato. Ci si chiede se lo avrebbe fatto prima del 5 ottobre. Ci si chiede se in quattordici anni non si poteva tirare fuori una scusa migliore, tipo scherzavo.

A suo favore non gioca il clima che si respirava nel 1997 sul mercato americano. Nella stessa occasione in cui si pronunciò Dell, l’allora chairman presidente e Ceo di Sun Microsystems Scott McNealy illustrò l’interesse che Sun avrebbe potuto avere nell’acquisto di Apple: Possiedono un grosso palazzo che ci farebbe piacere avere. Proprio di fronte a JavaSoft.

Non è che Dell fosse uscito con una idea sua. Quell’evento era un’occasione perfetta per sparlare in pubblico di un’azienda rivale in difficoltà.

Intanto Sun Microsystems è stata acquistata da Oracle e non esiste più. Chissà se chiarificherà anche McNealy.

