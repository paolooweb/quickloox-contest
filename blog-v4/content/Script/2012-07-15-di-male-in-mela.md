---
title: "Di male in mela"
date: 2012-07-15
draft: false
toc: false
comments: true
categories: [ ]
tags: [script]
---
Ricevo e pubblico tutelando la privacy dell’autore.

I tecnici del reparto informatico stanno diventando paranoici: bloccati tutti i siti che contengono giochi, bloccato il sito di Transmission perché è P2P, siti di fotografia basta che ce ne sia una di nudo, bloccato Urban Dictionary perché è adult language, bloccato un sito con un programma per Vpn perché è proxy avoidance…

So che è vero. Sono entrato in aziende dove il reparto marketing non può accedere a YouTube (che gli servirebbe pure). Non parliamo del divieto di Facebook.

Le scuole che hanno un collegamento Internet prendono come seconda decisione, se non è la prima, una lista nera di connessioni proibite. Eccetera.

Sono tendenzialmente poco d’accordo, se non per nulla a parte eccezioni eclatanti. Significa togliere ogni responsabilità a chi utilizza, nell’illusione che bastino i divieti a garantire. Invece, per esempio, in azienda potrebbe esserci un giustificatissimo motivo per usare Facebook. E piuttosto chi lo usa dovrebbe avere una parte di responsabilità, che significa poter dimostrare l’utilità del proprio operato piuttosto che pagare di persona per avere buttato tempo retribuito.

Se però Apple mette avvisi sullo scaricamento di certe app – non divieti, avvisi – ecco che il sistema chiuso, la censura, i bacchettoni, mica come Android dove lì fai quello che vuoi (che neanche è vero).

Quando invece, più giustamente, su App Store si tengono fuori gli estremi e poi, dove il confine è più morbido, si divide la responsabilità con chi utilizza.

