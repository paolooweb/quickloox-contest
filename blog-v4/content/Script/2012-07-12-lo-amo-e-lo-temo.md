---
title: "Lo amo e lo temo"
date: 2012-07-12
draft: false
toc: false
comments: true
categories: [ ]
tags: [script]
---
(raccomando la visione e la lettura integrale sul sito Asymco).

2007, su iPod e iPhone.

Stiamo giocando duro sull’innovazione. Ok, non siamo davanti. Apple è davanti ma, eh sì, alla fine dovrà stare al passo con un’agenda che anche noi stabiliamo.

2010, sulla concorrenza a iPad.

Mi chiedete se vedremo tavolette? Sì. Che processore avranno? Un processore Intel almeno nel futuro prevedibile. Funzioneranno con Windows? Certo. Saranno configurate a puntino? Sì! E ne venderemo come pazzi. Arriveremo sul mercato come pazzi. Abbiamo apparecchi che eseguiranno più applicazioni, che hanno un sacco di contenuto, che hanno tutto quello che potreste desiderare sul pianeta.

2012, parlando di Apple.

Non lasceremo ad Apple alcuno spazio libero da competizione
Non lo faremo.
Nessuno spazio ad Apple che non sia conteso
Abbiamo i nostri vantaggi in produttività
Abbiamo i nostri vantaggi in termini di gestione di impresa, gestibilità
Abbiamo i nostri vantaggi in termini di quando ci si collega all’infrastruttura dei server nell’impresa.
Ma non lasceremo ad Apple nulla che non sia conteso
Non il cloud per i consumatori
Non l’innovazione di hardware e software
Non lasceremo ad Apple niente di questo
Non accadrà
Non nel nostro quadrante.

Da una parte auguro a quest’uomo di restare in carica per il tempo più lungo possibile.

Dall’altra, uno capace di fare poesia epica involontaria, è capace di tutto.

