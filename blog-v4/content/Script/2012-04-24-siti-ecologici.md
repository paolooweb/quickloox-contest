---
title: "Siti ecologici"
date: 2012-04-24
draft: false
toc: false
comments: true
categories: [ ]
tags: [script]
---
A sinistra un avviso si vede al meglio in un browser WebKit come Safari o Chrome.

A destra una fascetta che recita Solo formati aperti – questo sito è libero da Flash.

In mezzo Text Tools, uno strumentino gratuito che colma una lacuna dell’ecosistema.

Voglio più pagine come Mars Themes. Fanno bene all’ambiente, quello del web.

