---
title: "Il troppo defocalizza"
date: 2012-04-17
draft: false
toc: false
comments: true
categories: [ ]
tags: [script]
---
Ieri si domandava fintamente a Jonathan Ive, il designer di Apple, Perché ogni anno appare un modello di iPhone, a parte la memoria o il colore, e non invece dodici o tredici diversi in dimensioni, funzioni, interfaccia, batteria eccetera, come fanno altri costruttori?

Ive rispondeva che avere come priorità obiettivi interni aziendali che prescindono dal riguardo per il cliente è sbagliato.

Sul New York Times è apparso un articolo dedicato ai travagli di Sony, che non registra profitti dal 2008 e nel 2011 ha perso 6,4 miliardi di dollari. Ecco un brano:

L’azienda propone tuttora un catalogo che confonde, di gadget che si sovrappongono o addirittura si cannibalizzano a vicenda. E ha lasciato che le linee di prodotto continuassero a spuntare come funghi: dieci diversi camcorder di livello consumer e quasi trenta diversi televisori, per esempio, affollano e confondono la scelta.

Sony produce troppi modelli e per nessuno di questi può dire “qui c’è dentro la nostra tecnologia migliore e più evoluta”, dichiara Yoshiaki Sakito, già dirigente Sony. Apple, dall’altra parte, produce uno straordinario telefono in due colori e nient’altro, e dice “questo è il migliore.”

Steve Jobs capì che era meglio avere quattro buoni prodotti che quindici linee confuse nel 1997.

