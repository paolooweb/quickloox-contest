---
title: "Dovrebbe bastare un qlik"
date: 2012-06-13
draft: false
toc: false
comments: true
categories: [ ]
tags: [script]
---
Non è una novità che lo sport costituisca un eccellente veicolo di marketing. Né che l’era dei computer metta l’accento sulle applicazioni di analisi dei dati. Di quelli che credono di favorire il pargolo insegnandogli il mestiere da piccolo, in modo che si troverà a proprio agio quindici anni dopo? Invece che metterlo su Excel come fanno i vecchi, metterlo su R. Rischia di acquisire metodi e conoscenze che torneranno utili.

Ma divago. Dicevo di sport e applicazioni di analisi dati, o business intelligence che dir si voglia. Sono saliti di recente alla ribalta due prodotti dei tanti, Roambi e Qlikview. Ambedue dispongono di una app gratuita su iOS.

Roambi mette a disposizione una serie di dati sui vincitori delle medaglie olimpiche recenti. Da iPhone, iPod touch o iPad, si carica da Safari la pagina http://www.roambi.com/infostrada e i dati si inseriscono automaticamente nell’interfaccia di Roambi.

Qlikview ha scelto invece gli Europei di calcio. Uno si collega a http://www.qlikview.com/kickitandqlikit e ottiene una pagina web dentro cui dilettarsi con statistiche e serie storiche.

La pagina non è minimamente ottimizzata per il touch e neanche per gli schermi: su iPhone o su un MacBook Pro Retina da 2.880 x 1.800 pixel, la pagina è sempre quella.

Ad aprire la app Qlikview e inserire l’indirizzo della pagina nell’elenco dei server, non funziona niente e arriva un errore di collegamento. Fare la stessa cosa con il lunghissimo indirizzo effettivo della pagina genera un ciclo infinito che imbambola la app.

Forse bisogna seguire un altro percorso, ma se c’è non l’ho trovato. All’apparenza, i dati messi a disposizione via web sono irraggiungibili dalla app.

Se domani avessi bisogno di fare business intelligence da iPad e dovessi scegliere tra Roambi e Qlikview, non avrei il minimo dubbio.

