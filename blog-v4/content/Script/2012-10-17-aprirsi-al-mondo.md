---
title: "Aprirsi al mondo"
date: 2012-10-17
draft: false
toc: false
comments: true
categories: [ ]
tags: [script]
---
Una bugia diventa verità a forza di ripeterla, giusto? Infatti ci arrabbiamo perché iTunes ancora non vende in Italia i programmi televisivi. E leggiamo che Apple fa sistemi chiusi, al contrario di altri che li fanno aperti.

Verificare, prego, alla luce di questo straordinario studio degli ecosistemi di intrattenimento pubblicato da MacStories, che mette a confronto Amazon, Apple, Google e Microsoft.

Attraverso grafici Html5 di rara chiarezza e senza nessun bisogno di inglese avanzato si vedrà a che punto è la copertura del mondo per chi vende musica, film, libri, applicazioni.

Attenzione al grafico Service Interoperability, costruito per mostrare chi offre maggiore apertura dei propri servizi ai concorrenti. Guardare Apple, la chiusa, contro Google e Microsoft, le aperte.

Frasetta vicina alla fine della pagina, che mi permetto di usare come conclusione mia:

Cinque anni fa, Apple era l’unica a produrre e vendere apparecchi, sistema operativo e servizi di intrattenimento.

