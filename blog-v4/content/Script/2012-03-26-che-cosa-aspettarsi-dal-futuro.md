---
title: "Che cosa aspettarsi dal futuro"
date: 2012-03-26
draft: false
toc: false
comments: true
categories: [ ]
tags: [script]
---
Riccardo Campaci spiega dottamente quando arriverà iPhone 5: in estate o in autunno. Cioè tra giugno e dicembre.

Certo, ci vogliono competenza e informazione. È dal 2007 che esce un iPhone l’anno nel secondo semestre (fatta eccezione per il 2007, giugno), succederà anche nel 2012. Non è una straordinaria sorpresa?

Mike Elgan titola Finalmente un iPad veramente magico.

E parla della vera novità del nuovo iPad (no Campaci, non si chiama iPad 3 e nemmeno iPed), cioè Bluetooth 4.0. Elgan descrive brevemente le caratteristiche della nuova versione dello standard e di come porti possibilità decisamente interessanti che prima invece no, come consentire l’apparentamento tra due apparecchi senza interazione umana (purché sia stato concesso il permesso in precedenza) e lo scambio automatico di dati di configurazione. In pratica, domani i nostri apparecchi potrebbero teoricamente collegarsi in automatico a una rete Wi-Fi che ci dà il permesso di farlo senza bisogno di dover inserire manualmente una password.

Elgan si lancia in avanti e arriva a prefigurare l’idea che gli apparecchi iOS presenti e futuri possano diventare telecomandi di Apple Tv capaci ovviamente di funzionare anche a voce (Siri), con una interazione tra area televisiva e area informatica che oggi, per quanto, non è ancora possibile.

Ci prende Elgan, o manca il bersaglio? Lo sapremo più avanti, probabilmente più avanti di quanto ci vorrà per avere iPhone 5 (e se si chiamasse il nuovo iPhone?). Potrebbe essere tutto sbagliato oppure una ottima previsione – diverso da predizione – di certe prossime mosse di Apple.

Ma quanto è stimolante leggere questo tipo di articoli sul domani. E non nel senso lassativo del termine.

So che l’inglese è una fatica per molti. Ma tutti possono farsi un favore: passare un articolo di valore dentro Google Traduttore. Garantisco, ne uscirà un elaborato italiano di valore superiore alla zuppa all’italiana. Refusi compresi: quando li lascia il computer, almeno viene da ridere.

