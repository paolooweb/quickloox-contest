---
title: "I mercati sono conversazioni"
date: 2012-06-02
draft: false
toc: false
comments: true
categories: [ ]
tags: [script]
---
Questa era la tesi fondamentale del Cluetrain Manifesto, uno dei primi documenti a mostrare una qualche comprensione dei cambiamenti che Internet iniziava a portare nel marketing e nella comunicazione.

Una dimostrazione fin troppo concreta: se piace il font Bariol, lo si può portare a casa nella versione Regular pagandolo un tweet.

Non con un tweet, attenzione.

Le altre varianti – Thin, Light, Bold o tutt’e tre insieme – si pagano ciascuna da un euro a cinquanta euro, a giudizio del compratore.

Anche a livello di marketing, qui di conversazioni possibili ce ne sono a bizzeffe. Quanto vale un tweet, per esempio?
