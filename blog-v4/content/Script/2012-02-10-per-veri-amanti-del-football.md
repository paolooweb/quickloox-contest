---
title: "Per veri amanti del football"
date: 2012-02-10
draft: false
toc: false
comments: true
categories: [ ]
tags: [script]
---
Ancora una volta il Super Bowl è stato una bella festa di sport, con la vittoria dei New York Giants al termine di una gara decisa da un touchdown all’inizio dell’ultimo minuto e che avrebbe potuto cambiare esito fino all’ultima azione nell’ultimo secondo. Come sempre è stata anche una festa della pubblicità video: le aziende si sono disputate a prezzi altissimi anche il più piccolo interstizio nello spettacolo pur di comunicare il loro messaggio al pubblico più ampio possibile, spesso con spot studiati appositamente per l’occasione, con creatività spinta al massimo come le accelerazioni dei running back o i passaggi al millimetro di Eli Manning.

Qualcuno l’ha presa malissimo, a prescindere. A Lance Ulanoff di Mashable è rimasto sullo stomaco che Apple non abbia fatto pubblicità durante la partita.

Dice che Apple ha un ritardo hardware, che iPhone 4S non è un iPhone 5. Batte tutti i record di vendita, ma non è iPhone 5 e quindi non va bene. E iPad? Che vergogna, invece che fare uscire un iPad 3 Apple vende a camionate iPad 2, quindi qualcosa non gira bene. Ci fosse stato Steve Jobs, argomenta, Apple avrebbe trasmesso un super spot, come quello del 1984 che annunciò Macintosh. Incurante del fatto che sono passati appena ventotto anni e forse qualcosina è cambiato, Ulanoff si chiede quale sia il piano di Tim Cook. Visto che al Super Bowl non c’è stato uno spot Apple.

La risposta l’hanno avuta i saggi e i pazzi che alle quattro del mattino italiane guardavano le immagini della premiazione. Il Vince Lombardi Trophy sfilava in mezzo ai Giants (una squadra di football allinea una quarantina di elementi) e un giocatore sì e uno no, inquadrato da tutte le telecamere del mondo nel momento del trionfo, scattava una foto al trofeo appena vinto con la propria fotocamera personale.

Indovinare, prego, su che apparecchio era montata.

