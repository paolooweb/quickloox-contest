---
title: "Che connettore lungo hai"
date: 2012-02-28
draft: false
toc: false
comments: true
categories: [ ]
tags: [script]
---
Suggestivo articolo di Tim Bajarin, presidente di Creative Strategies: iPhone come modello del computing prossimo venturo, nel quale un oggetto di quelle dimensioni contiene processore, sistema operativo, interfaccia e dati. Lo si porta in giro e quando serve lo si tira fuori, lo si collega a schermi e tastiere disponibili, ed eccoci al lavoro o al divertimento, o a quello che si vuole.

Questa visione ha scarsa possibilità di realizzarsi perché Tim Bajarin è un analista di mercato. Fatica a capire come andrà Apple nei prossimi tre mesi e sembra strano che riesca a intuire l’uso futuro degli apparecchi portatili.

Tuttavia una sua considerazione colpisce e lascia aperte le porte all’immaginazione: il connettore a 30 piedini degli apparecchi iOS, oggi, viene utilizzato per non più di due terzi.

Potrebbero esserci in serbo per iPhone e compagni utilizzi che ancora nessuno conosce, a eccezione di qualche progettista.

