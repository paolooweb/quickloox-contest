---
title: "Con gli occhi del domani"
date: 2012-05-11
draft: false
toc: false
comments: true
categories: [ ]
tags: [script]
---
Scriveva Shawn Blanc il 18 luglio 2011, a proposito di iPad come apparecchio per la lettura:

Porta con sé ogni tipo di materiale in un colpo solo: libri e riviste, feed Rss, articoli da Internet che voglio leggere più tardi. La batteria dura per sempre. Grazie alla connessione Internet posso aggiornarmi e acquistare un libro, scaricare una rivista, leggere le ultimissime notizie in qualunque momento io voglia.

Ovviamente c’erano i contro:

Non è semplice da reggere con una mano e anche con due mani diventa pesante dopo un po’. Non posso leggere all’esterno in una giornata di sole. Non ho la qualità dello schermo di un Kindle o di un iPhone.

L’articolo contiene anche molte altre considerazioni interessanti sul ruolo delle riviste nel mondo digitale, che meritano la lettura. Mi fermo a riflettere sul fatto che, letti quasi dopo un anno, i pro restano tutti validi. Dei contro uno resta valido, uno è ancora abbastanza veritiero anche se la qualità dello schermo è migliorata e il terzo è scomparso, grazie allo schermo Retina.

Al che viene da pensare che, quando si giudica un apparecchio, si dovrebbero distinguere i difetti che hanno grandi probabilità di rimanere da quelli che invece molto probabilmente spariranno nelle prossime versioni. Altrimenti si fa la fine di quelli che criticavano il primo iPhone per la sua fotocamera, che è migliorata iterazione dopo iterazione e oggi ha portato vari quotidiani anglosassoni a fornire di iPhone 4 i propri reporter per quei momenti in cui è indispensabile scattare rapidamente senza porsi altri problemi.

