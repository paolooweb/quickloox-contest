---
title: "Apertura sul passato"
date: 2012-07-07
draft: false
toc: false
comments: true
categories: [ ]
tags: [script]
---
Nei dibattiti sulla compatibilità all’indietro e su quanto deve durare il supporto di una tecnologia in un settore dove si chiede cambiamento costante, va ricordato il ruolo dell’open source come opportunità estremamente abilitante per portare su piattaforme vecchie software corrente al meglio di quanto è possibile.

Per esempio, WebKit è open source. In questo modo, anche se Apple non si cura più di Leopard, è possibile avere una versione di Safari il più possibile aggiornata al codice corrente e capace di girare su Mac OS X 10.5.

Non che sia esclusiva di Apple, ovviamente. La stessa cosa accade per Firefox.

Più in generale, scrivevo che mantenere in vita con efficacia sistemi avanti negli anni richiede capacità. Più si è capaci, meglio si cava sangue dalle rape; mentre persone prive di abilità tecniche, più stanno sul nuovo, meglio è.

Ho sempre pensato che quando avrò più tempo libero metterò del tempo nell’apprendere le nozioni base della compilazione, per esempio. Non si ha idea di quanti programmi siano potenzialmente a disposizione di Mac, solo che nessuno si è dato pena di compilare con successo il codice sorgente, liberamente scaricabile, e creare un binario eseguibile con un doppio clic. Richiede qualche conoscenza tecnica ma è fattibile da una persona normale, con un po’ di impegno e magari qualche aiuto sulle mailing list relative allo sviluppo dei programmi in questione.

Non ho tempo libero ora. Però per qualcuno potrebbe essere un progettino interessante per la calma estiva, per giunta foriero di conoscenze e abilità che poi potrebbero pure tornare utili a settembre. Sempre meglio, per passare il tempo, che comprare le incredibili pubblicazioni a dispense che affollano la pubblicità televisiva dopo Ferragosto per quelli che tornano pieni di buoni propositi ma non vanno oltre la raccolta settimanale di nani da giardino e, nel primo numero, Brontolo a grandezza naturale.

