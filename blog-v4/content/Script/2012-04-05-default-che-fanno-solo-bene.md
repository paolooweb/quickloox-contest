---
title: "Default che fanno solo bene"
date: 2012-04-05
draft: false
toc: false
comments: true
categories: [ ]
tags: [script]
---
Se prendesse piede, questo elenco dei comandi defaults disponibili attraverso il Terminale per modificare il comportamento delle applicazioni Mac diventerebbe davvero una cosa eccellente. Magari come sito organizzato, con tanto di ricerca…

Mi rendo conto di avere visto citati innumerevoli comandi defaults e di averli altrettanto rapidamente persi di vista.

Adesso, come minimo, mi impegno a passare qualcosa pure io all’elenco.

