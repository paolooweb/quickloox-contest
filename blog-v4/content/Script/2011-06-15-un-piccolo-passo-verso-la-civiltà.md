---
title: "Un piccolo passo verso la civiltà"
date: 2011-06-15
draft: false
toc: false
comments: true
categories: [ ]
tags: [script]
---
Google ha iniziato a riconoscere nelle proprie indicizzazioni i tag che nelle pagine web consentono di individuare il nome dell’autore di un pezzo.

Sarà un pizzico più semplice attribuire valore a una persona capace e un filino più facile tutelare il suo lavoro dalle copie selvagge e dagli sciacalli del copia e incolla.

È un passo piccolissimo. Eppure sta tornando la tipografia. Se tornassero anche gli autori, tornerebbe una fettina di civiltà che rischia di perdersi.

