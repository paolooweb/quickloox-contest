---
title: "Voci su lion"
date: 2011-06-20
draft: false
toc: false
comments: true
categories: [ ]
tags: [script]
---
Ho trovato divertente questo trucco-aneddoto di Mac OS X Hints: temporaneamente afono a causa di un tremendo mal di gola, il programmatore è andato a fare colazione da Starbucks facendo ordinare al suo posto un AppleScript che sceglie la voce preferita e pronuncia una frase a piacere.

Se facessimo la stessa cosa in un caffè italiano, oggi, avremmo qualche difficoltà. Notoriamente Mac pronuncia in inglese. Ci sono soluzioni, ma costose e che richiedono l’acquisto di software. Oppure dobbiamo ingegnarci a storpiare le parole italiane in modo tale che, pronunciate in inglese, suonino vicine abbastanza a come dovrebbero essere nella nostra lingua.

Una delle novità di Lion è la presenza di fonemi italiani di serie: sono infatti previste le voci di Paolo e Silvia, oltre a Agnes, Albert, Alex e tutto il resto che da anni correda l’apparato di sintesi vocale di Mac.

Insomma, fare ordinare al computer un cappuccino con cornetto non sarà più un problema. Sempre che il barista non chiami l’antiterrorismo, ovvio.

