---
title: "Grande distribuzione"
date: 2012-04-28
draft: false
toc: false
comments: true
categories: [ ]
tags: [script]
---
Chi si ricorda Beppe, che scriveva da un prestigioso quotidiano britannico raccontando di una futura fornitura di iPad a prezzi agevolati per i dipendenti?

Mi ha riscritto:

Ciao Lux, è arrivato il gioiello! A quanto pare ne abbiamo ordinati 500, come dire che il 50% del personale ne ha ordinato uno.

Dedicato a chi pensa siano giocattoli, inutili per lavorare, un fenomeno di moda che tramonterà stile netbook. (Già, i netbook, che sciocca Apple a non farne uno, lo vogliono tutti…).

C’è anche la foto. No comment, in omaggio al prestigioso quotidiano. Ah, questo post è stato scritto in treno, su iPad.

20120427-154934.jpg

