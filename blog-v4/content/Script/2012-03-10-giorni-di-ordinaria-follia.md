---
title: "Giorni di ordinaria follia"
date: 2012-03-10
draft: false
toc: false
comments: true
categories: [ ]
tags: [script]
---
Il New York Times, racconta MacDailyNews, ha pubblicato un articolo di anticipazione sul nuovo iPad titolandolo Apple aggiorna iPad con modifiche modeste.

Uscito davvero il nuovo iPad, l’articolo è stato cambiato in più punti, con il titolo Apple aggiunge al nuovo iPad più velocità e uno schermo più nitido.

Senza informare i lettori che l’articolo era cambiato.

Apparentemente sembra impossibile trattare normalmente una normale uscita di prodotto.

Il Financial Post titola Il nuovo iPad di Apple fa poco per contrastare l’avanzata dei rivali.

Sull’avanzata dei rivali: vorrei scommettere del denaro su quanto tempo passerà prima che appaia un’altra tavoletta con schermo di risoluzione pari o superiore a quella del nuovo iPad. Io dico più di nove mesi. E quanto tempo prima che appaiano altre due tavolette? Io dico più di quindici.

La cosa meno folle di tutti l’hanno scritta i fantastici folli di The Onion: si intitola Questo articolo sta generando migliaia di dollari di pubblicità semplicemente per il fatto di menzionare il nuovo iPad.

Qualcuno che ha capito tutto e ci ride sopra. Una rarità da tesaurizzare.

