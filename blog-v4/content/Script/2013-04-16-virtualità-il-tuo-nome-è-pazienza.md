---
title: "Virtualità, Il Tuo Nome È Pazienza"
date: 2013-04-16
draft: false
toc: false
comments: true
categories: [ ]
tags: [script]
---
Dopo molti mesi ho avuto nuovamente bisogno di un OS X virtualizzato (eseguito dentro una macchina software, come finestra all’interno del sistema operativo principale). Le vecchie macchine virtuali VirtualBox tuttavia hanno rifiutato di avviarsi.

Serviva un aggiornamento di VirtualBox, ma neanche quello ha giovato. Il vero guaio è che l’attuale versione di VirtualBox non riesce a creare una macchina virtuale OS X funzionante.

Non va bene anche perché è tutto legale; in quanto possessore di una copia legale di Mountain Lion, come minimo posso virtualizzarla due volte per uso personale.

Dopo avere rimediato l’emergenza mediante la copia dimostrativa valida trenta giorni di Vmware Fusion (che funziona a meraviglia), ho trovato soccorso nella pagina OS X on OS X.

L’autore descrive una procedura complessa dall’effetto molto semplice: girare attorno a una estensione di sistema che al momento blocca tutto su VirtualBox.

Sono riuscito a complicare le cose la mia parte, primariamente dimenticandomi di uno slash aggiunto incautamente dal Terminale in fase di completamento. uno scambio di mail con l’autore con la pagina e lui è stato così acuto da capire come e dove ero stato ottuso, così si è risolto tutto.

Non metto volutamente ulteriori approfondimenti perché per arrivare in fondo bisogna avere dimestichezze varie, dall’inglese al Terminale. Facilissimo che sorgano casini e non basta la capacità di seguire istruzioni; ci vuole un livello sufficiente di comprensione di ciò che sta accadendo (e di attenzione superiore al mio).

Ora però il mio Mountain Lion è perfettamente virtualizzato. Sperando che aggiornamenti futuri non diano fastidio. O risolvano alla radice il problema riportando la compatibilità tra OS X e VirtualBox.

