---
title: "Memoria di sistema"
date: 2012-05-22T12:12:16+02:00
draft: false
toc: false
comments: true
categories: [ ]
tags: ["script"]
---
Mi segnala Fabio che il Museo Nazionale della Scienza e della Tecnologia Leonardo da Vinci è alla ricerca di oggetti prodotti da Apple per arricchire la propria collezione.

Sulla pagina compare un magnifico le espone ai fini si studio, perché gli oggetti Apple fanno gola e l’italiano invece non merita di essere conservato.

Faccio tuttavia la mia parte di bravo cittadino coscienzioso e dono la mia parte di memoria (il tuo dono vale un patrimonio, spiegano).

Esiste un museo Apple italiano da dieci anni. Si chiama All About Apple e tra l’altro espone proprio al Museo Nazionale della Scienza e della Tecnologia fino al 10 giugno.

Hai un oggetto Apple del passato o del presente, che ti fa piacere donare a una collezione di valore culturale e storico? Donalo a loro. Quelli di All About Apple sono gente vera, competente, appassionata, intraprendente e straordinaria umanamente e amicalmente. In più si autofinanziano – accettano volentieri il cinque per mille – invece che pesare sulla comunità.

Gli altri sono burocrati mantenuti dal sistema a spese di chi non può scegliere. Si accorgono di Apple adesso.

Un museo degno della maiuscola doveva accorgersene dieci anni fa. Se ci sono riusciti ragazzi entusiasti, avrebbe dovuto essere facile per gente del mestiere. Peraltro di mestiere bisogna anche averne uno vero.
