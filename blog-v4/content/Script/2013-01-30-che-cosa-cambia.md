---
title: "Che cosa cambia"
date: 2013-01-30
draft: false
toc: false
comments: true
categories: [ ]
tags: [script]
---
Espositore da negozio sagomato su misura per iPad, con tanto di alloggiamento per connettore.

Espositore con iPad
Chi lo avrebbe pensato nel 2009?
Si può discutere sulle specifiche o sul prezzo o sui pollici, meno su che cosa abbia effettivamente iniziato a cambiare le abitudini e la vita reale. Anche se è solo un oggetto.

