---
title: "Di chi non vuol scattare"
date: 2012-06-06
draft: false
toc: false
comments: true
categories: [ ]
tags: [script]
---
Torno sul tema di chi non vede perché non può e non perché non vuole.

Stephen van Egmond racconta della propria visita a un gruppo di non vedenti di Toronto, costituito in club di utenti VoiceOver e si chiede quale sia il senso di permettere a un cieco di scattare fotografie.

Ho trovato almeno sette risposte diverse, mentre la sua è “solo” Apple non solo si cura di creare applicazioni accessibili; ma le crea realmente.

Il tutto perché si è meravigliato davanti alla scena di una donna non vedente che scattava con iPhone una fotografia perfettamente inquadrata.
Salta fuori che le applicazioni Apple – Calendario, Messaggi, Mail, iPhoto, perfino Mappe e la più sorprendente di tutti, Fotocamera – sono completamente usabili da non vedenti. Queste applicazioni non fanno uso di alcun ingrediente di programmazione segreto. Usano la stessa architettura software per l’accessibilità cui abbiamo accesso anche voi e me.

Dentro l’articolo si trova anche una stroncatura della pessima abitudine di scrivere app e poi una breve guida a come scattare l’istantanea di un ritratto senza usare la vista, con un iPhone e la sua fotocamera.

Viene da dire: per me che ci vedo, che mi interessa sapere quanto siano accessibili le applicazioni? Risposta: pensa alla cura generale che è stata posta nel prodotto, se questi sono i dettagli.


