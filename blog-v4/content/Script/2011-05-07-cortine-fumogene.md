---
title: "Cortine fumogene"
date: 2011-05-07
draft: false
toc: false
comments: true
categories: [ ]
tags: [script]
---
È passato più di un anno e mezzo da quando Microsoft, senza avere in mano niente di concreto in argomento tablet, divulgò le chiacchiere nella forma degli (splendidi) video concept di Courier, un sistema articolato in due tavolette messe a quaderno, da usare con lo stilo oppure con le dita, a mano libera oppure per produrre documenti strutturati, perfetto per assemblare note e appunti con scrittura, audio, video, immagini, pagine web, ritagli e quant’altro.

Dopo avere alzato la cortina di fumo, Microsoft ammise che non se ne sarebbe fatto niente. Si ricordi che si tratta di un’azienda da sessanta-settanta miliardi annui di fatturato, con quasi centomila dipendenti e spese di ricerca e sviluppo di miliardi e miliardi di dollari ogni anno.

Beh, due ragazzi sono su Internet a cercare soldi per finanziarsi lo sviluppo di Taposé, app per iPad che proponga esattamente le funzioni del favoloso Courier. Li hanno trovati: hanno già raccolto più di quattordicimila dollari a fronte di un obiettivo iniziale di diecimila.

Courier si farà probabilmente grazie a due ragazzi e diecimila dollari chiesti in giro su Internet. Certo, il lavoro hardware lo ha già fatto Apple. Ma a questo punto viene da chiedersi chi avesse veramente lavorato su Courier in Microsoft, oltre al regista dei video e al reparto cortine fumogene.

