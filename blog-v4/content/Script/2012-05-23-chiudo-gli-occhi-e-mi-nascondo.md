---
title: "Chiudo gli occhi e mi nascondo"
date: 2012-05-23
draft: false
toc: false
comments: true
categories: [ ]
tags: ["script"]
---
Dell ha riportato un bilancio trimestrale in declino del quattro percento su quello del trimestre analogo dell’anno precedente.

Apple, nello stesso trimestre, ha accresciuto il fatturato del 59 percento.

Dell ha visto un lieve aumento del giro di affari nel settore di server e reti (due percento) e una notevole crescita delle unità disco di sua produzione (non quelle rivendute), del 24 percento. Ma il reparto consumatori è caduto del dodici percento e il fatturato dei computer mobile è sceso del tredici percento.

Questo è un brano della spiegazione dei dati come li ha presentati Brian Gladden, Chief Financial Officer di Dell:

Il nostro business dei notebook si è contratto per via di un ambiente maggiormente competitivo e aggressivo, soprattutto sui mercati emergenti e sulla fascia bassa. Crediamo che in parte questo sia dovuto alla ricostituzione dei livelli di inventario, dopo i problemi degli ultimi due trimestri con i dischi rigidi [la cui produzione globale è diminuita a causa di varie catastrofi naturali che hanno danneggiato fabbriche chiave]. In aggiunta, vediamo che aumenta la spesa dei consumatori diretta verso apparecchi di elaborazione mobile alternativi.

Proprio così. L’originale recita alternative mobile computing devices.

Forse credono che, evitando di pronunciare quelle quattro letterina una dietro l’altra, l’oggetto delle loro preoccupazioni sparirà nel nulla e si sveglieranno da una nottata un po’ pesante.
