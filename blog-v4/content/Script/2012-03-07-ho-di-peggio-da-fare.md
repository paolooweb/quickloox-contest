---
title: "Ho di peggio da fare"
date: 2012-03-07
draft: false
toc: false
comments: true
categories: [ ]
tags: [script]
---
C’è sempre qualcuno a ricordarmi della possibilità di trovarsi in chat Irc a chiacchierare in diretta di un evento Apple.

Stavolta è stato Gand e così sono entrato nel canale #freesmug, ad aspettare le 19.

In passato ho già scritto diverse volte su come collegarsi in chat Irc, per esempio in questa occasione. Cercando freesmug sul motore di ricerca di Script dovrebbero saltare fuori altre occorrenze.

Preannuncio che ho sul collo il fiato di alcune scadenze pressanti e dunque sarò presente, ma non assiduo. Un saluto però non lo si nega a nessuno.

Dei tanti siti che fanno liveblogging dell’evento, proverò a seguire The Verge.

Se servono suggerimenti, faccio da buttadentro nella stanza iChat/Messaggi ipad2s. Serve solo a quello, non condurrò assolutamente conversazioni lì.

A dopo!

