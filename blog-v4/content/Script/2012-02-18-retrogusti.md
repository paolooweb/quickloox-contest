---
title: "Retrogusti"
date: 2012-02-18
draft: false
toc: false
comments: true
categories: [ ]
tags: [script]
---
Normalmente le rielaborazioni grafiche di hardware e software sanno un po’ di tappo.

Come eccezione, sono disposto a mettere insieme un ipotetico iOS del 1986 con una custodia per iPhone a tema Macintosh.

I produttori di quest’ultima, Schreer Delights, sono deliziosamente fuori di testa e se avessi da regalare una custodia a qualcuno probabilmente passerei per prima cosa da loro.

Su una nota meno frizzante e più produttiva in fatto di design, va assolutamente scorsa la storia di Piezo di Rogue Amoeba nella sua evoluzione da idea a prodotto finito. Progettare software e interfacce poco si addice alla discussione da pausa caffè.

