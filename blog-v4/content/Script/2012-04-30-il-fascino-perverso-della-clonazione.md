---
title: "Il fascino perverso della clonazione"
date: 2012-04-30
draft: false
toc: false
comments: true
categories: [ ]
tags: [script]
---
Ho spiegato perché HyperCard ha segnato una splendida stagione e che la stagione suddetta è passata.

Tuttavia è giusto dare visibilità a chi ritiene ci sia ancora spazio per quell’approccio: su App Store ho individuato BayCard, 15,99 euro con player gratuito a parte. Un clone di HyperCard che sembra abbastanza moderno, completo e fedele all’idea originale.

Dal sito di Bayhoff è anche scaricabile una demo valida un mese.

Rimangono inoltre considerabili le altre possibilità già note: SuperCard, PythonCard, HyperStudio, LilCard eccetera. Gli ambienti variano da applicazioni commerciali a cornici open source dove bisogna scriptare non poco per avere qualcosa di valido. A livello di apprendimento, tuttavia, un PythonCard è raccomandabilissimo, per imparare Python.

Dove pensavo ci fosse Revolution, uno dei cloni storici e più importanti, ho trovato LiveCode, che promette di creare presto e bene app per iPhone e Android. Segno dei tempi.

