---
title: "Tutto non compreso"
date: 2012-06-05
draft: false
toc: false
comments: true
categories: [ ]
tags: [script]
---
Ogni tanto si fanno i conti con la dura realtà.

A Fabio si è fermato iPad durante il fine settimana. Abbiamo provato a rianimarlo in tutti i modi possibili, fino alla modalità Dfu, senza successo.

Difatti la macchina era veramente guasta ed è stata sostituita in garanzia all’Apple Store.

E questo è stato il commento di Fabio:

Al di là del problema tecnico (può capitare che una macchina nasca male), quello che mi stupisce e che la gente non considera, non è tanto la soluzione, ma il fermo macchina praticamente inesistente: appuntamento telefonico, diagnosi, sostituzione, iCloud. Pensa se avevi un fantastico Galaxy…

Del Galaxy ha parlato lui. Mi limito a fare presente che ci sono cose comprese nel prezzo, usualmente non comprese nella valutazione del prezzo.

