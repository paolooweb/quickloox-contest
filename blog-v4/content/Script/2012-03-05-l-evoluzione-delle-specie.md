---
title: "L evoluzione delle specie"
date: 2012-03-05
draft: false
toc: false
comments: true
categories: [ ]
tags: [script]
---
Un complesso di circostanze mi ha portato per la terza volta in pochi anni all’Hotel du Dragon a Parigi.

La prima volta non avevano connessione Internet per gli ospiti. La seconda volta avevano una base di accesso Wi-Fi nella lounge. Quest’anno c’è rete senza fili in tutte le stanze, compresa nel prezzo. Non capisco perché vi sia il limite di una sola macchina collegata, però poco importa.

Scrivo queste note con WordPress su iPad. La prima volta che ho provato a usare la app ho rinunciato, tanto era instabile e bacata. La seconda volta sono riuscito faticosamente a pubblicare un post semplice. Oggi mette a disposizione un potenziale ragionevolmente adeguato rispetto alla usuale interfaccia web.

La prima volta che sono stato qui avevo un iPhone 2G, rivoluzionario rispetto al concetto di navigare con un cellulare e però improponibile di fatto nel ruolo di terminale per accedere a WordPress. La seconda avevo un iPad con iOS 4, che permetteva di bloggare con fatica supplementare minima rispetto a un Mac. Ora, con iOS 5 e – esempio principale – la tastiera software ricollocabile in altezza e spezzabile a metà per favorire la digitazione con i pollici, posso veramente riposarmi a letto prima di dormire e intanto comporre un post.

L’evoluzione informatica è una gran cosa.

