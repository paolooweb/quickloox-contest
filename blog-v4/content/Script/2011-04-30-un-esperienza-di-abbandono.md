---
title: "Un'Esperienza Di Abbandono"
date: 2011-04-30
draft: false
toc: false
comments: true
categories: [ ]
tags: [script]
---
Un MacBook Pro lasciato in funzione per dodici giorni senza intervento umano, con diciotto applicazioni in funzione e, in particolare, Safari con una settantina di pagine aperte.

Al termine dei dodici giorni, il Mac è relativamente poco reattivo. Un’occhiata con Monitoraggio Attività ed è chiaro che Safari ha ancora qualche leak, perdita di memoria, da aggiustare: da solo si è preso oltre quattro gigabyte di Ram residente.

Molte finestre aperte, vero, ma è solo parte del problema. Dopo averle chiuse una a una, il programma nudo occupa due gigabyte e mezzo di Ram, decisamente più dell’usuale.

È sufficiente chiudere il programma e farlo ripartire per recuperare tutta la Ram intasata e riportare il Mac al proprio splendore naturale.

Peraltro, capitasse di saperlo in anticipo, occhio a Safari. Se il computer deve rimanere al massimo delle prestazioni per molti giorni incustodito – il leak non crea problemi, se non di qualche lentezza – è il caso di valutare l’uso di un browser alternativo.

