---
title: "Attenti, Riposo"
date: 2012-02-12
draft: false
toc: false
comments: true
categories: [ ]
tags: [script]
---
Reduce da due giorni massacranti ed estremamente appaganti passati a dare una mano in diretta alla comunicazione di una multinazionale.

Non posso approfondire i dettagli, ma si è parlato moltissimo di Apple e anche Apple ha parlato della propria offerta. Anche solo cinque anni fa non sarebbe mai accaduto.

A un certo punto hanno posto una domanda a una platea di quasi cento persone e più della metà hanno riferito di avere familiarità con iPad. Anche solo due anni fa non ci sarebbe stata alcuna possibilità.

I negazionisti a tutti i costi, quelli che il Mac in azienda non ha modo di essere, hanno di che stare attenti. La marea si sta alzando.

Nel frattempo mi riposo e allego un’istantanea presa davanti al banco della regia. Sarebbe accaduto anche dieci anni fa; la differenza è che adesso, invece di essere l’eccezione all’interno di una sala gremita, si avvicinano a una nuova regola.

