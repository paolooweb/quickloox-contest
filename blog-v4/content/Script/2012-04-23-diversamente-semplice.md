---
title: "Diversamente semplice"
date: 2012-04-23
draft: false
toc: false
comments: true
categories: [ ]
tags: [script]
---
Dopo molto tempo ho rifatto un giro sulla pagina Perché HyperCard doveva morire.

Contiene una eccellente spiegazione visiva del perché HyperCard fosse un eccezionale strumento per programmare destinato a chi non sapeva programmare, con tanto di puntatori alle risorse per provare il brivido di HyperCard anche oggi, in opportuno emulatore.

E alla fine si chiede perché HyperCard sia stato accantonato anni fa. La risposta fornita è si trattava di un mondo nel quale era stata indebolita la distinzione tra uso del computer e sua programmazione, visione che non faceva piacere a Steve Jobs.

È passato molto tempo e ho le idee vagamente più chiare.

Prima di tutto, era un mondo dove non c’era Internet. Si confronti l’esempio fornito – creazione di una piccola calcolatrice – con quello che è possibile fare su una pagina web, grazie a JavaScript. Sia in un esempio che nell’altro viene fornito il codice. Più che la complessità del codice, tutto sommato paragonabile, va posta attenzione alla facilità con la quale è possibile scoprire e fare proprio codice funzionale. Ai tempi di HyperCard si scopriva la programmazione grazie alle doti del programma; oggi, grazie a Google e al fatto che il codice delle pagine web è aperto e leggibile. Copiare, smontare e riutilizzare è l’inizio dell’imparare.

Servono altri linguaggi? Una ricerca e si trova tutto. In pochi secondi ho rintracciato il codice per creare una calcolatrice con notazione polacca inversa in Common Lisp.

Insomma, la facilità non è più la stessa di prima, ma non è inferiore. Si è spostata dall’esplorabilità alla reperibilità.

E il pendolo non ha ancora terminato di oscillare. Aperto il vaso di Pandora di Html, Css, JavaScript eccetera, arriveranno strumenti che faciliteranno anche la pura attività di programmazione. Automator, per esempio, non è HyperCard, ma permette di togliersi soddisfazioni non da poco.

HyperCard ha segnato una stagione affascinante. E passata per sempre. Oggi viene chiesto di più, sì. Ma perché si può fare molto di più.

