---
title: "Nessuna garanzia"
date: 2012-06-11
draft: false
toc: false
comments: true
categories: [ ]
tags: [script]
---
Alle 19, se ho fatto giusto il conto dei fusi orari, comincia il grande spettacolo della Worldwide Developer Conference 2012 di Apple. Attesa per nuovi Mac, nuovo OS X, nuovo iOS e chissà che altro.

Mi troverò in chat Irc sperando di poter sempre sfruttare lo spazio del canale #freesmug sul server irc.freenode.net.

Sono in connessione cellulare e in ambiente balneare; più per la prima motivazione che per la seconda, non ho idea di quanto e come riuscirò a stare collegato.

Qui sotto si trovano un po’ di istruzioni sommarie prese da un vecchio post, che potrebbero anche non essere del tutto aggiornate rispetto ai programmi e alle possibilità. Nell’intervallo, un video tratto dalla recentissima Norwegian Developers Conference di Microsoft, tanto per capire la differenza.



Chiacchiera libera dalle 19 sul canale Irc #freesmug, server freenode.net.

Rapidissimo: ecco i comandi Irc utili una volta collegati. Il comando per entrare in chat, una volta usato il programma Irc per collegarsi a irc.freenode.net, è /join #freesmug.

Programmi Irc per Mac: X-Chat Aqua, Colloquy, Conversation, JIrcii, simple irc…, Adium e altri.

App Irc per iPhone e app Irc per iPad.

Chi c’è… a dopo e un grande grazie a Gand per la graditissima ospitalità!

