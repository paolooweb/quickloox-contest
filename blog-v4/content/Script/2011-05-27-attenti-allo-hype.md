---
title: "Attenti allo hype"
date: 2011-05-27
draft: false
toc: false
comments: true
categories: [ ]
tags: [script]
---
Hype di Tumult, Strumento di creazione di contenuti multimediali e animazioni in Html5, 23,99 euro su App Store (Mac).

Attenzione, attenzione. Può diventare una cosa grossa. Per chi ha bisogno di cose del genere, è già un acquisto che neanche si discute.

