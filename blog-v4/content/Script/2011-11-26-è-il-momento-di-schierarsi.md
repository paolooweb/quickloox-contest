---
title: "È Il Momento Di Schierarsi"
date: 2011-11-26
draft: false
toc: false
comments: true
categories: [ ]
tags: [script]
---
Dopo circa quindici anni, credo che dovrò cambiare programma di posta elettronica.

Apple ha staccato la spina al supporto del protocollo di lettura Pop da parte di iCloud e riconosce solo il protocollo Imap. Mailsmith, che uso appunto dal 1996 o già di lì, ritira la posta con il solo Pop e non è previsto l’aggiornamento a iMap, a meno di una riscrittura radicale del programma. Così Mailsmith non ritira la posta iCloud.

Rich Siegel, l’anima di Bare Bones Software e curatore di Mailsmith, non ha mai diramato annunci sul futuro di uno dei suoi programmi e normalmente basta attendere per essere accontentati su qualsiasi aspetto o quasi, come dimostra la storia di BBEdit. Questa volta però il rischio che non succeda è alto e soprattutto, con decine di messaggi di posta al giorno da gestire, non ho tempo di aspettare.

Rinunciare a Mailsmith mi pesa moltissimo per tutta una serie di vantaggi. Compatibilità AppleScript ai massimi, filtri Unix, flessibilità estrema e molto altro ne hanno fatto la mia scelta principe per una gestione professionale della posta elettronica.

Potrei certamente organizzare accrocchi basati sull’inoltro attraverso Gmail, ma perderei in efficienza ed eleganza della soluzione. La versione web della posta iCloud è ottima, solo che ovviamente non uguaglia una gestione desktop. Ho in mente certe soluzioni di scripting, che però richiedono perizia e giorni di lavoro dedicato. Mail di Apple è diventato ottimo con Lion, ma non ha quelle funzioni avanzate che su Mailsmith mi fanno la differenza.

Mi sono guardato intorno prima di capire che è inutile. O vado verso la modernità e prendo il non plus ultra in termini di progresso dei programmi di posta attuali, oppure opto per la potenza pura e assoluta infischiandomene delle interfacce e delle varie funzioni smart del programma del momento, a costo di dover imparare qualcosa (che non è mai un costo, semmai un investimento).

In altre parole: o acquisto Postbox oppure vado dentro il Terminale e uso mutt.

Da che parte mi schiero?

