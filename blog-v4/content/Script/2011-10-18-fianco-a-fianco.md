---
title: "Fianco a fianco"
date: 2011-10-18
draft: false
toc: false
comments: true
categories: [ ]
tags: [script]
---
Steve Jobs è stato ricordato in forma privata presso la Stanford Memorial Church da una serie di personalità. Tra i nomi si contano quelli di Joan Baez (cantautrice), Bono degli U2, Bill Clinton (ex presidente degli Stati Uniti), Michael Dell (che sarà stato auspicabilmente zitto), Larry Ellison di Oracle, Bill Gates, Al Gore (ex vicepresidente degli Stati Uniti), Rupert Murdoch e altri.

Domani si svolgerà una cerimonia ufficiale presso Apple, riservata ai dipendenti.

Oggi, siccome la vita continua malgrado tutto, Apple annuncerà i risultati finanziari del trimestre estivo.

Giusto come inquadramento, i dati del grande Horace Dediu di Asymco che confrontano Apple e Microsoft dal 2007 a oggi.

Emerge che il business Mac è arrivato a generare più fatturato del business Windows. E che il business iOS è arrivato a generare più fatturato di tutti i prodotti Microsoft messi insieme.

I due grafici hanno la stessa scala e volendo si possono affiancare per valutare bene gli andamenti.

Quindici anni fa Apple era vicina al naufragio e oggi è in testa alla regata, con grande vantaggio sul secondo (che poi oramai è Ibm, neanche più Microsoft).

Al timone negli ultimi quindici anni è stato un nocchiero che speriamo qualche analista vorrà ricordare, prima di fare la propria domanda ottusa al capocontabile di Apple Fred Oppenheimer.

