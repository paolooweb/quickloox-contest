---
title: "Addensamenti nuvolosi"
date: 2011-06-23
draft: false
toc: false
comments: true
categories: [ ]
tags: [script]
---
Tra iCloud, MobileMe e tutto il resto si parla di nuvola in continuazione e non mancano neanche articoli che parlano delle alternative possibili. Mica di tutte le alternative possibili, però.

Uno bravino può usare ownCloud, open source: solo cento megabyte gratuiti e poi cinque euro al mese per stoccaggio illimitato.

Syncplicity offre due gigabyte gratis con sincronizzazione su due computer, oppure cinque computer sincronizzati e cinquanta gigabyte per 15 dollari al mese.

SpiderOak è allineata sui due gigabyte gratis (a vita, promette) e poi offre incrementi di cento gigabyte per volta, a dieci dollari al mese o cento dollari l’anno per ciascun incremento di cento gigabyte.

Il più bislacco è interessante è probabilmente Tarsnap, creato per i veri paranoici: funziona da Terminale, non supporta Windows se non facendo cose strane con Cygwin e costa trenta centesimi di dollaro per gigabyte per mese di stoccaggio (cento gigabyte per un mese sono trenta dollari al mese, ma venti gigabyte sarebbero sei dollari al mese), più 30 centesimi di dollaro per gigabyte per il traffico. Cioè, se carico su Tarsnap cinque gigabyte di dati pago 1,5 dollari per il caricamento. Poi quei cinque gigabyte mi costano 1,5 dollari al mese. Quando li scarico mi costano altri 1,5 dollari. All’attivazione del servizio viene richiesto un anticipo di cinque dollari.

Uno veramente bravo potrebbe pagarsi un bel po’ di stoccaggio su Tarsnap trovando bug nel sistema: sono previste ricompense da uno a duemila dollari.

