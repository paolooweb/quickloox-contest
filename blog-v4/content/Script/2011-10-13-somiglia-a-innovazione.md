---
title: "Somiglia a innovazione"
date: 2011-10-13
draft: false
toc: false
comments: true
categories: [ ]
tags: [script]
---
Bastano le figure per capire da questo articolo del New York Times che evoluzione abbia attraversato Kindle, il lettore di libri elettronici di Amazon.

Kindle è un prodotto eccellente nel proprio campo. L’evidenza mostra che creare prodotti eccellenti e fare innovazione, ovvero arrivare primi a un risultato, sono cose molto diverse.

