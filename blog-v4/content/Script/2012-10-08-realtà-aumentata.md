---
title: "Realtà Aumentata"
date: 2012-10-08
draft: false
toc: false
comments: true
categories: [ ]
tags: [script]
---
Quando vi siano fonti di luce esterne all’inquadratura, la maggior parte delle fotocamere di piccolo formato può riprendere qualche forma di riflesso sul bordo del fotogramma. Può succedere quando una fonte luminosa sia posizionata a un angolo (solitamente appena fuori dal campo visivo) tale da generare un riflesso sulle superfici interne alla fotocamera e sul sensore. Spostare leggermente la fotocamera per cambiare la posizione di ingresso nell’obiettivo della luce intensa, o schermare l’obiettivo con una mano, dovrebbe minimizzare o eliminare l’effetto.

Tratto da Fotografia e svezzamento. No. Oppure da Storia della fotografia da quando è stata inventata. Neanche. O altrimenti da Quando fai clic sul pulsante delle foto, stai fotografando. Neppure.

È una nota tecnica Apple (l’italiano non c’è ancora mentre scrivo e la traduzione potrebbe essere diversa). Scritta perché la gente ha scattato foto con iPhone 5 e ha visto a volte un riflesso viola.

Riflesso viola, o diversamente colorato, che ha visto innumerevoli volte, scattando con il cellulare, con le macchine convenzionali, nelle foto degli altri, nei documentari, nei libri di fotografia. Ma che è diventato degno di attenzione solo con iPhone 5, anno 2012.

La cosa interessante è che a questo apparecchio si chiedono cose oltre quello che la scienza e la tecnologia sono riuscite a realizzare. Dove quello che è normalissimo su qualsiasi altro apparecchio, diventa problema. Roba da riflessione filosofica, poiché esistono le applicazioni di realtà aumentata, tipo quelle che inquadri il quadro al museo e appare sullo schermo la scheda dell’opera, ma iPhone è un apparecchio saldamente reale e confinato a quello che si può fare nella realtà.

Speriamo in iPhone 6.

