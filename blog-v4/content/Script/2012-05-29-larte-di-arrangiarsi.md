---
title: "L'arte Di Arrangiarsi"
date: 2012-05-29T13:10:36+02:00
draft: false
toc: false
comments: true
categories: [ ]
tags: [script]
---
Un giorno Andrew Gordon, animatore in Pixar, aprì uno sportello situato su una delle pareti dell’ufficio. Celava un condotto. Si mise a seguirlo e sbucò in uno spazio probabilmente creato per consentire l’accesso ai tubi del condizionamento, ma assolutamente non usato.

Giorno dopo giorno, Gordon iniziò ad abbellire quello spazio clandestino. A un certo punto diventò uno spazio semiclandestino, completo di bar, divano e tappeti. Oggi è uno spazio semiufficiale, dove i dipendenti vanno a rilassarsi e nel quale hanno trovato ospitalità ospiti illustri tra i quali Steve Jobs, John Lasseter e molti altri.

Un aspirante imprenditore diciannovenne, Eric Simons, ha vissuto per due mesi da clandestino dentro la sede di America On Line (Aol), dormendo su divani lontani dal passaggio della sicurezza, lavandosi nella palestra, mangiando nella cafeteria interna. La situazione anomala si è creata quando sono terminati i soldi con i quali aveva trovato finanziamento iniziale per il proprio progetto. Adesso è stato cacciato ufficialmente dalla sede Aol, ma ha trovato un’altra linea di credito.

Ho scritto diverse volte di Ron Avitzur, autore di Calcolatrice Grafica. Ora il programma non è più compreso di serie nel software di sistema Mac, ma nel 1994 lo è stato e prima di essere escluso ha trovato posto in oltre venti milioni di macchine. Tutto il suo sviluppo avvenne clandestinamente negli uffici di Apple, con Avitzur che entrava grazie a un badge formalmente non più valido e lavorava di nascosto, aiutato di soppiatto da ingegneri e programmatori.

Il Paese dell’arte di arrangiarsi, della creatività che supera gli ostacoli, della gioiosa anarchia creativa, una volta si diceva fosse l’italia.

