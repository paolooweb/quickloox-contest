---
title: "Linventario della propaganda"
date: 2012-06-10
draft: false
toc: false
comments: true
categories: [ ]
tags: [script]
---
Ogni tanto si accenna al fatto che Apple riscuote i profitti più alti di tutti nei propri settori. Le risposte degli ovvi sono ovvie: succede perché i prodotti costano più degli altri, la produzione avviene in Cina – come se quella altrui invece avesse luogo su Saturno – sostanzialmente perché sotto sotto c’è un po’ la fregatura, dato che, lo sanno tutti, i computer sono tutti uguali.

Allora, per esempio, Gartner (che sul futuro vale meno del mago Otelma e invece sul passato qualcosa lo merita) pubblica annualmente una Supply Chain Top 25, dove supply chain è la catena della fornitura, tutto quello che succede dagli ordini dei materiali fino alla vendita.

Apple è in cima alla classifica con un punteggio composito di 9,69, ben sopra la seconda in classifica, Amazon, che sta a 5,40. McDonald’s segue a 5,37, Dell a 5,30.

Una parte importante della catena delle forniture è la gestione degli inventari. Se le merci rimangono troppo a lungo in magazzino, o se ce ne sono troppe e ci vogliono magazzini più grandi, i profitti decrescono, perché il magazzino è un costo.

Nel caso di McDonald’s, che vende cibo, è ovvio che il magazzino sia pressoché nullo; checché si dica della qualità del fast food, non si può conservare a lungo neanche cibo di quel tipo. Difatti McDonald’s ha 142 rotazioni di inventario ogni anno; vale a dire che ogni due giorni e mezzo, all’incirca e in media, il magazzino di McDonald’s è stato completamente rinnovato.

Apple produce beni elettronici, che si conservano piuttosto bene. Eppure ha 74 rotazioni di inventario l’anno, la metà di McDonald’s. Ogni cinque giorni (cinque giorni) il magazzino di Apple è totalmente rinnovato.

Amazon fa dieci rotazioni l’anno, una ogni cinque settimane.

Ora pensiamo a Dell, l’azienda che ha praticamente inventato la vendita diretta di computer su Internet. Fa quasi 36 rotazioni l’anno, cioè una ogni dieci giorni. La metà di quelle di Apple, che pure ha 360 negozi fisici da rifornire, oltre ai rivenditori. I magazzini di Apple si rinnovano a velocità doppia di quella di Dell.

Tradotto in profitti, a parità di tutto il resto un Mac fa guadagnare più di un Dell, perché rimane meno tempo in magazzino.

Ovviamente, se provi a raccontare che i profitti di Apple dipendono (anche) da una straordinaria efficienza di gestione, sembri quello pagato per fare propaganda.

