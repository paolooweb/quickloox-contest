---
title: "Apple, Conosci Te Stessa"
date: 2011-05-14
draft: false
toc: false
comments: true
categories: [ ]
tags: [script]
---
Il luogo comune vuole che Steve Jobs sia l’anima, il cuore e il cervello di Apple e che nel momento in cui cessasse di occuparsi degli affari della Mela questa andrebbe a catafascio. Dopotutto è già successo una volta, no? Nel 1985 Jobs venne cacciato per tornare in Apple a fine 1996, quando l’azienda navigava in cattivissime acque generali, non necessariamente finanziarie, ma proprio come nave sanza nocchiero in gran tempesta per scriverla con Dante.

Invece non è detto che la storia vada come è già andata; Jobs, tornato in Apple, non ha certo rifatto la stessa Apple del 1985. La Apple del 2011 è diversissima dal modo di procedere di quella del 1991 e del 1981.

E se Jobs, oltre che a creare prodotti, si fosse anche dedicato a creare un’azienda secondo principi codificati e replicabili? In questo caso non sarebbe lui a dare propulsione ad Apple ma piuttosto quest’ultima strutturata secondo le sue indicazioni, e pronta a proseguire il cammino anche quando il suo carismatico amministratore delegato decidesse di ritirarsi.

Se, in altre parole, Apple investisse anche nella conoscenza di sé e dei meccanismi che ne regolano il funzionamento e il successo? Sarebbe un’innovazione da fare impallidire quelle di prodotto. Pochissime altre aziende, fuori dall’enunciazione di qualche carta dei valori o di decaloghi rappresentativi, si studiano per comprendere il proprio funzionamento.

Invece Apple ha varato nel 2008 una iniziativa chiamata Apple University, retta da Joel Podolny, già direttore della Management School di Yale e scusate se è poco.

Secondo un articolo di Fortune che non ho ancora saputo linkare, citato da Horace Dediu di Asymco, a Podolny è stato chiesto di comprendere, sistematizzare e insegnare ai dipendenti il modo in cui funziona Apple.

Conoscenza che, attraverso i trasferimenti di personale, potrebbe piano piano filtrare nelle altre aziende.

È un pensiero il cui ordine di grandezza mette quasi i brividi. Apple potrebbe passare alla storia non solo per Macintosh o per iPad, ma per arrivare a cambiare radicalmente l’imprenditoria sul pianeta.

