---
title: "Una canzone, una lezione"
date: 2012-04-20
draft: false
toc: false
comments: true
categories: [ ]
tags: [script]
---
E per qualcuno navigare su Internet sarebbe una perdita di tempo.

Ho visto nel feed Rss che una dipendente Microsoft si è licenziata esprimendo i propri sentimenti in musica su YouTube.

Sono andato sul suo sito personale.

Lì ho scoperto come abbia lavorato a una semplice app (per iPhone!) che aiuta i residenti nella zona di Redmond a decidere se prendere la superstrada 520 oppure la 90, dilemma comune da quelle parti. Se la app è semplice, progettare la sua interfaccia in modo ottimale lo è stato relativamente e la spiegazione di come il team sia arrivato al prodotto finale è una piccola lezione di design.

Nel post è presente un link a una risposta su Quora del cofondatore di Dropbox Drew Houston, che spiega il motivo per cui Dropbox si fondi su una singola cartella.

Altra lezione di design, a livello più alto. Irresistibile e molto istruttivo il momento in cui si racconta del test condotto con gente presa per strada, i cui fallimenti hanno portato il team a semplificare il semplificabile. Commenta Houston:

È uno dei nostri principî fondamentali di progetto, fare poche cose bene invece che tante a metà (in originale half-assed), e non siamo riusciti a trovare una strada per avere cartelle multiple che non fosse a metà. Gli elenchi comparativi di funzioni sono gare per gli stupidi, senza altro motivo per vincerle che il fatto di esserci dentro.

Nel giro di dieci minuti ero finito dentro qualcosa di buono e ho imparato qualcosa. Perfino partendo da Microsoft, il che è tutto dire.

