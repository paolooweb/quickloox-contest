---
title: "Lo ieri e il domani"
date: 2012-03-12
draft: false
toc: false
comments: true
categories: [ ]
tags: [script]
---
Non è solo una curiosità, la notizia della vendita su eBay di un altrimenti introvabile prototipo di Walt: mostra che a togliere la tastiera dai telefoni qualcuno già ci pensava da un pezzo.

Fa anche commuovere, ogni volta che qualcuno pensa di reintrodurre lo stilo come dotazione permanente degli apparecchi touch. Un’idea serenamente anni novanta.

Nel frattempo Apple ha depositato presso il Comune di Cupertino piani aggiornati del futuro campus a forma di ciambella.

Ho guardato specialmente i nuovi rendering. Folli e affamati, sono gli unici aggettivi adatti. Tutta cosa di domani, neanche lontanissimo.

