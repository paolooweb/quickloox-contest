---
title: "Un mestiere mancato"
date: 2012-05-19T12:07:32+02:00
draft: false
toc: false
comments: true
categories: [ ]
tags: ["script"]
---
In questo periodo ho bisogno di giochi a corto respiro, una pausa di pochi secondi, al massimo un paio di minuti, e poi via di corsa verso le cose da fare.

Per questo sto giocando sempre meno a World of Warcraft – non è colpa sua, è mia – e ho invece aggiunto a New World Colony anche Hero Academy.

Il multiplayer è un’autentica brezza rinfrescante: lo stile di gioco cambia in continuazione perché cambia l’avversario, è possibile fare una mossa nel tempo di un caffè e poi tornare al lavoro, nessuno si offende se passano due giorni prima che io faccia la mossa successiva. Il costo di ingresso è minimo o anche zero, tollerando un po’ di pubblicità.

Se poi piace vincere facile, sono uno stratega fallimentare e ne prendo molte più di quelle che do. Chiedimi l’amicizia in Game Center e quando ti serve una vittoria a portata di mano c’è la vittima sacrificale a disposizione. :-)
