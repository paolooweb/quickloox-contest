---
title: "Scommettere sul servizio"
date: 2012-10-15
draft: false
toc: false
comments: true
categories: [ ]
tags: [script]
---
Bang & Olufsen, i visionari progettisti danesi di elettronica di quasi lusso, ha annunciato che i diffusori BeoPlay A8, già utilizzabili con iPhone via connessione fisica e senza fili tramite AirPlay, sono compatibili anche con il nuovo connettore Lightning di Phone 5.

Non solo: tutti gli acquirenti di BeoPlay A8 potranno ricevere gratuitamente il connettore Lightning suddetto, grazie a una iniziativa che verrà dettagliata a breve.

Uno dirà bella forza, casse portatili con un prezzo consigliato di millecentoquarantanove euro, il nuovo connettore lo voglio consegnato personalmente a casa mia dal signor Bang e dal signor Olufsen accompagnati da un corteo di musicanti e acrobati di strada.

Come dire che si dà per scontato, un livello di servizio del genere, a un prezzo del genere.

A me è venuto da pensare alla scommessa degli Apple Store fisici. Posti dove certo si va a fare acquisti, ma soprattutto dove si va con un iPhone cascato per terra, un Mac con il disco rigido in crisi, un iPad con il vetro rotto, e previo appuntamento si esce dal locale con un apparecchio di nuovo funzionante in mano.

Uno dirà bella forza, i prezzi di Apple sono superiori a quelli degli altri. Può essere, ma non così superiori. Durante il contatto più recente che ho avuto con un proprietario di un Windows Phone ho appreso che l’apparecchio era caduto e non funzionava più l’audio. Può succedere a tutti.

È stato mandato a riparare e, in attesa che torni, il proprietario di cui sopra gira temporaneamente con un cellulare da 19,99 euro, solo telefono e messaggi. Soluzione che a me piace moltissimo in linea di principio, come alternativa allo smartphone.

Come succedanea dello smartphone, è degna di ludibrio. Uso iPhone molto più per posta elettronica, FaceTime, Internet e agenda che per le telefonate; se cadesse e si rompesse qualcosa, sarei felicissimo di prendere appuntamento con un Apple Store e uscirne un’ora dopo con in mano un iPhone funzionante.

Per questo iPhone mi costa di più? Volentieri, grazie.

