---
title: "Apple continua a trascurare mac e pensa solo a iphone"
date: 2011-05-21
draft: false
toc: false
comments: true
categories: [ ]
tags: [script]
---
È da venti trimestri che le vendite di Mac continuano a crescere più della media del mercato. Nel primo trimestre 2011 le vendite di Mac sono cresciute del 28 percento anno su anno, a fronte di una contrazione del tre percento del mercato globale dei computer.

Chissà quando Apple avrà voglia di occuparsi di Mac.

