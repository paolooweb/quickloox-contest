---
title: "La libertà di leggere"
date: 2013-04-14
draft: false
toc: false
comments: true
categories: [ ]
tags: [script]
---
Facciamo finta per un istante che possa interessare qualcuno: Facebook Home, schermata orientata a Facebook per computer da tasca Android, è stata progettata con l’aiuto di Quartz Composer, sistema di prototipazione visiva inserito in Xcode di Apple per Mac.

Ne scopiazza Mauro Notarianni su Macity. La notizia arriva da AppleInsider che riporta quanto bloggato da Julie Zhoe, design director a Facebook. La cita cinque volte.

Anche Manuela Chimera su Comunità digitali riferisce il fatto. Cita Julie Zhoe tre volte e Zohe (refuso) una. Linka a un Composer non esattamente in tema, ma forse ci sono di mezzo centesimi in meccanismi di acquisto indotto.

Problemino: Julie Zhoe non si chiama Zhoe. Si chiama Zhuo. Davvero strano che lo stesso errore si sia misteriosamente propagato uguale su pagine diverse. Fa pensare a certi memorabili compiti in classe delle medie.

Chi sarà scopiazzator de’ scopiazzatori? Manuela Chimera è una preclara esperta di tecnologia, come recita il suo profilo:

Ciao, mi chiamo Manuela e sono un medico veterinario per passione e web writer per caso. Lavoro nel campo dei piccoli animali, occupandomi principalmente di cani e di gatti, ma per interesse personale mi dedico anche ai conigli, ai furetti, alle cavie, ai criceti, ai furetti e alle tartarughe, insomma, agli animali esotici più comuni. Ma la vita non è fatta solo di animali ed ecco che salta fuori il mio amore per la lettura, principalmente per i libri fantasy (che occupano gran parte della casa, fra un po’ mi espanderò nella casa dei vicini, magari facendo un buchino piccolino piccolino nel muro, forse non se ne accorgono), per gli horror, per i manga e i fumetti in generale, ma anche per il cinema, per i videogiochi (qui sono una schiappa stellare, ma mi piacciono lo stesso), per i telefilm fantasy-horror. Mi interessano la medicina, l’astronomia, l’archeologia, mi piacerebbe viaggiare, imparare la scherma medievale e il tiro con l’arco, vorrei andare a cavallo, ma dove lo trovo il tempo per fare tutto?

Mauro Notarianni si occupa di tecnologia da una vita. Chissà dove ha scopiazzato creativamente il cognome sbagliato di Zhuo. La quale non è design director come immagina lui bensì product design director.

Avete ancora la libertà di pensare ma quello non lo fate e in cambio pretendete la libertà di scrivere, cantava Gaber.

Scrivere, oramai, è un disastro compiuto. Ciò che mi preoccupa, con questa gente in giro, è la libertà di leggere.

