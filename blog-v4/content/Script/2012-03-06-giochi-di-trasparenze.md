---
title: "Giochi di trasparenze"
date: 2012-03-06
draft: false
toc: false
comments: true
categories: [ ]
tags: [script]
---
Di passaggio dal Louvre ho dato un’occhiata all’Apple Store locale. Solo stavolta, non era la prima, ho collegato il cristallo delle scale a quello della piramide che connota il museo dal 1989.

In ambedue i casi si tratta di innovazione non cieca, non fine a se stessa, con una base estetica.

Il lavoro di Ieoh Ming Pei ha sfidato tradizioni architettoniche secolari e ha finito per convincere ogni detrattore meritevole di ascolto.

Le scale dell’Apple Store hanno certamente meno ambizioni e suppongo che dureranno di meno. Sono affascinanti lo stesso, nella tensione a eliminare totalmente il metallo, come prima degli anni novanta non si poteva proprio fare.

