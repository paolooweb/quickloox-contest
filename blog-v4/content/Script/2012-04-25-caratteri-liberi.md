---
title: "Caratteri liberi"
date: 2012-04-25
draft: false
toc: false
comments: true
categories: [ ]
tags: [script]
---
Due storie parallele.

Ralph Hermann ha girato il mondo a fotografare i cartelli stradali come ispirazione per il proprio font Wayfinding Sans Pro.

Punto spettacolare: quando racconta che al confine con la Norvegia lo hanno messo in cella perché i doganieri non capivano che uno potesse avere l’unico scopo di fotografare tutti i cartelli stradali che trovava.

Punto di apprendimento: numerosi, compresa una notevole tabella sulle distanza di lettura.

Su iotic.com si racconta la storia di Avería, il font creato dalla media di tutti i font che l’autore aveva sul proprio computer.

Punto spettacolare: di fronte all’ambiguità del significato di media in vari casi, viene assemblata una web app per contribuire a una soluzione.

Punto di apprendimento: potrei passare una settimana intera solo su questa pagina, tanti sono gli spunti. Nei commenti ricorre perfino Douglas Hofstadter, l’autore di Gödel, Escher, Bach: un’Eterna Ghirlanda Brillante, e del tutto a ragione.

Per i cinici che vogliono solo mettere le mani su qualcosa, sono comunque due font dall’effetto interessante, pronti da usare.

Più un’ennesima dimostrazione che, se manteniamo un sito e sappiamo farlo solo in Verdana, stiamo perdendo qualcosa di importante. La vera liberazione, visto che siamo in tema, è la somma della conoscenza più gli strumenti per esprimersi in modo unico.

