---
title: "Aziende rompiscatole"
date: 2013-01-19
draft: false
toc: false
comments: true
categories: [ ]
tags: [script]
---
Negli Stati Uniti funziona un servizio di nome Gazelle che compra elettronica di consumo usata per rivenderla.

Siccome negli Stati Uniti le distanze non sono banali e per le aziende è imperativo offrire il servizio migliore, Gazelle spedisce ai venditori scatole di cartone in cui questi possono inserire i loro oggetti e mandare tutto a Gazelle.

Solo che gli oggetti sono ovviamente di dimensioni varie e così le scatole standard del servizio postale americano risultavano sempre troppo piccole o troppo grandi. Gli utenti del servizio, perché gli oggetti arrivassero a destinazione funzionanti nonostante la spedizione, li imballavano artigianalmente con carta di giornale, ritagli di stoffa, cuscini e persino pannolini, complicando notevolmente la logistica di Gazelle (che da qualche parte e in qualche modo doveva liberarsi in modo efficiente di tutto il materiale inutile).

Così l’azienda si è posta un problema di design (design, diceva Steve Jobs, è come funziona): creare una scatola adatta a oggetti di differenti dimensioni, che non necessitasse una farcitura da parte dell’utente e che fosse abbastanza semplice da essere prodotta a costi compatibili.

Ci hanno messo oltre cento versioni e ora sono convinti di averne una.

Più o meno quello che ha fatto Netflix per il proprio servizio di noleggio Dvd via posta. CnnMoney ha creato nel raccontare la storia una gallery fotografica che riassume l’evoluzione delle scatole.

Un’altra azienda che dedica molto tempo alle proprie scatole. Nel libro Insanely Simple: The Obsession That Drives Apple’s Success, per esempio, si parla di un momento in cui il team addetto al packaging ricevette un rifiuto da parte di Steve Jobs:

Non era il procedimento creativo che dava fastidio a Steve, ma il progetto in sé. Il capoprogetto aveva istruito il team a realizzare il packaging per due versioni dello stesso prodotto. A Steve non era piaciuto. Un prodotto, una scatola, aveva detto. Non era necessario considerare l’idea di un secondo involucro. E aveva ragione. Era più semplice, più rapido, migliore. La conversazione era terminata dopo pochi minuti lasciando un gruppo di persone intelligente e talentuoso a chiedersi perché non ci avevano pensato prima.

Ma naturalmente, se Apple dedica tempo alle proprie scatole al punto che la gente riempie YouTube di video sull’unboxing, si tratta dell’ossessione maniacale di un gruppo di fanatici religiosi.

