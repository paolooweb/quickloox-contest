---
title: "Guerrieri e canzoni"
date: 2012-07-02
draft: false
toc: false
comments: true
categories: [ ]
tags: [script]
---
Arrivato in fondo ad Angband e sconfitto Morgoth, ho provato a riprendere in mano il gioco, che ha rigiocabilità pressoché infinita grazie alle variazioni di classi e razze nonché alla ricchezza interna di elementi. Ma non ho più provato lo stesso interesse.

Se mai riprenderò in mano un altro roguelike, potrebbe essere Sil.

I programmatori hanno preso le mosse da Angband per rivoluzionare il sistema di gioco e sfrondare l’ambientazione, fino a rispettare il più possibile l’atmosfera tolkieniana. Dove la magia non è esibita e devastante, ma suggerita e sottotraccia. Nel mondo di Sil non si scagliano palle di fuoco e invece si cantano canzoni, proprio come gli eroi del Signore degli Anelli.

Il risultato finale è fresco e diverso dalle varianti classiche di Angband, più che abbastanza da motivare un reduce. Il sistema basato sugli skill (capacità) permette addirittura a un avventuriero decisamente esperto di tentare la discesa senza ingaggiare combattimenti con mostri e nemici, come alternativa alle carriere più bellicose tipiche del gioco di ruolo.

La complessità non viene meno – arrivare in fondo rimane una bella impresa – e però una spedizione è potenzialmente molto più veloce. Obiettivo non è necessariamente stroncare Morgoth, quanto rubare un Silmaril dalla sua corona.

Anche questo è molto tolkieniano e, nel mondo dei giochi odierno, consigliabile rispetto al rumore di fondo.

