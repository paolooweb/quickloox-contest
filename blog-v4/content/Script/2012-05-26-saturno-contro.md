---
title: "Saturno contro"
date: 2012-05-26 
draft: false
toc: false
comments: true
categories: [ ]
tags: [script]
---
Invece che buttare via banda per un post scritto in evidente stato di alterazione dai simpaticoni di Mela Marcia (che fatti alla mano non sta in piedi neanche con il girello), molto meglio godersi l’esegesi puntuale ed esaustiva di [Mario](https://multifinder.wordpress.com/2012/05/23/loroscopo/).

Che ha ancora il fegato di lanciarsi in letture tanto controcorrente – e non c’è niente di male – quanto contronatura. Un po’ lo invidio.

