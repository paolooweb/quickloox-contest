---
title: "Come fai, sbagli"
date: 2012-02-25
draft: false
toc: false
comments: true
categories: [ ]
tags: [script]
---
La prova provata che il mondo (parlo di quello informatico) sta cambiando drasticamente si trova in una domanda: quanto costerebbe Office per iPad?

Facciamo per un attimo finta che le voci sulla prossima uscita di Office per iPad siano vere.

Office desktop costa cifre che per il software di oggi sono iperboliche. E Office è uno dei pilastri del bilancio di Microsoft.

Se Office su iPad uscisse allo stesso prezzo, sarebbe completamente fuori mercato. Su App Store si può avere iWork a meno di 24 euro e altri programmi stile Office costano al più quindici/venti euro. Microsoft venderebbe pochissimo, mandando in fumo mesi di lavoro.

Quindi Office per iPad dovrà costare molto poco, rispetto alla versione desktop. Se però permette di sostituire abbastanza efficacemente l’edizione su computer, può darsi che un numero consistente di persone scelga un Office a basso costo su iPad piuttosto che uno ad alto costo sul computer. Se io dovessi scrivere un decimo delle cose che scrivo probabilmente rientrerei nella produzione media di testo da parte della persona media… e mi basterebbe un iPad con una tastiera esterna, forse neanche la tastiera.

Allora verrà da pensare che Office per iPad dovrà costare poco, ma anche fare poco, in modo da non cannibalizzare la versione da scrivania. Se è così, lo stesso accadrà sui tablet Windows 8, quando usciranno. Apparecchi dove Microsoft si è detta intenzionata a portare tutto il valore di Windows e non a farne parenti poveri. Ma sembra difficile che un utente con un tablet Windows 8 a tutta potenza, equipaggiato con una versione zoppa di Office, sarà contento.

Creare allora dunque una versione zoppa per iPad e una più competitiva per tablet Windows 8? Ma i soldi sulla vendita di software per tavolette, oggi, si fanno su iPad. E quanto costerebbe la versione competitiva? Se il prezzo fosse alto, questo potrebbe bastare ad affossare i tablet Windows 8, visto che il software Office-like su iPad costa poco ed è di buona qualità. Se fosse basso, ancora una volta cannibalizzerebbe Office da scrivania, risultando in una perdita di denaro per Microsoft.

Questa situazione è nota nella teoria dei giochi come dilemma del prigioniero. La cosa più conveniente per Microsoft è cooperare con Apple, in questo momento acerrima nemica, e sbarcare in modo competitivo – poste vere le voci – su iPad.

Vista da un altro angolo, questa situazione è tipica degli scacchi e si chiama zugzwang. Qualsiasi mossa faccia Microsoft con Office rispetto a iPad, ha da rimetterci. Perfino a non venderlo.

Il mondo (informatico) è cambiato drasticamente. Una volta Microsoft era la fermata obbligata. Lo slogan dell’azienda era Dove vuoi andare oggi?, a dire che non c’era nessun altro posto, o che comunque quello era il primo.

Oggi Microsoft è l’azienda che, comunque si muova, sembra fuori posto. E molte volte lo è.

