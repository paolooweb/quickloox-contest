---
title: "Il progetto nipote"
date: 2012-06-12
draft: false
toc: false
comments: true
categories: [ ]
tags: [script]
---
Succosa risposta su Quora alla questione di come faccia Apple a mantenere i propri segreti di produzione, con disvelamento di come sia nato Marklar, il progetto di OS X su processore Intel che infine portò alla svolta di qualche anno fa.

Riassunto veloce: un ingegnere Apple desidera poter lavorare dalla costa Est degli Stati Uniti, perché il figlio di un anno possa stare vicino ai nonni. Ma ha bisogno di lavorare su qualcosa da poter fare indipendentemente; allora rinuncia alla propria carica di quadro per tornare semplice ingegnere e propone di lavorare a Mac OS X su Intel. È il 2000.

Diciotto mesi più tardi, il suo capo gli chiede di vedere a che cosa sta lavorando, per giustificare il suo stipendio dentro il budget. Lui mostra Mac OS X pienamente funzionante su processori Intel (dicembre 2001).

Il capo rimane silente per un momento. Si assenta e in pochi minuti torna con Bertrand Serlet, allora responsabile assoluto dello sviluppo di Mac OS X. Serlet chiede quanto occorra per fare funzionare la cosa su un Vaio di Sony, l’ingegnere risponde due-tre ore e riceve mandato per acquistare il Vaio più costoso della linea. Per le sette e mezza della sera l’operazione è completata. Il giorno dopo Steve Jobs sale su un aereo per incontrare il presidente di Sony.

Dopo di che Serlet spiega all’ingegnere che nessuno dovrà sapere niente degli sviluppi del progetto, neanche la moglie, anche se lui lavora a casa.

Oggi sappiamo che per diciotto mesi il progetto Marklar è esistito solo perché un ingegnere autodestituito della propria carica aziendale voleva che il figlio piccolo stesse più vicino ai nonni.

Lo trovo straordinario.
