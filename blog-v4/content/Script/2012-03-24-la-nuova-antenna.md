---
title: "La nuova antenna"
date: 2012-03-24
draft: false
toc: false
comments: true
categories: [ ]
tags: [script]
---
Mancava uno scandalo da montare attorno al nuovo iPad ed è arrivato puntuale: raggiunge temperature operative superiori a quelle di iPad 2.

Su Internet è come gettare una bistecca nella vasca dei piranha e subito sono arrivati gli articoli sul surriscaldamento, pieni di punti interrogativi, immagini di falò sovraimposti alla foto di iPad, pare o si dice, lo ha provato mio nonno ma è un po’ sordo e dice che non sente calore eccetera.

I fatti: Tweakers.net ha riscontrato 33,6°C per il nuovo iPad e 28,3°C per quello vecchio. La differenza è di 5,3 gradi Celsius.

Dopo di che, il circo: i siti americani hanno riportato che il nuovo iPad era dieci gradi più caldo, perché loro misurano in Fahrenheit e non in Celsius; a qualche italianuzzo non è sembrato vero poter scrivere dieci gradi ignorando la questione della diversa scala.

The Verge, che ha fatto rapidamente marcia indietro, ha effettuato addirittura la divisione per titolare che il nuovo iPad era il 18,7 percento più caldo di iPad 2.

La scala Celsius (o Fahrenheit) è relativa, non assoluta, e applicare percentuali a una scala relativa è fuori luogo. Se lo si vuole fare, occorre usare una scala assoluta. La prova? In Fahrenheit, la stessa differenza è di 9,6 gradi, da 82,9 a 92,5. Pari a una differenza dell’11,6 percento, contro il 18,7 percento dello stesso rapporto in scala Celsius.

Come dire che il nuovo iPad scalda più di iPad 2, ma se lo misuri con un termometro europeo scalda molto di più.

È evidentemente una stupidaggine. Si possono applicare percentuali se si lavora con scale assolute. In Kelvin, appunto una scala assoluta, si va da 301,45 a 306,75.

C’è però un problema. Scrivere correttamente che il nuovo iPad è l’1,8 percento più caldo di quello vecchio non fa sensazione. Come commenta John Gruber su Daring Fireball, This is middle school science e non credo serva tradurre.

Ah, i bei tempi in cui iPhone 4 aveva un problema con l’antenna e allora si poteva prefigurare il ritiro (chiaramente mai avvenuto) di 1,8 milioni di unità vendute. Non è semplice fabbricare un caso ogni volta, in assenza di fatti veri.

