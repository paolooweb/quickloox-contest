---
title: "Politicamente corrotti"
date: 2012-06-19
draft: false
toc: false
comments: true
categories: [ ]
tags: [script]
---
Quelli che hanno la parola d’ordine innestata nel vocabolario sono noiosi. E così quelli che si riempiono la bocca di cause troppo grandi per loro, che non capiscono, di cui non colgono la complessità, di cui non hanno padronanza e che però azionano facilmente il riflesso pavloviano della parola d’ordine.

Prendiamo Wired e il riferimento a una curiosa serie di immagini del fotografo Henry Hargreaves, che si è immaginato iPod e iPad vari in frittura. Una operazione curiosa, divertente, non particolarmente profonda ma ben realizzata e degna di menzione.

L’articolo di Wired ritiene opportuno collegare l’evento e di conseguenza il cibo fritto, ohibó, al salvataggio del pianeta. Come per il cibo unto, cresce la preoccupazione riguardo alle conseguenze a lungo termine del consumo di elettronica. Lo scarso riciclaggio dei gadget presi e buttati, con i loro componenti tossici nelle discariche, è da paragonare al grasso che ottura le arterie. Sappiatelo, criminali con il colesterolo un po’ alto.

Le idee stupide hanno diritto di cittadinanza pari a quelle intelligenti, ma sui fatti c’è il dovere di verifica. E questi sono i fatti come li riferisce l’autore:

I circa 245 milioni di cellulari negli Stati Uniti hanno una vita media di diciotto mesi, con 900 milioni già a decomporsi. Ogni anno ne cessano di vivere 150 milioni e tale Seth Heine, amministratore delegato di Collective Good, stima che solo un quinto dei 150 milioni venga riciclato.

I paladini delle buone cause hanno una difficoltà riconosciuta a stimare: è recente il caso di Greenpeace, che attribuisce al centro dati Apple di Maiden in North Carolina un consumo massimo di 100 megawatt, quando invece sono venti. Un piccolo errore, forse l’arrotondamento.

Casualmente, Horace Dediu di Asymco, che manovra grandi fogli di calcolo al posto di parole d’ordine facili, si è messo a elaborare l’andamento degli apparecchi Apple dal 2001, anno di uscita di iPod.

Risultato: il tasso di abbandono degli apparecchi suddetti è in decelerazione. Gli apparecchi hanno vita più lunga di quanto non fossero anni fa. Prego, si facciano avanti i critici di Apple per la sua presunta mania di farci cambiare tutto una volta l’anno.

Secondo, Dediu totalizza circa 680 milioni di apparecchi venduti da Apple tra il 2001 e oggi. Dal 2001 a oggi sono passati undici anni e, calcola, il 60 percento di tutti questi apparecchi è ancora in uso.

Di più: la loro vita media appare costante, di due anni.

Questi dati cozzano fragorosamente con l’articolo di cui sopra. A meno di non dedurne che, se la vita media è di diciotto mesi ma gli apparecchi Apple vivono due anni, sia la vita media degli altri a essere più breve. Si facciano avanti per favore a questo punto i giacobini sempre alla caccia dei prodotti Apple che costano più degli altri ma sono uguali agli altri. Rifacciano, prego, i conti alla luce del fatto che gli altri costano meno ma durano meno.

Poi si presentino gentilmente gli alfieri con la parola d’ordine sempre sulla lingua e prendano atto che, se proprio uno non decide di annegarsi in una fossa biologica per salvare il mondo e preferisce invece, nemico della Dea Natura, acquistare un gadget (orrore! Parola degradante per oggetto da disprezzare!), allora forse fa meno danno se ha sopra il simbolo della mela.

A meno che la parola d’ordine non nasconda interessi materiali e che la lotta per la grande causa corrisponda alla posizione conveniente da cui guadagnare alle spalle (dove si trovano le natiche, insomma) di quelli che ci credono.

