---
title: "Scoosate, Ma"
date: 2011-09-13
draft: false
toc: false
comments: true
categories: [ ]
tags: [script]
---
Invece che perdere tempo con quei sitacci scritti da fotocopiatori semianalfabeti e mitomani brufolosi, perché non ci si fanno quattro sane risate giornaliere con Scoopertino, il sito che pubblica notizie gustose e dichiaratamente false?

Se l’inglese è un problema, lo si traduce con Google Traduttore. Garantisco che il risultato finale, per quanto approssimato, è italiano all’altezza di quelle altre schifezze.

La grafica è ottima, le idee sono buone: mentre scrivo, Scoopertino annuncia che Apple ha fatto causa ai falsi Apple Store cinesi e… ha perso, per cui deve chiudere i propri Apple Store.

Esilarante, con il racconto della disfida legale contro la cinese Appel e il suo amministratore delegato Steve Joobs.

Preso nel momento giusto con il giusto ragazzino disistruito al quadro comandi, un certo sito italiano sarebbe capace di pubblicarlo pari pari, con un punto interrogativo in fondo al titolo.

