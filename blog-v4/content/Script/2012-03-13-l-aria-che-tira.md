---
title: "L aria che tira"
date: 2012-03-13
draft: false
toc: false
comments: true
categories: [ ]
tags: [script]
---
Ci sono teste con un passato.

Le senti che argomentano: ma io vorrei collegare iPad a un videoproiettore e a una stampante e magari anche a un disco rigido… perché non mi mettono su iPad un connettore Thunderbolt o almeno un connettore Usb?

Ci sono teste con un presente. Ho tenuto varie presentazioni ultimamente, ma ne ho anche vista una. Il relatore aveva un iPad che tramite AirPlay inviava senza cavi il segnale a una Apple Tv, attaccata al videoproiettore.

C’è gente con un futuro. Apple immagina un futuro dove ci sono una Apple Tv all’uscita video, stampanti con AirPrint e magari una Time Capsule. In mezzo c’è un iPad che fa quello che gli pare, senza fili (metto i link perché uno possa verificare il necessario).

Questo futuro non si è ancora realizzato. Ma è preferibile pensare al futuro senza cavi oppure a quello uguale al passato, con i cavi, i cavetti, gli adattatori, i connettori?

Se una persona vede il futuro come uguale al passato, ha un problema nel presente.

Io preferisco l’idea di abolire i cavi. Se mi serve proprio il passato, un MacBook Air o Pro hanno tutti i connettori che servono. Anche l’idea che tutti gli apparecchi debbano avere la stessa filosofia, funzionare allo stesso modo, avere tutti la stessa dotazione, corrispondere tutti allo stesso costrutto mentale che si è cementato con gli anni nella nostra testa, è un’idea mefitica.

Aprire le teste, lasciare passare un po’ di aria fresca. A spazzare quella polvere che siamo abituati a trovare intorno ai cavi. Quando in casa c’è polvere, o è roba sporca o è roba vecchia. Che passi l’aria dove c’erano i cavi sa di novità e pulizia.

