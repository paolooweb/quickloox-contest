---
title: "Diffidare delle imitazioni"
date: 2012-05-21T12:09:40+02:00
draft: false
toc: false
comments: true
categories: [ ]
tags: ["script"]
---
Andre mi ha passato un pezzo semplicemente sensazionale sullo smontaggio del caricabatteria di iPhone.

Non meno di venti tra foto e schemi e una trattazione praticamente esaustiva: Internet al suo meglio, il che avviene ogni volta che un tema il più possibile circoscritto viene sviscerato al massimo.

Il circuito viene definito sorprendentemente complesso e innovativo e descritto in ogni sua parte.

Specialmente interessante sono le sezioni nelle quali il caricabatterie Apple viene messo a confronto con un analogo di Samsung e una imitazione “cinese” a basso costo.

Quest’ultima è molto meno sicura, realizzata senza rispetto per le norme di fabbricazione di questi apparecchi e con componenti di minore qualità. Il modello Samsung è inferiore a quello Apple e superiore al “cinese”. Tutti garantiscono presumibilmente profitti molto elevati ai rispettivi produttori.

Nel 2008 Apple richiamò i caricabatteria di iPhone 3G in Stati Uniti, Canada, Messico e America Latina, perché i contatti della spina erano male assicurati al caricabatteria e rischiavano di restare incastrati nella presa al momento di staccare l’oggetto.

Così l’autore ha provato a staccare i contatti, a forza se necessario.

I contatti “cinesi” sono venuti via facilmente. Quelli Samsung con difficoltà. Con quelli Apple è praticamente impossibile.

I computer non sono tutti uguali. Neanche i caricabatteria. Diffidare delle limitazioni, quelle di chi parla per approssimazione.
