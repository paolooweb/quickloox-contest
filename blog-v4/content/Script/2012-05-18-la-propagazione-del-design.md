---
title: "La propagazione del design"
date: 2012-05-18T12:00:30+02:00
draft: false
toc: false
comments: true
categories: [ ]
tags: ["script"]
---
La foto mi arriva grazie a Emeralit e la pubblico perché altrimenti non ci si crede.

A bordo della Celebrity Eclipse

Traduco parte del testo:

“Può sembrare strano”, afferma Emilio Perez, Design Manager di Celebrity Cruises, “ma mentre concettualizzavamo l’aspetto delle navi della classe Solstice, volevamo che possedessero la qualità del design di un iPod — il tipo di impatto che ti fa dire ‘Wow!’ quando lo tieni in mano. Per questo è solo sensatissimo che abbiamo una partner con Apple riguardante gran parte della tecnologia che i nostri ospiti trovano a bordo”. […]

Spazio dedicato ad Apple, la iLounge offre stazioni di lavoro Macintosh; un’area commerciale che vende iPod, Macbook e accessori; e un’aula nella quale personale con certificazione Apple conduce seminari per gli ospiti. […]

Tra la Celebrity Eclipse e le sue navi consorelle, centinaia di Mac si occupano di segnalazioni digitali, forniscono informazioni sull’ospitalità, amministrano la rete telefonica e costituiscono parte integrale di una rete digitale totalmente interattiva divertente e facile da navigare, che aiuta gli ospiti a godersi appieno le esperienze a disposizione a bordo e in porto.

A parte l’idea di navi da crociera basate su Mac in quanto tale, la parte da notare è l’idea del design Apple che si propaga fino a settori quanto mai diversi e lontani da quello tecnologico tradizionale. Un Mac non dà solo un feeling o fa sentire a proprio agio, ma la sua progettazione ispira valori e soluzioni, diventa un esempio e un punto di riferimento. No, i computer non sono tutti uguali.
