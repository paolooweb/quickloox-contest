---
title: "Intuizione funzionale"
date: 2013-04-17
draft: false
toc: false
comments: true
categories: [ ]
tags: [script]
---
Bel colpo di Horace Dediu di Asymco, che ha collegato in primo luogo la enorme crescita di Apple alla scelta di costruire un nuovo avveniristico campus a forma di ciambella capace di ospitare tredicimila persone.

Fino a qui ci voleva poco, ovvio.

Ha però compiuto anche il passo successivo. Molte altre aziende hanno un quartier generale con migliaia di dipendenti e hanno costruito campus convenzionali, con tanti edifici quanti servono per i vari reparti e divisioni. Perché costruire una ciambella invece di tanti parallelepipedi come hanno tutti gli altri?

Perché Apple non è organizzata in reparti e divisioni, ma è concepita secondo una organizzazione funzionale. Gli specialisti, siano di ingegneria, progetto, vendita, si spostano sugli obiettivi del momento.
Nelle aziende piccole è una cosa normale. In quelle grandi è un concetto organizzativo, nei fatti, sconosciuto.

Apple si riconfigura in continuazione, con persone che devono potersi spostare agevolmente tra vari progetti. Per questo ha senso una struttura architettonica unificata, per di più con una forma che consente di minimizzare gli spostamenti complessivi delle persone.

Pura logica, conclude Dediu. E si tiene fuori da considerazioni estetiche.

Io no. Da invidioso di chi sa veramente programmare ho sempre ammirato l’eleganza di alcuni linguaggi dove è possibile la programmazione funzionale.

Un’azienda organizzata secondo criteri funzionali si allestisce una sede elegante. Non per scelta, ma per conseguenza.

E per conseguenza si ha che un Mac non è elegante in quanto così ha deciso il design, ma perché partendo da un concetto evoluto non può che finire così.


