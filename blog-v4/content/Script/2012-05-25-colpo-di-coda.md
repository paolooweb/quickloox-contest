---
title: "Colpo di coda"
date: 2012-05-25
draft: false
toc: false
comments: true
categories: [ ]
tags: ["script"]
---
Ho sempre apprezzato molto Coda, il programma di Panic per lavorare con l’Html e i suoi addentellati.

Uscisse Coda 2, sarebbe già motivo di segnalazione; invece esce Coda 2, più la sua anteprima senza fili su iPad grazie a Diet Coda. Che di suo non è un sostituivo del programma principale, ma permette a un professionista del ramo di togliersi numerose soddisfazioni rispetto al proprio codice per il Web anche solo con un iPad a disposizione.

Temo che sarà il mantra dell’estate. Qualcuno lo chiama giocattolo, ma si vede che conosce poco il mondo del lavoro.
