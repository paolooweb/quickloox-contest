---
title: "Campato per arial"
date: 2013-01-26
draft: false
toc: false
comments: true
categories: [ ]
tags: [script]
---
Costa meno e fa le stesse cose.

Uno dei caratteri più usati dall’umanità è Helvetica. Cartelloni, marchi, titoli, testate, interfacce; Helvetica è usatissimo e ha mille varianti.

Per non pagare diritti, Microsoft si è appropriata anni fa di un clonaccio di Helvetica e lo ha chiamato Arial.

A causa della diffusione di Windows e di Office, Arial ha invaso la vita di tutti con tipografia a basso costo e di scarsa qualità. Che, appunto, fa la stessa cosa a minor prezzo.

Ironic Sans propone un test: venti marchi commerciali mostrati in versione originale (Helvetica) e clonata (Arial). Quante volte riusciamo a cogliere la differenza?

Siccome il test fornisce la risposta per ciascuna coppia di marchi, verso il quindicesimo confronto accade una cosa interessante: cominciamo a capire che cosa distingua l’originale dal clonaccio. Il quale sembra fare le stesse cose a minor prezzo, all’occhio distratto e incompetente.

Se uno indovina dieci volte su venti, non vuol dire che Helvetica e Arial valgono uguale; vuol dire che non è abituato a valutare a quel livello di dettaglio. Si parlasse di vino o di miscele di caffè a tavola, sarebbe giudicata una mancanza di gusto e di competenza. Invece, se si parla di elettronica di consumo, si passa per astuti.

