---
title: "Interessanti e aperti"
date: 2012-03-03
draft: false
toc: false
comments: true
categories: [ ]
tags: [script]
---
Interessanti e aperti

Keka, compressore e archiviatore un po’ qualunque di file. La cosa interessante non è che cosa fa, ma come si distribuisce. Il software è open source, scaricabile quindi gratuitamente da web, e allo stesso tempo disponibile a pagamento su App Store Mac. Il mantra è che App Store sia chiuso e nemico del software libero; in questo caso pare semplicemente una fonte sagace e più che legittima di finanziamento del lavoro di chi scrive il programma.

Trelby, software open source per scrivere sceneggiature. Ci sono anche Celtx e Final Draft, ma non sono software libero.

Dungeon Crawl Stone Soup, una di quelle cose di ruolo che piacciono a me con grafica ai minimi termini e discese in sotterranei senza fine. Open source e ok; giocabile anche via web, cosa meno comune.

