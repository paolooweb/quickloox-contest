---
title: "Dieci anni che ne valgono cento"
date: 2012-05-12
draft: false
toc: false
comments: true
categories: [ ]
tags: [script]
---
Salvo imprevisti, oggi passerò il pomeriggio a Savona presso la Fortezza del Priamar, corso Giuseppe Mazzini 1, dove All About Apple compie dieci anni e per l’occasione è allestita la mostra Apple Generation.

I ragazzi (di ogni età) di All About Apple meritano elogi superiori a quelli che io sono in grado di distribuire: hanno trasformato una passione in una vera impresa, l’hanno fatta crescere e maturare a livello di professionalità e il tutto sempre mantenendo una dimensione umana e amica che gli invidio e provoca la mia stima profonda e inesauribile.

Visto che per calendario siamo in argomento, sono anche una delle poche realtà cui ha davvero senso donare il cinque per mille. Ma c’è anche PayPal per esempio e pure la possibilità di donare hardware. Perché il loro museo è vivo, con gli schermi accesi e le tastiere funzionanti. Anche per questo è unico nel suo genere. È storia vera, non chiacchiere sul passato, dieci anni che ne valgono cento per accuratezza e competenza del lavoro.

Tutto quello che posso fare io è portare una copia del numero uno del nuovo Macworld (il numero due è in tutte – spero – le edicole da ieri…) e salutarli con affetto e amicizia. Loro e naturalmente chiunque vorrà essere della partita!

