---
title: "Il touch magico"
date: 2012-03-27
draft: false
toc: false
comments: true
categories: [ ]
tags: [script]
---
Una nuova dimensione della risposta alla domanda che cosa si può fare con una tavoletta che non si possa fare con un netbook, tanto in voga nei tempi passati.

È qualcosa più di una boutade. Tanto per cominciare, il personal computer esiste da quarant’anni quasi, ma solo oggi cominciano a proporsi figure come Marco Tempest, tecnoillusionista. Prima mancava qualcosa.

