---
title: "Le tenebre e la luce"
date: 2012-12-06
draft: false
toc: false
comments: true
categories: [ ]
tags: [script]
---
Ancora poco e le giornate torneranno ad allungarsi. Disse Steve Jobs a metà degli anni ottanta:

Se per qualche ragione commettiamo un grosso errore e Ibm vince, l’istinto mi dice che entreremo in una Era Oscura lunga almeno vent’anni.

Non fu Ibm a vincere. A parte questo, si guardi la serie di grafici in colore pieno di questo intervento di Horace Dediu e come la distribuzione dei sistemi, hardware e software, inizi a indicare una inversione di tendenza.

Forse gli stessi dati si vedono meglio nella diapositiva 24 di questa presentazione.

Di una curva simile abbiamo già parlato. Per coincidenza, riguardava il rapporto tra Mac e Pc venduti.

Abbiamo indizi che l’Era Oscura stia finalmente terminando.

