---
title: "Metodo e sostanza"
date: 2012-07-11
draft: false
toc: false
comments: true
categories: [ ]
tags: [script]
---
Didattica digitale: Claudio ha commentato un’osservazione che merita:

Dissento sull’inutilità ‘ontologica’ delle LIM che possono essere anche comode ed utilizzate efficacemente, SE gli insegnanti ricevono un minimo di formazione mirata e ci mettono un po’ di buona volontà o meglio ancora se le si lasciano usare dagli studenti

E dissento più che volentieri anch’io. In linea generale, osservo che il mondo oggi è fatto di rete e collegamenti orizzontali. Le piramidi scompaiono. Per cui mi viene da dire che l’insegnamento dovrebbe essere capace di evolversi secondo logiche di rete e collegamenti orizzontali, invece che secondo il modello della cattedra lassù e i ragazzi laggiù. L’insegnante ha autorità non perché lo ha detto il preside, ma perché si fa mentore dei ragazzi e scende idealmente in mezzo a loro senza rinunciare al proprio sapere e alle proprie prerogative, tutt’altro.

Il punto era un altro. Come ho commentato in risposta a Claudio, non è questione di discutere attorno a uno strumento o a un altro, per vedere se è meglio il primo o il secondo. Bisogna pensare a che cosa è meglio per i ragazzi e poi realizzarlo al meglio delle possibilità. Che questo significhi iPad, Lim, lavagne di ardesia o anfiteatri greci, non ci interessa. Non ci interessa lo strumento, ma il risultato.

Allora devo segnalare il lavoro di un insegnante inglese che non usa la Lim, ma il proprio iPad che comunica via Wi-Fi a una Apple Tv collegata a un videoproiettore.

Ho il sospetto che questa soluzione costi meno di una Lim e sia infinitamente più versatile.

Ma, dicevamo, non conta lo strumento, conta il risultato. Racconta Mr. Yeomans:

Apple Tv mi permette di mostrare qualsiasi cosa sia sul mio iPad da qualsiasi posizione nella classe. […] Posso bloccare l’immagine sullo schermo ma cambiare slide per mostrare una cosa a un ragazzo che chiede spiegazioni.

La cosa che mi ha colpito veramente è un’altra:

Ho usato iPad come lavagna interattiva di classe. I ragazzi possono stare al loro posto e mostrare il proprio lavoro. E possono collaborare insieme su uno schermo. Sappiamo tutti quanto amino avere il loro turno alla lavagna, ma immaginatevi il loro coinvolgimento quando una ragazza in fondo alla classe scrive qualcosa e tutti la vedono istantaneamente apparire sullo schermo. iPad è così leggero e compatto che i ragazzi possono passarselo durante la lezione.

Lo stesso autore scrive tranquillamente che non bisogna pensare a questo come una panacea per qualunque situazione.

Tuttavia è sempre da qui che partirei. Non dallo strumento, ma da che cosa si fa e che cosa si vuole ottenere. Voglio che i ragazzi ascoltino o che partecipino? E se partecipano, uno per volta alzandosi o con la possibilità di contribuire in tempo reale? Eccetera. Prima la sostanza, poi decidiamo il metodo.

