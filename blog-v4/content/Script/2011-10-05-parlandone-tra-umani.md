---
title: "Parlandone tra umani"
date: 2011-10-05
draft: false
toc: false
comments: true
categories: [ ]
tags: [script]
---
Sempre grazie a tutti e in primo luogo a Gand per la chiacchierata in occasione della presentazione di iPhone 4S e iOS 5.

Il tempo è passato piacevolmente, tra il fallimento sostanziale dei veri liveblog, chi più chi meno collassati tutti almeno una volta sotto il peso delle attenzioni, e perfino quello del sito Apple, che è rimasto sepolto per vari secondi dalla marea montante di visite.

Noi ci siamo divertiti (spero), chi soddisfatto di un aggiornamento solido che ripropone le dinamiche del passaggio da iPhone 3G a 3GS, chi un po’ deluso dalla mancanza di fuochi d’artificio.

Per parte mia, noto che il contenuto base di iOS 5 era noto da mesi. Due novità vere che mi hanno colpito: Siri e Cards.

Siri era ovvio che colpisse (posto qualcosina di più interessante nei prossimi giorni) ed è un peccato doverlo aspettare in italiano. Per ora possiamo solo chiacchierare tra umani.

Ma se Cards funziona, porterà l’umanità dall’epoca in cui si inviavano le cartoline prestampate a quella delle cartoline stampate apposta. Ho sentito molti scrollare le spalle e definirla stupidaggine. Forse lo è.

Certo, quando Apple si è posizionata per bocca di Steve Jobs all’incrocio tra la tecnologia e le arti liberali, si intendeva qualcosa di simile a Cards. Non certo il software, ma il complesso dei pensieri che portava, tempo fa, a spedire cartoline. Spedivo un sacco di cartoline e ogni messaggio doveva essere diverso da quello della cartolina precedente.

