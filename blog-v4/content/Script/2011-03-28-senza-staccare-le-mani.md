---
title: "Senza staccare le mani"
date: 2011-03-28
draft: false
toc: false
comments: true
categories: [ ]
tags: [script]
---
Nella mia ricerca continua di sistemi per usare il meno possibile il mouse e il più possibile la tastiera, prima di tutto sono stato piacevolmente disturbato da Magic Trackpad. Per le cose che faccio io è estremamente confortevole e non provoca tutti quei problemi ergonomici favoriti dall’uso intensivo del mouse.

A parte questo, tra i vari buchi nella copertura totale di Mac via tastiera mi mancava la modifica delle caselle dentro i fogli di Numbers.

Usando solo la tastiera, mi pareva, era possibile portarsi su una casella e scrivere, però sostituendo il contenuto originale. Diverso dall’apportare modifiche. Mi era necessario fare clic nel campo testo in alto, dove viene replicato il contenuto della casella selezionata.

Finalmente Mac OS X Hints mi ha spiegato che è sufficiente portarsi sulla casella e premere Opzione-Invio.

