---
title: "Appuntamento al 2016"
date: 2012-04-15
draft: false
toc: false
comments: true
categories: [ ]
tags: [script]
---
Qualcuno ricorderà la comparazione tra un iMac del 2000 e un iPhone del 2010. Dieci anni di distanza, ordini di prestazioni – approssimativamente – paragonabili.

Di recente il solito Horace Dediu di Asymco ha effettuato un altro confronto, tra il nuovo iPad e una coppia di Mac del 2008, MacBook Air e iMac.

Ancora una volta l’ordine di grandezza delle prestazioni è abbastanza paragonabile. Ma gli anni di distanza si sono ridotti a quattro.

Se l’ipotesi di Dediu si conferma nei fatti, ogni anno iOS riduce il divario prestazionale di due anni. Se così fosse, il pareggio avverrebbe nel 2016.

E quattro anni sono comunque lunghi, a dispetto dei buontemponi per i quali Apple vuole uccidere Mac.

