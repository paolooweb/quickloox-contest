---
title: "Riassunto sommario"
date: 2012-07-18
draft: false
toc: false
comments: true
categories: [ ]
tags: [script]
---
Da qualche anno sono coautore assieme a MisterAkko della Guida Completa a OS X edita da Apogeo, in uscita per ogni nuova versione principale del sistema.

Snow Leopard uscì il 28 agosto 2009. L’ultimo file del libro – tipicamente la prefazione o l’introduzione – fu consegnato il 22 luglio. Erano file Rtf, compilati con Pages, TextEdit, Pages e OpenOffice in varie articolazioni. Il libro di carta uscì una settimana o due dopo il software.

Lion uscì il 20 luglio 2011. L’ultimo file (il nono capitolo) fu consegnato il 14 luglio. Conservo una pletora di file Doc, Odt (Open Document, quello di LibreOffice) e Rtf. Da questi fu derivato un ebook, che uscì lo stesso giorno del software, mentre il libro di carta arrivò poche settimane dopo. Se non vado errato, potrebbe essere stata la prima volta in Italia che un testo tecnico è uscito prima in ebook che su carta.

Mountain Lion uscirà (mia previsione qualsiasi, basata su fondi del caffè e lanci di ossa di pollo) il 25 luglio 2012. L’ultima consegna all’editore data al 16 luglio e l’iter sarà quello dell’anno scorso, ebook in contemporanea con il software e cartaceo a seguire.

Solo che stavolta il libro è stato scritto quasi integralmente con BBEdit, in una forma di Html ibrida che permette all’editore una importazione abbastanza agevole dentro InDesign per arrivare all’impaginazione tradizionale.

Scommetto che l’anno prossimo uscirà (invento) Cougar e, se avremo il privilegio di scrivere il libro, lo faremo in una forma standard di Html oppure in Xml.

Tenere a mente questo trend. Chiunque produca contenuti, al giorno d’oggi, è chiamato a conoscere almeno parzialmente il lato tecnico della produzione. I tempi di ho scritto un libro in Word stanno volgendo al tramonto. Vale molto più la pena, in quel caso, sudare cinque minuti e andare su iBooks Author.

