---
title: "Libertà E Libertuccia"
date: 2012-07-08
draft: false
toc: false
comments: true
categories: [ ]
tags: [script]
---
Il bravo Paolo Attivissimo ha preso spunto dalla chiusura del servizio Minitel per spendere due parole su iPad, terminale “stupido” e lucchettato sui quali non siamo più liberi di installare quello che desideriamo.

Come possessore di iPad sul quale installo quello che desidero, dissento. Ma questi sono fatti personali. Probabilmente lui ha un fatto personale con iPad e non c’è da sindacare.

Più grave è che, nella piccola discussione che è nata sul suo blog, si sia prodotto in una affermazione come l’Apple Store [sic] non è compatibile con programmi rilasciati sotto licenza free software.

Ho fatto gli esempi di Battle for Wesnoth, Edhita, Colloquy, tutto software abbondantemente free, se non addirittura open source, perfettamente disponibile su App Store.

In altre parole, la sua affermazione è falsa, non documentata, o almeno sbadata, diciamo improvvida, facciamo pure distratta. In tutti i casi, non corrisponde a verità.

Posso aggiungere altri esempi. AntiMap Log è open source. Barcodes Scanner è open source. E così Bubbsy, CamLingual, Canabalt (dubbi? Ecco il codice sorgente), Climbers, ComicFlow, Count It Out, Diceshaker, Dollar Bets, Doom Classic, Impronta Ecologica, Freshbooks, Frotz, Pain Guide: Pain Management Quick Reference, Gorillas, HoxChess, iLabyrynth, iStrobe, iWvu, Little Go, Mobilesynth, Molecules, Mover, Mugician, news:yc, PlainNote, PwSafe Quick Bright, Rpi Shuttles, Sage Math, Sign+, SpaceBubble, SparkleShare, Stockfish Chess, Sudoku Resolv, Task Coach, Travel Advice -Uk, Tweedie, Tweejump, Tweetee, Tweetero, Vim (fior di hacker ci sono cresciuti dentro), wikiHow, Wikipedia Mobile, Wolfenstein 3D Classic Platinum, WordPress, XPilot (un classico dell’adolescenza di Internet), Your Rights, yTrack, Zbar.

Garantisco che non sono tutte. Ma neanche la metà di tutte, né un terzo di tutte.

Quello che vorrei capire è perché uno sbufalatore di professione, membro onoratissimo del comitato per il controllo delle affermazioni sul paranormale, con una genuina passione per la scienza e per l’astronautica, quando si arriva a iPad diventi falso, antiscientifico e faccia pure finta di niente pur di non riconoscere di avere detto una sciocchezza.

Se ci si appella alla libertà senza un angolino di amore per la verità delle cose, diventa libertuccia, quella delle scimmie libere di cogliere dall’albero tutte le banane che vogliono.

Io preferisco la vetrina “stupida” e lucchettata dell’ortolano, che le banane le ha scelte per me e mi fa un giusto prezzo.

