---
title: "Creatività E Curriculum"
date: 2012-05-16
draft: false
toc: false
comments: true
categories: [ ]
tags: [script]
---
Ho scoperto chartsnthings, un blog di schizzi dal reparto grafico del New York Times. L’inglese non è un problema, cade la mascella solo a guardare le figure.

Potrei restare a guardarlo per ore e lo consiglio a qualsiasi grafico, professionista e – soprattutto – aspirante. Forse ho una idea irreale del mercato, ma credo che se uno si presentasse a un colloquio di lavoro con una pagina del genere, invece che con un curriculum europeo pieno di fuffa burocratica, avrebbe più possibilità di trovare un lavoro e più velocemente.

Non parlo – volutamente – dei programmi citati qua e là, indispensabili per realizzare certe elaborazioni e più che mai indicati quasi a chiunque abbia responsabilità lavorative che comportano la produzione, l’analisi e l’elaborazione di dati. Cose che a scuola nessuno vede fino all’università e invece dovrebbero comparire nell’età dell’obbligo, ben prima della fine.

