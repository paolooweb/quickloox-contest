---
title: "L'Occhio Del Padrone Ingrassa L'Istogramma"
date: 2013-02-16
draft: false
toc: false
comments: true
categories: [ ]
tags: [script]
---
Una indagine di OnDevice condotta in Francia, Germania, Giappone, Indonesia, Regno Unito e Stati Uniti metterebbe iPhone 5 al quinto posto nella classifica della soddisfazione da parte dell’acquirente. iPhone 5, non iPhone in generale (che risulta primo).

Interessante il grafico che mostra la classifica:

Barre e numeri hanno valori diversi.

A parte l’idea di mostrare iPhone 5 ultimo di cinque invece di, che so, quinto di dieci, che è scelta legittima, si nota qualcosa di strano nell’altezza delle barre. Proviamo a immaginare che i numeri siano assenti: la barra da 8,23 è meno della metà della barra da 8,57. Invece i numeri differiscono per la venticinquesima parte. Ho provato a creare un grafico uguale con Numbers, ma contando da zero a otto invece che da 8,0 a 8,57:

Barre e numeri hanno lo stesso valore.

Le classifiche di soddisfazione dipendono fortemente da come, dove e quando sono stati raccolti i dati. A parte questo, OnDevice ha pubblicato i risultati come presentazione SlideShare:

US & UK Device Satisfaction from On Device Research
Alla dispositiva 14 si vede l’elenco dei clienti, tra i quali Motorola.

I primi due telefoni in classifica sono Motorola.

Viene da sospettare che, più di dove come e quando, questa classifica dipenda da chi l’ha pagata.

