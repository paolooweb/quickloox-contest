---
title: "Non è da tutti"
date: 2011-11-24
draft: false
toc: false
comments: true
categories: [ ]
tags: [script]
---
Non è da tutti abbonarsi all’edizione aggiornata del corso di programmazione per iPhone e iPad dell’università di Stanford.

Non è da tutti mettere a disposizione un corso universitario su Internet totalmente gratis.

Non è da tutti concepire una piattaforma come iTunes U, che rende possibili queste cose.

Non è da tutti capire che tempi eccezionali viviamo, a dispetto della cronaca.

