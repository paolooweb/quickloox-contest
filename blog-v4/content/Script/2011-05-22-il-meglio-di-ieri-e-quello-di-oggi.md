---
title: "Il meglio di ieri e quello di oggi"
date: 2011-05-22
draft: false
toc: false
comments: true
categories: [ ]
tags: [script]
---
Definire ambizioso il lavoro di Steven Wittens è dire poco. Sviluppatore, ingenegnere e impallinato (geek) come si autodefinisce, ha lanciato il progetto TermKit, che parte da una constatazione: Unix ha retto alla prova del tempo e il suo modello di piccoli programmi che intragiscono insieme funziona alla grande. L’unica cosa è che i mezzi per approfittarne sono ancora quelli degli anni sessanta, quando abbiamo a disposizione grafica straordinaria, formati di file per ogni necessità, standard e risorse di elaborazione che quarant’anni fa non si trovavano neanche nella fantascienza.

TermKit è un ripensamento integrale del Terminale, a partire dal fatto che un programma Unix dovrebbe essere in grado di mostrare a schermo come minimo quanto è in grado di mostrare un browser e quindi è ora di finirla con le righe di solo testo, magari monocromatico.
Questo è solo l’inizio: i vagamente interessanti dovrebbero assolutamente dare un’occhiata al suo articolo in argomento. Per quanti non si spaventano di fronte alla cosa vera, è anche disponibile una versione provvisoria di TermKit.

L’impresa è di quelle che fanno tremare i polsi per la vastità. E non mancherà qualche nostalgico che proverà ad accusare Wittens di lesa maestà o vilipendio alla bandiera, dato che c’è sempre qualcuno pronto a ricordare come si stava meglio quando si stava peggio (e lui, guarda la coincidenza, era giovane).

Eppure guardando in una stupida finestra di Terminale è anche possibile vedere il futuro. Finora non ci aveva provato nessuno e le conseguenze di un successo potrebbero diventare clamorose. Da tenere d’occhio.

