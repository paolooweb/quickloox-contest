---
title: "La libert non è star sopra un albero"
date: 2011-05-28
draft: false
toc: false
comments: true
categories: [ ]
tags: [script]
---
Ho scoperto Newseum. Niente di eclatante, ogni giorno a portata di mano la prima pagina di 760 quotidiani da 89 Paesi di tutto il mondo.

Ci sono due quotidiani dell’Alaska, quattro gatti in mezzo ai ghiacci; quattordici dell’Indiana, che ha una popolazione inferiore a sette milioni di persone, meno che in Lombardia; venti della Germania, quattro dell’Arabia Saudita, otto della Svezia.

Uno dell’Italia.

Che è anche in buona compagnia. La Grecia ne ha uno solo, la Serbia uno, le isole Samoa uno, la Slovacchia uno, la Francia ne ha due, la Svizzera due, la Russia due e comunque non lo ha ordinato il dottore di passare la prima pagina a Newseum.

Però penso a tante polemiche sulla libertà di stampa, la censura, il controllo, la crisi economica che riduce i lettori, la concorrenza di Internet e chi più ne ha. Nella mia crassa ignoranza di politiche editoriali, se mi ritrovassi a gestire un quotidiano manderei la prima pagina a chiunque, dovunque, senza pensarci neanche tanto così. Credo che stare aggrappati a un albero senza neanche tentare di guardare che cosa succede là sotto, alla lunga stanchi e faccia perdere la presa. Sul mercato, intendo.

