---
title: "(For)EverQuest"
date: 2012-02-16
draft: false
toc: false
comments: true
categories: [ ]
tags: [script]
---
In passato si è parlato dei miopi che hanno realizzato infrastrutture monopiattaforma a lungo termine senza vedere che il futuro sarebbe stato multipiattaforma, anzi, in certi casi persino agnostico.

Le infrastrutture ludiche a lungo termine sono particolarmente critiche e costose, dunque prendere decisioni miopi all’inizio provoca scelte ancora più difficili alla fine.

In mezzo si situa EverQuest, il mondo fantasy di massa di Sony. Anni fa Sony è stata abbastanza lungimirante da organizzare una edizione di EverQuest per Mac e miope al punto di metterla su server diversi da quelli dell’edizione Windows (una Blizzard, con World of Warcraft, si è ben guardata dal prendere decisioni del genere e anche per questo domina il settore).

Tanti anni dopo siamo a oggi. Sony è stata così miope da annunciare la prossima chiusura dei server Mac e poi sufficientemente lungimirante da annunciare che resteranno in funzione. Non solo: si potrà giocare senza abbonamento, rispecchiando la prossima trasformazione del modello di affari del gioco.

La decisione non ha a tanto che fare con l’amore per Mac quanto con le relazioni verso il pubblico e il mercato. E però sono convinto che, al prossimo giro, Sony si organizzerà in modo più intelligente che avere il server dei buoni e quello dei cattivi.

