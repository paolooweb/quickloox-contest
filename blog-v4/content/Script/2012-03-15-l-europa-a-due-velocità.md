---
title: "L europa a due velocità"
date: 2012-03-15
draft: false
toc: false
comments: true
categories: [ ]
tags: [script]
---
Il nuovo iPad arriva domani in Francia, Germania, Regno Unito e Svizzera.

Poi arriva il 23 marzo in Austria, Bulgaria, Danimarca, Finlandia, Grecia, Irlanda, Islanda, Italia, Liechtenstein, Lussemburgo, Norvegia, Olanda, Polonia, Portogallo, Repubblica Ceca, Romania, Slovacchia, Slovenia, Spagna, Svezia e Ungheria.

Sembra giusto un annuncio di Angela Merkel e Nicholas Sarkozy.

