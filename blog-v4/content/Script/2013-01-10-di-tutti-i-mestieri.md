---
title: "Di tutti i mestieri"
date: 2013-01-10
draft: false
toc: false
comments: true
categories: [ ]
tags: [script]
---
Trovo straordinario il resoconto di India, Ink. su come hanno semiautomatizzato il lavoro di pulizia dei libri.

La cosa straordinaria è che si parla di ebook e la pulizia è quella dentro i libri: pulire e abbellire i libri elettronici prodotti da un “tritacarne”, cioè da una conversione automatica.

Un sacco di cose da imparare. Che i libri sono oggetti preziosi e curati, devono esserlo, anche in forma elettronica. Che la qualità non sceglie mai strade comode.

Che sia stato scelto BBEdit è ulteriormente illuminante.

