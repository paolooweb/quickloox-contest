---
title: "Dieci anni di schiavitù"
date: 2012-06-17
draft: false
toc: false
comments: true
categories: [ ]
tags: [script]
---
Keith Woolcock, un analista azionario di Londra per Nomura International, si è rivolto in aprile a un gruppo di manager Nokia presso il loro quartier generale. Ha mostrato una diapositiva contenente una sola parola: SCHIAVI.

Woolcock ha chiesto al suo pubblico di “immaginare di essere su una di quelle galee romane, con un tizio che fa schioccare la frusta. Siete schiavi di Microsoft. Abituatevi”.

I trenta o più dirigenti lo hanno ascoltato in silenzio e uno ha lasciato la sala.

Così il Wall Street Journal racconta di come, nel 2002, Nokia si preparava a fronteggiare la minaccia di Microsoft. Bill Gates aveva proposto a Nokia di sviluppare insieme software per cellulari ma l’azienda finlandese aveva rifiutato, temendo che Microsoft avrebbe finito per succhiare i profitti a disposizione dell’industria.

Sono passati dieci anni.

Nokia annuncia il licenziamento di diecimila persone. Il valore di mercato dell’azienda è inferiore del 92 percento a quello che aveva nel 2007, poco sopra il prezzo pagato l’anno scorso da Microsoft per acquistare Skype. Lo scorso trimestre è andata in rosso di 1,2 miliardi di dollari, il prossimo trimestre sarà più o meno la stessa cosa.

A guidare Nokia è un uomo Microsoft e chissà che, del tutto casualmente, non si crei una situazione che favorisca l’acquisto della società finlandese a un prezzo scontatissimo, impensabile anche solo pochi mesi fa.

Schiavi.

