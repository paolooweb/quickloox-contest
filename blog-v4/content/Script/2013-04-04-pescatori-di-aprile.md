---
title: "Pescatori di aprile"
date: 2013-04-04
draft: false
toc: false
comments: true
categories: [ ]
tags: [script]
---
Grazie a tutti per la pazienza di avere sopportato lo scherzetto da primo di aprile. Per i curiosi, questi sono i dati registrati da Google (che fornisce anche una visione grafica, purtroppo condivisibile solo come immagine).


Il popolo a favore di iBooks Author, nominata una commissione di saggi.
Tra gli altri innumerevoli pesci in rete, mi hanno divertito molto Google Nose e naturalmente la burla di YouTube: chiudiamo e scegliamo il vincitore.

Cnet ha raccolto una buona quantità di pesci generici e dedicati ai gadget.

Dichiaro vincitore per eleganza e semplicità Slashdot, con l’annuncio di fornire un servizio in più agli utenti registrati scrivendo per tutti gli altri le notizie in Rot-13 (e una raffica seguente di pesci, tutti rigorosamente in Rot-13).

Un pensiero anche a quelli che pensano sia il primo di aprile tutto l’anno e scrivono cose indicibilmente imbecilli, raccolti in una top ten da brivido da The Macalope.

Tra tutti, Heidi Moore del Guardian che ancora riesce a parlare delle cose Apple come di una religione (notizia: nel trimestre di Natale hanno comprato un iPhone quarantotto milioni di persone e riesce difficile immaginarli tutti fanatici); Eugene Kaspersky che grida allo scandalo in quanto Apple non allenta la sicurezza di iOS allo scopo di permettergli la vendita dei suoi programmi per la sicurezza; Dan Lyons, che dopo anni passati a bloggare come finto Steve Jobs si è messo a fare l’anti-Apple per partito preso dovunque vada e dura sempre qualche mese, dopo di che sono talmente grosse che lo mandano via. Dispiace per la persona, non per le falsità che non leggeremo.

Il primo di aprile dovrebbe durare un giorno solo.

