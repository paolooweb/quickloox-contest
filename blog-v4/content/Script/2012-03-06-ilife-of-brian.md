---
title: "Ilife of brian"
date: 2012-03-06
draft: false
toc: false
comments: true
categories: [ ]
tags: [script]
---
Tempo fa avevo proposto a briand06, alias Sabino, di ripubblicare i suoi commenti come post. Condivido solo parzialmente il suo punto di vista sulla compatibilità all’indietro del software e sugli sviluppi di OS X. In certi casi non li condivido per niente.

Ciò non toglie che siano cose meritevoli di essere lette e magari rilette. Ovviamente sono a disposizione per correggere ogni mio errore di montaggio. Lo scopo è mostrare il suo pensiero, non piegarlo in direzione del mio.

Nel frattempo si è aggiunta la variabile imprevista dell’annuncio di Mountain Lion, che aggiunge ulteriore importanza e attualità alle sue considerazioni.

È un po’ lungo, sì. Io lo leggerei tutto. L’ho fatto, del resto.

Sono decisamente impaziente di poter provare con calma iBooks Author su qualche pubblicazione o presentazione già pronta, per vedere “cosa succede”. Magari diventerà uno degli standard futuri, come oggi Pdf, ma non anticipiamo i tempi.

Sono persino d’accordo quando dici che la compatibilità con Snow Leopard, su questa scala, diventa un fattore decisamente (e purtroppo) relativo nelle valutazioni nel senso che, è vero, la novità è molto grossa, troppo grossa rispetto a queste minuzie.

Allo stesso tempo continuo a pensare che la incompatibilità ufficiale con Snow Leopard sia un errore da parte di Apple, e anche un cattivo segno.

Non è basata su motivazioni funzionali. iBooks Author non usa evidentemente alcuna funzione specifica di Lion (o se lo fa, sono ben nascoste). Che senso ha quindi renderlo artificialmente incompatibile? Che senso ha non permettere allora di installarlo su un sistema operativo ancora ampiamente diffuso e recente, magari con l’avvertenza chiara, che non è ufficialmente supportato da Apple?
In un altro post sostieni che possono esserci motivazioni di marketing o di supporto. La cosa è plausibile, ma non mi convince completamente. Applicazioni Apple ben più importanti e diffuse – penso a iMovie, iPhoto, Pages, Keynote, Numbers – sono disponibili su App Store e supportano tranquillamente anche Snow Leopard. L’unica differenza è che sono tutte a pagamento, ma francamente non credo che Apple si arricchisca con Keynote o che non abbia la forza di supportare una applicazione che oggi è, e probabilmente rimarrà a lungo, oggetto di nicchia e non certo una cosa destinata a un uso diffuso. E il fatto che sia distribuita gratis, come Xcode (altra applicazione non destinata al grande pubblico), fa capire tante cose sulla strategia a lungo termine di Apple.
È invece evidente che Apple vuol farci passare tutti, rapidamente, a Lion. Ok, il costo di transizione è ridicolo come è usanza Apple, Lion gira bene anche su macchine non recentissime, ci sono evidenti desideri di far convergere iOS e OS X.
Tutto bene, ma… e a chi Lion non piace? Chi vuole il Finder, chi vuole continuare a sapere dove diavolo sono i suoi file, chi vuole il Terminale e gcc, chi vuole poter entrare nei meandri della sua macchina?

La convergenza mi sta bene, molto meno dover esssere obbligato ad usare la filosofia di iOS anche sul desktop.

In un telefono non ho nulla da dire, date le oggettive limitazioni fisiche e funzionali dell’oggetto. Su un iPad mi sta meno bene, ma insomma, iPad ha una sua filosofia di utilizzo chiara e ben diversa dal classico notebook, e quindi posso ancora accettarla.

Ma sul desktop? Semplificare per l’utente normale ok, ma per favore, non chiudete tutti i muri, i cancelli, le finestre.

Tanto più che ci sarà sempre qualcuno in grado di aprirle.

Tanto più che proprio la chiusura è stata la causa principale per cui il Mac originale non si è mai diffuso più di tanto, sconfitto dal Dos e dai PC, pur partendo dal successo enorme di una macchina aperta, apertissima come l’Apple II.

4. Connesso a questo c’è App Store. Comodo, comodissimo, un modello di distribuzione vincente, ma non vorrei che diventasse rapidamente l’unico modo di distribuire il software, con troppi limiti e troppe chiusure. Ne abbiamo già discusso un anno fa, non mi dilungo.

5. Il web ha un ruolo importante per orientare e modificare tante strategie, oggi. Penso alle proteste per il fatto che Xcode fosse a pagamento (anche se decisamente economico!) che hanno fatto tornare Apple sui suoi passi (o penso, più in grande, alle proteste contro il Sopa, che hanno fatto cambiare idea ai parlamentari americani).

Queste cose, al confronto, sono minuzie, vero, ma chissà che parlarne, qui e altrove, non serva a qualcosa.

Mi accorgo di essere andato ben oltre il discorso iniziale su iBooks Author e mi scuso per aver annoiato tanti.

La tua provocazione ha funzionato.

P.S.: Comunque posso confermare che iBooks Author si può tranquillamente installare su Snow Leopard e funziona molto bene. Sembra Keynote. Da approfondire, decisamente.

