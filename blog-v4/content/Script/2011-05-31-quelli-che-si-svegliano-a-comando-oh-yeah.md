---
title: "Quelli che si svegliano a comando, oh yeah"
date: 2011-05-31
draft: false
toc: false
comments: true
categories: [ ]
tags: [script]
---
Sembra che il mondo Mac si sia svegliato ieri mattina con l’incubo di MacDefender. In effetti sparare il titolo a sensazione fa assai gioco ai siti che vivono di pubblicità gabbando i lettori. E a molta gente non è parso vero di poter dire tu quoque agli amici e colleghi con un Mac, dopo anni di masticazione amara.

A queste lingue molto abili nello scattare a comando e a pappagallo sarà utile precisare che il sistema detto File Quarantine (quarantena dei file) è in uso su Mac OS X dall’edizione 10.4, nota come Tiger.

La nota tecnica che fornisce le informazioni base sul meccanismo relativamente a Leopard e Snow Leopard data al 15 dicembre 2009, che non è esattamente settimana scorsa.

Frugando nel file /Sistema/Libreria/CoreServices/CoreTypes.bundle con un clic destro (o altro modo di richiamare il menu contestuale) e il comando Mostra contenuti pacchetto, si arriva al file /Contents/Resources/XProtect.plist, contenente le definizioni di alcuni malware noti contro i quali Mac OS X prende provvedimenti non appena ne coglie la presenza.

E comunque le minacce che mi chiedono l’autorizzazione a essere installate in qualità di antivirus mi rendono perplesso, come uno che invece di tentare di sfilarmi il portafoglio con destrezza mi si parasse davanti a chiedere scusi, non è che mi darebbe i suoi soldi? Sa, io lavoro per la sicurezza dei cittadini. Mario si è espresso fin troppo bene in merito.

