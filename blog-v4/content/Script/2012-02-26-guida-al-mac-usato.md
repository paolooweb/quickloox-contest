---
title: "Guida al mac usato"
date: 2012-02-26
draft: false
toc: false
comments: true
categories: [ ]
tags: [script]
---
Spesso ricevo richieste di informazioni su come trovare un Mac usato, ovviamente potentissimo e come nuovo, a prezzo zero.

La pretesa iniziale è spesso difficile da accontentare. Cercando, però, si trova. Questo l’ho scritto recentemente per un amico e probabilmente potrebbe diventare una sorta di guida veloce per la prossima volta che mi chiedono.

Ovviamente, se ci sono suggerimenti, li incorporo.

In generale si può cercare, oltre che sui classici siti tipo Secondamano e eBay, su MacExchange e MacUsato.

Mac2Sell mostra la quotazione indicativa di un Mac.

Chi ha tempo e un rivenditore autorizzato Apple a portata di passeggio (non un Apple Store), può chiedere. Usualmente i rivenditori accettano i vecchi Mac di chi viene a comprarsene uno nuovo per rivenderli. Ovviamente la disponibilità non è garantita, dipende da chi è venuto, che cosa ha lasciato eccetera.

Chi si alza presto la mattina, dia assolutamente un’occhiata alla pagina dei Mac ricondizionati sul sito Apple. Si trovano anche ottime offerte, che però appunto si esauriscono in fretta.

Aggiornamento: kOoLiNUS segnala una Faq (domande e risposte) in tema. Grazie!

