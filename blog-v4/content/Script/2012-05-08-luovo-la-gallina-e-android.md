---
title: "L'uovo, la Gallina e Android"
date: 2012-05-08
draft: false
toc: false
comments: true
categories: [ ]
tags: [script]
---
I fautori di Android tengono molto a sottolineare che il sistema è nato prima che Apple presentasse iPhone e quello che sarebbe diventato iOS. E così l’accusa principale rivolta da Apple a Google, avere scopiazzato da iPhone profittando indebitamente della presenza nel consiglio di amministrazione di Apple di Eric Schmidt, amministratore delegato di Google, sarebbe priva di fondamento.

Si è aggiunta un’altra dimostrazione che forse Google dovrebbe ripensarci: una presentazione articolata e riccamente illustrata di Google Sooner, l’apparecchio su cui Google ha sviluppato la primissima versione di Android.

Consiglio anche di guardare il video introduttivo di Google, che riporto qui sotto per comodità. E vedere dove sono i gesti multitouch, le app da lanciare toccandole, l’assenza di pulsanti fisici, il browser simile all’edizione desktop, oppure come erano pensati prima che qualcuno annunciasse un “telefono” davvero diverso dagli altri.

