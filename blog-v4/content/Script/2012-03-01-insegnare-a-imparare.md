---
title: "Insegnare a imparare"
date: 2012-03-01
draft: false
toc: false
comments: true
categories: [ ]
tags: [script]
---
Perché l’unica cosa / che la scuola dovrebbe fare / è insegnare a imparare. — Eugenio Finardi

Sabato 10 marzo interverrò nell’ambito del primo convegno Erickson su La scuola nell’era digitale in quel di Trento.

Siccome le cose interessanti non vengono mai da sole, mi viene chiesto se non riesco a dare una mano per un progetto artistico in cui, a farla brevissima, si tratta di fare coincidere in altezza e in frequenza una serie di tracciati di funzoni periodiche per poi compiere manualmente una serie di elaborazioni grafiche creative.

Vengono a chiederlo a me perché sono quello dei computer e non sanno che quando facevo il liceo c’era lezione solo la metà delle ore previste: il resto se ne andava in occupazioni, picchetti, assemblee, collettivi, contestazioni, dibbbattiti (tre b), scioperi, soviet, attivi, mobilitazioni e caccia all’Altro, inteso come uno che la pensa diversamente e siccome c’è la democrazia bisogna come minimo intimidirlo, se tira aria buona. In altre parole, io di trigonometria al massimo ne ho sentito parlare al telegiornale.

Però ho un Mac. E ho Grapher.

Trovo dentro Grapher un esempio dedicato esattamente alle funzioni periodiche. Le equazioni interattive permettono di sperimentare. In pochi minuti riesco a capire come alzare, stringere, spostare, capovolgere un coseno. Nessun genio, giusto tentativi.

Dopo il coseno passano un seno e una cosa sconvolgente che inizia da una parabola tagliata a fette per diventare una specie di pallina rimbalzante. Ci metterei sei mesi per scriverla io, quell’equazione. Ma è già lì e sempre per tentativi arrivo a modellarla esattamente come serve. I comandi di rappresentazione grafica mi permettono alfine di consegnare file PostScript perfettamente atti allo scopo. Ancora una volta ho guadagnato punti e io non c’entro niente, ha fatto tutto Mac.

Avevo un groppo alla gola perché amo imparare e ho imparato più in un’ora di lambiccamenti con Grapher che in un quinquiennio di scuola superiore. Ora mi è ancora più chiaro che chiunque abbia di che ridire sulla necessità di informatizzare la scuola, e pesantemente, non sa che cosa dice. O non vuole che gli altri capiscono i suoi veri, inconfessabili scopi di difesa di qualche piccola posizione di potere e di ignoranza.

Ho anche capito che i computer sono macchine straordinariamente inefficienti per l’uso in classe. Ma la tecnologia ha portato gli iPad, che invece sembrano nati per lo scopo e sui quali girano decine di app come Free Graphic Symbolic Calculator, gratis o a prezzi che costano la rinuncia a un cappuccio con brioche.

Certo avrei dovuto essere più intelligente, da adolescente. Tuttavia, se la scuola fosse stata una cosa seria, in quel liceo si sarebbe fatta lezione tutti i giorni e forse ci sarebbe stato un professore capace di insegnarmi a disegnare un orologio in movimento usando funzioni matematiche.

Avrei imparato a imparare. Mi chiedo a quanti sia stato fatto molto peggio che a me. Tra due sabati a Trento faccio arrabbiare qualcuno, ma spiego come la penso. Soprattutto mostro Grapher, gratis dentro la cartella Utility e nessuno che lo sa.

