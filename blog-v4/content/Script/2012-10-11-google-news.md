---
title: "Google news"
date: 2012-10-11
draft: false
toc: false
comments: true
categories: [ ]
tags: [script]
---
Per tenere in prospettiva il lavoro di Apple con i suoi successi e insuccessi, quasi un anno fa Google affermava che entro l’estate del 2012 la maggioranza dei televisori nei negozi avrebbe avuto Google TV preinstallata.

Magari con telecomandi ispiratori e facili da usare come questo.

Notizia di attualità, Motorola Mobility (ovvero Google) ha confermato l’abbandono del progetto Webtop, che trasformava lo smartphone in personal computer portatile previo collegamento a un Lapdock. C’è mancanza di interesse tra i consumatori.

Altra notizia: sempre Motorola Mobility, che aveva sporto reclamo contro Apple in relazione a sette brevetti di telefonia, lo ha ritirato. Potrebbe anche doversi all’intenzione di farli valere in altra sede, o a un improbabile atto unilaterale di buona volontà. Di fatto è uno spreco di avvocati e burocrazia.

A detta di chiunque, Google ha acquisito Motorola Mobility per le sue soluzioni hardware e per il suo portafoglio brevetti. Le une e l’altro non sembrano brillare per efficacia e l’acquisizione è costata a Google 12,5 miliardi di dollari.

Giusto perché poi si parla dell’alone viola nell’obiettivo di iPhone 5 come di un problema.

