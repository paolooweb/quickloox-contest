---
title: "Text editing alla quarta"
date: 2012-04-12
draft: false
toc: false
comments: true
categories: [ ]
tags: [script]
---
Come è noto sono pagato da Bare Bones oltre che da Apple e sono quindi tenuto a caldeggiare presso ognuno l’acquisto di TextWrangler 4.0.

Il metodo migliore per capire quanto valga il programma e chi gli sta dietro è, come sempre nel caso di Bare Bones, leggere le note di rilascio.

Per quanti avessero fretta, TextWrangler 4 raggiunge il livello di sviluppo di BBEdit 10, del quale è a tutti gli effetti un sottoinsieme.

Se TextEdit fosse per il word processing quello che TextWrangler è per il text editing, quasi non ci sarebbe bisogno di altro.

Ed è gratis.

Post scriptum: oggi ricorre il ventesimo anniversario di BBEdit. Bel pezzo di Jason Snell con una schermata dei bei tempi.

