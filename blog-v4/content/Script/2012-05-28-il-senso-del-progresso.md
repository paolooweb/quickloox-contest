---
title: "Il senso del progresso"
date: 2012-05-28T13:07:19+02:00
draft: false
toc: false
comments: true
categories: [ ]
tags: [script]
---

Lo spiega iScore Baseball (iPhone/iPod touch, iPad, 7,99 euro). E sembra fatto apposta, dato che il baseball in Italia è sport per pochi illuminati e altrimenti misconosciuto.

In America invece è sport nazionale e non perché ci sia un forte campionato di vertice, ma perché in qualsiasi momento dato si stanno giocando migliaia di partite, dovunque e a qualsiasi livello, proprio come il calcio qui da noi.

Là è possibile che un genitore, alla partita di softball della figlia, registri le statistiche di squadra e magari consenta all’altro genitore, a casa, di vedere in tempo reale come sta andando (il baseball con le statistiche è come la panna con le fragole).

C’è un video che spiega come usare il programma, non per scoprire il baseball, ma tutto quello che può esserci in una app da 7,99 euro.



Vengo al punto, nascosto in un commento alla fine del pezzo:

L’aggiornamento in tempo reale della pagina web è notevole. Informazioni stile Espn per la partita di un ragazzino.

Si tratta effettivamente di cose che una volta potevano fare solo le televisioni. Adesso stanno su un computer da tasca, a un prezzo minimo, accessibili a chiunque, complete ed esaustive.

Questo è il progresso: quando le tecnologie avanzate escono dai laboratori e arrivano alla portata di tutti, complete di ogni possibilità.

Ecco perché non conta molto chi abbia inventato gli schermi touch, ma conta moltissimo chi li ha portati nei negozi di tutto il mondo.

Per inciso, gli stessi autori producono una app simile per il basket.


