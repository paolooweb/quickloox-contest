---
title: "Non solo per dire io c'ero "
date: 2011-04-15
draft: false
toc: false
comments: true
categories: [ ]
tags: [script]
---
Mi sono obbligato a lasciare decantare per qualche giorno le sensazioni della presentazione del libro di Jay Elliot Steve Jobs – L’uomo che ha inventato il futuro di sabato 2 aprile alla libreria Hoepli di Milano. Le impressioni a caldo le pubblicano tutti, basta poco.

Non mi aspettavo tutta questa folla di giornalisti per un libro su Steve Jobs, soprattutto per un libro sugli anni di Steve Jobs ad Apple prima che venisse cacciato da John Sculley, l’uomo che proprio Jobs aveva voluto al timone di Apple. Si tratta di preistoria, apparentemente del tutto distaccata dal presente. La Apple di oggi è completamente diversa, anche se Jobs è sempre lui.

Questo è anche uno dei due motivi per i quali il libro è interessante. Jobs è sempre lui, ma è cresciuto. Ha imparato lezioni, sbattuto la testa contro difficoltà che avrebbero schiantato almeno moralmente molti altri, colto tratti del proprio carattere che era meglio modificare e rafforzatone altri vincenti oggi come nel 1983, ventotto anni fa. Leggendo del Jobs di ieri si capiscono cose del Jobs di oggi che gli articoli di adesso non riescono a evidenziare, dato che non sanno cogliere le radici di ciò di cui parlano (anche perché metà di queste cose le scrive gente che quasi non era nata nel 1983 e l’altra metà arriva dai polpastrelli di signori maturi incapaci di fare altro che copiare e incollare invenzioni di siti americani di rara superficialità e cinismo, volti solo a fare soldi con i clic degli ingenui).

Il secondo motivo è che mancava proprio un testo scritto da qualcuno che abbia realmente lavorato assieme a Jobs, possibilmente non come centralinista a dieci livelli gerarchici di distanza ma come alto dirigente, spesso a tu per tu con il capo e chiamato anche a condividere, discutere, prendere decisioni a livello corporate.

Questo libro è denso di esperienza diretta, di vita vera, di istruzioni per l’uso della nostra vita. Contemporaneamente scorre liscio come una Coca-cola fresca e non pone ostacoli di principio alla lettura. Che uno si sia interessato alla storia di Apple da sempre, o che non gliene possa fregare di meno, o che sia affascinato da Steve Jobs e meno dai computer della sua azienda, o che voglia capire qualcosa di più su come nasce e da dove arriva la leadership… c’è qualcosa per tutti qui dentro, al prezzo giusto e con il giusto spirito.

