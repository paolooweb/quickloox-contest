---
title: "Aprirsi nel guscio"
date: 2012-02-27
draft: false
toc: false
comments: true
categories: [ ]
tags: [script]
---
Importa poco che si chiami Linux In The Shell: nove cose su dieci che appariranno in questo nuovo blog funzioneranno su OS X e probabilmente basterà poco per quasi tutta la rimanenza.

Il primo intervento, sulla ridirezione, da solo vale il post. È una funzione potentissima di Unix quanto trascurata, per il fatto che non basta memorizzare uno o due comandi, ma occorre la capacità mentale di stabilire tra loro un collegamento.

Quando si chiacchiera amabilmente dell’evoluzione di Mac e sembra che questa o quella scelta di OS X vada troppo in là, non c’è da dimenticarsi che abbiamo a disposizione un antidoto infallibile: per ogni passo avanti dell’interfaccia che pare troppo azzardato, possiamo studiare il Terminale e compiere un piccolo passo indietro, per equilibrare.

Avere la padronanza dell’interfaccia più evoluta e contemporaneamente le basi assolute del funzionamento della macchina è il massimo della libertà e delle potenzialità. Ben altro che fermarsi a mezza strada perché dà fastidio lo scorrimento naturale (ammesso che lo sia).

