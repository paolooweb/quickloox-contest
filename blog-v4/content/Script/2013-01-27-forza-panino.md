---
title: "Forza panino"
date: 2013-01-27
draft: false
toc: false
comments: true
categories: [ ]
tags: [script]
---
Iruben mi segnala la sedicente notizia su iSpazio per cui l’analista Gene Munster annuncia la Tv di Apple nel 2013.

Lo stesso iSpazio, informatissimo, scrive magari potrebbe essere la volta buona. […] va inoltre ricordato che Munster è dal 2011 che predice la messa in commercio di una televisione di Apple.

Non riesco a trattenermi dal commentare alla vista della foto (scopiazzata da 9to5Mac come tutto il resto): un albero di Natale, a fine gennaio. Dadaismo puro, neanche l’orinatoio di Duchamp è così dirompente.

I commenti sono quelli di Facebook, il quale si perita di indicarmi altri contenuti di livello analogo che mi potrebbero piacere:

Whatsapp diventerà a pagamento se non inoltrerai questo messaggio a 10 persone.

McDonald’s interrompe con effetto immediato la promozione per ricevere un panino GRATIS ogni giorno.

Qui casca l’asino: Elio e le Storie Tese ci hanno insegnato che, senza Panino, la festa è insoddisfacente.

