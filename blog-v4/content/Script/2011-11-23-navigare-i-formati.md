---
title: "Navigare i formati"
date: 2011-11-23
draft: false
toc: false
comments: true
categories: [ ]
tags: [script]
---
Uno dei punti di forza di iCloud è la sincronizzazione indolore dei contenuti prodotti con iWork. Prima passavo da iDisk e la procedura era certo funzionante, ma meno funzionale di ora, quando avere lo stesso documento su Mac, iPhone e iPad è veramente questione di un attimo.

Quando si arriverà a raffinare il meccanismo, spero che Apple metterà mano anche al raffinamento dei formati delle app. Ho appena affidato a iCloud milleottocento battute scritte in Pages, con dentro giusto una parola in corsivo, e il file risulta di 353 kilobyte, tipo centovantasei volte quanto ho scritto.

Con tutta la buona volontà e la tolleranza, è un tantino eccessivo. E non mi consola sapere che nel tempo la compatibilità iCloud diventerà prerogativa di tutti i programmi ben realizzati, come è già accaduto con GoodReader.

