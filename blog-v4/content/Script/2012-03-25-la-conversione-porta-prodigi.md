---
title: "La conversione porta prodigi"
date: 2012-03-25
draft: false
toc: false
comments: true
categories: [ ]
tags: [script]
---
Confesso di non averlo provato e che avrei dovuto. È che uso BBEdit.

Per tutti gli altri programmi, ecco spiegato come avere l’anteprima in OS X di testi scritti in Markdown con qualunque programma.

Markdown, ricordo, è un sistema per scrivere rapidamente testi pronti per il web senza dover studiare il linguaggio Html e senza ritrovare i codici Html a complicare la lettura, per cui si scrive **cappotto** nel documento di testo markdown e sulla pagina web apparirà cappotto.

Occorre avere installato Markdown nel sistema oppure, come ha fatto l’autore, MultiMarkdown, una versione molto più versatile. Dopo di che si seguono le istruzioni e si arriva a poter scrivere testo Markdown in qualsiasi programma, selezionarlo e averne l’anteprima “tradotto” attraverso il menu Servizi di OS X (Lion).

Chiaramente Markdown non è una soluzione per tutti. È una buonissima soluzione però per chi scrive molto e molto ha a che fare con il web. Senza contare il risparmio di spazio e la facilità di archiviazione e ricerca di documenti che sono solo testo, oltretutto facili da leggere per un umano.

