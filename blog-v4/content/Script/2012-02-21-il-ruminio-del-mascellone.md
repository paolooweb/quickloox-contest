---
title: "Il ruminio del mascellone"
date: 2012-02-21
draft: false
toc: false
comments: true
categories: [ ]
tags: [script]
---
Ogni giorno decollano decine e decine di finte notizie a tema Mac o Apple, pronte per la masticazione stile Neanderthal, quella che il massimo segno di raffinatezza segue le linee delle selce scheggiata.

Al che Apple annuncia il nuovo sistema operativo il 17 febbraio. Quello prima lo aveva annunciato il 24 febbraio dell’anno precedente.

Crunch, crunch. Sono passati sette mesi e già esce il nuovo sistema, crunch, crunch, che scandalo.

Chomp. È uno schifo, perché non ci sono abbastanza nuove funzioni. Chomp.

Glom. È uno schifo, perché sono passati pochi mesi e già cambia tutto di nuovo. Glom.

Cambia tutto, ma nello stesso momento ci sono poche novità. Questa gente mastica sempre da sola, non si incontra mai.

Gnam, gnam. È uno schifo, il Mac non è più lo stesso di prima. Gnam, gnam.

Gurgle. È uno schifo, Mountain Lion è uguale a Lion. Gurgle.

Sembra la pubblicità di quel profumo: She’s always and never the same, sempre uguale, mai uguale. L’unico profumo che si sente è tuttavia di digestione pesante.

Ci vorrebbero critiche. Tecniche, approfondite, articolate, documentate. Mica sarà perfetto Mountain Lion, qualsiasi sistema operativo ha decine se non centinaia di buchi e imperfezioni.

Invece si sentono solo moti esofagei. D’altro canto è difficile parlare con la bocca piena, giusto? E quei 79 euro che consentono di parlare da informati, che scandalo, nell’epoca in cui chiunque può esprimere le opinioni più vuote assolutamente gratis. E si manda giù tutto, senza distinguere.

