---
title: "Rapporti incompleti"
date: 2011-06-22
draft: false
toc: false
comments: true
categories: [ ]
tags: [script]
---
Normalmente la gente usa l’espressione rapporto prezzo/prestazioni come scusa socialmente accettabile per dire non importa se fa schifo, meno costa meglio è.

Preferisco dire rapporto prestazioni/prezzo, giusto per avere un valore più alto relativamente a un prodotto migliore, ma è solo una fisima e non conta.

Conta invece che oltre al prezzo si guardino effettivamente anche le prestazioni, altrimenti è un rapporto incompleto. Un esempio banale: secondo me Textastic e Paragraft hanno un rapporto prestazioni/prezzo vicino, anche se uno costa 7,99 euro e l’altro 0,79 euro.

