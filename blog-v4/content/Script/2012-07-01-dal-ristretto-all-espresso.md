---
title: "Dal ristretto all'espresso"
date: 2012-07-01
draft: false
toc: false
comments: true
categories: [ ]
tags: [script]
---
La nuova Base AirPort Express sta a quella vecchia come il giorno alla notte.

Non c’è confronto tra iPad con schermo Retina e iPad con schermo com’era prima.

Un iPhone 4S con Siri – aspettandolo anche in italiano – si pone su un gradino nettamente diverso di esperienza di utilizzo rispetto a un iPhone 4.

Non parlo di MacBook Pro con schermo Retina perché non ho ancora avuto l’occasione di maneggiarlo.

Quale altra azienda può offrire innovazione radicale su così tanti prodotti diversi un anno per l’altro?

