---
title: "Usato con stile"
date: 2011-05-11
draft: false
toc: false
comments: true
categories: [ ]
tags: [script]
---
Ho trovato splendide queste foto di design mind dedicate a un iPhone di prima generazione, ancora in uso e assolutamente segnato dal tempo e dalle vicissitudini.

Le ho trovate aderenti al mio modo di pensare; non mi dispiace affatto che il mio Mac o il mio iPhone mostrino, come dire, le rughe dell’età.

Mi rendo altrettanto conto che esiste una scuola di pensiero totalmente opposta, per la quale la tecnologia – dall’auto in poi – deve essere sempre scintillante e come nuova.

Se qualcuno vuole commentare il proprio orientamento, lo leggo con interesse. Cicatrici o perfezione?

