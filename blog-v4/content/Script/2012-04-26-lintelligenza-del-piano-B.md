---
title: "L'intelligenza Del Piano B"
date: 2012-04-26
draft: false
toc: false
comments: true
categories: [ ]
tags: [script]
---
Manifestavo la mia soddisfazione per il sito di Mars Themes, che si professa libero da Flash e consiglia la visione in WebKit (Safari o Chrome).

Manuel ha commentato alcune obiezioni che meritano un allargamento del discorso:

Bellissimo che sia flash-free. Bruttissimo che funzioni solo con WebKit.
Un sito veramente ben fatto non è un “works better with…” ma un sito scritto secondo gli standard. Avere un sito che non si apre con le ultime versioni di Firefox, con Opera, con IE secondo me è un suicidio commerciale e anche un po’ da presuntuosi. Non c’è da vantarsi in questo. D’accordo supportare le ultime tecnologie, d’accordo una spolverata di HTML5, ma il troppo stroppia. L’era dell’“ottimizzato per Netscape” direi che si è chiusa da un pezzo…

Non fa una piega. Nessuno vuole tornare a quell’epoca infelice in cui, invece che gli standard, si inseguivano le estensioni di Microsoft che cercava di prendersi il web come cosa propria e Internet deve essere più che mai il luogo della comunicazione universale. Detto questo, però, rispetto al tempo di Netscape e del monopolio di Explorer sono cambiate diverse cose e in modo fondamentale.

Prima di tutto, Explorer era un monopolio, WebKit non lo è affatto. Stando ai dati NetMarketshare di marzo, nel mondo desktop (circa il 93 percento del traffico totale), Explorer fa il 54 percento del traffico, Safari più Chrome (WebKit) fanno il 24 percento. Non c’è alcun rischio che il mondo si ritrovi schiacciato sotto il tallone di WebKit. Firefox fa il 21 percento, Opera il due percento. Non sono le alternative che mancano sul web, oggi. Ieri invece mancavano proprio.

WebKit non potrà monopolizzare niente perché è libero e aperto, open source. Chiunque può modificarlo, verificarlo, migliorarlo. Invece, non sappiamo che cosa ci sia dentro Explorer, che è chiuso e proprietario. Se domani un sito diventasse dipendente da WebKit, chiunque potrebbe guardare dentro la scatola, scoprire perché, risolvere il problema. Quando i siti diventavano dipendenti da Internet Explorer, nessuno poteva farci niente se non provare alla cieca.

È giustissimo invocare il rispetto degli standard. Si ricordi però che i motori di rendering più aderenti agli standard sono WebKit e Gecko (Firefox). Fino alle ultimissime versioni, Explorer si è invece distinto per ignorare gli standard, cercare di affermare i propri, fare le cose in modo diverso per acquisire un vantaggio indebito.

Bisogna poi aprire un capitolo fondamentale sulla differenza tra aprirsi solo con WebKit e meglio con WebKit. Gli standard web sono in divenire e in diverse situazioni non c’è ancora un pronunciamento univoco: varie parti di Html5 stanno nascendo mentre vengono usate ed è possibile che soluzioni adottate dentro un motore non funzionino, o non funzionino perfettamente, dentro un altro motore. E che però quella soluzione sia il più standard possibile.

Qui entra in ballo l’intelligenza del progettista del sito. Si ricorderà l’intelligenza dei progettisti dei siti solo Internet Explorer: niente Explorer, niente sito. Niente intelligenza.

Aprendo Mars Themes con Firefox, si vedrà che certe cose appaiono diverse rispetto all’apertura con Safari; ma la fruizione del sito è altrimenti perfetta. Tutto quello che si può fare sul sito Mars Themes si può fare sia con Safari sia con Firefox (e scommetto anche con Explorer).

La parola magica è fallback, traducibile in piano B. Una volta, quando si progettava un sito universale, lo si faceva in modo che funzionasse in qualsiasi situazione, secondo una logica da minimo comune denominatore. Vantaggio: il sito si apriva comunque. Svantaggio: si perdevano tutte le possibilità avanzate. Come una catena che è robusta quanto il proprio anello più debole, il browser più scarso dettava la progettazione e faceva da palla al piede. Per questo Explorer è in ritardo sugli standard: per conservare la presa serviva la mediocrità.

Oggi si lavora in modo più intelligente: si progetta il sito in modo che, visto con i browser migliori, offra il massimo. E poi si attrezza il fallback: se il sito viene visitato da un browser mediocre, il sito fa del proprio meglio per farlo sentire a casa e per offrire comunque tutto quello che è possibile offrire, anche se non è tutto quello che può fare un browser più evoluto.

Ma tutti possono accedere. Nessuno viene chiuso fuori, come ai bei tempi del monopolio di Explorer. Se il progettista è abbastanza bravo da fare meraviglie che però non tutti possono vedere, le fa. Chi potrà vederle le vedrà e chi non potrà avrà comunque accesso a tutto il sito. Niente palle al piede.

Un esempio splendido di fallback e intelligenza arriva da Flash. I siti venivano progettati con il presupposto che tutti avessero Flash e, se qualcuno non lo aveva, affari suoi. Intelligenza zero.

Poi i siti hanno cominciato a offrire Flash e, nel caso della sua assenza, erogare le stesse funzioni in Html5. Fallback, piano B. Nessuno chiuso fuori, a ognuno l’offerta migliore possibile.

Adesso sono sempre più numerosi i siti che usano Html5 e, nel caso, come fallback offrono anche Flash.

Più intelligenti. Perché Html5 è uno standard libero, aperto e universale. Flash no.

