---
title: "Troppo è meglio"
date: 2012-10-14
draft: false
toc: false
comments: true
categories: [ ]
tags: [script]
---
L’amico Stefano mi ha segnalato una foto di Guy Kawasaki: sushi bar con prese di corrente e un iPad per ogni postazione.

Stefano è rimasto un po’ perplesso. Ci ho pensato su e io no.

Prima di tutto va tenuto conto che si tratta dell’aeroporto (di Minneapolis, Usa). Un posto dove è difficile trovarsi la sera con gli amici e la socializzazione è, diciamo, estemporanea.

Le prese di corrente, a maggior ragione. magari ce ne fossero così a Malpensa. Fuori luogo probabilmente in un sushi bar usuale, appropriate dove passano viaggiatori.

La spiegazione dell’aeroporto ancora non mi basta, però. E se domani mettessero iPad (o altro) nella pizzeria all’angolo? Dopotutto A Virginia Beach gli iPad sono arrivati da McDonald’s.

La mia risposta è ben vengano. C’è voglia di socializzare? Si è in compagnia? Nessuno impedisce di spegnerli, gli iPad. È più interessante parlare con la persona che hai di fianco che guardare lo schermo? Basta fare a meno dello schermo. Lo schermo però, dovesse mai servire davvero per qualcosa, c’è.

Abituato alle ristrettezze italiane, mi fanno più paura i posti dove manca tutto di quelli dove c’è troppo. Al troppo si può sempre rinunciare.

