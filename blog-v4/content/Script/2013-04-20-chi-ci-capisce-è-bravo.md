---
title: "Chi ci capisce è bravo"
date: 2013-04-20
draft: false
toc: false
comments: true
categories: [ ]
tags: [script]
---
Si era già scritto a proposito del falso mito di Steve Jobs che visita il Palo Alto Research Center di Xerox a Palo Alto (Parc) e si appropria dei segreti della workstation Alto per copiarli e fabbricare Macintosh.

La parte falsa è segreti. Le persone che sapevano di Alto fuori da Xerox, specie nell’ambiente degli addetti, erano innumerevoli.

E fuori dall’ambiente? Steve Jobs visitò il Parc a fine 1979. Il Computer History Museum possiede il video di uno spot pubblicitario dedicato ad Alto, su una cassetta Vhs datata 14 maggio 1979.

Wired scrive che non si ha sicurezza della telediffusione del video. L’ipotesi più probabile è che sia stato trasmesso solo su alcune emittenti e non a livello nazionale.

Tuttavia è pressoché certo che sia stato trasmesso, dal momento che il Vhs sembra esattamente una videoregistrazione.

Per quanto pubblicitario, il video mostra la freccia del mouse, il monitor grafico a caratteri neri su fondo bianco, indizi di un’interfaccia che rispetto alla normalità di quegli anni appariva innovativa a chiunque.

La domanda non può più essere se e quanto Steve Jobs abbia copiato. Diventa invece dove stessero tutti gli altri; i “segreti” del Parc passarono perfino dalla televisione, prima della visita di Jobs.

A fare la differenza, più che la conoscenza dei fatti, fu il loro significato. Che solo una persona, evidentemente, capì.

