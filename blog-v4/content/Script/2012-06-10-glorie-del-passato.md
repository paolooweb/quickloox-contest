---
title: "Glorie del passato"
date: 2012-06-10
draft: false
toc: false
comments: true
categories: [ ]
tags: [script]
---
Due documenti eccezionali dedicati alla Apple che fu: il restauro meticoloso di un Apple II Plus del 1982, fino a rimetterlo in funzione, e una lunga retrospettiva di Newton nel suo ventennale.

Non ho mai posseduto un Apple II. Ne portai a casa uno dall’ufficio durante le vacanze di Natale, che passai a giocare a Lode Runner.

Invece vendetti il mio PowerBook Duo per sostituirlo con un Newton, pochi giorni dopo che Apple ne aveva annunciato la cessata produzione. Procedetti anche con una certa fretta, per essere sicuro di trovarlo ancora in qualche negozio. Non me ne sono mai pentito e il MessagePad 2100 è ancora vivo e funzionante a casa mia (lo ammetto, sempre per meno tempo e con meno frequenza). Dopo una quindicina di anni posso ancora rivedere gli appunti di certe riunioni di redazione tenute nel secolo scorso, scritti in inchiostro digitale. E vorrei sapere di quanti bloc notes si può raccontare lo stesso.

