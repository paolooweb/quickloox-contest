---
title: "Se me lo dicevi prima"
date: 2012-03-16
draft: false
toc: false
comments: true
categories: [ ]
tags: [script]
---
Credo di avere già parlato dell’articolo di Brian X. Chen su Wired del 26 febbraio 2009: Perché i giapponesi odiano iPhone.

Che cosa ha di sbagliato iPhone, da un punto di vista giapponese? Quasi tutto: i piani tariffari costosi, la scarsità di funzioni, la fotocamera di bassa qualità, il design non accattivante e il fatto di non essere giapponese.

L’articolo è comprensibilmente andato incontro a varie modifiche nel tempo, ache perché era stato scritto con scarsa deontologia nel maneggiare dichiarazioni e opinioni di persone interpellate.

Il 23 aprile 2010 è stato aggiunto un commento: è comprensibile il nostro scetticismo verso il nostro pezzo originale, ma è anche vero che prezzo alto, mancanza di videocamera e supporto per messaggistica multimediale sono stati problemi risolti.

Il design non accattivante e il fatto di non essere giapponese sono graziosamente ignorati. Il punto è tuttavia un altro.

Alzi la mano chi non ha pensato che nel tempo iPhone avrebbe migliorato le proprie caratteristiche.

I cellulari Apple diventano per la prima volta i più venduti in Giappone, 9 marzo 2012.

