---
title: "Che combinazione"
date: 2013-01-20
draft: false
toc: false
comments: true
categories: [ ]
tags: [script]
---
John Gruber di Daring Fireball è arrivato prima e meglio di me, quindi traduco lui e rimando ai link che si trovano nel suo pezzo:

Ricordate l’articolo che avevo citato a inizio settimana, dove Joe Springer evidenziava (da novembre) come i grandi investitori istituzionali che in estate avevano venduto opzioni sul titolo Apple si preparavano a guadagnare miliardi ove il titolo Apple avesse chiuso [venerdì 18 gennaio] a 500 dollari o meno? Il titolo ha chiuso [esattamente] a 500,00 dollari.

Se non pensate che ci sia stata manipolazione, ho ancora quel ponte da vendervi.

Giusto perché ogni tanto si legge di gente convinta che le azioni salgano o scendano per un milione di iPhone venduti in più o in meno.

Per una spiegazione sintetica dell’accaduto, c’è su Yahoo Finance.

