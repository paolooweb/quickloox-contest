---
title: "Test di lucidità"
date: 2012-06-01
draft: false
toc: false
comments: true
categories: [ ]
tags: [script]
---
Mi sono ritrovato a scattare una foto con iPhone avendo un certo bisogno di sistemarla al volo, direttamente sull’apparecchio, migliorandola in generale senza pretese sofisticate: luminosità e contrasto, un minimo di controllo colore, un ritaglio veloce e via su Twitter.

Ci ho provato con Snapseed, che complessivamente raccoglie recensioni favorevoli. Ma confesso di non avere raggiunto l’obiettivo. Poco tempo a disposizione, forse ero un po’ distratto.

Subito dopo, però, avendo quel bisogno di cui sopra, ho lanciato iPhoto. Ci sono riuscito nel giro di pochi secondi.

Se veramente prima ero poco lucido, vuol dire che l’interfaccia di iPhoto su iPhone riesce a soddisfare anche le esigenze di una persona confusa, il che è un complimento.

Considerazione a latere. L’interfaccia utente sugli apparecchi a schermo piccolo è una sfida formidabile per i progettisti. E si vede.

