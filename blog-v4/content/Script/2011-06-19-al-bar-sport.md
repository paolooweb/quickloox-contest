---
title: "Al bar sport"
date: 2011-06-19
draft: false
toc: false
comments: true
categories: [ ]
tags: [script]
---
Al Bar Sport senti frasi tipo con tutti i soldi che Apple fa con iTunes… seguite di solito da una stupidaggine clamorosa.

Apple fa i soldi con la vendita di hardware: principalmente iPhone, Mac, iPad, iPod touch e iPod. Che iTunes aiuti a vendere hardware è un fatto evidente; dalla vendita di musica o app, tuttavia, Apple ricava unicamente il recupero dei costi o poco più.

Non l’ho detta io al Bar Sport ma l’ha calcolata Asymco. Secondo loro, il costo annuale di gestione di iTunes è ora di 1,3 miliardi di dollari l’anno e con tutta probabilità i profitti eventualmente generati vengono reinvestiti nel potenziamento dell’infrastruttura.

E comunque il rombo non è un modulo adatto allo stile di gioco del Fanfulla.

