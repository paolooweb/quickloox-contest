---
title: "Il leone di montagna e il suo contrario"
date: 2012-02-17
draft: false
toc: false
comments: true
categories: [ ]
tags: [script]
---
Di Mountain Lion si parlerà di più e anche troppo nelle prossime settimane. Quasi totalmente a sproposito, specie al capitolo iOS e OS X diventano una cosa sola. La nuova edizione di OS X chiarirà le cose: a unificarsi è la filosofia e certe procedure di interfaccia, ma i sistemi operativi restano saldamente a evoluzione parallela (e vorrei pure vedere il contrario). A prevedere la sostituzione di OS X con iOS su Mac è stata la persona meno intelligente che io abbia avuto il dispiacere di conoscere e, manco a dirlo, anche questa volta il bersaglio è stato clamorosamente mancato.

Ma non era questo il punto.

La Developer Preview di Mountain Lion – disponibile this summer, questa estate – è stata mostrata confidenzialmente ad alcuni blogger e giornalisti americani tra cui John Gruber di Daring Fireball, che al solito ha scritto esattamente le cose che contano. Gli rubo un paio di passaggi.

Phil Schiller non ha annotazioni da seguire. È articolato, preciso ed esercitato come per i grandi eventi da palcoscenico. Conosce perfettamente la presentazione. Mi colpisce perché ho parlato davanti a mille persone ma non sono mai stato così preparato come lo è Schiller oggi, per un incontro faccia a faccia.

È una quantità di sforzo e attenzione terribile, spesa per quella che immagino sarà una lista di dieci o venti tra giornalisti e gente che scrive. È Phil Schiller [responsabile mondiale del marketing], che passa un’intera settimana sulla costa orientale degli Stati Uniti, che ripete questa presentazione ogni volta a un pubblico di una singola persona. Preparare questa presentazione ha richiesto lo stesso lavoro che ci sarebbe voluto per un annuncio al convegno mondiale degli sviluppatori.

Chi ne ha fatto l’esperienza, pensi a come si comporta un direttore generale del marketing di propria conoscenza. E come annuncia i prodotti alla stampa. Apple non è un’azienda come tutte le altre.

Così Gruber tratta il concetto che più modestamente ho cercato di spiegare sopra:

Mountain Lion non è un passo avanti verso un singolo sistema operativo che aziona sia Mac che iPad, ma piuttosto un altro passo di una serie verso la definizione di un insieme di concetti, stili e principî condivisi tra due sistemi operativi fondamentalmente distinti.

Si leggerà il contrario di questo. Totalmente a sproposito.

Non si dica che non ho avvisato.

