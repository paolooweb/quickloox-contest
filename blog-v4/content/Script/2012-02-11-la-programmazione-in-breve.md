---
title: "La programmazione in breve"
date: 2012-02-11
draft: false
toc: false
comments: true
categories: [ ]
tags: [script]
---
Mac c’entra relativamente. Tuttavia, nel giro di pochi giorni, si sono accumulate tre notizie.

Prima: hacking di un Nintendo Entertainment System usando Common Lisp, l’erede del linguaggio nato per l’intelligenza artificiale.

Seconda: proclamati i vincitori dell’annuale concorso per la creazione di codice C offuscato, nel senso che guardando il programma non si riesce assolutamente a capirne la funzione ma il programma fa qualcosa di ingegnoso, in poco spazio, possibilmente creando una silhouette con il listato del codice.

La terza: è in corso Js1k, gara a chi crea il più bel programma in JavaScript con il limite di un kilobyte di lunghezza. Come ai tempi dello Zx80 Sinclair di trent’anni fa, un quarto di miliardesimo della Ram di un computer qualunque di oggi.

Questi si possono guardare senza fatica anche con un Mac. C’è da divertirsi e restare ammirati.

