---
title: "I non asini volano"
date: 2012-06-16
draft: false
toc: false
comments: true
categories: [ ]
tags: [script]
---
Il sito thestar.com ha fatto il punto sull’adozione di iPad da parte dei piloti di linea nelle compagnie aeree.

Pare che oltre il 40 percento dei 400 mila piloti aderenti alla Aircraft Owners and Pilot’s Association possieda un iPad. Ma non è tanto questo.

Un iPad in mano a un pilota rimpiazza all’incirca diciotto chili di manuali, carte e documentazione. United Airlines valuta di risparmiare ogni anno 16 milioni di fogli di carta e 1,2 milioni di litri di carburante (per il peso risparmiato).

Prima, ogni pilota American Airlines doveva aggiornarsi la manualistica ogni due settimane, rimpiazzando manualmente nei raccoglitori centinaia di pagine, con un rischio di errori e disguidi evidente. iPad fa da solo.

Altri vantaggi sono il risparmiare tempo nella consultazione delle procedure di utilizzo delle apparecchiature e la possibilità di sovraimporre la situazione meteo alle carte.

L’articolo contiene ben altro e la lettura è vivamente consigliata, a costo di passarlo dentro Google Traduttore.

Chissà se a qualcuno di loro sono andati a dire che iPad è un giocattolo o una macchina non adatta a un uso professionale. A volte trovarsi più in alto della media è un vantaggio.

