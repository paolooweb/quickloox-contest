---
title: "Eccezionalmente, Pubblicità"
date: 2012-03-30
draft: false
toc: false
comments: true
categories: [ ]
tags: [script]
---
Non la farò lunga: faticaccia a mille per portare nuovamente in edicola Macworld Italia. Però ne è valsa la pena comunque andrà.

Per il momento non c’è nemmeno il sito, abbiamo solo Facebook e Twitter. Sta nascendo tutto letteralmente momento per momento (arriverà anche il sito).

Per il momento mi accontento di… spietatezza. Voglio sapere tutto quello che si può fare meglio. E molto probabilmente farlo meglio davvero sarà un gran lavoro; però avere la lista della spesa è comunque un enorme aiuto.

Grazie in anticipo.

