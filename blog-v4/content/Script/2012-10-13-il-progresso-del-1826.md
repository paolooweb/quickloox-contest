---
title: "Il progresso del 1826"
date: 2012-10-13
draft: false
toc: false
comments: true
categories: [ ]
tags: [script]
---
Leggo come Microsoft abbia ottenuto il brevetto per una tecnica di visualizzazione delle informazioni, attraverso scale di colore corrispondenti a variazioni nei dati, che somiglia in modo indecente – per usare un eufemismo – al lavoro compiuto dal barone Pierre Charles Dupin in Francia nel 1826.

Come consolarsi? Con un video del progetto Hope, che porta medicine e assistenza a bambini bisognosi. Giusto perché, parlando di brevetti, ci si scandalizza se Apple se la prende con Samsung per iPad e intanto, giustamente c’è chi sceglie di scippare gente morta da centocinquant’anni, che non obietterà.

