---
title: "Toccare il sogno"
date: 2012-05-30T13:36:36+02:00
draft: false
toc: false
comments: true
categories: [ ]
tags: [script]
---
Sono estremamente soddisfatto delle mie tastiere. È vero, il feeling della leggendaria Apple Extended Keyboard rimane tuttora inarrivabile. Eppure la tastiera integrata del mio MacBook Pro è resistente e precisa, la Apple Wireless Keyboard è ottima (le manca solo la retroilluminazione, a parafrasare un detto usato per i quadrupedi) e la tastiera software di iPad, ancora perfettibile, è comunque già molto adeguata.

Ciò detto, se non ci fosse mezzo la mancanza di un layout italiano, andrei dietro a un sogno impossibile ed estremamente concreto: la Model S Ultimate Keyboard di Das Keyboard.

Quella senza serigrafie sui tasti. Apparentemente per ricoverandi in clinica psichiatrica, quando in realtà – pagato il prezzo dell’apprendistato – velocizza la digitazione.

