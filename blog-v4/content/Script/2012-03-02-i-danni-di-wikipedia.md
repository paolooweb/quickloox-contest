---
title: "I danni di wikipedia"
date: 2012-03-02
draft: false
toc: false
comments: true
categories: [ ]
tags: [script]
---
Non si parla di Wikipedia come sito ovviamente, bensì di Wikipedia come approccio mentale. Progetto che partito con una idea nobile – iniziamo a scrivere l’enciclopedia definitiva, tutti insieme – è finito per diventare il binario morto dell’apprendimento. Wikipedia dovrebbe servire a chi studia per scrivere pagine, non per leggerle.

Purtroppo la convinzione che basti leggere una cosa su una pagina per avere compreso una materia è pervasiva e non conosce ostacoli. Ci è finito dentro persino il New York Times, con James B. Stewart a scrivere di Apple che va contro la legge dei grandi numeri.

Il senso del discorso vorrebbe essere per un’azienda enorme diventa sempre più difficile crescere velocemente e lo capisce anche un bambino. Solo che Stewart inizia citare matematici, variabili che vanno verso la media con il crescere dei campioni misurati e via via accumula sventatezze incredibili per il giornale e per il il giornalista.

Alla fine, su un blog qualsiasi, appare un articolo intitolato Arrestato per violazione della legge dei grandi numeri in cui si chiarisce che il matematico citato è solo un omonimo, la legge spiega una cosa tutta diversa e Stewart viene invitato a fare quello che sa fare meglio: trascrivere le proprie interviste credulone agli analisti.

Di Stewart viene detto che praticamente sembra voler riscrivere Wikipedia nel tentativo di apparire colto. E si capisce che il danno viene dall’averla letta, Wikipedia, ed essersi fermato lì.

