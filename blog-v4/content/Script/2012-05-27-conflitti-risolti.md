---
title: "Conflitti risolti"
date: 2012-05-27T13:05:52+02:00
draft: false
toc: false
comments: true
categories: [ ]
tags: [script]
---
Lo sviluppo di X-Chat Aqua era fermo all’ultima versione del 25 ottobre 2006.

Mi sono rallegrato di vederne una versione alternativa, chiamata XChat Azure, comparire su App Store per Mac. So che ci sono programmi più moderni e più graficamente interessanti per fare chat Irc, solo che su certe cose sono nostalgico.

Per versione alternativa intendo dire che X-Chat Aqua era un derivato del programma XChat per Linux e così vale anche per XChat Azure. Non evoluzioni sequenziali, piuttosto discendenti di un unico progenitore.

La cosa interessante è che ovviamente si parla di open source, software libero e aperto. La presenza di XChat Azure è una ulteriore conferma del fatto che la convivenza tra software libero e la distribuzione sorvegliata di App Store è possibile, a smentire molti timori di conflitto.

Per chi avesse dubbi, GitHub permette il pieno accesso a tutto il codice sorgente del programma e alle strutture dati necessarie per produrre l’eseguibile su Mac.

Pagina dove si scopre anche il possibile futuro arrivo di una versione touch per iOS.

