---
title: "Aggiornamento forzoso"
date: 2011-10-21
draft: false
toc: false
comments: true
categories: [ ]
tags: [script]
---
Sono uno specialista nel mettere in ginocchio il mio Mac. Quando annaspa sotto decine e decine di pagine web, impaginati a tutto schermo e quelle venti-venticinque applicazioni aperte, è possibile che succedano cose strane.

La cosa strana di questo giro è che le finestre del Finder si aggiornano con lentezza suprema, o non si aggiornano del tutto.

Non appena facessi un logout-login o chiudessi qualche applicazione superflua tornerebbe tutto a regime, così mi sono invece posto l’oziosa curiosità scientifica di come forzare l’aggiornamento di una finestra del Finder che non ne voglia sapere.

Mac OS X Hints mi ha dato la risposta: un semplicissimo AppleScript che recita

tell application "Finder"
tell front window
update every item with necessity
end tell
end tell

Le istruzioni vanno inserite in una finestra di AppleScript Editor, da salvare a piacere come script o applicazione. Cosa interessante su Snow Leopard e Lion: niente vieta di creare un nuovo servizio di sistema con Automator, usare l’azione Esegui AppleScript e indicare lo script come oggetto dell’azione stessa. In pratica, rendere sempre disponibile il comando nel menu Servizi.

