---
title: "In punta di dita"
date: 2012-04-01
draft: false
toc: false
comments: true
categories: [ ]
tags: [script]
---
È ancora presto per affermarlo con certezza e tuttavia iniziano a esserci prove indiziarie a sufficienza: la digitazione prolungata sulle tastiere dei Mac limita enormemente i danni ai polpastrelli che altrimenti tendono ad accumularsi nel caso di chi usa normali (leggi: di scarsa qualità) tastiere di Pc.

Certamente non è una questione che debba togliere il sonno al novantanove percento di noi, che al più scrive qualche email e documenti da ufficio o da scuola, poche pagine.

L’uno percento invece dovrebbe farci un pensierino, specialmente se ha l’abitudine di andare un po’ pesante sui tasti, come fa il sottoscritto (ammiro moltissimo le odierne tastiere Bluetooth di Apple, che non sono ancora riuscito a spaccare come quelle di certi vecchi PowerBook).

Per quantificare: non c’è rischio di consumare le impronte digitali come accade su certi portatili Dell, ironicamente proprio quelli che vantano la possibilità di utilizzarle come mezzo di autenticazione. Se infatti la digitazione è particolarmente intensa per lunghi periodi, i tasti economici tendono a perdere uno strato di materiale infinitesimo nello spessore e, disgraziatamente, dotato di una minima quanto inesorabile proprietà abrasiva. Il processo è del tutto indolore e, ripeto, ci vuole molto, per molto tempo. Il più delle volte nessuno se ne accorge, anche perché nella vita non è che le impronte digitali servano molto. Ma chi avesse per qualsiasi motivo un campione vecchio, magari depositato in caserma al tempo del servizio militare, e sapesse di avere lavorato su tastiere di Pc per molto tempo, provi a fare un confronto: è probabile che il rilievo dell’impronta si sia percettibilmente assottigliato. Niente a che vedere con certi scherzi che girano su Internet, ma un reale assottigliamento, decimi di millimetro.

Su un Mac non esiste questo problema. Le tastiere Apple sono realizzate con una miscela plastica di qualità elevata, messa a punto personalmente da Jonathan Ive durante una sperimentazione di molti mesi. Possono comunque spezzarsi o perdere la serigrafia (certo sudore è particolarmente corrosivo), ma non consumarsi. A costi di produzione nettamente inferiori rispetto a quelli delle (ottime) Tactile One Keyboard di Matias, pure compatibili iPhone.

Non c’è modo di tornare allo spessore precedente dell’impronta: si deve sperare di non avere problemi con la questura o con il consolato. Oppure essere abbastanza furbi da crearsi una impronta sintetica con un po’ di glicerina.

Ancora una volta si capisce che scegliere un Mac è la cosa più intelligente che si possa fare. Non solo si conservano il divertimento e l’appagamento dell’usare un computer fatto a dovere. Ma anche le punte delle dita.

