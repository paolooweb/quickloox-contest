---
title: "Guerre stellari   una vecchia speranza"
date: 2012-04-04
draft: false
toc: false
comments: true
categories: [ ]
tags: [script]
---
Ho appena replicato a Roma una presentazione tenuta settimane fa a Milano per un gruppo di responsabili informatici di grosse aziende.

Argomento generale: le nuove condizioni del computing stanno dando vita a una seconda rivoluzione industriale, che offre grandi opportunità competitive e di progresso a chi decida di cogliere l’occasione. Le aziende, abituate a una organizzazione informatica monolitica e monopolistica, hanno l’opportunità – o da qui a tre anni il bisogno, e sarà peggio – di abbracciare un nuovo mondo del computing dove domina la diversità e dove vige la regola della mescolanza tra l’uso privato e quello professionale degli apparati.

La distanza di tempo era insufficiente per modificare i contenuti. Però l’esposizione ha tenuto conto della stretta attualità.

Research In Motion, quella di BlackBerry, sta andando malino e il problema vero è che non si intravede una strada convincente di risalita dalla china. L’azienda è stata immobile come Nokia davanti alla novità dei computer formato cellulare e degli schermi al tocco e ora forse è troppo tardi. Nokia è finita lobotomizzata. Research In Motion potrebbe finire anche peggio.

Dell, l’azienda che aveva fatto dei personal computer venduti via Internet un business planetario, dopo essersi dichiarata per bocca del fondatore no longer a Pc company, non più una azienda di computer, sta per acquisire Wyse. I veterani dell’informatica ricorderanno questo nome: Wyse ha sempre fabbricato terminali vuoti, ottimi ieri per collegarsi ai mainframe e magari domani per i nuovi sistemi cloud. E visto che siamo nell’attualità, la capitalizzazione di mercato di Apple è oggi venti volte quella di Dell.

Un’ultima cosa è rimasta uguale dalla presentazione di Milano. Qualche amministratore di rete spera in Microsoft, in Windows 8, nei tablet che arriveranno. Spera che tornino i bei vecchi tempi in cui la guerra dei personal è stata dichiarata finita con la vittoria di Windows e in tanti cercavano di imporci un sistema operativo unico, senza alternative. Non sappiamo che tempi arriveranno. Sappiamo però che non saranno quelli già trascorsi.

