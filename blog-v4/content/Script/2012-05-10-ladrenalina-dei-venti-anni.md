---
title: "L'adrenalina Dei Vent'Anni"
date: 2012-05-10
draft: false
toc: false
comments: true
categories: [ ]
tags: [script]
---
In questo periodo l’agenda è particolarmente densa. Però ho ugualmente partecipato a modo mio ai festeggiamenti del ventesimo anno di Wolfenstein 3D, giocando una partita nell’edizione browser gratuita approntata per l’occasione.

Mi è tornata in mente l’estate in cui si restava in due in ufficio a giocare Castle of Wolfenstein su Apple II, nove tasti ciascuno per governare movimenti, armi da fuoco, granate, apertura dei bauli. L’edizione 3D è stata praticamente l’inaugurazione dei giochi d’azione con vista in prima persona. Ne sono usciti innumerevoli, ma la tensione generata dall’apertura delle porte d’acciaio e l’ansia con cui si fa fuoco a volontà contro l’ufficiale nazista cui rubare il mitra penso siano rimaste ineguagliate. Come generatore di adrenalina, è assolutamente perfetto.

Si può provare il brivido anche su iOS, con una versione Classic Lite gratuita e una versione Classic Platinum.

Su Mac è più complicato, dato che iOS è adattissimo (per paradosso) e recuperare i grandi classici, mentre i passaggi da un sistema all’altro sui desktop sono micidiali. Un Mac vecchio a sufficienza, o un emulatore, possono fare funzionare l’edizione allestita da MacPlay. Ho trovato una versione che gira su OS X grazie all’incapsulamento di una istanza di DosBox e però ho grossi dubbi sulla sua liceità.

Può darsi che qualcuno ne abbia compilata una versione Mac nativa, visto che il codice sorgente è liberamente disponibile. Va trovata, però. Con meno ansia di quella che viene quando sai perfettamente che ti aspetta dietro la porta d’acciaio.

