---
title: "E infine uscimmo"
date: 2012-02-22
draft: false
toc: false
comments: true
categories: [ ]
tags: [script]
---
Sono di nuovo su Lion 10.7.3, con i dati a posto e le applicazioni a posto.

Il mio errore è stato non avere a portata di mano una copia aggiornata dell’installazione di Lion. Ora ne ho fatte due di riserva, a contorno della possibilità sempre presente di scaricare l’installazione via Internet dalla partizione di recupero, ciò che non mi era stato possibile fare.

È anche tornato in funzione Messaggi versione beta, primo assaggio di Mountain Lion.

Il prossimo passaggio è appunto quello verso Mountain Lion Developer Preview 1. Solo rimandato di qualche giorno, intanto che viene riconsolidato il backup su Time Machine.

