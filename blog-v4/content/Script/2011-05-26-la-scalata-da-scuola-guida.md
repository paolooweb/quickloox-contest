---
title: "La scalata da scuola guida"
date: 2011-05-26
draft: false
toc: false
comments: true
categories: [ ]
tags: [script]
---
L’anno scorso, in termini di capitalizzazione di mercato (quotazione titolo moltiplicato numero di azioni), Microsoft era la prima azienda informatica in assoluto.

Poi è stata superata da Apple.

Adesso è finita al terzo posto, dietro Ibm. Azienda che addirittura ha reinventato da zero o quasi tutta la propria attività, rinunciando alla vendita dei personal computer per andare sui servizi alle aziende.

Coraggio, Microsoft. In un certo senso, dalla prima alla terza è comunque una scalata. Occhio alla retromarcia…

