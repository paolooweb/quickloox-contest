---
title: "Il design come il buon vino"
date: 2013-01-28
draft: false
toc: false
comments: true
categories: [ ]
tags: [script]
---
Quasi incredibile, pensando ai ritmi dell’elettronica di consumo, la coerenza della struttura della pagina home del sito Apple negli ultimi quindici anni, sintetizzata in una presentazione con l’aiuto della Wayback Machine presso l’Internet Archive.

C’è un breve confronto, per quanto assai meno articolato, con Microsoft, Dell, Hewlett-Packard, Ibm e Sony. Le immagini, anzi, le pagine parlano da sole su quanto significhi avere un design indovinato.

