---
title: "Quando il buon design finisce sottoterra"
date: 2011-05-12
draft: false
toc: false
comments: true
categories: [ ]
tags: [script]
---
Scrive (e lo ringrazio) Marco.

Da semplice appassionato di grafica, ho sempre osservato loghi e font usati dalle aziende. Sono a Londra ora e sono rimasto sorpreso dalla forza che ha Tfl, l’azienda che gestisce tutto il trasporto pubblico della città (metro, bus, car sharing, biciclette, ztl, taxi, tram, battelli, strade…). Per molti aspetti ricorda Apple: in ogni affissione c’è il suo logo, il font usato è sempre quello (a mio avviso molto elegante).

Il logo tra l’altro è piuttosto modellabile e quindi garantisce una maggiore forza (semplice bianco e nero, oppure cambia colore in base a settori specifici del trasporto pubblico).

Ma la gemma preziosa è che come Apple, Tfl nel sito ha una sorta di Gui Guidelines dove spiegano come sono progettate le mappe e il perché in alcuni casi venga usato una rappresentazione piuttosto che un’altra; quali siano i colori corretti usati dall’azienda (in Pantone, Rgb, web, Cmyk e altri) e in quali casi; come si fanno le locandine e perfino quali sono le regole per fare l’insegna del box informazioni, sempre accompagnate da spaziature, margini eccetera.

Una miniera di info che vale la pena sfogliare se si è appassionati. In ogni caso non è tempo perso, perchè la prossima volta che guarderò la mappa del Tube saprò che, dove c’è il nome di una stazione in un riquadro, dovrò camminare per raggiungere la stazione ferroviaria. Potenza del (buon) design.

Essere capaci di apprezzare il linguaggio della comunicazione è una marcia in più. Perfino… turistica.

