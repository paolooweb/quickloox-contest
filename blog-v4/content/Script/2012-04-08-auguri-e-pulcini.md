---
title: "Auguri e pulcini"
date: 2012-04-08
draft: false
toc: false
comments: true
categories: [ ]
tags: [script]
---
Ecco dai colleghi di Macworld.com una raccolta di app a tema pasquale e, dal californiano Santa Cruz Predatory Bird Research Group, una nest cam, nidocamera, che mostra in tempo reale pulcini autentici, per quanto di falco pellegrino.

Una ricerca nest cam live porta numerosissimi risultati, ma la cam di cui sopra è l’unica che sia riuscito a trovare funzionante senza Flash, quindi in teoria anche su iOS.

Poter osservare pulcini distanti diecimila chilometri significa che come umani siamo molto pasticcioni e però conteniamo qualcosa di grande. A ognuno declinarlo come vorrà per questa domenica.

E buona festa!

