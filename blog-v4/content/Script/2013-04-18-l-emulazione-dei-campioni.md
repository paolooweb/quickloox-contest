---
title: "L'Emulazione Dei Campioni"
date: 2013-04-18
draft: false
toc: false
comments: true
categories: [ ]
tags: [script]
---
Così Facebook Home è stato prototipato con Quartz Composer di Apple.

Assai più interessante la flessibilità dello strumento. Nel giro di una manciata di giorni David O’Brien ha deciso di cimentarsi e provare a raggiungere lo stesso risultato, con lo stesso strumento. Ha documentato i risultati in cinque video da pochi minuti ciascuno.

O’Brien non è uno sprovveduto, come si può constatare presso il suo sito personale.
I video raccontano comunque di una soluzione piuttosto accessibile per la creatività di questo genere, per giunta gratuita.

Chi se la sente di guardare le applicazioni dall’altro lato, quello della loro costruzione, e magari scoprire un nuovo ramo d’attività o di passione? Per arrivare al livello degli specialisti ci vuole molto lavoro. Per togliersi sfizi, un pizzico di applicazione.


