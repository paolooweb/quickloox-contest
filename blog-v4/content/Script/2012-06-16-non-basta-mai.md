---
title: "Non basta mai"
date: 2012-06-16
draft: false
toc: false
comments: true
categories: [ ]
tags: [script]
---
Scrivendo per Enter the Cloud ho avuto l’occasione di conoscere gli amici di Enter e anche di provare in anteprima il loro servizio Cloudup, partito da pochi giorni.

Cloudup è qualcosa che fa comodo a un professionista o a un appassionato molto appassionato: un server a comando, da pagare al minuto e istantaneamente configurabile in termini di dotazione, come se guardando il nostro Mac potessimo decidere che in questo momento, per usare BBEdit e scrivere un post, è sufficiente un processore solo, mentre questa sera, per una sessione di World of Warcraft, ne vogliamo quattro, con 32 gigabyte di Ram a disposizione invece dei consueti quattro o otto. E domani, visto che si torna dall’escursione, aggiungere altri dieci gigabyte di disco rigido, che dopodomani – una volta fatta la cernita delle foto – non serviranno più.

Ho provato in anteprima la cosa, dicevo, e funziona. All’inizio fa impressione perché poi a questo server bisogna parlare tramite la riga di comando, il Terminale. E tuttavia, per compiti come installare WordPress e distribuire un blog, oppure trasmettere lo streaming video di un evento, diventa una risorsa interessante, perché si paga al minuto, in relazione a quanto è grosso il server in quel momento, e quando serve a poco lo si tiene piccolo; quando non è più utile al proprio scopo lo si chiude e la spesa termina.

Avendolo provato in anteprima, l’ho provato in beta e così qualcosa non ha funzionato subito come doveva. Forse perché era in beta e poco affollato, forse perché è bene organizzato, ho avuto modo di collaudare anche il supporto per email. Due problemi, due risposte (a comunicare soluzione del problema) entro una manciata di minuti dall’invio. Non ho provato il supporto via chat, che adesso vedo inserito nelle pagine del sito, ma se funziona come quello via mail non ci saranno grossi problemi di utilizzo.

Non ho intenzione di attivare ora server Cloudup e però – questa è la vera novità – mi capiterà di farlo. In innumerevoli occasioni è capitato di pensare, per dire, allo streaming video senza che però fosse possibile acquistare le risorse per provvedere, o serviva un processore in più per velocizzare un urgentissimo rendering, o si è creata una esigenza temporanea di avere un server web a disposizione. Prima tutto rimaneva pour parler, adesso si tratta di fare qualche clic sulla pagina di Cloudup e nel giro di pochi minuti partire.

I prezzi variano proprio a seconda della configurazione, ma si riesce anche a stare intorno ai trenta euro al mese e la cosa importante è che se il mese dopo non c’è più bisogno, non esiste alcun abbonamento. Si spegne la macchina e i trenta diventano zero.

È un altro segno dei tempi che cambiano, velocemente quanto drasticamente. Una volta c’era il Mac e c’era caso che servisse a tutta la famiglia, o a mezzo ufficio. Ora abbiamo un computer in mano e un altro in tasca, con la possibilità di attivarne uno virtuale servisse anche solo per un quarto d’ora. E se prima non bastavano mai processore e disco rigido, andando sempre un po’ a peso, ora a non bastare mai è la flessibilità delle risorse a disposizione. Oltre che controllare i costi.

