---
title: "Un marchio inconfondibile"
date: 2012-03-30
draft: false
toc: false
comments: true
categories: [ ]
tags: [script]
---
Il marketing Microsoft ha lanciato la sfida Smoked by Windows Phone, letteralmente bruciati (per via della velocità) da Windows Phone.

In pratica si va in un negozio Microsoft e si partecipa con il proprio smartphone a una gara di velocità nell’esecuzione di un certo compito, contro un commesso del negozio armato di Windows Phone. Se il visitatore vince, si porta a casa un portatile da mille dollari. Se perde, ha l’opportunità di scambiare il proprio smartphone con un apparecchio Windows Phone.

Le sfide sono accuratamente pensate in modo da sfruttare le caratteristiche di Windows Phone e assicurare la vittoria al commesso. Fino a qui niente di male: è marketing e chiunque si informi prima è consapevole che certamente perderà.

Il bello è un altro: per pura fortuna o per un errore del commesso nell’enunciare i termini della sfida, qualcuno riesce a vincere. Ma il portatile mica glielo danno: con una scusa qualunque, viene dichiarato comunque vincitore Windows Phone.

In un caso, racconta Cnet,

Mi è stato chiesto di localizzare un ristorante a quattro stelle nelle vicinanze. L’ho fatto prima di loro. Allora mi hanno risposto che la mia localizzazione non mostrava le indicazioni per arrivarci, per cui avevo perso. Ho fatto presente che Android mostrava effettivamente le indicazioni e Windows Phone no. Mi hanno chiesto di lasciare il negozio senza premi.

A seguito del caso del secondo link, Ben Rudolph – un evangelist Microsoft che per mestiere difende l’azienda sui media digitali – ha lanciato un tweet che dice Voglio mettere le cose a posto. Così – rivolto al protagonista della vicenda – ho un portatile e un telefono (e le scuse) per te. Scrivimi!.

Tutto questo è inconfondibilmente, irreparabilmente, ineluttabilmente Microsoft. Che cambia, non è più la stessa di un tempo, avvicenda persone, modifica le strategie. E ti considera sempre nella stessa identica maniera.

