---
title: "Uomini e topi"
date: 2011-05-16
draft: false
toc: false
comments: true
categories: [ ]
tags: [script]
---
Riassunto di Cnn Money da un articolo del New Yorker che rimette a posto un po’ di cose.

Il mito, ripetuto alla nausea dai detrattori di Apple, è che per creare Macintosh Steve Jobs abbia rubato le idee sviluppate al Palo Alto Research Center di Xerox (Parc).

La verità è che per quelle idee pagò, con centomila titoli dell’azienda un anno prima dell’offerta pubblica di azioni.

La verità più profonda […] è che Jobs non aveva interesse a riprodurre il lavoro compiuto da Xerox al Parc.

Jobs sapeva che la dimostrazione cui avevano assistito nel 1979 lui e Bill Atkinson […] conteneva i semi di una rivoluzione. Ma conosceva bene anche i suoi difetti fatali, a partire da un mouse a tre pulsanti che costava trecento dollari e si rompeva in due settimane.

La differenza tra manipolazione diretta e indiretta – tra tre pulsanti e un pulsante, trecento dollari e cento dollari, tra una pallina sostenuta da cuscinetti a sfere e una a rotolamento libero – non è banale. È la differenza tra qualcosa concepito per esperti, ciò che avevano in mente al Parc, e qualcosa concepito per un grande pubblico, cià che aveva in mente Apple. Il Parc stava costruendo un personal computer. Apple voleva costruire un computer popolare.

Come promemoria per quando si leggerà l’enesimo articolo che accusa Jobs di avere semplicemente copiato il lavoro del Parc.

