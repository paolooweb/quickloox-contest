---
title: "Il vero sostegno all'istruzione"
date: 2011-05-05
draft: false
toc: false
comments: true
categories: [ ]
tags: [script]
---
Questa storia è solo vagamente attinente a Mac e compagnia.

Pochi sanno, a ragione, che Amar Bose, il fondatore dell’omonima azienda di prodotti audio, si è laureato al Massachussetts Institute of Technology, il Mit.

Oggi tutti sanno che, compiuti ottantun anni, Bose ha donato al Mit la maggioranza delle azioni della propria azienda.

Sono azioni senza diritto di voto; l’azienda Bose continuerà a funzionare esattamente come ha fatto finora. Tuttavia, in caso di dividendo azionario, il Mit riceverà la propria quota.

L’istituto non potrà neanche rivendere le azioni, che in pratica rappresentano una forma di finanziamento dipendente dalle fortune della Bose e tuttavia sganciatissimo da essa, senza che l’azienda e la scuola debbano mai entrare in conflitto oppure entrare in negoziazioni.

In Italia si chiacchiera molto sul rapporto tra l’industria e la scuola e a me piace molto leggere notiziole come questa, di gente che alle chiacchiere preferisce fatti… sonori.

