---
title: "Proteste a inversione"
date: 2012-04-14
draft: false
toc: false
comments: true
categories: [ ]
tags: [script]
---
Ci voleva finalmente: un articolo che si chiede se Apple non stia perdendo tempo a vendere Mac.

Lo svolgimento è piuttosto lungo e da nessuna parte si tiene minimamente conto del discorso profitti, ossia di quanto guadagna il produttore dalla vendita del prodotto. Invece si resta sulla quota di mercato, che è indicatore sempre più inutile e sempre più sciocco. Per guadagnare dai suoi Pc la stessa cifra che guadagna Apple dai Mac, Hp deve vendere circa il triplo dei Pc, dunque parlare di quote di mercato è abbastanza futile.

Il livello di incomprensione della faccenda è talmente elevato che mi fa pensare a qualche sorpresa significativa di Apple in campo Mac. Iniziano a girare gli schermi Retina, i lettori Dvd sono sempre meno considerati, OS X e iOS si intrecciano insieme nello sviluppo, il mercato promette centinaia di apparecchi compatibili Thunderbolt da qui al 2013… quelli che si lamentano perché i Mac rimangono sempre uguali a sé stessi, secondo me, presto potranno passare a lamentarsi che i cambiamenti ai nuovi Mac sono troppo drastici.

