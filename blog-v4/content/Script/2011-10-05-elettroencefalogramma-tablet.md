---
title: "Elettroencefalogramma tablet"
date: 2011-10-05
draft: false
toc: false
comments: true
categories: [ ]
tags: [script]
---
Storia esemplare di Technologizer su che cosa è accaduto alle tavolette che hanno rivaleggiato con iPad nel 2010.

Si tratta di nientemeno che trentadue (!) profili, prima di elencare i quali si avvisa: se piace il lieto fine, meglio smettere di leggere ora.

Qualche commento sparso.

Come tutte le tavolette Archos, sembra stia in un universo alternativo dove iPad non è mai esistito.

Streak 5” è stata in giro per un annetto e poi si è congedata.

L’era dei tablet a doppio schermo è terminata quando eDGe è morta nella primavera 2011.

Se conoscete qualcuno che possieda una ExoPc, ne rimarremo impressionati.

Non pensiamo che WindPad 110 di Msi sia mai entrata in commercio.

Libretto W100 di Toshiba è svanita rapidamente come è arrivata.

Parte delle conclusioni dell’articolo:

In agosto 2010, non pensavamo che qualunque azienda razionale ritenesse una passeggiata mettersi contro iPad. Ma siamo sicuri che molti pensassero possibile produrre una alternativa ragionevolmente di successo e che tutte le rivali di iPad messi insieme avrebbero superato il tablet di Apple. Invece iPad 2 continua imperterrito a dominare la categoria e la maggior parte delle notizie sulla vendita di prodotti alternativi riguarda il loro fallimento.

C’è da riflettere. Non si tratta di un prodotto che fallisce, ma di un intero comparto nato dal nulla, composto da decine di aziende e di prodotti, e nessuno che presenti qualcosa di serio, forse solo con l’eccezione di Samsung, che tuttavia ha un po’ giocato con i numeri effettivi di vendita.

