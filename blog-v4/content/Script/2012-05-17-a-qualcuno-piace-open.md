---
title: "A qualcuno piace open"
date: 2012-05-17
draft: false
toc: false
comments: true
categories: [ ]
tags: ["script"]
---

Da un post sull’atteggiamento degli operatori telefonici verso gli smartphone con personalizzabilità giudicata (da loro) eccessiva la discussione si è rapidamente spostata sull’apertura e la chiusura del software, con ulteriore salto a parlare di open source e relativi pro e contro. Da qui parto per fare ulteriore contributo alla discussione (o gettare benzina, non so).

La cosa bellissima del software open source è che piace giustamente a tutti. È che a ognuno piace per un motivo diverso.

Quella vecchia battuta, esistente in decine di varianti:

Il paradiso è un poliziotto inglese, un cuoco francese, un tecnico tedesco, un amante italiano: il tutto organizzato dagli svizzeri. L’inferno è un cuoco inglese, un tecnico francese, un poliziotto tedesco, un amante svizzero, e l’organizzazione affidata agli italiani.

Nell’open source c’è un fenomeno simile (chiedo perdono per le inevitabili generalizzazioni). Alla persona comune piace moltissimo perché è gratis; allo scienziato perché è personalizzabile; all’amministratore di sistema aziendale perché è multipiattaforma; al difensore dei diritti civili perché libera dalla schiavitù dei programmi proprietari; al programmatore perché ha una possibilità di contribuire liberamente e anche giovarsene sul lavoro; eccetera.

D’altra parte, la persona comune vorrebbe (gratis) interfacce utente come quelle dei programmi che si pagano; lo scienziato al lavoro su una piattaforma specifica vorrebbe che quella piattaforma venisse supportata perfettamente e che, per fare un esempio stupido, LibreOffice usasse AppleScript; il programmatore vorrebbe anche farci qualche soldo se possibile; il difensore dei diritti civili preferirebbe figure meno ingombranti di Stallman e così via.

In altre parole, le caratteristiche del software open source sono tali che con una mano dà e con l’altra toglie; il risultato netto è un’ottima offerta, a patto di accettare certi compromessi e limitazioni.

Dove sta l’equivoco? Nel voler fare qualcosa con l’open source. Invece il sistema decollerebbe, vertiginosamente, se si partisse con l’idea di fare qualcosa per l’open source.

La persona comune allungherebbe al progetto tre o quattro euro con entusiasmo e tante persone comuni contribuirebbero in modo più che significativo; lo scienziato darebbe un’ora a settimana per tradurre la documentazione o localizzare l’interfaccia; il programmatore, appagato, darebbe più tempo e magari le piattaforme sarebbero più servite nelle loro specificità; il difensore dei diritti civili contatterebbe un grafico e un esperto di interfaccia per aumentare l’usabilità.

L’open source dà il massimo se tutti gli diamo qualcosa. Se è posso scaricare gratis, c’è il compromesso dietro l’angolo.
,wq


