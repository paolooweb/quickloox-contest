---
title: "Quelli dei detersivi"
date: 2011-11-06
draft: false
toc: false
comments: true
categories: [ ]
tags: [script]
---
Li avevo chiamati quelli degli antiforfora.

Avevano scritto una magistrale recensione di iPad in contumacia (guardando su Internet la presentazione di Steve Jobs), che ancora oggi appare profetica.

…aspettative troppo alte rischiano poi di causare delusione quando si alza il sipario, e sembra esser questo il caso dell’iPad.

…potrebbe essere la miglior cornice per foto digitali di tutti i tempi…

La nostra prima impressione è che questo sia una specie di iPod Touch gigante, e non sembra scontato che il mondo ne abbia bisogno, men che meno che sia disposto a pagarlo 500 euro.

Che hanno fatto stavolta? Niente, mi hanno scritto per convincermi ad abbonarmi in promozione alla loro rivista e associazione.

Mi hanno convinto solo a disiscrivermi dal loro elenco di indirizzi e per questo ho fatto clic sul link apposito del loro messaggio.

Link che non porta da nessuna parte, per un errore nel codice.

Non solo incapaci di giudizio, ma anche di tecnica. Da adesso stanno nel filtro antispam, da qui all’eternità.

