---
title: "Il valore di una moneta"
date: 2011-10-11
draft: false
toc: false
comments: true
categories: [ ]
tags: [script]
---
Considererò alcuni segmenti di una interessante notizia diramata da iPhonia, il braccio cellulare di Macitynet, lo scorso 22 luglio. Intervallerò alcune osservazioni.

Bloomberg: iPhone 5 arriverà entro settembre

Non c’è alcun iPhone 5.

iPhone 5 sarà disponibile in Cina entro settembre:

Falso.

Il nuovo smartphone della Mela sarà commercializzato da China Unicom e anche da China Telecom.

China Unicom è l’operatore attuale che distribuisce iPhone ed era una previsione piuttosto facile. Rispetto a China Telecom, per il momento Apple non ha dichiarato alcunché e neanche lo hanno fatto i cinesi. Stabilito che entro settembre è una sciocchezza, abbiamo solo l’aspettativa di Anand Ramachandran, analista per Barclays Capital. Il quale si aspetta un modello speciale di iPhone 4S venduto da China Telecom per il primo trimestre 2012. L’errore non sarebbe di qualche settimana, ma come minimo di tre mesi e forse di sei. A che serve sbagliare di sei mesi un annuncio di prodotto?
La succosa anticipazione arriva dall’autorevole sito finanziario Bloomberg che cita come fonte un anonimo addetto ai lavori.

A parte la strage di aggettivi, è sbagliato. Seguendo Bloomberg ci si accorge che viene citato il sito cinese Caixin, il quale fa riferimento all’anonimo… che in quanto tale potrebbe anche non essere per nulla addetto. La notizia non è di seconda mano come la mette iPhonia, ma di quarta.

La conferma del lancio in Cina in questo trimestre…

Forse non era il caso di scriverlo per la sesta volta in tre paragrafi, visto che è pure falso. Chi fa informazione dovrebbe saper scrivere.

…lascia dedurre che Cupertino ha in programma il lancio di iPhone 5 entro lo stesso periodo in Usa e probabilmente anche negli altri principali mercati internazionali.

iPhone 5 che non esiste, Usa che vedranno il nuovo modello il 14 ottobre e mercati internazionali che arriveranno anche a dicembre. Per l’Italia è il 28 ottobre. Ho già incontrato uno sprovveduto che pianificava l’acquisto del nuovo iPhone sulla base di questo articolo.

Il punto non è essere all’oscuro dei fatti, ma ripetere pedissequamente per soldi quello che raccontano altri altrettanto all’oscuro. Ho giocato ad AppleBingo e ho totalizzato 12 previsioni esatte su 24. Lanciare una moneta mi avrebbe dato probabilmente lo stesso risultato. È normale ignorare che cosa succederà. Un po’ meno propinarlo agli sprovveduti come informazione.

Una moneta avrebbe lavorato molto meglio di iPhonia. A dire del valore reale che hanno le cose scritte lì sopra.
