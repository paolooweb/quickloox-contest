---
title: "Torture immaginarie"
date: 2011-05-20
draft: false
toc: false
comments: true
categories: [ ]
tags: [script]
---
Sergey Brin, cofondatore di Google, afferma che Windows, con la propria complessità da gestire, tortura i propri utenti.

Potrebbe anche essere una semplice frase a effetto, avente lo scopo di promuovere i portatili Chromebook che, sempre secondo Brin, potrebbero sostituire Windows in tre casi su quattro. Windows è drammatico, ma molte persone ci si trovano bene, lo sopportano, lo tollerano, cercano di non farci caso, insomma ci convivono.

Quello che frase a effetto non è: l’80 percento dei dipendenti di Google, parliamo di una multinazionale con numero di utenti a quattro zeri, non usa Windows.

Certe polverose aziendine italiane grandi come l’unghia di un pollice, dove se non c’è un Pc Windows l’ambiente non è professionale, in che anacronismo sono rimaste incastrate?

