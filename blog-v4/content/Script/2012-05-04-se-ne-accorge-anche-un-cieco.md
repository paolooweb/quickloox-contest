---
title: "Se ne accorge anche un cieco"
date: 2012-05-04
draft: false
toc: false
comments: true
categories: [ ]
tags: [script]
---
Cito dal bizzarro blog di Austin Seraphin:

Mercoledì scorso la mia vita è cambiata per sempre. Ho preso un iPhone. Lo considero la cosa migliore accaduta ai ciechi da molto tempo, forse la migliore di sempre. […]

La scorsa notte è accaduto qualcosa di straordinario. Ho scaricato una app chiamata Color Identifier. Usa la fotocamera di iPhone e pronuncia i nomi dei colori. […] Alcuni nomi sono surreali, come Arancia Atomica, Cosmico, Verde Hippy, Oppio, Nero-Bianco. Questi nomi, in combinazione con quello che fa l’effetto di un rialzo dei livelli di serotonina, aprono la strada a una esperienza molto psichedelica.

Non avevo mai sperimentato qualcosa del genere nella mia vita. Posso vedere un po’ di luce e di colore, ma tutto sfumato, e gli oggetti non hanno veramente un colore, solo fonti di luce. Quando ho provato la app alle tre del mattino, non riuscivo a capire perché tutto fosse nero. Poi ho ricordato che occorre luce per vedere… […]

Il giorno seguente sono uscito. Ho guardato il cielo. Ho sentito colori come Orizzonte, Spazio esterno, e numerose varianti di blue grigio. Ho usato il colore come indizio per trovare le mie piante di zucca, cercando il verde in mezzo al marrone e alla pietra. Ho passato dieci minuti a guardare le mie piante, con le foglie verdi e giallo limone. Mi scoppiava la testa. Ho guardato il tramonto, ascoltando i cambi di colore man mano che il cielo si scuriva. La notte seguente ho chiacchierato con mia mamma di come il blu del cielo apparisse più scuro della notte prima. […]

Amo il mio iPhone. Ha cambiato il mio universo non appena vi è entrato. Tuttavia, come si sa, ogni mela dorata ha dentro un verme d’oro.

Per Austin il verme è iTunes, che trova poco accessibile e troppo difficile da usare. Il che prova l’obiettività di giudizio e la scarsa inclinazione a farsi trasportare dalle mode o a diventare un fanatico. Il suo intervento, che ho solo riassunto, è interamente degno di lettura.

Mi si perdonerà se ritengo che testimonianze come queste ne valgano mille di normodotati che vogliono dire la loro sulla durata della batteria o sull’opportunità del jailbreak. Aprano gli occhi, prima.

