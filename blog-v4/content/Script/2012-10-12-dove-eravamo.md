---
title: "Dove eravamo"
date: 2012-10-12
draft: false
toc: false
comments: true
categories: [ ]
tags: [script]
---
Questo è Steve Jobs, ventisette anni prima di iPad:

La strategia di Apple è veramente semplice. Entro questo decennio vogliamo mettere un computer incredibile dentro un libro, che puoi portare in giro e imparare a usare in venti minuti. E vogliamo metterci un collegamento radio in modo che non servano cavi e si possa comunicare con qualsiasi database e altri computer.

Chissà se non lo avessero cacciato da Apple due anni dopo. Ma sarebbe stata dura.

