---
title: "Strategie alte"
date: 2012-02-19
draft: false
toc: false
comments: true
categories: [ ]
tags: [script]
---
iCloud, ha dichiarato l’amministratore delegato di Apple Tim Cook, ha cento milioni di utenti registrati. Tre settimane prima dell’annuncio erano 85 milioni.

Si sono venduti più apparecchi iOS in tre anni che Mac in ventotto, ma la vera mossa strategica di Apple per i prossimi dieci anni è iCloud. Da seguire, da ricordare più che iPhone, la nuvola su in alto (o dentro quel centro dati in North Carolina, se si preferisce).

L’obiezione che avevo sentito più spesso al suo annuncio era ma così mi consumano il piano tariffario a forza di backup. Un altro promemoria sull’opportunità di parlare solo quando c’è effettivamente qualcosa da dire, preziosissimo ora che si inizia il baccano su Mountain Lion.

