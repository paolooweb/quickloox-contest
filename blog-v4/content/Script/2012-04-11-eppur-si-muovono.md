---
title: "Eppur si muovono"
date: 2012-04-11
draft: false
toc: false
comments: true
categories: [ ]
tags: [script]
---
Le cose buone su Internet sono leggere, immediate, positive. Tre esempi.

L’Iran e il supposto tentativo di eliminare Internet. O meglio quella che conosciamo noi, per sostituirla con una di regime, controllata e succube. La negatività si taglia con il coltello. Tant’è che sembrerebbe una bufala. Pubblicata su un sito governativo iraniano non visibile dall’esterno dell’Iran. Prima di sapere come stanno davvero le cose passerà molto tempo e immediatezza addio.

OnLive Desktop e il suo sistema di offrire software in streaming. in questo modo è possibile, tra le altre cose, fare funzionare Office su un iPad (come si commentava tempo fa, difficile capire perché si dovrebbe volere una cosa del genere, ma ora siamo su un altro discorso). Solo che Microsoft non è d’accordo perché sostiene esserci una violazione di licenza. Allora OnLive Desktop cambia e invece di erogare Windows 7 eroga Windows Server 2008, così le questioni di licenza sono a posto. Solo che il software in streaming è meno usabile di prima e i concorrenti sostengono che ci sia sotto il trucco, perché alle tariffe offerte da OnLive il servizio può solo essere in perdita secca.

Quanta pesantezza. Quanta negatività. Segni sicuri che qualcosa non gira.

Virata di centoottanta gradi e veniamo a Minecraft, il gioco di costruzione più sorprendente finora creato rispetto alle risorse impiegate. Il suo autore, Snotch, ha annunciato un altro gioco: sarà di ambientazione fantascientifica e si chiamerà 0x10c.

Non si sa ancora moltissimo, se non che uno degli elementi principali del gioco sarà un processore immaginario e quel processore sarà programmabile dai giocatori.

Github, servizio di alloggiamento e condivisione di codice per programmatori, annuncia dopo pochi giorni che supporterà il linguaggio di programmazione del processore in questione. Il primo pensiero: se sono così rapidi per il linguaggio usato dentro un gioco, offriranno un servizio eccellente ai programmatori veri e ai linguaggi usati nella professione. Tutti ci guadagnano, la cosa è fresca, con una venatura di divertimento.

Leggerezza, immediatezza, positività. Muoversi non basta, conta anche il modo.

