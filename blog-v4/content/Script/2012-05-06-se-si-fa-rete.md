---
title: "Se si fa rete"
date: 2012-05-06
draft: false
toc: false
comments: true
categories: [ ]
tags: [script]
---
Non scopro io la convenienza di iPad in una situazione di gioco di ruolo da tavolo. Il giocatore dispone di sistemi per amministrare la scheda del personaggio, lanciare dadi, tenere nota di incantesimi e oggetti. Il master – colui che anima la storia per conto dei giocatori e li fa interagire con il mondo immaginario in cui si svolge l’azione – può disegnare mappe, tenere conto di mostri e personaggi non giocanti, tenere sotto controllo le schede di tutti i giocatori e così via.

La parte sorprendente tuttavia arriva quando al tavolo di gioco sono presenti più apparecchi, anche iPhone o iPod touch. Con i Messaggi, banalmente eppure con mostruosa efficacia, il master può inviare ai giocatori una mappa o il ritratto di un mostro, magari ricavando una istantanea al volo da un manuale in edizione Pdf. Un giocatore il cui personaggio ha ricevuto una informazione ignota agli altri può ricevere un messaggio personale, cosa altrimenti impossibile a voce. Una app come Whatsapp, per non scomodare Skype o Aim, risolve la convivenza con eventuali apparecchi Android.

Sembreranno sciocchezze, fino a quando si trasferiscono per esempio in un meeting aziendale e le app che lanciano dadi o disegnano mappe diventano fogli di calcolo oppure generatori di diagrammi.

Non è una questione di iOS o di Apple, ma di rete. Un ambiente che voglia essere produttivo – un tavolo di gioco di ruolo, un ufficio, una classe – moltiplica la produttività quanto più i partecipanti dispongono di apparecchi capaci di generare, condividere, ricevere informazione. Quando mi presentavo ai meeting con Newton nel 1997 era sempre presente lo scettico che con una battuta magnificava i vantaggi del suo kit di taccuino più stilografica. Oggi inizia a sembrare un disadattato (e il suo taccuino è al macero, i miei appunti digitali sono tuttora leggibili dentro Newton).

