---
title: "Altro che cambiamenti"
date: 2011-06-08
draft: false
toc: false
comments: true
categories: [ ]
tags: [script]
---
Lo so, lo si ripete alla nausea e il concetto viene abusato da chiunque, prima fra tutti i politici che è la cosa peggiore.

Però: Josh Quittner, uno dei pionieri del giornalismo digitale al prestigioso settimanale Time, dopo quindici anni se ne va per entrare nel social magazine Flipboard.

Ho detto Time, ho detto Flipboard. Per fare un paragone (inesatto) vagamente plausibile con personaggi che tutti conoscono, è come se Beppe Severgnini lasciasse il Corriere della Sera per andare a Lavoce.info.

In altro ambito: pensando al valore di mercato (quotazione delle azioni moltiplicato numero delle azioni), nel 1998 la piattaforma Wintel – Microsoft più Intel – valeva novantasei volte Apple, 339 miliardi di dollari contro 3,5 miliardi di dollari, dice Wolfram Alpha.

Bill Gates commentò così il ritorno di Steve Jobs in Apple: Ciò che non riesco a capire è perché ci stia perfino provando. Sa che non può vincere.

Tredici anni dopo, secondo Wolfram Alpha, il valore di mercato di Apple ha superato quello di Microsoft e Intel messe insieme. Partiva da un novantaseiesimo.

La frase abusata è che il mondo sta cambiando. Ma non sta cambiando come è cambiato, che so, dagli anni ottanta agli anni novanta. Sta cambiando in modo complicato, enorme, radicale. Chi ripete gli stessi ragionamenti che faceva vent’anni fa si rende ridicolo e occorre guardarsi allo specchio, leggere un po’ di giornali – non solo su carta – per ripensare.

