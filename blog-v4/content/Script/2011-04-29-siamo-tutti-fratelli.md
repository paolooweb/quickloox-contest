---
title: "Siamo tutti fratelli"
date: 2011-04-29
draft: false
toc: false
comments: true
categories: [ ]
tags: [script]
---
HunchBlog ha pubblicato un divertente e interessante profilo demografico di quanti arrivano su Hunch e, tra varie altre domande, si dichiarano persone Mac o persone Pc.

Il campione non è giudicabile per equilibrio; certamente, basato su quasi quattrocentomila utenti unici, suggerisce riflessioni gustose.

Qualche esempio a caso: la persona Mac ha più probabilità di possedere istruzione superiore rispetto a un Pc e maggiore probabilità di essere un frequentatore di feste tra amici, nonché di essere vegetariano.

Per fortuna viviamo un’epoca nella quale c’è una libertà maggiore di prima nello scegliersi hardware e software e possiamo curiosare in dati come questi alla ricerca dei gusti sul vino o sul migliore fast food, senza altri pensieri che la curiosità divertita.

Tuttavia voglio ricordare con forza che in un tempo recente veniva richiesta la giustificazione in ufficio, o peggio, per l’uso di un Mac, inesplicabile per quanti avevano già tirato le conclusioni: la storia dell’informatica era finita, Windows era lo standard, Office un obbligo, fine.

E invece quante sorprese, ragazzi.

