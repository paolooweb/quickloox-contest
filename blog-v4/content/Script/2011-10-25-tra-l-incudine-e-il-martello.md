---
title: "Tra l'incudine e il martello"
date: 2011-10-25
draft: false
toc: false
comments: true
categories: [ ]
tags: [script]
---
Fino al 3 ottobre non facevo che consigliare TypeKit, l’ottimo servizio di fornitura di veri font professionali per siti Internet.

Dal 3 ottobre TypeKit è stata acquista da Adobe e non faccio altro che arrovellarmi nel dubbio, se sia ancora un servizio da consigliare o se piuttosto non sia meglio incrociare le dita.

Nel dubbio, segnalo che il sito ha ristrutturato molto bene l’interfaccia di ricerca ed esame dei font. Ma è presto per capire se sia primavera o il canto del cigno.

