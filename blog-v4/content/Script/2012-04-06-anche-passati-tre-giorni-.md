---
title: "Anche passati tre giorni "
date: 2012-04-06
draft: false
toc: false
comments: true
categories: [ ]
tags: [script]
---
Devo a Francesco la segnalazione di April Fools’ Day On The Web, la rassegna di tutti gli scherzi da Internet dello scorso primo di aprile.

Apple è indirettamente presente solo nel resoconto del suo brevettare il rettangolo da parte di The Register.

Ho apprezzato comunque le mappe di Google a otto bit, l’ultrabook più piccolo del mondo concepito da Sony e l’arrivo dei voli spaziali low cost, 50 dollari per la sola andata, con ossigeno da pagare a parte.

La pagina ne comprende decine di altri e complessivamente è imperdibile. Né puzza.

E in Italia? Emaskew mi ha segnalato il bellissimo pesce d’aprile di The Apple Lounge… e soprattutto come è stato trangugiato da testate ufficiali e ufficiose, sempre alla ricerca di una news con il punto interrogativo da sparare senza nessuna verifica, capaci di ignorare tranquillamente la fonte dalla quale copiano e incollano a costo zero e intelligenza a seguire.

