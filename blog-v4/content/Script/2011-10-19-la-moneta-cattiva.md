---
title: "La moneta cattiva"
date: 2011-10-19
draft: false
toc: false
comments: true
categories: [ ]
tags: [script]
---
Vivo giorni piuttosto densi (chi sta ricevendo risposte in email con ritardo consistente se ne è accorto da sé) e non sono ancora riuscito a sentire come si deve Fabio, che è anche Zambelli ma per me Fabio è sufficiente, rispetto alla chiusura dichiarata di SetteB.it.

Fabio è stato il creatore e l’anima del sito, praticamente l’unico in Italia a informare in ambito Mac con dirittura morale e senza la faccia tosta indispensabile per sbattere qualunque cretinata in prima pagina, purché gli scemi abbocchino e si venda.

Fino a controannuncio, l’ultimo atto di SetteB.it è questa spettacolosa carrellata di immagini e contenuti sulla vita di Steve Jobs.

Spero che Fabio ci voglia ripensare. Contemporaneamente credo di capire le sue motivazioni, prima ancora di chiedergliele di persona.

Allo stesso tempo, i siti di informazione Mac dovrebbero chiudersi a partire dal peggiore, non a rovescio. È triste che la moneta cattiva finisca per scacciare la buona.

Nel frattempo il meccanismo infernale procede senza sosta. Dopo che per mesi siamo stati inondati di spazzatura su iPhone 5 e il suo design, per vedere l’ottimo iPhone 4S, ecco che un analista torna a dire che iPhone 5 arriverà e avrà il design più sottile e lo schermo più grande. Le stesse cose che si dicevano sei mesi fa, solo spostate avanti di un anno visto che stavolta è andata buca. Di più, perché se giochi al raddoppio la devi sparare ancora più grossa: iPhone 5 sarebbe l’ultimo progetto in cui Steve Jobs è stato intimamente coinvolto dal concept al design finale. Affermazione, oltre che di cattivo gusto, del tutto indimostrabile.

Si noti che potrebbe arrivarci chiunque: il prossimo iPhone si chiamerà iPhone 5, siccome la tecnologia procede sarà più sottile e più ampio, Jobs era vivo e quindi ci ha potuto lavorare. Non c’è bisogno di millantare appoggi di chissà che genere per scrivere questa roba. Rilancio: avrà un gigabyte di Ram. Che ci vuole? iPhone 4S ha 512 megabyte, quindi alzo il valore di una tacca. Olé, ho prodotto un rumor.

I soliti buffoni si sono già buttati a pesce sulla “notizia”.

Da ricordare: i rumor sono sempre veri. È Apple che non li rispetta. Quell’azienda non cambia mai, piena di round pegs in square holes, quei pioli rotondi per buchi quadrati che non sono mai entrati nel testo italiano della campagna Think Different.

