---
title: "Rischio povertà"
date: 2012-10-16
draft: false
toc: false
comments: true
categories: [ ]
tags: [script]
---
Pensiamo a uno smartphone totalmente identico a iPhone, ma costruito in plastica, con doppia Sim e alloggiamento per schede Sd, con una antenna per vedere la televisione e una vecchia versione di sistema operativo Android. Costo, centotrenta-duecento dollari secondo il modello.

Se si lascia cadere la notizia nella giusta pausa pranzo o nel bar appropriato, facile che distinti professionisti si mettano in fila a mucchi per saperne di più, in particolare il punto vendita.

L’oggetto si può vedere in questo articolo di Fast Company.

E il commento di Ricardo Lemos, direttore del Centro per la tecnologia e la società alla scuola di legge Fgv di Rio de Janeiro, è il seguente:

Questi apparecchi in realtà sono progettati per persone in situazione di povertà.

La doppia o tripla Sim serve perché spesso manca l’interconnessione tra operatori, il televisore in quanto non ci si può permettere un apparecchio.

Cercare la convenienza è doveroso, sempre. Poi però bisognerebbe sapere quello che si vuole e, se serve, magari anche risparmiare fino ad arrivarci. Altrimenti la corsa verso il basso diventa rapidamente fatto sociale dopo che individuale. E il punto non è che abbia uessebì o ladoppiasim, ma che migliori la vita, il lavoro, il divertimento. Altrimenti, la decisione più costosa di tutte è non per niente quella che conferisce la libertà totale: farne a meno invece di accontentarsi.

