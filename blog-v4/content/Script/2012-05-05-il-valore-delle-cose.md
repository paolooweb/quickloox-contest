---
title: "Il valore delle cose"
date: 2012-05-05
draft: false
toc: false
comments: true
categories: [ ]
tags: [script]
---
Ieri si parlava di iPhone per chi non vede (o forse vede più lontano degli altri, perché è il cervello che lavora). Il tema ne sottintendeva uno più importante, il valore delle cose.

Se capisci che una cosa vale, sei disposto a corrisponderle il prezzo che merita. Il che spiega abbondantemente un paradosso apparente di questi tempi, il successo di Apple dei suoi apparecchi percepiti come costosi in tempi di crisi e contrazione della spesa.

Si scrive all’ingrosso che la gente (meglio, la gggente) non abbia soldi da spendere ed è una grossa bugia. Non siamo ridotti così male e non credo che arriveremo a tanto.

La gente, in verità, non ha soldi da sprecare.

In tempi favorevoli, il cittadino medio spende per sentirsi contento. Entra in negozio, guarda la cosa che gli piace o salta all’occhio e porta via senza pensieri. Se non funziona, che sarà mai? vola nel cestino e si esce di nuovo a prendere qualcos’altro. Nulla impedisce che a saltare all’occhio possa essere una ciofeca a basso costo e scarso gusto.

In tempi meno favorevoli, il cittadino medio spende per sentirsi coperto. E ha una sola cartuccia. Chiede, tentenna, prova, perché se arriva in casa la ciofeca poi la si deve pure usare e non è piacevole, specie alla lunga.

Se hai una sola cartuccia, la spari contro un bersaglio che abbia valore.

Le aziende valutano il valore delle cose in funzione del guadagno che portano. Una cosa che si vende a carriolate ma porta guadagno poco o nullo ha poco valore.

Il bravo Horace Dediu ha riassunto in un tweet l’andamento recente del mercato dei computer da tasca, misurato in base ai guadagni generati da tutto il comparto. Apple ha guadagnato tre quarti della torta, Samsung quasi un quarto, Htc un centesimo del totale.

Tutti gli altri hanno guadagnato zero o hanno perso soldi.

Dunque è facilissimo distinguere tra le cose che hanno valore e le ciofeche.

