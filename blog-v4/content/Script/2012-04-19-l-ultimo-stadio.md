---
title: "L'Ultimo Stadio"
date: 2012-04-19
draft: false
toc: false
comments: true
categories: [ ]
tags: [script]
---
Mi sono già adombrato un paio di volte per il triste destino di Nokia, da leader tecnologico europeo a zombi telecomandato da Microsoft.

E sta andando sempre peggio: secondo Reuters, quattro grandi provider telefonici europei ritengono che i nuovi Lumia non siano buoni a sufficienza per competere con gli iPhone o i Galaxy di Samsung.

Si possono nutrire dubbi su Windows Phone o ricoprirlo di elogi. Il software. Ma se è l’hardware a non essere competitivo, che cosa resta di Nokia?

Qualcuno per favore aiuti quest’azienda. Che anche se domani raggiungesse il quindici percento – a suon di marketing e svendite si può fare molto, finché dura – è all’ultimo stadio.

