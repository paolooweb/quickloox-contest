---
title: "Melma di colonia"
date: 2012-04-27
draft: false
toc: false
comments: true
categories: [ ]
tags: [script]
---
George Colony, il Counterintuitive Ceo di Forrester, ha elargito una perla di saggezza: Apple declinerà durante il dopo-Jobs.

Questo perché secondo un libro del 1947 di Max Weber (linko l’inglese perché non riesco a individuare una edizione italiana corrispondente e chiedo il gentile aiuto dei filosofi e sociologi che mi seguono) esistono organizzazioni burocratiche (Ibm), tradizionali (la Chiesa cattolica) e carismatiche, dirette da individui speciali. Siccome l’individuo speciale era Jobs ed è scomparso, Apple andrà avanti per inerzia per 24-48 mesi e poi passerà da grande azienda a buona azienda, con un passo indietro corrispondente in fatturato e innovazione, come Sony dopo Akio Morita, la stessa Apple nel 1985 dopo Steve Jobs, Polaroid dopo Edwin Land e Disney nei primi venti anni dopo Walt Disney.

Ora sarebbe troppo facile notare che Weber è morto nel 1920 e quindi il libro è stato edito nel 1947, ma certamente scritto molto tempo prima, quando realtà come le multinazionali ed economie come quella globale erano fuori da ogni concepibile pensiero di Weber e di qualsiasi suo collega. Altrettanto facile rilevare che dire da due a quattro anni è dire sparo a pallini in modo che prima o poi centrerò un bersaglio qualunque cosa succeda e non sembra esattamente una previsione ficcante come un raggio laser che misura la distanza tra Terra e Luna al decimo di millimetro. E che dire di una profezia secondo cui la prima azienda al mondo, da qui a un lustro più o meno quasi circa, smetterà di essere la prima azienda al mondo? Pazzesco, vero? Come se non fosse accaduto a tutte le prime aziende al mondo dall’inizio della storia dell’uomo e delle aziende stesse.

Facciamo la cosa difficile invece e veniamo al punto.

Intanto il paragone con la Apple del 1985 non si tiene proprio, per un buon motivo: Jobs non comandava affatto. Non era amministratore delegato, si era preso una divisione di prodotto (Macintosh) apposta per fare quello che voleva perché non gli lasciavano fare ciò che stava facendo. Altro che carismatico.

Sony è in declino. Ma chiunque cercasse una coerenza nella sua azione si troverebbe in difficoltà. Vende televisori, realizza console di videogiochi, produce cinema, pubblica musica, commercializza impianti stereo, autoradio, macchine fotografiche, cellulari e chissà che altro. Se dovessi fare un paragone lo farei tra Sony e, boh, Philips, mica con Apple. Apple vende apparecchi elettronici per l’elaborazione individuale. Una frase e ho preso il 90 percento del fatturato dell’azienda. Qual è la frase che descrive il 90 percento del fatturato Sony? Elettronica di consumo? E come la mettiamo con i servizi finanziari?

Polaroid, sì. Ma Polaroid faceva una cosa sola. Apple ha tolto la dizione Computer dalla ragione sociale nel 2005. E Disney, beh, oggi Disney è leader incontrastata nel proprio settore. I rivali, Dreamworks per non fare nomi, giocano di rincorsa. Non ho lo spazio per una analisi approfondita, ma ho l’impressione che se Apple diventasse la Disney del proprio settore sarebbe ancora più grossa di oggi e gli altri ancora più piccoli.

Larry Dignan ha scritto una controanalisi degli argomenti di Colony focalizzata su Sony, trovando differenze sostanziali tra le due realtà. Cito solo l’ultima: nei mercati dove si muove Apple, lo spazio a sua disposizione è immenso. Nei mercati dove si muove Sony, c’è la congestione.

Alla fine, quella di Colony pare la tirata al bar contro la Juventus della situazione, che prima o poi smetterà pure di vincere. Come accade in tutte le cose umane. Ma per affermare una cosa ovvia non c’è bisogno di resuscitare Max Weber. E si deve anche essere un po’ più precisi di prima o poi, altrimenti si fa la figura dei lanciatori di melma.

