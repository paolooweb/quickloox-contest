---
title: "Il buonsenso resta, in compenso, introvabile"
date: 2011-04-28
draft: false
toc: false
comments: true
categories: [ ]
tags: [script]
---
Non capisco perché i dibattiti sulle abitudini di Apple rispetto alla privacy siano tanto più accesi quanto più il soggetto è inconsistente. Adesso è il turno di iPhone che, anatema, conserva un archivio dei luoghi visitati. Luoghi intesi come latitudine e longitudine, non come il tavolo tre della pizzeria di fronte al macellaio in piazza.

Niente dibattito senza inconsistenza, si ricordi. Difatti Apple ha spiegato tutta la situazione nientemeno che al Congresso degli Stati Uniti, mesi e mesi fa. La raccolta dei dati serve ad arricchire il database interno di torri cellulari e basi Wi-Fi. Se Apple fosse alla ricerca di volontari disposti a contribuire attivamente, sarei pronto ora.

Inoltre tutti i dettagli di come funziona il sistema sono stati descritti durante una manifestazione pubblica, per quanto molto tecnica, e chi lo sfodera ora ha una concezione tutta personale della tempestività dell’informazione. Insomma, questo argomento è fuori luogo da mesi.

Oltretutto nel Paese dove qualunque mia spesa medica è a conoscenza della commercialista e le autostrade hanno il casello.

