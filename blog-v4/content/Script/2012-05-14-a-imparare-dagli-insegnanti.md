---
title: "A imparare dagli insegnanti"
date: 2012-05-14
draft: false
toc: false
comments: true
categories: [ ]
tags: [script]
---
Oggi passo la giornata in quel di Borgo Valsugana, provincia di Trento, a chiacchierare con un nutrito gruppo di insegnanti dell’Alcide Degasperi di computer-tavoletta e del loro uso nella scuola.

Sarà una occasione preziosa perché si tratta di gente sveglia e avrò tra breve un sacco da imparare. Che è anche il loro compito, nel XXI secolo.

Ho anche ottenuto da Apple un nuovo iPad da mostrare per l’occasione, che dovrò restituire domani. Un quarto d’ora dopo averlo attivato, il confronto tra lo schermo e quello del mio iPad prima generazione è veramente impari. C’è una differenza enorme in termini di processore, ma non dà alcun peso particolare; a volte il mio iPad reagisce con una esitazione dove invece il nuovo iPad scatta come un missile. Finisce qui e non ho un particolare bisogno di avere un missile.

Ma lo schermo, lo schermo è un salto di qualità epocale. La lettura cambia da così a così. Non prevedo a breve di sostituire il mio iPad, ma se ci sono tre ragioni per farlo sono lo schermo, lo schermo e lo schermo. iPad 2 a prezzo ribassato ha un suo perché, del tutto diverso però.

Ma divago. Nel menu di oggi: inquadramento della tavoletta, perché deve sostituire la carta a scuola e che cosa la rende insostituibile – sempre a scuola – rispetto al computer. Con contorno di app e attività mirate segnatamente alla didattica.

Farò sapere. Intanto, l’ospitalità del Borgo è a livello di nuovo iPad. Il massimo.

