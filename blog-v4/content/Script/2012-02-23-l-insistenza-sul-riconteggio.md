---
title: "L'Insistenza Sul Riconteggio"
date: 2012-02-23
draft: false
toc: false
comments: true
categories: [ ]
tags: [script]
---
C’è App Store che sta raggiungendo i 25 miliardi di download e ha organizzato l’ormai consueta estrazione a premi per chi simbolicamente scaricherà la venticinquemiliardesima app.

Questo equivale a dire che App Store sia una cosa buona? Ovviamente no. La massa in quanto tale non significa niente.

Tuttavia la massa è composta da individui. Dentro quei venticinque miliardi di app c’è, se non altro per statistica, qualcuno intelligente, geniale, acuto, sopra la media.

Ecco. Vorrei spiegato da certi critici di App Store – che è una minaccia alla libertà, è chiuso, è vincolato all’approvazione di Apple – come mai ci sono persone intelligenti che scaricano app da lì.

