---
title: "Chiacchieriamo senza permesso (forse)"
date: 2011-10-04
draft: false
toc: false
comments: true
categories: [ ]
tags: [script]
---
Mi ha sollecitato Riccardo e allora stasera, anche in assenza di diffusione dell’evento da parte di Apple, chiacchiera libera dalle 19 sul canale Irc #freesmug, server freenode.net.

Ordine del giorno, il primo evento Apple pilotato da Tim Cook e naturalmente i nuovi annunci in tema iPhone, iOS 5 e iCloud.

Se ci sono problemi, sono in iChat nella stanza esse5 e da lì posso dare una mano a chi avesse problemi. esse5 non è il luogo della chat e serve solo per chiedere aiuto e dare una mano. Ignorerò gentilmente e fermamente ogni altra sollecitazione.

Rapidissimo: ecco i comandi Irc utili una volta collegati. Il comando per entrare in chat, una volta usato il programma Irc per collegarsi a irc.freenode.net, è /join #freesmug.

Programmi Irc per Mac: X-Chat Aqua, Colloquy, Conversation, JIrcii, simple irc…, Adium e altri.

App Irc per iPhone e app Irc per iPad.

Chi c’è… a dopo e un grande grazie a Gand per la graditissima ospitalità! A parte il fatto che non l’ho ancora trovato per chiedergli il permesso. Alla peggio ci butta tutti fuori, stile festa del liceo.

