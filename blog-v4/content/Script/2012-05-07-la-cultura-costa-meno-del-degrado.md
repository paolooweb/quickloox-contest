---
title: "La cultura costa (meno del degrado)"
date: 2012-05-07
draft: false
toc: false
comments: true
categories: [ ]
tags: [script]
---
La cultura, quella vera, si compone di tradizioni, figure storiche e mitiche, vite vissute, eventi che hanno segnato la crescita di una comunità.

Oramai si è consolidata una cultura digitale e chiunque abbia usato a fondo un computer da prima del secolo nuovo c’è dentro fino al collo.

Per questo oso suggerire un acquisto esagerato, di ben 49 dollari spese di spedizione comprese: Get Lamp, un documentario sulle avventure interattive che salirono alla ribalta negli anni ottanta per poi rapidamente declinare.

Un documentario vero, su Dvd. L’unica cosa è che sono presenti due versioni, una convenzionale e l’altra dove, in omaggio al tema, è possibile in parti prestabilite della narrazione decidere di seguire un binario oppure un altro.

Sarebbe troppo pretendere che Get Lamp porti alla riscoperta delle avventure Infocom, anche se progetti come Frotz su iOS, ScummVm e Zoom su Mac sono più che accessibili.

Mi accontenterei se anche solo uno o due trasferissero su Lumen et Umbra oppure Neonecronomicon la metà del tempo attualmente allocato su Facebook. Cultura è dove si coltiva, non dove si spruzza. E sono esperienze altrettanto gratuite.

