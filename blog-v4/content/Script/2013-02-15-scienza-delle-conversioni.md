---
title: "Scienza delle conversioni"
date: 2013-02-15
draft: false
toc: false
comments: true
categories: [ ]
tags: [script]
---
Si vendono più apparecchi Android che iOS, molti di più. In compenso iOS va su Internet più di Android, molto di più.

Le spiegazioni del perché tanti Android riescano solo a guardare da dietro e da lontano iOS si sprecano e abbondano in fantasia: chi li usa farebbe più posta elettronica che web, oppure ha meno da spendere e quindi va meno in rete e così via. Finalmente abbiamo un esperimento interessante.

Preso un popolare negozio online di abbigliamento, i progettisti di Electric Pulp hanno riprogettato il sito secondo le regole del responsive design, design reattivo: il sito si ridisegna da solo secondo quanto è grande la finestra e questo lo aiuta a funzionare al meglio su qualsiasi apparecchio, da un computer desktop con megaschermo a un telefono intelligente.

Prima di attuare tutto ciò sono stati raccolti i dati di vendita, piattaforma e conversione: quanto il sito vende, che apparecchio viene usato per comprare e soprattutto che percentuale di visitatori finisce per comprare qualcosa invece che andarsene a mani vuote.

Le stesse statistiche sono state raccolte dopo la riprogettazione. iOS ha aumentato le conversioni (visita con acquisto) del 66 percento, gli acquisti del 112 percento (più che raddoppiati), il venduto del 101 percento. Un successo.

Android ha fatto numeri dell’altro mondo: conversioni cresciute del 407 percento (quintuplicate!), acquisti in aumento del 333 percento (più che quadruplicati!), venduto superiore del 591 percento (quasi sette volte più di prima).

L’esperimento sembra dimostrare che Android, su siti ben costruiti, viaggia alla grande. Pare anche evidenziare che ci sia utenza Android con soldi da spendere.

Se tutto quadra, Android fallisce quando un sito è fatto male per gli schermi piccoli o per schermi touch.

Questo significa che, se un sito è fatto male, iPhone e iPad se la cavano ugualmente; infatti Internet è popolata da siti fatti male e iOS fa molto più traffico pur essendo in minoranza. Mentre Android si arrende, o meglio si arrende il suo pilota.

Tradotto: iPhone e iPad ti fanno andare meglio su Internet perché offrono strumenti superiori che compensano i difetti dei siti.

I soldi da spendere, la posta elettronica, le fasce demografiche, il costo degli apparecchi c’entrano niente.

Tutto questo potrebbe essere anche sbagliatissimo, alla stregua di qualunque esperimento condotto con criteri scientifici. Si prega di contestarlo sulla base di altri esperimenti, secondo il metodo scientifico.

