---
title: "Chi più vende"
date: 2011-06-14
draft: false
toc: false
comments: true
categories: [ ]
tags: [script]
---
Mentre scrivo, la pagina di ingresso di Apple.com strilla iCloud.

Le manchette inferiori, invece che le solite quattro, sono due: una per iOS 5 e una per Mac OS X Lion.

Nessuno di questi prodotti è attualmente in vendita.

Il prezzo complessivo di questi tre prodotti, due sistemi operativi e una infrastruttura di sincronizzazione multipiattaforma, è di 23,99 euro.

Apple incassa decine di miliardi di dollari ogni anno. Grazie a prodotti che oggi non appaiono sulla pagina principale del suo sito.

Una lezione di marketing impressionante.

