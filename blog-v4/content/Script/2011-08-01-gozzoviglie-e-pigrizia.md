---
title: "Gozzoviglie e pigrizia"
date: 2011-08-01
draft: false
toc: false
comments: true
categories: [ ]
tags: [script]
---
È agosto e come da (più o meno) tradizione vorrei passare un mese a parlare più del normale di giochi e magari (se riesco) di automazione di Mac, da AppleScript in avanti. Oltre che di argomenti a richiesta: accetto le suddette.

A partire da questa rassegna dei migliori trenta giochi per iPad compilata da Cnet.

Fossi io la riscriverei per metà, secondo l’amore per certi generi e il distacco verso altri. In realtà è una buona panoramica dei giochi, ma anche dei generi di gioco da aspettarsi e da cercare su iPad. Si può non condividere, ma va sfogliata. Tra l’altro la conoscenza dell’inglese, se non interessano i giudizi comunque soggettivi dei redattori, è del tutto secondaria. Al limite sono disponibile per traduzioni.

Il mio consiglio è cercare per questa estate giochi che abbiano una modalità hot seat: ci si passa l’iPad di mano in mano e ogni giocatore, quando lo ha in mano, completa il proprio turno. Un raduno estemporaneo sotto l’ombrellone, una pizza con numero congruo di amici, una serata in famiglia o in compagnia lontano dalla solita televisione acquisteranno un sapore particolare. Il costo, diciamocelo di fronte a quello del noleggio di un ombrellone o di un pranzo in trattoria sul lago, è veramente irrisorio.

Non me lo aspettavo, ma Cnet mi ha suggerito un paio di titoli interessanti, di cui non avevo idea alcuna e che mi intrigano. Giro la domanda: che sia per Mac, per iPhone/iPod touch o iPad non importa: ma c’è qualche gioco che è stata una scoperta personale del 2011? La parte cronologica è importante, perché di mio sto tuttora giocando ad Angband e lo faccio oramai da oltre un secolo…

