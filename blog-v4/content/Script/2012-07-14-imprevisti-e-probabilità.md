---
title: "Imprevisti e probabilità"
date: 2012-07-14
draft: false
toc: false
comments: true
categories: [ ]
tags: [script]
---
Se c’è un caso in cui la cultura dominante di basta avere il programma e lo so fare provoca danni potenzialmente incalcolabili, è quando diventa basta scegliere la password e sono al sicuro.

Chiunque abbia un impiego nelle assicurazioni mi insegnerà che la sicurezza non esiste e che invece esistono livelli di sicurezza, così come di aspettative di vita e di tranquillità.

La riprova? L’ultima notizia è il furto di quasi mezzo milione di identificativi con password a Yahoo. E poi rubano un milione di password su un forum Android.

Subito arriva l’esperto a spiegare i modi comuni in cui avvengono i furti di identità.

Guardo la prima voce: attacchi dizionario. Provo tutte le “parole” tipicamente usate dalla gente e prima o poi trovo quella giusta.

Qualcuno pensa che Fabo09wfDH!# sia una password vulnerabile a un attacco dizionario? No. Eppure è stata rubata.

La terza voce è password semplici. Se usi una password come 000000 oppure password oppure pippo, te la vai a cercare.

In parte è vero. Tuttavia shakthishivamganesh3 a me non sembra una cattivissima password. Eppure è stata rubata.

Controprova: sono il peggiore utente Yahoo di tutti i tempi. La mia password è sempre quella dagli anni novanta, lunga solo sei caratteri, banalissima. Non è stata rubata.

Perché non la cambio periodicamente? Perché non ne adotto una seria? Perché su Yahoo faccio solo stupidaggini. Se domani mi rubassero la password, sarebbe poco male.

Se nello stesso attacco sono state rubate insieme mmmmmmmm e 22march200622march20, tutti e due hanno lo stesso problema, ma uno deve sorbirsi una password micidiale tutti i giorni e non è detto che serva.

Non c’è una soluzione semplice. Ma bisogna cominciare a pensare in questo modo: se un servizio ha cento milioni di utenti, che probabilità ho che becchino proprio la mia password? Dipende da quanto è buona la password e poi anche da quanto è bravo il servizio a difendersi, che tipo di attacchi potrebbero essere portati, quanto mi conviene cambiarla spesso, per che cosa la sicurezza dell’account è critica e così via. Così facendo, se rubano mezzo milione di password a Yahoo e l’azienda riferisce che solo il cinque percento delle password rubate è attivo, la probabilità che abbiamo preso la mia password è incredibilmente bassa. Siccome su Yahoo non faccio niente di importante e siccome ci sono attacchi che potrebbero rivelare tanto una password facile che una difficile, lascio lì una password vecchia di secoli che almeno ricordo automaticamente.

Mentre su Gmail, per dire, le mie abitudini di password sono piuttosto diverse. E l’immagine disco cifrata su cui tengo i miei dati confidenziali su Mac richiede qualche migliaio di anni di tentativi come minimo.

Più importante essere svegli: di fronte a un sito che mi chiede la mail Yahoo per dirmi se mi hanno rubato la password, mi tengo il piccolo dubbio piuttosto che avere la quasi certezza di guadagnare posta indesiderata da chissà chi. Piuttosto scrivimi, ho l’elenco (come del resto mezza Internet) e ti tolgo il dubbio con una mail sola.

