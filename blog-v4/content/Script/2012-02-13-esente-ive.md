---
title: "Esente ive"
date: 2012-02-13
draft: false
toc: false
comments: true
categories: [ ]
tags: [script]
---
Jonathan Ive è l’attuale responsabile del design di Apple, cui si deve il design di tutta la linea di prodotti dalla fine del secolo scorso a oggi.

Seven Days ha colmato una lacuna notevole con un eccellente articolo su Jerry Manock, designer di Apple II nel 1977 e successivamente presente nel team di progettisti di Macintosh. A momenti Ive non era neanche nato (ok, aveva dieci anni, si fa per dire).

Il racconto è quello di una vita che in termini informatici è stata ovviamente di altri tempi, fatto di ricordi dell’epoca. Steve Jobs che poneva domande a ciascun progettista e riprendeva sempre il discorso da dove lo aveva lasciato con ciascuno, oppure la sua idiosincrasia verso i focus group: basano la conoscenza su quello che esiste già e non sono in grado di dire che cosa esisterà tra cinque anni.

L’articolo è impreziosito dalle foto del contratto originale di consulenza con il quale Manock accettò per 1.800 dollari di preparare i piani di progetto di Apple II.

Più tardi, a computer già in vendita, Manock avvicinò Jobs per chiedergli una royalty di un dollaro su ciascun Apple II in vendita, dato che la macchina aveva un gran successo.

Jobs gli rispose sei molto bravo. Ma se tu sapessi quanti pensiamo di venderne nei prossimi due o tre anni… non sei così bravo.

Manock ha commesso anche qualche errore, come quello di pensare troppo tardi a farsi assumere da Apple invece che rivendicare la propria autonomia come consulente. Avrebbe potuto essere il dipendente numero sei e diventare multimilionario al momento della quotazione di Apple. Invece insistette per restare consulente a tempo parziale (e lavorare cinquanta ore a settimana: nel team Macintosh girava una t-shirt con la scritta 90 ore a settimana e mi piace).

In compenso scelse un beige per il telaio, Pantone 453, che è passato alla storia. Non erano ancora i tempi di Ive e dell’alluminio.

Nell’articolo c’è molto più di questo. Va letto.

