---
title: "Il collezionista di spot"
date: 2011-05-10
draft: false
toc: false
comments: true
categories: [ ]
tags: [script]
---
Grande lavoro di AdWeek, che ha raccolto in poche pagine di facile consultazione l’intera raccolta degli spot Get a Mac, messa in onda da Apple tra maggio 2006 e ottobre 2009.

Il risultato è perfetto come archivio remoto (a che serve scaricare ossessivamente sul proprio hard disk, quando c’è Internet?) e per chi si trova a dover superare l’ostacolo dell’inglese c’è una piccola opportunità in più. Ogni spot viene infatti riassunto in una manciata di righe, che con l’aiuto del Traduttore di Google possono avvicinarsi all’italiano, in ogni caso più agevoli da comprendere dell’originale.

