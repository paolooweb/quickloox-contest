---
title: "Un nuovo livello"
date: 2012-03-17
draft: false
toc: false
comments: true
categories: [ ]
tags: [script]
---
Anche stavolta, letta la recensione del nuovo iPad 3 da parte di John Gruber su Daring Fireball, il resto è solo inutile rumore di fondo.

La suggerisco anche a costo di passare dal traduttore di Google. Se c’è bisogno di aiuto su qualche parte, sono a disposizione. Seguono alcuni estratti che trovo particolarmente degni di nota. Soprattutto quando sono tratti da recensioni passate, come quella di iPhone 4:

Apple sembra molto convinta dell’aspetto e delle dimensioni dello schermo di iPhone: 3,5 pollici, in rapporto 3:2. Non tre pollici. Non quattro pollici. In effetti, Apple sembra molto convinta di quello che ha deciso per l’iPhone originale del 2007. Non ci sono nuovi pulsanti, o perfino pulsanti spostati. Lo schermo è emblematico di iPhone 4 come un intero, hardware e software: la stessa idea fondamentale dell’iPhone originale, ma resa più chiara. Non è realmente cambiato, ma migliorato, come lo stesso soggetto fotografato in modo più nitido.

Quasi si potrebbe chiudere qui, adattando il paragrafo a iPad. Dedicato a quelli che sono delusi perché si aspettavano chissà quale diavoleria mozzafiato, coltivati a rumor dai siti-fogna, che fino a quando non si chiama iPhone 5 non lo considerano perché il numero fa sentire alla moda.

Questa arriva invece dalla recensione del primo iPad, aprile 2010:

Chiunque pensi che Apple fabbrichi solo prodotti di alto prezzo ha completamente perso il senso della realtà. Lusso accessibile è la parola d’ordine per avere successo sul mercato e Apple continua a fare centro. La sola cosa che mi fa venir male al cuore rispetto a iPad è quando inizio a immaginare un ipotetico modello Pro; immaginarsi quello che Apple potrebbe mettere in un iPad che costi quanto un MacBook Pro. (Il mio sogno di iPad Pro: risoluzione doppia dello schermo e un gigabyte o due di Ram).

Ritorno alla recensione attuale:

23 mesi più tardi, ho in mano esattamente quello che il mio cuore smaniava per avere. Eccetto che pagare il prezzo di un MacBook Pro, ho dovuto semplicemente aspettare due anni per comprarlo allo stesso prezzo del primo. […] Il mio ipotetico iPad di sogno del 2010 è il normale iPad di oggi.

Batteria e velocità:

Lascerò i test accurati ad altri recensori ma, per quanto riscontrato finora, l’autonomia del nuovo iPad sembra indistinguibile da quella di iPad 2, perfino usando Lte [la nuova connessione cellulare che va oltre 3G]. Già questo da solo mi colpisce come un risultato ingegneristico da rimarcare.

Ciò è che è cambiato – e non è cambiato – in questa più recente iterazione di iPad rivela le priorità di Apple. Più importante: come le cose appaiono a video, la sensazione che danno, come si animano fluidamente. Non importante: un processore più veloce. Importante: elaborazione grafica più rapida. […] Abbastanza importante: prezzo. Rimane lo stesso di prima, ma con una opzione in più verso il basso, iPad 2 a 399 dollari. Non importante: maggiore capacità. Scommetto che gli iPad del prossimo anno passeranno da 16/32/64 a 32/64/128.

La gente che parla del processore e della capacità, e più in generale delle specifiche di prodotto rispetto a come funziona il prodotto quando lo hai nelle mani, parla in modo vecchio e superato. Da tenere a mente.

50 grammi e sei decimi di millimetro sono compromessi minori, ma compromessi, e disvelano le priorità di Apple: meglio fare iPad leggermente più spesso e pesante che sacrificare anche leggermente l’autonomia. Tenendo a mente che il nuovo iPad rimane di gran lunga più sottile e leggero del primo iPad.

La cosa più importante per me l’ho lasciata per ultima, diversamente da come l’ha impostata Gruber:

Lo schermo di iPad è così di qualità che mostra, come nessun altro apparecchio prima, quanto sia mediocre la gran parte delle immagini che si trova sul web.

iPad, diversamente da qualsiasi netbook o tavoletta Android, compie un salto in avanti e sollecita un web – contenuti – di maggiore qualità complessiva. Un nuovo livello nella fornitura di informazione digitale. Tanti siti di basso impegno appariranno più evidentemente tali. I siti di qualità emergeranno. Abbiamo tutti da guadagnarne anche se non compriamo il nuovo iPad (e parla chi ha scritto parte di questo testo sul primo modello, che al momento non ha necessità né intenzione di cambiare).

Questa discriminante mi piace: apparecchi che spingono il mondo digitale a migliorarsi contro altri che costano poco e lasciano tutto com’è. Altro che il confronto tra questo e quel processore, come se interessasse più qualcosa.

Gruber ha caricato una schermata a pieno formato dello schermo di blocco del nuovo iPad 3. Questo sì che è un termine di confronto adatto al 2012. Messa in altri termini, ecco una veloce rassegna degli schermi di vari apparecchi elettronici, ingranditi al microscopio circa 80x.

