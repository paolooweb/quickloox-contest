---
title: "Spesso, Bugie E Videotape"
date: 2011-10-07
draft: false
toc: false
comments: true
categories: [ ]
tags: [script]
---
Si biasimano a volte le aziende per mentire rispetto ai prodotti o smentire illazioni che poi si dimostrano vere.

Non è del tutto corretto. In un ambito di concorrenza e di mercato, è più che comprensibile che un’azienda tenga segrete informazioni che reputa tornerebbero indebitamente o meno utili ai concorrenti.

Ovviamente una cosa è mentire, poniamo, su difetti di costruzione di un prodotto. Questo è esecrabile. Mentire sui programmi di sviluppo futuri è più comprensibile.

Apple si è già trovata in situazioni come queste in passato. Steve Jobs dichiarò pubblicamente nel 2003 che Apple non stava lavorando a un tablet. Sappiamo invece che i progetti per iPad nacquero prima di quelli per iPhone, anche se iPhone arrivò in anticipo. Ed è solo un esempio.

Oppure Amazon. Il suo amministratore delegato e fondatore, Jeff Bezos, dichiarò un anno fa l’app più venduta per iPad due giorni fa era Angry Birds, un gioco dove si tirano uccelli contro maiali e questi scoppiano. Il numero uno su Kindle è Stieg Larsson. È un pubblico differente. Progettiamo per gente che vuole leggere.

Praticamente un anno dopo, settimana scorsa, Bezos mostrava orgoglioso Kindle Fire, un nuovo apparecchio pronto per fare funzionare Angry Birds.

Ci sta tutto. È marketing, è tenere nascoste le carte per giocare l’asso quando l’avversario meno se lo aspetta. Chiunque di noi gioca tenendo le carte nascoste, o almeno quelle migliori.

Microsoft non è immune. Giorni fa pareva che avesse deciso di fare uscire di produzione Zune, il suo lettore musicale di successo, diciamo, relativo. Giustamente un dirigente Microsoft tenne a fare sapere su Twitter che non era vero: la sparizione della pagina si doveva a un errore e nient’altro.

Dopo poco tempo, Microsoft ha scritto sulla pagina Zune del proprio sito che effettivamente i lettori Zune non sarebbero più stati prodotti, pur restando in piedi il supporto. Mi piacerebbe linkare la pagina, solo che è vuota: Microsoft l’ha cancellata. In compenso Wmpoweruser ne ha registrata una schermata.

Mi ero dimenticato una considerazione. Mentire a livello aziendale per favorire la nascita di un nuovo prodotto è comprensibile. Mentire per nascondere la fine di un vecchio prodotto, beh, ne vale tutta questa pena?

