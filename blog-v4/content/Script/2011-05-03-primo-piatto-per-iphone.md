---
title: "Primo piatto per iphone"
date: 2011-05-03
draft: false
toc: false
comments: true
categories: [ ]
tags: [script]
---
O forse più il dolcetto: finalmente gli amici di 2Spaghi si sono decisi e hanno realizzato l’app per la propria comunità di condivisione di buoni posti dove mangiare fuori.

Conosco personalmente da tempo immemore uno dei fondatori del sito e gli ho sempre augurato la massima fortuna, specie quando ha deciso di fare il grande passo e trasformare la passione serale di 2Spaghi in un vero impiego. La trasposizione in realtà dello stereotipo della buona idea che, grazie a Internet e tanto lavoro sudato, diventa business. E tutto italiano, con i nostri commercialisti, le nostre tasse, le nostre idiosincrasie di Paese alla deriva dove nonostante tutto continuiamo a vivere bene.

Anche perché mangiamo bene e 2Spaghi da questo punto di vista è una garanzia. A consigliare la app non si può sbagliare, perché è gratis. Al massimo si butta via e non si è perso niente.

La community merita e merita tanto più quanto ognuno fa la propria parte nel segnalare, documentare, commentare. Buon appetito.

