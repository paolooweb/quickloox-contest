---
title: "Vive la difference"
date: 2011-06-18
draft: false
toc: false
comments: true
categories: [ ]
tags: [script]
---
Dalla pagina Facebook di Htc Regno Unito:

Desire e l’aggiornamento a Gingerbread
[uno smartphone Htc e Android 2.3]

Le nostre squadre di ingegneri hanno lavorato duramente negli ultimi mesi per trovare un modo di portare Gingerbread su Htc Desire senza compromettere l’esperienza di utilizzo dell’interfaccia Htc Sense che tutti ci aspettiamo dai nostri telefoni. Tuttavia siamo spiacenti di annunciare che siamo costretti a riconoscere l’assenza di memoria sufficiente per avere contemporaneamente Gingerbread e un buon funzionamento di Htc Sense. Siamo sinceramente dispiaciuti per il disappunto che questa notizia causerà ad alcuni di voi.

Poche ore dopo, sulla stessa pagina:

Contrariamente a quanto abbiamo detto prima, porteremo Gingerbread su Htc Desire.

Poi magari la gente si chiede perché iPhone costa più di un modello Android. Probabilmente perché l’azienda che lo fabbrica non ti avvisa da Facebook che ha tanto provato, ma proprio il nuovo sistema non ci sta, per poi cambiare idea dopo avere buttato l’esca e avere visto che invece i tuoi clienti non te la fanno passare liscia.

