---
title: "Il monopolio della qualità"
date: 2012-05-09
draft: false
toc: false
comments: true
categories: [ ]
tags: [script]
---
Come si fa a coltivare l’avversione per tutti i monopoli e contemporaneamente prendere atto della stima di Chitika, per la quale iPad genera il 94,64 percento del traffico web generato da tavolette?

Fermo restando che i dati in arrivo da Chitika vanno presi sempre con il grano di sale, però un fondo di verità ci deve essere. Dunque?

Il punto è che gli altri monopoli, normalmente, soffocano la libertà di scelta. I sistemi operativi alternativi a Windows, per esempio, sono sostanzialmente due e uno richiede l’adozione di hardware specifico, l’altro capacità tecniche non a disposizione di chiunque. Se domani creo un nuovo sistema operativo e cerco di venderlo preinstallato sui PC al posto di Windows, beh, buona fortuna.

Si entri in un qualsiasi Media World. Le tavolette esposte sono manciate. Nel 2011 sono stati annunciati oltre cento modelli di tavoletta. La libertà di scelta non manca proprio. Forse un po’ più di qualità complessiva.

