---
title: "Una ricerca al giorno e impari l'arte"
date: 2011-06-12
draft: false
toc: false
comments: true
categories: [ ]
tags: [script]
---
Non sapevo che esistesse A Google A Day.

Imparare l’inglese e impratichirsi con Google, una volta al giorno. A me suona come due piccioni con una fava, se di proverbi non ne avessimo già maltrattati abbastanza.

