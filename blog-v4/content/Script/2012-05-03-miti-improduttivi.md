---
title: "Miti improduttivi"
date: 2012-05-03
draft: false
toc: false
comments: true
categories: [ ]
tags: [script]
---
Provare a leggere l’elenco delle app usate da Matt Gemmell (sviluppatore iOS e progettista di interfacce software) per la sua produttività personale facendo finta di non sapere che cosa sia un iPad. Anzi, mostrandolo a qualcuno che ignora l’esistenza di iPad.

Vedrà le schermate e dirà che è un computer fatto e finito.

Commenta Gemmell:

Il falso mito spesso propalato di iPad inadatto come strumento di produttività e creazione ha perso qualunque credibilità. Chiunque si trastulli ancora con questa scarpa vecchia sta elaborando in base a preconcetti sorprendentemente sbagliati, o sta trovando scuse per le proprie inadeguatezze.

Oltretutto l’elenco delle app è pure interessante, anche se confesso di preferire Instapaper e GoodReader.

Il ragazzo ha avuto anche l’idea on del tutto malsana di creare uno schermo di blocco di iPad che riporta i suoi dati, nel caso l’apparecchio si perdesse. Chi volesse fare lo stesso prendendo spunto dalla sua grafica, può scaricare un file Photoshop apposito.

