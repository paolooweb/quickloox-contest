---
title: "Dieci gigabyte in plus"
date: 2011-11-19
draft: false
toc: false
comments: true
categories: [ ]
tags: [script]
---
Ero l’ultimo a non sapere che esiste Minus: dieci gigabyte di spazio in rete.

Non c’è condivisione alla Dropbox, ma Minus facilita comunque la distribuzione di file anche di grandi dimensioni (fino a due gigabyte).

La procedura per avere un account è la più rapida che abbia visto. C’è una applicazione dedicata per tutti i sistemi operativi, compresi quelli da tasca.

Niente di che… ma sono dieci gigabyte, gratis.

