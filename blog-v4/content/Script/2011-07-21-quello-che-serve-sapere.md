---
title: "Quello che serve sapere"
date: 2011-07-21
draft: false
toc: false
comments: true
categories: [ ]
tags: [script]
---
Macity ha ancora una volta indovinato correttamente la data di lancio di Lion: il 12 o 13 luglio (senza escludere il 14 luglio e il 19 luglio), il 15 luglio e ieri, dopo che lo ha detto Apple, il 20 luglio.

Venendo alle cose utili, chi passa a Lion cambiando nel contempo Mac si ricordi di avere installato su Snow Leopard Migration Assistant Update.

