---
title: "Tracciare una linea"
date: 2011-07-21
draft: false
toc: false
comments: true
categories: [ ]
tags: [script]
---
Come mi ha segnalato Paolomac, un tizio diversamente tecnologico ha scritto su Repubblica un articolo simpaticamente demenziale su iCloud (è la nuvola di Steve Jobs o quella di Fantozzi?), andando in America per collaudare una funzione provvisoria di un prodotto che stato annunciato per l’autunno e, previa ampia confusione sulla differenza tra iCloud e iTunes Music o iTunes Music Match, concludendo – senza avere capito niente – che è una fregatura.

Nel frattempo sta riprendendo le pubblicazioni il glorioso Byte, come costola di Information Week. È quasi commovente rivedere il vecchio Jerry Pournelle ripubblicare i suoi commentari dalla Magione del Caos. C’era gente negli anni ottanta che faceva la posta alle copie di Byte nelle edicole evolute, che avevano anche le riviste straniere, quasi solo per quelle pagine.

Sentimentalismi a parte, anche su Byte un diversamente tecnologico, tale Demetrius Mandzych, ha scritto una patetica fanfaronata su Apple. L’argomento sarebbe la scarsa qualità dei prodotti dell’azienda e i fatti a sostegno sarebbero, tenersi forte… l’antennagate di un anno fa, dissoltosi in una bolla di sapone; il vetro di iPhone 4 che si rompe (!) per colpa di un uso normale (sic) dell’apparecchio; la videocamera di iPad 2, di bassa qualità (che è vero, ha commentato uno, ma l’articolo fa schifo).
A essere gentili, al massimo, poteva essere uno di quei pezzi che abbondano su Internet: fai arrabbiare un bel po’ di pubblico, così questi commentano e cliccano e gli ascolti salgono, facendo fruttare meglio la pubblicità.

La redazione di Byte è arrivata a ritrattarlo. Non volendo però dare l’impressione di voler coprire o nascondere il misfatto, hanno pubblicato le scuse sopra l’articolo, che è rimasto pubblicato, ma tutto in stile strikethrough, cioè barrato.

Hanno tracciato una linea su tutto l’articolo inadeguato agli standard del buon giornalismo e indirettamente una linea tra il giornalismo serio, critico se necessario, e le sparate incompetenti.

Sembra che la linea, a Repubblica, non sappiano che cos’è.
