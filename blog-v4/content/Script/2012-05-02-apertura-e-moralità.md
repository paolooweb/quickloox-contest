---
title: "Apertura e moralità"
date: 2012-05-02
draft: false
toc: false
comments: true
categories: [ ]
tags: [script]
---
Andrea mi ha indirizzato a un articolo eccezionale pubblicato su Joystiq, in sostanza una elegia della saga ludica di Ultima, definita la più importante di sempre.

Se anche non fosse, certamente ha anticipato tutta una serie di tendenze e di conquiste in tema di trama e di svolgimento, comprese soluzioni di programmazione inedite o portate per la prima volta al grande pubblico.

Per esempio Ultima è stato un esempio di gioco open-world, mondo aperto, dove – a parte i limiti tecnologici – gli spostamenti del giocatore non sono ostacolati in alcun modo. Ultima VI, in particolare, manteneva costante la prospettiva dell’intero gioco, nel caso di un combattimento in un tunnel sotterraneo così come nell’attraversamento di una città, là dove le edizioni precedenti cambiavano punto di vista a seconda della situazione, per combattere con una schermata in pianta, esplorare con la vista in soggettiva e via discorrendo. Era il 1990, ventidue anni fa, non l’altroieri.

Ultima ha giocato (nel senso buono) alla grande con la morality, l’applicazione di schemi di valori alla trama. Di solito ci sono i buoni e ci sono i cattivi e gli uni devono sconfiggere gli altri. In Ultima IV mancava il cattivo e scopo del gioco era diventare l’Avatar, capace di infondere nel mondo di Britannia le otto virtù fondamentali per salvarlo da una crisi dei valori.

Si parla anche di tecnologia – il clic su un personaggio per interagire è apparso lì per primo o tra i primi, al posto di menu e tastiera per tutti – e di narrazione, dove la trama di Ultima VII contiene uno dei colpi di scena più scioccanti nella storia dei videogiochi.

E poi Ultima Online è stato gioco di ruolo di massa su Internet ben prima di quelli moderni, primo a offrire grafica di un certo significato e interazione di un certo livello (per esempio, in World of Warcraft i giocatori non possono proprio acquistare una abitazione).

L’unico difetto grave di Ultima Online fu non girare su Mac. Perfino oggi è un problema che richiede perizia tecnica e complicate installazioni di CrossOver Games oppure wine.

Per quanto riguarda le vecchie edizioni no Internet, Ultima III è addirittura ancora in vendita per cinque dollari, con versione gratuita che inibisce solo la visita ad Ambrosia (essenziale per completare il gioco). Gira su Mountain Lion.

Ultima IV ha una sua applicazione open source, xu4, in via di sviluppo (ma il gioco è giocabile). Servono sempre file originali (Ultima IV for Dos), tutti freeware e reperibili dal sito.

Exult probabilmente permette di fare funzionare i file originali di Ultima VII, ancora in commercio a 5,99 dollari su Good Old Games.

Per gli altri Ultima, è tutto da vedere. A ritrovare i file per Apple II, basterebbe un emulatore. Forse un emulatore Dos come Boxer aiuta specie con i titoli più recenti.

E già, tutto era partito da un articolo.

