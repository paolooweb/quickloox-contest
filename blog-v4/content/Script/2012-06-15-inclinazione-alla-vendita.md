---
title: "Inclinazione alla vendita"
date: 2012-06-15
draft: false
toc: false
comments: true
categories: [ ]
tags: [script]
---
Si chiede Carmine Gallo su Forbes, perché i nuovi MacBook Pro con schermo Retina vengono presentati dentro gli Apple Store con gli schermi inclinati in una posizione ben precisa?

Uno dirà: così lo schermo risalta. Neanche per idea (e poi i pannelli a cristalli liquidi con tecnologia In-Plane Switching, Ips, hanno angoli di visione ampi): sono messi in modo che il visitatore trovi naturale proprio aggiustare lo schermo… e così facendo tocchi il computer.

Ancora qualcuno pensa che l’elettronica di consumo si venda in base ai gigabyte e alle sigle delle schede grafiche? Contano la sensazione, l’esperienza, il comfort, l’ecosistema.

Non dovrebbe suonare strano. Da millenni si va a comprare un’auto e il concessionario, se tiene alla vendita, fa accomodare il cliente al posto di guida, il sedile, il cruscotto, le finiture.

Per misurare l’inclinazione di qualcosa, nel caso, c’è Simply Angle.

