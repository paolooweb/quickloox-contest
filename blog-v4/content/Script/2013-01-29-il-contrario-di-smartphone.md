---
title: "Il contrario di smartphone"
date: 2013-01-29
draft: false
toc: false
comments: true
categories: [ ]
tags: [script]
---
Era stato chiesto poco tempo fa che ripartizione di vendite vi fosse tra iOS e Android.

Una risposta non perfetta ma molto vicina è quella di MacDailyNews nel riportare le ultime stime di Idc.

Avvertenze d’obbligo: mentre i dati Apple sono ufficiali e si riferiscono al venduto, i dati Samsung sono stime. Samsung ha smesso mesi fa di comunicare dati ufficiali di vendita, dopo che sono finiti in mezzo alla causa con Apple per violazione di brevetti e prima ancora si sono dimostrati piuttosto lontani dalla realtà (Samsung dava per venduto quanto mandava in vendita e a volte la differenza era notevole).

Tra gli smartphone (i computer con dentro il telefono), Apple ha venduto nel 2012 136,8 milioni di unità e viene accreditata di un quarto del mercato. Samsung ne avrebbe collocati 215,8 milioni, due quinti del mercato (quasi il quaranta per cento).

Due quinti più un quarto fa tredici ventesimi, ossia: Samsung e Apple da soli fanno quasi due terzi del mercato. Il terzo rimanente è spartito tra tanti piccoli, in primis Nokia (primis si fa per dire, 6,4 percento con una quota di mercato in caduta libera), Htc con il sei percento e Rim (BlackBerry) pure.

Il totale di Android per il 2012 si può approssimare a 330 milioni di apparecchi, forse qualcosina in più, cambia poco. Diciamo, per arrotondare, dodici Android per cinque iPhone.

A questo punto bisogna ricordare che, stime di NetMarketShare alla mano, se dovessimo giudicare la ripartizione di mercato dal traffico su Internet, dovrebbero esserci tre iPhone per ciascun Android. Praticamente il contrario.

Continuo a sentire che Android fa le stesse cose spendendo meno. Ammesso che davvero si spenda meno, si fanno cose diverse, o si fanno in altro modo. Uno smartphone va su Internet, o è assai poco smart.

