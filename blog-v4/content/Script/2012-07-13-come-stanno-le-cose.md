---
title: "Come stanno le cose"
date: 2012-07-13
draft: false
toc: false
comments: true
categories: [ ]
tags: [script]
---
a verità in termini semplici espressa da Harry McCracken:

Harry McCracken @harrymccracken
I see: We’re not allowed to call an iPad a content-creation device until 100% of humans say it’s THE ONLY ACCEPTABLE CONTENT-CREATION DEVICE
11 Lug 12
Non è permesso chiamare iPad un apparecchio buono per produrre contenuti (altrimenti non sembra un giocattolo) fino a quando il cento percento dell’umanità dirà che non esiste alcuna altra via.

D’altronde, santo John Gruber, la definizione di fanboy Apple è qualcuno che ha usato Mac con entusiasmo prima che ci arrivassi io.

Si noti di passaggio che, se i dati estrapolati da Horace Dediu di Asymco sono corretti, Apple ha compiuto un altro passo verso l’eliminazione del Mac: mentre le vendite di computer Windows nel trimestre primaverile sarebbero in rosso profondo, Mac crescerebbe del 15 percento. Un altro segno inconfondibile che in Apple pensano solo agli iCosi.

