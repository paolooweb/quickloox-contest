---
title: "Grandi promesse"
date: 2013-04-03
draft: false
toc: false
comments: true
categories: [ ]
tags: [script]
---
Mi aspetto cose interessanti da Perspective di Pixxa per iOS. È gratis, l’ho scaricato dopo avere visto il primo grafico di questo post di Horace Dediu di Asymco (che merita lettura a parte). Rischia di dare davvero un tocco interessante in più con poca fatica a presentazioni e grafici.

Invece ne sta mantenendo tante, di promesse, OmniGraffle per iPad. Costa un occhio della testa, ma avevo un’emergenza. Me l’ha risolta. Quando una app è fatta pensando con criterio alle possibilità del touch, i risultati sono straordinari e così mi è successo. Sentivo i diagrammi nascermi sotto le dita, letteralmente. In omaggio al criterio di avere sincronia il più possibile totale tra Mac e iOS, comprerò presto anche l’edizione per Mac. Costa due occhi della testa. Però una volta l’anno capita (e merita).

Ultima promessa mantenuta, per addetti ai lavori (ludici): 4eTurnTracker è fenomenale per giocatori (e soprattutto master) di Dungeons and Dragons (D&D) quarta edizione (4e).

Sto cercando di riprodurre almeno parte delle sue capacità dentro MapTool, ma è impresa disperata per il poco tempo a disposizione. E si avvicina al cento percento di copertura del gioco come nessun altro strumento che abbia collaudato riesca a ottenere.

