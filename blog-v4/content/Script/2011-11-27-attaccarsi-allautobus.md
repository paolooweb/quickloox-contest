---
title: "Attaccarsi all'autobus"
date: 2011-11-27
draft: false
toc: false
comments: true
categories: [ ]
tags: [script]
---
Ringrazio Fabio per questo link dal Corriere del Ticino: dalla prossima primavera si potrò navigare in rete wireless sul 70 percento degli autopostali (gli autobus alla svizzera), previa registrazione con nome e indirizzo di posta elettronica.

Per incentivare l’uso dei mezzi pubblici forse ci sono sistemi più intelligenti delle domeniche a piedi.

La Svizzera non è lontanissima da Torino, Milano e altre grandi città. Chissà se qualche amministratore sentirà un’eco lontana dalle montagne.

