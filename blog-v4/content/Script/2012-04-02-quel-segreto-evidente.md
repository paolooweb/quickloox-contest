---
title: "Quel segreto evidente"
date: 2012-04-02
draft: false
toc: false
comments: true
categories: [ ]
tags: [script]
---
Federico ha allietato la mia domenica sera con questa foto.
Tavolette
Il suo commento: indovina che tablet guardavano?

Facile l’ironia, più difficile rendersi conto dei veri motivi per cui iPad spopola nonostante una offerta in letterale esplosione. Quel tavolo è ingombro di alternative, probabilmente a prezzo competitivo se non minore.

iPhone fa ottimi risultati, ma non spopola. iPad invece fa come iPod: supera qualsiasi valutazione di componentistica, prestazioni, dimensioni eccetera. Tutto l’armamentario che in molti ancora credono valido metro di paragone nell’informatica personale.

Tra poche ore bazzicherò un paio di aeroporti e sarà una interessante occasione di sbirciare che cosa estrae dalla borsa la gente. Se capita l’opportunità, voglio anche chiedere il perché. Il segreto di iPad è come quello raccontato nel celeberrimo racconto di Edgar Allan Poe, dove nessuno trovava la lettera nascosta proprio perché era in bella vista.

