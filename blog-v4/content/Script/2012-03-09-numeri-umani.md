---
title: "Numeri umani"
date: 2012-03-09
draft: false
toc: false
comments: true
categories: [ ]
tags: [script]
---
Apprezzato enormemente il fatto che Apple parli di iPad come de Il nuovo iPad. Niente iPad 3, iPad Hd eccetera.

Apprezzato dopo avere visto che cosa è successo al momento dell’uscita di iPhone 4S. Doveva chiamarsi iPhone 5 e invece si chiamava iPhone 4S, dunque era una delusione.

C’entravano qualcosa le specifiche del prodotto, o qualcuno lo aveva provato? Per niente. Non aveva il 5 davanti e dunque non andava bene.

A fare queste considerazioni sono tipicamente i residuati bellici dell’informatica d’antan, quando il disco interno un po’ più grande si meritava un comunicato stampa apposito e all’uscita di una nuova stampante i mensili pubblicavano la notizia con l’imbarazzante titolo Fiocco rosa in casa Hp (o Epson, o chi per loro). Ogni progresso nei numeri era un passo verso l’uscita dalla preistoria tecnologica e andava anche giustamente salutato.

Nell’informatica di oggi i numeri contano sempre meno e per oggetti come iPad si avviano a non servire più a niente. Conta l’esperienza dell’utilizzo. Lo schermo è Retina, non 2.048 x 1.536. A chi importano i pixel? L’idea è proprio che neanche si distinguano più! È fantastico per vedere le foto, mentre il dato del 40 percento in più di saturazione va verso il feticistico e così via. Certo, ha un megapixel in più di un televisore full Hd. Svelto, quanti pixel ha un televisore full Hd? Non tutti riescono a snocciolare la risposta esatta (1.920 x 1.080). Perché non conta. Uno dice full Hd e si è capito, che i film si vedono in un certo modo, che i particolari arrivano a un certo livello di dettaglio. L’idea è chiara senza bisogno di prendere riga e compasso per valutare l’angolo di parallasse rispetto al livello di osservazione del divano.

Che bello se il prossimo iPhone non fosse iPhone 5, ma il nuovo iPhone. Significherebbe un altro passo avanti, non in uscita dalla preistoria ma in direzione della maggiore umanità, nostra e dei nostri oggetti.

