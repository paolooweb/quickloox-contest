---
title: "Tavolette energetiche"
date: 2012-06-18
draft: false
toc: false
comments: true
categories: [ ]
tags: [script]
---
Su istigazione benigna e benvenuta dell’amico Pelo, sono finalmente giunto a condividere su SlideShare la presentazione che ho tenuto a Borgo Valsugana presso l’Istituto di Istruzione Alcide Degasperi davanti alla platea dei docenti.

Borgo valsugana alla tavoletta di lavoro

View more presentations from Lucio Bragagnolo
Notando di passaggio che SlideShare non vale ancora iWork, ma iWork chiude entro pochi giorni, ho affrontato un ambiente nel quale le tavolette si preparano a entrare in forma sperimentale e giustamente il corpo insegnante si interroga sul come e sul perché. Su decine di docenti si trovano tutte le posizioni concepibili, dall’ottimismo fattivo alla nostalgia per lavagne e gessetti. E nessuno ha concepito una didattica basata sui nuovi strumenti informatici, perché sono arrivati ieri e anche un po’ perché non è come nei centocinquant’anni precedenti, dove i cambiamenti si sono riflettuti in nuovi indirizzi editoriali dei libri di testo, che però sempre libri sono stati, proprio come le lavagne.

L’ho presa alla lontana, anche perché non sono un docente. Ma sono uno studente, a mio modo e non solo mio, perché l’autoaggiornamento costituisce la parola d’ordine di questi anni e non si vede all’orizzonte il suo tramonto. Mi servono strumenti efficaci per apprendere e, se anche fosse una novità per me, per la scuola e per gli studenti questa dovrebbe essere urgenza quotidiana.

Così ho parlato delle materie trasversali, quelle che non stanno nei programmi ministeriali e sono essenziali per crearsi una carriera lavorativa, lato informatico: le ho chiamate tech skill, un sottoinsieme dei soft skill di cui si dibatte spesso nei convegni, contrapposte agli hard skill che più o meno sono le effettive materie di studio. Ipertesto, modellazione 3D, editing di video e audio, ricerca e analisi dei dati, sono conoscenze che chi non metterà a frutto oggi lo metteranno in difficoltà domani.

Alla presentazione andrebbe aggiunto nell’elenco l’educazione digitale, il sapersi muovere sulle reti sociali come Facebook e Twitter, che domani saranno di valore professionale. E ho mostrato come esistano app capaci di esaltare questi aspetti e permettere alle classi di studiare le cose solite in modo diverso, lavorandole per arrivare al completamento del programma ufficiale e contemporaneamente sviluppare materie trasversali.

Tutto questo è necessario in un mondo globale, dove lo studio dei nostri ragazzi non si raffronta più con poche altre nazioni del mondo sviluppato, ma con letteralmente milioni di studenti, spesso più equipaggiati dei nostri, per i quali il multilinguismo non è un problema quando da noi siamo ancora alle prese con l’inglese. Mentre tenevo la mia presentazione, cinque liceali pugliesi stavano a Pittsburgh a competere con coetanei da settanta (settanta) nazioni diverse per borse di studio e agevolazioni per un totale di tre milioni di dollari. Nel mondo di domani un’esperienza all’estero sarà una conditio sine qua non e non solo per chi aspira a un ruolo aziendale.

Come fare lezione allora con i tablet? Ho parlato di fare lezione con iPad (e mi è stato rinfacciato), perché si parla tanto, ma alla fine di vere alternative, sui grandi numeri, non ce ne sono. Non parliamo della qualità del software o dell’hardware.

Sono in corso mille sperimentazioni e forse è anche un po’ tardi per sperimentare: quel 25.700 è il numero degli iPad acquistati dalle scuole primarie di San Diego, California, per metterli in uso da qui a novembre. In Corea del Sud hanno deciso di passare integralmente ai libri di testo digitali stanziando una cifra ingente in pochi anni. Se usare iPad a scuola procura un vantaggio, siamo già indietro, altro che sperimentazione. E se è importante per gli studenti e per l’apprendimento, l’argomento dei costi dovrebbe avere una rilevanza diversa da eh, ma costano. Per anni, da studente, ho comprato e ricomprato sempre gli stessi libri, vagamente revisionati anno dopo anno per giustificare l’aumento del prezzo di copertina, spendendo molto più di un iPad. Ma alle autorità scolastiche non è mai importato più di tanto. Ora forse è agli studenti che non dovrebbe importare più di tanto quanto dovranno spendere le scuole; tanto alla fine sono sempre i genitori che pagano e allora meglio che spendano in modo utile e produttivo.

Ma si diceva, come fare lezione con iPad? Risposte dirette è difficile darne. Indirettamente bisogna lavorare in modo trasversale e orizzontale. Non esiste più il professore che dalla cattedra trasmette il sapere agli alunni, giù in basso. C’è chi punta molto sulle Lavagne Interattive Magnetiche, che però perpetuano il modello del docente centro dell’universo, che distribuisce il sapere. Il sapere oggi va cercato insieme, in condivisione, facendo rete e non unicamente nel senso di Internet. Altrimenti, intanto che va la lavagna interattiva magnetica, gli studenti si fanno i fatti loro. Meglio avere un insegnante che sia mentore e guida, capace di coordinare il lavoro degli studenti all’esplorazione e alla rielaborazione del sapere, con compiti che costituiranno non solo in tesine, ma anche in podcast audio, filmati, documenti interattivi, pagine su Wikipedia (da scrivere, a scuola, non da leggere!). Sequestrare i cellulari in classe? Piuttosto, farli lavorare. Si collegano tutti a Internet, possono fare ricerche, possono manipolare dati… che diventino attrezzature di lavoro. E gli studenti che non possono permettersene uno, si dirà? Ci saranno strumenti offerti dalla scuola, si farà condivisione tra gli studenti di quello che c’è. Uno o due giorni a settimana si dovrebbe lavorare senza mezzi informatici, per acquisire padronanza manuale di tecniche e nozioni e per creare vera consapevolezza di che cosa significhi avere (o non avere) strumenti digitali.

Ma gli strumenti digitali sono indispensabili. Pensare di insegnare matematica o geometria senza app come Quick Graph è mettersi volontariamente un gradino sotto (gratis in versione base, 1,59 euro per avere tutte le funzioni e poi si fanno le questioni su quanto costano gli iPad). È solo un esempio.

Oggi le stampanti 3D sono scese sotto la soglia dei cinquecento dollari. Entro un triennio costeranno la metà. Quante scuole dovranno averne una, o risultare irrilevanti e così i loro studenti? 123D Sculpt è gratis. E sono due. E su App Store c’è un intero canale di app per la scuola, a prezzi nulli o ridicoli.

E torniamo all’inizio, alle materie trasversali. iPad o meno, gli studenti devono imparare Html, 3D, montaggio audio e video, ricerca e analisi dati, educazione digitale e attraverso tutto ciò occuparsi di letteratura, storia, chimica, fisica e quant’altro. La tavoletta consente di farlo meglio, più in fretta, a costi minori (sui tre anni, certo), con più efficacia, con ingombro nullo, autonomia perfetta e spazio per tutti i libri di testo che potranno mai servire. Dovessi inventare uno slogan, direi che la tavoletta dà energia allo studio.

Qualche insegnante è convinto che tutto ciò sia appannaggio dei suoi colleghi più giovani, perché a lui manca qualche anno alla pensione. Già, ma quanti ragazzi gli passano davanti, intanto? Che si aggiorni e studi quello che serve, o prenda atto che non è più all’altezza della sua missione, quale che sia il suo mestiere.

Per tanti anni gli insegnanti hanno potuto passare qualche anno a imparare e il resto della loro vita a spiegarlo. Oggi l’insegnante che non impara continuamente è un ossimoro. Ogni giorno che la scuola trascura la rete e il mondo digitale, o lo affronta nel solito modo verticistico e sindacalizzato, si scivola tutti un piccolo passo più indietro.

Mi scuso per la lunghezza. È che ci tengo.

