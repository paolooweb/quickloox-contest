---
title: "Tessuto umano"
date: 2011-11-22
draft: false
toc: false
comments: true
categories: [ ]
tags: [script]
---
Della tecnologia facciamo anche usi puerili, pericolosi, inutili.

Eppure, a guardare queste mappe della comunicazione Twitter attraverso il globo, viene solo da pensare alla grandezza dell’essere umano e alla preziosità del linguaggio, che tesse trame (idealmente) visibili anche dallo spazio.

