---
title: "Faccia un passo avanti"
date: 2011-11-25
draft: false
toc: false
comments: true
categories: [ ]
tags: [script]
---
Grazie a Mario per avermi segnalato un bel post di Fraser Speirs sulle applicazioni ambiziose per iOS.

Si possono chiamare tali, secondo Speirs, quelle che perseguono almeno uno dei seguenti obiettivi:

Spremere il massimo delle prestazioni dall’apparecchio
Portare su iOS un’applicazione che non si sarebbe pensata possibile
Permettere l’esecuzione completa su iOS di una attività
Rendere possibile qualcosa che su Mac non è così semplice
Sono nominati vari esempi di applicazione, perfettamente comprensibili anche senza bisogno dell’inglese. Per chi pensa di conoscere di suo applicazioni ambiziose per iOS che Speirs non ha trovato, come è logico che sia dato che nessun umano è in grado di conoscere tutta la produzione presente in App Store, le può inserire in un foglio elettronico condiviso su Google Documenti. Attenzione, perché i criteri che valgono sono quelli sopra esposti e Speirs cancellerà quelli che a suo giudizio sono solo esempi di buone applicazioni, ma non di applicazioni ambiziose. Quelle che davvero fanno compiere un passo avanti alla piattaforma.

Ho provato a inserire Textastic. Vediamo che se ne pensa.

