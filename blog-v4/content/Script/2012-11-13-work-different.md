---
title: "Work different"
date: 2012-11-13
draft: false
toc: false
comments: true
categories: [ ]
tags: [script]
---
La nozione meno compresa da persone e aziende altrimenti di intelligenza anche straordinaria: Apple funziona in modo diverso dalle altre aziende negli stessi settori.

Le comparazioni quindi, con Microsoft o Samsung o Sony o Disney o Google o Ibm, sono fuori luogo. A meno che siano state preventivamente elaborate in modo da riflettere la diversità.

I due esempi di oggi – sì, potrebbe essere una rassegna quotidiana, o quasi – sono i seguenti.

Su Asymco il bravo Horace Dediu analizza l’andamento delle azioni Apple in funzione dei profitti e scopre una cosa interessante: le prospettive promettenti possono penalizzare il titolo. Perché Apple realizza prodotti dirompenti, che tendono a cambiare le condizioni di gioco. Gli altri invece seguono le condizioni esistenti, così il mercato non ha paura di loro, in quanto non creano incertezza. Mentre un prodotto di successo Apple può creare incertezza e dunque, anche se il prodotto è di successo, il titolo viene punito.

Difficile trovare un’altra organizzazione dove le cose di Borsa funzionino allo stesso modo.

Secondo, Jean-Louis Gassée su Monday Note visita il nuovo, principesco Apple Store di Palo Alto, vicinissimo al quartier generale. Lo trova grande, orgoglioso, elegante e insopportabilmente rumoroso.

Ne deriva una serie di considerazioni comprensive del licenziamento recente di John Browett, ultimo responsabile degli Apple Store, ma il punto è un altro: torna una settimana dopo armato di iPhone e Spl Meter, misuratore dei livelli di rumore, 89 centesimi.

Trova un misuratore professionale dei livelli di rumore installato in un angolo e due dipendenti che portano sulle spalle rilevatori di suono omnidirezionali. Da quello che appare, commenta, Apple sta affrontando seriamente il problema.

Mi è capitato di entrare in numerosi negozi monomarca, di abbigliamento, giocattoli, elettronica, auto e così via, ma rilevatori di rumore non ne ho mai visti. Non parliamo di elettronica di consumo, ristoranti, supermercati.

Apple può fare giusto oppure può sbagliare. Difficilmente però le nozioni derivati dall’osservazione di altre aziende si possono applicare produttivamente al suo caso.

