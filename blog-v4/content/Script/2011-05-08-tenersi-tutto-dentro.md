---
title: "Tenersi tutto dentro"
date: 2011-05-08
draft: false
toc: false
comments: true
categories: [ ]
tags: [script]
---
Grande suggerimento di Marco Arment, il programmatore che ha creato Instapaper: tenere cifrato il backup di iPhone.

Niente a che vedere con la questione della sicurezza; tenere cifrato il backup significa che in caso di ripristino avrà già a disposizione le password di posta elettronica e accesso MobileMe.

Se il backup invece non è cifrato, le password vanno reinserite a mano. E la nostra pigrizia sa raggiungere vette da Champions League.

