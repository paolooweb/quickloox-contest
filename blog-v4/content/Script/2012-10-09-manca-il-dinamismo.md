---
title: "Manca il dinamismo"
date: 2012-10-09
draft: false
toc: false
comments: true
categories: [ ]
tags: [script]
---
La vicenda Pdf raccontata pochi giorni fa si è conclusa.

Lo staff di Readdle ha mantenuto la promessa e convertito il mio Pdf dinamico in una versione statica, che apparentemente sembra compilarsi a dovere in Pdf Expert. Non è sempre garantito che vada così, però, e sono passati giorni.

Mi era stato consigliato di provare PdfPen e ho scritto alla casa produttrice. Risposta del loro supporto: la app non è compatibile.

Riassumendo: vengono prodotti in formato Pdf anche moduli da compilare (form). Questi sono Acroforms, creati con Acrobat di Adobe, oppure seguono una specifica detta Xfa, Xml Forms Architecture, usata dalla suite di prodotti Livecycle, sempre di Adobe.

Da quello che ho compreso finora, se un form nasce da Acrobat, è compilabile su iPad con software come Pdf Expert, PdfPen o altro. Se invece nasce da LiveCycle, non c’è speranza. Occorre un Mac e occorrono strumenti Adobe, sul Reader per iPad della quale la compilazione dei form ignora l’esistenza di Xfa. Il quale, a leggere, sembra peraltro studiato dall’Ufficio complicazione affari semplici giusto per favorire qualche vendita in più.

