---
title: "Un flashback sulla sicurezza"
date: 2012-04-10
draft: false
toc: false
comments: true
categories: [ ]
tags: [script]
---
Parole immortali che Bruce Schneier, guru della sicurezza, scrisse nel lontanissimo 2000:

Security is a process, not a product. Products provide some protection, but the only way to effectively do business in an insecure world is to put processes in place that recognize the inherent insecurity in the products. The trick is to reduce your risk of exposure regardless of the products or patches.

La sicurezza è un procedimento, non un prodotto. I prodotti forniscono una qualche protezione, ma il solo modo di condurre efficacemente business in un mondo insicuro consiste nell’impiantare procedimenti che tengano in conto l’insicurezza intrinseca dei prodotti. Il trucco è ridurre il proprio rischio di esposizione quali che siano i prodotti o gli aggiornamenti software.

Rileggere e applicare al caso Flashback, il malware che avrebbe colpito seicentomila Mac stando a un’azienda che si fa più pubblicità se annuncia pericoli più sostanziosi e lavora nel campo, quindi da prendere con le molle. Diciamo che siano cifre vere e quindi un Mac su cento contenga la minaccia.

Flashback fa finta di essere un installatore di Flash. Penso che i Mac senza plugin Flash installato di serie siano una minoranza. Da questa vanno defalcati quanti, volendo scaricare Flash di Adobe, lo fanno dal sito Adobe. Nonché quelli che più saggiamente rinunciano a Flash, i cui problemi di sicurezza, come si vede, sono sia diretti che indiretti: il mio Mac, da cui ho levato Flash, è certamente pulito. (Molti si lamentarono dell’assenza di Flash su iOS, ricordo; iOS oggi non corre rischi di Flashback).

Ne deriva che gli infettati sono persone che scaricavano installatori Flash senza averne bisogno, o comunque da chissà quale sito in chissà quale modo.

Non si pretende la riflessione intellettuale sul ruolo di Flash nel Ventunesimo secolo, ma almeno un minimo di attenzione a quello che si combina.

È veramente sufficiente un prodotto per difendere disattenti, ignoranti, incuranti, ingenui? No, perché la sicurezza è un procedimento.

Su tutto questo si inserisce l’insipienza di Apple, che ha aggiornato la propria versione di Java mesi dopo che la falla utilizzata da Flashback era stata scoperta. Ma sarebbero stati comunque sessantamila, o seimila, o seicento: nessun prodotto potrà mia proteggere pienamente i gente che se la va a cercare. E la scarsa dimestichezza con il computer è scusa che vale fino a un certo punto: non sono più gli anni novanta, dove a usare il computer erano gli stregoni.

Cnet ha pubblicato una pagina contenente tutte le spiegazioni su Flashback, sulla sua individuazione e sulla sua eradicazione. Sì, è in inglese. In italiano c’è già una pagina del sito-fuffa per eccellenza, che elenca i tool per effettuare i controlli del caso senza bisogno di scrivere comandi nel Terminale. Munitevi di prodotti, sembra affermare, e sarete sicuri. Guai a sapere qualcosa di Terminale e a sporcarsi le mani con la conoscenza. E allora è il caso di impegnarsi un pochino, con l’inglese se necessario, per non fare la fine di quelli che scaricano Flashback con il vuoto in testa e il cervello lavato via browser.

