---
title: "La tendenza a sparire"
date: 2012-03-31
draft: false
toc: false
comments: true
categories: [ ]
tags: [script]
---
L’avvento di iOS ha creato varie ondate di sconcerto. Una di queste agita i sonni di chi ha (meritoriamente) letto Mac OS X Human Interface Guidelines e rimase convinto che quelle fossero le linee guida non tanto per un personal computer, quanto per qualsiasi apparecchio elettronico.

E così, se una delle seicentomila app su App Store per iOS esplora territori nuovi, grida allo scandalo.

Il fatto è iOS è fatto da apparecchi radicalmente diversi. Per dire, la visualizzazione delle informazioni si alterna al loro inserimento. Su Mac c’è un sistema per mostrare le cose e un altro sistema per inserirle.

Se uno vede Paper o Clear, e contesta in nome dell’interfaccia umana, sbaglia. Non siamo più su personal computer e per esempio, soprattutto su un apparecchio come iPhone, quando lo schermo è ridotto, l’interfaccia agisce al meglio se fa il possibile per restare fuori vista.

