---
title: "Il tocco imprevisto"
date: 2012-12-07
draft: false
toc: false
comments: true
categories: [ ]
tags: [script]
---
Placidamente appoggiato sulla scrivania, da un minuto a quello dopo il touchscreen di iPhone 4 ha cessato di reagire.

Ho provato tutti i ripristini possibili, a tutti i livelli, ma nulla è valso. Suppongo che la circuiteria dello schermo abbia ceduto, approssimativamente due anni e quattro mesi dopo l’acquisto (avvenuto ad agosto 2010, prima dell’Assunta, devo ancora controllare con precisione).

Motivi lavorativi mi hanno guidato verso il passaggio iPhone 5, di cui voglio comunicare le primissime impressioni.

Arrivando da un iPhone 4, colpiscono due cose: leggerezza e spessore. Non ci si crede e la sensazione di pesantezza in mano praticamente sparisce. Prima ero passato da iPhone 2G a iPhone 4 e garantisco che il passaggio non è altrettanto, positivamente, traumatico.

Senza che la leggerezza trasmetta pochezza. L’insieme comunica di essere solido e affidabile.

iPhone 4 è anche veloce. Meglio, il mio lo era. iPhone 5 è… liquido. Risponde sollecito e fluido, come se le icone fossero fisiche e sospese in un gel ad attrito zero. Il nuovo schermo più alto mette a disposizione una riga in più di icone che è ora vuota e disorientante. Sembra una specie di sfida, la promessa di potere avere sulla home più cose che servono.

La custodia degli auricolari è incredibilmente minuscola e ingegnosa. Praticamente occupa lo stesso spazio che usavano gli auricolari nella scatola di iPhone 4, solo che quelli erano semplicemente avvolti in un nastro di plastica e qui c’è una scatolina geniale. Gli auricolari stessi, riprogettati, suonano meglio. Non saprei dire quanto meglio o come meglio, non sono esperto di audio. Meglio, però, è indiscutibile.

Installazione e configurazione hanno cessato di risultare interessanti per un recensore. Id Apple, iCloud e attesa proporzionata ai dati. Ho – dati e app – lo stesso iPhone di prima, solo in un altro corpo, semplicemente aspettando il compimento dell’operazione.

Ultima nota, da ingenuo poco praticante delle botteghe di telefonia mobile. Passando da iPhone 2G a iPhone 4 ero passato da Sim a microSim. Era risultata una faccenda piuttosto complessa, con qualche giorno su un numero provvisorio in attesa che quello vecchio venisse passato da un operatore all’altro. Passaggio determinato dal fatto che l’operatore di prima non aveva le microSim e pretendeva che aspettassi una settimana. L’operatore nuovo ne aveva una subito.

Stavolta sono andato dall’operatore attuale per passare da microSim a nanoSim. Ce n’era una pronta, poche ore teoriche per il passaggio del numero. Sono arrivato a casa e tutto ha funzionato subito.

Sono una persona poco pratica. Operare con le nanoSim inizia ad avvicinarsi alla microchirurgia. D’altronde è un trend inevitabile. Il connettore Lightning è piccolo. Sono più piccoli persino le guaine del cavo auricolari e persino l’impugnatura del cavo Lightning lato Usb.

iPhone 4 mi ha servito molto bene ed era stato acquistato in modo imprevisto, per motivi lavorativi. In qualche modo la storia si ripete e spero di essere ugualmente soddisfatto da iPhone 5.

