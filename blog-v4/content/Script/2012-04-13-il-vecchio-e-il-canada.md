---
title: "Il vecchio e il canada"
date: 2012-04-13
draft: false
toc: false
comments: true
categories: [ ]
tags: [script]
---
All’inizio si era diffusa la voce che la vendita su eBay di un prototipo praticamente unico di Mac, Twiggy, addirittura con floppy disk da 5,25 pollici (3,5 pollici nel prodotto finale), fosse stata avviata da Steve Wozniak, cofondatore di Apple e figura unica di ingegnere.

La base d’asta di centomila dollari è probabilmente giustificata visto la rarità del pezzo, ma se fosse stato veramente Woz mi sarei intristito, come quando certi vecchi pugili vendono la cintura di campione per pagarsi l’affitto.

Grazie a Cult of Mac mi sono rasserenato: la vendita avviene a opera di un canadese.

Si avvicina a essere un altro perché mi piace immensamente l’idea dei musei Apple come All About Apple, purché li faccia qualcun altro.

