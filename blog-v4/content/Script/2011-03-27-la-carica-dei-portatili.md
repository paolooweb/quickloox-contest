---
title: "La carica dei portatili"
date: 2011-03-27
draft: false
toc: false
comments: true
categories: [ ]
tags: [script]
---
Era molto che, leggendo la rassegna stampa, non trovavo a breve distanza due cose interessanti sui portatili: la spiegazione di come abilitare la funzione Trim sui Mac con disco a stato solido (Ssd) e la notizia di porte Usb superalimentate sui nuovi MacBook Pro usciti da qualche settimana.

Superalimentate significa che le porte riescono a erogare fino a 2.100 milliampère, mentre quelli prima arrivano a mille (quando senti che i nuovi MacBook Pro sono uguali a quelli vecchi e che è cambiato solo il processore, fai una risata). In altre parole, il nuovo MacBook Pro riesce a caricare un iPad con efficacia equivalente a quella di un alimentatore autonomo.

La funzione Trim permette di prolungare la vita utile dei dischi a stato solido attraverso certe operazioni di scrittura sul disco stesso. E arriverà di serie su Mac OS X 10.7 Lion (quando senti che Apple trascura Mac OS X e pensa solo a iOS, fai una risata), mentre su Snow Leopard non c’è [aggiornamento: c’è, ma solo sui modelli che montano di serie dischi Ssd].

Chi volesse cimentarsi lo faccia con grande cautela e consapevolezza, oltre che naturalmente se in possesso di un disco a stato solido. A questo livello non si scherza e le disattenzioni possono costare molto.

Altro aggiornamento: è uscito Trim Enabler, che come dice il nome abilita Trim su tutti i Mac con Snow Leopard a bordo.

