---
title: "Software che vale non chiede trucchetti"
date: 2011-05-02
draft: false
toc: false
comments: true
categories: [ ]
tags: [script]
---
Due storie che convergono partendo da punti opposti. Da una parte lo staff di Autodesk che non soltanto è contento di avere riportato AutoCad su Mac, ma è pure rimasto positivamente sorpreso dall’accoglienza ricevuta da AutoCad Ws. Che, per capirci, è quella app per iPhone e iPad che permette di portare dal cliente un progetto 3d dentro un apparecchietto tascabile o ultrasottile e mostrargli tutto per filo e per segno, nonché correggere al volo quella piccola inesattezza dell’ultimo momento o magari integrare all’istante una osservazione del cliente. Fantascienza pura, prima di questo decennio. La cosa da notare è soprattutto che AutoCad, senza il quale la versione Ws ha ben poco da dire, non è certo un programmino da 0,79 euro.

Dall’altro parte ci sono Marco Arment e il proprio Instapaper, software irrinunciabile per la lettura differita di articoli e documenti importanti su web. Su iPhone Instapaper ha avuto storicamente una versione gratis e una a pagamento, mentre su iPad c’è sempre stata solo la versione a pagamento. Che ho acquistato appena possibile.

Arment ha tolto da App Store la versione gratis di Instapaper senza dirlo a nessuno, due volte, e per ben due volte ha assistito a un aumento delle vendite della versione a pagamento. Non solo; nessuno sembra essersi accorto della mancanza della versione gratuita.

Instapaper si usa anche su Mac, gratuitamente con un qualunque browser, ed è comunque una manna dal cielo.

A dire che più le cose hanno valore meno c’è bisogno di strategie arzigogolate per strappare una vendita in più. E che il software meritevole di cui ci conviene approfittare, perché ci dà un vantaggio, va preso subito e senza troppe storie per il prezzo. Instapaper per iPhone o in versione per iPad costa 3,99 euro, che se uno si trova a Milano e pretende anche il succo di frutta oltre al cappuccio e alla brioche, rischia di non farcela.

